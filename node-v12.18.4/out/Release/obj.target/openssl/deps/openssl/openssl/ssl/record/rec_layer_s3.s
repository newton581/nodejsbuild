	.file	"rec_layer_s3.c"
	.text
	.p2align 4
	.globl	RECORD_LAYER_init
	.type	RECORD_LAYER_init, @function
RECORD_LAYER_init:
.LFB964:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	addq	$1352, %rdi
	movl	$1, 6120(%rsi)
	movl	$32, %esi
	jmp	SSL3_RECORD_clear@PLT
	.cfi_endproc
.LFE964:
	.size	RECORD_LAYER_init, .-RECORD_LAYER_init
	.p2align 4
	.globl	RECORD_LAYER_clear
	.type	RECORD_LAYER_clear, @function
RECORD_LAYER_clear:
.LFB965:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	addq	$32, %rdi
	subq	$8, %rsp
	movl	$240, -20(%rdi)
	movq	$0, 3880(%rdi)
	movl	$0, 3904(%rdi)
	movq	$0, 3912(%rdi)
	movq	$0, 3928(%rdi)
	movl	$0, 3936(%rdi)
	movq	$0, 3944(%rdi)
	movq	$0, 3952(%rdi)
	movups	%xmm0, 3888(%rdi)
	call	SSL3_BUFFER_clear@PLT
	movq	(%r12), %rdi
	call	ssl3_release_write_buffer@PLT
	leaq	1352(%r12), %rdi
	movl	$32, %esi
	movq	$0, 16(%r12)
	call	SSL3_RECORD_clear@PLT
	cmpq	$0, 4016(%r12)
	movq	$0, 3992(%r12)
	movq	$0, 4000(%r12)
	je	.L3
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	DTLS_RECORD_LAYER_clear@PLT
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE965:
	.size	RECORD_LAYER_clear, .-RECORD_LAYER_clear
	.p2align 4
	.globl	RECORD_LAYER_release
	.type	RECORD_LAYER_release, @function
RECORD_LAYER_release:
.LFB966:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	$0, 32(%rdi)
	je	.L7
	movq	(%rdi), %rdi
	call	ssl3_release_read_buffer@PLT
.L7:
	cmpq	$0, 24(%rbx)
	jne	.L10
.L8:
	addq	$8, %rsp
	leaq	1352(%rbx), %rdi
	movl	$32, %esi
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	SSL3_RECORD_release@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	ssl3_release_write_buffer@PLT
	jmp	.L8
	.cfi_endproc
.LFE966:
	.size	RECORD_LAYER_release, .-RECORD_LAYER_release
	.p2align 4
	.globl	RECORD_LAYER_read_pending
	.type	RECORD_LAYER_read_pending, @function
RECORD_LAYER_read_pending:
.LFB967:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	$0, 64(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE967:
	.size	RECORD_LAYER_read_pending, .-RECORD_LAYER_read_pending
	.p2align 4
	.globl	RECORD_LAYER_processed_read_pending
	.type	RECORD_LAYER_processed_read_pending, @function
RECORD_LAYER_processed_read_pending:
.LFB968:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L16
	addq	$1408, %rdi
	xorl	%eax, %eax
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L15:
	addq	$1, %rax
	addq	$80, %rdi
	cmpq	%rax, %rdx
	je	.L17
.L14:
	movl	(%rdi), %ecx
	testl	%ecx, %ecx
	jne	.L15
.L13:
	cmpq	%rax, %rdx
	seta	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movq	%rdx, %rax
	cmpq	%rax, %rdx
	seta	%al
	movzbl	%al, %eax
	ret
.L16:
	xorl	%eax, %eax
	jmp	.L13
	.cfi_endproc
.LFE968:
	.size	RECORD_LAYER_processed_read_pending, .-RECORD_LAYER_processed_read_pending
	.p2align 4
	.globl	RECORD_LAYER_write_pending
	.type	RECORD_LAYER_write_pending, @function
RECORD_LAYER_write_pending:
.LFB969:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	xorl	%r8d, %r8d
	testq	%rax, %rax
	je	.L18
	leaq	-5(%rax,%rax,4), %rax
	xorl	%r8d, %r8d
	cmpq	$0, 104(%rdi,%rax,8)
	setne	%r8b
.L18:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE969:
	.size	RECORD_LAYER_write_pending, .-RECORD_LAYER_write_pending
	.p2align 4
	.globl	RECORD_LAYER_reset_read_sequence
	.type	RECORD_LAYER_reset_read_sequence, @function
RECORD_LAYER_reset_read_sequence:
.LFB970:
	.cfi_startproc
	endbr64
	movq	$0, 3992(%rdi)
	ret
	.cfi_endproc
.LFE970:
	.size	RECORD_LAYER_reset_read_sequence, .-RECORD_LAYER_reset_read_sequence
	.p2align 4
	.globl	RECORD_LAYER_reset_write_sequence
	.type	RECORD_LAYER_reset_write_sequence, @function
RECORD_LAYER_reset_write_sequence:
.LFB971:
	.cfi_startproc
	endbr64
	movq	$0, 4000(%rdi)
	ret
	.cfi_endproc
.LFE971:
	.size	RECORD_LAYER_reset_write_sequence, .-RECORD_LAYER_reset_write_sequence
	.p2align 4
	.globl	ssl3_pending
	.type	ssl3_pending, @function
ssl3_pending:
.LFB972:
	.cfi_startproc
	endbr64
	xorl	%r8d, %r8d
	cmpl	$241, 2124(%rdi)
	je	.L24
	movq	2128(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L24
	addq	$3468, %rdi
	xorl	%eax, %eax
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L31:
	addq	$1, %rax
	addq	4(%rdi), %r8
	addq	$80, %rdi
	cmpq	%rdx, %rax
	je	.L24
.L26:
	cmpl	$23, (%rdi)
	je	.L31
	xorl	%r8d, %r8d
.L24:
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE972:
	.size	ssl3_pending, .-ssl3_pending
	.p2align 4
	.globl	SSL_CTX_set_default_read_buffer_len
	.type	SSL_CTX_set_default_read_buffer_len, @function
SSL_CTX_set_default_read_buffer_len:
.LFB973:
	.cfi_startproc
	endbr64
	movq	%rsi, 480(%rdi)
	ret
	.cfi_endproc
.LFE973:
	.size	SSL_CTX_set_default_read_buffer_len, .-SSL_CTX_set_default_read_buffer_len
	.p2align 4
	.globl	SSL_set_default_read_buffer_len
	.type	SSL_set_default_read_buffer_len, @function
SSL_set_default_read_buffer_len:
.LFB974:
	.cfi_startproc
	endbr64
	movq	%rsi, 2152(%rdi)
	ret
	.cfi_endproc
.LFE974:
	.size	SSL_set_default_read_buffer_len, .-SSL_set_default_read_buffer_len
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unknown"
.LC1:
	.string	"read done"
.LC2:
	.string	"read body"
.LC3:
	.string	"read header"
	.text
	.p2align 4
	.globl	SSL_rstate_string_long
	.type	SSL_rstate_string_long, @function
SSL_rstate_string_long:
.LFB975:
	.cfi_startproc
	endbr64
	movl	2124(%rdi), %edx
	leaq	.LC2(%rip), %rax
	cmpl	$241, %edx
	je	.L34
	leaq	.LC1(%rip), %rax
	cmpl	$242, %edx
	je	.L34
	cmpl	$240, %edx
	leaq	.LC0(%rip), %rax
	leaq	.LC3(%rip), %rdx
	cmove	%rdx, %rax
.L34:
	ret
	.cfi_endproc
.LFE975:
	.size	SSL_rstate_string_long, .-SSL_rstate_string_long
	.section	.rodata.str1.1
.LC4:
	.string	"RD"
.LC5:
	.string	"RB"
.LC6:
	.string	"RH"
	.text
	.p2align 4
	.globl	SSL_rstate_string
	.type	SSL_rstate_string, @function
SSL_rstate_string:
.LFB976:
	.cfi_startproc
	endbr64
	movl	2124(%rdi), %edx
	leaq	.LC5(%rip), %rax
	cmpl	$241, %edx
	je	.L41
	leaq	.LC4(%rip), %rax
	cmpl	$242, %edx
	je	.L41
	cmpl	$240, %edx
	leaq	.LC0(%rip), %rax
	leaq	.LC6(%rip), %rdx
	cmove	%rdx, %rax
.L41:
	ret
	.cfi_endproc
.LFE976:
	.size	SSL_rstate_string, .-SSL_rstate_string
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"../deps/openssl/openssl/ssl/record/rec_layer_s3.c"
	.text
	.p2align 4
	.globl	ssl3_read_n
	.type	ssl3_read_n, @function
ssl3_read_n:
.LFB977:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, -72(%rbp)
	testq	%rsi, %rsi
	je	.L64
	movq	%rsi, %r13
	movq	2144(%rdi), %rsi
	movq	%rdi, %r12
	movq	%rdx, %r10
	movl	%ecx, %r15d
	testq	%rsi, %rsi
	je	.L105
.L51:
	movq	$-5, %rcx
	movq	2176(%r12), %rbx
	subq	%rsi, %rcx
	andl	$7, %ecx
	cmpl	$1, %r8d
	leaq	(%rsi,%rcx), %rdi
	sete	%al
	movq	%rdi, -56(%rbp)
	testl	%r15d, %r15d
	je	.L52
	movq	6024(%r12), %rsi
	movq	6032(%r12), %r14
	cmpq	%rdi, %rsi
	setne	%dl
	andl	%edx, %eax
	testb	%al, %al
	jne	.L106
.L61:
	movq	8(%r12), %rax
	movq	192(%rax), %rax
	movl	96(%rax), %eax
	andl	$8, %eax
	je	.L79
	testq	%rbx, %rbx
	jne	.L78
	testl	%r15d, %r15d
	je	.L78
.L64:
	xorl	%r8d, %r8d
.L48:
	addq	$40, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	testq	%rbx, %rbx
	je	.L107
	addq	2168(%r12), %rsi
	testq	%rcx, %rcx
	je	.L104
	cmpq	$4, %rbx
	jbe	.L104
	cmpb	$23, (%rsi)
	je	.L59
.L104:
	cmpq	-56(%rbp), %rsi
	movq	%rsi, 6024(%r12)
	movq	$0, 6032(%r12)
	setne	%dl
	xorl	%r14d, %r14d
	andl	%edx, %eax
	testb	%al, %al
	je	.L61
.L106:
	movq	-56(%rbp), %rdi
	leaq	(%rbx,%r14), %rdx
	movq	%r10, -80(%rbp)
	movq	%rcx, -64(%rbp)
	call	memmove@PLT
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %rax
	movq	-80(%rbp), %r10
	addq	%r14, %rcx
	movq	%rax, 6024(%r12)
	movq	%rcx, 2168(%r12)
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L107:
	movq	%rcx, 2168(%r12)
	movq	%rdi, 6024(%r12)
	movq	$0, 6032(%r12)
.L55:
	movq	8(%r12), %rax
	xorl	%r14d, %r14d
	movq	192(%rax), %rax
	movl	96(%rax), %eax
	andl	$8, %eax
	je	.L79
.L78:
	movq	2168(%r12), %rdx
	testq	%rbx, %rbx
	je	.L63
	xorl	%eax, %eax
	cmpq	%rbx, %r13
	jbe	.L101
.L66:
	movq	%rax, 2176(%r12)
	movq	-72(%rbp), %rax
	addq	%rbx, %rdx
	movl	$1, %r8d
	addq	%rbx, 6032(%r12)
	movq	%rdx, 2168(%r12)
	movq	%rbx, (%rax)
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L79:
	movq	2168(%r12), %rdx
	xorl	%eax, %eax
.L63:
	cmpq	%rbx, %r13
	jbe	.L101
	movq	2160(%r12), %rcx
	subq	%rdx, %rcx
	cmpq	%r13, %rcx
	jb	.L108
	orl	2120(%r12), %eax
	movq	%r13, -64(%rbp)
	je	.L70
	cmpq	%r10, %r13
	cmovnb	%r13, %r10
	cmpq	%rcx, %r10
	cmovbe	%r10, %rcx
	movq	%rcx, -64(%rbp)
.L70:
	call	__errno_location@PLT
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L77:
	movq	16(%r12), %rdi
	movl	$0, (%r15)
	testq	%rdi, %rdi
	je	.L71
	movl	$3, 40(%r12)
	movl	-64(%rbp), %edx
	leaq	(%r14,%rbx), %rsi
	addq	-56(%rbp), %rsi
	subl	%ebx, %edx
	call	BIO_read@PLT
	movslq	%eax, %r8
	movl	$0, %eax
	testl	%r8d, %r8d
	cmovns	%r8, %rax
	jle	.L72
	addq	%rax, %rbx
	movq	8(%r12), %rax
	movq	192(%rax), %rax
	testb	$8, 96(%rax)
	je	.L74
	cmpq	%rbx, %r13
	cmova	%rbx, %r13
	cmpq	%rbx, %r13
	ja	.L77
.L76:
	movq	-72(%rbp), %rax
	subq	%r13, %rbx
	addq	%r13, 2168(%r12)
	movl	$1, %r8d
	addq	%r13, 6032(%r12)
	movq	%rbx, 2176(%r12)
	movl	$1, 40(%r12)
	movq	%r13, (%rax)
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L74:
	cmpq	%r13, %rbx
	jb	.L77
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L71:
	leaq	.LC7(%rip), %r8
	movl	$300, %r9d
	movl	$211, %ecx
	movq	%r12, %rdi
	movl	$149, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movl	$-1, %r8d
.L72:
	movq	%rbx, 2176(%r12)
	testb	$16, 1496(%r12)
	je	.L48
	movq	8(%r12), %rax
	movq	192(%rax), %rax
	testb	$8, 96(%rax)
	jne	.L48
	addq	%rbx, %r14
	jne	.L48
	movq	%r12, %rdi
	movl	%r8d, -56(%rbp)
	call	ssl3_release_read_buffer@PLT
	movl	-56(%rbp), %r8d
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L105:
	movl	%r8d, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	ssl3_setup_read_buffer@PLT
	testl	%eax, %eax
	je	.L80
	movq	2144(%r12), %rsi
	movl	-64(%rbp), %r8d
	movq	-56(%rbp), %r10
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L101:
	movq	%rbx, %rax
	movq	%r13, %rbx
	subq	%r13, %rax
	jmp	.L66
.L59:
	movzwl	3(%rsi), %edx
	rolw	$8, %dx
	cmpw	$127, %dx
	jbe	.L104
	movq	%rbx, %rdx
	movq	%r10, -80(%rbp)
	movq	%rcx, -64(%rbp)
	call	memmove@PLT
	movq	-64(%rbp), %rcx
	movq	-80(%rbp), %r10
	movq	$0, 6032(%r12)
	movq	%rcx, 2168(%r12)
	addq	2144(%r12), %rcx
	movq	%rcx, -56(%rbp)
	movq	%rcx, 6024(%r12)
	jmp	.L55
.L108:
	leaq	.LC7(%rip), %r8
	movl	$266, %r9d
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$149, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movl	$-1, %r8d
	jmp	.L48
.L80:
	movl	$-1, %r8d
	jmp	.L48
	.cfi_endproc
.LFE977:
	.size	ssl3_read_n, .-ssl3_read_n
	.p2align 4
	.globl	ssl3_write_pending
	.type	ssl3_write_pending, @function
ssl3_write_pending:
.LFB980:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, -64(%rbp)
	cmpq	%rcx, 6072(%rdi)
	ja	.L110
	leaq	2184(%rdi), %rbx
	testb	$2, 1496(%rdi)
	je	.L127
.L111:
	cmpl	%esi, 6080(%r15)
	jne	.L110
	movq	2216(%r15), %rdx
	xorl	%r13d, %r13d
	movq	%rdx, -56(%rbp)
	call	__errno_location@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L112:
	leaq	0(%r13,%r13,4), %rax
	salq	$3, %rax
	leaq	(%rbx,%rax), %r14
	testq	%rdx, %rdx
	je	.L114
.L115:
	movq	24(%r15), %rdi
	movl	$0, (%r12)
	testq	%rdi, %rdi
	je	.L117
	movl	$2, 40(%r15)
	movq	24(%r14), %rsi
	addq	(%r14), %rsi
	call	BIO_write@PLT
	testl	%eax, %eax
	jle	.L119
	movq	24(%r14), %rsi
	movq	32(%r14), %rdx
	cltq
	addq	%rax, %rsi
	cmpq	%rax, %rdx
	je	.L128
	subq	%rax, %rdx
	movq	%rsi, 24(%r14)
	movq	%rdx, 32(%r14)
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L121:
	leaq	0(%r13,%r13,4), %rax
	salq	$3, %rax
	leaq	(%rbx,%rax), %r14
	.p2align 4,,10
	.p2align 3
.L114:
	movq	2136(%r15), %rcx
	xorl	%edx, %edx
	leaq	-1(%rcx), %rsi
	cmpq	%r13, %rsi
	jbe	.L115
	movq	72(%rbx,%rax), %rdx
	addq	$1, %r13
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L117:
	movl	$1161, %r9d
	leaq	.LC7(%rip), %r8
	movl	$128, %ecx
	movq	%r15, %rdi
	movl	$159, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movl	$-1, %eax
.L119:
	movq	8(%r15), %rdx
	movq	192(%rdx), %rdx
	testb	$8, 96(%rdx)
	je	.L109
	movq	$0, 32(%r14)
.L109:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	movq	$0, 32(%r14)
	leaq	1(%r13), %rax
	movq	%rsi, 24(%r14)
	cmpq	2136(%r15), %rax
	jb	.L121
	movq	6088(%r15), %rax
	movq	-64(%rbp), %rbx
	movl	$1, 40(%r15)
	movq	%rax, (%rbx)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	cmpq	%rdx, 6096(%rdi)
	je	.L111
.L110:
	movl	$1138, %r9d
	leaq	.LC7(%rip), %r8
	movl	$127, %ecx
	movq	%r15, %rdi
	movl	$159, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movl	$-1, %eax
	jmp	.L109
	.cfi_endproc
.LFE980:
	.size	ssl3_write_pending, .-ssl3_write_pending
	.p2align 4
	.globl	do_ssl3_write
	.type	do_ssl3_write, @function
do_ssl3_write:
.LFB979:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movl	%esi, -4212(%rbp)
	movq	%rdi, %r12
	movq	%rdx, -4224(%rbp)
	movq	%rcx, -4208(%rbp)
	movq	%r8, -4200(%rbp)
	movl	%r9d, -4216(%rbp)
	movq	%rax, -4264(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -4192(%rbp)
	testq	%r8, %r8
	je	.L243
	leaq	-1(%r8), %rax
	movq	%r8, %rbx
	cmpq	$2, %rax
	jbe	.L244
	shrq	%rbx
	movq	%rcx, %rax
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdx
	salq	$4, %rdx
	addq	%rcx, %rdx
	.p2align 4,,10
	.p2align 3
.L132:
	movdqu	(%rax), %xmm2
	addq	$16, %rax
	paddq	%xmm2, %xmm0
	cmpq	%rax, %rdx
	jne	.L132
	movq	-4200(%rbp), %rcx
	movdqa	%xmm0, %xmm1
	psrldq	$8, %xmm1
	movq	%rcx, %rax
	paddq	%xmm1, %xmm0
	andq	$-2, %rax
	andl	$1, %ecx
	movq	%xmm0, %r13
	je	.L130
.L131:
	movq	-4208(%rbp), %rcx
	movq	-4200(%rbp), %rdi
	leaq	1(%rax), %rdx
	addq	(%rcx,%rax,8), %r13
	cmpq	%rdx, %rdi
	jbe	.L130
	addq	$2, %rax
	addq	(%rcx,%rdx,8), %r13
	cmpq	%rax, %rdi
	jbe	.L130
	addq	(%rcx,%rax,8), %r13
.L130:
	movq	2136(%r12), %rax
	testq	%rax, %rax
	je	.L134
	leaq	-5(%rax,%rax,4), %rdx
	cmpq	$0, 2216(%r12,%rdx,8)
	je	.L134
	movq	-4264(%rbp), %r8
	movq	-4224(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movl	-4212(%rbp), %esi
	call	ssl3_write_pending
.L129:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L371
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	movq	168(%r12), %rdx
	movl	252(%rdx), %ebx
	testl	%ebx, %ebx
	je	.L136
	movq	8(%r12), %rax
	movq	%r12, %rdi
	call	*120(%rax)
	testl	%eax, %eax
	jle	.L129
	movq	2136(%r12), %rax
.L136:
	cmpq	%rax, -4200(%rbp)
	ja	.L137
.L140:
	testq	%r13, %r13
	jne	.L139
	movl	-4216(%rbp), %r11d
	xorl	%eax, %eax
	testl	%r11d, %r11d
	je	.L129
.L139:
	cmpq	$0, 1296(%r12)
	movq	1136(%r12), %rax
	je	.L141
	testq	%rax, %rax
	je	.L141
	movq	1160(%r12), %rdi
	call	EVP_MD_CTX_md@PLT
	testq	%rax, %rax
	je	.L372
	movq	1160(%r12), %rdi
	call	EVP_MD_CTX_md@PLT
	movq	%rax, %rdi
	call	EVP_MD_size@PLT
	movl	%eax, %ecx
	movl	%eax, -4268(%rbp)
	movl	-4216(%rbp), %eax
	testl	%ecx, %ecx
	jns	.L143
	movl	$711, %r9d
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L137:
	movq	-4200(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	ssl3_setup_write_buffer@PLT
	testl	%eax, %eax
	jne	.L140
.L369:
	movl	$-1, %eax
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L372:
	movq	1136(%r12), %rax
.L141:
	movl	$0, -4268(%rbp)
	testq	%rax, %rax
	sete	%al
	movzbl	%al, %eax
	orl	-4216(%rbp), %eax
.L143:
	testl	%eax, %eax
	jne	.L144
	movq	168(%r12), %rax
	movl	220(%rax), %r10d
	testl	%r10d, %r10d
	je	.L373
.L145:
	movq	-4192(%rbp), %rax
	movq	%rax, -4256(%rbp)
.L150:
	cmpq	$0, -4256(%rbp)
	jne	.L155
	leaq	-4160(%rbp), %rax
	cmpq	$0, -4200(%rbp)
	leaq	2184(%r12), %r14
	movq	$-5, %rbx
	movq	%rax, -4232(%rbp)
	je	.L374
	movq	%r12, -4240(%rbp)
	movq	-4256(%rbp), %r13
	movq	%rax, %r12
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L160:
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	WPACKET_allocate_bytes@PLT
	testl	%eax, %eax
	je	.L162
	addq	$1, %r13
	addq	$40, %r14
	addq	$48, %r12
	cmpq	%r13, -4200(%rbp)
	je	.L375
.L163:
	movq	(%r14), %rsi
	movq	%rbx, %r15
	movq	16(%r14), %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	subq	%rsi, %r15
	andl	$7, %r15d
	movq	%r15, 24(%r14)
	call	WPACKET_init_static_len@PLT
	testl	%eax, %eax
	jne	.L160
.L162:
	movq	%r13, -4256(%rbp)
	movq	-4240(%rbp), %r12
	movl	$799, %r9d
.L368:
	leaq	.LC7(%rip), %r8
	movl	$68, %ecx
	movl	$104, %edx
	movq	%r12, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
.L161:
	movq	-4256(%rbp), %rax
	testq	%rax, %rax
	je	.L369
	leaq	(%rax,%rax,2), %r12
	leaq	-4160(%rbp), %rbx
	salq	$4, %r12
	addq	%rbx, %r12
	.p2align 4,,10
	.p2align 3
.L233:
	movq	%rbx, %rdi
	addq	$48, %rbx
	call	WPACKET_cleanup@PLT
	cmpq	%r12, %rbx
	jne	.L233
	movl	$-1, %eax
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L144:
	movl	-4216(%rbp), %esi
	testl	%esi, %esi
	je	.L145
	movq	$-10, %r14
	movq	2184(%r12), %rsi
	xorl	%ecx, %ecx
	leaq	-4160(%rbp), %rax
	movq	2200(%r12), %rdx
	movq	%rax, %rdi
	movq	%rax, -4232(%rbp)
	subq	%rsi, %r14
	andl	$7, %r14d
	movq	%r14, 2208(%r12)
	call	WPACKET_init_static_len@PLT
	testl	%eax, %eax
	je	.L153
	movq	-4232(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	WPACKET_allocate_bytes@PLT
	testl	%eax, %eax
	je	.L153
.L154:
	movq	1136(%r12), %rax
	testq	%rax, %rax
	je	.L247
	movq	$1, -4256(%rbp)
	movq	8(%r12), %rdx
	movq	192(%rdx), %rcx
	movl	96(%rcx), %ecx
	testb	$1, %cl
	jne	.L239
.L248:
	movl	$0, -4248(%rbp)
.L165:
	leaq	-2624(%rbp), %rdi
	xorl	%eax, %eax
	movl	$320, %ecx
	cmpq	$0, -4200(%rbp)
	movq	%rdi, -4280(%rbp)
	rep stosq
	je	.L253
	leaq	-4160(%rbp), %rax
	movq	8(%r12), %rdx
	movq	%rax, %rbx
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L373:
	movl	216(%rax), %r9d
	testl	%r9d, %r9d
	je	.L146
	cmpl	$23, -4212(%rbp)
	je	.L376
.L146:
	movq	-4192(%rbp), %rcx
	movq	%rcx, -4256(%rbp)
.L149:
	movl	$1, 220(%rax)
	jmp	.L150
.L239:
	andl	$8, %ecx
	je	.L377
.L166:
	movl	132(%r12), %edi
	movl	$0, -4248(%rbp)
	leal	-1(%rdi), %edx
	cmpl	$3, %edx
	jbe	.L165
	cmpl	$1, 1248(%r12)
	je	.L165
	movq	%rax, %rdi
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpl	$2, %eax
	je	.L378
	subl	$6, %eax
	cmpl	$2, %eax
	setb	%al
	movzbl	%al, %eax
	sall	$3, %eax
	movl	%eax, -4248(%rbp)
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L243:
	xorl	%r13d, %r13d
	jmp	.L130
.L155:
	leaq	-4160(%rbp), %rax
	movq	2200(%r12), %rdx
	movq	2184(%r12), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	movq	%rax, -4232(%rbp)
	call	WPACKET_init_static_len@PLT
	testl	%eax, %eax
	je	.L159
	movq	-4232(%rbp), %rdi
	movq	-4192(%rbp), %rsi
	xorl	%edx, %edx
	addq	2208(%r12), %rsi
	call	WPACKET_allocate_bytes@PLT
	testl	%eax, %eax
	jne	.L154
.L159:
	movl	$781, %r9d
.L370:
	leaq	.LC7(%rip), %r8
	movl	$68, %ecx
	movl	$104, %edx
	movq	%r12, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movl	$-1, %eax
	jmp	.L129
.L244:
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	jmp	.L131
.L153:
	movl	$769, %r9d
	jmp	.L370
.L375:
	movq	-4240(%rbp), %r12
	movq	%r13, -4256(%rbp)
	movq	1136(%r12), %rax
	testq	%rax, %rax
	je	.L379
	movq	8(%r12), %rdx
	movq	192(%rdx), %rcx
	movl	96(%rcx), %ecx
	testb	$1, %cl
	jne	.L239
	leaq	-2624(%rbp), %rdi
	movl	$320, %ecx
	xorl	%eax, %eax
	movl	$0, -4248(%rbp)
	movq	%rdi, -4280(%rbp)
	movq	-4232(%rbp), %rbx
	rep stosq
	.p2align 4,,10
	.p2align 3
.L234:
	movq	$0, -4232(%rbp)
	movq	-4280(%rbp), %r13
	xorl	%r15d, %r15d
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L184:
	testq	%r14, %r14
	jne	.L380
.L183:
	movq	-4208(%rbp), %rax
	movq	-4224(%rbp), %rsi
	movq	-4176(%rbp), %xmm0
	movq	(%rax,%r15,8), %rdx
	movq	-4232(%rbp), %rax
	addq	%rax, %rsi
	addq	%rdx, %rax
	movq	%rdx, 8(%r13)
	cmpq	$0, 1120(%r12)
	movq	%rsi, %xmm3
	movq	%rax, -4232(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, 32(%r13)
	je	.L185
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	ssl3_do_compress@PLT
	testl	%eax, %eax
	je	.L187
	movq	8(%r13), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	WPACKET_allocate_bytes@PLT
	testl	%eax, %eax
	je	.L187
.L188:
	movq	8(%r12), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L190
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L190
	cmpl	$65536, %eax
	jne	.L191
.L190:
	movl	132(%r12), %eax
	subl	$1, %eax
	cmpl	$3, %eax
	ja	.L381
.L191:
	cmpq	$0, 1136(%r12)
	je	.L193
	cmpl	$2, 124(%r12)
	jne	.L257
	cmpl	$21, -4212(%rbp)
	je	.L193
.L257:
	movl	-4212(%rbp), %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L382
	addq	$1, 8(%r13)
	movq	%r12, %rdi
	call	ssl_get_max_send_fragment@PLT
	movq	8(%r13), %r8
	movl	%eax, %r14d
	cmpq	%r8, %r14
	jbe	.L193
	movq	6192(%r12), %rax
	testq	%rax, %rax
	je	.L197
	movq	%r8, -4240(%rbp)
	movq	%r8, %rdx
	movl	-4212(%rbp), %esi
	movq	%r12, %rdi
	movq	6200(%r12), %rcx
	call	*%rax
	movq	-4240(%rbp), %r8
.L198:
	testq	%rax, %rax
	jne	.L383
	.p2align 4,,10
	.p2align 3
.L193:
	movq	168(%r12), %rax
	movq	(%rax), %rax
	testb	$4, %ah
	jne	.L202
	movl	-4268(%rbp), %r10d
	testl	%r10d, %r10d
	jne	.L384
.L202:
	xorl	%edx, %edx
	movl	$16, %esi
	movq	%rbx, %rdi
	call	WPACKET_reserve_bytes@PLT
	testl	%eax, %eax
	je	.L206
	leaq	-4184(%rbp), %rsi
	movq	%rbx, %rdi
	call	WPACKET_get_length@PLT
	testl	%eax, %eax
	je	.L206
	movq	%rbx, %rdi
	addq	$1, %r15
	addq	$48, %rbx
	addq	$80, %r13
	call	WPACKET_get_curr@PLT
	movq	-4184(%rbp), %rdx
	subq	%rdx, %rax
	movq	%rdx, -72(%r13)
	movq	%rax, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -48(%r13)
	cmpq	%r15, -4200(%rbp)
	jbe	.L169
	movq	8(%r12), %rdx
.L207:
	movl	(%r12), %r14d
	cmpl	$772, %r14d
	je	.L254
	movl	%r14d, -4240(%rbp)
.L170:
	movq	192(%rdx), %rax
	movq	$0, -4176(%rbp)
	testb	$8, 96(%rax)
	jne	.L171
	movl	(%rdx), %eax
	cmpl	$771, %eax
	jle	.L171
	cmpl	$65536, %eax
	jne	.L172
.L171:
	movl	132(%r12), %eax
	subl	$1, %eax
	cmpl	$3, %eax
	ja	.L385
.L172:
	cmpq	$0, 1136(%r12)
	je	.L174
	cmpl	$2, 124(%r12)
	jne	.L255
	cmpl	$21, -4212(%rbp)
	je	.L174
.L255:
	movl	$23, %eax
	movl	$23, %esi
.L175:
	movl	%eax, 4(%r13)
	movq	%r12, %rdi
	movl	%esi, -4244(%rbp)
	call	SSL_get_state@PLT
	movl	-4244(%rbp), %esi
	cmpl	$12, %eax
	je	.L386
.L176:
	movq	-4208(%rbp), %rax
	movl	%r14d, 0(%r13)
	cmpq	$0, 1120(%r12)
	movq	(%rax,%r15,8), %r14
	je	.L177
	addq	$1024, %r14
.L177:
	movl	$1, %edx
	movq	%rbx, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L180
	movl	-4240(%rbp), %esi
	movl	$2, %edx
	movq	%rbx, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L180
	movl	$2, %esi
	movq	%rbx, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L180
	movl	-4248(%rbp), %r11d
	testl	%r11d, %r11d
	je	.L184
	movslq	-4248(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	WPACKET_allocate_bytes@PLT
	testl	%eax, %eax
	jne	.L184
.L180:
	movl	$873, %r9d
	jmp	.L368
.L377:
	movl	(%rdx), %edx
	cmpl	$65536, %edx
	je	.L166
	movl	$0, -4248(%rbp)
	cmpl	$771, %edx
	jle	.L166
	jmp	.L165
.L379:
	leaq	-2624(%rbp), %rdi
	movl	$320, %ecx
	movl	$0, -4248(%rbp)
	movq	8(%r12), %rdx
	movq	%rdi, -4280(%rbp)
	movq	-4232(%rbp), %rbx
	rep stosq
	jmp	.L234
.L376:
	subq	$8, %rsp
	leaq	-4192(%rbp), %rax
	movq	-4224(%rbp), %rdx
	movl	$1, %r8d
	pushq	%rax
	movq	%r12, %rdi
	leaq	-4168(%rbp), %rcx
	movl	$23, %esi
	movl	$1, %r9d
	movq	$0, -4168(%rbp)
	call	do_ssl3_write
	popq	%rdi
	popq	%r8
	testl	%eax, %eax
	jle	.L369
	movq	-4192(%rbp), %rax
	movl	$745, %r9d
	movq	%rax, -4256(%rbp)
	cmpq	$85, %rax
	ja	.L370
	movq	168(%r12), %rax
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L385:
	cmpl	$1, 1248(%r12)
	je	.L172
.L174:
	movl	-4212(%rbp), %eax
	movl	%eax, %esi
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L254:
	movl	$771, -4240(%rbp)
	movl	$771, %r14d
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L386:
	movl	1928(%r12), %ecx
	testl	%ecx, %ecx
	jne	.L176
	movq	%r12, %rdi
	call	SSL_version@PLT
	movl	-4244(%rbp), %esi
	sarl	$8, %eax
	cmpl	$3, %eax
	jne	.L176
	movq	%r12, %rdi
	call	SSL_version@PLT
	movl	-4244(%rbp), %esi
	cmpl	$769, %eax
	jle	.L176
	movl	1248(%r12), %edx
	movl	$769, %eax
	testl	%edx, %edx
	cmove	%eax, %r14d
	cmovne	-4240(%rbp), %eax
	movl	%eax, -4240(%rbp)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L380:
	leaq	-4176(%rbp), %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	WPACKET_reserve_bytes@PLT
	testl	%eax, %eax
	jne	.L183
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L185:
	movq	%rbx, %rdi
	call	WPACKET_memcpy@PLT
	testl	%eax, %eax
	je	.L387
	movq	32(%r13), %rax
	movq	%rax, 40(%r13)
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L384:
	movslq	-4268(%rbp), %rsi
	leaq	-4168(%rbp), %rdx
	movq	%rbx, %rdi
	call	WPACKET_allocate_bytes@PLT
	testl	%eax, %eax
	je	.L204
	movq	8(%r12), %rax
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-4168(%rbp), %rdx
	movq	192(%rax), %rax
	call	*8(%rax)
	testl	%eax, %eax
	jne	.L202
.L204:
	movl	$967, %r9d
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L381:
	cmpl	$1, 1248(%r12)
	jne	.L193
	jmp	.L191
.L197:
	movq	6208(%r12), %rcx
	testq	%rcx, %rcx
	je	.L193
	leaq	-1(%rcx), %rax
	testq	%rax, %rcx
	jne	.L199
	andq	%r8, %rax
	movq	%rax, %rdx
.L200:
	testq	%rdx, %rdx
	je	.L193
	movq	%rcx, %rax
	subq	%rdx, %rax
	jmp	.L198
.L383:
	subq	%r8, %r14
	movq	%rbx, %rdi
	cmpq	%rax, %r14
	cmova	%rax, %r14
	xorl	%esi, %esi
	movq	%r14, %rdx
	call	WPACKET_memset@PLT
	testl	%eax, %eax
	je	.L388
	addq	%r14, 8(%r13)
	jmp	.L193
.L206:
	movl	$985, %r9d
	jmp	.L368
.L247:
	movq	$1, -4256(%rbp)
	movl	$0, -4248(%rbp)
	jmp	.L165
.L187:
	movl	$893, %r9d
	leaq	.LC7(%rip), %r8
	movl	$141, %ecx
	movq	%r12, %rdi
	movl	$104, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L161
.L374:
	movq	1136(%r12), %rax
	testq	%rax, %rax
	je	.L389
	movq	8(%r12), %rdx
	movq	-4200(%rbp), %rdi
	movq	192(%rdx), %rcx
	movq	%rdi, -4256(%rbp)
	movl	96(%rcx), %ecx
	testb	$1, %cl
	jne	.L239
	movq	%rdi, %rax
	movl	$320, %ecx
	leaq	-2624(%rbp), %rdi
	movq	$0, -4232(%rbp)
	movq	%rdi, -4280(%rbp)
	rep stosq
	movq	%rax, -4256(%rbp)
.L169:
	cmpl	$2, 124(%r12)
	je	.L390
	movq	8(%r12), %rax
	movq	-4200(%rbp), %rdx
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	-4280(%rbp), %rsi
	movq	192(%rax), %rax
	call	*(%rax)
	testl	%eax, %eax
	jle	.L391
.L212:
	cmpq	$0, -4200(%rbp)
	je	.L211
	movslq	-4268(%rbp), %rax
	leaq	-4160(%rbp), %rbx
	xorl	%r14d, %r14d
	leaq	-2616(%rbp), %r13
	leaq	-4176(%rbp), %r15
	movq	%rax, -4208(%rbp)
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L216:
	movq	168(%r12), %rax
	movq	(%rax), %rax
	testb	$4, %ah
	je	.L217
	movl	-4268(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L392
.L217:
	leaq	-4184(%rbp), %rsi
	movq	%rbx, %rdi
	call	WPACKET_get_length@PLT
	testl	%eax, %eax
	je	.L221
	movq	%rbx, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L221
	cmpq	$0, 184(%r12)
	je	.L223
	movq	%rbx, %rdi
	call	WPACKET_get_curr@PLT
	subq	$8, %rsp
	movl	$256, %edx
	xorl	%esi, %esi
	subq	-4184(%rbp), %rax
	pushq	192(%r12)
	movl	$1, %edi
	movq	%r12, %r9
	leaq	-5(%rax), %rcx
	movl	$5, %r8d
	call	*184(%r12)
	movq	8(%r12), %rax
	popq	%rdi
	popq	%r8
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L224
	movl	(%rax), %eax
	cmpl	$65536, %eax
	je	.L224
	cmpl	$771, %eax
	jg	.L225
.L224:
	movl	132(%r12), %eax
	subl	$1, %eax
	cmpl	$3, %eax
	ja	.L393
.L225:
	cmpq	$0, 1136(%r12)
	je	.L223
	subq	$8, %rsp
	movl	(%r12), %esi
	movq	%r12, %r9
	movl	$257, %edx
	movzbl	-4212(%rbp), %eax
	pushq	192(%r12)
	movl	$1, %edi
	leaq	-4168(%rbp), %rcx
	movl	$1, %r8d
	movb	%al, -4168(%rbp)
	call	*184(%r12)
	popq	%rcx
	popq	%rsi
.L223:
	movq	%rbx, %rdi
	call	WPACKET_finish@PLT
	testl	%eax, %eax
	je	.L394
	movl	-4212(%rbp), %eax
	movl	-4216(%rbp), %edx
	movl	%eax, -4(%r13)
	movq	0(%r13), %rax
	addq	$5, %rax
	movq	%rax, 0(%r13)
	testl	%edx, %edx
	jne	.L395
	leaq	(%r14,%r14,4), %rdx
	addq	$48, %rbx
	addq	$1, %r14
	addq	$80, %r13
	addq	-4192(%rbp), %rax
	movq	%rax, 2216(%r12,%rdx,8)
	cmpq	%r14, -4200(%rbp)
	je	.L211
.L231:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	WPACKET_get_length@PLT
	testl	%eax, %eax
	je	.L215
	movq	0(%r13), %rsi
	movq	-4176(%rbp), %rax
	cmpq	%rax, %rsi
	jb	.L215
	jbe	.L216
	subq	%rax, %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	WPACKET_allocate_bytes@PLT
	testl	%eax, %eax
	jne	.L216
.L215:
	movl	$1033, %r9d
	jmp	.L368
.L392:
	movq	-4208(%rbp), %rsi
	leaq	-4168(%rbp), %rdx
	movq	%rbx, %rdi
	call	WPACKET_allocate_bytes@PLT
	testl	%eax, %eax
	je	.L219
	movq	8(%r12), %rax
	movq	-4168(%rbp), %rdx
	leaq	-8(%r13), %rsi
	movq	%r12, %rdi
	movl	$1, %ecx
	movq	192(%rax), %rax
	call	*8(%rax)
	testl	%eax, %eax
	je	.L219
	movq	-4208(%rbp), %rax
	addq	%rax, 0(%r13)
	jmp	.L217
.L199:
	movq	%r8, %rax
	xorl	%edx, %edx
	divq	%rcx
	jmp	.L200
.L393:
	cmpl	$1, 1248(%r12)
	jne	.L223
	jmp	.L225
.L382:
	movl	$913, %r9d
	jmp	.L368
.L387:
	movl	$899, %r9d
	jmp	.L368
.L390:
	movq	-4200(%rbp), %rdx
	movq	-4280(%rbp), %rsi
	movl	$1, %ecx
	movq	%r12, %rdi
	call	tls13_enc@PLT
	testl	%eax, %eax
	jg	.L212
	movq	%r12, %rdi
	call	ossl_statem_in_error@PLT
	testl	%eax, %eax
	jne	.L161
	movl	$1005, %r9d
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L221:
	movl	$1051, %r9d
	jmp	.L368
.L395:
	movl	$1092, %r9d
	testq	%r14, %r14
	jne	.L368
	movq	-4264(%rbp), %rcx
	movq	%rax, (%rcx)
	movl	$1, %eax
	jmp	.L129
.L394:
	movl	$1072, %r9d
	jmp	.L368
.L211:
	movq	-4232(%rbp), %rax
	movl	-4212(%rbp), %esi
	movq	%r12, %rdi
	movq	-4224(%rbp), %rdx
	movq	-4264(%rbp), %r8
	movq	%rax, 6072(%r12)
	movq	%rax, %rcx
	movq	%rdx, 6096(%r12)
	movl	%esi, 6080(%r12)
	movq	%rax, 6088(%r12)
	call	ssl3_write_pending
	jmp	.L129
.L391:
	movq	%r12, %rdi
	call	ossl_statem_in_error@PLT
	movl	$1013, %r9d
	testl	%eax, %eax
	jne	.L161
	jmp	.L368
.L389:
	movq	-4256(%rbp), %rax
	movl	$320, %ecx
	leaq	-2624(%rbp), %rdi
	movq	$0, -4232(%rbp)
	movq	%rdi, -4280(%rbp)
	rep stosq
	jmp	.L169
.L388:
	movl	$947, %r9d
	jmp	.L368
.L378:
	movq	1136(%r12), %rdi
	call	EVP_CIPHER_CTX_iv_length@PLT
	movl	%eax, -4248(%rbp)
	subl	$1, %eax
	jg	.L165
	jmp	.L248
.L219:
	movl	$1042, %r9d
	jmp	.L368
.L253:
	movq	$0, -4232(%rbp)
	jmp	.L169
.L371:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE979:
	.size	do_ssl3_write, .-do_ssl3_write
	.p2align 4
	.globl	ssl3_write_bytes
	.type	ssl3_write_bytes, @function
ssl3_write_bytes:
.LFB978:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$408, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -400(%rbp)
	movq	6040(%rdi), %r14
	movq	%r8, -432(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$1, 40(%rdi)
	cmpq	%rcx, %r14
	ja	.L397
	cmpq	$0, 2216(%rdi)
	movl	%esi, %r13d
	movq	%rcx, %rbx
	jne	.L620
	cmpl	$4, 132(%rdi)
	je	.L465
	movq	$0, 6040(%r15)
.L464:
	cmpl	$-1, 1932(%r15)
	je	.L402
	movl	$1, %esi
	movq	%r15, %rdi
	call	ossl_statem_set_in_init@PLT
.L402:
	movq	%r15, %rdi
	call	SSL_in_init@PLT
	testl	%eax, %eax
	jne	.L403
.L406:
	cmpq	$0, 2216(%r15)
	jne	.L621
	cmpl	$23, %r13d
	je	.L622
.L409:
	cmpq	%rbx, %r14
	je	.L623
.L436:
	movq	%r15, %rdi
	subq	%r14, %rbx
	call	ssl_get_max_send_fragment@PLT
	movq	%r15, %rdi
	movl	%eax, %r12d
	call	ssl_get_split_send_fragment@PLT
	movl	%eax, %eax
	movq	%rax, -424(%rbp)
	movq	1544(%r15), %rax
	movq	%rax, -416(%rbp)
	cmpq	$32, %rax
	ja	.L624
	cmpq	$0, -416(%rbp)
	je	.L441
	movq	1136(%r15), %rdi
	testq	%rdi, %rdi
	je	.L441
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	testl	$8388608, %eax
	je	.L441
	movq	8(%r15), %rax
	movq	192(%rax), %rax
	testb	$1, 96(%rax)
	jne	.L440
	.p2align 4,,10
	.p2align 3
.L441:
	movq	$1, -416(%rbp)
.L440:
	movq	-424(%rbp), %rdi
	testq	%r12, %r12
	sete	%dl
	testq	%rdi, %rdi
	sete	%al
	orb	%al, %dl
	jne	.L487
	cmpq	%rdi, %r12
	jb	.L487
	leaq	-376(%rbp), %rax
	leaq	-336(%rbp), %r10
	movq	%r15, %rdi
	movq	%rax, -408(%rbp)
	movq	%r10, %r15
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L627:
	xorl	%edx, %edx
	leaq	-1(%rbx), %rax
	divq	-424(%rbp)
	leaq	1(%rax), %r8
	movq	-416(%rbp), %rax
	cmpq	%r8, %rax
	cmovbe	%rax, %r8
	movq	%rbx, %rax
	xorl	%edx, %edx
	divq	%r8
	movq	%rax, %rsi
	cmpq	%r12, %rax
	jb	.L445
	cmpq	$1, %r8
	je	.L471
	movq	%r8, %rax
	movq	%r12, %xmm0
	punpcklqdq	%xmm0, %xmm0
	shrq	%rax
	movaps	%xmm0, -336(%rbp)
	cmpq	$1, %rax
	je	.L447
	movaps	%xmm0, -320(%rbp)
	cmpq	$2, %rax
	je	.L447
	movaps	%xmm0, -304(%rbp)
	cmpq	$3, %rax
	je	.L447
	movaps	%xmm0, -288(%rbp)
	cmpq	$4, %rax
	je	.L447
	movaps	%xmm0, -272(%rbp)
	cmpq	$5, %rax
	je	.L447
	movaps	%xmm0, -256(%rbp)
	cmpq	$6, %rax
	je	.L447
	movaps	%xmm0, -240(%rbp)
	cmpq	$7, %rax
	je	.L447
	movaps	%xmm0, -224(%rbp)
	cmpq	$8, %rax
	je	.L447
	movaps	%xmm0, -208(%rbp)
	cmpq	$9, %rax
	je	.L447
	movaps	%xmm0, -192(%rbp)
	cmpq	$10, %rax
	je	.L447
	movaps	%xmm0, -176(%rbp)
	cmpq	$11, %rax
	je	.L447
	movaps	%xmm0, -160(%rbp)
	cmpq	$12, %rax
	je	.L447
	movaps	%xmm0, -144(%rbp)
	cmpq	$13, %rax
	je	.L447
	movaps	%xmm0, -128(%rbp)
	cmpq	$14, %rax
	je	.L447
	movaps	%xmm0, -112(%rbp)
	cmpq	$16, %rax
	jne	.L447
	movaps	%xmm0, -96(%rbp)
	.p2align 4,,10
	.p2align 3
.L452:
	subq	$8, %rsp
	pushq	-408(%rbp)
	xorl	%r9d, %r9d
	movq	%r15, %rcx
	movq	-400(%rbp), %rax
	movl	%r13d, %esi
	movq	%rdi, -392(%rbp)
	leaq	(%rax,%r14), %rdx
	call	do_ssl3_write
	popq	%rdx
	movq	-392(%rbp), %rdi
	testl	%eax, %eax
	popq	%rcx
	jle	.L625
	movq	-376(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L459
	cmpl	$23, %r13d
	je	.L626
.L460:
	subq	%rax, %rbx
	addq	%rax, %r14
.L442:
	testq	%rbx, %rbx
	jne	.L627
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	movl	$1, %r8d
.L444:
	movq	%rsi, -336(%rbp,%rcx,8)
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L620:
	movq	6072(%rdi), %rax
	addq	%r14, %rax
	cmpq	%rcx, %rax
	ja	.L397
	cmpl	$4, 132(%rdi)
	je	.L465
	movq	$0, 6040(%r15)
	movq	%r15, %rdi
	call	SSL_in_init@PLT
	testl	%eax, %eax
	je	.L406
.L403:
	movq	%r15, %rdi
	call	ossl_statem_get_in_handshake@PLT
	testl	%eax, %eax
	jne	.L406
	cmpl	$6, 132(%r15)
	je	.L406
	movq	%r15, %rdi
	call	*48(%r15)
	movl	%eax, %r8d
	testl	%eax, %eax
	js	.L396
	jne	.L406
.L616:
	movl	$-1, %r8d
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L447:
	movq	%r8, %rax
	andq	$-2, %rax
	testb	$1, %r8b
	je	.L452
.L446:
	movq	%r12, -336(%rbp,%rax,8)
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L626:
	testb	$1, 1496(%rdi)
	je	.L460
	movq	168(%rdi), %rdx
	movq	%rax, %rbx
	movl	$0, 220(%rdx)
.L462:
	movq	-432(%rbp), %rax
	addq	%r14, %rbx
	movl	$1, %r8d
	movq	%rbx, (%rax)
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L471:
	xorl	%eax, %eax
	jmp	.L446
.L621:
	movq	-400(%rbp), %rax
	movq	6072(%r15), %rcx
	movl	%r13d, %esi
	movq	%r15, %rdi
	leaq	-376(%rbp), %r8
	leaq	(%rax,%r14), %rdx
	call	ssl3_write_pending
	movl	%eax, %r8d
	testl	%eax, %eax
	jle	.L618
	addq	-376(%rbp), %r14
	cmpl	$23, %r13d
	jne	.L409
.L622:
	movq	%r15, %rdi
	call	ssl_get_max_send_fragment@PLT
	movl	%eax, %r8d
	leaq	0(,%r8,4), %rax
	movq	%r8, %r12
	cmpq	%rbx, %rax
	ja	.L409
	cmpq	$0, 1120(%r15)
	jne	.L409
	cmpq	$0, 184(%r15)
	jne	.L409
	movq	168(%r15), %rax
	movq	(%rax), %rax
	testb	$4, %ah
	jne	.L409
	movq	8(%r15), %rax
	movq	192(%rax), %rax
	testb	$1, 96(%rax)
	je	.L409
	movq	1136(%r15), %rdi
	movq	%r8, -392(%rbp)
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	testl	$4194304, %eax
	je	.L409
	andl	$4095, %r12d
	movq	-392(%rbp), %r8
	jne	.L410
	subq	$512, %r8
.L410:
	testq	%r14, %r14
	je	.L411
	cmpq	$0, 2184(%r15)
	je	.L411
	cmpq	%rbx, %r14
	je	.L619
.L416:
	movq	%rbx, %r12
	leaq	0(,%r8,4), %rax
	leaq	0(,%r8,8), %rdi
	subq	%r14, %r12
	movq	%rax, -392(%rbp)
	movq	%rdi, -416(%rbp)
	cmpq	%r12, %rax
	ja	.L614
	movl	%r13d, -408(%rbp)
	jmp	.L435
.L425:
	movq	%r8, 2216(%r15)
	movq	%r13, %rcx
	movq	%r10, %rdx
	movq	%r15, %rdi
	movq	$0, 2208(%r15)
	movl	$23, %esi
	leaq	-376(%rbp), %r8
	movq	%r13, 6072(%r15)
	movq	%r10, 6096(%r15)
	movl	$23, 6080(%r15)
	movq	%r13, 6088(%r15)
	call	ssl3_write_pending
	movl	%eax, %r8d
	testl	%eax, %eax
	jle	.L628
	movq	-376(%rbp), %rax
	cmpq	%r12, %rax
	je	.L629
	subq	%rax, %r12
	addq	%rax, %r14
	cmpq	%r12, -392(%rbp)
	ja	.L422
.L435:
	movq	168(%r15), %rax
	movl	252(%rax), %edi
	testl	%edi, %edi
	je	.L420
	movq	8(%r15), %rax
	movq	%r15, %rdi
	call	*120(%rax)
	movl	%eax, %r8d
	testl	%eax, %eax
	jle	.L618
.L420:
	movq	-416(%rbp), %r13
	movl	$8, %eax
	cmpq	%r12, %r13
	jbe	.L421
	movq	-392(%rbp), %r13
	movl	$4, %eax
.L421:
	movl	%eax, -344(%rbp)
	movq	6112(%r15), %rax
	xorl	%esi, %esi
	leaq	-368(%rbp), %rcx
	movq	1136(%r15), %rdi
	movw	%si, -58(%rbp)
	movl	$32, %edx
	movl	$25, %esi
	movq	%rax, -69(%rbp)
	movzwl	(%r15), %eax
	movb	$23, -61(%rbp)
	rolw	$8, %ax
	movq	%r13, -352(%rbp)
	movw	%ax, -60(%rbp)
	leaq	-69(%rbp), %rax
	movq	$0, -368(%rbp)
	movq	%rax, -360(%rbp)
	movq	%rcx, -424(%rbp)
	call	EVP_CIPHER_CTX_ctrl@PLT
	movslq	%eax, %r8
	testl	%eax, %eax
	jle	.L422
	cmpq	%r8, 2200(%r15)
	movq	-424(%rbp), %rcx
	jb	.L422
	movq	2184(%r15), %rax
	movl	$32, %edx
	movq	1136(%r15), %rdi
	movl	$26, %esi
	movq	%r8, -440(%rbp)
	movq	%rax, -368(%rbp)
	movq	-400(%rbp), %rax
	movq	%r13, -352(%rbp)
	leaq	(%rax,%r14), %r10
	movq	%r10, -360(%rbp)
	movq	%r10, -424(%rbp)
	call	EVP_CIPHER_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L616
	movl	-344(%rbp), %edx
	movzbl	6119(%r15), %eax
	movq	-424(%rbp), %r10
	movq	-440(%rbp), %r8
	addl	%edx, %eax
	movb	%al, 6119(%r15)
	movzbl	%al, %eax
	cmpl	%eax, %edx
	jbe	.L425
	addb	$1, 6118(%r15)
	jne	.L425
	addb	$1, 6117(%r15)
	jne	.L425
	addb	$1, 6116(%r15)
	jne	.L425
	addb	$1, 6115(%r15)
	jne	.L425
	addb	$1, 6114(%r15)
	jne	.L425
	addb	$1, 6113(%r15)
	jne	.L425
	addb	$1, 6112(%r15)
	jmp	.L425
.L628:
	je	.L618
	movq	24(%r15), %rdi
	testq	%rdi, %rdi
	je	.L431
	movl	$8, %esi
	movl	%eax, -392(%rbp)
	call	BIO_test_flags@PLT
	movl	-392(%rbp), %r8d
	testl	%eax, %eax
	jne	.L618
.L431:
	movq	%r15, %rdi
	movl	%r8d, -392(%rbp)
	call	ssl3_release_write_buffer@PLT
	movl	-392(%rbp), %r8d
	.p2align 4,,10
	.p2align 3
.L618:
	movq	%r14, 6040(%r15)
.L396:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L630
	leaq	-40(%rbp), %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L625:
	.cfi_restore_state
	movl	%eax, %r8d
	movq	%rdi, %r15
	jmp	.L618
.L465:
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	early_data_count_ok@PLT
	testl	%eax, %eax
	je	.L616
	cmpq	$0, 2216(%r15)
	movq	$0, 6040(%r15)
	jne	.L402
	jmp	.L464
.L623:
	testb	$16, 1496(%r15)
	je	.L437
	movq	8(%r15), %rax
	movq	192(%rax), %rax
	testb	$8, 96(%rax)
	je	.L619
.L437:
	movq	-432(%rbp), %rax
	movl	$1, %r8d
	movq	%r14, (%rax)
	jmp	.L396
.L459:
	movq	168(%rdi), %rax
	movq	%rdi, %r15
	movl	$0, 220(%rax)
	testb	$16, 1496(%rdi)
	je	.L462
	movq	8(%r15), %rax
	movq	192(%rax), %rax
	testb	$8, 96(%rax)
	jne	.L462
	movq	%r15, %rdi
	call	ssl3_release_write_buffer@PLT
	movq	-376(%rbp), %rbx
	jmp	.L462
.L619:
	movq	%r15, %rdi
	call	ssl3_release_write_buffer@PLT
	jmp	.L437
.L397:
	movl	$363, %r9d
	leaq	.LC7(%rip), %r8
	movl	$271, %ecx
.L615:
	movl	$158, %edx
	movl	$80, %esi
	movq	%r15, %rdi
	call	ossl_statem_fatal@PLT
	jmp	.L616
.L487:
	movl	$585, %r9d
.L617:
	leaq	.LC7(%rip), %r8
	movl	$68, %ecx
	jmp	.L615
.L624:
	movl	$569, %r9d
	jmp	.L617
.L445:
	movq	%rdx, %rcx
	testq	%rdx, %rdx
	jne	.L631
	.p2align 4,,10
	.p2align 3
.L449:
	leaq	1(%rcx), %r10
	movq	%r8, %r9
	movl	$1, %edx
	subq	%rcx, %r9
	cmpq	%r8, %r10
	cmovbe	%r9, %rdx
	ja	.L444
	cmpq	$1, %r9
	je	.L444
	movq	%rax, %xmm0
	movq	%rdx, %r9
	leaq	(%r15,%rcx,8), %rax
	punpcklqdq	%xmm0, %xmm0
	shrq	%r9
	movups	%xmm0, (%rax)
	cmpq	$1, %r9
	je	.L454
	movups	%xmm0, 16(%rax)
	cmpq	$2, %r9
	je	.L454
	movups	%xmm0, 32(%rax)
	cmpq	$3, %r9
	je	.L454
	movups	%xmm0, 48(%rax)
	cmpq	$4, %r9
	je	.L454
	movups	%xmm0, 64(%rax)
	cmpq	$5, %r9
	je	.L454
	movups	%xmm0, 80(%rax)
	cmpq	$6, %r9
	je	.L454
	movups	%xmm0, 96(%rax)
	cmpq	$7, %r9
	je	.L454
	movups	%xmm0, 112(%rax)
	cmpq	$8, %r9
	je	.L454
	movups	%xmm0, 128(%rax)
	cmpq	$9, %r9
	je	.L454
	movups	%xmm0, 144(%rax)
	cmpq	$10, %r9
	je	.L454
	movups	%xmm0, 160(%rax)
	cmpq	$11, %r9
	je	.L454
	movups	%xmm0, 176(%rax)
	cmpq	$12, %r9
	je	.L454
	movups	%xmm0, 192(%rax)
	cmpq	$13, %r9
	je	.L454
	movups	%xmm0, 208(%rax)
	cmpq	$14, %r9
	je	.L454
	movups	%xmm0, 224(%rax)
	cmpq	$15, %r9
	je	.L454
	movups	%xmm0, 240(%rax)
	.p2align 4,,10
	.p2align 3
.L454:
	movq	%rdx, %rax
	andq	$-2, %rax
	addq	%rax, %rcx
	cmpq	%rax, %rdx
	jne	.L444
	jmp	.L452
.L631:
	cmpq	%rdx, %r8
	movl	$1, %r9d
	cmovbe	%r8, %rdx
	testq	%rdx, %rdx
	cmovne	%rdx, %r9
	cmpq	$3, %rdx
	jbe	.L472
	leaq	1(%rax), %r10
	movq	%r9, %rcx
	movq	%r10, %xmm0
	shrq	%rcx
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -336(%rbp)
	cmpq	$1, %rcx
	je	.L473
	movaps	%xmm0, -320(%rbp)
	cmpq	$2, %rcx
	je	.L474
	movaps	%xmm0, -304(%rbp)
	cmpq	$3, %rcx
	je	.L475
	movaps	%xmm0, -288(%rbp)
	cmpq	$4, %rcx
	je	.L476
	movaps	%xmm0, -272(%rbp)
	cmpq	$5, %rcx
	je	.L477
	movaps	%xmm0, -256(%rbp)
	cmpq	$6, %rcx
	je	.L478
	movaps	%xmm0, -240(%rbp)
	cmpq	$7, %rcx
	je	.L479
	movaps	%xmm0, -224(%rbp)
	cmpq	$8, %rcx
	je	.L480
	movaps	%xmm0, -208(%rbp)
	cmpq	$9, %rcx
	je	.L481
	movaps	%xmm0, -192(%rbp)
	cmpq	$10, %rcx
	je	.L482
	movaps	%xmm0, -176(%rbp)
	cmpq	$11, %rcx
	je	.L483
	movaps	%xmm0, -160(%rbp)
	cmpq	$12, %rcx
	je	.L484
	movaps	%xmm0, -144(%rbp)
	cmpq	$13, %rcx
	je	.L485
	movaps	%xmm0, -128(%rbp)
	cmpq	$15, %rcx
	jne	.L486
	movaps	%xmm0, -112(%rbp)
	movdqa	.LC8(%rip), %xmm0
	.p2align 4,,10
	.p2align 3
.L451:
	movq	%r9, %r11
	movhlps	%xmm0, %xmm1
	andq	$-2, %r11
	andl	$1, %r9d
	movq	%xmm1, %rcx
	je	.L453
.L450:
	leaq	1(%r11), %rcx
	movq	%r10, -336(%rbp,%r11,8)
	cmpq	%rcx, %rdx
	jbe	.L453
	movq	%r10, -336(%rbp,%rcx,8)
	leaq	2(%r11), %rcx
	cmpq	%rcx, %rdx
	jbe	.L453
	movq	%r10, -336(%rbp,%rcx,8)
	leaq	3(%r11), %rcx
.L453:
	cmpq	%rcx, %r8
	ja	.L449
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L473:
	movdqa	.LC11(%rip), %xmm0
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L474:
	movdqa	.LC12(%rip), %xmm0
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L475:
	movdqa	.LC13(%rip), %xmm0
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L472:
	xorl	%r11d, %r11d
	leaq	1(%rax), %r10
	jmp	.L450
.L476:
	movdqa	.LC14(%rip), %xmm0
	jmp	.L451
.L477:
	movdqa	.LC15(%rip), %xmm0
	jmp	.L451
.L478:
	movdqa	.LC16(%rip), %xmm0
	jmp	.L451
.L479:
	movdqa	.LC17(%rip), %xmm0
	jmp	.L451
.L480:
	movdqa	.LC18(%rip), %xmm0
	jmp	.L451
.L481:
	movdqa	.LC19(%rip), %xmm0
	jmp	.L451
.L482:
	movdqa	.LC20(%rip), %xmm0
	jmp	.L451
.L483:
	movdqa	.LC21(%rip), %xmm0
	jmp	.L451
.L484:
	movdqa	.LC9(%rip), %xmm0
	jmp	.L451
.L485:
	movdqa	.LC10(%rip), %xmm0
	jmp	.L451
.L486:
	movdqa	.LC22(%rip), %xmm0
	jmp	.L451
.L630:
	call	__stack_chk_fail@PLT
.L422:
	movl	-408(%rbp), %r13d
.L614:
	movq	%r15, %rdi
	call	ssl3_release_write_buffer@PLT
	jmp	.L436
.L411:
	movq	%r15, %rdi
	movq	%r8, -392(%rbp)
	call	ssl3_release_write_buffer@PLT
	movq	-392(%rbp), %r8
	xorl	%ecx, %ecx
	movq	1136(%r15), %rdi
	movl	$28, %esi
	movl	%r8d, %edx
	call	EVP_CIPHER_CTX_ctrl@PLT
	movq	-392(%rbp), %r8
	movslq	%eax, %rdx
	leaq	0(,%r8,8), %rax
	cmpq	%rbx, %rax
	ja	.L413
	salq	$3, %rdx
.L414:
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%r8, -392(%rbp)
	call	ssl3_setup_write_buffer@PLT
	movq	-392(%rbp), %r8
	testl	%eax, %eax
	jne	.L416
	jmp	.L616
.L629:
	movq	%r15, %rdi
	call	ssl3_release_write_buffer@PLT
	movq	-432(%rbp), %rax
	addq	-376(%rbp), %r14
	movl	$1, %r8d
	movq	%r14, (%rax)
	jmp	.L396
.L413:
	salq	$2, %rdx
	jmp	.L414
	.cfi_endproc
.LFE978:
	.size	ssl3_write_bytes, .-ssl3_write_bytes
	.section	.rodata.str1.1
.LC23:
	.string	"%d"
.LC24:
	.string	"SSL alert number "
	.text
	.p2align 4
	.globl	ssl3_read_bytes
	.type	ssl3_read_bytes, @function
ssl3_read_bytes:
.LFB981:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	%rdx, -112(%rbp)
	movl	%esi, -84(%rbp)
	movq	%r8, -96(%rbp)
	movl	%r9d, -88(%rbp)
	movq	%rax, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L731
	movl	(%rax), %eax
	cmpl	$65536, %eax
	je	.L731
	cmpl	$771, %eax
	jg	.L872
.L731:
	movl	$0, -116(%rbp)
	movl	$1, %ebx
.L633:
	cmpq	$0, 2144(%r12)
	je	.L634
.L638:
	movl	-84(%rbp), %esi
	cmpl	$23, %esi
	leal	-22(%rsi), %edx
	setne	%al
	cmpl	$1, %edx
	jbe	.L637
	testl	%esi, %esi
	jne	.L635
.L637:
	movl	-88(%rbp), %r14d
	testl	%r14d, %r14d
	setne	%dl
	testb	%al, %dl
	jne	.L635
	cmpl	$22, -84(%rbp)
	jne	.L641
	movq	6056(%r12), %rax
	testq	%rax, %rax
	jne	.L873
.L641:
	movq	%r12, %rdi
	call	ossl_statem_get_in_handshake@PLT
	testl	%eax, %eax
	je	.L647
.L649:
	cmpl	$22, -84(%rbp)
	movq	2128(%r12), %r14
	movq	$0, -128(%rbp)
	sete	%al
	cmpq	$0, -112(%rbp)
	setne	%dl
	movq	%r14, %r15
	andl	%edx, %eax
	andl	%ebx, %eax
	movb	%al, -117(%rbp)
.L648:
	movl	$1, 40(%r12)
	testq	%r15, %r15
	je	.L717
	.p2align 4,,10
	.p2align 3
.L651:
	leaq	3464(%r12), %rbx
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L652:
	movl	56(%rbx), %r11d
	movq	%rbx, %rcx
	testl	%r11d, %r11d
	je	.L874
	addq	$1, %r14
	addq	$80, %rbx
	cmpq	%r14, %r15
	jne	.L652
	movq	$0, 2128(%r12)
.L717:
	movq	%r12, %rdi
	call	ssl3_get_record@PLT
	movl	%eax, %r10d
	testl	%eax, %eax
	jle	.L632
	movq	2128(%r12), %r15
	testq	%r15, %r15
	jne	.L651
	movl	$1309, %r9d
.L868:
	leaq	.LC7(%rip), %r8
	movl	$68, %ecx
	movl	$148, %edx
	movq	%r12, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
.L869:
	movl	$-1, %r10d
.L632:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L875
	leaq	-40(%rbp), %rsp
	movl	%r10d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L874:
	.cfi_restore_state
	movq	6056(%r12), %rdx
	movl	4(%rbx), %eax
	testq	%rdx, %rdx
	jne	.L876
.L655:
	movq	168(%r12), %rsi
	movl	240(%rsi), %esi
	cmpl	$21, %eax
	je	.L657
.L725:
	cmpq	$0, 8(%rbx)
	jne	.L877
.L658:
	testl	%esi, %esi
	je	.L659
	cmpl	$22, %eax
	jne	.L724
.L659:
	movl	68(%r12), %esi
	movl	%esi, %r10d
	andl	$2, %r10d
	jne	.L878
	cmpl	%eax, -84(%rbp)
	je	.L661
	cmpl	$20, %eax
	je	.L879
.L662:
	movl	(%rbx), %ecx
	cmpl	$2, %ecx
	je	.L880
	movq	8(%r12), %r9
	movl	(%r9), %edi
	cmpl	$65536, %edi
	je	.L881
	cmpl	$21, %eax
	je	.L680
	andl	$1, %esi
	jne	.L882
	cmpl	$22, %eax
	je	.L700
	cmpl	$20, %eax
	je	.L727
	cmpq	$3, %rdx
	ja	.L728
.L706:
	cmpl	$22, %eax
	jg	.L708
	cmpl	$19, %eax
	jg	.L883
.L710:
	movl	$1703, %r9d
.L867:
	leaq	.LC7(%rip), %r8
	movl	$245, %ecx
	jmp	.L865
.L876:
	cmpl	$22, %eax
	je	.L656
	movq	8(%r12), %rsi
	movq	192(%rsi), %rdi
	testb	$8, 96(%rdi)
	jne	.L655
	movl	(%rsi), %esi
	cmpl	$65536, %esi
	je	.L655
	cmpl	$771, %esi
	jle	.L655
	movl	$1329, %r9d
	leaq	.LC7(%rip), %r8
	movl	$293, %ecx
	jmp	.L865
	.p2align 4,,10
	.p2align 3
.L647:
	movq	%r12, %rdi
	call	SSL_in_init@PLT
	testl	%eax, %eax
	je	.L649
	movq	%r12, %rdi
	call	*48(%r12)
	movl	%eax, %r10d
	testl	%eax, %eax
	js	.L632
	jne	.L649
	jmp	.L869
	.p2align 4,,10
	.p2align 3
.L877:
	movl	$0, 6124(%r12)
	jmp	.L658
.L873:
	movq	-96(%rbp), %rsi
	leaq	6048(%r12), %rcx
	xorl	%edx, %edx
	testq	%rsi, %rsi
	je	.L642
.L645:
	movzbl	(%rcx), %eax
	addq	$1, %rcx
	movb	%al, 0(%r13,%rdx)
	movq	6056(%r12), %rax
	addq	$1, %rdx
	subq	$1, %rax
	movq	%rax, 6056(%r12)
	cmpq	%rdx, %rsi
	je	.L884
	testq	%rax, %rax
	jne	.L645
.L644:
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L646
	movl	$22, (%rax)
.L646:
	movq	-136(%rbp), %rax
	movl	$1, %r10d
	movq	%rdx, (%rax)
	jmp	.L632
.L882:
	cmpl	$22, %eax
	jne	.L699
	movq	192(%r9), %rax
	testb	$8, 96(%rax)
	jne	.L738
	cmpl	$771, %edi
	jg	.L700
.L738:
	movq	$0, 8(%rbx)
	movl	$1, 56(%rbx)
	testb	$4, 1496(%r12)
	jne	.L648
	jmp	.L853
	.p2align 4,,10
	.p2align 3
.L872:
	movl	$1, -116(%rbp)
	xorl	%ebx, %ebx
	jmp	.L633
.L656:
	movq	168(%r12), %rsi
	movl	240(%rsi), %esi
	jmp	.L725
.L657:
	testl	%esi, %esi
	je	.L659
.L724:
	movl	$1347, %r9d
	leaq	.LC7(%rip), %r8
	movl	$145, %ecx
	jmp	.L865
	.p2align 4,,10
	.p2align 3
.L634:
	movq	%r12, %rdi
	call	ssl3_setup_read_buffer@PLT
	testl	%eax, %eax
	jne	.L638
	jmp	.L869
	.p2align 4,,10
	.p2align 3
.L879:
	cmpb	$0, -117(%rbp)
	je	.L662
	movq	%r12, %rdi
	movl	%r10d, -144(%rbp)
	movq	%rcx, -104(%rbp)
	call	SSL_in_init@PLT
	cmpl	$20, 4(%rbx)
	movq	-104(%rbp), %rcx
	movb	$0, -104(%rbp)
	movl	-144(%rbp), %r10d
	jne	.L719
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L881:
	movl	56(%r12), %edi
	testl	%edi, %edi
	jne	.L737
	cmpl	$21, %eax
	jne	.L737
.L680:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	jle	.L683
	subq	$1, %rax
	je	.L683
	movq	24(%rbx), %rcx
	addq	32(%rbx), %rcx
	movzwl	(%rcx), %edi
	movzbl	1(%rcx), %r14d
	movw	%di, -144(%rbp)
	movl	%r14d, -104(%rbp)
	cmpq	$1, %rax
	jne	.L683
	movq	184(%r12), %rax
	movzbl	(%rcx), %r15d
	testq	%rax, %rax
	je	.L685
	subq	$8, %rsp
	movl	%r10d, -148(%rbp)
	movq	%r12, %r9
	xorl	%edi, %edi
	movl	(%r12), %esi
	movl	$2, %r8d
	movl	$21, %edx
	pushq	192(%r12)
	call	*%rax
	movl	-148(%rbp), %r10d
	popq	%rcx
	popq	%rsi
.L685:
	movzbl	%r15b, %eax
	movq	1392(%r12), %r15
	testq	%r15, %r15
	je	.L885
.L686:
	movzwl	-144(%rbp), %edx
	movl	%eax, -152(%rbp)
	movl	$16388, %esi
	movq	%r12, %rdi
	movl	%r10d, -148(%rbp)
	rolw	$8, %dx
	movzwl	%dx, %edx
	call	*%r15
	movq	%r15, -128(%rbp)
	movl	-152(%rbp), %eax
	movl	-148(%rbp), %r10d
.L687:
	cmpl	$1, %eax
	je	.L688
	movl	-116(%rbp), %edx
	testl	%edx, %edx
	je	.L689
	cmpl	$90, %r14d
	je	.L886
.L691:
	movl	%r14d, %r15d
	testl	%r14d, %r14d
	jne	.L720
.L722:
	orl	$2, 68(%r12)
	jmp	.L632
	.p2align 4,,10
	.p2align 3
.L683:
	movl	$1496, %r9d
	leaq	.LC7(%rip), %r8
	movl	$205, %ecx
.L865:
	movl	$148, %edx
	movl	$10, %esi
	movq	%r12, %rdi
	call	ossl_statem_fatal@PLT
	movl	$-1, %r10d
	jmp	.L632
.L708:
	cmpl	$23, %eax
	jne	.L710
	movq	%r12, %rdi
	call	ossl_statem_app_data_allowed@PLT
	testl	%eax, %eax
	jne	.L887
	movq	%r12, %rdi
	call	ossl_statem_skip_early_data@PLT
	testl	%eax, %eax
	je	.L713
	movq	8(%rbx), %rsi
	xorl	%ecx, %ecx
	movl	$104, %edx
	movq	%r12, %rdi
	call	early_data_count_ok@PLT
	testl	%eax, %eax
	je	.L869
	movl	$1, 56(%rbx)
	jmp	.L863
.L884:
	testq	%rax, %rax
	je	.L644
.L642:
	movzbl	(%rcx), %esi
	movb	%sil, 6048(%r12)
	cmpq	$1, %rax
	jbe	.L644
	movzbl	1(%rcx), %esi
	movb	%sil, 6049(%r12)
	cmpq	$2, %rax
	je	.L644
	movzbl	2(%rcx), %esi
	movb	%sil, 6050(%r12)
	cmpq	$3, %rax
	je	.L644
	movzbl	3(%rcx), %eax
	movb	%al, 6051(%r12)
	jmp	.L644
.L661:
	movq	%r12, %rdi
	movl	%r10d, -148(%rbp)
	movq	%rcx, -144(%rbp)
	call	SSL_in_init@PLT
	cmpl	$23, -84(%rbp)
	movq	-144(%rbp), %rcx
	sete	-104(%rbp)
	testl	%eax, %eax
	movzbl	-104(%rbp), %edx
	movl	-148(%rbp), %r10d
	setne	%al
	andb	%dl, %al
	jne	.L888
	cmpl	$22, -84(%rbp)
	jne	.L665
	cmpl	$20, 4(%rbx)
	jne	.L665
.L718:
	cmpq	$0, 6056(%r12)
	je	.L665
	movl	$1385, %r9d
	leaq	.LC7(%rip), %r8
	movl	$133, %ecx
	jmp	.L865
	.p2align 4,,10
	.p2align 3
.L888:
	cmpq	$0, 1088(%r12)
	je	.L889
	movb	%al, -104(%rbp)
.L665:
	cmpq	$0, -112(%rbp)
	je	.L666
.L719:
	movl	4(%rbx), %eax
	movq	-112(%rbp), %rdx
	movl	%eax, (%rdx)
.L666:
	cmpq	$0, -96(%rbp)
	je	.L890
	movl	-88(%rbp), %r10d
	xorl	%r9d, %r9d
	testl	%r10d, %r10d
	jne	.L734
	movq	%r13, %rdi
	movq	%r12, -144(%rbp)
	movq	%r14, %r13
	movq	%rcx, %rbx
	movq	%r9, %r14
	jmp	.L673
.L891:
	cmpq	%r13, %r15
	jbe	.L857
.L673:
	movq	-96(%rbp), %r12
	movq	24(%rbx), %rsi
	subq	%r14, %r12
	cmpq	%r12, 8(%rbx)
	cmovbe	8(%rbx), %r12
	addq	32(%rbx), %rsi
	movq	%r12, %rdx
	call	memcpy@PLT
	addq	%r12, 24(%rbx)
	movq	%rax, %rdi
	movq	8(%rbx), %rax
	addq	%r12, %rdi
	subq	%r12, %rax
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	jne	.L669
	movq	-144(%rbp), %rax
	addq	$1, %r13
	addq	$80, %rbx
	movl	$240, 2124(%rax)
	movq	$0, -56(%rbx)
	movl	$1, -24(%rbx)
.L669:
	addq	%r12, %r14
	cmpq	%r14, -96(%rbp)
	seta	%al
	testb	%al, -104(%rbp)
	jne	.L891
.L857:
	movq	-144(%rbp), %r12
	movq	%r14, %r9
	movq	%r13, %r14
	movq	%rdi, %r13
.L672:
	testq	%r9, %r9
	jne	.L848
.L863:
	movq	2128(%r12), %r15
	jmp	.L648
.L734:
	movq	%r12, -144(%rbp)
	movq	%r9, %rbx
	movq	%rcx, %r12
	movq	%r13, %rdi
	jmp	.L668
.L892:
	movl	$1, 56(%r12)
.L714:
	addq	$1, %r14
	addq	$80, %r12
.L715:
	addq	%r13, %rbx
	cmpq	%rbx, -96(%rbp)
	seta	%al
	testb	%al, -104(%rbp)
	je	.L858
	cmpq	%r15, %r14
	jnb	.L858
.L668:
	movq	-96(%rbp), %r13
	movq	8(%r12), %rax
	movq	24(%r12), %rsi
	subq	%rbx, %r13
	cmpq	%rax, %r13
	cmova	%rax, %r13
	addq	32(%r12), %rsi
	movq	%r13, %rdx
	call	memcpy@PLT
	movq	%rax, %rdi
	movq	8(%r12), %rax
	addq	%r13, %rdi
	testq	%rax, %rax
	je	.L892
	cmpq	%r13, %rax
	jne	.L715
	jmp	.L714
.L858:
	movq	-144(%rbp), %r12
	movq	%rbx, %r9
	movq	%rdi, %r13
	jmp	.L672
.L883:
	movl	$1714, %r9d
	leaq	.LC7(%rip), %r8
	movl	$68, %ecx
	jmp	.L865
.L728:
	movq	%r12, %rdi
	call	ossl_statem_get_in_handshake@PLT
	testl	%eax, %eax
	je	.L707
	movl	4(%rbx), %eax
	jmp	.L706
.L635:
	movl	$1241, %r9d
	jmp	.L868
.L878:
	movq	$0, 8(%rbx)
	xorl	%r10d, %r10d
	movl	$1, 40(%r12)
	jmp	.L632
.L707:
	movl	$1, %esi
	movq	%r12, %rdi
	movl	132(%r12), %ebx
	call	ossl_statem_set_in_init@PLT
	movq	%r12, %rdi
	call	*48(%r12)
	movl	%eax, %r10d
	testl	%eax, %eax
	js	.L632
	je	.L869
	cmpl	$11, %ebx
	je	.L869
	testb	$4, 1496(%r12)
	jne	.L863
	cmpq	$0, 2176(%r12)
	jne	.L863
.L853:
	movl	$3, 40(%r12)
	movq	%r12, %rdi
	call	SSL_get_rbio@PLT
	movl	$15, %esi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	BIO_clear_flags@PLT
	movl	$9, %esi
	movq	%r12, %rdi
	call	BIO_set_flags@PLT
	movl	$-1, %r10d
	jmp	.L632
	.p2align 4,,10
	.p2align 3
.L700:
	movl	$4, %r14d
	movq	24(%rbx), %rsi
	leaq	6048(%r12,%rdx), %rdi
	subq	%rdx, %r14
	cmpq	%r14, 8(%rbx)
	cmovbe	8(%rbx), %r14
	addq	32(%rbx), %rsi
	movq	%r14, %rdx
	call	memcpy@PLT
	addq	%r14, 24(%rbx)
	subq	%r14, 8(%rbx)
	addq	6056(%r12), %r14
	movq	%r14, 6056(%r12)
	cmpq	$0, 8(%rbx)
	jne	.L703
	movl	$1, 56(%rbx)
.L703:
	cmpq	$3, %r14
	jbe	.L863
	cmpl	$20, 4(%rbx)
	jne	.L728
.L727:
	movl	$1642, %r9d
	leaq	.LC7(%rip), %r8
	movl	$133, %ecx
	jmp	.L865
.L720:
	movq	168(%r12), %rax
	movl	$1543, %r9d
	movq	%r12, %rdi
	movl	$1, 40(%r12)
	leaq	.LC7(%rip), %r8
	movl	$148, %edx
	leaq	-80(%rbp), %r13
	movl	%r10d, -84(%rbp)
	movl	%r15d, 248(%rax)
	movl	-104(%rbp), %r15d
	movl	$-1, %esi
	leal	1000(%r15), %ecx
	call	ossl_statem_fatal@PLT
	movl	%r15d, %ecx
	movl	$16, %esi
	movq	%r13, %rdi
	leaq	.LC23(%rip), %rdx
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	movq	%r13, %rdx
	movl	$2, %edi
	xorl	%eax, %eax
	leaq	.LC24(%rip), %rsi
	call	ERR_add_error_data@PLT
	orl	$2, 68(%r12)
	movq	1296(%r12), %rsi
	movl	$1, 56(%rbx)
	movq	1904(%r12), %rdi
	call	SSL_CTX_remove_session@PLT
	movl	-84(%rbp), %r10d
	jmp	.L632
.L689:
	movl	%r14d, %r15d
	testl	%r14d, %r14d
	jne	.L893
	cmpl	$2, %eax
	je	.L720
.L721:
	movl	$1568, %r9d
	leaq	.LC7(%rip), %r8
	movl	$246, %ecx
	movq	%r12, %rdi
	movl	$148, %edx
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
	orl	$-1, %r10d
	jmp	.L632
	.p2align 4,,10
	.p2align 3
.L688:
	movq	168(%r12), %rax
	movl	%r14d, 244(%rax)
	movl	6124(%r12), %eax
	movl	$1, 56(%rbx)
	addl	$1, %eax
	movl	%eax, 6124(%r12)
	cmpl	$5, %eax
	je	.L716
	movl	-116(%rbp), %eax
	testl	%eax, %eax
	je	.L894
	cmpl	$90, %r14d
	jne	.L691
	jmp	.L863
	.p2align 4,,10
	.p2align 3
.L885:
	movq	1440(%r12), %rdx
	movq	272(%rdx), %r15
	testq	%r15, %r15
	jne	.L686
	cmpq	$0, -128(%rbp)
	je	.L687
	movq	-128(%rbp), %r15
	jmp	.L686
	.p2align 4,,10
	.p2align 3
.L893:
	cmpl	$2, %eax
	je	.L720
	cmpl	$100, %r14d
	jne	.L721
.L726:
	movl	$1560, %r9d
	leaq	.LC7(%rip), %r8
	movl	$339, %ecx
	movq	%r12, %rdi
	movl	$148, %edx
	movl	$40, %esi
	call	ossl_statem_fatal@PLT
	orl	$-1, %r10d
	jmp	.L632
.L887:
	movq	168(%r12), %rax
	movl	$2, 272(%rax)
	jmp	.L869
.L848:
	movl	-88(%rbp), %r8d
	movq	%r15, %rax
	movq	%r9, %rbx
	movq	%r14, %r15
	testl	%r8d, %r8d
	jne	.L677
	cmpq	%r15, %rax
	jne	.L677
	testb	$16, 1496(%r12)
	je	.L677
	cmpq	$0, 2176(%r12)
	je	.L895
.L677:
	movq	-136(%rbp), %rax
	movl	$1, %r10d
	movq	%rbx, (%rax)
	jmp	.L632
.L880:
	movl	$1462, %r9d
	jmp	.L868
.L875:
	call	__stack_chk_fail@PLT
.L890:
	cmpq	$0, 8(%rbx)
	jne	.L632
	movl	$1, 56(%rbx)
	jmp	.L632
.L895:
	movq	%r12, %rdi
	call	ssl3_release_read_buffer@PLT
	jmp	.L677
	.p2align 4,,10
	.p2align 3
.L699:
	movq	$0, 8(%rbx)
	movl	$1607, %r9d
	movq	%r12, %rdi
	leaq	.LC7(%rip), %r8
	movl	$291, %ecx
	movl	$148, %edx
	movl	$-1, %esi
	movl	$1, 56(%rbx)
	call	ossl_statem_fatal@PLT
	movl	$-1, %r10d
	jmp	.L632
.L737:
	movl	%ecx, (%r12)
	movl	$1476, %r9d
	leaq	.LC7(%rip), %r8
	movl	$244, %ecx
	jmp	.L865
.L713:
	movl	$1746, %r9d
	jmp	.L867
.L886:
	movq	168(%r12), %rax
	movl	$90, 244(%rax)
	movl	6124(%r12), %eax
	movl	$1, 56(%rbx)
	addl	$1, %eax
	movl	%eax, 6124(%r12)
	cmpl	$5, %eax
	jne	.L863
.L716:
	movl	$1522, %r9d
	leaq	.LC7(%rip), %r8
	movl	$409, %ecx
	jmp	.L865
	.p2align 4,,10
	.p2align 3
.L894:
	testl	%r14d, %r14d
	je	.L722
	cmpl	$100, %r14d
	jne	.L863
	jmp	.L726
.L889:
	movl	$1377, %r9d
	leaq	.LC7(%rip), %r8
	movl	$100, %ecx
	jmp	.L865
	.cfi_endproc
.LFE981:
	.size	ssl3_read_bytes, .-ssl3_read_bytes
	.p2align 4
	.globl	ssl3_record_sequence_update
	.type	ssl3_record_sequence_update, @function
ssl3_record_sequence_update:
.LFB982:
	.cfi_startproc
	endbr64
	addb	$1, 7(%rdi)
	jne	.L896
	addb	$1, 6(%rdi)
	jne	.L896
	addb	$1, 5(%rdi)
	jne	.L896
	addb	$1, 4(%rdi)
	jne	.L896
	addb	$1, 3(%rdi)
	jne	.L896
	addb	$1, 2(%rdi)
	jne	.L896
	addb	$1, 1(%rdi)
	jne	.L896
	addb	$1, (%rdi)
.L896:
	ret
	.cfi_endproc
.LFE982:
	.size	ssl3_record_sequence_update, .-ssl3_record_sequence_update
	.p2align 4
	.globl	RECORD_LAYER_is_sslv2_record
	.type	RECORD_LAYER_is_sslv2_record, @function
RECORD_LAYER_is_sslv2_record:
.LFB983:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$2, 1352(%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE983:
	.size	RECORD_LAYER_is_sslv2_record, .-RECORD_LAYER_is_sslv2_record
	.p2align 4
	.globl	RECORD_LAYER_get_rrec_length
	.type	RECORD_LAYER_get_rrec_length, @function
RECORD_LAYER_get_rrec_length:
.LFB984:
	.cfi_startproc
	endbr64
	movq	1360(%rdi), %rax
	ret
	.cfi_endproc
.LFE984:
	.size	RECORD_LAYER_get_rrec_length, .-RECORD_LAYER_get_rrec_length
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC8:
	.quad	29
	.quad	30
	.align 16
.LC9:
	.quad	23
	.quad	24
	.align 16
.LC10:
	.quad	25
	.quad	26
	.align 16
.LC11:
	.quad	1
	.quad	2
	.align 16
.LC12:
	.quad	3
	.quad	4
	.align 16
.LC13:
	.quad	5
	.quad	6
	.align 16
.LC14:
	.quad	7
	.quad	8
	.align 16
.LC15:
	.quad	9
	.quad	10
	.align 16
.LC16:
	.quad	11
	.quad	12
	.align 16
.LC17:
	.quad	13
	.quad	14
	.align 16
.LC18:
	.quad	15
	.quad	16
	.align 16
.LC19:
	.quad	17
	.quad	18
	.align 16
.LC20:
	.quad	19
	.quad	20
	.align 16
.LC21:
	.quad	21
	.quad	22
	.align 16
.LC22:
	.quad	27
	.quad	28
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
