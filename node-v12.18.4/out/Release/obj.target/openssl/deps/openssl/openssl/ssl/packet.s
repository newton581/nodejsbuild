	.file	"packet.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/ssl/packet.c"
	.text
	.p2align 4
	.type	wpacket_intern_close, @function
wpacket_intern_close:
.LFB299:
	.cfi_startproc
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	24(%r8), %rax
	movq	%rax, %rcx
	subq	24(%rsi), %rcx
	jne	.L2
	movl	32(%rsi), %esi
	testb	$1, %sil
	jne	.L5
	andl	$2, %esi
	je	.L2
	testl	%edx, %edx
	je	.L5
	movq	16(%rdi), %rcx
	movq	16(%r8), %rdx
	subq	%rcx, %rdx
	cmpq	8(%rdi), %rdx
	jne	.L6
	subq	%rcx, %rax
	movq	%rdx, 16(%r8)
	movq	%rax, 24(%r8)
.L6:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 8(%rdi)
.L7:
	movq	(%rdi), %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$219, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rax, 40(%r8)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	CRYPTO_free@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore 6
	movq	16(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L11
	movq	8(%r8), %rax
	testq	%rax, %rax
	je	.L23
.L9:
	movq	8(%rdi), %r9
	addq	%rsi, %r9
	leaq	-1(%rax,%r9), %rax
	movq	%rax, %r10
	subq	%rsi, %r10
	movq	%r10, %rsi
	.p2align 4,,10
	.p2align 3
.L10:
	movb	%cl, (%rax)
	subq	$1, %rax
	shrq	$8, %rcx
	cmpq	%rax, %rsi
	jne	.L10
	testq	%rcx, %rcx
	jne	.L5
.L11:
	testl	%edx, %edx
	jne	.L7
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movq	(%r8), %rax
	movq	8(%rax), %rax
	jmp	.L9
	.cfi_endproc
.LFE299:
	.size	wpacket_intern_close, .-wpacket_intern_close
	.p2align 4
	.globl	WPACKET_allocate_bytes
	.type	WPACKET_allocate_bytes, @function
WPACKET_allocate_bytes:
.LFB288:
	.cfi_startproc
	endbr64
	cmpq	$0, 40(%rdi)
	je	.L46
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	testq	%rsi, %rsi
	je	.L28
	movq	24(%rdi), %rcx
	movq	32(%rdi), %rax
	movq	%rdi, %rbx
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L28
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L49
	movq	16(%rdi), %xmm0
	testq	%rdx, %rdx
	je	.L32
.L33:
	movq	%xmm0, %rdi
	addq	%rdi, %rax
	movq	%rax, (%rdx)
.L32:
	movq	%r12, %xmm1
	movq	%rcx, %xmm2
	movl	$1, %eax
	punpcklqdq	%xmm2, %xmm0
	punpcklqdq	%xmm1, %xmm1
	paddq	%xmm1, %xmm0
	movups	%xmm0, 16(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	movq	(%rdi), %rdi
	movq	(%rdi), %rax
	movq	%rax, %rsi
	subq	%rcx, %rsi
	cmpq	%rsi, %r12
	jbe	.L30
	cmpq	%rax, %r12
	movq	$-1, %rsi
	cmovnb	%r12, %rax
	testq	%rax, %rax
	js	.L31
	leaq	(%rax,%rax), %rsi
	movl	$256, %eax
	cmpq	$256, %rsi
	cmovb	%rax, %rsi
.L31:
	movq	%rdx, -24(%rbp)
	call	BUF_MEM_grow@PLT
	testq	%rax, %rax
	jne	.L50
.L28:
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	16(%rbx), %xmm0
	testq	%rdx, %rdx
	je	.L32
.L34:
	movq	8(%rdi), %rax
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L50:
	movq	-24(%rbp), %rdx
	movq	24(%rbx), %rcx
	movq	16(%rbx), %xmm0
	testq	%rdx, %rdx
	je	.L32
	movq	8(%rbx), %rax
	testq	%rax, %rax
	jne	.L33
	movq	(%rbx), %rdi
	jmp	.L34
	.cfi_endproc
.LFE288:
	.size	WPACKET_allocate_bytes, .-WPACKET_allocate_bytes
	.p2align 4
	.globl	WPACKET_sub_allocate_bytes__
	.type	WPACKET_sub_allocate_bytes__, @function
WPACKET_sub_allocate_bytes__:
.LFB289:
	.cfi_startproc
	endbr64
	cmpq	$0, 40(%rdi)
	je	.L97
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	leaq	.LC0(%rip), %rsi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	movl	$281, %edx
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$40, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$8, %rsp
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L98
	movq	40(%r12), %rax
	movq	%rax, 0(%r13)
	movq	24(%r12), %rax
	movq	%r13, 40(%r12)
	leaq	(%rbx,%rax), %rdx
	movq	%rbx, 16(%r13)
	movq	%rdx, 24(%r13)
	testq	%rbx, %rbx
	jne	.L55
	movq	$0, 8(%r13)
.L56:
	testq	%r13, %r13
	je	.L96
	testq	%r15, %r15
	je	.L96
	movq	24(%r12), %rdx
	movq	32(%r12), %rax
	subq	%rdx, %rax
	cmpq	%rax, %r15
	ja	.L96
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L99
	movq	16(%r12), %xmm0
	testq	%r14, %r14
	je	.L66
.L65:
	movq	%xmm0, %rbx
	addq	%rbx, %rax
	movq	%rax, (%r14)
.L64:
	movq	%rdx, %xmm2
	movq	%r15, %xmm1
	punpcklqdq	%xmm2, %xmm0
	punpcklqdq	%xmm1, %xmm1
	paddq	%xmm1, %xmm0
	movups	%xmm0, 16(%r12)
	testq	%r13, %r13
	je	.L96
.L69:
	cmpq	$0, 0(%r13)
	je	.L96
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1, %edx
	call	wpacket_intern_close
	testl	%eax, %eax
	setne	%al
	addq	$8, %rsp
	popq	%rbx
	movzbl	%al, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	movl	$282, %r8d
	movl	$65, %edx
	movl	$634, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L96:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	32(%r12), %rcx
	subq	%rax, %rcx
	cmpq	%rcx, %rbx
	ja	.L96
	movq	8(%r12), %rcx
	movq	%r13, %rsi
	testq	%rcx, %rcx
	je	.L100
.L95:
	movq	16(%r12), %rdi
	movq	%rdx, 24(%r12)
	leaq	(%rcx,%rdi), %rax
	addq	%rdi, %rbx
	subq	%rcx, %rax
	movq	%rbx, 16(%r12)
	movq	%rax, 8(%r13)
	movq	%rsi, %r13
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L99:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	movq	%rax, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %r15
	jbe	.L62
	cmpq	%rax, %r15
	movq	$-1, %rsi
	cmovnb	%r15, %rax
	testq	%rax, %rax
	js	.L63
	leaq	(%rax,%rax), %rsi
	movl	$256, %eax
	cmpq	$256, %rsi
	cmovb	%rax, %rsi
.L63:
	call	BUF_MEM_grow@PLT
	testq	%rax, %rax
	je	.L96
	movq	24(%r12), %rdx
	movq	16(%r12), %xmm0
	movq	40(%r12), %r13
	testq	%r14, %r14
	je	.L64
	movq	8(%r12), %rax
	testq	%rax, %rax
	jne	.L65
	movq	(%r12), %rdi
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L62:
	movq	16(%r12), %xmm0
	testq	%r14, %r14
	je	.L66
.L67:
	movq	8(%rdi), %rax
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L66:
	movdqa	%xmm0, %xmm1
	movq	%rdx, %xmm3
	movq	%r15, %xmm0
	punpcklqdq	%xmm3, %xmm1
	punpcklqdq	%xmm0, %xmm0
	paddq	%xmm1, %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L100:
	movq	(%r12), %rdi
	movq	(%rdi), %rcx
	movq	%rcx, %rsi
	subq	%rax, %rsi
	cmpq	%rsi, %rbx
	jbe	.L71
	cmpq	%rcx, %rbx
	movq	$-1, %rsi
	cmovnb	%rbx, %rcx
	testq	%rcx, %rcx
	js	.L60
	leaq	(%rcx,%rcx), %rsi
	movl	$256, %eax
	cmpq	$256, %rsi
	cmovb	%rax, %rsi
.L60:
	call	BUF_MEM_grow@PLT
	testq	%rax, %rax
	je	.L96
	movq	24(%r12), %rdx
	movq	8(%r12), %rcx
	movq	40(%r12), %rsi
	addq	%rbx, %rdx
	testq	%rcx, %rcx
	jne	.L95
	movq	(%r12), %rdi
.L59:
	movq	8(%rdi), %rcx
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L71:
	movq	%r13, %rsi
	jmp	.L59
	.cfi_endproc
.LFE289:
	.size	WPACKET_sub_allocate_bytes__, .-WPACKET_sub_allocate_bytes__
	.p2align 4
	.globl	WPACKET_reserve_bytes
	.type	WPACKET_reserve_bytes, @function
WPACKET_reserve_bytes:
.LFB290:
	.cfi_startproc
	endbr64
	cmpq	$0, 40(%rdi)
	je	.L121
	testq	%rsi, %rsi
	je	.L121
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %rcx
	movq	32(%rdi), %rax
	subq	%rcx, %rax
	cmpq	%rsi, %rax
	jb	.L105
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L124
	testq	%rdx, %rdx
	je	.L123
.L110:
	addq	16(%rbx), %rax
	movq	%rax, (%rdx)
.L123:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	movq	(%rdi), %rdi
	movq	(%rdi), %rax
	movq	%rax, %r9
	subq	%rcx, %r9
	cmpq	%rsi, %r9
	jnb	.L107
	cmpq	%rsi, %rax
	movq	$-1, %r8
	cmovnb	%rax, %rsi
	testq	%rsi, %rsi
	js	.L108
	leaq	(%rsi,%rsi), %r8
	movl	$256, %eax
	cmpq	$256, %r8
	cmovb	%rax, %r8
.L108:
	movq	%r8, %rsi
	movq	%rdx, -24(%rbp)
	call	BUF_MEM_grow@PLT
	testq	%rax, %rax
	jne	.L125
.L105:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	testq	%rdx, %rdx
	je	.L123
.L111:
	movq	8(%rdi), %rax
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L125:
	movq	-24(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L123
	movq	8(%rbx), %rax
	testq	%rax, %rax
	jne	.L110
	movq	(%rbx), %rdi
	jmp	.L111
	.cfi_endproc
.LFE290:
	.size	WPACKET_reserve_bytes, .-WPACKET_reserve_bytes
	.p2align 4
	.globl	WPACKET_sub_reserve_bytes__
	.type	WPACKET_sub_reserve_bytes__, @function
WPACKET_sub_reserve_bytes__:
.LFB291:
	.cfi_startproc
	endbr64
	addq	%rcx, %rsi
	cmpq	$0, 40(%rdi)
	je	.L147
	testq	%rsi, %rsi
	je	.L147
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %rdx
	movq	32(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L130
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L149
	testq	%r13, %r13
	je	.L138
.L136:
	addq	16(%rbx), %rax
.L135:
	leaq	(%rax,%rcx), %r12
	movl	$1, %eax
	movq	%r12, 0(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L149:
	.cfi_restore_state
	movq	(%rdi), %rdi
	movq	(%rdi), %rax
	movq	%rax, %r9
	subq	%rdx, %r9
	cmpq	%r9, %rsi
	jbe	.L132
	cmpq	%rax, %rsi
	movq	$-1, %r8
	cmovb	%rax, %rsi
	testq	%rsi, %rsi
	js	.L133
	leaq	(%rsi,%rsi), %r8
	movl	$256, %eax
	cmpq	$256, %r8
	cmovb	%rax, %r8
.L133:
	movq	%r8, %rsi
	movq	%rcx, -40(%rbp)
	call	BUF_MEM_grow@PLT
	testq	%rax, %rax
	jne	.L150
.L130:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	.cfi_restore_state
	testq	%r13, %r13
	jne	.L137
	.p2align 4,,10
	.p2align 3
.L138:
	movq	0, %rax
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L147:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	testq	%r13, %r13
	movq	-40(%rbp), %rcx
	je	.L138
	movq	8(%rbx), %rax
	testq	%rax, %rax
	jne	.L136
	movq	(%rbx), %rdi
.L137:
	movq	8(%rdi), %rax
	jmp	.L136
	.cfi_endproc
.LFE291:
	.size	WPACKET_sub_reserve_bytes__, .-WPACKET_sub_reserve_bytes__
	.p2align 4
	.globl	WPACKET_init_static_len
	.type	WPACKET_init_static_len, @function
WPACKET_init_static_len:
.LFB294:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	$-1, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	-1(%rcx), %rdi
	cmpq	$6, %rdi
	ja	.L152
	leal	0(,%rcx,8), %ecx
	movl	$1, %eax
	salq	%cl, %rax
	addq	%rdi, %rax
.L152:
	testq	%rsi, %rsi
	je	.L162
	testq	%rdx, %rdx
	je	.L162
	cmpq	%rax, %rdx
	movq	%rsi, 8(%rbx)
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	cmova	%rax, %rdx
	movq	$0, (%rbx)
	leaq	.LC0(%rip), %rsi
	movups	%xmm0, 16(%rbx)
	movq	%rdx, 32(%rbx)
	movl	$97, %edx
	call	CRYPTO_zalloc@PLT
	movq	%rax, 40(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L174
	movl	$1, %eax
	testq	%r12, %r12
	je	.L151
	movq	%r12, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 16(%rdi)
	movq	24(%rbx), %rdx
	movq	32(%rbx), %rax
	subq	%rdx, %rax
	cmpq	%rax, %r12
	ja	.L155
	movq	8(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L175
.L156:
	movq	16(%rbx), %rsi
	addq	%r12, %rdx
	movq	%rdx, 24(%rbx)
	leaq	(%rcx,%rsi), %rax
	addq	%rsi, %r12
	subq	%rcx, %rax
	movq	%r12, 16(%rbx)
	movq	%rax, 8(%rdi)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore_state
	xorl	%eax, %eax
.L151:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L176:
	.cfi_restore_state
	movq	40(%rbx), %rdi
	.p2align 4,,10
	.p2align 3
.L155:
	movl	$109, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 40(%rbx)
	xorl	%eax, %eax
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L174:
	movl	$98, %r8d
	movl	$65, %edx
	movl	$633, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L175:
	movq	(%rbx), %r8
	movq	(%r8), %rax
	movq	%rax, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %r12
	jbe	.L157
	cmpq	%rax, %r12
	movq	$-1, %rsi
	cmovnb	%r12, %rax
	testq	%rax, %rax
	js	.L158
	leaq	(%rax,%rax), %rsi
	movl	$256, %eax
	cmpq	$256, %rsi
	cmovb	%rax, %rsi
.L158:
	movq	%r8, %rdi
	call	BUF_MEM_grow@PLT
	testq	%rax, %rax
	je	.L176
	movq	8(%rbx), %rcx
	movq	24(%rbx), %rdx
	movq	40(%rbx), %rdi
	testq	%rcx, %rcx
	jne	.L156
	movq	(%rbx), %r8
.L157:
	movq	8(%r8), %rcx
	jmp	.L156
	.cfi_endproc
.LFE294:
	.size	WPACKET_init_static_len, .-WPACKET_init_static_len
	.p2align 4
	.globl	WPACKET_init_len
	.type	WPACKET_init_len, @function
WPACKET_init_len:
.LFB295:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L188
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	leaq	-1(%rdx), %rdx
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	$0, 8(%rdi)
	movq	%rsi, (%rdi)
	cmpq	$6, %rdx
	ja	.L179
	movl	$1, %eax
	leal	0(,%r12,8), %ecx
	pxor	%xmm0, %xmm0
	salq	%cl, %rax
	movups	%xmm0, 16(%rdi)
	leaq	.LC0(%rip), %rsi
	addq	%rdx, %rax
	movl	$97, %edx
	movq	%rax, 32(%rdi)
	movl	$40, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, 40(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L181
.L180:
	movq	%r12, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 16(%rdi)
	movq	24(%rbx), %rcx
	movq	32(%rbx), %rax
	subq	%rcx, %rax
	cmpq	%rax, %r12
	ja	.L182
	movq	8(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L198
.L183:
	movq	16(%rbx), %rdx
	addq	%r12, %rcx
	movq	%rcx, 24(%rbx)
	addq	%rdx, %r12
	leaq	(%rsi,%rdx), %rax
	movq	%r12, 16(%rbx)
.L187:
	subq	%rsi, %rax
	movq	%rax, 8(%rdi)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	.cfi_restore_state
	movl	$98, %r8d
	movl	$65, %edx
	movl	$633, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L188:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	pxor	%xmm0, %xmm0
	movq	$-1, 32(%rdi)
	movl	$97, %edx
	leaq	.LC0(%rip), %rsi
	movups	%xmm0, 16(%rdi)
	movl	$40, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, 40(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L181
	testq	%r12, %r12
	jne	.L180
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L199:
	.cfi_restore_state
	movq	40(%rbx), %rdi
.L182:
	movl	$109, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 40(%rbx)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L198:
	.cfi_restore_state
	movq	(%rbx), %r8
	movq	(%r8), %rax
	movq	%rax, %rdx
	subq	%rcx, %rdx
	cmpq	%rdx, %r12
	jbe	.L184
	cmpq	%rax, %r12
	movq	$-1, %rsi
	cmovnb	%r12, %rax
	testq	%rax, %rax
	js	.L185
	leaq	(%rax,%rax), %rsi
	movl	$256, %eax
	cmpq	$256, %rsi
	cmovb	%rax, %rsi
.L185:
	movq	%r8, %rdi
	call	BUF_MEM_grow@PLT
	testq	%rax, %rax
	je	.L199
	movq	8(%rbx), %rsi
	movq	24(%rbx), %rcx
	movq	40(%rbx), %rdi
	testq	%rsi, %rsi
	jne	.L183
	movq	(%rbx), %r8
.L184:
	movq	8(%r8), %rsi
	movq	16(%rbx), %rdx
	addq	%r12, %rcx
	movq	%rcx, 24(%rbx)
	leaq	(%rsi,%rdx), %rax
	addq	%r12, %rdx
	movq	%rdx, 16(%rbx)
	jmp	.L187
	.cfi_endproc
.LFE295:
	.size	WPACKET_init_len, .-WPACKET_init_len
	.p2align 4
	.globl	WPACKET_init
	.type	WPACKET_init, @function
WPACKET_init:
.LFB296:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L202
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movl	$97, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rsi, (%rdi)
	leaq	.LC0(%rip), %rsi
	movq	$0, 8(%rdi)
	movq	$-1, 32(%rdi)
	movups	%xmm0, 16(%rdi)
	movl	$40, %edi
	call	CRYPTO_zalloc@PLT
	movl	$1, %r8d
	movq	%rax, 40(%rbx)
	testq	%rax, %rax
	je	.L208
.L200:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L202:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%r8d, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$98, %r8d
	movl	$65, %edx
	movl	$633, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r8d, %r8d
	jmp	.L200
	.cfi_endproc
.LFE296:
	.size	WPACKET_init, .-WPACKET_init
	.p2align 4
	.globl	WPACKET_set_flags
	.type	WPACKET_set_flags, @function
WPACKET_set_flags:
.LFB297:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	testq	%rax, %rax
	je	.L211
	movl	%esi, 32(%rax)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L211:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE297:
	.size	WPACKET_set_flags, .-WPACKET_set_flags
	.p2align 4
	.globl	WPACKET_fill_lengths
	.type	WPACKET_fill_lengths, @function
WPACKET_fill_lengths:
.LFB300:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L213
	.p2align 4,,10
	.p2align 3
.L216:
	movq	24(%rdi), %rax
	subq	24(%rdx), %rax
	jne	.L214
	testb	$3, 32(%rdx)
	je	.L214
.L213:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L214:
	movq	16(%rdx), %rsi
	testq	%rsi, %rsi
	je	.L220
	movq	8(%rdi), %rcx
	testq	%rcx, %rcx
	je	.L229
.L218:
	movq	8(%rdx), %r8
	addq	%rsi, %r8
	leaq	-1(%rcx,%r8), %rcx
	movq	%rcx, %r9
	subq	%rsi, %r9
	movq	%r9, %rsi
	.p2align 4,,10
	.p2align 3
.L219:
	movb	%al, (%rcx)
	subq	$1, %rcx
	shrq	$8, %rax
	cmpq	%rcx, %rsi
	jne	.L219
	testq	%rax, %rax
	jne	.L213
.L220:
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L216
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L229:
	movq	(%rdi), %rcx
	movq	8(%rcx), %rcx
	jmp	.L218
	.cfi_endproc
.LFE300:
	.size	WPACKET_fill_lengths, .-WPACKET_fill_lengths
	.p2align 4
	.globl	WPACKET_close
	.type	WPACKET_close, @function
WPACKET_close:
.LFB301:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L233
	movq	(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L233
	movq	24(%rdx), %r8
	movq	%r8, %rcx
	subq	24(%rdi), %rcx
	je	.L253
	movq	16(%rdi), %rax
.L235:
	testq	%rax, %rax
	je	.L237
	movq	8(%rdx), %rsi
	testq	%rsi, %rsi
	je	.L254
.L238:
	movq	8(%rdi), %r10
	leaq	-1(%r10), %r8
	addq	%r8, %rax
	addq	%rsi, %rax
	addq	%r8, %rsi
	.p2align 4,,10
	.p2align 3
.L239:
	movb	%cl, (%rax)
	subq	$1, %rax
	shrq	$8, %rcx
	cmpq	%rsi, %rax
	jne	.L239
	testq	%rcx, %rcx
	jne	.L233
	movq	(%rdi), %rsi
.L237:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, 40(%rdx)
	movl	$219, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	CRYPTO_free@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L253:
	.cfi_restore 6
	movl	32(%rdi), %r9d
	testb	$1, %r9b
	je	.L255
.L233:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	andl	$2, %r9d
	movq	16(%rdi), %rax
	je	.L235
	movq	16(%rdx), %rcx
	subq	%rax, %rcx
	cmpq	8(%rdi), %rcx
	jne	.L236
	subq	%rax, %r8
	movq	%rcx, 16(%rdx)
	movq	%r8, 24(%rdx)
.L236:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 8(%rdi)
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L254:
	movq	(%rdx), %rsi
	movq	8(%rsi), %rsi
	jmp	.L238
	.cfi_endproc
.LFE301:
	.size	WPACKET_close, .-WPACKET_close
	.p2align 4
	.globl	WPACKET_finish
	.type	WPACKET_finish, @function
WPACKET_finish:
.LFB302:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L259
	movq	(%rdi), %rcx
	testq	%rcx, %rcx
	je	.L274
.L259:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L274:
	.cfi_restore_state
	movq	24(%rbx), %rsi
	movq	%rsi, %rdx
	subq	24(%rdi), %rdx
	je	.L275
	movq	16(%rdi), %rax
.L261:
	testq	%rax, %rax
	je	.L263
	movq	8(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L276
.L264:
	movq	8(%rdi), %rsi
	subq	$1, %rsi
	addq	%rsi, %rax
	addq	%rcx, %rax
	addq	%rsi, %rcx
	.p2align 4,,10
	.p2align 3
.L265:
	movb	%dl, (%rax)
	subq	$1, %rax
	shrq	$8, %rdx
	cmpq	%rcx, %rax
	jne	.L265
	testq	%rdx, %rdx
	jne	.L259
	movq	(%rdi), %rcx
.L263:
	movq	%rcx, 40(%rbx)
	movl	$219, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	40(%rbx), %rdi
	movl	$265, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 40(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L275:
	.cfi_restore_state
	movl	32(%rdi), %r8d
	testb	$1, %r8b
	jne	.L259
	andl	$2, %r8d
	movq	16(%rdi), %rax
	je	.L261
	movq	16(%rbx), %rdx
	subq	%rax, %rdx
	cmpq	8(%rdi), %rdx
	jne	.L262
	subq	%rax, %rsi
	movq	%rdx, 16(%rbx)
	movq	%rsi, 24(%rbx)
.L262:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 8(%rdi)
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L276:
	movq	(%rbx), %rcx
	movq	8(%rcx), %rcx
	jmp	.L264
	.cfi_endproc
.LFE302:
	.size	WPACKET_finish, .-WPACKET_finish
	.p2align 4
	.globl	WPACKET_start_sub_packet_len__
	.type	WPACKET_start_sub_packet_len__, @function
WPACKET_start_sub_packet_len__:
.LFB303:
	.cfi_startproc
	endbr64
	cmpq	$0, 40(%rdi)
	je	.L296
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$281, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	leaq	.LC0(%rip), %rsi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$40, %edi
	subq	$8, %rsp
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L297
	movq	40(%rbx), %rax
	movq	%rax, (%r12)
	movq	24(%rbx), %rax
	movq	%r12, 40(%rbx)
	leaq	(%rax,%r13), %rdx
	movq	%r13, 16(%r12)
	movq	%rdx, 24(%r12)
	testq	%r13, %r13
	jne	.L281
	movq	$0, 8(%r12)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L281:
	.cfi_restore_state
	movq	32(%rbx), %rcx
	subq	%rax, %rcx
	cmpq	%rcx, %r13
	ja	.L295
	movq	8(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L298
.L294:
	movq	16(%rbx), %rsi
	movq	%rdx, 24(%rbx)
	leaq	(%rcx,%rsi), %rax
	addq	%r13, %rsi
	subq	%rcx, %rax
	movq	%rsi, 16(%rbx)
	movq	%rax, 8(%r12)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L297:
	.cfi_restore_state
	movl	$282, %r8d
	movl	$65, %edx
	movl	$634, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L295:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L296:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L298:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	(%rbx), %rdi
	movq	(%rdi), %rcx
	movq	%rcx, %rsi
	subq	%rax, %rsi
	cmpq	%rsi, %r13
	jbe	.L284
	cmpq	%rcx, %r13
	movq	$-1, %rsi
	cmovnb	%r13, %rcx
	testq	%rcx, %rcx
	js	.L285
	leaq	(%rcx,%rcx), %rsi
	movl	$256, %eax
	cmpq	$256, %rsi
	cmovb	%rax, %rsi
.L285:
	call	BUF_MEM_grow@PLT
	testq	%rax, %rax
	je	.L295
	movq	24(%rbx), %rdx
	movq	8(%rbx), %rcx
	addq	%r13, %rdx
	testq	%rcx, %rcx
	jne	.L294
	movq	(%rbx), %rdi
.L284:
	movq	8(%rdi), %rcx
	jmp	.L294
	.cfi_endproc
.LFE303:
	.size	WPACKET_start_sub_packet_len__, .-WPACKET_start_sub_packet_len__
	.p2align 4
	.globl	WPACKET_start_sub_packet
	.type	WPACKET_start_sub_packet, @function
WPACKET_start_sub_packet:
.LFB304:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	cmpq	$0, 40(%rdi)
	je	.L299
	movq	%rdi, %rbx
	movl	$281, %edx
	leaq	.LC0(%rip), %rsi
	movl	$40, %edi
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L304
	movq	40(%rbx), %rdx
	pxor	%xmm0, %xmm0
	movl	$1, %r12d
	movq	%rdx, (%rax)
	movq	24(%rbx), %rdx
	movq	%rax, 40(%rbx)
	movq	%rdx, 24(%rax)
	movups	%xmm0, 8(%rax)
.L299:
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L304:
	.cfi_restore_state
	movl	$282, %r8d
	movl	$65, %edx
	movl	$634, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L299
	.cfi_endproc
.LFE304:
	.size	WPACKET_start_sub_packet, .-WPACKET_start_sub_packet
	.p2align 4
	.globl	WPACKET_put_bytes__
	.type	WPACKET_put_bytes__, @function
WPACKET_put_bytes__:
.LFB305:
	.cfi_startproc
	endbr64
	cmpq	$4, %rdx
	jbe	.L306
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L306:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	cmpq	$0, 40(%rdi)
	je	.L308
	testq	%rdx, %rdx
	je	.L308
	movq	24(%rdi), %rax
	movq	32(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rcx, %rdx
	ja	.L308
	movq	8(%rdi), %rcx
	movl	%esi, %r12d
	testq	%rcx, %rcx
	je	.L332
.L309:
	movq	16(%rbx), %rdi
	addq	%rdx, %rax
	movl	%r12d, %esi
	movq	%rax, 24(%rbx)
	leaq	(%rdx,%rdi), %rax
	movq	%rax, 16(%rbx)
	leaq	-1(%rdi,%rdx), %rax
	addq	%rax, %rcx
	movq	%rsi, %rax
	movb	%r12b, (%rcx)
	shrq	$8, %rax
	cmpq	$1, %rdx
	je	.L312
	movb	%al, -1(%rcx)
	movq	%rsi, %rax
	shrq	$16, %rax
	subq	$2, %rdx
	je	.L312
	movb	%al, -2(%rcx)
	movq	%rsi, %rax
	shrq	$24, %rax
	cmpq	$1, %rdx
	je	.L312
	movb	%al, -3(%rcx)
	xorl	%eax, %eax
.L312:
	testq	%rax, %rax
	sete	%al
	addq	$16, %rsp
	popq	%rbx
	movzbl	%al, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L308:
	.cfi_restore_state
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L332:
	.cfi_restore_state
	movq	(%rdi), %rdi
	movq	(%rdi), %rcx
	movq	%rcx, %rsi
	subq	%rax, %rsi
	cmpq	%rsi, %rdx
	jbe	.L310
	cmpq	%rcx, %rdx
	movq	$-1, %rsi
	cmovnb	%rdx, %rcx
	testq	%rcx, %rcx
	js	.L311
	leaq	(%rcx,%rcx), %rsi
	movl	$256, %eax
	cmpq	$256, %rsi
	cmovb	%rax, %rsi
.L311:
	movq	%rdx, -24(%rbp)
	call	BUF_MEM_grow@PLT
	testq	%rax, %rax
	je	.L308
	movq	8(%rbx), %rcx
	movq	24(%rbx), %rax
	movq	-24(%rbp), %rdx
	testq	%rcx, %rcx
	jne	.L309
	movq	(%rbx), %rdi
.L310:
	movq	8(%rdi), %rcx
	jmp	.L309
	.cfi_endproc
.LFE305:
	.size	WPACKET_put_bytes__, .-WPACKET_put_bytes__
	.p2align 4
	.globl	WPACKET_set_max_size
	.type	WPACKET_set_max_size, @function
WPACKET_set_max_size:
.LFB306:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	testq	%rax, %rax
	je	.L337
.L335:
	movq	%rax, %rdx
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L335
	movq	16(%rdx), %rcx
	leaq	-1(%rcx), %rdx
	cmpq	$6, %rdx
	jbe	.L341
	xorl	%r8d, %r8d
	cmpq	%rsi, 24(%rdi)
	ja	.L333
.L342:
	movq	%rsi, 32(%rdi)
	movl	$1, %r8d
.L333:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L341:
	sall	$3, %ecx
	movl	$1, %eax
	xorl	%r8d, %r8d
	salq	%cl, %rax
	addq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L333
	xorl	%r8d, %r8d
	cmpq	%rsi, 24(%rdi)
	jbe	.L342
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L337:
	xorl	%r8d, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE306:
	.size	WPACKET_set_max_size, .-WPACKET_set_max_size
	.p2align 4
	.globl	WPACKET_memset
	.type	WPACKET_memset, @function
WPACKET_memset:
.LFB307:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	testq	%rdx, %rdx
	jne	.L360
	ret
	.p2align 4,,10
	.p2align 3
.L360:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	cmpq	$0, 40(%rdi)
	je	.L346
	movq	24(%rdi), %rax
	movq	32(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rcx, %rdx
	ja	.L346
	movq	8(%rdi), %rdi
	movl	%esi, %r12d
	testq	%rdi, %rdi
	je	.L361
.L347:
	movq	16(%rbx), %rcx
	addq	%rdx, %rax
	movl	%r12d, %esi
	movq	%rax, 24(%rbx)
	leaq	(%rdx,%rcx), %rax
	addq	%rcx, %rdi
	movq	%rax, 16(%rbx)
	call	memset@PLT
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L346:
	.cfi_restore_state
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L361:
	.cfi_restore_state
	movq	(%rbx), %rdi
	movq	(%rdi), %rcx
	movq	%rcx, %rsi
	subq	%rax, %rsi
	cmpq	%rsi, %rdx
	jbe	.L348
	cmpq	%rcx, %rdx
	movq	$-1, %rsi
	cmovnb	%rdx, %rcx
	testq	%rcx, %rcx
	js	.L349
	leaq	(%rcx,%rcx), %rsi
	movl	$256, %eax
	cmpq	$256, %rsi
	cmovb	%rax, %rsi
.L349:
	movq	%rdx, -24(%rbp)
	call	BUF_MEM_grow@PLT
	testq	%rax, %rax
	je	.L346
	movq	8(%rbx), %rdi
	movq	24(%rbx), %rax
	movq	-24(%rbp), %rdx
	testq	%rdi, %rdi
	jne	.L347
	movq	(%rbx), %rdi
.L348:
	movq	8(%rdi), %rdi
	jmp	.L347
	.cfi_endproc
.LFE307:
	.size	WPACKET_memset, .-WPACKET_memset
	.p2align 4
	.globl	WPACKET_memcpy
	.type	WPACKET_memcpy, @function
WPACKET_memcpy:
.LFB308:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	testq	%rdx, %rdx
	jne	.L379
	ret
	.p2align 4,,10
	.p2align 3
.L379:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	cmpq	$0, 40(%rdi)
	je	.L365
	movq	24(%rdi), %rax
	movq	32(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rcx, %rdx
	ja	.L365
	movq	8(%rdi), %rdi
	movq	%rsi, %r12
	testq	%rdi, %rdi
	je	.L380
.L366:
	movq	16(%rbx), %rcx
	addq	%rdx, %rax
	movq	%r12, %rsi
	movq	%rax, 24(%rbx)
	leaq	(%rdx,%rcx), %rax
	addq	%rcx, %rdi
	movq	%rax, 16(%rbx)
	call	memcpy@PLT
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L365:
	.cfi_restore_state
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L380:
	.cfi_restore_state
	movq	(%rbx), %rdi
	movq	(%rdi), %rcx
	movq	%rcx, %rsi
	subq	%rax, %rsi
	cmpq	%rsi, %rdx
	jbe	.L367
	cmpq	%rcx, %rdx
	movq	$-1, %rsi
	cmovnb	%rdx, %rcx
	testq	%rcx, %rcx
	js	.L368
	leaq	(%rcx,%rcx), %rsi
	movl	$256, %eax
	cmpq	$256, %rsi
	cmovb	%rax, %rsi
.L368:
	movq	%rdx, -24(%rbp)
	call	BUF_MEM_grow@PLT
	testq	%rax, %rax
	je	.L365
	movq	8(%rbx), %rdi
	movq	24(%rbx), %rax
	movq	-24(%rbp), %rdx
	testq	%rdi, %rdi
	jne	.L366
	movq	(%rbx), %rdi
.L367:
	movq	8(%rdi), %rdi
	jmp	.L366
	.cfi_endproc
.LFE308:
	.size	WPACKET_memcpy, .-WPACKET_memcpy
	.p2align 4
	.globl	WPACKET_sub_memcpy__
	.type	WPACKET_sub_memcpy__, @function
WPACKET_sub_memcpy__:
.LFB309:
	.cfi_startproc
	endbr64
	cmpq	$0, 40(%rdi)
	je	.L427
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movl	$281, %edx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	leaq	.LC0(%rip), %rsi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$40, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$8, %rsp
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L428
	movq	24(%r12), %rdx
	movq	40(%r12), %rax
	leaq	(%rbx,%rdx), %rsi
	movq	%rax, 0(%r13)
	movq	%r13, 40(%r12)
	movq	%rsi, 24(%r13)
	movq	%rbx, 16(%r13)
	testq	%rbx, %rbx
	jne	.L385
	movq	$0, 8(%r13)
	testq	%r15, %r15
	jne	.L387
.L386:
	testq	%rax, %rax
	je	.L426
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1, %edx
	call	wpacket_intern_close
	testl	%eax, %eax
	setne	%al
	addq	$8, %rsp
	popq	%rbx
	movzbl	%al, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L428:
	.cfi_restore_state
	movl	$282, %r8d
	movl	$65, %edx
	movl	$634, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L426:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L427:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L385:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	32(%r12), %rax
	subq	%rdx, %rax
	cmpq	%rax, %rbx
	ja	.L426
	movq	8(%r12), %rdi
	movq	%r13, %rcx
	testq	%rdi, %rdi
	je	.L429
.L425:
	movq	16(%r12), %rdx
	movq	%rsi, 24(%r12)
	leaq	(%rdi,%rdx), %rax
	addq	%rdx, %rbx
	subq	%rdi, %rax
	movq	%rbx, 16(%r12)
	movq	%rax, 8(%r13)
	testq	%r15, %r15
	je	.L392
	testq	%rcx, %rcx
	je	.L426
	movq	24(%r12), %rdx
.L387:
	movq	32(%r12), %rcx
	movq	%rdx, %rax
	subq	%rdx, %rcx
	cmpq	%rcx, %r15
	ja	.L426
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L430
.L393:
	movq	16(%r12), %rdx
	addq	%r15, %rax
	movq	%r14, %rsi
	movq	%rax, 24(%r12)
	leaq	(%r15,%rdx), %rax
	addq	%rdx, %rdi
	movq	%r15, %rdx
	movq	%rax, 16(%r12)
	call	memcpy@PLT
	movq	40(%r12), %rcx
.L392:
	testq	%rcx, %rcx
	je	.L426
	movq	(%rcx), %rax
	movq	%rcx, %r13
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L430:
	movq	(%r12), %rdi
	movq	(%rdi), %rcx
	movq	%rcx, %rbx
	subq	%rdx, %rbx
	cmpq	%rbx, %r15
	jbe	.L394
	cmpq	%rcx, %r15
	movq	$-1, %rsi
	cmovnb	%r15, %rcx
	testq	%rcx, %rcx
	js	.L395
	leaq	(%rcx,%rcx), %rsi
	movl	$256, %eax
	cmpq	$256, %rsi
	cmovb	%rax, %rsi
.L395:
	call	BUF_MEM_grow@PLT
	testq	%rax, %rax
	je	.L426
	movq	8(%r12), %rdi
	movq	24(%r12), %rax
	testq	%rdi, %rdi
	jne	.L393
	movq	(%r12), %rdi
.L394:
	movq	8(%rdi), %rdi
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L429:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	movq	%rax, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %rbx
	jbe	.L398
	cmpq	%rax, %rbx
	movq	$-1, %rsi
	cmovnb	%rbx, %rax
	testq	%rax, %rax
	js	.L391
	leaq	(%rax,%rax), %rsi
	movl	$256, %eax
	cmpq	$256, %rsi
	cmovb	%rax, %rsi
.L391:
	call	BUF_MEM_grow@PLT
	testq	%rax, %rax
	je	.L426
	movq	24(%r12), %rsi
	movq	8(%r12), %rdi
	movq	40(%r12), %rcx
	addq	%rbx, %rsi
	testq	%rdi, %rdi
	jne	.L425
	movq	(%r12), %rdi
.L390:
	movq	8(%rdi), %rdi
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L398:
	movq	%r13, %rcx
	jmp	.L390
	.cfi_endproc
.LFE309:
	.size	WPACKET_sub_memcpy__, .-WPACKET_sub_memcpy__
	.p2align 4
	.globl	WPACKET_get_total_written
	.type	WPACKET_get_total_written, @function
WPACKET_get_total_written:
.LFB310:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L433
	movq	24(%rdi), %rax
	movq	%rax, (%rsi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L433:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE310:
	.size	WPACKET_get_total_written, .-WPACKET_get_total_written
	.p2align 4
	.globl	WPACKET_get_length
	.type	WPACKET_get_length, @function
WPACKET_get_length:
.LFB311:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L436
	testq	%rsi, %rsi
	je	.L436
	movq	24(%rdi), %rax
	subq	24(%rdx), %rax
	movq	%rax, (%rsi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L436:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE311:
	.size	WPACKET_get_length, .-WPACKET_get_length
	.p2align 4
	.globl	WPACKET_get_curr
	.type	WPACKET_get_curr, @function
WPACKET_get_curr:
.LFB312:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L445
	addq	16(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L445:
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	addq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE312:
	.size	WPACKET_get_curr, .-WPACKET_get_curr
	.p2align 4
	.globl	WPACKET_cleanup
	.type	WPACKET_cleanup, @function
WPACKET_cleanup:
.LFB313:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	40(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L447
	leaq	.LC0(%rip), %r12
	.p2align 4,,10
	.p2align 3
.L448:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	movl	$421, %edx
	movq	%r12, %rsi
	call	CRYPTO_free@PLT
	testq	%rbx, %rbx
	jne	.L448
.L447:
	movq	$0, 40(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE313:
	.size	WPACKET_cleanup, .-WPACKET_cleanup
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
