	.file	"extensions_clnt.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/ssl/statem/extensions_clnt.c"
	.text
	.p2align 4
	.globl	tls_construct_ctos_renegotiate
	.type	tls_construct_ctos_renegotiate, @function
tls_construct_ctos_renegotiate:
.LFB1614:
	.cfi_startproc
	endbr64
	movl	1928(%rdi), %edx
	movl	$2, %eax
	testl	%edx, %edx
	je	.L16
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movl	$65281, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L4
	movl	$2, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L4
	movq	168(%r12), %rsi
	movl	$1, %ecx
	movq	%r13, %rdi
	movq	904(%rsi), %rdx
	addq	$840, %rsi
	call	WPACKET_sub_memcpy__@PLT
	testl	%eax, %eax
	jne	.L20
	.p2align 4,,10
	.p2align 3
.L4:
	movl	$28, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$473, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L1:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L4
	movl	$1, %eax
	jmp	.L1
	.cfi_endproc
.LFE1614:
	.size	tls_construct_ctos_renegotiate, .-tls_construct_ctos_renegotiate
	.p2align 4
	.globl	tls_construct_ctos_server_name
	.type	tls_construct_ctos_server_name, @function
tls_construct_ctos_server_name:
.LFB1615:
	.cfi_startproc
	endbr64
	cmpq	$0, 1600(%rdi)
	je	.L25
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$8, %rsp
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L24
	movl	$2, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	jne	.L48
.L24:
	movl	$54, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$475, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L21:
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	movl	$2, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L24
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L24
	movq	1600(%r12), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	movl	$2, %ecx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	WPACKET_sub_memcpy__@PLT
	testl	%eax, %eax
	je	.L24
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L24
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L24
	movl	$1, %eax
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	movl	$2, %eax
	ret
	.cfi_endproc
.LFE1615:
	.size	tls_construct_ctos_server_name, .-tls_construct_ctos_server_name
	.p2align 4
	.globl	tls_construct_ctos_maxfragmentlen
	.type	tls_construct_ctos_maxfragmentlen, @function
tls_construct_ctos_maxfragmentlen:
.LFB1616:
	.cfi_startproc
	endbr64
	cmpb	$0, 1844(%rdi)
	movl	$2, %eax
	je	.L64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movl	$1, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L52
	movl	$2, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L52
	movzbl	1844(%r12), %esi
	movl	$1, %edx
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	jne	.L67
	.p2align 4,,10
	.p2align 3
.L52:
	movl	$80, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$549, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L49:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L52
	movl	$1, %eax
	jmp	.L49
	.cfi_endproc
.LFE1616:
	.size	tls_construct_ctos_maxfragmentlen, .-tls_construct_ctos_maxfragmentlen
	.p2align 4
	.globl	tls_construct_ctos_srp
	.type	tls_construct_ctos_srp, @function
tls_construct_ctos_srp:
.LFB1617:
	.cfi_startproc
	endbr64
	cmpq	$0, 2008(%rdi)
	je	.L72
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	movl	$12, %esi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$8, %rsp
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L71
	movl	$2, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	jne	.L95
.L71:
	movl	$106, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$478, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L68:
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	movl	$1, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L71
	movl	$1, %esi
	movq	%r13, %rdi
	call	WPACKET_set_flags@PLT
	testl	%eax, %eax
	je	.L71
	movq	2008(%r12), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	WPACKET_memcpy@PLT
	testl	%eax, %eax
	je	.L71
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L71
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L71
	movl	$1, %eax
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	movl	$2, %eax
	ret
	.cfi_endproc
.LFE1617:
	.size	tls_construct_ctos_srp, .-tls_construct_ctos_srp
	.p2align 4
	.globl	tls_construct_ctos_ec_pt_formats
	.type	tls_construct_ctos_ec_pt_formats, @function
tls_construct_ctos_ec_pt_formats:
.LFB1619:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$768, (%rdi)
	movl	$2, %eax
	je	.L96
	movq	%rdi, %r12
	movq	%rsi, %r13
	call	SSL_get1_supported_ciphers@PLT
	movq	%rax, %rdi
	movq	%rax, %r14
	call	OPENSSL_sk_num@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	jle	.L98
	xorl	%ebx, %ebx
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L118:
	cmpl	$771, 44(%rax)
	jg	.L99
	addl	$1, %ebx
	cmpl	%ebx, %r15d
	je	.L98
.L100:
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movl	28(%rax), %ecx
	movl	32(%rax), %edx
	andl	$132, %ecx
	andl	$8, %edx
	orl	%edx, %ecx
	je	.L118
.L99:
	movq	%r14, %rdi
	call	OPENSSL_sk_free@PLT
	leaq	-64(%rbp), %rdx
	leaq	-72(%rbp), %rsi
	movq	%r12, %rdi
	call	tls1_get_formatlist@PLT
	movl	$2, %edx
	movl	$11, %esi
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L102
	movl	$2, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	jne	.L101
.L102:
	movl	$163, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$467, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L96:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L119
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rsi
	movl	$1, %ecx
	movq	%r13, %rdi
	call	WPACKET_sub_memcpy__@PLT
	testl	%eax, %eax
	je	.L102
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L102
	movl	$1, %eax
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L98:
	movq	%r14, %rdi
	call	OPENSSL_sk_free@PLT
	movl	$2, %eax
	jmp	.L96
.L119:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1619:
	.size	tls_construct_ctos_ec_pt_formats, .-tls_construct_ctos_ec_pt_formats
	.p2align 4
	.globl	tls_construct_ctos_supported_groups
	.type	tls_construct_ctos_supported_groups, @function
tls_construct_ctos_supported_groups:
.LFB1620:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$768, (%rdi)
	movq	$0, -72(%rbp)
	movl	$2, %eax
	movq	$0, -64(%rbp)
	je	.L120
	movq	%rdi, %r12
	movq	%rsi, %r13
	call	SSL_get1_supported_ciphers@PLT
	movq	%rax, %rdi
	movq	%rax, %r15
	call	OPENSSL_sk_num@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	jle	.L122
	xorl	%r14d, %r14d
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L156:
	cmpl	$771, 44(%rax)
	jg	.L123
	addl	$1, %r14d
	cmpl	%r14d, %ebx
	je	.L122
.L124:
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	movl	28(%rax), %ecx
	movl	32(%rax), %edx
	andl	$132, %ecx
	andl	$8, %edx
	orl	%edx, %ecx
	je	.L156
.L123:
	movq	%r15, %rdi
	call	OPENSSL_sk_free@PLT
	leaq	-64(%rbp), %rdx
	leaq	-72(%rbp), %rsi
	movq	%r12, %rdi
	call	tls1_get_supported_groups@PLT
	movl	$2, %edx
	movl	$10, %esi
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L125
	movl	$2, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	jne	.L157
.L125:
	movl	$191, %r9d
.L155:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	$480, %edx
	movq	%r12, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L120:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L158
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L157:
	.cfi_restore_state
	movl	$2, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L125
	xorl	%ebx, %ebx
	cmpq	$0, -64(%rbp)
	jne	.L126
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L128:
	addq	$1, %rbx
	cmpq	%rbx, -64(%rbp)
	jbe	.L127
.L126:
	movq	-72(%rbp), %rax
	movl	$131076, %edx
	movq	%r12, %rdi
	movzwl	(%rax,%rbx,2), %r14d
	movl	%r14d, %esi
	call	tls_curve_allowed@PLT
	testl	%eax, %eax
	je	.L128
	movl	$2, %edx
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	jne	.L128
	movl	$202, %r9d
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L122:
	movq	%r15, %rdi
	call	OPENSSL_sk_free@PLT
	movl	$2, %eax
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L127:
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L130
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L130
	movl	$1, %eax
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L130:
	movl	$210, %r9d
	jmp	.L155
.L158:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1620:
	.size	tls_construct_ctos_supported_groups, .-tls_construct_ctos_supported_groups
	.p2align 4
	.globl	tls_construct_ctos_session_ticket
	.type	tls_construct_ctos_session_ticket, @function
tls_construct_ctos_session_ticket:
.LFB1621:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	call	tls_use_ticket@PLT
	testl	%eax, %eax
	je	.L171
	movl	60(%r12), %eax
	movq	1296(%r12), %rbx
	testl	%eax, %eax
	je	.L186
	testq	%rbx, %rbx
	je	.L168
.L166:
	movq	1736(%r12), %rax
	testq	%rax, %rax
	je	.L165
	cmpq	$0, 8(%rax)
	je	.L171
	movzwl	(%rax), %r13d
	movl	$236, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_malloc@PLT
	movl	$238, %r9d
	movq	%rax, 552(%rbx)
	movq	1296(%r12), %rax
	movq	552(%rax), %rdi
	testq	%rdi, %rdi
	je	.L185
	movq	1736(%r12), %rax
	movq	%r13, %rdx
	movq	8(%rax), %rsi
	call	memcpy@PLT
	movq	1296(%r12), %rax
	movq	%r13, 560(%rax)
	testq	%r13, %r13
	jne	.L170
.L168:
	movq	1736(%r12), %rax
	testq	%rax, %rax
	je	.L165
	cmpq	$0, 8(%rax)
	je	.L171
.L165:
	xorl	%r13d, %r13d
.L170:
	movl	$2, %edx
	movl	$35, %esi
	movq	%r14, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L174
	movq	1296(%r12), %rax
	movl	$2, %ecx
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	552(%rax), %rsi
	call	WPACKET_sub_memcpy__@PLT
	testl	%eax, %eax
	je	.L174
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L186:
	.cfi_restore_state
	testq	%rbx, %rbx
	je	.L168
	cmpq	$0, 552(%rbx)
	je	.L166
	cmpl	$772, (%rbx)
	je	.L166
	movq	560(%rbx), %r13
	testq	%r13, %r13
	je	.L168
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L171:
	popq	%rbx
	movl	$2, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L174:
	.cfi_restore_state
	movl	$256, %r9d
.L185:
	movq	%r12, %rdi
	movl	$68, %ecx
	movl	$476, %edx
	movl	$80, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1621:
	.size	tls_construct_ctos_session_ticket, .-tls_construct_ctos_session_ticket
	.p2align 4
	.globl	tls_construct_ctos_sig_algs
	.type	tls_construct_ctos_sig_algs, @function
tls_construct_ctos_sig_algs:
.LFB1622:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	8(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	1524(%rdi), %eax
	movq	192(%rdx), %rdx
	testb	$8, 96(%rdx)
	jne	.L188
	movl	$2, %r8d
	cmpl	$770, %eax
	jg	.L190
.L187:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L212
	addq	$24, %rsp
	movl	%r8d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L188:
	.cfi_restore_state
	cmpl	$256, %eax
	je	.L195
	cmpl	$65277, %eax
	jg	.L195
.L190:
	leaq	-48(%rbp), %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	call	tls12_get_psigalgs@PLT
	movl	$2, %edx
	movl	$13, %esi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L192
	movl	$2, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L192
	movl	$2, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	jne	.L213
	.p2align 4,,10
	.p2align 3
.L192:
	leaq	.LC0(%rip), %r8
	movl	$283, %r9d
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$477, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%r8d, %r8d
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L213:
	movq	-48(%rbp), %rdx
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	tls12_copy_sigalgs@PLT
	testl	%eax, %eax
	je	.L192
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L192
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	movl	$1, %r8d
	testl	%eax, %eax
	jne	.L187
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L195:
	movl	$2, %r8d
	jmp	.L187
.L212:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1622:
	.size	tls_construct_ctos_sig_algs, .-tls_construct_ctos_sig_algs
	.p2align 4
	.globl	tls_construct_ctos_status_request
	.type	tls_construct_ctos_status_request, @function
tls_construct_ctos_status_request:
.LFB1623:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$2, %eax
	testq	%rcx, %rcx
	jne	.L214
	cmpl	$1, 1608(%rdi)
	movq	%rdi, %r12
	je	.L260
.L214:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L261
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L260:
	.cfi_restore_state
	movq	%rsi, %r13
	movl	$2, %edx
	movl	$5, %esi
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L216
	movl	$2, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L216
	movl	$1, %edx
	movl	$1, %esi
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	jne	.L262
	.p2align 4,,10
	.p2align 3
.L216:
	movl	$311, %r9d
.L258:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	$479, %edx
	movq	%r12, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L262:
	movl	$2, %esi
	movq	%r13, %rdi
	leaq	-64(%rbp), %rbx
	xorl	%r14d, %r14d
	call	WPACKET_start_sub_packet_len__@PLT
	movq	%rbx, -72(%rbp)
	testl	%eax, %eax
	jne	.L217
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L220:
	movq	1632(%r12), %rdi
	movl	%r14d, %esi
	call	OPENSSL_sk_value@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	i2d_OCSP_RESPID@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	jle	.L219
	movq	-72(%rbp), %rdx
	movslq	%eax, %rsi
	movl	$2, %ecx
	movq	%r13, %rdi
	call	WPACKET_sub_allocate_bytes__@PLT
	testl	%eax, %eax
	je	.L219
	movq	-72(%rbp), %rsi
	movq	%r15, %rdi
	call	i2d_OCSP_RESPID@PLT
	cmpl	%ebx, %eax
	jne	.L219
	addl	$1, %r14d
.L217:
	movq	1632(%r12), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r14d
	jl	.L220
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L222
	movl	$2, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L222
	movq	1640(%r12), %rdi
	testq	%rdi, %rdi
	je	.L223
	xorl	%esi, %esi
	call	i2d_X509_EXTENSIONS@PLT
	movl	$341, %r9d
	movl	%eax, %ebx
	testl	%eax, %eax
	js	.L258
	leaq	-64(%rbp), %r14
	movslq	%eax, %rsi
	movq	%r13, %rdi
	movq	%r14, %rdx
	call	WPACKET_allocate_bytes@PLT
	testl	%eax, %eax
	je	.L227
	movq	1640(%r12), %rdi
	movq	%r14, %rsi
	call	i2d_X509_EXTENSIONS@PLT
	cmpl	%ebx, %eax
	je	.L223
.L227:
	movl	$349, %r9d
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L219:
	movl	$324, %r9d
	jmp	.L258
.L222:
	movl	$332, %r9d
	jmp	.L258
.L223:
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L229
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L229
	movl	$1, %eax
	jmp	.L214
.L229:
	movl	$356, %r9d
	jmp	.L258
.L261:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1623:
	.size	tls_construct_ctos_status_request, .-tls_construct_ctos_status_request
	.p2align 4
	.globl	tls_construct_ctos_npn
	.type	tls_construct_ctos_npn, @function
tls_construct_ctos_npn:
.LFB1624:
	.cfi_startproc
	endbr64
	movq	1440(%rdi), %rax
	cmpq	$0, 664(%rax)
	je	.L268
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	168(%rdi), %rdx
	movq	%rdi, %r12
	cmpq	$0, 408(%rdx)
	je	.L265
	cmpq	$0, 544(%rdx)
	movl	$2, %eax
	jne	.L263
.L265:
	movl	$2, %edx
	movl	$13172, %esi
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L267
	xorl	%esi, %esi
	movl	$2, %edx
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L267
	movl	$1, %eax
.L263:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L267:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$378, %r9d
	movl	$68, %ecx
	movl	$471, %edx
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L268:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$2, %eax
	ret
	.cfi_endproc
.LFE1624:
	.size	tls_construct_ctos_npn, .-tls_construct_ctos_npn
	.p2align 4
	.globl	tls_construct_ctos_alpn
	.type	tls_construct_ctos_alpn, @function
tls_construct_ctos_alpn:
.LFB1625:
	.cfi_startproc
	endbr64
	movq	168(%rdi), %rax
	cmpq	$0, 1776(%rdi)
	movl	$0, 1024(%rax)
	je	.L282
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	cmpq	$0, 408(%rax)
	movq	%rdi, %r12
	je	.L279
	cmpq	$0, 544(%rax)
	movl	$2, %r8d
	jne	.L277
.L279:
	movl	$2, %edx
	movl	$16, %esi
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L281
	movl	$2, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	jne	.L297
.L281:
	leaq	.LC0(%rip), %r8
	movl	$401, %r9d
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$466, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%r8d, %r8d
.L277:
	popq	%r12
	movl	%r8d, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L297:
	.cfi_restore_state
	movq	1784(%r12), %rdx
	movq	1776(%r12), %rsi
	movl	$2, %ecx
	movq	%r13, %rdi
	call	WPACKET_sub_memcpy__@PLT
	testl	%eax, %eax
	je	.L281
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L281
	movq	168(%r12), %rax
	movl	$1, %r8d
	movl	$1, 1024(%rax)
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L282:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$2, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE1625:
	.size	tls_construct_ctos_alpn, .-tls_construct_ctos_alpn
	.p2align 4
	.globl	tls_construct_ctos_use_srtp
	.type	tls_construct_ctos_use_srtp, @function
tls_construct_ctos_use_srtp:
.LFB1626:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	SSL_get_srtp_profiles@PLT
	testq	%rax, %rax
	je	.L308
	movl	$2, %edx
	movl	$14, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L301
	movl	$2, %esi
	movq	%r12, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	jne	.L327
.L301:
	movl	$427, %r9d
.L326:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	$482, %edx
	movq	%r13, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L298:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L327:
	.cfi_restore_state
	movl	$2, %esi
	movq	%r12, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L301
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	jle	.L302
	xorl	%r15d, %r15d
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L303:
	movq	8(%rax), %rsi
	movl	$2, %edx
	movq	%r12, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L304
	addl	$1, %r15d
	cmpl	%r14d, %r15d
	je	.L302
.L305:
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	testq	%rax, %rax
	jne	.L303
.L304:
	movl	$438, %r9d
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L308:
	addq	$8, %rsp
	movl	$2, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L302:
	.cfi_restore_state
	movq	%r12, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L307
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%r12, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L307
	movq	%r12, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L307
	movl	$1, %eax
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L307:
	movl	$447, %r9d
	jmp	.L326
	.cfi_endproc
.LFE1626:
	.size	tls_construct_ctos_use_srtp, .-tls_construct_ctos_use_srtp
	.p2align 4
	.globl	tls_construct_ctos_etm
	.type	tls_construct_ctos_etm, @function
tls_construct_ctos_etm:
.LFB1627:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	1492(%rdi), %r12d
	andl	$524288, %r12d
	jne	.L332
	movq	%rsi, %r14
	movq	%rdi, %r13
	movl	$2, %edx
	movl	$22, %esi
	movq	%r14, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L331
	xorl	%esi, %esi
	movl	$2, %edx
	movq	%r14, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L331
	addq	$8, %rsp
	movl	$1, %r12d
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L331:
	.cfi_restore_state
	movq	%r13, %rdi
	movl	$464, %r9d
	movl	$68, %ecx
	movl	$469, %edx
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L332:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$2, %r12d
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1627:
	.size	tls_construct_ctos_etm, .-tls_construct_ctos_etm
	.p2align 4
	.globl	tls_construct_ctos_sct
	.type	tls_construct_ctos_sct, @function
tls_construct_ctos_sct:
.LFB1628:
	.cfi_startproc
	endbr64
	cmpq	$0, 1872(%rdi)
	je	.L341
	testq	%rcx, %rcx
	jne	.L341
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movl	$18, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L340
	xorl	%esi, %esi
	movl	$2, %edx
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L340
	popq	%r12
	movl	$1, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L341:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L340:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	%r12, %rdi
	movl	$485, %r9d
	movl	$68, %ecx
	movl	$474, %edx
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1628:
	.size	tls_construct_ctos_sct, .-tls_construct_ctos_sct
	.p2align 4
	.globl	tls_construct_ctos_ems
	.type	tls_construct_ctos_ems, @function
tls_construct_ctos_ems:
.LFB1629:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	movl	$23, %esi
	movq	%r12, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L352
	xorl	%esi, %esi
	movl	$2, %edx
	movq	%r12, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L352
	popq	%r12
	movl	$1, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L352:
	.cfi_restore_state
	movq	%r13, %rdi
	movl	$499, %r9d
	movl	$68, %ecx
	movl	$468, %edx
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1629:
	.size	tls_construct_ctos_ems, .-tls_construct_ctos_ems
	.p2align 4
	.globl	tls_construct_ctos_supported_versions
	.type	tls_construct_ctos_supported_versions, @function
tls_construct_ctos_supported_versions:
.LFB1630:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	leaq	-44(%rbp), %rdx
	.cfi_offset 12, -32
	movq	%rsi, %r12
	leaq	-48(%rbp), %rsi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	ssl_get_min_max_version@PLT
	movl	$515, %r9d
	leaq	.LC0(%rip), %r8
	movl	%eax, %ecx
	testl	%eax, %eax
	jne	.L380
	cmpl	$771, -44(%rbp)
	movl	$2, %eax
	jle	.L357
	movl	$2, %edx
	movl	$43, %esi
	movq	%r12, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L361
	movl	$2, %esi
	movq	%r12, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	jne	.L382
.L361:
	movl	$530, %r9d
.L381:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
.L380:
	movl	$481, %edx
	movl	$80, %esi
	movq	%r13, %rdi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L357:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L383
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L382:
	.cfi_restore_state
	movl	$1, %esi
	movq	%r12, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L361
	movl	-44(%rbp), %ebx
	cmpl	-48(%rbp), %ebx
	jge	.L364
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L363:
	subl	$1, %ebx
	cmpl	%ebx, -48(%rbp)
	jg	.L362
.L364:
	movl	$2, %edx
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	jne	.L363
	movl	$538, %r9d
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L362:
	movq	%r12, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L366
	movq	%r12, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L366
	movl	$1, %eax
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L366:
	movl	$545, %r9d
	jmp	.L381
.L383:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1630:
	.size	tls_construct_ctos_supported_versions, .-tls_construct_ctos_supported_versions
	.p2align 4
	.globl	tls_construct_ctos_psk_kex_modes
	.type	tls_construct_ctos_psk_kex_modes, @function
tls_construct_ctos_psk_kex_modes:
.LFB1631:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	movl	$45, %esi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movl	1492(%rdi), %ebx
	movq	%r12, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L387
	movl	$2, %esi
	movq	%r12, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	jne	.L412
.L387:
	movl	$571, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r13, %rdi
	movl	$509, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L384:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L412:
	.cfi_restore_state
	movl	$1, %esi
	movq	%r12, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L387
	movl	$1, %edx
	movl	$1, %esi
	movq	%r12, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L387
	andl	$1024, %ebx
	je	.L390
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%r12, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L387
.L390:
	movq	%r12, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L387
	movq	%r12, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L387
	testl	%ebx, %ebx
	jne	.L391
	movl	$2, 1808(%r13)
	movl	$1, %eax
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L391:
	movl	$3, 1808(%r13)
	movl	$1, %eax
	jmp	.L384
	.cfi_endproc
.LFE1631:
	.size	tls_construct_ctos_psk_kex_modes, .-tls_construct_ctos_psk_kex_modes
	.p2align 4
	.globl	tls_construct_ctos_key_share
	.type	tls_construct_ctos_key_share, @function
tls_construct_ctos_key_share:
.LFB1633:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$51, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L416
	movl	$2, %esi
	movq	%r12, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	jne	.L455
.L416:
	movl	$658, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
.L453:
	movl	$470, %edx
	movl	$80, %esi
	movq	%r13, %rdi
	call	ossl_statem_fatal@PLT
.L454:
	xorl	%eax, %eax
.L413:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L456
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L455:
	.cfi_restore_state
	movl	$2, %esi
	movq	%r12, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L416
	leaq	-80(%rbp), %rdx
	leaq	-72(%rbp), %rsi
	movq	%r13, %rdi
	call	tls1_get_supported_groups@PLT
	movq	168(%r13), %rax
	movzwl	1030(%rax), %ebx
	testw	%bx, %bx
	jne	.L417
	xorl	%ebx, %ebx
	cmpq	$0, -80(%rbp)
	je	.L421
.L420:
	movq	-72(%rbp), %rax
	movl	$131076, %edx
	movq	%r13, %rdi
	leaq	(%rbx,%rbx), %r14
	movzwl	(%rax,%rbx,2), %esi
	call	tls_curve_allowed@PLT
	testl	%eax, %eax
	je	.L457
	movq	-72(%rbp), %rax
	movzwl	(%rax,%r14), %ebx
	testw	%bx, %bx
	jne	.L458
.L421:
	movl	$683, %r9d
	leaq	.LC0(%rip), %r8
	movl	$101, %ecx
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L458:
	movq	168(%r13), %rax
.L417:
	movq	576(%rax), %r14
	movq	$0, -64(%rbp)
	testq	%r14, %r14
	je	.L422
	cmpl	$1, 1248(%r13)
	jne	.L459
.L423:
	leaq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	EVP_PKEY_get1_tls_encodedpoint@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L460
	movzwl	%bx, %esi
	movl	$2, %edx
	movq	%r12, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L428
	movq	-64(%rbp), %rsi
	movl	$2, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	WPACKET_sub_memcpy__@PLT
	testl	%eax, %eax
	jne	.L461
.L428:
	movl	$620, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r13, %rdi
	movl	$512, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
.L426:
	movq	168(%r13), %rax
	cmpq	$0, 576(%rax)
	je	.L462
.L430:
	movq	-64(%rbp), %rdi
	movl	$638, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	xorl	%eax, %eax
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L460:
	movl	$613, %r9d
	leaq	.LC0(%rip), %r8
	movl	$16, %ecx
	movq	%r13, %rdi
	movl	$512, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L461:
	movq	168(%r13), %rax
	movq	-64(%rbp), %rdi
	movl	$632, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, 576(%rax)
	movw	%bx, 1030(%rax)
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L431
	movq	%r12, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L431
	movl	$1, %eax
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L457:
	addq	$1, %rbx
	cmpq	%rbx, -80(%rbp)
	ja	.L420
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L431:
	movl	$694, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L459:
	movl	$593, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r13, %rdi
	movl	$512, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L422:
	movzwl	%bx, %esi
	movq	%r13, %rdi
	call	ssl_generate_pkey_group@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L423
	jmp	.L454
.L462:
	movq	%r14, %rdi
	call	EVP_PKEY_free@PLT
	jmp	.L430
.L456:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1633:
	.size	tls_construct_ctos_key_share, .-tls_construct_ctos_key_share
	.p2align 4
	.globl	tls_construct_ctos_cookie
	.type	tls_construct_ctos_cookie, @function
tls_construct_ctos_cookie:
.LFB1634:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	$2, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	cmpq	$0, 1832(%rdi)
	je	.L463
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movl	$2, %edx
	movl	$44, %esi
	movq	%r12, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L467
	movl	$2, %esi
	movq	%r12, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L467
	movq	1832(%rbx), %rdx
	movq	1824(%rbx), %rsi
	movl	$2, %ecx
	movq	%r12, %rdi
	call	WPACKET_sub_memcpy__@PLT
	testl	%eax, %eax
	jne	.L480
	.p2align 4,,10
	.p2align 3
.L467:
	movl	$68, %ecx
	movl	$535, %edx
	movq	%rbx, %rdi
	xorl	%r13d, %r13d
	movl	$719, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
.L466:
	movq	1824(%rbx), %rdi
	movl	$726, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 1824(%rbx)
	movq	$0, 1832(%rbx)
.L463:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L480:
	.cfi_restore_state
	movq	%r12, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L467
	movl	$1, %r13d
	jmp	.L466
	.cfi_endproc
.LFE1634:
	.size	tls_construct_ctos_cookie, .-tls_construct_ctos_cookie
	.p2align 4
	.globl	tls_construct_ctos_early_data
	.type	tls_construct_ctos_early_data, @function
tls_construct_ctos_early_data:
.LFB1635:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$456, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$1, 1248(%rdi)
	movq	$0, -496(%rbp)
	movq	$0, -488(%rbp)
	movq	$0, -480(%rbp)
	je	.L574
	movq	1432(%rdi), %r9
	xorl	%esi, %esi
	testq	%r9, %r9
	je	.L490
.L516:
	movq	%r12, %rdi
	leaq	-488(%rbp), %rcx
	leaq	-496(%rbp), %rdx
	leaq	-480(%rbp), %r8
	call	*%r9
	movq	-480(%rbp), %rdi
	testl	%eax, %eax
	je	.L485
	testq	%rdi, %rdi
	je	.L490
	cmpl	$772, (%rdi)
	jne	.L485
.L500:
	movq	1304(%r12), %rdi
	call	SSL_SESSION_free@PLT
	movq	-480(%rbp), %rax
	movq	%rax, 1304(%r12)
	testq	%rax, %rax
	je	.L501
	movq	1312(%r12), %rdi
	movl	$816, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-488(%rbp), %rsi
	movq	-496(%rbp), %rdi
	movl	$817, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_memdup@PLT
	movl	$819, %r9d
	movl	$68, %ecx
	leaq	.LC0(%rip), %r8
	movq	%rax, 1312(%r12)
	testq	%rax, %rax
	je	.L571
	movq	-488(%rbp), %rax
	movq	%rax, 1320(%r12)
.L501:
	cmpl	$2, 132(%r12)
	jne	.L503
	movq	1296(%r12), %rbx
	movl	580(%rbx), %eax
	testl	%eax, %eax
	jne	.L504
	movq	-480(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L503
	movl	580(%rbx), %eax
	testl	%eax, %eax
	je	.L503
.L504:
	movq	544(%rbx), %rsi
	movl	%eax, 6176(%r12)
	testq	%rsi, %rsi
	je	.L505
	movq	1600(%r12), %rdi
	testq	%rdi, %rdi
	je	.L506
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L505
.L506:
	movl	$839, %r9d
	leaq	.LC0(%rip), %r8
	movl	$231, %ecx
	jmp	.L571
	.p2align 4,,10
	.p2align 3
.L485:
	call	SSL_SESSION_free@PLT
	movl	$754, %r9d
	movl	$219, %ecx
	leaq	.LC0(%rip), %r8
.L571:
	movl	$530, %edx
	movl	$80, %esi
	movq	%r12, %rdi
	call	ossl_statem_fatal@PLT
.L573:
	xorl	%eax, %eax
.L481:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L575
	addq	$456, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L574:
	.cfi_restore_state
	call	ssl_handshake_md@PLT
	movq	1432(%r12), %r9
	movq	%rax, %rsi
	testq	%r9, %r9
	jne	.L516
	cmpq	$0, -480(%rbp)
	jne	.L500
	.p2align 4,,10
	.p2align 3
.L490:
	movq	1408(%r12), %r10
	testq	%r10, %r10
	je	.L500
	leaq	-464(%rbp), %r15
	xorl	%eax, %eax
	movl	$16, %ecx
	xorl	%esi, %esi
	movq	%r15, %rdi
	leaq	-320(%rbp), %r14
	movl	$256, %r9d
	movq	%r15, %rdx
	rep stosq
	movq	%r14, %r8
	movl	$128, %ecx
	movb	$0, (%rdi)
	movq	%r12, %rdi
	call	*%r10
	movl	%eax, %ebx
	cmpl	$256, %eax
	ja	.L576
	testq	%rbx, %rbx
	je	.L500
	movl	$275, %eax
	movq	%r15, %rdx
	movw	%ax, -466(%rbp)
.L494:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L494
	movl	%eax, %ecx
	leaq	-466(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r15, -496(%rbp)
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	cmove	%rcx, %rdx
	movl	%eax, %ecx
	addb	%al, %cl
	sbbq	$3, %rdx
	subq	%r15, %rdx
	movq	%rdx, -488(%rbp)
	call	SSL_CIPHER_find@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L577
	call	SSL_SESSION_new@PLT
	movq	%rax, -480(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L499
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	SSL_SESSION_set1_master_key@PLT
	testl	%eax, %eax
	je	.L499
	movq	-480(%rbp), %rdi
	movq	%r15, %rsi
	call	SSL_SESSION_set_cipher@PLT
	testl	%eax, %eax
	je	.L499
	movq	-480(%rbp), %rdi
	movl	$772, %esi
	call	SSL_SESSION_set_protocol_version@PLT
	testl	%eax, %eax
	je	.L499
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	OPENSSL_cleanse@PLT
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L503:
	movl	$0, 6176(%r12)
	movl	$2, %eax
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L505:
	movq	1776(%r12), %r15
	movq	584(%rbx), %rax
	testq	%r15, %r15
	je	.L578
	testq	%rax, %rax
	je	.L508
	movq	1784(%r12), %r14
	testq	%r14, %r14
	js	.L509
	je	.L510
.L511:
	movzbl	(%r15), %edx
	subq	$1, %r14
	cmpq	%rdx, %r14
	jb	.L510
	leaq	1(%r15), %rdi
	subq	%rdx, %r14
	leaq	(%rdi,%rdx), %r15
	cmpq	%rdx, 592(%rbx)
	je	.L579
.L512:
	testq	%r14, %r14
	jne	.L511
.L510:
	movl	$873, %r9d
.L572:
	leaq	.LC0(%rip), %r8
	movl	$222, %ecx
	jmp	.L571
	.p2align 4,,10
	.p2align 3
.L578:
	movl	$847, %r9d
	testq	%rax, %rax
	jne	.L572
.L508:
	movl	$2, %edx
	movl	$42, %esi
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L515
	movl	$2, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	jne	.L580
.L515:
	movl	$883, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	jmp	.L571
	.p2align 4,,10
	.p2align 3
.L580:
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L515
	movabsq	$4294967297, %rax
	movq	%rax, 1816(%r12)
	movl	$1, %eax
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L509:
	movl	$861, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	jmp	.L571
	.p2align 4,,10
	.p2align 3
.L576:
	movl	$769, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$530, %edx
	movl	$40, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L499:
	movq	%r12, %rdi
	movl	$802, %r9d
	movl	$68, %ecx
	movl	$530, %edx
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	OPENSSL_cleanse@PLT
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L579:
	movq	584(%rbx), %rsi
	call	CRYPTO_memcmp@PLT
	testl	%eax, %eax
	jne	.L512
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L577:
	movl	$791, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$530, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L573
.L575:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1635:
	.size	tls_construct_ctos_early_data, .-tls_construct_ctos_early_data
	.p2align 4
	.globl	tls_construct_ctos_padding
	.type	tls_construct_ctos_padding, @function
tls_construct_ctos_padding:
.LFB1636:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	$2, %r14d
	pushq	%r13
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testb	$16, 1492(%rdi)
	je	.L581
	movq	%rsi, %r13
	movq	%rdi, %r12
	leaq	-48(%rbp), %rsi
	movq	%r13, %rdi
	call	WPACKET_get_total_written@PLT
	movl	$932, %r9d
	testl	%eax, %eax
	je	.L604
	movq	1296(%r12), %rax
	cmpl	$772, (%rax)
	je	.L584
.L603:
	movq	-48(%rbp), %rax
.L585:
	leaq	-256(%rax), %rdx
	movl	$1, %r14d
	cmpq	$255, %rdx
	ja	.L581
	movl	$512, %ecx
	movl	$508, %edx
	movl	$21, %esi
	movq	%r13, %rdi
	subq	%rax, %rdx
	subq	%rax, %rcx
	cmpq	$4, %rcx
	movq	%rdx, %rax
	movl	$1, %edx
	cmovbe	%rdx, %rax
	movl	$2, %edx
	movq	%rax, -48(%rbp)
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L590
	movq	-48(%rbp), %rsi
	leaq	-56(%rbp), %rdx
	movl	$2, %ecx
	movq	%r13, %rdi
	call	WPACKET_sub_allocate_bytes__@PLT
	testl	%eax, %eax
	je	.L590
	movq	-48(%rbp), %rdx
	movq	-56(%rbp), %rdi
	xorl	%esi, %esi
	call	memset@PLT
	.p2align 4,,10
	.p2align 3
.L581:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L605
	addq	$40, %rsp
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L590:
	.cfi_restore_state
	movl	$973, %r9d
.L604:
	movl	$68, %ecx
	movl	$472, %edx
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L584:
	cmpq	$0, 560(%rax)
	je	.L603
	movq	504(%rax), %rax
	testq	%rax, %rax
	je	.L603
	movl	64(%rax), %edi
	call	ssl_md@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L603
	movq	1296(%r12), %rax
	movq	560(%rax), %r14
	call	EVP_MD_size@PLT
	movslq	%eax, %rdx
	movq	-48(%rbp), %rax
	leaq	15(%r14,%rax), %rax
	addq	%rdx, %rax
	jmp	.L585
.L605:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1636:
	.size	tls_construct_ctos_padding, .-tls_construct_ctos_padding
	.p2align 4
	.globl	tls_construct_ctos_psk
	.type	tls_construct_ctos_psk, @function
tls_construct_ctos_psk:
.LFB1637:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	1296(%rdi), %rax
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movl	$0, 1848(%rdi)
	cmpl	$772, (%rax)
	jne	.L610
	movq	560(%rax), %r13
	movq	%rdi, %r15
	movq	%rsi, %r12
	testq	%r13, %r13
	jne	.L609
	movq	1304(%rdi), %r14
	testq	%r14, %r14
	je	.L610
	cmpl	$1, 1248(%rdi)
	je	.L641
	movl	$0, -112(%rbp)
	xorl	%ebx, %ebx
	movq	$0, -104(%rbp)
	movl	$0, -108(%rbp)
.L619:
	movq	504(%r14), %rax
	movl	64(%rax), %edi
	call	ssl_md@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L715
	cmpl	$1, 1248(%r15)
	jne	.L622
	movl	$1111, %r9d
	cmpq	%rbx, %rax
	je	.L622
.L714:
	leaq	.LC0(%rip), %r8
	movl	$219, %ecx
	jmp	.L713
	.p2align 4,,10
	.p2align 3
.L609:
	cmpl	$1, 1248(%rdi)
	je	.L641
	xorl	%ebx, %ebx
.L642:
	movq	504(%rax), %rax
	movl	$1018, %r9d
	movl	$68, %ecx
	leaq	.LC0(%rip), %r8
	testq	%rax, %rax
	je	.L713
	movl	64(%rax), %edi
	call	ssl_md@PLT
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L712
	cmpl	$1, 1248(%r15)
	jne	.L616
	cmpq	-104(%rbp), %rbx
	jne	.L712
.L616:
	xorl	%edi, %edi
	call	time@PLT
	movq	1296(%r15), %rdx
	subl	488(%rdx), %eax
	movl	%eax, -108(%rbp)
	je	.L617
	subl	$1, %eax
	movl	%eax, %ecx
	cmpq	%rax, 568(%rdx)
	jnb	.L618
.L712:
	movl	$0, -108(%rbp)
	movq	1304(%r15), %r14
.L615:
	testq	%r14, %r14
	jne	.L716
	.p2align 4,,10
	.p2align 3
.L610:
	movl	$2, %eax
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L622:
	movq	%r14, %rdi
	call	EVP_MD_size@PLT
	movslq	%eax, %rbx
.L620:
	movl	$2, %edx
	movl	$41, %esi
	movq	%r12, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L624
	movl	$2, %esi
	movq	%r12, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	jne	.L717
.L624:
	movl	$1123, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
.L713:
	movl	$501, %edx
	movl	$80, %esi
	movq	%r15, %rdi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L606:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L718
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L717:
	.cfi_restore_state
	movl	$2, %esi
	movq	%r12, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L624
	movl	-112(%rbp), %edi
	testl	%edi, %edi
	jne	.L719
.L625:
	cmpq	$0, 1304(%r15)
	je	.L628
	movq	1320(%r15), %rdx
	movq	1312(%r15), %rsi
	movl	$2, %ecx
	movq	%r12, %rdi
	call	WPACKET_sub_memcpy__@PLT
	testl	%eax, %eax
	je	.L630
	xorl	%esi, %esi
	movl	$4, %edx
	movq	%r12, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L630
	addl	$1, 1848(%r15)
.L628:
	movq	%r12, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L632
	leaq	-88(%rbp), %rsi
	movq	%r12, %rdi
	call	WPACKET_get_total_written@PLT
	testl	%eax, %eax
	je	.L632
	movl	$2, %esi
	movq	%r12, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L632
	movl	-112(%rbp), %esi
	testl	%esi, %esi
	jne	.L633
.L636:
	cmpq	$0, 1304(%r15)
	je	.L635
	leaq	-64(%rbp), %rdx
	movl	$1, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	WPACKET_sub_allocate_bytes__@PLT
	testl	%eax, %eax
	je	.L632
.L635:
	movq	%r12, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L632
	movq	%r12, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L632
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	call	WPACKET_get_total_written@PLT
	testl	%eax, %eax
	je	.L632
	movq	%r12, %rdi
	call	WPACKET_fill_lengths@PLT
	testl	%eax, %eax
	je	.L632
	movq	%r12, %rdi
	call	WPACKET_get_curr@PLT
	subq	-80(%rbp), %rax
	cmpl	$0, -112(%rbp)
	movq	%rax, %r12
	jne	.L638
.L640:
	movq	1304(%r15), %rax
	testq	%rax, %rax
	je	.L720
	pushq	%rdx
	movq	-64(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	pushq	$1
	movq	-88(%rbp), %rcx
	movq	%r14, %rsi
	movq	%r15, %rdi
	pushq	$1
	pushq	%rax
	call	tls_psk_do_binder@PLT
	addq	$32, %rsp
	subl	$1, %eax
	sete	%al
	movzbl	%al, %eax
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L617:
	movq	-104(%rbp), %rdi
	movl	576(%rdx), %ecx
	addl	%ecx, -108(%rbp)
	call	EVP_MD_size@PLT
	movq	1304(%r15), %r14
	addl	$1, 1848(%r15)
	movl	$1, -112(%rbp)
	movslq	%eax, %r13
	testq	%r14, %r14
	jne	.L619
	xorl	%ebx, %ebx
	jmp	.L620
	.p2align 4,,10
	.p2align 3
.L641:
	movq	%r15, %rdi
	call	ssl_handshake_md@PLT
	movq	%rax, %rbx
	movq	1296(%r15), %rax
	cmpq	$0, 560(%rax)
	jne	.L642
	movq	$0, -104(%rbp)
	movq	1304(%r15), %r14
	movl	$0, -108(%rbp)
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L716:
	movl	$0, -112(%rbp)
	xorl	%r13d, %r13d
	jmp	.L619
	.p2align 4,,10
	.p2align 3
.L630:
	movl	$1142, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	jmp	.L713
.L633:
	leaq	-72(%rbp), %rdx
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	WPACKET_sub_allocate_bytes__@PLT
	testl	%eax, %eax
	jne	.L636
	.p2align 4,,10
	.p2align 3
.L632:
	movl	$1164, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	jmp	.L713
	.p2align 4,,10
	.p2align 3
.L715:
	movl	$1101, %r9d
	jmp	.L714
	.p2align 4,,10
	.p2align 3
.L719:
	movq	1296(%r15), %rax
	movl	$2, %ecx
	movq	%r12, %rdi
	movq	560(%rax), %rdx
	movq	552(%rax), %rsi
	call	WPACKET_sub_memcpy__@PLT
	testl	%eax, %eax
	je	.L627
	movl	-108(%rbp), %esi
	movl	$4, %edx
	movq	%r12, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	jne	.L625
.L627:
	movl	$1132, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	jmp	.L713
	.p2align 4,,10
	.p2align 3
.L618:
	imull	$1000, %eax, %eax
	movl	%eax, -108(%rbp)
	testl	%ecx, %ecx
	je	.L617
	imulq	$274877907, %rax, %rax
	shrq	$38, %rax
	cmpl	%ecx, %eax
	je	.L617
	movq	1304(%r15), %r14
	jmp	.L615
.L718:
	call	__stack_chk_fail@PLT
.L720:
	movl	$1, %eax
	jmp	.L606
.L638:
	pushq	%rcx
	movq	-72(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%rax, %rdx
	movq	-88(%rbp), %rcx
	movq	-104(%rbp), %rsi
	pushq	$0
	movq	%r15, %rdi
	pushq	$1
	pushq	1296(%r15)
	call	tls_psk_do_binder@PLT
	addq	$32, %rsp
	movl	%eax, %r8d
	xorl	%eax, %eax
	subl	$1, %r8d
	jne	.L606
	jmp	.L640
	.cfi_endproc
.LFE1637:
	.size	tls_construct_ctos_psk, .-tls_construct_ctos_psk
	.p2align 4
	.globl	tls_construct_ctos_post_handshake_auth
	.type	tls_construct_ctos_post_handshake_auth, @function
tls_construct_ctos_post_handshake_auth:
.LFB1638:
	.cfi_startproc
	endbr64
	movl	1940(%rdi), %edx
	movl	$2, %eax
	testl	%edx, %edx
	je	.L733
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movl	$49, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L724
	movl	$2, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L724
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L724
	movl	$1, 1936(%r12)
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L724:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$1203, %r9d
	movl	$68, %ecx
	movl	$619, %edx
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L733:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE1638:
	.size	tls_construct_ctos_post_handshake_auth, .-tls_construct_ctos_post_handshake_auth
	.p2align 4
	.globl	tls_parse_stoc_renegotiate
	.type	tls_parse_stoc_renegotiate, @function
tls_parse_stoc_renegotiate:
.LFB1639:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	168(%rdi), %r13
	movq	904(%r13), %rax
	movq	976(%r13), %rdx
	movq	%rax, %rcx
	addq	%rdx, %rcx
	je	.L737
	testq	%rax, %rax
	je	.L748
	testq	%rdx, %rdx
	je	.L748
.L737:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L740
	movq	(%rbx), %rdi
	subq	$1, %rax
	movzbl	(%rdi), %edx
	addq	$1, %rdi
	movq	%rax, 8(%rbx)
	movq	%rdi, (%rbx)
	cmpq	%rdx, %rax
	jne	.L759
	movl	$1255, %r9d
	cmpq	%rax, %rcx
	jne	.L758
	movq	904(%r13), %rax
	cmpq	%rcx, %rax
	ja	.L745
	leaq	(%rdi,%rax), %r8
	subq	%rax, %rcx
	leaq	840(%r13), %rsi
	movq	%rcx, 8(%rbx)
	movq	904(%r13), %rdx
	movq	%rcx, %r14
	movq	%r8, (%rbx)
	movq	%r8, -56(%rbp)
	call	memcmp@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	jne	.L745
	movq	976(%r13), %rax
	movq	-56(%rbp), %r8
	cmpq	%r14, %rax
	ja	.L747
	movq	%r14, %rcx
	leaq	(%r8,%rax), %rdx
	leaq	912(%r13), %rsi
	movq	%r8, %rdi
	subq	%rax, %rcx
	movq	%rdx, (%rbx)
	movq	%rcx, 8(%rbx)
	movq	976(%r13), %rdx
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L747
	movl	$1, 984(%r13)
	movl	$1, %r15d
	jmp	.L736
	.p2align 4,,10
	.p2align 3
.L740:
	movl	$1241, %r9d
.L757:
	movl	$336, %ecx
	movl	$448, %edx
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	leaq	.LC0(%rip), %r8
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
.L736:
	addq	$24, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L748:
	.cfi_restore_state
	movl	$68, %ecx
	movl	$448, %edx
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	movl	$1234, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L736
	.p2align 4,,10
	.p2align 3
.L745:
	movl	$1263, %r9d
.L758:
	movl	$337, %ecx
	movl	$448, %edx
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	leaq	.LC0(%rip), %r8
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L736
	.p2align 4,,10
	.p2align 3
.L747:
	movl	$1271, %r9d
	leaq	.LC0(%rip), %r8
	movl	$337, %ecx
	movq	%r12, %rdi
	movl	$448, %edx
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L736
	.p2align 4,,10
	.p2align 3
.L759:
	movl	$1248, %r9d
	jmp	.L757
	.cfi_endproc
.LFE1639:
	.size	tls_parse_stoc_renegotiate, .-tls_parse_stoc_renegotiate
	.p2align 4
	.globl	tls_parse_stoc_maxfragmentlen
	.type	tls_parse_stoc_maxfragmentlen, @function
tls_parse_stoc_maxfragmentlen:
.LFB1640:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpq	$1, 8(%rsi)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	jne	.L761
	movq	(%rsi), %rax
	movzbl	(%rax), %edx
	addq	$1, %rax
	movq	$0, 8(%rsi)
	movq	%rax, (%rsi)
	movzbl	%dl, %eax
	subl	$1, %eax
	cmpl	$3, %eax
	ja	.L769
	cmpb	%dl, 1844(%rdi)
	jne	.L770
	movq	1296(%rdi), %rax
	movb	%dl, 600(%rax)
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L761:
	.cfi_restore_state
	movl	$1287, %r9d
	movl	$110, %ecx
	movl	$581, %edx
	movl	$50, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L770:
	.cfi_restore_state
	movl	$1307, %r9d
.L768:
	movl	$232, %ecx
	movl	$581, %edx
	movl	$47, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L769:
	.cfi_restore_state
	movl	$1294, %r9d
	jmp	.L768
	.cfi_endproc
.LFE1640:
	.size	tls_parse_stoc_maxfragmentlen, .-tls_parse_stoc_maxfragmentlen
	.p2align 4
	.globl	tls_parse_stoc_server_name
	.type	tls_parse_stoc_server_name, @function
tls_parse_stoc_server_name:
.LFB1641:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	1600(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L780
	cmpq	$0, 8(%rsi)
	jne	.L781
	movl	200(%r12), %r13d
	testl	%r13d, %r13d
	je	.L775
.L777:
	movl	$1, %r13d
.L771:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L775:
	.cfi_restore_state
	movq	1296(%r12), %rbx
	movl	$1339, %r9d
	cmpq	$0, 544(%rbx)
	je	.L782
.L779:
	movq	%r12, %rdi
	movl	$68, %ecx
	movl	$583, %edx
	movl	$80, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L782:
	.cfi_restore_state
	movl	$1343, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, 544(%rbx)
	movq	1296(%r12), %rax
	cmpq	$0, 544(%rax)
	jne	.L777
	movl	$1345, %r9d
	jmp	.L779
	.p2align 4,,10
	.p2align 3
.L781:
	movq	%r12, %rdi
	movl	$110, %ecx
	movl	$583, %edx
	xorl	%r13d, %r13d
	movl	$1332, %r9d
	leaq	.LC0(%rip), %r8
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L780:
	.cfi_restore_state
	movl	$68, %ecx
	movl	$583, %edx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	movl	$1326, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L771
	.cfi_endproc
.LFE1641:
	.size	tls_parse_stoc_server_name, .-tls_parse_stoc_server_name
	.p2align 4
	.globl	tls_parse_stoc_ec_pt_formats
	.type	tls_parse_stoc_ec_pt_formats, @function
tls_parse_stoc_ec_pt_formats:
.LFB1642:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	8(%rsi), %rax
	testq	%rax, %rax
	je	.L784
	movq	(%rsi), %r14
	leaq	-1(%rax), %rbx
	movzbl	(%r14), %edx
	cmpq	%rdx, %rbx
	je	.L800
.L784:
	movl	$110, %ecx
	movl	$580, %edx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	movl	$1362, %r9d
	leaq	.LC0(%rip), %r8
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
.L783:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L800:
	.cfi_restore_state
	leaq	1(%r14), %r15
	movl	200(%rdi), %r13d
	movq	$0, 8(%rsi)
	leaq	(%r15,%rbx), %rax
	movq	%rax, (%rsi)
	testl	%r13d, %r13d
	jne	.L790
	testq	%rbx, %rbx
	je	.L801
	movq	$0, 1688(%rdi)
	movq	1696(%rdi), %rdi
	movl	$1375, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$1376, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rbx, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, 1696(%r12)
	testq	%rax, %rax
	je	.L802
	movq	%rbx, 1688(%r12)
	cmpl	$8, %ebx
	jnb	.L789
	testb	$4, %bl
	jne	.L803
	testl	%ebx, %ebx
	je	.L790
	movzbl	1(%r14), %edx
	movb	%dl, (%rax)
	testb	$2, %bl
	je	.L790
	movl	%ebx, %ebx
	movzwl	-2(%r15,%rbx), %edx
	movw	%dx, -2(%rax,%rbx)
	.p2align 4,,10
	.p2align 3
.L790:
	movl	$1, %r13d
	jmp	.L783
	.p2align 4,,10
	.p2align 3
.L789:
	movq	1(%r14), %rdx
	leaq	8(%rax), %rdi
	movq	%r15, %rsi
	andq	$-8, %rdi
	movq	%rdx, (%rax)
	movl	%ebx, %edx
	movq	-8(%r15,%rdx), %rcx
	movq	%rcx, -8(%rax,%rdx)
	subq	%rdi, %rax
	leal	(%rbx,%rax), %ecx
	subq	%rax, %rsi
	movl	%ecx, %eax
	shrl	$3, %eax
	movl	%eax, %ecx
	rep movsq
	jmp	.L790
	.p2align 4,,10
	.p2align 3
.L801:
	movl	$1369, %r9d
	movl	$271, %ecx
	movl	$580, %edx
	movl	$50, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	jmp	.L783
.L802:
	movl	$1378, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$580, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L783
.L803:
	movl	1(%r14), %edx
	movl	%ebx, %ebx
	movl	%edx, (%rax)
	movl	-4(%r15,%rbx), %edx
	movl	%edx, -4(%rax,%rbx)
	jmp	.L790
	.cfi_endproc
.LFE1642:
	.size	tls_parse_stoc_ec_pt_formats, .-tls_parse_stoc_ec_pt_formats
	.p2align 4
	.globl	tls_parse_stoc_session_ticket
	.type	tls_parse_stoc_session_ticket, @function
tls_parse_stoc_session_ticket:
.LFB1643:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	1744(%rdi), %rax
	testq	%rax, %rax
	je	.L805
	movl	8(%rsi), %edx
	movq	1752(%rdi), %rcx
	movq	(%rsi), %rsi
	call	*%rax
	testl	%eax, %eax
	je	.L813
.L805:
	movq	%r13, %rdi
	call	tls_use_ticket@PLT
	testl	%eax, %eax
	je	.L814
	cmpq	$0, 8(%rbx)
	jne	.L815
	movl	$1, 1664(%r13)
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L814:
	.cfi_restore_state
	movq	%r13, %rdi
	movl	$110, %ecx
	movl	$584, %edx
	movl	%eax, -20(%rbp)
	movl	$1411, %r9d
	leaq	.LC0(%rip), %r8
	movl	$110, %esi
	call	ossl_statem_fatal@PLT
	movl	-20(%rbp), %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L813:
	.cfi_restore_state
	movq	%r13, %rdi
	movl	$110, %ecx
	movl	$584, %edx
	movl	%eax, -20(%rbp)
	movl	$1405, %r9d
	leaq	.LC0(%rip), %r8
	movl	$40, %esi
	call	ossl_statem_fatal@PLT
	movl	-20(%rbp), %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L815:
	.cfi_restore_state
	movq	%r13, %rdi
	movl	$1416, %r9d
	movl	$110, %ecx
	movl	$584, %edx
	leaq	.LC0(%rip), %r8
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1643:
	.size	tls_parse_stoc_session_ticket, .-tls_parse_stoc_session_ticket
	.p2align 4
	.globl	tls_parse_stoc_status_request
	.type	tls_parse_stoc_status_request, @function
tls_parse_stoc_status_request:
.LFB1644:
	.cfi_startproc
	endbr64
	cmpl	$16384, %edx
	je	.L825
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	1608(%rdi), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$1, %eax
	jne	.L837
	movq	8(%rdi), %rdx
	movq	192(%rdx), %rcx
	testb	$8, 96(%rcx)
	je	.L838
	cmpq	$0, 8(%rsi)
	jne	.L823
.L822:
	movl	$1, 1628(%rdi)
.L816:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L838:
	.cfi_restore_state
	movl	(%rdx), %edx
	cmpl	$65536, %edx
	je	.L820
	cmpl	$771, %edx
	jle	.L820
.L821:
	testq	%r8, %r8
	jne	.L816
	popq	%rbp
	.cfi_remember_state
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	tls_process_cert_status_body@PLT
	.p2align 4,,10
	.p2align 3
.L837:
	.cfi_restore_state
	movl	$1441, %r9d
	movl	$110, %ecx
	movl	$585, %edx
	movl	$110, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L825:
	.cfi_restore 6
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L820:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	cmpq	$0, 8(%rsi)
	jne	.L823
	cmpl	$771, %edx
	jle	.L822
	cmpl	$65536, %edx
	je	.L822
	jmp	.L821
	.p2align 4,,10
	.p2align 3
.L823:
	movl	$1446, %r9d
	movl	$110, %ecx
	movl	$585, %edx
	movl	$50, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1644:
	.size	tls_parse_stoc_status_request, .-tls_parse_stoc_status_request
	.p2align 4
	.globl	tls_parse_stoc_sct
	.type	tls_parse_stoc_sct, @function
tls_parse_stoc_sct:
.LFB1645:
	.cfi_startproc
	endbr64
	cmpl	$16384, %edx
	je	.L858
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	cmpq	$0, 1872(%rdi)
	je	.L842
	movq	8(%rsi), %r13
	movq	1616(%rdi), %rdi
	movl	$1489, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 1616(%r12)
	movw	%r13w, 1624(%r12)
	testq	%r13, %r13
	jne	.L859
.L857:
	movl	$1, %eax
.L839:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L842:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	1168(%rdi), %rax
	testb	$1, %dh
	movl	%edx, %r13d
	sete	%sil
	movq	%rcx, %r15
	movl	$18, %edx
	xorl	%ecx, %ecx
	addl	%esi, %esi
	leaq	472(%rax), %rdi
	movq	%r8, %r14
	call	custom_ext_find@PLT
	testq	%rax, %rax
	je	.L860
	subq	$8, %rsp
	movq	(%rbx), %rcx
	movq	%r15, %r9
	movl	$18, %edx
	pushq	%r14
	movq	8(%rbx), %r8
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	custom_ext_parse@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	setne	%al
	leaq	-40(%rbp), %rsp
	popq	%rbx
	movzbl	%al, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L859:
	.cfi_restore_state
	movq	%r13, %rdi
	movl	$1494, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, 1616(%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L844
	cmpq	%r13, 8(%rbx)
	jb	.L844
	movq	(%rbx), %rsi
	movq	%r13, %rdx
	call	memcpy@PLT
	addq	%r13, (%rbx)
	subq	%r13, 8(%rbx)
	jmp	.L857
	.p2align 4,,10
	.p2align 3
.L858:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L844:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$1497, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$564, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L839
	.p2align 4,,10
	.p2align 3
.L860:
	movl	$1513, %r9d
	leaq	.LC0(%rip), %r8
	movl	$110, %ecx
	movq	%r12, %rdi
	movl	$564, %edx
	movl	$110, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L839
	.cfi_endproc
.LFE1645:
	.size	tls_parse_stoc_sct, .-tls_parse_stoc_sct
	.p2align 4
	.globl	tls_parse_stoc_npn
	.type	tls_parse_stoc_npn, @function
tls_parse_stoc_npn:
.LFB1647:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	cmpq	$0, 408(%rax)
	je	.L862
	cmpq	$0, 544(%rax)
	movl	$1, %r13d
	jne	.L861
.L862:
	movq	1440(%r12), %rdi
	movq	664(%rdi), %r10
	testq	%r10, %r10
	je	.L882
	movq	(%rsi), %r11
	movq	8(%rsi), %r8
	movq	%r11, %rcx
	movq	%r8, %rax
.L865:
	testq	%rax, %rax
	je	.L883
	movzbl	(%rcx), %edx
	subq	$1, %rax
	cmpq	%rdx, %rax
	jb	.L866
	leaq	1(%rcx,%rdx), %rcx
	subq	%rdx, %rax
	testq	%rdx, %rdx
	jne	.L865
	.p2align 4,,10
	.p2align 3
.L866:
	movl	$110, %ecx
	movl	$565, %edx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	movl	$1545, %r9d
	leaq	.LC0(%rip), %r8
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
.L861:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L884
	addq	$32, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L883:
	.cfi_restore_state
	movq	672(%rdi), %r9
	leaq	-33(%rbp), %rdx
	leaq	-32(%rbp), %rsi
	movq	%r11, %rcx
	movq	%r12, %rdi
	call	*%r10
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.L885
	movq	1792(%r12), %rdi
	movl	$1592, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movzbl	-33(%rbp), %edi
	movl	$1593, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, 1792(%r12)
	testq	%rax, %rax
	je	.L886
	movzbl	-33(%rbp), %edx
	movq	-32(%rbp), %rsi
	cmpl	$8, %edx
	jb	.L887
	movq	(%rsi), %rcx
	leaq	8(%rax), %rdi
	andq	$-8, %rdi
	movq	%rcx, (%rax)
	movq	-8(%rsi,%rdx), %rcx
	movq	%rcx, -8(%rax,%rdx)
	subq	%rdi, %rax
	leal	(%rdx,%rax), %ecx
	subq	%rax, %rsi
	shrl	$3, %ecx
	rep movsq
.L881:
	movzbl	-33(%rbp), %edx
.L871:
	movq	168(%r12), %rax
	movq	%rdx, 1800(%r12)
	movl	$1, %r13d
	movl	$1, 988(%rax)
	jmp	.L861
	.p2align 4,,10
	.p2align 3
.L887:
	testb	$4, %dl
	jne	.L888
	testl	%edx, %edx
	je	.L871
	movzbl	(%rsi), %ecx
	movb	%cl, (%rax)
	testb	$2, %dl
	je	.L881
	movzwl	-2(%rsi,%rdx), %ecx
	movw	%cx, -2(%rax,%rdx)
	movzbl	-33(%rbp), %edx
	jmp	.L871
	.p2align 4,,10
	.p2align 3
.L885:
	movl	$110, %ecx
	movl	$582, %edx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	movl	$1583, %r9d
	leaq	.LC0(%rip), %r8
	movl	$40, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L861
	.p2align 4,,10
	.p2align 3
.L882:
	movl	$110, %ecx
	movl	$582, %edx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	movl	$1567, %r9d
	leaq	.LC0(%rip), %r8
	movl	$110, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L861
	.p2align 4,,10
	.p2align 3
.L886:
	movl	$1595, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$582, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L861
	.p2align 4,,10
	.p2align 3
.L888:
	movl	(%rsi), %ecx
	movl	%ecx, (%rax)
	movl	-4(%rsi,%rdx), %ecx
	movl	%ecx, -4(%rax,%rdx)
	movzbl	-33(%rbp), %edx
	jmp	.L871
.L884:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1647:
	.size	tls_parse_stoc_npn, .-tls_parse_stoc_npn
	.p2align 4
	.globl	tls_parse_stoc_alpn
	.type	tls_parse_stoc_alpn, @function
tls_parse_stoc_alpn:
.LFB1648:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	168(%rdi), %rdi
	movl	1024(%rdi), %r13d
	testl	%r13d, %r13d
	je	.L924
	movq	8(%rsi), %rax
	movq	%rsi, %rbx
	cmpq	$1, %rax
	jbe	.L893
	movq	(%rsi), %rcx
	movzwl	(%rcx), %edx
	leaq	2(%rcx), %rsi
	movq	%rsi, (%rbx)
	leaq	-2(%rax), %rsi
	movq	%rsi, 8(%rbx)
	rolw	$8, %dx
	testq	%rsi, %rsi
	je	.L893
	movzwl	%dx, %edx
	cmpq	%rsi, %rdx
	je	.L925
.L893:
	movl	$1628, %r9d
.L922:
	movl	$110, %ecx
	movl	$579, %edx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	leaq	.LC0(%rip), %r8
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
.L889:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L924:
	.cfi_restore_state
	movl	$1615, %r9d
	leaq	.LC0(%rip), %r8
	movl	$110, %ecx
	movq	%r12, %rdi
	movl	$579, %edx
	movl	$110, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L889
	.p2align 4,,10
	.p2align 3
.L925:
	movzbl	2(%rcx), %r14d
	leaq	-3(%rax), %r13
	addq	$3, %rcx
	movq	%rcx, (%rbx)
	movq	%r13, 8(%rbx)
	cmpq	%r14, %r13
	jne	.L893
	movq	992(%rdi), %rdi
	movl	$1632, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$1633, %edx
	movq	%r13, %rdi
	leaq	.LC0(%rip), %rsi
	movq	168(%r12), %r15
	call	CRYPTO_malloc@PLT
	movq	168(%r12), %rdx
	movl	$1635, %r9d
	movq	%rax, 992(%r15)
	movq	992(%rdx), %rax
	testq	%rax, %rax
	je	.L923
	movl	$1640, %r9d
	cmpq	8(%rbx), %r14
	ja	.L922
	movq	(%rbx), %rsi
	cmpl	$8, %r14d
	jnb	.L896
	testb	$4, %r14b
	jne	.L926
	testl	%r14d, %r14d
	je	.L897
	movzbl	(%rsi), %edx
	movb	%dl, (%rax)
	testb	$2, %r14b
	jne	.L916
.L920:
	movq	168(%r12), %rdx
.L897:
	addq	%r14, (%rbx)
	subq	%r14, 8(%rbx)
	movq	1296(%r12), %rbx
	movq	%r14, 1000(%rdx)
	movq	584(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L927
	movl	200(%r12), %r13d
	cmpq	%r14, 592(%rbx)
	je	.L928
.L902:
	movl	$0, 1820(%r12)
.L921:
	movl	$1659, %r9d
	testl	%r13d, %r13d
	je	.L923
.L906:
	movl	$1, %r13d
	jmp	.L889
	.p2align 4,,10
	.p2align 3
.L896:
	movq	(%rsi), %rdx
	leaq	8(%rax), %rdi
	andq	$-8, %rdi
	movq	%rdx, (%rax)
	movq	-8(%rsi,%r14), %rdx
	movq	%rdx, -8(%rax,%r14)
	subq	%rdi, %rax
	subq	%rax, %rsi
	addl	%r14d, %eax
	shrl	$3, %eax
	movl	%eax, %ecx
	rep movsq
	jmp	.L920
	.p2align 4,,10
	.p2align 3
.L923:
	movl	$68, %ecx
	movl	$579, %edx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L889
.L927:
	movl	200(%r12), %r13d
	movl	$0, 1820(%r12)
	testl	%r13d, %r13d
	jne	.L906
	movq	992(%rdx), %rdi
	movl	$1664, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r14, %rsi
	call	CRYPTO_memdup@PLT
	movq	%rax, 584(%rbx)
	movq	1296(%r12), %rax
	cmpq	$0, 584(%rax)
	je	.L929
	movq	168(%r12), %rdx
	movl	$1, %r13d
	movq	1000(%rdx), %rdx
	movq	%rdx, 592(%rax)
	jmp	.L889
.L928:
	movq	992(%rdx), %rsi
	movq	%r14, %rdx
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L902
	jmp	.L921
.L926:
	movl	(%rsi), %edx
	movl	%edx, (%rax)
	movl	-4(%rsi,%r14), %edx
	movl	%edx, -4(%rax,%r14)
	movq	168(%r12), %rdx
	jmp	.L897
.L916:
	movzwl	-2(%rsi,%r14), %edx
	movw	%dx, -2(%rax,%r14)
	movq	168(%r12), %rdx
	jmp	.L897
.L929:
	movl	$1666, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$579, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L889
	.cfi_endproc
.LFE1648:
	.size	tls_parse_stoc_alpn, .-tls_parse_stoc_alpn
	.p2align 4
	.globl	tls_parse_stoc_use_srtp
	.type	tls_parse_stoc_use_srtp, @function
tls_parse_stoc_use_srtp:
.LFB1649:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	8(%rsi), %rax
	cmpq	$1, %rax
	jbe	.L933
	movq	(%rsi), %rdx
	movzwl	(%rdx), %ecx
	leaq	2(%rdx), %rdi
	movq	%rdi, (%rsi)
	leaq	-2(%rax), %rdi
	movq	%rdi, 8(%rsi)
	rolw	$8, %cx
	cmpq	$1, %rdi
	jbe	.L933
	cmpw	$2, %cx
	jne	.L933
	leaq	4(%rdx), %rcx
	movzwl	2(%rdx), %r14d
	movq	%rcx, (%rsi)
	leaq	-4(%rax), %rcx
	movq	%rcx, 8(%rsi)
	testq	%rcx, %rcx
	je	.L933
	movzbl	4(%rdx), %ecx
	addq	$5, %rdx
	subq	$5, %rax
	movq	%rdx, (%rsi)
	movq	%rax, 8(%rsi)
	jne	.L933
	testb	%cl, %cl
	jne	.L945
	movq	%r12, %rdi
	call	SSL_get_srtp_profiles@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L935
	rolw	$8, %r14w
	xorl	%ebx, %ebx
	movzwl	%r14w, %r14d
	jmp	.L936
	.p2align 4,,10
	.p2align 3
.L938:
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	cmpq	%r14, 8(%rax)
	je	.L946
	addl	$1, %ebx
.L936:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L938
	movl	$1722, %r9d
	jmp	.L944
	.p2align 4,,10
	.p2align 3
.L933:
	movl	$1689, %r9d
.L944:
	leaq	.LC0(%rip), %r8
	movl	$353, %ecx
.L943:
	movl	$446, %edx
	movl	$50, %esi
	movq	%r12, %rdi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L930:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L945:
	.cfi_restore_state
	movl	$1696, %r9d
	leaq	.LC0(%rip), %r8
	movl	$352, %ecx
	movq	%r12, %rdi
	movl	$446, %edx
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L930
	.p2align 4,,10
	.p2align 3
.L946:
	movq	%rax, 1920(%r12)
	movl	$1, %eax
	jmp	.L930
.L935:
	movl	$1704, %r9d
	leaq	.LC0(%rip), %r8
	movl	$359, %ecx
	jmp	.L943
	.cfi_endproc
.LFE1649:
	.size	tls_parse_stoc_use_srtp, .-tls_parse_stoc_use_srtp
	.p2align 4
	.globl	tls_parse_stoc_etm
	.type	tls_parse_stoc_etm, @function
tls_parse_stoc_etm:
.LFB1650:
	.cfi_startproc
	endbr64
	testb	$8, 1494(%rdi)
	jne	.L948
	movq	168(%rdi), %rax
	movq	568(%rax), %rax
	cmpl	$64, 40(%rax)
	je	.L948
	cmpl	$4, 36(%rax)
	je	.L948
	movl	$1, 1812(%rdi)
.L948:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1650:
	.size	tls_parse_stoc_etm, .-tls_parse_stoc_etm
	.p2align 4
	.globl	tls_parse_stoc_ems
	.type	tls_parse_stoc_ems, @function
tls_parse_stoc_ems:
.LFB1651:
	.cfi_startproc
	endbr64
	movq	168(%rdi), %rax
	orq	$512, (%rax)
	movl	200(%rdi), %eax
	testl	%eax, %eax
	jne	.L950
	movq	1296(%rdi), %rax
	orl	$1, 632(%rax)
.L950:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1651:
	.size	tls_parse_stoc_ems, .-tls_parse_stoc_ems
	.p2align 4
	.globl	tls_parse_stoc_supported_versions
	.type	tls_parse_stoc_supported_versions, @function
tls_parse_stoc_supported_versions:
.LFB1652:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	8(%rsi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpq	$1, %rax
	jbe	.L954
	movq	(%rsi), %rcx
	movzwl	(%rcx), %r8d
	addq	$2, %rcx
	subq	$2, %rax
	movq	%rcx, (%rsi)
	movq	%rax, 8(%rsi)
	jne	.L954
	cmpw	$1027, %r8w
	jne	.L959
	movl	$1, %eax
	cmpl	$2048, %edx
	je	.L951
	movl	$772, (%rdi)
.L951:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L954:
	.cfi_restore_state
	movl	$1757, %r9d
	movl	$159, %ecx
	movl	$612, %edx
	movl	$50, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L959:
	.cfi_restore_state
	movl	$1768, %r9d
	movl	$116, %ecx
	movl	$612, %edx
	movl	$47, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1652:
	.size	tls_parse_stoc_supported_versions, .-tls_parse_stoc_supported_versions
	.p2align 4
	.globl	tls_parse_stoc_key_share
	.type	tls_parse_stoc_key_share, @function
tls_parse_stoc_key_share:
.LFB1653:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movq	576(%rax), %r14
	testq	%r14, %r14
	je	.L961
	cmpq	$0, 1032(%rax)
	je	.L962
.L961:
	movl	$1794, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
.L995:
	movl	$445, %edx
	movl	$80, %esi
	movq	%r12, %rdi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L960:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L998
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L962:
	.cfi_restore_state
	movq	8(%rsi), %rcx
	movl	$1800, %r9d
	cmpq	$1, %rcx
	jbe	.L996
	movq	(%rsi), %rdi
	leaq	-2(%rcx), %rbx
	movzwl	(%rdi), %r13d
	leaq	2(%rdi), %r8
	movq	%rbx, 8(%rsi)
	movq	%r8, (%rsi)
	rolw	$8, %r13w
	andb	$8, %dh
	je	.L965
	movq	$0, -72(%rbp)
	testq	%rbx, %rbx
	jne	.L999
	movl	$1820, %r9d
	cmpw	%r13w, 1030(%rax)
	je	.L997
	leaq	-64(%rbp), %rdx
	leaq	-72(%rbp), %rsi
	movq	%r12, %rdi
	call	tls1_get_supported_groups@PLT
	movq	-64(%rbp), %rax
	movq	-72(%rbp), %rdx
	testq	%rax, %rax
	jne	.L971
	jmp	.L972
	.p2align 4,,10
	.p2align 3
.L1000:
	addq	$1, %rbx
	cmpq	%rax, %rbx
	je	.L972
.L971:
	cmpw	%r13w, (%rdx,%rbx,2)
	jne	.L1000
	movzwl	%r13w, %esi
	movl	$131076, %edx
	movq	%r12, %rdi
	call	tls_curve_allowed@PLT
	testl	%eax, %eax
	jne	.L1001
.L972:
	movl	$1833, %r9d
.L997:
	leaq	.LC0(%rip), %r8
	movl	$108, %ecx
	movl	$445, %edx
	movq	%r12, %rdi
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L960
	.p2align 4,,10
	.p2align 3
.L999:
	movl	$1810, %r9d
	.p2align 4,,10
	.p2align 3
.L996:
	leaq	.LC0(%rip), %r8
	movl	$159, %ecx
	movl	$445, %edx
	movq	%r12, %rdi
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L960
	.p2align 4,,10
	.p2align 3
.L965:
	movl	$1849, %r9d
	cmpw	%r13w, 1030(%rax)
	jne	.L997
	cmpq	$1, %rbx
	jbe	.L975
	movzwl	2(%rdi), %ebx
	subq	$4, %rcx
	rolw	$8, %bx
	movzwl	%bx, %ebx
	cmpq	%rbx, %rcx
	je	.L1002
.L975:
	movl	$1856, %r9d
	jmp	.L996
	.p2align 4,,10
	.p2align 3
.L1002:
	leaq	4(%rdi), %r13
	movq	$0, 8(%rsi)
	leaq	0(%r13,%rbx), %rax
	movq	%rax, (%rsi)
	testq	%rbx, %rbx
	je	.L975
	call	EVP_PKEY_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L977
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	EVP_PKEY_copy_parameters@PLT
	testl	%eax, %eax
	jle	.L977
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	EVP_PKEY_set1_tls_encodedpoint@PLT
	testl	%eax, %eax
	je	.L1003
	movl	$1, %ecx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	ssl_derive@PLT
	testl	%eax, %eax
	je	.L1004
	movq	168(%r12), %rax
	movq	%r15, 1032(%rax)
	movl	$1, %eax
	jmp	.L960
	.p2align 4,,10
	.p2align 3
.L1001:
	movq	168(%r12), %rax
	movw	%r13w, 1030(%rax)
	movq	576(%rax), %rdi
	call	EVP_PKEY_free@PLT
	movq	168(%r12), %rax
	movq	$0, 576(%rax)
	movl	$1, %eax
	jmp	.L960
.L977:
	movl	$1863, %r9d
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
	jmp	.L995
.L1003:
	movl	$306, %ecx
	movl	$445, %edx
	movq	%r12, %rdi
	movl	%eax, -84(%rbp)
	movl	$1869, %r9d
	leaq	.LC0(%rip), %r8
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
	movq	%r15, %rdi
	call	EVP_PKEY_free@PLT
	movl	-84(%rbp), %eax
	jmp	.L960
.L1004:
	movq	%r15, %rdi
	movl	%eax, -84(%rbp)
	call	EVP_PKEY_free@PLT
	movl	-84(%rbp), %eax
	jmp	.L960
.L998:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1653:
	.size	tls_parse_stoc_key_share, .-tls_parse_stoc_key_share
	.section	.rodata.str1.8
	.align 8
.LC1:
	.string	"../deps/openssl/openssl/ssl/statem/../packet_local.h"
	.text
	.p2align 4
	.globl	tls_parse_stoc_cookie
	.type	tls_parse_stoc_cookie, @function
tls_parse_stoc_cookie:
.LFB1654:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	8(%rsi), %rdx
	cmpq	$1, %rdx
	jbe	.L1006
	movq	(%rsi), %rcx
	leaq	-2(%rdx), %rbx
	movzwl	(%rcx), %eax
	rolw	$8, %ax
	movzwl	%ax, %eax
	cmpq	%rax, %rbx
	je	.L1014
.L1006:
	movl	$159, %ecx
	movl	$534, %edx
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	movl	$1894, %r9d
	leaq	.LC0(%rip), %r8
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
.L1005:
	popq	%rbx
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1014:
	.cfi_restore_state
	leaq	2(%rcx), %r13
	movq	$0, 8(%rsi)
	movl	$420, %edx
	movl	$1, %r14d
	leaq	0(%r13,%rbx), %rax
	movq	%rax, (%rsi)
	movq	1824(%rdi), %rdi
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 1824(%r12)
	movq	$0, 1832(%r12)
	testq	%rbx, %rbx
	je	.L1005
	movl	$429, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	CRYPTO_memdup@PLT
	movq	%rax, 1824(%r12)
	testq	%rax, %rax
	je	.L1006
	movq	%rbx, 1832(%r12)
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1654:
	.size	tls_parse_stoc_cookie, .-tls_parse_stoc_cookie
	.p2align 4
	.globl	tls_parse_stoc_early_data
	.type	tls_parse_stoc_early_data, @function
tls_parse_stoc_early_data:
.LFB1655:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	8(%rsi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$8192, %edx
	je	.L1024
	testq	%rax, %rax
	jne	.L1025
	movl	1820(%rdi), %edx
	testl	%edx, %edx
	je	.L1021
	movl	200(%rdi), %eax
	testl	%eax, %eax
	je	.L1021
	movl	$2, 1816(%rdi)
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1021:
	.cfi_restore_state
	movl	$1933, %r9d
	movl	$110, %ecx
	movl	$538, %edx
	movl	$47, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1024:
	.cfi_restore_state
	cmpq	$3, %rax
	jbe	.L1019
	movq	(%rsi), %rdx
	movl	(%rdx), %ecx
	addq	$4, %rdx
	subq	$4, %rax
	movq	%rdx, (%rsi)
	movq	%rax, 8(%rsi)
	jne	.L1019
	movq	1296(%rdi), %rax
	bswap	%ecx
	movl	%ecx, 580(%rax)
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1019:
	.cfi_restore_state
	movl	$1910, %r9d
	movl	$174, %ecx
	movl	$538, %edx
	movl	$50, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1025:
	.cfi_restore_state
	movl	$1921, %r9d
	movl	$110, %ecx
	movl	$538, %edx
	movl	$50, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1655:
	.size	tls_parse_stoc_early_data, .-tls_parse_stoc_early_data
	.p2align 4
	.globl	tls_parse_stoc_psk
	.type	tls_parse_stoc_psk, @function
tls_parse_stoc_psk:
.LFB1656:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	8(%rsi), %rax
	cmpq	$1, %rax
	jbe	.L1029
	movq	(%rsi), %rdx
	movzwl	(%rdx), %ebx
	addq	$2, %rdx
	subq	$2, %rax
	movq	%rdx, (%rsi)
	movq	%rax, 8(%rsi)
	jne	.L1029
	rolw	$8, %bx
	movl	1848(%rdi), %eax
	movzwl	%bx, %ebx
	cmpl	%ebx, %eax
	jbe	.L1043
	movq	1304(%rdi), %rdi
	testl	%ebx, %ebx
	je	.L1044
	testq	%rdi, %rdi
	je	.L1045
.L1032:
	movl	132(%r12), %eax
	movq	1296(%r12), %r8
	andl	$-5, %eax
	cmpl	$3, %eax
	jne	.L1034
	movl	580(%r8), %edx
	testl	%edx, %edx
	jne	.L1034
	movl	580(%rdi), %eax
	testl	%eax, %eax
	jne	.L1035
.L1034:
	movdqu	16(%rdi), %xmm0
	movups	%xmm0, 316(%r12)
	movdqu	32(%rdi), %xmm1
	movups	%xmm1, 332(%r12)
	movdqu	48(%rdi), %xmm2
	movups	%xmm2, 348(%r12)
	movdqu	64(%rdi), %xmm3
	movups	%xmm3, 364(%r12)
.L1035:
	movq	%r8, %rdi
	call	SSL_SESSION_free@PLT
	movq	1304(%r12), %rax
	movl	$1, 200(%r12)
	movq	$0, 1304(%r12)
	movq	%rax, 1296(%r12)
	movl	$1, %eax
	testl	%ebx, %ebx
	je	.L1026
	movl	$0, 1820(%r12)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1029:
	.cfi_restore_state
	movl	$1950, %r9d
	leaq	.LC0(%rip), %r8
	movl	$159, %ecx
	movq	%r12, %rdi
	movl	$502, %edx
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L1026:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1044:
	.cfi_restore_state
	cmpl	$2, %eax
	je	.L1037
	testq	%rdi, %rdi
	jne	.L1032
.L1037:
	movl	$1, 200(%r12)
	call	SSL_SESSION_free@PLT
	movl	$1, %eax
	movq	$0, 1304(%r12)
	jmp	.L1026
	.p2align 4,,10
	.p2align 3
.L1043:
	movl	$1956, %r9d
	movl	$114, %ecx
	movl	$502, %edx
	movl	$47, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L1026
	.p2align 4,,10
	.p2align 3
.L1045:
	movl	$1975, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$502, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L1026
	.cfi_endproc
.LFE1656:
	.size	tls_parse_stoc_psk, .-tls_parse_stoc_psk
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
