	.file	"ecdh_kdf.c"
	.text
	.p2align 4
	.type	ecdh_KDF_X9_63.part.0, @function
ecdh_KDF_X9_63.part.0:
.LFB351:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %rbx
	movq	%rdx, -160(%rbp)
	movq	%rcx, -168(%rbp)
	movq	%r8, -176(%rbp)
	movq	%r9, -184(%rbp)
	movq	%rbx, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_MD_CTX_new@PLT
	testq	%rax, %rax
	je	.L1
	movq	%rbx, %rdi
	movq	%rax, %r15
	movl	$1, %ebx
	call	EVP_MD_size@PLT
	movslq	%eax, %r13
	leaq	-132(%rbp), %rax
	movq	%rax, -192(%rbp)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L35:
	movq	-168(%rbp), %rdx
	movq	-160(%rbp), %rsi
	movl	%ebx, %eax
	movq	%r15, %rdi
	bswap	%eax
	movl	%eax, -132(%rbp)
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L3
	movq	-192(%rbp), %rsi
	movl	$4, %edx
	movq	%r15, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L3
	movq	-184(%rbp), %rdx
	movq	-176(%rbp), %rsi
	movq	%r15, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L3
	cmpq	%r12, %r13
	ja	.L4
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	EVP_DigestFinal@PLT
	testl	%eax, %eax
	je	.L3
	subq	%r13, %r12
	je	.L5
	addq	%r13, %r14
	addl	$1, %ebx
.L6:
	movq	-152(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	jne	.L35
	.p2align 4,,10
	.p2align 3
.L3:
	xorl	%r13d, %r13d
.L7:
	movq	%r15, %rdi
	call	EVP_MD_CTX_free@PLT
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L36
	addq	$152, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	leaq	-128(%rbp), %rbx
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	call	EVP_DigestFinal@PLT
	testl	%eax, %eax
	je	.L3
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%r12, %rdx
	call	memcpy@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	OPENSSL_cleanse@PLT
.L5:
	movl	$1, %r13d
	jmp	.L7
.L36:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE351:
	.size	ecdh_KDF_X9_63.part.0, .-ecdh_KDF_X9_63.part.0
	.p2align 4
	.globl	ecdh_KDF_X9_63
	.type	ecdh_KDF_X9_63, @function
ecdh_KDF_X9_63:
.LFB349:
	.cfi_startproc
	endbr64
	cmpq	$1073741824, %rsi
	seta	%r10b
	cmpq	$1073741824, %rcx
	seta	%al
	orb	%al, %r10b
	jne	.L38
	cmpq	$1073741824, %r9
	jbe	.L39
.L38:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	jmp	ecdh_KDF_X9_63.part.0
	.cfi_endproc
.LFE349:
	.size	ecdh_KDF_X9_63, .-ecdh_KDF_X9_63
	.p2align 4
	.globl	ECDH_KDF_X9_62
	.type	ECDH_KDF_X9_62, @function
ECDH_KDF_X9_62:
.LFB350:
	.cfi_startproc
	endbr64
	cmpq	$1073741824, %r9
	seta	%r10b
	cmpq	$1073741824, %rcx
	seta	%al
	orb	%al, %r10b
	jne	.L41
	cmpq	$1073741824, %rsi
	jbe	.L42
.L41:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	jmp	ecdh_KDF_X9_63.part.0
	.cfi_endproc
.LFE350:
	.size	ECDH_KDF_X9_62, .-ECDH_KDF_X9_62
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
