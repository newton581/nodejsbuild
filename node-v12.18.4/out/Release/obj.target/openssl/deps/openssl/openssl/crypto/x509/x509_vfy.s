	.file	"x509_vfy.c"
	.text
	.p2align 4
	.type	null_callback, @function
null_callback:
.LFB1394:
	.cfi_startproc
	endbr64
	movl	%edi, %eax
	ret
	.cfi_endproc
.LFE1394:
	.size	null_callback, .-null_callback
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/x509/x509_vfy.c"
	.text
	.p2align 4
	.type	lookup_certs_sk, @function
lookup_certs_sk:
.LFB1405:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$8, %rsp
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L6:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L7
	movq	%r12, %rdi
	call	X509_up_ref@PLT
.L5:
	addl	$1, %ebx
.L4:
	movq	40(%r13), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L3
	movq	40(%r13), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	X509_get_subject_name@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	X509_NAME_cmp@PLT
	testl	%eax, %eax
	jne	.L5
	testq	%r14, %r14
	jne	.L6
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L6
.L7:
	movq	X509_free@GOTPCREL(%rip), %rsi
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	OPENSSL_sk_pop_free@PLT
	movl	$373, %r8d
	movl	$65, %edx
	leaq	.LC0(%rip), %rcx
	movl	$152, %esi
	movl	$11, %edi
	call	ERR_put_error@PLT
	movl	$17, 176(%r13)
.L3:
	addq	$8, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1405:
	.size	lookup_certs_sk, .-lookup_certs_sk
	.p2align 4
	.type	crl_extension_match, @function
crl_extension_match:
.LFB1418:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	movl	$-1, %edx
	movl	%r12d, %esi
	call	X509_CRL_get_ext_by_NID@PLT
	testl	%eax, %eax
	js	.L27
	movl	%eax, %edx
	movl	%r12d, %esi
	movq	%r13, %rdi
	movl	%eax, %r14d
	call	X509_CRL_get_ext_by_NID@PLT
	cmpl	$-1, %eax
	je	.L19
.L21:
	xorl	%eax, %eax
.L15:
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	movl	$-1, %edx
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	X509_CRL_get_ext_by_NID@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jns	.L22
.L23:
	popq	%r12
	movl	$1, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	X509_CRL_get_ext@PLT
	movq	%rax, %rdi
	call	X509_EXTENSION_get_data@PLT
	movl	$-1, %edx
	movl	%r12d, %esi
	movq	%r15, %rdi
	movq	%rax, %r14
	call	X509_CRL_get_ext_by_NID@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jns	.L17
	movl	$1, %eax
	testq	%r14, %r14
	jne	.L21
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L22:
	xorl	%r14d, %r14d
.L17:
	movl	%r13d, %edx
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	X509_CRL_get_ext_by_NID@PLT
	cmpl	$-1, %eax
	jne	.L21
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	X509_CRL_get_ext@PLT
	movq	%rax, %rdi
	call	X509_EXTENSION_get_data@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	movq	%rsi, %rcx
	sete	%al
	orq	%r14, %rcx
	je	.L23
	testq	%r14, %r14
	je	.L21
	testb	%al, %al
	jne	.L21
	movq	%r14, %rdi
	call	ASN1_OCTET_STRING_cmp@PLT
	popq	%r12
	popq	%r13
	testl	%eax, %eax
	popq	%r14
	popq	%r15
	sete	%al
	popq	%rbp
	.cfi_def_cfa 7, 8
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1418:
	.size	crl_extension_match, .-crl_extension_match
	.p2align 4
	.type	dane_match.isra.0, @function
dane_match.isra.0:
.LFB1504:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -200(%rbp)
	movl	44(%r12), %edx
	movl	%ecx, -184(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	cmpl	$1, %ecx
	movl	$0, -140(%rbp)
	sbbl	%esi, %esi
	notl	%esi
	addl	$2, %esi
	cmpl	$1, %ecx
	sbbl	%eax, %eax
	andl	$5, %eax
	addl	$5, %eax
	cmpl	%edi, %ecx
	cmovge	%esi, %eax
	movl	%eax, -152(%rbp)
	testl	%edx, %edx
	js	.L31
	andl	$-4, %eax
	movl	%eax, -152(%rbp)
.L31:
	movl	-152(%rbp), %eax
	testl	%eax, 40(%r12)
	jne	.L32
.L34:
	movq	$0, -160(%rbp)
	xorl	%r8d, %r8d
.L33:
	movq	-160(%rbp), %rdi
	movl	$2702, %edx
	leaq	.LC0(%rip), %rsi
	movl	%r8d, -148(%rbp)
	call	CRYPTO_free@PLT
	movl	-148(%rbp), %r8d
.L28:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L68
	addq	$168, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movq	8(%r12), %rdi
	call	OPENSSL_sk_num@PLT
	movl	%eax, -168(%rbp)
	testl	%eax, %eax
	jle	.L34
	leaq	-140(%rbp), %rax
	xorl	%r15d, %r15d
	movl	$0, -180(%rbp)
	movl	$256, %r14d
	movq	$0, -192(%rbp)
	movl	%r15d, %r13d
	movl	$256, -148(%rbp)
	movl	$256, -176(%rbp)
	movq	$0, -160(%rbp)
	movl	$256, -164(%rbp)
	movq	%rax, -208(%rbp)
	.p2align 4,,10
	.p2align 3
.L50:
	movq	8(%r12), %rdi
	movl	%r13d, %esi
	call	OPENSSL_sk_value@PLT
	movl	-152(%rbp), %ecx
	movzbl	(%rax), %r15d
	movq	%rax, %rbx
	btl	%r15d, %ecx
	jnc	.L35
	cmpl	%r14d, %r15d
	je	.L36
	movq	(%r12), %rdx
	movzbl	2(%rbx), %eax
	movl	$256, -148(%rbp)
	movq	8(%rdx), %rdx
	movzbl	(%rdx,%rax), %eax
	movl	%eax, -176(%rbp)
.L36:
	movzbl	1(%rbx), %ecx
	cmpl	-164(%rbp), %ecx
	movl	%ecx, -172(%rbp)
	movl	%ecx, %r14d
	je	.L37
	movq	-160(%rbp), %rdi
	movl	$2641, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	testb	%r14b, %r14b
	movl	-172(%rbp), %ecx
	movq	$0, -136(%rbp)
	je	.L38
	cmpb	$1, %r14b
	je	.L39
	movl	$2548, %r8d
	movl	$133, %edx
	movl	$107, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %r8d
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L53:
	movl	%r15d, %r14d
	.p2align 4,,10
	.p2align 3
.L35:
	addl	$1, %r13d
	cmpl	%r13d, -168(%rbp)
	jne	.L50
	xorl	%r8d, %r8d
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L37:
	movzbl	2(%rbx), %eax
	testb	%al, %al
	je	.L46
	movq	(%r12), %rcx
	movzbl	%al, %edx
	movq	8(%rcx), %rcx
	movzbl	(%rcx,%rdx), %edx
	cmpl	-176(%rbp), %edx
	jb	.L53
.L46:
	movzbl	%al, %ecx
	cmpl	-148(%rbp), %ecx
	jne	.L47
	movl	-140(%rbp), %esi
.L48:
	movl	%r15d, %r14d
	cmpq	%rsi, 16(%rbx)
	jne	.L35
	movq	8(%rbx), %r8
	movq	-192(%rbp), %rdi
	movq	%rsi, %rdx
	movq	%r8, %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L35
	movl	%eax, %r8d
	movl	$12, %eax
	btl	%r15d, %eax
	jc	.L58
	movl	44(%r12), %eax
	testl	%eax, %eax
	jns	.L33
.L49:
	movl	-184(%rbp), %eax
	movq	%rbx, 24(%r12)
	movl	$2693, %edx
	leaq	.LC0(%rip), %rsi
	movq	32(%r12), %rdi
	movl	%r8d, -148(%rbp)
	movl	%eax, 44(%r12)
	call	CRYPTO_free@PLT
	movq	-200(%rbp), %rax
	movq	%rax, 32(%r12)
	movq	%rax, %rdi
	call	X509_up_ref@PLT
	movl	-148(%rbp), %r8d
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L39:
	movq	-200(%rbp), %rdi
	movl	%ecx, -148(%rbp)
	call	X509_get_X509_PUBKEY@PLT
	leaq	-136(%rbp), %rsi
	movq	%rax, %rdi
	call	i2d_X509_PUBKEY@PLT
	movl	-148(%rbp), %ecx
	testl	%eax, %eax
	js	.L43
.L69:
	movq	-136(%rbp), %rdx
	movq	%rdx, -160(%rbp)
	testq	%rdx, %rdx
	je	.L43
	movq	(%r12), %rdx
	movl	%eax, -180(%rbp)
	movzbl	2(%rbx), %eax
	movl	%ecx, -164(%rbp)
	movq	8(%rdx), %rdi
	movl	%eax, -148(%rbp)
	movzbl	(%rdi,%rax), %edi
	movl	%edi, -176(%rbp)
.L45:
	movq	(%rdx), %rdx
	movl	-180(%rbp), %esi
	movq	(%rdx,%rax,8), %r8
	movl	%esi, -140(%rbp)
	testq	%r8, %r8
	je	.L54
	leaq	-128(%rbp), %r14
	movq	-208(%rbp), %rcx
	movq	-160(%rbp), %rdi
	xorl	%r9d, %r9d
	movq	%r14, %rdx
	call	EVP_Digest@PLT
	testl	%eax, %eax
	je	.L55
	movq	%r14, -192(%rbp)
	movl	-140(%rbp), %esi
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L54:
	movq	-160(%rbp), %rax
	movq	%rax, -192(%rbp)
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L38:
	movq	-200(%rbp), %rdi
	leaq	-136(%rbp), %rsi
	movl	%ecx, -148(%rbp)
	call	i2d_X509@PLT
	movl	-148(%rbp), %ecx
	testl	%eax, %eax
	jns	.L69
.L43:
	movl	$2553, %r8d
	movl	$65, %edx
	movl	$107, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %r8d
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L47:
	movl	%ecx, -148(%rbp)
	movq	(%r12), %rdx
	jmp	.L45
.L55:
	movl	$-1, %r8d
	jmp	.L33
.L58:
	movl	$1, %r8d
	jmp	.L49
.L68:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1504:
	.size	dane_match.isra.0, .-dane_match.isra.0
	.p2align 4
	.type	cert_crl, @function
cert_crl:
.LFB1430:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rax
	testb	$16, 24(%rax)
	jne	.L74
	testb	$2, 133(%rsi)
	je	.L74
	movq	%rdx, -40(%rbp)
	movq	%rdi, %rsi
	movl	$36, 176(%rdi)
	xorl	%edi, %edi
	call	*56(%rbx)
	movq	-40(%rbp), %rdx
	testl	%eax, %eax
	je	.L70
.L74:
	leaq	-32(%rbp), %rsi
	movq	%r12, %rdi
	call	X509_CRL_get0_by_cert@PLT
	testl	%eax, %eax
	jne	.L72
	movl	$1, %eax
.L70:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L84
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore_state
	movq	-32(%rbp), %rax
	cmpl	$8, 48(%rax)
	je	.L76
	movl	$23, 176(%rbx)
	xorl	%edi, %edi
	movq	%rbx, %rsi
	call	*56(%rbx)
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L76:
	movl	$2, %eax
	jmp	.L70
.L84:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1430:
	.size	cert_crl, .-cert_crl
	.p2align 4
	.type	check_issued, @function
check_issued:
.LFB1403:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpq	%rdx, %rsi
	je	.L105
	movq	%rdi, %r13
	movq	%rdx, %rdi
	movq	%rdx, %rbx
	call	X509_check_issued@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L106
	xorl	%r14d, %r14d
.L85:
	popq	%rbx
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%edx, %edx
	movl	$-1, %esi
	call	X509_check_purpose@PLT
	movq	152(%r13), %rdi
	cmpl	$1, %eax
	je	.L107
.L90:
	xorl	%r12d, %r12d
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L94:
	movq	152(%r13), %rdi
	movl	%r12d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	cmpq	%rax, %rbx
	je	.L85
	movq	%rbx, %rsi
	call	X509_cmp@PLT
	testl	%eax, %eax
	je	.L85
	movq	152(%r13), %rdi
	addl	$1, %r12d
.L93:
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jl	.L94
.L97:
	movl	$1, %r14d
	popq	%rbx
	popq	%r12
	movl	%r14d, %eax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	testb	$32, 225(%r12)
	je	.L90
	call	OPENSSL_sk_num@PLT
	cmpl	$1, %eax
	je	.L97
	movq	152(%r13), %rdi
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L105:
	xorl	%edx, %edx
	movl	$-1, %esi
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	X509_check_purpose@PLT
	cmpl	$1, %eax
	jne	.L85
	movl	224(%r12), %r14d
	shrl	$13, %r14d
	andl	$1, %r14d
	jmp	.L85
	.cfi_endproc
.LFE1403:
	.size	check_issued, .-check_issued
	.p2align 4
	.type	check_policy, @function
check_policy:
.LFB1431:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	cmpq	$0, 216(%rdi)
	je	.L109
.L120:
	movl	$1, %r13d
.L108:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	movl	240(%rdi), %ecx
	movq	152(%rdi), %rdx
	movq	%rdi, %r12
	testl	%ecx, %ecx
	je	.L111
	xorl	%esi, %esi
	movq	%rdx, %rdi
	call	OPENSSL_sk_push@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L112
	movq	152(%r12), %rdx
.L111:
	movq	32(%r12), %rax
	leaq	168(%r12), %rsi
	leaq	160(%r12), %rdi
	movq	48(%rax), %rcx
	movl	24(%rax), %r8d
	call	X509_policy_check@PLT
	movl	%eax, %r13d
	movl	240(%r12), %eax
	testl	%eax, %eax
	jne	.L133
.L113:
	testl	%r13d, %r13d
	je	.L134
	cmpl	$-1, %r13d
	je	.L135
	cmpl	$-2, %r13d
	je	.L136
	cmpl	$1, %r13d
	jne	.L137
	movq	32(%r12), %rax
	testb	$8, 25(%rax)
	je	.L120
	movq	%r12, %rsi
	movl	$2, %edi
	xorl	%r13d, %r13d
	movq	$0, 184(%r12)
	call	*56(%r12)
	testl	%eax, %eax
	setne	%r13b
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L133:
	movq	152(%r12), %rdi
	call	OPENSSL_sk_pop@PLT
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L135:
	movl	$1, %ebx
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L119:
	movq	152(%r12), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	testb	$8, 225(%rax)
	je	.L118
	movl	%ebx, 172(%r12)
	movq	%r12, %rsi
	xorl	%edi, %edi
	movq	%rax, 184(%r12)
	movl	$42, 176(%r12)
	call	*56(%r12)
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L108
.L118:
	addl	$1, %ebx
.L115:
	movq	152(%r12), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L119
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L134:
	movl	$1618, %r8d
.L132:
	movl	$65, %edx
	movl	$145, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	%r13d, %eax
	movl	$17, 176(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
	movl	$1644, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$68, %edx
	xorl	%r13d, %r13d
	movl	$145, %esi
	movl	$11, %edi
	call	ERR_put_error@PLT
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L112:
	movl	$1608, %r8d
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L136:
	movq	56(%r12), %rax
	movq	%r12, %rsi
	xorl	%edi, %edi
	movq	$0, 184(%r12)
	movl	$43, 176(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE1431:
	.size	check_policy, .-check_policy
	.p2align 4
	.type	check_trust, @function
check_trust:
.LFB1413:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	232(%rdi), %r14
	movl	%esi, -52(%rbp)
	movq	152(%rdi), %rdi
	call	OPENSSL_sk_num@PLT
	movl	%eax, %r12d
	testq	%r14, %r14
	je	.L142
	testb	$5, 40(%r14)
	jne	.L203
.L142:
	cmpl	%r12d, -52(%rbp)
	jge	.L201
.L140:
	movq	152(%rbx), %rdi
.L164:
	movl	-52(%rbp), %r15d
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L204:
	cmpl	$2, %eax
	je	.L149
	addl	$1, %r15d
	cmpl	%r12d, %r15d
	jge	.L150
	movq	152(%rbx), %rdi
.L151:
	movl	%r15d, %esi
	call	OPENSSL_sk_value@PLT
	xorl	%edx, %edx
	movq	%rax, %r13
	movq	32(%rbx), %rax
	movq	%r13, %rdi
	movl	36(%rax), %esi
	call	X509_check_trust@PLT
	cmpl	$1, %eax
	jne	.L204
.L148:
	testq	%r14, %r14
	je	.L161
	movq	8(%r14), %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jle	.L161
	movl	48(%r14), %edx
	testl	%edx, %edx
	js	.L205
.L162:
	movl	44(%r14), %eax
	testl	%eax, %eax
	js	.L152
.L161:
	movl	$1, %eax
.L138:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L203:
	.cfi_restore_state
	movl	-52(%rbp), %esi
	testl	%esi, %esi
	jle	.L142
	cmpl	%eax, %esi
	jl	.L206
.L201:
	jne	.L152
	movq	32(%rbx), %rax
	testb	$8, 26(%rax)
	jne	.L207
.L152:
	addq	$24, %rsp
	movl	$3, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L209:
	.cfi_restore_state
	movq	-64(%rbp), %rdi
	xorl	%r15d, %r15d
	call	X509_free@PLT
	.p2align 4,,10
	.p2align 3
.L149:
	movl	%r15d, 172(%rbx)
	testq	%r13, %r13
	je	.L208
.L158:
	xorl	%edi, %edi
	movq	%r13, 184(%rbx)
	movq	%rbx, %rsi
	movl	$28, 176(%rbx)
	call	*56(%rbx)
	testl	%eax, %eax
	jne	.L152
.L159:
	addq	$24, %rsp
	movl	$2, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L206:
	.cfi_restore_state
	movq	232(%rbx), %rax
	movq	152(%rbx), %rdi
	testq	%rax, %rax
	je	.L164
	testb	$5, 40(%rax)
	je	.L164
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L140
	movq	232(%rbx), %rsi
	movl	148(%rbx), %edi
	movl	-52(%rbp), %ecx
	call	dane_match.isra.0
	testl	%eax, %eax
	js	.L159
	je	.L140
	movl	-52(%rbp), %r13d
	movl	$1, %eax
	subl	$1, %r13d
	movl	%r13d, 148(%rbx)
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L205:
	movl	-52(%rbp), %eax
	movl	%eax, 48(%r14)
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L150:
	movq	32(%rbx), %rax
	testb	$8, 26(%rax)
	jne	.L148
	jmp	.L152
.L208:
	movq	152(%rbx), %rdi
	movl	%r15d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r13
	jmp	.L158
.L207:
	movq	152(%rbx), %rdi
	xorl	%esi, %esi
	call	OPENSSL_sk_value@PLT
	movq	120(%rbx), %r12
	movq	%rax, %rdi
	movq	%rax, %r13
	call	X509_get_subject_name@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	*%r12
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L152
	movq	$0, -64(%rbp)
	xorl	%r15d, %r15d
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L155:
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	X509_cmp@PLT
	testl	%eax, %eax
	je	.L154
	addl	$1, %r15d
.L153:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r15d
	jl	.L155
.L154:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r15d
	jl	.L156
	movq	X509_free@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	call	OPENSSL_sk_pop_free@PLT
	movl	$3, %eax
	jmp	.L138
.L156:
	movq	-64(%rbp), %r15
	movq	%r15, %rdi
	call	X509_up_ref@PLT
	movq	X509_free@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	%r15, %rdi
	testq	%r15, %r15
	je	.L152
	movq	32(%rbx), %rax
	xorl	%edx, %edx
	movl	36(%rax), %esi
	call	X509_check_trust@PLT
	cmpl	$2, %eax
	je	.L209
	movq	152(%rbx), %rdi
	movq	-64(%rbp), %rdx
	xorl	%esi, %esi
	call	OPENSSL_sk_set@PLT
	movq	%r13, %rdi
	call	X509_free@PLT
	movl	$0, 148(%rbx)
	jmp	.L148
	.cfi_endproc
.LFE1413:
	.size	check_trust, .-check_trust
	.p2align 4
	.type	check_id, @function
check_id:
.LFB1412:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	32(%rdi), %rbx
	movq	8(%rdi), %r14
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L215
	call	OPENSSL_sk_num@PLT
	movq	72(%rbx), %rdi
	movl	%eax, -52(%rbp)
	testq	%rdi, %rdi
	je	.L214
	movl	$719, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 72(%rbx)
.L214:
	movl	-52(%rbp), %eax
	leaq	72(%rbx), %r15
	xorl	%r13d, %r13d
	testl	%eax, %eax
	jg	.L216
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L237:
	addl	$1, %r13d
	cmpl	%r13d, -52(%rbp)
	je	.L220
.L216:
	movq	56(%rbx), %rdi
	movl	%r13d, %esi
	call	OPENSSL_sk_value@PLT
	movl	64(%rbx), %ecx
	xorl	%edx, %edx
	movq	%r15, %r8
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	X509_check_host@PLT
	testl	%eax, %eax
	jle	.L237
.L215:
	movq	80(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L213
	movq	88(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	call	X509_check_email@PLT
	testl	%eax, %eax
	jle	.L238
.L213:
	movq	96(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L223
	movq	104(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	call	X509_check_ip@PLT
	testl	%eax, %eax
	jle	.L239
.L223:
	movl	$1, %eax
.L210:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L236:
	.cfi_restore_state
	je	.L215
	.p2align 4,,10
	.p2align 3
.L220:
	movq	8(%r12), %rax
	movl	$0, 172(%r12)
	testq	%rax, %rax
	je	.L240
.L218:
	xorl	%edi, %edi
	movq	%rax, 184(%r12)
	movq	%r12, %rsi
	movl	$62, 176(%r12)
	call	*56(%r12)
	testl	%eax, %eax
	jne	.L215
.L225:
	xorl	%eax, %eax
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L238:
	movq	8(%r12), %rax
	movl	$0, 172(%r12)
	testq	%rax, %rax
	je	.L241
.L224:
	xorl	%edi, %edi
	movq	%rax, 184(%r12)
	movq	%r12, %rsi
	movl	$63, 176(%r12)
	call	*56(%r12)
	testl	%eax, %eax
	jne	.L213
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L239:
	movq	8(%r12), %rax
	movl	$0, 172(%r12)
	testq	%rax, %rax
	je	.L242
.L226:
	movq	%rax, 184(%r12)
	movq	%r12, %rsi
	xorl	%edi, %edi
	movl	$64, 176(%r12)
	call	*56(%r12)
	testl	%eax, %eax
	setne	%al
	addq	$24, %rsp
	popq	%rbx
	movzbl	%al, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L240:
	.cfi_restore_state
	movq	152(%r12), %rdi
	xorl	%esi, %esi
	call	OPENSSL_sk_value@PLT
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L241:
	movq	152(%r12), %rdi
	xorl	%esi, %esi
	call	OPENSSL_sk_value@PLT
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L242:
	movq	152(%r12), %rdi
	xorl	%esi, %esi
	call	OPENSSL_sk_value@PLT
	jmp	.L226
	.cfi_endproc
.LFE1412:
	.size	check_id, .-check_id
	.p2align 4
	.globl	X509_cmp_current_time
	.type	X509_cmp_current_time, @function
X509_cmp_current_time:
.LFB1434:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	4(%rdi), %eax
	cmpl	$23, %eax
	je	.L244
	cmpl	$24, %eax
	jne	.L248
	cmpl	$15, (%rdi)
	je	.L254
	.p2align 4,,10
	.p2align 3
.L248:
	xorl	%eax, %eax
.L243:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L265
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L244:
	.cfi_restore_state
	cmpl	$13, (%rdi)
	jne	.L248
.L254:
	xorl	%ebx, %ebx
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L267:
	movslq	(%r12), %rax
	addq	$1, %rbx
	leal	-1(%rax), %edx
	cmpl	%ebx, %edx
	jle	.L266
.L249:
	movq	8(%r12), %rax
	movsbl	(%rax,%rbx), %edi
	call	ascii_isdigit@PLT
	testl	%eax, %eax
	jne	.L267
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L266:
	movq	8(%r12), %rdx
	cmpb	$90, -1(%rdx,%rax)
	jne	.L248
	leaq	-48(%rbp), %r14
	movq	%r14, %rdi
	call	time@PLT
	movq	-48(%rbp), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%edi, %edi
	call	ASN1_TIME_adj@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L252
	leaq	-52(%rbp), %rdi
	movq	%rax, %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	ASN1_TIME_diff@PLT
	testl	%eax, %eax
	je	.L252
	movl	-52(%rbp), %edx
	movl	$1, %eax
	testl	%edx, %edx
	js	.L251
	movl	-48(%rbp), %eax
	sarl	$31, %eax
	andl	$2, %eax
	subl	$1, %eax
.L251:
	movq	%r13, %rdi
	movl	%eax, -68(%rbp)
	call	ASN1_TIME_free@PLT
	movl	-68(%rbp), %eax
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L252:
	xorl	%eax, %eax
	jmp	.L251
.L265:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1434:
	.size	X509_cmp_current_time, .-X509_cmp_current_time
	.p2align 4
	.globl	X509_cmp_time
	.type	X509_cmp_time, @function
X509_cmp_time:
.LFB1435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	4(%rdi), %eax
	cmpl	$23, %eax
	je	.L269
	cmpl	$24, %eax
	jne	.L273
	cmpl	$15, (%rdi)
	je	.L281
	.p2align 4,,10
	.p2align 3
.L273:
	xorl	%eax, %eax
.L268:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L292
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L269:
	.cfi_restore_state
	cmpl	$13, (%rdi)
	jne	.L273
.L281:
	xorl	%ebx, %ebx
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L294:
	movslq	(%r12), %rax
	addq	$1, %rbx
	leal	-1(%rax), %edx
	cmpl	%ebx, %edx
	jle	.L293
.L274:
	movq	8(%r12), %rax
	movsbl	(%rax,%rbx), %edi
	call	ascii_isdigit@PLT
	testl	%eax, %eax
	jne	.L294
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L293:
	movq	8(%r12), %rdx
	cmpb	$90, -1(%rdx,%rax)
	jne	.L273
	testq	%r13, %r13
	je	.L275
	movq	0(%r13), %rsi
	movq	%rsi, -48(%rbp)
.L276:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%edi, %edi
	call	ASN1_TIME_adj@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L279
	leaq	-48(%rbp), %rsi
	leaq	-52(%rbp), %rdi
	movq	%rax, %rcx
	movq	%r12, %rdx
	call	ASN1_TIME_diff@PLT
	testl	%eax, %eax
	je	.L279
	movl	-52(%rbp), %edx
	movl	$1, %eax
	testl	%edx, %edx
	js	.L278
	movl	-48(%rbp), %eax
	sarl	$31, %eax
	andl	$2, %eax
	subl	$1, %eax
.L278:
	movq	%r13, %rdi
	movl	%eax, -68(%rbp)
	call	ASN1_TIME_free@PLT
	movl	-68(%rbp), %eax
	jmp	.L268
.L275:
	leaq	-48(%rbp), %rdi
	call	time@PLT
	movq	-48(%rbp), %rsi
	jmp	.L276
.L279:
	xorl	%eax, %eax
	jmp	.L278
.L292:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1435:
	.size	X509_cmp_time, .-X509_cmp_time
	.p2align 4
	.globl	x509_check_cert_time
	.type	x509_check_cert_time, @function
x509_check_cert_time:
.LFB1432:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	32(%rdi), %r13
	movq	24(%r13), %rax
	addq	$8, %r13
	testb	$2, %al
	jne	.L297
	xorl	%r13d, %r13d
	testl	$2097152, %eax
	jne	.L309
.L297:
	movq	%r12, %rdi
	movl	%r14d, %r15d
	call	X509_get0_notBefore@PLT
	movq	%r13, %rsi
	shrl	$31, %r15d
	movq	%rax, %rdi
	call	X509_cmp_time
	testl	%eax, %eax
	js	.L299
	testb	%r15b, %r15b
	je	.L299
.L302:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L299:
	.cfi_restore_state
	testl	%eax, %eax
	je	.L327
	jle	.L305
	movl	%r14d, 172(%rbx)
	movq	%r12, %rax
	testq	%r12, %r12
	je	.L328
.L306:
	movq	%rax, 184(%rbx)
	movl	$9, 176(%rbx)
.L325:
	xorl	%edi, %edi
	movq	%rbx, %rsi
	call	*56(%rbx)
	testl	%eax, %eax
	je	.L302
.L305:
	movq	%r12, %rdi
	call	X509_get0_notAfter@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	X509_cmp_time
	testl	%eax, %eax
	jg	.L304
	testb	%r15b, %r15b
	jne	.L302
.L304:
	testl	%eax, %eax
	je	.L329
	jns	.L309
	movl	%r14d, 172(%rbx)
	testq	%r12, %r12
	je	.L330
.L310:
	movq	%r12, 184(%rbx)
	movl	$10, 176(%rbx)
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L309:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L327:
	.cfi_restore_state
	movl	%r14d, 172(%rbx)
	movq	%r12, %rax
	testq	%r12, %r12
	je	.L331
.L301:
	movq	%rax, 184(%rbx)
	movl	$13, 176(%rbx)
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L329:
	movl	%r14d, 172(%rbx)
	testq	%r12, %r12
	je	.L332
.L308:
	movq	%r12, 184(%rbx)
	movl	$14, 176(%rbx)
.L326:
	movq	%rbx, %rsi
	xorl	%edi, %edi
	call	*56(%rbx)
	testl	%eax, %eax
	setne	%al
	addq	$8, %rsp
	popq	%rbx
	movzbl	%al, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L328:
	.cfi_restore_state
	movq	152(%rbx), %rdi
	movl	%r14d, %esi
	call	OPENSSL_sk_value@PLT
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L330:
	movq	152(%rbx), %rdi
	movl	%r14d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r12
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L331:
	movq	152(%rbx), %rdi
	movl	%r14d, %esi
	call	OPENSSL_sk_value@PLT
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L332:
	movq	152(%rbx), %rdi
	movl	%r14d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r12
	jmp	.L308
	.cfi_endproc
.LFE1432:
	.size	x509_check_cert_time, .-x509_check_cert_time
	.p2align 4
	.type	internal_verify, @function
internal_verify:
.LFB1433:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	152(%rdi), %rdi
	call	OPENSSL_sk_num@PLT
	movq	152(%r12), %rdi
	leal	-1(%rax), %r14d
	movl	%eax, %r13d
	movl	%r14d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rbx
	movl	240(%r12), %eax
	testl	%eax, %eax
	jne	.L349
	movq	%rbx, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	*72(%r12)
	testl	%eax, %eax
	jne	.L335
	movq	32(%r12), %rax
	testb	$8, 26(%rax)
	jne	.L350
	testl	%r14d, %r14d
	jle	.L368
	subl	$2, %r13d
	movq	152(%r12), %rdi
	movl	%r13d, 172(%r12)
	movl	%r13d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r15
	testl	%r13d, %r13d
	jns	.L345
.L338:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L335:
	.cfi_restore_state
	testl	%r14d, %r14d
	js	.L338
.L348:
	movq	32(%r12), %rax
	testb	$64, 25(%rax)
	je	.L350
	movq	%rbx, %rdi
	call	X509_get0_pubkey@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L354
	movq	%rbx, -56(%rbp)
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L349:
	movq	$0, -56(%rbp)
.L334:
	movl	%r14d, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	x509_check_cert_time
	testl	%eax, %eax
	je	.L344
	movq	%rbx, %xmm0
	movl	%r14d, 172(%r12)
	movq	%r12, %rsi
	movl	$1, %edi
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, 184(%r12)
	call	*56(%r12)
	testl	%eax, %eax
	je	.L344
	movl	%r14d, %r13d
	subl	$1, %r13d
	js	.L338
	movq	152(%r12), %rdi
	movl	%r13d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r15
.L345:
	cmpq	%r15, %rbx
	je	.L369
	movq	%rbx, %rdi
	call	X509_get0_pubkey@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L346
	movq	%rbx, -56(%rbp)
	movl	%r13d, %r14d
	movq	%r15, %rbx
.L347:
	movq	%rbx, %rdi
	call	X509_verify@PLT
	testl	%eax, %eax
	jg	.L334
	movl	%r14d, 172(%r12)
	movq	%rbx, %rax
	testq	%rbx, %rbx
	je	.L370
.L343:
	xorl	%edi, %edi
	movq	%rax, 184(%r12)
	movq	%r12, %rsi
	movl	$7, 176(%r12)
	call	*56(%r12)
	testl	%eax, %eax
	jne	.L334
.L344:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L350:
	.cfi_restore_state
	movq	%rbx, -56(%rbp)
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L368:
	movl	$0, 172(%r12)
	testq	%rbx, %rbx
	je	.L371
.L337:
	movq	%rbx, 184(%r12)
	movq	56(%r12), %rax
	movq	%r12, %rsi
	xorl	%edi, %edi
	movl	$21, 176(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L354:
	.cfi_restore_state
	movq	%rbx, %r15
	movl	%r14d, %r13d
.L346:
	movl	%r14d, 172(%r12)
	movq	%rbx, %rax
	testq	%rbx, %rbx
	je	.L372
.L341:
	xorl	%edi, %edi
	movq	%rax, 184(%r12)
	movq	%r12, %rsi
	movl	$6, 176(%r12)
	call	*56(%r12)
	testl	%eax, %eax
	je	.L344
	movq	%rbx, -56(%rbp)
	movl	%r13d, %r14d
	movq	%r15, %rbx
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L371:
	movq	152(%r12), %rdi
	xorl	%esi, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rbx
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L372:
	movq	152(%r12), %rdi
	movl	%r14d, %esi
	call	OPENSSL_sk_value@PLT
	jmp	.L341
.L370:
	movq	152(%r12), %rdi
	movl	%r14d, %esi
	call	OPENSSL_sk_value@PLT
	jmp	.L343
.L369:
	movl	%r13d, %r14d
	jmp	.L348
	.cfi_endproc
.LFE1433:
	.size	internal_verify, .-internal_verify
	.p2align 4
	.type	get_issuer_sk, @function
get_issuer_sk:
.LFB1404:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%rdi, -56(%rbp)
	movq	40(%rsi), %r15
	movq	%rdx, -64(%rbp)
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L375:
	addl	$1, %r12d
.L374:
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L376
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	movq	-64(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rax, %r14
	movq	%rax, %rdx
	call	*72(%rbx)
	testl	%eax, %eax
	je	.L375
	movl	$-1, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r14, %r13
	call	x509_check_cert_time
	testl	%eax, %eax
	je	.L375
.L376:
	movq	-56(%rbp), %rax
	movq	%r13, (%rax)
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L373
	movq	%r13, %rdi
	call	X509_up_ref@PLT
	movl	$1, %eax
.L373:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1404:
	.size	get_issuer_sk, .-get_issuer_sk
	.p2align 4
	.type	check_crl_time, @function
check_crl_time:
.LFB1416:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	testl	%edx, %edx
	je	.L387
	movq	%rsi, 200(%rbx)
.L387:
	movq	32(%rbx), %r13
	movq	24(%r13), %rax
	addq	$8, %r13
	testb	$2, %al
	jne	.L389
	xorl	%r13d, %r13d
	testl	$2097152, %eax
	jne	.L455
.L389:
	movq	%r14, %rdi
	call	X509_CRL_get0_lastUpdate@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	X509_cmp_time
	testl	%eax, %eax
	je	.L456
	jle	.L395
	testl	%r12d, %r12d
	je	.L393
	movl	$11, 176(%rbx)
	xorl	%edi, %edi
	movq	%rbx, %rsi
	call	*56(%rbx)
	testl	%eax, %eax
	je	.L393
	movq	%r14, %rdi
	call	X509_CRL_get0_nextUpdate@PLT
	testq	%rax, %rax
	je	.L399
	movq	%r14, %rdi
	call	X509_CRL_get0_nextUpdate@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	X509_cmp_time
	testl	%eax, %eax
	je	.L401
	jns	.L399
	testb	$2, 208(%rbx)
	je	.L406
.L397:
	testl	%r12d, %r12d
	jne	.L399
.L455:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L456:
	.cfi_restore_state
	testl	%r12d, %r12d
	je	.L393
	movl	$15, 176(%rbx)
	xorl	%edi, %edi
	movq	%rbx, %rsi
	call	*56(%rbx)
	testl	%eax, %eax
	je	.L393
	movq	%r14, %rdi
	call	X509_CRL_get0_nextUpdate@PLT
	testq	%rax, %rax
	je	.L399
	movq	%r14, %rdi
	call	X509_CRL_get0_nextUpdate@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	X509_cmp_time
	testl	%eax, %eax
	je	.L401
	jns	.L399
.L407:
	testb	$2, 208(%rbx)
	jne	.L397
	testl	%r12d, %r12d
	je	.L393
	.p2align 4,,10
	.p2align 3
.L406:
	movl	$12, 176(%rbx)
	jmp	.L454
.L457:
	testl	%r12d, %r12d
	jne	.L401
.L393:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L401:
	.cfi_restore_state
	movl	$16, 176(%rbx)
.L454:
	xorl	%edi, %edi
	movq	%rbx, %rsi
	call	*56(%rbx)
	testl	%eax, %eax
	je	.L393
.L399:
	movq	$0, 200(%rbx)
	jmp	.L455
.L395:
	movq	%r14, %rdi
	call	X509_CRL_get0_nextUpdate@PLT
	testq	%rax, %rax
	je	.L397
	movq	%r14, %rdi
	call	X509_CRL_get0_nextUpdate@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	X509_cmp_time
	testl	%eax, %eax
	je	.L457
	jns	.L397
	jmp	.L407
	.cfi_endproc
.LFE1416:
	.size	check_crl_time, .-check_crl_time
	.p2align 4
	.type	get_crl_sk, @function
get_crl_sk:
.LFB1417:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	%rsi, -208(%rbp)
	movq	%rdx, -192(%rbp)
	movq	%rax, -88(%rbp)
	movq	%rcx, -216(%rbp)
	movq	%r8, -200(%rbp)
	movq	%r9, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r8), %eax
	movl	$0, -104(%rbp)
	movq	$0, -120(%rbp)
	movl	%eax, -80(%rbp)
	movq	184(%rdi), %rax
	movq	-88(%rbp), %rdi
	movq	$0, -112(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-60(%rbp), %rax
	movq	$0, -144(%rbp)
	movl	$0, -72(%rbp)
	movq	%rax, -168(%rbp)
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, -72(%rbp)
	jge	.L603
	.p2align 4,,10
	.p2align 3
.L504:
	movl	-72(%rbp), %esi
	movq	-88(%rbp), %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r15
	movl	152(%rax), %eax
	movl	%eax, %r12d
	andl	$2, %r12d
	jne	.L460
	movq	-136(%rbp), %rcx
	movl	(%rcx), %edx
	movl	%edx, -100(%rbp)
	movq	32(%r13), %rdx
	testb	$16, 25(%rdx)
	jne	.L461
	testb	$96, %al
	jne	.L460
.L462:
	movq	%r15, %rdi
	call	X509_CRL_get_issuer@PLT
	movq	-128(%rbp), %rdi
	movq	%rax, %r14
	call	X509_get_issuer_name@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	X509_NAME_cmp@PLT
	testl	%eax, %eax
	je	.L513
	testb	$32, 152(%r15)
	je	.L460
	movl	$0, -76(%rbp)
	movl	$256, %eax
.L464:
	testb	$2, 133(%r15)
	cmovne	-76(%rbp), %eax
	movq	%r15, %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	check_crl_time
	movq	%r15, %rdi
	movl	%eax, %r8d
	movl	%ebx, %eax
	orl	$64, %eax
	testl	%r8d, %r8d
	cmove	%ebx, %eax
	movl	%eax, -76(%rbp)
	call	X509_CRL_get_issuer@PLT
	movq	152(%r13), %rdi
	movl	172(%r13), %r14d
	movq	%rax, -96(%rbp)
	call	OPENSSL_sk_num@PLT
	movq	152(%r13), %rdi
	leal	-1(%rax), %ebx
	leal	1(%r14), %eax
	cmpl	%ebx, %r14d
	cmovne	%eax, %ebx
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	136(%r15), %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	X509_check_akid@PLT
	testl	%eax, %eax
	jne	.L468
	testb	$32, -76(%rbp)
	jne	.L604
.L468:
	movl	%r12d, -148(%rbp)
	movq	-96(%rbp), %r14
	addl	$1, %ebx
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L472:
	movq	152(%r13), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	X509_get_subject_name@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	X509_NAME_cmp@PLT
	testl	%eax, %eax
	jne	.L471
	movq	136(%r15), %rsi
	movq	%r12, %rdi
	call	X509_check_akid@PLT
	testl	%eax, %eax
	je	.L605
.L471:
	addl	$1, %ebx
.L470:
	movq	152(%r13), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L472
	movq	32(%r13), %rax
	movl	-148(%rbp), %r12d
	movq	-96(%rbp), %r14
	testb	$16, 25(%rax)
	jne	.L473
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L476:
	movq	16(%r13), %rdi
	movl	%r12d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	X509_get_subject_name@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	X509_NAME_cmp@PLT
	testl	%eax, %eax
	jne	.L474
	movq	136(%r15), %rsi
	movq	%rbx, %rdi
	call	X509_check_akid@PLT
	testl	%eax, %eax
	je	.L606
.L474:
	addl	$1, %r12d
.L473:
	movq	16(%r13), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jl	.L476
.L469:
	testb	$4, -76(%rbp)
	jne	.L475
	.p2align 4,,10
	.p2align 3
.L460:
	movq	-88(%rbp), %rdi
	addl	$1, -72(%rbp)
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, -72(%rbp)
	jl	.L504
.L603:
	movq	-112(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L506
	movq	-208(%rbp), %r14
	movq	%rbx, %r15
	movq	(%r14), %rdi
	call	X509_CRL_free@PLT
	movq	-216(%rbp), %rdx
	movq	%rbx, (%r14)
	movq	%rbx, %rdi
	movq	-144(%rbp), %rcx
	movq	%rcx, (%rdx)
	movq	-200(%rbp), %rdx
	movl	-80(%rbp), %ecx
	movl	%ecx, (%rdx)
	movq	-136(%rbp), %rdx
	movl	-104(%rbp), %ecx
	movl	%ecx, (%rdx)
	call	X509_CRL_up_ref@PLT
	movq	-192(%rbp), %r14
	movq	(%r14), %rdi
	call	X509_CRL_free@PLT
	movq	32(%r13), %rax
	movq	$0, (%r14)
	testb	$32, 25(%rax)
	je	.L506
	movq	184(%r13), %rax
	movl	224(%rax), %eax
	orl	132(%rbx), %eax
	testb	$16, %ah
	je	.L506
	movq	-88(%rbp), %r14
	xorl	%ebx, %ebx
	movq	%r13, -72(%rbp)
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L607
	.p2align 4,,10
	.p2align 3
.L511:
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	cmpq	$0, 168(%rax)
	movq	%rax, %r12
	je	.L509
	cmpq	$0, 160(%r15)
	je	.L509
	movq	%rax, %rdi
	call	X509_CRL_get_issuer@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	X509_CRL_get_issuer@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	X509_NAME_cmp@PLT
	testl	%eax, %eax
	jne	.L509
	movl	$90, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	crl_extension_match
	testl	%eax, %eax
	je	.L509
	movl	$770, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	crl_extension_match
	testl	%eax, %eax
	jne	.L608
	.p2align 4,,10
	.p2align 3
.L509:
	movq	%r14, %rdi
	addl	$1, %ebx
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L511
.L607:
	movq	-192(%rbp), %rax
	movq	$0, (%rax)
	.p2align 4,,10
	.p2align 3
.L506:
	xorl	%eax, %eax
	cmpl	$447, -80(%rbp)
	setg	%al
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L609
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L461:
	.cfi_restore_state
	testb	$64, %al
	je	.L463
	movl	-100(%rbp), %eax
	notl	%eax
	testl	%eax, 156(%r15)
	jne	.L462
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L513:
	movl	$32, -76(%rbp)
	movl	$288, %eax
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L463:
	cmpq	$0, 168(%r15)
	jne	.L460
	jmp	.L462
	.p2align 4,,10
	.p2align 3
.L606:
	orl	$4, -76(%rbp)
	movq	%rbx, -120(%rbp)
.L475:
	movl	152(%r15), %eax
	testb	$16, %al
	jne	.L477
	movq	-128(%rbp), %rcx
	testb	$16, 224(%rcx)
	jne	.L610
	testb	$8, %al
	jne	.L477
.L479:
	movl	156(%r15), %eax
	movq	%r15, -96(%rbp)
	movq	-128(%rbp), %r15
	movq	%r13, -160(%rbp)
	movl	%eax, -148(%rbp)
	movl	-76(%rbp), %eax
	andl	$32, %eax
	movl	%eax, -152(%rbp)
	xorl	%eax, %eax
	movl	%eax, %r13d
	.p2align 4,,10
	.p2align 3
.L480:
	movq	264(%r15), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jge	.L611
	movq	264(%r15), %rdi
	movl	%r13d, %esi
	xorl	%r12d, %r12d
	call	OPENSSL_sk_value@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %rbx
	call	X509_CRL_get_issuer@PLT
	movq	16(%rbx), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	jne	.L481
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L485:
	movq	16(%rbx), %rdi
	addl	$1, %r12d
.L481:
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L483
	movq	16(%rbx), %rdi
	movl	%r12d, %esi
	call	OPENSSL_sk_value@PLT
	cmpl	$4, (%rax)
	jne	.L485
	movq	8(%rax), %rdi
	movq	%r14, %rsi
	call	X509_NAME_cmp@PLT
	testl	%eax, %eax
	jne	.L485
.L482:
	movq	-96(%rbp), %rax
	movq	144(%rax), %rax
	testq	%rax, %rax
	je	.L599
	movq	(%rax), %r12
	movq	(%rbx), %rax
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L599
	testq	%r12, %r12
	je	.L599
	cmpl	$1, (%rax)
	je	.L613
	xorl	%eax, %eax
	cmpl	$1, (%r12)
	je	.L614
	movq	%rdx, %r14
	movl	%r13d, -220(%rbp)
	movl	%eax, %r13d
	movq	8(%r14), %rdi
	movq	%r15, -176(%rbp)
	movq	%rbx, -184(%rbp)
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jge	.L615
.L498:
	movq	8(%r14), %rdi
	movl	%r13d, %esi
	xorl	%r15d, %r15d
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rbx
	jmp	.L496
	.p2align 4,,10
	.p2align 3
.L497:
	movq	8(%r12), %rdi
	movl	%r15d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	GENERAL_NAME_cmp@PLT
	testl	%eax, %eax
	je	.L601
	addl	$1, %r15d
.L496:
	movq	8(%r12), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r15d
	jl	.L497
	movq	8(%r14), %rdi
	addl	$1, %r13d
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jl	.L498
.L615:
	movq	-176(%rbp), %r15
	movl	-220(%rbp), %r13d
	.p2align 4,,10
	.p2align 3
.L483:
	addl	$1, %r13d
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L617:
	movq	16(%r12), %rsi
	testq	%rsi, %rsi
	je	.L483
	call	X509_NAME_cmp@PLT
	testl	%eax, %eax
	jne	.L483
	.p2align 4,,10
	.p2align 3
.L599:
	movq	-96(%rbp), %r15
	movq	-160(%rbp), %r13
.L487:
	movl	24(%rbx), %ecx
	andl	%ecx, -148(%rbp)
.L499:
	movl	-100(%rbp), %ecx
	movl	-148(%rbp), %edx
	movl	%ecx, %eax
	notl	%eax
	testl	%eax, %edx
	je	.L460
	orl	%edx, %ecx
	orl	$128, -76(%rbp)
	movl	%ecx, -100(%rbp)
.L477:
	movl	-76(%rbp), %ecx
	cmpl	%ecx, -80(%rbp)
	jg	.L460
	jne	.L516
	movq	-112(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L516
	movq	%r15, %rdi
	call	X509_CRL_get0_lastUpdate@PLT
	movq	%rbx, %rdi
	movq	%rax, %r12
	call	X509_CRL_get0_lastUpdate@PLT
	movq	-168(%rbp), %rsi
	leaq	-64(%rbp), %rdi
	movq	%r12, %rcx
	movq	%rax, %rdx
	call	ASN1_TIME_diff@PLT
	testl	%eax, %eax
	je	.L460
	movl	-64(%rbp), %edx
	testl	%edx, %edx
	jle	.L616
	.p2align 4,,10
	.p2align 3
.L516:
	movq	-120(%rbp), %rax
	movq	%r15, -112(%rbp)
	movq	%rax, -144(%rbp)
	movl	-100(%rbp), %eax
	movl	%eax, -104(%rbp)
	movl	-76(%rbp), %eax
	movl	%eax, -80(%rbp)
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L605:
	orl	$12, -76(%rbp)
	movq	%r12, -120(%rbp)
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L610:
	testb	$4, %al
	je	.L479
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L612:
	movl	-152(%rbp), %esi
	testl	%esi, %esi
	jne	.L482
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L601:
	movq	-176(%rbp), %rax
	movq	-96(%rbp), %r15
	movq	-184(%rbp), %rbx
	movq	-160(%rbp), %r13
	movq	%rax, -128(%rbp)
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L613:
	movq	16(%rax), %rax
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L483
	cmpl	$1, (%r12)
	je	.L617
	movq	8(%r12), %r14
.L490:
	movq	%rbx, -176(%rbp)
	xorl	%r12d, %r12d
	movq	%rdi, %rbx
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L494:
	addl	$1, %r12d
.L493:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L483
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	cmpl	$4, (%rax)
	jne	.L494
	movq	8(%rax), %rsi
	movq	%rbx, %rdi
	call	X509_NAME_cmp@PLT
	testl	%eax, %eax
	jne	.L494
	movq	-96(%rbp), %r15
	movq	-176(%rbp), %rbx
	movq	-160(%rbp), %r13
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L614:
	movq	16(%r12), %rax
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L483
	movq	8(%rdx), %r14
	jmp	.L490
	.p2align 4,,10
	.p2align 3
.L616:
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jg	.L516
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L604:
	orl	$28, -76(%rbp)
	movq	%r14, -120(%rbp)
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L611:
	movq	-96(%rbp), %r15
	movq	-160(%rbp), %r13
	movq	144(%r15), %rax
	testq	%rax, %rax
	je	.L501
	cmpq	$0, (%rax)
	jne	.L477
.L501:
	movl	-152(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L477
	jmp	.L499
	.p2align 4,,10
	.p2align 3
.L608:
	movq	160(%r15), %rsi
	movq	168(%r12), %rdi
	call	ASN1_INTEGER_cmp@PLT
	testl	%eax, %eax
	jg	.L509
	movq	160(%r15), %rsi
	movq	160(%r12), %rdi
	call	ASN1_INTEGER_cmp@PLT
	testl	%eax, %eax
	jle	.L509
	movq	-72(%rbp), %r13
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	check_crl_time
	testl	%eax, %eax
	je	.L510
	movq	-200(%rbp), %rax
	orl	$2, (%rax)
.L510:
	movq	%r12, %rdi
	call	X509_CRL_up_ref@PLT
	movq	-192(%rbp), %rax
	movq	%r12, (%rax)
	jmp	.L506
.L609:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1417:
	.size	get_crl_sk, .-get_crl_sk
	.p2align 4
	.type	check_revocation, @function
check_revocation:
.LFB1414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rax
	movq	24(%rax), %rax
	testb	$4, %al
	je	.L623
	movq	%rdi, %r15
	testb	$8, %al
	jne	.L678
	cmpq	$0, 216(%rdi)
	jne	.L623
	movl	$0, -132(%rbp)
.L622:
	leaq	-92(%rbp), %rax
	xorl	%r14d, %r14d
	movq	%rax, -128(%rbp)
	.p2align 4,,10
	.p2align 3
.L638:
	movl	%r14d, 172(%r15)
	movq	152(%r15), %rdi
	movl	%r14d, %esi
	movq	$0, -88(%rbp)
	call	OPENSSL_sk_value@PLT
	movq	$0, 208(%r15)
	movq	%rax, 184(%r15)
	movq	%rax, %rbx
	movl	224(%rax), %eax
	movq	$0, 192(%r15)
	andl	$1024, %eax
	movl	%eax, -100(%rbp)
	jne	.L625
	movl	%r14d, -104(%rbp)
	movq	%r15, %r14
	movq	88(%r14), %rax
	testq	%rax, %rax
	je	.L626
	.p2align 4,,10
	.p2align 3
.L682:
	leaq	-88(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L672
	movq	-88(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rsi, 200(%r14)
	call	*96(%r14)
	testl	%eax, %eax
	jne	.L679
	movq	%r14, %r15
	xorl	%r12d, %r12d
.L632:
	movq	-88(%rbp), %rdi
	call	X509_CRL_free@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	X509_CRL_free@PLT
	movq	$0, 200(%r15)
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L678:
	movq	152(%rdi), %rdi
	call	OPENSSL_sk_num@PLT
	subl	$1, %eax
	movl	%eax, -132(%rbp)
	jns	.L622
	.p2align 4,,10
	.p2align 3
.L623:
	movl	$1, %r12d
.L618:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L680
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L679:
	.cfi_restore_state
	xorl	%r12d, %r12d
.L635:
	movq	-88(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	call	*104(%r14)
	testl	%eax, %eax
	je	.L675
.L636:
	movq	-88(%rbp), %rdi
	call	X509_CRL_free@PLT
	movq	%r12, %rdi
	call	X509_CRL_free@PLT
	movq	$0, -88(%rbp)
	movl	212(%r14), %eax
	cmpl	-100(%rbp), %eax
	je	.L672
	cmpl	$32895, %eax
	je	.L681
	movl	%eax, -100(%rbp)
	movq	88(%r14), %rax
	testq	%rax, %rax
	jne	.L682
.L626:
	movq	%rbx, %rdi
	movq	$0, -80(%rbp)
	leaq	-64(%rbp), %r15
	leaq	-72(%rbp), %r13
	movl	$0, -96(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	call	X509_get_issuer_name@PLT
	subq	$8, %rsp
	movq	-128(%rbp), %r9
	movq	%r13, %rsi
	movq	%rax, %r12
	movl	212(%r14), %eax
	movq	%r15, %rdx
	movq	%r14, %rdi
	pushq	24(%r14)
	leaq	-80(%rbp), %rcx
	leaq	-96(%rbp), %r8
	movq	%rcx, -112(%rbp)
	movl	%eax, -92(%rbp)
	movq	%r8, -120(%rbp)
	call	get_crl_sk
	popq	%rcx
	popq	%rsi
	testl	%eax, %eax
	jne	.L629
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	*128(%r14)
	movq	-112(%rbp), %rcx
	movq	-120(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L683
.L630:
	subq	$8, %rsp
	movq	-128(%rbp), %r9
	movq	%r15, %rdx
	movq	%r13, %rsi
	pushq	%r12
	movq	%r14, %rdi
	call	get_crl_sk
	movq	X509_CRL_free@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	call	OPENSSL_sk_pop_free@PLT
	popq	%rax
	popq	%rdx
.L629:
	movq	-72(%rbp), %rsi
	testq	%rsi, %rsi
	jne	.L631
.L672:
.L634:
	endbr64
	movq	%r14, %r15
	xorl	%edi, %edi
	movl	-104(%rbp), %r14d
	movl	$3, 176(%r15)
	movq	%r15, %rsi
	call	*56(%r15)
	movq	-88(%rbp), %rdi
	movl	%eax, %r12d
	call	X509_CRL_free@PLT
	xorl	%edi, %edi
	call	X509_CRL_free@PLT
	movq	$0, 200(%r15)
	testl	%r12d, %r12d
	je	.L618
.L625:
	addl	$1, %r14d
	cmpl	-132(%rbp), %r14d
	jle	.L638
	jmp	.L623
	.p2align 4,,10
	.p2align 3
.L683:
	movq	-72(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L630
.L631:
	movq	-80(%rbp), %rax
	movq	-64(%rbp), %r12
	movq	%rsi, -88(%rbp)
	movq	%r14, %rdi
	movq	%rsi, 200(%r14)
	movq	%rax, 192(%r14)
	movl	-96(%rbp), %eax
	movl	%eax, 208(%r14)
	movl	-92(%rbp), %eax
	movl	%eax, 212(%r14)
	call	*96(%r14)
	testl	%eax, %eax
	je	.L675
	testq	%r12, %r12
	je	.L635
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	*96(%r14)
	testl	%eax, %eax
	je	.L675
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	*104(%r14)
	testl	%eax, %eax
	je	.L675
	cmpl	$2, %eax
	jne	.L635
	jmp	.L636
	.p2align 4,,10
	.p2align 3
.L675:
	movq	%r14, %r15
	jmp	.L632
	.p2align 4,,10
	.p2align 3
.L681:
	xorl	%edi, %edi
	movq	%r14, %r15
	movl	-104(%rbp), %r14d
	call	X509_CRL_free@PLT
	xorl	%edi, %edi
	call	X509_CRL_free@PLT
	movq	$0, 200(%r15)
	jmp	.L625
.L680:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1414:
	.size	check_revocation, .-check_revocation
	.p2align 4
	.globl	X509_gmtime_adj
	.type	X509_gmtime_adj, @function
X509_gmtime_adj:
.LFB1436:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	time@PLT
	movq	-32(%rbp), %rsi
	testq	%r12, %r12
	je	.L686
	testb	$64, 16(%r12)
	je	.L691
.L686:
	movq	%r13, %rcx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	ASN1_TIME_adj@PLT
.L684:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L692
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L691:
	.cfi_restore_state
	movl	4(%r12), %eax
	cmpl	$23, %eax
	je	.L693
	cmpl	$24, %eax
	jne	.L686
	movq	%r13, %rcx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	ASN1_GENERALIZEDTIME_adj@PLT
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L693:
	movq	%r13, %rcx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	ASN1_UTCTIME_adj@PLT
	jmp	.L684
.L692:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1436:
	.size	X509_gmtime_adj, .-X509_gmtime_adj
	.p2align 4
	.globl	X509_time_adj
	.type	X509_time_adj, @function
X509_time_adj:
.LFB1437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L695
	movq	(%rdx), %rsi
	movq	%rsi, -32(%rbp)
.L696:
	testq	%r12, %r12
	je	.L697
	testb	$64, 16(%r12)
	je	.L705
.L697:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	ASN1_TIME_adj@PLT
.L694:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L706
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L705:
	.cfi_restore_state
	movl	4(%r12), %eax
	cmpl	$23, %eax
	je	.L707
	cmpl	$24, %eax
	jne	.L697
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	ASN1_GENERALIZEDTIME_adj@PLT
	jmp	.L694
	.p2align 4,,10
	.p2align 3
.L695:
	leaq	-32(%rbp), %rdi
	movq	%rsi, -40(%rbp)
	call	time@PLT
	movq	-32(%rbp), %rsi
	movq	-40(%rbp), %rcx
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L707:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	ASN1_UTCTIME_adj@PLT
	jmp	.L694
.L706:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1437:
	.size	X509_time_adj, .-X509_time_adj
	.p2align 4
	.globl	X509_time_adj_ex
	.type	X509_time_adj_ex, @function
X509_time_adj_ex:
.LFB1438:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L709
	movq	(%rcx), %rsi
	movq	%rsi, -48(%rbp)
.L710:
	testq	%r12, %r12
	je	.L711
	testb	$64, 16(%r12)
	je	.L719
.L711:
	movq	%r14, %rcx
	movl	%r13d, %edx
	movq	%r12, %rdi
	call	ASN1_TIME_adj@PLT
.L708:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L720
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L719:
	.cfi_restore_state
	movl	4(%r12), %eax
	cmpl	$23, %eax
	je	.L721
	cmpl	$24, %eax
	jne	.L711
	movq	%r14, %rcx
	movl	%r13d, %edx
	movq	%r12, %rdi
	call	ASN1_GENERALIZEDTIME_adj@PLT
	jmp	.L708
	.p2align 4,,10
	.p2align 3
.L709:
	leaq	-48(%rbp), %rdi
	call	time@PLT
	movq	-48(%rbp), %rsi
	jmp	.L710
	.p2align 4,,10
	.p2align 3
.L721:
	movq	%r14, %rcx
	movl	%r13d, %edx
	movq	%r12, %rdi
	call	ASN1_UTCTIME_adj@PLT
	jmp	.L708
.L720:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1438:
	.size	X509_time_adj_ex, .-X509_time_adj_ex
	.p2align 4
	.globl	X509_get_pubkey_parameters
	.type	X509_get_pubkey_parameters, @function
X509_get_pubkey_parameters:
.LFB1439:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdi, %rdi
	je	.L725
	call	EVP_PKEY_missing_parameters@PLT
	testl	%eax, %eax
	je	.L744
.L725:
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	jmp	.L724
	.p2align 4,,10
	.p2align 3
.L729:
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	call	X509_get0_pubkey@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L745
	movq	%rax, %rdi
	call	EVP_PKEY_missing_parameters@PLT
	testl	%eax, %eax
	je	.L728
	addl	$1, %ebx
.L724:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L729
	testq	%r12, %r12
	je	.L746
.L728:
	leal	-1(%rbx), %r15d
	testl	%ebx, %ebx
	je	.L730
	.p2align 4,,10
	.p2align 3
.L731:
	movl	%r15d, %esi
	movq	%r13, %rdi
	subl	$1, %r15d
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	call	X509_get0_pubkey@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	EVP_PKEY_copy_parameters@PLT
	cmpl	$-1, %r15d
	jne	.L731
.L730:
	testq	%r14, %r14
	je	.L744
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	EVP_PKEY_copy_parameters@PLT
.L744:
	movl	$1, %eax
.L722:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L745:
	.cfi_restore_state
	movl	$1896, %r8d
	movl	$108, %edx
	movl	$110, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L746:
	.cfi_restore_state
	movl	$1904, %r8d
	movl	$107, %edx
	movl	$110, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L722
	.cfi_endproc
.LFE1439:
	.size	X509_get_pubkey_parameters, .-X509_get_pubkey_parameters
	.p2align 4
	.type	verify_chain, @function
verify_chain:
.LFB1400:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	232(%rdi), %rax
	movq	152(%rdi), %rdi
	movq	%rax, -88(%rbp)
	call	OPENSSL_sk_num@PLT
	movq	152(%r15), %rdi
	leal	-1(%rax), %esi
	movl	%eax, %ebx
	call	OPENSSL_sk_value@PLT
	xorl	%edx, %edx
	movl	$-1, %esi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	X509_check_purpose@PLT
	movl	$0, -72(%rbp)
	cmpl	$1, %eax
	jne	.L748
	movl	224(%r12), %eax
	shrl	$13, %eax
	andl	$1, %eax
	movl	%eax, -72(%rbp)
.L748:
	cmpl	$1, %ebx
	jne	.L749
	cmpl	$1, 148(%r15)
	je	.L1133
.L749:
	movl	$2882, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$68, %edx
	xorl	%r12d, %r12d
	movl	$106, %esi
	movl	$11, %edi
	call	ERR_put_error@PLT
	movq	152(%r15), %rsi
	xorl	%edi, %edi
	movl	$1, 176(%r15)
	call	X509_get_pubkey_parameters
.L747:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1134
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1133:
	.cfi_restore_state
	movq	16(%r15), %r13
	xorl	%ebx, %ebx
	testq	%r13, %r13
	setne	%bl
	cmpq	$0, -88(%rbp)
	je	.L751
	movq	-88(%rbp), %rax
	movl	40(%rax), %eax
	testb	$3, %al
	jne	.L751
	testb	$12, %al
	je	.L751
	testq	%r13, %r13
	jne	.L919
	movl	$0, -124(%rbp)
	xorl	%ebx, %ebx
	movl	$0, -128(%rbp)
.L920:
	movq	-88(%rbp), %r14
	movq	8(%r14), %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jle	.L758
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.L758
	testq	%r13, %r13
	je	.L1135
.L759:
	movq	-88(%rbp), %r12
	xorl	%r14d, %r14d
	jmp	.L761
	.p2align 4,,10
	.p2align 3
.L762:
	movq	16(%r12), %rdi
	addl	$1, %r14d
.L761:
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r14d
	jge	.L758
	movq	16(%r12), %rdi
	movl	%r14d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L762
	movq	%r13, %rdi
	movl	%eax, %r12d
	call	OPENSSL_sk_free@PLT
	movl	$2936, %r8d
	movl	$65, %edx
	leaq	.LC0(%rip), %rcx
	movl	$106, %esi
	movl	$11, %edi
	call	ERR_put_error@PLT
	movq	152(%r15), %rsi
	xorl	%edi, %edi
	movl	$17, 176(%r15)
	call	X509_get_pubkey_parameters
	jmp	.L747
	.p2align 4,,10
	.p2align 3
.L751:
	testq	%r13, %r13
	je	.L754
	movq	32(%r15), %rax
	movq	24(%rax), %rax
	testb	$-128, %ah
	jne	.L1136
	shrq	$20, %rax
	xorq	$1, %rax
	andl	$1, %eax
	movl	%eax, -128(%rbp)
.L756:
	movq	%r13, %rdi
	call	OPENSSL_sk_dup@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L927
.L757:
	cmpq	$0, -88(%rbp)
	movl	$1, -124(%rbp)
	jne	.L920
.L758:
	movq	32(%r15), %rax
	movl	40(%rax), %edx
	cmpl	$1073741823, %edx
	jg	.L764
	leal	1(%rdx), %eax
	movl	%eax, -104(%rbp)
.L765:
	testl	%ebx, %ebx
	je	.L935
	movl	$3, -100(%rbp)
	movq	152(%r15), %rdi
	movl	$0, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L813:
	movq	$0, -64(%rbp)
	testb	$2, %bl
	jne	.L1137
.L767:
	testb	$1, %bl
	je	.L813
.L926:
	call	OPENSSL_sk_num@PLT
	movl	%eax, %r12d
	cmpl	148(%r15), %eax
	je	.L795
	movl	$3138, %r8d
	movl	$68, %edx
	movl	$106, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r13, %rdi
	movl	$1, 176(%r15)
	call	OPENSSL_sk_free@PLT
	movq	152(%r15), %rdi
	call	OPENSSL_sk_num@PLT
	movq	152(%r15), %rdi
.L833:
	movq	%rdi, %rsi
	xorl	%edi, %edi
	call	X509_get_pubkey_parameters
	.p2align 4,,10
	.p2align 3
.L1129:
	xorl	%r12d, %r12d
	jmp	.L747
	.p2align 4,,10
	.p2align 3
.L754:
	movl	$0, -128(%rbp)
	orl	$2, %ebx
	jmp	.L757
	.p2align 4,,10
	.p2align 3
.L1137:
	call	OPENSSL_sk_num@PLT
	movl	-80(%rbp), %r14d
	movq	152(%r15), %rdi
	movl	%eax, %r12d
	movl	%ebx, %eax
	andl	$4, %eax
	cmove	%r12d, %r14d
	movl	%eax, -96(%rbp)
	leal	-1(%r14), %esi
	call	OPENSSL_sk_value@PLT
	cmpl	-104(%rbp), %r12d
	jg	.L1119
	movq	%rax, -112(%rbp)
	leaq	-64(%rbp), %rdi
	movq	%rax, %rdx
	movq	%r15, %rsi
	movq	152(%r15), %r9
	movq	$0, 152(%r15)
	movq	%r9, -120(%rbp)
	call	*64(%r15)
	movq	-120(%rbp), %r9
	movq	-112(%rbp), %r10
	testl	%eax, %eax
	movq	%r9, 152(%r15)
	js	.L1138
	je	.L1119
	movl	-96(%rbp), %eax
	testl	%eax, %eax
	je	.L774
	cmpl	%r14d, %r12d
	jle	.L775
	testl	%r14d, %r14d
	jle	.L775
	movl	-72(%rbp), %eax
	testl	%eax, %eax
	je	.L776
.L775:
	movl	$3023, %r8d
	movl	$68, %edx
	movl	$106, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-64(%rbp), %rdi
	call	X509_free@PLT
	movl	$2, -100(%rbp)
	movl	$1, 176(%r15)
.L772:
	movq	%r13, %rdi
	call	OPENSSL_sk_free@PLT
	movq	152(%r15), %rdi
	call	OPENSSL_sk_num@PLT
.L826:
	movl	-100(%rbp), %eax
	movq	152(%r15), %rdi
	cmpl	$1, %eax
	je	.L823
	cmpl	$2, %eax
	jne	.L825
	jmp	.L833
	.p2align 4,,10
	.p2align 3
.L1119:
	movl	%ebx, %eax
	andl	$1, %eax
	testl	%eax, %eax
	je	.L791
.L788:
	movq	152(%r15), %rdi
	jmp	.L926
	.p2align 4,,10
	.p2align 3
.L795:
	movq	152(%r15), %rdi
	leal	-1(%rax), %esi
	call	OPENSSL_sk_value@PLT
	testb	$1, -72(%rbp)
	jne	.L804
	cmpl	%r12d, -104(%rbp)
	jl	.L804
	movq	$0, -96(%rbp)
	xorl	%r12d, %r12d
	movq	%rax, %r14
	movl	%ebx, -112(%rbp)
	jmp	.L798
	.p2align 4,,10
	.p2align 3
.L801:
	addl	$1, %r12d
.L798:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L1139
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	movq	%rax, %rdx
	call	*72(%r15)
	testl	%eax, %eax
	je	.L801
	movl	$-1, %edx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	x509_check_cert_time
	testl	%eax, %eax
	jne	.L938
	movq	%rbx, -96(%rbp)
	jmp	.L801
	.p2align 4,,10
	.p2align 3
.L804:
	movl	-124(%rbp), %r10d
	andl	$-2, %ebx
	testl	%r10d, %r10d
	je	.L790
	orl	$2, %ebx
.L812:
	movq	152(%r15), %rdi
	jmp	.L813
	.p2align 4,,10
	.p2align 3
.L791:
	testb	$4, %bl
	je	.L787
	subl	$1, -80(%rbp)
	movl	-80(%rbp), %eax
	testl	%eax, %eax
	jg	.L812
.L766:
	movq	%r13, %rdi
	call	OPENSSL_sk_free@PLT
	movq	152(%r15), %rdi
	call	OPENSSL_sk_num@PLT
	movl	%eax, -96(%rbp)
	cmpl	%eax, -104(%rbp)
	jl	.L814
	cmpl	$3, -100(%rbp)
	jne	.L814
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	je	.L815
	movl	148(%r15), %esi
	testb	$4, 40(%rax)
	jne	.L1140
.L816:
	cmpl	-96(%rbp), %esi
	je	.L824
.L814:
	movq	152(%r15), %rdi
.L825:
	call	OPENSSL_sk_num@PLT
	movl	%eax, %ebx
	cmpl	%eax, -104(%rbp)
	jl	.L1141
	cmpq	$0, -88(%rbp)
	jne	.L1142
.L829:
	movl	-72(%rbp), %r8d
	movq	152(%r15), %rdi
	leal	-1(%rbx), %r12d
	testl	%r8d, %r8d
	jne	.L1143
	cmpl	148(%r15), %ebx
	jg	.L1144
	movl	%r12d, 172(%r15)
	movl	%r12d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%r15, %rsi
	xorl	%edi, %edi
	movl	$20, 176(%r15)
	movq	%rax, 184(%r15)
	call	*56(%r15)
.L828:
	movq	152(%r15), %rdi
	testl	%eax, %eax
	je	.L833
.L823:
	call	OPENSSL_sk_num@PLT
	cmpq	$0, 216(%r15)
	movl	$0, -96(%rbp)
	movl	%eax, -72(%rbp)
	movl	$6, -80(%rbp)
	je	.L1145
.L834:
	movl	-72(%rbp), %esi
	testl	%esi, %esi
	jle	.L835
	movl	$0, -88(%rbp)
	xorl	%r12d, %r12d
	movl	$-1, %r13d
	xorl	%ebx, %ebx
	jmp	.L861
	.p2align 4,,10
	.p2align 3
.L1151:
	testl	%eax, %eax
	je	.L843
	leal	1(%rbx), %edx
	cmpl	%edx, -72(%rbp)
	jle	.L1146
.L844:
	cmpl	$1, %eax
	je	.L841
.L843:
	movl	$24, 176(%r15)
.L842:
	testb	$16, 224(%r14)
	jne	.L845
	cmpq	$-1, 208(%r14)
	je	.L845
	movq	32(%r15), %rax
	testb	$32, 24(%rax)
	je	.L845
.L931:
	movl	$41, 176(%r15)
.L845:
	xorl	%edi, %edi
	movl	%ebx, 172(%r15)
	movq	%r15, %rsi
	movq	%r14, 184(%r15)
	call	*56(%r15)
	testl	%eax, %eax
	je	.L1125
.L911:
	movl	-80(%rbp), %ecx
	testl	%ecx, %ecx
	jg	.L1147
.L851:
	cmpl	$1, %ebx
	jle	.L853
.L1156:
	movq	208(%r14), %rsi
	cmpq	$-1, %rsi
	je	.L1126
	movslq	%r12d, %rax
	movslq	-88(%rbp), %rdx
	addq	%rsi, %rax
	cmpq	%rax, %rdx
	jg	.L1101
.L1126:
	movl	224(%r14), %eax
.L855:
	movl	%eax, %edx
	andl	$32, %edx
	cmpl	$1, %edx
	adcl	$0, -88(%rbp)
.L856:
	testb	$4, %ah
	je	.L945
	movq	216(%r14), %rax
	cmpq	$-1, %rax
	je	.L858
	movslq	%r12d, %r12
	cmpq	%r12, %rax
	jl	.L1148
.L859:
	movl	%eax, %r12d
.L858:
	addl	$1, %r12d
	xorl	%r13d, %r13d
	addl	$1, %ebx
	cmpl	%ebx, -72(%rbp)
	je	.L835
.L861:
	movq	152(%r15), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r14
	movq	32(%r15), %rax
	testb	$16, 24(%rax)
	jne	.L836
	testb	$2, 225(%r14)
	jne	.L1149
.L836:
	movl	-96(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L837
	testb	$4, 225(%r14)
	jne	.L1150
.L837:
	movq	%r14, %rdi
	call	X509_check_ca@PLT
	cmpl	$-1, %r13d
	je	.L838
	testl	%r13d, %r13d
	jne	.L1151
	testl	%eax, %eax
	je	.L841
	movl	$37, 176(%r15)
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L774:
	movl	-72(%rbp), %r14d
	testl	%r14d, %r14d
	je	.L780
	movl	148(%r15), %eax
	cmpl	%eax, %r12d
	je	.L1152
.L785:
	cmpl	%r12d, %eax
	jg	.L1153
.L1108:
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	check_trust
	movl	%eax, -100(%rbp)
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L772
	movl	-72(%rbp), %r11d
	andl	$-2, %ebx
	testl	%r11d, %r11d
	jne	.L791
	.p2align 4,,10
	.p2align 3
.L790:
	testl	%ebx, %ebx
	jne	.L812
	jmp	.L766
	.p2align 4,,10
	.p2align 3
.L1152:
	movq	-64(%rbp), %rsi
	movq	%r10, %rdi
	movq	%r10, -96(%rbp)
	call	X509_cmp@PLT
	movq	-96(%rbp), %r10
	testl	%eax, %eax
	je	.L786
	movq	-64(%rbp), %rdi
	call	X509_free@PLT
	testb	$1, %bl
	jne	.L788
	.p2align 4,,10
	.p2align 3
.L787:
	movl	-128(%rbp), %eax
	testl	%eax, %eax
	je	.L766
	movl	148(%r15), %eax
	cmpl	$1, %eax
	jle	.L766
	subl	$1, %eax
	movl	$0, -72(%rbp)
	movq	152(%r15), %rdi
	orl	$4, %ebx
	movl	%eax, -80(%rbp)
	jmp	.L767
	.p2align 4,,10
	.p2align 3
.L1138:
	movl	$70, 176(%r15)
	movl	$2, -100(%rbp)
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L945:
	movl	$1, %r13d
	addl	$1, %ebx
	cmpl	%ebx, -72(%rbp)
	jne	.L861
.L835:
	movq	152(%r15), %rdi
	call	OPENSSL_sk_num@PLT
	movl	%eax, %r12d
	movq	32(%r15), %rax
	movl	44(%rax), %edx
	testl	%edx, %edx
	jle	.L906
	testl	%r12d, %r12d
	jle	.L906
	movq	152(%r15), %rdi
	xorl	%esi, %esi
	leal	-1(%r12), %r13d
	xorl	%r14d, %r14d
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L928:
	cmpl	%r13d, %r14d
	jl	.L1154
.L869:
	addl	$1, %r14d
	cmpl	%r14d, %r12d
	je	.L906
	movq	152(%r15), %rdi
	movl	%r14d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	X509_get0_pubkey@PLT
	movq	32(%r15), %rdx
	movl	44(%rdx), %edx
	testl	%edx, %edx
	jle	.L928
	testq	%rax, %rax
	je	.L866
	movq	%rax, %rdi
	movl	%edx, -72(%rbp)
	call	EVP_PKEY_security_bits@PLT
	movl	-72(%rbp), %edx
	movl	$5, %ecx
	cmpl	$5, %edx
	cmovg	%ecx, %edx
	leaq	minbits_table(%rip), %rcx
	subl	$1, %edx
	movslq	%edx, %rdx
	cmpl	(%rcx,%rdx,4), %eax
	jge	.L928
	.p2align 4,,10
	.p2align 3
.L866:
	movl	%r14d, 172(%r15)
	testq	%rbx, %rbx
	je	.L864
	movq	%rbx, %rax
.L865:
	xorl	%edi, %edi
	movq	%rax, 184(%r15)
	movq	%r15, %rsi
	movl	$67, 176(%r15)
	call	*56(%r15)
	testl	%eax, %eax
	jne	.L928
	.p2align 4,,10
	.p2align 3
.L1125:
	movq	152(%r15), %rdi
	jmp	.L833
	.p2align 4,,10
	.p2align 3
.L838:
	movq	32(%r15), %rdx
	testb	$32, 24(%rdx)
	je	.L841
	cmpl	$1, %eax
	ja	.L843
	.p2align 4,,10
	.p2align 3
.L841:
	testb	$16, 224(%r14)
	jne	.L911
	cmpq	$-1, 208(%r14)
	je	.L911
	movq	32(%r15), %rax
	testb	$32, 24(%rax)
	jne	.L931
	movl	-80(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.L851
	.p2align 4,,10
	.p2align 3
.L1147:
	cmpl	148(%r15), %ebx
	jl	.L847
	movq	32(%r15), %rax
	cmpl	32(%rax), %ecx
	je	.L1155
.L847:
	xorl	%edx, %edx
	movl	-80(%rbp), %esi
	testl	%r13d, %r13d
	movq	%r14, %rdi
	setg	%dl
	call	X509_check_purpose@PLT
	testl	%eax, %eax
	je	.L849
.L1122:
	cmpl	$1, %eax
	je	.L851
	movq	32(%r15), %rax
	testb	$32, 24(%rax)
	je	.L851
.L849:
	xorl	%edi, %edi
	movq	%r15, %rsi
	movl	%ebx, 172(%r15)
	movq	%r14, 184(%r15)
	movl	$26, 176(%r15)
	call	*56(%r15)
	testl	%eax, %eax
	je	.L1125
	cmpl	$1, %ebx
	jg	.L1156
.L853:
	movl	224(%r14), %eax
	testl	%ebx, %ebx
	je	.L856
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L1149:
	xorl	%edi, %edi
	movq	%r15, %rsi
	movl	%ebx, 172(%r15)
	movq	%r14, 184(%r15)
	movl	$34, 176(%r15)
	call	*56(%r15)
	testl	%eax, %eax
	jne	.L836
	jmp	.L1125
	.p2align 4,,10
	.p2align 3
.L1150:
	xorl	%edi, %edi
	movq	%r15, %rsi
	movl	%ebx, 172(%r15)
	movq	%r14, 184(%r15)
	movl	$40, 176(%r15)
	call	*56(%r15)
	testl	%eax, %eax
	jne	.L837
	jmp	.L1125
	.p2align 4,,10
	.p2align 3
.L1101:
	xorl	%edi, %edi
	movq	%r15, %rsi
	movl	%ebx, 172(%r15)
	movq	%r14, 184(%r15)
	movl	$25, 176(%r15)
	call	*56(%r15)
	testl	%eax, %eax
	jne	.L1126
	jmp	.L1125
	.p2align 4,,10
	.p2align 3
.L1148:
	xorl	%edi, %edi
	movq	%r15, %rsi
	movl	%ebx, 172(%r15)
	movq	%r14, 184(%r15)
	movl	$38, 176(%r15)
	call	*56(%r15)
	testl	%eax, %eax
	je	.L1125
	movq	216(%r14), %rax
	jmp	.L859
	.p2align 4,,10
	.p2align 3
.L1146:
	movq	32(%r15), %rdx
	testb	$32, 24(%rdx)
	je	.L841
	jmp	.L844
	.p2align 4,,10
	.p2align 3
.L1155:
	movl	36(%rax), %esi
	movl	$4, %edx
	movq	%r14, %rdi
	call	X509_check_trust@PLT
	cmpl	$1, %eax
	je	.L851
	cmpl	$2, %eax
	je	.L849
	xorl	%edx, %edx
	movl	-80(%rbp), %esi
	testl	%r13d, %r13d
	movq	%r14, %rdi
	setg	%dl
	call	X509_check_purpose@PLT
	testl	%eax, %eax
	je	.L849
	jmp	.L1122
	.p2align 4,,10
	.p2align 3
.L776:
	andl	$-5, %ebx
	movq	%r9, %rdi
	movl	%ebx, -72(%rbp)
	movl	%r12d, %ebx
	jmp	.L778
	.p2align 4,,10
	.p2align 3
.L1157:
	movq	152(%r15), %rdi
.L778:
	call	OPENSSL_sk_pop@PLT
	subl	$1, %ebx
	movq	%rax, %rdi
	call	X509_free@PLT
	cmpl	%ebx, %r14d
	jl	.L1157
	leal	-1(%r12), %edx
	movl	%r14d, %eax
	movl	$0, %ecx
	movl	-72(%rbp), %ebx
	subl	%edx, %eax
	cmpl	%r14d, %r12d
	cmovle	%ecx, %eax
	cmpq	$0, -88(%rbp)
	leal	(%rax,%rdx), %r12d
	movl	%r12d, 148(%r15)
	je	.L1118
	movq	-88(%rbp), %r14
	movq	8(%r14), %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jle	.L781
	movl	148(%r15), %eax
	cmpl	%eax, 44(%r14)
	jge	.L1158
.L781:
	movq	-88(%rbp), %rax
	movq	8(%rax), %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jle	.L1118
	movq	-88(%rbp), %rax
	movq	152(%r15), %r9
	movl	148(%r15), %ecx
	cmpl	%ecx, 48(%rax)
	jl	.L780
	movl	$-1, 48(%rax)
	.p2align 4,,10
	.p2align 3
.L780:
	movq	-64(%rbp), %r14
	movq	%r9, %rdi
	movq	%r14, %rsi
	call	OPENSSL_sk_push@PLT
	movl	%eax, -72(%rbp)
	testl	%eax, %eax
	je	.L1159
	xorl	%edx, %edx
	movl	$-1, %esi
	movq	%r14, %rdi
	call	X509_check_purpose@PLT
	cmpl	$1, %eax
	jne	.L784
	movl	224(%r14), %eax
	shrl	$13, %eax
	andl	$1, %eax
	movl	%eax, -72(%rbp)
	movl	148(%r15), %eax
	cmpl	%r12d, %eax
	jle	.L1108
.L1153:
	movl	$3093, %r8d
	movl	$68, %edx
	movl	$106, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$2, -100(%rbp)
	movl	$1, 176(%r15)
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L1139:
	movl	-112(%rbp), %ebx
.L802:
	movq	-96(%rbp), %rax
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L804
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_delete_ptr@PLT
	movq	152(%r15), %rdi
	movq	-64(%rbp), %rsi
	call	OPENSSL_sk_push@PLT
	movl	%eax, -72(%rbp)
	testl	%eax, %eax
	je	.L1160
	movq	-64(%rbp), %rdi
	call	X509_up_ref@PLT
	movq	-64(%rbp), %r12
	xorl	%edx, %edx
	movl	$-1, %esi
	addl	$1, 148(%r15)
	movq	%r12, %rdi
	call	X509_check_purpose@PLT
	movl	$0, -72(%rbp)
	cmpl	$1, %eax
	jne	.L807
	movl	224(%r12), %eax
	shrl	$13, %eax
	andl	$1, %eax
	movl	%eax, -72(%rbp)
.L807:
	movl	148(%r15), %r12d
	movq	232(%r15), %rax
	movq	152(%r15), %rdi
	leal	-1(%r12), %r14d
	testq	%rax, %rax
	je	.L941
	testb	$5, 40(%rax)
	je	.L941
	testl	%r14d, %r14d
	je	.L941
	movl	%r14d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1117
	movq	232(%r15), %rsi
	movl	148(%r15), %edi
	movl	%r14d, %ecx
	call	dane_match.isra.0
	testl	%eax, %eax
	js	.L1128
	je	.L1117
	leal	-2(%r12), %eax
	movl	$1, -100(%rbp)
	movl	%eax, 148(%r15)
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L1160:
	movl	$3162, %r8d
.L1127:
	movl	$65, %edx
	movl	$106, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$17, 176(%r15)
.L1128:
	movl	$2, -100(%rbp)
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L941:
	movl	$3, -100(%rbp)
	jmp	.L813
	.p2align 4,,10
	.p2align 3
.L1118:
	movq	152(%r15), %r9
	jmp	.L780
	.p2align 4,,10
	.p2align 3
.L1117:
	movl	$3, -100(%rbp)
	movq	152(%r15), %rdi
	jmp	.L813
	.p2align 4,,10
	.p2align 3
.L764:
	movl	$1073741823, 40(%rax)
	movl	$1073741824, -104(%rbp)
	jmp	.L765
	.p2align 4,,10
	.p2align 3
.L1154:
	movq	32(%r15), %rax
	movl	$-1, -64(%rbp)
	movl	44(%rax), %r9d
	testl	%r9d, %r9d
	movl	%r9d, -72(%rbp)
	jle	.L869
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	leaq	-64(%rbp), %rcx
	call	X509_get_signature_info@PLT
	testl	%eax, %eax
	je	.L871
	movl	-72(%rbp), %r9d
	movl	$5, %eax
	movl	-64(%rbp), %ecx
	cmpl	$5, %r9d
	cmovg	%eax, %r9d
	leaq	minbits_table(%rip), %rax
	subl	$1, %r9d
	movslq	%r9d, %r9
	cmpl	%ecx, (%rax,%r9,4)
	jle	.L869
.L871:
	movl	%r14d, 172(%r15)
	testq	%rbx, %rbx
	je	.L1161
.L872:
	xorl	%edi, %edi
	movq	%rbx, 184(%r15)
	movq	%r15, %rsi
	movl	$68, 176(%r15)
	call	*56(%r15)
	testl	%eax, %eax
	jne	.L869
	jmp	.L1125
	.p2align 4,,10
	.p2align 3
.L906:
	movq	%r15, %rdi
	call	check_id
	movq	152(%r15), %rsi
	xorl	%edi, %edi
	movl	%eax, %ebx
	call	X509_get_pubkey_parameters
	testl	%ebx, %ebx
	je	.L1129
	movq	%r15, %rdi
	call	*80(%r15)
	testl	%eax, %eax
	je	.L1129
	movq	32(%r15), %rax
	movq	152(%r15), %rdx
	leaq	172(%r15), %rdi
	xorl	%esi, %esi
	movq	24(%rax), %rcx
	call	X509_chain_check_suiteb@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	jne	.L876
.L879:
	movq	48(%r15), %rax
	testq	%rax, %rax
	je	.L1162
	movq	%r15, %rdi
	call	*%rax
.L880:
	testl	%eax, %eax
	je	.L1129
	movq	152(%r15), %rdi
	call	OPENSSL_sk_num@PLT
	subl	$1, %eax
	movl	%eax, %ebx
	js	.L904
.L881:
	movq	152(%r15), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r12
	movl	224(%rax), %eax
	testl	%ebx, %ebx
	je	.L884
	testb	$32, %al
	jne	.L885
.L884:
	testb	$4, %ah
	je	.L892
	movq	%r12, %rdi
	call	X509_get_subject_name@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	X509_get_issuer_name@PLT
	movq	%r13, %rdi
	movq	%rax, -72(%rbp)
	call	X509_NAME_entry_count@PLT
	leal	-1(%rax), %r14d
	movl	%eax, -88(%rbp)
	testl	%r14d, %r14d
	jle	.L891
	movq	%r13, %rdi
	call	X509_NAME_entry_count@PLT
	movq	-72(%rbp), %rdi
	movl	%eax, -80(%rbp)
	call	X509_NAME_entry_count@PLT
	addl	$1, %eax
	cmpl	%eax, -80(%rbp)
	jne	.L891
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	X509_NAME_get_entry@PLT
	movq	%rax, %rdi
	call	X509_NAME_ENTRY_set@PLT
	movl	-88(%rbp), %esi
	movq	%r13, %rdi
	movl	%eax, -80(%rbp)
	subl	$2, %esi
	call	X509_NAME_get_entry@PLT
	movq	%rax, %rdi
	call	X509_NAME_ENTRY_set@PLT
	cmpl	%eax, -80(%rbp)
	je	.L891
	movq	%r13, %rdi
	call	X509_NAME_dup@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1163
	movl	%r14d, %esi
	movq	%rax, %rdi
	call	X509_NAME_delete_entry@PLT
	movq	%rax, %rdi
	movq	%rax, -80(%rbp)
	call	X509_NAME_ENTRY_get_object@PLT
	movq	%rax, %rdi
	call	OBJ_obj2nid@PLT
	movq	-80(%rbp), %r8
	cmpl	$13, %eax
	je	.L1164
	movq	%r8, %rdi
.L1123:
	call	X509_NAME_ENTRY_free@PLT
	movq	%r13, %rdi
	call	X509_NAME_free@PLT
.L891:
	xorl	%edi, %edi
	movq	%r15, %rsi
	movl	%ebx, 172(%r15)
	movq	%r12, 184(%r15)
	movl	$72, 176(%r15)
	call	*56(%r15)
	testl	%eax, %eax
	je	.L1129
.L892:
	movq	152(%r15), %rdi
	call	OPENSSL_sk_num@PLT
	leal	-1(%rax), %r13d
	cmpl	%ebx, %r13d
	jle	.L885
.L903:
	movq	152(%r15), %rdi
	movl	%r13d, %esi
	call	OPENSSL_sk_value@PLT
	movq	280(%rax), %r14
	testq	%r14, %r14
	je	.L894
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	NAME_CONSTRAINTS_check@PLT
	movl	%eax, %r8d
	orl	%ebx, %r8d
	movl	%r8d, -72(%rbp)
	jne	.L895
	movq	32(%r15), %rax
	movl	64(%rax), %eax
	testb	$32, %al
	jne	.L896
	testb	$1, %al
	je	.L1165
.L905:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	NAME_CONSTRAINTS_check_CN@PLT
.L895:
	testl	%eax, %eax
	je	.L894
	cmpl	$17, %eax
	je	.L1129
	xorl	%edi, %edi
	movl	%ebx, 172(%r15)
	movq	%r15, %rsi
	movq	%r12, 184(%r15)
	movl	%eax, 176(%r15)
	call	*56(%r15)
	testl	%eax, %eax
	je	.L1129
.L894:
	subl	$1, %r13d
	cmpl	%ebx, %r13d
	jg	.L903
.L885:
	subl	$1, %ebx
	cmpl	$-1, %ebx
	jne	.L881
.L904:
	movq	%r15, %rdi
	call	X509v3_asid_validate_path@PLT
	testl	%eax, %eax
	je	.L1129
	movq	%r15, %rdi
	call	X509v3_addr_validate_path@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L1129
	movq	32(%r15), %rax
	testb	$-128, 24(%rax)
	je	.L747
	movq	%r15, %rdi
	call	*112(%r15)
	movl	%eax, %r12d
	jmp	.L747
	.p2align 4,,10
	.p2align 3
.L899:
	call	GENERAL_NAMES_free@PLT
.L896:
	subl	$1, %r13d
	testl	%r13d, %r13d
	jg	.L903
	jmp	.L904
	.p2align 4,,10
	.p2align 3
.L876:
	movl	172(%r15), %esi
	movq	152(%r15), %rdi
	call	OPENSSL_sk_value@PLT
	xorl	%edi, %edi
	movl	%ebx, 176(%r15)
	movq	%r15, %rsi
	movq	%rax, 184(%r15)
	call	*56(%r15)
	testl	%eax, %eax
	jne	.L879
	jmp	.L1129
	.p2align 4,,10
	.p2align 3
.L784:
	movl	$0, -72(%rbp)
	movl	148(%r15), %eax
	jmp	.L785
	.p2align 4,,10
	.p2align 3
.L1159:
	movq	-64(%rbp), %rdi
	call	X509_free@PLT
	movl	$3053, %r8d
	jmp	.L1127
	.p2align 4,,10
	.p2align 3
.L1145:
	movq	32(%r15), %rdx
	movq	24(%rdx), %rax
	shrq	$6, %rax
	andl	$1, %eax
	movl	%eax, -96(%rbp)
	movl	32(%rdx), %eax
	movl	%eax, -80(%rbp)
	jmp	.L834
	.p2align 4,,10
	.p2align 3
.L938:
	movq	%rbx, %r14
	movl	-112(%rbp), %ebx
	movq	%r14, -96(%rbp)
	jmp	.L802
	.p2align 4,,10
	.p2align 3
.L786:
	movq	%r10, %rdi
	subl	$1, %r12d
	call	X509_free@PLT
	movl	%r12d, 148(%r15)
	movq	-64(%rbp), %rdx
	movl	%r12d, %esi
	movq	152(%r15), %rdi
	call	OPENSSL_sk_set@PLT
	movl	148(%r15), %eax
	jmp	.L785
	.p2align 4,,10
	.p2align 3
.L1136:
	movl	$0, -128(%rbp)
	orl	$2, %ebx
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L1165:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$85, %esi
	call	X509_get_ext_d2i@PLT
	movl	-72(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, %rdi
	jne	.L898
	jmp	.L905
	.p2align 4,,10
	.p2align 3
.L900:
	movl	%r8d, %esi
	movq	%rdi, -72(%rbp)
	movl	%r8d, -80(%rbp)
	call	OPENSSL_sk_value@PLT
	movq	-72(%rbp), %rdi
	cmpl	$2, (%rax)
	je	.L899
	movl	-80(%rbp), %r8d
	addl	$1, %r8d
.L898:
	movl	%r8d, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	OPENSSL_sk_num@PLT
	movl	-80(%rbp), %r8d
	movq	-72(%rbp), %rdi
	cmpl	%eax, %r8d
	jl	.L900
	call	GENERAL_NAMES_free@PLT
	jmp	.L905
	.p2align 4,,10
	.p2align 3
.L1140:
	leal	-1(%rsi), %eax
	movq	232(%r15), %rbx
	movq	152(%r15), %rdi
	xorl	%r12d, %r12d
	movl	%eax, %esi
	movl	%eax, -100(%rbp)
	call	OPENSSL_sk_value@PLT
	movq	8(%rbx), %rdi
	movq	%rax, -80(%rbp)
	call	OPENSSL_sk_num@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jle	.L925
	.p2align 4,,10
	.p2align 3
.L817:
	movq	8(%rbx), %rdi
	movl	%r12d, %esi
	call	OPENSSL_sk_value@PLT
	cmpw	$258, (%rax)
	movq	%rax, %r14
	jne	.L820
	cmpb	$0, 2(%rax)
	jne	.L820
	movq	24(%rax), %rsi
	movq	-80(%rbp), %rdi
	call	X509_verify@PLT
	testl	%eax, %eax
	jle	.L820
	movq	32(%rbx), %rdi
	call	X509_free@PLT
	movl	-100(%rbp), %eax
	movq	$0, 32(%rbx)
	movl	$1, 240(%r15)
	movq	152(%r15), %rdi
	movl	%eax, 44(%rbx)
	movq	%r14, 24(%rbx)
	call	OPENSSL_sk_num@PLT
	movl	%eax, %ebx
	cmpl	%eax, 148(%r15)
	jge	.L1120
	.p2align 4,,10
	.p2align 3
.L822:
	movq	152(%r15), %rdi
	subl	$1, %ebx
	call	OPENSSL_sk_pop@PLT
	movq	%rax, %rdi
	call	X509_free@PLT
	cmpl	148(%r15), %ebx
	jg	.L822
.L1120:
	movq	152(%r15), %rdi
	jmp	.L823
	.p2align 4,,10
	.p2align 3
.L919:
	movq	%r13, %rdi
	call	OPENSSL_sk_dup@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L927
	movl	$0, -124(%rbp)
	movl	$1, %ebx
	movl	$0, -128(%rbp)
	jmp	.L920
	.p2align 4,,10
	.p2align 3
.L820:
	addl	$1, %r12d
	cmpl	%r12d, %r13d
	jne	.L817
.L925:
	movl	148(%r15), %esi
	jmp	.L816
	.p2align 4,,10
	.p2align 3
.L1162:
	movq	%r15, %rdi
	call	internal_verify
	jmp	.L880
	.p2align 4,,10
	.p2align 3
.L864:
	movq	152(%r15), %rdi
	movl	%r14d, %esi
	call	OPENSSL_sk_value@PLT
	jmp	.L865
	.p2align 4,,10
	.p2align 3
.L1161:
	movq	152(%r15), %rdi
	movl	%r14d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rbx
	jmp	.L872
	.p2align 4,,10
	.p2align 3
.L1142:
	movq	-88(%rbp), %r14
	movq	8(%r14), %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jle	.L829
	testb	$3, 40(%r14)
	je	.L830
	movl	48(%r14), %r9d
	testl	%r9d, %r9d
	js	.L829
.L830:
	leal	-1(%rbx), %esi
	movq	152(%r15), %rdi
	movl	%esi, 172(%r15)
	call	OPENSSL_sk_value@PLT
	movq	%r15, %rsi
	xorl	%edi, %edi
	movl	$65, 176(%r15)
	movq	%rax, 184(%r15)
	call	*56(%r15)
	jmp	.L828
	.p2align 4,,10
	.p2align 3
.L1158:
	movq	-88(%rbp), %r14
	movl	$-1, 44(%r14)
	movq	32(%r14), %rdi
	call	X509_free@PLT
	movq	$0, 32(%r14)
	jmp	.L781
	.p2align 4,,10
	.p2align 3
.L1143:
	call	OPENSSL_sk_num@PLT
	movl	%r12d, 172(%r15)
	movq	152(%r15), %rdi
	movl	%r12d, %esi
	cmpl	$1, %eax
	je	.L1166
	call	OPENSSL_sk_value@PLT
	movq	%r15, %rsi
	xorl	%edi, %edi
	movl	$19, 176(%r15)
	movq	%rax, 184(%r15)
	call	*56(%r15)
	jmp	.L828
	.p2align 4,,10
	.p2align 3
.L1141:
	leal	-1(%rax), %esi
	movq	152(%r15), %rdi
	movl	%esi, 172(%r15)
	call	OPENSSL_sk_value@PLT
	movq	%r15, %rsi
	xorl	%edi, %edi
	movl	$22, 176(%r15)
	movq	%rax, 184(%r15)
	call	*56(%r15)
	jmp	.L828
	.p2align 4,,10
	.p2align 3
.L927:
	movl	$2912, %r8d
.L1130:
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$106, %esi
	xorl	%r12d, %r12d
	movl	$11, %edi
	call	ERR_put_error@PLT
	movq	152(%r15), %rsi
	xorl	%edi, %edi
	movl	$17, 176(%r15)
	call	X509_get_pubkey_parameters
	jmp	.L747
	.p2align 4,,10
	.p2align 3
.L1144:
	movl	%r12d, 172(%r15)
	movl	%r12d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%r15, %rsi
	xorl	%edi, %edi
	movl	$2, 176(%r15)
	movq	%rax, 184(%r15)
	call	*56(%r15)
	jmp	.L828
	.p2align 4,,10
	.p2align 3
.L1135:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L760
	movq	-88(%rbp), %rax
	movq	16(%rax), %rdi
	jmp	.L759
.L824:
	movq	%r15, %rdi
	call	check_trust
	movl	%eax, -100(%rbp)
	jmp	.L826
.L1164:
	movq	-72(%rbp), %rsi
	movq	%r13, %rdi
	call	X509_NAME_cmp@PLT
	movq	-80(%rbp), %r8
	testl	%eax, %eax
	movq	%r8, %rdi
	jne	.L1123
	call	X509_NAME_ENTRY_free@PLT
	movq	%r13, %rdi
	call	X509_NAME_free@PLT
	jmp	.L892
.L1166:
	call	OPENSSL_sk_value@PLT
	movq	%r15, %rsi
	xorl	%edi, %edi
	movl	$18, 176(%r15)
	movq	%rax, 184(%r15)
	call	*56(%r15)
	jmp	.L828
.L760:
	movl	$2929, %r8d
	jmp	.L1130
.L1163:
	movl	$646, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$149, %esi
	movl	$11, %edi
	call	ERR_put_error@PLT
	movl	$17, 176(%r15)
	jmp	.L747
.L1134:
	call	__stack_chk_fail@PLT
.L815:
	cmpl	$3, -100(%rbp)
	jne	.L814
	jmp	.L925
.L935:
	movl	$3, -100(%rbp)
	jmp	.L766
	.cfi_endproc
.LFE1400:
	.size	verify_chain, .-verify_chain
	.p2align 4
	.globl	X509_verify_cert
	.type	X509_verify_cert, @function
X509_verify_cert:
.LFB1401:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpq	$0, 8(%rdi)
	movq	%rdi, %rbx
	movq	232(%rdi), %r13
	je	.L1227
	cmpq	$0, 152(%rdi)
	jne	.L1228
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 152(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1172
	movq	8(%rbx), %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L1172
	movq	8(%rbx), %rdi
	call	X509_up_ref@PLT
	movq	8(%rbx), %rdi
	movl	$1, 148(%rbx)
	call	X509_get0_pubkey@PLT
	movq	%rax, %rdi
	movq	32(%rbx), %rax
	movl	44(%rax), %r12d
	testl	%r12d, %r12d
	jle	.L1180
	testq	%rdi, %rdi
	je	.L1179
	call	EVP_PKEY_security_bits@PLT
	cmpl	$5, %r12d
	movl	%eax, %r8d
	movl	$5, %eax
	cmovg	%eax, %r12d
	leaq	minbits_table(%rip), %rax
	subl	$1, %r12d
	movslq	%r12d, %r12
	cmpl	(%rax,%r12,4), %r8d
	jl	.L1179
.L1180:
	testq	%r13, %r13
	je	.L1175
	movq	8(%r13), %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jg	.L1229
.L1175:
	movq	%rbx, %rdi
	call	verify_chain
.L1190:
	testl	%eax, %eax
	jle	.L1189
.L1167:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1229:
	.cfi_restore_state
	movq	232(%rbx), %r12
	movq	8(%rbx), %r14
	movq	32(%r12), %rdi
	call	X509_free@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	$-1, 44(%r12)
	movups	%xmm0, 24(%r12)
	movq	8(%rbx), %rdx
	movq	232(%rbx), %rsi
	movl	148(%rbx), %edi
	call	dane_match.isra.0
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.L1181
	testb	$5, 40(%r12)
	jne	.L1175
	movl	44(%r12), %ecx
	testl	%ecx, %ecx
	jns	.L1175
	movq	152(%rbx), %rsi
	xorl	%edi, %edi
	call	X509_get_pubkey_parameters
	movq	32(%rbx), %rax
	xorl	%edx, %edx
	movq	%r14, %rsi
	xorl	%edi, %edi
	movq	24(%rax), %rcx
	call	X509_chain_check_suiteb@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L1230
	movl	$0, 172(%rbx)
	testq	%r14, %r14
	je	.L1192
.L1193:
	movq	%r14, 184(%rbx)
	movq	%rbx, %rsi
	xorl	%edi, %edi
	movl	$65, 176(%rbx)
	call	*56(%rbx)
	jmp	.L1190
	.p2align 4,,10
	.p2align 3
.L1179:
	movl	$0, 172(%rbx)
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L1231
.L1178:
	xorl	%edi, %edi
	movq	%rax, 184(%rbx)
	movq	%rbx, %rsi
	movl	$66, 176(%rbx)
	call	*56(%rbx)
	testl	%eax, %eax
	jne	.L1180
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1230:
	.cfi_restore_state
	movl	$0, 172(%rbx)
	testq	%r14, %r14
	je	.L1232
	xorl	%edi, %edi
	movq	%r14, 184(%rbx)
	movq	%rbx, %rsi
	movl	%r12d, 176(%rbx)
	call	*56(%rbx)
	testl	%eax, %eax
	jne	.L1233
.L1191:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1189:
	movl	176(%rbx), %edx
	testl	%edx, %edx
	jne	.L1167
	movl	$1, 176(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1181:
	.cfi_restore_state
	movq	152(%rbx), %rsi
	xorl	%edi, %edi
	call	X509_get_pubkey_parameters
	testl	%r13d, %r13d
	jle	.L1234
	movq	32(%rbx), %rax
	xorl	%edx, %edx
	movq	%r14, %rsi
	xorl	%edi, %edi
	movq	24(%rax), %rcx
	call	X509_chain_check_suiteb@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.L1184
.L1188:
	testb	$1, 56(%r12)
	je	.L1235
.L1186:
	movq	%r14, 184(%rbx)
	movq	%rbx, %rsi
	movl	$1, %edi
	movl	$0, 172(%rbx)
	call	*56(%rbx)
	jmp	.L1190
	.p2align 4,,10
	.p2align 3
.L1231:
	movq	152(%rbx), %rdi
	xorl	%esi, %esi
	call	OPENSSL_sk_value@PLT
	jmp	.L1178
	.p2align 4,,10
	.p2align 3
.L1235:
	movq	%rbx, %rdi
	call	check_id
	testl	%eax, %eax
	jne	.L1186
	xorl	%eax, %eax
	jmp	.L1189
.L1233:
	movl	$0, 172(%rbx)
	jmp	.L1193
	.p2align 4,,10
	.p2align 3
.L1184:
	movl	$0, 172(%rbx)
	movq	%r14, %rax
	testq	%r14, %r14
	je	.L1236
.L1187:
	xorl	%edi, %edi
	movq	%rax, 184(%rbx)
	movq	%rbx, %rsi
	movl	%r13d, 176(%rbx)
	call	*56(%rbx)
	testl	%eax, %eax
	jne	.L1188
	xorl	%eax, %eax
	jmp	.L1189
	.p2align 4,,10
	.p2align 3
.L1172:
	movl	$276, %r8d
	movl	$65, %edx
	movl	$127, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	movl	$17, 176(%rbx)
	jmp	.L1167
	.p2align 4,,10
	.p2align 3
.L1228:
	movl	$265, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$66, %edx
.L1225:
	movl	$127, %esi
	movl	$11, %edi
	call	ERR_put_error@PLT
	movl	$-1, %eax
	movl	$69, 176(%rbx)
	jmp	.L1167
.L1236:
	movq	152(%rbx), %rdi
	xorl	%esi, %esi
	call	OPENSSL_sk_value@PLT
	jmp	.L1187
.L1232:
	movq	152(%rbx), %rdi
	xorl	%esi, %esi
	call	OPENSSL_sk_value@PLT
	xorl	%edi, %edi
	movl	%r12d, 176(%rbx)
	movq	%rbx, %rsi
	movq	%rax, 184(%rbx)
	call	*56(%rbx)
	testl	%eax, %eax
	je	.L1191
	movl	$0, 172(%rbx)
.L1192:
	movq	152(%rbx), %rdi
	xorl	%esi, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r14
	jmp	.L1193
.L1227:
	movl	$255, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$105, %edx
	jmp	.L1225
.L1234:
	movabsq	$73014444032, %rax
	movq	%r14, 184(%rbx)
	movq	%rax, 172(%rbx)
	movl	$-1, %eax
	jmp	.L1167
	.cfi_endproc
.LFE1401:
	.size	X509_verify_cert, .-X509_verify_cert
	.p2align 4
	.globl	X509_CRL_diff
	.type	X509_CRL_diff, @function
X509_CRL_diff:
.LFB1440:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -72(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 168(%rdi)
	jne	.L1238
	movq	168(%rsi), %r13
	movq	%rsi, %r12
	testq	%r13, %r13
	jne	.L1238
	cmpq	$0, 160(%rdi)
	movq	%rdi, %rbx
	je	.L1241
	cmpq	$0, 160(%rsi)
	je	.L1241
	movq	%rsi, %rdi
	call	X509_CRL_get_issuer@PLT
	movq	%rbx, %rdi
	movq	%rax, %r14
	call	X509_CRL_get_issuer@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	X509_NAME_cmp@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	jne	.L1295
	movl	$90, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	crl_extension_match
	testl	%eax, %eax
	je	.L1296
	movl	$770, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	crl_extension_match
	testl	%eax, %eax
	je	.L1297
	movq	160(%rbx), %rsi
	movq	160(%r12), %rdi
	call	ASN1_INTEGER_cmp@PLT
	testl	%eax, %eax
	jle	.L1298
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L1247
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	X509_CRL_verify@PLT
	testl	%eax, %eax
	jle	.L1249
	movq	-72(%rbp), %rsi
	movq	%r12, %rdi
	call	X509_CRL_verify@PLT
	testl	%eax, %eax
	jle	.L1249
.L1247:
	call	X509_CRL_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1251
	movl	$1, %esi
	movq	%rax, %rdi
	call	X509_CRL_set_version@PLT
	testl	%eax, %eax
	je	.L1251
	movq	%r12, %rdi
	call	X509_CRL_get_issuer@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	X509_CRL_set_issuer_name@PLT
	testl	%eax, %eax
	je	.L1251
	movq	%r12, %rdi
	call	X509_CRL_get0_lastUpdate@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	X509_CRL_set1_lastUpdate@PLT
	testl	%eax, %eax
	je	.L1251
	movq	%r12, %rdi
	call	X509_CRL_get0_nextUpdate@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	X509_CRL_set1_nextUpdate@PLT
	testl	%eax, %eax
	je	.L1251
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$140, %esi
	movq	%r15, %rdi
	movq	160(%rbx), %rdx
	call	X509_CRL_add1_ext_i2d@PLT
	testl	%eax, %eax
	je	.L1251
	xorl	%ecx, %ecx
	jmp	.L1253
	.p2align 4,,10
	.p2align 3
.L1254:
	movl	%ecx, %esi
	movq	%r12, %rdi
	movl	%ecx, -88(%rbp)
	call	X509_CRL_get_ext@PLT
	movl	$-1, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	X509_CRL_add_ext@PLT
	testl	%eax, %eax
	je	.L1251
	movl	-88(%rbp), %ecx
	addl	$1, %ecx
.L1253:
	movq	%r12, %rdi
	movl	%ecx, -88(%rbp)
	call	X509_CRL_get_ext_count@PLT
	movl	-88(%rbp), %ecx
	cmpl	%ecx, %eax
	jg	.L1254
	movq	%r12, %rdi
	call	X509_CRL_get_REVOKED@PLT
	movq	%rax, -88(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rax, -96(%rbp)
	jmp	.L1255
	.p2align 4,,10
	.p2align 3
.L1256:
	addl	$1, %r14d
.L1255:
	movq	-88(%rbp), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r14d
	jge	.L1299
	movq	-88(%rbp), %rdi
	movl	%r14d, %esi
	call	OPENSSL_sk_value@PLT
	movq	-96(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	movq	%rax, %r12
	call	X509_CRL_get0_by_serial@PLT
	testl	%eax, %eax
	jne	.L1256
	movq	%r12, %rdi
	call	X509_REVOKED_dup@PLT
	movq	%rax, -64(%rbp)
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1251
	movq	%r15, %rdi
	call	X509_CRL_add0_revoked@PLT
	testl	%eax, %eax
	jne	.L1256
	movq	-64(%rbp), %rdi
	call	X509_REVOKED_free@PLT
	.p2align 4,,10
	.p2align 3
.L1251:
	movl	$2022, %r8d
	movl	$65, %edx
	movl	$105, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r15, %rdi
	call	X509_CRL_free@PLT
	jmp	.L1237
	.p2align 4,,10
	.p2align 3
.L1238:
	movl	$1930, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$127, %edx
	xorl	%r13d, %r13d
	movl	$105, %esi
	movl	$11, %edi
	call	ERR_put_error@PLT
.L1237:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1300
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1241:
	.cfi_restore_state
	movl	$1935, %r8d
	movl	$130, %edx
	movl	$105, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1237
	.p2align 4,,10
	.p2align 3
.L1295:
	movl	$1940, %r8d
	movl	$129, %edx
	movl	$105, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1237
	.p2align 4,,10
	.p2align 3
.L1296:
	movl	$1945, %r8d
	movl	$110, %edx
	movl	$105, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1237
	.p2align 4,,10
	.p2align 3
.L1297:
	movl	$1949, %r8d
	movl	$128, %edx
	movl	$105, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1237
	.p2align 4,,10
	.p2align 3
.L1249:
	movl	$1960, %r8d
	movl	$131, %edx
	movl	$105, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1237
	.p2align 4,,10
	.p2align 3
.L1298:
	movl	$1954, %r8d
	movl	$132, %edx
	movl	$105, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1237
	.p2align 4,,10
	.p2align 3
.L1299:
	movq	-72(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L1260
	movq	-80(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1260
	movq	%r15, %rdi
	call	X509_CRL_sign@PLT
	testl	%eax, %eax
	je	.L1251
.L1260:
	movq	%r15, %r13
	jmp	.L1237
.L1300:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1440:
	.size	X509_CRL_diff, .-X509_CRL_diff
	.p2align 4
	.globl	X509_STORE_CTX_set_ex_data
	.type	X509_STORE_CTX_set_ex_data, @function
X509_STORE_CTX_set_ex_data:
.LFB1441:
	.cfi_startproc
	endbr64
	addq	$224, %rdi
	jmp	CRYPTO_set_ex_data@PLT
	.cfi_endproc
.LFE1441:
	.size	X509_STORE_CTX_set_ex_data, .-X509_STORE_CTX_set_ex_data
	.p2align 4
	.globl	X509_STORE_CTX_get_ex_data
	.type	X509_STORE_CTX_get_ex_data, @function
X509_STORE_CTX_get_ex_data:
.LFB1442:
	.cfi_startproc
	endbr64
	addq	$224, %rdi
	jmp	CRYPTO_get_ex_data@PLT
	.cfi_endproc
.LFE1442:
	.size	X509_STORE_CTX_get_ex_data, .-X509_STORE_CTX_get_ex_data
	.p2align 4
	.globl	X509_STORE_CTX_get_error
	.type	X509_STORE_CTX_get_error, @function
X509_STORE_CTX_get_error:
.LFB1443:
	.cfi_startproc
	endbr64
	movl	176(%rdi), %eax
	ret
	.cfi_endproc
.LFE1443:
	.size	X509_STORE_CTX_get_error, .-X509_STORE_CTX_get_error
	.p2align 4
	.globl	X509_STORE_CTX_set_error
	.type	X509_STORE_CTX_set_error, @function
X509_STORE_CTX_set_error:
.LFB1444:
	.cfi_startproc
	endbr64
	movl	%esi, 176(%rdi)
	ret
	.cfi_endproc
.LFE1444:
	.size	X509_STORE_CTX_set_error, .-X509_STORE_CTX_set_error
	.p2align 4
	.globl	X509_STORE_CTX_get_error_depth
	.type	X509_STORE_CTX_get_error_depth, @function
X509_STORE_CTX_get_error_depth:
.LFB1445:
	.cfi_startproc
	endbr64
	movl	172(%rdi), %eax
	ret
	.cfi_endproc
.LFE1445:
	.size	X509_STORE_CTX_get_error_depth, .-X509_STORE_CTX_get_error_depth
	.p2align 4
	.globl	X509_STORE_CTX_set_error_depth
	.type	X509_STORE_CTX_set_error_depth, @function
X509_STORE_CTX_set_error_depth:
.LFB1446:
	.cfi_startproc
	endbr64
	movl	%esi, 172(%rdi)
	ret
	.cfi_endproc
.LFE1446:
	.size	X509_STORE_CTX_set_error_depth, .-X509_STORE_CTX_set_error_depth
	.p2align 4
	.globl	X509_STORE_CTX_get_current_cert
	.type	X509_STORE_CTX_get_current_cert, @function
X509_STORE_CTX_get_current_cert:
.LFB1447:
	.cfi_startproc
	endbr64
	movq	184(%rdi), %rax
	ret
	.cfi_endproc
.LFE1447:
	.size	X509_STORE_CTX_get_current_cert, .-X509_STORE_CTX_get_current_cert
	.p2align 4
	.globl	X509_STORE_CTX_set_current_cert
	.type	X509_STORE_CTX_set_current_cert, @function
X509_STORE_CTX_set_current_cert:
.LFB1448:
	.cfi_startproc
	endbr64
	movq	%rsi, 184(%rdi)
	ret
	.cfi_endproc
.LFE1448:
	.size	X509_STORE_CTX_set_current_cert, .-X509_STORE_CTX_set_current_cert
	.p2align 4
	.globl	X509_STORE_CTX_get0_chain
	.type	X509_STORE_CTX_get0_chain, @function
X509_STORE_CTX_get0_chain:
.LFB1449:
	.cfi_startproc
	endbr64
	movq	152(%rdi), %rax
	ret
	.cfi_endproc
.LFE1449:
	.size	X509_STORE_CTX_get0_chain, .-X509_STORE_CTX_get0_chain
	.p2align 4
	.globl	X509_STORE_CTX_get1_chain
	.type	X509_STORE_CTX_get1_chain, @function
X509_STORE_CTX_get1_chain:
.LFB1450:
	.cfi_startproc
	endbr64
	movq	152(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1310
	jmp	X509_chain_up_ref@PLT
	.p2align 4,,10
	.p2align 3
.L1310:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1450:
	.size	X509_STORE_CTX_get1_chain, .-X509_STORE_CTX_get1_chain
	.p2align 4
	.globl	X509_STORE_CTX_get0_current_issuer
	.type	X509_STORE_CTX_get0_current_issuer, @function
X509_STORE_CTX_get0_current_issuer:
.LFB1451:
	.cfi_startproc
	endbr64
	movq	192(%rdi), %rax
	ret
	.cfi_endproc
.LFE1451:
	.size	X509_STORE_CTX_get0_current_issuer, .-X509_STORE_CTX_get0_current_issuer
	.p2align 4
	.globl	X509_STORE_CTX_get0_current_crl
	.type	X509_STORE_CTX_get0_current_crl, @function
X509_STORE_CTX_get0_current_crl:
.LFB1452:
	.cfi_startproc
	endbr64
	movq	200(%rdi), %rax
	ret
	.cfi_endproc
.LFE1452:
	.size	X509_STORE_CTX_get0_current_crl, .-X509_STORE_CTX_get0_current_crl
	.p2align 4
	.globl	X509_STORE_CTX_get0_parent_ctx
	.type	X509_STORE_CTX_get0_parent_ctx, @function
X509_STORE_CTX_get0_parent_ctx:
.LFB1453:
	.cfi_startproc
	endbr64
	movq	216(%rdi), %rax
	ret
	.cfi_endproc
.LFE1453:
	.size	X509_STORE_CTX_get0_parent_ctx, .-X509_STORE_CTX_get0_parent_ctx
	.p2align 4
	.globl	X509_STORE_CTX_set_cert
	.type	X509_STORE_CTX_set_cert, @function
X509_STORE_CTX_set_cert:
.LFB1454:
	.cfi_startproc
	endbr64
	movq	%rsi, 8(%rdi)
	ret
	.cfi_endproc
.LFE1454:
	.size	X509_STORE_CTX_set_cert, .-X509_STORE_CTX_set_cert
	.p2align 4
	.globl	X509_STORE_CTX_set0_crls
	.type	X509_STORE_CTX_set0_crls, @function
X509_STORE_CTX_set0_crls:
.LFB1455:
	.cfi_startproc
	endbr64
	movq	%rsi, 24(%rdi)
	ret
	.cfi_endproc
.LFE1455:
	.size	X509_STORE_CTX_set0_crls, .-X509_STORE_CTX_set0_crls
	.p2align 4
	.globl	X509_STORE_CTX_set_purpose
	.type	X509_STORE_CTX_set_purpose, @function
X509_STORE_CTX_set_purpose:
.LFB1456:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testl	%esi, %esi
	je	.L1329
	movq	%rdi, %r13
	movl	%esi, %edi
	movl	%esi, %ebx
	call	X509_PURPOSE_get_by_id@PLT
	movl	%eax, %edi
	cmpl	$-1, %eax
	je	.L1330
	call	X509_PURPOSE_get0@PLT
	movl	4(%rax), %r12d
	testl	%r12d, %r12d
	je	.L1331
.L1321:
	movl	%r12d, %edi
	call	X509_TRUST_get_by_id@PLT
	cmpl	$-1, %eax
	je	.L1332
	movq	32(%r13), %rax
	movl	32(%rax), %ecx
	testl	%ecx, %ecx
	jne	.L1325
	movl	%ebx, 32(%rax)
.L1325:
	movl	36(%rax), %edx
	testl	%edx, %edx
	jne	.L1329
	movl	%r12d, 36(%rax)
.L1329:
	movl	$1, %r12d
.L1317:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1330:
	.cfi_restore_state
	movl	$2146, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$121, %edx
	xorl	%r12d, %r12d
	movl	$134, %esi
	movl	$11, %edi
	call	ERR_put_error@PLT
	jmp	.L1317
	.p2align 4,,10
	.p2align 3
.L1331:
	xorl	%edi, %edi
	call	X509_PURPOSE_get_by_id@PLT
	movl	%eax, %edi
	cmpl	$-1, %eax
	je	.L1333
	call	X509_PURPOSE_get0@PLT
	movl	4(%rax), %r12d
	testl	%r12d, %r12d
	jne	.L1321
	movq	32(%r13), %rax
	cmpl	$0, 32(%rax)
	jne	.L1329
	movl	%ebx, 32(%rax)
	jmp	.L1329
	.p2align 4,,10
	.p2align 3
.L1332:
	movl	$2172, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$120, %edx
	xorl	%r12d, %r12d
	movl	$134, %esi
	movl	$11, %edi
	call	ERR_put_error@PLT
	jmp	.L1317
	.p2align 4,,10
	.p2align 3
.L1333:
	movl	$2159, %r8d
	movl	$121, %edx
	movl	$134, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1317
	.cfi_endproc
.LFE1456:
	.size	X509_STORE_CTX_set_purpose, .-X509_STORE_CTX_set_purpose
	.p2align 4
	.globl	X509_STORE_CTX_set_trust
	.type	X509_STORE_CTX_set_trust, @function
X509_STORE_CTX_set_trust:
.LFB1457:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	jne	.L1335
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1335:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	%esi, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	call	X509_TRUST_get_by_id@PLT
	cmpl	$-1, %eax
	je	.L1344
	movq	32(%r12), %rax
	movl	36(%rax), %edx
	testl	%edx, %edx
	jne	.L1343
	movl	%ebx, 36(%rax)
.L1343:
	movl	$1, %eax
.L1334:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1344:
	.cfi_restore_state
	movl	$2172, %r8d
	movl	$120, %edx
	movl	$134, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L1334
	.cfi_endproc
.LFE1457:
	.size	X509_STORE_CTX_set_trust, .-X509_STORE_CTX_set_trust
	.p2align 4
	.globl	X509_STORE_CTX_purpose_inherit
	.type	X509_STORE_CTX_purpose_inherit, @function
X509_STORE_CTX_purpose_inherit:
.LFB1458:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$8, %rsp
	testl	%edx, %edx
	je	.L1374
	movl	%edx, %r13d
.L1346:
	movl	%r13d, %edi
	call	X509_PURPOSE_get_by_id@PLT
	movl	%eax, %edi
	cmpl	$-1, %eax
	je	.L1375
	call	X509_PURPOSE_get0@PLT
	movl	4(%rax), %r12d
	testl	%r12d, %r12d
	je	.L1376
.L1350:
	testl	%ebx, %ebx
	jne	.L1352
	movl	4(%rax), %ebx
	testl	%ebx, %ebx
	jne	.L1352
	movq	32(%r14), %rax
	movl	32(%rax), %ecx
	testl	%ecx, %ecx
	jne	.L1373
	movl	%r13d, 32(%rax)
	.p2align 4,,10
	.p2align 3
.L1373:
	movl	$1, %r12d
.L1345:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1374:
	.cfi_restore_state
	testl	%esi, %esi
	jne	.L1377
	testl	%ecx, %ecx
	je	.L1373
	movl	%ecx, %edi
	call	X509_TRUST_get_by_id@PLT
	cmpl	$-1, %eax
	je	.L1356
	movq	32(%r14), %rax
.L1357:
	movl	36(%rax), %esi
	testl	%esi, %esi
	jne	.L1373
	movl	%ebx, 36(%rax)
	jmp	.L1373
	.p2align 4,,10
	.p2align 3
.L1376:
	movl	%r15d, %edi
	call	X509_PURPOSE_get_by_id@PLT
	movl	%eax, %edi
	cmpl	$-1, %eax
	je	.L1378
	call	X509_PURPOSE_get0@PLT
	jmp	.L1350
	.p2align 4,,10
	.p2align 3
.L1375:
	movl	$2146, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$121, %edx
	xorl	%r12d, %r12d
	movl	$134, %esi
	movl	$11, %edi
	call	ERR_put_error@PLT
	jmp	.L1345
.L1352:
	movl	%ebx, %edi
	call	X509_TRUST_get_by_id@PLT
	addl	$1, %eax
	je	.L1356
	movq	32(%r14), %rax
	movl	32(%rax), %edx
	testl	%edx, %edx
	jne	.L1357
	movl	%r13d, 32(%rax)
	jmp	.L1357
	.p2align 4,,10
	.p2align 3
.L1356:
	movl	$2172, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$120, %edx
	xorl	%r12d, %r12d
	movl	$134, %esi
	movl	$11, %edi
	call	ERR_put_error@PLT
	jmp	.L1345
	.p2align 4,,10
	.p2align 3
.L1378:
	movl	$2159, %r8d
	movl	$121, %edx
	movl	$134, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1345
.L1377:
	movl	%esi, %r13d
	jmp	.L1346
	.cfi_endproc
.LFE1458:
	.size	X509_STORE_CTX_purpose_inherit, .-X509_STORE_CTX_purpose_inherit
	.p2align 4
	.globl	X509_STORE_CTX_new
	.type	X509_STORE_CTX_new, @function
X509_STORE_CTX_new:
.LFB1459:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2187, %edx
	movl	$248, %edi
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L1382
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1382:
	.cfi_restore_state
	movl	$2190, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$142, %esi
	movl	$11, %edi
	movq	%rax, -8(%rbp)
	call	ERR_put_error@PLT
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1459:
	.size	X509_STORE_CTX_new, .-X509_STORE_CTX_new
	.p2align 4
	.globl	X509_STORE_CTX_free
	.type	X509_STORE_CTX_free, @function
X509_STORE_CTX_free:
.LFB1460:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1383
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	136(%rdi), %rax
	testq	%rax, %rax
	je	.L1385
	call	*%rax
	movq	$0, 136(%r12)
.L1385:
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1386
	cmpq	$0, 216(%r12)
	je	.L1397
.L1387:
	movq	$0, 32(%r12)
.L1386:
	movq	160(%r12), %rdi
	call	X509_policy_tree_free@PLT
	movq	152(%r12), %rdi
	movq	$0, 160(%r12)
	movq	X509_free@GOTPCREL(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	leaq	224(%r12), %rdx
	movq	%r12, %rsi
	movq	$0, 152(%r12)
	movl	$5, %edi
	call	CRYPTO_free_ex_data@PLT
	movq	%r12, %rdi
	movq	$0, 224(%r12)
	addq	$8, %rsp
	popq	%r12
	.cfi_restore 12
	movl	$2202, %edx
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L1383:
	ret
	.p2align 4,,10
	.p2align 3
.L1397:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	call	X509_VERIFY_PARAM_free@PLT
	jmp	.L1387
	.cfi_endproc
.LFE1460:
	.size	X509_STORE_CTX_free, .-X509_STORE_CTX_free
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"default"
	.text
	.p2align 4
	.globl	X509_STORE_CTX_init
	.type	X509_STORE_CTX_init, @function
X509_STORE_CTX_init:
.LFB1461:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	224(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movups	%xmm0, (%rdi)
	pxor	%xmm0, %xmm0
	movq	%rcx, 16(%rdi)
	movq	$0, 24(%rdi)
	movq	$0, 40(%rdi)
	movq	$0, 144(%rdi)
	movq	$0, 168(%rdi)
	movl	$0, 176(%rdi)
	movq	$0, 200(%rdi)
	movq	$0, 208(%rdi)
	movq	$0, 216(%rdi)
	movq	$0, 232(%rdi)
	movl	$0, 240(%rdi)
	movq	$0, 224(%rdi)
	movups	%xmm0, 152(%rdi)
	movups	%xmm0, 184(%rdi)
	testq	%rsi, %rsi
	je	.L1399
	movq	120(%rsi), %rax
	movq	56(%rsi), %rdx
	movq	%rsi, %r12
	movq	%rax, 136(%rdi)
	testq	%rdx, %rdx
	leaq	check_issued(%rip), %rax
	cmovne	%rdx, %rax
	movq	%rax, 72(%rdi)
	movq	48(%rsi), %rax
	testq	%rax, %rax
	je	.L1457
	movq	%rax, 64(%rdi)
.L1433:
	movq	40(%r12), %rdx
	leaq	null_callback(%rip), %rax
	testq	%rdx, %rdx
	cmovne	%rdx, %rax
	movq	32(%r12), %rdx
	movq	%rax, 56(%rbx)
	testq	%rdx, %rdx
	leaq	internal_verify(%rip), %rax
	cmovne	%rdx, %rax
	movq	64(%r12), %rdx
	movq	%rax, 48(%rbx)
	testq	%rdx, %rdx
	leaq	check_revocation(%rip), %rax
	cmovne	%rdx, %rax
	movq	80(%r12), %rdx
	movq	%rax, 80(%rbx)
	movq	72(%r12), %rax
	testq	%rdx, %rdx
	movq	%rax, 88(%rbx)
	leaq	check_crl(%rip), %rax
	cmovne	%rdx, %rax
	movq	88(%r12), %rdx
	movq	%rax, 96(%rbx)
	testq	%rdx, %rdx
	leaq	cert_crl(%rip), %rax
	cmovne	%rdx, %rax
	movq	96(%r12), %rdx
	movq	%rax, 104(%rbx)
	testq	%rdx, %rdx
	leaq	check_policy(%rip), %rax
	cmovne	%rdx, %rax
	movq	%rax, 112(%rbx)
	movq	104(%r12), %rax
	testq	%rax, %rax
	je	.L1412
	movq	%rax, 120(%rbx)
.L1425:
	movq	112(%r12), %rax
	testq	%rax, %rax
	je	.L1413
	movq	%rax, 128(%rbx)
	call	X509_VERIFY_PARAM_new@PLT
	movq	%rax, 32(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1414
.L1424:
	movq	24(%r12), %rsi
	call	X509_VERIFY_PARAM_inherit@PLT
	testl	%eax, %eax
	jne	.L1415
.L1416:
	movl	$2313, %r8d
.L1456:
.L1403:
	endbr64
	movl	$65, %edx
	movl	$143, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	136(%rbx), %rax
	testq	%rax, %rax
	je	.L1421
	movq	%rbx, %rdi
	call	*%rax
	movq	$0, 136(%rbx)
.L1421:
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1422
	cmpq	$0, 216(%rbx)
	je	.L1458
.L1423:
	movq	$0, 32(%rbx)
.L1422:
	movq	160(%rbx), %rdi
	call	X509_policy_tree_free@PLT
	movq	152(%rbx), %rdi
	movq	$0, 160(%rbx)
	movq	X509_free@GOTPCREL(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movl	$5, %edi
	movq	$0, 152(%rbx)
	call	CRYPTO_free_ex_data@PLT
	xorl	%eax, %eax
	movq	$0, 224(%rbx)
.L1398:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1457:
	.cfi_restore_state
	movq	X509_STORE_CTX_get1_issuer@GOTPCREL(%rip), %rax
	movq	%rax, 64(%rdi)
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L1399:
	leaq	null_callback(%rip), %rax
	leaq	internal_verify(%rip), %rcx
	movq	$0, 136(%rdi)
	movq	%rax, %xmm2
	movq	%rcx, %xmm0
	leaq	check_issued(%rip), %rax
	movq	$0, 88(%rdi)
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, %xmm3
	leaq	check_revocation(%rip), %rax
	movups	%xmm0, 48(%rdi)
	leaq	check_crl(%rip), %rcx
	movq	X509_STORE_CTX_get1_issuer@GOTPCREL(%rip), %xmm0
	movq	%rax, 80(%rdi)
	leaq	cert_crl(%rip), %rax
	punpcklqdq	%xmm3, %xmm0
	movq	%rax, %xmm4
	leaq	check_policy(%rip), %rax
	movups	%xmm0, 64(%rdi)
	movq	%rcx, %xmm0
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 96(%rdi)
	movq	%rax, %xmm0
	movq	X509_STORE_CTX_get1_crls@GOTPCREL(%rip), %rax
	movhps	X509_STORE_CTX_get1_certs@GOTPCREL(%rip), %xmm0
	movq	%rax, 128(%rdi)
	movups	%xmm0, 112(%rdi)
	call	X509_VERIFY_PARAM_new@PLT
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	je	.L1414
	orl	$17, 16(%rax)
.L1415:
	leaq	.LC1(%rip), %rdi
	call	X509_VERIFY_PARAM_lookup@PLT
	movq	32(%rbx), %rdi
	movq	%rax, %rsi
	call	X509_VERIFY_PARAM_inherit@PLT
	testl	%eax, %eax
	je	.L1416
	movq	32(%rbx), %rax
	movl	36(%rax), %edx
	testl	%edx, %edx
	je	.L1459
.L1418:
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movl	$5, %edi
	call	CRYPTO_new_ex_data@PLT
	movl	%eax, %r8d
	movl	$1, %eax
	testl	%r8d, %r8d
	jne	.L1398
	movl	$2332, %r8d
	jmp	.L1456
	.p2align 4,,10
	.p2align 3
.L1459:
	movl	32(%rax), %edi
	call	X509_PURPOSE_get_by_id@PLT
	movl	%eax, %edi
	call	X509_PURPOSE_get0@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1418
	movq	32(%rbx), %r12
	call	X509_PURPOSE_get_trust@PLT
	movl	%eax, 36(%r12)
	jmp	.L1418
	.p2align 4,,10
	.p2align 3
.L1413:
	movq	X509_STORE_CTX_get1_crls@GOTPCREL(%rip), %rax
	movq	%rax, 128(%rbx)
	call	X509_VERIFY_PARAM_new@PLT
	movq	%rax, 32(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L1424
.L1414:
	movl	$2296, %r8d
	jmp	.L1456
	.p2align 4,,10
	.p2align 3
.L1412:
	movq	X509_STORE_CTX_get1_certs@GOTPCREL(%rip), %rax
	movq	%rax, 120(%rbx)
	jmp	.L1425
	.p2align 4,,10
	.p2align 3
.L1458:
	call	X509_VERIFY_PARAM_free@PLT
	jmp	.L1423
	.cfi_endproc
.LFE1461:
	.size	X509_STORE_CTX_init, .-X509_STORE_CTX_init
	.p2align 4
	.type	check_crl, @function
check_crl:
.LFB1429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$280, %rsp
	movl	172(%rdi), %r14d
	movq	152(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	OPENSSL_sk_num@PLT
	movq	192(%rbx), %r13
	testq	%r13, %r13
	je	.L1523
.L1461:
	cmpq	$0, 168(%r12)
	je	.L1465
.L1475:
	testb	$64, 208(%rbx)
	jne	.L1467
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	check_crl_time
	testl	%eax, %eax
	je	.L1471
.L1467:
	movq	%r13, %rdi
	call	X509_get0_pubkey@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1524
	movq	32(%rbx), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	24(%rax), %rdx
	call	X509_CRL_check_suiteb@PLT
	testl	%eax, %eax
	je	.L1488
	xorl	%edi, %edi
	movl	%eax, 176(%rbx)
	movq	%rbx, %rsi
	call	*56(%rbx)
	testl	%eax, %eax
	je	.L1471
.L1488:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	X509_CRL_verify@PLT
	testl	%eax, %eax
	jle	.L1525
	movl	$1, %eax
	jmp	.L1460
	.p2align 4,,10
	.p2align 3
.L1465:
	movabsq	$8589934594, %rax
	andq	224(%r13), %rax
	cmpq	$2, %rax
	je	.L1468
.L1472:
	movl	208(%rbx), %eax
	testb	$-128, %al
	jne	.L1470
	movl	$44, 176(%rbx)
	xorl	%edi, %edi
	movq	%rbx, %rsi
	call	*56(%rbx)
	testl	%eax, %eax
	je	.L1471
	movl	208(%rbx), %eax
.L1470:
	testb	$8, %al
	je	.L1473
.L1483:
	testb	$2, 152(%r12)
	je	.L1475
	movl	$41, 176(%rbx)
	xorl	%edi, %edi
	movq	%rbx, %rsi
	call	*56(%rbx)
	testl	%eax, %eax
	jne	.L1475
	.p2align 4,,10
	.p2align 3
.L1471:
	xorl	%eax, %eax
.L1460:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1526
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1523:
	.cfi_restore_state
	leal	-1(%rax), %esi
	movq	152(%rbx), %rdi
	cmpl	%esi, %r14d
	jge	.L1462
	leal	1(%r14), %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r13
.L1463:
	testq	%r13, %r13
	jne	.L1461
	movl	$1, %eax
	jmp	.L1460
	.p2align 4,,10
	.p2align 3
.L1524:
	movl	$6, 176(%rbx)
.L1522:
	xorl	%edi, %edi
	movq	%rbx, %rsi
	call	*56(%rbx)
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L1460
	.p2align 4,,10
	.p2align 3
.L1462:
	call	OPENSSL_sk_value@PLT
	movq	%rbx, %rdi
	movq	%rax, %r13
	movq	%rax, %rdx
	movq	%rax, %rsi
	call	*72(%rbx)
	testl	%eax, %eax
	jne	.L1463
	movl	$33, 176(%rbx)
	xorl	%edi, %edi
	movq	%rbx, %rsi
	call	*56(%rbx)
	testl	%eax, %eax
	jne	.L1463
	jmp	.L1471
	.p2align 4,,10
	.p2align 3
.L1525:
	movl	$8, 176(%rbx)
	jmp	.L1522
	.p2align 4,,10
	.p2align 3
.L1468:
	movl	$35, 176(%rbx)
	xorl	%edi, %edi
	movq	%rbx, %rsi
	call	*56(%rbx)
	testl	%eax, %eax
	jne	.L1472
	jmp	.L1471
	.p2align 4,,10
	.p2align 3
.L1473:
	cmpq	$0, 216(%rbx)
	je	.L1527
.L1477:
	movl	$54, 176(%rbx)
	xorl	%edi, %edi
	movq	%rbx, %rsi
	call	*56(%rbx)
	testl	%eax, %eax
	jne	.L1483
	jmp	.L1471
	.p2align 4,,10
	.p2align 3
.L1527:
	leaq	-304(%rbp), %r15
	movq	16(%rbx), %rcx
	movq	192(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r15, %rdi
	call	X509_STORE_CTX_init
	testl	%eax, %eax
	je	.L1477
	movq	24(%rbx), %rax
	movq	-272(%rbp), %rdi
	movq	32(%rbx), %r14
	movq	%rax, -280(%rbp)
	call	X509_VERIFY_PARAM_free@PLT
	movq	56(%rbx), %rax
	movq	%r15, %rdi
	movq	%rbx, -88(%rbp)
	movq	%r14, -272(%rbp)
	movq	%rax, -248(%rbp)
	call	X509_verify_cert
	movl	%eax, %r14d
	testl	%eax, %eax
	jle	.L1479
	movq	152(%rbx), %r14
	movq	-152(%rbp), %r8
	movq	%r14, %rdi
	movq	%r8, -312(%rbp)
	call	OPENSSL_sk_num@PLT
	movq	%r14, %rdi
	leal	-1(%rax), %esi
	call	OPENSSL_sk_value@PLT
	movq	-312(%rbp), %r8
	movq	%rax, %r14
	movq	%r8, %rdi
	call	OPENSSL_sk_num@PLT
	movq	-312(%rbp), %r8
	leal	-1(%rax), %esi
	movq	%r8, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	movq	%rax, %rsi
	call	X509_cmp@PLT
	testl	%eax, %eax
	sete	%r14b
.L1479:
	movq	-168(%rbp), %rax
	testq	%rax, %rax
	je	.L1480
	movq	%r15, %rdi
	call	*%rax
	movq	$0, -168(%rbp)
.L1480:
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1481
	cmpq	$0, -88(%rbp)
	je	.L1528
.L1482:
	movq	$0, -272(%rbp)
.L1481:
	movq	-144(%rbp), %rdi
	call	X509_policy_tree_free@PLT
	movq	X509_free@GOTPCREL(%rip), %rsi
	movq	-152(%rbp), %rdi
	movq	$0, -144(%rbp)
	call	OPENSSL_sk_pop_free@PLT
	leaq	-80(%rbp), %rdx
	movq	%r15, %rsi
	movl	$5, %edi
	movq	$0, -152(%rbp)
	call	CRYPTO_free_ex_data@PLT
	cmpl	$1, %r14d
	je	.L1483
	jmp	.L1477
.L1528:
	call	X509_VERIFY_PARAM_free@PLT
	jmp	.L1482
.L1526:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1429:
	.size	check_crl, .-check_crl
	.p2align 4
	.globl	X509_STORE_CTX_set0_trusted_stack
	.type	X509_STORE_CTX_set0_trusted_stack, @function
X509_STORE_CTX_set0_trusted_stack:
.LFB1462:
	.cfi_startproc
	endbr64
	leaq	get_issuer_sk(%rip), %rax
	movq	%rsi, 40(%rdi)
	movq	%rax, 64(%rdi)
	leaq	lookup_certs_sk(%rip), %rax
	movq	%rax, 120(%rdi)
	ret
	.cfi_endproc
.LFE1462:
	.size	X509_STORE_CTX_set0_trusted_stack, .-X509_STORE_CTX_set0_trusted_stack
	.p2align 4
	.globl	X509_STORE_CTX_cleanup
	.type	X509_STORE_CTX_cleanup, @function
X509_STORE_CTX_cleanup:
.LFB1463:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	136(%rdi), %rax
	testq	%rax, %rax
	je	.L1531
	call	*%rax
	movq	$0, 136(%rbx)
.L1531:
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1532
	cmpq	$0, 216(%rbx)
	je	.L1541
.L1533:
	movq	$0, 32(%rbx)
.L1532:
	movq	160(%rbx), %rdi
	call	X509_policy_tree_free@PLT
	movq	152(%rbx), %rdi
	movq	$0, 160(%rbx)
	movq	X509_free@GOTPCREL(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	leaq	224(%rbx), %rdx
	movq	%rbx, %rsi
	movq	$0, 152(%rbx)
	movl	$5, %edi
	call	CRYPTO_free_ex_data@PLT
	movq	$0, 224(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1541:
	.cfi_restore_state
	call	X509_VERIFY_PARAM_free@PLT
	jmp	.L1533
	.cfi_endproc
.LFE1463:
	.size	X509_STORE_CTX_cleanup, .-X509_STORE_CTX_cleanup
	.p2align 4
	.globl	X509_STORE_CTX_set_depth
	.type	X509_STORE_CTX_set_depth, @function
X509_STORE_CTX_set_depth:
.LFB1464:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdi
	jmp	X509_VERIFY_PARAM_set_depth@PLT
	.cfi_endproc
.LFE1464:
	.size	X509_STORE_CTX_set_depth, .-X509_STORE_CTX_set_depth
	.p2align 4
	.globl	X509_STORE_CTX_set_flags
	.type	X509_STORE_CTX_set_flags, @function
X509_STORE_CTX_set_flags:
.LFB1465:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdi
	jmp	X509_VERIFY_PARAM_set_flags@PLT
	.cfi_endproc
.LFE1465:
	.size	X509_STORE_CTX_set_flags, .-X509_STORE_CTX_set_flags
	.p2align 4
	.globl	X509_STORE_CTX_set_time
	.type	X509_STORE_CTX_set_time, @function
X509_STORE_CTX_set_time:
.LFB1466:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdi
	movq	%rdx, %rsi
	jmp	X509_VERIFY_PARAM_set_time@PLT
	.cfi_endproc
.LFE1466:
	.size	X509_STORE_CTX_set_time, .-X509_STORE_CTX_set_time
	.p2align 4
	.globl	X509_STORE_CTX_get0_cert
	.type	X509_STORE_CTX_get0_cert, @function
X509_STORE_CTX_get0_cert:
.LFB1467:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE1467:
	.size	X509_STORE_CTX_get0_cert, .-X509_STORE_CTX_get0_cert
	.p2align 4
	.globl	X509_STORE_CTX_get0_untrusted
	.type	X509_STORE_CTX_get0_untrusted, @function
X509_STORE_CTX_get0_untrusted:
.LFB1468:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE1468:
	.size	X509_STORE_CTX_get0_untrusted, .-X509_STORE_CTX_get0_untrusted
	.p2align 4
	.globl	X509_STORE_CTX_set0_untrusted
	.type	X509_STORE_CTX_set0_untrusted, @function
X509_STORE_CTX_set0_untrusted:
.LFB1469:
	.cfi_startproc
	endbr64
	movq	%rsi, 16(%rdi)
	ret
	.cfi_endproc
.LFE1469:
	.size	X509_STORE_CTX_set0_untrusted, .-X509_STORE_CTX_set0_untrusted
	.p2align 4
	.globl	X509_STORE_CTX_set0_verified_chain
	.type	X509_STORE_CTX_set0_verified_chain, @function
X509_STORE_CTX_set0_verified_chain:
.LFB1470:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	movq	X509_free@GOTPCREL(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	152(%rdi), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	%r12, 152(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1470:
	.size	X509_STORE_CTX_set0_verified_chain, .-X509_STORE_CTX_set0_verified_chain
	.p2align 4
	.globl	X509_STORE_CTX_set_verify_cb
	.type	X509_STORE_CTX_set_verify_cb, @function
X509_STORE_CTX_set_verify_cb:
.LFB1471:
	.cfi_startproc
	endbr64
	movq	%rsi, 56(%rdi)
	ret
	.cfi_endproc
.LFE1471:
	.size	X509_STORE_CTX_set_verify_cb, .-X509_STORE_CTX_set_verify_cb
	.p2align 4
	.globl	X509_STORE_CTX_get_verify_cb
	.type	X509_STORE_CTX_get_verify_cb, @function
X509_STORE_CTX_get_verify_cb:
.LFB1472:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rax
	ret
	.cfi_endproc
.LFE1472:
	.size	X509_STORE_CTX_get_verify_cb, .-X509_STORE_CTX_get_verify_cb
	.p2align 4
	.globl	X509_STORE_CTX_set_verify
	.type	X509_STORE_CTX_set_verify, @function
X509_STORE_CTX_set_verify:
.LFB1473:
	.cfi_startproc
	endbr64
	movq	%rsi, 48(%rdi)
	ret
	.cfi_endproc
.LFE1473:
	.size	X509_STORE_CTX_set_verify, .-X509_STORE_CTX_set_verify
	.p2align 4
	.globl	X509_STORE_CTX_get_verify
	.type	X509_STORE_CTX_get_verify, @function
X509_STORE_CTX_get_verify:
.LFB1474:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	ret
	.cfi_endproc
.LFE1474:
	.size	X509_STORE_CTX_get_verify, .-X509_STORE_CTX_get_verify
	.p2align 4
	.globl	X509_STORE_CTX_get_get_issuer
	.type	X509_STORE_CTX_get_get_issuer, @function
X509_STORE_CTX_get_get_issuer:
.LFB1475:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	ret
	.cfi_endproc
.LFE1475:
	.size	X509_STORE_CTX_get_get_issuer, .-X509_STORE_CTX_get_get_issuer
	.p2align 4
	.globl	X509_STORE_CTX_get_check_issued
	.type	X509_STORE_CTX_get_check_issued, @function
X509_STORE_CTX_get_check_issued:
.LFB1476:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rax
	ret
	.cfi_endproc
.LFE1476:
	.size	X509_STORE_CTX_get_check_issued, .-X509_STORE_CTX_get_check_issued
	.p2align 4
	.globl	X509_STORE_CTX_get_check_revocation
	.type	X509_STORE_CTX_get_check_revocation, @function
X509_STORE_CTX_get_check_revocation:
.LFB1477:
	.cfi_startproc
	endbr64
	movq	80(%rdi), %rax
	ret
	.cfi_endproc
.LFE1477:
	.size	X509_STORE_CTX_get_check_revocation, .-X509_STORE_CTX_get_check_revocation
	.p2align 4
	.globl	X509_STORE_CTX_get_get_crl
	.type	X509_STORE_CTX_get_get_crl, @function
X509_STORE_CTX_get_get_crl:
.LFB1478:
	.cfi_startproc
	endbr64
	movq	88(%rdi), %rax
	ret
	.cfi_endproc
.LFE1478:
	.size	X509_STORE_CTX_get_get_crl, .-X509_STORE_CTX_get_get_crl
	.p2align 4
	.globl	X509_STORE_CTX_get_check_crl
	.type	X509_STORE_CTX_get_check_crl, @function
X509_STORE_CTX_get_check_crl:
.LFB1479:
	.cfi_startproc
	endbr64
	movq	96(%rdi), %rax
	ret
	.cfi_endproc
.LFE1479:
	.size	X509_STORE_CTX_get_check_crl, .-X509_STORE_CTX_get_check_crl
	.p2align 4
	.globl	X509_STORE_CTX_get_cert_crl
	.type	X509_STORE_CTX_get_cert_crl, @function
X509_STORE_CTX_get_cert_crl:
.LFB1480:
	.cfi_startproc
	endbr64
	movq	104(%rdi), %rax
	ret
	.cfi_endproc
.LFE1480:
	.size	X509_STORE_CTX_get_cert_crl, .-X509_STORE_CTX_get_cert_crl
	.p2align 4
	.globl	X509_STORE_CTX_get_check_policy
	.type	X509_STORE_CTX_get_check_policy, @function
X509_STORE_CTX_get_check_policy:
.LFB1481:
	.cfi_startproc
	endbr64
	movq	112(%rdi), %rax
	ret
	.cfi_endproc
.LFE1481:
	.size	X509_STORE_CTX_get_check_policy, .-X509_STORE_CTX_get_check_policy
	.p2align 4
	.globl	X509_STORE_CTX_get_lookup_certs
	.type	X509_STORE_CTX_get_lookup_certs, @function
X509_STORE_CTX_get_lookup_certs:
.LFB1482:
	.cfi_startproc
	endbr64
	movq	120(%rdi), %rax
	ret
	.cfi_endproc
.LFE1482:
	.size	X509_STORE_CTX_get_lookup_certs, .-X509_STORE_CTX_get_lookup_certs
	.p2align 4
	.globl	X509_STORE_CTX_get_lookup_crls
	.type	X509_STORE_CTX_get_lookup_crls, @function
X509_STORE_CTX_get_lookup_crls:
.LFB1483:
	.cfi_startproc
	endbr64
	movq	128(%rdi), %rax
	ret
	.cfi_endproc
.LFE1483:
	.size	X509_STORE_CTX_get_lookup_crls, .-X509_STORE_CTX_get_lookup_crls
	.p2align 4
	.globl	X509_STORE_CTX_get_cleanup
	.type	X509_STORE_CTX_get_cleanup, @function
X509_STORE_CTX_get_cleanup:
.LFB1484:
	.cfi_startproc
	endbr64
	movq	136(%rdi), %rax
	ret
	.cfi_endproc
.LFE1484:
	.size	X509_STORE_CTX_get_cleanup, .-X509_STORE_CTX_get_cleanup
	.p2align 4
	.globl	X509_STORE_CTX_get0_policy_tree
	.type	X509_STORE_CTX_get0_policy_tree, @function
X509_STORE_CTX_get0_policy_tree:
.LFB1485:
	.cfi_startproc
	endbr64
	movq	160(%rdi), %rax
	ret
	.cfi_endproc
.LFE1485:
	.size	X509_STORE_CTX_get0_policy_tree, .-X509_STORE_CTX_get0_policy_tree
	.p2align 4
	.globl	X509_STORE_CTX_get_explicit_policy
	.type	X509_STORE_CTX_get_explicit_policy, @function
X509_STORE_CTX_get_explicit_policy:
.LFB1486:
	.cfi_startproc
	endbr64
	movl	168(%rdi), %eax
	ret
	.cfi_endproc
.LFE1486:
	.size	X509_STORE_CTX_get_explicit_policy, .-X509_STORE_CTX_get_explicit_policy
	.p2align 4
	.globl	X509_STORE_CTX_get_num_untrusted
	.type	X509_STORE_CTX_get_num_untrusted, @function
X509_STORE_CTX_get_num_untrusted:
.LFB1487:
	.cfi_startproc
	endbr64
	movl	148(%rdi), %eax
	ret
	.cfi_endproc
.LFE1487:
	.size	X509_STORE_CTX_get_num_untrusted, .-X509_STORE_CTX_get_num_untrusted
	.p2align 4
	.globl	X509_STORE_CTX_set_default
	.type	X509_STORE_CTX_set_default, @function
X509_STORE_CTX_set_default:
.LFB1488:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	call	X509_VERIFY_PARAM_lookup@PLT
	testq	%rax, %rax
	je	.L1568
	movq	32(%rbx), %rdi
	addq	$8, %rsp
	movq	%rax, %rsi
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	X509_VERIFY_PARAM_inherit@PLT
	.p2align 4,,10
	.p2align 3
.L1568:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1488:
	.size	X509_STORE_CTX_set_default, .-X509_STORE_CTX_set_default
	.p2align 4
	.globl	X509_STORE_CTX_get0_param
	.type	X509_STORE_CTX_get0_param, @function
X509_STORE_CTX_get0_param:
.LFB1489:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE1489:
	.size	X509_STORE_CTX_get0_param, .-X509_STORE_CTX_get0_param
	.p2align 4
	.globl	X509_STORE_CTX_set0_param
	.type	X509_STORE_CTX_set0_param, @function
X509_STORE_CTX_set0_param:
.LFB1490:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	32(%rdi), %rdi
	call	X509_VERIFY_PARAM_free@PLT
	movq	%r12, 32(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1490:
	.size	X509_STORE_CTX_set0_param, .-X509_STORE_CTX_set0_param
	.p2align 4
	.globl	X509_STORE_CTX_set0_dane
	.type	X509_STORE_CTX_set0_dane, @function
X509_STORE_CTX_set0_dane:
.LFB1491:
	.cfi_startproc
	endbr64
	movq	%rsi, 232(%rdi)
	ret
	.cfi_endproc
.LFE1491:
	.size	X509_STORE_CTX_set0_dane, .-X509_STORE_CTX_set0_dane
	.section	.rodata
	.align 16
	.type	minbits_table, @object
	.size	minbits_table, 20
minbits_table:
	.long	80
	.long	112
	.long	128
	.long	192
	.long	256
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
