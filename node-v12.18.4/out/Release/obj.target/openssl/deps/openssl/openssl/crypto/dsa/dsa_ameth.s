	.file	"dsa_ameth.c"
	.text
	.p2align 4
	.type	dsa_missing_parameters, @function
dsa_missing_parameters:
.LFB1449:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	testq	%rax, %rax
	je	.L5
	cmpq	$0, 8(%rax)
	je	.L5
	cmpq	$0, 16(%rax)
	je	.L5
	cmpq	$0, 24(%rax)
	sete	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1449:
	.size	dsa_missing_parameters, .-dsa_missing_parameters
	.p2align 4
	.type	old_dsa_priv_encode, @function
old_dsa_priv_encode:
.LFB1461:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	jmp	i2d_DSAPrivateKey@PLT
	.cfi_endproc
.LFE1461:
	.size	old_dsa_priv_encode, .-old_dsa_priv_encode
	.p2align 4
	.type	dsa_pkey_ctrl, @function
dsa_pkey_ctrl:
.LFB1463:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movq	%rcx, %rdi
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$5, %esi
	je	.L9
	jg	.L10
	cmpl	$1, %esi
	je	.L11
	cmpl	$3, %esi
	jne	.L45
	movl	$672, (%rcx)
	movl	$1, %r12d
	.p2align 4,,10
	.p2align 3
.L8:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L51
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	cmpl	$8, %esi
	jne	.L45
	movl	$-1, (%rcx)
	movl	$1, %r12d
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L45:
	movl	$-2, %r12d
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L11:
	movl	$1, %r12d
	testq	%rdx, %rdx
	jne	.L8
	leaq	-48(%rbp), %rcx
	leaq	-56(%rbp), %rdx
	xorl	%esi, %esi
	call	PKCS7_SIGNER_INFO_get0_algs@PLT
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L16
.L50:
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L16
	call	OBJ_obj2nid@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L16
	movq	%r13, %rdi
	call	EVP_PKEY_id@PLT
	leaq	-60(%rbp), %rdi
	movl	%r14d, %esi
	movl	%eax, %edx
	call	OBJ_find_sigid_by_algs@PLT
	testl	%eax, %eax
	je	.L16
	movl	-60(%rbp), %edi
	call	OBJ_nid2obj@PLT
	movq	-48(%rbp), %rdi
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%rax, %rsi
	call	X509_ALGOR_set0@PLT
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$1, %r12d
	testq	%rdx, %rdx
	jne	.L8
	leaq	-56(%rbp), %rcx
	leaq	-48(%rbp), %r8
	xorl	%esi, %esi
	call	CMS_SignerInfo_get0_algs@PLT
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	jne	.L50
.L16:
	movl	$-1, %r12d
	jmp	.L8
.L51:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1463:
	.size	dsa_pkey_ctrl, .-dsa_pkey_ctrl
	.p2align 4
	.type	int_dsa_free, @function
int_dsa_free:
.LFB1453:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	jmp	DSA_free@PLT
	.cfi_endproc
.LFE1453:
	.size	int_dsa_free, .-int_dsa_free
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"\n"
.LC1:
	.string	"r:   "
.LC2:
	.string	"s:   "
	.text
	.p2align 4
	.type	dsa_sig_print, @function
dsa_sig_print:
.LFB1462:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	subq	$32, %rsp
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L65
	movq	8(%rdx), %rax
	movq	%rdx, %r12
	movslq	(%rdx), %rdx
	leaq	-64(%rbp), %rsi
	xorl	%edi, %edi
	movl	%ecx, %r15d
	movq	%rax, -64(%rbp)
	call	d2i_DSA_SIG@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L56
	leaq	-48(%rbp), %rdx
	leaq	-56(%rbp), %rsi
	movq	%rax, %rdi
	call	DSA_SIG_get0@PLT
	movl	$1, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_write@PLT
	cmpl	$1, %eax
	je	.L57
.L59:
	xorl	%r12d, %r12d
.L58:
	movq	%r14, %rdi
	call	DSA_SIG_free@PLT
.L53:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L66
	addq	$32, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	movq	%r12, %rsi
	movl	%r15d, %edx
	movq	%r13, %rdi
	call	X509_signature_dump@PLT
	movl	%eax, %r12d
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L65:
	leaq	.LC0(%rip), %rsi
	xorl	%r12d, %r12d
	call	BIO_puts@PLT
	testl	%eax, %eax
	setg	%r12b
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L57:
	movq	-56(%rbp), %rdx
	xorl	%ecx, %ecx
	movl	%r15d, %r8d
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L59
	movq	-48(%rbp), %rdx
	xorl	%ecx, %ecx
	movl	%r15d, %r8d
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	setne	%r12b
	jmp	.L58
.L66:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1462:
	.size	dsa_sig_print, .-dsa_sig_print
	.section	.rodata.str1.1
.LC3:
	.string	"priv:"
.LC4:
	.string	"Private-Key"
.LC5:
	.string	"%s: (%d bit)\n"
.LC6:
	.string	"pub: "
.LC7:
	.string	"P:   "
.LC8:
	.string	"Q:   "
.LC9:
	.string	"G:   "
	.text
	.p2align 4
	.type	dsa_priv_print, @function
dsa_priv_print:
.LFB1459:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$8, %rsp
	movq	40(%rsi), %r14
	movq	40(%r14), %r13
	movq	32(%r14), %r15
	testq	%r13, %r13
	je	.L73
	movl	$128, %edx
	movl	%ebx, %esi
	call	BIO_indent@PLT
	testl	%eax, %eax
	jne	.L71
.L69:
	xorl	%eax, %eax
.L67:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	movq	8(%r14), %rdi
	call	BN_num_bits@PLT
	leaq	.LC4(%rip), %rdx
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, %ecx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L69
.L73:
	xorl	%ecx, %ecx
	movl	%ebx, %r8d
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	.LC3(%rip), %rsi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L69
	xorl	%ecx, %ecx
	movl	%ebx, %r8d
	movq	%r15, %rdx
	movq	%r12, %rdi
	leaq	.LC6(%rip), %rsi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L69
	movq	8(%r14), %rdx
	xorl	%ecx, %ecx
	movl	%ebx, %r8d
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L69
	movq	16(%r14), %rdx
	xorl	%ecx, %ecx
	movl	%ebx, %r8d
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L69
.L72:
	endbr64
	movq	24(%r14), %rdx
	xorl	%ecx, %ecx
	movl	%ebx, %r8d
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L67
	.cfi_endproc
.LFE1459:
	.size	dsa_priv_print, .-dsa_priv_print
	.p2align 4
	.type	dsa_cmp_parameters, @function
dsa_cmp_parameters:
.LFB1451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	40(%rsi), %rax
	movq	%rdi, %rbx
	movq	8(%rax), %rsi
	movq	40(%rdi), %rax
	movq	8(%rax), %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	je	.L89
.L91:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	movq	40(%r12), %rax
	movq	16(%rax), %rsi
	movq	40(%rbx), %rax
	movq	16(%rax), %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	jne	.L91
	movq	40(%r12), %rax
	movq	24(%rax), %rsi
	movq	40(%rbx), %rax
	movq	24(%rax), %rdi
	call	BN_cmp@PLT
	popq	%rbx
	popq	%r12
	testl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	sete	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1451:
	.size	dsa_cmp_parameters, .-dsa_cmp_parameters
	.p2align 4
	.type	dsa_pub_cmp, @function
dsa_pub_cmp:
.LFB1452:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	40(%rdi), %rax
	movq	%rsi, %r8
	movq	32(%rax), %rsi
	movq	40(%r8), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	32(%rax), %rdi
	call	BN_cmp@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1452:
	.size	dsa_pub_cmp, .-dsa_pub_cmp
	.p2align 4
	.type	dsa_copy_parameters, @function
dsa_copy_parameters:
.LFB1450:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	$0, 40(%rdi)
	je	.L96
.L99:
	movq	40(%r12), %rax
	movq	8(%rax), %rdi
	call	BN_dup@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L97
	movq	40(%rbx), %rax
	movq	8(%rax), %rdi
	call	BN_free@PLT
	movq	40(%rbx), %rax
	movq	%r13, 8(%rax)
	movq	40(%r12), %rax
	movq	16(%rax), %rdi
	call	BN_dup@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L97
	movq	40(%rbx), %rax
	movq	16(%rax), %rdi
	call	BN_free@PLT
	movq	40(%rbx), %rax
	movq	%r13, 16(%rax)
	movq	40(%r12), %rax
	movq	24(%rax), %rdi
	call	BN_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L97
	movq	40(%rbx), %rax
	movq	24(%rax), %rdi
	call	BN_free@PLT
	movq	40(%rbx), %rax
	movq	%r12, 24(%rax)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	call	DSA_new@PLT
	movq	%rax, 40(%rbx)
	testq	%rax, %rax
	jne	.L99
.L97:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1450:
	.size	dsa_copy_parameters, .-dsa_copy_parameters
	.p2align 4
	.type	dsa_param_encode, @function
dsa_param_encode:
.LFB1456:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	jmp	i2d_DSAparams@PLT
	.cfi_endproc
.LFE1456:
	.size	dsa_param_encode, .-dsa_param_encode
	.p2align 4
	.type	dsa_security_bits, @function
dsa_security_bits:
.LFB1448:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	jmp	DSA_security_bits@PLT
	.cfi_endproc
.LFE1448:
	.size	dsa_security_bits, .-dsa_security_bits
	.p2align 4
	.type	dsa_bits, @function
dsa_bits:
.LFB1447:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	jmp	DSA_bits@PLT
	.cfi_endproc
.LFE1447:
	.size	dsa_bits, .-dsa_bits
	.p2align 4
	.type	int_dsa_size, @function
int_dsa_size:
.LFB1446:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	jmp	DSA_size@PLT
	.cfi_endproc
.LFE1446:
	.size	int_dsa_size, .-int_dsa_size
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"../deps/openssl/openssl/crypto/dsa/dsa_ameth.c"
	.text
	.p2align 4
	.type	dsa_priv_encode, @function
dsa_priv_encode:
.LFB1445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	40(%rsi), %rax
	movq	$0, -48(%rbp)
	testq	%rax, %rax
	je	.L117
	cmpq	$0, 40(%rax)
	je	.L117
	movq	%rdi, %r13
	movq	%rsi, %rbx
	call	ASN1_STRING_new@PLT
	movl	$218, %r8d
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L130
	movq	40(%rbx), %rdi
	leaq	8(%rax), %rsi
	call	i2d_DSAparams@PLT
	movl	%eax, (%r12)
	testl	%eax, %eax
	jle	.L131
	movl	$16, 4(%r12)
	movq	40(%rbx), %rax
	xorl	%esi, %esi
	movq	40(%rax), %rdi
	call	BN_to_ASN1_INTEGER@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L132
	leaq	-48(%rbp), %rsi
	movq	%rax, %rdi
	call	i2d_ASN1_INTEGER@PLT
	movq	%r14, %rdi
	movl	%eax, %ebx
	call	ASN1_STRING_clear_free@PLT
	movl	$116, %edi
	movq	-48(%rbp), %r14
	call	OBJ_nid2obj@PLT
	subq	$8, %rsp
	movq	%r12, %r8
	xorl	%edx, %edx
	pushq	%rbx
	movq	%rax, %rsi
	movq	%r14, %r9
	movl	$16, %ecx
	movq	%r13, %rdi
	call	PKCS8_pkey_set0@PLT
	movl	%eax, %r8d
	popq	%rax
	movl	$1, %eax
	popq	%rdx
	testl	%r8d, %r8d
	je	.L119
.L116:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L133
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	.cfi_restore_state
	movl	$211, %r8d
	leaq	.LC10(%rip), %rcx
	movl	$101, %edx
	xorl	%r12d, %r12d
	movl	$116, %esi
	movl	$10, %edi
	call	ERR_put_error@PLT
.L119:
	movq	-48(%rbp), %rdi
	movl	$249, %edx
	leaq	.LC10(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	call	ASN1_STRING_free@PLT
	xorl	%edi, %edi
	call	ASN1_STRING_clear_free@PLT
	xorl	%eax, %eax
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L131:
	movl	$224, %r8d
.L130:
	movl	$65, %edx
	movl	$116, %esi
	movl	$10, %edi
	leaq	.LC10(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L132:
	movl	$233, %r8d
	movl	$109, %edx
	movl	$116, %esi
	movl	$10, %edi
	leaq	.LC10(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L119
.L133:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1445:
	.size	dsa_priv_encode, .-dsa_priv_encode
	.p2align 4
	.type	dsa_priv_decode, @function
dsa_priv_decode:
.LFB1444:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-72(%rbp), %r14
	leaq	-48(%rbp), %rcx
	pushq	%r13
	leaq	-80(%rbp), %rdx
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r14, %rsi
	pushq	%r12
	xorl	%edi, %edi
	subq	$48, %rsp
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	PKCS8_pkey_get0@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L160
.L134:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L161
	addq	$48, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L160:
	.cfi_restore_state
	movq	-48(%rbp), %rcx
	leaq	-56(%rbp), %rdx
	leaq	-76(%rbp), %rsi
	xorl	%edi, %edi
	call	X509_ALGOR_get0@PLT
	movslq	-80(%rbp), %rdx
	movq	%r14, %rsi
	xorl	%edi, %edi
	call	d2i_ASN1_INTEGER@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L136
	cmpl	$258, 4(%rax)
	je	.L136
	cmpl	$16, -76(%rbp)
	je	.L162
.L136:
	movl	$104, %edx
	movl	$115, %esi
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
	movl	$194, %r8d
	leaq	.LC10(%rip), %rcx
	movl	$10, %edi
	call	ERR_put_error@PLT
.L138:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	DSA_free@PLT
.L143:
	movq	%r15, %rdi
	call	BN_CTX_free@PLT
	movq	%r14, %rdi
	call	ASN1_STRING_clear_free@PLT
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L162:
	movq	-56(%rbp), %rax
	leaq	-64(%rbp), %rsi
	xorl	%edi, %edi
	movq	8(%rax), %rdx
	movq	%rdx, -64(%rbp)
	movslq	(%rax), %rdx
	call	d2i_DSAparams@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L136
	call	BN_secure_new@PLT
	movq	%rax, 40(%r12)
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L139
	movq	%r14, %rdi
	call	ASN1_INTEGER_to_BN@PLT
	testq	%rax, %rax
	je	.L139
	call	BN_new@PLT
	movl	$174, %r8d
	movq	%rax, 32(%r12)
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L159
	call	BN_CTX_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L163
	movq	40(%r12), %rdi
	movl	$4, %esi
	call	BN_set_flags@PLT
	movq	8(%r12), %rcx
	movq	40(%r12), %rdx
	movq	%r15, %r8
	movq	24(%r12), %rsi
	movq	32(%r12), %rdi
	call	BN_mod_exp@PLT
	testl	%eax, %eax
	jne	.L142
	movl	$184, %r8d
	movl	$109, %edx
	movl	$115, %esi
	movl	$10, %edi
	leaq	.LC10(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L139:
	movl	$169, %r8d
	leaq	.LC10(%rip), %rcx
	movl	$109, %edx
	xorl	%r15d, %r15d
	movl	$115, %esi
	movl	$10, %edi
	call	ERR_put_error@PLT
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L142:
	movq	%r12, %rdx
	movl	$116, %esi
	movq	%r13, %rdi
	movl	$1, %r12d
	call	EVP_PKEY_assign@PLT
	jmp	.L143
.L163:
	movl	$178, %r8d
.L159:
	movl	$65, %edx
	movl	$115, %esi
	movl	$10, %edi
	leaq	.LC10(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L138
.L161:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1444:
	.size	dsa_priv_decode, .-dsa_priv_decode
	.p2align 4
	.type	dsa_pub_encode, @function
dsa_pub_encode:
.LFB1443:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	40(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	48(%rsi), %eax
	movq	$0, -64(%rbp)
	testl	%eax, %eax
	je	.L172
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L175
	movq	16(%r13), %r12
	testq	%r12, %r12
	je	.L175
	movq	24(%r13), %r12
	testq	%r12, %r12
	je	.L175
	call	ASN1_STRING_new@PLT
	movl	$91, %r8d
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L182
	leaq	8(%rax), %rsi
	movq	%r13, %rdi
	movl	$16, %r15d
	call	i2d_DSAparams@PLT
	movl	%eax, (%r12)
	testl	%eax, %eax
	jg	.L165
	movl	$96, %r8d
	.p2align 4,,10
	.p2align 3
.L182:
	movl	$65, %edx
	movl	$118, %esi
	movl	$10, %edi
	leaq	.LC10(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L175:
	movl	$-1, %r15d
.L165:
	movq	32(%r13), %rdi
	xorl	%esi, %esi
	call	BN_to_ASN1_INTEGER@PLT
	movl	$106, %r8d
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L182
	leaq	-64(%rbp), %rsi
	movq	%rax, %rdi
	call	i2d_ASN1_INTEGER@PLT
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	ASN1_INTEGER_free@PLT
	movl	$114, %r8d
	testl	%ebx, %ebx
	jle	.L182
	movl	$116, %edi
	call	OBJ_nid2obj@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L167
	movq	-64(%rbp), %r8
	movl	%ebx, %r9d
	movq	%r12, %rcx
	movl	%r15d, %edx
	movq	%r14, %rdi
	call	X509_PUBKEY_set0_param@PLT
	movl	%eax, %r8d
	movl	$1, %eax
	testl	%r8d, %r8d
	jne	.L164
.L167:
	movq	-64(%rbp), %rdi
	movl	$126, %edx
	leaq	.LC10(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	call	ASN1_STRING_free@PLT
	xorl	%eax, %eax
.L164:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L183
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	.cfi_restore_state
	xorl	%r12d, %r12d
	movl	$-1, %r15d
	jmp	.L165
.L183:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1443:
	.size	dsa_pub_encode, .-dsa_pub_encode
	.p2align 4
	.type	dsa_pub_decode, @function
dsa_pub_decode:
.LFB1442:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-72(%rbp), %r13
	leaq	-48(%rbp), %rcx
	pushq	%r12
	leaq	-80(%rbp), %rdx
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rsi
	xorl	%edi, %edi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	X509_PUBKEY_get0_param@PLT
	testl	%eax, %eax
	jne	.L199
.L184:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L200
	addq	$56, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L199:
	.cfi_restore_state
	movq	-48(%rbp), %rcx
	leaq	-56(%rbp), %rdx
	leaq	-76(%rbp), %rsi
	xorl	%edi, %edi
	call	X509_ALGOR_get0@PLT
	movl	-76(%rbp), %eax
	cmpl	$16, %eax
	je	.L201
	cmpl	$5, %eax
	je	.L194
	cmpl	$-1, %eax
	je	.L194
	movl	$105, %edx
	movl	$117, %esi
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	movl	$52, %r8d
	leaq	.LC10(%rip), %rcx
	movl	$10, %edi
	call	ERR_put_error@PLT
.L188:
	movq	%r13, %rdi
	call	ASN1_INTEGER_free@PLT
	movq	%r14, %rdi
	call	DSA_free@PLT
	xorl	%eax, %eax
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L194:
	call	DSA_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L202
.L187:
	movslq	-80(%rbp), %rdx
	movq	%r13, %rsi
	xorl	%edi, %edi
	call	d2i_ASN1_INTEGER@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L203
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	ASN1_INTEGER_to_BN@PLT
	movq	%rax, 32(%r14)
	testq	%rax, %rax
	je	.L204
	movq	%r13, %rdi
	call	ASN1_INTEGER_free@PLT
	movq	%r14, %rdx
	movl	$116, %esi
	movq	%r12, %rdi
	call	EVP_PKEY_assign@PLT
	movl	$1, %eax
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L201:
	movq	-56(%rbp), %rax
	leaq	-64(%rbp), %rsi
	xorl	%edi, %edi
	movq	8(%rax), %rdx
	movq	%rdx, -64(%rbp)
	movslq	(%rax), %rdx
	call	d2i_DSAparams@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L187
	movl	$42, %r8d
	leaq	.LC10(%rip), %rcx
	movl	$104, %edx
	xorl	%r13d, %r13d
	movl	$117, %esi
	movl	$10, %edi
	call	ERR_put_error@PLT
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L203:
	movl	$57, %r8d
	movl	$104, %edx
	movl	$117, %esi
	movl	$10, %edi
	leaq	.LC10(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L204:
	movl	$62, %r8d
	movl	$108, %edx
	movl	$117, %esi
	movl	$10, %edi
	leaq	.LC10(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L202:
	movl	$48, %r8d
	leaq	.LC10(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$117, %esi
	movl	$10, %edi
	call	ERR_put_error@PLT
	jmp	.L188
.L200:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1442:
	.size	dsa_pub_decode, .-dsa_pub_decode
	.p2align 4
	.type	old_dsa_priv_decode, @function
old_dsa_priv_decode:
.LFB1460:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	xorl	%edi, %edi
	subq	$8, %rsp
	call	d2i_DSAPrivateKey@PLT
	testq	%rax, %rax
	je	.L209
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	$116, %esi
	call	EVP_PKEY_assign@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	.cfi_restore_state
	movl	$417, %r8d
	movl	$10, %edx
	movl	$122, %esi
	movl	$10, %edi
	leaq	.LC10(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1460:
	.size	old_dsa_priv_decode, .-old_dsa_priv_decode
	.p2align 4
	.type	dsa_param_decode, @function
dsa_param_decode:
.LFB1455:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	xorl	%edi, %edi
	subq	$8, %rsp
	call	d2i_DSAparams@PLT
	testq	%rax, %rax
	je	.L214
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	$116, %esi
	call	EVP_PKEY_assign@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L214:
	.cfi_restore_state
	movl	$381, %r8d
	movl	$10, %edx
	movl	$119, %esi
	movl	$10, %edi
	leaq	.LC10(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1455:
	.size	dsa_param_decode, .-dsa_param_decode
	.p2align 4
	.type	dsa_param_print, @function
dsa_param_print:
.LFB1457:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r8d
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	xorl	%edx, %edx
	subq	$8, %rsp
	movq	40(%rsi), %r13
	leaq	.LC3(%rip), %rsi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	jne	.L216
.L218:
	xorl	%eax, %eax
.L215:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L216:
	.cfi_restore_state
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%ebx, %r8d
	movq	%r12, %rdi
	leaq	.LC6(%rip), %rsi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L218
	movq	8(%r13), %rdx
	xorl	%ecx, %ecx
	movl	%ebx, %r8d
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L218
	movq	16(%r13), %rdx
	xorl	%ecx, %ecx
	movl	%ebx, %r8d
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L218
.L217:
	endbr64
	movq	24(%r13), %rdx
	xorl	%ecx, %ecx
	movl	%ebx, %r8d
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L215
	.cfi_endproc
.LFE1457:
	.size	dsa_param_print, .-dsa_param_print
	.p2align 4
	.type	dsa_pub_print, @function
dsa_pub_print:
.LFB1458:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r8d
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	40(%rsi), %r13
	movl	%edx, %ebx
	leaq	.LC3(%rip), %rsi
	xorl	%edx, %edx
	movq	32(%r13), %r14
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	jne	.L230
.L232:
	xorl	%eax, %eax
.L229:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L230:
	.cfi_restore_state
	xorl	%ecx, %ecx
	movl	%ebx, %r8d
	movq	%r14, %rdx
	movq	%r12, %rdi
	leaq	.LC6(%rip), %rsi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L232
	movq	8(%r13), %rdx
	xorl	%ecx, %ecx
	movl	%ebx, %r8d
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L232
	movq	16(%r13), %rdx
	xorl	%ecx, %ecx
	movl	%ebx, %r8d
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L232
.L231:
	endbr64
	movq	24(%r13), %rdx
	xorl	%ecx, %ecx
	movl	%ebx, %r8d
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L229
	.cfi_endproc
.LFE1458:
	.size	dsa_pub_print, .-dsa_pub_print
	.globl	dsa_asn1_meths
	.section	.rodata.str1.1
.LC11:
	.string	"DSA"
.LC12:
	.string	"OpenSSL DSA method"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	dsa_asn1_meths, @object
	.size	dsa_asn1_meths, 1400
dsa_asn1_meths:
	.long	66
	.long	116
	.quad	1
	.zero	264
	.long	67
	.long	116
	.quad	1
	.zero	264
	.long	70
	.long	116
	.quad	1
	.zero	264
	.long	113
	.long	116
	.quad	1
	.zero	264
	.long	116
	.long	116
	.quad	0
	.quad	.LC11
	.quad	.LC12
	.quad	dsa_pub_decode
	.quad	dsa_pub_encode
	.quad	dsa_pub_cmp
	.quad	dsa_pub_print
	.quad	dsa_priv_decode
	.quad	dsa_priv_encode
	.quad	dsa_priv_print
	.quad	int_dsa_size
	.quad	dsa_bits
	.quad	dsa_security_bits
	.quad	dsa_param_decode
	.quad	dsa_param_encode
	.quad	dsa_missing_parameters
	.quad	dsa_copy_parameters
	.quad	dsa_cmp_parameters
	.quad	dsa_param_print
	.quad	dsa_sig_print
	.quad	int_dsa_free
	.quad	dsa_pkey_ctrl
	.quad	old_dsa_priv_decode
	.quad	old_dsa_priv_encode
	.zero	80
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
