	.file	"t1_lib.c"
	.text
	.p2align 4
	.type	nid_cb, @function
nid_cb:
.LFB1588:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L10
	cmpq	$30, (%rdx)
	movq	%rdx, %rbx
	je	.L10
	cmpl	$19, %esi
	jg	.L10
	movslq	%esi, %r12
	leaq	-64(%rbp), %r13
	movq	%rdi, %rsi
	movl	$20, %ecx
	movq	%r13, %rdi
	movq	%r12, %rdx
	call	__memcpy_chk@PLT
	movq	%r13, %rdi
	movb	$0, -64(%rbp,%r12)
	call	EC_curve_nist2nid@PLT
	testl	%eax, %eax
	je	.L15
.L3:
	movq	(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L11
	xorl	%edx, %edx
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L17:
	addq	$1, %rdx
	cmpq	%rcx, %rdx
	je	.L16
.L7:
	cmpl	%eax, 8(%rbx,%rdx,4)
	jne	.L17
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%eax, %eax
.L1:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L18
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	movq	%r13, %rdi
	call	OBJ_sn2nid@PLT
	testl	%eax, %eax
	jne	.L3
	movq	%r13, %rdi
	call	OBJ_ln2nid@PLT
	testl	%eax, %eax
	je	.L10
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L16:
	leaq	1(%rcx), %rdx
.L5:
	movq	%rdx, (%rbx)
	movl	%eax, 8(%rbx,%rcx,4)
	movl	$1, %eax
	jmp	.L1
.L11:
	movl	$1, %edx
	jmp	.L5
.L18:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1588:
	.size	nid_cb, .-nid_cb
	.p2align 4
	.type	tls1_check_pkey_comp.part.0, @function
tls1_check_pkey_comp.part.0:
.LFB1655:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	call	EVP_PKEY_get0_EC_KEY@PLT
	movq	%rax, %r12
	movq	%rax, %rdi
	call	EC_KEY_get0_group@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	EC_KEY_get_conv_form@PLT
	cmpl	$4, %eax
	je	.L27
	movq	8(%rbx), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L21
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L21
	cmpl	$65536, %eax
	jne	.L22
.L21:
	movq	%r13, %rdi
	call	EC_GROUP_method_of@PLT
	movq	%rax, %rdi
	call	EC_METHOD_get_field_type@PLT
	cmpl	$406, %eax
	je	.L28
	cmpl	$407, %eax
	je	.L29
.L25:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	xorl	%ecx, %ecx
.L20:
	movq	1696(%rbx), %rax
	testq	%rax, %rax
	je	.L22
	movq	1688(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L25
	addq	%rax, %rdx
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L41:
	addq	$1, %rax
	cmpq	%rdx, %rax
	je	.L25
.L26:
	cmpb	%cl, (%rax)
	jne	.L41
.L22:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movl	$1, %ecx
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L29:
	movl	$2, %ecx
	jmp	.L20
	.cfi_endproc
.LFE1655:
	.size	tls1_check_pkey_comp.part.0, .-tls1_check_pkey_comp.part.0
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"RSA"
.LC1:
	.string	"RSA-PSS"
.LC2:
	.string	"PSS"
.LC3:
	.string	"DSA"
.LC4:
	.string	"ECDSA"
	.text
	.p2align 4
	.type	get_sigorhash, @function
get_sigorhash:
.LFB1623:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movl	$4, %ecx
	leaq	.LC0(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	subq	$24, %rsp
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L43
	movl	$6, (%r8)
.L42:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	movl	$8, %ecx
	leaq	.LC1(%rip), %rdi
	movq	%rdx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L53
.L45:
	movl	$912, (%r8)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movl	$4, %ecx
	leaq	.LC2(%rip), %rdi
	movq	%rdx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L45
	movl	$4, %ecx
	leaq	.LC3(%rip), %rdi
	movq	%rdx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L47
	movl	$116, (%r8)
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L47:
	movl	$6, %ecx
	leaq	.LC4(%rip), %rdi
	movq	%rdx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L48
	movl	$408, (%r8)
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L48:
	movq	%rdx, %rdi
	movq	%rdx, -24(%rbp)
	call	OBJ_sn2nid@PLT
	movq	-24(%rbp), %rdx
	testl	%eax, %eax
	movl	%eax, (%rbx)
	jne	.L42
	movq	%rdx, %rdi
	call	OBJ_ln2nid@PLT
	movl	%eax, (%rbx)
	jmp	.L42
	.cfi_endproc
.LFE1623:
	.size	get_sigorhash, .-get_sigorhash
	.p2align 4
	.type	sig_cb, @function
sig_cb:
.LFB1624:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -104(%rbp)
	movl	$0, -100(%rbp)
	testq	%rdi, %rdi
	je	.L81
	movq	(%rdx), %r13
	movq	%rdx, %r12
	cmpl	$39, %esi
	jg	.L81
	cmpq	$46, %r13
	je	.L81
	movslq	%esi, %rbx
	leaq	-96(%rbp), %r15
	movq	%rdi, %rsi
	movl	$40, %ecx
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	__memcpy_chk@PLT
	movl	$43, %esi
	movq	%r15, %rdi
	movb	$0, -96(%rbp,%rbx)
	call	strchr@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L82
	cmpb	$0, 1(%rax)
	movb	$0, (%rax)
	jne	.L83
	.p2align 4,,10
	.p2align 3
.L81:
	xorl	%eax, %eax
.L54:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L84
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	leaq	-100(%rbp), %r14
	leaq	-104(%rbp), %r13
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	get_sigorhash
	movq	%r14, %rsi
	leaq	1(%rbx), %rdx
	movq	%r13, %rdi
	call	get_sigorhash
	movl	-104(%rbp), %esi
	testl	%esi, %esi
	je	.L81
	movl	-100(%rbp), %edx
	testl	%edx, %edx
	je	.L81
	leaq	sigalg_lookup_tbl(%rip), %rax
	leaq	920(%rax), %rcx
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L62:
	addq	$40, %rax
	cmpq	%rcx, %rax
	je	.L81
.L63:
	cmpl	12(%rax), %edx
	jne	.L62
	cmpl	20(%rax), %esi
	jne	.L62
	movq	(%r12), %r13
	leaq	1(%r13), %rdx
	movq	%rdx, (%r12)
	movzwl	8(%rax), %edx
	movw	%dx, 8(%r12,%r13,2)
.L60:
	testq	%r13, %r13
	je	.L67
	xorl	%eax, %eax
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L64:
	addq	$1, %rax
	cmpq	%rax, %r13
	je	.L67
.L65:
	cmpw	%dx, 8(%r12,%rax,2)
	jne	.L64
	movq	%r13, (%r12)
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L82:
	leaq	sigalg_lookup_tbl(%rip), %rbx
	leaq	920(%rbx), %r14
	.p2align 4,,10
	.p2align 3
.L61:
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L59
	movq	%r15, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L85
.L59:
	addq	$40, %rbx
	cmpq	%r14, %rbx
	jne	.L61
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L67:
	movl	$1, %eax
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L85:
	movzwl	8(%rbx), %edx
	leaq	1(%r13), %rax
	movq	%rax, (%r12)
	movw	%dx, 8(%r12,%r13,2)
	jmp	.L60
.L84:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1624:
	.size	sig_cb, .-sig_cb
	.p2align 4
	.type	check_cert_usable.isra.0, @function
check_cert_usable.isra.0:
.LFB1657:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	ERR_set_mark@PLT
	leaq	-68(%rbp), %rsi
	movq	%r12, %rdi
	call	EVP_PKEY_get_default_digest_nid@PLT
	cmpl	$2, %eax
	jne	.L87
	movl	-68(%rbp), %eax
	xorl	%r8d, %r8d
	cmpl	%eax, (%rbx)
	je	.L87
.L86:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L111
	addq	$40, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L87:
	.cfi_restore_state
	call	ERR_pop_to_mark@PLT
	movq	0(%r13), %rdx
	movl	$1, %r8d
	movq	752(%rdx), %rax
	testq	%rax, %rax
	je	.L86
	cmpq	$0, 768(%rdx)
	je	.L89
	xorl	%r15d, %r15d
	leaq	920+sigalg_lookup_tbl(%rip), %r12
	.p2align 4,,10
	.p2align 3
.L95:
	movzwl	(%rax,%r15,2), %eax
	leaq	sigalg_lookup_tbl(%rip), %rbx
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L112:
	addq	$40, %rbx
	cmpq	%r12, %rbx
	je	.L93
.L91:
	cmpw	8(%rbx), %ax
	jne	.L112
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	-60(%rbp), %rdx
	movq	%r14, %rdi
	leaq	-64(%rbp), %rsi
	call	X509_get_signature_info@PLT
	testl	%eax, %eax
	je	.L110
	movl	-64(%rbp), %eax
	cmpl	%eax, 12(%rbx)
	je	.L106
.L110:
	movq	0(%r13), %rdx
.L93:
	addq	$1, %r15
	cmpq	768(%rdx), %r15
	jnb	.L89
	movq	752(%rdx), %rax
	jmp	.L95
.L89:
	xorl	%r8d, %r8d
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L106:
	movl	-60(%rbp), %eax
	cmpl	%eax, 20(%rbx)
	jne	.L110
	movl	$1, %r8d
	jmp	.L86
.L111:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1657:
	.size	check_cert_usable.isra.0, .-check_cert_usable.isra.0
	.p2align 4
	.type	tls1_check_sig_alg.part.0, @function
tls1_check_sig_alg.part.0:
.LFB1659:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	call	X509_get_signature_nid@PLT
	testl	%r12d, %r12d
	jne	.L138
	movq	8(%rbx), %rdx
	movq	192(%rdx), %rcx
	testb	$8, 96(%rcx)
	je	.L139
.L116:
	movq	6272(%rbx), %r8
	xorl	%r9d, %r9d
.L117:
	testq	%r8, %r8
	je	.L123
	xorl	%ecx, %ecx
	leaq	920+sigalg_lookup_tbl(%rip), %rdi
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L118:
	movq	6264(%rbx), %rdx
	movq	(%rdx,%rcx,8), %rdx
	testq	%rdx, %rdx
	je	.L121
	cmpl	28(%rdx), %eax
	je	.L124
.L121:
	addq	$1, %rcx
	cmpq	%r8, %rcx
	je	.L123
.L122:
	testl	%r9d, %r9d
	je	.L118
	movq	168(%rbx), %rdx
	movq	752(%rdx), %rdx
	movzwl	(%rdx,%rcx,2), %esi
	leaq	sigalg_lookup_tbl(%rip), %rdx
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L140:
	addq	$40, %rdx
	cmpq	%rdi, %rdx
	je	.L121
.L120:
	cmpw	8(%rdx), %si
	jne	.L140
	cmpl	28(%rdx), %eax
	jne	.L121
.L124:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	movl	(%rdx), %edx
	cmpl	$65536, %edx
	je	.L116
	cmpl	$771, %edx
	jle	.L116
	movq	168(%rbx), %rdx
	cmpq	$0, 752(%rdx)
	je	.L116
	movq	768(%rdx), %r8
	movl	$1, %r9d
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L123:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	cmpl	%r12d, %eax
	popq	%rbx
	popq	%r12
	sete	%al
	popq	%rbp
	.cfi_def_cfa 7, 8
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1659:
	.size	tls1_check_sig_alg.part.0, .-tls1_check_sig_alg.part.0
	.p2align 4
	.type	find_sig_alg, @function
find_sig_alg:
.LFB1642:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	movq	%rsi, -88(%rbp)
	movq	6272(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-64(%rbp), %rax
	movl	$-1, -68(%rbp)
	movq	%rax, -80(%rbp)
	testq	%rdx, %rdx
	je	.L162
.L142:
	movq	6264(%r14), %rax
	movq	(%rax,%r13,8), %r12
	movl	12(%r12), %eax
	cmpl	$64, %eax
	je	.L144
	cmpl	$675, %eax
	je	.L144
	movl	20(%r12), %ecx
	cmpl	$6, %ecx
	je	.L144
	cmpl	$116, %ecx
	je	.L144
	testl	%eax, %eax
	jne	.L182
	testq	%rbx, %rbx
	je	.L183
.L146:
	movq	-80(%rbp), %rsi
	movq	%rbx, %rdi
	call	ssl_cert_lookup_by_pkey@PLT
	testq	%rax, %rax
	je	.L180
	movl	-64(%rbp), %eax
	cmpl	%eax, 24(%r12)
	je	.L151
.L180:
	movq	6272(%r14), %rdx
.L144:
	addq	$1, %r13
	cmpq	%r13, %rdx
	ja	.L142
	.p2align 4,,10
	.p2align 3
.L156:
	cmpq	%rdx, %r13
	je	.L162
.L141:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L184
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L182:
	.cfi_restore_state
	movl	16(%r12), %edi
	call	ssl_md@PLT
	testq	%rax, %rax
	je	.L180
	testq	%rbx, %rbx
	jne	.L146
.L183:
	movslq	24(%r12), %rax
	cmpl	$8, %eax
	ja	.L180
	movq	1168(%r14), %rsi
	leaq	(%rax,%rax,4), %rax
	leaq	(%rsi,%rax,8), %rax
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L180
	movq	40(%rax), %rcx
	testq	%rcx, %rcx
	je	.L180
	leaq	12(%r12), %rsi
	leaq	168(%r14), %rdi
	call	check_cert_usable.isra.0
	testl	%eax, %eax
	je	.L180
	movslq	24(%r12), %rax
	movq	1168(%r14), %rdx
	leaq	(%rax,%rax,4), %rax
	leaq	(%rdx,%rax,8), %rax
	movq	40(%rax), %rdi
	movl	20(%r12), %eax
	cmpl	$408, %eax
	jne	.L153
.L186:
	cmpl	$-1, -68(%rbp)
	je	.L185
.L154:
	movl	32(%r12), %eax
	movq	6272(%r14), %rdx
	cmpl	-68(%rbp), %eax
	je	.L156
	testl	%eax, %eax
	jne	.L144
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L151:
	movq	-88(%rbp), %rdx
	leaq	168(%r14), %rdi
	leaq	12(%r12), %rsi
	movq	%rbx, %rcx
	call	check_cert_usable.isra.0
	movq	%rbx, %rdi
	testl	%eax, %eax
	je	.L180
	movl	20(%r12), %eax
	cmpl	$408, %eax
	je	.L186
.L153:
	cmpl	$912, %eax
	jne	.L187
	call	EVP_PKEY_get0@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L180
	movl	12(%r12), %ecx
	testl	%ecx, %ecx
	je	.L180
	movl	16(%r12), %edi
	call	ssl_md@PLT
	testq	%rax, %rax
	je	.L180
	movq	%r15, %rdi
	movq	%rax, -96(%rbp)
	call	RSA_size@PLT
	movq	-96(%rbp), %r11
	movl	%eax, %r15d
	movq	%r11, %rdi
	call	EVP_MD_size@PLT
	movq	6272(%r14), %rdx
	leal	2(%rax,%rax), %eax
	cmpl	%eax, %r15d
	jge	.L156
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L185:
	call	EVP_PKEY_get0_EC_KEY@PLT
	movq	%rax, %rdi
	call	EC_KEY_get0_group@PLT
	movq	%rax, %rdi
	call	EC_GROUP_get_curve_name@PLT
	movl	%eax, -68(%rbp)
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L162:
	xorl	%r12d, %r12d
	jmp	.L141
.L187:
	movq	6272(%r14), %rdx
	jmp	.L156
.L184:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1642:
	.size	find_sig_alg, .-find_sig_alg
	.p2align 4
	.type	tls1_shared_group.part.0, @function
tls1_shared_group.part.0:
.LFB1661:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, -84(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	1168(%rdi), %rax
	movl	28(%rax), %eax
	andl	$196608, %eax
	cmpl	$-2, %esi
	je	.L229
	movq	1720(%rdi), %rbx
	movq	1728(%rdi), %r13
	testb	$64, 1494(%rdi)
	je	.L192
	cmpl	$131072, %eax
	je	.L208
	cmpl	$196608, %eax
	je	.L209
	cmpl	$65536, %eax
	je	.L230
.L203:
	movq	1712(%r15), %r10
	testq	%r10, %r10
	je	.L211
	movq	1704(%r15), %r9
	jmp	.L195
.L192:
	cmpl	$131072, %eax
	je	.L212
	cmpl	$196608, %eax
	je	.L213
	cmpl	$65536, %eax
	je	.L214
.L204:
	movq	1712(%r15), %rax
	movq	%rbx, %r9
	testq	%rax, %rax
	je	.L215
	movq	1704(%r15), %rbx
	movq	%r13, %r10
	movq	%rax, %r13
.L195:
	xorl	%r11d, %r11d
	testq	%r9, %r9
	je	.L194
.L193:
	xorl	%r11d, %r11d
	movq	%r15, -96(%rbp)
	xorl	%r14d, %r14d
	movl	%r11d, %r15d
	.p2align 4,,10
	.p2align 3
.L199:
	movzwl	(%r10,%r14,2), %r12d
	testq	%rbx, %rbx
	je	.L196
	xorl	%eax, %eax
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L231:
	addq	$1, %rax
	cmpq	%rbx, %rax
	je	.L196
.L198:
	cmpw	0(%r13,%rax,2), %r12w
	jne	.L231
	leal	-1(%r12), %eax
	cmpw	$29, %ax
	ja	.L196
	movzwl	%r12w, %eax
	movq	-96(%rbp), %rdi
	leaq	-58(%rbp), %r8
	movq	%r9, -80(%rbp)
	subl	$1, %eax
	leaq	nid_list(%rip), %rdx
	movl	$131077, %esi
	movq	%r10, -72(%rbp)
	cltq
	movb	$0, -58(%rbp)
	leaq	(%rax,%rax,2), %rax
	movb	%r12b, -57(%rbp)
	leaq	(%rdx,%rax,4), %rax
	movl	(%rax), %ecx
	movl	4(%rax), %edx
	call	ssl_security@PLT
	movq	-72(%rbp), %r10
	movq	-80(%rbp), %r9
	testl	%eax, %eax
	je	.L196
	cmpl	-84(%rbp), %r15d
	je	.L188
	addl	$1, %r15d
.L196:
	addq	$1, %r14
	cmpq	%r9, %r14
	jb	.L199
	movl	%r15d, %r11d
.L194:
	cmpl	$-1, -84(%rbp)
	movl	%r11d, %r12d
	je	.L188
.L200:
	xorl	%r12d, %r12d
.L188:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L232
	addq	$56, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L229:
	.cfi_restore_state
	testl	%eax, %eax
	je	.L190
	movq	168(%rdi), %rax
	movq	568(%rax), %rax
	movl	24(%rax), %eax
	cmpq	$50380843, %rax
	je	.L206
	movl	$24, %r12d
	cmpq	$50380844, %rax
	jne	.L200
	jmp	.L188
.L190:
	testb	$64, 1494(%rdi)
	movl	$0, -84(%rbp)
	movq	1720(%rdi), %rbx
	movq	1728(%rdi), %r13
	je	.L204
	jmp	.L203
.L209:
	movl	$2, %r9d
	leaq	suiteb_curves(%rip), %r10
	jmp	.L193
.L230:
	movl	$1, %r9d
	leaq	suiteb_curves(%rip), %r10
	jmp	.L193
.L214:
	movq	%rbx, %r9
	movq	%r13, %r10
	movl	$1, %ebx
	leaq	suiteb_curves(%rip), %r13
	jmp	.L195
.L213:
	movq	%rbx, %r9
	movq	%r13, %r10
	movl	$2, %ebx
	leaq	suiteb_curves(%rip), %r13
	jmp	.L195
.L212:
	movq	%rbx, %r9
	movq	%r13, %r10
	movl	$1, %ebx
	leaq	2+suiteb_curves(%rip), %r13
	jmp	.L195
.L208:
	movl	$1, %r9d
	leaq	2+suiteb_curves(%rip), %r10
	jmp	.L193
.L215:
	movq	%r13, %r10
	movl	$5, %ebx
	leaq	eccurves_default(%rip), %r13
	jmp	.L195
.L211:
	movl	$5, %r9d
	leaq	eccurves_default(%rip), %r10
	jmp	.L193
.L206:
	movl	$23, %r12d
	jmp	.L188
.L232:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1661:
	.size	tls1_shared_group.part.0, .-tls1_shared_group.part.0
	.p2align 4
	.globl	tls1_default_timeout
	.type	tls1_default_timeout, @function
tls1_default_timeout:
.LFB1577:
	.cfi_startproc
	endbr64
	movl	$7200, %eax
	ret
	.cfi_endproc
.LFE1577:
	.size	tls1_default_timeout, .-tls1_default_timeout
	.p2align 4
	.globl	tls1_new
	.type	tls1_new, @function
tls1_new:
.LFB1578:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	ssl3_new@PLT
	testl	%eax, %eax
	je	.L234
	movq	8(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
.L234:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1578:
	.size	tls1_new, .-tls1_new
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"../deps/openssl/openssl/ssl/t1_lib.c"
	.text
	.p2align 4
	.globl	tls1_free
	.type	tls1_free, @function
tls1_free:
.LFB1579:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$117, %edx
	leaq	.LC5(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	1736(%rdi), %rdi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	ssl3_free@PLT
	.cfi_endproc
.LFE1579:
	.size	tls1_free, .-tls1_free
	.p2align 4
	.globl	tls1_clear
	.type	tls1_clear, @function
tls1_clear:
.LFB1580:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	ssl3_clear@PLT
	testl	%eax, %eax
	je	.L242
	movq	8(%rbx), %rax
	movl	(%rax), %eax
	cmpl	$65536, %eax
	je	.L249
	movl	%eax, (%rbx)
	movl	$1, %eax
.L242:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L249:
	.cfi_restore_state
	movl	$772, (%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1580:
	.size	tls1_clear, .-tls1_clear
	.p2align 4
	.globl	tls1_group_id_lookup
	.type	tls1_group_id_lookup, @function
tls1_group_id_lookup:
.LFB1581:
	.cfi_startproc
	endbr64
	leal	-1(%rdi), %eax
	cmpw	$29, %ax
	ja	.L252
	movzwl	%di, %edi
	leaq	nid_list(%rip), %rax
	subl	$1, %edi
	movslq	%edi, %rdi
	leaq	(%rdi,%rdi,2), %rdx
	leaq	(%rax,%rdx,4), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L252:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1581:
	.size	tls1_group_id_lookup, .-tls1_group_id_lookup
	.p2align 4
	.globl	tls1_get_supported_groups
	.type	tls1_get_supported_groups, @function
tls1_get_supported_groups:
.LFB1583:
	.cfi_startproc
	endbr64
	movq	1168(%rdi), %rax
	movl	28(%rax), %eax
	andl	$196608, %eax
	cmpl	$131072, %eax
	je	.L254
	cmpl	$196608, %eax
	je	.L255
	cmpl	$65536, %eax
	je	.L261
	movq	1712(%rdi), %rax
	testq	%rax, %rax
	je	.L262
	movq	%rax, (%rsi)
	movq	1704(%rdi), %rax
	movq	%rax, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L261:
	leaq	suiteb_curves(%rip), %rax
	movq	%rax, (%rsi)
	movq	$1, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	leaq	suiteb_curves(%rip), %rax
	movq	%rax, (%rsi)
	movq	$2, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L254:
	leaq	2+suiteb_curves(%rip), %rax
	movq	%rax, (%rsi)
	movq	$1, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L262:
	leaq	eccurves_default(%rip), %rax
	movq	%rax, (%rsi)
	movq	$5, (%rdx)
	ret
	.cfi_endproc
.LFE1583:
	.size	tls1_get_supported_groups, .-tls1_get_supported_groups
	.p2align 4
	.globl	tls_curve_allowed
	.type	tls_curve_allowed, @function
tls_curve_allowed:
.LFB1584:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r9d
	leal	-1(%rsi), %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	cmpw	$29, %dx
	ja	.L266
	movzwl	%si, %eax
	movb	%sil, -9(%rbp)
	leaq	-10(%rbp), %r8
	movl	%r9d, %esi
	subl	$1, %eax
	movb	$0, -10(%rbp)
	cltq
	leaq	(%rax,%rax,2), %rdx
	leaq	nid_list(%rip), %rax
	leaq	(%rax,%rdx,4), %rax
	movl	(%rax), %ecx
	movl	4(%rax), %edx
	call	ssl_security@PLT
.L263:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L268
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L266:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L263
.L268:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1584:
	.size	tls_curve_allowed, .-tls_curve_allowed
	.p2align 4
	.globl	tls1_shared_group
	.type	tls1_shared_group, @function
tls1_shared_group:
.LFB1586:
	.cfi_startproc
	endbr64
	movl	56(%rdi), %eax
	testl	%eax, %eax
	je	.L270
	jmp	tls1_shared_group.part.0
	.p2align 4,,10
	.p2align 3
.L270:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1586:
	.size	tls1_shared_group, .-tls1_shared_group
	.p2align 4
	.globl	tls1_set_groups
	.type	tls1_set_groups, @function
tls1_set_groups:
.LFB1587:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rcx, %rcx
	je	.L285
	movq	%rdi, %rbx
	movq	%rsi, %r12
	leaq	(%rcx,%rcx), %rdi
	movq	%rdx, %r14
	leaq	.LC5(%rip), %rsi
	movl	$353, %edx
	movq	%rcx, %r13
	call	CRYPTO_malloc@PLT
	xorl	%edi, %edi
	movl	$1, %r8d
	movq	%rax, %r15
	xorl	%eax, %eax
	testq	%r15, %r15
	je	.L286
	.p2align 4,,10
	.p2align 3
.L274:
	movl	(%r14,%rdi,4), %esi
	leaq	nid_list(%rip), %rdx
	xorl	%ecx, %ecx
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L275:
	addq	$1, %rcx
	addq	$12, %rdx
	cmpq	$30, %rcx
	je	.L276
.L278:
	cmpl	(%rdx), %esi
	jne	.L275
	addl	$1, %ecx
	movq	%r8, %rdx
	salq	%cl, %rdx
	testq	%rax, %rdx
	jne	.L276
	movw	%cx, (%r15,%rdi,2)
	addq	$1, %rdi
	orq	%rdx, %rax
	cmpq	%rdi, %r13
	jne	.L274
	movq	(%rbx), %rdi
	movl	$370, %edx
	leaq	.LC5(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r15, (%rbx)
	movl	$1, %eax
	movq	%r13, (%r12)
.L271:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L276:
	.cfi_restore_state
	movq	%r15, %rdi
	movl	$364, %edx
	leaq	.LC5(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L285:
	.cfi_restore_state
	movl	$350, %r8d
	movl	$271, %edx
	movl	$629, %esi
	movl	$20, %edi
	leaq	.LC5(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L286:
	.cfi_restore_state
	movl	$354, %r8d
	movl	$65, %edx
	movl	$629, %esi
	movl	$20, %edi
	leaq	.LC5(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L271
	.cfi_endproc
.LFE1587:
	.size	tls1_set_groups, .-tls1_set_groups
	.p2align 4
	.globl	tls1_set_groups_list
	.type	tls1_set_groups_list, @function
tls1_set_groups_list:
.LFB1589:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	nid_cb(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movl	$58, %esi
	pushq	%r12
	leaq	-160(%rbp), %r8
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rdx, %rdi
	movl	$1, %edx
	subq	$144, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$0, -160(%rbp)
	call	CONF_parse_list@PLT
	testl	%eax, %eax
	je	.L287
	movl	$1, %eax
	testq	%r12, %r12
	je	.L287
	movq	-160(%rbp), %rcx
	leaq	-152(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	tls1_set_groups
.L287:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L296
	addq	$144, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L296:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1589:
	.size	tls1_set_groups_list, .-tls1_set_groups_list
	.p2align 4
	.globl	tls1_check_group_id
	.type	tls1_check_group_id, @function
tls1_check_group_id:
.LFB1592:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testw	%si, %si
	je	.L298
	movq	1168(%rdi), %rax
	movq	%rdi, %rbx
	movl	%esi, %r12d
	movl	28(%rax), %eax
	andl	$196608, %eax
	jne	.L334
	movzwl	%si, %edi
	testl	%edx, %edx
	jne	.L310
.L303:
	leal	-1(%rsi), %eax
	cmpw	$29, %ax
	ja	.L298
.L306:
	leal	-1(%rdi), %eax
	movb	%sil, -25(%rbp)
	leaq	-26(%rbp), %r8
	movq	%rbx, %rdi
	cltq
	movl	$131078, %esi
	movb	$0, -26(%rbp)
	leaq	(%rax,%rax,2), %rdx
	leaq	nid_list(%rip), %rax
	leaq	(%rax,%rdx,4), %rax
	movl	(%rax), %ecx
	movl	4(%rax), %edx
	call	ssl_security@PLT
	testl	%eax, %eax
	je	.L298
	movl	56(%rbx), %edx
	movl	$1, %eax
	testl	%edx, %edx
	je	.L297
	movq	1720(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L297
	movq	1728(%rbx), %rcx
	xorl	%eax, %eax
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L335:
	addq	$1, %rax
	cmpq	%rax, %rdx
	je	.L298
.L308:
	cmpw	(%rcx,%rax,2), %r12w
	jne	.L335
	movl	$1, %eax
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L334:
	movq	168(%rdi), %rcx
	movq	568(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L300
	movl	24(%rcx), %ecx
	cmpq	$50380843, %rcx
	je	.L336
	cmpw	$24, %si
	jne	.L298
	cmpq	$50380844, %rcx
	je	.L302
	.p2align 4,,10
	.p2align 3
.L298:
	xorl	%eax, %eax
.L297:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L337
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L336:
	.cfi_restore_state
	cmpw	$23, %si
	jne	.L298
.L302:
	movzwl	%si, %edi
	testl	%edx, %edx
	je	.L306
.L309:
	cmpl	$131072, %eax
	je	.L312
	movl	$2, %edx
	leaq	suiteb_curves(%rip), %rcx
	cmpl	$196608, %eax
	je	.L304
	movl	$1, %edx
	cmpl	$65536, %eax
	je	.L304
.L310:
	movq	1712(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L315
	movq	1704(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L298
	.p2align 4,,10
	.p2align 3
.L304:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L305:
	cmpw	(%rcx,%rax,2), %r12w
	je	.L303
	addq	$1, %rax
	cmpq	%rdx, %rax
	jb	.L305
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L300:
	movzwl	%si, %edi
	testl	%edx, %edx
	jne	.L309
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L312:
	movl	$1, %edx
	leaq	2+suiteb_curves(%rip), %rcx
	jmp	.L304
.L315:
	movl	$5, %edx
	leaq	eccurves_default(%rip), %rcx
	jmp	.L304
.L337:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1592:
	.size	tls1_check_group_id, .-tls1_check_group_id
	.p2align 4
	.type	tls1_check_cert_param, @function
tls1_check_cert_param:
.LFB1594:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	call	X509_get0_pubkey@PLT
	testq	%rax, %rax
	je	.L345
	movq	%rax, %rdi
	movq	%rax, %r12
	call	EVP_PKEY_id@PLT
	cmpl	$408, %eax
	je	.L341
.L350:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L341:
	.cfi_restore_state
	movq	%r12, %rdi
	call	EVP_PKEY_id@PLT
	cmpl	$408, %eax
	je	.L342
.L346:
	movq	%r12, %rdi
	call	EVP_PKEY_get0_EC_KEY@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L343
	call	EC_KEY_get0_group@PLT
	movq	%rax, %rdi
	call	EC_GROUP_get_curve_name@PLT
	leaq	nid_list(%rip), %rdx
	xorl	%ecx, %ecx
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L348:
	addq	$1, %rcx
	addq	$12, %rdx
	cmpq	$30, %rcx
	je	.L343
.L349:
	cmpl	(%rdx), %eax
	jne	.L348
	leal	1(%rcx), %r12d
	movzwl	%r12w, %esi
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L342:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	tls1_check_pkey_comp.part.0
	testl	%eax, %eax
	jne	.L346
.L345:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L343:
	.cfi_restore_state
	xorl	%esi, %esi
	xorl	%r12d, %r12d
.L347:
	movl	56(%rbx), %eax
	xorl	%edx, %edx
	movq	%rbx, %rdi
	testl	%eax, %eax
	sete	%dl
	call	tls1_check_group_id
	testl	%eax, %eax
	je	.L345
	testl	%r13d, %r13d
	je	.L350
	movq	1168(%rbx), %rax
	testl	$196608, 28(%rax)
	je	.L350
	cmpw	$23, %r12w
	je	.L353
	movl	$795, %esi
	cmpw	$24, %r12w
	jne	.L345
.L351:
	movq	6272(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L345
	movq	6264(%rbx), %rdi
	xorl	%eax, %eax
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L376:
	addq	$1, %rax
	cmpq	%rcx, %rax
	je	.L345
.L352:
	movq	(%rdi,%rax,8), %rdx
	cmpl	%esi, 28(%rdx)
	jne	.L376
	jmp	.L350
.L353:
	movl	$794, %esi
	jmp	.L351
	.cfi_endproc
.LFE1594:
	.size	tls1_check_cert_param, .-tls1_check_cert_param
	.p2align 4
	.globl	tls1_get_formatlist
	.type	tls1_get_formatlist, @function
tls1_get_formatlist:
.LFB1593:
	.cfi_startproc
	endbr64
	movq	1680(%rdi), %rax
	testq	%rax, %rax
	je	.L378
	movq	%rax, (%rsi)
	movq	1672(%rdi), %rax
	movq	%rax, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L378:
	leaq	ecformats_default(%rip), %rax
	movq	%rax, (%rsi)
	movq	1168(%rdi), %rax
	testl	$196608, 28(%rax)
	sete	%al
	movzbl	%al, %eax
	addq	$2, %rax
	movq	%rax, (%rdx)
	ret
	.cfi_endproc
.LFE1593:
	.size	tls1_get_formatlist, .-tls1_get_formatlist
	.p2align 4
	.globl	tls1_check_ec_tmp_key
	.type	tls1_check_ec_tmp_key, @function
tls1_check_ec_tmp_key:
.LFB1595:
	.cfi_startproc
	endbr64
	movq	1168(%rdi), %rax
	testl	$196608, 28(%rax)
	je	.L394
	cmpq	$50380843, %rsi
	je	.L395
	xorl	%eax, %eax
	cmpq	$50380844, %rsi
	je	.L396
.L391:
	ret
	.p2align 4,,10
	.p2align 3
.L394:
	movl	56(%rdi), %eax
	testl	%eax, %eax
	je	.L391
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	tls1_shared_group.part.0
	popq	%rbp
	.cfi_def_cfa 7, 8
	testw	%ax, %ax
	setne	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L396:
	.cfi_restore 6
	movl	$1, %edx
	movl	$24, %esi
	jmp	tls1_check_group_id
	.p2align 4,,10
	.p2align 3
.L395:
	movl	$1, %edx
	movl	$23, %esi
	jmp	tls1_check_group_id
	.cfi_endproc
.LFE1595:
	.size	tls1_check_ec_tmp_key, .-tls1_check_ec_tmp_key
	.p2align 4
	.globl	tls1_lookup_md
	.type	tls1_lookup_md, @function
tls1_lookup_md:
.LFB1597:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L411
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	movl	12(%rdi), %edx
	testl	%edx, %edx
	jne	.L412
.L400:
	movl	$1, %r8d
	testq	%rbx, %rbx
	je	.L397
	movq	%rax, (%rbx)
.L397:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L412:
	.cfi_restore_state
	movl	16(%rdi), %edi
	call	ssl_md@PLT
	testq	%rax, %rax
	jne	.L400
	addq	$8, %rsp
	xorl	%r8d, %r8d
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L411:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%r8d, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE1597:
	.size	tls1_lookup_md, .-tls1_lookup_md
	.p2align 4
	.globl	tls12_get_psigalgs
	.type	tls12_get_psigalgs, @function
tls12_get_psigalgs:
.LFB1601:
	.cfi_startproc
	endbr64
	movq	1168(%rdi), %rcx
	movl	28(%rcx), %eax
	andl	$196608, %eax
	cmpl	$131072, %eax
	je	.L414
	cmpl	$196608, %eax
	je	.L415
	cmpl	$65536, %eax
	je	.L425
	cmpl	%esi, 56(%rdi)
	je	.L426
.L419:
	movq	408(%rcx), %rax
	testq	%rax, %rax
	je	.L420
	movq	%rax, (%rdx)
	movq	416(%rcx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L425:
	leaq	suiteb_sigalgs(%rip), %rax
	movq	%rax, (%rdx)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L415:
	leaq	suiteb_sigalgs(%rip), %rax
	movq	%rax, (%rdx)
	movl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L414:
	leaq	2+suiteb_sigalgs(%rip), %rax
	movq	%rax, (%rdx)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L426:
	movq	424(%rcx), %rax
	testq	%rax, %rax
	je	.L419
	movq	%rax, (%rdx)
	movq	432(%rcx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L420:
	leaq	tls12_sigalgs(%rip), %rax
	movq	%rax, (%rdx)
	movl	$23, %eax
	ret
	.cfi_endproc
.LFE1601:
	.size	tls12_get_psigalgs, .-tls12_get_psigalgs
	.p2align 4
	.globl	tls_check_sigalg_curve
	.type	tls_check_sigalg_curve, @function
tls_check_sigalg_curve:
.LFB1602:
	.cfi_startproc
	endbr64
	movq	1168(%rdi), %rax
	movl	%esi, %r9d
	movq	408(%rax), %rdi
	testq	%rdi, %rdi
	je	.L436
	movq	416(%rax), %r8
	testq	%r8, %r8
	je	.L437
.L428:
	xorl	%esi, %esi
	leaq	920+sigalg_lookup_tbl(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L434:
	movzwl	(%rdi,%rsi,2), %edx
	leaq	sigalg_lookup_tbl(%rip), %rax
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L444:
	addq	$40, %rax
	cmpq	%rcx, %rax
	je	.L432
.L431:
	cmpw	8(%rax), %dx
	jne	.L444
	cmpl	$408, 20(%rax)
	je	.L445
.L432:
	addq	$1, %rsi
	cmpq	%r8, %rsi
	jb	.L434
.L437:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L445:
	movl	32(%rax), %eax
	cmpl	%r9d, %eax
	jne	.L432
	testl	%eax, %eax
	je	.L432
	movl	$1, %eax
	ret
.L436:
	movl	$23, %r8d
	leaq	tls12_sigalgs(%rip), %rdi
	jmp	.L428
	.cfi_endproc
.LFE1602:
	.size	tls_check_sigalg_curve, .-tls_check_sigalg_curve
	.p2align 4
	.globl	tls12_check_peer_sigalg
	.type	tls12_check_peer_sigalg, @function
tls12_check_peer_sigalg:
.LFB1604:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%rdx, %rdi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movl	%esi, -84(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_PKEY_id@PLT
	movl	%eax, %r13d
	cmpl	$-1, %eax
	je	.L446
	movq	8(%r15), %rcx
	movq	192(%rcx), %rax
	movl	96(%rax), %eax
	andl	$8, %eax
	movl	%eax, %edx
	je	.L577
.L448:
	leaq	sigalg_lookup_tbl(%rip), %rbx
	leaq	920(%rbx), %rax
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L578:
	addq	$40, %rbx
	cmpq	%rax, %rbx
	je	.L452
.L451:
	cmpw	8(%rbx), %r12w
	jne	.L578
	testl	%edx, %edx
	jne	.L453
	movl	(%rcx), %eax
	cmpl	$65536, %eax
	je	.L453
	cmpl	$771, %eax
	jg	.L579
	.p2align 4,,10
	.p2align 3
.L453:
	movl	20(%rbx), %eax
	cmpl	%r13d, %eax
	je	.L454
	cmpl	$6, %r13d
	jne	.L452
	cmpl	$912, %eax
	je	.L454
.L452:
	movl	$1057, %r9d
.L575:
	leaq	.LC5(%rip), %r8
	movl	$370, %ecx
.L576:
	movl	$333, %edx
	movl	$47, %esi
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	ossl_statem_fatal@PLT
.L446:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L580
	addq	$56, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L577:
	.cfi_restore_state
	movl	(%rcx), %eax
	cmpl	$771, %eax
	jle	.L448
	cmpl	$65536, %eax
	je	.L448
	movl	$1040, %r9d
	cmpl	$116, %r13d
	je	.L575
	cmpl	$6, %r13d
	movl	$912, %eax
	cmove	%eax, %r13d
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L454:
	movq	%r14, %rdi
	call	EVP_PKEY_id@PLT
	leaq	-72(%rbp), %rsi
	movl	%eax, %edi
	call	ssl_cert_lookup_by_nid@PLT
	testl	%eax, %eax
	je	.L456
	movl	-72(%rbp), %eax
	cmpl	%eax, 24(%rbx)
	jne	.L456
	cmpl	$408, %r13d
	je	.L581
	movq	1168(%r15), %rax
	movl	$1110, %r9d
	movl	$370, %ecx
	leaq	.LC5(%rip), %r8
	movl	28(%rax), %esi
	testl	$196608, %esi
	jne	.L574
.L491:
	cmpl	$1, 56(%r15)
	je	.L582
.L475:
	movq	408(%rax), %rcx
	testq	%rcx, %rcx
	je	.L501
	movq	416(%rax), %rdx
.L476:
	testq	%rdx, %rdx
	jne	.L474
.L477:
	cmpl	$64, 12(%rbx)
	jne	.L480
	andl	$196609, %esi
	jne	.L480
.L484:
	movl	16(%rbx), %edi
	call	ssl_md@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L583
	movzwl	-84(%rbp), %r14d
	movl	12(%rbx), %eax
	rolw	$8, %r14w
	movw	%r14w, -58(%rbp)
	testl	%eax, %eax
	je	.L584
	movl	16(%rbx), %edi
	call	ssl_md@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L489
	call	EVP_MD_size@PLT
	sall	$2, %eax
	movl	%eax, %r12d
	je	.L489
.L496:
	movq	%r13, %rdi
	call	EVP_MD_type@PLT
	movl	%eax, %ecx
.L494:
	leaq	-58(%rbp), %r8
	movl	%r12d, %edx
	movl	$327693, %esi
	movq	%r15, %rdi
	call	ssl_security@PLT
	testl	%eax, %eax
	je	.L489
	movq	168(%r15), %rax
	movl	$1, %r13d
	movq	%rbx, 776(%rax)
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L579:
	movl	12(%rbx), %eax
	cmpl	$64, %eax
	je	.L452
	cmpl	$675, %eax
	jne	.L453
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L456:
	movl	$1064, %r9d
	jmp	.L575
.L587:
	je	.L477
.L478:
	movl	12(%rbx), %ecx
	testl	%ecx, %ecx
	jne	.L484
	movzwl	-84(%rbp), %r14d
	movzwl	8(%rbx), %eax
	rolw	$8, %r14w
	movw	%r14w, -58(%rbp)
	cmpw	$2055, %ax
	je	.L585
	cmpw	$2056, %ax
	je	.L586
.L489:
	movl	$1145, %r9d
	leaq	.LC5(%rip), %r8
	movl	$370, %ecx
.L574:
	movl	$333, %edx
	movl	$40, %esi
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	ossl_statem_fatal@PLT
	jmp	.L446
.L582:
	movq	424(%rax), %rcx
	testq	%rcx, %rcx
	je	.L475
	movq	432(%rax), %rdx
	jmp	.L476
.L501:
	leaq	tls12_sigalgs(%rip), %rcx
	movl	$23, %edx
	.p2align 4,,10
	.p2align 3
.L474:
	xorl	%eax, %eax
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L588:
	addq	$1, %rax
	cmpq	%rdx, %rax
	jnb	.L587
.L479:
	cmpw	%r12w, (%rcx,%rax,2)
	jne	.L588
	jmp	.L478
.L581:
	movq	%r14, %rdi
	call	EVP_PKEY_id@PLT
	cmpl	$408, %eax
	je	.L589
.L459:
	movq	8(%r15), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L460
	movl	(%rax), %eax
	cmpl	$65536, %eax
	je	.L461
	cmpl	$771, %eax
	jg	.L492
.L461:
	movq	1168(%r15), %rdx
	testl	$196608, 28(%rdx)
	je	.L493
.L492:
	movq	%r14, %rdi
	call	EVP_PKEY_get0_EC_KEY@PLT
	movq	%rax, %rdi
	call	EC_KEY_get0_group@PLT
	movq	%rax, %rdi
	call	EC_GROUP_get_curve_name@PLT
	movl	%eax, %r9d
	movl	32(%rbx), %eax
	cmpl	%r9d, %eax
	je	.L462
	movl	$1086, %r9d
	leaq	.LC5(%rip), %r8
	movl	$378, %ecx
	testl	%eax, %eax
	jne	.L576
.L462:
	movq	8(%r15), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L466
	movl	(%rax), %eax
.L493:
	cmpl	$65536, %eax
	je	.L466
	cmpl	$771, %eax
	jle	.L466
	movq	1168(%r15), %rax
	movl	28(%rax), %esi
	movl	%esi, %edi
	andl	$196608, %edi
.L473:
	cmpl	$131072, %edi
	je	.L498
	leaq	suiteb_sigalgs(%rip), %rcx
	movl	$2, %edx
	cmpl	$196608, %edi
	je	.L474
	movl	$1, %edx
	cmpl	$65536, %edi
	je	.L474
	jmp	.L491
.L498:
	leaq	2+suiteb_sigalgs(%rip), %rcx
	movl	$1, %edx
	jmp	.L474
.L480:
	movl	$1125, %r9d
	leaq	.LC5(%rip), %r8
	movl	$370, %ecx
	jmp	.L574
.L460:
	movq	1168(%r15), %rax
	testl	$196608, 28(%rax)
	jne	.L492
.L466:
	movq	%r14, %rdi
	call	EVP_PKEY_get0_EC_KEY@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L464
	call	EC_KEY_get0_group@PLT
	movq	%rax, %rdi
	call	EC_GROUP_get_curve_name@PLT
	leaq	nid_list(%rip), %rdx
	movl	%eax, %ecx
	xorl	%eax, %eax
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L469:
	addq	$1, %rax
	addq	$12, %rdx
	cmpq	$30, %rax
	je	.L464
.L470:
	cmpl	(%rdx), %ecx
	jne	.L469
	addl	$1, %eax
	movzwl	%ax, %esi
.L468:
	movl	$1, %edx
	movq	%r15, %rdi
	call	tls1_check_group_id
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L590
	movq	1168(%r15), %rax
	movl	28(%rax), %esi
	movl	%esi, %edi
	andl	$196608, %edi
	je	.L491
	movzwl	-84(%rbp), %edx
	movl	$1102, %r9d
	movl	$370, %ecx
	leaq	.LC5(%rip), %r8
	andb	$-2, %dh
	cmpw	$1027, %dx
	je	.L473
	jmp	.L574
.L585:
	movl	$128, %r12d
	jmp	.L494
.L589:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	tls1_check_pkey_comp.part.0
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.L459
	movl	$1074, %r9d
	leaq	.LC5(%rip), %r8
	movl	$162, %ecx
	movq	%r15, %rdi
	movl	$333, %edx
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L446
.L464:
	xorl	%esi, %esi
	jmp	.L468
.L583:
	movl	$1130, %r9d
	leaq	.LC5(%rip), %r8
	movl	$368, %ecx
	jmp	.L574
.L586:
	movl	$224, %r12d
	jmp	.L494
.L590:
	movl	$1094, %r9d
	leaq	.LC5(%rip), %r8
	movl	$378, %ecx
	movq	%r15, %rdi
	movl	$333, %edx
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L446
.L580:
	call	__stack_chk_fail@PLT
.L584:
	movzwl	8(%rbx), %eax
	cmpw	$2055, %ax
	je	.L591
	cmpw	$2056, %ax
	jne	.L489
	movl	$224, %r12d
	jmp	.L496
.L591:
	movl	$128, %r12d
	jmp	.L496
	.cfi_endproc
.LFE1604:
	.size	tls12_check_peer_sigalg, .-tls12_check_peer_sigalg
	.p2align 4
	.globl	SSL_get_peer_signature_type_nid
	.type	SSL_get_peer_signature_type_nid, @function
SSL_get_peer_signature_type_nid:
.LFB1605:
	.cfi_startproc
	endbr64
	movq	168(%rdi), %rax
	movq	776(%rax), %rax
	testq	%rax, %rax
	je	.L594
	movl	20(%rax), %eax
	movl	%eax, (%rsi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L594:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1605:
	.size	SSL_get_peer_signature_type_nid, .-SSL_get_peer_signature_type_nid
	.p2align 4
	.globl	SSL_get_signature_type_nid
	.type	SSL_get_signature_type_nid, @function
SSL_get_signature_type_nid:
.LFB1606:
	.cfi_startproc
	endbr64
	movq	168(%rdi), %rax
	movq	728(%rax), %rax
	testq	%rax, %rax
	je	.L597
	movl	20(%rax), %eax
	movl	%eax, (%rsi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L597:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1606:
	.size	SSL_get_signature_type_nid, .-SSL_get_signature_type_nid
	.p2align 4
	.globl	ssl_cipher_disabled
	.type	ssl_cipher_disabled, @function
ssl_cipher_disabled:
.LFB1608:
	.cfi_startproc
	endbr64
	movq	168(%rdi), %rax
	movq	%rsi, %r8
	movl	$1, %r9d
	movl	%edx, %esi
	movl	28(%r8), %edx
	testl	%edx, 820(%rax)
	jne	.L628
	movl	32(%r8), %r10d
	testl	%r10d, 824(%rax)
	jne	.L628
	movl	832(%rax), %r10d
	testl	%r10d, %r10d
	je	.L628
	movq	8(%rdi), %r9
	movq	192(%r9), %r9
	testb	$8, 96(%r9)
	jne	.L600
	movl	44(%r8), %r11d
	cmpl	$769, %r11d
	jne	.L601
	testl	%ecx, %ecx
	je	.L601
	xorl	%r11d, %r11d
	andl	$132, %edx
	sete	%r11b
	addl	$768, %r11d
.L601:
	movl	$1, %r9d
	cmpl	%r11d, %r10d
	jl	.L628
	movl	828(%rax), %eax
	cmpl	%eax, 48(%r8)
	jl	.L628
.L602:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	68(%r8), %edx
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ssl_security@PLT
	xorl	%r9d, %r9d
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%r9b
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L600:
	.cfi_restore 6
	movl	52(%r8), %edx
	cmpl	$256, %edx
	je	.L603
	cmpl	$256, %r10d
	movl	$65280, %ecx
	cmove	%ecx, %r10d
.L604:
	movl	$1, %r9d
	cmpl	%edx, %r10d
	jg	.L628
.L607:
	movl	56(%r8), %edx
	movl	828(%rax), %eax
	cmpl	$256, %edx
	je	.L605
	cmpl	$256, %eax
	movl	$65280, %ecx
	cmove	%ecx, %eax
.L606:
	movl	$1, %r9d
	cmpl	%edx, %eax
	jge	.L602
.L628:
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L603:
	movl	$65280, %edx
	cmpl	$256, %r10d
	jne	.L604
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L605:
	movl	$65280, %edx
	cmpl	$256, %eax
	jne	.L606
	jmp	.L602
	.cfi_endproc
.LFE1608:
	.size	ssl_cipher_disabled, .-ssl_cipher_disabled
	.p2align 4
	.type	tls12_sigalg_allowed, @function
tls12_sigalg_allowed:
.LFB1613:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L636
	movq	%rdx, %rbx
	movl	12(%rdx), %edx
	movq	%rdi, %r12
	movl	%esi, %r13d
	testl	%edx, %edx
	jne	.L633
.L637:
	movq	8(%r12), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L635
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L638
	cmpl	$65536, %eax
	jne	.L679
.L638:
	movl	56(%r12), %eax
	testl	%eax, %eax
	jne	.L635
	movq	168(%r12), %rax
	cmpl	$771, 828(%rax)
	jle	.L635
	cmpl	$116, 20(%rbx)
	je	.L636
	movl	16(%rbx), %eax
	cmpl	$10, %eax
	je	.L636
	cmpl	$1, %eax
	jbe	.L636
	.p2align 4,,10
	.p2align 3
.L635:
	movslq	24(%rbx), %rdi
	call	ssl_cert_is_disabled@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	jne	.L636
	movl	20(%rbx), %eax
	leal	-979(%rax), %edx
	cmpl	$1, %edx
	jbe	.L639
	cmpl	$811, %eax
	jne	.L643
.L639:
	movl	56(%r12), %r14d
	movq	8(%r12), %rax
	testl	%r14d, %r14d
	je	.L642
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L643
	movl	(%rax), %eax
	cmpl	$65536, %eax
	je	.L643
	cmpl	$771, %eax
	jg	.L636
	.p2align 4,,10
	.p2align 3
.L643:
	movl	12(%rbx), %ecx
	testl	%ecx, %ecx
	jne	.L680
	movzwl	8(%rbx), %edx
	movl	$128, %r15d
	cmpw	$2055, %dx
	je	.L648
	cmpw	$2056, %dx
	movl	$224, %r9d
	cmovne	%ecx, %r9d
	movl	%r9d, %r15d
.L648:
	rolw	$8, %dx
	leaq	-58(%rbp), %r8
	movl	%r13d, %esi
	movq	%r12, %rdi
	movw	%dx, -58(%rbp)
	movl	%r15d, %edx
	call	ssl_security@PLT
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L679:
	cmpl	$116, 20(%rbx)
	jne	.L638
	.p2align 4,,10
	.p2align 3
.L636:
	xorl	%eax, %eax
.L630:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L681
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L633:
	.cfi_restore_state
	movl	16(%rbx), %edi
	call	ssl_md@PLT
	testq	%rax, %rax
	jne	.L637
	jmp	.L636
	.p2align 4,,10
	.p2align 3
.L642:
	cmpl	$65536, (%rax)
	jne	.L643
	movq	168(%r12), %rax
	cmpl	$771, 832(%rax)
	jle	.L643
	cmpl	$771, 828(%rax)
	jg	.L636
	movq	%r12, %rdi
	call	SSL_get_ciphers@PLT
	movq	%rax, -80(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L636
	call	OPENSSL_sk_num@PLT
	movl	%eax, -84(%rbp)
	testl	%eax, %eax
	jg	.L647
	jmp	.L645
	.p2align 4,,10
	.p2align 3
.L646:
	addl	$1, %r14d
	cmpl	-84(%rbp), %r14d
	je	.L636
.L647:
	movq	-80(%rbp), %rdi
	movl	%r14d, %esi
	call	OPENSSL_sk_value@PLT
	xorl	%ecx, %ecx
	movl	$65537, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -72(%rbp)
	call	ssl_cipher_disabled
	testl	%eax, %eax
	jne	.L646
	movq	-72(%rbp), %rsi
	testb	$16, 28(%rsi)
	je	.L646
	jmp	.L643
	.p2align 4,,10
	.p2align 3
.L680:
	movl	16(%rbx), %edi
	call	ssl_md@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L678
	call	EVP_MD_size@PLT
	leal	0(,%rax,4), %r15d
.L678:
	movzwl	8(%rbx), %edx
	movl	12(%rbx), %ecx
	jmp	.L648
.L645:
	jne	.L643
	jmp	.L636
.L681:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1613:
	.size	tls12_sigalg_allowed, .-tls12_sigalg_allowed
	.p2align 4
	.type	tls1_get_legacy_sigalg, @function
tls1_get_legacy_sigalg:
.LFB1599:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	cmpl	$-1, %esi
	je	.L713
.L683:
	cmpl	$8, %esi
	ja	.L689
.L697:
	movq	8(%r13), %rax
	movq	192(%rax), %rax
	movl	96(%rax), %eax
	andl	$2, %eax
	orl	%esi, %eax
	je	.L691
	movslq	%esi, %rsi
	leaq	tls_default_sigalg(%rip), %rax
	movzwl	(%rax,%rsi,2), %eax
.L690:
	leaq	sigalg_lookup_tbl(%rip), %r12
	leaq	920(%r12), %rdx
	jmp	.L693
	.p2align 4,,10
	.p2align 3
.L714:
	addq	$40, %r12
	cmpq	%rdx, %r12
	je	.L689
.L693:
	cmpw	8(%r12), %ax
	jne	.L714
	movl	12(%r12), %eax
	testl	%eax, %eax
	jne	.L715
.L695:
	movq	%r12, %rdx
	movl	$327691, %esi
	movq	%r13, %rdi
	call	tls12_sigalg_allowed
	testl	%eax, %eax
	je	.L689
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L715:
	.cfi_restore_state
	movl	16(%r12), %edi
	call	ssl_md@PLT
	testq	%rax, %rax
	jne	.L695
	.p2align 4,,10
	.p2align 3
.L689:
	xorl	%r12d, %r12d
.L717:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L713:
	.cfi_restore_state
	movl	56(%rdi), %edx
	testl	%edx, %edx
	je	.L684
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L688:
	movq	%r12, %rdi
	call	ssl_cert_lookup_by_idx@PLT
	movq	%rax, %r8
	movq	168(%r13), %rax
	movq	568(%rax), %rax
	movl	32(%rax), %eax
	testl	%eax, 4(%r8)
	jne	.L716
	addq	$1, %r12
	cmpq	$9, %r12
	jne	.L688
	xorl	%r12d, %r12d
	jmp	.L717
	.p2align 4,,10
	.p2align 3
.L684:
	movq	1168(%rdi), %rax
	movq	(%rax), %rsi
	leaq	32(%rax), %rdx
	subq	%rdx, %rsi
	sarq	$3, %rsi
	imull	$-858993459, %esi, %esi
	jmp	.L683
	.p2align 4,,10
	.p2align 3
.L691:
	leaq	legacy_rsa_sigalg(%rip), %rdx
	movl	$327691, %esi
	movq	%r13, %rdi
	call	tls12_sigalg_allowed
	leaq	legacy_rsa_sigalg(%rip), %r12
	testl	%eax, %eax
	je	.L689
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L716:
	.cfi_restore_state
	movl	%r12d, %esi
	cmpq	$4, %r12
	jne	.L697
	cmpl	$32, %eax
	je	.L698
	movq	1168(%r13), %rax
	cmpq	$0, 280(%rax)
	jne	.L699
	cmpq	$0, 240(%rax)
	jne	.L700
	cmpq	$0, 200(%rax)
	je	.L698
	movl	$4, %esi
	jmp	.L697
	.p2align 4,,10
	.p2align 3
.L698:
	movl	$-4627, %eax
	jmp	.L690
.L699:
	movl	$6, %esi
	jmp	.L697
.L700:
	movl	$5, %esi
	jmp	.L697
	.cfi_endproc
.LFE1599:
	.size	tls1_get_legacy_sigalg, .-tls1_get_legacy_sigalg
	.p2align 4
	.globl	tls1_set_peer_legacy_sigalg
	.type	tls1_set_peer_legacy_sigalg, @function
tls1_set_peer_legacy_sigalg:
.LFB1600:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	leaq	-32(%rbp), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	ssl_cert_lookup_by_pkey@PLT
	testq	%rax, %rax
	je	.L721
	movl	-32(%rbp), %esi
	movq	%rbx, %rdi
	call	tls1_get_legacy_sigalg
	testq	%rax, %rax
	je	.L721
	movq	168(%rbx), %rdx
	movq	%rax, 776(%rdx)
	movl	$1, %eax
.L718:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L727
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L721:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L718
.L727:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1600:
	.size	tls1_set_peer_legacy_sigalg, .-tls1_set_peer_legacy_sigalg
	.p2align 4
	.globl	tls_use_ticket
	.type	tls_use_ticket, @function
tls_use_ticket:
.LFB1609:
	.cfi_startproc
	endbr64
	testb	$64, 1493(%rdi)
	je	.L730
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L730:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$10, %esi
	jmp	ssl_security@PLT
	.cfi_endproc
.LFE1609:
	.size	tls_use_ticket, .-tls_use_ticket
	.p2align 4
	.globl	tls_decrypt_ticket
	.type	tls_decrypt_ticket, @function
tls_decrypt_ticket:
.LFB1612:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	%rsi, -160(%rbp)
	movq	%rcx, -168(%rbp)
	movq	%r9, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L763
	movq	8(%rdi), %rax
	movq	%r8, %r10
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L733
	movl	(%rax), %eax
	cmpl	$65536, %eax
	je	.L733
	cmpl	$771, %eax
	jg	.L734
.L733:
	cmpq	$0, 1760(%rbx)
	je	.L734
.L765:
	xorl	%r11d, %r11d
	xorl	%r13d, %r13d
	movl	$4, %r14d
.L732:
	movq	%r11, %rdi
	call	EVP_CIPHER_CTX_free@PLT
	movq	%r13, %rdi
	call	HMAC_CTX_free@PLT
	movq	1904(%rbx), %rsi
	movq	976(%rsi), %rax
	testq	%rax, %rax
	jne	.L761
	xorl	%r15d, %r15d
.L813:
	cmpq	$0, 1760(%rbx)
	je	.L758
.L759:
	movq	8(%rbx), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L756
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L756
	cmpl	$65536, %eax
	jne	.L814
	.p2align 4,,10
	.p2align 3
.L756:
	movq	-152(%rbp), %rax
	movq	%r15, (%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L815
	addq	$168, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L734:
	.cfi_restore_state
	movq	%r10, -176(%rbp)
	cmpq	$31, %r12
	jbe	.L765
	movq	1904(%rbx), %r14
	call	HMAC_CTX_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L766
	call	EVP_CIPHER_CTX_new@PLT
	movq	%rax, %r11
	testq	%rax, %rax
	je	.L767
	movq	552(%r14), %rax
	movq	-176(%rbp), %r10
	testq	%rax, %rax
	je	.L737
	movq	-160(%rbp), %rsi
	xorl	%r9d, %r9d
	movq	%r11, %rcx
	movq	%r13, %r8
	movq	%r11, -176(%rbp)
	movq	%rbx, %rdi
	movq	%r10, -184(%rbp)
	leaq	16(%rsi), %rdx
	call	*%rax
	movq	-176(%rbp), %r11
	testl	%eax, %eax
	js	.L776
	je	.L769
	cmpl	$2, %eax
	movq	-184(%rbp), %r10
	sete	%al
	movzbl	%al, %eax
	movl	%eax, -188(%rbp)
.L738:
	movq	%r13, %rdi
	movq	%r11, -176(%rbp)
	movl	$1, %r14d
	movq	%r10, -184(%rbp)
	call	HMAC_size@PLT
	movq	-176(%rbp), %r11
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L736
	movq	%r11, %rdi
	movl	$4, %r14d
	call	EVP_CIPHER_CTX_iv_length@PLT
	movq	-176(%rbp), %r11
	addl	$16, %eax
	cltq
	addq	%r15, %rax
	cmpq	%r12, %rax
	jnb	.L732
	subq	%r15, %r12
	movq	-160(%rbp), %rsi
	movq	%r13, %rdi
	movq	%r12, %rdx
	call	HMAC_Update@PLT
	movq	-176(%rbp), %r11
	testl	%eax, %eax
	jle	.L776
	leaq	-128(%rbp), %r8
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%r8, %rsi
	movq	%r8, -200(%rbp)
	call	HMAC_Final@PLT
	movq	-176(%rbp), %r11
	testl	%eax, %eax
	jle	.L776
	movq	-160(%rbp), %rax
	movq	-200(%rbp), %r8
	movq	%r15, %rdx
	leaq	(%rax,%r12), %rsi
	movq	%r8, %rdi
	call	CRYPTO_memcmp@PLT
	movq	-176(%rbp), %r11
	testl	%eax, %eax
	jne	.L732
	movq	%r11, %rdi
	call	EVP_CIPHER_CTX_iv_length@PLT
	movq	-176(%rbp), %r11
	movq	-160(%rbp), %rcx
	cltq
	movq	%r11, %rdi
	leaq	16(%rcx,%rax), %rax
	movq	%rax, -136(%rbp)
	call	EVP_CIPHER_CTX_iv_length@PLT
	movl	$1473, %edx
	leaq	.LC5(%rip), %rsi
	addl	$16, %eax
	cltq
	subq	%rax, %r12
	movq	%r12, %rdi
	call	CRYPTO_malloc@PLT
	movq	-176(%rbp), %r11
	movq	-184(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %r9
	je	.L742
	movq	-136(%rbp), %rcx
	movq	%r11, %rdi
	movl	%r12d, %r8d
	movq	%rax, %rsi
	leaq	-144(%rbp), %rdx
	movq	%r11, -176(%rbp)
	movq	%r10, -200(%rbp)
	movq	%rax, -184(%rbp)
	call	EVP_DecryptUpdate@PLT
	movq	-176(%rbp), %r11
	movq	-184(%rbp), %r9
	testl	%eax, %eax
	jle	.L742
	movslq	-144(%rbp), %rsi
	movq	%r11, %rdi
	leaq	-140(%rbp), %rdx
	addq	%r9, %rsi
	call	EVP_DecryptFinal@PLT
	movq	-176(%rbp), %r11
	movq	-184(%rbp), %r9
	testl	%eax, %eax
	movq	-200(%rbp), %r10
	jle	.L816
	movl	-140(%rbp), %eax
	addl	-144(%rbp), %eax
	xorl	%edi, %edi
	leaq	-136(%rbp), %rsi
	movslq	%eax, %rdx
	movq	%r11, -184(%rbp)
	movq	%r10, -200(%rbp)
	movq	%r9, -136(%rbp)
	movq	%r9, -176(%rbp)
	movl	%eax, -144(%rbp)
	call	d2i_SSL_SESSION@PLT
	movq	-176(%rbp), %r9
	movl	$1490, %edx
	leaq	.LC5(%rip), %rsi
	movq	%rax, %r15
	movq	-136(%rbp), %rax
	movq	%r9, %rdi
	subq	%r9, %rax
	subl	%eax, -144(%rbp)
	call	CRYPTO_free@PLT
	testq	%r15, %r15
	movq	-184(%rbp), %r11
	je	.L744
	cmpl	$0, -144(%rbp)
	movq	-200(%rbp), %r10
	jne	.L817
	testq	%r10, %r10
	jne	.L818
.L746:
	movq	%r11, %rdi
	movl	-188(%rbp), %r14d
	call	EVP_CIPHER_CTX_free@PLT
	movq	%r13, %rdi
	call	HMAC_CTX_free@PLT
	movq	1904(%rbx), %rdx
	addl	$5, %r14d
	movq	976(%rdx), %rax
	testq	%rax, %rax
	je	.L819
	cmpq	$16, %r12
	movl	$16, %ecx
	movl	%r14d, %r8d
	movq	%r15, %rsi
	cmovbe	%r12, %rcx
	movq	984(%rdx), %r9
	movq	%rbx, %rdi
	movq	-160(%rbp), %rdx
	call	*%rax
	cmpl	$2, %eax
	je	.L778
	jle	.L810
	leal	-3(%rax), %edx
	cmpl	$1, %edx
	ja	.L752
	movl	$6, %r14d
	cmpl	$3, %eax
	jne	.L813
	movl	$5, %r14d
	.p2align 4,,10
	.p2align 3
.L811:
	cmpq	$0, 1760(%rbx)
	jne	.L759
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L761:
	cmpq	$16, %r12
	movl	$16, %ecx
	movl	%r14d, %r8d
	movq	%rbx, %rdi
	cmovbe	%r12, %rcx
	movq	984(%rsi), %r9
	xorl	%esi, %esi
	movq	-160(%rbp), %rdx
	call	*%rax
	cmpl	$2, %eax
	je	.L783
	jg	.L760
	xorl	%r15d, %r15d
.L810:
	cmpl	$1, %eax
	jne	.L752
	movq	%r15, %rdi
	movl	$2, %r14d
	xorl	%r15d, %r15d
	call	SSL_SESSION_free@PLT
	jmp	.L811
	.p2align 4,,10
	.p2align 3
.L760:
	subl	$3, %eax
	cmpl	$1, %eax
	ja	.L785
	xorl	%r15d, %r15d
	cmpq	$0, 1760(%rbx)
	movl	$1, %r14d
	jne	.L759
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L763:
	xorl	%r11d, %r11d
	xorl	%r13d, %r13d
	movl	$3, %r14d
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L820:
	cmpl	$2, %r14d
	jle	.L756
	.p2align 4,,10
	.p2align 3
.L758:
	movl	$1, 1664(%rbx)
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L785:
	xorl	%r15d, %r15d
.L752:
	movl	$1, %r14d
	jmp	.L811
	.p2align 4,,10
	.p2align 3
.L814:
	cmpl	$4, %r14d
	jle	.L820
.L757:
	cmpl	$6, %r14d
	je	.L758
	movl	$5, %r14d
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L783:
	xorl	%r15d, %r15d
.L749:
	movq	%r15, %rdi
	xorl	%r15d, %r15d
	call	SSL_SESSION_free@PLT
	jmp	.L813
	.p2align 4,,10
	.p2align 3
.L766:
	xorl	%r11d, %r11d
	xorl	%r14d, %r14d
.L736:
	movq	%r11, %rdi
	xorl	%r15d, %r15d
	call	EVP_CIPHER_CTX_free@PLT
	movq	%r13, %rdi
	call	HMAC_CTX_free@PLT
	cmpq	$0, 1760(%rbx)
	jne	.L759
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L769:
	movl	$4, %r14d
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L778:
	movl	$4, %r14d
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L776:
	movl	$1, %r14d
	jmp	.L736
	.p2align 4,,10
	.p2align 3
.L737:
	movq	-160(%rbp), %rdi
	movq	%r10, -176(%rbp)
	movq	(%rdi), %rax
	movq	8(%rdi), %rdx
	xorq	528(%r14), %rax
	xorq	536(%r14), %rdx
	orq	%rax, %rdx
	jne	.L769
	movq	%r11, -184(%rbp)
	movl	$0, -188(%rbp)
	call	EVP_sha256@PLT
	xorl	%r8d, %r8d
	movl	$32, %edx
	movq	%r13, %rdi
	movq	544(%r14), %rsi
	movq	%rax, %rcx
	call	HMAC_Init_ex@PLT
	movq	-184(%rbp), %r11
	movq	-176(%rbp), %r10
	testl	%eax, %eax
	jle	.L776
	movq	%r10, -184(%rbp)
	movq	544(%r14), %r14
	movq	%r11, -176(%rbp)
	call	EVP_aes_256_cbc@PLT
	movq	-176(%rbp), %r11
	addq	$32, %r14
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	-160(%rbp), %rax
	movq	%r14, %rcx
	movq	%r11, %rdi
	leaq	16(%rax), %r8
	call	EVP_DecryptInit_ex@PLT
	movq	-176(%rbp), %r11
	testl	%eax, %eax
	jle	.L776
	movq	8(%rbx), %rax
	movq	-184(%rbp), %r10
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L738
	movl	(%rax), %eax
	cmpl	$65536, %eax
	setne	%dl
	cmpl	$771, %eax
	setg	%al
	movzbl	%al, %eax
	andl	%edx, %eax
	movl	%eax, -188(%rbp)
	jmp	.L738
	.p2align 4,,10
	.p2align 3
.L767:
	xorl	%r14d, %r14d
	jmp	.L736
.L742:
	movl	$1476, %edx
	movq	%r9, %rdi
	leaq	.LC5(%rip), %rsi
	movq	%r11, -160(%rbp)
	call	CRYPTO_free@PLT
	movl	$1, %r14d
	movq	-160(%rbp), %r11
	jmp	.L736
.L815:
	call	__stack_chk_fail@PLT
.L816:
	movl	$1481, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r9, %rdi
	movq	%r11, -168(%rbp)
	call	CRYPTO_free@PLT
	movq	-168(%rbp), %r11
	jmp	.L732
.L818:
	movq	-168(%rbp), %rsi
	movq	%r10, %rdx
	leaq	344(%r15), %rdi
	movq	%r11, -184(%rbp)
	movq	%r10, -176(%rbp)
	call	memcpy@PLT
	movq	-176(%rbp), %r10
	movq	-184(%rbp), %r11
	movq	%r10, 336(%r15)
	jmp	.L746
.L817:
	movq	%r15, %rdi
	movq	%r11, -168(%rbp)
	movl	$4, %r14d
	call	SSL_SESSION_free@PLT
	movq	-168(%rbp), %r11
	jmp	.L732
.L744:
	movq	%r11, -168(%rbp)
	call	ERR_clear_error@PLT
	movq	-168(%rbp), %r11
	jmp	.L732
.L819:
	cmpq	$0, 1760(%rbx)
	jne	.L759
	jmp	.L757
	.cfi_endproc
.LFE1612:
	.size	tls_decrypt_ticket, .-tls_decrypt_ticket
	.p2align 4
	.globl	tls_get_ticket_from_client
	.type	tls_get_ticket_from_client, @function
tls_get_ticket_from_client:
.LFB1611:
	.cfi_startproc
	endbr64
	cmpl	$768, (%rdi)
	movq	$0, (%rdx)
	movl	$0, 1664(%rdi)
	jle	.L829
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	testb	$64, 1493(%rdi)
	je	.L832
.L823:
	addq	$8, %rsp
	movl	$2, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L832:
	.cfi_restore_state
	movq	%rsi, %r13
	movq	%rdx, %rbx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$10, %esi
	call	ssl_security@PLT
	testl	%eax, %eax
	je	.L823
	movq	648(%r13), %rax
	movl	256(%rax), %edx
	testl	%edx, %edx
	je	.L823
	movq	40(%r13), %r8
	leaq	48(%r13), %rcx
	movq	%rbx, %r9
	movq	%r12, %rdi
	movq	248(%rax), %rdx
	movq	240(%rax), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	tls_decrypt_ticket
	.p2align 4,,10
	.p2align 3
.L829:
	movl	$2, %eax
	ret
	.cfi_endproc
.LFE1611:
	.size	tls_get_ticket_from_client, .-tls_get_ticket_from_client
	.p2align 4
	.globl	ssl_set_sig_mask
	.type	ssl_set_sig_mask, @function
ssl_set_sig_mask:
.LFB1614:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -68(%rbp)
	movq	1168(%rsi), %rdx
	movq	%rdi, -88(%rbp)
	movl	28(%rdx), %eax
	movq	%rsi, -64(%rbp)
	andl	$196608, %eax
	cmpl	$131072, %eax
	je	.L843
	cmpl	$196608, %eax
	je	.L844
	cmpl	$65536, %eax
	je	.L845
	cmpl	$1, 56(%rsi)
	je	.L866
.L835:
	movq	408(%rdx), %rbx
	testq	%rbx, %rbx
	je	.L846
	movq	416(%rdx), %rax
	movq	%rax, -56(%rbp)
.L836:
	cmpq	$0, -56(%rbp)
	movl	$11, %r13d
	jne	.L834
.L837:
	movq	-88(%rbp), %rax
	orl	%r13d, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L845:
	.cfi_restore_state
	movq	$1, -56(%rbp)
	leaq	suiteb_sigalgs(%rip), %rbx
.L834:
	movl	$11, %r13d
	xorl	%r14d, %r14d
	leaq	920+sigalg_lookup_tbl(%rip), %r15
	.p2align 4,,10
	.p2align 3
.L841:
	movzwl	(%rbx,%r14,2), %eax
	leaq	sigalg_lookup_tbl(%rip), %r12
	jmp	.L839
	.p2align 4,,10
	.p2align 3
.L867:
	addq	$40, %r12
	cmpq	%r15, %r12
	je	.L840
.L839:
	cmpw	8(%r12), %ax
	jne	.L867
	movslq	24(%r12), %rdi
	call	ssl_cert_lookup_by_idx@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L840
	testl	%r13d, 4(%r9)
	jne	.L868
.L840:
	addq	$1, %r14
	cmpq	%r14, -56(%rbp)
	ja	.L841
	jmp	.L837
	.p2align 4,,10
	.p2align 3
.L868:
	movl	-68(%rbp), %esi
	movq	-64(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r9, -80(%rbp)
	call	tls12_sigalg_allowed
	testl	%eax, %eax
	je	.L840
	movq	-80(%rbp), %r9
	movl	4(%r9), %eax
	notl	%eax
	andl	%eax, %r13d
	jmp	.L840
.L844:
	movq	$2, -56(%rbp)
	leaq	suiteb_sigalgs(%rip), %rbx
	jmp	.L834
.L843:
	movq	$1, -56(%rbp)
	leaq	2+suiteb_sigalgs(%rip), %rbx
	jmp	.L834
.L866:
	movq	424(%rdx), %rbx
	testq	%rbx, %rbx
	je	.L835
	movq	432(%rdx), %rax
	movq	%rax, -56(%rbp)
	jmp	.L836
.L846:
	movq	$23, -56(%rbp)
	leaq	tls12_sigalgs(%rip), %rbx
	jmp	.L834
	.cfi_endproc
.LFE1614:
	.size	ssl_set_sig_mask, .-ssl_set_sig_mask
	.p2align 4
	.globl	ssl_set_client_disabled
	.type	ssl_set_client_disabled, @function
ssl_set_client_disabled:
.LFB1607:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$327694, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rbx, %rsi
	subq	$8, %rsp
	movq	168(%rdi), %rdi
	movq	$0, 820(%rdi)
	addq	$824, %rdi
	call	ssl_set_sig_mask
	movq	168(%rbx), %rsi
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	leaq	832(%rsi), %rdx
	addq	$828, %rsi
	call	ssl_get_min_max_version@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jne	.L869
	cmpq	$0, 1408(%rbx)
	je	.L875
	movl	$1, %eax
	testb	$32, 2096(%rbx)
	jne	.L869
.L876:
	movq	168(%rbx), %rdx
	movabsq	$274877906976, %rcx
	orq	%rcx, 820(%rdx)
.L869:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L875:
	.cfi_restore_state
	movq	168(%rbx), %rax
	movabsq	$68719477192, %rdx
	orq	%rdx, 820(%rax)
	movl	$1, %eax
	testb	$32, 2096(%rbx)
	jne	.L869
	jmp	.L876
	.cfi_endproc
.LFE1607:
	.size	ssl_set_client_disabled, .-ssl_set_client_disabled
	.p2align 4
	.globl	tls12_copy_sigalgs
	.type	tls12_copy_sigalgs, @function
tls12_copy_sigalgs:
.LFB1615:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -72(%rbp)
	movq	%rcx, -56(%rbp)
	testq	%rcx, %rcx
	je	.L878
	movl	$0, -60(%rbp)
	movq	%rdi, %r12
	movq	%rdx, %rbx
	xorl	%r13d, %r13d
	leaq	920+sigalg_lookup_tbl(%rip), %r15
	.p2align 4,,10
	.p2align 3
.L883:
	movzwl	(%rbx,%r13,2), %eax
	leaq	sigalg_lookup_tbl(%rip), %r14
	jmp	.L880
	.p2align 4,,10
	.p2align 3
.L902:
	addq	$40, %r14
	cmpq	%r15, %r14
	je	.L901
.L880:
	cmpw	8(%r14), %ax
	jne	.L902
.L879:
	movq	%r14, %rdx
	movl	$327691, %esi
	movq	%r12, %rdi
	call	tls12_sigalg_allowed
	testl	%eax, %eax
	je	.L881
	movzwl	(%rbx,%r13,2), %esi
	movq	-72(%rbp), %rdi
	movl	$2, %edx
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L885
	movl	-60(%rbp), %edx
	testl	%edx, %edx
	jne	.L881
	movq	8(%r12), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L887
	movl	(%rax), %eax
	cmpl	$65536, %eax
	je	.L887
	cmpl	$771, %eax
	jg	.L903
.L887:
	movl	$1, -60(%rbp)
.L881:
	addq	$1, %r13
	cmpq	%r13, -56(%rbp)
	jne	.L883
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	je	.L878
.L877:
	movl	-60(%rbp), %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L901:
	.cfi_restore_state
	xorl	%r14d, %r14d
	jmp	.L879
.L903:
	cmpl	$6, 20(%r14)
	je	.L881
	movl	12(%r14), %eax
	cmpl	$64, %eax
	setne	%dl
	xorl	%r14d, %r14d
	cmpl	$675, %eax
	setne	%r14b
	andl	%edx, %r14d
	movl	%r14d, -60(%rbp)
	jmp	.L881
.L878:
	movl	$1720, %r8d
	movl	$118, %edx
	movl	$533, %esi
	movl	$20, %edi
	leaq	.LC5(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$0, -60(%rbp)
	jmp	.L877
.L885:
	movl	$0, -60(%rbp)
	jmp	.L877
	.cfi_endproc
.LFE1615:
	.size	tls12_copy_sigalgs, .-tls12_copy_sigalgs
	.p2align 4
	.globl	tls1_save_u16
	.type	tls1_save_u16, @function
tls1_save_u16:
.LFB1618:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r9d, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L904
	movq	%rdi, %r12
	andl	$1, %r12d
	je	.L918
.L904:
	addq	$24, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L918:
	.cfi_restore_state
	movq	%rdi, %r15
	movq	%rsi, %r13
	movq	%rdx, %r14
	andq	$-2, %rdi
	movl	$1811, %edx
	leaq	.LC5(%rip), %rsi
	movl	%r9d, -56(%rbp)
	shrq	%r15
	call	CRYPTO_malloc@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L919
	testq	%r15, %r15
	je	.L907
	movq	8(%rbx), %rax
	movq	%rax, %rsi
	subq	$2, %rax
	shrq	%rsi
.L909:
	cmpq	%r12, %rsi
	je	.L920
	movq	(%rbx), %rdx
	movzwl	(%rdx), %ecx
	addq	$2, %rdx
	movq	%rdx, (%rbx)
	movq	%r12, %rdx
	negq	%rdx
	rolw	$8, %cx
	leaq	(%rax,%rdx,2), %rdx
	movq	%rdx, 8(%rbx)
	movw	%cx, (%r8,%r12,2)
	addq	$1, %r12
	cmpq	%r12, %r15
	jne	.L909
.L907:
	movq	0(%r13), %rdi
	movl	$1823, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r8, -56(%rbp)
	call	CRYPTO_free@PLT
	movq	-56(%rbp), %r8
	movl	$1, %r9d
	movq	%r8, 0(%r13)
	movq	%r15, (%r14)
	jmp	.L904
	.p2align 4,,10
	.p2align 3
.L920:
	movl	$1819, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r8, %rdi
	call	CRYPTO_free@PLT
	xorl	%r9d, %r9d
	jmp	.L904
	.p2align 4,,10
	.p2align 3
.L919:
	movl	$1812, %r8d
	movl	$65, %edx
	movl	$628, %esi
	movl	$20, %edi
	leaq	.LC5(%rip), %rcx
	call	ERR_put_error@PLT
	movl	-56(%rbp), %r9d
	jmp	.L904
	.cfi_endproc
.LFE1618:
	.size	tls1_save_u16, .-tls1_save_u16
	.p2align 4
	.globl	tls1_save_sigalgs
	.type	tls1_save_sigalgs, @function
tls1_save_sigalgs:
.LFB1619:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	192(%rax), %rcx
	movl	$1, %eax
	testb	$2, 96(%rcx)
	je	.L953
	cmpq	$0, 1168(%rdi)
	je	.L933
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	168(%rdi), %r13
	movq	8(%rsi), %rdi
	testl	%edx, %edx
	je	.L923
	testq	%rdi, %rdi
	je	.L921
	movq	%rdi, %r12
	andl	$1, %r12d
	jne	.L921
	movq	%rdi, %r15
	movl	$1811, %edx
	andq	$-2, %rdi
	leaq	.LC5(%rip), %rsi
	shrq	%r15
	call	CRYPTO_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L928
	testq	%r15, %r15
	je	.L925
	movq	8(%rbx), %rax
	movq	%rax, %rsi
	subq	$2, %rax
	shrq	%rsi
.L927:
	cmpq	%r12, %rsi
	je	.L956
	movq	(%rbx), %rdx
	movzwl	(%rdx), %ecx
	addq	$2, %rdx
	movq	%rdx, (%rbx)
	movq	%r12, %rdx
	negq	%rdx
	rolw	$8, %cx
	leaq	(%rax,%rdx,2), %rdx
	movq	%rdx, 8(%rbx)
	movw	%cx, (%r14,%r12,2)
	addq	$1, %r12
	cmpq	%r12, %r15
	jne	.L927
.L925:
	movq	752(%r13), %rdi
	movl	$1823, %edx
	leaq	.LC5(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r14, 752(%r13)
	movl	$1, %eax
	movq	%r15, 768(%r13)
.L921:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L923:
	.cfi_restore_state
	testq	%rdi, %rdi
	je	.L921
	movq	%rdi, %r12
	movl	%edx, %eax
	andl	$1, %r12d
	jne	.L921
	movq	%rdi, %r15
	movl	$1811, %edx
	andq	$-2, %rdi
	leaq	.LC5(%rip), %rsi
	shrq	%r15
	call	CRYPTO_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L928
	testq	%r15, %r15
	je	.L929
	movq	8(%rbx), %rax
	movq	%rax, %rsi
	subq	$2, %rax
	shrq	%rsi
.L931:
	cmpq	%r12, %rsi
	je	.L956
	movq	(%rbx), %rdx
	movzwl	(%rdx), %ecx
	addq	$2, %rdx
	movq	%rdx, (%rbx)
	movq	%r12, %rdx
	negq	%rdx
	rolw	$8, %cx
	leaq	(%rax,%rdx,2), %rdx
	movq	%rdx, 8(%rbx)
	movw	%cx, (%r14,%r12,2)
	addq	$1, %r12
	cmpq	%r12, %r15
	jne	.L931
.L929:
	movq	744(%r13), %rdi
	movl	$1823, %edx
	leaq	.LC5(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r14, 744(%r13)
	movl	$1, %eax
	movq	%r15, 760(%r13)
	jmp	.L921
	.p2align 4,,10
	.p2align 3
.L953:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L956:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%r14, %rdi
	movl	$1819, %edx
	leaq	.LC5(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L933:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L928:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$1812, %r8d
	movl	$65, %edx
	movl	$628, %esi
	movl	$20, %edi
	leaq	.LC5(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L921
	.cfi_endproc
.LFE1619:
	.size	tls1_save_sigalgs, .-tls1_save_sigalgs
	.p2align 4
	.globl	tls1_process_sigalgs
	.type	tls1_process_sigalgs, @function
tls1_process_sigalgs:
.LFB1620:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1759, %edx
	leaq	.LC5(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	168(%rdi), %rax
	movq	1168(%rdi), %r13
	movq	6264(%rdi), %rdi
	movq	%rax, -80(%rbp)
	addq	$784, %rax
	movl	28(%r13), %ebx
	movq	%rax, -72(%rbp)
	call	CRYPTO_free@PLT
	movl	56(%r15), %eax
	andl	$196608, %ebx
	movq	$0, 6264(%r15)
	movq	$0, 6272(%r15)
	testl	%eax, %eax
	jne	.L958
	movq	424(%r13), %r12
	testq	%r12, %r12
	je	.L958
	testl	%ebx, %ebx
	je	.L1049
	movq	1168(%r15), %rsi
	movl	28(%rsi), %eax
	andl	$196608, %eax
	cmpl	$131072, %eax
	je	.L996
	cmpl	$196608, %eax
	je	.L997
	leaq	suiteb_sigalgs(%rip), %r12
	movl	$1, %r13d
	cmpl	$65536, %eax
	je	.L985
.L987:
	movq	424(%rsi), %r12
	testq	%r12, %r12
	je	.L963
	movq	432(%rsi), %r13
	.p2align 4,,10
	.p2align 3
.L962:
	movl	1492(%r15), %eax
	andl	$4194304, %eax
	orl	%ebx, %eax
	je	.L964
	.p2align 4,,10
	.p2align 3
.L985:
	movq	168(%r15), %rax
	movq	744(%rax), %rcx
	movq	760(%rax), %rbx
	movq	%rcx, -56(%rbp)
	jmp	.L965
	.p2align 4,,10
	.p2align 3
.L958:
	movq	408(%r13), %r12
	testl	%ebx, %ebx
	jne	.L961
	testq	%r12, %r12
	je	.L961
	movq	416(%r13), %r13
.L960:
	testb	$64, 1494(%r15)
	jne	.L985
.L964:
	movq	168(%r15), %rax
	movq	%r12, -56(%rbp)
	movq	%r13, %rbx
	movq	744(%rax), %r12
	movq	760(%rax), %r13
.L965:
	xorl	%r14d, %r14d
	movq	$0, -64(%rbp)
	movq	%r14, %rax
	movq	%r13, %r14
	testq	%r13, %r13
	je	.L973
	.p2align 4,,10
	.p2align 3
.L1047:
	movq	%rax, %r13
	leaq	sigalg_lookup_tbl(%rip), %rdx
	movzwl	(%r12,%rax,2), %eax
	jmp	.L969
	.p2align 4,,10
	.p2align 3
.L1051:
	addq	$40, %rdx
	leaq	920+sigalg_lookup_tbl(%rip), %rcx
	cmpq	%rcx, %rdx
	je	.L1050
.L969:
	cmpw	8(%rdx), %ax
	jne	.L1051
.L968:
	movl	$327692, %esi
	movq	%r15, %rdi
	call	tls12_sigalg_allowed
	testl	%eax, %eax
	jne	.L1052
.L970:
	leaq	1(%r13), %rax
	cmpq	%rax, %r14
	jne	.L1047
	cmpq	$0, -64(%rbp)
	movq	%r13, %r14
	jne	.L1053
.L973:
	movq	-80(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 6264(%r15)
	movq	$0, 6272(%r15)
	movups	%xmm0, 784(%rax)
	movq	-72(%rbp), %rax
	movl	$0, 32(%rax)
	movups	%xmm0, 16(%rax)
.L967:
	movl	$1, %eax
.L957:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L961:
	.cfi_restore_state
	movq	1168(%r15), %rsi
	movl	28(%rsi), %edx
	andl	$196608, %edx
	cmpl	$131072, %edx
	je	.L988
	leaq	suiteb_sigalgs(%rip), %r12
	movl	$2, %r13d
	cmpl	$196608, %edx
	je	.L962
	movl	$1, %r13d
	cmpl	$65536, %edx
	je	.L962
	testl	%eax, %eax
	je	.L987
.L963:
	movq	408(%rsi), %r12
	testq	%r12, %r12
	je	.L991
	movq	416(%rsi), %r13
	jmp	.L962
	.p2align 4,,10
	.p2align 3
.L1050:
	xorl	%edx, %edx
	jmp	.L968
	.p2align 4,,10
	.p2align 3
.L1052:
	testq	%rbx, %rbx
	je	.L970
	movzwl	(%r12,%r13,2), %edx
	xorl	%eax, %eax
	jmp	.L972
	.p2align 4,,10
	.p2align 3
.L971:
	addq	$1, %rax
	cmpq	%rax, %rbx
	je	.L970
.L972:
	movq	-56(%rbp), %rcx
	cmpw	(%rcx,%rax,2), %dx
	jne	.L971
	addq	$1, -64(%rbp)
	jmp	.L970
	.p2align 4,,10
	.p2align 3
.L1053:
	movq	-64(%rbp), %rdi
	movl	$1784, %edx
	leaq	.LC5(%rip), %rsi
	salq	$3, %rdi
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L1054
	xorl	%ecx, %ecx
	movq	%rax, -88(%rbp)
	xorl	%r8d, %r8d
	movq	-56(%rbp), %r13
	movq	%r12, -64(%rbp)
	leaq	920+sigalg_lookup_tbl(%rip), %r9
	movq	%rax, -104(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%r14, -56(%rbp)
	movq	%r8, %r14
	.p2align 4,,10
	.p2align 3
.L974:
	movq	-64(%rbp), %rax
	leaq	sigalg_lookup_tbl(%rip), %r12
	movzwl	(%rax,%r14,2), %eax
	jmp	.L977
	.p2align 4,,10
	.p2align 3
.L1056:
	addq	$40, %r12
	cmpq	%r9, %r12
	je	.L1055
.L977:
	cmpw	8(%r12), %ax
	jne	.L1056
.L976:
	movq	%r12, %rdx
	movl	$327692, %esi
	movq	%r15, %rdi
	call	tls12_sigalg_allowed
	leaq	920+sigalg_lookup_tbl(%rip), %r9
	testl	%eax, %eax
	jne	.L1057
.L978:
	leaq	1(%r14), %rax
	cmpq	-56(%rbp), %r14
	je	.L1058
.L995:
	movq	%rax, %r14
	jmp	.L974
	.p2align 4,,10
	.p2align 3
.L1055:
	xorl	%r12d, %r12d
	jmp	.L976
	.p2align 4,,10
	.p2align 3
.L1057:
	testq	%rbx, %rbx
	je	.L978
	movq	-64(%rbp), %rax
	movzwl	(%rax,%r14,2), %esi
	xorl	%eax, %eax
	jmp	.L980
	.p2align 4,,10
	.p2align 3
.L979:
	addq	$1, %rax
	cmpq	%rax, %rbx
	je	.L978
.L980:
	cmpw	0(%r13,%rax,2), %si
	jne	.L979
	movq	-88(%rbp), %rax
	addq	$1, -96(%rbp)
	movq	%r12, (%rax)
	addq	$8, %rax
	movq	%rax, -88(%rbp)
	leaq	1(%r14), %rax
	cmpq	-56(%rbp), %r14
	jne	.L995
.L1058:
	movq	-72(%rbp), %r14
	movq	-104(%rbp), %r13
	pxor	%xmm0, %xmm0
	movq	-96(%rbp), %rcx
	movq	-80(%rbp), %rax
	movq	%r13, 6264(%r15)
	movq	%rcx, 6272(%r15)
	movups	%xmm0, 784(%rax)
	movl	$0, 32(%r14)
	movups	%xmm0, 16(%r14)
	testq	%rcx, %rcx
	je	.L967
	xorl	%ebx, %ebx
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L982:
	addq	$1, %rbx
	cmpq	%rcx, %rbx
	jnb	.L967
	movq	6264(%r15), %r13
.L984:
	movq	8(%r15), %rax
	movq	0(%r13,%rbx,8), %rdx
	movq	192(%rax), %rsi
	movslq	24(%rdx), %rdi
	testb	$8, 96(%rsi)
	jne	.L981
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L981
	cmpl	$65536, %eax
	je	.L981
	cmpl	$6, 20(%rdx)
	je	.L982
	.p2align 4,,10
	.p2align 3
.L981:
	leaq	(%r14,%rdi,4), %r12
	movl	(%r12), %eax
	testl	%eax, %eax
	jne	.L982
	call	ssl_cert_is_disabled@PLT
	testl	%eax, %eax
	jne	.L1048
	movl	$258, (%r12)
.L1048:
	movq	6272(%r15), %rcx
	jmp	.L982
	.p2align 4,,10
	.p2align 3
.L1049:
	movq	432(%r13), %r13
	jmp	.L960
	.p2align 4,,10
	.p2align 3
.L988:
	leaq	2+suiteb_sigalgs(%rip), %r12
	movl	$1, %r13d
	jmp	.L962
.L997:
	leaq	suiteb_sigalgs(%rip), %r12
	movl	$2, %r13d
	jmp	.L985
.L1054:
	movl	$1785, %r8d
	movl	$65, %edx
	movl	$631, %esi
	movl	$20, %edi
	leaq	.LC5(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L957
.L996:
	leaq	2+suiteb_sigalgs(%rip), %r12
	movl	$1, %r13d
	jmp	.L985
.L991:
	leaq	tls12_sigalgs(%rip), %r12
	movl	$23, %r13d
	jmp	.L962
	.cfi_endproc
.LFE1620:
	.size	tls1_process_sigalgs, .-tls1_process_sigalgs
	.p2align 4
	.globl	tls1_set_server_sigalgs
	.type	tls1_set_server_sigalgs, @function
tls1_set_server_sigalgs:
.LFB1610:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1253, %edx
	leaq	.LC5(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	6264(%rdi), %rdi
	call	CRYPTO_free@PLT
	movq	168(%r12), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 6264(%r12)
	movq	$0, 6272(%r12)
	cmpq	$0, 752(%rax)
	movups	%xmm0, 784(%rax)
	movl	$0, 816(%rax)
	movups	%xmm0, 800(%rax)
	je	.L1088
.L1060:
	movq	%r12, %rdi
	call	tls1_process_sigalgs
	testl	%eax, %eax
	je	.L1089
	cmpq	$0, 6264(%r12)
	je	.L1090
.L1069:
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1088:
	.cfi_restore_state
	cmpq	$0, 744(%rax)
	jne	.L1060
	movq	1168(%r12), %rdx
	movl	28(%rdx), %eax
	andl	$196608, %eax
	cmpl	$131072, %eax
	je	.L1070
	leaq	suiteb_sigalgs(%rip), %r13
	movl	$2, %ebx
	cmpl	$196608, %eax
	je	.L1061
	movl	$1, %ebx
	cmpl	$65536, %eax
	je	.L1061
	cmpl	$1, 56(%r12)
	je	.L1091
.L1062:
	movq	408(%rdx), %r13
	testq	%r13, %r13
	je	.L1073
	movq	416(%rdx), %rbx
.L1061:
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L1066:
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	tls1_get_legacy_sigalg
	testq	%rax, %rax
	je	.L1063
	testq	%rbx, %rbx
	je	.L1063
	movzwl	8(%rax), %edx
	xorl	%eax, %eax
	jmp	.L1065
	.p2align 4,,10
	.p2align 3
.L1064:
	addq	$1, %rax
	cmpq	%rbx, %rax
	je	.L1063
.L1065:
	cmpw	0(%r13,%rax,2), %dx
	jne	.L1064
	movq	168(%r12), %rax
	movl	$2, 784(%rax,%r14,4)
.L1063:
	addq	$1, %r14
	cmpq	$9, %r14
	jne	.L1066
	jmp	.L1069
	.p2align 4,,10
	.p2align 3
.L1090:
	movq	%r12, %rdi
	movl	$1294, %r9d
	movl	$376, %ecx
	movl	$335, %edx
	leaq	.LC5(%rip), %r8
	movl	$40, %esi
	call	ossl_statem_fatal@PLT
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1089:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$68, %ecx
	movl	$335, %edx
	movl	%eax, -36(%rbp)
	movl	$1286, %r9d
	leaq	.LC5(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movl	-36(%rbp), %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1070:
	.cfi_restore_state
	leaq	2+suiteb_sigalgs(%rip), %r13
	movl	$1, %ebx
	jmp	.L1061
.L1091:
	movq	424(%rdx), %r13
	testq	%r13, %r13
	je	.L1062
	movq	432(%rdx), %rbx
	jmp	.L1061
.L1073:
	leaq	tls12_sigalgs(%rip), %r13
	movl	$23, %ebx
	jmp	.L1061
	.cfi_endproc
.LFE1610:
	.size	tls1_set_server_sigalgs, .-tls1_set_server_sigalgs
	.p2align 4
	.globl	SSL_get_sigalgs
	.type	SSL_get_sigalgs, @function
SSL_get_sigalgs:
.LFB1621:
	.cfi_startproc
	endbr64
	movq	168(%rdi), %rax
	movq	744(%rax), %rdi
	movq	760(%rax), %rax
	testq	%rdi, %rdi
	je	.L1112
	cmpq	$2147483647, %rax
	ja	.L1112
	movl	%eax, %r10d
	testl	%esi, %esi
	js	.L1125
	cmpl	%eax, %esi
	jge	.L1112
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	leaq	(%rdi,%rsi,2), %rax
	movzwl	(%rax), %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpq	$0, 16(%rbp)
	je	.L1096
	movq	16(%rbp), %rdi
	shrw	$8, %si
	movb	%sil, (%rdi)
	movzwl	(%rax), %esi
.L1096:
	testq	%r9, %r9
	je	.L1097
	movb	%sil, (%r9)
	movzwl	(%rax), %esi
.L1097:
	leaq	sigalg_lookup_tbl(%rip), %rax
	leaq	920(%rax), %rdi
	jmp	.L1099
	.p2align 4,,10
	.p2align 3
.L1131:
	addq	$40, %rax
	cmpq	%rdi, %rax
	je	.L1130
.L1099:
	cmpw	%si, 8(%rax)
	jne	.L1131
	testq	%rdx, %rdx
	je	.L1106
	movl	20(%rax), %esi
	movl	%esi, (%rdx)
.L1106:
	testq	%rcx, %rcx
	je	.L1101
	movl	12(%rax), %edx
	movl	%edx, (%rcx)
	testq	%r8, %r8
	je	.L1092
.L1102:
	movl	28(%rax), %eax
.L1104:
	movl	%eax, (%r8)
.L1092:
	movl	%r10d, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1112:
	.cfi_restore 6
	xorl	%r10d, %r10d
.L1125:
	movl	%r10d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1130:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	testq	%rdx, %rdx
	je	.L1128
	movl	$0, (%rdx)
.L1128:
	testq	%rcx, %rcx
	je	.L1108
	movl	$0, (%rcx)
	testq	%r8, %r8
	je	.L1092
	xorl	%eax, %eax
	jmp	.L1104
	.p2align 4,,10
	.p2align 3
.L1101:
	testq	%r8, %r8
	jne	.L1102
	jmp	.L1092
	.p2align 4,,10
	.p2align 3
.L1108:
	testq	%r8, %r8
	je	.L1092
	xorl	%eax, %eax
	jmp	.L1104
	.cfi_endproc
.LFE1621:
	.size	SSL_get_sigalgs, .-SSL_get_sigalgs
	.p2align 4
	.globl	SSL_get_shared_sigalgs
	.type	SSL_get_shared_sigalgs, @function
SSL_get_shared_sigalgs:
.LFB1622:
	.cfi_startproc
	endbr64
	movq	6264(%rdi), %rax
	testq	%rax, %rax
	je	.L1140
	testl	%esi, %esi
	js	.L1140
	movq	6272(%rdi), %r10
	cmpq	$2147483647, %r10
	ja	.L1140
	cmpl	%r10d, %esi
	jge	.L1140
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	(%rax,%rsi,8), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rcx, %rcx
	je	.L1134
	movl	12(%rax), %esi
	movl	%esi, (%rcx)
.L1134:
	testq	%rdx, %rdx
	je	.L1135
	movl	20(%rax), %ecx
	movl	%ecx, (%rdx)
.L1135:
	testq	%r8, %r8
	je	.L1136
	movl	28(%rax), %edx
	movl	%edx, (%r8)
.L1136:
	testq	%r9, %r9
	je	.L1137
	movzwl	8(%rax), %edx
	movb	%dl, (%r9)
.L1137:
	cmpq	$0, 16(%rbp)
	je	.L1138
	movzbl	9(%rax), %eax
	movq	16(%rbp), %rcx
	movb	%al, (%rcx)
.L1138:
	movl	6272(%rdi), %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1140:
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1622:
	.size	SSL_get_shared_sigalgs, .-SSL_get_shared_sigalgs
	.p2align 4
	.globl	tls1_set_sigalgs_list
	.type	tls1_set_sigalgs_list, @function
tls1_set_sigalgs_list:
.LFB1625:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	sig_cb(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-160(%rbp), %r8
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	movl	$1, %edx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movl	$58, %esi
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -160(%rbp)
	call	CONF_parse_list@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L1160
	movl	$1, %r12d
	testq	%rbx, %rbx
	je	.L1160
	movq	-160(%rbp), %r15
	movl	$2041, %edx
	leaq	.LC5(%rip), %rsi
	leaq	(%r15,%r15), %r8
	movq	%r8, %rdi
	movq	%r8, -168(%rbp)
	call	CRYPTO_malloc@PLT
	movq	-168(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L1171
	leaq	-152(%rbp), %rsi
	movq	%r8, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	testl	%r13d, %r13d
	je	.L1163
	movq	424(%rbx), %rdi
	movl	$2048, %edx
	leaq	.LC5(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r14, 424(%rbx)
	movq	%r15, 432(%rbx)
	.p2align 4,,10
	.p2align 3
.L1160:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1172
	addq	$136, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1163:
	.cfi_restore_state
	movq	408(%rbx), %rdi
	movl	$2052, %edx
	leaq	.LC5(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r14, 408(%rbx)
	movq	%r15, 416(%rbx)
	jmp	.L1160
	.p2align 4,,10
	.p2align 3
.L1171:
	movl	$2042, %r8d
	leaq	.LC5(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$630, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L1160
.L1172:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1625:
	.size	tls1_set_sigalgs_list, .-tls1_set_sigalgs_list
	.p2align 4
	.globl	tls1_set_raw_sigalgs
	.type	tls1_set_raw_sigalgs, @function
tls1_set_raw_sigalgs:
.LFB1626:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	leaq	.LC5(%rip), %rsi
	pushq	%r14
	.cfi_offset 14, -32
	leaq	(%rdx,%rdx), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movl	$2041, %edx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	%ecx, -52(%rbp)
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L1178
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	memcpy@PLT
	movl	-52(%rbp), %eax
	testl	%eax, %eax
	jne	.L1179
	movq	408(%r12), %rdi
	movl	$2052, %edx
	leaq	.LC5(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%rbx, 408(%r12)
	movl	$1, %eax
	movq	%r13, 416(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1179:
	.cfi_restore_state
	movq	424(%r12), %rdi
	movl	$2048, %edx
	leaq	.LC5(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%rbx, 424(%r12)
	movl	$1, %eax
	movq	%r13, 432(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1178:
	.cfi_restore_state
	movl	$2042, %r8d
	movl	$65, %edx
	movl	$630, %esi
	movl	$20, %edi
	leaq	.LC5(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1626:
	.size	tls1_set_raw_sigalgs, .-tls1_set_raw_sigalgs
	.p2align 4
	.globl	tls1_set_sigalgs
	.type	tls1_set_sigalgs, @function
tls1_set_sigalgs:
.LFB1627:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r9d, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	andl	$1, %ebx
	je	.L1198
.L1180:
	addq	$24, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1198:
	.cfi_restore_state
	movq	%rdi, %r14
	movq	%rdx, %rdi
	movq	%rsi, %r12
	movq	%rdx, %r13
	andq	$-2, %rdi
	movl	$2067, %edx
	movl	%r9d, -56(%rbp)
	movl	%ecx, %r15d
	leaq	.LC5(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L1199
	leaq	920+sigalg_lookup_tbl(%rip), %rsi
	testq	%r13, %r13
	je	.L1189
	.p2align 4,,10
	.p2align 3
.L1184:
	movl	(%r12,%rbx,4), %edx
	movl	4(%r12,%rbx,4), %edi
	leaq	sigalg_lookup_tbl(%rip), %rax
	jmp	.L1187
	.p2align 4,,10
	.p2align 3
.L1183:
	addq	$40, %rax
	cmpq	%rsi, %rax
	je	.L1200
.L1187:
	cmpl	%edx, 12(%rax)
	jne	.L1183
	cmpl	%edi, 20(%rax)
	jne	.L1183
	movzwl	8(%rax), %eax
	movw	%ax, (%r8,%rbx)
	addq	$2, %rbx
	cmpq	%rbx, %r13
	ja	.L1184
.L1189:
	shrq	%r13
	testl	%r15d, %r15d
	je	.L1201
	movq	424(%r14), %rdi
	movl	$2090, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r8, -56(%rbp)
	call	CRYPTO_free@PLT
	movq	-56(%rbp), %r8
	movq	%r13, 432(%r14)
	movl	$1, %r9d
	movq	%r8, 424(%r14)
	jmp	.L1180
	.p2align 4,,10
	.p2align 3
.L1200:
	movl	$2102, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r8, %rdi
	call	CRYPTO_free@PLT
	xorl	%r9d, %r9d
	jmp	.L1180
.L1201:
	movq	408(%r14), %rdi
	movl	$2094, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r8, -56(%rbp)
	call	CRYPTO_free@PLT
	movq	-56(%rbp), %r8
	movq	%r13, 416(%r14)
	movl	$1, %r9d
	movq	%r8, 408(%r14)
	jmp	.L1180
.L1199:
	movl	$2068, %r8d
	movl	$65, %edx
	movl	$632, %esi
	movl	$20, %edi
	leaq	.LC5(%rip), %rcx
	call	ERR_put_error@PLT
	movl	-56(%rbp), %r9d
	jmp	.L1180
	.cfi_endproc
.LFE1627:
	.size	tls1_set_sigalgs, .-tls1_set_sigalgs
	.p2align 4
	.globl	tls1_check_chain
	.type	tls1_check_chain, @function
tls1_check_chain:
.LFB1630:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	1168(%rdi), %r14
	movq	%rdx, -80(%rbp)
	movl	28(%r14), %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%esi, %ebx
	andl	$196608, %ebx
	cmpl	$-1, %r8d
	je	.L1203
	leaq	32(%r14), %rcx
	cmpl	$-2, %r8d
	je	.L1386
	movslq	%r8d, %rax
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rcx,%rdx,8), %rdx
.L1205:
	movq	168(%r12), %rcx
	movq	(%rdx), %r13
	andl	$196609, %esi
	movl	%esi, -72(%rbp)
	leaq	784(%rcx,%rax,4), %rax
	movq	%rax, -88(%rbp)
	movq	8(%rdx), %rax
	movq	%rax, -80(%rbp)
	testq	%r13, %r13
	je	.L1383
	testq	%rax, %rax
	je	.L1383
	movq	16(%rdx), %r15
	testl	%ebx, %ebx
	jne	.L1285
.L1284:
	movq	%r12, %rdi
	movl	%r8d, -68(%rbp)
	call	SSL_version@PLT
	movl	-68(%rbp), %r8d
	sarl	$8, %eax
	cmpl	$3, %eax
	je	.L1387
.L1244:
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	tls1_check_cert_param
	movl	%eax, -68(%rbp)
	testl	%eax, %eax
	je	.L1206
	movl	$0, -68(%rbp)
.L1278:
	movl	56(%r12), %eax
	orl	$64, %ebx
	testl	%eax, %eax
	je	.L1287
.L1248:
	movl	-72(%rbp), %r11d
	testl	%r11d, %r11d
	je	.L1250
.L1288:
	movl	%ebx, %eax
	xorl	%r14d, %r14d
	orb	$-128, %al
	movl	%eax, -72(%rbp)
	jmp	.L1251
	.p2align 4,,10
	.p2align 3
.L1254:
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	tls1_check_cert_param
	testl	%eax, %eax
	je	.L1388
	addl	$1, %r14d
.L1251:
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r14d
	jl	.L1254
	movl	56(%r12), %esi
	testl	%esi, %esi
	je	.L1249
	movl	-72(%rbp), %ebx
	jmp	.L1250
	.p2align 4,,10
	.p2align 3
.L1386:
	movq	(%r14), %rdx
	movq	%rdx, %rax
	subq	%rcx, %rax
	movabsq	$-3689348814741910323, %rcx
	sarq	$3, %rax
	imulq	%rcx, %rax
	movl	%eax, %r8d
	cltq
	jmp	.L1205
	.p2align 4,,10
	.p2align 3
.L1203:
	testq	%r13, %r13
	je	.L1385
	movq	%rdx, %rdi
	testq	%rdx, %rdx
	je	.L1385
	leaq	-64(%rbp), %rsi
	movq	%rcx, %r15
	call	ssl_cert_lookup_by_pkey@PLT
	testq	%rax, %rax
	je	.L1385
	movq	-64(%rbp), %rax
	movq	168(%r12), %rdx
	movl	%eax, %r8d
	cltq
	leaq	784(%rdx,%rax,4), %rax
	movq	%rax, -88(%rbp)
	movl	28(%r14), %eax
	andl	$196609, %eax
	cmpl	$1, %eax
	sbbl	%edx, %edx
	andl	$-1696, %edx
	addl	$3824, %edx
	cmpl	$1, %eax
	sbbl	%eax, %eax
	movl	%edx, -68(%rbp)
	andl	$-1696, %eax
	addl	$1776, %eax
	testl	%ebx, %ebx
	je	.L1389
	xorl	%edi, %edi
	movl	%ebx, %ecx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movl	%r8d, -72(%rbp)
	call	X509_chain_check_suiteb@PLT
	movl	-72(%rbp), %r8d
	testl	%eax, %eax
	je	.L1390
	movq	%r12, %rdi
	movl	%r8d, -72(%rbp)
	call	SSL_version@PLT
	movl	-72(%rbp), %r8d
	sarl	$8, %eax
	cmpl	$3, %eax
	je	.L1310
	movl	$1, -72(%rbp)
	movl	$48, %ebx
.L1229:
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	tls1_check_cert_param
	testl	%eax, %eax
	jne	.L1278
	movl	56(%r12), %eax
	testl	%eax, %eax
	jne	.L1248
.L1287:
	movl	-72(%rbp), %r14d
	orb	$-128, %bl
	testl	%r14d, %r14d
	jne	.L1391
.L1250:
	movl	-68(%rbp), %r9d
	orb	$6, %bh
	testl	%r9d, %r9d
	jne	.L1264
.L1263:
	orl	$1, %ebx
.L1206:
	movq	%r12, %rdi
	call	SSL_version@PLT
	sarl	$8, %eax
	cmpl	$3, %eax
	je	.L1392
.L1275:
	movl	%ebx, %eax
	orl	$258, %eax
.L1276:
	movl	-68(%rbp), %r8d
	testl	%r8d, %r8d
	jne	.L1202
	testb	$1, %al
	je	.L1277
	movq	-88(%rbp), %rbx
	movl	%eax, (%rbx)
.L1202:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1393
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1277:
	.cfi_restore_state
	movq	-88(%rbp), %rax
	andl	$258, (%rax)
.L1385:
	xorl	%eax, %eax
	jmp	.L1202
	.p2align 4,,10
	.p2align 3
.L1285:
	movl	%ebx, %ecx
	xorl	%edi, %edi
	movq	%r15, %rdx
	movq	%r13, %rsi
	movl	%r8d, -68(%rbp)
	movl	$2048, %ebx
	call	X509_chain_check_suiteb@PLT
	movl	-68(%rbp), %r8d
	testl	%eax, %eax
	je	.L1284
.L1383:
	movq	%r12, %rdi
	movl	$0, -68(%rbp)
	xorl	%ebx, %ebx
	call	SSL_version@PLT
	sarl	$8, %eax
	cmpl	$3, %eax
	jne	.L1275
.L1392:
	movq	%r12, %rdi
	call	SSL_version@PLT
	cmpl	$770, %eax
	jle	.L1275
	movq	-88(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -80(%rbp)
	andl	$258, %eax
	orl	%ebx, %eax
	jmp	.L1276
	.p2align 4,,10
	.p2align 3
.L1391:
	movl	%ebx, -72(%rbp)
.L1249:
	movq	-80(%rbp), %rdi
	call	EVP_PKEY_id@PLT
	cmpl	$116, %eax
	je	.L1305
	cmpl	$408, %eax
	je	.L1306
	cmpl	$6, %eax
	je	.L1307
	orl	$1024, -72(%rbp)
	movq	168(%r12), %rdi
.L1257:
	movq	608(%rdi), %rbx
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	je	.L1381
	movq	%r13, %rdi
	xorl	%r14d, %r14d
	call	X509_get_issuer_name@PLT
	movq	%rax, %r13
	jmp	.L1265
	.p2align 4,,10
	.p2align 3
.L1267:
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	X509_NAME_cmp@PLT
	testl	%eax, %eax
	je	.L1381
	addl	$1, %r14d
.L1265:
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r14d
	jl	.L1267
	movq	%r12, -80(%rbp)
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L1268:
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r14d
	jge	.L1394
	movl	%r14d, %esi
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	call	X509_get_issuer_name@PLT
	movq	%rax, %r13
	jmp	.L1269
	.p2align 4,,10
	.p2align 3
.L1271:
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	X509_NAME_cmp@PLT
	testl	%eax, %eax
	je	.L1270
	addl	$1, %r12d
.L1269:
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jl	.L1271
	addl	$1, %r14d
	jmp	.L1268
	.p2align 4,,10
	.p2align 3
.L1388:
	movl	-68(%rbp), %r10d
	testl	%r10d, %r10d
	je	.L1382
	movl	56(%r12), %edx
	movl	%ebx, -72(%rbp)
	testl	%edx, %edx
	je	.L1249
	orb	$6, %bh
	.p2align 4,,10
	.p2align 3
.L1264:
	movl	-68(%rbp), %ecx
	movl	%ecx, %eax
	andl	%ebx, %eax
	cmpl	%ecx, %eax
	jne	.L1206
	jmp	.L1263
	.p2align 4,,10
	.p2align 3
.L1387:
	movl	$0, -68(%rbp)
.L1294:
	movl	-72(%rbp), %edx
	testl	%edx, %edx
	setne	-92(%rbp)
.L1282:
	movq	%r12, %rdi
	movl	%r8d, -96(%rbp)
	call	SSL_version@PLT
	cmpl	$770, %eax
	jle	.L1214
	cmpb	$0, -92(%rbp)
	jne	.L1395
.L1214:
	movl	-68(%rbp), %eax
	testl	%eax, %eax
	je	.L1244
.L1295:
	orl	$48, %ebx
	jmp	.L1229
	.p2align 4,,10
	.p2align 3
.L1390:
	movl	$2048, %ebx
.L1211:
	movq	%r12, %rdi
	movl	%r8d, -72(%rbp)
	call	SSL_version@PLT
	movl	-72(%rbp), %r8d
	movl	$1, -72(%rbp)
	sarl	$8, %eax
	cmpl	$3, %eax
	jne	.L1295
	jmp	.L1294
	.p2align 4,,10
	.p2align 3
.L1395:
	movq	168(%r12), %rax
	xorl	%ecx, %ecx
	movl	-96(%rbp), %r8d
	cmpq	$0, 752(%rax)
	je	.L1396
.L1215:
	movq	8(%r12), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L1280
	movl	(%rax), %eax
	cmpl	$65536, %eax
	je	.L1280
	cmpl	$771, %eax
	jg	.L1234
.L1280:
	movl	%ecx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%ecx, -92(%rbp)
	call	tls1_check_sig_alg.part.0
	movl	-92(%rbp), %ecx
	testl	%eax, %eax
	jne	.L1236
	movl	-68(%rbp), %eax
	testl	%eax, %eax
	je	.L1206
.L1374:
	movl	%ebx, -92(%rbp)
	orl	$32, %ebx
.L1289:
	xorl	%r14d, %r14d
	movl	%ebx, -96(%rbp)
	movl	%r14d, %ebx
	movl	%ecx, %r14d
	jmp	.L1242
	.p2align 4,,10
	.p2align 3
.L1397:
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	movl	%r14d, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	tls1_check_sig_alg.part.0
	testl	%eax, %eax
	je	.L1241
	addl	$1, %ebx
.L1242:
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L1397
	movl	-96(%rbp), %ebx
.L1240:
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	tls1_check_cert_param
	testl	%eax, %eax
	jne	.L1278
	movl	-68(%rbp), %eax
	testl	%eax, %eax
	je	.L1206
	movl	56(%r12), %eax
	testl	%eax, %eax
	je	.L1287
	jmp	.L1288
	.p2align 4,,10
	.p2align 3
.L1306:
	movl	$64, %esi
.L1256:
	movq	168(%r12), %rdi
	movq	600(%rdi), %rcx
	movq	592(%rdi), %rax
	testq	%rcx, %rcx
	je	.L1258
	addq	%rax, %rcx
	jmp	.L1260
	.p2align 4,,10
	.p2align 3
.L1259:
	addq	$1, %rax
	cmpq	%rcx, %rax
	je	.L1258
.L1260:
	movzbl	(%rax), %edx
	cmpl	%esi, %edx
	jne	.L1259
	orl	$1024, -72(%rbp)
	jmp	.L1257
	.p2align 4,,10
	.p2align 3
.L1307:
	movl	$1, %esi
	jmp	.L1256
	.p2align 4,,10
	.p2align 3
.L1258:
	movl	-72(%rbp), %eax
	andl	$1024, %eax
	orl	-68(%rbp), %eax
	jne	.L1257
	movl	$0, -68(%rbp)
.L1382:
	movl	-72(%rbp), %ebx
	jmp	.L1206
	.p2align 4,,10
	.p2align 3
.L1305:
	movl	$2, %esi
	jmp	.L1256
	.p2align 4,,10
	.p2align 3
.L1270:
	movq	-80(%rbp), %r12
.L1381:
	movl	-72(%rbp), %ebx
	movl	-68(%rbp), %edi
	orb	$2, %bh
	testl	%edi, %edi
	jne	.L1264
	jmp	.L1263
	.p2align 4,,10
	.p2align 3
.L1310:
	movb	$1, -92(%rbp)
	xorl	%ebx, %ebx
	movl	$1, -72(%rbp)
	jmp	.L1282
.L1236:
	orl	$16, %ebx
	jmp	.L1374
	.p2align 4,,10
	.p2align 3
.L1394:
	movl	-68(%rbp), %ecx
	movq	-80(%rbp), %r12
	movl	-72(%rbp), %ebx
	testl	%ecx, %ecx
	je	.L1206
	jmp	.L1264
.L1241:
	movl	-68(%rbp), %eax
	movl	-96(%rbp), %ebx
	testl	%eax, %eax
	je	.L1206
	movl	-92(%rbp), %ebx
	jmp	.L1229
.L1396:
	cmpq	$0, 744(%rax)
	jne	.L1215
	cmpl	$6, %r8d
	ja	.L1216
	leaq	.L1218(%rip), %rdx
	movslq	(%rdx,%r8,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1218:
	.long	.L1223-.L1218
	.long	.L1216-.L1218
	.long	.L1222-.L1218
	.long	.L1221-.L1218
	.long	.L1220-.L1218
	.long	.L1219-.L1218
	.long	.L1302-.L1218
	.text
.L1302:
	movl	$980, %eax
	movl	$986, %ecx
.L1217:
	movq	408(%r14), %r10
	testq	%r10, %r10
	je	.L1215
	movq	416(%r14), %r9
	testq	%r9, %r9
	je	.L1224
	xorl	%r8d, %r8d
	leaq	920+sigalg_lookup_tbl(%rip), %rdi
	.p2align 4,,10
	.p2align 3
.L1228:
	movzwl	(%r10,%r8,2), %esi
	leaq	sigalg_lookup_tbl(%rip), %rdx
	jmp	.L1226
	.p2align 4,,10
	.p2align 3
.L1398:
	addq	$40, %rdx
	cmpq	%rdi, %rdx
	je	.L1227
.L1226:
	cmpw	8(%rdx), %si
	jne	.L1398
	cmpl	$64, 12(%rdx)
	je	.L1399
.L1227:
	addq	$1, %r8
	cmpq	%r9, %r8
	jne	.L1228
.L1224:
	cmpl	$0, -68(%rbp)
	jne	.L1229
	jmp	.L1206
.L1219:
	movl	$979, %eax
	movl	$985, %ecx
	jmp	.L1217
.L1220:
	movl	$811, %eax
	movl	$807, %ecx
	jmp	.L1217
.L1221:
	movl	$408, %eax
	movl	$416, %ecx
	jmp	.L1217
.L1222:
	movl	$116, %eax
	movl	$113, %ecx
	jmp	.L1217
.L1223:
	movl	$6, %eax
	movl	$65, %ecx
	jmp	.L1217
.L1216:
	movq	8(%r12), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L1290
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L1290
	cmpl	$65536, %eax
	jne	.L1400
.L1290:
	orl	$48, %ebx
.L1238:
	xorl	%r14d, %r14d
	jmp	.L1239
	.p2align 4,,10
	.p2align 3
.L1243:
	movl	%r14d, %esi
	movq	%r15, %rdi
	addl	$1, %r14d
	call	OPENSSL_sk_value@PLT
.L1239:
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r14d
	jl	.L1243
	jmp	.L1240
.L1399:
	cmpl	%eax, 20(%rdx)
	jne	.L1227
	jmp	.L1215
	.p2align 4,,10
	.p2align 3
.L1234:
	movq	-80(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%ecx, -92(%rbp)
	call	find_sig_alg
	movl	-92(%rbp), %ecx
	testq	%rax, %rax
	je	.L1374
	movl	%ebx, %eax
	orl	$48, %ebx
	orl	$16, %eax
	movl	%eax, -92(%rbp)
	jmp	.L1289
.L1393:
	call	__stack_chk_fail@PLT
.L1400:
	movq	-80(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	find_sig_alg
	testq	%rax, %rax
	jne	.L1290
	orl	$32, %ebx
	jmp	.L1238
.L1389:
	movl	%eax, -68(%rbp)
	jmp	.L1211
	.cfi_endproc
.LFE1630:
	.size	tls1_check_chain, .-tls1_check_chain
	.p2align 4
	.globl	tls1_set_cert_validity
	.type	tls1_set_cert_validity, @function
tls1_set_cert_validity:
.LFB1631:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	tls1_check_chain
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	$1, %r8d
	call	tls1_check_chain
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	$2, %r8d
	call	tls1_check_chain
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	$3, %r8d
	call	tls1_check_chain
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	$4, %r8d
	call	tls1_check_chain
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	$5, %r8d
	call	tls1_check_chain
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$6, %r8d
	xorl	%esi, %esi
	call	tls1_check_chain
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	xorl	%esi, %esi
	movl	$7, %r8d
	call	tls1_check_chain
	addq	$8, %rsp
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	popq	%r12
	movl	$8, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	tls1_check_chain
	.cfi_endproc
.LFE1631:
	.size	tls1_set_cert_validity, .-tls1_set_cert_validity
	.p2align 4
	.globl	SSL_check_chain
	.type	SSL_check_chain, @function
SSL_check_chain:
.LFB1632:
	.cfi_startproc
	endbr64
	movl	$-1, %r8d
	jmp	tls1_check_chain
	.cfi_endproc
.LFE1632:
	.size	SSL_check_chain, .-SSL_check_chain
	.p2align 4
	.globl	ssl_get_auto_dh
	.type	ssl_get_auto_dh, @function
ssl_get_auto_dh:
.LFB1633:
	.cfi_startproc
	endbr64
	movq	1168(%rdi), %rax
	cmpl	$2, 24(%rax)
	je	.L1432
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	168(%rdi), %rax
	movq	568(%rax), %rdx
	testb	$20, 32(%rdx)
	je	.L1406
	cmpl	$256, 68(%rdx)
	je	.L1433
.L1408:
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
.L1432:
	jmp	DH_get_1024_160@PLT
	.p2align 4,,10
	.p2align 3
.L1406:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	736(%rax), %rax
	testq	%rax, %rax
	je	.L1431
	movq	8(%rax), %rdi
	call	EVP_PKEY_security_bits@PLT
	movl	%eax, %ebx
	cmpl	$127, %eax
	jle	.L1411
.L1407:
	call	DH_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1431
	call	BN_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1414
	movl	$2, %esi
	movq	%rax, %rdi
	call	BN_set_word@PLT
	testl	%eax, %eax
	je	.L1414
	xorl	%edi, %edi
	cmpl	$191, %ebx
	jg	.L1434
	call	BN_get_rfc3526_prime_3072@PLT
	movq	%rax, %r14
.L1416:
	testq	%r14, %r14
	je	.L1418
	xorl	%edx, %edx
	movq	%r13, %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	DH_set0_pqg@PLT
	testl	%eax, %eax
	je	.L1418
.L1404:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1411:
	.cfi_restore_state
	cmpl	$111, %eax
	jle	.L1408
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	DH_get_2048_224@PLT
	.p2align 4,,10
	.p2align 3
.L1434:
	.cfi_restore_state
	call	BN_get_rfc3526_prime_8192@PLT
	movq	%rax, %r14
	jmp	.L1416
	.p2align 4,,10
	.p2align 3
.L1414:
	movq	%r12, %rdi
	call	DH_free@PLT
	movq	%r13, %rdi
	call	BN_free@PLT
.L1431:
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1433:
	.cfi_restore_state
	movl	$128, %ebx
	jmp	.L1407
	.p2align 4,,10
	.p2align 3
.L1418:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	DH_free@PLT
	movq	%r14, %rdi
	call	BN_free@PLT
	movq	%r13, %rdi
	call	BN_free@PLT
	jmp	.L1404
	.cfi_endproc
.LFE1633:
	.size	ssl_get_auto_dh, .-ssl_get_auto_dh
	.p2align 4
	.globl	ssl_security_cert
	.type	ssl_security_cert, @function
ssl_security_cert:
.LFB1636:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$1, %ecx
	sbbl	%r14d, %r14d
	andl	$-4096, %r14d
	addl	$397330, %r14d
	cmpl	$1, %ecx
	sbbl	%r15d, %r15d
	andl	$-4096, %r15d
	addl	$397329, %r15d
	cmpl	$1, %ecx
	sbbl	%esi, %esi
	andl	$-4096, %esi
	addl	$397328, %esi
	movl	%esi, -84(%rbp)
	movl	%esi, -88(%rbp)
	testl	%r8d, %r8d
	je	.L1437
	call	X509_get0_pubkey@PLT
	movl	-84(%rbp), %esi
	movl	$-1, %edx
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L1438
	call	EVP_PKEY_security_bits@PLT
	movl	-84(%rbp), %esi
	movl	%eax, %edx
.L1438:
	movq	%rbx, %r8
	xorl	%ecx, %ecx
	testq	%r12, %r12
	je	.L1439
	movq	%r12, %rdi
	call	ssl_security@PLT
	movl	%eax, %edx
.L1440:
	movl	$399, %eax
	testl	%edx, %edx
	je	.L1435
.L1447:
	movq	%rbx, %rdi
	call	X509_get_extension_flags@PLT
	movl	%eax, %r8d
	movl	$1, %eax
	andl	$8192, %r8d
	jne	.L1435
	xorl	%r8d, %r8d
	leaq	-68(%rbp), %rcx
	leaq	-60(%rbp), %rdx
	movq	%rbx, %rdi
	leaq	-64(%rbp), %rsi
	call	X509_get_signature_info@PLT
	testl	%eax, %eax
	je	.L1448
	movl	-68(%rbp), %edx
.L1449:
	movl	-64(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L1450
	movl	-60(%rbp), %ecx
	movl	%ecx, -64(%rbp)
.L1450:
	movq	%rbx, %r8
	movl	%r14d, %esi
	testq	%r12, %r12
	je	.L1451
	movq	%r12, %rdi
	call	ssl_security@PLT
.L1452:
	cmpl	$1, %eax
	sbbl	%eax, %eax
	andl	$397, %eax
	addl	$1, %eax
.L1435:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1466
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1437:
	.cfi_restore_state
	call	X509_get0_pubkey@PLT
	movl	$-1, %edx
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1444
	call	EVP_PKEY_security_bits@PLT
	movl	%eax, %edx
.L1444:
	movq	%rbx, %r8
	xorl	%ecx, %ecx
	movl	%r15d, %esi
	testq	%r12, %r12
	je	.L1445
	movq	%r12, %rdi
	call	ssl_security@PLT
	movl	%eax, %edx
.L1446:
	movl	$397, %eax
	testl	%edx, %edx
	jne	.L1447
	jmp	.L1435
	.p2align 4,,10
	.p2align 3
.L1448:
	movl	$-1, -68(%rbp)
	movl	$-1, %edx
	jmp	.L1449
	.p2align 4,,10
	.p2align 3
.L1445:
	movq	%r13, %rdi
	call	ssl_ctx_security@PLT
	movl	%eax, %edx
	jmp	.L1446
	.p2align 4,,10
	.p2align 3
.L1439:
	movq	%r13, %rdi
	call	ssl_ctx_security@PLT
	movl	%eax, %edx
	jmp	.L1440
	.p2align 4,,10
	.p2align 3
.L1451:
	movq	%r13, %rdi
	call	ssl_ctx_security@PLT
	jmp	.L1452
.L1466:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1636:
	.size	ssl_security_cert, .-ssl_security_cert
	.p2align 4
	.globl	ssl_security_cert_chain
	.type	ssl_security_cert_chain, @function
ssl_security_cert_chain:
.LFB1637:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L1490
.L1468:
	movl	$1, %r8d
	movl	%ebx, %ecx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	ssl_security_cert
	movl	%eax, -84(%rbp)
	cmpl	$1, %eax
	jne	.L1467
	cmpl	$1, %ebx
	sbbl	%eax, %eax
	notl	%eax
	andl	$4096, %eax
	movl	%eax, %r15d
	orl	$393234, %eax
	movl	%eax, -88(%rbp)
	orl	$393233, %r15d
	.p2align 4,,10
	.p2align 3
.L1470:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L1467
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	X509_get0_pubkey@PLT
	movl	$-1, %edx
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1471
	call	EVP_PKEY_security_bits@PLT
	movl	%eax, %edx
.L1471:
	movq	%rbx, %r8
	xorl	%ecx, %ecx
	movl	%r15d, %esi
	testq	%r13, %r13
	je	.L1472
	movq	%r13, %rdi
	call	ssl_security@PLT
	testl	%eax, %eax
	je	.L1485
.L1492:
	movq	%rbx, %rdi
	call	X509_get_extension_flags@PLT
	testb	$32, %ah
	jne	.L1475
	xorl	%r8d, %r8d
	leaq	-68(%rbp), %rcx
	leaq	-60(%rbp), %rdx
	movq	%rbx, %rdi
	leaq	-64(%rbp), %rsi
	call	X509_get_signature_info@PLT
	testl	%eax, %eax
	je	.L1476
	movl	-68(%rbp), %edx
.L1477:
	movl	-64(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L1478
	movl	-60(%rbp), %ecx
	movl	%ecx, -64(%rbp)
.L1478:
	movl	-88(%rbp), %esi
	movq	%rbx, %r8
	testq	%r13, %r13
	je	.L1479
	movq	%r13, %rdi
	call	ssl_security@PLT
.L1480:
	testl	%eax, %eax
	jne	.L1475
	movl	$398, -84(%rbp)
.L1467:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1491
	movl	-84(%rbp), %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1475:
	.cfi_restore_state
	addl	$1, %r12d
	jmp	.L1470
	.p2align 4,,10
	.p2align 3
.L1472:
	xorl	%edi, %edi
	call	ssl_ctx_security@PLT
	testl	%eax, %eax
	jne	.L1492
.L1485:
	movl	$397, -84(%rbp)
	jmp	.L1467
	.p2align 4,,10
	.p2align 3
.L1476:
	movl	$-1, -68(%rbp)
	movl	$-1, %edx
	jmp	.L1477
	.p2align 4,,10
	.p2align 3
.L1479:
	xorl	%edi, %edi
	call	ssl_ctx_security@PLT
	jmp	.L1480
	.p2align 4,,10
	.p2align 3
.L1490:
	xorl	%esi, %esi
	movq	%r14, %rdi
	movl	$1, %r12d
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdx
	jmp	.L1468
.L1491:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1637:
	.size	ssl_security_cert_chain, .-ssl_security_cert_chain
	.p2align 4
	.globl	tls_choose_sigalg
	.type	tls_choose_sigalg, @function
tls_choose_sigalg:
.LFB1643:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	168(%rdi), %rax
	movl	%esi, -64(%rbp)
	movups	%xmm0, 728(%rax)
	movq	8(%rdi), %rdx
	movq	192(%rdx), %rcx
	movl	96(%rcx), %ecx
	testb	$8, %cl
	jne	.L1494
	movl	(%rdx), %edx
	cmpl	$771, %edx
	jle	.L1494
	cmpl	$65536, %edx
	jne	.L1593
.L1494:
	movq	568(%rax), %rdx
	testb	$-85, 32(%rdx)
	je	.L1590
	movl	56(%r12), %r10d
	testl	%r10d, %r10d
	je	.L1594
.L1499:
	andl	$2, %ecx
	je	.L1500
	cmpq	$0, 744(%rax)
	je	.L1501
	movq	1168(%r12), %rax
	movl	$-1, -60(%rbp)
	testl	$196608, 28(%rax)
	jne	.L1595
.L1502:
	movq	6272(%r12), %rdx
	xorl	%ebx, %ebx
	testq	%rdx, %rdx
	jne	.L1503
	jmp	.L1519
	.p2align 4,,10
	.p2align 3
.L1505:
	movq	1168(%r12), %rax
	movabsq	$-3689348814741910323, %rdi
	leaq	32(%rax), %rcx
	movq	(%rax), %rax
	subq	%rcx, %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	cmpl	%eax, %r10d
	jne	.L1507
	cmpl	$-1, %r10d
	jne	.L1532
.L1507:
	addq	$1, %rbx
	cmpq	%rdx, %rbx
	jnb	.L1596
.L1503:
	movq	6264(%r12), %rax
	movl	56(%r12), %r8d
	movq	(%rax,%rbx,8), %r14
	movl	24(%r14), %r10d
	testl	%r8d, %r8d
	je	.L1505
	movslq	%r10d, %r13
	movl	%r10d, -56(%rbp)
	movq	%r13, %rdi
	call	ssl_cert_lookup_by_idx@PLT
	movl	-56(%rbp), %r10d
	testq	%rax, %rax
	je	.L1508
	movq	168(%r12), %rdx
	movl	4(%rax), %esi
	movq	568(%rdx), %rcx
	testl	%esi, 32(%rcx)
	je	.L1508
	cmpl	$912, (%rax)
	je	.L1597
.L1509:
	testb	$1, 784(%rdx,%r13,4)
	je	.L1508
	cmpl	$-1, %r10d
	je	.L1508
.L1532:
	cmpl	$8, %r10d
	ja	.L1589
	movslq	%r10d, %r13
	movq	1168(%r12), %rax
	leaq	0(%r13,%r13,4), %r15
	salq	$3, %r15
	addq	%r15, %rax
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1589
	movq	40(%rax), %rcx
	testq	%rcx, %rcx
	je	.L1589
	leaq	12(%r14), %rsi
	leaq	168(%r12), %rdi
	movl	%r10d, -56(%rbp)
	call	check_cert_usable.isra.0
	movl	-56(%rbp), %r10d
	testl	%eax, %eax
	je	.L1589
	cmpl	$912, 20(%r14)
	je	.L1598
.L1514:
	movl	-60(%rbp), %eax
	movq	6272(%r12), %rdx
	cmpl	$-1, %eax
	je	.L1518
	cmpl	%eax, 32(%r14)
	jne	.L1507
.L1518:
	cmpq	%rbx, %rdx
	jne	.L1531
.L1519:
	movl	-64(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L1599
	.p2align 4,,10
	.p2align 3
.L1590:
	movl	$1, %eax
.L1493:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1594:
	.cfi_restore_state
	movq	1168(%r12), %rsi
	movabsq	$-3689348814741910323, %rdi
	movq	(%rsi), %rbx
	leaq	32(%rsi), %rdx
	subq	%rdx, %rbx
	movq	%rbx, %rdx
	sarq	$3, %rdx
	imulq	%rdi, %rdx
	cmpl	$8, %edx
	ja	.L1590
	movslq	%edx, %rdx
	leaq	(%rdx,%rdx,4), %rdx
	leaq	(%rsi,%rdx,8), %rdx
	cmpq	$0, 32(%rdx)
	je	.L1590
	cmpq	$0, 40(%rdx)
	jne	.L1499
	jmp	.L1590
	.p2align 4,,10
	.p2align 3
.L1598:
	addq	1168(%r12), %r15
	movl	%r10d, -56(%rbp)
	movq	40(%r15), %rdi
	call	EVP_PKEY_get0@PLT
	movl	-56(%rbp), %r10d
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L1589
	movl	12(%r14), %edi
	testl	%edi, %edi
	jne	.L1516
	.p2align 4,,10
	.p2align 3
.L1589:
	movq	6272(%r12), %rdx
	addq	$1, %rbx
	cmpq	%rdx, %rbx
	jb	.L1503
.L1596:
	je	.L1519
	movslq	%r10d, %r13
	cmpl	$-1, %r10d
	je	.L1495
	jmp	.L1531
	.p2align 4,,10
	.p2align 3
.L1597:
	testb	$1, 28(%rcx)
	je	.L1509
	.p2align 4,,10
	.p2align 3
.L1508:
	movq	6272(%r12), %rdx
	movl	$-1, %r10d
	jmp	.L1507
	.p2align 4,,10
	.p2align 3
.L1593:
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	find_sig_alg
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1600
.L1495:
	movslq	24(%r14), %r13
.L1531:
	movq	1168(%r12), %rdx
	movq	168(%r12), %rax
	leaq	0(%r13,%r13,4), %rcx
	leaq	32(%rdx,%rcx,8), %rcx
	movq	%rcx, 736(%rax)
	movq	%rcx, (%rdx)
	movq	%r14, 728(%rax)
	jmp	.L1590
	.p2align 4,,10
	.p2align 3
.L1595:
	movq	160(%rax), %rdi
	call	EVP_PKEY_get0_EC_KEY@PLT
	movq	%rax, %rdi
	call	EC_KEY_get0_group@PLT
	movq	%rax, %rdi
	call	EC_GROUP_get_curve_name@PLT
	movl	%eax, -60(%rbp)
	jmp	.L1502
	.p2align 4,,10
	.p2align 3
.L1516:
	movl	16(%r14), %edi
	movl	%r10d, -56(%rbp)
	call	ssl_md@PLT
	movl	-56(%rbp), %r10d
	testq	%rax, %rax
	je	.L1589
	movq	%r15, %rdi
	movl	%r10d, -56(%rbp)
	movq	%rax, -72(%rbp)
	call	RSA_size@PLT
	movq	-72(%rbp), %r8
	movl	%eax, %r15d
	movq	%r8, %rdi
	call	EVP_MD_size@PLT
	movl	-56(%rbp), %r10d
	leal	2(%rax,%rax), %eax
	cmpl	%eax, %r15d
	jge	.L1514
	jmp	.L1589
	.p2align 4,,10
	.p2align 3
.L1500:
	movl	$-1, %esi
	movq	%r12, %rdi
	call	tls1_get_legacy_sigalg
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L1495
	movl	-64(%rbp), %eax
	testl	%eax, %eax
	je	.L1590
	movl	$2871, %r9d
.L1592:
	leaq	.LC5(%rip), %r8
	movl	$68, %ecx
	movl	$513, %edx
	movq	%r12, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L1493
.L1599:
	movl	$2831, %r9d
.L1591:
	leaq	.LC5(%rip), %r8
	movl	$118, %ecx
	movl	$513, %edx
	movq	%r12, %rdi
	movl	$40, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L1493
	.p2align 4,,10
	.p2align 3
.L1501:
	movl	$-1, %esi
	movq	%r12, %rdi
	call	tls1_get_legacy_sigalg
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1601
	movq	1168(%r12), %rdx
	movl	28(%rdx), %eax
	andl	$196608, %eax
	cmpl	$131072, %eax
	je	.L1535
	cmpl	$196608, %eax
	je	.L1536
	cmpl	$65536, %eax
	je	.L1537
	cmpl	$1, 56(%r12)
	je	.L1602
.L1523:
	movq	408(%rdx), %r8
	testq	%r8, %r8
	je	.L1538
	movq	416(%rdx), %r15
.L1524:
	testq	%r15, %r15
	jne	.L1522
.L1530:
	movl	-64(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L1590
	movl	$2861, %r9d
	leaq	.LC5(%rip), %r8
	movl	$370, %ecx
	movq	%r12, %rdi
	movl	$513, %edx
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L1493
	.p2align 4,,10
	.p2align 3
.L1600:
	movl	-64(%rbp), %r11d
	testl	%r11d, %r11d
	je	.L1590
	movl	$2750, %r9d
	jmp	.L1591
.L1536:
	leaq	suiteb_sigalgs(%rip), %r8
	movl	$2, %r15d
.L1522:
	xorl	%r13d, %r13d
	leaq	12(%r14), %rbx
	jmp	.L1527
	.p2align 4,,10
	.p2align 3
.L1529:
	addq	$1, %r13
	cmpq	%r15, %r13
	jnb	.L1603
.L1527:
	movzwl	(%r8,%r13,2), %eax
	cmpw	%ax, 8(%r14)
	jne	.L1529
	movslq	24(%r14), %rax
	cmpl	$8, %eax
	ja	.L1529
	movq	1168(%r12), %rdx
	leaq	(%rax,%rax,4), %rax
	leaq	(%rdx,%rax,8), %rax
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1529
	movq	40(%rax), %rcx
	testq	%rcx, %rcx
	je	.L1529
	leaq	168(%r12), %rdi
	movq	%rbx, %rsi
	movq	%r8, -56(%rbp)
	call	check_cert_usable.isra.0
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	je	.L1529
	jmp	.L1495
.L1601:
	movl	-64(%rbp), %esi
	movl	$2846, %r9d
	testl	%esi, %esi
	je	.L1590
	jmp	.L1592
.L1603:
	jne	.L1495
	jmp	.L1530
.L1537:
	leaq	suiteb_sigalgs(%rip), %r8
	movl	$1, %r15d
	jmp	.L1522
.L1535:
	leaq	2+suiteb_sigalgs(%rip), %r8
	movl	$1, %r15d
	jmp	.L1522
.L1602:
	movq	424(%rdx), %r8
	testq	%r8, %r8
	je	.L1523
	movq	432(%rdx), %r15
	jmp	.L1524
.L1538:
	leaq	tls12_sigalgs(%rip), %r8
	movl	$23, %r15d
	jmp	.L1522
	.cfi_endproc
.LFE1643:
	.size	tls_choose_sigalg, .-tls_choose_sigalg
	.p2align 4
	.globl	SSL_CTX_set_tlsext_max_fragment_length
	.type	SSL_CTX_set_tlsext_max_fragment_length, @function
SSL_CTX_set_tlsext_max_fragment_length:
.LFB1644:
	.cfi_startproc
	endbr64
	cmpb	$4, %sil
	ja	.L1611
	movb	%sil, 580(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1611:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$232, %edx
	movl	$551, %esi
	movl	$20, %edi
	movl	$2889, %r8d
	leaq	.LC5(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1644:
	.size	SSL_CTX_set_tlsext_max_fragment_length, .-SSL_CTX_set_tlsext_max_fragment_length
	.p2align 4
	.globl	SSL_set_tlsext_max_fragment_length
	.type	SSL_set_tlsext_max_fragment_length, @function
SSL_set_tlsext_max_fragment_length:
.LFB1645:
	.cfi_startproc
	endbr64
	cmpb	$4, %sil
	ja	.L1619
	movb	%sil, 1844(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1619:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$232, %edx
	movl	$550, %esi
	movl	$20, %edi
	movl	$2902, %r8d
	leaq	.LC5(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1645:
	.size	SSL_set_tlsext_max_fragment_length, .-SSL_set_tlsext_max_fragment_length
	.p2align 4
	.globl	SSL_SESSION_get_max_fragment_length
	.type	SSL_SESSION_get_max_fragment_length, @function
SSL_SESSION_get_max_fragment_length:
.LFB1646:
	.cfi_startproc
	endbr64
	movzbl	600(%rdi), %eax
	ret
	.cfi_endproc
.LFE1646:
	.size	SSL_SESSION_get_max_fragment_length, .-SSL_SESSION_get_max_fragment_length
	.section	.rodata
	.align 16
	.type	tls_default_sigalg, @object
	.size	tls_default_sigalg, 18
tls_default_sigalg:
	.value	513
	.value	0
	.value	514
	.value	515
	.value	-4627
	.value	-4370
	.value	-4113
	.value	0
	.value	0
	.section	.rodata.str1.1
.LC6:
	.string	"rsa_pkcs1_md5_sha1"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	legacy_rsa_sigalg, @object
	.size	legacy_rsa_sigalg, 40
legacy_rsa_sigalg:
	.quad	.LC6
	.value	0
	.zero	2
	.long	114
	.long	9
	.long	6
	.long	0
	.long	0
	.long	0
	.zero	4
	.section	.rodata.str1.1
.LC7:
	.string	"ecdsa_secp256r1_sha256"
.LC8:
	.string	"ecdsa_secp384r1_sha384"
.LC9:
	.string	"ecdsa_secp521r1_sha512"
.LC10:
	.string	"ed25519"
.LC11:
	.string	"ed448"
.LC12:
	.string	"rsa_pss_rsae_sha256"
.LC13:
	.string	"rsa_pss_rsae_sha384"
.LC14:
	.string	"rsa_pss_rsae_sha512"
.LC15:
	.string	"rsa_pss_pss_sha256"
.LC16:
	.string	"rsa_pss_pss_sha384"
.LC17:
	.string	"rsa_pss_pss_sha512"
.LC18:
	.string	"rsa_pkcs1_sha256"
.LC19:
	.string	"rsa_pkcs1_sha384"
.LC20:
	.string	"rsa_pkcs1_sha512"
.LC21:
	.string	"rsa_pkcs1_sha224"
.LC22:
	.string	"rsa_pkcs1_sha1"
	.section	.data.rel.ro.local
	.align 32
	.type	sigalg_lookup_tbl, @object
	.size	sigalg_lookup_tbl, 920
sigalg_lookup_tbl:
	.quad	.LC7
	.value	1027
	.zero	2
	.long	672
	.long	4
	.long	408
	.long	3
	.long	794
	.long	415
	.zero	4
	.quad	.LC8
	.value	1283
	.zero	2
	.long	673
	.long	5
	.long	408
	.long	3
	.long	795
	.long	715
	.zero	4
	.quad	.LC9
	.value	1539
	.zero	2
	.long	674
	.long	11
	.long	408
	.long	3
	.long	796
	.long	716
	.zero	4
	.quad	.LC10
	.value	2055
	.zero	2
	.long	0
	.long	-1
	.long	1087
	.long	7
	.long	0
	.long	0
	.zero	4
	.quad	.LC11
	.value	2056
	.zero	2
	.long	0
	.long	-1
	.long	1088
	.long	8
	.long	0
	.long	0
	.zero	4
	.quad	0
	.value	771
	.zero	2
	.long	675
	.long	10
	.long	408
	.long	3
	.long	793
	.long	0
	.zero	4
	.quad	0
	.value	515
	.zero	2
	.long	64
	.long	1
	.long	408
	.long	3
	.long	416
	.long	0
	.zero	4
	.quad	.LC12
	.value	2052
	.zero	2
	.long	672
	.long	4
	.long	912
	.long	0
	.long	0
	.long	0
	.zero	4
	.quad	.LC13
	.value	2053
	.zero	2
	.long	673
	.long	5
	.long	912
	.long	0
	.long	0
	.long	0
	.zero	4
	.quad	.LC14
	.value	2054
	.zero	2
	.long	674
	.long	11
	.long	912
	.long	0
	.long	0
	.long	0
	.zero	4
	.quad	.LC15
	.value	2057
	.zero	2
	.long	672
	.long	4
	.long	912
	.long	1
	.long	0
	.long	0
	.zero	4
	.quad	.LC16
	.value	2058
	.zero	2
	.long	673
	.long	5
	.long	912
	.long	1
	.long	0
	.long	0
	.zero	4
	.quad	.LC17
	.value	2059
	.zero	2
	.long	674
	.long	11
	.long	912
	.long	1
	.long	0
	.long	0
	.zero	4
	.quad	.LC18
	.value	1025
	.zero	2
	.long	672
	.long	4
	.long	6
	.long	0
	.long	668
	.long	0
	.zero	4
	.quad	.LC19
	.value	1281
	.zero	2
	.long	673
	.long	5
	.long	6
	.long	0
	.long	669
	.long	0
	.zero	4
	.quad	.LC20
	.value	1537
	.zero	2
	.long	674
	.long	11
	.long	6
	.long	0
	.long	670
	.long	0
	.zero	4
	.quad	.LC21
	.value	769
	.zero	2
	.long	675
	.long	10
	.long	6
	.long	0
	.long	671
	.long	0
	.zero	4
	.quad	.LC22
	.value	513
	.zero	2
	.long	64
	.long	1
	.long	6
	.long	0
	.long	65
	.long	0
	.zero	4
	.quad	0
	.value	1026
	.zero	2
	.long	672
	.long	4
	.long	116
	.long	2
	.long	803
	.long	0
	.zero	4
	.quad	0
	.value	1282
	.zero	2
	.long	673
	.long	5
	.long	116
	.long	2
	.long	0
	.long	0
	.zero	4
	.quad	0
	.value	1538
	.zero	2
	.long	674
	.long	11
	.long	116
	.long	2
	.long	0
	.long	0
	.zero	4
	.quad	0
	.value	770
	.zero	2
	.long	675
	.long	10
	.long	116
	.long	2
	.long	0
	.long	0
	.zero	4
	.quad	0
	.value	514
	.zero	2
	.long	64
	.long	1
	.long	116
	.long	2
	.long	113
	.long	0
	.zero	4
	.section	.rodata
	.align 2
	.type	suiteb_sigalgs, @object
	.size	suiteb_sigalgs, 4
suiteb_sigalgs:
	.value	1027
	.value	1283
	.align 32
	.type	tls12_sigalgs, @object
	.size	tls12_sigalgs, 46
tls12_sigalgs:
	.value	1027
	.value	1283
	.value	1539
	.value	2055
	.value	2056
	.value	2057
	.value	2058
	.value	2059
	.value	2052
	.value	2053
	.value	2054
	.value	1025
	.value	1281
	.value	1537
	.value	771
	.value	515
	.value	769
	.value	513
	.value	770
	.value	514
	.value	1026
	.value	1282
	.value	1538
	.align 2
	.type	suiteb_curves, @object
	.size	suiteb_curves, 4
suiteb_curves:
	.value	23
	.value	24
	.align 8
	.type	eccurves_default, @object
	.size	eccurves_default, 10
eccurves_default:
	.value	29
	.value	23
	.value	30
	.value	25
	.value	24
	.type	ecformats_default, @object
	.size	ecformats_default, 3
ecformats_default:
	.string	""
	.ascii	"\001\002"
	.align 32
	.type	nid_list, @object
	.size	nid_list, 360
nid_list:
	.long	721
	.long	80
	.value	1
	.zero	2
	.long	722
	.long	80
	.value	1
	.zero	2
	.long	723
	.long	80
	.value	1
	.zero	2
	.long	724
	.long	80
	.value	1
	.zero	2
	.long	725
	.long	80
	.value	1
	.zero	2
	.long	726
	.long	112
	.value	1
	.zero	2
	.long	727
	.long	112
	.value	1
	.zero	2
	.long	728
	.long	112
	.value	1
	.zero	2
	.long	729
	.long	128
	.value	1
	.zero	2
	.long	730
	.long	128
	.value	1
	.zero	2
	.long	731
	.long	192
	.value	1
	.zero	2
	.long	732
	.long	192
	.value	1
	.zero	2
	.long	733
	.long	256
	.value	1
	.zero	2
	.long	734
	.long	256
	.value	1
	.zero	2
	.long	708
	.long	80
	.value	0
	.zero	2
	.long	709
	.long	80
	.value	0
	.zero	2
	.long	710
	.long	80
	.value	0
	.zero	2
	.long	711
	.long	80
	.value	0
	.zero	2
	.long	409
	.long	80
	.value	0
	.zero	2
	.long	712
	.long	112
	.value	0
	.zero	2
	.long	713
	.long	112
	.value	0
	.zero	2
	.long	714
	.long	128
	.value	0
	.zero	2
	.long	415
	.long	128
	.value	0
	.zero	2
	.long	715
	.long	192
	.value	0
	.zero	2
	.long	716
	.long	256
	.value	0
	.zero	2
	.long	927
	.long	128
	.value	0
	.zero	2
	.long	931
	.long	192
	.value	0
	.zero	2
	.long	933
	.long	256
	.value	0
	.zero	2
	.long	1034
	.long	128
	.value	2
	.zero	2
	.long	1035
	.long	224
	.value	2
	.zero	2
	.globl	TLSv1_3_enc_data
	.section	.rodata.str1.1
.LC23:
	.string	"client finished"
.LC24:
	.string	"server finished"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	TLSv1_3_enc_data, @object
	.size	TLSv1_3_enc_data, 128
TLSv1_3_enc_data:
	.quad	tls13_enc
	.quad	tls1_mac
	.quad	tls13_setup_key_block
	.quad	tls13_generate_master_secret
	.quad	tls13_change_cipher_state
	.quad	tls13_final_finish_mac
	.quad	.LC23
	.quad	15
	.quad	.LC24
	.quad	15
	.quad	tls13_alert_code
	.quad	tls13_export_keying_material
	.long	6
	.zero	4
	.quad	ssl3_set_handshake_header
	.quad	tls_close_construct_packet
	.quad	ssl3_handshake_write
	.globl	TLSv1_2_enc_data
	.align 32
	.type	TLSv1_2_enc_data, @object
	.size	TLSv1_2_enc_data, 128
TLSv1_2_enc_data:
	.quad	tls1_enc
	.quad	tls1_mac
	.quad	tls1_setup_key_block
	.quad	tls1_generate_master_secret
	.quad	tls1_change_cipher_state
	.quad	tls1_final_finish_mac
	.quad	.LC23
	.quad	15
	.quad	.LC24
	.quad	15
	.quad	tls1_alert_code
	.quad	tls1_export_keying_material
	.long	23
	.zero	4
	.quad	ssl3_set_handshake_header
	.quad	tls_close_construct_packet
	.quad	ssl3_handshake_write
	.globl	TLSv1_1_enc_data
	.align 32
	.type	TLSv1_1_enc_data, @object
	.size	TLSv1_1_enc_data, 128
TLSv1_1_enc_data:
	.quad	tls1_enc
	.quad	tls1_mac
	.quad	tls1_setup_key_block
	.quad	tls1_generate_master_secret
	.quad	tls1_change_cipher_state
	.quad	tls1_final_finish_mac
	.quad	.LC23
	.quad	15
	.quad	.LC24
	.quad	15
	.quad	tls1_alert_code
	.quad	tls1_export_keying_material
	.long	1
	.zero	4
	.quad	ssl3_set_handshake_header
	.quad	tls_close_construct_packet
	.quad	ssl3_handshake_write
	.globl	TLSv1_enc_data
	.align 32
	.type	TLSv1_enc_data, @object
	.size	TLSv1_enc_data, 128
TLSv1_enc_data:
	.quad	tls1_enc
	.quad	tls1_mac
	.quad	tls1_setup_key_block
	.quad	tls1_generate_master_secret
	.quad	tls1_change_cipher_state
	.quad	tls1_final_finish_mac
	.quad	.LC23
	.quad	15
	.quad	.LC24
	.quad	15
	.quad	tls1_alert_code
	.quad	tls1_export_keying_material
	.long	0
	.zero	4
	.quad	ssl3_set_handshake_header
	.quad	tls_close_construct_packet
	.quad	ssl3_handshake_write
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
