	.file	"rsa_none.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/rsa/rsa_none.c"
	.text
	.p2align 4
	.globl	RSA_padding_add_none
	.type	RSA_padding_add_none, @function
RSA_padding_add_none:
.LFB419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	%esi, %ecx
	jg	.L7
	jl	.L8
	movq	%rdx, %rsi
	movl	%ecx, %edx
	call	memcpy@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movl	$23, %r8d
	movl	$122, %edx
	movl	$107, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	movl	$18, %r8d
	movl	$110, %edx
	movl	$107, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE419:
	.size	RSA_padding_add_none, .-RSA_padding_add_none
	.p2align 4
	.globl	RSA_padding_check_none
	.type	RSA_padding_check_none, @function
RSA_padding_check_none:
.LFB420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	cmpl	%esi, %ecx
	jg	.L13
	movq	%rdx, %r13
	movl	%esi, %edx
	movl	%esi, %ebx
	xorl	%esi, %esi
	subl	%ecx, %edx
	movl	%ecx, %r12d
	movslq	%edx, %rdx
	call	memset@PLT
	movslq	%r12d, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movslq	%ebx, %rax
	subq	%rdx, %rax
	addq	%rax, %rdi
	call	memcpy@PLT
	movl	%ebx, %eax
.L9:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	movl	$36, %r8d
	movl	$109, %edx
	movl	$111, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L9
	.cfi_endproc
.LFE420:
	.size	RSA_padding_check_none, .-RSA_padding_check_none
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
