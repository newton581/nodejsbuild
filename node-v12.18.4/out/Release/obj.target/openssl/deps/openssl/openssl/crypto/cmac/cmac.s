	.file	"cmac.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/cmac/cmac.c"
	.text
	.p2align 4
	.globl	CMAC_CTX_new
	.type	CMAC_CTX_new, @function
CMAC_CTX_new:
.LFB420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$50, %edx
	movl	$144, %edi
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L7
	call	EVP_CIPHER_CTX_new@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L8
	movl	$-1, 136(%r12)
.L1:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	movl	$51, %r8d
	movl	$65, %edx
	movl	$120, %esi
	movl	$15, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%r12, %rdi
	movl	$56, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%r12d, %r12d
	call	CRYPTO_free@PLT
	jmp	.L1
	.cfi_endproc
.LFE420:
	.size	CMAC_CTX_new, .-CMAC_CTX_new
	.p2align 4
	.globl	CMAC_CTX_cleanup
	.type	CMAC_CTX_cleanup, @function
CMAC_CTX_cleanup:
.LFB421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	EVP_CIPHER_CTX_reset@PLT
	leaq	72(%rbx), %rdi
	movl	$32, %esi
	call	OPENSSL_cleanse@PLT
	leaq	8(%rbx), %rdi
	movl	$32, %esi
	call	OPENSSL_cleanse@PLT
	leaq	40(%rbx), %rdi
	movl	$32, %esi
	call	OPENSSL_cleanse@PLT
	leaq	104(%rbx), %rdi
	movl	$32, %esi
	call	OPENSSL_cleanse@PLT
	movl	$-1, 136(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE421:
	.size	CMAC_CTX_cleanup, .-CMAC_CTX_cleanup
	.p2align 4
	.globl	CMAC_CTX_get0_cipher_ctx
	.type	CMAC_CTX_get0_cipher_ctx, @function
CMAC_CTX_get0_cipher_ctx:
.LFB422:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE422:
	.size	CMAC_CTX_get0_cipher_ctx, .-CMAC_CTX_get0_cipher_ctx
	.p2align 4
	.globl	CMAC_CTX_free
	.type	CMAC_CTX_free, @function
CMAC_CTX_free:
.LFB423:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L12
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	EVP_CIPHER_CTX_reset@PLT
	leaq	72(%r12), %rdi
	movl	$32, %esi
	call	OPENSSL_cleanse@PLT
	leaq	8(%r12), %rdi
	movl	$32, %esi
	call	OPENSSL_cleanse@PLT
	leaq	40(%r12), %rdi
	movl	$32, %esi
	call	OPENSSL_cleanse@PLT
	leaq	104(%r12), %rdi
	movl	$32, %esi
	call	OPENSSL_cleanse@PLT
	movq	(%r12), %rdi
	movl	$-1, 136(%r12)
	call	EVP_CIPHER_CTX_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$84, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L12:
	ret
	.cfi_endproc
.LFE423:
	.size	CMAC_CTX_free, .-CMAC_CTX_free
	.p2align 4
	.globl	CMAC_CTX_copy
	.type	CMAC_CTX_copy, @function
CMAC_CTX_copy:
.LFB424:
	.cfi_startproc
	endbr64
	cmpl	$-1, 136(%rsi)
	je	.L27
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rsi
	movq	(%rdi), %rdi
	call	EVP_CIPHER_CTX_copy@PLT
	testl	%eax, %eax
	jne	.L28
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	EVP_CIPHER_CTX_block_size@PLT
	leaq	8(%r13), %rdi
	leaq	8(%rbx), %rsi
	movslq	%eax, %r12
	movq	%r12, %rdx
	call	memcpy@PLT
	leaq	40(%r13), %rdi
	leaq	40(%rbx), %rsi
	movq	%r12, %rdx
	call	memcpy@PLT
	leaq	72(%r13), %rdi
	leaq	72(%rbx), %rsi
	movq	%r12, %rdx
	call	memcpy@PLT
	leaq	104(%r13), %rdi
	leaq	104(%rbx), %rsi
	movq	%r12, %rdx
	call	memcpy@PLT
	movl	136(%rbx), %eax
	movl	%eax, 136(%r13)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE424:
	.size	CMAC_CTX_copy, .-CMAC_CTX_copy
	.p2align 4
	.globl	CMAC_Init
	.type	CMAC_Init, @function
CMAC_Init:
.LFB425:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	movq	%r8, %rdx
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	orq	%rcx, %rax
	jne	.L30
	orq	%r13, %rdx
	je	.L31
.L70:
	movl	$1, %eax
.L29:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movq	%rcx, %rsi
	testq	%rcx, %rcx
	je	.L38
	movq	(%rdi), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	call	EVP_EncryptInit_ex@PLT
	testl	%eax, %eax
	je	.L34
.L38:
	testq	%r12, %r12
	je	.L70
	movq	(%rbx), %rdi
	call	EVP_CIPHER_CTX_cipher@PLT
	testq	%rax, %rax
	je	.L34
	movq	(%rbx), %rdi
	movl	%r13d, %esi
	call	EVP_CIPHER_CTX_set_key_length@PLT
	testl	%eax, %eax
	jne	.L71
.L34:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	cmpl	$-1, 136(%rdi)
	je	.L34
	movq	(%rdi), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	zero_iv.10479(%rip), %r8
	call	EVP_EncryptInit_ex@PLT
	testl	%eax, %eax
	je	.L34
	movq	(%rbx), %rdi
	call	EVP_CIPHER_CTX_block_size@PLT
	leaq	72(%rbx), %rdi
	xorl	%esi, %esi
	movslq	%eax, %rdx
	call	memset@PLT
	movl	$0, 136(%rbx)
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L71:
	movq	(%rbx), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	zero_iv.10479(%rip), %r8
	movq	%r12, %rcx
	call	EVP_EncryptInit_ex@PLT
	testl	%eax, %eax
	je	.L34
	movq	(%rbx), %rdi
	leaq	72(%rbx), %r12
	call	EVP_CIPHER_CTX_block_size@PLT
	movq	(%rbx), %rdi
	leaq	zero_iv.10479(%rip), %rdx
	movq	%r12, %rsi
	movslq	%eax, %r13
	movl	%r13d, %ecx
	call	EVP_Cipher@PLT
	testl	%eax, %eax
	je	.L34
	movzbl	72(%rbx), %eax
	leal	-1(%r13), %esi
	leaq	8(%rbx), %r9
	movl	%eax, %r8d
	shrb	$7, %r8b
	testl	%esi, %esi
	jle	.L39
	leal	-2(%r13), %ecx
	movq	%r9, %rdx
	leaq	9(%rbx,%rcx), %r10
	.p2align 4,,10
	.p2align 3
.L40:
	leal	(%rax,%rax), %edi
	movzbl	65(%rdx), %eax
	addq	$1, %rdx
	movl	%eax, %ecx
	shrb	$7, %cl
	orl	%edi, %ecx
	movb	%cl, -1(%rdx)
	cmpq	%rdx, %r10
	jne	.L40
	movslq	%esi, %rdx
	addq	%rdx, %r9
.L39:
	addl	%eax, %eax
	negl	%r8d
	leaq	40(%rbx), %r10
	cmpl	$16, %r13d
	je	.L41
	andl	$27, %r8d
	xorl	%r8d, %eax
	movb	%al, (%r9)
	movzbl	8(%rbx), %edi
	movl	$27, %r9d
	movl	%edi, %r8d
	shrb	$7, %r8b
	testl	%esi, %esi
	jle	.L42
.L44:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L43:
	movzbl	%dil, %edx
	movzbl	9(%rbx,%rax), %edi
	leal	(%rdx,%rdx), %ecx
	movl	%edi, %edx
	shrb	$7, %dl
	orl	%ecx, %edx
	movb	%dl, 40(%rbx,%rax)
	addq	$1, %rax
	cmpl	%eax, %esi
	jg	.L43
	cmpl	$1, %r13d
	movl	$1, %eax
	cmovle	%eax, %esi
	movslq	%esi, %rsi
	addq	%rsi, %r10
.L42:
	movl	%r8d, %edx
	movq	%r13, %rsi
	negl	%edx
	andl	%edx, %r9d
	leal	(%rdi,%rdi), %edx
	movq	%r12, %rdi
	xorl	%r9d, %edx
	movb	%dl, (%r10)
	call	OPENSSL_cleanse@PLT
	movq	(%rbx), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	zero_iv.10479(%rip), %r8
	call	EVP_EncryptInit_ex@PLT
	testl	%eax, %eax
	je	.L34
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	memset@PLT
	movl	$1, %eax
	movl	$0, 136(%rbx)
	jmp	.L29
.L41:
	andl	$-121, %r8d
	xorl	%r8d, %eax
	movb	%al, (%r9)
	movzbl	8(%rbx), %edi
	movl	$-121, %r9d
	movl	%edi, %r8d
	shrb	$7, %r8b
	jmp	.L44
	.cfi_endproc
.LFE425:
	.size	CMAC_Init, .-CMAC_Init
	.p2align 4
	.globl	CMAC_Update
	.type	CMAC_Update, @function
CMAC_Update:
.LFB426:
	.cfi_startproc
	endbr64
	cmpl	$-1, 136(%rdi)
	je	.L98
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	testq	%rdx, %rdx
	jne	.L75
.L97:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	movq	%rdi, %r12
	movq	(%rdi), %rdi
	movq	%rsi, %r15
	call	EVP_CIPHER_CTX_block_size@PLT
	movl	%eax, -52(%rbp)
	movslq	%eax, %r13
	movslq	136(%r12), %rax
	testl	%eax, %eax
	jg	.L76
.L81:
	leaq	72(%r12), %r14
	cmpq	%rbx, %r13
	jb	.L82
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L99:
	subq	%r13, %rbx
	addq	%r13, %r15
	cmpq	%rbx, %r13
	jnb	.L78
.L82:
	movl	-52(%rbp), %ecx
	movq	(%r12), %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	EVP_Cipher@PLT
	testl	%eax, %eax
	jne	.L99
	xorl	%eax, %eax
.L100:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	movq	%r13, %r14
	leaq	104(%r12), %r9
	movq	%r15, %rsi
	subq	%rax, %r14
	leaq	(%r9,%rax), %rdi
	movq	%r9, -64(%rbp)
	cmpq	%r14, %rbx
	cmovbe	%rbx, %r14
	movq	%r14, %rdx
	call	memcpy@PLT
	addl	%r14d, 136(%r12)
	subq	%r14, %rbx
	je	.L97
	movq	-64(%rbp), %r9
	movl	-52(%rbp), %ecx
	leaq	72(%r12), %rsi
	addq	%r14, %r15
	movq	(%r12), %rdi
	movq	%r9, %rdx
	call	EVP_Cipher@PLT
	testl	%eax, %eax
	jne	.L81
	xorl	%eax, %eax
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L78:
	leaq	104(%r12), %rdi
	movq	%rbx, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movl	%ebx, 136(%r12)
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE426:
	.size	CMAC_Update, .-CMAC_Update
	.p2align 4
	.globl	CMAC_Final
	.type	CMAC_Final, @function
CMAC_Final:
.LFB427:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$-1, 136(%rdi)
	je	.L140
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rdi
	call	EVP_CIPHER_CTX_block_size@PLT
	movslq	%eax, %r15
	movq	%r15, (%r14)
	movq	%r15, %r13
	testq	%r12, %r12
	je	.L117
	movl	136(%rbx), %eax
	cmpl	%eax, %r15d
	je	.L143
	movl	%r15d, %edi
	movslq	%eax, %rcx
	subl	%eax, %edi
	movb	$-128, 104(%rbx,%rcx)
	movl	%edi, %eax
	cmpl	$1, %edi
	jle	.L111
	subl	$1, %eax
	leaq	105(%rbx,%rcx), %rdi
	xorl	%esi, %esi
	movslq	%eax, %rdx
	call	memset@PLT
.L111:
	testl	%r13d, %r13d
	jle	.L105
	leaq	16(%r12), %rcx
	leaq	40(%rbx), %rdx
	cmpq	%rdx, %rcx
	leaq	120(%rbx), %rdx
	leal	-1(%r13), %eax
	setbe	%cl
	cmpq	%rdx, %r12
	setnb	%dl
	orb	%dl, %cl
	je	.L112
	cmpl	$14, %eax
	jbe	.L112
	movdqu	104(%rbx), %xmm0
	movdqu	40(%rbx), %xmm1
	movl	%r13d, %eax
	shrl	$4, %eax
	pxor	%xmm1, %xmm0
	movups	%xmm0, (%r12)
	cmpl	$1, %eax
	jne	.L144
.L113:
	movl	%r13d, %eax
	andl	$-16, %eax
	testb	$15, %r13b
	je	.L105
	movslq	%eax, %rcx
	movl	%eax, %esi
	movzbl	104(%rbx,%rcx), %edx
	xorb	40(%rbx,%rcx), %dl
	movb	%dl, (%r12,%rsi)
	leal	1(%rax), %edx
	cmpl	%edx, %r13d
	jle	.L105
	movslq	%edx, %rdx
	movzbl	104(%rbx,%rdx), %ecx
	xorb	40(%rbx,%rdx), %cl
	movb	%cl, (%r12,%rdx)
	leal	2(%rax), %edx
	cmpl	%edx, %r13d
	jle	.L105
	movslq	%edx, %rdx
	movzbl	104(%rbx,%rdx), %ecx
	xorb	40(%rbx,%rdx), %cl
	movb	%cl, (%r12,%rdx)
	leal	3(%rax), %edx
	cmpl	%edx, %r13d
	jle	.L105
	movslq	%edx, %rdx
	movzbl	104(%rbx,%rdx), %ecx
	xorb	40(%rbx,%rdx), %cl
	movb	%cl, (%r12,%rdx)
	leal	4(%rax), %edx
	cmpl	%edx, %r13d
	jle	.L105
	movslq	%edx, %rdx
	movzbl	104(%rbx,%rdx), %ecx
	xorb	40(%rbx,%rdx), %cl
	movb	%cl, (%r12,%rdx)
	leal	5(%rax), %edx
	cmpl	%edx, %r13d
	jle	.L105
	movslq	%edx, %rdx
	movzbl	104(%rbx,%rdx), %ecx
	xorb	40(%rbx,%rdx), %cl
	movb	%cl, (%r12,%rdx)
	leal	6(%rax), %edx
	cmpl	%edx, %r13d
	jle	.L105
	movslq	%edx, %rdx
	movzbl	104(%rbx,%rdx), %ecx
	xorb	40(%rbx,%rdx), %cl
	movb	%cl, (%r12,%rdx)
	leal	7(%rax), %edx
	cmpl	%edx, %r13d
	jle	.L105
	movslq	%edx, %rdx
	movzbl	104(%rbx,%rdx), %ecx
	xorb	40(%rbx,%rdx), %cl
	movb	%cl, (%r12,%rdx)
	leal	8(%rax), %edx
	cmpl	%edx, %r13d
	jle	.L105
	movslq	%edx, %rdx
	movzbl	104(%rbx,%rdx), %ecx
	xorb	40(%rbx,%rdx), %cl
	movb	%cl, (%r12,%rdx)
	leal	9(%rax), %edx
	cmpl	%edx, %r13d
	jle	.L105
	movslq	%edx, %rdx
	movzbl	104(%rbx,%rdx), %ecx
	xorb	40(%rbx,%rdx), %cl
	movb	%cl, (%r12,%rdx)
	leal	10(%rax), %edx
	cmpl	%edx, %r13d
	jle	.L105
	movslq	%edx, %rdx
	movzbl	104(%rbx,%rdx), %ecx
	xorb	40(%rbx,%rdx), %cl
	movb	%cl, (%r12,%rdx)
	leal	11(%rax), %edx
	cmpl	%edx, %r13d
	jle	.L105
	movslq	%edx, %rdx
	movzbl	104(%rbx,%rdx), %ecx
	xorb	40(%rbx,%rdx), %cl
	movb	%cl, (%r12,%rdx)
	leal	12(%rax), %edx
	cmpl	%edx, %r13d
	jle	.L105
	movslq	%edx, %rdx
	movzbl	104(%rbx,%rdx), %ecx
	xorb	40(%rbx,%rdx), %cl
	movb	%cl, (%r12,%rdx)
	leal	13(%rax), %edx
	cmpl	%edx, %r13d
	jle	.L105
	movslq	%edx, %rdx
	addl	$14, %eax
	movzbl	104(%rbx,%rdx), %ecx
	xorb	40(%rbx,%rdx), %cl
	movb	%cl, (%r12,%rdx)
	cmpl	%eax, %r13d
	jle	.L105
	cltq
	movzbl	104(%rbx,%rax), %edx
	xorb	40(%rbx,%rax), %dl
	movb	%dl, (%r12,%rax)
	.p2align 4,,10
	.p2align 3
.L105:
	movq	(%rbx), %rdi
	movl	%r13d, %ecx
	movq	%r12, %rdx
	movq	%r12, %rsi
	call	EVP_Cipher@PLT
	testl	%eax, %eax
	je	.L145
.L117:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	testl	%r15d, %r15d
	jle	.L105
	leaq	8(%rbx), %rsi
	leaq	16(%r12), %rcx
	cmpq	%rcx, %rsi
	leaq	120(%rbx), %rcx
	leal	-1(%r15), %edx
	setnb	%sil
	cmpq	%rcx, %r12
	setnb	%cl
	orb	%cl, %sil
	je	.L106
	cmpl	$14, %edx
	jbe	.L106
	movdqu	104(%rbx), %xmm0
	movdqu	8(%rbx), %xmm2
	movl	%r15d, %edx
	shrl	$4, %edx
	pxor	%xmm2, %xmm0
	movups	%xmm0, (%r12)
	cmpl	$1, %edx
	je	.L107
	movdqu	120(%rbx), %xmm0
	movdqu	24(%rbx), %xmm4
	pxor	%xmm4, %xmm0
	movups	%xmm0, 16(%r12)
.L107:
	movl	%r13d, %edx
	andl	$-16, %edx
	testb	$15, %r13b
	je	.L105
	movslq	%edx, %rsi
	movl	%edx, %edi
	movzbl	104(%rbx,%rsi), %ecx
	xorb	8(%rbx,%rsi), %cl
	movb	%cl, (%r12,%rdi)
	leal	1(%rdx), %ecx
	cmpl	%eax, %ecx
	jge	.L105
	movslq	%ecx, %rcx
	movzbl	104(%rbx,%rcx), %esi
	xorb	8(%rbx,%rcx), %sil
	movb	%sil, (%r12,%rcx)
	leal	2(%rdx), %ecx
	cmpl	%ecx, %eax
	jle	.L105
	movslq	%ecx, %rcx
	movzbl	104(%rbx,%rcx), %esi
	xorb	8(%rbx,%rcx), %sil
	movb	%sil, (%r12,%rcx)
	leal	3(%rdx), %ecx
	cmpl	%ecx, %eax
	jle	.L105
	movslq	%ecx, %rcx
	movzbl	104(%rbx,%rcx), %esi
	xorb	8(%rbx,%rcx), %sil
	movb	%sil, (%r12,%rcx)
	leal	4(%rdx), %ecx
	cmpl	%ecx, %eax
	jle	.L105
	movslq	%ecx, %rcx
	movzbl	104(%rbx,%rcx), %esi
	xorb	8(%rbx,%rcx), %sil
	movb	%sil, (%r12,%rcx)
	leal	5(%rdx), %ecx
	cmpl	%ecx, %eax
	jle	.L105
	movslq	%ecx, %rcx
	movzbl	104(%rbx,%rcx), %esi
	xorb	8(%rbx,%rcx), %sil
	movb	%sil, (%r12,%rcx)
	leal	6(%rdx), %ecx
	cmpl	%ecx, %eax
	jle	.L105
	movslq	%ecx, %rcx
	movzbl	104(%rbx,%rcx), %esi
	xorb	8(%rbx,%rcx), %sil
	movb	%sil, (%r12,%rcx)
	leal	7(%rdx), %ecx
	cmpl	%ecx, %eax
	jle	.L105
	movslq	%ecx, %rcx
	movzbl	104(%rbx,%rcx), %esi
	xorb	8(%rbx,%rcx), %sil
	movb	%sil, (%r12,%rcx)
	leal	8(%rdx), %ecx
	cmpl	%ecx, %eax
	jle	.L105
	movslq	%ecx, %rcx
	movzbl	104(%rbx,%rcx), %esi
	xorb	8(%rbx,%rcx), %sil
	movb	%sil, (%r12,%rcx)
	leal	9(%rdx), %ecx
	cmpl	%ecx, %eax
	jle	.L105
	movslq	%ecx, %rcx
	movzbl	104(%rbx,%rcx), %esi
	xorb	8(%rbx,%rcx), %sil
	movb	%sil, (%r12,%rcx)
	leal	10(%rdx), %ecx
	cmpl	%ecx, %eax
	jle	.L105
	movslq	%ecx, %rcx
	movzbl	104(%rbx,%rcx), %esi
	xorb	8(%rbx,%rcx), %sil
	movb	%sil, (%r12,%rcx)
	leal	11(%rdx), %ecx
	cmpl	%ecx, %eax
	jle	.L105
	movslq	%ecx, %rcx
	movzbl	104(%rbx,%rcx), %esi
	xorb	8(%rbx,%rcx), %sil
	movb	%sil, (%r12,%rcx)
	leal	12(%rdx), %ecx
	cmpl	%ecx, %eax
	jle	.L105
	movslq	%ecx, %rcx
	movzbl	104(%rbx,%rcx), %esi
	xorb	8(%rbx,%rcx), %sil
	movb	%sil, (%r12,%rcx)
	leal	13(%rdx), %ecx
	cmpl	%ecx, %eax
	jle	.L105
	movslq	%ecx, %rcx
	addl	$14, %edx
	movzbl	104(%rbx,%rcx), %esi
	xorb	8(%rbx,%rcx), %sil
	movb	%sil, (%r12,%rcx)
	cmpl	%edx, %eax
	jle	.L105
	movslq	%edx, %rax
	movzbl	104(%rbx,%rax), %edx
	xorb	8(%rbx,%rax), %dl
	movb	%dl, (%r12,%rax)
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L144:
	movdqu	120(%rbx), %xmm0
	movdqu	56(%rbx), %xmm3
	pxor	%xmm3, %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L145:
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, -52(%rbp)
	call	OPENSSL_cleanse@PLT
	movl	-52(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	movl	%eax, %ecx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L115:
	movzbl	104(%rbx,%rax), %edx
	xorb	40(%rbx,%rax), %dl
	movb	%dl, (%r12,%rax)
	movq	%rax, %rdx
	addq	$1, %rax
	cmpq	%rcx, %rdx
	jne	.L115
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L106:
	movl	%edx, %ecx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L109:
	movzbl	8(%rbx,%rax), %edx
	xorb	104(%rbx,%rax), %dl
	movb	%dl, (%r12,%rax)
	movq	%rax, %rdx
	addq	$1, %rax
	cmpq	%rdx, %rcx
	jne	.L109
	jmp	.L105
	.cfi_endproc
.LFE427:
	.size	CMAC_Final, .-CMAC_Final
	.p2align 4
	.globl	CMAC_resume
	.type	CMAC_resume, @function
CMAC_resume:
.LFB428:
	.cfi_startproc
	endbr64
	cmpl	$-1, 136(%rdi)
	je	.L147
	leaq	72(%rdi), %r8
	movq	(%rdi), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	jmp	EVP_EncryptInit_ex@PLT
	.p2align 4,,10
	.p2align 3
.L147:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE428:
	.size	CMAC_resume, .-CMAC_resume
	.section	.rodata
	.align 32
	.type	zero_iv.10479, @object
	.size	zero_iv.10479, 32
zero_iv.10479:
	.zero	32
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
