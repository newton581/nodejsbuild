	.file	"store_register.c"
	.text
	.p2align 4
	.type	store_loader_cmp, @function
store_loader_cmp:
.LFB818:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rsi
	movq	(%rdi), %rdi
	jmp	strcmp@PLT
	.cfi_endproc
.LFE818:
	.size	store_loader_cmp, .-store_loader_cmp
	.p2align 4
	.type	store_loader_hash, @function
store_loader_hash:
.LFB817:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	jmp	OPENSSL_LH_strhash@PLT
	.cfi_endproc
.LFE817:
	.size	store_loader_hash, .-store_loader_hash
	.p2align 4
	.type	do_registry_init_ossl_, @function
do_registry_init_ossl_:
.LFB803:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	CRYPTO_THREAD_lock_new@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	movq	%rax, registry_lock(%rip)
	setne	%al
	movzbl	%al, %eax
	movl	%eax, do_registry_init_ossl_ret_(%rip)
	ret
	.cfi_endproc
.LFE803:
	.size	do_registry_init_ossl_, .-do_registry_init_ossl_
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/store/store_register.c"
	.text
	.p2align 4
	.globl	OSSL_STORE_LOADER_new
	.type	OSSL_STORE_LOADER_new, @function
OSSL_STORE_LOADER_new:
.LFB805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -24(%rbp)
	testq	%rsi, %rsi
	je	.L11
	movq	%rsi, %rbx
	movl	$47, %edx
	movl	$80, %edi
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L12
	movq	%rbx, %xmm0
	movhps	-24(%rbp), %xmm0
	movups	%xmm0, (%rax)
.L6:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movl	$42, %r8d
	movl	$106, %edx
	movl	$113, %esi
	movl	$44, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$48, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$113, %esi
	movl	$44, %edi
	movq	%rax, -24(%rbp)
	call	ERR_put_error@PLT
	movq	-24(%rbp), %rax
	jmp	.L6
	.cfi_endproc
.LFE805:
	.size	OSSL_STORE_LOADER_new, .-OSSL_STORE_LOADER_new
	.p2align 4
	.globl	OSSL_STORE_LOADER_get0_engine
	.type	OSSL_STORE_LOADER_get0_engine, @function
OSSL_STORE_LOADER_get0_engine:
.LFB806:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE806:
	.size	OSSL_STORE_LOADER_get0_engine, .-OSSL_STORE_LOADER_get0_engine
	.p2align 4
	.globl	OSSL_STORE_LOADER_get0_scheme
	.type	OSSL_STORE_LOADER_get0_scheme, @function
OSSL_STORE_LOADER_get0_scheme:
.LFB807:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE807:
	.size	OSSL_STORE_LOADER_get0_scheme, .-OSSL_STORE_LOADER_get0_scheme
	.p2align 4
	.globl	OSSL_STORE_LOADER_set_open
	.type	OSSL_STORE_LOADER_set_open, @function
OSSL_STORE_LOADER_set_open:
.LFB808:
	.cfi_startproc
	endbr64
	movq	%rsi, 16(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE808:
	.size	OSSL_STORE_LOADER_set_open, .-OSSL_STORE_LOADER_set_open
	.p2align 4
	.globl	OSSL_STORE_LOADER_set_ctrl
	.type	OSSL_STORE_LOADER_set_ctrl, @function
OSSL_STORE_LOADER_set_ctrl:
.LFB809:
	.cfi_startproc
	endbr64
	movq	%rsi, 24(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE809:
	.size	OSSL_STORE_LOADER_set_ctrl, .-OSSL_STORE_LOADER_set_ctrl
	.p2align 4
	.globl	OSSL_STORE_LOADER_set_expect
	.type	OSSL_STORE_LOADER_set_expect, @function
OSSL_STORE_LOADER_set_expect:
.LFB810:
	.cfi_startproc
	endbr64
	movq	%rsi, 32(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE810:
	.size	OSSL_STORE_LOADER_set_expect, .-OSSL_STORE_LOADER_set_expect
	.p2align 4
	.globl	OSSL_STORE_LOADER_set_find
	.type	OSSL_STORE_LOADER_set_find, @function
OSSL_STORE_LOADER_set_find:
.LFB811:
	.cfi_startproc
	endbr64
	movq	%rsi, 40(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE811:
	.size	OSSL_STORE_LOADER_set_find, .-OSSL_STORE_LOADER_set_find
	.p2align 4
	.globl	OSSL_STORE_LOADER_set_load
	.type	OSSL_STORE_LOADER_set_load, @function
OSSL_STORE_LOADER_set_load:
.LFB812:
	.cfi_startproc
	endbr64
	movq	%rsi, 48(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE812:
	.size	OSSL_STORE_LOADER_set_load, .-OSSL_STORE_LOADER_set_load
	.p2align 4
	.globl	OSSL_STORE_LOADER_set_eof
	.type	OSSL_STORE_LOADER_set_eof, @function
OSSL_STORE_LOADER_set_eof:
.LFB813:
	.cfi_startproc
	endbr64
	movq	%rsi, 56(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE813:
	.size	OSSL_STORE_LOADER_set_eof, .-OSSL_STORE_LOADER_set_eof
	.p2align 4
	.globl	OSSL_STORE_LOADER_set_error
	.type	OSSL_STORE_LOADER_set_error, @function
OSSL_STORE_LOADER_set_error:
.LFB814:
	.cfi_startproc
	endbr64
	movq	%rsi, 64(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE814:
	.size	OSSL_STORE_LOADER_set_error, .-OSSL_STORE_LOADER_set_error
	.p2align 4
	.globl	OSSL_STORE_LOADER_set_close
	.type	OSSL_STORE_LOADER_set_close, @function
OSSL_STORE_LOADER_set_close:
.LFB815:
	.cfi_startproc
	endbr64
	movq	%rsi, 72(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE815:
	.size	OSSL_STORE_LOADER_set_close, .-OSSL_STORE_LOADER_set_close
	.p2align 4
	.globl	OSSL_STORE_LOADER_free
	.type	OSSL_STORE_LOADER_free, @function
OSSL_STORE_LOADER_free:
.LFB816:
	.cfi_startproc
	endbr64
	movl	$125, %edx
	leaq	.LC0(%rip), %rsi
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE816:
	.size	OSSL_STORE_LOADER_free, .-OSSL_STORE_LOADER_free
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"+-."
.LC2:
	.string	"scheme="
	.text
	.p2align 4
	.globl	ossl_store_register_loader_int
	.type	ossl_store_register_loader_int, @function
ossl_store_register_loader_int:
.LFB819:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rbx
	movsbl	(%rbx), %edi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	jne	.L25
	movzbl	(%rbx), %r14d
.L26:
	testb	%r14b, %r14b
	jne	.L54
.L30:
	cmpq	$0, 16(%r12)
	je	.L32
	cmpq	$0, 48(%r12)
	je	.L32
	cmpq	$0, 56(%r12)
	je	.L32
	cmpq	$0, 64(%r12)
	je	.L32
	cmpq	$0, 72(%r12)
	je	.L32
	leaq	do_registry_init_ossl_(%rip), %rsi
	leaq	registry_init(%rip), %rdi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L34
	movl	do_registry_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	jne	.L35
.L34:
	xorl	%r12d, %r12d
	movl	$179, %r8d
	movl	$65, %edx
	movl	$117, %esi
	leaq	.LC0(%rip), %rcx
	movl	$44, %edi
	call	ERR_put_error@PLT
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	xorl	%r12d, %r12d
	movl	$173, %r8d
	movl	$116, %edx
	movl	$117, %esi
	leaq	.LC0(%rip), %rcx
	movl	$44, %edi
	call	ERR_put_error@PLT
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	movsbl	(%rbx), %edi
	leaq	.LC1(%rip), %r13
	testb	%dil, %dil
	je	.L30
	movl	$3, %esi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	je	.L55
	.p2align 4,,10
	.p2align 3
.L29:
	movsbl	1(%rbx), %edi
	addq	$1, %rbx
	testb	%dil, %dil
	je	.L30
	movl	$3, %esi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	jne	.L29
.L55:
	movsbl	(%rbx), %edi
	movl	$4, %esi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	jne	.L29
	movsbl	(%rbx), %esi
	movq	%r13, %rdi
	movl	%esi, %r14d
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L29
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L54:
	movl	$164, %r8d
	movl	$106, %edx
	movl	$117, %esi
	movl	$44, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	(%r12), %rdx
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	leaq	.LC2(%rip), %rsi
	movl	$2, %edi
	call	ERR_add_error_data@PLT
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movq	registry_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	loader_register(%rip), %rdi
	testq	%rdi, %rdi
	je	.L56
.L36:
	movq	%r12, %rsi
	movl	$1, %r12d
	call	OPENSSL_LH_insert@PLT
	testq	%rax, %rax
	je	.L57
.L37:
	movq	registry_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L57:
	.cfi_restore_state
	movq	loader_register(%rip), %rdi
	xorl	%r12d, %r12d
	call	OPENSSL_LH_error@PLT
	testl	%eax, %eax
	sete	%r12b
	jmp	.L37
.L56:
	leaq	store_loader_hash(%rip), %rdi
	leaq	store_loader_cmp(%rip), %rsi
	call	OPENSSL_LH_new@PLT
	movq	%rax, loader_register(%rip)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L36
	xorl	%r12d, %r12d
	jmp	.L37
	.cfi_endproc
.LFE819:
	.size	ossl_store_register_loader_int, .-ossl_store_register_loader_int
	.p2align 4
	.globl	OSSL_STORE_register_loader
	.type	OSSL_STORE_register_loader, @function
OSSL_STORE_register_loader:
.LFB820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	ossl_store_init_once@PLT
	testl	%eax, %eax
	jne	.L61
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	ossl_store_register_loader_int
	.cfi_endproc
.LFE820:
	.size	OSSL_STORE_register_loader, .-OSSL_STORE_register_loader
	.p2align 4
	.globl	ossl_store_get0_loader_int
	.type	ossl_store_get0_loader_int, @function
ossl_store_get0_loader_int:
.LFB821:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -112(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -40(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	ossl_store_init_once@PLT
	testl	%eax, %eax
	je	.L62
	leaq	do_registry_init_ossl_(%rip), %rsi
	leaq	registry_init(%rip), %rdi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L64
	movl	do_registry_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	je	.L64
	movq	registry_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	loader_register(%rip), %rdi
	leaq	-112(%rbp), %rsi
	call	OPENSSL_LH_retrieve@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L74
.L66:
	movq	registry_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
.L62:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L75
	addq	$96, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
	movl	$221, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$100, %esi
	movl	$44, %edi
	call	ERR_put_error@PLT
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L74:
	movl	$230, %r8d
	movl	$105, %edx
	movl	$100, %esi
	movl	$44, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdx
	movl	$2, %edi
	xorl	%eax, %eax
	leaq	.LC2(%rip), %rsi
	call	ERR_add_error_data@PLT
	jmp	.L66
.L75:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE821:
	.size	ossl_store_get0_loader_int, .-ossl_store_get0_loader_int
	.p2align 4
	.globl	ossl_store_unregister_loader_int
	.type	ossl_store_unregister_loader_int, @function
ossl_store_unregister_loader_int:
.LFB822:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	leaq	do_registry_init_ossl_(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -112(%rbp)
	leaq	registry_init(%rip), %rdi
	movq	$0, -96(%rbp)
	movq	$0, -40(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L77
	movl	do_registry_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	je	.L77
	movq	registry_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	loader_register(%rip), %rdi
	leaq	-112(%rbp), %rsi
	call	OPENSSL_LH_delete@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L86
.L80:
	movq	registry_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
.L76:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L87
	addq	$96, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	movl	$252, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$116, %esi
	movl	$44, %edi
	call	ERR_put_error@PLT
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L86:
	movl	$261, %r8d
	movl	$105, %edx
	movl	$116, %esi
	movl	$44, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdx
	movl	$2, %edi
	xorl	%eax, %eax
	leaq	.LC2(%rip), %rsi
	call	ERR_add_error_data@PLT
	jmp	.L80
.L87:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE822:
	.size	ossl_store_unregister_loader_int, .-ossl_store_unregister_loader_int
	.p2align 4
	.globl	OSSL_STORE_unregister_loader
	.type	OSSL_STORE_unregister_loader, @function
OSSL_STORE_unregister_loader:
.LFB823:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	xorl	%r12d, %r12d
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	ossl_store_init_once@PLT
	testl	%eax, %eax
	je	.L88
	pxor	%xmm0, %xmm0
	leaq	do_registry_init_ossl_(%rip), %rsi
	movq	%r13, -112(%rbp)
	leaq	registry_init(%rip), %rdi
	movq	$0, -96(%rbp)
	movq	$0, -40(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L90
	movl	do_registry_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	je	.L90
	movq	registry_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	loader_register(%rip), %rdi
	leaq	-112(%rbp), %rsi
	call	OPENSSL_LH_delete@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L101
.L93:
	movq	registry_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
.L88:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L102
	addq	$96, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	movl	$252, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$116, %esi
	movl	$44, %edi
	call	ERR_put_error@PLT
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L101:
	movl	$261, %r8d
	movl	$105, %edx
	movl	$116, %esi
	movl	$44, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r13, %rdx
	movl	$2, %edi
	xorl	%eax, %eax
	leaq	.LC2(%rip), %rsi
	call	ERR_add_error_data@PLT
	jmp	.L93
.L102:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE823:
	.size	OSSL_STORE_unregister_loader, .-OSSL_STORE_unregister_loader
	.p2align 4
	.globl	ossl_store_destroy_loaders_int
	.type	ossl_store_destroy_loaders_int, @function
ossl_store_destroy_loaders_int:
.LFB824:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	loader_register(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	OPENSSL_LH_free@PLT
	movq	registry_lock(%rip), %rdi
	movq	$0, loader_register(%rip)
	call	CRYPTO_THREAD_lock_free@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	$0, registry_lock(%rip)
	ret
	.cfi_endproc
.LFE824:
	.size	ossl_store_destroy_loaders_int, .-ossl_store_destroy_loaders_int
	.p2align 4
	.globl	OSSL_STORE_do_all_loaders
	.type	OSSL_STORE_do_all_loaders, @function
OSSL_STORE_do_all_loaders:
.LFB826:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	loader_register(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	OPENSSL_LH_doall_arg@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE826:
	.size	OSSL_STORE_do_all_loaders, .-OSSL_STORE_do_all_loaders
	.local	loader_register
	.comm	loader_register,8,8
	.local	do_registry_init_ossl_ret_
	.comm	do_registry_init_ossl_ret_,4,4
	.local	registry_init
	.comm	registry_init,4,4
	.local	registry_lock
	.comm	registry_lock,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
