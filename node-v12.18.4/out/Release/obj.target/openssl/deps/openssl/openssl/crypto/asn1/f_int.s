	.file	"f_int.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"-"
.LC1:
	.string	"00"
.LC2:
	.string	"\\\n"
.LC3:
	.string	"0123456789ABCDEF"
	.text
	.p2align 4
	.globl	i2a_ASN1_INTEGER
	.type	i2a_ASN1_INTEGER, @function
i2a_ASN1_INTEGER:
.LFB419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L10
	movl	4(%rsi), %ebx
	movq	%rdi, %r13
	movq	%rsi, %r15
	movl	$2, %r8d
	andl	$256, %ebx
	jne	.L20
.L3:
	movl	(%r15), %eax
	testl	%eax, %eax
	je	.L5
	leaq	-58(%rbp), %rax
	movl	$0, %r14d
	leaq	.LC3(%rip), %r12
	movl	%ebx, %r8d
	movq	%rax, -72(%rbp)
	jg	.L6
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L7:
	addq	$1, %r14
	movl	%r8d, %ebx
.L6:
	movq	8(%r15), %rdx
	movq	-72(%rbp), %rsi
	movq	%r13, %rdi
	addq	%r14, %rdx
	movzbl	(%rdx), %eax
	shrb	$4, %al
	movzbl	%al, %eax
	movzbl	(%r12,%rax), %eax
	movb	%al, -58(%rbp)
	movzbl	(%rdx), %eax
	movl	$2, %edx
	andl	$15, %eax
	movzbl	(%r12,%rax), %eax
	movb	%al, -57(%rbp)
	call	BIO_write@PLT
	cmpl	$2, %eax
	jne	.L4
	leal	2(%rbx), %r8d
	leal	1(%r14), %eax
	cmpl	%eax, (%r15)
	jle	.L1
	imull	$-1963413621, %eax, %eax
	addl	$61356675, %eax
	cmpl	$122713350, %eax
	ja	.L7
	movl	$2, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_write@PLT
	cmpl	$2, %eax
	jne	.L4
	leal	4(%rbx), %r8d
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L21
	addq	$40, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	movl	$2, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	movl	%r8d, -72(%rbp)
	call	BIO_write@PLT
	movl	-72(%rbp), %r8d
	cmpl	$2, %eax
	je	.L1
	.p2align 4,,10
	.p2align 3
.L4:
	movl	$-1, %r8d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$1, %edx
	leaq	.LC0(%rip), %rsi
	call	BIO_write@PLT
	movl	%eax, %ebx
	cmpl	$1, %eax
	jne	.L4
	movl	$3, %r8d
	jmp	.L3
.L21:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE419:
	.size	i2a_ASN1_INTEGER, .-i2a_ASN1_INTEGER
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"../deps/openssl/openssl/crypto/asn1/f_int.c"
	.text
	.p2align 4
	.globl	a2i_ASN1_INTEGER
	.type	a2i_ASN1_INTEGER, @function
a2i_ASN1_INTEGER:
.LFB420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movl	%ecx, %edx
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movl	$2, 4(%rsi)
	movq	%rsi, -96(%rbp)
	movq	%r12, %rsi
	movq	%rdi, -88(%rbp)
	movl	%ecx, -80(%rbp)
	call	BIO_gets@PLT
	testl	%eax, %eax
	jle	.L40
	movl	$1, -72(%rbp)
	movl	%eax, %ebx
	movl	$0, -68(%rbp)
	movl	$0, -56(%rbp)
	movq	$0, -64(%rbp)
.L39:
	movslq	%ebx, %r13
	movzbl	-1(%r12,%r13), %eax
	movb	%al, -49(%rbp)
	cmpb	$10, %al
	je	.L60
	cmpb	$13, -49(%rbp)
	je	.L61
.L25:
	xorl	%eax, %eax
	cmpb	$92, -49(%rbp)
	movq	%r12, %r14
	sete	%al
	xorl	%r15d, %r15d
	movl	%eax, -76(%rbp)
	.p2align 4,,10
	.p2align 3
.L27:
	movsbl	(%r14), %edi
	movl	$16, %esi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	je	.L26
	addl	$1, %r15d
	addq	$1, %r14
	cmpl	%ebx, %r15d
	jl	.L27
	leaq	(%r12,%r13), %r14
	movl	%ebx, %r15d
.L26:
	movb	$0, (%r14)
	cmpl	$1, %r15d
	jle	.L23
	movl	-72(%rbp), %eax
	movq	%r12, %rbx
	testl	%eax, %eax
	je	.L28
	cmpb	$48, (%r12)
	je	.L62
.L28:
	movl	%r15d, %ecx
	subl	-76(%rbp), %ecx
	testb	$1, %cl
	jne	.L63
	movl	-56(%rbp), %eax
	sarl	%ecx
	movl	%ecx, %r15d
	leal	(%rax,%rcx), %r14d
	cmpl	-68(%rbp), %r14d
	jg	.L64
.L31:
	testl	%r15d, %r15d
	je	.L38
	movslq	-56(%rbp), %rax
	movq	-64(%rbp), %rsi
	leal	-1(%r15), %ecx
	leaq	1(%rax,%rcx), %r15
	leaq	(%rsi,%rax), %r13
	addq	%rsi, %r15
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L65:
	movzbl	0(%r13), %eax
	sall	$4, %eax
	orl	%edx, %eax
	movb	%al, 0(%r13)
	movzbl	1(%rbx), %edi
	call	OPENSSL_hexchar2int@PLT
	movl	%eax, %edx
	testl	%eax, %eax
	js	.L35
	movzbl	0(%r13), %eax
	addq	$1, %r13
	addq	$2, %rbx
	sall	$4, %eax
	orl	%edx, %eax
	movb	%al, -1(%r13)
	cmpq	%r13, %r15
	je	.L38
.L37:
	movzbl	(%rbx), %edi
	call	OPENSSL_hexchar2int@PLT
	movl	%eax, %edx
	testl	%eax, %eax
	jns	.L65
.L35:
	movl	$122, %r8d
	movl	$141, %edx
	movl	$102, %esi
	movl	$13, %edi
	leaq	.LC4(%rip), %rcx
	call	ERR_put_error@PLT
.L23:
	movl	$140, %r8d
	movl	$150, %edx
	movl	$102, %esi
	movl	$13, %edi
	leaq	.LC4(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-64(%rbp), %rdi
	movl	$141, %edx
	leaq	.LC4(%rip), %rsi
	call	CRYPTO_free@PLT
	xorl	%eax, %eax
.L22:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	cmpb	$48, 1(%r12)
	jne	.L28
	leaq	2(%r12), %rbx
	subl	$2, %r15d
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L60:
	subl	$1, %ebx
	movslq	%ebx, %r13
	movb	$0, (%r12,%r13)
	je	.L23
	movzbl	-1(%r12,%r13), %eax
	movb	%al, -49(%rbp)
	cmpb	$13, -49(%rbp)
	jne	.L25
.L61:
	subl	$1, %ebx
	movslq	%ebx, %r13
	movb	$0, (%r12,%r13)
	je	.L23
	movzbl	-1(%r12,%r13), %eax
	movb	%al, -49(%rbp)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L38:
	cmpb	$92, -49(%rbp)
	jne	.L66
	movl	-80(%rbp), %edx
	movq	-88(%rbp), %rdi
	movq	%r12, %rsi
	call	BIO_gets@PLT
	movl	$0, -72(%rbp)
	movl	%eax, %ebx
	testl	%eax, %eax
	jle	.L23
	movl	%r14d, -56(%rbp)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L64:
	leal	(%r14,%rcx), %r13d
	movslq	-68(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movl	$109, %r8d
	movslq	%r13d, %rdx
	leaq	.LC4(%rip), %rcx
	call	CRYPTO_clear_realloc@PLT
	testq	%rax, %rax
	je	.L67
	movl	%r13d, -68(%rbp)
	movq	%rax, -64(%rbp)
	jmp	.L31
.L63:
	movl	$103, %r8d
	movl	$145, %edx
	movl	$102, %esi
	movl	$13, %edi
	leaq	.LC4(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-64(%rbp), %rdi
	movl	$104, %edx
	leaq	.LC4(%rip), %rsi
	call	CRYPTO_free@PLT
	xorl	%eax, %eax
	jmp	.L22
.L40:
	movq	$0, -64(%rbp)
	jmp	.L23
.L66:
	movq	-96(%rbp), %rax
	movq	-64(%rbp), %rcx
	movl	%r14d, (%rax)
	movq	%rcx, 8(%rax)
	movl	$1, %eax
	jmp	.L22
.L67:
	movl	$111, %r8d
	movl	$65, %edx
	movl	$102, %esi
	movl	$13, %edi
	leaq	.LC4(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-64(%rbp), %rdi
	movl	$112, %edx
	leaq	.LC4(%rip), %rsi
	call	CRYPTO_free@PLT
	xorl	%eax, %eax
	jmp	.L22
	.cfi_endproc
.LFE420:
	.size	a2i_ASN1_INTEGER, .-a2i_ASN1_INTEGER
	.p2align 4
	.globl	i2a_ASN1_ENUMERATED
	.type	i2a_ASN1_ENUMERATED, @function
i2a_ASN1_ENUMERATED:
.LFB421:
	.cfi_startproc
	endbr64
	jmp	i2a_ASN1_INTEGER
	.cfi_endproc
.LFE421:
	.size	i2a_ASN1_ENUMERATED, .-i2a_ASN1_ENUMERATED
	.p2align 4
	.globl	a2i_ASN1_ENUMERATED
	.type	a2i_ASN1_ENUMERATED, @function
a2i_ASN1_ENUMERATED:
.LFB422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	a2i_ASN1_INTEGER
	cmpl	$1, %eax
	jne	.L69
	movl	4(%rbx), %edx
	andl	$256, %edx
	orl	$2, %edx
	movl	%edx, 4(%rbx)
.L69:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE422:
	.size	a2i_ASN1_ENUMERATED, .-a2i_ASN1_ENUMERATED
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
