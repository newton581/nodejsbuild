	.file	"bn_exp.c"
	.text
	.p2align 4
	.type	MOD_EXP_CTIME_COPY_FROM_PREBUF, @function
MOD_EXP_CTIME_COPY_FROM_PREBUF:
.LFB294:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	$1, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	movl	%r8d, %ecx
	pushq	%r12
	.cfi_offset 12, -48
	movl	%r15d, %r12d
	pushq	%rbx
	sall	%cl, %r12d
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$312, %rsp
	movq	%rdi, -216(%rbp)
	movl	%esi, -64(%rbp)
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L10
	cmpl	$3, %r14d
	jle	.L19
	leal	-2(%r14), %esi
	movl	%r15d, %edx
	movl	-64(%rbp), %r14d
	movl	%esi, %ecx
	movl	%esi, -60(%rbp)
	movl	%esi, %r15d
	sall	%cl, %edx
	leal	-1(%rdx), %eax
	andl	%r13d, %eax
	sarl	%cl, %r13d
	movl	%r13d, %r9d
	leal	-1(%r13), %ecx
	movl	%r13d, %esi
	movl	%r13d, %r8d
	notl	%r9d
	xorl	$1, %esi
	movl	%r13d, %edi
	xorl	$3, %r8d
	andl	%ecx, %r9d
	movl	%r13d, %ecx
	subl	$1, %esi
	subl	$1, %r8d
	xorl	$-2, %ecx
	xorl	$2, %edi
	shrl	$31, %r9d
	andl	%ecx, %esi
	movl	%r13d, %ecx
	xorl	$-4, %r13d
	subl	$1, %edi
	andl	%r8d, %r13d
	xorl	$-3, %ecx
	shrl	$31, %esi
	negq	%r9
	andl	%ecx, %edi
	shrl	$31, %r13d
	negq	%rsi
	shrl	$31, %edi
	movl	%r13d, %r8d
	negq	%rdi
	negq	%r8
	testl	%r14d, %r14d
	jle	.L7
	movslq	%r12d, %r12
	movl	%r15d, %ecx
	movl	$3, %r11d
	movl	$2, %r10d
	leaq	0(,%r12,8), %r15
	sall	%cl, %r11d
	sall	%cl, %r10d
	movl	%eax, %r12d
	movq	%r15, -72(%rbp)
	movq	-216(%rbp), %r15
	movl	%r11d, %ecx
	leal	-1(%r14), %r11d
	notl	%r12d
	movq	(%r15), %r15
	movq	%r15, -56(%rbp)
	addq	$8, %r15
	leaq	(%r15,%r11,8), %r14
	movslq	%edx, %r11
	movq	%r14, -208(%rbp)
	leaq	0(,%r11,8), %r14
	movslq	%r10d, %r11
	movq	%r14, -144(%rbp)
	leaq	0(,%r11,8), %r14
	movslq	%ecx, %r11
	movq	%r14, -136(%rbp)
	leaq	0(,%r11,8), %r14
	leal	-1(%rax), %r11d
	andl	%r12d, %r11d
	movq	%r14, -120(%rbp)
	movl	%eax, %r12d
	shrl	$31, %r11d
	xorl	$-2, %r12d
	negq	%r11
	movq	%r11, -112(%rbp)
	leal	1(%rdx), %r11d
	leaq	0(,%r11,8), %r14
	leal	1(%r10), %r11d
	movq	%r14, -104(%rbp)
	leaq	0(,%r11,8), %r14
	leal	1(%rcx), %r11d
	movq	%r14, -96(%rbp)
	leaq	0(,%r11,8), %r14
	movl	%eax, %r11d
	xorl	$1, %r11d
	movq	%r14, -80(%rbp)
	subl	$1, %r11d
	andl	%r12d, %r11d
	movl	%eax, %r12d
	shrl	$31, %r11d
	xorl	$-3, %r12d
	negq	%r11
	movq	%r11, -152(%rbp)
	leal	2(%rdx), %r11d
	leaq	0(,%r11,8), %r14
	leal	2(%r10), %r11d
	movq	%r14, -128(%rbp)
	leaq	0(,%r11,8), %r14
	leal	2(%rcx), %r11d
	movq	%r14, -88(%rbp)
	leaq	0(,%r11,8), %r14
	movl	%eax, %r11d
	xorl	$2, %r11d
	movq	%r14, -160(%rbp)
	subl	$1, %r11d
	andl	%r12d, %r11d
	movl	%eax, %r12d
	shrl	$31, %r11d
	xorl	$-4, %r12d
	negq	%r11
	movq	%r11, -168(%rbp)
	leal	3(%rdx), %r11d
	leaq	0(,%r11,8), %r14
	leal	3(%r10), %r11d
	movq	%r14, -176(%rbp)
	leaq	0(,%r11,8), %r14
	leal	3(%rcx), %r11d
	movq	%r14, -184(%rbp)
	leaq	0(,%r11,8), %r14
	movl	%eax, %r11d
	xorl	$3, %r11d
	movq	%r14, -192(%rbp)
	subl	$1, %r11d
	andl	%r12d, %r11d
	movl	%eax, %r12d
	shrl	$31, %r11d
	xorl	$-5, %r12d
	negq	%r11
	movq	%r11, -200(%rbp)
	leal	4(%rdx), %r11d
	leaq	0(,%r11,8), %r14
	leal	4(%r10), %r11d
	movq	%r14, -224(%rbp)
	leaq	0(,%r11,8), %r14
	leal	4(%rcx), %r11d
	movq	%r14, -232(%rbp)
	leaq	0(,%r11,8), %r14
	movl	%eax, %r11d
	xorl	$4, %r11d
	movq	%r14, -240(%rbp)
	subl	$1, %r11d
	andl	%r12d, %r11d
	movl	%eax, %r12d
	shrl	$31, %r11d
	negq	%r11
	movq	%r11, -248(%rbp)
	leal	5(%rdx), %r11d
	leaq	0(,%r11,8), %r14
	leal	5(%r10), %r11d
	movq	%r14, -256(%rbp)
	leaq	0(,%r11,8), %r14
	leal	5(%rcx), %r11d
	movq	%r14, -264(%rbp)
	leaq	0(,%r11,8), %r14
	movl	%eax, %r11d
	xorl	$5, %r11d
	movq	%r14, -272(%rbp)
	subl	$1, %r11d
	xorl	$-6, %r12d
	andl	%r12d, %r11d
	movl	%eax, %r12d
	shrl	$31, %r11d
	xorl	$-7, %r12d
	negq	%r11
	movq	%r11, -280(%rbp)
	leal	6(%rdx), %r11d
	addl	$7, %edx
	leaq	0(,%r11,8), %r14
	leal	6(%r10), %r11d
	salq	$3, %rdx
	movq	%r14, -288(%rbp)
	leaq	0(,%r11,8), %r14
	leal	6(%rcx), %r11d
	movq	%r14, -296(%rbp)
	leaq	0(,%r11,8), %r14
	movl	%eax, %r11d
	xorl	$6, %r11d
	movq	%r14, -304(%rbp)
	subl	$1, %r11d
	andl	%r12d, %r11d
	shrl	$31, %r11d
	negq	%r11
	movq	%r11, -312(%rbp)
	movq	%rdx, -320(%rbp)
	leal	7(%r10), %edx
	salq	$3, %rdx
	movq	%rdx, -328(%rbp)
	leal	7(%rcx), %edx
	leaq	0(,%rdx,8), %rcx
	movl	%eax, %edx
	xorl	$-8, %eax
	xorl	$7, %edx
	movq	%rcx, -336(%rbp)
	subl	$1, %edx
	andl	%edx, %eax
	shrl	$31, %eax
	negq	%rax
	movq	%rax, -344(%rbp)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L20:
	addq	$8, %r15
.L9:
	movq	-144(%rbp), %rcx
	movq	(%rbx), %rax
	movq	-80(%rbp), %r11
	leaq	(%rcx,%rbx), %rdx
	movq	-136(%rbp), %rcx
	andq	%r9, %rax
	movq	(%rdx), %r10
	addq	%rbx, %r11
	leaq	(%rcx,%rbx), %rdx
	movq	-120(%rbp), %rcx
	movq	(%rdx), %r14
	andq	%rsi, %r10
	leaq	(%rcx,%rbx), %rdx
	movq	(%rdx), %r13
	movq	-104(%rbp), %rdx
	andq	%rdi, %r14
	movq	8(%rbx), %rcx
	orq	%r14, %r10
	movq	-160(%rbp), %r14
	addq	%rbx, %rdx
	orq	%rax, %r10
	andq	%r8, %r13
	movq	-128(%rbp), %rax
	movq	(%rdx), %r12
	movq	-96(%rbp), %rdx
	andq	%r9, %rcx
	orq	%r13, %r10
	movq	-152(%rbp), %r13
	addq	%rbx, %rax
	andq	-112(%rbp), %r10
	addq	%rbx, %rdx
	andq	%rsi, %r12
	movq	(%rdx), %rdx
	movq	(%r11), %r11
	orq	%rcx, %r12
	movq	16(%rbx), %rcx
	movq	(%rax), %rax
	andq	%rdi, %rdx
	andq	%r8, %r11
	orq	%rdx, %r12
	movq	-88(%rbp), %rdx
	andq	%rsi, %rax
	andq	%r9, %rcx
	orq	%r11, %r12
	orq	%rax, %rcx
	movq	-168(%rbp), %rax
	andq	%r12, %r13
	addq	%rbx, %rdx
	movq	(%rdx), %rdx
	orq	%r10, %r13
	leaq	(%r14,%rbx), %r10
	movq	-192(%rbp), %r14
	movq	(%r10), %r11
	andq	%rdi, %rdx
	orq	%rdx, %rcx
	andq	%r8, %r11
	orq	%r11, %rcx
	leaq	(%rbx,%r14), %r11
	andq	%rcx, %rax
	movq	-176(%rbp), %rcx
	orq	%r13, %rax
	leaq	(%rbx,%rcx), %rdx
	movq	%rax, %r10
	movq	24(%rbx), %rax
	movq	(%rdx), %rcx
	movq	-184(%rbp), %rdx
	addq	%rbx, %rdx
	andq	%r9, %rax
	andq	%rsi, %rcx
	movq	(%rdx), %rdx
	movq	(%r11), %r11
	orq	%rax, %rcx
	movq	-200(%rbp), %rax
	movl	-60(%rbp), %r14d
	andq	%rdi, %rdx
	andq	%r8, %r11
	orq	%rdx, %rcx
	orq	%r11, %rcx
	andq	%rcx, %rax
	orq	%r10, %rax
	cmpl	$2, %r14d
	je	.L8
	movq	-224(%rbp), %rdx
	movq	32(%rbx), %rcx
	movq	-240(%rbp), %r11
	addq	%rbx, %rdx
	andq	%r9, %rcx
	movq	(%rdx), %r10
	movq	-232(%rbp), %rdx
	addq	%rbx, %r11
	addq	%rbx, %rdx
	andq	%rsi, %r10
	movq	(%rdx), %rdx
	movq	(%r11), %r11
	orq	%rcx, %r10
	movq	-248(%rbp), %rcx
	andq	%rdi, %rdx
	andq	%r8, %r11
	orq	%rdx, %r10
	movq	-256(%rbp), %rdx
	orq	%r11, %r10
	movq	-272(%rbp), %r11
	andq	%r10, %rcx
	addq	%rbx, %rdx
	orq	%rcx, %rax
	movq	40(%rbx), %rcx
	movq	(%rdx), %r10
	addq	%rbx, %r11
	movq	-264(%rbp), %rdx
	andq	%r9, %rcx
	andq	%rsi, %r10
	addq	%rbx, %rdx
	orq	%rcx, %r10
	movq	-280(%rbp), %rcx
	movq	(%rdx), %rdx
	movq	(%r11), %r11
	andq	%rdi, %rdx
	andq	%r8, %r11
	orq	%rdx, %r10
	movq	-288(%rbp), %rdx
	orq	%r11, %r10
	movq	-304(%rbp), %r11
	andq	%r10, %rcx
	addq	%rbx, %rdx
	orq	%rax, %rcx
	movq	48(%rbx), %rax
	movq	(%rdx), %r10
	addq	%rbx, %r11
	movq	-296(%rbp), %rdx
	andq	%r9, %rax
	andq	%rsi, %r10
	addq	%rbx, %rdx
	orq	%rax, %r10
	movq	-312(%rbp), %rax
	movq	(%rdx), %rdx
	movq	(%r11), %r11
	andq	%rdi, %rdx
	orq	%rdx, %r10
	andq	%r8, %r11
	orq	%r11, %r10
	andq	%r10, %rax
	orq	%rcx, %rax
	cmpl	$3, %r14d
	jne	.L8
	movq	-320(%rbp), %rcx
	movq	56(%rbx), %r11
	movq	-336(%rbp), %r14
	leaq	(%rcx,%rbx), %rdx
	andq	%r9, %r11
	movq	(%rdx), %rcx
	movq	-328(%rbp), %rdx
	leaq	(%r14,%rbx), %r10
	addq	%rbx, %rdx
	andq	%rsi, %rcx
	movq	(%rdx), %rdx
	movq	(%r10), %r10
	orq	%r11, %rcx
	andq	%rdi, %rdx
	andq	%r8, %r10
	orq	%rdx, %rcx
	orq	%r10, %rcx
	andq	-344(%rbp), %rcx
	orq	%rcx, %rax
.L8:
	movq	-56(%rbp), %rcx
	addq	-72(%rbp), %rbx
	movq	%r15, -56(%rbp)
	movq	%rax, (%rcx)
	cmpq	-208(%rbp), %r15
	jne	.L20
.L7:
	movq	-216(%rbp), %rax
	movl	-64(%rbp), %esi
	movl	%esi, 8(%rax)
	movl	$1, %eax
.L1:
	addq	$312, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movl	-64(%rbp), %eax
	testl	%eax, %eax
	jle	.L7
	movq	-216(%rbp), %rax
	movslq	%r12d, %r11
	salq	$3, %r11
	movq	(%rax), %r10
	movl	-64(%rbp), %eax
	leaq	8(%r10), %r9
	subl	$1, %eax
	leaq	(%r9,%rax,8), %r14
	.p2align 4,,10
	.p2align 3
.L6:
	movq	%rbx, %rcx
	xorl	%esi, %esi
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L5:
	movl	%edx, %r8d
	movq	(%rcx), %rdi
	addl	$1, %edx
	addq	$8, %rcx
	xorl	%r13d, %r8d
	leal	-1(%r8), %eax
	notl	%r8d
	andl	%r8d, %eax
	shrl	$31, %eax
	negq	%rax
	andq	%rdi, %rax
	orq	%rax, %rsi
	cmpl	%edx, %r12d
	jne	.L5
	movq	%rsi, (%r10)
	addq	%r11, %rbx
	movq	%r9, %r10
	cmpq	%r14, %r9
	je	.L7
	addq	$8, %r9
	jmp	.L6
.L10:
	xorl	%eax, %eax
	jmp	.L1
	.cfi_endproc
.LFE294:
	.size	MOD_EXP_CTIME_COPY_FROM_PREBUF, .-MOD_EXP_CTIME_COPY_FROM_PREBUF
	.p2align 4
	.type	BN_mod_exp_recp.part.0, @function
BN_mod_exp_recp.part.0:
.LFB300:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	subq	$392, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -392(%rbp)
	movq	%rdx, %rdi
	movq	%rsi, -400(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	BN_num_bits@PLT
	movl	%eax, -412(%rbp)
	testl	%eax, %eax
	je	.L101
	movq	%r12, %rdi
	leaq	-384(%rbp), %r15
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -408(%rbp)
	call	BN_CTX_get@PLT
	movq	%rax, -320(%rbp)
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L28
	movq	%r15, %rdi
	call	BN_RECP_CTX_init@PLT
	movl	16(%r13), %ecx
	testl	%ecx, %ecx
	je	.L27
	movq	-408(%rbp), %rdi
	movq	%r13, %rsi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L28
	movq	-408(%rbp), %rax
	movq	%r12, %rdx
	movq	%r15, %rdi
	movl	$0, 16(%rax)
	movq	%rax, %rsi
	call	BN_RECP_CTX_set@PLT
	testl	%eax, %eax
	jle	.L28
.L30:
	movq	-400(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	BN_nnmod@PLT
	testl	%eax, %eax
	jne	.L102
.L28:
	xorl	%r13d, %r13d
.L26:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	movq	%r15, %rdi
	call	BN_RECP_CTX_free@PLT
.L21:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L103
	addq	$392, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	movl	$1, %esi
	movq	%r13, %rdi
	call	BN_abs_is_word@PLT
	testl	%eax, %eax
	je	.L23
	movq	-392(%rbp), %rdi
	xorl	%esi, %esi
	movl	$1, %r13d
	call	BN_set_word@PLT
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L27:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	BN_RECP_CTX_set@PLT
	testl	%eax, %eax
	jg	.L30
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L23:
	movq	-392(%rbp), %rdi
	movl	$1, %esi
	call	BN_set_word@PLT
	movl	%eax, %r13d
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L102:
	movq	%rbx, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L104
	movl	-412(%rbp), %eax
	cmpl	$671, %eax
	jg	.L51
	cmpl	$239, %eax
	jle	.L105
	movl	$16, -400(%rbp)
	movl	$5, -416(%rbp)
.L32:
	movq	-408(%rbp), %rdi
	movq	%r12, %r8
	movq	%r15, %rcx
	movq	%rbx, %rdx
	movq	%rbx, %rsi
	call	BN_mod_mul_reciprocal@PLT
	testl	%eax, %eax
	je	.L28
	movq	%r14, -424(%rbp)
	movq	-408(%rbp), %r14
	movl	$1, %r13d
	leaq	-320(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L34:
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, (%rbx,%r13,8)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L28
	movq	-8(%rbx,%r13,8), %rsi
	movq	%r12, %r8
	movq	%r15, %rcx
	movq	%r14, %rdx
	call	BN_mod_mul_reciprocal@PLT
	testl	%eax, %eax
	je	.L28
	addq	$1, %r13
	cmpl	%r13d, -400(%rbp)
	jg	.L34
	movq	-424(%rbp), %r14
.L33:
	movq	-392(%rbp), %rbx
	movl	$1, %esi
	movq	%rbx, %rdi
	call	BN_set_word@PLT
	testl	%eax, %eax
	je	.L28
	movl	-412(%rbp), %eax
	movq	%r12, -408(%rbp)
	movl	$1, -400(%rbp)
	subl	$1, %eax
	movl	%eax, %r13d
.L35:
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	BN_is_bit_set@PLT
	testl	%eax, %eax
	jne	.L106
	movl	-400(%rbp), %edx
	testl	%edx, %edx
	je	.L39
.L42:
	testl	%r13d, %r13d
	jne	.L107
.L40:
	movq	-408(%rbp), %r12
	movl	$1, %r13d
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L105:
	cmpl	$79, %eax
	jg	.L53
	cmpl	$23, %eax
	jle	.L54
	movl	$4, -400(%rbp)
	movl	$3, -416(%rbp)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L51:
	movl	$32, -400(%rbp)
	movl	$6, -416(%rbp)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L104:
	movq	-392(%rbp), %rdi
	xorl	%esi, %esi
	movl	$1, %r13d
	call	BN_set_word@PLT
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L53:
	movl	$8, -400(%rbp)
	movl	$4, -416(%rbp)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L106:
	cmpl	$1, -416(%rbp)
	je	.L55
	testl	%r13d, %r13d
	jle	.L56
	movl	$1, %r12d
	leal	1(%r13), %eax
	xorl	%ecx, %ecx
	movq	%rbx, -424(%rbp)
	movq	%r15, -432(%rbp)
	movl	%ecx, %ebx
	movl	%r12d, %r15d
	movl	%eax, %r12d
	movl	$1, -392(%rbp)
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L45:
	cmpl	%r15d, %r12d
	je	.L97
.L38:
	movl	%r13d, %esi
	movq	%r14, %rdi
	subl	%r15d, %esi
	call	BN_is_bit_set@PLT
	testl	%eax, %eax
	je	.L44
	movl	-392(%rbp), %eax
	movl	%r15d, %ecx
	subl	%ebx, %ecx
	movl	%r15d, %ebx
	sall	%cl, %eax
	orl	$1, %eax
	movl	%eax, -392(%rbp)
.L44:
	addl	$1, %r15d
	cmpl	-416(%rbp), %r15d
	jne	.L45
.L97:
	movl	%ebx, -412(%rbp)
	movq	-432(%rbp), %r15
	movq	-424(%rbp), %rbx
.L37:
	movl	-400(%rbp), %eax
	testl	%eax, %eax
	je	.L46
.L49:
	movl	-392(%rbp), %eax
	movq	%r15, %rcx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	movq	-408(%rbp), %r8
	sarl	%eax
	cltq
	movq	-320(%rbp,%rax,8), %rdx
	call	BN_mod_mul_reciprocal@PLT
	testl	%eax, %eax
	je	.L100
	movl	-412(%rbp), %eax
	addl	$1, %eax
	subl	%eax, %r13d
	js	.L40
	movl	$0, -400(%rbp)
	jmp	.L35
.L54:
	movl	$1, -416(%rbp)
	jmp	.L33
.L39:
	movq	-408(%rbp), %r8
	movq	%r15, %rcx
	movq	%rbx, %rdx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	BN_mod_mul_reciprocal@PLT
	testl	%eax, %eax
	jne	.L42
.L100:
	movq	-408(%rbp), %r12
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L107:
	subl	$1, %r13d
	jmp	.L35
.L46:
	movl	-412(%rbp), %eax
	movl	%r13d, -424(%rbp)
	movl	-400(%rbp), %r13d
	movq	%r14, -400(%rbp)
	movq	%rbx, %r14
	leal	1(%rax), %r12d
	movl	%r12d, %ebx
	movq	-408(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L48:
	movq	%r12, %r8
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	BN_mod_mul_reciprocal@PLT
	testl	%eax, %eax
	je	.L28
	addl	$1, %r13d
	cmpl	%ebx, %r13d
	jne	.L48
	movq	%r14, %rbx
	movl	-424(%rbp), %r13d
	movq	-400(%rbp), %r14
	jmp	.L49
.L55:
	movl	$1, -392(%rbp)
	movl	$0, -412(%rbp)
	jmp	.L37
.L56:
	movl	$0, -412(%rbp)
	movl	$1, -392(%rbp)
	jmp	.L37
.L103:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE300:
	.size	BN_mod_exp_recp.part.0, .-BN_mod_exp_recp.part.0
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/bn/bn_exp.c"
	.text
	.p2align 4
	.type	BN_mod_exp_mont.part.0, @function
BN_mod_exp_mont.part.0:
.LFB301:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	subq	$360, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -344(%rbp)
	movq	%rcx, %rdi
	movq	%rdx, -336(%rbp)
	movq	%r9, -328(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	BN_is_odd@PLT
	testl	%eax, %eax
	je	.L212
	movq	-336(%rbp), %rdi
	call	BN_num_bits@PLT
	movl	%eax, -360(%rbp)
	testl	%eax, %eax
	je	.L213
	movq	%r12, %rdi
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -368(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -376(%rbp)
	call	BN_CTX_get@PLT
	movq	%rax, -320(%rbp)
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L150
	movq	-328(%rbp), %rax
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L214
.L115:
	movl	16(%r14), %ecx
	testl	%ecx, %ecx
	je	.L117
.L119:
	movq	%r14, %rsi
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	BN_nnmod@PLT
	movq	%rbx, %r14
	testl	%eax, %eax
	je	.L114
.L118:
	movq	%r12, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	bn_to_mont_fixed_top@PLT
	testl	%eax, %eax
	je	.L114
	movl	-360(%rbp), %eax
	cmpl	$671, %eax
	jg	.L153
	cmpl	$239, %eax
	jg	.L154
	cmpl	$79, %eax
	jg	.L155
	cmpl	$23, %eax
	jle	.L156
	movl	$4, -352(%rbp)
	movl	$3, -380(%rbp)
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L213:
	movl	$1, %esi
	movq	%r13, %rdi
	call	BN_abs_is_word@PLT
	testl	%eax, %eax
	jne	.L215
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L211
	movq	-344(%rbp), %rdi
	addq	$360, %rsp
	movl	$1, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BN_set_word@PLT
	.p2align 4,,10
	.p2align 3
.L154:
	.cfi_restore_state
	movl	$16, -352(%rbp)
	movl	$5, -380(%rbp)
.L120:
	movq	-368(%rbp), %rdi
	movq	%r12, %r8
	movq	%r15, %rcx
	movq	%rbx, %rdx
	movq	%rbx, %rsi
	call	bn_mul_mont_fixed_top@PLT
	testl	%eax, %eax
	je	.L114
	movq	%r13, -392(%rbp)
	movq	-368(%rbp), %r13
	movl	$1, %ebx
	leaq	-320(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L122:
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, (%r14,%rbx,8)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L158
	movq	-8(%r14,%rbx,8), %rsi
	movq	%r12, %r8
	movq	%r15, %rcx
	movq	%r13, %rdx
	call	bn_mul_mont_fixed_top@PLT
	testl	%eax, %eax
	je	.L114
	addq	$1, %rbx
	cmpl	%ebx, -352(%rbp)
	jg	.L122
	movq	-392(%rbp), %r13
.L121:
	movslq	8(%r13), %rdx
	movq	0(%r13), %rax
	cmpq	$0, -8(%rax,%rdx,8)
	movq	%rdx, %rbx
	js	.L216
	call	BN_value_one@PLT
	movq	-376(%rbp), %rdi
	movq	%r12, %rcx
	movq	%r15, %rdx
	movq	%rax, %rsi
	call	bn_to_mont_fixed_top@PLT
	testl	%eax, %eax
	je	.L114
.L125:
	movl	-360(%rbp), %r14d
	movq	%r12, -360(%rbp)
	movq	-376(%rbp), %rbx
	movq	-336(%rbp), %r13
	movl	$1, -352(%rbp)
	subl	$1, %r14d
.L133:
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	BN_is_bit_set@PLT
	testl	%eax, %eax
	jne	.L217
	movl	-352(%rbp), %edx
	testl	%edx, %edx
	je	.L137
.L140:
	testl	%r14d, %r14d
	jne	.L218
.L138:
	movq	-360(%rbp), %r12
	movq	-376(%rbp), %rsi
	movq	%r15, %rdx
	movq	-344(%rbp), %rdi
	movq	%r12, %rcx
	call	BN_from_montgomery@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	.p2align 4,,10
	.p2align 3
.L114:
	cmpq	$0, -328(%rbp)
	je	.L116
.L148:
	movq	%r12, %rdi
	movl	%eax, -328(%rbp)
	call	BN_CTX_end@PLT
	movl	-328(%rbp), %eax
.L108:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L211
	addq	$360, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L212:
	.cfi_restore_state
	movl	$318, %r8d
	movl	$102, %edx
	movl	$109, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	movl	%eax, -328(%rbp)
	call	ERR_put_error@PLT
	movl	-328(%rbp), %eax
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L117:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	jns	.L119
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L215:
	movq	-344(%rbp), %rdi
	xorl	%esi, %esi
	call	BN_set_word@PLT
	movl	$1, %eax
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L150:
	xorl	%r15d, %r15d
	xorl	%eax, %eax
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L214:
	call	BN_MONT_CTX_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L219
	xorl	%eax, %eax
.L116:
	movq	%r15, %rdi
	movl	%eax, -328(%rbp)
	call	BN_MONT_CTX_free@PLT
	movl	-328(%rbp), %eax
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L153:
	movl	$32, -352(%rbp)
	movl	$6, -380(%rbp)
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L219:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	BN_MONT_CTX_set@PLT
	testl	%eax, %eax
	jne	.L115
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L155:
	movl	$8, -352(%rbp)
	movl	$4, -380(%rbp)
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L158:
	xorl	%eax, %eax
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L216:
	movq	-376(%rbp), %r14
	movl	%edx, %esi
	movq	%r14, %rdi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L158
	movq	0(%r13), %rdx
	movq	(%r14), %rcx
	movq	(%rdx), %rax
	negq	%rax
	movq	%rax, (%rcx)
	cmpl	$1, %ebx
	jle	.L129
	leaq	8(%rdx), %rdi
	leaq	24(%rcx), %rsi
	cmpq	%rsi, %rdi
	leaq	8(%rcx), %r8
	leaq	24(%rdx), %rsi
	setnb	%dil
	cmpq	%rsi, %r8
	leal	-2(%rbx), %eax
	setnb	%sil
	orb	%sil, %dil
	je	.L126
	cmpl	$3, %eax
	jbe	.L126
	leal	-1(%rbx), %edi
	movl	$8, %eax
	pcmpeqd	%xmm1, %xmm1
	movl	%edi, %esi
	shrl	%esi
	salq	$4, %rsi
	addq	$8, %rsi
	.p2align 4,,10
	.p2align 3
.L127:
	movdqu	(%rdx,%rax), %xmm0
	pxor	%xmm1, %xmm0
	movups	%xmm0, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L127
	movl	%edi, %eax
	orl	$1, %eax
	andl	$1, %edi
	je	.L129
	cltq
	movq	(%rdx,%rax,8), %rdx
	notq	%rdx
	movq	%rdx, (%rcx,%rax,8)
.L129:
	movq	-376(%rbp), %rax
	movl	%ebx, 8(%rax)
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L217:
	cmpl	$1, -380(%rbp)
	je	.L160
	testl	%r14d, %r14d
	jle	.L160
	movl	$1, %r12d
	leal	1(%r14), %eax
	xorl	%ecx, %ecx
	movq	%rbx, -392(%rbp)
	movq	%r15, -368(%rbp)
	movl	%ecx, %ebx
	movl	%r12d, %r15d
	movl	%eax, %r12d
	movl	$1, -336(%rbp)
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L143:
	cmpl	%r12d, %r15d
	je	.L207
.L136:
	movl	%r14d, %esi
	movq	%r13, %rdi
	subl	%r15d, %esi
	call	BN_is_bit_set@PLT
	testl	%eax, %eax
	je	.L142
	movl	-336(%rbp), %eax
	movl	%r15d, %ecx
	subl	%ebx, %ecx
	movl	%r15d, %ebx
	sall	%cl, %eax
	orl	$1, %eax
	movl	%eax, -336(%rbp)
.L142:
	addl	$1, %r15d
	cmpl	-380(%rbp), %r15d
	jne	.L143
.L207:
	movq	-368(%rbp), %r15
	movl	%ebx, -368(%rbp)
	movq	-392(%rbp), %rbx
.L135:
	movl	-352(%rbp), %eax
	testl	%eax, %eax
	je	.L144
.L147:
	movl	-336(%rbp), %eax
	movq	%r15, %rcx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	movq	-360(%rbp), %r8
	sarl	%eax
	cltq
	movq	-320(%rbp,%rax,8), %rdx
	call	bn_mul_mont_fixed_top@PLT
	testl	%eax, %eax
	je	.L210
	movl	-368(%rbp), %eax
	addl	$1, %eax
	subl	%eax, %r14d
	js	.L138
	movl	$0, -352(%rbp)
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L137:
	movq	-360(%rbp), %r8
	movq	%r15, %rcx
	movq	%rbx, %rdx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	bn_mul_mont_fixed_top@PLT
	testl	%eax, %eax
	jne	.L140
.L210:
	movq	-360(%rbp), %r12
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L218:
	subl	$1, %r14d
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L156:
	movl	$1, -380(%rbp)
	jmp	.L121
.L144:
	movl	-368(%rbp), %eax
	movl	%r14d, -392(%rbp)
	movl	-352(%rbp), %r14d
	movq	%r13, -352(%rbp)
	movq	%rbx, %r13
	leal	1(%rax), %r12d
	movl	%r12d, %ebx
	movq	-360(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L146:
	movq	%r12, %r8
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	bn_mul_mont_fixed_top@PLT
	testl	%eax, %eax
	je	.L114
	addl	$1, %r14d
	cmpl	%r14d, %ebx
	jne	.L146
	movq	%r13, %rbx
	movl	-392(%rbp), %r14d
	movq	-352(%rbp), %r13
	jmp	.L147
.L126:
	leaq	2(%rax), %rdi
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L131:
	movq	(%rdx,%rax,8), %rsi
	notq	%rsi
	movq	%rsi, (%rcx,%rax,8)
	addq	$1, %rax
	cmpq	%rdi, %rax
	jne	.L131
	jmp	.L129
.L160:
	movl	$1, -336(%rbp)
	movl	$0, -368(%rbp)
	jmp	.L135
.L211:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE301:
	.size	BN_mod_exp_mont.part.0, .-BN_mod_exp_mont.part.0
	.p2align 4
	.type	BN_mod_exp_mont_word.part.0, @function
BN_mod_exp_mont_word.part.0:
.LFB302:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	cmpl	$1, 8(%rcx)
	movq	%rdi, -104(%rbp)
	movq	%rsi, -56(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%r9, -96(%rbp)
	jne	.L221
	movq	(%rcx), %rbx
	movq	%rsi, %rax
	xorl	%edx, %edx
	divq	(%rbx)
	movq	%rdx, -56(%rbp)
.L221:
	movq	-64(%rbp), %rdi
	call	BN_num_bits@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	je	.L312
	cmpq	$0, -56(%rbp)
	je	.L225
	movq	%r12, %rdi
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	BN_CTX_get@PLT
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L244
	movq	-96(%rbp), %rax
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L313
.L227:
	movq	-56(%rbp), %r14
	subl	$2, %ebx
	js	.L229
	movl	$1, %eax
	movq	%r15, -72(%rbp)
	movq	%r13, %r15
	movl	%eax, %r13d
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L315:
	movq	%r14, %rsi
	movq	%r15, %rdi
	testl	%r13d, %r13d
	je	.L231
	call	BN_set_word@PLT
	testl	%eax, %eax
	je	.L308
	movq	-72(%rbp), %rdx
	movq	%r12, %rcx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	BN_to_montgomery@PLT
	testl	%eax, %eax
	je	.L308
.L311:
	movl	$1, %r14d
.L232:
	movq	-72(%rbp), %rcx
	movq	%r12, %r8
	movq	%r15, %rdx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	BN_mod_mul_montgomery@PLT
	testl	%eax, %eax
	je	.L308
	xorl	%r13d, %r13d
.L233:
	movq	-64(%rbp), %rdi
	movl	%ebx, %esi
	call	BN_is_bit_set@PLT
	testl	%eax, %eax
	je	.L234
	movq	-56(%rbp), %rdx
	movq	%rdx, %rcx
	movq	%rdx, %rsi
	xorl	%edx, %edx
	imulq	%r14, %rcx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%r14, %rax
	je	.L248
	movq	%r14, %rsi
	movq	%r15, %rdi
	testl	%r13d, %r13d
	je	.L235
	call	BN_set_word@PLT
	testl	%eax, %eax
	je	.L308
	movq	-72(%rbp), %rdx
	movq	%r12, %rcx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	BN_to_montgomery@PLT
	testl	%eax, %eax
	je	.L308
	movq	-56(%rbp), %r14
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L234:
	subl	$1, %ebx
	cmpl	$-1, %ebx
	je	.L314
.L236:
	movq	%r14, %rcx
	xorl	%edx, %edx
	imulq	%r14, %rcx
	movq	%rcx, %rax
	divq	%r14
	cmpq	%r14, %rax
	jne	.L315
	movq	%rcx, %r14
	testl	%r13d, %r13d
	jne	.L233
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L231:
	call	BN_mul_word@PLT
	testl	%eax, %eax
	je	.L308
	movq	-80(%rbp), %r14
	movq	-88(%rbp), %rcx
	xorl	%edi, %edi
	movq	%r12, %r8
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	BN_div@PLT
	testl	%eax, %eax
	je	.L308
	movq	%r15, %rax
	movq	%r14, %r15
	movq	%rax, -80(%rbp)
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L248:
	subl	$1, %ebx
	movq	%rcx, %r14
	cmpl	$-1, %ebx
	jne	.L236
	.p2align 4,,10
	.p2align 3
.L314:
	movl	%r13d, %eax
	movq	%r15, %r13
	movq	-72(%rbp), %r15
	cmpq	$1, %r14
	je	.L239
	testl	%eax, %eax
	jne	.L238
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	BN_mul_word@PLT
	testl	%eax, %eax
	je	.L226
	movq	%r13, %rdx
	movq	-80(%rbp), %r13
	movq	-88(%rbp), %rcx
	xorl	%edi, %edi
	movq	%r12, %r8
	movq	%r13, %rsi
	call	BN_div@PLT
	testl	%eax, %eax
	je	.L226
.L241:
	movq	-104(%rbp), %rdi
	movq	%r12, %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	BN_from_montgomery@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L235:
	call	BN_mul_word@PLT
	testl	%eax, %eax
	je	.L308
	movq	-80(%rbp), %r14
	movq	-88(%rbp), %rcx
	xorl	%edi, %edi
	movq	%r12, %r8
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	BN_div@PLT
	testl	%eax, %eax
	je	.L308
	movq	%r15, %rax
	movq	%r14, %r15
	movq	-56(%rbp), %r14
	movq	%rax, -80(%rbp)
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L312:
	movq	-88(%rbp), %rdi
	movl	$1, %esi
	call	BN_abs_is_word@PLT
	testl	%eax, %eax
	je	.L223
.L225:
	movq	-104(%rbp), %rdi
	xorl	%esi, %esi
	call	BN_set_word@PLT
	addq	$72, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L308:
	.cfi_restore_state
	movq	-72(%rbp), %r15
.L226:
	cmpq	$0, -96(%rbp)
	je	.L228
.L243:
	movq	%r12, %rdi
	movl	%eax, -56(%rbp)
	call	BN_CTX_end@PLT
	movl	-56(%rbp), %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	.cfi_restore_state
	movq	-104(%rbp), %rdi
	addq	$72, %rsp
	movl	$1, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BN_set_word@PLT
	.p2align 4,,10
	.p2align 3
.L244:
	.cfi_restore_state
	xorl	%r15d, %r15d
	xorl	%eax, %eax
	jmp	.L226
.L246:
	xorl	%eax, %eax
.L228:
	movq	%r15, %rdi
	movl	%eax, -56(%rbp)
	call	BN_MONT_CTX_free@PLT
	movl	-56(%rbp), %eax
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L313:
	call	BN_MONT_CTX_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L246
	movq	-88(%rbp), %rsi
	movq	%r12, %rdx
	movq	%rax, %rdi
	call	BN_MONT_CTX_set@PLT
	testl	%eax, %eax
	jne	.L227
	jmp	.L228
.L229:
	cmpq	$1, %r14
	je	.L242
.L238:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	BN_set_word@PLT
	testl	%eax, %eax
	je	.L226
	movq	%r12, %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	BN_to_montgomery@PLT
	testl	%eax, %eax
	jne	.L241
	jmp	.L226
.L239:
	testl	%eax, %eax
	je	.L241
.L242:
	movq	-104(%rbp), %rdi
	movl	$1, %esi
	call	BN_set_word@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L226
	.cfi_endproc
.LFE302:
	.size	BN_mod_exp_mont_word.part.0, .-BN_mod_exp_mont_word.part.0
	.p2align 4
	.globl	BN_exp
	.type	BN_exp, @function
BN_exp:
.LFB288:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	$4, %esi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	%rdi, -56(%rbp)
	movq	%rdx, %rdi
	call	BN_get_flags@PLT
	testl	%eax, %eax
	jne	.L319
	movl	$4, %esi
	movq	%r14, %rdi
	call	BN_get_flags@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L355
.L319:
	movl	$49, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$66, %edx
	xorl	%r13d, %r13d
	movl	$123, %esi
	movl	$3, %edi
	call	ERR_put_error@PLT
.L316:
	addq	$40, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L355:
	.cfi_restore_state
	movq	%r12, %rdi
	call	BN_CTX_start@PLT
	movq	-56(%rbp), %rax
	cmpq	%rax, %r14
	je	.L335
	movq	%rax, -64(%rbp)
	cmpq	%rax, %rbx
	je	.L335
.L320:
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	cmpq	$0, -64(%rbp)
	movq	%rax, %r15
	je	.L324
	testq	%rax, %rax
	je	.L324
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L324
	movq	%rbx, %rdi
	call	BN_num_bits@PLT
	movq	%rbx, %rdi
	movl	%eax, -68(%rbp)
	call	BN_is_odd@PLT
	testl	%eax, %eax
	je	.L326
	movq	-64(%rbp), %rdi
	movq	%r14, %rsi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L324
.L327:
	cmpl	$1, -68(%rbp)
	movl	$1, %r14d
	jg	.L329
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L331:
	addl	$1, %r14d
	cmpl	%r14d, -68(%rbp)
	je	.L330
.L329:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	BN_sqr@PLT
	testl	%eax, %eax
	je	.L324
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	BN_is_bit_set@PLT
	testl	%eax, %eax
	je	.L331
	movq	-64(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r15, %rdx
	movq	%rsi, %rdi
	call	BN_mul@PLT
	testl	%eax, %eax
	jne	.L331
	.p2align 4,,10
	.p2align 3
.L324:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L335:
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, -64(%rbp)
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L326:
	movq	-64(%rbp), %rdi
	movl	$1, %esi
	call	BN_set_word@PLT
	testl	%eax, %eax
	jne	.L327
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L330:
	movq	-56(%rbp), %rbx
	movl	$1, %r13d
	cmpq	%rbx, -64(%rbp)
	je	.L324
	movq	-64(%rbp), %rsi
	movq	%rbx, %rdi
	xorl	%r13d, %r13d
	call	BN_copy@PLT
	testq	%rax, %rax
	setne	%r13b
	jmp	.L324
	.cfi_endproc
.LFE288:
	.size	BN_exp, .-BN_exp
	.p2align 4
	.globl	BN_mod_exp_recp
	.type	BN_mod_exp_recp, @function
BN_mod_exp_recp:
.LFB290:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%rdx, %rdi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$4, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$8, %rsp
	call	BN_get_flags@PLT
	testl	%eax, %eax
	jne	.L359
	movl	$4, %esi
	movq	%r13, %rdi
	call	BN_get_flags@PLT
	testl	%eax, %eax
	je	.L362
.L359:
	movl	$175, %r8d
	movl	$66, %edx
	movl	$125, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L362:
	.cfi_restore_state
	movl	$4, %esi
	movq	%r14, %rdi
	call	BN_get_flags@PLT
	testl	%eax, %eax
	jne	.L359
	addq	$8, %rsp
	movq	%rbx, %r8
	movq	%r14, %rcx
	movq	%r12, %rdx
	popq	%rbx
	movq	%r13, %rsi
	popq	%r12
	movq	%r15, %rdi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	BN_mod_exp_recp.part.0
	.cfi_endproc
.LFE290:
	.size	BN_mod_exp_recp, .-BN_mod_exp_recp
	.p2align 4
	.globl	BN_mod_exp_mont_consttime
	.type	BN_mod_exp_mont_consttime, @function
BN_mod_exp_mont_consttime:
.LFB295:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -160(%rbp)
	movq	%rcx, %rdi
	movq	%rdx, -136(%rbp)
	movq	%r8, -128(%rbp)
	movq	%r9, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	BN_is_odd@PLT
	testl	%eax, %eax
	je	.L571
	movq	-136(%rbp), %rax
	movl	8(%rax), %eax
	movl	%eax, -120(%rbp)
	sall	$6, %eax
	movl	%eax, -168(%rbp)
	je	.L572
	movq	-128(%rbp), %rdi
	movl	8(%r12), %ebx
	call	BN_CTX_start@PLT
	movq	-144(%rbp), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L573
.L368:
	movl	16(%r13), %esi
	testl	%esi, %esi
	jne	.L374
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r13, %r14
	call	BN_ucmp@PLT
	testl	%eax, %eax
	js	.L375
.L374:
	movq	-128(%rbp), %rdi
	call	BN_CTX_get@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L372
	movq	-128(%rbp), %rcx
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	BN_nnmod@PLT
	testl	%eax, %eax
	je	.L372
.L375:
	movl	8(%r14), %eax
	cmpl	$16, %eax
	je	.L574
.L376:
	cmpl	$8, %eax
	je	.L575
.L377:
	cmpl	$306, -168(%rbp)
	jle	.L576
	movl	$32, -176(%rbp)
	leal	0(,%rbx,8), %edx
	movl	$1, %r13d
	movl	$32, %r9d
	movl	$5, -184(%rbp)
.L382:
	movl	-176(%rbp), %esi
	leal	(%rbx,%rbx), %eax
	movzbl	-184(%rbp), %ecx
	cmpl	%esi, %eax
	cmovl	%esi, %eax
	movl	%ebx, %esi
	sall	%cl, %esi
	addl	%esi, %eax
	leal	(%rdx,%rax,8), %ecx
	leal	64(%rcx), %edi
	movslq	%edi, %rdi
	cmpl	$3071, %ecx
	jg	.L383
	addq	$23, %rdi
	movq	%rsp, %rdx
	movq	%rdi, %rax
	andq	$-4096, %rdi
	subq	%rdi, %rdx
	andq	$-16, %rax
	cmpq	%rdx, %rsp
	je	.L385
.L577:
	subq	$4096, %rsp
	orq	$0, 4088(%rsp)
	cmpq	%rdx, %rsp
	jne	.L577
.L385:
	andl	$4095, %eax
	subq	%rax, %rsp
	testq	%rax, %rax
	jne	.L578
.L386:
	leaq	15(%rsp), %rax
	movq	%rax, %rdx
	andl	$48, %eax
	andq	$-16, %rdx
	subq	%rax, %rdx
	movslq	%ecx, %rax
	leaq	64(%rdx), %r15
	movq	%rax, -192(%rbp)
	movq	%r15, %rdi
	cmpq	$8, %rax
	jnb	.L579
.L387:
	testb	$4, -192(%rbp)
	jne	.L580
.L388:
	testb	$2, -192(%rbp)
	jne	.L581
.L389:
	testb	$1, -192(%rbp)
	jne	.L582
.L390:
	movq	$0, -208(%rbp)
.L391:
	movslq	%ebx, %rax
	movq	(%r12), %rdx
	movl	%ebx, -68(%rbp)
	imulq	%rax, %r9
	leaq	0(,%rax,8), %r8
	movq	%rax, -120(%rbp)
	movl	$0, -72(%rbp)
	movl	$0, -104(%rbp)
	salq	$3, %r9
	cmpq	$0, -8(%rdx,%r8)
	movl	%ebx, -100(%rbp)
	leaq	(%r15,%r9), %rcx
	leaq	(%rcx,%r8), %rax
	movq	%rcx, -112(%rbp)
	movq	%rax, -80(%rbp)
	movabsq	$8589934592, %rax
	movq	%rax, -64(%rbp)
	movq	%rax, -96(%rbp)
	jns	.L393
	movq	(%rdx), %rax
	negq	%rax
	movq	%rax, (%rcx)
	cmpl	$1, %ebx
	jle	.L399
	leaq	24(%r15,%r9), %rsi
	leaq	8(%rdx), %r10
	cmpq	%rsi, %r10
	leaq	8(%r15,%r9), %rdi
	leaq	24(%rdx), %rsi
	setnb	%r9b
	cmpq	%rsi, %rdi
	leal	-2(%rbx), %eax
	setnb	%sil
	orb	%sil, %r9b
	je	.L396
	cmpl	$3, %eax
	jbe	.L396
	leal	-1(%rbx), %r9d
	xorl	%eax, %eax
	pcmpeqd	%xmm1, %xmm1
	movl	%r9d, %esi
	shrl	%esi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L397:
	movdqu	8(%rdx,%rax), %xmm0
	pxor	%xmm1, %xmm0
	movups	%xmm0, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rax, %rsi
	jne	.L397
	movl	%r9d, %eax
	orl	$1, %eax
	andl	$1, %r9d
	je	.L399
	cltq
	movq	(%rdx,%rax,8), %rdx
	notq	%rdx
	movq	%rdx, (%rcx,%rax,8)
.L399:
	movl	%ebx, -104(%rbp)
.L395:
	leaq	-80(%rbp), %rax
	movq	-128(%rbp), %rcx
	movq	%r14, %rsi
	movq	-152(%rbp), %rdx
	movq	%rax, %rdi
	movq	%r8, -216(%rbp)
	movq	%rax, -200(%rbp)
	call	bn_to_mont_fixed_top@PLT
	testl	%eax, %eax
	je	.L403
	movq	-112(%rbp), %r9
	movslq	-104(%rbp), %rcx
	movslq	-72(%rbp), %rsi
	movq	-80(%rbp), %r12
	cmpl	$1, %ebx
	jle	.L404
	testb	%r13b, %r13b
	je	.L404
	movq	-152(%rbp), %rax
	cmpl	%ebx, %esi
	movq	-216(%rbp), %r8
	leaq	80(%rax), %r13
	jge	.L408
	movl	%esi, %eax
	leaq	(%r12,%rsi,8), %rdi
	xorl	%esi, %esi
	movq	%r9, -200(%rbp)
	notl	%eax
	movl	%ecx, -184(%rbp)
	addl	%ebx, %eax
	movq	%r8, -176(%rbp)
	leaq	8(,%rax,8), %rdx
	call	memset@PLT
	movq	-176(%rbp), %r8
	movslq	-184(%rbp), %rcx
	movq	-200(%rbp), %r9
.L408:
	cmpl	%ecx, %ebx
	jle	.L407
	movl	%ecx, %eax
	leaq	(%r9,%rcx,8), %rdi
	xorl	%esi, %esi
	movq	%r8, -184(%rbp)
	notl	%eax
	movq	%r9, -176(%rbp)
	addl	%ebx, %eax
	leaq	8(,%rax,8), %rdx
	call	memset@PLT
	movq	-176(%rbp), %r9
	movq	-184(%rbp), %r8
.L407:
	movq	-152(%rbp), %rax
	leaq	(%r12,%r8), %r14
	movq	32(%rax), %rdx
	leaq	16(%r12,%r8), %rax
	cmpq	%rax, %rdx
	leaq	16(%rdx), %rax
	setnb	%cl
	cmpq	%rax, %r14
	setnb	%al
	orb	%al, %cl
	je	.L469
	cmpl	$4, %ebx
	jle	.L469
	testl	%ebx, %ebx
	movl	$1, %esi
	cmovg	%ebx, %esi
	xorl	%eax, %eax
	movl	%esi, %ecx
	shrl	%ecx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L414:
	movdqu	(%rdx,%rax), %xmm2
	movups	%xmm2, (%r14,%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L414
	movl	%esi, %eax
	andl	$-2, %eax
	andl	$1, %esi
	je	.L413
	movq	(%rdx,%rax,8), %rdx
	movq	%rdx, (%r14,%rax,8)
.L413:
	movq	-120(%rbp), %r12
	movq	%r9, %rdi
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	bn_scatter5@PLT
	movslq	-72(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movq	%r15, %rdx
	movl	$1, %ecx
	call	bn_scatter5@PLT
	movq	-80(%rbp), %rsi
	movq	-112(%rbp), %rdi
	movl	%ebx, %r9d
	movq	%r14, %rcx
	movq	%r13, %r8
	movq	%rsi, %rdx
	call	bn_mul_mont@PLT
	movq	-112(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	movl	$4, %r12d
	movl	$2, %ecx
	call	bn_scatter5@PLT
	movl	$3, %r10d
	movl	%r12d, %eax
	movl	%r10d, %r12d
	movq	%r14, %r10
	movl	%ebx, %r14d
	movl	%eax, %ebx
.L416:
	movq	-112(%rbp), %rdi
	movq	%r10, %rcx
	movl	%r14d, %r9d
	movq	%r13, %r8
	movq	%r10, -176(%rbp)
	movq	%rdi, %rdx
	movq	%rdi, %rsi
	call	bn_mul_mont@PLT
	movq	-120(%rbp), %rsi
	movq	-112(%rbp), %rdi
	movslq	%ebx, %rcx
	movq	%r15, %rdx
	addl	%ebx, %ebx
	call	bn_scatter5@PLT
	subl	$1, %r12d
	movq	-176(%rbp), %r10
	jne	.L416
	movq	$3, -176(%rbp)
	movl	%r14d, %ebx
	movq	%r13, %rax
	movq	%r10, %r14
	movl	%ebx, %r13d
	movq	%rax, %rbx
.L418:
	movq	-176(%rbp), %r12
	movl	%r13d, %r9d
	movq	%rbx, %r8
	movq	%r14, %rcx
	subq	$8, %rsp
	movq	-80(%rbp), %rsi
	movq	-112(%rbp), %rdi
	movq	%r15, %rdx
	movl	%r12d, %eax
	subl	$1, %eax
	pushq	%rax
	call	bn_mul_mont_gather5@PLT
	movq	-120(%rbp), %rsi
	movq	-112(%rbp), %rdi
	movq	%r12, %rcx
	movq	%r15, %rdx
	leal	(%r12,%r12), %r12d
	call	bn_scatter5@PLT
	popq	%rax
	popq	%rdx
	.p2align 4,,10
	.p2align 3
.L417:
	movq	-112(%rbp), %rdi
	movl	%r13d, %r9d
	movq	%rbx, %r8
	movq	%r14, %rcx
	movq	%rdi, %rdx
	movq	%rdi, %rsi
	call	bn_mul_mont@PLT
	movq	-120(%rbp), %rsi
	movq	-112(%rbp), %rdi
	movslq	%r12d, %rcx
	movq	%r15, %rdx
	addl	%r12d, %r12d
	call	bn_scatter5@PLT
	cmpl	$31, %r12d
	jle	.L417
	addq	$2, -176(%rbp)
	movq	-176(%rbp), %rax
	cmpq	$9, %rax
	jne	.L418
	movq	%rax, %r12
.L419:
	subq	$8, %rsp
	leal	-1(%r12), %eax
	movq	-80(%rbp), %rsi
	movl	%r13d, %r9d
	pushq	%rax
	movq	-112(%rbp), %rdi
	movq	%rbx, %r8
	movq	%r14, %rcx
	movq	%r15, %rdx
	call	bn_mul_mont_gather5@PLT
	movq	-120(%rbp), %rsi
	movq	-112(%rbp), %rdi
	movq	%r12, %rcx
	movq	%r15, %rdx
	call	bn_scatter5@PLT
	movq	-112(%rbp), %rdi
	movl	%r13d, %r9d
	movq	%rbx, %r8
	movq	%r14, %rcx
	movq	%rdi, %rdx
	movq	%rdi, %rsi
	call	bn_mul_mont@PLT
	movq	-120(%rbp), %rsi
	movq	-112(%rbp), %rdi
	movq	%r15, %rdx
	leaq	(%r12,%r12), %rcx
	addq	$2, %r12
	call	bn_scatter5@PLT
	popq	%r10
	popq	%r11
	cmpq	$17, %r12
	jne	.L419
	movq	%rbx, %rax
	movl	%r12d, %ebx
	movq	%rax, %r8
	.p2align 4,,10
	.p2align 3
.L420:
	subq	$8, %rsp
	leal	-1(%rbx), %edx
	movq	-80(%rbp), %rsi
	movq	-112(%rbp), %rdi
	pushq	%rdx
	movl	%r13d, %r9d
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r8, -176(%rbp)
	addl	$2, %ebx
	call	bn_mul_mont_gather5@PLT
	movq	-120(%rbp), %rsi
	movq	-112(%rbp), %rdi
	movq	%r12, %rcx
	movq	%r15, %rdx
	addq	$2, %r12
	call	bn_scatter5@PLT
	cmpl	$31, %ebx
	popq	%r8
	popq	%r9
	movq	-176(%rbp), %r8
	jle	.L420
	movl	-168(%rbp), %esi
	movl	%r13d, %ebx
	movq	%r8, %r13
	xorl	%r8d, %r8d
	leal	-1(%rsi), %ecx
	movslq	%ecx, %rax
	movl	%ecx, %edx
	imulq	$1717986919, %rax, %rax
	sarl	$31, %edx
	sarq	$33, %rax
	subl	%edx, %eax
	leal	(%rax,%rax,4), %eax
	subl	%eax, %ecx
	addl	$1, %ecx
	subl	%ecx, %esi
	movl	%esi, %r12d
	cmpl	$-63, %esi
	jl	.L421
	testl	%esi, %esi
	leal	63(%rsi), %edx
	movq	-136(%rbp), %rdi
	cmovns	%esi, %edx
	movl	8(%rdi), %esi
	sarl	$6, %edx
	cmpl	%esi, %edx
	jge	.L421
	movq	(%rdi), %rdi
	movl	$1, %eax
	sall	%cl, %eax
	movslq	%edx, %rcx
	movq	(%rdi,%rcx,8), %r8
	subl	$1, %eax
	leaq	0(,%rcx,8), %r11
	testb	$63, %r12b
	je	.L567
	movl	%r12d, %r9d
	addl	$1, %edx
	sarl	$31, %r9d
	shrl	$26, %r9d
	leal	(%r12,%r9), %ecx
	andl	$63, %ecx
	subl	%r9d, %ecx
	shrq	%cl, %r8
	cmpl	%edx, %esi
	jle	.L567
	movl	$64, %edx
	subl	%ecx, %edx
	movl	%edx, %ecx
	movq	8(%rdi,%r11), %rdx
	salq	%cl, %rdx
	orq	%rdx, %r8
	andl	%r8d, %eax
	movslq	%eax, %r8
	.p2align 4,,10
	.p2align 3
.L421:
	movq	-120(%rbp), %rsi
	movq	-112(%rbp), %rdi
	movq	%r8, %rcx
	movq	%r15, %rdx
	call	bn_gather5@PLT
	testb	$7, %bl
	je	.L568
	testl	%r12d, %r12d
	jle	.L428
	.p2align 4,,10
	.p2align 3
.L429:
	movq	-112(%rbp), %rdi
	movl	%ebx, %r9d
	movq	%r13, %r8
	movq	%r14, %rcx
	subl	$5, %r12d
	movq	%rdi, %rdx
	movq	%rdi, %rsi
	call	bn_mul_mont@PLT
	movq	-112(%rbp), %rdi
	movl	%ebx, %r9d
	movq	%r13, %r8
	movq	%r14, %rcx
	movq	%rdi, %rdx
	movq	%rdi, %rsi
	call	bn_mul_mont@PLT
	movq	-112(%rbp), %rdi
	movl	%ebx, %r9d
	movq	%r13, %r8
	movq	%r14, %rcx
	movq	%rdi, %rdx
	movq	%rdi, %rsi
	call	bn_mul_mont@PLT
	movq	-112(%rbp), %rdi
	movl	%ebx, %r9d
	movq	%r13, %r8
	movq	%r14, %rcx
	movq	%rdi, %rdx
	movq	%rdi, %rsi
	call	bn_mul_mont@PLT
	movq	-112(%rbp), %rdi
	movl	%ebx, %r9d
	movq	%r13, %r8
	movq	%r14, %rcx
	movq	%rdi, %rdx
	movq	%rdi, %rsi
	call	bn_mul_mont@PLT
	movq	-136(%rbp), %rax
	movl	%r12d, %esi
	movq	(%rax), %rdi
	call	bn_get_bits5@PLT
	subq	$8, %rsp
	movq	-112(%rbp), %rdi
	movl	%ebx, %r9d
	pushq	%rax
	movq	%r13, %r8
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%rdi, %rsi
	call	bn_mul_mont_gather5@PLT
	popq	%rsi
	popq	%rdi
	testl	%r12d, %r12d
	jg	.L429
.L428:
	movq	-112(%rbp), %rdi
	movq	%r14, %rcx
	xorl	%edx, %edx
	movl	%ebx, %r9d
	movq	%r13, %r8
	leaq	-112(%rbp), %r12
	movq	%rdi, %rsi
	call	bn_from_montgomery@PLT
	movq	%r12, %rdi
	movl	%ebx, -104(%rbp)
	movl	%eax, %r14d
	call	bn_correct_top@PLT
	testl	%r14d, %r14d
	jne	.L583
.L427:
	movq	-128(%rbp), %rcx
	movq	-152(%rbp), %rdx
	movq	%r12, %rsi
	xorl	%r14d, %r14d
	movq	-160(%rbp), %rdi
	call	BN_from_montgomery@PLT
	testl	%eax, %eax
	setne	%r14b
.L430:
	cmpq	$0, -144(%rbp)
	je	.L584
.L458:
	movq	-192(%rbp), %rsi
	movq	%r15, %rdi
	call	OPENSSL_cleanse@PLT
	movq	-208(%rbp), %rdi
	movl	$1123, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L372:
	xorl	%r14d, %r14d
.L380:
	cmpq	$0, -144(%rbp)
	je	.L370
.L456:
	movq	-128(%rbp), %rdi
	call	BN_CTX_end@PLT
.L363:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L585
	leaq	-40(%rbp), %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L572:
	.cfi_restore_state
	movl	$1, %esi
	movq	%r12, %rdi
	call	BN_abs_is_word@PLT
	testl	%eax, %eax
	je	.L367
	movq	-160(%rbp), %rdi
	xorl	%esi, %esi
	movl	$1, %r14d
	call	BN_set_word@PLT
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L571:
	movl	$614, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$102, %edx
	movl	%eax, %r14d
	movl	$124, %esi
	movl	$3, %edi
	call	ERR_put_error@PLT
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L367:
	movq	-160(%rbp), %rdi
	movl	$1, %esi
	call	BN_set_word@PLT
	movl	%eax, %r14d
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L574:
	movq	-136(%rbp), %rax
	cmpl	$16, 8(%rax)
	jne	.L377
	movq	%r12, %rdi
	call	BN_num_bits@PLT
	cmpl	$1024, %eax
	je	.L378
.L566:
	movl	8(%r14), %eax
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L573:
	call	BN_MONT_CTX_new@PLT
	movq	%rax, -152(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L369
	movq	-128(%rbp), %rdx
	movq	%r12, %rsi
	call	BN_MONT_CTX_set@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	jne	.L368
.L370:
	movq	-152(%rbp), %rdi
	call	BN_MONT_CTX_free@PLT
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L425:
	movq	-136(%rbp), %rax
	subl	$5, %r12d
	movl	%r12d, %esi
	movq	(%rax), %rdi
	call	bn_get_bits5@PLT
	subq	$8, %rsp
	movq	-112(%rbp), %rdi
	movq	%r14, %rcx
	pushq	%rax
	movq	%r15, %rdx
	movl	%ebx, %r9d
	movq	%r13, %r8
	movq	%rdi, %rsi
	call	bn_power5@PLT
	popq	%rdx
	popq	%rcx
.L568:
	testl	%r12d, %r12d
	jg	.L425
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L404:
	cmpl	%ecx, %ebx
	cmovle	%ebx, %ecx
	testl	%ecx, %ecx
	jle	.L435
	movslq	-176(%rbp), %rdi
	subl	$1, %ecx
	movq	%r9, %rax
	movq	%r15, %rdx
	leaq	8(%r9,%rcx,8), %r8
	salq	$3, %rdi
	.p2align 4,,10
	.p2align 3
.L434:
	movq	(%rax), %rcx
	addq	$8, %rax
	movq	%rcx, (%rdx)
	addq	%rdi, %rdx
	cmpq	%rax, %r8
	jne	.L434
.L435:
	cmpl	%ebx, %esi
	cmovg	%ebx, %esi
	testl	%esi, %esi
	jle	.L433
	movslq	-176(%rbp), %rdi
	leal	-1(%rsi), %ecx
	movq	%r12, %rax
	leaq	8(%r15), %rdx
	leaq	8(%r12,%rcx,8), %rsi
	salq	$3, %rdi
	.p2align 4,,10
	.p2align 3
.L438:
	movq	(%rax), %rcx
	addq	$8, %rax
	movq	%rcx, (%rdx)
	addq	%rdi, %rdx
	cmpq	%rax, %rsi
	jne	.L438
.L433:
	cmpl	$1, -184(%rbp)
	leaq	-112(%rbp), %r12
	je	.L437
	movq	-200(%rbp), %rsi
	movq	-128(%rbp), %r8
	movq	%r12, %rdi
	movq	-152(%rbp), %rcx
	movq	%rsi, %rdx
	call	bn_mul_mont_fixed_top@PLT
	testl	%eax, %eax
	je	.L403
	cmpl	%ebx, -104(%rbp)
	movl	%ebx, %ecx
	cmovle	-104(%rbp), %ecx
	movq	-112(%rbp), %rax
	testl	%ecx, %ecx
	jle	.L444
	movslq	-176(%rbp), %rsi
	subl	$1, %ecx
	leaq	16(%r15), %rdx
	leaq	8(%rax,%rcx,8), %rdi
	salq	$3, %rsi
	.p2align 4,,10
	.p2align 3
.L443:
	movq	(%rax), %rcx
	addq	$8, %rax
	movq	%rcx, (%rdx)
	addq	%rsi, %rdx
	cmpq	%rdi, %rax
	jne	.L443
.L444:
	cmpl	$3, -176(%rbp)
	jle	.L437
	movslq	-176(%rbp), %r13
	leaq	24(%r15), %r9
	movq	%r15, -120(%rbp)
	movl	$3, %r14d
	movl	%ebx, %r15d
	movq	%r12, %rbx
	movq	%r9, %r12
	salq	$3, %r13
	.p2align 4,,10
	.p2align 3
.L446:
	movq	-128(%rbp), %r8
	movq	-152(%rbp), %rcx
	movq	%rbx, %rdx
	movq	%rbx, %rdi
	movq	-200(%rbp), %rsi
	call	bn_mul_mont_fixed_top@PLT
	testl	%eax, %eax
	je	.L561
	cmpl	%r15d, -104(%rbp)
	movl	%r15d, %ecx
	cmovle	-104(%rbp), %ecx
	movq	-112(%rbp), %rax
	testl	%ecx, %ecx
	jle	.L448
	subl	$1, %ecx
	leaq	8(%rax,%rcx,8), %rdi
	movq	%r12, %rcx
	.p2align 4,,10
	.p2align 3
.L447:
	movq	(%rax), %rdx
	addq	$8, %rax
	movq	%rdx, (%rcx)
	addq	%r13, %rcx
	cmpq	%rax, %rdi
	jne	.L447
.L448:
	addl	$1, %r14d
	addq	$8, %r12
	cmpl	-176(%rbp), %r14d
	jne	.L446
	movq	%rbx, %r12
	movl	%r15d, %ebx
	movq	-120(%rbp), %r15
.L437:
	movl	-168(%rbp), %esi
	xorl	%r9d, %r9d
	leal	-1(%rsi), %eax
	cltd
	idivl	-184(%rbp)
	leal	1(%rdx), %ecx
	subl	%ecx, %esi
	movl	%esi, %r13d
	cmpl	$-63, %esi
	jl	.L440
	testl	%esi, %esi
	leal	63(%rsi), %eax
	movq	-136(%rbp), %rdi
	cmovns	%esi, %eax
	xorl	%r9d, %r9d
	movl	8(%rdi), %esi
	sarl	$6, %eax
	cmpl	%esi, %eax
	jge	.L440
	movl	$1, %edx
	movq	(%rdi), %rdi
	sall	%cl, %edx
	leal	-1(%rdx), %r9d
	movslq	%eax, %rdx
	leaq	0(,%rdx,8), %r8
	movq	(%rdi,%rdx,8), %rdx
	testb	$63, %r13b
	je	.L569
	movl	%r13d, %r10d
	addl	$1, %eax
	sarl	$31, %r10d
	shrl	$26, %r10d
	leal	0(%r13,%r10), %ecx
	andl	$63, %ecx
	subl	%r10d, %ecx
	shrq	%cl, %rdx
	cmpl	%eax, %esi
	jle	.L569
	movl	$64, %eax
	subl	%ecx, %eax
	movl	%eax, %ecx
	movq	8(%rdi,%r8), %rax
	salq	%cl, %rax
	orq	%rax, %rdx
.L569:
	andl	%edx, %r9d
.L440:
	movl	-184(%rbp), %r8d
	movl	%r9d, %ecx
	movq	%r15, %rdx
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	MOD_EXP_CTIME_COPY_FROM_PREBUF
	testl	%eax, %eax
	je	.L403
	movl	-176(%rbp), %eax
	movq	-128(%rbp), %r14
	movl	%r13d, -120(%rbp)
	movl	%ebx, -176(%rbp)
	movq	-152(%rbp), %rbx
	subl	$1, %eax
	movq	%r15, -168(%rbp)
	movq	%r12, %r15
	movl	-184(%rbp), %r12d
	movl	%eax, -216(%rbp)
.L451:
	movl	-120(%rbp), %eax
	testl	%eax, %eax
	jle	.L586
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L452:
	movq	%r14, %r8
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	bn_mul_mont_fixed_top@PLT
	testl	%eax, %eax
	je	.L564
	addl	$1, %r13d
	cmpl	%r13d, %r12d
	jne	.L452
	subl	%r12d, -120(%rbp)
	movl	-120(%rbp), %r11d
	movq	-136(%rbp), %rdx
	testl	%r11d, %r11d
	leal	63(%r11), %eax
	cmovns	%r11d, %eax
	movl	8(%rdx), %esi
	xorl	%ecx, %ecx
	sarl	$6, %eax
	cmpl	%esi, %eax
	jge	.L453
	movq	(%rdx), %rdi
	movslq	%eax, %rdx
	leaq	0(,%rdx,8), %r8
	movq	(%rdi,%rdx,8), %rdx
	testb	$63, %r11b
	je	.L570
	movl	%r11d, %ecx
	sarl	$31, %r11d
	addl	$1, %eax
	shrl	$26, %r11d
	addl	%r11d, %ecx
	andl	$63, %ecx
	subl	%r11d, %ecx
	shrq	%cl, %rdx
	cmpl	%eax, %esi
	jle	.L570
	movl	$64, %eax
	subl	%ecx, %eax
	movl	%eax, %ecx
	movq	8(%rdi,%r8), %rax
	salq	%cl, %rax
	movq	%rax, %rcx
	orq	%rdx, %rcx
	andl	-216(%rbp), %ecx
.L453:
	movl	%r13d, %r8d
	movq	-200(%rbp), %r13
	movq	-168(%rbp), %rdx
	movl	-176(%rbp), %esi
	movq	%r13, %rdi
	call	MOD_EXP_CTIME_COPY_FROM_PREBUF
	testl	%eax, %eax
	je	.L564
	movq	%r14, %r8
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	bn_mul_mont_fixed_top@PLT
	testl	%eax, %eax
	jne	.L451
	.p2align 4,,10
	.p2align 3
.L564:
	movq	-168(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L403:
	xorl	%r14d, %r14d
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L393:
	movq	%r8, -200(%rbp)
	leaq	-112(%rbp), %r12
	call	BN_value_one@PLT
	movq	-128(%rbp), %rcx
	movq	-152(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	bn_to_mont_fixed_top@PLT
	movq	-200(%rbp), %r8
	testl	%eax, %eax
	jne	.L395
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L575:
	movq	-136(%rbp), %rax
	cmpl	$8, 8(%rax)
	jne	.L377
	movq	%r12, %rdi
	call	BN_num_bits@PLT
	cmpl	$512, %eax
	jne	.L377
	movq	-160(%rbp), %rbx
	movl	$8, %esi
	movq	%rbx, %rdi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L372
	movq	-152(%rbp), %rax
	movq	(%r14), %rsi
	movl	$1, %r14d
	movq	(%r12), %rcx
	movq	(%rbx), %rdi
	movq	8(%rax), %r9
	movq	80(%rax), %r8
	movq	-136(%rbp), %rax
	movq	(%rax), %rdx
	call	RSAZ_512_mod_exp@PLT
	movl	$8, 8(%rbx)
	movq	%rbx, %rdi
	movl	$0, 16(%rbx)
	call	bn_correct_top@PLT
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L383:
	movl	$723, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r9, -192(%rbp)
	movl	%ecx, -120(%rbp)
	call	CRYPTO_malloc@PLT
	movl	-120(%rbp), %ecx
	movq	-192(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, -208(%rbp)
	je	.L372
	andq	$-64, %rax
	xorl	%esi, %esi
	movq	%r9, -120(%rbp)
	leaq	64(%rax), %r15
	movslq	%ecx, %rax
	movq	%rax, %rdx
	movq	%r15, %rdi
	movq	%rax, -192(%rbp)
	call	memset@PLT
	movq	-120(%rbp), %r9
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L576:
	movl	-168(%rbp), %esi
	xorl	%r13d, %r13d
	cmpl	$89, %esi
	jg	.L462
	cmpl	$22, %esi
	movl	$2, %eax
	movl	$8, %edx
	movl	$8, %r9d
	cmovle	%rax, %r9
	cmovg	%edx, %eax
	movl	$0, %edx
	movl	%eax, -176(%rbp)
	setg	%al
	movzbl	%al, %eax
	leal	1(%rax,%rax), %eax
	movl	%eax, -184(%rbp)
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L462:
	movl	$16, -176(%rbp)
	movl	$16, %r9d
	xorl	%edx, %edx
	movl	$4, -184(%rbp)
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L582:
	movb	$0, (%rdi)
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L581:
	xorl	%ecx, %ecx
	addq	$2, %rdi
	movw	%cx, -2(%rdi)
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L580:
	movl	$0, (%rdi)
	addq	$4, %rdi
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L578:
	orq	$0, -8(%rsp,%rax)
	jmp	.L386
.L579:
	movq	%rax, %rcx
	xorl	%eax, %eax
	shrq	$3, %rcx
	rep stosq
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L369:
	xorl	%edi, %edi
	xorl	%r14d, %r14d
	call	BN_MONT_CTX_free@PLT
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L396:
	leaq	2(%rax), %rdi
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L401:
	movq	(%rdx,%rax,8), %rsi
	notq	%rsi
	movq	%rsi, (%rcx,%rax,8)
	addq	$1, %rax
	cmpq	%rax, %rdi
	jne	.L401
	jmp	.L399
.L584:
	movq	-152(%rbp), %rdi
	call	BN_MONT_CTX_free@PLT
	jmp	.L458
.L561:
	movq	-120(%rbp), %r15
	jmp	.L403
.L583:
	movq	-160(%rbp), %rdi
	movq	%r12, %rsi
	call	BN_copy@PLT
	testq	%rax, %rax
	jne	.L430
	jmp	.L403
.L469:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L411:
	movq	(%rdx,%rax,8), %rcx
	movq	%rcx, (%r14,%rax,8)
	addq	$1, %rax
	cmpl	%eax, %ebx
	jg	.L411
	jmp	.L413
.L567:
	andl	%eax, %r8d
	movslq	%r8d, %r8
	jmp	.L421
.L570:
	movl	-216(%rbp), %ecx
	andl	%edx, %ecx
	jmp	.L453
.L378:
	call	rsaz_avx2_eligible@PLT
	testl	%eax, %eax
	je	.L566
	movq	-160(%rbp), %rbx
	movl	$16, %esi
	movq	%rbx, %rdi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L372
	movq	-152(%rbp), %rax
	movq	(%r14), %rsi
	movl	$1, %r14d
	movq	(%r12), %rcx
	movq	(%rbx), %rdi
	movq	80(%rax), %r9
	movq	8(%rax), %r8
	movq	-136(%rbp), %rax
	movq	(%rax), %rdx
	call	RSAZ_1024_mod_exp_avx2@PLT
	movl	$16, 8(%rbx)
	movq	%rbx, %rdi
	movl	$0, 16(%rbx)
	call	bn_correct_top@PLT
	jmp	.L380
.L586:
	movq	%r15, %r12
	movq	-168(%rbp), %r15
	jmp	.L427
.L585:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE295:
	.size	BN_mod_exp_mont_consttime, .-BN_mod_exp_mont_consttime
	.p2align 4
	.globl	BN_mod_exp_mont
	.type	BN_mod_exp_mont, @function
BN_mod_exp_mont:
.LFB291:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$4, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$24, %rsp
	movq	%rdi, -56(%rbp)
	movq	%rdx, %rdi
	call	BN_get_flags@PLT
	testl	%eax, %eax
	jne	.L589
	movl	$4, %esi
	movq	%r12, %rdi
	call	BN_get_flags@PLT
	testl	%eax, %eax
	je	.L591
.L589:
	movq	-56(%rbp), %rdi
	addq	$24, %rsp
	movq	%r15, %r9
	movq	%rbx, %r8
	movq	%r13, %rcx
	popq	%rbx
	movq	%r14, %rdx
	movq	%r12, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BN_mod_exp_mont_consttime
	.p2align 4,,10
	.p2align 3
.L591:
	.cfi_restore_state
	movl	$4, %esi
	movq	%r13, %rdi
	call	BN_get_flags@PLT
	testl	%eax, %eax
	jne	.L589
	movq	-56(%rbp), %rdi
	addq	$24, %rsp
	movq	%r15, %r9
	movq	%rbx, %r8
	movq	%r13, %rcx
	popq	%rbx
	movq	%r14, %rdx
	movq	%r12, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	BN_mod_exp_mont.part.0
	.cfi_endproc
.LFE291:
	.size	BN_mod_exp_mont, .-BN_mod_exp_mont
	.p2align 4
	.globl	BN_mod_exp
	.type	BN_mod_exp, @function
BN_mod_exp:
.LFB289:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%rcx, %rdi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$8, %rsp
	call	BN_is_odd@PLT
	testl	%eax, %eax
	je	.L593
	cmpl	$1, 8(%r12)
	jne	.L594
	movl	16(%r12), %eax
	testl	%eax, %eax
	je	.L604
.L594:
	movl	$4, %esi
	movq	%r14, %rdi
	call	BN_get_flags@PLT
	testl	%eax, %eax
	jne	.L600
	movl	$4, %esi
	movq	%r12, %rdi
	call	BN_get_flags@PLT
	testl	%eax, %eax
	jne	.L600
	movl	$4, %esi
	movq	%r13, %rdi
	call	BN_get_flags@PLT
	testl	%eax, %eax
	je	.L605
.L600:
	addq	$8, %rsp
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	%r14, %rdx
	popq	%rbx
	movq	%r12, %rsi
	movq	%r15, %rdi
	popq	%r12
	xorl	%r9d, %r9d
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BN_mod_exp_mont_consttime
	.p2align 4,,10
	.p2align 3
.L593:
	.cfi_restore_state
	movl	$4, %esi
	movq	%r14, %rdi
	call	BN_get_flags@PLT
	testl	%eax, %eax
	jne	.L602
	movl	$4, %esi
	movq	%r12, %rdi
	call	BN_get_flags@PLT
	testl	%eax, %eax
	je	.L606
.L602:
	movl	$175, %r8d
	movl	$66, %edx
	movl	$125, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L592:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L604:
	.cfi_restore_state
	movl	$4, %esi
	movq	%r14, %rdi
	call	BN_get_flags@PLT
	testl	%eax, %eax
	jne	.L594
	movl	$4, %esi
	movq	%r12, %rdi
	call	BN_get_flags@PLT
	testl	%eax, %eax
	jne	.L594
	movl	$4, %esi
	movq	%r13, %rdi
	call	BN_get_flags@PLT
	testl	%eax, %eax
	jne	.L594
	movq	(%r12), %rax
	movl	$4, %esi
	movq	%r14, %rdi
	movq	(%rax), %r12
	call	BN_get_flags@PLT
	testl	%eax, %eax
	jne	.L597
	movl	$4, %esi
	movq	%r13, %rdi
	call	BN_get_flags@PLT
	testl	%eax, %eax
	jne	.L597
	movq	%r13, %rdi
	call	BN_is_odd@PLT
	testl	%eax, %eax
	je	.L607
	addq	$8, %rsp
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	%r14, %rdx
	popq	%rbx
	movq	%r12, %rsi
	movq	%r15, %rdi
	popq	%r12
	xorl	%r9d, %r9d
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BN_mod_exp_mont_word.part.0
	.p2align 4,,10
	.p2align 3
.L606:
	.cfi_restore_state
	movl	$4, %esi
	movq	%r13, %rdi
	call	BN_get_flags@PLT
	testl	%eax, %eax
	jne	.L602
	addq	$8, %rsp
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	%r14, %rdx
	popq	%rbx
	movq	%r12, %rsi
	movq	%r15, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BN_mod_exp_recp.part.0
	.p2align 4,,10
	.p2align 3
.L605:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	%r14, %rdx
	popq	%rbx
	movq	%r12, %rsi
	movq	%r15, %rdi
	popq	%r12
	xorl	%r9d, %r9d
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BN_mod_exp_mont.part.0
.L597:
	.cfi_restore_state
	movl	$1158, %r8d
	movl	$66, %edx
	movl	$117, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L592
.L607:
	movl	$1166, %r8d
	movl	$102, %edx
	movl	$117, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L592
	.cfi_endproc
.LFE289:
	.size	BN_mod_exp, .-BN_mod_exp
	.p2align 4
	.globl	BN_mod_exp_mont_word
	.type	BN_mod_exp_mont_word, @function
BN_mod_exp_mont_word:
.LFB296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	$4, %esi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$24, %rsp
	movq	%rdi, -56(%rbp)
	movq	%rdx, %rdi
	call	BN_get_flags@PLT
	testl	%eax, %eax
	jne	.L611
	movl	$4, %esi
	movq	%r13, %rdi
	call	BN_get_flags@PLT
	testl	%eax, %eax
	je	.L614
.L611:
	movl	$1158, %r8d
	movl	$66, %edx
	movl	$117, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L608:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L614:
	.cfi_restore_state
	movq	%r13, %rdi
	call	BN_is_odd@PLT
	testl	%eax, %eax
	je	.L615
	movq	-56(%rbp), %rdi
	addq	$24, %rsp
	movq	%r15, %r9
	movq	%rbx, %r8
	movq	%r13, %rcx
	popq	%rbx
	movq	%r12, %rdx
	movq	%r14, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BN_mod_exp_mont_word.part.0
	.p2align 4,,10
	.p2align 3
.L615:
	.cfi_restore_state
	movl	$1166, %r8d
	movl	$102, %edx
	movl	$117, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L608
	.cfi_endproc
.LFE296:
	.size	BN_mod_exp_mont_word, .-BN_mod_exp_mont_word
	.p2align 4
	.globl	BN_mod_exp_simple
	.type	BN_mod_exp_simple, @function
BN_mod_exp_simple:
.LFB297:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$4, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$344, %rsp
	movq	%rdi, -328(%rbp)
	movq	%rdx, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	BN_get_flags@PLT
	testl	%eax, %eax
	jne	.L619
	movl	$4, %esi
	movq	%r13, %rdi
	call	BN_get_flags@PLT
	testl	%eax, %eax
	je	.L693
.L619:
	movl	$1290, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$66, %edx
	xorl	%r15d, %r15d
	movl	$126, %esi
	movl	$3, %edi
	call	ERR_put_error@PLT
.L616:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L692
	addq	$344, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L693:
	.cfi_restore_state
	movl	$4, %esi
	movq	%r14, %rdi
	call	BN_get_flags@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	jne	.L619
	movq	%rbx, %rdi
	call	BN_num_bits@PLT
	movl	%eax, -332(%rbp)
	testl	%eax, %eax
	je	.L694
	movq	%r12, %rdi
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -352(%rbp)
	call	BN_CTX_get@PLT
	movq	%rax, -320(%rbp)
	testq	%rax, %rax
	je	.L624
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -344(%rbp)
	call	BN_nnmod@PLT
	testl	%eax, %eax
	jne	.L695
.L624:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	jmp	.L616
	.p2align 4,,10
	.p2align 3
.L694:
	movl	$1, %esi
	movq	%r14, %rdi
	call	BN_abs_is_word@PLT
	testl	%eax, %eax
	jne	.L696
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L692
	movq	-328(%rbp), %rdi
	addq	$344, %rsp
	movl	$1, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BN_set_word@PLT
	.p2align 4,,10
	.p2align 3
.L695:
	.cfi_restore_state
	movq	-344(%rbp), %r10
	movq	%r10, %rdi
	call	BN_is_zero@PLT
	movq	-344(%rbp), %r10
	testl	%eax, %eax
	jne	.L697
	movl	-332(%rbp), %eax
	cmpl	$671, %eax
	jg	.L646
	cmpl	$239, %eax
	jg	.L647
	cmpl	$79, %eax
	jg	.L648
	cmpl	$23, %eax
	jle	.L649
	movl	$4, -344(%rbp)
	movl	$3, -336(%rbp)
	.p2align 4,,10
	.p2align 3
.L627:
	movq	-352(%rbp), %rdi
	movq	%r12, %r8
	movq	%r14, %rcx
	movq	%r10, %rdx
	movq	%r10, %rsi
	call	BN_mod_mul@PLT
	testl	%eax, %eax
	je	.L624
	leaq	-320(%rbp), %rax
	movl	%r15d, -356(%rbp)
	movl	$1, %r13d
	movq	-352(%rbp), %r15
	movq	%rbx, -352(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L629:
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, (%rbx,%r13,8)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L687
	movq	-8(%rbx,%r13,8), %rsi
	movq	%r12, %r8
	movq	%r14, %rcx
	movq	%r15, %rdx
	call	BN_mod_mul@PLT
	testl	%eax, %eax
	je	.L687
	addq	$1, %r13
	cmpl	%r13d, -344(%rbp)
	jg	.L629
	movl	-356(%rbp), %r15d
	movq	-352(%rbp), %rbx
.L628:
	movq	-328(%rbp), %rdi
	movl	$1, %esi
	call	BN_set_word@PLT
	testl	%eax, %eax
	je	.L624
	movl	-332(%rbp), %r13d
	movl	%r15d, -352(%rbp)
	movl	$1, -332(%rbp)
	movq	%r14, -344(%rbp)
	movq	-328(%rbp), %r14
	subl	$1, %r13d
.L630:
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	BN_is_bit_set@PLT
	testl	%eax, %eax
	jne	.L698
	movl	-332(%rbp), %edx
	testl	%edx, %edx
	je	.L634
.L637:
	testl	%r13d, %r13d
	jne	.L699
.L635:
	movl	$1, %r15d
	jmp	.L624
	.p2align 4,,10
	.p2align 3
.L696:
	movq	-328(%rbp), %rdi
	xorl	%esi, %esi
	movl	$1, %r15d
	call	BN_set_word@PLT
	jmp	.L616
	.p2align 4,,10
	.p2align 3
.L687:
	movl	-356(%rbp), %r15d
	jmp	.L624
.L647:
	movl	$16, -344(%rbp)
	movl	$5, -336(%rbp)
	jmp	.L627
.L646:
	movl	$32, -344(%rbp)
	movl	$6, -336(%rbp)
	jmp	.L627
.L697:
	movq	-328(%rbp), %rdi
	xorl	%esi, %esi
	movl	$1, %r15d
	call	BN_set_word@PLT
	jmp	.L624
.L648:
	movl	$8, -344(%rbp)
	movl	$4, -336(%rbp)
	jmp	.L627
.L698:
	cmpl	$1, -336(%rbp)
	je	.L650
	testl	%r13d, %r13d
	jle	.L651
	leal	1(%r13), %eax
	xorl	%edx, %edx
	movq	%r14, -368(%rbp)
	movl	$1, %r15d
	movl	%eax, -328(%rbp)
	movl	$1, %eax
	movl	%edx, %r14d
	movq	%r12, -376(%rbp)
	movl	%eax, %r12d
	jmp	.L633
	.p2align 4,,10
	.p2align 3
.L640:
	cmpl	%r15d, -328(%rbp)
	je	.L688
.L633:
	movl	%r13d, %esi
	movq	%rbx, %rdi
	subl	%r15d, %esi
	call	BN_is_bit_set@PLT
	testl	%eax, %eax
	je	.L639
	movl	%r15d, %ecx
	subl	%r14d, %ecx
	movl	%r15d, %r14d
	sall	%cl, %r12d
	orl	$1, %r12d
.L639:
	addl	$1, %r15d
	cmpl	-336(%rbp), %r15d
	jne	.L640
.L688:
	movl	%r14d, -328(%rbp)
	movq	-368(%rbp), %r14
	movl	%r12d, -356(%rbp)
	movq	-376(%rbp), %r12
.L632:
	movl	-332(%rbp), %eax
	testl	%eax, %eax
	je	.L641
.L644:
	movl	-356(%rbp), %eax
	movq	%r12, %r8
	movq	%r14, %rsi
	movq	%r14, %rdi
	movq	-344(%rbp), %rcx
	sarl	%eax
	cltq
	movq	-320(%rbp,%rax,8), %rdx
	call	BN_mod_mul@PLT
	testl	%eax, %eax
	je	.L691
	movl	-328(%rbp), %eax
	addl	$1, %eax
	subl	%eax, %r13d
	js	.L635
	movl	$0, -332(%rbp)
	jmp	.L630
.L634:
	movq	-344(%rbp), %rcx
	movq	%r12, %r8
	movq	%r14, %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	BN_mod_mul@PLT
	testl	%eax, %eax
	jne	.L637
.L691:
	movl	-352(%rbp), %r15d
	jmp	.L624
	.p2align 4,,10
	.p2align 3
.L649:
	movl	$1, -336(%rbp)
	jmp	.L628
.L699:
	subl	$1, %r13d
	jmp	.L630
.L641:
	movl	-328(%rbp), %eax
	movl	%r13d, -368(%rbp)
	movq	%r12, %r13
	movq	%rbx, -376(%rbp)
	movl	-332(%rbp), %r12d
	movq	-344(%rbp), %rbx
	leal	1(%rax), %r15d
.L643:
	movq	%r13, %r8
	movq	%rbx, %rcx
	movq	%r14, %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	BN_mod_mul@PLT
	testl	%eax, %eax
	je	.L689
	addl	$1, %r12d
	cmpl	%r15d, %r12d
	jne	.L643
	movq	%r13, %r12
	movq	-376(%rbp), %rbx
	movl	-368(%rbp), %r13d
	jmp	.L644
.L689:
	movl	-352(%rbp), %r15d
	movq	%r13, %r12
	jmp	.L624
.L692:
	call	__stack_chk_fail@PLT
.L651:
	movl	$0, -328(%rbp)
	movl	$1, -356(%rbp)
	jmp	.L632
.L650:
	movl	$1, -356(%rbp)
	movl	$0, -328(%rbp)
	jmp	.L632
	.cfi_endproc
.LFE297:
	.size	BN_mod_exp_simple, .-BN_mod_exp_simple
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
