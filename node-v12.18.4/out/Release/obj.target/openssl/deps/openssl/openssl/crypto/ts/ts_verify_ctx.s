	.file	"ts_verify_ctx.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ts/ts_verify_ctx.c"
	.text
	.p2align 4
	.type	TS_VERIFY_CTX_cleanup.part.0, @function
TS_VERIFY_CTX_cleanup.part.0:
.LFB1381:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	call	X509_STORE_free@PLT
	movq	X509_free@GOTPCREL(%rip), %rsi
	movq	16(%rbx), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	24(%rbx), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	32(%rbx), %rdi
	call	X509_ALGOR_free@PLT
	movq	40(%rbx), %rdi
	movl	$89, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	56(%rbx), %rdi
	call	BIO_free_all@PLT
	movq	64(%rbx), %rdi
	call	ASN1_INTEGER_free@PLT
	movq	72(%rbx), %rdi
	call	GENERAL_NAME_free@PLT
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 64(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1381:
	.size	TS_VERIFY_CTX_cleanup.part.0, .-TS_VERIFY_CTX_cleanup.part.0
	.p2align 4
	.globl	TS_VERIFY_CTX_new
	.type	TS_VERIFY_CTX_new, @function
TS_VERIFY_CTX_new:
.LFB1368:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$17, %edx
	movl	$80, %edi
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L7
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	movl	$20, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$144, %esi
	movl	$47, %edi
	movq	%rax, -8(%rbp)
	call	ERR_put_error@PLT
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1368:
	.size	TS_VERIFY_CTX_new, .-TS_VERIFY_CTX_new
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"assertion failed: ctx != NULL"
	.text
	.p2align 4
	.globl	TS_VERIFY_CTX_init
	.type	TS_VERIFY_CTX_init, @function
TS_VERIFY_CTX_init:
.LFB1369:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L13
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rdi)
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm0, 64(%rdi)
	ret
.L13:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$26, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE1369:
	.size	TS_VERIFY_CTX_init, .-TS_VERIFY_CTX_init
	.p2align 4
	.globl	TS_VERIFY_CTX_free
	.type	TS_VERIFY_CTX_free, @function
TS_VERIFY_CTX_free:
.LFB1370:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L14
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	TS_VERIFY_CTX_cleanup.part.0
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$36, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L14:
	ret
	.cfi_endproc
.LFE1370:
	.size	TS_VERIFY_CTX_free, .-TS_VERIFY_CTX_free
	.p2align 4
	.globl	TS_VERIFY_CTX_add_flags
	.type	TS_VERIFY_CTX_add_flags, @function
TS_VERIFY_CTX_add_flags:
.LFB1371:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	orl	%esi, %eax
	movl	%eax, (%rdi)
	ret
	.cfi_endproc
.LFE1371:
	.size	TS_VERIFY_CTX_add_flags, .-TS_VERIFY_CTX_add_flags
	.p2align 4
	.globl	TS_VERIFY_CTX_set_flags
	.type	TS_VERIFY_CTX_set_flags, @function
TS_VERIFY_CTX_set_flags:
.LFB1372:
	.cfi_startproc
	endbr64
	movl	%esi, (%rdi)
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE1372:
	.size	TS_VERIFY_CTX_set_flags, .-TS_VERIFY_CTX_set_flags
	.p2align 4
	.globl	TS_VERIFY_CTX_set_data
	.type	TS_VERIFY_CTX_set_data, @function
TS_VERIFY_CTX_set_data:
.LFB1373:
	.cfi_startproc
	endbr64
	movq	%rsi, 56(%rdi)
	movq	%rsi, %rax
	ret
	.cfi_endproc
.LFE1373:
	.size	TS_VERIFY_CTX_set_data, .-TS_VERIFY_CTX_set_data
	.p2align 4
	.globl	TS_VERIFY_CTX_set_store
	.type	TS_VERIFY_CTX_set_store, @function
TS_VERIFY_CTX_set_store:
.LFB1374:
	.cfi_startproc
	endbr64
	movq	%rsi, 8(%rdi)
	movq	%rsi, %rax
	ret
	.cfi_endproc
.LFE1374:
	.size	TS_VERIFY_CTX_set_store, .-TS_VERIFY_CTX_set_store
	.p2align 4
	.globl	TS_VERIFY_CTS_set_certs
	.type	TS_VERIFY_CTS_set_certs, @function
TS_VERIFY_CTS_set_certs:
.LFB1375:
	.cfi_startproc
	endbr64
	movq	%rsi, 16(%rdi)
	movq	%rsi, %rax
	ret
	.cfi_endproc
.LFE1375:
	.size	TS_VERIFY_CTS_set_certs, .-TS_VERIFY_CTS_set_certs
	.p2align 4
	.globl	TS_VERIFY_CTX_set_imprint
	.type	TS_VERIFY_CTX_set_imprint, @function
TS_VERIFY_CTX_set_imprint:
.LFB1376:
	.cfi_startproc
	endbr64
	movq	%rsi, 40(%rdi)
	movq	%rsi, %rax
	movl	%edx, 48(%rdi)
	ret
	.cfi_endproc
.LFE1376:
	.size	TS_VERIFY_CTX_set_imprint, .-TS_VERIFY_CTX_set_imprint
	.p2align 4
	.globl	TS_VERIFY_CTX_cleanup
	.type	TS_VERIFY_CTX_cleanup, @function
TS_VERIFY_CTX_cleanup:
.LFB1377:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L25
	jmp	TS_VERIFY_CTX_cleanup.part.0
	.p2align 4,,10
	.p2align 3
.L25:
	ret
	.cfi_endproc
.LFE1377:
	.size	TS_VERIFY_CTX_cleanup, .-TS_VERIFY_CTX_cleanup
	.section	.rodata.str1.1
.LC2:
	.string	"assertion failed: req != NULL"
	.text
	.p2align 4
	.globl	TS_REQ_to_TS_VERIFY_CTX
	.type	TS_REQ_to_TS_VERIFY_CTX, @function
TS_REQ_to_TS_VERIFY_CTX:
.LFB1378:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdi, %rdi
	je	.L50
	movq	%rdi, %rbx
	movq	%rsi, %r13
	testq	%rsi, %rsi
	je	.L29
	movq	%rsi, %rdi
	movq	%r13, %r12
	call	TS_VERIFY_CTX_cleanup.part.0
.L30:
	movq	16(%rbx), %rdi
	movl	$110, (%r12)
	testq	%rdi, %rdi
	je	.L32
	call	OBJ_dup@PLT
	movq	%rax, 24(%r12)
	testq	%rax, %rax
	je	.L33
.L34:
	movq	8(%rbx), %r14
	movq	(%r14), %rdi
	call	X509_ALGOR_dup@PLT
	movq	%rax, 32(%r12)
	testq	%rax, %rax
	je	.L33
	movq	8(%r14), %r14
	movq	%r14, %rdi
	call	ASN1_STRING_length@PLT
	movl	$129, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, 48(%r12)
	movl	%eax, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, 40(%r12)
	testq	%rax, %rax
	je	.L33
	movl	48(%r12), %r15d
	movq	%r14, %rdi
	call	ASN1_STRING_get0_data@PLT
	movq	40(%r12), %rdi
	movq	%rax, %rsi
	movq	%r15, %rdx
	call	memcpy@PLT
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L35
	call	ASN1_INTEGER_dup@PLT
	movq	%rax, 64(%r12)
	testq	%rax, %rax
	jne	.L27
	.p2align 4,,10
	.p2align 3
.L33:
	testq	%r13, %r13
	je	.L36
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	call	TS_VERIFY_CTX_cleanup.part.0
.L27:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movl	$106, (%r12)
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L29:
	movl	$17, %edx
	leaq	.LC0(%rip), %rsi
	movl	$80, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L30
	movl	$20, %r8d
	movl	$65, %edx
	movl	$144, %esi
	movl	$47, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L36:
	movq	%r12, %rdi
	call	TS_VERIFY_CTX_cleanup.part.0
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movl	$36, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L35:
	andl	$-33, (%r12)
	jmp	.L27
.L50:
	movl	$109, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE1378:
	.size	TS_REQ_to_TS_VERIFY_CTX, .-TS_REQ_to_TS_VERIFY_CTX
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
