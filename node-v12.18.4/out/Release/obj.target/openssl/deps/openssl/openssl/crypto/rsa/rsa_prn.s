	.file	"rsa_prn.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/rsa/rsa_prn.c"
	.text
	.p2align 4
	.globl	RSA_print_fp
	.type	RSA_print_fp, @function
RSA_print_fp:
.LFB419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	testq	%rax, %rax
	je	.L12
	movq	%r13, %rcx
	xorl	%edx, %edx
	movl	$106, %esi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	BIO_ctrl@PLT
	call	EVP_PKEY_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L6
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	EVP_PKEY_set1_RSA@PLT
	testl	%eax, %eax
	jne	.L13
.L6:
	xorl	%r14d, %r14d
.L5:
	movq	%r12, %rdi
	call	BIO_free@PLT
	popq	%r12
	movl	%r14d, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movl	%r15d, %edx
	movq	%r13, %rsi
	call	EVP_PKEY_print_private@PLT
	movq	%r13, %rdi
	movl	%eax, %r14d
	call	EVP_PKEY_free@PLT
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L12:
	xorl	%r14d, %r14d
	movl	$22, %r8d
	movl	$7, %edx
	movl	$116, %esi
	leaq	.LC0(%rip), %rcx
	movl	$4, %edi
	call	ERR_put_error@PLT
	popq	%r12
	movl	%r14d, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE419:
	.size	RSA_print_fp, .-RSA_print_fp
	.p2align 4
	.globl	RSA_print
	.type	RSA_print, @function
RSA_print:
.LFB420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	call	EVP_PKEY_new@PLT
	testq	%rax, %rax
	je	.L17
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	EVP_PKEY_set1_RSA@PLT
	testl	%eax, %eax
	jne	.L22
.L17:
	xorl	%r13d, %r13d
	popq	%r12
	movl	%r13d, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	movl	%r15d, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	xorl	%ecx, %ecx
	call	EVP_PKEY_print_private@PLT
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	EVP_PKEY_free@PLT
	popq	%r12
	movl	%r13d, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE420:
	.size	RSA_print, .-RSA_print
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
