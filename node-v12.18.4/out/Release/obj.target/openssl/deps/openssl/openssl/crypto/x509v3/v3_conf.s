	.file	"v3_conf.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/x509v3/v3_conf.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"name="
.LC2:
	.string	"value="
	.text
	.p2align 4
	.type	v3_generic_extension, @function
v3_generic_extension:
.LFB1305:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	xorl	%esi, %esi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$56, %rsp
	movl	%edx, -84(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -72(%rbp)
	call	OBJ_txt2obj@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L20
	cmpl	$1, %r13d
	je	.L21
	cmpl	$2, %r13d
	jne	.L7
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	$0, -64(%rbp)
	call	ASN1_generate_v3@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L7
	leaq	-64(%rbp), %rsi
	movq	%rax, %rdi
	call	i2d_ASN1_TYPE@PLT
	movq	%r13, %rdi
	cltq
	movq	%rax, -72(%rbp)
	call	ASN1_TYPE_free@PLT
	movq	-64(%rbp), %rbx
.L5:
	testq	%rbx, %rbx
	je	.L7
	call	ASN1_OCTET_STRING_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L22
	movq	%rbx, 8(%rax)
	movq	-72(%rbp), %rax
	movq	%r13, %rcx
	movq	%r12, %rsi
	movl	-84(%rbp), %edx
	xorl	%edi, %edi
	xorl	%ebx, %ebx
	movl	%eax, 0(%r13)
	call	X509_EXTENSION_create_by_OBJ@PLT
	movq	%rax, %r14
.L3:
	movq	%r12, %rdi
	call	ASN1_OBJECT_free@PLT
	movq	%r13, %rdi
	call	ASN1_OCTET_STRING_free@PLT
	movl	$265, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rbx, %rdi
	call	CRYPTO_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L23
	addq	$56, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	movl	$116, %edx
	movl	$116, %esi
	movl	$245, %r8d
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r15, %rdx
	leaq	.LC2(%rip), %rsi
.L18:
	movl	$2, %edi
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	call	ERR_add_error_data@PLT
	xorl	%ebx, %ebx
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L21:
	leaq	-72(%rbp), %rsi
	movq	%r15, %rdi
	call	OPENSSL_hexstr2buf@PLT
	movq	%rax, %rbx
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L22:
	movl	$252, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r14d, %r14d
	movl	$116, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$115, %edx
	movl	$116, %esi
	movl	$233, %r8d
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r14, %rdx
	leaq	.LC1(%rip), %rsi
	jmp	.L18
.L23:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1305:
	.size	v3_generic_extension, .-v3_generic_extension
	.p2align 4
	.type	nconf_get_section, @function
nconf_get_section:
.LFB1317:
	.cfi_startproc
	endbr64
	jmp	NCONF_get_section@PLT
	.cfi_endproc
.LFE1317:
	.size	nconf_get_section, .-nconf_get_section
	.p2align 4
	.type	nconf_get_string, @function
nconf_get_string:
.LFB1316:
	.cfi_startproc
	endbr64
	jmp	NCONF_get_string@PLT
	.cfi_endproc
.LFE1316:
	.size	nconf_get_string, .-nconf_get_string
	.p2align 4
	.type	conf_lhash_get_section, @function
conf_lhash_get_section:
.LFB1323:
	.cfi_startproc
	endbr64
	jmp	CONF_get_section@PLT
	.cfi_endproc
.LFE1323:
	.size	conf_lhash_get_section, .-conf_lhash_get_section
	.p2align 4
	.type	conf_lhash_get_string, @function
conf_lhash_get_string:
.LFB1322:
	.cfi_startproc
	endbr64
	jmp	CONF_get_string@PLT
	.cfi_endproc
.LFE1322:
	.size	conf_lhash_get_string, .-conf_lhash_get_string
	.p2align 4
	.type	do_ext_i2d.isra.0, @function
do_ext_i2d.isra.0:
.LFB1328:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -72(%rbp)
	testq	%rdi, %rdi
	je	.L29
	movq	%rdi, %rdx
	leaq	-72(%rbp), %rsi
	movq	%r8, %rdi
	call	ASN1_item_i2d@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	js	.L30
.L31:
	call	ASN1_OCTET_STRING_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L30
	movq	-72(%rbp), %rax
	movl	%r13d, %esi
	movq	%r12, %rcx
	movl	%r14d, %edx
	movq	$0, -72(%rbp)
	xorl	%edi, %edi
	movq	%rax, 8(%r12)
	movl	%ebx, (%r12)
	call	X509_EXTENSION_create_by_NID@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L33
	movq	%r12, %rdi
	call	ASN1_OCTET_STRING_free@PLT
.L28:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L43
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	xorl	%r12d, %r12d
.L33:
	movl	$168, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$135, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	movq	-72(%rbp), %rdi
	movl	$169, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	call	ASN1_OCTET_STRING_free@PLT
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L29:
	movq	%rsi, %r10
	movq	%r8, %rdi
	xorl	%esi, %esi
	movq	%r10, -88(%rbp)
	call	*(%r10)
	movl	$149, %edx
	leaq	.LC0(%rip), %rsi
	movslq	%eax, %rdi
	movq	%rdi, %rbx
	call	CRYPTO_malloc@PLT
	movq	%rax, -72(%rbp)
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L33
	movq	-88(%rbp), %r10
	movq	%rax, -64(%rbp)
	leaq	-64(%rbp), %rsi
	movq	%r15, %rdi
	call	*(%r10)
	jmp	.L31
.L43:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1328:
	.size	do_ext_i2d.isra.0, .-do_ext_i2d.isra.0
	.section	.rodata.str1.1
.LC3:
	.string	",section="
	.text
	.p2align 4
	.type	do_ext_nconf, @function
do_ext_nconf:
.LFB1300:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%ecx, -52(%rbp)
	testl	%edx, %edx
	je	.L79
	movq	%rdi, %rbx
	movl	%edx, %edi
	movq	%rsi, %r14
	movl	%edx, %r12d
	movq	%r8, %r13
	call	X509V3_EXT_get_nid@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L80
	cmpq	$0, 72(%rax)
	je	.L48
	cmpb	$64, 0(%r13)
	je	.L81
	movq	%r13, %rdi
	call	X509V3_parse_list@PLT
	movq	%rax, %rbx
.L50:
	testq	%rbx, %rbx
	je	.L54
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jle	.L54
	movq	%r14, %rsi
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	*72(%r15)
	cmpb	$64, 0(%r13)
	movq	%rax, %r14
	je	.L75
	movq	X509V3_conf_free@GOTPCREL(%rip), %rsi
	movq	%rbx, %rdi
	call	OPENSSL_sk_pop_free@PLT
.L75:
	testq	%r14, %r14
	je	.L78
	movq	8(%r15), %rdi
	movl	-52(%rbp), %ecx
	leaq	40(%r15), %rsi
	movl	%r12d, %edx
	movq	%r14, %r8
	call	do_ext_i2d.isra.0
	movq	8(%r15), %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	testq	%rsi, %rsi
	je	.L62
	call	ASN1_item_free@PLT
.L44:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	call	*24(%r15)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L48:
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L58
.L77:
	movq	%r14, %rsi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	*%rax
	movq	%rax, %r14
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L81:
	movq	%rbx, %rdi
	leaq	1(%r13), %rsi
	call	NCONF_get_section@PLT
	movq	%rax, %rbx
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L58:
	movq	88(%r15), %rax
	testq	%rax, %rax
	je	.L59
	cmpq	$0, 48(%r14)
	je	.L60
	cmpq	$0, 40(%r14)
	jne	.L77
.L60:
	movl	$110, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$136, %edx
	xorl	%r12d, %r12d
	movl	$151, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L79:
	movl	$78, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$130, %edx
	xorl	%r12d, %r12d
	movl	$151, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	jmp	.L44
.L59:
	movl	$103, %edx
	movl	$151, %esi
	movl	$116, %r8d
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	%r12d, %edi
	call	OBJ_nid2sn@PLT
	leaq	.LC1(%rip), %rsi
	movl	$2, %edi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	ERR_add_error_data@PLT
	.p2align 4,,10
	.p2align 3
.L78:
	xorl	%r12d, %r12d
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L54:
	movl	$92, %r8d
	movl	$105, %edx
	movl	$151, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	%r12d, %edi
	call	OBJ_nid2sn@PLT
	movq	%r13, %r8
	movl	$4, %edi
	leaq	.LC3(%rip), %rcx
	movq	%rax, %rdx
	leaq	.LC1(%rip), %rsi
	xorl	%eax, %eax
	call	ERR_add_error_data@PLT
	cmpb	$64, 0(%r13)
	je	.L78
	movq	X509V3_conf_free@GOTPCREL(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%r12d, %r12d
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L80:
	movl	$82, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$129, %edx
	xorl	%r12d, %r12d
	movl	$151, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	jmp	.L44
	.cfi_endproc
.LFE1300:
	.size	do_ext_nconf, .-do_ext_nconf
	.section	.rodata.str1.1
.LC4:
	.string	"critical,"
.LC5:
	.string	"DER:"
.LC6:
	.string	"ASN1:"
.LC7:
	.string	", value="
	.text
	.p2align 4
	.globl	X509V3_EXT_nconf
	.type	X509V3_EXT_nconf, @function
X509V3_EXT_nconf:
.LFB1298:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%rcx, %rdi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	strlen@PLT
	cmpq	$8, %rax
	jbe	.L95
	movl	$9, %ecx
	leaq	.LC4(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L84
	addq	$9, %r12
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L86:
	addq	$1, %r12
.L85:
	movsbl	(%r12), %edi
	movl	$8, %esi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	jne	.L86
	movq	%r12, %rdi
	movl	$1, %ebx
	call	strlen@PLT
	cmpq	$3, %rax
	ja	.L103
.L87:
	movq	%r13, %rdi
	call	OBJ_sn2nid@PLT
	movq	%r14, %rsi
	movq	%r12, %r8
	movl	%ebx, %ecx
	movl	%eax, %edx
	movq	%r15, %rdi
	call	do_ext_nconf
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L104
.L82:
	addq	$8, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	movl	$4, %ecx
	leaq	.LC5(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%bl
	sbbb	$0, %bl
	movsbl	%bl, %ebx
	testl	%ebx, %ebx
	jne	.L105
.L93:
	addq	$4, %r12
	movl	$1, %r15d
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L91:
	addq	$1, %r12
.L90:
	movsbl	(%r12), %edi
	movl	$8, %esi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	jne	.L91
	addq	$8, %rsp
	movq	%r14, %r8
	movl	%r15d, %ecx
	movl	%ebx, %edx
	movq	%r12, %rsi
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	v3_generic_extension
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	xorl	%ebx, %ebx
	cmpq	$3, %rax
	jbe	.L87
.L103:
	movl	$4, %ecx
	leaq	.LC5(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L93
	cmpq	$4, %rax
	je	.L87
.L94:
	movl	$5, %ecx
	leaq	.LC6(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L87
	addq	$5, %r12
	movl	$2, %r15d
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L105:
	xorl	%ebx, %ebx
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L104:
	movl	$47, %r8d
	movl	$128, %edx
	movl	$152, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %r8
	movq	%r13, %rdx
	movl	$4, %edi
	leaq	.LC7(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	xorl	%eax, %eax
	call	ERR_add_error_data@PLT
	jmp	.L82
	.cfi_endproc
.LFE1298:
	.size	X509V3_EXT_nconf, .-X509V3_EXT_nconf
	.p2align 4
	.globl	X509V3_EXT_nconf_nid
	.type	X509V3_EXT_nconf_nid, @function
X509V3_EXT_nconf_nid:
.LFB1299:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%rcx, %rdi
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	strlen@PLT
	cmpq	$8, %rax
	jbe	.L118
	movl	$9, %ecx
	leaq	.LC4(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L108
	addq	$9, %r12
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L110:
	addq	$1, %r12
.L109:
	movsbl	(%r12), %edi
	movl	$8, %esi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	jne	.L110
	movq	%r12, %rdi
	movl	$1, %ebx
	call	strlen@PLT
	cmpq	$3, %rax
	ja	.L126
.L111:
	addq	$8, %rsp
	movq	%r12, %r8
	movl	%ebx, %ecx
	movl	%r14d, %edx
	popq	%rbx
	movq	%r13, %rsi
	popq	%r12
	movq	%r15, %rdi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	do_ext_nconf
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	movl	$4, %ecx
	leaq	.LC5(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%bl
	sbbb	$0, %bl
	movsbl	%bl, %ebx
	testl	%ebx, %ebx
	jne	.L127
.L116:
	addq	$4, %r12
	movl	$1, %r15d
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L115:
	addq	$1, %r12
.L114:
	movsbl	(%r12), %edi
	movl	$8, %esi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	jne	.L115
	movl	%r14d, %edi
	call	OBJ_nid2sn@PLT
	addq	$8, %rsp
	movq	%r13, %r8
	movl	%r15d, %ecx
	movl	%ebx, %edx
	movq	%r12, %rsi
	popq	%rbx
	movq	%rax, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	v3_generic_extension
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	xorl	%ebx, %ebx
	cmpq	$3, %rax
	jbe	.L111
.L126:
	movl	$4, %ecx
	leaq	.LC5(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L116
	cmpq	$4, %rax
	je	.L111
.L117:
	movl	$5, %ecx
	leaq	.LC6(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L111
	addq	$5, %r12
	movl	$2, %r15d
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L127:
	xorl	%ebx, %ebx
	jmp	.L117
	.cfi_endproc
.LFE1299:
	.size	X509V3_EXT_nconf_nid, .-X509V3_EXT_nconf_nid
	.p2align 4
	.globl	X509V3_EXT_i2d
	.type	X509V3_EXT_i2d, @function
X509V3_EXT_i2d:
.LFB1302:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movl	%edi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	X509V3_EXT_get_nid@PLT
	testq	%rax, %rax
	je	.L132
	movq	8(%rax), %rdi
	addq	$8, %rsp
	movq	%rbx, %r8
	movl	%r13d, %ecx
	popq	%rbx
	movl	%r12d, %edx
	leaq	40(%rax), %rsi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	do_ext_i2d.isra.0
	.p2align 4,,10
	.p2align 3
.L132:
	.cfi_restore_state
	movl	$182, %r8d
	movl	$129, %edx
	movl	$136, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1302:
	.size	X509V3_EXT_i2d, .-X509V3_EXT_i2d
	.p2align 4
	.globl	X509V3_EXT_add_nconf_sk
	.type	X509V3_EXT_add_nconf_sk, @function
X509V3_EXT_add_nconf_sk:
.LFB1308:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movq	%rdx, %rsi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$56, %rsp
	movq	%rdi, -88(%rbp)
	movq	%rcx, -72(%rbp)
	call	NCONF_get_section@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L135
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L138:
	cmpq	$0, -72(%rbp)
	je	.L142
.L143:
	movq	-72(%rbp), %rdi
	movl	$-1, %edx
	movq	%r12, %rsi
	call	X509v3_add_ext@PLT
	testq	%rax, %rax
	je	.L153
.L142:
	movq	%r12, %rdi
	addl	$1, %ebx
	call	X509_EXTENSION_free@PLT
.L135:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L154
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movq	-88(%rbp), %rdi
	movq	%r15, %rsi
	movq	16(%rax), %rcx
	movq	8(%rax), %rdx
	call	X509V3_EXT_nconf
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L152
	cmpl	$2, (%r15)
	jne	.L138
	movq	-72(%rbp), %rax
	movq	%r12, %rdi
	movq	(%rax), %r13
	call	X509_EXTENSION_get_object@PLT
	movq	%rax, -80(%rbp)
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L140:
	movq	%r13, %rdi
	movl	%esi, -60(%rbp)
	call	X509v3_get_ext@PLT
	movl	-60(%rbp), %esi
	movq	%r13, %rdi
	movq	%rax, -56(%rbp)
	call	X509v3_delete_ext@PLT
	movq	-56(%rbp), %r8
	movq	%r8, %rdi
	call	X509_EXTENSION_free@PLT
.L139:
	movq	-80(%rbp), %rsi
	movl	$-1, %edx
	movq	%r13, %rdi
	call	X509v3_get_ext_by_OBJ@PLT
	movl	%eax, %esi
	testl	%eax, %eax
	jns	.L140
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L153:
	movq	%r12, %rdi
	call	X509_EXTENSION_free@PLT
.L152:
	addq	$56, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L154:
	.cfi_restore_state
	addq	$56, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1308:
	.size	X509V3_EXT_add_nconf_sk, .-X509V3_EXT_add_nconf_sk
	.p2align 4
	.globl	X509V3_EXT_add_nconf
	.type	X509V3_EXT_add_nconf, @function
X509V3_EXT_add_nconf:
.LFB1309:
	.cfi_startproc
	endbr64
	leaq	104(%rcx), %rax
	testq	%rcx, %rcx
	cmovne	%rax, %rcx
	jmp	X509V3_EXT_add_nconf_sk
	.cfi_endproc
.LFE1309:
	.size	X509V3_EXT_add_nconf, .-X509V3_EXT_add_nconf
	.p2align 4
	.globl	X509V3_EXT_CRL_add_nconf
	.type	X509V3_EXT_CRL_add_nconf, @function
X509V3_EXT_CRL_add_nconf:
.LFB1310:
	.cfi_startproc
	endbr64
	leaq	56(%rcx), %rax
	testq	%rcx, %rcx
	cmovne	%rax, %rcx
	jmp	X509V3_EXT_add_nconf_sk
	.cfi_endproc
.LFE1310:
	.size	X509V3_EXT_CRL_add_nconf, .-X509V3_EXT_CRL_add_nconf
	.p2align 4
	.globl	X509V3_EXT_REQ_add_nconf
	.type	X509V3_EXT_REQ_add_nconf, @function
X509V3_EXT_REQ_add_nconf:
.LFB1311:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	subq	$40, %rsp
	.cfi_offset 13, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$0, -32(%rbp)
	testq	%rcx, %rcx
	je	.L164
	movq	%rcx, %r13
	leaq	-32(%rbp), %rcx
	call	X509V3_EXT_add_nconf_sk
	testl	%eax, %eax
	je	.L163
	movq	-32(%rbp), %rsi
	movq	%r13, %rdi
	call	X509_REQ_add_extensions@PLT
	movq	X509_EXTENSION_free@GOTPCREL(%rip), %rsi
	movq	-32(%rbp), %rdi
	movl	%eax, -36(%rbp)
	call	OPENSSL_sk_pop_free@PLT
	movl	-36(%rbp), %eax
.L163:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L171
	addq	$40, %rsp
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	xorl	%ecx, %ecx
	call	X509V3_EXT_add_nconf_sk
	jmp	.L163
.L171:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1311:
	.size	X509V3_EXT_REQ_add_nconf, .-X509V3_EXT_REQ_add_nconf
	.p2align 4
	.globl	X509V3_get_string
	.type	X509V3_get_string, @function
X509V3_get_string:
.LFB1312:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %r8
	testq	%r8, %r8
	je	.L173
	movq	40(%rdi), %rax
	testq	%rax, %rax
	je	.L173
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L173
	movq	%r8, %rdi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L173:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$148, %edx
	movl	$143, %esi
	movl	$34, %edi
	movl	$373, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1312:
	.size	X509V3_get_string, .-X509V3_get_string
	.p2align 4
	.globl	X509V3_get_section
	.type	X509V3_get_section, @function
X509V3_get_section:
.LFB1313:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %r8
	testq	%r8, %r8
	je	.L186
	movq	40(%rdi), %rax
	testq	%rax, %rax
	je	.L186
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L186
	movq	%r8, %rdi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L186:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$148, %edx
	movl	$142, %esi
	movl	$34, %edi
	movl	$384, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1313:
	.size	X509V3_get_section, .-X509V3_get_section
	.p2align 4
	.globl	X509V3_string_free
	.type	X509V3_string_free, @function
X509V3_string_free:
.LFB1314:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L198
	movq	40(%rdi), %rax
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L198
	movq	48(%rdi), %rdi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L198:
	ret
	.cfi_endproc
.LFE1314:
	.size	X509V3_string_free, .-X509V3_string_free
	.p2align 4
	.globl	X509V3_section_free
	.type	X509V3_section_free, @function
X509V3_section_free:
.LFB1315:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L206
	movq	40(%rdi), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L206
	movq	48(%rdi), %rdi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L206:
	ret
	.cfi_endproc
.LFE1315:
	.size	X509V3_section_free, .-X509V3_section_free
	.p2align 4
	.globl	X509V3_set_nconf
	.type	X509V3_set_nconf, @function
X509V3_set_nconf:
.LFB1318:
	.cfi_startproc
	endbr64
	leaq	nconf_method(%rip), %rax
	movq	%rsi, 48(%rdi)
	movq	%rax, 40(%rdi)
	ret
	.cfi_endproc
.LFE1318:
	.size	X509V3_set_nconf, .-X509V3_set_nconf
	.p2align 4
	.globl	X509V3_set_ctx
	.type	X509V3_set_ctx, @function
X509V3_set_ctx:
.LFB1319:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movups	%xmm0, 8(%rdi)
	movq	%rcx, %xmm0
	movq	%r8, -8(%rbp)
	movl	%r9d, (%rdi)
	movhps	-8(%rbp), %xmm0
	movups	%xmm0, 24(%rdi)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1319:
	.size	X509V3_set_ctx, .-X509V3_set_ctx
	.p2align 4
	.globl	X509V3_EXT_conf
	.type	X509V3_EXT_conf, @function
X509V3_EXT_conf:
.LFB1320:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r15
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%rdi, %rsi
	movq	%r15, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	CONF_set_nconf@PLT
	movq	%r13, %rdx
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	X509V3_EXT_nconf
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L220
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L220:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1320:
	.size	X509V3_EXT_conf, .-X509V3_EXT_conf
	.p2align 4
	.globl	X509V3_EXT_conf_nid
	.type	X509V3_EXT_conf_nid, @function
X509V3_EXT_conf_nid:
.LFB1321:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r15
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%rdi, %rsi
	movq	%r15, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	CONF_set_nconf@PLT
	movl	%r13d, %edx
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	X509V3_EXT_nconf_nid
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L224
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L224:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1321:
	.size	X509V3_EXT_conf_nid, .-X509V3_EXT_conf_nid
	.p2align 4
	.globl	X509V3_set_conf_lhash
	.type	X509V3_set_conf_lhash, @function
X509V3_set_conf_lhash:
.LFB1324:
	.cfi_startproc
	endbr64
	leaq	conf_lhash_method(%rip), %rax
	movq	%rsi, 48(%rdi)
	movq	%rax, 40(%rdi)
	ret
	.cfi_endproc
.LFE1324:
	.size	X509V3_set_conf_lhash, .-X509V3_set_conf_lhash
	.p2align 4
	.globl	X509V3_EXT_add_conf
	.type	X509V3_EXT_add_conf, @function
X509V3_EXT_add_conf:
.LFB1325:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r15
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rdi, %rsi
	movq	%r15, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	CONF_set_nconf@PLT
	testq	%r12, %r12
	leaq	104(%r12), %rax
	movq	%r14, %rdx
	cmovne	%rax, %r12
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%r12, %rcx
	call	X509V3_EXT_add_nconf_sk
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L232
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L232:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1325:
	.size	X509V3_EXT_add_conf, .-X509V3_EXT_add_conf
	.p2align 4
	.globl	X509V3_EXT_CRL_add_conf
	.type	X509V3_EXT_CRL_add_conf, @function
X509V3_EXT_CRL_add_conf:
.LFB1326:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r15
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rdi, %rsi
	movq	%r15, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	CONF_set_nconf@PLT
	testq	%r12, %r12
	leaq	56(%r12), %rax
	movq	%r14, %rdx
	cmovne	%rax, %r12
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%r12, %rcx
	call	X509V3_EXT_add_nconf_sk
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L239
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L239:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1326:
	.size	X509V3_EXT_CRL_add_conf, .-X509V3_EXT_CRL_add_conf
	.p2align 4
	.globl	X509V3_EXT_REQ_add_conf
	.type	X509V3_EXT_REQ_add_conf, @function
X509V3_EXT_REQ_add_conf:
.LFB1327:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r15
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%rdi, %rsi
	movq	%r15, %rdi
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	CONF_set_nconf@PLT
	movq	$0, -72(%rbp)
	testq	%r13, %r13
	je	.L241
	leaq	-72(%rbp), %rcx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	X509V3_EXT_add_nconf_sk
	testl	%eax, %eax
	je	.L240
	movq	-72(%rbp), %rsi
	movq	%r13, %rdi
	call	X509_REQ_add_extensions@PLT
	movq	X509_EXTENSION_free@GOTPCREL(%rip), %rsi
	movq	-72(%rbp), %rdi
	movl	%eax, -84(%rbp)
	call	OPENSSL_sk_pop_free@PLT
	movl	-84(%rbp), %eax
.L240:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L248
	addq	$64, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L241:
	.cfi_restore_state
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	X509V3_EXT_add_nconf_sk
	jmp	.L240
.L248:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1327:
	.size	X509V3_EXT_REQ_add_conf, .-X509V3_EXT_REQ_add_conf
	.section	.data.rel.local,"aw"
	.align 32
	.type	conf_lhash_method, @object
	.size	conf_lhash_method, 32
conf_lhash_method:
	.quad	conf_lhash_get_string
	.quad	conf_lhash_get_section
	.quad	0
	.quad	0
	.align 32
	.type	nconf_method, @object
	.size	nconf_method, 32
nconf_method:
	.quad	nconf_get_string
	.quad	nconf_get_section
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
