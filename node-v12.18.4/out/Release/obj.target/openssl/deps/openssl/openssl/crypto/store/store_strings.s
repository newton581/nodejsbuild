	.file	"store_strings.c"
	.text
	.p2align 4
	.globl	OSSL_STORE_INFO_type_string
	.type	OSSL_STORE_INFO_type_string, @function
OSSL_STORE_INFO_type_string:
.LFB718:
	.cfi_startproc
	endbr64
	subl	$1, %edi
	cmpl	$4, %edi
	ja	.L3
	movslq	%edi, %rdi
	leaq	type_strings(%rip), %rax
	movq	(%rax,%rdi,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE718:
	.size	OSSL_STORE_INFO_type_string, .-OSSL_STORE_INFO_type_string
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Name"
.LC1:
	.string	"Parameters"
.LC2:
	.string	"Pkey"
.LC3:
	.string	"Certificate"
.LC4:
	.string	"CRL"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	type_strings, @object
	.size	type_strings, 40
type_strings:
	.quad	.LC0
	.quad	.LC1
	.quad	.LC2
	.quad	.LC3
	.quad	.LC4
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
