	.file	"sm2_crypt.c"
	.text
	.p2align 4
	.type	ec_field_size, @function
ec_field_size:
.LFB410:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	call	BN_new@PLT
	movq	%rax, %r12
	call	BN_new@PLT
	movq	%rax, %r13
	call	BN_new@PLT
	testq	%r12, %r12
	sete	%dl
	testq	%r13, %r13
	movq	%rax, %r14
	sete	%al
	orb	%al, %dl
	jne	.L5
	testq	%r14, %r14
	je	.L5
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	EC_GROUP_get_curve@PLT
	testl	%eax, %eax
	je	.L5
	movq	%r12, %rdi
	call	BN_num_bits@PLT
	leal	14(%rax), %r15d
	addl	$7, %eax
	cmovns	%eax, %r15d
	sarl	$3, %r15d
	movslq	%r15d, %r15
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L5:
	xorl	%r15d, %r15d
.L4:
	movq	%r12, %rdi
	call	BN_free@PLT
	movq	%r13, %rdi
	call	BN_free@PLT
	movq	%r14, %rdi
	call	BN_free@PLT
	popq	%r12
	movq	%r15, %rax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE410:
	.size	ec_field_size, .-ec_field_size
	.p2align 4
	.globl	d2i_SM2_Ciphertext
	.type	d2i_SM2_Ciphertext, @function
d2i_SM2_Ciphertext:
.LFB406:
	.cfi_startproc
	endbr64
	leaq	SM2_Ciphertext_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE406:
	.size	d2i_SM2_Ciphertext, .-d2i_SM2_Ciphertext
	.p2align 4
	.globl	i2d_SM2_Ciphertext
	.type	i2d_SM2_Ciphertext, @function
i2d_SM2_Ciphertext:
.LFB407:
	.cfi_startproc
	endbr64
	leaq	SM2_Ciphertext_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE407:
	.size	i2d_SM2_Ciphertext, .-i2d_SM2_Ciphertext
	.p2align 4
	.globl	SM2_Ciphertext_new
	.type	SM2_Ciphertext_new, @function
SM2_Ciphertext_new:
.LFB408:
	.cfi_startproc
	endbr64
	leaq	SM2_Ciphertext_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE408:
	.size	SM2_Ciphertext_new, .-SM2_Ciphertext_new
	.p2align 4
	.globl	SM2_Ciphertext_free
	.type	SM2_Ciphertext_free, @function
SM2_Ciphertext_free:
.LFB409:
	.cfi_startproc
	endbr64
	leaq	SM2_Ciphertext_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE409:
	.size	SM2_Ciphertext_free, .-SM2_Ciphertext_free
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/sm2/sm2_crypt.c"
	.text
	.p2align 4
	.globl	sm2_plaintext_size
	.type	sm2_plaintext_size, @function
sm2_plaintext_size:
.LFB411:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	call	EC_KEY_get0_group@PLT
	movq	%rax, %rdi
	call	ec_field_size
	movq	%r14, %rdi
	movq	%rax, %r13
	call	EVP_MD_size@PLT
	testl	%eax, %eax
	js	.L21
	testq	%r13, %r13
	je	.L22
	cltq
	leaq	10(%rax,%r13,2), %rax
	cmpq	%rbx, %rax
	jnb	.L23
	subq	%rax, %rbx
	movl	$1, %eax
	movq	%rbx, (%r12)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	movl	$76, %r8d
	movl	$105, %edx
	movl	$104, %esi
	movl	$53, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	movl	$72, %r8d
	movl	$102, %edx
	movl	$104, %esi
	movl	$53, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	movl	$82, %r8d
	movl	$104, %edx
	movl	$104, %esi
	movl	$53, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE411:
	.size	sm2_plaintext_size, .-sm2_plaintext_size
	.p2align 4
	.globl	sm2_ciphertext_size
	.type	sm2_ciphertext_size, @function
sm2_ciphertext_size:
.LFB412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	call	EC_KEY_get0_group@PLT
	movq	%rax, %rdi
	call	ec_field_size
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	EVP_MD_size@PLT
	testq	%rbx, %rbx
	je	.L26
	movl	%eax, %r14d
	testl	%eax, %eax
	js	.L26
	leal	1(%rbx), %esi
	movl	$2, %edx
	xorl	%edi, %edi
	call	ASN1_object_size@PLT
	movl	%r14d, %esi
	movl	$4, %edx
	xorl	%edi, %edi
	movl	%eax, %ebx
	call	ASN1_object_size@PLT
	movl	%r13d, %esi
	movl	$4, %edx
	xorl	%edi, %edi
	leal	(%rax,%rbx,2), %ebx
	call	ASN1_object_size@PLT
	movl	$16, %edx
	movl	$1, %edi
	leal	(%rbx,%rax), %esi
	call	ASN1_object_size@PLT
	popq	%rbx
	cltq
	movq	%rax, (%r12)
	popq	%r12
	movl	$1, %eax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE412:
	.size	sm2_ciphertext_size, .-sm2_ciphertext_size
	.p2align 4
	.globl	sm2_encrypt
	.type	sm2_encrypt, @function
sm2_encrypt:
.LFB413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$200, %rsp
	movq	%rsi, -176(%rbp)
	movq	%rdx, -168(%rbp)
	movq	%rcx, -160(%rbp)
	movq	%r8, -104(%rbp)
	movq	%r9, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_MD_CTX_new@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	EC_KEY_get0_group@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	EC_GROUP_get0_order@PLT
	movq	%r14, %rdi
	movq	%rax, %r15
	call	EC_KEY_get0_public_key@PLT
	movq	%rbx, %rdi
	movq	%rax, -152(%rbp)
	call	EVP_MD_size@PLT
	pxor	%xmm0, %xmm0
	movl	%eax, -136(%rbp)
	movaps	%xmm0, -80(%rbp)
	testq	%r13, %r13
	je	.L60
	testl	%eax, %eax
	jle	.L60
	movq	%r12, %rdi
	call	ec_field_size
	movq	%rax, -184(%rbp)
	testq	%rax, %rax
	je	.L101
	movq	%r12, %rdi
	call	EC_POINT_new@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	EC_POINT_new@PLT
	movq	%rax, -112(%rbp)
	call	BN_CTX_new@PLT
	testq	%rbx, %rbx
	sete	%dl
	cmpq	$0, -112(%rbp)
	movq	%rax, %r14
	sete	%al
	orb	%al, %dl
	jne	.L61
	testq	%r14, %r14
	je	.L61
	movq	%r14, %rdi
	call	BN_CTX_start@PLT
	movq	%r14, %rdi
	call	BN_CTX_get@PLT
	movq	%r14, %rdi
	movq	%rax, -192(%rbp)
	call	BN_CTX_get@PLT
	movq	%r14, %rdi
	movq	%rax, -208(%rbp)
	call	BN_CTX_get@PLT
	movq	%r14, %rdi
	movq	%rax, -224(%rbp)
	call	BN_CTX_get@PLT
	movq	%r14, %rdi
	movq	%rax, -216(%rbp)
	call	BN_CTX_get@PLT
	testq	%rax, %rax
	je	.L102
	movq	%rax, -200(%rbp)
	movq	-184(%rbp), %rax
	movl	$171, %edx
	leaq	.LC0(%rip), %rsi
	addq	%rax, %rax
	movq	%rax, %rdi
	movq	%rax, -232(%rbp)
	call	CRYPTO_zalloc@PLT
	movslq	-136(%rbp), %rdi
	movl	$172, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rax, -120(%rbp)
	call	CRYPTO_zalloc@PLT
	cmpq	$0, -120(%rbp)
	movq	%rax, -128(%rbp)
	je	.L62
	testq	%rax, %rax
	movq	-200(%rbp), %r10
	je	.L62
	movq	-144(%rbp), %rax
	movq	-104(%rbp), %rdi
	xorl	%esi, %esi
	movq	%r10, -200(%rbp)
	movq	(%rax), %rdx
	call	memset@PLT
	movq	-192(%rbp), %rdi
	movq	%r15, %rsi
	call	BN_priv_rand_range@PLT
	movq	-200(%rbp), %r10
	testl	%eax, %eax
	movl	%eax, %r15d
	je	.L103
	movq	-192(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %r9
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%r10, -200(%rbp)
	call	EC_POINT_mul@PLT
	movq	-200(%rbp), %r10
	testl	%eax, %eax
	je	.L40
	movq	-216(%rbp), %rcx
	movq	%r14, %r8
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-208(%rbp), %rdx
	movq	%r10, -200(%rbp)
	call	EC_POINT_get_affine_coordinates@PLT
	testl	%eax, %eax
	jne	.L104
.L40:
	movl	$190, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$16, %edx
.L98:
	movl	$103, %esi
	movl	$53, %edi
	xorl	%r15d, %r15d
	call	ERR_put_error@PLT
	xorl	%r10d, %r10d
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L101:
	movl	$147, %r8d
.L97:
	leaq	.LC0(%rip), %rcx
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	movl	$68, %edx
	movl	$103, %esi
	movl	$53, %edi
	call	ERR_put_error@PLT
	movq	$0, -128(%rbp)
	xorl	%r10d, %r10d
	movq	$0, -120(%rbp)
	movq	$0, -112(%rbp)
.L31:
	movq	-72(%rbp), %rdi
	movq	%r10, -136(%rbp)
	call	ASN1_OCTET_STRING_free@PLT
	movq	-80(%rbp), %rdi
	call	ASN1_OCTET_STRING_free@PLT
	movq	-136(%rbp), %r10
	movl	$253, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r10, %rdi
	call	CRYPTO_free@PLT
	movq	-120(%rbp), %rdi
	movl	$254, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-128(%rbp), %rdi
	movl	$255, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r13, %rdi
	call	EVP_MD_CTX_free@PLT
	movq	%r14, %rdi
	call	BN_CTX_free@PLT
	movq	%rbx, %rdi
	call	EC_POINT_free@PLT
	movq	-112(%rbp), %rdi
	call	EC_POINT_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L105
	leaq	-40(%rbp), %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	movl	$141, %r8d
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L61:
	movl	$155, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r15d, %r15d
	movl	$103, %esi
	movl	$53, %edi
	call	ERR_put_error@PLT
	movq	$0, -128(%rbp)
	xorl	%r10d, %r10d
	movq	$0, -120(%rbp)
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L62:
	movl	$175, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L102:
	movl	$167, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$3, %edx
	xorl	%r15d, %r15d
	movl	$103, %esi
	movl	$53, %edi
	movq	%rax, -136(%rbp)
	call	ERR_put_error@PLT
	movq	$0, -128(%rbp)
	movq	-136(%rbp), %r10
	movq	$0, -120(%rbp)
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L104:
	movq	-112(%rbp), %r15
	movq	-192(%rbp), %r8
	xorl	%edx, %edx
	movq	%r14, %r9
	movq	-152(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	EC_POINT_mul@PLT
	testl	%eax, %eax
	je	.L40
	movq	-200(%rbp), %r10
	movq	%r14, %r8
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	-224(%rbp), %rdx
	movq	%r10, %rcx
	movq	%r10, -152(%rbp)
	call	EC_POINT_get_affine_coordinates@PLT
	testl	%eax, %eax
	je	.L40
	movl	-184(%rbp), %edx
	movq	-120(%rbp), %rsi
	movq	-224(%rbp), %rdi
	call	BN_bn2binpad@PLT
	movq	-152(%rbp), %r10
	testl	%eax, %eax
	js	.L42
	movq	-184(%rbp), %rax
	movq	-120(%rbp), %r12
	movq	%r10, %rdi
	addq	%rax, %r12
	movl	%eax, %edx
	movq	%r12, %rsi
	call	BN_bn2binpad@PLT
	testl	%eax, %eax
	js	.L42
	movq	-160(%rbp), %rdi
	movl	$200, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L106
	subq	$8, %rsp
	pushq	-176(%rbp)
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	-120(%rbp), %rdx
	movq	%rax, %rdi
	movq	%rax, -152(%rbp)
	movq	-232(%rbp), %rcx
	movq	-160(%rbp), %rsi
	call	ecdh_KDF_X9_63@PLT
	movq	-152(%rbp), %r10
	movl	%eax, %r15d
	popq	%rax
	popq	%rdx
	testl	%r15d, %r15d
	je	.L44
	cmpq	$0, -160(%rbp)
	je	.L46
	movq	-168(%rbp), %rcx
	leaq	15(%r10), %rax
	subq	%rcx, %rax
	cmpq	$30, %rax
	jbe	.L59
	movq	-160(%rbp), %rsi
	leaq	-1(%rsi), %rax
	cmpq	$14, %rax
	jbe	.L59
	andq	$-16, %rsi
	xorl	%eax, %eax
	movq	%rsi, %rdx
.L48:
	movdqu	(%r10,%rax), %xmm0
	movdqu	(%rcx,%rax), %xmm1
	pxor	%xmm1, %xmm0
	movups	%xmm0, (%r10,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L48
	movq	-160(%rbp), %rsi
	movq	%rsi, %rax
	andq	$-16, %rax
	testb	$15, %sil
	je	.L46
	movq	-168(%rbp), %rdi
	movzbl	(%rdi,%rax), %edx
	xorb	%dl, (%r10,%rax)
	leaq	1(%rax), %rdx
	cmpq	%rdx, %rsi
	je	.L46
	movzbl	1(%rdi,%rax), %ecx
	xorb	%cl, (%r10,%rdx)
	leaq	2(%rax), %rdx
	cmpq	%rdx, %rsi
	je	.L46
	movzbl	2(%rdi,%rax), %ecx
	xorb	%cl, (%r10,%rdx)
	leaq	3(%rax), %rdx
	cmpq	%rdx, %rsi
	je	.L46
	movzbl	3(%rdi,%rax), %ecx
	xorb	%cl, (%r10,%rdx)
	leaq	4(%rax), %rdx
	cmpq	%rdx, %rsi
	je	.L46
	movzbl	4(%rdi,%rax), %ecx
	xorb	%cl, (%r10,%rdx)
	leaq	5(%rax), %rdx
	cmpq	%rdx, %rsi
	je	.L46
	movzbl	5(%rdi,%rax), %ecx
	xorb	%cl, (%r10,%rdx)
	leaq	6(%rax), %rdx
	cmpq	%rdx, %rsi
	je	.L46
	movzbl	6(%rdi,%rax), %ecx
	xorb	%cl, (%r10,%rdx)
	leaq	7(%rax), %rdx
	cmpq	%rdx, %rsi
	je	.L46
	movzbl	7(%rdi,%rax), %ecx
	xorb	%cl, (%r10,%rdx)
	leaq	8(%rax), %rdx
	cmpq	%rdx, %rsi
	je	.L46
	movzbl	8(%rdi,%rax), %ecx
	xorb	%cl, (%r10,%rdx)
	leaq	9(%rax), %rdx
	cmpq	%rdx, %rsi
	je	.L46
	movzbl	9(%rdi,%rax), %ecx
	xorb	%cl, (%r10,%rdx)
	leaq	10(%rax), %rdx
	cmpq	%rdx, %rsi
	je	.L46
	movzbl	10(%rdi,%rax), %ecx
	xorb	%cl, (%r10,%rdx)
	leaq	11(%rax), %rdx
	cmpq	%rdx, %rsi
	je	.L46
	movzbl	11(%rdi,%rax), %ecx
	xorb	%cl, (%r10,%rdx)
	leaq	12(%rax), %rdx
	cmpq	%rdx, %rsi
	je	.L46
	movzbl	12(%rdi,%rax), %ecx
	xorb	%cl, (%r10,%rdx)
	leaq	13(%rax), %rdx
	cmpq	%rdx, %rsi
	je	.L46
	movzbl	13(%rdi,%rax), %ecx
	xorb	%cl, (%r10,%rdx)
	leaq	14(%rax), %rdx
	cmpq	%rdx, %rsi
	je	.L46
	movzbl	14(%rdi,%rax), %eax
	xorb	%al, (%r10,%rdx)
.L46:
	movq	-176(%rbp), %rsi
	movq	%r13, %rdi
	movq	%r10, -152(%rbp)
	call	EVP_DigestInit@PLT
	movq	-152(%rbp), %r10
	testl	%eax, %eax
	je	.L52
	movq	-184(%rbp), %r15
	movq	-120(%rbp), %rsi
	movq	%r13, %rdi
	movq	%r10, -152(%rbp)
	movq	%r15, %rdx
	call	EVP_DigestUpdate@PLT
	movq	-152(%rbp), %r10
	testl	%eax, %eax
	je	.L52
	movq	-160(%rbp), %rdx
	movq	-168(%rbp), %rsi
	movq	%r13, %rdi
	call	EVP_DigestUpdate@PLT
	movq	-152(%rbp), %r10
	testl	%eax, %eax
	je	.L52
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	EVP_DigestUpdate@PLT
	movq	-152(%rbp), %r10
	testl	%eax, %eax
	je	.L52
	movq	-128(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	EVP_DigestFinal@PLT
	movq	-152(%rbp), %r10
	testl	%eax, %eax
	je	.L52
	movq	-208(%rbp), %xmm0
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	ASN1_OCTET_STRING_new@PLT
	movq	%rax, -80(%rbp)
	call	ASN1_OCTET_STRING_new@PLT
	movq	-80(%rbp), %rdi
	movq	-152(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, -72(%rbp)
	je	.L63
	testq	%rdi, %rdi
	je	.L63
	movl	-136(%rbp), %edx
	movq	-128(%rbp), %rsi
	movq	%r10, -152(%rbp)
	call	ASN1_OCTET_STRING_set@PLT
	movq	-152(%rbp), %r10
	testl	%eax, %eax
	je	.L56
	movl	-160(%rbp), %edx
	movq	-72(%rbp), %rdi
	movq	%r10, %rsi
	movq	%r10, -136(%rbp)
	call	ASN1_OCTET_STRING_set@PLT
	movq	-136(%rbp), %r10
	testl	%eax, %eax
	je	.L56
	leaq	-104(%rbp), %rsi
	leaq	-96(%rbp), %rdi
	leaq	SM2_Ciphertext_it(%rip), %rdx
	call	ASN1_item_i2d@PLT
	movq	-136(%rbp), %r10
	testl	%eax, %eax
	js	.L107
	movq	-144(%rbp), %rcx
	cltq
	movl	$1, %r15d
	movq	%rax, (%rcx)
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L103:
	movl	$182, %r8d
	movl	$68, %edx
	movl	$103, %esi
	movl	$53, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r10d, %r10d
	jmp	.L31
.L42:
	movl	$196, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$68, %edx
	jmp	.L98
.L52:
	movq	%r10, -136(%rbp)
	movl	$221, %r8d
	movl	$6, %edx
	leaq	.LC0(%rip), %rcx
.L100:
	movl	$103, %esi
	movl	$53, %edi
	xorl	%r15d, %r15d
	call	ERR_put_error@PLT
	movq	-136(%rbp), %r10
	jmp	.L31
.L106:
	movq	%rax, -136(%rbp)
	movl	$202, %r8d
	movl	$65, %edx
	leaq	.LC0(%rip), %rcx
	jmp	.L100
.L44:
	movl	$209, %r8d
	movl	$6, %edx
	movl	$103, %esi
	movl	$53, %edi
	leaq	.LC0(%rip), %rcx
	movq	%r10, -136(%rbp)
	call	ERR_put_error@PLT
	movq	-136(%rbp), %r10
	jmp	.L31
.L105:
	call	__stack_chk_fail@PLT
.L59:
	movq	-168(%rbp), %rcx
	movq	-160(%rbp), %rsi
	xorl	%eax, %eax
.L47:
	movzbl	(%rcx,%rax), %edx
	xorb	%dl, (%r10,%rax)
	addq	$1, %rax
	cmpq	%rax, %rsi
	jne	.L47
	jmp	.L46
.L63:
	movq	%r10, -136(%rbp)
	movl	$231, %r8d
	movl	$65, %edx
	leaq	.LC0(%rip), %rcx
	jmp	.L100
.L107:
	movl	$243, %r8d
.L99:
	leaq	.LC0(%rip), %rcx
	movl	$68, %edx
	jmp	.L100
.L56:
	movq	%r10, -136(%rbp)
	movl	$236, %r8d
	jmp	.L99
	.cfi_endproc
.LFE413:
	.size	sm2_encrypt, .-sm2_encrypt
	.p2align 4
	.globl	sm2_decrypt
	.type	sm2_decrypt, @function
sm2_decrypt:
.LFB414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -104(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%rdx, -56(%rbp)
	movq	%r8, -80(%rbp)
	movq	%r9, -88(%rbp)
	call	EC_KEY_get0_group@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	ec_field_size
	movq	%r15, %rdi
	movq	%rax, %rbx
	movq	%rax, -96(%rbp)
	call	EVP_MD_size@PLT
	testq	%rbx, %rbx
	je	.L132
	movl	%eax, %r14d
	testl	%eax, %eax
	jle	.L132
	movq	-88(%rbp), %rax
	movq	-80(%rbp), %rdi
	movl	$255, %esi
	movq	(%rax), %rdx
	call	memset@PLT
	movq	%r12, %rdx
	leaq	-56(%rbp), %rsi
	xorl	%edi, %edi
	leaq	SM2_Ciphertext_it(%rip), %rcx
	call	ASN1_item_d2i@PLT
	movl	$294, %r8d
	movl	$100, %edx
	leaq	.LC0(%rip), %rcx
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L164
	movq	16(%rax), %rax
	cmpl	%r14d, (%rax)
	je	.L111
	movl	$299, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$104, %edx
.L164:
	movl	$102, %esi
	movl	$53, %edi
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	call	ERR_put_error@PLT
	xorl	%r15d, %r15d
	movq	$0, -72(%rbp)
	xorl	%r10d, %r10d
	movq	$0, -64(%rbp)
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L132:
	movq	$0, -72(%rbp)
	xorl	%ebx, %ebx
	xorl	%r10d, %r10d
	xorl	%r12d, %r12d
	movq	$0, -64(%rbp)
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
.L109:
	movq	-88(%rbp), %rax
	movq	-80(%rbp), %rdi
	xorl	%esi, %esi
	movq	%r10, -96(%rbp)
	xorl	%r13d, %r13d
	movq	(%rax), %rdx
	call	memset@PLT
	movq	-96(%rbp), %r10
.L131:
	movq	%r10, %rdi
	movl	$384, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-64(%rbp), %rdi
	movl	$385, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-72(%rbp), %rdi
	movl	$386, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r14, %rdi
	call	EC_POINT_free@PLT
	movq	%r15, %rdi
	call	BN_CTX_free@PLT
	movq	%r12, %rdi
	leaq	SM2_Ciphertext_it(%rip), %rsi
	call	ASN1_item_free@PLT
	movq	%rbx, %rdi
	call	EVP_MD_CTX_free@PLT
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	movq	24(%r12), %rdx
	movq	8(%rax), %rax
	movq	8(%rdx), %rcx
	movq	%rax, -144(%rbp)
	movl	(%rdx), %eax
	movq	%rcx, -136(%rbp)
	movl	%eax, -116(%rbp)
	call	BN_CTX_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L166
	movq	%rax, %rdi
	call	BN_CTX_start@PLT
	movq	%r15, %rdi
	call	BN_CTX_get@PLT
	movq	%r15, %rdi
	movq	%rax, -152(%rbp)
	call	BN_CTX_get@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L167
	movslq	-116(%rbp), %rax
	movl	$322, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -160(%rbp)
	call	CRYPTO_zalloc@PLT
	movl	$323, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rax, -128(%rbp)
	movq	-96(%rbp), %rax
	addq	%rax, %rax
	movq	%rax, %rdi
	movq	%rax, -168(%rbp)
	call	CRYPTO_zalloc@PLT
	movslq	%r14d, %rcx
	movl	$324, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rcx, %rdi
	movq	%rax, -64(%rbp)
	movq	%rcx, -176(%rbp)
	call	CRYPTO_zalloc@PLT
	movq	-128(%rbp), %r10
	movq	%rax, -72(%rbp)
	movq	%rax, %rsi
	testq	%r10, %r10
	sete	%dl
	cmpq	$0, -64(%rbp)
	sete	%al
	orb	%al, %dl
	jne	.L133
	testq	%rsi, %rsi
	je	.L133
	movq	%r13, %rdi
	movq	%r10, -128(%rbp)
	call	EC_POINT_new@PLT
	movq	-128(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L168
	movq	8(%r12), %rcx
	movq	(%r12), %rdx
	movq	%r15, %r8
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	%r10, -128(%rbp)
	call	EC_POINT_set_affine_coordinates@PLT
	movq	-128(%rbp), %r10
	testl	%eax, %eax
	je	.L118
	movq	-104(%rbp), %rdi
	movq	%r10, -128(%rbp)
	call	EC_KEY_get0_private_key@PLT
	xorl	%edx, %edx
	movq	%r15, %r9
	movq	%r14, %rcx
	movq	%rax, %r8
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	EC_POINT_mul@PLT
	movq	-128(%rbp), %r10
	testl	%eax, %eax
	je	.L118
	movq	-152(%rbp), %rdx
	movq	%r15, %r8
	movq	%rbx, %rcx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%r10, -104(%rbp)
	call	EC_POINT_get_affine_coordinates@PLT
	movq	-104(%rbp), %r10
	testl	%eax, %eax
	je	.L118
	movq	-64(%rbp), %r13
	movl	-96(%rbp), %edx
	movq	-152(%rbp), %rdi
	movq	%r13, %rsi
	call	BN_bn2binpad@PLT
	movq	-104(%rbp), %r10
	testl	%eax, %eax
	js	.L119
	movq	-96(%rbp), %rax
	movq	%r13, -64(%rbp)
	movq	%rbx, %rdi
	addq	%rax, %r13
	movl	%eax, %edx
	movq	%r13, %rsi
	call	BN_bn2binpad@PLT
	movq	-104(%rbp), %r10
	testl	%eax, %eax
	js	.L119
	subq	$8, %rsp
	pushq	-112(%rbp)
	movq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	movq	-168(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r10, %rdi
	movq	-160(%rbp), %rsi
	call	ecdh_KDF_X9_63@PLT
	popq	%rdx
	movq	-104(%rbp), %r10
	testl	%eax, %eax
	popq	%rcx
	je	.L119
	movl	-116(%rbp), %eax
	testl	%eax, %eax
	je	.L121
	movq	-80(%rbp), %rdi
	movq	-136(%rbp), %r11
	movl	-116(%rbp), %ebx
	leaq	15(%rdi), %rax
	movq	%rax, %rdx
	leal	-1(%rbx), %ecx
	subq	%r11, %rdx
	cmpq	$30, %rdx
	seta	%sil
	cmpl	$14, %ecx
	seta	%dl
	testb	%dl, %sil
	je	.L122
	subq	%r10, %rax
	cmpq	$30, %rax
	jbe	.L122
	shrl	$4, %ebx
	xorl	%eax, %eax
	movq	%r11, %rcx
	movq	%rdi, %rsi
	movl	%ebx, %edx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L123:
	movdqu	(%r10,%rax), %xmm0
	movdqu	(%rcx,%rax), %xmm1
	pxor	%xmm1, %xmm0
	movups	%xmm0, (%rsi,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L123
	movl	-116(%rbp), %ebx
	movl	%ebx, %eax
	andl	$-16, %eax
	testb	$15, %bl
	je	.L121
	movq	-136(%rbp), %rsi
	movslq	%eax, %rdx
	movq	-80(%rbp), %rdi
	movzbl	(%rsi,%rdx), %ecx
	xorb	(%r10,%rdx), %cl
	movb	%cl, (%rdi,%rdx)
	leal	1(%rax), %edx
	cmpl	%edx, %ebx
	je	.L121
	movslq	%edx, %rdx
	movzbl	(%r10,%rdx), %ecx
	xorb	(%rsi,%rdx), %cl
	movb	%cl, (%rdi,%rdx)
	leal	2(%rax), %edx
	cmpl	%edx, %ebx
	je	.L121
	movslq	%edx, %rdx
	movzbl	(%r10,%rdx), %ecx
	xorb	(%rsi,%rdx), %cl
	movb	%cl, (%rdi,%rdx)
	leal	3(%rax), %edx
	cmpl	%edx, %ebx
	je	.L121
	movslq	%edx, %rdx
	movzbl	(%rsi,%rdx), %ecx
	xorb	(%r10,%rdx), %cl
	movb	%cl, (%rdi,%rdx)
	leal	4(%rax), %edx
	cmpl	%edx, %ebx
	je	.L121
	movslq	%edx, %rdx
	movzbl	(%r10,%rdx), %ecx
	xorb	(%rsi,%rdx), %cl
	movb	%cl, (%rdi,%rdx)
	leal	5(%rax), %edx
	cmpl	%edx, %ebx
	je	.L121
	movslq	%edx, %rdx
	movzbl	(%r10,%rdx), %ecx
	xorb	(%rsi,%rdx), %cl
	movb	%cl, (%rdi,%rdx)
	leal	6(%rax), %edx
	cmpl	%edx, %ebx
	je	.L121
	movslq	%edx, %rdx
	movzbl	(%r10,%rdx), %ecx
	xorb	(%rsi,%rdx), %cl
	movb	%cl, (%rdi,%rdx)
	leal	7(%rax), %edx
	cmpl	%ebx, %edx
	je	.L121
	movslq	%edx, %rdx
	movzbl	(%rsi,%rdx), %ecx
	xorb	(%r10,%rdx), %cl
	movb	%cl, (%rdi,%rdx)
	leal	8(%rax), %edx
	cmpl	%edx, %ebx
	je	.L121
	movslq	%edx, %rdx
	movzbl	(%rsi,%rdx), %ecx
	xorb	(%r10,%rdx), %cl
	movb	%cl, (%rdi,%rdx)
	leal	9(%rax), %edx
	cmpl	%edx, %ebx
	je	.L121
	movslq	%edx, %rdx
	movzbl	(%rsi,%rdx), %ecx
	xorb	(%r10,%rdx), %cl
	movb	%cl, (%rdi,%rdx)
	leal	10(%rax), %edx
	cmpl	%edx, %ebx
	je	.L121
	movslq	%edx, %rdx
	movzbl	(%rsi,%rdx), %ecx
	xorb	(%r10,%rdx), %cl
	movb	%cl, (%rdi,%rdx)
	leal	11(%rax), %edx
	cmpl	%edx, %ebx
	je	.L121
	movslq	%edx, %rdx
	movzbl	(%rsi,%rdx), %ecx
	xorb	(%r10,%rdx), %cl
	movb	%cl, (%rdi,%rdx)
	leal	12(%rax), %edx
	cmpl	%edx, %ebx
	je	.L121
	movslq	%edx, %rdx
	movzbl	(%rsi,%rdx), %ecx
	xorb	(%r10,%rdx), %cl
	movb	%cl, (%rdi,%rdx)
	leal	13(%rax), %edx
	cmpl	%edx, %ebx
	je	.L121
	movslq	%edx, %rdx
	addl	$14, %eax
	movzbl	(%rsi,%rdx), %ecx
	xorb	(%r10,%rdx), %cl
	movb	%cl, (%rdi,%rdx)
	cmpl	%eax, %ebx
	je	.L121
	cltq
	movzbl	(%r10,%rax), %edx
	xorb	(%rsi,%rax), %dl
	movb	%dl, (%rdi,%rax)
.L121:
	movq	%r10, -104(%rbp)
	call	EVP_MD_CTX_new@PLT
	movq	-104(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L169
	movq	-112(%rbp), %rsi
	movq	%rax, %rdi
	movq	%r10, -104(%rbp)
	call	EVP_DigestInit@PLT
	movq	-104(%rbp), %r10
	testl	%eax, %eax
	je	.L129
	movq	-96(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%r10, -104(%rbp)
	call	EVP_DigestUpdate@PLT
	movq	-104(%rbp), %r10
	testl	%eax, %eax
	je	.L129
	movq	-160(%rbp), %rdx
	movq	-80(%rbp), %rsi
	movq	%rbx, %rdi
	call	EVP_DigestUpdate@PLT
	movq	-104(%rbp), %r10
	testl	%eax, %eax
	je	.L129
	movq	-96(%rbp), %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	EVP_DigestUpdate@PLT
	movq	-104(%rbp), %r10
	testl	%eax, %eax
	je	.L129
	movq	-72(%rbp), %r13
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%r10, -96(%rbp)
	movq	%r13, %rsi
	call	EVP_DigestFinal@PLT
	movq	-96(%rbp), %r10
	testl	%eax, %eax
	je	.L129
	movq	-176(%rbp), %rdx
	movq	-144(%rbp), %rsi
	movq	%r13, %rdi
	call	CRYPTO_memcmp@PLT
	movq	-96(%rbp), %r10
	testl	%eax, %eax
	jne	.L170
	movq	-88(%rbp), %rax
	movq	-160(%rbp), %rcx
	movl	$1, %r13d
	movq	%rcx, (%rax)
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L118:
	movq	%r10, -96(%rbp)
	movl	$342, %r8d
	movl	$16, %edx
	leaq	.LC0(%rip), %rcx
.L165:
	movl	$102, %esi
	movl	$53, %edi
	xorl	%ebx, %ebx
	call	ERR_put_error@PLT
	movq	-96(%rbp), %r10
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L133:
	movl	$327, %r8d
	movl	$65, %edx
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	leaq	.LC0(%rip), %rcx
	movl	$102, %esi
	movl	$53, %edi
	movq	%r10, -96(%rbp)
	call	ERR_put_error@PLT
	movq	-96(%rbp), %r10
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L166:
	movl	$309, %r8d
	movl	$65, %edx
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	leaq	.LC0(%rip), %rcx
	movl	$102, %esi
	movl	$53, %edi
	call	ERR_put_error@PLT
	movq	$0, -72(%rbp)
	xorl	%r10d, %r10d
	movq	$0, -64(%rbp)
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L167:
	movl	$318, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$3, %edx
	xorl	%r14d, %r14d
	movl	$102, %esi
	movl	$53, %edi
	call	ERR_put_error@PLT
	movq	$0, -72(%rbp)
	xorl	%r10d, %r10d
	movq	$0, -64(%rbp)
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L168:
	movq	%r10, -96(%rbp)
	movl	$333, %r8d
	movl	$65, %edx
	leaq	.LC0(%rip), %rcx
	jmp	.L165
.L119:
	movq	%r10, -96(%rbp)
	movl	$350, %r8d
	movl	$68, %edx
	leaq	.LC0(%rip), %rcx
	jmp	.L165
.L129:
	movl	$368, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$6, %edx
	movl	$102, %esi
	movl	$53, %edi
	movq	%r10, -96(%rbp)
	call	ERR_put_error@PLT
	movq	-96(%rbp), %r10
	jmp	.L109
.L169:
	movl	$359, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$102, %esi
	movl	$53, %edi
	movq	%r10, -96(%rbp)
	call	ERR_put_error@PLT
	movq	-96(%rbp), %r10
	jmp	.L109
.L122:
	movq	-136(%rbp), %rsi
	movq	-80(%rbp), %rdi
	xorl	%eax, %eax
.L125:
	movzbl	(%rsi,%rax), %edx
	xorb	(%r10,%rax), %dl
	movb	%dl, (%rdi,%rax)
	movq	%rax, %rdx
	addq	$1, %rax
	cmpq	%rcx, %rdx
	jne	.L125
	jmp	.L121
.L170:
	movl	$373, %r8d
	movl	$102, %edx
	movl	$102, %esi
	movl	$53, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-96(%rbp), %r10
	jmp	.L109
	.cfi_endproc
.LFE414:
	.size	sm2_decrypt, .-sm2_decrypt
	.globl	SM2_Ciphertext_it
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"SM2_Ciphertext"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	SM2_Ciphertext_it, @object
	.size	SM2_Ciphertext_it, 56
SM2_Ciphertext_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	SM2_Ciphertext_seq_tt
	.quad	4
	.quad	0
	.quad	32
	.quad	.LC1
	.section	.rodata.str1.1
.LC2:
	.string	"C1x"
.LC3:
	.string	"C1y"
.LC4:
	.string	"C3"
.LC5:
	.string	"C2"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	SM2_Ciphertext_seq_tt, @object
	.size	SM2_Ciphertext_seq_tt, 160
SM2_Ciphertext_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC2
	.quad	BIGNUM_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC3
	.quad	BIGNUM_it
	.quad	0
	.quad	0
	.quad	16
	.quad	.LC4
	.quad	ASN1_OCTET_STRING_it
	.quad	0
	.quad	0
	.quad	24
	.quad	.LC5
	.quad	ASN1_OCTET_STRING_it
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
