	.file	"o_names.c"
	.text
	.p2align 4
	.type	obj_name_hash, @function
obj_name_hash:
.LFB448:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	name_funcs_stack(%rip), %rdi
	testq	%rdi, %rdi
	je	.L2
	call	OPENSSL_sk_num@PLT
	movl	(%rbx), %esi
	cmpl	%eax, %esi
	jl	.L9
.L2:
	movq	8(%rbx), %rdi
	call	openssl_lh_strcasehash@PLT
	movq	%rax, %rdx
	movslq	(%rbx), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	xorq	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	movq	name_funcs_stack(%rip), %rdi
	call	OPENSSL_sk_value@PLT
	movq	8(%rbx), %rdi
	call	*(%rax)
	movq	%rax, %rdx
	movslq	(%rbx), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	xorq	%rdx, %rax
	ret
	.cfi_endproc
.LFE448:
	.size	obj_name_hash, .-obj_name_hash
	.p2align 4
	.type	o_names_init_ossl_, @function
o_names_init_ossl_:
.LFB443:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	CRYPTO_mem_ctrl@PLT
	leaq	obj_name_cmp(%rip), %rsi
	leaq	obj_name_hash(%rip), %rdi
	call	OPENSSL_LH_new@PLT
	movq	%rax, names_lh(%rip)
	call	CRYPTO_THREAD_lock_new@PLT
	movl	$2, %edi
	movq	%rax, obj_lock(%rip)
	call	CRYPTO_mem_ctrl@PLT
	xorl	%eax, %eax
	cmpq	$0, names_lh(%rip)
	je	.L11
	xorl	%eax, %eax
	cmpq	$0, obj_lock(%rip)
	setne	%al
.L11:
	movl	%eax, o_names_init_ossl_ret_(%rip)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE443:
	.size	o_names_init_ossl_, .-o_names_init_ossl_
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/objects/o_names.c"
	.text
	.p2align 4
	.type	name_funcs_free, @function
name_funcs_free:
.LFB459:
	.cfi_startproc
	endbr64
	movl	$382, %edx
	leaq	.LC0(%rip), %rsi
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE459:
	.size	name_funcs_free, .-name_funcs_free
	.p2align 4
	.type	do_all_sorted_cmp, @function
do_all_sorted_cmp:
.LFB456:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movq	8(%rax), %rsi
	movq	(%rdi), %rax
	movq	8(%rax), %rdi
	jmp	strcmp@PLT
	.cfi_endproc
.LFE456:
	.size	do_all_sorted_cmp, .-do_all_sorted_cmp
	.p2align 4
	.type	do_all_fn, @function
do_all_fn:
.LFB452:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	cmpl	%eax, (%rdi)
	je	.L19
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movq	16(%rsi), %r8
	movq	8(%rsi), %rax
	movq	%r8, %rsi
	jmp	*%rax
	.cfi_endproc
.LFE452:
	.size	do_all_fn, .-do_all_fn
	.p2align 4
	.type	do_all_sorted_fn, @function
do_all_sorted_fn:
.LFB455:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	cmpl	%eax, (%rdi)
	jne	.L20
	movslq	4(%rsi), %rax
	movq	8(%rsi), %rdx
	leal	1(%rax), %ecx
	movl	%ecx, 4(%rsi)
	movq	%rdi, (%rdx,%rax,8)
.L20:
	ret
	.cfi_endproc
.LFE455:
	.size	do_all_sorted_fn, .-do_all_sorted_fn
	.p2align 4
	.type	obj_name_cmp, @function
obj_name_cmp:
.LFB447:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	subl	(%rsi), %eax
	jne	.L22
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	name_funcs_stack(%rip), %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	testq	%rdi, %rdi
	je	.L24
	call	OPENSSL_sk_num@PLT
	movl	(%r12), %esi
	cmpl	%eax, %esi
	jl	.L31
.L24:
	movq	8(%rbx), %rsi
	movq	8(%r12), %rdi
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	strcasecmp@PLT
	.p2align 4,,10
	.p2align 3
.L22:
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	name_funcs_stack(%rip), %rdi
	call	OPENSSL_sk_value@PLT
	movq	8(%rbx), %rsi
	movq	8(%r12), %rdi
	popq	%rbx
	.cfi_restore 3
	movq	8(%rax), %rax
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE447:
	.size	obj_name_cmp, .-obj_name_cmp
	.p2align 4
	.type	names_lh_free_doall, @function
names_lh_free_doall:
.LFB458:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L32
	movl	free_type(%rip), %eax
	movl	(%rdi), %ebx
	testl	%eax, %eax
	js	.L35
	cmpl	%ebx, %eax
	je	.L35
.L32:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L49
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movq	8(%rdi), %r12
	leaq	o_names_init_ossl_(%rip), %rsi
	leaq	init(%rip), %rdi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L32
	movl	o_names_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	je	.L32
	movq	obj_lock(%rip), %rdi
	andb	$127, %bh
	call	CRYPTO_THREAD_write_lock@PLT
	movq	names_lh(%rip), %rdi
	leaq	-48(%rbp), %rsi
	movq	%r12, -40(%rbp)
	movl	%ebx, -48(%rbp)
	call	OPENSSL_LH_delete@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L37
	movq	name_funcs_stack(%rip), %rdi
	testq	%rdi, %rdi
	je	.L39
	call	OPENSSL_sk_num@PLT
	movl	(%r12), %esi
	cmpl	%esi, %eax
	jle	.L39
	movq	name_funcs_stack(%rip), %rdi
	call	OPENSSL_sk_value@PLT
	movq	16(%r12), %rdx
	movq	8(%r12), %rdi
	movl	(%r12), %esi
	call	*16(%rax)
	.p2align 4,,10
	.p2align 3
.L39:
	movl	$287, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
.L37:
	movq	obj_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	jmp	.L32
.L49:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE458:
	.size	names_lh_free_doall, .-names_lh_free_doall
	.p2align 4
	.globl	OBJ_NAME_init
	.type	OBJ_NAME_init, @function
OBJ_NAME_init:
.LFB445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	o_names_init_ossl_(%rip), %rsi
	leaq	init(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	CRYPTO_THREAD_run_once@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	cmovne	o_names_init_ossl_ret_(%rip), %eax
	ret
	.cfi_endproc
.LFE445:
	.size	OBJ_NAME_init, .-OBJ_NAME_init
	.p2align 4
	.globl	OBJ_NAME_new_index
	.type	OBJ_NAME_new_index, @function
OBJ_NAME_new_index:
.LFB446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -56(%rbp)
	leaq	init(%rip), %rdi
	movq	%rsi, -64(%rbp)
	leaq	o_names_init_ossl_(%rip), %rsi
	movq	%rdx, -72(%rbp)
	call	CRYPTO_THREAD_run_once@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L55
	movl	o_names_init_ossl_ret_(%rip), %r13d
	testl	%r13d, %r13d
	jne	.L82
.L55:
	addq	$40, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	movq	obj_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	name_funcs_stack(%rip), %rdi
	testq	%rdi, %rdi
	je	.L83
.L57:
	movl	names_type_num(%rip), %r13d
	leaq	.LC0(%rip), %r15
	leal	1(%r13), %eax
	movl	%eax, names_type_num(%rip)
	call	OPENSSL_sk_num@PLT
	cmpl	names_type_num(%rip), %eax
	movl	%eax, %ebx
	jl	.L59
	.p2align 4,,10
	.p2align 3
.L64:
	movq	name_funcs_stack(%rip), %rdi
	movl	%r13d, %esi
	call	OPENSSL_sk_value@PLT
	cmpq	$0, -56(%rbp)
	je	.L61
	movq	-56(%rbp), %rcx
	movq	%rcx, (%rax)
.L61:
	movq	-64(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L65
	movq	%rcx, 8(%rax)
.L65:
	movq	-72(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L58
	movq	%rcx, 16(%rax)
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L62:
	movq	openssl_lh_strcasehash@GOTPCREL(%rip), %xmm0
	movl	$3, %edi
	movhps	strcasecmp@GOTPCREL(%rip), %xmm0
	movups	%xmm0, (%r12)
	call	CRYPTO_mem_ctrl@PLT
	movq	name_funcs_stack(%rip), %rdi
	movq	%r12, %rsi
	call	OPENSSL_sk_push@PLT
	movl	$2, %edi
	movl	%eax, %r14d
	call	CRYPTO_mem_ctrl@PLT
	testl	%r14d, %r14d
	je	.L84
	addl	$1, %ebx
	cmpl	%ebx, names_type_num(%rip)
	jle	.L64
.L59:
	movl	$3, %edi
	call	CRYPTO_mem_ctrl@PLT
	movl	$24, %edi
	movl	$106, %edx
	movq	%r15, %rsi
	call	CRYPTO_zalloc@PLT
	movl	$2, %edi
	movq	%rax, %r12
	call	CRYPTO_mem_ctrl@PLT
	testq	%r12, %r12
	jne	.L62
	movl	$109, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$106, %esi
	movl	$8, %edi
	call	ERR_put_error@PLT
.L58:
	movq	obj_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L83:
	movl	$3, %edi
	call	CRYPTO_mem_ctrl@PLT
	call	OPENSSL_sk_new_null@PLT
	movl	$2, %edi
	movq	%rax, name_funcs_stack(%rip)
	call	CRYPTO_mem_ctrl@PLT
	movq	name_funcs_stack(%rip), %rdi
	testq	%rdi, %rdi
	jne	.L57
	xorl	%r13d, %r13d
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L84:
	movl	$121, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$106, %esi
	movl	$8, %edi
	call	ERR_put_error@PLT
	movl	$122, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	jmp	.L58
	.cfi_endproc
.LFE446:
	.size	OBJ_NAME_new_index, .-OBJ_NAME_new_index
	.p2align 4
	.globl	OBJ_NAME_get
	.type	OBJ_NAME_get, @function
OBJ_NAME_get:
.LFB449:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L88
	movq	%rdi, %rbx
	movl	%esi, %r12d
	leaq	init(%rip), %rdi
	leaq	o_names_init_ossl_(%rip), %rsi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L88
	movl	o_names_init_ossl_ret_(%rip), %edx
	testl	%edx, %edx
	je	.L88
	movq	obj_lock(%rip), %rdi
	movl	%r12d, %r13d
	leaq	-64(%rbp), %r14
	andl	$32768, %r13d
	call	CRYPTO_THREAD_read_lock@PLT
	movl	%r12d, %esi
	movq	%rbx, -56(%rbp)
	movl	$11, %ebx
	andl	$-32769, %esi
	movl	%esi, -64(%rbp)
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L107:
	movq	16(%r12), %rax
	movq	%rax, -56(%rbp)
.L91:
	movq	names_lh(%rip), %rdi
	movq	%r14, %rsi
	call	OPENSSL_LH_retrieve@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L89
	movl	4(%rax), %eax
	testl	%eax, %eax
	je	.L90
	testl	%r13d, %r13d
	jne	.L90
	subl	$1, %ebx
	jne	.L107
	xorl	%r12d, %r12d
.L89:
	movq	obj_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L88:
	xorl	%r12d, %r12d
.L85:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L108
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	movq	16(%r12), %r12
	jmp	.L89
.L108:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE449:
	.size	OBJ_NAME_get, .-OBJ_NAME_get
	.p2align 4
	.globl	OBJ_NAME_add
	.type	OBJ_NAME_add, @function
OBJ_NAME_add:
.LFB450:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	leaq	init(%rip), %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	leaq	o_names_init_ossl_(%rip), %rsi
	subq	$8, %rsp
	call	CRYPTO_THREAD_run_once@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L109
	movl	o_names_init_ossl_ret_(%rip), %r12d
	testl	%r12d, %r12d
	jne	.L127
.L109:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	movl	$219, %edx
	leaq	.LC0(%rip), %rsi
	movl	$24, %edi
	xorl	%r12d, %r12d
	call	CRYPTO_malloc@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L111
	movl	%ebx, %eax
	andb	$127, %bh
	movq	%r14, %xmm1
	movq	obj_lock(%rip), %rdi
	andl	$32768, %eax
	movl	%ebx, (%r15)
	movq	%r13, %xmm0
	movl	%eax, 4(%r15)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r15)
	call	CRYPTO_THREAD_write_lock@PLT
	movq	names_lh(%rip), %rdi
	movq	%r15, %rsi
	call	OPENSSL_LH_insert@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L112
	movq	name_funcs_stack(%rip), %rdi
	testq	%rdi, %rdi
	je	.L114
	call	OPENSSL_sk_num@PLT
	movl	(%r12), %esi
	cmpl	%eax, %esi
	jl	.L128
.L114:
	movq	%r12, %rdi
	movl	$245, %edx
	leaq	.LC0(%rip), %rsi
	movl	$1, %r12d
	call	CRYPTO_free@PLT
.L111:
	movq	obj_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L128:
	movq	name_funcs_stack(%rip), %rdi
	call	OPENSSL_sk_value@PLT
	movq	16(%r12), %rdx
	movq	8(%r12), %rdi
	movl	(%r12), %esi
	call	*16(%rax)
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L112:
	movq	names_lh(%rip), %rdi
	movl	$1, %r12d
	call	OPENSSL_LH_error@PLT
	testl	%eax, %eax
	je	.L111
	movl	$249, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	CRYPTO_free@PLT
	jmp	.L111
	.cfi_endproc
.LFE450:
	.size	OBJ_NAME_add, .-OBJ_NAME_add
	.p2align 4
	.globl	OBJ_NAME_remove
	.type	OBJ_NAME_remove, @function
OBJ_NAME_remove:
.LFB451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	leaq	init(%rip), %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	%esi, %ebx
	leaq	o_names_init_ossl_(%rip), %rsi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	CRYPTO_THREAD_run_once@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L129
	movl	o_names_init_ossl_ret_(%rip), %r12d
	testl	%r12d, %r12d
	jne	.L145
.L129:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L146
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	movq	obj_lock(%rip), %rdi
	andb	$127, %bh
	xorl	%r12d, %r12d
	call	CRYPTO_THREAD_write_lock@PLT
	movq	names_lh(%rip), %rdi
	leaq	-64(%rbp), %rsi
	movq	%r13, -56(%rbp)
	movl	%ebx, -64(%rbp)
	call	OPENSSL_LH_delete@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L131
	movq	name_funcs_stack(%rip), %rdi
	testq	%rdi, %rdi
	je	.L133
	call	OPENSSL_sk_num@PLT
	movl	0(%r13), %esi
	cmpl	%esi, %eax
	jg	.L147
.L133:
	movl	$287, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	movl	$1, %r12d
	call	CRYPTO_free@PLT
.L131:
	movq	obj_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L147:
	movq	name_funcs_stack(%rip), %rdi
	call	OPENSSL_sk_value@PLT
	movq	16(%r13), %rdx
	movq	8(%r13), %rdi
	movl	0(%r13), %esi
	call	*16(%rax)
	jmp	.L133
.L146:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE451:
	.size	OBJ_NAME_remove, .-OBJ_NAME_remove
	.p2align 4
	.globl	OBJ_NAME_do_all
	.type	OBJ_NAME_do_all, @function
OBJ_NAME_do_all:
.LFB454:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	%edi, -32(%rbp)
	movq	names_lh(%rip), %rdi
	movq	%rsi, -24(%rbp)
	leaq	do_all_fn(%rip), %rsi
	movq	%rdx, -16(%rbp)
	leaq	-32(%rbp), %rdx
	call	OPENSSL_LH_doall_arg@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L151
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L151:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE454:
	.size	OBJ_NAME_do_all, .-OBJ_NAME_do_all
	.p2align 4
	.globl	OBJ_NAME_do_all_sorted
	.type	OBJ_NAME_do_all_sorted, @function
OBJ_NAME_do_all_sorted:
.LFB457:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edi, %ebx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	%edi, -80(%rbp)
	movq	names_lh(%rip), %rdi
	call	OPENSSL_LH_num_items@PLT
	movl	$354, %edx
	leaq	.LC0(%rip), %rsi
	leaq	0(,%rax,8), %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	je	.L152
	movq	names_lh(%rip), %rdi
	leaq	-64(%rbp), %rdx
	leaq	do_all_sorted_fn(%rip), %rax
	movl	$0, -76(%rbp)
	movq	%rax, -56(%rbp)
	leaq	do_all_fn(%rip), %rsi
	leaq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	movl	%ebx, -64(%rbp)
	call	OPENSSL_LH_doall_arg@PLT
	movslq	-76(%rbp), %rsi
	movq	-72(%rbp), %rdi
	leaq	do_all_sorted_cmp(%rip), %rcx
	movl	$8, %edx
	call	qsort@PLT
	movl	-76(%rbp), %eax
	testl	%eax, %eax
	jle	.L154
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L155:
	movq	-72(%rbp), %rax
	movq	%r13, %rsi
	movq	(%rax,%rbx,8), %rdi
	addq	$1, %rbx
	call	*%r12
	cmpl	%ebx, -76(%rbp)
	jg	.L155
.L154:
	movq	-72(%rbp), %rdi
	movl	$365, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
.L152:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L162
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L162:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE457:
	.size	OBJ_NAME_do_all_sorted, .-OBJ_NAME_do_all_sorted
	.p2align 4
	.globl	OBJ_NAME_cleanup
	.type	OBJ_NAME_cleanup, @function
OBJ_NAME_cleanup:
.LFB460:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	%edi, %ebx
	movq	names_lh(%rip), %rdi
	testq	%rdi, %rdi
	je	.L163
	movl	%ebx, free_type(%rip)
	call	OPENSSL_LH_get_down_load@PLT
	movq	names_lh(%rip), %rdi
	xorl	%esi, %esi
	movq	%rax, %r12
	call	OPENSSL_LH_set_down_load@PLT
	movq	names_lh(%rip), %rdi
	leaq	names_lh_free_doall(%rip), %rsi
	call	OPENSSL_LH_doall@PLT
	testl	%ebx, %ebx
	js	.L170
	movq	names_lh(%rip), %rdi
	popq	%rbx
	movq	%r12, %rsi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	OPENSSL_LH_set_down_load@PLT
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore_state
	movq	names_lh(%rip), %rdi
	call	OPENSSL_LH_free@PLT
	movq	name_funcs_stack(%rip), %rdi
	leaq	name_funcs_free(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	obj_lock(%rip), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	movq	$0, names_lh(%rip)
	movq	$0, name_funcs_stack(%rip)
	movq	$0, obj_lock(%rip)
.L163:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE460:
	.size	OBJ_NAME_cleanup, .-OBJ_NAME_cleanup
	.local	free_type
	.comm	free_type,4,4
	.local	o_names_init_ossl_ret_
	.comm	o_names_init_ossl_ret_,4,4
	.local	init
	.comm	init,4,4
	.local	name_funcs_stack
	.comm	name_funcs_stack,8,8
	.local	obj_lock
	.comm	obj_lock,8,8
	.data
	.align 4
	.type	names_type_num, @object
	.size	names_type_num, 4
names_type_num:
	.long	5
	.local	names_lh
	.comm	names_lh,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
