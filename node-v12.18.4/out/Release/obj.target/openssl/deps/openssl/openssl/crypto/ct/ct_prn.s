	.file	"ct_prn.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unknown status"
.LC1:
	.string	"not set"
.LC2:
	.string	"unknown log"
.LC3:
	.string	"unverified"
.LC4:
	.string	"invalid"
.LC5:
	.string	"valid"
.LC6:
	.string	"unknown version"
	.text
	.p2align 4
	.globl	SCT_validation_status_string
	.type	SCT_validation_status_string, @function
SCT_validation_status_string:
.LFB1285:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	SCT_get_validation_status@PLT
	cmpl	$5, %eax
	ja	.L2
	leaq	.L4(%rip), %rdx
	movl	%eax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L4:
	.long	.L10-.L4
	.long	.L8-.L4
	.long	.L7-.L4
	.long	.L6-.L4
	.long	.L5-.L4
	.long	.L3-.L4
	.text
	.p2align 4,,10
	.p2align 3
.L10:
	leaq	.LC1(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	leaq	.LC6(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	leaq	.LC3(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	leaq	.LC2(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	leaq	.LC5(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	leaq	.LC4(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2:
	.cfi_restore_state
	leaq	.LC0(%rip), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1285:
	.size	SCT_validation_status_string, .-SCT_validation_status_string
	.section	.rodata.str1.1
.LC7:
	.string	""
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"%*sSigned Certificate Timestamp:"
	.section	.rodata.str1.1
.LC9:
	.string	"\n%*sVersion   : "
.LC10:
	.string	"unknown\n%*s"
.LC11:
	.string	"v1 (0x0)"
.LC12:
	.string	"\n%*sLog       : %s"
.LC13:
	.string	"\n%*sLog ID    : "
.LC14:
	.string	"\n%*sTimestamp : "
.LC15:
	.string	"%.14s.%03dZ"
.LC16:
	.string	"\n%*sExtensions: "
.LC17:
	.string	"none"
.LC18:
	.string	"\n%*sSignature : "
.LC19:
	.string	"%02X%02X"
.LC20:
	.string	"%s"
.LC21:
	.string	"\n%*s            "
	.text
	.p2align 4
	.globl	SCT_print
	.type	SCT_print, @function
SCT_print:
.LFB1286:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leal	4(%rdx), %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L14
	movq	32(%rbx), %rdx
	movq	24(%rbx), %rsi
	movq	%rcx, %rdi
	call	CTLOG_STORE_get0_log_by_id@PLT
	leaq	.LC7(%rip), %rcx
	movl	%r13d, %edx
	movq	%r12, %rdi
	leaq	.LC8(%rip), %rsi
	movq	%rax, %r15
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	%r14d, %edx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC7(%rip), %rcx
	leaq	.LC9(%rip), %rsi
	call	BIO_printf@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jne	.L25
	xorl	%eax, %eax
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	testq	%r15, %r15
	je	.L17
	movq	%r15, %rdi
	call	CTLOG_get0_name@PLT
	leaq	.LC7(%rip), %rcx
	movl	%r14d, %edx
	movq	%r12, %rdi
	movq	%rax, %r8
	leaq	.LC12(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L17:
	leaq	.LC7(%rip), %rcx
	movl	%r14d, %edx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC13(%rip), %rsi
	addl	$16, %r13d
	call	BIO_printf@PLT
	movl	32(%rbx), %r8d
	movq	24(%rbx), %rcx
	movl	%r13d, %esi
	movl	$16, %edx
	movq	%r12, %rdi
	call	BIO_hex_string@PLT
	movl	%r14d, %edx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC7(%rip), %rcx
	leaq	.LC14(%rip), %rsi
	call	BIO_printf@PLT
	movq	40(%rbx), %r8
	movq	%r8, -88(%rbp)
	call	ASN1_GENERALIZEDTIME_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L19
	movq	-88(%rbp), %r8
	movq	%r15, %rdi
	movabsq	$13992196986280431, %rsi
	movabsq	$2361183241434822607, %r9
	movq	%r8, %rdx
	shrq	$10, %rdx
	movq	%rdx, %rax
	mulq	%rsi
	movq	%rdx, %rsi
	movq	%r8, %rdx
	shrq	$6, %rsi
	imulq	$86400000, %rsi, %rax
	subq	%rax, %rdx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	movq	%rcx, %rax
	mulq	%r9
	movq	%rdx, %rcx
	movl	%esi, %edx
	xorl	%esi, %esi
	shrq	$4, %rcx
	call	ASN1_GENERALIZEDTIME_adj@PLT
	movq	%r15, %rdi
	call	ASN1_STRING_get0_data@PLT
	movq	-88(%rbp), %r8
	movl	$20, %esi
	movabsq	$2361183241434822607, %r9
	movq	%rax, %rcx
	movq	%r8, %rdx
	shrq	$3, %rdx
	movq	%rdx, %rax
	mulq	%r9
	leaq	-80(%rbp), %r9
	xorl	%eax, %eax
	movq	%r9, %rdi
	movq	%r9, -88(%rbp)
	shrq	$4, %rdx
	imulq	$1000, %rdx, %rdx
	subl	%edx, %r8d
	leaq	.LC15(%rip), %rdx
	call	BIO_snprintf@PLT
	movq	-88(%rbp), %r9
	movq	%r15, %rdi
	movq	%r9, %rsi
	call	ASN1_GENERALIZEDTIME_set_string@PLT
	testl	%eax, %eax
	jne	.L36
.L20:
	movq	%r15, %rdi
	call	ASN1_GENERALIZEDTIME_free@PLT
.L19:
	leaq	.LC7(%rip), %rcx
	movl	%r14d, %edx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC16(%rip), %rsi
	call	BIO_printf@PLT
	movq	56(%rbx), %r8
	testq	%r8, %r8
	jne	.L21
	leaq	.LC17(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L22:
	leaq	.LC7(%rip), %rcx
	movl	%r14d, %edx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC18(%rip), %rsi
	call	BIO_printf@PLT
	movq	%rbx, %rdi
	call	SCT_get_signature_nid@PLT
	movl	%eax, %edi
	testl	%eax, %eax
	jne	.L23
	movzbl	65(%rbx), %ecx
	movzbl	64(%rbx), %edx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC19(%rip), %rsi
	call	BIO_printf@PLT
.L24:
	leaq	.LC7(%rip), %rcx
	movl	%r14d, %edx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC21(%rip), %rsi
	call	BIO_printf@PLT
	movq	72(%rbx), %rcx
	movl	80(%rbx), %r8d
	movl	%r13d, %esi
	movl	$16, %edx
	movq	%r12, %rdi
	call	BIO_hex_string@PLT
.L13:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L37
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	addl	$16, %r13d
	movq	%r12, %rdi
	leaq	.LC7(%rip), %rcx
	xorl	%eax, %eax
	movl	%r13d, %edx
	leaq	.LC10(%rip), %rsi
	call	BIO_printf@PLT
	movq	8(%rbx), %rcx
	movl	16(%rbx), %r8d
	movl	%r13d, %esi
	movl	$16, %edx
	movq	%r12, %rdi
	call	BIO_hex_string@PLT
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L14:
	leaq	.LC7(%rip), %rcx
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	%r14d, %edx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC7(%rip), %rcx
	leaq	.LC9(%rip), %rsi
	call	BIO_printf@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L25
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L23:
	call	OBJ_nid2ln@PLT
	leaq	.LC20(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L21:
	movq	48(%rbx), %rcx
	movl	$16, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	BIO_hex_string@PLT
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L36:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	ASN1_GENERALIZEDTIME_print@PLT
	jmp	.L20
.L37:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1286:
	.size	SCT_print, .-SCT_print
	.p2align 4
	.globl	SCT_LIST_print
	.type	SCT_LIST_print, @function
SCT_LIST_print:
.LFB1287:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%rcx, -64(%rbp)
	call	OPENSSL_sk_num@PLT
	movl	%eax, -52(%rbp)
	testl	%eax, %eax
	jle	.L38
	xorl	%r13d, %r13d
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L40:
	addl	$1, %r13d
	cmpl	-52(%rbp), %r13d
	je	.L38
.L41:
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r15, %rcx
	movl	%r14d, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	SCT_print
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	subl	$1, %eax
	cmpl	%r13d, %eax
	jle	.L40
	movq	-64(%rbp), %rdx
	leaq	.LC20(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	addl	$1, %r13d
	call	BIO_printf@PLT
	cmpl	-52(%rbp), %r13d
	jne	.L41
.L38:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1287:
	.size	SCT_LIST_print, .-SCT_LIST_print
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
