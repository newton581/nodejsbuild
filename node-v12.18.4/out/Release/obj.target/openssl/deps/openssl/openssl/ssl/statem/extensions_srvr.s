	.file	"extensions_srvr.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/ssl/statem/extensions_srvr.c"
	.text
	.p2align 4
	.globl	tls_parse_ctos_renegotiate
	.type	tls_parse_ctos_renegotiate, @function
tls_parse_ctos_renegotiate:
.LFB1614:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	8(%rsi), %rax
	testq	%rax, %rax
	je	.L2
	movq	(%rsi), %rdi
	subq	$1, %rax
	movzbl	(%rdi), %edx
	addq	$1, %rdi
	movq	%rax, 8(%rsi)
	movq	%rdi, (%rsi)
	cmpq	%rax, %rdx
	ja	.L2
	movq	168(%r12), %rbx
	leaq	(%rdi,%rdx), %rcx
	subq	%rdx, %rax
	movq	%rcx, (%rsi)
	movq	%rax, 8(%rsi)
	cmpq	904(%rbx), %rdx
	jne	.L14
	leaq	840(%rbx), %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L15
	movl	$1, 984(%rbx)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$50, %r9d
	movl	$336, %ecx
	movl	$464, %edx
	leaq	.LC0(%rip), %r8
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	movl	$57, %r9d
.L12:
	movq	%r12, %rdi
	movl	$337, %ecx
	movl	$464, %edx
	movl	$40, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	movl	$64, %r9d
	jmp	.L12
	.cfi_endproc
.LFE1614:
	.size	tls_parse_ctos_renegotiate, .-tls_parse_ctos_renegotiate
	.section	.rodata.str1.8
	.align 8
.LC1:
	.string	"../deps/openssl/openssl/ssl/statem/../packet_local.h"
	.text
	.p2align 4
	.globl	tls_parse_ctos_server_name
	.type	tls_parse_ctos_server_name, @function
tls_parse_ctos_server_name:
.LFB1615:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	8(%rsi), %rdx
	cmpq	$1, %rdx
	jbe	.L19
	movq	(%rsi), %rcx
	leaq	-2(%rdx), %rdi
	movzwl	(%rcx), %eax
	rolw	$8, %ax
	movzwl	%ax, %eax
	cmpq	%rax, %rdi
	je	.L44
.L19:
	movl	$106, %r9d
.L42:
	leaq	.LC0(%rip), %r8
	movl	$110, %ecx
	movl	$573, %edx
	movq	%r12, %rdi
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L16:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	leaq	(%rcx,%rdx), %rax
	movq	$0, 8(%rsi)
	movq	%rax, (%rsi)
	testq	%rdi, %rdi
	je	.L19
	leaq	-3(%rdx), %rax
	cmpq	$1, %rax
	jbe	.L20
	cmpb	$0, 2(%rcx)
	jne	.L20
	movzwl	3(%rcx), %r14d
	subq	$5, %rdx
	rolw	$8, %r14w
	movzwl	%r14w, %r14d
	cmpq	%r14, %rdx
	je	.L45
.L20:
	movl	$125, %r9d
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L45:
	movl	200(%r12), %eax
	leaq	5(%rcx), %r13
	testl	%eax, %eax
	je	.L21
	movq	8(%r12), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L23
	movl	(%rax), %eax
	cmpl	$65536, %eax
	je	.L23
	cmpl	$771, %eax
	jg	.L21
.L23:
	movq	1296(%r12), %rax
	xorl	%ebx, %ebx
	movq	544(%rax), %r15
	testq	%r15, %r15
	je	.L27
	movq	%r15, %rdi
	call	strlen@PLT
	cmpq	%r14, %rax
	je	.L46
.L27:
	movl	%ebx, 1864(%r12)
	movl	$1, %eax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L21:
	movl	$136, %r9d
	cmpq	$255, %r14
	ja	.L43
	xorl	%esi, %esi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	memchr@PLT
	testq	%rax, %rax
	je	.L25
	movl	$143, %r9d
.L43:
	leaq	.LC0(%rip), %r8
	movl	$110, %ecx
	movl	$573, %edx
	movq	%r12, %rdi
	movl	$112, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L25:
	movq	1600(%r12), %rdi
	movl	$153, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$449, %edx
	leaq	.LC1(%rip), %rsi
	xorl	%edi, %edi
	movq	$0, 1600(%r12)
	call	CRYPTO_free@PLT
	movl	$452, %ecx
	movq	%r14, %rsi
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	call	CRYPTO_strndup@PLT
	movq	%rax, 1600(%r12)
	testq	%rax, %rax
	je	.L47
	movl	$1, 1864(%r12)
	movl	$1, %eax
	jmp	.L16
.L46:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	xorl	%ebx, %ebx
	call	CRYPTO_memcmp@PLT
	testl	%eax, %eax
	sete	%bl
	jmp	.L27
.L47:
	movl	$156, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$573, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L16
	.cfi_endproc
.LFE1615:
	.size	tls_parse_ctos_server_name, .-tls_parse_ctos_server_name
	.p2align 4
	.globl	tls_parse_ctos_maxfragmentlen
	.type	tls_parse_ctos_maxfragmentlen, @function
tls_parse_ctos_maxfragmentlen:
.LFB1616:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpq	$1, 8(%rsi)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	jne	.L49
	movq	(%rsi), %rax
	movzbl	(%rax), %edx
	addq	$1, %rax
	movq	$0, 8(%rsi)
	movq	%rax, (%rsi)
	movzbl	%dl, %eax
	subl	$1, %eax
	cmpl	$3, %eax
	ja	.L57
	movl	200(%rdi), %ecx
	movq	1296(%rdi), %rax
	testl	%ecx, %ecx
	je	.L53
	cmpb	%dl, 600(%rax)
	jne	.L58
.L53:
	movb	%dl, 600(%rax)
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	movl	$186, %r9d
	movl	$110, %ecx
	movl	$571, %edx
	movl	$50, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	movl	$205, %r9d
.L56:
	movl	$232, %ecx
	movl	$571, %edx
	movl	$47, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	movl	$193, %r9d
	jmp	.L56
	.cfi_endproc
.LFE1616:
	.size	tls_parse_ctos_maxfragmentlen, .-tls_parse_ctos_maxfragmentlen
	.p2align 4
	.globl	tls_parse_ctos_srp
	.type	tls_parse_ctos_srp, @function
tls_parse_ctos_srp:
.LFB1617:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	movq	8(%rsi), %rax
	testq	%rax, %rax
	je	.L62
	movq	(%rsi), %rdx
	leaq	-1(%rax), %r12
	movzbl	(%rdx), %ecx
	cmpq	%rcx, %r12
	je	.L65
.L62:
	movl	$227, %r9d
	leaq	.LC0(%rip), %r8
	movl	$110, %ecx
	movq	%r13, %rdi
	movl	$576, %edx
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L59:
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	leaq	1(%rdx), %r14
	movq	$0, 8(%rsi)
	movq	%r12, %rdx
	leaq	(%r14,%r12), %rax
	movq	%r14, %rdi
	movq	%rax, (%rsi)
	xorl	%esi, %esi
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L62
	movq	2008(%r13), %rdi
	movl	$449, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$452, %ecx
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	.LC1(%rip), %rdx
	call	CRYPTO_strndup@PLT
	movq	%rax, %rdx
	movq	%rax, 2008(%r13)
	movl	$1, %eax
	testq	%rdx, %rdx
	jne	.L59
	movl	$238, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r13, %rdi
	movl	$576, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L59
	.cfi_endproc
.LFE1617:
	.size	tls_parse_ctos_srp, .-tls_parse_ctos_srp
	.p2align 4
	.globl	tls_parse_ctos_ec_pt_formats
	.type	tls_parse_ctos_ec_pt_formats, @function
tls_parse_ctos_ec_pt_formats:
.LFB1618:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	8(%rsi), %rax
	testq	%rax, %rax
	je	.L69
	movq	(%rsi), %rdx
	leaq	-1(%rax), %rbx
	movzbl	(%rdx), %ecx
	cmpq	%rcx, %rbx
	je	.L76
.L69:
	movl	$110, %ecx
	movl	$569, %edx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	movl	$255, %r9d
	leaq	.LC0(%rip), %r8
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
.L66:
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	leaq	1(%rdx), %r14
	movq	$0, 8(%rsi)
	leaq	(%r14,%rbx), %rax
	movq	%rax, (%rsi)
	testq	%rbx, %rbx
	je	.L69
	movl	200(%rdi), %r13d
	testl	%r13d, %r13d
	je	.L77
	movl	$1, %r13d
	popq	%rbx
	popq	%r12
	movl	%r13d, %eax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	movq	1696(%rdi), %rdi
	movl	$420, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$429, %ecx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	$0, 1696(%r12)
	leaq	.LC1(%rip), %rdx
	movq	$0, 1688(%r12)
	call	CRYPTO_memdup@PLT
	movq	%rax, 1696(%r12)
	testq	%rax, %rax
	je	.L70
	movq	%rbx, 1688(%r12)
	movl	$1, %r13d
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L70:
	movl	$264, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$569, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L66
	.cfi_endproc
.LFE1618:
	.size	tls_parse_ctos_ec_pt_formats, .-tls_parse_ctos_ec_pt_formats
	.p2align 4
	.globl	tls_parse_ctos_session_ticket
	.type	tls_parse_ctos_session_ticket, @function
tls_parse_ctos_session_ticket:
.LFB1619:
	.cfi_startproc
	endbr64
	movq	1744(%rdi), %rax
	testq	%rax, %rax
	je	.L85
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movl	8(%rsi), %edx
	movq	1752(%rdi), %rcx
	movq	(%rsi), %rsi
	call	*%rax
	testl	%eax, %eax
	je	.L86
	addq	$24, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	%r12, %rdi
	movl	$68, %ecx
	movl	$574, %edx
	movl	%eax, -20(%rbp)
	movl	$281, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movl	-20(%rbp), %eax
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1619:
	.size	tls_parse_ctos_session_ticket, .-tls_parse_ctos_session_ticket
	.p2align 4
	.globl	tls_parse_ctos_sig_algs_cert
	.type	tls_parse_ctos_sig_algs_cert, @function
tls_parse_ctos_sig_algs_cert:
.LFB1620:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	cmpq	$1, %rax
	jbe	.L90
	movq	(%rsi), %rcx
	subq	$2, %rax
	movzwl	(%rcx), %edx
	rolw	$8, %dx
	movzwl	%dx, %edx
	cmpq	%rdx, %rax
	je	.L98
.L90:
	movl	$296, %r9d
	movl	$110, %ecx
	movl	$615, %edx
	movl	$50, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L87:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L99
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	addq	$2, %rcx
	movq	$0, 8(%rsi)
	leaq	(%rcx,%rax), %rdx
	movq	%rcx, -32(%rbp)
	movq	%rdx, (%rsi)
	movq	%rax, -24(%rbp)
	testq	%rax, %rax
	je	.L90
	movl	200(%rdi), %eax
	testl	%eax, %eax
	je	.L91
.L92:
	movl	$1, %eax
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L91:
	leaq	-32(%rbp), %rsi
	movl	$1, %edx
	movq	%rdi, -40(%rbp)
	call	tls1_save_sigalgs@PLT
	movq	-40(%rbp), %rdi
	testl	%eax, %eax
	jne	.L92
	movl	$302, %r9d
	leaq	.LC0(%rip), %r8
	movl	$110, %ecx
	movl	%eax, -40(%rbp)
	movl	$615, %edx
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	movl	-40(%rbp), %eax
	jmp	.L87
.L99:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1620:
	.size	tls_parse_ctos_sig_algs_cert, .-tls_parse_ctos_sig_algs_cert
	.p2align 4
	.globl	tls_parse_ctos_sig_algs
	.type	tls_parse_ctos_sig_algs, @function
tls_parse_ctos_sig_algs:
.LFB1621:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	cmpq	$1, %rax
	jbe	.L103
	movq	(%rsi), %rcx
	subq	$2, %rax
	movzwl	(%rcx), %edx
	rolw	$8, %dx
	movzwl	%dx, %edx
	cmpq	%rdx, %rax
	je	.L111
.L103:
	movl	$317, %r9d
	movl	$110, %ecx
	movl	$575, %edx
	movl	$50, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L100:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L112
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	addq	$2, %rcx
	movq	$0, 8(%rsi)
	leaq	(%rcx,%rax), %rdx
	movq	%rcx, -32(%rbp)
	movq	%rdx, (%rsi)
	movq	%rax, -24(%rbp)
	testq	%rax, %rax
	je	.L103
	movl	200(%rdi), %eax
	testl	%eax, %eax
	je	.L104
.L105:
	movl	$1, %eax
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L104:
	xorl	%edx, %edx
	leaq	-32(%rbp), %rsi
	movq	%rdi, -40(%rbp)
	call	tls1_save_sigalgs@PLT
	movq	-40(%rbp), %rdi
	testl	%eax, %eax
	jne	.L105
	movl	$323, %r9d
	leaq	.LC0(%rip), %r8
	movl	$110, %ecx
	movl	%eax, -40(%rbp)
	movl	$575, %edx
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	movl	-40(%rbp), %eax
	jmp	.L100
.L112:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1621:
	.size	tls_parse_ctos_sig_algs, .-tls_parse_ctos_sig_algs
	.p2align 4
	.globl	tls_parse_ctos_status_request
	.type	tls_parse_ctos_status_request, @function
tls_parse_ctos_status_request:
.LFB1622:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	200(%rdi), %eax
	testl	%eax, %eax
	jne	.L150
	testq	%rcx, %rcx
	je	.L114
.L150:
	movl	$1, %eax
.L113:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L153
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore_state
	movq	8(%rsi), %rax
	movq	%rdi, %r12
	movq	%rsi, %rbx
	movl	$346, %r9d
	testq	%rax, %rax
	je	.L151
	movq	(%rsi), %rdx
	movzbl	(%rdx), %esi
	movl	%esi, 1608(%rdi)
	movl	%esi, %ecx
	leaq	1(%rdx), %rsi
	movq	%rsi, (%rbx)
	leaq	-1(%rax), %rsi
	movq	%rsi, 8(%rbx)
	cmpb	$1, %cl
	je	.L118
	movl	$-1, 1608(%rdi)
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L118:
	cmpq	$1, %rsi
	jbe	.L119
	movzwl	1(%rdx), %r14d
	subq	$3, %rax
	rolw	$8, %r14w
	movzwl	%r14w, %r14d
	cmpq	%r14, %rax
	jb	.L119
	leaq	3(%rdx), %r13
	subq	%r14, %rax
	movq	1632(%rdi), %rdi
	movq	OCSP_RESPID_free@GOTPCREL(%rip), %rsi
	leaq	0(%r13,%r14), %rdx
	movq	%rax, 8(%rbx)
	movq	%rdx, (%rbx)
	call	OPENSSL_sk_pop_free@PLT
	testq	%r14, %r14
	jne	.L154
	movq	$0, 1632(%r12)
.L123:
	movq	8(%rbx), %r13
	cmpq	$1, %r13
	jbe	.L131
	movq	(%rbx), %rax
	subq	$2, %r13
	movzwl	(%rax), %edx
	rolw	$8, %dx
	movzwl	%dx, %edx
	cmpq	%rdx, %r13
	je	.L155
.L131:
	movl	$422, %r9d
	.p2align 4,,10
	.p2align 3
.L151:
	leaq	.LC0(%rip), %r8
	movl	$110, %ecx
	movl	$577, %edx
	movq	%r12, %rdi
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L119:
	movl	$360, %r9d
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L154:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 1632(%r12)
	testq	%rax, %rax
	je	.L156
	cmpq	$1, %r14
	je	.L126
	leaq	-64(%rbp), %rax
	movq	%rax, -72(%rbp)
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L160:
	leaq	2(%r13), %rax
	subq	%rdx, %r14
	leaq	(%rax,%rdx), %r13
	testq	%rdx, %rdx
	je	.L126
	movq	-72(%rbp), %rsi
	xorl	%edi, %edi
	movq	%rax, -64(%rbp)
	call	d2i_OCSP_RESPID@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L157
	cmpq	%r13, -64(%rbp)
	jne	.L158
	movq	1632(%r12), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L159
	testq	%r14, %r14
	je	.L123
	cmpq	$1, %r14
	je	.L126
.L124:
	movzwl	0(%r13), %edx
	subq	$2, %r14
	rolw	$8, %dx
	movzwl	%dx, %edx
	cmpq	%rdx, %r14
	jnb	.L160
.L126:
	movl	$388, %r9d
	jmp	.L151
.L156:
	movl	$373, %r9d
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
	movq	%r12, %rdi
	movl	$577, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L113
.L158:
	movq	%rax, %rdi
	call	OCSP_RESPID_free@PLT
	movl	$405, %r9d
	jmp	.L151
.L157:
	movl	$398, %r9d
	jmp	.L151
.L159:
	movq	%r15, %rdi
	call	OCSP_RESPID_free@PLT
	movl	$413, %r9d
	movl	$68, %ecx
	movq	%r12, %rdi
	leaq	.LC0(%rip), %r8
	movl	$577, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L113
.L155:
	addq	$2, %rax
	movq	$0, 8(%rbx)
	leaq	(%rax,%r13), %r14
	movq	%r14, (%rbx)
	testq	%r13, %r13
	je	.L150
	movq	1640(%r12), %rdi
	movq	X509_EXTENSION_free@GOTPCREL(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	OPENSSL_sk_pop_free@PLT
	leaq	-64(%rbp), %rsi
	movq	%r13, %rdx
	xorl	%edi, %edi
	call	d2i_X509_EXTENSIONS@PLT
	movq	%rax, 1640(%r12)
	testq	%rax, %rax
	je	.L134
	movl	$1, %eax
	cmpq	%r14, -64(%rbp)
	je	.L113
.L134:
	movl	$435, %r9d
	jmp	.L151
.L153:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1622:
	.size	tls_parse_ctos_status_request, .-tls_parse_ctos_status_request
	.p2align 4
	.globl	tls_parse_ctos_npn
	.type	tls_parse_ctos_npn, @function
tls_parse_ctos_npn:
.LFB1623:
	.cfi_startproc
	endbr64
	movq	168(%rdi), %rax
	cmpq	$0, 408(%rax)
	je	.L162
	cmpq	$0, 544(%rax)
	jne	.L163
.L162:
	movl	$1, 988(%rax)
.L163:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1623:
	.size	tls_parse_ctos_npn, .-tls_parse_ctos_npn
	.p2align 4
	.globl	tls_parse_ctos_alpn
	.type	tls_parse_ctos_alpn, @function
tls_parse_ctos_alpn:
.LFB1624:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	168(%rdi), %rdx
	cmpq	$0, 408(%rdx)
	je	.L165
	cmpq	$0, 544(%rdx)
	movl	$1, %eax
	jne	.L164
.L165:
	movq	8(%rsi), %rax
	cmpq	$1, %rax
	jbe	.L168
	movq	(%rsi), %rcx
	subq	$2, %rax
	movzwl	(%rcx), %ebx
	rolw	$8, %bx
	movzwl	%bx, %ebx
	cmpq	%rbx, %rax
	je	.L177
.L168:
	movl	$474, %r9d
.L176:
	leaq	.LC0(%rip), %r8
	movl	$110, %ecx
	movl	$567, %edx
	movq	%r12, %rdi
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L164:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	leaq	2(%rcx), %r13
	movq	$0, 8(%rsi)
	leaq	0(%r13,%rbx), %rax
	movq	%rax, (%rsi)
	cmpq	$1, %rbx
	jbe	.L168
	movq	%r13, %rsi
	movq	%rbx, %rax
.L171:
	movzbl	(%rsi), %ecx
	subq	$1, %rax
	cmpq	%rcx, %rax
	jb	.L169
	leaq	1(%rsi,%rcx), %rsi
	subq	%rcx, %rax
	testq	%rcx, %rcx
	je	.L169
	testq	%rax, %rax
	jne	.L171
	movq	1008(%rdx), %rdi
	leaq	.LC0(%rip), %rsi
	movl	$490, %edx
	call	CRYPTO_free@PLT
	movq	168(%r12), %r14
	movl	$420, %edx
	xorl	%edi, %edi
	leaq	.LC1(%rip), %rsi
	movq	$0, 1008(%r14)
	movq	$0, 1016(%r14)
	call	CRYPTO_free@PLT
	movl	$429, %ecx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	$0, 1008(%r14)
	leaq	.LC1(%rip), %rdx
	movq	$0, 1016(%r14)
	call	CRYPTO_memdup@PLT
	movq	%rax, 1008(%r14)
	testq	%rax, %rax
	je	.L172
	movq	%rbx, 1016(%r14)
	movl	$1, %eax
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L169:
	movl	$484, %r9d
	jmp	.L176
.L172:
	movl	$495, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$567, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L164
	.cfi_endproc
.LFE1624:
	.size	tls_parse_ctos_alpn, .-tls_parse_ctos_alpn
	.p2align 4
	.globl	tls_parse_ctos_use_srtp
	.type	tls_parse_ctos_use_srtp, @function
tls_parse_ctos_use_srtp:
.LFB1625:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -64(%rbp)
	call	SSL_get_srtp_profiles@PLT
	testq	%rax, %rax
	je	.L195
	movq	-64(%rbp), %rdi
	movq	8(%rdi), %rax
	cmpq	$1, %rax
	jbe	.L181
	movq	(%rdi), %rcx
	subq	$2, %rax
	movzwl	(%rcx), %edx
	leaq	2(%rcx), %rbx
	movq	%rax, 8(%rdi)
	movq	%rbx, (%rdi)
	movl	%edx, %esi
	rolw	$8, %si
	andb	$1, %dh
	jne	.L181
	movzwl	%si, %r14d
	movq	%r14, -56(%rbp)
	cmpq	%rax, %r14
	ja	.L181
	leaq	(%rbx,%r14), %rdx
	subq	%r14, %rax
	movq	%rdx, (%rdi)
	movq	%rax, 8(%rdi)
	movq	%r15, %rdi
	call	SSL_get_srtp_profiles@PLT
	movq	$0, 1920(%r15)
	movq	%rax, %rdi
	movq	%rax, %r13
	call	OPENSSL_sk_num@PLT
	movl	%eax, %r12d
	testq	%r14, %r14
	je	.L192
	cmpq	$1, -56(%rbp)
	je	.L185
	movq	%rbx, -72(%rbp)
.L186:
	movq	-72(%rbp), %rax
	subq	$2, -56(%rbp)
	xorl	%r14d, %r14d
	movzwl	(%rax), %eax
	movl	%eax, %ebx
	movw	%ax, -74(%rbp)
	rolw	$8, %bx
	movzwl	%bx, %ebx
	testl	%r12d, %r12d
	jg	.L190
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L189:
	addl	$1, %r14d
	cmpl	%r12d, %r14d
	je	.L188
.L190:
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	cmpq	%rbx, 8(%rax)
	jne	.L189
	movq	%rax, 1920(%r15)
	movl	%r14d, %r12d
.L188:
	cmpq	$0, -56(%rbp)
	je	.L192
	addq	$2, -72(%rbp)
	cmpq	$1, -56(%rbp)
	jne	.L186
.L185:
	movl	$531, %r9d
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L181:
	movl	$519, %r9d
.L207:
	leaq	.LC0(%rip), %r8
	movl	$353, %ecx
.L206:
	movq	%r15, %rdi
	movl	$465, %edx
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L195:
	.cfi_restore_state
	addq	$40, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L192:
	.cfi_restore_state
	movq	-64(%rbp), %rax
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L208
	movq	-64(%rbp), %rbx
	subq	$1, %rax
	movq	(%rbx), %rdx
	movzbl	(%rdx), %ecx
	addq	$1, %rdx
	movq	%rax, 8(%rbx)
	movq	%rdx, (%rbx)
	cmpq	%rax, %rcx
	ja	.L194
	addq	%rcx, %rdx
	subq	%rcx, %rax
	movq	%rdx, (%rbx)
	movq	%rax, 8(%rbx)
	je	.L195
.L194:
	movl	$563, %r9d
	leaq	.LC0(%rip), %r8
	movl	$352, %ecx
	jmp	.L206
.L208:
	movl	$556, %r9d
	jmp	.L207
	.cfi_endproc
.LFE1625:
	.size	tls_parse_ctos_use_srtp, .-tls_parse_ctos_use_srtp
	.p2align 4
	.globl	tls_parse_ctos_etm
	.type	tls_parse_ctos_etm, @function
tls_parse_ctos_etm:
.LFB1626:
	.cfi_startproc
	endbr64
	testb	$8, 1494(%rdi)
	jne	.L210
	movl	$1, 1812(%rdi)
.L210:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1626:
	.size	tls_parse_ctos_etm, .-tls_parse_ctos_etm
	.p2align 4
	.globl	tls_parse_ctos_psk_kex_modes
	.type	tls_parse_ctos_psk_kex_modes, @function
tls_parse_ctos_psk_kex_modes:
.LFB1627:
	.cfi_startproc
	endbr64
	movq	8(%rsi), %rdx
	testq	%rdx, %rdx
	je	.L212
	movq	(%rsi), %rax
	leaq	-1(%rdx), %rcx
	movzbl	(%rax), %r8d
	cmpq	%r8, %rcx
	je	.L232
.L212:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$110, %ecx
	movl	$572, %edx
	movl	$50, %esi
	movl	$594, %r9d
	leaq	.LC0(%rip), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L232:
	.cfi_restore 6
	addq	%rax, %rdx
	movq	$0, 8(%rsi)
	movq	%rdx, (%rsi)
	testq	%rcx, %rcx
	je	.L212
	addq	%rax, %rcx
.L213:
	movzbl	1(%rax), %edx
	cmpl	$1, %edx
	je	.L233
	testl	%edx, %edx
	jne	.L215
	testb	$4, 1493(%rdi)
	je	.L215
	orl	$1, 1808(%rdi)
	.p2align 4,,10
	.p2align 3
.L215:
	addq	$1, %rax
	cmpq	%rax, %rcx
	jne	.L213
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L233:
	orl	$2, 1808(%rdi)
	jmp	.L215
	.cfi_endproc
.LFE1627:
	.size	tls_parse_ctos_psk_kex_modes, .-tls_parse_ctos_psk_kex_modes
	.p2align 4
	.globl	tls_parse_ctos_key_share
	.type	tls_parse_ctos_key_share, @function
tls_parse_ctos_key_share:
.LFB1628:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	200(%rdi), %eax
	testl	%eax, %eax
	je	.L235
	testb	$2, 1808(%rdi)
	je	.L243
.L235:
	movq	168(%r12), %rax
	cmpq	$0, 1032(%rax)
	jne	.L280
	movq	8(%rsi), %rax
	cmpq	$1, %rax
	jbe	.L238
	movq	(%rsi), %r15
	leaq	-2(%rax), %rbx
	movzwl	(%r15), %r13d
	rolw	$8, %r13w
	movzwl	%r13w, %r14d
	cmpq	%r14, %rbx
	je	.L281
.L238:
	movl	$636, %r9d
.L278:
	movl	$159, %ecx
	movl	$463, %edx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	leaq	.LC0(%rip), %r8
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
.L234:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L282
	addq	$72, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L280:
	.cfi_restore_state
	movl	$68, %ecx
	movl	$463, %edx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	movl	$630, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L243:
	movl	$1, %r13d
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L281:
	leaq	2(%r15), %rax
	movq	$0, 8(%rsi)
	movq	%r12, %rdi
	leaq	(%rax,%rbx), %rdx
	movq	%rax, -88(%rbp)
	movq	%rdx, (%rsi)
	leaq	-64(%rbp), %rdx
	leaq	-72(%rbp), %rsi
	call	tls1_get_supported_groups@PLT
	movq	1720(%r12), %r9
	movq	-88(%rbp), %rax
	testq	%r9, %r9
	je	.L283
	movq	168(%r12), %rdx
	cmpw	$0, 1030(%rdx)
	je	.L256
	testq	%rbx, %rbx
	je	.L241
.L256:
	testq	%r14, %r14
	je	.L243
	cmpw	$3, %r13w
	jbe	.L246
	movq	1728(%r12), %rcx
	movzwl	2(%r15), %esi
	xorl	%r13d, %r13d
	movq	%rcx, -88(%rbp)
	rolw	$8, %si
	movzwl	%si, %ebx
.L245:
	movzwl	2(%rax), %r10d
	subq	$4, %r14
	rolw	$8, %r10w
	movzwl	%r10w, %r15d
	cmpq	%r15, %r14
	jb	.L246
	subq	%r15, %r14
	testq	%r15, %r15
	je	.L246
	addq	$4, %rax
	movq	%rax, -96(%rbp)
	testl	%r13d, %r13d
	jne	.L247
	movq	168(%r12), %rax
	movzwl	1030(%rax), %eax
	testw	%ax, %ax
	je	.L248
	testq	%r14, %r14
	jne	.L257
	cmpl	%ebx, %eax
	jne	.L257
.L248:
	movq	-88(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r9, %rcx
	movl	%ebx, %esi
	movq	%r12, %rdi
	movq	%r9, -104(%rbp)
	call	check_in_list@PLT
	movq	-104(%rbp), %r9
	testl	%eax, %eax
	je	.L284
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rdx
	movl	$1, %r8d
	movl	%ebx, %esi
	movq	%r12, %rdi
	movq	%r9, -104(%rbp)
	call	check_in_list@PLT
	movq	-104(%rbp), %r9
	testl	%eax, %eax
	je	.L255
	movq	168(%r12), %rdx
	movl	%ebx, %edi
	movq	%r9, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	ssl_generate_param_group@PLT
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, 1032(%rdx)
	je	.L285
	movq	168(%r12), %rax
	movq	-96(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r9, -104(%rbp)
	movw	%bx, 1030(%rax)
	movq	1032(%rax), %rdi
	call	EVP_PKEY_set1_tls_encodedpoint@PLT
	movq	-104(%rbp), %r9
	testl	%eax, %eax
	movl	%eax, %r13d
	je	.L286
.L247:
	movl	$1, %r13d
.L251:
	testq	%r14, %r14
	je	.L243
	cmpq	$1, %r14
	je	.L246
	movq	-96(%rbp), %rax
	leaq	-2(%r14), %rdx
	addq	%r15, %rax
	movzwl	(%rax), %esi
	rolw	$8, %si
	movzwl	%si, %ebx
	cmpq	$1, %rdx
	ja	.L245
	.p2align 4,,10
	.p2align 3
.L246:
	movl	$671, %r9d
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L283:
	movl	$209, %ecx
	movl	$463, %edx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	movl	$651, %r9d
	leaq	.LC0(%rip), %r8
	movl	$109, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L255:
	xorl	%r13d, %r13d
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L241:
	movl	$662, %r9d
.L279:
	movl	$108, %ecx
	movl	$463, %edx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	leaq	.LC0(%rip), %r8
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L234
.L257:
	movl	$690, %r9d
	leaq	.LC0(%rip), %r8
	movl	$108, %ecx
	movq	%r12, %rdi
	movl	$463, %edx
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L234
.L284:
	movl	$697, %r9d
	jmp	.L279
.L282:
	call	__stack_chk_fail@PLT
.L286:
	movl	$719, %r9d
	leaq	.LC0(%rip), %r8
	movl	$306, %ecx
	movq	%r12, %rdi
	movl	$463, %edx
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L234
.L285:
	movl	$709, %r9d
	leaq	.LC0(%rip), %r8
	movl	$314, %ecx
	movq	%r12, %rdi
	movl	$463, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L234
	.cfi_endproc
.LFE1628:
	.size	tls_parse_ctos_key_share, .-tls_parse_ctos_key_share
	.p2align 4
	.globl	tls_parse_ctos_cookie
	.type	tls_parse_ctos_cookie, @function
tls_parse_ctos_cookie:
.LFB1629:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$376, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	1440(%rdi), %rax
	cmpq	$0, 224(%rax)
	je	.L391
	movq	168(%rdi), %rax
	movq	%rdi, %r12
	movq	(%rax), %rax
	testb	$8, %ah
	je	.L391
	movq	8(%rsi), %r10
	cmpq	$1, %r10
	jbe	.L291
	movq	(%rsi), %rbx
	leaq	-2(%r10), %rax
	movzwl	(%rbx), %r9d
	rolw	$8, %r9w
	movzwl	%r9w, %r13d
	cmpq	%r13, %rax
	je	.L397
.L291:
	movl	$752, %r9d
.L392:
	movl	$159, %ecx
	movl	$614, %edx
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	leaq	.LC0(%rip), %r8
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
.L287:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L398
	addq	$4472, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L303:
	.cfi_restore_state
	movzbl	10(%rbx), %eax
	movl	11(%rbx), %ebx
	xorl	%edi, %edi
	movq	%rdx, -4504(%rbp)
	bswap	%ebx
	movb	%al, -4480(%rbp)
	movl	%ebx, %ebx
	call	time@PLT
	cmpq	%rbx, %rax
	jb	.L391
	subq	%rbx, %rax
	cmpq	$600, %rax
	ja	.L391
	movq	1440(%r12), %rax
	movq	-4504(%rbp), %rdx
	leaq	1(%r14), %rsi
	movq	%r12, %rdi
	call	*224(%rax)
	testl	%eax, %eax
	je	.L399
	leaq	-4352(%rbp), %rbx
	leaq	-4432(%rbp), %r14
	xorl	%ecx, %ecx
	movl	$4296, %edx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	WPACKET_init_static_len@PLT
	testl	%eax, %eax
	je	.L400
	movl	$1, %edx
	movl	$2, %esi
	movq	%r14, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L307
	movl	$3, %esi
	movq	%r14, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L307
	movl	$2, %edx
	movl	$771, %esi
	movq	%r14, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L307
	movl	$32, %edx
	leaq	hrrrandom(%rip), %rsi
	movq	%r14, %rdi
	call	WPACKET_memcpy@PLT
	testl	%eax, %eax
	je	.L307
	movq	1368(%r12), %rdx
	leaq	1336(%r12), %rsi
	movl	$1, %ecx
	movq	%r14, %rdi
	call	WPACKET_sub_memcpy__@PLT
	testl	%eax, %eax
	je	.L307
	movq	168(%r12), %rax
	leaq	-4440(%rbp), %rdx
	movq	%r14, %rsi
	movq	568(%rax), %rdi
	movq	8(%r12), %rax
	call	*152(%rax)
	testl	%eax, %eax
	je	.L307
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%r14, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L307
	movl	$2, %esi
	movq	%r14, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L307
	movl	$2, %edx
	movl	$43, %esi
	movq	%r14, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L309
	movl	$2, %esi
	movq	%r14, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L309
	movl	(%r12), %esi
	movl	$2, %edx
	movq	%r14, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L309
	movq	%r14, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L309
	cmpb	$0, -4480(%rbp)
	je	.L310
	movl	$2, %edx
	movl	$51, %esi
	movq	%r14, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L312
	movl	$2, %esi
	movq	%r14, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L312
	movq	168(%r12), %rax
	movl	$2, %edx
	movq	%r14, %rdi
	movzwl	1030(%rax), %esi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L312
	movq	%r14, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L312
.L310:
	movl	$2, %edx
	movl	$44, %esi
	movq	%r14, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L314
	movl	$2, %esi
	movq	%r14, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L314
	movq	-4472(%rbp), %rsi
	movl	$2, %ecx
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	WPACKET_sub_memcpy__@PLT
	testl	%eax, %eax
	je	.L314
	movq	%r14, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L314
	movq	%r14, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L314
	movq	%r14, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L314
	leaq	-4448(%rbp), %rsi
	movq	%r14, %rdi
	call	WPACKET_get_total_written@PLT
	testl	%eax, %eax
	je	.L314
	movq	%r14, %rdi
	call	WPACKET_finish@PLT
	testl	%eax, %eax
	je	.L314
	movq	-4448(%rbp), %r8
	movq	-4496(%rbp), %rdx
	movq	%rbx, %rcx
	movq	%r12, %rdi
	movq	-4488(%rbp), %rsi
	call	create_synthetic_message_hash@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L287
	movl	$1, 1248(%r12)
	movl	$1, 1840(%r12)
	.p2align 4,,10
	.p2align 3
.L391:
	movl	$1, %r15d
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L397:
	leaq	2(%rbx), %rax
	movq	$0, 8(%rsi)
	movq	%rax, -4472(%rbp)
	addq	%r13, %rax
	movq	%rax, (%rsi)
	cmpq	$31, %r13
	jbe	.L401
	movq	%r10, -4480(%rbp)
	call	EVP_MD_CTX_new@PLT
	movl	$32, %ecx
	xorl	%esi, %esi
	movl	$855, %edi
	movq	%rax, %r14
	movq	1904(%r12), %rax
	leaq	680(%rax), %rdx
	call	EVP_PKEY_new_raw_private_key@PLT
	movq	%rax, %r15
	testq	%r14, %r14
	je	.L294
	testq	%rax, %rax
	movq	-4480(%rbp), %r10
	je	.L294
	movq	%r10, -4480(%rbp)
	movq	$32, -4456(%rbp)
	call	EVP_sha256@PLT
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%r15, %r8
	movq	%rax, %rdx
	movq	%r14, %rdi
	call	EVP_DigestSignInit@PLT
	movq	-4480(%rbp), %r10
	testl	%eax, %eax
	jle	.L297
	leaq	-4384(%rbp), %r11
	leaq	-34(%r10), %r8
	movq	%r14, %rdi
	movq	-4472(%rbp), %rcx
	leaq	-4456(%rbp), %rdx
	movq	%r11, %rsi
	movq	%r8, -4488(%rbp)
	movq	%r11, -4480(%rbp)
	call	EVP_DigestSign@PLT
	testl	%eax, %eax
	jle	.L297
	cmpq	$32, -4456(%rbp)
	movq	-4480(%rbp), %r11
	movq	-4488(%rbp), %r8
	jne	.L297
	movq	%r14, %rdi
	movq	%r11, -4488(%rbp)
	movq	%r8, -4480(%rbp)
	call	EVP_MD_CTX_free@PLT
	movq	%r15, %rdi
	call	EVP_PKEY_free@PLT
	movq	-4472(%rbp), %rax
	movl	$32, %edx
	movq	-4480(%rbp), %r8
	movq	-4488(%rbp), %r11
	leaq	(%rax,%r8), %rsi
	movq	%r11, %rdi
	call	CRYPTO_memcmp@PLT
	movl	$798, %r9d
	movl	%eax, %r15d
	testl	%eax, %eax
	jne	.L396
	cmpw	$0, 2(%rbx)
	jne	.L391
	cmpw	$1027, 4(%rbx)
	jne	.L402
	movzwl	6(%rbx), %eax
	movq	168(%r12), %rdx
	rolw	$8, %ax
	cmpw	%ax, 1030(%rdx)
	jne	.L301
	movq	568(%rdx), %r14
	leaq	8(%rbx), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	ssl_get_cipher_by_char@PLT
	cmpq	%rax, %r14
	je	.L403
.L301:
	movl	$848, %r9d
	leaq	.LC0(%rip), %r8
	movl	$186, %ecx
	movq	%r12, %rdi
	movl	$614, %edx
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L297:
	movq	%r14, %rdi
	call	EVP_MD_CTX_free@PLT
	movq	%r15, %rdi
	call	EVP_PKEY_free@PLT
	movl	$789, %r9d
.L395:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
.L394:
	movl	$614, %edx
	movl	$80, %esi
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	call	ossl_statem_fatal@PLT
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L401:
	movl	$762, %r9d
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L294:
	movq	%r14, %rdi
	call	EVP_MD_CTX_free@PLT
	movq	%r15, %rdi
	call	EVP_PKEY_free@PLT
	movl	$777, %r9d
	movl	$65, %ecx
	leaq	.LC0(%rip), %r8
	jmp	.L394
.L399:
	movl	$873, %r9d
.L396:
	movl	$308, %ecx
	movl	$614, %edx
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	leaq	.LC0(%rip), %r8
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L287
.L402:
	movl	$824, %r9d
	leaq	.LC0(%rip), %r8
	movl	$116, %ecx
	movq	%r12, %rdi
	movl	$614, %edx
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L287
.L398:
	call	__stack_chk_fail@PLT
.L403:
	movzwl	15(%rbx), %edx
	leaq	-15(%r13), %rax
	rolw	$8, %dx
	movzwl	%dx, %ecx
	movq	%rcx, -4496(%rbp)
	cmpq	%rcx, %rax
	jb	.L302
	subq	%rcx, %rax
	je	.L302
	leaq	17(%rbx), %r14
	subq	$1, %rax
	movq	%r14, -4488(%rbp)
	addq	%rcx, %r14
	movzbl	(%r14), %edx
	cmpq	%rdx, %rax
	jb	.L302
	subq	%rdx, %rax
	cmpq	$32, %rax
	je	.L303
.L302:
	movl	$858, %r9d
	leaq	.LC0(%rip), %r8
	movl	$159, %ecx
	movq	%r12, %rdi
	movl	$614, %edx
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L287
.L309:
	movq	%r14, %rdi
	call	WPACKET_cleanup@PLT
	movl	$908, %r9d
.L393:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	$614, %edx
	movq	%r12, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L287
.L314:
	movq	%r14, %rdi
	call	WPACKET_cleanup@PLT
	movl	$932, %r9d
	jmp	.L393
.L307:
	movq	%r14, %rdi
	call	WPACKET_cleanup@PLT
	movl	$899, %r9d
	jmp	.L393
.L400:
	movl	$884, %r9d
	jmp	.L395
.L312:
	movq	%r14, %rdi
	call	WPACKET_cleanup@PLT
	movl	$918, %r9d
	jmp	.L393
	.cfi_endproc
.LFE1629:
	.size	tls_parse_ctos_cookie, .-tls_parse_ctos_cookie
	.p2align 4
	.globl	tls_parse_ctos_supported_groups
	.type	tls_parse_ctos_supported_groups, @function
tls_parse_ctos_supported_groups:
.LFB1630:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	cmpq	$1, %rax
	jbe	.L407
	movq	(%rsi), %rcx
	subq	$2, %rax
	movzwl	(%rcx), %edx
	rolw	$8, %dx
	movzwl	%dx, %edi
	cmpq	%rdi, %rax
	je	.L422
.L407:
	movl	$964, %r9d
	leaq	.LC0(%rip), %r8
	movl	$110, %ecx
	movq	%r12, %rdi
	movl	$578, %edx
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L404:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L423
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L422:
	.cfi_restore_state
	addq	$2, %rcx
	movq	$0, 8(%rsi)
	leaq	(%rcx,%rax), %rdi
	movq	%rcx, -48(%rbp)
	movq	%rdi, (%rsi)
	movq	%rax, -40(%rbp)
	testq	%rax, %rax
	je	.L407
	andl	$1, %edx
	jne	.L407
	movl	200(%r12), %eax
	testl	%eax, %eax
	je	.L408
	movq	8(%r12), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L410
	movl	(%rax), %eax
	cmpl	$65536, %eax
	je	.L410
	cmpl	$771, %eax
	jg	.L408
	.p2align 4,,10
	.p2align 3
.L410:
	movl	$1, %eax
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L408:
	movq	1728(%r12), %rdi
	movl	$970, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	leaq	1720(%r12), %rdx
	leaq	-48(%rbp), %rdi
	movq	$0, 1728(%r12)
	movq	$0, 1720(%r12)
	leaq	1728(%r12), %rsi
	call	tls1_save_u16@PLT
	testl	%eax, %eax
	jne	.L410
	movl	$68, %ecx
	movl	$578, %edx
	movq	%r12, %rdi
	movl	%eax, -52(%rbp)
	movl	$976, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movl	-52(%rbp), %eax
	jmp	.L404
.L423:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1630:
	.size	tls_parse_ctos_supported_groups, .-tls_parse_ctos_supported_groups
	.p2align 4
	.globl	tls_parse_ctos_ems
	.type	tls_parse_ctos_ems, @function
tls_parse_ctos_ems:
.LFB1631:
	.cfi_startproc
	endbr64
	cmpq	$0, 8(%rsi)
	jne	.L431
	movq	168(%rdi), %rax
	orq	$512, (%rax)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L431:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$110, %ecx
	movl	$570, %edx
	movl	$50, %esi
	movl	$992, %r9d
	leaq	.LC0(%rip), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1631:
	.size	tls_parse_ctos_ems, .-tls_parse_ctos_ems
	.p2align 4
	.globl	tls_parse_ctos_early_data
	.type	tls_parse_ctos_early_data, @function
tls_parse_ctos_early_data:
.LFB1632:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpq	$0, 8(%rsi)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	jne	.L437
	movl	1248(%rdi), %edx
	movl	$1, %eax
	testl	%edx, %edx
	jne	.L438
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L437:
	.cfi_restore_state
	movl	$1007, %r9d
	movl	$110, %ecx
	movl	$568, %edx
	movl	$50, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L438:
	.cfi_restore_state
	movl	$1013, %r9d
	movl	$110, %ecx
	movl	$568, %edx
	movl	$47, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1632:
	.size	tls_parse_ctos_early_data, .-tls_parse_ctos_early_data
	.p2align 4
	.globl	tls_parse_ctos_psk
	.type	tls_parse_ctos_psk, @function
tls_parse_ctos_psk:
.LFB1634:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$360, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -336(%rbp)
	testb	$3, 1808(%rdi)
	je	.L444
	movq	8(%rsi), %rax
	movq	%rdi, %r12
	cmpq	$1, %rax
	jbe	.L442
	movq	(%rsi), %r14
	subq	$2, %rax
	movzwl	(%r14), %ebx
	rolw	$8, %bx
	movzwl	%bx, %ebx
	cmpq	%rbx, %rax
	jb	.L442
	addq	$2, %r14
	subq	%rbx, %rax
	leaq	(%r14,%rbx), %rdx
	movq	%rax, 8(%rsi)
	movq	%rdx, (%rsi)
	movl	$0, 1664(%rdi)
	testq	%rbx, %rbx
	je	.L444
	leaq	-336(%rbp), %rax
	xorl	%r15d, %r15d
	movq	%r14, %r13
	movl	$0, -344(%rbp)
	movl	$0, -340(%rbp)
	movq	%rax, -352(%rbp)
	movq	%rsi, -360(%rbp)
.L443:
	cmpq	$1, %rbx
	je	.L445
	movzwl	0(%r13), %r8d
	subq	$2, %rbx
	rolw	$8, %r8w
	movzwl	%r8w, %r14d
	cmpq	%r14, %rbx
	jb	.L445
	subq	%r14, %rbx
	cmpq	$3, %rbx
	jbe	.L445
	leaq	2(%r13), %r11
	leaq	(%r11,%r14), %r13
	movl	0(%r13), %eax
	addq	$4, %r13
	movl	%eax, -380(%rbp)
	movq	1424(%r12), %rax
	testq	%rax, %rax
	je	.L447
	movl	%r8d, -376(%rbp)
	movq	%r11, %rsi
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%r11, -368(%rbp)
	movq	-352(%rbp), %rcx
	call	*%rax
	movq	-368(%rbp), %r11
	movl	-376(%rbp), %r8d
	testl	%eax, %eax
	je	.L524
.L447:
	movq	-336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L525
.L449:
	xorl	%esi, %esi
	call	ssl_session_dup@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L526
	movq	-336(%rbp), %rdi
	call	SSL_SESSION_free@PLT
	movq	1256(%r12), %rdx
	leaq	384(%r14), %rdi
	leaq	1264(%r12), %rsi
	movq	%r14, -336(%rbp)
	call	memcpy@PLT
	movq	1256(%r12), %rax
	movl	-340(%rbp), %edi
	movq	%rax, 376(%r14)
	testl	%edi, %edi
	je	.L527
.L460:
	movl	$1, 1664(%r12)
	movl	$1, -344(%rbp)
.L461:
	movq	504(%r14), %rax
	movl	64(%rax), %edi
	call	ssl_md@PLT
	movq	%rax, %r15
	movq	168(%r12), %rax
	movq	568(%rax), %rax
	movl	64(%rax), %edi
	call	ssl_md@PLT
	cmpq	%rax, %r15
	jne	.L528
.L470:
	cmpq	$0, -336(%rbp)
	movq	-360(%rbp), %r13
	je	.L444
	movq	136(%r12), %rax
	movq	%r15, %rdi
	movq	0(%r13), %rbx
	movq	8(%rax), %r14
	call	EVP_MD_size@PLT
	movq	8(%r13), %rdx
	cmpq	$1, %rdx
	jbe	.L471
	movq	0(%r13), %rsi
	leaq	-2(%rdx), %rcx
	movzwl	(%rsi), %edx
	rolw	$8, %dx
	movzwl	%dx, %edx
	cmpq	%rdx, %rcx
	jb	.L471
	subq	%rdx, %rcx
	addq	$2, %rsi
	movq	%rcx, 8(%r13)
	leaq	(%rsi,%rdx), %rdi
	movq	%rdi, 0(%r13)
	xorl	%edi, %edi
.L472:
	testq	%rdx, %rdx
	je	.L473
	movzbl	(%rsi), %ecx
	subq	$1, %rdx
	cmpq	%rcx, %rdx
	jb	.L473
	leaq	1(%rsi), %r8
	subq	%rcx, %rdx
	addl	$1, %edi
	leaq	(%r8,%rcx), %rsi
	cmpl	%edi, -340(%rbp)
	jnb	.L472
	cltq
	movl	$1271, %r9d
	cmpq	%rcx, %rax
	jne	.L522
	movq	%rbx, %rcx
	xorl	%r9d, %r9d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	136(%r12), %rax
	subq	%r14, %rcx
	movq	8(%rax), %rdx
	pushq	%rax
	movl	-344(%rbp), %eax
	pushq	%rax
	pushq	$0
	pushq	-336(%rbp)
	call	tls_psk_do_binder@PLT
	addq	$32, %rsp
	cmpl	$1, %eax
	je	.L529
	.p2align 4,,10
	.p2align 3
.L457:
	movq	-336(%rbp), %rdi
	call	SSL_SESSION_free@PLT
	xorl	%eax, %eax
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L442:
	movl	$1067, %r9d
	leaq	.LC0(%rip), %r8
	movl	$110, %ecx
	movq	%r12, %rdi
	movl	$505, %edx
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L439:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L530
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L445:
	.cfi_restore_state
	movl	$1080, %r9d
.L521:
	leaq	.LC0(%rip), %r8
	movl	$110, %ecx
	movl	$505, %edx
	movq	%r12, %rdi
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L444:
	movl	$1, %eax
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L527:
	movl	$1, 1820(%r12)
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L473:
	movl	$1264, %r9d
.L522:
	leaq	.LC0(%rip), %r8
	movl	$110, %ecx
	movl	$505, %edx
	movq	%r12, %rdi
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L525:
	cmpq	$0, 1416(%r12)
	je	.L450
	cmpq	$128, %r14
	jbe	.L531
.L450:
	movl	1492(%r12), %eax
	testb	$64, %ah
	jne	.L462
	movl	6176(%r12), %esi
	testl	%esi, %esi
	je	.L463
	testl	$16777216, %eax
	je	.L462
.L463:
	movq	-352(%rbp), %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r11, %rsi
	movq	%r12, %rdi
	call	tls_decrypt_ticket@PLT
	cmpl	$3, %eax
	je	.L476
	cmpl	$1, %eax
	jbe	.L532
	subl	$2, %eax
	andl	$-3, %eax
	je	.L464
.L465:
	movl	6176(%r12), %ecx
	testl	%ecx, %ecx
	je	.L468
	testb	$1, 1495(%r12)
	je	.L533
.L468:
	xorl	%edi, %edi
	call	time@PLT
	movl	-340(%rbp), %edx
	movq	-336(%rbp), %r14
	testl	%edx, %edx
	jne	.L461
	subl	488(%r14), %eax
	movl	%eax, %edx
	cmpq	%rdx, 480(%r14)
	jl	.L461
	imull	$1000, %eax, %edx
	movq	%rdx, %rcx
	imulq	$274877907, %rdx, %rdx
	shrq	$38, %rdx
	cmpl	%eax, %edx
	jne	.L461
	movl	-380(%rbp), %eax
	addl	$1000, %ecx
	bswap	%eax
	subl	576(%r14), %eax
	cmpl	%eax, %ecx
	jb	.L461
	addl	$10000, %eax
	cmpl	%eax, %ecx
	ja	.L461
	movl	$1, 1820(%r12)
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L533:
	movq	1904(%r12), %rdi
	movq	-336(%rbp), %rsi
	call	SSL_CTX_remove_session@PLT
	testl	%eax, %eax
	jne	.L468
	movq	-336(%rbp), %rdi
	call	SSL_SESSION_free@PLT
	movq	$0, -336(%rbp)
	.p2align 4,,10
	.p2align 3
.L464:
	addl	$1, -340(%rbp)
	subq	$4, %rbx
	jne	.L443
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L528:
	movq	-336(%rbp), %rdi
	call	SSL_SESSION_free@PLT
	movq	$0, -336(%rbp)
	movl	$0, 1820(%r12)
	movl	$0, 1664(%r12)
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L462:
	movl	$1, 1664(%r12)
	testw	%r8w, %r8w
	je	.L476
	cmpw	$32, %r8w
	jne	.L464
	movl	$32, %edx
	movq	%r11, %rsi
	movq	%r12, %rdi
	call	lookup_sess_in_cache@PLT
	testq	%rax, %rax
	je	.L464
	movq	%rax, -336(%rbp)
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L471:
	movl	$1257, %r9d
	jmp	.L522
.L531:
	movl	$449, %edx
	leaq	.LC1(%rip), %rsi
	movl	%r8d, -376(%rbp)
	movq	%r11, -368(%rbp)
	call	CRYPTO_free@PLT
	movq	-368(%rbp), %r11
	movl	$452, %ecx
	movq	%r14, %rsi
	leaq	.LC1(%rip), %rdx
	movq	%r11, %rdi
	call	CRYPTO_strndup@PLT
	movq	-368(%rbp), %r11
	movl	-376(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, %r9
	je	.L534
	leaq	-320(%rbp), %rax
	movl	$256, %ecx
	movq	%r9, %rsi
	movq	%r12, %rdi
	movl	%r8d, -384(%rbp)
	movq	%rax, %rdx
	movq	%r11, -392(%rbp)
	movq	%r9, -376(%rbp)
	movq	%rax, -400(%rbp)
	call	*1416(%r12)
	movq	-376(%rbp), %r9
	movl	$1109, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, -368(%rbp)
	movq	%r9, %rdi
	call	CRYPTO_free@PLT
	movl	-368(%rbp), %ecx
	movq	-392(%rbp), %r11
	movl	-384(%rbp), %r8d
	cmpl	$256, %ecx
	ja	.L535
	testl	%ecx, %ecx
	jne	.L536
.L454:
	movq	-336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L450
	jmp	.L449
	.p2align 4,,10
	.p2align 3
.L536:
	leaq	-322(%rbp), %rsi
	movq	%r12, %rdi
	movl	%r8d, -384(%rbp)
	movl	$275, %r8d
	movq	%r11, -392(%rbp)
	movl	%ecx, -368(%rbp)
	movw	%r8w, -322(%rbp)
	call	SSL_CIPHER_find@PLT
	movl	-368(%rbp), %ecx
	movq	-392(%rbp), %r11
	testq	%rax, %rax
	movq	%rax, -376(%rbp)
	movl	-384(%rbp), %r8d
	movq	%rcx, -368(%rbp)
	je	.L537
	movl	%r8d, -384(%rbp)
	movq	%r11, -392(%rbp)
	call	SSL_SESSION_new@PLT
	movq	-392(%rbp), %r11
	movl	-384(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, -336(%rbp)
	movq	%rax, %rdi
	je	.L458
	movq	-368(%rbp), %rdx
	movq	-400(%rbp), %rsi
	movl	%r8d, -384(%rbp)
	movq	%r11, -392(%rbp)
	call	SSL_SESSION_set1_master_key@PLT
	testl	%eax, %eax
	je	.L458
	movq	-376(%rbp), %rsi
	movq	-336(%rbp), %rdi
	call	SSL_SESSION_set_cipher@PLT
	testl	%eax, %eax
	je	.L458
	movq	-336(%rbp), %rdi
	movl	$772, %esi
	call	SSL_SESSION_set_protocol_version@PLT
	testl	%eax, %eax
	je	.L458
	movq	-368(%rbp), %rsi
	movq	-400(%rbp), %rdi
	call	OPENSSL_cleanse@PLT
	movl	-384(%rbp), %r8d
	movq	-392(%rbp), %r11
	jmp	.L454
.L524:
	movl	$1089, %r9d
	leaq	.LC0(%rip), %r8
	movl	$110, %ecx
	movq	%r12, %rdi
	movl	$505, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L439
.L526:
	movl	$1152, %r9d
.L520:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	$505, %edx
	movq	%r12, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L439
.L532:
	movl	$1195, %r9d
	jmp	.L520
.L476:
	movl	$1188, %r9d
	jmp	.L521
.L530:
	call	__stack_chk_fail@PLT
.L529:
	movl	%eax, -352(%rbp)
	movl	-340(%rbp), %eax
	movq	1296(%r12), %rdi
	movl	%eax, 1848(%r12)
	call	SSL_SESSION_free@PLT
	movq	-336(%rbp), %rdx
	movl	-352(%rbp), %eax
	movq	%rdx, 1296(%r12)
	jmp	.L439
.L458:
	movq	-368(%rbp), %rsi
	movq	-400(%rbp), %rdi
	call	OPENSSL_cleanse@PLT
	movl	$1138, %r9d
	movl	$68, %ecx
	movq	%r12, %rdi
	leaq	.LC0(%rip), %r8
	movl	$505, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L457
.L535:
	movl	$1111, %r9d
	jmp	.L520
.L534:
	movl	$1103, %r9d
	jmp	.L520
.L537:
	movq	-400(%rbp), %rdi
	movq	%rcx, %rsi
	call	OPENSSL_cleanse@PLT
	movl	$1125, %r9d
	jmp	.L520
	.cfi_endproc
.LFE1634:
	.size	tls_parse_ctos_psk, .-tls_parse_ctos_psk
	.p2align 4
	.globl	tls_parse_ctos_post_handshake_auth
	.type	tls_parse_ctos_post_handshake_auth, @function
tls_parse_ctos_post_handshake_auth:
.LFB1635:
	.cfi_startproc
	endbr64
	cmpq	$0, 8(%rsi)
	jne	.L545
	movl	$2, 1936(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L545:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$278, %ecx
	movl	$620, %edx
	movl	$50, %esi
	movl	$1296, %r9d
	leaq	.LC0(%rip), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1635:
	.size	tls_parse_ctos_post_handshake_auth, .-tls_parse_ctos_post_handshake_auth
	.p2align 4
	.globl	tls_construct_stoc_renegotiate
	.type	tls_construct_stoc_renegotiate, @function
tls_construct_stoc_renegotiate:
.LFB1636:
	.cfi_startproc
	endbr64
	movq	168(%rdi), %rdx
	movl	$2, %eax
	movl	984(%rdx), %edx
	testl	%edx, %edx
	je	.L570
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movl	$65281, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L549
	movl	$2, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L549
	movl	$1, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	jne	.L573
	.p2align 4,,10
	.p2align 3
.L549:
	movl	$1326, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$458, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L546:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L570:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L573:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	168(%r12), %rsi
	movq	%r13, %rdi
	movq	904(%rsi), %rdx
	addq	$840, %rsi
	call	WPACKET_memcpy@PLT
	testl	%eax, %eax
	je	.L549
	movq	168(%r12), %rsi
	movq	%r13, %rdi
	movq	976(%rsi), %rdx
	addq	$912, %rsi
	call	WPACKET_memcpy@PLT
	testl	%eax, %eax
	je	.L549
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L549
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L549
	movl	$1, %eax
	jmp	.L546
	.cfi_endproc
.LFE1636:
	.size	tls_construct_stoc_renegotiate, .-tls_construct_stoc_renegotiate
	.p2align 4
	.globl	tls_construct_stoc_server_name
	.type	tls_construct_stoc_server_name, @function
tls_construct_stoc_server_name:
.LFB1637:
	.cfi_startproc
	endbr64
	cmpl	$1, 1864(%rdi)
	movl	$2, %eax
	jne	.L586
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	200(%rdi), %edx
	movq	%rdi, %r12
	testl	%edx, %edx
	je	.L576
	movq	8(%rdi), %rdx
	movq	192(%rdx), %rcx
	testb	$8, 96(%rcx)
	jne	.L574
	movl	(%rdx), %eax
	cmpl	$771, %eax
	jle	.L581
	cmpl	$65536, %eax
	je	.L581
.L576:
	xorl	%esi, %esi
	movl	$2, %edx
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L578
	xorl	%esi, %esi
	movl	$2, %edx
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L578
	popq	%r12
	movl	$1, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L578:
	.cfi_restore_state
	movl	$1350, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$459, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L574:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L586:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L581:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	popq	%r12
	movl	$2, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1637:
	.size	tls_construct_stoc_server_name, .-tls_construct_stoc_server_name
	.p2align 4
	.globl	tls_construct_stoc_maxfragmentlen
	.type	tls_construct_stoc_maxfragmentlen, @function
tls_construct_stoc_maxfragmentlen:
.LFB1638:
	.cfi_startproc
	endbr64
	movq	1296(%rdi), %rax
	movl	$2, %r8d
	movzbl	600(%rax), %eax
	subl	$1, %eax
	cmpb	$3, %al
	ja	.L604
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movl	$1, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L592
	movl	$2, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L592
	movq	1296(%r12), %rax
	movl	$1, %edx
	movq	%r13, %rdi
	movzbl	600(%rax), %esi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	jne	.L607
	.p2align 4,,10
	.p2align 3
.L592:
	leaq	.LC0(%rip), %r8
	movl	$1374, %r9d
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$548, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%r8d, %r8d
.L589:
	popq	%r12
	movl	%r8d, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L604:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L607:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L592
	movl	$1, %r8d
	jmp	.L589
	.cfi_endproc
.LFE1638:
	.size	tls_construct_stoc_maxfragmentlen, .-tls_construct_stoc_maxfragmentlen
	.p2align 4
	.globl	tls_construct_stoc_ec_pt_formats
	.type	tls_construct_stoc_ec_pt_formats, @function
tls_construct_stoc_ec_pt_formats:
.LFB1639:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$32, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movq	568(%rax), %rax
	movl	28(%rax), %edx
	movl	32(%rax), %eax
	andl	$4, %edx
	andl	$8, %eax
	orl	%eax, %edx
	je	.L608
	cmpq	$0, 1696(%rdi)
	movq	%rdi, %r12
	je	.L608
	movq	%rsi, %r13
	leaq	-32(%rbp), %rdx
	leaq	-40(%rbp), %rsi
	call	tls1_get_formatlist@PLT
	movl	$2, %edx
	movl	$11, %esi
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L611
	movl	$2, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L611
	movq	-32(%rbp), %rdx
	movq	-40(%rbp), %rsi
	movl	$1, %ecx
	movq	%r13, %rdi
	call	WPACKET_sub_memcpy__@PLT
	testl	%eax, %eax
	jne	.L626
	.p2align 4,,10
	.p2align 3
.L611:
	leaq	.LC0(%rip), %r8
	movl	$1402, %r9d
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$453, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%r8d, %r8d
.L608:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L627
	addq	$32, %rsp
	movl	%r8d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L626:
	.cfi_restore_state
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L611
	movl	$1, %r8d
	jmp	.L608
.L627:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1639:
	.size	tls_construct_stoc_ec_pt_formats, .-tls_construct_stoc_ec_pt_formats
	.p2align 4
	.globl	tls_construct_stoc_supported_groups
	.type	tls_construct_stoc_supported_groups, @function
tls_construct_stoc_supported_groups:
.LFB1640:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	cmpw	$0, 1030(%rax)
	je	.L634
	movq	%rsi, %r14
	leaq	-64(%rbp), %rdx
	leaq	-72(%rbp), %rsi
	movq	%rdi, %r13
	call	tls1_get_supported_groups@PLT
	xorl	%ebx, %ebx
	cmpq	$0, -64(%rbp)
	movq	$1, -88(%rbp)
	movl	$1426, %r9d
	je	.L660
	.p2align 4,,10
	.p2align 3
.L631:
	movq	-72(%rbp), %rax
	movl	$131076, %edx
	movq	%r13, %rdi
	movzwl	(%rax,%rbx,2), %r12d
	movl	%r12d, %esi
	call	tls_curve_allowed@PLT
	testl	%eax, %eax
	je	.L632
	cmpq	$0, -88(%rbp)
	je	.L633
	movq	168(%r13), %rax
	cmpw	%r12w, 1030(%rax)
	je	.L634
	movl	$2, %edx
	movl	$10, %esi
	movq	%r14, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L636
	movl	$2, %esi
	movq	%r14, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L636
	movl	$2, %esi
	movq	%r14, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L636
.L633:
	movl	$2, %edx
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L661
	movq	$0, -88(%rbp)
.L632:
	addq	$1, %rbx
	cmpq	%rbx, -64(%rbp)
	ja	.L631
	movq	%r14, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L638
	movq	%r14, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L638
	movl	$1, %eax
	jmp	.L628
	.p2align 4,,10
	.p2align 3
.L638:
	movl	$1467, %r9d
.L660:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	$544, %edx
	movq	%r13, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L628:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L662
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L634:
	.cfi_restore_state
	movl	$2, %eax
	jmp	.L628
	.p2align 4,,10
	.p2align 3
.L636:
	movl	$1449, %r9d
	jmp	.L660
.L661:
	movl	$1458, %r9d
	jmp	.L660
.L662:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1640:
	.size	tls_construct_stoc_supported_groups, .-tls_construct_stoc_supported_groups
	.p2align 4
	.globl	tls_construct_stoc_session_ticket
	.type	tls_construct_stoc_session_ticket, @function
tls_construct_stoc_session_ticket:
.LFB1641:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	1664(%rdi), %eax
	movq	%rdi, %r12
	testl	%eax, %eax
	je	.L666
	movq	%rsi, %r13
	call	tls_use_ticket@PLT
	testl	%eax, %eax
	je	.L666
	movl	$2, %edx
	movl	$35, %esi
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L668
	xorl	%esi, %esi
	movl	$2, %edx
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L668
	popq	%r12
	movl	$1, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L666:
	.cfi_restore_state
	movl	$0, 1664(%r12)
	movl	$2, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L668:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$1488, %r9d
	movl	$68, %ecx
	movl	$460, %edx
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1641:
	.size	tls_construct_stoc_session_ticket, .-tls_construct_stoc_session_ticket
	.p2align 4
	.globl	tls_construct_stoc_status_request
	.type	tls_construct_stoc_status_request, @function
tls_construct_stoc_status_request:
.LFB1642:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	cmpl	$16384, %edx
	je	.L685
	movl	1628(%rdi), %eax
	movq	%rdi, %r12
	movl	$2, %r14d
	testl	%eax, %eax
	je	.L676
	movq	8(%rdi), %rax
	movq	%rsi, %r13
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L678
	movl	(%rax), %eax
	cmpl	$771, %eax
	setg	%dl
	cmpl	$65536, %eax
	setne	%al
	testb	%al, %dl
	je	.L678
	testq	%r8, %r8
	jne	.L676
.L678:
	movl	$2, %edx
	movl	$5, %esi
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L681
	movl	$2, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L681
	movq	8(%r12), %rax
	movq	192(%rax), %rdx
	movl	96(%rdx), %r14d
	andl	$8, %r14d
	jne	.L684
	movl	(%rax), %eax
	cmpl	$65536, %eax
	je	.L684
	cmpl	$771, %eax
	jg	.L705
.L684:
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L683
	movl	$1, %r14d
.L676:
	addq	$8, %rsp
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L681:
	.cfi_restore_state
	movl	$1513, %r9d
.L704:
	movq	%r12, %rdi
	movl	$68, %ecx
	movl	$461, %edx
	xorl	%r14d, %r14d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	addq	$8, %rsp
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L685:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$2, %r14d
	popq	%r12
	movl	%r14d, %eax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L683:
	.cfi_restore_state
	movl	$1528, %r9d
	jmp	.L704
	.p2align 4,,10
	.p2align 3
.L705:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	tls_construct_cert_status_body@PLT
	testl	%eax, %eax
	jne	.L684
	jmp	.L676
	.cfi_endproc
.LFE1642:
	.size	tls_construct_stoc_status_request, .-tls_construct_stoc_status_request
	.p2align 4
	.globl	tls_construct_stoc_next_proto_neg
	.type	tls_construct_stoc_next_proto_neg, @function
tls_construct_stoc_next_proto_neg:
.LFB1643:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	$2, %r14d
	pushq	%r13
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movl	988(%rax), %edx
	movl	$0, 988(%rax)
	testl	%edx, %edx
	je	.L706
	movq	1440(%rdi), %rdx
	movq	%rdi, %r12
	movq	648(%rdx), %rax
	testq	%rax, %rax
	je	.L706
	movq	656(%rdx), %rcx
	movq	%rsi, %r13
	leaq	-52(%rbp), %rdx
	leaq	-48(%rbp), %rsi
	call	*%rax
	movl	$1, %r14d
	testl	%eax, %eax
	je	.L719
.L706:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L720
	addq	$40, %rsp
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L719:
	.cfi_restore_state
	movl	$2, %edx
	movl	$13172, %esi
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L709
	movl	-52(%rbp), %edx
	movq	-48(%rbp), %rsi
	movl	$2, %ecx
	movq	%r13, %rdi
	call	WPACKET_sub_memcpy__@PLT
	testl	%eax, %eax
	je	.L709
	movq	168(%r12), %rax
	movl	$1, 988(%rax)
	jmp	.L706
	.p2align 4,,10
	.p2align 3
.L709:
	movl	$68, %ecx
	movl	$457, %edx
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	movl	$1556, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L706
.L720:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1643:
	.size	tls_construct_stoc_next_proto_neg, .-tls_construct_stoc_next_proto_neg
	.p2align 4
	.globl	tls_construct_stoc_alpn
	.type	tls_construct_stoc_alpn, @function
tls_construct_stoc_alpn:
.LFB1644:
	.cfi_startproc
	endbr64
	movq	168(%rdi), %rax
	cmpq	$0, 992(%rax)
	je	.L725
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movl	$16, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L724
	movl	$2, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	jne	.L745
.L724:
	movl	$1582, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$451, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L721:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L745:
	.cfi_restore_state
	movl	$2, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L724
	movq	168(%r12), %rax
	movl	$1, %ecx
	movq	%r13, %rdi
	movq	1000(%rax), %rdx
	movq	992(%rax), %rsi
	call	WPACKET_sub_memcpy__@PLT
	testl	%eax, %eax
	je	.L724
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L724
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L724
	movl	$1, %eax
	jmp	.L721
	.p2align 4,,10
	.p2align 3
.L725:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$2, %eax
	ret
	.cfi_endproc
.LFE1644:
	.size	tls_construct_stoc_alpn, .-tls_construct_stoc_alpn
	.p2align 4
	.globl	tls_construct_stoc_use_srtp
	.type	tls_construct_stoc_use_srtp, @function
tls_construct_stoc_use_srtp:
.LFB1645:
	.cfi_startproc
	endbr64
	cmpq	$0, 1920(%rdi)
	je	.L750
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movl	$14, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L749
	movl	$2, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	jne	.L770
.L749:
	movl	$1604, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$462, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L746:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L770:
	.cfi_restore_state
	movl	$2, %edx
	movl	$2, %esi
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L749
	movq	1920(%r12), %rax
	movl	$2, %edx
	movq	%r13, %rdi
	movq	8(%rax), %rsi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L749
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L749
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L749
	movl	$1, %eax
	jmp	.L746
	.p2align 4,,10
	.p2align 3
.L750:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$2, %eax
	ret
	.cfi_endproc
.LFE1645:
	.size	tls_construct_stoc_use_srtp, .-tls_construct_stoc_use_srtp
	.p2align 4
	.globl	tls_construct_stoc_etm
	.type	tls_construct_stoc_etm, @function
tls_construct_stoc_etm:
.LFB1646:
	.cfi_startproc
	endbr64
	movl	1812(%rdi), %edx
	movl	$2, %eax
	testl	%edx, %edx
	je	.L783
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	168(%rdi), %rax
	movq	%rdi, %r12
	movq	568(%rax), %rax
	cmpl	$64, 40(%rax)
	je	.L773
	movl	36(%rax), %eax
	cmpl	$4, %eax
	sete	%cl
	cmpl	$1024, %eax
	sete	%dl
	orb	%dl, %cl
	jne	.L773
	cmpl	$262144, %eax
	je	.L773
	movq	%rsi, %r13
	movl	$2, %edx
	movl	$22, %esi
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L777
	xorl	%esi, %esi
	movl	$2, %edx
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L777
	popq	%r12
	movl	$1, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L773:
	.cfi_restore_state
	movl	$0, 1812(%r12)
	movl	$2, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L777:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$1633, %r9d
	movl	$68, %ecx
	movl	$455, %edx
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L783:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE1646:
	.size	tls_construct_stoc_etm, .-tls_construct_stoc_etm
	.p2align 4
	.globl	tls_construct_stoc_ems
	.type	tls_construct_stoc_ems, @function
tls_construct_stoc_ems:
.LFB1647:
	.cfi_startproc
	endbr64
	movq	168(%rdi), %rax
	movq	(%rax), %rdx
	movl	$2, %eax
	andb	$2, %dh
	je	.L796
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movl	$23, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L789
	xorl	%esi, %esi
	movl	$2, %edx
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L789
	popq	%r12
	movl	$1, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L789:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$1649, %r9d
	movl	$68, %ecx
	movl	$454, %edx
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L796:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE1647:
	.size	tls_construct_stoc_ems, .-tls_construct_stoc_ems
	.p2align 4
	.globl	tls_construct_stoc_supported_versions
	.type	tls_construct_stoc_supported_versions, @function
tls_construct_stoc_supported_versions:
.LFB1648:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	8(%rdi), %rdx
	movq	192(%rdx), %rax
	movl	96(%rax), %eax
	andl	$8, %eax
	jne	.L800
	movl	%eax, %r13d
	movl	(%rdx), %eax
	cmpl	$771, %eax
	jle	.L800
	cmpl	$65536, %eax
	je	.L800
	movq	%rsi, %r12
	movl	$2, %edx
	movl	$43, %esi
	movq	%r12, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L803
	movl	$2, %esi
	movq	%r12, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	jne	.L820
.L803:
	movq	%r14, %rdi
	movl	$1672, %r9d
	movl	$68, %ecx
	movl	$611, %edx
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L800:
	.cfi_restore_state
	movl	$68, %ecx
	movl	$611, %edx
	movq	%r14, %rdi
	xorl	%r13d, %r13d
	movl	$1662, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
.L799:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L820:
	.cfi_restore_state
	movl	(%r14), %esi
	movl	$2, %edx
	movq	%r12, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L803
	movq	%r12, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L803
	movl	$1, %r13d
	jmp	.L799
	.cfi_endproc
.LFE1648:
	.size	tls_construct_stoc_supported_versions, .-tls_construct_stoc_supported_versions
	.p2align 4
	.globl	tls_construct_stoc_key_share
	.type	tls_construct_stoc_key_share, @function
tls_construct_stoc_key_share:
.LFB1649:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$16, %rsp
	movl	1248(%rdi), %r14d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movq	1032(%rax), %r15
	cmpl	$1, %r14d
	je	.L861
	testq	%r15, %r15
	je	.L862
	movl	$2, %edx
	movl	$51, %esi
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L832
	movl	$2, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	jne	.L863
.L832:
	movl	$1721, %r9d
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L862:
	movl	200(%rdi), %eax
	testl	%eax, %eax
	je	.L829
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	tls13_generate_handshake_secret@PLT
	testl	%eax, %eax
	jne	.L830
.L829:
	movl	$1711, %r9d
.L860:
	movl	$68, %ecx
	movl	$456, %edx
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
.L821:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L864
	addq	$16, %rsp
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L861:
	.cfi_restore_state
	testq	%r15, %r15
	je	.L823
.L830:
	movl	$2, %r14d
	jmp	.L821
	.p2align 4,,10
	.p2align 3
.L823:
	movl	$2, %edx
	movl	$51, %esi
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L826
	movl	$2, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	jne	.L865
.L826:
	movl	$1699, %r9d
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L863:
	movq	168(%r12), %rax
	movl	$2, %edx
	movq	%r13, %rdi
	movzwl	1030(%rax), %esi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L832
	movq	%r15, %rdi
	call	ssl_generate_pkey@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L866
	leaq	-48(%rbp), %rsi
	movq	%rax, %rdi
	call	EVP_PKEY_get1_tls_encodedpoint@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L867
	movq	-48(%rbp), %rsi
	movl	$2, %ecx
	movq	%r13, %rdi
	call	WPACKET_sub_memcpy__@PLT
	testl	%eax, %eax
	je	.L836
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L836
	movq	-48(%rbp), %rdi
	movl	$1750, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r14, %rsi
	movl	$1, %ecx
	movq	%r15, %rdx
	movq	168(%r12), %rax
	movq	%r12, %rdi
	movq	%r14, 576(%rax)
	xorl	%r14d, %r14d
	call	ssl_derive@PLT
	testl	%eax, %eax
	setne	%r14b
	jmp	.L821
	.p2align 4,,10
	.p2align 3
.L865:
	movq	168(%r12), %rax
	movl	$2, %edx
	movq	%r13, %rdi
	movzwl	1030(%rax), %esi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L826
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L826
	jmp	.L821
	.p2align 4,,10
	.p2align 3
.L866:
	movl	$1728, %r9d
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
	movq	%r12, %rdi
	movl	$456, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L821
	.p2align 4,,10
	.p2align 3
.L836:
	movl	$1744, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$456, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	EVP_PKEY_free@PLT
	movq	-48(%rbp), %rdi
	movl	$1747, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L821
	.p2align 4,,10
	.p2align 3
.L867:
	movl	$1736, %r9d
	leaq	.LC0(%rip), %r8
	movl	$16, %ecx
	movq	%r12, %rdi
	movl	$456, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	EVP_PKEY_free@PLT
	jmp	.L821
.L864:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1649:
	.size	tls_construct_stoc_key_share, .-tls_construct_stoc_key_share
	.p2align 4
	.globl	tls_construct_stoc_cookie
	.type	tls_construct_stoc_cookie, @function
tls_construct_stoc_cookie:
.LFB1650:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$112, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movq	(%rax), %rdx
	movl	$2, %eax
	andb	$8, %dh
	je	.L868
	movq	1440(%rdi), %rax
	movq	%rdi, %r12
	movl	$1779, %r9d
	leaq	.LC0(%rip), %r8
	movl	$287, %ecx
	cmpq	$0, 216(%rax)
	je	.L959
	movq	%rsi, %r13
	movl	$2, %edx
	movl	$44, %esi
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L872
	movl	$2, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L872
	movl	$2, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	jne	.L962
	.p2align 4,,10
	.p2align 3
.L872:
	movl	$1799, %r9d
.L960:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
.L959:
	movl	$613, %edx
	movl	$80, %esi
	movq	%r12, %rdi
	call	ossl_statem_fatal@PLT
.L961:
	xorl	%eax, %eax
.L868:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L963
	addq	$112, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L962:
	.cfi_restore_state
	leaq	-88(%rbp), %rsi
	movq	%r13, %rdi
	call	WPACKET_get_total_written@PLT
	testl	%eax, %eax
	je	.L872
	leaq	-112(%rbp), %rdx
	movl	$4210, %esi
	movq	%r13, %rdi
	call	WPACKET_reserve_bytes@PLT
	testl	%eax, %eax
	je	.L872
	xorl	%esi, %esi
	movl	$2, %edx
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L872
	movl	$2, %edx
	movl	$772, %esi
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L872
	movq	168(%r12), %rax
	movl	$2, %edx
	movq	%r13, %rdi
	movzwl	1030(%rax), %esi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L872
	movq	168(%r12), %rax
	leaq	-80(%rbp), %rdx
	movq	%r13, %rsi
	movq	568(%rax), %rdi
	movq	8(%r12), %rax
	call	*152(%rax)
	testl	%eax, %eax
	je	.L872
	movq	168(%r12), %rax
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%r13, %rdi
	cmpq	$0, 1032(%rax)
	sete	%sil
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L872
	xorl	%edi, %edi
	call	time@PLT
	movl	$4, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L872
	movl	$2, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L872
	leaq	-144(%rbp), %rdx
	movl	$64, %esi
	movq	%r13, %rdi
	call	WPACKET_reserve_bytes@PLT
	testl	%eax, %eax
	je	.L872
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	ssl3_digest_cached_records@PLT
	testl	%eax, %eax
	je	.L961
	movq	-144(%rbp), %rsi
	leaq	-64(%rbp), %rcx
	movl	$64, %edx
	movq	%r12, %rdi
	call	ssl_handshake_hash@PLT
	testl	%eax, %eax
	je	.L961
	movq	-64(%rbp), %rsi
	leaq	-136(%rbp), %rdx
	movq	%r13, %rdi
	call	WPACKET_allocate_bytes@PLT
	testl	%eax, %eax
	je	.L876
	movq	-136(%rbp), %rax
	cmpq	%rax, -144(%rbp)
	je	.L964
.L876:
	movl	$1820, %r9d
	jmp	.L960
.L963:
	call	__stack_chk_fail@PLT
.L964:
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L876
	movl	$1, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L876
	leaq	-128(%rbp), %rdx
	movl	$4096, %esi
	movq	%r13, %rdi
	call	WPACKET_reserve_bytes@PLT
	testl	%eax, %eax
	je	.L876
	movq	1440(%r12), %rax
	movq	-128(%rbp), %rsi
	leaq	-48(%rbp), %rdx
	movq	%r12, %rdi
	call	*216(%rax)
	testl	%eax, %eax
	je	.L965
	movq	-48(%rbp), %rsi
	leaq	-120(%rbp), %rdx
	movq	%r13, %rdi
	call	WPACKET_allocate_bytes@PLT
	testl	%eax, %eax
	je	.L879
	movq	-120(%rbp), %rax
	cmpq	%rax, -128(%rbp)
	je	.L966
.L879:
	movl	$1837, %r9d
	jmp	.L960
.L965:
	movl	$1827, %r9d
	leaq	.LC0(%rip), %r8
	movl	$400, %ecx
	jmp	.L959
.L966:
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L879
	leaq	-72(%rbp), %rsi
	movq	%r13, %rdi
	call	WPACKET_get_total_written@PLT
	testl	%eax, %eax
	je	.L879
	leaq	-104(%rbp), %rdx
	movl	$32, %esi
	movq	%r13, %rdi
	call	WPACKET_reserve_bytes@PLT
	testl	%eax, %eax
	je	.L879
	movq	-72(%rbp), %rax
	subq	-88(%rbp), %rax
	movq	$32, -56(%rbp)
	movl	$1845, %r9d
	movq	%rax, -72(%rbp)
	cmpq	$4178, %rax
	ja	.L960
	call	EVP_MD_CTX_new@PLT
	movl	$32, %ecx
	xorl	%esi, %esi
	movl	$855, %edi
	movq	%rax, %r14
	movq	1904(%r12), %rax
	leaq	680(%rax), %rdx
	call	EVP_PKEY_new_raw_private_key@PLT
	movq	%rax, %r15
	testq	%r14, %r14
	je	.L891
	testq	%rax, %rax
	je	.L891
	call	EVP_sha256@PLT
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%r15, %r8
	movq	%rax, %rdx
	movq	%r14, %rdi
	call	EVP_DigestSignInit@PLT
	testl	%eax, %eax
	jle	.L885
	movq	-72(%rbp), %r8
	movq	-112(%rbp), %rcx
	leaq	-56(%rbp), %rdx
	movq	%r14, %rdi
	movq	-104(%rbp), %rsi
	call	EVP_DigestSign@PLT
	testl	%eax, %eax
	jle	.L885
	movq	-56(%rbp), %rsi
	movq	-72(%rbp), %rax
	movl	$1871, %r9d
	addq	%rsi, %rax
	cmpq	$4210, %rax
	ja	.L958
	leaq	-96(%rbp), %rdx
	movq	%r13, %rdi
	call	WPACKET_allocate_bytes@PLT
	testl	%eax, %eax
	je	.L888
	movq	-104(%rbp), %rax
	cmpq	-96(%rbp), %rax
	je	.L967
.L888:
	movl	$1881, %r9d
.L958:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
.L957:
	movq	%r12, %rdi
	movl	$613, %edx
	movl	$80, %esi
	xorl	%r12d, %r12d
	call	ossl_statem_fatal@PLT
.L883:
	movq	%r14, %rdi
	call	EVP_MD_CTX_free@PLT
	movq	%r15, %rdi
	call	EVP_PKEY_free@PLT
	movl	%r12d, %eax
	jmp	.L868
.L885:
	movl	$1865, %r9d
	jmp	.L958
.L891:
	movl	$1857, %r9d
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
	jmp	.L957
.L967:
	subq	-72(%rbp), %rax
	cmpq	%rax, -112(%rbp)
	jne	.L888
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L888
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L888
	movl	$1, %r12d
	jmp	.L883
	.cfi_endproc
.LFE1650:
	.size	tls_construct_stoc_cookie, .-tls_construct_stoc_cookie
	.p2align 4
	.globl	tls_construct_stoc_cryptopro_bug
	.type	tls_construct_stoc_cryptopro_bug, @function
tls_construct_stoc_cryptopro_bug:
.LFB1651:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$48, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movdqa	.LC2(%rip), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movl	$386007555, -32(%rbp)
	movaps	%xmm0, -64(%rbp)
	movdqa	.LC3(%rip), %xmm0
	movq	568(%rax), %rax
	movaps	%xmm0, -48(%rbp)
	movl	24(%rax), %eax
	andl	$65534, %eax
	cmpl	$128, %eax
	je	.L969
.L971:
	movl	$2, %eax
.L968:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L978
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L969:
	.cfi_restore_state
	movq	%rdi, %r12
	movq	%rsi, %r13
	call	SSL_get_options@PLT
	testl	$2147483648, %eax
	je	.L971
	leaq	-64(%rbp), %rsi
	movl	$36, %edx
	movq	%r13, %rdi
	call	WPACKET_memcpy@PLT
	movl	%eax, %r8d
	movl	$1, %eax
	testl	%r8d, %r8d
	jne	.L968
	movl	$1916, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$452, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L968
.L978:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1651:
	.size	tls_construct_stoc_cryptopro_bug, .-tls_construct_stoc_cryptopro_bug
	.p2align 4
	.globl	tls_construct_stoc_early_data
	.type	tls_construct_stoc_early_data, @function
tls_construct_stoc_early_data:
.LFB1652:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	cmpl	$8192, %edx
	je	.L1006
	cmpl	$2, 1816(%rdi)
	je	.L1007
.L979:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1007:
	.cfi_restore_state
	movl	$2, %edx
	movl	$42, %esi
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L985
	movl	$2, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	jne	.L1008
.L985:
	movl	$1950, %r9d
.L1005:
	movq	%r12, %rdi
	movl	$68, %ecx
	movl	$531, %edx
	movl	$80, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1006:
	.cfi_restore_state
	movl	6176(%rdi), %edx
	testl	%edx, %edx
	je	.L979
	movl	$2, %edx
	movl	$42, %esi
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L983
	movl	$2, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L983
	movl	6176(%r12), %esi
	movl	$4, %edx
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	jne	.L1009
	.p2align 4,,10
	.p2align 3
.L983:
	movl	$1936, %r9d
	jmp	.L1005
	.p2align 4,,10
	.p2align 3
.L1008:
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L985
.L986:
	movl	$1, %eax
	jmp	.L979
	.p2align 4,,10
	.p2align 3
.L1009:
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L983
	jmp	.L986
	.cfi_endproc
.LFE1652:
	.size	tls_construct_stoc_early_data, .-tls_construct_stoc_early_data
	.p2align 4
	.globl	tls_construct_stoc_psk
	.type	tls_construct_stoc_psk, @function
tls_construct_stoc_psk:
.LFB1653:
	.cfi_startproc
	endbr64
	movl	200(%rdi), %edx
	movl	$2, %eax
	testl	%edx, %edx
	je	.L1025
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movl	$41, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L1013
	movl	$2, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L1013
	movl	1848(%r12), %esi
	movl	$2, %edx
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	jne	.L1028
	.p2align 4,,10
	.p2align 3
.L1013:
	movl	$1968, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$504, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L1010:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1025:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L1028:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L1013
	movl	$1, %eax
	jmp	.L1010
	.cfi_endproc
.LFE1653:
	.size	tls_construct_stoc_psk, .-tls_construct_stoc_psk
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.quad	590004743229729021
	.quad	649083510437053958
	.align 16
.LC3:
	.quad	145106028542167088
	.quad	-8851255494666349054
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
