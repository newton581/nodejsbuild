	.file	"t_spki.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"UNKNOWN"
.LC1:
	.string	"Netscape SPKI:\n"
.LC2:
	.string	"  Public Key Algorithm: %s\n"
.LC3:
	.string	"  Unable to load public key\n"
.LC4:
	.string	"  Challenge String: %s\n"
.LC5:
	.string	"  Signature Algorithm: %s"
.LC6:
	.string	"\n      "
.LC7:
	.string	":"
.LC8:
	.string	"%02x%s"
.LC9:
	.string	""
.LC10:
	.string	"\n"
	.text
	.p2align 4
	.globl	NETSCAPE_SPKI_print
	.type	NETSCAPE_SPKI_print, @function
NETSCAPE_SPKI_print:
.LFB779:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	.LC1(%rip), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	(%rbx), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	(%rax), %r8
	call	X509_PUBKEY_get0_param@PLT
	movq	-64(%rbp), %rdi
	call	OBJ_obj2nid@PLT
	leaq	.LC0(%rip), %rdx
	testl	%eax, %eax
	jne	.L19
.L2:
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	(%rbx), %rax
	movq	(%rax), %rdi
	call	X509_PUBKEY_get@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L20
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movl	$4, %edx
	movq	%rax, %rsi
	call	EVP_PKEY_print_public@PLT
	movq	%r13, %rdi
	call	EVP_PKEY_free@PLT
.L4:
	movq	(%rbx), %rax
	movq	8(%rax), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jne	.L21
.L5:
	movq	8(%rbx), %rdi
	call	OBJ_obj2nid@PLT
	leaq	.LC0(%rip), %rdx
	testl	%eax, %eax
	jne	.L22
.L6:
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	24(%rbx), %rax
	movl	(%rax), %r13d
	movq	8(%rax), %r15
	testl	%r13d, %r13d
	jle	.L7
	xorl	%ebx, %ebx
	leaq	.LC6(%rip), %r14
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L8:
	addl	$1, %ebx
	movzbl	(%r15), %edx
	cmpl	%r13d, %ebx
	je	.L9
.L23:
	leaq	.LC7(%rip), %rcx
	movq	%r12, %rdi
	xorl	%eax, %eax
	addq	$1, %r15
	leaq	.LC8(%rip), %rsi
	call	BIO_printf@PLT
.L10:
	imull	$954437177, %ebx, %eax
	rorl	%eax
	cmpl	$238609294, %eax
	ja	.L8
	movl	$7, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	addl	$1, %ebx
	call	BIO_write@PLT
	movzbl	(%r15), %edx
	cmpl	%r13d, %ebx
	jne	.L23
.L9:
	leaq	.LC9(%rip), %rcx
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L7:
	movl	$1, %edx
	leaq	.LC10(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L24
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movl	%eax, %edi
	call	OBJ_nid2ln@PLT
	movq	%rax, %rdx
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L22:
	movl	%eax, %edi
	call	OBJ_nid2ln@PLT
	movq	%rax, %rdx
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L21:
	movq	8(%rax), %rdx
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L20:
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L4
.L24:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE779:
	.size	NETSCAPE_SPKI_print, .-NETSCAPE_SPKI_print
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
