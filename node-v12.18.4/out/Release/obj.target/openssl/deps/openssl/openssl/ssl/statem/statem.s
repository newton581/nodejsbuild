	.file	"statem.c"
	.text
	.p2align 4
	.globl	SSL_get_state
	.type	SSL_get_state, @function
SSL_get_state:
.LFB1001:
	.cfi_startproc
	endbr64
	movl	92(%rdi), %eax
	ret
	.cfi_endproc
.LFE1001:
	.size	SSL_get_state, .-SSL_get_state
	.p2align 4
	.globl	SSL_in_init
	.type	SSL_in_init, @function
SSL_in_init:
.LFB1002:
	.cfi_startproc
	endbr64
	movl	100(%rdi), %eax
	ret
	.cfi_endproc
.LFE1002:
	.size	SSL_in_init, .-SSL_in_init
	.p2align 4
	.globl	SSL_is_init_finished
	.type	SSL_is_init_finished, @function
SSL_is_init_finished:
.LFB1003:
	.cfi_startproc
	endbr64
	movl	100(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jne	.L4
	xorl	%eax, %eax
	cmpl	$1, 92(%rdi)
	sete	%al
.L4:
	ret
	.cfi_endproc
.LFE1003:
	.size	SSL_is_init_finished, .-SSL_is_init_finished
	.p2align 4
	.globl	SSL_in_before
	.type	SSL_in_before, @function
SSL_in_before:
.LFB1004:
	.cfi_startproc
	endbr64
	movl	92(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jne	.L7
	movl	72(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	sete	%al
.L7:
	ret
	.cfi_endproc
.LFE1004:
	.size	SSL_in_before, .-SSL_in_before
	.p2align 4
	.globl	ossl_statem_clear
	.type	ossl_statem_clear, @function
ossl_statem_clear:
.LFB1005:
	.cfi_startproc
	endbr64
	movl	$0, 72(%rdi)
	movl	$0, 92(%rdi)
	movl	$1, 100(%rdi)
	movl	$0, 116(%rdi)
	ret
	.cfi_endproc
.LFE1005:
	.size	ossl_statem_clear, .-ossl_statem_clear
	.p2align 4
	.globl	ossl_statem_set_renegotiate
	.type	ossl_statem_set_renegotiate, @function
ossl_statem_set_renegotiate:
.LFB1006:
	.cfi_startproc
	endbr64
	movabsq	$4294967315, %rax
	movq	%rax, 96(%rdi)
	ret
	.cfi_endproc
.LFE1006:
	.size	ossl_statem_set_renegotiate, .-ossl_statem_set_renegotiate
	.p2align 4
	.globl	ossl_statem_fatal
	.type	ossl_statem_fatal, @function
ossl_statem_fatal:
.LFB1007:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	movl	%edx, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movl	%ecx, %edx
	movq	%rdi, %r12
	movq	%r8, %rcx
	movl	$20, %edi
	movl	%r9d, %r8d
	call	ERR_put_error@PLT
	movl	100(%r12), %eax
	testl	%eax, %eax
	je	.L13
	cmpl	$1, 72(%r12)
	je	.L12
.L13:
	movl	$1, 100(%r12)
	movl	$1, 72(%r12)
	cmpl	$-1, %r13d
	je	.L12
	cmpl	$1, 124(%r12)
	jne	.L20
.L12:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movl	%r13d, %edx
	movq	%r12, %rdi
	movl	$2, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	ssl3_send_alert@PLT
	.cfi_endproc
.LFE1007:
	.size	ossl_statem_fatal, .-ossl_statem_fatal
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/ssl/statem/statem.c"
	.text
	.p2align 4
	.type	state_machine.part.0, @function
state_machine.part.0:
.LFB1034:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, -172(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	ERR_clear_error@PLT
	call	__errno_location@PLT
	movl	$0, (%rax)
	movq	1392(%r15), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	jne	.L22
	movq	1440(%r15), %rax
	movq	272(%rax), %rax
	movq	%rax, -160(%rbp)
.L22:
	movl	100(%r15), %r12d
	addl	$1, 108(%r15)
	testl	%r12d, %r12d
	je	.L23
	movl	72(%r15), %eax
	movl	%eax, %edx
	orl	92(%r15), %edx
	jne	.L24
.L23:
	movq	168(%r15), %rax
	movq	(%rax), %rax
	testb	$8, %ah
	je	.L25
	movl	72(%r15), %eax
.L24:
	testl	$-5, %eax
	je	.L307
	cmpl	$2, %eax
	je	.L151
	cmpl	$3, %eax
	jne	.L96
.L52:
	movq	1392(%r15), %rbx
	testq	%rbx, %rbx
	jne	.L97
	movq	1440(%r15), %rax
	movq	272(%rax), %rbx
.L97:
	movl	56(%r15), %eax
	testl	%eax, %eax
	jne	.L164
	movq	ossl_statem_client_construct_message@GOTPCREL(%rip), %rax
	movq	ossl_statem_client_post_work@GOTPCREL(%rip), %r12
	movq	ossl_statem_client_write_transition@GOTPCREL(%rip), %r14
	movq	%rax, -144(%rbp)
	movq	ossl_statem_client_pre_work@GOTPCREL(%rip), %rax
	movq	%rax, -136(%rbp)
.L98:
	movl	76(%r15), %eax
	leaq	-124(%rbp), %rcx
	movabsq	$12884901891, %r13
	movq	%rcx, -152(%rbp)
	cmpl	$2, %eax
	je	.L100
.L313:
	ja	.L101
	testl	%eax, %eax
	je	.L102
.L103:
	movl	80(%r15), %esi
	movq	-136(%rbp), %rax
	movq	%r15, %rdi
	call	*%rax
	movl	%eax, 80(%r15)
	cmpl	$2, %eax
	je	.L116
	ja	.L117
	testl	%eax, %eax
	je	.L308
.L119:
	movl	$4, 72(%r15)
	movl	$1, %r12d
	xorl	%r13d, %r13d
.L37:
	subl	$1, 108(%r15)
	movq	%r13, %rdi
	call	BUF_MEM_free@PLT
	movq	-160(%rbp), %rax
	testq	%rax, %rax
	je	.L21
	movl	-172(%rbp), %ecx
	movl	%r12d, %edx
	testl	%ecx, %ecx
	je	.L156
	movl	$8194, %esi
	movq	%r15, %rdi
	call	*%rax
.L21:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L309
	addq	$136, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L307:
	.cfi_restore_state
	testl	%eax, %eax
	jne	.L28
	movq	$0, 92(%r15)
.L28:
	movl	-172(%rbp), %eax
	cmpq	$0, -160(%rbp)
	movl	%eax, 56(%r15)
	je	.L29
	movq	168(%r15), %rax
	cmpq	$0, 408(%rax)
	jne	.L310
.L30:
	movq	-160(%rbp), %rax
	movl	$1, %edx
	movl	$16, %esi
	movq	%r15, %rdi
	call	*%rax
.L29:
	movq	8(%r15), %rax
	movl	(%r15), %ecx
	movq	192(%rax), %rax
	testb	$8, 96(%rax)
	je	.L33
	movl	%ecx, %eax
	andl	$65280, %eax
	cmpl	$65024, %eax
	je	.L34
	cmpl	$256, %eax
	jne	.L165
	testb	$1, -172(%rbp)
	jne	.L165
.L34:
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movl	$9, %esi
	movq	%r15, %rdi
	call	ssl_security@PLT
	movl	$372, %r8d
	testl	%eax, %eax
	je	.L287
	cmpq	$0, 136(%r15)
	je	.L311
.L41:
	movq	%r15, %rdi
	call	ssl3_setup_buffers@PLT
	movl	$393, %r8d
	testl	%eax, %eax
	je	.L287
	movq	168(%r15), %rax
	movq	%r15, %rdi
	movq	$0, 152(%r15)
	movl	$0, 240(%rax)
	call	ssl_init_wbio_buffer@PLT
	testl	%eax, %eax
	je	.L312
	movl	92(%r15), %r10d
	testl	%r10d, %r10d
	jne	.L47
	movl	72(%r15), %r9d
	testl	%r9d, %r9d
	jne	.L47
.L48:
	movq	%r15, %rdi
	call	tls_setup_handshake@PLT
	testl	%eax, %eax
	je	.L284
	movq	168(%r15), %rax
	cmpq	$0, 408(%rax)
	je	.L50
	cmpq	$0, 544(%rax)
	jne	.L86
.L50:
	movl	$1, 104(%r15)
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L25:
	movq	%r15, %rdi
	movl	$-1, %r12d
	call	SSL_clear@PLT
	testl	%eax, %eax
	je	.L21
	movl	72(%r15), %eax
	jmp	.L24
.L312:
	movl	$412, %r8d
	.p2align 4,,10
	.p2align 3
.L287:
	movl	$68, %edx
	movl	$353, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	100(%r15), %r11d
	testl	%r11d, %r11d
	je	.L38
	cmpl	$1, 72(%r15)
	je	.L284
.L38:
	movl	$1, 100(%r15)
	movl	$-1, %r12d
	xorl	%r13d, %r13d
	movl	$1, 72(%r15)
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L101:
	cmpl	$3, %eax
	jne	.L104
.L125:
	movl	80(%r15), %esi
	movq	%r15, %rdi
	call	*%r12
	movl	%eax, 80(%r15)
	cmpl	$2, %eax
	je	.L143
.L315:
	ja	.L144
	testl	%eax, %eax
	jne	.L119
	movl	100(%r15), %r9d
	testl	%r9d, %r9d
	je	.L148
	cmpl	$1, 72(%r15)
	je	.L284
.L148:
	movl	$873, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$256, %edx
	.p2align 4,,10
	.p2align 3
.L303:
	movl	$586, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	movl	100(%r15), %r8d
	testl	%r8d, %r8d
	je	.L114
	cmpl	$1, 72(%r15)
	je	.L284
.L114:
	cmpl	$1, 124(%r15)
	movl	$1, 100(%r15)
	movl	$1, 72(%r15)
	je	.L284
	movl	$80, %edx
	movl	$2, %esi
	movq	%r15, %rdi
	call	ssl3_send_alert@PLT
	.p2align 4,,10
	.p2align 3
.L284:
	movl	$-1, %r12d
	xorl	%r13d, %r13d
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L144:
	subl	$3, %eax
	cmpl	$2, %eax
	jbe	.L284
.L253:
	movl	76(%r15), %eax
	cmpl	$2, %eax
	jne	.L313
	.p2align 4,,10
	.p2align 3
.L100:
	movq	8(%r15), %rax
	movq	192(%rax), %rax
	testb	$8, 96(%rax)
	je	.L137
	movl	120(%r15), %r10d
	testl	%r10d, %r10d
	jne	.L314
	movl	92(%r15), %edx
	cmpl	$35, %edx
	je	.L157
	cmpl	$16, %edx
	jne	.L158
.L157:
	movl	$20, %esi
	movq	%r15, %rdi
	call	dtls1_do_write@PLT
.L142:
	testl	%eax, %eax
	jle	.L284
	movq	%r13, 76(%r15)
	movl	$3, %esi
	movq	%r15, %rdi
	call	*%r12
	movl	%eax, 80(%r15)
	cmpl	$2, %eax
	jne	.L315
.L143:
	movl	$0, 76(%r15)
	.p2align 4,,10
	.p2align 3
.L102:
	testq	%rbx, %rbx
	je	.L106
	movl	56(%r15), %eax
	movl	$1, %edx
	testl	%eax, %eax
	je	.L107
	movl	$8193, %esi
	movq	%r15, %rdi
	call	*%rbx
.L106:
	movq	%r15, %rdi
	call	*%r14
	cmpl	$1, %eax
	je	.L108
.L320:
	cmpl	$2, %eax
	je	.L109
	testl	%eax, %eax
	jne	.L253
	movl	100(%r15), %r12d
	testl	%r12d, %r12d
	je	.L112
	cmpl	$1, 72(%r15)
	je	.L284
.L112:
	movl	$804, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$256, %edx
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L164:
	movq	ossl_statem_server_construct_message@GOTPCREL(%rip), %rax
	movq	ossl_statem_server_post_work@GOTPCREL(%rip), %r12
	movq	ossl_statem_server_write_transition@GOTPCREL(%rip), %r14
	movq	%rax, -144(%rbp)
	movq	ossl_statem_server_pre_work@GOTPCREL(%rip), %rax
	movq	%rax, -136(%rbp)
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L104:
	movl	$890, %r8d
.L305:
	leaq	.LC0(%rip), %rcx
	movl	$68, %edx
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L117:
	subl	$3, %eax
	cmpl	$2, %eax
	jbe	.L284
.L120:
	leaq	-112(%rbp), %rax
	movq	-152(%rbp), %rcx
	leaq	-120(%rbp), %rdx
	movq	%r15, %rdi
	movq	%rax, -168(%rbp)
	movq	%rax, %rsi
	movq	-144(%rbp), %rax
	call	*%rax
	testl	%eax, %eax
	je	.L284
	cmpl	$-1, -124(%rbp)
	jne	.L124
	movq	%r13, 76(%r15)
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L109:
	movl	$2, 72(%r15)
	movl	$0, 84(%r15)
.L151:
	movq	1392(%r15), %rbx
	movq	$0, -120(%rbp)
	testq	%rbx, %rbx
	jne	.L53
	movq	1440(%r15), %rax
	movq	272(%rax), %rbx
.L53:
	movl	56(%r15), %edi
	testl	%edi, %edi
	jne	.L163
	movq	ossl_statem_client_max_message_size@GOTPCREL(%rip), %rax
	movq	ossl_statem_client_post_process_message@GOTPCREL(%rip), %r13
	movq	ossl_statem_client_process_message@GOTPCREL(%rip), %r12
	movq	ossl_statem_client_read_transition@GOTPCREL(%rip), %r14
	movq	%rax, -136(%rbp)
.L54:
	movl	104(%r15), %esi
	testl	%esi, %esi
	je	.L55
	movl	$1, 1520(%r15)
	movl	$0, 104(%r15)
.L55:
	movq	%r15, %rcx
	movl	84(%r15), %eax
	movq	%r14, %r15
	movq	%r13, %r14
	movq	%r12, %r13
	movq	%rbx, %r12
	movq	%rcx, %rbx
.L56:
	cmpl	$1, %eax
	je	.L57
	cmpl	$2, %eax
	je	.L58
	testl	%eax, %eax
	je	.L59
	movq	%rbx, %r15
	movl	$688, %r8d
.L300:
	leaq	.LC0(%rip), %rcx
	movl	$68, %edx
.L297:
	movl	$352, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	movl	100(%r15), %eax
	testl	%eax, %eax
	je	.L73
	cmpl	$1, 72(%r15)
	je	.L284
.L73:
	cmpl	$1, 124(%r15)
	movl	$1, 100(%r15)
	movl	$1, 72(%r15)
	je	.L284
	movl	$80, %edx
.L283:
	movl	$2, %esi
	movq	%r15, %rdi
	movl	$-1, %r12d
	xorl	%r13d, %r13d
	call	ssl3_send_alert@PLT
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L57:
	movq	8(%rbx), %rax
	movq	192(%rax), %rax
	movl	96(%rax), %eax
	andl	$8, %eax
.L61:
	testl	%eax, %eax
	je	.L74
.L77:
	movl	$0, 1520(%rbx)
	movq	-120(%rbp), %rax
	testq	%rax, %rax
	js	.L316
	movq	144(%rbx), %rdx
	movq	%rax, -104(%rbp)
	leaq	-112(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rdx, -112(%rbp)
	call	*%r13
	movq	$0, 152(%rbx)
	cmpl	$1, %eax
	je	.L80
	cmpl	$2, %eax
	je	.L81
	testl	%eax, %eax
	je	.L317
.L82:
	movl	$0, 84(%rbx)
.L59:
	movq	8(%rbx), %rax
	movq	192(%rax), %rax
	testb	$8, 96(%rax)
	je	.L62
	leaq	-120(%rbp), %rdx
	leaq	-124(%rbp), %rsi
	movq	%rbx, %rdi
	call	dtls_get_message@PLT
.L63:
	testl	%eax, %eax
	je	.L257
	testq	%r12, %r12
	je	.L65
	movl	56(%rbx), %ecx
	movl	$1, %edx
	testl	%ecx, %ecx
	je	.L66
	movl	$8193, %esi
	movq	%rbx, %rdi
	call	*%r12
.L65:
	movl	-124(%rbp), %esi
	movq	%rbx, %rdi
	call	*%r15
	testl	%eax, %eax
	je	.L257
	movq	168(%rbx), %rax
	movq	-136(%rbp), %rcx
	movq	%rbx, %rdi
	movq	552(%rax), %rax
	movq	%rax, -144(%rbp)
	call	*%rcx
	cmpq	%rax, -144(%rbp)
	ja	.L318
	movq	8(%rbx), %rax
	movq	192(%rax), %rax
	movl	96(%rax), %eax
	andl	$8, %eax
	jne	.L70
	movq	168(%rbx), %rax
	movq	552(%rax), %rax
	testq	%rax, %rax
	jne	.L319
	movl	$1, 84(%rbx)
.L74:
	leaq	-120(%rbp), %rsi
	movq	%rbx, %rdi
	call	tls_get_message_body@PLT
	testl	%eax, %eax
	jne	.L77
.L257:
	movq	%rbx, %r15
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L137:
	movl	92(%r15), %edx
	cmpl	$35, %edx
	je	.L141
	cmpl	$16, %edx
	jne	.L158
.L141:
	movl	$20, %esi
	movq	%r15, %rdi
	call	ssl3_do_write@PLT
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L163:
	movq	ossl_statem_server_max_message_size@GOTPCREL(%rip), %rax
	movq	ossl_statem_server_post_process_message@GOTPCREL(%rip), %r13
	movq	ossl_statem_server_process_message@GOTPCREL(%rip), %r12
	movq	ossl_statem_server_read_transition@GOTPCREL(%rip), %r14
	movq	%rax, -136(%rbp)
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L314:
	movq	%r15, %rdi
	call	dtls1_start_timer@PLT
	movl	92(%r15), %eax
	cmpl	$16, %eax
	je	.L166
	cmpl	$35, %eax
	je	.L166
	movq	8(%r15), %rax
	movq	192(%rax), %rax
	.p2align 4,,10
	.p2align 3
.L158:
	movq	%r15, %rdi
	call	*120(%rax)
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L107:
	movq	%r15, %rdi
	movl	$4097, %esi
	call	*%rbx
	movq	%r15, %rdi
	call	*%r14
	cmpl	$1, %eax
	jne	.L320
.L108:
	movabsq	$12884901889, %rax
	movq	%rax, 76(%r15)
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L81:
	movabsq	$12884901890, %rax
	movq	%rax, 84(%rbx)
.L58:
	movl	88(%rbx), %esi
	movq	%rbx, %rdi
	call	*%r14
	movl	%eax, 88(%rbx)
	cmpl	$2, %eax
	je	.L82
	ja	.L87
	movq	%rbx, %r15
	testl	%eax, %eax
	je	.L321
.L275:
	movq	8(%r15), %rax
	movq	192(%rax), %rax
	testb	$8, 96(%rax)
	jne	.L322
.L86:
	movq	$3, 72(%r15)
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L87:
	subl	$3, %eax
	cmpl	$2, %eax
	ja	.L323
	movq	%rbx, %r15
	movl	$-1, %r12d
	xorl	%r13d, %r13d
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L116:
	movl	$2, 76(%r15)
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L308:
	movl	100(%r15), %ebx
	testl	%ebx, %ebx
	je	.L121
	cmpl	$1, 72(%r15)
	je	.L284
.L121:
	movl	$812, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$256, %edx
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L166:
	movq	8(%r15), %rax
	movq	192(%rax), %rax
	testb	$8, 96(%rax)
	jne	.L157
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L319:
	movq	136(%rbx), %rdi
	leaq	4(%rax), %rdx
	movq	144(%rbx), %rax
	movslq	%edx, %rsi
	movq	%rdx, -152(%rbp)
	subq	8(%rdi), %rax
	movq	%rax, -144(%rbp)
	call	BUF_MEM_grow_clean@PLT
	testq	%rax, %rax
	je	.L72
	movq	-144(%rbp), %rcx
	movq	-152(%rbp), %rdx
	cmpq	%rcx, %rdx
	jb	.L72
	movq	136(%rbx), %rax
	addq	8(%rax), %rcx
	movq	8(%rbx), %rax
	movq	%rcx, 144(%rbx)
	movq	192(%rax), %rax
	movl	96(%rax), %eax
	andl	$8, %eax
	.p2align 4,,10
	.p2align 3
.L70:
	movl	$1, 84(%rbx)
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L124:
	movq	136(%r15), %rsi
	movq	-168(%rbp), %rdi
	call	WPACKET_init@PLT
	testl	%eax, %eax
	je	.L281
	movq	8(%r15), %rax
	movl	-124(%rbp), %edx
	movq	%r15, %rdi
	movq	-168(%rbp), %rsi
	movq	192(%rax), %rax
	call	*104(%rax)
	testl	%eax, %eax
	je	.L281
	movq	-120(%rbp), %rax
	testq	%rax, %rax
	je	.L131
	movq	-168(%rbp), %rsi
	movq	%r15, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L324
.L131:
	movq	8(%r15), %rax
	movl	-124(%rbp), %edx
	movq	%r15, %rdi
	movq	-168(%rbp), %rsi
	movq	192(%rax), %rax
	call	*112(%rax)
	testl	%eax, %eax
	je	.L282
	movq	-168(%rbp), %rdi
	call	WPACKET_finish@PLT
	testl	%eax, %eax
	jne	.L100
.L282:
	movq	-168(%rbp), %r8
	movq	%r8, %rdi
	call	WPACKET_cleanup@PLT
	movl	$851, %r8d
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L156:
	movl	$4098, %esi
	movq	%r15, %rdi
	call	*%rax
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L316:
	movq	%rbx, %r15
	movl	$632, %r8d
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L62:
	leaq	-124(%rbp), %rsi
	movq	%rbx, %rdi
	call	tls_get_message_header@PLT
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L310:
	cmpq	$0, 544(%rax)
	je	.L30
	movq	8(%r15), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L30
	movl	(%rax), %eax
	cmpl	$65536, %eax
	je	.L30
	cmpl	$771, %eax
	jle	.L30
	movl	(%r15), %ecx
	.p2align 4,,10
	.p2align 3
.L33:
	movl	%ecx, %eax
	movl	$365, %r8d
	sarl	$8, %eax
	cmpl	$3, %eax
	je	.L34
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L66:
	movl	$4097, %esi
	movq	%rbx, %rdi
	call	*%r12
	jmp	.L65
.L311:
	call	BUF_MEM_new@PLT
	movl	$379, %r8d
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L287
	movl	$16384, %esi
	movq	%rax, %rdi
	call	BUF_MEM_grow@PLT
	testq	%rax, %rax
	je	.L325
	movq	%r13, 136(%r15)
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L165:
	movl	$359, %r8d
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L47:
	movl	1928(%r15), %r8d
	testl	%r8d, %r8d
	je	.L86
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L317:
	movl	100(%rbx), %eax
	movq	%rbx, %r15
	testl	%eax, %eax
	je	.L83
	cmpl	$1, 72(%rbx)
	je	.L284
.L83:
	movl	$643, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$256, %edx
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L80:
	movq	%rbx, %r15
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L96:
	movl	100(%r15), %edi
	testl	%edi, %edi
	je	.L152
	cmpl	$1, %eax
	je	.L153
.L152:
	movl	$353, %esi
	movl	$455, %r8d
	movl	$256, %edx
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	100(%r15), %esi
	testl	%esi, %esi
	je	.L154
	cmpl	$1, 72(%r15)
	je	.L153
.L154:
	cmpl	$1, 124(%r15)
	movl	$1, 100(%r15)
	movl	$1, 72(%r15)
	je	.L153
	movl	$80, %edx
	movl	$2, %esi
	movq	%r15, %rdi
	call	ssl3_send_alert@PLT
.L153:
	movl	$456, %r8d
	movl	$66, %edx
	movl	$353, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L321:
	movl	100(%rbx), %eax
	testl	%eax, %eax
	je	.L92
	cmpl	$1, 72(%rbx)
	je	.L284
.L92:
	movl	$667, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$256, %edx
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L323:
	movl	84(%rbx), %eax
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L322:
	movq	%r15, %rdi
	call	dtls1_stop_timer@PLT
	jmp	.L86
.L72:
	movq	%rbx, %r15
	movl	$612, %r8d
	movl	$7, %edx
	leaq	.LC0(%rip), %rcx
	jmp	.L297
.L325:
	movl	$384, %r8d
	movl	$68, %edx
	movl	$353, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	100(%r15), %ebx
	testl	%ebx, %ebx
	je	.L44
	cmpl	$1, 72(%r15)
	movl	$-1, %r12d
	je	.L37
.L44:
	movl	$1, 100(%r15)
	movl	$-1, %r12d
	movl	$1, 72(%r15)
	jmp	.L37
.L281:
	movq	-168(%rbp), %r8
	movq	%r8, %rdi
	call	WPACKET_cleanup@PLT
	movl	$839, %r8d
	jmp	.L305
.L318:
	movl	$152, %edx
	movl	$602, %r8d
	movl	$352, %esi
	movq	%rbx, %r15
	leaq	.LC0(%rip), %rcx
	movl	$20, %edi
	call	ERR_put_error@PLT
	movl	100(%rbx), %edx
	testl	%edx, %edx
	je	.L68
	cmpl	$1, 72(%rbx)
	je	.L284
.L68:
	cmpl	$1, 124(%r15)
	movl	$1, 100(%r15)
	movl	$47, %edx
	movl	$1, 72(%r15)
	je	.L284
	jmp	.L283
.L324:
	movq	-168(%rbp), %r8
	movq	%r8, %rdi
	call	WPACKET_cleanup@PLT
	movl	100(%r15), %r11d
	testl	%r11d, %r11d
	je	.L132
	cmpl	$1, 72(%r15)
	je	.L284
.L132:
	movl	$845, %r9d
	leaq	.LC0(%rip), %r8
	movl	$256, %ecx
	movq	%r15, %rdi
	movl	$586, %edx
	movl	$80, %esi
	call	ossl_statem_fatal
	jmp	.L284
.L309:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1034:
	.size	state_machine.part.0, .-state_machine.part.0
	.p2align 4
	.globl	ossl_statem_in_error
	.type	ossl_statem_in_error, @function
ossl_statem_in_error:
.LFB1008:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$1, 72(%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE1008:
	.size	ossl_statem_in_error, .-ossl_statem_in_error
	.p2align 4
	.globl	ossl_statem_set_in_init
	.type	ossl_statem_set_in_init, @function
ossl_statem_set_in_init:
.LFB1009:
	.cfi_startproc
	endbr64
	movl	%esi, 100(%rdi)
	ret
	.cfi_endproc
.LFE1009:
	.size	ossl_statem_set_in_init, .-ossl_statem_set_in_init
	.p2align 4
	.globl	ossl_statem_get_in_handshake
	.type	ossl_statem_get_in_handshake, @function
ossl_statem_get_in_handshake:
.LFB1010:
	.cfi_startproc
	endbr64
	movl	108(%rdi), %eax
	ret
	.cfi_endproc
.LFE1010:
	.size	ossl_statem_get_in_handshake, .-ossl_statem_get_in_handshake
	.p2align 4
	.globl	ossl_statem_set_in_handshake
	.type	ossl_statem_set_in_handshake, @function
ossl_statem_set_in_handshake:
.LFB1011:
	.cfi_startproc
	endbr64
	movl	108(%rdi), %eax
	leal	1(%rax), %edx
	subl	$1, %eax
	testl	%esi, %esi
	cmovne	%edx, %eax
	movl	%eax, 108(%rdi)
	ret
	.cfi_endproc
.LFE1011:
	.size	ossl_statem_set_in_handshake, .-ossl_statem_set_in_handshake
	.p2align 4
	.globl	ossl_statem_skip_early_data
	.type	ossl_statem_skip_early_data, @function
ossl_statem_skip_early_data:
.LFB1012:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$1, 1816(%rdi)
	jne	.L332
	movl	56(%rdi), %eax
	testl	%eax, %eax
	je	.L332
	xorl	%eax, %eax
	cmpl	$46, 92(%rdi)
	je	.L339
.L332:
	ret
	.p2align 4,,10
	.p2align 3
.L339:
	xorl	%eax, %eax
	cmpl	$2, 1248(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE1012:
	.size	ossl_statem_skip_early_data, .-ossl_statem_skip_early_data
	.p2align 4
	.globl	ossl_statem_check_finish_init
	.type	ossl_statem_check_finish_init, @function
ossl_statem_check_finish_init:
.LFB1013:
	.cfi_startproc
	endbr64
	cmpl	$-1, %esi
	je	.L357
	movl	56(%rdi), %eax
	testl	%eax, %eax
	jne	.L345
	movl	92(%rdi), %eax
	testl	%esi, %esi
	je	.L346
	subl	$46, %eax
	cmpl	$1, %eax
	ja	.L340
	movl	132(%rdi), %eax
	cmpl	$4, %eax
	je	.L358
	movl	$1, 100(%rdi)
	cmpl	$3, %eax
	je	.L349
.L340:
	ret
	.p2align 4,,10
	.p2align 3
.L345:
	cmpl	$12, 132(%rdi)
	jne	.L340
	cmpl	$46, 92(%rdi)
	jne	.L340
.L348:
	movl	$1, 100(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L357:
	movl	92(%rdi), %eax
	subl	$46, %eax
	cmpl	$1, %eax
	ja	.L340
	cmpl	$3, 132(%rdi)
	movl	$1, 100(%rdi)
	jne	.L340
.L349:
	movl	$7, 132(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L346:
	cmpl	$46, %eax
	je	.L348
	ret
	.p2align 4,,10
	.p2align 3
.L358:
	ret
	.cfi_endproc
.LFE1013:
	.size	ossl_statem_check_finish_init, .-ossl_statem_check_finish_init
	.p2align 4
	.globl	ossl_statem_set_hello_verify_done
	.type	ossl_statem_set_hello_verify_done, @function
ossl_statem_set_hello_verify_done:
.LFB1014:
	.cfi_startproc
	endbr64
	movl	$0, 72(%rdi)
	movl	$1, 100(%rdi)
	movl	$20, 92(%rdi)
	ret
	.cfi_endproc
.LFE1014:
	.size	ossl_statem_set_hello_verify_done, .-ossl_statem_set_hello_verify_done
	.p2align 4
	.globl	ossl_statem_connect
	.type	ossl_statem_connect, @function
ossl_statem_connect:
.LFB1015:
	.cfi_startproc
	endbr64
	cmpl	$1, 72(%rdi)
	je	.L361
	xorl	%esi, %esi
	jmp	state_machine.part.0
	.p2align 4,,10
	.p2align 3
.L361:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE1015:
	.size	ossl_statem_connect, .-ossl_statem_connect
	.p2align 4
	.globl	ossl_statem_accept
	.type	ossl_statem_accept, @function
ossl_statem_accept:
.LFB1016:
	.cfi_startproc
	endbr64
	cmpl	$1, 72(%rdi)
	je	.L363
	movl	$1, %esi
	jmp	state_machine.part.0
	.p2align 4,,10
	.p2align 3
.L363:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE1016:
	.size	ossl_statem_accept, .-ossl_statem_accept
	.p2align 4
	.globl	statem_flush
	.type	statem_flush, @function
statem_flush:
.LFB1025:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	$2, 40(%rdi)
	movq	24(%rdi), %rdi
	call	BIO_ctrl@PLT
	movq	%rax, %r8
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L364
	movl	$1, 40(%rbx)
	movl	$1, %eax
.L364:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1025:
	.size	statem_flush, .-statem_flush
	.p2align 4
	.globl	ossl_statem_app_data_allowed
	.type	ossl_statem_app_data_allowed, @function
ossl_statem_app_data_allowed:
.LFB1026:
	.cfi_startproc
	endbr64
	movl	72(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L369
	movq	168(%rdi), %rdx
	movl	272(%rdx), %eax
	testl	%eax, %eax
	je	.L369
	movl	264(%rdx), %eax
	testl	%eax, %eax
	je	.L369
	movl	56(%rdi), %eax
	movl	92(%rdi), %edx
	testl	%eax, %eax
	je	.L371
	testl	%edx, %edx
	sete	%al
	cmpl	$20, %edx
	sete	%dl
	orl	%edx, %eax
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L371:
	xorl	%eax, %eax
	cmpl	$12, %edx
	sete	%al
.L369:
	ret
	.cfi_endproc
.LFE1026:
	.size	ossl_statem_app_data_allowed, .-ossl_statem_app_data_allowed
	.p2align 4
	.globl	ossl_statem_export_allowed
	.type	ossl_statem_export_allowed, @function
ossl_statem_export_allowed:
.LFB1027:
	.cfi_startproc
	endbr64
	movq	168(%rdi), %rdx
	xorl	%eax, %eax
	cmpq	$0, 976(%rdx)
	je	.L379
	xorl	%eax, %eax
	cmpl	$36, 92(%rdi)
	setne	%al
.L379:
	ret
	.cfi_endproc
.LFE1027:
	.size	ossl_statem_export_allowed, .-ossl_statem_export_allowed
	.p2align 4
	.globl	ossl_statem_export_early_allowed
	.type	ossl_statem_export_early_allowed, @function
ossl_statem_export_early_allowed:
.LFB1028:
	.cfi_startproc
	endbr64
	movl	1816(%rdi), %edx
	movl	$1, %eax
	cmpl	$2, %edx
	je	.L382
	movl	56(%rdi), %eax
	testl	%eax, %eax
	sete	%cl
	xorl	%eax, %eax
	testl	%edx, %edx
	setne	%al
	andl	%ecx, %eax
.L382:
	ret
	.cfi_endproc
.LFE1028:
	.size	ossl_statem_export_early_allowed, .-ossl_statem_export_early_allowed
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
