	.file	"cversion.c"
	.text
	.p2align 4
	.globl	OpenSSL_version_num
	.type	OpenSSL_version_num, @function
OpenSSL_version_num:
.LFB251:
	.cfi_startproc
	endbr64
	movl	$269488255, %eax
	ret
	.cfi_endproc
.LFE251:
	.size	OpenSSL_version_num, .-OpenSSL_version_num
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"not available"
.LC1:
	.string	"OpenSSL 1.1.1g  21 Apr 2020"
.LC2:
	.string	"platform: linux-x86_64"
.LC3:
	.string	"OPENSSLDIR: \"/etc/ssl\""
.LC4:
	.string	"ENGINESDIR: \"/dev/null\""
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"built on: Tue Apr 21 13:29:29 2020 UTC"
	.text
	.p2align 4
	.globl	OpenSSL_version
	.type	OpenSSL_version, @function
OpenSSL_version:
.LFB252:
	.cfi_startproc
	endbr64
	cmpl	$5, %edi
	ja	.L4
	leaq	.L6(%rip), %rdx
	movl	%edi, %edi
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L6:
	.long	.L12-.L6
	.long	.L10-.L6
	.long	.L9-.L6
	.long	.L8-.L6
	.long	.L7-.L6
	.long	.L5-.L6
	.text
	.p2align 4,,10
	.p2align 3
.L9:
	leaq	.LC5(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	leaq	.LC1(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	leaq	compiler_flags(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	leaq	.LC2(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	leaq	.LC3(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	leaq	.LC4(%rip), %rax
	ret
.L4:
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE252:
	.size	OpenSSL_version, .-OpenSSL_version
	.section	.rodata
	.align 32
	.type	compiler_flags, @object
	.size	compiler_flags, 383
compiler_flags:
	.ascii	"compiler: gcc -fPIC -pthread -m64 -Wa,--noexecstack -Wall -O"
	.ascii	"3 -DOPENSSL_USE_NODELETE -DL_ENDIAN -DOPENSSL_PIC -DOPENSSL_"
	.ascii	"CPUID_"
	.string	"OBJ -DOPENSSL_IA32_SSE2 -DOPENSSL_BN_ASM_MONT -DOPENSSL_BN_ASM_MONT5 -DOPENSSL_BN_ASM_GF2m -DSHA1_ASM -DSHA256_ASM -DSHA512_ASM -DKECCAK1600_ASM -DRC4_ASM -DMD5_ASM -DAESNI_ASM -DVPAES_ASM -DGHASH_ASM -DECP_NISTZ256_ASM -DX25519_ASM -DPOLY1305_ASM -DNDEBUG"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
