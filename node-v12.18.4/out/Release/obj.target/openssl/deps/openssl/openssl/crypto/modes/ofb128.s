	.file	"ofb128.c"
	.text
	.p2align 4
	.globl	CRYPTO_ofb128_encrypt
	.type	CRYPTO_ofb128_encrypt, @function
CRYPTO_ofb128_encrypt:
.LFB151:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movl	(%r9), %ebx
	movq	%r9, -80(%rbp)
	testl	%ebx, %ebx
	jne	.L68
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L70:
	movl	%ebx, %ecx
	addq	$1, %rax
	addq	$1, %r15
	addl	$1, %ebx
	movzbl	(%r14,%rcx), %ecx
	xorb	-1(%rax), %cl
	subq	$1, %r13
	movb	%cl, -1(%r15)
	andl	$15, %ebx
	je	.L2
.L68:
	testq	%r13, %r13
	jne	.L70
.L2:
	cmpq	$15, %r13
	jbe	.L10
	leaq	-16(%r13), %rcx
	andq	$-16, %rcx
	leaq	16(%rcx), %rdx
	movq	%r15, %rcx
	movq	%rdx, -88(%rbp)
	addq	%rax, %rdx
	movq	%rdx, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%rax, -64(%rbp)
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	movq	%rcx, -56(%rbp)
	call	*16(%rbp)
	cmpl	$15, %ebx
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rax
	ja	.L6
	movl	%ebx, %esi
	addl	$8, %ebx
	movq	(%rax,%rsi), %rdi
	xorq	(%r14,%rsi), %rdi
	movq	%rdi, (%rcx,%rsi)
	cmpl	$15, %ebx
	ja	.L6
	movq	(%rax,%rbx), %rsi
	xorq	(%r14,%rbx), %rsi
	movq	%rsi, (%rcx,%rbx)
.L6:
	addq	$16, %rcx
	addq	$16, %rax
	xorl	%ebx, %ebx
	cmpq	-72(%rbp), %rax
	jne	.L7
	addq	-88(%rbp), %r15
	andl	$15, %r13d
.L5:
	testq	%r13, %r13
	je	.L8
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r14, %rsi
	call	*16(%rbp)
	movq	-72(%rbp), %rdi
	movl	%ebx, %eax
	movq	%r13, %rcx
	movzbl	(%r14,%rax), %edx
	xorb	(%rdi,%rax), %dl
	movb	%dl, (%r15,%rax)
	leal	1(%rbx), %edx
	subq	$1, %rcx
	je	.L9
	movl	%edx, %eax
	movzbl	(%r14,%rax), %esi
	xorb	(%rdi,%rax), %sil
	movb	%sil, (%r15,%rax)
	leal	2(%rbx), %eax
	cmpq	$2, %r13
	je	.L9
	movzbl	(%r14,%rax), %esi
	xorb	(%rdi,%rax), %sil
	movb	%sil, (%r15,%rax)
	leal	3(%rbx), %eax
	cmpq	$3, %r13
	je	.L9
	movzbl	(%rdi,%rax), %esi
	xorb	(%r14,%rax), %sil
	movb	%sil, (%r15,%rax)
	leal	4(%rbx), %eax
	cmpq	$4, %r13
	je	.L9
	movzbl	(%rdi,%rax), %esi
	xorb	(%r14,%rax), %sil
	movb	%sil, (%r15,%rax)
	leal	5(%rbx), %eax
	cmpq	$5, %r13
	je	.L9
	movzbl	(%rdi,%rax), %esi
	xorb	(%r14,%rax), %sil
	movb	%sil, (%r15,%rax)
	leal	6(%rbx), %eax
	cmpq	$6, %r13
	je	.L9
	movzbl	(%rdi,%rax), %esi
	xorb	(%r14,%rax), %sil
	movb	%sil, (%r15,%rax)
	leal	7(%rbx), %eax
	cmpq	$7, %r13
	je	.L9
	movzbl	(%rdi,%rax), %esi
	xorb	(%r14,%rax), %sil
	movb	%sil, (%r15,%rax)
	leal	8(%rbx), %eax
	cmpq	$8, %r13
	je	.L9
	movzbl	(%rdi,%rax), %esi
	xorb	(%r14,%rax), %sil
	movb	%sil, (%r15,%rax)
	leal	9(%rbx), %eax
	cmpq	$9, %r13
	je	.L9
	movzbl	(%rdi,%rax), %esi
	xorb	(%r14,%rax), %sil
	movb	%sil, (%r15,%rax)
	leal	10(%rbx), %eax
	cmpq	$10, %r13
	je	.L9
	movzbl	(%rdi,%rax), %esi
	xorb	(%r14,%rax), %sil
	movb	%sil, (%r15,%rax)
	leal	11(%rbx), %eax
	cmpq	$11, %r13
	je	.L9
	movzbl	(%rdi,%rax), %esi
	xorb	(%r14,%rax), %sil
	movb	%sil, (%r15,%rax)
	leal	12(%rbx), %eax
	cmpq	$12, %r13
	je	.L9
	movzbl	(%rdi,%rax), %esi
	xorb	(%r14,%rax), %sil
	movb	%sil, (%r15,%rax)
	leal	13(%rbx), %eax
	cmpq	$13, %r13
	je	.L9
	movzbl	(%rdi,%rax), %esi
	xorb	(%r14,%rax), %sil
	movb	%sil, (%r15,%rax)
	leal	14(%rbx), %eax
	cmpq	$14, %r13
	je	.L9
	movzbl	(%rdi,%rax), %esi
	xorb	(%r14,%rax), %sil
	movb	%sil, (%r15,%rax)
.L9:
	leal	(%rdx,%rcx), %ebx
.L8:
	movq	-80(%rbp), %rax
	movl	%ebx, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L10:
	.cfi_restore_state
	movq	%rax, -72(%rbp)
	jmp	.L5
	.cfi_endproc
.LFE151:
	.size	CRYPTO_ofb128_encrypt, .-CRYPTO_ofb128_encrypt
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
