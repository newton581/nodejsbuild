	.file	"extensions.c"
	.text
	.p2align 4
	.type	init_session_ticket, @function
init_session_ticket:
.LFB1014:
	.cfi_startproc
	endbr64
	movl	56(%rdi), %eax
	testl	%eax, %eax
	jne	.L2
	movl	$0, 1664(%rdi)
.L2:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1014:
	.size	init_session_ticket, .-init_session_ticket
	.p2align 4
	.type	init_npn, @function
init_npn:
.LFB1016:
	.cfi_startproc
	endbr64
	movq	168(%rdi), %rax
	movl	$0, 988(%rax)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1016:
	.size	init_npn, .-init_npn
	.p2align 4
	.type	init_etm, @function
init_etm:
.LFB1022:
	.cfi_startproc
	endbr64
	movl	$0, 1812(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1022:
	.size	init_etm, .-init_etm
	.p2align 4
	.type	init_srtp, @function
init_srtp:
.LFB1028:
	.cfi_startproc
	endbr64
	movl	56(%rdi), %eax
	testl	%eax, %eax
	je	.L7
	movq	$0, 1920(%rdi)
.L7:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1028:
	.size	init_srtp, .-init_srtp
	.p2align 4
	.type	init_psk_kex_modes, @function
init_psk_kex_modes:
.LFB1031:
	.cfi_startproc
	endbr64
	movl	$0, 1808(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1031:
	.size	init_psk_kex_modes, .-init_psk_kex_modes
	.p2align 4
	.type	init_post_handshake_auth, @function
init_post_handshake_auth:
.LFB1035:
	.cfi_startproc
	endbr64
	movl	$0, 1936(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1035:
	.size	init_post_handshake_auth, .-init_post_handshake_auth
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/ssl/statem/extensions.c"
	.text
	.p2align 4
	.type	tls_construct_certificate_authorities, @function
tls_construct_certificate_authorities:
.LFB1026:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	call	get_ca_names@PLT
	testq	%rax, %rax
	je	.L13
	movq	%rax, %rdi
	movq	%rax, %r12
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	je	.L13
	movl	$2, %edx
	movl	$47, %esi
	movq	%r14, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L15
	movl	$2, %esi
	movq	%r14, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	jne	.L27
.L15:
	movl	$1214, %r9d
.L26:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	$542, %edx
	movq	%r13, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L10:
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$2, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	construct_ca_names@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	je	.L10
	movq	%r14, %rdi
	call	WPACKET_close@PLT
	movl	%eax, %r8d
	movl	$1, %eax
	testl	%r8d, %r8d
	jne	.L10
	movl	$1226, %r9d
	jmp	.L26
	.cfi_endproc
.LFE1026:
	.size	tls_construct_certificate_authorities, .-tls_construct_certificate_authorities
	.p2align 4
	.type	init_certificate_authorities, @function
init_certificate_authorities:
.LFB1025:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	168(%rdi), %rax
	movq	X509_NAME_free@GOTPCREL(%rip), %rsi
	movq	608(%rax), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	168(%rbx), %rax
	movq	$0, 608(%rax)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1025:
	.size	init_certificate_authorities, .-init_certificate_authorities
	.p2align 4
	.type	init_sig_algs, @function
init_sig_algs:
.LFB1019:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1137, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	168(%rdi), %rax
	movq	744(%rax), %rdi
	call	CRYPTO_free@PLT
	movq	168(%rbx), %rax
	movq	$0, 744(%rax)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1019:
	.size	init_sig_algs, .-init_sig_algs
	.p2align 4
	.type	init_sig_algs_cert, @function
init_sig_algs_cert:
.LFB1020:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1146, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	168(%rdi), %rax
	movq	752(%rax), %rdi
	call	CRYPTO_free@PLT
	movq	168(%rbx), %rax
	movq	$0, 752(%rax)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1020:
	.size	init_sig_algs_cert, .-init_sig_algs_cert
	.p2align 4
	.type	init_alpn, @function
init_alpn:
.LFB1017:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1103, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	168(%rdi), %rax
	movq	992(%rax), %rdi
	call	CRYPTO_free@PLT
	movq	168(%rbx), %rax
	movl	56(%rbx), %edx
	movq	$0, 992(%rax)
	movq	$0, 1000(%rax)
	testl	%edx, %edx
	jne	.L37
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	movq	1008(%rax), %rdi
	movl	$1107, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	168(%rbx), %rax
	movq	$0, 1008(%rax)
	movq	$0, 1016(%rax)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1017:
	.size	init_alpn, .-init_alpn
	.p2align 4
	.type	init_srp, @function
init_srp:
.LFB1021:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1155, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	2008(%rdi), %rdi
	call	CRYPTO_free@PLT
	movl	$1, %eax
	movq	$0, 2008(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1021:
	.size	init_srp, .-init_srp
	.p2align 4
	.type	final_alpn, @function
final_alpn:
.LFB1018:
	.cfi_startproc
	endbr64
	movl	56(%rdi), %eax
	orl	%eax, %edx
	jne	.L41
	movq	1296(%rdi), %rax
	cmpq	$0, 584(%rax)
	je	.L40
	movl	$0, 1820(%rdi)
.L40:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	testl	%eax, %eax
	je	.L40
	movq	8(%rdi), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L40
	movl	(%rax), %eax
	cmpl	$65536, %eax
	je	.L40
	cmpl	$771, %eax
	jle	.L40
	jmp	tls_handle_alpn@PLT
	.cfi_endproc
.LFE1018:
	.size	final_alpn, .-final_alpn
	.p2align 4
	.type	final_server_name, @function
final_server_name:
.LFB1012:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$112, -44(%rbp)
	call	SSL_get_options@PLT
	movq	%rax, %rbx
	movq	1440(%r12), %rax
	testq	%rax, %rax
	je	.L49
	movq	1904(%r12), %rdx
	testq	%rdx, %rdx
	je	.L49
	movq	512(%rax), %rcx
	testq	%rcx, %rcx
	je	.L52
	movq	520(%rax), %rdx
	leaq	-44(%rbp), %rsi
	movq	%r12, %rdi
	call	*%rcx
	movl	%eax, %r13d
.L53:
	movl	56(%r12), %edx
	testl	%edx, %edx
	je	.L54
	testl	%r14d, %r14d
	je	.L54
	testl	%r13d, %r13d
	jne	.L54
	movl	200(%r12), %r13d
	testl	%r13d, %r13d
	je	.L55
.L56:
	xorl	%r13d, %r13d
.L54:
	movq	168(%r12), %rax
	cmpq	$0, 408(%rax)
	jne	.L94
.L57:
	movq	1440(%r12), %rax
	cmpq	1904(%r12), %rax
	je	.L58
	lock addl	$1, 124(%rax)
	movq	1904(%r12), %rax
	lock subl	$1, 124(%rax)
.L58:
	testl	%r13d, %r13d
	jne	.L59
	andb	$64, %bh
	jne	.L92
	movl	1664(%r12), %eax
	testl	%eax, %eax
	jne	.L60
	.p2align 4,,10
	.p2align 3
.L92:
	movl	$1, %r13d
.L48:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L95
	addq	$16, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	cmpl	$2, %r13d
	je	.L64
	cmpl	$3, %r13d
	jne	.L96
	movl	$0, 1864(%r12)
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L96:
	cmpl	$1, %r13d
	jne	.L92
	movq	8(%r12), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L67
	movl	(%rax), %eax
	cmpl	$65536, %eax
	je	.L67
	cmpl	$771, %eax
	jg	.L68
.L67:
	movl	-44(%rbp), %edx
	movl	$1, %esi
	movq	%r12, %rdi
	call	ssl3_send_alert@PLT
.L68:
	movl	$0, 1864(%r12)
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L94:
	cmpq	$0, 544(%rax)
	jne	.L58
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L49:
	movl	$68, %ecx
	movl	$558, %edx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	movl	$931, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L52:
	movq	512(%rdx), %rax
	movl	$3, %r13d
	testq	%rax, %rax
	je	.L54
	movq	520(%rdx), %rdx
	leaq	-44(%rbp), %rsi
	movq	%r12, %rdi
	call	*%rax
	movl	%eax, %r13d
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L64:
	movl	-44(%rbp), %esi
	movl	$1006, %r9d
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	leaq	.LC0(%rip), %r8
	movl	$234, %ecx
	movl	$558, %edx
	call	ossl_statem_fatal@PLT
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L60:
	movq	%r12, %rdi
	call	SSL_get_options@PLT
	testb	$64, %ah
	je	.L92
	movl	200(%r12), %r13d
	movl	$0, 1664(%r12)
	testl	%r13d, %r13d
	jne	.L92
	movq	%r12, %rdi
	call	SSL_get_session@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L63
	movq	552(%rax), %rdi
	movl	$986, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	$0, 552(%r14)
	movl	$0, 576(%r14)
	movups	%xmm0, 560(%r14)
	call	ssl_generate_session_id@PLT
	movl	$992, %r9d
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.L92
.L93:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	$558, %edx
	movq	%r12, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L55:
	movq	1296(%r12), %rax
	movl	$954, %edx
	leaq	.LC0(%rip), %rsi
	movq	544(%rax), %rdi
	call	CRYPTO_free@PLT
	movq	1600(%r12), %rdi
	movq	1296(%r12), %r14
	movl	$955, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, 544(%r14)
	movq	1296(%r12), %rax
	cmpq	$0, 544(%rax)
	jne	.L56
	cmpq	$0, 1600(%r12)
	je	.L56
	movl	$957, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$558, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L54
.L63:
	movl	$997, %r9d
	jmp	.L93
.L95:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1012:
	.size	final_server_name, .-final_server_name
	.p2align 4
	.type	init_ems, @function
init_ems:
.LFB1023:
	.cfi_startproc
	endbr64
	movl	56(%rdi), %eax
	testl	%eax, %eax
	jne	.L98
	movq	168(%rdi), %rax
	andq	$-513, (%rax)
.L98:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1023:
	.size	init_ems, .-init_ems
	.p2align 4
	.type	final_sig_algs, @function
final_sig_algs:
.LFB1029:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	testl	%edx, %edx
	jne	.L112
	movq	8(%rdi), %rdx
	movq	192(%rdx), %rcx
	testb	$8, 96(%rcx)
	jne	.L112
	movl	(%rdx), %eax
	cmpl	$65536, %eax
	je	.L104
	cmpl	$771, %eax
	jle	.L104
	movl	200(%rdi), %eax
	testl	%eax, %eax
	je	.L115
.L104:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$112, %ecx
	movl	$497, %edx
	movl	$109, %esi
	movl	$1262, %r9d
	leaq	.LC0(%rip), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	%eax, -4(%rbp)
	call	ossl_statem_fatal@PLT
	movl	-4(%rbp), %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1029:
	.size	final_sig_algs, .-final_sig_algs
	.p2align 4
	.type	final_ems, @function
final_ems:
.LFB1024:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movl	56(%rdi), %r12d
	testl	%r12d, %r12d
	jne	.L120
	movl	200(%rdi), %eax
	testl	%eax, %eax
	jne	.L122
.L120:
	movl	$1, %r12d
.L116:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_restore_state
	movq	168(%rdi), %rax
	movq	1296(%rdi), %rdx
	movq	(%rax), %rax
	movl	632(%rdx), %edx
	shrq	$9, %rax
	notl	%edx
	xorq	$1, %rax
	andl	$1, %edx
	andl	$1, %eax
	cmpb	%dl, %al
	je	.L120
	movl	$1186, %r9d
	movl	$104, %ecx
	movl	$486, %edx
	movl	$40, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	jmp	.L116
	.cfi_endproc
.LFE1024:
	.size	final_ems, .-final_ems
	.p2align 4
	.type	final_ec_pt_formats, @function
final_ec_pt_formats:
.LFB1013:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movl	56(%rdi), %r12d
	testl	%r12d, %r12d
	jne	.L134
	cmpq	$0, 1680(%rdi)
	je	.L134
	cmpq	$0, 1672(%rdi)
	je	.L134
	movq	1696(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L134
	movq	1688(%rdi), %rcx
	testq	%rcx, %rcx
	je	.L134
	movq	168(%rdi), %rax
	movq	568(%rax), %rax
	movl	28(%rax), %esi
	movl	32(%rax), %eax
	andl	$4, %esi
	andl	$8, %eax
	orl	%eax, %esi
	je	.L134
	xorl	%eax, %eax
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L137:
	addq	$1, %rax
	cmpq	%rax, %rcx
	je	.L127
.L126:
	cmpb	$0, (%rdx,%rax)
	jne	.L137
	cmpq	%rax, %rcx
	jne	.L134
	.p2align 4,,10
	.p2align 3
.L127:
	movl	$1055, %r9d
	movl	$157, %ecx
	movl	$485, %edx
	movl	$47, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L134:
	movl	$1, %r12d
.L123:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1013:
	.size	final_ec_pt_formats, .-final_ec_pt_formats
	.p2align 4
	.type	final_renegotiate, @function
final_renegotiate:
.LFB1010:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	56(%rdi), %eax
	testl	%eax, %eax
	jne	.L139
	movl	1492(%rdi), %ecx
	andl	$262148, %ecx
	orl	%edx, %ecx
	je	.L145
	movl	$1, %eax
.L138:
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	movl	1928(%rdi), %ecx
	movl	$1, %eax
	testl	%ecx, %ecx
	je	.L138
	movl	1492(%rdi), %ecx
	andl	$262144, %ecx
	orl	%edx, %ecx
	jne	.L138
	movl	$903, %r9d
	movl	$338, %ecx
	movl	$483, %edx
	movl	$40, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	movl	$891, %r9d
	leaq	.LC0(%rip), %r8
	movl	$338, %ecx
	movl	%eax, -4(%rbp)
	movl	$483, %edx
	movl	$40, %esi
	call	ossl_statem_fatal@PLT
	movl	-4(%rbp), %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1010:
	.size	final_renegotiate, .-final_renegotiate
	.p2align 4
	.type	tls_parse_certificate_authorities, @function
tls_parse_certificate_authorities:
.LFB1027:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	parse_ca_names@PLT
	testl	%eax, %eax
	je	.L146
	cmpq	$0, 8(%rbx)
	movl	$1, %eax
	jne	.L153
.L146:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L153:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$1242, %r9d
	movl	$110, %ecx
	movl	$566, %edx
	leaq	.LC0(%rip), %r8
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1027:
	.size	tls_parse_certificate_authorities, .-tls_parse_certificate_authorities
	.p2align 4
	.type	final_early_data, @function
final_early_data:
.LFB1033:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	testl	%edx, %edx
	je	.L171
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	56(%rdi), %r8d
	testl	%r8d, %r8d
	jne	.L156
	cmpl	$1024, %esi
	jne	.L154
	movl	1820(%rdi), %eax
	testl	%eax, %eax
	je	.L174
	movl	$1, %eax
.L154:
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L156:
	.cfi_restore_state
	movl	6176(%rdi), %esi
	testl	%esi, %esi
	je	.L158
	movl	200(%rdi), %ecx
	testl	%ecx, %ecx
	je	.L158
	cmpl	$9, 132(%rdi)
	je	.L175
.L158:
	movl	$1, 1816(%rdi)
	movl	$1, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L174:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	movl	$1643, %r9d
	leaq	.LC0(%rip), %r8
	movl	$233, %ecx
	movl	%eax, -8(%rbp)
	movl	$556, %edx
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
	movl	-8(%rbp), %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L175:
	.cfi_restore_state
	movl	1820(%rdi), %edx
	testl	%edx, %edx
	je	.L158
	movl	1248(%rdi), %eax
	testl	%eax, %eax
	jne	.L158
	movq	6248(%rdi), %rax
	testq	%rax, %rax
	je	.L159
	movq	%rdi, -8(%rbp)
	movq	6256(%rdi), %rsi
	call	*%rax
	movq	-8(%rbp), %rdi
	testl	%eax, %eax
	je	.L158
.L159:
	movl	$2, 1816(%rdi)
	movl	$97, %esi
	call	tls13_change_cipher_state@PLT
	leave
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1033:
	.size	final_early_data, .-final_early_data
	.p2align 4
	.type	final_key_share, @function
final_key_share:
.LFB1030:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	192(%rax), %rcx
	testb	$8, 96(%rcx)
	jne	.L240
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L240
	cmpl	$65536, %eax
	je	.L240
	andl	$2048, %esi
	jne	.L240
	movl	56(%rdi), %ecx
	movl	%edx, %eax
	orl	%ecx, %eax
	jne	.L180
	movl	200(%rdi), %ecx
	testl	%ecx, %ecx
	je	.L181
	testb	$1, 1808(%rdi)
	je	.L181
.L182:
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rdi, -72(%rbp)
	call	tls13_generate_handshake_secret@PLT
	movq	-72(%rbp), %rdi
	testl	%eax, %eax
	je	.L241
	.p2align 4,,10
	.p2align 3
.L240:
	movl	$1, %eax
.L176:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L242
	addq	$48, %rsp
	popq	%rbx
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	.cfi_restore_state
	testl	%ecx, %ecx
	je	.L183
	movq	168(%rdi), %rax
	movl	1248(%rdi), %ecx
	cmpq	$0, 1032(%rax)
	je	.L184
	movq	(%rax), %rax
	testb	$8, %ah
	je	.L185
	movl	1840(%rdi), %eax
	testl	%eax, %eax
	jne	.L185
	testl	%ecx, %ecx
	jne	.L243
.L239:
	movl	$1, 1248(%rdi)
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L185:
	cmpl	$1, %ecx
	jne	.L240
	movl	$2, 1248(%rdi)
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L183:
	testl	%edx, %edx
	jne	.L240
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L181:
	movl	$1296, %r9d
	leaq	.LC0(%rip), %r8
	movl	$101, %ecx
	movl	%eax, -72(%rbp)
	movl	$503, %edx
	movl	$109, %esi
	call	ossl_statem_fatal@PLT
	movl	-72(%rbp), %eax
	jmp	.L176
.L241:
	movl	%eax, -72(%rbp)
	movl	$1425, %r9d
.L237:
	movl	$68, %ecx
	movl	$503, %edx
	movl	$80, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	movl	-72(%rbp), %eax
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L184:
	movl	200(%rdi), %esi
	testl	%ecx, %ecx
	jne	.L187
	testl	%edx, %edx
	je	.L188
	testl	%esi, %esi
	je	.L189
	movl	1808(%rdi), %eax
	testb	$2, %al
	jne	.L189
.L190:
	testb	$1, %al
	je	.L199
	movq	168(%rdi), %rax
	movl	1248(%rdi), %ecx
.L196:
	movq	(%rax), %rax
	testb	$8, %ah
	je	.L185
	movl	1840(%rdi), %eax
	testl	%eax, %eax
	movl	%eax, -72(%rbp)
	jne	.L185
	movl	$1403, %r9d
	testl	%ecx, %ecx
	je	.L239
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L187:
	testl	%esi, %esi
	je	.L195
	testb	$1, 1808(%rdi)
	jne	.L196
.L195:
	movl	$109, %esi
	testl	%edx, %edx
	je	.L197
.L199:
	movl	$40, %esi
.L197:
	movl	$1389, %r9d
	movl	$101, %ecx
	movl	$503, %edx
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L176
.L188:
	testl	%esi, %esi
	je	.L204
	testb	$1, 1808(%rdi)
	je	.L204
	movq	(%rax), %rax
	testb	$8, %ah
	je	.L240
	movl	1840(%rdi), %eax
	testl	%eax, %eax
	je	.L239
	jmp	.L240
.L243:
	movl	%eax, -72(%rbp)
	movl	$1345, %r9d
	jmp	.L237
.L189:
	leaq	-48(%rbp), %rdx
	leaq	-56(%rbp), %rsi
	movq	%rdi, -72(%rbp)
	movq	1720(%rdi), %r14
	movq	1728(%rdi), %r15
	call	tls1_get_supported_groups@PLT
	cmpq	$0, -48(%rbp)
	movq	-72(%rbp), %rdi
	je	.L194
	xorl	%ebx, %ebx
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L244:
	addq	$1, %rbx
	cmpq	-48(%rbp), %rbx
	jnb	.L194
.L193:
	movq	-56(%rbp), %rax
	movl	$1, %r8d
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%rdi, -72(%rbp)
	movzwl	(%rax,%rbx,2), %esi
	movl	%esi, %r13d
	call	check_in_list@PLT
	movq	-72(%rbp), %rdi
	testl	%eax, %eax
	je	.L244
	cmpq	%rbx, -48(%rbp)
	jbe	.L194
	movq	168(%rdi), %rax
	movw	%r13w, 1030(%rax)
	jmp	.L239
.L204:
	movl	$109, %esi
	jmp	.L197
.L194:
	movl	200(%rdi), %edx
	testl	%edx, %edx
	je	.L199
	movl	1808(%rdi), %eax
	jmp	.L190
.L242:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1030:
	.size	final_key_share, .-final_key_share
	.p2align 4
	.type	init_status_request, @function
init_status_request:
.LFB1015:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	56(%rdi), %eax
	testl	%eax, %eax
	je	.L246
	movl	$-1, 1608(%rdi)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L246:
	.cfi_restore_state
	movq	1648(%rdi), %rdi
	movl	$1083, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$1, %eax
	movq	$0, 1648(%rbx)
	movq	$0, 1656(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1015:
	.size	init_status_request, .-init_status_request
	.p2align 4
	.type	init_server_name, @function
init_server_name:
.LFB1011:
	.cfi_startproc
	endbr64
	movl	56(%rdi), %eax
	testl	%eax, %eax
	jne	.L255
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$917, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	$0, 1864(%rdi)
	movq	1600(%rdi), %rdi
	call	CRYPTO_free@PLT
	movl	$1, %eax
	movq	$0, 1600(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1011:
	.size	init_server_name, .-init_server_name
	.p2align 4
	.type	final_maxfragmentlen, @function
final_maxfragmentlen:
.LFB1034:
	.cfi_startproc
	endbr64
	movl	56(%rdi), %esi
	movq	1296(%rdi), %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%esi, %esi
	je	.L257
	movl	200(%rdi), %eax
	testl	%eax, %eax
	je	.L257
	movzbl	600(%rcx), %ecx
	leal	-1(%rcx), %esi
	testl	%edx, %edx
	jne	.L258
	cmpb	$3, %sil
	ja	.L258
	movl	$1681, %r9d
	movl	$110, %ecx
	movl	$557, %edx
	movl	$109, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L256:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L257:
	.cfi_restore_state
	movl	$1, %eax
	testq	%rcx, %rcx
	je	.L256
	movzbl	600(%rcx), %ecx
	leal	-1(%rcx), %esi
.L258:
	movl	$1, %eax
	cmpb	$3, %sil
	ja	.L256
	subl	$1, %ecx
	movl	$512, %edx
	sall	%cl, %edx
	cmpq	%rdx, 1536(%rdi)
	jnb	.L256
	call	ssl3_setup_buffers@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1034:
	.size	final_maxfragmentlen, .-final_maxfragmentlen
	.p2align 4
	.globl	tls_validate_all_contexts
	.type	tls_validate_all_contexts, @function
tls_validate_all_contexts:
.LFB1002:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	andl	$128, %esi
	jne	.L284
	testl	$256, %r13d
	movl	$2, %eax
	cmove	%eax, %esi
.L272:
	movq	1168(%r8), %rax
	movq	480(%rax), %r15
	addq	$26, %r15
	je	.L273
	leaq	4+ext_defs(%rip), %r14
	xorl	%r12d, %r12d
	leaq	-64(%rbp), %rcx
.L275:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	je	.L282
	cmpq	$25, %r12
	ja	.L276
	movl	(%r14), %eax
	testl	%r13d, %eax
	je	.L280
.L297:
	movq	8(%r8), %rdx
	movq	192(%rdx), %rdx
	testb	$8, 96(%rdx)
	je	.L281
	testb	$1, %al
	jne	.L280
.L282:
	addq	$1, %r12
	addq	$40, %rbx
	addq	$56, %r14
	cmpq	%r12, %r15
	jne	.L275
.L273:
	movl	$1, %eax
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L276:
	movq	1168(%r8), %rax
	movl	24(%rbx), %edx
	movq	%r8, -88(%rbp)
	movq	%rcx, -80(%rbp)
	leaq	472(%rax), %rdi
	movl	%esi, -68(%rbp)
	call	custom_ext_find@PLT
	movl	-68(%rbp), %esi
	movq	-80(%rbp), %rcx
	testq	%rax, %rax
	movq	-88(%rbp), %r8
	je	.L280
	movl	8(%rax), %eax
	testl	%r13d, %eax
	jne	.L297
.L280:
	xorl	%eax, %eax
.L271:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L298
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L281:
	.cfi_restore_state
	testb	$2, %al
	je	.L282
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L284:
	movl	$1, %esi
	jmp	.L272
.L298:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1002:
	.size	tls_validate_all_contexts, .-tls_validate_all_contexts
	.p2align 4
	.globl	extension_is_relevant
	.type	extension_is_relevant, @function
extension_is_relevant:
.LFB1004:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	movq	192(%rcx), %rax
	movl	96(%rax), %eax
	andl	$8, %eax
	testb	$8, %dh
	jne	.L300
	testl	%eax, %eax
	jne	.L301
	movl	(%rcx), %eax
	cmpl	$771, %eax
	jle	.L339
	cmpl	$65536, %eax
	je	.L339
	cmpl	$768, (%rdi)
	jne	.L307
	xorl	%eax, %eax
	testb	$8, %sil
	jne	.L307
	ret
	.p2align 4,,10
	.p2align 3
.L300:
	testl	%eax, %eax
	jne	.L340
.L305:
	cmpl	$768, (%rdi)
	movl	$1, %ecx
	je	.L311
.L307:
	xorl	%eax, %eax
	testb	$16, %sil
	jne	.L341
.L309:
	movl	200(%rdi), %ecx
	movl	$1, %eax
	testl	%ecx, %ecx
	je	.L299
	shrl	$6, %esi
	xorl	$1, %esi
	movl	%esi, %eax
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L340:
	xorl	%eax, %eax
	testb	$4, %sil
	je	.L305
.L299:
	ret
	.p2align 4,,10
	.p2align 3
.L301:
	xorl	%eax, %eax
	testb	$4, %sil
	jne	.L299
.L339:
	cmpl	$768, (%rdi)
	je	.L342
.L308:
	testb	$32, %sil
	je	.L309
	xorl	%eax, %eax
	andl	$128, %edx
	je	.L343
	movl	56(%rdi), %edx
	testl	%edx, %edx
	je	.L309
	ret
	.p2align 4,,10
	.p2align 3
.L342:
	xorl	%ecx, %ecx
.L311:
	xorl	%eax, %eax
	testb	$8, %sil
	je	.L299
	testl	%ecx, %ecx
	je	.L308
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L341:
	ret
	.p2align 4,,10
	.p2align 3
.L343:
	ret
	.cfi_endproc
.LFE1004:
	.size	extension_is_relevant, .-extension_is_relevant
	.p2align 4
	.globl	tls_collect_extensions
	.type	tls_collect_extensions, @function
tls_collect_extensions:
.LFB1005:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r9d, -80(%rbp)
	movq	8(%rsi), %rbx
	movq	1168(%rdi), %r12
	movq	(%rsi), %r9
	movq	%rcx, -104(%rbp)
	movq	%r8, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	472(%r12), %rax
	movq	$0, (%rcx)
	movq	%rax, -88(%rbp)
	movl	%edx, %eax
	andl	$128, %eax
	movl	%eax, -76(%rbp)
	jne	.L432
.L345:
	movl	$573, %edx
	movq	480(%r12), %r15
	leaq	.LC0(%rip), %rsi
	movq	%r9, -72(%rbp)
	addq	$26, %r15
	leaq	(%r15,%r15,4), %rdi
	salq	$3, %rdi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L346
	testq	%rbx, %rbx
	movq	-72(%rbp), %r9
	je	.L373
	cmpq	$3, %rbx
	jbe	.L350
	movl	%r13d, %eax
	movzwl	(%r9), %r12d
	movq	%r15, -128(%rbp)
	movl	%r13d, %r11d
	andl	$256, %eax
	movq	$0, -72(%rbp)
	movl	%eax, -116(%rbp)
	leaq	-64(%rbp), %rax
	rolw	$8, %r12w
	movq	%rax, -112(%rbp)
	movzwl	%r12w, %r12d
.L351:
	movzwl	2(%r9), %r15d
	subq	$4, %rbx
	rolw	$8, %r15w
	movzwl	%r15w, %r13d
	cmpq	%r13, %rbx
	jb	.L350
	subq	%r13, %rbx
	xorl	%edx, %edx
	leaq	ext_defs(%rip), %rax
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L353:
	addq	$56, %rax
	leaq	1456+ext_defs(%rip), %rdi
	addq	$1, %rdx
	cmpq	%rdi, %rax
	je	.L433
.L360:
	cmpl	%r12d, (%rax)
	jne	.L353
	movl	4(%rax), %eax
	testl	%eax, %r11d
	je	.L355
	movq	8(%r14), %rcx
	movq	192(%rcx), %rcx
	testb	$8, 96(%rcx)
	je	.L356
	testb	$1, %al
	je	.L358
.L355:
	movl	$602, %r9d
	leaq	.LC0(%rip), %r8
	movl	$110, %ecx
	movq	%r14, %rdi
	movl	$435, %edx
	movl	$47, %esi
	movq	%r10, -72(%rbp)
	call	ossl_statem_fatal@PLT
	movq	-72(%rbp), %r10
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L356:
	testb	$2, %al
	jne	.L355
.L358:
	leaq	(%rdx,%rdx,4), %rax
	leaq	(%r10,%rax,8), %rax
.L359:
	cmpl	$1, 16(%rax)
	je	.L355
.L366:
	movl	-76(%rbp), %ecx
	testl	%ecx, %ecx
	setne	%cl
	cmpl	$41, %r12d
	sete	%dl
	testb	%dl, %cl
	je	.L369
	testq	%rbx, %rbx
	jne	.L355
.L369:
	movabsq	$-3689348814741910323, %rcx
	movq	%rax, %rdx
	subq	%r10, %rdx
	sarq	$3, %rdx
	imulq	%rcx, %rdx
	cmpl	$25, %edx
	ja	.L370
	testl	$24704, %r11d
	sete	%cl
	cmpl	$44, %r12d
	setne	%sil
	andl	%esi, %ecx
	cmpl	$65281, %r12d
	setne	%sil
	testb	%sil, %cl
	je	.L370
	cmpl	$18, %r12d
	jne	.L434
.L370:
	leaq	4(%r9), %rcx
	testq	%rax, %rax
	je	.L371
	movq	-72(%rbp), %rsi
	movq	%rcx, (%rax)
	movq	%r13, 8(%rax)
	movq	%rsi, 32(%rax)
	addq	$1, %rsi
	movl	$1, 16(%rax)
	movl	%r12d, 24(%rax)
	movq	1584(%r14), %rax
	movq	%rsi, -72(%rbp)
	testq	%rax, %rax
	je	.L371
	movl	56(%r14), %edx
	xorl	%esi, %esi
	movl	%r11d, -120(%rbp)
	movzwl	%r15w, %r8d
	movq	%r10, -144(%rbp)
	movq	1592(%r14), %r9
	movq	%r14, %rdi
	testl	%edx, %edx
	movq	%rcx, -136(%rbp)
	movl	%r12d, %edx
	sete	%sil
	call	*%rax
	movl	-120(%rbp), %r11d
	movq	-144(%rbp), %r10
	movq	-136(%rbp), %rcx
.L371:
	testq	%rbx, %rbx
	je	.L435
	cmpq	$1, %rbx
	jbe	.L350
	leaq	(%rcx,%r13), %r9
	leaq	-2(%rbx), %rax
	movzwl	(%r9), %r12d
	rolw	$8, %r12w
	movzwl	%r12w, %r12d
	cmpq	$1, %rax
	ja	.L351
	.p2align 4,,10
	.p2align 3
.L350:
	movl	$588, %r9d
	leaq	.LC0(%rip), %r8
	movl	$110, %ecx
	movq	%r14, %rdi
	movl	$435, %edx
	movl	$50, %esi
	movq	%r10, -72(%rbp)
	call	ossl_statem_fatal@PLT
	movq	-72(%rbp), %r10
.L367:
	movl	$673, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r10, %rdi
	call	CRYPTO_free@PLT
	xorl	%eax, %eax
.L344:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L436
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L432:
	.cfi_restore_state
	leaq	472(%r12), %rdi
	movq	%r9, -72(%rbp)
	call	custom_ext_init@PLT
	movq	-72(%rbp), %r9
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L433:
	movl	-76(%rbp), %r8d
	movq	$0, -64(%rbp)
	movl	$1, %esi
	testl	%r8d, %r8d
	jne	.L361
	movl	-116(%rbp), %edi
	xorl	%esi, %esi
	testl	%edi, %edi
	sete	%sil
	addl	%esi, %esi
.L361:
	movq	-112(%rbp), %rcx
	movq	-88(%rbp), %rdi
	movl	%r12d, %edx
	movl	%r11d, -120(%rbp)
	movq	%r9, -144(%rbp)
	movq	%r10, -136(%rbp)
	call	custom_ext_find@PLT
	movq	-136(%rbp), %r10
	movq	-144(%rbp), %r9
	testq	%rax, %rax
	movl	-120(%rbp), %r11d
	je	.L366
	movl	8(%rax), %eax
	testl	%eax, %r11d
	je	.L355
	movq	8(%r14), %rdx
	movq	192(%rdx), %rdx
	testb	$8, 96(%rdx)
	jne	.L437
	testb	$2, %al
	jne	.L355
.L365:
	movq	-64(%rbp), %rax
	leaq	130(%rax,%rax,4), %rax
	leaq	(%r10,%rax,8), %rax
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L346:
	movl	$575, %r9d
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
	movq	%r14, %rdi
	movl	$435, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L437:
	testb	$1, %al
	jne	.L355
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L434:
	movl	%edx, %edx
	testb	$2, 1552(%r14,%rdx)
	jne	.L370
	movl	$634, %r9d
	leaq	.LC0(%rip), %r8
	movl	$217, %ecx
	movq	%r14, %rdi
	movl	$435, %edx
	movl	$110, %esi
	movq	%r10, -72(%rbp)
	call	ossl_statem_fatal@PLT
	movq	-72(%rbp), %r10
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L435:
	movq	-128(%rbp), %r15
	movl	%r11d, %r13d
.L373:
	movl	-80(%rbp), %r9d
	testl	%r9d, %r9d
	je	.L349
	leaq	ext_defs(%rip), %r12
	movq	%r15, -72(%rbp)
	movq	%r10, %r15
	leaq	1456(%r12), %rbx
	.p2align 4,,10
	.p2align 3
.L348:
	addq	$56, %r12
	cmpq	%rbx, %r12
	je	.L438
	movq	8(%r12), %r8
	testq	%r8, %r8
	je	.L348
	movl	4(%r12), %esi
	testl	%r13d, %esi
	je	.L348
	movl	%r13d, %edx
	movq	%r14, %rdi
	call	extension_is_relevant
	testl	%eax, %eax
	je	.L348
	movl	%r13d, %esi
	call	*%r8
	testl	%eax, %eax
	jne	.L348
	movq	%r15, %r10
	jmp	.L367
.L438:
	movq	%r15, %r10
	movq	-72(%rbp), %r15
.L349:
	movq	-104(%rbp), %rax
	cmpq	$0, -96(%rbp)
	movq	%r10, (%rax)
	je	.L431
	movq	-96(%rbp), %rax
	movq	%r15, (%rax)
.L431:
	movl	$1, %eax
	jmp	.L344
.L436:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1005:
	.size	tls_collect_extensions, .-tls_collect_extensions
	.p2align 4
	.globl	tls_parse_extension
	.type	tls_parse_extension, @function
tls_parse_extension:
.LFB1006:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	movl	%edx, %r11d
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rcx,%rdx,8), %r10
	movl	16(%r10), %edx
	testl	%edx, %edx
	je	.L492
	movl	20(%r10), %edx
	testl	%edx, %edx
	jne	.L492
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	$1, 20(%r10)
	cmpl	$25, %esi
	ja	.L443
	movq	8(%rdi), %rbx
	leaq	0(,%rax,8), %rcx
	leaq	ext_defs(%rip), %r8
	subq	%rax, %rcx
	movl	4(%r8,%rcx,8), %esi
	movq	192(%rbx), %rcx
	movl	96(%rcx), %ecx
	andl	$8, %ecx
	testl	$2048, %r11d
	je	.L496
	testl	%ecx, %ecx
	je	.L449
	testb	$4, %sil
	jne	.L442
.L449:
	cmpl	$768, (%rdi)
	je	.L497
.L450:
	testb	$16, %sil
	jne	.L442
	movl	200(%rdi), %ebx
	testl	%ebx, %ebx
	jne	.L459
.L455:
	leaq	0(,%rax,8), %rdx
	subq	%rax, %rdx
	leaq	(%r8,%rdx,8), %rdx
	movl	56(%rdi), %r8d
	movq	24(%rdx), %rax
	testl	%r8d, %r8d
	cmovne	16(%rdx), %rax
.L456:
	testq	%rax, %rax
	je	.L443
	leaq	-16(%rbp), %rsp
	movq	%r12, %rcx
	movq	%r9, %r8
	movl	%r11d, %edx
	popq	%rbx
	.cfi_restore 3
	movq	%r10, %rsi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L492:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L443:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	subq	$8, %rsp
	movl	24(%r10), %edx
	movl	%r11d, %esi
	pushq	%r9
	movq	(%r10), %rcx
	movq	%r12, %r9
	movq	8(%r10), %r8
	call	custom_ext_parse@PLT
	popq	%rcx
	popq	%rsi
	leaq	-16(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L496:
	.cfi_restore_state
	testl	%ecx, %ecx
	jne	.L445
	movl	(%rbx), %ecx
	cmpl	$771, %ecx
	jle	.L448
	cmpl	$65536, %ecx
	je	.L448
	cmpl	$768, (%rdi)
	jne	.L450
	testb	$8, %sil
	jne	.L450
.L442:
	leaq	-16(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L459:
	.cfi_restore_state
	andl	$64, %esi
	je	.L455
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L445:
	testb	$4, %sil
	jne	.L442
.L448:
	cmpl	$768, (%rdi)
	je	.L464
.L451:
	testb	$32, %sil
	je	.L453
	testb	$-128, %r11b
	je	.L442
	movl	56(%rdi), %edx
	testl	%edx, %edx
	jne	.L442
.L458:
	movl	200(%rdi), %ebx
	testl	%ebx, %ebx
	jne	.L459
	leaq	0(,%rax,8), %rdx
	subq	%rax, %rdx
	movq	24(%r8,%rdx,8), %rax
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L497:
	movl	$1, %edx
.L464:
	testb	$8, %sil
	je	.L442
	testl	%edx, %edx
	je	.L451
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L453:
	movl	56(%rdi), %edx
	testl	%edx, %edx
	je	.L458
	movl	200(%rdi), %ecx
	testl	%ecx, %ecx
	je	.L490
	andl	$64, %esi
	jne	.L442
.L490:
	leaq	0(,%rax,8), %rdx
	subq	%rax, %rdx
	movq	16(%r8,%rdx,8), %rax
	jmp	.L456
	.cfi_endproc
.LFE1006:
	.size	tls_parse_extension, .-tls_parse_extension
	.p2align 4
	.globl	tls_parse_all_extensions
	.type	tls_parse_all_extensions, @function
tls_parse_all_extensions:
.LFB1007:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	1168(%rdi), %rax
	movq	%rcx, -56(%rbp)
	movq	%r8, -64(%rbp)
	movq	480(%rax), %r13
	movl	%r9d, -68(%rbp)
	addq	$26, %r13
	je	.L503
	xorl	%ebx, %ebx
	movq	%rbx, %r14
	movq	%rdi, %rbx
	jmp	.L499
	.p2align 4,,10
	.p2align 3
.L532:
	leaq	0(,%rax,8), %rdx
	leaq	ext_defs(%rip), %rcx
	movq	%rbx, %rdi
	subq	%rax, %rdx
	leaq	(%rcx,%rdx,8), %r8
	movl	%r15d, %edx
	movl	4(%r8), %esi
	call	extension_is_relevant
	testl	%eax, %eax
	je	.L504
	movl	56(%rbx), %esi
	movq	24(%r8), %rax
	testl	%esi, %esi
	cmovne	16(%r8), %rax
	testq	%rax, %rax
	je	.L505
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rcx
	movl	%r15d, %edx
	movq	%r11, %rsi
	call	*%rax
	testl	%eax, %eax
	je	.L512
	.p2align 4,,10
	.p2align 3
.L504:
	addq	$1, %r14
	cmpq	%r14, %r13
	je	.L531
.L499:
	movl	%r14d, %eax
	leaq	(%rax,%rax,4), %rdx
	leaq	(%r12,%rdx,8), %r11
	movl	16(%r11), %r8d
	testl	%r8d, %r8d
	je	.L504
	movl	20(%r11), %edi
	testl	%edi, %edi
	jne	.L504
	movl	$1, 20(%r11)
	cmpl	$25, %r14d
	jbe	.L532
.L505:
	subq	$8, %rsp
	movl	24(%r11), %edx
	pushq	-64(%rbp)
	movl	%r15d, %esi
	movq	(%r11), %rcx
	movq	%rbx, %rdi
	movq	-56(%rbp), %r9
	movq	8(%r11), %r8
	call	custom_ext_parse@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	jne	.L504
.L512:
	leaq	-40(%rbp), %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L531:
	.cfi_restore_state
	movq	%rbx, %r14
.L503:
	movl	-68(%rbp), %r9d
	testl	%r9d, %r9d
	je	.L501
	leaq	ext_defs(%rip), %rbx
	addq	$16, %r12
	leaq	1456(%rbx), %r13
	.p2align 4,,10
	.p2align 3
.L510:
	movq	48(%rbx), %rax
	testq	%rax, %rax
	je	.L511
	testl	%r15d, 4(%rbx)
	je	.L511
	movl	(%r12), %edx
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L512
.L511:
	addq	$56, %rbx
	addq	$40, %r12
	cmpq	%r13, %rbx
	jne	.L510
.L501:
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1007:
	.size	tls_parse_all_extensions, .-tls_parse_all_extensions
	.p2align 4
	.globl	should_add_extension
	.type	should_add_extension, @function
should_add_extension:
.LFB1008:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testl	%edx, %esi
	je	.L533
	movq	8(%rdi), %r9
	movl	%edx, %r10d
	movq	192(%r9), %r8
	movl	96(%r8), %r8d
	andl	$8, %r8d
	andl	$2048, %r10d
	je	.L585
	testl	%r8d, %r8d
	je	.L540
	testb	$4, %sil
	jne	.L533
.L540:
	cmpl	$768, (%rdi)
	movl	$1, %r9d
	je	.L550
.L541:
	xorl	%eax, %eax
	testb	$16, %sil
	jne	.L586
	movl	200(%rdi), %r10d
	testl	%r10d, %r10d
	jne	.L548
.L546:
	andl	$32, %esi
	movl	$1, %eax
	je	.L533
.L549:
	andl	$128, %edx
	movl	$1, %eax
	je	.L533
	cmpl	$771, %ecx
	setle	%al
	testl	%r8d, %r8d
	setne	%dl
	orl	%edx, %eax
	xorl	$1, %eax
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L585:
	testl	%r8d, %r8d
	je	.L587
	movl	%r10d, %eax
	testb	$4, %sil
	jne	.L533
.L539:
	cmpl	$768, (%rdi)
	je	.L588
.L542:
	testb	$32, %sil
	je	.L547
	xorl	%eax, %eax
	testb	$-128, %dl
	je	.L589
	movl	56(%rdi), %r9d
	testl	%r9d, %r9d
	je	.L590
.L533:
	ret
	.p2align 4,,10
	.p2align 3
.L587:
	movl	(%r9), %eax
	cmpl	$771, %eax
	jle	.L539
	cmpl	$65536, %eax
	je	.L539
	cmpl	$768, (%rdi)
	jne	.L541
	movl	%r8d, %eax
	testb	$8, %sil
	jne	.L541
	ret
	.p2align 4,,10
	.p2align 3
.L590:
	movl	200(%rdi), %edi
	testl	%edi, %edi
	je	.L549
	.p2align 4,,10
	.p2align 3
.L548:
	xorl	%eax, %eax
	testb	$64, %sil
	je	.L546
	ret
	.p2align 4,,10
	.p2align 3
.L588:
	xorl	%r9d, %r9d
.L550:
	xorl	%eax, %eax
	testb	$8, %sil
	je	.L533
	testl	%r9d, %r9d
	je	.L542
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L586:
	ret
	.p2align 4,,10
	.p2align 3
.L547:
	movl	200(%rdi), %eax
	testl	%eax, %eax
	jne	.L548
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L589:
	ret
	.cfi_endproc
.LFE1008:
	.size	should_add_extension, .-should_add_extension
	.p2align 4
	.globl	tls_construct_extensions
	.type	tls_construct_extensions, @function
tls_construct_extensions:
.LFB1009:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$2, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -60(%rbp)
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L595
	testl	$384, %r14d
	jne	.L673
.L594:
	movl	%r14d, %eax
	andl	$128, %eax
	movl	%eax, -72(%rbp)
	jne	.L674
.L596:
	movl	-60(%rbp), %r9d
	movq	-88(%rbp), %rcx
	movq	%r15, %r8
	movq	%r13, %rdx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	custom_ext_add@PLT
	testl	%eax, %eax
	je	.L671
	movl	%r14d, %eax
	leaq	ext_defs(%rip), %rbx
	xorl	%r9d, %r9d
	movq	%r15, %r8
	andl	$2048, %eax
	movl	%r14d, %r15d
	leaq	1456(%rbx), %r11
	movl	%eax, -68(%rbp)
	movl	%r14d, %eax
	movq	%r9, %r14
	andl	$24704, %eax
	movl	%eax, -92(%rbp)
	jmp	.L620
	.p2align 4,,10
	.p2align 3
.L678:
	testl	%ecx, %ecx
	jne	.L602
	movl	(%rsi), %esi
	cmpl	$65536, %esi
	je	.L605
	cmpl	$771, %esi
	jle	.L605
	cmpl	$768, (%r12)
	je	.L675
	.p2align 4,,10
	.p2align 3
.L608:
	testb	$16, %al
	jne	.L600
.L610:
	movl	200(%r12), %edx
	testl	%edx, %edx
	je	.L614
	testb	$64, %al
	jne	.L600
.L614:
	testb	$32, %al
	je	.L618
	movl	-72(%rbp), %eax
	testl	%eax, %eax
	jne	.L615
.L618:
	movl	56(%r12), %r10d
	movq	40(%rbx), %rax
	testl	%r10d, %r10d
	cmovne	32(%rbx), %rax
.L617:
	testq	%rax, %rax
	je	.L600
	movq	%r8, -80(%rbp)
	movq	-88(%rbp), %rcx
	movl	%r15d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L671
	cmpl	$1, %eax
	movq	-80(%rbp), %r8
	leaq	1456+ext_defs(%rip), %r11
	je	.L676
	.p2align 4,,10
	.p2align 3
.L600:
	addq	$56, %rbx
	addq	$1, %r14
	cmpq	%r11, %rbx
	je	.L677
.L620:
	movl	4(%rbx), %eax
	testl	%r15d, %eax
	je	.L600
	movq	8(%r12), %rsi
	movl	-68(%rbp), %r9d
	movq	192(%rsi), %rcx
	movl	96(%rcx), %ecx
	andl	$8, %ecx
	testl	%r9d, %r9d
	je	.L678
	testl	%ecx, %ecx
	je	.L606
	testb	$4, %al
	jne	.L600
.L606:
	cmpl	$768, (%r12)
	movl	$1, %esi
	jne	.L608
.L623:
	testb	$8, %al
	je	.L600
	testl	%esi, %esi
	jne	.L608
.L609:
	movl	%eax, %esi
	andl	$32, %esi
	je	.L612
	movl	-72(%rbp), %edi
	testl	%edi, %edi
	je	.L600
.L612:
	movl	56(%r12), %edi
	testl	%edi, %edi
	je	.L610
	testl	%esi, %esi
	jne	.L600
	movl	200(%r12), %edx
	testl	%edx, %edx
	jne	.L622
	movq	32(%rbx), %rax
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L673:
	movl	$2, %esi
	movq	%r13, %rdi
	call	WPACKET_set_flags@PLT
	testl	%eax, %eax
	jne	.L594
.L595:
	movl	$814, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
.L670:
	movl	$447, %edx
	movl	$80, %esi
	movq	%r12, %rdi
	call	ossl_statem_fatal@PLT
.L671:
	xorl	%eax, %eax
.L591:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L679
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L674:
	.cfi_restore_state
	xorl	%ecx, %ecx
	leaq	-60(%rbp), %rdx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	call	ssl_get_min_max_version@PLT
	testl	%eax, %eax
	jne	.L680
	movq	1168(%r12), %rax
	leaq	472(%rax), %rdi
	call	custom_ext_init@PLT
	jmp	.L596
	.p2align 4,,10
	.p2align 3
.L602:
	testb	$4, %al
	jne	.L600
.L605:
	cmpl	$768, (%r12)
	jne	.L609
	xorl	%esi, %esi
	jmp	.L623
	.p2align 4,,10
	.p2align 3
.L676:
	movl	-92(%rbp), %r9d
	testl	%r9d, %r9d
	je	.L600
	orb	$2, 1552(%r12,%r14)
	addq	$56, %rbx
	addq	$1, %r14
	cmpq	%r11, %rbx
	jne	.L620
	.p2align 4,,10
	.p2align 3
.L677:
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L681
	movl	$1, %eax
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L615:
	cmpl	$771, -60(%rbp)
	jle	.L600
	testl	%ecx, %ecx
	je	.L618
	jmp	.L600
	.p2align 4,,10
	.p2align 3
.L622:
	testb	$64, %al
	je	.L618
	jmp	.L600
	.p2align 4,,10
	.p2align 3
.L675:
	testb	$8, %al
	jne	.L608
	jmp	.L600
.L681:
	movl	$68, %ecx
	movl	$447, %edx
	movq	%r12, %rdi
	movl	%eax, -68(%rbp)
	movl	$866, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movl	-68(%rbp), %eax
	jmp	.L591
.L680:
	movl	$822, %r9d
	leaq	.LC0(%rip), %r8
	movl	%eax, %ecx
	jmp	.L670
.L679:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1009:
	.size	tls_construct_extensions, .-tls_construct_extensions
	.p2align 4
	.globl	tls_psk_do_binder
	.type	tls_psk_do_binder, @function
tls_psk_do_binder:
.LFB1032:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	subq	$360, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -344(%rbp)
	movq	16(%rbp), %r15
	movq	%rcx, -352(%rbp)
	movq	%r8, -368(%rbp)
	movq	%r9, -360(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_MD_size@PLT
	movslq	%eax, %rbx
	testl	%ebx, %ebx
	js	.L721
	movl	32(%rbp), %edi
	testl	%edi, %edi
	jne	.L722
	leaq	resumption_label.24462(%rip), %rax
	movq	%rax, -376(%rbp)
.L685:
	leaq	316(%r12), %r14
.L688:
	movq	8(%r15), %r8
	xorl	%edx, %edx
	leaq	80(%r15), %rcx
	movq	%r14, %r9
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	tls13_generate_secret@PLT
	testl	%eax, %eax
	je	.L706
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L690
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	jle	.L690
	leaq	-320(%rbp), %rax
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, -384(%rbp)
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	jle	.L690
	pushq	$1
	movq	%rbx, %rdx
	movq	-384(%rbp), %r9
	movq	%r13, %rsi
	pushq	%rbx
	leaq	-256(%rbp), %rbx
	movq	-376(%rbp), %rcx
	movq	%r12, %rdi
	pushq	%rbx
	movl	$10, %r8d
	pushq	%rdx
	movq	%rdx, -392(%rbp)
	movq	%r14, %rdx
	call	tls13_hkdf_expand@PLT
	addq	$32, %rsp
	testl	%eax, %eax
	je	.L707
	leaq	-192(%rbp), %r14
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-392(%rbp), %r8
	movq	%r14, %rcx
	call	tls13_derive_finishedkey@PLT
	testl	%eax, %eax
	jne	.L723
.L708:
	movl	$-1, %r13d
	xorl	%r10d, %r10d
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L722:
	cmpl	$2, 132(%r12)
	movl	56(%r12), %eax
	je	.L724
.L686:
	testl	%eax, %eax
	jne	.L710
.L687:
	leaq	external_label.24463(%rip), %rax
	leaq	16(%r15), %r14
	movq	%rax, -376(%rbp)
	jmp	.L688
	.p2align 4,,10
	.p2align 3
.L690:
	movl	$1513, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$506, %edx
	movl	$80, %esi
	movl	$-1, %r13d
	call	ossl_statem_fatal@PLT
	leaq	-256(%rbp), %rbx
	xorl	%r10d, %r10d
	leaq	-192(%rbp), %r14
.L684:
	movl	$64, %esi
	movq	%rbx, %rdi
	movq	%r10, -344(%rbp)
	call	OPENSSL_cleanse@PLT
	movl	$64, %esi
	movq	%r14, %rdi
	call	OPENSSL_cleanse@PLT
	movq	-344(%rbp), %r10
	movq	%r10, %rdi
	call	EVP_PKEY_free@PLT
	movq	%r15, %rdi
	call	EVP_MD_CTX_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L725
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L721:
	.cfi_restore_state
	movl	$1466, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$506, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
.L706:
	movl	$-1, %r13d
	xorl	%r15d, %r15d
	xorl	%r10d, %r10d
	leaq	-256(%rbp), %rbx
	leaq	-192(%rbp), %r14
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L724:
	movq	1296(%r12), %rcx
	movl	580(%rcx), %esi
	testl	%esi, %esi
	jne	.L686
	orl	580(%r15), %eax
	je	.L687
	.p2align 4,,10
	.p2align 3
.L710:
	leaq	external_label.24463(%rip), %rax
	movq	%rax, -376(%rbp)
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L707:
	movl	$-1, %r13d
	xorl	%r10d, %r10d
	leaq	-192(%rbp), %r14
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L723:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	EVP_DigestInit_ex@PLT
	movl	$1532, %r9d
	testl	%eax, %eax
	jle	.L719
	cmpl	$1, 1248(%r12)
	je	.L726
.L692:
	movq	-352(%rbp), %rdx
	movq	-344(%rbp), %rsi
	movq	%r15, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jle	.L699
	movq	-384(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	jle	.L699
	movq	-392(%rbp), %rcx
	movq	%r14, %rdx
	xorl	%esi, %esi
	movl	$855, %edi
	call	EVP_PKEY_new_raw_private_key@PLT
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L727
	movl	24(%rbp), %edx
	leaq	-128(%rbp), %rax
	movq	%r10, %r8
	movq	%r15, %rdi
	movq	%r10, -344(%rbp)
	testl	%edx, %edx
	cmovne	-360(%rbp), %rax
	movq	%r13, %rdx
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%rax, -360(%rbp)
	movq	-392(%rbp), %rax
	movq	%rax, -328(%rbp)
	call	EVP_DigestSignInit@PLT
	movq	-344(%rbp), %r10
	testl	%eax, %eax
	jle	.L703
	movq	-392(%rbp), %r13
	movq	-384(%rbp), %rsi
	movq	%r15, %rdi
	movq	%r10, -344(%rbp)
	movq	%r13, %rdx
	call	EVP_DigestUpdate@PLT
	movq	-344(%rbp), %r10
	testl	%eax, %eax
	jle	.L703
	movq	-360(%rbp), %rsi
	leaq	-328(%rbp), %rdx
	movq	%r15, %rdi
	call	EVP_DigestSignFinal@PLT
	movq	-344(%rbp), %r10
	testl	%eax, %eax
	jle	.L703
	cmpq	%r13, -328(%rbp)
	jne	.L703
	movl	24(%rbp), %eax
	movl	$1, %r13d
	testl	%eax, %eax
	jne	.L684
	movq	-392(%rbp), %rdx
	movq	-360(%rbp), %rsi
	xorl	%r13d, %r13d
	movq	-368(%rbp), %rdi
	call	CRYPTO_memcmp@PLT
	movq	-344(%rbp), %r10
	testl	%eax, %eax
	sete	%r13b
	je	.L684
	movl	$1616, %r9d
	leaq	.LC0(%rip), %r8
	movl	$253, %ecx
	movq	%r12, %rdi
	movl	$506, %edx
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
	movq	-344(%rbp), %r10
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L699:
	movl	$1584, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$506, %edx
	movl	$80, %esi
	movl	$-1, %r13d
	call	ossl_statem_fatal@PLT
	xorl	%r10d, %r10d
	jmp	.L684
.L726:
	movq	168(%r12), %rax
	xorl	%edx, %edx
	movl	$3, %esi
	leaq	-328(%rbp), %rcx
	movq	224(%rax), %rdi
	call	BIO_ctrl@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	jle	.L728
	movl	56(%r12), %ecx
	movq	-328(%rbp), %rsi
	testl	%ecx, %ecx
	je	.L695
	cmpq	$3, %rax
	jbe	.L696
	movzbl	1(%rsi), %eax
	movzbl	2(%rsi), %ecx
	salq	$16, %rax
	salq	$8, %rcx
	orq	%rcx, %rax
	movzbl	3(%rsi), %ecx
	orq	%rcx, %rax
	leaq	-4(%rdx), %rcx
	cmpq	%rcx, %rax
	ja	.L696
	subq	%rax, %rcx
	cmpq	$3, %rcx
	jbe	.L696
	leaq	5(%rsi,%rax), %rdi
	subq	$4, %rcx
	movzbl	(%rdi), %eax
	movzbl	1(%rdi), %r8d
	movzbl	2(%rdi), %edi
	salq	$16, %rax
	salq	$8, %r8
	orq	%r8, %rax
	orq	%rdi, %rax
	cmpq	%rcx, %rax
	ja	.L696
	addq	%rax, %rdx
	subq	%rcx, %rdx
.L695:
	movq	%r15, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jg	.L692
	movl	$1576, %r9d
.L719:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	$506, %edx
	movq	%r12, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L708
.L703:
	movq	%r10, -344(%rbp)
	movl	$1605, %r9d
.L720:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	$506, %edx
	movq	%r12, %rdi
	movl	$80, %esi
	movl	$-1, %r13d
	call	ossl_statem_fatal@PLT
	movq	-344(%rbp), %r10
	jmp	.L684
.L696:
	movl	$1568, %r9d
	jmp	.L719
.L728:
	movl	$1550, %r9d
	leaq	.LC0(%rip), %r8
	movl	$332, %ecx
	movq	%r12, %rdi
	movl	$506, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L708
.L727:
	movq	%rax, -344(%rbp)
	movl	$1592, %r9d
	jmp	.L720
.L725:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1032:
	.size	tls_psk_do_binder, .-tls_psk_do_binder
	.section	.rodata
	.align 8
	.type	resumption_label.24462, @object
	.size	resumption_label.24462, 11
resumption_label.24462:
	.string	"res binder"
	.align 8
	.type	external_label.24463, @object
	.size	external_label.24463, 11
external_label.24463:
	.string	"ext binder"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	ext_defs, @object
	.size	ext_defs, 1456
ext_defs:
	.long	65281
	.long	408
	.quad	0
	.quad	tls_parse_ctos_renegotiate
	.quad	tls_parse_stoc_renegotiate
	.quad	tls_construct_stoc_renegotiate
	.quad	tls_construct_ctos_renegotiate
	.quad	final_renegotiate
	.long	0
	.long	1408
	.quad	init_server_name
	.quad	tls_parse_ctos_server_name
	.quad	tls_parse_stoc_server_name
	.quad	tls_construct_stoc_server_name
	.quad	tls_construct_ctos_server_name
	.quad	final_server_name
	.long	1
	.long	1408
	.quad	0
	.quad	tls_parse_ctos_maxfragmentlen
	.quad	tls_parse_stoc_maxfragmentlen
	.quad	tls_construct_stoc_maxfragmentlen
	.quad	tls_construct_ctos_maxfragmentlen
	.quad	final_maxfragmentlen
	.long	12
	.long	144
	.quad	init_srp
	.quad	tls_parse_ctos_srp
	.quad	0
	.quad	0
	.quad	tls_construct_ctos_srp
	.quad	0
	.long	11
	.long	400
	.quad	0
	.quad	tls_parse_ctos_ec_pt_formats
	.quad	tls_parse_stoc_ec_pt_formats
	.quad	tls_construct_stoc_ec_pt_formats
	.quad	tls_construct_ctos_ec_pt_formats
	.quad	final_ec_pt_formats
	.long	10
	.long	1408
	.quad	0
	.quad	tls_parse_ctos_supported_groups
	.quad	0
	.quad	tls_construct_stoc_supported_groups
	.quad	tls_construct_ctos_supported_groups
	.quad	0
	.long	35
	.long	400
	.quad	init_session_ticket
	.quad	tls_parse_ctos_session_ticket
	.quad	tls_parse_stoc_session_ticket
	.quad	tls_construct_stoc_session_ticket
	.quad	tls_construct_ctos_session_ticket
	.quad	0
	.long	5
	.long	20864
	.quad	init_status_request
	.quad	tls_parse_ctos_status_request
	.quad	tls_parse_stoc_status_request
	.quad	tls_construct_stoc_status_request
	.quad	tls_construct_ctos_status_request
	.quad	0
	.long	13172
	.long	400
	.quad	init_npn
	.quad	tls_parse_ctos_npn
	.quad	tls_parse_stoc_npn
	.quad	tls_construct_stoc_next_proto_neg
	.quad	tls_construct_ctos_npn
	.quad	0
	.long	16
	.long	1408
	.quad	init_alpn
	.quad	tls_parse_ctos_alpn
	.quad	tls_parse_stoc_alpn
	.quad	tls_construct_stoc_alpn
	.quad	tls_construct_ctos_alpn
	.quad	final_alpn
	.long	14
	.long	1410
	.quad	init_srtp
	.quad	tls_parse_ctos_use_srtp
	.quad	tls_parse_stoc_use_srtp
	.quad	tls_construct_stoc_use_srtp
	.quad	tls_construct_ctos_use_srtp
	.quad	0
	.long	22
	.long	400
	.quad	init_etm
	.quad	tls_parse_ctos_etm
	.quad	tls_parse_stoc_etm
	.quad	tls_construct_stoc_etm
	.quad	tls_construct_ctos_etm
	.quad	0
	.long	18
	.long	20864
	.quad	0
	.quad	0
	.quad	tls_parse_stoc_sct
	.quad	0
	.quad	tls_construct_ctos_sct
	.quad	0
	.long	23
	.long	400
	.quad	init_ems
	.quad	tls_parse_ctos_ems
	.quad	tls_parse_stoc_ems
	.quad	tls_construct_stoc_ems
	.quad	tls_construct_ctos_ems
	.quad	final_ems
	.long	50
	.long	16512
	.quad	init_sig_algs_cert
	.quad	tls_parse_ctos_sig_algs_cert
	.quad	tls_parse_ctos_sig_algs_cert
	.quad	0
	.quad	0
	.quad	0
	.long	49
	.long	160
	.quad	init_post_handshake_auth
	.quad	tls_parse_ctos_post_handshake_auth
	.quad	0
	.quad	0
	.quad	tls_construct_ctos_post_handshake_auth
	.quad	0
	.long	13
	.long	16512
	.quad	init_sig_algs
	.quad	tls_parse_ctos_sig_algs
	.quad	tls_parse_ctos_sig_algs
	.quad	tls_construct_ctos_sig_algs
	.quad	tls_construct_ctos_sig_algs
	.quad	final_sig_algs
	.long	43
	.long	2692
	.quad	0
	.quad	0
	.quad	tls_parse_stoc_supported_versions
	.quad	tls_construct_stoc_supported_versions
	.quad	tls_construct_ctos_supported_versions
	.quad	0
	.long	45
	.long	164
	.quad	init_psk_kex_modes
	.quad	tls_parse_ctos_psk_kex_modes
	.quad	0
	.quad	0
	.quad	tls_construct_ctos_psk_kex_modes
	.quad	0
	.long	51
	.long	2724
	.quad	0
	.quad	tls_parse_ctos_key_share
	.quad	tls_parse_stoc_key_share
	.quad	tls_construct_stoc_key_share
	.quad	tls_construct_ctos_key_share
	.quad	final_key_share
	.long	44
	.long	2212
	.quad	0
	.quad	tls_parse_ctos_cookie
	.quad	tls_parse_stoc_cookie
	.quad	tls_construct_stoc_cookie
	.quad	tls_construct_ctos_cookie
	.quad	0
	.long	65000
	.long	400
	.quad	0
	.quad	0
	.quad	0
	.quad	tls_construct_stoc_cryptopro_bug
	.quad	0
	.quad	0
	.long	42
	.long	9376
	.quad	0
	.quad	tls_parse_ctos_early_data
	.quad	tls_parse_stoc_early_data
	.quad	tls_construct_stoc_early_data
	.quad	tls_construct_ctos_early_data
	.quad	final_early_data
	.long	47
	.long	16544
	.quad	init_certificate_authorities
	.quad	tls_parse_certificate_authorities
	.quad	tls_parse_certificate_authorities
	.quad	tls_construct_certificate_authorities
	.quad	tls_construct_certificate_authorities
	.quad	0
	.long	21
	.long	128
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	tls_construct_ctos_padding
	.quad	0
	.long	41
	.long	676
	.quad	0
	.quad	tls_parse_ctos_psk
	.quad	tls_parse_stoc_psk
	.quad	tls_construct_stoc_psk
	.quad	tls_construct_ctos_psk
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
