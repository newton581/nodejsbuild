	.file	"x_val.c"
	.text
	.p2align 4
	.globl	d2i_X509_VAL
	.type	d2i_X509_VAL, @function
d2i_X509_VAL:
.LFB803:
	.cfi_startproc
	endbr64
	leaq	X509_VAL_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE803:
	.size	d2i_X509_VAL, .-d2i_X509_VAL
	.p2align 4
	.globl	i2d_X509_VAL
	.type	i2d_X509_VAL, @function
i2d_X509_VAL:
.LFB804:
	.cfi_startproc
	endbr64
	leaq	X509_VAL_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE804:
	.size	i2d_X509_VAL, .-i2d_X509_VAL
	.p2align 4
	.globl	X509_VAL_new
	.type	X509_VAL_new, @function
X509_VAL_new:
.LFB805:
	.cfi_startproc
	endbr64
	leaq	X509_VAL_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE805:
	.size	X509_VAL_new, .-X509_VAL_new
	.p2align 4
	.globl	X509_VAL_free
	.type	X509_VAL_free, @function
X509_VAL_free:
.LFB806:
	.cfi_startproc
	endbr64
	leaq	X509_VAL_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE806:
	.size	X509_VAL_free, .-X509_VAL_free
	.globl	X509_VAL_it
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"X509_VAL"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	X509_VAL_it, @object
	.size	X509_VAL_it, 56
X509_VAL_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	X509_VAL_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC0
	.section	.rodata.str1.1
.LC1:
	.string	"notBefore"
.LC2:
	.string	"notAfter"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	X509_VAL_seq_tt, @object
	.size	X509_VAL_seq_tt, 80
X509_VAL_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC1
	.quad	ASN1_TIME_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC2
	.quad	ASN1_TIME_it
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
