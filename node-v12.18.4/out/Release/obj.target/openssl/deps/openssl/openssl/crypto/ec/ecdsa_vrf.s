	.file	"ecdsa_vrf.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ec/ecdsa_vrf.c"
	.text
	.p2align 4
	.globl	ECDSA_do_verify
	.type	ECDSA_do_verify, @function
ECDSA_do_verify:
.LFB377:
	.cfi_startproc
	endbr64
	movq	(%rcx), %rax
	movq	112(%rax), %rax
	testq	%rax, %rax
	je	.L2
	jmp	*%rax
.L2:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$152, %edx
	movl	$252, %esi
	movl	$16, %edi
	movl	$25, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	movl	$-1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE377:
	.size	ECDSA_do_verify, .-ECDSA_do_verify
	.p2align 4
	.globl	ECDSA_verify
	.type	ECDSA_verify, @function
ECDSA_verify:
.LFB378:
	.cfi_startproc
	endbr64
	movq	(%r9), %rax
	movq	104(%rax), %rax
	testq	%rax, %rax
	je	.L7
	jmp	*%rax
.L7:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$152, %edx
	movl	$253, %esi
	movl	$16, %edi
	movl	$41, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	movl	$-1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE378:
	.size	ECDSA_verify, .-ECDSA_verify
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
