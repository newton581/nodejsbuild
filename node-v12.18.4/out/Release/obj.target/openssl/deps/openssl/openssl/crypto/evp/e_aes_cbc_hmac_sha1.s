	.file	"e_aes_cbc_hmac_sha1.c"
	.text
	.p2align 4
	.type	sha1_update, @function
sha1_update:
.LFB406:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	92(%rdi), %eax
	movq	%rdx, %rbx
	testq	%rax, %rax
	jne	.L17
	movq	%rbx, %r14
	andl	$63, %r14d
	andq	$-64, %rbx
	jne	.L18
.L3:
	testq	%r14, %r14
	jne	.L19
.L1:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movl	$64, %r14d
	subq	%rax, %r14
	cmpq	%r14, %rdx
	cmovbe	%rdx, %r14
	subq	%r14, %rbx
	movq	%r14, %rdx
	addq	%r14, %r13
	call	SHA1_Update@PLT
	movq	%rbx, %r14
	andl	$63, %r14d
	andq	$-64, %rbx
	je	.L3
.L18:
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	addq	%rbx, %r13
	shrq	$6, %rdx
	call	sha1_block_data_order@PLT
	movq	%rbx, %rax
	salq	$3, %rbx
	shrq	$29, %rax
	addl	20(%r12), %ebx
	adcl	%eax, 24(%r12)
	movl	%ebx, 20(%r12)
	testq	%r14, %r14
	je	.L1
.L19:
	popq	%rbx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	SHA1_Update@PLT
	.cfi_endproc
.LFE406:
	.size	sha1_update, .-sha1_update
	.p2align 4
	.type	aesni_cbc_hmac_sha1_cipher, @function
aesni_cbc_hmac_sha1_cipher:
.LFB408:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	536(%rax), %r10
	movl	528(%rax), %ebx
	movq	%rax, %r15
	movq	$-1, 536(%rax)
	movq	%r12, %rax
	andl	$15, %eax
	movq	%rax, -128(%rbp)
	je	.L21
.L25:
	xorl	%eax, %eax
.L20:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L74
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	movq	%r13, %rdi
	movq	%r10, -136(%rbp)
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	-136(%rbp), %r10
	testl	%eax, %eax
	je	.L23
	movl	$64, %r11d
	subl	%ebx, %r11d
	cmpq	$-1, %r10
	je	.L24
	leaq	36(%r10), %rax
	andq	$-16, %rax
	cmpq	%r12, %rax
	jne	.L25
	cmpl	$769, 544(%r15)
	ja	.L75
	movq	$0, -160(%rbp)
	movq	%r14, %rsi
	movq	%r11, %rax
.L26:
	movq	-120(%rbp), %rcx
	movq	%r12, -144(%rbp)
	leaq	436(%r15), %rbx
	movq	%rcx, -136(%rbp)
	cmpq	%rax, %r10
	jbe	.L27
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L23:
	cmpq	$-1, %r10
	je	.L34
	movzbl	540(%r15,%r10), %edx
	movzbl	541(%r15,%r10), %esi
	sall	$8, %edx
	orl	%esi, %edx
	cmpl	$769, %edx
	jle	.L35
	cmpq	$36, %r12
	jbe	.L20
	movq	%r13, %rdi
	movq	%r10, -136(%rbp)
	addq	$16, %r14
	subq	$16, %r12
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	addq	$16, -120(%rbp)
	movdqu	-16(%r14), %xmm6
	movq	-136(%rbp), %r10
	movups	%xmm6, (%rax)
.L37:
	movq	%r13, %rdi
	leaq	464(%r15), %rax
	movq	%r10, -136(%rbp)
	leaq	-81(%rbp), %rbx
	movq	%rax, -168(%rbp)
	andq	$-32, %rbx
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	-120(%rbp), %r13
	movq	%r14, %rdi
	xorl	%r9d, %r9d
	movq	%rax, %r8
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%r12, %r14
	movq	%r13, %rsi
	call	aesni_cbc_encrypt@PLT
	leal	-21(%r12), %esi
	movl	$255, %eax
	movzbl	-1(%r13,%r12), %edx
	subl	%esi, %eax
	movq	-136(%rbp), %r10
	movdqu	244(%r15), %xmm1
	leaq	436(%r15), %r13
	movdqu	260(%r15), %xmm2
	shrl	$24, %eax
	movdqu	276(%r15), %xmm3
	movdqu	292(%r15), %xmm4
	orl	%esi, %eax
	movdqu	308(%r15), %xmm5
	movdqu	324(%r15), %xmm6
	movzbl	%al, %ecx
	movl	%ecx, %eax
	movl	%ecx, %esi
	movl	%ecx, -180(%rbp)
	subl	%edx, %eax
	xorl	%edx, %esi
	xorl	%edx, %eax
	orl	%esi, %eax
	xorl	%ecx, %eax
	sarl	$31, %eax
	movl	%eax, %esi
	leal	1(%rax), %edi
	notl	%esi
	movl	%edi, -184(%rbp)
	movq	%r13, %rdi
	andl	%esi, %edx
	andl	%ecx, %eax
	leaq	544(%r15), %rsi
	orl	%eax, %edx
	leal	21(%rdx), %eax
	movl	%edx, -188(%rbp)
	movq	%r10, %rdx
	subq	%rax, %r14
	movq	%r14, %rax
	movb	%r14b, 543(%r15,%r10)
	shrq	$8, %rax
	movb	%al, 542(%r15,%r10)
	movups	%xmm1, 436(%r15)
	movups	%xmm2, 452(%r15)
	movups	%xmm3, 468(%r15)
	movups	%xmm4, 484(%r15)
	movups	%xmm5, 500(%r15)
	movups	%xmm6, 516(%r15)
	call	sha1_update
	leaq	-20(%r12), %r10
	cmpq	$319, %r10
	ja	.L76
	movl	456(%r15), %eax
	movl	$0, 16(%rbx)
	pxor	%xmm0, %xmm0
	movl	528(%r15), %edx
	movaps	%xmm0, (%rbx)
	leal	(%rax,%r14,8), %eax
#APP
# 587 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %eax
# 0 "" 2
#NO_APP
	movl	%eax, -176(%rbp)
.L56:
	movq	-120(%rbp), %rax
	movq	%r14, %r12
	movq	%rbx, %rdi
	movq	%r13, %rbx
	negq	%r12
	movq	%rdi, %r13
	leaq	(%rax,%r14), %rcx
	movq	%rcx, %rax
	movq	%r14, %rcx
	movq	%rax, %r14
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L40:
	addq	$1, %r12
	leaq	(%rcx,%r12), %rax
	cmpq	%rax, %r10
	jbe	.L77
.L41:
	movl	%r8d, %edx
.L43:
	movq	%r12, %rax
	movq	%r12, %rdi
	movzbl	(%r14,%r12), %esi
	movl	%edx, %r11d
	negq	%rax
	shrq	$56, %rdi
	leal	1(%rdx), %r8d
	shrq	$56, %rax
	andq	%rdi, %rsi
	orq	%rdi, %rax
	notq	%rax
	andl	$128, %eax
	orq	%rax, %rsi
	movb	%sil, 464(%r15,%r11)
	cmpl	$63, %edx
	jne	.L40
	movl	$7, %eax
	movq	-168(%rbp), %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	subq	%r12, %rax
	movq	%rcx, -160(%rbp)
	sarq	$63, %rax
	movq	%r10, -144(%rbp)
	movq	%rax, %r8
	movl	-176(%rbp), %eax
	movq	%r8, -136(%rbp)
	andl	%r8d, %eax
	orl	%eax, 524(%r15)
	call	sha1_block_data_order@PLT
	leaq	-72(%r12), %rax
	addq	$1, %r12
	movq	-136(%rbp), %r8
	movl	436(%r15), %edx
	sarq	$63, %rax
	movq	-160(%rbp), %rcx
	andq	%r8, %rax
	movq	-144(%rbp), %r10
	andl	%eax, %edx
	orl	%edx, 0(%r13)
	movl	440(%r15), %edx
	andl	%eax, %edx
	orl	%edx, 4(%r13)
	movl	444(%r15), %edx
	andl	%eax, %edx
	orl	%edx, 8(%r13)
	movl	448(%r15), %edx
	andl	%eax, %edx
	andl	452(%r15), %eax
	orl	%eax, 16(%r13)
	leaq	(%rcx,%r12), %rax
	orl	%edx, 12(%r13)
	cmpq	%r10, %rax
	jnb	.L78
	xorl	%r8d, %r8d
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L24:
	leaq	436(%r15), %rbx
	cmpq	%r12, %r11
	jnb	.L79
	movq	%r14, %rsi
	movq	%r12, %r10
	movq	%r11, %rax
	movq	$0, -160(%rbp)
.L54:
	movq	-120(%rbp), %rdi
	movq	%r10, %rcx
	movq	%r12, -144(%rbp)
	subq	%rax, %rcx
	movq	%rcx, -168(%rbp)
	movq	%rdi, -136(%rbp)
	cmpq	$63, %rcx
	ja	.L80
.L27:
	movq	%r10, %rdx
	movq	%rbx, %rdi
	subq	-160(%rbp), %rdx
	movq	%r10, -168(%rbp)
	call	sha1_update
	movq	-168(%rbp), %r10
	cmpq	%r10, %r12
	je	.L30
	cmpq	-120(%rbp), %r14
	je	.L31
	movq	-128(%rbp), %rax
	movq	%r10, %rdx
	movq	-136(%rbp), %rdi
	movq	%r10, -160(%rbp)
	subq	%rax, %rdx
	leaq	(%r14,%rax), %rsi
	call	memcpy@PLT
	movq	-160(%rbp), %r10
.L31:
	movq	-120(%rbp), %rax
	movq	%rbx, %rsi
	movq	%r10, -128(%rbp)
	leaq	(%rax,%r10), %r14
	movq	%r14, %rdi
	call	SHA1_Final@PLT
	movl	$20, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movdqu	340(%r15), %xmm2
	movdqu	356(%r15), %xmm3
	movdqu	372(%r15), %xmm4
	movdqu	388(%r15), %xmm5
	movdqu	404(%r15), %xmm6
	movups	%xmm2, 436(%r15)
	movdqu	420(%r15), %xmm7
	movups	%xmm3, 452(%r15)
	movups	%xmm4, 468(%r15)
	movups	%xmm5, 484(%r15)
	movups	%xmm6, 500(%r15)
	movups	%xmm7, 516(%r15)
	call	sha1_update
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	SHA1_Final@PLT
	movq	-128(%rbp), %r10
	leal	-1(%r12), %esi
	leaq	20(%r10), %rdi
	subl	%edi, %esi
	cmpq	%rdi, %r12
	jbe	.L33
	leaq	-20(%r12), %rdx
	movzbl	%sil, %esi
	addq	-120(%rbp), %rdi
	subq	%r10, %rdx
	call	memset@PLT
.L33:
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	-136(%rbp), %rdi
	movq	-144(%rbp), %rdx
	movq	%r15, %rcx
	movq	%rax, %r8
	movl	$1, %r9d
	movq	%rdi, %rsi
	call	aesni_cbc_encrypt@PLT
	movl	$1, %eax
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L35:
	cmpq	$20, %r12
	jbe	.L20
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L34:
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	-120(%rbp), %rbx
	xorl	%r9d, %r9d
	movq	%r15, %rcx
	movq	%rax, %r8
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	aesni_cbc_encrypt@PLT
	leaq	436(%r15), %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	sha1_update
	movl	$1, %eax
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L75:
	movq	$16, -160(%rbp)
	leaq	16(%r11), %rax
	leaq	16(%r14), %rsi
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L79:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	sha1_update
.L53:
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	-120(%rbp), %rsi
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%rax, %r8
	movl	$1, %r9d
	movq	%r14, %rdi
	call	aesni_cbc_encrypt@PLT
	movl	$1, %eax
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L77:
	movq	%r13, %rax
	movq	%rcx, %r14
	movq	%rbx, %r13
	movq	%rax, %rbx
.L42:
	testq	%r10, %r10
	movl	$1, %eax
	cmovne	%r10, %rax
	movq	%rax, %r11
.L39:
	movl	%r8d, %r12d
	cmpl	$63, %r8d
	ja	.L44
	movl	$64, %edx
	leaq	464(%r15,%r12), %rax
	xorl	%r9d, %r9d
	subq	%r12, %rdx
	cmpl	$8, %edx
	jnb	.L45
	testb	$4, %dl
	jne	.L81
	testl	%edx, %edx
	je	.L46
	movb	$0, (%rax)
	testb	$2, %dl
	jne	.L82
.L46:
	leaq	64(%r11), %rax
	subq	%r12, %rax
	movq	%rax, %r11
	movq	$-73, %rax
	subq	%r14, %rax
	leaq	(%r11,%rax), %r12
	sarq	$63, %r12
	cmpl	$56, %r8d
	ja	.L57
.L51:
	movl	-176(%rbp), %eax
	movq	-168(%rbp), %rsi
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%r10, -136(%rbp)
	movl	%eax, 524(%r15)
	call	sha1_block_data_order@PLT
	movl	440(%r15), %edi
	movl	444(%r15), %esi
	movl	448(%r15), %edx
	movl	452(%r15), %eax
	movdqu	340(%r15), %xmm7
	andl	%r12d, %edi
	andl	%r12d, %esi
	orl	4(%rbx), %edi
	orl	8(%rbx), %esi
	andl	%r12d, %edx
	andl	%r12d, %eax
	orl	12(%rbx), %edx
	orl	16(%rbx), %eax
	andl	436(%r15), %r12d
	movdqu	356(%r15), %xmm1
	orl	(%rbx), %r12d
	movdqu	372(%r15), %xmm2
	movdqu	388(%r15), %xmm3
	movdqu	404(%r15), %xmm4
	movdqu	420(%r15), %xmm5
#APP
# 656 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %eax
# 0 "" 2
# 653 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %edi
# 0 "" 2
#NO_APP
	movl	%eax, 16(%rbx)
	movl	%edi, 4(%rbx)
	movq	%r13, %rdi
#APP
# 654 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %esi
# 0 "" 2
# 655 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %edx
# 0 "" 2
#NO_APP
	movl	%esi, 8(%rbx)
	movq	%rbx, %rsi
	movl	%edx, 12(%rbx)
	movl	$20, %edx
#APP
# 652 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %r12d
# 0 "" 2
#NO_APP
	movl	%r12d, (%rbx)
	movups	%xmm7, 436(%r15)
	movups	%xmm1, 452(%r15)
	movups	%xmm2, 468(%r15)
	movups	%xmm3, 484(%r15)
	movups	%xmm4, 500(%r15)
	movups	%xmm5, 516(%r15)
	call	sha1_update
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	SHA1_Final@PLT
	movl	-180(%rbp), %esi
	movq	-128(%rbp), %rdi
	xorl	%ecx, %ecx
	movq	-136(%rbp), %r10
	movl	$-20, %r8d
	movl	-188(%rbp), %r11d
	movq	%rsi, %rax
	notq	%rsi
	addq	%r10, %rsi
	leal	20(%rax), %r9d
	subq	%rsi, %r14
	addq	-120(%rbp), %rsi
	subl	%r14d, %r8d
	addq	%rsi, %r9
	leal	-1(%rsi,%r14), %r10d
	subl	%esi, %r8d
	.p2align 4,,10
	.p2align 3
.L52:
	movzbl	(%rsi), %r12d
	movl	%r10d, %eax
	leal	(%r8,%rsi), %r13d
	subl	%esi, %eax
	sarl	$31, %r13d
	addq	$1, %rsi
	movl	%r12d, %edx
	sarl	$31, %eax
	xorb	(%rbx,%rdi), %dl
	xorl	%r11d, %r12d
	andl	%r13d, %eax
	movzbl	%dl, %edx
	notl	%r13d
	andl	%eax, %edx
	andl	%r13d, %r12d
	andl	$1, %eax
	orl	%r12d, %edx
	addq	%rax, %rdi
	orl	%edx, %ecx
	cmpq	%r9, %rsi
	jne	.L52
	movl	%ecx, %eax
	negl	%eax
	sarl	$31, %eax
	notl	%eax
	andl	-184(%rbp), %eax
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L45:
	movl	%edx, %esi
	movq	$0, (%rax)
	movq	$0, -8(%rax,%rsi)
	leaq	8(%rax), %rsi
	andq	$-8, %rsi
	subq	%rsi, %rax
	addl	%edx, %eax
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L46
	andl	$-8, %eax
	xorl	%edx, %edx
.L49:
	movl	%edx, %ecx
	addl	$8, %edx
	movq	%r9, (%rsi,%rcx)
	cmpl	%eax, %edx
	jb	.L49
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L80:
	movq	%r11, %rdx
	movq	%rbx, %rdi
	movq	%rax, -128(%rbp)
	movq	%r10, -176(%rbp)
	movq	%r11, -136(%rbp)
	call	sha1_update
	movq	-128(%rbp), %rax
	movq	%r13, %rdi
	addq	%r14, %rax
	movq	%rax, -128(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	subq	$8, %rsp
	pushq	-128(%rbp)
	movq	-120(%rbp), %rsi
	movq	-168(%rbp), %rdx
	movq	%rax, %r8
	movq	%r15, %rcx
	movq	%r14, %rdi
	movq	%rbx, %r9
	shrq	$6, %rdx
	call	aesni_cbc_sha1_enc@PLT
	movq	-168(%rbp), %rax
	popq	%rsi
	movq	-120(%rbp), %rsi
	popq	%rdi
	xorl	%edi, %edi
	movq	%rax, %rcx
	movq	-136(%rbp), %r11
	shrq	$29, %rax
	addq	-160(%rbp), %r11
	andq	$-64, %rcx
	addl	460(%r15), %eax
	movq	-176(%rbp), %r10
	leaq	0(,%rcx,8), %rdx
	addl	456(%r15), %edx
	movq	%rcx, -128(%rbp)
	setc	%dil
	addq	%rcx, %rsi
	movl	%edx, 456(%r15)
	movq	%r11, %rdx
	movq	%rsi, -136(%rbp)
	movq	%r12, %rsi
	addq	%rcx, %rdx
	addl	%edi, %eax
	subq	%rcx, %rsi
	movq	%rdx, -160(%rbp)
	movq	%rsi, -144(%rbp)
	leaq	(%r14,%rdx), %rsi
	movl	%eax, 460(%r15)
	jmp	.L27
.L44:
	movq	$-73, %rax
	subq	%r14, %rax
	leaq	(%r11,%rax), %r12
	sarq	$63, %r12
.L57:
	leaq	8(%r14), %rdx
	movq	%r12, %xmm1
	movq	%r13, %rdi
	movq	-168(%rbp), %rsi
	subq	%r11, %rdx
	punpcklqdq	%xmm1, %xmm1
	movq	%rax, -208(%rbp)
	movq	%rdx, %rcx
	movl	-176(%rbp), %edx
	movq	%r10, -200(%rbp)
	sarq	$63, %rcx
	movq	%r11, -144(%rbp)
	andl	%ecx, %edx
	orl	%edx, 524(%r15)
	movl	$1, %edx
	movq	%rcx, -136(%rbp)
	movaps	%xmm1, -160(%rbp)
	call	sha1_block_data_order@PLT
	movq	-136(%rbp), %rcx
	movdqa	-160(%rbp), %xmm1
	movq	%r12, %rdx
	movdqu	436(%r15), %xmm7
	movq	-168(%rbp), %rax
	movq	%rcx, %xmm0
	shufps	$136, %xmm1, %xmm1
	andq	%rcx, %rdx
	movq	-144(%rbp), %r11
	punpcklqdq	%xmm0, %xmm0
	andl	452(%r15), %edx
	orl	%edx, 16(%rbx)
	shufps	$136, %xmm0, %xmm0
	pand	%xmm1, %xmm0
	movq	-200(%rbp), %r10
	pand	%xmm7, %xmm0
	por	(%rbx), %xmm0
	movaps	%xmm0, (%rbx)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 464(%r15)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movq	-208(%rbp), %rax
	leaq	64(%r11,%rax), %r12
	sarq	$63, %r12
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L76:
	subq	$340, %r12
	movq	-120(%rbp), %rsi
	movq	%r13, %rdi
	movq	%r10, -136(%rbp)
	movq	%r12, %rax
	movl	$64, %r12d
	subl	528(%r15), %r12d
	andq	$-64, %rax
	addq	%rax, %r12
	movq	%r12, %rdx
	subq	%r12, %r14
	call	sha1_update
	movl	456(%r15), %eax
	movq	-136(%rbp), %r10
	pxor	%xmm0, %xmm0
	addq	%r12, -120(%rbp)
	movl	528(%r15), %edx
	leal	(%rax,%r14,8), %eax
	movl	$0, 16(%rbx)
#APP
# 587 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %eax
# 0 "" 2
#NO_APP
	movaps	%xmm0, (%rbx)
	movl	%eax, -176(%rbp)
	subq	%r12, %r10
	jne	.L56
	movl	%edx, %r8d
	xorl	%r11d, %r11d
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L78:
	movq	%r13, %rax
	movq	%rcx, %r14
	movq	%rbx, %r13
	xorl	%r8d, %r8d
	movq	%rax, %rbx
	jmp	.L42
.L81:
	movl	%edx, %edx
	movl	$0, (%rax)
	movl	$0, -4(%rax,%rdx)
	jmp	.L46
.L82:
	movl	%edx, %edx
	xorl	%ecx, %ecx
	movw	%cx, -2(%rax,%rdx)
	jmp	.L46
.L74:
	call	__stack_chk_fail@PLT
.L30:
	movq	-136(%rbp), %rax
	movq	-144(%rbp), %r12
	addq	-128(%rbp), %r14
	movq	%rax, -120(%rbp)
	jmp	.L53
	.cfi_endproc
.LFE408:
	.size	aesni_cbc_hmac_sha1_cipher, .-aesni_cbc_hmac_sha1_cipher
	.p2align 4
	.type	aesni_cbc_hmac_sha1_init_key, @function
aesni_cbc_hmac_sha1_init_key:
.LFB405:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	testl	%r14d, %r14d
	je	.L84
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%rbx, %rdx
	movq	%r13, %rdi
	leal	0(,%rax,8), %esi
	call	aesni_set_encrypt_key@PLT
	movl	%eax, %r12d
.L85:
	leaq	244(%rbx), %rdi
	call	SHA1_Init@PLT
	movdqu	244(%rbx), %xmm5
	movl	%r12d, %eax
	movdqu	260(%rbx), %xmm4
	movdqu	276(%rbx), %xmm3
	movdqu	292(%rbx), %xmm2
	notl	%eax
	movq	$-1, 536(%rbx)
	movdqu	308(%rbx), %xmm1
	movdqu	324(%rbx), %xmm0
	movups	%xmm5, 340(%rbx)
	shrl	$31, %eax
	movups	%xmm4, 356(%rbx)
	movups	%xmm3, 372(%rbx)
	movups	%xmm2, 388(%rbx)
	movups	%xmm1, 404(%rbx)
	movups	%xmm0, 420(%rbx)
	movups	%xmm5, 436(%rbx)
	movups	%xmm4, 452(%rbx)
	movups	%xmm3, 468(%rbx)
	movups	%xmm2, 484(%rbx)
	movups	%xmm1, 500(%rbx)
	movups	%xmm0, 516(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%rbx, %rdx
	movq	%r13, %rdi
	leal	0(,%rax,8), %esi
	call	aesni_set_decrypt_key@PLT
	movl	%eax, %r12d
	jmp	.L85
	.cfi_endproc
.LFE405:
	.size	aesni_cbc_hmac_sha1_init_key, .-aesni_cbc_hmac_sha1_init_key
	.p2align 4
	.type	tls1_1_multi_block_encrypt, @function
tls1_1_multi_block_encrypt:
.LFB407:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	leal	0(,%r8,4), %ecx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$1928, %rsp
	movq	%rsi, -1944(%rbp)
	movl	%r8d, %esi
	movl	%r8d, -1904(%rbp)
	sall	$6, %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-1280(%rbp), %rax
	movl	%ecx, -1860(%rbp)
	movq	%rax, %rdi
	movq	%rax, -1880(%rbp)
	call	RAND_bytes@PLT
	movq	$0, -1896(%rbp)
	testl	%eax, %eax
	jle	.L87
	leal	1(%r15), %ecx
	movl	%r14d, %esi
	leaq	-256(%rbp), %rax
	shrl	%cl, %esi
	andq	$-32, %rax
	leaq	32(%rax), %r12
	addl	%esi, %r14d
	movl	%esi, %eax
	movl	%esi, -1864(%rbp)
	sall	%cl, %eax
	movl	%r14d, %edi
	subl	%eax, %edi
	movl	%edi, -1868(%rbp)
	cmpl	%edi, %esi
	jb	.L156
.L89:
	movl	-1864(%rbp), %esi
	movl	-1868(%rbp), %edx
	movq	%rbx, -1856(%rbp)
	movq	-1944(%rbp), %rdi
	movdqa	-1280(%rbp), %xmm0
	movq	%rbx, -1600(%rbp)
	leal	36(%rsi), %eax
	andl	$-16, %eax
	leaq	21(%rdi), %rcx
	movups	%xmm0, 5(%rdi)
	addl	$21, %eax
	cmpl	%edx, %esi
	movq	%rcx, -1592(%rbp)
	cmovbe	%esi, %edx
	movups	%xmm0, -1576(%rbp)
	leal	-51(%rdx), %edi
	movl	%edi, -1928(%rbp)
	movl	-1860(%rbp), %edi
	cmpl	$1, %edi
	jle	.L90
	movl	%esi, %edx
	addq	%rax, %rcx
	movdqa	-1264(%rbp), %xmm3
	addq	%rdx, %rbx
	movq	%rcx, %xmm2
	movq	%rbx, %xmm0
	movq	%rbx, -1840(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, -1560(%rbp)
	movups	%xmm3, -16(%rcx)
	movq	-1840(%rbp), %rcx
	movq	-1552(%rbp), %rsi
	movdqa	-1264(%rbp), %xmm4
	movdqa	-1248(%rbp), %xmm2
	addq	%rdx, %rcx
	addq	%rax, %rsi
	movq	%rsi, %xmm5
	movq	%rcx, %xmm0
	movq	%rcx, -1824(%rbp)
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm4, -1536(%rbp)
	movaps	%xmm0, -1520(%rbp)
	movups	%xmm2, -16(%rsi)
	movq	-1824(%rbp), %rcx
	movq	-1512(%rbp), %rsi
	movdqa	-1248(%rbp), %xmm3
	movdqa	-1232(%rbp), %xmm5
	addq	%rdx, %rcx
	addq	%rax, %rsi
	movq	%rcx, %xmm0
	movq	%rsi, %xmm4
	movq	%rcx, -1808(%rbp)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm3, -1496(%rbp)
	movups	%xmm0, -1480(%rbp)
	movups	%xmm5, -16(%rsi)
	movdqa	-1232(%rbp), %xmm6
	movaps	%xmm6, -1456(%rbp)
	cmpl	$4, %edi
	je	.L91
	movq	-1808(%rbp), %rcx
	movq	-1472(%rbp), %rsi
	addq	%rdx, %rcx
	addq	%rax, %rsi
	movq	%rsi, %xmm7
	movq	%rcx, %xmm0
	movq	%rcx, -1792(%rbp)
	punpcklqdq	%xmm7, %xmm0
	movdqa	-1216(%rbp), %xmm7
	movaps	%xmm0, -1440(%rbp)
	movups	%xmm7, -16(%rsi)
	movq	-1792(%rbp), %rcx
	movq	-1432(%rbp), %rsi
	movdqa	-1216(%rbp), %xmm2
	movdqa	-1200(%rbp), %xmm4
	addq	%rdx, %rcx
	addq	%rax, %rsi
	movq	%rsi, %xmm3
	movq	%rcx, %xmm0
	movq	%rcx, -1776(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm2, -1416(%rbp)
	movups	%xmm0, -1400(%rbp)
	movups	%xmm4, -16(%rsi)
	movq	-1776(%rbp), %rcx
	movq	-1392(%rbp), %rsi
	movdqa	-1200(%rbp), %xmm5
	movdqa	-1184(%rbp), %xmm7
	addq	%rdx, %rcx
	addq	%rax, %rsi
	movq	%rcx, %xmm0
	movq	%rsi, %xmm6
	movq	%rcx, -1760(%rbp)
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm5, -1376(%rbp)
	movaps	%xmm0, -1360(%rbp)
	movups	%xmm7, -16(%rsi)
	addq	-1760(%rbp), %rdx
	addq	-1352(%rbp), %rax
	movdqa	-1184(%rbp), %xmm2
	movq	%rdx, %xmm0
	movdqa	-1168(%rbp), %xmm4
	movq	%rax, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movq	%rdx, -1744(%rbp)
	movups	%xmm2, -1336(%rbp)
	movups	%xmm0, -1320(%rbp)
	movups	%xmm4, -16(%rax)
	movdqa	-1168(%rbp), %xmm5
	movaps	%xmm5, -1296(%rbp)
.L91:
	movq	464(%r13), %r9
	movq	%r9, -1280(%rbp)
#APP
# 210 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapq %r9
# 0 "" 2
#NO_APP
.L121:
	movl	-1860(%rbp), %eax
	leaq	-1856(%rbp), %rbx
	xorl	%edx, %edx
	movzbl	472(%r13), %ecx
	movl	448(%r13), %edi
	movq	%rbx, -1912(%rbp)
	leaq	-1728(%rbp), %r14
	movq	%rbx, %rsi
	subl	$1, %eax
	movq	%r9, -1920(%rbp)
	movl	452(%r13), %r15d
	movq	%r14, %r8
	movl	%eax, -1872(%rbp)
	movl	436(%r13), %eax
	movq	%r13, -1936(%rbp)
	movzbl	473(%r13), %r11d
	movl	%eax, -1888(%rbp)
	movl	440(%r13), %eax
	movq	%r14, -1952(%rbp)
	movzbl	474(%r13), %r10d
	movl	%ecx, %r14d
	movl	%eax, -1896(%rbp)
	movl	444(%r13), %eax
	movl	%edi, %r13d
	movl	%eax, -1900(%rbp)
	movq	-1880(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L94:
	movl	-1888(%rbp), %edi
	cmpl	%edx, -1872(%rbp)
	movl	%r13d, 96(%r12,%rdx,4)
	movl	-1868(%rbp), %ebx
	cmovne	-1864(%rbp), %ebx
	movb	%r14b, 8(%rax)
	addq	$16, %rsi
	movl	%edi, (%r12,%rdx,4)
	movl	-1896(%rbp), %edi
	addq	$16, %r8
	movb	%bh, 11(%rax)
	movl	%edi, 32(%r12,%rdx,4)
	movl	-1900(%rbp), %edi
	movb	%bl, 12(%rax)
	subl	$51, %ebx
	movl	%edi, 64(%r12,%rdx,4)
	movq	-1920(%rbp), %rdi
	shrl	$6, %ebx
	movl	%r15d, 128(%r12,%rdx,4)
	leaq	(%rdi,%rdx), %rcx
	movb	%r11b, 9(%rax)
	addq	$1, %rdx
#APP
# 226 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapq %rcx
# 0 "" 2
#NO_APP
	movq	%rcx, (%rax)
	movq	-16(%rsi), %rcx
	movb	%r10b, 10(%rax)
	movdqu	(%rcx), %xmm2
	addq	$51, %rcx
	movups	%xmm2, 13(%rax)
	movdqu	-35(%rcx), %xmm3
	movups	%xmm3, 29(%rax)
	movdqu	-19(%rcx), %xmm4
	movups	%xmm4, 45(%rax)
	movzwl	-3(%rcx), %r9d
	movw	%r9w, 61(%rax)
	movzbl	-1(%rcx), %r9d
	movq	%rax, -16(%r8)
	subq	$-128, %rax
	movb	%r9b, -65(%rax)
	movq	%rcx, -16(%rsi)
	movl	%ebx, -8(%rsi)
	movl	$1, -8(%r8)
	cmpl	%edx, -1860(%rbp)
	ja	.L94
	movq	-1952(%rbp), %r14
	movl	-1904(%rbp), %edx
	movq	%r12, %rdi
	movq	-1936(%rbp), %r13
	movq	%r14, %rsi
	call	sha1_multi_block@PLT
	cmpl	$2111, -1928(%rbp)
	ja	.L120
	leaq	-1600(%rbp), %rax
	movl	$0, -1900(%rbp)
	movq	%rax, -1936(%rbp)
.L100:
	movl	-1904(%rbp), %edx
	movq	-1912(%rbp), %rsi
	movq	%r12, %rdi
	call	sha1_multi_block@PLT
	movq	-1880(%rbp), %rdi
	xorl	%eax, %eax
	movl	$128, %ecx
	rep stosq
	movl	-1860(%rbp), %eax
	testl	%eax, %eax
	je	.L102
	subl	$1, %eax
	movl	$-51, %ebx
	subl	-1900(%rbp), %ebx
	xorl	%r15d, %r15d
	movl	%eax, -1872(%rbp)
	leaq	-1720(%rbp), %rax
	movl	%ebx, -1920(%rbp)
	leaq	-1848(%rbp), %rbx
	movq	%r12, -1952(%rbp)
	movq	%r14, %r12
	movq	%r13, -1960(%rbp)
	movq	%rbx, %r13
	movl	%ecx, %ebx
	movq	%rax, -1928(%rbp)
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L158:
	movl	%eax, 60(%rsi,%r15,8)
	movq	-1928(%rbp), %rax
	movl	$1, (%rax,%r15)
.L155:
	movq	%rdi, (%r12,%r15)
	addl	$1, %ebx
	addq	$16, %r15
	cmpl	%ebx, -1860(%rbp)
	je	.L157
.L107:
	movl	0(%r13,%r15), %esi
	movl	-1920(%rbp), %ecx
	cmpl	-1872(%rbp), %ebx
	movl	-1864(%rbp), %r8d
	cmove	-1868(%rbp), %r8d
	sall	$6, %esi
	movq	-1880(%rbp), %rax
	subl	%esi, %ecx
	leal	(%rcx,%r8), %edx
	movq	-1912(%rbp), %rcx
	leaq	(%rax,%r15,8), %rdi
	movl	%r8d, -1896(%rbp)
	movq	%rdx, -1888(%rbp)
	movq	%rdx, %r14
	addq	(%rcx,%r15), %rsi
	call	memcpy@PLT
	movq	-1888(%rbp), %rdx
	leaq	-48(%rbp), %rcx
	movl	-1896(%rbp), %r8d
	movq	%rax, %rdi
	movl	%ebx, %eax
	movq	-1880(%rbp), %rsi
	salq	$7, %rax
	addq	%rcx, %rax
	movb	$-128, -1232(%rdx,%rax)
	leal	616(,%r8,8), %eax
#APP
# 300 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %eax
# 0 "" 2
#NO_APP
	cmpl	$55, %r14d
	jbe	.L158
	movl	%eax, 124(%rsi,%r15,8)
	movq	-1928(%rbp), %rax
	movl	$2, (%rax,%r15)
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L162:
	movq	-1920(%rbp), %r12
.L116:
	movl	-1904(%rbp), %edx
	movq	-1936(%rbp), %rdi
	movq	%r13, %rsi
	call	aesni_multi_cbc_encrypt@PLT
	movq	-1880(%rbp), %rdi
	movl	$1024, %esi
	call	OPENSSL_cleanse@PLT
	movl	$160, %esi
	movq	%r12, %rdi
	call	OPENSSL_cleanse@PLT
.L87:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L159
	movq	-1896(%rbp), %rax
	addq	$1928, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L156:
	.cfi_restore_state
	movl	-1860(%rbp), %ecx
	leal	22(%rdi), %eax
	andl	$63, %eax
	leal	-1(%rcx), %edx
	cmpl	%edx, %eax
	jnb	.L89
	movl	%edi, %eax
	addl	$1, %esi
	subl	%ecx, %eax
	movl	%esi, -1864(%rbp)
	addl	$1, %eax
	movl	%eax, -1868(%rbp)
	jmp	.L89
.L120:
	movq	-1856(%rbp), %rax
	movl	-1860(%rbp), %ebx
	movl	$32, -1720(%rbp)
	movl	$128, -1584(%rbp)
	movq	%rax, -1728(%rbp)
	cmpl	$1, %ebx
	jbe	.L98
	movq	-1840(%rbp), %rax
	movl	$32, -1704(%rbp)
	movl	$128, -1544(%rbp)
	movq	%rax, -1712(%rbp)
	movq	-1824(%rbp), %rax
	movl	$32, -1688(%rbp)
	movq	%rax, -1696(%rbp)
	movq	-1808(%rbp), %rax
	movl	$128, -1504(%rbp)
	movq	%rax, -1680(%rbp)
	movl	$32, -1672(%rbp)
	movl	$128, -1464(%rbp)
	cmpl	$4, %ebx
	je	.L98
	movq	-1792(%rbp), %rax
	movl	$32, -1656(%rbp)
	movl	$128, -1424(%rbp)
	movq	%rax, -1664(%rbp)
	movq	-1776(%rbp), %rax
	movl	$32, -1640(%rbp)
	movq	%rax, -1648(%rbp)
	movq	-1760(%rbp), %rax
	movl	$128, -1384(%rbp)
	movq	%rax, -1632(%rbp)
	movl	$32, -1624(%rbp)
	movl	$128, -1344(%rbp)
	cmpl	$8, %ebx
	jne	.L98
	movq	-1744(%rbp), %rax
	movl	$32, -1608(%rbp)
	movl	$128, -1304(%rbp)
	movq	%rax, -1616(%rbp)
.L98:
	leaq	-1600(%rbp), %rax
	movl	-1928(%rbp), %ebx
	movl	$0, -1900(%rbp)
	movq	%rax, -1936(%rbp)
	movl	-1904(%rbp), %r15d
	shrl	$6, %ebx
	.p2align 4,,10
	.p2align 3
.L97:
	movl	%r15d, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	sha1_multi_block@PLT
	movq	-1936(%rbp), %rdi
	movl	%r15d, %edx
	movq	%r13, %rsi
	call	aesni_multi_cbc_encrypt@PLT
	movl	-1860(%rbp), %eax
	movdqa	.LC0(%rip), %xmm0
	testl	%eax, %eax
	je	.L101
	movq	-1856(%rbp), %rax
	movdqa	-1600(%rbp), %xmm1
	movl	$32, -1720(%rbp)
	subl	$32, -1848(%rbp)
	addq	$2048, %rax
	paddq	%xmm0, %xmm1
	subl	$32, -1832(%rbp)
	movq	%rax, -1856(%rbp)
	movq	%rax, -1728(%rbp)
	movq	-1592(%rbp), %rax
	movaps	%xmm1, -1600(%rbp)
	movdqu	-1560(%rbp), %xmm1
	movdqu	2032(%rax), %xmm5
	movq	-1840(%rbp), %rax
	movl	$128, -1584(%rbp)
	paddq	%xmm0, %xmm1
	subl	$32, -1816(%rbp)
	addq	$2048, %rax
	movups	%xmm5, -1576(%rbp)
	movq	%rax, -1840(%rbp)
	movq	%rax, -1712(%rbp)
	movq	-1552(%rbp), %rax
	movups	%xmm1, -1560(%rbp)
	movdqa	-1520(%rbp), %xmm1
	movdqu	2032(%rax), %xmm6
	movq	-1824(%rbp), %rax
	movl	$32, -1704(%rbp)
	movl	$128, -1544(%rbp)
	paddq	%xmm0, %xmm1
	addq	$2048, %rax
	movaps	%xmm6, -1536(%rbp)
	movq	%rax, -1824(%rbp)
	movq	%rax, -1696(%rbp)
	movq	-1512(%rbp), %rax
	movl	$32, -1688(%rbp)
	movaps	%xmm1, -1520(%rbp)
	movdqu	2032(%rax), %xmm7
	movq	-1808(%rbp), %rax
	movdqu	-1480(%rbp), %xmm1
	subl	$32, -1800(%rbp)
	addq	$2048, %rax
	cmpl	$4, -1860(%rbp)
	movl	$128, -1504(%rbp)
	movq	%rax, -1808(%rbp)
	paddq	%xmm0, %xmm1
	movq	%rax, -1680(%rbp)
	movq	-1472(%rbp), %rax
	movl	$32, -1672(%rbp)
	movdqu	2032(%rax), %xmm5
	movups	%xmm7, -1496(%rbp)
	movl	$128, -1464(%rbp)
	movups	%xmm1, -1480(%rbp)
	movaps	%xmm5, -1456(%rbp)
	je	.L101
	movq	-1792(%rbp), %rax
	movdqa	-1440(%rbp), %xmm1
	movl	$32, -1656(%rbp)
	subl	$32, -1784(%rbp)
	addq	$2048, %rax
	paddq	%xmm0, %xmm1
	subl	$32, -1768(%rbp)
	movq	%rax, -1792(%rbp)
	movq	%rax, -1664(%rbp)
	movq	-1432(%rbp), %rax
	movaps	%xmm1, -1440(%rbp)
	movdqu	-1400(%rbp), %xmm1
	movdqu	2032(%rax), %xmm6
	movq	-1776(%rbp), %rax
	movl	$128, -1424(%rbp)
	paddq	%xmm0, %xmm1
	subl	$32, -1752(%rbp)
	addq	$2048, %rax
	movups	%xmm6, -1416(%rbp)
	movq	%rax, -1776(%rbp)
	movq	%rax, -1648(%rbp)
	movq	-1392(%rbp), %rax
	movups	%xmm1, -1400(%rbp)
	movdqa	-1360(%rbp), %xmm1
	movdqu	2032(%rax), %xmm7
	movq	-1760(%rbp), %rax
	movl	$32, -1640(%rbp)
	movl	$128, -1384(%rbp)
	paddq	%xmm0, %xmm1
	addq	$2048, %rax
	movaps	%xmm7, -1376(%rbp)
	movq	%rax, -1760(%rbp)
	movq	%rax, -1632(%rbp)
	movq	-1352(%rbp), %rax
	movl	$32, -1624(%rbp)
	movaps	%xmm1, -1360(%rbp)
	movdqu	2032(%rax), %xmm6
	movq	-1744(%rbp), %rax
	movdqu	-1320(%rbp), %xmm1
	subl	$32, -1736(%rbp)
	addq	$2048, %rax
	movl	$128, -1344(%rbp)
	movq	%rax, -1744(%rbp)
	paddq	%xmm0, %xmm1
	movq	%rax, -1616(%rbp)
	movq	-1312(%rbp), %rax
	movl	$32, -1608(%rbp)
	movdqu	2032(%rax), %xmm7
	movups	%xmm6, -1336(%rbp)
	movl	$128, -1304(%rbp)
	movups	%xmm1, -1320(%rbp)
	movaps	%xmm7, -1296(%rbp)
.L101:
	addl	$2048, -1900(%rbp)
	subl	$32, %ebx
	cmpl	$32, %ebx
	ja	.L97
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L157:
	movq	%r12, %r14
	movq	-1952(%rbp), %r12
	movl	-1904(%rbp), %edx
	movq	%r14, %rsi
	movq	-1960(%rbp), %r13
	movq	%r12, %rdi
	call	sha1_multi_block@PLT
	movq	-1880(%rbp), %rdi
	xorl	%eax, %eax
	movl	$128, %ecx
	movl	340(%r13), %r8d
	movl	356(%r13), %edx
	rep stosq
	movl	348(%r13), %esi
	movl	344(%r13), %edi
	movb	$-128, -1260(%rbp)
	movl	352(%r13), %ecx
	movl	(%r12), %eax
	movl	%r8d, (%r12)
	movl	4(%r12), %r9d
	movl	%r8d, 4(%r12)
#APP
# 322 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %eax
# 0 "" 2
# 322 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%eax, -1280(%rbp)
	movl	32(%r12), %eax
	movl	%r9d, -1152(%rbp)
	movl	36(%r12), %r9d
#APP
# 324 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %eax
# 0 "" 2
# 324 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%eax, -1276(%rbp)
	movl	64(%r12), %eax
	movl	%r9d, -1148(%rbp)
	movl	68(%r12), %r9d
#APP
# 326 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %eax
# 0 "" 2
# 326 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%eax, -1272(%rbp)
	movl	96(%r12), %eax
	movl	%r9d, -1144(%rbp)
#APP
# 328 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %eax
# 0 "" 2
#NO_APP
	movl	%edi, 32(%r12)
	movl	%eax, -1268(%rbp)
	movl	128(%r12), %eax
	movl	%esi, 64(%r12)
#APP
# 330 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %eax
# 0 "" 2
#NO_APP
	movl	%ecx, 96(%r12)
	movl	%eax, -1264(%rbp)
	movl	$672, %eax
	movl	%edx, 128(%r12)
#APP
# 333 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %eax
# 0 "" 2
#NO_APP
	movl	%edi, 36(%r12)
	movl	%eax, -1220(%rbp)
	movl	%esi, 68(%r12)
	movl	100(%r12), %r9d
	movb	$-128, -1132(%rbp)
#APP
# 328 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%ecx, 100(%r12)
	movl	%r9d, -1140(%rbp)
	movl	132(%r12), %r9d
	movl	%eax, -1092(%rbp)
#APP
# 330 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%edx, 132(%r12)
	movl	%r9d, -1136(%rbp)
	movl	8(%r12), %r9d
	movb	$-128, -1004(%rbp)
#APP
# 322 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r8d, 8(%r12)
	movl	%r9d, -1024(%rbp)
	movl	40(%r12), %r9d
	movl	%eax, -964(%rbp)
#APP
# 324 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%edi, 40(%r12)
	movl	%r9d, -1020(%rbp)
	movl	72(%r12), %r9d
	movl	%esi, 72(%r12)
#APP
# 326 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -1016(%rbp)
	movl	104(%r12), %r9d
	movl	%ecx, 104(%r12)
#APP
# 328 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -1012(%rbp)
	movl	136(%r12), %r9d
	movl	%edx, 136(%r12)
#APP
# 330 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -1008(%rbp)
	movl	12(%r12), %r9d
	movl	%r8d, 12(%r12)
#APP
# 322 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -896(%rbp)
	movl	44(%r12), %r9d
	movl	%edi, 44(%r12)
#APP
# 324 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -892(%rbp)
	movl	76(%r12), %r9d
#APP
# 326 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -888(%rbp)
	movl	108(%r12), %r9d
#APP
# 328 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -884(%rbp)
	movl	140(%r12), %r9d
#APP
# 330 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	cmpl	$4, -1860(%rbp)
	movl	%esi, 76(%r12)
	movl	%ecx, 108(%r12)
	movl	%r9d, -880(%rbp)
	movl	%edx, 140(%r12)
	movb	$-128, -876(%rbp)
	movl	%eax, -836(%rbp)
	je	.L117
	movl	16(%r12), %r9d
	movb	$-128, -748(%rbp)
	movl	%r8d, 16(%r12)
#APP
# 322 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%eax, -708(%rbp)
	movl	%r9d, -768(%rbp)
	movl	48(%r12), %r9d
	movl	%edi, 48(%r12)
#APP
# 324 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -764(%rbp)
	movl	80(%r12), %r9d
	movl	%esi, 80(%r12)
#APP
# 326 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -760(%rbp)
	movl	112(%r12), %r9d
	movl	%ecx, 112(%r12)
#APP
# 328 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -756(%rbp)
	movl	144(%r12), %r9d
	movl	%edx, 144(%r12)
#APP
# 330 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -752(%rbp)
	movl	20(%r12), %r9d
	movl	%r8d, 20(%r12)
#APP
# 322 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -640(%rbp)
	movl	52(%r12), %r9d
	movl	%edi, 52(%r12)
#APP
# 324 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -636(%rbp)
	movl	84(%r12), %r9d
	movl	%esi, 84(%r12)
#APP
# 326 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -632(%rbp)
	movl	116(%r12), %r9d
	movl	%ecx, 116(%r12)
#APP
# 328 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -628(%rbp)
	movl	148(%r12), %r9d
	movl	%edx, 148(%r12)
#APP
# 330 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -624(%rbp)
	movb	$-128, -620(%rbp)
	movl	24(%r12), %r9d
	movl	%eax, -580(%rbp)
	movl	%r8d, 24(%r12)
#APP
# 322 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movb	$-128, -492(%rbp)
	movl	%r9d, -512(%rbp)
	movl	56(%r12), %r9d
	movl	%eax, -452(%rbp)
	movl	%edi, 56(%r12)
#APP
# 324 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -508(%rbp)
	movl	88(%r12), %r9d
	movl	%esi, 88(%r12)
#APP
# 326 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -504(%rbp)
	movl	120(%r12), %r9d
	movl	%ecx, 120(%r12)
#APP
# 328 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -500(%rbp)
	movl	152(%r12), %r9d
	movl	%edx, 152(%r12)
#APP
# 330 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -496(%rbp)
	movl	28(%r12), %r9d
	movl	%r8d, 28(%r12)
	movl	60(%r12), %r8d
#APP
# 322 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%edi, 60(%r12)
	movl	92(%r12), %edi
	movl	%r9d, -384(%rbp)
	movl	%esi, 92(%r12)
	movl	124(%r12), %esi
#APP
# 324 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %r8d
# 0 "" 2
#NO_APP
	movl	%ecx, 124(%r12)
	movl	156(%r12), %ecx
	movl	%r8d, -380(%rbp)
#APP
# 326 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %edi
# 0 "" 2
# 328 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %esi
# 0 "" 2
#NO_APP
	movl	%edi, -376(%rbp)
	movl	%esi, -372(%rbp)
#APP
# 330 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %ecx
# 0 "" 2
#NO_APP
	movl	%ecx, -368(%rbp)
	movl	%edx, 156(%r12)
	movb	$-128, -364(%rbp)
	movl	%eax, -324(%rbp)
.L117:
	cmpl	$4, -1860(%rbp)
	movl	$1, -1720(%rbp)
	movl	$1, -1704(%rbp)
	movl	$1, -1688(%rbp)
	movl	$1, -1672(%rbp)
	je	.L118
	movl	$1, -1656(%rbp)
	movl	$1, -1640(%rbp)
	movl	$1, -1624(%rbp)
	movl	$1, -1608(%rbp)
.L118:
	movq	-1880(%rbp), %rax
	cmpl	$4, -1860(%rbp)
	movq	%rax, -1728(%rbp)
	leaq	-1152(%rbp), %rax
	movq	%rax, -1712(%rbp)
	leaq	-1024(%rbp), %rax
	movq	%rax, -1696(%rbp)
	leaq	-896(%rbp), %rax
	movq	%rax, -1680(%rbp)
	je	.L119
	leaq	-768(%rbp), %rax
	movq	%rax, -1664(%rbp)
	leaq	-640(%rbp), %rax
	movq	%rax, -1648(%rbp)
	leaq	-512(%rbp), %rax
	movq	%rax, -1632(%rbp)
	leaq	-384(%rbp), %rax
	movq	%rax, -1616(%rbp)
.L119:
	movl	-1904(%rbp), %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r12, %rbx
	xorl	%r15d, %r15d
	call	sha1_multi_block@PLT
	movl	$1, %eax
	subl	-1900(%rbp), %eax
	movq	%r12, -1920(%rbp)
	movl	%eax, -1912(%rbp)
	movq	-1936(%rbp), %r14
	movq	$0, -1896(%rbp)
	movq	-1944(%rbp), %r12
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L163:
	testb	$4, %cl
	jne	.L160
	testl	%ecx, %ecx
	je	.L110
	movb	%dil, 20(%rax)
	testb	$2, %cl
	jne	.L161
.L110:
	movl	-1912(%rbp), %eax
	addl	%esi, %edx
	addq	%r10, %rcx
	addl	$1, %r15d
	addq	$40, %r14
	addq	$4, %rbx
	addl	%edx, %eax
	shrl	$4, %eax
	movl	%eax, -24(%r14)
	movzbl	472(%r13), %eax
	movb	%al, (%r12)
	movzbl	473(%r13), %eax
	movb	%al, 1(%r12)
	movzbl	474(%r13), %eax
	movb	%al, 2(%r12)
	leal	17(%rdx), %eax
	rolw	$8, %ax
	movw	%ax, 3(%r12)
	leal	22(%rdx), %eax
	addq	%rax, -1896(%rbp)
	cmpl	%r15d, -1860(%rbp)
	je	.L162
	movq	%rcx, %r12
.L115:
	cmpl	-1872(%rbp), %r15d
	movl	-1864(%rbp), %ecx
	cmove	-1868(%rbp), %ecx
	movq	8(%r14), %rdi
	movq	(%r14), %rsi
	movl	%ecx, %edx
	subl	-1900(%rbp), %edx
	movl	%ecx, -1888(%rbp)
	call	memcpy@PLT
	movl	-1888(%rbp), %ecx
	movq	8(%r14), %rax
	movabsq	$72340172838076673, %r11
	movl	(%rbx), %edx
	movq	%rax, (%r14)
	leal	21(%rcx), %eax
	leal	20(%rcx), %esi
	addq	%r12, %rax
#APP
# 365 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %edx
# 0 "" 2
#NO_APP
	movl	%edx, (%rax)
	movl	32(%rbx), %edx
	leaq	20(%rax), %r10
#APP
# 366 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %edx
# 0 "" 2
#NO_APP
	movl	%edx, 4(%rax)
	movl	64(%rbx), %edx
#APP
# 367 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %edx
# 0 "" 2
#NO_APP
	movl	%edx, 8(%rax)
	movl	96(%rbx), %edx
#APP
# 368 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %edx
# 0 "" 2
#NO_APP
	movl	%edx, 12(%rax)
	movl	128(%rbx), %edx
#APP
# 369 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapl %edx
# 0 "" 2
#NO_APP
	movl	%edx, 16(%rax)
	movl	%esi, %edx
	notl	%edx
	andl	$15, %edx
	movl	%edx, %ecx
	movzbl	%dl, %edi
	addq	$1, %rcx
	imulq	%r11, %rdi
	cmpl	$8, %ecx
	jb	.L163
	movq	%rdi, 20(%rax)
	addq	$28, %rax
	movq	%r10, %r9
	andq	$-8, %rax
	movq	%rdi, -8(%r10,%rcx)
	subq	%rax, %r9
	addl	%ecx, %r9d
	andl	$-8, %r9d
	cmpl	$8, %r9d
	jb	.L110
	andl	$-8, %r9d
	xorl	%r8d, %r8d
.L113:
	movl	%r8d, %r11d
	addl	$8, %r8d
	movq	%rdi, (%rax,%r11)
	cmpl	%r9d, %r8d
	jb	.L113
	jmp	.L110
.L160:
	movl	%edi, 20(%rax)
	movl	%edi, -4(%r10,%rcx)
	jmp	.L110
.L161:
	movw	%di, -2(%r10,%rcx)
	jmp	.L110
.L90:
	movq	464(%r13), %r9
	movl	-1860(%rbp), %edx
	movq	%r9, -1280(%rbp)
#APP
# 210 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c" 1
	bswapq %r9
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L121
	movl	-1904(%rbp), %edx
	leaq	-1728(%rbp), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	sha1_multi_block@PLT
	cmpl	$2111, -1928(%rbp)
	jbe	.L96
	leaq	-1856(%rbp), %rax
	movq	%rax, -1912(%rbp)
	jmp	.L98
.L96:
	movl	-1904(%rbp), %edx
	movq	%r12, %rdi
	leaq	-1856(%rbp), %rsi
	call	sha1_multi_block@PLT
	movq	-1880(%rbp), %rdi
	xorl	%eax, %eax
	movl	$128, %ecx
	rep stosq
	leaq	-1600(%rbp), %rax
	movq	%rax, -1936(%rbp)
.L102:
	movl	-1904(%rbp), %ebx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%ebx, %edx
	call	sha1_multi_block@PLT
	movq	-1880(%rbp), %rdi
	xorl	%eax, %eax
	movl	%ebx, %edx
	movl	$128, %ecx
	movq	%r14, %rsi
	rep stosq
	movq	%r12, %rdi
	call	sha1_multi_block@PLT
	movq	$0, -1896(%rbp)
	jmp	.L116
.L159:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE407:
	.size	tls1_1_multi_block_encrypt, .-tls1_1_multi_block_encrypt
	.p2align 4
	.type	aesni_cbc_hmac_sha1_ctrl, @function
aesni_cbc_hmac_sha1_ctrl:
.LFB409:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	leal	-22(%rbx), %esi
	cmpl	$6, %esi
	ja	.L184
	leaq	.L167(%rip), %rdx
	movq	%rax, %r13
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L167:
	.long	.L171-.L167
	.long	.L170-.L167
	.long	.L184-.L167
	.long	.L169-.L167
	.long	.L168-.L167
	.long	.L184-.L167
	.long	.L166-.L167
	.text
	.p2align 4,,10
	.p2align 3
.L184:
	movl	$-1, %eax
	.p2align 4,,10
	.p2align 3
.L164:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L191
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	cmpl	$13, %r12d
	jne	.L184
	movzwl	11(%r14), %r12d
	movq	%r15, %rdi
	rolw	$8, %r12w
	movzwl	%r12w, %ebx
	call	EVP_CIPHER_CTX_encrypting@PLT
	testl	%eax, %eax
	je	.L175
	movzwl	%r12w, %eax
	movq	%rax, 536(%r13)
	movzwl	9(%r14), %eax
	rolw	$8, %ax
	movzwl	%ax, %edx
	movl	%edx, 544(%r13)
	cmpw	$769, %ax
	jbe	.L176
	cmpw	$15, %r12w
	jbe	.L180
	subl	$16, %ebx
	movl	%ebx, %eax
	rolw	$8, %ax
	movw	%ax, 11(%r14)
.L176:
	movdqu	244(%r13), %xmm2
	leaq	436(%r13), %rdi
	movq	%r14, %rsi
	movdqu	260(%r13), %xmm3
	movdqu	276(%r13), %xmm4
	movl	$13, %edx
	movdqu	292(%r13), %xmm5
	movdqu	308(%r13), %xmm6
	movups	%xmm2, 436(%r13)
	movdqu	324(%r13), %xmm7
	movups	%xmm3, 452(%r13)
	movups	%xmm4, 468(%r13)
	movups	%xmm5, 484(%r13)
	movups	%xmm6, 500(%r13)
	movups	%xmm7, 516(%r13)
	call	sha1_update
	leal	36(%rbx), %eax
	andl	$-16, %eax
	subl	%ebx, %eax
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L170:
	pxor	%xmm0, %xmm0
	leaq	-128(%rbp), %r15
	leaq	244(%r13), %rbx
	movslq	%r12d, %rdx
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	cmpl	$64, %r12d
	jle	.L172
	movq	%rbx, %rdi
	movq	%rdx, -136(%rbp)
	call	SHA1_Init@PLT
	movq	-136(%rbp), %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	sha1_update
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	SHA1_Final@PLT
.L173:
	movdqa	.LC1(%rip), %xmm0
	movdqa	-128(%rbp), %xmm1
	movq	%rbx, %rdi
	addq	$340, %r13
	pxor	%xmm0, %xmm1
	movaps	%xmm1, -128(%rbp)
	movdqa	-112(%rbp), %xmm1
	pxor	%xmm0, %xmm1
	movaps	%xmm1, -112(%rbp)
	movdqa	-96(%rbp), %xmm1
	pxor	%xmm0, %xmm1
	pxor	-80(%rbp), %xmm0
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	SHA1_Init@PLT
	movl	$64, %edx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	sha1_update
	movdqa	.LC2(%rip), %xmm0
	movdqa	-128(%rbp), %xmm1
	movq	%r13, %rdi
	pxor	%xmm0, %xmm1
	movaps	%xmm1, -128(%rbp)
	movdqa	-112(%rbp), %xmm1
	pxor	%xmm0, %xmm1
	movaps	%xmm1, -112(%rbp)
	movdqa	-96(%rbp), %xmm1
	pxor	%xmm0, %xmm1
	pxor	-80(%rbp), %xmm0
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	SHA1_Init@PLT
	movl	$64, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	sha1_update
	movl	$64, %esi
	movq	%r15, %rdi
	call	OPENSSL_cleanse@PLT
	movl	$1, %eax
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L169:
	cmpl	$31, %r12d
	jle	.L184
	movq	8(%r14), %rax
	movq	%r15, %rdi
	movzwl	11(%rax), %r12d
	rolw	$8, %r12w
	call	EVP_CIPHER_CTX_encrypting@PLT
	testl	%eax, %eax
	je	.L184
	movq	8(%r14), %rsi
	movzwl	9(%rsi), %eax
	rolw	$8, %ax
	cmpw	$769, %ax
	jbe	.L184
	testw	%r12w, %r12w
	je	.L179
	movzwl	%r12w, %ebx
	cmpw	$4095, %r12w
	jbe	.L180
	cmpl	$8191, %ebx
	jbe	.L185
	movl	8+OPENSSL_ia32cap_P(%rip), %eax
	andl	$32, %eax
	cmpl	$1, %eax
	sbbl	%r15d, %r15d
	addl	$3, %r15d
	cmpl	$1, %eax
	sbbl	%r12d, %r12d
	andl	$-4, %r12d
	addl	$8, %r12d
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L168:
	movl	24(%r14), %r8d
	movq	16(%r14), %rcx
	movq	%r13, %rdi
	movq	8(%r14), %rdx
	movq	(%r14), %rsi
	shrl	$2, %r8d
	call	tls1_1_multi_block_encrypt
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L166:
	addl	$36, %r12d
	andl	$-16, %r12d
	leal	21(%r12), %eax
	jmp	.L164
.L180:
	xorl	%eax, %eax
	jmp	.L164
.L172:
	movl	$64, %ecx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	__memcpy_chk@PLT
	jmp	.L173
.L179:
	movl	24(%r14), %eax
	movl	%eax, %r15d
	subl	$4, %eax
	shrl	$2, %r15d
	cmpl	$7, %eax
	ja	.L184
	movl	16(%r14), %ebx
	leal	0(,%r15,4), %r12d
	addl	$1, %r15d
.L181:
	movdqu	244(%r13), %xmm2
	movl	$13, %edx
	movdqu	260(%r13), %xmm3
	leaq	436(%r13), %rdi
	movdqu	276(%r13), %xmm4
	movdqu	292(%r13), %xmm5
	movdqu	308(%r13), %xmm6
	movups	%xmm2, 436(%r13)
	movdqu	324(%r13), %xmm7
	movups	%xmm3, 452(%r13)
	movups	%xmm4, 468(%r13)
	movups	%xmm5, 484(%r13)
	movups	%xmm6, 500(%r13)
	movups	%xmm7, 516(%r13)
	call	sha1_update
	movl	%ebx, %edx
	movl	%r15d, %ecx
	shrl	%cl, %edx
	leal	(%rbx,%rdx), %eax
	movl	%edx, %ebx
	sall	%cl, %ebx
	subl	%ebx, %eax
	cmpl	%eax, %edx
	jnb	.L182
	leal	22(%rax), %ecx
	leal	-1(%r12), %esi
	andl	$63, %ecx
	cmpl	%esi, %ecx
	jnb	.L182
	subl	%r12d, %eax
	addl	$1, %edx
	addl	$1, %eax
.L182:
	addl	$36, %edx
	addl	$36, %eax
	movl	%r15d, %ecx
	movl	%r12d, 24(%r14)
	andl	$-16, %edx
	andl	$-16, %eax
	addl	$21, %edx
	movl	%edx, %ebx
	sall	%cl, %ebx
	leal	21(%rax,%rbx), %eax
	subl	%edx, %eax
	jmp	.L164
.L175:
	movq	(%r14), %rdx
	movl	$20, %eax
	movq	%rdx, 544(%r13)
	movl	8(%r14), %edx
	movl	%edx, 552(%r13)
	movzbl	12(%r14), %edx
	movq	$13, 536(%r13)
	movb	%dl, 556(%r13)
	jmp	.L164
.L185:
	movl	$2, %r15d
	movl	$4, %r12d
	jmp	.L181
.L191:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE409:
	.size	aesni_cbc_hmac_sha1_ctrl, .-aesni_cbc_hmac_sha1_ctrl
	.p2align 4
	.globl	EVP_aes_128_cbc_hmac_sha1
	.type	EVP_aes_128_cbc_hmac_sha1, @function
EVP_aes_128_cbc_hmac_sha1:
.LFB410:
	.cfi_startproc
	endbr64
	testb	$2, 7+OPENSSL_ia32cap_P(%rip)
	movl	$0, %edx
	leaq	aesni_128_cbc_hmac_sha1_cipher(%rip), %rax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE410:
	.size	EVP_aes_128_cbc_hmac_sha1, .-EVP_aes_128_cbc_hmac_sha1
	.p2align 4
	.globl	EVP_aes_256_cbc_hmac_sha1
	.type	EVP_aes_256_cbc_hmac_sha1, @function
EVP_aes_256_cbc_hmac_sha1:
.LFB411:
	.cfi_startproc
	endbr64
	testb	$2, 7+OPENSSL_ia32cap_P(%rip)
	movl	$0, %edx
	leaq	aesni_256_cbc_hmac_sha1_cipher(%rip), %rax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE411:
	.size	EVP_aes_256_cbc_hmac_sha1, .-EVP_aes_256_cbc_hmac_sha1
	.section	.data.rel.local,"aw"
	.align 32
	.type	aesni_256_cbc_hmac_sha1_cipher, @object
	.size	aesni_256_cbc_hmac_sha1_cipher, 88
aesni_256_cbc_hmac_sha1_cipher:
	.long	918
	.long	16
	.long	32
	.long	16
	.quad	6295554
	.quad	aesni_cbc_hmac_sha1_init_key
	.quad	aesni_cbc_hmac_sha1_cipher
	.quad	0
	.long	560
	.zero	4
	.quad	0
	.quad	0
	.quad	aesni_cbc_hmac_sha1_ctrl
	.quad	0
	.align 32
	.type	aesni_128_cbc_hmac_sha1_cipher, @object
	.size	aesni_128_cbc_hmac_sha1_cipher, 88
aesni_128_cbc_hmac_sha1_cipher:
	.long	916
	.long	16
	.long	16
	.long	16
	.quad	6295554
	.quad	aesni_cbc_hmac_sha1_init_key
	.quad	aesni_cbc_hmac_sha1_cipher
	.quad	0
	.long	560
	.zero	4
	.quad	0
	.quad	0
	.quad	aesni_cbc_hmac_sha1_ctrl
	.quad	0
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.quad	2048
	.quad	2048
	.align 16
.LC1:
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.align 16
.LC2:
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
