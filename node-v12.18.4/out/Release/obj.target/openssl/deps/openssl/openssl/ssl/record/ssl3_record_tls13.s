	.file	"ssl3_record_tls13.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/ssl/record/ssl3_record_tls13.c"
	.text
	.p2align 4
	.globl	tls13_enc
	.type	tls13_enc, @function
tls13_enc:
.LFB1001:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$40, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$1, %rdx
	jne	.L71
	movq	%rsi, %rbx
	movl	%ecx, %r14d
	testl	%ecx, %ecx
	jne	.L74
	movq	1088(%rdi), %r15
	leaq	1096(%rdi), %r11
	leaq	6104(%rdi), %r9
.L5:
	testq	%r15, %r15
	je	.L6
	cmpl	$21, 4(%rbx)
	je	.L6
	movq	%r15, %rdi
	movq	%r9, -176(%rbp)
	movq	%r11, -168(%rbp)
	call	EVP_CIPHER_CTX_iv_length@PLT
	movq	-168(%rbp), %r11
	movq	-176(%rbp), %r9
	movslq	%eax, %r13
	movl	132(%r12), %eax
	subl	$3, %eax
	cmpl	$1, %eax
	jbe	.L75
	movq	168(%r12), %rax
	movq	568(%rax), %rax
	testq	%rax, %rax
	je	.L76
	movl	36(%rax), %eax
.L10:
	movl	%eax, %ecx
	andl	$245760, %ecx
	movl	%ecx, -200(%rbp)
	jne	.L77
	testl	$536576, %eax
	je	.L78
	movl	$16, -204(%rbp)
	movl	$16, %r10d
	testl	%r14d, %r14d
	je	.L79
	movq	$16, -168(%rbp)
.L16:
	cmpq	$7, %r13
	jbe	.L80
	leaq	-80(%rbp), %r8
	leaq	-8(%r13), %rdx
	movq	%r11, %rsi
	movl	$16, %ecx
	movq	%r8, %rdi
	movl	%r10d, -196(%rbp)
	movq	%r9, -192(%rbp)
	movq	%rdx, -184(%rbp)
	movq	%r11, -176(%rbp)
	call	__memcpy_chk@PLT
	movq	-192(%rbp), %r9
	movq	-176(%rbp), %r11
	movq	-184(%rbp), %rdx
	movq	%rax, %r8
	movl	-196(%rbp), %r10d
	movzbl	-8(%r11,%r13), %eax
	xorb	(%r9), %al
	movb	%al, -80(%rbp,%rdx)
	movzbl	-7(%r11,%r13), %eax
	xorb	1(%r9), %al
	movzbl	-1(%r11,%r13), %edx
	movb	%al, -87(%rbp,%r13)
	movzbl	-6(%r11,%r13), %eax
	xorb	2(%r9), %al
	movb	%al, -86(%rbp,%r13)
	movzbl	-5(%r11,%r13), %eax
	xorb	3(%r9), %al
	movb	%al, -85(%rbp,%r13)
	movzbl	-4(%r11,%r13), %eax
	xorb	4(%r9), %al
	movb	%al, -84(%rbp,%r13)
	movzbl	-3(%r11,%r13), %eax
	xorb	5(%r9), %al
	movb	%al, -83(%rbp,%r13)
	movzbl	-2(%r11,%r13), %eax
	xorb	6(%r9), %al
	movb	%al, -82(%rbp,%r13)
	movzbl	7(%r9), %eax
	xorl	%eax, %edx
	addl	$1, %eax
	testb	%al, %al
	movb	%dl, -81(%rbp,%r13)
	movb	%al, 7(%r9)
	jne	.L19
	addb	$1, 6(%r9)
	jne	.L19
	addb	$1, 5(%r9)
	jne	.L19
	addb	$1, 4(%r9)
	jne	.L19
	addb	$1, 3(%r9)
	jne	.L19
	addb	$1, 2(%r9)
	jne	.L19
	addb	$1, 1(%r9)
	jne	.L19
	addb	$1, (%r9)
	je	.L72
	.p2align 4,,10
	.p2align 3
.L19:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	%r14d, %r9d
	movq	%r15, %rdi
	movl	%r10d, -176(%rbp)
	call	EVP_CipherInit_ex@PLT
	movl	-176(%rbp), %r10d
	testl	%eax, %eax
	jle	.L72
	testl	%r14d, %r14d
	jne	.L24
	movq	8(%rbx), %rcx
	movl	%r10d, %edx
	addq	32(%rbx), %rcx
	movl	$17, %esi
	movq	%r15, %rdi
	movl	%r10d, -176(%rbp)
	call	EVP_CIPHER_CTX_ctrl@PLT
	movl	-176(%rbp), %r10d
	testl	%eax, %eax
	jle	.L72
.L24:
	leaq	-85(%rbp), %rax
	leaq	-144(%rbp), %r13
	xorl	%ecx, %ecx
	movl	$5, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	movl	%r10d, -176(%rbp)
	movq	%rax, -184(%rbp)
	call	WPACKET_init_static_len@PLT
	movl	-176(%rbp), %r10d
	testl	%eax, %eax
	je	.L21
	movl	4(%rbx), %esi
	movl	$1, %edx
	movq	%r13, %rdi
	movl	%r10d, -176(%rbp)
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L21
	movl	(%rbx), %esi
	movl	$2, %edx
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L21
	movl	-204(%rbp), %esi
	movl	$2, %edx
	addl	8(%rbx), %esi
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L21
	leaq	-152(%rbp), %rsi
	movq	%r13, %rdi
	call	WPACKET_get_total_written@PLT
	testl	%eax, %eax
	je	.L21
	cmpq	$5, -152(%rbp)
	jne	.L21
	movq	%r13, %rdi
	call	WPACKET_finish@PLT
	testl	%eax, %eax
	je	.L21
	cmpl	$0, -200(%rbp)
	movl	-176(%rbp), %r10d
	jne	.L25
	leaq	-160(%rbp), %rdx
.L27:
	movq	-184(%rbp), %rcx
	xorl	%esi, %esi
	movl	$5, %r8d
	movq	%r15, %rdi
	movl	%r10d, -192(%rbp)
	movq	%rdx, -176(%rbp)
	call	EVP_CipherUpdate@PLT
	movq	-176(%rbp), %rdx
	movl	-192(%rbp), %r10d
	testl	%eax, %eax
	jle	.L72
	movq	40(%rbx), %rcx
	movq	32(%rbx), %rsi
	movq	%r15, %rdi
	movl	%r10d, -176(%rbp)
	movl	8(%rbx), %r8d
	call	EVP_CipherUpdate@PLT
	testl	%eax, %eax
	jle	.L72
	movslq	-160(%rbp), %rsi
	leaq	-156(%rbp), %rdx
	addq	32(%rbx), %rsi
	movq	%r15, %rdi
	call	EVP_CipherFinal_ex@PLT
	testl	%eax, %eax
	jle	.L72
	movl	-156(%rbp), %eax
	addl	-160(%rbp), %eax
	cltq
	cmpq	8(%rbx), %rax
	jne	.L72
	testl	%r14d, %r14d
	movl	-176(%rbp), %r10d
	movl	$1, %r8d
	je	.L1
	addq	32(%rbx), %rax
	movl	%r10d, %edx
	movl	$16, %esi
	movq	%r15, %rdi
	movq	%rax, %rcx
	movl	%r8d, -176(%rbp)
	call	EVP_CIPHER_CTX_ctrl@PLT
	movl	-176(%rbp), %r8d
	movl	$188, %r9d
	testl	%eax, %eax
	jle	.L71
	movq	-168(%rbp), %rax
	addq	%rax, 8(%rbx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L11:
	movl	$76, %r9d
	.p2align 4,,10
	.p2align 3
.L71:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	$609, %edx
	movq	%r12, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
.L72:
	movl	$-1, %r8d
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L81
	addq	$168, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	movq	1136(%rdi), %r15
	leaq	1144(%rdi), %r11
	leaq	6112(%rdi), %r9
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L6:
	movq	8(%rbx), %rdx
	movq	40(%rbx), %rsi
	movq	32(%rbx), %rdi
	call	memmove@PLT
	movq	32(%rbx), %rax
	movl	$1, %r8d
	movq	%rax, 40(%rbx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L77:
	andl	$196608, %eax
	cmpl	$1, %eax
	sbbl	%esi, %esi
	andl	$8, %esi
	addl	$8, %esi
	cmpl	$1, %eax
	sbbq	%rdx, %rdx
	movl	%esi, -204(%rbp)
	movl	%esi, %r10d
	andl	$8, %edx
	addq	$9, %rdx
	cmpl	$1, %eax
	sbbq	%rax, %rax
	andl	$8, %eax
	addq	$8, %rax
	movq	%rax, -168(%rbp)
	testl	%r14d, %r14d
	jne	.L82
.L15:
	movq	8(%rbx), %rax
	xorl	%r8d, %r8d
	cmpq	%rdx, %rax
	jb	.L1
	subq	-168(%rbp), %rax
	movq	%rax, 8(%rbx)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L21:
	movq	%r13, %rdi
	call	WPACKET_cleanup@PLT
	movl	$-1, %r8d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L75:
	movq	1296(%r12), %rax
	testq	%rax, %rax
	je	.L9
	movl	580(%rax), %ecx
	testl	%ecx, %ecx
	jne	.L70
.L9:
	movq	1304(%r12), %rax
	testq	%rax, %rax
	je	.L11
	movl	580(%rax), %edx
	testl	%edx, %edx
	je	.L11
.L70:
	movq	504(%rax), %rax
	movl	36(%rax), %eax
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L80:
	movl	$129, %r9d
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L82:
	movl	%esi, -176(%rbp)
	xorl	%ecx, %ecx
	movl	%esi, %edx
	movq	%r15, %rdi
	movl	$17, %esi
	movq	%r9, -192(%rbp)
	movq	%r11, -184(%rbp)
	call	EVP_CIPHER_CTX_ctrl@PLT
	movl	-176(%rbp), %r10d
	movq	-184(%rbp), %r11
	testl	%eax, %eax
	movq	-192(%rbp), %r9
	jg	.L16
	movl	$102, %r9d
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L79:
	movq	$16, -168(%rbp)
	movl	$17, %edx
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L76:
	movl	$88, %r9d
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L78:
	movl	$111, %r9d
	jmp	.L71
.L81:
	call	__stack_chk_fail@PLT
.L25:
	movl	8(%rbx), %r8d
	leaq	-160(%rbp), %rdx
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movl	%r10d, -192(%rbp)
	movq	%rdx, -176(%rbp)
	call	EVP_CipherUpdate@PLT
	movq	-176(%rbp), %rdx
	movl	-192(%rbp), %r10d
	testl	%eax, %eax
	jg	.L27
	jmp	.L72
	.cfi_endproc
.LFE1001:
	.size	tls13_enc, .-tls13_enc
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
