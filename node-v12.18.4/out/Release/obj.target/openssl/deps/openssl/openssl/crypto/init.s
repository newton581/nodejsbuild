	.file	"init.c"
	.text
	.p2align 4
	.type	ossl_init_no_register_atexit_ossl_, @function
ossl_init_no_register_atexit_ossl_:
.LFB931:
	.cfi_startproc
	endbr64
	movl	$1, ossl_init_register_atexit_ossl_ret_(%rip)
	ret
	.cfi_endproc
.LFE931:
	.size	ossl_init_no_register_atexit_ossl_, .-ossl_init_no_register_atexit_ossl_
	.p2align 4
	.type	ossl_init_load_crypto_nodelete_ossl_, @function
ossl_init_load_crypto_nodelete_ossl_:
.LFB933:
	.cfi_startproc
	endbr64
	movl	$1, ossl_init_load_crypto_nodelete_ossl_ret_(%rip)
	ret
	.cfi_endproc
.LFE933:
	.size	ossl_init_load_crypto_nodelete_ossl_, .-ossl_init_load_crypto_nodelete_ossl_
	.p2align 4
	.type	ossl_init_no_load_crypto_strings_ossl_, @function
ossl_init_no_load_crypto_strings_ossl_:
.LFB937:
	.cfi_startproc
	endbr64
	movl	$1, ossl_init_load_crypto_strings_ossl_ret_(%rip)
	ret
	.cfi_endproc
.LFE937:
	.size	ossl_init_no_load_crypto_strings_ossl_, .-ossl_init_no_load_crypto_strings_ossl_
	.p2align 4
	.type	ossl_init_no_add_all_ciphers_ossl_, @function
ossl_init_no_add_all_ciphers_ossl_:
.LFB941:
	.cfi_startproc
	endbr64
	movl	$1, ossl_init_add_all_ciphers_ossl_ret_(%rip)
	ret
	.cfi_endproc
.LFE941:
	.size	ossl_init_no_add_all_ciphers_ossl_, .-ossl_init_no_add_all_ciphers_ossl_
	.p2align 4
	.type	ossl_init_no_add_all_digests_ossl_, @function
ossl_init_no_add_all_digests_ossl_:
.LFB945:
	.cfi_startproc
	endbr64
	movl	$1, ossl_init_add_all_digests_ossl_ret_(%rip)
	ret
	.cfi_endproc
.LFE945:
	.size	ossl_init_no_add_all_digests_ossl_, .-ossl_init_no_add_all_digests_ossl_
	.p2align 4
	.type	ossl_init_engine_dynamic_ossl_, @function
ossl_init_engine_dynamic_ossl_:
.LFB957:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	engine_load_dynamic_int@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	$1, ossl_init_engine_dynamic_ossl_ret_(%rip)
	ret
	.cfi_endproc
.LFE957:
	.size	ossl_init_engine_dynamic_ossl_, .-ossl_init_engine_dynamic_ossl_
	.p2align 4
	.type	ossl_init_engine_rdrand_ossl_, @function
ossl_init_engine_rdrand_ossl_:
.LFB955:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	engine_load_rdrand_int@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	$1, ossl_init_engine_rdrand_ossl_ret_(%rip)
	ret
	.cfi_endproc
.LFE955:
	.size	ossl_init_engine_rdrand_ossl_, .-ossl_init_engine_rdrand_ossl_
	.p2align 4
	.type	ossl_init_engine_openssl_ossl_, @function
ossl_init_engine_openssl_ossl_:
.LFB953:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	engine_load_openssl_int@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	$1, ossl_init_engine_openssl_ossl_ret_(%rip)
	ret
	.cfi_endproc
.LFE953:
	.size	ossl_init_engine_openssl_ossl_, .-ossl_init_engine_openssl_ossl_
	.p2align 4
	.type	ossl_init_async_ossl_, @function
ossl_init_async_ossl_:
.LFB951:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	async_init@PLT
	testl	%eax, %eax
	je	.L14
	movl	$1, async_inited(%rip)
	movl	$1, %eax
.L14:
	movl	%eax, ossl_init_async_ossl_ret_(%rip)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE951:
	.size	ossl_init_async_ossl_, .-ossl_init_async_ossl_
	.p2align 4
	.type	ossl_init_config_ossl_, @function
ossl_init_config_ossl_:
.LFB947:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	conf_settings(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	openssl_config_int@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%eax, ossl_init_config_ossl_ret_(%rip)
	ret
	.cfi_endproc
.LFE947:
	.size	ossl_init_config_ossl_, .-ossl_init_config_ossl_
	.p2align 4
	.type	ossl_init_no_config_ossl_, @function
ossl_init_no_config_ossl_:
.LFB949:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	openssl_no_config_int@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	$1, ossl_init_config_ossl_ret_(%rip)
	ret
	.cfi_endproc
.LFE949:
	.size	ossl_init_no_config_ossl_, .-ossl_init_no_config_ossl_
	.p2align 4
	.type	ossl_init_add_all_digests_ossl_, @function
ossl_init_add_all_digests_ossl_:
.LFB943:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	openssl_add_all_digests_int@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	$1, ossl_init_add_all_digests_ossl_ret_(%rip)
	ret
	.cfi_endproc
.LFE943:
	.size	ossl_init_add_all_digests_ossl_, .-ossl_init_add_all_digests_ossl_
	.p2align 4
	.type	ossl_init_add_all_ciphers_ossl_, @function
ossl_init_add_all_ciphers_ossl_:
.LFB939:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	openssl_add_all_ciphers_int@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	$1, ossl_init_add_all_ciphers_ossl_ret_(%rip)
	ret
	.cfi_endproc
.LFE939:
	.size	ossl_init_add_all_ciphers_ossl_, .-ossl_init_add_all_ciphers_ossl_
	.p2align 4
	.type	ossl_init_load_crypto_strings_ossl_, @function
ossl_init_load_crypto_strings_ossl_:
.LFB935:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	err_load_crypto_strings_int@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	$1, load_crypto_strings_inited(%rip)
	movl	%eax, ossl_init_load_crypto_strings_ossl_ret_(%rip)
	ret
	.cfi_endproc
.LFE935:
	.size	ossl_init_load_crypto_strings_ossl_, .-ossl_init_load_crypto_strings_ossl_
	.p2align 4
	.type	ossl_init_register_atexit_ossl_, @function
ossl_init_register_atexit_ossl_:
.LFB929:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	OPENSSL_cleanup(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	atexit@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
	movl	%eax, ossl_init_register_atexit_ossl_ret_(%rip)
	ret
	.cfi_endproc
.LFE929:
	.size	ossl_init_register_atexit_ossl_, .-ossl_init_register_atexit_ossl_
	.p2align 4
	.type	ossl_init_base_ossl_, @function
ossl_init_base_ossl_:
.LFB927:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	ossl_init_thread_destructor(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-28(%rbp), %r12
	movq	%r12, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	CRYPTO_THREAD_init_local@PLT
	testl	%eax, %eax
	jne	.L39
.L32:
	movl	%eax, ossl_init_base_ossl_ret_(%rip)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L40
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	call	CRYPTO_THREAD_lock_new@PLT
	movq	%rax, init_lock(%rip)
	testq	%rax, %rax
	je	.L41
	call	OPENSSL_cpuid_setup@PLT
	movl	-28(%rbp), %eax
	movl	$1, base_inited(%rip)
	movl	%eax, destructor_key(%rip)
	movl	$1, %eax
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L41:
	xorl	%edi, %edi
	call	CRYPTO_THREAD_lock_free@PLT
	movq	%r12, %rdi
	movq	$0, init_lock(%rip)
	call	CRYPTO_THREAD_cleanup_local@PLT
	xorl	%eax, %eax
	jmp	.L32
.L40:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE927:
	.size	ossl_init_base_ossl_, .-ossl_init_base_ossl_
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/init.c"
	.text
	.p2align 4
	.globl	OPENSSL_cleanup
	.type	OPENSSL_cleanup, @function
OPENSSL_cleanup:
.LFB962:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	base_inited(%rip), %r9d
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	je	.L42
	movl	stopped(%rip), %r8d
	testl	%r8d, %r8d
	je	.L61
.L42:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L62
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	leaq	destructor_key(%rip), %rdi
	movl	$1, stopped(%rip)
	call	CRYPTO_THREAD_get_local@PLT
	xorl	%esi, %esi
	leaq	destructor_key(%rip), %rdi
	movq	%rax, %r12
	call	CRYPTO_THREAD_set_local@PLT
	testq	%r12, %r12
	je	.L44
	movl	(%r12), %edi
	testl	%edi, %edi
	jne	.L63
.L45:
	movl	4(%r12), %esi
	testl	%esi, %esi
	jne	.L64
.L46:
	movl	8(%r12), %ecx
	testl	%ecx, %ecx
	jne	.L65
.L47:
	movl	$449, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
.L44:
	movq	stop_handlers(%rip), %rbx
	testq	%rbx, %rbx
	je	.L48
	leaq	.LC0(%rip), %r12
	.p2align 4,,10
	.p2align 3
.L49:
	call	*(%rbx)
	movq	%rbx, %rdi
	movq	8(%rbx), %rbx
	movl	$522, %edx
	movq	%r12, %rsi
	call	CRYPTO_free@PLT
	testq	%rbx, %rbx
	jne	.L49
.L48:
	movq	init_lock(%rip), %rdi
	movq	$0, stop_handlers(%rip)
	call	CRYPTO_THREAD_lock_free@PLT
	movl	async_inited(%rip), %edx
	movq	$0, init_lock(%rip)
	testl	%edx, %edx
	jne	.L66
.L50:
	movl	load_crypto_strings_inited(%rip), %eax
	testl	%eax, %eax
	jne	.L67
.L51:
	movl	destructor_key(%rip), %eax
	leaq	-28(%rbp), %rdi
	movq	$-1, destructor_key(%rip)
	movl	%eax, -28(%rbp)
	call	CRYPTO_THREAD_cleanup_local@PLT
	call	rand_cleanup_int@PLT
	call	rand_drbg_cleanup_int@PLT
	call	conf_modules_free_int@PLT
	call	engine_cleanup_int@PLT
	call	ossl_store_cleanup_int@PLT
	call	crypto_cleanup_all_ex_data_int@PLT
	call	bio_cleanup@PLT
	call	evp_cleanup_int@PLT
	call	obj_cleanup_int@PLT
	call	err_cleanup@PLT
	call	CRYPTO_secure_malloc_done@PLT
	movl	$0, base_inited(%rip)
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L65:
	call	drbg_delete_thread_state@PLT
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L64:
	call	err_delete_thread_state@PLT
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L63:
	call	async_delete_thread_state@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L67:
	call	err_free_strings_int@PLT
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L66:
	call	async_deinit@PLT
	jmp	.L50
.L62:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE962:
	.size	OPENSSL_cleanup, .-OPENSSL_cleanup
	.p2align 4
	.type	ossl_init_thread_destructor, @function
ossl_init_thread_destructor:
.LFB925:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L68
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movl	(%rdi), %ecx
	testl	%ecx, %ecx
	jne	.L76
	movl	4(%r12), %edx
	testl	%edx, %edx
	jne	.L77
.L71:
	movl	8(%r12), %eax
	testl	%eax, %eax
	jne	.L78
.L72:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$449, %edx
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	call	async_delete_thread_state@PLT
	movl	4(%r12), %edx
	testl	%edx, %edx
	je	.L71
.L77:
	call	err_delete_thread_state@PLT
	movl	8(%r12), %eax
	testl	%eax, %eax
	je	.L72
.L78:
	call	drbg_delete_thread_state@PLT
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE925:
	.size	ossl_init_thread_destructor, .-ossl_init_thread_destructor
	.p2align 4
	.globl	OPENSSL_thread_stop
	.type	OPENSSL_thread_stop, @function
OPENSSL_thread_stop:
.LFB960:
	.cfi_startproc
	endbr64
	cmpq	$-1, destructor_key(%rip)
	jne	.L93
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	destructor_key(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	CRYPTO_THREAD_get_local@PLT
	xorl	%esi, %esi
	leaq	destructor_key(%rip), %rdi
	movq	%rax, %r12
	call	CRYPTO_THREAD_set_local@PLT
	testq	%r12, %r12
	je	.L79
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jne	.L94
	movl	4(%r12), %edx
	testl	%edx, %edx
	jne	.L95
.L84:
	movl	8(%r12), %eax
	testl	%eax, %eax
	jne	.L96
.L85:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$449, %edx
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	call	async_delete_thread_state@PLT
	movl	4(%r12), %edx
	testl	%edx, %edx
	je	.L84
.L95:
	call	err_delete_thread_state@PLT
	movl	8(%r12), %eax
	testl	%eax, %eax
	je	.L85
.L96:
	call	drbg_delete_thread_state@PLT
	jmp	.L85
	.cfi_endproc
.LFE960:
	.size	OPENSSL_thread_stop, .-OPENSSL_thread_stop
	.p2align 4
	.globl	ossl_init_thread_start
	.type	ossl_init_thread_start, @function
ossl_init_thread_start:
.LFB961:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	stopped(%rip), %r12d
	testl	%r12d, %r12d
	je	.L98
	movl	$624, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$70, %edx
	xorl	%r12d, %r12d
	movl	$116, %esi
	movl	$15, %edi
	call	ERR_put_error@PLT
.L97:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	movq	%rdi, %rbx
	leaq	ossl_init_base_ossl_(%rip), %rsi
	leaq	base(%rip), %rdi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L97
	movl	ossl_init_base_ossl_ret_(%rip), %ecx
	testl	%ecx, %ecx
	je	.L97
	leaq	ossl_init_register_atexit_ossl_(%rip), %rsi
	leaq	register_atexit(%rip), %rdi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L97
	movl	ossl_init_register_atexit_ossl_ret_(%rip), %edx
	testl	%edx, %edx
	je	.L97
	leaq	ossl_init_load_crypto_nodelete_ossl_(%rip), %rsi
	leaq	load_crypto_nodelete(%rip), %rdi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L97
	movl	ossl_init_load_crypto_nodelete_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	je	.L97
	leaq	destructor_key(%rip), %rdi
	call	CRYPTO_THREAD_get_local@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L121
.L102:
	testb	$1, %bl
	je	.L105
	movl	$1, 0(%r13)
.L105:
	testb	$2, %bl
	je	.L103
	movl	$1, 4(%r13)
.L103:
	andl	$4, %ebx
	movl	$1, %r12d
	je	.L97
	movl	$1, 8(%r13)
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	movl	$66, %edx
	leaq	.LC0(%rip), %rsi
	movl	$12, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L97
	movq	%rax, %rsi
	leaq	destructor_key(%rip), %rdi
	call	CRYPTO_THREAD_set_local@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L102
	movl	$68, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	jmp	.L97
	.cfi_endproc
.LFE961:
	.size	ossl_init_thread_start, .-ossl_init_thread_start
	.p2align 4
	.globl	OPENSSL_init_crypto
	.type	OPENSSL_init_crypto, @function
OPENSSL_init_crypto:
.LFB963:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	stopped(%rip), %eax
	testl	%eax, %eax
	je	.L123
	testl	$262144, %edi
	je	.L124
.L210:
	xorl	%r12d, %r12d
.L122:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore_state
	movq	%rsi, %r13
	leaq	base(%rip), %rdi
	leaq	ossl_init_base_ossl_(%rip), %rsi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L210
	movl	ossl_init_base_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	je	.L210
	movl	$1, %r12d
	testl	$262144, %ebx
	jne	.L122
	testl	$524288, %ebx
	je	.L127
	leaq	ossl_init_no_register_atexit_ossl_(%rip), %rsi
	leaq	register_atexit(%rip), %rdi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L210
.L209:
	movl	ossl_init_register_atexit_ossl_ret_(%rip), %r12d
	testl	%r12d, %r12d
	je	.L210
	leaq	ossl_init_load_crypto_nodelete_ossl_(%rip), %rsi
	leaq	load_crypto_nodelete(%rip), %rdi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L210
	movl	ossl_init_load_crypto_nodelete_ossl_ret_(%rip), %r11d
	testl	%r11d, %r11d
	je	.L210
	testb	$1, %bl
	jne	.L130
.L133:
	testb	$2, %bl
	jne	.L212
.L132:
	testb	$16, %bl
	jne	.L213
.L135:
	testb	$4, %bl
	jne	.L214
.L137:
	testb	$32, %bl
	jne	.L215
.L139:
	testb	$8, %bl
	jne	.L216
.L141:
	testl	$131072, %ebx
	jne	.L217
.L143:
	testb	$-128, %bl
	jne	.L218
.L145:
	testb	$64, %bl
	jne	.L219
.L147:
	testb	$1, %bh
	jne	.L220
.L149:
	testb	$8, %bh
	jne	.L221
.L152:
	testb	$2, %bh
	jne	.L222
.L154:
	testb	$4, %bh
	jne	.L223
.L156:
	andb	$-2, %bh
	jne	.L157
.L211:
	movl	$1, %r12d
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L124:
	movl	$624, %r8d
	movl	$70, %edx
	movl	$116, %esi
	movl	$15, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L127:
	leaq	ossl_init_register_atexit_ossl_(%rip), %rsi
	leaq	register_atexit(%rip), %rdi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	jne	.L209
	jmp	.L210
.L130:
	leaq	ossl_init_no_load_crypto_strings_ossl_(%rip), %rsi
	leaq	load_crypto_strings(%rip), %rdi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L210
	movl	ossl_init_load_crypto_strings_ossl_ret_(%rip), %r10d
	testl	%r10d, %r10d
	jne	.L133
	jmp	.L210
.L212:
	leaq	ossl_init_load_crypto_strings_ossl_(%rip), %rsi
	leaq	load_crypto_strings(%rip), %rdi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L210
	movl	ossl_init_load_crypto_strings_ossl_ret_(%rip), %r9d
	testl	%r9d, %r9d
	jne	.L132
	jmp	.L210
.L213:
	leaq	ossl_init_no_add_all_ciphers_ossl_(%rip), %rsi
	leaq	add_all_ciphers(%rip), %rdi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L210
	movl	ossl_init_add_all_ciphers_ossl_ret_(%rip), %r8d
	testl	%r8d, %r8d
	jne	.L135
	jmp	.L210
.L214:
	leaq	ossl_init_add_all_ciphers_ossl_(%rip), %rsi
	leaq	add_all_ciphers(%rip), %rdi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L210
	movl	ossl_init_add_all_ciphers_ossl_ret_(%rip), %edi
	testl	%edi, %edi
	jne	.L137
	jmp	.L210
.L215:
	leaq	ossl_init_no_add_all_digests_ossl_(%rip), %rsi
	leaq	add_all_digests(%rip), %rdi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L210
	movl	ossl_init_add_all_digests_ossl_ret_(%rip), %esi
	testl	%esi, %esi
	jne	.L139
	jmp	.L210
.L216:
	leaq	ossl_init_add_all_digests_ossl_(%rip), %rsi
	leaq	add_all_digests(%rip), %rdi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L210
	movl	ossl_init_add_all_digests_ossl_ret_(%rip), %ecx
	testl	%ecx, %ecx
	jne	.L141
	jmp	.L210
.L217:
	call	openssl_init_fork_handlers@PLT
	testl	%eax, %eax
	jne	.L143
	jmp	.L210
.L218:
	leaq	ossl_init_no_config_ossl_(%rip), %rsi
	leaq	config(%rip), %rdi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L210
	movl	ossl_init_config_ossl_ret_(%rip), %edx
	testl	%edx, %edx
	jne	.L145
	jmp	.L210
.L219:
	movq	init_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	leaq	ossl_init_config_ossl_(%rip), %rsi
	leaq	config(%rip), %rdi
	movq	%r13, conf_settings(%rip)
	call	CRYPTO_THREAD_run_once@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L150
	movq	init_lock(%rip), %rdi
	movq	$0, conf_settings(%rip)
	call	CRYPTO_THREAD_unlock@PLT
	jmp	.L122
.L220:
	leaq	ossl_init_async_ossl_(%rip), %rsi
	leaq	async(%rip), %rdi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L210
	movl	ossl_init_async_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	jne	.L149
	jmp	.L210
.L221:
	leaq	ossl_init_engine_openssl_ossl_(%rip), %rsi
	leaq	engine_openssl(%rip), %rdi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L210
	cmpl	$0, ossl_init_engine_openssl_ossl_ret_(%rip)
	jne	.L152
	jmp	.L210
.L222:
	leaq	ossl_init_engine_rdrand_ossl_(%rip), %rsi
	leaq	engine_rdrand(%rip), %rdi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L210
	cmpl	$0, ossl_init_engine_rdrand_ossl_ret_(%rip)
	jne	.L154
	jmp	.L210
.L223:
	leaq	ossl_init_engine_dynamic_ossl_(%rip), %rsi
	leaq	engine_dynamic(%rip), %rdi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L210
	cmpl	$0, ossl_init_engine_dynamic_ossl_ret_(%rip)
	jne	.L156
	jmp	.L210
.L150:
	movl	ossl_init_config_ossl_ret_(%rip), %r12d
	movq	init_lock(%rip), %rdi
	movq	$0, conf_settings(%rip)
	call	CRYPTO_THREAD_unlock@PLT
	testl	%r12d, %r12d
	jg	.L147
	jmp	.L210
.L157:
	call	ENGINE_register_all_complete@PLT
	jmp	.L211
	.cfi_endproc
.LFE963:
	.size	OPENSSL_init_crypto, .-OPENSSL_init_crypto
	.p2align 4
	.globl	OPENSSL_atexit
	.type	OPENSSL_atexit, @function
OPENSSL_atexit:
.LFB964:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$816, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$16, %edi
	subq	$8, %rsp
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L228
	movq	stop_handlers(%rip), %rdx
	movq	%rbx, (%rax)
	movq	%rax, stop_handlers(%rip)
	movq	%rdx, 8(%rax)
	movl	$1, %eax
.L224:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L228:
	.cfi_restore_state
	movl	$817, %r8d
	movl	$65, %edx
	movl	$114, %esi
	movl	$15, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L224
	.cfi_endproc
.LFE964:
	.size	OPENSSL_atexit, .-OPENSSL_atexit
	.p2align 4
	.globl	OPENSSL_fork_prepare
	.type	OPENSSL_fork_prepare, @function
OPENSSL_fork_prepare:
.LFB965:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE965:
	.size	OPENSSL_fork_prepare, .-OPENSSL_fork_prepare
	.p2align 4
	.globl	OPENSSL_fork_parent
	.type	OPENSSL_fork_parent, @function
OPENSSL_fork_parent:
.LFB972:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE972:
	.size	OPENSSL_fork_parent, .-OPENSSL_fork_parent
	.p2align 4
	.globl	OPENSSL_fork_child
	.type	OPENSSL_fork_child, @function
OPENSSL_fork_child:
.LFB974:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE974:
	.size	OPENSSL_fork_child, .-OPENSSL_fork_child
	.local	ossl_init_engine_dynamic_ossl_ret_
	.comm	ossl_init_engine_dynamic_ossl_ret_,4,4
	.local	engine_dynamic
	.comm	engine_dynamic,4,4
	.local	ossl_init_engine_rdrand_ossl_ret_
	.comm	ossl_init_engine_rdrand_ossl_ret_,4,4
	.local	engine_rdrand
	.comm	engine_rdrand,4,4
	.local	ossl_init_engine_openssl_ossl_ret_
	.comm	ossl_init_engine_openssl_ossl_ret_,4,4
	.local	engine_openssl
	.comm	engine_openssl,4,4
	.local	ossl_init_async_ossl_ret_
	.comm	ossl_init_async_ossl_ret_,4,4
	.local	async_inited
	.comm	async_inited,4,4
	.local	async
	.comm	async,4,4
	.local	ossl_init_config_ossl_ret_
	.comm	ossl_init_config_ossl_ret_,4,4
	.local	conf_settings
	.comm	conf_settings,8,8
	.local	config
	.comm	config,4,4
	.local	ossl_init_add_all_digests_ossl_ret_
	.comm	ossl_init_add_all_digests_ossl_ret_,4,4
	.local	add_all_digests
	.comm	add_all_digests,4,4
	.local	ossl_init_add_all_ciphers_ossl_ret_
	.comm	ossl_init_add_all_ciphers_ossl_ret_,4,4
	.local	add_all_ciphers
	.comm	add_all_ciphers,4,4
	.local	ossl_init_load_crypto_strings_ossl_ret_
	.comm	ossl_init_load_crypto_strings_ossl_ret_,4,4
	.local	load_crypto_strings_inited
	.comm	load_crypto_strings_inited,4,4
	.local	load_crypto_strings
	.comm	load_crypto_strings,4,4
	.local	ossl_init_load_crypto_nodelete_ossl_ret_
	.comm	ossl_init_load_crypto_nodelete_ossl_ret_,4,4
	.local	load_crypto_nodelete
	.comm	load_crypto_nodelete,4,4
	.local	ossl_init_register_atexit_ossl_ret_
	.comm	ossl_init_register_atexit_ossl_ret_,4,4
	.local	register_atexit
	.comm	register_atexit,4,4
	.local	ossl_init_base_ossl_ret_
	.comm	ossl_init_base_ossl_ret_,4,4
	.local	base_inited
	.comm	base_inited,4,4
	.local	base
	.comm	base,4,4
	.local	init_lock
	.comm	init_lock,8,8
	.local	stop_handlers
	.comm	stop_handlers,8,8
	.data
	.align 8
	.type	destructor_key, @object
	.size	destructor_key, 8
destructor_key:
	.quad	-1
	.local	stopped
	.comm	stopped,4,4
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
