	.file	"p12_sbag.c"
	.text
	.p2align 4
	.globl	PKCS12_get_attr
	.type	PKCS12_get_attr, @function
PKCS12_get_attr:
.LFB803:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	PKCS12_get_attr_gen@PLT
	.cfi_endproc
.LFE803:
	.size	PKCS12_get_attr, .-PKCS12_get_attr
	.p2align 4
	.globl	PKCS12_SAFEBAG_get0_attr
	.type	PKCS12_SAFEBAG_get0_attr, @function
PKCS12_SAFEBAG_get0_attr:
.LFB821:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	PKCS12_get_attr_gen@PLT
	.cfi_endproc
.LFE821:
	.size	PKCS12_SAFEBAG_get0_attr, .-PKCS12_SAFEBAG_get0_attr
	.p2align 4
	.globl	PKCS8_get_attr
	.type	PKCS8_get_attr, @function
PKCS8_get_attr:
.LFB805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	subq	$8, %rsp
	call	PKCS8_pkey_get0_attrs@PLT
	addq	$8, %rsp
	movl	%r12d, %esi
	movq	%rax, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	PKCS12_get_attr_gen@PLT
	.cfi_endproc
.LFE805:
	.size	PKCS8_get_attr, .-PKCS8_get_attr
	.p2align 4
	.globl	PKCS12_SAFEBAG_get0_p8inf
	.type	PKCS12_SAFEBAG_get0_p8inf, @function
PKCS12_SAFEBAG_get0_p8inf:
.LFB806:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	cmpl	$150, %r8d
	jne	.L6
	movq	8(%rbx), %rax
.L6:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE806:
	.size	PKCS12_SAFEBAG_get0_p8inf, .-PKCS12_SAFEBAG_get0_p8inf
	.p2align 4
	.globl	PKCS12_SAFEBAG_get0_pkcs8
	.type	PKCS12_SAFEBAG_get0_pkcs8, @function
PKCS12_SAFEBAG_get0_pkcs8:
.LFB807:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	cmpl	$151, %r8d
	jne	.L11
	movq	8(%rbx), %rax
.L11:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE807:
	.size	PKCS12_SAFEBAG_get0_pkcs8, .-PKCS12_SAFEBAG_get0_pkcs8
	.p2align 4
	.globl	PKCS12_SAFEBAG_get0_safes
	.type	PKCS12_SAFEBAG_get0_safes, @function
PKCS12_SAFEBAG_get0_safes:
.LFB808:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	cmpl	$155, %r8d
	jne	.L16
	movq	8(%rbx), %rax
.L16:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE808:
	.size	PKCS12_SAFEBAG_get0_safes, .-PKCS12_SAFEBAG_get0_safes
	.p2align 4
	.globl	PKCS12_SAFEBAG_get0_type
	.type	PKCS12_SAFEBAG_get0_type, @function
PKCS12_SAFEBAG_get0_type:
.LFB809:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE809:
	.size	PKCS12_SAFEBAG_get0_type, .-PKCS12_SAFEBAG_get0_type
	.p2align 4
	.globl	PKCS12_SAFEBAG_get_nid
	.type	PKCS12_SAFEBAG_get_nid, @function
PKCS12_SAFEBAG_get_nid:
.LFB810:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	jmp	OBJ_obj2nid@PLT
	.cfi_endproc
.LFE810:
	.size	PKCS12_SAFEBAG_get_nid, .-PKCS12_SAFEBAG_get_nid
	.p2align 4
	.globl	PKCS12_SAFEBAG_get_bag_nid
	.type	PKCS12_SAFEBAG_get_bag_nid, @function
PKCS12_SAFEBAG_get_bag_nid:
.LFB811:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	subl	$152, %eax
	cmpl	$2, %eax
	ja	.L24
	movq	8(%rbx), %rax
	movq	(%rax), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	OBJ_obj2nid@PLT
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE811:
	.size	PKCS12_SAFEBAG_get_bag_nid, .-PKCS12_SAFEBAG_get_bag_nid
	.p2align 4
	.globl	PKCS12_SAFEBAG_get1_cert
	.type	PKCS12_SAFEBAG_get1_cert, @function
PKCS12_SAFEBAG_get1_cert:
.LFB812:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$152, %eax
	jne	.L28
	movq	8(%rbx), %rax
	movq	(%rax), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$158, %eax
	jne	.L28
	movq	8(%rbx), %rax
	leaq	X509_it(%rip), %rsi
	movq	8(%rax), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ASN1_item_unpack@PLT
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE812:
	.size	PKCS12_SAFEBAG_get1_cert, .-PKCS12_SAFEBAG_get1_cert
	.p2align 4
	.globl	PKCS12_SAFEBAG_get1_crl
	.type	PKCS12_SAFEBAG_get1_crl, @function
PKCS12_SAFEBAG_get1_crl:
.LFB813:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$153, %eax
	jne	.L33
	movq	8(%rbx), %rax
	movq	(%rax), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$160, %eax
	jne	.L33
	movq	8(%rbx), %rax
	leaq	X509_CRL_it(%rip), %rsi
	movq	8(%rax), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ASN1_item_unpack@PLT
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE813:
	.size	PKCS12_SAFEBAG_get1_crl, .-PKCS12_SAFEBAG_get1_crl
	.p2align 4
	.globl	PKCS12_SAFEBAG_create_cert
	.type	PKCS12_SAFEBAG_create_cert, @function
PKCS12_SAFEBAG_create_cert:
.LFB814:
	.cfi_startproc
	endbr64
	movl	$152, %ecx
	movl	$158, %edx
	leaq	X509_it(%rip), %rsi
	jmp	PKCS12_item_pack_safebag@PLT
	.cfi_endproc
.LFE814:
	.size	PKCS12_SAFEBAG_create_cert, .-PKCS12_SAFEBAG_create_cert
	.p2align 4
	.globl	PKCS12_SAFEBAG_create_crl
	.type	PKCS12_SAFEBAG_create_crl, @function
PKCS12_SAFEBAG_create_crl:
.LFB815:
	.cfi_startproc
	endbr64
	movl	$153, %ecx
	movl	$160, %edx
	leaq	X509_CRL_it(%rip), %rsi
	jmp	PKCS12_item_pack_safebag@PLT
	.cfi_endproc
.LFE815:
	.size	PKCS12_SAFEBAG_create_crl, .-PKCS12_SAFEBAG_create_crl
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/pkcs12/p12_sbag.c"
	.text
	.p2align 4
	.globl	PKCS12_SAFEBAG_create0_p8inf
	.type	PKCS12_SAFEBAG_create0_p8inf, @function
PKCS12_SAFEBAG_create0_p8inf:
.LFB816:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movq	%rdi, -24(%rbp)
	call	PKCS12_SAFEBAG_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L42
	movl	$150, %edi
	call	OBJ_nid2obj@PLT
	movq	%rax, %xmm0
	movhps	-24(%rbp), %xmm0
	movups	%xmm0, (%r12)
.L38:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	movl	$113, %r8d
	movl	$65, %edx
	movl	$112, %esi
	movl	$35, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L38
	.cfi_endproc
.LFE816:
	.size	PKCS12_SAFEBAG_create0_p8inf, .-PKCS12_SAFEBAG_create0_p8inf
	.p2align 4
	.globl	PKCS12_SAFEBAG_create0_pkcs8
	.type	PKCS12_SAFEBAG_create0_pkcs8, @function
PKCS12_SAFEBAG_create0_pkcs8:
.LFB817:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movq	%rdi, -24(%rbp)
	call	PKCS12_SAFEBAG_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L47
	movl	$151, %edi
	call	OBJ_nid2obj@PLT
	movq	%rax, %xmm0
	movhps	-24(%rbp), %xmm0
	movups	%xmm0, (%r12)
.L43:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	movl	$129, %r8d
	movl	$65, %edx
	movl	$113, %esi
	movl	$35, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L43
	.cfi_endproc
.LFE817:
	.size	PKCS12_SAFEBAG_create0_pkcs8, .-PKCS12_SAFEBAG_create0_pkcs8
	.p2align 4
	.globl	PKCS12_SAFEBAG_create_pkcs8_encrypt
	.type	PKCS12_SAFEBAG_create_pkcs8_encrypt, @function
PKCS12_SAFEBAG_create_pkcs8_encrypt:
.LFB818:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	movl	%r9d, -52(%rbp)
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_cipherbyname@PLT
	movl	-52(%rbp), %r9d
	pushq	16(%rbp)
	movl	%r14d, %ecx
	testq	%rax, %rax
	movq	%rax, %rsi
	movl	$-1, %eax
	movq	%r13, %rdx
	pushq	%r9
	cmovne	%eax, %r12d
	movl	%r15d, %r9d
	movq	%rbx, %r8
	movl	%r12d, %edi
	call	PKCS8_encrypt@PLT
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L52
	movq	%rax, %r13
	call	PKCS12_SAFEBAG_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L56
	movl	$151, %edi
	call	OBJ_nid2obj@PLT
	movq	%r13, %xmm1
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r12)
.L48:
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L56:
	movl	$129, %r8d
	movl	$65, %edx
	movl	$113, %esi
	movl	$35, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r13, %rdi
	call	X509_SIG_free@PLT
	jmp	.L48
	.cfi_endproc
.LFE818:
	.size	PKCS12_SAFEBAG_create_pkcs8_encrypt, .-PKCS12_SAFEBAG_create_pkcs8_encrypt
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
