	.file	"drbg_lib.c"
	.text
	.p2align 4
	.type	drbg_status, @function
drbg_status:
.LFB449:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	do_rand_drbg_init_ossl_(%rip), %rsi
	leaq	rand_drbg_init(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	call	CRYPTO_THREAD_run_once@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L1
	movl	do_rand_drbg_init_ossl_ret_(%rip), %r12d
	testl	%r12d, %r12d
	je	.L1
	movq	master_drbg(%rip), %rbx
	testq	%rbx, %rbx
	je	.L4
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L16
	xorl	%r12d, %r12d
	cmpl	$1, 152(%rbx)
	sete	%r12b
.L1:
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	call	CRYPTO_THREAD_write_lock@PLT
	movq	(%rbx), %rdi
	xorl	%r12d, %r12d
	cmpl	$1, 152(%rbx)
	sete	%r12b
	testq	%rdi, %rdi
	je	.L1
	call	CRYPTO_THREAD_unlock@PLT
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L1
	.cfi_endproc
.LFE449:
	.size	drbg_status, .-drbg_status
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/rand/drbg_lib.c"
	.text
	.p2align 4
	.type	rand_drbg_new, @function
rand_drbg_new:
.LFB421:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$8, %rsp
	testl	%edi, %edi
	je	.L18
	movl	$191, %edx
	leaq	.LC0(%rip), %rsi
	movl	$360, %edi
	call	CRYPTO_secure_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L21
	movq	%rax, %rdi
	xorl	%r15d, %r15d
	call	CRYPTO_secure_allocated@PLT
	testl	%eax, %eax
	setne	%r15b
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L18:
	movl	%edi, %r15d
	movl	$192, %edx
	leaq	.LC0(%rip), %rsi
	movl	$360, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L21
.L22:
	movl	%r15d, 16(%r12)
	call	openssl_get_fork_id@PLT
	movq	%r13, 8(%r12)
	movq	rand_drbg_get_entropy@GOTPCREL(%rip), %xmm0
	movl	%eax, 24(%r12)
	movhps	rand_drbg_cleanup_entropy@GOTPCREL(%rip), %xmm0
	movups	%xmm0, 328(%r12)
	testq	%r13, %r13
	je	.L58
	movl	slave_reseed_interval(%rip), %eax
	movl	%eax, 116(%r12)
	movq	slave_reseed_time_interval(%rip), %rax
	movq	%rax, 128(%r12)
.L24:
	movl	%r14d, %eax
	orl	%ebx, %eax
	jne	.L25
	movl	rand_drbg_type(%rip), %ebx
	movl	rand_drbg_flags(%rip), %r14d
.L25:
	movl	20(%r12), %eax
	testl	%eax, %eax
	jne	.L59
.L26:
	movl	$0, 152(%r12)
	movw	%r14w, 28(%r12)
	movl	%ebx, 20(%r12)
	testl	%ebx, %ebx
	je	.L29
.L41:
	subl	$904, %ebx
	cmpl	$2, %ebx
	ja	.L30
	movq	%r12, %rdi
	call	drbg_ctr_init@PLT
	testl	%eax, %eax
	je	.L60
.L32:
	testq	%r13, %r13
	je	.L17
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	jne	.L61
	movl	48(%r12), %eax
	cmpl	%eax, 48(%r13)
	jl	.L37
.L17:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	cmpl	%ebx, %eax
	je	.L62
.L27:
	movq	320(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	movq	40(%r12), %rdi
	call	rand_pool_free@PLT
	movq	$0, 40(%r12)
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L61:
	call	CRYPTO_THREAD_write_lock@PLT
	movl	48(%r13), %eax
	cmpl	%eax, 48(%r12)
	movq	0(%r13), %rdi
	jg	.L63
	testq	%rdi, %rdi
	je	.L17
	call	CRYPTO_THREAD_unlock@PLT
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L30:
	xorl	%edx, %edx
	movl	$104, %esi
	movl	$36, %edi
	movl	$0, 20(%r12)
	movw	%dx, 28(%r12)
	movl	$128, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$120, %edx
	movq	$0, 320(%r12)
	call	ERR_put_error@PLT
.L33:
	movq	320(%r12), %rax
	testq	%rax, %rax
	je	.L38
	movq	%r12, %rdi
	call	*24(%rax)
.L38:
	movq	40(%r12), %rdi
	call	rand_pool_free@PLT
	movq	(%r12), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	leaq	160(%r12), %rdx
	movq	%r12, %rsi
	movl	$15, %edi
	call	CRYPTO_free_ex_data@PLT
	movl	16(%r12), %eax
	testl	%eax, %eax
	je	.L39
	movq	%r12, %rdi
	movl	$275, %ecx
	movl	$360, %esi
	xorl	%r12d, %r12d
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_secure_clear_free@PLT
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L29:
	movq	$0, 320(%r12)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L58:
	movq	rand_drbg_get_nonce@GOTPCREL(%rip), %xmm0
	movl	master_reseed_interval(%rip), %eax
	movl	%eax, 116(%r12)
	movq	master_reseed_time_interval(%rip), %rax
	movhps	rand_drbg_cleanup_nonce@GOTPCREL(%rip), %xmm0
	movups	%xmm0, 344(%r12)
	movq	%rax, 128(%r12)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L21:
	movl	$195, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$109, %esi
	movl	$36, %edi
	call	ERR_put_error@PLT
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L39:
	movq	%r12, %rdi
	movl	$277, %ecx
	movl	$360, %esi
	xorl	%r12d, %r12d
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L63:
	testq	%rdi, %rdi
	je	.L37
	call	CRYPTO_THREAD_unlock@PLT
.L37:
	movl	$236, %r8d
	movl	$131, %edx
	movl	$109, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L62:
	movzwl	28(%r12), %eax
	cmpl	%r14d, %eax
	jne	.L27
	movl	$0, 152(%r12)
	movw	%r14w, 28(%r12)
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L60:
	movl	$143, %r8d
	movl	$107, %edx
	movl	$104, %esi
	movl	$2, 152(%r12)
	leaq	.LC0(%rip), %rcx
	movl	$36, %edi
	call	ERR_put_error@PLT
	jmp	.L33
	.cfi_endproc
.LFE421:
	.size	rand_drbg_new, .-rand_drbg_new
	.p2align 4
	.globl	RAND_DRBG_set
	.type	RAND_DRBG_set, @function
RAND_DRBG_set:
.LFB419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	orl	%esi, %eax
	jne	.L77
	movl	rand_drbg_type(%rip), %r12d
	movl	rand_drbg_flags(%rip), %r13d
.L65:
	movl	20(%rbx), %eax
	testl	%eax, %eax
	je	.L66
	cmpl	%r12d, %eax
	je	.L79
.L67:
	movq	320(%rbx), %rax
	movq	%rbx, %rdi
	call	*24(%rax)
	movq	40(%rbx), %rdi
	call	rand_pool_free@PLT
	movq	$0, 40(%rbx)
.L66:
	movl	$0, 152(%rbx)
	movw	%r13w, 28(%rbx)
	movl	%r12d, 20(%rbx)
	testl	%r12d, %r12d
	je	.L69
.L73:
	subl	$904, %r12d
	cmpl	$2, %r12d
	ja	.L70
	movq	%rbx, %rdi
	call	drbg_ctr_init@PLT
	testl	%eax, %eax
	je	.L80
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	movl	%esi, %r12d
	movl	%edx, %r13d
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L69:
	movq	$0, 320(%rbx)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	xorl	%eax, %eax
	movl	$120, %edx
	movl	$104, %esi
	movl	$0, 20(%rbx)
	movw	%ax, 28(%rbx)
	movl	$128, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$36, %edi
	movq	$0, 320(%rbx)
	call	ERR_put_error@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	movzwl	28(%rbx), %eax
	cmpl	%r13d, %eax
	jne	.L67
	movl	$0, 152(%rbx)
	movw	%r13w, 28(%rbx)
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L80:
	movl	$143, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$107, %edx
	movl	%eax, -36(%rbp)
	movl	$2, 152(%rbx)
	movl	$104, %esi
	movl	$36, %edi
	call	ERR_put_error@PLT
	movl	-36(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE419:
	.size	RAND_DRBG_set, .-RAND_DRBG_set
	.p2align 4
	.globl	RAND_DRBG_set_defaults
	.type	RAND_DRBG_set_defaults, @function
RAND_DRBG_set_defaults:
.LFB420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	-904(%rdi), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$2, %eax
	ja	.L86
	testl	$-2, %esi
	jne	.L87
	movl	$1, %eax
	movl	%edi, rand_drbg_type(%rip)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	%esi, rand_drbg_flags(%rip)
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	movl	$159, %r8d
	movl	$120, %edx
	movl	$121, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	movl	$168, %r8d
	movl	$132, %edx
	movl	$121, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE420:
	.size	RAND_DRBG_set_defaults, .-RAND_DRBG_set_defaults
	.p2align 4
	.globl	RAND_DRBG_new
	.type	RAND_DRBG_new, @function
RAND_DRBG_new:
.LFB422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	leaq	.LC0(%rip), %rsi
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	movl	$192, %edx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	%edi, %ebx
	movl	$360, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L123
	movl	$0, 16(%rax)
	call	openssl_get_fork_id@PLT
	movq	%r13, 8(%r12)
	movq	rand_drbg_get_entropy@GOTPCREL(%rip), %xmm0
	movl	%eax, 24(%r12)
	movhps	rand_drbg_cleanup_entropy@GOTPCREL(%rip), %xmm0
	movups	%xmm0, 328(%r12)
	testq	%r13, %r13
	je	.L124
	movl	slave_reseed_interval(%rip), %eax
	movl	%eax, 116(%r12)
	movq	slave_reseed_time_interval(%rip), %rax
	movq	%rax, 128(%r12)
.L92:
	movl	%r14d, %eax
	orl	%ebx, %eax
	jne	.L93
	movl	rand_drbg_type(%rip), %ebx
	movl	rand_drbg_flags(%rip), %r14d
.L93:
	movl	20(%r12), %eax
	testl	%eax, %eax
	jne	.L125
.L94:
	movl	$0, 152(%r12)
	movw	%r14w, 28(%r12)
	movl	%ebx, 20(%r12)
	testl	%ebx, %ebx
	je	.L97
.L109:
	subl	$904, %ebx
	cmpl	$2, %ebx
	ja	.L98
	movq	%r12, %rdi
	call	drbg_ctr_init@PLT
	testl	%eax, %eax
	je	.L126
.L100:
	testq	%r13, %r13
	je	.L88
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	jne	.L127
	movl	48(%r12), %eax
	cmpl	%eax, 48(%r13)
	jl	.L105
.L88:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_restore_state
	cmpl	%ebx, %eax
	je	.L128
.L95:
	movq	320(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	movq	40(%r12), %rdi
	call	rand_pool_free@PLT
	movq	$0, 40(%r12)
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L127:
	call	CRYPTO_THREAD_write_lock@PLT
	movl	48(%r13), %eax
	cmpl	%eax, 48(%r12)
	movq	0(%r13), %rdi
	jg	.L129
	testq	%rdi, %rdi
	je	.L88
	call	CRYPTO_THREAD_unlock@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	xorl	%edx, %edx
	movl	$104, %esi
	movl	$36, %edi
	movl	$0, 20(%r12)
	movw	%dx, 28(%r12)
	movl	$128, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$120, %edx
	movq	$0, 320(%r12)
	call	ERR_put_error@PLT
.L101:
	movq	320(%r12), %rax
	testq	%rax, %rax
	je	.L106
	movq	%r12, %rdi
	call	*24(%rax)
.L106:
	movq	40(%r12), %rdi
	call	rand_pool_free@PLT
	movq	(%r12), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	leaq	160(%r12), %rdx
	movq	%r12, %rsi
	movl	$15, %edi
	call	CRYPTO_free_ex_data@PLT
	movl	16(%r12), %eax
	testl	%eax, %eax
	je	.L107
	movq	%r12, %rdi
	movl	$275, %ecx
	xorl	%r12d, %r12d
	movl	$360, %esi
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_secure_clear_free@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	.cfi_restore_state
	movq	$0, 320(%r12)
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L124:
	movq	rand_drbg_get_nonce@GOTPCREL(%rip), %xmm0
	movl	master_reseed_interval(%rip), %eax
	movl	%eax, 116(%r12)
	movq	master_reseed_time_interval(%rip), %rax
	movhps	rand_drbg_cleanup_nonce@GOTPCREL(%rip), %xmm0
	movups	%xmm0, 344(%r12)
	movq	%rax, 128(%r12)
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L107:
	movq	%r12, %rdi
	movl	$277, %ecx
	xorl	%r12d, %r12d
	movl	$360, %esi
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	testq	%rdi, %rdi
	je	.L105
	call	CRYPTO_THREAD_unlock@PLT
.L105:
	movl	$236, %r8d
	movl	$131, %edx
	movl	$109, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L128:
	movzwl	28(%r12), %eax
	cmpl	%r14d, %eax
	jne	.L95
	movl	$0, 152(%r12)
	movw	%r14w, 28(%r12)
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L126:
	movl	$143, %r8d
	movl	$107, %edx
	movl	$104, %esi
	movl	$2, 152(%r12)
	leaq	.LC0(%rip), %rcx
	movl	$36, %edi
	call	ERR_put_error@PLT
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L123:
	movl	$195, %r8d
	movl	$65, %edx
	movl	$109, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L88
	.cfi_endproc
.LFE422:
	.size	RAND_DRBG_new, .-RAND_DRBG_new
	.p2align 4
	.globl	RAND_DRBG_secure_new
	.type	RAND_DRBG_secure_new, @function
RAND_DRBG_secure_new:
.LFB423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	leaq	.LC0(%rip), %rsi
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	movl	$191, %edx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	%edi, %ebx
	movl	$360, %edi
	call	CRYPTO_secure_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L165
	movq	%rax, %rdi
	call	CRYPTO_secure_allocated@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	movl	%eax, 16(%r12)
	call	openssl_get_fork_id@PLT
	movq	%r13, 8(%r12)
	movq	rand_drbg_get_entropy@GOTPCREL(%rip), %xmm0
	movl	%eax, 24(%r12)
	movhps	rand_drbg_cleanup_entropy@GOTPCREL(%rip), %xmm0
	movups	%xmm0, 328(%r12)
	testq	%r13, %r13
	je	.L166
	movl	slave_reseed_interval(%rip), %eax
	movl	%eax, 116(%r12)
	movq	slave_reseed_time_interval(%rip), %rax
	movq	%rax, 128(%r12)
.L134:
	movl	%r14d, %eax
	orl	%ebx, %eax
	jne	.L135
	movl	rand_drbg_type(%rip), %ebx
	movl	rand_drbg_flags(%rip), %r14d
.L135:
	movl	20(%r12), %eax
	testl	%eax, %eax
	jne	.L167
.L136:
	movl	$0, 152(%r12)
	movw	%r14w, 28(%r12)
	movl	%ebx, 20(%r12)
	testl	%ebx, %ebx
	je	.L139
.L151:
	subl	$904, %ebx
	cmpl	$2, %ebx
	ja	.L140
	movq	%r12, %rdi
	call	drbg_ctr_init@PLT
	testl	%eax, %eax
	je	.L168
.L142:
	testq	%r13, %r13
	je	.L130
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	jne	.L169
	movl	48(%r12), %eax
	cmpl	%eax, 48(%r13)
	jl	.L147
.L130:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L167:
	.cfi_restore_state
	cmpl	%ebx, %eax
	je	.L170
.L137:
	movq	320(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	movq	40(%r12), %rdi
	call	rand_pool_free@PLT
	movq	$0, 40(%r12)
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L169:
	call	CRYPTO_THREAD_write_lock@PLT
	movl	48(%r13), %eax
	cmpl	%eax, 48(%r12)
	movq	0(%r13), %rdi
	jg	.L171
	testq	%rdi, %rdi
	je	.L130
	call	CRYPTO_THREAD_unlock@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	xorl	%edx, %edx
	movl	$104, %esi
	movl	$36, %edi
	movl	$0, 20(%r12)
	movw	%dx, 28(%r12)
	movl	$128, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$120, %edx
	movq	$0, 320(%r12)
	call	ERR_put_error@PLT
.L143:
	movq	320(%r12), %rax
	testq	%rax, %rax
	je	.L148
	movq	%r12, %rdi
	call	*24(%rax)
.L148:
	movq	40(%r12), %rdi
	call	rand_pool_free@PLT
	movq	(%r12), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	leaq	160(%r12), %rdx
	movq	%r12, %rsi
	movl	$15, %edi
	call	CRYPTO_free_ex_data@PLT
	movl	16(%r12), %eax
	testl	%eax, %eax
	je	.L149
	movq	%r12, %rdi
	movl	$275, %ecx
	xorl	%r12d, %r12d
	movl	$360, %esi
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_secure_clear_free@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	movq	$0, 320(%r12)
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L166:
	movq	rand_drbg_get_nonce@GOTPCREL(%rip), %xmm0
	movl	master_reseed_interval(%rip), %eax
	movl	%eax, 116(%r12)
	movq	master_reseed_time_interval(%rip), %rax
	movhps	rand_drbg_cleanup_nonce@GOTPCREL(%rip), %xmm0
	movups	%xmm0, 344(%r12)
	movq	%rax, 128(%r12)
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L149:
	movq	%r12, %rdi
	movl	$277, %ecx
	xorl	%r12d, %r12d
	movl	$360, %esi
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	testq	%rdi, %rdi
	je	.L147
	call	CRYPTO_THREAD_unlock@PLT
.L147:
	movl	$236, %r8d
	movl	$131, %edx
	movl	$109, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L170:
	movzwl	28(%r12), %eax
	cmpl	%r14d, %eax
	jne	.L137
	movl	$0, 152(%r12)
	movw	%r14w, 28(%r12)
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L168:
	movl	$143, %r8d
	movl	$107, %edx
	movl	$104, %esi
	movl	$2, 152(%r12)
	leaq	.LC0(%rip), %rcx
	movl	$36, %edi
	call	ERR_put_error@PLT
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L165:
	movl	$195, %r8d
	movl	$65, %edx
	movl	$109, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L130
	.cfi_endproc
.LFE423:
	.size	RAND_DRBG_secure_new, .-RAND_DRBG_secure_new
	.p2align 4
	.globl	RAND_DRBG_free
	.type	RAND_DRBG_free, @function
RAND_DRBG_free:
.LFB424:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L172
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	320(%rdi), %rax
	testq	%rax, %rax
	je	.L174
	call	*24(%rax)
.L174:
	movq	40(%r12), %rdi
	call	rand_pool_free@PLT
	movq	(%r12), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	leaq	160(%r12), %rdx
	movq	%r12, %rsi
	movl	$15, %edi
	call	CRYPTO_free_ex_data@PLT
	movl	16(%r12), %eax
	testl	%eax, %eax
	je	.L175
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$275, %ecx
	movl	$360, %esi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	leaq	.LC0(%rip), %rdx
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_secure_clear_free@PLT
	.p2align 4,,10
	.p2align 3
.L175:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$277, %ecx
	movl	$360, %esi
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rdx
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_clear_free@PLT
	.p2align 4,,10
	.p2align 3
.L172:
	ret
	.cfi_endproc
.LFE424:
	.size	RAND_DRBG_free, .-RAND_DRBG_free
	.p2align 4
	.globl	RAND_DRBG_instantiate
	.type	RAND_DRBG_instantiate, @function
RAND_DRBG_instantiate:
.LFB425:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	cmpq	%rdx, 96(%rdi)
	jb	.L226
	cmpq	$0, 320(%rdi)
	je	.L227
	movl	152(%rdi), %eax
	testl	%eax, %eax
	jne	.L228
	movq	%rdx, %r12
	movq	80(%rdi), %rcx
	movslq	48(%rdi), %rdx
	movq	%rsi, %r13
	movl	$2, 152(%rdi)
	movq	64(%rdi), %r15
	movq	72(%rdi), %r8
	movq	%rdx, %rax
	testq	%rcx, %rcx
	je	.L188
	cmpq	$0, 344(%rdi)
	je	.L229
.L188:
	movl	136(%rbx), %eax
	testl	%eax, %eax
	jne	.L189
	movq	328(%rbx), %rax
	movl	$0, 140(%rbx)
	testq	%rax, %rax
	je	.L203
.L232:
	movq	%r8, -88(%rbp)
	xorl	%r9d, %r9d
	leaq	-64(%rbp), %rsi
	movq	%r15, %rcx
	movq	%rbx, %rdi
	call	*%rax
	movq	-88(%rbp), %r8
	movq	%rax, %r14
	cmpq	%rax, %r8
	setb	%al
.L193:
	cmpq	%r15, %r14
	jb	.L205
	testb	%al, %al
	jne	.L205
	movq	80(%rbx), %r15
	testq	%r15, %r15
	je	.L196
	movq	344(%rbx), %rax
	testq	%rax, %rax
	je	.L204
	movl	48(%rbx), %ecx
	movq	88(%rbx), %r8
	leaq	-72(%rbp), %rsi
	movq	%rbx, %rdi
	movl	%ecx, %edx
	shrl	$31, %edx
	addl	%ecx, %edx
	movq	%r15, %rcx
	sarl	%edx
	call	*%rax
	movq	%rax, %r15
	cmpq	%rax, 80(%rbx)
	ja	.L197
	cmpq	%rax, 88(%rbx)
	jb	.L197
.L196:
	movq	320(%rbx), %rax
	movq	%r14, %rdx
	movq	%r13, %r9
	movq	%r15, %r8
	subq	$8, %rsp
	movq	-72(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	%rbx, %rdi
	pushq	%r12
	call	*(%rax)
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	je	.L230
	movl	$1, 152(%rbx)
	xorl	%edi, %edi
	movl	$1, 112(%rbx)
	call	time@PLT
	movq	%rax, 120(%rbx)
	movl	140(%rbx), %eax
	movl	%eax, 136(%rbx)
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L228:
	cmpl	$2, %eax
	movl	$114, %edx
	movl	$103, %eax
	movl	$310, %r8d
	cmovne	%eax, %edx
	leaq	.LC0(%rip), %rcx
.L225:
	movl	$108, %esi
	movl	$36, %edi
	xorl	%r14d, %r14d
	call	ERR_put_error@PLT
	xorl	%r15d, %r15d
.L184:
	movq	-64(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L199
	movq	336(%rbx), %rax
	testq	%rax, %rax
	je	.L199
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	*%rax
.L199:
	movq	-72(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L200
	movq	352(%rbx), %rax
	testq	%rax, %rax
	je	.L200
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	*%rax
.L200:
	xorl	%eax, %eax
	cmpl	$1, 152(%rbx)
	sete	%al
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L231
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore_state
	addl	$1, %eax
	movl	$1, %ecx
	cmove	%ecx, %eax
	movl	%eax, 140(%rbx)
	movq	328(%rbx), %rax
	testq	%rax, %rax
	jne	.L232
.L203:
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L226:
	movl	$298, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$116, %edx
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L229:
	movl	%edx, %esi
	addq	%rcx, %r15
	addq	88(%rdi), %r8
	shrl	$31, %esi
	addl	%esi, %eax
	sarl	%eax
	cltq
	addq	%rax, %rdx
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L197:
	movl	$350, %r8d
	movl	$111, %edx
	movl	$108, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L230:
	movl	$357, %r8d
	movl	$108, %edx
	movl	$108, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L205:
	movl	$342, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$110, %edx
	xorl	%r15d, %r15d
	movl	$108, %esi
	movl	$36, %edi
	call	ERR_put_error@PLT
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L227:
	movl	$304, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$128, %edx
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L204:
	xorl	%r15d, %r15d
	jmp	.L196
.L231:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE425:
	.size	RAND_DRBG_instantiate, .-RAND_DRBG_instantiate
	.p2align 4
	.globl	RAND_DRBG_uninstantiate
	.type	RAND_DRBG_uninstantiate, @function
RAND_DRBG_uninstantiate:
.LFB426:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	320(%rdi), %rax
	testq	%rax, %rax
	je	.L241
	call	*24(%rax)
	movzwl	28(%rbx), %ecx
	movl	20(%rbx), %eax
	movl	%ecx, %edx
	orl	%eax, %ecx
	je	.L242
.L236:
	movl	$0, 152(%rbx)
	movw	%dx, 28(%rbx)
	movl	%eax, 20(%rbx)
	testl	%eax, %eax
	je	.L237
	subl	$904, %eax
	cmpl	$2, %eax
	ja	.L238
	movq	%rbx, %rdi
	call	drbg_ctr_init@PLT
	testl	%eax, %eax
	je	.L243
.L233:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L242:
	.cfi_restore_state
	movl	rand_drbg_type(%rip), %eax
	movzwl	rand_drbg_flags(%rip), %edx
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L238:
	xorl	%eax, %eax
	movl	$120, %edx
	movl	$104, %esi
	movl	$0, 20(%rbx)
	movw	%ax, 28(%rbx)
	movl	$128, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$36, %edi
	movq	$0, 320(%rbx)
	call	ERR_put_error@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L237:
	.cfi_restore_state
	movq	$0, 320(%rbx)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L243:
	.cfi_restore_state
	movl	$143, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$107, %edx
	movl	%eax, -20(%rbp)
	movl	$2, 152(%rbx)
	movl	$104, %esi
	movl	$36, %edi
	call	ERR_put_error@PLT
	movl	-20(%rbp), %eax
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L241:
	movl	$387, %r8d
	movl	$128, %edx
	movl	$118, %esi
	movl	$2, 152(%rdi)
	leaq	.LC0(%rip), %rcx
	movl	$36, %edi
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L233
	.cfi_endproc
.LFE426:
	.size	RAND_DRBG_uninstantiate, .-RAND_DRBG_uninstantiate
	.p2align 4
	.globl	RAND_DRBG_reseed
	.type	RAND_DRBG_reseed, @function
RAND_DRBG_reseed:
.LFB427:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	152(%rdi), %eax
	movq	$0, -48(%rbp)
	cmpl	$2, %eax
	je	.L271
	testl	%eax, %eax
	je	.L272
	movq	%rdi, %rbx
	movq	%rsi, %r12
	testq	%rsi, %rsi
	je	.L259
	movq	%rdx, %r13
	cmpq	%rdx, 104(%rdi)
	jb	.L273
.L248:
	movl	$2, 152(%rbx)
	movl	136(%rbx), %eax
	testl	%eax, %eax
	jne	.L249
	movq	328(%rbx), %rax
	movq	64(%rbx), %r14
	movl	$0, 140(%rbx)
	testq	%rax, %rax
	je	.L253
.L276:
	movl	%ecx, %r9d
	movl	48(%rbx), %edx
	movq	%r14, %rcx
	movq	72(%rbx), %r8
	leaq	-48(%rbp), %rsi
	movq	%rbx, %rdi
	call	*%rax
	movq	%rax, %r14
	cmpq	%rax, 64(%rbx)
	ja	.L254
	cmpq	%rax, 72(%rbx)
	jb	.L254
.L255:
	movq	320(%rbx), %rax
	movq	%r13, %r8
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	-48(%rbp), %rsi
	movq	%rbx, %rdi
	call	*8(%rax)
	testl	%eax, %eax
	jne	.L274
.L256:
	movq	-48(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L257
	movq	336(%rbx), %rax
	testq	%rax, %rax
	je	.L257
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	*%rax
.L257:
	xorl	%eax, %eax
	cmpl	$1, 152(%rbx)
	sete	%al
.L244:
	movq	-40(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L275
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L249:
	.cfi_restore_state
	addl	$1, %eax
	movl	$1, %edx
	movq	64(%rbx), %r14
	cmove	%edx, %eax
	movl	%eax, 140(%rbx)
	movq	328(%rbx), %rax
	testq	%rax, %rax
	jne	.L276
.L253:
	testq	%r14, %r14
	je	.L255
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L254:
	movl	$446, %r8d
	movl	$110, %edx
	movl	$110, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L274:
	movl	$1, 152(%rbx)
	xorl	%edi, %edi
	movl	$1, 112(%rbx)
	call	time@PLT
	movq	%rax, 120(%rbx)
	movl	140(%rbx), %eax
	movl	%eax, 136(%rbx)
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L273:
	movl	$426, %r8d
	movl	$102, %edx
	movl	$110, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L272:
	movl	$419, %r8d
	movl	$115, %edx
	movl	$110, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L259:
	xorl	%r13d, %r13d
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L271:
	movl	$415, %r8d
	movl	$114, %edx
	movl	$110, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L244
.L275:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE427:
	.size	RAND_DRBG_reseed, .-RAND_DRBG_reseed
	.p2align 4
	.type	drbg_add, @function
drbg_add:
.LFB447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	leaq	rand_drbg_init(%rip), %rdi
	pushq	%r13
	.cfi_offset 13, -32
	movslq	%esi, %r13
	leaq	do_rand_drbg_init_ossl_(%rip), %rsi
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movsd	%xmm0, -40(%rbp)
	call	CRYPTO_THREAD_run_once@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L277
	movl	do_rand_drbg_init_ossl_ret_(%rip), %r12d
	testl	%r12d, %r12d
	je	.L277
	movq	master_drbg(%rip), %rbx
	xorl	%r12d, %r12d
	testl	%r13d, %r13d
	js	.L277
	testq	%rbx, %rbx
	je	.L277
	pxor	%xmm0, %xmm0
	comisd	-40(%rbp), %xmm0
	ja	.L277
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L279
	call	CRYPTO_THREAD_write_lock@PLT
.L279:
	movslq	48(%rbx), %rax
	movq	80(%rbx), %rsi
	movq	64(%rbx), %rdx
	movq	%rax, %rcx
	testq	%rsi, %rsi
	je	.L280
	cmpq	$0, 344(%rbx)
	je	.L338
.L280:
	shrq	$3, %rax
	cmpq	%rdx, %rax
	cmovb	%rdx, %rax
	cmpq	%rax, %r13
	jb	.L337
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	comisd	-40(%rbp), %xmm0
	jbe	.L331
.L337:
	cmpq	$0, 32(%rbx)
	jne	.L282
	testq	%r14, %r14
	je	.L284
.L283:
	cmpq	104(%rbx), %r13
	ja	.L339
.L297:
	movl	152(%rbx), %eax
	cmpl	$2, %eax
	je	.L340
.L298:
	testl	%eax, %eax
	je	.L300
	cmpl	$1, %eax
	je	.L341
.L302:
	movq	32(%rbx), %rdi
	xorl	%r12d, %r12d
	call	rand_pool_free@PLT
	movq	$0, 32(%rbx)
	cmpl	$1, 152(%rbx)
	sete	%r12b
.L294:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L277
	call	CRYPTO_THREAD_unlock@PLT
.L277:
	addq	$16, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L331:
	.cfi_restore_state
	movsd	-40(%rbp), %xmm4
	comisd	%xmm0, %xmm4
	jbe	.L342
	mulsd	.LC2(%rip), %xmm0
	movsd	.LC3(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L291
.L333:
	cvttsd2siq	%xmm0, %rdx
.L290:
	cmpq	$0, 32(%rbx)
	jne	.L282
	testq	%r14, %r14
	je	.L284
	testq	%rdx, %rdx
	je	.L283
	cmpq	72(%rbx), %r13
	ja	.L343
	leaq	0(,%r13,8), %rax
	cmpq	%rdx, %rax
	jb	.L344
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	rand_pool_attach@PLT
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	jne	.L284
	xorl	%r12d, %r12d
	jmp	.L294
.L342:
	movsd	.LC2(%rip), %xmm0
	movsd	.LC3(%rip), %xmm1
	mulsd	%xmm4, %xmm0
	comisd	%xmm1, %xmm0
	jb	.L333
	.p2align 4,,10
	.p2align 3
.L291:
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %rdx
	btcq	$63, %rdx
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L338:
	movl	%eax, %edi
	addq	%rsi, %rdx
	shrl	$31, %edi
	addl	%edi, %ecx
	sarl	%ecx
	movslq	%ecx, %rcx
	addq	%rcx, %rax
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L284:
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L282:
	movl	$491, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$68, %edx
	xorl	%r12d, %r12d
	movl	$102, %esi
	movl	$36, %edi
	call	ERR_put_error@PLT
	movq	32(%rbx), %rdi
	movl	$2, 152(%rbx)
	call	rand_pool_free@PLT
	movq	$0, 32(%rbx)
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L300:
	movl	$28, %edx
	leaq	ossl_pers_string(%rip), %rsi
	movq	%rbx, %rdi
	call	RAND_DRBG_instantiate
	cmpl	$1, 152(%rbx)
	jne	.L302
	testq	%r14, %r14
	je	.L302
.L305:
	movq	320(%rbx), %rax
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	*8(%rax)
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L341:
	testq	%r14, %r14
	jne	.L305
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	RAND_DRBG_reseed
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L340:
	movq	320(%rbx), %rax
	testq	%rax, %rax
	je	.L345
	movq	%rbx, %rdi
	call	*24(%rax)
	movzwl	28(%rbx), %edx
	movl	20(%rbx), %esi
	movq	%rbx, %rdi
	call	RAND_DRBG_set
	movl	152(%rbx), %eax
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L344:
	movl	$508, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$124, %edx
.L335:
	movl	$102, %esi
	movl	$36, %edi
	xorl	%r12d, %r12d
	call	ERR_put_error@PLT
	movl	$2, 152(%rbx)
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L339:
	movl	$519, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$102, %edx
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L343:
	movl	$501, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$106, %edx
	jmp	.L335
.L345:
	movl	$387, %r8d
	movl	$128, %edx
	movl	$118, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	152(%rbx), %eax
	jmp	.L298
	.cfi_endproc
.LFE447:
	.size	drbg_add, .-drbg_add
	.p2align 4
	.type	drbg_seed, @function
drbg_seed:
.LFB448:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	leaq	rand_drbg_init(%rip), %rdi
	pushq	%r13
	.cfi_offset 13, -32
	movl	%esi, %r13d
	leaq	do_rand_drbg_init_ossl_(%rip), %rsi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	call	CRYPTO_THREAD_run_once@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L346
	movl	do_rand_drbg_init_ossl_ret_(%rip), %r12d
	testl	%r12d, %r12d
	je	.L346
	movq	master_drbg(%rip), %rbx
	testl	%r13d, %r13d
	js	.L375
	testq	%rbx, %rbx
	je	.L375
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L348
	call	CRYPTO_THREAD_write_lock@PLT
.L348:
	movslq	48(%rbx), %rax
	movq	80(%rbx), %rsi
	movq	64(%rbx), %rdx
	movq	%rax, %rcx
	testq	%rsi, %rsi
	je	.L349
	cmpq	$0, 344(%rbx)
	je	.L406
.L349:
	shrq	$3, %rax
	movslq	%r13d, %r12
	cmpq	%rdx, %rax
	cmovb	%rdx, %rax
	cmpq	%r12, %rax
	ja	.L405
	pxor	%xmm1, %xmm1
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%r13d, %xmm1
	cvtsi2sdq	%rax, %xmm0
	comisd	%xmm1, %xmm0
	jbe	.L399
.L405:
	cmpq	$0, 32(%rbx)
	jne	.L351
	testq	%r14, %r14
	je	.L353
.L352:
	cmpq	104(%rbx), %r12
	ja	.L407
.L366:
	movl	152(%rbx), %eax
	cmpl	$2, %eax
	je	.L408
.L367:
	testl	%eax, %eax
	je	.L369
	cmpl	$1, %eax
	je	.L409
.L371:
	movq	32(%rbx), %rdi
	xorl	%r12d, %r12d
	call	rand_pool_free@PLT
	movq	$0, 32(%rbx)
	cmpl	$1, 152(%rbx)
	sete	%r12b
.L363:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L346
	call	CRYPTO_THREAD_unlock@PLT
.L346:
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L399:
	.cfi_restore_state
	comisd	%xmm0, %xmm1
	jbe	.L410
	mulsd	.LC2(%rip), %xmm0
	movsd	.LC3(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L360
.L401:
	cvttsd2siq	%xmm0, %rdx
.L359:
	cmpq	$0, 32(%rbx)
	jne	.L351
	testq	%r14, %r14
	je	.L353
	testq	%rdx, %rdx
	je	.L352
	cmpq	72(%rbx), %r12
	ja	.L411
	leaq	0(,%r12,8), %rax
	cmpq	%rdx, %rax
	jb	.L412
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	rand_pool_attach@PLT
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	jne	.L353
	xorl	%r12d, %r12d
	jmp	.L363
.L410:
	mulsd	.LC2(%rip), %xmm1
	movapd	%xmm1, %xmm0
	movsd	.LC3(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jb	.L401
	.p2align 4,,10
	.p2align 3
.L360:
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %rdx
	btcq	$63, %rdx
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L375:
	xorl	%r12d, %r12d
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L406:
	.cfi_restore_state
	movl	%eax, %edi
	addq	%rsi, %rdx
	shrl	$31, %edi
	addl	%edi, %ecx
	sarl	%ecx
	movslq	%ecx, %rcx
	addq	%rcx, %rax
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L353:
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L351:
	movl	$491, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$68, %edx
	xorl	%r12d, %r12d
	movl	$102, %esi
	movl	$36, %edi
	call	ERR_put_error@PLT
	movq	32(%rbx), %rdi
	movl	$2, 152(%rbx)
	call	rand_pool_free@PLT
	movq	$0, 32(%rbx)
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L369:
	movl	$28, %edx
	leaq	ossl_pers_string(%rip), %rsi
	movq	%rbx, %rdi
	call	RAND_DRBG_instantiate
	cmpl	$1, 152(%rbx)
	jne	.L371
	testq	%r14, %r14
	je	.L371
.L374:
	movq	320(%rbx), %rax
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	*8(%rax)
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L409:
	testq	%r14, %r14
	jne	.L374
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	RAND_DRBG_reseed
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L408:
	movq	320(%rbx), %rax
	testq	%rax, %rax
	je	.L413
	movq	%rbx, %rdi
	call	*24(%rax)
	movzwl	28(%rbx), %edx
	movl	20(%rbx), %esi
	movq	%rbx, %rdi
	call	RAND_DRBG_set
	movl	152(%rbx), %eax
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L412:
	movl	$508, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$124, %edx
.L403:
	movl	$102, %esi
	movl	$36, %edi
	xorl	%r12d, %r12d
	call	ERR_put_error@PLT
	movl	$2, 152(%rbx)
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L407:
	movl	$519, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$102, %edx
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L411:
	movl	$501, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$106, %edx
	jmp	.L403
.L413:
	movl	$387, %r8d
	movl	$128, %edx
	movl	$118, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	152(%rbx), %eax
	jmp	.L367
	.cfi_endproc
.LFE448:
	.size	drbg_seed, .-drbg_seed
	.p2align 4
	.globl	rand_drbg_restart
	.type	rand_drbg_restart, @function
rand_drbg_restart:
.LFB428:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	$0, 32(%rdi)
	jne	.L436
	movq	%rsi, %r12
	testq	%rsi, %rsi
	je	.L417
	movq	%rdx, %r13
	testq	%rcx, %rcx
	je	.L418
	cmpq	72(%rdi), %rdx
	ja	.L437
	leaq	0(,%rdx,8), %rax
	cmpq	%rax, %rcx
	ja	.L438
	movq	%rcx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	rand_pool_attach@PLT
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	je	.L439
.L417:
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
.L421:
	movl	152(%rbx), %eax
	cmpl	$2, %eax
	je	.L440
.L422:
	testl	%eax, %eax
	je	.L424
	cmpl	$1, %eax
	je	.L441
.L426:
	movq	32(%rbx), %rdi
	call	rand_pool_free@PLT
	movq	$0, 32(%rbx)
	xorl	%eax, %eax
	cmpl	$1, 152(%rbx)
	sete	%al
.L414:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L436:
	.cfi_restore_state
	movl	$491, %r8d
	movl	$68, %edx
	movl	$102, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	32(%rbx), %rdi
	movl	$2, 152(%rbx)
	call	rand_pool_free@PLT
	movq	$0, 32(%rbx)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L418:
	.cfi_restore_state
	cmpq	104(%rdi), %rdx
	jbe	.L421
	movl	$519, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$102, %edx
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L424:
	movl	$28, %edx
	leaq	ossl_pers_string(%rip), %rsi
	movq	%rbx, %rdi
	call	RAND_DRBG_instantiate
	cmpl	$1, 152(%rbx)
	jne	.L426
	testq	%r12, %r12
	je	.L426
.L429:
	movq	320(%rbx), %rax
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	*8(%rax)
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L441:
	testq	%r12, %r12
	jne	.L429
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	RAND_DRBG_reseed
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L440:
	movq	320(%rbx), %rax
	testq	%rax, %rax
	je	.L442
	movq	%rbx, %rdi
	call	*24(%rax)
	movzwl	28(%rbx), %edx
	movl	20(%rbx), %esi
	movq	%rbx, %rdi
	call	RAND_DRBG_set
	movl	152(%rbx), %eax
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L438:
	movl	$508, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$124, %edx
.L435:
	movl	$102, %esi
	movl	$36, %edi
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	movl	$2, 152(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L437:
	.cfi_restore_state
	movl	$501, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$106, %edx
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L439:
	xorl	%eax, %eax
	jmp	.L414
.L442:
	movl	$387, %r8d
	movl	$128, %edx
	movl	$118, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	152(%rbx), %eax
	jmp	.L422
	.cfi_endproc
.LFE428:
	.size	rand_drbg_restart, .-rand_drbg_restart
	.p2align 4
	.globl	RAND_DRBG_generate
	.type	RAND_DRBG_generate, @function
RAND_DRBG_generate:
.LFB429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpl	$1, 152(%rdi)
	movl	%ecx, -52(%rbp)
	jne	.L470
.L444:
	cmpq	%r14, 56(%rbx)
	jb	.L471
	cmpq	%r12, 104(%rbx)
	jb	.L472
	call	openssl_get_fork_id@PLT
	xorl	%edx, %edx
	cmpl	%eax, 24(%rbx)
	je	.L449
	movl	%eax, 24(%rbx)
	movl	$1, %edx
.L449:
	movl	116(%rbx), %eax
	testl	%eax, %eax
	je	.L450
	cmpl	112(%rbx), %eax
	movl	$1, %eax
	cmovbe	%eax, %edx
.L450:
	cmpq	$0, 128(%rbx)
	jg	.L451
.L455:
	cmpq	$0, 8(%rbx)
	je	.L453
	movl	136(%rbx), %eax
	testl	%eax, %eax
	jne	.L460
.L453:
	orl	-52(%rbp), %edx
	jne	.L458
.L457:
	movq	320(%rbx), %rax
	movq	%r12, %r8
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	*16(%rax)
	testl	%eax, %eax
	je	.L473
	addl	$1, 112(%rbx)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L472:
	.cfi_restore_state
	movl	$603, %r8d
	movl	$102, %edx
	movl	$107, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L443:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L470:
	.cfi_restore_state
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	rand_drbg_restart
	movl	152(%rbx), %eax
	cmpl	$2, %eax
	je	.L474
	testl	%eax, %eax
	jne	.L444
	movl	$593, %r8d
	movl	$115, %edx
	movl	$107, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L451:
	xorl	%edi, %edi
	movl	%edx, -56(%rbp)
	call	time@PLT
	movq	120(%rbx), %rcx
	cmpq	%rax, %rcx
	jg	.L454
	subq	%rcx, %rax
	cmpq	128(%rbx), %rax
	jl	.L475
.L454:
	cmpq	$0, 8(%rbx)
	je	.L458
	movl	136(%rbx), %eax
	movl	$1, %edx
	testl	%eax, %eax
	je	.L458
.L460:
	movq	8(%rbx), %rcx
	movl	136(%rcx), %ecx
	cmpl	%eax, %ecx
	je	.L453
.L458:
	movl	-52(%rbp), %ecx
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	RAND_DRBG_reseed
	testl	%eax, %eax
	je	.L456
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L471:
	movl	$599, %r8d
	movl	$117, %edx
	movl	$107, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L473:
	.cfi_restore_state
	movl	$643, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$112, %edx
	movl	%eax, -52(%rbp)
	movl	$2, 152(%rbx)
	movl	$107, %esi
	movl	$36, %edi
	call	ERR_put_error@PLT
	movl	-52(%rbp), %eax
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L474:
	movl	$589, %r8d
	movl	$114, %edx
	movl	$107, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L475:
	movl	-56(%rbp), %edx
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L456:
	movl	$634, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$118, %edx
	movl	%eax, -52(%rbp)
	movl	$107, %esi
	movl	$36, %edi
	call	ERR_put_error@PLT
	movl	-52(%rbp), %eax
	jmp	.L443
	.cfi_endproc
.LFE429:
	.size	RAND_DRBG_generate, .-RAND_DRBG_generate
	.p2align 4
	.globl	RAND_DRBG_bytes
	.type	RAND_DRBG_bytes, @function
RAND_DRBG_bytes:
.LFB430:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	40(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	testq	%rdi, %rdi
	je	.L521
.L477:
	leaq	-64(%rbp), %rsi
	call	rand_drbg_get_additional_data@PLT
	movq	%rax, -72(%rbp)
	testq	%r12, %r12
	je	.L497
	.p2align 4,,10
	.p2align 3
.L480:
	movq	56(%rbx), %rax
	movq	%r12, %r15
	movq	-64(%rbp), %r14
	cmpq	%r12, %rax
	cmovbe	%rax, %r15
	cmpl	$1, 152(%rbx)
	je	.L481
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	rand_drbg_restart
	movl	152(%rbx), %eax
	cmpl	$2, %eax
	je	.L522
	testl	%eax, %eax
	je	.L483
	movq	56(%rbx), %rax
.L481:
	cmpq	%rax, %r15
	ja	.L523
	movq	-72(%rbp), %rax
	cmpq	104(%rbx), %rax
	ja	.L524
	call	openssl_get_fork_id@PLT
	xorl	%edx, %edx
	cmpl	24(%rbx), %eax
	je	.L486
	movl	%eax, 24(%rbx)
	movl	$1, %edx
.L486:
	movl	116(%rbx), %eax
	movq	128(%rbx), %rcx
	testl	%eax, %eax
	je	.L487
	cmpl	112(%rbx), %eax
	ja	.L487
	testq	%rcx, %rcx
	jg	.L502
.L520:
	cmpq	$0, 8(%rbx)
	je	.L494
	movl	136(%rbx), %eax
	movl	$1, %edx
	testl	%eax, %eax
	je	.L494
.L498:
	movq	8(%rbx), %rcx
	movl	136(%rcx), %ecx
	cmpl	%eax, %ecx
	je	.L495
.L494:
	movq	-72(%rbp), %rdx
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	call	RAND_DRBG_reseed
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L490
	xorl	%r8d, %r8d
	xorl	%r14d, %r14d
.L491:
	movq	320(%rbx), %rax
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	*16(%rax)
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L525
	addl	$1, 112(%rbx)
	addq	%r15, %r13
	subq	%r15, %r12
	jne	.L480
.L497:
	movl	$1, %r14d
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L487:
	testq	%rcx, %rcx
	jg	.L488
.L492:
	cmpq	$0, 8(%rbx)
	je	.L495
	movl	136(%rbx), %eax
	testl	%eax, %eax
	jne	.L498
.L495:
	testl	%edx, %edx
	jne	.L494
	movq	-72(%rbp), %r8
	jmp	.L491
	.p2align 4,,10
	.p2align 3
.L524:
	movl	$603, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$102, %edx
	xorl	%r14d, %r14d
	movl	$107, %esi
	movl	$36, %edi
	call	ERR_put_error@PLT
.L479:
	movq	-64(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L476
	movq	40(%rbx), %rdi
	call	rand_drbg_cleanup_additional_data@PLT
.L476:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L526
	addq	$40, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L502:
	.cfi_restore_state
	movl	$1, %edx
.L488:
	xorl	%edi, %edi
	movl	%edx, -76(%rbp)
	call	time@PLT
	movq	120(%rbx), %rcx
	cmpq	%rcx, %rax
	jl	.L520
	subq	%rcx, %rax
	cmpq	128(%rbx), %rax
	movl	-76(%rbp), %edx
	jl	.L492
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L523:
	movl	$599, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$117, %edx
	xorl	%r14d, %r14d
	movl	$107, %esi
	movl	$36, %edi
	call	ERR_put_error@PLT
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L483:
	movl	$593, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$115, %edx
	xorl	%r14d, %r14d
	movl	$107, %esi
	movl	$36, %edi
	call	ERR_put_error@PLT
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L525:
	movl	$643, %r8d
	movl	$112, %edx
	movl	$107, %esi
	movl	$2, 152(%rbx)
	leaq	.LC0(%rip), %rcx
	movl	$36, %edi
	call	ERR_put_error@PLT
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L521:
	movl	20(%rbx), %r14d
	testl	%r14d, %r14d
	je	.L476
	movq	104(%rbx), %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	rand_pool_new@PLT
	movq	%rax, 40(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L477
	xorl	%r14d, %r14d
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L522:
	movl	$589, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$114, %edx
	xorl	%r14d, %r14d
	movl	$107, %esi
	movl	$36, %edi
	call	ERR_put_error@PLT
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L490:
	movl	$634, %r8d
	movl	$118, %edx
	movl	$107, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L479
.L526:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE430:
	.size	RAND_DRBG_bytes, .-RAND_DRBG_bytes
	.p2align 4
	.globl	RAND_DRBG_set_callbacks
	.type	RAND_DRBG_set_callbacks, @function
RAND_DRBG_set_callbacks:
.LFB431:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdx, -8(%rbp)
	movl	152(%rdi), %edx
	movq	%r8, -16(%rbp)
	testl	%edx, %edx
	jne	.L527
	cmpq	$0, 8(%rdi)
	je	.L532
.L527:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L532:
	.cfi_restore_state
	movq	%rsi, %xmm0
	movl	$1, %eax
	movhps	-8(%rbp), %xmm0
	movups	%xmm0, 328(%rdi)
	movq	%rcx, %xmm0
	movhps	-16(%rbp), %xmm0
	movups	%xmm0, 344(%rdi)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE431:
	.size	RAND_DRBG_set_callbacks, .-RAND_DRBG_set_callbacks
	.p2align 4
	.globl	RAND_DRBG_set_reseed_interval
	.type	RAND_DRBG_set_reseed_interval, @function
RAND_DRBG_set_reseed_interval:
.LFB432:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$16777216, %esi
	ja	.L533
	movl	%esi, 116(%rdi)
	movl	$1, %eax
.L533:
	ret
	.cfi_endproc
.LFE432:
	.size	RAND_DRBG_set_reseed_interval, .-RAND_DRBG_set_reseed_interval
	.p2align 4
	.globl	RAND_DRBG_set_reseed_time_interval
	.type	RAND_DRBG_set_reseed_time_interval, @function
RAND_DRBG_set_reseed_time_interval:
.LFB433:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	$1048576, %rsi
	jg	.L536
	movq	%rsi, 128(%rdi)
	movl	$1, %eax
.L536:
	ret
	.cfi_endproc
.LFE433:
	.size	RAND_DRBG_set_reseed_time_interval, .-RAND_DRBG_set_reseed_time_interval
	.p2align 4
	.globl	RAND_DRBG_set_reseed_defaults
	.type	RAND_DRBG_set_reseed_defaults, @function
RAND_DRBG_set_reseed_defaults:
.LFB434:
	.cfi_startproc
	endbr64
	cmpl	$16777216, %edi
	ja	.L542
	cmpl	$16777216, %esi
	ja	.L542
	cmpq	$1048576, %rdx
	jg	.L542
	cmpq	$1048576, %rcx
	jg	.L542
	movl	%edi, master_reseed_interval(%rip)
	movl	$1, %eax
	movl	%esi, slave_reseed_interval(%rip)
	movq	%rdx, master_reseed_time_interval(%rip)
	movq	%rcx, slave_reseed_time_interval(%rip)
	ret
	.p2align 4,,10
	.p2align 3
.L542:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE434:
	.size	RAND_DRBG_set_reseed_defaults, .-RAND_DRBG_set_reseed_defaults
	.p2align 4
	.globl	rand_drbg_lock
	.type	rand_drbg_lock, @function
rand_drbg_lock:
.LFB435:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.L545
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L545:
	jmp	CRYPTO_THREAD_write_lock@PLT
	.cfi_endproc
.LFE435:
	.size	rand_drbg_lock, .-rand_drbg_lock
	.p2align 4
	.globl	rand_drbg_unlock
	.type	rand_drbg_unlock, @function
rand_drbg_unlock:
.LFB436:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.L548
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L548:
	jmp	CRYPTO_THREAD_unlock@PLT
	.cfi_endproc
.LFE436:
	.size	rand_drbg_unlock, .-rand_drbg_unlock
	.p2align 4
	.globl	rand_drbg_enable_locking
	.type	rand_drbg_enable_locking, @function
rand_drbg_enable_locking:
.LFB437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	152(%rdi), %eax
	testl	%eax, %eax
	jne	.L559
	cmpq	$0, (%rdi)
	movq	%rdi, %rbx
	je	.L552
.L554:
	movl	$1, %eax
.L549:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L552:
	.cfi_restore_state
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L553
	cmpq	$0, (%rax)
	je	.L560
.L553:
	call	CRYPTO_THREAD_lock_new@PLT
	movq	%rax, (%rbx)
	testq	%rax, %rax
	jne	.L554
	movl	$840, %r8d
	movl	$126, %edx
	movl	$119, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L559:
	movl	$826, %r8d
	movl	$129, %edx
	movl	$119, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L560:
	.cfi_restore_state
	movl	$833, %r8d
	movl	$130, %edx
	movl	$119, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L549
	.cfi_endproc
.LFE437:
	.size	rand_drbg_enable_locking, .-rand_drbg_enable_locking
	.p2align 4
	.type	do_rand_drbg_init_ossl_, @function
do_rand_drbg_init_ossl_:
.LFB441:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	OPENSSL_init_crypto@PLT
	testl	%eax, %eax
	jne	.L562
.L582:
	xorl	%eax, %eax
.L563:
	movl	%eax, do_rand_drbg_init_ossl_ret_(%rip)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L562:
	.cfi_restore_state
	xorl	%esi, %esi
	leaq	private_drbg(%rip), %rdi
	call	CRYPTO_THREAD_init_local@PLT
	testl	%eax, %eax
	je	.L582
	xorl	%esi, %esi
	leaq	public_drbg(%rip), %rdi
	call	CRYPTO_THREAD_init_local@PLT
	testl	%eax, %eax
	jne	.L583
.L565:
	leaq	private_drbg(%rip), %rdi
	call	CRYPTO_THREAD_cleanup_local@PLT
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L583:
	movl	rand_drbg_flags(%rip), %edx
	movl	rand_drbg_type(%rip), %esi
	xorl	%ecx, %ecx
	movl	$1, %edi
	call	rand_drbg_new
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L581
	movq	%rax, %rdi
	call	rand_drbg_enable_locking
	testl	%eax, %eax
	je	.L584
	movl	$1, 136(%r12)
	movl	$28, %edx
	leaq	ossl_pers_string(%rip), %rsi
	movq	%r12, %rdi
	call	RAND_DRBG_instantiate
	movq	%r12, master_drbg(%rip)
	movl	$1, %eax
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L584:
	movq	320(%r12), %rax
	testq	%rax, %rax
	je	.L569
	movq	%r12, %rdi
	call	*24(%rax)
.L569:
	movq	40(%r12), %rdi
	call	rand_pool_free@PLT
	movq	(%r12), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	leaq	160(%r12), %rdx
	movq	%r12, %rsi
	movl	$15, %edi
	call	CRYPTO_free_ex_data@PLT
	movl	16(%r12), %eax
	testl	%eax, %eax
	je	.L570
	movl	$275, %ecx
	leaq	.LC0(%rip), %rdx
	movl	$360, %esi
	movq	%r12, %rdi
	call	CRYPTO_secure_clear_free@PLT
.L581:
	movq	$0, master_drbg(%rip)
.L567:
	leaq	public_drbg(%rip), %rdi
	call	CRYPTO_THREAD_cleanup_local@PLT
	jmp	.L565
.L570:
	movl	$277, %ecx
	leaq	.LC0(%rip), %rdx
	movl	$360, %esi
	movq	%r12, %rdi
	call	CRYPTO_clear_free@PLT
	movq	$0, master_drbg(%rip)
	jmp	.L567
	.cfi_endproc
.LFE441:
	.size	do_rand_drbg_init_ossl_, .-do_rand_drbg_init_ossl_
	.p2align 4
	.type	drbg_bytes, @function
drbg_bytes:
.LFB445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	leaq	rand_drbg_init(%rip), %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	leaq	do_rand_drbg_init_ossl_(%rip), %rsi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L585
	movl	do_rand_drbg_init_ossl_ret_(%rip), %edx
	testl	%edx, %edx
	je	.L585
	leaq	public_drbg(%rip), %rdi
	call	CRYPTO_THREAD_get_local@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L604
.L587:
	movslq	%ebx, %rdx
	movq	%r12, %rsi
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	RAND_DRBG_bytes
	.p2align 4,,10
	.p2align 3
.L604:
	.cfi_restore_state
	movl	$4, %edi
	call	ossl_init_thread_start@PLT
	testl	%eax, %eax
	je	.L585
	movq	master_drbg(%rip), %r14
	movl	rand_drbg_flags(%rip), %edx
	movl	$1, %edi
	movl	rand_drbg_type(%rip), %esi
	movq	%r14, %rcx
	call	rand_drbg_new
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L603
	testq	%r14, %r14
	jne	.L590
	movq	%rax, %rdi
	call	rand_drbg_enable_locking
	testl	%eax, %eax
	jne	.L590
	movq	320(%r13), %rax
	testq	%rax, %rax
	je	.L591
	movq	%r13, %rdi
	call	*24(%rax)
.L591:
	movq	40(%r13), %rdi
	call	rand_pool_free@PLT
	movq	0(%r13), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	leaq	160(%r13), %rdx
	movq	%r13, %rsi
	movl	$15, %edi
	call	CRYPTO_free_ex_data@PLT
	movl	16(%r13), %eax
	testl	%eax, %eax
	je	.L592
	movl	$275, %ecx
	leaq	.LC0(%rip), %rdx
	movl	$360, %esi
	movq	%r13, %rdi
	call	CRYPTO_secure_clear_free@PLT
.L603:
	xorl	%esi, %esi
	leaq	public_drbg(%rip), %rdi
	call	CRYPTO_THREAD_set_local@PLT
.L585:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L590:
	.cfi_restore_state
	movl	$1, 136(%r13)
	movq	%r13, %rdi
	movl	$28, %edx
	leaq	ossl_pers_string(%rip), %rsi
	call	RAND_DRBG_instantiate
	movq	%r13, %rsi
	leaq	public_drbg(%rip), %rdi
	call	CRYPTO_THREAD_set_local@PLT
	jmp	.L587
.L592:
	movq	%r13, %rdi
	movl	$277, %ecx
	movl	$360, %esi
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	xorl	%esi, %esi
	leaq	public_drbg(%rip), %rdi
	call	CRYPTO_THREAD_set_local@PLT
	jmp	.L585
	.cfi_endproc
.LFE445:
	.size	drbg_bytes, .-drbg_bytes
	.p2align 4
	.globl	RAND_DRBG_set_ex_data
	.type	RAND_DRBG_set_ex_data, @function
RAND_DRBG_set_ex_data:
.LFB438:
	.cfi_startproc
	endbr64
	addq	$160, %rdi
	jmp	CRYPTO_set_ex_data@PLT
	.cfi_endproc
.LFE438:
	.size	RAND_DRBG_set_ex_data, .-RAND_DRBG_set_ex_data
	.p2align 4
	.globl	RAND_DRBG_get_ex_data
	.type	RAND_DRBG_get_ex_data, @function
RAND_DRBG_get_ex_data:
.LFB439:
	.cfi_startproc
	endbr64
	addq	$160, %rdi
	jmp	CRYPTO_get_ex_data@PLT
	.cfi_endproc
.LFE439:
	.size	RAND_DRBG_get_ex_data, .-RAND_DRBG_get_ex_data
	.p2align 4
	.globl	rand_drbg_cleanup_int
	.type	rand_drbg_cleanup_int, @function
rand_drbg_cleanup_int:
.LFB443:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	master_drbg(%rip), %r12
	testq	%r12, %r12
	je	.L607
	movq	320(%r12), %rax
	testq	%rax, %rax
	je	.L609
	movq	%r12, %rdi
	call	*24(%rax)
.L609:
	movq	40(%r12), %rdi
	call	rand_pool_free@PLT
	movq	(%r12), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	leaq	160(%r12), %rdx
	movq	%r12, %rsi
	movl	$15, %edi
	call	CRYPTO_free_ex_data@PLT
	movl	16(%r12), %eax
	testl	%eax, %eax
	je	.L610
	movl	$275, %ecx
	leaq	.LC0(%rip), %rdx
	movl	$360, %esi
	movq	%r12, %rdi
	call	CRYPTO_secure_clear_free@PLT
.L611:
	leaq	private_drbg(%rip), %rdi
	movq	$0, master_drbg(%rip)
	call	CRYPTO_THREAD_cleanup_local@PLT
	addq	$8, %rsp
	leaq	public_drbg(%rip), %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_THREAD_cleanup_local@PLT
	.p2align 4,,10
	.p2align 3
.L607:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L610:
	.cfi_restore_state
	movl	$277, %ecx
	leaq	.LC0(%rip), %rdx
	movl	$360, %esi
	movq	%r12, %rdi
	call	CRYPTO_clear_free@PLT
	jmp	.L611
	.cfi_endproc
.LFE443:
	.size	rand_drbg_cleanup_int, .-rand_drbg_cleanup_int
	.p2align 4
	.globl	drbg_delete_thread_state
	.type	drbg_delete_thread_state, @function
drbg_delete_thread_state:
.LFB444:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	public_drbg(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	CRYPTO_THREAD_get_local@PLT
	xorl	%esi, %esi
	leaq	public_drbg(%rip), %rdi
	movq	%rax, %r12
	call	CRYPTO_THREAD_set_local@PLT
	testq	%r12, %r12
	je	.L617
	movq	320(%r12), %rax
	testq	%rax, %rax
	je	.L618
	movq	%r12, %rdi
	call	*24(%rax)
.L618:
	movq	40(%r12), %rdi
	call	rand_pool_free@PLT
	movq	(%r12), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	leaq	160(%r12), %rdx
	movq	%r12, %rsi
	movl	$15, %edi
	call	CRYPTO_free_ex_data@PLT
	movl	16(%r12), %edx
	testl	%edx, %edx
	je	.L619
	movl	$275, %ecx
	leaq	.LC0(%rip), %rdx
	movl	$360, %esi
	movq	%r12, %rdi
	call	CRYPTO_secure_clear_free@PLT
.L617:
	leaq	private_drbg(%rip), %rdi
	call	CRYPTO_THREAD_get_local@PLT
	xorl	%esi, %esi
	leaq	private_drbg(%rip), %rdi
	movq	%rax, %r12
	call	CRYPTO_THREAD_set_local@PLT
	testq	%r12, %r12
	je	.L616
	movq	320(%r12), %rax
	testq	%rax, %rax
	je	.L621
	movq	%r12, %rdi
	call	*24(%rax)
.L621:
	movq	40(%r12), %rdi
	call	rand_pool_free@PLT
	movq	(%r12), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	leaq	160(%r12), %rdx
	movq	%r12, %rsi
	movl	$15, %edi
	call	CRYPTO_free_ex_data@PLT
	movl	16(%r12), %eax
	testl	%eax, %eax
	je	.L622
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$275, %ecx
	movl	$360, %esi
	popq	%r12
	leaq	.LC0(%rip), %rdx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_secure_clear_free@PLT
	.p2align 4,,10
	.p2align 3
.L622:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$277, %ecx
	movl	$360, %esi
	popq	%r12
	leaq	.LC0(%rip), %rdx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_clear_free@PLT
	.p2align 4,,10
	.p2align 3
.L619:
	.cfi_restore_state
	movl	$277, %ecx
	leaq	.LC0(%rip), %rdx
	movl	$360, %esi
	movq	%r12, %rdi
	call	CRYPTO_clear_free@PLT
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L616:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE444:
	.size	drbg_delete_thread_state, .-drbg_delete_thread_state
	.p2align 4
	.globl	rand_drbg_seedlen
	.type	rand_drbg_seedlen, @function
rand_drbg_seedlen:
.LFB446:
	.cfi_startproc
	endbr64
	movslq	48(%rdi), %rax
	movq	80(%rdi), %rsi
	movq	64(%rdi), %rdx
	movq	%rax, %rcx
	testq	%rsi, %rsi
	je	.L634
	cmpq	$0, 344(%rdi)
	je	.L638
.L634:
	shrq	$3, %rax
	cmpq	%rdx, %rax
	cmovb	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L638:
	movl	%eax, %edi
	addq	%rsi, %rdx
	shrl	$31, %edi
	addl	%edi, %ecx
	sarl	%ecx
	movslq	%ecx, %rcx
	addq	%rcx, %rax
	shrq	$3, %rax
	cmpq	%rdx, %rax
	cmovb	%rdx, %rax
	ret
	.cfi_endproc
.LFE446:
	.size	rand_drbg_seedlen, .-rand_drbg_seedlen
	.p2align 4
	.globl	RAND_DRBG_get0_master
	.type	RAND_DRBG_get0_master, @function
RAND_DRBG_get0_master:
.LFB450:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	do_rand_drbg_init_ossl_(%rip), %rsi
	leaq	rand_drbg_init(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L642
	movl	do_rand_drbg_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	je	.L642
	movq	master_drbg(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L642:
	.cfi_restore_state
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE450:
	.size	RAND_DRBG_get0_master, .-RAND_DRBG_get0_master
	.p2align 4
	.globl	RAND_DRBG_get0_public
	.type	RAND_DRBG_get0_public, @function
RAND_DRBG_get0_public:
.LFB451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	do_rand_drbg_init_ossl_(%rip), %rsi
	leaq	rand_drbg_init(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L647
	movl	do_rand_drbg_init_ossl_ret_(%rip), %edx
	testl	%edx, %edx
	je	.L647
	leaq	public_drbg(%rip), %rdi
	call	CRYPTO_THREAD_get_local@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L662
.L644:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L647:
	.cfi_restore_state
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L662:
	.cfi_restore_state
	movl	$4, %edi
	call	ossl_init_thread_start@PLT
	testl	%eax, %eax
	je	.L647
	movq	master_drbg(%rip), %rbx
	movl	rand_drbg_flags(%rip), %edx
	movl	$1, %edi
	movl	rand_drbg_type(%rip), %esi
	movq	%rbx, %rcx
	call	rand_drbg_new
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L648
	testq	%rbx, %rbx
	je	.L663
.L649:
	movl	$1, 136(%r12)
	movl	$28, %edx
	leaq	ossl_pers_string(%rip), %rsi
	movq	%r12, %rdi
	call	RAND_DRBG_instantiate
.L648:
	movq	%r12, %rsi
	leaq	public_drbg(%rip), %rdi
	call	CRYPTO_THREAD_set_local@PLT
	jmp	.L644
	.p2align 4,,10
	.p2align 3
.L663:
	movq	%rax, %rdi
	call	rand_drbg_enable_locking
	testl	%eax, %eax
	jne	.L649
	movq	320(%r12), %rax
	testq	%rax, %rax
	je	.L650
	movq	%r12, %rdi
	call	*24(%rax)
.L650:
	movq	40(%r12), %rdi
	call	rand_pool_free@PLT
	movq	(%r12), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	leaq	160(%r12), %rdx
	movq	%r12, %rsi
	movl	$15, %edi
	call	CRYPTO_free_ex_data@PLT
	movl	16(%r12), %eax
	testl	%eax, %eax
	je	.L651
	movq	%r12, %rdi
	movl	$275, %ecx
	movl	$360, %esi
	xorl	%r12d, %r12d
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_secure_clear_free@PLT
	jmp	.L648
.L651:
	movq	%r12, %rdi
	movl	$277, %ecx
	movl	$360, %esi
	xorl	%r12d, %r12d
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	jmp	.L648
	.cfi_endproc
.LFE451:
	.size	RAND_DRBG_get0_public, .-RAND_DRBG_get0_public
	.p2align 4
	.globl	RAND_DRBG_get0_private
	.type	RAND_DRBG_get0_private, @function
RAND_DRBG_get0_private:
.LFB452:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	do_rand_drbg_init_ossl_(%rip), %rsi
	leaq	rand_drbg_init(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L667
	movl	do_rand_drbg_init_ossl_ret_(%rip), %edx
	testl	%edx, %edx
	je	.L667
	leaq	private_drbg(%rip), %rdi
	call	CRYPTO_THREAD_get_local@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L682
.L664:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L667:
	.cfi_restore_state
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L682:
	.cfi_restore_state
	movl	$4, %edi
	call	ossl_init_thread_start@PLT
	testl	%eax, %eax
	je	.L667
	movq	master_drbg(%rip), %rbx
	movl	rand_drbg_flags(%rip), %edx
	movl	$1, %edi
	movl	rand_drbg_type(%rip), %esi
	movq	%rbx, %rcx
	call	rand_drbg_new
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L668
	testq	%rbx, %rbx
	je	.L683
.L669:
	movl	$1, 136(%r12)
	movl	$28, %edx
	leaq	ossl_pers_string(%rip), %rsi
	movq	%r12, %rdi
	call	RAND_DRBG_instantiate
.L668:
	movq	%r12, %rsi
	leaq	private_drbg(%rip), %rdi
	call	CRYPTO_THREAD_set_local@PLT
	jmp	.L664
	.p2align 4,,10
	.p2align 3
.L683:
	movq	%rax, %rdi
	call	rand_drbg_enable_locking
	testl	%eax, %eax
	jne	.L669
	movq	320(%r12), %rax
	testq	%rax, %rax
	je	.L670
	movq	%r12, %rdi
	call	*24(%rax)
.L670:
	movq	40(%r12), %rdi
	call	rand_pool_free@PLT
	movq	(%r12), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	leaq	160(%r12), %rdx
	movq	%r12, %rsi
	movl	$15, %edi
	call	CRYPTO_free_ex_data@PLT
	movl	16(%r12), %eax
	testl	%eax, %eax
	je	.L671
	movq	%r12, %rdi
	movl	$275, %ecx
	movl	$360, %esi
	xorl	%r12d, %r12d
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_secure_clear_free@PLT
	jmp	.L668
.L671:
	movq	%r12, %rdi
	movl	$277, %ecx
	movl	$360, %esi
	xorl	%r12d, %r12d
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	jmp	.L668
	.cfi_endproc
.LFE452:
	.size	RAND_DRBG_get0_private, .-RAND_DRBG_get0_private
	.p2align 4
	.globl	RAND_OpenSSL
	.type	RAND_OpenSSL, @function
RAND_OpenSSL:
.LFB453:
	.cfi_startproc
	endbr64
	leaq	rand_meth(%rip), %rax
	ret
	.cfi_endproc
.LFE453:
	.size	RAND_OpenSSL, .-RAND_OpenSSL
	.globl	rand_meth
	.section	.data.rel.local,"aw"
	.align 32
	.type	rand_meth, @object
	.size	rand_meth, 48
rand_meth:
	.quad	drbg_seed
	.quad	drbg_bytes
	.quad	0
	.quad	drbg_add
	.quad	drbg_bytes
	.quad	drbg_status
	.local	do_rand_drbg_init_ossl_ret_
	.comm	do_rand_drbg_init_ossl_ret_,4,4
	.data
	.align 8
	.type	slave_reseed_time_interval, @object
	.size	slave_reseed_time_interval, 8
slave_reseed_time_interval:
	.quad	420
	.align 8
	.type	master_reseed_time_interval, @object
	.size	master_reseed_time_interval, 8
master_reseed_time_interval:
	.quad	3600
	.align 4
	.type	slave_reseed_interval, @object
	.size	slave_reseed_interval, 4
slave_reseed_interval:
	.long	65536
	.align 4
	.type	master_reseed_interval, @object
	.size	master_reseed_interval, 4
master_reseed_interval:
	.long	256
	.local	rand_drbg_flags
	.comm	rand_drbg_flags,4,4
	.align 4
	.type	rand_drbg_type, @object
	.size	rand_drbg_type, 4
rand_drbg_type:
	.long	906
	.local	rand_drbg_init
	.comm	rand_drbg_init,4,4
	.section	.rodata
	.align 16
	.type	ossl_pers_string, @object
	.size	ossl_pers_string, 29
ossl_pers_string:
	.string	"OpenSSL NIST SP 800-90A DRBG"
	.local	private_drbg
	.comm	private_drbg,4,4
	.local	public_drbg
	.comm	public_drbg,4,4
	.local	master_drbg
	.comm	master_drbg,8,8
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC2:
	.long	0
	.long	1075838976
	.align 8
.LC3:
	.long	0
	.long	1138753536
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
