	.file	"ct_vfy.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ct/ct_vfy.c"
	.text
	.p2align 4
	.globl	SCT_CTX_verify
	.type	SCT_CTX_verify, @function
SCT_CTX_verify:
.LFB1308:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	SCT_is_complete@PLT
	testl	%eax, %eax
	je	.L2
	cmpq	$0, 0(%r13)
	je	.L2
	movl	88(%rbx), %eax
	cmpl	$-1, %eax
	je	.L2
	cmpl	$1, %eax
	je	.L54
.L3:
	movl	(%rbx), %r12d
	testl	%r12d, %r12d
	jne	.L55
	movq	16(%r13), %rdx
	cmpq	%rdx, 32(%rbx)
	jne	.L6
	movq	8(%r13), %rsi
	movq	24(%rbx), %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L6
	movq	72(%r13), %rax
	cmpq	%rax, 40(%rbx)
	ja	.L56
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L14
	movq	0(%r13), %r12
	call	EVP_sha256@PLT
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%r12, %r8
	call	EVP_DigestVerifyInit@PLT
	testl	%eax, %eax
	jne	.L57
.L14:
	xorl	%r12d, %r12d
.L10:
	movq	%r14, %rdi
	call	EVP_MD_CTX_free@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L54:
	cmpq	$0, 24(%r13)
	jne	.L3
	.p2align 4,,10
	.p2align 3
.L2:
	movl	$104, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$106, %edx
	xorl	%r12d, %r12d
	movl	$128, %esi
	movl	$50, %edi
	call	ERR_put_error@PLT
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L58
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movl	$113, %r8d
	movl	$114, %edx
	movl	$128, %esi
	movl	$50, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L55:
	movl	$108, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$115, %edx
	xorl	%r12d, %r12d
	movl	$128, %esi
	movl	$50, %edi
	call	ERR_put_error@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L56:
	movl	$117, %r8d
	movl	$116, %edx
	movl	$128, %esi
	movl	$50, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L57:
	movzwl	88(%rbx), %eax
	movl	88(%rbx), %edx
	rolw	$8, %ax
	cmpl	$-1, %edx
	je	.L14
	cmpl	$1, %edx
	jne	.L13
	cmpq	$0, 24(%r13)
	je	.L14
.L13:
	movl	(%rbx), %edx
	leaq	-68(%rbp), %r12
	movq	%r14, %rdi
	movb	$0, -67(%rbp)
	movq	%r12, %rsi
	movw	%ax, -58(%rbp)
	movb	%dl, -68(%rbp)
	movq	40(%rbx), %rdx
	bswap	%rdx
	movq	%rdx, -66(%rbp)
	movl	$12, %edx
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L14
	movl	88(%rbx), %eax
	testl	%eax, %eax
	jne	.L15
	movq	40(%r13), %r15
	movq	48(%r13), %r13
.L16:
	testq	%r15, %r15
	je	.L14
	movq	%r13, %rax
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	shrq	$16, %rax
	movb	%al, -68(%rbp)
	movl	%r13d, %eax
	rolw	$8, %ax
	movw	%ax, -67(%rbp)
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L14
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L14
	movzwl	56(%rbx), %eax
	movl	$2, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	rolw	$8, %ax
	movw	%ax, -68(%rbp)
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L14
	movq	56(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.L59
.L17:
	movq	80(%rbx), %rdx
	movq	72(%rbx), %rsi
	movq	%r14, %rdi
	call	EVP_DigestVerifyFinal@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L10
	movl	$135, %r8d
	movl	$107, %edx
	movl	$128, %esi
	movl	$50, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L10
.L15:
	movq	32(%r13), %rdx
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L14
	movq	56(%r13), %r15
	movq	64(%r13), %r13
	jmp	.L16
.L59:
	movq	48(%rbx), %rsi
	movq	%r14, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jne	.L17
	jmp	.L14
.L58:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1308:
	.size	SCT_CTX_verify, .-SCT_CTX_verify
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
