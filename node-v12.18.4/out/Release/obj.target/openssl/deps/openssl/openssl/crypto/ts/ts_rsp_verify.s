	.file	"ts_rsp_verify.c"
	.text
	.p2align 4
	.type	ts_issuer_serial_cmp, @function
ts_issuer_serial_cmp:
.LFB1375:
	.cfi_startproc
	testq	%rdi, %rdi
	je	.L7
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	testq	%rsi, %rsi
	je	.L5
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	$1, %eax
	jne	.L5
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	call	OPENSSL_sk_value@PLT
	cmpl	$4, (%rax)
	movq	%rax, %r13
	jne	.L5
	movq	%r12, %rdi
	call	X509_get_issuer_name@PLT
	movq	8(%r13), %rdi
	movq	%rax, %rsi
	call	X509_NAME_cmp@PLT
	testl	%eax, %eax
	jne	.L5
	movq	%r12, %rdi
	call	X509_get_serialNumber@PLT
	movq	8(%rbx), %rdi
	movq	%rax, %rsi
	call	ASN1_INTEGER_cmp@PLT
	testl	%eax, %eax
	setne	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	popq	%rbx
	popq	%r12
	negl	%eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE1375:
	.size	ts_issuer_serial_cmp, .-ts_issuer_serial_cmp
	.p2align 4
	.type	ts_find_cert, @function
ts_find_cert:
.LFB1373:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L15
	movq	%rsi, %r14
	testq	%rsi, %rsi
	je	.L15
	xorl	%edx, %edx
	movl	$-1, %esi
	leaq	-64(%rbp), %r13
	movq	%rdi, %rbx
	movq	%r14, %rdi
	call	X509_check_purpose@PLT
	call	EVP_sha1@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	X509_digest@PLT
	testl	%eax, %eax
	je	.L15
	xorl	%r12d, %r12d
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L20:
	addl	$1, %r12d
.L16:
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L15
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	movq	(%rax), %rdx
	cmpl	$20, (%rdx)
	jne	.L20
	movq	8(%rdx), %rsi
	movq	(%rsi), %rdx
	movq	8(%rsi), %rcx
	xorq	0(%r13), %rdx
	xorq	8(%r13), %rcx
	orq	%rdx, %rcx
	jne	.L20
	movl	16(%r13), %ecx
	cmpl	%ecx, 16(%rsi)
	jne	.L20
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L11
	movq	%r14, %rsi
	call	ts_issuer_serial_cmp
	testl	%eax, %eax
	jne	.L20
	.p2align 4,,10
	.p2align 3
.L11:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L33
	addq	$32, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	movl	$-1, %r12d
	jmp	.L11
.L33:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1373:
	.size	ts_find_cert, .-ts_find_cert
	.p2align 4
	.type	ts_find_cert_v2, @function
ts_find_cert_v2:
.LFB1374:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-132(%rbp), %rax
	movq	%rax, -152(%rbp)
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L52:
	movq	(%rax), %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	movq	%rax, %rsi
.L37:
	leaq	-128(%rbp), %r15
	movq	-152(%rbp), %rcx
	movq	%r14, %rdi
	movq	%r15, %rdx
	call	X509_digest@PLT
	testl	%eax, %eax
	je	.L40
	movq	8(%rbx), %rax
	movslq	(%rax), %rdx
	cmpl	-132(%rbp), %edx
	jne	.L40
	movq	8(%rax), %rdi
	movq	%r15, %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L42
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L34
	movq	%r14, %rsi
	call	ts_issuer_serial_cmp
	testl	%eax, %eax
	je	.L34
.L42:
	addl	$1, %r12d
.L35:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L40
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rbx
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L52
	call	EVP_sha256@PLT
	movq	%rax, %rsi
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L40:
	movl	$-1, %r12d
.L34:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L53
	addq	$120, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L53:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1374:
	.size	ts_find_cert_v2, .-ts_find_cert_v2
	.p2align 4
	.type	ts_check_signer_name, @function
ts_check_signer_name:
.LFB1385:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$4, (%rdi)
	movl	$-1, -60(%rbp)
	je	.L55
.L59:
	leaq	-60(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L72:
	movq	%r14, %rcx
	xorl	%edx, %edx
	movl	$85, %esi
	movq	%r13, %rdi
	call	X509_get_ext_d2i@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L57
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L62:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%r15d, %eax
	jle	.L73
	movl	%r15d, %esi
	movq	%r12, %rdi
	addl	$1, %r15d
	call	OPENSSL_sk_value@PLT
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	GENERAL_NAME_cmp@PLT
	testl	%eax, %eax
	jne	.L62
	movl	$1, %r14d
.L60:
	movq	%r12, %rdi
	call	GENERAL_NAMES_free@PLT
.L54:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L74
	addq	$24, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	movq	%r12, %rdi
	call	GENERAL_NAMES_free@PLT
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L57:
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L55:
	movq	%rsi, %rdi
	movl	$1, %r14d
	call	X509_get_subject_name@PLT
	movq	8(%rbx), %rdi
	movq	%rax, %rsi
	call	X509_NAME_cmp@PLT
	testl	%eax, %eax
	jne	.L59
	jmp	.L54
.L74:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1385:
	.size	ts_check_signer_name, .-ts_check_signer_name
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ts/ts_rsp_verify.c"
	.text
	.p2align 4
	.type	ts_compute_imprint.isra.0, @function
ts_compute_imprint.isra.0:
.LFB1390:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, -4168(%rbp)
	movq	%rdi, %r13
	movq	(%rsi), %rdi
	movq	%rdx, %r12
	movq	%rcx, %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, (%rdx)
	movq	$0, (%rcx)
	call	X509_ALGOR_dup@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L76
	movq	(%rax), %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L99
	movq	%rax, %rdi
	call	EVP_MD_size@PLT
	testl	%eax, %eax
	js	.L76
	movq	-4168(%rbp), %rcx
	movslq	%eax, %rdi
	movl	$591, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, (%rcx)
	call	CRYPTO_malloc@PLT
	movl	$592, %r8d
	movq	%rax, (%rbx)
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L98
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L100
	movq	%r15, %rsi
	movq	%rax, %rdi
	leaq	-4160(%rbp), %r15
	call	EVP_DigestInit@PLT
	testl	%eax, %eax
	jne	.L81
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L76:
	xorl	%r14d, %r14d
.L78:
	movq	%r14, %rdi
	call	EVP_MD_CTX_free@PLT
	movq	(%r12), %rdi
	call	X509_ALGOR_free@PLT
	movq	(%rbx), %rdi
	movl	$615, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-4168(%rbp), %rax
	movl	$0, (%rax)
	xorl	%eax, %eax
	movq	$0, (%rbx)
.L75:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L101
	addq	$4136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	movslq	%eax, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L78
.L81:
	movl	$4096, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	BIO_read@PLT
	testl	%eax, %eax
	jg	.L82
	movq	(%rbx), %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	EVP_DigestFinal@PLT
	testl	%eax, %eax
	je	.L78
	movq	%r14, %rdi
	call	EVP_MD_CTX_free@PLT
	movl	$1, %eax
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L99:
	movl	$584, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$126, %edx
	xorl	%r14d, %r14d
	movl	$145, %esi
	movl	$47, %edi
	call	ERR_put_error@PLT
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L100:
	movl	$598, %r8d
.L98:
	movl	$65, %edx
	movl	$145, %esi
	movl	$47, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L78
.L101:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1390:
	.size	ts_compute_imprint.isra.0, .-ts_compute_imprint.isra.0
	.p2align 4
	.type	ts_check_imprints.isra.0, @function
ts_check_imprints.isra.0:
.LFB1391:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	testq	%rdi, %rdi
	je	.L110
	movq	(%rcx), %r15
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	(%r15), %rsi
	call	OBJ_cmp@PLT
	testl	%eax, %eax
	jne	.L105
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L111
	call	ASN1_TYPE_get@PLT
	cmpl	$5, %eax
	je	.L111
	.p2align 4,,10
	.p2align 3
.L105:
	movl	$645, %r8d
	movl	$103, %edx
	movl	$100, %esi
	movl	$47, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L102:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L110
	call	ASN1_TYPE_get@PLT
	cmpl	$5, %eax
	jne	.L105
	.p2align 4,,10
	.p2align 3
.L110:
	movq	8(%r12), %rdi
	call	ASN1_STRING_length@PLT
	cmpl	%r14d, %eax
	jne	.L105
	movq	8(%r12), %rdi
	call	ASN1_STRING_get0_data@PLT
	movl	%r14d, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L105
	movl	$1, %eax
	jmp	.L102
	.cfi_endproc
.LFE1391:
	.size	ts_check_imprints.isra.0, .-ts_check_imprints.isra.0
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Verify error:"
	.text
	.p2align 4
	.globl	TS_RESP_verify_signature
	.type	TS_RESP_verify_signature, @function
TS_RESP_verify_signature:
.LFB1368:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -4184(%rbp)
	movq	%rdi, %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L164
	movq	24(%rdi), %rdi
	movq	%rsi, %r12
	movq	%rdx, %r13
	call	OBJ_obj2nid@PLT
	cmpl	$22, %eax
	jne	.L165
	movq	%rbx, %rdi
	call	PKCS7_get_signer_info@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L118
	movq	%rax, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	$1, %eax
	je	.L166
.L118:
	movl	$113, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$110, %edx
.L163:
	movl	$106, %esi
	movl	$47, %edi
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	call	ERR_put_error@PLT
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
.L115:
	movq	%r15, %rdi
	call	BIO_free_all@PLT
	movq	X509_free@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	%rbx, %rdi
	call	OPENSSL_sk_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L167
	addq	$4184, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	.cfi_restore_state
	movl	$108, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$114, %edx
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L164:
	movl	$102, %edx
	movl	$106, %esi
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	movl	$104, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$47, %edi
	xorl	%r12d, %r12d
	call	ERR_put_error@PLT
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L166:
	movq	%r14, %rdi
	xorl	%esi, %esi
	call	OPENSSL_sk_value@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$2, %esi
	movq	%rbx, %rdi
	movq	%rax, %r14
	call	PKCS7_ctrl@PLT
	movl	$118, %r8d
	movl	$106, %edx
	leaq	.LC0(%rip), %rcx
	testq	%rax, %rax
	jne	.L163
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	PKCS7_get0_signers@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L142
	movq	%rax, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	$1, %eax
	je	.L168
	movq	%r15, %rbx
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L168:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, -4192(%rbp)
	call	X509_STORE_CTX_new@PLT
	testq	%rax, %rax
	je	.L169
	movq	-4192(%rbp), %rdx
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -4200(%rbp)
	call	X509_STORE_CTX_init@PLT
	movq	-4200(%rbp), %r11
	testl	%eax, %eax
	movl	%eax, %r12d
	je	.L121
	movq	%r11, %rdi
	movl	$9, %esi
	call	X509_STORE_CTX_set_purpose@PLT
	movq	-4200(%rbp), %r11
	movq	%r11, %rdi
	call	X509_verify_cert@PLT
	movq	-4200(%rbp), %r11
	testl	%eax, %eax
	movq	%r11, %rdi
	jle	.L170
	movq	%r11, -4200(%rbp)
	call	X509_STORE_CTX_get1_chain@PLT
	movq	-4200(%rbp), %r11
	movq	%rax, %r13
	movq	%r11, %rdi
	call	X509_STORE_CTX_free@PLT
	movl	$223, %esi
	movq	%r14, %rdi
	call	PKCS7_get_signed_attribute@PLT
	testq	%rax, %rax
	je	.L171
	movq	8(%rax), %rax
	leaq	-4168(%rbp), %r8
	xorl	%edi, %edi
	movq	%r8, %rsi
	movq	%r8, -4208(%rbp)
	movq	8(%rax), %rdx
	movq	%rdx, -4168(%rbp)
	movslq	(%rax), %rdx
	call	d2i_ESS_SIGNING_CERT@PLT
	movl	$1086, %esi
	movq	%r14, %rdi
	movq	%rax, -4200(%rbp)
	call	PKCS7_get_signed_attribute@PLT
	movq	-4208(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L172
	movq	8(%rax), %rax
	xorl	%edi, %edi
	movq	%r8, %rsi
	movq	8(%rax), %rdx
	movq	%rdx, -4168(%rbp)
	movslq	(%rax), %rdx
	call	d2i_ESS_SIGNING_CERT_V2@PLT
	cmpq	$0, -4200(%rbp)
	movq	%rax, %r12
	je	.L128
.L126:
	movq	-4200(%rbp), %rax
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	(%rax), %rax
	movq	%rax, -4216(%rbp)
	call	OPENSSL_sk_value@PLT
	movq	-4216(%rbp), %rdi
	movq	%rax, %rsi
	call	ts_find_cert
	testl	%eax, %eax
	jne	.L129
	movq	-4216(%rbp), %rdi
	call	OPENSSL_sk_num@PLT
	movl	$1, %edx
	cmpl	$1, %eax
	jg	.L130
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L132:
	movl	%edx, %esi
	movq	%r13, %rdi
	movl	%edx, -4208(%rbp)
	call	OPENSSL_sk_value@PLT
	movq	-4216(%rbp), %rdi
	movq	%rax, %rsi
	call	ts_find_cert
	testl	%eax, %eax
	js	.L129
	movl	-4208(%rbp), %edx
	addl	$1, %edx
.L130:
	movq	%r13, %rdi
	movl	%edx, -4208(%rbp)
	call	OPENSSL_sk_num@PLT
	movl	-4208(%rbp), %edx
	cmpl	%eax, %edx
	jl	.L132
.L159:
	movq	-4200(%rbp), %rdi
	call	ESS_SIGNING_CERT_free@PLT
	movq	%r12, %rdi
	call	ESS_SIGNING_CERT_V2_free@PLT
.L137:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	leaq	-4160(%rbp), %r12
	call	PKCS7_dataInit@PLT
	movq	%rax, -4200(%rbp)
.L138:
	movq	-4200(%rbp), %rdi
	movl	$4096, %edx
	movq	%r12, %rsi
	call	BIO_read@PLT
	testl	%eax, %eax
	jg	.L138
	movq	-4192(%rbp), %rcx
	movq	-4200(%rbp), %rdi
	movq	%r14, %rdx
	movq	%rbx, %rsi
	call	PKCS7_signatureVerify@PLT
	testl	%eax, %eax
	jle	.L173
	movq	-4184(%rbp), %rax
	testq	%rax, %rax
	je	.L148
	movq	-4192(%rbp), %rcx
	movq	%r15, %rbx
	movl	$1, %r12d
	movq	%rcx, (%rax)
	movq	%rcx, %rdi
	call	X509_up_ref@PLT
	movq	-4200(%rbp), %r15
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L142:
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	jmp	.L115
.L121:
	movq	%r11, %rdi
	movq	%r15, %rbx
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	call	X509_STORE_CTX_free@PLT
	jmp	.L115
.L172:
	cmpq	$0, -4200(%rbp)
	jne	.L126
.L127:
	movq	$0, -4200(%rbp)
	xorl	%r12d, %r12d
.L129:
	movl	$253, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$101, %edx
	movq	%r15, %rbx
	movl	$103, %esi
	movl	$47, %edi
	xorl	%r15d, %r15d
	call	ERR_put_error@PLT
	movq	-4200(%rbp), %rdi
	call	ESS_SIGNING_CERT_free@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	ESS_SIGNING_CERT_V2_free@PLT
	jmp	.L115
.L171:
	movl	$1086, %esi
	movq	%r14, %rdi
	call	PKCS7_get_signed_attribute@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L147
	movq	8(%rax), %rax
	leaq	-4168(%rbp), %rsi
	xorl	%edi, %edi
	movq	8(%rax), %rdx
	movq	%rdx, -4168(%rbp)
	movslq	(%rax), %rdx
	call	d2i_ESS_SIGNING_CERT_V2@PLT
	movq	%rax, %r12
.L128:
	testq	%r12, %r12
	je	.L127
	movq	(%r12), %rax
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, -4208(%rbp)
	call	OPENSSL_sk_value@PLT
	movq	-4208(%rbp), %rdi
	movq	%rax, %rsi
	call	ts_find_cert_v2
	movq	$0, -4200(%rbp)
	testl	%eax, %eax
	jne	.L129
	movq	-4208(%rbp), %rdi
	call	OPENSSL_sk_num@PLT
	movl	$1, %edx
	cmpl	$1, %eax
	jg	.L134
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L136:
	movl	%edx, %esi
	movq	%r13, %rdi
	movl	%edx, -4200(%rbp)
	call	OPENSSL_sk_value@PLT
	movq	-4208(%rbp), %rdi
	movq	%rax, %rsi
	call	ts_find_cert_v2
	testl	%eax, %eax
	js	.L147
	movl	-4200(%rbp), %edx
	addl	$1, %edx
.L134:
	movq	%r13, %rdi
	movl	%edx, -4200(%rbp)
	call	OPENSSL_sk_num@PLT
	movl	-4200(%rbp), %edx
	cmpl	%eax, %edx
	jl	.L136
	movq	$0, -4200(%rbp)
	jmp	.L159
.L169:
	movl	$65, %edx
	movl	$109, %esi
	movq	%r15, %rbx
	xorl	%r13d, %r13d
	movl	$175, %r8d
	leaq	.LC0(%rip), %rcx
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	movl	$47, %edi
	call	ERR_put_error@PLT
	xorl	%edi, %edi
	call	X509_STORE_CTX_free@PLT
	jmp	.L115
.L170:
	movq	%r11, -4184(%rbp)
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	call	X509_STORE_CTX_get_error@PLT
	movl	$184, %r8d
	movl	$100, %edx
	leaq	.LC0(%rip), %rcx
	movl	%eax, %ebx
	movl	$109, %esi
	movl	$47, %edi
	call	ERR_put_error@PLT
	movslq	%ebx, %rdi
	movq	%r15, %rbx
	xorl	%r15d, %r15d
	call	X509_verify_cert_error_string@PLT
	leaq	.LC1(%rip), %rsi
	movl	$2, %edi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	ERR_add_error_data@PLT
	movq	-4184(%rbp), %r11
	movq	%r11, %rdi
	call	X509_STORE_CTX_free@PLT
	jmp	.L115
.L148:
	movq	%r15, %rbx
	movl	$1, %r12d
	movq	-4200(%rbp), %r15
	jmp	.L115
.L147:
	movq	$0, -4200(%rbp)
	jmp	.L129
.L173:
	movl	$109, %edx
	movl	$106, %esi
	movq	%r15, %rbx
	xorl	%r12d, %r12d
	movl	$143, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$47, %edi
	call	ERR_put_error@PLT
	movq	-4200(%rbp), %r15
	jmp	.L115
.L174:
	xorl	%edi, %edi
	call	ESS_SIGNING_CERT_free@PLT
	movq	%r12, %rdi
	call	ESS_SIGNING_CERT_V2_free@PLT
	jmp	.L137
.L167:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1368:
	.size	TS_RESP_verify_signature, .-TS_RESP_verify_signature
	.p2align 4
	.type	int_ts_RESP_verify_token, @function
int_ts_RESP_verify_token:
.LFB1378:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%rsi, %rdi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	64(%rdx), %r13
	movl	(%r15), %ebx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	testq	%r13, %r13
	movq	$0, -72(%rbp)
	setne	-97(%rbp)
	movzbl	-97(%rbp), %eax
	movq	$0, -64(%rbp)
	movl	$0, -84(%rbp)
	testb	$64, %bl
	je	.L193
	testb	%al, %al
	je	.L193
.L176:
	orl	$1, %ebx
.L178:
	movq	8(%r15), %rdx
	movq	16(%r15), %rsi
	leaq	-80(%rbp), %rcx
	call	TS_RESP_verify_signature
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L230
	testb	$2, %bl
	jne	.L232
.L181:
	testb	$4, %bl
	jne	.L233
.L182:
	testb	$8, %bl
	jne	.L234
.L183:
	testb	$16, %bl
	jne	.L235
.L184:
	testb	$32, %bl
	jne	.L236
.L186:
	movq	-80(%rbp), %rdi
	testb	$64, %bl
	je	.L188
	cmpb	$0, -97(%rbp)
	jne	.L237
.L188:
	andl	$128, %ebx
	movl	$1, %r12d
	jne	.L238
.L180:
	call	X509_free@PLT
	movq	-72(%rbp), %rdi
	call	X509_ALGOR_free@PLT
	movq	-64(%rbp), %rdi
	movl	$468, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L239
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L193:
	.cfi_restore_state
	testb	$-128, %bl
	jne	.L176
	testb	$1, %bl
	jne	.L178
	testb	$2, %bl
	je	.L181
.L232:
	movq	%r14, %rdi
	call	TS_TST_INFO_get_version@PLT
	cmpq	$1, %rax
	je	.L181
	movl	$47, %edi
	movl	$435, %r8d
	movl	$113, %edx
	xorl	%r12d, %r12d
	leaq	.LC0(%rip), %rcx
	movl	$149, %esi
	call	ERR_put_error@PLT
	movq	-80(%rbp), %rdi
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L234:
	movq	16(%r14), %rcx
	movl	48(%r15), %edx
	movq	40(%r15), %rsi
	movq	32(%r15), %rdi
	call	ts_check_imprints.isra.0
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L183
.L230:
	movq	-80(%rbp), %rdi
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L235:
	movq	16(%r14), %rsi
	movq	56(%r15), %rdi
	leaq	-64(%rbp), %rcx
	leaq	-72(%rbp), %rdx
	leaq	-84(%rbp), %r8
	call	ts_compute_imprint.isra.0
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L230
	movq	16(%r14), %rcx
	movl	-84(%rbp), %edx
	movq	-64(%rbp), %rsi
	movq	-72(%rbp), %rdi
	call	ts_check_imprints.isra.0
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L184
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L233:
	movq	8(%r14), %rsi
	movq	24(%r15), %rdi
	call	OBJ_cmp@PLT
	testl	%eax, %eax
	je	.L182
	movl	$47, %edi
	movl	$560, %r8d
	movl	$108, %edx
	xorl	%r12d, %r12d
	leaq	.LC0(%rip), %rcx
	movl	$102, %esi
	call	ERR_put_error@PLT
	movq	-80(%rbp), %rdi
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L237:
	movq	%rdi, %rsi
	movq	%r13, %rdi
	call	ts_check_signer_name
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L189
	movq	-80(%rbp), %rdi
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L236:
	movq	56(%r14), %rsi
	movl	$654, %r8d
	movl	$105, %edx
	leaq	.LC0(%rip), %rcx
	testq	%rsi, %rsi
	je	.L229
	movq	64(%r15), %rdi
	call	ASN1_INTEGER_cmp@PLT
	testl	%eax, %eax
	je	.L186
	movl	$660, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$104, %edx
.L229:
	movl	$47, %edi
	movl	$101, %esi
	xorl	%r12d, %r12d
	call	ERR_put_error@PLT
	movq	-80(%rbp), %rdi
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L238:
	movq	72(%r15), %r8
	movq	%rdi, %rsi
	movq	%r8, %rdi
	call	ts_check_signer_name
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L190
	movq	-80(%rbp), %rdi
	movl	$1, %r12d
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L190:
	movl	$460, %r8d
	movl	$112, %edx
	movl	$149, %esi
	movl	$47, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L189:
	movl	$47, %edi
	movl	$455, %r8d
	movl	$111, %edx
	movl	$149, %esi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-80(%rbp), %rdi
	jmp	.L180
.L239:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1378:
	.size	int_ts_RESP_verify_token, .-int_ts_RESP_verify_token
	.section	.rodata.str1.1
.LC2:
	.string	"unknown code"
.LC3:
	.string	"unspecified"
.LC4:
	.string	","
.LC5:
	.string	", failure codes: "
.LC6:
	.string	", status text: "
.LC7:
	.string	"status code: "
	.text
	.p2align 4
	.globl	TS_RESP_verify_response
	.type	TS_RESP_verify_response, @function
TS_RESP_verify_response:
.LFB1376:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %r13
	movq	8(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rsi), %r15
	movq	0(%r13), %rdi
	call	ASN1_INTEGER_get@PLT
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -320(%rbp)
	movaps	%xmm0, -304(%rbp)
	movaps	%xmm0, -288(%rbp)
	movaps	%xmm0, -272(%rbp)
	movaps	%xmm0, -256(%rbp)
	movaps	%xmm0, -240(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	cmpq	$1, %rax
	jbe	.L241
	leaq	.LC2(%rip), %rcx
	movq	%rcx, -336(%rbp)
	cmpq	$5, %rax
	ja	.L242
	leaq	ts_status_text(%rip), %rdx
	movq	(%rdx,%rax,8), %rax
	movq	%rax, -336(%rbp)
.L242:
	movq	8(%r13), %rdi
	xorl	%r15d, %r15d
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jg	.L273
	movq	16(%r13), %rdi
	leaq	-320(%rbp), %rbx
	testq	%rdi, %rdi
	je	.L251
.L277:
	leaq	ts_failure_info(%rip), %r14
	movl	$1, %r12d
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L253:
	movq	8(%r14), %rsi
	movl	$256, %edx
	movq	%rbx, %rdi
	xorl	%r12d, %r12d
	call	__strcat_chk@PLT
.L252:
	addq	$16, %r14
	leaq	128+ts_failure_info(%rip), %rax
	cmpq	%r14, %rax
	je	.L251
	movq	16(%r13), %rdi
.L254:
	movl	(%r14), %esi
	call	ASN1_BIT_STRING_get_bit@PLT
	testl	%eax, %eax
	je	.L252
	testl	%r12d, %r12d
	jne	.L253
	movl	$256, %edx
	leaq	.LC4(%rip), %rsi
	movq	%rbx, %rdi
	call	__strcat_chk@PLT
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L251:
	cmpb	$0, -320(%rbp)
	je	.L274
.L255:
	movl	$511, %r8d
	movl	$107, %edx
	movl	$104, %esi
	movl	$47, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	testq	%r15, %r15
	leaq	.LC3(%rip), %r8
	movq	-336(%rbp), %rdx
	cmovne	%r15, %r8
	subq	$8, %rsp
	xorl	%eax, %eax
	movl	$6, %edi
	pushq	%rbx
	leaq	.LC5(%rip), %r9
	leaq	.LC6(%rip), %rcx
	leaq	.LC7(%rip), %rsi
	call	ERR_add_error_data@PLT
	movl	$517, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	call	CRYPTO_free@PLT
	popq	%rax
	xorl	%eax, %eax
	popq	%rdx
.L240:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L275
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L273:
	.cfi_restore_state
	movq	8(%r13), %r14
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L246:
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	movq	%rax, %r15
	call	ASN1_STRING_length@PLT
	movl	%eax, %r8d
	movl	$1048575, %eax
	subl	%r12d, %eax
	cmpl	%eax, %r8d
	jg	.L245
	movq	%r15, %rdi
	addl	$1, %ebx
	call	ASN1_STRING_length@PLT
	leal	1(%r12,%rax), %r12d
.L244:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L246
	movslq	%r12d, %rdi
	movl	$536, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%ebx, %ebx
	call	CRYPTO_malloc@PLT
	movq	%rax, %r15
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L247
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L250:
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	movq	%rax, -328(%rbp)
	call	ASN1_STRING_length@PLT
	testl	%ebx, %ebx
	movq	-328(%rbp), %rdi
	je	.L249
	movb	$47, (%r12)
	addq	$1, %r12
.L249:
	cltq
	addl	$1, %ebx
	movq	%rax, -328(%rbp)
	call	ASN1_STRING_get0_data@PLT
	movq	-328(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	strncpy@PLT
	addq	-328(%rbp), %r12
.L247:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L250
	movb	$0, (%r12)
	movq	16(%r13), %rdi
	leaq	-320(%rbp), %rbx
	testq	%rdi, %rdi
	jne	.L277
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L274:
	movabsq	$7379538751752728181, %rax
	movl	$6579561, -312(%rbp)
	movq	%rax, -320(%rbp)
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L241:
.L248:
	endbr64
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	int_ts_RESP_verify_token
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L245:
	xorl	%eax, %eax
	jmp	.L240
.L276:
	movl	$537, %r8d
	movl	$65, %edx
	movl	$105, %esi
	movl	$47, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L240
.L275:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1376:
	.size	TS_RESP_verify_response, .-TS_RESP_verify_response
	.p2align 4
	.globl	TS_RESP_verify_token
	.type	TS_RESP_verify_token, @function
TS_RESP_verify_token:
.LFB1377:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rsi, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	call	PKCS7_to_TS_TST_INFO@PLT
	testq	%rax, %rax
	je	.L278
	movq	%rax, %r12
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%r13, %rsi
	call	int_ts_RESP_verify_token
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	TS_TST_INFO_free@PLT
.L278:
	popq	%r12
	movl	%r15d, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1377:
	.size	TS_RESP_verify_token, .-TS_RESP_verify_token
	.section	.rodata.str1.1
.LC8:
	.string	"badAlg"
.LC9:
	.string	"badRequest"
.LC10:
	.string	"badDataFormat"
.LC11:
	.string	"timeNotAvailable"
.LC12:
	.string	"unacceptedPolicy"
.LC13:
	.string	"unacceptedExtension"
.LC14:
	.string	"addInfoNotAvailable"
.LC15:
	.string	"systemFailure"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	ts_failure_info, @object
	.size	ts_failure_info, 128
ts_failure_info:
	.long	0
	.zero	4
	.quad	.LC8
	.long	2
	.zero	4
	.quad	.LC9
	.long	5
	.zero	4
	.quad	.LC10
	.long	14
	.zero	4
	.quad	.LC11
	.long	15
	.zero	4
	.quad	.LC12
	.long	16
	.zero	4
	.quad	.LC13
	.long	17
	.zero	4
	.quad	.LC14
	.long	25
	.zero	4
	.quad	.LC15
	.section	.rodata.str1.1
.LC16:
	.string	"granted"
.LC17:
	.string	"grantedWithMods"
.LC18:
	.string	"rejection"
.LC19:
	.string	"waiting"
.LC20:
	.string	"revocationWarning"
.LC21:
	.string	"revocationNotification"
	.section	.data.rel.ro.local
	.align 32
	.type	ts_status_text, @object
	.size	ts_status_text, 48
ts_status_text:
	.quad	.LC16
	.quad	.LC17
	.quad	.LC18
	.quad	.LC19
	.quad	.LC20
	.quad	.LC21
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
