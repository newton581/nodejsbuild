# 1 "../deps/openssl/openssl/crypto/rc2/rc2_ecb.c"
# 1 "<built-in>"
# 1 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 1 "<command-line>" 2
# 1 "../deps/openssl/openssl/crypto/rc2/rc2_ecb.c"
# 10 "../deps/openssl/openssl/crypto/rc2/rc2_ecb.c"
# 1 "../deps/openssl/openssl/include/openssl/rc2.h" 1
# 13 "../deps/openssl/openssl/include/openssl/rc2.h"
# 1 "../deps/openssl/openssl/include/openssl/opensslconf.h" 1
# 1 "../deps/openssl/openssl/include/../../config/opensslconf.h" 1
# 9 "../deps/openssl/openssl/include/../../config/opensslconf.h"
# 1 "../deps/openssl/openssl/include/../../config/./opensslconf_asm.h" 1
# 98 "../deps/openssl/openssl/include/../../config/./opensslconf_asm.h"
# 1 "../deps/openssl/openssl/include/../../config/././archs/linux-x86_64/asm/include/openssl/opensslconf.h" 1
# 13 "../deps/openssl/openssl/include/../../config/././archs/linux-x86_64/asm/include/openssl/opensslconf.h"
# 1 "../deps/openssl/openssl/include/openssl/opensslv.h" 1
# 14 "../deps/openssl/openssl/include/../../config/././archs/linux-x86_64/asm/include/openssl/opensslconf.h" 2
# 99 "../deps/openssl/openssl/include/../../config/./opensslconf_asm.h" 2
# 10 "../deps/openssl/openssl/include/../../config/opensslconf.h" 2
# 1 "../deps/openssl/openssl/include/openssl/opensslconf.h" 2
# 14 "../deps/openssl/openssl/include/openssl/rc2.h" 2






typedef unsigned int RC2_INT;







typedef struct rc2_key_st {
    RC2_INT data[64];
} RC2_KEY;

void RC2_set_key(RC2_KEY *key, int len, const unsigned char *data, int bits);
void RC2_ecb_encrypt(const unsigned char *in, unsigned char *out,
                     RC2_KEY *key, int enc);
void RC2_encrypt(unsigned long *data, RC2_KEY *key);
void RC2_decrypt(unsigned long *data, RC2_KEY *key);
void RC2_cbc_encrypt(const unsigned char *in, unsigned char *out, long length,
                     RC2_KEY *ks, unsigned char *iv, int enc);
void RC2_cfb64_encrypt(const unsigned char *in, unsigned char *out,
                       long length, RC2_KEY *schedule, unsigned char *ivec,
                       int *num, int enc);
void RC2_ofb64_encrypt(const unsigned char *in, unsigned char *out,
                       long length, RC2_KEY *schedule, unsigned char *ivec,
                       int *num);
# 11 "../deps/openssl/openssl/crypto/rc2/rc2_ecb.c" 2
# 1 "../deps/openssl/openssl/crypto/rc2/rc2_local.h" 1
# 12 "../deps/openssl/openssl/crypto/rc2/rc2_ecb.c" 2
# 22 "../deps/openssl/openssl/crypto/rc2/rc2_ecb.c"
void RC2_ecb_encrypt(const unsigned char *in, unsigned char *out, RC2_KEY *ks,
                     int encrypt)
{
    unsigned long l, d[2];

    (l =((unsigned long)(*((in)++))) , l|=((unsigned long)(*((in)++)))<< 8L, l|=((unsigned long)(*((in)++)))<<16L, l|=((unsigned long)(*((in)++)))<<24L);
    d[0] = l;
    (l =((unsigned long)(*((in)++))) , l|=((unsigned long)(*((in)++)))<< 8L, l|=((unsigned long)(*((in)++)))<<16L, l|=((unsigned long)(*((in)++)))<<24L);
    d[1] = l;
    if (encrypt)
        RC2_encrypt(d, ks);
    else
        RC2_decrypt(d, ks);
    l = d[0];
    (*((out)++)=(unsigned char)(((l) )&0xff), *((out)++)=(unsigned char)(((l)>> 8L)&0xff), *((out)++)=(unsigned char)(((l)>>16L)&0xff), *((out)++)=(unsigned char)(((l)>>24L)&0xff));
    l = d[1];
    (*((out)++)=(unsigned char)(((l) )&0xff), *((out)++)=(unsigned char)(((l)>> 8L)&0xff), *((out)++)=(unsigned char)(((l)>>16L)&0xff), *((out)++)=(unsigned char)(((l)>>24L)&0xff));
    l = d[0] = d[1] = 0;
}
