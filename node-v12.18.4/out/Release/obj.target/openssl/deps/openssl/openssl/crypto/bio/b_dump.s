	.file	"b_dump.c"
	.text
	.p2align 4
	.type	write_fp, @function
write_fp:
.LFB269:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rcx
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	fwrite@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE269:
	.size	write_fp, .-write_fp
	.p2align 4
	.type	write_bio, @function
write_bio:
.LFB272:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rdx, %rdi
	movl	%esi, %edx
	movq	%r8, %rsi
	jmp	BIO_write@PLT
	.cfi_endproc
.LFE272:
	.size	write_bio, .-write_bio
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	"%*s%04x - "
.LC2:
	.string	"%02x%c"
	.text
	.p2align 4
	.type	BIO_dump_indent_cb.constprop.0, @function
BIO_dump_indent_cb.constprop.0:
.LFB276:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$376, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -392(%rbp)
	movq	%rsi, -400(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	leal	15(%rcx), %eax
	cmovns	%ecx, %eax
	movl	%eax, %ecx
	andl	$-16, %eax
	sarl	$4, %ecx
	cmpl	%eax, %r14d
	setg	%al
	movzbl	%al, %eax
	addl	%ecx, %eax
	testl	%eax, %eax
	jle	.L23
	subl	$1, %eax
	leaq	16(%rdx), %r13
	movq	%rdx, -368(%rbp)
	leaq	-352(%rbp), %r15
	addq	$1, %rax
	movq	%r15, -360(%rbp)
	movl	%r14d, %r15d
	movq	%r13, %r14
	movq	$0, -376(%rbp)
	salq	$4, %rax
	movq	%rax, -408(%rbp)
	movl	$0, -380(%rbp)
	.p2align 4,,10
	.p2align 3
.L21:
	movq	-376(%rbp), %rax
	movq	-360(%rbp), %rdi
	movl	$289, %esi
	xorl	%ecx, %ecx
	leaq	.LC0(%rip), %r8
	leaq	.LC1(%rip), %rdx
	xorl	%r12d, %r12d
	movl	%eax, %r13d
	movl	%eax, %r9d
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	movl	$289, %r9d
	movslq	%eax, %rsi
	movl	%r13d, %eax
	movl	%eax, %ebx
	movl	%esi, %r13d
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L9:
	movq	-368(%rbp), %rax
	cmpq	$7, %r12
	movl	$45, %r8d
	leaq	.LC2(%rip), %rdx
	movl	$4, %esi
	movzbl	(%rax,%r12), %ecx
	movl	$32, %eax
	cmovne	%eax, %r8d
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	movl	$289, %r9d
.L10:
	addl	$3, %r13d
	movq	%r9, %rax
	movslq	%r13d, %rsi
	subq	%rsi, %rax
.L8:
	addq	$1, %r12
	cmpq	$16, %r12
	je	.L28
.L12:
	movq	%r9, %rax
	subq	%rsi, %rax
	cmpq	$3, %rax
	jbe	.L8
	movq	-360(%rbp), %rax
	leaq	(%rax,%rsi), %rdi
	leal	(%rbx,%r12), %eax
	cmpl	%eax, %r15d
	jg	.L9
	movl	$2105376, (%rdi)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L28:
	movl	%ebx, %edi
	movl	%r13d, %ebx
	movl	%edi, %r13d
	cmpq	$2, %rax
	ja	.L29
.L13:
	movq	-368(%rbp), %rax
	movl	$289, %edi
	movq	%rax, %rdx
	subl	%eax, %r13d
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L30:
	movslq	%ebx, %rsi
.L17:
	movq	%rdi, %rcx
	leal	0(%r13,%rdx), %eax
	subq	%rsi, %rcx
	cmpl	%eax, %r15d
	jle	.L14
	cmpq	$1, %rcx
	jbe	.L15
	movzbl	(%rdx), %eax
	leal	-32(%rax), %ecx
	cmpb	$95, %cl
	movl	$46, %ecx
	cmovnb	%ecx, %eax
	addl	$1, %ebx
	movq	%rdi, %rcx
	movb	%al, -352(%rbp,%rsi)
	movslq	%ebx, %rsi
	movb	$0, -352(%rbp,%rsi)
	subq	%rsi, %rcx
.L15:
	addq	$1, %rdx
	cmpq	%rdx, %r14
	jne	.L30
.L14:
	cmpq	$1, %rcx
	jbe	.L18
	leal	1(%rbx), %esi
	movslq	%ebx, %rbx
	movslq	%esi, %rsi
	movb	$10, -352(%rbp,%rbx)
	movb	$0, -352(%rbp,%rsi)
.L18:
	movq	-400(%rbp), %rdx
	movq	-360(%rbp), %rdi
	addq	$16, %r14
	movq	-392(%rbp), %rax
	call	*%rax
	addq	$16, -376(%rbp)
	addl	%eax, -380(%rbp)
	movq	-376(%rbp), %rax
	addq	$16, -368(%rbp)
	cmpq	-408(%rbp), %rax
	jne	.L21
.L5:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L31
	movl	-380(%rbp), %eax
	addq	$376, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	addq	-360(%rbp), %rsi
	movl	$8224, %eax
	addl	$2, %ebx
	movw	%ax, (%rsi)
	movb	$0, 2(%rsi)
	movslq	%ebx, %rsi
	jmp	.L13
.L23:
	movl	$0, -380(%rbp)
	jmp	.L5
.L31:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE276:
	.size	BIO_dump_indent_cb.constprop.0, .-BIO_dump_indent_cb.constprop.0
	.p2align 4
	.globl	BIO_dump_cb
	.type	BIO_dump_cb, @function
BIO_dump_cb:
.LFB267:
	.cfi_startproc
	endbr64
	jmp	BIO_dump_indent_cb.constprop.0
	.cfi_endproc
.LFE267:
	.size	BIO_dump_cb, .-BIO_dump_cb
	.p2align 4
	.globl	BIO_dump_indent_cb
	.type	BIO_dump_indent_cb, @function
BIO_dump_indent_cb:
.LFB268:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movl	$0, %edx
	pushq	%r12
	pushq	%rbx
	subq	$392, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -408(%rbp)
	movl	%ecx, %edi
	movq	%rsi, -416(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$64, %r8d
	movl	$64, %eax
	movl	%edi, -364(%rbp)
	cmovle	%r8d, %eax
	testl	%eax, %eax
	cmovns	%eax, %edx
	movl	$6, %eax
	cmpl	$6, %edx
	movl	%edx, %ebx
	movl	%edx, -392(%rbp)
	cmovle	%edx, %eax
	subl	%eax, %ebx
	movl	%ebx, %eax
	leal	6(%rbx), %ecx
	movl	%edi, %ebx
	addl	$3, %eax
	cmovns	%eax, %ecx
	movl	%edi, %eax
	cltd
	sarl	$2, %ecx
	negl	%ecx
	leal	16(%rcx), %r15d
	idivl	%r15d
	movl	%r15d, %edx
	imull	%eax, %edx
	cmpl	%edi, %edx
	setl	%dl
	movzbl	%dl, %edx
	leal	(%rax,%rdx), %edi
	movl	%edi, -396(%rbp)
	testl	%edi, %edi
	jle	.L55
	movl	$0, -368(%rbp)
	movslq	%r15d, %rax
	xorl	%r14d, %r14d
	movq	%rax, -424(%rbp)
	leal	-1(%rbx), %eax
	movl	%r14d, %r9d
	movq	%r13, %r14
	movl	%eax, -380(%rbp)
	leaq	-352(%rbp), %rax
	movq	%rax, -360(%rbp)
	leal	15(%rcx), %eax
	movl	$0, -388(%rbp)
	movl	%eax, -400(%rbp)
	.p2align 4,,10
	.p2align 3
.L51:
	movl	-392(%rbp), %ecx
	movq	-360(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC0(%rip), %r8
	leaq	.LC1(%rip), %rdx
	movl	$289, %esi
	movl	%r9d, -376(%rbp)
	call	BIO_snprintf@PLT
	testl	%r15d, %r15d
	movl	-376(%rbp), %r9d
	movl	%eax, %ebx
	je	.L36
	movl	-400(%rbp), %r11d
	xorl	%r12d, %r12d
	movslq	%eax, %rdx
	movl	%r9d, %ebx
	movq	%r14, -376(%rbp)
	movl	$289, %r13d
	movl	%r15d, -384(%rbp)
	movq	%r11, %r14
	movl	%eax, %r15d
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L38:
	movq	-376(%rbp), %rax
	cmpq	$7, %r12
	movl	$32, %r8d
	leaq	.LC2(%rip), %rdx
	movl	$4, %esi
	movzbl	(%rax,%r12), %ecx
	movl	$45, %eax
	cmove	%eax, %r8d
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
.L39:
	addl	$3, %r15d
	movq	%r13, %rcx
	movslq	%r15d, %rdx
	movq	%rdx, %rsi
	subq	%rdx, %rcx
.L37:
	leaq	1(%r12), %rax
	cmpq	%r12, %r14
	je	.L62
	movq	%rax, %r12
.L41:
	movq	%r13, %rcx
	movq	%rdx, %rsi
	subq	%rdx, %rcx
	cmpq	$3, %rcx
	jbe	.L37
	movq	-360(%rbp), %rax
	leaq	(%rax,%rdx), %rdi
	leal	(%rbx,%r12), %eax
	cmpl	-364(%rbp), %eax
	jl	.L38
	movl	$2105376, (%rdi)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L62:
	movl	%ebx, %r9d
	movq	-376(%rbp), %r14
	movl	%r15d, %ebx
	movl	-384(%rbp), %r15d
	cmpq	$2, %rcx
	ja	.L63
.L53:
	cmpl	-364(%rbp), %r9d
	jge	.L43
	movl	-380(%rbp), %r11d
	movl	$1, %edi
	movl	$289, %r8d
	addq	$2, %r11
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L47:
	addq	$1, %rdi
	cmpq	%rdi, %r11
	je	.L43
	movslq	%ebx, %rdx
.L44:
	movq	%r8, %rcx
	movq	%rdx, %rsi
	subq	%rdx, %rcx
	cmpq	$1, %rcx
	jbe	.L45
	movzbl	-1(%r14,%rdi), %eax
	movl	$46, %esi
	leal	-32(%rax), %ecx
	cmpb	$95, %cl
	movq	%r8, %rcx
	cmovnb	%esi, %eax
	addl	$1, %ebx
	movslq	%ebx, %rsi
	movb	%al, -352(%rbp,%rdx)
	subq	%rsi, %rcx
	movb	$0, -352(%rbp,%rsi)
.L45:
	cmpl	%edi, %r15d
	jg	.L47
.L43:
	movl	%r9d, -376(%rbp)
	cmpq	$1, %rcx
	jbe	.L48
	leal	1(%rbx), %esi
	movslq	%ebx, %rbx
	movq	-416(%rbp), %rdx
	movq	-360(%rbp), %rdi
	movslq	%esi, %rsi
	movb	$10, -352(%rbp,%rbx)
	movq	-408(%rbp), %rax
	movb	$0, -352(%rbp,%rsi)
	call	*%rax
	movl	-376(%rbp), %r9d
	addl	%eax, -388(%rbp)
	addl	$1, -368(%rbp)
	addq	-424(%rbp), %r14
	subl	%r15d, -380(%rbp)
	movl	-368(%rbp), %eax
	addl	%r15d, %r9d
	cmpl	-396(%rbp), %eax
	jne	.L51
.L33:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L64
	movl	-388(%rbp), %eax
	addq	$392, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	movq	-416(%rbp), %rdx
	movq	-360(%rbp), %rdi
	movq	-408(%rbp), %rax
	call	*%rax
	movl	-376(%rbp), %r9d
	addl	%eax, -388(%rbp)
	addl	$1, -368(%rbp)
	addq	-424(%rbp), %r14
	subl	%r15d, -380(%rbp)
	movl	-368(%rbp), %eax
	addl	%r15d, %r9d
	cmpl	%eax, -396(%rbp)
	jne	.L51
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L63:
	addq	-360(%rbp), %rdx
	movl	$8224, %eax
	addl	$2, %ebx
	movl	$289, %ecx
	movw	%ax, (%rdx)
	movb	$0, 2(%rdx)
	movslq	%ebx, %rdx
	movq	%rdx, %rsi
	subq	%rdx, %rcx
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L36:
	movslq	%eax, %rsi
	movl	$289, %eax
	movq	%rax, %rcx
	subq	%rsi, %rcx
	cmpq	$2, %rcx
	jbe	.L43
	addq	-360(%rbp), %rsi
	addl	$2, %ebx
	movw	$8224, (%rsi)
	movb	$0, 2(%rsi)
	movslq	%ebx, %rsi
	subq	%rsi, %rax
	movq	%rax, %rcx
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L55:
	movl	$0, -388(%rbp)
	jmp	.L33
.L64:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE268:
	.size	BIO_dump_indent_cb, .-BIO_dump_indent_cb
	.p2align 4
	.globl	BIO_dump_fp
	.type	BIO_dump_fp, @function
BIO_dump_fp:
.LFB270:
	.cfi_startproc
	endbr64
	movl	%edx, %ecx
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	write_fp(%rip), %rdi
	jmp	BIO_dump_indent_cb.constprop.0
	.cfi_endproc
.LFE270:
	.size	BIO_dump_fp, .-BIO_dump_fp
	.p2align 4
	.globl	BIO_dump_indent_fp
	.type	BIO_dump_indent_fp, @function
BIO_dump_indent_fp:
.LFB271:
	.cfi_startproc
	endbr64
	movl	%ecx, %r8d
	movl	%edx, %ecx
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	write_fp(%rip), %rdi
	jmp	BIO_dump_indent_cb
	.cfi_endproc
.LFE271:
	.size	BIO_dump_indent_fp, .-BIO_dump_indent_fp
	.p2align 4
	.globl	BIO_dump
	.type	BIO_dump, @function
BIO_dump:
.LFB273:
	.cfi_startproc
	endbr64
	movl	%edx, %ecx
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	write_bio(%rip), %rdi
	jmp	BIO_dump_indent_cb.constprop.0
	.cfi_endproc
.LFE273:
	.size	BIO_dump, .-BIO_dump
	.p2align 4
	.globl	BIO_dump_indent
	.type	BIO_dump_indent, @function
BIO_dump_indent:
.LFB274:
	.cfi_startproc
	endbr64
	movl	%ecx, %r8d
	movl	%edx, %ecx
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	write_bio(%rip), %rdi
	jmp	BIO_dump_indent_cb
	.cfi_endproc
.LFE274:
	.size	BIO_dump_indent, .-BIO_dump_indent
	.section	.rodata.str1.1
.LC3:
	.string	"%*s"
.LC4:
	.string	"%02X:"
.LC5:
	.string	"\n"
.LC6:
	.string	"%02X"
	.text
	.p2align 4
	.globl	BIO_hex_string
	.type	BIO_hex_string, @function
BIO_hex_string:
.LFB275:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, -56(%rbp)
	movl	%edx, -52(%rbp)
	movq	%rcx, -64(%rbp)
	movl	%r8d, -68(%rbp)
	testl	%r8d, %r8d
	jle	.L79
	movq	%rdi, %r15
	cmpl	$1, %r8d
	je	.L73
	leal	-2(%r8), %r13d
	movq	%rcx, %r12
	leaq	.LC4(%rip), %r14
	xorl	%ebx, %ebx
	addq	%rcx, %r13
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L74:
	cmpq	%r12, %r13
	je	.L73
.L77:
	addq	$1, %r12
.L72:
	movzbl	(%r12), %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	leal	1(%rbx), %eax
	cltd
	idivl	-52(%rbp)
	movl	%edx, %ebx
	testl	%edx, %edx
	jne	.L74
	xorl	%eax, %eax
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	call	BIO_printf@PLT
	cmpq	%r12, %r13
	je	.L84
	movl	-56(%rbp), %edx
	leaq	.LC0(%rip), %rcx
	movq	%r15, %rdi
	xorl	%eax, %eax
	leaq	.LC3(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L84:
	movl	-56(%rbp), %edx
	leaq	.LC0(%rip), %rcx
	movq	%r15, %rdi
	xorl	%eax, %eax
	leaq	.LC3(%rip), %rsi
	call	BIO_printf@PLT
.L73:
	movq	-64(%rbp), %rax
	movslq	-68(%rbp), %r8
	leaq	.LC6(%rip), %rsi
	movq	%r15, %rdi
	movzbl	-1(%rax,%r8), %edx
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L79:
	addq	$40, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE275:
	.size	BIO_hex_string, .-BIO_hex_string
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
