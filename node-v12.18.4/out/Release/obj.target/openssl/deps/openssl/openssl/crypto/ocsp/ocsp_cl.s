	.file	"ocsp_cl.c"
	.text
	.p2align 4
	.globl	OCSP_request_add0_id
	.type	OCSP_request_add0_id, @function
OCSP_request_add0_id:
.LFB1392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	OCSP_ONEREQ_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1
	movq	(%rax), %rdi
	call	OCSP_CERTID_free@PLT
	movq	%r13, (%r12)
	testq	%rbx, %rbx
	je	.L1
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L11
.L1:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movq	$0, (%r12)
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	OCSP_ONEREQ_free@PLT
	jmp	.L1
	.cfi_endproc
.LFE1392:
	.size	OCSP_request_add0_id, .-OCSP_request_add0_id
	.p2align 4
	.globl	OCSP_request_set1_name
	.type	OCSP_request_set1_name, @function
OCSP_request_set1_name:
.LFB1393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	GENERAL_NAME_new@PLT
	movq	%rax, %r12
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L12
	leaq	8(%r12), %rdi
	movq	%r13, %rsi
	call	X509_NAME_set@PLT
	testl	%eax, %eax
	je	.L18
	movl	$4, (%r12)
	movq	8(%rbx), %rdi
	call	GENERAL_NAME_free@PLT
	movq	%r12, 8(%rbx)
	movl	$1, %eax
.L12:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	%eax, -36(%rbp)
	call	GENERAL_NAME_free@PLT
	movl	-36(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1393:
	.size	OCSP_request_set1_name, .-OCSP_request_set1_name
	.p2align 4
	.globl	OCSP_request_add1_cert
	.type	OCSP_request_add1_cert, @function
OCSP_request_add1_cert:
.LFB1394:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	32(%rdi), %r13
	testq	%r13, %r13
	je	.L32
.L20:
	testq	%r12, %r12
	je	.L24
	movq	24(%r13), %rdi
	testq	%rdi, %rdi
	je	.L33
	movq	%r12, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L34
.L23:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	movq	%r12, %rdi
	call	X509_up_ref@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 24(%r13)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L23
	movq	%r12, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L23
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L32:
	movq	%rdi, %rbx
	call	OCSP_SIGNATURE_new@PLT
	movq	%rax, 32(%rbx)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L23
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L24:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1394:
	.size	OCSP_request_add1_cert, .-OCSP_request_add1_cert
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ocsp/ocsp_cl.c"
	.text
	.p2align 4
	.globl	OCSP_request_sign
	.type	OCSP_request_sign, @function
OCSP_request_sign:
.LFB1395:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$40, %rsp
	movq	%rcx, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	X509_get_subject_name@PLT
	movq	%rax, -56(%rbp)
	call	GENERAL_NAME_new@PLT
	testq	%rax, %rax
	je	.L36
	movq	-56(%rbp), %rsi
	leaq	8(%rax), %rdi
	movq	%rax, %r15
	call	X509_NAME_set@PLT
	testl	%eax, %eax
	je	.L74
	movl	$4, (%r15)
	movq	8(%rbx), %rdi
	call	GENERAL_NAME_free@PLT
	movq	%r15, 8(%rbx)
	call	OCSP_SIGNATURE_new@PLT
	movq	%rax, 32(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L38
	testq	%r14, %r14
	je	.L39
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	X509_check_private_key@PLT
	testl	%eax, %eax
	je	.L75
	movq	32(%rbx), %rsi
	subq	$8, %rsp
	xorl	%edx, %edx
	movq	%r14, %r9
	movq	%rbx, %r8
	leaq	OCSP_REQINFO_it(%rip), %rdi
	movq	16(%rsi), %rcx
	pushq	-72(%rbp)
	call	ASN1_item_sign@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	je	.L36
	testb	$1, -64(%rbp)
	je	.L42
.L54:
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	testb	$1, -64(%rbp)
	jne	.L54
.L44:
	testq	%r12, %r12
	je	.L45
	movq	24(%rdi), %r8
	testq	%r8, %r8
	jne	.L46
	movq	%rdi, -56(%rbp)
	call	OPENSSL_sk_new_null@PLT
	movq	-56(%rbp), %rdi
	movq	%rax, %r8
	movq	%rax, 24(%rdi)
	testq	%rax, %rax
	je	.L36
.L46:
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L36
	movq	%r12, %rdi
	call	X509_up_ref@PLT
.L45:
	xorl	%r12d, %r12d
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L52:
	movq	%r14, %rdi
	call	X509_up_ref@PLT
.L50:
	addl	$1, %r12d
.L48:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L54
	movq	%r13, %rdi
	movl	%r12d, %esi
	call	OPENSSL_sk_value@PLT
	movq	32(%rbx), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L76
.L49:
	testq	%r14, %r14
	je	.L50
	movq	24(%rdi), %r8
	testq	%r8, %r8
	je	.L77
.L51:
	movq	%r14, %rsi
	movq	%r8, %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L52
.L36:
	movq	32(%rbx), %rdi
.L38:
	call	OCSP_SIGNATURE_free@PLT
	movq	$0, 32(%rbx)
	xorl	%eax, %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	movq	%r15, %rdi
	call	GENERAL_NAME_free@PLT
	movq	32(%rbx), %rdi
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L75:
	movl	$39, %edi
	movl	$112, %r8d
	movl	$110, %edx
	movl	$110, %esi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	32(%rbx), %rdi
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L77:
	movq	%rdi, -56(%rbp)
	call	OPENSSL_sk_new_null@PLT
	movq	-56(%rbp), %rdi
	movq	%rax, %r8
	movq	%rax, 24(%rdi)
	testq	%rax, %rax
	jne	.L51
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L76:
	call	OCSP_SIGNATURE_new@PLT
	movq	%rax, 32(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L38
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L42:
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L44
	call	OCSP_SIGNATURE_new@PLT
	movq	%rax, 32(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L38
	jmp	.L44
	.cfi_endproc
.LFE1395:
	.size	OCSP_request_sign, .-OCSP_request_sign
	.p2align 4
	.globl	OCSP_response_status
	.type	OCSP_response_status, @function
OCSP_response_status:
.LFB1396:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ASN1_ENUMERATED_get@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1396:
	.size	OCSP_response_status, .-OCSP_response_status
	.p2align 4
	.globl	OCSP_response_get1_basic
	.type	OCSP_response_get1_basic, @function
OCSP_response_get1_basic:
.LFB1397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	8(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L85
	movq	(%rbx), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$365, %eax
	jne	.L86
	movq	8(%rbx), %rdi
	addq	$8, %rsp
	leaq	OCSP_BASICRESP_it(%rip), %rsi
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ASN1_item_unpack@PLT
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	movl	$158, %r8d
	movl	$104, %edx
	movl	$111, %esi
	movl	$39, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L80:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	movl	$154, %r8d
	movl	$108, %edx
	movl	$111, %esi
	movl	$39, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L80
	.cfi_endproc
.LFE1397:
	.size	OCSP_response_get1_basic, .-OCSP_response_get1_basic
	.p2align 4
	.globl	OCSP_resp_get0_signature
	.type	OCSP_resp_get0_signature, @function
OCSP_resp_get0_signature:
.LFB1398:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	ret
	.cfi_endproc
.LFE1398:
	.size	OCSP_resp_get0_signature, .-OCSP_resp_get0_signature
	.p2align 4
	.globl	OCSP_resp_get0_tbs_sigalg
	.type	OCSP_resp_get0_tbs_sigalg, @function
OCSP_resp_get0_tbs_sigalg:
.LFB1399:
	.cfi_startproc
	endbr64
	leaq	48(%rdi), %rax
	ret
	.cfi_endproc
.LFE1399:
	.size	OCSP_resp_get0_tbs_sigalg, .-OCSP_resp_get0_tbs_sigalg
	.p2align 4
	.globl	OCSP_resp_get0_respdata
	.type	OCSP_resp_get0_respdata, @function
OCSP_resp_get0_respdata:
.LFB1400:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE1400:
	.size	OCSP_resp_get0_respdata, .-OCSP_resp_get0_respdata
	.p2align 4
	.globl	OCSP_resp_count
	.type	OCSP_resp_count, @function
OCSP_resp_count:
.LFB1401:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L91
	movq	32(%rdi), %rdi
	jmp	OPENSSL_sk_num@PLT
.L91:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE1401:
	.size	OCSP_resp_count, .-OCSP_resp_count
	.p2align 4
	.globl	OCSP_resp_get0
	.type	OCSP_resp_get0, @function
OCSP_resp_get0:
.LFB1402:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L93
	movq	32(%rdi), %rdi
	jmp	OPENSSL_sk_value@PLT
	.p2align 4,,10
	.p2align 3
.L93:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1402:
	.size	OCSP_resp_get0, .-OCSP_resp_get0
	.p2align 4
	.globl	OCSP_resp_get0_produced_at
	.type	OCSP_resp_get0_produced_at, @function
OCSP_resp_get0_produced_at:
.LFB1403:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE1403:
	.size	OCSP_resp_get0_produced_at, .-OCSP_resp_get0_produced_at
	.p2align 4
	.globl	OCSP_resp_get0_certs
	.type	OCSP_resp_get0_certs, @function
OCSP_resp_get0_certs:
.LFB1404:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rax
	ret
	.cfi_endproc
.LFE1404:
	.size	OCSP_resp_get0_certs, .-OCSP_resp_get0_certs
	.p2align 4
	.globl	OCSP_resp_get0_id
	.type	OCSP_resp_get0_id, @function
OCSP_resp_get0_id:
.LFB1405:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jne	.L97
	movq	16(%rdi), %rax
	movq	%rax, (%rdx)
	movl	$1, %eax
	movq	$0, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	cmpl	$1, %eax
	jne	.L99
	movq	16(%rdi), %rcx
	movq	%rcx, (%rsi)
	movq	$0, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1405:
	.size	OCSP_resp_get0_id, .-OCSP_resp_get0_id
	.p2align 4
	.globl	OCSP_resp_get1_id
	.type	OCSP_resp_get1_id, @function
OCSP_resp_get1_id:
.LFB1406:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	8(%rdi), %eax
	movq	%rdx, %rbx
	testl	%eax, %eax
	je	.L109
	xorl	%r8d, %r8d
	cmpl	$1, %eax
	je	.L110
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	movq	16(%rdi), %rdi
	call	ASN1_OCTET_STRING_dup@PLT
	movq	%rax, (%r12)
	movq	$0, (%rbx)
.L102:
	xorl	%r8d, %r8d
	testq	%rax, %rax
	popq	%rbx
	popq	%r12
	setne	%r8b
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	movq	16(%rdi), %rdi
	call	X509_NAME_dup@PLT
	movq	%rax, (%rbx)
	movq	$0, (%r12)
	testq	%rax, %rax
	je	.L102
	movl	$1, %r8d
	popq	%rbx
	popq	%r12
	movl	%r8d, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1406:
	.size	OCSP_resp_get1_id, .-OCSP_resp_get1_id
	.p2align 4
	.globl	OCSP_resp_find
	.type	OCSP_resp_find, @function
OCSP_resp_find:
.LFB1407:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testq	%rdi, %rdi
	je	.L117
	leal	1(%rdx), %r12d
	testl	%edx, %edx
	movl	$0, %eax
	movq	32(%rdi), %rbx
	cmovs	%eax, %r12d
	movq	%rsi, %r13
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L116:
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	OCSP_id_cmp@PLT
	testl	%eax, %eax
	je	.L111
	addl	$1, %r12d
.L115:
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jl	.L116
.L117:
	movl	$-1, %r12d
.L111:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1407:
	.size	OCSP_resp_find, .-OCSP_resp_find
	.p2align 4
	.globl	OCSP_single_get0_status
	.type	OCSP_single_get0_status, @function
OCSP_single_get0_status:
.LFB1408:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 13, -24
	.cfi_offset 3, -32
	testq	%rdi, %rdi
	je	.L131
	movq	8(%rdi), %rax
	movq	%rdi, %rbx
	movl	(%rax), %r13d
	cmpl	$1, %r13d
	je	.L145
.L126:
	testq	%rcx, %rcx
	je	.L130
	movq	16(%rbx), %rax
	movq	%rax, (%rcx)
.L130:
	testq	%r8, %r8
	je	.L123
	movq	24(%rbx), %rax
	movq	%rax, (%r8)
.L123:
	addq	$32, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	movq	8(%rax), %rax
	testq	%rdx, %rdx
	je	.L127
	movq	(%rax), %rdi
	movq	%rdi, (%rdx)
.L127:
	testq	%rsi, %rsi
	je	.L126
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L129
	movq	%r8, -40(%rbp)
	movq	%rcx, -32(%rbp)
	movq	%rsi, -24(%rbp)
	call	ASN1_ENUMERATED_get@PLT
	movq	-24(%rbp), %rsi
	movq	-32(%rbp), %rcx
	movq	-40(%rbp), %r8
	movl	%eax, (%rsi)
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L129:
	movl	$-1, (%rsi)
	jmp	.L126
.L131:
	movl	$-1, %r13d
	jmp	.L123
	.cfi_endproc
.LFE1408:
	.size	OCSP_single_get0_status, .-OCSP_single_get0_status
	.p2align 4
	.globl	OCSP_resp_find_status
	.type	OCSP_resp_find_status, @function
OCSP_resp_find_status:
.LFB1409:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -64(%rbp)
	movq	%r8, -72(%rbp)
	movq	%r9, -56(%rbp)
	testq	%rdi, %rdi
	je	.L152
	movq	32(%rdi), %r13
	movq	%rdi, %rbx
	movq	%rsi, %r14
	movq	%rdx, %r12
	xorl	%r15d, %r15d
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L151:
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r14, %rdi
	movq	(%rax), %rsi
	call	OCSP_id_cmp@PLT
	testl	%eax, %eax
	je	.L150
	addl	$1, %r15d
.L149:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r15d
	jl	.L151
.L152:
	xorl	%eax, %eax
.L146:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	.cfi_restore_state
	movq	32(%rbx), %rdi
	movl	%r15d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L178
	movq	8(%rbx), %rax
	movl	(%rax), %r14d
	cmpl	$1, %r14d
	je	.L179
.L154:
	movq	-56(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L158
	movq	16(%rbx), %rax
	movq	%rax, (%rcx)
.L158:
	cmpq	$0, 16(%rbp)
	je	.L159
	movq	24(%rbx), %rax
	movq	16(%rbp), %rdx
	movq	%rax, (%rdx)
.L159:
	movl	$1, %eax
	testq	%r12, %r12
	je	.L146
	movl	%r14d, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	.cfi_restore_state
	movq	-72(%rbp), %rsi
	movq	8(%rax), %rax
	testq	%rsi, %rsi
	je	.L155
	movq	(%rax), %rdx
	movq	%rdx, (%rsi)
.L155:
	movq	-64(%rbp), %r15
	testq	%r15, %r15
	je	.L154
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L157
	call	ASN1_ENUMERATED_get@PLT
	movl	%eax, (%r15)
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L157:
	movl	$-1, (%r15)
	jmp	.L154
.L178:
	orl	$-1, %r14d
	jmp	.L159
	.cfi_endproc
.LFE1409:
	.size	OCSP_resp_find_status, .-OCSP_resp_find_status
	.p2align 4
	.globl	OCSP_check_validity
	.type	OCSP_check_validity, @function
OCSP_check_validity:
.LFB1410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-72(%rbp), %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	time@PLT
	movq	%r14, %rdi
	call	ASN1_GENERALIZEDTIME_check@PLT
	testl	%eax, %eax
	jne	.L181
	movl	$346, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$123, %edx
	movl	%eax, %r12d
	movl	$115, %esi
	movl	$39, %edi
	call	ERR_put_error@PLT
.L182:
	testq	%r13, %r13
	je	.L180
	movq	%r13, %rdi
	call	ASN1_GENERALIZEDTIME_check@PLT
	testl	%eax, %eax
	je	.L193
	movq	-72(%rbp), %rax
	leaq	-64(%rbp), %rsi
	movq	%r13, %rdi
	subq	%rbx, %rax
	movq	%rax, -64(%rbp)
	call	X509_cmp_time@PLT
	testl	%eax, %eax
	js	.L194
.L186:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	ASN1_STRING_cmp@PLT
	testl	%eax, %eax
	js	.L195
.L180:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L196
	addq	$56, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	.cfi_restore_state
	movq	-72(%rbp), %rax
	leaq	-64(%rbp), %r9
	movq	%r14, %rdi
	movl	$1, %r12d
	movq	%r9, %rsi
	movq	%r9, -88(%rbp)
	addq	%rbx, %rax
	movq	%rax, -64(%rbp)
	call	X509_cmp_time@PLT
	movq	-88(%rbp), %r9
	testl	%eax, %eax
	jg	.L197
	testq	%r15, %r15
	js	.L182
.L198:
	movq	-72(%rbp), %rax
	movq	%r9, %rsi
	movq	%r14, %rdi
	subq	%r15, %rax
	movq	%rax, -64(%rbp)
	call	X509_cmp_time@PLT
	testl	%eax, %eax
	jns	.L182
	movl	$362, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$127, %edx
	xorl	%r12d, %r12d
	movl	$115, %esi
	movl	$39, %edi
	call	ERR_put_error@PLT
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L193:
	movl	$115, %esi
	movl	$39, %edi
	xorl	%r12d, %r12d
	movl	$373, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$122, %edx
	call	ERR_put_error@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	ASN1_STRING_cmp@PLT
	testl	%eax, %eax
	jns	.L180
.L195:
	movl	$385, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$124, %edx
	xorl	%r12d, %r12d
	movl	$115, %esi
	movl	$39, %edi
	call	ERR_put_error@PLT
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L197:
	movl	$351, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$126, %edx
	xorl	%r12d, %r12d
	movl	$115, %esi
	movl	$39, %edi
	call	ERR_put_error@PLT
	movq	-88(%rbp), %r9
	testq	%r15, %r15
	js	.L182
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L194:
	movl	$378, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$125, %edx
	xorl	%r12d, %r12d
	movl	$115, %esi
	movl	$39, %edi
	call	ERR_put_error@PLT
	jmp	.L186
.L196:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1410:
	.size	OCSP_check_validity, .-OCSP_check_validity
	.p2align 4
	.globl	OCSP_SINGLERESP_get0_id
	.type	OCSP_SINGLERESP_get0_id, @function
OCSP_SINGLERESP_get0_id:
.LFB1411:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE1411:
	.size	OCSP_SINGLERESP_get0_id, .-OCSP_SINGLERESP_get0_id
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
