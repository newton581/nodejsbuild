	.file	"rmd_dgst.c"
	.text
	.p2align 4
	.globl	RIPEMD160_Init
	.type	RIPEMD160_Init, @function
RIPEMD160_Init:
.LFB154:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1, %eax
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm0, 64(%rdi)
	movups	%xmm0, 80(%rdi)
	movdqa	.LC0(%rip), %xmm0
	movl	$-1009589776, 16(%rdi)
	movups	%xmm0, (%rdi)
	ret
	.cfi_endproc
.LFE154:
	.size	RIPEMD160_Init, .-RIPEMD160_Init
	.p2align 4
	.globl	ripemd160_block_data_order
	.type	ripemd160_block_data_order, @function
ripemd160_block_data_order:
.LFB155:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-1(%rdx), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -144(%rbp)
	movq	%rax, -104(%rbp)
	testq	%rdx, %rdx
	je	.L3
	movl	(%rdi), %eax
	movq	%rsi, %r14
	movl	%eax, -108(%rbp)
	movl	4(%rdi), %eax
	movl	%eax, -88(%rbp)
	movl	8(%rdi), %eax
	movl	%eax, -96(%rbp)
	movl	12(%rdi), %eax
	movl	%eax, -84(%rbp)
	movl	16(%rdi), %eax
	movl	%eax, -92(%rbp)
	.p2align 4,,10
	.p2align 3
.L5:
	movl	-88(%rbp), %ecx
	movl	-96(%rbp), %ebx
	movl	(%r14), %eax
	movl	-84(%rbp), %edx
	movl	%ecx, %esi
	movl	-92(%rbp), %r11d
	movl	4(%r14), %r10d
	movl	%ecx, %r13d
	xorl	%ebx, %esi
	movl	%eax, -48(%rbp)
	addl	-108(%rbp), %eax
	roll	$10, %r13d
	xorl	%edx, %esi
	movl	8(%r14), %edi
	movl	12(%r14), %r9d
	addl	%eax, %esi
	movl	%ebx, %eax
	roll	$10, %eax
	roll	$11, %esi
	movl	%edi, -52(%rbp)
	movl	%eax, %r8d
	addl	%r11d, %esi
	movl	%eax, %ebx
	movl	%r9d, -56(%rbp)
	xorl	%ecx, %r8d
	leal	(%r10,%r11), %eax
	movl	%edx, %r11d
	movl	%ebx, -120(%rbp)
	xorl	%esi, %r8d
	addl	%eax, %r8d
	movl	%ebx, %eax
	roll	$14, %r8d
	addl	%r9d, %eax
	addl	%edx, %r8d
	movl	%esi, %edx
	roll	$10, %esi
	xorl	%r13d, %edx
	movl	%r8d, %r12d
	movl	%edx, %ecx
	movl	%edi, %edx
	xorl	%esi, %r12d
	addl	%r11d, %edx
	xorl	%r8d, %ecx
	roll	$10, %r8d
	movl	16(%r14), %r11d
	addl	%edx, %ecx
	movl	%r12d, %edx
	movl	20(%r14), %r12d
	roll	$15, %ecx
	addl	%ebx, %ecx
	movl	%r12d, -60(%rbp)
	xorl	%ecx, %edx
	movl	%ecx, %ebx
	addl	%eax, %edx
	xorl	%r8d, %ebx
	leal	0(%r13,%r11), %eax
	roll	$12, %edx
	movl	%ebx, %edi
	movl	24(%r14), %ebx
	addl	%r13d, %edx
	xorl	%edx, %edi
	movl	%edx, %r9d
	movl	%ebx, -64(%rbp)
	addl	%eax, %edi
	roll	$5, %edi
	addl	%esi, %edi
	roll	$10, %ecx
	addl	%r12d, %esi
	xorl	%ecx, %r9d
	roll	$10, %edx
	movl	%r9d, %eax
	xorl	%edi, %eax
	addl	%esi, %eax
	movl	28(%r14), %esi
	roll	$8, %eax
	movl	%esi, %r9d
	movl	%edi, %esi
	addl	%r8d, %eax
	addl	%ebx, %r8d
	xorl	%edx, %esi
	roll	$10, %edi
	movl	%eax, %r15d
	movl	%r9d, -68(%rbp)
	xorl	%eax, %esi
	xorl	%edi, %r15d
	roll	$10, %eax
	movl	40(%r14), %ebx
	addl	%r8d, %esi
	movl	32(%r14), %r8d
	roll	$7, %esi
	addl	%ecx, %esi
	movl	%r8d, %r12d
	movl	%r15d, %r8d
	addl	%r9d, %ecx
	xorl	%esi, %r8d
	leal	(%rdx,%r12), %r9d
	movl	%r12d, -72(%rbp)
	movl	48(%r14), %r12d
	addl	%ecx, %r8d
	movl	36(%r14), %ecx
	roll	$9, %r8d
	addl	%edx, %r8d
	movl	%esi, %edx
	movl	%ecx, %r15d
	roll	$10, %esi
	xorl	%eax, %edx
	movl	%r15d, -76(%rbp)
	movl	%edx, %ecx
	xorl	%r8d, %ecx
	leal	(%rcx,%r9), %edx
	movl	%r8d, %r9d
	xorl	%esi, %r9d
	roll	$11, %edx
	addl	%edi, %edx
	movl	%r9d, %ecx
	addl	%r15d, %edi
	movl	44(%r14), %r9d
	xorl	%edx, %ecx
	addl	%edi, %ecx
	movl	%edx, %edi
	movl	%r9d, %r15d
	roll	$13, %ecx
	leal	(%rax,%rbx), %r9d
	movl	%r15d, -80(%rbp)
	addl	%eax, %ecx
	roll	$10, %r8d
	addq	$64, %r14
	xorl	%r8d, %edi
	roll	$10, %edx
	xorl	%ecx, %edi
	leal	(%rdi,%r9), %eax
	movl	%ecx, %edi
	roll	$10, %ecx
	movl	-12(%r14), %r9d
	roll	$14, %eax
	xorl	%edx, %edi
	addl	%esi, %eax
	addl	%r15d, %esi
	xorl	%eax, %edi
	addl	%esi, %edi
	movl	%eax, %esi
	roll	$10, %eax
	roll	$15, %edi
	xorl	%ecx, %esi
	addl	%r8d, %edi
	addl	%r12d, %r8d
	xorl	%edi, %esi
	addl	%r8d, %esi
	movl	-8(%r14), %r8d
	roll	$6, %esi
	movl	%r8d, -44(%rbp)
	movl	%edi, %r8d
	addl	%edx, %esi
	addl	%r9d, %edx
	xorl	%eax, %r8d
	roll	$10, %edi
	movl	%r8d, %r15d
	xorl	%esi, %r15d
	addl	%edx, %r15d
	movl	%esi, %edx
	roll	$7, %r15d
	xorl	%edi, %edx
	addl	%ecx, %r15d
	addl	-44(%rbp), %ecx
	xorl	%r15d, %edx
	addl	%edx, %ecx
	roll	$10, %esi
	movl	%r15d, %edx
	roll	$9, %ecx
	xorl	%esi, %edx
	roll	$10, %r15d
	addl	%eax, %ecx
	addl	-4(%r14), %eax
	xorl	%ecx, %edx
	addl	%eax, %edx
	movl	-68(%rbp), %eax
	roll	$8, %edx
	addl	%edi, %edx
	leal	1518500249(%rax,%rdi), %eax
	movl	%ecx, %edi
	roll	$10, %ecx
	xorl	%r15d, %edi
	andl	%edx, %edi
	xorl	%r15d, %edi
	addl	%eax, %edi
	leal	1518500249(%r11,%rsi), %eax
	roll	$7, %edi
	addl	%esi, %edi
	movl	%edx, %esi
	roll	$10, %edx
	xorl	%ecx, %esi
	movl	%edi, %r8d
	andl	%edi, %esi
	xorl	%edx, %r8d
	roll	$10, %edi
	xorl	%ecx, %esi
	addl	%eax, %esi
	movl	%r8d, %eax
	roll	$6, %esi
	addl	%r15d, %esi
	leal	1518500249(%r9,%r15), %r15d
	andl	%esi, %eax
	movl	%esi, %r8d
	xorl	%edx, %eax
	addl	%r15d, %eax
	leal	1518500249(%r10,%rcx), %r15d
	roll	$8, %eax
	addl	%ecx, %eax
	xorl	%edi, %r8d
	roll	$10, %esi
	movl	%r8d, %ecx
	movl	-4(%r14), %r8d
	andl	%eax, %ecx
	xorl	%edi, %ecx
	addl	%r15d, %ecx
	leal	1518500249(%rbx,%rdx), %r15d
	roll	$13, %ecx
	addl	%edx, %ecx
	movl	%eax, %edx
	roll	$10, %eax
	xorl	%esi, %edx
	andl	%ecx, %edx
	xorl	%esi, %edx
	addl	%r15d, %edx
	movl	-64(%rbp), %r15d
	roll	$11, %edx
	addl	%edi, %edx
	leal	1518500249(%r15,%rdi), %r15d
	movl	%ecx, %edi
	roll	$10, %ecx
	xorl	%eax, %edi
	andl	%edx, %edi
	xorl	%eax, %edi
	addl	%r15d, %edi
	leal	1518500249(%r8,%rsi), %r15d
	roll	$9, %edi
	addl	%esi, %edi
	movl	%edx, %esi
	roll	$10, %edx
	xorl	%ecx, %esi
	movl	%edi, %r8d
	andl	%edi, %esi
	xorl	%edx, %r8d
	xorl	%ecx, %esi
	addl	%r15d, %esi
	movl	-56(%rbp), %r15d
	roll	$7, %esi
	addl	%eax, %esi
	leal	1518500249(%r15,%rax), %r15d
	movl	%r8d, %eax
	andl	%esi, %eax
	movl	%esi, %r8d
	xorl	%edx, %eax
	addl	%r15d, %eax
	leal	1518500249(%r12,%rcx), %r15d
	roll	$15, %eax
	roll	$10, %edi
	xorl	%edi, %r8d
	addl	%ecx, %eax
	roll	$10, %esi
	movl	%r8d, %ecx
	andl	%eax, %ecx
	xorl	%edi, %ecx
	addl	%r15d, %ecx
	movl	-48(%rbp), %r15d
	roll	$7, %ecx
	addl	%edx, %ecx
	leal	1518500249(%r15,%rdx), %r15d
	movl	%eax, %edx
	roll	$10, %eax
	xorl	%esi, %edx
	andl	%ecx, %edx
	xorl	%esi, %edx
	addl	%r15d, %edx
	movl	-76(%rbp), %r15d
	roll	$12, %edx
	addl	%edi, %edx
	leal	1518500249(%r15,%rdi), %r15d
	movl	%ecx, %edi
	roll	$10, %ecx
	xorl	%eax, %edi
	andl	%edx, %edi
	xorl	%eax, %edi
	addl	%r15d, %edi
	movl	-60(%rbp), %r15d
	roll	$15, %edi
	addl	%esi, %edi
	leal	1518500249(%r15,%rsi), %r15d
	movl	%edx, %esi
	roll	$10, %edx
	xorl	%ecx, %esi
	movl	%edi, %r8d
	andl	%edi, %esi
	xorl	%edx, %r8d
	xorl	%ecx, %esi
	addl	%r15d, %esi
	movl	-52(%rbp), %r15d
	roll	$9, %esi
	addl	%eax, %esi
	leal	1518500249(%r15,%rax), %r15d
	movl	%r8d, %eax
	movl	-44(%rbp), %r8d
	andl	%esi, %eax
	roll	$10, %edi
	xorl	%edx, %eax
	addl	%r15d, %eax
	leal	1518500249(%r8,%rcx), %r15d
	roll	$11, %eax
	addl	%ecx, %eax
	movl	%esi, %ecx
	roll	$10, %esi
	xorl	%edi, %ecx
	andl	%eax, %ecx
	xorl	%edi, %ecx
	addl	%r15d, %ecx
	movl	-80(%rbp), %r15d
	roll	$7, %ecx
	addl	%edx, %ecx
	leal	1518500249(%r15,%rdx), %r15d
	movl	%eax, %edx
	roll	$10, %eax
	xorl	%esi, %edx
	andl	%ecx, %edx
	xorl	%esi, %edx
	addl	%r15d, %edx
	movl	-72(%rbp), %r15d
	roll	$13, %edx
	addl	%edi, %edx
	leal	1518500249(%r15,%rdi), %r15d
	movl	%ecx, %edi
	roll	$10, %ecx
	xorl	%eax, %edi
	andl	%edx, %edi
	xorl	%eax, %edi
	addl	%r15d, %edi
	movl	-56(%rbp), %r15d
	roll	$12, %edi
	addl	%esi, %edi
	leal	1859775393(%r15,%rsi), %r15d
	movl	%edx, %esi
	notl	%esi
	movl	%edi, %r8d
	orl	%edi, %esi
	notl	%r8d
	xorl	%ecx, %esi
	addl	%r15d, %esi
	leal	1859775393(%rbx,%rax), %r15d
	roll	$11, %esi
	addl	%eax, %esi
	movl	%r8d, %eax
	roll	$10, %edx
	movl	-44(%rbp), %r8d
	orl	%esi, %eax
	roll	$10, %edi
	xorl	%edx, %eax
	addl	%r15d, %eax
	leal	1859775393(%r8,%rcx), %r15d
	movl	-4(%r14), %r8d
	roll	$13, %eax
	addl	%ecx, %eax
	movl	%esi, %ecx
	roll	$10, %esi
	notl	%ecx
	orl	%eax, %ecx
	xorl	%edi, %ecx
	addl	%r15d, %ecx
	leal	1859775393(%r11,%rdx), %r15d
	roll	$6, %ecx
	addl	%edx, %ecx
	movl	%eax, %edx
	roll	$10, %eax
	notl	%edx
	orl	%ecx, %edx
	xorl	%esi, %edx
	addl	%r15d, %edx
	movl	-76(%rbp), %r15d
	roll	$7, %edx
	addl	%edi, %edx
	leal	1859775393(%r15,%rdi), %r15d
	movl	%ecx, %edi
	roll	$10, %ecx
	notl	%edi
	orl	%edx, %edi
	xorl	%eax, %edi
	addl	%r15d, %edi
	leal	1859775393(%r8,%rsi), %r15d
	roll	$14, %edi
	addl	%esi, %edi
	movl	%edx, %esi
	roll	$10, %edx
	notl	%esi
	movl	%edi, %r8d
	orl	%edi, %esi
	notl	%r8d
	xorl	%ecx, %esi
	addl	%r15d, %esi
	movl	-72(%rbp), %r15d
	roll	$9, %esi
	addl	%eax, %esi
	leal	1859775393(%r15,%rax), %r15d
	movl	%r8d, %eax
	orl	%esi, %eax
	xorl	%edx, %eax
	roll	$10, %edi
	addl	%r15d, %eax
	leal	1859775393(%r10,%rcx), %r15d
	roll	$13, %eax
	addl	%ecx, %eax
	movl	%esi, %ecx
	roll	$10, %esi
	notl	%ecx
	orl	%eax, %ecx
	xorl	%edi, %ecx
	addl	%r15d, %ecx
	movl	-52(%rbp), %r15d
	roll	$15, %ecx
	addl	%edx, %ecx
	leal	1859775393(%r15,%rdx), %r15d
	movl	%eax, %edx
	roll	$10, %eax
	notl	%edx
	orl	%ecx, %edx
	xorl	%esi, %edx
	addl	%r15d, %edx
	movl	-68(%rbp), %r15d
	roll	$14, %edx
	addl	%edi, %edx
	leal	1859775393(%r15,%rdi), %r15d
	movl	%ecx, %edi
	roll	$10, %ecx
	notl	%edi
	orl	%edx, %edi
	xorl	%eax, %edi
	addl	%r15d, %edi
	movl	-48(%rbp), %r15d
	roll	$8, %edi
	addl	%esi, %edi
	leal	1859775393(%r15,%rsi), %r15d
	movl	%edx, %esi
	roll	$10, %edx
	notl	%esi
	orl	%edi, %esi
	xorl	%ecx, %esi
	addl	%r15d, %esi
	movl	-64(%rbp), %r15d
	roll	$13, %esi
	addl	%eax, %esi
	leal	1859775393(%r15,%rax), %r15d
	movl	%edi, %eax
	notl	%eax
	orl	%esi, %eax
	xorl	%edx, %eax
	addl	%r15d, %eax
	leal	1859775393(%r9,%rcx), %r15d
	roll	$6, %eax
	roll	$10, %edi
	addl	%ecx, %eax
	movl	%esi, %ecx
	roll	$10, %esi
	notl	%ecx
	orl	%eax, %ecx
	xorl	%edi, %ecx
	addl	%r15d, %ecx
	movl	-80(%rbp), %r15d
	roll	$5, %ecx
	addl	%edx, %ecx
	leal	1859775393(%r15,%rdx), %r15d
	movl	%eax, %edx
	roll	$10, %eax
	notl	%edx
	orl	%ecx, %edx
	xorl	%esi, %edx
	addl	%r15d, %edx
	movl	-60(%rbp), %r15d
	roll	$12, %edx
	addl	%edi, %edx
	leal	1859775393(%r15,%rdi), %r15d
	movl	%ecx, %edi
	roll	$10, %ecx
	notl	%edi
	orl	%edx, %edi
	xorl	%eax, %edi
	addl	%r15d, %edi
	leal	1859775393(%r12,%rsi), %r15d
	roll	$7, %edi
	addl	%esi, %edi
	movl	%edx, %esi
	roll	$10, %edx
	notl	%esi
	movl	%edi, %r8d
	orl	%edi, %esi
	xorl	%ecx, %esi
	addl	%r15d, %esi
	leal	-1894007588(%r10,%rax), %r15d
	roll	$5, %esi
	addl	%eax, %esi
	xorl	%esi, %r8d
	movl	%r8d, %eax
	movl	-4(%r14), %r8d
	andl	%edx, %eax
	xorl	%edi, %eax
	addl	%r15d, %eax
	movl	-76(%rbp), %r15d
	roll	$11, %eax
	addl	%ecx, %eax
	leal	-1894007588(%r15,%rcx), %r15d
	movl	%esi, %ecx
	roll	$10, %edi
	xorl	%eax, %ecx
	andl	%edi, %ecx
	xorl	%esi, %ecx
	roll	$10, %esi
	addl	%r15d, %ecx
	movl	-80(%rbp), %r15d
	roll	$12, %ecx
	addl	%edx, %ecx
	leal	-1894007588(%r15,%rdx), %r15d
	movl	%eax, %edx
	xorl	%ecx, %edx
	andl	%esi, %edx
	xorl	%eax, %edx
	roll	$10, %eax
	addl	%r15d, %edx
	leal	-1894007588(%rbx,%rdi), %r15d
	roll	$14, %edx
	addl	%edi, %edx
	movl	%ecx, %edi
	xorl	%edx, %edi
	andl	%eax, %edi
	xorl	%ecx, %edi
	roll	$10, %ecx
	addl	%r15d, %edi
	movl	-48(%rbp), %r15d
	roll	$15, %edi
	addl	%esi, %edi
	leal	-1894007588(%r15,%rsi), %r15d
	movl	%edx, %esi
	xorl	%edi, %esi
	andl	%ecx, %esi
	xorl	%edx, %esi
	roll	$10, %edx
	addl	%r15d, %esi
	movl	-72(%rbp), %r15d
	roll	$14, %esi
	addl	%eax, %esi
	leal	-1894007588(%r15,%rax), %r15d
	movl	%edi, %eax
	xorl	%esi, %eax
	andl	%edx, %eax
	xorl	%edi, %eax
	roll	$10, %edi
	addl	%r15d, %eax
	leal	-1894007588(%r12,%rcx), %r15d
	roll	$15, %eax
	addl	%ecx, %eax
	movl	%esi, %ecx
	xorl	%eax, %ecx
	andl	%edi, %ecx
	xorl	%esi, %ecx
	roll	$10, %esi
	addl	%r15d, %ecx
	leal	-1894007588(%r11,%rdx), %r15d
	roll	$9, %ecx
	addl	%edx, %ecx
	movl	%eax, %edx
	xorl	%ecx, %edx
	andl	%esi, %edx
	xorl	%eax, %edx
	roll	$10, %eax
	addl	%r15d, %edx
	leal	-1894007588(%r9,%rdi), %r15d
	roll	$8, %edx
	addl	%edi, %edx
	movl	%ecx, %edi
	xorl	%edx, %edi
	andl	%eax, %edi
	xorl	%ecx, %edi
	roll	$10, %ecx
	addl	%r15d, %edi
	movl	-56(%rbp), %r15d
	roll	$9, %edi
	addl	%esi, %edi
	leal	-1894007588(%r15,%rsi), %r15d
	movl	%edx, %esi
	xorl	%edi, %esi
	andl	%ecx, %esi
	xorl	%edx, %esi
	addl	%r15d, %esi
	movl	-68(%rbp), %r15d
	roll	$14, %esi
	addl	%eax, %esi
	leal	-1894007588(%r15,%rax), %r15d
	movl	%edi, %eax
	roll	$10, %edx
	xorl	%esi, %eax
	andl	%edx, %eax
	xorl	%edi, %eax
	roll	$10, %edi
	addl	%r15d, %eax
	leal	-1894007588(%r8,%rcx), %r15d
	movl	-44(%rbp), %r8d
	roll	$5, %eax
	addl	%ecx, %eax
	movl	%esi, %ecx
	xorl	%eax, %ecx
	andl	%edi, %ecx
	xorl	%esi, %ecx
	roll	$10, %esi
	addl	%r15d, %ecx
	leal	-1894007588(%r8,%rdx), %r15d
	roll	$6, %ecx
	addl	%edx, %ecx
	movl	%eax, %edx
	xorl	%ecx, %edx
	movl	%ecx, %r8d
	andl	%esi, %edx
	xorl	%eax, %edx
	roll	$10, %eax
	addl	%r15d, %edx
	movl	%eax, %r15d
	movl	-60(%rbp), %eax
	roll	$8, %edx
	addl	%edi, %edx
	leal	-1894007588(%rax,%rdi), %edi
	xorl	%edx, %r8d
	movl	%r8d, %eax
	andl	%r15d, %eax
	xorl	%ecx, %eax
	roll	$10, %ecx
	addl	%edi, %eax
	movl	-64(%rbp), %edi
	roll	$6, %eax
	addl	%esi, %eax
	leal	-1894007588(%rdi,%rsi), %edi
	movl	%edx, %esi
	xorl	%eax, %esi
	movl	%eax, %r8d
	andl	%ecx, %esi
	xorl	%edx, %esi
	addl	%edi, %esi
	movl	-52(%rbp), %edi
	roll	$10, %edx
	roll	$5, %esi
	addl	%r15d, %esi
	leal	-1894007588(%rdi,%r15), %r15d
	xorl	%esi, %r8d
	movl	%r8d, %edi
	andl	%edx, %edi
	xorl	%eax, %edi
	roll	$10, %eax
	addl	%r15d, %edi
	leal	-1454113458(%r11,%rcx), %r15d
	roll	$12, %edi
	addl	%ecx, %edi
	movl	%eax, %ecx
	notl	%ecx
	orl	%esi, %ecx
	roll	$10, %esi
	xorl	%edi, %ecx
	addl	%r15d, %ecx
	movl	-48(%rbp), %r15d
	roll	$9, %ecx
	addl	%edx, %ecx
	leal	-1454113458(%r15,%rdx), %r15d
	movl	%esi, %edx
	notl	%edx
	orl	%edi, %edx
	roll	$10, %edi
	xorl	%ecx, %edx
	addl	%r15d, %edx
	movl	-60(%rbp), %r15d
	roll	$15, %edx
	addl	%eax, %edx
	leal	-1454113458(%r15,%rax), %r15d
	movl	%edi, %eax
	notl	%eax
	orl	%ecx, %eax
	roll	$10, %ecx
	xorl	%edx, %eax
	addl	%r15d, %eax
	movl	-76(%rbp), %r15d
	roll	$5, %eax
	addl	%esi, %eax
	leal	-1454113458(%r15,%rsi), %r15d
	movl	%ecx, %esi
	notl	%esi
	orl	%edx, %esi
	xorl	%eax, %esi
	addl	%r15d, %esi
	roll	$11, %esi
	roll	$10, %edx
	movl	%edx, %r15d
	movl	-68(%rbp), %edx
	addl	%edi, %esi
	movl	%r15d, %r8d
	notl	%r8d
	leal	-1454113458(%rdx,%rdi), %edi
	movl	%r8d, %edx
	orl	%eax, %edx
	roll	$10, %eax
	xorl	%esi, %edx
	addl	%edi, %edx
	movl	%eax, %edi
	leal	-1454113458(%r12,%rcx), %eax
	roll	$6, %edx
	addl	%ecx, %edx
	movl	%edi, %ecx
	notl	%ecx
	orl	%esi, %ecx
	roll	$10, %esi
	xorl	%edx, %ecx
	movl	%esi, %r8d
	addl	%eax, %ecx
	movl	-52(%rbp), %eax
	notl	%r8d
	roll	$8, %ecx
	addl	%r15d, %ecx
	leal	-1454113458(%rax,%r15), %r15d
	movl	%r8d, %eax
	movl	%edx, %r8d
	orl	%edx, %eax
	roll	$10, %r8d
	leal	-1454113458(%rbx,%rdi), %edx
	xorl	%ecx, %eax
	addl	%r15d, %eax
	movl	%r8d, %r15d
	notl	%r8d
	roll	$13, %eax
	addl	%edi, %eax
	movl	%r8d, %edi
	movl	-44(%rbp), %r8d
	orl	%ecx, %edi
	roll	$10, %ecx
	xorl	%eax, %edi
	addl	%edx, %edi
	roll	$12, %edi
	addl	%esi, %edi
	leal	-1454113458(%r8,%rsi), %esi
	movl	%ecx, %r8d
	notl	%r8d
	movl	%r8d, %edx
	orl	%eax, %edx
	xorl	%edi, %edx
	addl	%esi, %edx
	movl	%eax, %esi
	leal	-1454113458(%r10,%r15), %eax
	roll	$5, %edx
	addl	%r15d, %edx
	roll	$10, %esi
	movl	%esi, %r8d
	notl	%r8d
	movl	%r8d, %r15d
	orl	%edi, %r15d
	roll	$10, %edi
	xorl	%edx, %r15d
	movl	%edi, %r8d
	addl	%eax, %r15d
	movl	-56(%rbp), %eax
	notl	%r8d
	roll	$12, %r15d
	addl	%ecx, %r15d
	leal	-1454113458(%rax,%rcx), %ecx
	movl	%r8d, %eax
	orl	%edx, %eax
	roll	$10, %edx
	xorl	%r15d, %eax
	addl	%ecx, %eax
	movl	-72(%rbp), %ecx
	roll	$13, %eax
	addl	%esi, %eax
	leal	-1454113458(%rcx,%rsi), %ecx
	movl	%edx, %esi
	notl	%esi
	orl	%r15d, %esi
	roll	$10, %r15d
	xorl	%eax, %esi
	addl	%ecx, %esi
	movl	-80(%rbp), %ecx
	roll	$14, %esi
	addl	%edi, %esi
	leal	-1454113458(%rcx,%rdi), %edi
	movl	%r15d, %ecx
	notl	%ecx
	orl	%eax, %ecx
	roll	$10, %eax
	xorl	%esi, %ecx
	movl	%eax, %r8d
	addl	%edi, %ecx
	movl	-64(%rbp), %edi
	notl	%r8d
	roll	$11, %ecx
	addl	%edx, %ecx
	leal	-1454113458(%rdi,%rdx), %edi
	movl	%r8d, %edx
	orl	%esi, %edx
	xorl	%ecx, %edx
	addl	%edi, %edx
	roll	$8, %edx
	leal	(%rdx,%r15), %r8d
	movl	%r8d, %edi
	movl	%esi, %r8d
	movl	-4(%r14), %esi
	roll	$10, %r8d
	movl	%edi, -124(%rbp)
	movl	%r8d, %edx
	leal	-1454113458(%rsi,%r15), %esi
	movl	%r8d, -128(%rbp)
	notl	%edx
	orl	%ecx, %edx
	xorl	%edi, %edx
	movl	-120(%rbp), %edi
	addl	%esi, %edx
	roll	$5, %edx
	leal	(%rdx,%rax), %esi
	movl	-60(%rbp), %edx
	movl	%esi, -112(%rbp)
	movl	%ecx, %esi
	movl	%edi, %ecx
	roll	$10, %esi
	notl	%ecx
	orl	-88(%rbp), %ecx
	movl	%esi, %r15d
	leal	-1454113458(%r9,%rax), %esi
	movl	%esi, -132(%rbp)
	movl	%r8d, %esi
	movl	-108(%rbp), %r8d
	addl	%r8d, %esi
	leal	1352829926(%rdx,%r8), %eax
	movl	-92(%rbp), %r8d
	movl	-84(%rbp), %edx
	movl	%esi, -116(%rbp)
	movl	-84(%rbp), %esi
	notl	%esi
	orl	-96(%rbp), %esi
	xorl	-88(%rbp), %esi
	addl	%eax, %esi
	movl	-44(%rbp), %eax
	roll	$8, %esi
	addl	%r8d, %esi
	leal	1352829926(%r8,%rax), %eax
	movl	%r13d, %r8d
	xorl	%esi, %ecx
	notl	%r8d
	addl	%eax, %ecx
	movl	%r8d, %eax
	movl	%edx, %r8d
	roll	$9, %ecx
	orl	%esi, %eax
	roll	$10, %esi
	addl	%edx, %ecx
	movl	-68(%rbp), %edx
	xorl	%ecx, %eax
	leal	1352829926(%r8,%rdx), %edx
	movl	%esi, %r8d
	addl	%edx, %eax
	movl	-48(%rbp), %edx
	notl	%r8d
	roll	$9, %eax
	addl	%edi, %eax
	leal	1352829926(%rdx,%rdi), %edi
	movl	%r8d, %edx
	orl	%ecx, %edx
	roll	$10, %ecx
	xorl	%eax, %edx
	addl	%edi, %edx
	movl	-76(%rbp), %edi
	roll	$11, %edx
	addl	%r13d, %edx
	leal	1352829926(%r13,%rdi), %r13d
	movl	%ecx, %edi
	notl	%edi
	movl	%edx, %r8d
	orl	%eax, %edi
	xorl	%edx, %edi
	addl	%r13d, %edi
	movl	%eax, %r13d
	movl	-52(%rbp), %eax
	roll	$10, %r13d
	roll	$13, %edi
	addl	%esi, %edi
	leal	1352829926(%rax,%rsi), %esi
	movl	%r13d, %eax
	notl	%eax
	orl	%edx, %eax
	movl	-80(%rbp), %edx
	roll	$10, %r8d
	xorl	%edi, %eax
	addl	%esi, %eax
	leal	1352829926(%rdx,%rcx), %edx
	movl	%r8d, %esi
	roll	$15, %eax
	addl	%ecx, %eax
	movl	%r8d, %ecx
	notl	%ecx
	orl	%edi, %ecx
	roll	$10, %edi
	xorl	%eax, %ecx
	movl	%edi, %r8d
	addl	%edx, %ecx
	notl	%r8d
	roll	$15, %ecx
	movl	%r8d, %edx
	addl	%r13d, %ecx
	orl	%eax, %edx
	leal	1352829926(%r11,%r13), %r13d
	xorl	%ecx, %edx
	addl	%r13d, %edx
	movl	%eax, %r13d
	leal	1352829926(%r9,%rsi), %eax
	roll	$10, %r13d
	roll	$5, %edx
	movl	%r13d, %r8d
	addl	%esi, %edx
	notl	%r8d
	movl	%r8d, %esi
	orl	%ecx, %esi
	roll	$10, %ecx
	xorl	%edx, %esi
	movl	%ecx, %r8d
	addl	%eax, %esi
	movl	-64(%rbp), %eax
	notl	%r8d
	roll	$7, %esi
	addl	%edi, %esi
	leal	1352829926(%rax,%rdi), %edi
	movl	%r8d, %eax
	movl	-4(%r14), %r8d
	orl	%edx, %eax
	xorl	%esi, %eax
	addl	%edi, %eax
	movl	%edx, %edi
	leal	1352829926(%r8,%r13), %edx
	roll	$10, %edi
	roll	$7, %eax
	addl	%r13d, %eax
	movl	%edi, %r13d
	notl	%r13d
	orl	%esi, %r13d
	xorl	%eax, %r13d
	addl	%edx, %r13d
	roll	$10, %esi
	movl	-72(%rbp), %edx
	movl	%esi, %r8d
	roll	$8, %r13d
	notl	%r8d
	addl	%ecx, %r13d
	leal	1352829926(%rdx,%rcx), %ecx
	movl	%r8d, %edx
	orl	%eax, %edx
	xorl	%r13d, %edx
	addl	%ecx, %edx
	movl	%eax, %ecx
	leal	1352829926(%r10,%rdi), %eax
	roll	$10, %ecx
	roll	$11, %edx
	addl	%edi, %edx
	movl	%ecx, %edi
	notl	%edi
	orl	%r13d, %edi
	roll	$10, %r13d
	xorl	%edx, %edi
	movl	%r13d, %r8d
	addl	%eax, %edi
	notl	%r8d
	roll	$14, %edi
	movl	%r8d, %eax
	movl	%edx, %r8d
	addl	%esi, %edi
	orl	%edx, %eax
	leal	1352829926(%rbx,%rsi), %esi
	movl	-56(%rbp), %edx
	xorl	%edi, %eax
	roll	$10, %r8d
	addl	%esi, %eax
	leal	1352829926(%rdx,%rcx), %edx
	movl	%r8d, %esi
	roll	$14, %eax
	addl	%ecx, %eax
	movl	%r8d, %ecx
	notl	%ecx
	orl	%edi, %ecx
	roll	$10, %edi
	xorl	%eax, %ecx
	movl	%edi, %r8d
	addl	%edx, %ecx
	notl	%r8d
	roll	$12, %ecx
	movl	%r8d, %edx
	addl	%r13d, %ecx
	orl	%eax, %edx
	leal	1352829926(%r12,%r13), %r13d
	xorl	%ecx, %edx
	movl	%ecx, %r8d
	addl	%r13d, %edx
	movl	%eax, %r13d
	movl	-64(%rbp), %eax
	roll	$6, %edx
	addl	%esi, %edx
	leal	1548603684(%rax,%rsi), %eax
	roll	$10, %r13d
	xorl	%edx, %r8d
	movl	%r8d, %esi
	movl	%edx, %r8d
	andl	%r13d, %esi
	xorl	%ecx, %esi
	roll	$10, %ecx
	addl	%eax, %esi
	movl	-80(%rbp), %eax
	roll	$9, %esi
	addl	%edi, %esi
	leal	1548603684(%rax,%rdi), %edi
	xorl	%esi, %r8d
	movl	%r8d, %eax
	andl	%ecx, %eax
	xorl	%edx, %eax
	addl	%edi, %eax
	movl	%edx, %edi
	movl	-56(%rbp), %edx
	roll	$13, %eax
	roll	$10, %edi
	addl	%r13d, %eax
	leal	1548603684(%rdx,%r13), %edx
	movl	%esi, %r13d
	xorl	%eax, %r13d
	movl	%eax, %r8d
	andl	%edi, %r13d
	xorl	%esi, %r13d
	roll	$10, %esi
	addl	%edx, %r13d
	movl	-68(%rbp), %edx
	roll	$15, %r13d
	addl	%ecx, %r13d
	leal	1548603684(%rdx,%rcx), %ecx
	xorl	%r13d, %r8d
	movl	%r8d, %edx
	andl	%esi, %edx
	xorl	%eax, %edx
	addl	%ecx, %edx
	movl	%eax, %ecx
	movl	-48(%rbp), %eax
	roll	$7, %edx
	roll	$10, %ecx
	addl	%edi, %edx
	leal	1548603684(%rax,%rdi), %eax
	movl	%r13d, %edi
	xorl	%edx, %edi
	movl	%edx, %r8d
	andl	%ecx, %edi
	xorl	%r13d, %edi
	roll	$10, %r13d
	addl	%eax, %edi
	roll	$12, %edi
	addl	%esi, %edi
	leal	1548603684(%r9,%rsi), %esi
	xorl	%edi, %r8d
	movl	%r8d, %eax
	movl	%edx, %r8d
	andl	%r13d, %eax
	roll	$10, %r8d
	xorl	%edx, %eax
	movl	-60(%rbp), %edx
	addl	%esi, %eax
	movl	%r8d, %esi
	roll	$8, %eax
	leal	1548603684(%rdx,%rcx), %edx
	addl	%ecx, %eax
	movl	%edi, %ecx
	xorl	%eax, %ecx
	andl	%r8d, %ecx
	movl	%eax, %r8d
	xorl	%edi, %ecx
	roll	$10, %edi
	addl	%edx, %ecx
	roll	$9, %ecx
	addl	%r13d, %ecx
	leal	1548603684(%rbx,%r13), %r13d
	xorl	%ecx, %r8d
	movl	%r8d, %edx
	movl	-44(%rbp), %r8d
	andl	%edi, %edx
	xorl	%eax, %edx
	addl	%r13d, %edx
	movl	%eax, %r13d
	leal	1548603684(%r8,%rsi), %eax
	movl	%ecx, %r8d
	roll	$11, %edx
	roll	$10, %r13d
	addl	%esi, %edx
	xorl	%edx, %r8d
	movl	%r8d, %esi
	movl	-4(%r14), %r8d
	andl	%r13d, %esi
	xorl	%ecx, %esi
	addl	%eax, %esi
	roll	$7, %esi
	addl	%edi, %esi
	leal	1548603684(%r8,%rdi), %edi
	movl	%edx, %r8d
	roll	$10, %ecx
	xorl	%esi, %r8d
	movl	%r8d, %eax
	andl	%ecx, %eax
	xorl	%edx, %eax
	addl	%edi, %eax
	movl	%edx, %edi
	movl	-72(%rbp), %edx
	roll	$7, %eax
	roll	$10, %edi
	addl	%r13d, %eax
	leal	1548603684(%rdx,%r13), %edx
	movl	%esi, %r13d
	xorl	%eax, %r13d
	movl	%eax, %r8d
	andl	%edi, %r13d
	xorl	%esi, %r13d
	roll	$10, %esi
	addl	%edx, %r13d
	roll	$12, %r13d
	addl	%ecx, %r13d
	leal	1548603684(%r12,%rcx), %ecx
	xorl	%r13d, %r8d
	movl	%r8d, %edx
	andl	%esi, %edx
	xorl	%eax, %edx
	addl	%ecx, %edx
	movl	%eax, %ecx
	leal	1548603684(%r11,%rdi), %eax
	roll	$7, %edx
	roll	$10, %ecx
	addl	%edi, %edx
	movl	%r13d, %edi
	xorl	%edx, %edi
	movl	%edx, %r8d
	andl	%ecx, %edi
	xorl	%r13d, %edi
	roll	$10, %r13d
	addl	%eax, %edi
	movl	-76(%rbp), %eax
	roll	$6, %edi
	addl	%esi, %edi
	leal	1548603684(%rax,%rsi), %esi
	xorl	%edi, %r8d
	movl	%r8d, %eax
	movl	%edi, %r8d
	andl	%r13d, %eax
	xorl	%edx, %eax
	addl	%esi, %eax
	roll	$10, %edx
	roll	$15, %eax
	addl	%ecx, %eax
	leal	1548603684(%r10,%rcx), %ecx
	xorl	%eax, %r8d
	movl	%r8d, %esi
	andl	%edx, %esi
	xorl	%edi, %esi
	roll	$10, %edi
	addl	%ecx, %esi
	movl	-52(%rbp), %ecx
	roll	$13, %esi
	addl	%r13d, %esi
	leal	1548603684(%rcx,%r13), %ecx
	movl	%eax, %r13d
	xorl	%esi, %r13d
	andl	%edi, %r13d
	xorl	%eax, %r13d
	roll	$10, %eax
	addl	%ecx, %r13d
	movl	-4(%r14), %ecx
	roll	$11, %r13d
	addl	%edx, %r13d
	leal	1836072691(%rcx,%rdx), %edx
	movl	%esi, %ecx
	roll	$10, %esi
	notl	%ecx
	orl	%r13d, %ecx
	xorl	%eax, %ecx
	addl	%edx, %ecx
	movl	-60(%rbp), %edx
	roll	$9, %ecx
	addl	%edi, %ecx
	leal	1836072691(%rdx,%rdi), %edx
	movl	%r13d, %edi
	roll	$10, %r13d
	notl	%edi
	movl	%ecx, %r8d
	orl	%ecx, %edi
	notl	%r8d
	xorl	%esi, %edi
	addl	%edx, %edi
	movl	%r8d, %edx
	roll	$7, %edi
	addl	%eax, %edi
	leal	1836072691(%r10,%rax), %eax
	orl	%edi, %edx
	movl	%edi, %r8d
	xorl	%r13d, %edx
	notl	%r8d
	addl	%eax, %edx
	movl	-56(%rbp), %eax
	roll	$10, %ecx
	roll	$15, %edx
	roll	$10, %edi
	addl	%esi, %edx
	leal	1836072691(%rax,%rsi), %eax
	movl	%r8d, %esi
	orl	%edx, %esi
	movl	%edx, %r8d
	xorl	%ecx, %esi
	notl	%r8d
	addl	%eax, %esi
	movl	-68(%rbp), %eax
	roll	$11, %esi
	addl	%r13d, %esi
	leal	1836072691(%rax,%r13), %r13d
	movl	%r8d, %eax
	movl	-44(%rbp), %r8d
	orl	%esi, %eax
	xorl	%edi, %eax
	addl	%r13d, %eax
	movl	%edx, %r13d
	leal	1836072691(%r8,%rcx), %edx
	roll	$8, %eax
	roll	$10, %r13d
	addl	%ecx, %eax
	movl	%esi, %ecx
	roll	$10, %esi
	notl	%ecx
	movl	%eax, %r8d
	orl	%eax, %ecx
	notl	%r8d
	xorl	%r13d, %ecx
	addl	%edx, %ecx
	movl	-64(%rbp), %edx
	roll	$6, %ecx
	addl	%edi, %ecx
	leal	1836072691(%rdx,%rdi), %edi
	movl	%r8d, %edx
	orl	%ecx, %edx
	xorl	%esi, %edx
	addl	%edi, %edx
	movl	%eax, %edi
	movl	-76(%rbp), %eax
	roll	$6, %edx
	roll	$10, %edi
	addl	%r13d, %edx
	leal	1836072691(%rax,%r13), %eax
	movl	%ecx, %r13d
	notl	%r13d
	movl	%edx, %r8d
	orl	%edx, %r13d
	notl	%r8d
	xorl	%edi, %r13d
	addl	%eax, %r13d
	movl	-80(%rbp), %eax
	roll	$14, %r13d
	addl	%esi, %r13d
	leal	1836072691(%rax,%rsi), %esi
	movl	%r8d, %eax
	roll	$10, %ecx
	orl	%r13d, %eax
	movl	%edx, %r8d
	movl	-72(%rbp), %edx
	xorl	%ecx, %eax
	roll	$10, %r8d
	addl	%esi, %eax
	leal	1836072691(%rdx,%rdi), %edx
	movl	%r8d, %esi
	roll	$12, %eax
	addl	%edi, %eax
	movl	%r13d, %edi
	roll	$10, %r13d
	notl	%edi
	orl	%eax, %edi
	xorl	%r8d, %edi
	movl	%eax, %r8d
	addl	%edx, %edi
	notl	%r8d
	roll	$13, %edi
	movl	%r8d, %edx
	addl	%ecx, %edi
	leal	1836072691(%r12,%rcx), %ecx
	orl	%edi, %edx
	movl	%edi, %r8d
	roll	$10, %edi
	xorl	%r13d, %edx
	notl	%r8d
	addl	%ecx, %edx
	movl	%eax, %ecx
	movl	-52(%rbp), %eax
	roll	$5, %edx
	roll	$10, %ecx
	addl	%esi, %edx
	leal	1836072691(%rax,%rsi), %eax
	movl	%r8d, %esi
	orl	%edx, %esi
	movl	%edx, %r8d
	xorl	%ecx, %esi
	notl	%r8d
	addl	%eax, %esi
	movl	%r8d, %eax
	roll	$14, %esi
	addl	%r13d, %esi
	leal	1836072691(%rbx,%r13), %r13d
	orl	%esi, %eax
	xorl	%edi, %eax
	addl	%r13d, %eax
	movl	%edx, %r13d
	movl	-48(%rbp), %edx
	roll	$13, %eax
	roll	$10, %r13d
	addl	%ecx, %eax
	leal	1836072691(%rdx,%rcx), %edx
	movl	%esi, %ecx
	notl	%ecx
	movl	%eax, %r8d
	orl	%eax, %ecx
	notl	%r8d
	roll	$10, %esi
	xorl	%r13d, %ecx
	addl	%edx, %ecx
	movl	%r8d, %edx
	roll	$13, %ecx
	addl	%edi, %ecx
	leal	1836072691(%r11,%rdi), %edi
	orl	%ecx, %edx
	xorl	%esi, %edx
	addl	%edi, %edx
	movl	%eax, %edi
	leal	1836072691(%r9,%r13), %eax
	roll	$7, %edx
	roll	$10, %edi
	addl	%r13d, %edx
	movl	%ecx, %r13d
	roll	$10, %ecx
	notl	%r13d
	movl	%edx, %r8d
	orl	%edx, %r13d
	xorl	%ecx, %r8d
	xorl	%edi, %r13d
	addl	%eax, %r13d
	movl	-72(%rbp), %eax
	roll	$5, %r13d
	addl	%esi, %r13d
	leal	2053994217(%rax,%rsi), %esi
	movl	%r8d, %eax
	movl	%edx, %r8d
	andl	%r13d, %eax
	movl	-64(%rbp), %edx
	roll	$10, %r8d
	xorl	%ecx, %eax
	addl	%esi, %eax
	leal	2053994217(%rdx,%rdi), %edx
	movl	%r8d, %esi
	roll	$15, %eax
	addl	%edi, %eax
	movl	%r13d, %edi
	roll	$10, %r13d
	xorl	%r8d, %edi
	andl	%eax, %edi
	xorl	%r8d, %edi
	movl	%eax, %r8d
	addl	%edx, %edi
	roll	$5, %edi
	addl	%ecx, %edi
	xorl	%r13d, %r8d
	leal	2053994217(%r11,%rcx), %ecx
	movl	%r8d, %edx
	movl	%edi, %r8d
	andl	%edi, %edx
	roll	$10, %edi
	xorl	%r13d, %edx
	addl	%ecx, %edx
	movl	%eax, %ecx
	leal	2053994217(%r10,%rsi), %eax
	roll	$10, %ecx
	roll	$8, %edx
	xorl	%ecx, %r8d
	addl	%esi, %edx
	movl	%r8d, %esi
	movl	%edx, %r8d
	andl	%edx, %esi
	xorl	%edi, %r8d
	xorl	%ecx, %esi
	addl	%eax, %esi
	movl	-56(%rbp), %eax
	roll	$11, %esi
	addl	%r13d, %esi
	leal	2053994217(%rax,%r13), %r13d
	movl	%r8d, %eax
	movl	-4(%r14), %r8d
	andl	%esi, %eax
	xorl	%edi, %eax
	addl	%r13d, %eax
	movl	%edx, %r13d
	movl	-80(%rbp), %edx
	roll	$14, %eax
	roll	$10, %r13d
	addl	%ecx, %eax
	leal	2053994217(%rdx,%rcx), %edx
	movl	%esi, %ecx
	roll	$10, %esi
	xorl	%r13d, %ecx
	andl	%eax, %ecx
	xorl	%r13d, %ecx
	addl	%edx, %ecx
	roll	$14, %ecx
	addl	%edi, %ecx
	leal	2053994217(%r8,%rdi), %edi
	movl	%eax, %r8d
	xorl	%esi, %r8d
	movl	%r8d, %edx
	andl	%ecx, %edx
	xorl	%esi, %edx
	addl	%edi, %edx
	movl	%eax, %edi
	movl	-48(%rbp), %eax
	roll	$6, %edx
	roll	$10, %edi
	addl	%r13d, %edx
	leal	2053994217(%rax,%r13), %eax
	movl	%ecx, %r13d
	roll	$10, %ecx
	xorl	%edi, %r13d
	movl	%edx, %r8d
	andl	%edx, %r13d
	xorl	%ecx, %r8d
	xorl	%edi, %r13d
	addl	%eax, %r13d
	movl	-60(%rbp), %eax
	roll	$14, %r13d
	addl	%esi, %r13d
	leal	2053994217(%rax,%rsi), %esi
	movl	%r8d, %eax
	movl	%edx, %r8d
	andl	%r13d, %eax
	roll	$10, %r8d
	leal	2053994217(%r12,%rdi), %edx
	xorl	%ecx, %eax
	addl	%esi, %eax
	movl	%r8d, %esi
	roll	$6, %eax
	addl	%edi, %eax
	movl	%r13d, %edi
	roll	$10, %r13d
	xorl	%r8d, %edi
	andl	%eax, %edi
	xorl	%r8d, %edi
	movl	%eax, %r8d
	addl	%edx, %edi
	movl	-52(%rbp), %edx
	xorl	%r13d, %r8d
	roll	$9, %edi
	addl	%ecx, %edi
	leal	2053994217(%rdx,%rcx), %ecx
	movl	%r8d, %edx
	andl	%edi, %edx
	movl	%edi, %r8d
	xorl	%r13d, %edx
	addl	%ecx, %edx
	movl	%eax, %ecx
	leal	2053994217(%r9,%rsi), %eax
	roll	$10, %ecx
	roll	$12, %edx
	xorl	%ecx, %r8d
	addl	%esi, %edx
	movl	%r8d, %esi
	movl	%edx, %r8d
	andl	%edx, %esi
	roll	$10, %edi
	xorl	%ecx, %esi
	xorl	%edi, %r8d
	roll	$10, %edx
	addl	%eax, %esi
	movl	-76(%rbp), %eax
	roll	$9, %esi
	addl	%r13d, %esi
	leal	2053994217(%rax,%r13), %r13d
	movl	%r8d, %eax
	movl	-44(%rbp), %r8d
	andl	%esi, %eax
	xorl	%edi, %eax
	addl	%r13d, %eax
	movl	-68(%rbp), %r13d
	roll	$12, %eax
	addl	%ecx, %eax
	leal	2053994217(%r13,%rcx), %r13d
	movl	%esi, %ecx
	roll	$10, %esi
	xorl	%edx, %ecx
	andl	%eax, %ecx
	xorl	%edx, %ecx
	addl	%r13d, %ecx
	leal	2053994217(%rbx,%rdi), %r13d
	roll	$5, %ecx
	addl	%edi, %ecx
	movl	%eax, %edi
	roll	$10, %eax
	xorl	%esi, %edi
	andl	%ecx, %edi
	xorl	%esi, %edi
	addl	%r13d, %edi
	leal	2053994217(%r8,%rdx), %r13d
	movl	%ecx, %r8d
	xorl	%eax, %r8d
	roll	$15, %edi
	addl	%edx, %edi
	movl	%r8d, %edx
	andl	%edi, %edx
	xorl	%eax, %edx
	addl	%r13d, %edx
	movl	%edi, %r13d
	roll	$8, %edx
	addl	%esi, %edx
	roll	$10, %ecx
	addl	%r12d, %esi
	xorl	%ecx, %r13d
	roll	$10, %edi
	xorl	%edx, %r13d
	addl	%r13d, %esi
	movl	%edx, %r13d
	roll	$10, %edx
	roll	$8, %esi
	xorl	%edi, %r13d
	addl	%eax, %esi
	addl	-4(%r14), %eax
	xorl	%esi, %r13d
	addl	%r13d, %eax
	roll	$5, %eax
	addl	%ecx, %eax
	addl	%ebx, %ecx
	movl	%esi, %ebx
	roll	$10, %esi
	xorl	%edx, %ebx
	movl	%ebx, %r12d
	movl	%eax, %ebx
	xorl	%eax, %r12d
	xorl	%esi, %ebx
	roll	$10, %eax
	addl	%r12d, %ecx
	roll	$12, %ecx
	addl	%edi, %ecx
	addl	%r11d, %edi
	xorl	%ecx, %ebx
	addl	%ebx, %edi
	movl	%ecx, %ebx
	xorl	%eax, %ebx
	roll	$9, %edi
	addl	%edx, %edi
	movl	%ebx, %r11d
	addl	%r10d, %edx
	xorl	%edi, %r11d
	movl	%edi, %ebx
	roll	$10, %ecx
	addl	%r11d, %edx
	xorl	%ecx, %ebx
	roll	$10, %edi
	roll	$12, %edx
	movl	%ebx, %r11d
	addl	%esi, %edx
	addl	-60(%rbp), %esi
	xorl	%edx, %r11d
	movl	%edx, %ebx
	roll	$10, %edx
	addl	%r11d, %esi
	xorl	%edi, %ebx
	roll	$5, %esi
	movl	%ebx, %r11d
	movl	-96(%rbp), %ebx
	addl	%eax, %esi
	addl	-72(%rbp), %eax
	xorl	%esi, %r11d
	movl	%esi, %r13d
	roll	$10, %esi
	addl	%r11d, %eax
	xorl	%edx, %r13d
	roll	$14, %eax
	movl	%r13d, %r10d
	addl	%ecx, %eax
	addl	-68(%rbp), %ecx
	xorl	%eax, %r10d
	addl	%r10d, %ecx
	movl	%eax, %r10d
	roll	$6, %ecx
	xorl	%esi, %r10d
	addl	%edi, %ecx
	addl	-64(%rbp), %edi
	xorl	%ecx, %r10d
	addl	%r10d, %edi
	movl	%ecx, %r10d
	roll	$8, %edi
	addl	%edx, %edi
	roll	$10, %eax
	addl	-52(%rbp), %edx
	xorl	%eax, %r10d
	roll	$10, %ecx
	xorl	%edi, %r10d
	addl	%r10d, %edx
	movl	%edi, %r10d
	roll	$10, %edi
	roll	$13, %edx
	xorl	%ecx, %r10d
	addl	%esi, %edx
	addl	%r9d, %esi
	xorl	%edx, %r10d
	movl	%edx, %r13d
	roll	$10, %edx
	addl	%r10d, %esi
	xorl	%edi, %r13d
	movl	-124(%rbp), %r10d
	roll	$6, %esi
	leal	(%rsi,%rax), %r9d
	movl	%r13d, %esi
	addl	-44(%rbp), %eax
	movl	%r10d, %r12d
	xorl	%r9d, %esi
	movl	%r9d, %r13d
	roll	$10, %r9d
	addl	%esi, %eax
	xorl	%edx, %r13d
	roll	$5, %eax
	movl	%r13d, %esi
	addl	%ecx, %eax
	addl	-48(%rbp), %ecx
	xorl	%eax, %esi
	movl	%eax, %r13d
	addl	%esi, %ecx
	xorl	%r9d, %r13d
	roll	$15, %ecx
	movl	%r13d, %esi
	addl	%edi, %ecx
	addl	-56(%rbp), %edi
	xorl	%ecx, %esi
	movl	%ecx, %r13d
	addl	%esi, %edi
	roll	$13, %edi
	roll	$10, %eax
	roll	$10, %ecx
	xorl	%eax, %r13d
	addl	%edx, %edi
	addl	-76(%rbp), %edx
	movl	%r13d, %r8d
	movl	%edi, %r13d
	leal	(%rcx,%rbx), %esi
	roll	$10, %r12d
	xorl	%edi, %r8d
	roll	$10, %r13d
	xorl	%edi, %ecx
	leal	(%r12,%rsi), %ebx
	addl	%edx, %r8d
	movl	-88(%rbp), %edx
	movl	%ecx, %edi
	movl	%ebx, -88(%rbp)
	roll	$11, %r8d
	movl	%ebx, %r11d
	movq	-144(%rbp), %rbx
	addl	%r13d, %edx
	movl	-112(%rbp), %r13d
	addl	%r9d, %r8d
	addl	-80(%rbp), %r9d
	xorl	%r8d, %edi
	movl	%r11d, 4(%rbx)
	addl	%edx, %r13d
	movl	-84(%rbp), %edx
	addl	%edi, %r9d
	roll	$11, %r9d
	movl	%r13d, -108(%rbp)
	leal	(%rax,%rdx), %esi
	leal	(%rsi,%r15), %edx
	movl	-128(%rbp), %esi
	addl	-92(%rbp), %esi
	movl	%edx, 8(%rbx)
	addl	%esi, %eax
	movl	%edx, -96(%rbp)
	movl	%r15d, %edx
	leal	(%r9,%rax), %edi
	movq	%rbx, %rax
	notl	%edx
	movl	%edi, 12(%rbx)
	movl	%edi, -84(%rbp)
	orl	%r10d, %edx
	xorl	-112(%rbp), %edx
	addl	-132(%rbp), %edx
	addl	-116(%rbp), %r8d
	movl	%r13d, (%rax)
	roll	$6, %edx
	subq	$1, -104(%rbp)
	leal	(%rdx,%r8), %ebx
	movl	%ebx, 16(%rax)
	movq	-104(%rbp), %rax
	movl	%ebx, -92(%rbp)
	cmpq	$-1, %rax
	jne	.L5
.L3:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE155:
	.size	ripemd160_block_data_order, .-ripemd160_block_data_order
	.p2align 4
	.globl	RIPEMD160_Update
	.type	RIPEMD160_Update, @function
RIPEMD160_Update:
.LFB151:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L43
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	0(,%rdx,8), %eax
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	addl	20(%rdi), %eax
	movl	24(%rdi), %edx
	setc	%cl
	movl	%eax, 20(%rdi)
	movl	92(%rdi), %eax
	cmpl	$1, %ecx
	movq	%r12, %rcx
	sbbl	$-1, %edx
	shrq	$29, %rcx
	addl	%ecx, %edx
	movl	%edx, 24(%rdi)
	testq	%rax, %rax
	jne	.L46
	cmpq	$63, %r12
	ja	.L24
.L27:
	movl	%r14d, 92(%rbx)
	leaq	28(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
.L33:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	leaq	28(%rdi), %rcx
	leaq	(%r12,%rax), %r15
	leaq	(%rcx,%rax), %rdi
	cmpq	$63, %r12
	ja	.L16
	cmpq	$63, %r15
	jbe	.L17
.L16:
	movl	$64, %r12d
	subq	%rax, %r12
	cmpq	$8, %r12
	jnb	.L18
	testb	$4, %r12b
	jne	.L47
	testq	%r12, %r12
	je	.L19
	movzbl	0(%r13), %eax
	movb	%al, (%rdi)
	testb	$2, %r12b
	jne	.L48
.L19:
	movq	%rcx, %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%rcx, -56(%rbp)
	call	ripemd160_block_data_order
	movq	-56(%rbp), %rcx
	pxor	%xmm0, %xmm0
	addq	%r12, %r13
	leaq	-64(%r15), %r12
	movl	$0, 92(%rbx)
	movups	%xmm0, 28(%rbx)
	movups	%xmm0, 16(%rcx)
	movups	%xmm0, 32(%rcx)
	movups	%xmm0, 48(%rcx)
	cmpq	$63, %r12
	jbe	.L25
.L24:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	shrq	$6, %rdx
	call	ripemd160_block_data_order
	movq	%r12, %rax
	andq	$-64, %rax
	addq	%rax, %r13
	subq	%rax, %r12
.L25:
	testq	%r12, %r12
	je	.L33
	movl	%r12d, %r14d
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	0(%r13), %rax
	leaq	8(%rdi), %rdx
	movq	%r13, %r8
	andq	$-8, %rdx
	movq	%rax, (%rdi)
	movq	-8(%r13,%r12), %rax
	movq	%rax, -8(%rdi,%r12)
	subq	%rdx, %rdi
	subq	%rdi, %r8
	addq	%r12, %rdi
	andq	$-8, %rdi
	cmpq	$8, %rdi
	jb	.L19
	andq	$-8, %rdi
	xorl	%eax, %eax
.L22:
	movq	(%r8,%rax), %rsi
	movq	%rsi, (%rdx,%rax)
	addq	$8, %rax
	cmpq	%rdi, %rax
	jb	.L22
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L17:
	movq	%r12, %rdx
	call	memcpy@PLT
	addl	%r12d, 92(%rbx)
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L47:
	movl	0(%r13), %eax
	movl	%eax, (%rdi)
	movl	-4(%r13,%r12), %eax
	movl	%eax, -4(%rdi,%r12)
	jmp	.L19
.L48:
	movzwl	-2(%r13,%r12), %eax
	movw	%ax, -2(%rdi,%r12)
	jmp	.L19
	.cfi_endproc
.LFE151:
	.size	RIPEMD160_Update, .-RIPEMD160_Update
	.p2align 4
	.globl	RIPEMD160_Transform
	.type	RIPEMD160_Transform, @function
RIPEMD160_Transform:
.LFB152:
	.cfi_startproc
	endbr64
	movl	$1, %edx
	jmp	ripemd160_block_data_order
	.cfi_endproc
.LFE152:
	.size	RIPEMD160_Transform, .-RIPEMD160_Transform
	.p2align 4
	.globl	RIPEMD160_Final
	.type	RIPEMD160_Final, @function
RIPEMD160_Final:
.LFB153:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	28(%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movl	92(%rsi), %eax
	movb	$-128, 28(%rsi,%rax)
	addq	$1, %rax
	leaq	0(%r13,%rax), %rcx
	cmpq	$56, %rax
	ja	.L51
	movl	$56, %edx
	subq	%rax, %rdx
	movq	%rdx, %rax
.L52:
	xorl	%edi, %edi
	cmpl	$8, %eax
	jnb	.L55
	testb	$4, %al
	jne	.L73
	testl	%eax, %eax
	je	.L56
	movb	$0, (%rcx)
	testb	$2, %al
	jne	.L74
.L56:
	movq	20(%rbx), %rax
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movl	$1, %edx
	movq	%rax, 84(%rbx)
	call	ripemd160_block_data_order
	movl	$0, 92(%rbx)
	movq	%r13, %rdi
	movl	$64, %esi
	call	OPENSSL_cleanse@PLT
	movl	(%rbx), %eax
	movl	%eax, (%r12)
	movl	4(%rbx), %eax
	movl	%eax, 4(%r12)
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movl	12(%rbx), %eax
	movl	%eax, 12(%r12)
	movl	16(%rbx), %eax
	movl	%eax, 16(%r12)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	leaq	8(%rcx), %rsi
	movl	%eax, %edx
	movq	$0, (%rcx)
	movq	$0, -8(%rcx,%rdx)
	andq	$-8, %rsi
	subq	%rsi, %rcx
	leal	(%rax,%rcx), %edx
	andl	$-8, %edx
	cmpl	$8, %edx
	jb	.L56
	andl	$-8, %edx
	xorl	%eax, %eax
.L59:
	movl	%eax, %ecx
	addl	$8, %eax
	movq	%rdi, (%rsi,%rcx)
	cmpl	%edx, %eax
	jb	.L59
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L51:
	movl	$64, %edx
	subq	%rax, %rdx
	je	.L54
	xorl	%eax, %eax
.L53:
	movb	$0, (%rcx,%rax)
	addq	$1, %rax
	cmpq	%rdx, %rax
	jb	.L53
.L54:
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	ripemd160_block_data_order
	movq	%r13, %rcx
	movl	$56, %eax
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L73:
	movl	%eax, %eax
	movl	$0, (%rcx)
	movl	$0, -4(%rcx,%rax)
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L74:
	movl	%eax, %eax
	xorl	%edx, %edx
	movw	%dx, -2(%rcx,%rax)
	jmp	.L56
	.cfi_endproc
.LFE153:
	.size	RIPEMD160_Final, .-RIPEMD160_Final
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	1732584193
	.long	-271733879
	.long	-1732584194
	.long	271733878
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
