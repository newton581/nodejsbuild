	.file	"x_name.c"
	.text
	.p2align 4
	.globl	X509_NAME_ENTRY_free
	.type	X509_NAME_ENTRY_free, @function
X509_NAME_ENTRY_free:
.LFB904:
	.cfi_startproc
	endbr64
	leaq	X509_NAME_ENTRY_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE904:
	.size	X509_NAME_ENTRY_free, .-X509_NAME_ENTRY_free
	.p2align 4
	.type	x509_name_ex_print, @function
x509_name_ex_print:
.LFB918:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	8(%r8), %rcx
	movq	(%rsi), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509_NAME_print_ex@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	ret
	.cfi_endproc
.LFE918:
	.size	x509_name_ex_print, .-x509_name_ex_print
	.p2align 4
	.type	local_sk_X509_NAME_ENTRY_free, @function
local_sk_X509_NAME_ENTRY_free:
.LFB913:
	.cfi_startproc
	endbr64
	jmp	OPENSSL_sk_free@PLT
	.cfi_endproc
.LFE913:
	.size	local_sk_X509_NAME_ENTRY_free, .-local_sk_X509_NAME_ENTRY_free
	.p2align 4
	.type	local_sk_X509_NAME_ENTRY_pop_free, @function
local_sk_X509_NAME_ENTRY_pop_free:
.LFB914:
	.cfi_startproc
	endbr64
	leaq	X509_NAME_ENTRY_free(%rip), %rsi
	jmp	OPENSSL_sk_pop_free@PLT
	.cfi_endproc
.LFE914:
	.size	local_sk_X509_NAME_ENTRY_pop_free, .-local_sk_X509_NAME_ENTRY_pop_free
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/x509/x_name.c"
	.text
	.p2align 4
	.type	x509_name_ex_free, @function
x509_name_ex_free:
.LFB912:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L18
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	testq	%r12, %r12
	je	.L9
	movq	16(%r12), %rdi
	call	BUF_MEM_free@PLT
	movq	(%r12), %rdi
	leaq	X509_NAME_ENTRY_free(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	24(%r12), %rdi
	movl	$123, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$124, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	movq	$0, (%rbx)
.L9:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE912:
	.size	x509_name_ex_free, .-x509_name_ex_free
	.p2align 4
	.type	x509_name_ex_new, @function
x509_name_ex_new:
.LFB911:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$92, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$40, %edi
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L22
	movq	%rax, %r12
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L23
	call	BUF_MEM_new@PLT
	movq	%rax, 16(%r12)
	testq	%rax, %rax
	je	.L23
	movl	$1, 8(%r12)
	movl	$1, %eax
	movq	%r12, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	movl	$105, %r8d
	movl	$65, %edx
	movl	$171, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	(%r12), %rdi
	call	OPENSSL_sk_free@PLT
	movq	%r12, %rdi
	movl	$108, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	movl	$105, %r8d
	movl	$65, %edx
	movl	$171, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE911:
	.size	x509_name_ex_new, .-x509_name_ex_new
	.p2align 4
	.type	x509_name_canon.part.0, @function
x509_name_canon.part.0:
.LFB925:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	OPENSSL_sk_new_null@PLT
	movl	$-1, -88(%rbp)
	movq	%rax, -128(%rbp)
	movl	$0, -84(%rbp)
	movq	$0, -112(%rbp)
	testq	%rax, %rax
	je	.L93
	.p2align 4,,10
	.p2align 3
.L33:
	movq	-104(%rbp), %rax
	movq	(%rax), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, -84(%rbp)
	jge	.L94
	movq	-104(%rbp), %rax
	movl	-84(%rbp), %esi
	movq	(%rax), %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rbx
	movl	-88(%rbp), %eax
	cmpl	%eax, 16(%rbx)
	je	.L35
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L67
	movq	-128(%rbp), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L95
	movl	16(%rbx), %eax
	movl	%eax, -88(%rbp)
.L35:
	leaq	X509_NAME_ENTRY_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L96
	movq	(%rbx), %rdi
	call	OBJ_dup@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L97
	movq	8(%rbx), %r14
	movq	8(%r12), %rbx
	movl	4(%r14), %edi
	movq	%rbx, -96(%rbp)
	call	ASN1_tag2bit@PLT
	testl	$10582, %eax
	jne	.L39
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	ASN1_STRING_copy@PLT
	testl	%eax, %eax
	jne	.L40
	movq	%r12, %rbx
.L34:
	leaq	X509_NAME_ENTRY_it(%rip), %rsi
	movq	%rbx, %rdi
	movl	%eax, -84(%rbp)
	call	ASN1_item_free@PLT
	movq	-128(%rbp), %rdi
	leaq	local_sk_X509_NAME_ENTRY_pop_free(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	movl	-84(%rbp), %eax
	jne	.L98
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	movq	-96(%rbp), %rcx
	movl	%r14d, %eax
	movq	-136(%rbp), %r12
	subl	8(%rcx), %eax
	movl	%eax, (%rcx)
.L40:
	movq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L99
	addl	$1, -84(%rbp)
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L39:
	movq	-96(%rbp), %r15
	movq	%r14, %rsi
	movl	$12, 4(%r15)
	leaq	8(%r15), %rdi
	call	ASN1_STRING_to_UTF8@PLT
	movl	%eax, (%r15)
	movl	%eax, %ebx
	cmpl	$-1, %eax
	je	.L68
	movq	8(%r15), %r13
	testl	%eax, %eax
	jg	.L41
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L43:
	addq	$1, %r13
	subl	$1, %ebx
	je	.L45
.L41:
	movzbl	0(%r13), %edi
	movl	$8, %esi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	jne	.L43
	movslq	%ebx, %r14
	addq	%r13, %r14
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L46:
	subq	$1, %r14
	subl	$1, %ebx
	je	.L45
.L44:
	movzbl	-1(%r14), %edi
	movl	$8, %esi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	jne	.L46
	movl	%eax, %ecx
	movq	-96(%rbp), %rax
	movq	%r12, -136(%rbp)
	movq	%r13, %r12
	movl	%ebx, -116(%rbp)
	movl	%ecx, %r13d
	movq	8(%rax), %r14
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L90:
	movb	%al, -1(%r14)
.L91:
	movl	%ebx, %r13d
	movq	%r15, %r12
	cmpl	-116(%rbp), %ebx
	jge	.L100
.L51:
	movzbl	(%r12), %edi
	addq	$1, %r14
	leaq	1(%r12), %r15
	leal	1(%r13), %ebx
	movl	%edi, %eax
	testb	$-128, %al
	jne	.L90
	movl	$8, %esi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	jne	.L101
	movzbl	(%r12), %edi
	call	ossl_tolower@PLT
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L101:
	movb	$32, -1(%r14)
	movq	%r12, %r15
	.p2align 4,,10
	.p2align 3
.L50:
	addq	$1, %r15
	movzbl	(%r15), %edi
	movl	$8, %esi
	leal	0(%r13,%r15), %ebx
	subl	%r12d, %ebx
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	jne	.L50
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L68:
	movq	%r12, %rbx
	xorl	%eax, %eax
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L45:
	movq	-96(%rbp), %rcx
	xorl	%eax, %eax
	movl	%eax, (%rcx)
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L94:
	movq	-128(%rbp), %r14
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	leaq	-64(%rbp), %r12
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L56:
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	xorl	%esi, %esi
	movl	$-1, %r8d
	movq	%r12, %rdi
	movl	$-1, %ecx
	leaq	X509_NAME_ENTRIES_it(%rip), %rdx
	movq	%rax, -64(%rbp)
	call	ASN1_item_ex_i2d@PLT
	testl	%eax, %eax
	js	.L55
	addl	%eax, %ebx
	addl	$1, %r13d
.L54:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jl	.L56
	movq	-104(%rbp), %rax
	movslq	%ebx, %rdi
	movl	$358, %edx
	leaq	.LC0(%rip), %rsi
	movl	%ebx, 32(%rax)
	call	CRYPTO_malloc@PLT
	movq	%rax, -72(%rbp)
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L102
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %r13
	leaq	-72(%rbp), %r12
	movq	%rbx, 24(%rax)
	xorl	%ebx, %ebx
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L60:
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	leaq	-64(%rbp), %rdi
	movq	%r12, %rsi
	movl	$-1, %r8d
	movl	$-1, %ecx
	leaq	X509_NAME_ENTRIES_it(%rip), %rdx
	movq	%rax, -64(%rbp)
	call	ASN1_item_ex_i2d@PLT
	testl	%eax, %eax
	js	.L61
	addl	$1, %ebx
.L58:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L60
.L61:
	movl	$1, %eax
	xorl	%ebx, %ebx
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L96:
	movq	%rax, %rbx
	movl	$335, %r8d
.L92:
	movl	$65, %edx
	movl	$156, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L97:
	movq	%r12, %rbx
	movl	$340, %r8d
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L99:
	movl	$65, %edx
	movl	$156, %esi
	movl	%eax, -84(%rbp)
	movq	%r12, %rbx
	movl	$346, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$11, %edi
	call	ERR_put_error@PLT
	movl	-84(%rbp), %eax
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L93:
	movl	$317, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%ebx, %ebx
	movl	$156, %esi
	movl	$11, %edi
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L67:
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L95:
	movq	-112(%rbp), %rdi
	movl	%eax, -84(%rbp)
	xorl	%ebx, %ebx
	call	OPENSSL_sk_free@PLT
	movl	$328, %r8d
	movl	$65, %edx
	leaq	.LC0(%rip), %rcx
	movl	$156, %esi
	movl	$11, %edi
	call	ERR_put_error@PLT
	movl	-84(%rbp), %eax
	jmp	.L34
.L55:
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	jmp	.L34
.L102:
	movl	$360, %r8d
	jmp	.L92
.L98:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE925:
	.size	x509_name_canon.part.0, .-x509_name_canon.part.0
	.p2align 4
	.type	x509_name_ex_i2d, @function
x509_name_ex_i2d:
.LFB916:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	8(%rbx), %eax
	testl	%eax, %eax
	jne	.L129
.L105:
	movq	16(%rbx), %rdx
	movq	(%rdx), %rax
	movl	%eax, %r12d
	testq	%r13, %r13
	je	.L103
	movslq	%eax, %rbx
	movq	8(%rdx), %rsi
	movq	0(%r13), %rdi
	movq	%rbx, %rdx
	call	memcpy@PLT
	addq	%rbx, 0(%r13)
.L103:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L130
	addq	$56, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	movq	$0, -72(%rbp)
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, -72(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L106
	movl	$-1, -84(%rbp)
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L112:
	movq	(%rbx), %rdi
	movl	%r14d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r15
	movl	-84(%rbp), %eax
	cmpl	%eax, 16(%r15)
	je	.L108
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L128
	movq	-72(%rbp), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L131
	movl	16(%r15), %eax
	movl	%eax, -84(%rbp)
.L108:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L128
	addl	$1, %r14d
.L107:
	movq	(%rbx), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r14d
	jl	.L112
	leaq	-72(%rbp), %r14
	xorl	%esi, %esi
	movl	$-1, %r8d
	movl	$-1, %ecx
	movq	%r14, %rdi
	leaq	X509_NAME_INTERNAL_it(%rip), %rdx
	call	ASN1_item_ex_i2d@PLT
	movq	16(%rbx), %rdi
	movslq	%eax, %rsi
	movq	%rsi, %r12
	call	BUF_MEM_grow@PLT
	testq	%rax, %rax
	je	.L128
	movq	16(%rbx), %rax
	leaq	-64(%rbp), %rsi
	movl	$-1, %ecx
	movq	%r14, %rdi
	movl	$-1, %r8d
	leaq	X509_NAME_INTERNAL_it(%rip), %rdx
	movq	8(%rax), %rax
	movq	%rax, -64(%rbp)
	call	ASN1_item_ex_i2d@PLT
	movq	-72(%rbp), %rdi
	leaq	local_sk_X509_NAME_ENTRY_free(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movl	$0, 8(%rbx)
	testl	%r12d, %r12d
	js	.L103
	movq	24(%rbx), %rdi
	movl	$308, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 24(%rbx)
	movq	(%rbx), %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jne	.L116
	movl	$0, 32(%rbx)
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L131:
	movq	%r12, %rdi
	call	OPENSSL_sk_free@PLT
.L128:
	movq	-72(%rbp), %rdi
.L106:
	leaq	local_sk_X509_NAME_ENTRY_free(%rip), %rsi
	movl	$-1, %r12d
	call	OPENSSL_sk_pop_free@PLT
	movl	$276, %r8d
	movl	$65, %edx
	leaq	.LC0(%rip), %rcx
	movl	$203, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L116:
	movq	%rbx, %rdi
	call	x509_name_canon.part.0
	movl	%eax, %r12d
	testl	%eax, %eax
	js	.L103
	jmp	.L105
.L130:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE916:
	.size	x509_name_ex_i2d, .-x509_name_ex_i2d
	.p2align 4
	.type	x509_name_ex_d2i, @function
x509_name_ex_d2i:
.LFB915:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	X509_NAME_INTERNAL_it(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	leaq	-72(%rbp), %rdi
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %r12
	leaq	-80(%rbp), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$1048576, %rdx
	movl	$1048576, %eax
	pushq	24(%rbp)
	cmovg	%rax, %rdx
	movsbl	16(%rbp), %eax
	movq	%r12, -80(%rbp)
	movq	$0, -72(%rbp)
	pushq	%rax
	movq	$0, -64(%rbp)
	call	ASN1_item_ex_d2i@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	jle	.L132
	movq	(%r15), %r13
	testq	%r13, %r13
	je	.L134
	movq	16(%r13), %rdi
	call	BUF_MEM_free@PLT
	movq	0(%r13), %rdi
	leaq	X509_NAME_ENTRY_free(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	24(%r13), %rdi
	movl	$123, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$124, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	movq	$0, (%r15)
.L134:
	xorl	%esi, %esi
	leaq	-64(%rbp), %rdi
	call	x509_name_ex_new
	testl	%eax, %eax
	jne	.L163
.L135:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L142
	leaq	X509_NAME_it(%rip), %rsi
	call	ASN1_item_free@PLT
.L142:
	movq	-72(%rbp), %rdi
	leaq	local_sk_X509_NAME_ENTRY_pop_free(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movl	$206, %r8d
	movl	$58, %edx
	leaq	.LC0(%rip), %rcx
	movl	$158, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L132:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L164
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L163:
	.cfi_restore_state
	movq	-64(%rbp), %rax
	movq	-80(%rbp), %rsi
	movq	16(%rax), %rdi
	subq	%r12, %rsi
	call	BUF_MEM_grow@PLT
	testq	%rax, %rax
	je	.L135
	movq	-64(%rbp), %rax
	movq	-80(%rbp), %rdx
	movq	%r12, %rsi
	xorl	%r13d, %r13d
	movq	16(%rax), %rax
	subq	%r12, %rdx
	movq	8(%rax), %rdi
	call	memcpy@PLT
.L136:
	movq	-72(%rbp), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jge	.L165
	movq	-72(%rbp), %rdi
	movl	%r13d, %esi
	xorl	%ebx, %ebx
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r12
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L138:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	movl	%r13d, 16(%rax)
	movq	%rax, %rsi
	movq	-64(%rbp), %rax
	movq	(%rax), %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L135
	movl	%ebx, %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	addl	$1, %ebx
	call	OPENSSL_sk_set@PLT
.L137:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L138
	addl	$1, %r13d
	jmp	.L136
.L165:
	movq	-64(%rbp), %r12
	movl	$308, %edx
	leaq	.LC0(%rip), %rsi
	movq	24(%r12), %rdi
	call	CRYPTO_free@PLT
	movq	(%r12), %rdi
	movq	$0, 24(%r12)
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jne	.L140
	movl	$0, 32(%r12)
.L141:
	movq	-72(%rbp), %rdi
	leaq	local_sk_X509_NAME_ENTRY_free(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	-64(%rbp), %rax
	movl	$0, 8(%rax)
	movq	%rax, (%r15)
	movq	-80(%rbp), %rax
	movq	%rax, (%r14)
	movl	$1, %eax
	jmp	.L132
.L140:
	movq	%r12, %rdi
	call	x509_name_canon.part.0
	testl	%eax, %eax
	je	.L135
	jmp	.L141
.L164:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE915:
	.size	x509_name_ex_d2i, .-x509_name_ex_d2i
	.p2align 4
	.globl	d2i_X509_NAME_ENTRY
	.type	d2i_X509_NAME_ENTRY, @function
d2i_X509_NAME_ENTRY:
.LFB901:
	.cfi_startproc
	endbr64
	leaq	X509_NAME_ENTRY_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE901:
	.size	d2i_X509_NAME_ENTRY, .-d2i_X509_NAME_ENTRY
	.p2align 4
	.globl	i2d_X509_NAME_ENTRY
	.type	i2d_X509_NAME_ENTRY, @function
i2d_X509_NAME_ENTRY:
.LFB902:
	.cfi_startproc
	endbr64
	leaq	X509_NAME_ENTRY_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE902:
	.size	i2d_X509_NAME_ENTRY, .-i2d_X509_NAME_ENTRY
	.p2align 4
	.globl	X509_NAME_ENTRY_new
	.type	X509_NAME_ENTRY_new, @function
X509_NAME_ENTRY_new:
.LFB903:
	.cfi_startproc
	endbr64
	leaq	X509_NAME_ENTRY_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE903:
	.size	X509_NAME_ENTRY_new, .-X509_NAME_ENTRY_new
	.p2align 4
	.globl	X509_NAME_ENTRY_dup
	.type	X509_NAME_ENTRY_dup, @function
X509_NAME_ENTRY_dup:
.LFB905:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	leaq	X509_NAME_ENTRY_it(%rip), %rdi
	jmp	ASN1_item_dup@PLT
	.cfi_endproc
.LFE905:
	.size	X509_NAME_ENTRY_dup, .-X509_NAME_ENTRY_dup
	.p2align 4
	.globl	d2i_X509_NAME
	.type	d2i_X509_NAME, @function
d2i_X509_NAME:
.LFB906:
	.cfi_startproc
	endbr64
	leaq	X509_NAME_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE906:
	.size	d2i_X509_NAME, .-d2i_X509_NAME
	.p2align 4
	.globl	i2d_X509_NAME
	.type	i2d_X509_NAME, @function
i2d_X509_NAME:
.LFB907:
	.cfi_startproc
	endbr64
	leaq	X509_NAME_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE907:
	.size	i2d_X509_NAME, .-i2d_X509_NAME
	.p2align 4
	.globl	X509_NAME_new
	.type	X509_NAME_new, @function
X509_NAME_new:
.LFB908:
	.cfi_startproc
	endbr64
	leaq	X509_NAME_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE908:
	.size	X509_NAME_new, .-X509_NAME_new
	.p2align 4
	.globl	X509_NAME_free
	.type	X509_NAME_free, @function
X509_NAME_free:
.LFB909:
	.cfi_startproc
	endbr64
	leaq	X509_NAME_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE909:
	.size	X509_NAME_free, .-X509_NAME_free
	.p2align 4
	.globl	X509_NAME_dup
	.type	X509_NAME_dup, @function
X509_NAME_dup:
.LFB910:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	leaq	X509_NAME_it(%rip), %rdi
	jmp	ASN1_item_dup@PLT
	.cfi_endproc
.LFE910:
	.size	X509_NAME_dup, .-X509_NAME_dup
	.p2align 4
	.globl	X509_NAME_set
	.type	X509_NAME_set, @function
X509_NAME_set:
.LFB922:
	.cfi_startproc
	endbr64
	cmpq	%rsi, (%rdi)
	je	.L183
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	X509_NAME_it(%rip), %rdi
	call	ASN1_item_dup@PLT
	movq	%rax, %r12
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L175
	movq	(%rbx), %rdi
	leaq	X509_NAME_it(%rip), %rsi
	call	ASN1_item_free@PLT
	movq	%r12, (%rbx)
	movl	$1, %eax
.L175:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L183:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	testq	%rsi, %rsi
	setne	%al
	ret
	.cfi_endproc
.LFE922:
	.size	X509_NAME_set, .-X509_NAME_set
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	", "
	.text
	.p2align 4
	.globl	X509_NAME_print
	.type	X509_NAME_print, @function
X509_NAME_print:
.LFB923:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	xorl	%esi, %esi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	X509_NAME_oneline@PLT
	testq	%rax, %rax
	je	.L195
	leaq	1(%rax), %r14
	cmpb	$0, (%rax)
	movq	%rax, %r13
	movq	%r14, %rbx
	je	.L203
	movzbl	(%rbx), %eax
	cmpb	$47, %al
	je	.L204
	.p2align 4,,10
	.p2align 3
.L187:
	leaq	1(%rbx), %r15
	testb	%al, %al
	jne	.L191
.L189:
	movq	%rbx, %r15
	movq	%r14, %rsi
	movq	%r12, %rdi
	subq	%r14, %r15
	movl	%r15d, %edx
	call	BIO_write@PLT
	cmpl	%r15d, %eax
	jne	.L192
	leaq	1(%rbx), %r15
	cmpb	$0, (%rbx)
	movq	%r15, %r14
	jne	.L205
.L193:
	movq	%r13, %rdi
	movl	$532, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	.cfi_restore_state
	movl	$2, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	cmpl	$2, %eax
	jne	.L192
	cmpb	$0, (%rbx)
	je	.L193
.L191:
	movq	%r15, %rbx
	movzbl	(%rbx), %eax
	cmpb	$47, %al
	jne	.L187
.L204:
	movsbl	1(%rbx), %edi
	movl	$2, %esi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	je	.L202
	movsbl	2(%rbx), %edi
	cmpb	$61, %dil
	je	.L189
	movl	$2, %esi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	je	.L202
	cmpb	$61, 3(%rbx)
	je	.L189
	.p2align 4,,10
	.p2align 3
.L202:
	movzbl	(%rbx), %eax
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L192:
	movl	$535, %r8d
	movl	$7, %edx
	movl	$117, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r13, %rdi
	movl	$536, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L195:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L203:
	.cfi_restore_state
	movq	%rax, %rdi
	movl	$504, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE923:
	.size	X509_NAME_print, .-X509_NAME_print
	.p2align 4
	.globl	X509_NAME_get0_der
	.type	X509_NAME_get0_der, @function
X509_NAME_get0_der:
.LFB924:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	leaq	X509_NAME_it(%rip), %rdx
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	ASN1_item_i2d@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L206
	testq	%r13, %r13
	je	.L208
	movq	16(%rbx), %rax
	movq	8(%rax), %rax
	movq	%rax, 0(%r13)
.L208:
	movl	$1, %eax
	testq	%r12, %r12
	je	.L206
	movq	16(%rbx), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, (%r12)
.L206:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE924:
	.size	X509_NAME_get0_der, .-X509_NAME_get0_der
	.globl	X509_NAME_it
	.section	.rodata.str1.1
.LC2:
	.string	"X509_NAME"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	X509_NAME_it, @object
	.size	X509_NAME_it, 56
X509_NAME_it:
	.byte	4
	.zero	7
	.quad	16
	.quad	0
	.quad	0
	.quad	x509_name_ff
	.quad	0
	.quad	.LC2
	.align 32
	.type	x509_name_ff, @object
	.size	x509_name_ff, 56
x509_name_ff:
	.quad	0
	.quad	x509_name_ex_new
	.quad	x509_name_ex_free
	.quad	0
	.quad	x509_name_ex_d2i
	.quad	x509_name_ex_i2d
	.quad	x509_name_ex_print
	.section	.rodata.str1.1
.LC3:
	.string	"X509_NAME_INTERNAL"
	.section	.data.rel.ro.local
	.align 32
	.type	X509_NAME_INTERNAL_it, @object
	.size	X509_NAME_INTERNAL_it, 56
X509_NAME_INTERNAL_it:
	.byte	0
	.zero	7
	.quad	-1
	.quad	X509_NAME_INTERNAL_item_tt
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC3
	.section	.rodata.str1.1
.LC4:
	.string	"Name"
	.section	.data.rel.ro.local
	.align 32
	.type	X509_NAME_INTERNAL_item_tt, @object
	.size	X509_NAME_INTERNAL_item_tt, 40
X509_NAME_INTERNAL_item_tt:
	.quad	4
	.quad	0
	.quad	0
	.quad	.LC4
	.quad	X509_NAME_ENTRIES_it
	.section	.rodata.str1.1
.LC5:
	.string	"X509_NAME_ENTRIES"
	.section	.data.rel.ro.local
	.align 32
	.type	X509_NAME_ENTRIES_it, @object
	.size	X509_NAME_ENTRIES_it, 56
X509_NAME_ENTRIES_it:
	.byte	0
	.zero	7
	.quad	-1
	.quad	X509_NAME_ENTRIES_item_tt
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC5
	.section	.rodata.str1.1
.LC6:
	.string	"RDNS"
	.section	.data.rel.ro.local
	.align 32
	.type	X509_NAME_ENTRIES_item_tt, @object
	.size	X509_NAME_ENTRIES_item_tt, 40
X509_NAME_ENTRIES_item_tt:
	.quad	2
	.quad	0
	.quad	0
	.quad	.LC6
	.quad	X509_NAME_ENTRY_it
	.globl	X509_NAME_ENTRY_it
	.section	.rodata.str1.1
.LC7:
	.string	"X509_NAME_ENTRY"
	.section	.data.rel.ro.local
	.align 32
	.type	X509_NAME_ENTRY_it, @object
	.size	X509_NAME_ENTRY_it, 56
X509_NAME_ENTRY_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	X509_NAME_ENTRY_seq_tt
	.quad	2
	.quad	0
	.quad	24
	.quad	.LC7
	.section	.rodata.str1.1
.LC8:
	.string	"object"
.LC9:
	.string	"value"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	X509_NAME_ENTRY_seq_tt, @object
	.size	X509_NAME_ENTRY_seq_tt, 80
X509_NAME_ENTRY_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC8
	.quad	ASN1_OBJECT_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC9
	.quad	ASN1_PRINTABLE_it
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
