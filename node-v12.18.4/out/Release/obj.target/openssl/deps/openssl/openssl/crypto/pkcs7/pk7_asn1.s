	.file	"pk7_asn1.c"
	.text
	.p2align 4
	.type	pk7_cb, @function
pk7_cb:
.LFB803:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rcx, %rbx
	cmpl	$12, %edi
	je	.L2
	jg	.L3
	cmpl	$10, %edi
	jne	.L20
	movq	(%rsi), %rsi
	leaq	16(%rcx), %rdi
	call	PKCS7_stream@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L1
.L2:
	movq	(%rbx), %rsi
	movq	(%r12), %rdi
	call	PKCS7_dataInit@PLT
	testq	%rax, %rax
	movq	%rax, 8(%rbx)
	setne	%al
	movzbl	%al, %eax
.L1:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	cmpl	$11, %edi
	jne	.L18
.L5:
	movq	8(%rbx), %rsi
	movq	(%r12), %rdi
	call	PKCS7_dataFinal@PLT
	popq	%rbx
	popq	%r12
	testl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	setg	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	cmpl	$13, %edi
	je	.L5
.L18:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE803:
	.size	pk7_cb, .-pk7_cb
	.p2align 4
	.type	si_cb, @function
si_cb:
.LFB814:
	.cfi_startproc
	endbr64
	cmpl	$3, %edi
	je	.L27
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rsi), %rax
	movq	56(%rax), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	EVP_PKEY_free@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE814:
	.size	si_cb, .-si_cb
	.p2align 4
	.type	ri_cb, @function
ri_cb:
.LFB827:
	.cfi_startproc
	endbr64
	cmpl	$3, %edi
	je	.L34
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rsi), %rax
	movq	32(%rax), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509_free@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE827:
	.size	ri_cb, .-ri_cb
	.p2align 4
	.globl	d2i_PKCS7
	.type	d2i_PKCS7, @function
d2i_PKCS7:
.LFB804:
	.cfi_startproc
	endbr64
	leaq	PKCS7_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE804:
	.size	d2i_PKCS7, .-d2i_PKCS7
	.p2align 4
	.globl	i2d_PKCS7
	.type	i2d_PKCS7, @function
i2d_PKCS7:
.LFB805:
	.cfi_startproc
	endbr64
	leaq	PKCS7_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE805:
	.size	i2d_PKCS7, .-i2d_PKCS7
	.p2align 4
	.globl	PKCS7_new
	.type	PKCS7_new, @function
PKCS7_new:
.LFB806:
	.cfi_startproc
	endbr64
	leaq	PKCS7_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE806:
	.size	PKCS7_new, .-PKCS7_new
	.p2align 4
	.globl	PKCS7_free
	.type	PKCS7_free, @function
PKCS7_free:
.LFB807:
	.cfi_startproc
	endbr64
	leaq	PKCS7_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE807:
	.size	PKCS7_free, .-PKCS7_free
	.p2align 4
	.globl	i2d_PKCS7_NDEF
	.type	i2d_PKCS7_NDEF, @function
i2d_PKCS7_NDEF:
.LFB808:
	.cfi_startproc
	endbr64
	leaq	PKCS7_it(%rip), %rdx
	jmp	ASN1_item_ndef_i2d@PLT
	.cfi_endproc
.LFE808:
	.size	i2d_PKCS7_NDEF, .-i2d_PKCS7_NDEF
	.p2align 4
	.globl	PKCS7_dup
	.type	PKCS7_dup, @function
PKCS7_dup:
.LFB809:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	leaq	PKCS7_it(%rip), %rdi
	jmp	ASN1_item_dup@PLT
	.cfi_endproc
.LFE809:
	.size	PKCS7_dup, .-PKCS7_dup
	.p2align 4
	.globl	d2i_PKCS7_SIGNED
	.type	d2i_PKCS7_SIGNED, @function
d2i_PKCS7_SIGNED:
.LFB810:
	.cfi_startproc
	endbr64
	leaq	PKCS7_SIGNED_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE810:
	.size	d2i_PKCS7_SIGNED, .-d2i_PKCS7_SIGNED
	.p2align 4
	.globl	i2d_PKCS7_SIGNED
	.type	i2d_PKCS7_SIGNED, @function
i2d_PKCS7_SIGNED:
.LFB811:
	.cfi_startproc
	endbr64
	leaq	PKCS7_SIGNED_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE811:
	.size	i2d_PKCS7_SIGNED, .-i2d_PKCS7_SIGNED
	.p2align 4
	.globl	PKCS7_SIGNED_new
	.type	PKCS7_SIGNED_new, @function
PKCS7_SIGNED_new:
.LFB812:
	.cfi_startproc
	endbr64
	leaq	PKCS7_SIGNED_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE812:
	.size	PKCS7_SIGNED_new, .-PKCS7_SIGNED_new
	.p2align 4
	.globl	PKCS7_SIGNED_free
	.type	PKCS7_SIGNED_free, @function
PKCS7_SIGNED_free:
.LFB813:
	.cfi_startproc
	endbr64
	leaq	PKCS7_SIGNED_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE813:
	.size	PKCS7_SIGNED_free, .-PKCS7_SIGNED_free
	.p2align 4
	.globl	d2i_PKCS7_SIGNER_INFO
	.type	d2i_PKCS7_SIGNER_INFO, @function
d2i_PKCS7_SIGNER_INFO:
.LFB815:
	.cfi_startproc
	endbr64
	leaq	PKCS7_SIGNER_INFO_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE815:
	.size	d2i_PKCS7_SIGNER_INFO, .-d2i_PKCS7_SIGNER_INFO
	.p2align 4
	.globl	i2d_PKCS7_SIGNER_INFO
	.type	i2d_PKCS7_SIGNER_INFO, @function
i2d_PKCS7_SIGNER_INFO:
.LFB816:
	.cfi_startproc
	endbr64
	leaq	PKCS7_SIGNER_INFO_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE816:
	.size	i2d_PKCS7_SIGNER_INFO, .-i2d_PKCS7_SIGNER_INFO
	.p2align 4
	.globl	PKCS7_SIGNER_INFO_new
	.type	PKCS7_SIGNER_INFO_new, @function
PKCS7_SIGNER_INFO_new:
.LFB817:
	.cfi_startproc
	endbr64
	leaq	PKCS7_SIGNER_INFO_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE817:
	.size	PKCS7_SIGNER_INFO_new, .-PKCS7_SIGNER_INFO_new
	.p2align 4
	.globl	PKCS7_SIGNER_INFO_free
	.type	PKCS7_SIGNER_INFO_free, @function
PKCS7_SIGNER_INFO_free:
.LFB818:
	.cfi_startproc
	endbr64
	leaq	PKCS7_SIGNER_INFO_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE818:
	.size	PKCS7_SIGNER_INFO_free, .-PKCS7_SIGNER_INFO_free
	.p2align 4
	.globl	d2i_PKCS7_ISSUER_AND_SERIAL
	.type	d2i_PKCS7_ISSUER_AND_SERIAL, @function
d2i_PKCS7_ISSUER_AND_SERIAL:
.LFB819:
	.cfi_startproc
	endbr64
	leaq	PKCS7_ISSUER_AND_SERIAL_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE819:
	.size	d2i_PKCS7_ISSUER_AND_SERIAL, .-d2i_PKCS7_ISSUER_AND_SERIAL
	.p2align 4
	.globl	i2d_PKCS7_ISSUER_AND_SERIAL
	.type	i2d_PKCS7_ISSUER_AND_SERIAL, @function
i2d_PKCS7_ISSUER_AND_SERIAL:
.LFB820:
	.cfi_startproc
	endbr64
	leaq	PKCS7_ISSUER_AND_SERIAL_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE820:
	.size	i2d_PKCS7_ISSUER_AND_SERIAL, .-i2d_PKCS7_ISSUER_AND_SERIAL
	.p2align 4
	.globl	PKCS7_ISSUER_AND_SERIAL_new
	.type	PKCS7_ISSUER_AND_SERIAL_new, @function
PKCS7_ISSUER_AND_SERIAL_new:
.LFB821:
	.cfi_startproc
	endbr64
	leaq	PKCS7_ISSUER_AND_SERIAL_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE821:
	.size	PKCS7_ISSUER_AND_SERIAL_new, .-PKCS7_ISSUER_AND_SERIAL_new
	.p2align 4
	.globl	PKCS7_ISSUER_AND_SERIAL_free
	.type	PKCS7_ISSUER_AND_SERIAL_free, @function
PKCS7_ISSUER_AND_SERIAL_free:
.LFB822:
	.cfi_startproc
	endbr64
	leaq	PKCS7_ISSUER_AND_SERIAL_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE822:
	.size	PKCS7_ISSUER_AND_SERIAL_free, .-PKCS7_ISSUER_AND_SERIAL_free
	.p2align 4
	.globl	d2i_PKCS7_ENVELOPE
	.type	d2i_PKCS7_ENVELOPE, @function
d2i_PKCS7_ENVELOPE:
.LFB823:
	.cfi_startproc
	endbr64
	leaq	PKCS7_ENVELOPE_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE823:
	.size	d2i_PKCS7_ENVELOPE, .-d2i_PKCS7_ENVELOPE
	.p2align 4
	.globl	i2d_PKCS7_ENVELOPE
	.type	i2d_PKCS7_ENVELOPE, @function
i2d_PKCS7_ENVELOPE:
.LFB824:
	.cfi_startproc
	endbr64
	leaq	PKCS7_ENVELOPE_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE824:
	.size	i2d_PKCS7_ENVELOPE, .-i2d_PKCS7_ENVELOPE
	.p2align 4
	.globl	PKCS7_ENVELOPE_new
	.type	PKCS7_ENVELOPE_new, @function
PKCS7_ENVELOPE_new:
.LFB825:
	.cfi_startproc
	endbr64
	leaq	PKCS7_ENVELOPE_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE825:
	.size	PKCS7_ENVELOPE_new, .-PKCS7_ENVELOPE_new
	.p2align 4
	.globl	PKCS7_ENVELOPE_free
	.type	PKCS7_ENVELOPE_free, @function
PKCS7_ENVELOPE_free:
.LFB826:
	.cfi_startproc
	endbr64
	leaq	PKCS7_ENVELOPE_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE826:
	.size	PKCS7_ENVELOPE_free, .-PKCS7_ENVELOPE_free
	.p2align 4
	.globl	d2i_PKCS7_RECIP_INFO
	.type	d2i_PKCS7_RECIP_INFO, @function
d2i_PKCS7_RECIP_INFO:
.LFB828:
	.cfi_startproc
	endbr64
	leaq	PKCS7_RECIP_INFO_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE828:
	.size	d2i_PKCS7_RECIP_INFO, .-d2i_PKCS7_RECIP_INFO
	.p2align 4
	.globl	i2d_PKCS7_RECIP_INFO
	.type	i2d_PKCS7_RECIP_INFO, @function
i2d_PKCS7_RECIP_INFO:
.LFB829:
	.cfi_startproc
	endbr64
	leaq	PKCS7_RECIP_INFO_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE829:
	.size	i2d_PKCS7_RECIP_INFO, .-i2d_PKCS7_RECIP_INFO
	.p2align 4
	.globl	PKCS7_RECIP_INFO_new
	.type	PKCS7_RECIP_INFO_new, @function
PKCS7_RECIP_INFO_new:
.LFB830:
	.cfi_startproc
	endbr64
	leaq	PKCS7_RECIP_INFO_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE830:
	.size	PKCS7_RECIP_INFO_new, .-PKCS7_RECIP_INFO_new
	.p2align 4
	.globl	PKCS7_RECIP_INFO_free
	.type	PKCS7_RECIP_INFO_free, @function
PKCS7_RECIP_INFO_free:
.LFB831:
	.cfi_startproc
	endbr64
	leaq	PKCS7_RECIP_INFO_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE831:
	.size	PKCS7_RECIP_INFO_free, .-PKCS7_RECIP_INFO_free
	.p2align 4
	.globl	d2i_PKCS7_ENC_CONTENT
	.type	d2i_PKCS7_ENC_CONTENT, @function
d2i_PKCS7_ENC_CONTENT:
.LFB832:
	.cfi_startproc
	endbr64
	leaq	PKCS7_ENC_CONTENT_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE832:
	.size	d2i_PKCS7_ENC_CONTENT, .-d2i_PKCS7_ENC_CONTENT
	.p2align 4
	.globl	i2d_PKCS7_ENC_CONTENT
	.type	i2d_PKCS7_ENC_CONTENT, @function
i2d_PKCS7_ENC_CONTENT:
.LFB833:
	.cfi_startproc
	endbr64
	leaq	PKCS7_ENC_CONTENT_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE833:
	.size	i2d_PKCS7_ENC_CONTENT, .-i2d_PKCS7_ENC_CONTENT
	.p2align 4
	.globl	PKCS7_ENC_CONTENT_new
	.type	PKCS7_ENC_CONTENT_new, @function
PKCS7_ENC_CONTENT_new:
.LFB834:
	.cfi_startproc
	endbr64
	leaq	PKCS7_ENC_CONTENT_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE834:
	.size	PKCS7_ENC_CONTENT_new, .-PKCS7_ENC_CONTENT_new
	.p2align 4
	.globl	PKCS7_ENC_CONTENT_free
	.type	PKCS7_ENC_CONTENT_free, @function
PKCS7_ENC_CONTENT_free:
.LFB835:
	.cfi_startproc
	endbr64
	leaq	PKCS7_ENC_CONTENT_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE835:
	.size	PKCS7_ENC_CONTENT_free, .-PKCS7_ENC_CONTENT_free
	.p2align 4
	.globl	d2i_PKCS7_SIGN_ENVELOPE
	.type	d2i_PKCS7_SIGN_ENVELOPE, @function
d2i_PKCS7_SIGN_ENVELOPE:
.LFB836:
	.cfi_startproc
	endbr64
	leaq	PKCS7_SIGN_ENVELOPE_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE836:
	.size	d2i_PKCS7_SIGN_ENVELOPE, .-d2i_PKCS7_SIGN_ENVELOPE
	.p2align 4
	.globl	i2d_PKCS7_SIGN_ENVELOPE
	.type	i2d_PKCS7_SIGN_ENVELOPE, @function
i2d_PKCS7_SIGN_ENVELOPE:
.LFB837:
	.cfi_startproc
	endbr64
	leaq	PKCS7_SIGN_ENVELOPE_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE837:
	.size	i2d_PKCS7_SIGN_ENVELOPE, .-i2d_PKCS7_SIGN_ENVELOPE
	.p2align 4
	.globl	PKCS7_SIGN_ENVELOPE_new
	.type	PKCS7_SIGN_ENVELOPE_new, @function
PKCS7_SIGN_ENVELOPE_new:
.LFB838:
	.cfi_startproc
	endbr64
	leaq	PKCS7_SIGN_ENVELOPE_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE838:
	.size	PKCS7_SIGN_ENVELOPE_new, .-PKCS7_SIGN_ENVELOPE_new
	.p2align 4
	.globl	PKCS7_SIGN_ENVELOPE_free
	.type	PKCS7_SIGN_ENVELOPE_free, @function
PKCS7_SIGN_ENVELOPE_free:
.LFB839:
	.cfi_startproc
	endbr64
	leaq	PKCS7_SIGN_ENVELOPE_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE839:
	.size	PKCS7_SIGN_ENVELOPE_free, .-PKCS7_SIGN_ENVELOPE_free
	.p2align 4
	.globl	d2i_PKCS7_ENCRYPT
	.type	d2i_PKCS7_ENCRYPT, @function
d2i_PKCS7_ENCRYPT:
.LFB840:
	.cfi_startproc
	endbr64
	leaq	PKCS7_ENCRYPT_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE840:
	.size	d2i_PKCS7_ENCRYPT, .-d2i_PKCS7_ENCRYPT
	.p2align 4
	.globl	i2d_PKCS7_ENCRYPT
	.type	i2d_PKCS7_ENCRYPT, @function
i2d_PKCS7_ENCRYPT:
.LFB841:
	.cfi_startproc
	endbr64
	leaq	PKCS7_ENCRYPT_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE841:
	.size	i2d_PKCS7_ENCRYPT, .-i2d_PKCS7_ENCRYPT
	.p2align 4
	.globl	PKCS7_ENCRYPT_new
	.type	PKCS7_ENCRYPT_new, @function
PKCS7_ENCRYPT_new:
.LFB842:
	.cfi_startproc
	endbr64
	leaq	PKCS7_ENCRYPT_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE842:
	.size	PKCS7_ENCRYPT_new, .-PKCS7_ENCRYPT_new
	.p2align 4
	.globl	PKCS7_ENCRYPT_free
	.type	PKCS7_ENCRYPT_free, @function
PKCS7_ENCRYPT_free:
.LFB843:
	.cfi_startproc
	endbr64
	leaq	PKCS7_ENCRYPT_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE843:
	.size	PKCS7_ENCRYPT_free, .-PKCS7_ENCRYPT_free
	.p2align 4
	.globl	d2i_PKCS7_DIGEST
	.type	d2i_PKCS7_DIGEST, @function
d2i_PKCS7_DIGEST:
.LFB844:
	.cfi_startproc
	endbr64
	leaq	PKCS7_DIGEST_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE844:
	.size	d2i_PKCS7_DIGEST, .-d2i_PKCS7_DIGEST
	.p2align 4
	.globl	i2d_PKCS7_DIGEST
	.type	i2d_PKCS7_DIGEST, @function
i2d_PKCS7_DIGEST:
.LFB845:
	.cfi_startproc
	endbr64
	leaq	PKCS7_DIGEST_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE845:
	.size	i2d_PKCS7_DIGEST, .-i2d_PKCS7_DIGEST
	.p2align 4
	.globl	PKCS7_DIGEST_new
	.type	PKCS7_DIGEST_new, @function
PKCS7_DIGEST_new:
.LFB846:
	.cfi_startproc
	endbr64
	leaq	PKCS7_DIGEST_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE846:
	.size	PKCS7_DIGEST_new, .-PKCS7_DIGEST_new
	.p2align 4
	.globl	PKCS7_DIGEST_free
	.type	PKCS7_DIGEST_free, @function
PKCS7_DIGEST_free:
.LFB847:
	.cfi_startproc
	endbr64
	leaq	PKCS7_DIGEST_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE847:
	.size	PKCS7_DIGEST_free, .-PKCS7_DIGEST_free
	.p2align 4
	.globl	PKCS7_print_ctx
	.type	PKCS7_print_ctx, @function
PKCS7_print_ctx:
.LFB848:
	.cfi_startproc
	endbr64
	movq	%rcx, %r8
	leaq	PKCS7_it(%rip), %rcx
	jmp	ASN1_item_print@PLT
	.cfi_endproc
.LFE848:
	.size	PKCS7_print_ctx, .-PKCS7_print_ctx
	.globl	PKCS7_ATTR_VERIFY_it
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"PKCS7_ATTR_VERIFY"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	PKCS7_ATTR_VERIFY_it, @object
	.size	PKCS7_ATTR_VERIFY_it, 56
PKCS7_ATTR_VERIFY_it:
	.byte	0
	.zero	7
	.quad	-1
	.quad	PKCS7_ATTR_VERIFY_item_tt
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC0
	.section	.rodata.str1.1
.LC1:
	.string	"PKCS7_ATTRIBUTES"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	PKCS7_ATTR_VERIFY_item_tt, @object
	.size	PKCS7_ATTR_VERIFY_item_tt, 40
PKCS7_ATTR_VERIFY_item_tt:
	.quad	12
	.quad	17
	.quad	0
	.quad	.LC1
	.quad	X509_ATTRIBUTE_it
	.globl	PKCS7_ATTR_SIGN_it
	.section	.rodata.str1.1
.LC2:
	.string	"PKCS7_ATTR_SIGN"
	.section	.data.rel.ro.local
	.align 32
	.type	PKCS7_ATTR_SIGN_it, @object
	.size	PKCS7_ATTR_SIGN_it, 56
PKCS7_ATTR_SIGN_it:
	.byte	0
	.zero	7
	.quad	-1
	.quad	PKCS7_ATTR_SIGN_item_tt
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC2
	.section	.data.rel.ro
	.align 32
	.type	PKCS7_ATTR_SIGN_item_tt, @object
	.size	PKCS7_ATTR_SIGN_item_tt, 40
PKCS7_ATTR_SIGN_item_tt:
	.quad	6
	.quad	0
	.quad	0
	.quad	.LC1
	.quad	X509_ATTRIBUTE_it
	.globl	PKCS7_DIGEST_it
	.section	.rodata.str1.1
.LC3:
	.string	"PKCS7_DIGEST"
	.section	.data.rel.ro.local
	.align 32
	.type	PKCS7_DIGEST_it, @object
	.size	PKCS7_DIGEST_it, 56
PKCS7_DIGEST_it:
	.byte	6
	.zero	7
	.quad	16
	.quad	PKCS7_DIGEST_seq_tt
	.quad	4
	.quad	0
	.quad	32
	.quad	.LC3
	.section	.rodata.str1.1
.LC4:
	.string	"version"
.LC5:
	.string	"md"
.LC6:
	.string	"contents"
.LC7:
	.string	"digest"
	.section	.data.rel.ro
	.align 32
	.type	PKCS7_DIGEST_seq_tt, @object
	.size	PKCS7_DIGEST_seq_tt, 160
PKCS7_DIGEST_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC4
	.quad	ASN1_INTEGER_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC5
	.quad	X509_ALGOR_it
	.quad	0
	.quad	0
	.quad	16
	.quad	.LC6
	.quad	PKCS7_it
	.quad	0
	.quad	0
	.quad	24
	.quad	.LC7
	.quad	ASN1_OCTET_STRING_it
	.globl	PKCS7_ENCRYPT_it
	.section	.rodata.str1.1
.LC8:
	.string	"PKCS7_ENCRYPT"
	.section	.data.rel.ro.local
	.align 32
	.type	PKCS7_ENCRYPT_it, @object
	.size	PKCS7_ENCRYPT_it, 56
PKCS7_ENCRYPT_it:
	.byte	6
	.zero	7
	.quad	16
	.quad	PKCS7_ENCRYPT_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC8
	.section	.rodata.str1.1
.LC9:
	.string	"enc_data"
	.section	.data.rel.ro
	.align 32
	.type	PKCS7_ENCRYPT_seq_tt, @object
	.size	PKCS7_ENCRYPT_seq_tt, 80
PKCS7_ENCRYPT_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC4
	.quad	ASN1_INTEGER_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC9
	.quad	PKCS7_ENC_CONTENT_it
	.globl	PKCS7_SIGN_ENVELOPE_it
	.section	.rodata.str1.1
.LC10:
	.string	"PKCS7_SIGN_ENVELOPE"
	.section	.data.rel.ro.local
	.align 32
	.type	PKCS7_SIGN_ENVELOPE_it, @object
	.size	PKCS7_SIGN_ENVELOPE_it, 56
PKCS7_SIGN_ENVELOPE_it:
	.byte	6
	.zero	7
	.quad	16
	.quad	PKCS7_SIGN_ENVELOPE_seq_tt
	.quad	7
	.quad	0
	.quad	56
	.quad	.LC10
	.section	.rodata.str1.1
.LC11:
	.string	"recipientinfo"
.LC12:
	.string	"md_algs"
.LC13:
	.string	"cert"
.LC14:
	.string	"crl"
.LC15:
	.string	"signer_info"
	.section	.data.rel.ro
	.align 32
	.type	PKCS7_SIGN_ENVELOPE_seq_tt, @object
	.size	PKCS7_SIGN_ENVELOPE_seq_tt, 280
PKCS7_SIGN_ENVELOPE_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC4
	.quad	ASN1_INTEGER_it
	.quad	2
	.quad	0
	.quad	48
	.quad	.LC11
	.quad	PKCS7_RECIP_INFO_it
	.quad	2
	.quad	0
	.quad	8
	.quad	.LC12
	.quad	X509_ALGOR_it
	.quad	0
	.quad	0
	.quad	40
	.quad	.LC9
	.quad	PKCS7_ENC_CONTENT_it
	.quad	139
	.quad	0
	.quad	16
	.quad	.LC13
	.quad	X509_it
	.quad	139
	.quad	1
	.quad	24
	.quad	.LC14
	.quad	X509_CRL_it
	.quad	2
	.quad	0
	.quad	32
	.quad	.LC15
	.quad	PKCS7_SIGNER_INFO_it
	.globl	PKCS7_ENC_CONTENT_it
	.section	.rodata.str1.1
.LC16:
	.string	"PKCS7_ENC_CONTENT"
	.section	.data.rel.ro.local
	.align 32
	.type	PKCS7_ENC_CONTENT_it, @object
	.size	PKCS7_ENC_CONTENT_it, 56
PKCS7_ENC_CONTENT_it:
	.byte	6
	.zero	7
	.quad	16
	.quad	PKCS7_ENC_CONTENT_seq_tt
	.quad	3
	.quad	0
	.quad	32
	.quad	.LC16
	.section	.rodata.str1.1
.LC17:
	.string	"content_type"
.LC18:
	.string	"algorithm"
	.section	.data.rel.ro
	.align 32
	.type	PKCS7_ENC_CONTENT_seq_tt, @object
	.size	PKCS7_ENC_CONTENT_seq_tt, 120
PKCS7_ENC_CONTENT_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC17
	.quad	ASN1_OBJECT_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC18
	.quad	X509_ALGOR_it
	.quad	137
	.quad	0
	.quad	16
	.quad	.LC9
	.quad	ASN1_OCTET_STRING_NDEF_it
	.globl	PKCS7_RECIP_INFO_it
	.section	.rodata.str1.1
.LC19:
	.string	"PKCS7_RECIP_INFO"
	.section	.data.rel.ro.local
	.align 32
	.type	PKCS7_RECIP_INFO_it, @object
	.size	PKCS7_RECIP_INFO_it, 56
PKCS7_RECIP_INFO_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	PKCS7_RECIP_INFO_seq_tt
	.quad	4
	.quad	PKCS7_RECIP_INFO_aux
	.quad	40
	.quad	.LC19
	.section	.rodata.str1.1
.LC20:
	.string	"issuer_and_serial"
.LC21:
	.string	"key_enc_algor"
.LC22:
	.string	"enc_key"
	.section	.data.rel.ro
	.align 32
	.type	PKCS7_RECIP_INFO_seq_tt, @object
	.size	PKCS7_RECIP_INFO_seq_tt, 160
PKCS7_RECIP_INFO_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC4
	.quad	ASN1_INTEGER_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC20
	.quad	PKCS7_ISSUER_AND_SERIAL_it
	.quad	0
	.quad	0
	.quad	16
	.quad	.LC21
	.quad	X509_ALGOR_it
	.quad	0
	.quad	0
	.quad	24
	.quad	.LC22
	.quad	ASN1_OCTET_STRING_it
	.section	.data.rel.ro.local
	.align 32
	.type	PKCS7_RECIP_INFO_aux, @object
	.size	PKCS7_RECIP_INFO_aux, 40
PKCS7_RECIP_INFO_aux:
	.quad	0
	.long	0
	.long	0
	.long	0
	.zero	4
	.quad	ri_cb
	.long	0
	.zero	4
	.globl	PKCS7_ENVELOPE_it
	.section	.rodata.str1.1
.LC23:
	.string	"PKCS7_ENVELOPE"
	.section	.data.rel.ro.local
	.align 32
	.type	PKCS7_ENVELOPE_it, @object
	.size	PKCS7_ENVELOPE_it, 56
PKCS7_ENVELOPE_it:
	.byte	6
	.zero	7
	.quad	16
	.quad	PKCS7_ENVELOPE_seq_tt
	.quad	3
	.quad	0
	.quad	24
	.quad	.LC23
	.section	.data.rel.ro
	.align 32
	.type	PKCS7_ENVELOPE_seq_tt, @object
	.size	PKCS7_ENVELOPE_seq_tt, 120
PKCS7_ENVELOPE_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC4
	.quad	ASN1_INTEGER_it
	.quad	2
	.quad	0
	.quad	8
	.quad	.LC11
	.quad	PKCS7_RECIP_INFO_it
	.quad	0
	.quad	0
	.quad	16
	.quad	.LC9
	.quad	PKCS7_ENC_CONTENT_it
	.globl	PKCS7_ISSUER_AND_SERIAL_it
	.section	.rodata.str1.1
.LC24:
	.string	"PKCS7_ISSUER_AND_SERIAL"
	.section	.data.rel.ro.local
	.align 32
	.type	PKCS7_ISSUER_AND_SERIAL_it, @object
	.size	PKCS7_ISSUER_AND_SERIAL_it, 56
PKCS7_ISSUER_AND_SERIAL_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	PKCS7_ISSUER_AND_SERIAL_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC24
	.section	.rodata.str1.1
.LC25:
	.string	"issuer"
.LC26:
	.string	"serial"
	.section	.data.rel.ro
	.align 32
	.type	PKCS7_ISSUER_AND_SERIAL_seq_tt, @object
	.size	PKCS7_ISSUER_AND_SERIAL_seq_tt, 80
PKCS7_ISSUER_AND_SERIAL_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC25
	.quad	X509_NAME_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC26
	.quad	ASN1_INTEGER_it
	.globl	PKCS7_SIGNER_INFO_it
	.section	.rodata.str1.1
.LC27:
	.string	"PKCS7_SIGNER_INFO"
	.section	.data.rel.ro.local
	.align 32
	.type	PKCS7_SIGNER_INFO_it, @object
	.size	PKCS7_SIGNER_INFO_it, 56
PKCS7_SIGNER_INFO_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	PKCS7_SIGNER_INFO_seq_tt
	.quad	7
	.quad	PKCS7_SIGNER_INFO_aux
	.quad	64
	.quad	.LC27
	.section	.rodata.str1.1
.LC28:
	.string	"digest_alg"
.LC29:
	.string	"auth_attr"
.LC30:
	.string	"digest_enc_alg"
.LC31:
	.string	"enc_digest"
.LC32:
	.string	"unauth_attr"
	.section	.data.rel.ro
	.align 32
	.type	PKCS7_SIGNER_INFO_seq_tt, @object
	.size	PKCS7_SIGNER_INFO_seq_tt, 280
PKCS7_SIGNER_INFO_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC4
	.quad	ASN1_INTEGER_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC20
	.quad	PKCS7_ISSUER_AND_SERIAL_it
	.quad	0
	.quad	0
	.quad	16
	.quad	.LC28
	.quad	X509_ALGOR_it
	.quad	141
	.quad	0
	.quad	24
	.quad	.LC29
	.quad	X509_ATTRIBUTE_it
	.quad	0
	.quad	0
	.quad	32
	.quad	.LC30
	.quad	X509_ALGOR_it
	.quad	0
	.quad	0
	.quad	40
	.quad	.LC31
	.quad	ASN1_OCTET_STRING_it
	.quad	139
	.quad	1
	.quad	48
	.quad	.LC32
	.quad	X509_ATTRIBUTE_it
	.section	.data.rel.ro.local
	.align 32
	.type	PKCS7_SIGNER_INFO_aux, @object
	.size	PKCS7_SIGNER_INFO_aux, 40
PKCS7_SIGNER_INFO_aux:
	.quad	0
	.long	0
	.long	0
	.long	0
	.zero	4
	.quad	si_cb
	.long	0
	.zero	4
	.globl	PKCS7_SIGNED_it
	.section	.rodata.str1.1
.LC33:
	.string	"PKCS7_SIGNED"
	.section	.data.rel.ro.local
	.align 32
	.type	PKCS7_SIGNED_it, @object
	.size	PKCS7_SIGNED_it, 56
PKCS7_SIGNED_it:
	.byte	6
	.zero	7
	.quad	16
	.quad	PKCS7_SIGNED_seq_tt
	.quad	6
	.quad	0
	.quad	48
	.quad	.LC33
	.section	.data.rel.ro
	.align 32
	.type	PKCS7_SIGNED_seq_tt, @object
	.size	PKCS7_SIGNED_seq_tt, 240
PKCS7_SIGNED_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC4
	.quad	ASN1_INTEGER_it
	.quad	2
	.quad	0
	.quad	8
	.quad	.LC12
	.quad	X509_ALGOR_it
	.quad	0
	.quad	0
	.quad	40
	.quad	.LC6
	.quad	PKCS7_it
	.quad	141
	.quad	0
	.quad	16
	.quad	.LC13
	.quad	X509_it
	.quad	139
	.quad	1
	.quad	24
	.quad	.LC14
	.quad	X509_CRL_it
	.quad	2
	.quad	0
	.quad	32
	.quad	.LC15
	.quad	PKCS7_SIGNER_INFO_it
	.globl	PKCS7_it
	.section	.rodata.str1.1
.LC34:
	.string	"PKCS7"
	.section	.data.rel.ro.local
	.align 32
	.type	PKCS7_it, @object
	.size	PKCS7_it, 56
PKCS7_it:
	.byte	6
	.zero	7
	.quad	16
	.quad	PKCS7_seq_tt
	.quad	2
	.quad	PKCS7_aux
	.quad	40
	.quad	.LC34
	.section	.rodata.str1.1
.LC35:
	.string	"type"
	.section	.data.rel.ro
	.align 32
	.type	PKCS7_seq_tt, @object
	.size	PKCS7_seq_tt, 80
PKCS7_seq_tt:
	.quad	0
	.quad	0
	.quad	24
	.quad	.LC35
	.quad	ASN1_OBJECT_it
	.quad	256
	.quad	-1
	.quad	0
	.quad	.LC34
	.quad	PKCS7_adb
	.section	.data.rel.ro.local
	.align 32
	.type	PKCS7_aux, @object
	.size	PKCS7_aux, 40
PKCS7_aux:
	.quad	0
	.long	0
	.long	0
	.long	0
	.zero	4
	.quad	pk7_cb
	.long	0
	.zero	4
	.align 32
	.type	PKCS7_adb, @object
	.size	PKCS7_adb, 56
PKCS7_adb:
	.quad	0
	.quad	24
	.quad	0
	.quad	PKCS7_adbtbl
	.quad	6
	.quad	p7default_tt
	.quad	0
	.section	.rodata.str1.1
.LC36:
	.string	"d.data"
.LC37:
	.string	"d.sign"
.LC38:
	.string	"d.enveloped"
.LC39:
	.string	"d.signed_and_enveloped"
.LC40:
	.string	"d.digest"
.LC41:
	.string	"d.encrypted"
	.section	.data.rel.ro
	.align 32
	.type	PKCS7_adbtbl, @object
	.size	PKCS7_adbtbl, 288
PKCS7_adbtbl:
	.quad	21
	.quad	2193
	.quad	0
	.quad	32
	.quad	.LC36
	.quad	ASN1_OCTET_STRING_NDEF_it
	.quad	22
	.quad	2193
	.quad	0
	.quad	32
	.quad	.LC37
	.quad	PKCS7_SIGNED_it
	.quad	23
	.quad	2193
	.quad	0
	.quad	32
	.quad	.LC38
	.quad	PKCS7_ENVELOPE_it
	.quad	24
	.quad	2193
	.quad	0
	.quad	32
	.quad	.LC39
	.quad	PKCS7_SIGN_ENVELOPE_it
	.quad	25
	.quad	2193
	.quad	0
	.quad	32
	.quad	.LC40
	.quad	PKCS7_DIGEST_it
	.quad	26
	.quad	2193
	.quad	0
	.quad	32
	.quad	.LC41
	.quad	PKCS7_ENCRYPT_it
	.section	.rodata.str1.1
.LC42:
	.string	"d.other"
	.section	.data.rel.ro
	.align 32
	.type	p7default_tt, @object
	.size	p7default_tt, 40
p7default_tt:
	.quad	145
	.quad	0
	.quad	32
	.quad	.LC42
	.quad	ASN1_ANY_it
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
