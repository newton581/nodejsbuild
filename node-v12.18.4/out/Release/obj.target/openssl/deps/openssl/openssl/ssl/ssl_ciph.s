	.file	"ssl_ciph.c"
	.text
	.p2align 4
	.type	ssl_cipher_apply_rule, @function
ssl_cipher_apply_rule:
.LFB1123:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -56(%rbp)
	movl	%r8d, -64(%rbp)
	movl	24(%rbp), %r10d
	movq	40(%rbp), %rbx
	movl	%esi, -52(%rbp)
	movl	%ecx, -60(%rbp)
	movl	32(%rbp), %r8d
	cmpl	$3, %r10d
	movq	(%rbx), %r14
	movq	48(%rbp), %rbx
	movl	%r9d, -68(%rbp)
	sete	%dl
	cmpl	$6, %r10d
	sete	%al
	movq	(%rbx), %r13
	orb	%al, %dl
	movb	%dl, -69(%rbp)
	je	.L38
	movq	%r14, %r9
	movq	%r13, %rax
	movl	$1, %ebx
.L2:
	testq	%rax, %rax
	je	.L3
	testq	%r9, %r9
	je	.L3
	movl	16(%rbp), %edi
	movq	%r14, -48(%rbp)
	pxor	%xmm0, %xmm0
	andl	$31, %edi
	movl	%edi, -76(%rbp)
	movl	16(%rbp), %edi
	andl	$32, %edi
	movl	%edi, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L4:
	movq	16(%rax), %rdx
	movq	24(%rax), %rcx
	testl	%ebx, %ebx
	movq	(%rax), %rdi
	movq	%rdx, %rsi
	movq	%rdx, %r11
	movq	%rcx, %r12
	cmovne	%rcx, %rsi
	testl	%r8d, %r8d
	js	.L6
	cmpl	%r8d, 68(%rdi)
	jne	.L7
.L8:
	cmpl	$1, %r10d
	je	.L122
.L16:
	cmpl	$4, %r10d
	je	.L123
	cmpl	$3, %r10d
	je	.L124
	cmpb	$0, -69(%rbp)
	jne	.L125
	cmpl	$2, %r10d
	je	.L126
	.p2align 4,,10
	.p2align 3
.L7:
	cmpq	%rax, %r9
	je	.L119
.L127:
	testq	%rsi, %rsi
	je	.L119
	movq	%rsi, %rax
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L6:
	testl	%r15d, %r15d
	je	.L9
	cmpl	%r15d, 24(%rdi)
	jne	.L7
.L9:
	movl	-52(%rbp), %r14d
	testl	%r14d, %r14d
	je	.L10
	testl	%r14d, 28(%rdi)
	je	.L7
.L10:
	movl	-56(%rbp), %r14d
	testl	%r14d, %r14d
	je	.L11
	testl	%r14d, 32(%rdi)
	je	.L7
.L11:
	movl	-60(%rbp), %r14d
	testl	%r14d, %r14d
	je	.L12
	testl	%r14d, 36(%rdi)
	je	.L7
.L12:
	movl	-64(%rbp), %r14d
	testl	%r14d, %r14d
	je	.L13
	testl	%r14d, 40(%rdi)
	je	.L7
.L13:
	movl	-68(%rbp), %r14d
	testl	%r14d, %r14d
	je	.L14
	cmpl	%r14d, 44(%rdi)
	jne	.L7
.L14:
	movl	-76(%rbp), %r14d
	testl	%r14d, %r14d
	je	.L15
	movl	16(%rbp), %r14d
	andl	60(%rdi), %r14d
	andl	$31, %r14d
	je	.L7
.L15:
	movl	-80(%rbp), %r14d
	testl	%r14d, %r14d
	je	.L8
	movl	16(%rbp), %r14d
	andl	60(%rdi), %r14d
	movl	%r14d, %edi
	andl	$32, %edi
	je	.L7
	cmpl	$1, %r10d
	jne	.L16
	.p2align 4,,10
	.p2align 3
.L122:
	movl	8(%rax), %r14d
	testl	%r14d, %r14d
	jne	.L7
	cmpq	%r13, %rax
	je	.L17
	movq	-48(%rbp), %rdi
	cmpq	%rdi, %rax
	cmove	%rdx, %rdi
	movq	%rdi, -48(%rbp)
	testq	%rcx, %rcx
	je	.L19
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
.L19:
	testq	%rdx, %rdx
	je	.L20
	movq	%rcx, 24(%rdx)
.L20:
	movq	%rax, 16(%r13)
	movq	$0, 16(%rax)
	movq	%r13, 24(%rax)
	movq	%rax, %r13
.L17:
	movl	$1, 8(%rax)
	cmpq	%rax, %r9
	jne	.L127
	.p2align 4,,10
	.p2align 3
.L119:
	movq	-48(%rbp), %r14
.L3:
	movq	40(%rbp), %rax
	movq	%r14, (%rax)
	movq	48(%rbp), %rax
	movq	%r13, (%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore_state
	movl	8(%rax), %r12d
	testl	%r12d, %r12d
	je	.L7
	cmpq	%r13, %rax
	je	.L7
	movq	-48(%rbp), %rdi
	cmpq	%rdi, %rax
	cmove	%rdx, %rdi
	movq	%rdi, -48(%rbp)
	testq	%rcx, %rcx
	je	.L23
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %r11
.L23:
	testq	%r11, %r11
	je	.L24
	movq	%rcx, 24(%r11)
.L24:
	movq	%rax, 16(%r13)
	movq	%r13, 24(%rax)
	movq	%rax, %r13
	movq	$0, 16(%rax)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L38:
	movq	%r13, %r9
	movq	%r14, %rax
	xorl	%ebx, %ebx
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L126:
	cmpq	-48(%rbp), %rax
	je	.L44
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %r11
.L34:
	cmpq	%r13, %rax
	movl	$0, 8(%rax)
	cmove	%rcx, %r13
	testq	%r11, %r11
	je	.L36
	movq	%rcx, 24(%r11)
	movq	24(%rax), %r12
.L36:
	testq	%r12, %r12
	je	.L37
	movq	%r11, 16(%r12)
.L37:
	movups	%xmm0, 16(%rax)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L124:
	movl	8(%rax), %r11d
	testl	%r11d, %r11d
	je	.L7
	cmpq	-48(%rbp), %rax
	je	.L26
	cmpq	%r13, %rax
	cmove	%rcx, %r13
	testq	%rdx, %rdx
	je	.L28
	movq	%rcx, 24(%rdx)
	movq	24(%rax), %rcx
.L28:
	testq	%rcx, %rcx
	je	.L29
	movq	%rdx, 16(%rcx)
.L29:
	movq	-48(%rbp), %rcx
	movq	%rax, -48(%rbp)
	movq	%rax, 24(%rcx)
	movq	%rcx, 16(%rax)
	movq	$0, 24(%rax)
.L26:
	movl	$0, 8(%rax)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L125:
	movl	8(%rax), %edi
	testl	%edi, %edi
	je	.L7
	cmpq	-48(%rbp), %rax
	je	.L7
	cmpq	%r13, %rax
	cmove	%rcx, %r13
	testq	%rdx, %rdx
	je	.L32
	movq	%rcx, 24(%rdx)
	movq	24(%rax), %r12
.L32:
	testq	%r12, %r12
	je	.L33
	movq	%rdx, 16(%r12)
.L33:
	movq	-48(%rbp), %rcx
	movq	%rax, -48(%rbp)
	movq	%rax, 24(%rcx)
	movq	%rcx, 16(%rax)
	movq	$0, 24(%rax)
	jmp	.L7
.L44:
	movq	%rdx, -48(%rbp)
	jmp	.L34
	.cfi_endproc
.LFE1123:
	.size	ssl_cipher_apply_rule, .-ssl_cipher_apply_rule
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/ssl/ssl_ciph.c"
	.text
	.p2align 4
	.type	ciphersuite_cb, @function
ciphersuite_cb:
.LFB1127:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1285, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$79, %esi
	jg	.L135
	movslq	%esi, %rbx
	leaq	-112(%rbp), %r8
	movq	%rdi, %rsi
	movl	$80, %ecx
	movq	%r8, %rdi
	movq	%rdx, %r12
	movq	%rbx, %rdx
	call	__memcpy_chk@PLT
	movb	$0, -112(%rbp,%rbx)
	movq	%rax, %rdi
	call	ssl3_get_cipher_by_std_name@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L136
	movq	%r12, %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L137
	movl	$1, %eax
.L128:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L138
	addq	$112, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	.cfi_restore_state
	movl	$1294, %r8d
.L135:
	movl	$185, %edx
	movl	$622, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L137:
	movl	$1299, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$68, %edx
	movl	%eax, -116(%rbp)
	movl	$622, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	movl	-116(%rbp), %eax
	jmp	.L128
.L138:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1127:
	.size	ciphersuite_cb, .-ciphersuite_cb
	.p2align 4
	.type	ssl_cipher_apply_rule.constprop.0, @function
ssl_cipher_apply_rule.constprop.0:
.LFB1168:
	.cfi_startproc
	movq	(%rcx), %rax
	movq	(%r8), %r9
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r9, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L140
	testq	%r9, %r9
	je	.L140
	testl	%edi, %edi
	jne	.L274
	testl	%esi, %esi
	jne	.L173
	testl	%edx, %edx
	jne	.L174
	.p2align 4,,10
	.p2align 3
.L183:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	movl	8(%rdx), %r12d
	testl	%r12d, %r12d
	jne	.L175
	cmpq	%rdx, %r10
	je	.L180
	movq	24(%rdx), %rsi
	cmpq	%rdx, %rbx
	cmove	%rax, %rbx
	testq	%rsi, %rsi
	je	.L275
	movq	%rax, 16(%rsi)
	movq	16(%rdx), %rdi
.L182:
	testq	%rdi, %rdi
	je	.L181
	movq	%rsi, 24(%rdi)
.L181:
	movq	%rdx, 16(%r10)
	movq	%r10, 24(%rdx)
	movq	$0, 16(%rdx)
.L180:
	movl	$1, 8(%rdx)
	movq	%rdx, %r10
	.p2align 4,,10
	.p2align 3
.L175:
	testq	%rax, %rax
	je	.L140
	cmpq	%rdx, %r9
	jne	.L183
.L140:
	movq	%rbx, (%rcx)
	popq	%rbx
	movq	%r10, (%r8)
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L274:
	.cfi_restore_state
	testl	%edx, %edx
	je	.L142
	testl	%esi, %esi
	jne	.L143
	.p2align 4,,10
	.p2align 3
.L155:
	movq	%rax, %rsi
	movq	16(%rax), %rax
	movq	(%rsi), %r11
	testl	%edi, 28(%r11)
	je	.L144
	testl	%edx, 36(%r11)
	je	.L144
	movl	8(%rsi), %r11d
	testl	%r11d, %r11d
	jne	.L144
	cmpq	%r10, %rsi
	je	.L150
	movq	24(%rsi), %r11
	cmpq	%rbx, %rsi
	cmove	%rax, %rbx
	testq	%r11, %r11
	je	.L276
	movq	%rax, 16(%r11)
	movq	16(%rsi), %r12
.L152:
	testq	%r12, %r12
	je	.L151
	movq	%r11, 24(%r12)
.L151:
	movq	%rsi, 16(%r10)
	movq	%r10, 24(%rsi)
	movq	$0, 16(%rsi)
.L150:
	movl	$1, 8(%rsi)
	movq	%rsi, %r10
	.p2align 4,,10
	.p2align 3
.L144:
	testq	%rax, %rax
	je	.L140
	cmpq	%rsi, %r9
	jne	.L155
	jmp	.L140
.L277:
	testl	%edx, 36(%r12)
	je	.L207
	movl	8(%r11), %r12d
	testl	%r12d, %r12d
	jne	.L207
	cmpq	%r11, %r10
	je	.L208
	movq	24(%r11), %r13
	cmpq	%r11, %rbx
	movq	%rax, %r12
	cmove	%rax, %rbx
	testq	%r13, %r13
	je	.L210
	movq	%rax, 16(%r13)
	movq	16(%r11), %r12
.L210:
	testq	%r12, %r12
	je	.L211
	movq	%r13, 24(%r12)
.L211:
	movq	%r11, 16(%r10)
	movq	%r10, 24(%r11)
	movq	$0, 16(%r11)
.L208:
	movl	$1, 8(%r11)
	movq	%r11, %r10
	.p2align 4,,10
	.p2align 3
.L207:
	cmpq	%r11, %r9
	je	.L140
	testq	%rax, %rax
	je	.L140
.L143:
	movq	%rax, %r11
	movq	16(%rax), %rax
	movq	(%r11), %r12
	testl	%edi, 28(%r12)
	je	.L207
	testl	%esi, 32(%r12)
	jne	.L277
	jmp	.L207
.L278:
	cmpq	%rsi, %r10
	je	.L185
	movq	24(%rsi), %r11
	cmpq	%rbx, %rsi
	movq	%rax, %rdi
	cmove	%rax, %rbx
	testq	%r11, %r11
	je	.L187
	movq	%rax, 16(%r11)
	movq	16(%rsi), %rdi
.L187:
	testq	%rdi, %rdi
	je	.L188
	movq	%r11, 24(%rdi)
.L188:
	movq	%rsi, 16(%r10)
	movq	%r10, 24(%rsi)
	movq	$0, 16(%rsi)
.L185:
	movl	$1, 8(%rsi)
	movq	%rsi, %r10
	.p2align 4,,10
	.p2align 3
.L184:
	testq	%rax, %rax
	je	.L140
	cmpq	%rsi, %r9
	je	.L140
.L174:
	movq	%rax, %rsi
	movq	16(%rax), %rax
	movq	(%rsi), %rdi
	testl	%edx, 36(%rdi)
	je	.L184
	movl	8(%rsi), %r11d
	testl	%r11d, %r11d
	jne	.L184
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L173:
	testl	%edx, %edx
	jne	.L189
	.p2align 4,,10
	.p2align 3
.L195:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	movq	(%rdx), %rdi
	testl	%esi, 32(%rdi)
	je	.L190
	movl	8(%rdx), %edi
	testl	%edi, %edi
	jne	.L190
	cmpq	%r10, %rdx
	je	.L191
	movq	24(%rdx), %r11
	cmpq	%rbx, %rdx
	movq	%rax, %rdi
	cmove	%rax, %rbx
	testq	%r11, %r11
	je	.L193
	movq	%rax, 16(%r11)
	movq	16(%rdx), %rdi
.L193:
	testq	%rdi, %rdi
	je	.L194
	movq	%r11, 24(%rdi)
.L194:
	movq	%rdx, 16(%r10)
	movq	%r10, 24(%rdx)
	movq	$0, 16(%rdx)
.L191:
	movl	$1, 8(%rdx)
	movq	%rdx, %r10
	.p2align 4,,10
	.p2align 3
.L190:
	testq	%rax, %rax
	je	.L140
	cmpq	%rdx, %r9
	jne	.L195
	jmp	.L140
.L205:
	movl	8(%rdi), %r13d
	testl	%r13d, %r13d
	jne	.L196
	cmpq	%rdi, %r10
	je	.L202
	movq	24(%rdi), %r11
	cmpq	%rdi, %rbx
	cmove	%rax, %rbx
	testq	%r11, %r11
	je	.L279
	movq	%rax, 16(%r11)
	movq	16(%rdi), %r12
.L204:
	testq	%r12, %r12
	je	.L203
	movq	%r11, 24(%r12)
.L203:
	movq	%rdi, 16(%r10)
	movq	%r10, 24(%rdi)
	movq	$0, 16(%rdi)
.L202:
	movl	$1, 8(%rdi)
	movq	%rdi, %r10
	.p2align 4,,10
	.p2align 3
.L196:
	testq	%rax, %rax
	je	.L140
	cmpq	%rdi, %r9
	je	.L140
.L189:
	movq	%rax, %rdi
	movq	16(%rax), %rax
	movq	(%rdi), %r11
	testl	%esi, 32(%r11)
	je	.L196
	testl	%edx, 36(%r11)
	jne	.L205
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L142:
	testl	%esi, %esi
	jne	.L156
	.p2align 4,,10
	.p2align 3
.L162:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	movq	(%rdx), %rsi
	testl	%edi, 28(%rsi)
	je	.L157
	movl	8(%rdx), %esi
	testl	%esi, %esi
	jne	.L157
	cmpq	%r10, %rdx
	je	.L158
	movq	24(%rdx), %r11
	cmpq	%rbx, %rdx
	movq	%rax, %rsi
	cmove	%rax, %rbx
	testq	%r11, %r11
	je	.L160
	movq	%rax, 16(%r11)
	movq	16(%rdx), %rsi
.L160:
	testq	%rsi, %rsi
	je	.L161
	movq	%r11, 24(%rsi)
.L161:
	movq	%rdx, 16(%r10)
	movq	%r10, 24(%rdx)
	movq	$0, 16(%rdx)
.L158:
	movl	$1, 8(%rdx)
	movq	%rdx, %r10
	.p2align 4,,10
	.p2align 3
.L157:
	testq	%rax, %rax
	je	.L140
	cmpq	%rdx, %r9
	jne	.L162
	jmp	.L140
.L172:
	movl	8(%rdx), %r13d
	testl	%r13d, %r13d
	jne	.L163
	cmpq	%r10, %rdx
	je	.L169
	movq	24(%rdx), %r11
	cmpq	%rbx, %rdx
	cmove	%rax, %rbx
	testq	%r11, %r11
	je	.L280
	movq	%rax, 16(%r11)
	movq	16(%rdx), %r12
.L171:
	testq	%r12, %r12
	je	.L170
	movq	%r11, 24(%r12)
.L170:
	movq	%rdx, 16(%r10)
	movq	%r10, 24(%rdx)
	movq	$0, 16(%rdx)
.L169:
	movl	$1, 8(%rdx)
	movq	%rdx, %r10
	.p2align 4,,10
	.p2align 3
.L163:
	testq	%rax, %rax
	je	.L140
	cmpq	%rdx, %r9
	je	.L140
.L156:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	movq	(%rdx), %r11
	testl	%edi, 28(%r11)
	je	.L163
	testl	%esi, 32(%r11)
	jne	.L172
	jmp	.L163
.L279:
	movq	%rax, %r12
	jmp	.L204
.L280:
	movq	%rax, %r12
	jmp	.L171
.L275:
	movq	%rax, %rdi
	jmp	.L182
.L276:
	movq	%rax, %r12
	jmp	.L152
	.cfi_endproc
.LFE1168:
	.size	ssl_cipher_apply_rule.constprop.0, .-ssl_cipher_apply_rule.constprop.0
	.p2align 4
	.type	ssl_cipher_apply_rule.constprop.1, @function
ssl_cipher_apply_rule.constprop.1:
.LFB1167:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%r9), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rax, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	16(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	0(%r13), %rbx
	movq	%rbx, %r12
	testq	%rbx, %rbx
	je	.L282
	.p2align 4,,10
	.p2align 3
.L325:
	testq	%rax, %rax
	je	.L282
	movq	%rax, %r10
	movq	16(%rax), %rax
	movq	(%r10), %r11
	testl	%r8d, %r8d
	js	.L284
	cmpl	68(%r11), %r8d
	jne	.L285
.L286:
	movl	8(%r10), %r11d
	testl	%r11d, %r11d
	je	.L285
	cmpq	%r10, %r12
	je	.L285
	movq	24(%r10), %r15
	cmpq	%r10, %r14
	movq	%rax, %r11
	cmove	%rax, %r14
	testq	%r15, %r15
	je	.L291
	movq	%rax, 16(%r15)
	movq	16(%r10), %r11
.L291:
	testq	%r11, %r11
	je	.L292
	movq	%r15, 24(%r11)
.L292:
	movq	%r10, 16(%r12)
	movq	$0, 16(%r10)
	movq	%r12, 24(%r10)
	movq	%r10, %r12
	.p2align 4,,10
	.p2align 3
.L285:
	cmpq	%r10, %rbx
	jne	.L325
.L282:
	movq	%r14, (%r9)
	movq	%r12, 0(%r13)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L284:
	.cfi_restore_state
	testl	%edi, %edi
	je	.L287
	testl	%edi, 28(%r11)
	je	.L285
.L287:
	testl	%esi, %esi
	je	.L288
	testl	%esi, 32(%r11)
	je	.L285
.L288:
	testl	%edx, %edx
	je	.L289
	testl	%edx, 36(%r11)
	je	.L285
.L289:
	testl	%ecx, %ecx
	je	.L286
	testl	%ecx, 40(%r11)
	jne	.L286
	cmpq	%r10, %rbx
	jne	.L325
	jmp	.L282
	.cfi_endproc
.LFE1167:
	.size	ssl_cipher_apply_rule.constprop.1, .-ssl_cipher_apply_rule.constprop.1
	.p2align 4
	.type	ssl_cipher_strength_sort, @function
ssl_cipher_strength_sort:
.LFB1124:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L339
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L330:
	movl	8(%rax), %edi
	testl	%edi, %edi
	je	.L329
	movq	(%rax), %rdx
	movslq	68(%rdx), %rdx
	cmpl	%edx, %ebx
	cmovl	%rdx, %rbx
.L329:
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L330
	leal	1(%rbx), %edi
	movslq	%edi, %rdi
	salq	$2, %rdi
.L328:
	movl	$929, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L347
	movq	0(%r13), %rdx
	testq	%rdx, %rdx
	je	.L334
	.p2align 4,,10
	.p2align 3
.L333:
	movl	8(%rdx), %esi
	testl	%esi, %esi
	je	.L335
	movq	(%rdx), %rax
	movq	16(%rdx), %rdx
	movslq	68(%rax), %rax
	addl	$1, (%r14,%rax,4)
	testq	%rdx, %rdx
	jne	.L333
	.p2align 4,,10
	.p2align 3
.L334:
	movl	(%r14,%rbx,4), %ecx
	testl	%ecx, %ecx
	jle	.L338
	subq	$8, %rsp
	xorl	%edx, %edx
	movq	%r13, %r9
	movl	%ebx, %r8d
	pushq	%r12
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	ssl_cipher_apply_rule.constprop.1
	popq	%rax
	popq	%rdx
.L338:
	subq	$1, %rbx
	cmpl	$-1, %ebx
	jne	.L334
	movl	$953, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	movl	$1, %eax
.L327:
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L335:
	.cfi_restore_state
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L333
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L339:
	movl	$4, %edi
	xorl	%ebx, %ebx
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L347:
	movl	$931, %r8d
	movl	$65, %edx
	movl	$231, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L327
	.cfi_endproc
.LFE1124:
	.size	ssl_cipher_strength_sort, .-ssl_cipher_strength_sort
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"STRENGTH"
.LC2:
	.string	"SECLEVEL="
	.text
	.p2align 4
	.type	ssl_cipher_process_rulestr.isra.0, @function
ssl_cipher_process_rulestr.isra.0:
.LFB1162:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movzbl	(%rdi), %eax
	movq	%rsi, -112(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%r8, -128(%rbp)
	testb	%al, %al
	je	.L396
	movl	$1, -100(%rbp)
	movq	%rdi, %r15
	jmp	.L350
.L351:
	cmpb	$43, %al
	je	.L479
	cmpb	$33, %al
	je	.L480
	cmpb	$64, %al
	je	.L481
	cmpb	$58, %al
	je	.L357
	cmpb	$32, %al
	jne	.L482
.L357:
	movzbl	1(%r15), %eax
	leaq	1(%r15), %r14
	testb	%al, %al
	je	.L348
.L359:
	movq	%r14, %r15
.L350:
	cmpb	$45, %al
	jne	.L351
	movl	$3, -68(%rbp)
	addq	$1, %r15
.L352:
	movl	$0, -96(%rbp)
	movq	%r15, %rsi
	movl	$0, -56(%rbp)
	movl	$0, -92(%rbp)
	movl	$0, -88(%rbp)
	movl	$0, -84(%rbp)
	movl	$0, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L383:
	movzbl	(%rsi), %edx
	movq	%rsi, %r14
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L360:
	movl	%edx, %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$25, %al
	jbe	.L361
	cmpb	$61, %dl
	jbe	.L483
.L362:
	testl	%ecx, %ecx
	je	.L484
	cmpl	$5, -68(%rbp)
	je	.L366
	movl	$0, -52(%rbp)
	cmpb	$43, %dl
	jne	.L367
	movl	$1, -52(%rbp)
	addq	$1, %r14
.L367:
	movq	-80(%rbp), %rax
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L478
	movslq	%ecx, %r12
	movq	%r14, -64(%rbp)
	leaq	8(%rax), %rbx
	movq	%r12, %r14
	movq	%rsi, %r12
	.p2align 4,,10
	.p2align 3
.L370:
	movq	8(%r13), %r15
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	strncmp@PLT
	testl	%eax, %eax
	jne	.L368
	cmpb	$0, (%r15,%r14)
	je	.L369
.L368:
	movq	(%rbx), %r13
	addq	$8, %rbx
	testq	%r13, %r13
	jne	.L370
	movq	-64(%rbp), %r14
	jmp	.L478
.L485:
	cmpb	$59, %al
	ja	.L390
	movabsq	$864708724936146944, %rsi
	btq	%rax, %rsi
	jc	.L359
.L390:
	addq	$1, %r14
.L478:
	movzbl	(%r14), %eax
	testb	%al, %al
	jne	.L485
.L348:
	movl	-100(%rbp), %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L483:
	.cfi_restore_state
	movabsq	$2593897463504961536, %rax
	btq	%rdx, %rax
	jnc	.L362
	.p2align 4,,10
	.p2align 3
.L361:
	addq	$1, %r14
	addl	$1, %ecx
	movzbl	(%r14), %edx
	jmp	.L360
.L369:
	movl	28(%r13), %eax
	movq	-64(%rbp), %r14
	testl	%eax, %eax
	je	.L371
	movl	-72(%rbp), %edi
	testl	%edi, %edi
	je	.L399
	andl	%eax, %edi
	movl	%edi, -72(%rbp)
	je	.L478
.L371:
	movl	32(%r13), %eax
	testl	%eax, %eax
	je	.L372
	movl	-84(%rbp), %esi
	testl	%esi, %esi
	je	.L400
	andl	%eax, %esi
	movl	%esi, -84(%rbp)
	je	.L478
.L372:
	movl	36(%r13), %eax
	testl	%eax, %eax
	je	.L373
	movl	-88(%rbp), %edi
	testl	%edi, %edi
	je	.L401
	andl	%eax, %edi
	movl	%edi, -88(%rbp)
	je	.L478
.L373:
	movl	40(%r13), %eax
	testl	%eax, %eax
	je	.L374
	movl	-92(%rbp), %esi
	testl	%esi, %esi
	je	.L402
	andl	%eax, %esi
	movl	%esi, -92(%rbp)
	je	.L478
.L374:
	movl	60(%r13), %eax
	movl	%eax, %edx
	andl	$31, %edx
	je	.L375
	movl	-56(%rbp), %esi
	testb	$31, %sil
	je	.L376
	movl	%eax, %edx
	orl	$-32, %edx
	andl	%edx, %esi
	movl	%esi, -56(%rbp)
	andl	$31, %esi
	je	.L478
.L375:
	movl	%eax, %edx
	andl	$32, %edx
	je	.L377
	movl	-56(%rbp), %edi
	testb	$32, %dil
	je	.L378
	orl	$-33, %eax
	andl	%eax, %edi
	movl	%edi, -56(%rbp)
	andl	$32, %edi
	je	.L478
.L377:
	movl	0(%r13), %edi
	testl	%edi, %edi
	je	.L379
	movl	24(%r13), %edi
.L380:
	movl	-52(%rbp), %eax
	testl	%eax, %eax
	je	.L382
.L486:
	movq	%r14, %rsi
	jmp	.L383
.L379:
	movl	44(%r13), %eax
	testl	%eax, %eax
	je	.L380
	movl	-96(%rbp), %esi
	testl	%esi, %esi
	je	.L404
	cmpl	%esi, %eax
	jne	.L478
.L404:
	movl	%eax, -96(%rbp)
	movl	-52(%rbp), %eax
	testl	%eax, %eax
	jne	.L486
.L382:
	movl	-68(%rbp), %eax
	subq	$8, %rsp
	movl	-96(%rbp), %r9d
	pushq	-120(%rbp)
	movl	-92(%rbp), %r8d
	pushq	-112(%rbp)
	movl	-88(%rbp), %ecx
	pushq	$-1
	movl	-84(%rbp), %edx
	movl	-72(%rbp), %esi
	pushq	%rax
	movl	-56(%rbp), %eax
	pushq	%rax
	call	ssl_cipher_apply_rule
	movzbl	(%r14), %eax
	addq	$48, %rsp
.L389:
	testb	%al, %al
	jne	.L359
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L401:
	movl	%eax, -88(%rbp)
	jmp	.L373
.L402:
	movl	%eax, -92(%rbp)
	jmp	.L374
.L399:
	movl	%eax, -72(%rbp)
	jmp	.L371
.L400:
	movl	%eax, -84(%rbp)
	jmp	.L372
.L376:
	andl	$32, %eax
	movl	%edx, -56(%rbp)
	movl	%eax, %edx
	je	.L377
.L378:
	orl	%edx, -56(%rbp)
	jmp	.L377
.L482:
	cmpb	$59, %al
	je	.L357
	cmpb	$44, %al
	je	.L357
	movl	$1, -68(%rbp)
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L479:
	movl	$4, -68(%rbp)
	addq	$1, %r15
	jmp	.L352
.L480:
	movl	$2, -68(%rbp)
	addq	$1, %r15
	jmp	.L352
.L481:
	movl	$5, -68(%rbp)
	addq	$1, %r15
	jmp	.L352
.L366:
	movq	%rsi, %r15
	cmpl	$8, %ecx
	je	.L487
	cmpl	$10, %ecx
	jne	.L364
	movl	$9, %ecx
	leaq	.LC2(%rip), %rdi
	movq	%r15, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L488
.L364:
	movl	$1193, %r8d
.L477:
	movl	$280, %edx
	movl	$230, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$0, -100(%rbp)
.L384:
	movzbl	(%r14), %eax
	testb	%al, %al
	jne	.L386
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L388:
	movzbl	1(%r14), %eax
	addq	$1, %r14
	testb	%al, %al
	je	.L348
.L386:
	cmpb	$59, %al
	ja	.L388
	movabsq	$864708724936146944, %rsi
	btq	%rax, %rsi
	jnc	.L388
	movzbl	(%r14), %eax
	jmp	.L389
.L484:
	movl	$1028, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$280, %edx
	movl	$230, %esi
	movl	$20, %edi
	addq	$1, %r14
	call	ERR_put_error@PLT
	cmpl	$5, -68(%rbp)
	je	.L364
	movl	$0, -100(%rbp)
	jmp	.L478
.L487:
	movl	$8, %ecx
	leaq	.LC1(%rip), %rdi
	movq	%r15, %rsi
	repz cmpsb
	seta	%bl
	sbbb	$0, %bl
	movsbl	%bl, %ebx
	testl	%ebx, %ebx
	jne	.L364
	movq	-120(%rbp), %rsi
	movq	-112(%rbp), %rdi
	call	ssl_cipher_strength_sort
	testl	%eax, %eax
	cmovne	-100(%rbp), %ebx
	movl	%ebx, -100(%rbp)
	jmp	.L384
.L488:
	movsbl	9(%r15), %eax
	movl	$1186, %r8d
	subl	$48, %eax
	cmpl	$5, %eax
	ja	.L477
	movq	-128(%rbp), %rsi
	movl	%eax, (%rsi)
	jmp	.L384
.L396:
	movl	$1, -100(%rbp)
	jmp	.L348
	.cfi_endproc
.LFE1162:
	.size	ssl_cipher_process_rulestr.isra.0, .-ssl_cipher_process_rulestr.isra.0
	.p2align 4
	.type	ssl_cipher_apply_rule.constprop.2, @function
ssl_cipher_apply_rule.constprop.2:
.LFB1166:
	.cfi_startproc
	movq	(%rcx), %r9
	movq	(%r8), %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r9, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rax, %rbx
	testq	%r9, %r9
	je	.L490
	testq	%rax, %rax
	je	.L490
	testl	%edi, %edi
	jne	.L606
	testl	%esi, %esi
	jne	.L518
	testl	%edx, %edx
	jne	.L519
	.p2align 4,,10
	.p2align 3
.L526:
	movq	%rax, %rdx
	movq	24(%rax), %rax
	movl	8(%rdx), %r12d
	testl	%r12d, %r12d
	je	.L520
	cmpq	%rdx, %r11
	je	.L520
	movq	16(%rdx), %rdi
	cmpq	%rdx, %rbx
	cmove	%rax, %rbx
	testq	%rdi, %rdi
	je	.L607
	movq	%rax, 24(%rdi)
	movq	24(%rdx), %rsi
.L525:
	testq	%rsi, %rsi
	je	.L524
	movq	%rdi, 16(%rsi)
.L524:
	movq	%rdx, 24(%r11)
	movq	$0, 24(%rdx)
	movq	%r11, 16(%rdx)
	movq	%rdx, %r11
	.p2align 4,,10
	.p2align 3
.L520:
	testq	%rax, %rax
	je	.L490
	cmpq	%rdx, %r9
	jne	.L526
.L490:
	movq	%r11, (%rcx)
	movq	%rbx, (%r8)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L606:
	.cfi_restore_state
	testl	%edx, %edx
	je	.L492
	testl	%esi, %esi
	jne	.L493
	.p2align 4,,10
	.p2align 3
.L503:
	movq	%rax, %rsi
	movq	24(%rax), %rax
	movq	(%rsi), %r10
	testl	%edi, 28(%r10)
	je	.L494
	cmpl	44(%r10), %edx
	jne	.L494
	movl	8(%rsi), %r10d
	testl	%r10d, %r10d
	je	.L494
	cmpq	%r11, %rsi
	je	.L494
	movq	16(%rsi), %r12
	cmpq	%rbx, %rsi
	cmove	%rax, %rbx
	testq	%r12, %r12
	je	.L608
	movq	%rax, 24(%r12)
	movq	24(%rsi), %r10
.L500:
	testq	%r10, %r10
	je	.L499
	movq	%r12, 16(%r10)
.L499:
	movq	%rsi, 24(%r11)
	movq	$0, 24(%rsi)
	movq	%r11, 16(%rsi)
	movq	%rsi, %r11
	.p2align 4,,10
	.p2align 3
.L494:
	testq	%rax, %rax
	je	.L490
	cmpq	%rsi, %r9
	jne	.L503
	jmp	.L490
.L609:
	cmpl	44(%r12), %edx
	jne	.L546
	movl	8(%r10), %r12d
	testl	%r12d, %r12d
	je	.L546
	cmpq	%r11, %r10
	je	.L546
	movq	16(%r10), %r13
	cmpq	%rbx, %r10
	movq	%rax, %r12
	cmove	%rax, %rbx
	testq	%r13, %r13
	je	.L548
	movq	%rax, 24(%r13)
	movq	24(%r10), %r12
.L548:
	testq	%r12, %r12
	je	.L549
	movq	%r13, 16(%r12)
.L549:
	movq	%r10, 24(%r11)
	movq	$0, 24(%r10)
	movq	%r11, 16(%r10)
	movq	%r10, %r11
	.p2align 4,,10
	.p2align 3
.L546:
	cmpq	%r9, %r10
	je	.L490
	testq	%rax, %rax
	je	.L490
.L493:
	movq	%rax, %r10
	movq	24(%rax), %rax
	movq	(%r10), %r12
	testl	%edi, 28(%r12)
	je	.L546
	testl	%esi, 40(%r12)
	jne	.L609
	jmp	.L546
.L610:
	cmpq	%rsi, %r11
	je	.L527
	movq	16(%rsi), %r10
	cmpq	%rsi, %rbx
	movq	%rax, %rdi
	cmove	%rax, %rbx
	testq	%r10, %r10
	je	.L529
	movq	%rax, 24(%r10)
	movq	24(%rsi), %rdi
.L529:
	testq	%rdi, %rdi
	je	.L530
	movq	%r10, 16(%rdi)
.L530:
	movq	%rsi, 24(%r11)
	movq	$0, 24(%rsi)
	movq	%r11, 16(%rsi)
	movq	%rsi, %r11
	.p2align 4,,10
	.p2align 3
.L527:
	testq	%rax, %rax
	je	.L490
	cmpq	%rsi, %r9
	je	.L490
.L519:
	movq	%rax, %rsi
	movq	24(%rax), %rax
	movq	(%rsi), %rdi
	cmpl	44(%rdi), %edx
	jne	.L527
	movl	8(%rsi), %r10d
	testl	%r10d, %r10d
	jne	.L610
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L518:
	testl	%edx, %edx
	jne	.L531
	.p2align 4,,10
	.p2align 3
.L536:
	movq	%rax, %rdx
	movq	24(%rax), %rax
	movq	(%rdx), %rdi
	testl	%esi, 40(%rdi)
	je	.L532
	movl	8(%rdx), %edi
	testl	%edi, %edi
	je	.L532
	cmpq	%r11, %rdx
	je	.L532
	movq	16(%rdx), %r10
	cmpq	%rbx, %rdx
	movq	%rax, %rdi
	cmove	%rax, %rbx
	testq	%r10, %r10
	je	.L534
	movq	%rax, 24(%r10)
	movq	24(%rdx), %rdi
.L534:
	testq	%rdi, %rdi
	je	.L535
	movq	%r10, 16(%rdi)
.L535:
	movq	%rdx, 24(%r11)
	movq	$0, 24(%rdx)
	movq	%r11, 16(%rdx)
	movq	%rdx, %r11
	.p2align 4,,10
	.p2align 3
.L532:
	testq	%rax, %rax
	je	.L490
	cmpq	%rdx, %r9
	jne	.L536
	jmp	.L490
.L544:
	movl	8(%rdi), %r13d
	testl	%r13d, %r13d
	je	.L537
	cmpq	%rdi, %r11
	je	.L537
	movq	16(%rdi), %r12
	cmpq	%rdi, %rbx
	cmove	%rax, %rbx
	testq	%r12, %r12
	je	.L611
	movq	%rax, 24(%r12)
	movq	24(%rdi), %r10
.L543:
	testq	%r10, %r10
	je	.L542
	movq	%r12, 16(%r10)
.L542:
	movq	%rdi, 24(%r11)
	movq	$0, 24(%rdi)
	movq	%r11, 16(%rdi)
	movq	%rdi, %r11
	.p2align 4,,10
	.p2align 3
.L537:
	cmpq	%rdi, %r9
	je	.L490
	testq	%rax, %rax
	je	.L490
.L531:
	movq	%rax, %rdi
	movq	24(%rax), %rax
	movq	(%rdi), %r10
	testl	%esi, 40(%r10)
	je	.L537
	cmpl	44(%r10), %edx
	je	.L544
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L492:
	testl	%esi, %esi
	jne	.L504
	.p2align 4,,10
	.p2align 3
.L509:
	movq	%rax, %rdx
	movq	24(%rax), %rax
	movq	(%rdx), %rsi
	testl	%edi, 28(%rsi)
	je	.L505
	movl	8(%rdx), %esi
	testl	%esi, %esi
	je	.L505
	cmpq	%r11, %rdx
	je	.L505
	movq	16(%rdx), %r10
	cmpq	%rbx, %rdx
	movq	%rax, %rsi
	cmove	%rax, %rbx
	testq	%r10, %r10
	je	.L507
	movq	%rax, 24(%r10)
	movq	24(%rdx), %rsi
.L507:
	testq	%rsi, %rsi
	je	.L508
	movq	%r10, 16(%rsi)
.L508:
	movq	%rdx, 24(%r11)
	movq	$0, 24(%rdx)
	movq	%r11, 16(%rdx)
	movq	%rdx, %r11
	.p2align 4,,10
	.p2align 3
.L505:
	testq	%rax, %rax
	je	.L490
	cmpq	%rdx, %r9
	jne	.L509
	jmp	.L490
.L517:
	movl	8(%rdx), %r13d
	testl	%r13d, %r13d
	je	.L510
	cmpq	%r11, %rdx
	je	.L510
	movq	16(%rdx), %r12
	cmpq	%rbx, %rdx
	cmove	%rax, %rbx
	testq	%r12, %r12
	je	.L612
	movq	%rax, 24(%r12)
	movq	24(%rdx), %r10
.L516:
	testq	%r10, %r10
	je	.L515
	movq	%r12, 16(%r10)
.L515:
	movq	%rdx, 24(%r11)
	movq	$0, 24(%rdx)
	movq	%r11, 16(%rdx)
	movq	%rdx, %r11
	.p2align 4,,10
	.p2align 3
.L510:
	testq	%rax, %rax
	je	.L490
	cmpq	%rdx, %r9
	je	.L490
.L504:
	movq	%rax, %rdx
	movq	24(%rax), %rax
	movq	(%rdx), %r10
	testl	%edi, 28(%r10)
	je	.L510
	testl	%esi, 40(%r10)
	jne	.L517
	jmp	.L510
.L607:
	movq	%rax, %rsi
	jmp	.L525
.L612:
	movq	%rax, %r10
	jmp	.L516
.L608:
	movq	%rax, %r10
	jmp	.L500
.L611:
	movq	%rax, %r10
	jmp	.L543
	.cfi_endproc
.LFE1166:
	.size	ssl_cipher_apply_rule.constprop.2, .-ssl_cipher_apply_rule.constprop.2
	.p2align 4
	.type	update_cipher_list, @function
update_cipher_list:
.LFB1130:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	call	OPENSSL_sk_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L614
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L616:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	cmpl	$772, 44(%rax)
	jne	.L619
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_delete@PLT
.L614:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jg	.L616
.L619:
	xorl	%ebx, %ebx
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L620:
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movl	%ebx, %edx
	movq	%r12, %rdi
	addl	$1, %ebx
	movq	%rax, %rsi
	call	OPENSSL_sk_insert@PLT
.L617:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L620
	movq	%r12, %rdi
	call	OPENSSL_sk_dup@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L621
	movq	(%r15), %rdi
	call	OPENSSL_sk_free@PLT
	movq	%r13, (%r15)
	movq	ssl_cipher_ptr_id_cmp@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_set_cmp_func@PLT
	movq	(%r15), %rdi
	call	OPENSSL_sk_sort@PLT
	movq	(%r14), %rdi
	call	OPENSSL_sk_free@PLT
	movq	%r12, (%r14)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L621:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1130:
	.size	update_cipher_list, .-update_cipher_list
	.section	.rodata.str1.1
.LC3:
	.string	"gost-mac"
.LC4:
	.string	"gost-mac-12"
.LC5:
	.string	"gost2001"
.LC6:
	.string	"gost2012_256"
.LC7:
	.string	"gost2012_512"
	.text
	.p2align 4
	.globl	ssl_load_ciphers
	.type	ssl_load_ciphers, @function
ssl_load_ciphers:
.LFB1114:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	ssl_cipher_methods(%rip), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	ssl_cipher_table_cipher(%rip), %rbx
	leaq	176(%rbx), %r13
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, disabled_enc_mask(%rip)
	call	ssl_sort_cipher_list@PLT
	jmp	.L631
	.p2align 4,,10
	.p2align 3
.L667:
	movq	$0, (%r12)
.L630:
	addq	$8, %rbx
	addq	$8, %r12
	cmpq	%r13, %rbx
	je	.L666
.L631:
	movl	4(%rbx), %edi
	testl	%edi, %edi
	je	.L667
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_cipherbyname@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	jne	.L630
	movl	(%rbx), %eax
	addq	$8, %rbx
	orl	%eax, disabled_enc_mask(%rip)
	addq	$8, %r12
	cmpq	%r13, %rbx
	jne	.L631
	.p2align 4,,10
	.p2align 3
.L666:
	leaq	4+ssl_cipher_table_mac(%rip), %r13
	xorl	%ebx, %ebx
	movl	$0, disabled_mac_mask(%rip)
	leaq	ssl_digest_methods(%rip), %r12
	leaq	ssl_mac_secret_size(%rip), %r14
	leaq	-4(%r13), %r15
	jmp	.L636
	.p2align 4,,10
	.p2align 3
.L632:
	call	EVP_MD_size@PLT
	testl	%eax, %eax
	js	.L637
	cltq
	movq	%rax, (%r14,%rbx,8)
	addq	$1, %rbx
	cmpq	$12, %rbx
	je	.L668
.L636:
	movl	0(%r13,%rbx,8), %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	movq	%rax, (%r12,%rbx,8)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L632
	movl	(%r15,%rbx,8), %eax
	addq	$1, %rbx
	orl	%eax, disabled_mac_mask(%rip)
	cmpq	$12, %rbx
	jne	.L636
.L668:
	cmpq	$0, ssl_digest_methods(%rip)
	je	.L637
	cmpq	$0, 8+ssl_digest_methods(%rip)
	je	.L637
	leaq	-64(%rbp), %r12
	movl	$-1, %edx
	leaq	.LC3(%rip), %rsi
	movl	$0, disabled_mkey_mask(%rip)
	movq	%r12, %rdi
	movq	$0, -64(%rbp)
	movl	$0, disabled_auth_mask(%rip)
	movl	$0, -68(%rbp)
	call	EVP_PKEY_asn1_find_str@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L639
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	-68(%rbp), %rdi
	call	EVP_PKEY_asn1_get0_info@PLT
	testl	%eax, %eax
	jle	.L669
.L639:
	movq	-64(%rbp), %rdi
	call	ENGINE_finish@PLT
	movl	-68(%rbp), %eax
	movl	%eax, 12+ssl_mac_pkey_id(%rip)
	testl	%eax, %eax
	je	.L641
	movq	$32, 24+ssl_mac_secret_size(%rip)
.L642:
	movl	$-1, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	movq	$0, -64(%rbp)
	movl	$0, -68(%rbp)
	call	EVP_PKEY_asn1_find_str@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L644
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	-68(%rbp), %rdi
	call	EVP_PKEY_asn1_get0_info@PLT
	testl	%eax, %eax
	jle	.L670
.L644:
	movq	-64(%rbp), %rdi
	call	ENGINE_finish@PLT
	movl	-68(%rbp), %eax
	movl	%eax, 28+ssl_mac_pkey_id(%rip)
	testl	%eax, %eax
	je	.L646
	movq	$32, 56+ssl_mac_secret_size(%rip)
.L647:
	movl	$-1, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	movq	$0, -64(%rbp)
	movl	$0, -68(%rbp)
	call	EVP_PKEY_asn1_find_str@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L649
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	-68(%rbp), %rdi
	call	EVP_PKEY_asn1_get0_info@PLT
	testl	%eax, %eax
	jle	.L671
.L649:
	movq	-64(%rbp), %rdi
	call	ENGINE_finish@PLT
	movl	-68(%rbp), %esi
	testl	%esi, %esi
	jne	.L651
	orl	$160, disabled_auth_mask(%rip)
.L651:
	movl	$-1, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	movq	$0, -64(%rbp)
	movl	$0, -68(%rbp)
	call	EVP_PKEY_asn1_find_str@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L653
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	-68(%rbp), %rdi
	call	EVP_PKEY_asn1_get0_info@PLT
	testl	%eax, %eax
	jle	.L672
.L653:
	movq	-64(%rbp), %rdi
	call	ENGINE_finish@PLT
	movl	-68(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L655
	orl	$128, disabled_auth_mask(%rip)
.L655:
	movl	$-1, %edx
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	movq	$0, -64(%rbp)
	movl	$0, -68(%rbp)
	call	EVP_PKEY_asn1_find_str@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L657
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	-68(%rbp), %rdi
	call	EVP_PKEY_asn1_get0_info@PLT
	testl	%eax, %eax
	jle	.L673
.L657:
	movq	-64(%rbp), %rdi
	call	ENGINE_finish@PLT
	movl	-68(%rbp), %edx
	movl	disabled_auth_mask(%rip), %eax
	testl	%edx, %edx
	jne	.L660
	orb	$-128, %al
	movl	%eax, disabled_auth_mask(%rip)
.L660:
	andl	$160, %eax
	movl	$1, %r8d
	cmpl	$160, %eax
	jne	.L628
	orl	$16, disabled_mkey_mask(%rip)
	jmp	.L628
	.p2align 4,,10
	.p2align 3
.L637:
	xorl	%r8d, %r8d
.L628:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L674
	addq	$40, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L646:
	.cfi_restore_state
	orl	$256, disabled_mac_mask(%rip)
	jmp	.L647
.L641:
	orl	$8, disabled_mac_mask(%rip)
	jmp	.L642
.L673:
	movl	$0, -68(%rbp)
	jmp	.L657
.L672:
	movl	$0, -68(%rbp)
	jmp	.L653
.L671:
	movl	$0, -68(%rbp)
	jmp	.L649
.L670:
	movl	$0, -68(%rbp)
	jmp	.L644
.L669:
	movl	$0, -68(%rbp)
	jmp	.L639
.L674:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1114:
	.size	ssl_load_ciphers, .-ssl_load_ciphers
	.section	.rodata.str1.1
.LC8:
	.string	"RC4-HMAC-MD5"
.LC9:
	.string	"AES-128-CBC-HMAC-SHA1"
.LC10:
	.string	"AES-256-CBC-HMAC-SHA1"
.LC11:
	.string	"AES-128-CBC-HMAC-SHA256"
.LC12:
	.string	"AES-256-CBC-HMAC-SHA256"
	.text
	.p2align 4
	.globl	ssl_cipher_get_evp
	.type	ssl_cipher_get_evp, @function
ssl_cipher_get_evp:
.LFB1115:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	504(%rdi), %r15
	testq	%r15, %r15
	je	.L680
	movq	%rdi, %r14
	movq	%rsi, %rbx
	movq	%rdx, %r12
	movq	%rcx, %r13
	testq	%r9, %r9
	je	.L678
	movq	%rsi, %rax
	movq	$0, (%r9)
	orq	%rdx, %rax
	je	.L757
.L678:
	testq	%rbx, %rbx
	je	.L680
	testq	%r12, %r12
	je	.L680
	movl	36(%r15), %ecx
	xorl	%eax, %eax
	leaq	ssl_cipher_table_cipher(%rip), %rdx
	.p2align 4,,10
	.p2align 3
.L684:
	cmpl	(%rdx,%rax,8), %ecx
	je	.L758
	addq	$1, %rax
	cmpq	$22, %rax
	jne	.L684
	movq	$0, (%rbx)
	xorl	%edi, %edi
	jmp	.L686
	.p2align 4,,10
	.p2align 3
.L758:
	cmpq	$5, %rax
	je	.L759
	cltq
	leaq	ssl_cipher_methods(%rip), %rdx
	movq	(%rdx,%rax,8), %rdi
	movq	%rdi, (%rbx)
.L686:
	movl	40(%r15), %edx
	cmpl	$1, %edx
	je	.L711
	cmpl	$2, %edx
	je	.L712
	cmpl	$4, %edx
	je	.L713
	cmpl	$8, %edx
	je	.L714
	cmpl	$16, %edx
	je	.L715
	cmpl	$32, %edx
	je	.L716
	cmpl	$128, %edx
	je	.L717
	cmpl	$256, %edx
	je	.L718
	cmpl	$512, %edx
	je	.L719
	testl	%edx, %edx
	je	.L720
	movq	$0, (%r12)
	testq	%r13, %r13
	je	.L689
	movl	$0, 0(%r13)
	movl	40(%r15), %edx
.L689:
	testq	%r8, %r8
	je	.L692
	movq	$0, (%r8)
.L692:
	cmpl	$64, %edx
	movl	$0, %eax
	cmove	%rax, %r13
	testq	%rdi, %rdi
	je	.L680
.L710:
	call	EVP_CIPHER_flags@PLT
	testl	$2097152, %eax
	jne	.L695
.L680:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L711:
	.cfi_restore_state
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L687:
	movslq	%edx, %rax
	leaq	ssl_digest_methods(%rip), %rcx
	movslq	%edx, %rdx
	movq	(%rcx,%rdx,8), %rcx
	movq	%rcx, (%r12)
	testq	%r13, %r13
	je	.L691
	leaq	ssl_mac_pkey_id(%rip), %rsi
	movl	(%rsi,%rdx,4), %edx
	movl	%edx, 0(%r13)
.L691:
	testq	%r8, %r8
	je	.L694
	leaq	ssl_mac_secret_size(%rip), %rdx
	movq	(%rdx,%rax,8), %rax
	movq	%rax, (%r8)
.L694:
	testq	%rdi, %rdi
	je	.L680
	testq	%rcx, %rcx
	je	.L710
.L695:
	testq	%r13, %r13
	je	.L696
	movl	0(%r13), %edx
	testl	%edx, %edx
	je	.L680
.L696:
	movl	16(%rbp), %eax
	testl	%eax, %eax
	jne	.L757
	movl	(%r14), %eax
	movl	%eax, %edx
	sarl	$8, %edx
	cmpl	$3, %edx
	jne	.L757
	cmpl	$768, %eax
	je	.L757
	movl	36(%r15), %eax
	cmpl	$4, %eax
	jne	.L699
	cmpl	$1, 40(%r15)
	jne	.L757
	leaq	.LC8(%rip), %rdi
	call	EVP_get_cipherbyname@PLT
	testq	%rax, %rax
	jne	.L756
	movl	36(%r15), %eax
.L699:
	cmpl	$64, %eax
	je	.L760
	cmpl	$128, %eax
	jne	.L757
.L704:
	cmpl	$2, 40(%r15)
	je	.L761
.L706:
	cmpl	$16, 40(%r15)
	jne	.L757
	leaq	.LC12(%rip), %rdi
	call	EVP_get_cipherbyname@PLT
	testq	%rax, %rax
	je	.L757
.L756:
	movq	%rax, (%rbx)
	movq	$0, (%r12)
.L757:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L759:
	.cfi_restore_state
	movq	%r8, -56(%rbp)
	call	EVP_enc_null@PLT
	movq	-56(%rbp), %r8
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	jmp	.L686
.L712:
	movl	$1, %edx
	jmp	.L687
.L713:
	movl	$2, %edx
	jmp	.L687
.L714:
	movl	$3, %edx
	jmp	.L687
.L715:
	movl	$4, %edx
	jmp	.L687
.L716:
	movl	$5, %edx
	jmp	.L687
.L760:
	cmpl	$2, 40(%r15)
	je	.L762
.L702:
	cmpl	$16, 40(%r15)
	jne	.L757
	leaq	.LC11(%rip), %rdi
	call	EVP_get_cipherbyname@PLT
	testq	%rax, %rax
	jne	.L756
	movl	36(%r15), %eax
.L708:
	cmpl	$128, %eax
	je	.L706
	jmp	.L757
	.p2align 4,,10
	.p2align 3
.L717:
	movl	$6, %edx
	jmp	.L687
.L718:
	movl	$7, %edx
	jmp	.L687
.L719:
	movl	$8, %edx
	jmp	.L687
.L720:
	movl	$9, %edx
	jmp	.L687
.L761:
	leaq	.LC10(%rip), %rdi
	call	EVP_get_cipherbyname@PLT
	testq	%rax, %rax
	jne	.L756
	movl	36(%r15), %eax
	cmpl	$64, %eax
	jne	.L708
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L762:
	leaq	.LC9(%rip), %rdi
	call	EVP_get_cipherbyname@PLT
	testq	%rax, %rax
	jne	.L756
	movl	36(%r15), %eax
	cmpl	$128, %eax
	je	.L704
	cmpl	$64, %eax
	jne	.L757
	jmp	.L702
	.cfi_endproc
.LFE1115:
	.size	ssl_cipher_get_evp, .-ssl_cipher_get_evp
	.p2align 4
	.globl	ssl_md
	.type	ssl_md, @function
ssl_md:
.LFB1116:
	.cfi_startproc
	endbr64
	movzbl	%dil, %edi
	cmpl	$11, %edi
	jg	.L765
	leaq	ssl_digest_methods(%rip), %rax
	movq	(%rax,%rdi,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L765:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1116:
	.size	ssl_md, .-ssl_md
	.p2align 4
	.globl	ssl_handshake_md
	.type	ssl_handshake_md, @function
ssl_handshake_md:
.LFB1117:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ssl_get_algorithm2@PLT
	movzbl	%al, %eax
	cmpl	$11, %eax
	jg	.L768
	leaq	ssl_digest_methods(%rip), %rdx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	(%rdx,%rax,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L768:
	.cfi_restore_state
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1117:
	.size	ssl_handshake_md, .-ssl_handshake_md
	.p2align 4
	.globl	ssl_prf_md
	.type	ssl_prf_md, @function
ssl_prf_md:
.LFB1118:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ssl_get_algorithm2@PLT
	movzbl	%ah, %eax
	cmpl	$11, %eax
	jg	.L772
	cltq
	leaq	ssl_digest_methods(%rip), %rdx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	(%rdx,%rax,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L772:
	.cfi_restore_state
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1118:
	.size	ssl_prf_md, .-ssl_prf_md
	.p2align 4
	.globl	SSL_CTX_set_ciphersuites
	.type	SSL_CTX_set_ciphersuites, @function
SSL_CTX_set_ciphersuites:
.LFB1131:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	OPENSSL_sk_new_null@PLT
	testq	%rax, %rax
	je	.L777
	cmpb	$0, 0(%r13)
	movq	%rax, %r12
	je	.L776
	movq	%rax, %r8
	movl	$1, %edx
	movl	$58, %esi
	movq	%r13, %rdi
	leaq	ciphersuite_cb(%rip), %rcx
	call	CONF_parse_list@PLT
	testl	%eax, %eax
	je	.L780
.L776:
	movq	24(%rbx), %rdi
	call	OPENSSL_sk_free@PLT
	cmpq	$0, 8(%rbx)
	movq	%r12, 24(%rbx)
	movl	$1, %eax
	jne	.L781
.L774:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L781:
	.cfi_restore_state
	addq	$24, %rsp
	leaq	16(%rbx), %rsi
	leaq	8(%rbx), %rdi
	movq	%r12, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	update_cipher_list
	.p2align 4,,10
	.p2align 3
.L780:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	%eax, -36(%rbp)
	call	OPENSSL_sk_free@PLT
	movl	-36(%rbp), %eax
	jmp	.L774
	.p2align 4,,10
	.p2align 3
.L777:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1131:
	.size	SSL_CTX_set_ciphersuites, .-SSL_CTX_set_ciphersuites
	.p2align 4
	.globl	SSL_set_ciphersuites
	.type	SSL_set_ciphersuites, @function
SSL_set_ciphersuites:
.LFB1132:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	OPENSSL_sk_new_null@PLT
	testq	%rax, %rax
	je	.L783
	cmpb	$0, 0(%r13)
	movq	%rax, %r12
	jne	.L798
.L784:
	movq	304(%rbx), %rdi
	call	OPENSSL_sk_free@PLT
	cmpq	$0, 288(%rbx)
	movq	%r12, 304(%rbx)
	je	.L799
.L786:
	addq	$8, %rsp
	leaq	296(%rbx), %rsi
	leaq	288(%rbx), %rdi
	movq	%r12, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	update_cipher_list
	.p2align 4,,10
	.p2align 3
.L798:
	.cfi_restore_state
	movq	%rax, %r8
	movl	$1, %edx
	movl	$58, %esi
	movq	%r13, %rdi
	leaq	ciphersuite_cb(%rip), %rcx
	call	CONF_parse_list@PLT
	testl	%eax, %eax
	jne	.L784
	movq	%r12, %rdi
	call	OPENSSL_sk_free@PLT
.L783:
	cmpq	$0, 288(%rbx)
	je	.L800
.L790:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L799:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	SSL_get_ciphers@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L787
	call	OPENSSL_sk_dup@PLT
	movq	%rax, 288(%rbx)
.L787:
	cmpq	$0, 288(%rbx)
	jne	.L801
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L801:
	.cfi_restore_state
	movq	304(%rbx), %r12
	jmp	.L786
.L800:
	movq	%rbx, %rdi
	call	SSL_get_ciphers@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L790
	call	OPENSSL_sk_dup@PLT
	movq	%rax, 288(%rbx)
	jmp	.L790
	.cfi_endproc
.LFE1132:
	.size	SSL_set_ciphersuites, .-SSL_set_ciphersuites
	.section	.rodata.str1.1
.LC13:
	.string	"ECDHE-ECDSA-AES256-GCM-SHA384"
	.section	.rodata.str1.8
	.align 8
.LC14:
	.string	"ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384"
	.section	.rodata.str1.1
.LC15:
	.string	"ECDHE-ECDSA-AES128-GCM-SHA256"
.LC16:
	.string	"SUITEB128ONLY"
.LC17:
	.string	"SUITEB128C2"
.LC18:
	.string	"SUITEB128"
.LC19:
	.string	"SUITEB192"
.LC20:
	.string	"DEFAULT"
	.section	.rodata.str1.8
	.align 8
.LC21:
	.string	"ALL:!COMPLEMENTOFDEFAULT:!eNULL"
	.text
	.p2align 4
	.globl	ssl_create_cipher_list
	.type	ssl_create_cipher_list, @function
ssl_create_cipher_list:
.LFB1133:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	subq	$104, %rsp
	movq	%rdx, -128(%rbp)
	movq	%rcx, -136(%rbp)
	movq	%r9, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r8, %r8
	movq	$0, -72(%rbp)
	sete	%dl
	testq	%rcx, %rcx
	movq	$0, -64(%rbp)
	sete	%al
	orb	%al, %dl
	jne	.L849
	testq	%rsi, %rsi
	je	.L849
	movq	%rdi, %r13
	movl	$13, %ecx
	movq	%r8, %rsi
	movl	28(%r9), %eax
	leaq	.LC16(%rip), %rdi
	movq	%r8, %r15
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	jne	.L918
	andl	$-196609, %eax
	orl	$65536, %eax
	movl	%eax, 28(%r9)
	movq	192(%r13), %rax
	testb	$16, 96(%rax)
	jne	.L850
.L810:
	movl	$1249, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$158, %edx
	xorl	%r12d, %r12d
	movl	$331, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
.L802:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L919
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L918:
	.cfi_restore_state
	movl	$11, %ecx
	leaq	.LC17(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	jne	.L920
	orl	$196608, %eax
	movl	%eax, 28(%r9)
	movq	192(%r13), %rax
	testb	$16, 96(%rax)
	je	.L810
.L851:
	leaq	.LC13(%rip), %r15
.L808:
	movl	disabled_mkey_mask(%rip), %eax
	movl	%eax, -116(%rbp)
	movl	disabled_auth_mask(%rip), %eax
	movl	%eax, -84(%rbp)
	movl	disabled_enc_mask(%rip), %eax
	movl	%eax, -88(%rbp)
	movl	disabled_mac_mask(%rip), %eax
	movl	%eax, -112(%rbp)
	call	*168(%r13)
	movl	$1444, %edx
	leaq	.LC0(%rip), %rsi
	movslq	%eax, %rdi
	movq	%rdi, %r12
	salq	$5, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L812
	movl	$0, -108(%rbp)
	xorl	%r14d, %r14d
	testl	%r12d, %r12d
	jle	.L814
	movq	%rbx, -144(%rbp)
	movl	%r14d, %ebx
	movl	-116(%rbp), %r14d
	.p2align 4,,10
	.p2align 3
.L813:
	movl	%ebx, %edi
	call	*176(%r13)
	testq	%rax, %rax
	je	.L815
	movl	(%rax), %r8d
	testl	%r8d, %r8d
	je	.L815
	testl	%r14d, 28(%rax)
	jne	.L815
	movl	-84(%rbp), %edx
	testl	%edx, 32(%rax)
	jne	.L815
	movl	-88(%rbp), %edx
	testl	%edx, 36(%rax)
	jne	.L815
	movl	-112(%rbp), %esi
	testl	%esi, 40(%rax)
	jne	.L815
	movq	192(%r13), %rcx
	testb	$8, 96(%rcx)
	je	.L921
	movl	52(%rax), %esi
	testl	%esi, %esi
	je	.L815
.L817:
	movslq	-108(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	%rcx, %rdi
	salq	$5, %rcx
	addq	-96(%rbp), %rcx
	addl	$1, %edi
	movq	%rax, (%rcx)
	movl	$0, 8(%rcx)
	movl	%edi, -108(%rbp)
	movups	%xmm0, 16(%rcx)
	.p2align 4,,10
	.p2align 3
.L815:
	addl	$1, %ebx
	cmpl	%ebx, %r12d
	jne	.L813
	movl	-108(%rbp), %eax
	movq	-144(%rbp), %rbx
	testl	%eax, %eax
	je	.L814
	movq	-96(%rbp), %rdx
	movslq	%eax, %rsi
	salq	$5, %rsi
	movq	$0, 24(%rdx)
	movq	%rdx, %rcx
	leaq	-32(%rdx,%rsi), %rdi
	cmpl	$1, %eax
	je	.L819
	leaq	32(%rdx), %rdx
	movq	%rdx, 16(%rcx)
	cmpl	$2, %eax
	je	.L822
	movl	%eax, %ecx
	movq	-96(%rbp), %rax
	subl	$3, %ecx
	salq	$5, %rcx
	addq	%rdx, %rcx
	.p2align 4,,10
	.p2align 3
.L821:
	leaq	64(%rax), %rdx
	movq	%rax, 56(%rax)
	addq	$32, %rax
	movq	%rdx, 16(%rax)
	cmpq	%rcx, %rax
	jne	.L821
.L822:
	movq	-96(%rbp), %rax
	leaq	-64(%rax,%rsi), %rax
	movq	%rax, 24(%rdi)
.L819:
	movq	-96(%rbp), %rax
	movq	$0, 16(%rdi)
	movq	%rdi, -64(%rbp)
	movq	%rax, -72(%rbp)
.L814:
	leaq	-64(%rbp), %r14
	leaq	-72(%rbp), %r13
	xorl	%edx, %edx
	movl	$8, %esi
	movq	%r14, %r8
	movq	%r13, %rcx
	movl	$4, %edi
	call	ssl_cipher_apply_rule.constprop.0
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$4, %edi
	call	ssl_cipher_apply_rule.constprop.0
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	%r14
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%edi, %edi
	pushq	%r13
	movl	$4, %esi
	pushq	$-1
	pushq	$3
	pushq	$0
	call	ssl_cipher_apply_rule
	addq	$48, %rsp
	movq	%r14, %r8
	movq	%r13, %rcx
	xorl	%esi, %esi
	xorl	%edi, %edi
	movl	$12288, %edx
	call	ssl_cipher_apply_rule.constprop.0
	xorl	%esi, %esi
	xorl	%edi, %edi
	movl	$524288, %edx
	call	ssl_cipher_apply_rule.constprop.0
	xorl	%esi, %esi
	xorl	%edi, %edi
	movl	$245952, %edx
	call	ssl_cipher_apply_rule.constprop.0
	xorl	%edx, %edx
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	ssl_cipher_apply_rule.constprop.0
	subq	$8, %rsp
	movq	%r13, %r9
	xorl	%esi, %esi
	pushq	%r14
	xorl	%edi, %edi
	movl	$-1, %r8d
	movl	$1, %ecx
	xorl	%edx, %edx
	call	ssl_cipher_apply_rule.constprop.1
	xorl	%ecx, %ecx
	movl	$-1, %r8d
	xorl	%edx, %edx
	movl	$4, %esi
	xorl	%edi, %edi
	movq	%r14, (%rsp)
	call	ssl_cipher_apply_rule.constprop.1
	movl	$-1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$1, %edi
	movq	%r14, (%rsp)
	call	ssl_cipher_apply_rule.constprop.1
	movl	$-1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$8, %edi
	movq	%r14, (%rsp)
	call	ssl_cipher_apply_rule.constprop.1
	xorl	%ecx, %ecx
	movl	$4, %edx
	xorl	%esi, %esi
	movl	$-1, %r8d
	xorl	%edi, %edi
	movq	%r14, (%rsp)
	call	ssl_cipher_apply_rule.constprop.1
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	ssl_cipher_strength_sort
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	je	.L922
	movq	%r14, %r8
	movq	%r13, %rcx
	movl	$771, %edx
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	ssl_cipher_apply_rule.constprop.2
	xorl	%edx, %edx
	movl	$64, %esi
	xorl	%edi, %edi
	call	ssl_cipher_apply_rule.constprop.2
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$6, %edi
	call	ssl_cipher_apply_rule.constprop.2
	xorl	%edx, %edx
	movl	$64, %esi
	movl	$6, %edi
	call	ssl_cipher_apply_rule.constprop.2
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	%r14
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	pushq	%r13
	xorl	%edi, %edi
	pushq	$-1
	pushq	$3
	pushq	$0
	call	ssl_cipher_apply_rule
	leal	77(%r12), %edi
	addq	$48, %rsp
	movl	$1557, %edx
	movslq	%edi, %rdi
	leaq	.LC0(%rip), %rsi
	salq	$3, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L923
	movl	-116(%rbp), %edi
	movl	-84(%rbp), %r8d
	movq	%r12, %rdx
	movl	-88(%rbp), %r9d
	movl	-112(%rbp), %r10d
	movq	-72(%rbp), %rax
	notl	%edi
	notl	%r8d
	notl	%r9d
	notl	%r10d
	testq	%rax, %rax
	je	.L825
	.p2align 4,,10
	.p2align 3
.L826:
	movq	(%rax), %rcx
	movq	16(%rax), %rax
	addq	$8, %rdx
	movq	%rcx, -8(%rdx)
	testq	%rax, %rax
	jne	.L826
.L825:
	leaq	cipher_aliases(%rip), %rax
	leaq	6080(%rax), %r11
	.p2align 4,,10
	.p2align 3
.L832:
	movl	28(%rax), %ecx
	movl	32(%rax), %esi
	testl	%ecx, %ecx
	je	.L827
	testl	%ecx, %edi
	je	.L828
.L827:
	testl	%esi, %esi
	je	.L829
	testl	%esi, %r8d
	je	.L828
.L829:
	movl	36(%rax), %ecx
	testl	%ecx, %ecx
	je	.L830
	testl	%ecx, %r9d
	je	.L828
.L830:
	movl	40(%rax), %ecx
	testl	%ecx, %ecx
	je	.L831
	testl	%ecx, %r10d
	je	.L828
.L831:
	movq	%rax, (%rdx)
	addq	$8, %rdx
.L828:
	addq	$80, %rax
	cmpq	%rax, %r11
	jne	.L832
	movq	$0, (%rdx)
	movl	$7, %ecx
	leaq	.LC20(%rip), %rdi
	movq	%r15, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L924
.L833:
	cmpb	$0, (%r15)
	jne	.L925
	movl	$1584, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
.L838:
	call	OPENSSL_sk_new_null@PLT
	xorl	%r13d, %r13d
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L839
	jmp	.L926
	.p2align 4,,10
	.p2align 3
.L841:
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L843
	addl	$1, %r13d
.L839:
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jl	.L841
	movq	-72(%rbp), %rbx
	testq	%rbx, %rbx
	jne	.L842
	jmp	.L846
	.p2align 4,,10
	.p2align 3
.L845:
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L846
.L842:
	movl	8(%rbx), %eax
	testl	%eax, %eax
	je	.L845
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L845
	movq	-96(%rbp), %rdi
	movl	$1616, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	OPENSSL_sk_free@PLT
	jmp	.L802
	.p2align 4,,10
	.p2align 3
.L849:
	xorl	%r12d, %r12d
	jmp	.L802
	.p2align 4,,10
	.p2align 3
.L920:
	movl	$9, %ecx
	leaq	.LC18(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L806
	movl	$9, %ecx
	leaq	.LC19(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L807
	andl	$196608, %eax
	je	.L808
	movq	192(%r13), %rdx
	testb	$16, 96(%rdx)
	je	.L810
	cmpl	$131072, %eax
	je	.L851
	cmpl	$196608, %eax
	je	.L852
	cmpl	$65536, %eax
	leaq	.LC15(%rip), %rax
	cmove	%rax, %r15
	jmp	.L808
	.p2align 4,,10
	.p2align 3
.L921:
	movl	44(%rax), %edi
	testl	%edi, %edi
	jne	.L817
	jmp	.L815
	.p2align 4,,10
	.p2align 3
.L843:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	OPENSSL_sk_free@PLT
	jmp	.L802
	.p2align 4,,10
	.p2align 3
.L924:
	movq	-104(%rbp), %rax
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	leaq	.LC21(%rip), %rdi
	leaq	496(%rax), %r8
	call	ssl_cipher_process_rulestr.isra.0
	cmpb	$58, 7(%r15)
	je	.L834
	addq	$7, %r15
.L835:
	testl	%eax, %eax
	jne	.L833
	movl	$1584, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
.L847:
	movq	-96(%rbp), %rdi
	movl	$1587, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%r12d, %r12d
	call	CRYPTO_free@PLT
	jmp	.L802
	.p2align 4,,10
	.p2align 3
.L846:
	movq	-96(%rbp), %rdi
	movl	$1625, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	call	OPENSSL_sk_dup@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L843
	movq	-136(%rbp), %rbx
	movq	(%rbx), %rdi
	call	OPENSSL_sk_free@PLT
	movq	%r13, (%rbx)
	movq	ssl_cipher_ptr_id_cmp@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_set_cmp_func@PLT
	movq	(%rbx), %rdi
	call	OPENSSL_sk_sort@PLT
	movq	-128(%rbp), %rbx
	movq	(%rbx), %rdi
	call	OPENSSL_sk_free@PLT
	movq	%r12, (%rbx)
	jmp	.L802
	.p2align 4,,10
	.p2align 3
.L925:
	movq	-104(%rbp), %r8
	movq	%r13, %rsi
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%r12, %rcx
	addq	$496, %r8
	call	ssl_cipher_process_rulestr.isra.0
	movl	$1584, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	CRYPTO_free@PLT
	testl	%r13d, %r13d
	jne	.L838
	jmp	.L847
	.p2align 4,,10
	.p2align 3
.L922:
	movq	-96(%rbp), %rdi
	movl	$1513, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%r12d, %r12d
	call	CRYPTO_free@PLT
	jmp	.L802
	.p2align 4,,10
	.p2align 3
.L850:
	leaq	.LC15(%rip), %r15
	jmp	.L808
	.p2align 4,,10
	.p2align 3
.L806:
	orl	$196608, %eax
	movl	%eax, 28(%r9)
	movq	192(%r13), %rax
	testb	$16, 96(%rax)
	je	.L810
.L852:
	leaq	.LC14(%rip), %r15
	jmp	.L808
	.p2align 4,,10
	.p2align 3
.L807:
	andl	$-196609, %eax
	orl	$131072, %eax
	movl	%eax, 28(%r9)
	movq	192(%r13), %rax
	testb	$16, 96(%rax)
	jne	.L851
	jmp	.L810
	.p2align 4,,10
	.p2align 3
.L812:
	movl	$1446, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$166, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L802
	.p2align 4,,10
	.p2align 3
.L834:
	addq	$8, %r15
	jmp	.L835
	.p2align 4,,10
	.p2align 3
.L923:
	movq	-96(%rbp), %rdi
	movl	$1559, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$1560, %r8d
	movl	$65, %edx
	leaq	.LC0(%rip), %rcx
	movl	$166, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L802
.L926:
	movq	-96(%rbp), %rdi
	movl	$1596, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L802
.L919:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1133:
	.size	ssl_create_cipher_list, .-ssl_create_cipher_list
	.section	.rodata.str1.1
.LC22:
	.string	"unknown"
.LC23:
	.string	"RSA"
.LC24:
	.string	"ECDH"
.LC25:
	.string	"PSK"
.LC26:
	.string	"DHEPSK"
.LC27:
	.string	"DH"
.LC28:
	.string	"ECDHEPSK"
.LC29:
	.string	"SRP"
.LC30:
	.string	"GOST"
.LC31:
	.string	"any"
.LC32:
	.string	"RSAPSK"
.LC33:
	.string	"None"
.LC34:
	.string	"ECDSA"
.LC35:
	.string	"DSS"
.LC36:
	.string	"GOST01"
.LC37:
	.string	"GOST12"
.LC38:
	.string	"DES(56)"
.LC39:
	.string	"RC4(128)"
.LC40:
	.string	"RC2(128)"
.LC41:
	.string	"IDEA(128)"
.LC42:
	.string	"Camellia(128)"
.LC43:
	.string	"AES(128)"
.LC44:
	.string	"AESCCM(128)"
.LC45:
	.string	"AESGCM(128)"
.LC46:
	.string	"AESCCM8(128)"
.LC47:
	.string	"AESGCM(256)"
.LC48:
	.string	"GOST89(256)"
.LC49:
	.string	"CHACHA20/POLY1305(256)"
.LC50:
	.string	"3DES(168)"
.LC51:
	.string	"AES(256)"
.LC52:
	.string	"ARIAGCM(128)"
.LC53:
	.string	"AESCCM8(256)"
.LC54:
	.string	"Camellia(256)"
.LC55:
	.string	"AESCCM(256)"
.LC56:
	.string	"SEED(128)"
.LC57:
	.string	"ARIAGCM(256)"
.LC58:
	.string	"MD5"
.LC59:
	.string	"SHA256"
.LC60:
	.string	"SHA384"
.LC61:
	.string	"GOST2012"
.LC62:
	.string	"GOST89"
.LC63:
	.string	"GOST94"
.LC64:
	.string	"SHA1"
.LC65:
	.string	"AEAD"
	.section	.rodata.str1.8
	.align 8
.LC66:
	.string	"%-23s %s Kx=%-8s Au=%-4s Enc=%-9s Mac=%-4s\n"
	.text
	.p2align 4
	.globl	SSL_CIPHER_description
	.type	SSL_CIPHER_description, @function
SSL_CIPHER_description:
.LFB1134:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rsi, %rsi
	je	.L1025
	cmpl	$127, %edx
	jle	.L976
	movq	%rsi, %r13
	movslq	%edx, %r14
.L929:
	movl	28(%r15), %edx
	movl	32(%r15), %ecx
	movl	44(%r15), %edi
	movl	36(%r15), %ebx
	movl	%edx, -56(%rbp)
	movl	40(%r15), %r12d
	movl	%ecx, -52(%rbp)
	call	ssl_protocol_to_string@PLT
	movl	-56(%rbp), %edx
	movl	-52(%rbp), %ecx
	movq	%rax, %r8
	cmpl	$32, %edx
	ja	.L931
	leaq	.L934(%rip), %rsi
	movslq	(%rsi,%rdx,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L934:
	.long	.L940-.L934
	.long	.L977-.L934
	.long	.L938-.L934
	.long	.L932-.L934
	.long	.L937-.L934
	.long	.L932-.L934
	.long	.L932-.L934
	.long	.L932-.L934
	.long	.L936-.L934
	.long	.L932-.L934
	.long	.L932-.L934
	.long	.L932-.L934
	.long	.L932-.L934
	.long	.L932-.L934
	.long	.L932-.L934
	.long	.L932-.L934
	.long	.L935-.L934
	.long	.L932-.L934
	.long	.L932-.L934
	.long	.L932-.L934
	.long	.L932-.L934
	.long	.L932-.L934
	.long	.L932-.L934
	.long	.L932-.L934
	.long	.L932-.L934
	.long	.L932-.L934
	.long	.L932-.L934
	.long	.L932-.L934
	.long	.L932-.L934
	.long	.L932-.L934
	.long	.L932-.L934
	.long	.L932-.L934
	.long	.L933-.L934
	.text
.L938:
	leaq	.LC27(%rip), %r9
.L939:
	cmpl	$32, %ecx
	ja	.L941
.L1026:
	leaq	.L944(%rip), %rdx
	movslq	(%rdx,%rcx,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L944:
	.long	.L950-.L944
	.long	.L981-.L944
	.long	.L948-.L944
	.long	.L942-.L944
	.long	.L947-.L944
	.long	.L942-.L944
	.long	.L942-.L944
	.long	.L942-.L944
	.long	.L946-.L944
	.long	.L942-.L944
	.long	.L942-.L944
	.long	.L942-.L944
	.long	.L942-.L944
	.long	.L942-.L944
	.long	.L942-.L944
	.long	.L942-.L944
	.long	.L945-.L944
	.long	.L942-.L944
	.long	.L942-.L944
	.long	.L942-.L944
	.long	.L942-.L944
	.long	.L942-.L944
	.long	.L942-.L944
	.long	.L942-.L944
	.long	.L942-.L944
	.long	.L942-.L944
	.long	.L942-.L944
	.long	.L942-.L944
	.long	.L942-.L944
	.long	.L942-.L944
	.long	.L942-.L944
	.long	.L942-.L944
	.long	.L943-.L944
	.text
.L948:
	leaq	.LC35(%rip), %rdx
.L949:
	leaq	.LC56(%rip), %rcx
	cmpl	$2048, %ebx
	je	.L951
	ja	.L952
	cmpl	$32, %ebx
	ja	.L953
	leaq	.LC22(%rip), %rcx
	testl	%ebx, %ebx
	je	.L951
	cmpl	$32, %ebx
	ja	.L954
	leaq	.L956(%rip), %rcx
	movslq	(%rcx,%rbx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L956:
	.long	.L954-.L956
	.long	.L986-.L956
	.long	.L960-.L956
	.long	.L954-.L956
	.long	.L959-.L956
	.long	.L954-.L956
	.long	.L954-.L956
	.long	.L954-.L956
	.long	.L958-.L956
	.long	.L954-.L956
	.long	.L954-.L956
	.long	.L954-.L956
	.long	.L954-.L956
	.long	.L954-.L956
	.long	.L954-.L956
	.long	.L954-.L956
	.long	.L957-.L956
	.long	.L954-.L956
	.long	.L954-.L956
	.long	.L954-.L956
	.long	.L954-.L956
	.long	.L954-.L956
	.long	.L954-.L956
	.long	.L954-.L956
	.long	.L954-.L956
	.long	.L954-.L956
	.long	.L954-.L956
	.long	.L954-.L956
	.long	.L954-.L956
	.long	.L954-.L956
	.long	.L954-.L956
	.long	.L954-.L956
	.long	.L955-.L956
	.text
.L981:
	leaq	.LC23(%rip), %rdx
	jmp	.L949
.L942:
	leaq	.LC22(%rip), %rdx
	jmp	.L949
.L977:
	leaq	.LC23(%rip), %r9
	cmpl	$32, %ecx
	jbe	.L1026
.L941:
	leaq	.LC29(%rip), %rdx
	cmpl	$64, %ecx
	je	.L949
	cmpl	$160, %ecx
	leaq	.LC22(%rip), %rdx
	leaq	.LC37(%rip), %rax
	cmove	%rax, %rdx
	jmp	.L949
.L932:
	leaq	.LC22(%rip), %r9
	jmp	.L939
.L960:
	leaq	.LC50(%rip), %rcx
.L951:
	cmpl	$32, %r12d
	ja	.L965
.L1027:
	leaq	.LC22(%rip), %rdi
	testl	%r12d, %r12d
	je	.L966
	cmpl	$32, %r12d
	ja	.L967
	leaq	.L969(%rip), %rsi
	movslq	(%rsi,%r12,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L969:
	.long	.L967-.L969
	.long	.L1003-.L969
	.long	.L973-.L969
	.long	.L967-.L969
	.long	.L972-.L969
	.long	.L967-.L969
	.long	.L967-.L969
	.long	.L967-.L969
	.long	.L971-.L969
	.long	.L967-.L969
	.long	.L967-.L969
	.long	.L967-.L969
	.long	.L967-.L969
	.long	.L967-.L969
	.long	.L967-.L969
	.long	.L967-.L969
	.long	.L970-.L969
	.long	.L967-.L969
	.long	.L967-.L969
	.long	.L967-.L969
	.long	.L967-.L969
	.long	.L967-.L969
	.long	.L967-.L969
	.long	.L967-.L969
	.long	.L967-.L969
	.long	.L967-.L969
	.long	.L967-.L969
	.long	.L967-.L969
	.long	.L967-.L969
	.long	.L967-.L969
	.long	.L967-.L969
	.long	.L967-.L969
	.long	.L968-.L969
	.text
.L971:
	leaq	.LC62(%rip), %rdi
.L966:
	subq	$8, %rsp
	movq	8(%r15), %r10
	movq	%r14, %rsi
	xorl	%eax, %eax
	pushq	%rdi
	movq	%r13, %rdi
	pushq	%rcx
	movq	%r10, %rcx
	pushq	%rdx
	leaq	.LC66(%rip), %rdx
	call	BIO_snprintf@PLT
	addq	$32, %rsp
.L927:
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L973:
	.cfi_restore_state
	leaq	.LC64(%rip), %rdi
	jmp	.L966
.L1003:
	leaq	.LC58(%rip), %rdi
	jmp	.L966
.L967:
	leaq	.LC22(%rip), %rdi
	jmp	.L966
.L954:
	leaq	.LC22(%rip), %rcx
	cmpl	$32, %r12d
	jbe	.L1027
.L965:
	leaq	.LC61(%rip), %rdi
	cmpl	$128, %r12d
	je	.L966
	jbe	.L1028
	leaq	.LC62(%rip), %rdi
	cmpl	$256, %r12d
	je	.L966
	cmpl	$512, %r12d
	leaq	.LC22(%rip), %rdi
	leaq	.LC61(%rip), %rax
	cmove	%rax, %rdi
	jmp	.L966
.L986:
	leaq	.LC38(%rip), %rcx
	jmp	.L951
	.p2align 4,,10
	.p2align 3
.L952:
	leaq	.LC46(%rip), %rcx
	cmpl	$65536, %ebx
	je	.L951
	jbe	.L1029
	leaq	.LC49(%rip), %rcx
	cmpl	$524288, %ebx
	je	.L951
	jbe	.L1030
	leaq	.LC52(%rip), %rcx
	cmpl	$1048576, %ebx
	je	.L951
	cmpl	$2097152, %ebx
	leaq	.LC22(%rip), %rcx
	leaq	.LC57(%rip), %rax
	cmove	%rax, %rcx
	jmp	.L951
	.p2align 4,,10
	.p2align 3
.L953:
	leaq	.LC42(%rip), %rcx
	cmpl	$256, %ebx
	je	.L951
	jbe	.L1031
	leaq	.LC54(%rip), %rcx
	cmpl	$512, %ebx
	je	.L951
	cmpl	$1024, %ebx
	leaq	.LC22(%rip), %rcx
	leaq	.LC48(%rip), %rax
	cmove	%rax, %rcx
	jmp	.L951
	.p2align 4,,10
	.p2align 3
.L1028:
	cmpl	$64, %r12d
	leaq	.LC22(%rip), %rdi
	leaq	.LC65(%rip), %rax
	cmove	%rax, %rdi
	jmp	.L966
	.p2align 4,,10
	.p2align 3
.L931:
	leaq	.LC28(%rip), %r9
	cmpl	$128, %edx
	je	.L939
	cmpl	$256, %edx
	jne	.L1032
	leaq	.LC26(%rip), %r9
	jmp	.L939
.L1032:
	cmpl	$64, %edx
	leaq	.LC22(%rip), %rax
	leaq	.LC32(%rip), %r9
	cmovne	%rax, %r9
	jmp	.L939
.L1031:
	leaq	.LC43(%rip), %rcx
	cmpl	$64, %ebx
	je	.L951
	cmpl	$128, %ebx
	leaq	.LC22(%rip), %rcx
	leaq	.LC51(%rip), %rax
	cmove	%rax, %rcx
	jmp	.L951
.L1030:
	leaq	.LC53(%rip), %rcx
	cmpl	$131072, %ebx
	je	.L951
	cmpl	$262144, %ebx
	leaq	.LC22(%rip), %rcx
	leaq	.LC48(%rip), %rax
	cmove	%rax, %rcx
	jmp	.L951
	.p2align 4,,10
	.p2align 3
.L1029:
	leaq	.LC44(%rip), %rcx
	cmpl	$16384, %ebx
	je	.L951
	jbe	.L1033
	cmpl	$32768, %ebx
	leaq	.LC22(%rip), %rcx
	leaq	.LC55(%rip), %rax
	cmove	%rax, %rcx
	jmp	.L951
.L1033:
	leaq	.LC45(%rip), %rcx
	cmpl	$4096, %ebx
	je	.L951
	cmpl	$8192, %ebx
	leaq	.LC22(%rip), %rcx
	leaq	.LC47(%rip), %rax
	cmove	%rax, %rcx
	jmp	.L951
.L968:
	leaq	.LC60(%rip), %rdi
	jmp	.L966
.L972:
	leaq	.LC63(%rip), %rdi
	jmp	.L966
.L970:
	leaq	.LC59(%rip), %rdi
	jmp	.L966
.L945:
	leaq	.LC25(%rip), %rdx
	jmp	.L949
.L943:
	leaq	.LC36(%rip), %rdx
	jmp	.L949
.L950:
	leaq	.LC31(%rip), %rdx
	jmp	.L949
.L947:
	leaq	.LC33(%rip), %rdx
	jmp	.L949
.L946:
	leaq	.LC34(%rip), %rdx
	jmp	.L949
.L933:
	leaq	.LC29(%rip), %r9
	jmp	.L939
.L940:
	leaq	.LC31(%rip), %r9
	jmp	.L939
.L937:
	leaq	.LC24(%rip), %r9
	jmp	.L939
.L936:
	leaq	.LC25(%rip), %r9
	jmp	.L939
.L935:
	leaq	.LC30(%rip), %r9
	jmp	.L939
	.p2align 4,,10
	.p2align 3
.L1025:
	movl	$1646, %edx
	leaq	.LC0(%rip), %rsi
	movl	$128, %edi
	movl	$128, %r14d
	call	CRYPTO_malloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L929
	movl	$1647, %r8d
	movl	$65, %edx
	movl	$626, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L976:
	xorl	%r13d, %r13d
	jmp	.L927
.L959:
	leaq	.LC39(%rip), %rcx
	jmp	.L951
.L955:
	leaq	.LC33(%rip), %rcx
	jmp	.L951
.L957:
	leaq	.LC41(%rip), %rcx
	jmp	.L951
.L958:
	leaq	.LC40(%rip), %rcx
	jmp	.L951
	.cfi_endproc
.LFE1134:
	.size	SSL_CIPHER_description, .-SSL_CIPHER_description
	.section	.rodata.str1.1
.LC67:
	.string	"(NONE)"
.LC68:
	.string	"TLSv1.0"
	.text
	.p2align 4
	.globl	SSL_CIPHER_get_version
	.type	SSL_CIPHER_get_version, @function
SSL_CIPHER_get_version:
.LFB1135:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1036
	movl	44(%rdi), %edi
	cmpl	$769, %edi
	je	.L1037
	jmp	ssl_protocol_to_string@PLT
	.p2align 4,,10
	.p2align 3
.L1037:
	leaq	.LC68(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1036:
	leaq	.LC67(%rip), %rax
	ret
	.cfi_endproc
.LFE1135:
	.size	SSL_CIPHER_get_version, .-SSL_CIPHER_get_version
	.p2align 4
	.globl	SSL_CIPHER_get_name
	.type	SSL_CIPHER_get_name, @function
SSL_CIPHER_get_name:
.LFB1136:
	.cfi_startproc
	endbr64
	leaq	.LC67(%rip), %rax
	testq	%rdi, %rdi
	je	.L1038
	movq	8(%rdi), %rax
.L1038:
	ret
	.cfi_endproc
.LFE1136:
	.size	SSL_CIPHER_get_name, .-SSL_CIPHER_get_name
	.p2align 4
	.globl	SSL_CIPHER_standard_name
	.type	SSL_CIPHER_standard_name, @function
SSL_CIPHER_standard_name:
.LFB1137:
	.cfi_startproc
	endbr64
	leaq	.LC67(%rip), %rax
	testq	%rdi, %rdi
	je	.L1042
	movq	16(%rdi), %rax
.L1042:
	ret
	.cfi_endproc
.LFE1137:
	.size	SSL_CIPHER_standard_name, .-SSL_CIPHER_standard_name
	.p2align 4
	.globl	OPENSSL_cipher_name
	.type	OPENSSL_cipher_name, @function
OPENSSL_cipher_name:
.LFB1138:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1056
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ssl3_get_cipher_by_std_name@PLT
	testq	%rax, %rax
	je	.L1049
	movq	8(%rax), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1049:
	.cfi_restore_state
	leaq	.LC67(%rip), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1056:
	.cfi_restore 6
	leaq	.LC67(%rip), %rax
	ret
	.cfi_endproc
.LFE1138:
	.size	OPENSSL_cipher_name, .-OPENSSL_cipher_name
	.p2align 4
	.globl	SSL_CIPHER_get_bits
	.type	SSL_CIPHER_get_bits, @function
SSL_CIPHER_get_bits:
.LFB1139:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1057
	testq	%rsi, %rsi
	je	.L1059
	movl	72(%rdi), %eax
	movl	%eax, (%rsi)
.L1059:
	movl	68(%rdi), %eax
.L1057:
	ret
	.cfi_endproc
.LFE1139:
	.size	SSL_CIPHER_get_bits, .-SSL_CIPHER_get_bits
	.p2align 4
	.globl	SSL_CIPHER_get_id
	.type	SSL_CIPHER_get_id, @function
SSL_CIPHER_get_id:
.LFB1140:
	.cfi_startproc
	endbr64
	movl	24(%rdi), %eax
	ret
	.cfi_endproc
.LFE1140:
	.size	SSL_CIPHER_get_id, .-SSL_CIPHER_get_id
	.p2align 4
	.globl	SSL_CIPHER_get_protocol_id
	.type	SSL_CIPHER_get_protocol_id, @function
SSL_CIPHER_get_protocol_id:
.LFB1141:
	.cfi_startproc
	endbr64
	movl	24(%rdi), %eax
	ret
	.cfi_endproc
.LFE1141:
	.size	SSL_CIPHER_get_protocol_id, .-SSL_CIPHER_get_protocol_id
	.p2align 4
	.globl	ssl3_comp_find
	.type	ssl3_comp_find, @function
ssl3_comp_find:
.LFB1142:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	je	.L1073
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L1068
	movl	%esi, %r12d
	call	OPENSSL_sk_num@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jle	.L1068
	xorl	%r14d, %r14d
	jmp	.L1070
	.p2align 4,,10
	.p2align 3
.L1077:
	addl	$1, %r14d
	cmpl	%r13d, %r14d
	je	.L1068
.L1070:
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	cmpl	%r12d, (%rax)
	jne	.L1077
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1068:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1073:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1142:
	.size	ssl3_comp_find, .-ssl3_comp_find
	.p2align 4
	.globl	SSL_COMP_get_compression_methods
	.type	SSL_COMP_get_compression_methods, @function
SSL_COMP_get_compression_methods:
.LFB1143:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1143:
	.size	SSL_COMP_get_compression_methods, .-SSL_COMP_get_compression_methods
	.p2align 4
	.globl	SSL_COMP_set0_compression_methods
	.type	SSL_COMP_set0_compression_methods, @function
SSL_COMP_set0_compression_methods:
.LFB1144:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE1144:
	.size	SSL_COMP_set0_compression_methods, .-SSL_COMP_set0_compression_methods
	.p2align 4
	.globl	SSL_COMP_add_compression_method
	.type	SSL_COMP_add_compression_method, @function
SSL_COMP_add_compression_method:
.LFB1145:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1145:
	.size	SSL_COMP_add_compression_method, .-SSL_COMP_add_compression_method
	.p2align 4
	.globl	SSL_COMP_get_name
	.type	SSL_COMP_get_name, @function
SSL_COMP_get_name:
.LFB1146:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1146:
	.size	SSL_COMP_get_name, .-SSL_COMP_get_name
	.p2align 4
	.globl	SSL_COMP_get0_name
	.type	SSL_COMP_get0_name, @function
SSL_COMP_get0_name:
.LFB1164:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1164:
	.size	SSL_COMP_get0_name, .-SSL_COMP_get0_name
	.p2align 4
	.globl	SSL_COMP_get_id
	.type	SSL_COMP_get_id, @function
SSL_COMP_get_id:
.LFB1148:
	.cfi_startproc
	endbr64
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE1148:
	.size	SSL_COMP_get_id, .-SSL_COMP_get_id
	.p2align 4
	.globl	ssl_get_cipher_by_char
	.type	ssl_get_cipher_by_char, @function
ssl_get_cipher_by_char:
.LFB1149:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%edx, %ebx
	subq	$8, %rsp
	movq	8(%r8), %rax
	call	*144(%rax)
	testq	%rax, %rax
	je	.L1084
	testl	%ebx, %ebx
	jne	.L1084
	movl	(%rax), %edx
	testl	%edx, %edx
	movl	$0, %edx
	cmove	%rdx, %rax
.L1084:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1149:
	.size	ssl_get_cipher_by_char, .-ssl_get_cipher_by_char
	.p2align 4
	.globl	SSL_CIPHER_find
	.type	SSL_CIPHER_find, @function
SSL_CIPHER_find:
.LFB1150:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	8(%r8), %rax
	jmp	*144(%rax)
	.cfi_endproc
.LFE1150:
	.size	SSL_CIPHER_find, .-SSL_CIPHER_find
	.p2align 4
	.globl	SSL_CIPHER_get_cipher_nid
	.type	SSL_CIPHER_get_cipher_nid, @function
SSL_CIPHER_get_cipher_nid:
.LFB1151:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1096
	movl	36(%rdi), %ecx
	xorl	%eax, %eax
	leaq	ssl_cipher_table_cipher(%rip), %rdx
	.p2align 4,,10
	.p2align 3
.L1095:
	cmpl	(%rdx,%rax,8), %ecx
	je	.L1098
	addq	$1, %rax
	cmpq	$22, %rax
	jne	.L1095
.L1096:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1098:
	cltq
	movl	4(%rdx,%rax,8), %eax
	ret
	.cfi_endproc
.LFE1151:
	.size	SSL_CIPHER_get_cipher_nid, .-SSL_CIPHER_get_cipher_nid
	.p2align 4
	.globl	SSL_CIPHER_get_digest_nid
	.type	SSL_CIPHER_get_digest_nid, @function
SSL_CIPHER_get_digest_nid:
.LFB1152:
	.cfi_startproc
	endbr64
	movl	40(%rdi), %eax
	cmpl	$1, %eax
	je	.L1102
	cmpl	$2, %eax
	je	.L1103
	cmpl	$4, %eax
	je	.L1104
	cmpl	$8, %eax
	je	.L1105
	cmpl	$16, %eax
	je	.L1106
	cmpl	$32, %eax
	je	.L1107
	cmpl	$128, %eax
	je	.L1108
	cmpl	$256, %eax
	je	.L1109
	cmpl	$512, %eax
	je	.L1110
	xorl	%r8d, %r8d
	movl	$9, %edx
	testl	%eax, %eax
	je	.L1100
	movl	%r8d, %eax
	ret
.L1110:
	movl	$8, %edx
.L1100:
	leaq	ssl_cipher_table_mac(%rip), %rax
	movl	4(%rax,%rdx,8), %r8d
	movl	%r8d, %eax
	ret
.L1102:
	xorl	%edx, %edx
	jmp	.L1100
.L1103:
	movl	$1, %edx
	jmp	.L1100
.L1104:
	movl	$2, %edx
	jmp	.L1100
.L1105:
	movl	$3, %edx
	jmp	.L1100
.L1106:
	movl	$4, %edx
	jmp	.L1100
.L1107:
	movl	$5, %edx
	jmp	.L1100
.L1108:
	movl	$6, %edx
	jmp	.L1100
.L1109:
	movl	$7, %edx
	jmp	.L1100
	.cfi_endproc
.LFE1152:
	.size	SSL_CIPHER_get_digest_nid, .-SSL_CIPHER_get_digest_nid
	.p2align 4
	.globl	SSL_CIPHER_get_kx_nid
	.type	SSL_CIPHER_get_kx_nid, @function
SSL_CIPHER_get_kx_nid:
.LFB1153:
	.cfi_startproc
	endbr64
	movl	28(%rdi), %eax
	cmpl	$1, %eax
	je	.L1115
	cmpl	$4, %eax
	je	.L1116
	cmpl	$2, %eax
	je	.L1117
	cmpl	$128, %eax
	je	.L1118
	cmpl	$256, %eax
	je	.L1119
	cmpl	$64, %eax
	je	.L1120
	cmpl	$8, %eax
	je	.L1121
	cmpl	$32, %eax
	je	.L1122
	cmpl	$16, %eax
	je	.L1123
	xorl	%r8d, %r8d
	movl	$9, %edx
	testl	%eax, %eax
	je	.L1113
	movl	%r8d, %eax
	ret
.L1123:
	movl	$8, %edx
.L1113:
	leaq	ssl_cipher_table_kx(%rip), %rax
	movl	4(%rax,%rdx,8), %r8d
	movl	%r8d, %eax
	ret
.L1115:
	xorl	%edx, %edx
	jmp	.L1113
.L1116:
	movl	$1, %edx
	jmp	.L1113
.L1117:
	movl	$2, %edx
	jmp	.L1113
.L1118:
	movl	$3, %edx
	jmp	.L1113
.L1119:
	movl	$4, %edx
	jmp	.L1113
.L1120:
	movl	$5, %edx
	jmp	.L1113
.L1121:
	movl	$6, %edx
	jmp	.L1113
.L1122:
	movl	$7, %edx
	jmp	.L1113
	.cfi_endproc
.LFE1153:
	.size	SSL_CIPHER_get_kx_nid, .-SSL_CIPHER_get_kx_nid
	.p2align 4
	.globl	SSL_CIPHER_get_auth_nid
	.type	SSL_CIPHER_get_auth_nid, @function
SSL_CIPHER_get_auth_nid:
.LFB1154:
	.cfi_startproc
	endbr64
	movl	32(%rdi), %eax
	cmpl	$1, %eax
	je	.L1128
	cmpl	$8, %eax
	je	.L1129
	cmpl	$16, %eax
	je	.L1130
	cmpl	$2, %eax
	je	.L1131
	cmpl	$32, %eax
	je	.L1132
	cmpl	$128, %eax
	je	.L1133
	cmpl	$64, %eax
	je	.L1134
	cmpl	$4, %eax
	je	.L1135
	xorl	%r8d, %r8d
	movl	$8, %edx
	testl	%eax, %eax
	je	.L1126
	movl	%r8d, %eax
	ret
.L1135:
	movl	$7, %edx
.L1126:
	leaq	ssl_cipher_table_auth(%rip), %rax
	movl	4(%rax,%rdx,8), %r8d
	movl	%r8d, %eax
	ret
.L1128:
	xorl	%edx, %edx
	jmp	.L1126
.L1129:
	movl	$1, %edx
	jmp	.L1126
.L1130:
	movl	$2, %edx
	jmp	.L1126
.L1131:
	movl	$3, %edx
	jmp	.L1126
.L1132:
	movl	$4, %edx
	jmp	.L1126
.L1133:
	movl	$5, %edx
	jmp	.L1126
.L1134:
	movl	$6, %edx
	jmp	.L1126
	.cfi_endproc
.LFE1154:
	.size	SSL_CIPHER_get_auth_nid, .-SSL_CIPHER_get_auth_nid
	.p2align 4
	.globl	SSL_CIPHER_get_handshake_digest
	.type	SSL_CIPHER_get_handshake_digest, @function
SSL_CIPHER_get_handshake_digest:
.LFB1155:
	.cfi_startproc
	endbr64
	movzbl	64(%rdi), %eax
	cmpl	$11, %eax
	jg	.L1139
	leaq	ssl_digest_methods(%rip), %rdx
	movq	(%rdx,%rax,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1139:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1155:
	.size	SSL_CIPHER_get_handshake_digest, .-SSL_CIPHER_get_handshake_digest
	.p2align 4
	.globl	SSL_CIPHER_is_aead
	.type	SSL_CIPHER_is_aead, @function
SSL_CIPHER_is_aead:
.LFB1156:
	.cfi_startproc
	endbr64
	movl	40(%rdi), %eax
	shrl	$6, %eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE1156:
	.size	SSL_CIPHER_is_aead, .-SSL_CIPHER_is_aead
	.p2align 4
	.globl	ssl_cipher_get_overhead
	.type	ssl_cipher_get_overhead, @function
ssl_cipher_get_overhead:
.LFB1157:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	36(%rdi), %eax
	testl	$3207168, %eax
	je	.L1172
	movl	$24, %ebx
	xorl	%eax, %eax
	xorl	%edi, %edi
	xorl	%r12d, %r12d
.L1142:
	movq	%r12, (%rsi)
	movq	%rdi, (%rdx)
	movq	%rax, (%rcx)
	movl	$1, %eax
	movq	%rbx, (%r8)
.L1141:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1172:
	.cfi_restore_state
	testl	$720896, %eax
	je	.L1173
	movl	$16, %ebx
	xorl	%eax, %eax
	xorl	%edi, %edi
	xorl	%r12d, %r12d
	jmp	.L1142
	.p2align 4,,10
	.p2align 3
.L1173:
	movl	40(%rdi), %eax
	movq	%rdi, %rbx
	testb	$64, %al
	jne	.L1147
	cmpl	$1, %eax
	je	.L1153
	cmpl	$2, %eax
	je	.L1154
	cmpl	$4, %eax
	je	.L1155
	cmpl	$8, %eax
	je	.L1156
	cmpl	$16, %eax
	je	.L1157
	cmpl	$32, %eax
	je	.L1158
	cmpl	$128, %eax
	je	.L1159
	cmpl	$256, %eax
	je	.L1160
	cmpl	$512, %eax
	je	.L1161
	xorl	%edi, %edi
	testl	%eax, %eax
	je	.L1174
.L1146:
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	movq	%rdx, -48(%rbp)
	movq	%rsi, -40(%rbp)
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1147
	call	EVP_MD_size@PLT
	movl	36(%rbx), %edi
	movq	-40(%rbp), %rsi
	movq	-48(%rbp), %rdx
	movq	-56(%rbp), %rcx
	movslq	%eax, %r12
	cmpl	$32, %edi
	movq	-64(%rbp), %r8
	je	.L1163
	xorl	%eax, %eax
	leaq	ssl_cipher_table_cipher(%rip), %r9
	.p2align 4,,10
	.p2align 3
.L1150:
	cmpl	(%r9,%rax,8), %edi
	je	.L1175
	addq	$1, %rax
	cmpq	$22, %rax
	jne	.L1150
	xorl	%edi, %edi
.L1149:
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	movq	%rdx, -48(%rbp)
	movq	%rsi, -40(%rbp)
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_cipherbyname@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1147
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpq	$2, %rax
	jne	.L1147
	movq	%r13, %rdi
	call	EVP_CIPHER_iv_length@PLT
	movq	%r13, %rdi
	movslq	%eax, %rbx
	call	EVP_CIPHER_block_size@PLT
	movq	-40(%rbp), %rsi
	movq	-48(%rbp), %rdx
	movl	$1, %edi
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %r8
	cltq
	jmp	.L1142
	.p2align 4,,10
	.p2align 3
.L1147:
	xorl	%eax, %eax
	jmp	.L1141
.L1175:
	cltq
	movl	4(%r9,%rax,8), %edi
	jmp	.L1149
.L1163:
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	xorl	%edi, %edi
	jmp	.L1142
.L1153:
	xorl	%edi, %edi
	.p2align 4,,10
	.p2align 3
.L1145:
	leaq	ssl_cipher_table_mac(%rip), %rax
	movl	4(%rax,%rdi,8), %edi
	jmp	.L1146
.L1154:
	movl	$1, %edi
	jmp	.L1145
.L1155:
	movl	$2, %edi
	jmp	.L1145
.L1156:
	movl	$3, %edi
	jmp	.L1145
.L1157:
	movl	$4, %edi
	jmp	.L1145
.L1158:
	movl	$5, %edi
	jmp	.L1145
.L1159:
	movl	$6, %edi
	jmp	.L1145
.L1160:
	movl	$7, %edi
	jmp	.L1145
.L1174:
	movl	$9, %edi
	jmp	.L1145
.L1161:
	movl	$8, %edi
	jmp	.L1145
	.cfi_endproc
.LFE1157:
	.size	ssl_cipher_get_overhead, .-ssl_cipher_get_overhead
	.p2align 4
	.globl	ssl_cert_is_disabled
	.type	ssl_cert_is_disabled, @function
ssl_cert_is_disabled:
.LFB1158:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ssl_cert_lookup_by_idx@PLT
	testq	%rax, %rax
	je	.L1178
	movl	4(%rax), %eax
	testl	%eax, disabled_auth_mask(%rip)
	setne	%al
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1178:
	.cfi_restore_state
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1158:
	.size	ssl_cert_is_disabled, .-ssl_cert_is_disabled
	.local	disabled_auth_mask
	.comm	disabled_auth_mask,4,4
	.local	disabled_mkey_mask
	.comm	disabled_mkey_mask,4,4
	.local	disabled_mac_mask
	.comm	disabled_mac_mask,4,4
	.local	disabled_enc_mask
	.comm	disabled_enc_mask,4,4
	.section	.rodata.str1.1
.LC69:
	.string	"ALL"
.LC70:
	.string	"COMPLEMENTOFALL"
.LC71:
	.string	"COMPLEMENTOFDEFAULT"
.LC72:
	.string	"kRSA"
.LC73:
	.string	"kEDH"
.LC74:
	.string	"kDHE"
.LC75:
	.string	"kEECDH"
.LC76:
	.string	"kECDHE"
.LC77:
	.string	"kPSK"
.LC78:
	.string	"kRSAPSK"
.LC79:
	.string	"kECDHEPSK"
.LC80:
	.string	"kDHEPSK"
.LC81:
	.string	"kSRP"
.LC82:
	.string	"kGOST"
.LC83:
	.string	"aRSA"
.LC84:
	.string	"aDSS"
.LC85:
	.string	"aNULL"
.LC86:
	.string	"aECDSA"
.LC87:
	.string	"aPSK"
.LC88:
	.string	"aGOST01"
.LC89:
	.string	"aGOST12"
.LC90:
	.string	"aGOST"
.LC91:
	.string	"aSRP"
.LC92:
	.string	"EDH"
.LC93:
	.string	"DHE"
.LC94:
	.string	"EECDH"
.LC95:
	.string	"ECDHE"
.LC96:
	.string	"NULL"
.LC97:
	.string	"ADH"
.LC98:
	.string	"AECDH"
.LC99:
	.string	"3DES"
.LC100:
	.string	"RC4"
.LC101:
	.string	"RC2"
.LC102:
	.string	"IDEA"
.LC103:
	.string	"SEED"
.LC104:
	.string	"eNULL"
.LC105:
	.string	"AES128"
.LC106:
	.string	"AES256"
.LC107:
	.string	"AES"
.LC108:
	.string	"AESGCM"
.LC109:
	.string	"AESCCM"
.LC110:
	.string	"AESCCM8"
.LC111:
	.string	"CAMELLIA128"
.LC112:
	.string	"CAMELLIA256"
.LC113:
	.string	"CAMELLIA"
.LC114:
	.string	"CHACHA20"
.LC115:
	.string	"ARIA"
.LC116:
	.string	"ARIAGCM"
.LC117:
	.string	"ARIA128"
.LC118:
	.string	"ARIA256"
.LC119:
	.string	"SHA"
.LC120:
	.string	"GOST89MAC"
.LC121:
	.string	"SSLv3"
.LC122:
	.string	"TLSv1"
.LC123:
	.string	"TLSv1.2"
.LC124:
	.string	"LOW"
.LC125:
	.string	"MEDIUM"
.LC126:
	.string	"HIGH"
.LC127:
	.string	"FIPS"
.LC128:
	.string	"EDH-DSS-DES-CBC3-SHA"
.LC129:
	.string	"EDH-RSA-DES-CBC3-SHA"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	cipher_aliases, @object
	.size	cipher_aliases, 6080
cipher_aliases:
	.long	0
	.zero	4
	.quad	.LC69
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	-33
	.zero	40
	.long	0
	.zero	4
	.quad	.LC70
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	32
	.zero	40
	.long	0
	.zero	4
	.quad	.LC71
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	32
	.zero	16
	.long	0
	.zero	4
	.quad	.LC72
	.quad	0
	.long	0
	.long	1
	.zero	48
	.long	0
	.zero	4
	.quad	.LC73
	.quad	0
	.long	0
	.long	2
	.zero	48
	.long	0
	.zero	4
	.quad	.LC74
	.quad	0
	.long	0
	.long	2
	.zero	48
	.long	0
	.zero	4
	.quad	.LC27
	.quad	0
	.long	0
	.long	2
	.zero	48
	.long	0
	.zero	4
	.quad	.LC75
	.quad	0
	.long	0
	.long	4
	.zero	48
	.long	0
	.zero	4
	.quad	.LC76
	.quad	0
	.long	0
	.long	4
	.zero	48
	.long	0
	.zero	4
	.quad	.LC24
	.quad	0
	.long	0
	.long	4
	.zero	48
	.long	0
	.zero	4
	.quad	.LC77
	.quad	0
	.long	0
	.long	8
	.zero	48
	.long	0
	.zero	4
	.quad	.LC78
	.quad	0
	.long	0
	.long	64
	.zero	48
	.long	0
	.zero	4
	.quad	.LC79
	.quad	0
	.long	0
	.long	128
	.zero	48
	.long	0
	.zero	4
	.quad	.LC80
	.quad	0
	.long	0
	.long	256
	.zero	48
	.long	0
	.zero	4
	.quad	.LC81
	.quad	0
	.long	0
	.long	32
	.zero	48
	.long	0
	.zero	4
	.quad	.LC82
	.quad	0
	.long	0
	.long	16
	.zero	48
	.long	0
	.zero	4
	.quad	.LC83
	.quad	0
	.long	0
	.long	0
	.long	1
	.zero	44
	.long	0
	.zero	4
	.quad	.LC84
	.quad	0
	.long	0
	.long	0
	.long	2
	.zero	44
	.long	0
	.zero	4
	.quad	.LC35
	.quad	0
	.long	0
	.long	0
	.long	2
	.zero	44
	.long	0
	.zero	4
	.quad	.LC85
	.quad	0
	.long	0
	.long	0
	.long	4
	.zero	44
	.long	0
	.zero	4
	.quad	.LC86
	.quad	0
	.long	0
	.long	0
	.long	8
	.zero	44
	.long	0
	.zero	4
	.quad	.LC34
	.quad	0
	.long	0
	.long	0
	.long	8
	.zero	44
	.long	0
	.zero	4
	.quad	.LC87
	.quad	0
	.long	0
	.long	0
	.long	16
	.zero	44
	.long	0
	.zero	4
	.quad	.LC88
	.quad	0
	.long	0
	.long	0
	.long	32
	.zero	44
	.long	0
	.zero	4
	.quad	.LC89
	.quad	0
	.long	0
	.long	0
	.long	128
	.zero	44
	.long	0
	.zero	4
	.quad	.LC90
	.quad	0
	.long	0
	.long	0
	.long	160
	.zero	44
	.long	0
	.zero	4
	.quad	.LC91
	.quad	0
	.long	0
	.long	0
	.long	64
	.zero	44
	.long	0
	.zero	4
	.quad	.LC92
	.quad	0
	.long	0
	.long	2
	.long	-5
	.zero	44
	.long	0
	.zero	4
	.quad	.LC93
	.quad	0
	.long	0
	.long	2
	.long	-5
	.zero	44
	.long	0
	.zero	4
	.quad	.LC94
	.quad	0
	.long	0
	.long	4
	.long	-5
	.zero	44
	.long	0
	.zero	4
	.quad	.LC95
	.quad	0
	.long	0
	.long	4
	.long	-5
	.zero	44
	.long	0
	.zero	4
	.quad	.LC96
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	32
	.zero	40
	.long	0
	.zero	4
	.quad	.LC23
	.quad	0
	.long	0
	.long	1
	.long	1
	.zero	44
	.long	0
	.zero	4
	.quad	.LC97
	.quad	0
	.long	0
	.long	2
	.long	4
	.zero	44
	.long	0
	.zero	4
	.quad	.LC98
	.quad	0
	.long	0
	.long	4
	.long	4
	.zero	44
	.long	0
	.zero	4
	.quad	.LC25
	.quad	0
	.long	0
	.long	456
	.zero	48
	.long	0
	.zero	4
	.quad	.LC29
	.quad	0
	.long	0
	.long	32
	.zero	48
	.long	0
	.zero	4
	.quad	.LC99
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	2
	.zero	40
	.long	0
	.zero	4
	.quad	.LC100
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	4
	.zero	40
	.long	0
	.zero	4
	.quad	.LC101
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	8
	.zero	40
	.long	0
	.zero	4
	.quad	.LC102
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	16
	.zero	40
	.long	0
	.zero	4
	.quad	.LC103
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	2048
	.zero	40
	.long	0
	.zero	4
	.quad	.LC104
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	32
	.zero	40
	.long	0
	.zero	4
	.quad	.LC62
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	263168
	.zero	40
	.long	0
	.zero	4
	.quad	.LC105
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	86080
	.zero	40
	.long	0
	.zero	4
	.quad	.LC106
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	172160
	.zero	40
	.long	0
	.zero	4
	.quad	.LC107
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	258240
	.zero	40
	.long	0
	.zero	4
	.quad	.LC108
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	12288
	.zero	40
	.long	0
	.zero	4
	.quad	.LC109
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	245760
	.zero	40
	.long	0
	.zero	4
	.quad	.LC110
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	196608
	.zero	40
	.long	0
	.zero	4
	.quad	.LC111
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	256
	.zero	40
	.long	0
	.zero	4
	.quad	.LC112
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	512
	.zero	40
	.long	0
	.zero	4
	.quad	.LC113
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	768
	.zero	40
	.long	0
	.zero	4
	.quad	.LC114
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	524288
	.zero	40
	.long	0
	.zero	4
	.quad	.LC115
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	3145728
	.zero	40
	.long	0
	.zero	4
	.quad	.LC116
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	3145728
	.zero	40
	.long	0
	.zero	4
	.quad	.LC117
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	1048576
	.zero	40
	.long	0
	.zero	4
	.quad	.LC118
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	2097152
	.zero	40
	.long	0
	.zero	4
	.quad	.LC58
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	1
	.zero	36
	.long	0
	.zero	4
	.quad	.LC64
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	2
	.zero	36
	.long	0
	.zero	4
	.quad	.LC119
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	2
	.zero	36
	.long	0
	.zero	4
	.quad	.LC63
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	4
	.zero	36
	.long	0
	.zero	4
	.quad	.LC120
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	264
	.zero	36
	.long	0
	.zero	4
	.quad	.LC59
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	16
	.zero	36
	.long	0
	.zero	4
	.quad	.LC60
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	32
	.zero	36
	.long	0
	.zero	4
	.quad	.LC37
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	128
	.zero	36
	.long	0
	.zero	4
	.quad	.LC121
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	768
	.zero	32
	.long	0
	.zero	4
	.quad	.LC122
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	769
	.zero	32
	.long	0
	.zero	4
	.quad	.LC68
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	769
	.zero	32
	.long	0
	.zero	4
	.quad	.LC123
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	771
	.zero	32
	.long	0
	.zero	4
	.quad	.LC124
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	2
	.zero	16
	.long	0
	.zero	4
	.quad	.LC125
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	4
	.zero	16
	.long	0
	.zero	4
	.quad	.LC126
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	8
	.zero	16
	.long	0
	.zero	4
	.quad	.LC127
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	-33
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	16
	.zero	16
	.long	0
	.zero	4
	.quad	.LC128
	.quad	0
	.long	0
	.long	2
	.long	2
	.long	2
	.long	2
	.long	0
	.long	0
	.long	0
	.long	0
	.long	24
	.zero	16
	.long	0
	.zero	4
	.quad	.LC129
	.quad	0
	.long	0
	.long	2
	.long	1
	.long	2
	.long	2
	.long	0
	.long	0
	.long	0
	.long	0
	.long	24
	.zero	16
	.local	ssl_mac_secret_size
	.comm	ssl_mac_secret_size,96,32
	.data
	.align 32
	.type	ssl_mac_pkey_id, @object
	.size	ssl_mac_pkey_id, 48
ssl_mac_pkey_id:
	.long	855
	.long	855
	.long	855
	.long	0
	.long	855
	.long	855
	.long	855
	.long	0
	.long	855
	.long	0
	.long	0
	.long	0
	.section	.rodata
	.align 32
	.type	ssl_cipher_table_auth, @object
	.size	ssl_cipher_table_auth, 72
ssl_cipher_table_auth:
	.long	1
	.long	1046
	.long	8
	.long	1047
	.long	16
	.long	1048
	.long	2
	.long	1049
	.long	32
	.long	1050
	.long	128
	.long	1051
	.long	64
	.long	1052
	.long	4
	.long	1053
	.long	0
	.long	1064
	.align 32
	.type	ssl_cipher_table_kx, @object
	.size	ssl_cipher_table_kx, 80
ssl_cipher_table_kx:
	.long	1
	.long	1037
	.long	4
	.long	1038
	.long	2
	.long	1039
	.long	128
	.long	1040
	.long	256
	.long	1041
	.long	64
	.long	1042
	.long	8
	.long	1043
	.long	32
	.long	1044
	.long	16
	.long	1045
	.long	0
	.long	1063
	.local	ssl_digest_methods
	.comm	ssl_digest_methods,96,32
	.align 32
	.type	ssl_cipher_table_mac, @object
	.size	ssl_cipher_table_mac, 96
ssl_cipher_table_mac:
	.long	1
	.long	4
	.long	2
	.long	64
	.long	4
	.long	809
	.long	8
	.long	815
	.long	16
	.long	672
	.long	32
	.long	673
	.long	128
	.long	982
	.long	256
	.long	976
	.long	512
	.long	983
	.long	0
	.long	114
	.long	0
	.long	675
	.long	0
	.long	674
	.local	ssl_cipher_methods
	.comm	ssl_cipher_methods,176,32
	.align 32
	.type	ssl_cipher_table_cipher, @object
	.size	ssl_cipher_table_cipher, 176
ssl_cipher_table_cipher:
	.long	1
	.long	31
	.long	2
	.long	44
	.long	4
	.long	5
	.long	8
	.long	37
	.long	16
	.long	34
	.long	32
	.long	0
	.long	64
	.long	419
	.long	128
	.long	427
	.long	256
	.long	751
	.long	512
	.long	753
	.long	1024
	.long	814
	.long	2048
	.long	777
	.long	4096
	.long	895
	.long	8192
	.long	901
	.long	16384
	.long	896
	.long	32768
	.long	902
	.long	65536
	.long	896
	.long	131072
	.long	902
	.long	262144
	.long	975
	.long	524288
	.long	1018
	.long	1048576
	.long	1123
	.long	2097152
	.long	1125
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
