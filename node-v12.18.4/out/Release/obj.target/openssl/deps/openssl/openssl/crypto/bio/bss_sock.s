	.file	"bss_sock.c"
	.text
	.p2align 4
	.type	sock_new, @function
sock_new:
.LFB269:
	.cfi_startproc
	endbr64
	movl	$0, 32(%rdi)
	movl	$1, %eax
	movl	$0, 48(%rdi)
	movq	$0, 56(%rdi)
	movl	$0, 40(%rdi)
	ret
	.cfi_endproc
.LFE269:
	.size	sock_new, .-sock_new
	.p2align 4
	.type	sock_ctrl, @function
sock_ctrl:
.LFB273:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpl	$12, %esi
	jg	.L4
	cmpl	$10, %esi
	jg	.L14
	cmpl	$8, %esi
	je	.L6
	cmpl	$9, %esi
	jne	.L24
	movl	%edx, 36(%rdi)
	movl	$1, %eax
.L3:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	cmpl	$2, %esi
	jne	.L23
	movl	40(%rdi), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	sarl	$11, %eax
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	cmpl	$104, %esi
	je	.L9
	cmpl	$105, %esi
	jne	.L23
	movl	32(%rdi), %eax
	testl	%eax, %eax
	je	.L15
	movslq	48(%rdi), %rax
	testq	%rcx, %rcx
	je	.L3
	movl	%eax, (%rcx)
	movslq	48(%rdi), %rax
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L23:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	testq	%rdi, %rdi
	je	.L11
	movl	36(%rdi), %edi
	testl	%edi, %edi
	je	.L11
	movl	32(%rbx), %esi
	testl	%esi, %esi
	jne	.L25
.L12:
	movl	$0, 32(%rbx)
	movl	$0, 40(%rbx)
.L11:
	movl	(%rcx), %eax
	movl	%edx, 36(%rbx)
	movl	$1, 32(%rbx)
	movl	%eax, 48(%rbx)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movslq	36(%rdi), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	movl	48(%rbx), %edi
	movq	%rcx, -32(%rbp)
	movq	%rdx, -24(%rbp)
	call	BIO_closesocket@PLT
	movq	-32(%rbp), %rcx
	movq	-24(%rbp), %rdx
	jmp	.L12
.L15:
	movq	$-1, %rax
	jmp	.L3
	.cfi_endproc
.LFE273:
	.size	sock_ctrl, .-sock_ctrl
	.p2align 4
	.type	sock_read, @function
sock_read:
.LFB271:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rsi, %rsi
	je	.L26
	movq	%rdi, %r13
	movl	%edx, %ebx
	movq	%rsi, %r12
	call	__errno_location@PLT
	movslq	%ebx, %rdx
	movq	%r12, %rsi
	movl	$0, (%rax)
	movl	48(%r13), %edi
	movq	%rax, %r14
	call	read@PLT
	movl	$15, %esi
	movq	%r13, %rdi
	movq	%rax, %rbx
	movl	%eax, %r15d
	call	BIO_clear_flags@PLT
	testl	%ebx, %ebx
	jle	.L43
.L26:
	addq	$8, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	leal	1(%rbx), %eax
	cmpl	$1, %eax
	jbe	.L44
.L28:
	testl	%ebx, %ebx
	jne	.L26
	orl	$2048, 40(%r13)
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L44:
	movl	(%r14), %eax
	cmpl	$115, %eax
	jg	.L28
	cmpl	$70, %eax
	jg	.L29
	cmpl	$4, %eax
	je	.L30
	cmpl	$11, %eax
	jne	.L28
.L30:
	movl	$9, %esi
	movq	%r13, %rdi
	call	BIO_set_flags@PLT
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L29:
	movabsq	$26456998543361, %rdx
	subl	$71, %eax
	btq	%rax, %rdx
	jnc	.L28
	movl	$9, %esi
	movq	%r13, %rdi
	call	BIO_set_flags@PLT
	jmp	.L26
	.cfi_endproc
.LFE271:
	.size	sock_read, .-sock_read
	.p2align 4
	.type	sock_free, @function
sock_free:
.LFB270:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L48
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	36(%rdi), %edx
	testl	%edx, %edx
	je	.L45
	movl	32(%rdi), %eax
	testl	%eax, %eax
	jne	.L54
.L47:
	movl	$0, 32(%rbx)
	movl	$1, %eax
	movl	$0, 40(%rbx)
.L45:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	48(%rdi), %edi
	call	BIO_closesocket@PLT
	jmp	.L47
	.cfi_endproc
.LFE270:
	.size	sock_free, .-sock_free
	.p2align 4
	.type	sock_puts, @function
sock_puts:
.LFB274:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	call	strlen@PLT
	movq	%rax, %rbx
	call	__errno_location@PLT
	movslq	%ebx, %rdx
	movq	%r13, %rsi
	movl	$0, (%rax)
	movl	48(%r12), %edi
	movq	%rax, %r14
	call	write@PLT
	movl	$15, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	BIO_clear_flags@PLT
	testl	%ebx, %ebx
	jle	.L70
.L55:
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	leal	1(%rbx), %eax
	cmpl	$1, %eax
	ja	.L55
	movl	(%r14), %eax
	cmpl	$115, %eax
	jg	.L55
	cmpl	$70, %eax
	jg	.L57
	cmpl	$4, %eax
	je	.L58
	cmpl	$11, %eax
	jne	.L55
.L58:
	movl	$10, %esi
	movq	%r12, %rdi
	call	BIO_set_flags@PLT
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L57:
	movabsq	$26456998543361, %rdx
	subl	$71, %eax
	btq	%rax, %rdx
	jc	.L58
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE274:
	.size	sock_puts, .-sock_puts
	.p2align 4
	.type	sock_write, @function
sock_write:
.LFB272:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	call	__errno_location@PLT
	movslq	%ebx, %rdx
	movq	%r14, %rsi
	movl	$0, (%rax)
	movl	48(%r12), %edi
	movq	%rax, %r13
	call	write@PLT
	movl	$15, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	BIO_clear_flags@PLT
	testl	%ebx, %ebx
	jle	.L86
.L71:
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	leal	1(%rbx), %eax
	cmpl	$1, %eax
	ja	.L71
	movl	0(%r13), %eax
	cmpl	$115, %eax
	jg	.L71
	cmpl	$70, %eax
	jg	.L73
	cmpl	$4, %eax
	je	.L74
	cmpl	$11, %eax
	jne	.L71
.L74:
	movl	$10, %esi
	movq	%r12, %rdi
	call	BIO_set_flags@PLT
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L73:
	movabsq	$26456998543361, %rdx
	subl	$71, %eax
	btq	%rax, %rdx
	jc	.L74
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE272:
	.size	sock_write, .-sock_write
	.p2align 4
	.globl	BIO_s_socket
	.type	BIO_s_socket, @function
BIO_s_socket:
.LFB267:
	.cfi_startproc
	endbr64
	leaq	methods_sockp(%rip), %rax
	ret
	.cfi_endproc
.LFE267:
	.size	BIO_s_socket, .-BIO_s_socket
	.p2align 4
	.globl	BIO_new_socket
	.type	BIO_new_socket, @function
BIO_new_socket:
.LFB268:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edi, %r13d
	leaq	methods_sockp(%rip), %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$8, %rsp
	call	BIO_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L88
	movslq	%ebx, %rdx
	movl	%r13d, %ecx
	movl	$104, %esi
	movq	%rax, %rdi
	call	BIO_int_ctrl@PLT
.L88:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE268:
	.size	BIO_new_socket, .-BIO_new_socket
	.p2align 4
	.globl	BIO_sock_should_retry
	.type	BIO_sock_should_retry, @function
BIO_sock_should_retry:
.LFB275:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addl	$1, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	xorl	%r12d, %r12d
	subq	$8, %rsp
	cmpl	$1, %edi
	jbe	.L102
.L94:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	call	__errno_location@PLT
	movl	(%rax), %ecx
	cmpl	$115, %ecx
	jg	.L94
	cmpl	$70, %ecx
	jg	.L96
	movl	$1, %r12d
	cmpl	$4, %ecx
	je	.L94
	xorl	%r12d, %r12d
	cmpl	$11, %ecx
	sete	%r12b
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	subl	$71, %ecx
	movl	$1, %eax
	xorl	%r12d, %r12d
	movabsq	$26456998543361, %rdx
	salq	%cl, %rax
	testq	%rdx, %rax
	setne	%r12b
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE275:
	.size	BIO_sock_should_retry, .-BIO_sock_should_retry
	.p2align 4
	.globl	BIO_sock_non_fatal_error
	.type	BIO_sock_non_fatal_error, @function
BIO_sock_non_fatal_error:
.LFB276:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$115, %edi
	jg	.L103
	cmpl	$70, %edi
	jg	.L105
	movl	$1, %eax
	cmpl	$4, %edi
	je	.L103
	xorl	%eax, %eax
	cmpl	$11, %edi
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	leal	-71(%rdi), %ecx
	movl	$1, %eax
	movabsq	$26456998543361, %rdx
	salq	%cl, %rax
	testq	%rdx, %rax
	setne	%al
	movzbl	%al, %eax
.L103:
	ret
	.cfi_endproc
.LFE276:
	.size	BIO_sock_non_fatal_error, .-BIO_sock_non_fatal_error
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"socket"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	methods_sockp, @object
	.size	methods_sockp, 96
methods_sockp:
	.long	1285
	.zero	4
	.quad	.LC0
	.quad	bwrite_conv
	.quad	sock_write
	.quad	bread_conv
	.quad	sock_read
	.quad	sock_puts
	.quad	0
	.quad	sock_ctrl
	.quad	sock_new
	.quad	sock_free
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
