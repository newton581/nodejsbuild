	.file	"des_enc.c"
	.text
	.p2align 4
	.globl	DES_encrypt1
	.type	DES_encrypt1, @function
DES_encrypt1:
.LFB151:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	4(%rdi), %esi
	movl	(%rdi), %r9d
	movl	108(%rax), %r15d
	movl	96(%rax), %r10d
	movl	%esi, %ecx
	movl	%r9d, %edx
	movl	100(%rax), %r13d
	movl	104(%rax), %ebx
	shrl	$4, %ecx
	movl	112(%rax), %r12d
	movl	116(%rax), %r11d
	movl	%r15d, -48(%rbp)
	xorl	%r9d, %ecx
	movl	92(%rax), %r15d
	movl	%r10d, -52(%rbp)
	andl	$252645135, %ecx
	movl	80(%rax), %r10d
	movl	%r13d, -56(%rbp)
	xorl	%ecx, %edx
	sall	$4, %ecx
	movl	84(%rax), %r13d
	movl	%r15d, -64(%rbp)
	movl	%edx, %r8d
	xorl	%ecx, %esi
	movl	%r10d, -68(%rbp)
	shrl	$16, %r8d
	movl	%r13d, -72(%rbp)
	xorl	%esi, %r8d
	movl	%ebx, -44(%rbp)
	movzwl	%r8w, %r9d
	xorl	%r9d, %esi
	sall	$16, %r9d
	movl	%esi, %r8d
	xorl	%edx, %r9d
	movl	%esi, %ecx
	shrl	$2, %r8d
	xorl	%r9d, %r8d
	andl	$858993459, %r8d
	xorl	%r8d, %r9d
	leal	0(,%r8,4), %esi
	movl	124(%rax), %r8d
	movl	%r9d, %edx
	xorl	%ecx, %esi
	shrl	$8, %edx
	xorl	%esi, %edx
	andl	$16711935, %edx
	xorl	%edx, %esi
	sall	$8, %edx
	movl	%esi, %ecx
	xorl	%edx, %r9d
	shrl	%ecx
	xorl	%r9d, %ecx
	andl	$1431655765, %ecx
	xorl	%ecx, %r9d
	addl	%ecx, %ecx
	movl	%r9d, %edx
	movl	88(%rax), %r9d
	xorl	%esi, %ecx
	movl	120(%rax), %esi
#APP
# 31 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $29,%edx
# 0 "" 2
#NO_APP
	movl	%r9d, -60(%rbp)
	movl	72(%rax), %r9d
#APP
# 32 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $29,%ecx
# 0 "" 2
#NO_APP
	movl	%r9d, -76(%rbp)
	movl	76(%rax), %ebx
	movl	64(%rax), %r15d
	movl	68(%rax), %r10d
	movl	60(%rax), %r9d
	movl	56(%rax), %r13d
	movl	%ebx, -80(%rbp)
	movl	48(%rax), %ebx
	movl	%r15d, -84(%rbp)
	movl	%r10d, -88(%rbp)
	movl	52(%rax), %r15d
	movl	40(%rax), %r10d
	movl	%r9d, -96(%rbp)
	movl	%ebx, -100(%rbp)
	movl	44(%rax), %r9d
	movl	36(%rax), %ebx
	movl	%r13d, -92(%rbp)
	movl	%r15d, -104(%rbp)
	movl	32(%rax), %r13d
	movl	%r10d, -108(%rbp)
	movl	24(%rax), %r15d
	movl	28(%rax), %r10d
	movl	%r9d, -112(%rbp)
	movl	%ebx, -120(%rbp)
	movl	16(%rax), %r9d
	movl	20(%rax), %ebx
	movl	%r10d, -128(%rbp)
	movl	%r9d, -132(%rbp)
	movl	12(%rax), %r10d
	movl	%ebx, -136(%rbp)
	movl	8(%rax), %ebx
	movl	%r13d, -116(%rbp)
	movl	%r15d, -124(%rbp)
	movl	(%rax), %r13d
	movl	4(%rax), %r9d
	testl	%r14d, %r14d
	je	.L2
	xorl	%edx, %r13d
	leaq	DES_SPtrans(%rip), %rax
	xorl	%edx, %r9d
	movl	%r13d, %r14d
	movl	%r13d, %r15d
	shrl	$2, %r14d
	shrl	$26, %r15d
	andl	$63, %r14d
	xorl	1536(%rax,%r15,4), %ecx
	xorl	(%rax,%r14,4), %ecx
	movl	%r13d, %r14d
	shrl	$18, %r13d
	shrl	$10, %r14d
	andl	$63, %r13d
	andl	$63, %r14d
	xorl	512(%rax,%r14,4), %ecx
	xorl	1024(%rax,%r13,4), %ecx
#APP
# 40 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r9d
# 0 "" 2
#NO_APP
	movl	%r9d, %r14d
	shrl	$26, %r14d
	movl	%r14d, %r13d
	movl	%r9d, %r14d
	shrl	$2, %r14d
	xorl	1792(%rax,%r13,4), %ecx
	movl	%r14d, %r13d
	movl	%r9d, %r14d
	shrl	$18, %r9d
	andl	$63, %r13d
	shrl	$10, %r14d
	andl	$63, %r9d
	xorl	256(%rax,%r13,4), %ecx
	movl	%r14d, %r13d
	andl	$63, %r13d
	xorl	768(%rax,%r13,4), %ecx
	xorl	1280(%rax,%r9,4), %ecx
	xorl	%ecx, %r10d
	xorl	%ecx, %ebx
#APP
# 41 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r10d
# 0 "" 2
#NO_APP
	movl	%r10d, %r14d
	shrl	$2, %r14d
	movl	%r14d, %r9d
	movl	%r10d, %r14d
	shrl	$26, %r14d
	andl	$63, %r9d
	movl	%r14d, %r13d
	movl	%r10d, %r14d
	xorl	1792(%rax,%r13,4), %edx
	xorl	256(%rax,%r9,4), %edx
	shrl	$10, %r14d
	movl	%r14d, %r9d
	movl	%ebx, %r14d
	shrl	$18, %r10d
	andl	$63, %r9d
	shrl	$26, %r14d
	andl	$63, %r10d
	xorl	768(%rax,%r9,4), %edx
	movl	%r14d, %r9d
	movl	%ebx, %r14d
	xorl	1280(%rax,%r10,4), %edx
	shrl	$2, %r14d
	xorl	1536(%rax,%r9,4), %edx
	movl	%r14d, %r9d
	movl	%ebx, %r14d
	shrl	$18, %ebx
	andl	$63, %r9d
	shrl	$10, %r14d
	andl	$63, %ebx
	xorl	(%rax,%r9,4), %edx
	movl	%r14d, %r9d
	movl	-136(%rbp), %r14d
	andl	$63, %r9d
	xorl	512(%rax,%r9,4), %edx
	movl	-132(%rbp), %r9d
	xorl	1024(%rax,%rbx,4), %edx
	xorl	%edx, %r14d
	xorl	%edx, %r9d
	movl	%r14d, %r10d
	movl	%r9d, %ebx
#APP
# 42 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r10d
# 0 "" 2
#NO_APP
	movl	%r10d, %r14d
	movl	%r10d, %r9d
	shrl	$10, %r14d
	shrl	$2, %r9d
	movl	%r14d, %r13d
	movl	%r10d, %r14d
	andl	$63, %r9d
	shrl	$18, %r10d
	andl	$63, %r13d
	shrl	$26, %r14d
	movl	256(%rax,%r9,4), %r9d
	andl	$63, %r10d
	xorl	768(%rax,%r13,4), %r9d
	movl	%r14d, %r13d
	movl	-124(%rbp), %r14d
	xorl	1792(%rax,%r13,4), %r9d
	xorl	1280(%rax,%r10,4), %r9d
	xorl	%r9d, %ecx
	movl	%ebx, %r9d
	movl	-128(%rbp), %r10d
	shrl	$26, %r9d
	xorl	1536(%rax,%r9,4), %ecx
	movl	%ebx, %r9d
	shrl	$2, %r9d
	andl	$63, %r9d
	xorl	(%rax,%r9,4), %ecx
	movl	%ebx, %r9d
	shrl	$18, %ebx
	shrl	$10, %r9d
	andl	$63, %ebx
	andl	$63, %r9d
	xorl	512(%rax,%r9,4), %ecx
	xorl	1024(%rax,%rbx,4), %ecx
	xorl	%ecx, %r14d
	xorl	%ecx, %r10d
	movl	%r14d, %ebx
#APP
# 43 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r10d
# 0 "" 2
#NO_APP
	movl	%r10d, %r14d
	shrl	$2, %r14d
	movl	%r14d, %r9d
	movl	%r10d, %r14d
	shrl	$10, %r14d
	andl	$63, %r9d
	movl	%r14d, %r13d
	movl	%r10d, %r14d
	shrl	$18, %r10d
	movl	256(%rax,%r9,4), %r9d
	andl	$63, %r13d
	shrl	$26, %r14d
	andl	$63, %r10d
	xorl	768(%rax,%r13,4), %r9d
	movl	%r14d, %r13d
	movl	%ebx, %r14d
	xorl	1792(%rax,%r13,4), %r9d
	shrl	$26, %r14d
	xorl	1280(%rax,%r10,4), %r9d
	xorl	%r9d, %edx
	movl	%r14d, %r9d
	movl	%ebx, %r14d
	shrl	$2, %r14d
	xorl	1536(%rax,%r9,4), %edx
	movl	-116(%rbp), %r13d
	movl	%r14d, %r9d
	movl	%ebx, %r14d
	andl	$63, %r9d
	shrl	$10, %r14d
	xorl	(%rax,%r9,4), %edx
	movl	%r14d, %r9d
	movl	-108(%rbp), %r14d
	andl	$63, %r9d
	xorl	512(%rax,%r9,4), %edx
	shrl	$18, %ebx
	andl	$63, %ebx
	xorl	1024(%rax,%rbx,4), %edx
	xorl	%edx, %r13d
	movl	%r13d, %ebx
	movl	-120(%rbp), %r13d
	xorl	%edx, %r13d
	movl	%r13d, %r10d
#APP
# 44 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r10d
# 0 "" 2
#NO_APP
	movl	%r10d, %r13d
	shrl	$2, %r13d
	movl	%r13d, %r9d
	movl	%r10d, %r13d
	shrl	$10, %r13d
	andl	$63, %r9d
	andl	$63, %r13d
	movl	256(%rax,%r9,4), %r9d
	xorl	768(%rax,%r13,4), %r9d
	movl	%r10d, %r13d
	shrl	$18, %r10d
	shrl	$26, %r13d
	andl	$63, %r10d
	xorl	1792(%rax,%r13,4), %r9d
	movl	%ebx, %r13d
	xorl	1280(%rax,%r10,4), %r9d
	shrl	$26, %r13d
	xorl	%r9d, %ecx
	movl	%r13d, %r9d
	movl	%ebx, %r13d
	shrl	$2, %r13d
	xorl	1536(%rax,%r9,4), %ecx
	movl	%r13d, %r9d
	movl	%ebx, %r13d
	shrl	$18, %ebx
	andl	$63, %r9d
	shrl	$10, %r13d
	andl	$63, %ebx
	xorl	(%rax,%r9,4), %ecx
	movl	%r13d, %r9d
	andl	$63, %r9d
	xorl	512(%rax,%r9,4), %ecx
	xorl	1024(%rax,%rbx,4), %ecx
	xorl	%ecx, %r14d
	movl	%r14d, %ebx
	movl	-112(%rbp), %r14d
	xorl	%ecx, %r14d
	movl	%r14d, %r10d
#APP
# 45 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r10d
# 0 "" 2
#NO_APP
	movl	%r10d, %r14d
	shrl	$2, %r14d
	movl	%r14d, %r9d
	movl	%r10d, %r14d
	shrl	$10, %r14d
	andl	$63, %r9d
	movl	%r14d, %r13d
	movl	%r10d, %r14d
	shrl	$18, %r10d
	movl	256(%rax,%r9,4), %r9d
	andl	$63, %r13d
	shrl	$26, %r14d
	andl	$63, %r10d
	xorl	768(%rax,%r13,4), %r9d
	movl	%r14d, %r13d
	movl	%ebx, %r14d
	xorl	1792(%rax,%r13,4), %r9d
	shrl	$26, %r14d
	xorl	1280(%rax,%r10,4), %r9d
	xorl	%r9d, %edx
	movl	%r14d, %r9d
	movl	%ebx, %r14d
	shrl	$2, %r14d
	xorl	1536(%rax,%r9,4), %edx
	movl	%r14d, %r9d
	movl	%ebx, %r14d
	shrl	$18, %ebx
	andl	$63, %r9d
	shrl	$10, %r14d
	andl	$63, %ebx
	xorl	(%rax,%r9,4), %edx
	movl	%r14d, %r9d
	movl	-104(%rbp), %r14d
	andl	$63, %r9d
	xorl	512(%rax,%r9,4), %edx
	xorl	1024(%rax,%rbx,4), %edx
	xorl	%edx, %r14d
	movl	-100(%rbp), %ebx
	movl	%r14d, %r10d
#APP
# 46 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r10d
# 0 "" 2
#NO_APP
	movl	%r10d, %r14d
	xorl	%edx, %ebx
	shrl	$2, %r14d
	movl	%r14d, %r9d
	movl	%r10d, %r14d
	shrl	$10, %r14d
	andl	$63, %r9d
	movl	%r14d, %r13d
	movl	%r10d, %r14d
	movl	256(%rax,%r9,4), %r9d
	andl	$63, %r13d
	shrl	$26, %r14d
	xorl	768(%rax,%r13,4), %r9d
	movl	%r14d, %r13d
	movl	%ebx, %r14d
	xorl	1792(%rax,%r13,4), %r9d
	shrl	$18, %r10d
	andl	$63, %r10d
	shrl	$26, %r14d
	xorl	1280(%rax,%r10,4), %r9d
	xorl	%r9d, %ecx
	movl	%r14d, %r9d
	movl	%ebx, %r14d
	shrl	$2, %r14d
	xorl	1536(%rax,%r9,4), %ecx
	movl	%r14d, %r9d
	movl	%ebx, %r14d
	shrl	$18, %ebx
	andl	$63, %r9d
	shrl	$10, %r14d
	andl	$63, %ebx
	xorl	(%rax,%r9,4), %ecx
	movl	%r14d, %r9d
	andl	$63, %r9d
	xorl	512(%rax,%r9,4), %ecx
	movl	-96(%rbp), %r14d
	xorl	1024(%rax,%rbx,4), %ecx
	movl	-92(%rbp), %r13d
	xorl	%ecx, %r14d
	movl	-84(%rbp), %r15d
	xorl	%ecx, %r13d
	movl	%r14d, %r10d
#APP
# 47 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r10d
# 0 "" 2
#NO_APP
	movl	%r10d, %r14d
	movl	%r13d, %ebx
	movl	%r10d, %r13d
	shrl	$2, %r14d
	shrl	$10, %r13d
	movl	%r14d, %r9d
	andl	$63, %r13d
	movl	%ebx, %r14d
	andl	$63, %r9d
	shrl	$26, %r14d
	movl	256(%rax,%r9,4), %r9d
	xorl	768(%rax,%r13,4), %r9d
	movl	%r10d, %r13d
	shrl	$18, %r10d
	shrl	$26, %r13d
	andl	$63, %r10d
	xorl	1792(%rax,%r13,4), %r9d
	xorl	1280(%rax,%r10,4), %r9d
	xorl	%r9d, %edx
	movl	%r14d, %r9d
	movl	%ebx, %r14d
	movl	-88(%rbp), %r10d
	shrl	$2, %r14d
	xorl	1536(%rax,%r9,4), %edx
	movl	%r14d, %r9d
	movl	%ebx, %r14d
	andl	$63, %r9d
	shrl	$10, %r14d
	xorl	(%rax,%r9,4), %edx
	shrl	$18, %ebx
	movl	%r14d, %r9d
	movl	-76(%rbp), %r14d
	andl	$63, %r9d
	andl	$63, %ebx
	xorl	512(%rax,%r9,4), %edx
	xorl	1024(%rax,%rbx,4), %edx
	xorl	%edx, %r15d
	xorl	%edx, %r10d
	movl	%r15d, %ebx
#APP
# 48 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r10d
# 0 "" 2
#NO_APP
	movl	%r10d, %r15d
	movl	%r10d, %r13d
	shrl	$2, %r15d
	shrl	$10, %r13d
	movl	%r15d, %r9d
	andl	$63, %r13d
	movl	%ebx, %r15d
	andl	$63, %r9d
	shrl	$26, %r15d
	movl	256(%rax,%r9,4), %r9d
	xorl	768(%rax,%r13,4), %r9d
	movl	%r10d, %r13d
	shrl	$18, %r10d
	shrl	$26, %r13d
	andl	$63, %r10d
	xorl	1792(%rax,%r13,4), %r9d
	xorl	1280(%rax,%r10,4), %r9d
	xorl	%r9d, %ecx
	movl	%r15d, %r9d
	movl	%ebx, %r15d
	shrl	$2, %r15d
	xorl	1536(%rax,%r9,4), %ecx
	movl	%r15d, %r9d
	movl	%ebx, %r15d
	shrl	$18, %ebx
	andl	$63, %r9d
	shrl	$10, %r15d
	andl	$63, %ebx
	xorl	(%rax,%r9,4), %ecx
	movl	%r15d, %r9d
	andl	$63, %r9d
	xorl	512(%rax,%r9,4), %ecx
	xorl	1024(%rax,%rbx,4), %ecx
	xorl	%ecx, %r14d
	movl	%r14d, %ebx
	movl	-80(%rbp), %r14d
	xorl	%ecx, %r14d
	movl	%r14d, %r10d
#APP
# 49 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r10d
# 0 "" 2
#NO_APP
	movl	%r10d, %r14d
	shrl	$2, %r14d
	movl	%r14d, %r9d
	movl	%r10d, %r14d
	shrl	$10, %r14d
	andl	$63, %r9d
	movl	%r14d, %r13d
	movl	%r10d, %r14d
	shrl	$18, %r10d
	movl	256(%rax,%r9,4), %r9d
	andl	$63, %r13d
	shrl	$26, %r14d
	andl	$63, %r10d
	xorl	768(%rax,%r13,4), %r9d
	movl	%r14d, %r13d
	movl	1280(%rax,%r10,4), %r14d
	xorl	1792(%rax,%r13,4), %r9d
	xorl	%r9d, %r14d
	movl	%r14d, %r10d
	xorl	%edx, %r10d
	movl	%ebx, %edx
	shrl	$26, %edx
	xorl	1536(%rax,%rdx,4), %r10d
	movl	%ebx, %edx
	shrl	$2, %edx
	andl	$63, %edx
	xorl	(%rax,%rdx,4), %r10d
	movl	%ebx, %edx
	shrl	$18, %ebx
	shrl	$10, %edx
	andl	$63, %ebx
	andl	$63, %edx
	xorl	512(%rax,%rdx,4), %r10d
	xorl	1024(%rax,%rbx,4), %r10d
	movl	-68(%rbp), %ebx
	movl	-72(%rbp), %edx
	xorl	%r10d, %edx
	xorl	%r10d, %ebx
#APP
# 50 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%edx
# 0 "" 2
#NO_APP
	movl	%edx, %r14d
	movl	%edx, %r13d
	shrl	$2, %r14d
	shrl	$10, %r13d
	movl	%r14d, %r9d
	movl	-60(%rbp), %r14d
	andl	$63, %r9d
	andl	$63, %r13d
	movl	256(%rax,%r9,4), %r9d
	xorl	768(%rax,%r13,4), %r9d
	movl	%edx, %r13d
	shrl	$18, %edx
	shrl	$26, %r13d
	andl	$63, %edx
	xorl	1792(%rax,%r13,4), %r9d
	xorl	1280(%rax,%rdx,4), %r9d
	movl	%ebx, %edx
	shrl	$26, %edx
	xorl	%r9d, %ecx
	xorl	1536(%rax,%rdx,4), %ecx
	movl	%ebx, %edx
	shrl	$2, %edx
	andl	$63, %edx
	xorl	(%rax,%rdx,4), %ecx
	movl	%ebx, %edx
	shrl	$18, %ebx
	shrl	$10, %edx
	andl	$63, %ebx
	andl	$63, %edx
	xorl	512(%rax,%rdx,4), %ecx
	xorl	1024(%rax,%rbx,4), %ecx
	xorl	%ecx, %r14d
	movl	%r14d, %ebx
	movl	-64(%rbp), %r14d
	xorl	%ecx, %r14d
	movl	%r14d, %r9d
#APP
# 51 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r9d
# 0 "" 2
#NO_APP
	movl	%r9d, %r14d
	shrl	$2, %r14d
	movl	%r14d, %edx
	movl	%r9d, %r14d
	shrl	$10, %r14d
	andl	$63, %edx
	movl	%r14d, %r13d
	movl	%r9d, %r14d
	shrl	$18, %r9d
	movl	256(%rax,%rdx,4), %edx
	andl	$63, %r13d
	shrl	$26, %r14d
	andl	$63, %r9d
	xorl	768(%rax,%r13,4), %edx
	movl	%r14d, %r13d
	movl	%ebx, %r14d
	xorl	1792(%rax,%r13,4), %edx
	xorl	1280(%rax,%r9,4), %edx
	xorl	%edx, %r10d
	shrl	$26, %r14d
	movl	%r14d, %edx
	movl	%ebx, %r14d
	shrl	$2, %r14d
	xorl	1536(%rax,%rdx,4), %r10d
	movl	%r14d, %edx
	movl	%ebx, %r14d
	shrl	$18, %ebx
	andl	$63, %edx
	shrl	$10, %r14d
	andl	$63, %ebx
	xorl	(%rax,%rdx,4), %r10d
	movl	%r14d, %edx
	andl	$63, %edx
	xorl	512(%rax,%rdx,4), %r10d
	movl	-52(%rbp), %edx
	xorl	1024(%rax,%rbx,4), %r10d
	xorl	%r10d, %edx
	movl	%edx, %r9d
	movl	-56(%rbp), %edx
	xorl	%r10d, %edx
#APP
# 52 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%edx
# 0 "" 2
#NO_APP
	movl	%edx, %r14d
	movl	%edx, %r13d
	shrl	$2, %r14d
	shrl	$10, %r13d
	movl	%r14d, %ebx
	andl	$63, %r13d
	andl	$63, %ebx
	movl	256(%rax,%rbx,4), %ebx
	xorl	768(%rax,%r13,4), %ebx
	movl	%edx, %r13d
	shrl	$18, %edx
	andl	$63, %edx
	shrl	$26, %r13d
	movl	1280(%rax,%rdx,4), %r14d
	xorl	1792(%rax,%r13,4), %ebx
	xorl	%ebx, %r14d
	movl	-44(%rbp), %ebx
	movl	%r14d, %edx
	xorl	%ecx, %edx
	movl	%r9d, %ecx
	shrl	$26, %ecx
	xorl	1536(%rax,%rcx,4), %edx
	movl	%r9d, %ecx
	shrl	$2, %ecx
	andl	$63, %ecx
	xorl	(%rax,%rcx,4), %edx
	movl	%r9d, %ecx
	shrl	$10, %ecx
	andl	$63, %ecx
	shrl	$18, %r9d
	andl	$63, %r9d
	xorl	512(%rax,%rcx,4), %edx
	xorl	1024(%rax,%r9,4), %edx
	movl	-48(%rbp), %r9d
	xorl	%edx, %ebx
	xorl	%edx, %r9d
#APP
# 53 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r9d
# 0 "" 2
#NO_APP
	movl	%r9d, %ecx
	movl	%r9d, %r13d
	shrl	$2, %ecx
	shrl	$10, %r13d
	andl	$63, %r13d
	andl	$63, %ecx
	movl	256(%rax,%rcx,4), %ecx
	xorl	768(%rax,%r13,4), %ecx
	movl	%r9d, %r13d
	shrl	$18, %r9d
	andl	$63, %r9d
	shrl	$26, %r13d
	xorl	1792(%rax,%r13,4), %ecx
	xorl	1280(%rax,%r9,4), %ecx
	movl	%ebx, %r9d
	shrl	$26, %r9d
	xorl	%r10d, %ecx
	xorl	1536(%rax,%r9,4), %ecx
	movl	%ebx, %r9d
	shrl	$2, %r9d
	andl	$63, %r9d
	xorl	(%rax,%r9,4), %ecx
	movl	%ebx, %r9d
	shrl	$18, %ebx
	shrl	$10, %r9d
	andl	$63, %ebx
	andl	$63, %r9d
	xorl	512(%rax,%r9,4), %ecx
	xorl	1024(%rax,%rbx,4), %ecx
	xorl	%ecx, %r12d
	movl	%ecx, %r9d
	xorl	%ecx, %r11d
#APP
# 54 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r11d
# 0 "" 2
#NO_APP
	movl	%r11d, %ecx
	movl	%r11d, %r10d
	shrl	$2, %ecx
	shrl	$10, %r10d
	andl	$63, %r10d
	andl	$63, %ecx
	movl	256(%rax,%rcx,4), %ecx
	xorl	768(%rax,%r10,4), %ecx
	movl	%r11d, %r10d
	shrl	$18, %r11d
	shrl	$26, %r10d
	andl	$63, %r11d
	xorl	1792(%rax,%r10,4), %ecx
	xorl	1280(%rax,%r11,4), %ecx
	xorl	%ecx, %edx
	movl	%r12d, %ecx
	shrl	$26, %ecx
	xorl	1536(%rax,%rcx,4), %edx
	movl	%r12d, %ecx
	shrl	$2, %ecx
	andl	$63, %ecx
	xorl	(%rax,%rcx,4), %edx
	movl	%r12d, %ecx
	shrl	$18, %r12d
	shrl	$10, %ecx
	andl	$63, %r12d
	andl	$63, %ecx
	xorl	512(%rax,%rcx,4), %edx
	xorl	1024(%rax,%r12,4), %edx
	xorl	%edx, %r8d
	xorl	%edx, %esi
#APP
# 55 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r8d
# 0 "" 2
#NO_APP
	movl	%r8d, %ecx
	movl	%r8d, %r10d
	shrl	$2, %ecx
	shrl	$10, %r10d
	andl	$63, %r10d
	andl	$63, %ecx
	movl	256(%rax,%rcx,4), %ecx
	xorl	768(%rax,%r10,4), %ecx
	movl	%r8d, %r10d
	shrl	$26, %r10d
	xorl	1792(%rax,%r10,4), %ecx
	shrl	$18, %r8d
	andl	$63, %r8d
	xorl	1280(%rax,%r8,4), %ecx
	movl	%esi, %r8d
	shrl	$26, %r8d
	xorl	%r9d, %ecx
	xorl	1536(%rax,%r8,4), %ecx
	movl	%esi, %r8d
	shrl	$2, %r8d
	andl	$63, %r8d
	xorl	(%rax,%r8,4), %ecx
	movl	%esi, %r8d
	shrl	$18, %esi
	shrl	$10, %r8d
	andl	$63, %esi
	andl	$63, %r8d
	xorl	512(%rax,%r8,4), %ecx
	xorl	1024(%rax,%rsi,4), %ecx
	movl	%ecx, %eax
.L3:
#APP
# 77 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $3,%eax
# 0 "" 2
#NO_APP
	movl	%eax, %esi
	shrl	%esi
#APP
# 76 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $3,%edx
# 0 "" 2
#NO_APP
	xorl	%edx, %esi
	andl	$1431655765, %esi
	xorl	%esi, %edx
	addl	%esi, %esi
	movl	%edx, %ecx
	xorl	%esi, %eax
	movl	%edx, %r8d
	shrl	$8, %ecx
	xorl	%eax, %ecx
	andl	$16711935, %ecx
	xorl	%ecx, %eax
	movl	%ecx, %edx
	sall	$8, %edx
	movl	%eax, %ecx
	movl	%eax, %esi
	xorl	%r8d, %edx
	shrl	$2, %ecx
	xorl	%edx, %ecx
	andl	$858993459, %ecx
	xorl	%ecx, %edx
	leal	0(,%rcx,4), %eax
	movl	%edx, %ecx
	xorl	%esi, %eax
	shrl	$16, %ecx
	xorl	%eax, %ecx
	movzwl	%cx, %ecx
	xorl	%ecx, %eax
	sall	$16, %ecx
	xorl	%ecx, %edx
	movl	%eax, %ecx
	shrl	$4, %ecx
	xorl	%edx, %ecx
	andl	$252645135, %ecx
	xorl	%ecx, %edx
	sall	$4, %ecx
	xorl	%ecx, %eax
	movl	%edx, (%rdi)
	movl	%eax, 4(%rdi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	xorl	%edx, %esi
	leaq	DES_SPtrans(%rip), %rax
	xorl	%edx, %r8d
	movl	%esi, %r14d
	movl	%esi, %r15d
	shrl	$2, %r14d
	shrl	$26, %r15d
	andl	$63, %r14d
	xorl	1536(%rax,%r15,4), %ecx
	xorl	(%rax,%r14,4), %ecx
	movl	%esi, %r14d
	shrl	$18, %esi
	shrl	$10, %r14d
	andl	$63, %esi
	andl	$63, %r14d
#APP
# 57 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r8d
# 0 "" 2
#NO_APP
	xorl	512(%rax,%r14,4), %ecx
	xorl	1024(%rax,%rsi,4), %ecx
	movl	%r8d, %esi
	shrl	$26, %esi
	xorl	1792(%rax,%rsi,4), %ecx
	movl	%r8d, %esi
	shrl	$2, %esi
	andl	$63, %esi
	xorl	256(%rax,%rsi,4), %ecx
	movl	%r8d, %esi
	shrl	$18, %r8d
	shrl	$10, %esi
	andl	$63, %r8d
	andl	$63, %esi
	xorl	768(%rax,%rsi,4), %ecx
	xorl	1280(%rax,%r8,4), %ecx
	xorl	%ecx, %r11d
	xorl	%ecx, %r12d
#APP
# 58 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r11d
# 0 "" 2
#NO_APP
	movl	%r11d, %esi
	movl	%r11d, %r8d
	shrl	$2, %esi
	shrl	$26, %r8d
	andl	$63, %esi
	xorl	1792(%rax,%r8,4), %edx
	xorl	256(%rax,%rsi,4), %edx
	movl	%r11d, %esi
	shrl	$18, %r11d
	shrl	$10, %esi
	andl	$63, %r11d
	andl	$63, %esi
	xorl	768(%rax,%rsi,4), %edx
	movl	%r12d, %esi
	xorl	1280(%rax,%r11,4), %edx
	shrl	$26, %esi
	movl	-44(%rbp), %r11d
	xorl	1536(%rax,%rsi,4), %edx
	movl	%r12d, %esi
	movl	%edx, %r8d
	shrl	$2, %esi
	movl	%r12d, %edx
	shrl	$10, %edx
	andl	$63, %esi
	shrl	$18, %r12d
	xorl	(%rax,%rsi,4), %r8d
	andl	$63, %edx
	andl	$63, %r12d
	movl	-48(%rbp), %esi
	xorl	512(%rax,%rdx,4), %r8d
	xorl	1024(%rax,%r12,4), %r8d
	xorl	%r8d, %esi
	xorl	%r8d, %r11d
#APP
# 59 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%esi
# 0 "" 2
#NO_APP
	movl	%esi, %edx
	movl	%esi, %r12d
	shrl	$2, %edx
	shrl	$10, %r12d
	andl	$63, %r12d
	andl	$63, %edx
	movl	256(%rax,%rdx,4), %edx
	xorl	768(%rax,%r12,4), %edx
	movl	%esi, %r12d
	shrl	$18, %esi
	shrl	$26, %r12d
	andl	$63, %esi
	xorl	1792(%rax,%r12,4), %edx
	xorl	1280(%rax,%rsi,4), %edx
	xorl	%ecx, %edx
	movl	%r11d, %ecx
	movl	-52(%rbp), %esi
	shrl	$26, %ecx
	xorl	1536(%rax,%rcx,4), %edx
	movl	%r11d, %ecx
	shrl	$2, %ecx
	andl	$63, %ecx
	xorl	(%rax,%rcx,4), %edx
	movl	%r11d, %ecx
	shrl	$18, %r11d
	shrl	$10, %ecx
	andl	$63, %r11d
	andl	$63, %ecx
	xorl	512(%rax,%rcx,4), %edx
	xorl	1024(%rax,%r11,4), %edx
	movl	-56(%rbp), %r11d
	xorl	%edx, %esi
	xorl	%edx, %r11d
#APP
# 60 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r11d
# 0 "" 2
#NO_APP
	movl	%r11d, %ecx
	movl	%r11d, %r12d
	shrl	$2, %ecx
	shrl	$10, %r12d
	andl	$63, %r12d
	andl	$63, %ecx
	movl	256(%rax,%rcx,4), %ecx
	xorl	768(%rax,%r12,4), %ecx
	movl	%r11d, %r12d
	shrl	$18, %r11d
	shrl	$26, %r12d
	andl	$63, %r11d
	xorl	1792(%rax,%r12,4), %ecx
	xorl	1280(%rax,%r11,4), %ecx
	xorl	%ecx, %r8d
	movl	%esi, %ecx
	shrl	$26, %ecx
	xorl	1536(%rax,%rcx,4), %r8d
	movl	%esi, %ecx
	movl	-64(%rbp), %r11d
	shrl	$2, %ecx
	andl	$63, %ecx
	xorl	(%rax,%rcx,4), %r8d
	movl	%esi, %ecx
	shrl	$10, %ecx
	andl	$63, %ecx
	xorl	512(%rax,%rcx,4), %r8d
	shrl	$18, %esi
	movl	-60(%rbp), %ecx
	andl	$63, %esi
	xorl	1024(%rax,%rsi,4), %r8d
	xorl	%r8d, %r11d
	xorl	%r8d, %ecx
#APP
# 61 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r11d
# 0 "" 2
#NO_APP
	movl	%r11d, %esi
	movl	%r11d, %r12d
	shrl	$2, %esi
	shrl	$10, %r12d
	andl	$63, %r12d
	andl	$63, %esi
	movl	256(%rax,%rsi,4), %esi
	xorl	768(%rax,%r12,4), %esi
	movl	%r11d, %r12d
	shrl	$18, %r11d
	shrl	$26, %r12d
	andl	$63, %r11d
	xorl	1792(%rax,%r12,4), %esi
	xorl	1280(%rax,%r11,4), %esi
	xorl	%edx, %esi
	movl	%ecx, %edx
	movl	-68(%rbp), %r11d
	shrl	$26, %edx
	xorl	1536(%rax,%rdx,4), %esi
	movl	%ecx, %edx
	shrl	$2, %edx
	andl	$63, %edx
	xorl	(%rax,%rdx,4), %esi
	movl	%ecx, %edx
	shrl	$18, %ecx
	shrl	$10, %edx
	andl	$63, %ecx
	andl	$63, %edx
	xorl	512(%rax,%rdx,4), %esi
	movl	-72(%rbp), %edx
	xorl	1024(%rax,%rcx,4), %esi
	xorl	%esi, %edx
	xorl	%esi, %r11d
#APP
# 62 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%edx
# 0 "" 2
#NO_APP
	movl	%edx, %ecx
	movl	%edx, %r12d
	shrl	$2, %ecx
	shrl	$10, %r12d
	andl	$63, %r12d
	andl	$63, %ecx
	movl	256(%rax,%rcx,4), %ecx
	xorl	768(%rax,%r12,4), %ecx
	movl	%edx, %r12d
	shrl	$18, %edx
	shrl	$26, %r12d
	andl	$63, %edx
	xorl	1792(%rax,%r12,4), %ecx
	xorl	1280(%rax,%rdx,4), %ecx
	movl	%r11d, %edx
	shrl	$26, %edx
	xorl	%ecx, %r8d
	movl	-80(%rbp), %ecx
	xorl	1536(%rax,%rdx,4), %r8d
	movl	%r11d, %edx
	shrl	$2, %edx
	andl	$63, %edx
	xorl	(%rax,%rdx,4), %r8d
	movl	%r11d, %edx
	shrl	$18, %r11d
	shrl	$10, %edx
	andl	$63, %r11d
	andl	$63, %edx
	xorl	512(%rax,%rdx,4), %r8d
	xorl	1024(%rax,%r11,4), %r8d
	xorl	%r8d, %ecx
	movl	-76(%rbp), %r11d
#APP
# 63 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%ecx
# 0 "" 2
#NO_APP
	movl	%ecx, %edx
	movl	%ecx, %r12d
	xorl	%r8d, %r11d
	shrl	$2, %edx
	shrl	$10, %r12d
	andl	$63, %r12d
	andl	$63, %edx
	movl	256(%rax,%rdx,4), %edx
	xorl	768(%rax,%r12,4), %edx
	movl	%ecx, %r12d
	shrl	$26, %r12d
	xorl	1792(%rax,%r12,4), %edx
	shrl	$18, %ecx
	andl	$63, %ecx
	xorl	1280(%rax,%rcx,4), %edx
	xorl	%edx, %esi
	movl	%r11d, %edx
	shrl	$26, %edx
	xorl	1536(%rax,%rdx,4), %esi
	movl	%r11d, %edx
	shrl	$2, %edx
	andl	$63, %edx
	xorl	(%rax,%rdx,4), %esi
	movl	%r11d, %edx
	shrl	$18, %r11d
	shrl	$10, %edx
	andl	$63, %r11d
	andl	$63, %edx
	xorl	512(%rax,%rdx,4), %esi
	xorl	1024(%rax,%r11,4), %esi
	movl	-88(%rbp), %r11d
	movl	-84(%rbp), %edx
	xorl	%esi, %r11d
	xorl	%esi, %edx
#APP
# 64 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r11d
# 0 "" 2
#NO_APP
	movl	%r11d, %ecx
	movl	%r11d, %r12d
	shrl	$2, %ecx
	shrl	$10, %r12d
	andl	$63, %r12d
	andl	$63, %ecx
	movl	256(%rax,%rcx,4), %ecx
	xorl	768(%rax,%r12,4), %ecx
	movl	%r11d, %r12d
	shrl	$18, %r11d
	shrl	$26, %r12d
	andl	$63, %r11d
	xorl	1792(%rax,%r12,4), %ecx
	xorl	1280(%rax,%r11,4), %ecx
	xorl	%ecx, %r8d
	movl	%edx, %ecx
	movl	-96(%rbp), %r11d
	shrl	$26, %ecx
	xorl	1536(%rax,%rcx,4), %r8d
	movl	%edx, %ecx
	shrl	$2, %ecx
	andl	$63, %ecx
	xorl	(%rax,%rcx,4), %r8d
	movl	%edx, %ecx
	shrl	$18, %edx
	shrl	$10, %ecx
	andl	$63, %edx
	andl	$63, %ecx
	xorl	512(%rax,%rcx,4), %r8d
	xorl	1024(%rax,%rdx,4), %r8d
	xorl	%r8d, %r11d
	movl	-92(%rbp), %ecx
#APP
# 65 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r11d
# 0 "" 2
#NO_APP
	movl	%r11d, %edx
	movl	%r11d, %r12d
	xorl	%r8d, %ecx
	shrl	$2, %edx
	shrl	$10, %r12d
	andl	$63, %r12d
	andl	$63, %edx
	movl	256(%rax,%rdx,4), %edx
	xorl	768(%rax,%r12,4), %edx
	movl	%r11d, %r12d
	shrl	$18, %r11d
	shrl	$26, %r12d
	andl	$63, %r11d
	xorl	1792(%rax,%r12,4), %edx
	xorl	1280(%rax,%r11,4), %edx
	xorl	%edx, %esi
	movl	%ecx, %edx
	movl	-104(%rbp), %r11d
	shrl	$26, %edx
	xorl	1536(%rax,%rdx,4), %esi
	movl	%ecx, %edx
	shrl	$2, %edx
	andl	$63, %edx
	xorl	(%rax,%rdx,4), %esi
	movl	%ecx, %edx
	shrl	$18, %ecx
	shrl	$10, %edx
	andl	$63, %ecx
	andl	$63, %edx
	xorl	512(%rax,%rdx,4), %esi
	xorl	1024(%rax,%rcx,4), %esi
	xorl	%esi, %r11d
	movl	-100(%rbp), %edx
#APP
# 66 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r11d
# 0 "" 2
#NO_APP
	movl	%r11d, %ecx
	movl	%r11d, %r12d
	xorl	%esi, %edx
	shrl	$2, %ecx
	shrl	$10, %r12d
	andl	$63, %r12d
	andl	$63, %ecx
	movl	256(%rax,%rcx,4), %ecx
	xorl	768(%rax,%r12,4), %ecx
	movl	%r11d, %r12d
	shrl	$18, %r11d
	shrl	$26, %r12d
	andl	$63, %r11d
	xorl	1792(%rax,%r12,4), %ecx
	xorl	1280(%rax,%r11,4), %ecx
	xorl	%ecx, %r8d
	movl	%edx, %ecx
	shrl	$26, %ecx
	xorl	1536(%rax,%rcx,4), %r8d
	movl	%edx, %ecx
	shrl	$2, %ecx
	andl	$63, %ecx
	xorl	(%rax,%rcx,4), %r8d
	movl	%edx, %ecx
	shrl	$18, %edx
	shrl	$10, %ecx
	andl	$63, %edx
	andl	$63, %ecx
	xorl	512(%rax,%rcx,4), %r8d
	xorl	1024(%rax,%rdx,4), %r8d
	movl	-108(%rbp), %ecx
	movl	-112(%rbp), %r11d
	xorl	%r8d, %r11d
	xorl	%r8d, %ecx
#APP
# 67 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r11d
# 0 "" 2
#NO_APP
	movl	%r11d, %edx
	movl	%r11d, %r12d
	shrl	$2, %edx
	shrl	$10, %r12d
	andl	$63, %edx
	andl	$63, %r12d
	movl	256(%rax,%rdx,4), %edx
	xorl	768(%rax,%r12,4), %edx
	movl	%r11d, %r12d
	shrl	$18, %r11d
	shrl	$26, %r12d
	andl	$63, %r11d
	xorl	1792(%rax,%r12,4), %edx
	xorl	1280(%rax,%r11,4), %edx
	xorl	%edx, %esi
	movl	%ecx, %edx
	movl	-120(%rbp), %r11d
	shrl	$26, %edx
	xorl	1536(%rax,%rdx,4), %esi
	movl	%ecx, %edx
	shrl	$2, %edx
	andl	$63, %edx
	xorl	(%rax,%rdx,4), %esi
	movl	%ecx, %edx
	shrl	$18, %ecx
	shrl	$10, %edx
	andl	$63, %ecx
	andl	$63, %edx
	xorl	512(%rax,%rdx,4), %esi
	xorl	1024(%rax,%rcx,4), %esi
	xorl	%esi, %r11d
	movl	-116(%rbp), %edx
#APP
# 68 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r11d
# 0 "" 2
#NO_APP
	movl	%r11d, %ecx
	movl	%r11d, %r12d
	xorl	%esi, %edx
	shrl	$2, %ecx
	shrl	$10, %r12d
	andl	$63, %r12d
	andl	$63, %ecx
	movl	256(%rax,%rcx,4), %ecx
	xorl	768(%rax,%r12,4), %ecx
	movl	%r11d, %r12d
	shrl	$18, %r11d
	shrl	$26, %r12d
	andl	$63, %r11d
	xorl	1792(%rax,%r12,4), %ecx
	xorl	1280(%rax,%r11,4), %ecx
	xorl	%ecx, %r8d
	movl	%edx, %ecx
	shrl	$26, %ecx
	xorl	1536(%rax,%rcx,4), %r8d
	movl	%edx, %ecx
	shrl	$2, %ecx
	andl	$63, %ecx
	xorl	(%rax,%rcx,4), %r8d
	movl	%edx, %ecx
	shrl	$18, %edx
	shrl	$10, %ecx
	andl	$63, %edx
	andl	$63, %ecx
	xorl	512(%rax,%rcx,4), %r8d
	xorl	1024(%rax,%rdx,4), %r8d
	movl	-128(%rbp), %edx
	movl	-124(%rbp), %ecx
	xorl	%r8d, %edx
	xorl	%r8d, %ecx
#APP
# 69 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%edx
# 0 "" 2
#NO_APP
	movl	%edx, %r12d
	movl	%edx, %r11d
	shrl	$2, %r12d
	shrl	$10, %r11d
	andl	$63, %r11d
	andl	$63, %r12d
	movl	256(%rax,%r12,4), %r12d
	xorl	768(%rax,%r11,4), %r12d
	movl	%edx, %r11d
	shrl	$18, %edx
	shrl	$26, %r11d
	xorl	1792(%rax,%r11,4), %r12d
	movl	%edx, %r11d
	andl	$63, %r11d
	movl	1280(%rax,%r11,4), %edx
	movl	-132(%rbp), %r11d
	xorl	%r12d, %edx
	xorl	%edx, %esi
	movl	%ecx, %edx
	shrl	$26, %edx
	xorl	1536(%rax,%rdx,4), %esi
	movl	%ecx, %edx
	shrl	$2, %edx
	andl	$63, %edx
	xorl	(%rax,%rdx,4), %esi
	movl	%ecx, %edx
	shrl	$10, %edx
	andl	$63, %edx
	shrl	$18, %ecx
	xorl	512(%rax,%rdx,4), %esi
	andl	$63, %ecx
	movl	-136(%rbp), %edx
	xorl	1024(%rax,%rcx,4), %esi
	xorl	%esi, %edx
	xorl	%esi, %r11d
#APP
# 70 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%edx
# 0 "" 2
#NO_APP
	movl	%edx, %ecx
	movl	%edx, %r12d
	shrl	$2, %ecx
	shrl	$10, %r12d
	andl	$63, %r12d
	andl	$63, %ecx
	movl	256(%rax,%rcx,4), %ecx
	xorl	768(%rax,%r12,4), %ecx
	movl	%edx, %r12d
	shrl	$18, %edx
	andl	$63, %edx
	shrl	$26, %r12d
	xorl	1792(%rax,%r12,4), %ecx
	xorl	1280(%rax,%rdx,4), %ecx
	movl	%r11d, %edx
	shrl	$26, %edx
	xorl	%r8d, %ecx
	xorl	1536(%rax,%rdx,4), %ecx
	movl	%r11d, %edx
	shrl	$2, %edx
	andl	$63, %edx
	xorl	(%rax,%rdx,4), %ecx
	movl	%r11d, %edx
	shrl	$18, %r11d
	shrl	$10, %edx
	andl	$63, %r11d
	andl	$63, %edx
	xorl	512(%rax,%rdx,4), %ecx
	xorl	1024(%rax,%r11,4), %ecx
	xorl	%ecx, %ebx
	movl	%ecx, %r8d
	xorl	%ecx, %r10d
#APP
# 71 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r10d
# 0 "" 2
#NO_APP
	movl	%r10d, %edx
	movl	%r10d, %ecx
	shrl	$2, %edx
	shrl	$10, %ecx
	andl	$63, %ecx
	andl	$63, %edx
	movl	256(%rax,%rdx,4), %edx
	xorl	768(%rax,%rcx,4), %edx
	movl	%r10d, %ecx
	shrl	$18, %r10d
	shrl	$26, %ecx
	andl	$63, %r10d
	xorl	1792(%rax,%rcx,4), %edx
	movl	%ebx, %ecx
	xorl	1280(%rax,%r10,4), %edx
	shrl	$26, %ecx
	xorl	%esi, %edx
	xorl	1536(%rax,%rcx,4), %edx
	movl	%ebx, %ecx
	shrl	$2, %ecx
	andl	$63, %ecx
	xorl	(%rax,%rcx,4), %edx
	movl	%ebx, %ecx
	shrl	$18, %ebx
	shrl	$10, %ecx
	andl	$63, %ebx
	andl	$63, %ecx
	xorl	512(%rax,%rcx,4), %edx
	xorl	1024(%rax,%rbx,4), %edx
	xorl	%edx, %r9d
	xorl	%edx, %r13d
#APP
# 72 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r9d
# 0 "" 2
#NO_APP
	movl	%r9d, %ecx
	movl	%r9d, %esi
	shrl	$2, %ecx
	shrl	$10, %esi
	andl	$63, %esi
	andl	$63, %ecx
	movl	256(%rax,%rcx,4), %ecx
	xorl	768(%rax,%rsi,4), %ecx
	movl	%r9d, %esi
	shrl	$26, %esi
	xorl	1792(%rax,%rsi,4), %ecx
	shrl	$18, %r9d
	movl	%r13d, %esi
	andl	$63, %r9d
	shrl	$26, %esi
	xorl	1280(%rax,%r9,4), %ecx
	xorl	%r8d, %ecx
	xorl	1536(%rax,%rsi,4), %ecx
	movl	%r13d, %esi
	shrl	$2, %esi
	andl	$63, %esi
	xorl	(%rax,%rsi,4), %ecx
	movl	%r13d, %esi
	shrl	$18, %r13d
	shrl	$10, %esi
	andl	$63, %r13d
	andl	$63, %esi
	xorl	512(%rax,%rsi,4), %ecx
	xorl	1024(%rax,%r13,4), %ecx
	movl	%ecx, %eax
	jmp	.L3
	.cfi_endproc
.LFE151:
	.size	DES_encrypt1, .-DES_encrypt1
	.p2align 4
	.globl	DES_encrypt2
	.type	DES_encrypt2, @function
DES_encrypt2:
.LFB152:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	108(%rax), %r15d
	movl	96(%rax), %r10d
	movl	100(%rax), %r13d
	movl	88(%rax), %r9d
	movl	104(%rax), %ebx
	movl	(%rdi), %edx
	movl	%r15d, -48(%rbp)
	movl	92(%rax), %r15d
	movl	4(%rdi), %ecx
	movl	%r10d, -52(%rbp)
	movl	80(%rax), %r10d
	movl	120(%rsi), %esi
	movl	%r13d, -56(%rbp)
	movl	84(%rax), %r13d
	movl	124(%rax), %r8d
	movl	%r9d, -60(%rbp)
	movl	72(%rax), %r9d
	movl	112(%rax), %r12d
	movl	%r15d, -64(%rbp)
	movl	%r10d, -68(%rbp)
	movl	116(%rax), %r11d
	movl	%r13d, -72(%rbp)
	movl	%r9d, -76(%rbp)
	movl	%ebx, -44(%rbp)
	movl	76(%rax), %ebx
	movl	64(%rax), %r15d
	movl	68(%rax), %r10d
	movl	60(%rax), %r9d
	movl	56(%rax), %r13d
	movl	%ebx, -80(%rbp)
	movl	48(%rax), %ebx
	movl	%r15d, -84(%rbp)
	movl	%r10d, -88(%rbp)
	movl	52(%rax), %r15d
	movl	40(%rax), %r10d
	movl	%r9d, -96(%rbp)
	movl	%ebx, -100(%rbp)
	movl	44(%rax), %r9d
	movl	36(%rax), %ebx
	movl	%r13d, -92(%rbp)
	movl	%r15d, -104(%rbp)
	movl	32(%rax), %r13d
	movl	%r10d, -108(%rbp)
	movl	24(%rax), %r15d
	movl	28(%rax), %r10d
	movl	%r9d, -112(%rbp)
	movl	%ebx, -120(%rbp)
	movl	16(%rax), %r9d
	movl	20(%rax), %ebx
	movl	%r10d, -128(%rbp)
	movl	%r9d, -132(%rbp)
	movl	12(%rax), %r10d
	movl	%ebx, -136(%rbp)
	movl	8(%rax), %ebx
	movl	%r13d, -116(%rbp)
	movl	%r15d, -124(%rbp)
	movl	(%rax), %r13d
#APP
# 100 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $29,%edx
# 0 "" 2
#NO_APP
	movl	4(%rax), %r9d
#APP
# 101 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $29,%ecx
# 0 "" 2
#NO_APP
	testl	%r14d, %r14d
	je	.L7
	xorl	%edx, %r13d
	leaq	DES_SPtrans(%rip), %rax
	xorl	%edx, %r9d
	movl	%r13d, %r14d
	movl	%r13d, %r15d
	shrl	$2, %r14d
	shrl	$26, %r15d
	andl	$63, %r14d
	xorl	1536(%rax,%r15,4), %ecx
	xorl	(%rax,%r14,4), %ecx
	movl	%r13d, %r14d
	shrl	$18, %r13d
	shrl	$10, %r14d
	andl	$63, %r13d
	andl	$63, %r14d
	xorl	512(%rax,%r14,4), %ecx
	xorl	1024(%rax,%r13,4), %ecx
#APP
# 109 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r9d
# 0 "" 2
#NO_APP
	movl	%r9d, %r14d
	shrl	$26, %r14d
	movl	%r14d, %r13d
	movl	%r9d, %r14d
	shrl	$2, %r14d
	xorl	1792(%rax,%r13,4), %ecx
	movl	%r14d, %r13d
	movl	%r9d, %r14d
	shrl	$18, %r9d
	andl	$63, %r13d
	shrl	$10, %r14d
	andl	$63, %r9d
	xorl	256(%rax,%r13,4), %ecx
	movl	%r14d, %r13d
	andl	$63, %r13d
	xorl	768(%rax,%r13,4), %ecx
	xorl	1280(%rax,%r9,4), %ecx
	xorl	%ecx, %r10d
	xorl	%ecx, %ebx
#APP
# 110 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r10d
# 0 "" 2
#NO_APP
	movl	%r10d, %r14d
	shrl	$2, %r14d
	movl	%r14d, %r9d
	movl	%r10d, %r14d
	shrl	$26, %r14d
	andl	$63, %r9d
	movl	%r14d, %r13d
	movl	%r10d, %r14d
	xorl	1792(%rax,%r13,4), %edx
	xorl	256(%rax,%r9,4), %edx
	shrl	$10, %r14d
	movl	%r14d, %r9d
	movl	%ebx, %r14d
	shrl	$18, %r10d
	andl	$63, %r9d
	shrl	$26, %r14d
	andl	$63, %r10d
	xorl	768(%rax,%r9,4), %edx
	movl	%r14d, %r9d
	movl	%ebx, %r14d
	xorl	1280(%rax,%r10,4), %edx
	shrl	$2, %r14d
	xorl	1536(%rax,%r9,4), %edx
	movl	%r14d, %r9d
	movl	%ebx, %r14d
	shrl	$18, %ebx
	andl	$63, %r9d
	shrl	$10, %r14d
	andl	$63, %ebx
	xorl	(%rax,%r9,4), %edx
	movl	%r14d, %r9d
	movl	-136(%rbp), %r14d
	andl	$63, %r9d
	xorl	512(%rax,%r9,4), %edx
	movl	-132(%rbp), %r9d
	xorl	1024(%rax,%rbx,4), %edx
	xorl	%edx, %r14d
	xorl	%edx, %r9d
	movl	%r14d, %r10d
	movl	%r9d, %ebx
#APP
# 111 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r10d
# 0 "" 2
#NO_APP
	movl	%r10d, %r14d
	movl	%r10d, %r9d
	shrl	$10, %r14d
	shrl	$2, %r9d
	movl	%r14d, %r13d
	movl	%r10d, %r14d
	andl	$63, %r9d
	shrl	$18, %r10d
	andl	$63, %r13d
	shrl	$26, %r14d
	movl	256(%rax,%r9,4), %r9d
	andl	$63, %r10d
	xorl	768(%rax,%r13,4), %r9d
	movl	%r14d, %r13d
	movl	-124(%rbp), %r14d
	xorl	1792(%rax,%r13,4), %r9d
	xorl	1280(%rax,%r10,4), %r9d
	xorl	%r9d, %ecx
	movl	%ebx, %r9d
	movl	-128(%rbp), %r10d
	shrl	$26, %r9d
	xorl	1536(%rax,%r9,4), %ecx
	movl	%ebx, %r9d
	shrl	$2, %r9d
	andl	$63, %r9d
	xorl	(%rax,%r9,4), %ecx
	movl	%ebx, %r9d
	shrl	$18, %ebx
	shrl	$10, %r9d
	andl	$63, %ebx
	andl	$63, %r9d
	xorl	512(%rax,%r9,4), %ecx
	xorl	1024(%rax,%rbx,4), %ecx
	xorl	%ecx, %r14d
	xorl	%ecx, %r10d
	movl	%r14d, %ebx
#APP
# 112 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r10d
# 0 "" 2
#NO_APP
	movl	%r10d, %r14d
	shrl	$2, %r14d
	movl	%r14d, %r9d
	movl	%r10d, %r14d
	shrl	$10, %r14d
	andl	$63, %r9d
	movl	%r14d, %r13d
	movl	%r10d, %r14d
	shrl	$18, %r10d
	movl	256(%rax,%r9,4), %r9d
	andl	$63, %r13d
	shrl	$26, %r14d
	andl	$63, %r10d
	xorl	768(%rax,%r13,4), %r9d
	movl	%r14d, %r13d
	movl	%ebx, %r14d
	xorl	1792(%rax,%r13,4), %r9d
	shrl	$26, %r14d
	xorl	1280(%rax,%r10,4), %r9d
	xorl	%r9d, %edx
	movl	%r14d, %r9d
	movl	%ebx, %r14d
	shrl	$2, %r14d
	xorl	1536(%rax,%r9,4), %edx
	movl	-116(%rbp), %r13d
	movl	%r14d, %r9d
	movl	%ebx, %r14d
	andl	$63, %r9d
	shrl	$10, %r14d
	xorl	(%rax,%r9,4), %edx
	movl	%r14d, %r9d
	movl	-108(%rbp), %r14d
	andl	$63, %r9d
	xorl	512(%rax,%r9,4), %edx
	shrl	$18, %ebx
	andl	$63, %ebx
	xorl	1024(%rax,%rbx,4), %edx
	xorl	%edx, %r13d
	movl	%r13d, %ebx
	movl	-120(%rbp), %r13d
	xorl	%edx, %r13d
	movl	%r13d, %r10d
#APP
# 113 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r10d
# 0 "" 2
#NO_APP
	movl	%r10d, %r13d
	shrl	$2, %r13d
	movl	%r13d, %r9d
	movl	%r10d, %r13d
	shrl	$10, %r13d
	andl	$63, %r9d
	andl	$63, %r13d
	movl	256(%rax,%r9,4), %r9d
	xorl	768(%rax,%r13,4), %r9d
	movl	%r10d, %r13d
	shrl	$18, %r10d
	shrl	$26, %r13d
	andl	$63, %r10d
	xorl	1792(%rax,%r13,4), %r9d
	movl	%ebx, %r13d
	xorl	1280(%rax,%r10,4), %r9d
	shrl	$26, %r13d
	xorl	%r9d, %ecx
	movl	%r13d, %r9d
	movl	%ebx, %r13d
	shrl	$2, %r13d
	xorl	1536(%rax,%r9,4), %ecx
	movl	%r13d, %r9d
	movl	%ebx, %r13d
	shrl	$18, %ebx
	andl	$63, %r9d
	shrl	$10, %r13d
	andl	$63, %ebx
	xorl	(%rax,%r9,4), %ecx
	movl	%r13d, %r9d
	andl	$63, %r9d
	xorl	512(%rax,%r9,4), %ecx
	xorl	1024(%rax,%rbx,4), %ecx
	xorl	%ecx, %r14d
	movl	%r14d, %ebx
	movl	-112(%rbp), %r14d
	xorl	%ecx, %r14d
	movl	%r14d, %r10d
#APP
# 114 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r10d
# 0 "" 2
#NO_APP
	movl	%r10d, %r14d
	shrl	$2, %r14d
	movl	%r14d, %r9d
	movl	%r10d, %r14d
	shrl	$10, %r14d
	andl	$63, %r9d
	movl	%r14d, %r13d
	movl	%r10d, %r14d
	shrl	$18, %r10d
	movl	256(%rax,%r9,4), %r9d
	andl	$63, %r13d
	shrl	$26, %r14d
	andl	$63, %r10d
	xorl	768(%rax,%r13,4), %r9d
	movl	%r14d, %r13d
	movl	%ebx, %r14d
	xorl	1792(%rax,%r13,4), %r9d
	shrl	$26, %r14d
	xorl	1280(%rax,%r10,4), %r9d
	xorl	%r9d, %edx
	movl	%r14d, %r9d
	movl	%ebx, %r14d
	shrl	$2, %r14d
	xorl	1536(%rax,%r9,4), %edx
	movl	%r14d, %r9d
	movl	%ebx, %r14d
	shrl	$18, %ebx
	andl	$63, %r9d
	shrl	$10, %r14d
	andl	$63, %ebx
	xorl	(%rax,%r9,4), %edx
	movl	%r14d, %r9d
	movl	-104(%rbp), %r14d
	andl	$63, %r9d
	xorl	512(%rax,%r9,4), %edx
	xorl	1024(%rax,%rbx,4), %edx
	xorl	%edx, %r14d
	movl	-100(%rbp), %ebx
	movl	%r14d, %r10d
#APP
# 115 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r10d
# 0 "" 2
#NO_APP
	movl	%r10d, %r14d
	xorl	%edx, %ebx
	shrl	$2, %r14d
	movl	%r14d, %r9d
	movl	%r10d, %r14d
	shrl	$10, %r14d
	andl	$63, %r9d
	movl	%r14d, %r13d
	movl	%r10d, %r14d
	movl	256(%rax,%r9,4), %r9d
	andl	$63, %r13d
	shrl	$26, %r14d
	xorl	768(%rax,%r13,4), %r9d
	movl	%r14d, %r13d
	movl	%ebx, %r14d
	xorl	1792(%rax,%r13,4), %r9d
	shrl	$18, %r10d
	andl	$63, %r10d
	shrl	$26, %r14d
	xorl	1280(%rax,%r10,4), %r9d
	xorl	%r9d, %ecx
	movl	%r14d, %r9d
	movl	%ebx, %r14d
	shrl	$2, %r14d
	xorl	1536(%rax,%r9,4), %ecx
	movl	%r14d, %r9d
	movl	%ebx, %r14d
	shrl	$18, %ebx
	andl	$63, %r9d
	shrl	$10, %r14d
	andl	$63, %ebx
	xorl	(%rax,%r9,4), %ecx
	movl	%r14d, %r9d
	andl	$63, %r9d
	xorl	512(%rax,%r9,4), %ecx
	movl	-96(%rbp), %r14d
	xorl	1024(%rax,%rbx,4), %ecx
	movl	-92(%rbp), %r13d
	xorl	%ecx, %r14d
	movl	-84(%rbp), %r15d
	xorl	%ecx, %r13d
	movl	%r14d, %r10d
#APP
# 116 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r10d
# 0 "" 2
#NO_APP
	movl	%r10d, %r14d
	movl	%r13d, %ebx
	movl	%r10d, %r13d
	shrl	$2, %r14d
	shrl	$10, %r13d
	movl	%r14d, %r9d
	andl	$63, %r13d
	movl	%ebx, %r14d
	andl	$63, %r9d
	shrl	$26, %r14d
	movl	256(%rax,%r9,4), %r9d
	xorl	768(%rax,%r13,4), %r9d
	movl	%r10d, %r13d
	shrl	$18, %r10d
	shrl	$26, %r13d
	andl	$63, %r10d
	xorl	1792(%rax,%r13,4), %r9d
	xorl	1280(%rax,%r10,4), %r9d
	xorl	%r9d, %edx
	movl	%r14d, %r9d
	movl	%ebx, %r14d
	movl	-88(%rbp), %r10d
	shrl	$2, %r14d
	xorl	1536(%rax,%r9,4), %edx
	movl	%r14d, %r9d
	movl	%ebx, %r14d
	andl	$63, %r9d
	shrl	$10, %r14d
	xorl	(%rax,%r9,4), %edx
	shrl	$18, %ebx
	movl	%r14d, %r9d
	movl	-76(%rbp), %r14d
	andl	$63, %r9d
	andl	$63, %ebx
	xorl	512(%rax,%r9,4), %edx
	xorl	1024(%rax,%rbx,4), %edx
	xorl	%edx, %r15d
	xorl	%edx, %r10d
	movl	%r15d, %ebx
#APP
# 117 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r10d
# 0 "" 2
#NO_APP
	movl	%r10d, %r15d
	movl	%r10d, %r13d
	shrl	$2, %r15d
	shrl	$10, %r13d
	movl	%r15d, %r9d
	andl	$63, %r13d
	movl	%ebx, %r15d
	andl	$63, %r9d
	shrl	$26, %r15d
	movl	256(%rax,%r9,4), %r9d
	xorl	768(%rax,%r13,4), %r9d
	movl	%r10d, %r13d
	shrl	$18, %r10d
	shrl	$26, %r13d
	andl	$63, %r10d
	xorl	1792(%rax,%r13,4), %r9d
	xorl	1280(%rax,%r10,4), %r9d
	xorl	%r9d, %ecx
	movl	%r15d, %r9d
	movl	%ebx, %r15d
	shrl	$2, %r15d
	xorl	1536(%rax,%r9,4), %ecx
	movl	%r15d, %r9d
	movl	%ebx, %r15d
	shrl	$18, %ebx
	andl	$63, %r9d
	shrl	$10, %r15d
	andl	$63, %ebx
	xorl	(%rax,%r9,4), %ecx
	movl	%r15d, %r9d
	andl	$63, %r9d
	xorl	512(%rax,%r9,4), %ecx
	xorl	1024(%rax,%rbx,4), %ecx
	xorl	%ecx, %r14d
	movl	%r14d, %ebx
	movl	-80(%rbp), %r14d
	xorl	%ecx, %r14d
	movl	%r14d, %r10d
#APP
# 118 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r10d
# 0 "" 2
#NO_APP
	movl	%r10d, %r14d
	shrl	$2, %r14d
	movl	%r14d, %r9d
	movl	%r10d, %r14d
	shrl	$10, %r14d
	andl	$63, %r9d
	movl	%r14d, %r13d
	movl	%r10d, %r14d
	shrl	$18, %r10d
	movl	256(%rax,%r9,4), %r9d
	andl	$63, %r13d
	shrl	$26, %r14d
	andl	$63, %r10d
	xorl	768(%rax,%r13,4), %r9d
	movl	%r14d, %r13d
	movl	1280(%rax,%r10,4), %r14d
	xorl	1792(%rax,%r13,4), %r9d
	xorl	%r9d, %r14d
	movl	%r14d, %r10d
	xorl	%edx, %r10d
	movl	%ebx, %edx
	shrl	$26, %edx
	xorl	1536(%rax,%rdx,4), %r10d
	movl	%ebx, %edx
	shrl	$2, %edx
	andl	$63, %edx
	xorl	(%rax,%rdx,4), %r10d
	movl	%ebx, %edx
	shrl	$18, %ebx
	shrl	$10, %edx
	andl	$63, %ebx
	andl	$63, %edx
	xorl	512(%rax,%rdx,4), %r10d
	xorl	1024(%rax,%rbx,4), %r10d
	movl	-68(%rbp), %ebx
	movl	-72(%rbp), %edx
	xorl	%r10d, %edx
	xorl	%r10d, %ebx
#APP
# 119 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%edx
# 0 "" 2
#NO_APP
	movl	%edx, %r14d
	movl	%edx, %r13d
	shrl	$2, %r14d
	shrl	$10, %r13d
	movl	%r14d, %r9d
	movl	-60(%rbp), %r14d
	andl	$63, %r9d
	andl	$63, %r13d
	movl	256(%rax,%r9,4), %r9d
	xorl	768(%rax,%r13,4), %r9d
	movl	%edx, %r13d
	shrl	$18, %edx
	shrl	$26, %r13d
	andl	$63, %edx
	xorl	1792(%rax,%r13,4), %r9d
	xorl	1280(%rax,%rdx,4), %r9d
	movl	%ebx, %edx
	shrl	$26, %edx
	xorl	%r9d, %ecx
	xorl	1536(%rax,%rdx,4), %ecx
	movl	%ebx, %edx
	shrl	$2, %edx
	andl	$63, %edx
	xorl	(%rax,%rdx,4), %ecx
	movl	%ebx, %edx
	shrl	$18, %ebx
	shrl	$10, %edx
	andl	$63, %ebx
	andl	$63, %edx
	xorl	512(%rax,%rdx,4), %ecx
	xorl	1024(%rax,%rbx,4), %ecx
	xorl	%ecx, %r14d
	movl	%r14d, %ebx
	movl	-64(%rbp), %r14d
	xorl	%ecx, %r14d
	movl	%r14d, %r9d
#APP
# 120 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r9d
# 0 "" 2
#NO_APP
	movl	%r9d, %r14d
	shrl	$2, %r14d
	movl	%r14d, %edx
	movl	%r9d, %r14d
	shrl	$10, %r14d
	andl	$63, %edx
	movl	%r14d, %r13d
	movl	%r9d, %r14d
	shrl	$18, %r9d
	movl	256(%rax,%rdx,4), %edx
	andl	$63, %r13d
	shrl	$26, %r14d
	andl	$63, %r9d
	xorl	768(%rax,%r13,4), %edx
	movl	%r14d, %r13d
	movl	%ebx, %r14d
	xorl	1792(%rax,%r13,4), %edx
	xorl	1280(%rax,%r9,4), %edx
	xorl	%edx, %r10d
	shrl	$26, %r14d
	movl	%r14d, %edx
	movl	%ebx, %r14d
	shrl	$2, %r14d
	xorl	1536(%rax,%rdx,4), %r10d
	movl	%r14d, %edx
	movl	%ebx, %r14d
	shrl	$18, %ebx
	andl	$63, %edx
	shrl	$10, %r14d
	andl	$63, %ebx
	xorl	(%rax,%rdx,4), %r10d
	movl	%r14d, %edx
	andl	$63, %edx
	xorl	512(%rax,%rdx,4), %r10d
	movl	-52(%rbp), %edx
	xorl	1024(%rax,%rbx,4), %r10d
	xorl	%r10d, %edx
	movl	%edx, %r9d
	movl	-56(%rbp), %edx
	xorl	%r10d, %edx
#APP
# 121 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%edx
# 0 "" 2
#NO_APP
	movl	%edx, %r14d
	movl	%edx, %r13d
	shrl	$2, %r14d
	shrl	$10, %r13d
	movl	%r14d, %ebx
	andl	$63, %r13d
	andl	$63, %ebx
	movl	256(%rax,%rbx,4), %ebx
	xorl	768(%rax,%r13,4), %ebx
	movl	%edx, %r13d
	shrl	$18, %edx
	andl	$63, %edx
	shrl	$26, %r13d
	movl	1280(%rax,%rdx,4), %r14d
	xorl	1792(%rax,%r13,4), %ebx
	xorl	%ebx, %r14d
	movl	-44(%rbp), %ebx
	movl	%r14d, %edx
	xorl	%ecx, %edx
	movl	%r9d, %ecx
	shrl	$26, %ecx
	xorl	1536(%rax,%rcx,4), %edx
	movl	%r9d, %ecx
	shrl	$2, %ecx
	andl	$63, %ecx
	xorl	(%rax,%rcx,4), %edx
	movl	%r9d, %ecx
	shrl	$10, %ecx
	andl	$63, %ecx
	shrl	$18, %r9d
	andl	$63, %r9d
	xorl	512(%rax,%rcx,4), %edx
	xorl	1024(%rax,%r9,4), %edx
	movl	-48(%rbp), %r9d
	xorl	%edx, %ebx
	xorl	%edx, %r9d
#APP
# 122 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r9d
# 0 "" 2
#NO_APP
	movl	%r9d, %ecx
	movl	%r9d, %r13d
	shrl	$2, %ecx
	shrl	$10, %r13d
	andl	$63, %r13d
	andl	$63, %ecx
	movl	256(%rax,%rcx,4), %ecx
	xorl	768(%rax,%r13,4), %ecx
	movl	%r9d, %r13d
	shrl	$18, %r9d
	andl	$63, %r9d
	shrl	$26, %r13d
	xorl	1792(%rax,%r13,4), %ecx
	xorl	1280(%rax,%r9,4), %ecx
	movl	%ebx, %r9d
	shrl	$26, %r9d
	xorl	%r10d, %ecx
	xorl	1536(%rax,%r9,4), %ecx
	movl	%ebx, %r9d
	shrl	$2, %r9d
	andl	$63, %r9d
	xorl	(%rax,%r9,4), %ecx
	movl	%ebx, %r9d
	shrl	$18, %ebx
	shrl	$10, %r9d
	andl	$63, %ebx
	andl	$63, %r9d
	xorl	512(%rax,%r9,4), %ecx
	xorl	1024(%rax,%rbx,4), %ecx
	xorl	%ecx, %r12d
	movl	%ecx, %r9d
	xorl	%ecx, %r11d
#APP
# 123 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r11d
# 0 "" 2
#NO_APP
	movl	%r11d, %ecx
	movl	%r11d, %r10d
	shrl	$2, %ecx
	shrl	$10, %r10d
	andl	$63, %r10d
	andl	$63, %ecx
	movl	256(%rax,%rcx,4), %ecx
	xorl	768(%rax,%r10,4), %ecx
	movl	%r11d, %r10d
	shrl	$18, %r11d
	shrl	$26, %r10d
	andl	$63, %r11d
	xorl	1792(%rax,%r10,4), %ecx
	xorl	1280(%rax,%r11,4), %ecx
	xorl	%ecx, %edx
	movl	%r12d, %ecx
	shrl	$26, %ecx
	xorl	1536(%rax,%rcx,4), %edx
	movl	%r12d, %ecx
	shrl	$2, %ecx
	andl	$63, %ecx
	xorl	(%rax,%rcx,4), %edx
	movl	%r12d, %ecx
	shrl	$18, %r12d
	shrl	$10, %ecx
	andl	$63, %r12d
	andl	$63, %ecx
	xorl	512(%rax,%rcx,4), %edx
	xorl	1024(%rax,%r12,4), %edx
	xorl	%edx, %r8d
	xorl	%edx, %esi
#APP
# 124 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r8d
# 0 "" 2
#NO_APP
	movl	%r8d, %ecx
	movl	%r8d, %r10d
	shrl	$2, %ecx
	shrl	$10, %r10d
	andl	$63, %r10d
	andl	$63, %ecx
	movl	256(%rax,%rcx,4), %ecx
	xorl	768(%rax,%r10,4), %ecx
	movl	%r8d, %r10d
	shrl	$26, %r10d
	xorl	1792(%rax,%r10,4), %ecx
	shrl	$18, %r8d
	andl	$63, %r8d
	xorl	1280(%rax,%r8,4), %ecx
	movl	%esi, %r8d
	shrl	$26, %r8d
	xorl	%r9d, %ecx
	xorl	1536(%rax,%r8,4), %ecx
	movl	%esi, %r8d
	shrl	$2, %r8d
	andl	$63, %r8d
	xorl	(%rax,%r8,4), %ecx
	movl	%esi, %r8d
	shrl	$18, %esi
	shrl	$10, %r8d
	andl	$63, %esi
	andl	$63, %r8d
	xorl	512(%rax,%r8,4), %ecx
	xorl	1024(%rax,%rsi,4), %ecx
	movl	%ecx, %eax
.L8:
#APP
# 144 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $3,%edx
# 0 "" 2
# 145 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $3,%eax
# 0 "" 2
#NO_APP
	movl	%edx, (%rdi)
	movl	%eax, 4(%rdi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	xorl	%edx, %esi
	leaq	DES_SPtrans(%rip), %rax
	xorl	%edx, %r8d
	movl	%esi, %r14d
	movl	%esi, %r15d
	shrl	$2, %r14d
	shrl	$26, %r15d
	andl	$63, %r14d
	xorl	1536(%rax,%r15,4), %ecx
	xorl	(%rax,%r14,4), %ecx
	movl	%esi, %r14d
	shrl	$18, %esi
	shrl	$10, %r14d
	andl	$63, %esi
	andl	$63, %r14d
#APP
# 126 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r8d
# 0 "" 2
#NO_APP
	xorl	512(%rax,%r14,4), %ecx
	xorl	1024(%rax,%rsi,4), %ecx
	movl	%r8d, %esi
	shrl	$26, %esi
	xorl	1792(%rax,%rsi,4), %ecx
	movl	%r8d, %esi
	shrl	$2, %esi
	andl	$63, %esi
	xorl	256(%rax,%rsi,4), %ecx
	movl	%r8d, %esi
	shrl	$18, %r8d
	shrl	$10, %esi
	andl	$63, %r8d
	andl	$63, %esi
	xorl	768(%rax,%rsi,4), %ecx
	xorl	1280(%rax,%r8,4), %ecx
	xorl	%ecx, %r11d
	xorl	%ecx, %r12d
#APP
# 127 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r11d
# 0 "" 2
#NO_APP
	movl	%r11d, %esi
	movl	%r11d, %r8d
	shrl	$2, %esi
	shrl	$26, %r8d
	andl	$63, %esi
	xorl	1792(%rax,%r8,4), %edx
	xorl	256(%rax,%rsi,4), %edx
	movl	%r11d, %esi
	shrl	$18, %r11d
	shrl	$10, %esi
	andl	$63, %r11d
	andl	$63, %esi
	xorl	768(%rax,%rsi,4), %edx
	movl	%r12d, %esi
	xorl	1280(%rax,%r11,4), %edx
	shrl	$26, %esi
	movl	-44(%rbp), %r11d
	xorl	1536(%rax,%rsi,4), %edx
	movl	%r12d, %esi
	movl	%edx, %r8d
	shrl	$2, %esi
	movl	%r12d, %edx
	shrl	$10, %edx
	andl	$63, %esi
	shrl	$18, %r12d
	xorl	(%rax,%rsi,4), %r8d
	andl	$63, %edx
	andl	$63, %r12d
	movl	-48(%rbp), %esi
	xorl	512(%rax,%rdx,4), %r8d
	xorl	1024(%rax,%r12,4), %r8d
	xorl	%r8d, %esi
	xorl	%r8d, %r11d
#APP
# 128 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%esi
# 0 "" 2
#NO_APP
	movl	%esi, %edx
	movl	%esi, %r12d
	shrl	$2, %edx
	shrl	$10, %r12d
	andl	$63, %r12d
	andl	$63, %edx
	movl	256(%rax,%rdx,4), %edx
	xorl	768(%rax,%r12,4), %edx
	movl	%esi, %r12d
	shrl	$18, %esi
	shrl	$26, %r12d
	andl	$63, %esi
	xorl	1792(%rax,%r12,4), %edx
	xorl	1280(%rax,%rsi,4), %edx
	xorl	%ecx, %edx
	movl	%r11d, %ecx
	movl	-52(%rbp), %esi
	shrl	$26, %ecx
	xorl	1536(%rax,%rcx,4), %edx
	movl	%r11d, %ecx
	shrl	$2, %ecx
	andl	$63, %ecx
	xorl	(%rax,%rcx,4), %edx
	movl	%r11d, %ecx
	shrl	$18, %r11d
	shrl	$10, %ecx
	andl	$63, %r11d
	andl	$63, %ecx
	xorl	512(%rax,%rcx,4), %edx
	xorl	1024(%rax,%r11,4), %edx
	movl	-56(%rbp), %r11d
	xorl	%edx, %esi
	xorl	%edx, %r11d
#APP
# 129 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r11d
# 0 "" 2
#NO_APP
	movl	%r11d, %ecx
	movl	%r11d, %r12d
	shrl	$2, %ecx
	shrl	$10, %r12d
	andl	$63, %r12d
	andl	$63, %ecx
	movl	256(%rax,%rcx,4), %ecx
	xorl	768(%rax,%r12,4), %ecx
	movl	%r11d, %r12d
	shrl	$18, %r11d
	shrl	$26, %r12d
	andl	$63, %r11d
	xorl	1792(%rax,%r12,4), %ecx
	xorl	1280(%rax,%r11,4), %ecx
	xorl	%ecx, %r8d
	movl	%esi, %ecx
	shrl	$26, %ecx
	xorl	1536(%rax,%rcx,4), %r8d
	movl	%esi, %ecx
	movl	-64(%rbp), %r11d
	shrl	$2, %ecx
	andl	$63, %ecx
	xorl	(%rax,%rcx,4), %r8d
	movl	%esi, %ecx
	shrl	$10, %ecx
	andl	$63, %ecx
	xorl	512(%rax,%rcx,4), %r8d
	shrl	$18, %esi
	movl	-60(%rbp), %ecx
	andl	$63, %esi
	xorl	1024(%rax,%rsi,4), %r8d
	xorl	%r8d, %r11d
	xorl	%r8d, %ecx
#APP
# 130 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r11d
# 0 "" 2
#NO_APP
	movl	%r11d, %esi
	movl	%r11d, %r12d
	shrl	$2, %esi
	shrl	$10, %r12d
	andl	$63, %r12d
	andl	$63, %esi
	movl	256(%rax,%rsi,4), %esi
	xorl	768(%rax,%r12,4), %esi
	movl	%r11d, %r12d
	shrl	$18, %r11d
	shrl	$26, %r12d
	andl	$63, %r11d
	xorl	1792(%rax,%r12,4), %esi
	xorl	1280(%rax,%r11,4), %esi
	xorl	%edx, %esi
	movl	%ecx, %edx
	movl	-68(%rbp), %r11d
	shrl	$26, %edx
	xorl	1536(%rax,%rdx,4), %esi
	movl	%ecx, %edx
	shrl	$2, %edx
	andl	$63, %edx
	xorl	(%rax,%rdx,4), %esi
	movl	%ecx, %edx
	shrl	$18, %ecx
	shrl	$10, %edx
	andl	$63, %ecx
	andl	$63, %edx
	xorl	512(%rax,%rdx,4), %esi
	movl	-72(%rbp), %edx
	xorl	1024(%rax,%rcx,4), %esi
	xorl	%esi, %edx
	xorl	%esi, %r11d
#APP
# 131 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%edx
# 0 "" 2
#NO_APP
	movl	%edx, %ecx
	movl	%edx, %r12d
	shrl	$2, %ecx
	shrl	$10, %r12d
	andl	$63, %r12d
	andl	$63, %ecx
	movl	256(%rax,%rcx,4), %ecx
	xorl	768(%rax,%r12,4), %ecx
	movl	%edx, %r12d
	shrl	$18, %edx
	shrl	$26, %r12d
	andl	$63, %edx
	xorl	1792(%rax,%r12,4), %ecx
	xorl	1280(%rax,%rdx,4), %ecx
	movl	%r11d, %edx
	shrl	$26, %edx
	xorl	%ecx, %r8d
	movl	-80(%rbp), %ecx
	xorl	1536(%rax,%rdx,4), %r8d
	movl	%r11d, %edx
	shrl	$2, %edx
	andl	$63, %edx
	xorl	(%rax,%rdx,4), %r8d
	movl	%r11d, %edx
	shrl	$18, %r11d
	shrl	$10, %edx
	andl	$63, %r11d
	andl	$63, %edx
	xorl	512(%rax,%rdx,4), %r8d
	xorl	1024(%rax,%r11,4), %r8d
	xorl	%r8d, %ecx
	movl	-76(%rbp), %r11d
#APP
# 132 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%ecx
# 0 "" 2
#NO_APP
	movl	%ecx, %edx
	movl	%ecx, %r12d
	xorl	%r8d, %r11d
	shrl	$2, %edx
	shrl	$10, %r12d
	andl	$63, %r12d
	andl	$63, %edx
	movl	256(%rax,%rdx,4), %edx
	xorl	768(%rax,%r12,4), %edx
	movl	%ecx, %r12d
	shrl	$26, %r12d
	xorl	1792(%rax,%r12,4), %edx
	shrl	$18, %ecx
	andl	$63, %ecx
	xorl	1280(%rax,%rcx,4), %edx
	xorl	%edx, %esi
	movl	%r11d, %edx
	shrl	$26, %edx
	xorl	1536(%rax,%rdx,4), %esi
	movl	%r11d, %edx
	shrl	$2, %edx
	andl	$63, %edx
	xorl	(%rax,%rdx,4), %esi
	movl	%r11d, %edx
	shrl	$18, %r11d
	shrl	$10, %edx
	andl	$63, %r11d
	andl	$63, %edx
	xorl	512(%rax,%rdx,4), %esi
	xorl	1024(%rax,%r11,4), %esi
	movl	-88(%rbp), %r11d
	movl	-84(%rbp), %edx
	xorl	%esi, %r11d
	xorl	%esi, %edx
#APP
# 133 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r11d
# 0 "" 2
#NO_APP
	movl	%r11d, %ecx
	movl	%r11d, %r12d
	shrl	$2, %ecx
	shrl	$10, %r12d
	andl	$63, %r12d
	andl	$63, %ecx
	movl	256(%rax,%rcx,4), %ecx
	xorl	768(%rax,%r12,4), %ecx
	movl	%r11d, %r12d
	shrl	$18, %r11d
	shrl	$26, %r12d
	andl	$63, %r11d
	xorl	1792(%rax,%r12,4), %ecx
	xorl	1280(%rax,%r11,4), %ecx
	xorl	%ecx, %r8d
	movl	%edx, %ecx
	movl	-96(%rbp), %r11d
	shrl	$26, %ecx
	xorl	1536(%rax,%rcx,4), %r8d
	movl	%edx, %ecx
	shrl	$2, %ecx
	andl	$63, %ecx
	xorl	(%rax,%rcx,4), %r8d
	movl	%edx, %ecx
	shrl	$18, %edx
	shrl	$10, %ecx
	andl	$63, %edx
	andl	$63, %ecx
	xorl	512(%rax,%rcx,4), %r8d
	xorl	1024(%rax,%rdx,4), %r8d
	xorl	%r8d, %r11d
	movl	-92(%rbp), %ecx
#APP
# 134 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r11d
# 0 "" 2
#NO_APP
	movl	%r11d, %edx
	movl	%r11d, %r12d
	xorl	%r8d, %ecx
	shrl	$2, %edx
	shrl	$10, %r12d
	andl	$63, %r12d
	andl	$63, %edx
	movl	256(%rax,%rdx,4), %edx
	xorl	768(%rax,%r12,4), %edx
	movl	%r11d, %r12d
	shrl	$18, %r11d
	shrl	$26, %r12d
	andl	$63, %r11d
	xorl	1792(%rax,%r12,4), %edx
	xorl	1280(%rax,%r11,4), %edx
	xorl	%edx, %esi
	movl	%ecx, %edx
	movl	-104(%rbp), %r11d
	shrl	$26, %edx
	xorl	1536(%rax,%rdx,4), %esi
	movl	%ecx, %edx
	shrl	$2, %edx
	andl	$63, %edx
	xorl	(%rax,%rdx,4), %esi
	movl	%ecx, %edx
	shrl	$18, %ecx
	shrl	$10, %edx
	andl	$63, %ecx
	andl	$63, %edx
	xorl	512(%rax,%rdx,4), %esi
	xorl	1024(%rax,%rcx,4), %esi
	xorl	%esi, %r11d
	movl	-100(%rbp), %edx
#APP
# 135 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r11d
# 0 "" 2
#NO_APP
	movl	%r11d, %ecx
	movl	%r11d, %r12d
	xorl	%esi, %edx
	shrl	$2, %ecx
	shrl	$10, %r12d
	andl	$63, %r12d
	andl	$63, %ecx
	movl	256(%rax,%rcx,4), %ecx
	xorl	768(%rax,%r12,4), %ecx
	movl	%r11d, %r12d
	shrl	$18, %r11d
	shrl	$26, %r12d
	andl	$63, %r11d
	xorl	1792(%rax,%r12,4), %ecx
	xorl	1280(%rax,%r11,4), %ecx
	xorl	%ecx, %r8d
	movl	%edx, %ecx
	shrl	$26, %ecx
	xorl	1536(%rax,%rcx,4), %r8d
	movl	%edx, %ecx
	shrl	$2, %ecx
	andl	$63, %ecx
	xorl	(%rax,%rcx,4), %r8d
	movl	%edx, %ecx
	shrl	$18, %edx
	shrl	$10, %ecx
	andl	$63, %edx
	andl	$63, %ecx
	xorl	512(%rax,%rcx,4), %r8d
	xorl	1024(%rax,%rdx,4), %r8d
	movl	-108(%rbp), %ecx
	movl	-112(%rbp), %r11d
	xorl	%r8d, %r11d
	xorl	%r8d, %ecx
#APP
# 136 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r11d
# 0 "" 2
#NO_APP
	movl	%r11d, %edx
	movl	%r11d, %r12d
	shrl	$2, %edx
	shrl	$10, %r12d
	andl	$63, %edx
	andl	$63, %r12d
	movl	256(%rax,%rdx,4), %edx
	xorl	768(%rax,%r12,4), %edx
	movl	%r11d, %r12d
	shrl	$18, %r11d
	shrl	$26, %r12d
	andl	$63, %r11d
	xorl	1792(%rax,%r12,4), %edx
	xorl	1280(%rax,%r11,4), %edx
	xorl	%edx, %esi
	movl	%ecx, %edx
	movl	-120(%rbp), %r11d
	shrl	$26, %edx
	xorl	1536(%rax,%rdx,4), %esi
	movl	%ecx, %edx
	shrl	$2, %edx
	andl	$63, %edx
	xorl	(%rax,%rdx,4), %esi
	movl	%ecx, %edx
	shrl	$18, %ecx
	shrl	$10, %edx
	andl	$63, %ecx
	andl	$63, %edx
	xorl	512(%rax,%rdx,4), %esi
	xorl	1024(%rax,%rcx,4), %esi
	xorl	%esi, %r11d
	movl	-116(%rbp), %edx
#APP
# 137 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r11d
# 0 "" 2
#NO_APP
	movl	%r11d, %ecx
	movl	%r11d, %r12d
	xorl	%esi, %edx
	shrl	$2, %ecx
	shrl	$10, %r12d
	andl	$63, %r12d
	andl	$63, %ecx
	movl	256(%rax,%rcx,4), %ecx
	xorl	768(%rax,%r12,4), %ecx
	movl	%r11d, %r12d
	shrl	$18, %r11d
	shrl	$26, %r12d
	andl	$63, %r11d
	xorl	1792(%rax,%r12,4), %ecx
	xorl	1280(%rax,%r11,4), %ecx
	xorl	%ecx, %r8d
	movl	%edx, %ecx
	shrl	$26, %ecx
	xorl	1536(%rax,%rcx,4), %r8d
	movl	%edx, %ecx
	shrl	$2, %ecx
	andl	$63, %ecx
	xorl	(%rax,%rcx,4), %r8d
	movl	%edx, %ecx
	shrl	$18, %edx
	shrl	$10, %ecx
	andl	$63, %edx
	andl	$63, %ecx
	xorl	512(%rax,%rcx,4), %r8d
	xorl	1024(%rax,%rdx,4), %r8d
	movl	-128(%rbp), %edx
	movl	-124(%rbp), %ecx
	xorl	%r8d, %edx
	xorl	%r8d, %ecx
#APP
# 138 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%edx
# 0 "" 2
#NO_APP
	movl	%edx, %r12d
	movl	%edx, %r11d
	shrl	$2, %r12d
	shrl	$10, %r11d
	andl	$63, %r11d
	andl	$63, %r12d
	movl	256(%rax,%r12,4), %r12d
	xorl	768(%rax,%r11,4), %r12d
	movl	%edx, %r11d
	shrl	$18, %edx
	shrl	$26, %r11d
	xorl	1792(%rax,%r11,4), %r12d
	movl	%edx, %r11d
	andl	$63, %r11d
	movl	1280(%rax,%r11,4), %edx
	movl	-132(%rbp), %r11d
	xorl	%r12d, %edx
	xorl	%edx, %esi
	movl	%ecx, %edx
	shrl	$26, %edx
	xorl	1536(%rax,%rdx,4), %esi
	movl	%ecx, %edx
	shrl	$2, %edx
	andl	$63, %edx
	xorl	(%rax,%rdx,4), %esi
	movl	%ecx, %edx
	shrl	$10, %edx
	andl	$63, %edx
	shrl	$18, %ecx
	xorl	512(%rax,%rdx,4), %esi
	andl	$63, %ecx
	movl	-136(%rbp), %edx
	xorl	1024(%rax,%rcx,4), %esi
	xorl	%esi, %edx
	xorl	%esi, %r11d
#APP
# 139 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%edx
# 0 "" 2
#NO_APP
	movl	%edx, %ecx
	movl	%edx, %r12d
	shrl	$2, %ecx
	shrl	$10, %r12d
	andl	$63, %r12d
	andl	$63, %ecx
	movl	256(%rax,%rcx,4), %ecx
	xorl	768(%rax,%r12,4), %ecx
	movl	%edx, %r12d
	shrl	$18, %edx
	andl	$63, %edx
	shrl	$26, %r12d
	xorl	1792(%rax,%r12,4), %ecx
	xorl	1280(%rax,%rdx,4), %ecx
	movl	%r11d, %edx
	shrl	$26, %edx
	xorl	%r8d, %ecx
	xorl	1536(%rax,%rdx,4), %ecx
	movl	%r11d, %edx
	shrl	$2, %edx
	andl	$63, %edx
	xorl	(%rax,%rdx,4), %ecx
	movl	%r11d, %edx
	shrl	$18, %r11d
	shrl	$10, %edx
	andl	$63, %r11d
	andl	$63, %edx
	xorl	512(%rax,%rdx,4), %ecx
	xorl	1024(%rax,%r11,4), %ecx
	xorl	%ecx, %ebx
	movl	%ecx, %r8d
	xorl	%ecx, %r10d
#APP
# 140 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r10d
# 0 "" 2
#NO_APP
	movl	%r10d, %edx
	movl	%r10d, %ecx
	shrl	$2, %edx
	shrl	$10, %ecx
	andl	$63, %ecx
	andl	$63, %edx
	movl	256(%rax,%rdx,4), %edx
	xorl	768(%rax,%rcx,4), %edx
	movl	%r10d, %ecx
	shrl	$18, %r10d
	shrl	$26, %ecx
	andl	$63, %r10d
	xorl	1792(%rax,%rcx,4), %edx
	movl	%ebx, %ecx
	xorl	1280(%rax,%r10,4), %edx
	shrl	$26, %ecx
	xorl	%esi, %edx
	xorl	1536(%rax,%rcx,4), %edx
	movl	%ebx, %ecx
	shrl	$2, %ecx
	andl	$63, %ecx
	xorl	(%rax,%rcx,4), %edx
	movl	%ebx, %ecx
	shrl	$18, %ebx
	shrl	$10, %ecx
	andl	$63, %ebx
	andl	$63, %ecx
	xorl	512(%rax,%rcx,4), %edx
	xorl	1024(%rax,%rbx,4), %edx
	xorl	%edx, %r9d
	xorl	%edx, %r13d
#APP
# 141 "../deps/openssl/openssl/crypto/des/des_enc.c" 1
	rorl $4,%r9d
# 0 "" 2
#NO_APP
	movl	%r9d, %ecx
	movl	%r9d, %esi
	shrl	$2, %ecx
	shrl	$10, %esi
	andl	$63, %esi
	andl	$63, %ecx
	movl	256(%rax,%rcx,4), %ecx
	xorl	768(%rax,%rsi,4), %ecx
	movl	%r9d, %esi
	shrl	$26, %esi
	xorl	1792(%rax,%rsi,4), %ecx
	shrl	$18, %r9d
	movl	%r13d, %esi
	andl	$63, %r9d
	shrl	$26, %esi
	xorl	1280(%rax,%r9,4), %ecx
	xorl	%r8d, %ecx
	xorl	1536(%rax,%rsi,4), %ecx
	movl	%r13d, %esi
	shrl	$2, %esi
	andl	$63, %esi
	xorl	(%rax,%rsi,4), %ecx
	movl	%r13d, %esi
	shrl	$18, %r13d
	shrl	$10, %esi
	andl	$63, %r13d
	andl	$63, %esi
	xorl	512(%rax,%rsi,4), %ecx
	xorl	1024(%rax,%r13,4), %ecx
	movl	%ecx, %eax
	jmp	.L8
	.cfi_endproc
.LFE152:
	.size	DES_encrypt2, .-DES_encrypt2
	.p2align 4
	.globl	DES_encrypt3
	.type	DES_encrypt3, @function
DES_encrypt3:
.LFB153:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	movl	(%rdi), %r8d
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%r8d, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	movl	%eax, %ecx
	shrl	$4, %ecx
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	xorl	%r8d, %ecx
	andl	$252645135, %ecx
	xorl	%ecx, %r9d
	sall	$4, %ecx
	movl	%r9d, %edx
	xorl	%ecx, %eax
	shrl	$16, %edx
	xorl	%eax, %edx
	movzwl	%dx, %r8d
	xorl	%r8d, %eax
	sall	$16, %r8d
	movl	%eax, %edx
	xorl	%r9d, %r8d
	movl	%eax, %ecx
	shrl	$2, %edx
	xorl	%r8d, %edx
	andl	$858993459, %edx
	xorl	%edx, %r8d
	leal	0(,%rdx,4), %eax
	movl	%r8d, %edx
	xorl	%ecx, %eax
	shrl	$8, %edx
	xorl	%eax, %edx
	andl	$16711935, %edx
	xorl	%edx, %eax
	sall	$8, %edx
	xorl	%edx, %r8d
	movl	%eax, %edx
	shrl	%edx
	xorl	%r8d, %edx
	andl	$1431655765, %edx
	xorl	%edx, %r8d
	addl	%edx, %edx
	xorl	%edx, %eax
	movl	%r8d, (%rdi)
	movl	$1, %edx
	movl	%eax, 4(%rdi)
	call	DES_encrypt2
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	DES_encrypt2
	movq	%r13, %rsi
	movl	$1, %edx
	call	DES_encrypt2
	popq	%r12
	popq	%r13
	movl	4(%rdi), %esi
	movl	(%rdi), %ecx
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%esi, %edx
	shrl	%edx
	xorl	%ecx, %edx
	andl	$1431655765, %edx
	xorl	%edx, %ecx
	leal	(%rdx,%rdx), %eax
	xorl	%esi, %eax
	movl	%ecx, %r8d
	shrl	$8, %ecx
	xorl	%eax, %ecx
	andl	$16711935, %ecx
	xorl	%ecx, %eax
	sall	$8, %ecx
	movl	%eax, %edx
	xorl	%r8d, %ecx
	movl	%eax, %esi
	shrl	$2, %edx
	xorl	%ecx, %edx
	andl	$858993459, %edx
	xorl	%edx, %ecx
	leal	0(,%rdx,4), %eax
	movl	%ecx, %edx
	xorl	%esi, %eax
	shrl	$16, %edx
	xorl	%eax, %edx
	movzwl	%dx, %edx
	xorl	%edx, %eax
	sall	$16, %edx
	xorl	%edx, %ecx
	movl	%eax, %edx
	shrl	$4, %edx
	xorl	%ecx, %edx
	andl	$252645135, %edx
	xorl	%edx, %ecx
	sall	$4, %edx
	xorl	%edx, %eax
	movl	%ecx, (%rdi)
	movl	%eax, 4(%rdi)
	ret
	.cfi_endproc
.LFE153:
	.size	DES_encrypt3, .-DES_encrypt3
	.p2align 4
	.globl	DES_decrypt3
	.type	DES_decrypt3, @function
DES_decrypt3:
.LFB154:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	movl	(%rdi), %r8d
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%r8d, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	movq	%rcx, %rsi
	movl	%eax, %ecx
	shrl	$4, %ecx
	xorl	%r8d, %ecx
	andl	$252645135, %ecx
	xorl	%ecx, %r9d
	sall	$4, %ecx
	movl	%r9d, %edx
	xorl	%ecx, %eax
	shrl	$16, %edx
	xorl	%eax, %edx
	movzwl	%dx, %r8d
	xorl	%r8d, %eax
	sall	$16, %r8d
	movl	%eax, %edx
	xorl	%r9d, %r8d
	movl	%eax, %ecx
	shrl	$2, %edx
	xorl	%r8d, %edx
	andl	$858993459, %edx
	xorl	%edx, %r8d
	leal	0(,%rdx,4), %eax
	movl	%r8d, %edx
	xorl	%ecx, %eax
	shrl	$8, %edx
	xorl	%eax, %edx
	andl	$16711935, %edx
	xorl	%edx, %eax
	sall	$8, %edx
	xorl	%edx, %r8d
	movl	%eax, %edx
	shrl	%edx
	xorl	%r8d, %edx
	andl	$1431655765, %edx
	xorl	%edx, %r8d
	addl	%edx, %edx
	xorl	%edx, %eax
	movl	%r8d, (%rdi)
	xorl	%edx, %edx
	movl	%eax, 4(%rdi)
	call	DES_encrypt2
	movq	%r13, %rsi
	movl	$1, %edx
	call	DES_encrypt2
	movq	%r12, %rsi
	xorl	%edx, %edx
	call	DES_encrypt2
	popq	%r12
	popq	%r13
	movl	4(%rdi), %esi
	movl	(%rdi), %ecx
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%esi, %edx
	shrl	%edx
	xorl	%ecx, %edx
	andl	$1431655765, %edx
	xorl	%edx, %ecx
	leal	(%rdx,%rdx), %eax
	xorl	%esi, %eax
	movl	%ecx, %r8d
	shrl	$8, %ecx
	xorl	%eax, %ecx
	andl	$16711935, %ecx
	xorl	%ecx, %eax
	sall	$8, %ecx
	movl	%eax, %edx
	xorl	%r8d, %ecx
	movl	%eax, %esi
	shrl	$2, %edx
	xorl	%ecx, %edx
	andl	$858993459, %edx
	xorl	%edx, %ecx
	leal	0(,%rdx,4), %eax
	movl	%ecx, %edx
	xorl	%esi, %eax
	shrl	$16, %edx
	xorl	%eax, %edx
	movzwl	%dx, %edx
	xorl	%edx, %eax
	sall	$16, %edx
	xorl	%edx, %ecx
	movl	%eax, %edx
	shrl	$4, %edx
	xorl	%ecx, %edx
	andl	$252645135, %edx
	xorl	%edx, %ecx
	sall	$4, %edx
	xorl	%edx, %eax
	movl	%ecx, (%rdi)
	movl	%eax, 4(%rdi)
	ret
	.cfi_endproc
.LFE154:
	.size	DES_decrypt3, .-DES_decrypt3
	.p2align 4
	.globl	DES_ncbc_encrypt
	.type	DES_ncbc_encrypt, @function
DES_ncbc_encrypt:
.LFB155:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-8(%rdx), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movl	(%r8), %ecx
	movl	4(%r8), %r12d
	movq	%rdi, -104(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%r8, -120(%rbp)
	movq	%fs:40, %rsi
	movq	%rsi, -56(%rbp)
	xorl	%esi, %esi
	movq	%rax, -112(%rbp)
	testl	%r9d, %r9d
	je	.L15
	testq	%rax, %rax
	js	.L42
	shrq	$3, %rax
	movq	%rdi, %r15
	leaq	-64(%rbp), %rdi
	movq	%rax, -72(%rbp)
	leaq	8(,%rax,8), %rax
	movq	%rax, -80(%rbp)
	leaq	(%rbx,%rax), %r14
	movl	%r12d, %eax
	.p2align 4,,10
	.p2align 3
.L17:
	movl	(%r15), %esi
	movl	4(%r15), %edx
	addq	$8, %rbx
	addq	$8, %r15
	xorl	%esi, %ecx
	xorl	%edx, %eax
	movq	%r13, %rsi
	movl	$1, %edx
	movl	%ecx, -64(%rbp)
	movl	%eax, -60(%rbp)
	call	DES_encrypt1
	movl	-64(%rbp), %ecx
	movl	%ecx, %eax
	movb	%cl, -8(%rbx)
	shrl	$16, %eax
	movb	%ch, -7(%rbx)
	movb	%al, -6(%rbx)
	movl	%ecx, %eax
	shrl	$24, %eax
	movb	%al, -5(%rbx)
	movl	-60(%rbp), %eax
	movl	%eax, %edx
	movb	%al, -4(%rbx)
	shrl	$16, %edx
	movb	%ah, -3(%rbx)
	movb	%dl, -2(%rbx)
	movl	%eax, %edx
	shrl	$24, %edx
	movb	%dl, -1(%rbx)
	cmpq	%r14, %rbx
	jne	.L17
	movq	-72(%rbp), %r14
	movl	%eax, %r12d
	movq	-96(%rbp), %rax
	movq	-80(%rbp), %rdi
	addq	%rdi, -104(%rbp)
	negq	%r14
	salq	$3, %r14
	leaq	-16(%rax,%r14), %rdx
	addq	-112(%rbp), %r14
	movq	%r14, -96(%rbp)
.L16:
	cmpq	$-8, %rdx
	je	.L59
	movq	-104(%rbp), %rax
	leaq	8(%rax,%rdx), %rdx
	movq	-96(%rbp), %rax
	cmpq	$7, %rax
	ja	.L20
	leaq	.L22(%rip), %r9
	movslq	(%r9,%rax,4), %rdi
	addq	%r9, %rdi
	notrack jmp	*%rdi
	.section	.rodata
	.align 4
	.align 4
.L22:
	.long	.L20-.L22
	.long	.L43-.L22
	.long	.L44-.L22
	.long	.L45-.L22
	.long	.L46-.L22
	.long	.L47-.L22
	.long	.L48-.L22
	.long	.L21-.L22
	.text
	.p2align 4,,10
	.p2align 3
.L15:
	testq	%rax, %rax
	js	.L30
	shrq	$3, %rax
	movq	%rdi, %r9
	movq	%r13, -80(%rbp)
	leaq	-64(%rbp), %rdi
	movq	%rax, -128(%rbp)
	leaq	8(,%rax,8), %rax
	movl	%r12d, %r13d
	movl	%ecx, %r15d
	movq	%rax, -136(%rbp)
	addq	%rbx, %rax
	movq	%r9, %r12
	movq	%rax, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L31:
	movl	%r15d, -72(%rbp)
	movq	-80(%rbp), %rsi
	movl	%r13d, %r14d
	xorl	%edx, %edx
	movl	(%r12), %r15d
	movl	4(%r12), %r13d
	addq	$8, %rbx
	addq	$8, %r12
	movl	%r15d, -64(%rbp)
	movl	%r13d, -60(%rbp)
	call	DES_encrypt1
	movl	-72(%rbp), %eax
	xorl	-64(%rbp), %eax
	movl	%eax, %edx
	movl	-60(%rbp), %eax
	movb	%dl, -8(%rbx)
	movl	%edx, %esi
	xorl	%r14d, %eax
	movb	%dh, -7(%rbx)
	shrl	$24, %edx
	movb	%dl, -5(%rbx)
	movl	%eax, %edx
	shrl	$16, %esi
	movb	%al, -4(%rbx)
	shrl	$16, %edx
	movb	%ah, -3(%rbx)
	shrl	$24, %eax
	movb	%sil, -6(%rbx)
	movb	%dl, -2(%rbx)
	movb	%al, -1(%rbx)
	cmpq	-88(%rbp), %rbx
	jne	.L31
	movq	-128(%rbp), %rdx
	movq	-96(%rbp), %rax
	movl	%r13d, %r12d
	movl	%r15d, %ecx
	movq	-136(%rbp), %rdi
	addq	%rdi, -104(%rbp)
	negq	%rdx
	movq	-80(%rbp), %r13
	salq	$3, %rdx
	leaq	-16(%rax,%rdx), %rax
	addq	-112(%rbp), %rdx
	movq	%rdx, -96(%rbp)
.L30:
	cmpq	$-8, %rax
	movq	%rax, -72(%rbp)
	jne	.L60
.L32:
	movq	-120(%rbp), %rax
	movl	%ecx, (%rax)
	movl	%r12d, 4(%rax)
.L14:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L61
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	movq	-104(%rbp), %rax
	leaq	-64(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	movl	%ecx, -80(%rbp)
	movl	(%rax), %r14d
	movl	4(%rax), %r15d
	movl	%r14d, -64(%rbp)
	movl	%r15d, -60(%rbp)
	call	DES_encrypt1
	movq	-72(%rbp), %rax
	movq	-96(%rbp), %rdi
	movl	-80(%rbp), %ecx
	xorl	-60(%rbp), %r12d
	xorl	-64(%rbp), %ecx
	leaq	8(%rbx,%rax), %rax
	cmpq	$7, %rdi
	ja	.L50
	leaq	.L34(%rip), %rsi
	movslq	(%rsi,%rdi,4), %rdx
	addq	%rsi, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L34:
	.long	.L50-.L34
	.long	.L40-.L34
	.long	.L51-.L34
	.long	.L38-.L34
	.long	.L52-.L34
	.long	.L36-.L34
	.long	.L35-.L34
	.long	.L33-.L34
	.text
.L33:
	movl	%r12d, %edx
	subq	$1, %rax
	shrl	$16, %edx
	movb	%dl, (%rax)
.L35:
	movl	%r12d, %ebx
	subq	$1, %rax
	movb	%bh, (%rax)
.L36:
	movb	%r12b, -1(%rax)
	leaq	-1(%rax), %rdx
.L37:
	movl	%ecx, %esi
	leaq	-1(%rdx), %rax
	shrl	$24, %esi
	movb	%sil, -1(%rdx)
.L38:
	movl	%ecx, %esi
	leaq	-1(%rax), %rdx
	shrl	$16, %esi
	movb	%sil, -1(%rax)
.L39:
	movb	%ch, -1(%rdx)
	leaq	-1(%rdx), %rax
.L40:
	movb	%cl, -1(%rax)
	movl	%r15d, %r12d
	movl	%r14d, %ecx
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L48:
	xorl	%r9d, %r9d
.L23:
	movzbl	-1(%rdx), %edi
	subq	$1, %rdx
	sall	$8, %edi
	orl	%r9d, %edi
.L24:
	leaq	-1(%rdx), %r9
	movzbl	-1(%rdx), %edx
	orl	%edx, %edi
	xorl	%edi, %r12d
.L25:
	leaq	-1(%r9), %rdx
	movzbl	-1(%r9), %r9d
	sall	$24, %r9d
.L26:
	movzbl	-1(%rdx), %edi
	leaq	-1(%rdx), %r10
	sall	$16, %edi
	orl	%edi, %r9d
.L27:
	movzbl	-1(%r10), %edi
	leaq	-1(%r10), %rdx
	sall	$8, %edi
	orl	%r9d, %edi
.L28:
	movzbl	-1(%rdx), %edx
	orl	%edx, %edi
	xorl	%edi, %ecx
.L20:
	leaq	-64(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movl	%ecx, -64(%rbp)
	movl	%r12d, -60(%rbp)
	call	DES_encrypt1
	movq	-64(%rbp), %r11
	movl	-64(%rbp), %ecx
	movl	-60(%rbp), %edx
	movq	%r11, (%rbx)
	movzbl	%ch, %eax
	movl	%ecx, %edi
	movl	%ecx, %r10d
	movl	%edx, %esi
	movl	%eax, %r9d
	movl	%edx, %r8d
	movzbl	%dh, %eax
	shrl	$16, %edi
	shrl	$24, %ecx
	shrl	$16, %esi
	shrl	$24, %edx
.L19:
	movq	-120(%rbp), %rbx
	movb	%r10b, (%rbx)
	movb	%r9b, 1(%rbx)
	movb	%dil, 2(%rbx)
	movb	%cl, 3(%rbx)
	movb	%r8b, 4(%rbx)
	movb	%al, 5(%rbx)
	movb	%sil, 6(%rbx)
	movb	%dl, 7(%rbx)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L42:
	movq	%rax, %rdx
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L59:
	movzbl	%ch, %eax
	movl	%ecx, %edi
	movl	%r12d, %esi
	movl	%r12d, %r8d
	shrl	$16, %edi
	movl	%eax, %r9d
	movl	%r12d, %eax
	shrl	$24, %r12d
	movl	%ecx, %r10d
	movzbl	%ah, %eax
	shrl	$24, %ecx
	movl	%r12d, %edx
	shrl	$16, %esi
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L43:
	xorl	%edi, %edi
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L44:
	movq	%rdx, %r10
	xorl	%r9d, %r9d
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L45:
	xorl	%r9d, %r9d
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L46:
	movq	%rdx, %r9
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L47:
	xorl	%edi, %edi
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L21:
	movzbl	-1(%rdx), %edi
	subq	$1, %rdx
	sall	$16, %edi
	movl	%edi, %r9d
	jmp	.L23
.L51:
	movq	%rax, %rdx
	jmp	.L39
.L52:
	movq	%rax, %rdx
	jmp	.L37
.L61:
	call	__stack_chk_fail@PLT
.L50:
	movl	%r15d, %r12d
	movl	%r14d, %ecx
	jmp	.L32
	.cfi_endproc
.LFE155:
	.size	DES_ncbc_encrypt, .-DES_ncbc_encrypt
	.p2align 4
	.globl	DES_ede3_cbc_encrypt
	.type	DES_ede3_cbc_encrypt, @function
DES_ede3_cbc_encrypt:
.LFB156:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rdx, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	%rsi, -120(%rbp)
	movq	%rcx, %rsi
	movq	%rdx, -112(%rbp)
	movq	%r9, %rcx
	movq	%r8, %rdx
	movq	%rax, -136(%rbp)
	movl	4(%rax), %r12d
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	movl	(%rax), %ebx
	leaq	-8(%rdi), %rax
	movl	24(%rbp), %edi
	movq	%rax, -128(%rbp)
	testl	%edi, %edi
	je	.L63
	testq	%rax, %rax
	js	.L90
	shrq	$3, %rax
	movl	%r12d, %edx
	leaq	-64(%rbp), %rdi
	movq	%r9, %r12
	movq	%rax, -80(%rbp)
	leaq	8(,%rax,8), %rax
	leaq	0(%r13,%rax), %r14
	movq	%rax, -88(%rbp)
	movl	%ebx, %eax
	movq	%r8, %rbx
	movq	%r14, -72(%rbp)
	movq	%r13, %r14
	movq	%rsi, %r13
	.p2align 4,,10
	.p2align 3
.L65:
	movl	4(%r14), %ecx
	movl	(%r14), %esi
	addq	$8, %r15
	addq	$8, %r14
	xorl	%ecx, %edx
	xorl	%esi, %eax
	movq	%r12, %rcx
	movq	%r13, %rsi
	movl	%edx, -60(%rbp)
	movq	%rbx, %rdx
	movl	%eax, -64(%rbp)
	call	DES_encrypt3
	movl	-64(%rbp), %eax
	movl	-60(%rbp), %edx
	movzbl	%ah, %ecx
	movb	%al, -8(%r15)
	movb	%cl, -7(%r15)
	movl	%eax, %ecx
	shrl	$16, %ecx
	movb	%dl, -4(%r15)
	movb	%cl, -6(%r15)
	movl	%eax, %ecx
	shrl	$24, %ecx
	movb	%cl, -5(%r15)
	movzbl	%dh, %ecx
	movb	%cl, -3(%r15)
	movl	%edx, %ecx
	shrl	$16, %ecx
	movb	%cl, -2(%r15)
	movl	%edx, %ecx
	shrl	$24, %ecx
	movb	%cl, -1(%r15)
	cmpq	-72(%rbp), %r14
	jne	.L65
	movq	%r12, %rcx
	movl	%edx, %r12d
	movq	%rbx, %rdx
	movl	%eax, %ebx
	movq	-80(%rbp), %rax
	movq	-88(%rbp), %rdi
	movq	%r13, %rsi
	movq	%r14, %r13
	addq	%rdi, -120(%rbp)
	movq	-112(%rbp), %rdi
	negq	%rax
	salq	$3, %rax
	leaq	-16(%rdi,%rax), %rdi
	addq	-128(%rbp), %rax
	movq	%rax, -112(%rbp)
.L64:
	cmpq	$-8, %rdi
	je	.L107
	leaq	8(%r13,%rdi), %rax
	movq	-112(%rbp), %rdi
	cmpq	$7, %rdi
	ja	.L68
	leaq	.L70(%rip), %r8
	movslq	(%r8,%rdi,4), %rdi
	addq	%r8, %rdi
	notrack jmp	*%rdi
	.section	.rodata
	.align 4
	.align 4
.L70:
	.long	.L68-.L70
	.long	.L91-.L70
	.long	.L92-.L70
	.long	.L93-.L70
	.long	.L94-.L70
	.long	.L95-.L70
	.long	.L96-.L70
	.long	.L69-.L70
	.text
	.p2align 4,,10
	.p2align 3
.L63:
	testq	%rax, %rax
	js	.L97
	shrq	$3, %rax
	movq	%rsi, -88(%rbp)
	movq	%r15, %r14
	leaq	-64(%rbp), %rdi
	movq	%rax, -144(%rbp)
	leaq	8(,%rax,8), %rax
	movl	%ebx, %r15d
	movl	%r12d, %ebx
	movq	%rax, -152(%rbp)
	addq	%r13, %rax
	movq	%r13, %r12
	movq	%r9, %r13
	movq	%rax, -104(%rbp)
	movq	%r8, -96(%rbp)
	.p2align 4,,10
	.p2align 3
.L79:
	movl	%r15d, -80(%rbp)
	movq	-96(%rbp), %rdx
	movq	%r13, %rcx
	addq	$8, %r14
	movl	(%r12), %r15d
	movq	-88(%rbp), %rsi
	movl	%ebx, -72(%rbp)
	addq	$8, %r12
	movl	-4(%r12), %ebx
	movl	%r15d, -64(%rbp)
	movl	%ebx, -60(%rbp)
	call	DES_decrypt3
	movl	-80(%rbp), %r8d
	xorl	-64(%rbp), %r8d
	movl	%r8d, %ecx
	movl	-72(%rbp), %eax
	xorl	-60(%rbp), %eax
	movb	%r8b, -8(%r14)
	movzbl	%ch, %edx
	movb	%al, -4(%r14)
	movb	%dl, -7(%r14)
	movl	%r8d, %edx
	shrl	$24, %r8d
	shrl	$16, %edx
	movb	%r8b, -5(%r14)
	movb	%dl, -6(%r14)
	movzbl	%ah, %edx
	movb	%dl, -3(%r14)
	movl	%eax, %edx
	shrl	$24, %eax
	shrl	$16, %edx
	movb	%dl, -2(%r14)
	movb	%al, -1(%r14)
	cmpq	-104(%rbp), %r12
	jne	.L79
	movq	-144(%rbp), %rax
	movq	-152(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r12, %r13
	addq	%rdi, -120(%rbp)
	movq	-112(%rbp), %rdi
	movl	%ebx, %r12d
	movl	%r15d, %ebx
	negq	%rax
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %rdx
	salq	$3, %rax
	leaq	-16(%rdi,%rax), %r14
	addq	-128(%rbp), %rax
	movq	%rax, -112(%rbp)
.L78:
	cmpq	$-8, %r14
	je	.L80
	movl	0(%r13), %r15d
	movl	4(%r13), %r13d
	leaq	-64(%rbp), %rdi
	movl	%r15d, -64(%rbp)
	movl	%r13d, -60(%rbp)
	call	DES_decrypt3
	movq	-120(%rbp), %rax
	movq	-112(%rbp), %rsi
	xorl	-64(%rbp), %ebx
	xorl	-60(%rbp), %r12d
	leaq	8(%rax,%r14), %rax
	cmpq	$7, %rsi
	ja	.L98
	leaq	.L82(%rip), %rcx
	movslq	(%rcx,%rsi,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L82:
	.long	.L98-.L82
	.long	.L88-.L82
	.long	.L99-.L82
	.long	.L86-.L82
	.long	.L100-.L82
	.long	.L84-.L82
	.long	.L83-.L82
	.long	.L81-.L82
	.text
.L81:
	movl	%r12d, %edx
	subq	$1, %rax
	shrl	$16, %edx
	movb	%dl, (%rax)
.L83:
	movl	%r12d, %ecx
	subq	$1, %rax
	movb	%ch, (%rax)
.L84:
	movb	%r12b, -1(%rax)
	leaq	-1(%rax), %rdx
.L85:
	movl	%ebx, %ecx
	leaq	-1(%rdx), %rax
	shrl	$24, %ecx
	movb	%cl, -1(%rdx)
.L86:
	movl	%ebx, %ecx
	leaq	-1(%rax), %rdx
	shrl	$16, %ecx
	movb	%cl, -1(%rax)
.L87:
	movb	%bh, -1(%rdx)
	leaq	-1(%rdx), %rax
.L88:
	movb	%bl, -1(%rax)
	movl	%r13d, %r12d
	movl	%r15d, %ebx
.L80:
	movq	-136(%rbp), %rax
	movl	%ebx, (%rax)
	movl	%r12d, 4(%rax)
.L62:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L108
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	xorl	%r8d, %r8d
.L71:
	movzbl	-1(%rax), %edi
	subq	$1, %rax
	sall	$8, %edi
	orl	%r8d, %edi
.L72:
	leaq	-1(%rax), %r8
	movzbl	-1(%rax), %eax
	orl	%eax, %edi
	xorl	%edi, %r12d
.L73:
	leaq	-1(%r8), %rax
	movzbl	-1(%r8), %r8d
	sall	$24, %r8d
.L74:
	movzbl	-1(%rax), %edi
	leaq	-1(%rax), %r9
	sall	$16, %edi
	orl	%edi, %r8d
.L75:
	movzbl	-1(%r9), %edi
	leaq	-1(%r9), %rax
	sall	$8, %edi
	orl	%r8d, %edi
.L76:
	movzbl	-1(%rax), %eax
	orl	%eax, %edi
	xorl	%edi, %ebx
.L68:
	leaq	-64(%rbp), %rdi
	movl	%ebx, -64(%rbp)
	movl	%r12d, -60(%rbp)
	call	DES_encrypt3
	movl	-60(%rbp), %eax
	movq	-64(%rbp), %r10
	movq	-120(%rbp), %r11
	movl	-64(%rbp), %ebx
	movzbl	%ah, %edx
	movl	%eax, %r9d
	movq	%r10, (%r11)
	movl	%ebx, %ecx
	movl	%edx, %r8d
	movl	%eax, %edx
	shrl	$16, %ecx
	movl	%ebx, %edi
	movzbl	%bh, %esi
	shrl	$16, %edx
	shrl	$24, %ebx
	shrl	$24, %eax
.L67:
	movq	-136(%rbp), %r11
	movb	%dil, (%r11)
	movb	%sil, 1(%r11)
	movb	%cl, 2(%r11)
	movb	%bl, 3(%r11)
	movb	%r9b, 4(%r11)
	movb	%r8b, 5(%r11)
	movb	%dl, 6(%r11)
	movb	%al, 7(%r11)
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L90:
	movq	%rax, %rdi
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L97:
	movq	%rax, %r14
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L107:
	movl	%r12d, %eax
	movl	%ebx, %ecx
	movl	%r12d, %edx
	movl	%r12d, %r9d
	movzbl	%ah, %eax
	shrl	$24, %r12d
	movl	%ebx, %edi
	movzbl	%bh, %esi
	shrl	$16, %ecx
	movl	%eax, %r8d
	shrl	$24, %ebx
	movl	%r12d, %eax
	shrl	$16, %edx
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L91:
	xorl	%edi, %edi
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L92:
	movq	%rax, %r9
	xorl	%r8d, %r8d
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L93:
	xorl	%r8d, %r8d
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L94:
	movq	%rax, %r8
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L95:
	xorl	%edi, %edi
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L69:
	movzbl	-1(%rax), %edi
	subq	$1, %rax
	sall	$16, %edi
	movl	%edi, %r8d
	jmp	.L71
.L99:
	movq	%rax, %rdx
	jmp	.L87
.L100:
	movq	%rax, %rdx
	jmp	.L85
.L108:
	call	__stack_chk_fail@PLT
.L98:
	movl	%r13d, %r12d
	movl	%r15d, %ebx
	jmp	.L80
	.cfi_endproc
.LFE156:
	.size	DES_ede3_cbc_encrypt, .-DES_ede3_cbc_encrypt
	.globl	DES_SPtrans
	.section	.rodata
	.align 32
	.type	DES_SPtrans, @object
	.size	DES_SPtrans, 2048
DES_SPtrans:
	.long	34080768
	.long	524288
	.long	33554434
	.long	34080770
	.long	33554432
	.long	526338
	.long	524290
	.long	33554434
	.long	526338
	.long	34080768
	.long	34078720
	.long	2050
	.long	33556482
	.long	33554432
	.long	0
	.long	524290
	.long	524288
	.long	2
	.long	33556480
	.long	526336
	.long	34080770
	.long	34078720
	.long	2050
	.long	33556480
	.long	2
	.long	2048
	.long	526336
	.long	34078722
	.long	2048
	.long	33556482
	.long	34078722
	.long	0
	.long	0
	.long	34080770
	.long	33556480
	.long	524290
	.long	34080768
	.long	524288
	.long	2050
	.long	33556480
	.long	34078722
	.long	2048
	.long	526336
	.long	33554434
	.long	526338
	.long	2
	.long	33554434
	.long	34078720
	.long	34080770
	.long	526336
	.long	34078720
	.long	33556482
	.long	33554432
	.long	2050
	.long	524290
	.long	0
	.long	524288
	.long	33554432
	.long	33556482
	.long	34080768
	.long	2
	.long	34078722
	.long	2048
	.long	526338
	.long	1074823184
	.long	0
	.long	1081344
	.long	1074790400
	.long	1073741840
	.long	32784
	.long	1073774592
	.long	1081344
	.long	32768
	.long	1074790416
	.long	16
	.long	1073774592
	.long	1048592
	.long	1074823168
	.long	1074790400
	.long	16
	.long	1048576
	.long	1073774608
	.long	1074790416
	.long	32768
	.long	1081360
	.long	1073741824
	.long	0
	.long	1048592
	.long	1073774608
	.long	1081360
	.long	1074823168
	.long	1073741840
	.long	1073741824
	.long	1048576
	.long	32784
	.long	1074823184
	.long	1048592
	.long	1074823168
	.long	1073774592
	.long	1081360
	.long	1074823184
	.long	1048592
	.long	1073741840
	.long	0
	.long	1073741824
	.long	32784
	.long	1048576
	.long	1074790416
	.long	32768
	.long	1073741824
	.long	1081360
	.long	1073774608
	.long	1074823168
	.long	32768
	.long	0
	.long	1073741840
	.long	16
	.long	1074823184
	.long	1081344
	.long	1074790400
	.long	1074790416
	.long	1048576
	.long	32784
	.long	1073774592
	.long	1073774608
	.long	16
	.long	1074790400
	.long	1081344
	.long	67108865
	.long	67371264
	.long	256
	.long	67109121
	.long	262145
	.long	67108864
	.long	67109121
	.long	262400
	.long	67109120
	.long	262144
	.long	67371008
	.long	1
	.long	67371265
	.long	257
	.long	1
	.long	67371009
	.long	0
	.long	262145
	.long	67371264
	.long	256
	.long	257
	.long	67371265
	.long	262144
	.long	67108865
	.long	67371009
	.long	67109120
	.long	262401
	.long	67371008
	.long	262400
	.long	0
	.long	67108864
	.long	262401
	.long	67371264
	.long	256
	.long	1
	.long	262144
	.long	257
	.long	262145
	.long	67371008
	.long	67109121
	.long	0
	.long	67371264
	.long	262400
	.long	67371009
	.long	262145
	.long	67108864
	.long	67371265
	.long	1
	.long	262401
	.long	67108865
	.long	67108864
	.long	67371265
	.long	262144
	.long	67109120
	.long	67109121
	.long	262400
	.long	67109120
	.long	0
	.long	67371009
	.long	257
	.long	67108865
	.long	262401
	.long	256
	.long	67371008
	.long	4198408
	.long	268439552
	.long	8
	.long	272633864
	.long	0
	.long	272629760
	.long	268439560
	.long	4194312
	.long	272633856
	.long	268435464
	.long	268435456
	.long	4104
	.long	268435464
	.long	4198408
	.long	4194304
	.long	268435456
	.long	272629768
	.long	4198400
	.long	4096
	.long	8
	.long	4198400
	.long	268439560
	.long	272629760
	.long	4096
	.long	4104
	.long	0
	.long	4194312
	.long	272633856
	.long	268439552
	.long	272629768
	.long	272633864
	.long	4194304
	.long	272629768
	.long	4104
	.long	4194304
	.long	268435464
	.long	4198400
	.long	268439552
	.long	8
	.long	272629760
	.long	268439560
	.long	0
	.long	4096
	.long	4194312
	.long	0
	.long	272629768
	.long	272633856
	.long	4096
	.long	268435456
	.long	272633864
	.long	4198408
	.long	4194304
	.long	272633864
	.long	8
	.long	268439552
	.long	4198408
	.long	4194312
	.long	4198400
	.long	272629760
	.long	268439560
	.long	4104
	.long	268435456
	.long	268435464
	.long	272633856
	.long	134217728
	.long	65536
	.long	1024
	.long	134284320
	.long	134283296
	.long	134218752
	.long	66592
	.long	134283264
	.long	65536
	.long	32
	.long	134217760
	.long	66560
	.long	134218784
	.long	134283296
	.long	134284288
	.long	0
	.long	66560
	.long	134217728
	.long	65568
	.long	1056
	.long	134218752
	.long	66592
	.long	0
	.long	134217760
	.long	32
	.long	134218784
	.long	134284320
	.long	65568
	.long	134283264
	.long	1024
	.long	1056
	.long	134284288
	.long	134284288
	.long	134218784
	.long	65568
	.long	134283264
	.long	65536
	.long	32
	.long	134217760
	.long	134218752
	.long	134217728
	.long	66560
	.long	134284320
	.long	0
	.long	66592
	.long	134217728
	.long	1024
	.long	65568
	.long	134218784
	.long	1024
	.long	0
	.long	134284320
	.long	134283296
	.long	134284288
	.long	1056
	.long	65536
	.long	66560
	.long	134283296
	.long	134218752
	.long	1056
	.long	32
	.long	66592
	.long	134283264
	.long	134217760
	.long	-2147483584
	.long	2097216
	.long	0
	.long	-2145378304
	.long	2097216
	.long	8192
	.long	-2147475392
	.long	2097152
	.long	8256
	.long	-2145378240
	.long	2105344
	.long	-2147483648
	.long	-2147475456
	.long	-2147483584
	.long	-2145386496
	.long	2105408
	.long	2097152
	.long	-2147475392
	.long	-2145386432
	.long	0
	.long	8192
	.long	64
	.long	-2145378304
	.long	-2145386432
	.long	-2145378240
	.long	-2145386496
	.long	-2147483648
	.long	8256
	.long	64
	.long	2105344
	.long	2105408
	.long	-2147475456
	.long	8256
	.long	-2147483648
	.long	-2147475456
	.long	2105408
	.long	-2145378304
	.long	2097216
	.long	0
	.long	-2147475456
	.long	-2147483648
	.long	8192
	.long	-2145386432
	.long	2097152
	.long	2097216
	.long	-2145378240
	.long	2105344
	.long	64
	.long	-2145378240
	.long	2105344
	.long	2097152
	.long	-2147475392
	.long	-2147483584
	.long	-2145386496
	.long	2105408
	.long	0
	.long	8192
	.long	-2147483584
	.long	-2147475392
	.long	-2145378304
	.long	-2145386496
	.long	8256
	.long	64
	.long	-2145386432
	.long	16384
	.long	512
	.long	16777728
	.long	16777220
	.long	16794116
	.long	16388
	.long	16896
	.long	0
	.long	16777216
	.long	16777732
	.long	516
	.long	16793600
	.long	4
	.long	16794112
	.long	16793600
	.long	516
	.long	16777732
	.long	16384
	.long	16388
	.long	16794116
	.long	0
	.long	16777728
	.long	16777220
	.long	16896
	.long	16793604
	.long	16900
	.long	16794112
	.long	4
	.long	16900
	.long	16793604
	.long	512
	.long	16777216
	.long	16900
	.long	16793600
	.long	16793604
	.long	516
	.long	16384
	.long	512
	.long	16777216
	.long	16793604
	.long	16777732
	.long	16900
	.long	16896
	.long	0
	.long	512
	.long	16777220
	.long	4
	.long	16777728
	.long	0
	.long	16777732
	.long	16777728
	.long	16896
	.long	516
	.long	16384
	.long	16794116
	.long	16777216
	.long	16794112
	.long	4
	.long	16388
	.long	16794116
	.long	16777220
	.long	16794112
	.long	16793600
	.long	16388
	.long	545259648
	.long	545390592
	.long	131200
	.long	0
	.long	537001984
	.long	8388736
	.long	545259520
	.long	545390720
	.long	128
	.long	536870912
	.long	8519680
	.long	131200
	.long	8519808
	.long	537002112
	.long	536871040
	.long	545259520
	.long	131072
	.long	8519808
	.long	8388736
	.long	537001984
	.long	545390720
	.long	536871040
	.long	0
	.long	8519680
	.long	536870912
	.long	8388608
	.long	537002112
	.long	545259648
	.long	8388608
	.long	131072
	.long	545390592
	.long	128
	.long	8388608
	.long	131072
	.long	536871040
	.long	545390720
	.long	131200
	.long	536870912
	.long	0
	.long	8519680
	.long	545259648
	.long	537002112
	.long	537001984
	.long	8388736
	.long	545390592
	.long	128
	.long	8388736
	.long	537001984
	.long	545390720
	.long	8388608
	.long	545259520
	.long	536871040
	.long	8519680
	.long	131200
	.long	537002112
	.long	545259520
	.long	128
	.long	545390592
	.long	8519808
	.long	0
	.long	536870912
	.long	545259648
	.long	131072
	.long	8519808
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
