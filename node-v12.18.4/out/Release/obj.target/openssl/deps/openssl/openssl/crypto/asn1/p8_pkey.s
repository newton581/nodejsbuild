	.file	"p8_pkey.c"
	.text
	.p2align 4
	.type	pkey_cb, @function
pkey_cb:
.LFB805:
	.cfi_startproc
	endbr64
	cmpl	$2, %edi
	je	.L11
.L7:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	(%rsi), %rax
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L7
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	(%rax), %rsi
	movq	8(%rax), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	OPENSSL_cleanse@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE805:
	.size	pkey_cb, .-pkey_cb
	.p2align 4
	.globl	d2i_PKCS8_PRIV_KEY_INFO
	.type	d2i_PKCS8_PRIV_KEY_INFO, @function
d2i_PKCS8_PRIV_KEY_INFO:
.LFB806:
	.cfi_startproc
	endbr64
	leaq	PKCS8_PRIV_KEY_INFO_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE806:
	.size	d2i_PKCS8_PRIV_KEY_INFO, .-d2i_PKCS8_PRIV_KEY_INFO
	.p2align 4
	.globl	i2d_PKCS8_PRIV_KEY_INFO
	.type	i2d_PKCS8_PRIV_KEY_INFO, @function
i2d_PKCS8_PRIV_KEY_INFO:
.LFB807:
	.cfi_startproc
	endbr64
	leaq	PKCS8_PRIV_KEY_INFO_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE807:
	.size	i2d_PKCS8_PRIV_KEY_INFO, .-i2d_PKCS8_PRIV_KEY_INFO
	.p2align 4
	.globl	PKCS8_PRIV_KEY_INFO_new
	.type	PKCS8_PRIV_KEY_INFO_new, @function
PKCS8_PRIV_KEY_INFO_new:
.LFB808:
	.cfi_startproc
	endbr64
	leaq	PKCS8_PRIV_KEY_INFO_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE808:
	.size	PKCS8_PRIV_KEY_INFO_new, .-PKCS8_PRIV_KEY_INFO_new
	.p2align 4
	.globl	PKCS8_PRIV_KEY_INFO_free
	.type	PKCS8_PRIV_KEY_INFO_free, @function
PKCS8_PRIV_KEY_INFO_free:
.LFB809:
	.cfi_startproc
	endbr64
	leaq	PKCS8_PRIV_KEY_INFO_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE809:
	.size	PKCS8_PRIV_KEY_INFO_free, .-PKCS8_PRIV_KEY_INFO_free
	.p2align 4
	.globl	PKCS8_pkey_set0
	.type	PKCS8_pkey_set0, @function
PKCS8_pkey_set0:
.LFB810:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	testl	%edx, %edx
	js	.L20
	movq	(%rdi), %rdi
	movslq	%edx, %rsi
	call	ASN1_INTEGER_set@PLT
	testl	%eax, %eax
	je	.L18
.L20:
	movq	8(%rbx), %rdi
	movq	%r15, %rcx
	movl	%r14d, %edx
	movq	%r13, %rsi
	call	X509_ALGOR_set0@PLT
	testl	%eax, %eax
	je	.L18
	movl	$1, %r13d
	testq	%r12, %r12
	je	.L16
	movq	16(%rbx), %rdi
	movl	16(%rbp), %edx
	movq	%r12, %rsi
	call	ASN1_STRING_set0@PLT
.L16:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	xorl	%r13d, %r13d
	jmp	.L16
	.cfi_endproc
.LFE810:
	.size	PKCS8_pkey_set0, .-PKCS8_pkey_set0
	.p2align 4
	.globl	PKCS8_pkey_get0
	.type	PKCS8_pkey_get0, @function
PKCS8_pkey_get0:
.LFB811:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%r8, %rbx
	testq	%rdi, %rdi
	je	.L30
	movq	8(%r8), %rax
	movq	(%rax), %rax
	movq	%rax, (%rdi)
.L30:
	testq	%r13, %r13
	je	.L31
	movq	16(%rbx), %rdi
	call	ASN1_STRING_get0_data@PLT
	movq	16(%rbx), %rdi
	movq	%rax, 0(%r13)
	call	ASN1_STRING_length@PLT
	movl	%eax, (%r14)
.L31:
	testq	%r12, %r12
	je	.L32
	movq	8(%rbx), %rax
	movq	%rax, (%r12)
.L32:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE811:
	.size	PKCS8_pkey_get0, .-PKCS8_pkey_get0
	.p2align 4
	.globl	PKCS8_pkey_get0_attrs
	.type	PKCS8_pkey_get0_attrs, @function
PKCS8_pkey_get0_attrs:
.LFB812:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE812:
	.size	PKCS8_pkey_get0_attrs, .-PKCS8_pkey_get0_attrs
	.p2align 4
	.globl	PKCS8_pkey_add1_attr_by_NID
	.type	PKCS8_pkey_add1_attr_by_NID, @function
PKCS8_pkey_add1_attr_by_NID:
.LFB813:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$24, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509at_add1_attr_by_NID@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE813:
	.size	PKCS8_pkey_add1_attr_by_NID, .-PKCS8_pkey_add1_attr_by_NID
	.globl	PKCS8_PRIV_KEY_INFO_it
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"PKCS8_PRIV_KEY_INFO"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	PKCS8_PRIV_KEY_INFO_it, @object
	.size	PKCS8_PRIV_KEY_INFO_it, 56
PKCS8_PRIV_KEY_INFO_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	PKCS8_PRIV_KEY_INFO_seq_tt
	.quad	4
	.quad	PKCS8_PRIV_KEY_INFO_aux
	.quad	32
	.quad	.LC0
	.section	.rodata.str1.1
.LC1:
	.string	"version"
.LC2:
	.string	"pkeyalg"
.LC3:
	.string	"pkey"
.LC4:
	.string	"attributes"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	PKCS8_PRIV_KEY_INFO_seq_tt, @object
	.size	PKCS8_PRIV_KEY_INFO_seq_tt, 160
PKCS8_PRIV_KEY_INFO_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC1
	.quad	ASN1_INTEGER_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC2
	.quad	X509_ALGOR_it
	.quad	0
	.quad	0
	.quad	16
	.quad	.LC3
	.quad	ASN1_OCTET_STRING_it
	.quad	139
	.quad	0
	.quad	24
	.quad	.LC4
	.quad	X509_ATTRIBUTE_it
	.section	.data.rel.ro.local
	.align 32
	.type	PKCS8_PRIV_KEY_INFO_aux, @object
	.size	PKCS8_PRIV_KEY_INFO_aux, 40
PKCS8_PRIV_KEY_INFO_aux:
	.quad	0
	.long	0
	.long	0
	.long	0
	.zero	4
	.quad	pkey_cb
	.long	0
	.zero	4
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
