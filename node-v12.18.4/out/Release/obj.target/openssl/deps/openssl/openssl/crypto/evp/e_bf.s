	.file	"e_bf.c"
	.text
	.p2align 4
	.type	bf_cbc_cipher, @function
bf_cbc_cipher:
.LFB445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -80(%rbp)
	movabsq	$4611686018427387903, %rdx
	movq	%rsi, -72(%rbp)
	cmpq	%rdx, %rcx
	jbe	.L2
	movq	%rcx, %r14
	addq	%r12, %rax
	leaq	(%rsi,%rcx), %rcx
	movq	%rdx, %r12
	movq	%rcx, -56(%rbp)
	movq	%rax, -64(%rbp)
	movq	%r14, -88(%rbp)
.L3:
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r13, %rdi
	movl	%eax, %r15d
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movl	%r15d, %r9d
	movabsq	$4611686018427387904, %rdx
	movq	%rax, %rcx
	movq	%rbx, %r8
	subq	%r14, %rsi
	subq	%r14, %rdi
	call	BF_cbc_encrypt@PLT
	movabsq	$-4611686018427387904, %rax
	addq	%rax, %r14
	cmpq	%r12, %r14
	ja	.L3
	movq	-88(%rbp), %r14
	leaq	(%r14,%rax), %r10
	andq	%r14, %r12
	shrq	$62, %r10
	addq	$1, %r10
	salq	$62, %r10
	addq	%r10, -80(%rbp)
	addq	%r10, -72(%rbp)
.L2:
	testq	%r12, %r12
	jne	.L13
.L4:
	addq	$56, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r13, %rdi
	movl	%eax, %r15d
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	-72(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movl	%r15d, %r9d
	movq	%rax, %rcx
	movq	%rbx, %r8
	movq	%r12, %rdx
	call	BF_cbc_encrypt@PLT
	jmp	.L4
	.cfi_endproc
.LFE445:
	.size	bf_cbc_cipher, .-bf_cbc_cipher
	.p2align 4
	.type	bf_init_key, @function
bf_init_key:
.LFB453:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r14, %rdx
	movl	%r13d, %esi
	movq	%rax, %rdi
	call	BF_set_key@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE453:
	.size	bf_init_key, .-bf_init_key
	.p2align 4
	.type	bf_cfb64_cipher, @function
bf_cfb64_cipher:
.LFB446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, %rcx
	ja	.L22
	movq	%rcx, %rbx
	testq	%rcx, %rcx
	je	.L18
.L17:
	leaq	-60(%rbp), %rax
	movq	%rax, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L19:
	movq	%r12, %rdi
	subq	%rbx, %r15
	call	EVP_CIPHER_CTX_num@PLT
	movq	%r12, %rdi
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r12, %rdi
	movl	%eax, -76(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movl	-76(%rbp), %edx
	subq	$8, %rsp
	movq	-88(%rbp), %r9
	movq	-72(%rbp), %r8
	movq	%rax, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	pushq	%rdx
	movq	%rbx, %rdx
	addq	%rbx, %r14
	addq	%rbx, %r13
	call	BF_cfb64_encrypt@PLT
	movl	-60(%rbp), %esi
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	cmpq	%r15, %rbx
	popq	%rax
	popq	%rdx
	cmova	%r15, %rbx
	testq	%r15, %r15
	je	.L18
	cmpq	%rbx, %r15
	jnb	.L19
.L18:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L31
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	movabsq	$4611686018427387904, %rbx
	jmp	.L17
.L31:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE446:
	.size	bf_cfb64_cipher, .-bf_cfb64_cipher
	.p2align 4
	.type	bf_ofb_cipher, @function
bf_ofb_cipher:
.LFB448:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -96(%rbp)
	movabsq	$4611686018427387903, %rdx
	movq	%rsi, -88(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	movq	%rcx, %rbx
	cmpq	%rdx, %rcx
	jbe	.L33
	addq	%rcx, %rax
	movq	%rcx, -104(%rbp)
	leaq	(%rsi,%rcx), %rbx
	movq	%rcx, %r13
	movq	%rax, -80(%rbp)
	leaq	-60(%rbp), %r15
	movq	%rbx, -72(%rbp)
	movq	%rdx, %rbx
.L34:
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_num@PLT
	movq	%r12, %rdi
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	-72(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movq	%r15, %r9
	movabsq	$4611686018427387904, %rdx
	movq	%rax, %rcx
	movq	%r14, %r8
	subq	%r13, %rsi
	subq	%r13, %rdi
	call	BF_ofb64_encrypt@PLT
	movl	-60(%rbp), %esi
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	movabsq	$-4611686018427387904, %rax
	addq	%rax, %r13
	cmpq	%rbx, %r13
	ja	.L34
	movq	-104(%rbp), %r13
	leaq	0(%r13,%rax), %r11
	andq	%r13, %rbx
	shrq	$62, %r11
	addq	$1, %r11
	salq	$62, %r11
	addq	%r11, -96(%rbp)
	addq	%r11, -88(%rbp)
.L33:
	testq	%rbx, %rbx
	jne	.L44
.L35:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L45
	addq	$72, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_num@PLT
	movq	%r12, %rdi
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	%r14, %r8
	movq	%rax, %rcx
	leaq	-60(%rbp), %r9
	movq	%rbx, %rdx
	call	BF_ofb64_encrypt@PLT
	movl	-60(%rbp), %esi
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	jmp	.L35
.L45:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE448:
	.size	bf_ofb_cipher, .-bf_ofb_cipher
	.p2align 4
	.type	bf_ecb_cipher, @function
bf_ecb_cipher:
.LFB447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%rsi, -56(%rbp)
	call	EVP_CIPHER_CTX_cipher@PLT
	movslq	4(%rax), %r15
	cmpq	%r12, %r15
	ja	.L47
	subq	%r15, %r12
	movq	%r12, -64(%rbp)
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L48:
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%rbx, %rdi
	movl	%eax, %r13d
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	leaq	(%r14,%r12), %rdi
	movl	%r13d, %ecx
	movq	%rax, %rdx
	movq	-56(%rbp), %rax
	leaq	(%rax,%r12), %rsi
	addq	%r15, %r12
	call	BF_ecb_encrypt@PLT
	cmpq	%r12, -64(%rbp)
	jnb	.L48
.L47:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE447:
	.size	bf_ecb_cipher, .-bf_ecb_cipher
	.p2align 4
	.globl	EVP_bf_cbc
	.type	EVP_bf_cbc, @function
EVP_bf_cbc:
.LFB449:
	.cfi_startproc
	endbr64
	leaq	bf_cbc(%rip), %rax
	ret
	.cfi_endproc
.LFE449:
	.size	EVP_bf_cbc, .-EVP_bf_cbc
	.p2align 4
	.globl	EVP_bf_cfb64
	.type	EVP_bf_cfb64, @function
EVP_bf_cfb64:
.LFB450:
	.cfi_startproc
	endbr64
	leaq	bf_cfb64(%rip), %rax
	ret
	.cfi_endproc
.LFE450:
	.size	EVP_bf_cfb64, .-EVP_bf_cfb64
	.p2align 4
	.globl	EVP_bf_ofb
	.type	EVP_bf_ofb, @function
EVP_bf_ofb:
.LFB451:
	.cfi_startproc
	endbr64
	leaq	bf_ofb(%rip), %rax
	ret
	.cfi_endproc
.LFE451:
	.size	EVP_bf_ofb, .-EVP_bf_ofb
	.p2align 4
	.globl	EVP_bf_ecb
	.type	EVP_bf_ecb, @function
EVP_bf_ecb:
.LFB452:
	.cfi_startproc
	endbr64
	leaq	bf_ecb(%rip), %rax
	ret
	.cfi_endproc
.LFE452:
	.size	EVP_bf_ecb, .-EVP_bf_ecb
	.section	.data.rel.ro,"aw"
	.align 32
	.type	bf_ecb, @object
	.size	bf_ecb, 88
bf_ecb:
	.long	92
	.long	8
	.long	16
	.long	0
	.quad	9
	.quad	bf_init_key
	.quad	bf_ecb_cipher
	.quad	0
	.long	4168
	.zero	4
	.quad	EVP_CIPHER_set_asn1_iv
	.quad	EVP_CIPHER_get_asn1_iv
	.quad	0
	.quad	0
	.align 32
	.type	bf_ofb, @object
	.size	bf_ofb, 88
bf_ofb:
	.long	94
	.long	1
	.long	16
	.long	8
	.quad	12
	.quad	bf_init_key
	.quad	bf_ofb_cipher
	.quad	0
	.long	4168
	.zero	4
	.quad	EVP_CIPHER_set_asn1_iv
	.quad	EVP_CIPHER_get_asn1_iv
	.quad	0
	.quad	0
	.align 32
	.type	bf_cfb64, @object
	.size	bf_cfb64, 88
bf_cfb64:
	.long	93
	.long	1
	.long	16
	.long	8
	.quad	11
	.quad	bf_init_key
	.quad	bf_cfb64_cipher
	.quad	0
	.long	4168
	.zero	4
	.quad	EVP_CIPHER_set_asn1_iv
	.quad	EVP_CIPHER_get_asn1_iv
	.quad	0
	.quad	0
	.align 32
	.type	bf_cbc, @object
	.size	bf_cbc, 88
bf_cbc:
	.long	91
	.long	8
	.long	16
	.long	8
	.quad	10
	.quad	bf_init_key
	.quad	bf_cbc_cipher
	.quad	0
	.long	4168
	.zero	4
	.quad	EVP_CIPHER_set_asn1_iv
	.quad	EVP_CIPHER_get_asn1_iv
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
