	.file	"dsa_asn1.c"
	.text
	.p2align 4
	.type	dsa_cb, @function
dsa_cb:
.LFB451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	testl	%edi, %edi
	je	.L8
	movl	$1, %eax
	cmpl	$2, %edi
	je	.L9
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	call	DSA_new@PLT
	testq	%rax, %rax
	movq	%rax, (%rbx)
	setne	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	addl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	movq	(%rsi), %rdi
	call	DSA_free@PLT
	movq	$0, (%rbx)
	movl	$2, %eax
	jmp	.L1
	.cfi_endproc
.LFE451:
	.size	dsa_cb, .-dsa_cb
	.p2align 4
	.globl	d2i_DSA_SIG
	.type	d2i_DSA_SIG, @function
d2i_DSA_SIG:
.LFB445:
	.cfi_startproc
	endbr64
	leaq	DSA_SIG_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE445:
	.size	d2i_DSA_SIG, .-d2i_DSA_SIG
	.p2align 4
	.globl	i2d_DSA_SIG
	.type	i2d_DSA_SIG, @function
i2d_DSA_SIG:
.LFB446:
	.cfi_startproc
	endbr64
	leaq	DSA_SIG_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE446:
	.size	i2d_DSA_SIG, .-i2d_DSA_SIG
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/dsa/dsa_asn1.c"
	.text
	.p2align 4
	.globl	DSA_SIG_new
	.type	DSA_SIG_new, @function
DSA_SIG_new:
.LFB447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$26, %edx
	movl	$16, %edi
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L15
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	movl	$28, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$102, %esi
	movl	$10, %edi
	movq	%rax, -8(%rbp)
	call	ERR_put_error@PLT
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE447:
	.size	DSA_SIG_new, .-DSA_SIG_new
	.p2align 4
	.globl	DSA_SIG_free
	.type	DSA_SIG_free, @function
DSA_SIG_free:
.LFB448:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L16
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	BN_clear_free@PLT
	movq	8(%r12), %rdi
	call	BN_clear_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$38, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L16:
	ret
	.cfi_endproc
.LFE448:
	.size	DSA_SIG_free, .-DSA_SIG_free
	.p2align 4
	.globl	DSA_SIG_get0
	.type	DSA_SIG_get0, @function
DSA_SIG_get0:
.LFB449:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L22
	movq	(%rdi), %rax
	movq	%rax, (%rsi)
.L22:
	testq	%rdx, %rdx
	je	.L21
	movq	8(%rdi), %rax
	movq	%rax, (%rdx)
.L21:
	ret
	.cfi_endproc
.LFE449:
	.size	DSA_SIG_get0, .-DSA_SIG_get0
	.p2align 4
	.globl	DSA_SIG_set0
	.type	DSA_SIG_set0, @function
DSA_SIG_set0:
.LFB450:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L32
	testq	%rdx, %rdx
	je	.L32
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rdi
	movaps	%xmm0, -32(%rbp)
	call	BN_clear_free@PLT
	movq	8(%rbx), %rdi
	call	BN_clear_free@PLT
	movdqa	-32(%rbp), %xmm0
	movl	$1, %eax
	movups	%xmm0, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE450:
	.size	DSA_SIG_set0, .-DSA_SIG_set0
	.p2align 4
	.globl	d2i_DSAPrivateKey
	.type	d2i_DSAPrivateKey, @function
d2i_DSAPrivateKey:
.LFB452:
	.cfi_startproc
	endbr64
	leaq	DSAPrivateKey_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE452:
	.size	d2i_DSAPrivateKey, .-d2i_DSAPrivateKey
	.p2align 4
	.globl	i2d_DSAPrivateKey
	.type	i2d_DSAPrivateKey, @function
i2d_DSAPrivateKey:
.LFB453:
	.cfi_startproc
	endbr64
	leaq	DSAPrivateKey_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE453:
	.size	i2d_DSAPrivateKey, .-i2d_DSAPrivateKey
	.p2align 4
	.globl	d2i_DSAparams
	.type	d2i_DSAparams, @function
d2i_DSAparams:
.LFB454:
	.cfi_startproc
	endbr64
	leaq	DSAparams_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE454:
	.size	d2i_DSAparams, .-d2i_DSAparams
	.p2align 4
	.globl	i2d_DSAparams
	.type	i2d_DSAparams, @function
i2d_DSAparams:
.LFB455:
	.cfi_startproc
	endbr64
	leaq	DSAparams_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE455:
	.size	i2d_DSAparams, .-i2d_DSAparams
	.p2align 4
	.globl	d2i_DSAPublicKey
	.type	d2i_DSAPublicKey, @function
d2i_DSAPublicKey:
.LFB456:
	.cfi_startproc
	endbr64
	leaq	DSAPublicKey_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE456:
	.size	d2i_DSAPublicKey, .-d2i_DSAPublicKey
	.p2align 4
	.globl	i2d_DSAPublicKey
	.type	i2d_DSAPublicKey, @function
i2d_DSAPublicKey:
.LFB457:
	.cfi_startproc
	endbr64
	leaq	DSAPublicKey_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE457:
	.size	i2d_DSAPublicKey, .-i2d_DSAPublicKey
	.p2align 4
	.globl	DSAparams_dup
	.type	DSAparams_dup, @function
DSAparams_dup:
.LFB458:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	leaq	DSAparams_it(%rip), %rdi
	jmp	ASN1_item_dup@PLT
	.cfi_endproc
.LFE458:
	.size	DSAparams_dup, .-DSAparams_dup
	.p2align 4
	.globl	DSA_sign
	.type	DSA_sign, @function
DSA_sign:
.LFB459:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	%edx, %esi
	movq	%r9, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%r8, %rbx
	subq	$16, %rsp
	movq	%rcx, -24(%rbp)
	call	DSA_do_sign@PLT
	testq	%rax, %rax
	je	.L48
	leaq	-24(%rbp), %rsi
	leaq	DSA_SIG_it(%rip), %rdx
	movq	%rax, %rdi
	movq	%rax, %r12
	call	ASN1_item_i2d@PLT
	movq	(%r12), %rdi
	movl	%eax, (%rbx)
	call	BN_clear_free@PLT
	movq	8(%r12), %rdi
	call	BN_clear_free@PLT
	movq	%r12, %rdi
	movl	$38, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	movl	$0, (%rbx)
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE459:
	.size	DSA_sign, .-DSA_sign
	.p2align 4
	.globl	DSA_verify
	.type	DSA_verify, @function
DSA_verify:
.LFB460:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	movl	$26, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$56, %rsp
	movq	%rsi, -88(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rcx, -72(%rbp)
	movq	$0, -64(%rbp)
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L61
	movslq	%ebx, %r12
	leaq	-72(%rbp), %rsi
	leaq	-80(%rbp), %rdi
	movq	%rax, -80(%rbp)
	leaq	DSA_SIG_it(%rip), %rcx
	movq	%r12, %rdx
	call	ASN1_item_d2i@PLT
	testq	%rax, %rax
	je	.L62
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rsi
	leaq	DSA_SIG_it(%rip), %rdx
	call	ASN1_item_i2d@PLT
	cmpl	%eax, %ebx
	je	.L52
	movq	-64(%rbp), %rbx
	movslq	%eax, %r12
	movl	$-1, %r13d
.L53:
	movq	%r12, %rsi
	movl	$152, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%rbx, %rdi
	call	CRYPTO_clear_free@PLT
	movq	-80(%rbp), %r12
	testq	%r12, %r12
	je	.L49
	movq	(%r12), %rdi
	call	BN_clear_free@PLT
	movq	8(%r12), %rdi
	call	BN_clear_free@PLT
	movl	$38, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
.L49:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L63
	addq	$56, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	movq	-64(%rbp), %rbx
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$-1, %r13d
	movq	%rbx, %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L53
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rdi
	movq	%r15, %rcx
	movl	%r14d, %esi
	call	DSA_do_verify@PLT
	movq	-64(%rbp), %rbx
	movl	%eax, %r13d
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L62:
	movq	-64(%rbp), %rbx
	movq	$-1, %r12
	movl	$-1, %r13d
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L61:
	movl	$28, %r8d
	movl	$65, %edx
	movl	$102, %esi
	movl	$10, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r13d
	call	ERR_put_error@PLT
	jmp	.L49
.L63:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE460:
	.size	DSA_verify, .-DSA_verify
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"DSA"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	DSAPublicKey_it, @object
	.size	DSAPublicKey_it, 56
DSAPublicKey_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	DSAPublicKey_seq_tt
	.quad	4
	.quad	DSAPublicKey_aux
	.quad	104
	.quad	.LC1
	.section	.rodata.str1.1
.LC2:
	.string	"pub_key"
.LC3:
	.string	"p"
.LC4:
	.string	"q"
.LC5:
	.string	"g"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	DSAPublicKey_seq_tt, @object
	.size	DSAPublicKey_seq_tt, 160
DSAPublicKey_seq_tt:
	.quad	0
	.quad	0
	.quad	32
	.quad	.LC2
	.quad	BIGNUM_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC3
	.quad	BIGNUM_it
	.quad	0
	.quad	0
	.quad	16
	.quad	.LC4
	.quad	BIGNUM_it
	.quad	0
	.quad	0
	.quad	24
	.quad	.LC5
	.quad	BIGNUM_it
	.section	.data.rel.ro.local
	.align 32
	.type	DSAPublicKey_aux, @object
	.size	DSAPublicKey_aux, 40
DSAPublicKey_aux:
	.quad	0
	.long	0
	.long	0
	.long	0
	.zero	4
	.quad	dsa_cb
	.long	0
	.zero	4
	.align 32
	.type	DSAparams_it, @object
	.size	DSAparams_it, 56
DSAparams_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	DSAparams_seq_tt
	.quad	3
	.quad	DSAparams_aux
	.quad	104
	.quad	.LC1
	.section	.data.rel.ro
	.align 32
	.type	DSAparams_seq_tt, @object
	.size	DSAparams_seq_tt, 120
DSAparams_seq_tt:
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC3
	.quad	BIGNUM_it
	.quad	0
	.quad	0
	.quad	16
	.quad	.LC4
	.quad	BIGNUM_it
	.quad	0
	.quad	0
	.quad	24
	.quad	.LC5
	.quad	BIGNUM_it
	.section	.data.rel.ro.local
	.align 32
	.type	DSAparams_aux, @object
	.size	DSAparams_aux, 40
DSAparams_aux:
	.quad	0
	.long	0
	.long	0
	.long	0
	.zero	4
	.quad	dsa_cb
	.long	0
	.zero	4
	.align 32
	.type	DSAPrivateKey_it, @object
	.size	DSAPrivateKey_it, 56
DSAPrivateKey_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	DSAPrivateKey_seq_tt
	.quad	6
	.quad	DSAPrivateKey_aux
	.quad	104
	.quad	.LC1
	.section	.rodata.str1.1
.LC6:
	.string	"version"
.LC7:
	.string	"priv_key"
	.section	.data.rel.ro
	.align 32
	.type	DSAPrivateKey_seq_tt, @object
	.size	DSAPrivateKey_seq_tt, 240
DSAPrivateKey_seq_tt:
	.quad	4096
	.quad	0
	.quad	4
	.quad	.LC6
	.quad	INT32_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC3
	.quad	BIGNUM_it
	.quad	0
	.quad	0
	.quad	16
	.quad	.LC4
	.quad	BIGNUM_it
	.quad	0
	.quad	0
	.quad	24
	.quad	.LC5
	.quad	BIGNUM_it
	.quad	0
	.quad	0
	.quad	32
	.quad	.LC2
	.quad	BIGNUM_it
	.quad	0
	.quad	0
	.quad	40
	.quad	.LC7
	.quad	CBIGNUM_it
	.section	.data.rel.ro.local
	.align 32
	.type	DSAPrivateKey_aux, @object
	.size	DSAPrivateKey_aux, 40
DSAPrivateKey_aux:
	.quad	0
	.long	0
	.long	0
	.long	0
	.zero	4
	.quad	dsa_cb
	.long	0
	.zero	4
	.section	.rodata.str1.1
.LC8:
	.string	"DSA_SIG"
	.section	.data.rel.ro.local
	.align 32
	.type	DSA_SIG_it, @object
	.size	DSA_SIG_it, 56
DSA_SIG_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	DSA_SIG_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC8
	.section	.rodata.str1.1
.LC9:
	.string	"r"
.LC10:
	.string	"s"
	.section	.data.rel.ro
	.align 32
	.type	DSA_SIG_seq_tt, @object
	.size	DSA_SIG_seq_tt, 80
DSA_SIG_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC9
	.quad	CBIGNUM_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC10
	.quad	CBIGNUM_it
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
