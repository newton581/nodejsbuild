	.file	"ofb64ede.c"
	.text
	.p2align 4
	.globl	DES_ede3_ofb64_encrypt
	.type	DES_ede3_ofb64_encrypt, @function
DES_ede3_ofb64_encrypt:
.LFB54:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%rcx, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	24(%rbp), %rax
	movq	16(%rbp), %r14
	movq	(%r14), %r10
	movl	(%r14), %edi
	movq	%rax, -112(%rbp)
	movl	(%rax), %eax
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movl	4(%r14), %ecx
	movl	%edi, -72(%rbp)
	movq	%r10, -64(%rbp)
	movl	%ecx, -68(%rbp)
	testq	%rdx, %rdx
	je	.L2
	leaq	(%rbx,%rdx), %r15
	leaq	-72(%rbp), %rdx
	xorl	%r13d, %r13d
	movq	%r14, -120(%rbp)
	movq	%rdx, -104(%rbp)
	movl	%r13d, %r14d
	movq	%r15, %r13
	movq	%rsi, %r15
	.p2align 4,,10
	.p2align 3
.L7:
	testl	%eax, %eax
	je	.L19
.L3:
	movslq	%eax, %r11
	movzbl	(%rbx), %edx
	addq	$1, %rbx
	addq	$1, %r12
	xorb	-64(%rbp,%r11), %dl
	addl	$1, %eax
	movb	%dl, -1(%r12)
	andl	$7, %eax
	cmpq	%r13, %rbx
	jne	.L7
	movl	%r14d, %r13d
	movq	-120(%rbp), %r14
	testl	%r13d, %r13d
	jne	.L5
.L2:
	movq	-112(%rbp), %rsi
	movl	%eax, (%rsi)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L20
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movq	-104(%rbp), %rdi
	movq	%r9, %rcx
	movq	%r8, %rdx
	movq	%r15, %rsi
	movq	%r9, -96(%rbp)
	addq	$1, %rbx
	addq	$1, %r12
	addl	$1, %r14d
	movq	%r8, -88(%rbp)
	call	DES_encrypt3@PLT
	movq	-72(%rbp), %rax
	movl	-72(%rbp), %edi
	movl	-68(%rbp), %ecx
	movq	-88(%rbp), %r8
	movq	%rax, -64(%rbp)
	xorb	-1(%rbx), %al
	cmpq	%r13, %rbx
	movb	%al, -1(%r12)
	movq	-96(%rbp), %r9
	je	.L21
	movl	$1, %eax
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L21:
	movq	-120(%rbp), %r14
	movl	$1, %eax
.L5:
	movl	%edi, (%r14)
	movl	%ecx, 4(%r14)
	jmp	.L2
.L20:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE54:
	.size	DES_ede3_ofb64_encrypt, .-DES_ede3_ofb64_encrypt
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
