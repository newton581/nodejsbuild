	.file	"x_x509.c"
	.text
	.p2align 4
	.type	x509_cb, @function
x509_cb:
.LFB1326:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	(%rsi), %r12
	cmpl	$3, %edi
	je	.L2
	cmpl	$4, %edi
	je	.L3
	movl	$1, %eax
	leaq	200(%r12), %r13
	cmpl	$1, %edi
	je	.L5
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	leaq	200(%r12), %r13
	movq	%r12, %rsi
	movl	$3, %edi
	movq	%r13, %rdx
	call	CRYPTO_free_ex_data@PLT
	movq	328(%r12), %rdi
	call	X509_CERT_AUX_free@PLT
	movq	240(%r12), %rdi
	call	ASN1_OCTET_STRING_free@PLT
	movq	248(%r12), %rdi
	call	AUTHORITY_KEYID_free@PLT
	movq	264(%r12), %rdi
	call	CRL_DIST_POINTS_free@PLT
	movq	256(%r12), %rdi
	call	policy_cache_free@PLT
	movq	272(%r12), %rdi
	call	GENERAL_NAMES_free@PLT
	movq	280(%r12), %rdi
	call	NAME_CONSTRAINTS_free@PLT
	movq	288(%r12), %rdi
	movq	IPAddressFamily_free@GOTPCREL(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	296(%r12), %rdi
	call	ASIdentifiers_free@PLT
.L5:
	pxor	%xmm0, %xmm0
	movq	%r13, %rdx
	movq	%r12, %rsi
	movl	$3, %edi
	movups	%xmm0, 224(%r12)
	pcmpeqd	%xmm0, %xmm0
	movl	$0, 344(%r12)
	movq	$0, 328(%r12)
	movups	%xmm0, 208(%r12)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 240(%r12)
	movups	%xmm0, 256(%r12)
	movups	%xmm0, 272(%r12)
	movups	%xmm0, 288(%r12)
	call	CRYPTO_new_ex_data@PLT
	popq	%r12
	popq	%r13
	testl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	setne	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	leaq	200(%r12), %rdx
	movq	%r12, %rsi
	movl	$3, %edi
	call	CRYPTO_free_ex_data@PLT
	movq	328(%r12), %rdi
	call	X509_CERT_AUX_free@PLT
	movq	240(%r12), %rdi
	call	ASN1_OCTET_STRING_free@PLT
	movq	248(%r12), %rdi
	call	AUTHORITY_KEYID_free@PLT
	movq	264(%r12), %rdi
	call	CRL_DIST_POINTS_free@PLT
	movq	256(%r12), %rdi
	call	policy_cache_free@PLT
	movq	272(%r12), %rdi
	call	GENERAL_NAMES_free@PLT
	movq	280(%r12), %rdi
	call	NAME_CONSTRAINTS_free@PLT
	movq	288(%r12), %rdi
	movq	IPAddressFamily_free@GOTPCREL(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	296(%r12), %rdi
	call	ASIdentifiers_free@PLT
	popq	%r12
	movl	$1, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1326:
	.size	x509_cb, .-x509_cb
	.p2align 4
	.globl	d2i_X509_CINF
	.type	d2i_X509_CINF, @function
d2i_X509_CINF:
.LFB1322:
	.cfi_startproc
	endbr64
	leaq	X509_CINF_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1322:
	.size	d2i_X509_CINF, .-d2i_X509_CINF
	.p2align 4
	.globl	i2d_X509_CINF
	.type	i2d_X509_CINF, @function
i2d_X509_CINF:
.LFB1323:
	.cfi_startproc
	endbr64
	leaq	X509_CINF_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1323:
	.size	i2d_X509_CINF, .-i2d_X509_CINF
	.p2align 4
	.globl	X509_CINF_new
	.type	X509_CINF_new, @function
X509_CINF_new:
.LFB1324:
	.cfi_startproc
	endbr64
	leaq	X509_CINF_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1324:
	.size	X509_CINF_new, .-X509_CINF_new
	.p2align 4
	.globl	X509_CINF_free
	.type	X509_CINF_free, @function
X509_CINF_free:
.LFB1325:
	.cfi_startproc
	endbr64
	leaq	X509_CINF_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1325:
	.size	X509_CINF_free, .-X509_CINF_free
	.p2align 4
	.globl	d2i_X509
	.type	d2i_X509, @function
d2i_X509:
.LFB1327:
	.cfi_startproc
	endbr64
	leaq	X509_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1327:
	.size	d2i_X509, .-d2i_X509
	.p2align 4
	.globl	i2d_X509
	.type	i2d_X509, @function
i2d_X509:
.LFB1328:
	.cfi_startproc
	endbr64
	leaq	X509_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1328:
	.size	i2d_X509, .-i2d_X509
	.p2align 4
	.globl	X509_new
	.type	X509_new, @function
X509_new:
.LFB1329:
	.cfi_startproc
	endbr64
	leaq	X509_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1329:
	.size	X509_new, .-X509_new
	.p2align 4
	.globl	X509_free
	.type	X509_free, @function
X509_free:
.LFB1330:
	.cfi_startproc
	endbr64
	leaq	X509_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1330:
	.size	X509_free, .-X509_free
	.p2align 4
	.globl	X509_dup
	.type	X509_dup, @function
X509_dup:
.LFB1331:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	leaq	X509_it(%rip), %rdi
	jmp	ASN1_item_dup@PLT
	.cfi_endproc
.LFE1331:
	.size	X509_dup, .-X509_dup
	.p2align 4
	.globl	X509_set_ex_data
	.type	X509_set_ex_data, @function
X509_set_ex_data:
.LFB1332:
	.cfi_startproc
	endbr64
	addq	$200, %rdi
	jmp	CRYPTO_set_ex_data@PLT
	.cfi_endproc
.LFE1332:
	.size	X509_set_ex_data, .-X509_set_ex_data
	.p2align 4
	.globl	X509_get_ex_data
	.type	X509_get_ex_data, @function
X509_get_ex_data:
.LFB1333:
	.cfi_startproc
	endbr64
	addq	$200, %rdi
	jmp	CRYPTO_get_ex_data@PLT
	.cfi_endproc
.LFE1333:
	.size	X509_get_ex_data, .-X509_get_ex_data
	.p2align 4
	.globl	d2i_X509_AUX
	.type	d2i_X509_AUX, @function
d2i_X509_AUX:
.LFB1334:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movl	$1, -68(%rbp)
	movq	%rax, -64(%rbp)
	testq	%rdi, %rdi
	je	.L21
	xorl	%eax, %eax
	cmpq	$0, (%rdi)
	sete	%al
	movl	%eax, -68(%rbp)
.L21:
	leaq	-64(%rbp), %r14
	leaq	X509_it(%rip), %rcx
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	ASN1_item_d2i@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L35
	movq	-64(%rbp), %rax
	movq	%rax, %rdx
	subq	(%r12), %rdx
	subq	%rdx, %rbx
	movq	%rbx, %rdx
	testq	%rbx, %rbx
	jg	.L36
.L24:
	movq	%rax, (%r12)
.L20:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L37
	addq	$40, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	leaq	328(%r15), %rdi
	movq	%r14, %rsi
	call	d2i_X509_CERT_AUX@PLT
	testq	%rax, %rax
	je	.L25
	movq	-64(%rbp), %rax
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L25:
	movl	-68(%rbp), %eax
	testl	%eax, %eax
	je	.L35
	leaq	X509_it(%rip), %rsi
	movq	%r15, %rdi
	call	ASN1_item_free@PLT
	testq	%r13, %r13
	je	.L35
	movq	$0, 0(%r13)
	.p2align 4,,10
	.p2align 3
.L35:
	xorl	%r15d, %r15d
	jmp	.L20
.L37:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1334:
	.size	d2i_X509_AUX, .-d2i_X509_AUX
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/x509/x_x509.c"
	.text
	.p2align 4
	.globl	i2d_X509_AUX
	.type	i2d_X509_AUX, @function
i2d_X509_AUX:
.LFB1336:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	sete	%r14b
	testq	%rsi, %rsi
	je	.L51
	movq	(%rsi), %rbx
	testq	%rbx, %rbx
	je	.L65
.L39:
	leaq	X509_it(%rip), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	ASN1_item_i2d@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jle	.L38
	testb	%r14b, %r14b
	jne	.L38
	movq	328(%r15), %rdi
	movq	%r13, %rsi
	call	i2d_X509_CERT_AUX@PLT
	addl	%eax, %r12d
	testl	%eax, %eax
	js	.L66
.L38:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L67
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	leaq	X509_it(%rip), %rdx
	xorl	%esi, %esi
	call	ASN1_item_i2d@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jle	.L38
	testb	%r14b, %r14b
	jne	.L43
	movq	328(%r15), %rdi
	xorl	%esi, %esi
	call	i2d_X509_CERT_AUX@PLT
	testl	%eax, %eax
	js	.L53
	addl	%r12d, %eax
	movl	$214, %edx
	leaq	.LC0(%rip), %rsi
	movslq	%eax, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, -64(%rbp)
	movq	%rax, %rbx
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	je	.L44
	leaq	-64(%rbp), %r14
	leaq	X509_it(%rip), %rdx
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	ASN1_item_i2d@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jle	.L47
	movq	328(%r15), %rdi
	movq	%r14, %rsi
	call	i2d_X509_CERT_AUX@PLT
	addl	%eax, %r12d
	testl	%eax, %eax
	jns	.L38
	movq	%rbx, -64(%rbp)
	movl	%eax, %r12d
.L47:
	movq	0(%r13), %rdi
	movl	$223, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 0(%r13)
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L66:
	movl	%eax, %r12d
	testq	%rbx, %rbx
	je	.L38
	movq	%rbx, 0(%r13)
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L51:
	xorl	%ebx, %ebx
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L43:
	testl	%eax, %eax
	jle	.L38
	movslq	%eax, %rdi
	movl	$214, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, -64(%rbp)
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	je	.L44
	leaq	-64(%rbp), %rsi
	leaq	X509_it(%rip), %rdx
	movq	%r15, %rdi
	call	ASN1_item_i2d@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jg	.L38
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L53:
	movl	%eax, %r12d
	jmp	.L38
.L44:
	movl	$216, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$151, %esi
	movl	$11, %edi
	orl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L38
.L67:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1336:
	.size	i2d_X509_AUX, .-i2d_X509_AUX
	.p2align 4
	.globl	i2d_re_X509_tbs
	.type	i2d_re_X509_tbs, @function
i2d_re_X509_tbs:
.LFB1337:
	.cfi_startproc
	endbr64
	movl	$1, 128(%rdi)
	leaq	X509_CINF_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1337:
	.size	i2d_re_X509_tbs, .-i2d_re_X509_tbs
	.p2align 4
	.globl	X509_get0_signature
	.type	X509_get0_signature, @function
X509_get0_signature:
.LFB1338:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L70
	leaq	152(%rdx), %rax
	movq	%rax, (%rdi)
.L70:
	testq	%rsi, %rsi
	je	.L69
	addq	$136, %rdx
	movq	%rdx, (%rsi)
.L69:
	ret
	.cfi_endproc
.LFE1338:
	.size	X509_get0_signature, .-X509_get0_signature
	.p2align 4
	.globl	X509_get_signature_nid
	.type	X509_get_signature_nid, @function
X509_get_signature_nid:
.LFB1339:
	.cfi_startproc
	endbr64
	movq	136(%rdi), %rdi
	jmp	OBJ_obj2nid@PLT
	.cfi_endproc
.LFE1339:
	.size	X509_get_signature_nid, .-X509_get_signature_nid
	.globl	X509_it
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"X509"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	X509_it, @object
	.size	X509_it, 56
X509_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	X509_seq_tt
	.quad	3
	.quad	X509_aux
	.quad	352
	.quad	.LC1
	.section	.rodata.str1.1
.LC2:
	.string	"cert_info"
.LC3:
	.string	"sig_alg"
.LC4:
	.string	"signature"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	X509_seq_tt, @object
	.size	X509_seq_tt, 120
X509_seq_tt:
	.quad	4096
	.quad	0
	.quad	0
	.quad	.LC2
	.quad	X509_CINF_it
	.quad	4096
	.quad	0
	.quad	136
	.quad	.LC3
	.quad	X509_ALGOR_it
	.quad	4096
	.quad	0
	.quad	152
	.quad	.LC4
	.quad	ASN1_BIT_STRING_it
	.section	.data.rel.ro.local
	.align 32
	.type	X509_aux, @object
	.size	X509_aux, 40
X509_aux:
	.quad	0
	.long	1
	.long	192
	.long	336
	.zero	4
	.quad	x509_cb
	.long	0
	.zero	4
	.globl	X509_CINF_it
	.section	.rodata.str1.1
.LC5:
	.string	"X509_CINF"
	.section	.data.rel.ro.local
	.align 32
	.type	X509_CINF_it, @object
	.size	X509_CINF_it, 56
X509_CINF_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	X509_CINF_seq_tt
	.quad	10
	.quad	X509_CINF_aux
	.quad	136
	.quad	.LC5
	.section	.rodata.str1.1
.LC6:
	.string	"version"
.LC7:
	.string	"serialNumber"
.LC8:
	.string	"issuer"
.LC9:
	.string	"validity"
.LC10:
	.string	"subject"
.LC11:
	.string	"key"
.LC12:
	.string	"issuerUID"
.LC13:
	.string	"subjectUID"
.LC14:
	.string	"extensions"
	.section	.data.rel.ro
	.align 32
	.type	X509_CINF_seq_tt, @object
	.size	X509_CINF_seq_tt, 400
X509_CINF_seq_tt:
	.quad	145
	.quad	0
	.quad	0
	.quad	.LC6
	.quad	ASN1_INTEGER_it
	.quad	4096
	.quad	0
	.quad	8
	.quad	.LC7
	.quad	ASN1_INTEGER_it
	.quad	4096
	.quad	0
	.quad	32
	.quad	.LC4
	.quad	X509_ALGOR_it
	.quad	0
	.quad	0
	.quad	48
	.quad	.LC8
	.quad	X509_NAME_it
	.quad	4096
	.quad	0
	.quad	56
	.quad	.LC9
	.quad	X509_VAL_it
	.quad	0
	.quad	0
	.quad	72
	.quad	.LC10
	.quad	X509_NAME_it
	.quad	0
	.quad	0
	.quad	80
	.quad	.LC11
	.quad	X509_PUBKEY_it
	.quad	137
	.quad	1
	.quad	88
	.quad	.LC12
	.quad	ASN1_BIT_STRING_it
	.quad	137
	.quad	2
	.quad	96
	.quad	.LC13
	.quad	ASN1_BIT_STRING_it
	.quad	149
	.quad	3
	.quad	104
	.quad	.LC14
	.quad	X509_EXTENSION_it
	.section	.rodata
	.align 32
	.type	X509_CINF_aux, @object
	.size	X509_CINF_aux, 40
X509_CINF_aux:
	.quad	0
	.long	2
	.long	0
	.long	0
	.zero	4
	.quad	0
	.long	112
	.zero	4
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
