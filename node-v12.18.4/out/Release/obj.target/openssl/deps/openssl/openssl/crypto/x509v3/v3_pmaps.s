	.file	"v3_pmaps.c"
	.text
	.p2align 4
	.type	i2v_POLICY_MAPPINGS, @function
i2v_POLICY_MAPPINGS:
.LFB1322:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-224(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -232(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L3:
	movl	%r12d, %esi
	movq	%r13, %rdi
	leaq	-144(%rbp), %r15
	addl	$1, %r12d
	call	OPENSSL_sk_value@PLT
	movl	$80, %esi
	movq	%r14, %rdi
	movq	(%rax), %rdx
	movq	%rax, %rbx
	call	i2t_ASN1_OBJECT@PLT
	movq	8(%rbx), %rdx
	movl	$80, %esi
	movq	%r15, %rdi
	call	i2t_ASN1_OBJECT@PLT
	leaq	-232(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	X509V3_add_value@PLT
.L2:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jl	.L3
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	movq	-232(%rbp), %rax
	jne	.L7
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L7:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1322:
	.size	i2v_POLICY_MAPPINGS, .-i2v_POLICY_MAPPINGS
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/x509v3/v3_pmaps.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	",value:"
.LC2:
	.string	",name:"
.LC3:
	.string	"section:"
	.text
	.p2align 4
	.type	v2i_POLICY_MAPPINGS, @function
v2i_POLICY_MAPPINGS:
.LFB1323:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -56(%rbp)
	call	OPENSSL_sk_num@PLT
	xorl	%edi, %edi
	movl	%eax, %esi
	movl	%eax, -60(%rbp)
	movl	%eax, %r15d
	call	OPENSSL_sk_new_reserve@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L9
	xorl	%ebx, %ebx
	testl	%r15d, %r15d
	jg	.L10
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L12:
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L14
	xorl	%esi, %esi
	call	OBJ_txt2obj@PLT
	movq	16(%r14), %rdi
	xorl	%esi, %esi
	movq	%rax, %r12
	call	OBJ_txt2obj@PLT
	movq	%rax, %r15
	testq	%r12, %r12
	je	.L19
	testq	%rax, %rax
	je	.L19
	leaq	POLICY_MAPPING_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L26
	movq	%r15, %xmm1
	movq	%r12, %xmm0
	movq	%r13, %rdi
	addl	$1, %ebx
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rax)
	call	OPENSSL_sk_push@PLT
	cmpl	-60(%rbp), %ebx
	je	.L8
.L10:
	movq	-56(%rbp), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	cmpq	$0, 16(%rax)
	movq	%rax, %r14
	jne	.L12
.L14:
	movl	$110, %edx
	movl	$145, %esi
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	movl	$83, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$34, %edi
	call	ERR_put_error@PLT
	subq	$8, %rsp
	movq	8(%r14), %r8
	xorl	%eax, %eax
	pushq	16(%r14)
	movq	(%r14), %rdx
	leaq	.LC2(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC1(%rip), %r9
	movl	$6, %edi
	call	ERR_add_error_data@PLT
	popq	%rcx
	popq	%rsi
.L13:
	movq	%r12, %rdi
	call	ASN1_OBJECT_free@PLT
	movq	%r15, %rdi
	call	ASN1_OBJECT_free@PLT
	movq	%r13, %rdi
	leaq	POLICY_MAPPING_free(%rip), %rsi
	xorl	%r13d, %r13d
	call	OPENSSL_sk_pop_free@PLT
.L8:
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movl	$91, %r8d
	movl	$110, %edx
	movl	$145, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	subq	$8, %rsp
	movq	8(%r14), %r8
	xorl	%eax, %eax
	pushq	16(%r14)
	movq	(%r14), %rdx
	leaq	.LC1(%rip), %r9
	leaq	.LC2(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	movl	$6, %edi
	call	ERR_add_error_data@PLT
	popq	%rax
	popq	%rdx
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L26:
	movl	$98, %r8d
	movl	$65, %edx
	movl	$145, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L13
.L9:
	movl	$76, %r8d
	movl	$65, %edx
	movl	$145, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L8
	.cfi_endproc
.LFE1323:
	.size	v2i_POLICY_MAPPINGS, .-v2i_POLICY_MAPPINGS
	.p2align 4
	.globl	POLICY_MAPPING_free
	.type	POLICY_MAPPING_free, @function
POLICY_MAPPING_free:
.LFB1321:
	.cfi_startproc
	endbr64
	leaq	POLICY_MAPPING_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1321:
	.size	POLICY_MAPPING_free, .-POLICY_MAPPING_free
	.p2align 4
	.globl	POLICY_MAPPING_new
	.type	POLICY_MAPPING_new, @function
POLICY_MAPPING_new:
.LFB1320:
	.cfi_startproc
	endbr64
	leaq	POLICY_MAPPING_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1320:
	.size	POLICY_MAPPING_new, .-POLICY_MAPPING_new
	.globl	POLICY_MAPPINGS_it
	.section	.rodata.str1.1
.LC4:
	.string	"POLICY_MAPPINGS"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	POLICY_MAPPINGS_it, @object
	.size	POLICY_MAPPINGS_it, 56
POLICY_MAPPINGS_it:
	.byte	0
	.zero	7
	.quad	-1
	.quad	POLICY_MAPPINGS_item_tt
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC4
	.align 32
	.type	POLICY_MAPPINGS_item_tt, @object
	.size	POLICY_MAPPINGS_item_tt, 40
POLICY_MAPPINGS_item_tt:
	.quad	4
	.quad	0
	.quad	0
	.quad	.LC4
	.quad	POLICY_MAPPING_it
	.globl	POLICY_MAPPING_it
	.section	.rodata.str1.1
.LC5:
	.string	"POLICY_MAPPING"
	.section	.data.rel.ro.local
	.align 32
	.type	POLICY_MAPPING_it, @object
	.size	POLICY_MAPPING_it, 56
POLICY_MAPPING_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	POLICY_MAPPING_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC5
	.section	.rodata.str1.1
.LC6:
	.string	"issuerDomainPolicy"
.LC7:
	.string	"subjectDomainPolicy"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	POLICY_MAPPING_seq_tt, @object
	.size	POLICY_MAPPING_seq_tt, 80
POLICY_MAPPING_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC6
	.quad	ASN1_OBJECT_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC7
	.quad	ASN1_OBJECT_it
	.globl	v3_policy_mappings
	.section	.data.rel.ro.local
	.align 32
	.type	v3_policy_mappings, @object
	.size	v3_policy_mappings, 104
v3_policy_mappings:
	.long	747
	.long	0
	.quad	POLICY_MAPPINGS_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	i2v_POLICY_MAPPINGS
	.quad	v2i_POLICY_MAPPINGS
	.quad	0
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
