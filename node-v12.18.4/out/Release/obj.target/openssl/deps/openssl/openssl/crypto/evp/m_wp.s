	.file	"m_wp.c"
	.text
	.p2align 4
	.type	final, @function
final:
.LFB807:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	call	EVP_MD_CTX_md_data@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	WHIRLPOOL_Final@PLT
	.cfi_endproc
.LFE807:
	.size	final, .-final
	.p2align 4
	.type	update, @function
update:
.LFB806:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	call	EVP_MD_CTX_md_data@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	popq	%r12
	movq	%rax, %rdi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	WHIRLPOOL_Update@PLT
	.cfi_endproc
.LFE806:
	.size	update, .-update
	.p2align 4
	.type	init, @function
init:
.LFB805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	EVP_MD_CTX_md_data@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	%rax, %rdi
	jmp	WHIRLPOOL_Init@PLT
	.cfi_endproc
.LFE805:
	.size	init, .-init
	.p2align 4
	.globl	EVP_whirlpool
	.type	EVP_whirlpool, @function
EVP_whirlpool:
.LFB808:
	.cfi_startproc
	endbr64
	leaq	whirlpool_md(%rip), %rax
	ret
	.cfi_endproc
.LFE808:
	.size	EVP_whirlpool, .-EVP_whirlpool
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	whirlpool_md, @object
	.size	whirlpool_md, 80
whirlpool_md:
	.long	804
	.long	0
	.long	64
	.zero	4
	.quad	0
	.quad	init
	.quad	update
	.quad	final
	.quad	0
	.quad	0
	.long	64
	.long	176
	.zero	8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
