	.file	"ecp_mont.c"
	.text
	.p2align 4
	.globl	ec_GFp_mont_group_init
	.type	ec_GFp_mont_group_init, @function
ec_GFp_mont_group_init:
.LFB378:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	ec_GFp_simple_group_init@PLT
	pxor	%xmm0, %xmm0
	movups	%xmm0, 120(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE378:
	.size	ec_GFp_mont_group_init, .-ec_GFp_mont_group_init
	.p2align 4
	.globl	ec_GFp_mont_group_finish
	.type	ec_GFp_mont_group_finish, @function
ec_GFp_mont_group_finish:
.LFB379:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	120(%rdi), %rdi
	call	BN_MONT_CTX_free@PLT
	movq	128(%r12), %rdi
	movq	$0, 120(%r12)
	call	BN_free@PLT
	movq	%r12, %rdi
	movq	$0, 128(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	ec_GFp_simple_group_finish@PLT
	.cfi_endproc
.LFE379:
	.size	ec_GFp_mont_group_finish, .-ec_GFp_mont_group_finish
	.p2align 4
	.globl	ec_GFp_mont_group_clear_finish
	.type	ec_GFp_mont_group_clear_finish, @function
ec_GFp_mont_group_clear_finish:
.LFB380:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	120(%rdi), %rdi
	call	BN_MONT_CTX_free@PLT
	movq	128(%r12), %rdi
	movq	$0, 120(%r12)
	call	BN_clear_free@PLT
	movq	%r12, %rdi
	movq	$0, 128(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	ec_GFp_simple_group_clear_finish@PLT
	.cfi_endproc
.LFE380:
	.size	ec_GFp_mont_group_clear_finish, .-ec_GFp_mont_group_clear_finish
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ec/ecp_mont.c"
	.text
	.p2align 4
	.globl	ec_GFp_mont_group_set_curve
	.type	ec_GFp_mont_group_set_curve, @function
ec_GFp_mont_group_set_curve:
.LFB382:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	120(%rdi), %rdi
	movq	%rdx, -56(%rbp)
	movq	%rcx, -64(%rbp)
	call	BN_MONT_CTX_free@PLT
	movq	$0, 120(%rbx)
	movq	128(%rbx), %rdi
	call	BN_free@PLT
	movq	$0, 128(%rbx)
	testq	%r12, %r12
	je	.L24
	call	BN_MONT_CTX_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L15
.L26:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	BN_MONT_CTX_set@PLT
	testl	%eax, %eax
	je	.L25
	call	BN_new@PLT
	movq	%rax, %r8
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L11
	movq	%r8, -72(%rbp)
	call	BN_value_one@PLT
	movq	-72(%rbp), %r8
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%rax, %rsi
	movq	%r8, %rdi
	call	BN_to_montgomery@PLT
	movq	-72(%rbp), %r8
	testl	%eax, %eax
	je	.L11
	movq	%r8, %xmm1
	movq	%r13, %xmm0
	movq	%r12, %r8
	movq	%r14, %rsi
	punpcklqdq	%xmm1, %xmm0
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %rdx
	movq	%rbx, %rdi
	movups	%xmm0, 120(%rbx)
	xorl	%r13d, %r13d
	call	ec_GFp_simple_group_set_curve@PLT
	xorl	%r8d, %r8d
	testl	%eax, %eax
	jne	.L11
	movq	120(%rbx), %rdi
	movl	%eax, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	BN_MONT_CTX_free@PLT
	movq	$0, 120(%rbx)
	movq	128(%rbx), %rdi
	call	BN_free@PLT
	movq	-56(%rbp), %r8
	movl	-64(%rbp), %eax
	movq	$0, 128(%rbx)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L25:
	movl	$158, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$3, %edx
	movl	%eax, -56(%rbp)
	movl	$189, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	movl	-56(%rbp), %eax
	xorl	%r8d, %r8d
.L11:
	movq	%r8, %rdi
	movl	%eax, -56(%rbp)
	call	BN_free@PLT
	movq	%r15, %rdi
	call	BN_CTX_free@PLT
	movq	%r13, %rdi
	call	BN_MONT_CTX_free@PLT
	movl	-56(%rbp), %eax
.L8:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	call	BN_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L14
	movq	%rax, %r15
	call	BN_MONT_CTX_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L26
.L15:
	xorl	%r8d, %r8d
	xorl	%eax, %eax
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L14:
	xorl	%eax, %eax
	jmp	.L8
	.cfi_endproc
.LFE382:
	.size	ec_GFp_mont_group_set_curve, .-ec_GFp_mont_group_set_curve
	.p2align 4
	.globl	ec_GFp_mont_field_inv
	.type	ec_GFp_mont_field_inv, @function
ec_GFp_mont_field_inv:
.LFB385:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	$0, 120(%rdi)
	movq	%rdx, -56(%rbp)
	je	.L31
	movq	%rdi, %rbx
	movq	%rsi, %r13
	movq	%rcx, %r12
	xorl	%r15d, %r15d
	testq	%rcx, %rcx
	je	.L52
.L30:
	movq	%r12, %rdi
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L51
	movl	$2, %esi
	movq	%rax, %rdi
	call	BN_set_word@PLT
	testl	%eax, %eax
	jne	.L53
.L51:
	xorl	%r13d, %r13d
.L33:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	movq	%r15, %rdi
	call	BN_CTX_free@PLT
.L27:
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movq	64(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r14, %rdi
	call	BN_sub@PLT
	testl	%eax, %eax
	je	.L51
	movq	64(%rbx), %rcx
	movq	120(%rbx), %r9
	movq	%r12, %r8
	movq	%r14, %rdx
	movq	-56(%rbp), %rsi
	movq	%r13, %rdi
	call	BN_mod_exp_mont@PLT
	testl	%eax, %eax
	je	.L51
	movq	%r13, %rdi
	movl	$1, %r13d
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L33
	movl	$246, %r8d
	movl	$165, %edx
	movl	$297, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L52:
	call	BN_CTX_secure_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L31
	movq	%rax, %r15
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L31:
	xorl	%r13d, %r13d
	jmp	.L27
	.cfi_endproc
.LFE385:
	.size	ec_GFp_mont_field_inv, .-ec_GFp_mont_field_inv
	.p2align 4
	.globl	ec_GFp_mont_group_copy
	.type	ec_GFp_mont_group_copy, @function
ec_GFp_mont_group_copy:
.LFB381:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	120(%rdi), %rdi
	call	BN_MONT_CTX_free@PLT
	movq	$0, 120(%rbx)
	movq	128(%rbx), %rdi
	call	BN_clear_free@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	$0, 128(%rbx)
	call	ec_GFp_simple_group_copy@PLT
	testl	%eax, %eax
	je	.L70
	cmpq	$0, 120(%r12)
	je	.L61
	call	BN_MONT_CTX_new@PLT
	movq	%rax, 120(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L70
	movq	120(%r12), %rsi
	call	BN_MONT_CTX_copy@PLT
	testq	%rax, %rax
	je	.L62
.L61:
	movq	128(%r12), %rdi
	testq	%rdi, %rdi
	je	.L59
	call	BN_dup@PLT
	movq	%rax, 128(%rbx)
	testq	%rax, %rax
	je	.L62
.L59:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	movq	120(%rbx), %rdi
	call	BN_MONT_CTX_free@PLT
	movq	$0, 120(%rbx)
.L70:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE381:
	.size	ec_GFp_mont_group_copy, .-ec_GFp_mont_group_copy
	.p2align 4
	.globl	ec_GFp_mont_field_encode
	.type	ec_GFp_mont_field_encode, @function
ec_GFp_mont_field_encode:
.LFB386:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	movq	120(%r8), %rdx
	testq	%rdx, %rdx
	je	.L75
	jmp	BN_to_montgomery@PLT
	.p2align 4,,10
	.p2align 3
.L75:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$111, %edx
	movl	$134, %esi
	movl	$16, %edi
	movl	$262, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE386:
	.size	ec_GFp_mont_field_encode, .-ec_GFp_mont_field_encode
	.p2align 4
	.globl	ec_GFp_mont_field_mul
	.type	ec_GFp_mont_field_mul, @function
ec_GFp_mont_field_mul:
.LFB383:
	.cfi_startproc
	endbr64
	movq	%rdi, %r9
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	movq	120(%r9), %rcx
	testq	%rcx, %rcx
	je	.L82
	jmp	BN_mod_mul_montgomery@PLT
	.p2align 4,,10
	.p2align 3
.L82:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$111, %edx
	movl	$131, %esi
	movl	$16, %edi
	movl	$192, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE383:
	.size	ec_GFp_mont_field_mul, .-ec_GFp_mont_field_mul
	.p2align 4
	.globl	ec_GFp_mont_field_sqr
	.type	ec_GFp_mont_field_sqr, @function
ec_GFp_mont_field_sqr:
.LFB384:
	.cfi_startproc
	endbr64
	movq	%rcx, %r8
	movq	120(%rdi), %rcx
	testq	%rcx, %rcx
	je	.L89
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	jmp	BN_mod_mul_montgomery@PLT
	.p2align 4,,10
	.p2align 3
.L89:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$111, %edx
	movl	$132, %esi
	movl	$16, %edi
	movl	$203, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE384:
	.size	ec_GFp_mont_field_sqr, .-ec_GFp_mont_field_sqr
	.p2align 4
	.globl	ec_GFp_mont_field_decode
	.type	ec_GFp_mont_field_decode, @function
ec_GFp_mont_field_decode:
.LFB387:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	movq	120(%r8), %rdx
	testq	%rdx, %rdx
	je	.L96
	jmp	BN_from_montgomery@PLT
	.p2align 4,,10
	.p2align 3
.L96:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$111, %edx
	movl	$133, %esi
	movl	$16, %edi
	movl	$273, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE387:
	.size	ec_GFp_mont_field_decode, .-ec_GFp_mont_field_decode
	.p2align 4
	.globl	ec_GFp_mont_field_set_to_one
	.type	ec_GFp_mont_field_set_to_one, @function
ec_GFp_mont_field_set_to_one:
.LFB388:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	128(%r8), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rsi, %rsi
	je	.L103
	call	BN_copy@PLT
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	movl	$284, %r8d
	movl	$111, %edx
	movl	$209, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE388:
	.size	ec_GFp_mont_field_set_to_one, .-ec_GFp_mont_field_set_to_one
	.p2align 4
	.globl	EC_GFp_mont_method
	.type	EC_GFp_mont_method, @function
EC_GFp_mont_method:
.LFB377:
	.cfi_startproc
	endbr64
	leaq	ret.9690(%rip), %rax
	ret
	.cfi_endproc
.LFE377:
	.size	EC_GFp_mont_method, .-EC_GFp_mont_method
	.section	.data.rel.ro,"aw"
	.align 32
	.type	ret.9690, @object
	.size	ret.9690, 432
ret.9690:
	.long	1
	.long	406
	.quad	ec_GFp_mont_group_init
	.quad	ec_GFp_mont_group_finish
	.quad	ec_GFp_mont_group_clear_finish
	.quad	ec_GFp_mont_group_copy
	.quad	ec_GFp_mont_group_set_curve
	.quad	ec_GFp_simple_group_get_curve
	.quad	ec_GFp_simple_group_get_degree
	.quad	ec_group_simple_order_bits
	.quad	ec_GFp_simple_group_check_discriminant
	.quad	ec_GFp_simple_point_init
	.quad	ec_GFp_simple_point_finish
	.quad	ec_GFp_simple_point_clear_finish
	.quad	ec_GFp_simple_point_copy
	.quad	ec_GFp_simple_point_set_to_infinity
	.quad	ec_GFp_simple_set_Jprojective_coordinates_GFp
	.quad	ec_GFp_simple_get_Jprojective_coordinates_GFp
	.quad	ec_GFp_simple_point_set_affine_coordinates
	.quad	ec_GFp_simple_point_get_affine_coordinates
	.quad	0
	.quad	0
	.quad	0
	.quad	ec_GFp_simple_add
	.quad	ec_GFp_simple_dbl
	.quad	ec_GFp_simple_invert
	.quad	ec_GFp_simple_is_at_infinity
	.quad	ec_GFp_simple_is_on_curve
	.quad	ec_GFp_simple_cmp
	.quad	ec_GFp_simple_make_affine
	.quad	ec_GFp_simple_points_make_affine
	.quad	0
	.quad	0
	.quad	0
	.quad	ec_GFp_mont_field_mul
	.quad	ec_GFp_mont_field_sqr
	.quad	0
	.quad	ec_GFp_mont_field_inv
	.quad	ec_GFp_mont_field_encode
	.quad	ec_GFp_mont_field_decode
	.quad	ec_GFp_mont_field_set_to_one
	.quad	ec_key_simple_priv2oct
	.quad	ec_key_simple_oct2priv
	.quad	0
	.quad	ec_key_simple_generate_key
	.quad	ec_key_simple_check_key
	.quad	ec_key_simple_generate_public_key
	.quad	0
	.quad	0
	.quad	ecdh_simple_compute_key
	.quad	0
	.quad	ec_GFp_simple_blind_coordinates
	.quad	ec_GFp_simple_ladder_pre
	.quad	ec_GFp_simple_ladder_step
	.quad	ec_GFp_simple_ladder_post
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
