	.file	"bn_kron.c"
	.text
	.p2align 4
	.globl	BN_kronecker
	.type	BN_kronecker, @function
BN_kronecker:
.LFB252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rdx, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	call	BN_CTX_start@PLT
	movq	%r13, %rdi
	call	BN_CTX_get@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	BN_CTX_get@PLT
	testq	%rax, %rax
	je	.L3
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L3
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L3
	movq	%r12, %rdi
	call	BN_is_zero@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	jne	.L68
	movq	%r15, %rdi
	call	BN_is_odd@PLT
	testl	%eax, %eax
	jne	.L7
	movq	%r12, %rdi
	call	BN_is_odd@PLT
	testl	%eax, %eax
	jne	.L7
.L5:
	movq	%r13, %rdi
	movl	%eax, -52(%rbp)
	call	BN_CTX_end@PLT
	movl	-52(%rbp), %eax
.L72:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	movq	%r13, %rdi
	call	BN_CTX_end@PLT
	movl	$-2, %eax
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	addl	$1, %ebx
.L7:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	BN_is_bit_set@PLT
	testl	%eax, %eax
	je	.L9
	movl	%ebx, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	BN_rshift@PLT
	testl	%eax, %eax
	je	.L3
	andl	$1, %ebx
	movl	$1, %r14d
	je	.L10
	movl	8(%r15), %r14d
	testl	%r14d, %r14d
	jne	.L69
.L10:
	movl	16(%r12), %ecx
	testl	%ecx, %ecx
	je	.L22
	movl	$0, 16(%r12)
	movl	16(%r15), %edx
	testl	%edx, %edx
	jne	.L70
	.p2align 4,,10
	.p2align 3
.L22:
	movq	%r15, %rdi
	call	BN_is_zero@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	je	.L13
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L16:
	addl	$1, %ebx
.L13:
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	BN_is_bit_set@PLT
	testl	%eax, %eax
	je	.L16
	movl	%ebx, %edx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	BN_rshift@PLT
	testl	%eax, %eax
	je	.L3
	andl	$1, %ebx
	movl	8(%r12), %edx
	je	.L17
	testl	%edx, %edx
	je	.L24
	movq	(%r12), %rax
	leaq	tab.7125(%rip), %rcx
	movq	(%rax), %rax
	andl	$7, %eax
	imull	(%rcx,%rax,4), %r14d
.L17:
	movl	16(%r15), %eax
	movl	8(%r15), %ecx
	testl	%eax, %eax
	je	.L19
	movq	$-1, %rax
	testl	%ecx, %ecx
	je	.L20
	movq	(%r15), %rax
	movq	(%rax), %rax
	notq	%rax
.L20:
	testl	%edx, %edx
	je	.L21
	movq	(%r12), %rdx
	andq	(%rdx), %rax
	testb	$2, %al
	je	.L21
	negl	%r14d
.L21:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	BN_nnmod@PLT
	testl	%eax, %eax
	je	.L3
	movq	%r15, %rax
	movl	$0, 16(%r15)
	movq	%r12, %r15
	movq	%rax, %r12
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L19:
	testl	%ecx, %ecx
	je	.L21
	movq	(%r15), %rax
	movq	(%rax), %rax
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L24:
	xorl	%r14d, %r14d
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L68:
	movl	$1, %esi
	movq	%r15, %rdi
	call	BN_abs_is_word@PLT
	movq	%r13, %rdi
	movl	%eax, -52(%rbp)
	call	BN_CTX_end@PLT
	movl	-52(%rbp), %eax
	jmp	.L72
.L70:
	negl	%r14d
	jmp	.L22
.L69:
	movq	(%r15), %rax
	leaq	tab.7125(%rip), %rdx
	movq	(%rax), %rax
	andl	$7, %eax
	movl	(%rdx,%rax,4), %r14d
	jmp	.L10
.L71:
	movq	%r12, %rdi
	call	BN_is_one@PLT
	testl	%eax, %eax
	je	.L5
	movq	%r13, %rdi
	call	BN_CTX_end@PLT
	movl	%r14d, %eax
	jmp	.L1
	.cfi_endproc
.LFE252:
	.size	BN_kronecker, .-BN_kronecker
	.section	.rodata
	.align 32
	.type	tab.7125, @object
	.size	tab.7125, 32
tab.7125:
	.long	0
	.long	1
	.long	0
	.long	-1
	.long	0
	.long	-1
	.long	0
	.long	1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
