	.file	"bn_ctx.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/bn/bn_ctx.c"
	.text
	.p2align 4
	.globl	BN_CTX_new
	.type	BN_CTX_new, @function
BN_CTX_new:
.LFB252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$138, %edx
	movl	$64, %edi
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L6
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movups	%xmm0, (%rax)
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movl	$139, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$106, %esi
	movl	$3, %edi
	movq	%rax, -8(%rbp)
	call	ERR_put_error@PLT
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE252:
	.size	BN_CTX_new, .-BN_CTX_new
	.p2align 4
	.globl	BN_CTX_secure_new
	.type	BN_CTX_secure_new, @function
BN_CTX_secure_new:
.LFB253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$138, %edx
	movl	$64, %edi
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L11
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movl	$8, 60(%rax)
	movups	%xmm0, (%rax)
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movl	$139, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$106, %esi
	movl	$3, %edi
	movq	%rax, -8(%rbp)
	call	ERR_put_error@PLT
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE253:
	.size	BN_CTX_secure_new, .-BN_CTX_secure_new
	.p2align 4
	.globl	BN_CTX_free
	.type	BN_CTX_free, @function
BN_CTX_free:
.LFB254:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L12
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$251, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	32(%rdi), %rdi
	call	CRYPTO_free@PLT
	movq	(%r12), %rbx
	movq	$0, 32(%r12)
	testq	%rbx, %rbx
	je	.L14
	leaq	.LC0(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L17:
	leaq	384(%rbx), %r14
	.p2align 4,,10
	.p2align 3
.L16:
	cmpq	$0, (%rbx)
	je	.L15
	movq	%rbx, %rdi
	call	BN_clear_free@PLT
.L15:
	addq	$24, %rbx
	cmpq	%r14, %rbx
	jne	.L16
	movq	(%r12), %rdi
	movl	$303, %edx
	movq	%r13, %rsi
	movq	392(%rdi), %rax
	movq	%rax, 8(%r12)
	call	CRYPTO_free@PLT
	movq	8(%r12), %rbx
	movq	%rbx, (%r12)
	testq	%rbx, %rbx
	jne	.L17
.L14:
	popq	%rbx
	.cfi_restore 3
	movq	%r12, %rdi
	movl	$178, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L12:
	ret
	.cfi_endproc
.LFE254:
	.size	BN_CTX_free, .-BN_CTX_free
	.p2align 4
	.globl	BN_CTX_start
	.type	BN_CTX_start, @function
BN_CTX_start:
.LFB255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	52(%rdi), %eax
	movq	%rdi, %rbx
	testl	%eax, %eax
	jne	.L27
	movl	56(%rdi), %edx
	testl	%edx, %edx
	je	.L28
.L27:
	addl	$1, %eax
	movl	%eax, 52(%rbx)
.L26:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movl	48(%rdi), %r13d
	movl	40(%rdi), %eax
	cmpl	44(%rdi), %eax
	je	.L30
	movq	32(%rdi), %r12
.L31:
	leal	1(%rax), %edx
	movl	%edx, 40(%rbx)
	movl	%r13d, (%r12,%rax,4)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	testl	%eax, %eax
	jne	.L40
	movl	$128, %edi
	movl	$32, %r14d
.L32:
	movl	$264, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L41
	movl	40(%rbx), %eax
	movq	32(%rbx), %rdi
	testl	%eax, %eax
	jne	.L42
.L34:
	movl	$270, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, 32(%rbx)
	movl	40(%rbx), %eax
	movl	%r14d, 44(%rbx)
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L40:
	leal	(%rax,%rax,2), %r14d
	shrl	%r14d
	movl	%r14d, %edi
	salq	$2, %rdi
	jmp	.L32
.L41:
	movl	$265, %r8d
	movl	$65, %edx
	movl	$148, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$189, %r8d
	movl	$109, %edx
	leaq	.LC0(%rip), %rcx
	movl	$129, %esi
	movl	$3, %edi
	call	ERR_put_error@PLT
	addl	$1, 52(%rbx)
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L42:
	movq	%rdi, %rsi
	leaq	0(,%rax,4), %rdx
	movq	%r12, %rdi
	call	memcpy@PLT
	movq	32(%rbx), %rdi
	jmp	.L34
	.cfi_endproc
.LFE255:
	.size	BN_CTX_start, .-BN_CTX_start
	.p2align 4
	.globl	BN_CTX_end
	.type	BN_CTX_end, @function
BN_CTX_end:
.LFB256:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L43
	movl	52(%rdi), %eax
	testl	%eax, %eax
	jne	.L58
	movl	40(%rdi), %eax
	movq	32(%rdi), %rdx
	subl	$1, %eax
	movl	%eax, 40(%rdi)
	movl	(%rdx,%rax,4), %esi
	movl	48(%rdi), %eax
	cmpl	%esi, %eax
	jbe	.L49
	movl	24(%rdi), %ecx
	leal	-1(%rcx), %edx
	subl	%eax, %ecx
	subl	$1, %eax
	addl	%esi, %ecx
	andl	$15, %edx
	subl	%esi, %eax
	movl	%ecx, 24(%rdi)
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L47:
	movq	8(%rdi), %rdx
	movq	384(%rdx), %rdx
	movq	%rdx, 8(%rdi)
	testl	%eax, %eax
	je	.L49
	subl	$2, %eax
	movl	$14, %edx
.L48:
	testl	%ecx, %ecx
	je	.L49
.L50:
	leal	-1(%rax), %ecx
	testl	%edx, %edx
	je	.L47
	movl	%ecx, %r8d
	subl	$1, %edx
	movl	%eax, %ecx
	movl	%r8d, %eax
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L43:
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	subl	$1, %eax
	movl	%eax, 52(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	movl	%esi, 48(%rdi)
	movl	$0, 56(%rdi)
	ret
	.cfi_endproc
.LFE256:
	.size	BN_CTX_end, .-BN_CTX_end
	.p2align 4
	.globl	BN_CTX_get
	.type	BN_CTX_get, @function
BN_CTX_get:
.LFB257:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	52(%rdi), %edx
	testl	%edx, %edx
	jne	.L73
	movl	56(%rdi), %eax
	movq	%rdi, %rbx
	xorl	%r12d, %r12d
	testl	%eax, %eax
	jne	.L59
	movl	24(%rdi), %eax
	cmpl	28(%rdi), %eax
	je	.L78
	movl	%eax, %edx
	andl	$15, %edx
	testl	%eax, %eax
	jne	.L71
	movq	(%rdi), %rcx
	movq	%rcx, 8(%rdi)
.L72:
	addl	$1, %eax
	movl	%eax, 24(%rbx)
	movl	%edx, %eax
	leaq	(%rax,%rax,2), %rax
	leaq	(%rcx,%rax,8), %r12
	testq	%r12, %r12
	je	.L63
.L70:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	BN_set_word@PLT
	andl	$-5, 20(%r12)
	addl	$1, 48(%rbx)
.L59:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	movq	8(%rdi), %rcx
	testl	%edx, %edx
	jne	.L72
	movq	392(%rcx), %rcx
	movq	%rcx, 8(%rdi)
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L78:
	movl	60(%rdi), %r13d
	movl	$318, %edx
	leaq	.LC0(%rip), %rsi
	movl	$400, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L79
	andl	$8, %r13d
	movq	%rax, %r12
	leaq	384(%rax), %r15
	movq	%rax, %r13
	jne	.L67
	.p2align 4,,10
	.p2align 3
.L65:
	movq	%r13, %rdi
	addq	$24, %r13
	call	bn_init@PLT
	cmpq	%r15, %r13
	jne	.L65
.L66:
	movq	16(%rbx), %rax
	cmpq	$0, (%rbx)
	movq	$0, 392(%r14)
	movq	%rax, 384(%r14)
	je	.L80
	movq	%r14, %xmm0
	movq	%r14, 392(%rax)
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 8(%rbx)
.L69:
	addl	$16, 28(%rbx)
	addl	$1, 24(%rbx)
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L67:
	movq	%r13, %rdi
	call	bn_init@PLT
	movq	%r13, %rdi
	movl	$8, %esi
	addq	$24, %r13
	call	BN_set_flags@PLT
	cmpq	%r15, %r13
	jne	.L67
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L73:
	xorl	%r12d, %r12d
	jmp	.L59
.L79:
	movl	$319, %r8d
	movl	$65, %edx
	movl	$147, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L63:
	movl	$1, 56(%rbx)
	movl	$227, %r8d
	movl	$109, %edx
	xorl	%r12d, %r12d
	leaq	.LC0(%rip), %rcx
	movl	$116, %esi
	movl	$3, %edi
	call	ERR_put_error@PLT
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L80:
	movq	%r14, %xmm0
	movq	%r14, 16(%rbx)
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	jmp	.L69
	.cfi_endproc
.LFE257:
	.size	BN_CTX_get, .-BN_CTX_get
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
