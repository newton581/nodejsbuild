	.file	"sm4.c"
	.text
	.p2align 4
	.globl	SM4_set_key
	.type	SM4_set_key, @function
SM4_set_key:
.LFB9:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-4+CK.2108(%rip), %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	(%rdi), %r8d
	movl	12(%rdi), %edx
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	4(%rdi), %eax
	bswap	%r8d
	bswap	%edx
	xorl	$-1548633402, %r8d
	xorl	$-1301273892, %edx
	bswap	%eax
	xorl	$1453994832, %eax
	movl	%edx, -20(%rbp)
	movl	%edx, %ecx
	movl	%eax, -28(%rbp)
	movl	%eax, %r11d
	movl	8(%rdi), %eax
	movl	$1, %edx
	movl	%r8d, -32(%rbp)
	leaq	SM4_S(%rip), %rdi
	bswap	%eax
	xorl	$1736282519, %eax
	movl	%eax, -24(%rbp)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%rdx, %rax
	leal	2(%rdx), %r8d
	movslq	%r9d, %r9
	andl	$3, %eax
	andl	$3, %r8d
	movl	-32(%rbp,%rax,4), %r11d
	leal	1(%rdx), %eax
	movl	-32(%rbp,%r8,4), %ecx
	andl	$3, %eax
	movl	-32(%rbp,%r9,4), %r8d
	movl	-32(%rbp,%rax,4), %eax
.L3:
	xorl	%r11d, %eax
	xorl	(%r10,%rdx,4), %eax
	movl	%edx, %r9d
	xorl	%eax, %ecx
	andl	$3, %r9d
	movl	%ecx, %eax
	movzbl	%cl, %r11d
	shrl	$24, %eax
	movzbl	(%rdi,%r11), %r11d
	movzbl	(%rdi,%rax), %eax
	sall	$24, %eax
	orl	%r11d, %eax
	movl	%ecx, %r11d
	movzbl	%ch, %ecx
	shrl	$16, %r11d
	movzbl	(%rdi,%rcx), %ecx
	movzbl	%r11b, %r11d
	movzbl	(%rdi,%r11), %r11d
	sall	$8, %ecx
	sall	$16, %r11d
	orl	%r11d, %eax
	orl	%ecx, %eax
	movl	%eax, %ecx
	movl	%eax, %r11d
	rorl	$9, %ecx
	roll	$13, %r11d
	xorl	%r11d, %ecx
	xorl	%ecx, %eax
	xorl	%eax, %r8d
	leal	-1(%rdx), %eax
	movl	%r8d, -4(%rsi,%rdx,4)
	andl	$3, %eax
	addq	$1, %rdx
	movl	%r8d, -32(%rbp,%rax,4)
	cmpq	$33, %rdx
	jne	.L7
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L8
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	$1, %eax
	ret
.L8:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9:
	.size	SM4_set_key, .-SM4_set_key
	.p2align 4
	.globl	SM4_encrypt
	.type	SM4_encrypt, @function
SM4_encrypt:
.LFB10:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	SM4_S(%rip), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	8(%rdi), %ebx
	movl	4(%rdi), %r13d
	movl	(%rdi), %r12d
	movl	12(%rdi), %r11d
	bswap	%ebx
	movl	%ebx, %edi
	bswap	%r13d
	xorl	%r13d, %edi
	xorl	(%rdx), %edi
	bswap	%r12d
	bswap	%r11d
	xorl	%r11d, %edi
	movl	%edi, %eax
	movzbl	%dil, %r8d
	movl	%edi, %ecx
	shrl	$24, %eax
	movzbl	(%r15,%r8), %r8d
	movzbl	(%r15,%rax), %eax
	sall	$24, %eax
	orl	%r8d, %eax
	movl	%edi, %r8d
	movzbl	%ch, %edi
	shrl	$16, %r8d
	movzbl	(%r15,%rdi), %edi
	movzbl	%r8b, %r8d
	movzbl	(%r15,%r8), %r8d
	sall	$8, %edi
	sall	$16, %r8d
	orl	%r8d, %eax
	orl	%edi, %eax
	movl	%r11d, %edi
	movl	%eax, %r10d
	xorl	%eax, %r12d
	xorl	%ebx, %edi
	xorl	4(%rdx), %edi
	roll	$2, %r10d
	xorl	%r12d, %r10d
	movl	%eax, %r12d
	roll	$10, %r12d
	xorl	%r12d, %r10d
	movl	%eax, %r12d
	rorl	$8, %eax
	rorl	$14, %r12d
	xorl	%r12d, %r10d
	xorl	%eax, %r10d
	xorl	%r10d, %edi
	movl	%edi, %eax
	movzbl	%dil, %r8d
	movl	%edi, %ecx
	shrl	$24, %eax
	movzbl	(%r15,%r8), %r8d
	movzbl	(%r15,%rax), %eax
	sall	$24, %eax
	orl	%r8d, %eax
	movl	%edi, %r8d
	movzbl	%ch, %edi
	shrl	$16, %r8d
	movzbl	(%r15,%rdi), %edi
	movzbl	%r8b, %r8d
	movzbl	(%r15,%r8), %r8d
	sall	$8, %edi
	sall	$16, %r8d
	orl	%r8d, %eax
	movl	8(%rdx), %r8d
	orl	%edi, %eax
	movl	%eax, %r9d
	movl	%eax, %edi
	roll	$2, %r9d
	xorl	%eax, %r13d
	roll	$10, %edi
	xorl	%r11d, %r8d
	xorl	%r13d, %r9d
	xorl	%edi, %r9d
	movl	%eax, %edi
	rorl	$8, %eax
	rorl	$14, %edi
	xorl	%edi, %r9d
	movl	%r10d, %edi
	xorl	%eax, %r9d
	xorl	%r9d, %edi
	xorl	%edi, %r8d
	xorl	12(%rdx), %edi
	movl	%r8d, %eax
	movzbl	%r8b, %r12d
	movl	%r8d, %ecx
	shrl	$24, %eax
	movzbl	(%r15,%r12), %r12d
	movzbl	%ch, %ecx
	movzbl	(%r15,%rax), %eax
	sall	$24, %eax
	orl	%r12d, %eax
	movl	%r8d, %r12d
	movzbl	(%r15,%rcx), %r8d
	shrl	$16, %r12d
	movzbl	%r12b, %r12d
	sall	$8, %r8d
	movzbl	(%r15,%r12), %r12d
	sall	$16, %r12d
	orl	%r12d, %eax
	orl	%r8d, %eax
	movl	%eax, %r8d
	xorl	%eax, %ebx
	roll	$2, %r8d
	xorl	%ebx, %r8d
	movl	%eax, %ebx
	roll	$10, %ebx
	xorl	%ebx, %r8d
	movl	%eax, %ebx
	rorl	$8, %eax
	rorl	$14, %ebx
	xorl	%ebx, %r8d
	movl	%edi, %ebx
	xorl	%eax, %r8d
	xorl	%r8d, %ebx
	movl	%ebx, %eax
	movzbl	%bl, %edi
	shrl	$24, %eax
	movzbl	(%r15,%rdi), %edi
	movzbl	(%r15,%rax), %eax
	sall	$24, %eax
	orl	%edi, %eax
	movl	%ebx, %edi
	movzbl	%bh, %ebx
	shrl	$16, %edi
	movzbl	%dil, %edi
	movzbl	(%r15,%rdi), %edi
	sall	$16, %edi
	orl	%edi, %eax
	movzbl	(%r15,%rbx), %edi
	movl	16(%rdx), %ebx
	sall	$8, %edi
	xorl	%r9d, %ebx
	orl	%edi, %eax
	xorl	%r8d, %ebx
	movl	%eax, %edi
	xorl	%eax, %r11d
	roll	$2, %edi
	xorl	%r11d, %edi
	movl	%eax, %r11d
	roll	$10, %r11d
	xorl	%r11d, %edi
	movl	%eax, %r11d
	rorl	$8, %eax
	rorl	$14, %r11d
	xorl	%r11d, %edi
	xorl	%eax, %edi
	leaq	SM4_SBOX_T(%rip), %rax
	xorl	%edi, %ebx
	movzbl	%bl, %r11d
	movl	(%rax,%r11,4), %r12d
	movl	%ebx, %r11d
	shrl	$24, %r11d
	xorl	(%rax,%r11,4), %r10d
	roll	$8, %r12d
	movl	24(%rdx), %r13d
	xorl	%r10d, %r12d
	movl	%ebx, %r10d
	movzbl	%bh, %ebx
	shrl	$16, %r10d
	movzbl	%r10b, %r10d
	movl	(%rax,%r10,4), %r10d
	rorl	$8, %r10d
	xorl	%r12d, %r10d
	movl	(%rax,%rbx,4), %r12d
	movl	20(%rdx), %ebx
	roll	$16, %r12d
	xorl	%r8d, %ebx
	xorl	%r10d, %r12d
	xorl	%edi, %ebx
	xorl	%r12d, %ebx
	movzbl	%bl, %r10d
	movl	%ebx, %r11d
	movl	(%rax,%r10,4), %r10d
	shrl	$24, %r11d
	roll	$8, %r10d
	xorl	(%rax,%r11,4), %r9d
	xorl	%edi, %r13d
	xorl	%r10d, %r9d
	movl	%ebx, %r10d
	movzbl	%bh, %ebx
	shrl	$16, %r10d
	movzbl	%r10b, %r10d
	movl	(%rax,%r10,4), %r10d
	rorl	$8, %r10d
	xorl	%r9d, %r10d
	movl	(%rax,%rbx,4), %r9d
	movl	%r12d, %ebx
	roll	$16, %r9d
	xorl	%r10d, %r9d
	xorl	%r9d, %ebx
	xorl	%ebx, %r13d
	xorl	28(%rdx), %ebx
	movzbl	%r13b, %r10d
	movl	%r13d, %ecx
	movl	(%rax,%r10,4), %r11d
	movl	%r13d, %r10d
	movzbl	%ch, %ecx
	shrl	$24, %r10d
	xorl	(%rax,%r10,4), %r8d
	movl	%r13d, %r10d
	roll	$8, %r11d
	shrl	$16, %r10d
	xorl	%r11d, %r8d
	movzbl	%r10b, %r10d
	movl	(%rax,%r10,4), %r11d
	rorl	$8, %r11d
	xorl	%r8d, %r11d
	movl	(%rax,%rcx,4), %r8d
	roll	$16, %r8d
	xorl	%r11d, %r8d
	xorl	%r8d, %ebx
	movzbl	%bl, %r10d
	movl	%ebx, %r11d
	movl	(%rax,%r10,4), %r10d
	shrl	$24, %r11d
	xorl	(%rax,%r11,4), %edi
	roll	$8, %r10d
	xorl	%r10d, %edi
	movl	%ebx, %r10d
	movzbl	%bh, %ebx
	shrl	$16, %r10d
	movzbl	%r10b, %r10d
	movl	(%rax,%r10,4), %r10d
	rorl	$8, %r10d
	xorl	%edi, %r10d
	movl	(%rax,%rbx,4), %edi
	movl	32(%rdx), %ebx
	roll	$16, %edi
	xorl	%r9d, %ebx
	xorl	%r10d, %edi
	xorl	%r8d, %ebx
	xorl	%edi, %ebx
	movl	%ebx, %r10d
	movzbl	%bl, %r11d
	movl	(%rax,%r11,4), %ebx
	movl	%r10d, %r11d
	movl	%r10d, %ecx
	shrl	$24, %r11d
	xorl	(%rax,%r11,4), %r12d
	movl	%r10d, %r11d
	roll	$8, %ebx
	movl	36(%rdx), %r10d
	shrl	$16, %r11d
	xorl	%ebx, %r12d
	movzbl	%r11b, %r11d
	xorl	%r8d, %r10d
	movl	(%rax,%r11,4), %ebx
	xorl	%edi, %r10d
	rorl	$8, %ebx
	xorl	%ebx, %r12d
	movzbl	%ch, %ebx
	movl	(%rax,%rbx,4), %ebx
	roll	$16, %ebx
	xorl	%r12d, %ebx
	movl	40(%rdx), %r12d
	xorl	%ebx, %r10d
	movzbl	%r10b, %r11d
	movl	%r10d, %ecx
	xorl	%edi, %r12d
	movl	(%rax,%r11,4), %r14d
	movl	%r10d, %r11d
	movzbl	%ch, %ecx
	shrl	$24, %r11d
	xorl	(%rax,%r11,4), %r9d
	movl	%r10d, %r11d
	roll	$8, %r14d
	shrl	$16, %r11d
	xorl	%r14d, %r9d
	movzbl	%r11b, %r11d
	movl	(%rax,%r11,4), %r14d
	movl	%ebx, %r11d
	rorl	$8, %r14d
	xorl	%r14d, %r9d
	movl	(%rax,%rcx,4), %r14d
	roll	$16, %r14d
	xorl	%r9d, %r14d
	xorl	%r14d, %r11d
	xorl	%r11d, %r12d
	movzbl	%r12b, %r9d
	movl	%r12d, %ecx
	movl	(%rax,%r9,4), %r10d
	movl	%r12d, %r9d
	movzbl	%ch, %ecx
	shrl	$24, %r9d
	xorl	(%rax,%r9,4), %r8d
	movl	%r12d, %r9d
	roll	$8, %r10d
	shrl	$16, %r9d
	xorl	%r10d, %r8d
	movzbl	%r9b, %r9d
	movl	(%rax,%r9,4), %r10d
	rorl	$8, %r10d
	xorl	%r10d, %r8d
	movl	(%rax,%rcx,4), %r10d
	roll	$16, %r10d
	xorl	44(%rdx), %r11d
	xorl	%r8d, %r10d
	movl	%r11d, %r8d
	xorl	%r10d, %r8d
	movzbl	%r8b, %r9d
	movl	%r8d, %ecx
	movl	(%rax,%r9,4), %r11d
	movl	%r8d, %r9d
	movzbl	%ch, %ecx
	shrl	$24, %r9d
	xorl	(%rax,%r9,4), %edi
	movl	%r8d, %r9d
	roll	$8, %r11d
	shrl	$16, %r9d
	xorl	%r11d, %edi
	movzbl	%r9b, %r9d
	movl	(%rax,%r9,4), %r11d
	rorl	$8, %r11d
	xorl	%r11d, %edi
	movl	(%rax,%rcx,4), %r11d
	roll	$16, %r11d
	xorl	%edi, %r11d
	movl	48(%rdx), %edi
	xorl	%r14d, %edi
	xorl	%r10d, %edi
	xorl	%r11d, %edi
	movzbl	%dil, %r8d
	movl	%edi, %ecx
	movl	(%rax,%r8,4), %r13d
	movl	%edi, %r8d
	shrl	$24, %r8d
	xorl	(%rax,%r8,4), %ebx
	movl	%edi, %r8d
	roll	$8, %r13d
	movzbl	%ch, %edi
	shrl	$16, %r8d
	xorl	%r13d, %ebx
	movzbl	%r8b, %r8d
	movl	(%rax,%r8,4), %r13d
	rorl	$8, %r13d
	xorl	%r13d, %ebx
	movl	(%rax,%rdi,4), %r13d
	movl	52(%rdx), %edi
	xorl	%r10d, %edi
	roll	$16, %r13d
	xorl	%r11d, %edi
	xorl	%ebx, %r13d
	movl	%edi, %ebx
	xorl	%r13d, %ebx
	movzbl	%bl, %edi
	movl	(%rax,%rdi,4), %r12d
	movl	%ebx, %edi
	shrl	$24, %edi
	xorl	(%rax,%rdi,4), %r14d
	roll	$8, %r12d
	movl	%ebx, %edi
	movzbl	%bh, %ebx
	xorl	%r12d, %r14d
	shrl	$16, %edi
	movzbl	%dil, %edi
	movl	(%rax,%rdi,4), %r12d
	movl	56(%rdx), %edi
	rorl	$8, %r12d
	xorl	%r11d, %edi
	xorl	%r12d, %r14d
	movl	(%rax,%rbx,4), %r12d
	movl	%r13d, %ebx
	roll	$16, %r12d
	xorl	%r14d, %r12d
	xorl	%r12d, %ebx
	xorl	%ebx, %edi
	xorl	60(%rdx), %ebx
	movzbl	%dil, %r8d
	movl	%edi, %ecx
	movl	(%rax,%r8,4), %r9d
	movl	%edi, %r8d
	shrl	$24, %r8d
	xorl	(%rax,%r8,4), %r10d
	movl	%edi, %r8d
	roll	$8, %r9d
	movzbl	%ch, %edi
	shrl	$16, %r8d
	xorl	%r9d, %r10d
	movzbl	%r8b, %r8d
	movl	(%rax,%r8,4), %r9d
	rorl	$8, %r9d
	xorl	%r9d, %r10d
	movl	(%rax,%rdi,4), %r9d
	roll	$16, %r9d
	xorl	%r10d, %r9d
	xorl	%r9d, %ebx
	movzbl	%bl, %edi
	movl	%ebx, %r8d
	movl	(%rax,%rdi,4), %edi
	shrl	$24, %r8d
	xorl	(%rax,%r8,4), %r11d
	roll	$8, %edi
	xorl	%edi, %r11d
	movl	%ebx, %edi
	movzbl	%bh, %ebx
	shrl	$16, %edi
	movzbl	%dil, %edi
	movl	(%rax,%rdi,4), %edi
	rorl	$8, %edi
	xorl	%edi, %r11d
	movl	(%rax,%rbx,4), %edi
	movl	64(%rdx), %ebx
	roll	$16, %edi
	xorl	%r12d, %ebx
	xorl	%r11d, %edi
	xorl	%r9d, %ebx
	xorl	%edi, %ebx
	movzbl	%bl, %r10d
	movl	%ebx, %r8d
	movl	(%rax,%r10,4), %ebx
	movl	%r8d, %r10d
	movl	%r8d, %ecx
	roll	$8, %ebx
	shrl	$24, %r10d
	xorl	(%rax,%r10,4), %r13d
	movl	%r8d, %r10d
	movl	68(%rdx), %r8d
	shrl	$16, %r10d
	xorl	%ebx, %r13d
	movzbl	%r10b, %r10d
	xorl	%r9d, %r8d
	movl	(%rax,%r10,4), %ebx
	xorl	%edi, %r8d
	rorl	$8, %ebx
	xorl	%ebx, %r13d
	movzbl	%ch, %ebx
	movl	(%rax,%rbx,4), %ebx
	roll	$16, %ebx
	xorl	%r13d, %ebx
	xorl	%ebx, %r8d
	movzbl	%r8b, %r10d
	movl	%r8d, %ecx
	movl	(%rax,%r10,4), %r11d
	movl	%r8d, %r10d
	movzbl	%ch, %ecx
	shrl	$24, %r10d
	xorl	(%rax,%r10,4), %r12d
	movl	%r8d, %r10d
	roll	$8, %r11d
	shrl	$16, %r10d
	xorl	%r11d, %r12d
	movzbl	%r10b, %r10d
	movl	(%rax,%r10,4), %r11d
	movl	72(%rdx), %r10d
	rorl	$8, %r11d
	xorl	%edi, %r10d
	xorl	%r11d, %r12d
	movl	(%rax,%rcx,4), %r11d
	roll	$16, %r11d
	xorl	%r12d, %r11d
	movl	%ebx, %r12d
	xorl	%r11d, %r12d
	xorl	%r12d, %r10d
	movzbl	%r10b, %r8d
	movl	%r10d, %r13d
	movl	%r10d, %ecx
	movl	(%rax,%r8,4), %r8d
	shrl	$24, %r13d
	movzbl	%ch, %ecx
	xorl	(%rax,%r13,4), %r9d
	roll	$8, %r8d
	xorl	%r8d, %r9d
	movl	%r10d, %r8d
	shrl	$16, %r8d
	movzbl	%r8b, %r8d
	movl	(%rax,%r8,4), %r8d
	rorl	$8, %r8d
	xorl	%r8d, %r9d
	movl	(%rax,%rcx,4), %r8d
	roll	$16, %r8d
	xorl	%r9d, %r8d
	xorl	76(%rdx), %r12d
	xorl	%r8d, %r12d
	movzbl	%r12b, %r9d
	movl	%r12d, %ecx
	movl	(%rax,%r9,4), %r10d
	movl	%r12d, %r9d
	movzbl	%ch, %ecx
	shrl	$24, %r9d
	xorl	(%rax,%r9,4), %edi
	movl	%r12d, %r9d
	roll	$8, %r10d
	shrl	$16, %r9d
	xorl	%r10d, %edi
	movzbl	%r9b, %r9d
	movl	(%rax,%r9,4), %r10d
	rorl	$8, %r10d
	xorl	%r10d, %edi
	movl	(%rax,%rcx,4), %r10d
	roll	$16, %r10d
	xorl	%edi, %r10d
	movl	80(%rdx), %edi
	xorl	%r11d, %edi
	xorl	%r8d, %edi
	xorl	%r10d, %edi
	movl	%edi, %r9d
	movzbl	%dil, %edi
	movl	(%rax,%rdi,4), %edi
	movl	%r9d, %r12d
	movl	%r9d, %ecx
	shrl	$24, %r12d
	xorl	(%rax,%r12,4), %ebx
	roll	$8, %edi
	xorl	%edi, %ebx
	movl	%r9d, %edi
	shrl	$16, %edi
	movzbl	%dil, %edi
	movl	(%rax,%rdi,4), %edi
	rorl	$8, %edi
	xorl	%edi, %ebx
	movzbl	%ch, %edi
	movl	(%rax,%rdi,4), %edi
	roll	$16, %edi
	xorl	%ebx, %edi
	movl	84(%rdx), %ebx
	xorl	%r8d, %ebx
	xorl	%r10d, %ebx
	xorl	%edi, %ebx
	movzbl	%bl, %r9d
	movl	(%rax,%r9,4), %r13d
	movl	%ebx, %r9d
	shrl	$24, %r9d
	xorl	(%rax,%r9,4), %r11d
	movl	%ebx, %r9d
	roll	$8, %r13d
	movzbl	%bh, %ebx
	shrl	$16, %r9d
	xorl	%r13d, %r11d
	movzbl	%r9b, %r9d
	movl	(%rax,%r9,4), %r13d
	movl	88(%rdx), %r9d
	rorl	$8, %r13d
	xorl	%r13d, %r11d
	movl	(%rax,%rbx,4), %r13d
	movl	%edi, %ebx
	xorl	%r10d, %r9d
	roll	$16, %r13d
	xorl	%r11d, %r13d
	xorl	%r13d, %ebx
	xorl	%ebx, %r9d
	xorl	92(%rdx), %ebx
	movzbl	%r9b, %r11d
	movl	%r9d, %ecx
	movl	(%rax,%r11,4), %r12d
	movl	%r9d, %r11d
	movzbl	%ch, %ecx
	shrl	$24, %r11d
	xorl	(%rax,%r11,4), %r8d
	movl	%r9d, %r11d
	roll	$8, %r12d
	shrl	$16, %r11d
	xorl	%r12d, %r8d
	movzbl	%r11b, %r11d
	movl	(%rax,%r11,4), %r12d
	rorl	$8, %r12d
	xorl	%r12d, %r8d
	movl	(%rax,%rcx,4), %r12d
	roll	$16, %r12d
	xorl	%r8d, %r12d
	xorl	%r12d, %ebx
	movzbl	%bl, %r8d
	movl	(%rax,%r8,4), %r9d
	movl	%ebx, %r8d
	shrl	$24, %r8d
	xorl	(%rax,%r8,4), %r10d
	movl	%ebx, %r8d
	roll	$8, %r9d
	movzbl	%bh, %ebx
	shrl	$16, %r8d
	xorl	%r9d, %r10d
	movzbl	%r8b, %r8d
	movl	(%rax,%r8,4), %r9d
	rorl	$8, %r9d
	xorl	%r9d, %r10d
	movl	(%rax,%rbx,4), %r9d
	movl	96(%rdx), %ebx
	roll	$16, %r9d
	xorl	%r13d, %ebx
	xorl	%r10d, %r9d
	xorl	%r12d, %ebx
	xorl	%r9d, %ebx
	movl	%ebx, %r8d
	movzbl	%bl, %r10d
	movl	(%rax,%r10,4), %ebx
	movl	%r8d, %r10d
	movl	%r8d, %ecx
	shrl	$24, %r10d
	xorl	(%rax,%r10,4), %edi
	roll	$8, %ebx
	movl	%r8d, %r10d
	xorl	%ebx, %edi
	shrl	$16, %r10d
	movzbl	%r10b, %r10d
	movl	(%rax,%r10,4), %ebx
	movl	104(%rdx), %r10d
	rorl	$8, %ebx
	xorl	%r9d, %r10d
	xorl	%ebx, %edi
	movzbl	%ch, %ebx
	movl	(%rax,%rbx,4), %ebx
	roll	$16, %ebx
	xorl	%edi, %ebx
	movl	100(%rdx), %edi
	xorl	%r12d, %edi
	xorl	%r9d, %edi
	xorl	%ebx, %edi
	movzbl	%dil, %r8d
	movl	%edi, %ecx
	movl	(%rax,%r8,4), %r11d
	movl	%edi, %r8d
	shrl	$24, %r8d
	xorl	(%rax,%r8,4), %r13d
	movl	%edi, %r8d
	roll	$8, %r11d
	movzbl	%ch, %edi
	shrl	$16, %r8d
	xorl	%r11d, %r13d
	movzbl	%r8b, %r8d
	movl	(%rax,%r8,4), %r11d
	rorl	$8, %r11d
	xorl	%r11d, %r13d
	movl	(%rax,%rdi,4), %r11d
	movl	%ebx, %edi
	roll	$16, %r11d
	xorl	%r13d, %r11d
	xorl	%r11d, %edi
	xorl	%edi, %r10d
	xorl	108(%rdx), %edi
	movzbl	%r10b, %r8d
	movl	%r10d, %r13d
	movl	%r10d, %ecx
	movl	(%rax,%r8,4), %r8d
	shrl	$24, %r13d
	movzbl	%ch, %ecx
	xorl	(%rax,%r13,4), %r12d
	roll	$8, %r8d
	xorl	%r8d, %r12d
	movl	%r10d, %r8d
	shrl	$16, %r8d
	movzbl	%r8b, %r8d
	movl	(%rax,%r8,4), %r8d
	rorl	$8, %r8d
	xorl	%r8d, %r12d
	movl	(%rax,%rcx,4), %r8d
	roll	$16, %r8d
	xorl	%r12d, %r8d
	xorl	%r8d, %edi
	movzbl	%dil, %r10d
	movl	%edi, %r12d
	movl	%edi, %ecx
	movl	(%rax,%r10,4), %r10d
	roll	$8, %r10d
	shrl	$24, %r12d
	xorl	(%rax,%r12,4), %r9d
	xorl	%r10d, %r9d
	movl	%edi, %r10d
	movzbl	%ch, %edi
	shrl	$16, %r10d
	movzbl	%r10b, %r10d
	movl	(%rax,%r10,4), %r10d
	rorl	$8, %r10d
	xorl	%r10d, %r9d
	movl	(%rax,%rdi,4), %r10d
	movl	112(%rdx), %eax
	roll	$16, %r10d
	xorl	%r11d, %eax
	xorl	%r9d, %r10d
	xorl	%r8d, %eax
	xorl	%r10d, %eax
	movl	%eax, %edi
	shrl	$24, %edi
	movzbl	(%r15,%rdi), %r9d
	movzbl	%al, %edi
	movzbl	(%r15,%rdi), %edi
	sall	$24, %r9d
	orl	%edi, %r9d
	movl	%eax, %edi
	movzbl	%ah, %eax
	shrl	$16, %edi
	movzbl	(%r15,%rax), %eax
	movzbl	%dil, %edi
	movzbl	(%r15,%rdi), %edi
	sall	$8, %eax
	sall	$16, %edi
	orl	%edi, %r9d
	orl	%eax, %r9d
	movl	%r9d, %eax
	xorl	%r9d, %ebx
	movl	%r9d, %edi
	roll	$2, %eax
	roll	$10, %edi
	xorl	%ebx, %eax
	xorl	%edi, %eax
	movl	%r9d, %edi
	rorl	$14, %edi
	xorl	%eax, %edi
	movl	%r9d, %eax
	rorl	$8, %eax
	xorl	%edi, %eax
	movl	116(%rdx), %edi
	xorl	%r8d, %edi
	xorl	%r10d, %edi
	movl	%edi, %ebx
	xorl	%eax, %ebx
	movl	%ebx, %edi
	shrl	$24, %edi
	movzbl	(%r15,%rdi), %r9d
	movzbl	%bl, %edi
	movzbl	(%r15,%rdi), %edi
	sall	$24, %r9d
	orl	%edi, %r9d
	movl	%ebx, %edi
	movzbl	%bh, %ebx
	shrl	$16, %edi
	movzbl	%dil, %edi
	movzbl	(%r15,%rdi), %edi
	sall	$16, %edi
	orl	%edi, %r9d
	movzbl	(%r15,%rbx), %edi
	movl	120(%rdx), %ebx
	sall	$8, %edi
	xorl	%r10d, %ebx
	orl	%edi, %r9d
	movl	%r9d, %edi
	xorl	%r9d, %r11d
	roll	$2, %edi
	xorl	%r11d, %edi
	movl	%r9d, %r11d
	roll	$10, %r11d
	xorl	%r11d, %edi
	movl	%r9d, %r11d
	rorl	$14, %r11d
	xorl	%edi, %r11d
	movl	%r9d, %edi
	rorl	$8, %edi
	xorl	%r11d, %edi
	movl	%eax, %r11d
	bswap	%eax
	xorl	%edi, %r11d
	bswap	%edi
	xorl	%r11d, %ebx
	movl	%ebx, %r9d
	movzbl	%bl, %r12d
	shrl	$24, %r9d
	movzbl	(%r15,%r12), %r12d
	movzbl	(%r15,%r9), %r9d
	sall	$24, %r9d
	orl	%r12d, %r9d
	movl	%ebx, %r12d
	movzbl	%bh, %ebx
	shrl	$16, %r12d
	movzbl	(%r15,%rbx), %ebx
	movzbl	%r12b, %r12d
	movzbl	(%r15,%r12), %r12d
	sall	$8, %ebx
	sall	$16, %r12d
	orl	%r12d, %r9d
	orl	%ebx, %r9d
	movl	%r9d, %ebx
	xorl	%r9d, %r8d
	roll	$2, %ebx
	xorl	%ebx, %r8d
	movl	%r9d, %ebx
	roll	$10, %ebx
	xorl	%ebx, %r8d
	movl	%r9d, %ebx
	xorl	124(%rdx), %r11d
	rorl	$8, %r9d
	rorl	$14, %ebx
	movl	%edi, 8(%rsi)
	xorl	%ebx, %r8d
	movl	%r11d, %ebx
	movl	%eax, 12(%rsi)
	xorl	%r8d, %r9d
	xorl	%r9d, %ebx
	bswap	%r9d
	movl	%r9d, 4(%rsi)
	movl	%ebx, %edx
	movzbl	%bl, %r8d
	shrl	$24, %edx
	movzbl	(%r15,%r8), %r8d
	movzbl	(%r15,%rdx), %edx
	sall	$24, %edx
	orl	%r8d, %edx
	movl	%ebx, %r8d
	movzbl	%bh, %ebx
	shrl	$16, %r8d
	movzbl	(%r15,%rbx), %ecx
	popq	%rbx
	movzbl	%r8b, %r8d
	popq	%r12
	popq	%r13
	movzbl	(%r15,%r8), %r8d
	sall	$8, %ecx
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	sall	$16, %r8d
	orl	%r8d, %edx
	orl	%ecx, %edx
	movl	%edx, %ecx
	xorl	%edx, %r10d
	roll	$2, %ecx
	xorl	%ecx, %r10d
	movl	%edx, %ecx
	roll	$10, %ecx
	xorl	%ecx, %r10d
	movl	%edx, %ecx
	rorl	$8, %edx
	rorl	$14, %ecx
	xorl	%ecx, %r10d
	xorl	%r10d, %edx
	bswap	%edx
	movl	%edx, (%rsi)
	ret
	.cfi_endproc
.LFE10:
	.size	SM4_encrypt, .-SM4_encrypt
	.p2align 4
	.globl	SM4_decrypt
	.type	SM4_decrypt, @function
SM4_decrypt:
.LFB11:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	SM4_S(%rip), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	8(%rdi), %ebx
	movl	4(%rdi), %r13d
	movl	(%rdi), %r12d
	movl	12(%rdi), %r11d
	bswap	%ebx
	movl	%ebx, %edi
	bswap	%r13d
	xorl	%r13d, %edi
	xorl	124(%rdx), %edi
	bswap	%r12d
	bswap	%r11d
	xorl	%r11d, %edi
	movl	%edi, %eax
	movzbl	%dil, %r8d
	movl	%edi, %ecx
	shrl	$24, %eax
	movzbl	(%r15,%r8), %r8d
	movzbl	(%r15,%rax), %eax
	sall	$24, %eax
	orl	%r8d, %eax
	movl	%edi, %r8d
	movzbl	%ch, %edi
	shrl	$16, %r8d
	movzbl	(%r15,%rdi), %edi
	movzbl	%r8b, %r8d
	movzbl	(%r15,%r8), %r8d
	sall	$8, %edi
	sall	$16, %r8d
	orl	%r8d, %eax
	orl	%edi, %eax
	movl	%r11d, %edi
	movl	%eax, %r10d
	xorl	%eax, %r12d
	xorl	%ebx, %edi
	xorl	120(%rdx), %edi
	roll	$2, %r10d
	xorl	%r12d, %r10d
	movl	%eax, %r12d
	roll	$10, %r12d
	xorl	%r12d, %r10d
	movl	%eax, %r12d
	rorl	$8, %eax
	rorl	$14, %r12d
	xorl	%r12d, %r10d
	xorl	%eax, %r10d
	xorl	%r10d, %edi
	movl	%edi, %eax
	movzbl	%dil, %r8d
	movl	%edi, %ecx
	shrl	$24, %eax
	movzbl	(%r15,%r8), %r8d
	movzbl	(%r15,%rax), %eax
	sall	$24, %eax
	orl	%r8d, %eax
	movl	%edi, %r8d
	movzbl	%ch, %edi
	shrl	$16, %r8d
	movzbl	(%r15,%rdi), %edi
	movzbl	%r8b, %r8d
	movzbl	(%r15,%r8), %r8d
	sall	$8, %edi
	sall	$16, %r8d
	orl	%r8d, %eax
	movl	116(%rdx), %r8d
	orl	%edi, %eax
	movl	%eax, %r9d
	movl	%eax, %edi
	roll	$2, %r9d
	xorl	%eax, %r13d
	roll	$10, %edi
	xorl	%r11d, %r8d
	xorl	%r13d, %r9d
	xorl	%edi, %r9d
	movl	%eax, %edi
	rorl	$8, %eax
	rorl	$14, %edi
	xorl	%edi, %r9d
	movl	%r10d, %edi
	xorl	%eax, %r9d
	xorl	%r9d, %edi
	xorl	%edi, %r8d
	xorl	112(%rdx), %edi
	movl	%r8d, %eax
	movzbl	%r8b, %r12d
	movl	%r8d, %ecx
	shrl	$24, %eax
	movzbl	(%r15,%r12), %r12d
	movzbl	%ch, %ecx
	movzbl	(%r15,%rax), %eax
	sall	$24, %eax
	orl	%r12d, %eax
	movl	%r8d, %r12d
	movzbl	(%r15,%rcx), %r8d
	shrl	$16, %r12d
	movzbl	%r12b, %r12d
	sall	$8, %r8d
	movzbl	(%r15,%r12), %r12d
	sall	$16, %r12d
	orl	%r12d, %eax
	orl	%r8d, %eax
	movl	%eax, %r8d
	xorl	%eax, %ebx
	roll	$2, %r8d
	xorl	%ebx, %r8d
	movl	%eax, %ebx
	roll	$10, %ebx
	xorl	%ebx, %r8d
	movl	%eax, %ebx
	rorl	$8, %eax
	rorl	$14, %ebx
	xorl	%ebx, %r8d
	movl	%edi, %ebx
	xorl	%eax, %r8d
	xorl	%r8d, %ebx
	movl	%ebx, %eax
	movzbl	%bl, %edi
	shrl	$24, %eax
	movzbl	(%r15,%rdi), %edi
	movzbl	(%r15,%rax), %eax
	sall	$24, %eax
	orl	%edi, %eax
	movl	%ebx, %edi
	movzbl	%bh, %ebx
	shrl	$16, %edi
	movzbl	%dil, %edi
	movzbl	(%r15,%rdi), %edi
	sall	$16, %edi
	orl	%edi, %eax
	movzbl	(%r15,%rbx), %edi
	movl	108(%rdx), %ebx
	sall	$8, %edi
	xorl	%r9d, %ebx
	orl	%edi, %eax
	xorl	%r8d, %ebx
	movl	%eax, %edi
	xorl	%eax, %r11d
	roll	$2, %edi
	xorl	%r11d, %edi
	movl	%eax, %r11d
	roll	$10, %r11d
	xorl	%r11d, %edi
	movl	%eax, %r11d
	rorl	$8, %eax
	rorl	$14, %r11d
	xorl	%r11d, %edi
	xorl	%eax, %edi
	leaq	SM4_SBOX_T(%rip), %rax
	xorl	%edi, %ebx
	movzbl	%bl, %r11d
	movl	(%rax,%r11,4), %r12d
	movl	%ebx, %r11d
	shrl	$24, %r11d
	xorl	(%rax,%r11,4), %r10d
	roll	$8, %r12d
	movl	100(%rdx), %r13d
	xorl	%r10d, %r12d
	movl	%ebx, %r10d
	movzbl	%bh, %ebx
	shrl	$16, %r10d
	movzbl	%r10b, %r10d
	movl	(%rax,%r10,4), %r10d
	rorl	$8, %r10d
	xorl	%r12d, %r10d
	movl	(%rax,%rbx,4), %r12d
	movl	104(%rdx), %ebx
	roll	$16, %r12d
	xorl	%r8d, %ebx
	xorl	%r10d, %r12d
	xorl	%edi, %ebx
	xorl	%r12d, %ebx
	movzbl	%bl, %r10d
	movl	%ebx, %r11d
	movl	(%rax,%r10,4), %r10d
	shrl	$24, %r11d
	roll	$8, %r10d
	xorl	(%rax,%r11,4), %r9d
	xorl	%edi, %r13d
	xorl	%r10d, %r9d
	movl	%ebx, %r10d
	movzbl	%bh, %ebx
	shrl	$16, %r10d
	movzbl	%r10b, %r10d
	movl	(%rax,%r10,4), %r10d
	rorl	$8, %r10d
	xorl	%r9d, %r10d
	movl	(%rax,%rbx,4), %r9d
	movl	%r12d, %ebx
	roll	$16, %r9d
	xorl	%r10d, %r9d
	xorl	%r9d, %ebx
	xorl	%ebx, %r13d
	xorl	96(%rdx), %ebx
	movzbl	%r13b, %r10d
	movl	%r13d, %ecx
	movl	(%rax,%r10,4), %r11d
	movl	%r13d, %r10d
	movzbl	%ch, %ecx
	shrl	$24, %r10d
	xorl	(%rax,%r10,4), %r8d
	movl	%r13d, %r10d
	roll	$8, %r11d
	shrl	$16, %r10d
	xorl	%r11d, %r8d
	movzbl	%r10b, %r10d
	movl	(%rax,%r10,4), %r11d
	rorl	$8, %r11d
	xorl	%r8d, %r11d
	movl	(%rax,%rcx,4), %r8d
	roll	$16, %r8d
	xorl	%r11d, %r8d
	xorl	%r8d, %ebx
	movzbl	%bl, %r10d
	movl	%ebx, %r11d
	movl	(%rax,%r10,4), %r10d
	shrl	$24, %r11d
	xorl	(%rax,%r11,4), %edi
	roll	$8, %r10d
	xorl	%r10d, %edi
	movl	%ebx, %r10d
	movzbl	%bh, %ebx
	shrl	$16, %r10d
	movzbl	%r10b, %r10d
	movl	(%rax,%r10,4), %r10d
	rorl	$8, %r10d
	xorl	%edi, %r10d
	movl	(%rax,%rbx,4), %edi
	movl	92(%rdx), %ebx
	roll	$16, %edi
	xorl	%r9d, %ebx
	xorl	%r10d, %edi
	xorl	%r8d, %ebx
	xorl	%edi, %ebx
	movl	%ebx, %r10d
	movzbl	%bl, %r11d
	movl	(%rax,%r11,4), %ebx
	movl	%r10d, %r11d
	movl	%r10d, %ecx
	shrl	$24, %r11d
	xorl	(%rax,%r11,4), %r12d
	movl	%r10d, %r11d
	roll	$8, %ebx
	movl	88(%rdx), %r10d
	shrl	$16, %r11d
	xorl	%ebx, %r12d
	movzbl	%r11b, %r11d
	xorl	%r8d, %r10d
	movl	(%rax,%r11,4), %ebx
	xorl	%edi, %r10d
	rorl	$8, %ebx
	xorl	%ebx, %r12d
	movzbl	%ch, %ebx
	movl	(%rax,%rbx,4), %ebx
	roll	$16, %ebx
	xorl	%r12d, %ebx
	movl	84(%rdx), %r12d
	xorl	%ebx, %r10d
	movzbl	%r10b, %r11d
	movl	%r10d, %ecx
	xorl	%edi, %r12d
	movl	(%rax,%r11,4), %r14d
	movl	%r10d, %r11d
	movzbl	%ch, %ecx
	shrl	$24, %r11d
	xorl	(%rax,%r11,4), %r9d
	movl	%r10d, %r11d
	roll	$8, %r14d
	shrl	$16, %r11d
	xorl	%r14d, %r9d
	movzbl	%r11b, %r11d
	movl	(%rax,%r11,4), %r14d
	movl	%ebx, %r11d
	rorl	$8, %r14d
	xorl	%r14d, %r9d
	movl	(%rax,%rcx,4), %r14d
	roll	$16, %r14d
	xorl	%r9d, %r14d
	xorl	%r14d, %r11d
	xorl	%r11d, %r12d
	movzbl	%r12b, %r9d
	movl	%r12d, %ecx
	movl	(%rax,%r9,4), %r10d
	movl	%r12d, %r9d
	movzbl	%ch, %ecx
	shrl	$24, %r9d
	xorl	(%rax,%r9,4), %r8d
	movl	%r12d, %r9d
	roll	$8, %r10d
	shrl	$16, %r9d
	xorl	%r10d, %r8d
	movzbl	%r9b, %r9d
	movl	(%rax,%r9,4), %r10d
	rorl	$8, %r10d
	xorl	%r10d, %r8d
	movl	(%rax,%rcx,4), %r10d
	roll	$16, %r10d
	xorl	80(%rdx), %r11d
	xorl	%r8d, %r10d
	movl	%r11d, %r8d
	xorl	%r10d, %r8d
	movzbl	%r8b, %r9d
	movl	%r8d, %ecx
	movl	(%rax,%r9,4), %r11d
	movl	%r8d, %r9d
	movzbl	%ch, %ecx
	shrl	$24, %r9d
	xorl	(%rax,%r9,4), %edi
	movl	%r8d, %r9d
	roll	$8, %r11d
	shrl	$16, %r9d
	xorl	%r11d, %edi
	movzbl	%r9b, %r9d
	movl	(%rax,%r9,4), %r11d
	rorl	$8, %r11d
	xorl	%r11d, %edi
	movl	(%rax,%rcx,4), %r11d
	roll	$16, %r11d
	xorl	%edi, %r11d
	movl	76(%rdx), %edi
	xorl	%r14d, %edi
	xorl	%r10d, %edi
	xorl	%r11d, %edi
	movzbl	%dil, %r8d
	movl	%edi, %ecx
	movl	(%rax,%r8,4), %r13d
	movl	%edi, %r8d
	shrl	$24, %r8d
	xorl	(%rax,%r8,4), %ebx
	movl	%edi, %r8d
	roll	$8, %r13d
	movzbl	%ch, %edi
	shrl	$16, %r8d
	xorl	%r13d, %ebx
	movzbl	%r8b, %r8d
	movl	(%rax,%r8,4), %r13d
	rorl	$8, %r13d
	xorl	%r13d, %ebx
	movl	(%rax,%rdi,4), %r13d
	movl	72(%rdx), %edi
	xorl	%r10d, %edi
	roll	$16, %r13d
	xorl	%r11d, %edi
	xorl	%ebx, %r13d
	movl	%edi, %ebx
	xorl	%r13d, %ebx
	movzbl	%bl, %edi
	movl	(%rax,%rdi,4), %r12d
	movl	%ebx, %edi
	shrl	$24, %edi
	xorl	(%rax,%rdi,4), %r14d
	roll	$8, %r12d
	movl	%ebx, %edi
	movzbl	%bh, %ebx
	xorl	%r12d, %r14d
	shrl	$16, %edi
	movzbl	%dil, %edi
	movl	(%rax,%rdi,4), %r12d
	movl	68(%rdx), %edi
	rorl	$8, %r12d
	xorl	%r11d, %edi
	xorl	%r12d, %r14d
	movl	(%rax,%rbx,4), %r12d
	movl	%r13d, %ebx
	roll	$16, %r12d
	xorl	%r14d, %r12d
	xorl	%r12d, %ebx
	xorl	%ebx, %edi
	xorl	64(%rdx), %ebx
	movzbl	%dil, %r8d
	movl	%edi, %ecx
	movl	(%rax,%r8,4), %r9d
	movl	%edi, %r8d
	shrl	$24, %r8d
	xorl	(%rax,%r8,4), %r10d
	movl	%edi, %r8d
	roll	$8, %r9d
	movzbl	%ch, %edi
	shrl	$16, %r8d
	xorl	%r9d, %r10d
	movzbl	%r8b, %r8d
	movl	(%rax,%r8,4), %r9d
	rorl	$8, %r9d
	xorl	%r9d, %r10d
	movl	(%rax,%rdi,4), %r9d
	roll	$16, %r9d
	xorl	%r10d, %r9d
	xorl	%r9d, %ebx
	movzbl	%bl, %edi
	movl	%ebx, %r8d
	movl	(%rax,%rdi,4), %edi
	shrl	$24, %r8d
	xorl	(%rax,%r8,4), %r11d
	roll	$8, %edi
	xorl	%edi, %r11d
	movl	%ebx, %edi
	movzbl	%bh, %ebx
	shrl	$16, %edi
	movzbl	%dil, %edi
	movl	(%rax,%rdi,4), %edi
	rorl	$8, %edi
	xorl	%edi, %r11d
	movl	(%rax,%rbx,4), %edi
	movl	60(%rdx), %ebx
	roll	$16, %edi
	xorl	%r12d, %ebx
	xorl	%r11d, %edi
	xorl	%r9d, %ebx
	xorl	%edi, %ebx
	movzbl	%bl, %r10d
	movl	%ebx, %r8d
	movl	(%rax,%r10,4), %ebx
	movl	%r8d, %r10d
	movl	%r8d, %ecx
	roll	$8, %ebx
	shrl	$24, %r10d
	xorl	(%rax,%r10,4), %r13d
	movl	%r8d, %r10d
	movl	56(%rdx), %r8d
	shrl	$16, %r10d
	xorl	%ebx, %r13d
	movzbl	%r10b, %r10d
	xorl	%r9d, %r8d
	movl	(%rax,%r10,4), %ebx
	xorl	%edi, %r8d
	rorl	$8, %ebx
	xorl	%ebx, %r13d
	movzbl	%ch, %ebx
	movl	(%rax,%rbx,4), %ebx
	roll	$16, %ebx
	xorl	%r13d, %ebx
	xorl	%ebx, %r8d
	movzbl	%r8b, %r10d
	movl	%r8d, %ecx
	movl	(%rax,%r10,4), %r11d
	movl	%r8d, %r10d
	movzbl	%ch, %ecx
	shrl	$24, %r10d
	xorl	(%rax,%r10,4), %r12d
	movl	%r8d, %r10d
	roll	$8, %r11d
	shrl	$16, %r10d
	xorl	%r11d, %r12d
	movzbl	%r10b, %r10d
	movl	(%rax,%r10,4), %r11d
	movl	52(%rdx), %r10d
	rorl	$8, %r11d
	xorl	%edi, %r10d
	xorl	%r11d, %r12d
	movl	(%rax,%rcx,4), %r11d
	roll	$16, %r11d
	xorl	%r12d, %r11d
	movl	%ebx, %r12d
	xorl	%r11d, %r12d
	xorl	%r12d, %r10d
	movzbl	%r10b, %r8d
	movl	%r10d, %r13d
	movl	%r10d, %ecx
	movl	(%rax,%r8,4), %r8d
	shrl	$24, %r13d
	movzbl	%ch, %ecx
	xorl	(%rax,%r13,4), %r9d
	roll	$8, %r8d
	xorl	%r8d, %r9d
	movl	%r10d, %r8d
	shrl	$16, %r8d
	movzbl	%r8b, %r8d
	movl	(%rax,%r8,4), %r8d
	rorl	$8, %r8d
	xorl	%r8d, %r9d
	movl	(%rax,%rcx,4), %r8d
	roll	$16, %r8d
	xorl	%r9d, %r8d
	xorl	48(%rdx), %r12d
	xorl	%r8d, %r12d
	movzbl	%r12b, %r9d
	movl	%r12d, %ecx
	movl	(%rax,%r9,4), %r10d
	movl	%r12d, %r9d
	movzbl	%ch, %ecx
	shrl	$24, %r9d
	xorl	(%rax,%r9,4), %edi
	movl	%r12d, %r9d
	roll	$8, %r10d
	shrl	$16, %r9d
	xorl	%r10d, %edi
	movzbl	%r9b, %r9d
	movl	(%rax,%r9,4), %r10d
	rorl	$8, %r10d
	xorl	%r10d, %edi
	movl	(%rax,%rcx,4), %r10d
	roll	$16, %r10d
	xorl	%edi, %r10d
	movl	44(%rdx), %edi
	xorl	%r11d, %edi
	xorl	%r8d, %edi
	xorl	%r10d, %edi
	movl	%edi, %r9d
	movzbl	%dil, %edi
	movl	(%rax,%rdi,4), %edi
	movl	%r9d, %r12d
	movl	%r9d, %ecx
	shrl	$24, %r12d
	xorl	(%rax,%r12,4), %ebx
	roll	$8, %edi
	xorl	%edi, %ebx
	movl	%r9d, %edi
	shrl	$16, %edi
	movzbl	%dil, %edi
	movl	(%rax,%rdi,4), %edi
	rorl	$8, %edi
	xorl	%edi, %ebx
	movzbl	%ch, %edi
	movl	(%rax,%rdi,4), %edi
	roll	$16, %edi
	xorl	%ebx, %edi
	movl	40(%rdx), %ebx
	xorl	%r8d, %ebx
	xorl	%r10d, %ebx
	xorl	%edi, %ebx
	movzbl	%bl, %r9d
	movl	(%rax,%r9,4), %r13d
	movl	%ebx, %r9d
	shrl	$24, %r9d
	xorl	(%rax,%r9,4), %r11d
	movl	%ebx, %r9d
	roll	$8, %r13d
	movzbl	%bh, %ebx
	shrl	$16, %r9d
	xorl	%r13d, %r11d
	movzbl	%r9b, %r9d
	movl	(%rax,%r9,4), %r13d
	movl	36(%rdx), %r9d
	rorl	$8, %r13d
	xorl	%r13d, %r11d
	movl	(%rax,%rbx,4), %r13d
	movl	%edi, %ebx
	xorl	%r10d, %r9d
	roll	$16, %r13d
	xorl	%r11d, %r13d
	xorl	%r13d, %ebx
	xorl	%ebx, %r9d
	xorl	32(%rdx), %ebx
	movzbl	%r9b, %r11d
	movl	%r9d, %ecx
	movl	(%rax,%r11,4), %r12d
	movl	%r9d, %r11d
	movzbl	%ch, %ecx
	shrl	$24, %r11d
	xorl	(%rax,%r11,4), %r8d
	movl	%r9d, %r11d
	roll	$8, %r12d
	shrl	$16, %r11d
	xorl	%r12d, %r8d
	movzbl	%r11b, %r11d
	movl	(%rax,%r11,4), %r12d
	rorl	$8, %r12d
	xorl	%r12d, %r8d
	movl	(%rax,%rcx,4), %r12d
	roll	$16, %r12d
	xorl	%r8d, %r12d
	xorl	%r12d, %ebx
	movzbl	%bl, %r8d
	movl	(%rax,%r8,4), %r9d
	movl	%ebx, %r8d
	shrl	$24, %r8d
	xorl	(%rax,%r8,4), %r10d
	movl	%ebx, %r8d
	roll	$8, %r9d
	movzbl	%bh, %ebx
	shrl	$16, %r8d
	xorl	%r9d, %r10d
	movzbl	%r8b, %r8d
	movl	(%rax,%r8,4), %r9d
	rorl	$8, %r9d
	xorl	%r9d, %r10d
	movl	(%rax,%rbx,4), %r9d
	movl	28(%rdx), %ebx
	roll	$16, %r9d
	xorl	%r13d, %ebx
	xorl	%r10d, %r9d
	xorl	%r12d, %ebx
	xorl	%r9d, %ebx
	movl	%ebx, %r8d
	movzbl	%bl, %r10d
	movl	(%rax,%r10,4), %ebx
	movl	%r8d, %r10d
	movl	%r8d, %ecx
	shrl	$24, %r10d
	xorl	(%rax,%r10,4), %edi
	roll	$8, %ebx
	movl	%r8d, %r10d
	xorl	%ebx, %edi
	shrl	$16, %r10d
	movzbl	%r10b, %r10d
	movl	(%rax,%r10,4), %ebx
	movl	20(%rdx), %r10d
	rorl	$8, %ebx
	xorl	%r9d, %r10d
	xorl	%ebx, %edi
	movzbl	%ch, %ebx
	movl	(%rax,%rbx,4), %ebx
	roll	$16, %ebx
	xorl	%edi, %ebx
	movl	24(%rdx), %edi
	xorl	%r12d, %edi
	xorl	%r9d, %edi
	xorl	%ebx, %edi
	movzbl	%dil, %r8d
	movl	%edi, %ecx
	movl	(%rax,%r8,4), %r11d
	movl	%edi, %r8d
	shrl	$24, %r8d
	xorl	(%rax,%r8,4), %r13d
	movl	%edi, %r8d
	roll	$8, %r11d
	movzbl	%ch, %edi
	shrl	$16, %r8d
	xorl	%r11d, %r13d
	movzbl	%r8b, %r8d
	movl	(%rax,%r8,4), %r11d
	rorl	$8, %r11d
	xorl	%r11d, %r13d
	movl	(%rax,%rdi,4), %r11d
	movl	%ebx, %edi
	roll	$16, %r11d
	xorl	%r13d, %r11d
	xorl	%r11d, %edi
	xorl	%edi, %r10d
	xorl	16(%rdx), %edi
	movzbl	%r10b, %r8d
	movl	%r10d, %r13d
	movl	%r10d, %ecx
	movl	(%rax,%r8,4), %r8d
	shrl	$24, %r13d
	movzbl	%ch, %ecx
	xorl	(%rax,%r13,4), %r12d
	roll	$8, %r8d
	xorl	%r8d, %r12d
	movl	%r10d, %r8d
	shrl	$16, %r8d
	movzbl	%r8b, %r8d
	movl	(%rax,%r8,4), %r8d
	rorl	$8, %r8d
	xorl	%r8d, %r12d
	movl	(%rax,%rcx,4), %r8d
	roll	$16, %r8d
	xorl	%r12d, %r8d
	xorl	%r8d, %edi
	movzbl	%dil, %r10d
	movl	%edi, %r12d
	movl	%edi, %ecx
	movl	(%rax,%r10,4), %r10d
	roll	$8, %r10d
	shrl	$24, %r12d
	xorl	(%rax,%r12,4), %r9d
	xorl	%r10d, %r9d
	movl	%edi, %r10d
	movzbl	%ch, %edi
	shrl	$16, %r10d
	movzbl	%r10b, %r10d
	movl	(%rax,%r10,4), %r10d
	rorl	$8, %r10d
	xorl	%r10d, %r9d
	movl	(%rax,%rdi,4), %r10d
	movl	12(%rdx), %eax
	roll	$16, %r10d
	xorl	%r11d, %eax
	xorl	%r9d, %r10d
	xorl	%r8d, %eax
	xorl	%r10d, %eax
	movl	%eax, %edi
	shrl	$24, %edi
	movzbl	(%r15,%rdi), %r9d
	movzbl	%al, %edi
	movzbl	(%r15,%rdi), %edi
	sall	$24, %r9d
	orl	%edi, %r9d
	movl	%eax, %edi
	movzbl	%ah, %eax
	shrl	$16, %edi
	movzbl	(%r15,%rax), %eax
	movzbl	%dil, %edi
	movzbl	(%r15,%rdi), %edi
	sall	$8, %eax
	sall	$16, %edi
	orl	%edi, %r9d
	orl	%eax, %r9d
	movl	%r9d, %eax
	xorl	%r9d, %ebx
	movl	%r9d, %edi
	roll	$2, %eax
	roll	$10, %edi
	xorl	%ebx, %eax
	xorl	%edi, %eax
	movl	%r9d, %edi
	rorl	$14, %edi
	xorl	%eax, %edi
	movl	%r9d, %eax
	rorl	$8, %eax
	xorl	%edi, %eax
	movl	8(%rdx), %edi
	xorl	%r8d, %edi
	xorl	%r10d, %edi
	movl	%edi, %ebx
	xorl	%eax, %ebx
	movl	%ebx, %edi
	shrl	$24, %edi
	movzbl	(%r15,%rdi), %r9d
	movzbl	%bl, %edi
	movzbl	(%r15,%rdi), %edi
	sall	$24, %r9d
	orl	%edi, %r9d
	movl	%ebx, %edi
	movzbl	%bh, %ebx
	shrl	$16, %edi
	movzbl	%dil, %edi
	movzbl	(%r15,%rdi), %edi
	sall	$16, %edi
	orl	%edi, %r9d
	movzbl	(%r15,%rbx), %edi
	movl	4(%rdx), %ebx
	sall	$8, %edi
	xorl	%r10d, %ebx
	orl	%edi, %r9d
	movl	%r9d, %edi
	xorl	%r9d, %r11d
	roll	$2, %edi
	xorl	%r11d, %edi
	movl	%r9d, %r11d
	roll	$10, %r11d
	xorl	%r11d, %edi
	movl	%r9d, %r11d
	rorl	$14, %r11d
	xorl	%edi, %r11d
	movl	%r9d, %edi
	rorl	$8, %edi
	xorl	%r11d, %edi
	movl	%eax, %r11d
	bswap	%eax
	xorl	%edi, %r11d
	bswap	%edi
	xorl	%r11d, %ebx
	movl	%ebx, %r9d
	movzbl	%bl, %r12d
	shrl	$24, %r9d
	movzbl	(%r15,%r12), %r12d
	movzbl	(%r15,%r9), %r9d
	sall	$24, %r9d
	orl	%r12d, %r9d
	movl	%ebx, %r12d
	movzbl	%bh, %ebx
	shrl	$16, %r12d
	movzbl	(%r15,%rbx), %ebx
	movzbl	%r12b, %r12d
	movzbl	(%r15,%r12), %r12d
	sall	$8, %ebx
	sall	$16, %r12d
	orl	%r12d, %r9d
	orl	%ebx, %r9d
	movl	%r9d, %ebx
	xorl	%r9d, %r8d
	roll	$2, %ebx
	xorl	%ebx, %r8d
	movl	%r9d, %ebx
	roll	$10, %ebx
	xorl	%ebx, %r8d
	movl	%r9d, %ebx
	xorl	(%rdx), %r11d
	rorl	$8, %r9d
	rorl	$14, %ebx
	movl	%edi, 8(%rsi)
	xorl	%ebx, %r8d
	movl	%r11d, %ebx
	movl	%eax, 12(%rsi)
	xorl	%r8d, %r9d
	xorl	%r9d, %ebx
	bswap	%r9d
	movl	%r9d, 4(%rsi)
	movl	%ebx, %edx
	movzbl	%bl, %r8d
	shrl	$24, %edx
	movzbl	(%r15,%r8), %r8d
	movzbl	(%r15,%rdx), %edx
	sall	$24, %edx
	orl	%r8d, %edx
	movl	%ebx, %r8d
	movzbl	%bh, %ebx
	shrl	$16, %r8d
	movzbl	(%r15,%rbx), %ecx
	popq	%rbx
	movzbl	%r8b, %r8d
	popq	%r12
	popq	%r13
	movzbl	(%r15,%r8), %r8d
	sall	$8, %ecx
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	sall	$16, %r8d
	orl	%r8d, %edx
	orl	%ecx, %edx
	movl	%edx, %ecx
	xorl	%edx, %r10d
	roll	$2, %ecx
	xorl	%ecx, %r10d
	movl	%edx, %ecx
	roll	$10, %ecx
	xorl	%ecx, %r10d
	movl	%edx, %ecx
	rorl	$8, %edx
	rorl	$14, %ecx
	xorl	%ecx, %r10d
	xorl	%r10d, %edx
	bswap	%edx
	movl	%edx, (%rsi)
	ret
	.cfi_endproc
.LFE11:
	.size	SM4_decrypt, .-SM4_decrypt
	.section	.rodata
	.align 32
	.type	CK.2108, @object
	.size	CK.2108, 128
CK.2108:
	.long	462357
	.long	472066609
	.long	943670861
	.long	1415275113
	.long	1886879365
	.long	-1936483679
	.long	-1464879427
	.long	-993275175
	.long	-521670923
	.long	-66909679
	.long	404694573
	.long	876298825
	.long	1347903077
	.long	1819507329
	.long	-2003855715
	.long	-1532251463
	.long	-1060647211
	.long	-589042959
	.long	-117504499
	.long	337322537
	.long	808926789
	.long	1280531041
	.long	1752135293
	.long	-2071227751
	.long	-1599623499
	.long	-1128019247
	.long	-656414995
	.long	-184876535
	.long	269950501
	.long	741554753
	.long	1213159005
	.long	1684763257
	.align 32
	.type	SM4_SBOX_T, @object
	.size	SM4_SBOX_T, 1024
SM4_SBOX_T:
	.long	-1898620069
	.long	-795721150
	.long	1307223975
	.long	117308411
	.long	-53529805
	.long	1709344647
	.long	-918686476
	.long	1807081182
	.long	1310087256
	.long	1857346266
	.long	1142181968
	.long	-893318389
	.long	-2010603360
	.long	402190319
	.long	-1674792784
	.long	285545492
	.long	-2027180884
	.long	-77161059
	.long	-224892310
	.long	-1367877159
	.long	-2111133528
	.long	1186790138
	.long	335810576
	.long	-809496817
	.long	44608170
	.long	1413812497
	.long	1595100236
	.long	-1104766824
	.long	1833444645
	.long	-1635509734
	.long	503715864
	.long	-40147354
	.long	-325160334
	.long	1245907209
	.long	273760577
	.long	620221395
	.long	-711768506
	.long	1408024511
	.long	-124099998
	.long	-1837372951
	.long	-13382452
	.long	72700241
	.long	655043628
	.long	1329728781
	.long	1508816823
	.long	-204718273
	.long	481211058
	.long	-362575479
	.long	1961333651
	.long	2142359246
	.long	1813803120
	.long	229353126
	.long	-305518809
	.long	671621152
	.long	1223402403
	.long	-1047046570
	.long	-2138963454
	.long	-1545830529
	.long	-996781486
	.long	318368747
	.long	-1586178603
	.long	-1282589122
	.long	-1019216644
	.long	1050974874
	.long	1531321629
	.long	453450780
	.long	1000709790
	.long	218100723
	.long	1072746447
	.long	-1082995251
	.long	1259822172
	.long	1387850474
	.long	-1887367666
	.long	1029203301
	.long	-868421392
	.long	2098816100
	.long	2128976795
	.long	-1853417962
	.long	1934507325
	.long	145400482
	.long	-932601439
	.long	-949310035
	.long	-2055010810
	.long	2058406602
	.long	-1250900539
	.long	-194670191
	.long	-1294374037
	.long	-1484181970
	.long	419161059
	.long	1206431663
	.long	856636476
	.long	1732914477
	.long	-1334722111
	.long	240605529
	.long	-375425418
	.long	-516565804
	.long	1713272952
	.long	-1272672112
	.long	906901560
	.long	643791225
	.long	-278753907
	.long	945381729
	.long	-1781381305
	.long	715164298
	.long	-1322937196
	.long	-1440577400
	.long	-1937903119
	.long	-683938580
	.long	83952644
	.long	-1524530044
	.long	-1736842783
	.long	-1685774818
	.long	-2066263213
	.long	0
	.long	1581717785
	.long	190209373
	.long	-476217730
	.long	-1613738161
	.long	-1155031908
	.long	441665865
	.long	2085433649
	.long	-298395432
	.long	167905288
	.long	2078580639
	.long	547521154
	.long	-725150957
	.long	-389340381
	.long	-425952646
	.long	1122610091
	.long	1136525054
	.long	-1568134614
	.long	-1697559733
	.long	1078001921
	.long	-607903969
	.long	-667361056
	.long	1639438038
	.long	799116942
	.long	737468383
	.long	988924875
	.long	-154322117
	.long	502982631
	.long	-446659195
	.long	1091916884
	.long	631473798
	.long	1625523075
	.long	380418746
	.long	693925237
	.long	883331730
	.long	-140939666
	.long	-466300720
	.long	1914333288
	.long	22304085
	.long	430945974
	.long	-544125362
	.long	-97335096
	.long	-265240384
	.long	569825239
	.long	-1131531726
	.long	1974716102
	.long	1876987791
	.long	1763538036
	.long	787864539
	.long	1793166219
	.long	-1775322952
	.long	-1971320310
	.long	-26764903
	.long	-490132693
	.long	-530480767
	.long	-1060961533
	.long	-1926650716
	.long	-1356624756
	.long	128560814
	.long	957166644
	.long	525487437
	.long	1984903481
	.long	-747717187
	.long	-2116659369
	.long	-1210552465
	.long	-348660516
	.long	1363416341
	.long	-1495434373
	.long	167704567
	.long	-1232324038
	.long	-1825588036
	.long	251857932
	.long	66912255
	.long	-1033131607
	.long	-1166816823
	.long	-647187019
	.long	-596790863
	.long	928673133
	.long	357582149
	.long	-1181796810
	.long	1998285932
	.long	330153662
	.long	-628078006
	.long	1471803118
	.long	-1445038217
	.long	1287582450
	.long	-2088829443
	.long	1427194948
	.long	-1109760153
	.long	744321393
	.long	1161823493
	.long	1663007868
	.long	1343242304
	.long	844851561
	.long	-1193581725
	.long	571090984
	.long	-977139961
	.long	-181287740
	.long	-1467342302
	.long	833066646
	.long	-103925961
	.long	-1753551379
	.long	1237317366
	.long	-1725057868
	.long	-1535782447
	.long	-1865202877
	.long	1511147592
	.long	1488642786
	.long	1910937495
	.long	1689703122
	.long	1890763458
	.long	-1383389658
	.long	-848779867
	.long	-879403426
	.long	1649092905
	.long	1007431728
	.long	-829138342
	.long	-1418273315
	.long	-2038433287
	.long	-245066347
	.long	1572595430
	.long	905103303
	.long	755573796
	.long	-775547113
	.long	-697321031
	.long	-557507813
	.long	-1803152878
	.long	2014863456
	.long	821281731
	.long	-1988299275
	.long	1559212979
	.long	-767891224
	.long	-1394642061
	.long	2035037493
	.long	-1608482688
	.long	-1653021211
	.long	1458420667
	.long	593395069
	.long	-968951560
	.long	-1949016225
	.long	-406311121
	.long	-583408412
	.long	1749623073
	.align 32
	.type	SM4_S, @object
	.size	SM4_S, 256
SM4_S:
	.string	"\326\220\351\376\314\341=\267\026\266\024\302(\373,\005+g\232v*\276\004\303\252D\023&I\206\006\231\234BP\364\221\357\230z3T\013C\355\317\254b\344\263\034\251\311\b\350\225\200\337\224\372u\217?\246G\007\247\374\363s\027\272\203Y<\031\346\205O\250hk\201\262qd\332\213\370\353\017KpV\2355\036$\016^cX\321\242%\"|;\001!x\207\324"
	.ascii	"FW\237\323'RL6\002\347\240\304\310\236\352\277\212\322@\3078"
	.ascii	"\265\243\367\362\316\371a\025\241\340\256]\244\2334\032U\255"
	.ascii	"\22320\365\214\261\343\035\366\342.\202f\312`\300)#\253\rSNo"
	.ascii	"\325\3337E\336\375\216/\003\377jrml[Q\215\033\257\222\273\335"
	.ascii	"\274\177\021\331\\A\037\020Z\330\n\3011\210\245\315{\275-t\320"
	.ascii	"\022\270\345\264\260\211i\227J\f\226w~e\271\361\t\305n\306\204"
	.ascii	"\030\360}\354:\334M y\356_>\327\3139H"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
