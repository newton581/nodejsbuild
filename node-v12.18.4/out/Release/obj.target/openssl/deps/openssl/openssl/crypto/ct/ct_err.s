	.file	"ct_err.c"
	.text
	.p2align 4
	.globl	ERR_load_CT_strings
	.type	ERR_load_CT_strings, @function
ERR_load_CT_strings:
.LFB203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$839340032, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_func_error_string@PLT
	testq	%rax, %rax
	je	.L5
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	leaq	CT_str_functs(%rip), %rdi
	call	ERR_load_strings_const@PLT
	leaq	CT_str_reasons(%rip), %rdi
	call	ERR_load_strings_const@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE203:
	.size	ERR_load_CT_strings, .-ERR_load_CT_strings
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"base64 decode error"
.LC1:
	.string	"invalid log id length"
.LC2:
	.string	"log conf invalid"
.LC3:
	.string	"log conf invalid key"
.LC4:
	.string	"log conf missing description"
.LC5:
	.string	"log conf missing key"
.LC6:
	.string	"log key invalid"
.LC7:
	.string	"sct future timestamp"
.LC8:
	.string	"sct invalid"
.LC9:
	.string	"sct invalid signature"
.LC10:
	.string	"sct list invalid"
.LC11:
	.string	"sct log id mismatch"
.LC12:
	.string	"sct not set"
.LC13:
	.string	"sct unsupported version"
.LC14:
	.string	"unrecognized signature nid"
.LC15:
	.string	"unsupported entry type"
.LC16:
	.string	"unsupported version"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	CT_str_reasons, @object
	.size	CT_str_reasons, 288
CT_str_reasons:
	.quad	838860908
	.quad	.LC0
	.quad	838860900
	.quad	.LC1
	.quad	838860909
	.quad	.LC2
	.quad	838860910
	.quad	.LC3
	.quad	838860911
	.quad	.LC4
	.quad	838860912
	.quad	.LC5
	.quad	838860913
	.quad	.LC6
	.quad	838860916
	.quad	.LC7
	.quad	838860904
	.quad	.LC8
	.quad	838860907
	.quad	.LC9
	.quad	838860905
	.quad	.LC10
	.quad	838860914
	.quad	.LC11
	.quad	838860906
	.quad	.LC12
	.quad	838860915
	.quad	.LC13
	.quad	838860901
	.quad	.LC14
	.quad	838860902
	.quad	.LC15
	.quad	838860903
	.quad	.LC16
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC17:
	.string	"CTLOG_new"
.LC18:
	.string	"CTLOG_new_from_base64"
.LC19:
	.string	"ctlog_new_from_conf"
.LC20:
	.string	"ctlog_store_load_ctx_new"
.LC21:
	.string	"CTLOG_STORE_load_file"
.LC22:
	.string	"ctlog_store_load_log"
.LC23:
	.string	"CTLOG_STORE_new"
.LC24:
	.string	"ct_base64_decode"
.LC25:
	.string	"CT_POLICY_EVAL_CTX_new"
.LC26:
	.string	"ct_v1_log_id_from_pkey"
.LC27:
	.string	"i2o_SCT"
.LC28:
	.string	"i2o_SCT_LIST"
.LC29:
	.string	"i2o_SCT_signature"
.LC30:
	.string	"o2i_SCT"
.LC31:
	.string	"o2i_SCT_LIST"
.LC32:
	.string	"o2i_SCT_signature"
.LC33:
	.string	"SCT_CTX_new"
.LC34:
	.string	"SCT_CTX_verify"
.LC35:
	.string	"SCT_new"
.LC36:
	.string	"SCT_new_from_base64"
.LC37:
	.string	"SCT_set0_log_id"
.LC38:
	.string	"SCT_set1_extensions"
.LC39:
	.string	"SCT_set1_log_id"
.LC40:
	.string	"SCT_set1_signature"
.LC41:
	.string	"SCT_set_log_entry_type"
.LC42:
	.string	"SCT_set_signature_nid"
.LC43:
	.string	"SCT_set_version"
	.section	.data.rel.ro.local
	.align 32
	.type	CT_str_functs, @object
	.size	CT_str_functs, 448
CT_str_functs:
	.quad	839340032
	.quad	.LC17
	.quad	839344128
	.quad	.LC18
	.quad	839348224
	.quad	.LC19
	.quad	839360512
	.quad	.LC20
	.quad	839364608
	.quad	.LC21
	.quad	839393280
	.quad	.LC22
	.quad	839397376
	.quad	.LC23
	.quad	839368704
	.quad	.LC24
	.quad	839405568
	.quad	.LC25
	.quad	839372800
	.quad	.LC26
	.quad	839299072
	.quad	.LC27
	.quad	839303168
	.quad	.LC28
	.quad	839307264
	.quad	.LC29
	.quad	839311360
	.quad	.LC30
	.quad	839315456
	.quad	.LC31
	.quad	839319552
	.quad	.LC32
	.quad	839376896
	.quad	.LC33
	.quad	839385088
	.quad	.LC34
	.quad	839270400
	.quad	.LC35
	.quad	839380992
	.quad	.LC36
	.quad	839274496
	.quad	.LC37
	.quad	839327744
	.quad	.LC38
	.quad	839331840
	.quad	.LC39
	.quad	839335936
	.quad	.LC40
	.quad	839278592
	.quad	.LC41
	.quad	839282688
	.quad	.LC42
	.quad	839286784
	.quad	.LC43
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
