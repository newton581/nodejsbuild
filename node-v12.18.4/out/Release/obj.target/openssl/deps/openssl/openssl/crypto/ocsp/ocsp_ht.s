	.file	"ocsp_ht.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ocsp/ocsp_ht.c"
	.text
	.p2align 4
	.globl	OCSP_REQ_CTX_new
	.type	OCSP_REQ_CTX_new, @function
OCSP_REQ_CTX_new:
.LFB1366:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$66, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	%esi, %ebx
	leaq	.LC0(%rip), %rsi
	subq	$16, %rsp
	movq	%rdi, -24(%rbp)
	movl	$56, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1
	movl	$4096, (%rax)
	movq	$102400, 48(%rax)
	call	BIO_s_mem@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	-24(%rbp), %xmm0
	movslq	%ebx, %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 24(%r12)
	testl	%ebx, %ebx
	jle	.L12
.L3:
	movl	%ebx, 16(%r12)
	movl	$78, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	32(%r12), %rdi
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L5
	testq	%rdi, %rdi
	je	.L5
.L1:
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	movl	$4096, %edi
	movl	$4096, %ebx
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L5:
	call	BIO_free@PLT
	movq	8(%r12), %rdi
	movl	$91, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movl	$92, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1366:
	.size	OCSP_REQ_CTX_new, .-OCSP_REQ_CTX_new
	.p2align 4
	.globl	OCSP_REQ_CTX_free
	.type	OCSP_REQ_CTX_free, @function
OCSP_REQ_CTX_free:
.LFB1367:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L13
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	32(%rdi), %rdi
	call	BIO_free@PLT
	movq	8(%r12), %rdi
	movl	$91, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$92, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L13:
	ret
	.cfi_endproc
.LFE1367:
	.size	OCSP_REQ_CTX_free, .-OCSP_REQ_CTX_free
	.p2align 4
	.globl	OCSP_REQ_CTX_get0_mem_bio
	.type	OCSP_REQ_CTX_get0_mem_bio, @function
OCSP_REQ_CTX_get0_mem_bio:
.LFB1368:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE1368:
	.size	OCSP_REQ_CTX_get0_mem_bio, .-OCSP_REQ_CTX_get0_mem_bio
	.p2align 4
	.globl	OCSP_set_max_response_length
	.type	OCSP_set_max_response_length, @function
OCSP_set_max_response_length:
.LFB1369:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	movl	$102400, %eax
	cmove	%rax, %rsi
	movq	%rsi, 48(%rdi)
	ret
	.cfi_endproc
.LFE1369:
	.size	OCSP_set_max_response_length, .-OCSP_set_max_response_length
	.p2align 4
	.globl	OCSP_REQ_CTX_i2d
	.type	OCSP_REQ_CTX_i2d, @function
OCSP_REQ_CTX_i2d:
.LFB1370:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	movq	%rsi, %rdx
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$8, %rsp
	call	ASN1_item_i2d@PLT
	movq	32(%rbx), %rdi
	leaq	req_hdr.21788(%rip), %rsi
	movl	%eax, %edx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L24
	movq	32(%rbx), %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	ASN1_item_i2d_bio@PLT
	testl	%eax, %eax
	jle	.L24
	movl	$4101, (%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1370:
	.size	OCSP_REQ_CTX_i2d, .-OCSP_REQ_CTX_i2d
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"/"
	.text
	.p2align 4
	.globl	OCSP_REQ_CTX_http
	.type	OCSP_REQ_CTX_http, @function
OCSP_REQ_CTX_http:
.LFB1372:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rcx
	leaq	.LC1(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rdx, %rdx
	movq	32(%rdi), %rdi
	movq	%rsi, %rdx
	cmove	%rax, %rcx
	leaq	http_hdr.21803(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L26
	movl	$4105, (%rbx)
	movl	$1, %eax
.L26:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1372:
	.size	OCSP_REQ_CTX_http, .-OCSP_REQ_CTX_http
	.p2align 4
	.globl	OCSP_REQ_CTX_set1_req
	.type	OCSP_REQ_CTX_set1_req, @function
OCSP_REQ_CTX_set1_req:
.LFB1373:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	OCSP_REQUEST_it(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	ASN1_item_i2d@PLT
	movq	32(%rbx), %rdi
	leaq	req_hdr.21788(%rip), %rsi
	movl	%eax, %edx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L35
	movq	32(%rbx), %rsi
	movq	%r12, %rdx
	leaq	OCSP_REQUEST_it(%rip), %rdi
	call	ASN1_item_i2d_bio@PLT
	testl	%eax, %eax
	jle	.L35
	movl	$4101, (%rbx)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1373:
	.size	OCSP_REQ_CTX_set1_req, .-OCSP_REQ_CTX_set1_req
	.section	.rodata.str1.1
.LC2:
	.string	": "
.LC3:
	.string	"\r\n"
	.text
	.p2align 4
	.globl	OCSP_REQ_CTX_add1_header
	.type	OCSP_REQ_CTX_add1_header, @function
OCSP_REQ_CTX_add1_header:
.LFB1374:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L48
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	32(%rdi), %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L40
	movq	32(%rbx), %rdi
	testq	%r12, %r12
	je	.L41
	movl	$2, %edx
	leaq	.LC2(%rip), %rsi
	call	BIO_write@PLT
	cmpl	$2, %eax
	je	.L49
.L40:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	movq	32(%rbx), %rdi
	movq	%r12, %rsi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L40
	movq	32(%rbx), %rdi
.L41:
	movl	$2, %edx
	leaq	.LC3(%rip), %rsi
	call	BIO_write@PLT
	cmpl	$2, %eax
	jne	.L40
	movl	$4105, (%rbx)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1374:
	.size	OCSP_REQ_CTX_add1_header, .-OCSP_REQ_CTX_add1_header
	.section	.rodata.str1.1
.LC4:
	.string	"POST"
	.text
	.p2align 4
	.globl	OCSP_sendreq_new
	.type	OCSP_sendreq_new, @function
OCSP_sendreq_new:
.LFB1375:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	movl	%ecx, %esi
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	call	OCSP_REQ_CTX_new
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L50
	testq	%r14, %r14
	leaq	.LC1(%rip), %rax
	movq	32(%r12), %rdi
	leaq	.LC4(%rip), %rdx
	cmove	%rax, %r14
	leaq	http_hdr.21803(%rip), %rsi
	xorl	%eax, %eax
	movq	%r14, %rcx
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L53
	movl	$4105, (%r12)
	testq	%r13, %r13
	je	.L50
	xorl	%esi, %esi
	leaq	OCSP_REQUEST_it(%rip), %rdx
	movq	%r13, %rdi
	call	ASN1_item_i2d@PLT
	movq	32(%r12), %rdi
	leaq	req_hdr.21788(%rip), %rsi
	movl	%eax, %edx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L53
	movq	32(%r12), %rsi
	movq	%r13, %rdx
	leaq	OCSP_REQUEST_it(%rip), %rdi
	call	ASN1_item_i2d_bio@PLT
	testl	%eax, %eax
	jle	.L53
	movl	$4101, (%r12)
.L50:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movq	32(%r12), %rdi
	call	BIO_free@PLT
	movq	8(%r12), %rdi
	movl	$91, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movl	$92, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1375:
	.size	OCSP_sendreq_new, .-OCSP_sendreq_new
	.section	.rodata.str1.1
.LC5:
	.string	"Code="
.LC6:
	.string	",Reason="
	.text
	.p2align 4
	.globl	OCSP_REQ_CTX_nbio
	.type	OCSP_REQ_CTX_nbio, @function
OCSP_REQ_CTX_nbio:
.LFB1377:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-64(%rbp), %rcx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-72(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	.L72(%rip), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rcx, -96(%rbp)
	movl	(%rdi), %eax
.L62:
	testb	$16, %ah
	je	.L81
	cmpl	$4105, %eax
	jg	.L123
.L180:
	cmpl	$4100, %eax
	jle	.L177
	subl	$4102, %eax
	cmpl	$3, %eax
	ja	.L70
	movslq	(%r12,%rax,4), %rax
	addq	%r12, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L72:
	.long	.L74-.L72
	.long	.L73-.L72
	.long	.L124-.L72
	.long	.L71-.L72
	.text
.L124:
	movl	$1, %eax
.L61:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L178
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L71:
	.cfi_restore_state
	movq	32(%rbx), %rdi
	movl	$2, %edx
	leaq	.LC3(%rip), %rsi
	call	BIO_write@PLT
	cmpl	$2, %eax
	jne	.L78
	movl	$4101, (%rbx)
.L70:
	movq	32(%rbx), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$3, %esi
	call	BIO_ctrl@PLT
	movl	$4102, (%rbx)
	movq	%rax, 40(%rbx)
.L74:
	movq	32(%rbx), %rdi
	xorl	%edx, %edx
	movl	$3, %esi
	movq	%r14, %rcx
	call	BIO_ctrl@PLT
	movq	40(%rbx), %rdx
	movq	24(%rbx), %rdi
	movslq	%eax, %rsi
	subq	%rdx, %rsi
	addq	-72(%rbp), %rsi
	call	BIO_write@PLT
	testl	%eax, %eax
	jle	.L175
	cltq
	subq	%rax, 40(%rbx)
	jne	.L79
	movl	$4103, (%rbx)
	movq	32(%rbx), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %esi
	call	BIO_ctrl@PLT
.L73:
	movq	24(%rbx), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	testl	%eax, %eax
	jle	.L175
	movl	$1, (%rbx)
.L81:
	movl	16(%rbx), %edx
	movq	8(%rbx), %rsi
	movq	24(%rbx), %rdi
	call	BIO_read@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jle	.L179
	movq	8(%rbx), %rsi
	movq	32(%rbx), %rdi
	movl	%eax, %edx
	call	BIO_write@PLT
	cmpl	%r13d, %eax
	jne	.L123
	movl	(%rbx), %eax
	cmpl	$4105, %eax
	jle	.L180
	.p2align 4,,10
	.p2align 3
.L123:
	xorl	%eax, %eax
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L177:
	cmpl	$3, %eax
	je	.L67
	jle	.L181
	cmpl	$4, %eax
	jne	.L123
.L75:
	movq	32(%rbx), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$3, %esi
	call	BIO_ctrl@PLT
	cmpl	%eax, 40(%rbx)
	jle	.L119
.L79:
	movl	(%rbx), %eax
	jmp	.L62
.L181:
	subl	$1, %eax
	cmpl	$1, %eax
	ja	.L123
.L69:
	movq	32(%rbx), %rdi
	movq	%r14, %rcx
	xorl	%edx, %edx
	movl	$3, %esi
	call	BIO_ctrl@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jle	.L182
	movq	-72(%rbp), %rdi
	movslq	%eax, %rdx
	movl	$10, %esi
	call	memchr@PLT
	movl	16(%rbx), %edx
	testq	%rax, %rax
	je	.L84
	movq	8(%rbx), %rsi
	movq	32(%rbx), %rdi
	call	BIO_gets@PLT
	testl	%eax, %eax
	jle	.L183
	cmpl	%eax, 16(%rbx)
	je	.L78
	cmpl	$1, (%rbx)
	movq	8(%rbx), %r15
	je	.L176
	movq	%r15, -72(%rbp)
	movzbl	(%r15), %edx
	testb	%dl, %dl
	je	.L110
	leaq	1(%r15), %rax
	.p2align 4,,10
	.p2align 3
.L112:
	cmpb	$13, %dl
	je	.L127
	cmpb	$10, %dl
	jne	.L69
.L127:
	movq	%rax, -72(%rbp)
	movzbl	(%rax), %edx
	addq	$1, %rax
	testb	%dl, %dl
	jne	.L112
.L110:
	movl	$3, (%rbx)
.L67:
	movq	32(%rbx), %rdi
	xorl	%edx, %edx
	movq	%r14, %rcx
	movl	$3, %esi
	call	BIO_ctrl@PLT
	cmpl	$1, %eax
	jle	.L79
	movq	-72(%rbp), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, -72(%rbp)
	cmpb	$48, (%rcx)
	jne	.L78
	movzbl	1(%rcx), %edx
	testb	%dl, %dl
	js	.L184
	leaq	2(%rdx), %rax
	movq	%rax, 40(%rbx)
.L118:
	movl	$4, (%rbx)
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L91:
	addq	$1, %r15
.L176:
	movsbl	(%r15), %edi
	testb	%dil, %dil
	je	.L92
	movl	$8, %esi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	je	.L91
	movsbl	(%r15), %edi
	testb	%dil, %dil
	jne	.L93
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L95:
	movsbl	1(%r15), %edi
	addq	$1, %r15
	testb	%dil, %dil
	je	.L94
.L93:
	movl	$8, %esi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	jne	.L95
	movsbl	(%r15), %edi
	testb	%dil, %dil
	je	.L94
	movq	%r15, %rcx
.L96:
	movl	$8, %esi
	movq	%rcx, -88(%rbp)
	call	ossl_ctype_check@PLT
	movq	-88(%rbp), %rcx
	testl	%eax, %eax
	je	.L98
	cmpb	$0, (%rcx)
	je	.L97
	movb	$0, (%rcx)
	movq	-96(%rbp), %rsi
	movl	$10, %edx
	movq	%r15, %rdi
	movq	%rcx, -104(%rbp)
	call	strtoul@PLT
	movq	%rax, -88(%rbp)
	movq	-64(%rbp), %rax
	cmpb	$0, (%rax)
	jne	.L78
	movq	-104(%rbp), %rcx
	movsbl	1(%rcx), %edi
	leaq	1(%rcx), %r13
	testb	%dil, %dil
	jne	.L101
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L103:
	movsbl	1(%r13), %edi
	addq	$1, %r13
	testb	%dil, %dil
	je	.L104
.L101:
	movl	$8, %esi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	jne	.L103
	cmpb	$0, 0(%r13)
	je	.L104
	movq	%r13, %rdi
	call	strlen@PLT
	leaq	-1(%r13,%rax), %rax
	movq	%rax, -64(%rbp)
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L106:
	movq	-64(%rbp), %rax
	movb	$0, (%rax)
	movq	-64(%rbp), %rax
	subq	$1, %rax
	movq	%rax, -64(%rbp)
.L105:
	movsbl	(%rax), %edi
	movl	$8, %esi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	jne	.L106
.L104:
	cmpl	$200, -88(%rbp)
	jne	.L185
	movl	$2, (%rbx)
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L98:
	movsbl	1(%rcx), %edi
	addq	$1, %rcx
	testb	%dil, %dil
	jne	.L96
	.p2align 4,,10
	.p2align 3
.L97:
	movl	$233, %r8d
.L173:
	movl	$115, %edx
	movl	$118, %esi
	movl	$39, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L78:
	movl	$4096, (%rbx)
	xorl	%eax, %eax
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L92:
	movl	$215, %r8d
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L94:
	movl	$224, %r8d
	jmp	.L173
.L184:
	cmpl	$5, %eax
	jle	.L79
	movl	%edx, %eax
	andl	$127, %edx
	andl	$127, %eax
	leal	-1(%rax), %esi
	cmpb	$3, %sil
	ja	.L78
	leaq	2(%rcx), %rsi
	movq	$0, 40(%rbx)
	movq	%rsi, -72(%rbp)
	testb	%al, %al
	je	.L126
	leaq	3(%rcx), %rax
	movq	%rax, -72(%rbp)
	movzbl	2(%rcx), %esi
	movq	%rsi, 40(%rbx)
	cmpl	$1, %edx
	je	.L117
	movq	%rsi, %rax
	leaq	4(%rcx), %rsi
	salq	$8, %rax
	movq	%rsi, -72(%rbp)
	movq	%rax, 40(%rbx)
	movzbl	3(%rcx), %esi
	orq	%rax, %rsi
	movq	%rsi, 40(%rbx)
	cmpl	$2, %edx
	je	.L117
	movq	%rsi, %rax
	leaq	5(%rcx), %rsi
	salq	$8, %rax
	movq	%rsi, -72(%rbp)
	movq	%rax, 40(%rbx)
	movzbl	4(%rcx), %esi
	orq	%rax, %rsi
	movq	%rsi, 40(%rbx)
	cmpl	$3, %edx
	je	.L117
	movq	%rsi, %rax
	leaq	6(%rcx), %rsi
	salq	$8, %rax
	movq	%rsi, -72(%rbp)
	movq	%rax, 40(%rbx)
	movzbl	5(%rcx), %esi
	orq	%rax, %rsi
	movq	%rsi, 40(%rbx)
.L117:
	cmpq	48(%rbx), %rsi
	ja	.L78
.L116:
	leal	2(%rdx), %eax
	cltq
	addq	%rsi, %rax
	movq	%rax, 40(%rbx)
	jmp	.L118
.L182:
	movl	16(%rbx), %edx
.L84:
	cmpl	%edx, %r13d
	jge	.L78
	movl	(%rbx), %eax
	jmp	.L62
.L185:
	movl	$260, %r8d
	movl	$114, %edx
	movl	$118, %esi
	movl	$39, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	cmpb	$0, 0(%r13)
	jne	.L108
	movq	%r15, %rdx
	leaq	.LC5(%rip), %rsi
	movl	$2, %edi
	xorl	%eax, %eax
	call	ERR_add_error_data@PLT
	jmp	.L78
.L183:
	movq	32(%rbx), %rdi
	movl	$8, %esi
	call	BIO_test_flags@PLT
	testl	%eax, %eax
	je	.L78
	movl	(%rbx), %eax
	jmp	.L62
.L108:
	movq	%r13, %r8
	leaq	.LC6(%rip), %rcx
	movq	%r15, %rdx
	xorl	%eax, %eax
	leaq	.LC5(%rip), %rsi
	movl	$4, %edi
	call	ERR_add_error_data@PLT
	jmp	.L78
.L119:
	movl	$4104, (%rbx)
	movl	$1, %eax
	jmp	.L61
.L126:
	xorl	%esi, %esi
	jmp	.L116
.L175:
	movq	24(%rbx), %rdi
	movl	$8, %esi
	call	BIO_test_flags@PLT
	testl	%eax, %eax
	je	.L78
	movl	$-1, %eax
	jmp	.L61
.L179:
	movq	24(%rbx), %rdi
	movl	$8, %esi
	call	BIO_test_flags@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	negl	%eax
	jmp	.L61
.L178:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1377:
	.size	OCSP_REQ_CTX_nbio, .-OCSP_REQ_CTX_nbio
	.p2align 4
	.globl	OCSP_REQ_CTX_nbio_d2i
	.type	OCSP_REQ_CTX_nbio_d2i, @function
OCSP_REQ_CTX_nbio_d2i:
.LFB1371:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	OCSP_REQ_CTX_nbio
	movl	%eax, %r12d
	cmpl	$1, %eax
	je	.L191
.L186:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L192
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L191:
	.cfi_restore_state
	movq	32(%rbx), %rdi
	leaq	-64(%rbp), %r15
	xorl	%edx, %edx
	movl	$3, %esi
	movq	%r15, %rcx
	call	BIO_ctrl@PLT
	movq	%r14, %rcx
	movq	%r15, %rsi
	xorl	%edi, %edi
	movslq	%eax, %rdx
	call	ASN1_item_d2i@PLT
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	jne	.L186
	movl	$4096, (%rbx)
	xorl	%r12d, %r12d
	jmp	.L186
.L192:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1371:
	.size	OCSP_REQ_CTX_nbio_d2i, .-OCSP_REQ_CTX_nbio_d2i
	.p2align 4
	.globl	OCSP_sendreq_nbio
	.type	OCSP_sendreq_nbio, @function
OCSP_sendreq_nbio:
.LFB1378:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	OCSP_REQ_CTX_nbio
	movl	%eax, %r12d
	cmpl	$1, %eax
	je	.L198
.L193:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L199
	addq	$16, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L198:
	.cfi_restore_state
	movq	32(%rbx), %rdi
	leaq	-48(%rbp), %r14
	xorl	%edx, %edx
	movl	$3, %esi
	movq	%r14, %rcx
	call	BIO_ctrl@PLT
	leaq	OCSP_RESPONSE_it(%rip), %rcx
	movq	%r14, %rsi
	xorl	%edi, %edi
	movslq	%eax, %rdx
	call	ASN1_item_d2i@PLT
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	jne	.L193
	movl	$4096, (%rbx)
	xorl	%r12d, %r12d
	jmp	.L193
.L199:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1378:
	.size	OCSP_sendreq_nbio, .-OCSP_sendreq_nbio
	.p2align 4
	.globl	OCSP_sendreq_bio
	.type	OCSP_sendreq_bio, @function
OCSP_sendreq_bio:
.LFB1379:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movl	$66, %edx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	leaq	.LC0(%rip), %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$56, %edi
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L221
	movl	$4096, (%rax)
	movq	%rax, %r12
	movq	$102400, 48(%rax)
	call	BIO_s_mem@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%r13, %xmm0
	movl	$4096, 16(%r12)
	movl	$4096, %edi
	movq	%rax, %xmm1
	movl	$78, %edx
	leaq	.LC0(%rip), %rsi
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 24(%r12)
	call	CRYPTO_malloc@PLT
	movq	32(%r12), %rdi
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L220
	testq	%rdi, %rdi
	je	.L220
	testq	%r14, %r14
	leaq	.LC1(%rip), %rax
	leaq	.LC4(%rip), %rdx
	cmove	%rax, %r14
	leaq	http_hdr.21803(%rip), %rsi
	xorl	%eax, %eax
	movq	%r14, %rcx
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L211
	movl	$4105, (%r12)
	testq	%r15, %r15
	je	.L212
	xorl	%esi, %esi
	leaq	OCSP_REQUEST_it(%rip), %rdx
	movq	%r15, %rdi
	call	ASN1_item_i2d@PLT
	movq	32(%r12), %rdi
	leaq	req_hdr.21788(%rip), %rsi
	movl	%eax, %edx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L211
	movq	32(%r12), %rsi
	movq	%r15, %rdx
	leaq	OCSP_REQUEST_it(%rip), %rdi
	call	ASN1_item_i2d_bio@PLT
	testl	%eax, %eax
	jle	.L211
	movl	$4101, (%r12)
	movq	%r12, %rdi
	call	OCSP_REQ_CTX_nbio
	cmpl	$1, %eax
	je	.L222
	.p2align 4,,10
	.p2align 3
.L208:
	cmpl	$-1, %eax
	jne	.L211
	movl	$8, %esi
	movq	%r13, %rdi
	call	BIO_test_flags@PLT
	testl	%eax, %eax
	je	.L223
.L212:
	movq	%r12, %rdi
	call	OCSP_REQ_CTX_nbio
	cmpl	$1, %eax
	jne	.L208
.L222:
	leaq	-48(%rbp), %r13
	movq	32(%r12), %rdi
	xorl	%edx, %edx
	movl	$3, %esi
	movq	%r13, %rcx
	call	BIO_ctrl@PLT
	movq	%r13, %rsi
	leaq	OCSP_RESPONSE_it(%rip), %rcx
	xorl	%edi, %edi
	movslq	%eax, %rdx
	call	ASN1_item_d2i@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L224
.L210:
	movq	32(%r12), %rdi
	call	BIO_free@PLT
	movq	8(%r12), %rdi
	movl	$91, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$92, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
.L200:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L225
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L211:
	.cfi_restore_state
	movq	32(%r12), %rdi
.L220:
	call	BIO_free@PLT
	movq	8(%r12), %rdi
	movl	$91, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$92, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
.L221:
	xorl	%r13d, %r13d
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L224:
	movl	$4096, (%r12)
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L223:
	xorl	%r13d, %r13d
	jmp	.L210
.L225:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1379:
	.size	OCSP_sendreq_bio, .-OCSP_sendreq_bio
	.section	.rodata
	.align 16
	.type	http_hdr.21803, @object
	.size	http_hdr.21803, 17
http_hdr.21803:
	.string	"%s %s HTTP/1.0\r\n"
	.align 32
	.type	req_hdr.21788, @object
	.size	req_hdr.21788, 63
req_hdr.21788:
	.string	"Content-Type: application/ocsp-request\r\nContent-Length: %d\r\n\r\n"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
