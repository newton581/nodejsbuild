	.file	"x509name.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/x509/x509name.c"
	.text
	.p2align 4
	.type	X509_NAME_add_entry.part.0, @function
X509_NAME_add_entry.part.0:
.LFB801:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r13
	movl	%edx, -52(%rbp)
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	movl	-52(%rbp), %edx
	movl	$1, 8(%r15)
	movl	%eax, %ebx
	xorl	%eax, %eax
	testl	%r14d, %r14d
	sete	%al
	movl	%eax, -56(%rbp)
	testl	%edx, %edx
	js	.L2
	cmpl	%edx, %ebx
	jge	.L26
.L2:
	cmpl	$-1, %r14d
	je	.L12
.L13:
	xorl	%r14d, %r14d
	testl	%ebx, %ebx
	jne	.L27
.L4:
	movq	%r12, %rdi
	call	X509_NAME_ENTRY_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L6
.L30:
	movl	%r14d, 16(%rax)
	movl	%ebx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_insert@PLT
	testl	%eax, %eax
	je	.L28
	movl	-56(%rbp), %eax
	testl	%eax, %eax
	jne	.L8
.L10:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	cmpl	$-1, %r14d
	je	.L29
	cmpl	%edx, %ebx
	je	.L13
	movl	%edx, %esi
	movq	%r13, %rdi
	movl	%edx, -52(%rbp)
	call	OPENSSL_sk_value@PLT
	movl	-52(%rbp), %edx
	movq	%r12, %rdi
	movl	16(%rax), %r14d
	movl	%edx, %ebx
	call	X509_NAME_ENTRY_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L30
.L6:
	movq	%r12, %rdi
	call	X509_NAME_ENTRY_free@PLT
	xorl	%eax, %eax
.L32:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movl	%edx, %ebx
.L12:
	testl	%ebx, %ebx
	jne	.L31
	movl	$1, -56(%rbp)
	xorl	%r14d, %r14d
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%r13, %rdi
	addl	$1, %ebx
	call	OPENSSL_sk_num@PLT
	movl	%eax, %r12d
	cmpl	%ebx, %eax
	jle	.L10
	.p2align 4,,10
	.p2align 3
.L11:
	movl	%ebx, %esi
	movq	%r13, %rdi
	addl	$1, %ebx
	call	OPENSSL_sk_value@PLT
	addl	$1, 16(%rax)
	cmpl	%ebx, %r12d
	jne	.L11
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L27:
	leal	-1(%rbx), %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movl	16(%rax), %r14d
	addl	$1, %r14d
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L28:
	movl	$227, %r8d
	movl	$65, %edx
	movl	$113, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdi
	call	X509_NAME_ENTRY_free@PLT
	xorl	%eax, %eax
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L31:
	leal	-1(%rbx), %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movl	16(%rax), %r14d
	jmp	.L4
	.cfi_endproc
.LFE801:
	.size	X509_NAME_add_entry.part.0, .-X509_NAME_add_entry.part.0
	.p2align 4
	.globl	X509_NAME_get_text_by_OBJ
	.type	X509_NAME_get_text_by_OBJ, @function
X509_NAME_get_text_by_OBJ:
.LFB782:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -56(%rbp)
	movl	%ecx, -64(%rbp)
	testq	%rdi, %rdi
	je	.L36
	movq	(%rdi), %r14
	movq	%rdi, %rbx
	movq	%rdx, %r13
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	movl	%eax, %edx
	testl	%eax, %eax
	jle	.L36
	xorl	%r15d, %r15d
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L48:
	movl	-60(%rbp), %edx
	addl	$1, %r15d
	cmpl	%r15d, %edx
	je	.L36
.L38:
	movl	%r15d, %esi
	movq	%r14, %rdi
	movl	%edx, -60(%rbp)
	call	OPENSSL_sk_value@PLT
	movq	-56(%rbp), %rsi
	movq	(%rax), %rdi
	call	OBJ_cmp@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L48
	movq	(%rbx), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%r15d, %eax
	jle	.L43
	movq	(%rbx), %rdi
	movl	%r15d, %esi
	call	OPENSSL_sk_value@PLT
	testq	%rax, %rax
	je	.L43
	movq	8(%rax), %rax
.L40:
	testq	%r13, %r13
	je	.L49
	movl	-64(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.L33
	movl	(%rax), %r12d
	leal	-1(%rcx), %edx
	movq	8(%rax), %rsi
	movq	%r13, %rdi
	cmpl	%ecx, %r12d
	cmovge	%edx, %r12d
	movslq	%r12d, %rbx
	movq	%rbx, %rdx
	call	memcpy@PLT
	movb	$0, 0(%r13,%rbx)
.L33:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	movl	$-1, %r12d
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L49:
	movl	(%rax), %r12d
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L43:
	xorl	%eax, %eax
	jmp	.L40
	.cfi_endproc
.LFE782:
	.size	X509_NAME_get_text_by_OBJ, .-X509_NAME_get_text_by_OBJ
	.p2align 4
	.globl	X509_NAME_get_text_by_NID
	.type	X509_NAME_get_text_by_NID, @function
X509_NAME_get_text_by_NID:
.LFB781:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movl	%esi, %edi
	subq	$8, %rsp
	call	OBJ_nid2obj@PLT
	testq	%rax, %rax
	je	.L51
	addq	$8, %rsp
	movl	%r14d, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	X509_NAME_get_text_by_OBJ
.L51:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$-1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE781:
	.size	X509_NAME_get_text_by_NID, .-X509_NAME_get_text_by_NID
	.p2align 4
	.globl	X509_NAME_entry_count
	.type	X509_NAME_entry_count, @function
X509_NAME_entry_count:
.LFB783:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L54
	movq	(%rdi), %rdi
	jmp	OPENSSL_sk_num@PLT
	.p2align 4,,10
	.p2align 3
.L54:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE783:
	.size	X509_NAME_entry_count, .-X509_NAME_entry_count
	.p2align 4
	.globl	X509_NAME_get_index_by_NID
	.type	X509_NAME_get_index_by_NID, @function
X509_NAME_get_index_by_NID:
.LFB784:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movl	%esi, %edi
	call	OBJ_nid2obj@PLT
	testq	%rax, %rax
	je	.L60
	testq	%rbx, %rbx
	je	.L58
	movq	(%rbx), %r14
	testl	%r12d, %r12d
	movl	$-1, %edx
	movq	%rax, %r13
	cmovs	%edx, %r12d
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	addl	$1, %r12d
	movl	%eax, %ebx
	cmpl	%r12d, %eax
	jg	.L59
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L66:
	addl	$1, %r12d
	cmpl	%r12d, %ebx
	je	.L58
.L59:
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r13, %rsi
	movq	(%rax), %rdi
	call	OBJ_cmp@PLT
	testl	%eax, %eax
	jne	.L66
.L55:
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	movl	$-1, %r12d
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L60:
	.cfi_restore_state
	movl	$-2, %r12d
	jmp	.L55
	.cfi_endproc
.LFE784:
	.size	X509_NAME_get_index_by_NID, .-X509_NAME_get_index_by_NID
	.p2align 4
	.globl	X509_NAME_get_index_by_OBJ
	.type	X509_NAME_get_index_by_OBJ, @function
X509_NAME_get_index_by_OBJ:
.LFB785:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testq	%rdi, %rdi
	je	.L68
	testl	%edx, %edx
	movq	(%rdi), %r14
	movl	$-1, %r12d
	movq	%rsi, %r13
	cmovns	%edx, %r12d
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	addl	$1, %r12d
	movl	%eax, %ebx
	cmpl	%eax, %r12d
	jl	.L70
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L79:
	addl	$1, %r12d
	cmpl	%ebx, %r12d
	je	.L68
.L70:
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r13, %rsi
	movq	(%rax), %rdi
	call	OBJ_cmp@PLT
	testl	%eax, %eax
	jne	.L79
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	movl	$-1, %r12d
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE785:
	.size	X509_NAME_get_index_by_OBJ, .-X509_NAME_get_index_by_OBJ
	.p2align 4
	.globl	X509_NAME_get_entry
	.type	X509_NAME_get_entry, @function
X509_NAME_get_entry:
.LFB786:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L85
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	call	OPENSSL_sk_num@PLT
	testl	%r12d, %r12d
	js	.L82
	cmpl	%eax, %r12d
	jge	.L82
	movq	(%rbx), %rdi
	movl	%r12d, %esi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	OPENSSL_sk_value@PLT
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE786:
	.size	X509_NAME_get_entry, .-X509_NAME_get_entry
	.p2align 4
	.globl	X509_NAME_delete_entry
	.type	X509_NAME_delete_entry, @function
X509_NAME_delete_entry:
.LFB787:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdi, %rdi
	je	.L91
	movq	%rdi, %r12
	movq	(%rdi), %rdi
	movl	%esi, %ebx
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L91
	testl	%ebx, %ebx
	js	.L91
	movq	(%r12), %r14
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_delete@PLT
	movq	%r14, %rdi
	movq	%rax, %r15
	call	OPENSSL_sk_num@PLT
	movl	$1, 8(%r12)
	movl	%eax, %r13d
	cmpl	%eax, %ebx
	je	.L88
	testl	%ebx, %ebx
	jne	.L97
	movl	16(%r15), %r12d
.L93:
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	cmpl	16(%rax), %r12d
	jge	.L88
	cmpl	%r13d, %ebx
	jge	.L88
	.p2align 4,,10
	.p2align 3
.L94:
	movl	%ebx, %esi
	movq	%r14, %rdi
	addl	$1, %ebx
	call	OPENSSL_sk_value@PLT
	subl	$1, 16(%rax)
	cmpl	%r13d, %ebx
	jne	.L94
.L88:
	addq	$8, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	xorl	%r15d, %r15d
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L97:
	leal	-1(%rbx), %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movl	16(%rax), %r12d
	addl	$1, %r12d
	jmp	.L93
	.cfi_endproc
.LFE787:
	.size	X509_NAME_delete_entry, .-X509_NAME_delete_entry
	.p2align 4
	.globl	X509_NAME_add_entry
	.type	X509_NAME_add_entry, @function
X509_NAME_add_entry:
.LFB791:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L124
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	movq	(%rdi), %r14
	movq	%rdi, -56(%rbp)
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	movq	-56(%rbp), %r8
	movl	%eax, %r13d
	xorl	%eax, %eax
	testl	%r12d, %r12d
	sete	%al
	testl	%ebx, %ebx
	movl	$1, 8(%r8)
	movl	%eax, -60(%rbp)
	js	.L100
	cmpl	%r13d, %ebx
	jle	.L127
.L100:
	cmpl	$-1, %r12d
	je	.L109
.L110:
	xorl	%r12d, %r12d
	testl	%r13d, %r13d
	jne	.L128
.L102:
	movq	%r15, %rdi
	call	X509_NAME_ENTRY_dup@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L104
	movl	%r12d, 16(%rax)
	movl	%r13d, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	OPENSSL_sk_insert@PLT
	testl	%eax, %eax
	je	.L129
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jne	.L106
.L107:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	movl	$227, %r8d
	movl	$65, %edx
	movl	$113, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L104:
	movq	%r15, %rdi
	call	X509_NAME_ENTRY_free@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpl	$-1, %r12d
	jne	.L101
	movl	%ebx, %r13d
.L109:
	testl	%r13d, %r13d
	jne	.L130
	movl	$1, -60(%rbp)
	xorl	%r12d, %r12d
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L101:
	cmpl	%r13d, %ebx
	je	.L110
	movl	%ebx, %esi
	movq	%r14, %rdi
	movl	%ebx, %r13d
	call	OPENSSL_sk_value@PLT
	movl	16(%rax), %r12d
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L130:
	leal	-1(%r13), %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movl	16(%rax), %r12d
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L106:
	movq	%r14, %rdi
	addl	$1, %r13d
	call	OPENSSL_sk_num@PLT
	movl	%eax, %r12d
	cmpl	%r13d, %eax
	jle	.L107
	.p2align 4,,10
	.p2align 3
.L108:
	movl	%r13d, %esi
	movq	%r14, %rdi
	addl	$1, %r13d
	call	OPENSSL_sk_value@PLT
	addl	$1, 16(%rax)
	cmpl	%r13d, %r12d
	jne	.L108
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L128:
	leal	-1(%r13), %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movl	16(%rax), %r12d
	addl	$1, %r12d
	jmp	.L102
	.cfi_endproc
.LFE791:
	.size	X509_NAME_add_entry, .-X509_NAME_add_entry
	.p2align 4
	.globl	X509_NAME_ENTRY_set_object
	.type	X509_NAME_ENTRY_set_object, @function
X509_NAME_ENTRY_set_object:
.LFB795:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	testq	%rdi, %rdi
	je	.L135
	movq	%rsi, %r12
	testq	%rsi, %rsi
	je	.L135
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	%r12, %rdi
	call	OBJ_dup@PLT
	testq	%rax, %rax
	movq	%rax, (%rbx)
	popq	%rbx
	setne	%al
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	.cfi_restore_state
	movl	$309, %r8d
	movl	$67, %edx
	movl	$115, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE795:
	.size	X509_NAME_ENTRY_set_object, .-X509_NAME_ENTRY_set_object
	.p2align 4
	.globl	X509_NAME_ENTRY_set_data
	.type	X509_NAME_ENTRY_set_data, @function
X509_NAME_ENTRY_set_data:
.LFB796:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdi, %rdi
	je	.L140
	movq	%rdi, %rbx
	movl	%esi, %r13d
	movq	%rdx, %r14
	movl	%ecx, %r12d
	testq	%rdx, %rdx
	jne	.L146
	testl	%ecx, %ecx
	je	.L146
.L140:
	xorl	%r15d, %r15d
.L137:
	addq	$8, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_restore_state
	testl	%r13d, %r13d
	jle	.L142
	testl	$4096, %r13d
	jne	.L158
.L142:
	testl	%r12d, %r12d
	jns	.L143
	movq	%r14, %rdi
	call	strlen@PLT
	movl	%eax, %r12d
.L143:
	movq	8(%rbx), %rdi
	movl	%r12d, %edx
	movq	%r14, %rsi
	call	ASN1_STRING_set@PLT
	testl	%eax, %eax
	je	.L140
	movl	$1, %r15d
	cmpl	$-1, %r13d
	je	.L137
	movq	8(%rbx), %rbx
	cmpl	$-2, %r13d
	je	.L159
	movl	%r13d, 4(%rbx)
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L158:
	movq	(%rbx), %rdi
	xorl	%r15d, %r15d
	call	OBJ_obj2nid@PLT
	leaq	8(%rbx), %rdi
	movl	%r13d, %ecx
	movl	%r12d, %edx
	movl	%eax, %r8d
	movq	%r14, %rsi
	call	ASN1_STRING_set_by_NID@PLT
	testq	%rax, %rax
	setne	%r15b
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L159:
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	ASN1_PRINTABLE_type@PLT
	movl	%eax, 4(%rbx)
	jmp	.L137
	.cfi_endproc
.LFE796:
	.size	X509_NAME_ENTRY_set_data, .-X509_NAME_ENTRY_set_data
	.p2align 4
	.globl	X509_NAME_ENTRY_create_by_OBJ
	.type	X509_NAME_ENTRY_create_by_OBJ, @function
X509_NAME_ENTRY_create_by_OBJ:
.LFB794:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	%edx, -52(%rbp)
	testq	%rdi, %rdi
	je	.L161
	movq	(%rdi), %r12
	testq	%r12, %r12
	je	.L161
.L162:
	testq	%r13, %r13
	je	.L186
	movq	(%r12), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	%r13, %rdi
	call	OBJ_dup@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L165
	movl	-52(%rbp), %esi
	movl	%r15d, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	X509_NAME_ENTRY_set_data
	testl	%eax, %eax
	je	.L165
	testq	%rbx, %rbx
	je	.L160
	cmpq	$0, (%rbx)
	jne	.L160
	movq	%r12, (%rbx)
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L186:
	movl	$309, %r8d
	movl	$67, %edx
	movl	$115, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L165:
	testq	%rbx, %rbx
	je	.L167
	cmpq	%r12, (%rbx)
	je	.L185
.L167:
	movq	%r12, %rdi
	call	X509_NAME_ENTRY_free@PLT
.L185:
	xorl	%r12d, %r12d
.L160:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	call	X509_NAME_ENTRY_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L162
	jmp	.L185
	.cfi_endproc
.LFE794:
	.size	X509_NAME_ENTRY_create_by_OBJ, .-X509_NAME_ENTRY_create_by_OBJ
	.p2align 4
	.globl	X509_NAME_add_entry_by_OBJ
	.type	X509_NAME_add_entry_by_OBJ, @function
X509_NAME_add_entry_by_OBJ:
.LFB788:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%r9d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$24, %rsp
	movl	%edx, -52(%rbp)
	movq	%rcx, -64(%rbp)
	call	X509_NAME_ENTRY_new@PLT
	testq	%rax, %rax
	je	.L195
	movq	%rax, %r15
	testq	%r12, %r12
	je	.L203
	movq	(%rax), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	%r12, %rdi
	call	OBJ_dup@PLT
	movq	%rax, (%r15)
	testq	%rax, %rax
	je	.L190
	movq	-64(%rbp), %rdx
	movl	-52(%rbp), %esi
	movl	%ebx, %ecx
	movq	%r15, %rdi
	call	X509_NAME_ENTRY_set_data
	testl	%eax, %eax
	je	.L190
	xorl	%r12d, %r12d
	testq	%r13, %r13
	je	.L194
	movl	16(%rbp), %ecx
	movl	%r14d, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	X509_NAME_add_entry.part.0
	movl	%eax, %r12d
.L194:
	movq	%r15, %rdi
	call	X509_NAME_ENTRY_free@PLT
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L203:
	movl	$309, %r8d
	movl	$67, %edx
	movl	$115, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L190:
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	X509_NAME_ENTRY_free@PLT
.L187:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L195:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L187
	.cfi_endproc
.LFE788:
	.size	X509_NAME_add_entry_by_OBJ, .-X509_NAME_add_entry_by_OBJ
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"name="
	.text
	.p2align 4
	.globl	X509_NAME_add_entry_by_txt
	.type	X509_NAME_add_entry_by_txt, @function
X509_NAME_add_entry_by_txt:
.LFB790:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	xorl	%esi, %esi
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r9d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$24, %rsp
	movl	%edx, -52(%rbp)
	movq	%rcx, -64(%rbp)
	call	OBJ_txt2obj@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L219
	call	X509_NAME_ENTRY_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L207
	movq	(%rax), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	%r12, %rdi
	call	OBJ_dup@PLT
	movq	%rax, (%r15)
	testq	%rax, %rax
	jne	.L208
.L209:
	movq	%r15, %rdi
	call	X509_NAME_ENTRY_free@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	ASN1_OBJECT_free@PLT
.L204:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	.cfi_restore_state
	movq	-64(%rbp), %rdx
	movl	-52(%rbp), %esi
	movl	%ebx, %ecx
	movq	%r15, %rdi
	call	X509_NAME_ENTRY_set_data
	testl	%eax, %eax
	je	.L209
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	ASN1_OBJECT_free@PLT
	testq	%r13, %r13
	je	.L211
	movl	16(%rbp), %ecx
	movl	%r14d, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	X509_NAME_add_entry.part.0
	movl	%eax, %r12d
.L211:
	movq	%r15, %rdi
	call	X509_NAME_ENTRY_free@PLT
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L219:
	movl	$251, %r8d
	movl	$119, %edx
	movl	$131, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r15, %rdx
	movl	$2, %edi
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rsi
	call	ERR_add_error_data@PLT
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L207:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	ASN1_OBJECT_free@PLT
	jmp	.L204
	.cfi_endproc
.LFE790:
	.size	X509_NAME_add_entry_by_txt, .-X509_NAME_add_entry_by_txt
	.p2align 4
	.globl	X509_NAME_add_entry_by_NID
	.type	X509_NAME_add_entry_by_NID, @function
X509_NAME_add_entry_by_NID:
.LFB789:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%r9d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	%esi, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$24, %rsp
	movl	%edx, -52(%rbp)
	movq	%rcx, -64(%rbp)
	call	OBJ_nid2obj@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L235
	call	X509_NAME_ENTRY_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L223
	movq	(%rax), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	%r12, %rdi
	call	OBJ_dup@PLT
	movq	%rax, (%r15)
	testq	%rax, %rax
	jne	.L224
.L225:
	movq	%r15, %rdi
	call	X509_NAME_ENTRY_free@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	ASN1_OBJECT_free@PLT
.L220:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L224:
	.cfi_restore_state
	movq	-64(%rbp), %rdx
	movl	-52(%rbp), %esi
	movl	%ebx, %ecx
	movq	%r15, %rdi
	call	X509_NAME_ENTRY_set_data
	testl	%eax, %eax
	je	.L225
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	ASN1_OBJECT_free@PLT
	testq	%r13, %r13
	je	.L227
	movl	16(%rbp), %ecx
	movl	%r14d, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	X509_NAME_add_entry.part.0
	movl	%eax, %r12d
.L227:
	movq	%r15, %rdi
	call	X509_NAME_ENTRY_free@PLT
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L235:
	movl	$271, %r8d
	movl	$109, %edx
	movl	$114, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L223:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	ASN1_OBJECT_free@PLT
	jmp	.L220
	.cfi_endproc
.LFE789:
	.size	X509_NAME_add_entry_by_NID, .-X509_NAME_add_entry_by_NID
	.p2align 4
	.globl	X509_NAME_ENTRY_create_by_NID
	.type	X509_NAME_ENTRY_create_by_NID, @function
X509_NAME_ENTRY_create_by_NID:
.LFB793:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	%esi, %edi
	subq	$24, %rsp
	movl	%edx, -52(%rbp)
	call	OBJ_nid2obj@PLT
	testq	%rax, %rax
	je	.L273
	movq	%rax, %r12
	testq	%rbx, %rbx
	je	.L239
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.L239
	movq	0(%r13), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	%r12, %rdi
	call	OBJ_dup@PLT
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	je	.L247
	movl	-52(%rbp), %esi
	movl	%r15d, %ecx
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	X509_NAME_ENTRY_set_data
	testl	%eax, %eax
	je	.L247
.L248:
	cmpq	$0, (%rbx)
	jne	.L242
	movq	%r13, (%rbx)
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L239:
	call	X509_NAME_ENTRY_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L272
	movq	(%rax), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	%r12, %rdi
	call	OBJ_dup@PLT
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	jne	.L270
.L243:
	testq	%rbx, %rbx
	je	.L244
.L247:
	cmpq	%r13, (%rbx)
	je	.L272
.L244:
	movq	%r13, %rdi
	call	X509_NAME_ENTRY_free@PLT
.L272:
	xorl	%r13d, %r13d
.L242:
	movq	%r12, %rdi
	call	ASN1_OBJECT_free@PLT
.L236:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L273:
	.cfi_restore_state
	movl	$271, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$109, %edx
	xorl	%r13d, %r13d
	movl	$114, %esi
	movl	$11, %edi
	call	ERR_put_error@PLT
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L270:
	movl	-52(%rbp), %esi
	movl	%r15d, %ecx
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	X509_NAME_ENTRY_set_data
	testl	%eax, %eax
	je	.L243
	testq	%rbx, %rbx
	je	.L242
	jmp	.L248
	.cfi_endproc
.LFE793:
	.size	X509_NAME_ENTRY_create_by_NID, .-X509_NAME_ENTRY_create_by_NID
	.p2align 4
	.globl	X509_NAME_ENTRY_create_by_txt
	.type	X509_NAME_ENTRY_create_by_txt, @function
X509_NAME_ENTRY_create_by_txt:
.LFB792:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$24, %rsp
	movl	%edx, -52(%rbp)
	call	OBJ_txt2obj@PLT
	testq	%rax, %rax
	je	.L311
	movq	%rax, %r12
	testq	%rbx, %rbx
	je	.L277
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.L277
	movq	0(%r13), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	%r12, %rdi
	call	OBJ_dup@PLT
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	je	.L285
	movl	-52(%rbp), %esi
	movl	%r15d, %ecx
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	X509_NAME_ENTRY_set_data
	testl	%eax, %eax
	je	.L285
.L286:
	cmpq	$0, (%rbx)
	jne	.L280
	movq	%r13, (%rbx)
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L277:
	call	X509_NAME_ENTRY_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L310
	movq	(%rax), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	%r12, %rdi
	call	OBJ_dup@PLT
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	jne	.L308
.L281:
	testq	%rbx, %rbx
	je	.L282
.L285:
	cmpq	%r13, (%rbx)
	je	.L310
.L282:
	movq	%r13, %rdi
	call	X509_NAME_ENTRY_free@PLT
.L310:
	xorl	%r13d, %r13d
.L280:
	movq	%r12, %rdi
	call	ASN1_OBJECT_free@PLT
.L274:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L311:
	.cfi_restore_state
	movl	$251, %r8d
	movl	$119, %edx
	movl	$131, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r13, %rdx
	movl	$2, %edi
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rsi
	xorl	%r13d, %r13d
	call	ERR_add_error_data@PLT
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L308:
	movl	-52(%rbp), %esi
	movl	%r15d, %ecx
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	X509_NAME_ENTRY_set_data
	testl	%eax, %eax
	je	.L281
	testq	%rbx, %rbx
	je	.L280
	jmp	.L286
	.cfi_endproc
.LFE792:
	.size	X509_NAME_ENTRY_create_by_txt, .-X509_NAME_ENTRY_create_by_txt
	.p2align 4
	.globl	X509_NAME_ENTRY_get_object
	.type	X509_NAME_ENTRY_get_object, @function
X509_NAME_ENTRY_get_object:
.LFB797:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L314
	movq	(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L314:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE797:
	.size	X509_NAME_ENTRY_get_object, .-X509_NAME_ENTRY_get_object
	.p2align 4
	.globl	X509_NAME_ENTRY_get_data
	.type	X509_NAME_ENTRY_get_data, @function
X509_NAME_ENTRY_get_data:
.LFB798:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L317
	movq	8(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L317:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE798:
	.size	X509_NAME_ENTRY_get_data, .-X509_NAME_ENTRY_get_data
	.p2align 4
	.globl	X509_NAME_ENTRY_set
	.type	X509_NAME_ENTRY_set, @function
X509_NAME_ENTRY_set:
.LFB799:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %eax
	ret
	.cfi_endproc
.LFE799:
	.size	X509_NAME_ENTRY_set, .-X509_NAME_ENTRY_set
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
