	.file	"store_init.c"
	.text
	.p2align 4
	.type	do_store_init_ossl_, @function
do_store_init_ossl_:
.LFB792:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	OPENSSL_init_crypto@PLT
	testl	%eax, %eax
	jne	.L8
	movl	%eax, do_store_init_ossl_ret_(%rip)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	call	ossl_store_file_loader_init@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	movl	%eax, do_store_init_ossl_ret_(%rip)
	ret
	.cfi_endproc
.LFE792:
	.size	do_store_init_ossl_, .-do_store_init_ossl_
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/store/store_init.c"
	.text
	.p2align 4
	.globl	ossl_store_init_once
	.type	ossl_store_init_once, @function
ossl_store_init_once:
.LFB794:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	do_store_init_ossl_(%rip), %rsi
	leaq	store_init(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L10
	movl	do_store_init_ossl_ret_(%rip), %edx
	movl	$1, %eax
	testl	%edx, %edx
	je	.L10
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movl	$24, %r8d
	movl	$65, %edx
	movl	$112, %esi
	movl	$44, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE794:
	.size	ossl_store_init_once, .-ossl_store_init_once
	.p2align 4
	.globl	ossl_store_cleanup_int
	.type	ossl_store_cleanup_int, @function
ossl_store_cleanup_int:
.LFB795:
	.cfi_startproc
	endbr64
	jmp	ossl_store_destroy_loaders_int@PLT
	.cfi_endproc
.LFE795:
	.size	ossl_store_cleanup_int, .-ossl_store_cleanup_int
	.local	do_store_init_ossl_ret_
	.comm	do_store_init_ossl_ret_,4,4
	.local	store_init
	.comm	store_init,4,4
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
