	.file	"tb_eckey.c"
	.text
	.p2align 4
	.type	engine_unregister_all_EC, @function
engine_unregister_all_EC:
.LFB867:
	.cfi_startproc
	endbr64
	leaq	dh_table(%rip), %rdi
	jmp	engine_table_cleanup@PLT
	.cfi_endproc
.LFE867:
	.size	engine_unregister_all_EC, .-engine_unregister_all_EC
	.p2align 4
	.globl	ENGINE_unregister_EC
	.type	ENGINE_unregister_EC, @function
ENGINE_unregister_EC:
.LFB866:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	leaq	dh_table(%rip), %rdi
	jmp	engine_table_unregister@PLT
	.cfi_endproc
.LFE866:
	.size	ENGINE_unregister_EC, .-ENGINE_unregister_EC
	.p2align 4
	.globl	ENGINE_register_EC
	.type	ENGINE_register_EC, @function
ENGINE_register_EC:
.LFB868:
	.cfi_startproc
	endbr64
	cmpq	$0, 40(%rdi)
	jne	.L6
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movq	%rdi, %rdx
	xorl	%r9d, %r9d
	movl	$1, %r8d
	leaq	dummy_nid(%rip), %rcx
	leaq	engine_unregister_all_EC(%rip), %rsi
	leaq	dh_table(%rip), %rdi
	jmp	engine_table_register@PLT
	.cfi_endproc
.LFE868:
	.size	ENGINE_register_EC, .-ENGINE_register_EC
	.p2align 4
	.globl	ENGINE_register_all_EC
	.type	ENGINE_register_all_EC, @function
ENGINE_register_all_EC:
.LFB869:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	call	ENGINE_get_first@PLT
	testq	%rax, %rax
	je	.L7
	movq	%rax, %r12
	leaq	dummy_nid(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L11:
	cmpq	$0, 40(%r12)
	je	.L9
	xorl	%r9d, %r9d
	movl	$1, %r8d
	movq	%rbx, %rcx
	movq	%r12, %rdx
	leaq	engine_unregister_all_EC(%rip), %rsi
	leaq	dh_table(%rip), %rdi
	call	engine_table_register@PLT
.L9:
	movq	%r12, %rdi
	call	ENGINE_get_next@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L11
.L7:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE869:
	.size	ENGINE_register_all_EC, .-ENGINE_register_all_EC
	.p2align 4
	.globl	ENGINE_set_default_EC
	.type	ENGINE_set_default_EC, @function
ENGINE_set_default_EC:
.LFB870:
	.cfi_startproc
	endbr64
	cmpq	$0, 40(%rdi)
	jne	.L21
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	movq	%rdi, %rdx
	movl	$1, %r9d
	movl	$1, %r8d
	leaq	dummy_nid(%rip), %rcx
	leaq	engine_unregister_all_EC(%rip), %rsi
	leaq	dh_table(%rip), %rdi
	jmp	engine_table_register@PLT
	.cfi_endproc
.LFE870:
	.size	ENGINE_set_default_EC, .-ENGINE_set_default_EC
	.p2align 4
	.globl	ENGINE_get_default_EC
	.type	ENGINE_get_default_EC, @function
ENGINE_get_default_EC:
.LFB871:
	.cfi_startproc
	endbr64
	movl	$1, %esi
	leaq	dh_table(%rip), %rdi
	jmp	engine_table_select@PLT
	.cfi_endproc
.LFE871:
	.size	ENGINE_get_default_EC, .-ENGINE_get_default_EC
	.p2align 4
	.globl	ENGINE_get_EC
	.type	ENGINE_get_EC, @function
ENGINE_get_EC:
.LFB872:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE872:
	.size	ENGINE_get_EC, .-ENGINE_get_EC
	.p2align 4
	.globl	ENGINE_set_EC
	.type	ENGINE_set_EC, @function
ENGINE_set_EC:
.LFB873:
	.cfi_startproc
	endbr64
	movq	%rsi, 40(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE873:
	.size	ENGINE_set_EC, .-ENGINE_set_EC
	.section	.rodata
	.align 4
	.type	dummy_nid, @object
	.size	dummy_nid, 4
dummy_nid:
	.long	1
	.local	dh_table
	.comm	dh_table,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
