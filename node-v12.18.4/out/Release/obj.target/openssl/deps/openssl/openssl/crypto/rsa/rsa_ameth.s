	.file	"rsa_ameth.c"
	.text
	.p2align 4
	.type	rsa_pkey_check, @function
rsa_pkey_check:
.LFB1527:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	xorl	%esi, %esi
	jmp	RSA_check_key_ex@PLT
	.cfi_endproc
.LFE1527:
	.size	rsa_pkey_check, .-rsa_pkey_check
	.p2align 4
	.type	old_rsa_priv_encode, @function
old_rsa_priv_encode:
.LFB1496:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	jmp	i2d_RSAPrivateKey@PLT
	.cfi_endproc
.LFE1496:
	.size	old_rsa_priv_encode, .-old_rsa_priv_encode
	.p2align 4
	.type	int_rsa_free, @function
int_rsa_free:
.LFB1502:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	jmp	RSA_free@PLT
	.cfi_endproc
.LFE1502:
	.size	int_rsa_free, .-int_rsa_free
	.p2align 4
	.type	rsa_security_bits, @function
rsa_security_bits:
.LFB1501:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	jmp	RSA_security_bits@PLT
	.cfi_endproc
.LFE1501:
	.size	rsa_security_bits, .-rsa_security_bits
	.p2align 4
	.type	rsa_bits, @function
rsa_bits:
.LFB1500:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movq	24(%rax), %rdi
	jmp	BN_num_bits@PLT
	.cfi_endproc
.LFE1500:
	.size	rsa_bits, .-rsa_bits
	.p2align 4
	.type	int_rsa_size, @function
int_rsa_size:
.LFB1499:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	jmp	RSA_size@PLT
	.cfi_endproc
.LFE1499:
	.size	int_rsa_size, .-int_rsa_size
	.p2align 4
	.type	rsa_pub_cmp, @function
rsa_pub_cmp:
.LFB1494:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	40(%rdi), %rax
	movq	%rsi, %rbx
	movq	24(%rax), %rsi
	movq	40(%rbx), %rax
	movq	24(%rax), %rdi
	call	BN_cmp@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jne	.L8
	movq	40(%r12), %rax
	movq	32(%rax), %rsi
	movq	40(%rbx), %rax
	movq	32(%rax), %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
.L8:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1494:
	.size	rsa_pub_cmp, .-rsa_pub_cmp
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/rsa/rsa_ameth.c"
	.text
	.p2align 4
	.type	rsa_priv_encode, @function
rsa_priv_encode:
.LFB1497:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	40(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rsi), %rax
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	cmpl	$912, (%rax)
	jne	.L18
	movq	96(%rdi), %r8
	testq	%r8, %r8
	je	.L19
	leaq	-64(%rbp), %rdx
	leaq	RSA_PSS_PARAMS_it(%rip), %rsi
	movq	%r8, %rdi
	call	ASN1_item_pack@PLT
	testq	%rax, %rax
	je	.L20
	movq	40(%rbx), %rdi
	leaq	-72(%rbp), %rsi
	movl	$16, %r14d
	call	i2d_RSAPrivateKey@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jle	.L23
.L16:
	movq	16(%rbx), %rax
	movq	-72(%rbp), %r9
	movq	-64(%rbp), %r15
	movl	(%rax), %edi
	movq	%r9, -88(%rbp)
	call	OBJ_nid2obj@PLT
	subq	$8, %rsp
	movq	-88(%rbp), %r9
	movl	%r14d, %ecx
	pushq	%r13
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r15, %r8
	movq	%r12, %rdi
	call	PKCS8_pkey_set0@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	je	.L24
	movl	$1, %eax
.L13:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L25
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	movl	$5, %r14d
.L14:
	leaq	-72(%rbp), %rsi
	call	i2d_RSAPrivateKey@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jg	.L16
.L23:
	movl	$157, %r8d
	movl	$65, %edx
	movl	$138, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-64(%rbp), %rdi
	call	ASN1_STRING_free@PLT
	xorl	%eax, %eax
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L19:
	movl	$-1, %r14d
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L24:
	movl	$164, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	%eax, -88(%rbp)
	movl	$138, %esi
	movl	$4, %edi
	call	ERR_put_error@PLT
	movq	-64(%rbp), %rdi
	call	ASN1_STRING_free@PLT
	movl	-88(%rbp), %eax
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L20:
	xorl	%eax, %eax
	jmp	.L13
.L25:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1497:
	.size	rsa_priv_encode, .-rsa_priv_encode
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Minimum"
.LC2:
	.string	""
	.section	.rodata.str1.8
	.align 8
.LC3:
	.string	"No PSS parameter restrictions\n"
	.section	.rodata.str1.1
.LC4:
	.string	"PSS parameter restrictions:"
.LC5:
	.string	"(INVALID PSS PARAMETERS)\n"
.LC6:
	.string	"Hash Algorithm: "
.LC7:
	.string	"\n"
.LC8:
	.string	"sha1 (default)"
.LC9:
	.string	"Mask Algorithm: "
.LC10:
	.string	" with "
.LC11:
	.string	"INVALID"
.LC12:
	.string	"mgf1 with sha1 (default)"
.LC13:
	.string	"%s Salt Length: 0x"
.LC14:
	.string	"14 (default)"
.LC15:
	.string	"Trailer Field: 0x"
.LC16:
	.string	"BC (default)"
	.text
	.p2align 4
	.type	rsa_pss_param_print, @function
rsa_pss_param_print:
.LFB1504:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	movl	%ecx, %esi
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	movl	$128, %edx
	subq	$24, %rsp
	call	BIO_indent@PLT
	testl	%eax, %eax
	je	.L34
	testl	%r14d, %r14d
	je	.L29
	leaq	.LC3(%rip), %rsi
	testq	%rbx, %rbx
	je	.L78
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L26
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	addl	$2, %r13d
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L34
.L48:
	movl	$128, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	BIO_indent@PLT
	testl	%eax, %eax
	jne	.L79
.L34:
	xorl	%r8d, %r8d
	xorl	%r15d, %r15d
.L28:
	movq	%r8, %rdi
	call	X509_ALGOR_free@PLT
.L26:
	addq	$24, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	testq	%rbx, %rbx
	je	.L80
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jg	.L48
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L79:
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L34
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L35
	movq	(%rax), %rsi
	movq	%r12, %rdi
	call	i2a_ASN1_OBJECT@PLT
	testl	%eax, %eax
	jle	.L34
.L37:
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L34
	movl	$128, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	BIO_indent@PLT
	testl	%eax, %eax
	je	.L34
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L34
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L38
	movq	(%rax), %rsi
	movq	%r12, %rdi
	call	i2a_ASN1_OBJECT@PLT
	testl	%eax, %eax
	jle	.L34
	leaq	.LC10(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L34
	movq	8(%rbx), %r15
	movq	(%r15), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$911, %eax
	jne	.L39
	movq	8(%r15), %rsi
	leaq	X509_ALGOR_it(%rip), %rdi
	call	ASN1_TYPE_unpack_sequence@PLT
	testq	%rax, %rax
	je	.L39
	movq	(%rax), %rsi
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	xorl	%r15d, %r15d
	call	i2a_ASN1_OBJECT@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	jle	.L28
.L40:
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	BIO_puts@PLT
	movl	$128, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	BIO_indent@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	movl	%eax, %r15d
	je	.L28
	testl	%r14d, %r14d
	leaq	.LC2(%rip), %rax
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC13(%rip), %rsi
	cmove	%rax, %rdx
	xorl	%eax, %eax
	xorl	%r15d, %r15d
	call	BIO_printf@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	jle	.L28
	movq	16(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L43
	movq	%r12, %rdi
	call	i2a_ASN1_INTEGER@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	jle	.L28
.L45:
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	BIO_puts@PLT
	movl	$128, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	BIO_indent@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	movl	%eax, %r15d
	je	.L28
	leaq	.LC15(%rip), %rsi
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	xorl	%r15d, %r15d
	call	BIO_puts@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	jle	.L28
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L46
	movq	%r12, %rdi
	call	i2a_ASN1_INTEGER@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	jle	.L28
.L47:
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	movl	$1, %r15d
	call	BIO_puts@PLT
	movq	-56(%rbp), %r8
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L80:
	leaq	.LC5(%rip), %rsi
.L78:
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	call	BIO_puts@PLT
	testl	%eax, %eax
	setg	%r15b
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L35:
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jg	.L37
	jmp	.L34
.L38:
	leaq	.LC12(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L34
.L41:
	xorl	%r8d, %r8d
	jmp	.L40
.L39:
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jg	.L41
	jmp	.L34
.L43:
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	BIO_puts@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	jg	.L45
	jmp	.L28
.L46:
	leaq	.LC16(%rip), %rsi
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	BIO_puts@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	jg	.L47
	jmp	.L28
	.cfi_endproc
.LFE1504:
	.size	rsa_pss_param_print, .-rsa_pss_param_print
	.p2align 4
	.type	old_rsa_priv_decode, @function
old_rsa_priv_decode:
.LFB1495:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	xorl	%edi, %edi
	subq	$8, %rsp
	call	d2i_RSAPrivateKey@PLT
	testq	%rax, %rax
	je	.L85
	movq	%rax, %rdx
	movq	16(%r12), %rax
	movq	%r12, %rdi
	movl	(%rax), %esi
	call	EVP_PKEY_assign@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	movl	$133, %r8d
	movl	$4, %edx
	movl	$147, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1495:
	.size	old_rsa_priv_decode, .-old_rsa_priv_decode
	.section	.rodata.str1.1
.LC17:
	.string	"RSA-PSS"
.LC18:
	.string	"RSA"
.LC19:
	.string	"publicExponent:"
.LC20:
	.string	"modulus:"
.LC21:
	.string	"Exponent:"
.LC22:
	.string	"Modulus:"
.LC23:
	.string	"%s "
	.section	.rodata.str1.8
	.align 8
.LC24:
	.string	"Private-Key: (%d bit, %d primes)\n"
	.section	.rodata.str1.1
.LC25:
	.string	"Public-Key: (%d bit)\n"
.LC26:
	.string	"privateExponent:"
.LC27:
	.string	"prime1:"
.LC28:
	.string	"prime2:"
.LC29:
	.string	"exponent1:"
.LC30:
	.string	"exponent2:"
.LC31:
	.string	"coefficient:"
.LC32:
	.string	"prime%d:"
.LC33:
	.string	"exponent%d:"
.LC34:
	.string	"coefficient%d:"
	.text
	.p2align 4
	.type	rsa_priv_print, @function
rsa_priv_print:
.LFB1507:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	40(%rsi), %r14
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.L87
	call	BN_num_bits@PLT
	movl	%eax, %r15d
.L87:
	movq	88(%r14), %rdi
	call	OPENSSL_sk_num@PLT
	movl	$128, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	%eax, -52(%rbp)
	call	BIO_indent@PLT
	testl	%eax, %eax
	je	.L91
	movq	16(%rbx), %rax
	leaq	.LC17(%rip), %rdx
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	cmpl	$912, (%rax)
	leaq	.LC18(%rip), %rax
	cmovne	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L91
	cmpq	$0, 40(%r14)
	je	.L92
	movl	-52(%rbp), %eax
	movl	$0, %ecx
	movl	%r15d, %edx
	movq	%r12, %rdi
	leaq	.LC24(%rip), %rsi
	leaq	.LC19(%rip), %r15
	testl	%eax, %eax
	cmovns	%eax, %ecx
	xorl	%eax, %eax
	addl	$2, %ecx
	call	BIO_printf@PLT
	leaq	.LC20(%rip), %rsi
	testl	%eax, %eax
	jle	.L91
.L93:
	movq	24(%r14), %rdx
	xorl	%ecx, %ecx
	movl	%r13d, %r8d
	movq	%r12, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	jne	.L147
	.p2align 4,,10
	.p2align 3
.L91:
	xorl	%eax, %eax
.L86:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	xorl	%eax, %eax
	movl	%r15d, %edx
	leaq	.LC25(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L91
	leaq	.LC21(%rip), %r15
	leaq	.LC22(%rip), %rsi
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L147:
	movq	32(%r14), %rdx
	xorl	%ecx, %ecx
	movl	%r13d, %r8d
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L91
	movq	40(%r14), %rdx
	xorl	%ecx, %ecx
	movl	%r13d, %r8d
	leaq	.LC26(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L91
	movq	48(%r14), %rdx
	xorl	%ecx, %ecx
	movl	%r13d, %r8d
	leaq	.LC27(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L91
	movq	56(%r14), %rdx
	xorl	%ecx, %ecx
	movl	%r13d, %r8d
	leaq	.LC28(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L91
	movq	64(%r14), %rdx
	xorl	%ecx, %ecx
	movl	%r13d, %r8d
	leaq	.LC29(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L91
	movq	72(%r14), %rdx
	xorl	%ecx, %ecx
	movl	%r13d, %r8d
	leaq	.LC30(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L91
	movq	80(%r14), %rdx
	xorl	%ecx, %ecx
	movl	%r13d, %r8d
	leaq	.LC31(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L91
	movl	$0, -52(%rbp)
	.p2align 4,,10
	.p2align 3
.L94:
	movq	88(%r14), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, -52(%rbp)
	jge	.L148
	movq	88(%r14), %rdi
	movl	-52(%rbp), %esi
	xorl	%r15d, %r15d
	call	OPENSSL_sk_value@PLT
	movq	%rax, -64(%rbp)
	movl	-52(%rbp), %eax
	addl	$3, %eax
	movl	%eax, -56(%rbp)
.L100:
	movl	$128, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	BIO_indent@PLT
	testl	%eax, %eax
	je	.L91
	movl	-56(%rbp), %edx
	cmpl	$1, %r15d
	je	.L95
	cmpl	$2, %r15d
	je	.L96
	xorl	%eax, %eax
	leaq	.LC32(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L91
	movq	-64(%rbp), %rax
	movl	%r13d, %r8d
	xorl	%ecx, %ecx
	movq	(%rax), %rdx
.L146:
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L91
	addl	$1, %r15d
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L95:
	xorl	%eax, %eax
	leaq	.LC33(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L91
	movq	-64(%rbp), %rax
	movl	%r13d, %r8d
	xorl	%ecx, %ecx
	movq	8(%rax), %rdx
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L96:
	xorl	%eax, %eax
	leaq	.LC34(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L91
	movq	-64(%rbp), %rax
	xorl	%ecx, %ecx
	movl	%r13d, %r8d
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	16(%rax), %rdx
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L91
	addl	$1, -52(%rbp)
	jmp	.L94
.L148:
.L89:
	endbr64
	movq	16(%rbx), %rdx
	movl	$1, %eax
	cmpl	$912, (%rdx)
	jne	.L86
	movq	96(%r14), %rdx
	movl	%r13d, %ecx
	movl	$1, %esi
	movq	%r12, %rdi
	call	rsa_pss_param_print
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L86
	.cfi_endproc
.LFE1507:
	.size	rsa_priv_print, .-rsa_priv_print
	.p2align 4
	.type	rsa_pub_decode, @function
rsa_pub_decode:
.LFB1493:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-80(%rbp), %rcx
	leaq	-96(%rbp), %rdx
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-88(%rbp), %r13
	pushq	%r12
	movq	%r13, %rsi
	.cfi_offset 12, -48
	movq	%rdi, %r12
	xorl	%edi, %edi
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	X509_PUBKEY_get0_param@PLT
	testl	%eax, %eax
	jne	.L173
.L149:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L174
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L173:
	.cfi_restore_state
	movslq	-96(%rbp), %rdx
	movq	%r13, %rsi
	xorl	%edi, %edi
	call	d2i_RSAPublicKey@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L175
	movq	-80(%rbp), %rbx
	leaq	-72(%rbp), %rdi
	leaq	-64(%rbp), %rdx
	leaq	-92(%rbp), %rsi
	movq	%rbx, %rcx
	call	X509_ALGOR_get0@PLT
	movq	-72(%rbp), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$912, %eax
	jne	.L160
	movl	-92(%rbp), %eax
	cmpl	$-1, %eax
	je	.L160
	cmpl	$16, %eax
	je	.L153
	movl	$66, %r8d
	movl	$149, %edx
	movl	$164, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L154:
	movq	%r13, %rdi
	call	RSA_free@PLT
	xorl	%eax, %eax
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L153:
	movq	8(%rbx), %rsi
	leaq	RSA_PSS_PARAMS_it(%rip), %rdi
	call	ASN1_TYPE_unpack_sequence@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L155
	movq	8(%rax), %r14
	testq	%r14, %r14
	jne	.L176
.L156:
	movq	%r15, 96(%r13)
.L160:
	movq	16(%r12), %rax
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	(%rax), %esi
	call	EVP_PKEY_assign@PLT
	testl	%eax, %eax
	je	.L177
	movl	$1, %eax
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L175:
	movl	$105, %r8d
	movl	$4, %edx
	movl	$139, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L177:
	movq	%r13, %rdi
	movl	%eax, -100(%rbp)
	call	RSA_free@PLT
	movl	-100(%rbp), %eax
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L176:
	movq	(%r14), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$911, %eax
	jne	.L178
	movq	8(%r14), %rsi
	leaq	X509_ALGOR_it(%rip), %rdi
	call	ASN1_TYPE_unpack_sequence@PLT
	movq	%rax, 32(%r15)
	testq	%rax, %rax
	jne	.L156
	.p2align 4,,10
	.p2align 3
.L158:
	movq	%r15, %rdi
	call	RSA_PSS_PARAMS_free@PLT
	movq	$0, 96(%r13)
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L178:
	movq	$0, 32(%r15)
	jmp	.L158
.L155:
	movq	$0, 96(%r13)
	jmp	.L154
.L174:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1493:
	.size	rsa_pub_decode, .-rsa_pub_decode
	.p2align 4
	.type	rsa_pub_encode, @function
rsa_pub_encode:
.LFB1492:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	40(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rsi), %rax
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	cmpl	$912, (%rax)
	jne	.L185
	movq	96(%rdi), %r8
	testq	%r8, %r8
	je	.L186
	leaq	-64(%rbp), %rdx
	leaq	RSA_PSS_PARAMS_it(%rip), %rsi
	movq	%r8, %rdi
	call	ASN1_item_pack@PLT
	testq	%rax, %rax
	je	.L183
	movq	40(%rbx), %rdi
	leaq	-72(%rbp), %rsi
	movl	$16, %r14d
	call	i2d_RSAPublicKey@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jle	.L183
.L190:
	movq	16(%rbx), %rax
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %r15
	movl	(%rax), %edi
	movq	%r8, -88(%rbp)
	call	OBJ_nid2obj@PLT
	movq	-88(%rbp), %r8
	movl	%r13d, %r9d
	movq	%r15, %rcx
	movq	%rax, %rsi
	movl	%r14d, %edx
	movq	%r12, %rdi
	call	X509_PUBKEY_set0_param@PLT
	testl	%eax, %eax
	movl	%eax, -88(%rbp)
	je	.L189
	movl	$1, %eax
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L185:
	movl	$5, %r14d
.L180:
	leaq	-72(%rbp), %rsi
	call	i2d_RSAPublicKey@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jg	.L190
.L183:
	xorl	%eax, %eax
.L179:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L191
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L186:
	.cfi_restore_state
	movl	$-1, %r14d
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L189:
	movq	-72(%rbp), %rdi
	movl	$91, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	-88(%rbp), %eax
	jmp	.L179
.L191:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1492:
	.size	rsa_pub_encode, .-rsa_pub_encode
	.p2align 4
	.type	rsa_md_to_mgf1, @function
rsa_md_to_mgf1:
.LFB1512:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	movq	$0, (%rdi)
	testq	%rsi, %rsi
	je	.L195
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movq	%rsi, %r12
	call	EVP_MD_type@PLT
	cmpl	$64, %eax
	je	.L195
	movq	%r12, %rdi
	call	EVP_MD_type@PLT
	cmpl	$64, %eax
	je	.L202
	call	X509_ALGOR_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L207
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	X509_ALGOR_set_md@PLT
.L196:
	leaq	-48(%rbp), %rdx
	leaq	X509_ALGOR_it(%rip), %rsi
	movq	%r13, %rdi
	call	ASN1_item_pack@PLT
	testq	%rax, %rax
	je	.L207
	call	X509_ALGOR_new@PLT
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L207
	movq	-48(%rbp), %r12
	movl	$911, %edi
	call	OBJ_nid2obj@PLT
	movq	(%rbx), %rdi
	movl	$16, %edx
	movq	%rax, %rsi
	movq	%r12, %rcx
	call	X509_ALGOR_set0@PLT
	movq	$0, -48(%rbp)
	xorl	%edi, %edi
.L198:
	call	ASN1_STRING_free@PLT
	movq	%r13, %rdi
	call	X509_ALGOR_free@PLT
	xorl	%eax, %eax
	cmpq	$0, (%rbx)
	setne	%al
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L195:
	movl	$1, %eax
.L192:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L208
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L207:
	.cfi_restore_state
	movq	-48(%rbp), %rdi
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L202:
	xorl	%r13d, %r13d
	jmp	.L196
.L208:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1512:
	.size	rsa_md_to_mgf1, .-rsa_md_to_mgf1
	.p2align 4
	.type	rsa_pub_print, @function
rsa_pub_print:
.LFB1506:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	40(%rsi), %r14
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.L210
	call	BN_num_bits@PLT
	movl	%eax, %r15d
.L210:
	movq	88(%r14), %rdi
	call	OPENSSL_sk_num@PLT
	movl	$128, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	BIO_indent@PLT
	testl	%eax, %eax
	je	.L214
	movq	16(%rbx), %rax
	leaq	.LC17(%rip), %rdx
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	cmpl	$912, (%rax)
	leaq	.LC18(%rip), %rax
	cmovne	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L214
	xorl	%eax, %eax
	movl	%r15d, %edx
	leaq	.LC25(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L214
	movq	24(%r14), %rdx
	xorl	%ecx, %ecx
	movl	%r13d, %r8d
	leaq	.LC22(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	jne	.L227
.L214:
	xorl	%eax, %eax
.L209:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L227:
	.cfi_restore_state
	movq	32(%r14), %rdx
	xorl	%ecx, %ecx
	movl	%r13d, %r8d
	leaq	.LC21(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L214
.L212:
	endbr64
	movq	16(%rbx), %rdx
	movl	$1, %eax
	cmpl	$912, (%rdx)
	jne	.L209
	movq	96(%r14), %rdx
	movl	%r13d, %ecx
	movl	$1, %esi
	movq	%r12, %rdi
	call	rsa_pss_param_print
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L209
	.cfi_endproc
.LFE1506:
	.size	rsa_pub_print, .-rsa_pub_print
	.p2align 4
	.type	rsa_sig_print, @function
rsa_sig_print:
.LFB1509:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$912, %eax
	je	.L248
	testq	%r12, %r12
	je	.L235
.L236:
	addq	$8, %rsp
	movl	%r14d, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	X509_signature_dump@PLT
	.p2align 4,,10
	.p2align 3
.L248:
	.cfi_restore_state
	movq	8(%rbx), %rsi
	leaq	RSA_PSS_PARAMS_it(%rip), %rdi
	call	ASN1_TYPE_unpack_sequence@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L230
	movq	8(%rax), %r15
	testq	%r15, %r15
	je	.L230
	movq	(%r15), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$911, %eax
	jne	.L249
	movq	8(%r15), %rsi
	leaq	X509_ALGOR_it(%rip), %rdi
	call	ASN1_TYPE_unpack_sequence@PLT
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	je	.L232
.L230:
	xorl	%esi, %esi
	movq	%r13, %rdi
	movl	%r14d, %ecx
	movq	%rbx, %rdx
	call	rsa_pss_param_print
	movq	%rbx, %rdi
	movl	%eax, %r15d
	call	RSA_PSS_PARAMS_free@PLT
	testl	%r15d, %r15d
	je	.L228
	testq	%r12, %r12
	jne	.L236
	movl	$1, %r15d
.L228:
	addq	$8, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L235:
	.cfi_restore_state
	leaq	.LC7(%rip), %rsi
	movq	%r13, %rdi
	xorl	%r15d, %r15d
	call	BIO_puts@PLT
	testl	%eax, %eax
	setg	%r15b
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L249:
	movq	$0, 32(%rbx)
.L232:
	movq	%rbx, %rdi
	xorl	%ebx, %ebx
	call	RSA_PSS_PARAMS_free@PLT
	jmp	.L230
	.cfi_endproc
.LFE1509:
	.size	rsa_sig_print, .-rsa_sig_print
	.p2align 4
	.type	rsa_priv_decode, @function
rsa_priv_decode:
.LFB1498:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-80(%rbp), %rcx
	leaq	-96(%rbp), %rdx
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-88(%rbp), %r13
	pushq	%r12
	movq	%r13, %rsi
	.cfi_offset 12, -48
	movq	%rdi, %r12
	xorl	%edi, %edi
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	PKCS8_pkey_get0@PLT
	testl	%eax, %eax
	jne	.L273
.L250:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L274
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L273:
	.cfi_restore_state
	movslq	-96(%rbp), %rdx
	movq	%r13, %rsi
	xorl	%edi, %edi
	call	d2i_RSAPrivateKey@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L275
	movq	-80(%rbp), %rbx
	leaq	-72(%rbp), %rdi
	leaq	-64(%rbp), %rdx
	leaq	-92(%rbp), %rsi
	movq	%rbx, %rcx
	call	X509_ALGOR_get0@PLT
	movq	-72(%rbp), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$912, %eax
	jne	.L261
	movl	-92(%rbp), %eax
	cmpl	$-1, %eax
	je	.L261
	cmpl	$16, %eax
	je	.L254
	movl	$66, %r8d
	movl	$149, %edx
	movl	$164, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L255:
	movq	%r13, %rdi
	call	RSA_free@PLT
	xorl	%eax, %eax
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L254:
	movq	8(%rbx), %rsi
	leaq	RSA_PSS_PARAMS_it(%rip), %rdi
	call	ASN1_TYPE_unpack_sequence@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L256
	movq	8(%rax), %r14
	testq	%r14, %r14
	jne	.L276
.L257:
	movq	%r15, 96(%r13)
.L261:
	movq	16(%r12), %rax
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	(%rax), %esi
	call	EVP_PKEY_assign@PLT
	movl	$1, %eax
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L275:
	movl	$183, %r8d
	movl	$4, %edx
	movl	$150, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L276:
	movq	(%r14), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$911, %eax
	jne	.L277
	movq	8(%r14), %rsi
	leaq	X509_ALGOR_it(%rip), %rdi
	call	ASN1_TYPE_unpack_sequence@PLT
	movq	%rax, 32(%r15)
	testq	%rax, %rax
	jne	.L257
	.p2align 4,,10
	.p2align 3
.L259:
	movq	%r15, %rdi
	call	RSA_PSS_PARAMS_free@PLT
	movq	$0, 96(%r13)
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L277:
	movq	$0, 32(%r15)
	jmp	.L259
.L256:
	movq	$0, 96(%r13)
	jmp	.L255
.L274:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1498:
	.size	rsa_priv_decode, .-rsa_priv_decode
	.p2align 4
	.globl	rsa_pss_params_create
	.type	rsa_pss_params_create, @function
rsa_pss_params_create:
.LFB1515:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	%edx, %ebx
	call	RSA_PSS_PARAMS_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L279
	cmpl	$20, %ebx
	je	.L283
	call	ASN1_INTEGER_new@PLT
	movq	%rax, 16(%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L279
	movslq	%ebx, %rsi
	call	ASN1_INTEGER_set@PLT
	testl	%eax, %eax
	je	.L279
.L283:
	testq	%r13, %r13
	je	.L284
	movq	%r13, %rdi
	call	EVP_MD_type@PLT
	cmpl	$64, %eax
	je	.L284
	call	X509_ALGOR_new@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L279
	movq	%r13, %rsi
	call	X509_ALGOR_set_md@PLT
.L284:
	leaq	8(%r12), %rdi
	testq	%r14, %r14
	je	.L323
	movq	%r14, %rsi
	call	rsa_md_to_mgf1
	testl	%eax, %eax
	je	.L279
.L287:
	movq	%r14, %rdi
	call	EVP_MD_type@PLT
	cmpl	$64, %eax
	je	.L278
	call	X509_ALGOR_new@PLT
	movq	%rax, 32(%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L279
	movq	%r14, %rsi
	call	X509_ALGOR_set_md@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L279:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	RSA_PSS_PARAMS_free@PLT
.L278:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L323:
	.cfi_restore_state
	movq	%r13, %rsi
	call	rsa_md_to_mgf1
	testl	%eax, %eax
	je	.L279
	testq	%r13, %r13
	je	.L278
	movq	%r13, %r14
	jmp	.L287
	.cfi_endproc
.LFE1515:
	.size	rsa_pss_params_create, .-rsa_pss_params_create
	.p2align 4
	.type	rsa_ctx_to_pss_string, @function
rsa_ctx_to_pss_string:
.LFB1516:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	EVP_PKEY_CTX_get0_pkey@PLT
	xorl	%r8d, %r8d
	leaq	-40(%rbp), %r9
	movq	%r12, %rdi
	movl	$13, %ecx
	movl	$248, %edx
	movl	$-1, %esi
	movq	%rax, %r13
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L333
	xorl	%ecx, %ecx
	leaq	-32(%rbp), %r8
	movl	$4104, %edx
	movl	$1016, %esi
	movq	%r12, %rdi
	call	RSA_pkey_ctx_ctrl@PLT
	testl	%eax, %eax
	jle	.L333
	xorl	%ecx, %ecx
	leaq	-44(%rbp), %r8
	movl	$4103, %edx
	movl	$24, %esi
	movq	%r12, %rdi
	call	RSA_pkey_ctx_ctrl@PLT
	testl	%eax, %eax
	je	.L333
	movl	-44(%rbp), %edx
	cmpl	$-1, %edx
	je	.L338
	leal	3(%rdx), %eax
	cmpl	$1, %eax
	jbe	.L339
.L329:
	movq	-32(%rbp), %rsi
	movq	-40(%rbp), %rdi
	call	rsa_pss_params_create
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L333
	xorl	%edx, %edx
	leaq	RSA_PSS_PARAMS_it(%rip), %rsi
	movq	%rax, %rdi
	call	ASN1_item_pack@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	RSA_PSS_PARAMS_free@PLT
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L339:
	movq	%r13, %rdi
	call	EVP_PKEY_size@PLT
	movq	-40(%rbp), %rdi
	movl	%eax, %r12d
	call	EVP_MD_size@PLT
	movq	%r13, %rdi
	subl	%eax, %r12d
	subl	$2, %r12d
	movl	%r12d, -44(%rbp)
	call	EVP_PKEY_bits@PLT
	andl	$7, %eax
	cmpl	$1, %eax
	je	.L330
	movl	-44(%rbp), %edx
.L331:
	testl	%edx, %edx
	jns	.L329
	.p2align 4,,10
	.p2align 3
.L333:
	xorl	%r12d, %r12d
.L324:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L340
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L338:
	.cfi_restore_state
	movq	-40(%rbp), %rdi
	call	EVP_MD_size@PLT
	movl	%eax, -44(%rbp)
	movl	%eax, %edx
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L330:
	movl	-44(%rbp), %eax
	leal	-1(%rax), %edx
	movl	%edx, -44(%rbp)
	jmp	.L331
.L340:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1516:
	.size	rsa_ctx_to_pss_string, .-rsa_ctx_to_pss_string
	.p2align 4
	.type	rsa_item_sign, @function
rsa_item_sign:
.LFB1522:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	EVP_MD_CTX_pkey_ctx@PLT
	xorl	%ecx, %ecx
	leaq	-44(%rbp), %r8
	movl	$4102, %edx
	movl	$-1, %esi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	RSA_pkey_ctx_ctrl@PLT
	testl	%eax, %eax
	jle	.L356
	cmpl	$6, -44(%rbp)
	movl	$2, %eax
	je	.L357
.L341:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L358
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L359:
	.cfi_restore_state
	movq	%r12, %rdi
	call	ASN1_STRING_free@PLT
	.p2align 4,,10
	.p2align 3
.L356:
	xorl	%eax, %eax
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L357:
	movq	%r12, %rdi
	call	rsa_ctx_to_pss_string
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L356
	testq	%r13, %r13
	je	.L345
	movq	%rax, %rdi
	call	ASN1_STRING_dup@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L359
	movl	$912, %edi
	call	OBJ_nid2obj@PLT
	movq	%r15, %rcx
	movl	$16, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	X509_ALGOR_set0@PLT
.L345:
	movl	$912, %edi
	call	OBJ_nid2obj@PLT
	movq	%r12, %rcx
	movl	$16, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	X509_ALGOR_set0@PLT
	movl	$3, %eax
	jmp	.L341
.L358:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1522:
	.size	rsa_item_sign, .-rsa_item_sign
	.p2align 4
	.globl	rsa_pss_get_param
	.type	rsa_pss_get_param, @function
rsa_pss_get_param:
.LFB1518:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L387
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	testq	%rax, %rax
	je	.L388
	movq	(%rax), %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	testq	%rax, %rax
	je	.L389
	movq	%rax, (%r12)
.L365:
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.L390
	movq	(%rax), %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	testq	%rax, %rax
	je	.L391
	movq	%rax, 0(%r13)
.L368:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L370
	call	ASN1_INTEGER_get@PLT
	movl	%eax, (%r14)
	testl	%eax, %eax
	js	.L392
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L373
.L372:
	call	ASN1_INTEGER_get@PLT
	cmpq	$1, %rax
	je	.L373
	movl	$734, %r8d
	movl	$139, %edx
	movl	$151, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L373:
	movl	$1, %eax
.L360:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L389:
	.cfi_restore_state
	movl	$576, %r8d
	movl	$166, %edx
	movl	$156, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	$0, (%r12)
	xorl	%eax, %eax
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L370:
	movq	24(%rbx), %rdi
	movl	$20, (%r14)
	testq	%rdi, %rdi
	jne	.L372
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L387:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L388:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	call	EVP_sha1@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	jne	.L365
	xorl	%eax, %eax
.L393:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L390:
	.cfi_restore_state
	call	EVP_sha1@PLT
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	jne	.L368
	xorl	%eax, %eax
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L391:
	movl	$576, %r8d
	movl	$166, %edx
	movl	$156, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	$0, 0(%r13)
	xorl	%eax, %eax
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L392:
	movl	$722, %r8d
	movl	$150, %edx
	movl	$151, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L393
	.cfi_endproc
.LFE1518:
	.size	rsa_pss_get_param, .-rsa_pss_get_param
	.p2align 4
	.type	rsa_sig_info_set, @function
rsa_sig_info_set:
.LFB1523:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$32, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	call	OBJ_obj2nid@PLT
	cmpl	$912, %eax
	je	.L416
.L394:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L417
	addq	$32, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L416:
	.cfi_restore_state
	movq	8(%rbx), %rsi
	leaq	RSA_PSS_PARAMS_it(%rip), %rdi
	call	ASN1_TYPE_unpack_sequence@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L396
	movq	8(%rax), %rbx
	testq	%rbx, %rbx
	je	.L396
	movq	(%rbx), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$911, %eax
	jne	.L418
	movq	8(%rbx), %rsi
	leaq	X509_ALGOR_it(%rip), %rdi
	call	ASN1_TYPE_unpack_sequence@PLT
	movq	%rax, 32(%r14)
	testq	%rax, %rax
	je	.L398
.L396:
	leaq	-60(%rbp), %rcx
	leaq	-56(%rbp), %rdx
	movq	%r14, %rdi
	leaq	-48(%rbp), %rsi
	call	rsa_pss_get_param
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L419
.L399:
	endbr64
	movq	%r14, %rdi
	call	RSA_PSS_PARAMS_free@PLT
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L419:
	movq	-48(%rbp), %rdi
	call	EVP_MD_type@PLT
	movl	%eax, %r12d
	leal	-672(%rax), %eax
	cmpl	$2, %eax
	jbe	.L420
.L400:
	xorl	%ebx, %ebx
.L401:
	movq	-48(%rbp), %rdi
	call	EVP_MD_size@PLT
	movl	%r12d, %esi
	movq	%r13, %rdi
	movl	%ebx, %r8d
	leal	0(,%rax,4), %ecx
	movl	$912, %edx
	movl	$1, %r12d
	call	X509_SIG_INFO_set@PLT
	movq	%r14, %rdi
	call	RSA_PSS_PARAMS_free@PLT
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L420:
	movq	-56(%rbp), %rdi
	call	EVP_MD_type@PLT
	cmpl	%r12d, %eax
	jne	.L400
	movq	-48(%rbp), %rdi
	movl	$2, %ebx
	call	EVP_MD_size@PLT
	cmpl	-60(%rbp), %eax
	jne	.L400
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L418:
	movq	$0, 32(%r14)
.L398:
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	RSA_PSS_PARAMS_free@PLT
	jmp	.L396
.L417:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1523:
	.size	rsa_sig_info_set, .-rsa_sig_info_set
	.p2align 4
	.type	rsa_pkey_ctrl, @function
rsa_pkey_ctrl:
.LFB1510:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -104(%rbp)
	cmpl	$8, %esi
	ja	.L506
	movq	%rcx, %r12
	movl	%esi, %esi
	leaq	.L424(%rip), %rcx
	movslq	(%rcx,%rsi,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L424:
	.long	.L506-.L424
	.long	.L429-.L424
	.long	.L428-.L424
	.long	.L427-.L424
	.long	.L506-.L424
	.long	.L426-.L424
	.long	.L506-.L424
	.long	.L425-.L424
	.long	.L423-.L424
	.text
	.p2align 4,,10
	.p2align 3
.L506:
	movl	$-2, %r15d
	.p2align 4,,10
	.p2align 3
.L421:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L545
	addq	$88, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L429:
	.cfi_restore_state
	testq	%rdx, %rdx
	je	.L430
	.p2align 4,,10
	.p2align 3
.L543:
	movl	$1, %r15d
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L428:
	movq	16(%rdi), %rax
	cmpl	$912, (%rax)
	je	.L506
	testq	%rdx, %rdx
	jne	.L543
	leaq	-104(%rbp), %rsi
	movq	%r12, %rdi
	call	PKCS7_RECIP_INFO_get0_alg@PLT
.L431:
	cmpq	$0, -104(%rbp)
	je	.L543
	movl	$6, %edi
	call	OBJ_nid2obj@PLT
	movq	-104(%rbp), %rdi
	xorl	%ecx, %ecx
	movl	$5, %edx
	movq	%rax, %rsi
	call	X509_ALGOR_set0@PLT
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L427:
	movq	40(%rdi), %rax
	movq	96(%rax), %rdi
	testq	%rdi, %rdi
	je	.L484
	leaq	-80(%rbp), %rcx
	leaq	-64(%rbp), %rdx
	leaq	-72(%rbp), %rsi
	call	rsa_pss_get_param
	movl	%eax, %r15d
	testl	%eax, %eax
	jne	.L485
	movl	$506, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$68, %edx
	xorl	%esi, %esi
	movl	$4, %edi
	call	ERR_put_error@PLT
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L426:
	testq	%rdx, %rdx
	je	.L546
	cmpq	$1, %rdx
	jne	.L543
	movq	%r12, %rdi
	call	CMS_SignerInfo_get0_pkey_ctx@PLT
	leaq	-88(%rbp), %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	CMS_SignerInfo_get0_algs@PLT
	movq	-88(%rbp), %rax
	movq	(%rax), %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	cmpl	$912, %eax
	je	.L547
	movq	0(%r13), %rax
	cmpl	$912, (%rax)
	je	.L548
	movl	$1, %r15d
	cmpl	$6, %edi
	je	.L421
	leaq	-64(%rbp), %rdx
	xorl	%esi, %esi
	call	OBJ_find_sigid_algs@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L421
	xorl	%r15d, %r15d
	cmpl	$6, -64(%rbp)
	sete	%r15b
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L425:
	movq	16(%rdi), %rax
	cmpl	$912, (%rax)
	je	.L506
	testq	%rdx, %rdx
	je	.L549
	cmpq	$1, %rdx
	jne	.L543
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	call	CMS_RecipientInfo_get0_pkey_ctx@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L421
	leaq	-64(%rbp), %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	CMS_RecipientInfo_ktri_get0_algs@PLT
	movl	$-1, %r15d
	testl	%eax, %eax
	je	.L421
	movq	-64(%rbp), %rax
	movl	$1, %r15d
	movq	(%rax), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$6, %eax
	je	.L421
	cmpl	$919, %eax
	je	.L471
	movl	$926, %r8d
	movl	$162, %edx
	movl	$159, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r15d
	call	ERR_put_error@PLT
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L423:
	movq	16(%rdi), %rax
	cmpl	$912, (%rax)
	je	.L506
	movl	$0, (%r12)
	movl	$1, %r15d
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L484:
	movl	$672, (%r12)
	movl	$1, %r15d
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L485:
	movq	-72(%rbp), %rdi
	movl	$2, %r15d
	call	EVP_MD_type@PLT
	movl	%eax, (%r12)
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L430:
	leaq	-104(%rbp), %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	PKCS7_SIGNER_INFO_get0_algs@PLT
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L546:
	movq	%r12, %rdi
	movl	$1, -72(%rbp)
	call	CMS_SignerInfo_get0_pkey_ctx@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r13
	leaq	-64(%rbp), %r8
	movq	%r12, %rdi
	call	CMS_SignerInfo_get0_algs@PLT
	testq	%r13, %r13
	je	.L437
	xorl	%ecx, %ecx
	leaq	-72(%rbp), %r8
	movl	$4102, %edx
	movl	$-1, %esi
	movq	%r13, %rdi
	call	RSA_pkey_ctx_ctrl@PLT
	testl	%eax, %eax
	jle	.L544
.L437:
	movl	-72(%rbp), %r15d
	cmpl	$1, %r15d
	je	.L550
	cmpl	$6, %r15d
	je	.L551
.L544:
	xorl	%r15d, %r15d
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L549:
	movq	%r12, %rdi
	movq	$0, -80(%rbp)
	call	CMS_RecipientInfo_get0_pkey_ctx@PLT
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	-72(%rbp), %rcx
	movq	%r12, %rdi
	movl	$1, -108(%rbp)
	movq	%rax, %r13
	call	CMS_RecipientInfo_ktri_get0_algs@PLT
	testl	%eax, %eax
	jle	.L544
	testq	%r13, %r13
	je	.L461
	xorl	%ecx, %ecx
	leaq	-108(%rbp), %r8
	movl	$4102, %edx
	movl	$-1, %esi
	movq	%r13, %rdi
	call	RSA_pkey_ctx_ctrl@PLT
	testl	%eax, %eax
	jle	.L544
.L461:
	movl	-108(%rbp), %r15d
	cmpl	$1, %r15d
	je	.L552
	cmpl	$4, %r15d
	jne	.L544
	xorl	%r8d, %r8d
	leaq	-96(%rbp), %r9
	movl	$4107, %ecx
	movq	%r13, %rdi
	movl	$768, %edx
	movl	$6, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L464
	xorl	%ecx, %ecx
	leaq	-88(%rbp), %r8
	movl	$4104, %edx
	movl	$1016, %esi
	movq	%r13, %rdi
	call	RSA_pkey_ctx_ctrl@PLT
	testl	%eax, %eax
	jle	.L464
	leaq	-64(%rbp), %r9
	xorl	%r8d, %r8d
	movl	$4108, %ecx
	movq	%r13, %rdi
	movl	$768, %edx
	movl	$6, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	js	.L464
	call	RSA_OAEP_PARAMS_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L464
	movq	-96(%rbp), %r13
	testq	%r13, %r13
	je	.L466
	movq	%r13, %rdi
	call	EVP_MD_type@PLT
	cmpl	$64, %eax
	je	.L466
	call	X509_ALGOR_new@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L495
	movq	%r13, %rsi
	call	X509_ALGOR_set_md@PLT
.L466:
	movq	-88(%rbp), %rsi
	leaq	8(%r12), %rdi
	call	rsa_md_to_mgf1
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L463
	testl	%r14d, %r14d
	je	.L468
	call	X509_ALGOR_new@PLT
	movq	%rax, 16(%r12)
	testq	%rax, %rax
	je	.L495
	call	ASN1_OCTET_STRING_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L495
	movq	-64(%rbp), %rsi
	movl	%r14d, %edx
	movq	%rax, %rdi
	call	ASN1_OCTET_STRING_set@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L553
	movl	$935, %edi
	call	OBJ_nid2obj@PLT
	movq	16(%r12), %rdi
	movq	%r13, %rcx
	movl	$4, %edx
	movq	%rax, %rsi
	call	X509_ALGOR_set0@PLT
.L468:
	leaq	-80(%rbp), %rdx
	leaq	RSA_OAEP_PARAMS_it(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_item_pack@PLT
	testq	%rax, %rax
	je	.L495
	movq	-80(%rbp), %r13
	movl	$919, %edi
	movl	$1, %r15d
	call	OBJ_nid2obj@PLT
	movq	-72(%rbp), %rdi
	movl	$16, %edx
	movq	%rax, %rsi
	movq	%r13, %rcx
	call	X509_ALGOR_set0@PLT
	movq	$0, -80(%rbp)
.L463:
	movq	%r12, %rdi
	call	RSA_OAEP_PARAMS_free@PLT
	movq	-80(%rbp), %rdi
	call	ASN1_STRING_free@PLT
	jmp	.L421
.L551:
	movq	%r13, %rdi
	call	rsa_ctx_to_pss_string
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L544
	movl	$912, %edi
	movl	$1, %r15d
	call	OBJ_nid2obj@PLT
	movq	-64(%rbp), %rdi
	movq	%r12, %rcx
	movl	$16, %edx
	movq	%rax, %rsi
	call	X509_ALGOR_set0@PLT
	jmp	.L421
.L464:
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	jmp	.L463
.L550:
	movl	$6, %edi
	call	OBJ_nid2obj@PLT
	movq	-64(%rbp), %rdi
	xorl	%ecx, %ecx
	movl	$5, %edx
	movq	%rax, %rsi
	call	X509_ALGOR_set0@PLT
	jmp	.L421
.L552:
	movl	$6, %edi
	call	OBJ_nid2obj@PLT
	movq	-72(%rbp), %rdi
	xorl	%ecx, %ecx
	movl	$5, %edx
	movq	%rax, %rsi
	call	X509_ALGOR_set0@PLT
	jmp	.L421
.L471:
	movq	-64(%rbp), %rax
	leaq	RSA_OAEP_PARAMS_it(%rip), %rdi
	movq	8(%rax), %rsi
	call	ASN1_TYPE_unpack_sequence@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L472
	movq	8(%rax), %rbx
	testq	%rbx, %rbx
	je	.L473
	movq	(%rbx), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$911, %eax
	jne	.L554
	movq	8(%rbx), %rsi
	leaq	X509_ALGOR_it(%rip), %rdi
	call	ASN1_TYPE_unpack_sequence@PLT
	movq	%rax, 24(%r12)
	testq	%rax, %rax
	je	.L475
.L476:
	movq	(%rax), %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L542
.L478:
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L555
	movq	(%rax), %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L542
.L480:
	movq	16(%r12), %r14
	testq	%r14, %r14
	je	.L501
	movq	(%r14), %rdi
	movq	%r9, -120(%rbp)
	call	OBJ_obj2nid@PLT
	movq	-120(%rbp), %r9
	cmpl	$935, %eax
	jne	.L556
	movq	8(%r14), %rax
	cmpl	$4, (%rax)
	jne	.L557
	movq	8(%rax), %rax
	movq	8(%rax), %r14
	movq	$0, 8(%rax)
	movl	(%rax), %eax
	movl	%eax, -120(%rbp)
.L481:
	xorl	%r8d, %r8d
	movl	$4, %ecx
	movl	$4097, %edx
	orl	$-1, %esi
	movq	%r13, %rdi
	movq	%r9, -128(%rbp)
	orl	$-1, %r15d
	call	RSA_pkey_ctx_ctrl@PLT
	testl	%eax, %eax
	jle	.L477
	movq	-128(%rbp), %r9
	xorl	%r8d, %r8d
	movl	$4105, %ecx
	movq	%r13, %rdi
	movl	$768, %edx
	movl	$6, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L477
	xorl	%ecx, %ecx
	movq	%rbx, %r8
	movl	$4101, %edx
	movl	$1016, %esi
	movq	%r13, %rdi
	call	RSA_pkey_ctx_ctrl@PLT
	testl	%eax, %eax
	jle	.L477
	movl	-120(%rbp), %r8d
	movq	%r14, %r9
	movl	$4106, %ecx
	movq	%r13, %rdi
	movl	$768, %edx
	movl	$6, %esi
	xorl	%r15d, %r15d
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	setg	%r15b
	leal	-1(%r15,%r15), %r15d
.L477:
	movq	%r12, %rdi
	call	RSA_OAEP_PARAMS_free@PLT
	jmp	.L421
.L547:
	movq	-88(%rbp), %rbx
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	(%rbx), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$912, %eax
	je	.L441
	movl	$667, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$155, %edx
	movl	$155, %esi
	movl	$4, %edi
	orl	$-1, %r15d
	call	ERR_put_error@PLT
	jmp	.L421
.L548:
	movl	$754, %r8d
	movl	$144, %edx
	movl	$158, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L544
.L473:
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L476
	call	EVP_sha1@PLT
	orl	$-1, %r15d
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L477
	jmp	.L478
.L441:
	movq	8(%rbx), %rsi
	leaq	RSA_PSS_PARAMS_it(%rip), %rdi
	call	ASN1_TYPE_unpack_sequence@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L443
	movq	8(%rax), %rbx
	testq	%rbx, %rbx
	je	.L443
	movq	(%rbx), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$911, %eax
	jne	.L558
	movq	8(%rbx), %rsi
	leaq	X509_ALGOR_it(%rip), %rdi
	call	ASN1_TYPE_unpack_sequence@PLT
	movq	%rax, 32(%r12)
	testq	%rax, %rax
	je	.L445
.L443:
	leaq	-96(%rbp), %rcx
	leaq	-80(%rbp), %rdx
	movq	%r12, %rdi
	leaq	-72(%rbp), %rsi
	call	rsa_pss_get_param
	testl	%eax, %eax
	je	.L559
	xorl	%r8d, %r8d
	orl	$-1, %esi
	leaq	-64(%rbp), %r9
	movl	$13, %ecx
	movl	$248, %edx
	movq	%r13, %rdi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L448
	movq	-72(%rbp), %rdi
	call	EVP_MD_type@PLT
	movq	-64(%rbp), %rdi
	movl	%eax, %ebx
	call	EVP_MD_type@PLT
	cmpl	%eax, %ebx
	jne	.L560
	xorl	%r8d, %r8d
	orl	$-1, %esi
	movl	$6, %ecx
	movl	$4097, %edx
	movq	%r13, %rdi
	call	RSA_pkey_ctx_ctrl@PLT
	testl	%eax, %eax
	jle	.L448
	movl	-96(%rbp), %ecx
	xorl	%r8d, %r8d
	movl	$4098, %edx
	movl	$24, %esi
	movq	%r13, %rdi
	call	RSA_pkey_ctx_ctrl@PLT
	testl	%eax, %eax
	jle	.L448
	movq	-80(%rbp), %r8
	xorl	%ecx, %ecx
	movl	$4101, %edx
	movl	$1016, %esi
	movq	%r13, %rdi
	call	RSA_pkey_ctx_ctrl@PLT
	testl	%eax, %eax
	jle	.L448
	movl	$1, %r15d
.L447:
	movq	%r12, %rdi
	call	RSA_PSS_PARAMS_free@PLT
	jmp	.L421
.L560:
	movl	$687, %r8d
	movl	$158, %edx
	movl	$155, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L448:
	orl	$-1, %r15d
	jmp	.L447
.L554:
	movq	$0, 24(%r12)
.L475:
	movq	%r12, %rdi
	call	RSA_OAEP_PARAMS_free@PLT
.L472:
	movl	$933, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$161, %edx
	xorl	%r12d, %r12d
	movl	$159, %esi
	movl	$4, %edi
	orl	$-1, %r15d
	call	ERR_put_error@PLT
	jmp	.L477
.L545:
	call	__stack_chk_fail@PLT
.L542:
	movl	$576, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$166, %edx
	movl	$156, %esi
	movl	$4, %edi
	orl	$-1, %r15d
	call	ERR_put_error@PLT
	jmp	.L477
.L501:
	movl	$0, -120(%rbp)
	jmp	.L481
.L555:
	call	EVP_sha1@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	jne	.L480
	orl	$-1, %r15d
	jmp	.L477
.L559:
	movl	$674, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$149, %edx
	movl	$155, %esi
	movl	$4, %edi
	orl	$-1, %r15d
	call	ERR_put_error@PLT
	jmp	.L447
.L558:
	movq	$0, 32(%r12)
.L445:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	RSA_PSS_PARAMS_free@PLT
	jmp	.L443
.L556:
	movl	$948, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$163, %edx
	movl	$159, %esi
	movl	$4, %edi
	orl	$-1, %r15d
	call	ERR_put_error@PLT
	jmp	.L477
.L495:
	xorl	%r15d, %r15d
	jmp	.L463
.L557:
	movl	$952, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$160, %edx
	movl	$159, %esi
	movl	$4, %edi
	orl	$-1, %r15d
	call	ERR_put_error@PLT
	jmp	.L477
.L553:
	movq	%r13, %rdi
	call	ASN1_OCTET_STRING_free@PLT
	jmp	.L463
	.cfi_endproc
.LFE1510:
	.size	rsa_pkey_ctrl, .-rsa_pkey_ctrl
	.p2align 4
	.type	rsa_item_verify, @function
rsa_item_verify:
.LFB1520:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rcx, %rbx
	subq	$48, %rsp
	movq	(%rcx), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	OBJ_obj2nid@PLT
	cmpl	$912, %eax
	jne	.L587
	movq	(%rbx), %rdi
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	call	OBJ_obj2nid@PLT
	cmpl	$912, %eax
	je	.L564
	movl	$667, %r8d
	movl	$155, %edx
	movl	$155, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
.L561:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L588
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L564:
	.cfi_restore_state
	movq	8(%rbx), %rsi
	leaq	RSA_PSS_PARAMS_it(%rip), %rdi
	call	ASN1_TYPE_unpack_sequence@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L565
	movq	8(%rax), %rbx
	testq	%rbx, %rbx
	je	.L565
	movq	(%rbx), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$911, %eax
	jne	.L589
	movq	8(%rbx), %rsi
	leaq	X509_ALGOR_it(%rip), %rdi
	call	ASN1_TYPE_unpack_sequence@PLT
	movq	%rax, 32(%r12)
	testq	%rax, %rax
	je	.L567
.L565:
	leaq	-76(%rbp), %rcx
	leaq	-64(%rbp), %rdx
	movq	%r12, %rdi
	leaq	-56(%rbp), %rsi
	call	rsa_pss_get_param
	movl	$674, %r8d
	movl	$149, %edx
	leaq	.LC0(%rip), %rcx
	testl	%eax, %eax
	je	.L585
	testq	%r14, %r14
	je	.L569
	movq	-56(%rbp), %rdx
	xorl	%ecx, %ecx
	leaq	-72(%rbp), %rsi
	movq	%r14, %r8
	movq	%r13, %rdi
	call	EVP_DigestVerifyInit@PLT
	testl	%eax, %eax
	je	.L570
.L571:
	movq	-72(%rbp), %rdi
	xorl	%r8d, %r8d
	movl	$6, %ecx
	movl	$4097, %edx
	movl	$-1, %esi
	call	RSA_pkey_ctx_ctrl@PLT
	testl	%eax, %eax
	jle	.L570
	movl	-76(%rbp), %ecx
	movq	-72(%rbp), %rdi
	xorl	%r8d, %r8d
	movl	$4098, %edx
	movl	$24, %esi
	call	RSA_pkey_ctx_ctrl@PLT
	testl	%eax, %eax
	jle	.L570
	movq	-72(%rbp), %rdi
	movq	-64(%rbp), %r8
	xorl	%ecx, %ecx
	movl	$4101, %edx
	movl	$1016, %esi
	call	RSA_pkey_ctx_ctrl@PLT
	movq	%r12, %rdi
	testl	%eax, %eax
	jle	.L586
	call	RSA_PSS_PARAMS_free@PLT
	movl	$2, %eax
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L569:
	movq	-72(%rbp), %rdi
	xorl	%r8d, %r8d
	leaq	-48(%rbp), %r9
	movl	$13, %ecx
	movl	$248, %edx
	movl	$-1, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L570
	movq	-56(%rbp), %rdi
	call	EVP_MD_type@PLT
	movq	-48(%rbp), %rdi
	movl	%eax, %ebx
	call	EVP_MD_type@PLT
	cmpl	%eax, %ebx
	je	.L571
	movl	$687, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$158, %edx
	.p2align 4,,10
	.p2align 3
.L585:
	movl	$155, %esi
	movl	$4, %edi
	call	ERR_put_error@PLT
.L570:
	movq	%r12, %rdi
.L586:
	call	RSA_PSS_PARAMS_free@PLT
	movl	$-1, %eax
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L589:
	movq	$0, 32(%r12)
.L567:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	RSA_PSS_PARAMS_free@PLT
	jmp	.L565
	.p2align 4,,10
	.p2align 3
.L587:
	movl	$779, %r8d
	movl	$155, %edx
	movl	$148, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L561
.L588:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1520:
	.size	rsa_item_verify, .-rsa_item_verify
	.globl	rsa_pss_asn1_meth
	.section	.rodata.str1.1
.LC35:
	.string	"OpenSSL RSA-PSS method"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	rsa_pss_asn1_meth, @object
	.size	rsa_pss_asn1_meth, 280
rsa_pss_asn1_meth:
	.long	912
	.long	912
	.quad	4
	.quad	.LC17
	.quad	.LC35
	.quad	rsa_pub_decode
	.quad	rsa_pub_encode
	.quad	rsa_pub_cmp
	.quad	rsa_pub_print
	.quad	rsa_priv_decode
	.quad	rsa_priv_encode
	.quad	rsa_priv_print
	.quad	int_rsa_size
	.quad	rsa_bits
	.quad	rsa_security_bits
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	rsa_sig_print
	.quad	int_rsa_free
	.quad	rsa_pkey_ctrl
	.quad	0
	.quad	0
	.quad	rsa_item_verify
	.quad	rsa_item_sign
	.quad	0
	.quad	rsa_pkey_check
	.zero	48
	.globl	rsa_asn1_meths
	.section	.rodata.str1.1
.LC36:
	.string	"OpenSSL RSA method"
	.section	.data.rel.ro.local
	.align 32
	.type	rsa_asn1_meths, @object
	.size	rsa_asn1_meths, 560
rsa_asn1_meths:
	.long	6
	.long	6
	.quad	4
	.quad	.LC18
	.quad	.LC36
	.quad	rsa_pub_decode
	.quad	rsa_pub_encode
	.quad	rsa_pub_cmp
	.quad	rsa_pub_print
	.quad	rsa_priv_decode
	.quad	rsa_priv_encode
	.quad	rsa_priv_print
	.quad	int_rsa_size
	.quad	rsa_bits
	.quad	rsa_security_bits
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	rsa_sig_print
	.quad	int_rsa_free
	.quad	rsa_pkey_ctrl
	.quad	old_rsa_priv_decode
	.quad	old_rsa_priv_encode
	.quad	rsa_item_verify
	.quad	rsa_item_sign
	.quad	rsa_sig_info_set
	.quad	rsa_pkey_check
	.zero	48
	.long	19
	.long	6
	.quad	1
	.zero	264
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
