	.file	"by_file.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"r"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../deps/openssl/openssl/crypto/x509/by_file.c"
	.section	.rodata.str1.1
.LC2:
	.string	""
	.text
	.p2align 4
	.type	X509_load_cert_crl_file.part.0, @function
X509_load_cert_crl_file.part.0:
.LFB858:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%rsi, %rdi
	pushq	%r14
	leaq	.LC0(%rip), %rsi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	call	BIO_new_file@PLT
	testq	%rax, %rax
	je	.L25
	leaq	.LC2(%rip), %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r12
	xorl	%r14d, %r14d
	call	PEM_X509_INFO_read_bio@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movq	%rax, %r13
	call	BIO_free@PLT
	testq	%r13, %r13
	jne	.L4
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L8:
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movq	(%rax), %rsi
	movq	%rax, %rbx
	testq	%rsi, %rsi
	je	.L5
	movq	24(%r15), %rdi
	call	X509_STORE_add_cert@PLT
	testl	%eax, %eax
	je	.L6
	addl	$1, %r14d
.L5:
	movq	8(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L7
	movq	24(%r15), %rdi
	call	X509_STORE_add_crl@PLT
	testl	%eax, %eax
	je	.L6
	addl	$1, %r14d
.L7:
	addl	$1, %r12d
.L4:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jl	.L8
	testl	%r14d, %r14d
	je	.L27
.L6:
	movq	X509_INFO_free@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_pop_free@PLT
.L1:
	addq	$8, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	movl	$222, %r8d
	movl	$136, %edx
	movl	$132, %esi
	movl	$11, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L25:
	movl	$199, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$2, %edx
	xorl	%r14d, %r14d
	movl	$132, %esi
	movl	$11, %edi
	call	ERR_put_error@PLT
	jmp	.L1
.L26:
	movl	$205, %r8d
	movl	$9, %edx
	movl	$132, %esi
	movl	$11, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1
	.cfi_endproc
.LFE858:
	.size	X509_load_cert_crl_file.part.0, .-X509_load_cert_crl_file.part.0
	.p2align 4
	.globl	X509_LOOKUP_file
	.type	X509_LOOKUP_file, @function
X509_LOOKUP_file:
.LFB853:
	.cfi_startproc
	endbr64
	leaq	x509_file_lookup(%rip), %rax
	ret
	.cfi_endproc
.LFE853:
	.size	X509_LOOKUP_file, .-X509_LOOKUP_file
	.p2align 4
	.globl	X509_load_cert_file
	.type	X509_load_cert_file, @function
X509_load_cert_file:
.LFB855:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L32
	movq	%r14, %rcx
	movl	$3, %edx
	movl	$108, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	testl	%eax, %eax
	jle	.L32
	cmpl	$1, %r12d
	je	.L50
	cmpl	$2, %r12d
	jne	.L37
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	d2i_X509_bio@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L51
	movq	24(%rbx), %rdi
	movq	%rax, %rsi
	call	X509_STORE_add_cert@PLT
	movl	%eax, %r8d
.L31:
	movq	%r12, %rdi
	movl	%r8d, -52(%rbp)
	call	X509_free@PLT
	movq	%r13, %rdi
	call	BIO_free@PLT
	movl	-52(%rbp), %r8d
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	movl	%r8d, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	xorl	%r14d, %r14d
	leaq	.LC2(%rip), %r15
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L34:
	movq	24(%rbx), %rdi
	movq	%rax, %rsi
	call	X509_STORE_add_cert@PLT
	movl	%eax, %r8d
	testl	%eax, %eax
	je	.L31
	movq	%r12, %rdi
	addl	$1, %r14d
	call	X509_free@PLT
.L36:
	movq	%r15, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	PEM_read_bio_X509_AUX@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L34
	call	ERR_peek_last_error@PLT
	andl	$4095, %eax
	cmpl	$108, %eax
	jne	.L35
	testl	%r14d, %r14d
	jne	.L52
.L35:
	movl	$97, %r8d
	movl	$9, %edx
	movl	$111, %esi
	movl	$11, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r8d, %r8d
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L32:
	movl	$84, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$2, %edx
.L49:
	movl	$111, %esi
	movl	$11, %edi
	xorl	%r12d, %r12d
	call	ERR_put_error@PLT
	xorl	%r8d, %r8d
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L37:
	movl	$120, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$100, %edx
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L52:
	call	ERR_clear_error@PLT
	movl	%r14d, %r8d
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L51:
	movl	$112, %r8d
	movl	$13, %edx
	movl	$111, %esi
	movl	$11, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r8d, %r8d
	jmp	.L31
	.cfi_endproc
.LFE855:
	.size	X509_load_cert_file, .-X509_load_cert_file
	.p2align 4
	.type	by_file_ctrl, @function
by_file_ctrl:
.LFB854:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$1, %esi
	je	.L65
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	cmpq	$3, %rcx
	je	.L66
	movq	%rdx, %rsi
	cmpq	$1, %rcx
	je	.L67
	movl	%ecx, %edx
	call	X509_load_cert_file
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
.L53:
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	call	X509_load_cert_crl_file.part.0
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L66:
	call	X509_get_default_cert_file_env@PLT
	movq	%rax, %rdi
	call	ossl_safe_getenv@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L56
.L64:
	movq	%r12, %rdi
	call	X509_load_cert_crl_file.part.0
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	testl	%eax, %eax
	jne	.L53
	movl	$60, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$104, %edx
	movl	%eax, -20(%rbp)
	movl	$101, %esi
	movl	$11, %edi
	call	ERR_put_error@PLT
	movl	-20(%rbp), %eax
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L56:
	call	X509_get_default_cert_file@PLT
	movq	%rax, %rsi
	jmp	.L64
	.cfi_endproc
.LFE854:
	.size	by_file_ctrl, .-by_file_ctrl
	.p2align 4
	.globl	X509_load_crl_file
	.type	X509_load_crl_file, @function
X509_load_crl_file:
.LFB856:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L71
	movq	%r14, %rcx
	movl	$3, %edx
	movl	$108, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	testl	%eax, %eax
	jle	.L71
	cmpl	$1, %r12d
	je	.L89
	cmpl	$2, %r12d
	jne	.L76
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	d2i_X509_CRL_bio@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L90
	movq	24(%rbx), %rdi
	movq	%rax, %rsi
	call	X509_STORE_add_crl@PLT
	movl	%eax, %r8d
.L70:
	movq	%r12, %rdi
	movl	%r8d, -52(%rbp)
	call	X509_CRL_free@PLT
	movq	%r13, %rdi
	call	BIO_free@PLT
	movl	-52(%rbp), %r8d
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	movl	%r8d, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	xorl	%r14d, %r14d
	leaq	.LC2(%rip), %r15
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L73:
	movq	24(%rbx), %rdi
	movq	%rax, %rsi
	call	X509_STORE_add_crl@PLT
	movl	%eax, %r8d
	testl	%eax, %eax
	je	.L70
	movq	%r12, %rdi
	addl	$1, %r14d
	call	X509_CRL_free@PLT
.L75:
	movq	%r15, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	PEM_read_bio_X509_CRL@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L73
	call	ERR_peek_last_error@PLT
	andl	$4095, %eax
	cmpl	$108, %eax
	jne	.L74
	testl	%r14d, %r14d
	jne	.L91
.L74:
	movl	$154, %r8d
	movl	$9, %edx
	movl	$112, %esi
	movl	$11, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r8d, %r8d
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L71:
	movl	$141, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$2, %edx
.L88:
	movl	$112, %esi
	movl	$11, %edi
	xorl	%r12d, %r12d
	call	ERR_put_error@PLT
	xorl	%r8d, %r8d
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L76:
	movl	$177, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$100, %edx
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L91:
	call	ERR_clear_error@PLT
	movl	%r14d, %r8d
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L90:
	movl	$169, %r8d
	movl	$13, %edx
	movl	$112, %esi
	movl	$11, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r8d, %r8d
	jmp	.L70
	.cfi_endproc
.LFE856:
	.size	X509_load_crl_file, .-X509_load_crl_file
	.p2align 4
	.globl	X509_load_cert_crl_file
	.type	X509_load_cert_crl_file, @function
X509_load_cert_crl_file:
.LFB857:
	.cfi_startproc
	endbr64
	cmpl	$1, %edx
	je	.L93
	jmp	X509_load_cert_file
	.p2align 4,,10
	.p2align 3
.L93:
	jmp	X509_load_cert_crl_file.part.0
	.cfi_endproc
.LFE857:
	.size	X509_load_cert_crl_file, .-X509_load_cert_crl_file
	.section	.rodata.str1.1
.LC3:
	.string	"Load file into cache"
	.section	.data.rel.local,"aw"
	.align 32
	.type	x509_file_lookup, @object
	.size	x509_file_lookup, 80
x509_file_lookup:
	.quad	.LC3
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	by_file_ctrl
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
