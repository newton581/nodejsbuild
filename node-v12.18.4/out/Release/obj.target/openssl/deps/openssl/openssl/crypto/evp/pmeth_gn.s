	.file	"pmeth_gn.c"
	.text
	.p2align 4
	.type	trans_cb, @function
trans_cb:
.LFB475:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%edi, %r12d
	movq	%rdx, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	call	BN_GENCB_get_arg@PLT
	movq	%rax, %rdi
	movq	64(%rax), %rax
	movl	%r12d, (%rax)
	movl	%ebx, 4(%rax)
	movq	56(%rdi), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE475:
	.size	trans_cb, .-trans_cb
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/evp/pmeth_gn.c"
	.text
	.p2align 4
	.globl	EVP_PKEY_paramgen_init
	.type	EVP_PKEY_paramgen_init, @function
EVP_PKEY_paramgen_init:
.LFB469:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	testq	%rdi, %rdi
	je	.L5
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	testq	%rax, %rax
	je	.L5
	cmpq	$0, 40(%rax)
	je	.L5
	movq	32(%rax), %rax
	movl	$2, 32(%rdi)
	testq	%rax, %rax
	je	.L8
	call	*%rax
	testl	%eax, %eax
	jle	.L16
.L4:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movl	$0, 32(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	movl	$23, %r8d
	movl	$150, %edx
	movl	$149, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	jmp	.L4
	.cfi_endproc
.LFE469:
	.size	EVP_PKEY_paramgen_init, .-EVP_PKEY_paramgen_init
	.p2align 4
	.globl	EVP_PKEY_paramgen
	.type	EVP_PKEY_paramgen, @function
EVP_PKEY_paramgen:
.LFB470:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	testq	%rdi, %rdi
	je	.L18
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L18
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L18
	cmpl	$2, 32(%rdi)
	jne	.L32
	movq	%rsi, %rbx
	testq	%rsi, %rsi
	je	.L24
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L33
.L22:
	call	*%rax
	testl	%eax, %eax
	jle	.L34
.L17:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	movq	(%rbx), %rdi
	movl	%eax, -24(%rbp)
	call	EVP_PKEY_free@PLT
	movq	$0, (%rbx)
	movl	-24(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	movq	%rdi, -24(%rbp)
	call	EVP_PKEY_new@PLT
	movq	-24(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, (%rbx)
	movq	%rax, %rsi
	je	.L35
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$40, %r8d
	movl	$150, %edx
	movl	$148, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	jmp	.L17
.L32:
	movl	$46, %r8d
	movl	$151, %edx
	movl	$148, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L17
.L24:
	movl	$-1, %eax
	jmp	.L17
.L35:
	movl	$57, %r8d
	movl	$65, %edx
	movl	$148, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L17
	.cfi_endproc
.LFE470:
	.size	EVP_PKEY_paramgen, .-EVP_PKEY_paramgen
	.p2align 4
	.globl	EVP_PKEY_keygen_init
	.type	EVP_PKEY_keygen_init, @function
EVP_PKEY_keygen_init:
.LFB471:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	testq	%rdi, %rdi
	je	.L37
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	testq	%rax, %rax
	je	.L37
	cmpq	$0, 56(%rax)
	je	.L37
	movq	48(%rax), %rax
	movl	$4, 32(%rdi)
	testq	%rax, %rax
	je	.L40
	call	*%rax
	testl	%eax, %eax
	jle	.L48
.L36:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	movl	$0, 32(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	movl	$73, %r8d
	movl	$150, %edx
	movl	$147, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	jmp	.L36
	.cfi_endproc
.LFE471:
	.size	EVP_PKEY_keygen_init, .-EVP_PKEY_keygen_init
	.p2align 4
	.globl	EVP_PKEY_keygen
	.type	EVP_PKEY_keygen, @function
EVP_PKEY_keygen:
.LFB472:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	testq	%rdi, %rdi
	je	.L50
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L50
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L50
	cmpl	$4, 32(%rdi)
	jne	.L67
	movq	%rsi, %rbx
	testq	%rsi, %rsi
	je	.L56
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L68
.L55:
	call	*%rax
	testl	%eax, %eax
	jle	.L69
.L49:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	movq	(%rbx), %rdi
	movl	%eax, -24(%rbp)
	call	EVP_PKEY_free@PLT
	movq	$0, (%rbx)
	movl	-24(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	movq	%rdi, -24(%rbp)
	call	EVP_PKEY_new@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L56
	movq	-24(%rbp), %rdi
	movq	(%rdi), %rax
	movq	56(%rax), %rax
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L50:
	movl	$91, %r8d
	movl	$150, %edx
	movl	$146, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	jmp	.L49
.L67:
	movl	$96, %r8d
	movl	$151, %edx
	movl	$146, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L49
.L56:
	movl	$-1, %eax
	jmp	.L49
	.cfi_endproc
.LFE472:
	.size	EVP_PKEY_keygen, .-EVP_PKEY_keygen
	.p2align 4
	.globl	EVP_PKEY_CTX_set_cb
	.type	EVP_PKEY_CTX_set_cb, @function
EVP_PKEY_CTX_set_cb:
.LFB473:
	.cfi_startproc
	endbr64
	movq	%rsi, 56(%rdi)
	ret
	.cfi_endproc
.LFE473:
	.size	EVP_PKEY_CTX_set_cb, .-EVP_PKEY_CTX_set_cb
	.p2align 4
	.globl	EVP_PKEY_CTX_get_cb
	.type	EVP_PKEY_CTX_get_cb, @function
EVP_PKEY_CTX_get_cb:
.LFB474:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rax
	ret
	.cfi_endproc
.LFE474:
	.size	EVP_PKEY_CTX_get_cb, .-EVP_PKEY_CTX_get_cb
	.p2align 4
	.globl	evp_pkey_set_cb_translate
	.type	evp_pkey_set_cb_translate, @function
evp_pkey_set_cb_translate:
.LFB476:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	leaq	trans_cb(%rip), %rsi
	jmp	BN_GENCB_set@PLT
	.cfi_endproc
.LFE476:
	.size	evp_pkey_set_cb_translate, .-evp_pkey_set_cb_translate
	.p2align 4
	.globl	EVP_PKEY_CTX_get_keygen_info
	.type	EVP_PKEY_CTX_get_keygen_info, @function
EVP_PKEY_CTX_get_keygen_info:
.LFB477:
	.cfi_startproc
	endbr64
	cmpl	$-1, %esi
	je	.L78
	xorl	%eax, %eax
	testl	%esi, %esi
	js	.L73
	cmpl	%esi, 72(%rdi)
	jl	.L73
	movq	64(%rdi), %rax
	movslq	%esi, %rsi
	movl	(%rax,%rsi,4), %eax
.L73:
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	movl	72(%rdi), %eax
	ret
	.cfi_endproc
.LFE477:
	.size	EVP_PKEY_CTX_get_keygen_info, .-EVP_PKEY_CTX_get_keygen_info
	.p2align 4
	.globl	EVP_PKEY_new_mac_key
	.type	EVP_PKEY_new_mac_key, @function
EVP_PKEY_new_mac_key:
.LFB478:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	%ecx, %ebx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	call	EVP_PKEY_CTX_new_id@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L79
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L81
	cmpq	$0, 56(%rax)
	je	.L81
	movl	$4, 32(%r12)
	movq	48(%rax), %rax
	testq	%rax, %rax
	je	.L84
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	jle	.L96
.L84:
	movq	%r13, %r9
	movl	%ebx, %r8d
	movl	$6, %ecx
	movl	$4, %edx
	movl	$-1, %esi
	movq	%r12, %rdi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L83
	leaq	-48(%rbp), %rsi
	movq	%r12, %rdi
	call	EVP_PKEY_keygen
.L83:
	movq	%r12, %rdi
	call	EVP_PKEY_CTX_free@PLT
	movq	-48(%rbp), %r12
.L79:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L97
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	movl	$0, 32(%r12)
	jmp	.L83
.L81:
	movl	$73, %r8d
	movl	$150, %edx
	movl	$147, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L83
.L97:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE478:
	.size	EVP_PKEY_new_mac_key, .-EVP_PKEY_new_mac_key
	.p2align 4
	.globl	EVP_PKEY_check
	.type	EVP_PKEY_check, @function
EVP_PKEY_check:
.LFB479:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	16(%rdi), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%r8, %r8
	je	.L108
	movq	(%rdi), %rax
	movq	224(%rax), %rax
	testq	%rax, %rax
	je	.L101
.L103:
	movq	%r8, %rdi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	movq	16(%r8), %rax
	testq	%rax, %rax
	je	.L102
	movq	224(%rax), %rax
	testq	%rax, %rax
	jne	.L103
.L102:
	movl	$187, %r8d
	movl	$150, %edx
	movl	$186, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	movl	$177, %r8d
	movl	$154, %edx
	movl	$186, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE479:
	.size	EVP_PKEY_check, .-EVP_PKEY_check
	.p2align 4
	.globl	EVP_PKEY_public_check
	.type	EVP_PKEY_public_check, @function
EVP_PKEY_public_check:
.LFB480:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	16(%rdi), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%r8, %r8
	je	.L119
	movq	(%rdi), %rax
	movq	232(%rax), %rax
	testq	%rax, %rax
	je	.L112
.L114:
	movq	%r8, %rdi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	movq	16(%r8), %rax
	testq	%rax, %rax
	je	.L113
	movq	232(%rax), %rax
	testq	%rax, %rax
	jne	.L114
.L113:
	movl	$210, %r8d
	movl	$150, %edx
	movl	$190, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	.cfi_restore_state
	movl	$200, %r8d
	movl	$154, %edx
	movl	$190, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE480:
	.size	EVP_PKEY_public_check, .-EVP_PKEY_public_check
	.p2align 4
	.globl	EVP_PKEY_param_check
	.type	EVP_PKEY_param_check, @function
EVP_PKEY_param_check:
.LFB481:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	16(%rdi), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%r8, %r8
	je	.L130
	movq	(%rdi), %rax
	movq	240(%rax), %rax
	testq	%rax, %rax
	je	.L123
.L125:
	movq	%r8, %rdi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore_state
	movq	16(%r8), %rax
	testq	%rax, %rax
	je	.L124
	movq	240(%rax), %rax
	testq	%rax, %rax
	jne	.L125
.L124:
	movl	$233, %r8d
	movl	$150, %edx
	movl	$189, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	movl	$223, %r8d
	movl	$154, %edx
	movl	$189, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE481:
	.size	EVP_PKEY_param_check, .-EVP_PKEY_param_check
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
