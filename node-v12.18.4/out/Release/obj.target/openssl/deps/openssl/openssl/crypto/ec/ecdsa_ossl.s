	.file	"ecdsa_ossl.c"
	.text
	.p2align 4
	.globl	ossl_ecdsa_sign
	.type	ossl_ecdsa_sign, @function
ossl_ecdsa_sign:
.LFB388:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	%edx, %esi
	movq	%r9, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%r8, %rbx
	subq	$16, %rsp
	movq	%rcx, -24(%rbp)
	movq	24(%rbp), %r8
	movq	16(%rbp), %rcx
	call	ECDSA_do_sign_ex@PLT
	testq	%rax, %rax
	je	.L6
	leaq	-24(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	i2d_ECDSA_SIG@PLT
	movq	%r12, %rdi
	movl	%eax, (%rbx)
	call	ECDSA_SIG_free@PLT
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movl	$0, (%rbx)
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE388:
	.size	ossl_ecdsa_sign, .-ossl_ecdsa_sign
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ec/ecdsa_ossl.c"
	.text
	.p2align 4
	.globl	ossl_ecdsa_sign_setup
	.type	ossl_ecdsa_sign_setup, @function
ossl_ecdsa_sign_setup:
.LFB390:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -56(%rbp)
	movq	%rcx, -64(%rbp)
	testq	%rdi, %rdi
	je	.L10
	movq	%rdi, %r13
	movq	%rsi, %rbx
	call	EC_KEY_get0_group@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L10
	movq	%r13, %rdi
	call	EC_KEY_get0_private_key@PLT
	testq	%rax, %rax
	je	.L46
	movq	%r13, %rdi
	call	EC_KEY_can_sign@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L47
	movq	%rbx, -72(%rbp)
	testq	%rbx, %rbx
	je	.L48
.L13:
	call	BN_new@PLT
	movq	%rax, %r13
	call	BN_new@PLT
	movq	%rax, %r14
	call	BN_new@PLT
	testq	%r13, %r13
	sete	%dl
	testq	%r14, %r14
	movq	%rax, -80(%rbp)
	movq	%rax, %rcx
	sete	%al
	orb	%al, %dl
	jne	.L28
	testq	%rcx, %rcx
	je	.L28
	movq	%r12, %rdi
	call	EC_POINT_new@PLT
	movl	$75, %r8d
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L45
	movq	%r12, %rdi
	call	EC_GROUP_get0_order@PLT
	movq	%rax, %rdi
	movq	%rax, %r15
	call	BN_num_bits@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	movl	%eax, -92(%rbp)
	call	BN_set_bit@PLT
	testl	%eax, %eax
	je	.L16
	movl	-92(%rbp), %esi
	movq	%r14, %rdi
	call	BN_set_bit@PLT
	testl	%eax, %eax
	je	.L16
	movl	-92(%rbp), %esi
	movq	-80(%rbp), %rdi
	call	BN_set_bit@PLT
	testl	%eax, %eax
	jne	.L19
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L18:
	movq	%r13, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L49
.L19:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	BN_priv_rand_range@PLT
	testl	%eax, %eax
	jne	.L18
	movl	$99, %r8d
	movl	$158, %edx
	movl	$248, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L10:
	movl	$47, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$67, %edx
	xorl	%r13d, %r13d
	movl	$248, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
.L7:
	addq	$56, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	movl	$51, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$125, %edx
	xorl	%r13d, %r13d
	movl	$248, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L28:
	movl	$71, %r8d
	movl	$65, %edx
	movl	$248, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	$0, -88(%rbp)
.L16:
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	BN_clear_free@PLT
	movq	%r14, %rdi
	call	BN_clear_free@PLT
.L25:
	movq	-72(%rbp), %rax
	cmpq	%rax, %rbx
	je	.L26
	movq	%rax, %rdi
	call	BN_CTX_free@PLT
.L26:
	movq	-88(%rbp), %rdi
	call	EC_POINT_free@PLT
	movq	-80(%rbp), %rdi
	call	BN_clear_free@PLT
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L48:
	call	BN_CTX_new@PLT
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	jne	.L13
	movl	$62, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$248, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L47:
	movl	$56, %r8d
	movl	$159, %edx
	movl	$248, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L7
.L50:
	movl	$108, %r8d
	.p2align 4,,10
	.p2align 3
.L45:
	movl	$16, %edx
	movl	$248, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L49:
	movq	-72(%rbp), %r9
	movq	-88(%rbp), %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	EC_POINT_mul@PLT
	testl	%eax, %eax
	je	.L50
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	-88(%rbp), %rsi
	call	EC_POINT_get_affine_coordinates@PLT
	testl	%eax, %eax
	je	.L51
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	BN_nnmod@PLT
	testl	%eax, %eax
	je	.L52
	movq	%r14, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L19
	movq	-72(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	ec_group_do_inverse_ord@PLT
	testl	%eax, %eax
	je	.L53
	movq	-64(%rbp), %r15
	movq	(%r15), %rdi
	call	BN_clear_free@PLT
	movq	-56(%rbp), %r12
	movq	(%r12), %rdi
	call	BN_clear_free@PLT
	movq	%r14, (%r15)
	movq	%r13, (%r12)
	movl	$1, %r13d
	jmp	.L25
.L52:
	movl	$118, %r8d
.L44:
	movl	$3, %edx
	movl	$248, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L16
.L51:
	movl	$113, %r8d
	jmp	.L45
.L53:
	movl	$125, %r8d
	jmp	.L44
	.cfi_endproc
.LFE390:
	.size	ossl_ecdsa_sign_setup, .-ossl_ecdsa_sign_setup
	.p2align 4
	.globl	ossl_ecdsa_sign_sig
	.type	ossl_ecdsa_sign_sig, @function
ossl_ecdsa_sign_sig:
.LFB391:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -128(%rbp)
	movq	%r8, %rdi
	movl	%esi, -88(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%rcx, -72(%rbp)
	call	EC_KEY_get0_group@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	EC_KEY_get0_private_key@PLT
	testq	%r12, %r12
	je	.L141
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L142
	movq	%r14, %rdi
	call	EC_KEY_can_sign@PLT
	testl	%eax, %eax
	je	.L143
	call	ECDSA_SIG_new@PLT
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L144
	call	BN_new@PLT
	movq	-56(%rbp), %r13
	movq	%rax, 0(%r13)
	call	BN_new@PLT
	cmpq	$0, 0(%r13)
	movq	%rax, 8(%r13)
	movq	%rax, %rbx
	je	.L99
	testq	%rax, %rax
	je	.L99
	call	BN_CTX_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L64
	call	BN_new@PLT
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L64
	movq	%r12, %rdi
	call	EC_GROUP_get0_order@PLT
	movq	%rax, %rdi
	movq	%rax, -96(%rbp)
	call	BN_num_bits@PLT
	movl	-88(%rbp), %esi
	movl	%eax, %ecx
	leal	0(,%rsi,8), %eax
	cmpl	%ecx, %eax
	jg	.L145
	movq	-64(%rbp), %rdx
	movq	-128(%rbp), %rdi
	call	BN_bin2bn@PLT
	testq	%rax, %rax
	je	.L96
.L67:
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rcx
	movq	$0, -120(%rbp)
	movq	%r15, -104(%rbp)
	testq	%rsi, %rsi
	sete	%dl
	testq	%rcx, %rcx
	sete	%al
	orl	%eax, %edx
	testq	%rsi, %rsi
	movb	%dl, -84(%rbp)
	setne	%dl
	testq	%rcx, %rcx
	setne	%al
	andl	%eax, %edx
	movb	%dl, -105(%rbp)
	.p2align 4,,10
	.p2align 3
.L95:
	cmpb	$0, -84(%rbp)
	je	.L68
	testq	%r14, %r14
	je	.L71
	movq	%r14, %rdi
	call	EC_KEY_get0_group@PLT
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L71
	movq	%r14, %rdi
	call	EC_KEY_get0_private_key@PLT
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L146
	movq	%r14, %rdi
	call	EC_KEY_can_sign@PLT
	testl	%eax, %eax
	je	.L147
	call	BN_new@PLT
	movq	%rax, %r15
	call	BN_new@PLT
	movq	%rax, -144(%rbp)
	call	BN_new@PLT
	movq	%rax, -160(%rbp)
	testq	%r15, %r15
	je	.L74
	cmpq	$0, -144(%rbp)
	je	.L74
	testq	%rax, %rax
	je	.L74
	movq	-136(%rbp), %rdi
	call	EC_POINT_new@PLT
	movq	%rax, -168(%rbp)
	testq	%rax, %rax
	je	.L148
	movq	-136(%rbp), %rdi
	call	EC_GROUP_get0_order@PLT
	movq	%rax, %rdi
	movq	%rax, -184(%rbp)
	call	BN_num_bits@PLT
	movq	%r15, %rdi
	movl	%eax, %esi
	movl	%eax, -176(%rbp)
	call	BN_set_bit@PLT
	testl	%eax, %eax
	je	.L133
	movl	-176(%rbp), %esi
	movq	-144(%rbp), %rdi
	call	BN_set_bit@PLT
	testl	%eax, %eax
	jne	.L149
.L133:
	movq	%r15, %rbx
.L76:
	movq	%rbx, %rdi
	call	BN_clear_free@PLT
	movq	-144(%rbp), %rdi
	call	BN_clear_free@PLT
	movq	-168(%rbp), %rdi
	call	EC_POINT_free@PLT
	movq	-160(%rbp), %rdi
	call	BN_clear_free@PLT
.L70:
	movl	$221, %r8d
	movl	$42, %edx
	movl	$249, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L99:
	movl	$191, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$249, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	movq	$0, -120(%rbp)
	movq	$0, -64(%rbp)
.L62:
	movq	-56(%rbp), %rdi
	xorl	%r15d, %r15d
	call	ECDSA_SIG_free@PLT
.L94:
	movq	%r13, %rdi
	call	BN_CTX_free@PLT
	movq	-64(%rbp), %rdi
	call	BN_clear_free@PLT
	movq	-120(%rbp), %rdi
	call	BN_clear_free@PLT
.L54:
	addq	$152, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	movl	%ecx, %esi
	leal	14(%rcx), %edx
	movq	-128(%rbp), %rdi
	movl	%ecx, -104(%rbp)
	addl	$7, %esi
	movl	%esi, %eax
	cmovs	%edx, %eax
	movq	-64(%rbp), %rdx
	movl	%eax, %esi
	sarl	$3, %esi
	movl	%esi, -88(%rbp)
	movl	%esi, -84(%rbp)
	call	BN_bin2bn@PLT
	movl	-84(%rbp), %esi
	movl	-104(%rbp), %ecx
	testq	%rax, %rax
	je	.L96
	leal	0(,%rsi,8), %eax
	cmpl	%ecx, %eax
	jle	.L67
	movq	-64(%rbp), %rsi
	movl	%ecx, %eax
	movl	$8, %edx
	andl	$7, %eax
	subl	%eax, %edx
	movq	%rsi, %rdi
	call	BN_rshift@PLT
	testl	%eax, %eax
	jne	.L67
	movl	$215, %r8d
.L140:
	movl	$3, %edx
	movl	$249, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	$0, -120(%rbp)
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L68:
	movq	-56(%rbp), %rax
	movq	-72(%rbp), %rsi
	movq	(%rax), %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L150
	movq	-80(%rbp), %r15
.L88:
	movq	-56(%rbp), %rax
	movq	144(%r12), %rdx
	movq	%r13, %rcx
	movq	%rbx, %rdi
	movq	(%rax), %rsi
	call	bn_to_mont_fixed_top@PLT
	testl	%eax, %eax
	je	.L90
	movq	-104(%rbp), %rdx
	movq	%r13, %r8
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	movq	144(%r12), %rcx
	call	bn_mul_mont_fixed_top@PLT
	testl	%eax, %eax
	je	.L90
	movq	-96(%rbp), %rcx
	movq	-64(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	bn_mod_add_fixed_top@PLT
	testl	%eax, %eax
	je	.L151
	movq	144(%r12), %rdx
	movq	%r13, %rcx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	bn_to_mont_fixed_top@PLT
	testl	%eax, %eax
	je	.L93
	movq	%r13, %r8
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	movq	144(%r12), %rcx
	call	BN_mod_mul_montgomery@PLT
	testl	%eax, %eax
	je	.L93
	movq	%rbx, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L98
	cmpb	$0, -105(%rbp)
	je	.L95
	movl	$265, %r8d
	movl	$157, %edx
	movl	$249, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L71:
	movl	$47, %r8d
	movl	$67, %edx
	movl	$248, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L64:
	movl	$198, %r8d
	movl	$65, %edx
	movl	$249, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	$0, -120(%rbp)
	movq	$0, -64(%rbp)
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L143:
	movl	$179, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$159, %edx
	xorl	%r15d, %r15d
	movl	$249, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L74:
	movl	$71, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movq	%r15, %rbx
	movl	$248, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	movq	$0, -168(%rbp)
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L142:
	movl	$174, %r8d
	movl	$125, %edx
	movl	$249, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L146:
	movl	$51, %r8d
	movl	$125, %edx
	movl	$248, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L147:
	movl	$56, %r8d
	movl	$159, %edx
	movl	$248, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L144:
	movl	$185, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r15d, %r15d
	movl	$249, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L90:
	movl	$242, %r8d
.L139:
	movl	$3, %edx
	movl	$249, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L149:
	movl	-176(%rbp), %esi
	movq	-160(%rbp), %rdi
	call	BN_set_bit@PLT
	testl	%eax, %eax
	je	.L133
	movslq	-88(%rbp), %rsi
	movq	%r12, -192(%rbp)
	movq	-128(%rbp), %r12
	movq	%rbx, -176(%rbp)
	movq	-184(%rbp), %rbx
	movq	%r14, -184(%rbp)
	movq	%rsi, %r14
.L86:
	testq	%r12, %r12
	jne	.L78
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L82:
	movq	%r15, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L81
.L78:
	movq	-152(%rbp), %rdx
	movq	%r13, %r9
	movq	%r14, %r8
	movq	%r12, %rcx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	BN_generate_dsa_nonce@PLT
	testl	%eax, %eax
	jne	.L82
	movq	%r15, %rbx
	movl	$93, %r8d
.L138:
	movl	$158, %edx
	movl	$248, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L152:
	movq	%r15, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L81
.L80:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	BN_priv_rand_range@PLT
	testl	%eax, %eax
	jne	.L152
	movq	%r15, %rbx
	movl	$99, %r8d
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L93:
	movl	$255, %r8d
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L141:
	movl	$170, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$67, %edx
	xorl	%r15d, %r15d
	movl	$249, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L151:
	movl	$246, %r8d
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L148:
	movq	%r15, %rbx
	movl	$75, %r8d
.L136:
	movl	$16, %edx
	movl	$248, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L98:
	movq	-56(%rbp), %r15
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L96:
	movl	$210, %r8d
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L81:
	movq	-168(%rbp), %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %r9
	movq	-136(%rbp), %rdi
	movq	%r15, %rdx
	call	EC_POINT_mul@PLT
	testl	%eax, %eax
	je	.L153
	movq	-160(%rbp), %rdx
	movq	-168(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r13, %r8
	movq	-136(%rbp), %rdi
	call	EC_POINT_get_affine_coordinates@PLT
	testl	%eax, %eax
	je	.L154
	movq	-160(%rbp), %rsi
	movq	-144(%rbp), %rdi
	movq	%r13, %rcx
	movq	%rbx, %rdx
	call	BN_nnmod@PLT
	testl	%eax, %eax
	je	.L155
	movq	-144(%rbp), %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L86
	movq	-136(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r15, %rsi
	movq	-176(%rbp), %rbx
	movq	-192(%rbp), %r12
	movq	-184(%rbp), %r14
	call	ec_group_do_inverse_ord@PLT
	testl	%eax, %eax
	je	.L156
	movq	-56(%rbp), %rax
	movq	(%rax), %rdi
	call	BN_clear_free@PLT
	movq	-120(%rbp), %rdi
	call	BN_clear_free@PLT
	movq	-56(%rbp), %rax
	movq	-144(%rbp), %rsi
	movq	-168(%rbp), %rdi
	movq	%rsi, (%rax)
	call	EC_POINT_free@PLT
	movq	-160(%rbp), %rdi
	call	BN_clear_free@PLT
	movq	%r15, -120(%rbp)
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L150:
	movl	$228, %r8d
	movl	$65, %edx
	movl	$249, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L62
.L155:
	movq	%r15, %rbx
	movl	$118, %r8d
.L137:
	movl	$3, %edx
	movl	$248, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L76
.L154:
	movq	%r15, %rbx
	movl	$113, %r8d
	jmp	.L136
.L153:
	movq	%r15, %rbx
	movl	$108, %r8d
	jmp	.L136
.L156:
	movq	%r15, %rbx
	movl	$125, %r8d
	jmp	.L137
	.cfi_endproc
.LFE391:
	.size	ossl_ecdsa_sign_sig, .-ossl_ecdsa_sign_sig
	.p2align 4
	.globl	ossl_ecdsa_verify
	.type	ossl_ecdsa_verify, @function
ossl_ecdsa_verify:
.LFB392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$56, %rsp
	movq	%rsi, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rcx, -72(%rbp)
	movq	$0, -64(%rbp)
	call	ECDSA_SIG_new@PLT
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L163
	movslq	%ebx, %r13
	leaq	-72(%rbp), %rsi
	leaq	-80(%rbp), %rdi
	movq	%r13, %rdx
	call	d2i_ECDSA_SIG@PLT
	testq	%rax, %rax
	je	.L166
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rsi
	call	i2d_ECDSA_SIG@PLT
	cmpl	%eax, %ebx
	movq	-64(%rbp), %rbx
	je	.L161
	movl	$-1, %r12d
.L160:
	movq	%rbx, %rdi
	movl	$312, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-80(%rbp), %rdi
	call	ECDSA_SIG_free@PLT
.L157:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L167
	addq	$56, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movl	$-1, %r12d
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L160
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rdi
	movq	%r15, %rcx
	movl	%r14d, %esi
	call	ECDSA_do_verify@PLT
	movq	-64(%rbp), %rbx
	movl	%eax, %r12d
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L166:
	movq	-64(%rbp), %rbx
	movl	$-1, %r12d
	jmp	.L160
.L163:
	movl	$-1, %r12d
	jmp	.L157
.L167:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE392:
	.size	ossl_ecdsa_verify, .-ossl_ecdsa_verify
	.p2align 4
	.globl	ossl_ecdsa_verify_sig
	.type	ossl_ecdsa_verify_sig, @function
ossl_ecdsa_verify_sig:
.LFB393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, -52(%rbp)
	testq	%rcx, %rcx
	je	.L171
	movq	%rdi, %r13
	movq	%rcx, %rdi
	movq	%rdx, %rbx
	movq	%rcx, %r15
	call	EC_KEY_get0_group@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L171
	movq	%r15, %rdi
	call	EC_KEY_get0_public_key@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L171
	testq	%rbx, %rbx
	je	.L171
	movq	%r15, %rdi
	call	EC_KEY_can_sign@PLT
	testl	%eax, %eax
	je	.L200
	call	BN_CTX_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L201
	movq	%rax, %rdi
	call	BN_CTX_start@PLT
	movq	%r15, %rdi
	call	BN_CTX_get@PLT
	movq	%r15, %rdi
	movq	%rax, -80(%rbp)
	call	BN_CTX_get@PLT
	movq	%r15, %rdi
	movq	%rax, -88(%rbp)
	call	BN_CTX_get@PLT
	movq	%r15, %rdi
	movq	%rax, -96(%rbp)
	call	BN_CTX_get@PLT
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L202
	movq	%r12, %rdi
	call	EC_GROUP_get0_order@PLT
	movl	$357, %r8d
	movl	$16, %edx
	leaq	.LC0(%rip), %rcx
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	je	.L197
	movq	(%rbx), %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L178
	movq	(%rbx), %rdi
	call	BN_is_negative@PLT
	testl	%eax, %eax
	je	.L203
.L178:
	movl	$364, %r8d
	movl	$156, %edx
	movl	$250, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	$0, -64(%rbp)
	xorl	%eax, %eax
.L175:
	movq	%r15, %rdi
	movl	%eax, -52(%rbp)
	call	BN_CTX_end@PLT
	movq	%r15, %rdi
	call	BN_CTX_free@PLT
	movq	-64(%rbp), %rdi
	call	EC_POINT_free@PLT
	movl	-52(%rbp), %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	movl	$331, %r8d
	movl	$124, %edx
	movl	$250, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
.L168:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L200:
	.cfi_restore_state
	movl	$336, %r8d
	movl	$159, %edx
	movl	$250, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$56, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L201:
	.cfi_restore_state
	movl	$342, %r8d
	movl	$65, %edx
	movl	$250, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L203:
	movq	-72(%rbp), %rsi
	movq	(%rbx), %rdi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	jns	.L178
	movq	8(%rbx), %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L178
	movq	8(%rbx), %rdi
	call	BN_is_negative@PLT
	testl	%eax, %eax
	jne	.L178
	movq	8(%rbx), %rdi
	movq	-72(%rbp), %rsi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	jns	.L178
	movq	8(%rbx), %rdx
	movq	-88(%rbp), %rsi
	movq	%r15, %rcx
	movq	%r12, %rdi
	call	ec_group_do_inverse_ord@PLT
	testl	%eax, %eax
	je	.L204
	movq	-72(%rbp), %rdi
	call	BN_num_bits@PLT
	movl	%eax, %ecx
	movl	-52(%rbp), %eax
	sall	$3, %eax
	cmpl	%ecx, %eax
	jle	.L180
	leal	7(%rcx), %eax
	movl	$8, %r8d
	movq	%r13, %rdi
	movl	%ecx, -56(%rbp)
	cltd
	idivl	%r8d
	movq	-96(%rbp), %rdx
	movl	%eax, %esi
	movl	%eax, -52(%rbp)
	call	BN_bin2bn@PLT
	movl	-52(%rbp), %r9d
	movl	-56(%rbp), %ecx
	movl	$8, %r8d
	testq	%rax, %rax
	je	.L189
	leal	0(,%r9,8), %eax
	cmpl	%ecx, %eax
	jle	.L182
	movq	-96(%rbp), %rsi
	movl	%r8d, %edx
	andl	$7, %ecx
	subl	%ecx, %edx
	movq	%rsi, %rdi
	call	BN_rshift@PLT
	movl	$386, %r8d
	testl	%eax, %eax
	je	.L198
.L182:
	movq	-72(%rbp), %rcx
	movq	-88(%rbp), %rdx
	movq	%r15, %r8
	movq	-96(%rbp), %rsi
	movq	-80(%rbp), %rdi
	call	BN_mod_mul@PLT
	movl	$391, %r8d
	testl	%eax, %eax
	je	.L198
	movq	-88(%rbp), %rdx
	movq	-72(%rbp), %rcx
	movq	%r15, %r8
	movq	(%rbx), %rsi
	movq	%rdx, %rdi
	call	BN_mod_mul@PLT
	movl	$396, %r8d
	testl	%eax, %eax
	je	.L198
	movq	%r12, %rdi
	call	EC_POINT_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L205
	movq	-88(%rbp), %r8
	movq	-80(%rbp), %rdx
	movq	%r15, %r9
	movq	%r14, %rcx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	EC_POINT_mul@PLT
	testl	%eax, %eax
	je	.L206
	movq	-64(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%r15, %r8
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	EC_POINT_get_affine_coordinates@PLT
	testl	%eax, %eax
	je	.L207
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	%r15, %rcx
	movq	-80(%rbp), %rdi
	call	BN_nnmod@PLT
	testl	%eax, %eax
	je	.L208
	movq	(%rbx), %rsi
	movq	-80(%rbp), %rdi
	call	BN_ucmp@PLT
	movq	%r13, -64(%rbp)
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L202:
	movl	$351, %r8d
	movl	$3, %edx
	movl	$250, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L175
.L180:
	movq	-96(%rbp), %rdx
	movl	-52(%rbp), %esi
	movq	%r13, %rdi
	call	BN_bin2bn@PLT
	testq	%rax, %rax
	jne	.L182
.L189:
	movl	$381, %r8d
.L198:
	leaq	.LC0(%rip), %rcx
	movl	$3, %edx
	.p2align 4,,10
	.p2align 3
.L197:
	movl	$250, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	movq	$0, -64(%rbp)
	movl	$-1, %eax
	jmp	.L175
.L204:
	movl	$370, %r8d
	jmp	.L198
.L207:
	movl	$410, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$16, %edx
.L199:
	movl	$250, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	movq	%r13, -64(%rbp)
	orl	$-1, %eax
	jmp	.L175
.L206:
	movl	$405, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$16, %edx
	jmp	.L199
.L205:
	movl	$401, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	jmp	.L197
.L208:
	movl	$415, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$3, %edx
	jmp	.L199
	.cfi_endproc
.LFE393:
	.size	ossl_ecdsa_verify_sig, .-ossl_ecdsa_verify_sig
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
