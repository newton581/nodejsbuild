	.file	"rsa_lib.c"
	.text
	.p2align 4
	.globl	RSA_get_method
	.type	RSA_get_method, @function
RSA_get_method:
.LFB854:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE854:
	.size	RSA_get_method, .-RSA_get_method
	.p2align 4
	.globl	RSA_set_method
	.type	RSA_set_method, @function
RSA_set_method:
.LFB855:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	8(%rdi), %rax
	movq	%rsi, %rbx
	movq	64(%rax), %rax
	testq	%rax, %rax
	je	.L4
	call	*%rax
.L4:
	movq	16(%r12), %rdi
	call	ENGINE_finish@PLT
	movq	56(%rbx), %rax
	movq	%rbx, 8(%r12)
	movq	$0, 16(%r12)
	testq	%rax, %rax
	je	.L5
	movq	%r12, %rdi
	call	*%rax
.L5:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE855:
	.size	RSA_set_method, .-RSA_set_method
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/rsa/rsa_lib.c"
	.text
	.p2align 4
	.globl	RSA_free
	.type	RSA_free, @function
RSA_free:
.LFB857:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L27
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	lock xaddl	%eax, 112(%rdi)
	subl	$1, %eax
	testl	%eax, %eax
	je	.L30
	jle	.L17
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
.L17:
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L19
	movq	64(%rax), %rax
	testq	%rax, %rax
	je	.L19
	movq	%r12, %rdi
	call	*%rax
.L19:
	movq	16(%r12), %rdi
	call	ENGINE_finish@PLT
	leaq	104(%r12), %rdx
	movq	%r12, %rsi
	movl	$9, %edi
	call	CRYPTO_free_ex_data@PLT
	movq	168(%r12), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	movq	24(%r12), %rdi
	call	BN_free@PLT
	movq	32(%r12), %rdi
	call	BN_free@PLT
	movq	40(%r12), %rdi
	call	BN_clear_free@PLT
	movq	48(%r12), %rdi
	call	BN_clear_free@PLT
	movq	56(%r12), %rdi
	call	BN_clear_free@PLT
	movq	64(%r12), %rdi
	call	BN_clear_free@PLT
	movq	72(%r12), %rdi
	call	BN_clear_free@PLT
	movq	80(%r12), %rdi
	call	BN_clear_free@PLT
	movq	96(%r12), %rdi
	call	RSA_PSS_PARAMS_free@PLT
	movq	rsa_multip_info_free@GOTPCREL(%rip), %rsi
	movq	88(%r12), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	152(%r12), %rdi
	call	BN_BLINDING_free@PLT
	movq	160(%r12), %rdi
	call	BN_BLINDING_free@PLT
	movq	144(%r12), %rdi
	movl	$140, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$141, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L27:
	ret
	.cfi_endproc
.LFE857:
	.size	RSA_free, .-RSA_free
	.p2align 4
	.globl	RSA_new_method
	.type	RSA_new_method, @function
RSA_new_method:
.LFB856:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$52, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$176, %edi
	pushq	%r12
	.cfi_offset 12, -32
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L49
	movl	$1, 112(%rax)
	mfence
	call	CRYPTO_THREAD_lock_new@PLT
	movq	%rax, 168(%r12)
	testq	%rax, %rax
	je	.L50
	call	RSA_get_default_method@PLT
	movq	%rax, 8(%r12)
	movl	72(%rax), %eax
	andb	$-5, %ah
	movl	%eax, 116(%r12)
	testq	%r13, %r13
	je	.L35
	movq	%r13, %rdi
	call	ENGINE_init@PLT
	movl	$72, %r8d
	testl	%eax, %eax
	je	.L48
	movq	%r13, 16(%r12)
.L38:
	movq	%r13, %rdi
	call	ENGINE_get_RSA@PLT
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L51
.L40:
	movl	72(%rax), %eax
	leaq	104(%r12), %rdx
	movq	%r12, %rsi
	movl	$9, %edi
	andb	$-5, %ah
	movl	%eax, 116(%r12)
	call	CRYPTO_new_ex_data@PLT
	testl	%eax, %eax
	je	.L37
	movq	8(%r12), %rax
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L31
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L52
.L31:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	movl	$82, %r8d
.L48:
	movl	$38, %edx
	movl	$106, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L37:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	RSA_free
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	call	ENGINE_get_default_RSA@PLT
	movq	%rax, 16(%r12)
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L38
	movq	8(%r12), %rax
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L49:
	movl	$55, %r8d
	movl	$65, %edx
	movl	$106, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L50:
	movl	$62, %r8d
	movl	$65, %edx
	movl	$106, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movl	$63, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L52:
	movl	$94, %r8d
	movl	$70, %edx
	movl	$106, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L37
	.cfi_endproc
.LFE856:
	.size	RSA_new_method, .-RSA_new_method
	.p2align 4
	.globl	RSA_new
	.type	RSA_new, @function
RSA_new:
.LFB853:
	.cfi_startproc
	endbr64
	xorl	%edi, %edi
	jmp	RSA_new_method
	.cfi_endproc
.LFE853:
	.size	RSA_new, .-RSA_new
	.p2align 4
	.globl	RSA_up_ref
	.type	RSA_up_ref, @function
RSA_up_ref:
.LFB858:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	lock xaddl	%eax, 112(%rdi)
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE858:
	.size	RSA_up_ref, .-RSA_up_ref
	.p2align 4
	.globl	RSA_set_ex_data
	.type	RSA_set_ex_data, @function
RSA_set_ex_data:
.LFB859:
	.cfi_startproc
	endbr64
	addq	$104, %rdi
	jmp	CRYPTO_set_ex_data@PLT
	.cfi_endproc
.LFE859:
	.size	RSA_set_ex_data, .-RSA_set_ex_data
	.p2align 4
	.globl	RSA_get_ex_data
	.type	RSA_get_ex_data, @function
RSA_get_ex_data:
.LFB860:
	.cfi_startproc
	endbr64
	addq	$104, %rdi
	jmp	CRYPTO_get_ex_data@PLT
	.cfi_endproc
.LFE860:
	.size	RSA_get_ex_data, .-RSA_get_ex_data
	.p2align 4
	.globl	RSA_security_bits
	.type	RSA_security_bits, @function
RSA_security_bits:
.LFB861:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	24(%rdi), %rdi
	call	BN_num_bits@PLT
	cmpl	$1, 4(%rbx)
	movl	%eax, %r12d
	je	.L58
.L62:
	popq	%rbx
	movl	%r12d, %edi
	movl	$-1, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BN_security_bits@PLT
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	movq	88(%rbx), %rdi
	call	OPENSSL_sk_num@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	jle	.L60
	movl	%r12d, %edi
	addl	$1, %ebx
	call	rsa_multip_cap@PLT
	cmpl	%eax, %ebx
	jl	.L62
.L60:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE861:
	.size	RSA_security_bits, .-RSA_security_bits
	.p2align 4
	.globl	RSA_set0_key
	.type	RSA_set0_key, @function
RSA_set0_key:
.LFB862:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L88
	cmpq	$0, 32(%rbx)
	je	.L89
	testq	%rsi, %rsi
	jne	.L72
	testq	%r14, %r14
	jne	.L90
	.p2align 4,,10
	.p2align 3
.L71:
	movl	$1, %r12d
	testq	%r13, %r13
	je	.L64
	movq	40(%rbx), %rdi
	call	BN_clear_free@PLT
	movq	%r13, 40(%rbx)
	movl	$4, %esi
	movq	%r13, %rdi
	call	BN_set_flags@PLT
.L64:
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	testq	%rdx, %rdx
	je	.L77
	testq	%rsi, %rsi
	je	.L73
	.p2align 4,,10
	.p2align 3
.L72:
	call	BN_free@PLT
	movq	%r12, 24(%rbx)
	testq	%r14, %r14
	je	.L71
.L90:
	movq	32(%rbx), %r12
.L73:
	movq	%r12, %rdi
	call	BN_free@PLT
	movq	%r14, 32(%rbx)
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L88:
	testq	%rsi, %rsi
	je	.L77
	cmpq	$0, 32(%rbx)
	jne	.L72
	testq	%rdx, %rdx
	jne	.L72
.L77:
	xorl	%r12d, %r12d
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE862:
	.size	RSA_set0_key, .-RSA_set0_key
	.p2align 4
	.globl	RSA_set0_factors
	.type	RSA_set0_factors, @function
RSA_set0_factors:
.LFB863:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	48(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L111
	cmpq	$0, 56(%rbx)
	je	.L112
	testq	%rsi, %rsi
	jne	.L98
	movl	$1, %eax
	testq	%r13, %r13
	jne	.L113
.L91:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	testq	%rdx, %rdx
	je	.L103
	testq	%rsi, %rsi
	je	.L99
	.p2align 4,,10
	.p2align 3
.L98:
	call	BN_clear_free@PLT
	movq	%r12, 48(%rbx)
	movl	$4, %esi
	movq	%r12, %rdi
	call	BN_set_flags@PLT
	movl	$1, %eax
	testq	%r13, %r13
	je	.L91
.L113:
	movq	56(%rbx), %r12
.L99:
	movq	%r12, %rdi
	call	BN_clear_free@PLT
	movq	%r13, 56(%rbx)
	movq	%r13, %rdi
	movl	$4, %esi
	call	BN_set_flags@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L103
	cmpq	$0, 56(%rbx)
	jne	.L98
	testq	%rdx, %rdx
	jne	.L98
.L103:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE863:
	.size	RSA_set0_factors, .-RSA_set0_factors
	.p2align 4
	.globl	RSA_set0_crt_params
	.type	RSA_set0_crt_params, @function
RSA_set0_crt_params:
.LFB864:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	64(%rdi), %rdi
	movq	%rdi, %rax
	orq	%rsi, %rax
	je	.L120
	movq	%rdx, %rax
	orq	72(%rbx), %rax
	movq	%rdx, %r13
	je	.L120
	movq	%rcx, %rax
	orq	80(%rbx), %rax
	movq	%rcx, %r14
	je	.L120
	movq	%rsi, %r12
	testq	%rsi, %rsi
	je	.L116
	call	BN_clear_free@PLT
	movq	%r12, 64(%rbx)
	movl	$4, %esi
	movq	%r12, %rdi
	call	BN_set_flags@PLT
.L116:
	testq	%r13, %r13
	je	.L117
	movq	72(%rbx), %rdi
	call	BN_clear_free@PLT
	movq	%r13, 72(%rbx)
	movl	$4, %esi
	movq	%r13, %rdi
	call	BN_set_flags@PLT
.L117:
	movl	$1, %r12d
	testq	%r14, %r14
	je	.L114
	movq	80(%rbx), %rdi
	call	BN_clear_free@PLT
	movq	%r14, 80(%rbx)
	movl	$4, %esi
	movq	%r14, %rdi
	call	BN_set_flags@PLT
.L114:
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore_state
	xorl	%r12d, %r12d
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE864:
	.size	RSA_set0_crt_params, .-RSA_set0_crt_params
	.p2align 4
	.globl	RSA_set0_multi_prime_params
	.type	RSA_set0_multi_prime_params, @function
RSA_set0_multi_prime_params:
.LFB865:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -72(%rbp)
	testq	%rsi, %rsi
	je	.L152
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L152
	movq	%rcx, %r13
	testq	%rcx, %rcx
	je	.L152
	movl	%r8d, %r15d
	testl	%r8d, %r8d
	je	.L152
	movq	%rsi, %rbx
	xorl	%edi, %edi
	movl	%r8d, %esi
	call	OPENSSL_sk_new_reserve@PLT
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L152
	movq	-72(%rbp), %rax
	movq	88(%rax), %rax
	movq	%rax, -80(%rbp)
	testl	%r15d, %r15d
	jle	.L135
	leal	-1(%r15), %eax
	xorl	%r14d, %r14d
	movq	%rax, -64(%rbp)
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L153:
	cmpq	$0, (%rbx,%r14,8)
	je	.L137
	cmpq	$0, (%r12,%r14,8)
	je	.L137
	cmpq	$0, 0(%r13,%r14,8)
	je	.L137
	movq	(%rax), %rdi
	call	BN_clear_free@PLT
	movq	8(%r15), %rdi
	call	BN_clear_free@PLT
	movq	16(%r15), %rdi
	call	BN_clear_free@PLT
	movq	(%rbx,%r14,8), %rdi
	movl	$4, %esi
	movq	%rdi, (%r15)
	movq	(%r12,%r14,8), %rax
	movq	%rax, 8(%r15)
	movq	0(%r13,%r14,8), %rax
	movq	%rax, 16(%r15)
	call	BN_set_flags@PLT
	movq	8(%r15), %rdi
	movl	$4, %esi
	call	BN_set_flags@PLT
	movq	16(%r15), %rdi
	movl	$4, %esi
	call	BN_set_flags@PLT
	movq	-56(%rbp), %rdi
	movq	%r15, %rsi
	call	OPENSSL_sk_push@PLT
	leaq	1(%r14), %rax
	cmpq	-64(%rbp), %r14
	je	.L135
	movq	%rax, %r14
.L138:
	call	rsa_multip_info_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L153
.L136:
	movq	rsa_multip_info_free_ex@GOTPCREL(%rip), %rsi
	movq	-56(%rbp), %rdi
	call	OPENSSL_sk_pop_free@PLT
	.p2align 4,,10
	.p2align 3
.L152:
	xorl	%eax, %eax
.L130:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
	movq	%r15, %rdi
	call	rsa_multip_info_free@PLT
	movq	rsa_multip_info_free_ex@GOTPCREL(%rip), %rsi
	movq	-56(%rbp), %rdi
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L135:
	movq	-72(%rbp), %rax
	movq	-56(%rbp), %rcx
	movq	%rcx, 88(%rax)
	movq	%rax, %rdi
	call	rsa_multip_calc_product@PLT
	testl	%eax, %eax
	je	.L154
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L140
	movq	rsa_multip_info_free@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	call	OPENSSL_sk_pop_free@PLT
.L140:
	movq	-72(%rbp), %rax
	movl	$1, 4(%rax)
	movl	$1, %eax
	jmp	.L130
.L154:
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %rcx
	movq	%rcx, 88(%rax)
	jmp	.L136
	.cfi_endproc
.LFE865:
	.size	RSA_set0_multi_prime_params, .-RSA_set0_multi_prime_params
	.p2align 4
	.globl	RSA_get0_key
	.type	RSA_get0_key, @function
RSA_get0_key:
.LFB866:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L156
	movq	24(%rdi), %rax
	movq	%rax, (%rsi)
.L156:
	testq	%rdx, %rdx
	je	.L157
	movq	32(%rdi), %rax
	movq	%rax, (%rdx)
.L157:
	testq	%rcx, %rcx
	je	.L155
	movq	40(%rdi), %rax
	movq	%rax, (%rcx)
.L155:
	ret
	.cfi_endproc
.LFE866:
	.size	RSA_get0_key, .-RSA_get0_key
	.p2align 4
	.globl	RSA_get0_factors
	.type	RSA_get0_factors, @function
RSA_get0_factors:
.LFB867:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L169
	movq	48(%rdi), %rax
	movq	%rax, (%rsi)
.L169:
	testq	%rdx, %rdx
	je	.L168
	movq	56(%rdi), %rax
	movq	%rax, (%rdx)
.L168:
	ret
	.cfi_endproc
.LFE867:
	.size	RSA_get0_factors, .-RSA_get0_factors
	.p2align 4
	.globl	RSA_get_multi_prime_extra_count
	.type	RSA_get_multi_prime_extra_count, @function
RSA_get_multi_prime_extra_count:
.LFB868:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	88(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	OPENSSL_sk_num@PLT
	movl	$0, %edx
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	cmovs	%edx, %eax
	ret
	.cfi_endproc
.LFE868:
	.size	RSA_get_multi_prime_extra_count, .-RSA_get_multi_prime_extra_count
	.p2align 4
	.globl	RSA_get0_multi_prime_factors
	.type	RSA_get0_multi_prime_factors, @function
RSA_get0_multi_prime_factors:
.LFB869:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	88(%rdi), %rdi
	call	OPENSSL_sk_num@PLT
	movl	%eax, %r12d
	xorl	%eax, %eax
	testl	%r12d, %r12d
	jle	.L179
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L181:
	movq	88(%r14), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	(%rax), %rax
	movq	%rax, 0(%r13,%rbx,8)
	addq	$1, %rbx
	cmpl	%ebx, %r12d
	jg	.L181
	movl	$1, %eax
.L179:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE869:
	.size	RSA_get0_multi_prime_factors, .-RSA_get0_multi_prime_factors
	.p2align 4
	.globl	RSA_get0_crt_params
	.type	RSA_get0_crt_params, @function
RSA_get0_crt_params:
.LFB870:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L187
	movq	64(%rdi), %rax
	movq	%rax, (%rsi)
.L187:
	testq	%rdx, %rdx
	je	.L188
	movq	72(%rdi), %rax
	movq	%rax, (%rdx)
.L188:
	testq	%rcx, %rcx
	je	.L186
	movq	80(%rdi), %rax
	movq	%rax, (%rcx)
.L186:
	ret
	.cfi_endproc
.LFE870:
	.size	RSA_get0_crt_params, .-RSA_get0_crt_params
	.p2align 4
	.globl	RSA_get0_multi_prime_crt_params
	.type	RSA_get0_multi_prime_crt_params, @function
RSA_get0_multi_prime_crt_params:
.LFB871:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	88(%rdi), %rdi
	call	OPENSSL_sk_num@PLT
	movl	%eax, %r12d
	xorl	%eax, %eax
	testl	%r12d, %r12d
	jle	.L199
	movq	%r14, %rax
	orq	%r15, %rax
	je	.L207
	xorl	%ebx, %ebx
	testq	%r14, %r14
	je	.L203
	testq	%r15, %r15
	je	.L206
	.p2align 4,,10
	.p2align 3
.L205:
	movq	88(%r13), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	8(%rax), %rdx
	movq	%rdx, (%r14,%rbx,8)
	movq	16(%rax), %rax
	movq	%rax, (%r15,%rbx,8)
	addq	$1, %rbx
	cmpl	%ebx, %r12d
	jg	.L205
	.p2align 4,,10
	.p2align 3
.L207:
	movl	$1, %eax
.L199:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L206:
	.cfi_restore_state
	movq	88(%r13), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	8(%rax), %rax
	movq	%rax, (%r14,%rbx,8)
	addq	$1, %rbx
	cmpl	%ebx, %r12d
	jg	.L206
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L203:
	movq	88(%r13), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	16(%rax), %rax
	movq	%rax, (%r15,%rbx,8)
	addq	$1, %rbx
	cmpl	%ebx, %r12d
	jg	.L203
	jmp	.L207
	.cfi_endproc
.LFE871:
	.size	RSA_get0_multi_prime_crt_params, .-RSA_get0_multi_prime_crt_params
	.p2align 4
	.globl	RSA_get0_n
	.type	RSA_get0_n, @function
RSA_get0_n:
.LFB872:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE872:
	.size	RSA_get0_n, .-RSA_get0_n
	.p2align 4
	.globl	RSA_get0_e
	.type	RSA_get0_e, @function
RSA_get0_e:
.LFB873:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE873:
	.size	RSA_get0_e, .-RSA_get0_e
	.p2align 4
	.globl	RSA_get0_d
	.type	RSA_get0_d, @function
RSA_get0_d:
.LFB874:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE874:
	.size	RSA_get0_d, .-RSA_get0_d
	.p2align 4
	.globl	RSA_get0_p
	.type	RSA_get0_p, @function
RSA_get0_p:
.LFB875:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	ret
	.cfi_endproc
.LFE875:
	.size	RSA_get0_p, .-RSA_get0_p
	.p2align 4
	.globl	RSA_get0_q
	.type	RSA_get0_q, @function
RSA_get0_q:
.LFB876:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rax
	ret
	.cfi_endproc
.LFE876:
	.size	RSA_get0_q, .-RSA_get0_q
	.p2align 4
	.globl	RSA_get0_dmp1
	.type	RSA_get0_dmp1, @function
RSA_get0_dmp1:
.LFB877:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	ret
	.cfi_endproc
.LFE877:
	.size	RSA_get0_dmp1, .-RSA_get0_dmp1
	.p2align 4
	.globl	RSA_get0_dmq1
	.type	RSA_get0_dmq1, @function
RSA_get0_dmq1:
.LFB878:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rax
	ret
	.cfi_endproc
.LFE878:
	.size	RSA_get0_dmq1, .-RSA_get0_dmq1
	.p2align 4
	.globl	RSA_get0_iqmp
	.type	RSA_get0_iqmp, @function
RSA_get0_iqmp:
.LFB879:
	.cfi_startproc
	endbr64
	movq	80(%rdi), %rax
	ret
	.cfi_endproc
.LFE879:
	.size	RSA_get0_iqmp, .-RSA_get0_iqmp
	.p2align 4
	.globl	RSA_get0_pss_params
	.type	RSA_get0_pss_params, @function
RSA_get0_pss_params:
.LFB880:
	.cfi_startproc
	endbr64
	movq	96(%rdi), %rax
	ret
	.cfi_endproc
.LFE880:
	.size	RSA_get0_pss_params, .-RSA_get0_pss_params
	.p2align 4
	.globl	RSA_clear_flags
	.type	RSA_clear_flags, @function
RSA_clear_flags:
.LFB881:
	.cfi_startproc
	endbr64
	notl	%esi
	andl	%esi, 116(%rdi)
	ret
	.cfi_endproc
.LFE881:
	.size	RSA_clear_flags, .-RSA_clear_flags
	.p2align 4
	.globl	RSA_test_flags
	.type	RSA_test_flags, @function
RSA_test_flags:
.LFB882:
	.cfi_startproc
	endbr64
	movl	116(%rdi), %eax
	andl	%esi, %eax
	ret
	.cfi_endproc
.LFE882:
	.size	RSA_test_flags, .-RSA_test_flags
	.p2align 4
	.globl	RSA_set_flags
	.type	RSA_set_flags, @function
RSA_set_flags:
.LFB883:
	.cfi_startproc
	endbr64
	orl	%esi, 116(%rdi)
	ret
	.cfi_endproc
.LFE883:
	.size	RSA_set_flags, .-RSA_set_flags
	.p2align 4
	.globl	RSA_get_version
	.type	RSA_get_version, @function
RSA_get_version:
.LFB884:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	ret
	.cfi_endproc
.LFE884:
	.size	RSA_get_version, .-RSA_get_version
	.p2align 4
	.globl	RSA_get0_engine
	.type	RSA_get0_engine, @function
RSA_get0_engine:
.LFB885:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE885:
	.size	RSA_get0_engine, .-RSA_get0_engine
	.p2align 4
	.globl	RSA_pkey_ctx_ctrl
	.type	RSA_pkey_ctx_ctrl, @function
RSA_pkey_ctx_ctrl:
.LFB886:
	.cfi_startproc
	endbr64
	movq	%r8, %r9
	testq	%rdi, %rdi
	je	.L231
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L231
	movl	(%rax), %eax
	cmpl	$6, %eax
	je	.L231
	cmpl	$912, %eax
	jne	.L232
.L231:
	movl	%ecx, %r8d
	movl	%edx, %ecx
	movl	%esi, %edx
	movl	$-1, %esi
	jmp	EVP_PKEY_CTX_ctrl@PLT
.L232:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE886:
	.size	RSA_pkey_ctx_ctrl, .-RSA_pkey_ctx_ctrl
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
