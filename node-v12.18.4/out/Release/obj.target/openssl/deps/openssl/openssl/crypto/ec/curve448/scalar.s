	.file	"scalar.c"
	.text
	.p2align 4
	.type	sc_montmul, @function
sc_montmul:
.LFB206:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r9d, %r9d
	xorl	%r11d, %r11d
	xorl	%r10d, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r11, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$504, %rsp
	movq	%rdi, -544(%rbp)
	movq	%r9, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	%rsi, -144(%rbp)
	movq	$0, -432(%rbp)
	movq	%rax, -448(%rbp)
	movq	8(%rdx), %rax
	movq	$0, -416(%rbp)
	movq	%rax, -488(%rbp)
	movq	16(%rdx), %rax
	movq	$0, -440(%rbp)
	movq	%rax, -496(%rbp)
	movq	24(%rdx), %rax
	movq	%rax, -504(%rbp)
	movq	32(%rdx), %rax
	movq	%rax, -512(%rbp)
	movq	40(%rdx), %rax
	movq	%rax, -520(%rbp)
	movq	48(%rdx), %rax
	movq	%rax, -528(%rbp)
	movq	%rsi, %rax
	addq	$56, %rax
	movq	%rax, -536(%rbp)
	.p2align 4,,10
	.p2align 3
.L2:
	movq	-144(%rbp), %rax
	movq	%r12, -160(%rbp)
	movq	$0, -152(%rbp)
	movq	(%rax), %rcx
	movq	-448(%rbp), %rax
	movq	%r13, -176(%rbp)
	movq	$0, -168(%rbp)
	mulq	%rcx
	addq	-160(%rbp), %rax
	adcq	-152(%rbp), %rdx
	xorl	%r9d, %r9d
	movq	%rax, %r14
	movq	%rdx, %r15
	movq	-488(%rbp), %rax
	movq	%r10, -192(%rbp)
	movq	$0, -184(%rbp)
	mulq	%rcx
	addq	-176(%rbp), %rax
	adcq	-168(%rbp), %rdx
	movq	%rdi, -208(%rbp)
	movq	%rax, %r12
	movq	%rdx, %r13
	movq	-496(%rbp), %rax
	movq	$0, -200(%rbp)
	movq	$0, -216(%rbp)
	addq	%r15, %r12
	adcq	%r9, %r13
	mulq	%rcx
	xorl	%r9d, %r9d
	addq	-192(%rbp), %rax
	movq	%r13, %r8
	adcq	-184(%rbp), %rdx
	addq	%rax, %r8
	movq	-504(%rbp), %rax
	adcq	%rdx, %r9
	xorl	%r11d, %r11d
	mulq	%rcx
	addq	-208(%rbp), %rax
	adcq	-200(%rbp), %rdx
	movq	%rax, %rsi
	movq	%rdx, %rdi
	movq	-512(%rbp), %rax
	addq	%r9, %rsi
	adcq	%r11, %rdi
	movq	%rsi, -464(%rbp)
	mulq	%rcx
	xorl	%r11d, %r11d
	movq	%rdi, %r10
	movq	%rdi, -456(%rbp)
	movq	-416(%rbp), %rdi
	movq	%rdi, -224(%rbp)
	addq	-224(%rbp), %rax
	adcq	-216(%rbp), %rdx
	addq	%r10, %rax
	movq	-432(%rbp), %rdi
	movq	$0, -232(%rbp)
	movq	%rax, -416(%rbp)
	adcq	%r11, %rdx
	movq	-520(%rbp), %rax
	xorl	%r11d, %r11d
	movq	%rdi, -240(%rbp)
	movq	%rdx, %r10
	movq	%rdx, -408(%rbp)
	mulq	%rcx
	addq	-240(%rbp), %rax
	adcq	-232(%rbp), %rdx
	addq	%r10, %rax
	movq	%rbx, -256(%rbp)
	adcq	%r11, %rdx
	movq	%rax, -432(%rbp)
	xorl	%r11d, %r11d
	movq	%rcx, %rax
	movq	%rdx, %r10
	movq	%rdx, -424(%rbp)
	mulq	-528(%rbp)
	addq	-256(%rbp), %rax
	movq	$0, -248(%rbp)
	movq	%rax, %rcx
	adcq	-248(%rbp), %rdx
	movabsq	$2556006723728458995, %rax
	movq	%r14, -272(%rbp)
	addq	%r10, %rcx
	movq	%rdx, %rbx
	movq	$0, -264(%rbp)
	adcq	%r11, %rbx
	movabsq	$269446386856070085, %r11
	movq	%r12, -288(%rbp)
	imulq	%r14, %r11
	movq	%rcx, -480(%rbp)
	movq	%rbx, -472(%rbp)
	movq	$0, -280(%rbp)
	mulq	%r11
	addq	-272(%rbp), %rax
	movabsq	$2408513697996967765, %rax
	adcq	-264(%rbp), %rdx
	movq	%rdx, %rcx
	mulq	%r11
	xorl	%ebx, %ebx
	addq	-288(%rbp), %rax
	adcq	-280(%rbp), %rdx
	addq	%rax, %rcx
	movabsq	$-4301259484579875184, %rax
	movq	%r8, -304(%rbp)
	adcq	%rdx, %rbx
	movq	%rcx, %r12
	movq	-464(%rbp), %rdi
	movq	$0, -296(%rbp)
	mulq	%r11
	movq	%rbx, %rcx
	xorl	%ebx, %ebx
	addq	-304(%rbp), %rax
	adcq	-296(%rbp), %rdx
	addq	%rax, %rcx
	movabsq	$-2201345047, %rax
	movq	%rdi, -320(%rbp)
	adcq	%rdx, %rbx
	mulq	%r11
	movq	%rcx, %r13
	movq	-416(%rbp), %rdi
	movq	%rbx, %rcx
	xorl	%ebx, %ebx
	addq	-320(%rbp), %rax
	movq	$0, -312(%rbp)
	adcq	-312(%rbp), %rdx
	addq	%rax, %rcx
	movq	$-1, %rax
	movq	%rdi, -336(%rbp)
	adcq	%rdx, %rbx
	mulq	%r11
	movq	-336(%rbp), %rsi
	movq	%rcx, %r10
	movq	$0, -328(%rbp)
	movq	%rbx, %rcx
	xorl	%ebx, %ebx
	movq	-328(%rbp), %rdi
	movq	%r11, -368(%rbp)
	movq	-368(%rbp), %r14
	addq	%rax, %rsi
	movq	-480(%rbp), %r8
	movq	$0, -344(%rbp)
	adcq	%rdx, %rdi
	addq	%rsi, %rcx
	movq	-432(%rbp), %rsi
	movq	$0, -360(%rbp)
	adcq	%rdi, %rbx
	movq	%rcx, %rdi
	movq	-360(%rbp), %r15
	movq	%r8, -384(%rbp)
	movq	%rsi, -352(%rbp)
	movq	%rbx, %rcx
	xorl	%ebx, %ebx
	addq	-352(%rbp), %rax
	adcq	-344(%rbp), %rdx
	addq	%rcx, %rax
	movq	%r15, %rsi
	movq	-472(%rbp), %r9
	adcq	%rbx, %rdx
	movq	%r14, %rbx
	shldq	$62, %r14, %rsi
	movq	%rax, -416(%rbp)
	salq	$62, %rbx
	movq	%rdx, %rax
	xorl	%edx, %edx
	movq	%r9, %r8
	movq	$0, -376(%rbp)
	movq	%rbx, %rcx
	movq	%rsi, %rbx
	subq	%r14, %rcx
	movq	%r8, %r14
	sbbq	%r15, %rbx
	addq	-384(%rbp), %rcx
	adcq	-376(%rbp), %rbx
	movq	$0, -392(%rbp)
	addq	%rcx, %rax
	movq	-440(%rbp), %rsi
	adcq	%rbx, %rdx
	movq	%rax, -432(%rbp)
	xorl	%r9d, %r9d
	movq	%rdx, %rax
	movq	%rsi, -400(%rbp)
	xorl	%edx, %edx
	movq	%r9, %r15
	addq	-400(%rbp), %r14
	adcq	-392(%rbp), %r15
	addq	%rax, %r14
	adcq	%rdx, %r15
	movq	%r14, %rbx
	addq	$8, -144(%rbp)
	movq	-144(%rbp), %rax
	movq	%r15, -440(%rbp)
	cmpq	%rax, -536(%rbp)
	jne	.L2
	movq	%r12, %xmm0
	movq	%r13, %xmm1
	xorl	%edx, %edx
	movq	$-1, %rbx
	punpcklqdq	%xmm1, %xmm0
	movq	%rdi, %xmm2
	movabsq	$-2556006723728458995, %rcx
	movq	%xmm0, %r12
	movaps	%xmm0, -128(%rbp)
	movq	%r10, %xmm0
	movq	%r12, %rax
	punpcklqdq	%xmm2, %xmm0
	addq	%rcx, %rax
	movq	-120(%rbp), %rcx
	movq	%xmm0, %rdi
	movaps	%xmm0, -112(%rbp)
	movq	-416(%rbp), %xmm0
	adcq	%rbx, %rdx
	movabsq	$2556006723728458995, %rbx
	movq	%rdx, %rax
	subq	%rbx, %r12
	sarq	$63, %rdx
	xorl	%ebx, %ebx
	addq	%rcx, %rax
	movhps	-432(%rbp), %xmm0
	movabsq	$-2408513697996967765, %rcx
	adcq	%rbx, %rdx
	addq	%rcx, %rax
	movq	$-1, %rbx
	movaps	%xmm0, -96(%rbp)
	adcq	%rbx, %rdx
	movq	%rax, %rcx
	movq	%rdi, %rax
	movq	-88(%rbp), %rsi
	movq	%rdx, %rbx
	movq	%rcx, -208(%rbp)
	xorl	%edx, %edx
	movq	%rbx, %rcx
	movq	%rbx, -200(%rbp)
	sarq	$63, %rbx
	addq	%rcx, %rax
	movabsq	$4301259484579875184, %rcx
	adcq	%rbx, %rdx
	addq	%rcx, %rax
	movq	$-1, %rbx
	adcq	%rbx, %rdx
	movq	%rax, %r8
	movq	-104(%rbp), %rax
	movq	%rdx, %r9
	movq	%r8, -192(%rbp)
	xorl	%edx, %edx
	movq	%r9, %r8
	movq	%r9, -184(%rbp)
	sarq	$63, %r9
	addq	%rax, %r8
	movl	$2201345047, %eax
	adcq	%rdx, %r9
	addq	%rax, %r8
	movq	$-1, %rdx
	adcq	%rdx, %r9
	movq	%xmm0, %rax
	xorl	%edx, %edx
	movq	%r9, %rdi
	sarq	$63, %rdi
	addq	%r9, %rax
	adcq	%rdi, %rdx
	addq	$1, %rax
	adcq	$-1, %rdx
	movq	%rax, -176(%rbp)
	xorl	%edi, %edi
	movq	%rdx, %rax
	movq	%rdx, -168(%rbp)
	sarq	$63, %rdx
	addq	%rax, %rsi
	movabsq	$-4611686018427387903, %rax
	adcq	%rdx, %rdi
	addq	$1, %rsi
	adcq	$-1, %rdi
	movq	%rsi, -160(%rbp)
	xorl	%edx, %edx
	movq	%rdi, %rsi
	movq	%rdi, -152(%rbp)
	sarq	$63, %rdi
	movq	%rsi, %r10
	movq	%rdi, %r11
	movq	-544(%rbp), %rsi
	addq	%r14, %r10
	movq	-176(%rbp), %r14
	adcq	%rdx, %r11
	addq	%rax, %r10
	movq	$-1, %rdx
	movq	-192(%rbp), %rax
	adcq	%rdx, %r11
	xorl	%edx, %edx
	xorl	%ebx, %ebx
	movq	%r10, -144(%rbp)
	movq	%r11, %rdi
	movq	%r11, -136(%rbp)
	movabsq	$2556006723728458995, %r11
	addq	%r15, %rdi
	movabsq	$2408513697996967765, %r15
	andq	%rdi, %r11
	movq	%r11, %rcx
	addq	%r12, %rcx
	adcq	%rdx, %rbx
	addq	%r11, %r12
	xorl	%r13d, %r13d
	movq	%r12, (%rsi)
	movq	-208(%rbp), %r12
	movq	%rbx, %rcx
	sarq	$63, %rbx
	addq	%r12, %rcx
	adcq	%r13, %rbx
	andq	%rdi, %r15
	xorl	%edx, %edx
	addq	%r15, %rcx
	movabsq	$-4301259484579875184, %r15
	adcq	%rdx, %rbx
	movq	%rcx, 8(%rsi)
	xorl	%edx, %edx
	movq	%rbx, %rcx
	sarq	$63, %rbx
	addq	%rcx, %rax
	movq	%r8, %rcx
	movq	%rdi, %r8
	adcq	%rbx, %rdx
	andq	%rdi, %r15
	xorl	%ebx, %ebx
	addq	%r15, %rax
	adcq	%rbx, %rdx
	movq	%rax, 16(%rsi)
	xorl	%ebx, %ebx
	movq	%rdx, %rax
	sarq	$63, %rdx
	addq	%rax, %rcx
	movabsq	$-2201345047, %rax
	adcq	%rdx, %rbx
	andq	%rdi, %rax
	xorl	%r13d, %r13d
	addq	%rax, %rcx
	movq	-160(%rbp), %rax
	adcq	%r13, %rbx
	movq	%rcx, 24(%rsi)
	xorl	%r15d, %r15d
	movq	%rbx, %rcx
	sarq	$63, %rbx
	addq	%rcx, %r14
	movabsq	$4611686018427387903, %rcx
	adcq	%rbx, %r15
	xorl	%r9d, %r9d
	addq	%rdi, %r14
	adcq	%r9, %r15
	movq	%r14, 32(%rsi)
	xorl	%edx, %edx
	movq	%r15, %r14
	sarq	$63, %r15
	addq	%r14, %rax
	adcq	%r15, %rdx
	addq	%rax, %r8
	movq	-144(%rbp), %rax
	adcq	%rdx, %r9
	movq	%r8, 40(%rsi)
	movq	%r9, %r8
	addq	%rax, %r8
	andq	%rcx, %rdi
	movq	%rdi, %rax
	addq	%r8, %rax
	movq	%rax, 48(%rsi)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L7
	addq	$504, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L7:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE206:
	.size	sc_montmul, .-sc_montmul
	.p2align 4
	.globl	curve448_scalar_mul
	.type	curve448_scalar_mul, @function
curve448_scalar_mul:
.LFB207:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	sc_montmul
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r12, %rdi
	leaq	sc_r2(%rip), %rdx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	sc_montmul
	.cfi_endproc
.LFE207:
	.size	curve448_scalar_mul, .-curve448_scalar_mul
	.p2align 4
	.globl	curve448_scalar_sub
	.type	curve448_scalar_sub, @function
curve448_scalar_sub:
.LFB208:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rcx
	xorl	%r9d, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %r11
	movq	(%rdx), %r10
	xorl	%edx, %edx
	movq	%r11, %rax
	movq	%r11, %rbx
	subq	%r10, %rax
	sbbq	%r9, %rdx
	subq	%r10, %rbx
	xorl	%r9d, %r9d
	movq	%rbx, (%rdi)
	movq	8(%rsi), %r8
	movq	%rdx, %rax
	sarq	$63, %rdx
	addq	%r8, %rax
	movq	8(%rcx), %r8
	adcq	%r9, %rdx
	xorl	%r9d, %r9d
	subq	%r8, %rax
	movq	%rax, 8(%rdi)
	sbbq	%r9, %rdx
	movq	16(%rsi), %r12
	xorl	%r13d, %r13d
	movq	16(%rcx), %r8
	movq	%rax, -80(%rbp)
	movq	%rdx, %rax
	movq	%rdx, -72(%rbp)
	sarq	$63, %rdx
	addq	%rax, %r12
	adcq	%rdx, %r13
	xorl	%r9d, %r9d
	subq	%r8, %r12
	movq	%r12, 16(%rdi)
	sbbq	%r9, %r13
	movq	24(%rsi), %r8
	xorl	%r9d, %r9d
	movq	%r13, %r11
	movq	%r13, %r10
	sarq	$63, %r11
	addq	%r8, %r10
	movq	24(%rcx), %r8
	adcq	%r9, %r11
	movq	%r10, %r14
	xorl	%r9d, %r9d
	movq	%r11, %r15
	subq	%r8, %r14
	movq	%r14, 24(%rdi)
	sbbq	%r9, %r15
	movq	32(%rsi), %r8
	xorl	%r9d, %r9d
	movq	%r15, %r11
	movq	%r15, %r10
	sarq	$63, %r11
	addq	%r8, %r10
	movq	32(%rcx), %r8
	adcq	%r9, %r11
	xorl	%r9d, %r9d
	subq	%r8, %r10
	movq	%r10, 32(%rdi)
	sbbq	%r9, %r11
	movq	40(%rsi), %r8
	xorl	%r9d, %r9d
	movq	%r10, -96(%rbp)
	movq	%r11, %r10
	movq	%r11, -88(%rbp)
	sarq	$63, %r11
	addq	%r8, %r10
	movq	40(%rcx), %r8
	adcq	%r9, %r11
	movq	%r10, %rax
	xorl	%r9d, %r9d
	movq	%r11, %rdx
	subq	%r8, %rax
	sbbq	%r9, %rdx
	movq	%rax, 40(%rdi)
	movq	48(%rsi), %r8
	xorl	%r9d, %r9d
	movabsq	$2408513697996967765, %rsi
	movq	%rax, -112(%rbp)
	movq	%rdx, %rax
	movq	%rdx, -104(%rbp)
	movq	%rax, %r10
	sarq	$63, %rdx
	movq	%rdx, %r11
	addq	%r8, %r10
	movq	48(%rcx), %r8
	adcq	%r9, %r11
	movq	%r10, %rdx
	xorl	%r9d, %r9d
	movq	-80(%rbp), %r10
	subq	%r8, %rdx
	movq	%r11, %rcx
	movq	%rbx, %r8
	sbbq	%r9, %rcx
	movq	%rdx, -64(%rbp)
	xorl	%r11d, %r11d
	xorl	%r9d, %r9d
	movabsq	$2556006723728458995, %rdx
	movq	%rcx, -56(%rbp)
	andq	%rcx, %rdx
	addq	%rdx, %r8
	adcq	%r11, %r9
	addq	%rbx, %rdx
	xorl	%r11d, %r11d
	movq	%r9, %r8
	sarq	$63, %r9
	movq	%rdx, (%rdi)
	addq	%r8, %r10
	movq	%r12, %r8
	movq	%rcx, %r12
	adcq	%r9, %r11
	andq	%rcx, %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	movabsq	$-4301259484579875184, %rsi
	addq	%r10, %rax
	movq	-96(%rbp), %r10
	adcq	%r11, %rdx
	movq	%rax, 8(%rdi)
	xorl	%r9d, %r9d
	movq	%rdx, %rax
	sarq	$63, %rdx
	addq	%rax, %r8
	adcq	%rdx, %r9
	andq	%rcx, %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	movabsq	$-2201345047, %rsi
	addq	%r8, %rax
	movq	-112(%rbp), %r8
	adcq	%r9, %rdx
	movq	%rax, 16(%rdi)
	xorl	%r9d, %r9d
	movq	%rdx, %rax
	sarq	$63, %rdx
	addq	%r14, %rax
	adcq	%r9, %rdx
	andq	%rcx, %rsi
	xorl	%r11d, %r11d
	addq	%rsi, %rax
	adcq	%r11, %rdx
	movq	%rax, 24(%rdi)
	xorl	%r11d, %r11d
	movq	%rdx, %rax
	sarq	$63, %rdx
	addq	%r10, %rax
	movq	-64(%rbp), %r10
	adcq	%r11, %rdx
	xorl	%r13d, %r13d
	addq	%rcx, %rax
	adcq	%r13, %rdx
	movq	%rax, 32(%rdi)
	xorl	%r9d, %r9d
	movq	%rdx, %rax
	sarq	$63, %rdx
	addq	%r8, %rax
	adcq	%r9, %rdx
	addq	%rax, %r12
	movabsq	$4611686018427387903, %rax
	adcq	%rdx, %r13
	movq	%r12, 40(%rdi)
	movq	%r13, %r12
	addq	%r10, %r12
	andq	%rax, %rcx
	movq	%rcx, %r10
	addq	%r12, %r10
	movq	%r10, 48(%rdi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE208:
	.size	curve448_scalar_sub, .-curve448_scalar_sub
	.p2align 4
	.globl	curve448_scalar_add
	.type	curve448_scalar_add, @function
curve448_scalar_add:
.LFB209:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rcx
	xorl	%r9d, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	$-1, %rbx
	movq	(%rsi), %r8
	movq	(%rdx), %r10
	xorl	%edx, %edx
	leaq	(%r8,%r10), %r11
	movq	%r8, %rax
	movq	%r10, %r8
	movq	%r11, (%rdi)
	addq	%rax, %r8
	movq	8(%rcx), %r10
	movq	8(%rsi), %rax
	adcq	%rdx, %r9
	movq	%r11, -72(%rbp)
	xorl	%edx, %edx
	movq	%r9, %r8
	xorl	%r11d, %r11d
	xorl	%r9d, %r9d
	addq	%r10, %rax
	adcq	%r11, %rdx
	addq	%r8, %rax
	movq	%rax, 8(%rdi)
	movq	16(%rcx), %r8
	adcq	%r9, %rdx
	xorl	%r11d, %r11d
	movq	16(%rsi), %r12
	xorl	%r9d, %r9d
	xorl	%r13d, %r13d
	addq	%r8, %r12
	adcq	%r9, %r13
	addq	%rdx, %r12
	movq	%r12, 16(%rdi)
	movq	24(%rcx), %r10
	adcq	%r11, %r13
	xorl	%r11d, %r11d
	movq	24(%rsi), %r8
	movq	%r13, %r14
	xorl	%r15d, %r15d
	xorl	%r9d, %r9d
	movq	$-1, %r13
	addq	%r10, %r8
	adcq	%r11, %r9
	addq	%r8, %r14
	adcq	%r9, %r15
	xorl	%r10d, %r10d
	movq	%r14, 24(%rdi)
	movq	32(%rsi), %r8
	movq	%r10, -56(%rbp)
	movq	32(%rcx), %r10
	xorl	%r11d, %r11d
	xorl	%r9d, %r9d
	movq	%r15, -64(%rbp)
	movq	$-1, %r15
	addq	%r10, %r8
	adcq	%r11, %r9
	addq	-64(%rbp), %r8
	adcq	-56(%rbp), %r9
	xorl	%r10d, %r10d
	movq	%r8, -96(%rbp)
	xorl	%r11d, %r11d
	movq	%r9, -64(%rbp)
	xorl	%r9d, %r9d
	movq	%r8, 32(%rdi)
	movq	%r10, -56(%rbp)
	movq	40(%rsi), %r10
	movq	40(%rcx), %r8
	addq	%r10, %r8
	adcq	%r11, %r9
	addq	-64(%rbp), %r8
	adcq	-56(%rbp), %r9
	xorl	%r11d, %r11d
	movq	%r8, -112(%rbp)
	movq	%r8, 40(%rdi)
	movq	48(%rcx), %r10
	movabsq	$-2556006723728458995, %rcx
	movq	48(%rsi), %r8
	movq	%r9, -64(%rbp)
	xorl	%r9d, %r9d
	movabsq	$-2556006723728458995, %rsi
	movq	%r11, -56(%rbp)
	xorl	%r11d, %r11d
	addq	%r10, %r8
	movq	%rax, %r10
	adcq	%r11, %r9
	movq	-72(%rbp), %r11
	addq	-64(%rbp), %r8
	movq	%r8, -64(%rbp)
	adcq	-56(%rbp), %r9
	movq	%r11, %r8
	movq	%r9, -56(%rbp)
	xorl	%r9d, %r9d
	addq	%rcx, %r8
	movq	-56(%rbp), %rcx
	adcq	%rbx, %r9
	addq	%r11, %rsi
	xorl	%r11d, %r11d
	movq	%r9, %r8
	sarq	$63, %r9
	movq	%rsi, -72(%rbp)
	addq	%r8, %r10
	movabsq	$-2408513697996967765, %r8
	adcq	%r9, %r11
	addq	%r8, %r10
	movq	$-1, %r9
	movq	-96(%rbp), %r8
	adcq	%r9, %r11
	xorl	%r9d, %r9d
	movq	%r11, %rax
	movq	%r11, %rdx
	sarq	$63, %rdx
	addq	%r12, %rax
	movabsq	$4301259484579875184, %r12
	adcq	%r9, %rdx
	addq	%rax, %r12
	adcq	%rdx, %r13
	xorl	%r9d, %r9d
	movq	%r13, %rax
	movq	%r13, %rdx
	sarq	$63, %rdx
	addq	%r14, %rax
	movl	$2201345047, %r14d
	adcq	%r9, %rdx
	addq	%rax, %r14
	adcq	%rdx, %r15
	xorl	%r9d, %r9d
	movq	%r15, %rax
	movq	%r15, %rdx
	sarq	$63, %rdx
	addq	%r8, %rax
	movq	-112(%rbp), %r8
	adcq	%r9, %rdx
	addq	$1, %rax
	adcq	$-1, %rdx
	movq	%rax, -96(%rbp)
	xorl	%r9d, %r9d
	movq	%rdx, %rax
	movq	%rdx, -88(%rbp)
	sarq	$63, %rdx
	addq	%r8, %rax
	movabsq	$-4611686018427387903, %r8
	adcq	%r9, %rdx
	addq	$1, %rax
	adcq	$-1, %rdx
	movq	%rax, %rbx
	movq	-64(%rbp), %rax
	movq	%rdx, %rsi
	xorl	%edx, %edx
	movq	%rbx, -112(%rbp)
	movq	-72(%rbp), %rbx
	movq	%rsi, %r9
	movq	%rsi, -104(%rbp)
	sarq	$63, %r9
	addq	%rsi, %rax
	movabsq	$2556006723728458995, %rsi
	adcq	%r9, %rdx
	addq	%r8, %rax
	movq	$-1, %r9
	adcq	%r9, %rdx
	xorl	%r9d, %r9d
	movq	%rax, -64(%rbp)
	addq	%rdx, %rcx
	movq	%rdx, -56(%rbp)
	xorl	%edx, %edx
	andq	%rcx, %rsi
	movq	%rsi, %r8
	addq	%rbx, %r8
	adcq	%rdx, %r9
	addq	%rbx, %rsi
	xorl	%edx, %edx
	movq	%r9, %r8
	movq	%rsi, (%rdi)
	sarq	$63, %r9
	movabsq	$2408513697996967765, %rsi
	addq	%r10, %r8
	movq	%r14, %r10
	adcq	%rdx, %r9
	andq	%rcx, %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	movabsq	$-4301259484579875184, %rsi
	addq	%r8, %rax
	adcq	%r9, %rdx
	movq	%rax, 8(%rdi)
	xorl	%r9d, %r9d
	movq	%rdx, %rax
	sarq	$63, %rdx
	addq	%r12, %rax
	adcq	%r9, %rdx
	andq	%rcx, %rsi
	xorl	%r9d, %r9d
	movq	%rsi, %r8
	movabsq	$-2201345047, %rsi
	addq	%rax, %r8
	adcq	%rdx, %r9
	movq	%r8, 16(%rdi)
	xorl	%r11d, %r11d
	movq	%r9, %r8
	sarq	$63, %r9
	addq	%r8, %r10
	movq	%rcx, %r8
	adcq	%r9, %r11
	andq	%rcx, %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	addq	%r10, %rax
	movq	%rax, 24(%rdi)
	adcq	%r11, %rdx
	movq	-96(%rbp), %r10
	xorl	%r11d, %r11d
	movq	%rdx, %rax
	sarq	$63, %rdx
	movq	-64(%rbp), %r12
	addq	%r10, %rax
	movq	-112(%rbp), %r10
	adcq	%r11, %rdx
	xorl	%r9d, %r9d
	addq	%rcx, %rax
	adcq	%r9, %rdx
	movq	%rax, 32(%rdi)
	xorl	%r11d, %r11d
	movq	%rdx, %rax
	sarq	$63, %rdx
	addq	%r10, %rax
	adcq	%r11, %rdx
	addq	%rax, %r8
	movabsq	$4611686018427387903, %rax
	adcq	%rdx, %r9
	movq	%r8, 40(%rdi)
	movq	%r9, %r8
	addq	%r12, %r8
	andq	%rax, %rcx
	movq	%rcx, %r12
	addq	%r8, %r12
	movq	%r12, 48(%rdi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE209:
	.size	curve448_scalar_add, .-curve448_scalar_add
	.p2align 4
	.globl	curve448_scalar_decode
	.type	curve448_scalar_decode, @function
curve448_scalar_decode:
.LFB211:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r10
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$56, %rdi
	subq	$8, %rsp
	.p2align 4,,10
	.p2align 3
.L15:
	xorl	%r9d, %r9d
	xorl	%eax, %eax
	cmpq	$56, %rdx
	je	.L19
	.p2align 4,,10
	.p2align 3
.L16:
	movzbl	(%rsi,%rdx), %r8d
	leal	0(,%rax,8), %ecx
	addq	$1, %rax
	addq	$1, %rdx
	salq	%cl, %r8
	orq	%r8, %r9
	cmpq	$7, %rax
	ja	.L19
	cmpq	$55, %rdx
	jbe	.L16
.L19:
	movq	%r9, (%r10)
	addq	$8, %r10
	cmpq	%rdi, %r10
	jne	.L15
	movq	(%r12), %rax
	xorl	%edx, %edx
	movabsq	$-2556006723728458995, %rsi
	movq	$-1, %rdi
	movq	16(%r12), %r8
	movq	$-1, %r15
	movabsq	$-4611686018427387903, %r14
	addq	%rsi, %rax
	movq	8(%r12), %rsi
	adcq	%rdi, %rdx
	xorl	%edi, %edi
	movq	%rdx, %rax
	sarq	$63, %rdx
	addq	%rsi, %rax
	movabsq	$-2408513697996967765, %rsi
	adcq	%rdi, %rdx
	addq	%rax, %rsi
	movq	$-1, %rdi
	movq	24(%r12), %rax
	adcq	%rdx, %rdi
	xorl	%r9d, %r9d
	movq	%rdi, %rsi
	sarq	$63, %rdi
	addq	%rsi, %r8
	movabsq	$4301259484579875184, %rsi
	adcq	%rdi, %r9
	addq	%rsi, %r8
	movq	$-1, %rdi
	movl	$2201345047, %esi
	adcq	%rdi, %r9
	xorl	%edx, %edx
	movq	$-1, %rdi
	movq	%r9, %r8
	sarq	$63, %r9
	addq	%r8, %rax
	movq	48(%r12), %r8
	adcq	%r9, %rdx
	addq	%rsi, %rax
	movq	32(%r12), %rsi
	adcq	%rdi, %rdx
	xorl	%edi, %edi
	movq	%rdx, %rax
	sarq	$63, %rdx
	addq	%rsi, %rax
	movq	40(%r12), %rsi
	adcq	%rdi, %rdx
	addq	$1, %rax
	adcq	$-1, %rdx
	xorl	%edi, %edi
	movq	%rdx, %rax
	sarq	$63, %rdx
	addq	%rsi, %rax
	movq	%r12, %rsi
	adcq	%rdi, %rdx
	addq	$1, %rax
	movq	%r12, %rdi
	adcq	$-1, %rdx
	xorl	%r9d, %r9d
	movq	%rdx, %rax
	sarq	$63, %rdx
	addq	%r8, %rax
	adcq	%r9, %rdx
	addq	%rax, %r14
	adcq	%rdx, %r15
	leaq	curve448_scalar_one(%rip), %rdx
	call	sc_montmul
	leaq	sc_r2(%rip), %rdx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	sc_montmul
	movq	%r15, %rdx
	movl	%r15d, %eax
	addq	$8, %rsp
	notl	%eax
	subl	$1, %edx
	popq	%r12
	popq	%r14
	andl	%edx, %eax
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	sarl	$31, %eax
	notl	%eax
	ret
	.cfi_endproc
.LFE211:
	.size	curve448_scalar_decode, .-curve448_scalar_decode
	.p2align 4
	.globl	curve448_scalar_destroy
	.type	curve448_scalar_destroy, @function
curve448_scalar_destroy:
.LFB212:
	.cfi_startproc
	endbr64
	movl	$56, %esi
	jmp	OPENSSL_cleanse@PLT
	.cfi_endproc
.LFE212:
	.size	curve448_scalar_destroy, .-curve448_scalar_destroy
	.p2align 4
	.globl	curve448_scalar_decode_long
	.type	curve448_scalar_decode_long, @function
curve448_scalar_decode_long:
.LFB213:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$776, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -816(%rbp)
	movq	%rsi, -792(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L52
	movabsq	$2635249153387078803, %rcx
	movq	%rdx, %r11
	movq	%rsi, %r12
	shrq	$3, %rdx
	movq	%rdx, %rax
	movq	%r11, %r9
	mulq	%rcx
	leaq	0(,%rdx,8), %rax
	subq	%rdx, %rax
	leaq	0(,%rax,8), %rbx
	leaq	-56(%r11), %rax
	cmpq	%rbx, %r11
	cmovne	%rbx, %rax
	leaq	-120(%rbp), %rbx
	xorl	%esi, %esi
	movq	%rax, -184(%rbp)
	movq	%rax, %r8
	subq	%rax, %r9
	leaq	-176(%rbp), %rax
	movq	%rax, -800(%rbp)
	movq	%rax, %r10
	.p2align 4,,10
	.p2align 3
.L32:
	leaq	(%r8,%rsi), %r13
	xorl	%edi, %edi
	xorl	%eax, %eax
	addq	%r12, %r13
	cmpq	%rsi, %r9
	jbe	.L36
	.p2align 4,,10
	.p2align 3
.L33:
	movzbl	0(%r13,%rax), %edx
	leal	0(,%rax,8), %ecx
	addq	$1, %rax
	addq	$1, %rsi
	salq	%cl, %rdx
	orq	%rdx, %rdi
	cmpq	$7, %rax
	ja	.L36
	cmpq	%rsi, %r9
	ja	.L33
.L36:
	movq	%rdi, (%r10)
	addq	$8, %r10
	cmpq	%rbx, %r10
	jne	.L32
	cmpq	$56, %r11
	je	.L37
	leaq	-112(%rbp), %rax
	cmpq	$0, -184(%rbp)
	movq	%rax, -808(%rbp)
	je	.L39
	.p2align 4,,10
	.p2align 3
.L38:
	movq	-800(%rbp), %rdi
	leaq	sc_r2(%rip), %rdx
	subq	$56, -184(%rbp)
	movq	-184(%rbp), %rbx
	movq	%rdi, %rsi
	call	sc_montmul
	movq	-792(%rbp), %rsi
	movq	-808(%rbp), %rdi
	movq	%rbx, -184(%rbp)
	addq	%rbx, %rsi
	call	curve448_scalar_decode
	movq	-168(%rbp), %rdi
	movq	-176(%rbp), %rdx
	movq	$0, -232(%rbp)
	movq	-112(%rbp), %rax
	movq	$0, -248(%rbp)
	movq	%rdi, -272(%rbp)
	movq	-104(%rbp), %rdi
	movq	%rdx, -240(%rbp)
	movq	-232(%rbp), %r15
	movq	%rdi, -288(%rbp)
	movq	-160(%rbp), %rdi
	movq	%rax, -256(%rbp)
	movq	-240(%rbp), %r14
	addq	-256(%rbp), %r14
	adcq	-248(%rbp), %r15
	addq	%rdx, %rax
	xorl	%edx, %edx
	movq	%rdi, -304(%rbp)
	movq	-96(%rbp), %rdi
	movq	%rax, -192(%rbp)
	movq	%r15, %rax
	movq	-272(%rbp), %r14
	addq	-288(%rbp), %r14
	movq	$0, -264(%rbp)
	movq	-264(%rbp), %r15
	movq	%rdi, -320(%rbp)
	movq	$0, -280(%rbp)
	adcq	-280(%rbp), %r15
	addq	%rax, %r14
	adcq	%rdx, %r15
	movq	-304(%rbp), %r8
	xorl	%edx, %edx
	movq	$0, -296(%rbp)
	movq	$0, -312(%rbp)
	movq	-296(%rbp), %r9
	addq	-320(%rbp), %r8
	movq	-152(%rbp), %rdi
	movq	$0, -328(%rbp)
	movq	-144(%rbp), %rcx
	adcq	-312(%rbp), %r9
	addq	%r15, %r8
	movq	$0, -344(%rbp)
	movq	%rdi, -336(%rbp)
	movq	-88(%rbp), %rdi
	adcq	%rdx, %r9
	xorl	%edx, %edx
	movq	%rcx, -368(%rbp)
	movq	-80(%rbp), %rcx
	movq	$-1, %r15
	movq	%rdi, -352(%rbp)
	movq	-136(%rbp), %r10
	movq	-336(%rbp), %rsi
	addq	-352(%rbp), %rsi
	movq	%rcx, -384(%rbp)
	movq	-328(%rbp), %rdi
	adcq	-344(%rbp), %rdi
	addq	%r9, %rsi
	movq	%r10, -400(%rbp)
	adcq	%rdx, %rdi
	movq	-72(%rbp), %r10
	xorl	%edx, %edx
	movq	-368(%rbp), %rcx
	addq	-384(%rbp), %rcx
	movq	$0, -360(%rbp)
	movq	-360(%rbp), %rbx
	movq	%r10, -416(%rbp)
	movq	$0, -376(%rbp)
	adcq	-376(%rbp), %rbx
	addq	%rdi, %rcx
	adcq	%rdx, %rbx
	movq	-400(%rbp), %r10
	xorl	%edx, %edx
	movq	$0, -392(%rbp)
	movq	$0, -408(%rbp)
	movq	-392(%rbp), %r11
	movq	%rbx, %rax
	addq	-416(%rbp), %r10
	movq	-128(%rbp), %r13
	movq	$0, -424(%rbp)
	adcq	-408(%rbp), %r11
	addq	%r10, %rax
	movq	$0, -440(%rbp)
	movabsq	$-2556006723728458995, %r10
	movq	%r13, -432(%rbp)
	movq	-64(%rbp), %r13
	adcq	%r11, %rdx
	xorl	%r11d, %r11d
	movq	-432(%rbp), %r12
	movq	$0, -456(%rbp)
	movq	%r13, -448(%rbp)
	addq	-448(%rbp), %r12
	movq	-424(%rbp), %r13
	adcq	-440(%rbp), %r13
	addq	%rdx, %r12
	movq	%r14, -480(%rbp)
	adcq	%r11, %r13
	movq	-192(%rbp), %r11
	movq	%r12, -784(%rbp)
	movabsq	$-2408513697996967765, %r14
	movq	%r13, -776(%rbp)
	movq	%r11, -464(%rbp)
	movq	$-1, %r11
	addq	-464(%rbp), %r10
	adcq	-456(%rbp), %r11
	movq	$0, -472(%rbp)
	movq	%r11, %r13
	movq	%r11, %r12
	movq	%r8, -496(%rbp)
	movq	$0, -488(%rbp)
	sarq	$63, %r13
	addq	-480(%rbp), %r12
	adcq	-472(%rbp), %r13
	addq	%r14, %r12
	movabsq	$4301259484579875184, %r14
	adcq	%r15, %r13
	movq	%r13, %r15
	movq	%r13, %r8
	sarq	$63, %r15
	addq	-496(%rbp), %r8
	movq	%r15, %r9
	movq	$-1, %r15
	adcq	-488(%rbp), %r9
	addq	%r14, %r8
	adcq	%r15, %r9
	movq	%rsi, -512(%rbp)
	movq	%r9, %r15
	movq	%r9, %r14
	movq	$0, -504(%rbp)
	sarq	$63, %r15
	addq	-512(%rbp), %r14
	adcq	-504(%rbp), %r15
	movq	%rcx, -528(%rbp)
	movq	%r14, %rsi
	movl	$2201345047, %r14d
	movq	%r15, %rdi
	movq	$-1, %r15
	addq	%r14, %rsi
	movq	$0, -520(%rbp)
	adcq	%r15, %rdi
	movq	%rax, -544(%rbp)
	movq	%rdi, %r15
	movq	%rdi, %r14
	movq	$0, -536(%rbp)
	sarq	$63, %r15
	addq	-528(%rbp), %r14
	adcq	-520(%rbp), %r15
	movq	%rsi, -752(%rbp)
	addq	$1, %r14
	movq	-784(%rbp), %rsi
	movq	%rdi, -744(%rbp)
	adcq	$-1, %r15
	movq	-776(%rbp), %rdi
	movq	$0, -552(%rbp)
	movq	%r15, %rbx
	movq	%r15, %rcx
	movq	%rsi, -560(%rbp)
	sarq	$63, %rbx
	addq	-544(%rbp), %rcx
	adcq	-536(%rbp), %rbx
	movq	%r10, -224(%rbp)
	movq	$0, -216(%rbp)
	addq	$1, %rcx
	adcq	$-1, %rbx
	movq	%rcx, -768(%rbp)
	movabsq	$-4611686018427387903, %rcx
	movq	%rbx, %rdx
	movq	%rbx, %rax
	movq	%rbx, -760(%rbp)
	movq	$-1, %rbx
	movq	$0, -568(%rbp)
	sarq	$63, %rdx
	addq	-560(%rbp), %rax
	adcq	-552(%rbp), %rdx
	addq	%rcx, %rax
	adcq	%rbx, %rdx
	movq	%rax, -784(%rbp)
	movq	%rdx, -776(%rbp)
	addq	%rdi, %rdx
	movabsq	$2556006723728458995, %rdi
	movq	%rdx, %rsi
	andq	%rdi, %rsi
	movq	%rsi, %rax
	movq	%rsi, -576(%rbp)
	movq	-576(%rbp), %rbx
	movq	-568(%rbp), %rsi
	movq	-192(%rbp), %r11
	movq	%r12, -592(%rbp)
	addq	-224(%rbp), %rbx
	adcq	-216(%rbp), %rsi
	movq	$0, -584(%rbp)
	subq	%rdi, %r11
	movq	%rsi, %rbx
	movq	%rsi, %rcx
	movq	%r8, -624(%rbp)
	movq	%r11, %rdi
	sarq	$63, %rbx
	movq	$0, -600(%rbp)
	movabsq	$-2201345047, %rsi
	movq	$0, -616(%rbp)
	addq	%rax, %rdi
	addq	-592(%rbp), %rcx
	movq	%rdi, -176(%rbp)
	movabsq	$2408513697996967765, %rdi
	adcq	-584(%rbp), %rbx
	movq	$0, -632(%rbp)
	andq	%rdx, %rdi
	movq	%rdi, -608(%rbp)
	movabsq	$-4301259484579875184, %rdi
	addq	-608(%rbp), %rcx
	adcq	-600(%rbp), %rbx
	movq	%rcx, -168(%rbp)
	movq	$0, -648(%rbp)
	movq	%rbx, %rcx
	sarq	$63, %rbx
	addq	-624(%rbp), %rcx
	adcq	-616(%rbp), %rbx
	andq	%rdx, %rdi
	movq	%rdi, -640(%rbp)
	movq	-752(%rbp), %rdi
	addq	-640(%rbp), %rcx
	adcq	-632(%rbp), %rbx
	movq	%rcx, -160(%rbp)
	movq	%rbx, %rcx
	sarq	$63, %rbx
	movq	%rdi, -656(%rbp)
	addq	-656(%rbp), %rcx
	adcq	-648(%rbp), %rbx
	andq	%rdx, %rsi
	movq	%rdx, -208(%rbp)
	movq	%rsi, -672(%rbp)
	movq	-208(%rbp), %rsi
	addq	-672(%rbp), %rcx
	movq	$0, -664(%rbp)
	adcq	-664(%rbp), %rbx
	movq	%rcx, -152(%rbp)
	movq	%r14, -688(%rbp)
	movq	%rbx, %rcx
	sarq	$63, %rbx
	addq	-688(%rbp), %rcx
	movq	$0, -200(%rbp)
	movq	-200(%rbp), %rdi
	movq	-768(%rbp), %rax
	movq	$0, -680(%rbp)
	adcq	-680(%rbp), %rbx
	addq	%rsi, %rcx
	movq	$0, -696(%rbp)
	adcq	%rdi, %rbx
	movq	%rax, -704(%rbp)
	movabsq	$4611686018427387903, %rax
	movq	%rcx, -144(%rbp)
	movq	%rbx, %rcx
	sarq	$63, %rbx
	addq	-704(%rbp), %rcx
	adcq	-696(%rbp), %rbx
	addq	%rsi, %rcx
	movq	$0, -712(%rbp)
	adcq	%rdi, %rbx
	movq	-784(%rbp), %rdi
	movq	%rcx, -136(%rbp)
	movq	$0, -728(%rbp)
	movq	%rbx, %rcx
	movq	%rdi, -720(%rbp)
	addq	-720(%rbp), %rcx
	andq	%rax, %rdx
	movq	%rdx, -736(%rbp)
	addq	-736(%rbp), %rcx
	cmpq	$0, -184(%rbp)
	movq	%rcx, -128(%rbp)
	jne	.L38
.L39:
	movq	-816(%rbp), %rbx
	movq	-128(%rbp), %rax
	movl	$56, %esi
	movdqa	-176(%rbp), %xmm0
	movdqa	-160(%rbp), %xmm1
	movdqa	-144(%rbp), %xmm2
	movq	%rax, 48(%rbx)
	movq	-800(%rbp), %rdi
	movups	%xmm0, (%rbx)
	movups	%xmm1, 16(%rbx)
	movups	%xmm2, 32(%rbx)
	call	OPENSSL_cleanse@PLT
	movq	-808(%rbp), %rdi
	movl	$56, %esi
	call	OPENSSL_cleanse@PLT
.L28:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L53
	addq	$776, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L52:
	.cfi_restore_state
	movdqa	curve448_scalar_zero(%rip), %xmm3
	movdqa	16+curve448_scalar_zero(%rip), %xmm4
	movdqa	32+curve448_scalar_zero(%rip), %xmm5
	movq	48+curve448_scalar_zero(%rip), %rax
	movups	%xmm3, (%rdi)
	movq	%rax, 48(%rdi)
	movups	%xmm4, 16(%rdi)
	movups	%xmm5, 32(%rdi)
	jmp	.L28
.L37:
	movq	-816(%rbp), %rbx
	movq	-800(%rbp), %r15
	leaq	curve448_scalar_one(%rip), %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	sc_montmul
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	leaq	sc_r2(%rip), %rdx
	call	sc_montmul
	movl	$56, %esi
	movq	%r15, %rdi
	call	OPENSSL_cleanse@PLT
	jmp	.L28
.L53:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE213:
	.size	curve448_scalar_decode_long, .-curve448_scalar_decode_long
	.p2align 4
	.globl	curve448_scalar_encode
	.type	curve448_scalar_encode, @function
curve448_scalar_encode:
.LFB214:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movb	%al, (%rdi)
	movq	(%rsi), %rax
	shrq	$8, %rax
	movb	%al, 1(%rdi)
	movq	(%rsi), %rax
	shrq	$16, %rax
	movb	%al, 2(%rdi)
	movq	(%rsi), %rax
	shrq	$24, %rax
	movb	%al, 3(%rdi)
	movq	(%rsi), %rax
	shrq	$32, %rax
	movb	%al, 4(%rdi)
	movq	(%rsi), %rax
	shrq	$40, %rax
	movb	%al, 5(%rdi)
	movq	(%rsi), %rax
	shrq	$48, %rax
	movb	%al, 6(%rdi)
	movq	(%rsi), %rax
	shrq	$56, %rax
	movb	%al, 7(%rdi)
	movq	8(%rsi), %rax
	movb	%al, 8(%rdi)
	movq	8(%rsi), %rax
	shrq	$8, %rax
	movb	%al, 9(%rdi)
	movq	8(%rsi), %rax
	shrq	$16, %rax
	movb	%al, 10(%rdi)
	movq	8(%rsi), %rax
	shrq	$24, %rax
	movb	%al, 11(%rdi)
	movl	12(%rsi), %eax
	movb	%al, 12(%rdi)
	movq	8(%rsi), %rax
	shrq	$40, %rax
	movb	%al, 13(%rdi)
	movzwl	14(%rsi), %eax
	movb	%al, 14(%rdi)
	movzbl	15(%rsi), %eax
	movb	%al, 15(%rdi)
	movq	16(%rsi), %rax
	movb	%al, 16(%rdi)
	movq	16(%rsi), %rax
	shrq	$8, %rax
	movb	%al, 17(%rdi)
	movq	16(%rsi), %rax
	shrq	$16, %rax
	movb	%al, 18(%rdi)
	movq	16(%rsi), %rax
	shrq	$24, %rax
	movb	%al, 19(%rdi)
	movl	20(%rsi), %eax
	movb	%al, 20(%rdi)
	movq	16(%rsi), %rax
	shrq	$40, %rax
	movb	%al, 21(%rdi)
	movzwl	22(%rsi), %eax
	movb	%al, 22(%rdi)
	movzbl	23(%rsi), %eax
	movb	%al, 23(%rdi)
	movq	24(%rsi), %rax
	movb	%al, 24(%rdi)
	movq	24(%rsi), %rax
	shrq	$8, %rax
	movb	%al, 25(%rdi)
	movq	24(%rsi), %rax
	shrq	$16, %rax
	movb	%al, 26(%rdi)
	movq	24(%rsi), %rax
	shrq	$24, %rax
	movb	%al, 27(%rdi)
	movl	28(%rsi), %eax
	movb	%al, 28(%rdi)
	movq	24(%rsi), %rax
	shrq	$40, %rax
	movb	%al, 29(%rdi)
	movzwl	30(%rsi), %eax
	movb	%al, 30(%rdi)
	movzbl	31(%rsi), %eax
	movb	%al, 31(%rdi)
	movq	32(%rsi), %rax
	movb	%al, 32(%rdi)
	movq	32(%rsi), %rax
	shrq	$8, %rax
	movb	%al, 33(%rdi)
	movq	32(%rsi), %rax
	shrq	$16, %rax
	movb	%al, 34(%rdi)
	movq	32(%rsi), %rax
	shrq	$24, %rax
	movb	%al, 35(%rdi)
	movl	36(%rsi), %eax
	movb	%al, 36(%rdi)
	movq	32(%rsi), %rax
	shrq	$40, %rax
	movb	%al, 37(%rdi)
	movzwl	38(%rsi), %eax
	movb	%al, 38(%rdi)
	movzbl	39(%rsi), %eax
	movb	%al, 39(%rdi)
	movq	40(%rsi), %rax
	movb	%al, 40(%rdi)
	movq	40(%rsi), %rax
	shrq	$8, %rax
	movb	%al, 41(%rdi)
	movq	40(%rsi), %rax
	shrq	$16, %rax
	movb	%al, 42(%rdi)
	movq	40(%rsi), %rax
	shrq	$24, %rax
	movb	%al, 43(%rdi)
	movl	44(%rsi), %eax
	movb	%al, 44(%rdi)
	movq	40(%rsi), %rax
	shrq	$40, %rax
	movb	%al, 45(%rdi)
	movzwl	46(%rsi), %eax
	movb	%al, 46(%rdi)
	movzbl	47(%rsi), %eax
	movb	%al, 47(%rdi)
	movq	48(%rsi), %rax
	movb	%al, 48(%rdi)
	movq	48(%rsi), %rax
	shrq	$8, %rax
	movb	%al, 49(%rdi)
	movq	48(%rsi), %rax
	shrq	$16, %rax
	movb	%al, 50(%rdi)
	movq	48(%rsi), %rax
	shrq	$24, %rax
	movb	%al, 51(%rdi)
	movl	52(%rsi), %eax
	movb	%al, 52(%rdi)
	movq	48(%rsi), %rax
	shrq	$40, %rax
	movb	%al, 53(%rdi)
	movzwl	54(%rsi), %eax
	movb	%al, 54(%rdi)
	movzbl	55(%rsi), %eax
	movb	%al, 55(%rdi)
	ret
	.cfi_endproc
.LFE214:
	.size	curve448_scalar_encode, .-curve448_scalar_encode
	.p2align 4
	.globl	curve448_scalar_halve
	.type	curve448_scalar_halve, @function
curve448_scalar_halve:
.LFB215:
	.cfi_startproc
	endbr64
	movabsq	$2556006723728458995, %r9
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r11d, %r11d
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	(%rsi), %r8
	movabsq	$2408513697996967765, %rbx
	movq	%r8, %rcx
	andl	$1, %ecx
	negq	%rcx
	andq	%rcx, %r9
	leaq	(%r8,%r9), %rax
	movq	%rax, (%rdi)
	movq	%r8, %rax
	addq	%r9, %rax
	adcq	%r11, %rdx
	andq	%rcx, %rbx
	xorl	%r11d, %r11d
	movq	%rdx, %rax
	xorl	%edx, %edx
	movq	%rax, %r8
	movq	8(%rsi), %rax
	movq	%rdx, %r9
	xorl	%edx, %edx
	addq	%rbx, %rax
	movabsq	$-4301259484579875184, %rbx
	adcq	%r11, %rdx
	addq	%rax, %r8
	movq	%r8, 8(%rdi)
	movq	16(%rsi), %rax
	adcq	%rdx, %r9
	andq	%rcx, %rbx
	xorl	%r11d, %r11d
	movq	%r9, %r8
	xorl	%edx, %edx
	xorl	%r9d, %r9d
	addq	%rbx, %rax
	movdqu	(%rdi), %xmm0
	movabsq	$-2201345047, %rbx
	adcq	%r11, %rdx
	addq	%rax, %r8
	movq	%r8, 16(%rdi)
	movq	24(%rsi), %rax
	adcq	%rdx, %r9
	andq	%rcx, %rbx
	xorl	%r11d, %r11d
	movq	%r9, %r8
	xorl	%edx, %edx
	xorl	%r9d, %r9d
	addq	%rbx, %rax
	movdqu	8(%rdi), %xmm1
	psrlq	$1, %xmm0
	popq	%rbx
	adcq	%r11, %rdx
	addq	%rax, %r8
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	%r8, 24(%rdi)
	movq	32(%rsi), %rax
	adcq	%rdx, %r9
	xorl	%r11d, %r11d
	movq	%r9, %r8
	psllq	$63, %xmm1
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	addq	%rcx, %rax
	por	%xmm1, %xmm0
	adcq	%r11, %rdx
	addq	%rax, %r8
	movq	%r8, 32(%rdi)
	movq	40(%rsi), %rax
	adcq	%rdx, %r9
	xorl	%edx, %edx
	movq	%r9, %r8
	xorl	%r9d, %r9d
	movdqu	24(%rdi), %xmm1
	addq	%rcx, %rax
	adcq	%r11, %rdx
	psllq	$63, %xmm1
	addq	%r8, %rax
	adcq	%r9, %rdx
	movq	%rax, 40(%rdi)
	movq	48(%rsi), %r8
	xorl	%r9d, %r9d
	movabsq	$4611686018427387903, %rsi
	movq	%rdx, %rax
	movups	%xmm0, (%rdi)
	movdqu	16(%rdi), %xmm0
	andq	%rsi, %rcx
	xorl	%edx, %edx
	movq	%rax, %r10
	movq	%rcx, %rax
	psrlq	$1, %xmm0
	movq	%rdx, %r11
	xorl	%edx, %edx
	addq	%r8, %rax
	por	%xmm1, %xmm0
	movdqu	32(%rdi), %xmm1
	adcq	%r9, %rdx
	addq	%r10, %rax
	movups	%xmm0, 16(%rdi)
	movq	%rax, 48(%rdi)
	movdqu	40(%rdi), %xmm0
	adcq	%r11, %rdx
	psrlq	$1, %xmm1
	shrq	%rax
	salq	$63, %rdx
	psllq	$63, %xmm0
	orq	%rdx, %rax
	por	%xmm1, %xmm0
	movq	%rax, 48(%rdi)
	movups	%xmm0, 32(%rdi)
	ret
	.cfi_endproc
.LFE215:
	.size	curve448_scalar_halve, .-curve448_scalar_halve
	.globl	curve448_scalar_zero
	.section	.rodata
	.align 32
	.type	curve448_scalar_zero, @object
	.size	curve448_scalar_zero, 56
curve448_scalar_zero:
	.zero	56
	.globl	curve448_scalar_one
	.align 32
	.type	curve448_scalar_one, @object
	.size	curve448_scalar_one, 56
curve448_scalar_one:
	.quad	1
	.zero	48
	.align 32
	.type	sc_r2, @object
	.size	sc_r2, 56
sc_r2:
	.quad	-2066146901595808928
	.quad	8859473595851707865
	.quad	965703414319814745
	.quad	-5902020696520468424
	.quad	1917620071967259716
	.quad	2329131455307870383
	.quad	3747743906366994217
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
