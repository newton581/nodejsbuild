	.file	"cts128.c"
	.text
	.p2align 4
	.globl	CRYPTO_cts128_encrypt_block
	.type	CRYPTO_cts128_encrypt_block, @function
CRYPTO_cts128_encrypt_block:
.LFB151:
	.cfi_startproc
	endbr64
	cmpq	$16, %rdx
	jbe	.L68
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$40, %rsp
	andl	$15, %r14d
	movq	%r9, -72(%rbp)
	cmove	%rax, %r14
	movq	%rdx, %rax
	movq	%rcx, -64(%rbp)
	subq	%r14, %rax
	movq	%rax, %rdx
	movq	%rax, -56(%rbp)
	call	CRYPTO_cbc128_encrypt@PLT
	movq	-56(%rbp), %rax
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r9
	leaq	16(%r15,%rax), %rcx
	addq	%rax, %r12
	leaq	(%r15,%rax), %rdx
	cmpq	%rcx, %rbx
	leaq	16(%rbx), %rcx
	setnb	%sil
	cmpq	%rcx, %rdx
	setnb	%cl
	orb	%cl, %sil
	je	.L4
	leaq	-1(%r14), %rcx
	cmpq	$14, %rcx
	jbe	.L4
	movdqu	(%rdx), %xmm0
	movdqu	(%rbx), %xmm2
	pxor	%xmm2, %xmm0
	movups	%xmm0, (%rbx)
.L5:
	movq	%r10, %rdx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	*%r9
	leaq	-16(%r12), %rdx
	movl	%r14d, %eax
	cmpl	$8, %r14d
	jb	.L74
	movq	-16(%r12), %rax
	leaq	8(%r12), %rdi
	andq	$-8, %rdi
	movq	%rax, (%r12)
	movl	%r14d, %eax
	movq	-8(%rdx,%rax), %rcx
	movq	%rcx, -8(%r12,%rax)
	movq	%r12, %rax
	movq	%rdx, %rcx
	subq	%rdi, %rax
	subq	%rax, %rcx
	addl	%r14d, %eax
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L7
	andl	$-8, %eax
	xorl	%edx, %edx
.L10:
	movl	%edx, %esi
	addl	$8, %edx
	movq	(%rcx,%rsi), %r8
	movq	%r8, (%rdi,%rsi)
	cmpl	%eax, %edx
	jb	.L10
.L7:
	movdqu	(%rbx), %xmm1
	movq	%r13, %rax
	movups	%xmm1, -16(%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	andl	$4, %r14d
	jne	.L75
	testl	%eax, %eax
	je	.L7
	movzbl	-16(%r12), %ecx
	movb	%cl, (%r12)
	testb	$2, %al
	je	.L7
	movzwl	-2(%rdx,%rax), %edx
	movw	%dx, -2(%r12,%rax)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movzbl	(%rdx), %edx
	xorb	%dl, (%rbx)
	cmpq	$1, %r14
	je	.L5
	movzbl	1(%r15,%rax), %edx
	xorb	%dl, 1(%rbx)
	cmpq	$2, %r14
	je	.L5
	movzbl	2(%r15,%rax), %edx
	xorb	%dl, 2(%rbx)
	cmpq	$3, %r14
	je	.L5
	movzbl	3(%r15,%rax), %edx
	xorb	%dl, 3(%rbx)
	cmpq	$4, %r14
	je	.L5
	movzbl	4(%r15,%rax), %edx
	xorb	%dl, 4(%rbx)
	cmpq	$5, %r14
	je	.L5
	movzbl	5(%r15,%rax), %edx
	xorb	%dl, 5(%rbx)
	cmpq	$6, %r14
	je	.L5
	movzbl	6(%r15,%rax), %edx
	xorb	%dl, 6(%rbx)
	cmpq	$7, %r14
	je	.L5
	movzbl	7(%r15,%rax), %edx
	xorb	%dl, 7(%rbx)
	cmpq	$8, %r14
	je	.L5
	movzbl	8(%r15,%rax), %edx
	xorb	%dl, 8(%rbx)
	cmpq	$9, %r14
	je	.L5
	movzbl	9(%r15,%rax), %edx
	xorb	%dl, 9(%rbx)
	cmpq	$10, %r14
	je	.L5
	movzbl	10(%r15,%rax), %edx
	xorb	%dl, 10(%rbx)
	cmpq	$11, %r14
	je	.L5
	movzbl	11(%r15,%rax), %edx
	xorb	%dl, 11(%rbx)
	cmpq	$12, %r14
	je	.L5
	movzbl	12(%r15,%rax), %edx
	xorb	%dl, 12(%rbx)
	cmpq	$13, %r14
	je	.L5
	movzbl	13(%r15,%rax), %edx
	xorb	%dl, 13(%rbx)
	cmpq	$14, %r14
	je	.L5
	movzbl	14(%r15,%rax), %edx
	xorb	%dl, 14(%rbx)
	cmpq	$16, %r14
	jne	.L5
	movzbl	15(%r15,%rax), %eax
	xorb	%al, 15(%rbx)
	jmp	.L5
.L75:
	movl	-16(%r12), %ecx
	movl	%ecx, (%r12)
	movl	-4(%rdx,%rax), %edx
	movl	%edx, -4(%r12,%rax)
	jmp	.L7
	.cfi_endproc
.LFE151:
	.size	CRYPTO_cts128_encrypt_block, .-CRYPTO_cts128_encrypt_block
	.p2align 4
	.globl	CRYPTO_nistcts128_encrypt_block
	.type	CRYPTO_nistcts128_encrypt_block, @function
CRYPTO_nistcts128_encrypt_block:
.LFB152:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	cmpq	$15, %rdx
	ja	.L123
.L76:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore_state
	movq	%rdx, %r12
	movq	%rdx, %r11
	movq	%rdx, %r15
	movq	%rcx, -72(%rbp)
	andq	$-16, %r12
	andl	$15, %r11d
	movq	%rsi, -64(%rbp)
	movq	%rdi, %r14
	movq	%r12, %rdx
	movq	%r11, -56(%rbp)
	movq	%r8, %rbx
	movq	%r9, %r13
	call	CRYPTO_cbc128_encrypt@PLT
	movq	-56(%rbp), %r11
	testq	%r11, %r11
	je	.L76
	movzbl	(%r14,%r12), %edx
	xorb	%dl, (%rbx)
	cmpq	$1, %r11
	movq	-64(%rbp), %rax
	movq	-72(%rbp), %rcx
	je	.L78
	movzbl	1(%r14,%r12), %edx
	xorb	%dl, 1(%rbx)
	cmpq	$2, %r11
	je	.L78
	movzbl	2(%r14,%r12), %edx
	xorb	%dl, 2(%rbx)
	cmpq	$3, %r11
	je	.L78
	movzbl	3(%r14,%r12), %edx
	xorb	%dl, 3(%rbx)
	cmpq	$4, %r11
	je	.L78
	movzbl	4(%r14,%r12), %edx
	xorb	%dl, 4(%rbx)
	cmpq	$5, %r11
	je	.L78
	movzbl	5(%r14,%r12), %edx
	xorb	%dl, 5(%rbx)
	cmpq	$6, %r11
	je	.L78
	movzbl	6(%r14,%r12), %edx
	xorb	%dl, 6(%rbx)
	cmpq	$7, %r11
	je	.L78
	movzbl	7(%r14,%r12), %edx
	xorb	%dl, 7(%rbx)
	cmpq	$8, %r11
	je	.L78
	movzbl	8(%r14,%r12), %edx
	xorb	%dl, 8(%rbx)
	cmpq	$9, %r11
	je	.L78
	movzbl	9(%r14,%r12), %edx
	xorb	%dl, 9(%rbx)
	cmpq	$10, %r11
	je	.L78
	movzbl	10(%r14,%r12), %edx
	xorb	%dl, 10(%rbx)
	cmpq	$11, %r11
	je	.L78
	movzbl	11(%r14,%r12), %edx
	xorb	%dl, 11(%rbx)
	cmpq	$12, %r11
	je	.L78
	movzbl	12(%r14,%r12), %edx
	xorb	%dl, 12(%rbx)
	cmpq	$13, %r11
	je	.L78
	movzbl	13(%r14,%r12), %edx
	xorb	%dl, 13(%rbx)
	cmpq	$15, %r11
	jne	.L78
	movzbl	14(%r14,%r12), %edx
	xorb	%dl, 14(%rbx)
	.p2align 4,,10
	.p2align 3
.L78:
	movq	%rax, -56(%rbp)
	movq	%rcx, %rdx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	*%r13
	movdqu	(%rbx), %xmm0
	movq	-56(%rbp), %rax
	movq	%r15, %r12
	movups	%xmm0, -16(%rax,%r15)
	jmp	.L76
	.cfi_endproc
.LFE152:
	.size	CRYPTO_nistcts128_encrypt_block, .-CRYPTO_nistcts128_encrypt_block
	.p2align 4
	.globl	CRYPTO_cts128_encrypt
	.type	CRYPTO_cts128_encrypt, @function
CRYPTO_cts128_encrypt:
.LFB153:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	cmpq	$16, %rdx
	jbe	.L142
	movq	%rdx, %r13
	movq	%rdx, %r12
	movl	$16, %edx
	movq	%r8, -104(%rbp)
	andl	$15, %r13d
	movq	%rsi, %rbx
	movq	%rdi, -88(%rbp)
	movq	%r9, %r14
	cmove	%rdx, %r13
	movq	%r12, %rdx
	movl	$1, %r9d
	movq	%r15, %rcx
	subq	%r13, %rdx
	movq	%rdx, -96(%rbp)
	call	*%r14
	movq	-96(%rbp), %rdx
	movq	-88(%rbp), %rax
	leaq	-80(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movl	$16, %ecx
	addq	%rdx, %rbx
	leaq	(%rax,%rdx), %rsi
	movq	%r13, %rdx
	movaps	%xmm0, -80(%rbp)
	call	__memcpy_chk@PLT
	cmpl	$8, %r13d
	movq	-104(%rbp), %r8
	movl	%r13d, %esi
	movq	%rax, %rdi
	leaq	-16(%rbx), %r10
	jb	.L144
	movq	-16(%rbx), %rax
	leaq	8(%rbx), %rcx
	movq	%r10, %r9
	andq	$-8, %rcx
	movq	%rax, (%rbx)
	movl	%r13d, %eax
	movq	-8(%r10,%rax), %rdx
	movq	%rdx, -8(%rbx,%rax)
	subq	%rcx, %rbx
	leal	0(%r13,%rbx), %esi
	subq	%rbx, %r9
	andl	$-8, %esi
	cmpl	$8, %esi
	jb	.L128
	movl	%esi, %ebx
	xorl	%eax, %eax
	andl	$-8, %ebx
.L131:
	movl	%eax, %edx
	addl	$8, %eax
	movq	(%r9,%rdx), %rsi
	movq	%rsi, (%rcx,%rdx)
	cmpl	%ebx, %eax
	jb	.L131
.L128:
	movl	$1, %r9d
	movq	%r15, %rcx
	movl	$16, %edx
	movq	%r10, %rsi
	call	*%r14
	movq	%r12, %r9
.L124:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L145
	addq	$72, %rsp
	movq	%r9, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	andl	$4, %r13d
	jne	.L146
	testl	%esi, %esi
	je	.L128
	movzbl	-16(%rbx), %eax
	movb	%al, (%rbx)
	testb	$2, %sil
	je	.L128
	movzwl	-2(%r10,%rsi), %eax
	movw	%ax, -2(%rbx,%rsi)
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L142:
	xorl	%r9d, %r9d
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L146:
	movl	-16(%rbx), %eax
	movl	%eax, (%rbx)
	movl	-4(%r10,%rsi), %eax
	movl	%eax, -4(%rbx,%rsi)
	jmp	.L128
.L145:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE153:
	.size	CRYPTO_cts128_encrypt, .-CRYPTO_cts128_encrypt
	.p2align 4
	.globl	CRYPTO_nistcts128_encrypt
	.type	CRYPTO_nistcts128_encrypt, @function
CRYPTO_nistcts128_encrypt:
.LFB154:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$15, %rdx
	jbe	.L147
	movq	%rdx, %r11
	movq	%rdx, %rax
	movq	%r9, %r13
	movq	%rsi, -104(%rbp)
	andq	$-16, %rax
	andl	$15, %r11d
	movq	%rdi, %r12
	movq	%rdx, %rbx
	movq	%r11, -96(%rbp)
	movq	%rax, %rdx
	movq	%rcx, %r14
	movq	%r8, %r15
	movq	%rax, -88(%rbp)
	movl	$1, %r9d
	call	*%r13
	movq	-96(%rbp), %r11
	movq	-88(%rbp), %rax
	testq	%r11, %r11
	jne	.L155
.L147:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L156
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L155:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	movq	%r11, %rdx
	leaq	-80(%rbp), %rdi
	movl	$16, %ecx
	leaq	(%r12,%rax), %rsi
	movaps	%xmm0, -80(%rbp)
	call	__memcpy_chk@PLT
	movq	-104(%rbp), %r10
	movq	%r15, %r8
	movq	%r14, %rcx
	movq	%rax, %rdi
	movl	$1, %r9d
	movl	$16, %edx
	leaq	-16(%r10,%rbx), %rsi
	call	*%r13
	movq	%rbx, %rax
	jmp	.L147
.L156:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE154:
	.size	CRYPTO_nistcts128_encrypt, .-CRYPTO_nistcts128_encrypt
	.p2align 4
	.globl	CRYPTO_cts128_decrypt_block
	.type	CRYPTO_cts128_decrypt_block, @function
CRYPTO_cts128_decrypt_block:
.LFB155:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$16, %rdx
	jbe	.L157
	movq	%rdx, %r11
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	%rdx, %r14
	movq	%rcx, %r10
	movq	%r8, %r13
	movq	%r9, %r15
	andl	$15, %r11d
	je	.L167
	leaq	16(%r11), %rax
	leaq	-16(%r14), %rdx
	movq	%rax, -104(%rbp)
	subq	%r11, %rdx
	jne	.L226
.L160:
	leaq	-96(%rbp), %r9
	movq	%r10, %rdx
	movq	%r10, -120(%rbp)
	leaq	-80(%rbp), %rsi
	movq	%r11, -112(%rbp)
	movq	%rbx, %rdi
	movq	%r9, -128(%rbp)
	call	*%r15
	movq	-112(%rbp), %r11
	movq	-128(%rbp), %r9
	leaq	16(%rbx), %r8
	movdqa	-80(%rbp), %xmm2
	movq	%r8, %rsi
	movl	$32, %ecx
	movq	%r8, -112(%rbp)
	movq	%r11, %rdx
	movq	%r9, %rdi
	movaps	%xmm2, -96(%rbp)
	call	__memcpy_chk@PLT
	movq	-120(%rbp), %r10
	movq	%rax, %rsi
	movq	%rax, %rdi
	movq	%r10, %rdx
	call	*%r15
	leaq	15(%rbx), %rax
	movq	-112(%rbp), %r8
	movq	%rax, %rdx
	subq	%r12, %rdx
	cmpq	$30, %rdx
	seta	%dl
	subq	%r13, %rax
	cmpq	$30, %rax
	seta	%al
	testb	%al, %dl
	je	.L161
	leaq	15(%r12), %rax
	subq	%r13, %rax
	cmpq	$30, %rax
	jbe	.L161
	movdqu	0(%r13), %xmm0
	movdqu	(%rbx), %xmm1
	pxor	-96(%rbp), %xmm0
	movups	%xmm0, (%r12)
	movups	%xmm1, 0(%r13)
.L162:
	leaq	16(%r12), %rdx
	leaq	32(%rbx), %rax
	cmpq	%rax, %rdx
	leaq	32(%r12), %rax
	setnb	%dl
	cmpq	%rax, %r8
	setnb	%al
	orb	%al, %dl
	je	.L163
	movq	-104(%rbp), %rax
	subq	$17, %rax
	cmpq	$14, %rax
	jbe	.L163
	movdqu	16(%rbx), %xmm0
	pxor	-80(%rbp), %xmm0
	movups	%xmm0, 16(%r12)
.L164:
	leaq	16(%r14), %rax
.L157:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L227
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L167:
	.cfi_restore_state
	movl	$16, %r11d
	leaq	-16(%r14), %rdx
	movq	$32, -104(%rbp)
	subq	%r11, %rdx
	je	.L160
.L226:
	movq	%r10, %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%r15, %r9
	movq	%r13, %r8
	movq	%r11, -128(%rbp)
	movq	%r10, -120(%rbp)
	movq	%rdx, -112(%rbp)
	call	CRYPTO_cbc128_decrypt@PLT
	movq	-112(%rbp), %rdx
	movq	-128(%rbp), %r11
	movq	-120(%rbp), %r10
	addq	%rdx, %rbx
	addq	%rdx, %r12
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L161:
	movzbl	(%rbx), %edx
	movzbl	-96(%rbp), %eax
	xorb	0(%r13), %al
	movb	%al, (%r12)
	movzbl	-95(%rbp), %eax
	movb	%dl, 0(%r13)
	movzbl	1(%rbx), %edx
	xorb	1(%r13), %al
	movb	%al, 1(%r12)
	movzbl	-94(%rbp), %eax
	movb	%dl, 1(%r13)
	movzbl	2(%rbx), %edx
	xorb	2(%r13), %al
	movb	%al, 2(%r12)
	movzbl	-93(%rbp), %eax
	movb	%dl, 2(%r13)
	movzbl	3(%rbx), %edx
	xorb	3(%r13), %al
	movb	%al, 3(%r12)
	movzbl	-92(%rbp), %eax
	movb	%dl, 3(%r13)
	movzbl	4(%rbx), %edx
	xorb	4(%r13), %al
	movb	%al, 4(%r12)
	movzbl	-91(%rbp), %eax
	movb	%dl, 4(%r13)
	movzbl	5(%rbx), %edx
	xorb	5(%r13), %al
	movb	%al, 5(%r12)
	movzbl	-90(%rbp), %eax
	movb	%dl, 5(%r13)
	movzbl	6(%rbx), %edx
	xorb	6(%r13), %al
	movb	%al, 6(%r12)
	movzbl	-89(%rbp), %eax
	movb	%dl, 6(%r13)
	movzbl	7(%rbx), %edx
	xorb	7(%r13), %al
	movb	%al, 7(%r12)
	movzbl	-88(%rbp), %eax
	movb	%dl, 7(%r13)
	movzbl	8(%rbx), %edx
	xorb	8(%r13), %al
	movb	%al, 8(%r12)
	movzbl	-87(%rbp), %eax
	movb	%dl, 8(%r13)
	movzbl	9(%rbx), %edx
	xorb	9(%r13), %al
	movb	%al, 9(%r12)
	movzbl	-86(%rbp), %eax
	movb	%dl, 9(%r13)
	movzbl	10(%rbx), %edx
	xorb	10(%r13), %al
	movb	%al, 10(%r12)
	movzbl	-85(%rbp), %eax
	movb	%dl, 10(%r13)
	movzbl	11(%rbx), %edx
	xorb	11(%r13), %al
	movb	%al, 11(%r12)
	movzbl	-84(%rbp), %eax
	movb	%dl, 11(%r13)
	movzbl	12(%rbx), %edx
	xorb	12(%r13), %al
	movb	%al, 12(%r12)
	movb	%dl, 12(%r13)
	movzbl	13(%rbx), %edx
	movzbl	-83(%rbp), %eax
	xorb	13(%r13), %al
	movb	%al, 13(%r12)
	movzbl	-82(%rbp), %eax
	movb	%dl, 13(%r13)
	movzbl	14(%rbx), %edx
	xorb	14(%r13), %al
	movb	%al, 14(%r12)
	movzbl	-81(%rbp), %eax
	movb	%dl, 14(%r13)
	movzbl	15(%rbx), %edx
	xorb	15(%r13), %al
	movb	%al, 15(%r12)
	movb	%dl, 15(%r13)
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L163:
	movq	-104(%rbp), %rcx
	movzbl	-80(%rbp), %eax
	xorb	16(%rbx), %al
	movb	%al, 16(%r12)
	cmpq	$17, %rcx
	je	.L164
	movzbl	17(%rbx), %eax
	xorb	-79(%rbp), %al
	movb	%al, 17(%r12)
	cmpq	$18, %rcx
	je	.L164
	movzbl	-78(%rbp), %eax
	xorb	18(%rbx), %al
	movb	%al, 18(%r12)
	cmpq	$19, %rcx
	je	.L164
	movzbl	-77(%rbp), %eax
	xorb	19(%rbx), %al
	movb	%al, 19(%r12)
	cmpq	$20, %rcx
	je	.L164
	movzbl	-76(%rbp), %eax
	xorb	20(%rbx), %al
	movb	%al, 20(%r12)
	cmpq	$21, %rcx
	je	.L164
	movzbl	-75(%rbp), %eax
	xorb	21(%rbx), %al
	movb	%al, 21(%r12)
	cmpq	$22, %rcx
	je	.L164
	movzbl	-74(%rbp), %eax
	xorb	22(%rbx), %al
	movb	%al, 22(%r12)
	cmpq	$23, %rcx
	je	.L164
	movzbl	-73(%rbp), %eax
	xorb	23(%rbx), %al
	movb	%al, 23(%r12)
	cmpq	$24, %rcx
	je	.L164
	movzbl	-72(%rbp), %eax
	xorb	24(%rbx), %al
	movb	%al, 24(%r12)
	cmpq	$25, %rcx
	je	.L164
	movzbl	-71(%rbp), %eax
	xorb	25(%rbx), %al
	movb	%al, 25(%r12)
	cmpq	$26, %rcx
	je	.L164
	movzbl	-70(%rbp), %eax
	xorb	26(%rbx), %al
	movb	%al, 26(%r12)
	cmpq	$27, %rcx
	je	.L164
	movzbl	-69(%rbp), %eax
	xorb	27(%rbx), %al
	movb	%al, 27(%r12)
	cmpq	$28, %rcx
	je	.L164
	movzbl	-68(%rbp), %eax
	xorb	28(%rbx), %al
	movb	%al, 28(%r12)
	cmpq	$29, %rcx
	je	.L164
	movzbl	-67(%rbp), %eax
	xorb	29(%rbx), %al
	movb	%al, 29(%r12)
	cmpq	$30, %rcx
	je	.L164
	movzbl	-66(%rbp), %eax
	xorb	30(%rbx), %al
	movb	%al, 30(%r12)
	cmpq	$32, %rcx
	jne	.L164
	movzbl	31(%rbx), %eax
	xorb	-65(%rbp), %al
	movb	%al, 31(%r12)
	jmp	.L164
.L227:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE155:
	.size	CRYPTO_cts128_decrypt_block, .-CRYPTO_cts128_decrypt_block
	.p2align 4
	.globl	CRYPTO_nistcts128_decrypt_block
	.type	CRYPTO_nistcts128_decrypt_block, @function
CRYPTO_nistcts128_decrypt_block:
.LFB156:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$15, %rdx
	jbe	.L228
	movq	%rdx, %r15
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rdx, %r14
	movq	%rcx, %r10
	movq	%r8, %rbx
	andl	$15, %r15d
	je	.L286
	leaq	-16(%rdx), %rax
	subq	%r15, %rax
	jne	.L287
.L231:
	leaq	0(%r13,%r15), %r8
	leaq	-96(%rbp), %r11
	movq	%r10, %rdx
	movq	%r10, -120(%rbp)
	movq	%r8, %rdi
	movq	%r8, -112(%rbp)
	leaq	-80(%rbp), %rsi
	movq	%r9, -104(%rbp)
	movq	%r11, -128(%rbp)
	call	*%r9
	movq	-128(%rbp), %r11
	movdqa	-80(%rbp), %xmm2
	movq	%r15, %rdx
	movl	$32, %ecx
	movq	%r13, %rsi
	movq	%r11, %rdi
	movaps	%xmm2, -96(%rbp)
	call	__memcpy_chk@PLT
	movq	-120(%rbp), %r10
	movq	-104(%rbp), %r9
	movq	%rax, %rsi
	movq	%rax, %rdi
	movq	%r10, %rdx
	call	*%r9
	movq	-112(%rbp), %r8
	leaq	16(%rbx), %rdx
	leaq	16(%r15), %rax
	leaq	0(%r13,%rax), %rdi
	leaq	15(%r13), %rcx
	cmpq	%rdx, %r8
	setnb	%dl
	cmpq	%rdi, %rbx
	setnb	%sil
	orl	%esi, %edx
	leaq	16(%r12), %rsi
	cmpq	%rsi, %r8
	setnb	%sil
	cmpq	%rdi, %r12
	setnb	%dil
	orl	%edi, %esi
	andl	%esi, %edx
	movq	%rcx, %rsi
	subq	%r12, %rsi
	cmpq	$30, %rsi
	seta	%sil
	subq	%rbx, %rcx
	andl	%esi, %edx
	cmpq	$30, %rcx
	seta	%cl
	testb	%cl, %dl
	je	.L232
	leaq	15(%r12), %rdx
	subq	%rbx, %rdx
	cmpq	$30, %rdx
	jbe	.L232
	movdqu	(%rbx), %xmm0
	pxor	-96(%rbp), %xmm0
	movdqu	0(%r13), %xmm1
	movups	%xmm0, (%r12)
	movdqu	(%r8), %xmm3
	movaps	%xmm1, -96(%rbp)
	movups	%xmm3, (%rbx)
.L233:
	movzbl	-80(%rbp), %edx
	xorb	-96(%rbp), %dl
	movb	%dl, 16(%r12)
	cmpq	$17, %rax
	je	.L234
	movzbl	-79(%rbp), %edx
	xorb	-95(%rbp), %dl
	movb	%dl, 17(%r12)
	cmpq	$18, %rax
	je	.L234
	movzbl	-78(%rbp), %edx
	xorb	-94(%rbp), %dl
	movb	%dl, 18(%r12)
	cmpq	$19, %rax
	je	.L234
	movzbl	-77(%rbp), %edx
	xorb	-93(%rbp), %dl
	movb	%dl, 19(%r12)
	cmpq	$20, %rax
	je	.L234
	movzbl	-76(%rbp), %edx
	xorb	-92(%rbp), %dl
	movb	%dl, 20(%r12)
	cmpq	$21, %rax
	je	.L234
	movzbl	-75(%rbp), %edx
	xorb	-91(%rbp), %dl
	movb	%dl, 21(%r12)
	cmpq	$22, %rax
	je	.L234
	movzbl	-74(%rbp), %edx
	xorb	-90(%rbp), %dl
	movb	%dl, 22(%r12)
	cmpq	$23, %rax
	je	.L234
	movzbl	-73(%rbp), %edx
	xorb	-89(%rbp), %dl
	movb	%dl, 23(%r12)
	cmpq	$24, %rax
	je	.L234
	movzbl	-72(%rbp), %edx
	xorb	-88(%rbp), %dl
	movb	%dl, 24(%r12)
	cmpq	$25, %rax
	je	.L234
	movzbl	-71(%rbp), %edx
	xorb	-87(%rbp), %dl
	movb	%dl, 25(%r12)
	cmpq	$26, %rax
	je	.L234
	movzbl	-70(%rbp), %edx
	xorb	-86(%rbp), %dl
	movb	%dl, 26(%r12)
	cmpq	$27, %rax
	je	.L234
	movzbl	-69(%rbp), %edx
	xorb	-85(%rbp), %dl
	movb	%dl, 27(%r12)
	cmpq	$28, %rax
	je	.L234
	movzbl	-68(%rbp), %edx
	xorb	-84(%rbp), %dl
	movb	%dl, 28(%r12)
	cmpq	$29, %rax
	je	.L234
	movzbl	-67(%rbp), %edx
	xorb	-83(%rbp), %dl
	movb	%dl, 29(%r12)
	cmpq	$31, %rax
	jne	.L234
	movzbl	-66(%rbp), %eax
	xorb	-82(%rbp), %al
	movb	%al, 30(%r12)
	.p2align 4,,10
	.p2align 3
.L234:
	leaq	16(%r14), %rax
.L228:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L288
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L287:
	.cfi_restore_state
	movq	%rax, %rdx
	movq	%r9, -120(%rbp)
	movq	%rax, -104(%rbp)
	movq	%rcx, -112(%rbp)
	call	CRYPTO_cbc128_decrypt@PLT
	movq	-104(%rbp), %rax
	movq	-120(%rbp), %r9
	movq	-112(%rbp), %r10
	addq	%rax, %r13
	addq	%rax, %r12
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L232:
	movzbl	-96(%rbp), %edx
	xorb	(%rbx), %dl
	movzbl	0(%r13), %ecx
	movb	%dl, (%r12)
	movzbl	(%r8), %edx
	movb	%cl, -96(%rbp)
	movb	%dl, (%rbx)
	movzbl	-95(%rbp), %edx
	xorb	1(%rbx), %dl
	movzbl	1(%r13), %ecx
	movb	%dl, 1(%r12)
	movzbl	1(%r13,%r15), %edx
	movb	%cl, -95(%rbp)
	movb	%dl, 1(%rbx)
	movzbl	-94(%rbp), %edx
	xorb	2(%rbx), %dl
	movzbl	2(%r13), %ecx
	movb	%dl, 2(%r12)
	movzbl	2(%r13,%r15), %edx
	movb	%cl, -94(%rbp)
	movb	%dl, 2(%rbx)
	movzbl	-93(%rbp), %edx
	xorb	3(%rbx), %dl
	movzbl	3(%r13), %ecx
	movb	%dl, 3(%r12)
	movzbl	3(%r13,%r15), %edx
	movb	%cl, -93(%rbp)
	movb	%dl, 3(%rbx)
	movzbl	-92(%rbp), %edx
	xorb	4(%rbx), %dl
	movzbl	4(%r13), %ecx
	movb	%dl, 4(%r12)
	movzbl	4(%r13,%r15), %edx
	movb	%cl, -92(%rbp)
	movb	%dl, 4(%rbx)
	movzbl	-91(%rbp), %edx
	xorb	5(%rbx), %dl
	movzbl	5(%r13), %ecx
	movb	%dl, 5(%r12)
	movzbl	5(%r13,%r15), %edx
	movb	%cl, -91(%rbp)
	movb	%dl, 5(%rbx)
	movzbl	-90(%rbp), %edx
	xorb	6(%rbx), %dl
	movzbl	6(%r13), %ecx
	movb	%dl, 6(%r12)
	movzbl	6(%r13,%r15), %edx
	movb	%cl, -90(%rbp)
	movb	%dl, 6(%rbx)
	movzbl	-89(%rbp), %edx
	xorb	7(%rbx), %dl
	movzbl	7(%r13), %ecx
	movb	%dl, 7(%r12)
	movzbl	7(%r13,%r15), %edx
	movb	%cl, -89(%rbp)
	movb	%dl, 7(%rbx)
	movzbl	-88(%rbp), %edx
	xorb	8(%rbx), %dl
	movzbl	8(%r13), %ecx
	movb	%dl, 8(%r12)
	movzbl	8(%r13,%r15), %edx
	movb	%cl, -88(%rbp)
	movb	%dl, 8(%rbx)
	movzbl	9(%r13), %ecx
	movzbl	-87(%rbp), %edx
	xorb	9(%rbx), %dl
	movb	%dl, 9(%r12)
	movzbl	9(%r13,%r15), %edx
	movb	%cl, -87(%rbp)
	movb	%dl, 9(%rbx)
	movzbl	-86(%rbp), %edx
	xorb	10(%rbx), %dl
	movzbl	10(%r13), %ecx
	movb	%dl, 10(%r12)
	movzbl	10(%r13,%r15), %edx
	movb	%cl, -86(%rbp)
	movb	%dl, 10(%rbx)
	movzbl	-85(%rbp), %edx
	xorb	11(%rbx), %dl
	movzbl	11(%r13), %ecx
	movb	%dl, 11(%r12)
	movzbl	11(%r13,%r15), %edx
	movb	%cl, -85(%rbp)
	movb	%dl, 11(%rbx)
	movzbl	-84(%rbp), %edx
	xorb	12(%rbx), %dl
	movzbl	12(%r13), %ecx
	movb	%dl, 12(%r12)
	movzbl	12(%r13,%r15), %edx
	movb	%cl, -84(%rbp)
	movb	%dl, 12(%rbx)
	movzbl	-83(%rbp), %edx
	xorb	13(%rbx), %dl
	movzbl	13(%r13), %ecx
	movb	%dl, 13(%r12)
	movzbl	13(%r13,%r15), %edx
	movb	%dl, 13(%rbx)
	movzbl	-82(%rbp), %edx
	xorb	14(%rbx), %dl
	movb	%cl, -83(%rbp)
	movzbl	14(%r13), %ecx
	movb	%dl, 14(%r12)
	movzbl	14(%r13,%r15), %edx
	movb	%cl, -82(%rbp)
	movb	%dl, 14(%rbx)
	movzbl	-81(%rbp), %edx
	xorb	15(%rbx), %dl
	movzbl	15(%r13), %ecx
	movb	%dl, 15(%r12)
	movzbl	15(%r13,%r15), %edx
	movb	%cl, -81(%rbp)
	movb	%dl, 15(%rbx)
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L286:
	call	CRYPTO_cbc128_decrypt@PLT
	movq	%r14, %rax
	jmp	.L228
.L288:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE156:
	.size	CRYPTO_nistcts128_decrypt_block, .-CRYPTO_nistcts128_decrypt_block
	.p2align 4
	.globl	CRYPTO_cts128_decrypt
	.type	CRYPTO_cts128_decrypt, @function
CRYPTO_cts128_decrypt:
.LFB157:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$16, %rdx
	jbe	.L289
	movq	%rdx, %rax
	movq	%rsi, %rbx
	movq	%rdx, %r12
	movq	%rcx, %r10
	andl	$15, %eax
	movq	%r8, %r11
	movq	%r9, %r15
	leaq	16(%rax), %r14
	je	.L303
	leaq	-16(%r12), %rdx
	subq	%rax, %rdx
	movq	%rdx, %r13
	jne	.L304
.L292:
	leaq	-96(%rbp), %r13
	pxor	%xmm0, %xmm0
	movq	%r10, %rcx
	movq	%r11, -128(%rbp)
	movq	%r10, -112(%rbp)
	xorl	%r9d, %r9d
	leaq	-80(%rbp), %r8
	movq	%r13, %rsi
	movaps	%xmm0, -96(%rbp)
	movl	$16, %edx
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -120(%rbp)
	movq	%rdi, -104(%rbp)
	call	*%r15
	movq	-104(%rbp), %rdi
	movq	-120(%rbp), %rax
	movl	$32, %ecx
	leaq	16(%rdi), %rsi
	movq	%rax, %rdx
	movq	%r13, %rdi
	call	__memcpy_chk@PLT
	movq	-128(%rbp), %r11
	movq	-112(%rbp), %r10
	movq	%r13, %rsi
	movl	$32, %edx
	movq	%r13, %rdi
	xorl	%r9d, %r9d
	movq	%r11, %r8
	movq	%r10, %rcx
	call	*%r15
	movq	-96(%rbp), %rax
	leaq	8(%rbx), %rdx
	andq	$-8, %rdx
	movq	%rax, (%rbx)
	movq	-104(%rbp,%r14), %rax
	movq	%rax, -8(%rbx,%r14)
	subq	%rdx, %rbx
	xorl	%eax, %eax
	subq	%rbx, %r13
	addl	%r14d, %ebx
	andl	$-8, %ebx
.L293:
	movl	%eax, %ecx
	addl	$8, %eax
	movq	0(%r13,%rcx), %rsi
	movq	%rsi, (%rdx,%rcx)
	cmpl	%ebx, %eax
	jb	.L293
	movq	%r12, %rax
.L289:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L305
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L304:
	.cfi_restore_state
	movq	%rax, -128(%rbp)
	movq	%r11, %r8
	movq	%r10, %rcx
	movq	%rbx, %rsi
	movq	%r11, -120(%rbp)
	xorl	%r9d, %r9d
	addq	%r13, %rbx
	movq	%r10, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	*%r15
	movq	-104(%rbp), %rdi
	movq	-128(%rbp), %rax
	movq	-120(%rbp), %r11
	movq	-112(%rbp), %r10
	addq	%r13, %rdi
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L303:
	movl	$16, %eax
	leaq	-16(%r12), %rdx
	movl	$32, %r14d
	subq	%rax, %rdx
	movq	%rdx, %r13
	je	.L292
	jmp	.L304
.L305:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE157:
	.size	CRYPTO_cts128_decrypt, .-CRYPTO_cts128_decrypt
	.p2align 4
	.globl	CRYPTO_nistcts128_decrypt
	.type	CRYPTO_nistcts128_decrypt, @function
CRYPTO_nistcts128_decrypt:
.LFB158:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %r11
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	cmpq	$15, %rdx
	jbe	.L306
	movq	%rdx, %r10
	movq	%rdi, %rax
	movq	%rsi, %r12
	movq	%rdx, %rbx
	movq	%r9, %r14
	andl	$15, %r10d
	je	.L319
	leaq	-16(%rdx), %rdx
	subq	%r10, %rdx
	movq	%rdx, %r13
	je	.L309
	movq	%r10, -120(%rbp)
	movq	%r11, %r8
	xorl	%r9d, %r9d
	movq	%r15, %rcx
	movq	%r11, -112(%rbp)
	addq	%r13, %r12
	movq	%rdi, -104(%rbp)
	call	*%r14
	movq	-104(%rbp), %rax
	movq	-120(%rbp), %r10
	movq	-112(%rbp), %r11
	addq	%r13, %rax
.L309:
	leaq	-96(%rbp), %r13
	pxor	%xmm0, %xmm0
	leaq	(%rax,%r10), %rdi
	movq	%r11, -120(%rbp)
	movaps	%xmm0, -96(%rbp)
	xorl	%r9d, %r9d
	leaq	-80(%rbp), %r8
	movq	%r13, %rsi
	movaps	%xmm0, -80(%rbp)
	movq	%r15, %rcx
	movl	$16, %edx
	movq	%r10, -104(%rbp)
	movq	%rax, -112(%rbp)
	call	*%r14
	movq	-104(%rbp), %r10
	movq	-112(%rbp), %rax
	movq	%r13, %rdi
	movl	$32, %ecx
	movq	%r10, %rdx
	movq	%rax, %rsi
	call	__memcpy_chk@PLT
	movq	-120(%rbp), %r11
	movq	%r13, %rsi
	movq	%r15, %rcx
	movq	%r13, %rdi
	xorl	%r9d, %r9d
	movl	$32, %edx
	movq	%r11, %r8
	call	*%r14
	movq	-96(%rbp), %rax
	leaq	8(%r12), %rcx
	andq	$-8, %rcx
	movq	%rax, (%r12)
	movq	-104(%rbp), %r10
	movq	-88(%rbp,%r10), %rax
	movq	%rax, 8(%r12,%r10)
	subq	%rcx, %r12
	xorl	%eax, %eax
	leal	16(%r10,%r12), %esi
	subq	%r12, %r13
	andl	$-8, %esi
.L310:
	movl	%eax, %edx
	addl	$8, %eax
	movq	0(%r13,%rdx), %rdi
	movq	%rdi, (%rcx,%rdx)
	cmpl	%esi, %eax
	jb	.L310
	movq	%rbx, %r8
.L306:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L320
	addq	$88, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L319:
	.cfi_restore_state
	movq	%r11, %r8
	xorl	%r9d, %r9d
	movq	%r15, %rcx
	call	*%r14
	movq	%rbx, %r8
	jmp	.L306
.L320:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE158:
	.size	CRYPTO_nistcts128_decrypt, .-CRYPTO_nistcts128_decrypt
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
