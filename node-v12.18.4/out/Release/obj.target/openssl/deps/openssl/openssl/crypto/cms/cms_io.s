	.file	"cms_io.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/cms/cms_io.c"
	.text
	.p2align 4
	.globl	CMS_stream
	.type	CMS_stream, @function
CMS_stream:
.LFB1392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	call	CMS_get0_content@PLT
	testq	%rax, %rax
	je	.L5
	movq	%rax, %rbx
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L8
.L3:
	movq	16(%rax), %rdx
	addq	$8, %rax
	andq	$-33, %rdx
	orq	$16, %rdx
	movq	%rdx, 8(%rax)
	popq	%rbx
	movq	%rax, (%r12)
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	call	ASN1_OCTET_STRING_new@PLT
	movq	%rax, (%rbx)
	testq	%rax, %rax
	jne	.L3
	movl	$31, %r8d
	movl	$65, %edx
	movl	$155, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1392:
	.size	CMS_stream, .-CMS_stream
	.p2align 4
	.globl	d2i_CMS_bio
	.type	d2i_CMS_bio, @function
d2i_CMS_bio:
.LFB1393:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	CMS_ContentInfo_it(%rip), %rdi
	jmp	ASN1_item_d2i_bio@PLT
	.cfi_endproc
.LFE1393:
	.size	d2i_CMS_bio, .-d2i_CMS_bio
	.p2align 4
	.globl	i2d_CMS_bio
	.type	i2d_CMS_bio, @function
i2d_CMS_bio:
.LFB1394:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	CMS_ContentInfo_it(%rip), %rdi
	jmp	ASN1_item_i2d_bio@PLT
	.cfi_endproc
.LFE1394:
	.size	i2d_CMS_bio, .-i2d_CMS_bio
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"CMS"
	.text
	.p2align 4
	.globl	PEM_read_bio_CMS
	.type	PEM_read_bio_CMS, @function
PEM_read_bio_CMS:
.LFB1395:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	movq	%rdi, %rdx
	movq	d2i_CMS_ContentInfo@GOTPCREL(%rip), %rdi
	movq	%rcx, %r9
	movq	%rsi, %rcx
	leaq	.LC1(%rip), %rsi
	jmp	PEM_ASN1_read_bio@PLT
	.cfi_endproc
.LFE1395:
	.size	PEM_read_bio_CMS, .-PEM_read_bio_CMS
	.p2align 4
	.globl	PEM_read_CMS
	.type	PEM_read_CMS, @function
PEM_read_CMS:
.LFB1396:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	movq	%rdi, %rdx
	movq	d2i_CMS_ContentInfo@GOTPCREL(%rip), %rdi
	movq	%rcx, %r9
	movq	%rsi, %rcx
	leaq	.LC1(%rip), %rsi
	jmp	PEM_ASN1_read@PLT
	.cfi_endproc
.LFE1396:
	.size	PEM_read_CMS, .-PEM_read_CMS
	.p2align 4
	.globl	PEM_write_bio_CMS
	.type	PEM_write_bio_CMS, @function
PEM_write_bio_CMS:
.LFB1397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	i2d_CMS_ContentInfo@GOTPCREL(%rip), %rdi
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	.LC1(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write_bio@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1397:
	.size	PEM_write_bio_CMS, .-PEM_write_bio_CMS
	.p2align 4
	.globl	PEM_write_CMS
	.type	PEM_write_CMS, @function
PEM_write_CMS:
.LFB1398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	i2d_CMS_ContentInfo@GOTPCREL(%rip), %rdi
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	.LC1(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1398:
	.size	PEM_write_CMS, .-PEM_write_CMS
	.p2align 4
	.globl	BIO_new_CMS
	.type	BIO_new_CMS, @function
BIO_new_CMS:
.LFB1399:
	.cfi_startproc
	endbr64
	leaq	CMS_ContentInfo_it(%rip), %rdx
	jmp	BIO_new_NDEF@PLT
	.cfi_endproc
.LFE1399:
	.size	BIO_new_CMS, .-BIO_new_CMS
	.p2align 4
	.globl	i2d_CMS_bio_stream
	.type	i2d_CMS_bio_stream, @function
i2d_CMS_bio_stream:
.LFB1400:
	.cfi_startproc
	endbr64
	leaq	CMS_ContentInfo_it(%rip), %r8
	jmp	i2d_ASN1_bio_stream@PLT
	.cfi_endproc
.LFE1400:
	.size	i2d_CMS_bio_stream, .-i2d_CMS_bio_stream
	.p2align 4
	.globl	PEM_write_bio_CMS_stream
	.type	PEM_write_bio_CMS_stream, @function
PEM_write_bio_CMS_stream:
.LFB1401:
	.cfi_startproc
	endbr64
	leaq	CMS_ContentInfo_it(%rip), %r9
	leaq	.LC1(%rip), %r8
	jmp	PEM_write_bio_ASN1_stream@PLT
	.cfi_endproc
.LFE1401:
	.size	PEM_write_bio_CMS_stream, .-PEM_write_bio_CMS_stream
	.p2align 4
	.globl	SMIME_write_CMS
	.type	SMIME_write_CMS, @function
SMIME_write_CMS:
.LFB1402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	call	OBJ_obj2nid@PLT
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	CMS_get0_eContentType@PLT
	movq	%rax, %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %r9d
	xorl	%eax, %eax
	cmpl	$22, %ebx
	jne	.L21
	movq	8(%r12), %rax
	movq	8(%rax), %rax
.L21:
	leaq	CMS_ContentInfo_it(%rip), %rdx
	movl	%ebx, %r8d
	movl	%r15d, %ecx
	movq	%r12, %rsi
	pushq	%rdx
	movq	%r13, %rdi
	movq	%r14, %rdx
	pushq	%rax
	call	SMIME_write_ASN1@PLT
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1402:
	.size	SMIME_write_CMS, .-SMIME_write_CMS
	.p2align 4
	.globl	SMIME_read_CMS
	.type	SMIME_read_CMS, @function
SMIME_read_CMS:
.LFB1403:
	.cfi_startproc
	endbr64
	leaq	CMS_ContentInfo_it(%rip), %rdx
	jmp	SMIME_read_ASN1@PLT
	.cfi_endproc
.LFE1403:
	.size	SMIME_read_CMS, .-SMIME_read_CMS
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
