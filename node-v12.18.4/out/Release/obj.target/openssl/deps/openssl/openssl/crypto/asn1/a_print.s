	.file	"a_print.c"
	.text
	.p2align 4
	.globl	ASN1_PRINTABLE_type
	.type	ASN1_PRINTABLE_type, @function
ASN1_PRINTABLE_type:
.LFB419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	testl	%esi, %esi
	jle	.L30
	testq	%rdi, %rdi
	je	.L6
	movzbl	(%rdi), %eax
	testb	%al, %al
	je	.L6
	subl	$1, %esi
.L5:
	leaq	1(%r12,%rsi), %rcx
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	movl	$1, %r13d
	movq	%rcx, -56(%rbp)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L31:
	cmpq	%r12, -56(%rbp)
	je	.L9
.L10:
	movzbl	%al, %ebx
	movl	$2048, %esi
	addq	$1, %r12
	movl	%ebx, %edi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	movzbl	(%r12), %eax
	cmove	%r13d, %r14d
	andl	$128, %ebx
	cmovne	%r13d, %r15d
	testb	%al, %al
	jne	.L31
.L9:
	movl	$20, %eax
	testl	%r15d, %r15d
	jne	.L1
	testl	%r14d, %r14d
	jne	.L32
.L6:
	movl	$19, %eax
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	testq	%rdi, %rdi
	je	.L6
	movzbl	(%rdi), %eax
	movl	$4294967294, %esi
	testb	%al, %al
	jne	.L5
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L32:
	addq	$24, %rsp
	movl	$22, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE419:
	.size	ASN1_PRINTABLE_type, .-ASN1_PRINTABLE_type
	.p2align 4
	.globl	ASN1_UNIVERSALSTRING_to_string
	.type	ASN1_UNIVERSALSTRING_to_string, @function
ASN1_UNIVERSALSTRING_to_string:
.LFB420:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$28, 4(%rdi)
	jne	.L79
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	(%rdi), %ecx
	testb	$3, %cl
	jne	.L33
	movq	8(%rdi), %rdx
	testl	%ecx, %ecx
	jle	.L35
	xorl	%eax, %eax
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L82:
	cmpb	$0, 1(%rdx,%rax)
	jne	.L36
	cmpb	$0, 2(%rdx,%rax)
	jne	.L36
	addq	$4, %rax
	cmpl	%eax, %ecx
	jle	.L35
.L37:
	cmpb	$0, (%rdx,%rax)
	movl	%eax, %esi
	je	.L82
.L36:
	xorl	%eax, %eax
	cmpl	%esi, %ecx
	jg	.L33
.L35:
	cmpl	$3, %ecx
	jle	.L52
	movzbl	3(%rdx), %eax
	leaq	1(%rdx), %rcx
	movb	%al, (%rdx)
	cmpl	$7, (%r12)
	movl	$7, %eax
	jle	.L38
	.p2align 4,,10
	.p2align 3
.L39:
	movq	8(%r12), %rdx
	addq	$1, %rcx
	movzbl	(%rdx,%rax), %edx
	addq	$4, %rax
	movb	%dl, -1(%rcx)
	cmpl	%eax, (%r12)
	jg	.L39
.L38:
	movb	$0, (%rcx)
	movl	(%r12), %eax
	movq	8(%r12), %r15
	testl	%eax, %eax
	leal	3(%rax), %edx
	cmovns	%eax, %edx
	sarl	$2, %edx
	movl	%edx, (%r12)
	cmpl	$3, %eax
	jle	.L83
	testq	%r15, %r15
	je	.L42
	movzbl	(%r15), %eax
	testb	%al, %al
	je	.L42
	leal	-1(%rdx), %ecx
	testl	%edx, %edx
	je	.L42
.L43:
	leaq	1(%r15,%rcx), %rdi
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	movq	%rdi, -56(%rbp)
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L84:
	cmpq	-56(%rbp), %r15
	je	.L46
.L47:
	movzbl	%al, %ebx
	movl	$2048, %esi
	addq	$1, %r15
	movl	%ebx, %edi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	movl	$1, %eax
	cmove	%eax, %r14d
	andl	$128, %ebx
	cmovne	%eax, %r13d
	movzbl	(%r15), %eax
	testb	%al, %al
	jne	.L84
.L46:
	movl	$20, %eax
	testl	%r13d, %r13d
	jne	.L48
	movl	$22, %eax
	testl	%r14d, %r14d
	jne	.L48
.L42:
	movl	$19, %eax
.L48:
	movl	%eax, 4(%r12)
	movl	$1, %eax
.L33:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	testq	%r15, %r15
	je	.L42
	movzbl	(%r15), %eax
	movl	$4294967294, %ecx
	testb	%al, %al
	jne	.L43
	jmp	.L42
.L52:
	movq	%rdx, %rcx
	jmp	.L38
	.cfi_endproc
.LFE420:
	.size	ASN1_UNIVERSALSTRING_to_string, .-ASN1_UNIVERSALSTRING_to_string
	.p2align 4
	.globl	ASN1_STRING_print
	.type	ASN1_STRING_print, @function
ASN1_STRING_print:
.LFB421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L92
	movq	8(%rsi), %r14
	movq	%rsi, %r12
	movl	(%rsi), %esi
	movq	%rdi, %r15
	xorl	%ebx, %ebx
	xorl	%edx, %edx
	movl	$46, %r13d
	testl	%esi, %esi
	jg	.L88
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L109:
	cmpb	$31, %al
	setle	%r8b
	cmpb	$10, %al
	setne	%cl
	testb	%cl, %r8b
	je	.L89
	cmpb	$13, %al
	cmovne	%r13d, %eax
.L89:
	movslq	%edx, %rcx
	addl	$1, %edx
	movb	%al, -144(%rbp,%rcx)
	cmpl	$79, %edx
	jg	.L107
	addq	$1, %rbx
	cmpl	%ebx, %esi
	jle	.L108
.L88:
	movzbl	(%r14,%rbx), %eax
	cmpb	$127, %al
	jne	.L109
	movl	$46, %eax
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L107:
	leaq	-144(%rbp), %rsi
	movq	%r15, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jle	.L92
	movl	(%r12), %esi
	addq	$1, %rbx
	xorl	%edx, %edx
	cmpl	%ebx, %esi
	jg	.L88
.L108:
	testl	%edx, %edx
	jne	.L110
.L93:
	movl	$1, %eax
.L85:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L111
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	leaq	-144(%rbp), %rsi
	movq	%r15, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L92:
	xorl	%eax, %eax
	jmp	.L85
.L111:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE421:
	.size	ASN1_STRING_print, .-ASN1_STRING_print
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
