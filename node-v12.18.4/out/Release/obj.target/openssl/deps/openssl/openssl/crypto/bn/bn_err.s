	.file	"bn_err.c"
	.text
	.p2align 4
	.globl	ERR_load_BN_strings
	.type	ERR_load_BN_strings, @function
ERR_load_BN_strings:
.LFB203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$50851840, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_func_error_string@PLT
	testq	%rax, %rax
	je	.L5
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	leaq	BN_str_functs(%rip), %rdi
	call	ERR_load_strings_const@PLT
	leaq	BN_str_reasons(%rip), %rdi
	call	ERR_load_strings_const@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE203:
	.size	ERR_load_BN_strings, .-ERR_load_BN_strings
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"arg2 lt arg3"
.LC1:
	.string	"bad reciprocal"
.LC2:
	.string	"bignum too long"
.LC3:
	.string	"bits too small"
.LC4:
	.string	"called with even modulus"
.LC5:
	.string	"div by zero"
.LC6:
	.string	"encoding error"
.LC7:
	.string	"expand on static bignum data"
.LC8:
	.string	"input not reduced"
.LC9:
	.string	"invalid length"
.LC10:
	.string	"invalid range"
.LC11:
	.string	"invalid shift"
.LC12:
	.string	"not a square"
.LC13:
	.string	"not initialized"
.LC14:
	.string	"no inverse"
.LC15:
	.string	"no solution"
.LC16:
	.string	"private key too large"
.LC17:
	.string	"p is not prime"
.LC18:
	.string	"too many iterations"
.LC19:
	.string	"too many temporary variables"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	BN_str_reasons, @object
	.size	BN_str_reasons, 336
BN_str_reasons:
	.quad	50331748
	.quad	.LC0
	.quad	50331749
	.quad	.LC1
	.quad	50331762
	.quad	.LC2
	.quad	50331766
	.quad	.LC3
	.quad	50331750
	.quad	.LC4
	.quad	50331751
	.quad	.LC5
	.quad	50331752
	.quad	.LC6
	.quad	50331753
	.quad	.LC7
	.quad	50331758
	.quad	.LC8
	.quad	50331754
	.quad	.LC9
	.quad	50331763
	.quad	.LC10
	.quad	50331767
	.quad	.LC11
	.quad	50331759
	.quad	.LC12
	.quad	50331755
	.quad	.LC13
	.quad	50331756
	.quad	.LC14
	.quad	50331764
	.quad	.LC15
	.quad	50331765
	.quad	.LC16
	.quad	50331760
	.quad	.LC17
	.quad	50331761
	.quad	.LC18
	.quad	50331757
	.quad	.LC19
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC20:
	.string	"bnrand"
.LC21:
	.string	"bnrand_range"
.LC22:
	.string	"BN_BLINDING_convert_ex"
.LC23:
	.string	"BN_BLINDING_create_param"
.LC24:
	.string	"BN_BLINDING_invert_ex"
.LC25:
	.string	"BN_BLINDING_new"
.LC26:
	.string	"BN_BLINDING_update"
.LC27:
	.string	"BN_bn2dec"
.LC28:
	.string	"BN_bn2hex"
.LC29:
	.string	"bn_compute_wNAF"
.LC30:
	.string	"BN_CTX_get"
.LC31:
	.string	"BN_CTX_new"
.LC32:
	.string	"BN_CTX_start"
.LC33:
	.string	"BN_div"
.LC34:
	.string	"BN_div_recp"
.LC35:
	.string	"BN_exp"
.LC36:
	.string	"bn_expand_internal"
.LC37:
	.string	"BN_GENCB_new"
.LC38:
	.string	"BN_generate_dsa_nonce"
.LC39:
	.string	"BN_generate_prime_ex"
.LC40:
	.string	"BN_GF2m_mod"
.LC41:
	.string	"BN_GF2m_mod_exp"
.LC42:
	.string	"BN_GF2m_mod_mul"
.LC43:
	.string	"BN_GF2m_mod_solve_quad"
.LC44:
	.string	"BN_GF2m_mod_solve_quad_arr"
.LC45:
	.string	"BN_GF2m_mod_sqr"
.LC46:
	.string	"BN_GF2m_mod_sqrt"
.LC47:
	.string	"BN_lshift"
.LC48:
	.string	"BN_mod_exp2_mont"
.LC49:
	.string	"BN_mod_exp_mont"
.LC50:
	.string	"BN_mod_exp_mont_consttime"
.LC51:
	.string	"BN_mod_exp_mont_word"
.LC52:
	.string	"BN_mod_exp_recp"
.LC53:
	.string	"BN_mod_exp_simple"
.LC54:
	.string	"BN_mod_inverse"
.LC55:
	.string	"BN_mod_inverse_no_branch"
.LC56:
	.string	"BN_mod_lshift_quick"
.LC57:
	.string	"BN_mod_sqrt"
.LC58:
	.string	"BN_MONT_CTX_new"
.LC59:
	.string	"BN_mpi2bn"
.LC60:
	.string	"BN_new"
.LC61:
	.string	"BN_POOL_get"
.LC62:
	.string	"BN_rand"
.LC63:
	.string	"BN_rand_range"
.LC64:
	.string	"BN_RECP_CTX_new"
.LC65:
	.string	"BN_rshift"
.LC66:
	.string	"bn_set_words"
.LC67:
	.string	"BN_STACK_push"
.LC68:
	.string	"BN_usub"
	.section	.data.rel.ro.local
	.align 32
	.type	BN_str_functs, @object
	.size	BN_str_functs, 800
BN_str_functs:
	.quad	50851840
	.quad	.LC20
	.quad	50896896
	.quad	.LC21
	.quad	50741248
	.quad	.LC22
	.quad	50855936
	.quad	.LC23
	.quad	50745344
	.quad	.LC24
	.quad	50749440
	.quad	.LC25
	.quad	50753536
	.quad	.LC26
	.quad	50757632
	.quad	.LC27
	.quad	50761728
	.quad	.LC28
	.quad	50913280
	.quad	.LC29
	.quad	50806784
	.quad	.LC30
	.quad	50765824
	.quad	.LC31
	.quad	50860032
	.quad	.LC32
	.quad	50769920
	.quad	.LC33
	.quad	50864128
	.quad	.LC34
	.quad	50835456
	.quad	.LC35
	.quad	50823168
	.quad	.LC36
	.quad	50917376
	.quad	.LC37
	.quad	50905088
	.quad	.LC38
	.quad	50909184
	.quad	.LC39
	.quad	50868224
	.quad	.LC40
	.quad	50872320
	.quad	.LC41
	.quad	50876416
	.quad	.LC42
	.quad	50880512
	.quad	.LC43
	.quad	50884608
	.quad	.LC44
	.quad	50888704
	.quad	.LC45
	.quad	50892800
	.quad	.LC46
	.quad	50925568
	.quad	.LC47
	.quad	50814976
	.quad	.LC48
	.quad	50778112
	.quad	.LC49
	.quad	50839552
	.quad	.LC50
	.quad	50810880
	.quad	.LC51
	.quad	50843648
	.quad	.LC52
	.quad	50847744
	.quad	.LC53
	.quad	50782208
	.quad	.LC54
	.quad	50900992
	.quad	.LC55
	.quad	50819072
	.quad	.LC56
	.quad	50827264
	.quad	.LC57
	.quad	50941952
	.quad	.LC58
	.quad	50790400
	.quad	.LC59
	.quad	50794496
	.quad	.LC60
	.quad	50933760
	.quad	.LC61
	.quad	50798592
	.quad	.LC62
	.quad	50831360
	.quad	.LC63
	.quad	50946048
	.quad	.LC64
	.quad	50929664
	.quad	.LC65
	.quad	50921472
	.quad	.LC66
	.quad	50937856
	.quad	.LC67
	.quad	50802688
	.quad	.LC68
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
