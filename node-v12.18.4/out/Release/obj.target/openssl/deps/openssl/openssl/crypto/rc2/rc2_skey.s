	.file	"rc2_skey.c"
	.text
	.p2align 4
	.globl	RC2_set_key
	.type	RC2_set_key, @function
RC2_set_key:
.LFB0:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$128, %r9d
	cmpl	$128, %esi
	movl	%r9d, %eax
	movb	$0, (%rdi)
	cmovle	%esi, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testl	%ecx, %ecx
	jle	.L15
	cmpl	$1024, %ecx
	movl	$1024, %r8d
	movl	$255, %ebx
	cmovg	%r8d, %ecx
	leal	7(%rcx), %r8d
	negl	%ecx
	sarl	$3, %r8d
	andl	$7, %ecx
	subl	%r8d, %r9d
	sarl	%cl, %ebx
	movl	$127, %ecx
	movslq	%r9d, %r10
	subl	%r8d, %ecx
	addq	%rdi, %r10
	testl	%esi, %esi
	jle	.L3
.L49:
	leaq	15(%rdi), %r11
	subq	%rdx, %r11
	cmpq	$30, %r11
	jbe	.L16
	cmpl	$15, %esi
	jle	.L16
	movdqu	(%rdx), %xmm0
	movl	%eax, %r11d
	shrl	$4, %r11d
	movups	%xmm0, (%rdi)
	cmpl	$1, %r11d
	je	.L5
	movdqu	16(%rdx), %xmm1
	movups	%xmm1, 16(%rdi)
	cmpl	$2, %r11d
	je	.L5
	movdqu	32(%rdx), %xmm2
	movups	%xmm2, 32(%rdi)
	cmpl	$3, %r11d
	je	.L5
	movdqu	48(%rdx), %xmm3
	movups	%xmm3, 48(%rdi)
	cmpl	$4, %r11d
	je	.L5
	movdqu	64(%rdx), %xmm4
	movups	%xmm4, 64(%rdi)
	cmpl	$5, %r11d
	je	.L5
	movdqu	80(%rdx), %xmm5
	movups	%xmm5, 80(%rdi)
	cmpl	$6, %r11d
	je	.L5
	movdqu	96(%rdx), %xmm6
	movups	%xmm6, 96(%rdi)
	cmpl	$8, %r11d
	jne	.L5
	movdqu	112(%rdx), %xmm7
	movups	%xmm7, 112(%rdi)
.L9:
	cltq
	movzbl	-1(%rdi,%rax), %edx
	cmpl	$127, %esi
	jg	.L48
.L6:
	movq	%rdi, %r12
	leaq	key_table(%rip), %rsi
	subq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L12:
	addb	(%r12,%rax), %dl
	movzbl	%dl, %edx
	movzbl	(%rsi,%rdx), %edx
	movb	%dl, (%rdi,%rax)
	addq	$1, %rax
	cmpl	$127, %eax
	jle	.L12
.L7:
	andb	(%r10), %bl
	movzbl	%bl, %eax
	movzbl	(%rsi,%rax), %eax
	movb	%al, (%r10)
	testl	%r9d, %r9d
	je	.L11
	movslq	%ecx, %r9
	movl	%ecx, %ecx
	movslq	%r8d, %r8
	leaq	(%rdi,%r9), %rdx
	leaq	-1(%rdi,%r9), %r9
	subq	%rcx, %r9
	.p2align 4,,10
	.p2align 3
.L14:
	xorb	(%rdx,%r8), %al
	subq	$1, %rdx
	movzbl	%al, %eax
	movzbl	(%rsi,%rax), %eax
	movb	%al, 1(%rdx)
	cmpq	%r9, %rdx
	jne	.L14
.L11:
	movl	$127, %eax
	.p2align 4,,10
	.p2align 3
.L13:
	movzbl	(%rdi,%rax), %edx
	movzbl	-1(%rdi,%rax), %ecx
	sall	$8, %edx
	orl	%ecx, %edx
	movl	%edx, -2(%rdi,%rax,2)
	subq	$2, %rax
	cmpq	$-1, %rax
	jne	.L13
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5:
	.cfi_restore_state
	movl	%eax, %r11d
	andl	$-16, %r11d
	testb	$15, %al
	je	.L9
	movl	%r11d, %r12d
	movzbl	(%rdx,%r12), %r14d
	movb	%r14b, (%rdi,%r12)
	leal	1(%r11), %r12d
	cmpl	%r12d, %eax
	jle	.L9
	movslq	%r12d, %r12
	movzbl	(%rdx,%r12), %r14d
	movb	%r14b, (%rdi,%r12)
	leal	2(%r11), %r12d
	cmpl	%r12d, %eax
	jle	.L9
	movslq	%r12d, %r12
	movzbl	(%rdx,%r12), %r14d
	movb	%r14b, (%rdi,%r12)
	leal	3(%r11), %r12d
	cmpl	%r12d, %eax
	jle	.L9
	movslq	%r12d, %r12
	movzbl	(%rdx,%r12), %r14d
	movb	%r14b, (%rdi,%r12)
	leal	4(%r11), %r12d
	cmpl	%r12d, %eax
	jle	.L9
	movslq	%r12d, %r12
	movzbl	(%rdx,%r12), %r14d
	movb	%r14b, (%rdi,%r12)
	leal	5(%r11), %r12d
	cmpl	%r12d, %eax
	jle	.L9
	movslq	%r12d, %r12
	movzbl	(%rdx,%r12), %r14d
	movb	%r14b, (%rdi,%r12)
	leal	6(%r11), %r12d
	cmpl	%r12d, %eax
	jle	.L9
	movslq	%r12d, %r12
	movzbl	(%rdx,%r12), %r14d
	movb	%r14b, (%rdi,%r12)
	leal	7(%r11), %r12d
	cmpl	%eax, %r12d
	jge	.L9
	movslq	%r12d, %r12
	movzbl	(%rdx,%r12), %r14d
	movb	%r14b, (%rdi,%r12)
	leal	8(%r11), %r12d
	cmpl	%r12d, %eax
	jle	.L9
	movslq	%r12d, %r12
	movzbl	(%rdx,%r12), %r14d
	movb	%r14b, (%rdi,%r12)
	leal	9(%r11), %r12d
	cmpl	%r12d, %eax
	jle	.L9
	movslq	%r12d, %r12
	movzbl	(%rdx,%r12), %r14d
	movb	%r14b, (%rdi,%r12)
	leal	10(%r11), %r12d
	cmpl	%r12d, %eax
	jle	.L9
	movslq	%r12d, %r12
	movzbl	(%rdx,%r12), %r14d
	movb	%r14b, (%rdi,%r12)
	leal	11(%r11), %r12d
	cmpl	%r12d, %eax
	jle	.L9
	movslq	%r12d, %r12
	movzbl	(%rdx,%r12), %r14d
	movb	%r14b, (%rdi,%r12)
	leal	12(%r11), %r12d
	cmpl	%r12d, %eax
	jle	.L9
	movslq	%r12d, %r12
	movzbl	(%rdx,%r12), %r14d
	movb	%r14b, (%rdi,%r12)
	leal	13(%r11), %r12d
	cmpl	%r12d, %eax
	jle	.L9
	movslq	%r12d, %r12
	addl	$14, %r11d
	movzbl	(%rdx,%r12), %r14d
	movb	%r14b, (%rdi,%r12)
	cmpl	%r11d, %eax
	jle	.L9
	movslq	%r11d, %r11
	cltq
	movzbl	(%rdx,%r11), %edx
	movb	%dl, (%rdi,%r11)
	movzbl	-1(%rdi,%rax), %edx
	cmpl	$127, %esi
	jle	.L6
.L48:
	leaq	key_table(%rip), %rsi
	jmp	.L7
.L15:
	movq	%rdi, %r10
	movl	$-1, %ecx
	movl	$255, %ebx
	xorl	%r9d, %r9d
	movl	$128, %r8d
	testl	%esi, %esi
	jg	.L49
.L3:
	cltq
	movzbl	-1(%rdi,%rax), %edx
	jmp	.L6
.L16:
	xorl	%r11d, %r11d
	.p2align 4,,10
	.p2align 3
.L4:
	movzbl	(%rdx,%r11), %r12d
	movb	%r12b, (%rdi,%r11)
	addq	$1, %r11
	cmpl	%r11d, %eax
	jg	.L4
	jmp	.L9
	.cfi_endproc
.LFE0:
	.size	RC2_set_key, .-RC2_set_key
	.section	.rodata
	.align 32
	.type	key_table, @object
	.size	key_table, 256
key_table:
	.string	"\331x\371\304\031\335\265\355(\351\375yJ\240\330\235\306~7\203+vS\216bLd\210D\213\373\242\027\232Y\365\207\263O\023aEm\215\t\201}2\275\217@\353\206\267{\013\360\225!\"\\kN\202T\326e\223\316`\262\034sV\300\024\247\214\361\334\022u\312\037;\276\344\321B=\3240\243<\266&o\277\016\332Fi\007W'\362\035\233\274\224C\003\370\021\307\366\220\357>\347\006\303\325/\310f\036\327\b\350\352\336\200R\356\367\204\252r\2545Mj*\226\032\322qZ\025ItK\237\320^\004\030\244\354\302\340An\017Q\313\314$\221\257P\241\364p9\231|:\205#\270\264z\374\0026[%U\2271-]\372\230\343\212\222\256\005\337)\020gl\272\311\323"
	.ascii	"\346\317\341\236\250,c\026\001?X\342\211\251\r84\033\2533\377"
	.ascii	"\260\273H\f_\271\261\315.\305\363\333G\345\245\234w\n\246 h\376"
	.ascii	"\177\301\255"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
