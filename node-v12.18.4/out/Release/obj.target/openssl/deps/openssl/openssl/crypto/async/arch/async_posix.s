	.file	"async_posix.c"
	.text
	.p2align 4
	.globl	ASYNC_is_capable
	.type	ASYNC_is_capable, @function
ASYNC_is_capable:
.LFB176:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$976, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-976(%rbp), %rdi
	call	getcontext@PLT
	endbr64
	testl	%eax, %eax
	sete	%al
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L6
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movzbl	%al, %eax
	ret
.L6:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE176:
	.size	ASYNC_is_capable, .-ASYNC_is_capable
	.p2align 4
	.globl	async_local_cleanup
	.type	async_local_cleanup, @function
async_local_cleanup:
.LFB177:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE177:
	.size	async_local_cleanup, .-async_local_cleanup
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/async/arch/async_posix.c"
	.text
	.p2align 4
	.globl	async_fibre_makecontext
	.type	async_fibre_makecontext, @function
async_fibre_makecontext:
.LFB178:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movq	%rdi, -24(%rbp)
	movl	$0, 1168(%rdi)
	call	getcontext@PLT
	endbr64
	testl	%eax, %eax
	je	.L13
	movq	-24(%rbp), %rax
	xorl	%r12d, %r12d
	movq	$0, 16(%rax)
.L8:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	movl	$39, %edx
	leaq	.LC0(%rip), %rsi
	movl	$32768, %edi
	movl	%eax, %r12d
	call	CRYPTO_malloc@PLT
	movq	-24(%rbp), %rcx
	movq	%rax, 16(%rcx)
	testq	%rax, %rax
	je	.L8
	movq	$32768, 32(%rcx)
	xorl	%eax, %eax
	xorl	%edx, %edx
	movq	%rcx, %rdi
	movq	$0, 8(%rcx)
	movq	async_start_func@GOTPCREL(%rip), %rsi
	movl	$1, %r12d
	call	makecontext@PLT
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE178:
	.size	async_fibre_makecontext, .-async_fibre_makecontext
	.p2align 4
	.globl	async_fibre_free
	.type	async_fibre_free, @function
async_fibre_free:
.LFB179:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$54, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	call	CRYPTO_free@PLT
	movq	$0, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE179:
	.size	async_fibre_free, .-async_fibre_free
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
