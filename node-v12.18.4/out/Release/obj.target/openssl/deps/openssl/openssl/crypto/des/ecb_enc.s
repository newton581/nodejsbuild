	.file	"ecb_enc.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"des(int)"
	.text
	.p2align 4
	.globl	DES_options
	.type	DES_options, @function
DES_options:
.LFB175:
	.cfi_startproc
	endbr64
	movl	init.5775(%rip), %eax
	testl	%eax, %eax
	jne	.L8
	leaq	buf.5776(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$12, %edx
	leaq	.LC0(%rip), %rsi
	leaq	buf.5776(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	OPENSSL_strlcpy@PLT
	leaq	buf.5776(%rip), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	$0, init.5775(%rip)
	ret
	.cfi_endproc
.LFE175:
	.size	DES_options, .-DES_options
	.p2align 4
	.globl	DES_ecb_encrypt
	.type	DES_ecb_encrypt, @function
DES_ecb_encrypt:
.LFB176:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	movl	%ecx, %edx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	leaq	-32(%rbp), %rdi
	movq	%rax, -32(%rbp)
	call	DES_encrypt1@PLT
	movq	-32(%rbp), %rax
	movq	%rax, (%rbx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L12
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L12:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE176:
	.size	DES_ecb_encrypt, .-DES_ecb_encrypt
	.local	buf.5776
	.comm	buf.5776,12,8
	.data
	.align 4
	.type	init.5775, @object
	.size	init.5775, 4
init.5775:
	.long	1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
