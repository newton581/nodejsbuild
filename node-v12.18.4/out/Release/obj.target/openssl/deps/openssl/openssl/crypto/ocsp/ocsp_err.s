	.file	"ocsp_err.c"
	.text
	.p2align 4
	.globl	ERR_load_OCSP_strings
	.type	ERR_load_OCSP_strings, @function
ERR_load_OCSP_strings:
.LFB203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$654729216, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_func_error_string@PLT
	testq	%rax, %rax
	je	.L5
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	leaq	OCSP_str_functs(%rip), %rdi
	call	ERR_load_strings_const@PLT
	leaq	OCSP_str_reasons(%rip), %rdi
	call	ERR_load_strings_const@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE203:
	.size	ERR_load_OCSP_strings, .-ERR_load_OCSP_strings
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"certificate verify error"
.LC1:
	.string	"digest err"
.LC2:
	.string	"error in nextupdate field"
.LC3:
	.string	"error in thisupdate field"
.LC4:
	.string	"error parsing url"
.LC5:
	.string	"missing ocspsigning usage"
.LC6:
	.string	"nextupdate before thisupdate"
.LC7:
	.string	"not basic response"
.LC8:
	.string	"no certificates in chain"
.LC9:
	.string	"no response data"
.LC10:
	.string	"no revoked time"
.LC11:
	.string	"no signer key"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"private key does not match certificate"
	.section	.rodata.str1.1
.LC13:
	.string	"request not signed"
	.section	.rodata.str1.8
	.align 8
.LC14:
	.string	"response contains no revocation data"
	.section	.rodata.str1.1
.LC15:
	.string	"root ca not trusted"
.LC16:
	.string	"server response error"
.LC17:
	.string	"server response parse error"
.LC18:
	.string	"signature failure"
.LC19:
	.string	"signer certificate not found"
.LC20:
	.string	"status expired"
.LC21:
	.string	"status not yet valid"
.LC22:
	.string	"status too old"
.LC23:
	.string	"unknown message digest"
.LC24:
	.string	"unknown nid"
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"unsupported requestorname type"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	OCSP_str_reasons, @object
	.size	OCSP_str_reasons, 432
OCSP_str_reasons:
	.quad	654311525
	.quad	.LC0
	.quad	654311526
	.quad	.LC1
	.quad	654311546
	.quad	.LC2
	.quad	654311547
	.quad	.LC3
	.quad	654311545
	.quad	.LC4
	.quad	654311527
	.quad	.LC5
	.quad	654311548
	.quad	.LC6
	.quad	654311528
	.quad	.LC7
	.quad	654311529
	.quad	.LC8
	.quad	654311532
	.quad	.LC9
	.quad	654311533
	.quad	.LC10
	.quad	654311554
	.quad	.LC11
	.quad	654311534
	.quad	.LC12
	.quad	654311552
	.quad	.LC13
	.quad	654311535
	.quad	.LC14
	.quad	654311536
	.quad	.LC15
	.quad	654311538
	.quad	.LC16
	.quad	654311539
	.quad	.LC17
	.quad	654311541
	.quad	.LC18
	.quad	654311542
	.quad	.LC19
	.quad	654311549
	.quad	.LC20
	.quad	654311550
	.quad	.LC21
	.quad	654311551
	.quad	.LC22
	.quad	654311543
	.quad	.LC23
	.quad	654311544
	.quad	.LC24
	.quad	654311553
	.quad	.LC25
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC26:
	.string	"d2i_ocsp_nonce"
.LC27:
	.string	"OCSP_basic_add1_status"
.LC28:
	.string	"OCSP_basic_sign"
.LC29:
	.string	"OCSP_basic_sign_ctx"
.LC30:
	.string	"OCSP_basic_verify"
.LC31:
	.string	"OCSP_cert_id_new"
.LC32:
	.string	"ocsp_check_delegated"
.LC33:
	.string	"ocsp_check_ids"
.LC34:
	.string	"ocsp_check_issuer"
.LC35:
	.string	"OCSP_check_validity"
.LC36:
	.string	"ocsp_match_issuerid"
.LC37:
	.string	"OCSP_parse_url"
.LC38:
	.string	"OCSP_request_sign"
.LC39:
	.string	"OCSP_request_verify"
.LC40:
	.string	"OCSP_response_get1_basic"
.LC41:
	.string	"parse_http_line1"
	.section	.data.rel.ro.local
	.align 32
	.type	OCSP_str_functs, @object
	.size	OCSP_str_functs, 272
OCSP_str_functs:
	.quad	654729216
	.quad	.LC26
	.quad	654733312
	.quad	.LC27
	.quad	654737408
	.quad	.LC28
	.quad	654798848
	.quad	.LC29
	.quad	654741504
	.quad	.LC30
	.quad	654725120
	.quad	.LC31
	.quad	654745600
	.quad	.LC32
	.quad	654749696
	.quad	.LC33
	.quad	654753792
	.quad	.LC34
	.quad	654782464
	.quad	.LC35
	.quad	654757888
	.quad	.LC36
	.quad	654778368
	.quad	.LC37
	.quad	654761984
	.quad	.LC38
	.quad	654786560
	.quad	.LC39
	.quad	654766080
	.quad	.LC40
	.quad	654794752
	.quad	.LC41
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
