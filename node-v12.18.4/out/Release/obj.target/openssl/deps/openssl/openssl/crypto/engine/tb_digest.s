	.file	"tb_digest.c"
	.text
	.p2align 4
	.type	engine_unregister_all_digests, @function
engine_unregister_all_digests:
.LFB867:
	.cfi_startproc
	endbr64
	leaq	digest_table(%rip), %rdi
	jmp	engine_table_cleanup@PLT
	.cfi_endproc
.LFE867:
	.size	engine_unregister_all_digests, .-engine_unregister_all_digests
	.p2align 4
	.globl	ENGINE_unregister_digests
	.type	ENGINE_unregister_digests, @function
ENGINE_unregister_digests:
.LFB866:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	leaq	digest_table(%rip), %rdi
	jmp	engine_table_unregister@PLT
	.cfi_endproc
.LFE866:
	.size	ENGINE_unregister_digests, .-ENGINE_unregister_digests
	.p2align 4
	.globl	ENGINE_register_digests
	.type	ENGINE_register_digests, @function
ENGINE_register_digests:
.LFB868:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	$1, %r13d
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	64(%rdi), %rax
	testq	%rax, %rax
	je	.L4
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	leaq	-32(%rbp), %rdx
	movq	%rdi, %r12
	call	*%rax
	testl	%eax, %eax
	jg	.L11
.L4:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L12
	addq	$16, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movq	-32(%rbp), %rcx
	xorl	%r9d, %r9d
	movl	%eax, %r8d
	movq	%r12, %rdx
	leaq	engine_unregister_all_digests(%rip), %rsi
	leaq	digest_table(%rip), %rdi
	call	engine_table_register@PLT
	movl	%eax, %r13d
	jmp	.L4
.L12:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE868:
	.size	ENGINE_register_digests, .-ENGINE_register_digests
	.p2align 4
	.globl	ENGINE_register_all_digests
	.type	ENGINE_register_all_digests, @function
ENGINE_register_all_digests:
.LFB869:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	ENGINE_get_first@PLT
	testq	%rax, %rax
	je	.L13
	movq	%rax, %r12
	leaq	-32(%rbp), %rbx
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L15:
	movq	%r12, %rdi
	call	ENGINE_get_next@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L13
.L17:
	movq	64(%r12), %rax
	testq	%rax, %rax
	je	.L15
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	jle	.L15
	movq	-32(%rbp), %rcx
	movq	%r12, %rdx
	xorl	%r9d, %r9d
	movl	%eax, %r8d
	leaq	engine_unregister_all_digests(%rip), %rsi
	leaq	digest_table(%rip), %rdi
	call	engine_table_register@PLT
	movq	%r12, %rdi
	call	ENGINE_get_next@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L17
	.p2align 4,,10
	.p2align 3
.L13:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L27
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L27:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE869:
	.size	ENGINE_register_all_digests, .-ENGINE_register_all_digests
	.p2align 4
	.globl	ENGINE_set_default_digests
	.type	ENGINE_set_default_digests, @function
ENGINE_set_default_digests:
.LFB870:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	$1, %r13d
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	64(%rdi), %rax
	testq	%rax, %rax
	je	.L28
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	leaq	-32(%rbp), %rdx
	movq	%rdi, %r12
	call	*%rax
	testl	%eax, %eax
	jg	.L35
.L28:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L36
	addq	$16, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movq	-32(%rbp), %rcx
	movl	$1, %r9d
	movl	%eax, %r8d
	movq	%r12, %rdx
	leaq	engine_unregister_all_digests(%rip), %rsi
	leaq	digest_table(%rip), %rdi
	call	engine_table_register@PLT
	movl	%eax, %r13d
	jmp	.L28
.L36:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE870:
	.size	ENGINE_set_default_digests, .-ENGINE_set_default_digests
	.p2align 4
	.globl	ENGINE_get_digest_engine
	.type	ENGINE_get_digest_engine, @function
ENGINE_get_digest_engine:
.LFB871:
	.cfi_startproc
	endbr64
	movl	%edi, %esi
	leaq	digest_table(%rip), %rdi
	jmp	engine_table_select@PLT
	.cfi_endproc
.LFE871:
	.size	ENGINE_get_digest_engine, .-ENGINE_get_digest_engine
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/engine/tb_digest.c"
	.text
	.p2align 4
	.globl	ENGINE_get_digest
	.type	ENGINE_get_digest, @function
ENGINE_get_digest:
.LFB872:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	64(%rdi), %rax
	testq	%rax, %rax
	je	.L41
	movl	%esi, %ecx
	xorl	%edx, %edx
	leaq	-16(%rbp), %rsi
	call	*%rax
	testl	%eax, %eax
	je	.L41
	movq	-16(%rbp), %rax
.L38:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L47
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movl	$74, %r8d
	movl	$147, %edx
	movl	$186, %esi
	movl	$38, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L38
.L47:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE872:
	.size	ENGINE_get_digest, .-ENGINE_get_digest
	.p2align 4
	.globl	ENGINE_get_digests
	.type	ENGINE_get_digests, @function
ENGINE_get_digests:
.LFB873:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	ret
	.cfi_endproc
.LFE873:
	.size	ENGINE_get_digests, .-ENGINE_get_digests
	.p2align 4
	.globl	ENGINE_set_digests
	.type	ENGINE_set_digests, @function
ENGINE_set_digests:
.LFB874:
	.cfi_startproc
	endbr64
	movq	%rsi, 64(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE874:
	.size	ENGINE_set_digests, .-ENGINE_set_digests
	.local	digest_table
	.comm	digest_table,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
