	.file	"ssl_sess.c"
	.text
	.p2align 4
	.type	def_generate_session_id, @function
def_generate_session_id:
.LFB1035:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	$10, %ebx
	.p2align 4,,10
	.p2align 3
.L4:
	movl	(%r12), %esi
	movq	%r13, %rdi
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jle	.L2
	movl	(%r12), %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	SSL_has_matching_session_id@PLT
	testl	%eax, %eax
	je	.L5
	subl	$1, %ebx
	jne	.L4
.L2:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1035:
	.size	def_generate_session_id, .-def_generate_session_id
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/ssl/ssl_sess.c"
	.text
	.p2align 4
	.type	SSL_SESSION_free.part.0, @function
SSL_SESSION_free.part.0:
.LFB1103:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	520(%rdi), %rdx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$2, %edi
	subq	$8, %rsp
	call	CRYPTO_free_ex_data@PLT
	leaq	80(%r12), %rdi
	movl	$256, %esi
	call	OPENSSL_cleanse@PLT
	leaq	344(%r12), %rdi
	movl	$32, %esi
	call	OPENSSL_cleanse@PLT
	movq	440(%r12), %rdi
	call	X509_free@PLT
	movq	456(%r12), %rdi
	movq	X509_free@GOTPCREL(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	544(%r12), %rdi
	movl	$765, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	552(%r12), %rdi
	movl	$766, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	416(%r12), %rdi
	movl	$768, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	424(%r12), %rdi
	movl	$769, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	608(%r12), %rdi
	movl	$772, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	584(%r12), %rdi
	movl	$774, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	616(%r12), %rdi
	movl	$775, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	640(%r12), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$777, %ecx
	popq	%r12
	leaq	.LC0(%rip), %rdx
	movl	$648, %esi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_clear_free@PLT
	.cfi_endproc
.LFE1103:
	.size	SSL_SESSION_free.part.0, .-SSL_SESSION_free.part.0
	.p2align 4
	.type	timeout_cb, @function
timeout_cb:
.LFB1072:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	8(%rsi), %rdx
	movq	%rsi, %rbx
	testq	%rdx, %rdx
	je	.L12
	movq	480(%rdi), %rax
	addq	488(%rdi), %rax
	cmpq	%rax, %rdx
	jle	.L11
.L12:
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	OPENSSL_LH_delete@PLT
	movq	536(%r12), %rax
	movq	(%rbx), %rdi
	testq	%rax, %rax
	je	.L14
	movq	528(%r12), %rdx
	testq	%rdx, %rdx
	je	.L14
	leaq	64(%rdi), %rsi
	leaq	56(%rdi), %rcx
	cmpq	%rsi, %rax
	je	.L38
	cmpq	%rdx, %rcx
	je	.L39
	movq	%rdx, 528(%rax)
	movq	%rax, 536(%rdx)
.L17:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 528(%r12)
.L14:
	movl	$1, 432(%r12)
	movq	96(%rdi), %rax
	testq	%rax, %rax
	je	.L19
	movq	%r12, %rsi
	call	*%rax
.L19:
	movl	$-1, %eax
	lock xaddl	%eax, 472(%r12)
	subl	$1, %eax
	testl	%eax, %eax
	je	.L40
	jle	.L20
.L11:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
.L20:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	SSL_SESSION_free.part.0
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	cmpq	%rdx, %rcx
	je	.L41
	movq	%rdx, 64(%rdi)
	movq	%rax, 536(%rdx)
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L39:
	movq	%rax, 56(%rdi)
	movq	%rcx, 528(%rax)
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L41:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 56(%rdi)
	jmp	.L17
	.cfi_endproc
.LFE1072:
	.size	timeout_cb, .-timeout_cb
	.p2align 4
	.globl	SSL_get_session
	.type	SSL_get_session, @function
SSL_get_session:
.LFB1025:
	.cfi_startproc
	endbr64
	movq	1296(%rdi), %rax
	ret
	.cfi_endproc
.LFE1025:
	.size	SSL_get_session, .-SSL_get_session
	.p2align 4
	.globl	SSL_get1_session
	.type	SSL_get1_session, @function
SSL_get1_session:
.LFB1026:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	6216(%rdi), %rdi
	call	CRYPTO_THREAD_read_lock@PLT
	movq	1296(%rbx), %r12
	testq	%r12, %r12
	je	.L44
	lock addl	$1, 472(%r12)
.L44:
	movq	6216(%rbx), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1026:
	.size	SSL_get1_session, .-SSL_get1_session
	.p2align 4
	.globl	SSL_SESSION_set_ex_data
	.type	SSL_SESSION_set_ex_data, @function
SSL_SESSION_set_ex_data:
.LFB1027:
	.cfi_startproc
	endbr64
	addq	$520, %rdi
	jmp	CRYPTO_set_ex_data@PLT
	.cfi_endproc
.LFE1027:
	.size	SSL_SESSION_set_ex_data, .-SSL_SESSION_set_ex_data
	.p2align 4
	.globl	SSL_SESSION_get_ex_data
	.type	SSL_SESSION_get_ex_data, @function
SSL_SESSION_get_ex_data:
.LFB1028:
	.cfi_startproc
	endbr64
	addq	$520, %rdi
	jmp	CRYPTO_get_ex_data@PLT
	.cfi_endproc
.LFE1028:
	.size	SSL_SESSION_get_ex_data, .-SSL_SESSION_get_ex_data
	.p2align 4
	.globl	SSL_SESSION_new
	.type	SSL_SESSION_new, @function
SSL_SESSION_new:
.LFB1029:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movl	$2097152, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	xorl	%r12d, %r12d
	subq	$8, %rsp
	call	OPENSSL_init_ssl@PLT
	testl	%eax, %eax
	je	.L51
	movl	$72, %edx
	leaq	.LC0(%rip), %rsi
	movl	$648, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L58
	movq	$1, 464(%rax)
	xorl	%edi, %edi
	movl	$1, 472(%rax)
	mfence
	movq	$304, 480(%rax)
	call	time@PLT
	movq	%rax, 488(%r12)
	call	CRYPTO_THREAD_lock_new@PLT
	movq	%rax, 640(%r12)
	testq	%rax, %rax
	je	.L59
	leaq	520(%r12), %rdx
	movq	%r12, %rsi
	movl	$2, %edi
	call	CRYPTO_new_ex_data@PLT
	testl	%eax, %eax
	je	.L60
.L51:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	movl	$74, %r8d
	movl	$65, %edx
	movl	$189, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L60:
	movq	640(%r12), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movl	$91, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L59:
	movl	$84, %r8d
	movl	$65, %edx
	movl	$189, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movl	$85, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L51
	.cfi_endproc
.LFE1029:
	.size	SSL_SESSION_new, .-SSL_SESSION_new
	.p2align 4
	.globl	ssl_session_dup
	.type	ssl_session_dup, @function
ssl_session_dup:
.LFB1031:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$110, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	leaq	.LC0(%rip), %rsi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$648, %edi
	subq	$8, %rsp
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L62
	movq	(%rbx), %rax
	leaq	8(%r12), %rdi
	movq	%r12, %rcx
	movq	%rbx, %rsi
	andq	$-8, %rdi
	pxor	%xmm0, %xmm0
	leaq	520(%r12), %r15
	movq	%rax, (%r12)
	subq	%rdi, %rcx
	movq	640(%rbx), %rax
	leaq	472(%r12), %r14
	subq	%rcx, %rsi
	addl	$648, %ecx
	movq	%rax, 640(%r12)
	shrl	$3, %ecx
	rep movsq
	movq	$0, 584(%r12)
	movq	$0, 456(%r12)
	movq	$0, 440(%r12)
	movq	$0, 520(%r12)
	movups	%xmm0, 416(%r12)
	movups	%xmm0, 544(%r12)
	movups	%xmm0, 608(%r12)
	movups	%xmm0, 528(%r12)
	movl	$1, 472(%r12)
	mfence
	call	CRYPTO_THREAD_lock_new@PLT
	movq	%rax, 640(%r12)
	testq	%rax, %rax
	je	.L63
	movq	%r15, %rdx
	movq	%r12, %rsi
	movl	$2, %edi
	call	CRYPTO_new_ex_data@PLT
	testl	%eax, %eax
	je	.L63
	movq	440(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L64
	call	X509_up_ref@PLT
	testl	%eax, %eax
	je	.L63
	movq	440(%rbx), %rax
	movq	%rax, 440(%r12)
.L64:
	movq	456(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L68
	call	X509_chain_up_ref@PLT
	movq	%rax, 456(%r12)
	testq	%rax, %rax
	je	.L63
.L68:
	movq	416(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L67
	movl	$161, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, 416(%r12)
	testq	%rax, %rax
	je	.L63
.L67:
	movq	424(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L70
	movl	$167, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, 424(%r12)
	testq	%rax, %rax
	je	.L63
.L70:
	leaq	520(%rbx), %rdx
	movq	%r15, %rsi
	movl	$2, %edi
	call	CRYPTO_dup_ex_data@PLT
	testl	%eax, %eax
	je	.L63
	movq	544(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L75
	movl	$180, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, 544(%r12)
	testq	%rax, %rax
	je	.L63
.L75:
	testl	%r13d, %r13d
	je	.L74
	movq	552(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L74
	movq	560(%rbx), %rsi
	movl	$188, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_memdup@PLT
	movq	%rax, 552(%r12)
	testq	%rax, %rax
	jne	.L76
	.p2align 4,,10
	.p2align 3
.L63:
	movl	$221, %r8d
	movl	$65, %edx
	movl	$348, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	lock xaddl	%eax, (%r14)
	subl	$1, %eax
	testl	%eax, %eax
	je	.L132
	jle	.L83
	xorl	%r12d, %r12d
.L61:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	movups	%xmm0, 560(%r12)
.L76:
	movq	584(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L80
	movq	592(%rbx), %rsi
	movl	$197, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_memdup@PLT
	movq	%rax, 584(%r12)
	testq	%rax, %rax
	je	.L63
.L80:
	movq	608(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L79
	movl	$205, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, 608(%r12)
	testq	%rax, %rax
	je	.L63
.L79:
	movq	616(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L61
	movq	624(%rbx), %rsi
	movl	$214, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_memdup@PLT
	movq	%rax, 616(%r12)
	testq	%rax, %rax
	jne	.L61
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L132:
.L83:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	SSL_SESSION_free.part.0
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L62:
	movl	$221, %r8d
	movl	$65, %edx
	movl	$348, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L61
	.cfi_endproc
.LFE1031:
	.size	ssl_session_dup, .-ssl_session_dup
	.p2align 4
	.globl	SSL_SESSION_dup
	.type	SSL_SESSION_dup, @function
SSL_SESSION_dup:
.LFB1030:
	.cfi_startproc
	endbr64
	movl	$1, %esi
	jmp	ssl_session_dup
	.cfi_endproc
.LFE1030:
	.size	SSL_SESSION_dup, .-SSL_SESSION_dup
	.p2align 4
	.globl	SSL_SESSION_get_id
	.type	SSL_SESSION_get_id, @function
SSL_SESSION_get_id:
.LFB1032:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L135
	movq	336(%rdi), %rax
	movl	%eax, (%rsi)
.L135:
	leaq	344(%rdi), %rax
	ret
	.cfi_endproc
.LFE1032:
	.size	SSL_SESSION_get_id, .-SSL_SESSION_get_id
	.p2align 4
	.globl	SSL_SESSION_get0_id_context
	.type	SSL_SESSION_get0_id_context, @function
SSL_SESSION_get0_id_context:
.LFB1033:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L140
	movq	376(%rdi), %rax
	movl	%eax, (%rsi)
.L140:
	leaq	384(%rdi), %rax
	ret
	.cfi_endproc
.LFE1033:
	.size	SSL_SESSION_get0_id_context, .-SSL_SESSION_get0_id_context
	.p2align 4
	.globl	SSL_SESSION_get_compress_id
	.type	SSL_SESSION_get_compress_id, @function
SSL_SESSION_get_compress_id:
.LFB1034:
	.cfi_startproc
	endbr64
	movl	496(%rdi), %eax
	ret
	.cfi_endproc
.LFE1034:
	.size	SSL_SESSION_get_compress_id, .-SSL_SESSION_get_compress_id
	.p2align 4
	.globl	ssl_generate_session_id
	.type	ssl_generate_session_id, @function
ssl_generate_session_id:
.LFB1036:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rdi), %eax
	cmpl	$65277, %eax
	je	.L146
	jg	.L147
	cmpl	$256, %eax
	je	.L146
	subl	$768, %eax
	cmpl	$4, %eax
	jbe	.L146
.L148:
	movl	$297, %r9d
	leaq	.LC0(%rip), %r8
	movl	$259, %ecx
.L172:
	movl	$547, %edx
	movl	$80, %esi
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	ossl_statem_fatal@PLT
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L147:
	cmpl	$65279, %eax
	jne	.L148
.L146:
	movl	1664(%r12), %r13d
	movq	$32, 336(%rbx)
	testl	%r13d, %r13d
	je	.L173
	movq	$0, 336(%rbx)
	movl	$1, %r13d
.L145:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L174
	addq	$40, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L173:
	.cfi_restore_state
	movq	6216(%r12), %rdi
	call	CRYPTO_THREAD_read_lock@PLT
	movq	1904(%r12), %rax
	movq	920(%rax), %rdi
	call	CRYPTO_THREAD_read_lock@PLT
	movq	1328(%r12), %r14
	movq	1904(%r12), %rax
	testq	%r14, %r14
	je	.L152
.L153:
	movq	920(%rax), %rdi
	leaq	344(%rbx), %r15
	call	CRYPTO_THREAD_unlock@PLT
	movq	6216(%r12), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movq	336(%rbx), %rdx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rdx, -72(%rbp)
	call	memset@PLT
	movq	-72(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%edx, -60(%rbp)
	leaq	-60(%rbp), %rdx
	call	*%r14
	testl	%eax, %eax
	je	.L175
	movl	-60(%rbp), %edx
	testl	%edx, %edx
	je	.L156
	movl	%edx, %eax
	cmpq	336(%rbx), %rax
	jbe	.L157
.L156:
	movl	$346, %r9d
	leaq	.LC0(%rip), %r8
	movl	$303, %ecx
	movq	%r12, %rdi
	movl	$547, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L157:
	movq	%rax, 336(%rbx)
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	SSL_has_matching_session_id@PLT
	testl	%eax, %eax
	jne	.L176
	movl	$1, %r13d
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L152:
	movq	408(%rax), %r14
	leaq	def_generate_session_id(%rip), %rdx
	testq	%r14, %r14
	cmove	%rdx, %r14
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L175:
	movl	$336, %r9d
	leaq	.LC0(%rip), %r8
	movl	$301, %ecx
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L176:
	movl	$354, %r9d
	leaq	.LC0(%rip), %r8
	movl	$302, %ecx
	movq	%r12, %rdi
	movl	$547, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L145
.L174:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1036:
	.size	ssl_generate_session_id, .-ssl_generate_session_id
	.p2align 4
	.globl	ssl_get_new_session
	.type	ssl_get_new_session, @function
ssl_get_new_session:
.LFB1037:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$24, %rsp
	call	SSL_SESSION_new
	testq	%rax, %rax
	je	.L206
	movq	%rax, %r13
	movq	1904(%r12), %rax
	movq	80(%rax), %rax
	testq	%rax, %rax
	je	.L207
	movq	%rax, 480(%r13)
.L181:
	movq	1296(%r12), %rdi
	testq	%rdi, %rdi
	je	.L183
	movl	$-1, %eax
	lock xaddl	%eax, 472(%rdi)
	subl	$1, %eax
	testl	%eax, %eax
	je	.L208
	jle	.L185
.L183:
	movq	$0, 1296(%r12)
	testl	%ebx, %ebx
	je	.L187
.L211:
	movq	8(%r12), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L188
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L188
	cmpl	$65536, %eax
	jne	.L187
.L188:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	ssl_generate_session_id
	testl	%eax, %eax
	jne	.L189
	movl	$-1, %edx
	lock xaddl	%edx, 472(%r13)
	subl	$1, %edx
	testl	%edx, %edx
	je	.L209
	jg	.L205
	.p2align 4,,10
	.p2align 3
.L191:
	movq	%r13, %rdi
	movl	%eax, -36(%rbp)
	call	SSL_SESSION_free.part.0
	movl	-36(%rbp), %eax
.L177:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L187:
	.cfi_restore_state
	movq	$0, 336(%r13)
.L189:
	movq	1256(%r12), %rdx
	cmpq	$32, %rdx
	ja	.L210
	leaq	384(%r13), %rdi
	leaq	1264(%r12), %rsi
	call	memcpy@PLT
	movq	1256(%r12), %rax
	movq	%rax, 376(%r13)
	movl	(%r12), %eax
	movq	%r13, 1296(%r12)
	movl	%eax, 0(%r13)
	movq	168(%r12), %rax
	movq	$0, 464(%r13)
	movq	(%rax), %rdx
	movl	$1, %eax
	andb	$2, %dh
	je	.L177
	orl	$1, 632(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	.cfi_restore_state
.L185:
	call	SSL_SESSION_free.part.0
	movq	$0, 1296(%r12)
	testl	%ebx, %ebx
	je	.L187
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L207:
	movq	%r12, %rdi
	call	SSL_get_default_timeout@PLT
	movq	%rax, 480(%r13)
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L210:
	movl	$401, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$181, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movl	$-1, %eax
	lock xaddl	%eax, 472(%r13)
	subl	$1, %eax
	testl	%eax, %eax
	je	.L212
	jle	.L194
.L205:
	xorl	%eax, %eax
.L213:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L212:
	.cfi_restore_state
.L194:
	movq	%r13, %rdi
	call	SSL_SESSION_free.part.0
	xorl	%eax, %eax
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L206:
	movl	$369, %r9d
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
	movq	%r12, %rdi
	movl	$181, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L209:
	jmp	.L191
	.cfi_endproc
.LFE1037:
	.size	ssl_get_new_session, .-ssl_get_new_session
	.p2align 4
	.globl	SSL_CTX_add_session
	.type	SSL_CTX_add_session, @function
SSL_CTX_add_session:
.LFB1040:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	lock addl	$1, 472(%rsi)
	movq	920(%rdi), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	40(%rbx), %rdi
	movq	%r12, %rsi
	call	OPENSSL_LH_insert@PLT
	testq	%rax, %rax
	je	.L282
	movq	%rax, %rdi
	cmpq	%rax, %r12
	jne	.L288
	testq	%rax, %rax
	je	.L282
.L223:
	movl	$-1, %eax
	lock xaddl	%eax, 472(%rdi)
	subl	$1, %eax
	testl	%eax, %eax
	je	.L289
	movl	$0, %r12d
	jle	.L234
.L235:
	movq	920(%rbx), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L288:
	.cfi_restore_state
	movq	536(%rax), %rax
	leaq	56(%rbx), %r13
	testq	%rax, %rax
	je	.L216
	movq	528(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L216
	leaq	64(%rbx), %rcx
	cmpq	%rcx, %rax
	je	.L290
	cmpq	%r13, %rdx
	je	.L291
	movq	%rdx, 528(%rax)
	movq	%rax, 536(%rdx)
.L219:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 528(%rdi)
.L216:
	movl	$-1, %eax
	lock xaddl	%eax, 472(%rdi)
	subl	$1, %eax
	testl	%eax, %eax
	je	.L292
	jle	.L221
.L222:
	movq	536(%r12), %rax
	testq	%rax, %rax
	je	.L227
	movq	528(%r12), %rdx
	testq	%rdx, %rdx
	je	.L227
	leaq	64(%rbx), %rcx
	cmpq	%rcx, %rax
	je	.L293
	cmpq	%r13, %rdx
	je	.L294
	movq	%rdx, 528(%rax)
	movq	528(%r12), %rdx
	movq	%rax, 536(%rdx)
.L230:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 528(%r12)
.L227:
	movq	56(%rbx), %rax
	testq	%rax, %rax
	je	.L295
	movq	%rax, 536(%r12)
	movq	%r12, 528(%rax)
	movq	%r13, 528(%r12)
	movq	%r12, 56(%rbx)
.L233:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$43, %esi
	movq	%rbx, %rdi
	call	SSL_CTX_ctrl@PLT
	leaq	64(%rbx), %r14
	testq	%rax, %rax
	jle	.L287
.L251:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$20, %esi
	movq	%rbx, %rdi
	call	SSL_CTX_ctrl@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$43, %esi
	movq	%rbx, %rdi
	movq	%rax, %r12
	call	SSL_CTX_ctrl@PLT
	cmpq	%rax, %r12
	jle	.L287
	movq	64(%rbx), %r12
	testq	%r12, %r12
	je	.L287
	cmpq	$0, 336(%r12)
	je	.L287
	movq	40(%rbx), %rdi
	movq	%r12, %rsi
	call	OPENSSL_LH_retrieve@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L238
	movq	40(%rbx), %rdi
	call	OPENSSL_LH_delete@PLT
	movq	%rax, %r15
	movq	536(%rax), %rax
	testq	%rax, %rax
	je	.L239
	movq	528(%r15), %rdx
	testq	%rdx, %rdx
	je	.L239
	cmpq	%r14, %rax
	je	.L296
	cmpq	%r13, %rdx
	je	.L297
	movq	%rdx, 528(%rax)
	movq	%rax, 536(%rdx)
.L242:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 528(%r15)
.L239:
	movl	$1, 432(%r12)
	movq	96(%rbx), %rax
	testq	%rax, %rax
	je	.L249
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	*%rax
.L249:
	movl	$-1, %eax
	lock xaddl	%eax, 472(%r15)
	subl	$1, %eax
	testl	%eax, %eax
	je	.L298
	jle	.L245
.L246:
	lock addl	$1, 144(%rbx)
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L292:
.L221:
	call	SSL_SESSION_free.part.0
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L290:
	cmpq	%r13, %rdx
	je	.L299
	movq	%rdx, 64(%rbx)
	movq	%rax, 536(%rdx)
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L238:
	movl	$1, 432(%r12)
	movq	96(%rbx), %rax
	testq	%rax, %rax
	je	.L287
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L287:
	movl	$1, %r12d
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L289:
.L234:
	call	SSL_SESSION_free.part.0
	xorl	%r12d, %r12d
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L295:
	movq	%r12, %xmm0
	leaq	64(%rbx), %rax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 56(%rbx)
	movq	%r13, 528(%r12)
	movq	%rax, 536(%r12)
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L282:
	movq	40(%rbx), %rdi
	movq	%r12, %rsi
	call	OPENSSL_LH_retrieve@PLT
	testq	%rax, %rax
	jne	.L256
	testq	%r12, %r12
	jne	.L254
.L256:
	leaq	56(%rbx), %r13
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L299:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 56(%rbx)
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L291:
	movq	%rax, 56(%rbx)
	movq	%r13, 528(%rax)
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L293:
	cmpq	%r13, %rdx
	je	.L300
	movq	%rdx, 64(%rbx)
	movq	%rax, 536(%rdx)
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L294:
	movq	%rax, 56(%rbx)
	movq	%r13, 528(%rax)
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L300:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 56(%rbx)
	jmp	.L230
.L296:
	cmpq	%r13, %rdx
	je	.L301
	movq	%rdx, 64(%rbx)
	movq	%r14, 536(%rdx)
	jmp	.L242
.L298:
.L245:
	movq	%r15, %rdi
	call	SSL_SESSION_free.part.0
	jmp	.L246
.L297:
	movq	%rax, 56(%rbx)
	movq	%r13, 528(%rax)
	jmp	.L242
.L301:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 56(%rbx)
	jmp	.L242
.L254:
	movq	%r12, %rdi
	jmp	.L223
	.cfi_endproc
.LFE1040:
	.size	SSL_CTX_add_session, .-SSL_CTX_add_session
	.p2align 4
	.globl	lookup_sess_in_cache
	.type	lookup_sess_in_cache, @function
lookup_sess_in_cache:
.LFB1038:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$680, %rsp
	movq	1904(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$1, 73(%r12)
	jne	.L303
	movl	(%rdi), %eax
	movl	%eax, -704(%rbp)
	cmpq	$32, %rdx
	ja	.L315
	movl	$304, %ecx
	leaq	-360(%rbp), %rdi
	leaq	-704(%rbp), %r15
	call	__memcpy_chk@PLT
	movq	920(%r12), %rdi
	movq	%r13, -368(%rbp)
	call	CRYPTO_THREAD_read_lock@PLT
	movq	1904(%rbx), %rax
	movq	%r15, %rsi
	movq	40(%rax), %rdi
	call	OPENSSL_LH_retrieve@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L306
	lock addl	$1, 472(%rax)
	movq	1904(%rbx), %rax
	movq	920(%rax), %rdi
	call	CRYPTO_THREAD_unlock@PLT
.L302:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L316
	addq	$680, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L306:
	.cfi_restore_state
	movq	1904(%rbx), %rax
	movq	920(%rax), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movq	1904(%rbx), %rax
	lock addl	$1, 136(%rax)
	movq	1904(%rbx), %r12
	.p2align 4,,10
	.p2align 3
.L303:
	movq	104(%r12), %rax
	xorl	%r12d, %r12d
	testq	%rax, %rax
	je	.L302
	leaq	-708(%rbp), %rcx
	movl	%r13d, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movl	$1, -708(%rbp)
	call	*%rax
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L302
	movq	1904(%rbx), %rax
	lock addl	$1, 152(%rax)
	movl	-708(%rbp), %eax
	testl	%eax, %eax
	jne	.L317
.L309:
	movq	1904(%rbx), %rdi
	testb	$2, 73(%rdi)
	jne	.L302
	movq	%r12, %rsi
	call	SSL_CTX_add_session
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L315:
	xorl	%r12d, %r12d
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L317:
	lock addl	$1, 472(%r12)
	jmp	.L309
.L316:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1038:
	.size	lookup_sess_in_cache, .-lookup_sess_in_cache
	.p2align 4
	.globl	SSL_CTX_remove_session
	.type	SSL_CTX_remove_session, @function
SSL_CTX_remove_session:
.LFB1041:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L344
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$8, %rsp
	cmpq	$0, 336(%rsi)
	jne	.L345
.L343:
	xorl	%eax, %eax
.L318:
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L345:
	.cfi_restore_state
	movq	%rdi, %r13
	movq	920(%rdi), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	40(%r13), %rdi
	movq	%r12, %rsi
	call	OPENSSL_LH_retrieve@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L322
	movq	40(%r13), %rdi
	call	OPENSSL_LH_delete@PLT
	movq	%rax, %r14
	movq	536(%rax), %rax
	testq	%rax, %rax
	je	.L323
	movq	528(%r14), %rdx
	testq	%rdx, %rdx
	je	.L323
	leaq	64(%r13), %rcx
	leaq	56(%r13), %rsi
	cmpq	%rcx, %rax
	je	.L346
	cmpq	%rsi, %rdx
	je	.L347
	movq	%rdx, 528(%rax)
	movq	%rax, 536(%rdx)
.L326:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 528(%r14)
.L323:
	movl	$1, 432(%r12)
	movq	920(%r13), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movq	96(%r13), %rax
	testq	%rax, %rax
	je	.L328
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	*%rax
.L328:
	movl	$-1, %eax
	lock xaddl	%eax, 472(%r14)
	subl	$1, %eax
	testl	%eax, %eax
	je	.L348
	movl	$1, %eax
	jg	.L318
	.p2align 4,,10
	.p2align 3
.L330:
	movq	%r14, %rdi
	call	SSL_SESSION_free.part.0
	movl	$1, %eax
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L344:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L322:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movl	$1, 432(%r12)
	movq	920(%r13), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movq	96(%r13), %rax
	testq	%rax, %rax
	je	.L343
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	*%rax
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L346:
	cmpq	%rsi, %rdx
	je	.L349
	movq	%rdx, 64(%r13)
	movq	%rax, 536(%rdx)
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L347:
	movq	%rax, 56(%r13)
	movq	%rdx, 528(%rax)
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L348:
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L349:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 56(%r13)
	jmp	.L326
	.cfi_endproc
.LFE1041:
	.size	SSL_CTX_remove_session, .-SSL_CTX_remove_session
	.p2align 4
	.globl	SSL_SESSION_free
	.type	SSL_SESSION_free, @function
SSL_SESSION_free:
.LFB1043:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L350
	movl	$-1, %eax
	lock xaddl	%eax, 472(%rdi)
	subl	$1, %eax
	testl	%eax, %eax
	je	.L356
	jle	.L354
.L350:
	ret
	.p2align 4,,10
	.p2align 3
.L356:
.L354:
	jmp	SSL_SESSION_free.part.0
	.cfi_endproc
.LFE1043:
	.size	SSL_SESSION_free, .-SSL_SESSION_free
	.p2align 4
	.globl	ssl_get_prev_session
	.type	ssl_get_prev_session, @function
ssl_get_prev_session:
.LFB1039:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	$0, -64(%rbp)
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L358
	movl	(%rax), %eax
	cmpl	$65536, %eax
	je	.L358
	cmpl	$771, %eax
	jg	.L443
.L358:
	leaq	-64(%rbp), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	tls_get_ticket_from_client@PLT
	cmpl	$1, %eax
	jle	.L444
	subl	$2, %eax
	cmpl	$1, %eax
	ja	.L441
	movq	40(%r12), %rdx
	testq	%rdx, %rdx
	je	.L441
	leaq	48(%r12), %rsi
	movq	%rbx, %rdi
	movl	$1, %r14d
	call	lookup_sess_in_cache
	movq	%rax, -64(%rbp)
	movq	%rax, %r12
.L362:
	testq	%r12, %r12
	je	.L369
.L449:
	movl	(%rbx), %eax
	cmpl	%eax, (%r12)
	je	.L445
.L370:
	xorl	%r13d, %r13d
.L395:
	movl	$-1, %eax
	lock xaddl	%eax, 472(%r12)
	subl	$1, %eax
	testl	%eax, %eax
	je	.L446
	jle	.L390
.L391:
	movq	8(%rbx), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L392
	movl	(%rax), %eax
	cmpl	$65536, %eax
	je	.L392
	cmpl	$771, %eax
	jle	.L392
	movq	$0, 1296(%rbx)
	.p2align 4,,10
	.p2align 3
.L392:
	testl	%r14d, %r14d
	jne	.L357
	movl	$1, 1664(%rbx)
.L357:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L447
	addq	$40, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L444:
	.cfi_restore_state
	testl	%eax, %eax
	jns	.L448
.L441:
	movq	-64(%rbp), %r12
	xorl	%r14d, %r14d
	testq	%r12, %r12
	jne	.L449
.L369:
	xorl	%r13d, %r13d
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L445:
	movq	376(%r12), %r15
	cmpq	1256(%rbx), %r15
	jne	.L370
	leaq	1264(%rbx), %rsi
	leaq	384(%r12), %rdi
	movq	%r15, %rdx
	call	memcmp@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.L370
	testb	$1, 1376(%rbx)
	je	.L371
	testq	%r15, %r15
	je	.L450
.L371:
	xorl	%edi, %edi
	movq	480(%r12), %r15
	call	time@PLT
	movq	-64(%rbp), %r12
	subq	488(%r12), %rax
	cmpq	%rax, %r15
	jl	.L451
	movq	168(%rbx), %rax
	movq	(%rax), %rax
	andl	$512, %eax
	testb	$1, 632(%r12)
	jne	.L452
	testq	%rax, %rax
	jne	.L370
.L381:
	movq	8(%rbx), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L382
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L382
	cmpl	$65536, %eax
	jne	.L383
.L382:
	movq	1296(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L385
	movl	$-1, %eax
	lock xaddl	%eax, 472(%rdi)
	subl	$1, %eax
	testl	%eax, %eax
	je	.L453
	jle	.L387
.L442:
	movq	-64(%rbp), %r12
.L385:
	movq	%r12, 1296(%rbx)
.L383:
	movq	1904(%rbx), %rax
	lock addl	$1, 148(%rax)
	movl	$1, %r13d
	movq	1296(%rbx), %rax
	movq	464(%rax), %rax
	movq	%rax, 1456(%rbx)
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L448:
	movl	$68, %ecx
	movl	$217, %edx
	movq	%rbx, %rdi
	xorl	%r14d, %r14d
	movl	$530, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	movl	$-1, %r13d
	call	ossl_statem_fatal@PLT
.L367:
	movq	-64(%rbp), %r12
	testq	%r12, %r12
	je	.L357
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L443:
	movl	$1, 1664(%rdi)
	movq	648(%rsi), %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$128, %edx
	movl	$18, %esi
	call	tls_parse_extension@PLT
	testl	%eax, %eax
	je	.L361
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$128, %edx
	movl	$25, %esi
	movq	648(%r12), %rcx
	movq	%rbx, %rdi
	call	tls_parse_extension@PLT
	testl	%eax, %eax
	je	.L361
	movq	1296(%rbx), %r12
	xorl	%r14d, %r14d
	movq	%r12, -64(%rbp)
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L446:
.L390:
	movq	%r12, %rdi
	call	SSL_SESSION_free.part.0
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L452:
	testq	%rax, %rax
	jne	.L381
	movl	$596, %r9d
	leaq	.LC0(%rip), %r8
	movl	$104, %ecx
	movq	%rbx, %rdi
	movl	$217, %edx
	movl	$47, %esi
	movl	$-1, %r13d
	call	ossl_statem_fatal@PLT
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L450:
	movl	$577, %r9d
	leaq	.LC0(%rip), %r8
	movl	$277, %ecx
	movq	%rbx, %rdi
	movl	$217, %edx
	movl	$80, %esi
	movl	$-1, %r13d
	call	ossl_statem_fatal@PLT
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L451:
	movq	1904(%rbx), %rax
	lock addl	$1, 140(%rax)
	testl	%r14d, %r14d
	je	.L397
	movq	-64(%rbp), %r12
	testq	%r12, %r12
	je	.L369
	cmpq	$0, 336(%r12)
	je	.L370
	movq	1904(%rbx), %r15
	movq	920(%r15), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	40(%r15), %rdi
	movq	%r12, %rsi
	call	OPENSSL_LH_retrieve@PLT
	testq	%rax, %rax
	je	.L373
	movq	40(%r15), %rdi
	movq	%rax, %rsi
	call	OPENSSL_LH_delete@PLT
	movq	536(%rax), %rdx
	movq	%rax, %r8
	testq	%rdx, %rdx
	je	.L374
	movq	528(%rax), %rax
	testq	%rax, %rax
	je	.L374
	leaq	64(%r15), %rsi
	leaq	56(%r15), %rcx
	cmpq	%rsi, %rdx
	je	.L454
	cmpq	%rcx, %rax
	je	.L455
	movq	%rax, 528(%rdx)
	movq	%rdx, 536(%rax)
.L377:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 528(%r8)
.L374:
	movq	920(%r15), %rdi
	movq	%r8, -72(%rbp)
	movl	$1, 432(%r12)
	call	CRYPTO_THREAD_unlock@PLT
	movq	96(%r15), %rax
	movq	-72(%rbp), %r8
	testq	%rax, %rax
	je	.L379
	movq	%r8, -72(%rbp)
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	*%rax
	movq	-72(%rbp), %r8
.L379:
	movq	%r8, %rdi
	call	SSL_SESSION_free
	jmp	.L367
.L453:
.L387:
	call	SSL_SESSION_free.part.0
	jmp	.L442
.L397:
	xorl	%r13d, %r13d
	jmp	.L367
.L361:
	movl	$-1, %r13d
	jmp	.L357
.L373:
	movl	$1, 432(%r12)
	movq	920(%r15), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movq	96(%r15), %rax
	testq	%rax, %rax
	je	.L367
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	*%rax
	jmp	.L367
.L447:
	call	__stack_chk_fail@PLT
.L454:
	cmpq	%rcx, %rax
	je	.L456
	movq	%rax, 64(%r15)
	movq	%rdx, 536(%rax)
	jmp	.L377
.L455:
	movq	%rdx, 56(%r15)
	movq	%rax, 528(%rdx)
	jmp	.L377
.L456:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 56(%r15)
	jmp	.L377
	.cfi_endproc
.LFE1039:
	.size	ssl_get_prev_session, .-ssl_get_prev_session
	.p2align 4
	.globl	SSL_SESSION_up_ref
	.type	SSL_SESSION_up_ref, @function
SSL_SESSION_up_ref:
.LFB1044:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	lock xaddl	%eax, 472(%rdi)
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1044:
	.size	SSL_SESSION_up_ref, .-SSL_SESSION_up_ref
	.p2align 4
	.globl	SSL_set_session
	.type	SSL_set_session, @function
SSL_set_session:
.LFB1045:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	$0, 1296(%rdi)
	je	.L460
	testb	$1, 68(%rdi)
	je	.L498
.L460:
	movq	1440(%rbx), %rax
	movq	(%rax), %rsi
	cmpq	8(%rbx), %rsi
	je	.L474
	movq	%rbx, %rdi
	call	SSL_set_ssl_method@PLT
	testl	%eax, %eax
	je	.L458
.L474:
	testq	%r12, %r12
	je	.L472
	lock addl	$1, 472(%r12)
	movq	464(%r12), %rax
	movq	%rax, 1456(%rbx)
.L472:
	movq	1296(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L476
	movl	$-1, %eax
	lock xaddl	%eax, 472(%rdi)
	subl	$1, %eax
	testl	%eax, %eax
	je	.L499
	jle	.L478
.L476:
	movq	%r12, 1296(%rbx)
	movl	$1, %eax
.L458:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L498:
	.cfi_restore_state
	call	SSL_in_init@PLT
	testl	%eax, %eax
	jne	.L460
	movq	%rbx, %rdi
	call	SSL_in_before@PLT
	testl	%eax, %eax
	jne	.L460
	movq	1296(%rbx), %r13
	testq	%r13, %r13
	je	.L460
	cmpq	$0, 336(%r13)
	je	.L460
	movq	1904(%rbx), %r14
	movq	920(%r14), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	40(%r14), %rdi
	movq	%r13, %rsi
	call	OPENSSL_LH_retrieve@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L462
	movq	40(%r14), %rdi
	call	OPENSSL_LH_delete@PLT
	movq	%rax, %r15
	movq	536(%rax), %rax
	testq	%rax, %rax
	je	.L463
	movq	528(%r15), %rdx
	testq	%rdx, %rdx
	je	.L463
	leaq	64(%r14), %rcx
	leaq	56(%r14), %rsi
	cmpq	%rcx, %rax
	je	.L500
	cmpq	%rsi, %rdx
	je	.L501
	movq	%rdx, 528(%rax)
	movq	%rax, 536(%rdx)
.L466:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 528(%r15)
.L463:
	movl	$1, 432(%r13)
	movq	920(%r14), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movq	96(%r14), %rax
	testq	%rax, %rax
	je	.L481
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	*%rax
.L481:
	movl	$-1, %eax
	lock xaddl	%eax, 472(%r15)
	subl	$1, %eax
	testl	%eax, %eax
	je	.L502
	jg	.L460
.L469:
	movq	%r15, %rdi
	call	SSL_SESSION_free.part.0
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L499:
.L478:
	call	SSL_SESSION_free.part.0
	movq	%r12, 1296(%rbx)
	movl	$1, %eax
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L462:
	movl	$1, 432(%r13)
	movq	920(%r14), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movq	96(%r14), %rax
	testq	%rax, %rax
	je	.L460
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	*%rax
	jmp	.L460
.L500:
	cmpq	%rsi, %rdx
	je	.L503
	movq	%rdx, 64(%r14)
	movq	%rcx, 536(%rdx)
	jmp	.L466
.L502:
	jmp	.L469
.L501:
	movq	%rax, 56(%r14)
	movq	%rdx, 528(%rax)
	jmp	.L466
.L503:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 56(%r14)
	jmp	.L466
	.cfi_endproc
.LFE1045:
	.size	SSL_set_session, .-SSL_set_session
	.p2align 4
	.globl	SSL_SESSION_set1_id
	.type	SSL_SESSION_set1_id, @function
SSL_SESSION_set1_id:
.LFB1046:
	.cfi_startproc
	endbr64
	cmpl	$32, %edx
	ja	.L525
	movl	%edx, %ecx
	leaq	344(%rdi), %rax
	movl	$1, %r8d
	movq	%rcx, 336(%rdi)
	cmpq	%rsi, %rax
	je	.L522
	cmpl	$8, %edx
	jnb	.L507
	testb	$4, %dl
	jne	.L526
	testl	%edx, %edx
	je	.L508
	movzbl	(%rsi), %r8d
	andl	$2, %edx
	movb	%r8b, 344(%rdi)
	jne	.L527
.L508:
	movl	$1, %r8d
.L522:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L507:
	movq	(%rsi), %r8
	addq	$352, %rdi
	movq	%r8, -8(%rdi)
	movq	-8(%rsi,%rcx), %r8
	andq	$-8, %rdi
	movq	%r8, -8(%rax,%rcx)
	subq	%rdi, %rax
	addl	%eax, %edx
	subq	%rax, %rsi
	andl	$-8, %edx
	cmpl	$8, %edx
	jb	.L508
	andl	$-8, %edx
	xorl	%eax, %eax
.L511:
	movl	%eax, %ecx
	addl	$8, %eax
	movq	(%rsi,%rcx), %r8
	movq	%r8, (%rdi,%rcx)
	cmpl	%edx, %eax
	jb	.L511
	movl	$1, %r8d
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L525:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$408, %edx
	movl	$423, %esi
	movl	$20, %edi
	movl	$814, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%r8d, %r8d
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L526:
	.cfi_restore 6
	movl	(%rsi), %edx
	movl	%edx, 344(%rdi)
	movl	-4(%rsi,%rcx), %edx
	movl	%edx, -4(%rax,%rcx)
	jmp	.L508
.L527:
	movzwl	-2(%rsi,%rcx), %edx
	movw	%dx, -2(%rax,%rcx)
	jmp	.L508
	.cfi_endproc
.LFE1046:
	.size	SSL_SESSION_set1_id, .-SSL_SESSION_set1_id
	.p2align 4
	.globl	SSL_SESSION_set_timeout
	.type	SSL_SESSION_set_timeout, @function
SSL_SESSION_set_timeout:
.LFB1047:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L530
	movq	%rsi, 480(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L530:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1047:
	.size	SSL_SESSION_set_timeout, .-SSL_SESSION_set_timeout
	.p2align 4
	.globl	SSL_SESSION_get_timeout
	.type	SSL_SESSION_get_timeout, @function
SSL_SESSION_get_timeout:
.LFB1048:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L533
	movq	480(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L533:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1048:
	.size	SSL_SESSION_get_timeout, .-SSL_SESSION_get_timeout
	.p2align 4
	.globl	SSL_SESSION_get_time
	.type	SSL_SESSION_get_time, @function
SSL_SESSION_get_time:
.LFB1049:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L536
	movq	488(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L536:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1049:
	.size	SSL_SESSION_get_time, .-SSL_SESSION_get_time
	.p2align 4
	.globl	SSL_SESSION_set_time
	.type	SSL_SESSION_set_time, @function
SSL_SESSION_set_time:
.LFB1050:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L539
	movq	%rsi, 488(%rdi)
	movq	%rsi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L539:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1050:
	.size	SSL_SESSION_set_time, .-SSL_SESSION_set_time
	.p2align 4
	.globl	SSL_SESSION_get_protocol_version
	.type	SSL_SESSION_get_protocol_version, @function
SSL_SESSION_get_protocol_version:
.LFB1051:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	ret
	.cfi_endproc
.LFE1051:
	.size	SSL_SESSION_get_protocol_version, .-SSL_SESSION_get_protocol_version
	.p2align 4
	.globl	SSL_SESSION_set_protocol_version
	.type	SSL_SESSION_set_protocol_version, @function
SSL_SESSION_set_protocol_version:
.LFB1052:
	.cfi_startproc
	endbr64
	movl	%esi, (%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1052:
	.size	SSL_SESSION_set_protocol_version, .-SSL_SESSION_set_protocol_version
	.p2align 4
	.globl	SSL_SESSION_get0_cipher
	.type	SSL_SESSION_get0_cipher, @function
SSL_SESSION_get0_cipher:
.LFB1053:
	.cfi_startproc
	endbr64
	movq	504(%rdi), %rax
	ret
	.cfi_endproc
.LFE1053:
	.size	SSL_SESSION_get0_cipher, .-SSL_SESSION_get0_cipher
	.p2align 4
	.globl	SSL_SESSION_set_cipher
	.type	SSL_SESSION_set_cipher, @function
SSL_SESSION_set_cipher:
.LFB1054:
	.cfi_startproc
	endbr64
	movq	%rsi, 504(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1054:
	.size	SSL_SESSION_set_cipher, .-SSL_SESSION_set_cipher
	.p2align 4
	.globl	SSL_SESSION_get0_hostname
	.type	SSL_SESSION_get0_hostname, @function
SSL_SESSION_get0_hostname:
.LFB1055:
	.cfi_startproc
	endbr64
	movq	544(%rdi), %rax
	ret
	.cfi_endproc
.LFE1055:
	.size	SSL_SESSION_get0_hostname, .-SSL_SESSION_get0_hostname
	.p2align 4
	.globl	SSL_SESSION_set1_hostname
	.type	SSL_SESSION_set1_hostname, @function
SSL_SESSION_set1_hostname:
.LFB1056:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$883, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	leaq	.LC0(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	544(%rdi), %rdi
	call	CRYPTO_free@PLT
	testq	%r12, %r12
	je	.L549
	movq	%r12, %rdi
	movl	$888, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	testq	%rax, %rax
	movq	%rax, 544(%rbx)
	popq	%rbx
	setne	%al
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L549:
	.cfi_restore_state
	movq	$0, 544(%rbx)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1056:
	.size	SSL_SESSION_set1_hostname, .-SSL_SESSION_set1_hostname
	.p2align 4
	.globl	SSL_SESSION_has_ticket
	.type	SSL_SESSION_has_ticket, @function
SSL_SESSION_has_ticket:
.LFB1057:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	$0, 560(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE1057:
	.size	SSL_SESSION_has_ticket, .-SSL_SESSION_has_ticket
	.p2align 4
	.globl	SSL_SESSION_get_ticket_lifetime_hint
	.type	SSL_SESSION_get_ticket_lifetime_hint, @function
SSL_SESSION_get_ticket_lifetime_hint:
.LFB1058:
	.cfi_startproc
	endbr64
	movq	568(%rdi), %rax
	ret
	.cfi_endproc
.LFE1058:
	.size	SSL_SESSION_get_ticket_lifetime_hint, .-SSL_SESSION_get_ticket_lifetime_hint
	.p2align 4
	.globl	SSL_SESSION_get0_ticket
	.type	SSL_SESSION_get0_ticket, @function
SSL_SESSION_get0_ticket:
.LFB1059:
	.cfi_startproc
	endbr64
	movq	560(%rdi), %rax
	movq	%rax, (%rdx)
	testq	%rsi, %rsi
	je	.L552
	movq	552(%rdi), %rax
	movq	%rax, (%rsi)
.L552:
	ret
	.cfi_endproc
.LFE1059:
	.size	SSL_SESSION_get0_ticket, .-SSL_SESSION_get0_ticket
	.p2align 4
	.globl	SSL_SESSION_get_max_early_data
	.type	SSL_SESSION_get_max_early_data, @function
SSL_SESSION_get_max_early_data:
.LFB1060:
	.cfi_startproc
	endbr64
	movl	580(%rdi), %eax
	ret
	.cfi_endproc
.LFE1060:
	.size	SSL_SESSION_get_max_early_data, .-SSL_SESSION_get_max_early_data
	.p2align 4
	.globl	SSL_SESSION_set_max_early_data
	.type	SSL_SESSION_set_max_early_data, @function
SSL_SESSION_set_max_early_data:
.LFB1061:
	.cfi_startproc
	endbr64
	movl	%esi, 580(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1061:
	.size	SSL_SESSION_set_max_early_data, .-SSL_SESSION_set_max_early_data
	.p2align 4
	.globl	SSL_SESSION_get0_alpn_selected
	.type	SSL_SESSION_get0_alpn_selected, @function
SSL_SESSION_get0_alpn_selected:
.LFB1062:
	.cfi_startproc
	endbr64
	movq	584(%rdi), %rax
	movq	%rax, (%rsi)
	movq	592(%rdi), %rax
	movq	%rax, (%rdx)
	ret
	.cfi_endproc
.LFE1062:
	.size	SSL_SESSION_get0_alpn_selected, .-SSL_SESSION_get0_alpn_selected
	.p2align 4
	.globl	SSL_SESSION_set1_alpn_selected
	.type	SSL_SESSION_set1_alpn_selected, @function
SSL_SESSION_set1_alpn_selected:
.LFB1063:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	leaq	.LC0(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	movl	$934, %edx
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	584(%rdi), %rdi
	call	CRYPTO_free@PLT
	testq	%r13, %r13
	je	.L565
	testq	%r12, %r12
	je	.L565
	movl	$940, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	CRYPTO_memdup@PLT
	movq	%rax, 584(%rbx)
	testq	%rax, %rax
	je	.L567
	movq	%r12, 592(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L565:
	.cfi_restore_state
	movq	$0, 584(%rbx)
	movl	$1, %eax
	movq	$0, 592(%rbx)
.L560:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L567:
	.cfi_restore_state
	movq	$0, 592(%rbx)
	jmp	.L560
	.cfi_endproc
.LFE1063:
	.size	SSL_SESSION_set1_alpn_selected, .-SSL_SESSION_set1_alpn_selected
	.p2align 4
	.globl	SSL_SESSION_get0_peer
	.type	SSL_SESSION_get0_peer, @function
SSL_SESSION_get0_peer:
.LFB1064:
	.cfi_startproc
	endbr64
	movq	440(%rdi), %rax
	ret
	.cfi_endproc
.LFE1064:
	.size	SSL_SESSION_get0_peer, .-SSL_SESSION_get0_peer
	.p2align 4
	.globl	SSL_SESSION_set1_id_context
	.type	SSL_SESSION_set1_id_context, @function
SSL_SESSION_set1_id_context:
.LFB1065:
	.cfi_startproc
	endbr64
	cmpl	$32, %edx
	ja	.L590
	movl	%edx, %ecx
	leaq	384(%rdi), %rax
	movl	$1, %r8d
	movq	%rcx, 376(%rdi)
	cmpq	%rsi, %rax
	je	.L587
	cmpl	$8, %edx
	jnb	.L572
	testb	$4, %dl
	jne	.L591
	testl	%edx, %edx
	je	.L573
	movzbl	(%rsi), %r8d
	andl	$2, %edx
	movb	%r8b, 384(%rdi)
	jne	.L592
.L573:
	movl	$1, %r8d
.L587:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L572:
	movq	(%rsi), %r8
	addq	$392, %rdi
	movq	%r8, -8(%rdi)
	movq	-8(%rsi,%rcx), %r8
	andq	$-8, %rdi
	movq	%r8, -8(%rax,%rcx)
	subq	%rdi, %rax
	addl	%eax, %edx
	subq	%rax, %rsi
	andl	$-8, %edx
	cmpl	$8, %edx
	jb	.L573
	andl	$-8, %edx
	xorl	%eax, %eax
.L576:
	movl	%eax, %ecx
	addl	$8, %eax
	movq	(%rsi,%rcx), %r8
	movq	%r8, (%rdi,%rcx)
	cmpl	%edx, %eax
	jb	.L576
	movl	$1, %r8d
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L590:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$273, %edx
	movl	$312, %esi
	movl	$20, %edi
	movl	$959, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%r8d, %r8d
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L591:
	.cfi_restore 6
	movl	(%rsi), %edx
	movl	%edx, 384(%rdi)
	movl	-4(%rsi,%rcx), %edx
	movl	%edx, -4(%rax,%rcx)
	jmp	.L573
.L592:
	movzwl	-2(%rsi,%rcx), %edx
	movw	%dx, -2(%rax,%rcx)
	jmp	.L573
	.cfi_endproc
.LFE1065:
	.size	SSL_SESSION_set1_id_context, .-SSL_SESSION_set1_id_context
	.p2align 4
	.globl	SSL_SESSION_is_resumable
	.type	SSL_SESSION_is_resumable, @function
SSL_SESSION_is_resumable:
.LFB1066:
	.cfi_startproc
	endbr64
	movl	432(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jne	.L593
	cmpq	$0, 336(%rdi)
	movl	$1, %eax
	je	.L597
.L593:
	ret
	.p2align 4,,10
	.p2align 3
.L597:
	xorl	%eax, %eax
	cmpq	$0, 560(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE1066:
	.size	SSL_SESSION_is_resumable, .-SSL_SESSION_is_resumable
	.p2align 4
	.globl	SSL_CTX_set_timeout
	.type	SSL_CTX_set_timeout, @function
SSL_CTX_set_timeout:
.LFB1067:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L600
	movq	80(%rdi), %rax
	movq	%rsi, 80(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L600:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1067:
	.size	SSL_CTX_set_timeout, .-SSL_CTX_set_timeout
	.p2align 4
	.globl	SSL_CTX_get_timeout
	.type	SSL_CTX_get_timeout, @function
SSL_CTX_get_timeout:
.LFB1068:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L603
	movq	80(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L603:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1068:
	.size	SSL_CTX_get_timeout, .-SSL_CTX_get_timeout
	.p2align 4
	.globl	SSL_set_session_secret_cb
	.type	SSL_set_session_secret_cb, @function
SSL_set_session_secret_cb:
.LFB1069:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L606
	movq	%rsi, 1760(%rdi)
	movl	$1, %eax
	movq	%rdx, 1768(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L606:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1069:
	.size	SSL_set_session_secret_cb, .-SSL_set_session_secret_cb
	.p2align 4
	.globl	SSL_set_session_ticket_ext_cb
	.type	SSL_set_session_ticket_ext_cb, @function
SSL_set_session_ticket_ext_cb:
.LFB1070:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L609
	movq	%rsi, 1744(%rdi)
	movl	$1, %eax
	movq	%rdx, 1752(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L609:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1070:
	.size	SSL_set_session_ticket_ext_cb, .-SSL_set_session_ticket_ext_cb
	.p2align 4
	.globl	SSL_set_session_ticket_ext
	.type	SSL_set_session_ticket_ext, @function
SSL_set_session_ticket_ext:
.LFB1071:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpl	$768, (%rdi)
	jg	.L616
.L610:
	addq	$8, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L616:
	.cfi_restore_state
	movq	%rdi, %rbx
	movq	1736(%rdi), %rdi
	movl	%edx, %r12d
	movq	%rsi, %r13
	movl	$1021, %edx
	leaq	.LC0(%rip), %rsi
	movslq	%r12d, %r15
	call	CRYPTO_free@PLT
	leaq	16(%r15), %rdi
	movq	$0, 1736(%rbx)
	movl	$1024, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, 1736(%rbx)
	testq	%rax, %rax
	je	.L617
	testq	%r13, %r13
	je	.L613
	leaq	16(%rax), %rdi
	movw	%r12w, (%rax)
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%rdi, 8(%rax)
	movl	$1, %r14d
	call	memcpy@PLT
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L613:
	xorl	%edx, %edx
	movq	$0, 8(%rax)
	movl	$1, %r14d
	movw	%dx, (%rax)
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L617:
	movl	$1026, %r8d
	movl	$65, %edx
	movl	$294, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L610
	.cfi_endproc
.LFE1071:
	.size	SSL_set_session_ticket_ext, .-SSL_set_session_ticket_ext
	.p2align 4
	.globl	SSL_CTX_flush_sessions
	.type	SSL_CTX_flush_sessions, @function
SSL_CTX_flush_sessions:
.LFB1074:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	movq	%rdi, -48(%rbp)
	movq	%rax, -32(%rbp)
	testq	%rax, %rax
	je	.L618
	movq	%rdi, %rbx
	movq	920(%rdi), %rdi
	movq	%rsi, -40(%rbp)
	call	CRYPTO_THREAD_write_lock@PLT
	movq	40(%rbx), %rdi
	call	OPENSSL_LH_get_down_load@PLT
	movq	40(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rax, %r12
	call	OPENSSL_LH_set_down_load@PLT
	movq	-32(%rbp), %rdi
	leaq	-48(%rbp), %rdx
	leaq	timeout_cb(%rip), %rsi
	call	OPENSSL_LH_doall_arg@PLT
	movq	40(%rbx), %rdi
	movq	%r12, %rsi
	call	OPENSSL_LH_set_down_load@PLT
	movq	920(%rbx), %rdi
	call	CRYPTO_THREAD_unlock@PLT
.L618:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L623
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L623:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1074:
	.size	SSL_CTX_flush_sessions, .-SSL_CTX_flush_sessions
	.p2align 4
	.globl	ssl_clear_bad_session
	.type	ssl_clear_bad_session, @function
ssl_clear_bad_session:
.LFB1075:
	.cfi_startproc
	endbr64
	cmpq	$0, 1296(%rdi)
	je	.L653
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	testb	$1, 68(%rdi)
	je	.L654
.L627:
	xorl	%eax, %eax
.L624:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L654:
	.cfi_restore_state
	call	SSL_in_init@PLT
	testl	%eax, %eax
	jne	.L627
	movq	%rbx, %rdi
	call	SSL_in_before@PLT
	testl	%eax, %eax
	jne	.L627
	movq	1296(%rbx), %r12
	testq	%r12, %r12
	je	.L652
	cmpq	$0, 336(%r12)
	je	.L652
	movq	1904(%rbx), %r13
	movq	920(%r13), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	40(%r13), %rdi
	movq	%r12, %rsi
	call	OPENSSL_LH_retrieve@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L630
	movq	40(%r13), %rdi
	call	OPENSSL_LH_delete@PLT
	movq	%rax, %r14
	movq	536(%rax), %rax
	testq	%rax, %rax
	je	.L631
	movq	528(%r14), %rdx
	testq	%rdx, %rdx
	je	.L631
	leaq	64(%r13), %rcx
	leaq	56(%r13), %rsi
	cmpq	%rcx, %rax
	je	.L655
	cmpq	%rdx, %rsi
	je	.L656
	movq	%rdx, 528(%rax)
	movq	%rax, 536(%rdx)
.L634:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 528(%r14)
.L631:
	movl	$1, 432(%r12)
	movq	920(%r13), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movq	96(%r13), %rax
	testq	%rax, %rax
	je	.L639
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	*%rax
.L639:
	movl	$-1, %eax
	lock xaddl	%eax, 472(%r14)
	subl	$1, %eax
	testl	%eax, %eax
	je	.L657
	jg	.L652
.L637:
	movq	%r14, %rdi
	call	SSL_SESSION_free.part.0
	.p2align 4,,10
	.p2align 3
.L652:
	movl	$1, %eax
	jmp	.L624
	.p2align 4,,10
	.p2align 3
.L653:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L630:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movl	$1, 432(%r12)
	movq	920(%r13), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movq	96(%r13), %rax
	testq	%rax, %rax
	je	.L652
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	*%rax
	movl	$1, %eax
	jmp	.L624
.L655:
	cmpq	%rdx, %rsi
	je	.L658
	movq	%rdx, 64(%r13)
	movq	%rcx, 536(%rdx)
	jmp	.L634
.L657:
	jmp	.L637
.L656:
	movq	%rax, 56(%r13)
	movq	%rsi, 528(%rax)
	jmp	.L634
.L658:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 56(%r13)
	jmp	.L634
	.cfi_endproc
.LFE1075:
	.size	ssl_clear_bad_session, .-ssl_clear_bad_session
	.p2align 4
	.globl	SSL_CTX_sess_set_new_cb
	.type	SSL_CTX_sess_set_new_cb, @function
SSL_CTX_sess_set_new_cb:
.LFB1078:
	.cfi_startproc
	endbr64
	movq	%rsi, 88(%rdi)
	ret
	.cfi_endproc
.LFE1078:
	.size	SSL_CTX_sess_set_new_cb, .-SSL_CTX_sess_set_new_cb
	.p2align 4
	.globl	SSL_CTX_sess_get_new_cb
	.type	SSL_CTX_sess_get_new_cb, @function
SSL_CTX_sess_get_new_cb:
.LFB1079:
	.cfi_startproc
	endbr64
	movq	88(%rdi), %rax
	ret
	.cfi_endproc
.LFE1079:
	.size	SSL_CTX_sess_get_new_cb, .-SSL_CTX_sess_get_new_cb
	.p2align 4
	.globl	SSL_CTX_sess_set_remove_cb
	.type	SSL_CTX_sess_set_remove_cb, @function
SSL_CTX_sess_set_remove_cb:
.LFB1080:
	.cfi_startproc
	endbr64
	movq	%rsi, 96(%rdi)
	ret
	.cfi_endproc
.LFE1080:
	.size	SSL_CTX_sess_set_remove_cb, .-SSL_CTX_sess_set_remove_cb
	.p2align 4
	.globl	SSL_CTX_sess_get_remove_cb
	.type	SSL_CTX_sess_get_remove_cb, @function
SSL_CTX_sess_get_remove_cb:
.LFB1081:
	.cfi_startproc
	endbr64
	movq	96(%rdi), %rax
	ret
	.cfi_endproc
.LFE1081:
	.size	SSL_CTX_sess_get_remove_cb, .-SSL_CTX_sess_get_remove_cb
	.p2align 4
	.globl	SSL_CTX_sess_set_get_cb
	.type	SSL_CTX_sess_set_get_cb, @function
SSL_CTX_sess_set_get_cb:
.LFB1082:
	.cfi_startproc
	endbr64
	movq	%rsi, 104(%rdi)
	ret
	.cfi_endproc
.LFE1082:
	.size	SSL_CTX_sess_set_get_cb, .-SSL_CTX_sess_set_get_cb
	.p2align 4
	.globl	SSL_CTX_sess_get_get_cb
	.type	SSL_CTX_sess_get_get_cb, @function
SSL_CTX_sess_get_get_cb:
.LFB1083:
	.cfi_startproc
	endbr64
	movq	104(%rdi), %rax
	ret
	.cfi_endproc
.LFE1083:
	.size	SSL_CTX_sess_get_get_cb, .-SSL_CTX_sess_get_get_cb
	.p2align 4
	.globl	SSL_CTX_set_info_callback
	.type	SSL_CTX_set_info_callback, @function
SSL_CTX_set_info_callback:
.LFB1084:
	.cfi_startproc
	endbr64
	movq	%rsi, 272(%rdi)
	ret
	.cfi_endproc
.LFE1084:
	.size	SSL_CTX_set_info_callback, .-SSL_CTX_set_info_callback
	.p2align 4
	.globl	SSL_CTX_get_info_callback
	.type	SSL_CTX_get_info_callback, @function
SSL_CTX_get_info_callback:
.LFB1085:
	.cfi_startproc
	endbr64
	movq	272(%rdi), %rax
	ret
	.cfi_endproc
.LFE1085:
	.size	SSL_CTX_get_info_callback, .-SSL_CTX_get_info_callback
	.p2align 4
	.globl	SSL_CTX_set_client_cert_cb
	.type	SSL_CTX_set_client_cert_cb, @function
SSL_CTX_set_client_cert_cb:
.LFB1086:
	.cfi_startproc
	endbr64
	movq	%rsi, 192(%rdi)
	ret
	.cfi_endproc
.LFE1086:
	.size	SSL_CTX_set_client_cert_cb, .-SSL_CTX_set_client_cert_cb
	.p2align 4
	.globl	SSL_CTX_get_client_cert_cb
	.type	SSL_CTX_get_client_cert_cb, @function
SSL_CTX_get_client_cert_cb:
.LFB1087:
	.cfi_startproc
	endbr64
	movq	192(%rdi), %rax
	ret
	.cfi_endproc
.LFE1087:
	.size	SSL_CTX_get_client_cert_cb, .-SSL_CTX_get_client_cert_cb
	.p2align 4
	.globl	SSL_CTX_set_client_cert_engine
	.type	SSL_CTX_set_client_cert_engine, @function
SSL_CTX_set_client_cert_engine:
.LFB1088:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$16, %rsp
	call	ENGINE_init@PLT
	testl	%eax, %eax
	je	.L674
	movq	%r13, %rdi
	call	ENGINE_get_ssl_client_cert_function@PLT
	testq	%rax, %rax
	je	.L675
	movq	%r13, 488(%rbx)
	movl	$1, %eax
.L669:
	addq	$16, %rsp
	popq	%rbx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L674:
	.cfi_restore_state
	movl	$1209, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$38, %edx
	movl	%eax, -20(%rbp)
	movl	$290, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	movl	-20(%rbp), %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L675:
	.cfi_restore_state
	movl	$1213, %r8d
	movl	$331, %edx
	movl	$290, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r13, %rdi
	call	ENGINE_finish@PLT
	xorl	%eax, %eax
	jmp	.L669
	.cfi_endproc
.LFE1088:
	.size	SSL_CTX_set_client_cert_engine, .-SSL_CTX_set_client_cert_engine
	.p2align 4
	.globl	SSL_CTX_set_cookie_generate_cb
	.type	SSL_CTX_set_cookie_generate_cb, @function
SSL_CTX_set_cookie_generate_cb:
.LFB1089:
	.cfi_startproc
	endbr64
	movq	%rsi, 200(%rdi)
	ret
	.cfi_endproc
.LFE1089:
	.size	SSL_CTX_set_cookie_generate_cb, .-SSL_CTX_set_cookie_generate_cb
	.p2align 4
	.globl	SSL_CTX_set_cookie_verify_cb
	.type	SSL_CTX_set_cookie_verify_cb, @function
SSL_CTX_set_cookie_verify_cb:
.LFB1090:
	.cfi_startproc
	endbr64
	movq	%rsi, 208(%rdi)
	ret
	.cfi_endproc
.LFE1090:
	.size	SSL_CTX_set_cookie_verify_cb, .-SSL_CTX_set_cookie_verify_cb
	.p2align 4
	.globl	SSL_SESSION_set1_ticket_appdata
	.type	SSL_SESSION_set1_ticket_appdata, @function
SSL_SESSION_set1_ticket_appdata:
.LFB1091:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	leaq	.LC0(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	movl	$1241, %edx
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	616(%rdi), %rdi
	call	CRYPTO_free@PLT
	movq	$0, 624(%rbx)
	testq	%r13, %r13
	je	.L683
	testq	%r12, %r12
	je	.L683
	movl	$1247, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	CRYPTO_memdup@PLT
	xorl	%r8d, %r8d
	movq	%rax, 616(%rbx)
	testq	%rax, %rax
	je	.L678
	movq	%r12, 624(%rbx)
	movl	$1, %r8d
	addq	$8, %rsp
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L683:
	.cfi_restore_state
	movq	$0, 616(%rbx)
	movl	$1, %r8d
.L678:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1091:
	.size	SSL_SESSION_set1_ticket_appdata, .-SSL_SESSION_set1_ticket_appdata
	.p2align 4
	.globl	SSL_SESSION_get0_ticket_appdata
	.type	SSL_SESSION_get0_ticket_appdata, @function
SSL_SESSION_get0_ticket_appdata:
.LFB1092:
	.cfi_startproc
	endbr64
	movq	616(%rdi), %rax
	movq	%rax, (%rsi)
	movq	624(%rdi), %rax
	movq	%rax, (%rdx)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1092:
	.size	SSL_SESSION_get0_ticket_appdata, .-SSL_SESSION_get0_ticket_appdata
	.p2align 4
	.globl	SSL_CTX_set_stateless_cookie_generate_cb
	.type	SSL_CTX_set_stateless_cookie_generate_cb, @function
SSL_CTX_set_stateless_cookie_generate_cb:
.LFB1093:
	.cfi_startproc
	endbr64
	movq	%rsi, 216(%rdi)
	ret
	.cfi_endproc
.LFE1093:
	.size	SSL_CTX_set_stateless_cookie_generate_cb, .-SSL_CTX_set_stateless_cookie_generate_cb
	.p2align 4
	.globl	SSL_CTX_set_stateless_cookie_verify_cb
	.type	SSL_CTX_set_stateless_cookie_verify_cb, @function
SSL_CTX_set_stateless_cookie_verify_cb:
.LFB1094:
	.cfi_startproc
	endbr64
	movq	%rsi, 224(%rdi)
	ret
	.cfi_endproc
.LFE1094:
	.size	SSL_CTX_set_stateless_cookie_verify_cb, .-SSL_CTX_set_stateless_cookie_verify_cb
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"SSL SESSION PARAMETERS"
	.text
	.p2align 4
	.globl	PEM_read_bio_SSL_SESSION
	.type	PEM_read_bio_SSL_SESSION, @function
PEM_read_bio_SSL_SESSION:
.LFB1095:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	movq	%rdi, %rdx
	movq	d2i_SSL_SESSION@GOTPCREL(%rip), %rdi
	movq	%rcx, %r9
	movq	%rsi, %rcx
	leaq	.LC1(%rip), %rsi
	jmp	PEM_ASN1_read_bio@PLT
	.cfi_endproc
.LFE1095:
	.size	PEM_read_bio_SSL_SESSION, .-PEM_read_bio_SSL_SESSION
	.p2align 4
	.globl	PEM_read_SSL_SESSION
	.type	PEM_read_SSL_SESSION, @function
PEM_read_SSL_SESSION:
.LFB1096:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	movq	%rdi, %rdx
	movq	d2i_SSL_SESSION@GOTPCREL(%rip), %rdi
	movq	%rcx, %r9
	movq	%rsi, %rcx
	leaq	.LC1(%rip), %rsi
	jmp	PEM_ASN1_read@PLT
	.cfi_endproc
.LFE1096:
	.size	PEM_read_SSL_SESSION, .-PEM_read_SSL_SESSION
	.p2align 4
	.globl	PEM_write_bio_SSL_SESSION
	.type	PEM_write_bio_SSL_SESSION, @function
PEM_write_bio_SSL_SESSION:
.LFB1097:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	i2d_SSL_SESSION@GOTPCREL(%rip), %rdi
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	.LC1(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write_bio@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1097:
	.size	PEM_write_bio_SSL_SESSION, .-PEM_write_bio_SSL_SESSION
	.p2align 4
	.globl	PEM_write_SSL_SESSION
	.type	PEM_write_SSL_SESSION, @function
PEM_write_SSL_SESSION:
.LFB1098:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	i2d_SSL_SESSION@GOTPCREL(%rip), %rdi
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	.LC1(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1098:
	.size	PEM_write_SSL_SESSION, .-PEM_write_SSL_SESSION
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
