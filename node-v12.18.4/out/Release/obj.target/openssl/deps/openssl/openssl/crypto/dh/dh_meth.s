	.file	"dh_meth.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/dh/dh_meth.c"
	.text
	.p2align 4
	.globl	DH_meth_new
	.type	DH_meth_new, @function
DH_meth_new:
.LFB384:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$72, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	%esi, %ebx
	leaq	.LC0(%rip), %rsi
	subq	$8, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L2
	movl	%ebx, 48(%rax)
	movl	$21, %edx
	movq	%r13, %rdi
	movq	%rax, %r12
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L9
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	movl	$25, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
.L2:
	movl	$28, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$118, %esi
	movl	$5, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE384:
	.size	DH_meth_new, .-DH_meth_new
	.p2align 4
	.globl	DH_meth_free
	.type	DH_meth_free, @function
DH_meth_free:
.LFB385:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L10
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$35, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$36, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	ret
	.cfi_endproc
.LFE385:
	.size	DH_meth_free, .-DH_meth_free
	.p2align 4
	.globl	DH_meth_dup
	.type	DH_meth_dup, @function
DH_meth_dup:
.LFB386:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$42, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$72, %edi
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L16
	movdqu	(%rbx), %xmm0
	movq	%rax, %r12
	movl	$47, %edx
	leaq	.LC0(%rip), %rsi
	movups	%xmm0, (%rax)
	movdqu	16(%rbx), %xmm1
	movups	%xmm1, 16(%rax)
	movdqu	32(%rbx), %xmm2
	movups	%xmm2, 32(%rax)
	movdqu	48(%rbx), %xmm3
	movups	%xmm3, 48(%rax)
	movq	64(%rbx), %rax
	movq	%rax, 64(%r12)
	movq	(%rbx), %rdi
	call	CRYPTO_strdup@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L22
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	movl	$51, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
.L16:
	xorl	%r12d, %r12d
	movl	$54, %r8d
	movl	$65, %edx
	movl	$117, %esi
	leaq	.LC0(%rip), %rcx
	movl	$5, %edi
	call	ERR_put_error@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE386:
	.size	DH_meth_dup, .-DH_meth_dup
	.p2align 4
	.globl	DH_meth_get0_name
	.type	DH_meth_get0_name, @function
DH_meth_get0_name:
.LFB387:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE387:
	.size	DH_meth_get0_name, .-DH_meth_get0_name
	.p2align 4
	.globl	DH_meth_set1_name
	.type	DH_meth_set1_name, @function
DH_meth_set1_name:
.LFB388:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$65, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	testq	%rax, %rax
	je	.L28
	movq	(%r12), %rdi
	movq	%rax, %rbx
	movl	$72, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%rbx, (%r12)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movl	$68, %r8d
	movl	$65, %edx
	movl	$119, %esi
	movl	$5, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE388:
	.size	DH_meth_set1_name, .-DH_meth_set1_name
	.p2align 4
	.globl	DH_meth_get_flags
	.type	DH_meth_get_flags, @function
DH_meth_get_flags:
.LFB389:
	.cfi_startproc
	endbr64
	movl	48(%rdi), %eax
	ret
	.cfi_endproc
.LFE389:
	.size	DH_meth_get_flags, .-DH_meth_get_flags
	.p2align 4
	.globl	DH_meth_set_flags
	.type	DH_meth_set_flags, @function
DH_meth_set_flags:
.LFB390:
	.cfi_startproc
	endbr64
	movl	%esi, 48(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE390:
	.size	DH_meth_set_flags, .-DH_meth_set_flags
	.p2align 4
	.globl	DH_meth_get0_app_data
	.type	DH_meth_get0_app_data, @function
DH_meth_get0_app_data:
.LFB391:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rax
	ret
	.cfi_endproc
.LFE391:
	.size	DH_meth_get0_app_data, .-DH_meth_get0_app_data
	.p2align 4
	.globl	DH_meth_set0_app_data
	.type	DH_meth_set0_app_data, @function
DH_meth_set0_app_data:
.LFB392:
	.cfi_startproc
	endbr64
	movq	%rsi, 56(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE392:
	.size	DH_meth_set0_app_data, .-DH_meth_set0_app_data
	.p2align 4
	.globl	DH_meth_get_generate_key
	.type	DH_meth_get_generate_key, @function
DH_meth_get_generate_key:
.LFB393:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE393:
	.size	DH_meth_get_generate_key, .-DH_meth_get_generate_key
	.p2align 4
	.globl	DH_meth_set_generate_key
	.type	DH_meth_set_generate_key, @function
DH_meth_set_generate_key:
.LFB394:
	.cfi_startproc
	endbr64
	movq	%rsi, 8(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE394:
	.size	DH_meth_set_generate_key, .-DH_meth_set_generate_key
	.p2align 4
	.globl	DH_meth_get_compute_key
	.type	DH_meth_get_compute_key, @function
DH_meth_get_compute_key:
.LFB395:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE395:
	.size	DH_meth_get_compute_key, .-DH_meth_get_compute_key
	.p2align 4
	.globl	DH_meth_set_compute_key
	.type	DH_meth_set_compute_key, @function
DH_meth_set_compute_key:
.LFB396:
	.cfi_startproc
	endbr64
	movq	%rsi, 16(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE396:
	.size	DH_meth_set_compute_key, .-DH_meth_set_compute_key
	.p2align 4
	.globl	DH_meth_get_bn_mod_exp
	.type	DH_meth_get_bn_mod_exp, @function
DH_meth_get_bn_mod_exp:
.LFB397:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE397:
	.size	DH_meth_get_bn_mod_exp, .-DH_meth_get_bn_mod_exp
	.p2align 4
	.globl	DH_meth_set_bn_mod_exp
	.type	DH_meth_set_bn_mod_exp, @function
DH_meth_set_bn_mod_exp:
.LFB398:
	.cfi_startproc
	endbr64
	movq	%rsi, 24(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE398:
	.size	DH_meth_set_bn_mod_exp, .-DH_meth_set_bn_mod_exp
	.p2align 4
	.globl	DH_meth_get_init
	.type	DH_meth_get_init, @function
DH_meth_get_init:
.LFB399:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE399:
	.size	DH_meth_get_init, .-DH_meth_get_init
	.p2align 4
	.globl	DH_meth_set_init
	.type	DH_meth_set_init, @function
DH_meth_set_init:
.LFB400:
	.cfi_startproc
	endbr64
	movq	%rsi, 32(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE400:
	.size	DH_meth_set_init, .-DH_meth_set_init
	.p2align 4
	.globl	DH_meth_get_finish
	.type	DH_meth_get_finish, @function
DH_meth_get_finish:
.LFB401:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE401:
	.size	DH_meth_get_finish, .-DH_meth_get_finish
	.p2align 4
	.globl	DH_meth_set_finish
	.type	DH_meth_set_finish, @function
DH_meth_set_finish:
.LFB402:
	.cfi_startproc
	endbr64
	movq	%rsi, 40(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE402:
	.size	DH_meth_set_finish, .-DH_meth_set_finish
	.p2align 4
	.globl	DH_meth_get_generate_params
	.type	DH_meth_get_generate_params, @function
DH_meth_get_generate_params:
.LFB403:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	ret
	.cfi_endproc
.LFE403:
	.size	DH_meth_get_generate_params, .-DH_meth_get_generate_params
	.p2align 4
	.globl	DH_meth_set_generate_params
	.type	DH_meth_set_generate_params, @function
DH_meth_set_generate_params:
.LFB404:
	.cfi_startproc
	endbr64
	movq	%rsi, 64(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE404:
	.size	DH_meth_set_generate_params, .-DH_meth_set_generate_params
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
