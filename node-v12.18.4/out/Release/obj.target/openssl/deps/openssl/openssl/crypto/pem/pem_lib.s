	.file	"pem_lib.c"
	.text
	.p2align 4
	.type	sanitize_line, @function
sanitize_line:
.LFB869:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	testb	$2, %dl
	je	.L2
	testl	%esi, %esi
	js	.L28
	movslq	%esi, %rax
	addq	%rdi, %rax
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L6:
	subl	$1, %r12d
	leaq	-1(%rbx), %rax
	cmpl	$-1, %r12d
	je	.L15
.L4:
	cmpb	$32, (%rax)
	movq	%rax, %rbx
	jle	.L6
.L28:
	leal	1(%r12), %eax
	movslq	%eax, %rbx
	addq	%r13, %rbx
.L5:
	addl	$1, %eax
	movslq	%eax, %rcx
	addq	%r13, %rcx
.L7:
	movb	$10, (%rbx)
	movb	$0, (%rcx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	andl	$4, %edx
	leaq	1(%rdi), %rcx
	jne	.L8
	movq	%rdi, %r15
	xorl	%r14d, %r14d
	testl	%esi, %esi
	jg	.L14
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L17:
	movl	%eax, %r14d
.L14:
	movsbl	(%r15), %edi
	movq	%r15, %rbx
	cmpb	$10, %dil
	je	.L18
	cmpb	$13, %dil
	je	.L18
	movl	$64, %esi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	je	.L13
	movb	$32, (%r15)
.L13:
	leal	1(%r14), %eax
	addq	$1, %r15
	cmpl	%eax, %r12d
	jne	.L17
	leal	2(%r14), %eax
	movslq	%r12d, %rbx
	movslq	%eax, %rcx
	addq	%r13, %rbx
	addq	%r13, %rcx
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L8:
	testl	%esi, %esi
	jle	.L16
	leal	-1(%rsi), %r14d
	movl	$1, %r12d
	movq	%rdi, %rdx
	addq	%rcx, %r14
	subl	%edi, %r12d
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L31:
	movzbl	(%rbx), %esi
	cmpb	$13, %sil
	je	.L7
	cmpb	$10, %sil
	je	.L7
	cmpq	%rdx, %r14
	je	.L30
.L10:
	movsbl	(%rdx), %edi
	movl	$1024, %esi
	movq	%rdx, %rbx
	call	ossl_ctype_check@PLT
	leaq	1(%rbx), %rdx
	movl	%eax, %r8d
	movq	%rdx, %rcx
	leal	(%r12,%rbx), %eax
	testl	%r8d, %r8d
	jne	.L31
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L15:
	xorl	%eax, %eax
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L18:
	leal	1(%r14), %eax
	movslq	%eax, %rcx
	addq	%r13, %rcx
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L30:
	addl	$1, %eax
	movq	%rcx, %rbx
	movslq	%eax, %rdx
	leaq	0(%r13,%rdx), %rcx
	jmp	.L7
.L16:
	movq	%r13, %rbx
	movl	$1, %eax
	jmp	.L7
	.cfi_endproc
.LFE869:
	.size	sanitize_line, .-sanitize_line
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Enter PEM pass phrase:"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../deps/openssl/openssl/crypto/pem/pem_lib.c"
	.text
	.p2align 4
	.globl	PEM_def_callback
	.type	PEM_def_callback, @function
PEM_def_callback:
.LFB851:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	%esi, %ebx
	testq	%rcx, %rcx
	je	.L33
	movq	%rcx, %rdi
	movq	%rcx, %r13
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	cmpl	%eax, %ebx
	movl	%eax, %r12d
	cmovle	%ebx, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
.L32:
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	movl	%edx, %r12d
	call	EVP_get_pw_prompt@PLT
	movl	%r12d, %r8d
	movl	%ebx, %edx
	movq	%r14, %rdi
	testq	%rax, %rax
	movq	%rax, %rcx
	leaq	.LC0(%rip), %rax
	cmove	%rax, %rcx
	xorl	%esi, %esi
	testl	%r12d, %r12d
	setne	%sil
	sall	$2, %esi
	call	EVP_read_pw_string_min@PLT
	testl	%eax, %eax
	jne	.L40
	movq	%r14, %rdi
	call	strlen@PLT
	popq	%rbx
	movl	%eax, %r12d
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L40:
	.cfi_restore_state
	movl	$59, %r8d
	movl	$109, %edx
	movl	$100, %esi
	movl	$9, %edi
	leaq	.LC1(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	movl	%ebx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	memset@PLT
	jmp	.L32
	.cfi_endproc
.LFE851:
	.size	PEM_def_callback, .-PEM_def_callback
	.section	.rodata.str1.1
.LC2:
	.string	"MIC-CLEAR"
.LC3:
	.string	"MIC-ONLY"
.LC4:
	.string	"ENCRYPTED"
.LC5:
	.string	"BAD-TYPE"
.LC6:
	.string	"Proc-Type: 4,%s\n"
	.text
	.p2align 4
	.globl	PEM_proc_type
	.type	PEM_proc_type, @function
PEM_proc_type:
.LFB852:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	strlen@PLT
	leaq	.LC4(%rip), %rcx
	leaq	(%rbx,%rax), %rdi
	cmpl	$10, %r12d
	je	.L42
	leaq	.LC2(%rip), %rcx
	cmpl	$30, %r12d
	je	.L42
	cmpl	$20, %r12d
	leaq	.LC3(%rip), %rcx
	leaq	.LC5(%rip), %rdx
	cmovne	%rdx, %rcx
.L42:
	movl	$1024, %esi
	leaq	.LC6(%rip), %rdx
	popq	%rbx
	subq	%rax, %rsi
	popq	%r12
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	BIO_snprintf@PLT
	.cfi_endproc
.LFE852:
	.size	PEM_proc_type, .-PEM_proc_type
	.section	.rodata.str1.1
.LC7:
	.string	"DEK-Info: %s,"
.LC8:
	.string	"%02X"
	.text
	.p2align 4
	.globl	PEM_dek_info
	.type	PEM_dek_info, @function
PEM_dek_info:
.LFB853:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	$1024, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movslq	%edx, %rbx
	subq	$8, %rsp
	call	strlen@PLT
	movq	%r12, %rcx
	leaq	.LC7(%rip), %rdx
	addq	%rax, %r14
	subl	%eax, %r15d
	xorl	%eax, %eax
	movslq	%r15d, %rsi
	movq	%r14, %rdi
	call	BIO_snprintf@PLT
	testl	%eax, %eax
	jle	.L49
	subl	%eax, %r15d
	cltq
	addq	%rax, %r14
	testl	%ebx, %ebx
	jle	.L52
	addq	%r13, %rbx
	leaq	.LC8(%rip), %r12
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L57:
	subl	%eax, %r15d
	addq	$1, %r13
	cltq
	addq	%rax, %r14
	cmpq	%rbx, %r13
	je	.L52
.L54:
	movzbl	0(%r13), %ecx
	xorl	%eax, %eax
	movslq	%r15d, %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	BIO_snprintf@PLT
	testl	%eax, %eax
	jg	.L57
.L49:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	cmpl	$1, %r15d
	jle	.L49
	movl	$10, %eax
	movw	%ax, (%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE853:
	.size	PEM_dek_info, .-PEM_dek_info
	.p2align 4
	.globl	PEM_ASN1_read
	.type	PEM_ASN1_read, @function
PEM_ASN1_read:
.LFB854:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	movq	%rsi, -64(%rbp)
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	-56(%rbp), %rdx
	testq	%rax, %rax
	je	.L62
	movq	%rdx, %rcx
	movl	$106, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	movq	%rax, %r13
	call	BIO_ctrl@PLT
	movq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r15, %r9
	movq	%rbx, %r8
	movq	%r14, %rcx
	movq	%r13, %rdx
	call	PEM_ASN1_read_bio@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	BIO_free@PLT
.L58:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	movl	$113, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$7, %edx
	xorl	%r12d, %r12d
	movl	$102, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
	jmp	.L58
	.cfi_endproc
.LFE854:
	.size	PEM_ASN1_read, .-PEM_ASN1_read
	.p2align 4
	.globl	PEM_do_header
	.type	PEM_do_header, @function
PEM_do_header:
.LFB863:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$1144, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -1176(%rbp)
	movq	(%rdx), %rdx
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movl	%edx, -1156(%rbp)
	cmpq	$2147483647, %rdx
	jg	.L88
	cmpq	$0, (%rdi)
	movq	%rdi, %rbx
	je	.L74
	leaq	-1088(%rbp), %r13
	movq	%r8, %rcx
	xorl	%edx, %edx
	movl	$1024, %esi
	movq	%r13, %rdi
	testq	%rax, %rax
	je	.L89
	call	*%rax
	movl	%eax, %r8d
.L67:
	testl	%r8d, %r8d
	js	.L90
	leaq	-1152(%rbp), %r14
	movl	%r8d, -1184(%rbp)
	leaq	8(%rbx), %r15
	call	EVP_md5@PLT
	pushq	$0
	movq	(%rbx), %rdi
	movq	%r13, %rcx
	pushq	%r14
	movl	-1184(%rbp), %r8d
	movq	%r15, %rdx
	movq	%rax, %rsi
	movl	$1, %r9d
	call	EVP_BytesToKey@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	je	.L70
	call	EVP_CIPHER_CTX_new@PLT
	testq	%rax, %rax
	je	.L70
	movq	(%rbx), %rsi
	xorl	%edx, %edx
	movq	%r15, %r8
	movq	%r14, %rcx
	movq	%rax, %rdi
	movq	%rax, -1184(%rbp)
	call	EVP_DecryptInit_ex@PLT
	movq	-1184(%rbp), %r9
	testl	%eax, %eax
	jne	.L91
.L71:
	movl	$461, %r8d
	movl	$101, %edx
	movl	$106, %esi
	movl	$9, %edi
	leaq	.LC1(%rip), %rcx
	movq	%r9, -1176(%rbp)
	call	ERR_put_error@PLT
	movq	-1176(%rbp), %r9
	xorl	%eax, %eax
.L72:
	movq	%r9, %rdi
	movl	%eax, -1176(%rbp)
	call	EVP_CIPHER_CTX_free@PLT
	movl	$1024, %esi
	movq	%r13, %rdi
	call	OPENSSL_cleanse@PLT
	movl	$64, %esi
	movq	%r14, %rdi
	call	OPENSSL_cleanse@PLT
	movl	-1176(%rbp), %eax
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L70:
	xorl	%eax, %eax
.L63:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L92
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	movl	$422, %r8d
	movl	$128, %edx
	movl	$106, %esi
	movl	$9, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L90:
	movl	$434, %r8d
	movl	$104, %edx
	movl	$106, %esi
	movl	$9, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L74:
	movl	$1, %eax
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L89:
	call	PEM_def_callback
	movl	%eax, %r8d
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L91:
	movq	-1176(%rbp), %rbx
	movl	-1156(%rbp), %r8d
	movq	%r9, %rdi
	leaq	-1156(%rbp), %r15
	movq	%r15, %rdx
	movq	%r9, -1176(%rbp)
	movq	%rbx, %rcx
	movq	%rbx, %rsi
	call	EVP_DecryptUpdate@PLT
	movq	-1176(%rbp), %r9
	testl	%eax, %eax
	je	.L71
	movslq	-1156(%rbp), %rsi
	movq	%r9, %rdi
	movq	%r15, %rdx
	movq	%rsi, (%r12)
	addq	%rbx, %rsi
	call	EVP_DecryptFinal_ex@PLT
	movq	-1176(%rbp), %r9
	testl	%eax, %eax
	je	.L71
	movslq	-1156(%rbp), %rdx
	addq	%rdx, (%r12)
	jmp	.L72
.L92:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE863:
	.size	PEM_do_header, .-PEM_do_header
	.section	.rodata.str1.1
.LC9:
	.string	" \t"
.LC10:
	.string	" \t\r\n"
.LC11:
	.string	" \t\r"
.LC12:
	.string	" \t,"
	.text
	.p2align 4
	.globl	PEM_get_EVP_CIPHER_INFO
	.type	PEM_get_EVP_CIPHER_INFO, @function
PEM_get_EVP_CIPHER_INFO:
.LFB864:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	$0, (%rsi)
	movups	%xmm0, 8(%rsi)
	testq	%rdi, %rdi
	je	.L96
	movzbl	(%rdi), %eax
	movq	%rdi, %rbx
	testb	%al, %al
	je	.L96
	cmpb	$10, %al
	je	.L96
	movq	%rsi, %r14
	leaq	8(%rsi), %r12
	movl	$10, %edx
	leaq	ProcType.19085(%rip), %rsi
	call	strncmp@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.L111
	addq	$10, %rbx
	leaq	.LC9(%rip), %rsi
	movq	%rbx, %rdi
	call	strspn@PLT
	addq	%rax, %rbx
	cmpb	$52, (%rbx)
	jne	.L93
	cmpb	$44, 1(%rbx)
	jne	.L93
	addq	$2, %rbx
	leaq	.LC9(%rip), %rsi
	movq	%rbx, %rdi
	call	strspn@PLT
	movl	$9, %edx
	leaq	ENCRYPTED.19086(%rip), %rsi
	addq	%rax, %rbx
	movq	%rbx, %rdi
	call	strncmp@PLT
	testl	%eax, %eax
	jne	.L98
	addq	$9, %rbx
	leaq	.LC10(%rip), %rsi
	movq	%rbx, %rdi
	call	strspn@PLT
	testq	%rax, %rax
	je	.L98
	movq	%rbx, %rdi
	leaq	.LC11(%rip), %rsi
	call	strspn@PLT
	addq	%rax, %rbx
	cmpb	$10, (%rbx)
	jne	.L112
	leaq	1(%rbx), %rdi
	movl	$9, %edx
	leaq	DEKInfo.19087(%rip), %rsi
	call	strncmp@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.L113
	addq	$10, %rbx
	leaq	.LC9(%rip), %rsi
	movq	%rbx, %rdi
	call	strspn@PLT
	leaq	.LC12(%rip), %rsi
	leaq	(%rbx,%rax), %r15
	movq	%r15, %rdi
	call	strcspn@PLT
	movq	%r15, %rdi
	leaq	(%r15,%rax), %rbx
	movzbl	(%rbx), %edx
	movb	$0, (%rbx)
	movb	%dl, -49(%rbp)
	call	EVP_get_cipherbyname@PLT
	movzbl	-49(%rbp), %edx
	movq	%rbx, %rdi
	leaq	.LC9(%rip), %rsi
	movq	%rax, (%r14)
	movq	%rax, %r15
	movb	%dl, (%rbx)
	call	strspn@PLT
	addq	%rax, %rbx
	testq	%r15, %r15
	je	.L114
	movq	%r15, %rdi
	call	EVP_CIPHER_iv_length@PLT
	testl	%eax, %eax
	jle	.L103
	cmpb	$44, (%rbx)
	jne	.L115
	addq	$1, %rbx
.L105:
	movq	%r15, %rdi
	call	EVP_CIPHER_iv_length@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	jle	.L96
	leal	-1(%rax), %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	addl	%r14d, %r14d
	addq	$1, %rdx
	xorl	%r15d, %r15d
	call	memset@PLT
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L106:
	movl	%r15d, %edx
	movl	%eax, %ecx
	sarl	%edx
	sall	$4, %ecx
	testb	$1, %r15b
	cmove	%ecx, %eax
	movslq	%edx, %rdx
	addq	$1, %r15
	orb	%al, (%r12,%rdx)
	cmpl	%r15d, %r14d
	jle	.L96
.L108:
	movzbl	(%rbx,%r15), %edi
	call	OPENSSL_hexchar2int@PLT
	testl	%eax, %eax
	jns	.L106
	movl	$573, %r8d
	movl	$103, %edx
	movl	$101, %esi
	movl	$9, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L96:
	movl	$1, %r13d
.L93:
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	movl	$496, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$107, %edx
	xorl	%r13d, %r13d
	movl	$107, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L98:
	movl	$509, %r8d
	movl	$106, %edx
	movl	$107, %esi
	movl	$9, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L112:
	movl	$515, %r8d
	movl	$112, %edx
	movl	$107, %esi
	movl	$9, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L113:
	movl	$524, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$105, %edx
	xorl	%r13d, %r13d
	movl	$107, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
	jmp	.L93
.L103:
	jne	.L105
	cmpb	$44, (%rbx)
	jne	.L105
	movl	$551, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$130, %edx
	xorl	%r13d, %r13d
	movl	$107, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
	jmp	.L93
.L114:
	movl	$543, %r8d
	movl	$114, %edx
	movl	$107, %esi
	movl	$9, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L93
.L115:
	movl	$548, %r8d
	movl	$129, %edx
	movl	$107, %esi
	movl	$9, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L93
	.cfi_endproc
.LFE864:
	.size	PEM_get_EVP_CIPHER_INFO, .-PEM_get_EVP_CIPHER_INFO
	.section	.rodata.str1.1
.LC13:
	.string	"-----BEGIN "
.LC14:
	.string	"-----\n"
.LC15:
	.string	"\n"
.LC16:
	.string	"-----END "
	.text
	.p2align 4
	.globl	PEM_write_bio
	.type	PEM_write_bio, @function
PEM_write_bio:
.LFB867:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_ENCODE_CTX_new@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L122
	movq	%rax, %rdi
	call	EVP_EncodeInit@PLT
	movq	%r13, %rdi
	call	strlen@PLT
	movl	$11, %edx
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	call	BIO_write@PLT
	cmpl	$11, %eax
	je	.L119
.L120:
	movl	$7, %r14d
	xorl	%r15d, %r15d
.L118:
	movl	%r14d, %edx
	movl	$114, %esi
	movl	$9, %edi
	xorl	%r12d, %r12d
	movl	$658, %r8d
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
.L131:
	movq	%rbx, %rdi
	call	EVP_ENCODE_CTX_free@PLT
	movl	$660, %ecx
	movl	$8192, %esi
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L150
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	.cfi_restore_state
	movl	-80(%rbp), %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	cmpl	-80(%rbp), %eax
	jne	.L120
	movl	$6, %edx
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	cmpl	$6, %eax
	jne	.L120
	movq	%r15, %rdi
	call	strlen@PLT
	testl	%eax, %eax
	jle	.L124
	movl	%eax, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -88(%rbp)
	call	BIO_write@PLT
	movq	-88(%rbp), %rcx
	cmpl	%ecx, %eax
	jne	.L120
	movl	$1, %edx
	leaq	.LC15(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	cmpl	$1, %eax
	jne	.L120
	.p2align 4,,10
	.p2align 3
.L124:
	movl	$630, %edx
	leaq	.LC1(%rip), %rsi
	movl	$8192, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L122
	testq	%r14, %r14
	jle	.L133
	xorl	%ecx, %ecx
	leaq	-60(%rbp), %rax
	movq	%r12, -104(%rbp)
	movq	%r13, -112(%rbp)
	movl	%ecx, %r12d
	movq	%r14, %r13
	movl	$0, -92(%rbp)
	movq	%rax, -88(%rbp)
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L126:
	subq	%r14, %r13
	addl	%edx, -92(%rbp)
	addl	%r14d, %r12d
	testq	%r13, %r13
	jle	.L151
.L127:
	cmpq	$5120, %r13
	movslq	%r12d, %rcx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movl	$5120, %r14d
	movq	-88(%rbp), %rdx
	cmovle	%r13, %r14
	addq	-72(%rbp), %rcx
	movl	%r14d, %r8d
	call	EVP_EncodeUpdate@PLT
	testl	%eax, %eax
	je	.L137
	movl	-60(%rbp), %edx
	testl	%edx, %edx
	je	.L126
	movq	-104(%rbp), %rdi
	movq	%r15, %rsi
	call	BIO_write@PLT
	movl	-60(%rbp), %edx
	cmpl	%edx, %eax
	je	.L126
	.p2align 4,,10
	.p2align 3
.L137:
	movl	$7, %r14d
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L122:
	movl	$65, %r14d
	xorl	%r15d, %r15d
	jmp	.L118
.L151:
	movq	-104(%rbp), %r12
	movq	-112(%rbp), %r13
.L125:
	movq	-88(%rbp), %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	EVP_EncodeFinal@PLT
	movl	-60(%rbp), %edx
	testl	%edx, %edx
	jle	.L130
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	cmpl	-60(%rbp), %eax
	jne	.L137
.L130:
	movl	$9, %edx
	leaq	.LC16(%rip), %rsi
	movq	%r12, %rdi
	movl	$7, %r14d
	call	BIO_write@PLT
	cmpl	$9, %eax
	jne	.L118
	movl	-80(%rbp), %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	cmpl	-80(%rbp), %eax
	jne	.L118
	movl	$6, %edx
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	cmpl	$6, %eax
	jne	.L118
	movl	-92(%rbp), %r12d
	addl	-60(%rbp), %r12d
	jne	.L131
	jmp	.L118
.L133:
	leaq	-60(%rbp), %rax
	movl	$0, -92(%rbp)
	movq	%rax, -88(%rbp)
	jmp	.L125
.L150:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE867:
	.size	PEM_write_bio, .-PEM_write_bio
	.p2align 4
	.globl	PEM_ASN1_write_bio
	.type	PEM_ASN1_write_bio, @function
PEM_ASN1_write_bio:
.LFB862:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1192, %rsp
	movq	32(%rbp), %rax
	movq	24(%rbp), %r14
	movq	%rsi, -1216(%rbp)
	movq	%rdx, -1224(%rbp)
	movq	%rax, -1232(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -1184(%rbp)
	movl	$0, -1180(%rbp)
	testq	%r8, %r8
	je	.L168
	movq	%r8, %rdi
	call	EVP_CIPHER_nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, -1200(%rbp)
	testq	%rax, %rax
	je	.L156
	movq	%r12, %rdi
	call	EVP_CIPHER_iv_length@PLT
	testl	%eax, %eax
	je	.L156
	movq	%r12, %rdi
	call	EVP_CIPHER_iv_length@PLT
	cmpl	$16, %eax
	jle	.L191
.L156:
	movl	$330, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$113, %edx
.L190:
	movl	$105, %esi
	movl	$9, %edi
	xorl	%r15d, %r15d
	call	ERR_put_error@PLT
	leaq	-1088(%rbp), %r13
	xorl	%eax, %eax
	movq	$0, -1208(%rbp)
	leaq	-1168(%rbp), %r14
	leaq	-1152(%rbp), %rbx
	movq	$0, -1192(%rbp)
.L155:
	movl	$64, %esi
	movq	%rbx, %rdi
	movl	%eax, -1200(%rbp)
	call	OPENSSL_cleanse@PLT
	movl	$16, %esi
	movq	%r14, %rdi
	call	OPENSSL_cleanse@PLT
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_free@PLT
	movl	$1024, %esi
	movq	%r13, %rdi
	call	OPENSSL_cleanse@PLT
	movq	-1208(%rbp), %rsi
	movq	-1192(%rbp), %rdi
	movl	$404, %ecx
	leaq	.LC1(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	movl	-1200(%rbp), %eax
	jne	.L192
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L168:
	.cfi_restore_state
	movq	$0, -1200(%rbp)
.L153:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	*%rbx
	testl	%eax, %eax
	js	.L193
	leal	20(%rax), %edi
	movl	$342, %edx
	leaq	.LC1(%rip), %rsi
	movl	%eax, -1208(%rbp)
	call	CRYPTO_malloc@PLT
	movslq	-1208(%rbp), %rcx
	movq	%rax, -1192(%rbp)
	movq	%rcx, -1208(%rbp)
	testq	%rax, %rax
	je	.L194
	movq	%rax, -1176(%rbp)
	leaq	-1176(%rbp), %rsi
	movq	%r13, %rdi
	call	*%rbx
	movl	%eax, -1184(%rbp)
	testq	%r12, %r12
	je	.L159
	leaq	-1088(%rbp), %r13
	testq	%r15, %r15
	je	.L195
.L160:
	movq	%r12, %rdi
	leaq	-1168(%rbp), %r14
	call	EVP_CIPHER_iv_length@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jle	.L170
	call	EVP_md5@PLT
	pushq	$0
	movl	16(%rbp), %r8d
	movq	%r15, %rcx
	leaq	-1152(%rbp), %rbx
	movq	%r14, %rdx
	movq	%rax, %rsi
	movq	%r12, %rdi
	pushq	%rbx
	movl	$1, %r9d
	call	EVP_BytesToKey@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	je	.L171
	cmpq	%r13, %r15
	je	.L196
.L163:
	movb	$0, -1088(%rbp)
	movq	%r13, %rdi
.L164:
	movl	(%rdi), %edx
	addq	$4, %rdi
	leal	-16843009(%rdx), %eax
	notl	%edx
	andl	%edx, %eax
	andl	$-2139062144, %eax
	je	.L164
	movl	%eax, %edx
	movl	$1024, %esi
	leaq	.LC4(%rip), %rcx
	shrl	$16, %edx
	testl	$32896, %eax
	cmove	%edx, %eax
	leaq	2(%rdi), %rdx
	cmove	%rdx, %rdi
	movl	%eax, %edx
	addb	%al, %dl
	leaq	.LC6(%rip), %rdx
	sbbq	$3, %rdi
	xorl	%eax, %eax
	subq	%r13, %rdi
	subq	%rdi, %rsi
	addq	%r13, %rdi
	call	BIO_snprintf@PLT
	movq	%r12, %rdi
	call	EVP_CIPHER_iv_length@PLT
	movq	-1200(%rbp), %rsi
	movq	%r14, %rcx
	movq	%r13, %rdi
	movl	%eax, %edx
	call	PEM_dek_info
	call	EVP_CIPHER_CTX_new@PLT
	movq	%rax, %r15
	xorl	%eax, %eax
	testq	%r15, %r15
	je	.L155
	xorl	%edx, %edx
	movq	%r14, %r8
	movq	%rbx, %rcx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	EVP_EncryptInit_ex@PLT
	testl	%eax, %eax
	je	.L155
	movq	-1192(%rbp), %r12
	movl	-1184(%rbp), %r8d
	leaq	-1180(%rbp), %rdx
	movq	%r15, %rdi
	movq	%r12, %rcx
	movq	%r12, %rsi
	call	EVP_EncryptUpdate@PLT
	testl	%eax, %eax
	je	.L155
	movslq	-1180(%rbp), %rsi
	leaq	-1184(%rbp), %rdx
	movq	%r15, %rdi
	addq	%r12, %rsi
	call	EVP_EncryptFinal_ex@PLT
	testl	%eax, %eax
	je	.L155
	movl	-1180(%rbp), %eax
	addl	-1184(%rbp), %eax
	movl	%eax, -1184(%rbp)
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L195:
	movq	-1232(%rbp), %rcx
	movl	$1, %edx
	movl	$1024, %esi
	movq	%r13, %rdi
	testq	%r14, %r14
	je	.L197
	call	*%r14
	movl	%eax, 16(%rbp)
.L162:
	movl	16(%rbp), %esi
	testl	%esi, %esi
	jle	.L198
	movq	%r13, %r15
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L191:
	movq	-1200(%rbp), %rdi
	call	strlen@PLT
	movq	%r12, %rdi
	movq	%rax, -1192(%rbp)
	call	EVP_CIPHER_iv_length@PLT
	movq	-1192(%rbp), %rdx
	addl	%eax, %eax
	cltq
	leaq	36(%rdx,%rax), %rax
	cmpq	$1024, %rax
	jbe	.L153
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L170:
	xorl	%eax, %eax
	xorl	%r15d, %r15d
	leaq	-1152(%rbp), %rbx
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L193:
	movl	$336, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$13, %edx
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L159:
	movb	$0, -1088(%rbp)
	xorl	%r15d, %r15d
	leaq	-1088(%rbp), %r13
	leaq	-1168(%rbp), %r14
	leaq	-1152(%rbp), %rbx
.L166:
	movq	-1192(%rbp), %rcx
	movq	-1216(%rbp), %rsi
	movslq	%eax, %r8
	movq	%r13, %rdx
	movq	-1224(%rbp), %rdi
	call	PEM_write_bio
	testl	%eax, %eax
	movl	%eax, -1184(%rbp)
	setg	%al
	movzbl	%al, %eax
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L171:
	xorl	%r15d, %r15d
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L194:
	movl	$344, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$65, %edx
	xorl	%r15d, %r15d
	movl	$105, %esi
	movl	$9, %edi
	leaq	-1088(%rbp), %r13
	call	ERR_put_error@PLT
	leaq	-1168(%rbp), %r14
	xorl	%eax, %eax
	leaq	-1152(%rbp), %rbx
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L197:
	call	PEM_def_callback
	movl	%eax, 16(%rbp)
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L198:
	movl	$357, %r8d
	movl	$111, %edx
	movl	$105, %esi
	movl	$9, %edi
	leaq	.LC1(%rip), %rcx
	leaq	-1168(%rbp), %r14
	call	ERR_put_error@PLT
	leaq	-1152(%rbp), %rbx
	xorl	%eax, %eax
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L196:
	movl	$1024, %esi
	movq	%r13, %rdi
	call	OPENSSL_cleanse@PLT
	jmp	.L163
.L192:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE862:
	.size	PEM_ASN1_write_bio, .-PEM_ASN1_write_bio
	.p2align 4
	.globl	PEM_ASN1_write
	.type	PEM_ASN1_write, @function
PEM_ASN1_write:
.LFB861:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	movq	%rsi, -64(%rbp)
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	-56(%rbp), %rdx
	testq	%rax, %rax
	je	.L203
	movq	%rdx, %rcx
	movl	$106, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	movq	%rax, %r13
	call	BIO_ctrl@PLT
	movl	16(%rbp), %eax
	subq	$8, %rsp
	pushq	32(%rbp)
	pushq	24(%rbp)
	movq	%r12, %rdi
	movq	%r15, %r9
	movq	%rbx, %r8
	movq	-64(%rbp), %rsi
	pushq	%rax
	movq	%r14, %rcx
	movq	%r13, %rdx
	call	PEM_ASN1_write_bio
	addq	$32, %rsp
	movq	%r13, %rdi
	movl	%eax, %r12d
	call	BIO_free@PLT
.L199:
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L203:
	.cfi_restore_state
	movl	$298, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$7, %edx
	xorl	%r12d, %r12d
	movl	$104, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
	jmp	.L199
	.cfi_endproc
.LFE861:
	.size	PEM_ASN1_write, .-PEM_ASN1_write
	.p2align 4
	.globl	PEM_write
	.type	PEM_write, @function
PEM_write:
.LFB866:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$24, %rsp
	movq	%rdi, -56(%rbp)
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	-56(%rbp), %r9
	testq	%rax, %rax
	je	.L208
	movq	%rax, %r12
	movq	%r9, %rcx
	xorl	%edx, %edx
	movl	$106, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	%r14, %rdx
	call	PEM_write_bio
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	BIO_free@PLT
.L204:
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	.cfi_restore_state
	movl	$592, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$7, %edx
	xorl	%r13d, %r13d
	movl	$113, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
	jmp	.L204
	.cfi_endproc
.LFE866:
	.size	PEM_write, .-PEM_write
	.p2align 4
	.globl	PEM_read_bio_ex
	.type	PEM_read_bio_ex, @function
PEM_read_bio_ex:
.LFB872:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -152(%rbp)
	movq	%rdx, -144(%rbp)
	movq	%rcx, -136(%rbp)
	movq	%r8, -160(%rbp)
	movl	%r9d, -92(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_ENCODE_CTX_new@PLT
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L308
	movq	-160(%rbp), %rax
	movq	$0, (%rax)
	movq	-144(%rbp), %rax
	movq	$0, (%rax)
	movq	-152(%rbp), %rax
	movq	$0, (%rax)
	movq	-136(%rbp), %rax
	movq	$0, (%rax)
	movl	-92(%rbp), %eax
	movl	%eax, %ecx
	andl	$6, %eax
	andl	$1, %ecx
	movl	%ecx, -88(%rbp)
	cmpl	$6, %eax
	je	.L309
	movl	-88(%rbp), %eax
	testl	%eax, %eax
	jne	.L310
	call	BIO_s_mem@PLT
	movq	%rax, %r12
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%r12, %rdi
	movq	%rax, -120(%rbp)
	movq	%rax, %rbx
	call	BIO_new@PLT
	movq	%rax, -104(%rbp)
	testq	%rbx, %rbx
	je	.L215
	testq	%rax, %rax
	je	.L215
	movl	$229, %edx
	leaq	.LC1(%rip), %rsi
	movl	$256, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
.L219:
	movl	-92(%rbp), %r15d
	andl	$-5, %r15d
	testq	%r12, %r12
	je	.L311
	.p2align 4,,10
	.p2align 3
.L282:
	movq	%r12, %rsi
	movl	$255, %edx
	movq	%r14, %rdi
	call	BIO_gets@PLT
	movl	%eax, %esi
	testl	%eax, %eax
	jle	.L312
	movl	%r15d, %edx
	movq	%r12, %rdi
	call	sanitize_line
	movl	$11, %edx
	leaq	beginstr(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	strncmp@PLT
	testl	%eax, %eax
	jne	.L282
	cmpl	$5, %ebx
	jle	.L282
	movslq	%ebx, %rax
	movl	$6, %edx
	leaq	tailstr(%rip), %rsi
	leaq	-6(%r12,%rax), %r13
	movq	%r13, %rdi
	call	strncmp@PLT
	testl	%eax, %eax
	jne	.L282
	movq	%r13, %rcx
	movl	%eax, %r13d
	movl	-88(%rbp), %eax
	subl	$16, %ebx
	movb	$0, (%rcx)
	movslq	%ebx, %rbx
	testl	%eax, %eax
	jne	.L313
	movl	$229, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rbx, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, -128(%rbp)
	testq	%rax, %rax
	je	.L314
	leaq	11(%r12), %rsi
	movq	%rax, %rdi
	movq	%rbx, %rdx
	call	memcpy@PLT
	movq	%r12, %rdi
	movl	$223, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$229, %edx
	movl	$256, %edi
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
.L267:
	movq	-120(%rbp), %rax
	movl	$0, -96(%rbp)
	movl	$0, -84(%rbp)
	movq	%rax, -168(%rbp)
	testq	%r12, %r12
	je	.L315
	movq	%r14, %rax
	movq	%r12, %r14
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L229:
	movl	$255, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	BIO_gets@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	jle	.L316
	movl	-84(%rbp), %r11d
	testl	%r11d, %r11d
	jne	.L232
	movslq	%eax, %rdx
	movl	$58, %esi
	movq	%r14, %rdi
	call	memchr@PLT
	testq	%rax, %rax
	je	.L317
	movl	$1, -84(%rbp)
.L233:
	movl	%r15d, %edx
.L307:
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	sanitize_line
	cmpb	$10, (%r14)
	movl	%eax, %ebx
	je	.L273
.L237:
	movl	$9, %edx
	leaq	endstr(%rip), %rsi
	movq	%r14, %rdi
	call	strncmp@PLT
	testl	%eax, %eax
	je	.L318
	movl	-96(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L319
	movq	-168(%rbp), %rdi
	movq	%r14, %rsi
	call	BIO_puts@PLT
	testl	%eax, %eax
	js	.L302
	cmpl	$2, -84(%rbp)
	jne	.L229
	cmpl	$65, %ebx
	jg	.L302
	setne	%al
	movzbl	%al, %eax
	movl	%eax, -96(%rbp)
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L310:
	call	BIO_s_secmem@PLT
	movq	%rax, %r12
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%r12, %rdi
	movq	%rax, -120(%rbp)
	movq	%rax, %rbx
	call	BIO_new@PLT
	movq	%rax, -104(%rbp)
	testq	%rbx, %rbx
	je	.L215
	testq	%rax, %rax
	je	.L215
	movl	$228, %edx
	leaq	.LC1(%rip), %rsi
	movl	$256, %edi
	call	CRYPTO_secure_malloc@PLT
	movq	%rax, %r12
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L312:
	movl	$745, %r8d
	movl	$108, %edx
	movl	$144, %esi
	movl	$9, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	movl	-88(%rbp), %eax
	testl	%eax, %eax
	jne	.L222
	movl	$223, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	movq	$0, -128(%rbp)
.L223:
	movq	-112(%rbp), %rdi
	xorl	%r13d, %r13d
	call	EVP_ENCODE_CTX_free@PLT
.L253:
	movq	-128(%rbp), %rdi
	movl	$223, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L215:
	movl	$913, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$145, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
	movq	$0, -128(%rbp)
.L213:
	movq	-112(%rbp), %rdi
	call	EVP_ENCODE_CTX_free@PLT
	movl	-88(%rbp), %esi
	testl	%esi, %esi
	je	.L253
.L252:
	movq	-128(%rbp), %rdi
	movl	$221, %ecx
	leaq	.LC1(%rip), %rdx
	xorl	%esi, %esi
	call	CRYPTO_secure_clear_free@PLT
.L254:
	movq	-120(%rbp), %rdi
	call	BIO_free@PLT
	movq	-104(%rbp), %rdi
	call	BIO_free@PLT
.L209:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L320
	addq	$136, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L321:
	.cfi_restore_state
	movl	$760, %r8d
	movl	$65, %edx
	movl	$144, %esi
	movl	$9, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L222:
	movl	$221, %ecx
	movl	$256, %esi
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	leaq	.LC1(%rip), %rdx
	call	CRYPTO_secure_clear_free@PLT
	movq	$0, -128(%rbp)
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L309:
	movl	$905, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$7, %edx
	xorl	%r13d, %r13d
	movl	$145, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
	movq	$0, -128(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -120(%rbp)
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L308:
	movl	$896, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$145, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L311:
	movl	$737, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$144, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
	movq	$0, -128(%rbp)
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L313:
	movl	$228, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rbx, %rdi
	call	CRYPTO_secure_malloc@PLT
	movq	%rax, -128(%rbp)
	testq	%rax, %rax
	je	.L321
	leaq	11(%r12), %rsi
	movq	%rax, %rdi
	movq	%rbx, %rdx
	call	memcpy@PLT
	movq	%r12, %rdi
	movl	$221, %ecx
	leaq	.LC1(%rip), %rdx
	movl	$256, %esi
	call	CRYPTO_secure_clear_free@PLT
	movl	$228, %edx
	movl	$256, %edi
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_secure_malloc@PLT
	movq	%rax, %r12
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L232:
	movl	$9, %edx
	leaq	endstr(%rip), %rsi
	movq	%r14, %rdi
	call	strncmp@PLT
	testl	%eax, %eax
	je	.L235
	cmpl	$1, -84(%rbp)
	je	.L233
	movl	-92(%rbp), %edx
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	sanitize_line
	cmpb	$10, (%r14)
	movl	%eax, %ebx
	je	.L255
	movl	$2, -84(%rbp)
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L273:
	movq	-104(%rbp), %rax
	movl	$2, -84(%rbp)
	movq	%rax, -168(%rbp)
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L235:
	movl	%ebx, %esi
	movl	%r15d, %edx
	movq	%r14, %rdi
	call	sanitize_line
	cmpb	$10, (%r14)
	movl	%eax, %ebx
	jne	.L237
	cmpl	$2, -84(%rbp)
	jne	.L273
	.p2align 4,,10
	.p2align 3
.L255:
	movq	%r14, %r12
	movl	$828, %r8d
.L304:
	movl	$102, %edx
	movl	$143, %esi
	movl	$9, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
.L231:
	movl	-88(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L322
	movq	%r12, %rdi
	movl	$223, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-112(%rbp), %rdi
	call	EVP_ENCODE_CTX_free@PLT
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L317:
	movl	$9, %edx
	leaq	endstr(%rip), %rsi
	movq	%r14, %rdi
	call	strncmp@PLT
	testl	%eax, %eax
	je	.L233
	movl	-92(%rbp), %edx
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L322:
	movq	%r12, %rdi
	movl	$221, %ecx
	movl	$256, %esi
	leaq	.LC1(%rip), %rdx
	call	CRYPTO_secure_clear_free@PLT
	movq	-112(%rbp), %rdi
	call	EVP_ENCODE_CTX_free@PLT
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L316:
	movl	$812, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$112, %edx
	movq	%r14, %r12
	movl	$143, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L302:
	movq	%r14, %r12
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L315:
	movl	$804, %r8d
	movl	$65, %edx
	movl	$143, %esi
	movl	$9, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L318:
	movq	-128(%rbp), %r15
	leaq	9(%r14), %rbx
	movq	%r14, %r12
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	movq	%rax, %r14
	call	strncmp@PLT
	testl	%eax, %eax
	jne	.L241
	leaq	(%rbx,%r14), %rdi
	movl	$6, %edx
	leaq	tailstr(%rip), %rsi
	call	strncmp@PLT
	testl	%eax, %eax
	jne	.L241
	movl	-84(%rbp), %r10d
	testl	%r10d, %r10d
	jne	.L323
	movl	-88(%rbp), %edx
	testl	%edx, %edx
	jne	.L324
	movl	$223, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	movq	-104(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-168(%rbp), %rax
	movq	%rax, -104(%rbp)
.L262:
	movq	-112(%rbp), %rbx
	movq	%rbx, %rdi
	call	EVP_DecodeInit@PLT
	movq	-104(%rbp), %rdi
	xorl	%edx, %edx
	leaq	-64(%rbp), %rcx
	movl	$115, %esi
	call	BIO_ctrl@PLT
	movq	-64(%rbp), %rax
	leaq	-72(%rbp), %rdx
	movq	%rbx, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %r8
	movq	%rsi, %rcx
	movl	%r8d, -72(%rbp)
	call	EVP_DecodeUpdate@PLT
	testl	%eax, %eax
	js	.L247
	movq	-64(%rbp), %rax
	movq	-112(%rbp), %rdi
	leaq	-68(%rbp), %rdx
	movslq	-72(%rbp), %rsi
	addq	8(%rax), %rsi
	call	EVP_DecodeFinal@PLT
	testl	%eax, %eax
	js	.L247
	movl	-68(%rbp), %r13d
	movq	-64(%rbp), %rax
	addl	-72(%rbp), %r13d
	movslq	%r13d, %rdx
	movl	%r13d, -72(%rbp)
	movq	%rdx, (%rax)
	je	.L213
	movq	-120(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$3, %esi
	call	BIO_ctrl@PLT
	movl	-88(%rbp), %r8d
	leal	1(%rax), %edi
	movq	%rax, %rbx
	movslq	%edi, %rdi
	testl	%r8d, %r8d
	je	.L248
	movl	$228, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_secure_malloc@PLT
	movq	-144(%rbp), %r15
	movslq	-72(%rbp), %rdi
	movl	$228, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, (%r15)
	call	CRYPTO_secure_malloc@PLT
	movq	-136(%rbp), %rcx
	movq	(%r15), %r8
	movq	%rax, (%rcx)
	testq	%rax, %rax
	je	.L249
	testq	%r8, %r8
	je	.L249
.L250:
	movq	-120(%rbp), %rdi
	movl	%ebx, %edx
	movq	%r8, %rsi
	movslq	%ebx, %rbx
	movl	$1, %r13d
	call	BIO_read@PLT
	movq	-144(%rbp), %rax
	movq	-104(%rbp), %rdi
	movq	(%rax), %rax
	movb	$0, (%rax,%rbx)
	movq	-136(%rbp), %rax
	movl	-72(%rbp), %edx
	movq	(%rax), %rsi
	call	BIO_read@PLT
	movslq	-72(%rbp), %rax
	movq	-160(%rbp), %rcx
	movq	%rax, (%rcx)
	movq	-128(%rbp), %rcx
	movq	-152(%rbp), %rax
	movq	$0, -128(%rbp)
	movq	%rcx, (%rax)
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L247:
	movl	$929, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$100, %edx
	xorl	%r13d, %r13d
	movl	$145, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L319:
	movq	%r14, %r12
	movl	$852, %r8d
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L248:
	movl	$229, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	-144(%rbp), %r15
	movslq	-72(%rbp), %rdi
	movl	$229, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, (%r15)
	call	CRYPTO_malloc@PLT
	movq	-136(%rbp), %rcx
	movq	(%r15), %r8
	movq	%rax, (%rcx)
	testq	%r8, %r8
	je	.L251
	testq	%rax, %rax
	jne	.L250
.L251:
	movq	%r8, %rdi
	movl	$223, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-136(%rbp), %rax
	movl	$223, %edx
	leaq	.LC1(%rip), %rsi
	movq	(%rax), %rdi
	call	CRYPTO_free@PLT
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L241:
	movl	$842, %r8d
	jmp	.L304
.L314:
	movl	$760, %r8d
	movl	$65, %edx
	movl	$144, %esi
	movl	$9, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$223, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	jmp	.L223
.L249:
	movq	%r8, %rdi
	movl	$221, %ecx
	xorl	%esi, %esi
	xorl	%r13d, %r13d
	leaq	.LC1(%rip), %rdx
	call	CRYPTO_secure_clear_free@PLT
	movq	-136(%rbp), %rax
	movl	$221, %ecx
	xorl	%esi, %esi
	leaq	.LC1(%rip), %rdx
	movq	(%rax), %rdi
	call	CRYPTO_secure_clear_free@PLT
	movq	-112(%rbp), %rdi
	call	EVP_ENCODE_CTX_free@PLT
	jmp	.L252
.L323:
	movl	-88(%rbp), %eax
	testl	%eax, %eax
	je	.L325
	movl	$221, %ecx
	leaq	.LC1(%rip), %rdx
	movl	$256, %esi
	movq	%r12, %rdi
	call	CRYPTO_secure_clear_free@PLT
	jmp	.L262
.L320:
	call	__stack_chk_fail@PLT
.L324:
	movl	$221, %ecx
	leaq	.LC1(%rip), %rdx
	movl	$256, %esi
	movq	%r12, %rdi
	call	CRYPTO_secure_clear_free@PLT
	movq	-104(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-168(%rbp), %rax
	movq	%rax, -104(%rbp)
	jmp	.L262
.L325:
	movl	$223, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	jmp	.L262
	.cfi_endproc
.LFE872:
	.size	PEM_read_bio_ex, .-PEM_read_bio_ex
	.p2align 4
	.globl	PEM_read
	.type	PEM_read, @function
PEM_read:
.LFB868:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$24, %rsp
	movq	%rdi, -56(%rbp)
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	-56(%rbp), %r9
	testq	%rax, %rax
	je	.L330
	movq	%rax, %r12
	movq	%r9, %rcx
	xorl	%edx, %edx
	movl	$106, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rbx, %r8
	movl	$2, %r9d
	movq	%r15, %rcx
	movq	%r14, %rdx
	call	PEM_read_bio_ex
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	BIO_free@PLT
.L326:
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L330:
	.cfi_restore_state
	movl	$672, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$7, %edx
	xorl	%r13d, %r13d
	movl	$108, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
	jmp	.L326
	.cfi_endproc
.LFE868:
	.size	PEM_read, .-PEM_read
	.section	.rodata.str1.1
.LC17:
	.string	"Expecting: "
.LC18:
	.string	"ANY PRIVATE KEY"
.LC19:
	.string	"ENCRYPTED PRIVATE KEY"
.LC20:
	.string	"PRIVATE KEY"
.LC21:
	.string	"PARAMETERS"
.LC22:
	.string	"X9.42 DH PARAMETERS"
.LC23:
	.string	"DH PARAMETERS"
.LC24:
	.string	"X509 CERTIFICATE"
.LC25:
	.string	"CERTIFICATE"
.LC26:
	.string	"NEW CERTIFICATE REQUEST"
.LC27:
	.string	"CERTIFICATE REQUEST"
.LC28:
	.string	"TRUSTED CERTIFICATE"
.LC29:
	.string	"PKCS7"
.LC30:
	.string	"CMS"
.LC31:
	.string	"PKCS #7 SIGNED DATA"
	.text
	.p2align 4
	.type	pem_bytes_read_bio_flags, @function
pem_bytes_read_bio_flags:
.LFB858:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-104(%rbp), %r15
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$152, %rsp
	movq	16(%rbp), %rax
	movq	%rdi, -176(%rbp)
	movq	%rsi, -184(%rbp)
	movq	%rdx, -192(%rbp)
	movq	%r9, -160(%rbp)
	movq	%rax, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	24(%rbp), %eax
	movq	$0, -120(%rbp)
	movq	$0, -112(%rbp)
	andl	$1, %eax
	movq	$0, -104(%rbp)
	movl	%eax, -132(%rbp)
	leaq	-112(%rbp), %rax
	movq	%rax, -152(%rbp)
	leaq	-120(%rbp), %rax
	movq	$0, -96(%rbp)
	movq	%rax, -144(%rbp)
.L332:
	movl	-132(%rbp), %edi
	testl	%edi, %edi
	je	.L333
.L423:
	movl	$221, %ecx
	leaq	.LC1(%rip), %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	CRYPTO_secure_clear_free@PLT
	movq	-112(%rbp), %rdi
	movl	$221, %ecx
	xorl	%esi, %esi
	leaq	.LC1(%rip), %rdx
	call	CRYPTO_secure_clear_free@PLT
	movq	-96(%rbp), %rsi
	movq	-104(%rbp), %rdi
	movl	$221, %ecx
	leaq	.LC1(%rip), %rdx
	call	CRYPTO_secure_clear_free@PLT
.L334:
	movl	24(%rbp), %r9d
	movq	%r13, %r8
	movq	%r15, %rcx
	movq	%rbx, %rdi
	movq	-152(%rbp), %rdx
	movq	-144(%rbp), %rsi
	call	PEM_read_bio_ex
	testl	%eax, %eax
	je	.L422
	movq	-120(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L338
	movl	$16, %ecx
	movq	%r12, %rsi
	leaq	.LC18(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L339
	movl	$22, %ecx
	leaq	.LC19(%rip), %rdi
	movq	%r14, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L338
	leaq	.LC20(%rip), %rdx
	movl	$12, %ecx
	movq	%r14, %rsi
	movq	%rdx, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L338
	movq	%r14, %rdi
	call	strlen@PLT
	cmpl	$12, %eax
	jle	.L332
	cltq
	leaq	.LC20(%rip), %rdx
	movl	$12, %ecx
	leaq	-11(%r14,%rax), %rax
	movq	%rdx, %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	jne	.L332
	cmpb	$32, -1(%rax)
	jne	.L332
	leaq	-1(%rax), %rdx
	subq	%r14, %rdx
	testl	%edx, %edx
	jle	.L332
	xorl	%edi, %edi
	movq	%r14, %rsi
	call	EVP_PKEY_asn1_find_str@PLT
	testq	%rax, %rax
	je	.L419
	cmpq	$0, 184(%rax)
	jne	.L338
.L419:
	movq	-120(%rbp), %r14
.L427:
	movl	-132(%rbp), %edi
	testl	%edi, %edi
	jne	.L423
	.p2align 4,,10
	.p2align 3
.L333:
	movq	%r14, %rdi
	movl	$223, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-112(%rbp), %rdi
	movl	$223, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-104(%rbp), %rdi
	movl	$223, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L339:
	movl	$11, %ecx
	movq	%r12, %rsi
	leaq	.LC21(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L344
	movq	%r14, %rdi
	call	strlen@PLT
	cmpl	$11, %eax
	jle	.L332
	cltq
	movl	$11, %ecx
	leaq	.LC21(%rip), %rdi
	leaq	-10(%r14,%rax), %rax
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	jne	.L332
	cmpb	$32, -1(%rax)
	jne	.L332
	leaq	-1(%rax), %rdx
	subq	%r14, %rdx
	testl	%edx, %edx
	jle	.L332
	leaq	-88(%rbp), %rdi
	movq	%r14, %rsi
	call	EVP_PKEY_asn1_find_str@PLT
	testq	%rax, %rax
	je	.L419
	cmpq	$0, 112(%rax)
	movq	-88(%rbp), %rdi
	je	.L424
	call	ENGINE_finish@PLT
	.p2align 4,,10
	.p2align 3
.L338:
	leaq	-80(%rbp), %r12
	movq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	PEM_get_EVP_CIPHER_INFO
	testl	%eax, %eax
	jne	.L425
	movl	-132(%rbp), %ecx
	movq	-120(%rbp), %rdi
	testl	%ecx, %ecx
	jne	.L367
.L407:
	movl	$223, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-112(%rbp), %rdi
	movl	$223, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-104(%rbp), %rdi
	movl	$223, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L331:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L426
	addq	$152, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L344:
	.cfi_restore_state
	movl	$20, %ecx
	movq	%r14, %rsi
	leaq	.LC22(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L347
	movl	$14, %ecx
	leaq	.LC23(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L338
.L347:
	movl	$17, %ecx
	movq	%r14, %rsi
	leaq	.LC24(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	movsbl	%al, %eax
	testl	%eax, %eax
	jne	.L348
	movl	$12, %ecx
	leaq	.LC25(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L338
.L348:
	movl	$24, %ecx
	leaq	.LC26(%rip), %rdi
	movq	%r14, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	jne	.L349
	movl	$20, %ecx
	leaq	.LC27(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L338
.L349:
	movl	$12, %ecx
	leaq	.LC25(%rip), %rdi
	movq	%r14, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	jne	.L350
	movl	$20, %ecx
	leaq	.LC28(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L338
	leaq	.LC29(%rip), %rax
	movl	$6, %ecx
	movq	%r12, %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L338
.L421:
	movl	$4, %ecx
	leaq	.LC30(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L332
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L422:
	movl	%eax, -132(%rbp)
	call	ERR_peek_error@PLT
	movl	-132(%rbp), %r8d
	andl	$4095, %eax
	cmpl	$108, %eax
	jne	.L331
	movq	%r12, %rdx
	leaq	.LC17(%rip), %rsi
	movl	$2, %edi
	xorl	%eax, %eax
	call	ERR_add_error_data@PLT
	movl	-132(%rbp), %r8d
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L350:
	testl	%eax, %eax
	jne	.L361
	movl	$20, %ecx
	leaq	.LC28(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L338
.L361:
	movl	$20, %ecx
	leaq	.LC31(%rip), %rdi
	movq	%r14, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	leaq	.LC29(%rip), %rax
	jne	.L359
	movl	$6, %ecx
	movq	%r12, %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L338
.L359:
	movq	%rax, %rdi
	movl	$6, %ecx
	movq	%r14, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L332
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L425:
	movq	-168(%rbp), %r8
	movq	-160(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	-104(%rbp), %rsi
	call	PEM_do_header
	testl	%eax, %eax
	je	.L353
	movq	-104(%rbp), %rax
	movq	-176(%rbp), %rbx
	movq	-120(%rbp), %rdi
	movq	%rax, (%rbx)
	movq	-96(%rbp), %rax
	movq	-184(%rbp), %rbx
	movq	%rax, (%rbx)
	movq	-192(%rbp), %rax
	testq	%rax, %rax
	je	.L354
	movl	-132(%rbp), %esi
	movq	%rdi, (%rax)
	movq	-112(%rbp), %rdi
	testl	%esi, %esi
	je	.L404
	movl	$221, %ecx
	leaq	.LC1(%rip), %rdx
	xorl	%esi, %esi
	call	CRYPTO_secure_clear_free@PLT
	movl	$1, %r8d
	jmp	.L331
.L353:
	movl	-132(%rbp), %eax
	movq	-120(%rbp), %rdi
	testl	%eax, %eax
	je	.L407
.L367:
	movl	$221, %ecx
	leaq	.LC1(%rip), %rdx
	xorl	%esi, %esi
	call	CRYPTO_secure_clear_free@PLT
	movq	-112(%rbp), %rdi
	movl	$221, %ecx
	xorl	%esi, %esi
	leaq	.LC1(%rip), %rdx
	call	CRYPTO_secure_clear_free@PLT
	movq	-96(%rbp), %rsi
	movq	-104(%rbp), %rdi
	movl	$221, %ecx
	leaq	.LC1(%rip), %rdx
	call	CRYPTO_secure_clear_free@PLT
	xorl	%r8d, %r8d
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L424:
	call	ENGINE_finish@PLT
	movq	-120(%rbp), %r14
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L404:
	movl	$223, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$1, %r8d
	jmp	.L331
.L354:
	movl	-132(%rbp), %edx
	testl	%edx, %edx
	je	.L428
	movl	$221, %ecx
	leaq	.LC1(%rip), %rdx
	xorl	%esi, %esi
	call	CRYPTO_secure_clear_free@PLT
	movq	-112(%rbp), %rdi
	movl	$221, %ecx
	xorl	%esi, %esi
	leaq	.LC1(%rip), %rdx
	call	CRYPTO_secure_clear_free@PLT
	movl	$1, %r8d
	jmp	.L331
.L428:
	movl	$223, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-112(%rbp), %rdi
	movl	$223, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$1, %r8d
	jmp	.L331
.L426:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE858:
	.size	pem_bytes_read_bio_flags, .-pem_bytes_read_bio_flags
	.p2align 4
	.globl	PEM_bytes_read_bio
	.type	PEM_bytes_read_bio, @function
PEM_bytes_read_bio:
.LFB859:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	$2
	pushq	16(%rbp)
	call	pem_bytes_read_bio_flags
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE859:
	.size	PEM_bytes_read_bio, .-PEM_bytes_read_bio
	.p2align 4
	.globl	PEM_bytes_read_bio_secmem
	.type	PEM_bytes_read_bio_secmem, @function
PEM_bytes_read_bio_secmem:
.LFB860:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	$3
	pushq	16(%rbp)
	call	pem_bytes_read_bio_flags
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE860:
	.size	PEM_bytes_read_bio_secmem, .-PEM_bytes_read_bio_secmem
	.p2align 4
	.globl	PEM_read_bio
	.type	PEM_read_bio, @function
PEM_read_bio:
.LFB873:
	.cfi_startproc
	endbr64
	movl	$2, %r9d
	jmp	PEM_read_bio_ex
	.cfi_endproc
.LFE873:
	.size	PEM_read_bio, .-PEM_read_bio
	.p2align 4
	.globl	pem_check_suffix
	.type	pem_check_suffix, @function
pem_check_suffix:
.LFB874:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	strlen@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	strlen@PLT
	xorl	%r8d, %r8d
	leal	1(%rax), %edx
	cmpl	%ebx, %edx
	jge	.L434
	cltq
	movslq	%ebx, %rbx
	movq	%r12, %rsi
	subq	%rax, %rbx
	addq	%r13, %rbx
	movq	%rbx, %rdi
	call	strcmp@PLT
	movl	%eax, %r8d
	testl	%eax, %eax
	jne	.L437
	cmpb	$32, -1(%rbx)
	jne	.L434
	leaq	-1(%rbx), %r8
	subl	%r13d, %r8d
.L434:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L437:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%r8d, %r8d
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE874:
	.size	pem_check_suffix, .-pem_check_suffix
	.section	.rodata
	.align 8
	.type	DEKInfo.19087, @object
	.size	DEKInfo.19087, 10
DEKInfo.19087:
	.string	"DEK-Info:"
	.align 8
	.type	ENCRYPTED.19086, @object
	.size	ENCRYPTED.19086, 10
ENCRYPTED.19086:
	.string	"ENCRYPTED"
	.align 8
	.type	ProcType.19085, @object
	.size	ProcType.19085, 11
ProcType.19085:
	.string	"Proc-Type:"
	.type	tailstr, @object
	.size	tailstr, 7
tailstr:
	.string	"-----\n"
	.align 8
	.type	endstr, @object
	.size	endstr, 10
endstr:
	.string	"-----END "
	.align 8
	.type	beginstr, @object
	.size	beginstr, 12
beginstr:
	.string	"-----BEGIN "
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
