	.file	"p12_mutl.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/pkcs12/p12_mutl.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"LEGACY_GOST_PKCS12"
	.text
	.p2align 4
	.type	pkcs12_gen_mac.isra.0, @function
pkcs12_gen_mac.isra.0:
.LFB811:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$264, %rsp
	movq	%rdx, -256(%rbp)
	movq	16(%rbp), %r14
	movl	%ecx, -260(%rbp)
	movq	%r8, -280(%rbp)
	movq	%r9, -288(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r14, %r14
	je	.L30
.L2:
	movq	(%rbx), %rax
	movq	24(%rax), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$21, %eax
	jne	.L31
	movl	$1, -248(%rbp)
	movq	(%r12), %rax
	movq	8(%rax), %rdx
	movq	16(%rax), %rdi
	movq	8(%rdx), %rcx
	movq	%rcx, -272(%rbp)
	movl	(%rdx), %ecx
	movl	%ecx, -264(%rbp)
	testq	%rdi, %rdi
	je	.L5
	call	ASN1_INTEGER_get@PLT
	movl	%eax, -248(%rbp)
	movq	(%r12), %rax
.L5:
	movq	(%rax), %rdi
	leaq	-240(%rbp), %rsi
	xorl	%edx, %edx
	call	X509_SIG_get0@PLT
	movq	-240(%rbp), %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	-232(%rbp), %rdi
	call	X509_ALGOR_get0@PLT
	movq	-232(%rbp), %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L32
	movq	%rax, %rdi
	call	EVP_MD_size@PLT
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	EVP_MD_type@PLT
	movl	%eax, %edx
	xorl	%eax, %eax
	testl	%r13d, %r13d
	js	.L1
	leal	-982(%rdx), %eax
	cmpl	$1, %eax
	jbe	.L16
	cmpl	$809, %edx
	je	.L16
.L7:
	subq	$8, %rsp
	leaq	-224(%rbp), %r15
	movl	-248(%rbp), %r9d
	movl	-264(%rbp), %ecx
	pushq	%r12
	movq	-272(%rbp), %rdx
	movl	$3, %r8d
	movl	-260(%rbp), %esi
	movq	-256(%rbp), %rdi
	pushq	%r15
	pushq	%r13
	call	*%r14
	addq	$32, %rsp
	testl	%eax, %eax
	je	.L33
.L10:
	call	HMAC_CTX_new@PLT
	movq	%rax, %r10
	xorl	%eax, %eax
	testq	%r10, %r10
	je	.L11
	xorl	%r8d, %r8d
	movq	%r10, %rdi
	movq	%r12, %rcx
	movl	%r13d, %edx
	movq	%r15, %rsi
	movq	%r10, -248(%rbp)
	call	HMAC_Init_ex@PLT
	movq	-248(%rbp), %r10
	testl	%eax, %eax
	jne	.L34
.L11:
	movl	$64, %esi
	movq	%r15, %rdi
	movl	%eax, -256(%rbp)
	movq	%r10, -248(%rbp)
	call	OPENSSL_cleanse@PLT
	movq	-248(%rbp), %r10
	movq	%r10, %rdi
	call	HMAC_CTX_free@PLT
	movl	-256(%rbp), %eax
.L1:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L35
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	leaq	.LC1(%rip), %rdi
	call	ossl_safe_getenv@PLT
	testq	%rax, %rax
	jne	.L7
	leaq	-160(%rbp), %r13
	movl	-264(%rbp), %ecx
	movq	%r12, %r9
	movq	-272(%rbp), %rdx
	movl	-248(%rbp), %r8d
	movl	-260(%rbp), %esi
	pushq	%r13
	pushq	$96
	movq	-256(%rbp), %rdi
	movq	%rax, -296(%rbp)
	call	PKCS5_PBKDF2_HMAC@PLT
	popq	%rdx
	movq	-296(%rbp), %r10
	testl	%eax, %eax
	popq	%rcx
	je	.L9
	movdqa	-96(%rbp), %xmm0
	movdqa	-80(%rbp), %xmm1
	movq	%r13, %rdi
	movl	$96, %esi
	movl	$32, %r13d
	leaq	-224(%rbp), %r15
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm1, -208(%rbp)
	call	OPENSSL_cleanse@PLT
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L31:
	movl	$92, %r8d
	movl	$121, %edx
	movl	$107, %esi
	movl	$35, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L30:
	movq	PKCS12_key_gen_utf8@GOTPCREL(%rip), %r14
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L34:
	movq	(%rbx), %rax
	movq	%r10, %rdi
	movq	32(%rax), %rax
	movslq	(%rax), %rdx
	movq	8(%rax), %rsi
	call	HMAC_Update@PLT
	movq	-248(%rbp), %r10
	testl	%eax, %eax
	je	.L11
	movq	-288(%rbp), %rdx
	movq	-280(%rbp), %rsi
	movq	%r10, %rdi
	call	HMAC_Final@PLT
	movq	-248(%rbp), %r10
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L32:
	movl	$105, %r8d
	movl	$118, %edx
	movl	$107, %esi
	movl	$35, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L33:
	movl	$125, %r8d
	movl	$107, %edx
	movl	$107, %esi
	movl	$35, %edi
	leaq	.LC0(%rip), %rcx
	movl	%eax, -248(%rbp)
	call	ERR_put_error@PLT
	movl	-248(%rbp), %eax
	xorl	%r10d, %r10d
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$119, %r8d
	movl	$107, %edx
	movl	$107, %esi
	movl	$35, %edi
	leaq	.LC0(%rip), %rcx
	movl	%eax, -256(%rbp)
	leaq	-224(%rbp), %r15
	movq	%r10, -248(%rbp)
	call	ERR_put_error@PLT
	movq	-248(%rbp), %r10
	movl	-256(%rbp), %eax
	jmp	.L11
.L35:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE811:
	.size	pkcs12_gen_mac.isra.0, .-pkcs12_gen_mac.isra.0
	.p2align 4
	.globl	PKCS12_mac_present
	.type	PKCS12_mac_present, @function
PKCS12_mac_present:
.LFB803:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	$0, 8(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE803:
	.size	PKCS12_mac_present, .-PKCS12_mac_present
	.p2align 4
	.globl	PKCS12_get0_mac
	.type	PKCS12_get0_mac, @function
PKCS12_get0_mac:
.LFB804:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rcx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	8(%r8), %rax
	testq	%rax, %rax
	je	.L38
	movq	%rdi, %rdx
	movq	(%rax), %rdi
	movq	%r8, %rbx
	call	X509_SIG_get0@PLT
	testq	%r13, %r13
	je	.L39
	movq	8(%rbx), %rax
	movq	8(%rax), %rax
	movq	%rax, 0(%r13)
.L39:
	testq	%r12, %r12
	je	.L37
	movq	8(%rbx), %rax
	movq	16(%rax), %rax
	movq	%rax, (%r12)
.L37:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	testq	%rdi, %rdi
	je	.L42
	movq	$0, (%rdi)
.L42:
	testq	%rsi, %rsi
	je	.L43
	movq	$0, (%rsi)
.L43:
	testq	%r13, %r13
	je	.L44
	movq	$0, 0(%r13)
.L44:
	testq	%r12, %r12
	je	.L37
	movq	$0, (%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE804:
	.size	PKCS12_get0_mac, .-PKCS12_get0_mac
	.p2align 4
	.globl	PKCS12_gen_mac
	.type	PKCS12_gen_mac, @function
PKCS12_gen_mac:
.LFB807:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	movq	%r8, %r9
	leaq	16(%rdi), %rsi
	movq	%rcx, %r8
	addq	$8, %rdi
	movl	%edx, %ecx
	movq	%r10, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	call	pkcs12_gen_mac.isra.0
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE807:
	.size	PKCS12_gen_mac, .-PKCS12_gen_mac
	.p2align 4
	.globl	PKCS12_verify_mac
	.type	PKCS12_verify_mac, @function
PKCS12_verify_mac:
.LFB808:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 8(%rdi)
	je	.L71
	subq	$8, %rsp
	pushq	PKCS12_key_gen_utf8@GOTPCREL(%rip)
	leaq	16(%rdi), %r10
	movl	%edx, %ecx
	leaq	-96(%rbp), %r12
	movq	%rsi, %rdx
	movq	%rdi, %rbx
	movq	%r10, %rsi
	leaq	8(%rdi), %rdi
	leaq	-108(%rbp), %r9
	movq	%r12, %r8
	call	pkcs12_gen_mac.isra.0
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	je	.L72
	movq	8(%rbx), %rax
	leaq	-104(%rbp), %rdx
	xorl	%esi, %esi
	movq	(%rax), %rdi
	call	X509_SIG_get0@PLT
	movq	-104(%rbp), %rdi
	call	ASN1_STRING_length@PLT
	movl	%eax, %ebx
	xorl	%eax, %eax
	cmpl	-108(%rbp), %ebx
	je	.L73
.L64:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L74
	leaq	-16(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore_state
	movl	$162, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$109, %edx
	movl	%eax, -116(%rbp)
	movl	$126, %esi
	movl	$35, %edi
	call	ERR_put_error@PLT
	movl	-116(%rbp), %eax
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L73:
	movq	-104(%rbp), %rdi
	call	ASN1_STRING_get0_data@PLT
	movl	%ebx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	CRYPTO_memcmp@PLT
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L71:
	movl	$157, %r8d
	movl	$108, %edx
	movl	$126, %esi
	movl	$35, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L64
.L74:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE808:
	.size	PKCS12_verify_mac, .-PKCS12_verify_mac
	.p2align 4
	.globl	PKCS12_setup_mac
	.type	PKCS12_setup_mac, @function
PKCS12_setup_mac:
.LFB810:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	8(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	PKCS12_MAC_DATA_free@PLT
	movq	$0, 8(%rbx)
	call	PKCS12_MAC_DATA_new@PLT
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	je	.L90
	movq	%rax, %rdx
	cmpl	$1, %r13d
	jg	.L93
	movslq	%r12d, %r13
	testl	%r12d, %r12d
	je	.L94
.L81:
	movq	8(%rdx), %rcx
	leaq	.LC0(%rip), %rsi
	movl	$228, %edx
	movq	%r13, %rdi
	movq	%rcx, -72(%rbp)
	call	CRYPTO_malloc@PLT
	movq	-72(%rbp), %rcx
	movq	%rax, 8(%rcx)
	testq	%rax, %rax
	je	.L95
	movq	8(%rbx), %rax
	movq	8(%rax), %rax
	movl	%r12d, (%rax)
	movq	8(%rax), %rdi
	testq	%r14, %r14
	je	.L96
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
.L85:
	movq	8(%rbx), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	(%rax), %rdi
	call	X509_SIG_getm@PLT
	movq	%r15, %rdi
	call	EVP_MD_type@PLT
	movl	%eax, %edi
	call	OBJ_nid2obj@PLT
	movq	-64(%rbp), %rdi
	xorl	%ecx, %ecx
	movl	$5, %edx
	movq	%rax, %rsi
	call	X509_ALGOR_set0@PLT
	testl	%eax, %eax
	je	.L97
	movl	$1, %eax
.L75:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L98
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	movl	$8, %r13d
	movl	$8, %r12d
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L93:
	movq	%rax, -72(%rbp)
	call	ASN1_INTEGER_new@PLT
	movq	-72(%rbp), %rdx
	movl	$218, %r8d
	movq	%rax, 16(%rdx)
	testq	%rax, %rax
	je	.L91
	movq	8(%rbx), %rax
	movslq	%r13d, %rsi
	movq	16(%rax), %rdi
	call	ASN1_INTEGER_set@PLT
	testl	%eax, %eax
	je	.L80
	movq	8(%rbx), %rdx
	movslq	%r12d, %r13
	testl	%r12d, %r12d
	jne	.L81
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L95:
	movl	$229, %r8d
.L91:
	movl	$65, %edx
	movl	$122, %esi
	movl	$35, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L90:
	xorl	%eax, %eax
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L96:
	movl	%r12d, %esi
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jg	.L85
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L97:
	movl	%eax, -72(%rbp)
	movl	$241, %r8d
.L92:
	movl	$65, %edx
	movl	$122, %esi
	movl	$35, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	-72(%rbp), %eax
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L80:
	movl	%eax, -72(%rbp)
	movl	$222, %r8d
	jmp	.L92
.L98:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE810:
	.size	PKCS12_setup_mac, .-PKCS12_setup_mac
	.p2align 4
	.globl	PKCS12_set_mac
	.type	PKCS12_set_mac, @function
PKCS12_set_mac:
.LFB809:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	movq	%rcx, %rdx
	movl	%r8d, %ecx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	%r9d, %esi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	16(%rbp), %r8
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L107
.L100:
	movq	%rbx, %rdi
	call	PKCS12_setup_mac
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L108
	subq	$8, %rsp
	pushq	PKCS12_key_gen_utf8@GOTPCREL(%rip)
	movq	%r13, %rdx
	movl	%r14d, %ecx
	leaq	-128(%rbp), %r15
	leaq	16(%rbx), %rsi
	leaq	8(%rbx), %rdi
	leaq	-140(%rbp), %r9
	movq	%r15, %r8
	call	pkcs12_gen_mac.isra.0
	movl	%eax, %r12d
	popq	%rax
	popq	%rdx
	testl	%r12d, %r12d
	je	.L109
	movq	8(%rbx), %rax
	leaq	-136(%rbp), %rdx
	xorl	%esi, %esi
	movq	(%rax), %rdi
	call	X509_SIG_getm@PLT
	movl	-140(%rbp), %edx
	movq	-136(%rbp), %rdi
	movq	%r15, %rsi
	call	ASN1_OCTET_STRING_set@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L110
	movl	$1, %r12d
.L99:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L111
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	movl	$194, %r8d
	movl	$109, %edx
	movl	$123, %esi
	movl	$35, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L108:
	movl	$186, %r8d
	movl	$110, %edx
	movl	$123, %esi
	movl	$35, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L107:
	movl	%ecx, -156(%rbp)
	movq	%rdx, -152(%rbp)
	movl	%r9d, -160(%rbp)
	call	EVP_sha1@PLT
	movl	-160(%rbp), %esi
	movl	-156(%rbp), %ecx
	movq	-152(%rbp), %rdx
	movq	%rax, %r8
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L110:
	movl	$199, %r8d
	movl	$111, %edx
	movl	$123, %esi
	movl	$35, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L99
.L111:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE809:
	.size	PKCS12_set_mac, .-PKCS12_set_mac
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
