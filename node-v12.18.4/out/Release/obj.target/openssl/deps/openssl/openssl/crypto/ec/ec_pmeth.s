	.file	"ec_pmeth.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"ec_paramgen_curve"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../deps/openssl/openssl/crypto/ec/ec_pmeth.c"
	.section	.rodata.str1.1
.LC2:
	.string	"ec_param_enc"
.LC3:
	.string	"explicit"
.LC4:
	.string	"named_curve"
.LC5:
	.string	"ecdh_kdf_md"
.LC6:
	.string	"ecdh_cofactor_mode"
	.text
	.p2align 4
	.type	pkey_ec_ctrl_str, @function
pkey_ec_ctrl_str:
.LFB841:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$18, %ecx
	movq	%rsi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	leaq	.LC0(%rip), %rdi
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L18
	movl	$13, %ecx
	leaq	.LC2(%rip), %rdi
	movq	%r9, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L19
	movl	$12, %ecx
	leaq	.LC5(%rip), %rdi
	movq	%r9, %rsi
	repz cmpsb
	seta	%r12b
	sbbb	$0, %r12b
	movsbl	%r12b, %r12d
	testl	%r12d, %r12d
	je	.L20
	movl	$19, %ecx
	leaq	.LC6(%rip), %rdi
	movq	%r9, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L10
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	strtol@PLT
	xorl	%r9d, %r9d
	movl	$4099, %ecx
	movl	%eax, %r8d
.L16:
	movl	$1024, %edx
.L15:
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$408, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	EVP_PKEY_CTX_ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movl	$9, %ecx
	leaq	.LC3(%rip), %rdi
	movq	%rdx, %rsi
	repz cmpsb
	seta	%r8b
	sbbb	$0, %r8b
	movsbl	%r8b, %r8d
	testl	%r8d, %r8d
	je	.L6
	movl	$12, %ecx
	leaq	.LC4(%rip), %rdi
	movq	%rdx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L10
	movl	$1, %r8d
.L6:
	xorl	%r9d, %r9d
	movl	$4098, %ecx
	movl	$6, %edx
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L18:
	movq	%rdx, %rdi
	call	EC_curve_nist2nid@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L21
.L3:
	xorl	%r9d, %r9d
	movl	%r12d, %r8d
	movl	$4097, %ecx
	movl	$6, %edx
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L20:
	movq	%rdx, %rdi
	call	EVP_get_digestbyname@PLT
	xorl	%r8d, %r8d
	movl	$4101, %ecx
	movq	%rax, %r9
	testq	%rax, %rax
	jne	.L16
	movl	$381, %r8d
	movl	$151, %edx
	movl	$198, %esi
	movl	$16, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L21:
	movq	%r14, %rdi
	call	OBJ_sn2nid@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L3
	movq	%r14, %rdi
	call	OBJ_ln2nid@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L3
	movl	$365, %r8d
	movl	$141, %edx
	movl	$198, %esi
	movl	$16, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
.L1:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movl	$-2, %r12d
	jmp	.L1
	.cfi_endproc
.LFE841:
	.size	pkey_ec_ctrl_str, .-pkey_ec_ctrl_str
	.p2align 4
	.type	pkey_ec_cleanup, @function
pkey_ec_cleanup:
.LFB835:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	40(%rdi), %r12
	testq	%r12, %r12
	je	.L22
	movq	%rdi, %rbx
	movq	(%r12), %rdi
	call	EC_GROUP_free@PLT
	movq	16(%r12), %rdi
	call	EC_KEY_free@PLT
	movq	40(%r12), %rdi
	movl	$94, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$95, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	movq	$0, 40(%rbx)
.L22:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE835:
	.size	pkey_ec_cleanup, .-pkey_ec_cleanup
	.p2align 4
	.type	pkey_ec_ctrl, @function
pkey_ec_ctrl:
.LFB840:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	40(%rdi), %r13
	cmpl	$13, %esi
	jle	.L102
	leal	-4097(%rsi), %ebx
	cmpl	$9, %ebx
	ja	.L62
	movslq	%edx, %r14
	leaq	.L33(%rip), %rdx
	movslq	(%rdx,%rbx,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L33:
	.long	.L42-.L33
	.long	.L41-.L33
	.long	.L40-.L33
	.long	.L39-.L33
	.long	.L38-.L33
	.long	.L37-.L33
	.long	.L36-.L33
	.long	.L35-.L33
	.long	.L34-.L33
	.long	.L32-.L33
	.text
	.p2align 4,,10
	.p2align 3
.L102:
	testl	%esi, %esi
	jg	.L30
.L62:
	movl	$-2, %r12d
.L28:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	cmpl	$13, %esi
	ja	.L62
	leaq	.L45(%rip), %rcx
	movl	%esi, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L45:
	.long	.L62-.L45
	.long	.L46-.L45
	.long	.L63-.L45
	.long	.L62-.L45
	.long	.L62-.L45
	.long	.L63-.L45
	.long	.L62-.L45
	.long	.L63-.L45
	.long	.L62-.L45
	.long	.L62-.L45
	.long	.L62-.L45
	.long	.L63-.L45
	.long	.L62-.L45
	.long	.L44-.L45
	.text
.L63:
	movl	$1, %r12d
	jmp	.L28
.L44:
	movq	8(%r13), %rax
	movq	%rax, (%r12)
	movl	$1, %r12d
	jmp	.L28
.L32:
	movq	40(%r13), %rax
	movq	%rax, (%r12)
	movl	48(%r13), %r12d
	jmp	.L28
.L42:
	movl	%r14d, %edi
	call	EC_GROUP_new_by_curve_name@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L103
	movq	0(%r13), %rdi
	movl	$1, %r12d
	call	EC_GROUP_free@PLT
	movq	%rbx, 0(%r13)
	jmp	.L28
.L41:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L104
	movl	%r14d, %esi
	movl	$1, %r12d
	call	EC_GROUP_set_asn1_flag@PLT
	jmp	.L28
.L40:
	cmpl	$-2, %r14d
	je	.L105
	leal	1(%r14), %eax
	cmpl	$2, %eax
	ja	.L62
	movb	%r14b, 24(%r13)
	cmpl	$-1, %r14d
	je	.L53
	movq	16(%rdi), %rax
	movq	40(%rax), %r15
	movq	24(%r15), %rax
	testq	%rax, %rax
	je	.L62
	movq	24(%rax), %rdi
	call	BN_is_one@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L63
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.L106
.L55:
	movl	$4096, %esi
	testl	%r14d, %r14d
	je	.L56
	call	EC_KEY_set_flags@PLT
	movl	$1, %r12d
	jmp	.L28
.L39:
	cmpl	$-2, %r14d
	je	.L107
	leal	-1(%r14), %eax
	cmpl	$1, %eax
	ja	.L62
	movb	%r14b, 25(%r13)
	movl	$1, %r12d
	jmp	.L28
.L38:
	movq	%r12, 32(%r13)
	movl	$1, %r12d
	jmp	.L28
.L37:
	movq	32(%r13), %rax
	movq	%rax, (%r12)
	movl	$1, %r12d
	jmp	.L28
.L36:
	testl	%r14d, %r14d
	jle	.L62
	movq	%r14, 56(%r13)
	movl	$1, %r12d
	jmp	.L28
.L35:
	movq	56(%r13), %rax
	movl	%eax, (%r12)
	movl	$1, %r12d
	jmp	.L28
.L34:
	movq	40(%r13), %rdi
	movl	$308, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, 40(%r13)
	testq	%r12, %r12
	je	.L58
	movq	%r14, 48(%r13)
	movl	$1, %r12d
	jmp	.L28
.L46:
	movq	%r12, %rdi
	movl	%esi, -52(%rbp)
	call	EVP_MD_type@PLT
	movl	-52(%rbp), %esi
	cmpl	$64, %eax
	jne	.L108
.L59:
	movq	%r12, 8(%r13)
	movl	%esi, %r12d
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L108:
	movq	%r12, %rdi
	call	EVP_MD_type@PLT
	movl	-52(%rbp), %esi
	cmpl	$416, %eax
	je	.L59
	movq	%r12, %rdi
	call	EVP_MD_type@PLT
	movl	-52(%rbp), %esi
	cmpl	$675, %eax
	je	.L59
	movq	%r12, %rdi
	call	EVP_MD_type@PLT
	movl	-52(%rbp), %esi
	cmpl	$672, %eax
	je	.L59
	movq	%r12, %rdi
	call	EVP_MD_type@PLT
	movl	-52(%rbp), %esi
	cmpl	$673, %eax
	je	.L59
	movq	%r12, %rdi
	call	EVP_MD_type@PLT
	movl	-52(%rbp), %esi
	cmpl	$674, %eax
	je	.L59
	movq	%r12, %rdi
	call	EVP_MD_type@PLT
	movl	-52(%rbp), %esi
	cmpl	$1096, %eax
	je	.L59
	movq	%r12, %rdi
	call	EVP_MD_type@PLT
	movl	-52(%rbp), %esi
	cmpl	$1097, %eax
	je	.L59
	movq	%r12, %rdi
	call	EVP_MD_type@PLT
	movl	-52(%rbp), %esi
	cmpl	$1098, %eax
	je	.L59
	movq	%r12, %rdi
	call	EVP_MD_type@PLT
	movl	-52(%rbp), %esi
	cmpl	$1099, %eax
	je	.L59
	movl	$331, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$138, %edx
	xorl	%r12d, %r12d
	movl	$197, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L105:
	movzbl	24(%r13), %ebx
	cmpb	$-1, %bl
	je	.L51
	movsbl	%bl, %r12d
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L58:
	movq	$0, 48(%r13)
	movl	$1, %r12d
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L107:
	movsbl	25(%r13), %r12d
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L104:
	movl	$240, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$139, %edx
	xorl	%r12d, %r12d
	movl	$197, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L103:
	movl	$231, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$141, %edx
	xorl	%r12d, %r12d
	movl	$197, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L56:
	call	EC_KEY_clear_flags@PLT
	movl	$1, %r12d
	jmp	.L28
.L53:
	movq	16(%r13), %rdi
	movl	$1, %r12d
	call	EC_KEY_free@PLT
	movq	$0, 16(%r13)
	jmp	.L28
.L51:
	movq	16(%rdi), %rax
	movq	40(%rax), %rdi
	call	EC_KEY_get_flags@PLT
	shrl	$12, %eax
	movl	%eax, %ebx
	andl	$1, %ebx
	movl	%ebx, %r12d
	jmp	.L28
.L106:
	movq	%r15, %rdi
	call	EC_KEY_dup@PLT
	movq	%rax, 16(%r13)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L55
	jmp	.L28
	.cfi_endproc
.LFE840:
	.size	pkey_ec_ctrl, .-pkey_ec_ctrl
	.p2align 4
	.type	pkey_ec_derive, @function
pkey_ec_derive:
.LFB838:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L110
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L110
	movq	40(%rdi), %rcx
	movq	%rsi, %r12
	movq	16(%rcx), %r13
	testq	%r13, %r13
	je	.L120
	testq	%r12, %r12
	je	.L121
.L114:
	movq	40(%rax), %rdi
	call	EC_KEY_get0_public_key@PLT
	movq	(%rbx), %rsi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%rax, %rdx
	movq	%r12, %rdi
	call	ECDH_compute_key@PLT
	xorl	%r8d, %r8d
	testl	%eax, %eax
	jle	.L109
	cltq
	movl	$1, %r8d
	movq	%rax, (%rbx)
.L109:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	movl	$160, %r8d
	movl	$140, %edx
	movl	$217, %esi
	movl	$16, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%r8d, %r8d
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore_state
	movq	40(%rdx), %r13
	testq	%r12, %r12
	jne	.L114
.L121:
	movq	%r13, %rdi
	call	EC_KEY_get0_group@PLT
	movq	%rax, %rdi
	call	EC_GROUP_get_degree@PLT
	movl	$1, %r8d
	movl	%eax, %edx
	leal	14(%rax), %eax
	addl	$7, %edx
	cmovns	%edx, %eax
	sarl	$3, %eax
	cltq
	movq	%rax, (%rbx)
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE838:
	.size	pkey_ec_derive, .-pkey_ec_derive
	.p2align 4
	.type	pkey_ec_verify, @function
pkey_ec_verify:
.LFB837:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	40(%rax), %r15
	movq	40(%rdi), %rax
	movl	$64, %edi
	movq	8(%rax), %r8
	testq	%r8, %r8
	je	.L123
	movq	%r8, %rdi
	call	EVP_MD_type@PLT
	movl	%eax, %edi
.L123:
	addq	$8, %rsp
	movq	%r15, %r9
	movl	%r14d, %r8d
	movq	%r13, %rcx
	movl	%ebx, %edx
	movq	%r12, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	ECDSA_verify@PLT
	.cfi_endproc
.LFE837:
	.size	pkey_ec_verify, .-pkey_ec_verify
	.p2align 4
	.type	pkey_ec_keygen, @function
pkey_ec_keygen:
.LFB843:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpq	$0, 16(%rdi)
	movq	%rdi, %rbx
	movq	40(%rdi), %r14
	je	.L140
.L129:
	call	EC_KEY_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L128
	movq	%rax, %rdx
	movl	$408, %esi
	movq	%r13, %rdi
	call	EVP_PKEY_assign@PLT
	testl	%eax, %eax
	je	.L141
	movq	16(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L133
	movq	%r13, %rdi
	call	EVP_PKEY_copy_parameters@PLT
.L134:
	testl	%eax, %eax
	jne	.L142
.L128:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	cmpq	$0, (%r14)
	jne	.L129
	movl	$420, %r8d
	movl	$139, %edx
	movl	$199, %esi
	movl	$16, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L142:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	EC_KEY_generate_key@PLT
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore_state
	movq	(%r14), %rsi
	movq	%r12, %rdi
	call	EC_KEY_set_group@PLT
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L141:
	movq	%r12, %rdi
	call	EC_KEY_free@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE843:
	.size	pkey_ec_keygen, .-pkey_ec_keygen
	.p2align 4
	.type	pkey_ec_sign, @function
pkey_ec_sign:
.LFB836:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	%r8, -72(%rbp)
	movq	40(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	40(%rax), %r13
	movq	%r13, %rdi
	call	ECDSA_size@PLT
	xorl	%r10d, %r10d
	testl	%eax, %eax
	jle	.L143
	cltq
	testq	%r15, %r15
	je	.L153
	cmpq	%rax, (%rbx)
	jb	.L154
	movq	8(%r14), %r8
	movl	$64, %edi
	testq	%r8, %r8
	je	.L147
	movq	%r8, %rdi
	call	EVP_MD_type@PLT
	movl	%eax, %edi
.L147:
	movl	-72(%rbp), %edx
	movq	%r13, %r9
	leaq	-60(%rbp), %r8
	movq	%r15, %rcx
	movq	%r12, %rsi
	call	ECDSA_sign@PLT
	movl	%eax, %r10d
	testl	%eax, %eax
	jle	.L143
	movl	-60(%rbp), %eax
.L153:
	movq	%rax, (%rbx)
	movl	$1, %r10d
.L143:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L155
	addq	$40, %rsp
	movl	%r10d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L154:
	.cfi_restore_state
	movl	$119, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$100, %edx
	movl	$218, %esi
	movl	$16, %edi
	movl	%r10d, -72(%rbp)
	call	ERR_put_error@PLT
	movl	-72(%rbp), %r10d
	jmp	.L143
.L155:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE836:
	.size	pkey_ec_sign, .-pkey_ec_sign
	.p2align 4
	.type	pkey_ec_paramgen, @function
pkey_ec_paramgen:
.LFB842:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	40(%rdi), %rbx
	cmpq	$0, (%rbx)
	je	.L168
	movq	%rsi, %r13
	call	EC_KEY_new@PLT
	movq	%rax, %r12
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L156
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	EC_KEY_set_group@PLT
	testl	%eax, %eax
	jne	.L159
	movq	%r12, %rdi
	call	EC_KEY_free@PLT
	xorl	%eax, %eax
.L156:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore_state
	movq	%r12, %rdx
	movl	$408, %esi
	movq	%r13, %rdi
	call	EVP_PKEY_assign@PLT
	testl	%eax, %eax
	jne	.L156
	movq	%r12, %rdi
	call	EC_KEY_free@PLT
	xorl	%eax, %eax
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L168:
	movl	$401, %r8d
	movl	$139, %edx
	movl	$219, %esi
	movl	$16, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE842:
	.size	pkey_ec_paramgen, .-pkey_ec_paramgen
	.p2align 4
	.type	pkey_ec_init, @function
pkey_ec_init:
.LFB833:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$45, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$64, %edi
	subq	$8, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L173
	movl	$511, %edx
	movw	%dx, 24(%rax)
	movq	%rax, 40(%rbx)
	movl	$1, %eax
.L169:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L173:
	.cfi_restore_state
	movl	$46, %r8d
	movl	$65, %edx
	movl	$282, %esi
	movl	$16, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L169
	.cfi_endproc
.LFE833:
	.size	pkey_ec_init, .-pkey_ec_init
	.p2align 4
	.type	pkey_ec_copy, @function
pkey_ec_copy:
.LFB834:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$45, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$64, %edi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	leaq	.LC1(%rip), %rsi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L193
	movq	%rax, %rbx
	movl	$511, %eax
	movw	%ax, 24(%rbx)
	movq	%rbx, 40(%r13)
	movq	40(%r12), %r12
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L180
	call	EC_GROUP_dup@PLT
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L183
.L180:
	movq	8(%r12), %rax
	movq	16(%r12), %rdi
	movq	%rax, 8(%rbx)
	testq	%rdi, %rdi
	je	.L179
	call	EC_KEY_dup@PLT
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	je	.L183
.L179:
	movzbl	25(%r12), %eax
	movb	%al, 25(%rbx)
	movq	32(%r12), %rax
	movq	40(%r12), %rdi
	movq	48(%r12), %rsi
	movq	%rax, 32(%rbx)
	movq	56(%r12), %rax
	movq	%rax, 56(%rbx)
	testq	%rdi, %rdi
	je	.L194
	movl	$79, %ecx
	leaq	.LC1(%rip), %rdx
	call	CRYPTO_memdup@PLT
	movq	%rax, 40(%rbx)
	testq	%rax, %rax
	je	.L183
	movq	48(%r12), %rsi
.L184:
	movq	%rsi, 48(%rbx)
	movl	$1, %eax
.L174:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L194:
	.cfi_restore_state
	movq	$0, 40(%rbx)
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L183:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L193:
	.cfi_restore_state
	movl	$46, %r8d
	movl	$65, %edx
	movl	$282, %esi
	movl	$16, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L174
	.cfi_endproc
.LFE834:
	.size	pkey_ec_copy, .-pkey_ec_copy
	.p2align 4
	.type	pkey_ec_kdf_derive, @function
pkey_ec_kdf_derive:
.LFB839:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	40(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$1, 25(%rbx)
	je	.L213
	movq	56(%rbx), %rdx
	testq	%rsi, %rsi
	je	.L214
	xorl	%eax, %eax
	cmpq	%rdx, (%r14)
	je	.L215
.L195:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L216
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L215:
	.cfi_restore_state
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L199
	cmpq	$0, 24(%rdi)
	je	.L199
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L217
.L201:
	call	EC_KEY_get0_group@PLT
	movq	%rax, %rdi
	call	EC_GROUP_get_degree@PLT
	movl	$205, %edx
	leaq	.LC1(%rip), %rsi
	leal	14(%rax), %edi
	addl	$7, %eax
	cmovns	%eax, %edi
	sarl	$3, %edi
	movslq	%edi, %rdi
	movq	%rdi, -64(%rbp)
	call	CRYPTO_malloc@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L218
	leaq	-64(%rbp), %rdx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	pkey_ec_derive
	testl	%eax, %eax
	jne	.L219
.L203:
	movq	-64(%rbp), %rsi
	movl	$218, %ecx
	movq	%r15, %rdi
	movl	%eax, -68(%rbp)
	leaq	.LC1(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movl	-68(%rbp), %eax
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L199:
	movl	$160, %r8d
	movl	$140, %edx
	movl	$217, %esi
	movl	$16, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L214:
	movq	%rdx, (%r14)
	movl	$1, %eax
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L213:
	call	pkey_ec_derive
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L217:
	movq	40(%rax), %rdi
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L219:
	subq	$8, %rsp
	movq	48(%rbx), %r9
	movq	40(%rbx), %r8
	pushq	32(%rbx)
	movq	-64(%rbp), %rcx
	movq	%r15, %rdx
	movq	%r13, %rdi
	movq	(%r14), %rsi
	call	ecdh_KDF_X9_63@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L218:
	movl	$206, %r8d
	movl	$65, %edx
	movl	$283, %esi
	movl	$16, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L195
.L216:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE839:
	.size	pkey_ec_kdf_derive, .-pkey_ec_kdf_derive
	.globl	ec_pkey_meth
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	ec_pkey_meth, @object
	.size	ec_pkey_meth, 256
ec_pkey_meth:
	.long	408
	.long	0
	.quad	pkey_ec_init
	.quad	pkey_ec_copy
	.quad	pkey_ec_cleanup
	.quad	0
	.quad	pkey_ec_paramgen
	.quad	0
	.quad	pkey_ec_keygen
	.quad	0
	.quad	pkey_ec_sign
	.quad	0
	.quad	pkey_ec_verify
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	pkey_ec_kdf_derive
	.quad	pkey_ec_ctrl
	.quad	pkey_ec_ctrl_str
	.zero	48
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
