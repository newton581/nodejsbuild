	.file	"bss_mem.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/bio/bss_mem.c"
	.text
	.p2align 4
	.type	mem_new, @function
mem_new:
.LFB271:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$115, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$16, %edi
	subq	$8, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L1
	xorl	%edi, %edi
	movq	%rax, %r12
	call	BUF_MEM_new_ex@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L9
	movl	$123, %edx
	leaq	.LC0(%rip), %rsi
	movl	$32, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L10
	movq	(%r12), %rdx
	movl	$1, %r13d
	movdqu	(%rdx), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%rdx), %xmm1
	movups	%xmm1, 16(%rax)
	movabsq	$4294967297, %rax
	movq	%rax, 32(%rbx)
	movl	$-1, 48(%rbx)
	movq	%r12, 56(%rbx)
.L1:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$120, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movq	(%r12), %rdi
	call	BUF_MEM_free@PLT
	movl	$125, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	jmp	.L1
	.cfi_endproc
.LFE271:
	.size	mem_new, .-mem_new
	.p2align 4
	.type	mem_read, @function
mem_read:
.LFB276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	56(%rdi), %rax
	movq	8(%rax), %rbx
	testb	$2, 41(%rdi)
	je	.L12
	movq	(%rax), %rbx
.L12:
	movl	$15, %esi
	movq	%r13, %rdi
	call	BIO_clear_flags@PLT
	testl	%r12d, %r12d
	js	.L13
	movq	(%rbx), %rax
	movslq	%r12d, %rdx
	cmpq	%rax, %rdx
	cmova	%eax, %r12d
.L13:
	testq	%r14, %r14
	je	.L14
	testl	%r12d, %r12d
	jg	.L29
.L14:
	cmpq	$0, (%rbx)
	jne	.L15
	movl	48(%r13), %r12d
	testl	%r12d, %r12d
	jne	.L30
.L15:
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movslq	%r12d, %r13
	movq	8(%rbx), %rsi
	movq	%r14, %rdi
	movq	%r13, %rdx
	call	memcpy@PLT
	subq	%r13, (%rbx)
	movl	%r12d, %eax
	subq	%r13, 16(%rbx)
	addq	%r13, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movq	%r13, %rdi
	movl	$9, %esi
	call	BIO_set_flags@PLT
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE276:
	.size	mem_read, .-mem_read
	.p2align 4
	.type	mem_gets, @function
mem_gets:
.LFB279:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	56(%rdi), %rax
	movq	%rsi, %rbx
	movq	8(%rax), %r14
	testb	$2, 41(%rdi)
	je	.L32
	movq	(%rax), %r14
.L32:
	movl	$15, %esi
	movq	%r12, %rdi
	call	BIO_clear_flags@PLT
	movq	(%r14), %rax
	leal	-1(%r13), %edx
	cmpl	%eax, %r13d
	cmovle	%edx, %eax
	testl	%eax, %eax
	jle	.L43
	leal	-1(%rax), %edx
	movq	8(%r14), %rcx
	movl	$1, %eax
	addq	$2, %rdx
	.p2align 4,,10
	.p2align 3
.L37:
	cmpb	$10, -1(%rcx,%rax)
	movl	%eax, %r8d
	je	.L36
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L37
.L36:
	movl	%r8d, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	mem_read
	testl	%eax, %eax
	jle	.L31
	movslq	%eax, %rdx
	movb	$0, (%rbx,%rdx)
.L31:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	movb	$0, (%rbx)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE279:
	.size	mem_gets, .-mem_gets
	.p2align 4
	.type	mem_free, @function
mem_free:
.LFB273:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L48
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movl	36(%rdi), %edx
	movq	56(%rdi), %r12
	testl	%edx, %edx
	je	.L46
	movl	32(%rdi), %eax
	testl	%eax, %eax
	je	.L46
	testq	%r12, %r12
	je	.L46
	movq	(%r12), %r8
	testb	$2, 41(%rdi)
	je	.L47
	movq	$0, 8(%r8)
.L47:
	movq	%r8, %rdi
	call	BUF_MEM_free@PLT
.L46:
	movq	8(%r12), %rdi
	movl	$156, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	movl	$157, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE273:
	.size	mem_free, .-mem_free
	.p2align 4
	.type	mem_ctrl, @function
mem_ctrl:
.LFB278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	56(%rdi), %r12
	movl	40(%rdi), %edi
	movq	(%r12), %r13
	movq	8(%r12), %r9
	movl	%edi, %r11d
	andl	$512, %r11d
	movq	%r13, %r10
	cmove	%r9, %r10
	cmpl	$12, %esi
	jg	.L64
	xorl	%eax, %eax
	testl	%esi, %esi
	jle	.L62
	cmpl	$12, %esi
	ja	.L84
	leaq	.L67(%rip), %r8
	movl	%esi, %esi
	movslq	(%r8,%rsi,4), %rax
	addq	%r8, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L67:
	.long	.L84-.L67
	.long	.L73-.L67
	.long	.L72-.L67
	.long	.L71-.L67
	.long	.L84-.L67
	.long	.L84-.L67
	.long	.L84-.L67
	.long	.L84-.L67
	.long	.L70-.L67
	.long	.L69-.L67
	.long	.L68-.L67
	.long	.L96-.L67
	.long	.L96-.L67
	.text
.L73:
	movq	8(%r13), %r8
	movl	$1, %eax
	testq	%r8, %r8
	je	.L62
	testl	%r11d, %r11d
	jne	.L77
	andl	$1024, %edi
	je	.L97
.L78:
	movdqu	0(%r13), %xmm2
	movups	%xmm2, (%r9)
	movdqu	16(%r13), %xmm3
	movups	%xmm3, 16(%r9)
.L96:
	movl	$1, %eax
.L62:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L84:
	.cfi_restore_state
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
	cmpl	$115, %esi
	je	.L74
	cmpl	$130, %esi
	jne	.L98
	movl	%edx, 48(%rbx)
	movl	$1, %eax
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L98:
	cmpl	$114, %esi
	jne	.L84
	movl	36(%rbx), %edi
	testl	%edi, %edi
	je	.L79
	movl	32(%rbx), %esi
	testl	%esi, %esi
	je	.L79
	testl	%r11d, %r11d
	je	.L80
	movq	$0, 8(%r13)
.L80:
	movq	%r13, %rdi
	movq	%rcx, -48(%rbp)
	movq	%rdx, -40(%rbp)
	call	BUF_MEM_free@PLT
	movq	8(%r12), %r9
	movq	-48(%rbp), %rcx
	movq	-40(%rbp), %rdx
	.p2align 4,,10
	.p2align 3
.L79:
	movdqu	(%rcx), %xmm0
	movl	%edx, 36(%rbx)
	movl	$1, %eax
	movq	%rcx, (%r12)
	movups	%xmm0, (%r9)
	movdqu	16(%rcx), %xmm1
	movups	%xmm1, 16(%r9)
	jmp	.L62
.L68:
	movq	(%r10), %rax
	jmp	.L62
.L72:
	xorl	%eax, %eax
	cmpq	$0, (%r10)
	sete	%al
	jmp	.L62
.L71:
	movq	(%r10), %rax
	testq	%rcx, %rcx
	je	.L62
	movq	8(%r10), %rdx
	movq	%rdx, (%rcx)
	jmp	.L62
.L70:
	movslq	36(%rbx), %rax
	jmp	.L62
.L69:
	movl	%edx, 36(%rbx)
	movl	$1, %eax
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L74:
	movl	$1, %eax
	testq	%rcx, %rcx
	je	.L62
	testl	%r11d, %r11d
	jne	.L81
	movl	32(%rbx), %eax
	testl	%eax, %eax
	je	.L81
	movq	8(%r9), %rsi
	movq	8(%r13), %rdi
	cmpq	%rdi, %rsi
	je	.L81
	movq	(%r9), %rdx
	movq	%rcx, -40(%rbp)
	call	memmove@PLT
	movq	8(%r12), %rax
	movq	(%r12), %r13
	movq	-40(%rbp), %rcx
	movq	(%rax), %rdx
	movq	%rdx, 0(%r13)
	movq	8(%r13), %rdx
	movq	%rdx, 8(%rax)
	.p2align 4,,10
	.p2align 3
.L81:
	movq	%r13, (%rcx)
	movl	$1, %eax
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L77:
	movdqu	(%r9), %xmm4
	movups	%xmm4, 0(%r13)
	movdqu	16(%r9), %xmm5
	movups	%xmm5, 16(%r13)
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L97:
	movq	16(%r13), %rdx
	xorl	%esi, %esi
	movq	%r8, %rdi
	call	memset@PLT
	movq	$0, 0(%r13)
	movq	8(%r12), %r9
	movq	(%r12), %r13
	jmp	.L78
	.cfi_endproc
.LFE278:
	.size	mem_ctrl, .-mem_ctrl
	.p2align 4
	.type	secmem_new, @function
secmem_new:
.LFB272:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$115, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$16, %edi
	subq	$8, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L99
	movl	$1, %edi
	movq	%rax, %r12
	call	BUF_MEM_new_ex@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L106
	movl	$123, %edx
	leaq	.LC0(%rip), %rsi
	movl	$32, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L107
	movq	(%r12), %rdx
	movl	$1, %r13d
	movdqu	(%rdx), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%rdx), %xmm1
	movups	%xmm1, 16(%rax)
	movabsq	$4294967297, %rax
	movq	%rax, 32(%rbx)
	movl	$-1, 48(%rbx)
	movq	%r12, 56(%rbx)
.L99:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$120, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	movq	(%r12), %rdi
	call	BUF_MEM_free@PLT
	movl	$125, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	jmp	.L99
	.cfi_endproc
.LFE272:
	.size	secmem_new, .-secmem_new
	.p2align 4
	.type	mem_write, @function
mem_write:
.LFB277:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	56(%rdi), %r15
	testq	%rsi, %rsi
	je	.L121
	movq	%rdi, %rbx
	testb	$2, 41(%rdi)
	jne	.L122
	movq	%rsi, %r13
	movl	%edx, %r12d
	movl	$15, %esi
	call	BIO_clear_flags@PLT
	testl	%r12d, %r12d
	je	.L110
	movq	8(%r15), %rax
	movq	(%rax), %r14
	movl	32(%rbx), %eax
	testl	%eax, %eax
	jne	.L123
.L112:
	leal	(%r12,%r14), %esi
	movq	(%r15), %rdi
	movslq	%esi, %rsi
	call	BUF_MEM_grow_clean@PLT
	testq	%rax, %rax
	je	.L113
	movq	(%r15), %rax
	movslq	%r12d, %rdx
	movslq	%r14d, %rdi
	movq	%r13, %rsi
	addq	8(%rax), %rdi
	call	memcpy@PLT
	movq	(%r15), %rdx
	movq	8(%r15), %rax
	movdqu	(%rdx), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%rdx), %xmm1
	movups	%xmm1, 16(%rax)
.L110:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore_state
	movq	56(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L112
	movq	8(%rbx), %rax
	movq	(%rbx), %rdx
	movq	8(%rax), %rsi
	movq	8(%rdx), %rdi
	cmpq	%rdi, %rsi
	je	.L112
	movq	(%rax), %rdx
	call	memmove@PLT
	movq	8(%rbx), %rax
	movq	(%rbx), %rdx
	movq	(%rax), %rcx
	movq	%rcx, (%rdx)
	movq	8(%rdx), %rdx
	movq	%rdx, 8(%rax)
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L113:
	movl	$-1, %r12d
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L122:
	movl	$228, %r8d
	movl	$126, %edx
	movl	$117, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L110
.L121:
	movl	$224, %r8d
	movl	$115, %edx
	movl	$117, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L110
	.cfi_endproc
.LFE277:
	.size	mem_write, .-mem_write
	.p2align 4
	.type	mem_puts, @function
mem_puts:
.LFB280:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	call	strlen@PLT
	movq	56(%r12), %r15
	testb	$2, 41(%r12)
	jne	.L133
	movq	%rax, %rbx
	movl	$15, %esi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	BIO_clear_flags@PLT
	testl	%ebx, %ebx
	je	.L124
	movq	8(%r15), %rax
	movq	(%rax), %rcx
	movl	32(%r12), %eax
	testl	%eax, %eax
	jne	.L134
.L127:
	leal	(%rbx,%rcx), %esi
	movq	(%r15), %rdi
	movq	%rcx, -56(%rbp)
	movslq	%esi, %rsi
	call	BUF_MEM_grow_clean@PLT
	testq	%rax, %rax
	je	.L128
	movq	(%r15), %rax
	movq	-56(%rbp), %rcx
	movslq	%ebx, %rdx
	movq	%r13, %rsi
	movslq	%ecx, %rdi
	addq	8(%rax), %rdi
	call	memcpy@PLT
	movq	(%r15), %rdx
	movq	8(%r15), %rax
	movdqu	(%rdx), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%rdx), %xmm1
	movups	%xmm1, 16(%rax)
.L124:
	addq	$24, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	movq	56(%r12), %r12
	testq	%r12, %r12
	je	.L127
	movq	8(%r12), %rax
	movq	(%r12), %rdx
	movq	8(%rax), %rsi
	movq	8(%rdx), %rdi
	cmpq	%rdi, %rsi
	je	.L127
	movq	(%rax), %rdx
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	8(%r12), %rax
	movq	(%r12), %rdx
	movq	-56(%rbp), %rcx
	movq	(%rax), %rsi
	movq	%rsi, (%rdx)
	movq	8(%rdx), %rdx
	movq	%rdx, 8(%rax)
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L128:
	movl	$-1, %r14d
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L133:
	movl	$228, %r8d
	movl	$126, %edx
	movl	$117, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r14d
	call	ERR_put_error@PLT
	jmp	.L124
	.cfi_endproc
.LFE280:
	.size	mem_puts, .-mem_puts
	.p2align 4
	.globl	BIO_s_mem
	.type	BIO_s_mem, @function
BIO_s_mem:
.LFB267:
	.cfi_startproc
	endbr64
	leaq	mem_method(%rip), %rax
	ret
	.cfi_endproc
.LFE267:
	.size	BIO_s_mem, .-BIO_s_mem
	.p2align 4
	.globl	BIO_s_secmem
	.type	BIO_s_secmem, @function
BIO_s_secmem:
.LFB268:
	.cfi_startproc
	endbr64
	leaq	secmem_method(%rip), %rax
	ret
	.cfi_endproc
.LFE268:
	.size	BIO_s_secmem, .-BIO_s_secmem
	.p2align 4
	.globl	BIO_new_mem_buf
	.type	BIO_new_mem_buf, @function
BIO_new_mem_buf:
.LFB269:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	testq	%rdi, %rdi
	je	.L146
	movq	%rdi, %rbx
	movslq	%esi, %r12
	testl	%esi, %esi
	jns	.L141
	call	strlen@PLT
	movq	%rax, %r12
.L141:
	leaq	mem_method(%rip), %rdi
	call	BIO_new@PLT
	testq	%rax, %rax
	je	.L137
	movq	56(%rax), %rcx
	movq	(%rcx), %rdx
	movq	8(%rcx), %rcx
	movq	%rbx, 8(%rdx)
	movq	%r12, (%rdx)
	movdqu	(%rdx), %xmm0
	movq	%r12, 16(%rdx)
	movups	%xmm0, (%rcx)
	movdqu	16(%rdx), %xmm1
	movups	%xmm1, 16(%rcx)
	orl	$512, 40(%rax)
	movl	$0, 48(%rax)
.L137:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_restore_state
	movl	$94, %r8d
	movl	$115, %edx
	movl	$126, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L137
	.cfi_endproc
.LFE269:
	.size	BIO_new_mem_buf, .-BIO_new_mem_buf
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"secure memory buffer"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	secmem_method, @object
	.size	secmem_method, 96
secmem_method:
	.long	1025
	.zero	4
	.quad	.LC1
	.quad	bwrite_conv
	.quad	mem_write
	.quad	bread_conv
	.quad	mem_read
	.quad	mem_puts
	.quad	mem_gets
	.quad	mem_ctrl
	.quad	secmem_new
	.quad	mem_free
	.quad	0
	.section	.rodata.str1.1
.LC2:
	.string	"memory buffer"
	.section	.data.rel.ro
	.align 32
	.type	mem_method, @object
	.size	mem_method, 96
mem_method:
	.long	1025
	.zero	4
	.quad	.LC2
	.quad	bwrite_conv
	.quad	mem_write
	.quad	bread_conv
	.quad	mem_read
	.quad	mem_puts
	.quad	mem_gets
	.quad	mem_ctrl
	.quad	mem_new
	.quad	mem_free
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
