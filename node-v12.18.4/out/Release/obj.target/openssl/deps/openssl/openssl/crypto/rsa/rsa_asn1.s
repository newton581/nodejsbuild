	.file	"rsa_asn1.c"
	.text
	.p2align 4
	.type	rsa_cb, @function
rsa_cb:
.LFB829:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	testl	%edi, %edi
	je	.L18
	cmpl	$2, %edi
	je	.L19
	movl	$1, %eax
	cmpl	$5, %edi
	je	.L20
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movq	(%rsi), %rdi
	cmpl	$1, 4(%rdi)
	jne	.L1
	call	rsa_multip_calc_product@PLT
	cmpl	$1, %eax
	je	.L16
.L3:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	call	RSA_new@PLT
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L3
.L16:
	addq	$8, %rsp
	movl	$2, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movq	(%rsi), %rdi
	call	RSA_free@PLT
	movq	$0, (%rbx)
	jmp	.L16
	.cfi_endproc
.LFE829:
	.size	rsa_cb, .-rsa_cb
	.p2align 4
	.type	rsa_pss_cb, @function
rsa_pss_cb:
.LFB830:
	.cfi_startproc
	endbr64
	cmpl	$2, %edi
	je	.L27
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rsi), %rax
	movq	32(%rax), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509_ALGOR_free@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE830:
	.size	rsa_pss_cb, .-rsa_pss_cb
	.p2align 4
	.type	rsa_oaep_cb, @function
rsa_oaep_cb:
.LFB835:
	.cfi_startproc
	endbr64
	cmpl	$2, %edi
	je	.L34
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rsi), %rax
	movq	24(%rax), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509_ALGOR_free@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE835:
	.size	rsa_oaep_cb, .-rsa_oaep_cb
	.p2align 4
	.globl	d2i_RSA_PSS_PARAMS
	.type	d2i_RSA_PSS_PARAMS, @function
d2i_RSA_PSS_PARAMS:
.LFB831:
	.cfi_startproc
	endbr64
	leaq	RSA_PSS_PARAMS_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE831:
	.size	d2i_RSA_PSS_PARAMS, .-d2i_RSA_PSS_PARAMS
	.p2align 4
	.globl	i2d_RSA_PSS_PARAMS
	.type	i2d_RSA_PSS_PARAMS, @function
i2d_RSA_PSS_PARAMS:
.LFB832:
	.cfi_startproc
	endbr64
	leaq	RSA_PSS_PARAMS_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE832:
	.size	i2d_RSA_PSS_PARAMS, .-i2d_RSA_PSS_PARAMS
	.p2align 4
	.globl	RSA_PSS_PARAMS_new
	.type	RSA_PSS_PARAMS_new, @function
RSA_PSS_PARAMS_new:
.LFB833:
	.cfi_startproc
	endbr64
	leaq	RSA_PSS_PARAMS_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE833:
	.size	RSA_PSS_PARAMS_new, .-RSA_PSS_PARAMS_new
	.p2align 4
	.globl	RSA_PSS_PARAMS_free
	.type	RSA_PSS_PARAMS_free, @function
RSA_PSS_PARAMS_free:
.LFB834:
	.cfi_startproc
	endbr64
	leaq	RSA_PSS_PARAMS_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE834:
	.size	RSA_PSS_PARAMS_free, .-RSA_PSS_PARAMS_free
	.p2align 4
	.globl	d2i_RSA_OAEP_PARAMS
	.type	d2i_RSA_OAEP_PARAMS, @function
d2i_RSA_OAEP_PARAMS:
.LFB836:
	.cfi_startproc
	endbr64
	leaq	RSA_OAEP_PARAMS_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE836:
	.size	d2i_RSA_OAEP_PARAMS, .-d2i_RSA_OAEP_PARAMS
	.p2align 4
	.globl	i2d_RSA_OAEP_PARAMS
	.type	i2d_RSA_OAEP_PARAMS, @function
i2d_RSA_OAEP_PARAMS:
.LFB837:
	.cfi_startproc
	endbr64
	leaq	RSA_OAEP_PARAMS_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE837:
	.size	i2d_RSA_OAEP_PARAMS, .-i2d_RSA_OAEP_PARAMS
	.p2align 4
	.globl	RSA_OAEP_PARAMS_new
	.type	RSA_OAEP_PARAMS_new, @function
RSA_OAEP_PARAMS_new:
.LFB838:
	.cfi_startproc
	endbr64
	leaq	RSA_OAEP_PARAMS_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE838:
	.size	RSA_OAEP_PARAMS_new, .-RSA_OAEP_PARAMS_new
	.p2align 4
	.globl	RSA_OAEP_PARAMS_free
	.type	RSA_OAEP_PARAMS_free, @function
RSA_OAEP_PARAMS_free:
.LFB839:
	.cfi_startproc
	endbr64
	leaq	RSA_OAEP_PARAMS_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE839:
	.size	RSA_OAEP_PARAMS_free, .-RSA_OAEP_PARAMS_free
	.p2align 4
	.globl	d2i_RSAPrivateKey
	.type	d2i_RSAPrivateKey, @function
d2i_RSAPrivateKey:
.LFB840:
	.cfi_startproc
	endbr64
	leaq	RSAPrivateKey_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE840:
	.size	d2i_RSAPrivateKey, .-d2i_RSAPrivateKey
	.p2align 4
	.globl	i2d_RSAPrivateKey
	.type	i2d_RSAPrivateKey, @function
i2d_RSAPrivateKey:
.LFB841:
	.cfi_startproc
	endbr64
	leaq	RSAPrivateKey_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE841:
	.size	i2d_RSAPrivateKey, .-i2d_RSAPrivateKey
	.p2align 4
	.globl	d2i_RSAPublicKey
	.type	d2i_RSAPublicKey, @function
d2i_RSAPublicKey:
.LFB842:
	.cfi_startproc
	endbr64
	leaq	RSAPublicKey_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE842:
	.size	d2i_RSAPublicKey, .-d2i_RSAPublicKey
	.p2align 4
	.globl	i2d_RSAPublicKey
	.type	i2d_RSAPublicKey, @function
i2d_RSAPublicKey:
.LFB843:
	.cfi_startproc
	endbr64
	leaq	RSAPublicKey_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE843:
	.size	i2d_RSAPublicKey, .-i2d_RSAPublicKey
	.p2align 4
	.globl	RSAPublicKey_dup
	.type	RSAPublicKey_dup, @function
RSAPublicKey_dup:
.LFB844:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	leaq	RSAPublicKey_it(%rip), %rdi
	jmp	ASN1_item_dup@PLT
	.cfi_endproc
.LFE844:
	.size	RSAPublicKey_dup, .-RSAPublicKey_dup
	.p2align 4
	.globl	RSAPrivateKey_dup
	.type	RSAPrivateKey_dup, @function
RSAPrivateKey_dup:
.LFB845:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	leaq	RSAPrivateKey_it(%rip), %rdi
	jmp	ASN1_item_dup@PLT
	.cfi_endproc
.LFE845:
	.size	RSAPrivateKey_dup, .-RSAPrivateKey_dup
	.globl	RSA_OAEP_PARAMS_it
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"RSA_OAEP_PARAMS"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	RSA_OAEP_PARAMS_it, @object
	.size	RSA_OAEP_PARAMS_it, 56
RSA_OAEP_PARAMS_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	RSA_OAEP_PARAMS_seq_tt
	.quad	3
	.quad	RSA_OAEP_PARAMS_aux
	.quad	32
	.quad	.LC0
	.section	.rodata.str1.1
.LC1:
	.string	"hashFunc"
.LC2:
	.string	"maskGenFunc"
.LC3:
	.string	"pSourceFunc"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	RSA_OAEP_PARAMS_seq_tt, @object
	.size	RSA_OAEP_PARAMS_seq_tt, 120
RSA_OAEP_PARAMS_seq_tt:
	.quad	145
	.quad	0
	.quad	0
	.quad	.LC1
	.quad	X509_ALGOR_it
	.quad	145
	.quad	1
	.quad	8
	.quad	.LC2
	.quad	X509_ALGOR_it
	.quad	145
	.quad	2
	.quad	16
	.quad	.LC3
	.quad	X509_ALGOR_it
	.section	.data.rel.ro.local
	.align 32
	.type	RSA_OAEP_PARAMS_aux, @object
	.size	RSA_OAEP_PARAMS_aux, 40
RSA_OAEP_PARAMS_aux:
	.quad	0
	.long	0
	.long	0
	.long	0
	.zero	4
	.quad	rsa_oaep_cb
	.long	0
	.zero	4
	.globl	RSA_PSS_PARAMS_it
	.section	.rodata.str1.1
.LC4:
	.string	"RSA_PSS_PARAMS"
	.section	.data.rel.ro.local
	.align 32
	.type	RSA_PSS_PARAMS_it, @object
	.size	RSA_PSS_PARAMS_it, 56
RSA_PSS_PARAMS_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	RSA_PSS_PARAMS_seq_tt
	.quad	4
	.quad	RSA_PSS_PARAMS_aux
	.quad	40
	.quad	.LC4
	.section	.rodata.str1.1
.LC5:
	.string	"hashAlgorithm"
.LC6:
	.string	"maskGenAlgorithm"
.LC7:
	.string	"saltLength"
.LC8:
	.string	"trailerField"
	.section	.data.rel.ro
	.align 32
	.type	RSA_PSS_PARAMS_seq_tt, @object
	.size	RSA_PSS_PARAMS_seq_tt, 160
RSA_PSS_PARAMS_seq_tt:
	.quad	145
	.quad	0
	.quad	0
	.quad	.LC5
	.quad	X509_ALGOR_it
	.quad	145
	.quad	1
	.quad	8
	.quad	.LC6
	.quad	X509_ALGOR_it
	.quad	145
	.quad	2
	.quad	16
	.quad	.LC7
	.quad	ASN1_INTEGER_it
	.quad	145
	.quad	3
	.quad	24
	.quad	.LC8
	.quad	ASN1_INTEGER_it
	.section	.data.rel.ro.local
	.align 32
	.type	RSA_PSS_PARAMS_aux, @object
	.size	RSA_PSS_PARAMS_aux, 40
RSA_PSS_PARAMS_aux:
	.quad	0
	.long	0
	.long	0
	.long	0
	.zero	4
	.quad	rsa_pss_cb
	.long	0
	.zero	4
	.globl	RSAPublicKey_it
	.section	.rodata.str1.1
.LC9:
	.string	"RSAPublicKey"
	.section	.data.rel.ro.local
	.align 32
	.type	RSAPublicKey_it, @object
	.size	RSAPublicKey_it, 56
RSAPublicKey_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	RSAPublicKey_seq_tt
	.quad	2
	.quad	RSAPublicKey_aux
	.quad	176
	.quad	.LC9
	.section	.rodata.str1.1
.LC10:
	.string	"n"
.LC11:
	.string	"e"
	.section	.data.rel.ro
	.align 32
	.type	RSAPublicKey_seq_tt, @object
	.size	RSAPublicKey_seq_tt, 80
RSAPublicKey_seq_tt:
	.quad	0
	.quad	0
	.quad	24
	.quad	.LC10
	.quad	BIGNUM_it
	.quad	0
	.quad	0
	.quad	32
	.quad	.LC11
	.quad	BIGNUM_it
	.section	.data.rel.ro.local
	.align 32
	.type	RSAPublicKey_aux, @object
	.size	RSAPublicKey_aux, 40
RSAPublicKey_aux:
	.quad	0
	.long	0
	.long	0
	.long	0
	.zero	4
	.quad	rsa_cb
	.long	0
	.zero	4
	.globl	RSAPrivateKey_it
	.section	.rodata.str1.1
.LC12:
	.string	"RSAPrivateKey"
	.section	.data.rel.ro.local
	.align 32
	.type	RSAPrivateKey_it, @object
	.size	RSAPrivateKey_it, 56
RSAPrivateKey_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	RSAPrivateKey_seq_tt
	.quad	10
	.quad	RSAPrivateKey_aux
	.quad	176
	.quad	.LC12
	.section	.rodata.str1.1
.LC13:
	.string	"version"
.LC14:
	.string	"d"
.LC15:
	.string	"p"
.LC16:
	.string	"q"
.LC17:
	.string	"dmp1"
.LC18:
	.string	"dmq1"
.LC19:
	.string	"iqmp"
.LC20:
	.string	"prime_infos"
	.section	.data.rel.ro
	.align 32
	.type	RSAPrivateKey_seq_tt, @object
	.size	RSAPrivateKey_seq_tt, 400
RSAPrivateKey_seq_tt:
	.quad	4096
	.quad	0
	.quad	4
	.quad	.LC13
	.quad	INT32_it
	.quad	0
	.quad	0
	.quad	24
	.quad	.LC10
	.quad	BIGNUM_it
	.quad	0
	.quad	0
	.quad	32
	.quad	.LC11
	.quad	BIGNUM_it
	.quad	0
	.quad	0
	.quad	40
	.quad	.LC14
	.quad	CBIGNUM_it
	.quad	0
	.quad	0
	.quad	48
	.quad	.LC15
	.quad	CBIGNUM_it
	.quad	0
	.quad	0
	.quad	56
	.quad	.LC16
	.quad	CBIGNUM_it
	.quad	0
	.quad	0
	.quad	64
	.quad	.LC17
	.quad	CBIGNUM_it
	.quad	0
	.quad	0
	.quad	72
	.quad	.LC18
	.quad	CBIGNUM_it
	.quad	0
	.quad	0
	.quad	80
	.quad	.LC19
	.quad	CBIGNUM_it
	.quad	5
	.quad	0
	.quad	88
	.quad	.LC20
	.quad	RSA_PRIME_INFO_it
	.section	.data.rel.ro.local
	.align 32
	.type	RSAPrivateKey_aux, @object
	.size	RSAPrivateKey_aux, 40
RSAPrivateKey_aux:
	.quad	0
	.long	0
	.long	0
	.long	0
	.zero	4
	.quad	rsa_cb
	.long	0
	.zero	4
	.globl	RSA_PRIME_INFO_it
	.section	.rodata.str1.1
.LC21:
	.string	"RSA_PRIME_INFO"
	.section	.data.rel.ro.local
	.align 32
	.type	RSA_PRIME_INFO_it, @object
	.size	RSA_PRIME_INFO_it, 56
RSA_PRIME_INFO_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	RSA_PRIME_INFO_seq_tt
	.quad	3
	.quad	0
	.quad	40
	.quad	.LC21
	.section	.rodata.str1.1
.LC22:
	.string	"r"
.LC23:
	.string	"t"
	.section	.data.rel.ro
	.align 32
	.type	RSA_PRIME_INFO_seq_tt, @object
	.size	RSA_PRIME_INFO_seq_tt, 120
RSA_PRIME_INFO_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC22
	.quad	CBIGNUM_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC14
	.quad	CBIGNUM_it
	.quad	0
	.quad	0
	.quad	16
	.quad	.LC23
	.quad	CBIGNUM_it
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
