	.file	"buffer.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/buffer/buffer.c"
	.text
	.p2align 4
	.globl	BUF_MEM_new_ex
	.type	BUF_MEM_new_ex, @function
BUF_MEM_new_ex:
.LFB251:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$35, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$32, %edi
	subq	$24, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L6
	movq	%rbx, 24(%rax)
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movl	$37, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$101, %esi
	movl	$7, %edi
	movq	%rax, -24(%rbp)
	call	ERR_put_error@PLT
	movq	-24(%rbp), %rax
	jmp	.L1
	.cfi_endproc
.LFE251:
	.size	BUF_MEM_new_ex, .-BUF_MEM_new_ex
	.p2align 4
	.globl	BUF_MEM_new
	.type	BUF_MEM_new, @function
BUF_MEM_new:
.LFB252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$35, %edx
	movl	$32, %edi
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L10
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movl	$37, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$101, %esi
	movl	$7, %edi
	movq	%rax, -8(%rbp)
	call	ERR_put_error@PLT
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE252:
	.size	BUF_MEM_new, .-BUF_MEM_new
	.p2align 4
	.globl	BUF_MEM_free
	.type	BUF_MEM_free, @function
BUF_MEM_free:
.LFB253:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L11
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L13
	movq	16(%r12), %rsi
	testb	$1, 24(%r12)
	je	.L14
	movl	$49, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_secure_clear_free@PLT
.L13:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$53, %edx
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	movl	$51, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE253:
	.size	BUF_MEM_free, .-BUF_MEM_free
	.p2align 4
	.globl	BUF_MEM_grow
	.type	BUF_MEM_grow, @function
BUF_MEM_grow:
.LFB255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rdi
	movq	%rsi, %rbx
	cmpq	%rsi, %rdi
	jnb	.L43
	cmpq	%rsi, 16(%r12)
	jnb	.L44
	movl	$90, %r8d
	cmpq	$1610612732, %rsi
	ja	.L42
	movabsq	$-6148914691236517205, %r13
	leaq	3(%rsi), %rdx
	movq	%rdx, %rax
	mulq	%r13
	shrq	%rdx
	leaq	0(,%rdx,4), %r13
	testb	$1, 24(%r12)
	je	.L27
	leaq	.LC0(%rip), %rsi
	movl	$62, %edx
	movq	%r13, %rdi
	call	CRYPTO_secure_malloc@PLT
	movq	8(%r12), %rsi
	movq	%rax, %r14
	testq	%rsi, %rsi
	je	.L28
	testq	%rax, %rax
	je	.L31
	movq	(%r12), %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	8(%r12), %rdi
	movq	(%r12), %rsi
	movl	$66, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_secure_clear_free@PLT
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L27:
	movq	8(%r12), %rdi
	movl	$97, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r13, %rsi
	call	CRYPTO_realloc@PLT
	movq	%rax, %r14
.L28:
	testq	%r14, %r14
	je	.L31
.L30:
	movq	(%r12), %rdi
	movq	%r14, 8(%r12)
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%r13, 16(%r12)
	subq	%rdi, %rdx
	addq	%r14, %rdi
	call	memset@PLT
.L43:
	movq	%rbx, (%r12)
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L43
	movq	%rsi, %rdx
	xorl	%esi, %esi
	subq	%rdi, %rdx
	addq	%rax, %rdi
	call	memset@PLT
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L31:
	movl	$99, %r8d
.L42:
	movl	$65, %edx
	movl	$100, %esi
	movl	$7, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE255:
	.size	BUF_MEM_grow, .-BUF_MEM_grow
	.p2align 4
	.globl	BUF_MEM_grow_clean
	.type	BUF_MEM_grow_clean, @function
BUF_MEM_grow_clean:
.LFB256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rdx
	movq	%rsi, %rbx
	cmpq	%rsi, %rdx
	jnb	.L68
	movq	16(%rdi), %rsi
	cmpq	%rbx, %rsi
	jnb	.L69
	movl	$128, %r8d
	cmpq	$1610612732, %rbx
	ja	.L66
	movabsq	$-6148914691236517205, %r13
	leaq	3(%rbx), %rdx
	movq	%rdx, %rax
	mulq	%r13
	shrq	%rdx
	leaq	0(,%rdx,4), %r13
	testb	$1, 24(%rdi)
	je	.L51
	leaq	.LC0(%rip), %rsi
	movl	$62, %edx
	movq	%r13, %rdi
	call	CRYPTO_secure_malloc@PLT
	movq	8(%r12), %rsi
	movq	%rax, %r14
	testq	%rsi, %rsi
	je	.L52
	testq	%rax, %rax
	je	.L55
	movq	(%r12), %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	8(%r12), %rdi
	movq	(%r12), %rsi
	movl	$66, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_secure_clear_free@PLT
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L55:
	movl	$137, %r8d
.L66:
	movl	$65, %edx
	movl	$105, %esi
	movl	$7, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	movl	$135, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%r13, %rdx
	call	CRYPTO_clear_realloc@PLT
	movq	%rax, %r14
.L52:
	testq	%r14, %r14
	je	.L55
.L54:
	movq	(%r12), %rdi
	movq	%r14, 8(%r12)
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%r13, 16(%r12)
	subq	%rdi, %rdx
	addq	%r14, %rdi
	call	memset@PLT
.L67:
	movq	%rbx, (%r12)
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L67
	subq	%rsi, %rdx
	addq	%rsi, %rdi
	xorl	%esi, %esi
	call	memset@PLT
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L69:
	movq	8(%rdi), %rdi
	movq	%rbx, %r8
	xorl	%esi, %esi
	subq	%rdx, %r8
	addq	%rdx, %rdi
	movq	%r8, %rdx
	call	memset@PLT
	movq	%rbx, (%r12)
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE256:
	.size	BUF_MEM_grow_clean, .-BUF_MEM_grow_clean
	.p2align 4
	.globl	BUF_reverse
	.type	BUF_reverse, @function
BUF_reverse:
.LFB257:
	.cfi_startproc
	endbr64
	leaq	-1(%rdi,%rdx), %rax
	testq	%rsi, %rsi
	je	.L71
	subq	$1, %rdi
	testq	%rdx, %rdx
	je	.L70
	.p2align 4,,10
	.p2align 3
.L73:
	movzbl	(%rsi), %edx
	subq	$1, %rax
	addq	$1, %rsi
	movb	%dl, 1(%rax)
	cmpq	%rdi, %rax
	jne	.L73
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	shrq	%rdx
	je	.L70
	addq	%rdi, %rdx
	.p2align 4,,10
	.p2align 3
.L75:
	movzbl	(%rax), %ecx
	movzbl	(%rdi), %esi
	addq	$1, %rdi
	subq	$1, %rax
	movb	%sil, 1(%rax)
	movb	%cl, -1(%rdi)
	cmpq	%rdi, %rdx
	jne	.L75
.L70:
	ret
	.cfi_endproc
.LFE257:
	.size	BUF_reverse, .-BUF_reverse
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
