	.file	"p_seal.c"
	.text
	.p2align 4
	.globl	EVP_SealInit
	.type	EVP_SealInit, @function
EVP_SealInit:
.LFB779:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%rcx, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L5
	movq	%rsi, %r15
	call	EVP_CIPHER_CTX_reset@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	EVP_EncryptInit_ex@PLT
	testl	%eax, %eax
	je	.L6
.L5:
	movl	16(%rbp), %eax
	testl	%eax, %eax
	jle	.L13
	testq	%r12, %r12
	je	.L13
	leaq	-128(%rbp), %r15
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	EVP_CIPHER_CTX_rand_key@PLT
	testl	%eax, %eax
	jle	.L6
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_iv_length@PLT
	testl	%eax, %eax
	je	.L10
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_iv_length@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jle	.L8
.L10:
	movq	%r14, %r8
	movq	%r15, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	xorl	%r14d, %r14d
	call	EVP_EncryptInit_ex@PLT
	testl	%eax, %eax
	je	.L8
	.p2align 4,,10
	.p2align 3
.L9:
	movq	(%r12,%r14,8), %rcx
	movq	%rbx, %rdi
	movq	%rcx, -136(%rbp)
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	0(%r13,%r14,8), %rdi
	movq	-136(%rbp), %rcx
	movq	%r15, %rsi
	movl	%eax, %edx
	call	EVP_PKEY_encrypt_old@PLT
	movq	-144(%rbp), %rsi
	movl	%eax, (%rsi,%r14,4)
	testl	%eax, %eax
	jle	.L14
	addq	$1, %r14
	cmpl	%r14d, 16(%rbp)
	jg	.L9
	movl	16(%rbp), %eax
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$1, %eax
.L1:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L22
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L8:
	xorl	%eax, %eax
.L11:
	movl	$64, %esi
	movq	%r15, %rdi
	movl	%eax, -136(%rbp)
	call	OPENSSL_cleanse@PLT
	movl	-136(%rbp), %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L14:
	movl	$-1, %eax
	jmp	.L11
.L22:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE779:
	.size	EVP_SealInit, .-EVP_SealInit
	.p2align 4
	.globl	EVP_SealFinal
	.type	EVP_SealFinal, @function
EVP_SealFinal:
.LFB780:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	EVP_EncryptFinal_ex@PLT
	testl	%eax, %eax
	jne	.L26
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	popq	%r12
	xorl	%edx, %edx
	xorl	%esi, %esi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	EVP_EncryptInit_ex@PLT
	.cfi_endproc
.LFE780:
	.size	EVP_SealFinal, .-EVP_SealFinal
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
