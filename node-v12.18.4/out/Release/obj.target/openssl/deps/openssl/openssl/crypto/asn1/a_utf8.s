	.file	"a_utf8.c"
	.text
	.p2align 4
	.globl	UTF8_getc
	.type	UTF8_getc, @function
UTF8_getc:
.LFB419:
	.cfi_startproc
	endbr64
	xorl	%r8d, %r8d
	testl	%esi, %esi
	jle	.L1
	movzbl	(%rdi), %eax
	movl	$1, %r8d
	testb	%al, %al
	js	.L37
.L4:
	movq	%rax, (%rdx)
.L1:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	movl	%eax, %ecx
	movzbl	%al, %r8d
	andl	$-32, %ecx
	cmpb	$-64, %cl
	je	.L38
	movl	%eax, %ecx
	andl	$-16, %ecx
	cmpb	$-32, %cl
	je	.L39
	movl	%eax, %ecx
	andl	$-8, %ecx
	cmpb	$-16, %cl
	je	.L40
	movl	%eax, %ecx
	andl	$-4, %ecx
	cmpb	$-8, %cl
	je	.L41
	movl	%eax, %ecx
	andl	$-2, %ecx
	cmpb	$-4, %cl
	jne	.L28
	cmpl	$5, %esi
	jle	.L29
	movzbl	1(%rdi), %ecx
	movl	%ecx, %esi
	andl	$-64, %esi
	cmpb	$-128, %sil
	jne	.L34
	movzbl	2(%rdi), %esi
	movl	%esi, %r8d
	andl	$-64, %r8d
	cmpb	$-128, %r8b
	jne	.L34
	movzbl	3(%rdi), %r9d
	movl	%r9d, %r8d
	andl	$-64, %r8d
	cmpb	$-128, %r8b
	jne	.L34
	movzbl	4(%rdi), %r8d
	movl	%r8d, %r10d
	andl	$-64, %r10d
	cmpb	$-128, %r10b
	jne	.L34
	movzbl	5(%rdi), %edi
	movl	%edi, %r10d
	andl	$-64, %r10d
	cmpb	$-128, %r10b
	jne	.L34
	salq	$30, %rax
	salq	$24, %rcx
	andl	$63, %edi
	andl	$1073741824, %eax
	andl	$1056964608, %ecx
	salq	$18, %rsi
	orq	%rcx, %rax
	andl	$16515072, %esi
	salq	$12, %r9
	orq	%rdi, %rax
	andl	$258048, %r9d
	sall	$6, %r8d
	orq	%rsi, %rax
	andl	$4032, %r8d
	orq	%r9, %rax
	orq	%r8, %rax
	cmpq	$67108863, %rax
	jbe	.L35
	movl	$6, %r8d
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L38:
	cmpl	$1, %esi
	je	.L29
	movzbl	1(%rdi), %ecx
	movl	%ecx, %eax
	andl	$-64, %eax
	cmpb	$-128, %al
	jne	.L34
	sall	$6, %r8d
	movq	%rcx, %rax
	andl	$1984, %r8d
	andl	$63, %eax
	orq	%r8, %rax
	cmpq	$127, %rax
	jbe	.L35
	movl	$2, %r8d
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L39:
	cmpl	$2, %esi
	jle	.L29
	movzbl	1(%rdi), %esi
	movl	%esi, %eax
	andl	$-64, %eax
	cmpb	$-128, %al
	jne	.L34
	movzbl	2(%rdi), %ecx
	movl	%ecx, %eax
	andl	$-64, %eax
	cmpb	$-128, %al
	jne	.L34
	sall	$12, %r8d
	sall	$6, %esi
	andl	$4032, %esi
	movzwl	%r8w, %eax
	orl	%esi, %eax
	movslq	%eax, %rsi
	movq	%rcx, %rax
	andl	$63, %eax
	orq	%rsi, %rax
	cmpq	$2047, %rax
	jbe	.L35
	movl	$3, %r8d
	jmp	.L4
.L35:
	movl	$-4, %r8d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L40:
	cmpl	$3, %esi
	jle	.L29
	movzbl	1(%rdi), %ecx
	movl	%ecx, %esi
	andl	$-64, %esi
	cmpb	$-128, %sil
	jne	.L34
	movzbl	2(%rdi), %esi
	movl	%esi, %r8d
	andl	$-64, %r8d
	cmpb	$-128, %r8b
	jne	.L34
	movzbl	3(%rdi), %edi
	movl	%edi, %r8d
	andl	$-64, %r8d
	cmpb	$-128, %r8b
	jne	.L34
	salq	$18, %rax
	andl	$63, %edi
	sall	$12, %ecx
	andl	$1835008, %eax
	andl	$258048, %ecx
	sall	$6, %esi
	orq	%rdi, %rax
	andl	$4032, %esi
	orq	%rcx, %rax
	orq	%rsi, %rax
	cmpq	$65535, %rax
	jbe	.L35
	movl	$4, %r8d
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L41:
	cmpl	$4, %esi
	jle	.L29
	movzbl	1(%rdi), %ecx
	movl	%ecx, %esi
	andl	$-64, %esi
	cmpb	$-128, %sil
	jne	.L34
	movzbl	2(%rdi), %esi
	movl	%esi, %r8d
	andl	$-64, %r8d
	cmpb	$-128, %r8b
	jne	.L34
	movzbl	3(%rdi), %r8d
	movl	%r8d, %r9d
	andl	$-64, %r9d
	cmpb	$-128, %r9b
	jne	.L34
	movzbl	4(%rdi), %edi
	movl	%edi, %r9d
	andl	$-64, %r9d
	cmpb	$-128, %r9b
	jne	.L34
	salq	$24, %rax
	salq	$18, %rcx
	andl	$63, %edi
	salq	$12, %rsi
	andl	$50331648, %eax
	sall	$6, %r8d
	movq	%rax, %r9
	movq	%rcx, %rax
	andl	$258048, %esi
	andl	$4032, %r8d
	andl	$16515072, %eax
	orq	%r9, %rax
	orq	%rdi, %rax
	orq	%rsi, %rax
	orq	%r8, %rax
	cmpq	$2097151, %rax
	jbe	.L35
	movl	$5, %r8d
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L34:
	movl	$-3, %r8d
	jmp	.L1
.L29:
	movl	$-1, %r8d
	jmp	.L1
.L28:
	movl	$-2, %r8d
	jmp	.L1
	.cfi_endproc
.LFE419:
	.size	UTF8_getc, .-UTF8_getc
	.p2align 4
	.globl	UTF8_putc
	.type	UTF8_putc, @function
UTF8_putc:
.LFB420:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L67
	testl	%esi, %esi
	jle	.L62
	cmpq	$127, %rdx
	jbe	.L68
	cmpq	$2047, %rdx
	ja	.L47
	cmpl	$1, %esi
	je	.L62
	movq	%rdx, %rax
	andl	$63, %edx
	shrq	$6, %rax
	orl	$-128, %edx
	orl	$-64, %eax
	movb	%dl, 1(%rdi)
	movb	%al, (%rdi)
	movl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	movb	%dl, (%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	movl	$1, %eax
	cmpq	$127, %rdx
	jbe	.L42
	movl	$2, %eax
	cmpq	$2047, %rdx
	jbe	.L42
	movl	$3, %eax
	cmpq	$65535, %rdx
	jbe	.L42
	movl	$4, %eax
	cmpq	$2097151, %rdx
	jbe	.L42
	xorl	%eax, %eax
	cmpq	$67108863, %rdx
	seta	%al
	addl	$5, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	cmpq	$65535, %rdx
	jbe	.L69
	cmpq	$2097151, %rdx
	ja	.L50
	cmpl	$3, %esi
	jle	.L62
	movq	%rdx, %rax
	shrq	$18, %rax
	orl	$-16, %eax
	movb	%al, (%rdi)
	movq	%rdx, %rax
	shrq	$12, %rax
	andl	$63, %eax
	orl	$-128, %eax
	movb	%al, 1(%rdi)
	movq	%rdx, %rax
	andl	$63, %edx
	shrq	$6, %rax
	orl	$-128, %edx
	andl	$63, %eax
	movb	%dl, 3(%rdi)
	orl	$-128, %eax
	movb	%al, 2(%rdi)
	movl	$4, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	cmpl	$2, %esi
	jle	.L62
	movq	%rdx, %rax
	shrq	$12, %rax
	orl	$-32, %eax
	movb	%al, (%rdi)
	movq	%rdx, %rax
	andl	$63, %edx
	shrq	$6, %rax
	orl	$-128, %edx
	andl	$63, %eax
	movb	%dl, 2(%rdi)
	orl	$-128, %eax
	movb	%al, 1(%rdi)
	movl	$3, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	cmpq	$67108863, %rdx
	ja	.L51
	cmpl	$4, %esi
	jle	.L62
	movq	%rdx, %rax
	shrq	$24, %rax
	orl	$-8, %eax
	movb	%al, (%rdi)
	movq	%rdx, %rax
	shrq	$18, %rax
	andl	$63, %eax
	orl	$-128, %eax
	movb	%al, 1(%rdi)
	movq	%rdx, %rax
	shrq	$12, %rax
	andl	$63, %eax
	orl	$-128, %eax
	movb	%al, 2(%rdi)
	movq	%rdx, %rax
	andl	$63, %edx
	shrq	$6, %rax
	orl	$-128, %edx
	andl	$63, %eax
	movb	%dl, 4(%rdi)
	orl	$-128, %eax
	movb	%al, 3(%rdi)
	movl	$5, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	cmpl	$5, %esi
	jle	.L62
	movq	%rdx, %rax
	shrq	$30, %rax
	andl	$1, %eax
	orl	$-4, %eax
	movb	%al, (%rdi)
	movq	%rdx, %rax
	shrq	$24, %rax
	andl	$63, %eax
	orl	$-128, %eax
	movb	%al, 1(%rdi)
	movq	%rdx, %rax
	shrq	$18, %rax
	andl	$63, %eax
	orl	$-128, %eax
	movb	%al, 2(%rdi)
	movq	%rdx, %rax
	shrq	$12, %rax
	andl	$63, %eax
	orl	$-128, %eax
	movb	%al, 3(%rdi)
	movq	%rdx, %rax
	andl	$63, %edx
	shrq	$6, %rax
	orl	$-128, %edx
	andl	$63, %eax
	movb	%dl, 5(%rdi)
	orl	$-128, %eax
	movb	%al, 4(%rdi)
	movl	$6, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE420:
	.size	UTF8_putc, .-UTF8_putc
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
