	.file	"cryptlib.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"OPENSSL_ia32cap"
	.text
	.p2align 4
	.globl	OPENSSL_cpuid_setup
	.type	OPENSSL_cpuid_setup, @function
OPENSSL_cpuid_setup:
.LFB265:
	.cfi_startproc
	endbr64
	movl	trigger.7011(%rip), %eax
	testl	%eax, %eax
	je	.L43
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	$1, trigger.7011(%rip)
	call	getenv@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L3
	movzbl	(%rax), %eax
	xorl	%r13d, %r13d
	movl	$10, %r14d
	cmpb	$126, %al
	movb	%al, -64(%rbp)
	sete	%r13b
	addq	%r12, %r13
	cmpb	$48, 0(%r13)
	je	.L44
.L4:
	movl	%r14d, %eax
	xorl	%ebx, %ebx
	movq	%rax, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L6:
	movsbl	0(%r13), %r15d
	movl	$4, %esi
	addq	$1, %r13
	movl	%r15d, %edi
	call	ossl_ctype_check@PLT
	leal	-48(%r15), %edi
	testl	%eax, %eax
	jne	.L8
	movl	$16, %esi
	movl	%r15d, %edi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	jne	.L9
.L13:
	cmpb	$126, -64(%rbp)
	je	.L45
	movzbl	(%r12), %r15d
	cmpb	$58, %r15b
	je	.L46
.L15:
	testb	%r15b, %r15b
	jne	.L18
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L47:
	addq	$1, %r12
	testb	%r15b, %r15b
	je	.L16
.L18:
	movl	%r15d, %eax
	movzbl	1(%r12), %r15d
	cmpb	$58, %al
	jne	.L47
	xorl	%eax, %eax
	cmpb	$126, %r15b
	sete	%al
	leaq	1(%r12,%rax), %r13
	cmpb	$48, 0(%r13)
	je	.L30
	movl	$10, -56(%rbp)
.L20:
	movl	-56(%rbp), %eax
	xorl	%r12d, %r12d
	movq	%rax, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L21:
	movsbl	0(%r13), %r14d
	movl	$4, %esi
	addq	$1, %r13
	movl	%r14d, %edi
	call	ossl_ctype_check@PLT
	leal	-48(%r14), %edi
	testl	%eax, %eax
	jne	.L23
	movl	$16, %esi
	movl	%r14d, %edi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	jne	.L24
.L28:
	movq	%r12, %rax
	shrq	$32, %rax
	cmpb	$126, %r15b
	je	.L48
	movq	%r12, 8+OPENSSL_ia32cap_P(%rip)
.L29:
	movl	%ebx, %eax
	shrq	$32, %rbx
	orb	$4, %ah
	movl	%ebx, 4+OPENSSL_ia32cap_P(%rip)
	movl	%eax, OPENSSL_ia32cap_P(%rip)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	movl	%r15d, %edi
	call	ossl_tolower@PLT
	leal	-87(%rax), %edi
.L8:
	cmpl	%edi, %r14d
	jbe	.L13
	imulq	-56(%rbp), %rbx
	addq	%rdi, %rbx
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L24:
	movl	%r14d, %edi
	call	ossl_tolower@PLT
	leal	-87(%rax), %edi
.L23:
	cmpl	%edi, -56(%rbp)
	jbe	.L28
	imulq	-64(%rbp), %r12
	addq	%rdi, %r12
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L16:
	movq	$0, 8+OPENSSL_ia32cap_P(%rip)
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L3:
	leaq	OPENSSL_ia32cap_P(%rip), %rdi
	call	OPENSSL_ia32_cpuid@PLT
	movq	%rax, %rbx
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L45:
	leaq	OPENSSL_ia32cap_P(%rip), %rdi
	call	OPENSSL_ia32_cpuid@PLT
	movq	%rax, %r8
	movq	%rbx, %rax
	notq	%rax
	andq	%r8, %rax
	testl	$16777216, %ebx
	je	.L49
	movabsq	$-1297045497365659649, %rbx
	movzbl	(%r12), %r15d
	andq	%rax, %rbx
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L44:
	movsbl	1(%r13), %edi
	call	ossl_tolower@PLT
	cmpl	$120, %eax
	je	.L5
	addq	$1, %r13
	movl	$8, %r14d
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L48:
	notl	%r12d
	notl	%eax
	andl	%r12d, 8+OPENSSL_ia32cap_P(%rip)
	andl	%eax, 12+OPENSSL_ia32cap_P(%rip)
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L46:
	leaq	OPENSSL_ia32cap_P(%rip), %rdi
	call	OPENSSL_ia32_cpuid@PLT
	movzbl	(%r12), %r15d
	movq	%rax, %rbx
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L49:
	movzbl	(%r12), %r15d
	movq	%rax, %rbx
	jmp	.L15
.L5:
	addq	$2, %r13
	movl	$16, %r14d
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L30:
	movsbl	1(%r13), %edi
	call	ossl_tolower@PLT
	cmpl	$120, %eax
	je	.L50
	movl	$8, -56(%rbp)
	addq	$1, %r13
	jmp	.L20
.L50:
	movl	$16, -56(%rbp)
	addq	$2, %r13
	jmp	.L20
	.cfi_endproc
.LFE265:
	.size	OPENSSL_cpuid_setup, .-OPENSSL_cpuid_setup
	.p2align 4
	.globl	OPENSSL_showfatal
	.type	OPENSSL_showfatal, @function
OPENSSL_showfatal:
.LFB266:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$208, %rsp
	movq	%rsi, -168(%rbp)
	movq	%rdx, -160(%rbp)
	movq	%rcx, -152(%rbp)
	movq	%r8, -144(%rbp)
	movq	%r9, -136(%rbp)
	testb	%al, %al
	je	.L52
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm4, -64(%rbp)
	movaps	%xmm5, -48(%rbp)
	movaps	%xmm6, -32(%rbp)
	movaps	%xmm7, -16(%rbp)
.L52:
	movq	%fs:40, %rax
	movq	%rax, -184(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	movq	%r10, %rdx
	movl	$1, %esi
	movq	stderr(%rip), %rdi
	movq	%rax, -200(%rbp)
	leaq	-208(%rbp), %rcx
	leaq	-176(%rbp), %rax
	movl	$8, -208(%rbp)
	movl	$48, -204(%rbp)
	movq	%rax, -192(%rbp)
	call	__vfprintf_chk@PLT
	movq	-184(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L55
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L55:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE266:
	.size	OPENSSL_showfatal, .-OPENSSL_showfatal
	.p2align 4
	.globl	OPENSSL_isservice
	.type	OPENSSL_isservice, @function
OPENSSL_isservice:
.LFB267:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE267:
	.size	OPENSSL_isservice, .-OPENSSL_isservice
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"%s:%d: OpenSSL internal error: %s\n"
	.text
	.p2align 4
	.globl	OPENSSL_die
	.type	OPENSSL_die, @function
OPENSSL_die:
.LFB268:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rcx
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	OPENSSL_showfatal
	call	abort@PLT
	.cfi_endproc
.LFE268:
	.size	OPENSSL_die, .-OPENSSL_die
	.local	trigger.7011
	.comm	trigger.7011,4,4
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
