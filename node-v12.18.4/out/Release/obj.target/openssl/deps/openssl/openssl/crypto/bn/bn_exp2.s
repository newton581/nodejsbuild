	.file	"bn_exp2.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/bn/bn_exp2.c"
	.text
	.p2align 4
	.globl	BN_mod_exp2_mont
	.type	BN_mod_exp2_mont, @function
BN_mod_exp2_mont:
.LFB252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$648, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rbp), %rax
	movq	%rdi, -600(%rbp)
	movq	%rcx, -608(%rbp)
	movq	16(%rbp), %r12
	movq	%r8, -592(%rbp)
	movq	%rax, -584(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%r9), %rax
	movq	(%rax), %rax
	andl	$1, %eax
	movq	%rax, -616(%rbp)
	je	.L149
	movq	%rdx, %rdi
	movq	%rsi, %r14
	movq	%rdx, %rbx
	movq	%r9, %r13
	call	BN_num_bits@PLT
	movq	-592(%rbp), %rdi
	movl	%eax, %r15d
	movl	%eax, -620(%rbp)
	call	BN_num_bits@PLT
	movl	%r15d, %ecx
	orl	%eax, %ecx
	movl	%eax, -624(%rbp)
	je	.L150
	movq	%r12, %rdi
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -656(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -648(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -576(%rbp)
	movq	%rax, %r15
	call	BN_CTX_get@PLT
	movq	%rax, -664(%rbp)
	movq	%rax, -320(%rbp)
	testq	%rax, %rax
	je	.L49
	movq	-584(%rbp), %rax
	movq	%rax, -632(%rbp)
	testq	%rax, %rax
	je	.L151
.L7:
	movl	-620(%rbp), %eax
	cmpl	$671, %eax
	jle	.L152
	movl	$32, -636(%rbp)
	movl	$6, -640(%rbp)
.L9:
	movl	-624(%rbp), %eax
	cmpl	$671, %eax
	jg	.L56
	cmpl	$239, %eax
	jg	.L57
	cmpl	$79, %eax
	jg	.L58
	cmpl	$24, %eax
	setge	%al
	movzbl	%al, %eax
	leal	1(%rax,%rax,2), %eax
	movl	%eax, -668(%rbp)
	setge	%al
	movzbl	%al, %eax
	leal	1(%rax,%rax), %eax
	movl	%eax, -672(%rbp)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L150:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L147
	movq	-600(%rbp), %rdi
	addq	$648, %rsp
	movl	$1, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BN_set_word@PLT
	.p2align 4,,10
	.p2align 3
.L149:
	.cfi_restore_state
	movl	$36, %r8d
	movl	$102, %edx
	movl	$118, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L1:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L147
	addq	$648, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore_state
	cmpl	$239, %eax
	jg	.L53
	cmpl	$79, %eax
	jg	.L54
	cmpl	$24, %eax
	setge	%al
	movzbl	%al, %eax
	leal	1(%rax,%rax,2), %eax
	movl	%eax, -636(%rbp)
	setge	%al
	movzbl	%al, %eax
	leal	1(%rax,%rax), %eax
	movl	%eax, -640(%rbp)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L56:
	movl	$32, -668(%rbp)
	movl	$6, -672(%rbp)
.L10:
	movl	16(%r14), %ecx
	testl	%ecx, %ecx
	jne	.L13
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	js	.L12
.L13:
	movq	%r14, %rdx
	movq	%r12, %r8
	movq	%r13, %rcx
	movq	%r15, %rsi
	xorl	%edi, %edi
	movq	%r15, %r14
	call	BN_div@PLT
	testl	%eax, %eax
	je	.L6
.L12:
	movq	%r14, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L146
	movq	-632(%rbp), %rdx
	movq	%r12, %rcx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	BN_to_montgomery@PLT
	testl	%eax, %eax
	je	.L6
	cmpl	$1, -640(%rbp)
	je	.L19
	movq	-632(%rbp), %rcx
	movq	%r12, %r8
	movq	%r15, %rdx
	movq	%r15, %rsi
	movq	-656(%rbp), %rdi
	call	BN_mod_mul_montgomery@PLT
	testl	%eax, %eax
	je	.L6
	movq	%rbx, -680(%rbp)
	movq	-616(%rbp), %r15
	leaq	-576(%rbp), %r14
	movq	%r13, -688(%rbp)
	movq	-632(%rbp), %rbx
	movq	-656(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L18:
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, (%r14,%r15,8)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L61
	movq	-8(%r14,%r15,8), %rsi
	movq	%r12, %r8
	movq	%rbx, %rcx
	movq	%r13, %rdx
	call	BN_mod_mul_montgomery@PLT
	testl	%eax, %eax
	je	.L6
	addq	$1, %r15
	cmpl	%r15d, -636(%rbp)
	jg	.L18
	movq	-680(%rbp), %rbx
	movq	-688(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L19:
	movq	-608(%rbp), %rax
	movl	16(%rax), %edx
	testl	%edx, %edx
	je	.L153
.L16:
	movq	-664(%rbp), %r14
	xorl	%edi, %edi
	movq	%r12, %r8
	movq	%r13, %rcx
	movq	-608(%rbp), %rdx
	movq	%r14, %rsi
	call	BN_div@PLT
	testl	%eax, %eax
	je	.L6
	movq	%r14, -608(%rbp)
.L20:
	movq	-608(%rbp), %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L146
	movq	-632(%rbp), %rdx
	movq	-608(%rbp), %rsi
	movq	%r12, %rcx
	movq	-664(%rbp), %rdi
	call	BN_to_montgomery@PLT
	testl	%eax, %eax
	je	.L6
	cmpl	$1, -672(%rbp)
	je	.L25
	movq	-664(%rbp), %rdx
	movq	-656(%rbp), %r14
	movq	%r12, %r8
	movq	-632(%rbp), %rcx
	movq	%rdx, %rsi
	movq	%r14, %rdi
	call	BN_mod_mul_montgomery@PLT
	testl	%eax, %eax
	je	.L6
	movq	%rbx, -608(%rbp)
	movq	-616(%rbp), %r15
	leaq	-320(%rbp), %r13
	movq	-632(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L24:
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, 0(%r13,%r15,8)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L61
	movq	-8(%r13,%r15,8), %rsi
	movq	%r12, %r8
	movq	%rbx, %rcx
	movq	%r14, %rdx
	call	BN_mod_mul_montgomery@PLT
	testl	%eax, %eax
	je	.L6
	addq	$1, %r15
	cmpl	%r15d, -668(%rbp)
	jg	.L24
	movq	-608(%rbp), %rbx
.L25:
	call	BN_value_one@PLT
	movq	-632(%rbp), %rdx
	movq	-648(%rbp), %rdi
	movq	%r12, %rcx
	movq	%rax, %rsi
	call	BN_to_montgomery@PLT
	testl	%eax, %eax
	je	.L6
	movl	-620(%rbp), %edx
	movl	-624(%rbp), %eax
	cmpl	%eax, %edx
	cmovge	%edx, %eax
	subl	$1, %eax
	movl	%eax, %r13d
	js	.L43
	movl	$1, %eax
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	movl	$0, -620(%rbp)
	movl	%eax, -608(%rbp)
	subl	-672(%rbp), %eax
	movl	$0, -624(%rbp)
	movl	%eax, -668(%rbp)
	movq	%r12, -616(%rbp)
	movq	%rbx, %r12
	.p2align 4,,10
	.p2align 3
.L26:
	movl	$1, %eax
	testl	%r15d, %r15d
	je	.L154
.L29:
	cmpl	%r13d, -620(%rbp)
	sete	%bl
	andl	%eax, %ebx
	testl	%r14d, %r14d
	je	.L155
.L35:
	testb	%bl, %bl
	jne	.L156
.L41:
	testl	%r14d, %r14d
	je	.L42
.L134:
	cmpl	-624(%rbp), %r13d
	jne	.L42
	sarl	%r14d
	movq	-648(%rbp), %rsi
	movq	-616(%rbp), %r8
	movslq	%r14d, %r14
	movq	-632(%rbp), %rcx
	movq	-320(%rbp,%r14,8), %rdx
	movq	%rsi, %rdi
	call	BN_mod_mul_montgomery@PLT
	testl	%eax, %eax
	je	.L142
	subl	$1, %r13d
	cmpl	$-1, %r13d
	je	.L145
	xorl	%r14d, %r14d
.L27:
	movq	-648(%rbp), %rsi
	movq	-616(%rbp), %r8
	movq	-632(%rbp), %rcx
	movq	%rsi, %rdx
	movq	%rsi, %rdi
	call	BN_mod_mul_montgomery@PLT
	testl	%eax, %eax
	je	.L142
	movl	$0, -608(%rbp)
	movl	$1, %eax
	testl	%r15d, %r15d
	jne	.L29
.L154:
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	BN_is_bit_set@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	jne	.L157
	testl	%r14d, %r14d
	jne	.L134
	movq	-592(%rbp), %rdi
	movl	%r13d, %esi
	call	BN_is_bit_set@PLT
	movl	%eax, %r15d
	movl	%eax, %r14d
	testl	%eax, %eax
	jne	.L158
	.p2align 4,,10
	.p2align 3
.L42:
	subl	$1, %r13d
	cmpl	$-1, %r13d
	je	.L145
	movl	-608(%rbp), %eax
	testl	%eax, %eax
	jne	.L26
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L61:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L6:
	cmpq	$0, -584(%rbp)
	je	.L8
.L45:
	movq	%r12, %rdi
	movl	%eax, -584(%rbp)
	call	BN_CTX_end@PLT
	movl	-584(%rbp), %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L146:
	movq	-600(%rbp), %rdi
	xorl	%esi, %esi
	call	BN_set_word@PLT
	movl	$1, %eax
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L57:
	movl	$16, -668(%rbp)
	movl	$5, -672(%rbp)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L53:
	movl	$16, -636(%rbp)
	movl	$5, -640(%rbp)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L49:
	movq	$0, -632(%rbp)
	xorl	%eax, %eax
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L151:
	call	BN_MONT_CTX_new@PLT
	movq	%rax, -632(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L159
	xorl	%eax, %eax
.L8:
	movq	-632(%rbp), %rdi
	movl	%eax, -584(%rbp)
	call	BN_MONT_CTX_free@PLT
	movl	-584(%rbp), %eax
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L159:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	BN_MONT_CTX_set@PLT
	testl	%eax, %eax
	jne	.L7
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L54:
	movl	$8, -636(%rbp)
	movl	$4, -640(%rbp)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L58:
	movl	$8, -668(%rbp)
	movl	$4, -672(%rbp)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L153:
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	jns	.L16
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L156:
	sarl	%r15d
	movq	-648(%rbp), %rsi
	movq	-616(%rbp), %r8
	movslq	%r15d, %r15
	movq	-632(%rbp), %rcx
	movq	-576(%rbp,%r15,8), %rdx
	movq	%rsi, %rdi
	call	BN_mod_mul_montgomery@PLT
	testl	%eax, %eax
	je	.L142
	movl	$0, -608(%rbp)
	xorl	%r15d, %r15d
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L155:
	movq	-592(%rbp), %rdi
	movl	%r13d, %esi
	call	BN_is_bit_set@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	jne	.L47
	testb	%bl, %bl
	je	.L42
	sarl	%r15d
	movq	-648(%rbp), %rsi
	movq	-616(%rbp), %r8
	movslq	%r15d, %rax
	movq	-632(%rbp), %rcx
	movq	-576(%rbp,%rax,8), %rdx
	movq	%rsi, %rdi
	call	BN_mod_mul_montgomery@PLT
	testl	%eax, %eax
	je	.L142
	subl	$1, %r13d
	cmpl	$-1, %r13d
	je	.L145
	xorl	%r15d, %r15d
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L157:
	movl	$1, %eax
	subl	-640(%rbp), %eax
	leal	(%rax,%r13), %ebx
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L32:
	addl	$1, %ebx
.L31:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	BN_is_bit_set@PLT
	testl	%eax, %eax
	je	.L32
	movl	%ebx, -620(%rbp)
	movl	%ebx, %eax
	leal	-1(%r13), %ebx
	cmpl	%ebx, %eax
	jg	.L64
	movl	$1, %r15d
	movl	%r14d, %edx
	subl	$1, %eax
	movl	%r13d, -636(%rbp)
	movl	%r15d, %r14d
	movq	%r12, %r13
	movl	%edx, %r15d
	movl	%eax, %r12d
	.p2align 4,,10
	.p2align 3
.L34:
	movl	%ebx, %esi
	movq	%r13, %rdi
	addl	%r14d, %r14d
	call	BN_is_bit_set@PLT
	cmpl	$1, %eax
	sbbl	$-1, %r14d
	subl	$1, %ebx
	cmpl	%r12d, %ebx
	jne	.L34
	movl	%r15d, %eax
	movl	%r14d, %r15d
	movq	%r13, %r12
	movl	-636(%rbp), %r13d
	testl	%r15d, %r15d
	movl	%eax, %r14d
	setne	%al
	jmp	.L29
.L158:
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
.L47:
	movl	-668(%rbp), %eax
	movb	%bl, -636(%rbp)
	movq	-592(%rbp), %r14
	addl	%r13d, %eax
	movl	%eax, %ebx
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L38:
	addl	$1, %ebx
.L37:
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	BN_is_bit_set@PLT
	testl	%eax, %eax
	je	.L38
	movl	%ebx, %eax
	leal	-1(%r13), %edx
	movl	%ebx, -624(%rbp)
	movzbl	-636(%rbp), %ebx
	cmpl	%edx, %eax
	jg	.L65
	movl	$1, %r14d
	movb	%bl, -656(%rbp)
	subl	$1, %eax
	movq	-592(%rbp), %rbx
	movl	%r13d, -636(%rbp)
	movl	%eax, %r13d
	movq	%r12, -664(%rbp)
	movl	%r14d, %r12d
	movl	%edx, %r14d
	.p2align 4,,10
	.p2align 3
.L40:
	movl	%r14d, %esi
	movq	%rbx, %rdi
	addl	%r12d, %r12d
	call	BN_is_bit_set@PLT
	cmpl	$1, %eax
	sbbl	$-1, %r12d
	subl	$1, %r14d
	cmpl	%r13d, %r14d
	jne	.L40
	movl	%r12d, %r14d
	movl	-636(%rbp), %r13d
	movzbl	-656(%rbp), %ebx
	movq	-664(%rbp), %r12
	jmp	.L35
.L145:
	movq	-616(%rbp), %r12
.L43:
	movq	-632(%rbp), %rdx
	movq	-648(%rbp), %rsi
	movq	%r12, %rcx
	movq	-600(%rbp), %rdi
	call	BN_from_montgomery@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L6
.L142:
	movq	-616(%rbp), %r12
	jmp	.L6
.L65:
	movl	$1, %r14d
	jmp	.L35
.L64:
	movl	$1, %eax
	movl	$1, %r15d
	jmp	.L29
.L147:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE252:
	.size	BN_mod_exp2_mont, .-BN_mod_exp2_mont
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
