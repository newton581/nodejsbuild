	.file	"p12_attr.c"
	.text
	.p2align 4
	.globl	PKCS12_add_localkeyid
	.type	PKCS12_add_localkeyid, @function
PKCS12_add_localkeyid:
.LFB803:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	movl	%edx, %r8d
	addq	$16, %rdi
	movl	$4, %edx
	movl	$157, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509at_add1_attr_by_NID@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE803:
	.size	PKCS12_add_localkeyid, .-PKCS12_add_localkeyid
	.p2align 4
	.globl	PKCS8_add_keyusage
	.type	PKCS8_add_keyusage, @function
PKCS8_add_keyusage:
.LFB804:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %edx
	movl	$1, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-9(%rbp), %rcx
	movb	%sil, -9(%rbp)
	movl	$83, %esi
	call	PKCS8_pkey_add1_attr_by_NID@PLT
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L7
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L7:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE804:
	.size	PKCS8_add_keyusage, .-PKCS8_add_keyusage
	.p2align 4
	.globl	PKCS12_add_friendlyname_asc
	.type	PKCS12_add_friendlyname_asc, @function
PKCS12_add_friendlyname_asc:
.LFB805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	movl	%edx, %r8d
	addq	$16, %rdi
	movl	$4097, %edx
	movl	$156, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509at_add1_attr_by_NID@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE805:
	.size	PKCS12_add_friendlyname_asc, .-PKCS12_add_friendlyname_asc
	.p2align 4
	.globl	PKCS12_add_friendlyname_utf8
	.type	PKCS12_add_friendlyname_utf8, @function
PKCS12_add_friendlyname_utf8:
.LFB806:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	movl	%edx, %r8d
	addq	$16, %rdi
	movl	$4096, %edx
	movl	$156, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509at_add1_attr_by_NID@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE806:
	.size	PKCS12_add_friendlyname_utf8, .-PKCS12_add_friendlyname_utf8
	.p2align 4
	.globl	PKCS12_add_friendlyname_uni
	.type	PKCS12_add_friendlyname_uni, @function
PKCS12_add_friendlyname_uni:
.LFB807:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	movl	%edx, %r8d
	addq	$16, %rdi
	movl	$4098, %edx
	movl	$156, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509at_add1_attr_by_NID@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE807:
	.size	PKCS12_add_friendlyname_uni, .-PKCS12_add_friendlyname_uni
	.p2align 4
	.globl	PKCS12_add_CSPName_asc
	.type	PKCS12_add_CSPName_asc, @function
PKCS12_add_CSPName_asc:
.LFB808:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	movl	%edx, %r8d
	addq	$16, %rdi
	movl	$4097, %edx
	movl	$417, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509at_add1_attr_by_NID@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE808:
	.size	PKCS12_add_CSPName_asc, .-PKCS12_add_CSPName_asc
	.p2align 4
	.globl	PKCS12_get_attr_gen
	.type	PKCS12_get_attr_gen, @function
PKCS12_get_attr_gen:
.LFB809:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	X509at_get_attr_by_NID@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	X509at_get_attr@PLT
	addq	$8, %rsp
	xorl	%esi, %esi
	popq	%r12
	movq	%rax, %rdi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	X509_ATTRIBUTE_get0_type@PLT
	.cfi_endproc
.LFE809:
	.size	PKCS12_get_attr_gen, .-PKCS12_get_attr_gen
	.p2align 4
	.globl	PKCS12_get_friendlyname
	.type	PKCS12_get_friendlyname, @function
PKCS12_get_friendlyname:
.LFB810:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$156, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	PKCS12_SAFEBAG_get0_attr@PLT
	testq	%rax, %rax
	je	.L18
	cmpl	$30, (%rax)
	jne	.L18
	movq	8(%rax), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	8(%rax), %rdi
	movl	(%rax), %esi
	jmp	OPENSSL_uni2utf8@PLT
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE810:
	.size	PKCS12_get_friendlyname, .-PKCS12_get_friendlyname
	.p2align 4
	.globl	PKCS12_SAFEBAG_get0_attrs
	.type	PKCS12_SAFEBAG_get0_attrs, @function
PKCS12_SAFEBAG_get0_attrs:
.LFB811:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE811:
	.size	PKCS12_SAFEBAG_get0_attrs, .-PKCS12_SAFEBAG_get0_attrs
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
