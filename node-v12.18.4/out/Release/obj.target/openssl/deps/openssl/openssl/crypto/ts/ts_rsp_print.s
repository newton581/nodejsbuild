	.file	"ts_rsp_print.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"yes"
.LC1:
	.string	"no"
.LC2:
	.string	"Version: %d\n"
.LC3:
	.string	"Policy OID: "
.LC4:
	.string	"Serial number: "
.LC5:
	.string	"unspecified"
.LC6:
	.string	"\n"
.LC7:
	.string	"Time stamp: "
.LC8:
	.string	"Accuracy: "
.LC9:
	.string	" seconds, "
.LC10:
	.string	" millis, "
.LC11:
	.string	" micros"
.LC12:
	.string	"Ordering: %s\n"
.LC13:
	.string	"Nonce: "
.LC14:
	.string	"TSA: "
	.text
	.p2align 4
	.type	TS_TST_INFO_print_bio.part.0, @function
TS_TST_INFO_print_bio.part.0:
.LFB1373:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rdi
	call	ASN1_INTEGER_get@PLT
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	8(%rbx), %rsi
	movq	%r12, %rdi
	call	TS_OBJ_print_bio@PLT
	movq	16(%rbx), %rsi
	movq	%r12, %rdi
	call	TS_MSG_IMPRINT_print_bio@PLT
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L24
	movq	%r12, %rdi
	call	TS_ASN1_INTEGER_print_bio@PLT
.L3:
	movl	$1, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	32(%rbx), %rsi
	movq	%r12, %rdi
	call	ASN1_GENERALIZEDTIME_print@PLT
	movl	$1, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	40(%rbx), %r13
	testq	%r13, %r13
	je	.L25
	movq	0(%r13), %rsi
	testq	%rsi, %rsi
	je	.L6
	movq	%r12, %rdi
	call	TS_ASN1_INTEGER_print_bio@PLT
.L7:
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	8(%r13), %rsi
	testq	%rsi, %rsi
	je	.L8
	movq	%r12, %rdi
	call	TS_ASN1_INTEGER_print_bio@PLT
.L9:
	leaq	.LC10(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	16(%r13), %rsi
	testq	%rsi, %rsi
	je	.L10
	movq	%r12, %rdi
	call	TS_ASN1_INTEGER_print_bio@PLT
.L11:
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L5:
	movl	$1, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	movl	48(%rbx), %eax
	leaq	.LC0(%rip), %rdx
	movq	%r12, %rdi
	leaq	.LC12(%rip), %rsi
	testl	%eax, %eax
	leaq	.LC1(%rip), %rax
	cmove	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	56(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L26
	movq	%r12, %rdi
	call	TS_ASN1_INTEGER_print_bio@PLT
.L14:
	movl	$1, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	64(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L27
	xorl	%edx, %edx
	xorl	%edi, %edi
	call	i2v_GENERAL_NAME@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L17
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	X509V3_EXT_val_prn@PLT
.L17:
	movq	X509V3_conf_free@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_pop_free@PLT
.L16:
	movq	%r12, %rdi
	movl	$1, %edx
	leaq	.LC6(%rip), %rsi
	call	BIO_write@PLT
	movq	72(%rbx), %rsi
	movq	%r12, %rdi
	call	TS_ext_print_bio@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L26:
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L27:
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L24:
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L25:
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L10:
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L8:
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L9
	.cfi_endproc
.LFE1373:
	.size	TS_TST_INFO_print_bio.part.0, .-TS_TST_INFO_print_bio.part.0
	.section	.rodata.str1.1
.LC15:
	.string	"Status: "
.LC16:
	.string	"%s\n"
.LC17:
	.string	"out of bounds\n"
.LC18:
	.string	"Status description: "
.LC19:
	.string	"\t"
.LC20:
	.string	"unspecified\n"
.LC21:
	.string	"Failure info: "
.LC22:
	.string	", "
.LC23:
	.string	"%s"
	.text
	.p2align 4
	.globl	TS_STATUS_INFO_print_bio
	.type	TS_STATUS_INFO_print_bio, @function
TS_STATUS_INFO_print_bio:
.LFB1369:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	leaq	.LC15(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	BIO_printf@PLT
	movq	0(%r13), %rdi
	call	ASN1_INTEGER_get@PLT
	cmpq	$5, %rax
	ja	.L29
	leaq	status_map.21156(%rip), %rdx
	leaq	.LC16(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rdx,%rax,8), %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L30:
	leaq	.LC18(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	call	BIO_printf@PLT
	leaq	.LC19(%rip), %r14
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L32:
	movq	8(%r13), %rdi
	movl	%ebx, %esi
	addl	$1, %ebx
	call	OPENSSL_sk_value@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	ASN1_STRING_print_ex@PLT
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
.L31:
	movq	8(%r13), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L56
	testl	%ebx, %ebx
	je	.L32
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L56:
	testl	%ebx, %ebx
	je	.L57
.L34:
	leaq	.LC21(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	16(%r13), %r14
	xorl	%esi, %esi
	xorl	%r13d, %r13d
	leaq	failure_map.21157(%rip), %rbx
	leaq	.LC23(%rip), %r15
	testq	%r14, %r14
	je	.L39
	.p2align 4,,10
	.p2align 3
.L35:
	movq	%r14, %rdi
	call	ASN1_BIT_STRING_get_bit@PLT
	testl	%eax, %eax
	je	.L37
	addl	$1, %r13d
	cmpl	$1, %r13d
	je	.L38
	leaq	.LC22(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L38:
	movq	8(%rbx), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L37:
	movl	16(%rbx), %esi
	addq	$16, %rbx
	testl	%esi, %esi
	jns	.L35
	testl	%r13d, %r13d
	jne	.L36
.L39:
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L36:
	movq	%r12, %rdi
	leaq	.LC6(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	leaq	.LC17(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L57:
	leaq	.LC20(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L34
	.cfi_endproc
.LFE1369:
	.size	TS_STATUS_INFO_print_bio, .-TS_STATUS_INFO_print_bio
	.section	.rodata.str1.1
.LC24:
	.string	"Status info:\n"
.LC25:
	.string	"\nTST info:\n"
.LC26:
	.string	"Not included.\n"
	.text
	.p2align 4
	.globl	TS_RESP_print_bio
	.type	TS_RESP_print_bio, @function
TS_RESP_print_bio:
.LFB1368:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	leaq	.LC24(%rip), %rsi
	call	BIO_printf@PLT
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	TS_STATUS_INFO_print_bio
	leaq	.LC25(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	16(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L59
	movq	%r12, %rdi
	call	TS_TST_INFO_print_bio.part.0
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	movq	%r12, %rdi
	leaq	.LC26(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1368:
	.size	TS_RESP_print_bio, .-TS_RESP_print_bio
	.p2align 4
	.globl	TS_TST_INFO_print_bio
	.type	TS_TST_INFO_print_bio, @function
TS_TST_INFO_print_bio:
.LFB1371:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L63
	jmp	TS_TST_INFO_print_bio.part.0
	.p2align 4,,10
	.p2align 3
.L63:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1371:
	.size	TS_TST_INFO_print_bio, .-TS_TST_INFO_print_bio
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC27:
	.string	"unrecognized or unsupported algorithm identifier"
	.align 8
.LC28:
	.string	"transaction not permitted or supported"
	.align 8
.LC29:
	.string	"the data submitted has the wrong format"
	.align 8
.LC30:
	.string	"the TSA's time source is not available"
	.align 8
.LC31:
	.string	"the requested TSA policy is not supported by the TSA"
	.align 8
.LC32:
	.string	"the requested extension is not supported by the TSA"
	.align 8
.LC33:
	.string	"the additional information requested could not be understood or is not available"
	.align 8
.LC34:
	.string	"the request cannot be handled due to system failure"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	failure_map.21157, @object
	.size	failure_map.21157, 144
failure_map.21157:
	.long	0
	.zero	4
	.quad	.LC27
	.long	2
	.zero	4
	.quad	.LC28
	.long	5
	.zero	4
	.quad	.LC29
	.long	14
	.zero	4
	.quad	.LC30
	.long	15
	.zero	4
	.quad	.LC31
	.long	16
	.zero	4
	.quad	.LC32
	.long	17
	.zero	4
	.quad	.LC33
	.long	25
	.zero	4
	.quad	.LC34
	.long	-1
	.zero	4
	.quad	0
	.section	.rodata.str1.1
.LC35:
	.string	"Granted."
.LC36:
	.string	"Granted with modifications."
.LC37:
	.string	"Rejected."
.LC38:
	.string	"Waiting."
.LC39:
	.string	"Revocation warning."
.LC40:
	.string	"Revoked."
	.section	.data.rel.ro.local
	.align 32
	.type	status_map.21156, @object
	.size	status_map.21156, 48
status_map.21156:
	.quad	.LC35
	.quad	.LC36
	.quad	.LC37
	.quad	.LC38
	.quad	.LC39
	.quad	.LC40
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
