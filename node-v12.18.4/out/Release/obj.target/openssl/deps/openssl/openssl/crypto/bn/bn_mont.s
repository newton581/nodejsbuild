	.file	"bn_mont.c"
	.text
	.p2align 4
	.type	bn_from_montgomery_word, @function
bn_from_montgomery_word:
.LFB254:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	40(%rdx), %r14d
	movq	%rdi, -72(%rbp)
	testl	%r14d, %r14d
	jne	.L2
	movl	$0, 8(%rdi)
	movl	$1, %eax
.L1:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movq	%rsi, %rbx
	leal	(%r14,%r14), %r12d
	movq	%rdx, %r13
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L15
	movl	48(%r13), %eax
	movq	32(%r13), %r11
	xorl	%eax, 16(%rbx)
	movq	(%rbx), %r15
	movl	8(%rbx), %ecx
	testl	%r12d, %r12d
	jle	.L11
	leal	-1(%r12), %eax
	cmpl	$2, %eax
	jbe	.L8
	movl	%r12d, %edx
	movd	%ecx, %xmm3
	pxor	%xmm4, %xmm4
	movq	%r15, %rax
	shrl	$2, %edx
	movdqa	.LC0(%rip), %xmm2
	pshufd	$0, %xmm3, %xmm6
	movdqa	.LC1(%rip), %xmm5
	salq	$5, %rdx
	pxor	%xmm3, %xmm3
	addq	%r15, %rdx
	.p2align 4,,10
	.p2align 3
.L9:
	movdqa	%xmm2, %xmm0
	movdqa	%xmm3, %xmm7
	paddd	%xmm5, %xmm2
	addq	$32, %rax
	psubd	%xmm6, %xmm0
	psrld	$31, %xmm0
	movdqa	%xmm0, %xmm1
	punpckldq	%xmm4, %xmm0
	punpckhdq	%xmm4, %xmm1
	psubq	%xmm1, %xmm7
	movdqa	%xmm7, %xmm1
	movdqu	-16(%rax), %xmm7
	pand	%xmm7, %xmm1
	movdqa	%xmm3, %xmm7
	psubq	%xmm0, %xmm7
	movups	%xmm1, -16(%rax)
	movdqa	%xmm7, %xmm0
	movdqu	-32(%rax), %xmm7
	pand	%xmm7, %xmm0
	movups	%xmm0, -32(%rax)
	cmpq	%rdx, %rax
	jne	.L9
	movl	%r12d, %eax
	andl	$-4, %eax
	testb	$3, %r12b
	je	.L11
	movl	%eax, %edx
	movslq	%eax, %rsi
	addl	$1, %eax
	subl	%ecx, %edx
	shrl	$31, %edx
	negq	%rdx
	andq	%rdx, (%r15,%rsi,8)
	cmpl	%eax, %r12d
	jle	.L11
.L24:
	movl	%eax, %edx
	movslq	%eax, %rsi
	addl	$1, %eax
	subl	%ecx, %edx
	shrl	$31, %edx
	negq	%rdx
	andq	%rdx, (%r15,%rsi,8)
	cmpl	%eax, %r12d
	jle	.L11
	movslq	%eax, %rdx
	subl	%ecx, %eax
	shrl	$31, %eax
	negq	%rax
	andq	%rax, (%r15,%rdx,8)
.L11:
	movl	%r12d, 8(%rbx)
	movq	80(%r13), %r12
	testl	%r14d, %r14d
	jle	.L46
	leal	-1(%r14), %eax
	movslq	%r14d, %r10
	xorl	%r8d, %r8d
	movq	%r12, -64(%rbp)
	movl	%eax, -84(%rbp)
	leaq	0(,%r10,8), %r13
	movq	%r11, %r12
	movq	%rax, -96(%rbp)
	leaq	8(%r15,%rax,8), %rax
	movq	%rax, -56(%rbp)
	movq	%rbx, -80(%rbp)
	movq	%r8, %rbx
	.p2align 4,,10
	.p2align 3
.L13:
	movq	-64(%rbp), %rcx
	imulq	(%r15), %rcx
	movl	%r14d, %edx
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	bn_mul_add_words@PLT
	movq	(%r15,%r13), %rcx
	xorl	%edx, %edx
	addq	%rbx, %rax
	addq	%rcx, %rax
	cmpq	%rax, %rcx
	movq	%rax, (%r15,%r13)
	setne	%dl
	orq	%rbx, %rdx
	movq	%rdx, %r8
	xorl	%edx, %edx
	cmpq	%rax, %rcx
	setnb	%dl
	addq	$8, %r15
	movq	%rdx, %rbx
	andq	%r8, %rbx
	cmpq	-56(%rbp), %r15
	jne	.L13
	movq	-72(%rbp), %rdi
	movq	%rbx, %r8
	movl	%r14d, %esi
	movq	%r12, -56(%rbp)
	movq	%r8, -64(%rbp)
	movq	-80(%rbp), %rbx
	call	bn_wexpand@PLT
	movq	-56(%rbp), %r11
	movq	-64(%rbp), %r8
	testq	%rax, %rax
	je	.L15
	movq	-72(%rbp), %rsi
	movl	16(%rbx), %eax
	movl	%r14d, %ecx
	movq	%r11, %rdx
	movq	(%rbx), %r15
	movq	%r8, -56(%rbp)
	movq	(%rsi), %r12
	movl	%r14d, 8(%rsi)
	movl	%eax, 16(%rsi)
	leaq	(%r15,%r13), %rbx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	bn_sub_words@PLT
	movq	-56(%rbp), %r8
	movq	%r8, %rdx
	subq	%rax, %rdx
	leaq	16(%r15,%r13), %rax
	cmpq	%rax, %r12
	leaq	16(%r12), %rax
	setnb	%cl
	cmpq	%rax, %rbx
	setnb	%al
	orb	%al, %cl
	je	.L25
	cmpl	$1, -84(%rbp)
	jbe	.L25
	movl	%r14d, %ecx
	movq	%rdx, %xmm2
	pxor	%xmm1, %xmm1
	xorl	%eax, %eax
	shrl	%ecx
	punpcklqdq	%xmm2, %xmm2
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L19:
	movdqu	(%r12,%rax), %xmm5
	movdqu	(%rbx,%rax), %xmm0
	pxor	%xmm5, %xmm0
	pand	%xmm2, %xmm0
	pxor	%xmm5, %xmm0
	movups	%xmm0, (%r12,%rax)
	movups	%xmm1, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L19
	movl	%r14d, %eax
	andl	$-2, %eax
	andl	$1, %r14d
	je	.L18
	salq	$3, %rax
	addq	%rax, %rbx
	addq	%r12, %rax
	movq	(%rax), %rcx
	movq	(%rbx), %r8
	xorq	%rcx, %r8
	andq	%rdx, %r8
	xorq	%rcx, %r8
	movq	%r8, (%rax)
	movq	$0, (%rbx)
.L18:
	addq	$56, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	addq	$56, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	movq	-96(%rbp), %rdi
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L17:
	movq	(%r12,%rax,8), %rsi
	movq	(%rbx,%rax,8), %rcx
	xorq	%rsi, %rcx
	andq	%rdx, %rcx
	xorq	%rsi, %rcx
	movq	%rcx, (%r12,%rax,8)
	movq	%rax, %rcx
	movq	$0, (%rbx,%rax,8)
	addq	$1, %rax
	cmpq	%rdi, %rcx
	jne	.L17
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L46:
	movq	-72(%rbp), %r15
	movl	%r14d, %esi
	movq	%r11, -56(%rbp)
	movq	%r15, %rdi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L15
	movl	16(%rbx), %eax
	movq	-56(%rbp), %r11
	movl	%r14d, 8(%r15)
	movslq	%r14d, %rdx
	movq	(%r15), %rdi
	movl	%r14d, %ecx
	movl	%eax, 16(%r15)
	movq	(%rbx), %rax
	leaq	(%rax,%rdx,8), %rsi
	movq	%r11, %rdx
	call	bn_sub_words@PLT
	movl	$1, %eax
	jmp	.L1
.L8:
	movl	%ecx, %eax
	negl	%eax
	shrl	$31, %eax
	negq	%rax
	andq	%rax, (%r15)
	movl	$1, %eax
	jmp	.L24
	.cfi_endproc
.LFE254:
	.size	bn_from_montgomery_word, .-bn_from_montgomery_word
	.p2align 4
	.globl	BN_mod_mul_montgomery
	.type	BN_mod_mul_montgomery, @function
BN_mod_mul_montgomery:
.LFB252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	40(%rcx), %ebx
	movl	8(%rsi), %eax
	movl	8(%rdx), %edx
	cmpl	$1, %ebx
	jle	.L48
	cmpl	%eax, %ebx
	je	.L65
.L48:
	addl	%edx, %eax
	addl	%ebx, %ebx
	movq	%r10, -56(%rbp)
	cmpl	%ebx, %eax
	jle	.L66
.L52:
	xorl	%r12d, %r12d
.L50:
	movq	%r14, %rdi
	call	bn_correct_top@PLT
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movq	%r13, %rdi
	call	BN_CTX_start@PLT
	movq	%r13, %rdi
	call	BN_CTX_get@PLT
	movq	-56(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L56
	movq	%r10, -56(%rbp)
	cmpq	%r15, %r12
	je	.L67
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	bn_mul_fixed_top@PLT
	movq	-56(%rbp), %r10
	testl	%eax, %eax
	je	.L56
.L57:
	movq	%r10, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	xorl	%r12d, %r12d
	call	bn_from_montgomery_word
	testl	%eax, %eax
	setne	%r12b
.L54:
	movq	%r13, %rdi
	call	BN_CTX_end@PLT
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L65:
	cmpl	%edx, %ebx
	jne	.L48
	movl	%ebx, %esi
	movq	%rcx, -56(%rbp)
	call	bn_wexpand@PLT
	movq	-56(%rbp), %r10
	testq	%rax, %rax
	je	.L52
	movq	32(%r10), %rcx
	movq	(%r15), %rdx
	movl	%ebx, %r9d
	leaq	80(%r10), %r8
	movq	(%r12), %rsi
	movq	(%r14), %rdi
	movq	%r10, -56(%rbp)
	call	bn_mul_mont@PLT
	testl	%eax, %eax
	jne	.L51
	movl	8(%r12), %eax
	movl	8(%r15), %edx
	movq	-56(%rbp), %r10
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L67:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	bn_sqr_fixed_top@PLT
	movq	-56(%rbp), %r10
	testl	%eax, %eax
	jne	.L57
	.p2align 4,,10
	.p2align 3
.L56:
	xorl	%r12d, %r12d
	jmp	.L54
.L51:
	movl	16(%r12), %eax
	xorl	16(%r15), %eax
	movl	$1, %r12d
	movl	%ebx, 8(%r14)
	movl	%eax, 16(%r14)
	jmp	.L50
	.cfi_endproc
.LFE252:
	.size	BN_mod_mul_montgomery, .-BN_mod_mul_montgomery
	.p2align 4
	.globl	bn_mul_mont_fixed_top
	.type	bn_mul_mont_fixed_top, @function
bn_mul_mont_fixed_top:
.LFB253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	40(%rcx), %ebx
	movl	8(%rsi), %eax
	movq	%rdi, -56(%rbp)
	movl	8(%rdx), %edx
	cmpl	$1, %ebx
	jle	.L69
	cmpl	%eax, %ebx
	je	.L86
.L69:
	addl	%edx, %eax
	addl	%ebx, %ebx
	cmpl	%ebx, %eax
	jle	.L87
.L73:
	xorl	%r12d, %r12d
.L68:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	movq	%r13, %rdi
	call	BN_CTX_start@PLT
	movq	%r13, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L77
	cmpq	%r15, %r12
	je	.L88
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	bn_mul_fixed_top@PLT
	testl	%eax, %eax
	je	.L77
.L78:
	movq	-56(%rbp), %rdi
	movq	%r14, %rdx
	movq	%rbx, %rsi
	xorl	%r12d, %r12d
	call	bn_from_montgomery_word
	testl	%eax, %eax
	setne	%r12b
.L75:
	movq	%r13, %rdi
	call	BN_CTX_end@PLT
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L86:
	cmpl	%edx, %ebx
	jne	.L69
	movl	%ebx, %esi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L73
	movq	-56(%rbp), %rax
	movq	32(%r14), %rcx
	movl	%ebx, %r9d
	leaq	80(%r14), %r8
	movq	(%r15), %rdx
	movq	(%r12), %rsi
	movq	(%rax), %rdi
	call	bn_mul_mont@PLT
	testl	%eax, %eax
	jne	.L72
	movl	8(%r12), %eax
	movl	8(%r15), %edx
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L88:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	bn_sqr_fixed_top@PLT
	testl	%eax, %eax
	jne	.L78
	.p2align 4,,10
	.p2align 3
.L77:
	xorl	%r12d, %r12d
	jmp	.L75
.L72:
	movq	-56(%rbp), %rcx
	movl	16(%r12), %eax
	movl	$1, %r12d
	xorl	16(%r15), %eax
	movl	%eax, 16(%rcx)
	movl	%ebx, 8(%rcx)
	jmp	.L68
	.cfi_endproc
.LFE253:
	.size	bn_mul_mont_fixed_top, .-bn_mul_mont_fixed_top
	.p2align 4
	.globl	BN_from_montgomery
	.type	BN_from_montgomery, @function
BN_from_montgomery:
.LFB255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rcx, %rdi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	testq	%rax, %rax
	je	.L92
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L92
	movq	%r13, %rsi
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	bn_from_montgomery_word
	movl	%eax, %r13d
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L92:
	xorl	%r13d, %r13d
.L91:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	movq	%r14, %rdi
	call	bn_correct_top@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE255:
	.size	BN_from_montgomery, .-BN_from_montgomery
	.p2align 4
	.globl	bn_from_mont_fixed_top
	.type	bn_from_mont_fixed_top, @function
bn_from_mont_fixed_top:
.LFB256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rcx, %rdi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	testq	%rax, %rax
	je	.L100
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L100
	movq	%r13, %rsi
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	bn_from_montgomery_word
	movl	%eax, %r13d
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L100:
	xorl	%r13d, %r13d
.L99:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE256:
	.size	bn_from_mont_fixed_top, .-bn_from_mont_fixed_top
	.p2align 4
	.globl	bn_to_mont_fixed_top
	.type	bn_to_mont_fixed_top, @function
bn_to_mont_fixed_top:
.LFB257:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	40(%rdx), %ebx
	movl	8(%rsi), %eax
	movl	16(%rdx), %r9d
	cmpl	$1, %ebx
	jle	.L106
	cmpl	%eax, %ebx
	je	.L123
.L106:
	addl	%eax, %r9d
	addl	%ebx, %ebx
	cmpl	%ebx, %r9d
	jle	.L124
.L110:
	xorl	%r12d, %r12d
.L105:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	movq	%r15, %rdi
	call	BN_CTX_start@PLT
	movq	%r15, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L114
	leaq	8(%r12), %rdx
	cmpq	%r13, %rdx
	je	.L125
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	bn_mul_fixed_top@PLT
	testl	%eax, %eax
	je	.L114
.L115:
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	xorl	%r12d, %r12d
	call	bn_from_montgomery_word
	testl	%eax, %eax
	setne	%r12b
.L112:
	movq	%r15, %rdi
	call	BN_CTX_end@PLT
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L123:
	cmpl	%ebx, %r9d
	jne	.L106
	movl	%r9d, %esi
	movl	%r9d, -52(%rbp)
	call	bn_wexpand@PLT
	movl	-52(%rbp), %r9d
	testq	%rax, %rax
	je	.L110
	movq	0(%r13), %rsi
	movq	(%r14), %rdi
	leaq	80(%r12), %r8
	movl	%r9d, -52(%rbp)
	movq	32(%r12), %rcx
	movq	8(%r12), %rdx
	call	bn_mul_mont@PLT
	movl	-52(%rbp), %r9d
	testl	%eax, %eax
	jne	.L109
	movl	8(%r13), %eax
	movl	16(%r12), %r9d
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L125:
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	bn_sqr_fixed_top@PLT
	testl	%eax, %eax
	jne	.L115
	.p2align 4,,10
	.p2align 3
.L114:
	xorl	%r12d, %r12d
	jmp	.L112
.L109:
	movl	16(%r13), %eax
	xorl	24(%r12), %eax
	movl	%r9d, 8(%r14)
	movl	$1, %r12d
	movl	%eax, 16(%r14)
	jmp	.L105
	.cfi_endproc
.LFE257:
	.size	bn_to_mont_fixed_top, .-bn_to_mont_fixed_top
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"../deps/openssl/openssl/crypto/bn/bn_mont.c"
	.text
	.p2align 4
	.globl	BN_MONT_CTX_new
	.type	BN_MONT_CTX_new, @function
BN_MONT_CTX_new:
.LFB258:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$232, %edx
	movl	$104, %edi
	leaq	.LC2(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L130
	movl	$0, (%rax)
	leaq	8(%rax), %rdi
	call	bn_init@PLT
	leaq	32(%r12), %rdi
	call	bn_init@PLT
	leaq	56(%r12), %rdi
	call	bn_init@PLT
	pxor	%xmm0, %xmm0
	movl	$1, 96(%r12)
	movups	%xmm0, 80(%r12)
.L126:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	movl	$233, %r8d
	movl	$65, %edx
	movl	$149, %esi
	movl	$3, %edi
	leaq	.LC2(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L126
	.cfi_endproc
.LFE258:
	.size	BN_MONT_CTX_new, .-BN_MONT_CTX_new
	.p2align 4
	.globl	BN_MONT_CTX_init
	.type	BN_MONT_CTX_init, @function
BN_MONT_CTX_init:
.LFB259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	8(%rdi), %rdi
	subq	$8, %rsp
	movl	$0, -8(%rdi)
	call	bn_init@PLT
	leaq	32(%rbx), %rdi
	call	bn_init@PLT
	leaq	56(%rbx), %rdi
	call	bn_init@PLT
	pxor	%xmm0, %xmm0
	movl	$0, 96(%rbx)
	movups	%xmm0, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE259:
	.size	BN_MONT_CTX_init, .-BN_MONT_CTX_init
	.p2align 4
	.globl	BN_MONT_CTX_free
	.type	BN_MONT_CTX_free, @function
BN_MONT_CTX_free:
.LFB260:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L141
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	subq	$8, %rsp
	call	BN_clear_free@PLT
	leaq	32(%r12), %rdi
	call	BN_clear_free@PLT
	leaq	56(%r12), %rdi
	call	BN_clear_free@PLT
	testb	$1, 96(%r12)
	jne	.L144
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$260, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC2(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L141:
	ret
	.cfi_endproc
.LFE260:
	.size	BN_MONT_CTX_free, .-BN_MONT_CTX_free
	.p2align 4
	.globl	BN_MONT_CTX_set
	.type	BN_MONT_CTX_set, @function
BN_MONT_CTX_set:
.LFB261:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rsi, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L199
.L145:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L200
	addq	$104, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L199:
	.cfi_restore_state
	movq	%r13, %rdi
	movl	%eax, %r12d
	call	BN_CTX_start@PLT
	movq	%r13, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L148
	leaq	32(%r14), %rax
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, -128(%rbp)
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L148
	movl	$4, %esi
	movq	%rbx, %rdi
	call	BN_get_flags@PLT
	testl	%eax, %eax
	jne	.L201
.L150:
	movl	$0, 48(%r14)
	leaq	-112(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	call	bn_init@PLT
	leaq	-80(%rbp), %rax
	movq	%rbx, %rdi
	movl	$4, %esi
	movq	%rax, -112(%rbp)
	movq	$2, -100(%rbp)
	call	BN_get_flags@PLT
	testl	%eax, %eax
	jne	.L202
.L151:
	leaq	8(%r14), %rax
	movq	%rbx, %rdi
	movq	%rax, -136(%rbp)
	call	BN_num_bits@PLT
	leaq	8(%r14), %rdi
	leal	126(%rax), %edx
	addl	$63, %eax
	cmovs	%edx, %eax
	xorl	%esi, %esi
	andl	$-64, %eax
	movl	%eax, (%r14)
	call	BN_set_word@PLT
	movl	$64, %esi
	leaq	8(%r14), %rdi
	call	BN_set_bit@PLT
	testl	%eax, %eax
	jne	.L203
.L148:
	movq	%r13, %rdi
	call	BN_CTX_end@PLT
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L203:
	movq	(%rbx), %rax
	movq	-120(%rbp), %rdi
	movq	(%rax), %rax
	movq	$0, -72(%rbp)
	testq	%rax, %rax
	movq	%rax, -80(%rbp)
	setne	%al
	movzbl	%al, %eax
	movl	%eax, -104(%rbp)
	call	BN_is_one@PLT
	testl	%eax, %eax
	je	.L153
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	BN_set_word@PLT
.L154:
	movl	$64, %edx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	BN_lshift@PLT
	testl	%eax, %eax
	je	.L148
	movq	%r15, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L204
	movq	$-1, %rsi
	movq	%r15, %rdi
	call	BN_set_word@PLT
	testl	%eax, %eax
	je	.L148
.L157:
	movq	-120(%rbp), %rcx
	xorl	%esi, %esi
	movq	%r13, %r8
	movq	%r15, %rdx
	movq	%r15, %rdi
	call	BN_div@PLT
	testl	%eax, %eax
	je	.L148
	movl	8(%r15), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L158
	movq	(%r15), %rax
	movq	(%rax), %rax
.L158:
	movq	%rax, 80(%r14)
	leaq	8(%r14), %rbx
	xorl	%esi, %esi
	movq	$0, 88(%r14)
	movq	%rbx, %rdi
	call	BN_set_word@PLT
	movl	(%r14), %eax
	movq	%rbx, %rdi
	leal	(%rax,%rax), %esi
	call	BN_set_bit@PLT
	testl	%eax, %eax
	je	.L148
	movq	-128(%rbp), %rcx
	xorl	%edi, %edi
	movq	%r13, %r8
	movq	%rbx, %rdx
	movq	%rbx, %rsi
	call	BN_div@PLT
	testl	%eax, %eax
	je	.L148
	movslq	16(%r14), %rax
	movl	40(%r14), %ebx
	cmpl	%ebx, %eax
	jge	.L160
	movq	8(%r14), %rcx
	leal	-1(%rbx), %edx
	xorl	%esi, %esi
	subl	%eax, %edx
	leaq	8(,%rdx,8), %rdx
	leaq	(%rcx,%rax,8), %rdi
	call	memset@PLT
.L160:
	movl	%ebx, 16(%r14)
	movl	$1, %r12d
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L202:
	movq	-120(%rbp), %rdi
	movl	$4, %esi
	call	BN_set_flags@PLT
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L201:
	movq	-128(%rbp), %rdi
	movl	$4, %esi
	call	BN_set_flags@PLT
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L153:
	movq	-120(%rbp), %rdx
	movq	%r13, %rcx
	leaq	8(%r14), %rsi
	movq	%r15, %rdi
	call	BN_mod_inverse@PLT
	testq	%rax, %rax
	jne	.L154
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L204:
	movl	$1, %esi
	movq	%r15, %rdi
	call	BN_sub_word@PLT
	testl	%eax, %eax
	jne	.L157
	jmp	.L148
.L200:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE261:
	.size	BN_MONT_CTX_set, .-BN_MONT_CTX_set
	.p2align 4
	.globl	BN_MONT_CTX_copy
	.type	BN_MONT_CTX_copy, @function
BN_MONT_CTX_copy:
.LFB262:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpq	%rsi, %rdi
	je	.L205
	movq	%rsi, %rbx
	leaq	8(%rdi), %rdi
	leaq	8(%rsi), %rsi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L208
	leaq	32(%rbx), %rsi
	leaq	32(%r12), %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L208
	leaq	56(%rbx), %rsi
	leaq	56(%r12), %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L208
	movl	(%rbx), %eax
	movdqu	80(%rbx), %xmm0
	movl	%eax, (%r12)
	movups	%xmm0, 80(%r12)
.L205:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	.cfi_restore_state
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE262:
	.size	BN_MONT_CTX_copy, .-BN_MONT_CTX_copy
	.p2align 4
	.globl	BN_MONT_CTX_set_locked
	.type	BN_MONT_CTX_set_locked, @function
BN_MONT_CTX_set_locked:
.LFB263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$40, %rsp
	movq	%rdx, -56(%rbp)
	call	CRYPTO_THREAD_read_lock@PLT
	movq	(%rbx), %r15
	movq	%r12, %rdi
	call	CRYPTO_THREAD_unlock@PLT
	testq	%r15, %r15
	je	.L231
.L217:
	addq	$40, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L231:
	.cfi_restore_state
	movl	$232, %edx
	leaq	.LC2(%rip), %rsi
	movl	$104, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L232
	movl	$0, (%rax)
	leaq	8(%rax), %rax
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	bn_init@PLT
	leaq	32(%r13), %rax
	movq	%rax, %rdi
	movq	%rax, -72(%rbp)
	call	bn_init@PLT
	leaq	56(%r13), %rax
	movq	%rax, %rdi
	movq	%rax, -80(%rbp)
	call	bn_init@PLT
	pxor	%xmm0, %xmm0
	movq	-56(%rbp), %rsi
	movq	%r14, %rdx
	movl	$1, 96(%r13)
	movq	%r13, %rdi
	movups	%xmm0, 80(%r13)
	call	BN_MONT_CTX_set
	testl	%eax, %eax
	je	.L233
	movq	%r12, %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	cmpq	$0, (%rbx)
	je	.L221
	movq	-64(%rbp), %rdi
	call	BN_clear_free@PLT
	movq	-72(%rbp), %rdi
	call	BN_clear_free@PLT
	movq	-80(%rbp), %rdi
	call	BN_clear_free@PLT
	testb	$1, 96(%r13)
	jne	.L234
.L222:
	movq	(%rbx), %r15
.L223:
	movq	%r12, %rdi
	call	CRYPTO_THREAD_unlock@PLT
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L221:
	movq	%r13, (%rbx)
	movq	%r13, %r15
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L233:
	movq	-64(%rbp), %rdi
	call	BN_clear_free@PLT
	movq	-72(%rbp), %rdi
	call	BN_clear_free@PLT
	movq	-80(%rbp), %rdi
	call	BN_clear_free@PLT
	testb	$1, 96(%r13)
	je	.L217
	movl	$260, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L234:
	movl	$260, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L232:
	movl	$233, %r8d
	movl	$65, %edx
	movl	$149, %esi
	movl	$3, %edi
	leaq	.LC2(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L217
	.cfi_endproc
.LFE263:
	.size	BN_MONT_CTX_set_locked, .-BN_MONT_CTX_set_locked
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	1
	.long	2
	.long	3
	.align 16
.LC1:
	.long	4
	.long	4
	.long	4
	.long	4
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
