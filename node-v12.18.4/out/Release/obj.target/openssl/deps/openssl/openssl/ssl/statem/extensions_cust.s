	.file	"extensions_cust.c"
	.text
	.p2align 4
	.type	custom_ext_add_old_cb_wrap, @function
custom_ext_add_old_cb_wrap:
.LFB1001:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %rdx
	movq	%r8, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	32(%rbp), %r9
	movq	24(%rbp), %r8
	movq	8(%r9), %rax
	testq	%rax, %rax
	je	.L2
	movq	(%r9), %r9
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1001:
	.size	custom_ext_add_old_cb_wrap, .-custom_ext_add_old_cb_wrap
	.p2align 4
	.type	custom_ext_free_old_cb_wrap, @function
custom_ext_free_old_cb_wrap:
.LFB1002:
	.cfi_startproc
	endbr64
	movq	16(%r8), %rax
	movq	%rcx, %rdx
	testq	%rax, %rax
	je	.L5
	movq	(%r8), %rcx
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L5:
	ret
	.cfi_endproc
.LFE1002:
	.size	custom_ext_free_old_cb_wrap, .-custom_ext_free_old_cb_wrap
	.p2align 4
	.type	custom_ext_parse_old_cb_wrap, @function
custom_ext_parse_old_cb_wrap:
.LFB1003:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %rdx
	movq	%r8, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	32(%rbp), %r9
	movq	24(%rbp), %r8
	movq	8(%r9), %rax
	testq	%rax, %rax
	je	.L8
	movq	(%r9), %r9
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1003:
	.size	custom_ext_parse_old_cb_wrap, .-custom_ext_parse_old_cb_wrap
	.p2align 4
	.globl	custom_ext_find
	.type	custom_ext_find, @function
custom_ext_find:
.LFB1004:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r9
	movq	(%rdi), %rax
	testq	%r9, %r9
	je	.L17
	xorl	%edi, %edi
	cmpl	$2, %esi
	jne	.L15
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L13:
	addq	$1, %rdi
	addq	$56, %rax
	cmpq	%rdi, %r9
	je	.L17
.L15:
	movzwl	(%rax), %r8d
	cmpl	%r8d, %edx
	jne	.L13
	movl	4(%rax), %r8d
	cmpl	$2, %r8d
	je	.L14
	cmpl	%r8d, %esi
	jne	.L13
.L14:
	testq	%rcx, %rcx
	je	.L10
	movq	%rdi, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	xorl	%eax, %eax
.L10:
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	addq	$1, %rdi
	addq	$56, %rax
	cmpq	%r9, %rdi
	je	.L17
.L12:
	movzwl	(%rax), %esi
	cmpl	%edx, %esi
	jne	.L16
	jmp	.L14
	.cfi_endproc
.LFE1004:
	.size	custom_ext_find, .-custom_ext_find
	.p2align 4
	.globl	custom_ext_init
	.type	custom_ext_init, @function
custom_ext_init:
.LFB1005:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	movq	(%rdi), %rax
	testq	%rcx, %rcx
	je	.L24
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L26:
	addq	$1, %rdx
	movl	$0, 12(%rax)
	addq	$56, %rax
	cmpq	%rcx, %rdx
	jne	.L26
.L24:
	ret
	.cfi_endproc
.LFE1005:
	.size	custom_ext_init, .-custom_ext_init
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/ssl/statem/extensions_cust.c"
	.text
	.p2align 4
	.globl	custom_ext_parse
	.type	custom_ext_parse, @function
custom_ext_parse:
.LFB1006:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	movl	$2, %ecx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	andl	$384, %esi
	movq	1168(%rdi), %rax
	je	.L32
	movl	56(%rdi), %edx
	xorl	%ecx, %ecx
	testl	%edx, %edx
	setne	%cl
.L32:
	movq	480(%rax), %rsi
	movq	472(%rax), %rbx
	xorl	%eax, %eax
	testq	%rsi, %rsi
	jne	.L33
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L35:
	addq	$1, %rax
	addq	$56, %rbx
	cmpq	%rsi, %rax
	je	.L37
.L33:
	movzwl	(%rbx), %edx
	cmpl	%edx, %r12d
	jne	.L35
	cmpl	$2, %ecx
	je	.L36
	movl	4(%rbx), %edx
	cmpl	%edx, %ecx
	je	.L36
	cmpl	$2, %edx
	jne	.L35
.L36:
	movl	8(%rbx), %esi
	movl	%r13d, %edx
	movq	%r14, %rdi
	movq	%r9, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	extension_is_relevant@PLT
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %r9
	testl	%eax, %eax
	je	.L37
	testl	$1792, %r13d
	je	.L38
	testb	$2, 12(%rbx)
	je	.L65
.L38:
	testb	$-128, %r13b
	je	.L39
	orl	$1, 12(%rbx)
.L39:
	movq	40(%rbx), %rax
	testq	%rax, %rax
	je	.L37
	subq	$8, %rsp
	leaq	-60(%rbp), %rdx
	pushq	48(%rbx)
	movq	%r15, %rcx
	pushq	%rdx
	movl	%r12d, %esi
	movl	%r13d, %edx
	movq	%r14, %rdi
	pushq	16(%rbp)
	call	*%rax
	addq	$32, %rsp
	testl	%eax, %eax
	jle	.L66
.L37:
	movl	$1, %eax
.L31:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L67
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L66:
	.cfi_restore_state
	movl	-60(%rbp), %esi
	movl	$110, %ecx
	movl	$555, %edx
	movq	%r14, %rdi
	movl	$162, %r9d
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L31
.L65:
	movl	$142, %r9d
	leaq	.LC0(%rip), %r8
	movl	$110, %ecx
	movq	%r14, %rdi
	movl	$555, %edx
	movl	$110, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L31
.L67:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1006:
	.size	custom_ext_parse, .-custom_ext_parse
	.p2align 4
	.globl	custom_ext_add
	.type	custom_ext_add, @function
custom_ext_add:
.LFB1007:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -88(%rbp)
	movq	1168(%rdi), %r12
	movq	%rcx, -112(%rbp)
	movq	%r8, -120(%rbp)
	movl	%r9d, -92(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 480(%r12)
	je	.L86
	movl	%esi, %eax
	movq	%rdi, %r14
	movl	%esi, %r13d
	xorl	%ebx, %ebx
	andl	$7936, %eax
	movl	%eax, -100(%rbp)
	movl	%esi, %eax
	andl	$128, %eax
	movl	%eax, -96(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rax, -128(%rbp)
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L130:
	testq	%rax, %rax
	je	.L87
.L74:
	subq	$8, %rsp
	leaq	-76(%rbp), %rdx
	movzwl	(%r15), %esi
	pushq	32(%r15)
	movq	-112(%rbp), %r9
	pushq	%rdx
	leaq	-72(%rbp), %rcx
	movl	%r13d, %edx
	pushq	-120(%rbp)
	movq	-128(%rbp), %r8
	movq	%r14, %rdi
	call	*%rax
	addq	$32, %rsp
	testl	%eax, %eax
	js	.L128
	je	.L87
.L79:
	movzwl	(%r15), %esi
	movq	-88(%rbp), %rdi
	movl	$2, %edx
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L75
	movq	-88(%rbp), %rdi
	movl	$2, %esi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L75
	movq	-64(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L82
	movq	-72(%rbp), %rsi
	movq	-88(%rbp), %rdi
	call	WPACKET_memcpy@PLT
	testl	%eax, %eax
	je	.L75
.L82:
	movq	-88(%rbp), %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L75
	movl	-96(%rbp), %eax
	testl	%eax, %eax
	je	.L83
	movl	12(%r15), %eax
	testb	$2, %al
	jne	.L129
	orl	$2, %eax
	movl	%eax, 12(%r15)
.L83:
	movq	24(%r15), %rax
	testq	%rax, %rax
	je	.L87
	movzwl	(%r15), %esi
	movq	32(%r15), %r8
	movl	%r13d, %edx
	movq	%r14, %rdi
	movq	-72(%rbp), %rcx
	call	*%rax
.L87:
	addq	$1, %rbx
	cmpq	%rbx, 480(%r12)
	jbe	.L86
.L85:
	movq	472(%r12), %rdx
	leaq	0(,%rbx,8), %rax
	movl	-92(%rbp), %ecx
	movq	%r14, %rdi
	subq	%rbx, %rax
	movq	$0, -72(%rbp)
	leaq	(%rdx,%rax,8), %r15
	movl	%r13d, %edx
	movq	$0, -64(%rbp)
	movl	8(%r15), %esi
	call	should_add_extension@PLT
	testl	%eax, %eax
	je	.L87
	movl	-100(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L72
	testb	$1, 12(%r15)
	je	.L87
.L72:
	movl	-96(%rbp), %edx
	movq	16(%r15), %rax
	testl	%edx, %edx
	je	.L130
	testq	%rax, %rax
	jne	.L74
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L75:
	movl	$223, %r9d
.L127:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	$554, %edx
	movq	%r14, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
.L78:
	xorl	%eax, %eax
.L68:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L131
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	movl	$1, %eax
	jmp	.L68
.L128:
	movl	-76(%rbp), %esi
	movl	$234, %ecx
	movl	$554, %edx
	movq	%r14, %rdi
	movl	$212, %r9d
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	jmp	.L78
.L129:
	movl	$232, %r9d
	jmp	.L127
.L131:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1007:
	.size	custom_ext_add, .-custom_ext_add
	.p2align 4
	.globl	custom_exts_copy_flags
	.type	custom_exts_copy_flags, @function
custom_exts_copy_flags:
.LFB1008:
	.cfi_startproc
	endbr64
	movq	8(%rsi), %r11
	movq	(%rsi), %r9
	testq	%r11, %r11
	je	.L152
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r10d, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	(%rdi), %rbx
	movq	8(%rdi), %rsi
	.p2align 4,,10
	.p2align 3
.L139:
	movzwl	(%r9), %ecx
	movl	4(%r9), %r8d
	testq	%rsi, %rsi
	je	.L134
	movq	%rbx, %rax
	xorl	%edx, %edx
	cmpl	$2, %r8d
	jne	.L138
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L136:
	addq	$1, %rdx
	addq	$56, %rax
	cmpq	%rsi, %rdx
	je	.L134
.L138:
	cmpw	(%rax), %cx
	jne	.L136
	movl	4(%rax), %edi
	cmpl	$2, %edi
	je	.L137
	cmpl	%edi, %r8d
	jne	.L136
.L137:
	movl	12(%r9), %edx
	movl	%edx, 12(%rax)
.L134:
	addq	$1, %r10
	addq	$56, %r9
	cmpq	%r11, %r10
	jne	.L139
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L155:
	.cfi_restore_state
	addq	$1, %rdx
	addq	$56, %rax
	cmpq	%rsi, %rdx
	je	.L134
.L135:
	cmpw	(%rax), %cx
	jne	.L155
	jmp	.L137
.L152:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1008:
	.size	custom_exts_copy_flags, .-custom_exts_copy_flags
	.p2align 4
	.globl	custom_exts_copy
	.type	custom_exts_copy, @function
custom_exts_copy:
.LFB1009:
	.cfi_startproc
	endbr64
	movq	8(%rsi), %rax
	testq	%rax, %rax
	jne	.L157
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L157:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$277, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	leaq	0(,%rax,8), %rsi
	pushq	%r12
	subq	%rax, %rsi
	pushq	%rbx
	salq	$3, %rsi
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	0(%r13), %rdi
	call	CRYPTO_memdup@PLT
	movq	%rax, (%r14)
	testq	%rax, %rax
	je	.L169
	movq	8(%r13), %rdx
	movq	%rdx, 8(%r14)
	movq	8(%r13), %rsi
	testq	%rsi, %rsi
	je	.L159
	xorl	%ecx, %ecx
	xorl	%ebx, %ebx
	leaq	custom_ext_add_old_cb_wrap(%rip), %r15
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L160:
	addq	$1, %rbx
	cmpq	%rsi, %rbx
	jnb	.L162
.L182:
	movq	(%r14), %rax
.L163:
	leaq	0(,%rbx,8), %rdx
	movq	0(%r13), %r9
	subq	%rbx, %rdx
	salq	$3, %rdx
	addq	%rdx, %r9
	cmpq	%r15, 16(%r9)
	jne	.L160
	leaq	(%rax,%rdx), %r12
	testl	%ecx, %ecx
	je	.L161
	movq	$0, 32(%r12)
	addq	$1, %rbx
	movq	$0, 48(%r12)
	cmpq	%rsi, %rbx
	jb	.L182
.L162:
	testl	%ecx, %ecx
	jne	.L183
.L159:
	movl	$1, %eax
.L156:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	movq	32(%r9), %rdi
	movl	$301, %ecx
	leaq	.LC0(%rip), %rdx
	movl	$24, %esi
	movq	%r9, -56(%rbp)
	call	CRYPTO_memdup@PLT
	movq	-56(%rbp), %r9
	movl	$303, %ecx
	movl	$16, %esi
	movq	%rax, 32(%r12)
	leaq	.LC0(%rip), %rdx
	movq	48(%r9), %rdi
	call	CRYPTO_memdup@PLT
	cmpq	$0, 32(%r12)
	movq	8(%r13), %rsi
	sete	%cl
	testq	%rax, %rax
	movq	%rax, 48(%r12)
	sete	%al
	orl	%eax, %ecx
	movzbl	%cl, %ecx
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L183:
	movq	8(%r14), %rax
	movq	(%r14), %r12
	testq	%rax, %rax
	je	.L164
	xorl	%ebx, %ebx
	leaq	.LC0(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L168:
	cmpq	%r15, 16(%r12)
	je	.L184
	addq	$1, %rbx
	addq	$56, %r12
	cmpq	%rax, %rbx
	jb	.L168
.L167:
	movq	(%r14), %r12
.L164:
	movl	$332, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	xorl	%eax, %eax
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L169:
	xorl	%eax, %eax
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L184:
	movq	32(%r12), %rdi
	movq	%r13, %rsi
	addq	$1, %rbx
	addq	$56, %r12
	movl	$329, %edx
	call	CRYPTO_free@PLT
	movq	-8(%r12), %rdi
	movl	$330, %edx
	movq	%r13, %rsi
	call	CRYPTO_free@PLT
	movq	8(%r14), %rax
	cmpq	%rax, %rbx
	jb	.L168
	jmp	.L167
	.cfi_endproc
.LFE1009:
	.size	custom_exts_copy, .-custom_exts_copy
	.p2align 4
	.globl	custom_exts_free
	.type	custom_exts_free, @function
custom_exts_free:
.LFB1010:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	(%rdi), %r12
	testq	%rax, %rax
	je	.L186
	movq	%rdi, %r15
	xorl	%ebx, %ebx
	leaq	custom_ext_add_old_cb_wrap(%rip), %r13
	leaq	.LC0(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L190:
	cmpq	%r13, 16(%r12)
	je	.L195
	addq	$1, %rbx
	addq	$56, %r12
	cmpq	%rbx, %rax
	ja	.L190
.L189:
	movq	(%r15), %r12
.L186:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$332, %edx
	popq	%rbx
	leaq	.LC0(%rip), %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L195:
	.cfi_restore_state
	movq	32(%r12), %rdi
	movq	%r14, %rsi
	addq	$1, %rbx
	addq	$56, %r12
	movl	$329, %edx
	call	CRYPTO_free@PLT
	movq	-8(%r12), %rdi
	movl	$330, %edx
	movq	%r14, %rsi
	call	CRYPTO_free@PLT
	movq	8(%r15), %rax
	cmpq	%rax, %rbx
	jb	.L190
	jmp	.L189
	.cfi_endproc
.LFE1010:
	.size	custom_exts_free, .-custom_exts_free
	.p2align 4
	.globl	SSL_CTX_has_client_custom_ext
	.type	SSL_CTX_has_client_custom_ext, @function
SSL_CTX_has_client_custom_ext:
.LFB1011:
	.cfi_startproc
	endbr64
	movq	320(%rdi), %rdx
	movq	480(%rdx), %rdi
	movq	472(%rdx), %rax
	testq	%rdi, %rdi
	je	.L200
	xorl	%edx, %edx
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L198:
	addq	$1, %rdx
	addq	$56, %rax
	cmpq	%rdi, %rdx
	je	.L200
.L199:
	movzwl	(%rax), %ecx
	cmpl	%ecx, %esi
	jne	.L198
	testl	$-3, 4(%rax)
	jne	.L198
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L200:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1011:
	.size	SSL_CTX_has_client_custom_ext, .-SSL_CTX_has_client_custom_ext
	.p2align 4
	.globl	SSL_CTX_add_client_custom_ext
	.type	SSL_CTX_add_client_custom_ext, @function
SSL_CTX_add_client_custom_ext:
.LFB1014:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$24, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	leaq	.LC0(%rip), %rsi
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	movl	$416, %edx
	movq	%rcx, -64(%rbp)
	call	CRYPTO_malloc@PLT
	movl	$418, %edx
	movl	$16, %edi
	leaq	.LC0(%rip), %rsi
	movq	%rax, %r12
	call	CRYPTO_malloc@PLT
	movq	%rax, %r8
	testq	%r12, %r12
	je	.L219
	testq	%rax, %rax
	je	.L219
	movq	-56(%rbp), %xmm0
	movq	16(%rbp), %rax
	movq	%r15, (%r12)
	movhps	-64(%rbp), %xmm0
	movups	%xmm0, 8(%r12)
	movq	%r14, 8(%r8)
	movq	320(%r13), %r14
	movq	%rax, (%r8)
	cmpl	$18, %ebx
	je	.L237
	cmpl	$51, %ebx
	ja	.L211
	leaq	.L214(%rip), %rcx
	movl	%ebx, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L214:
	.long	.L213-.L214
	.long	.L213-.L214
	.long	.L212-.L214
	.long	.L212-.L214
	.long	.L212-.L214
	.long	.L213-.L214
	.long	.L212-.L214
	.long	.L212-.L214
	.long	.L212-.L214
	.long	.L212-.L214
	.long	.L213-.L214
	.long	.L213-.L214
	.long	.L213-.L214
	.long	.L213-.L214
	.long	.L213-.L214
	.long	.L212-.L214
	.long	.L213-.L214
	.long	.L212-.L214
	.long	.L213-.L214
	.long	.L212-.L214
	.long	.L212-.L214
	.long	.L213-.L214
	.long	.L213-.L214
	.long	.L213-.L214
	.long	.L212-.L214
	.long	.L212-.L214
	.long	.L212-.L214
	.long	.L212-.L214
	.long	.L212-.L214
	.long	.L212-.L214
	.long	.L212-.L214
	.long	.L212-.L214
	.long	.L212-.L214
	.long	.L212-.L214
	.long	.L212-.L214
	.long	.L213-.L214
	.long	.L212-.L214
	.long	.L212-.L214
	.long	.L212-.L214
	.long	.L212-.L214
	.long	.L212-.L214
	.long	.L213-.L214
	.long	.L213-.L214
	.long	.L213-.L214
	.long	.L213-.L214
	.long	.L213-.L214
	.long	.L212-.L214
	.long	.L213-.L214
	.long	.L212-.L214
	.long	.L213-.L214
	.long	.L212-.L214
	.long	.L213-.L214
	.text
	.p2align 4,,10
	.p2align 3
.L211:
	cmpl	$13172, %ebx
	je	.L213
	cmpl	$65281, %ebx
	je	.L213
.L212:
	cmpl	$65535, %ebx
	jbe	.L215
.L213:
	movl	$442, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	CRYPTO_free@PLT
	movl	$443, %edx
.L236:
	movq	-56(%rbp), %r8
	leaq	.LC0(%rip), %rsi
	movq	%r8, %rdi
	call	CRYPTO_free@PLT
	xorl	%eax, %eax
.L203:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L237:
	.cfi_restore_state
	movq	%r13, %rdi
	movq	%r8, -56(%rbp)
	call	SSL_CTX_ct_is_enabled@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	jne	.L213
.L215:
	movq	472(%r14), %rdi
	movq	480(%r14), %rsi
	xorl	%edx, %edx
	movq	%rdi, %rax
	testq	%rsi, %rsi
	jne	.L209
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L216:
	addq	$1, %rdx
	addq	$56, %rax
	cmpq	%rdx, %rsi
	je	.L239
.L209:
	movzwl	(%rax), %ecx
	cmpl	%ecx, %ebx
	jne	.L216
	testl	$-3, 4(%rax)
	je	.L213
	addq	$1, %rdx
	addq	$56, %rax
	cmpq	%rdx, %rsi
	jne	.L209
.L239:
	addq	$1, %rsi
	leaq	0(,%rsi,8), %rax
	subq	%rsi, %rax
	movq	%rax, %rsi
	salq	$3, %rsi
.L210:
	movl	$387, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r8, -56(%rbp)
	call	CRYPTO_realloc@PLT
	movq	-56(%rbp), %r8
	testq	%rax, %rax
	je	.L213
	movq	480(%r14), %rcx
	pxor	%xmm0, %xmm0
	movq	%rax, 472(%r14)
	movabsq	$1992864825344, %rdi
	leaq	0(,%rcx,8), %rdx
	subq	%rcx, %rdx
	leaq	(%rax,%rdx,8), %rax
	leaq	custom_ext_free_old_cb_wrap(%rip), %rdx
	movups	%xmm0, (%rax)
	movq	%rdx, %xmm1
	movq	%rdi, 4(%rax)
	leaq	custom_ext_parse_old_cb_wrap(%rip), %rdi
	movq	%rdi, 40(%rax)
	leaq	custom_ext_add_old_cb_wrap(%rip), %rdi
	movq	%rdi, %xmm0
	movw	%bx, (%rax)
	punpcklqdq	%xmm1, %xmm0
	movq	%r12, 32(%rax)
	movq	%r8, 48(%rax)
	movups	%xmm0, 16(%rax)
	movl	$1, %eax
	addq	$1, 480(%r14)
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L219:
	movl	$422, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	CRYPTO_free@PLT
	movl	$423, %edx
	jmp	.L236
.L238:
	movl	$56, %esi
	jmp	.L210
	.cfi_endproc
.LFE1014:
	.size	SSL_CTX_add_client_custom_ext, .-SSL_CTX_add_client_custom_ext
	.p2align 4
	.globl	SSL_CTX_add_server_custom_ext
	.type	SSL_CTX_add_server_custom_ext, @function
SSL_CTX_add_server_custom_ext:
.LFB1015:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$24, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	leaq	.LC0(%rip), %rsi
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	movl	$416, %edx
	movq	%rcx, -64(%rbp)
	call	CRYPTO_malloc@PLT
	movl	$418, %edx
	movl	$16, %edi
	leaq	.LC0(%rip), %rsi
	movq	%rax, %r12
	call	CRYPTO_malloc@PLT
	movq	%rax, %r8
	testq	%r12, %r12
	je	.L256
	testq	%rax, %rax
	je	.L256
	movq	-56(%rbp), %xmm0
	movq	16(%rbp), %rax
	movq	%r15, (%r12)
	movhps	-64(%rbp), %xmm0
	movups	%xmm0, 8(%r12)
	movq	%r14, 8(%r8)
	movq	320(%r13), %r14
	movq	%rax, (%r8)
	cmpl	$18, %ebx
	je	.L271
	cmpl	$51, %ebx
	ja	.L248
	leaq	.L251(%rip), %rcx
	movl	%ebx, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L251:
	.long	.L250-.L251
	.long	.L250-.L251
	.long	.L249-.L251
	.long	.L249-.L251
	.long	.L249-.L251
	.long	.L250-.L251
	.long	.L249-.L251
	.long	.L249-.L251
	.long	.L249-.L251
	.long	.L249-.L251
	.long	.L250-.L251
	.long	.L250-.L251
	.long	.L250-.L251
	.long	.L250-.L251
	.long	.L250-.L251
	.long	.L249-.L251
	.long	.L250-.L251
	.long	.L249-.L251
	.long	.L250-.L251
	.long	.L249-.L251
	.long	.L249-.L251
	.long	.L250-.L251
	.long	.L250-.L251
	.long	.L250-.L251
	.long	.L249-.L251
	.long	.L249-.L251
	.long	.L249-.L251
	.long	.L249-.L251
	.long	.L249-.L251
	.long	.L249-.L251
	.long	.L249-.L251
	.long	.L249-.L251
	.long	.L249-.L251
	.long	.L249-.L251
	.long	.L249-.L251
	.long	.L250-.L251
	.long	.L249-.L251
	.long	.L249-.L251
	.long	.L249-.L251
	.long	.L249-.L251
	.long	.L249-.L251
	.long	.L250-.L251
	.long	.L250-.L251
	.long	.L250-.L251
	.long	.L250-.L251
	.long	.L250-.L251
	.long	.L249-.L251
	.long	.L250-.L251
	.long	.L249-.L251
	.long	.L250-.L251
	.long	.L249-.L251
	.long	.L250-.L251
	.text
	.p2align 4,,10
	.p2align 3
.L248:
	cmpl	$13172, %ebx
	je	.L250
	cmpl	$65281, %ebx
	je	.L250
.L249:
	cmpl	$65535, %ebx
	jbe	.L252
.L250:
	movl	$442, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	CRYPTO_free@PLT
	movl	$443, %edx
.L270:
	movq	-56(%rbp), %r8
	leaq	.LC0(%rip), %rsi
	movq	%r8, %rdi
	call	CRYPTO_free@PLT
	xorl	%eax, %eax
.L240:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L271:
	.cfi_restore_state
	movq	%r13, %rdi
	movq	%r8, -56(%rbp)
	call	SSL_CTX_ct_is_enabled@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	jne	.L250
.L252:
	movq	472(%r14), %rdi
	movq	480(%r14), %rsi
	xorl	%ecx, %ecx
	movq	%rdi, %rax
	testq	%rsi, %rsi
	jne	.L246
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L253:
	addq	$1, %rcx
	addq	$56, %rax
	cmpq	%rsi, %rcx
	je	.L273
.L246:
	movzwl	(%rax), %edx
	cmpl	%edx, %ebx
	jne	.L253
	movl	4(%rax), %edx
	subl	$1, %edx
	cmpl	$1, %edx
	jbe	.L250
	addq	$1, %rcx
	addq	$56, %rax
	cmpq	%rsi, %rcx
	jne	.L246
.L273:
	addq	$1, %rcx
	leaq	0(,%rcx,8), %rsi
	subq	%rcx, %rsi
	salq	$3, %rsi
.L247:
	movl	$387, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r8, -56(%rbp)
	call	CRYPTO_realloc@PLT
	movq	-56(%rbp), %r8
	testq	%rax, %rax
	je	.L250
	movq	480(%r14), %rcx
	pxor	%xmm0, %xmm0
	movq	%rax, 472(%r14)
	movabsq	$1992864825345, %rdi
	leaq	0(,%rcx,8), %rdx
	subq	%rcx, %rdx
	leaq	(%rax,%rdx,8), %rax
	leaq	custom_ext_free_old_cb_wrap(%rip), %rdx
	movups	%xmm0, (%rax)
	movq	%rdx, %xmm1
	movq	%rdi, 4(%rax)
	leaq	custom_ext_parse_old_cb_wrap(%rip), %rdi
	movq	%rdi, 40(%rax)
	leaq	custom_ext_add_old_cb_wrap(%rip), %rdi
	movq	%rdi, %xmm0
	movw	%bx, (%rax)
	punpcklqdq	%xmm1, %xmm0
	movq	%r12, 32(%rax)
	movq	%r8, 48(%rax)
	movups	%xmm0, 16(%rax)
	movl	$1, %eax
	addq	$1, 480(%r14)
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L256:
	movl	$422, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	CRYPTO_free@PLT
	movl	$423, %edx
	jmp	.L270
.L272:
	movl	$56, %esi
	jmp	.L247
	.cfi_endproc
.LFE1015:
	.size	SSL_CTX_add_server_custom_ext, .-SSL_CTX_add_server_custom_ext
	.p2align 4
	.globl	SSL_CTX_add_custom_ext
	.type	SSL_CTX_add_custom_ext, @function
SSL_CTX_add_custom_ext:
.LFB1016:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	subq	$16, %rsp
	testq	%rcx, %rcx
	jne	.L275
	testq	%r8, %r8
	je	.L275
.L289:
	xorl	%eax, %eax
.L274:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L275:
	.cfi_restore_state
	movq	320(%rdi), %r8
	cmpl	$18, %ebx
	je	.L307
	cmpl	$51, %ebx
	ja	.L280
	leaq	.L282(%rip), %rcx
	movl	%ebx, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L282:
	.long	.L289-.L282
	.long	.L289-.L282
	.long	.L281-.L282
	.long	.L281-.L282
	.long	.L281-.L282
	.long	.L289-.L282
	.long	.L281-.L282
	.long	.L281-.L282
	.long	.L281-.L282
	.long	.L281-.L282
	.long	.L289-.L282
	.long	.L289-.L282
	.long	.L289-.L282
	.long	.L289-.L282
	.long	.L289-.L282
	.long	.L281-.L282
	.long	.L289-.L282
	.long	.L281-.L282
	.long	.L289-.L282
	.long	.L281-.L282
	.long	.L281-.L282
	.long	.L289-.L282
	.long	.L289-.L282
	.long	.L289-.L282
	.long	.L281-.L282
	.long	.L281-.L282
	.long	.L281-.L282
	.long	.L281-.L282
	.long	.L281-.L282
	.long	.L281-.L282
	.long	.L281-.L282
	.long	.L281-.L282
	.long	.L281-.L282
	.long	.L281-.L282
	.long	.L281-.L282
	.long	.L289-.L282
	.long	.L281-.L282
	.long	.L281-.L282
	.long	.L281-.L282
	.long	.L281-.L282
	.long	.L281-.L282
	.long	.L289-.L282
	.long	.L289-.L282
	.long	.L289-.L282
	.long	.L289-.L282
	.long	.L289-.L282
	.long	.L281-.L282
	.long	.L289-.L282
	.long	.L281-.L282
	.long	.L289-.L282
	.long	.L281-.L282
	.long	.L289-.L282
	.text
	.p2align 4,,10
	.p2align 3
.L280:
	cmpl	$13172, %ebx
	je	.L289
	cmpl	$65281, %ebx
	je	.L289
.L281:
	cmpl	$65535, %ebx
	ja	.L289
.L283:
	movq	472(%r8), %rdi
	movq	480(%r8), %rsi
	xorl	%edx, %edx
	movq	%rdi, %rax
	testq	%rsi, %rsi
	je	.L308
.L284:
	movzwl	(%rax), %ecx
	cmpl	%ecx, %ebx
	je	.L289
	addq	$1, %rdx
	addq	$56, %rax
	cmpq	%rsi, %rdx
	jne	.L284
	addq	$1, %rdx
	leaq	0(,%rdx,8), %rsi
	subq	%rdx, %rsi
	salq	$3, %rsi
.L285:
	movl	$387, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r9, -48(%rbp)
	movq	%r8, -40(%rbp)
	call	CRYPTO_realloc@PLT
	movq	-40(%rbp), %r8
	movq	-48(%rbp), %r9
	testq	%rax, %rax
	je	.L289
	movq	480(%r8), %rcx
	movq	%rax, 472(%r8)
	pxor	%xmm0, %xmm0
	movq	%r13, %xmm1
	leaq	0(,%rcx,8), %rdx
	subq	%rcx, %rdx
	leaq	(%rax,%rdx,8), %rax
	movq	16(%rbp), %rdx
	movups	%xmm0, (%rax)
	movq	%r12, %xmm0
	movq	%rdx, 40(%rax)
	movq	24(%rbp), %rdx
	punpcklqdq	%xmm1, %xmm0
	movl	$2, 4(%rax)
	movl	%r14d, 8(%rax)
	movw	%bx, (%rax)
	movq	%r9, 32(%rax)
	movq	%rdx, 48(%rax)
	movups	%xmm0, 16(%rax)
	movl	$1, %eax
	addq	$1, 480(%r8)
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L307:
	testb	$-128, %r14b
	je	.L283
	movq	%r9, -48(%rbp)
	movq	%r8, -40(%rbp)
	call	SSL_CTX_ct_is_enabled@PLT
	movq	-40(%rbp), %r8
	movq	-48(%rbp), %r9
	testl	%eax, %eax
	je	.L283
	jmp	.L289
.L308:
	movl	$56, %esi
	jmp	.L285
	.cfi_endproc
.LFE1016:
	.size	SSL_CTX_add_custom_ext, .-SSL_CTX_add_custom_ext
	.p2align 4
	.globl	SSL_extension_supported
	.type	SSL_extension_supported, @function
SSL_extension_supported:
.LFB1017:
	.cfi_startproc
	endbr64
	cmpl	$51, %edi
	ja	.L310
	leaq	.L313(%rip), %rdx
	movl	%edi, %edi
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L313:
	.long	.L315-.L313
	.long	.L315-.L313
	.long	.L314-.L313
	.long	.L314-.L313
	.long	.L314-.L313
	.long	.L315-.L313
	.long	.L314-.L313
	.long	.L314-.L313
	.long	.L314-.L313
	.long	.L314-.L313
	.long	.L315-.L313
	.long	.L315-.L313
	.long	.L315-.L313
	.long	.L315-.L313
	.long	.L315-.L313
	.long	.L314-.L313
	.long	.L315-.L313
	.long	.L314-.L313
	.long	.L315-.L313
	.long	.L314-.L313
	.long	.L314-.L313
	.long	.L315-.L313
	.long	.L315-.L313
	.long	.L315-.L313
	.long	.L314-.L313
	.long	.L314-.L313
	.long	.L314-.L313
	.long	.L314-.L313
	.long	.L314-.L313
	.long	.L314-.L313
	.long	.L314-.L313
	.long	.L314-.L313
	.long	.L314-.L313
	.long	.L314-.L313
	.long	.L314-.L313
	.long	.L315-.L313
	.long	.L314-.L313
	.long	.L314-.L313
	.long	.L314-.L313
	.long	.L314-.L313
	.long	.L314-.L313
	.long	.L315-.L313
	.long	.L315-.L313
	.long	.L315-.L313
	.long	.L315-.L313
	.long	.L315-.L313
	.long	.L314-.L313
	.long	.L315-.L313
	.long	.L314-.L313
	.long	.L315-.L313
	.long	.L314-.L313
	.long	.L315-.L313
	.text
.L315:
	movl	$1, %eax
	ret
.L314:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L310:
	cmpl	$13172, %edi
	je	.L315
	xorl	%eax, %eax
	cmpl	$65281, %edi
	sete	%al
	ret
	.cfi_endproc
.LFE1017:
	.size	SSL_extension_supported, .-SSL_extension_supported
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
