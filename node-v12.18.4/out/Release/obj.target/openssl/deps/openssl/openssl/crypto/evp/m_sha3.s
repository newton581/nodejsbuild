	.file	"m_sha3.c"
	.text
	.p2align 4
	.type	shake_ctrl, @function
shake_ctrl:
.LFB398:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$3, %esi
	je	.L5
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	24(%rdi), %rax
	movslq	%edx, %rdx
	movq	%rdx, 208(%rax)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE398:
	.size	shake_ctrl, .-shake_ctrl
	.p2align 4
	.type	sha3_init, @function
sha3_init:
.LFB394:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	%rdi, %rdx
	movslq	64(%rax), %rsi
	xorl	%eax, %eax
	cmpq	$168, %rsi
	jbe	.L9
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	24(%rdi), %r8
	xorl	%eax, %eax
	leaq	8(%r8), %rdi
	movq	%r8, %rcx
	movq	$0, (%r8)
	movq	$0, 192(%r8)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$200, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	%rsi, 200(%r8)
	movq	$0, 216(%r8)
	movq	(%rdx), %rax
	movslq	8(%rax), %rax
	movb	$6, 392(%r8)
	movq	%rax, 208(%r8)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE394:
	.size	sha3_init, .-sha3_init
	.p2align 4
	.type	sha3_final, @function
sha3_final:
.LFB397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rdi), %r12
	cmpq	$0, 208(%r12)
	je	.L11
	movq	200(%r12), %r13
	movq	216(%r12), %rbx
	leaq	224(%r12), %r15
	movq	%rsi, %r14
	xorl	%esi, %esi
	movq	%r13, %rdx
	leaq	(%r15,%rbx), %rdi
	subq	%rbx, %rdx
	call	memset@PLT
	movq	%r13, %rcx
	movq	%r13, %rdx
	movq	%r15, %rsi
	movzbl	392(%r12), %eax
	movq	%r12, %rdi
	movb	%al, 224(%r12,%rbx)
	orb	$-128, 223(%r12,%r13)
	call	SHA3_absorb@PLT
	movq	%r13, %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	208(%r12), %rdx
	call	SHA3_squeeze@PLT
.L11:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE397:
	.size	sha3_final, .-sha3_final
	.p2align 4
	.type	sha3_update, @function
sha3_update:
.LFB396:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L28
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %r14
	movq	216(%r14), %r13
	movq	200(%r14), %r15
	testq	%r13, %r13
	je	.L16
	movq	%r15, %rdx
	leaq	224(%r14), %r8
	subq	%r13, %rdx
	leaq	(%r8,%r13), %rdi
	cmpq	%rdx, %rbx
	jb	.L31
	movq	%r8, -64(%rbp)
	addq	%rbx, %r13
	movq	%rdx, -56(%rbp)
	subq	%r15, %r13
	call	memcpy@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	movq	%r15, %rcx
	movq	%r14, %rdi
	movq	%r13, %rbx
	addq	%rdx, %r12
	movq	%r8, %rsi
	movq	%r15, %rdx
	call	SHA3_absorb@PLT
	movq	$0, 216(%r14)
	cmpq	%r13, %r15
	jbe	.L21
.L18:
	testq	%r13, %r13
	jne	.L32
.L24:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	cmpq	%rdx, %r15
	jbe	.L21
.L20:
	leaq	224(%r14), %rdi
	movq	%rbx, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
	movq	%rbx, 216(%r14)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%r15, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	SHA3_absorb@PLT
	movq	%rax, %r13
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L31:
	movq	%rbx, %rdx
	call	memcpy@PLT
	addq	%rbx, 216(%r14)
	jmp	.L24
.L32:
	subq	%r13, %rbx
	addq	%rbx, %r12
	movq	%r13, %rbx
	jmp	.L20
	.cfi_endproc
.LFE396:
	.size	sha3_update, .-sha3_update
	.p2align 4
	.type	shake_init, @function
shake_init:
.LFB395:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	%rdi, %rdx
	movslq	64(%rax), %rsi
	xorl	%eax, %eax
	cmpq	$168, %rsi
	jbe	.L36
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	movq	24(%rdi), %r8
	xorl	%eax, %eax
	leaq	8(%r8), %rdi
	movq	%r8, %rcx
	movq	$0, (%r8)
	movq	$0, 192(%r8)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$200, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	%rsi, 200(%r8)
	movq	$0, 216(%r8)
	movq	(%rdx), %rax
	movslq	8(%rax), %rax
	movb	$31, 392(%r8)
	movq	%rax, 208(%r8)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE395:
	.size	shake_init, .-shake_init
	.p2align 4
	.globl	EVP_sha3_224
	.type	EVP_sha3_224, @function
EVP_sha3_224:
.LFB399:
	.cfi_startproc
	endbr64
	leaq	sha3_224_md.10441(%rip), %rax
	ret
	.cfi_endproc
.LFE399:
	.size	EVP_sha3_224, .-EVP_sha3_224
	.p2align 4
	.globl	EVP_sha3_256
	.type	EVP_sha3_256, @function
EVP_sha3_256:
.LFB400:
	.cfi_startproc
	endbr64
	leaq	sha3_256_md.10445(%rip), %rax
	ret
	.cfi_endproc
.LFE400:
	.size	EVP_sha3_256, .-EVP_sha3_256
	.p2align 4
	.globl	EVP_sha3_384
	.type	EVP_sha3_384, @function
EVP_sha3_384:
.LFB401:
	.cfi_startproc
	endbr64
	leaq	sha3_384_md.10449(%rip), %rax
	ret
	.cfi_endproc
.LFE401:
	.size	EVP_sha3_384, .-EVP_sha3_384
	.p2align 4
	.globl	EVP_sha3_512
	.type	EVP_sha3_512, @function
EVP_sha3_512:
.LFB402:
	.cfi_startproc
	endbr64
	leaq	sha3_512_md.10453(%rip), %rax
	ret
	.cfi_endproc
.LFE402:
	.size	EVP_sha3_512, .-EVP_sha3_512
	.p2align 4
	.globl	EVP_shake128
	.type	EVP_shake128, @function
EVP_shake128:
.LFB403:
	.cfi_startproc
	endbr64
	leaq	shake128_md.10457(%rip), %rax
	ret
	.cfi_endproc
.LFE403:
	.size	EVP_shake128, .-EVP_shake128
	.p2align 4
	.globl	EVP_shake256
	.type	EVP_shake256, @function
EVP_shake256:
.LFB404:
	.cfi_startproc
	endbr64
	leaq	shake256_md.10461(%rip), %rax
	ret
	.cfi_endproc
.LFE404:
	.size	EVP_shake256, .-EVP_shake256
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	shake256_md.10461, @object
	.size	shake256_md.10461, 80
shake256_md.10461:
	.long	1101
	.long	0
	.long	32
	.zero	4
	.quad	2
	.quad	shake_init
	.quad	sha3_update
	.quad	sha3_final
	.quad	0
	.quad	0
	.long	136
	.long	400
	.quad	shake_ctrl
	.align 32
	.type	shake128_md.10457, @object
	.size	shake128_md.10457, 80
shake128_md.10457:
	.long	1100
	.long	0
	.long	16
	.zero	4
	.quad	2
	.quad	shake_init
	.quad	sha3_update
	.quad	sha3_final
	.quad	0
	.quad	0
	.long	168
	.long	400
	.quad	shake_ctrl
	.align 32
	.type	sha3_512_md.10453, @object
	.size	sha3_512_md.10453, 80
sha3_512_md.10453:
	.long	1099
	.long	1119
	.long	64
	.zero	4
	.quad	8
	.quad	sha3_init
	.quad	sha3_update
	.quad	sha3_final
	.quad	0
	.quad	0
	.long	72
	.long	400
	.zero	8
	.align 32
	.type	sha3_384_md.10449, @object
	.size	sha3_384_md.10449, 80
sha3_384_md.10449:
	.long	1098
	.long	1118
	.long	48
	.zero	4
	.quad	8
	.quad	sha3_init
	.quad	sha3_update
	.quad	sha3_final
	.quad	0
	.quad	0
	.long	104
	.long	400
	.zero	8
	.align 32
	.type	sha3_256_md.10445, @object
	.size	sha3_256_md.10445, 80
sha3_256_md.10445:
	.long	1097
	.long	1117
	.long	32
	.zero	4
	.quad	8
	.quad	sha3_init
	.quad	sha3_update
	.quad	sha3_final
	.quad	0
	.quad	0
	.long	136
	.long	400
	.zero	8
	.align 32
	.type	sha3_224_md.10441, @object
	.size	sha3_224_md.10441, 80
sha3_224_md.10441:
	.long	1096
	.long	1116
	.long	28
	.zero	4
	.quad	8
	.quad	sha3_init
	.quad	sha3_update
	.quad	sha3_final
	.quad	0
	.quad	0
	.long	144
	.long	400
	.zero	8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
