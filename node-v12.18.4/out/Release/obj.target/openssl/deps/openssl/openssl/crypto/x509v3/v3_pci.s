	.file	"v3_pci.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"language"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../deps/openssl/openssl/crypto/x509v3/v3_pci.c"
	.section	.rodata.str1.1
.LC2:
	.string	",value:"
.LC3:
	.string	",name:"
.LC4:
	.string	"section:"
.LC5:
	.string	"pathlen"
.LC6:
	.string	"policy"
.LC7:
	.string	"hex:"
.LC8:
	.string	"file:"
.LC9:
	.string	"r"
.LC10:
	.string	"text:"
	.text
	.p2align 4
	.type	process_pci_value, @function
process_pci_value:
.LFB1297:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	movl	$9, %ecx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$2104, %rsp
	movq	8(%rdi), %r8
	leaq	.LC0(%rip), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r8, %rsi
	repz cmpsb
	seta	%r10b
	sbbb	$0, %r10b
	movsbl	%r10b, %r15d
	testl	%r15d, %r15d
	jne	.L2
	cmpq	$0, 0(%r13)
	je	.L3
	movl	$93, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$155, %edx
.L40:
	movl	$150, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	subq	$8, %rsp
	pushq	16(%rbx)
	movq	8(%rbx), %r8
	movq	(%rbx), %rdx
	leaq	.LC2(%rip), %r9
	xorl	%eax, %eax
	leaq	.LC3(%rip), %rcx
	leaq	.LC4(%rip), %rsi
	movl	$6, %edi
	call	ERR_add_error_data@PLT
	popq	%r9
	popq	%r10
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L42
	leaq	-40(%rbp), %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	movq	16(%rbx), %rdi
	xorl	%esi, %esi
	call	OBJ_txt2obj@PLT
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	je	.L5
.L14:
	movl	$1, %r15d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L2:
	movl	$8, %ecx
	leaq	.LC5(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%r10b
	sbbb	$0, %r10b
	movsbl	%r10b, %r15d
	testl	%r15d, %r15d
	jne	.L6
	cmpq	$0, (%rdx)
	je	.L7
	movl	$106, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$157, %edx
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$7, %ecx
	leaq	.LC6(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L14
	xorl	%r15d, %r15d
	cmpq	$0, (%r12)
	je	.L43
.L9:
	movq	16(%rbx), %rax
	movl	$4, %ecx
	leaq	.LC7(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L44
	movl	$5, %ecx
	leaq	.LC8(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L45
	movl	$5, %ecx
	leaq	.LC10(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	jne	.L23
	leaq	5(%rax), %rdi
	call	strlen@PLT
	movq	(%r12), %rdx
	movq	%rax, -2120(%rbp)
	movslq	(%rdx), %rcx
	movq	8(%rdx), %rdi
	leaq	.LC1(%rip), %rdx
	leaq	1(%rax,%rcx), %rsi
	movl	$202, %ecx
	call	CRYPTO_realloc@PLT
	testq	%rax, %rax
	je	.L24
	movq	(%r12), %rdx
	movq	16(%rbx), %rsi
	movslq	(%rdx), %rdi
	movq	%rax, 8(%rdx)
	addq	$5, %rsi
	movq	-2120(%rbp), %rdx
	addq	%rax, %rdi
	call	memcpy@PLT
	movq	(%r12), %rdx
	movl	(%rdx), %eax
	addl	-2120(%rbp), %eax
	movl	%eax, (%rdx)
	movq	8(%rdx), %rdx
	cltq
	movb	$0, (%rdx,%rax)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%rdx, %rsi
	movq	%rbx, %rdi
	call	X509V3_get_value_int@PLT
	movl	$112, %r8d
	movl	$156, %edx
	leaq	.LC1(%rip), %rcx
	movl	%eax, %r15d
	testl	%eax, %eax
	jne	.L14
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L23:
	movl	$223, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$152, %edx
.L38:
	movl	$150, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
.L39:
	subq	$8, %rsp
	pushq	16(%rbx)
	movq	(%rbx), %rdx
	xorl	%eax, %eax
	movq	8(%rbx), %r8
	leaq	.LC2(%rip), %r9
	leaq	.LC3(%rip), %rcx
	movl	$6, %edi
	leaq	.LC4(%rip), %rsi
	call	ERR_add_error_data@PLT
	popq	%rax
	popq	%rdx
.L12:
	testl	%r15d, %r15d
	je	.L1
	movq	(%r12), %rdi
	xorl	%r15d, %r15d
	call	ASN1_OCTET_STRING_free@PLT
	movq	$0, (%r12)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L44:
	leaq	-2120(%rbp), %rsi
	leaq	4(%rax), %rdi
	call	OPENSSL_hexstr2buf@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L39
	movq	(%r12), %rax
	movl	$138, %ecx
	leaq	.LC1(%rip), %rdx
	movslq	(%rax), %rsi
	movq	8(%rax), %rdi
	addq	-2120(%rbp), %rsi
	addq	$1, %rsi
	call	CRYPTO_realloc@PLT
	testq	%rax, %rax
	je	.L13
	movq	(%r12), %rdx
	movq	%r13, %rsi
	movslq	(%rdx), %rdi
	movq	%rax, 8(%rdx)
	movq	-2120(%rbp), %rdx
	addq	%rax, %rdi
	call	memcpy@PLT
	movq	(%r12), %rdx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	movl	(%rdx), %eax
	addl	-2120(%rbp), %eax
	movl	%eax, (%rdx)
	movq	8(%rdx), %rdx
	cltq
	movb	$0, (%rdx,%rax)
	movl	$159, %edx
	call	CRYPTO_free@PLT
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L45:
	leaq	5(%rax), %rdi
	leaq	.LC9(%rip), %rsi
	call	BIO_new_file@PLT
	leaq	-2112(%rbp), %r13
	movq	$0, -2136(%rbp)
	movl	$165, %r8d
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L37
	.p2align 4,,10
	.p2align 3
.L21:
	movl	$2048, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	BIO_read@PLT
	movl	%eax, %r8d
	testl	%eax, %eax
	jle	.L46
	movq	(%r12), %rax
	movl	%r8d, %esi
	movl	$174, %ecx
	leaq	.LC1(%rip), %rdx
	movl	%r8d, -2140(%rbp)
	addl	(%rax), %esi
	movq	8(%rax), %rdi
	addl	$1, %esi
	movslq	%esi, %rsi
	call	CRYPTO_realloc@PLT
	movl	-2140(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, -2136(%rbp)
	je	.L47
	movq	%rax, %rcx
	movq	(%r12), %rax
	movslq	%r8d, %rdx
	movq	%r13, %rsi
	movl	%r8d, -2140(%rbp)
	movslq	(%rax), %rdi
	movq	%rcx, 8(%rax)
	addq	%rcx, %rdi
	call	memcpy@PLT
	movq	(%r12), %rax
	movl	-2140(%rbp), %r8d
	addl	(%rax), %r8d
	movl	%r8d, (%rax)
	movq	8(%rax), %rax
	movslq	%r8d, %r8
	movb	$0, (%rax,%r8)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L46:
	jne	.L20
	movl	$8, %esi
	movq	%r14, %rdi
	call	BIO_test_flags@PLT
	testl	%eax, %eax
	jne	.L21
	movq	%r14, %rdi
	call	BIO_free_all@PLT
	cmpq	$0, -2136(%rbp)
	jne	.L14
	movl	$229, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$65, %edx
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$99, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$110, %edx
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L43:
	call	ASN1_OCTET_STRING_new@PLT
	movl	$1, %r15d
	movq	%rax, (%r12)
	testq	%rax, %rax
	jne	.L9
	movl	$123, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$65, %edx
	xorl	%r15d, %r15d
	movl	$150, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	subq	$8, %rsp
	pushq	16(%rbx)
	movq	8(%rbx), %r8
	movq	(%rbx), %rdx
	movl	$6, %edi
	xorl	%eax, %eax
	leaq	.LC2(%rip), %r9
	leaq	.LC3(%rip), %rcx
	leaq	.LC4(%rip), %rsi
	call	ERR_add_error_data@PLT
	popq	%rdi
	popq	%r8
	jmp	.L1
.L20:
	movq	%r14, %rdi
	call	BIO_free_all@PLT
	movl	$196, %r8d
.L37:
	leaq	.LC1(%rip), %rcx
	movl	$32, %edx
	jmp	.L38
.L47:
	movq	(%r12), %rax
	movl	$178, %edx
	leaq	.LC1(%rip), %rsi
	movq	8(%rax), %rdi
	call	CRYPTO_free@PLT
	movq	(%r12), %rax
	movl	$181, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$65, %edx
	movl	$150, %esi
	movl	$34, %edi
	movq	$0, 8(%rax)
	movl	$0, (%rax)
	call	ERR_put_error@PLT
	subq	$8, %rsp
	pushq	16(%rbx)
	movq	8(%rbx), %r8
	movq	(%rbx), %rdx
	leaq	.LC3(%rip), %rcx
	xorl	%eax, %eax
	leaq	.LC4(%rip), %rsi
	leaq	.LC2(%rip), %r9
	movl	$6, %edi
	call	ERR_add_error_data@PLT
	movq	%r14, %rdi
	call	BIO_free_all@PLT
	popq	%rcx
	popq	%rsi
	jmp	.L12
.L24:
	movq	(%r12), %rax
	movl	$215, %edx
	leaq	.LC1(%rip), %rsi
	movq	8(%rax), %rdi
	call	CRYPTO_free@PLT
	movq	(%r12), %rax
	movl	$218, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$65, %edx
	movq	$0, 8(%rax)
	movl	$0, (%rax)
	jmp	.L38
.L13:
	movl	$147, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	movq	(%r12), %rax
	movl	$152, %edx
	leaq	.LC1(%rip), %rsi
	movq	8(%rax), %rdi
	call	CRYPTO_free@PLT
	movq	(%r12), %rax
	movl	$155, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$65, %edx
	movq	$0, 8(%rax)
	movl	$0, (%rax)
	jmp	.L38
.L42:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1297:
	.size	process_pci_value, .-process_pci_value
	.p2align 4
	.type	r2i_pci, @function
r2i_pci:
.LFB1298:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	call	X509V3_parse_list@PLT
	movq	%rax, %r13
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L74:
	cmpq	$0, 16(%rax)
	je	.L53
	leaq	-72(%rbp), %rdx
	leaq	-80(%rbp), %rsi
	movq	%r14, %rcx
	movq	%rax, %rdi
	call	process_pci_value
	testl	%eax, %eax
	je	.L72
.L59:
	addl	$1, %r12d
.L49:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L73
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movq	8(%rax), %rsi
	movq	%rax, %rbx
	testq	%rsi, %rsi
	je	.L53
	cmpb	$64, (%rsi)
	jne	.L74
	movq	-88(%rbp), %rdi
	addq	$1, %rsi
	call	X509V3_get_section@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L75
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L55:
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%ebx, %eax
	jle	.L76
	movl	%ebx, %esi
	movq	%r15, %rdi
	addl	$1, %ebx
	call	OPENSSL_sk_value@PLT
	leaq	-72(%rbp), %rdx
	leaq	-80(%rbp), %rsi
	movq	%r14, %rcx
	movq	%rax, %rdi
	call	process_pci_value
	testl	%eax, %eax
	jne	.L55
	movq	-88(%rbp), %rdi
	movq	%r15, %rsi
	call	X509V3_section_free@PLT
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L76:
	movq	-88(%rbp), %rdi
	movq	%r15, %rsi
	call	X509V3_section_free@PLT
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L53:
	movl	$257, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$153, %edx
.L71:
	movl	$155, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	subq	$8, %rsp
	movq	8(%rbx), %r8
	pushq	16(%rbx)
	movq	(%rbx), %rdx
	leaq	.LC3(%rip), %rcx
	xorl	%eax, %eax
	leaq	.LC4(%rip), %rsi
	leaq	.LC2(%rip), %r9
	movl	$6, %edi
	call	ERR_add_error_data@PLT
	popq	%rcx
	popq	%rsi
.L51:
	movq	-80(%rbp), %rdi
	xorl	%r12d, %r12d
	call	ASN1_OBJECT_free@PLT
	movq	-72(%rbp), %rdi
	call	ASN1_INTEGER_free@PLT
	movq	-64(%rbp), %rdi
	movq	$0, -72(%rbp)
	call	ASN1_OCTET_STRING_free@PLT
	xorl	%edi, %edi
	movq	$0, -64(%rbp)
	call	PROXY_CERT_INFO_EXTENSION_free@PLT
.L64:
	movq	X509V3_conf_free@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L77
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L78
	call	OBJ_obj2nid@PLT
	andl	$-3, %eax
	cmpl	$665, %eax
	jne	.L62
	cmpq	$0, -64(%rbp)
	jne	.L79
.L62:
	call	PROXY_CERT_INFO_EXTENSION_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L80
	movq	8(%rax), %rax
	movq	-80(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	-64(%rbp), %rdx
	movq	$0, -80(%rbp)
	movq	%rdx, 8(%rax)
	movq	-72(%rbp), %rax
	movq	$0, -64(%rbp)
	movq	%rax, (%r12)
	movq	$0, -72(%rbp)
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L72:
	subq	$8, %rsp
	movq	8(%rbx), %r8
	pushq	16(%rbx)
	leaq	.LC2(%rip), %r9
	movq	(%rbx), %rdx
	leaq	.LC3(%rip), %rcx
	movl	$6, %edi
	leaq	.LC4(%rip), %rsi
	call	ERR_add_error_data@PLT
	popq	%rax
	popq	%rdx
	jmp	.L51
.L79:
	movl	$296, %r8d
	movl	$159, %edx
	movl	$155, %esi
	movl	$34, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L51
.L80:
	movl	$303, %r8d
	movl	$65, %edx
	movl	$155, %esi
	movl	$34, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L51
.L75:
	movl	$268, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$135, %edx
	jmp	.L71
.L78:
	movl	$290, %r8d
	movl	$154, %edx
	movl	$155, %esi
	movl	$34, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L51
.L77:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1298:
	.size	r2i_pci, .-r2i_pci
	.section	.rodata.str1.1
.LC11:
	.string	""
.LC12:
	.string	"%*sPath Length Constraint: "
.LC13:
	.string	"infinite"
.LC14:
	.string	"\n"
.LC15:
	.string	"%*sPolicy Language: "
.LC16:
	.string	"%*sPolicy Text: %s\n"
	.text
	.p2align 4
	.type	i2r_pci, @function
i2r_pci:
.LFB1296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%ecx, %r13d
	leaq	.LC11(%rip), %rcx
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	movl	%r13d, %edx
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	leaq	.LC12(%rip), %rsi
	subq	$8, %rsp
	call	BIO_printf@PLT
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L82
	movq	%r12, %rdi
	call	i2a_ASN1_INTEGER@PLT
.L83:
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	movl	%r13d, %edx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC11(%rip), %rcx
	leaq	.LC15(%rip), %rsi
	call	BIO_printf@PLT
	movq	8(%rbx), %rax
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	i2a_ASN1_OBJECT@PLT
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	movq	8(%rbx), %rax
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L84
	movq	8(%rax), %r8
	testq	%r8, %r8
	je	.L84
	leaq	.LC11(%rip), %rcx
	movl	%r13d, %edx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC16(%rip), %rsi
	call	BIO_printf@PLT
.L84:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L83
	.cfi_endproc
.LFE1296:
	.size	i2r_pci, .-i2r_pci
	.globl	v3_pci
	.section	.data.rel.ro,"aw"
	.align 32
	.type	v3_pci, @object
	.size	v3_pci, 104
v3_pci:
	.long	663
	.long	0
	.quad	PROXY_CERT_INFO_EXTENSION_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	i2r_pci
	.quad	r2i_pci
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
