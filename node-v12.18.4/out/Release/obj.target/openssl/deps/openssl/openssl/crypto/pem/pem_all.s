	.file	"pem_all.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"CERTIFICATE REQUEST"
	.text
	.p2align 4
	.globl	PEM_read_bio_X509_REQ
	.type	PEM_read_bio_X509_REQ, @function
PEM_read_bio_X509_REQ:
.LFB779:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	movq	%rdi, %rdx
	movq	d2i_X509_REQ@GOTPCREL(%rip), %rdi
	movq	%rcx, %r9
	movq	%rsi, %rcx
	leaq	.LC0(%rip), %rsi
	jmp	PEM_ASN1_read_bio@PLT
	.cfi_endproc
.LFE779:
	.size	PEM_read_bio_X509_REQ, .-PEM_read_bio_X509_REQ
	.p2align 4
	.globl	PEM_read_X509_REQ
	.type	PEM_read_X509_REQ, @function
PEM_read_X509_REQ:
.LFB780:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	movq	%rdi, %rdx
	movq	d2i_X509_REQ@GOTPCREL(%rip), %rdi
	movq	%rcx, %r9
	movq	%rsi, %rcx
	leaq	.LC0(%rip), %rsi
	jmp	PEM_ASN1_read@PLT
	.cfi_endproc
.LFE780:
	.size	PEM_read_X509_REQ, .-PEM_read_X509_REQ
	.p2align 4
	.globl	PEM_write_bio_X509_REQ
	.type	PEM_write_bio_X509_REQ, @function
PEM_write_bio_X509_REQ:
.LFB781:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	i2d_X509_REQ@GOTPCREL(%rip), %rdi
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write_bio@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE781:
	.size	PEM_write_bio_X509_REQ, .-PEM_write_bio_X509_REQ
	.p2align 4
	.globl	PEM_write_X509_REQ
	.type	PEM_write_X509_REQ, @function
PEM_write_X509_REQ:
.LFB782:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	i2d_X509_REQ@GOTPCREL(%rip), %rdi
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE782:
	.size	PEM_write_X509_REQ, .-PEM_write_X509_REQ
	.section	.rodata.str1.1
.LC1:
	.string	"NEW CERTIFICATE REQUEST"
	.text
	.p2align 4
	.globl	PEM_write_bio_X509_REQ_NEW
	.type	PEM_write_bio_X509_REQ_NEW, @function
PEM_write_bio_X509_REQ_NEW:
.LFB783:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	i2d_X509_REQ@GOTPCREL(%rip), %rdi
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	.LC1(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write_bio@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE783:
	.size	PEM_write_bio_X509_REQ_NEW, .-PEM_write_bio_X509_REQ_NEW
	.p2align 4
	.globl	PEM_write_X509_REQ_NEW
	.type	PEM_write_X509_REQ_NEW, @function
PEM_write_X509_REQ_NEW:
.LFB784:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	i2d_X509_REQ@GOTPCREL(%rip), %rdi
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	.LC1(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE784:
	.size	PEM_write_X509_REQ_NEW, .-PEM_write_X509_REQ_NEW
	.section	.rodata.str1.1
.LC2:
	.string	"X509 CRL"
	.text
	.p2align 4
	.globl	PEM_read_bio_X509_CRL
	.type	PEM_read_bio_X509_CRL, @function
PEM_read_bio_X509_CRL:
.LFB785:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	movq	%rdi, %rdx
	movq	d2i_X509_CRL@GOTPCREL(%rip), %rdi
	movq	%rcx, %r9
	movq	%rsi, %rcx
	leaq	.LC2(%rip), %rsi
	jmp	PEM_ASN1_read_bio@PLT
	.cfi_endproc
.LFE785:
	.size	PEM_read_bio_X509_CRL, .-PEM_read_bio_X509_CRL
	.p2align 4
	.globl	PEM_read_X509_CRL
	.type	PEM_read_X509_CRL, @function
PEM_read_X509_CRL:
.LFB786:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	movq	%rdi, %rdx
	movq	d2i_X509_CRL@GOTPCREL(%rip), %rdi
	movq	%rcx, %r9
	movq	%rsi, %rcx
	leaq	.LC2(%rip), %rsi
	jmp	PEM_ASN1_read@PLT
	.cfi_endproc
.LFE786:
	.size	PEM_read_X509_CRL, .-PEM_read_X509_CRL
	.p2align 4
	.globl	PEM_write_bio_X509_CRL
	.type	PEM_write_bio_X509_CRL, @function
PEM_write_bio_X509_CRL:
.LFB787:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	i2d_X509_CRL@GOTPCREL(%rip), %rdi
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	.LC2(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write_bio@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE787:
	.size	PEM_write_bio_X509_CRL, .-PEM_write_bio_X509_CRL
	.p2align 4
	.globl	PEM_write_X509_CRL
	.type	PEM_write_X509_CRL, @function
PEM_write_X509_CRL:
.LFB788:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	i2d_X509_CRL@GOTPCREL(%rip), %rdi
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	.LC2(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE788:
	.size	PEM_write_X509_CRL, .-PEM_write_X509_CRL
	.section	.rodata.str1.1
.LC3:
	.string	"PKCS7"
	.text
	.p2align 4
	.globl	PEM_read_bio_PKCS7
	.type	PEM_read_bio_PKCS7, @function
PEM_read_bio_PKCS7:
.LFB789:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	movq	%rdi, %rdx
	movq	d2i_PKCS7@GOTPCREL(%rip), %rdi
	movq	%rcx, %r9
	movq	%rsi, %rcx
	leaq	.LC3(%rip), %rsi
	jmp	PEM_ASN1_read_bio@PLT
	.cfi_endproc
.LFE789:
	.size	PEM_read_bio_PKCS7, .-PEM_read_bio_PKCS7
	.p2align 4
	.globl	PEM_read_PKCS7
	.type	PEM_read_PKCS7, @function
PEM_read_PKCS7:
.LFB790:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	movq	%rdi, %rdx
	movq	d2i_PKCS7@GOTPCREL(%rip), %rdi
	movq	%rcx, %r9
	movq	%rsi, %rcx
	leaq	.LC3(%rip), %rsi
	jmp	PEM_ASN1_read@PLT
	.cfi_endproc
.LFE790:
	.size	PEM_read_PKCS7, .-PEM_read_PKCS7
	.p2align 4
	.globl	PEM_write_bio_PKCS7
	.type	PEM_write_bio_PKCS7, @function
PEM_write_bio_PKCS7:
.LFB791:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	i2d_PKCS7@GOTPCREL(%rip), %rdi
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	.LC3(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write_bio@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE791:
	.size	PEM_write_bio_PKCS7, .-PEM_write_bio_PKCS7
	.p2align 4
	.globl	PEM_write_PKCS7
	.type	PEM_write_PKCS7, @function
PEM_write_PKCS7:
.LFB792:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	i2d_PKCS7@GOTPCREL(%rip), %rdi
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	.LC3(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE792:
	.size	PEM_write_PKCS7, .-PEM_write_PKCS7
	.section	.rodata.str1.1
.LC4:
	.string	"CERTIFICATE"
	.text
	.p2align 4
	.globl	PEM_read_bio_NETSCAPE_CERT_SEQUENCE
	.type	PEM_read_bio_NETSCAPE_CERT_SEQUENCE, @function
PEM_read_bio_NETSCAPE_CERT_SEQUENCE:
.LFB793:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	movq	%rdi, %rdx
	movq	d2i_NETSCAPE_CERT_SEQUENCE@GOTPCREL(%rip), %rdi
	movq	%rcx, %r9
	movq	%rsi, %rcx
	leaq	.LC4(%rip), %rsi
	jmp	PEM_ASN1_read_bio@PLT
	.cfi_endproc
.LFE793:
	.size	PEM_read_bio_NETSCAPE_CERT_SEQUENCE, .-PEM_read_bio_NETSCAPE_CERT_SEQUENCE
	.p2align 4
	.globl	PEM_read_NETSCAPE_CERT_SEQUENCE
	.type	PEM_read_NETSCAPE_CERT_SEQUENCE, @function
PEM_read_NETSCAPE_CERT_SEQUENCE:
.LFB794:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	movq	%rdi, %rdx
	movq	d2i_NETSCAPE_CERT_SEQUENCE@GOTPCREL(%rip), %rdi
	movq	%rcx, %r9
	movq	%rsi, %rcx
	leaq	.LC4(%rip), %rsi
	jmp	PEM_ASN1_read@PLT
	.cfi_endproc
.LFE794:
	.size	PEM_read_NETSCAPE_CERT_SEQUENCE, .-PEM_read_NETSCAPE_CERT_SEQUENCE
	.p2align 4
	.globl	PEM_write_bio_NETSCAPE_CERT_SEQUENCE
	.type	PEM_write_bio_NETSCAPE_CERT_SEQUENCE, @function
PEM_write_bio_NETSCAPE_CERT_SEQUENCE:
.LFB795:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	i2d_NETSCAPE_CERT_SEQUENCE@GOTPCREL(%rip), %rdi
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	.LC4(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write_bio@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE795:
	.size	PEM_write_bio_NETSCAPE_CERT_SEQUENCE, .-PEM_write_bio_NETSCAPE_CERT_SEQUENCE
	.p2align 4
	.globl	PEM_write_NETSCAPE_CERT_SEQUENCE
	.type	PEM_write_NETSCAPE_CERT_SEQUENCE, @function
PEM_write_NETSCAPE_CERT_SEQUENCE:
.LFB796:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	i2d_NETSCAPE_CERT_SEQUENCE@GOTPCREL(%rip), %rdi
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	.LC4(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE796:
	.size	PEM_write_NETSCAPE_CERT_SEQUENCE, .-PEM_write_NETSCAPE_CERT_SEQUENCE
	.p2align 4
	.globl	PEM_read_bio_RSAPrivateKey
	.type	PEM_read_bio_RSAPrivateKey, @function
PEM_read_bio_RSAPrivateKey:
.LFB798:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	xorl	%esi, %esi
	subq	$8, %rsp
	call	PEM_read_bio_PrivateKey@PLT
	testq	%rax, %rax
	je	.L33
	movq	%rax, %rdi
	movq	%rax, %r12
	call	EVP_PKEY_get1_RSA@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	EVP_PKEY_free@PLT
	testq	%r13, %r13
	je	.L33
	testq	%rbx, %rbx
	je	.L30
	movq	(%rbx), %rdi
	call	RSA_free@PLT
	movq	%r13, (%rbx)
.L30:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%r13d, %r13d
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE798:
	.size	PEM_read_bio_RSAPrivateKey, .-PEM_read_bio_RSAPrivateKey
	.p2align 4
	.globl	PEM_read_RSAPrivateKey
	.type	PEM_read_RSAPrivateKey, @function
PEM_read_RSAPrivateKey:
.LFB799:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	xorl	%esi, %esi
	subq	$8, %rsp
	call	PEM_read_PrivateKey@PLT
	testq	%rax, %rax
	je	.L44
	movq	%rax, %rdi
	movq	%rax, %r12
	call	EVP_PKEY_get1_RSA@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	EVP_PKEY_free@PLT
	testq	%r13, %r13
	je	.L44
	testq	%rbx, %rbx
	je	.L41
	movq	(%rbx), %rdi
	call	RSA_free@PLT
	movq	%r13, (%rbx)
.L41:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%r13d, %r13d
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE799:
	.size	PEM_read_RSAPrivateKey, .-PEM_read_RSAPrivateKey
	.section	.rodata.str1.1
.LC5:
	.string	"RSA PRIVATE KEY"
	.text
	.p2align 4
	.globl	PEM_write_bio_RSAPrivateKey
	.type	PEM_write_bio_RSAPrivateKey, @function
PEM_write_bio_RSAPrivateKey:
.LFB800:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	16(%rbp)
	pushq	%r9
	movq	%rcx, %r9
	movq	%rsi, %rcx
	leaq	.LC5(%rip), %rsi
	pushq	%r8
	movq	%rdx, %r8
	movq	%rdi, %rdx
	movq	i2d_RSAPrivateKey@GOTPCREL(%rip), %rdi
	call	PEM_ASN1_write_bio@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE800:
	.size	PEM_write_bio_RSAPrivateKey, .-PEM_write_bio_RSAPrivateKey
	.p2align 4
	.globl	PEM_write_RSAPrivateKey
	.type	PEM_write_RSAPrivateKey, @function
PEM_write_RSAPrivateKey:
.LFB801:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	16(%rbp)
	pushq	%r9
	movq	%rcx, %r9
	movq	%rsi, %rcx
	leaq	.LC5(%rip), %rsi
	pushq	%r8
	movq	%rdx, %r8
	movq	%rdi, %rdx
	movq	i2d_RSAPrivateKey@GOTPCREL(%rip), %rdi
	call	PEM_ASN1_write@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE801:
	.size	PEM_write_RSAPrivateKey, .-PEM_write_RSAPrivateKey
	.section	.rodata.str1.1
.LC6:
	.string	"RSA PUBLIC KEY"
	.text
	.p2align 4
	.globl	PEM_read_bio_RSAPublicKey
	.type	PEM_read_bio_RSAPublicKey, @function
PEM_read_bio_RSAPublicKey:
.LFB802:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	movq	%rdi, %rdx
	movq	d2i_RSAPublicKey@GOTPCREL(%rip), %rdi
	movq	%rcx, %r9
	movq	%rsi, %rcx
	leaq	.LC6(%rip), %rsi
	jmp	PEM_ASN1_read_bio@PLT
	.cfi_endproc
.LFE802:
	.size	PEM_read_bio_RSAPublicKey, .-PEM_read_bio_RSAPublicKey
	.p2align 4
	.globl	PEM_read_RSAPublicKey
	.type	PEM_read_RSAPublicKey, @function
PEM_read_RSAPublicKey:
.LFB803:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	movq	%rdi, %rdx
	movq	d2i_RSAPublicKey@GOTPCREL(%rip), %rdi
	movq	%rcx, %r9
	movq	%rsi, %rcx
	leaq	.LC6(%rip), %rsi
	jmp	PEM_ASN1_read@PLT
	.cfi_endproc
.LFE803:
	.size	PEM_read_RSAPublicKey, .-PEM_read_RSAPublicKey
	.p2align 4
	.globl	PEM_write_bio_RSAPublicKey
	.type	PEM_write_bio_RSAPublicKey, @function
PEM_write_bio_RSAPublicKey:
.LFB804:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	i2d_RSAPublicKey@GOTPCREL(%rip), %rdi
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	.LC6(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write_bio@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE804:
	.size	PEM_write_bio_RSAPublicKey, .-PEM_write_bio_RSAPublicKey
	.p2align 4
	.globl	PEM_write_RSAPublicKey
	.type	PEM_write_RSAPublicKey, @function
PEM_write_RSAPublicKey:
.LFB805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	i2d_RSAPublicKey@GOTPCREL(%rip), %rdi
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	.LC6(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE805:
	.size	PEM_write_RSAPublicKey, .-PEM_write_RSAPublicKey
	.section	.rodata.str1.1
.LC7:
	.string	"PUBLIC KEY"
	.text
	.p2align 4
	.globl	PEM_read_bio_RSA_PUBKEY
	.type	PEM_read_bio_RSA_PUBKEY, @function
PEM_read_bio_RSA_PUBKEY:
.LFB806:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	movq	%rdi, %rdx
	movq	d2i_RSA_PUBKEY@GOTPCREL(%rip), %rdi
	movq	%rcx, %r9
	movq	%rsi, %rcx
	leaq	.LC7(%rip), %rsi
	jmp	PEM_ASN1_read_bio@PLT
	.cfi_endproc
.LFE806:
	.size	PEM_read_bio_RSA_PUBKEY, .-PEM_read_bio_RSA_PUBKEY
	.p2align 4
	.globl	PEM_read_RSA_PUBKEY
	.type	PEM_read_RSA_PUBKEY, @function
PEM_read_RSA_PUBKEY:
.LFB807:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	movq	%rdi, %rdx
	movq	d2i_RSA_PUBKEY@GOTPCREL(%rip), %rdi
	movq	%rcx, %r9
	movq	%rsi, %rcx
	leaq	.LC7(%rip), %rsi
	jmp	PEM_ASN1_read@PLT
	.cfi_endproc
.LFE807:
	.size	PEM_read_RSA_PUBKEY, .-PEM_read_RSA_PUBKEY
	.p2align 4
	.globl	PEM_write_bio_RSA_PUBKEY
	.type	PEM_write_bio_RSA_PUBKEY, @function
PEM_write_bio_RSA_PUBKEY:
.LFB808:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	i2d_RSA_PUBKEY@GOTPCREL(%rip), %rdi
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	.LC7(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write_bio@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE808:
	.size	PEM_write_bio_RSA_PUBKEY, .-PEM_write_bio_RSA_PUBKEY
	.p2align 4
	.globl	PEM_write_RSA_PUBKEY
	.type	PEM_write_RSA_PUBKEY, @function
PEM_write_RSA_PUBKEY:
.LFB809:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	i2d_RSA_PUBKEY@GOTPCREL(%rip), %rdi
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	.LC7(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE809:
	.size	PEM_write_RSA_PUBKEY, .-PEM_write_RSA_PUBKEY
	.p2align 4
	.globl	PEM_read_bio_DSAPrivateKey
	.type	PEM_read_bio_DSAPrivateKey, @function
PEM_read_bio_DSAPrivateKey:
.LFB811:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	xorl	%esi, %esi
	subq	$8, %rsp
	call	PEM_read_bio_PrivateKey@PLT
	testq	%rax, %rax
	je	.L71
	movq	%rax, %rdi
	movq	%rax, %r12
	call	EVP_PKEY_get1_DSA@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	EVP_PKEY_free@PLT
	testq	%r13, %r13
	je	.L71
	testq	%rbx, %rbx
	je	.L68
	movq	(%rbx), %rdi
	call	DSA_free@PLT
	movq	%r13, (%rbx)
.L68:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%r13d, %r13d
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE811:
	.size	PEM_read_bio_DSAPrivateKey, .-PEM_read_bio_DSAPrivateKey
	.section	.rodata.str1.1
.LC8:
	.string	"DSA PRIVATE KEY"
	.text
	.p2align 4
	.globl	PEM_write_bio_DSAPrivateKey
	.type	PEM_write_bio_DSAPrivateKey, @function
PEM_write_bio_DSAPrivateKey:
.LFB812:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	16(%rbp)
	pushq	%r9
	movq	%rcx, %r9
	movq	%rsi, %rcx
	leaq	.LC8(%rip), %rsi
	pushq	%r8
	movq	%rdx, %r8
	movq	%rdi, %rdx
	movq	i2d_DSAPrivateKey@GOTPCREL(%rip), %rdi
	call	PEM_ASN1_write_bio@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE812:
	.size	PEM_write_bio_DSAPrivateKey, .-PEM_write_bio_DSAPrivateKey
	.p2align 4
	.globl	PEM_write_DSAPrivateKey
	.type	PEM_write_DSAPrivateKey, @function
PEM_write_DSAPrivateKey:
.LFB813:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	16(%rbp)
	pushq	%r9
	movq	%rcx, %r9
	movq	%rsi, %rcx
	leaq	.LC8(%rip), %rsi
	pushq	%r8
	movq	%rdx, %r8
	movq	%rdi, %rdx
	movq	i2d_DSAPrivateKey@GOTPCREL(%rip), %rdi
	call	PEM_ASN1_write@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE813:
	.size	PEM_write_DSAPrivateKey, .-PEM_write_DSAPrivateKey
	.p2align 4
	.globl	PEM_read_bio_DSA_PUBKEY
	.type	PEM_read_bio_DSA_PUBKEY, @function
PEM_read_bio_DSA_PUBKEY:
.LFB814:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	movq	%rdi, %rdx
	movq	d2i_DSA_PUBKEY@GOTPCREL(%rip), %rdi
	movq	%rcx, %r9
	movq	%rsi, %rcx
	leaq	.LC7(%rip), %rsi
	jmp	PEM_ASN1_read_bio@PLT
	.cfi_endproc
.LFE814:
	.size	PEM_read_bio_DSA_PUBKEY, .-PEM_read_bio_DSA_PUBKEY
	.p2align 4
	.globl	PEM_read_DSA_PUBKEY
	.type	PEM_read_DSA_PUBKEY, @function
PEM_read_DSA_PUBKEY:
.LFB815:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	movq	%rdi, %rdx
	movq	d2i_DSA_PUBKEY@GOTPCREL(%rip), %rdi
	movq	%rcx, %r9
	movq	%rsi, %rcx
	leaq	.LC7(%rip), %rsi
	jmp	PEM_ASN1_read@PLT
	.cfi_endproc
.LFE815:
	.size	PEM_read_DSA_PUBKEY, .-PEM_read_DSA_PUBKEY
	.p2align 4
	.globl	PEM_write_bio_DSA_PUBKEY
	.type	PEM_write_bio_DSA_PUBKEY, @function
PEM_write_bio_DSA_PUBKEY:
.LFB816:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	i2d_DSA_PUBKEY@GOTPCREL(%rip), %rdi
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	.LC7(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write_bio@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE816:
	.size	PEM_write_bio_DSA_PUBKEY, .-PEM_write_bio_DSA_PUBKEY
	.p2align 4
	.globl	PEM_write_DSA_PUBKEY
	.type	PEM_write_DSA_PUBKEY, @function
PEM_write_DSA_PUBKEY:
.LFB817:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	i2d_DSA_PUBKEY@GOTPCREL(%rip), %rdi
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	.LC7(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE817:
	.size	PEM_write_DSA_PUBKEY, .-PEM_write_DSA_PUBKEY
	.p2align 4
	.globl	PEM_read_DSAPrivateKey
	.type	PEM_read_DSAPrivateKey, @function
PEM_read_DSAPrivateKey:
.LFB818:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	xorl	%esi, %esi
	subq	$8, %rsp
	call	PEM_read_PrivateKey@PLT
	testq	%rax, %rax
	je	.L92
	movq	%rax, %rdi
	movq	%rax, %r12
	call	EVP_PKEY_get1_DSA@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	EVP_PKEY_free@PLT
	testq	%r13, %r13
	je	.L92
	testq	%rbx, %rbx
	je	.L89
	movq	(%rbx), %rdi
	call	DSA_free@PLT
	movq	%r13, (%rbx)
.L89:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%r13d, %r13d
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE818:
	.size	PEM_read_DSAPrivateKey, .-PEM_read_DSAPrivateKey
	.section	.rodata.str1.1
.LC9:
	.string	"DSA PARAMETERS"
	.text
	.p2align 4
	.globl	PEM_read_bio_DSAparams
	.type	PEM_read_bio_DSAparams, @function
PEM_read_bio_DSAparams:
.LFB819:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	movq	%rdi, %rdx
	movq	d2i_DSAparams@GOTPCREL(%rip), %rdi
	movq	%rcx, %r9
	movq	%rsi, %rcx
	leaq	.LC9(%rip), %rsi
	jmp	PEM_ASN1_read_bio@PLT
	.cfi_endproc
.LFE819:
	.size	PEM_read_bio_DSAparams, .-PEM_read_bio_DSAparams
	.p2align 4
	.globl	PEM_read_DSAparams
	.type	PEM_read_DSAparams, @function
PEM_read_DSAparams:
.LFB820:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	movq	%rdi, %rdx
	movq	d2i_DSAparams@GOTPCREL(%rip), %rdi
	movq	%rcx, %r9
	movq	%rsi, %rcx
	leaq	.LC9(%rip), %rsi
	jmp	PEM_ASN1_read@PLT
	.cfi_endproc
.LFE820:
	.size	PEM_read_DSAparams, .-PEM_read_DSAparams
	.p2align 4
	.globl	PEM_write_bio_DSAparams
	.type	PEM_write_bio_DSAparams, @function
PEM_write_bio_DSAparams:
.LFB821:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	i2d_DSAparams@GOTPCREL(%rip), %rdi
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	.LC9(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write_bio@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE821:
	.size	PEM_write_bio_DSAparams, .-PEM_write_bio_DSAparams
	.p2align 4
	.globl	PEM_write_DSAparams
	.type	PEM_write_DSAparams, @function
PEM_write_DSAparams:
.LFB822:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	i2d_DSAparams@GOTPCREL(%rip), %rdi
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	.LC9(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE822:
	.size	PEM_write_DSAparams, .-PEM_write_DSAparams
	.p2align 4
	.globl	PEM_read_bio_ECPrivateKey
	.type	PEM_read_bio_ECPrivateKey, @function
PEM_read_bio_ECPrivateKey:
.LFB824:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	xorl	%esi, %esi
	subq	$8, %rsp
	call	PEM_read_bio_PrivateKey@PLT
	testq	%rax, %rax
	je	.L109
	movq	%rax, %rdi
	movq	%rax, %r12
	call	EVP_PKEY_get1_EC_KEY@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	EVP_PKEY_free@PLT
	testq	%r13, %r13
	je	.L109
	testq	%rbx, %rbx
	je	.L106
	movq	(%rbx), %rdi
	call	EC_KEY_free@PLT
	movq	%r13, (%rbx)
.L106:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%r13d, %r13d
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE824:
	.size	PEM_read_bio_ECPrivateKey, .-PEM_read_bio_ECPrivateKey
	.section	.rodata.str1.1
.LC10:
	.string	"EC PARAMETERS"
	.text
	.p2align 4
	.globl	PEM_read_bio_ECPKParameters
	.type	PEM_read_bio_ECPKParameters, @function
PEM_read_bio_ECPKParameters:
.LFB825:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	movq	%rdi, %rdx
	movq	d2i_ECPKParameters@GOTPCREL(%rip), %rdi
	movq	%rcx, %r9
	movq	%rsi, %rcx
	leaq	.LC10(%rip), %rsi
	jmp	PEM_ASN1_read_bio@PLT
	.cfi_endproc
.LFE825:
	.size	PEM_read_bio_ECPKParameters, .-PEM_read_bio_ECPKParameters
	.p2align 4
	.globl	PEM_read_ECPKParameters
	.type	PEM_read_ECPKParameters, @function
PEM_read_ECPKParameters:
.LFB826:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	movq	%rdi, %rdx
	movq	d2i_ECPKParameters@GOTPCREL(%rip), %rdi
	movq	%rcx, %r9
	movq	%rsi, %rcx
	leaq	.LC10(%rip), %rsi
	jmp	PEM_ASN1_read@PLT
	.cfi_endproc
.LFE826:
	.size	PEM_read_ECPKParameters, .-PEM_read_ECPKParameters
	.p2align 4
	.globl	PEM_write_bio_ECPKParameters
	.type	PEM_write_bio_ECPKParameters, @function
PEM_write_bio_ECPKParameters:
.LFB827:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	i2d_ECPKParameters@GOTPCREL(%rip), %rdi
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	.LC10(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write_bio@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE827:
	.size	PEM_write_bio_ECPKParameters, .-PEM_write_bio_ECPKParameters
	.p2align 4
	.globl	PEM_write_ECPKParameters
	.type	PEM_write_ECPKParameters, @function
PEM_write_ECPKParameters:
.LFB828:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	i2d_ECPKParameters@GOTPCREL(%rip), %rdi
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	.LC10(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE828:
	.size	PEM_write_ECPKParameters, .-PEM_write_ECPKParameters
	.section	.rodata.str1.1
.LC11:
	.string	"EC PRIVATE KEY"
	.text
	.p2align 4
	.globl	PEM_write_bio_ECPrivateKey
	.type	PEM_write_bio_ECPrivateKey, @function
PEM_write_bio_ECPrivateKey:
.LFB829:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	16(%rbp)
	pushq	%r9
	movq	%rcx, %r9
	movq	%rsi, %rcx
	leaq	.LC11(%rip), %rsi
	pushq	%r8
	movq	%rdx, %r8
	movq	%rdi, %rdx
	movq	i2d_ECPrivateKey@GOTPCREL(%rip), %rdi
	call	PEM_ASN1_write_bio@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE829:
	.size	PEM_write_bio_ECPrivateKey, .-PEM_write_bio_ECPrivateKey
	.p2align 4
	.globl	PEM_write_ECPrivateKey
	.type	PEM_write_ECPrivateKey, @function
PEM_write_ECPrivateKey:
.LFB830:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	16(%rbp)
	pushq	%r9
	movq	%rcx, %r9
	movq	%rsi, %rcx
	leaq	.LC11(%rip), %rsi
	pushq	%r8
	movq	%rdx, %r8
	movq	%rdi, %rdx
	movq	i2d_ECPrivateKey@GOTPCREL(%rip), %rdi
	call	PEM_ASN1_write@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE830:
	.size	PEM_write_ECPrivateKey, .-PEM_write_ECPrivateKey
	.p2align 4
	.globl	PEM_read_bio_EC_PUBKEY
	.type	PEM_read_bio_EC_PUBKEY, @function
PEM_read_bio_EC_PUBKEY:
.LFB831:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	movq	%rdi, %rdx
	movq	d2i_EC_PUBKEY@GOTPCREL(%rip), %rdi
	movq	%rcx, %r9
	movq	%rsi, %rcx
	leaq	.LC7(%rip), %rsi
	jmp	PEM_ASN1_read_bio@PLT
	.cfi_endproc
.LFE831:
	.size	PEM_read_bio_EC_PUBKEY, .-PEM_read_bio_EC_PUBKEY
	.p2align 4
	.globl	PEM_read_EC_PUBKEY
	.type	PEM_read_EC_PUBKEY, @function
PEM_read_EC_PUBKEY:
.LFB832:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	movq	%rdi, %rdx
	movq	d2i_EC_PUBKEY@GOTPCREL(%rip), %rdi
	movq	%rcx, %r9
	movq	%rsi, %rcx
	leaq	.LC7(%rip), %rsi
	jmp	PEM_ASN1_read@PLT
	.cfi_endproc
.LFE832:
	.size	PEM_read_EC_PUBKEY, .-PEM_read_EC_PUBKEY
	.p2align 4
	.globl	PEM_write_bio_EC_PUBKEY
	.type	PEM_write_bio_EC_PUBKEY, @function
PEM_write_bio_EC_PUBKEY:
.LFB833:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	i2d_EC_PUBKEY@GOTPCREL(%rip), %rdi
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	.LC7(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write_bio@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE833:
	.size	PEM_write_bio_EC_PUBKEY, .-PEM_write_bio_EC_PUBKEY
	.p2align 4
	.globl	PEM_write_EC_PUBKEY
	.type	PEM_write_EC_PUBKEY, @function
PEM_write_EC_PUBKEY:
.LFB834:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	i2d_EC_PUBKEY@GOTPCREL(%rip), %rdi
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	.LC7(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE834:
	.size	PEM_write_EC_PUBKEY, .-PEM_write_EC_PUBKEY
	.p2align 4
	.globl	PEM_read_ECPrivateKey
	.type	PEM_read_ECPrivateKey, @function
PEM_read_ECPrivateKey:
.LFB835:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	xorl	%esi, %esi
	subq	$8, %rsp
	call	PEM_read_PrivateKey@PLT
	testq	%rax, %rax
	je	.L136
	movq	%rax, %rdi
	movq	%rax, %r12
	call	EVP_PKEY_get1_EC_KEY@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	EVP_PKEY_free@PLT
	testq	%r13, %r13
	je	.L136
	testq	%rbx, %rbx
	je	.L133
	movq	(%rbx), %rdi
	call	EC_KEY_free@PLT
	movq	%r13, (%rbx)
.L133:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%r13d, %r13d
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE835:
	.size	PEM_read_ECPrivateKey, .-PEM_read_ECPrivateKey
	.section	.rodata.str1.1
.LC12:
	.string	"DH PARAMETERS"
	.text
	.p2align 4
	.globl	PEM_write_bio_DHparams
	.type	PEM_write_bio_DHparams, @function
PEM_write_bio_DHparams:
.LFB836:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	i2d_DHparams@GOTPCREL(%rip), %rdi
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	.LC12(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write_bio@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE836:
	.size	PEM_write_bio_DHparams, .-PEM_write_bio_DHparams
	.p2align 4
	.globl	PEM_write_DHparams
	.type	PEM_write_DHparams, @function
PEM_write_DHparams:
.LFB837:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	i2d_DHparams@GOTPCREL(%rip), %rdi
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	.LC12(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE837:
	.size	PEM_write_DHparams, .-PEM_write_DHparams
	.section	.rodata.str1.1
.LC13:
	.string	"X9.42 DH PARAMETERS"
	.text
	.p2align 4
	.globl	PEM_write_bio_DHxparams
	.type	PEM_write_bio_DHxparams, @function
PEM_write_bio_DHxparams:
.LFB838:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	i2d_DHxparams@GOTPCREL(%rip), %rdi
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	.LC13(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write_bio@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE838:
	.size	PEM_write_bio_DHxparams, .-PEM_write_bio_DHxparams
	.p2align 4
	.globl	PEM_write_DHxparams
	.type	PEM_write_DHxparams, @function
PEM_write_DHxparams:
.LFB839:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	i2d_DHxparams@GOTPCREL(%rip), %rdi
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	.LC13(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE839:
	.size	PEM_write_DHxparams, .-PEM_write_DHxparams
	.p2align 4
	.globl	PEM_read_bio_PUBKEY
	.type	PEM_read_bio_PUBKEY, @function
PEM_read_bio_PUBKEY:
.LFB840:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	movq	%rdi, %rdx
	movq	d2i_PUBKEY@GOTPCREL(%rip), %rdi
	movq	%rcx, %r9
	movq	%rsi, %rcx
	leaq	.LC7(%rip), %rsi
	jmp	PEM_ASN1_read_bio@PLT
	.cfi_endproc
.LFE840:
	.size	PEM_read_bio_PUBKEY, .-PEM_read_bio_PUBKEY
	.p2align 4
	.globl	PEM_read_PUBKEY
	.type	PEM_read_PUBKEY, @function
PEM_read_PUBKEY:
.LFB841:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	movq	%rdi, %rdx
	movq	d2i_PUBKEY@GOTPCREL(%rip), %rdi
	movq	%rcx, %r9
	movq	%rsi, %rcx
	leaq	.LC7(%rip), %rsi
	jmp	PEM_ASN1_read@PLT
	.cfi_endproc
.LFE841:
	.size	PEM_read_PUBKEY, .-PEM_read_PUBKEY
	.p2align 4
	.globl	PEM_write_bio_PUBKEY
	.type	PEM_write_bio_PUBKEY, @function
PEM_write_bio_PUBKEY:
.LFB842:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	i2d_PUBKEY@GOTPCREL(%rip), %rdi
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	.LC7(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write_bio@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE842:
	.size	PEM_write_bio_PUBKEY, .-PEM_write_bio_PUBKEY
	.p2align 4
	.globl	PEM_write_PUBKEY
	.type	PEM_write_PUBKEY, @function
PEM_write_PUBKEY:
.LFB843:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	i2d_PUBKEY@GOTPCREL(%rip), %rdi
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	.LC7(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE843:
	.size	PEM_write_PUBKEY, .-PEM_write_PUBKEY
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
