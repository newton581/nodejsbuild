	.file	"evp_lib.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/evp/evp_lib.c"
	.align 8
.LC1:
	.string	"assertion failed: j <= sizeof(c->iv)"
	.text
	.p2align 4
	.globl	EVP_CIPHER_param_to_asn1
	.type	EVP_CIPHER_param_to_asn1, @function
EVP_CIPHER_param_to_asn1:
.LFB469:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	subq	$32, %rsp
	movq	(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	56(%rdx), %rax
	testq	%rax, %rax
	je	.L2
	call	*%rax
.L3:
	movl	$-1, %ecx
	testl	%eax, %eax
	movl	%ecx, %r13d
	cmovns	%eax, %r13d
	jle	.L26
.L1:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L27
	addq	$32, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	cmpl	$-2, %eax
	movl	$228, %r9d
	movl	$122, %edx
	cmovne	%edx, %r9d
	cmove	%ecx, %r13d
.L4:
	movl	$44, %r8d
	movl	%r9d, %edx
	leaq	.LC0(%rip), %rcx
	movl	$205, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L2:
	movq	16(%rdx), %rcx
	testb	$16, %ch
	je	.L16
	movq	%rcx, %rax
	andl	$983047, %eax
	cmpq	$65538, %rax
	je	.L5
	ja	.L6
	cmpq	$7, %rax
	ja	.L7
	cmpq	$5, %rax
	ja	.L19
.L8:
	testq	%r12, %r12
	je	.L21
	andb	$8, %ch
	jne	.L28
	movl	12(%rdx), %edx
.L13:
	cmpl	$16, %edx
	ja	.L12
	leaq	24(%rdi), %rsi
	movq	%r12, %rdi
	call	ASN1_TYPE_set_octetstring@PLT
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L16:
	movl	$-1, %r13d
	movl	$122, %r9d
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L6:
	cmpq	$65539, %rax
	jne	.L8
.L19:
	movl	$-1, %r13d
	movl	$228, %r9d
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L7:
	movl	$-1, %r13d
	movl	$228, %r9d
	cmpq	$65537, %rax
	je	.L4
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L21:
	xorl	%r13d, %r13d
	movl	$122, %r9d
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L5:
	cmpl	$246, (%rdx)
	movl	$1, %r13d
	jne	.L1
	xorl	%edx, %edx
	movl	$5, %esi
	movq	%r12, %rdi
	call	ASN1_TYPE_set@PLT
	jmp	.L1
.L28:
	xorl	%edx, %edx
	leaq	-28(%rbp), %rcx
	movl	$37, %esi
	movq	%rdi, -40(%rbp)
	call	EVP_CIPHER_CTX_ctrl@PLT
	movl	-28(%rbp), %edx
	movq	-40(%rbp), %rdi
	cmpl	$1, %eax
	je	.L13
.L12:
	movl	$111, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	OPENSSL_die@PLT
.L27:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE469:
	.size	EVP_CIPHER_param_to_asn1, .-EVP_CIPHER_param_to_asn1
	.section	.rodata.str1.8
	.align 8
.LC2:
	.string	"assertion failed: l <= sizeof(c->iv)"
	.text
	.p2align 4
	.globl	EVP_CIPHER_get_asn1_iv
	.type	EVP_CIPHER_get_asn1_iv, @function
EVP_CIPHER_get_asn1_iv:
.LFB471:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L43
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	%rsi, %r12
	testb	$8, 17(%rax)
	jne	.L56
	movl	12(%rax), %r14d
.L34:
	cmpl	$16, %r14d
	ja	.L33
	leaq	24(%rbx), %r13
	movl	%r14d, %edx
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	ASN1_TYPE_get_octetstring@PLT
	cmpl	%r14d, %eax
	jne	.L44
	testl	%r14d, %r14d
	jne	.L57
.L29:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L58
	addq	$16, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	leaq	40(%rbx), %rax
	cmpl	$8, %r14d
	jb	.L59
	movq	24(%rbx), %rdx
	addq	$48, %rbx
	movq	%rdx, -8(%rbx)
	movl	%r14d, %edx
	andq	$-8, %rbx
	movq	-8(%r13,%rdx), %rcx
	movq	%rcx, -8(%rax,%rdx)
	subq	%rbx, %rax
	subq	%rax, %r13
	addl	%r14d, %eax
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L29
	andl	$-8, %eax
	xorl	%edx, %edx
.L40:
	movl	%edx, %ecx
	addl	$8, %edx
	movq	0(%r13,%rcx), %rsi
	movq	%rsi, (%rbx,%rcx)
	cmpl	%eax, %edx
	jb	.L40
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L43:
	xorl	%r14d, %r14d
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L59:
	testb	$4, %r14b
	jne	.L60
	testl	%r14d, %r14d
	je	.L29
	movzbl	24(%rbx), %edx
	movb	%dl, 40(%rbx)
	testb	$2, %r14b
	je	.L29
	movl	%r14d, %edx
	movzwl	-2(%r13,%rdx), %ecx
	movw	%cx, -2(%rax,%rdx)
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L56:
	leaq	-44(%rbp), %rcx
	xorl	%edx, %edx
	movl	$37, %esi
	call	EVP_CIPHER_CTX_ctrl@PLT
	movl	-44(%rbp), %r14d
	cmpl	$1, %eax
	je	.L34
.L33:
	movl	$94, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	OPENSSL_die@PLT
	.p2align 4,,10
	.p2align 3
.L44:
	movl	$-1, %r14d
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L60:
	movl	24(%rbx), %edx
	movl	%edx, 40(%rbx)
	movl	%r14d, %edx
	movl	-4(%r13,%rdx), %ecx
	movl	%ecx, -4(%rax,%rdx)
	jmp	.L29
.L58:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE471:
	.size	EVP_CIPHER_get_asn1_iv, .-EVP_CIPHER_get_asn1_iv
	.p2align 4
	.globl	EVP_CIPHER_asn1_to_param
	.type	EVP_CIPHER_asn1_to_param, @function
EVP_CIPHER_asn1_to_param:
.LFB470:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rdx
	movq	64(%rdx), %rax
	testq	%rax, %rax
	je	.L62
	call	*%rax
.L63:
	movl	$-1, %ecx
	testl	%eax, %eax
	movl	%ecx, %r12d
	cmovns	%eax, %r12d
	jle	.L78
.L61:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	cmpl	$-2, %eax
	movl	$107, %edx
	movl	$122, %esi
	cmovne	%esi, %edx
	cmove	%ecx, %r12d
.L64:
	movl	$79, %r8d
	movl	$204, %esi
	leaq	.LC0(%rip), %rcx
	movl	$6, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	movq	16(%rdx), %rax
	testb	$16, %ah
	je	.L69
	andl	$983047, %eax
	movl	$1, %r12d
	cmpq	$65538, %rax
	je	.L61
	ja	.L66
	cmpq	$7, %rax
	ja	.L67
	cmpq	$5, %rax
	ja	.L73
.L68:
	call	EVP_CIPHER_get_asn1_iv
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L69:
	movl	$-1, %r12d
	movl	$122, %edx
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L66:
	cmpq	$65539, %rax
	jne	.L68
.L73:
	movl	$-1, %r12d
	movl	$107, %edx
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L67:
	movl	$-1, %r12d
	movl	$107, %edx
	cmpq	$65537, %rax
	je	.L64
	jmp	.L68
	.cfi_endproc
.LFE470:
	.size	EVP_CIPHER_asn1_to_param, .-EVP_CIPHER_asn1_to_param
	.p2align 4
	.globl	EVP_CIPHER_set_asn1_iv
	.type	EVP_CIPHER_set_asn1_iv, @function
EVP_CIPHER_set_asn1_iv:
.LFB472:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L87
	movq	(%rdi), %rax
	movq	%rsi, %r12
	testb	$8, 17(%rax)
	jne	.L89
	movl	12(%rax), %edx
.L84:
	cmpl	$16, %edx
	ja	.L83
	leaq	24(%rdi), %rsi
	movq	%r12, %rdi
	call	ASN1_TYPE_set_octetstring@PLT
.L79:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L90
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L89:
	xorl	%edx, %edx
	leaq	-28(%rbp), %rcx
	movl	$37, %esi
	movq	%rdi, -40(%rbp)
	call	EVP_CIPHER_CTX_ctrl@PLT
	movl	-28(%rbp), %edx
	movq	-40(%rbp), %rdi
	cmpl	$1, %eax
	je	.L84
.L83:
	movl	$111, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	OPENSSL_die@PLT
.L90:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE472:
	.size	EVP_CIPHER_set_asn1_iv, .-EVP_CIPHER_set_asn1_iv
	.p2align 4
	.globl	EVP_CIPHER_type
	.type	EVP_CIPHER_type, @function
EVP_CIPHER_type:
.LFB473:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	(%rdi), %r12d
	cmpl	$421, %r12d
	je	.L91
	jg	.L93
	cmpl	$61, %r12d
	je	.L104
	jle	.L127
	cmpl	$98, %r12d
	je	.L106
	cmpl	$166, %r12d
	jne	.L128
.L106:
	movl	$37, %r12d
.L91:
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L104:
	.cfi_restore_state
	movl	$30, %r12d
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	cmpl	$659, %r12d
	jg	.L95
	cmpl	$649, %r12d
	jle	.L129
	subl	$651, %r12d
	cmpl	$8, %r12d
	ja	.L96
	leaq	.L97(%rip), %rdx
	movslq	(%rdx,%r12,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L97:
	.long	.L99-.L97
	.long	.L98-.L97
	.long	.L96-.L97
	.long	.L99-.L97
	.long	.L98-.L97
	.long	.L104-.L97
	.long	.L104-.L97
	.long	.L104-.L97
	.long	.L104-.L97
	.text
.L98:
	movl	$429, %r12d
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L99:
	.cfi_restore_state
	movl	$425, %r12d
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L96:
	.cfi_restore_state
	movl	$421, %r12d
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	cmpl	$425, %r12d
	je	.L91
	cmpl	$429, %r12d
	je	.L91
.L95:
	movl	%r12d, %edi
	call	OBJ_nid2obj@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	OBJ_get0_data@PLT
	movq	%r13, %rdi
	testq	%rax, %rax
	movl	$0, %eax
	cmove	%eax, %r12d
	call	ASN1_OBJECT_free@PLT
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	cmpl	$30, %r12d
	je	.L91
	cmpl	$37, %r12d
	je	.L91
	cmpl	$5, %r12d
	jne	.L95
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L128:
	.cfi_restore_state
	cmpl	$97, %r12d
	jne	.L95
	movl	$5, %r12d
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE473:
	.size	EVP_CIPHER_type, .-EVP_CIPHER_type
	.p2align 4
	.globl	EVP_CIPHER_block_size
	.type	EVP_CIPHER_block_size, @function
EVP_CIPHER_block_size:
.LFB474:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	ret
	.cfi_endproc
.LFE474:
	.size	EVP_CIPHER_block_size, .-EVP_CIPHER_block_size
	.p2align 4
	.globl	EVP_CIPHER_CTX_block_size
	.type	EVP_CIPHER_CTX_block_size, @function
EVP_CIPHER_CTX_block_size:
.LFB475:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	4(%rax), %eax
	ret
	.cfi_endproc
.LFE475:
	.size	EVP_CIPHER_CTX_block_size, .-EVP_CIPHER_CTX_block_size
	.p2align 4
	.globl	EVP_CIPHER_impl_ctx_size
	.type	EVP_CIPHER_impl_ctx_size, @function
EVP_CIPHER_impl_ctx_size:
.LFB476:
	.cfi_startproc
	endbr64
	movl	48(%rdi), %eax
	ret
	.cfi_endproc
.LFE476:
	.size	EVP_CIPHER_impl_ctx_size, .-EVP_CIPHER_impl_ctx_size
	.p2align 4
	.globl	EVP_Cipher
	.type	EVP_Cipher, @function
EVP_Cipher:
.LFB477:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	%ecx, %ecx
	jmp	*32(%rax)
	.cfi_endproc
.LFE477:
	.size	EVP_Cipher, .-EVP_Cipher
	.p2align 4
	.globl	EVP_CIPHER_CTX_cipher
	.type	EVP_CIPHER_CTX_cipher, @function
EVP_CIPHER_CTX_cipher:
.LFB478:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE478:
	.size	EVP_CIPHER_CTX_cipher, .-EVP_CIPHER_CTX_cipher
	.p2align 4
	.globl	EVP_CIPHER_CTX_encrypting
	.type	EVP_CIPHER_CTX_encrypting, @function
EVP_CIPHER_CTX_encrypting:
.LFB479:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %eax
	ret
	.cfi_endproc
.LFE479:
	.size	EVP_CIPHER_CTX_encrypting, .-EVP_CIPHER_CTX_encrypting
	.p2align 4
	.globl	EVP_CIPHER_flags
	.type	EVP_CIPHER_flags, @function
EVP_CIPHER_flags:
.LFB480:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE480:
	.size	EVP_CIPHER_flags, .-EVP_CIPHER_flags
	.p2align 4
	.globl	EVP_CIPHER_CTX_get_app_data
	.type	EVP_CIPHER_CTX_get_app_data, @function
EVP_CIPHER_CTX_get_app_data:
.LFB481:
	.cfi_startproc
	endbr64
	movq	96(%rdi), %rax
	ret
	.cfi_endproc
.LFE481:
	.size	EVP_CIPHER_CTX_get_app_data, .-EVP_CIPHER_CTX_get_app_data
	.p2align 4
	.globl	EVP_CIPHER_CTX_set_app_data
	.type	EVP_CIPHER_CTX_set_app_data, @function
EVP_CIPHER_CTX_set_app_data:
.LFB482:
	.cfi_startproc
	endbr64
	movq	%rsi, 96(%rdi)
	ret
	.cfi_endproc
.LFE482:
	.size	EVP_CIPHER_CTX_set_app_data, .-EVP_CIPHER_CTX_set_app_data
	.p2align 4
	.globl	EVP_CIPHER_CTX_get_cipher_data
	.type	EVP_CIPHER_CTX_get_cipher_data, @function
EVP_CIPHER_CTX_get_cipher_data:
.LFB483:
	.cfi_startproc
	endbr64
	movq	120(%rdi), %rax
	ret
	.cfi_endproc
.LFE483:
	.size	EVP_CIPHER_CTX_get_cipher_data, .-EVP_CIPHER_CTX_get_cipher_data
	.p2align 4
	.globl	EVP_CIPHER_CTX_set_cipher_data
	.type	EVP_CIPHER_CTX_set_cipher_data, @function
EVP_CIPHER_CTX_set_cipher_data:
.LFB484:
	.cfi_startproc
	endbr64
	movq	120(%rdi), %rax
	movq	%rsi, 120(%rdi)
	ret
	.cfi_endproc
.LFE484:
	.size	EVP_CIPHER_CTX_set_cipher_data, .-EVP_CIPHER_CTX_set_cipher_data
	.p2align 4
	.globl	EVP_CIPHER_iv_length
	.type	EVP_CIPHER_iv_length, @function
EVP_CIPHER_iv_length:
.LFB485:
	.cfi_startproc
	endbr64
	movl	12(%rdi), %eax
	ret
	.cfi_endproc
.LFE485:
	.size	EVP_CIPHER_iv_length, .-EVP_CIPHER_iv_length
	.p2align 4
	.globl	EVP_CIPHER_CTX_iv_length
	.type	EVP_CIPHER_CTX_iv_length, @function
EVP_CIPHER_CTX_iv_length:
.LFB486:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	testb	$8, 17(%rax)
	jne	.L149
	movl	12(%rax), %eax
.L142:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L150
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L149:
	.cfi_restore_state
	xorl	%edx, %edx
	leaq	-12(%rbp), %rcx
	movl	$37, %esi
	call	EVP_CIPHER_CTX_ctrl@PLT
	cmpl	$1, %eax
	movl	$-1, %eax
	cmove	-12(%rbp), %eax
	jmp	.L142
.L150:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE486:
	.size	EVP_CIPHER_CTX_iv_length, .-EVP_CIPHER_CTX_iv_length
	.p2align 4
	.globl	EVP_CIPHER_CTX_original_iv
	.type	EVP_CIPHER_CTX_original_iv, @function
EVP_CIPHER_CTX_original_iv:
.LFB487:
	.cfi_startproc
	endbr64
	leaq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE487:
	.size	EVP_CIPHER_CTX_original_iv, .-EVP_CIPHER_CTX_original_iv
	.p2align 4
	.globl	EVP_CIPHER_CTX_iv
	.type	EVP_CIPHER_CTX_iv, @function
EVP_CIPHER_CTX_iv:
.LFB488:
	.cfi_startproc
	endbr64
	leaq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE488:
	.size	EVP_CIPHER_CTX_iv, .-EVP_CIPHER_CTX_iv
	.p2align 4
	.globl	EVP_CIPHER_CTX_iv_noconst
	.type	EVP_CIPHER_CTX_iv_noconst, @function
EVP_CIPHER_CTX_iv_noconst:
.LFB545:
	.cfi_startproc
	endbr64
	leaq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE545:
	.size	EVP_CIPHER_CTX_iv_noconst, .-EVP_CIPHER_CTX_iv_noconst
	.p2align 4
	.globl	EVP_CIPHER_CTX_buf_noconst
	.type	EVP_CIPHER_CTX_buf_noconst, @function
EVP_CIPHER_CTX_buf_noconst:
.LFB490:
	.cfi_startproc
	endbr64
	leaq	56(%rdi), %rax
	ret
	.cfi_endproc
.LFE490:
	.size	EVP_CIPHER_CTX_buf_noconst, .-EVP_CIPHER_CTX_buf_noconst
	.p2align 4
	.globl	EVP_CIPHER_CTX_num
	.type	EVP_CIPHER_CTX_num, @function
EVP_CIPHER_CTX_num:
.LFB491:
	.cfi_startproc
	endbr64
	movl	88(%rdi), %eax
	ret
	.cfi_endproc
.LFE491:
	.size	EVP_CIPHER_CTX_num, .-EVP_CIPHER_CTX_num
	.p2align 4
	.globl	EVP_CIPHER_CTX_set_num
	.type	EVP_CIPHER_CTX_set_num, @function
EVP_CIPHER_CTX_set_num:
.LFB492:
	.cfi_startproc
	endbr64
	movl	%esi, 88(%rdi)
	ret
	.cfi_endproc
.LFE492:
	.size	EVP_CIPHER_CTX_set_num, .-EVP_CIPHER_CTX_set_num
	.p2align 4
	.globl	EVP_CIPHER_key_length
	.type	EVP_CIPHER_key_length, @function
EVP_CIPHER_key_length:
.LFB493:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE493:
	.size	EVP_CIPHER_key_length, .-EVP_CIPHER_key_length
	.p2align 4
	.globl	EVP_CIPHER_CTX_key_length
	.type	EVP_CIPHER_CTX_key_length, @function
EVP_CIPHER_CTX_key_length:
.LFB494:
	.cfi_startproc
	endbr64
	movl	104(%rdi), %eax
	ret
	.cfi_endproc
.LFE494:
	.size	EVP_CIPHER_CTX_key_length, .-EVP_CIPHER_CTX_key_length
	.p2align 4
	.globl	EVP_CIPHER_nid
	.type	EVP_CIPHER_nid, @function
EVP_CIPHER_nid:
.LFB495:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	ret
	.cfi_endproc
.LFE495:
	.size	EVP_CIPHER_nid, .-EVP_CIPHER_nid
	.p2align 4
	.globl	EVP_CIPHER_CTX_nid
	.type	EVP_CIPHER_CTX_nid, @function
EVP_CIPHER_CTX_nid:
.LFB496:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	(%rax), %eax
	ret
	.cfi_endproc
.LFE496:
	.size	EVP_CIPHER_CTX_nid, .-EVP_CIPHER_CTX_nid
	.p2align 4
	.globl	EVP_MD_block_size
	.type	EVP_MD_block_size, @function
EVP_MD_block_size:
.LFB541:
	.cfi_startproc
	endbr64
	movl	64(%rdi), %eax
	ret
	.cfi_endproc
.LFE541:
	.size	EVP_MD_block_size, .-EVP_MD_block_size
	.p2align 4
	.globl	EVP_MD_type
	.type	EVP_MD_type, @function
EVP_MD_type:
.LFB498:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	ret
	.cfi_endproc
.LFE498:
	.size	EVP_MD_type, .-EVP_MD_type
	.p2align 4
	.globl	EVP_MD_pkey_type
	.type	EVP_MD_pkey_type, @function
EVP_MD_pkey_type:
.LFB499:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	ret
	.cfi_endproc
.LFE499:
	.size	EVP_MD_pkey_type, .-EVP_MD_pkey_type
	.p2align 4
	.globl	EVP_MD_size
	.type	EVP_MD_size, @function
EVP_MD_size:
.LFB500:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L171
	movl	8(%rdi), %eax
	ret
.L171:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$159, %edx
	movl	$162, %esi
	movl	$6, %edi
	movl	$323, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	movl	$-1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE500:
	.size	EVP_MD_size, .-EVP_MD_size
	.p2align 4
	.globl	EVP_MD_flags
	.type	EVP_MD_flags, @function
EVP_MD_flags:
.LFB543:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE543:
	.size	EVP_MD_flags, .-EVP_MD_flags
	.p2align 4
	.globl	EVP_MD_meth_new
	.type	EVP_MD_meth_new, @function
EVP_MD_meth_new:
.LFB502:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$336, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%edi, %r12d
	movl	$80, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L173
	movl	%r12d, (%rax)
	movl	%ebx, 4(%rax)
.L173:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE502:
	.size	EVP_MD_meth_new, .-EVP_MD_meth_new
	.p2align 4
	.globl	EVP_MD_meth_dup
	.type	EVP_MD_meth_dup, @function
EVP_MD_meth_dup:
.LFB503:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$336, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rdi), %r12
	movl	$80, %edi
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L179
	movq	%r12, (%rax)
	movdqu	(%rbx), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%rbx), %xmm1
	movups	%xmm1, 16(%rax)
	movdqu	32(%rbx), %xmm2
	movups	%xmm2, 32(%rax)
	movdqu	48(%rbx), %xmm3
	movups	%xmm3, 48(%rax)
	movdqu	64(%rbx), %xmm4
	movups	%xmm4, 64(%rax)
.L179:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE503:
	.size	EVP_MD_meth_dup, .-EVP_MD_meth_dup
	.p2align 4
	.globl	EVP_MD_meth_free
	.type	EVP_MD_meth_free, @function
EVP_MD_meth_free:
.LFB504:
	.cfi_startproc
	endbr64
	movl	$354, %edx
	leaq	.LC0(%rip), %rsi
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE504:
	.size	EVP_MD_meth_free, .-EVP_MD_meth_free
	.p2align 4
	.globl	EVP_MD_meth_set_input_blocksize
	.type	EVP_MD_meth_set_input_blocksize, @function
EVP_MD_meth_set_input_blocksize:
.LFB505:
	.cfi_startproc
	endbr64
	movl	%esi, 64(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE505:
	.size	EVP_MD_meth_set_input_blocksize, .-EVP_MD_meth_set_input_blocksize
	.p2align 4
	.globl	EVP_MD_meth_set_result_size
	.type	EVP_MD_meth_set_result_size, @function
EVP_MD_meth_set_result_size:
.LFB506:
	.cfi_startproc
	endbr64
	movl	%esi, 8(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE506:
	.size	EVP_MD_meth_set_result_size, .-EVP_MD_meth_set_result_size
	.p2align 4
	.globl	EVP_MD_meth_set_app_datasize
	.type	EVP_MD_meth_set_app_datasize, @function
EVP_MD_meth_set_app_datasize:
.LFB507:
	.cfi_startproc
	endbr64
	movl	%esi, 68(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE507:
	.size	EVP_MD_meth_set_app_datasize, .-EVP_MD_meth_set_app_datasize
	.p2align 4
	.globl	EVP_MD_meth_set_flags
	.type	EVP_MD_meth_set_flags, @function
EVP_MD_meth_set_flags:
.LFB508:
	.cfi_startproc
	endbr64
	movq	%rsi, 16(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE508:
	.size	EVP_MD_meth_set_flags, .-EVP_MD_meth_set_flags
	.p2align 4
	.globl	EVP_MD_meth_set_init
	.type	EVP_MD_meth_set_init, @function
EVP_MD_meth_set_init:
.LFB509:
	.cfi_startproc
	endbr64
	movq	%rsi, 24(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE509:
	.size	EVP_MD_meth_set_init, .-EVP_MD_meth_set_init
	.p2align 4
	.globl	EVP_MD_meth_set_update
	.type	EVP_MD_meth_set_update, @function
EVP_MD_meth_set_update:
.LFB510:
	.cfi_startproc
	endbr64
	movq	%rsi, 32(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE510:
	.size	EVP_MD_meth_set_update, .-EVP_MD_meth_set_update
	.p2align 4
	.globl	EVP_MD_meth_set_final
	.type	EVP_MD_meth_set_final, @function
EVP_MD_meth_set_final:
.LFB511:
	.cfi_startproc
	endbr64
	movq	%rsi, 40(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE511:
	.size	EVP_MD_meth_set_final, .-EVP_MD_meth_set_final
	.p2align 4
	.globl	EVP_MD_meth_set_copy
	.type	EVP_MD_meth_set_copy, @function
EVP_MD_meth_set_copy:
.LFB512:
	.cfi_startproc
	endbr64
	movq	%rsi, 48(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE512:
	.size	EVP_MD_meth_set_copy, .-EVP_MD_meth_set_copy
	.p2align 4
	.globl	EVP_MD_meth_set_cleanup
	.type	EVP_MD_meth_set_cleanup, @function
EVP_MD_meth_set_cleanup:
.LFB513:
	.cfi_startproc
	endbr64
	movq	%rsi, 56(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE513:
	.size	EVP_MD_meth_set_cleanup, .-EVP_MD_meth_set_cleanup
	.p2align 4
	.globl	EVP_MD_meth_set_ctrl
	.type	EVP_MD_meth_set_ctrl, @function
EVP_MD_meth_set_ctrl:
.LFB514:
	.cfi_startproc
	endbr64
	movq	%rsi, 72(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE514:
	.size	EVP_MD_meth_set_ctrl, .-EVP_MD_meth_set_ctrl
	.p2align 4
	.globl	EVP_MD_meth_get_input_blocksize
	.type	EVP_MD_meth_get_input_blocksize, @function
EVP_MD_meth_get_input_blocksize:
.LFB515:
	.cfi_startproc
	endbr64
	movl	64(%rdi), %eax
	ret
	.cfi_endproc
.LFE515:
	.size	EVP_MD_meth_get_input_blocksize, .-EVP_MD_meth_get_input_blocksize
	.p2align 4
	.globl	EVP_MD_meth_get_result_size
	.type	EVP_MD_meth_get_result_size, @function
EVP_MD_meth_get_result_size:
.LFB516:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE516:
	.size	EVP_MD_meth_get_result_size, .-EVP_MD_meth_get_result_size
	.p2align 4
	.globl	EVP_MD_meth_get_app_datasize
	.type	EVP_MD_meth_get_app_datasize, @function
EVP_MD_meth_get_app_datasize:
.LFB517:
	.cfi_startproc
	endbr64
	movl	68(%rdi), %eax
	ret
	.cfi_endproc
.LFE517:
	.size	EVP_MD_meth_get_app_datasize, .-EVP_MD_meth_get_app_datasize
	.p2align 4
	.globl	EVP_MD_meth_get_flags
	.type	EVP_MD_meth_get_flags, @function
EVP_MD_meth_get_flags:
.LFB518:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE518:
	.size	EVP_MD_meth_get_flags, .-EVP_MD_meth_get_flags
	.p2align 4
	.globl	EVP_MD_meth_get_init
	.type	EVP_MD_meth_get_init, @function
EVP_MD_meth_get_init:
.LFB519:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE519:
	.size	EVP_MD_meth_get_init, .-EVP_MD_meth_get_init
	.p2align 4
	.globl	EVP_MD_meth_get_update
	.type	EVP_MD_meth_get_update, @function
EVP_MD_meth_get_update:
.LFB520:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE520:
	.size	EVP_MD_meth_get_update, .-EVP_MD_meth_get_update
	.p2align 4
	.globl	EVP_MD_meth_get_final
	.type	EVP_MD_meth_get_final, @function
EVP_MD_meth_get_final:
.LFB521:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE521:
	.size	EVP_MD_meth_get_final, .-EVP_MD_meth_get_final
	.p2align 4
	.globl	EVP_MD_meth_get_copy
	.type	EVP_MD_meth_get_copy, @function
EVP_MD_meth_get_copy:
.LFB522:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	ret
	.cfi_endproc
.LFE522:
	.size	EVP_MD_meth_get_copy, .-EVP_MD_meth_get_copy
	.p2align 4
	.globl	EVP_MD_meth_get_cleanup
	.type	EVP_MD_meth_get_cleanup, @function
EVP_MD_meth_get_cleanup:
.LFB523:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rax
	ret
	.cfi_endproc
.LFE523:
	.size	EVP_MD_meth_get_cleanup, .-EVP_MD_meth_get_cleanup
	.p2align 4
	.globl	EVP_MD_meth_get_ctrl
	.type	EVP_MD_meth_get_ctrl, @function
EVP_MD_meth_get_ctrl:
.LFB524:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rax
	ret
	.cfi_endproc
.LFE524:
	.size	EVP_MD_meth_get_ctrl, .-EVP_MD_meth_get_ctrl
	.p2align 4
	.globl	EVP_MD_CTX_md
	.type	EVP_MD_CTX_md, @function
EVP_MD_CTX_md:
.LFB525:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L208
	movq	(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE525:
	.size	EVP_MD_CTX_md, .-EVP_MD_CTX_md
	.p2align 4
	.globl	EVP_MD_CTX_pkey_ctx
	.type	EVP_MD_CTX_pkey_ctx, @function
EVP_MD_CTX_pkey_ctx:
.LFB526:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE526:
	.size	EVP_MD_CTX_pkey_ctx, .-EVP_MD_CTX_pkey_ctx
	.p2align 4
	.globl	EVP_MD_CTX_set_pkey_ctx
	.type	EVP_MD_CTX_set_pkey_ctx, @function
EVP_MD_CTX_set_pkey_ctx:
.LFB527:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	16(%rdi), %rax
	movq	%rdi, %rbx
	testb	$4, %ah
	je	.L215
.L211:
	movq	%rax, %rdx
	andb	$-5, %ah
	movq	%r12, 32(%rbx)
	orb	$4, %dh
	testq	%r12, %r12
	cmovne	%rdx, %rax
	movq	%rax, 16(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L215:
	.cfi_restore_state
	movq	32(%rdi), %rdi
	call	EVP_PKEY_CTX_free@PLT
	movq	16(%rbx), %rax
	jmp	.L211
	.cfi_endproc
.LFE527:
	.size	EVP_MD_CTX_set_pkey_ctx, .-EVP_MD_CTX_set_pkey_ctx
	.p2align 4
	.globl	EVP_MD_CTX_md_data
	.type	EVP_MD_CTX_md_data, @function
EVP_MD_CTX_md_data:
.LFB528:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE528:
	.size	EVP_MD_CTX_md_data, .-EVP_MD_CTX_md_data
	.p2align 4
	.globl	EVP_MD_CTX_update_fn
	.type	EVP_MD_CTX_update_fn, @function
EVP_MD_CTX_update_fn:
.LFB529:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE529:
	.size	EVP_MD_CTX_update_fn, .-EVP_MD_CTX_update_fn
	.p2align 4
	.globl	EVP_MD_CTX_set_update_fn
	.type	EVP_MD_CTX_set_update_fn, @function
EVP_MD_CTX_set_update_fn:
.LFB530:
	.cfi_startproc
	endbr64
	movq	%rsi, 40(%rdi)
	ret
	.cfi_endproc
.LFE530:
	.size	EVP_MD_CTX_set_update_fn, .-EVP_MD_CTX_set_update_fn
	.p2align 4
	.globl	EVP_MD_CTX_set_flags
	.type	EVP_MD_CTX_set_flags, @function
EVP_MD_CTX_set_flags:
.LFB531:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	orq	%rsi, 16(%rdi)
	ret
	.cfi_endproc
.LFE531:
	.size	EVP_MD_CTX_set_flags, .-EVP_MD_CTX_set_flags
	.p2align 4
	.globl	EVP_MD_CTX_clear_flags
	.type	EVP_MD_CTX_clear_flags, @function
EVP_MD_CTX_clear_flags:
.LFB532:
	.cfi_startproc
	endbr64
	notl	%esi
	movslq	%esi, %rsi
	andq	%rsi, 16(%rdi)
	ret
	.cfi_endproc
.LFE532:
	.size	EVP_MD_CTX_clear_flags, .-EVP_MD_CTX_clear_flags
	.p2align 4
	.globl	EVP_MD_CTX_test_flags
	.type	EVP_MD_CTX_test_flags, @function
EVP_MD_CTX_test_flags:
.LFB533:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %eax
	andl	%esi, %eax
	ret
	.cfi_endproc
.LFE533:
	.size	EVP_MD_CTX_test_flags, .-EVP_MD_CTX_test_flags
	.p2align 4
	.globl	EVP_CIPHER_CTX_set_flags
	.type	EVP_CIPHER_CTX_set_flags, @function
EVP_CIPHER_CTX_set_flags:
.LFB534:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	orq	%rsi, 112(%rdi)
	ret
	.cfi_endproc
.LFE534:
	.size	EVP_CIPHER_CTX_set_flags, .-EVP_CIPHER_CTX_set_flags
	.p2align 4
	.globl	EVP_CIPHER_CTX_clear_flags
	.type	EVP_CIPHER_CTX_clear_flags, @function
EVP_CIPHER_CTX_clear_flags:
.LFB535:
	.cfi_startproc
	endbr64
	notl	%esi
	movslq	%esi, %rsi
	andq	%rsi, 112(%rdi)
	ret
	.cfi_endproc
.LFE535:
	.size	EVP_CIPHER_CTX_clear_flags, .-EVP_CIPHER_CTX_clear_flags
	.p2align 4
	.globl	EVP_CIPHER_CTX_test_flags
	.type	EVP_CIPHER_CTX_test_flags, @function
EVP_CIPHER_CTX_test_flags:
.LFB536:
	.cfi_startproc
	endbr64
	movl	112(%rdi), %eax
	andl	%esi, %eax
	ret
	.cfi_endproc
.LFE536:
	.size	EVP_CIPHER_CTX_test_flags, .-EVP_CIPHER_CTX_test_flags
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
