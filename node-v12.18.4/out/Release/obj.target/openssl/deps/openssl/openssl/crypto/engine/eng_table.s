	.file	"eng_table.c"
	.text
	.p2align 4
	.type	engine_pile_hash, @function
engine_pile_hash:
.LFB868:
	.cfi_startproc
	endbr64
	movslq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE868:
	.size	engine_pile_hash, .-engine_pile_hash
	.p2align 4
	.type	engine_pile_cmp, @function
engine_pile_cmp:
.LFB869:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	subl	(%rsi), %eax
	ret
	.cfi_endproc
.LFE869:
	.size	engine_pile_cmp, .-engine_pile_cmp
	.p2align 4
	.type	int_dall, @function
int_dall:
.LFB878:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	movq	8(%rsi), %rcx
	movq	16(%rdi), %rdx
	movq	(%rsi), %rax
	movl	(%rdi), %edi
	movq	%r8, %rsi
	jmp	*%rax
	.cfi_endproc
.LFE878:
	.size	int_dall, .-int_dall
	.p2align 4
	.type	int_unregister_cb, @function
int_unregister_cb:
.LFB872:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L7:
	movq	8(%rbx), %rdi
	call	OPENSSL_sk_delete@PLT
	movl	$0, 24(%rbx)
.L6:
	movq	8(%rbx), %rdi
	movq	%r12, %rsi
	call	OPENSSL_sk_find@PLT
	movl	%eax, %esi
	testl	%eax, %eax
	jns	.L7
	cmpq	%r12, 16(%rbx)
	je	.L10
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	engine_unlocked_finish@PLT
	movq	$0, 16(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE872:
	.size	int_unregister_cb, .-int_unregister_cb
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/engine/eng_table.c"
	.text
	.p2align 4
	.type	int_cleanup_cb_doall, @function
int_cleanup_cb_doall:
.LFB875:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L11
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	call	OPENSSL_sk_free@PLT
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L13
	xorl	%esi, %esi
	call	engine_unlocked_finish@PLT
.L13:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$177, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L11:
	ret
	.cfi_endproc
.LFE875:
	.size	int_cleanup_cb_doall, .-int_cleanup_cb_doall
	.p2align 4
	.globl	ENGINE_get_table_flags
	.type	ENGINE_get_table_flags, @function
ENGINE_get_table_flags:
.LFB866:
	.cfi_startproc
	endbr64
	movl	table_flags(%rip), %eax
	ret
	.cfi_endproc
.LFE866:
	.size	ENGINE_get_table_flags, .-ENGINE_get_table_flags
	.p2align 4
	.globl	ENGINE_set_table_flags
	.type	ENGINE_set_table_flags, @function
ENGINE_set_table_flags:
.LFB867:
	.cfi_startproc
	endbr64
	movl	%edi, table_flags(%rip)
	ret
	.cfi_endproc
.LFE867:
	.size	ENGINE_set_table_flags, .-ENGINE_set_table_flags
	.p2align 4
	.globl	engine_table_register
	.type	engine_table_register, @function
engine_table_register:
.LFB871:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$88, %rsp
	movq	%rdi, -104(%rbp)
	movq	global_engine_lock(%rip), %rdi
	movl	%r9d, -108(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	CRYPTO_THREAD_write_lock@PLT
	cmpq	$0, (%r15)
	je	.L57
.L24:
	testl	%r14d, %r14d
	je	.L25
	leal	-1(%r14), %eax
	leaq	-96(%rbp), %r14
	leaq	4(%rbx,%rax,4), %rax
	movq	%rax, -120(%rbp)
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L31:
	addq	$4, %rbx
	cmpq	%rbx, -120(%rbp)
	je	.L25
.L34:
	movl	(%rbx), %eax
	movq	%r14, %rsi
	movl	%eax, -96(%rbp)
	movq	-104(%rbp), %rax
	movq	(%rax), %rdi
	call	OPENSSL_LH_retrieve@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L58
.L26:
	movq	8(%r13), %rdi
	movq	%r12, %rsi
	call	OPENSSL_sk_delete_ptr@PLT
	movq	8(%r13), %rdi
	movq	%r12, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L56
	movl	-108(%rbp), %eax
	movl	$0, 24(%r13)
	testl	%eax, %eax
	je	.L31
	movq	%r12, %rdi
	call	engine_unlocked_init@PLT
	testl	%eax, %eax
	je	.L59
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.L33
	xorl	%esi, %esi
	call	engine_unlocked_finish@PLT
.L33:
	movq	%r12, 16(%r13)
	addq	$4, %rbx
	movl	$1, 24(%r13)
	cmpq	%rbx, -120(%rbp)
	jne	.L34
.L25:
	movl	$1, %r9d
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L58:
	movl	$100, %edx
	leaq	.LC0(%rip), %rsi
	movl	$32, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L56
	movl	$1, 24(%rax)
	movl	(%rbx), %eax
	movl	%eax, (%r15)
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 8(%r15)
	testq	%rax, %rax
	je	.L60
	movq	-104(%rbp), %r13
	movq	$0, 16(%r15)
	movq	%r15, %rsi
	movq	0(%r13), %rdi
	call	OPENSSL_LH_insert@PLT
	movq	0(%r13), %rdi
	movq	%r14, %rsi
	call	OPENSSL_LH_retrieve@PLT
	movq	%rax, %r13
	cmpq	%rax, %r15
	je	.L26
	movq	8(%r15), %rdi
	call	OPENSSL_sk_free@PLT
	movl	$114, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	call	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L56:
	xorl	%r9d, %r9d
.L28:
	movq	global_engine_lock(%rip), %rdi
	movl	%r9d, -104(%rbp)
	call	CRYPTO_THREAD_unlock@PLT
	movl	-104(%rbp), %r9d
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L61
	addq	$88, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	leaq	engine_pile_cmp(%rip), %rsi
	leaq	engine_pile_hash(%rip), %rdi
	call	OPENSSL_LH_new@PLT
	testq	%rax, %rax
	je	.L56
	movq	-104(%rbp), %rcx
	movq	%r13, %rdi
	movq	%rax, (%rcx)
	call	engine_cleanup_add_first@PLT
	jmp	.L24
.L59:
	movl	$129, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$109, %edx
	movl	%eax, -104(%rbp)
	movl	$184, %esi
	movl	$38, %edi
	call	ERR_put_error@PLT
	movl	-104(%rbp), %r9d
	jmp	.L28
.L60:
	movl	$107, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	call	CRYPTO_free@PLT
	xorl	%r9d, %r9d
	jmp	.L28
.L61:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE871:
	.size	engine_table_register, .-engine_table_register
	.p2align 4
	.globl	engine_table_unregister
	.type	engine_table_unregister, @function
engine_table_unregister:
.LFB874:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	global_engine_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L63
	movq	%r12, %rdx
	leaq	int_unregister_cb(%rip), %rsi
	call	OPENSSL_LH_doall_arg@PLT
.L63:
	movq	global_engine_lock(%rip), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_THREAD_unlock@PLT
	.cfi_endproc
.LFE874:
	.size	engine_table_unregister, .-engine_table_unregister
	.p2align 4
	.globl	engine_table_cleanup
	.type	engine_table_cleanup, @function
engine_table_cleanup:
.LFB876:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	global_engine_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L69
	leaq	int_cleanup_cb_doall(%rip), %rsi
	call	OPENSSL_LH_doall@PLT
	movq	(%rbx), %rdi
	call	OPENSSL_LH_free@PLT
	movq	$0, (%rbx)
.L69:
	movq	global_engine_lock(%rip), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_THREAD_unlock@PLT
	.cfi_endproc
.LFE876:
	.size	engine_table_cleanup, .-engine_table_cleanup
	.p2align 4
	.globl	engine_table_select
	.type	engine_table_select, @function
engine_table_select:
.LFB877:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L74
	movq	%rdi, %rbx
	movl	%esi, %r13d
	call	ERR_set_mark@PLT
	movq	global_engine_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L76
	leaq	-80(%rbp), %rsi
	movl	%r13d, -80(%rbp)
	call	OPENSSL_LH_retrieve@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L76
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.L77
	call	engine_unlocked_init@PLT
	testl	%eax, %eax
	jne	.L116
.L77:
	movl	24(%rbx), %esi
	testl	%esi, %esi
	je	.L79
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L118:
	movl	160(%rax), %eax
	testl	%eax, %eax
	jg	.L80
	testb	$1, table_flags(%rip)
	jne	.L81
.L80:
	movq	%r12, %rdi
	call	engine_unlocked_init@PLT
	testl	%eax, %eax
	jne	.L117
.L81:
	movl	%r13d, %esi
.L79:
	movq	8(%rbx), %rdi
	leal	1(%rsi), %r13d
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L118
.L78:
	movl	$1, 24(%rbx)
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L76:
	xorl	%r12d, %r12d
.L83:
	movq	global_engine_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	call	ERR_pop_to_mark@PLT
.L74:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L119
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
	movl	$1, 24(%rbx)
	movq	16(%rbx), %r12
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L117:
	cmpq	%r12, 16(%rbx)
	je	.L78
	movq	%r12, %rdi
	call	engine_unlocked_init@PLT
	testl	%eax, %eax
	je	.L78
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L82
	xorl	%esi, %esi
	call	engine_unlocked_finish@PLT
.L82:
	movq	%r12, 16(%rbx)
	jmp	.L78
.L119:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE877:
	.size	engine_table_select, .-engine_table_select
	.p2align 4
	.globl	engine_table_doall
	.type	engine_table_doall, @function
engine_table_doall:
.LFB880:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -32(%rbp)
	movq	%rdx, -24(%rbp)
	testq	%rdi, %rdi
	je	.L120
	leaq	-32(%rbp), %rdx
	leaq	int_dall(%rip), %rsi
	call	OPENSSL_LH_doall_arg@PLT
.L120:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L127
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L127:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE880:
	.size	engine_table_doall, .-engine_table_doall
	.local	table_flags
	.comm	table_flags,4,4
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
