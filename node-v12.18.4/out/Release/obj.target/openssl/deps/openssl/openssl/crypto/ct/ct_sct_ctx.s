	.file	"ct_sct_ctx.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ct/ct_sct_ctx.c"
	.text
	.p2align 4
	.globl	SCT_CTX_new
	.type	SCT_CTX_new, @function
SCT_CTX_new:
.LFB1307:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$25, %edx
	movl	$80, %edi
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L5
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	movl	$28, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$126, %esi
	movl	$50, %edi
	movq	%rax, -8(%rbp)
	call	ERR_put_error@PLT
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1307:
	.size	SCT_CTX_new, .-SCT_CTX_new
	.p2align 4
	.globl	SCT_CTX_free
	.type	SCT_CTX_free, @function
SCT_CTX_free:
.LFB1308:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L6
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	EVP_PKEY_free@PLT
	movq	8(%r12), %rdi
	movl	$38, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	24(%r12), %rdi
	movl	$39, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	40(%r12), %rdi
	movl	$40, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	56(%r12), %rdi
	movl	$41, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$42, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L6:
	ret
	.cfi_endproc
.LFE1308:
	.size	SCT_CTX_free, .-SCT_CTX_free
	.p2align 4
	.globl	SCT_CTX_set1_cert
	.type	SCT_CTX_set1_cert, @function
SCT_CTX_set1_cert:
.LFB1311:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	movl	$-1, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$952, %esi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	call	X509_get_ext_by_NID@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jns	.L12
	cmpl	$-1, %eax
	jne	.L16
	testq	%r14, %r14
	je	.L57
.L15:
	xorl	%r13d, %r13d
.L21:
	movq	-72(%rbp), %rdi
	movl	$188, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-64(%rbp), %rdi
	movl	$189, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r13, %rdi
	call	X509_free@PLT
	xorl	%eax, %eax
.L11:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L58
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	movl	%eax, %edx
	movl	$952, %esi
	movq	%r13, %rdi
	call	X509_get_ext_by_NID@PLT
	testl	%eax, %eax
	jns	.L15
.L16:
	xorl	%r15d, %r15d
.L14:
	movl	$-1, %edx
	movl	$951, %esi
	movq	%r13, %rdi
	call	X509_get_ext_by_NID@PLT
	movl	%eax, %edx
	testl	%eax, %eax
	jns	.L59
	cmpl	$-1, %eax
	jne	.L35
	testl	%r12d, %r12d
	js	.L35
.L18:
	movq	%r13, %rdi
	call	X509_dup@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L15
	movq	%rax, %rdi
	movl	%r12d, %esi
	call	X509_delete_ext@PLT
	movq	%rax, %rdi
	call	X509_EXTENSION_free@PLT
	testq	%r14, %r14
	je	.L29
	movl	$-1, %edx
	movl	$90, %esi
	movq	%r14, %rdi
	call	X509_get_ext_by_NID@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jns	.L60
	movl	$-1, %edx
	movl	$90, %esi
	movq	%r13, %rdi
	call	X509_get_ext_by_NID@PLT
	testl	%eax, %eax
	jns	.L61
	cmpl	$-1, %eax
	jl	.L21
	cmpl	$-1, %r12d
	jne	.L21
	movq	%r14, %rdi
	call	X509_get_issuer_name@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	X509_set_issuer_name@PLT
	testl	%eax, %eax
	je	.L21
.L29:
	leaq	-64(%rbp), %rsi
	movq	%r13, %rdi
	call	i2d_re_X509_tbs@PLT
	movslq	%eax, %r12
	testl	%eax, %eax
	jg	.L19
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L35:
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
.L19:
	movq	%r13, %rdi
	call	X509_free@PLT
	movq	40(%rbx), %rdi
	movl	$178, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-72(%rbp), %rax
	movq	%r15, 48(%rbx)
	movl	$182, %edx
	movq	56(%rbx), %rdi
	leaq	.LC0(%rip), %rsi
	movq	%rax, 40(%rbx)
	call	CRYPTO_free@PLT
	movq	-64(%rbp), %rax
	movq	%r12, 64(%rbx)
	movq	%rax, 56(%rbx)
	movl	$1, %eax
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L59:
	movl	$951, %esi
	movq	%r13, %rdi
	movl	%eax, -84(%rbp)
	call	X509_get_ext_by_NID@PLT
	testl	%eax, %eax
	jns	.L15
	testl	%r12d, %r12d
	movl	-84(%rbp), %edx
	jns	.L15
	movl	%edx, %r12d
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L57:
	leaq	-72(%rbp), %rsi
	movq	%r13, %rdi
	call	i2d_X509@PLT
	movslq	%eax, %r15
	testl	%r15d, %r15d
	jns	.L14
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L60:
	movl	%eax, %edx
	movl	$90, %esi
	movq	%r14, %rdi
	call	X509_get_ext_by_NID@PLT
	movl	$-1, %edx
	movl	$90, %esi
	movq	%r13, %rdi
	movl	%eax, -84(%rbp)
	call	X509_get_ext_by_NID@PLT
	testl	%eax, %eax
	js	.L21
	movl	%eax, %edx
	movl	$90, %esi
	movq	%r13, %rdi
	movl	%eax, -88(%rbp)
	call	X509_get_ext_by_NID@PLT
	testl	%eax, %eax
	jns	.L21
	movl	-84(%rbp), %eax
	movl	-88(%rbp), %r9d
	testl	%eax, %eax
	jns	.L21
	movq	%r14, %rdi
	movl	%r9d, -84(%rbp)
	call	X509_get_issuer_name@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	X509_set_issuer_name@PLT
	movl	-84(%rbp), %r9d
	testl	%eax, %eax
	je	.L21
	movl	%r12d, %esi
	movq	%r14, %rdi
	movl	%r9d, -84(%rbp)
	call	X509_get_ext@PLT
	movl	-84(%rbp), %r9d
	movq	%r13, %rdi
	movq	%rax, %r14
	movl	%r9d, %esi
	call	X509_get_ext@PLT
	movq	%rax, %r12
	testq	%r14, %r14
	je	.L21
	testq	%rax, %rax
	je	.L21
	movq	%r14, %rdi
	call	X509_EXTENSION_get_data@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L21
	movq	%r12, %rdi
	call	X509_EXTENSION_set_data@PLT
	testl	%eax, %eax
	jne	.L29
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L61:
	movl	%eax, %edx
	movl	$90, %esi
	movq	%r13, %rdi
	call	X509_get_ext_by_NID@PLT
	jmp	.L21
.L58:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1311:
	.size	SCT_CTX_set1_cert, .-SCT_CTX_set1_cert
	.p2align 4
	.globl	SCT_CTX_set1_issuer
	.type	SCT_CTX_set1_issuer, @function
SCT_CTX_set1_issuer:
.LFB1313:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	X509_get_X509_PUBKEY@PLT
	movq	24(%rbx), %r12
	movq	$0, -48(%rbp)
	movq	%rax, %r13
	testq	%r12, %r12
	je	.L63
	cmpq	$31, 32(%rbx)
	jbe	.L63
.L64:
	movq	%r13, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%r14d, %r14d
	call	i2d_X509_PUBKEY@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jle	.L65
	call	EVP_sha256@PLT
	movq	-48(%rbp), %rdi
	leaq	-52(%rbp), %rcx
	movslq	%r13d, %rsi
	movq	%rax, %r8
	xorl	%r9d, %r9d
	movq	%r12, %rdx
	call	EVP_Digest@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L65
	movq	24(%rbx), %rdi
	cmpq	%rdi, %r12
	je	.L69
	movl	$220, %edx
	leaq	.LC0(%rip), %rsi
	movl	$1, %r14d
	call	CRYPTO_free@PLT
	movq	$32, 32(%rbx)
	movq	%r12, 24(%rbx)
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L65:
	movl	$228, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	movq	-48(%rbp), %rdi
	movl	$229, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L77
	addq	$32, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	movl	$206, %edx
	leaq	.LC0(%rip), %rsi
	movl	$32, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L64
	xorl	%r14d, %r14d
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L69:
	xorl	%r12d, %r12d
	movl	$1, %r14d
	jmp	.L65
.L77:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1313:
	.size	SCT_CTX_set1_issuer, .-SCT_CTX_set1_issuer
	.p2align 4
	.globl	SCT_CTX_set1_issuer_pubkey
	.type	SCT_CTX_set1_issuer_pubkey, @function
SCT_CTX_set1_issuer_pubkey:
.LFB1314:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	24(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	testq	%r12, %r12
	je	.L79
	cmpq	$31, 32(%rdi)
	jbe	.L79
.L80:
	movq	%r13, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%r14d, %r14d
	call	i2d_X509_PUBKEY@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jle	.L81
	call	EVP_sha256@PLT
	movq	-48(%rbp), %rdi
	leaq	-52(%rbp), %rcx
	movslq	%r13d, %rsi
	movq	%rax, %r8
	xorl	%r9d, %r9d
	movq	%r12, %rdx
	call	EVP_Digest@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L81
	movq	24(%rbx), %rdi
	cmpq	%rdi, %r12
	je	.L85
	movl	$220, %edx
	leaq	.LC0(%rip), %rsi
	movl	$1, %r14d
	call	CRYPTO_free@PLT
	movq	$32, 32(%rbx)
	movq	%r12, 24(%rbx)
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L81:
	movl	$228, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	movq	-48(%rbp), %rdi
	movl	$229, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L93
	addq	$32, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	movl	$206, %edx
	leaq	.LC0(%rip), %rsi
	movl	$32, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L80
	xorl	%r14d, %r14d
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L85:
	xorl	%r12d, %r12d
	movl	$1, %r14d
	jmp	.L81
.L93:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1314:
	.size	SCT_CTX_set1_issuer_pubkey, .-SCT_CTX_set1_issuer_pubkey
	.p2align 4
	.globl	SCT_CTX_set1_pubkey
	.type	SCT_CTX_set1_pubkey, @function
SCT_CTX_set1_pubkey:
.LFB1315:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	X509_PUBKEY_get@PLT
	movq	%rax, %r12
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L94
	movq	8(%rbx), %r14
	movq	$0, -48(%rbp)
	testq	%r14, %r14
	je	.L96
	cmpq	$31, 16(%rbx)
	jbe	.L96
.L97:
	movq	%r13, %rdi
	leaq	-48(%rbp), %rsi
	call	i2d_X509_PUBKEY@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jle	.L98
	call	EVP_sha256@PLT
	movq	-48(%rbp), %rdi
	xorl	%r9d, %r9d
	leaq	-52(%rbp), %rcx
	movq	%rax, %r8
	movslq	%r13d, %rsi
	movq	%r14, %rdx
	call	EVP_Digest@PLT
	testl	%eax, %eax
	je	.L98
	movq	8(%rbx), %rdi
	cmpq	%rdi, %r14
	je	.L99
	movl	$220, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r14, 8(%rbx)
	movq	$32, 16(%rbx)
.L99:
	movl	$228, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	CRYPTO_free@PLT
	movq	-48(%rbp), %rdi
	movl	$229, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	(%rbx), %rdi
	call	EVP_PKEY_free@PLT
	movq	%r12, (%rbx)
	movl	$1, %eax
.L94:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L113
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	movl	$206, %edx
	leaq	.LC0(%rip), %rsi
	movl	$32, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L97
	.p2align 4,,10
	.p2align 3
.L98:
	movl	$228, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	movq	-48(%rbp), %rdi
	movl	$229, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	call	EVP_PKEY_free@PLT
	xorl	%eax, %eax
	jmp	.L94
.L113:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1315:
	.size	SCT_CTX_set1_pubkey, .-SCT_CTX_set1_pubkey
	.p2align 4
	.globl	SCT_CTX_set_time
	.type	SCT_CTX_set_time, @function
SCT_CTX_set_time:
.LFB1316:
	.cfi_startproc
	endbr64
	movq	%rsi, 72(%rdi)
	ret
	.cfi_endproc
.LFE1316:
	.size	SCT_CTX_set_time, .-SCT_CTX_set_time
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
