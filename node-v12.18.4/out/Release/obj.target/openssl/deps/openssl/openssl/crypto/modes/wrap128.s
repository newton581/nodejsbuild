	.file	"wrap128.c"
	.text
	.p2align 4
	.type	crypto_128_unwrap_raw, @function
crypto_128_unwrap_raw:
.LFB252:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-8(%r8), %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -144(%rbp)
	movq	%rdx, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r8, %rax
	movq	%r10, -136(%rbp)
	andl	$7, %eax
	jne	.L10
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	leaq	-24(%r8), %rdx
	movq	%r8, %r14
	cmpq	$2147483632, %rdx
	jbe	.L17
.L1:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L18
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movq	%r10, %rax
	leaq	8(%rcx), %rsi
	movq	%r10, %rdx
	movq	%r9, %r12
	shrq	$3, %rax
	leaq	(%rax,%rax,2), %r15
	leaq	(%r15,%r15), %rax
	movq	%r10, %r15
	movq	%rax, -96(%rbp)
	movq	(%rcx), %rax
	movq	%rax, -80(%rbp)
	call	memmove@PLT
	leaq	-16(%r14), %rax
	movq	%rax, -120(%rbp)
	testq	%r15, %r15
	je	.L3
	subq	$9, %r14
	movq	$6, -128(%rbp)
	shrq	$3, %r14
	notq	%r14
	movq	%r14, -104(%rbp)
	leaq	-80(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L8:
	movq	-96(%rbp), %r15
	movq	-104(%rbp), %rax
	movq	-120(%rbp), %rcx
	addq	%r15, %rax
	movq	%rax, -88(%rbp)
	movq	%r15, %rax
	negq	%rax
	leaq	(%rcx,%rax,8), %rcx
	movq	-112(%rbp), %rax
	leaq	(%rax,%rcx), %r13
	.p2align 4,,10
	.p2align 3
.L7:
	xorb	%r15b, -73(%rbp)
	cmpq	$255, %r15
	jbe	.L4
	movq	%r15, %rax
	shrq	$8, %rax
	xorb	%al, -74(%rbp)
	movq	%r15, %rax
	shrq	$16, %rax
	xorb	%al, -75(%rbp)
	movq	%r15, %rax
	shrq	$24, %rax
	xorb	%al, -76(%rbp)
.L4:
	movq	0(%r13,%r15,8), %rax
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	movq	%rax, -72(%rbp)
	call	*%r12
	movq	-72(%rbp), %rax
	movq	%rax, 0(%r13,%r15,8)
	subq	$1, %r15
	cmpq	-88(%rbp), %r15
	jne	.L7
	movq	-104(%rbp), %rcx
	addq	%rcx, -96(%rbp)
	subq	$1, -128(%rbp)
	jne	.L8
.L3:
	movq	-80(%rbp), %rax
	movq	-144(%rbp), %rcx
	movq	%rax, (%rcx)
	movq	-136(%rbp), %rax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%eax, %eax
	jmp	.L1
.L18:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE252:
	.size	crypto_128_unwrap_raw, .-crypto_128_unwrap_raw
	.p2align 4
	.type	CRYPTO_128_wrap.part.0, @function
CRYPTO_128_wrap.part.0:
.LFB256:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movq	%r8, %rdx
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	leaq	8(%r15), %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rcx, %rsi
	subq	$72, %rsp
	movq	%r8, -88(%rbp)
	movq	%r9, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	memmove@PLT
	movabsq	$-6438275382588823898, %rax
	testq	%rbx, %rbx
	je	.L20
	movq	(%rbx), %rax
.L20:
	cmpq	$0, -88(%rbp)
	movq	%rax, -80(%rbp)
	movl	$1, %ebx
	leaq	-80(%rbp), %r12
	movq	$6, -104(%rbp)
	je	.L22
	.p2align 4,,10
	.p2align 3
.L21:
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L26:
	movq	8(%r15,%r14), %rdx
	movq	-96(%rbp), %rax
	movq	%r12, %rsi
	movq	%r12, %rdi
	movq	%rdx, -72(%rbp)
	movq	%r13, %rdx
	call	*%rax
	xorb	%bl, -73(%rbp)
	cmpq	$255, %rbx
	jbe	.L23
	movq	%rbx, %rdx
	shrq	$8, %rdx
	xorb	%dl, -74(%rbp)
	movq	%rbx, %rdx
	shrq	$16, %rdx
	xorb	%dl, -75(%rbp)
	movq	%rbx, %rdx
	shrq	$24, %rdx
	xorb	%dl, -76(%rbp)
.L23:
	movq	-72(%rbp), %rdx
	addq	$1, %rbx
	movq	%rdx, 8(%r15,%r14)
	addq	$8, %r14
	cmpq	%r14, -88(%rbp)
	ja	.L26
	subq	$1, -104(%rbp)
	jne	.L21
.L22:
	movq	-80(%rbp), %rax
	movq	%rax, (%r15)
	movq	-88(%rbp), %rax
	addq	$8, %rax
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L37
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L37:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE256:
	.size	CRYPTO_128_wrap.part.0, .-CRYPTO_128_wrap.part.0
	.p2align 4
	.globl	CRYPTO_128_wrap
	.type	CRYPTO_128_wrap, @function
CRYPTO_128_wrap:
.LFB251:
	.cfi_startproc
	endbr64
	testb	$7, %r8b
	jne	.L38
	leaq	-16(%r8), %rax
	cmpq	$2147483632, %rax
	jbe	.L41
.L38:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	jmp	CRYPTO_128_wrap.part.0
	.cfi_endproc
.LFE251:
	.size	CRYPTO_128_wrap, .-CRYPTO_128_wrap
	.p2align 4
	.globl	CRYPTO_128_unwrap
	.type	CRYPTO_128_unwrap, @function
CRYPTO_128_unwrap:
.LFB253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-48(%rbp), %r15
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%r15, %rsi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	crypto_128_unwrap_raw
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L42
	testq	%r12, %r12
	leaq	default_iv(%rip), %rax
	movl	$8, %edx
	movq	%r15, %rdi
	cmove	%rax, %r12
	movq	%r12, %rsi
	call	CRYPTO_memcmp@PLT
	testl	%eax, %eax
	jne	.L53
.L42:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L54
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r14, %rdi
	xorl	%r13d, %r13d
	call	OPENSSL_cleanse@PLT
	jmp	.L42
.L54:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE253:
	.size	CRYPTO_128_unwrap, .-CRYPTO_128_unwrap
	.p2align 4
	.globl	CRYPTO_128_wrap_pad
	.type	CRYPTO_128_wrap_pad, @function
CRYPTO_128_wrap_pad:
.LFB254:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	leaq	-1(%r8), %rdx
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$2147483646, %rdx
	ja	.L55
	movq	%rdi, %r14
	movq	%r8, %rbx
	movq	%r9, %r15
	testq	%rsi, %rsi
	je	.L66
	movl	(%rsi), %eax
	movl	%eax, -64(%rbp)
.L58:
	leaq	7(%rbx), %r13
	movl	%ebx, %eax
	andq	$-8, %r13
	bswap	%eax
	movl	%eax, -60(%rbp)
	movq	%r13, %r8
	subq	%rbx, %r8
	movq	%r8, -72(%rbp)
	cmpq	$8, %r13
	je	.L67
	movq	%rcx, %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	memmove@PLT
	movq	-72(%rbp), %r8
	leaq	(%r12,%rbx), %rdi
	xorl	%esi, %esi
	movq	%r8, %rdx
	call	memset@PLT
	leaq	-16(%r13), %rdx
	xorl	%eax, %eax
	cmpq	$2147483632, %rdx
	ja	.L55
	leaq	-64(%rbp), %rsi
	movq	%r15, %r9
	movq	%r13, %r8
	movq	%r12, %rcx
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	CRYPTO_128_wrap.part.0
	cltq
.L55:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L68
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	movq	%rcx, %rsi
	leaq	8(%r12), %rdi
	movq	%rbx, %rdx
	call	memmove@PLT
	movq	-64(%rbp), %rax
	movq	-72(%rbp), %r8
	xorl	%esi, %esi
	leaq	8(%r12,%rbx), %rdi
	movq	%rax, (%r12)
	movq	%r8, %rdx
	call	memset@PLT
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%r15
	movl	$16, %eax
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L66:
	movl	$-1504093786, -64(%rbp)
	jmp	.L58
.L68:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE254:
	.size	CRYPTO_128_wrap_pad, .-CRYPTO_128_wrap_pad
	.p2align 4
	.globl	CRYPTO_128_unwrap_pad
	.type	CRYPTO_128_unwrap_pad, @function
CRYPTO_128_unwrap_pad:
.LFB255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	andl	$7, %r15d
	jne	.L78
	leaq	-16(%r8), %rax
	movq	%r8, %r12
	cmpq	$2147483631, %rax
	ja	.L69
	movq	%rsi, %r14
	movq	%rdx, %r13
	cmpq	$16, %r8
	jne	.L71
	leaq	-80(%rbp), %rbx
	movq	%rdi, %rdx
	movq	%rcx, %rdi
	movq	%rbx, %rsi
	call	*%r9
	movq	-80(%rbp), %rax
	movq	%rbx, %rdi
	movl	$16, %esi
	leaq	-88(%rbp), %rbx
	movq	%rax, -88(%rbp)
	movq	-72(%rbp), %rax
	movq	%rax, 0(%r13)
	call	OPENSSL_cleanse@PLT
	movl	$8, %ecx
.L72:
	movq	%rcx, -104(%rbp)
	movl	$4, %edx
	leaq	default_aiv(%rip), %rsi
	testq	%r14, %r14
	je	.L83
	movq	%r14, %rsi
.L83:
	movq	%rbx, %rdi
	call	CRYPTO_memcmp@PLT
	movq	-104(%rbp), %rcx
	testl	%eax, %eax
	jne	.L75
	movl	-84(%rbp), %ebx
	movq	%r12, %rax
	shrq	$3, %rax
	bswap	%ebx
	leaq	-16(,%rax,8), %rax
	movl	%ebx, %ebx
	cmpq	%rbx, %rax
	jnb	.L75
	addq	$8, %rax
	cmpq	%rbx, %rax
	jnb	.L84
.L75:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	OPENSSL_cleanse@PLT
.L69:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L85
	addq	$72, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	xorl	%r15d, %r15d
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L71:
	leaq	-88(%rbp), %rbx
	movq	%rbx, %rsi
	call	crypto_128_unwrap_raw
	movq	%rax, %rcx
	leaq	-8(%r12), %rax
	cmpq	%rcx, %rax
	je	.L72
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L84:
	movq	%rcx, %rdx
	leaq	0(%r13,%rbx), %rdi
	leaq	zeros.6557(%rip), %rsi
	subq	%rbx, %rdx
	call	CRYPTO_memcmp@PLT
	testl	%eax, %eax
	jne	.L75
	movq	%rbx, %r15
	jmp	.L69
.L85:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE255:
	.size	CRYPTO_128_unwrap_pad, .-CRYPTO_128_unwrap_pad
	.local	zeros.6557
	.comm	zeros.6557,8,8
	.section	.rodata
	.type	default_aiv, @object
	.size	default_aiv, 4
default_aiv:
	.ascii	"\246YY\246"
	.align 8
	.type	default_iv, @object
	.size	default_iv, 8
default_iv:
	.ascii	"\246\246\246\246\246\246\246\246"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
