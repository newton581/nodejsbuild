	.file	"obj_lib.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/objects/obj_lib.c"
	.text
	.p2align 4
	.globl	OBJ_dup
	.type	OBJ_dup, @function
OBJ_dup:
.LFB443:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	testq	%rdi, %rdi
	je	.L2
	testb	$1, 32(%rdi)
	jne	.L25
.L2:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	call	ASN1_OBJECT_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L26
	movl	32(%r12), %eax
	movslq	20(%r12), %rsi
	orl	$13, %eax
	movl	%eax, 32(%r13)
	testl	%esi, %esi
	jg	.L27
.L4:
	movl	16(%r12), %eax
	movq	8(%r12), %rdi
	movl	%esi, 20(%r13)
	movl	%eax, 16(%r13)
	testq	%rdi, %rdi
	je	.L9
	movl	$44, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, 8(%r13)
	testq	%rax, %rax
	je	.L8
.L9:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L23
	movl	$47, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	je	.L8
.L23:
	movq	%r13, %r12
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	call	ASN1_OBJECT_free@PLT
	movl	$53, %r8d
	movl	$65, %edx
	leaq	.LC0(%rip), %rcx
	movl	$101, %esi
	movl	$8, %edi
	call	ERR_put_error@PLT
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L27:
	movq	24(%r12), %rdi
	movl	$38, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_memdup@PLT
	movq	%rax, 24(%r13)
	testq	%rax, %rax
	je	.L8
	movl	20(%r12), %esi
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L26:
	movl	$28, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$13, %edx
	xorl	%r12d, %r12d
	movl	$101, %esi
	movl	$8, %edi
	call	ERR_put_error@PLT
	jmp	.L2
	.cfi_endproc
.LFE443:
	.size	OBJ_dup, .-OBJ_dup
	.p2align 4
	.globl	OBJ_cmp
	.type	OBJ_cmp, @function
OBJ_cmp:
.LFB444:
	.cfi_startproc
	endbr64
	movslq	20(%rdi), %rdx
	movl	%edx, %eax
	subl	20(%rsi), %eax
	jne	.L28
	movq	24(%rsi), %rsi
	movq	24(%rdi), %rdi
	jmp	memcmp@PLT
	.p2align 4,,10
	.p2align 3
.L28:
	ret
	.cfi_endproc
.LFE444:
	.size	OBJ_cmp, .-OBJ_cmp
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
