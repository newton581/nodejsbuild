	.file	"conf_def.c"
	.text
	.p2align 4
	.type	def_init_default, @function
def_init_default:
.LFB345:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L3
	leaq	CONF_type_default(%rip), %rax
	leaq	default_method(%rip), %rdx
	movq	$0, 16(%rdi)
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movl	$1, %eax
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE345:
	.size	def_init_default, .-def_init_default
	.p2align 4
	.type	def_init_WIN32, @function
def_init_WIN32:
.LFB346:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L7
	leaq	CONF_type_win32(%rip), %rax
	leaq	WIN32_method(%rip), %rdx
	movq	$0, 16(%rdi)
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movl	$1, %eax
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE346:
	.size	def_init_WIN32, .-def_init_WIN32
	.p2align 4
	.type	def_is_number, @function
def_is_number:
.LFB364:
	.cfi_startproc
	endbr64
	movsbq	%sil, %rdx
	xorl	%eax, %eax
	testb	%sil, %sil
	js	.L8
	movq	8(%rdi), %rax
	movzwl	(%rax,%rdx,2), %eax
	andl	$1, %eax
.L8:
	ret
	.cfi_endproc
.LFE364:
	.size	def_is_number, .-def_is_number
	.p2align 4
	.type	def_to_int, @function
def_to_int:
.LFB365:
	.cfi_startproc
	endbr64
	movsbl	%sil, %eax
	subl	$48, %eax
	ret
	.cfi_endproc
.LFE365:
	.size	def_to_int, .-def_to_int
	.p2align 4
	.type	def_dump, @function
def_dump:
.LFB363:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	16(%rdi), %rdi
	movq	%rsi, %rdx
	leaq	dump_value_doall_arg(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	OPENSSL_LH_doall_arg@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE363:
	.size	def_dump, .-def_dump
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/conf/conf_def.c"
	.text
	.p2align 4
	.type	def_create, @function
def_create:
.LFB344:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$106, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$24, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L14
	movq	%rax, %rdi
	call	*16(%rbx)
	testl	%eax, %eax
	je	.L20
.L14:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$109, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%r12d, %r12d
	call	CRYPTO_free@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE344:
	.size	def_create, .-def_create
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	".conf"
.LC2:
	.string	".cnf"
.LC3:
	.string	"/"
.LC4:
	.string	"r"
	.text
	.p2align 4
	.type	get_next_file, @function
get_next_file:
.LFB354:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	call	strlen@PLT
	addq	$2, %rax
	movq	%rax, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L22:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	OPENSSL_DIR_read@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L28
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rax, %r15
	cmpq	$5, %rax
	jbe	.L23
	leaq	-5(%r14,%rax), %rdi
	leaq	.LC1(%rip), %rsi
	call	strcasecmp@PLT
	testl	%eax, %eax
	je	.L24
.L25:
	leaq	-4(%r14,%r15), %rdi
	leaq	.LC2(%rip), %rsi
	call	strcasecmp@PLT
	testl	%eax, %eax
	jne	.L22
.L24:
	addq	-56(%rbp), %r15
	movl	$722, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L39
	cmpb	$0, (%rax)
	je	.L40
.L29:
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	OPENSSL_strlcat@PLT
	leaq	.LC4(%rip), %rsi
	movq	%rbx, %rdi
	call	BIO_new_file@PLT
	movl	$746, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	CRYPTO_free@PLT
	testq	%r15, %r15
	je	.L22
.L21:
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	jne	.L22
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L40:
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	OPENSSL_strlcpy@PLT
	movq	%r15, %rdx
	leaq	.LC3(%rip), %rsi
	movq	%rbx, %rdi
	call	OPENSSL_strlcat@PLT
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L39:
	movl	$724, %r8d
	movl	$65, %edx
	movl	$107, %esi
	movl	$14, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L28:
	movq	%r13, %rdi
	xorl	%r15d, %r15d
	call	OPENSSL_DIR_end@PLT
	movq	$0, 0(%r13)
	jmp	.L21
	.cfi_endproc
.LFE354:
	.size	get_next_file, .-get_next_file
	.p2align 4
	.type	str_copy, @function
str_copy:
.LFB352:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -80(%rbp)
	movq	%rdx, -88(%rbp)
	call	BUF_MEM_new@PLT
	movq	%rax, %r15
	xorl	%eax, %eax
	testq	%r15, %r15
	je	.L41
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%r15, %rdi
	leal	1(%rax), %esi
	movslq	%esi, %rsi
	call	BUF_MEM_grow@PLT
	testq	%rax, %rax
	je	.L43
	movzbl	(%r12), %edi
	movq	8(%r14), %rdx
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L44:
	testb	%dil, %dil
	js	.L45
.L130:
	movsbq	%dil, %rax
	movzwl	(%rdx,%rax,2), %eax
	testb	$64, %al
	jne	.L128
	testb	$4, %ah
	jne	.L129
	testb	$32, %al
	je	.L61
	movzbl	1(%r12), %eax
	leaq	2(%r12), %rcx
	testb	%al, %al
	js	.L62
	movsbq	%al, %rsi
	testb	$8, (%rdx,%rsi,2)
	jne	.L63
	cmpb	$114, %al
	je	.L101
	cmpb	$110, %al
	jne	.L62
	movl	$10, %eax
.L64:
	movq	8(%r15), %rsi
	movslq	%ebx, %rdx
	addl	$1, %ebx
	movb	%al, (%rsi,%rdx)
	movzbl	2(%r12), %edi
	movq	%rcx, %r12
	movq	8(%r14), %rdx
	testb	%dil, %dil
	jns	.L130
.L45:
	leaq	1(%r12), %r10
.L65:
	movq	8(%r15), %rdx
	movslq	%ebx, %rax
	addl	$1, %ebx
	movb	%dil, (%rdx,%rax)
	movzbl	1(%r12), %edi
	movq	%r10, %r12
	movq	8(%r14), %rdx
	jmp	.L44
.L63:
	movq	8(%r15), %rax
	movslq	%ebx, %rbx
	movl	$654, %edx
	leaq	.LC0(%rip), %rsi
	movb	$0, (%rax,%rbx)
	movq	-88(%rbp), %rbx
	movq	(%rbx), %rdi
	call	CRYPTO_free@PLT
	movq	8(%r15), %rax
	movl	$656, %edx
	movq	%r15, %rdi
	leaq	.LC0(%rip), %rsi
	movq	%rax, (%rbx)
	call	CRYPTO_free@PLT
	movl	$1, %eax
.L41:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L133:
	.cfi_restore_state
	cmpb	$58, %r13b
	jne	.L126
	cmpb	$58, 1(%rcx)
	je	.L78
.L126:
	movsbl	(%rcx), %r9d
	movb	$0, (%rcx)
	movl	%r9d, %r13d
	testl	%esi, %esi
	je	.L69
.L72:
	cmpl	%esi, %r9d
	je	.L131
.L92:
	movl	$602, %r8d
	movl	$102, %edx
	movl	$101, %esi
	movl	$14, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L43:
	movq	%r15, %rdi
	call	BUF_MEM_free@PLT
	addq	$56, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	testb	$8, %al
	jne	.L63
	leaq	1(%r12), %r10
	cmpb	$36, %dil
	jne	.L65
	movzbl	1(%r12), %r13d
	cmpb	$123, %r13b
	je	.L66
	cmpb	$40, %r13b
	je	.L67
	movsbl	%r13b, %r9d
	xorl	%esi, %esi
	testb	%r13b, %r13b
	js	.L132
.L71:
	movq	%r10, %rcx
.L74:
	movzbl	%r13b, %eax
	testw	$263, (%rdx,%rax,2)
	je	.L133
	movsbl	1(%rcx), %r9d
	addq	$1, %rcx
	movl	%r9d, %r13d
	testb	%r9b, %r9b
	jns	.L74
	movb	$0, (%rcx)
	testl	%esi, %esi
	jne	.L72
.L69:
	movq	-80(%rbp), %rsi
	movq	%r10, %rdx
	movq	%r14, %rdi
	movq	%rcx, -56(%rbp)
	call	_CONF_get_string@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %rdx
	movq	%rcx, -64(%rbp)
.L91:
	movq	-56(%rbp), %rax
	movb	%r13b, (%rax)
	testq	%rdx, %rdx
	je	.L134
	movq	%rdx, %rdi
	movq	%rdx, -72(%rbp)
	call	strlen@PLT
	movq	(%r15), %rsi
	subq	-64(%rbp), %r12
	movq	-72(%rbp), %rdx
	addq	%r12, %rsi
	addq	%rax, %rsi
	cmpq	$65536, %rsi
	ja	.L135
	movq	%r15, %rdi
	movq	%rdx, -72(%rbp)
	call	BUF_MEM_grow_clean@PLT
	testq	%rax, %rax
	je	.L84
	movq	-72(%rbp), %rdx
	movslq	%ebx, %rax
	movzbl	(%rdx), %esi
	subq	%rax, %rdx
	testb	%sil, %sil
	je	.L86
	.p2align 4,,10
	.p2align 3
.L87:
	movq	8(%r15), %rcx
	leal	1(%rax), %ebx
	movb	%sil, (%rcx,%rax)
	addq	$1, %rax
	movzbl	(%rdx,%rax), %esi
	testb	%sil, %sil
	jne	.L87
.L86:
	movq	-56(%rbp), %rax
	movq	-64(%rbp), %r12
	movb	%r13b, (%rax)
	movzbl	(%r12), %edi
	movq	8(%r14), %rdx
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L128:
	addq	$1, %r12
	movslq	%ebx, %rcx
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L137:
	movzbl	1(%r12), %eax
	testb	%al, %al
	js	.L125
	movsbq	%al, %rsi
	testb	$8, (%rdx,%rsi,2)
	jne	.L97
.L125:
	addq	$2, %r12
.L48:
	movq	8(%r15), %rdx
	movb	%al, (%rdx,%rcx)
	movq	8(%r14), %rdx
	addq	$1, %rcx
.L47:
	movzbl	(%r12), %eax
	movl	%ecx, %ebx
	testb	%al, %al
	js	.L136
	movsbq	%al, %rsi
	movzwl	(%rdx,%rsi,2), %esi
	testb	$8, %sil
	jne	.L50
	cmpb	%dil, %al
	je	.L114
	andl	$32, %esi
	leaq	1(%r12), %r8
	jne	.L137
	movq	%r8, %r12
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L136:
	addq	$1, %r12
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L129:
	addq	$1, %r12
	movslq	%ebx, %rbx
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L99:
	movq	%rsi, %r12
.L57:
	movq	8(%r15), %rdx
	movb	%al, (%rdx,%rbx)
	movq	8(%r14), %rdx
	addq	$1, %rbx
.L56:
	movzbl	(%r12), %eax
	movl	%ebx, %ecx
	testb	%al, %al
	js	.L138
	movsbq	%al, %rsi
	testb	$8, (%rdx,%rsi,2)
	jne	.L139
	leaq	1(%r12), %rsi
	cmpb	%dil, %al
	jne	.L99
	movzbl	1(%r12), %eax
	cmpb	%dil, %al
	jne	.L58
	addq	$2, %r12
	movl	%edi, %eax
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L138:
	addq	$1, %r12
	jmp	.L57
.L97:
	movq	%r8, %r12
.L50:
	cmpb	%dil, %al
	je	.L114
.L100:
	movl	%eax, %edi
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L114:
	movzbl	1(%r12), %edi
	addq	$1, %r12
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L139:
	cmpb	%dil, %al
	jne	.L100
	movzbl	1(%r12), %eax
	leaq	1(%r12), %rsi
	.p2align 4,,10
	.p2align 3
.L58:
	movl	%eax, %edi
	movl	%ecx, %ebx
	movq	%rsi, %r12
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L62:
	cmpb	$98, %al
	je	.L103
	cmpb	$116, %al
	movl	$9, %edx
	cmove	%edx, %eax
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L101:
	movl	$13, %eax
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L103:
	movl	$8, %eax
	jmp	.L64
.L66:
	movsbl	2(%r12), %r9d
	leaq	2(%r12), %r10
	movl	$125, %esi
	movl	%r9d, %r13d
	testb	%r9b, %r9b
	jns	.L71
	movb	$0, 2(%r12)
	movq	%r10, %rcx
	movl	$125, %esi
	jmp	.L72
.L67:
	movsbl	2(%r12), %r9d
	leaq	2(%r12), %r10
	movl	$41, %esi
	movl	%r9d, %r13d
	testb	%r9b, %r9b
	jns	.L71
	movb	$0, 2(%r12)
	movq	%r10, %rcx
	movl	$41, %esi
	jmp	.L72
.L132:
	movb	$0, 1(%r12)
	movq	%r10, %rcx
	jmp	.L69
.L78:
	movzbl	2(%rcx), %r13d
	leaq	2(%rcx), %rdx
	movb	$0, (%rcx)
	movq	%rdx, -56(%rbp)
	testb	%r13b, %r13b
	js	.L79
	movq	8(%r14), %rdi
	movq	%rdx, %rax
.L80:
	movzbl	%r13b, %r8d
	testw	$263, (%rdi,%r8,2)
	je	.L121
	movzbl	1(%rax), %r13d
	addq	$1, %rax
	testb	%r13b, %r13b
	jns	.L80
.L121:
	movq	%rax, -56(%rbp)
.L79:
	movq	-56(%rbp), %rax
	movb	$0, (%rax)
	testl	%esi, %esi
	jne	.L140
	movq	%r10, %rsi
	movq	%r14, %rdi
	movq	%rcx, -96(%rbp)
	movl	%r9d, -72(%rbp)
	call	_CONF_get_string@PLT
	movl	-72(%rbp), %r9d
	movq	-96(%rbp), %rcx
	movq	%rax, %rdx
	movq	-56(%rbp), %rax
	movq	%rax, -64(%rbp)
.L88:
	movb	%r9b, (%rcx)
	jmp	.L91
.L134:
	movl	$622, %r8d
	movl	$104, %edx
	movl	$101, %esi
	movl	$14, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L43
.L135:
	movl	$627, %r8d
	movl	$116, %edx
	movl	$101, %esi
	movl	$14, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L43
.L84:
	movl	$631, %r8d
	movl	$65, %edx
	movl	$101, %esi
	movl	$14, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L43
.L140:
	movsbl	%r13b, %eax
	cmpl	%eax, %esi
	jne	.L92
	movq	-56(%rbp), %rax
	movq	%r10, %rsi
	movq	%r14, %rdi
	movq	%rcx, -96(%rbp)
	movl	%r9d, -72(%rbp)
	addq	$1, %rax
	movq	%rax, -64(%rbp)
	call	_CONF_get_string@PLT
	movl	-72(%rbp), %r9d
	movq	-96(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L88
.L131:
	movq	-80(%rbp), %rsi
	leaq	1(%rcx), %rax
	movq	%r10, %rdx
	movq	%r14, %rdi
	movq	%rcx, -56(%rbp)
	movq	%rax, -64(%rbp)
	call	_CONF_get_string@PLT
	movq	%rax, %rdx
	jmp	.L91
	.cfi_endproc
.LFE352:
	.size	str_copy, .-str_copy
	.p2align 4
	.type	def_destroy_data, @function
def_destroy_data:
.LFB348:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L145
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_CONF_free_data@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE348:
	.size	def_destroy_data, .-def_destroy_data
	.section	.rodata.str1.1
.LC5:
	.string	"[%s] %s=%s\n"
.LC6:
	.string	"[[%s]]\n"
	.text
	.p2align 4
	.type	dump_value_doall_arg, @function
dump_value_doall_arg:
.LFB361:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	movq	(%rdi), %rdx
	movq	%rsi, %r9
	testq	%rcx, %rcx
	je	.L151
	movq	16(%rdi), %r8
	leaq	.LC5(%rip), %rsi
	movq	%r9, %rdi
	xorl	%eax, %eax
	jmp	BIO_printf@PLT
	.p2align 4,,10
	.p2align 3
.L151:
	leaq	.LC6(%rip), %rsi
	movq	%r9, %rdi
	xorl	%eax, %eax
	jmp	BIO_printf@PLT
	.cfi_endproc
.LFE361:
	.size	dump_value_doall_arg, .-dump_value_doall_arg
	.p2align 4
	.type	def_destroy, @function
def_destroy:
.LFB347:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L156
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_CONF_free_data@PLT
	movq	%r12, %rdi
	movl	$142, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L156:
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE347:
	.size	def_destroy, .-def_destroy
	.section	.rodata.str1.1
.LC7:
	.string	"default"
.LC8:
	.string	".include"
.LC9:
	.string	"%ld"
.LC10:
	.string	"line "
	.text
	.p2align 4
	.type	def_load_bio, @function
def_load_bio:
.LFB350:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -328(%rbp)
	movq	%rdi, -296(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	$0, -248(%rbp)
	movq	$0, -240(%rbp)
	movq	%rax, -320(%rbp)
	call	BUF_MEM_new@PLT
	movl	$202, %r8d
	movl	$7, %edx
	leaq	.LC0(%rip), %rcx
	movq	%rax, -280(%rbp)
	testq	%rax, %rax
	je	.L424
	movl	$206, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	call	CRYPTO_strdup@PLT
	movl	$208, %r8d
	movl	$65, %edx
	leaq	.LC0(%rip), %rcx
	movq	%rax, -304(%rbp)
	movq	%rax, -248(%rbp)
	testq	%rax, %rax
	je	.L425
	movq	-296(%rbp), %rdi
	call	_CONF_new_data@PLT
	testl	%eax, %eax
	je	.L430
	movq	-248(%rbp), %rsi
	movq	-296(%rbp), %rdi
	call	_CONF_new_section@PLT
	movq	%rax, -304(%rbp)
	testq	%rax, %rax
	je	.L431
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	movq	$0, -312(%rbp)
	leaq	-240(%rbp), %r15
	movl	$0, -264(%rbp)
	movl	$0, -288(%rbp)
	movq	%r13, -272(%rbp)
.L166:
	movl	-288(%rbp), %eax
	movq	-280(%rbp), %rdi
	leal	512(%rax), %esi
	movslq	%esi, %rsi
	call	BUF_MEM_grow@PLT
	testq	%rax, %rax
	je	.L432
	movq	-280(%rbp), %rax
	movslq	-288(%rbp), %rbx
	movq	%r12, %r13
	addq	8(%rax), %rbx
	movb	$0, (%rbx)
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L418:
	movq	%r13, %rdi
	movq	%r12, %r13
	call	BIO_vfree@PLT
.L168:
	movq	%rbx, %rsi
	movl	$511, %edx
	movq	%r13, %rdi
	call	BIO_gets@PLT
	movb	$0, 511(%rbx)
	movq	%rbx, %rdi
	call	strlen@PLT
	movl	-264(%rbp), %ecx
	movl	%eax, %esi
	orl	%eax, %ecx
	jne	.L433
	cmpq	$0, -240(%rbp)
	je	.L172
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	get_next_file
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L418
	movq	%r14, %rdi
	movl	$250, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%r14d, %r14d
	call	CRYPTO_free@PLT
.L172:
	movq	-272(%rbp), %rdi
	call	OPENSSL_sk_pop@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L418
	movq	-280(%rbp), %rdi
	movq	-272(%rbp), %r13
	movl	$1, %r12d
	call	BUF_MEM_free@PLT
	movq	-248(%rbp), %rdi
	movl	$431, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r13, %rdi
	call	OPENSSL_sk_free@PLT
.L161:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L434
	addq	$296, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L430:
	.cfi_restore_state
	movl	$213, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
.L424:
	movl	$121, %esi
	movl	$14, %edi
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	call	ERR_put_error@PLT
	movq	$0, -304(%rbp)
	movq	$0, -312(%rbp)
.L163:
	movq	-280(%rbp), %rdi
	call	BUF_MEM_free@PLT
	movq	-248(%rbp), %rdi
	movl	$440, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L285:
	call	OPENSSL_sk_pop@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	BIO_vfree@PLT
	movq	%rbx, %r12
.L284:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	movq	%r13, %rdi
	testl	%eax, %eax
	jg	.L285
	call	OPENSSL_sk_free@PLT
	movl	$453, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	cmpq	$0, -240(%rbp)
	je	.L286
	leaq	-240(%rbp), %rdi
	call	OPENSSL_DIR_end@PLT
.L286:
	movq	-328(%rbp), %rax
	testq	%rax, %rax
	je	.L287
	movq	-312(%rbp), %rdx
	movq	%rdx, (%rax)
.L287:
	movq	-312(%rbp), %rcx
	leaq	-80(%rbp), %r12
	movl	$24, %esi
	xorl	%eax, %eax
	leaq	.LC9(%rip), %rdx
	movq	%r12, %rdi
	call	BIO_snprintf@PLT
	movl	$2, %edi
	movq	%r12, %rdx
	xorl	%eax, %eax
	leaq	.LC10(%rip), %rsi
	call	ERR_add_error_data@PLT
	movq	-296(%rbp), %rbx
	movq	16(%rbx), %rdi
	cmpq	-320(%rbp), %rdi
	je	.L288
	call	CONF_free@PLT
	movq	$0, 16(%rbx)
.L288:
	movq	-304(%rbp), %rbx
	xorl	%r12d, %r12d
	testq	%rbx, %rbx
	je	.L161
	movq	8(%rbx), %rdi
	movl	$466, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	16(%rbx), %rdi
	movl	$467, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$468, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rbx, %rdi
	call	CRYPTO_free@PLT
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L433:
	movq	%r13, %r12
	movl	$1, %edx
	testl	%eax, %eax
	jle	.L171
	movslq	%eax, %rdx
	leaq	-1(%rbx,%rdx), %rdx
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L176:
	subq	$1, %rdx
	subl	$1, %eax
	je	.L178
.L177:
	movzbl	(%rdx), %ecx
	cmpb	$13, %cl
	je	.L176
	cmpb	$10, %cl
	je	.L176
	cmpl	%eax, %esi
	sete	%dl
.L171:
	addl	%eax, -288(%rbp)
	testl	%esi, %esi
	je	.L178
	testb	%dl, %dl
	je	.L178
	movl	-288(%rbp), %eax
	testl	%eax, %eax
	jle	.L416
	movq	-280(%rbp), %rax
	movq	-296(%rbp), %rdx
	movq	8(%rax), %rbx
	movslq	-288(%rbp), %rax
	movq	8(%rdx), %rdx
	leaq	-1(%rbx,%rax), %rcx
	movsbq	(%rcx), %rax
	testb	%al, %al
	js	.L416
	testb	$32, (%rdx,%rax,2)
	jne	.L328
.L416:
	movl	$1, -264(%rbp)
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L178:
	cltq
	movl	-288(%rbp), %esi
	addq	$1, -312(%rbp)
	movb	$0, (%rbx,%rax)
	movq	-280(%rbp), %rax
	movq	8(%rax), %rbx
	testl	%esi, %esi
	jle	.L435
	movslq	-288(%rbp), %rax
	movq	-296(%rbp), %rdx
	leaq	-1(%rbx,%rax), %rcx
	movq	8(%rdx), %rdx
	movsbq	(%rcx), %rax
	testb	%al, %al
	js	.L182
	testb	$32, (%rdx,%rax,2)
	je	.L182
	movl	$0, -264(%rbp)
.L298:
	cmpl	$1, -288(%rbp)
	je	.L183
	movsbq	-1(%rcx), %rax
	testb	%al, %al
	js	.L183
	testb	$32, (%rdx,%rax,2)
	je	.L183
	movl	-264(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L166
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L435:
	movq	-296(%rbp), %rax
	movq	8(%rax), %rdx
.L182:
	movzbl	(%rbx), %r8d
	movl	%r8d, %esi
	testb	%r8b, %r8b
	js	.L301
	movzbl	%r8b, %ecx
	movq	%rbx, %rax
.L191:
	movzwl	(%rdx,%rcx,2), %ecx
	testb	$8, %ch
	jne	.L421
	andl	$16, %ecx
	je	.L436
	movzbl	1(%rax), %ecx
	addq	$1, %rax
	testb	%cl, %cl
	jns	.L191
	.p2align 4,,10
	.p2align 3
.L192:
	movzbl	1(%rax), %ecx
	addq	$1, %rax
.L190:
	testb	%cl, %cl
	js	.L192
	movsbq	%cl, %rdi
	movzwl	(%rdx,%rdi,2), %edi
	testb	$-128, %dil
	jne	.L421
	testw	$1024, %di
	jne	.L437
	testb	$64, %dil
	jne	.L419
	testb	$32, %dil
	jne	.L438
	andl	$8, %edi
	je	.L192
.L422:
	testb	%r8b, %r8b
	js	.L209
.L439:
	movzbl	%r8b, %ecx
	movzwl	(%rdx,%rcx,2), %eax
	movl	%eax, %esi
	andl	$8, %esi
	testb	$16, %al
	je	.L211
	testw	%si, %si
	jne	.L417
	movzbl	1(%rbx), %r8d
	addq	$1, %rbx
	movl	%r8d, %esi
	testb	%r8b, %r8b
	jns	.L439
.L209:
	movq	%rbx, %rax
.L232:
	movq	-248(%rbp), %r11
	testb	%r8b, %r8b
	js	.L313
.L246:
	movq	%rax, %r9
.L248:
	movzbl	%sil, %ecx
	movzwl	(%rdx,%rcx,2), %ecx
	testb	$16, %cl
	je	.L247
	andl	$8, %ecx
	jne	.L247
	movzbl	1(%r9), %r8d
	addq	$1, %r9
	movl	%r8d, %esi
	testb	%r8b, %r8b
	jns	.L248
.L247:
	movl	$8, %ecx
	movq	%rbx, %rsi
	leaq	.LC8(%rip), %rdi
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	jne	.L440
.L294:
	addq	$8, %rbx
	cmpq	%rbx, %r9
	jne	.L329
	cmpb	$61, %r8b
	jne	.L406
.L329:
	movq	$0, -232(%rbp)
	cmpb	$61, (%r9)
	je	.L441
.L253:
	movq	%r9, %rcx
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L257:
	addq	$1, %rcx
.L256:
	movsbq	(%rcx), %rax
	testb	%al, %al
	js	.L257
	testb	$8, (%rdx,%rax,2)
	je	.L257
	subq	$1, %rcx
	cmpq	%rcx, %r9
	ja	.L258
	leaq	-1(%r9), %rsi
.L259:
	movsbq	(%rcx), %rax
	testb	%al, %al
	js	.L258
	testb	$16, (%rdx,%rax,2)
	je	.L258
	subq	$1, %rcx
	cmpq	%rsi, %rcx
	jne	.L259
.L258:
	movb	$0, 1(%rcx)
	movq	-296(%rbp), %rdi
	movq	%r9, %rcx
	movq	%r11, %rsi
	leaq	-232(%rbp), %rdx
	call	str_copy
	testl	%eax, %eax
	je	.L411
	movq	-232(%rbp), %rbx
	xorl	%eax, %eax
	leaq	-224(%rbp), %rdi
	movl	$18, %ecx
	rep stosq
	leaq	-224(%rbp), %rdx
	movl	$1, %edi
	movq	%rbx, %rsi
	call	__xstat@PLT
	testl	%eax, %eax
	js	.L442
	movl	-200(%rbp), %eax
	andl	$61440, %eax
	cmpl	$16384, %eax
	jne	.L264
	cmpq	$0, -240(%rbp)
	je	.L265
	movl	$684, %r8d
	movl	$111, %edx
	movl	$116, %esi
	movl	$14, %edi
	leaq	.LC0(%rip), %rcx
.L429:
	call	ERR_put_error@PLT
	movq	%rbx, %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	call	ERR_add_error_data@PLT
.L263:
	movq	-232(%rbp), %rdi
	movq	%r12, %r13
	cmpq	%rdi, %r14
	je	.L268
	movl	$368, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
.L268:
	movq	%r13, %r12
.L417:
	movl	$0, -264(%rbp)
	movl	$0, -288(%rbp)
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L419:
	movzbl	1(%rax), %edi
	addq	$1, %rax
.L202:
	testb	%dil, %dil
	js	.L419
.L205:
	movsbq	%dil, %r9
	movzwl	(%rdx,%r9,2), %r9d
	testb	$8, %r9b
	jne	.L443
	movzbl	1(%rax), %r10d
	leaq	1(%rax), %r11
	cmpb	%dil, %cl
	je	.L291
	andl	$32, %r9d
	je	.L304
	testb	%r10b, %r10b
	js	.L420
	movsbq	%r10b, %rdi
	testb	$8, (%rdx,%rdi,2)
	jne	.L291
.L420:
	movzbl	2(%rax), %edi
	addq	$2, %rax
	testb	%dil, %dil
	jns	.L205
	jmp	.L419
.L183:
	subl	$1, -288(%rbp)
	movl	$1, -264(%rbp)
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L304:
	movl	%r10d, %edi
	movq	%r11, %rax
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L438:
	movzbl	1(%rax), %ecx
	testb	%cl, %cl
	js	.L208
	movsbq	%cl, %rdi
	testb	$8, (%rdx,%rdi,2)
	je	.L208
	addq	$1, %rax
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L437:
	movzbl	1(%rax), %edi
	addq	$1, %rax
	.p2align 4,,10
	.p2align 3
.L195:
	testb	%dil, %dil
	js	.L444
.L198:
	movsbq	%dil, %r9
	testb	$8, (%rdx,%r9,2)
	jne	.L445
	movzbl	1(%rax), %r9d
	leaq	1(%rax), %r10
	cmpb	%dil, %cl
	je	.L446
	movl	%r9d, %edi
	movq	%r10, %rax
	testb	%dil, %dil
	jns	.L198
.L444:
	movzbl	1(%rax), %edi
	addq	$1, %rax
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L446:
	cmpb	%r9b, %cl
	jne	.L197
	movzbl	2(%rax), %edi
	addq	$2, %rax
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L208:
	movzbl	2(%rax), %ecx
	addq	$2, %rax
	jmp	.L190
.L431:
	movl	$219, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$103, %edx
.L425:
	movl	$121, %esi
	movl	$14, %edi
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	call	ERR_put_error@PLT
	movq	$0, -312(%rbp)
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L445:
	cmpb	%dil, %cl
	je	.L447
.L306:
	movl	%edi, %ecx
	jmp	.L190
.L443:
	cmpb	%dil, %cl
	jne	.L306
	movzbl	1(%rax), %r10d
	leaq	1(%rax), %r11
	.p2align 4,,10
	.p2align 3
.L291:
	movl	%r10d, %ecx
	movq	%r11, %rax
	jmp	.L190
.L447:
	movzbl	1(%rax), %r9d
	leaq	1(%rax), %r10
.L197:
	movl	%r9d, %ecx
	movq	%r10, %rax
	jmp	.L190
.L421:
	movb	$0, (%rax)
	movzbl	(%rbx), %r8d
	movq	-296(%rbp), %rax
	movl	%r8d, %esi
	movq	8(%rax), %rdx
	jmp	.L422
.L211:
	testw	%si, %si
	jne	.L417
	movq	%rbx, %rax
	cmpb	$91, %r8b
	jne	.L214
	movzbl	1(%rbx), %eax
	leaq	1(%rbx), %r8
	testb	%al, %al
	js	.L399
.L216:
	movzbl	%al, %esi
	movzwl	(%rdx,%rsi,2), %ecx
	testb	$16, %cl
	je	.L307
	andl	$8, %ecx
	jne	.L307
	movzbl	1(%r8), %eax
	addq	$1, %r8
	testb	%al, %al
	jns	.L216
.L399:
	movq	-272(%rbp), %r13
.L215:
	movl	$323, %r8d
	movl	$100, %edx
	movl	$121, %esi
	movl	$14, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	$0, -304(%rbp)
	jmp	.L163
.L214:
	movzwl	(%rdx,%rcx,2), %ecx
	testb	$32, %cl
	jne	.L448
.L233:
	testw	$775, %cx
	je	.L449
	movzbl	1(%rax), %r8d
	addq	$1, %rax
	movl	%r8d, %esi
.L236:
	testb	%r8b, %r8b
	js	.L232
.L235:
	movzbl	%r8b, %ecx
	movzwl	(%rdx,%rcx,2), %ecx
	testb	$32, %cl
	je	.L233
.L448:
	movzbl	1(%rax), %r8d
	testb	%r8b, %r8b
	js	.L234
	movsbq	%r8b, %rcx
	testb	$8, (%rdx,%rcx,2)
	je	.L234
	addq	$1, %rax
	jmp	.L235
.L234:
	movzbl	2(%rax), %r8d
	addq	$2, %rax
	movl	%r8d, %esi
	jmp	.L236
.L301:
	movl	%r8d, %ecx
	movq	%rbx, %rax
	jmp	.L190
.L432:
	movl	$227, %r8d
	movl	$7, %edx
	movl	$121, %esi
	movl	$14, %edi
	leaq	.LC0(%rip), %rcx
	movq	-272(%rbp), %r13
	call	ERR_put_error@PLT
	movq	$0, -304(%rbp)
	jmp	.L163
.L440:
	cmpb	$61, %r8b
	jne	.L406
	movb	$0, (%rax)
	movq	-296(%rbp), %rax
	leaq	1(%r9), %rcx
	movq	8(%rax), %rsi
	movzbl	1(%r9), %eax
	testb	%al, %al
	js	.L271
.L273:
	movzwl	(%rsi,%rax,2), %eax
	testb	$16, %al
	je	.L271
	testb	$8, %al
	jne	.L271
	movzbl	1(%rcx), %eax
	addq	$1, %rcx
	testb	%al, %al
	jns	.L273
.L271:
	movq	%rcx, %rdx
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L276:
	addq	$1, %rdx
.L274:
	movsbq	(%rdx), %rax
	testb	%al, %al
	js	.L276
	testb	$8, (%rsi,%rax,2)
	je	.L276
	subq	$1, %rdx
	cmpq	%rcx, %rdx
	jb	.L277
	leaq	-1(%rcx), %rdi
.L278:
	movsbq	(%rdx), %rax
	testb	%al, %al
	js	.L277
	testb	$16, (%rsi,%rax,2)
	je	.L277
	subq	$1, %rdx
	cmpq	%rdi, %rdx
	jne	.L278
.L277:
	movb	$0, 1(%rdx)
	leaq	.LC0(%rip), %rsi
	movl	$399, %edx
	movl	$24, %edi
	movq	%r11, -336(%rbp)
	movq	%rcx, -288(%rbp)
	call	CRYPTO_malloc@PLT
	movq	-288(%rbp), %rcx
	movq	-336(%rbp), %r11
	testq	%rax, %rax
	movq	%rax, -264(%rbp)
	je	.L450
	movl	$403, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rbx, %rdi
	movq	%r11, -336(%rbp)
	movq	%rcx, -288(%rbp)
	call	CRYPTO_strdup@PLT
	movq	-264(%rbp), %rdx
	movq	-288(%rbp), %rcx
	testq	%rax, %rax
	movq	-336(%rbp), %r11
	movq	%rax, 8(%rdx)
	movq	$0, 16(%rdx)
	je	.L451
	movq	-296(%rbp), %rbx
	leaq	16(%rdx), %rdx
	movq	%r11, %rsi
	movq	%r11, -288(%rbp)
	movq	%rbx, %rdi
	call	str_copy
	testl	%eax, %eax
	je	.L318
	movq	-288(%rbp), %r11
	movq	-248(%rbp), %rsi
	movq	%r11, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L319
	movq	-288(%rbp), %r11
	movq	%rbx, %rdi
	movq	%r11, %rsi
	call	_CONF_get_section@PLT
	movq	-288(%rbp), %r11
	testq	%rax, %rax
	movq	%rax, %rsi
	je	.L452
.L282:
	movq	-264(%rbp), %rdx
	movq	-296(%rbp), %rdi
	call	_CONF_add_string@PLT
	testl	%eax, %eax
	jne	.L417
	movq	-272(%rbp), %r13
	movl	$424, %r8d
.L426:
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
.L427:
	movl	$121, %esi
	movl	$14, %edi
	call	ERR_put_error@PLT
	movq	-264(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L449:
	cmpb	$58, %r8b
	jne	.L453
	cmpb	$58, 1(%rax)
	movl	$58, %esi
	jne	.L232
	movq	-296(%rbp), %rdx
	movzbl	2(%rax), %r8d
	leaq	2(%rax), %r10
	movb	$0, (%rax)
	movq	%r10, %rax
	movq	8(%rdx), %rdx
	testb	%r8b, %r8b
	js	.L454
	movzbl	%r8b, %ecx
	movzwl	(%rdx,%rcx,2), %ecx
	testb	$32, %cl
	jne	.L455
.L241:
	testw	$775, %cx
	je	.L456
	movzbl	1(%rax), %r8d
	leaq	1(%rax), %r9
.L244:
	testb	%r8b, %r8b
	js	.L239
.L243:
	movzbl	%r8b, %ecx
	movq	%r9, %rax
	movzwl	(%rdx,%rcx,2), %ecx
	testb	$32, %cl
	je	.L241
.L455:
	movzbl	1(%rax), %r8d
	testb	%r8b, %r8b
	js	.L242
	movsbq	%r8b, %rcx
	leaq	1(%rax), %r9
	testb	$8, (%rdx,%rcx,2)
	jne	.L243
.L242:
	movzbl	2(%rax), %r8d
	leaq	2(%rax), %r9
	jmp	.L244
.L264:
	movq	%rbx, %rdi
	leaq	.LC4(%rip), %rsi
	call	BIO_new_file@PLT
	movq	-232(%rbp), %rdi
	movq	%rax, %r13
	cmpq	%r14, %rdi
	je	.L267
.L292:
	movl	$368, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
.L267:
	testq	%r13, %r13
	je	.L316
.L293:
	cmpq	$0, -272(%rbp)
	je	.L457
.L269:
	movq	-272(%rbp), %rdi
	movq	%r12, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L268
	movq	-272(%rbp), %r13
	movl	$383, %r8d
.L423:
	movl	$65, %edx
	movl	$121, %esi
	movl	$14, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L428:
	movq	$0, -304(%rbp)
	jmp	.L163
.L436:
	movzbl	(%rax), %ecx
	jmp	.L190
.L313:
	movq	%rax, %r9
	jmp	.L247
.L319:
	movq	-304(%rbp), %rsi
	jmp	.L282
.L307:
	movq	-272(%rbp), %r13
	movq	%r8, %r9
	.p2align 4,,10
	.p2align 3
.L231:
	movzwl	(%rdx,%rsi,2), %edi
	movq	%r9, %rcx
	testb	$32, %dil
	jne	.L458
.L222:
	testw	$775, %di
	je	.L459
	movzbl	1(%rcx), %eax
	addq	$1, %rcx
.L225:
	testb	%al, %al
	js	.L219
.L224:
	movzbl	%al, %esi
	movzwl	(%rdx,%rsi,2), %edi
	testb	$32, %dil
	je	.L222
.L458:
	movzbl	1(%rcx), %eax
	testb	%al, %al
	js	.L223
	movsbq	%al, %rsi
	testb	$8, (%rdx,%rsi,2)
	je	.L223
	addq	$1, %rcx
	jmp	.L224
.L459:
	movzwl	(%rdx,%rsi,2), %esi
	movq	%rcx, %rdi
	testb	$16, %sil
	je	.L228
.L460:
	andl	$8, %esi
	jne	.L228
	movzbl	1(%rdi), %eax
	addq	$1, %rdi
	testb	%al, %al
	js	.L389
	movzbl	%al, %esi
	movzwl	(%rdx,%rsi,2), %esi
	testb	$16, %sil
	jne	.L460
.L228:
	cmpb	$93, %al
	je	.L230
	testb	%al, %al
	setne	%al
	cmpq	%rdi, %r9
	movq	%rdi, %r9
	setne	%cl
	andl	%ecx, %eax
	.p2align 4,,10
	.p2align 3
.L221:
	testb	%al, %al
	je	.L215
	movzbl	(%r9), %eax
	testb	%al, %al
	js	.L215
	movzbl	%al, %esi
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L219:
	cmpq	%r9, %rcx
	movq	%rcx, %r9
	setne	%al
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L223:
	movzbl	2(%rcx), %eax
	addq	$2, %rcx
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L389:
	cmpq	%r9, %rdi
	movq	%rdi, %r9
	setne	%al
	jmp	.L221
.L230:
	movb	$0, (%rcx)
	movq	-296(%rbp), %rbx
	xorl	%esi, %esi
	movq	%r8, %rcx
	leaq	-248(%rbp), %rdx
	movq	%rbx, %rdi
	call	str_copy
	testl	%eax, %eax
	je	.L411
	movq	-248(%rbp), %rsi
	movq	%rbx, %rdi
	call	_CONF_get_section@PLT
	movl	$0, -264(%rbp)
	movq	%rax, -304(%rbp)
	movl	$0, -288(%rbp)
	testq	%rax, %rax
	jne	.L166
	movq	-248(%rbp), %rsi
	movq	-296(%rbp), %rdi
	call	_CONF_new_section@PLT
	movq	%rax, -304(%rbp)
	testq	%rax, %rax
	jne	.L166
	movl	$333, %r8d
	movl	$103, %edx
	movl	$121, %esi
	movl	$14, %edi
	leaq	.LC0(%rip), %rcx
	movq	-272(%rbp), %r13
	call	ERR_put_error@PLT
	jmp	.L163
.L457:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, -272(%rbp)
	testq	%rax, %rax
	jne	.L269
	movq	%rax, %r13
	movl	$378, %r8d
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L265:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	get_next_file
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L263
	movq	-232(%rbp), %rdi
	movq	%rbx, %r14
	cmpq	%rdi, %rbx
	jne	.L292
	jmp	.L293
.L441:
	movzbl	1(%r9), %eax
	addq	$1, %r9
	testb	%al, %al
	js	.L253
.L254:
	movzwl	(%rdx,%rax,2), %eax
	testb	$16, %al
	je	.L253
	testb	$8, %al
	jne	.L253
	movzbl	1(%r9), %eax
	addq	$1, %r9
	testb	%al, %al
	jns	.L254
	jmp	.L253
.L406:
	movl	$391, %r8d
	movl	$101, %edx
	movl	$121, %esi
	movl	$14, %edi
	leaq	.LC0(%rip), %rcx
	movq	-272(%rbp), %r13
	call	ERR_put_error@PLT
	jmp	.L428
.L454:
	movq	%r10, %r9
.L239:
	movl	$8, %ecx
	movq	%r10, %rsi
	leaq	.LC8(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L406
	movq	%rbx, %r11
	movq	%r10, %rbx
	jmp	.L294
.L442:
	call	__errno_location@PLT
	movl	$676, %r8d
	movl	$22, %esi
	leaq	.LC0(%rip), %rcx
	movl	(%rax), %edx
	movl	$2, %edi
	jmp	.L429
.L452:
	movq	-296(%rbp), %rdi
	movq	%r11, %rsi
	call	_CONF_new_section@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L282
	movq	-272(%rbp), %r13
	movl	$417, %r8d
	movl	$103, %edx
	leaq	.LC0(%rip), %rcx
	jmp	.L427
.L411:
	movq	-272(%rbp), %r13
	jmp	.L428
.L318:
	movq	-264(%rbp), %rax
	movq	-272(%rbp), %r13
	movq	%rax, -304(%rbp)
	jmp	.L163
.L453:
	movl	%r8d, %esi
	jmp	.L232
.L316:
	movq	%r12, %r13
	jmp	.L268
.L456:
	movq	%rbx, %r11
	movl	%r8d, %esi
	movq	%r10, %rbx
	jmp	.L246
.L451:
	movq	-272(%rbp), %r13
	movl	$406, %r8d
	jmp	.L426
.L450:
	movl	$400, %r8d
	movl	$65, %edx
	movl	$121, %esi
	movl	$14, %edi
	leaq	.LC0(%rip), %rcx
	movq	-272(%rbp), %r13
	call	ERR_put_error@PLT
	movq	$0, -304(%rbp)
	jmp	.L163
.L328:
	movl	$1, -264(%rbp)
	jmp	.L298
.L434:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE350:
	.size	def_load_bio, .-def_load_bio
	.section	.rodata.str1.1
.LC11:
	.string	"rb"
	.text
	.p2align 4
	.type	def_load, @function
def_load:
.LFB349:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movq	%rsi, %rdi
	leaq	.LC11(%rip), %rsi
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	call	BIO_new_file@PLT
	testq	%rax, %rax
	je	.L466
	movq	%r13, %rdi
	movq	%rax, %r12
	movq	%r14, %rdx
	movq	%rax, %rsi
	call	def_load_bio
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	BIO_free@PLT
.L461:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L466:
	.cfi_restore_state
	call	ERR_peek_last_error@PLT
	andl	$4095, %eax
	cmpl	$128, %eax
	je	.L467
	movl	$170, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$2, %edx
	xorl	%r13d, %r13d
	movl	$120, %esi
	movl	$14, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L467:
	.cfi_restore_state
	movl	$168, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$114, %edx
	xorl	%r13d, %r13d
	movl	$120, %esi
	movl	$14, %edi
	call	ERR_put_error@PLT
	jmp	.L461
	.cfi_endproc
.LFE349:
	.size	def_load, .-def_load
	.p2align 4
	.globl	NCONF_default
	.type	NCONF_default, @function
NCONF_default:
.LFB342:
	.cfi_startproc
	endbr64
	leaq	default_method(%rip), %rax
	ret
	.cfi_endproc
.LFE342:
	.size	NCONF_default, .-NCONF_default
	.p2align 4
	.globl	NCONF_WIN32
	.type	NCONF_WIN32, @function
NCONF_WIN32:
.LFB343:
	.cfi_startproc
	endbr64
	leaq	WIN32_method(%rip), %rax
	ret
	.cfi_endproc
.LFE343:
	.size	NCONF_WIN32, .-NCONF_WIN32
	.section	.rodata.str1.1
.LC12:
	.string	"WIN32"
	.section	.data.rel.local,"aw"
	.align 32
	.type	WIN32_method, @object
	.size	WIN32_method, 80
WIN32_method:
	.quad	.LC12
	.quad	def_create
	.quad	def_init_WIN32
	.quad	def_destroy
	.quad	def_destroy_data
	.quad	def_load_bio
	.quad	def_dump
	.quad	def_is_number
	.quad	def_to_int
	.quad	def_load
	.section	.rodata.str1.1
.LC13:
	.string	"OpenSSL default"
	.section	.data.rel.local
	.align 32
	.type	default_method, @object
	.size	default_method, 80
default_method:
	.quad	.LC13
	.quad	def_create
	.quad	def_init_default
	.quad	def_destroy
	.quad	def_destroy_data
	.quad	def_load_bio
	.quad	def_dump
	.quad	def_is_number
	.quad	def_to_int
	.quad	def_load
	.section	.rodata
	.align 32
	.type	CONF_type_win32, @object
	.size	CONF_type_win32, 256
CONF_type_win32:
	.value	8
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	16
	.value	16
	.value	0
	.value	0
	.value	16
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	16
	.value	512
	.value	1024
	.value	0
	.value	0
	.value	512
	.value	512
	.value	0
	.value	0
	.value	0
	.value	512
	.value	512
	.value	512
	.value	512
	.value	512
	.value	512
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	0
	.value	2560
	.value	0
	.value	0
	.value	0
	.value	512
	.value	512
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	0
	.value	0
	.value	0
	.value	512
	.value	256
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	512
	.value	0
	.value	512
	.value	0
	.align 32
	.type	CONF_type_default, @object
	.size	CONF_type_default, 256
CONF_type_default:
	.value	8
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	16
	.value	16
	.value	0
	.value	0
	.value	16
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	16
	.value	512
	.value	64
	.value	128
	.value	0
	.value	512
	.value	512
	.value	64
	.value	0
	.value	0
	.value	512
	.value	512
	.value	512
	.value	512
	.value	512
	.value	512
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	0
	.value	512
	.value	0
	.value	0
	.value	0
	.value	512
	.value	512
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	0
	.value	32
	.value	0
	.value	512
	.value	256
	.value	64
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	512
	.value	0
	.value	512
	.value	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
