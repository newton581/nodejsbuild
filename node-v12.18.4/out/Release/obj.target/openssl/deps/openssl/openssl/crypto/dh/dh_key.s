	.file	"dh_key.c"
	.text
	.p2align 4
	.type	dh_init, @function
dh_init:
.LFB430:
	.cfi_startproc
	endbr64
	orl	$1, 48(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE430:
	.size	dh_init, .-dh_init
	.p2align 4
	.type	dh_finish, @function
dh_finish:
.LFB431:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	56(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	BN_MONT_CTX_free@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE431:
	.size	dh_finish, .-dh_finish
	.p2align 4
	.type	dh_bn_mod_exp, @function
dh_bn_mod_exp:
.LFB429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	movq	%r8, %rcx
	movq	%r9, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	16(%rbp), %r9
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	BN_mod_exp_mont@PLT
	.cfi_endproc
.LFE429:
	.size	dh_bn_mod_exp, .-dh_bn_mod_exp
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/dh/dh_key.c"
	.text
	.p2align 4
	.type	compute_key, @function
compute_key:
.LFB428:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -72(%rbp)
	movq	8(%rdx), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	BN_num_bits@PLT
	cmpl	$10000, %eax
	jg	.L24
	call	BN_CTX_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L19
	movq	%rax, %rdi
	call	BN_CTX_start@PLT
	movq	%r13, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L19
	cmpq	$0, 40(%r12)
	je	.L25
	testb	$1, 48(%r12)
	jne	.L26
	xorl	%ebx, %ebx
.L11:
	leaq	-60(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	DH_check_pub_key@PLT
	testl	%eax, %eax
	je	.L12
	movl	-60(%rbp), %esi
	testl	%esi, %esi
	jne	.L12
	movq	120(%r12), %rax
	subq	$8, %rsp
	movq	%r14, %rdx
	movq	%r13, %r9
	movq	40(%r12), %rcx
	movq	8(%r12), %r8
	pushq	%rbx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	*24(%rax)
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	je	.L27
	movq	-72(%rbp), %rsi
	movq	%r15, %rdi
	call	BN_bn2bin@PLT
	movl	%eax, %r12d
.L9:
	movq	%r13, %rdi
	call	BN_CTX_end@PLT
	movq	%r13, %rdi
	call	BN_CTX_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L28
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movq	136(%r12), %rsi
	movq	8(%r12), %rdx
	leaq	56(%r12), %rdi
	movq	%r13, %rcx
	call	BN_MONT_CTX_set_locked@PLT
	movq	40(%r12), %rdi
	movl	$4, %esi
	movq	%rax, %rbx
	call	BN_set_flags@PLT
	testq	%rbx, %rbx
	jne	.L11
	.p2align 4,,10
	.p2align 3
.L19:
	movl	$-1, %r12d
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$205, %r8d
	movl	$102, %edx
	movl	$102, %esi
	movl	$5, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L24:
	movl	$179, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$103, %edx
	movl	$102, %esi
	movl	$5, %edi
	movl	$-1, %r12d
	xorl	%r13d, %r13d
	call	ERR_put_error@PLT
	jmp	.L9
.L27:
	movl	$211, %r8d
	movl	$3, %edx
	movl	$102, %esi
	movl	$5, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L9
.L25:
	movl	$192, %r8d
	movl	$100, %edx
	movl	$102, %esi
	movl	$5, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L9
.L28:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE428:
	.size	compute_key, .-compute_key
	.p2align 4
	.type	generate_key, @function
generate_key:
.LFB427:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rdi
	call	BN_num_bits@PLT
	cmpl	$10000, %eax
	jg	.L87
	call	BN_CTX_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L32
	movq	40(%rbx), %r12
	xorl	%r8d, %r8d
	testq	%r12, %r12
	je	.L88
.L33:
	movq	32(%rbx), %r14
	testq	%r14, %r14
	je	.L89
.L34:
	xorl	%r15d, %r15d
	testb	$1, 48(%rbx)
	jne	.L90
.L36:
	testl	%r8d, %r8d
	je	.L42
	movq	64(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.L41
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L91:
	movq	%r12, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L40
.L85:
	movq	64(%rbx), %rsi
.L41:
	movq	%r12, %rdi
	call	BN_priv_rand_range@PLT
	testl	%eax, %eax
	jne	.L91
.L35:
	movl	$160, %r8d
	movl	$3, %edx
	movl	$103, %esi
	movl	$5, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	cmpq	%r14, 32(%rbx)
	je	.L86
	movq	%r14, %rdi
	call	BN_free@PLT
.L86:
	movq	40(%rbx), %rax
	xorl	%r14d, %r14d
	cmpq	%r12, %rax
	je	.L46
	movq	%r12, %rdi
	call	BN_free@PLT
.L46:
	movq	%r13, %rdi
	call	BN_CTX_free@PLT
.L29:
	leaq	-40(%rbp), %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	movl	$86, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$103, %edx
	xorl	%r14d, %r14d
	movl	$103, %esi
	movl	$5, %edi
	call	ERR_put_error@PLT
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L39:
	movl	24(%rbx), %esi
	testl	%esi, %esi
	je	.L92
.L44:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	BN_priv_rand@PLT
	testl	%eax, %eax
	je	.L35
	movq	16(%rbx), %rdi
	movl	$2, %esi
	call	BN_is_word@PLT
	testl	%eax, %eax
	je	.L42
	movq	8(%rbx), %rdi
	movl	$2, %esi
	call	BN_is_bit_set@PLT
	testl	%eax, %eax
	jne	.L42
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	BN_clear_bit@PLT
	testl	%eax, %eax
	je	.L35
	.p2align 4,,10
	.p2align 3
.L42:
	call	BN_new@PLT
	testq	%rax, %rax
	je	.L35
	movq	%rax, %rdi
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%rax, -64(%rbp)
	call	BN_with_flags@PLT
	movq	-64(%rbp), %r10
	movq	120(%rbx), %rax
	movq	%r13, %r9
	subq	$8, %rsp
	movq	16(%rbx), %rdx
	movq	8(%rbx), %r8
	movq	%r14, %rsi
	pushq	%r15
	movq	%r10, %rcx
	movq	%rbx, %rdi
	call	*24(%rax)
	popq	%rdx
	movq	-64(%rbp), %r10
	testl	%eax, %eax
	popq	%rcx
	je	.L93
	movq	%r14, %xmm0
	movq	%r12, %xmm1
	movq	%r10, %rdi
	movl	$1, %r14d
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	BN_clear_free@PLT
	movdqa	-64(%rbp), %xmm0
	movups	%xmm0, 32(%rbx)
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L40:
	movq	%r12, %rdi
	call	BN_is_one@PLT
	testl	%eax, %eax
	jne	.L85
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L90:
	movq	8(%rbx), %rdx
	leaq	56(%rbx), %rdi
	movq	%r13, %rcx
	movl	%r8d, -64(%rbp)
	movq	136(%rbx), %rsi
	call	BN_MONT_CTX_set_locked@PLT
	movl	-64(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, %r15
	jne	.L36
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L88:
	call	BN_secure_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L32
	movl	$1, %r8d
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L89:
	movl	%r8d, -64(%rbp)
	call	BN_new@PLT
	movl	-64(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.L34
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L32:
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L93:
	movq	%r10, %rdi
	call	BN_clear_free@PLT
	jmp	.L35
.L92:
	movq	8(%rbx), %rdi
	call	BN_num_bits@PLT
	leal	-1(%rax), %esi
	jmp	.L44
	.cfi_endproc
.LFE427:
	.size	generate_key, .-generate_key
	.p2align 4
	.globl	DH_generate_key
	.type	DH_generate_key, @function
DH_generate_key:
.LFB421:
	.cfi_startproc
	endbr64
	movq	120(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE421:
	.size	DH_generate_key, .-DH_generate_key
	.p2align 4
	.globl	DH_compute_key
	.type	DH_compute_key, @function
DH_compute_key:
.LFB422:
	.cfi_startproc
	endbr64
	movq	120(%rdx), %rax
	jmp	*16(%rax)
	.cfi_endproc
.LFE422:
	.size	DH_compute_key, .-DH_compute_key
	.p2align 4
	.globl	DH_compute_key_padded
	.type	DH_compute_key_padded, @function
DH_compute_key_padded:
.LFB423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$8, %rsp
	movq	120(%rdx), %rax
	call	*16(%rax)
	movl	%eax, %r12d
	testl	%eax, %eax
	jle	.L96
	movq	8(%rbx), %rdi
	movl	%eax, %r14d
	call	BN_num_bits@PLT
	leal	14(%rax), %r12d
	addl	$7, %eax
	cmovns	%eax, %r12d
	sarl	$3, %r12d
	movl	%r12d, %eax
	subl	%r14d, %eax
	testl	%eax, %eax
	jg	.L100
.L96:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	movslq	%eax, %r15
	movslq	%r14d, %rdx
	movq	%r13, %rsi
	leaq	0(%r13,%r15), %rdi
	call	memmove@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	memset@PLT
	jmp	.L96
	.cfi_endproc
.LFE423:
	.size	DH_compute_key_padded, .-DH_compute_key_padded
	.p2align 4
	.globl	DH_OpenSSL
	.type	DH_OpenSSL, @function
DH_OpenSSL:
.LFB424:
	.cfi_startproc
	endbr64
	leaq	dh_ossl(%rip), %rax
	ret
	.cfi_endproc
.LFE424:
	.size	DH_OpenSSL, .-DH_OpenSSL
	.p2align 4
	.globl	DH_set_default_method
	.type	DH_set_default_method, @function
DH_set_default_method:
.LFB425:
	.cfi_startproc
	endbr64
	movq	%rdi, default_DH_method(%rip)
	ret
	.cfi_endproc
.LFE425:
	.size	DH_set_default_method, .-DH_set_default_method
	.p2align 4
	.globl	DH_get_default_method
	.type	DH_get_default_method, @function
DH_get_default_method:
.LFB426:
	.cfi_startproc
	endbr64
	movq	default_DH_method(%rip), %rax
	ret
	.cfi_endproc
.LFE426:
	.size	DH_get_default_method, .-DH_get_default_method
	.section	.data.rel.local,"aw"
	.align 8
	.type	default_DH_method, @object
	.size	default_DH_method, 8
default_DH_method:
	.quad	dh_ossl
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"OpenSSL DH Method"
	.section	.data.rel.local
	.align 32
	.type	dh_ossl, @object
	.size	dh_ossl, 72
dh_ossl:
	.quad	.LC1
	.quad	generate_key
	.quad	compute_key
	.quad	dh_bn_mod_exp
	.quad	dh_init
	.quad	dh_finish
	.long	1024
	.zero	4
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
