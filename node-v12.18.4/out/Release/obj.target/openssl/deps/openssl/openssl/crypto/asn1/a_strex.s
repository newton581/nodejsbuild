	.file	"a_strex.c"
	.text
	.p2align 4
	.type	send_bio_chars, @function
send_bio_chars:
.LFB803:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L5
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%edx, %ebx
	subq	$8, %rsp
	call	BIO_write@PLT
	cmpl	%eax, %ebx
	sete	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore 3
	.cfi_restore 6
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE803:
	.size	send_bio_chars, .-send_bio_chars
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"\\U%04lX"
.LC1:
	.string	"\\"
.LC2:
	.string	"\\%02X"
.LC3:
	.string	"\\\\"
	.text
	.p2align 4
	.type	do_esc_char.part.0, @function
do_esc_char.part.0:
.LFB818:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rcx, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$255, %rdi
	ja	.L52
	movl	%esi, %eax
	movb	%dil, -65(%rbp)
	andl	$4, %eax
	testb	%dil, %dil
	js	.L15
	movslq	%edi, %rax
	leaq	char_type(%rip), %rcx
	movzwl	(%rcx,%rax,2), %r9d
	andl	%esi, %r9d
	movl	%r9d, %eax
	testb	$97, %al
	je	.L16
	testb	$8, %al
	jne	.L53
	movl	$1, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r8, %rdi
	call	*%rbx
	testl	%eax, %eax
	je	.L19
	leaq	-65(%rbp), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	*%rbx
	testl	%eax, %eax
	je	.L19
.L23:
	movl	$2, %eax
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L16:
	andw	$1030, %ax
	.p2align 4,,10
	.p2align 3
.L15:
	testw	%ax, %ax
	jne	.L54
	cmpq	$92, %rdi
	je	.L55
.L22:
	leaq	-65(%rbp), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	*%rbx
	testl	%eax, %eax
	je	.L19
	movl	$1, %eax
.L11:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L56
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	leaq	-64(%rbp), %r13
	movq	%rdi, %rcx
	movl	$19, %esi
	leaq	.LC0(%rip), %rdx
	movq	%r13, %rdi
	call	BIO_snprintf@PLT
	movl	$6, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rbx
	movl	%eax, %r8d
	movl	$6, %eax
	testl	%r8d, %r8d
	jne	.L11
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L53:
	testq	%rdx, %rdx
	je	.L22
	movb	$1, (%rdx)
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L55:
	andl	$1039, %esi
	je	.L22
	movl	$2, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	*%rbx
	testl	%eax, %eax
	jne	.L23
	.p2align 4,,10
	.p2align 3
.L19:
	movl	$-1, %eax
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L54:
	leaq	-64(%rbp), %r13
	movl	%edi, %ecx
	leaq	.LC2(%rip), %rdx
	xorl	%eax, %eax
	movl	$11, %esi
	movq	%r13, %rdi
	call	BIO_snprintf@PLT
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rbx
	testl	%eax, %eax
	je	.L19
	movl	$3, %eax
	jmp	.L11
.L56:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE818:
	.size	do_esc_char.part.0, .-do_esc_char.part.0
	.p2align 4
	.type	send_fp_chars, @function
send_fp_chars:
.LFB804:
	.cfi_startproc
	endbr64
	movq	%rdi, %rcx
	testq	%rdi, %rdi
	je	.L61
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%edx, %ebx
	movslq	%edx, %rdx
	movl	%ebx, %ebx
	subq	$8, %rsp
	call	fwrite@PLT
	cmpq	%rax, %rbx
	sete	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore 3
	.cfi_restore 6
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE804:
	.size	send_fp_chars, .-send_fp_chars
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"../deps/openssl/openssl/crypto/asn1/a_strex.c"
	.section	.rodata.str1.1
.LC5:
	.string	"\\W%08lX"
	.text
	.p2align 4
	.type	do_buf, @function
do_buf:
.LFB806:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	andl	$7, %r13d
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -120(%rbp)
	movq	16(%rbp), %r14
	movl	%esi, -148(%rbp)
	movq	%r8, -144(%rbp)
	movq	%r9, -104(%rbp)
	movw	%cx, -132(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movslq	%esi, %rax
	addq	%rdi, %rax
	movq	%rax, -112(%rbp)
	cmpl	$2, %r13d
	je	.L67
	cmpl	$4, %r13d
	jne	.L68
	andl	$3, %esi
	jne	.L106
.L68:
	movq	-112(%rbp), %rsi
	xorl	%r12d, %r12d
	cmpq	%rsi, -120(%rbp)
	je	.L66
	movl	%ecx, %eax
	andl	$8, %edx
	movq	-120(%rbp), %r15
	xorl	%r12d, %r12d
	andl	$1, %eax
	movl	%edx, -136(%rbp)
	movw	%ax, -130(%rbp)
.L90:
	movq	-120(%rbp), %rax
	movzwl	-130(%rbp), %ebx
	movl	$0, %esi
	sall	$5, %ebx
	cmpq	%r15, %rax
	cmovne	%esi, %ebx
	cmpl	$2, %r13d
	je	.L73
	jg	.L74
	testl	%r13d, %r13d
	je	.L75
	cmpl	$1, %r13d
	jne	.L94
	movzbl	(%r15), %edi
	addq	$1, %r15
	movq	%rdi, -88(%rbp)
.L77:
	cmpq	-112(%rbp), %r15
	je	.L107
.L79:
	movl	-136(%rbp), %eax
	testl	%eax, %eax
	jne	.L108
.L80:
	movl	$4294967295, %eax
	cmpq	%rax, %rdi
	ja	.L94
	cmpq	$65535, %rdi
	ja	.L109
	orw	-132(%rbp), %bx
	movq	-104(%rbp), %rcx
	movq	%r14, %r8
	movq	-144(%rbp), %rdx
	movzwl	%bx, %esi
	call	do_esc_char.part.0
	testl	%eax, %eax
	js	.L94
.L88:
	addl	%eax, %r12d
.L84:
	cmpq	-112(%rbp), %r15
	jne	.L90
	.p2align 4,,10
	.p2align 3
.L66:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L110
	addq	$120, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	cmpl	$4, %r13d
	jne	.L94
	movl	(%r15), %edi
	addq	$4, %r15
	bswap	%edi
	movl	%edi, %edi
	movq	%rdi, -88(%rbp)
	cmpq	-112(%rbp), %r15
	jne	.L79
.L107:
	movl	$64, %eax
	cmpw	$0, -130(%rbp)
	cmovne	%eax, %ebx
	movl	-136(%rbp), %eax
	testl	%eax, %eax
	je	.L80
.L108:
	leaq	-80(%rbp), %rcx
	movq	%rdi, %rdx
	movl	$6, %esi
	movq	%rcx, %rdi
	movq	%rcx, -128(%rbp)
	call	UTF8_putc@PLT
	testl	%eax, %eax
	jle	.L84
	subl	$1, %eax
	movq	-128(%rbp), %rcx
	orw	-132(%rbp), %bx
	movq	%r15, -160(%rbp)
	leaq	-79(%rbp,%rax), %rax
	movzwl	%bx, %edx
	movl	%r13d, -152(%rbp)
	movq	-144(%rbp), %r15
	movq	%rax, -128(%rbp)
	movq	%r14, %rax
	movl	%edx, %r13d
	movq	%rcx, %r14
	movq	%rax, %rbx
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L82:
	addl	%eax, %r12d
	addq	$1, %r14
	cmpq	%r14, -128(%rbp)
	je	.L111
.L83:
	movzbl	(%r14), %edi
	movq	-104(%rbp), %rcx
	movq	%rbx, %r8
	movq	%r15, %rdx
	movl	%r13d, %esi
	call	do_esc_char.part.0
	testl	%eax, %eax
	jns	.L82
.L94:
	movl	$-1, %r12d
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L75:
	movl	-148(%rbp), %esi
	leaq	-88(%rbp), %rdx
	movq	%r15, %rdi
	call	UTF8_getc@PLT
	testl	%eax, %eax
	js	.L94
	subl	%eax, -148(%rbp)
	cltq
	movq	-88(%rbp), %rdi
	addq	%rax, %r15
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L73:
	movzwl	(%r15), %edi
	addq	$2, %r15
	rolw	$8, %di
	movzwl	%di, %edi
	movq	%rdi, -88(%rbp)
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L109:
	leaq	-80(%rbp), %rbx
	movq	%rdi, %rcx
	leaq	.LC5(%rip), %rdx
	xorl	%eax, %eax
	movl	$19, %esi
	movq	%rbx, %rdi
	call	BIO_snprintf@PLT
	movq	-104(%rbp), %rax
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movl	$10, %edx
	call	*%rax
	testl	%eax, %eax
	je	.L94
	movl	$10, %eax
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L67:
	testb	$1, -148(%rbp)
	je	.L68
	movl	$161, %r8d
	movl	$129, %edx
	movl	$142, %esi
	movl	$13, %edi
	leaq	.LC4(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L111:
	movq	-160(%rbp), %r15
	movl	-152(%rbp), %r13d
	movq	%rbx, %r14
	jmp	.L84
.L106:
	movl	$155, %r8d
	movl	$133, %edx
	movl	$142, %esi
	movl	$13, %edi
	leaq	.LC4(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L66
.L110:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE806:
	.size	do_buf, .-do_buf
	.section	.rodata.str1.1
.LC6:
	.string	" + "
.LC7:
	.string	"; "
.LC8:
	.string	", "
.LC9:
	.string	"+"
.LC10:
	.string	","
.LC11:
	.string	"\n"
.LC12:
	.string	" = "
.LC13:
	.string	"="
.LC14:
	.string	" "
.LC15:
	.string	":"
.LC16:
	.string	"#"
.LC17:
	.string	"\""
.LC18:
	.string	""
	.text
	.p2align 4
	.type	do_name_ex, @function
do_name_ex:
.LFB811:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	$0, %r13d
	pushq	%r12
	movl	%r13d, %r15d
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$296, %rsp
	movq	%rdi, -200(%rbp)
	movq	%rdx, -240(%rbp)
	movq	%r8, -232(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	cmovns	%ecx, %r15d
	movl	%r15d, -208(%rbp)
	jle	.L113
	leaq	.LC14(%rip), %r12
	movq	%rdi, %r14
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L114:
	addl	$1, %r13d
	cmpl	%r15d, %r13d
	jge	.L113
.L116:
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	*%r14
	testl	%eax, %eax
	jne	.L114
	.p2align 4,,10
	.p2align 3
.L249:
	movl	$-1, -208(%rbp)
.L112:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L252
	movl	-208(%rbp), %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore_state
	movq	-232(%rbp), %rax
	andl	$983040, %eax
	cmpq	$196608, %rax
	je	.L178
	ja	.L169
	cmpq	$65536, %rax
	je	.L179
	cmpq	$131072, %rax
	jne	.L249
	leaq	.LC6(%rip), %rax
	movl	$3, -292(%rbp)
	movq	%rax, -288(%rbp)
	leaq	.LC8(%rip), %rax
	movl	$2, -296(%rbp)
	movq	%rax, -312(%rbp)
	movl	$0, -268(%rbp)
.L168:
	movq	-232(%rbp), %r14
	leaq	.LC12(%rip), %rdx
	movq	%r14, %rax
	andl	$8388608, %eax
	cmpq	$1, %rax
	sbbl	%edi, %edi
	andl	$-2, %edi
	addl	$3, %edi
	testq	%rax, %rax
	leaq	.LC13(%rip), %rax
	cmovne	%rdx, %rax
	movl	%edi, -272(%rbp)
	movq	-240(%rbp), %rdi
	movq	%rax, -280(%rbp)
	movl	%r14d, %eax
	andl	$6291456, %eax
	movl	%eax, -216(%rbp)
	call	X509_NAME_entry_count@PLT
	testl	%eax, %eax
	jle	.L112
	movq	%r14, %rsi
	subl	$1, %eax
	andl	$1048576, %r14d
	movl	$-1, -212(%rbp)
	movl	%eax, -224(%rbp)
	movq	%rsi, %rdi
	movq	-200(%rbp), %r13
	movq	%r14, -248(%rbp)
	movq	%rsi, %r14
	andl	$64, %edi
	movl	%eax, -204(%rbp)
	movq	%rsi, %rax
	andl	$33554432, %r14d
	orb	$-128, %al
	movq	%r14, -320(%rbp)
	movq	%rsi, %r14
	movq	%rax, -328(%rbp)
	movl	%esi, %eax
	andl	$16777216, %r14d
	andw	$1039, %ax
	movq	%r14, -304(%rbp)
	movq	%rdi, -256(%rbp)
	movw	%ax, -330(%rbp)
	.p2align 4,,10
	.p2align 3
.L165:
	movl	-224(%rbp), %esi
	movl	-204(%rbp), %eax
	subl	%eax, %esi
	cmpq	$0, -248(%rbp)
	je	.L118
	movq	-240(%rbp), %rdi
	movl	%eax, %esi
	call	X509_NAME_get_entry@PLT
	movq	%rax, %r12
.L119:
	movl	-212(%rbp), %r14d
	cmpl	$-1, %r14d
	je	.L120
	movq	%r12, %rdi
	call	X509_NAME_ENTRY_set@PLT
	cmpl	%r14d, %eax
	jne	.L121
	movl	-292(%rbp), %r14d
	movq	-288(%rbp), %rsi
	movq	%rbx, %rdi
	movl	%r14d, %edx
	call	*%r13
	testl	%eax, %eax
	je	.L249
	addl	%r14d, -208(%rbp)
.L120:
	movq	%r12, %rdi
	call	X509_NAME_ENTRY_set@PLT
	movq	%r12, %rdi
	movl	%eax, -212(%rbp)
	call	X509_NAME_ENTRY_get_object@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	X509_NAME_ENTRY_get_data@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	OBJ_obj2nid@PLT
	movl	-216(%rbp), %ecx
	movl	%eax, -200(%rbp)
	cmpl	$6291456, %ecx
	je	.L125
	cmpl	$4194304, %ecx
	je	.L182
	testl	%eax, %eax
	je	.L182
	movl	-216(%rbp), %r9d
	testl	%r9d, %r9d
	je	.L253
	cmpl	$2097152, -216(%rbp)
	je	.L254
	xorl	%edx, %edx
	leaq	.LC18(%rip), %rsi
	movq	%rbx, %rdi
	call	*%r13
	testl	%eax, %eax
	je	.L249
	xorl	%r14d, %r14d
.L131:
	movl	-272(%rbp), %r15d
	movq	-280(%rbp), %rsi
	movq	%rbx, %rdi
	movl	%r15d, %edx
	call	*%r13
	testl	%eax, %eax
	je	.L249
	addl	%r15d, %r14d
	addl	%r14d, -208(%rbp)
.L125:
	movl	-200(%rbp), %r8d
	movl	4(%r12), %r14d
	testl	%r8d, %r8d
	jne	.L134
	cmpq	$0, -304(%rbp)
	je	.L134
	cmpq	$0, -256(%rbp)
	movb	$0, -185(%rbp)
	movq	-328(%rbp), %r15
	jne	.L255
.L139:
	movl	$1, %edx
	leaq	.LC16(%rip), %rsi
	movq	%rbx, %rdi
	call	*%r13
	testl	%eax, %eax
	je	.L249
	testl	$512, %r15d
	jne	.L146
	movl	(%r12), %eax
	movl	%eax, -220(%rbp)
	testq	%rbx, %rbx
	je	.L150
	movq	8(%r12), %r15
	movslq	-220(%rbp), %r12
	addq	%r15, %r12
	cmpq	%r12, %r15
	je	.L150
	leaq	-146(%rbp), %r14
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L256:
	addq	$1, %r15
	cmpq	%r15, %r12
	je	.L150
.L152:
	movzbl	(%r15), %eax
	leaq	hexdig.16953(%rip), %rcx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movl	%eax, %edx
	andl	$15, %eax
	shrb	$4, %dl
	movzbl	(%rcx,%rax), %eax
	andl	$15, %edx
	movzbl	(%rcx,%rdx), %edx
	movb	%al, -145(%rbp)
	movb	%dl, -146(%rbp)
	movl	$2, %edx
	call	*%r13
	testl	%eax, %eax
	jne	.L256
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L134:
	movq	-232(%rbp), %r15
	movb	$0, -185(%rbp)
	movl	$0, -200(%rbp)
	movl	%r15d, %eax
	andw	$1039, %ax
	cmpq	$0, -256(%rbp)
	movw	%ax, -220(%rbp)
	jne	.L170
.L136:
	testb	$-128, %r15b
	jne	.L139
	testb	$32, %r15b
	jne	.L142
	leal	-1(%r14), %eax
	cmpl	$29, %eax
	ja	.L251
	leaq	tag2nbyte(%rip), %rax
	movslq	%r14d, %r8
	movsbl	(%rax,%r8), %r10d
	movl	%r10d, %eax
	cmpb	$-1, %r10b
	je	.L251
	andl	$16, %r15d
	je	.L162
	testl	%r10d, %r10d
	jne	.L257
	movl	$1, %r10d
	.p2align 4,,10
	.p2align 3
.L162:
	subq	$8, %rsp
	movq	8(%r12), %rdi
	movl	(%r12), %esi
	movq	%r13, %r9
	pushq	$0
	movzwl	-220(%rbp), %r14d
	leaq	-185(%rbp), %r8
	movl	%r10d, %edx
	movl	%r10d, -220(%rbp)
	movl	%r14d, %ecx
	call	do_buf
	popq	%rsi
	popq	%rdi
	testl	%eax, %eax
	js	.L249
	movl	-200(%rbp), %r15d
	movl	-220(%rbp), %r10d
	addl	%eax, %r15d
	cmpb	$0, -185(%rbp)
	je	.L163
	movl	%r10d, -200(%rbp)
	addl	$2, %r15d
	testq	%rbx, %rbx
	je	.L164
	movl	$1, %edx
	leaq	.LC17(%rip), %rsi
	movq	%rbx, %rdi
	call	*%r13
	movl	-200(%rbp), %r10d
	testl	%eax, %eax
	je	.L249
.L166:
	subq	$8, %rsp
	movq	8(%r12), %rdi
	movl	(%r12), %esi
	movl	%r14d, %ecx
	pushq	%rbx
	movl	%r10d, %edx
	movq	%r13, %r9
	xorl	%r8d, %r8d
	call	do_buf
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	js	.L249
	cmpb	$0, -185(%rbp)
	je	.L164
	movl	$1, %edx
	leaq	.LC17(%rip), %rsi
	movq	%rbx, %rdi
	call	*%r13
	testl	%eax, %eax
	je	.L249
.L164:
	testl	%r15d, %r15d
	js	.L249
	subl	$1, -204(%rbp)
	movl	-204(%rbp), %eax
	addl	%r15d, -208(%rbp)
	cmpl	$-1, %eax
	jne	.L165
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L118:
	movq	-240(%rbp), %rdi
	call	X509_NAME_get_entry@PLT
	movq	%rax, %r12
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L251:
	testl	$256, %r15d
	jne	.L139
.L142:
	movq	%r15, %rcx
	andl	$16, %ecx
	cmpq	$1, %rcx
	sbbl	%r10d, %r10d
	andl	$-8, %r10d
	addl	$9, %r10d
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L255:
	movzwl	-330(%rbp), %eax
	movw	%ax, -220(%rbp)
.L170:
	movl	%r14d, %edi
	call	ASN1_tag2str@PLT
	movq	%rax, %rdi
	movq	%rax, -200(%rbp)
	call	strlen@PLT
	movq	-200(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rax, -264(%rbp)
	movl	%eax, %edx
	call	*%r13
	testl	%eax, %eax
	je	.L249
	movl	$1, %edx
	leaq	.LC15(%rip), %rsi
	movq	%rbx, %rdi
	call	*%r13
	testl	%eax, %eax
	je	.L249
	movl	-264(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -200(%rbp)
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L163:
	testq	%rbx, %rbx
	jne	.L166
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L121:
	movl	-296(%rbp), %r15d
	movq	-312(%rbp), %rsi
	movq	%rbx, %rdi
	movl	%r15d, %edx
	call	*%r13
	testl	%eax, %eax
	je	.L249
	movl	-208(%rbp), %r14d
	movl	-268(%rbp), %eax
	addl	%r15d, %r14d
	testl	%eax, %eax
	je	.L123
	xorl	%r15d, %r15d
	movq	%r12, -200(%rbp)
	movl	%r15d, %r12d
	movl	%eax, %r15d
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L259:
	addl	$1, %r12d
	cmpl	%r12d, %r15d
	je	.L258
.L124:
	movl	$1, %edx
	leaq	.LC14(%rip), %rsi
	movq	%rbx, %rdi
	call	*%r13
	testl	%eax, %eax
	jne	.L259
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L258:
	movq	-200(%rbp), %r12
.L123:
	addl	-268(%rbp), %r14d
	movl	%r14d, -208(%rbp)
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L182:
	leaq	-144(%rbp), %r15
	movl	$80, %esi
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	OBJ_obj2txt@PLT
	movq	%r15, %rsi
	movl	$0, -220(%rbp)
.L128:
	movq	%rsi, %rdi
	movq	%rsi, -264(%rbp)
	call	strlen@PLT
	movq	-264(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rax, %r15
	movl	%eax, %r14d
	movl	%eax, %edx
	call	*%r13
	testl	%eax, %eax
	je	.L249
	cmpl	%r15d, -220(%rbp)
	jle	.L131
	cmpq	$0, -320(%rbp)
	je	.L131
	movl	-220(%rbp), %eax
	movq	%r12, -264(%rbp)
	subl	%r15d, %eax
	xorl	%r15d, %r15d
	movl	%eax, -220(%rbp)
	movl	%r15d, %r12d
	movl	%eax, %r15d
	.p2align 4,,10
	.p2align 3
.L132:
	movl	$1, %edx
	leaq	.LC14(%rip), %rsi
	movq	%rbx, %rdi
	call	*%r13
	testl	%eax, %eax
	je	.L249
	addl	$1, %r12d
	cmpl	%r12d, %r15d
	jne	.L132
	movl	-220(%rbp), %esi
	movq	-264(%rbp), %r12
	addl	%esi, -208(%rbp)
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L146:
	movl	4(%r12), %eax
	leaq	-176(%rbp), %r14
	xorl	%esi, %esi
	movq	%r12, -168(%rbp)
	movq	%r14, %rdi
	movl	%eax, -176(%rbp)
	call	i2d_ASN1_TYPE@PLT
	movl	$283, %edx
	leaq	.LC4(%rip), %rsi
	movslq	%eax, %r15
	movl	%eax, -220(%rbp)
	movq	%r15, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L260
	leaq	-184(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, -184(%rbp)
	call	i2d_ASN1_TYPE@PLT
	testq	%rbx, %rbx
	je	.L158
	leaq	(%r12,%r15), %rax
	cmpq	%rax, %r12
	je	.L158
	movq	%r12, -264(%rbp)
	leaq	-146(%rbp), %r14
	movq	%rax, %r15
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L262:
	addq	$1, %r12
	cmpq	%r12, %r15
	je	.L261
.L160:
	movzbl	(%r12), %eax
	leaq	hexdig.16953(%rip), %rcx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movl	%eax, %edx
	andl	$15, %eax
	shrb	$4, %dl
	movzbl	(%rcx,%rax), %eax
	andl	$15, %edx
	movzbl	(%rcx,%rdx), %edx
	movb	%al, -145(%rbp)
	movb	%dl, -146(%rbp)
	movl	$2, %edx
	call	*%r13
	testl	%eax, %eax
	jne	.L262
	movq	-264(%rbp), %r12
	movl	$290, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L261:
	movq	-264(%rbp), %r12
.L158:
	movl	-220(%rbp), %r15d
	movl	$290, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	addl	%r15d, %r15d
	call	CRYPTO_free@PLT
	testl	%r15d, %r15d
	js	.L249
.L246:
	addl	$1, %r15d
	addl	-200(%rbp), %r15d
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L150:
	movl	-220(%rbp), %r15d
	addl	%r15d, %r15d
	jns	.L246
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L253:
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movl	$10, -220(%rbp)
	movq	%rax, %rsi
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L169:
	cmpq	$262144, %rax
	jne	.L249
	movl	$3, -292(%rbp)
	movl	-208(%rbp), %eax
	movl	$1, -296(%rbp)
	movl	%eax, -268(%rbp)
	leaq	.LC6(%rip), %rax
	movq	%rax, -288(%rbp)
	leaq	.LC11(%rip), %rax
	movq	%rax, -312(%rbp)
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L254:
	movl	%eax, %edi
	call	OBJ_nid2ln@PLT
	movl	$25, -220(%rbp)
	movq	%rax, %rsi
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L179:
	leaq	.LC9(%rip), %rax
	movl	$1, -292(%rbp)
	movq	%rax, -288(%rbp)
	leaq	.LC10(%rip), %rax
	movl	$1, -296(%rbp)
	movq	%rax, -312(%rbp)
	movl	$0, -268(%rbp)
	jmp	.L168
.L178:
	leaq	.LC6(%rip), %rax
	movl	$3, -292(%rbp)
	movq	%rax, -288(%rbp)
	leaq	.LC7(%rip), %rax
	movl	$2, -296(%rbp)
	movq	%rax, -312(%rbp)
	movl	$0, -268(%rbp)
	jmp	.L168
.L260:
	movl	$284, %r8d
	movl	$65, %edx
	movl	$125, %esi
	movl	$13, %edi
	leaq	.LC4(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L249
.L252:
	call	__stack_chk_fail@PLT
.L257:
	orl	$8, %eax
	movsbl	%al, %r10d
	jmp	.L162
	.cfi_endproc
.LFE811:
	.size	do_name_ex, .-do_name_ex
	.p2align 4
	.type	do_buf.constprop.1, @function
do_buf.constprop.1:
.LFB824:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	andl	$7, %r12d
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -128(%rbp)
	movl	%esi, -148(%rbp)
	movq	%r8, -160(%rbp)
	movq	%r9, -144(%rbp)
	movw	%cx, -132(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movslq	%esi, %rax
	addq	%rdi, %rax
	movq	%rax, -120(%rbp)
	cmpl	$2, %r12d
	je	.L264
	cmpl	$4, %r12d
	jne	.L265
	andl	$3, %esi
	jne	.L381
.L265:
	movq	-120(%rbp), %rdi
	xorl	%r15d, %r15d
	cmpq	%rdi, -128(%rbp)
	je	.L263
	movl	%ecx, %eax
	andl	$8, %edx
	movq	-128(%rbp), %r14
	xorl	%r15d, %r15d
	andl	$1, %eax
	movl	%edx, -136(%rbp)
	movl	%r15d, %r13d
	movw	%ax, -130(%rbp)
	leaq	-96(%rbp), %rax
	movq	%r14, %r15
	movq	%rax, -168(%rbp)
.L310:
	movq	-128(%rbp), %rax
	movzwl	-130(%rbp), %ebx
	sall	$5, %ebx
	cmpq	%r15, %rax
	movl	$0, %eax
	cmovne	%eax, %ebx
	cmpl	$2, %r12d
	je	.L270
	jg	.L271
	testl	%r12d, %r12d
	je	.L272
	cmpl	$1, %r12d
	jne	.L315
	movzbl	(%r15), %ecx
	addq	$1, %r15
	movq	%rcx, -96(%rbp)
.L274:
	cmpq	%r15, -120(%rbp)
	je	.L382
.L275:
	movl	-136(%rbp), %eax
	testl	%eax, %eax
	jne	.L383
.L276:
	movl	$4294967295, %eax
	cmpq	%rax, %rcx
	ja	.L315
	cmpq	$65535, %rcx
	ja	.L384
	cmpq	$255, %rcx
	ja	.L385
	orw	-132(%rbp), %bx
	movb	%cl, -97(%rbp)
	movl	%ebx, %eax
	andl	$4, %eax
	testb	%cl, %cl
	js	.L300
	movslq	%ecx, %rax
	leaq	char_type(%rip), %rdi
	movzwl	(%rdi,%rax,2), %edx
	andl	%ebx, %edx
	movl	%edx, %eax
	testb	$97, %al
	je	.L301
	testb	$8, %al
	jne	.L386
	movq	-144(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L306
	movl	$1, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rbx, %rdi
	call	BIO_write@PLT
	cmpl	$1, %eax
	jne	.L315
	leaq	-97(%rbp), %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	call	BIO_write@PLT
	cmpl	$1, %eax
	jne	.L315
.L306:
	movl	$2, %eax
	addl	%eax, %r13d
.L291:
	cmpq	%r15, -120(%rbp)
	jne	.L310
.L389:
	movl	%r13d, %r15d
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L271:
	cmpl	$4, %r12d
	jne	.L315
	movl	(%r15), %ecx
	addq	$4, %r15
	bswap	%ecx
	movl	%ecx, %ecx
	movq	%rcx, -96(%rbp)
	cmpq	%r15, -120(%rbp)
	jne	.L275
.L382:
	movl	$64, %eax
	cmpw	$0, -130(%rbp)
	cmovne	%eax, %ebx
	movl	-136(%rbp), %eax
	testl	%eax, %eax
	je	.L276
.L383:
	leaq	-86(%rbp), %r14
	movq	%rcx, %rdx
	movl	$6, %esi
	movq	%r14, %rdi
	call	UTF8_putc@PLT
	testl	%eax, %eax
	jle	.L291
	orw	-132(%rbp), %bx
	movl	%r12d, -172(%rbp)
	subl	$1, %eax
	movl	%ebx, %esi
	movl	%ebx, %edi
	movw	%bx, -150(%rbp)
	movq	%r14, %rbx
	andw	$1039, %di
	andl	$4, %esi
	leaq	-85(%rbp,%rax), %r14
	movq	%rbx, %r12
	movw	%di, -174(%rbp)
	movl	%r13d, %ebx
	movq	-144(%rbp), %r13
	movw	%si, -152(%rbp)
	.p2align 4,,10
	.p2align 3
.L290:
	movzbl	(%r12), %eax
	movb	%al, -97(%rbp)
	testb	%al, %al
	js	.L387
	movzbl	%al, %edx
	leaq	char_type(%rip), %rsi
	movzwl	-150(%rbp), %edi
	movzbl	%al, %ecx
	andw	(%rsi,%rdx,2), %di
	movl	%edi, %edx
	testb	$97, %dil
	je	.L281
	andl	$8, %edx
	je	.L282
	movq	-160(%rbp), %rax
	testq	%rax, %rax
	je	.L280
	movb	$1, (%rax)
.L280:
	testq	%r13, %r13
	je	.L284
	leaq	-97(%rbp), %rsi
	movl	$1, %edx
	movq	%r13, %rdi
	call	BIO_write@PLT
	cmpl	$1, %eax
	je	.L284
	.p2align 4,,10
	.p2align 3
.L315:
	movl	$-1, %r15d
.L263:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L388
	addq	$152, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L284:
	.cfi_restore_state
	movl	$1, %eax
.L285:
	addq	$1, %r12
	addl	%eax, %ebx
	cmpq	%r12, %r14
	jne	.L290
	movl	-172(%rbp), %r12d
	movl	%ebx, %r13d
	cmpq	%r15, -120(%rbp)
	jne	.L310
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L387:
	cmpw	$0, -152(%rbp)
	movzbl	%al, %ecx
	je	.L280
.L279:
	leaq	-80(%rbp), %r11
	xorl	%eax, %eax
	movl	$11, %esi
	leaq	.LC2(%rip), %rdx
	movq	%r11, %rdi
	movq	%r11, -184(%rbp)
	call	BIO_snprintf@PLT
	testq	%r13, %r13
	je	.L289
	movq	-184(%rbp), %r11
	movl	$3, %edx
	movq	%r13, %rdi
	movq	%r11, %rsi
	call	BIO_write@PLT
	cmpl	$3, %eax
	jne	.L315
.L289:
	movl	$3, %eax
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L282:
	testq	%r13, %r13
	je	.L286
	movl	$1, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_write@PLT
	cmpl	$1, %eax
	jne	.L315
	leaq	-97(%rbp), %rsi
	movl	$1, %edx
	movq	%r13, %rdi
	call	BIO_write@PLT
	cmpl	$1, %eax
	jne	.L315
.L286:
	movl	$2, %eax
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L270:
	movzwl	(%r15), %ecx
	addq	$2, %r15
	rolw	$8, %cx
	movzwl	%cx, %ecx
	movq	%rcx, -96(%rbp)
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L272:
	movl	-148(%rbp), %r14d
	movq	-168(%rbp), %rdx
	movq	%r15, %rdi
	movl	%r14d, %esi
	call	UTF8_getc@PLT
	testl	%eax, %eax
	js	.L315
	subl	%eax, %r14d
	cltq
	movq	-96(%rbp), %rcx
	movl	%r14d, -148(%rbp)
	addq	%rax, %r15
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L384:
	leaq	-80(%rbp), %r14
	leaq	.LC5(%rip), %rdx
	movl	$19, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	call	BIO_snprintf@PLT
	movq	-144(%rbp), %rax
	testq	%rax, %rax
	je	.L294
	movl	$10, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	BIO_write@PLT
	cmpl	$10, %eax
	jne	.L315
.L294:
	movl	$10, %eax
.L296:
	addl	%eax, %r13d
	cmpq	%r15, -120(%rbp)
	jne	.L310
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L301:
	andw	$1030, %ax
	.p2align 4,,10
	.p2align 3
.L300:
	testw	%ax, %ax
	jne	.L390
	cmpq	$92, %rcx
	je	.L391
.L309:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L304
	leaq	-97(%rbp), %rsi
	movl	$1, %edx
	call	BIO_write@PLT
	cmpl	$1, %eax
	jne	.L315
.L304:
	movl	$1, %eax
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L385:
	leaq	-80(%rbp), %r14
	leaq	.LC0(%rip), %rdx
	movl	$19, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	call	BIO_snprintf@PLT
	movq	-144(%rbp), %rax
	testq	%rax, %rax
	je	.L298
	movl	$6, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	BIO_write@PLT
	cmpl	$6, %eax
	jne	.L315
.L298:
	movl	$6, %eax
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L264:
	testb	$1, -148(%rbp)
	je	.L265
	movl	$161, %r8d
	movl	$129, %edx
	movl	$142, %esi
	movl	$13, %edi
	leaq	.LC4(%rip), %rcx
	movl	$-1, %r15d
	call	ERR_put_error@PLT
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L281:
	testw	$1030, %di
	jne	.L279
	cmpb	$92, %al
	jne	.L280
	cmpw	$0, -174(%rbp)
	je	.L280
	testq	%r13, %r13
	je	.L286
	movl	$2, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_write@PLT
	cmpl	$2, %eax
	jne	.L315
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L386:
	movq	-160(%rbp), %rax
	testq	%rax, %rax
	je	.L309
	movb	$1, (%rax)
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L391:
	testw	$1039, %bx
	je	.L309
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L306
	movl	$2, %edx
	leaq	.LC3(%rip), %rsi
	call	BIO_write@PLT
	cmpl	$2, %eax
	jne	.L315
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L390:
	leaq	-80(%rbp), %r14
	leaq	.LC2(%rip), %rdx
	movl	$11, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	call	BIO_snprintf@PLT
	movq	-144(%rbp), %rax
	testq	%rax, %rax
	je	.L308
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	BIO_write@PLT
	cmpl	$3, %eax
	jne	.L315
.L308:
	movl	$3, %eax
	jmp	.L296
.L381:
	movl	$155, %r8d
	movl	$133, %edx
	movl	$142, %esi
	movl	$13, %edi
	leaq	.LC4(%rip), %rcx
	movl	$-1, %r15d
	call	ERR_put_error@PLT
	jmp	.L263
.L388:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE824:
	.size	do_buf.constprop.1, .-do_buf.constprop.1
	.p2align 4
	.type	do_buf.constprop.0, @function
do_buf.constprop.0:
.LFB825:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	andl	$7, %r12d
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -128(%rbp)
	movl	%esi, -148(%rbp)
	movq	%r8, -160(%rbp)
	movq	%r9, -144(%rbp)
	movw	%cx, -132(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movslq	%esi, %rax
	addq	%rdi, %rax
	movq	%rax, -120(%rbp)
	cmpl	$2, %r12d
	je	.L393
	cmpl	$4, %r12d
	jne	.L394
	andl	$3, %esi
	jne	.L510
.L394:
	movq	-120(%rbp), %rdi
	xorl	%r15d, %r15d
	cmpq	%rdi, -128(%rbp)
	je	.L392
	movl	%ecx, %eax
	andl	$8, %edx
	movq	-128(%rbp), %r14
	xorl	%r15d, %r15d
	andl	$1, %eax
	movl	%edx, -136(%rbp)
	movl	%r15d, %r13d
	movw	%ax, -130(%rbp)
	leaq	-96(%rbp), %rax
	movq	%r14, %r15
	movq	%rax, -168(%rbp)
.L439:
	movq	-128(%rbp), %rax
	movzwl	-130(%rbp), %ebx
	sall	$5, %ebx
	cmpq	%r15, %rax
	movl	$0, %eax
	cmovne	%eax, %ebx
	cmpl	$2, %r12d
	je	.L399
	jg	.L400
	testl	%r12d, %r12d
	je	.L401
	cmpl	$1, %r12d
	jne	.L444
	movzbl	(%r15), %ecx
	addq	$1, %r15
	movq	%rcx, -96(%rbp)
.L403:
	cmpq	%r15, -120(%rbp)
	je	.L511
.L404:
	movl	-136(%rbp), %eax
	testl	%eax, %eax
	jne	.L512
.L405:
	movl	$4294967295, %eax
	cmpq	%rax, %rcx
	ja	.L444
	cmpq	$65535, %rcx
	ja	.L513
	cmpq	$255, %rcx
	ja	.L514
	orw	-132(%rbp), %bx
	movb	%cl, -97(%rbp)
	movl	%ebx, %eax
	andl	$4, %eax
	testb	%cl, %cl
	js	.L429
	movslq	%ecx, %rax
	leaq	char_type(%rip), %rdi
	movzwl	(%rdi,%rax,2), %edx
	andl	%ebx, %edx
	movl	%edx, %eax
	testb	$97, %al
	je	.L430
	testb	$8, %al
	jne	.L515
	movq	-144(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L435
	movq	%rbx, %rcx
	movl	$1, %edx
	movl	$1, %esi
	leaq	.LC1(%rip), %rdi
	call	fwrite@PLT
	cmpq	$1, %rax
	jne	.L444
	leaq	-97(%rbp), %rdi
	movq	%rbx, %rcx
	movl	$1, %edx
	movl	$1, %esi
	call	fwrite@PLT
	cmpq	$1, %rax
	jne	.L444
.L435:
	movl	$2, %eax
	addl	%eax, %r13d
.L420:
	cmpq	%r15, -120(%rbp)
	jne	.L439
.L518:
	movl	%r13d, %r15d
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L400:
	cmpl	$4, %r12d
	jne	.L444
	movl	(%r15), %ecx
	addq	$4, %r15
	bswap	%ecx
	movl	%ecx, %ecx
	movq	%rcx, -96(%rbp)
	cmpq	%r15, -120(%rbp)
	jne	.L404
.L511:
	movl	$64, %eax
	cmpw	$0, -130(%rbp)
	cmovne	%eax, %ebx
	movl	-136(%rbp), %eax
	testl	%eax, %eax
	je	.L405
.L512:
	leaq	-86(%rbp), %r14
	movq	%rcx, %rdx
	movl	$6, %esi
	movq	%r14, %rdi
	call	UTF8_putc@PLT
	testl	%eax, %eax
	jle	.L420
	orw	-132(%rbp), %bx
	movl	%r12d, -172(%rbp)
	subl	$1, %eax
	movl	%ebx, %esi
	movl	%ebx, %edi
	movw	%bx, -150(%rbp)
	movq	%r14, %rbx
	andw	$1039, %di
	andl	$4, %esi
	leaq	-85(%rbp,%rax), %r14
	movq	%rbx, %r12
	movw	%di, -174(%rbp)
	movl	%r13d, %ebx
	movq	-144(%rbp), %r13
	movw	%si, -152(%rbp)
	.p2align 4,,10
	.p2align 3
.L419:
	movzbl	(%r12), %eax
	movb	%al, -97(%rbp)
	testb	%al, %al
	js	.L516
	movzbl	%al, %edx
	leaq	char_type(%rip), %rdi
	movzwl	-150(%rbp), %esi
	movzbl	%al, %ecx
	andw	(%rdi,%rdx,2), %si
	movl	%esi, %edx
	testb	$97, %sil
	je	.L410
	andl	$8, %edx
	je	.L411
	movq	-160(%rbp), %rax
	testq	%rax, %rax
	je	.L409
	movb	$1, (%rax)
.L409:
	testq	%r13, %r13
	je	.L413
	leaq	-97(%rbp), %rdi
	movq	%r13, %rcx
	movl	$1, %edx
	movl	$1, %esi
	call	fwrite@PLT
	cmpq	$1, %rax
	je	.L413
	.p2align 4,,10
	.p2align 3
.L444:
	movl	$-1, %r15d
.L392:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L517
	addq	$152, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L413:
	.cfi_restore_state
	movl	$1, %eax
.L414:
	addq	$1, %r12
	addl	%eax, %ebx
	cmpq	%r12, %r14
	jne	.L419
	movl	-172(%rbp), %r12d
	movl	%ebx, %r13d
	cmpq	%r15, -120(%rbp)
	jne	.L439
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L516:
	cmpw	$0, -152(%rbp)
	movzbl	%al, %ecx
	je	.L409
.L408:
	leaq	-80(%rbp), %rdi
	xorl	%eax, %eax
	movl	$11, %esi
	leaq	.LC2(%rip), %rdx
	movq	%rdi, -184(%rbp)
	call	BIO_snprintf@PLT
	testq	%r13, %r13
	je	.L418
	movq	-184(%rbp), %rdi
	movq	%r13, %rcx
	movl	$3, %edx
	movl	$1, %esi
	call	fwrite@PLT
	cmpq	$3, %rax
	jne	.L444
.L418:
	movl	$3, %eax
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L411:
	testq	%r13, %r13
	je	.L415
	movq	%r13, %rcx
	movl	$1, %edx
	movl	$1, %esi
	leaq	.LC1(%rip), %rdi
	call	fwrite@PLT
	cmpq	$1, %rax
	jne	.L444
	leaq	-97(%rbp), %rdi
	movq	%r13, %rcx
	movl	$1, %edx
	movl	$1, %esi
	call	fwrite@PLT
	cmpq	$1, %rax
	jne	.L444
.L415:
	movl	$2, %eax
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L399:
	movzwl	(%r15), %ecx
	addq	$2, %r15
	rolw	$8, %cx
	movzwl	%cx, %ecx
	movq	%rcx, -96(%rbp)
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L401:
	movl	-148(%rbp), %r14d
	movq	-168(%rbp), %rdx
	movq	%r15, %rdi
	movl	%r14d, %esi
	call	UTF8_getc@PLT
	testl	%eax, %eax
	js	.L444
	subl	%eax, %r14d
	cltq
	movq	-96(%rbp), %rcx
	movl	%r14d, -148(%rbp)
	addq	%rax, %r15
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L513:
	leaq	-80(%rbp), %r14
	leaq	.LC5(%rip), %rdx
	movl	$19, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	call	BIO_snprintf@PLT
	movq	-144(%rbp), %rax
	testq	%rax, %rax
	je	.L423
	movq	%rax, %rcx
	movl	$10, %edx
	movl	$1, %esi
	movq	%r14, %rdi
	call	fwrite@PLT
	cmpq	$10, %rax
	jne	.L444
.L423:
	movl	$10, %eax
.L425:
	addl	%eax, %r13d
	cmpq	%r15, -120(%rbp)
	jne	.L439
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L430:
	andw	$1030, %ax
	.p2align 4,,10
	.p2align 3
.L429:
	testw	%ax, %ax
	jne	.L519
	cmpq	$92, %rcx
	je	.L520
.L438:
	movq	-144(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L433
	leaq	-97(%rbp), %rdi
	movl	$1, %edx
	movl	$1, %esi
	call	fwrite@PLT
	cmpq	$1, %rax
	jne	.L444
.L433:
	movl	$1, %eax
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L514:
	leaq	-80(%rbp), %r14
	leaq	.LC0(%rip), %rdx
	movl	$19, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	call	BIO_snprintf@PLT
	movq	-144(%rbp), %rax
	testq	%rax, %rax
	je	.L427
	movq	%rax, %rcx
	movl	$6, %edx
	movl	$1, %esi
	movq	%r14, %rdi
	call	fwrite@PLT
	cmpq	$6, %rax
	jne	.L444
.L427:
	movl	$6, %eax
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L393:
	testb	$1, -148(%rbp)
	je	.L394
	movl	$161, %r8d
	movl	$129, %edx
	movl	$142, %esi
	movl	$13, %edi
	leaq	.LC4(%rip), %rcx
	movl	$-1, %r15d
	call	ERR_put_error@PLT
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L410:
	testw	$1030, %si
	jne	.L408
	cmpb	$92, %al
	jne	.L409
	cmpw	$0, -174(%rbp)
	je	.L409
	testq	%r13, %r13
	je	.L415
	movq	%r13, %rcx
	movl	$2, %edx
	movl	$1, %esi
	leaq	.LC3(%rip), %rdi
	call	fwrite@PLT
	cmpq	$2, %rax
	jne	.L444
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L515:
	movq	-160(%rbp), %rax
	testq	%rax, %rax
	je	.L438
	movb	$1, (%rax)
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L520:
	testw	$1039, %bx
	je	.L438
	movq	-144(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L435
	movl	$2, %edx
	movl	$1, %esi
	leaq	.LC3(%rip), %rdi
	call	fwrite@PLT
	cmpq	$2, %rax
	jne	.L444
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L519:
	leaq	-80(%rbp), %r14
	leaq	.LC2(%rip), %rdx
	movl	$11, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	call	BIO_snprintf@PLT
	movq	-144(%rbp), %rax
	testq	%rax, %rax
	je	.L437
	movq	%rax, %rcx
	movl	$3, %edx
	movl	$1, %esi
	movq	%r14, %rdi
	call	fwrite@PLT
	cmpq	$3, %rax
	jne	.L444
.L437:
	movl	$3, %eax
	jmp	.L425
.L510:
	movl	$155, %r8d
	movl	$133, %edx
	movl	$142, %esi
	movl	$13, %edi
	leaq	.LC4(%rip), %rcx
	movl	$-1, %r15d
	call	ERR_put_error@PLT
	jmp	.L392
.L517:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE825:
	.size	do_buf.constprop.0, .-do_buf.constprop.0
	.p2align 4
	.globl	X509_NAME_print_ex
	.type	X509_NAME_print_ex, @function
X509_NAME_print_ex:
.LFB812:
	.cfi_startproc
	endbr64
	movq	%rcx, %r8
	testq	%rcx, %rcx
	je	.L523
	movl	%edx, %ecx
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	send_bio_chars(%rip), %rdi
	jmp	do_name_ex
	.p2align 4,,10
	.p2align 3
.L523:
	jmp	X509_NAME_print@PLT
	.cfi_endproc
.LFE812:
	.size	X509_NAME_print_ex, .-X509_NAME_print_ex
	.p2align 4
	.globl	X509_NAME_print_ex_fp
	.type	X509_NAME_print_ex_fp, @function
X509_NAME_print_ex_fp:
.LFB813:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$8, %rsp
	testq	%rcx, %rcx
	jne	.L525
	xorl	%esi, %esi
	call	BIO_new_fp@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L527
	movq	%r12, %rsi
	movq	%rax, %rdi
	movl	%r13d, %edx
	call	X509_NAME_print@PLT
	movq	%r14, %rdi
	movl	%eax, %r12d
	call	BIO_free@PLT
.L524:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L525:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%rcx, %r8
	movl	%edx, %ecx
	movq	%rsi, %rdx
	popq	%r12
	movq	%rdi, %rsi
	leaq	send_fp_chars(%rip), %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	do_name_ex
.L527:
	.cfi_restore_state
	movl	$-1, %r12d
	jmp	.L524
	.cfi_endproc
.LFE813:
	.size	X509_NAME_print_ex_fp, .-X509_NAME_print_ex_fp
	.p2align 4
	.globl	ASN1_STRING_print_ex
	.type	ASN1_STRING_print_ex, @function
ASN1_STRING_print_ex:
.LFB814:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movl	4(%rsi), %r14d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	andl	$64, %edx
	movb	$0, -89(%rbp)
	jne	.L604
	xorl	%ebx, %ebx
.L530:
	testb	$-128, %r13b
	jne	.L534
	testb	$32, %r13b
	je	.L605
.L535:
	movq	%r13, %rax
	andl	$16, %eax
	cmpq	$1, %rax
	sbbl	%r10d, %r10d
	andl	$-8, %r10d
	addl	$9, %r10d
.L554:
	andl	$1039, %r13d
	movl	(%r15), %esi
	xorl	%r9d, %r9d
	movl	%r10d, %edx
	movq	8(%r15), %rdi
	leaq	-89(%rbp), %r8
	movl	%r13d, %ecx
	movl	%r10d, -104(%rbp)
	call	do_buf.constprop.1
	testl	%eax, %eax
	js	.L603
	cmpb	$0, -89(%rbp)
	movl	-104(%rbp), %r10d
	leal	(%rbx,%rax), %r14d
	je	.L555
	addl	$2, %r14d
	testq	%r12, %r12
	je	.L529
	movl	$1, %edx
	leaq	.LC17(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	movl	-104(%rbp), %r10d
	cmpl	$1, %eax
	je	.L557
	.p2align 4,,10
	.p2align 3
.L603:
	movl	$-1, %r14d
.L529:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L606
	addq	$88, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L605:
	.cfi_restore_state
	leal	-1(%r14), %eax
	cmpl	$29, %eax
	ja	.L537
	movslq	%r14d, %rax
	leaq	tag2nbyte(%rip), %rdx
	movsbl	(%rdx,%rax), %r10d
	movl	%r10d, %eax
	cmpb	$-1, %r10b
	je	.L537
	testb	$16, %r13b
	je	.L554
	orl	$8, %eax
	testl	%r10d, %r10d
	movl	$1, %r10d
	movsbl	%al, %eax
	cmovne	%eax, %r10d
	jmp	.L554
	.p2align 4,,10
	.p2align 3
.L537:
	testl	$256, %r13d
	je	.L535
.L534:
	testq	%r12, %r12
	je	.L539
	movl	$1, %edx
	leaq	.LC16(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	cmpl	$1, %eax
	jne	.L603
	testl	$512, %r13d
	jne	.L607
	movslq	(%r15), %rax
	movq	8(%r15), %r15
	leaq	hexdig.16953(%rip), %r14
	leaq	-58(%rbp), %r13
	movl	%eax, -112(%rbp)
	addq	%r15, %rax
	movq	%rax, -104(%rbp)
	cmpq	%r15, %rax
	jne	.L547
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L608:
	addq	$1, %r15
	cmpq	%r15, -104(%rbp)
	je	.L556
.L547:
	movzbl	(%r15), %eax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %edx
	andl	$15, %eax
	shrb	$4, %dl
	movzbl	(%r14,%rax), %eax
	andl	$15, %edx
	movzbl	(%r14,%rdx), %edx
	movb	%al, -57(%rbp)
	movb	%dl, -58(%rbp)
	movl	$2, %edx
	call	BIO_write@PLT
	cmpl	$2, %eax
	jne	.L603
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L555:
	testq	%r12, %r12
	je	.L529
.L557:
	movq	8(%r15), %rdi
	movl	(%r15), %esi
	xorl	%r8d, %r8d
	movq	%r12, %r9
	movl	%r13d, %ecx
	movl	%r10d, %edx
	call	do_buf.constprop.1
	testl	%eax, %eax
	js	.L603
	cmpb	$0, -89(%rbp)
	je	.L529
	movl	$1, %edx
	leaq	.LC17(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	cmpl	$1, %eax
	je	.L529
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L604:
	movl	%r14d, %edi
	call	ASN1_tag2str@PLT
	movq	%rax, %rdi
	movq	%rax, -104(%rbp)
	call	strlen@PLT
	testq	%r12, %r12
	movq	-104(%rbp), %rsi
	movq	%rax, %rbx
	je	.L533
	movl	%eax, %edx
	movq	%r12, %rdi
	call	BIO_write@PLT
	cmpl	%ebx, %eax
	jne	.L603
	movl	$1, %edx
	leaq	.LC15(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	cmpl	$1, %eax
	jne	.L603
.L533:
	addl	$1, %ebx
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L539:
	testl	$512, %r13d
	jne	.L597
	movl	(%r15), %eax
	movl	%eax, -112(%rbp)
.L556:
	movl	-112(%rbp), %r14d
	addl	%r14d, %r14d
	js	.L603
.L602:
	addl	$1, %r14d
	addl	%ebx, %r14d
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L597:
	movl	4(%r15), %eax
	leaq	-80(%rbp), %r12
	xorl	%esi, %esi
	movq	%r15, -72(%rbp)
	movq	%r12, %rdi
	movl	%eax, -80(%rbp)
	call	i2d_ASN1_TYPE@PLT
	movl	$283, %edx
	leaq	.LC4(%rip), %rsi
	movslq	%eax, %rdi
	movl	%eax, -116(%rbp)
	call	CRYPTO_malloc@PLT
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L560
	leaq	-88(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -88(%rbp)
	call	i2d_ASN1_TYPE@PLT
.L559:
	movl	-116(%rbp), %r14d
	movq	-112(%rbp), %rdi
	movl	$290, %edx
	leaq	.LC4(%rip), %rsi
	addl	%r14d, %r14d
	call	CRYPTO_free@PLT
	testl	%r14d, %r14d
	jns	.L602
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L560:
	movl	$284, %r8d
	movl	$65, %edx
	movl	$125, %esi
	movl	$13, %edi
	leaq	.LC4(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L603
.L606:
	call	__stack_chk_fail@PLT
.L607:
	movl	4(%r15), %eax
	leaq	-80(%rbp), %r13
	xorl	%esi, %esi
	movq	%r15, -72(%rbp)
	movq	%r13, %rdi
	movl	%eax, -80(%rbp)
	call	i2d_ASN1_TYPE@PLT
	movl	$283, %edx
	leaq	.LC4(%rip), %rsi
	movslq	%eax, %r14
	movl	%eax, -116(%rbp)
	movq	%r14, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L560
	movq	%rax, %r15
	movq	%r13, %rdi
	leaq	-88(%rbp), %rsi
	movq	%rax, -88(%rbp)
	call	i2d_ASN1_TYPE@PLT
	addq	%r15, %r14
	leaq	-58(%rbp), %r13
	movq	%r14, %rcx
	movq	%r14, -104(%rbp)
	leaq	hexdig.16953(%rip), %r14
	cmpq	%r15, %rcx
	je	.L559
	.p2align 4,,10
	.p2align 3
.L551:
	movzbl	(%r15), %eax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %edx
	andl	$15, %eax
	shrb	$4, %dl
	movzbl	(%r14,%rax), %eax
	andl	$15, %edx
	movzbl	(%r14,%rdx), %edx
	movb	%al, -57(%rbp)
	movb	%dl, -58(%rbp)
	movl	$2, %edx
	call	BIO_write@PLT
	cmpl	$2, %eax
	je	.L609
	movq	-112(%rbp), %rdi
	movl	$290, %edx
	leaq	.LC4(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L609:
	addq	$1, %r15
	cmpq	%r15, -104(%rbp)
	jne	.L551
	jmp	.L559
	.cfi_endproc
.LFE814:
	.size	ASN1_STRING_print_ex, .-ASN1_STRING_print_ex
	.p2align 4
	.globl	ASN1_STRING_print_ex_fp
	.type	ASN1_STRING_print_ex_fp, @function
ASN1_STRING_print_ex_fp:
.LFB815:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movl	4(%rsi), %r14d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	andl	$64, %edx
	movb	$0, -89(%rbp)
	jne	.L685
	xorl	%ebx, %ebx
.L611:
	testb	$-128, %r13b
	jne	.L615
	testb	$32, %r13b
	je	.L686
.L616:
	movq	%r13, %rax
	andl	$16, %eax
	cmpq	$1, %rax
	sbbl	%r10d, %r10d
	andl	$-8, %r10d
	addl	$9, %r10d
.L635:
	andl	$1039, %r13d
	movl	(%r15), %esi
	xorl	%r9d, %r9d
	movl	%r10d, %edx
	movq	8(%r15), %rdi
	leaq	-89(%rbp), %r8
	movl	%r13d, %ecx
	movl	%r10d, -104(%rbp)
	call	do_buf.constprop.0
	testl	%eax, %eax
	js	.L684
	cmpb	$0, -89(%rbp)
	movl	-104(%rbp), %r10d
	leal	(%rbx,%rax), %r14d
	je	.L636
	addl	$2, %r14d
	testq	%r12, %r12
	je	.L610
	movq	%r12, %rcx
	movl	$1, %edx
	movl	$1, %esi
	leaq	.LC17(%rip), %rdi
	call	fwrite@PLT
	movl	-104(%rbp), %r10d
	cmpq	$1, %rax
	je	.L638
	.p2align 4,,10
	.p2align 3
.L684:
	movl	$-1, %r14d
.L610:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L687
	addq	$88, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L686:
	.cfi_restore_state
	leal	-1(%r14), %eax
	cmpl	$29, %eax
	ja	.L618
	movslq	%r14d, %rax
	leaq	tag2nbyte(%rip), %rdx
	movsbl	(%rdx,%rax), %r10d
	movl	%r10d, %eax
	cmpb	$-1, %r10b
	je	.L618
	testb	$16, %r13b
	je	.L635
	orl	$8, %eax
	testl	%r10d, %r10d
	movl	$1, %r10d
	movsbl	%al, %eax
	cmovne	%eax, %r10d
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L618:
	testl	$256, %r13d
	je	.L616
.L615:
	testq	%r12, %r12
	je	.L620
	movq	%r12, %rcx
	movl	$1, %edx
	movl	$1, %esi
	leaq	.LC16(%rip), %rdi
	call	fwrite@PLT
	cmpq	$1, %rax
	jne	.L684
	testl	$512, %r13d
	jne	.L688
	movslq	(%r15), %rax
	movq	8(%r15), %r15
	leaq	hexdig.16953(%rip), %r14
	leaq	-58(%rbp), %r13
	movl	%eax, -112(%rbp)
	addq	%r15, %rax
	movq	%rax, -104(%rbp)
	cmpq	%r15, %rax
	jne	.L628
	jmp	.L637
	.p2align 4,,10
	.p2align 3
.L689:
	addq	$1, %r15
	cmpq	%r15, -104(%rbp)
	je	.L637
.L628:
	movzbl	(%r15), %eax
	movq	%r12, %rcx
	movl	$1, %esi
	movq	%r13, %rdi
	movl	%eax, %edx
	andl	$15, %eax
	shrb	$4, %dl
	movzbl	(%r14,%rax), %eax
	andl	$15, %edx
	movzbl	(%r14,%rdx), %edx
	movb	%al, -57(%rbp)
	movb	%dl, -58(%rbp)
	movl	$2, %edx
	call	fwrite@PLT
	cmpq	$2, %rax
	jne	.L684
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L636:
	testq	%r12, %r12
	je	.L610
.L638:
	movq	8(%r15), %rdi
	movl	(%r15), %esi
	xorl	%r8d, %r8d
	movq	%r12, %r9
	movl	%r13d, %ecx
	movl	%r10d, %edx
	call	do_buf.constprop.0
	testl	%eax, %eax
	js	.L684
	cmpb	$0, -89(%rbp)
	je	.L610
	movq	%r12, %rcx
	movl	$1, %edx
	movl	$1, %esi
	leaq	.LC17(%rip), %rdi
	call	fwrite@PLT
	cmpq	$1, %rax
	je	.L610
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L685:
	movl	%r14d, %edi
	call	ASN1_tag2str@PLT
	movq	%rax, %rdi
	movq	%rax, -104(%rbp)
	call	strlen@PLT
	testq	%r12, %r12
	movq	-104(%rbp), %rdi
	movq	%rax, %rbx
	je	.L614
	movslq	%eax, %rdx
	movq	%r12, %rcx
	movl	$1, %esi
	call	fwrite@PLT
	movq	%rax, %r8
	movl	%ebx, %eax
	cmpq	%rax, %r8
	jne	.L684
	movq	%r12, %rcx
	movl	$1, %edx
	movl	$1, %esi
	leaq	.LC15(%rip), %rdi
	call	fwrite@PLT
	cmpq	$1, %rax
	jne	.L684
.L614:
	addl	$1, %ebx
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L620:
	testl	$512, %r13d
	jne	.L678
	movl	(%r15), %eax
	movl	%eax, -112(%rbp)
.L637:
	movl	-112(%rbp), %r14d
	addl	%r14d, %r14d
	js	.L684
.L683:
	addl	$1, %r14d
	addl	%ebx, %r14d
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L678:
	movl	4(%r15), %eax
	leaq	-80(%rbp), %r12
	xorl	%esi, %esi
	movq	%r15, -72(%rbp)
	movq	%r12, %rdi
	movl	%eax, -80(%rbp)
	call	i2d_ASN1_TYPE@PLT
	movl	$283, %edx
	leaq	.LC4(%rip), %rsi
	movslq	%eax, %rdi
	movl	%eax, -116(%rbp)
	call	CRYPTO_malloc@PLT
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L641
	leaq	-88(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -88(%rbp)
	call	i2d_ASN1_TYPE@PLT
.L640:
	movl	-116(%rbp), %r14d
	movq	-112(%rbp), %rdi
	movl	$290, %edx
	leaq	.LC4(%rip), %rsi
	addl	%r14d, %r14d
	call	CRYPTO_free@PLT
	testl	%r14d, %r14d
	jns	.L683
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L641:
	movl	$284, %r8d
	movl	$65, %edx
	movl	$125, %esi
	movl	$13, %edi
	leaq	.LC4(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L684
.L687:
	call	__stack_chk_fail@PLT
.L688:
	movl	4(%r15), %eax
	leaq	-80(%rbp), %r13
	xorl	%esi, %esi
	movq	%r15, -72(%rbp)
	movq	%r13, %rdi
	movl	%eax, -80(%rbp)
	call	i2d_ASN1_TYPE@PLT
	movl	$283, %edx
	leaq	.LC4(%rip), %rsi
	movslq	%eax, %r14
	movl	%eax, -116(%rbp)
	movq	%r14, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L641
	movq	%rax, %r15
	movq	%r13, %rdi
	leaq	-88(%rbp), %rsi
	movq	%rax, -88(%rbp)
	call	i2d_ASN1_TYPE@PLT
	addq	%r15, %r14
	leaq	-58(%rbp), %r13
	movq	%r14, %rcx
	movq	%r14, -104(%rbp)
	leaq	hexdig.16953(%rip), %r14
	cmpq	%r15, %rcx
	je	.L640
	.p2align 4,,10
	.p2align 3
.L632:
	movzbl	(%r15), %eax
	movq	%r12, %rcx
	movl	$1, %esi
	movq	%r13, %rdi
	movl	%eax, %edx
	andl	$15, %eax
	shrb	$4, %dl
	movzbl	(%r14,%rax), %eax
	andl	$15, %edx
	movzbl	(%r14,%rdx), %edx
	movb	%al, -57(%rbp)
	movb	%dl, -58(%rbp)
	movl	$2, %edx
	call	fwrite@PLT
	cmpq	$2, %rax
	je	.L690
	movq	-112(%rbp), %rdi
	movl	$290, %edx
	leaq	.LC4(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L690:
	addq	$1, %r15
	cmpq	%r15, -104(%rbp)
	jne	.L632
	jmp	.L640
	.cfi_endproc
.LFE815:
	.size	ASN1_STRING_print_ex_fp, .-ASN1_STRING_print_ex_fp
	.p2align 4
	.globl	ASN1_STRING_to_UTF8
	.type	ASN1_STRING_to_UTF8, @function
ASN1_STRING_to_UTF8:
.LFB816:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	-48(%rbp), %rax
	movq	%rax, -56(%rbp)
	testq	%rsi, %rsi
	je	.L695
	movslq	4(%rsi), %rax
	cmpl	$30, %eax
	ja	.L695
	leaq	tag2nbyte(%rip), %rdx
	movsbl	(%rdx,%rax), %eax
	cmpl	$-1, %eax
	je	.L691
	movq	8(%rsi), %r9
	orb	$16, %ah
	movl	(%rsi), %edx
	movq	%rdi, %rbx
	movl	%eax, %ecx
	leaq	-56(%rbp), %rdi
	movl	$8192, %r8d
	movq	$0, -40(%rbp)
	movq	%r9, %rsi
	movl	$0, -48(%rbp)
	movq	$0, -32(%rbp)
	call	ASN1_mbstring_copy@PLT
	testl	%eax, %eax
	js	.L691
	movq	-40(%rbp), %rax
	movq	%rax, (%rbx)
	movl	-48(%rbp), %eax
.L691:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L700
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L695:
	.cfi_restore_state
	movl	$-1, %eax
	jmp	.L691
.L700:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE816:
	.size	ASN1_STRING_to_UTF8, .-ASN1_STRING_to_UTF8
	.section	.rodata
	.align 16
	.type	hexdig.16953, @object
	.size	hexdig.16953, 17
hexdig.16953:
	.string	"0123456789ABCDEF"
	.align 16
	.type	tag2nbyte, @object
	.size	tag2nbyte, 31
tag2nbyte:
	.string	"\377\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\377\377\377\377\377\001\001\001\377\001\001\001\377\001\377"
	.ascii	"\004\377\002"
	.align 32
	.type	char_type, @object
	.size	char_type, 256
char_type:
	.value	1026
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	120
	.value	0
	.value	1
	.value	40
	.value	0
	.value	0
	.value	0
	.value	16
	.value	1040
	.value	1040
	.value	-31744
	.value	25
	.value	25
	.value	16400
	.value	8208
	.value	16
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	16
	.value	9
	.value	9
	.value	16
	.value	9
	.value	16
	.value	0
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	0
	.value	1025
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	4112
	.value	0
	.value	0
	.value	0
	.value	0
	.value	2
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
