	.file	"fcrypt_b.c"
	.text
	.p2align 4
	.globl	fcrypt_body
	.type	fcrypt_body, @function
fcrypt_body:
.LFB54:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$25, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -160(%rbp)
	movl	%r14d, %edi
	movl	16(%rsi), %eax
	movl	(%rsi), %r12d
	movl	4(%rsi), %ebx
	movl	8(%rsi), %r11d
	movl	%eax, -44(%rbp)
	movl	20(%rsi), %eax
	movl	12(%rsi), %r10d
	movl	%eax, -48(%rbp)
	movl	24(%rsi), %eax
	movl	%eax, -52(%rbp)
	movl	28(%rsi), %eax
	movl	%eax, -56(%rbp)
	movl	32(%rsi), %eax
	movl	%eax, -60(%rbp)
	movl	36(%rsi), %eax
	movl	%eax, -64(%rbp)
	movl	40(%rsi), %eax
	movl	%eax, -68(%rbp)
	movl	44(%rsi), %eax
	movl	%eax, -72(%rbp)
	movl	48(%rsi), %eax
	movl	%eax, -76(%rbp)
	movl	52(%rsi), %eax
	movl	%eax, -80(%rbp)
	movl	56(%rsi), %eax
	movl	%eax, -84(%rbp)
	movl	60(%rsi), %eax
	movl	%eax, -88(%rbp)
	movl	64(%rsi), %eax
	movl	%eax, -92(%rbp)
	movl	68(%rsi), %eax
	movl	%eax, -96(%rbp)
	movl	72(%rsi), %eax
	movl	%eax, -100(%rbp)
	movl	76(%rsi), %eax
	movl	%eax, -104(%rbp)
	movl	80(%rsi), %eax
	movl	%eax, -108(%rbp)
	movl	84(%rsi), %eax
	movl	%eax, -112(%rbp)
	movl	88(%rsi), %eax
	movl	%eax, -116(%rbp)
	movl	92(%rsi), %eax
	movl	%eax, -120(%rbp)
	movl	96(%rsi), %eax
	movl	%eax, -124(%rbp)
	movl	100(%rsi), %eax
	movl	%eax, -128(%rbp)
	movl	104(%rsi), %eax
	movl	%eax, -132(%rbp)
	movl	108(%rsi), %eax
	movl	%eax, -136(%rbp)
	movl	112(%rsi), %eax
	movl	%eax, -140(%rbp)
	movl	116(%rsi), %eax
	movl	%eax, -144(%rbp)
	movl	120(%rsi), %eax
	movl	%eax, -148(%rbp)
	movl	124(%rsi), %eax
	xorl	%esi, %esi
	movl	%eax, -152(%rbp)
	leaq	DES_SPtrans(%rip), %rax
	.p2align 4,,10
	.p2align 3
.L2:
	movl	%esi, %r8d
	movl	%edx, %r14d
	movl	%r12d, %r13d
	shrl	$16, %r8d
	xorl	%esi, %r13d
	xorl	%esi, %r8d
	andl	%r8d, %r14d
	andl	%ecx, %r8d
	movl	%r14d, %r15d
	xorl	%r14d, %r13d
	movl	%ebx, %r14d
	sall	$16, %r15d
	xorl	%esi, %r14d
	xorl	%r13d, %r15d
	movl	%r8d, %r13d
	xorl	%r14d, %r8d
	sall	$16, %r13d
	xorl	%r8d, %r13d
#APP
# 41 "../deps/openssl/openssl/crypto/des/fcrypt_b.c" 1
	rorl $4,%r13d
# 0 "" 2
#NO_APP
	movl	%r13d, %r8d
	movl	%r13d, %r14d
	shrl	$2, %r8d
	shrl	$10, %r14d
	andl	$63, %r14d
	andl	$63, %r8d
	movl	256(%rax,%r8,4), %r8d
	xorl	768(%rax,%r14,4), %r8d
	movl	%r13d, %r14d
	shrl	$18, %r13d
	shrl	$26, %r14d
	andl	$63, %r13d
	xorl	1792(%rax,%r14,4), %r8d
	xorl	1280(%rax,%r13,4), %r8d
	movl	%r15d, %r13d
	shrl	$26, %r13d
	xorl	1536(%rax,%r13,4), %r8d
	movl	%r15d, %r13d
	shrl	$2, %r13d
	andl	$63, %r13d
	xorl	(%rax,%r13,4), %r8d
	movl	%r15d, %r13d
	shrl	$18, %r15d
	shrl	$10, %r13d
	andl	$63, %r13d
	xorl	512(%rax,%r13,4), %r8d
	andl	$63, %r15d
	xorl	1024(%rax,%r15,4), %r8d
	movl	%edx, %r15d
	movl	%r8d, %r14d
	xorl	%edi, %r14d
	movl	%r11d, %edi
	movl	%r14d, %r13d
	xorl	%r14d, %edi
	shrl	$16, %r13d
	xorl	%r14d, %r13d
	andl	%r13d, %r15d
	andl	%ecx, %r13d
	movl	%r15d, %r8d
	xorl	%r15d, %edi
	movl	%r10d, %r15d
	sall	$16, %r8d
	xorl	%r14d, %r15d
	xorl	%edi, %r8d
	movl	%r13d, %edi
	xorl	%r15d, %r13d
	sall	$16, %edi
	xorl	%r13d, %edi
#APP
# 42 "../deps/openssl/openssl/crypto/des/fcrypt_b.c" 1
	rorl $4,%edi
# 0 "" 2
#NO_APP
	movl	%edi, %r13d
	movl	%edi, %r15d
	shrl	$2, %r13d
	shrl	$26, %r15d
	andl	$63, %r13d
	xorl	1792(%rax,%r15,4), %esi
	movl	%edx, %r15d
	xorl	256(%rax,%r13,4), %esi
	movl	%edi, %r13d
	shrl	$18, %edi
	shrl	$10, %r13d
	andl	$63, %edi
	andl	$63, %r13d
	xorl	768(%rax,%r13,4), %esi
	xorl	1280(%rax,%rdi,4), %esi
	movl	%r8d, %edi
	shrl	$26, %edi
	xorl	1536(%rax,%rdi,4), %esi
	movl	%r8d, %edi
	shrl	$2, %edi
	andl	$63, %edi
	xorl	(%rax,%rdi,4), %esi
	movl	%r8d, %edi
	shrl	$10, %edi
	shrl	$18, %r8d
	andl	$63, %edi
	andl	$63, %r8d
	xorl	512(%rax,%rdi,4), %esi
	xorl	1024(%rax,%r8,4), %esi
	movl	%esi, %edi
	movl	-44(%rbp), %r8d
	shrl	$16, %edi
	xorl	%esi, %edi
	xorl	%esi, %r8d
	andl	%edi, %r15d
	andl	%ecx, %edi
	xorl	%r15d, %r8d
	movl	%r15d, %r13d
	movl	-48(%rbp), %r15d
	sall	$16, %r13d
	xorl	%r8d, %r13d
	xorl	%esi, %r15d
	movl	%edi, %r8d
	sall	$16, %r8d
	xorl	%r15d, %edi
	xorl	%edi, %r8d
#APP
# 43 "../deps/openssl/openssl/crypto/des/fcrypt_b.c" 1
	rorl $4,%r8d
# 0 "" 2
#NO_APP
	movl	%r8d, %edi
	movl	%r8d, %r15d
	shrl	$2, %edi
	shrl	$10, %r15d
	andl	$63, %r15d
	andl	$63, %edi
	movl	256(%rax,%rdi,4), %edi
	xorl	768(%rax,%r15,4), %edi
	movl	%r8d, %r15d
	shrl	$18, %r8d
	shrl	$26, %r15d
	andl	$63, %r8d
	xorl	1792(%rax,%r15,4), %edi
	xorl	1280(%rax,%r8,4), %edi
	movl	%edx, %r15d
	xorl	%edi, %r14d
	movl	%r13d, %edi
	movl	-52(%rbp), %r8d
	shrl	$26, %edi
	xorl	1536(%rax,%rdi,4), %r14d
	movl	%r13d, %edi
	shrl	$2, %edi
	andl	$63, %edi
	xorl	(%rax,%rdi,4), %r14d
	movl	%r13d, %edi
	shrl	$18, %r13d
	shrl	$10, %edi
	andl	$63, %r13d
	andl	$63, %edi
	xorl	512(%rax,%rdi,4), %r14d
	xorl	1024(%rax,%r13,4), %r14d
	movl	%r14d, %edi
	xorl	%r14d, %r8d
	shrl	$16, %edi
	xorl	%r14d, %edi
	andl	%edi, %r15d
	andl	%ecx, %edi
	xorl	%r15d, %r8d
	movl	%r15d, %r13d
	movl	-56(%rbp), %r15d
	sall	$16, %r13d
	xorl	%r8d, %r13d
	xorl	%r14d, %r15d
	movl	%edi, %r8d
	sall	$16, %r8d
	xorl	%r15d, %edi
	xorl	%edi, %r8d
#APP
# 44 "../deps/openssl/openssl/crypto/des/fcrypt_b.c" 1
	rorl $4,%r8d
# 0 "" 2
#NO_APP
	movl	%r8d, %edi
	movl	%r8d, %r15d
	shrl	$2, %edi
	shrl	$10, %r15d
	andl	$63, %r15d
	andl	$63, %edi
	movl	256(%rax,%rdi,4), %edi
	xorl	768(%rax,%r15,4), %edi
	movl	%r8d, %r15d
	shrl	$18, %r8d
	shrl	$26, %r15d
	andl	$63, %r8d
	xorl	1792(%rax,%r15,4), %edi
	xorl	1280(%rax,%r8,4), %edi
	xorl	%edi, %esi
	movl	%r13d, %edi
	shrl	$26, %edi
	xorl	1536(%rax,%rdi,4), %esi
	movl	%r13d, %edi
	movl	-60(%rbp), %r8d
	shrl	$2, %edi
	andl	$63, %edi
	xorl	(%rax,%rdi,4), %esi
	movl	%r13d, %edi
	shrl	$18, %r13d
	shrl	$10, %edi
	andl	$63, %r13d
	andl	$63, %edi
	xorl	512(%rax,%rdi,4), %esi
	xorl	1024(%rax,%r13,4), %esi
	movl	%edx, %r13d
	movl	%esi, %edi
	xorl	%esi, %r8d
	shrl	$16, %edi
	xorl	%esi, %edi
	andl	%edi, %r13d
	andl	%ecx, %edi
	movl	%r13d, %r15d
	xorl	%r13d, %r8d
	movl	%edi, %r13d
	sall	$16, %r15d
	sall	$16, %r13d
	xorl	%r8d, %r15d
	movl	-64(%rbp), %r8d
	xorl	%esi, %r8d
	xorl	%r8d, %edi
	xorl	%edi, %r13d
#APP
# 45 "../deps/openssl/openssl/crypto/des/fcrypt_b.c" 1
	rorl $4,%r13d
# 0 "" 2
#NO_APP
	movl	%r13d, %edi
	movl	%r13d, %r8d
	shrl	$2, %edi
	shrl	$10, %r8d
	andl	$63, %r8d
	andl	$63, %edi
	movl	256(%rax,%rdi,4), %edi
	xorl	768(%rax,%r8,4), %edi
	movl	%r13d, %r8d
	shrl	$26, %r8d
	xorl	1792(%rax,%r8,4), %edi
	shrl	$18, %r13d
	andl	$63, %r13d
	xorl	1280(%rax,%r13,4), %edi
	movl	-68(%rbp), %r13d
	movl	%edi, %r8d
	movl	%r15d, %edi
	shrl	$26, %edi
	xorl	%r14d, %r8d
	xorl	1536(%rax,%rdi,4), %r8d
	movl	%r15d, %edi
	shrl	$2, %edi
	andl	$63, %edi
	xorl	(%rax,%rdi,4), %r8d
	movl	%r15d, %edi
	shrl	$18, %r15d
	shrl	$10, %edi
	andl	$63, %r15d
	andl	$63, %edi
	xorl	512(%rax,%rdi,4), %r8d
	xorl	1024(%rax,%r15,4), %r8d
	movl	%edx, %r15d
	movl	%r8d, %edi
	xorl	%r8d, %r13d
	shrl	$16, %edi
	xorl	%r8d, %edi
	andl	%edi, %r15d
	andl	%ecx, %edi
	xorl	%r15d, %r13d
	movl	%r15d, %r14d
	movl	-72(%rbp), %r15d
	sall	$16, %r14d
	xorl	%r13d, %r14d
	xorl	%r8d, %r15d
	movl	%edi, %r13d
	sall	$16, %r13d
	xorl	%r15d, %edi
	xorl	%edi, %r13d
#APP
# 46 "../deps/openssl/openssl/crypto/des/fcrypt_b.c" 1
	rorl $4,%r13d
# 0 "" 2
#NO_APP
	movl	%r13d, %edi
	movl	%r13d, %r15d
	shrl	$2, %edi
	shrl	$10, %r15d
	andl	$63, %edi
	andl	$63, %r15d
	movl	256(%rax,%rdi,4), %edi
	xorl	768(%rax,%r15,4), %edi
	movl	%r13d, %r15d
	shrl	$18, %r13d
	shrl	$26, %r15d
	andl	$63, %r13d
	xorl	1792(%rax,%r15,4), %edi
	xorl	1280(%rax,%r13,4), %edi
	movl	%edx, %r15d
	xorl	%esi, %edi
	movl	%r14d, %esi
	movl	-76(%rbp), %r13d
	shrl	$26, %esi
	xorl	1536(%rax,%rsi,4), %edi
	movl	%r14d, %esi
	shrl	$2, %esi
	andl	$63, %esi
	xorl	(%rax,%rsi,4), %edi
	movl	%r14d, %esi
	shrl	$18, %r14d
	shrl	$10, %esi
	andl	$63, %r14d
	andl	$63, %esi
	xorl	512(%rax,%rsi,4), %edi
	xorl	1024(%rax,%r14,4), %edi
	movl	%edi, %esi
	xorl	%edi, %r13d
	shrl	$16, %esi
	xorl	%edi, %esi
	andl	%esi, %r15d
	andl	%ecx, %esi
	xorl	%r15d, %r13d
	movl	%r15d, %r14d
	movl	-80(%rbp), %r15d
	sall	$16, %r14d
	xorl	%r13d, %r14d
	xorl	%edi, %r15d
	movl	%esi, %r13d
	sall	$16, %r13d
	xorl	%r15d, %esi
	xorl	%esi, %r13d
#APP
# 47 "../deps/openssl/openssl/crypto/des/fcrypt_b.c" 1
	rorl $4,%r13d
# 0 "" 2
#NO_APP
	movl	%r13d, %esi
	movl	%r13d, %r15d
	shrl	$2, %esi
	andl	$63, %esi
	shrl	$10, %r15d
	andl	$63, %r15d
	movl	256(%rax,%rsi,4), %esi
	xorl	768(%rax,%r15,4), %esi
	movl	%r13d, %r15d
	shrl	$18, %r13d
	shrl	$26, %r15d
	andl	$63, %r13d
	xorl	1792(%rax,%r15,4), %esi
	xorl	1280(%rax,%r13,4), %esi
	xorl	%r8d, %esi
	movl	%r14d, %r8d
	shrl	$26, %r8d
	xorl	1536(%rax,%r8,4), %esi
	movl	%r14d, %r8d
	shrl	$2, %r8d
	andl	$63, %r8d
	xorl	(%rax,%r8,4), %esi
	movl	%r14d, %r8d
	shrl	$18, %r14d
	shrl	$10, %r8d
	andl	$63, %r14d
	andl	$63, %r8d
	xorl	512(%rax,%r8,4), %esi
	xorl	1024(%rax,%r14,4), %esi
	movl	%edx, %r14d
	movl	%esi, %r8d
	movl	-84(%rbp), %r13d
	shrl	$16, %r8d
	xorl	%esi, %r8d
	xorl	%esi, %r13d
	andl	%r8d, %r14d
	andl	%ecx, %r8d
	movl	%r14d, %r15d
	xorl	%r14d, %r13d
	movl	%r8d, %r14d
	sall	$16, %r15d
	sall	$16, %r14d
	xorl	%r13d, %r15d
	movl	-88(%rbp), %r13d
	xorl	%esi, %r13d
	xorl	%r13d, %r8d
	xorl	%r8d, %r14d
#APP
# 48 "../deps/openssl/openssl/crypto/des/fcrypt_b.c" 1
	rorl $4,%r14d
# 0 "" 2
#NO_APP
	movl	%r14d, %r8d
	movl	%r14d, %r13d
	shrl	$2, %r8d
	shrl	$10, %r13d
	andl	$63, %r13d
	andl	$63, %r8d
	movl	256(%rax,%r8,4), %r8d
	xorl	768(%rax,%r13,4), %r8d
	movl	%r14d, %r13d
	shrl	$18, %r14d
	shrl	$26, %r13d
	andl	$63, %r14d
	xorl	1792(%rax,%r13,4), %r8d
	xorl	1280(%rax,%r14,4), %r8d
	movl	%edx, %r14d
	movl	%r8d, %r13d
	movl	-92(%rbp), %r8d
	xorl	%edi, %r13d
	movl	%r15d, %edi
	shrl	$26, %edi
	xorl	1536(%rax,%rdi,4), %r13d
	movl	%r15d, %edi
	shrl	$2, %edi
	andl	$63, %edi
	xorl	(%rax,%rdi,4), %r13d
	movl	%r15d, %edi
	shrl	$18, %r15d
	shrl	$10, %edi
	andl	$63, %r15d
	andl	$63, %edi
	xorl	512(%rax,%rdi,4), %r13d
	xorl	1024(%rax,%r15,4), %r13d
	movl	%r13d, %edi
	xorl	%r13d, %r8d
	shrl	$16, %edi
	xorl	%r13d, %edi
	andl	%edi, %r14d
	andl	%ecx, %edi
	movl	%r14d, %r15d
	xorl	%r14d, %r8d
	movl	%edi, %r14d
	sall	$16, %r15d
	xorl	%r8d, %r15d
	movl	-96(%rbp), %r8d
	sall	$16, %r14d
	xorl	%r13d, %r8d
	xorl	%r8d, %edi
	xorl	%edi, %r14d
#APP
# 49 "../deps/openssl/openssl/crypto/des/fcrypt_b.c" 1
	rorl $4,%r14d
# 0 "" 2
#NO_APP
	movl	%r14d, %edi
	movl	%r14d, %r8d
	shrl	$2, %edi
	shrl	$10, %r8d
	andl	$63, %r8d
	andl	$63, %edi
	movl	256(%rax,%rdi,4), %edi
	xorl	768(%rax,%r8,4), %edi
	movl	%r14d, %r8d
	shrl	$18, %r14d
	shrl	$26, %r8d
	andl	$63, %r14d
	xorl	1792(%rax,%r8,4), %edi
	xorl	1280(%rax,%r14,4), %edi
	movl	%edx, %r14d
	movl	%edi, %r8d
	movl	-100(%rbp), %edi
	xorl	%esi, %r8d
	movl	%r15d, %esi
	shrl	$26, %esi
	xorl	1536(%rax,%rsi,4), %r8d
	movl	%r15d, %esi
	shrl	$2, %esi
	andl	$63, %esi
	xorl	(%rax,%rsi,4), %r8d
	movl	%r15d, %esi
	shrl	$18, %r15d
	shrl	$10, %esi
	andl	$63, %r15d
	andl	$63, %esi
	xorl	512(%rax,%rsi,4), %r8d
	xorl	1024(%rax,%r15,4), %r8d
	movl	%r8d, %esi
	shrl	$16, %esi
	xorl	%r8d, %esi
	andl	%esi, %r14d
	andl	%ecx, %esi
	movl	%r14d, %r15d
	sall	$16, %r15d
	xorl	%r8d, %edi
	xorl	%r14d, %edi
	movl	%esi, %r14d
	xorl	%edi, %r15d
	movl	-104(%rbp), %edi
	sall	$16, %r14d
	xorl	%r8d, %edi
	xorl	%edi, %esi
	xorl	%esi, %r14d
#APP
# 50 "../deps/openssl/openssl/crypto/des/fcrypt_b.c" 1
	rorl $4,%r14d
# 0 "" 2
#NO_APP
	movl	%r14d, %esi
	movl	%r14d, %edi
	shrl	$2, %esi
	shrl	$10, %edi
	andl	$63, %edi
	andl	$63, %esi
	movl	256(%rax,%rsi,4), %esi
	xorl	768(%rax,%rdi,4), %esi
	movl	%r14d, %edi
	shrl	$18, %r14d
	shrl	$26, %edi
	andl	$63, %r14d
	xorl	1792(%rax,%rdi,4), %esi
	xorl	1280(%rax,%r14,4), %esi
	movl	%edx, %r14d
	movl	%esi, %edi
	movl	%r15d, %esi
	shrl	$26, %esi
	xorl	%r13d, %edi
	xorl	1536(%rax,%rsi,4), %edi
	movl	%r15d, %esi
	shrl	$2, %esi
	andl	$63, %esi
	xorl	(%rax,%rsi,4), %edi
	movl	%r15d, %esi
	shrl	$18, %r15d
	shrl	$10, %esi
	andl	$63, %r15d
	andl	$63, %esi
	xorl	512(%rax,%rsi,4), %edi
	xorl	1024(%rax,%r15,4), %edi
	movl	%edi, %esi
	movl	-108(%rbp), %r13d
	shrl	$16, %esi
	xorl	%edi, %esi
	andl	%esi, %r14d
	xorl	%edi, %r13d
	andl	%ecx, %esi
	xorl	%r14d, %r13d
	movl	%r14d, %r15d
	movl	-112(%rbp), %r14d
	sall	$16, %r15d
	xorl	%r13d, %r15d
	xorl	%edi, %r14d
	movl	%esi, %r13d
	sall	$16, %r13d
	xorl	%r14d, %esi
	xorl	%esi, %r13d
#APP
# 51 "../deps/openssl/openssl/crypto/des/fcrypt_b.c" 1
	rorl $4,%r13d
# 0 "" 2
#NO_APP
	movl	%r13d, %esi
	movl	%r13d, %r14d
	shrl	$2, %esi
	shrl	$10, %r14d
	andl	$63, %r14d
	andl	$63, %esi
	movl	256(%rax,%rsi,4), %esi
	xorl	768(%rax,%r14,4), %esi
	movl	%r13d, %r14d
	shrl	$18, %r13d
	shrl	$26, %r14d
	andl	$63, %r13d
	xorl	1792(%rax,%r14,4), %esi
	xorl	1280(%rax,%r13,4), %esi
	xorl	%r8d, %esi
	movl	%r15d, %r8d
	movl	-116(%rbp), %r13d
	shrl	$26, %r8d
	xorl	1536(%rax,%r8,4), %esi
	movl	%r15d, %r8d
	shrl	$2, %r8d
	andl	$63, %r8d
	xorl	(%rax,%r8,4), %esi
	movl	%r15d, %r8d
	shrl	$18, %r15d
	shrl	$10, %r8d
	andl	$63, %r15d
	andl	$63, %r8d
	xorl	512(%rax,%r8,4), %esi
	xorl	1024(%rax,%r15,4), %esi
	movl	%edx, %r15d
	movl	%esi, %r8d
	xorl	%esi, %r13d
	shrl	$16, %r8d
	xorl	%esi, %r8d
	andl	%r8d, %r15d
	andl	%ecx, %r8d
	xorl	%r15d, %r13d
	movl	%r15d, %r14d
	movl	-120(%rbp), %r15d
	sall	$16, %r14d
	xorl	%r13d, %r14d
	xorl	%esi, %r15d
	movl	%r8d, %r13d
	sall	$16, %r13d
	xorl	%r15d, %r8d
	xorl	%r8d, %r13d
#APP
# 52 "../deps/openssl/openssl/crypto/des/fcrypt_b.c" 1
	rorl $4,%r13d
# 0 "" 2
#NO_APP
	movl	%r13d, %r8d
	movl	%r13d, %r15d
	shrl	$2, %r8d
	shrl	$10, %r15d
	andl	$63, %r15d
	andl	$63, %r8d
	movl	256(%rax,%r8,4), %r8d
	xorl	768(%rax,%r15,4), %r8d
	movl	%r13d, %r15d
	shrl	$18, %r13d
	shrl	$26, %r15d
	andl	$63, %r13d
	xorl	1792(%rax,%r15,4), %r8d
	movl	1280(%rax,%r13,4), %r15d
	xorl	%r8d, %r15d
	movl	-124(%rbp), %r8d
	xorl	%edi, %r15d
	movl	%r14d, %edi
	shrl	$26, %edi
	xorl	1536(%rax,%rdi,4), %r15d
	movl	%r14d, %edi
	shrl	$2, %edi
	andl	$63, %edi
	xorl	(%rax,%rdi,4), %r15d
	movl	%r14d, %edi
	shrl	$10, %edi
	andl	$63, %edi
	xorl	512(%rax,%rdi,4), %r15d
	shrl	$18, %r14d
	andl	$63, %r14d
	xorl	1024(%rax,%r14,4), %r15d
	movl	%edx, %r14d
	movl	%r15d, %edi
	xorl	%r15d, %r8d
	shrl	$16, %edi
	xorl	%r15d, %edi
	andl	%edi, %r14d
	andl	%ecx, %edi
	xorl	%r14d, %r8d
	movl	%r14d, %r13d
	movl	-128(%rbp), %r14d
	sall	$16, %r13d
	xorl	%r8d, %r13d
	xorl	%r15d, %r14d
	movl	%edi, %r8d
	sall	$16, %r8d
	xorl	%r14d, %edi
	xorl	%edi, %r8d
#APP
# 53 "../deps/openssl/openssl/crypto/des/fcrypt_b.c" 1
	rorl $4,%r8d
# 0 "" 2
#NO_APP
	movl	%r8d, %edi
	movl	%r8d, %r14d
	shrl	$2, %edi
	shrl	$10, %r14d
	andl	$63, %r14d
	andl	$63, %edi
	movl	256(%rax,%rdi,4), %edi
	xorl	768(%rax,%r14,4), %edi
	movl	%r8d, %r14d
	shrl	$18, %r8d
	shrl	$26, %r14d
	andl	$63, %r8d
	xorl	1792(%rax,%r14,4), %edi
	xorl	1280(%rax,%r8,4), %edi
	movl	%edi, %r14d
	xorl	%esi, %r14d
	movl	%r13d, %esi
	shrl	$26, %esi
	xorl	1536(%rax,%rsi,4), %r14d
	movl	%r13d, %esi
	shrl	$2, %esi
	andl	$63, %esi
	xorl	(%rax,%rsi,4), %r14d
	movl	%r13d, %esi
	shrl	$18, %r13d
	shrl	$10, %esi
	andl	$63, %r13d
	andl	$63, %esi
	xorl	512(%rax,%rsi,4), %r14d
	xorl	1024(%rax,%r13,4), %r14d
	movl	%edx, %r13d
	movl	%r14d, %edi
	movl	-132(%rbp), %esi
	shrl	$16, %edi
	xorl	%r14d, %edi
	xorl	%r14d, %esi
	andl	%edi, %r13d
	andl	%ecx, %edi
	xorl	%r13d, %esi
	movl	%r13d, %r8d
	movl	-136(%rbp), %r13d
	sall	$16, %r8d
	xorl	%esi, %r8d
	xorl	%r14d, %r13d
	movl	%edi, %esi
	sall	$16, %esi
	xorl	%r13d, %edi
	xorl	%edi, %esi
#APP
# 54 "../deps/openssl/openssl/crypto/des/fcrypt_b.c" 1
	rorl $4,%esi
# 0 "" 2
#NO_APP
	movl	%esi, %r13d
	movl	%esi, %edi
	shrl	$2, %r13d
	shrl	$10, %edi
	andl	$63, %edi
	andl	$63, %r13d
	movl	256(%rax,%r13,4), %r13d
	xorl	768(%rax,%rdi,4), %r13d
	movl	%esi, %edi
	shrl	$18, %esi
	shrl	$26, %edi
	andl	$63, %esi
	xorl	1792(%rax,%rdi,4), %r13d
	movl	1280(%rax,%rsi,4), %edi
	movl	%r8d, %esi
	shrl	$26, %esi
	xorl	%r13d, %edi
	movl	%edx, %r13d
	xorl	%r15d, %edi
	xorl	1536(%rax,%rsi,4), %edi
	movl	%r8d, %esi
	shrl	$2, %esi
	andl	$63, %esi
	xorl	(%rax,%rsi,4), %edi
	movl	%r8d, %esi
	shrl	$18, %r8d
	shrl	$10, %esi
	andl	$63, %r8d
	andl	$63, %esi
	xorl	512(%rax,%rsi,4), %edi
	xorl	1024(%rax,%r8,4), %edi
	movl	%edi, %esi
	movl	-140(%rbp), %r8d
	shrl	$16, %esi
	xorl	%edi, %esi
	xorl	%edi, %r8d
	andl	%esi, %r13d
	andl	%ecx, %esi
	movl	%r13d, %r15d
	xorl	%r13d, %r8d
	movl	%esi, %r13d
	sall	$16, %r15d
	sall	$16, %r13d
	xorl	%r8d, %r15d
	movl	-144(%rbp), %r8d
	xorl	%edi, %r8d
	xorl	%r8d, %esi
	xorl	%esi, %r13d
#APP
# 55 "../deps/openssl/openssl/crypto/des/fcrypt_b.c" 1
	rorl $4,%r13d
# 0 "" 2
#NO_APP
	movl	%r13d, %r8d
	movl	%r13d, %esi
	shrl	$2, %r8d
	shrl	$10, %esi
	andl	$63, %esi
	andl	$63, %r8d
	movl	256(%rax,%r8,4), %r8d
	xorl	768(%rax,%rsi,4), %r8d
	movl	%r13d, %esi
	shrl	$18, %r13d
	shrl	$26, %esi
	andl	$63, %r13d
	xorl	1792(%rax,%rsi,4), %r8d
	movl	1280(%rax,%r13,4), %esi
	xorl	%r8d, %esi
	movl	%r15d, %r8d
	shrl	$26, %r8d
	xorl	%r14d, %esi
	movl	%edx, %r14d
	xorl	1536(%rax,%r8,4), %esi
	movl	%r15d, %r8d
	shrl	$2, %r8d
	andl	$63, %r8d
	xorl	(%rax,%r8,4), %esi
	movl	%r15d, %r8d
	shrl	$18, %r15d
	shrl	$10, %r8d
	andl	$63, %r15d
	andl	$63, %r8d
	xorl	512(%rax,%r8,4), %esi
	xorl	1024(%rax,%r15,4), %esi
	movl	%esi, %r13d
	movl	-148(%rbp), %r8d
	shrl	$16, %r13d
	xorl	%esi, %r13d
	xorl	%esi, %r8d
	andl	%r13d, %r14d
	andl	%ecx, %r13d
	xorl	%r14d, %r8d
	movl	%r14d, %r15d
	movl	-152(%rbp), %r14d
	sall	$16, %r15d
	xorl	%r8d, %r15d
	xorl	%esi, %r14d
	movl	%r13d, %r8d
	sall	$16, %r8d
	xorl	%r14d, %r13d
	xorl	%r13d, %r8d
#APP
# 56 "../deps/openssl/openssl/crypto/des/fcrypt_b.c" 1
	rorl $4,%r8d
# 0 "" 2
#NO_APP
	movl	%r8d, %r13d
	movl	%r8d, %r14d
	shrl	$2, %r13d
	shrl	$10, %r14d
	andl	$63, %r14d
	andl	$63, %r13d
	movl	256(%rax,%r13,4), %r13d
	xorl	768(%rax,%r14,4), %r13d
	movl	%r8d, %r14d
	shrl	$26, %r14d
	xorl	1792(%rax,%r14,4), %r13d
	shrl	$18, %r8d
	andl	$63, %r8d
	xorl	1280(%rax,%r8,4), %r13d
	movl	%r15d, %r8d
	shrl	$26, %r8d
	xorl	%r13d, %edi
	xorl	1536(%rax,%r8,4), %edi
	movl	%r15d, %r8d
	shrl	$2, %r8d
	andl	$63, %r8d
	xorl	(%rax,%r8,4), %edi
	movl	%r15d, %r8d
	shrl	$18, %r15d
	shrl	$10, %r8d
	andl	$63, %r15d
	andl	$63, %r8d
	xorl	512(%rax,%r8,4), %edi
	xorl	1024(%rax,%r15,4), %edi
	subl	$1, %r9d
	jne	.L2
#APP
# 61 "../deps/openssl/openssl/crypto/des/fcrypt_b.c" 1
	rorl $3,%edi
# 0 "" 2
#NO_APP
	movl	%edi, %r14d
	movq	-160(%rbp), %rbx
	shrl	%r14d
#APP
# 62 "../deps/openssl/openssl/crypto/des/fcrypt_b.c" 1
	rorl $3,%esi
# 0 "" 2
#NO_APP
	xorl	%esi, %r14d
	andl	$1431655765, %r14d
	xorl	%r14d, %esi
	addl	%r14d, %r14d
	movl	%esi, %eax
	xorl	%r14d, %edi
	shrl	$8, %eax
	xorl	%edi, %eax
	andl	$16711935, %eax
	xorl	%eax, %edi
	sall	$8, %eax
	xorl	%eax, %esi
	movl	%edi, %eax
	movl	%edi, %r14d
	shrl	$2, %eax
	xorl	%esi, %eax
	andl	$858993459, %eax
	xorl	%eax, %esi
	leal	0(,%rax,4), %edi
	movl	%esi, %eax
	xorl	%r14d, %edi
	shrl	$16, %eax
	xorl	%edi, %eax
	movzwl	%ax, %eax
	xorl	%eax, %edi
	sall	$16, %eax
	xorl	%eax, %esi
	movl	%edi, %eax
	shrl	$4, %eax
	xorl	%esi, %eax
	andl	$252645135, %eax
	xorl	%eax, %esi
	sall	$4, %eax
	xorl	%eax, %edi
	movl	%esi, (%rbx)
	movl	%edi, 4(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE54:
	.size	fcrypt_body, .-fcrypt_body
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
