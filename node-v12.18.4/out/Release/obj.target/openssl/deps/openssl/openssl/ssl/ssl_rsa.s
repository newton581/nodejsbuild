	.file	"ssl_rsa.c"
	.text
	.p2align 4
	.type	serverinfoex_srv_parse_cb, @function
serverinfoex_srv_parse_cb:
.LFB988:
	.cfi_startproc
	endbr64
	testq	%r8, %r8
	jne	.L11
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	24(%rbp), %rax
	movl	$50, (%rax)
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE988:
	.size	serverinfoex_srv_parse_cb, .-serverinfoex_srv_parse_cb
	.p2align 4
	.type	serverinfo_srv_parse_cb, @function
serverinfo_srv_parse_cb:
.LFB989:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	testq	%rcx, %rcx
	jne	.L16
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movl	$50, (%r8)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE989:
	.size	serverinfo_srv_parse_cb, .-serverinfo_srv_parse_cb
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/ssl/ssl_rsa.c"
	.text
	.p2align 4
	.type	ssl_set_cert, @function
ssl_set_cert:
.LFB975:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	X509_get0_pubkey@PLT
	testq	%rax, %rax
	je	.L35
	leaq	-48(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	ssl_cert_lookup_by_pkey@PLT
	testq	%rax, %rax
	je	.L36
	movq	-48(%rbp), %rax
	cmpq	$3, %rax
	je	.L37
.L21:
	leaq	(%rax,%rax,4), %rdx
	movq	40(%rbx,%rdx,8), %rsi
	testq	%rsi, %rsi
	je	.L23
	movq	%r13, %rdi
	call	EVP_PKEY_copy_parameters@PLT
	call	ERR_clear_error@PLT
	movq	-48(%rbp), %rax
	leaq	(%rax,%rax,4), %rax
	movq	40(%rbx,%rax,8), %rdi
	call	EVP_PKEY_id@PLT
	cmpl	$6, %eax
	je	.L38
.L24:
	movq	-48(%rbp), %rax
	movq	%r12, %rdi
	leaq	(%rax,%rax,4), %rax
	movq	40(%rbx,%rax,8), %rsi
	call	X509_check_private_key@PLT
	testl	%eax, %eax
	movq	-48(%rbp), %rax
	je	.L25
.L23:
	leaq	(%rax,%rax,4), %rax
	movq	32(%rbx,%rax,8), %rdi
	call	X509_free@PLT
	movq	%r12, %rdi
	call	X509_up_ref@PLT
	movq	-48(%rbp), %rax
	leaq	(%rax,%rax,4), %rax
	salq	$3, %rax
	movq	%r12, 32(%rbx,%rax)
	leaq	32(%rbx,%rax), %rax
	movq	%rax, (%rbx)
	movl	$1, %eax
.L17:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L39
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	leaq	(%rax,%rax,4), %rax
	movq	40(%rbx,%rax,8), %rdi
	call	EVP_PKEY_free@PLT
	movq	-48(%rbp), %rax
	leaq	(%rax,%rax,4), %rax
	movq	$0, 40(%rbx,%rax,8)
	call	ERR_clear_error@PLT
.L34:
	movq	-48(%rbp), %rax
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L35:
	movl	$323, %r8d
	movl	$268, %edx
	movl	$191, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L37:
	movq	%r13, %rdi
	call	EVP_PKEY_get0_EC_KEY@PLT
	movq	%rax, %rdi
	call	EC_KEY_can_sign@PLT
	testl	%eax, %eax
	je	.L22
	movq	-48(%rbp), %rax
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L38:
	movq	-48(%rbp), %rax
	leaq	(%rax,%rax,4), %rax
	movq	40(%rbx,%rax,8), %rdi
	call	EVP_PKEY_get0_RSA@PLT
	movq	%rax, %rdi
	call	RSA_flags@PLT
	testb	$1, %al
	je	.L24
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$328, %r8d
	movl	$247, %edx
	movl	$191, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L22:
	movl	$333, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$318, %edx
	movl	%eax, -52(%rbp)
	movl	$191, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	movl	-52(%rbp), %eax
	jmp	.L17
.L39:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE975:
	.size	ssl_set_cert, .-ssl_set_cert
	.p2align 4
	.type	ssl_set_pkey, @function
ssl_set_pkey:
.LFB968:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	leaq	-32(%rbp), %rsi
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	ssl_cert_lookup_by_pkey@PLT
	testq	%rax, %rax
	je	.L56
	movq	-32(%rbp), %rax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%rbx,%rdx,8), %rdi
	testq	%rdi, %rdi
	je	.L43
	call	X509_get0_pubkey@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L57
	movq	%r12, %rsi
	call	EVP_PKEY_copy_parameters@PLT
	call	ERR_clear_error@PLT
	movq	%r12, %rdi
	call	EVP_PKEY_id@PLT
	cmpl	$6, %eax
	je	.L58
.L45:
	movq	-32(%rbp), %rax
	movq	%r12, %rsi
	leaq	(%rax,%rax,4), %rax
	movq	32(%rbx,%rax,8), %rdi
	call	X509_check_private_key@PLT
	testl	%eax, %eax
	je	.L46
.L55:
	movq	-32(%rbp), %rax
.L43:
	leaq	(%rax,%rax,4), %rax
	movq	40(%rbx,%rax,8), %rdi
	call	EVP_PKEY_free@PLT
	movq	%r12, %rdi
	call	EVP_PKEY_up_ref@PLT
	movq	-32(%rbp), %rax
	leaq	(%rax,%rax,4), %rax
	salq	$3, %rax
	movq	%r12, 40(%rbx,%rax)
	leaq	32(%rbx,%rax), %rax
	movq	%rax, (%rbx)
	movl	$1, %eax
.L40:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L59
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	movq	%r12, %rdi
	call	EVP_PKEY_get0_RSA@PLT
	movq	%rax, %rdi
	call	RSA_flags@PLT
	testb	$1, %al
	je	.L45
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L57:
	movl	$141, %r8d
	movl	$65, %edx
	movl	$193, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L56:
	movl	$133, %r8d
	movl	$247, %edx
	movl	$193, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L46:
	movq	-32(%rbp), %rdx
	movl	%eax, -36(%rbp)
	leaq	(%rdx,%rdx,4), %rdx
	movq	32(%rbx,%rdx,8), %rdi
	call	X509_free@PLT
	movq	-32(%rbp), %rdx
	movl	-36(%rbp), %eax
	leaq	(%rdx,%rdx,4), %rdx
	movq	$0, 32(%rbx,%rdx,8)
	jmp	.L40
.L59:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE968:
	.size	ssl_set_pkey, .-ssl_set_pkey
	.p2align 4
	.type	ssl_set_cert_and_key, @function
ssl_set_cert_and_key:
.LFB996:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%rdx, -72(%rbp)
	movq	%rcx, -80(%rbp)
	movl	%r9d, -84(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L61
	movq	1168(%rdi), %rax
	movq	%rax, -96(%rbp)
.L62:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	ssl_security_cert@PLT
	xorl	%r15d, %r15d
	movl	$1053, %r8d
	leaq	.LC0(%rip), %rcx
	movl	%eax, %r14d
	movl	%eax, %edx
	cmpl	$1, %eax
	je	.L63
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L66:
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdx
	movq	%rbx, %rdi
	call	ssl_security_cert@PLT
	cmpl	$1, %eax
	jne	.L96
	addl	$1, %r15d
.L63:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r15d
	jl	.L66
	movq	-72(%rbp), %rdi
	call	X509_get_pubkey@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L81
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L82
	movq	%rax, %rdi
	call	EVP_PKEY_missing_parameters@PLT
	movq	%r13, %rdi
	testl	%eax, %eax
	je	.L68
	call	EVP_PKEY_missing_parameters@PLT
	testl	%eax, %eax
	jne	.L97
	movq	-80(%rbp), %rdi
	movq	%r13, %rsi
	call	EVP_PKEY_copy_parameters@PLT
.L70:
	movq	-80(%rbp), %rdi
	call	EVP_PKEY_id@PLT
	cmpl	$6, %eax
	je	.L71
.L74:
	movq	-80(%rbp), %rsi
	movq	%r13, %rdi
	call	EVP_PKEY_cmp@PLT
	cmpl	$1, %eax
	jne	.L98
.L67:
	leaq	-64(%rbp), %rsi
	movq	%r13, %rdi
	call	ssl_cert_lookup_by_pkey@PLT
	testq	%rax, %rax
	je	.L99
	movl	-84(%rbp), %eax
	testl	%eax, %eax
	jne	.L76
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rcx
	leaq	(%rax,%rax,4), %rax
	leaq	(%rcx,%rax,8), %rax
	cmpq	$0, 32(%rax)
	jne	.L77
	cmpq	$0, 40(%rax)
	je	.L100
.L77:
	movl	$1107, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$289, %edx
	xorl	%r14d, %r14d
	movl	$621, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L96:
	movl	$1059, %r8d
	leaq	.LC0(%rip), %rcx
	movl	%eax, %edx
.L95:
	movl	$621, %esi
	movl	$20, %edi
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	call	ERR_put_error@PLT
.L64:
	movq	%r13, %rdi
	call	EVP_PKEY_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L101
	addq	$56, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	movq	320(%rsi), %rax
	movq	%rax, -96(%rbp)
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L82:
	movq	%r13, -80(%rbp)
	jmp	.L67
.L100:
	cmpq	$0, 48(%rax)
	jne	.L77
	.p2align 4,,10
	.p2align 3
.L76:
	testq	%r12, %r12
	je	.L78
	movq	%r12, %rdi
	call	X509_chain_up_ref@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L102
.L78:
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rbx
	movq	X509_free@GOTPCREL(%rip), %rsi
	leaq	(%rax,%rax,4), %rax
	movq	48(%rbx,%rax,8), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	-64(%rbp), %rax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rbx,%rax,8), %rax
	movq	%r12, 48(%rax)
	movq	32(%rax), %rdi
	call	X509_free@PLT
	movq	-72(%rbp), %r15
	movq	%r15, %rdi
	call	X509_up_ref@PLT
	movq	-64(%rbp), %rax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rbx,%rax,8), %rax
	movq	%r15, 32(%rax)
	movq	40(%rax), %rdi
	call	EVP_PKEY_free@PLT
	movq	-80(%rbp), %r15
	movq	%r15, %rdi
	call	EVP_PKEY_up_ref@PLT
	movq	-64(%rbp), %rax
	leaq	(%rax,%rax,4), %rax
	salq	$3, %rax
	movq	%r15, 40(%rbx,%rax)
	leaq	32(%rbx,%rax), %rax
	movq	%rax, (%rbx)
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L68:
	call	EVP_PKEY_missing_parameters@PLT
	testl	%eax, %eax
	je	.L70
	movq	-80(%rbp), %rsi
	movq	%r13, %rdi
	call	EVP_PKEY_copy_parameters@PLT
	jmp	.L70
.L81:
	xorl	%r14d, %r14d
	jmp	.L64
.L71:
	movq	-80(%rbp), %rdi
	call	EVP_PKEY_get0_RSA@PLT
	movq	%rax, %rdi
	call	RSA_flags@PLT
	testb	$1, %al
	je	.L74
	jmp	.L67
.L98:
	movl	$1094, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$288, %edx
	xorl	%r14d, %r14d
	movl	$621, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L64
.L97:
	movl	$1074, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$290, %edx
	xorl	%r14d, %r14d
	movl	$621, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L64
.L99:
	movl	$1099, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$247, %edx
	xorl	%r14d, %r14d
	movl	$621, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L64
.L102:
	movl	$1114, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r14d, %r14d
	movl	$621, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L64
.L101:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE996:
	.size	ssl_set_cert_and_key, .-ssl_set_cert_and_key
	.p2align 4
	.type	serverinfo_process_buffer.part.0, @function
serverinfo_process_buffer.part.0:
.LFB1005:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	serverinfoex_srv_parse_cb(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$8, %rsp
.L104:
	testq	%r13, %r13
	je	.L112
.L131:
	cmpl	$2, %r14d
	je	.L113
	testq	%rbx, %rbx
	je	.L119
	cmpq	$3, %rbx
	jbe	.L107
	movzwl	2(%r12), %eax
	subq	$4, %rbx
	rolw	$8, %ax
	movzwl	%ax, %eax
	cmpq	%rbx, %rax
	ja	.L107
	movq	%r12, %rcx
	subq	%rax, %rbx
	leaq	4(%r12,%rax), %r12
	xorl	%edx, %edx
.L114:
	movzwl	(%rcx), %esi
	rolw	$8, %si
	movzwl	%si, %esi
	cmpq	$464, %rdx
	je	.L120
	cmpl	$1, %r14d
	je	.L120
	pushq	$0
	leaq	serverinfoex_srv_add_cb(%rip), %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	%r15
	movq	%r13, %rdi
	call	SSL_CTX_add_custom_ext@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	jne	.L104
	.p2align 4,,10
	.p2align 3
.L107:
	xorl	%eax, %eax
.L103:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore_state
	subq	$8, %rsp
	movq	%r13, %rdi
	leaq	serverinfo_srv_parse_cb(%rip), %r9
	xorl	%ecx, %ecx
	pushq	$0
	xorl	%r8d, %r8d
	leaq	serverinfo_srv_add_cb(%rip), %rdx
	call	SSL_CTX_add_server_custom_ext@PLT
	popq	%rsi
	popq	%rdi
	testl	%eax, %eax
	je	.L107
	testq	%r13, %r13
	jne	.L131
	.p2align 4,,10
	.p2align 3
.L112:
	cmpl	$2, %r14d
	je	.L108
	testq	%rbx, %rbx
	je	.L119
.L132:
	cmpq	$3, %rbx
	jbe	.L107
	movzwl	2(%r12), %eax
	subq	$4, %rbx
	rolw	$8, %ax
	movzwl	%ax, %eax
	cmpq	%rbx, %rax
	ja	.L107
	subq	%rax, %rbx
	leaq	4(%r12,%rax), %r12
	testq	%rbx, %rbx
	jne	.L132
	.p2align 4,,10
	.p2align 3
.L119:
	movl	$1, %eax
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L113:
	testq	%rbx, %rbx
	je	.L119
	cmpq	$3, %rbx
	jbe	.L107
	movl	(%r12), %edx
	leaq	-4(%rbx), %rax
	leaq	4(%r12), %rcx
	bswap	%edx
	movl	%edx, %edx
	cmpq	$3, %rax
	jbe	.L107
	movzwl	6(%r12), %eax
	subq	$8, %rbx
	rolw	$8, %ax
	movzwl	%ax, %eax
	cmpq	%rax, %rbx
	jb	.L107
	leaq	8(%r12,%rax), %r12
	subq	%rax, %rbx
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L105:
	leaq	-4(%rbx), %rax
	cmpq	$3, %rax
	jbe	.L107
	movzwl	6(%r12), %eax
	subq	$8, %rbx
	rolw	$8, %ax
	movzwl	%ax, %eax
	cmpq	%rax, %rbx
	jb	.L107
	leaq	8(%r12,%rax), %r12
	subq	%rax, %rbx
.L108:
	testq	%rbx, %rbx
	je	.L119
	cmpq	$3, %rbx
	ja	.L105
	jmp	.L107
	.cfi_endproc
.LFE1005:
	.size	serverinfo_process_buffer.part.0, .-serverinfo_process_buffer.part.0
	.p2align 4
	.type	serverinfoex_srv_add_cb, @function
serverinfoex_srv_add_cb:
.LFB990:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%r8, %rbx
	subq	$32, %rsp
	movq	24(%rbp), %r14
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	andb	$16, %dh
	je	.L134
	cmpq	$0, 16(%rbp)
	jne	.L139
.L134:
	leaq	-48(%rbp), %rdx
	leaq	-56(%rbp), %rsi
	call	ssl_get_server_cert_serverinfo@PLT
	testl	%eax, %eax
	jne	.L156
.L139:
	xorl	%eax, %eax
.L133:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L157
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L156:
	.cfi_restore_state
	movq	-56(%rbp), %rcx
	movq	-48(%rbp), %rax
	movq	$0, 0(%r13)
	movq	$0, (%rbx)
	testq	%rcx, %rcx
	je	.L140
	testq	%rax, %rax
	jle	.L140
.L137:
	cmpq	$7, %rax
	jbe	.L140
	movzwl	6(%rcx), %edx
	subq	$8, %rax
	rolw	$8, %dx
	movzwl	%dx, %edx
	cmpq	%rdx, %rax
	jb	.L140
	movzwl	4(%rcx), %esi
	leaq	8(%rcx), %rdi
	subq	%rdx, %rax
	leaq	(%rdi,%rdx), %rcx
	rolw	$8, %si
	movzwl	%si, %esi
	cmpl	%esi, %r12d
	je	.L158
	testq	%rax, %rax
	jne	.L137
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L140:
	movl	$80, (%r14)
	movl	$-1, %eax
	jmp	.L133
.L158:
	movq	%rdi, 0(%r13)
	movl	$1, %eax
	movq	%rdx, (%rbx)
	jmp	.L133
.L157:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE990:
	.size	serverinfoex_srv_add_cb, .-serverinfoex_srv_add_cb
	.p2align 4
	.type	serverinfo_srv_add_cb, @function
serverinfo_srv_add_cb:
.LFB991:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	pushq	%r13
	leaq	-56(%rbp), %rsi
	.cfi_offset 13, -32
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	leaq	-48(%rbp), %rdx
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rcx, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	call	ssl_get_server_cert_serverinfo@PLT
	testl	%eax, %eax
	jne	.L174
.L159:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L175
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L174:
	.cfi_restore_state
	movq	-56(%rbp), %rcx
	movq	-48(%rbp), %rax
	movq	$0, (%r12)
	movq	$0, (%rbx)
	testq	%rcx, %rcx
	je	.L164
	testq	%rax, %rax
	jle	.L164
.L161:
	cmpq	$7, %rax
	jbe	.L164
	movzwl	6(%rcx), %edx
	subq	$8, %rax
	rolw	$8, %dx
	movzwl	%dx, %edx
	cmpq	%rdx, %rax
	jb	.L164
	movzwl	4(%rcx), %esi
	leaq	8(%rcx), %rdi
	subq	%rdx, %rax
	leaq	(%rdi,%rdx), %rcx
	rolw	$8, %si
	movzwl	%si, %esi
	cmpl	%esi, %r14d
	je	.L176
	testq	%rax, %rax
	jne	.L161
	xorl	%eax, %eax
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L164:
	movl	$80, 0(%r13)
	movl	$-1, %eax
	jmp	.L159
.L176:
	movq	%rdi, (%r12)
	movl	$1, %eax
	movq	%rdx, (%rbx)
	jmp	.L159
.L175:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE991:
	.size	serverinfo_srv_add_cb, .-serverinfo_srv_add_cb
	.p2align 4
	.type	use_certificate_chain_file, @function
use_certificate_chain_file:
.LFB984:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%rsi, -56(%rbp)
	call	ERR_clear_error@PLT
	testq	%rbx, %rbx
	je	.L178
	movq	176(%rbx), %r14
	movq	184(%rbx), %r15
.L179:
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movl	$610, %r8d
	movl	$7, %edx
	leaq	.LC0(%rip), %rcx
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L211
	movq	%r13, %rcx
	movl	$3, %edx
	movl	$108, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	testl	%eax, %eax
	jle	.L213
	movq	%r15, %rcx
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	PEM_read_bio_X509_AUX@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L214
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rdx
	testq	%rbx, %rbx
	je	.L184
	movq	%rbx, %rsi
	xorl	%edi, %edi
	call	ssl_security_cert@PLT
	movl	%eax, %edx
	cmpl	$1, %eax
	jne	.L215
	movq	320(%rbx), %rdi
	movq	%r13, %rsi
	call	ssl_set_cert
	movl	%eax, -68(%rbp)
	call	ERR_peek_error@PLT
	testq	%rax, %rax
	jne	.L212
	movl	-68(%rbp), %eax
	testl	%eax, %eax
	je	.L212
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$88, %esi
	movq	%rbx, %rdi
	call	SSL_CTX_ctrl@PLT
	movl	%eax, %r8d
	testl	%eax, %eax
	jne	.L191
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L195:
	movq	%r8, %rcx
	xorl	%edx, %edx
	movl	$89, %esi
	movq	%rbx, %rdi
	movq	%r8, -56(%rbp)
	call	SSL_CTX_ctrl@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	je	.L194
.L191:
	movq	%r15, %rcx
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	PEM_read_bio_X509@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	jne	.L195
.L193:
	call	ERR_peek_last_error@PLT
	xorl	%r8d, %r8d
	movl	%eax, %edx
	shrl	$24, %edx
	cmpl	$9, %edx
	jne	.L181
	andl	$4095, %eax
	cmpl	$108, %eax
	jne	.L181
	call	ERR_clear_error@PLT
	movl	-68(%rbp), %r8d
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L213:
	movl	$615, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$2, %edx
.L211:
	movl	$220, %esi
	movl	$20, %edi
	xorl	%r13d, %r13d
	call	ERR_put_error@PLT
	xorl	%r8d, %r8d
.L181:
	movq	%r13, %rdi
	movl	%r8d, -56(%rbp)
	call	X509_free@PLT
	movq	%r12, %rdi
	call	BIO_free@PLT
	movl	-56(%rbp), %r8d
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	movl	%r8d, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	movq	-56(%rbp), %rax
	movq	6136(%rax), %r14
	movq	6144(%rax), %r15
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L184:
	movq	-56(%rbp), %rdi
	xorl	%esi, %esi
	call	ssl_security_cert@PLT
	movl	%eax, %edx
	cmpl	$1, %eax
	je	.L188
	movl	$36, %r8d
	movl	$198, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	call	ERR_peek_error@PLT
	xorl	%r8d, %r8d
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L215:
	movl	$310, %r8d
	movl	$171, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	call	ERR_peek_error@PLT
	xorl	%r8d, %r8d
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L194:
	movq	%r8, %rdi
	call	X509_free@PLT
.L212:
	xorl	%r8d, %r8d
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L188:
	movq	-56(%rbp), %rax
	movq	%r13, %rsi
	movq	1168(%rax), %rdi
	call	ssl_set_cert
	movl	%eax, -68(%rbp)
	movl	%eax, %ebx
	call	ERR_peek_error@PLT
	testq	%rax, %rax
	jne	.L212
	testl	%ebx, %ebx
	je	.L212
	movq	-56(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$88, %esi
	call	SSL_ctrl@PLT
	movl	%eax, %r8d
	testl	%eax, %eax
	jne	.L192
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L216:
	movq	-56(%rbp), %rdi
	xorl	%edx, %edx
	movq	%rax, %rcx
	movl	$89, %esi
	movq	%rax, -64(%rbp)
	call	SSL_ctrl@PLT
	movq	-64(%rbp), %r8
	testl	%eax, %eax
	je	.L194
.L192:
	xorl	%esi, %esi
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	PEM_read_bio_X509@PLT
	testq	%rax, %rax
	jne	.L216
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L214:
	movl	$622, %r8d
	movl	$9, %edx
	movl	$220, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r8d, %r8d
	jmp	.L181
	.cfi_endproc
.LFE984:
	.size	use_certificate_chain_file, .-use_certificate_chain_file
	.p2align 4
	.globl	SSL_use_certificate
	.type	SSL_use_certificate, @function
SSL_use_certificate:
.LFB964:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	testq	%rsi, %rsi
	je	.L222
	movq	%rsi, %rdx
	movq	%rsi, %r12
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%rdi, %rbx
	call	ssl_security_cert@PLT
	movl	%eax, %edx
	cmpl	$1, %eax
	je	.L220
	movl	$36, %r8d
	movl	$198, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L220:
	.cfi_restore_state
	movq	1168(%rbx), %rdi
	movq	%r12, %rsi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ssl_set_cert
	.p2align 4,,10
	.p2align 3
.L222:
	.cfi_restore_state
	movl	$31, %r8d
	movl	$67, %edx
	movl	$198, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE964:
	.size	SSL_use_certificate, .-SSL_use_certificate
	.p2align 4
	.globl	SSL_use_certificate_file
	.type	SSL_use_certificate_file, @function
SSL_use_certificate_file:
.LFB965:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	%edx, %ebx
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movl	$52, %r8d
	movl	$7, %edx
	leaq	.LC0(%rip), %rcx
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L233
	movq	%r14, %rcx
	movl	$3, %edx
	movl	$108, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	testl	%eax, %eax
	jle	.L234
	cmpl	$2, %ebx
	je	.L235
	cmpl	$1, %ebx
	jne	.L229
	movq	6136(%r13), %rdx
	movq	6144(%r13), %rcx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	PEM_read_bio_X509@PLT
	movl	$9, %edx
	movq	%rax, %r14
.L228:
	testq	%r14, %r14
	je	.L236
	movq	%r14, %rdx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	ssl_security_cert@PLT
	movl	%eax, %edx
	cmpl	$1, %eax
	je	.L231
	movl	$36, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$198, %esi
	xorl	%r13d, %r13d
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L234:
	movl	$57, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$2, %edx
.L233:
	movl	$200, %esi
	movl	$20, %edi
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	call	ERR_put_error@PLT
.L225:
	movq	%r14, %rdi
	call	X509_free@PLT
	movq	%r12, %rdi
	call	BIO_free@PLT
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L229:
	.cfi_restore_state
	movl	$68, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$124, %edx
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L231:
	movq	1168(%r13), %rdi
	movq	%r14, %rsi
	call	ssl_set_cert
	movl	%eax, %r13d
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L235:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	d2i_X509_bio@PLT
	movl	$13, %edx
	movq	%rax, %r14
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L236:
	movl	$73, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$200, %esi
	xorl	%r13d, %r13d
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L225
	.cfi_endproc
.LFE965:
	.size	SSL_use_certificate_file, .-SSL_use_certificate_file
	.p2align 4
	.globl	SSL_use_certificate_ASN1
	.type	SSL_use_certificate_ASN1, @function
SSL_use_certificate_ASN1:
.LFB966:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	xorl	%edi, %edi
	subq	$24, %rsp
	movq	%rsi, -40(%rbp)
	leaq	-40(%rbp), %rsi
	call	d2i_X509@PLT
	testq	%rax, %rax
	je	.L243
	movq	%rax, %rdx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rax, %r12
	call	ssl_security_cert@PLT
	movl	%eax, %edx
	cmpl	$1, %eax
	jne	.L244
	movq	1168(%rbx), %rdi
	movq	%r12, %rsi
	call	ssl_set_cert
	movl	%eax, %r13d
.L241:
	movq	%r12, %rdi
	call	X509_free@PLT
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L244:
	.cfi_restore_state
	movl	$36, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$198, %esi
	xorl	%r13d, %r13d
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L243:
	movl	$91, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$13, %edx
	xorl	%r13d, %r13d
	movl	$199, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE966:
	.size	SSL_use_certificate_ASN1, .-SSL_use_certificate_ASN1
	.p2align 4
	.globl	SSL_use_RSAPrivateKey
	.type	SSL_use_RSAPrivateKey, @function
SSL_use_RSAPrivateKey:
.LFB967:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	testq	%rsi, %rsi
	je	.L251
	movq	%rdi, %rbx
	call	EVP_PKEY_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L252
	movq	%r12, %rdi
	call	RSA_up_ref@PLT
	movq	%r12, %rdx
	movl	$6, %esi
	movq	%r13, %rdi
	call	EVP_PKEY_assign@PLT
	testl	%eax, %eax
	jle	.L253
	movq	1168(%rbx), %rdi
	movq	%r13, %rsi
	call	ssl_set_pkey
	movq	%r13, %rdi
	movl	%eax, %r12d
	call	EVP_PKEY_free@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L253:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	RSA_free@PLT
	movq	%r13, %rdi
	call	EVP_PKEY_free@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L251:
	.cfi_restore_state
	movl	$107, %r8d
	movl	$67, %edx
	movl	$204, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L252:
	.cfi_restore_state
	movl	$111, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$6, %edx
	xorl	%r12d, %r12d
	movl	$204, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE967:
	.size	SSL_use_RSAPrivateKey, .-SSL_use_RSAPrivateKey
	.p2align 4
	.globl	SSL_use_RSAPrivateKey_file
	.type	SSL_use_RSAPrivateKey_file, @function
SSL_use_RSAPrivateKey_file:
.LFB969:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	%edx, %ebx
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L263
	movq	%r14, %rcx
	movl	$3, %edx
	movl	$108, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	testl	%eax, %eax
	jle	.L264
	cmpl	$2, %ebx
	je	.L265
	cmpl	$1, %ebx
	jne	.L260
	movq	6136(%r13), %rdx
	movq	6144(%r13), %rcx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	PEM_read_bio_RSAPrivateKey@PLT
	movl	$9, %edx
	movq	%rax, %r14
.L259:
	testq	%r14, %r14
	je	.L266
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	SSL_use_RSAPrivateKey
	movq	%r14, %rdi
	movl	%eax, %r13d
	call	RSA_free@PLT
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L260:
	movl	$200, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$124, %edx
	xorl	%r13d, %r13d
	movl	$206, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
.L256:
	movq	%r12, %rdi
	call	BIO_free@PLT
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L263:
	.cfi_restore_state
	movl	$183, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$7, %edx
	xorl	%r13d, %r13d
	movl	$206, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L265:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	d2i_RSAPrivateKey_bio@PLT
	movl	$13, %edx
	movq	%rax, %r14
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L264:
	movl	$188, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$2, %edx
	xorl	%r13d, %r13d
	movl	$206, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L266:
	movl	$204, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$206, %esi
	xorl	%r13d, %r13d
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L256
	.cfi_endproc
.LFE969:
	.size	SSL_use_RSAPrivateKey_file, .-SSL_use_RSAPrivateKey_file
	.p2align 4
	.globl	SSL_use_RSAPrivateKey_ASN1
	.type	SSL_use_RSAPrivateKey_ASN1, @function
SSL_use_RSAPrivateKey_ASN1:
.LFB970:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	xorl	%edi, %edi
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -32(%rbp)
	leaq	-32(%rbp), %rsi
	call	d2i_RSAPrivateKey@PLT
	testq	%rax, %rax
	je	.L272
	movq	%r13, %rdi
	movq	%rax, %r12
	movq	%rax, %rsi
	call	SSL_use_RSAPrivateKey
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	RSA_free@PLT
.L267:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L273
	addq	$16, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L272:
	.cfi_restore_state
	movl	$222, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$13, %edx
	xorl	%r13d, %r13d
	movl	$205, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L267
.L273:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE970:
	.size	SSL_use_RSAPrivateKey_ASN1, .-SSL_use_RSAPrivateKey_ASN1
	.p2align 4
	.globl	SSL_use_PrivateKey
	.type	SSL_use_PrivateKey, @function
SSL_use_PrivateKey:
.LFB971:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L292
	movq	%rsi, %rbx
	movq	1168(%rdi), %r12
	leaq	-32(%rbp), %rsi
	movq	%rbx, %rdi
	call	ssl_cert_lookup_by_pkey@PLT
	testq	%rax, %rax
	je	.L293
	movq	-32(%rbp), %rax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12,%rdx,8), %rdi
	testq	%rdi, %rdi
	je	.L279
	call	X509_get0_pubkey@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L294
	movq	%rbx, %rsi
	call	EVP_PKEY_copy_parameters@PLT
	call	ERR_clear_error@PLT
	movq	%rbx, %rdi
	call	EVP_PKEY_id@PLT
	cmpl	$6, %eax
	je	.L295
.L281:
	movq	-32(%rbp), %rax
	movq	%rbx, %rsi
	leaq	(%rax,%rax,4), %rax
	movq	32(%r12,%rax,8), %rdi
	call	X509_check_private_key@PLT
	testl	%eax, %eax
	je	.L282
.L291:
	movq	-32(%rbp), %rax
.L279:
	leaq	(%rax,%rax,4), %rax
	movq	40(%r12,%rax,8), %rdi
	call	EVP_PKEY_free@PLT
	movq	%rbx, %rdi
	call	EVP_PKEY_up_ref@PLT
	movq	-32(%rbp), %rax
	leaq	(%rax,%rax,4), %rax
	salq	$3, %rax
	movq	%rbx, 40(%r12,%rax)
	leaq	32(%r12,%rax), %rax
	movq	%rax, (%r12)
	movl	$1, %eax
.L274:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L296
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L294:
	.cfi_restore_state
	movl	$141, %r8d
	movl	$65, %edx
	movl	$193, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L292:
	movl	$237, %r8d
	movl	$67, %edx
	movl	$201, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L295:
	movq	%rbx, %rdi
	call	EVP_PKEY_get0_RSA@PLT
	movq	%rax, %rdi
	call	RSA_flags@PLT
	testb	$1, %al
	je	.L281
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L293:
	movl	$133, %r8d
	movl	$247, %edx
	movl	$193, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L282:
	movq	-32(%rbp), %rdx
	movl	%eax, -36(%rbp)
	leaq	(%rdx,%rdx,4), %rdx
	movq	32(%r12,%rdx,8), %rdi
	call	X509_free@PLT
	movq	-32(%rbp), %rdx
	movl	-36(%rbp), %eax
	leaq	(%rdx,%rdx,4), %rdx
	movq	$0, 32(%r12,%rdx,8)
	jmp	.L274
.L296:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE971:
	.size	SSL_use_PrivateKey, .-SSL_use_PrivateKey
	.p2align 4
	.globl	SSL_use_PrivateKey_file
	.type	SSL_use_PrivateKey_file, @function
SSL_use_PrivateKey_file:
.LFB972:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	%edx, %ebx
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L306
	movq	%r14, %rcx
	movl	$3, %edx
	movl	$108, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	testl	%eax, %eax
	jle	.L307
	cmpl	$1, %ebx
	je	.L308
	cmpl	$2, %ebx
	jne	.L303
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	d2i_PrivateKey_bio@PLT
	movl	$13, %edx
	movq	%rax, %r14
.L302:
	testq	%r14, %r14
	je	.L309
	movq	1168(%r13), %rdi
	movq	%r14, %rsi
	call	ssl_set_pkey
	movq	%r14, %rdi
	movl	%eax, %r13d
	call	EVP_PKEY_free@PLT
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L303:
	movl	$269, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$124, %edx
	xorl	%r13d, %r13d
	movl	$203, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
.L299:
	movq	%r12, %rdi
	call	BIO_free@PLT
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L306:
	.cfi_restore_state
	movl	$252, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$7, %edx
	xorl	%r13d, %r13d
	movl	$203, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L308:
	movq	6136(%r13), %rdx
	movq	6144(%r13), %rcx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	PEM_read_bio_PrivateKey@PLT
	movl	$9, %edx
	movq	%rax, %r14
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L307:
	movl	$257, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$2, %edx
	xorl	%r13d, %r13d
	movl	$203, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L309:
	movl	$273, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$203, %esi
	xorl	%r13d, %r13d
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L299
	.cfi_endproc
.LFE972:
	.size	SSL_use_PrivateKey_file, .-SSL_use_PrivateKey_file
	.p2align 4
	.globl	SSL_use_PrivateKey_ASN1
	.type	SSL_use_PrivateKey_ASN1, @function
SSL_use_PrivateKey_ASN1:
.LFB973:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	xorl	%esi, %esi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdx, -48(%rbp)
	leaq	-48(%rbp), %rdx
	call	d2i_PrivateKey@PLT
	testq	%rax, %rax
	je	.L315
	movq	1168(%rbx), %rdi
	movq	%rax, %r12
	movq	%rax, %rsi
	call	ssl_set_pkey
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	EVP_PKEY_free@PLT
.L310:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L316
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L315:
	.cfi_restore_state
	movl	$292, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$13, %edx
	xorl	%r13d, %r13d
	movl	$202, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L310
.L316:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE973:
	.size	SSL_use_PrivateKey_ASN1, .-SSL_use_PrivateKey_ASN1
	.p2align 4
	.globl	SSL_CTX_use_certificate
	.type	SSL_CTX_use_certificate, @function
SSL_CTX_use_certificate:
.LFB974:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	testq	%rsi, %rsi
	je	.L322
	movq	%rsi, %rdx
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movl	$1, %r8d
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	call	ssl_security_cert@PLT
	movl	%eax, %edx
	cmpl	$1, %eax
	je	.L320
	movl	$310, %r8d
	movl	$171, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L320:
	.cfi_restore_state
	movq	320(%rbx), %rdi
	movq	%r12, %rsi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ssl_set_cert
	.p2align 4,,10
	.p2align 3
.L322:
	.cfi_restore_state
	movl	$305, %r8d
	movl	$67, %edx
	movl	$171, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE974:
	.size	SSL_CTX_use_certificate, .-SSL_CTX_use_certificate
	.p2align 4
	.globl	SSL_CTX_use_certificate_file
	.type	SSL_CTX_use_certificate_file, @function
SSL_CTX_use_certificate_file:
.LFB976:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	%edx, %ebx
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movl	$385, %r8d
	movl	$7, %edx
	leaq	.LC0(%rip), %rcx
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L333
	movq	%r14, %rcx
	movl	$3, %edx
	movl	$108, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	testl	%eax, %eax
	jle	.L334
	cmpl	$2, %ebx
	je	.L335
	cmpl	$1, %ebx
	jne	.L329
	movq	176(%r13), %rdx
	movq	184(%r13), %rcx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	PEM_read_bio_X509@PLT
	movl	$9, %edx
	movq	%rax, %r14
.L328:
	testq	%r14, %r14
	je	.L336
	movq	%r14, %rdx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	xorl	%edi, %edi
	call	ssl_security_cert@PLT
	movl	%eax, %edx
	cmpl	$1, %eax
	je	.L331
	movl	$310, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$171, %esi
	xorl	%r13d, %r13d
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L334:
	movl	$390, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$2, %edx
.L333:
	movl	$173, %esi
	movl	$20, %edi
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	call	ERR_put_error@PLT
.L325:
	movq	%r14, %rdi
	call	X509_free@PLT
	movq	%r12, %rdi
	call	BIO_free@PLT
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L329:
	.cfi_restore_state
	movl	$401, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$124, %edx
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L331:
	movq	320(%r13), %rdi
	movq	%r14, %rsi
	call	ssl_set_cert
	movl	%eax, %r13d
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L335:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	d2i_X509_bio@PLT
	movl	$13, %edx
	movq	%rax, %r14
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L336:
	movl	$406, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$173, %esi
	xorl	%r13d, %r13d
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L325
	.cfi_endproc
.LFE976:
	.size	SSL_CTX_use_certificate_file, .-SSL_CTX_use_certificate_file
	.p2align 4
	.globl	SSL_CTX_use_certificate_ASN1
	.type	SSL_CTX_use_certificate_ASN1, @function
SSL_CTX_use_certificate_ASN1:
.LFB977:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	xorl	%edi, %edi
	subq	$24, %rsp
	movq	%rdx, -40(%rbp)
	movslq	%esi, %rdx
	leaq	-40(%rbp), %rsi
	call	d2i_X509@PLT
	testq	%rax, %rax
	je	.L343
	movq	%rax, %rdx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	xorl	%edi, %edi
	movq	%rax, %r12
	call	ssl_security_cert@PLT
	movl	%eax, %edx
	cmpl	$1, %eax
	jne	.L344
	movq	320(%rbx), %rdi
	movq	%r12, %rsi
	call	ssl_set_cert
	movl	%eax, %r13d
.L341:
	movq	%r12, %rdi
	call	X509_free@PLT
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L344:
	.cfi_restore_state
	movl	$310, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$171, %esi
	xorl	%r13d, %r13d
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L343:
	movl	$424, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$13, %edx
	xorl	%r13d, %r13d
	movl	$172, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE977:
	.size	SSL_CTX_use_certificate_ASN1, .-SSL_CTX_use_certificate_ASN1
	.p2align 4
	.globl	SSL_CTX_use_RSAPrivateKey
	.type	SSL_CTX_use_RSAPrivateKey, @function
SSL_CTX_use_RSAPrivateKey:
.LFB978:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	testq	%rsi, %rsi
	je	.L351
	movq	%rdi, %rbx
	call	EVP_PKEY_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L352
	movq	%r12, %rdi
	call	RSA_up_ref@PLT
	movq	%r12, %rdx
	movl	$6, %esi
	movq	%r13, %rdi
	call	EVP_PKEY_assign@PLT
	testl	%eax, %eax
	jle	.L353
	movq	320(%rbx), %rdi
	movq	%r13, %rsi
	call	ssl_set_pkey
	movq	%r13, %rdi
	movl	%eax, %r12d
	call	EVP_PKEY_free@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L353:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	RSA_free@PLT
	movq	%r13, %rdi
	call	EVP_PKEY_free@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L351:
	.cfi_restore_state
	movl	$440, %r8d
	movl	$67, %edx
	movl	$177, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L352:
	.cfi_restore_state
	movl	$444, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$6, %edx
	xorl	%r12d, %r12d
	movl	$177, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE978:
	.size	SSL_CTX_use_RSAPrivateKey, .-SSL_CTX_use_RSAPrivateKey
	.p2align 4
	.globl	SSL_CTX_use_RSAPrivateKey_file
	.type	SSL_CTX_use_RSAPrivateKey_file, @function
SSL_CTX_use_RSAPrivateKey_file:
.LFB979:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	%edx, %ebx
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L363
	movq	%r14, %rcx
	movl	$3, %edx
	movl	$108, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	testl	%eax, %eax
	jle	.L364
	cmpl	$2, %ebx
	je	.L365
	cmpl	$1, %ebx
	jne	.L360
	movq	176(%r13), %rdx
	movq	184(%r13), %rcx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	PEM_read_bio_RSAPrivateKey@PLT
	movl	$9, %edx
	movq	%rax, %r14
.L359:
	testq	%r14, %r14
	je	.L366
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	SSL_CTX_use_RSAPrivateKey
	movq	%r14, %rdi
	movl	%eax, %r13d
	call	RSA_free@PLT
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L360:
	movl	$485, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$124, %edx
	xorl	%r13d, %r13d
	movl	$179, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
.L356:
	movq	%r12, %rdi
	call	BIO_free@PLT
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L363:
	.cfi_restore_state
	movl	$468, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$7, %edx
	xorl	%r13d, %r13d
	movl	$179, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L365:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	d2i_RSAPrivateKey_bio@PLT
	movl	$13, %edx
	movq	%rax, %r14
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L364:
	movl	$473, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$2, %edx
	xorl	%r13d, %r13d
	movl	$179, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L366:
	movl	$489, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$179, %esi
	xorl	%r13d, %r13d
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L356
	.cfi_endproc
.LFE979:
	.size	SSL_CTX_use_RSAPrivateKey_file, .-SSL_CTX_use_RSAPrivateKey_file
	.p2align 4
	.globl	SSL_CTX_use_RSAPrivateKey_ASN1
	.type	SSL_CTX_use_RSAPrivateKey_ASN1, @function
SSL_CTX_use_RSAPrivateKey_ASN1:
.LFB980:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	xorl	%edi, %edi
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -32(%rbp)
	leaq	-32(%rbp), %rsi
	call	d2i_RSAPrivateKey@PLT
	testq	%rax, %rax
	je	.L372
	movq	%r13, %rdi
	movq	%rax, %r12
	movq	%rax, %rsi
	call	SSL_CTX_use_RSAPrivateKey
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	RSA_free@PLT
.L367:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L373
	addq	$16, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L372:
	.cfi_restore_state
	movl	$508, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$13, %edx
	xorl	%r13d, %r13d
	movl	$178, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L367
.L373:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE980:
	.size	SSL_CTX_use_RSAPrivateKey_ASN1, .-SSL_CTX_use_RSAPrivateKey_ASN1
	.p2align 4
	.globl	SSL_CTX_use_PrivateKey
	.type	SSL_CTX_use_PrivateKey, @function
SSL_CTX_use_PrivateKey:
.LFB981:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L392
	movq	%rsi, %rbx
	movq	320(%rdi), %r12
	leaq	-32(%rbp), %rsi
	movq	%rbx, %rdi
	call	ssl_cert_lookup_by_pkey@PLT
	testq	%rax, %rax
	je	.L393
	movq	-32(%rbp), %rax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12,%rdx,8), %rdi
	testq	%rdi, %rdi
	je	.L379
	call	X509_get0_pubkey@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L394
	movq	%rbx, %rsi
	call	EVP_PKEY_copy_parameters@PLT
	call	ERR_clear_error@PLT
	movq	%rbx, %rdi
	call	EVP_PKEY_id@PLT
	cmpl	$6, %eax
	je	.L395
.L381:
	movq	-32(%rbp), %rax
	movq	%rbx, %rsi
	leaq	(%rax,%rax,4), %rax
	movq	32(%r12,%rax,8), %rdi
	call	X509_check_private_key@PLT
	testl	%eax, %eax
	je	.L382
.L391:
	movq	-32(%rbp), %rax
.L379:
	leaq	(%rax,%rax,4), %rax
	movq	40(%r12,%rax,8), %rdi
	call	EVP_PKEY_free@PLT
	movq	%rbx, %rdi
	call	EVP_PKEY_up_ref@PLT
	movq	-32(%rbp), %rax
	leaq	(%rax,%rax,4), %rax
	salq	$3, %rax
	movq	%rbx, 40(%r12,%rax)
	leaq	32(%r12,%rax), %rax
	movq	%rax, (%r12)
	movl	$1, %eax
.L374:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L396
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L394:
	.cfi_restore_state
	movl	$141, %r8d
	movl	$65, %edx
	movl	$193, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L392:
	movl	$521, %r8d
	movl	$67, %edx
	movl	$174, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L395:
	movq	%rbx, %rdi
	call	EVP_PKEY_get0_RSA@PLT
	movq	%rax, %rdi
	call	RSA_flags@PLT
	testb	$1, %al
	je	.L381
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L393:
	movl	$133, %r8d
	movl	$247, %edx
	movl	$193, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L382:
	movq	-32(%rbp), %rdx
	movl	%eax, -36(%rbp)
	leaq	(%rdx,%rdx,4), %rdx
	movq	32(%r12,%rdx,8), %rdi
	call	X509_free@PLT
	movq	-32(%rbp), %rdx
	movl	-36(%rbp), %eax
	leaq	(%rdx,%rdx,4), %rdx
	movq	$0, 32(%r12,%rdx,8)
	jmp	.L374
.L396:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE981:
	.size	SSL_CTX_use_PrivateKey, .-SSL_CTX_use_PrivateKey
	.p2align 4
	.globl	SSL_CTX_use_PrivateKey_file
	.type	SSL_CTX_use_PrivateKey_file, @function
SSL_CTX_use_PrivateKey_file:
.LFB982:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	%edx, %ebx
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L406
	movq	%r14, %rcx
	movl	$3, %edx
	movl	$108, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	testl	%eax, %eax
	jle	.L407
	cmpl	$1, %ebx
	je	.L408
	cmpl	$2, %ebx
	jne	.L403
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	d2i_PrivateKey_bio@PLT
	movl	$13, %edx
	movq	%rax, %r14
.L402:
	testq	%r14, %r14
	je	.L409
	movq	320(%r13), %rdi
	movq	%r14, %rsi
	call	ssl_set_pkey
	movq	%r14, %rdi
	movl	%eax, %r13d
	call	EVP_PKEY_free@PLT
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L403:
	movl	$552, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$124, %edx
	xorl	%r13d, %r13d
	movl	$176, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
.L399:
	movq	%r12, %rdi
	call	BIO_free@PLT
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L406:
	.cfi_restore_state
	movl	$535, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$7, %edx
	xorl	%r13d, %r13d
	movl	$176, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L408:
	movq	176(%r13), %rdx
	movq	184(%r13), %rcx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	PEM_read_bio_PrivateKey@PLT
	movl	$9, %edx
	movq	%rax, %r14
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L407:
	movl	$540, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$2, %edx
	xorl	%r13d, %r13d
	movl	$176, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L409:
	movl	$556, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$176, %esi
	xorl	%r13d, %r13d
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L399
	.cfi_endproc
.LFE982:
	.size	SSL_CTX_use_PrivateKey_file, .-SSL_CTX_use_PrivateKey_file
	.p2align 4
	.globl	SSL_CTX_use_PrivateKey_ASN1
	.type	SSL_CTX_use_PrivateKey_ASN1, @function
SSL_CTX_use_PrivateKey_ASN1:
.LFB983:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	xorl	%esi, %esi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdx, -48(%rbp)
	leaq	-48(%rbp), %rdx
	call	d2i_PrivateKey@PLT
	testq	%rax, %rax
	je	.L415
	movq	320(%rbx), %rdi
	movq	%rax, %r12
	movq	%rax, %rsi
	call	ssl_set_pkey
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	EVP_PKEY_free@PLT
.L410:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L416
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L415:
	.cfi_restore_state
	movl	$575, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$13, %edx
	xorl	%r13d, %r13d
	movl	$175, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L410
.L416:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE983:
	.size	SSL_CTX_use_PrivateKey_ASN1, .-SSL_CTX_use_PrivateKey_ASN1
	.p2align 4
	.globl	SSL_CTX_use_certificate_chain_file
	.type	SSL_CTX_use_certificate_chain_file, @function
SSL_CTX_use_certificate_chain_file:
.LFB985:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	xorl	%esi, %esi
	jmp	use_certificate_chain_file
	.cfi_endproc
.LFE985:
	.size	SSL_CTX_use_certificate_chain_file, .-SSL_CTX_use_certificate_chain_file
	.p2align 4
	.globl	SSL_use_certificate_chain_file
	.type	SSL_use_certificate_chain_file, @function
SSL_use_certificate_chain_file:
.LFB986:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	xorl	%edi, %edi
	jmp	use_certificate_chain_file
	.cfi_endproc
.LFE986:
	.size	SSL_use_certificate_chain_file, .-SSL_use_certificate_chain_file
	.p2align 4
	.globl	SSL_CTX_use_serverinfo_ex
	.type	SSL_CTX_use_serverinfo_ex, @function
SSL_CTX_use_serverinfo_ex:
.LFB993:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	testq	%rdx, %rdx
	sete	%dl
	testq	%rcx, %rcx
	sete	%al
	orb	%al, %dl
	jne	.L428
	movq	%rdi, %r13
	testq	%rdi, %rdi
	je	.L428
	leal	-1(%rsi), %eax
	movl	%esi, %r15d
	cmpl	$1, %eax
	ja	.L424
	movq	%rcx, %r12
	testq	%rcx, %rcx
	js	.L424
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movl	%r15d, %edi
	call	serverinfo_process_buffer.part.0
	testl	%eax, %eax
	je	.L424
	movq	320(%r13), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L433
	movq	24(%rax), %rdi
	movl	$879, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r12, %rsi
	call	CRYPTO_realloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L434
	movq	320(%r13), %rax
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	(%rax), %rax
	movq	%rdi, 24(%rax)
	call	memcpy@PLT
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	320(%r13), %rax
	movl	%r15d, %edi
	movq	(%rax), %rax
	movq	%r12, 32(%rax)
	call	serverinfo_process_buffer.part.0
	testl	%eax, %eax
	je	.L435
	addq	$16, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L428:
	.cfi_restore_state
	movl	$867, %r8d
	movl	$67, %edx
	movl	$543, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L419:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L424:
	.cfi_restore_state
	movl	$872, %r8d
	movl	$388, %edx
	movl	$543, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L433:
	.cfi_restore_state
	movl	$876, %r8d
	movl	$68, %edx
	movl	$543, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L434:
	movl	$882, %r8d
	movl	$65, %edx
	movl	$543, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L419
.L435:
	movl	$895, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$388, %edx
	movl	%eax, -36(%rbp)
	movl	$543, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	movl	-36(%rbp), %eax
	jmp	.L419
	.cfi_endproc
.LFE993:
	.size	SSL_CTX_use_serverinfo_ex, .-SSL_CTX_use_serverinfo_ex
	.p2align 4
	.globl	SSL_CTX_use_serverinfo
	.type	SSL_CTX_use_serverinfo, @function
SSL_CTX_use_serverinfo:
.LFB994:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	subq	$24, %rsp
	testq	%rsi, %rsi
	sete	%dl
	testq	%r12, %r12
	sete	%al
	orb	%al, %dl
	jne	.L448
	movq	%rdi, %r13
	testq	%rdi, %rdi
	je	.L448
	testq	%r12, %r12
	js	.L440
	movq	%rsi, %r14
	movq	%rsi, %rcx
	movq	%r12, %rax
.L442:
	cmpq	$3, %rax
	jbe	.L440
	leaq	-4(%rax), %rdx
	movzwl	2(%rcx), %eax
	rolw	$8, %ax
	movzwl	%ax, %eax
	cmpq	%rax, %rdx
	jb	.L440
	subq	%rax, %rdx
	leaq	4(%rcx,%rax), %rcx
	movq	%rdx, %rax
	jne	.L442
	movq	320(%r13), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L453
	movq	24(%rax), %rdi
	movl	$879, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r12, %rsi
	call	CRYPTO_realloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L454
	movq	320(%r13), %rax
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	(%rax), %rax
	movq	%rdi, 24(%rax)
	call	memcpy@PLT
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	320(%r13), %rax
	movl	$1, %edi
	movq	(%rax), %rax
	movq	%r12, 32(%rax)
	call	serverinfo_process_buffer.part.0
	testl	%eax, %eax
	je	.L455
	addq	$24, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L448:
	.cfi_restore_state
	movl	$867, %r8d
	movl	$67, %edx
	movl	$543, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L436:
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L440:
	.cfi_restore_state
	movl	$872, %r8d
	movl	$388, %edx
	movl	$543, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L453:
	movl	$876, %r8d
	movl	$68, %edx
	movl	$543, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L454:
	movl	$882, %r8d
	movl	$65, %edx
	movl	$543, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L436
.L455:
	movl	$895, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$388, %edx
	movl	%eax, -36(%rbp)
	movl	$543, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	movl	-36(%rbp), %eax
	jmp	.L436
	.cfi_endproc
.LFE994:
	.size	SSL_CTX_use_serverinfo, .-SSL_CTX_use_serverinfo
	.p2align 4
	.globl	SSL_CTX_use_serverinfo_file
	.type	SSL_CTX_use_serverinfo_file, @function
SSL_CTX_use_serverinfo_file:
.LFB995:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -200(%rbp)
	movdqa	.LC1(%rip), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -96(%rbp)
	movl	$32, %eax
	movdqa	.LC2(%rip), %xmm0
	movq	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movw	%ax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	testq	%rdi, %rdi
	je	.L481
	movq	%rsi, %r12
	testq	%rsi, %rsi
	je	.L481
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L493
	movq	%r12, %rcx
	movl	$3, %edx
	movl	$108, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	testl	%eax, %eax
	jle	.L494
	leaq	-120(%rbp), %rax
	movq	%r14, -136(%rbp)
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	movq	%rax, -152(%rbp)
	leaq	-128(%rbp), %rax
	xorl	%r13d, %r13d
	movq	%rax, -144(%rbp)
	leaq	-104(%rbp), %rax
	movq	%rax, -168(%rbp)
	leaq	-112(%rbp), %rax
	movq	%rax, -160(%rbp)
	leaq	-96(%rbp), %rax
	movq	$0, -184(%rbp)
	movq	%rax, -176(%rbp)
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L499:
	cmpq	$16, %r14
	jbe	.L495
	leaq	-80(%rbp), %rsi
	movl	$17, %edx
	movq	%r15, %rdi
	call	strncmp@PLT
	testl	%eax, %eax
	jne	.L496
	movq	-120(%rbp), %rsi
	cmpq	$7, %rsi
	jle	.L490
	movq	-128(%rbp), %rcx
	movzbl	6(%rcx), %edx
	movzbl	7(%rcx), %ecx
	sall	$8, %edx
	addl	%ecx, %edx
	leaq	-8(%rsi), %rcx
	movslq	%edx, %rdx
	cmpq	%rcx, %rdx
	jne	.L490
	leaq	(%r12,%rbx), %r8
	movl	$999, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r13, %rdi
	addq	%r8, %rsi
	movq	%r8, -192(%rbp)
	call	CRYPTO_realloc@PLT
	movq	-192(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L476
	testq	%rbx, %rbx
	jne	.L477
	movq	%r12, %rdi
.L475:
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rsi
	addq	%r15, %rdi
	movq	%r15, %r13
	call	memcpy@PLT
	movq	-120(%rbp), %rax
	movq	-112(%rbp), %rdi
	movl	$1019, %edx
	leaq	.LC0(%rip), %rsi
	addq	%rbx, %rax
	addq	%rax, %r12
	call	CRYPTO_free@PLT
	movq	-104(%rbp), %rdi
	movl	$1021, %edx
	leaq	.LC0(%rip), %rsi
	movq	$0, -112(%rbp)
	call	CRYPTO_free@PLT
	movq	-128(%rbp), %rdi
	movl	$1023, %edx
	leaq	.LC0(%rip), %rsi
	movq	$0, -104(%rbp)
	call	CRYPTO_free@PLT
	addq	$1, -184(%rbp)
	movq	$0, -128(%rbp)
.L461:
	movq	-152(%rbp), %r8
	movq	-144(%rbp), %rcx
	movq	-168(%rbp), %rdx
	movq	-160(%rbp), %rsi
	movq	-136(%rbp), %rdi
	call	PEM_read_bio@PLT
	testl	%eax, %eax
	je	.L497
	movq	-112(%rbp), %r15
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%rax, %r14
	cmpq	$14, %rax
	jbe	.L498
	movq	-176(%rbp), %rsi
	movl	$15, %edx
	movq	%r15, %rdi
	call	strncmp@PLT
	testl	%eax, %eax
	jne	.L499
	movq	-120(%rbp), %rcx
	cmpq	$3, %rcx
	jle	.L489
	movq	-128(%rbp), %rsi
	movzbl	2(%rsi), %edx
	movzbl	3(%rsi), %esi
	sall	$8, %edx
	addl	%esi, %edx
	leaq	-4(%rcx), %rsi
	movslq	%edx, %rdx
	cmpq	%rsi, %rdx
	jne	.L489
	leaq	4(%rcx,%r12), %rsi
	leaq	.LC0(%rip), %rdx
	movl	$999, %ecx
	movq	%r13, %rdi
	call	CRYPTO_realloc@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L476
	movl	$4, %ebx
	leaq	4(%r12), %r8
.L477:
	movl	$-805240832, (%r15,%r12)
	movq	%r8, %rdi
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L481:
	movl	$67, %edx
	movl	$337, %esi
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	movl	$924, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$20, %edi
	call	ERR_put_error@PLT
	xorl	%r9d, %r9d
.L459:
	movq	-112(%rbp), %rdi
	movl	$1031, %edx
	leaq	.LC0(%rip), %rsi
	movl	%r9d, -136(%rbp)
	call	CRYPTO_free@PLT
	movq	-104(%rbp), %rdi
	movl	$1032, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-128(%rbp), %rdi
	movl	$1033, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$1034, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	movq	%r14, %rdi
	call	BIO_free@PLT
	movl	-136(%rbp), %r9d
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L500
	addq	$168, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L494:
	.cfi_restore_state
	movl	$934, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$2, %edx
	xorl	%r13d, %r13d
	movl	$337, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	xorl	%r9d, %r9d
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L493:
	movl	$930, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$7, %edx
	xorl	%r13d, %r13d
	movl	$337, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	xorl	%r9d, %r9d
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L497:
	cmpq	$0, -184(%rbp)
	movq	-136(%rbp), %r14
	je	.L501
	movq	-200(%rbp), %rdi
	movq	%r12, %rcx
	movq	%r13, %rdx
	movl	$2, %esi
	call	SSL_CTX_use_serverinfo_ex
	movl	%eax, %r9d
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L498:
	movq	-136(%rbp), %r14
	movl	$955, %r8d
.L491:
	movl	$392, %edx
	movl	$337, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r9d, %r9d
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L489:
	movq	-136(%rbp), %r14
	movl	%eax, -136(%rbp)
	movl	$981, %r8d
.L492:
	movl	$390, %edx
	movl	$337, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	-136(%rbp), %r9d
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L490:
	movq	-136(%rbp), %r14
	movl	$994, %r8d
	movl	%eax, -136(%rbp)
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L476:
	movl	$1002, %r8d
	movl	$65, %edx
	movl	$337, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	movq	-136(%rbp), %r14
	call	ERR_put_error@PLT
	xorl	%r9d, %r9d
	jmp	.L459
.L495:
	movq	-136(%rbp), %r14
	movl	$962, %r8d
	jmp	.L491
.L496:
	movl	$967, %r8d
	movl	$391, %edx
	movl	$337, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	movq	-136(%rbp), %r14
	call	ERR_put_error@PLT
	xorl	%r9d, %r9d
	jmp	.L459
.L501:
	movl	$947, %r8d
	movl	$389, %edx
	movl	$337, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	movl	%eax, -136(%rbp)
	call	ERR_put_error@PLT
	movl	-136(%rbp), %r9d
	jmp	.L459
.L500:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE995:
	.size	SSL_CTX_use_serverinfo_file, .-SSL_CTX_use_serverinfo_file
	.p2align 4
	.globl	SSL_use_cert_and_key
	.type	SSL_use_cert_and_key, @function
SSL_use_cert_and_key:
.LFB997:
	.cfi_startproc
	endbr64
	movl	%r8d, %r9d
	movq	%rcx, %r8
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	xorl	%esi, %esi
	jmp	ssl_set_cert_and_key
	.cfi_endproc
.LFE997:
	.size	SSL_use_cert_and_key, .-SSL_use_cert_and_key
	.p2align 4
	.globl	SSL_CTX_use_cert_and_key
	.type	SSL_CTX_use_cert_and_key, @function
SSL_CTX_use_cert_and_key:
.LFB998:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%r8d, %r9d
	movq	%rcx, %r8
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	xorl	%edi, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	ssl_set_cert_and_key
	.cfi_endproc
.LFE998:
	.size	SSL_CTX_use_cert_and_key, .-SSL_CTX_use_cert_and_key
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.quad	5641130466012710227
	.quad	9097699687157574
	.align 16
.LC2:
	.quad	5641130466012710227
	.quad	5931036338367647558
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
