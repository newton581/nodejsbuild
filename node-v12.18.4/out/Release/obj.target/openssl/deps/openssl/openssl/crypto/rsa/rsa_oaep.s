	.file	"rsa_oaep.c"
	.text
	.p2align 4
	.globl	PKCS1_MGF1
	.type	PKCS1_MGF1, @function
PKCS1_MGF1:
.LFB485:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$168, %rsp
	movq	%rdi, -200(%rbp)
	movq	%rdx, -176(%rbp)
	movq	%rcx, -184(%rbp)
	movq	%r8, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L33
	movq	-168(%rbp), %rdi
	call	EVP_MD_size@PLT
	testl	%eax, %eax
	js	.L33
	testq	%rbx, %rbx
	jle	.L8
	cltq
	movq	-200(%rbp), %r15
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	movq	%rax, -152(%rbp)
	leaq	-132(%rbp), %rax
	movq	%rax, -192(%rbp)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L35:
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	EVP_DigestFinal_ex@PLT
	movq	-160(%rbp), %rcx
	testl	%eax, %eax
	je	.L33
	addq	$1, %r13
	addq	-152(%rbp), %r15
	cmpq	%rbx, %rcx
	jge	.L8
	movq	%rcx, %r14
.L7:
	movq	-168(%rbp), %rsi
	movl	%r13d, %eax
	xorl	%edx, %edx
	movq	%r12, %rdi
	bswap	%eax
	movl	%eax, -132(%rbp)
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	je	.L33
	movq	-184(%rbp), %rdx
	movq	-176(%rbp), %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L33
	movq	-192(%rbp), %rsi
	movl	$4, %edx
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L33
	movq	-152(%rbp), %rax
	leaq	(%rax,%r14), %rcx
	cmpq	%rbx, %rcx
	movq	%rcx, -160(%rbp)
	jle	.L35
	leaq	-128(%rbp), %r13
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	je	.L2
	movq	-200(%rbp), %rdi
	subq	%r14, %rbx
	movq	%r13, %rsi
	movq	%rbx, %rdx
	addq	%r14, %rdi
	xorl	%r14d, %r14d
	call	memcpy@PLT
.L4:
	movl	$64, %esi
	movq	%r13, %rdi
	call	OPENSSL_cleanse@PLT
	movq	%r12, %rdi
	call	EVP_MD_CTX_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L36
	addq	$168, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	leaq	-128(%rbp), %r13
.L2:
	movl	$-1, %r14d
	jmp	.L4
.L8:
	xorl	%r14d, %r14d
	leaq	-128(%rbp), %r13
	jmp	.L4
.L36:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE485:
	.size	PKCS1_MGF1, .-PKCS1_MGF1
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/rsa/rsa_oaep.c"
	.text
	.p2align 4
	.globl	RSA_padding_add_PKCS1_OAEP_mgf1
	.type	RSA_padding_add_PKCS1_OAEP_mgf1, @function
RSA_padding_add_PKCS1_OAEP_mgf1:
.LFB482:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leal	-1(%rsi), %ebx
	subq	$136, %rsp
	movq	%r8, -144(%rbp)
	movq	16(%rbp), %r8
	movq	%rdx, -160(%rbp)
	movq	24(%rbp), %r15
	movl	%r9d, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L83
.L38:
	testq	%r15, %r15
	movq	%r8, %rdi
	movq	%r8, -136(%rbp)
	cmove	%r8, %r15
	call	EVP_MD_size@PLT
	movq	-136(%rbp), %r8
	leal	(%rax,%rax), %r11d
	movl	%eax, %r12d
	movl	%ebx, %eax
	subl	%r11d, %eax
	cmpl	%r14d, %eax
	movl	%eax, -164(%rbp)
	jle	.L84
	cmpl	%ebx, %r11d
	jge	.L85
	movslq	%r12d, %rax
	movb	$0, 0(%r13)
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	leaq	1(%r13,%rax), %r10
	movslq	-152(%rbp), %rsi
	movq	-144(%rbp), %rdi
	movl	%r11d, -176(%rbp)
	movq	%r10, %rdx
	movq	%r10, -152(%rbp)
	movq	%rax, -136(%rbp)
	call	EVP_Digest@PLT
	movq	-152(%rbp), %r10
	movl	-176(%rbp), %r11d
	testl	%eax, %eax
	jne	.L43
.L45:
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	leaq	-128(%rbp), %rdi
	xorl	%r15d, %r15d
.L44:
	movl	$64, %esi
	call	OPENSSL_cleanse@PLT
	movl	$104, %ecx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
.L37:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L86
	addq	$136, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	movl	%ebx, %ecx
	movq	-136(%rbp), %rsi
	leaq	1(%r13), %rax
	movq	%r10, -144(%rbp)
	subl	%r14d, %ecx
	movq	%rax, -176(%rbp)
	subl	%r11d, %ecx
	leaq	(%r10,%rsi), %rdi
	xorl	%esi, %esi
	leal	-1(%rcx), %edx
	movl	%ecx, -152(%rbp)
	movslq	%edx, %rdx
	call	memset@PLT
	movslq	%r14d, %rax
	movslq	%ebx, %rdi
	movl	%r14d, %edx
	movl	-152(%rbp), %ecx
	movq	-144(%rbp), %r10
	subq	%rax, %rdi
	movq	-160(%rbp), %rsi
	subq	-136(%rbp), %rdi
	addl	%r12d, %ecx
	addq	%r10, %rdi
	movslq	%ecx, %rcx
	movb	$1, -1(%r10,%rcx)
	call	memcpy@PLT
	movl	%r12d, %esi
	leaq	1(%r13), %rdi
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jle	.L45
	movl	-164(%rbp), %r9d
	movl	$85, %edx
	leaq	.LC0(%rip), %rsi
	addl	%r12d, %r9d
	movslq	%r9d, %r14
	movl	%r9d, -152(%rbp)
	movq	%r14, %rdi
	call	CRYPTO_malloc@PLT
	movl	-152(%rbp), %r9d
	movq	-144(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L87
	movq	-136(%rbp), %rcx
	movq	%r15, %r8
	movq	%r14, %rsi
	movq	%rax, %rdi
	leaq	1(%r13), %rdx
	movl	%r9d, -152(%rbp)
	movq	%r10, -144(%rbp)
	call	PKCS1_MGF1
	testl	%eax, %eax
	js	.L60
	movl	-152(%rbp), %r9d
	movq	-144(%rbp), %r10
	testl	%r9d, %r9d
	jle	.L52
	movq	-136(%rbp), %rsi
	leal	-1(%r9), %eax
	leaq	17(%r13,%rsi), %rdx
	cmpq	%rdx, %rbx
	leaq	16(%rbx), %rdx
	setnb	%cl
	cmpq	%rdx, %r10
	setnb	%dl
	orb	%dl, %cl
	je	.L49
	cmpl	$14, %eax
	jbe	.L49
	movl	%r9d, %edx
	xorl	%eax, %eax
	shrl	$4, %edx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L50:
	movdqu	(%r10,%rax), %xmm0
	movdqu	(%rbx,%rax), %xmm1
	pxor	%xmm1, %xmm0
	movups	%xmm0, (%r10,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L50
	movl	%r9d, %eax
	andl	$-16, %eax
	testb	$15, %r9b
	je	.L52
	movl	%eax, %edx
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r10,%rdx)
	leal	1(%rax), %edx
	cmpl	%edx, %r9d
	jle	.L52
	movslq	%edx, %rdx
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r10,%rdx)
	leal	2(%rax), %edx
	cmpl	%edx, %r9d
	jle	.L52
	movslq	%edx, %rdx
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r10,%rdx)
	leal	3(%rax), %edx
	cmpl	%edx, %r9d
	jle	.L52
	movslq	%edx, %rdx
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r10,%rdx)
	leal	4(%rax), %edx
	cmpl	%edx, %r9d
	jle	.L52
	movslq	%edx, %rdx
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r10,%rdx)
	leal	5(%rax), %edx
	cmpl	%edx, %r9d
	jle	.L52
	movslq	%edx, %rdx
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r10,%rdx)
	leal	6(%rax), %edx
	cmpl	%edx, %r9d
	jle	.L52
	movslq	%edx, %rdx
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r10,%rdx)
	leal	7(%rax), %edx
	cmpl	%edx, %r9d
	jle	.L52
	movslq	%edx, %rdx
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r10,%rdx)
	leal	8(%rax), %edx
	cmpl	%edx, %r9d
	jle	.L52
	movslq	%edx, %rdx
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r10,%rdx)
	leal	9(%rax), %edx
	cmpl	%edx, %r9d
	jle	.L52
	movslq	%edx, %rdx
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r10,%rdx)
	leal	10(%rax), %edx
	cmpl	%edx, %r9d
	jle	.L52
	movslq	%edx, %rdx
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r10,%rdx)
	leal	11(%rax), %edx
	cmpl	%edx, %r9d
	jle	.L52
	movslq	%edx, %rdx
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r10,%rdx)
	leal	12(%rax), %edx
	cmpl	%edx, %r9d
	jle	.L52
	movslq	%edx, %rdx
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r10,%rdx)
	leal	13(%rax), %edx
	cmpl	%edx, %r9d
	jle	.L52
	movslq	%edx, %rdx
	addl	$14, %eax
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r10,%rdx)
	cmpl	%eax, %r9d
	jle	.L52
	cltq
	movzbl	(%rbx,%rax), %edx
	xorb	%dl, (%r10,%rax)
	.p2align 4,,10
	.p2align 3
.L52:
	movq	-136(%rbp), %rsi
	movq	%r15, %r8
	movq	%r14, %rcx
	movq	%r10, %rdx
	leaq	-128(%rbp), %rdi
	xorl	%r15d, %r15d
	movq	%rdi, -136(%rbp)
	call	PKCS1_MGF1
	movq	-136(%rbp), %rdi
	testl	%eax, %eax
	js	.L44
	testl	%r12d, %r12d
	jle	.L62
	leal	-1(%r12), %eax
	cmpl	$14, %eax
	jbe	.L63
	movdqu	1(%r13), %xmm0
	movl	%r12d, %eax
	pxor	-128(%rbp), %xmm0
	shrl	$4, %eax
	movups	%xmm0, 1(%r13)
	cmpl	$1, %eax
	je	.L57
	movdqu	17(%r13), %xmm0
	pxor	-112(%rbp), %xmm0
	movups	%xmm0, 17(%r13)
	cmpl	$2, %eax
	je	.L57
	movdqu	33(%r13), %xmm0
	pxor	-96(%rbp), %xmm0
	movups	%xmm0, 33(%r13)
	cmpl	$3, %eax
	je	.L57
	movdqu	49(%r13), %xmm0
	pxor	-80(%rbp), %xmm0
	movups	%xmm0, 49(%r13)
.L57:
	movl	%r12d, %eax
	andl	$-16, %eax
	testb	$15, %r12b
	je	.L62
.L56:
	movq	-176(%rbp), %rsi
	movslq	%eax, %rdx
	movzbl	-128(%rbp,%rdx), %ecx
	xorb	%cl, (%rsi,%rdx)
	leal	1(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L62
	movslq	%edx, %rdx
	movzbl	-128(%rbp,%rdx), %ecx
	xorb	%cl, (%rsi,%rdx)
	leal	2(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L62
	movslq	%edx, %rdx
	movzbl	-128(%rbp,%rdx), %ecx
	xorb	%cl, (%rsi,%rdx)
	leal	3(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L62
	movslq	%edx, %rdx
	movzbl	-128(%rbp,%rdx), %ecx
	xorb	%cl, (%rsi,%rdx)
	leal	4(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L62
	movslq	%edx, %rdx
	movzbl	-128(%rbp,%rdx), %ecx
	xorb	%cl, (%rsi,%rdx)
	leal	5(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L62
	movslq	%edx, %rdx
	movzbl	-128(%rbp,%rdx), %ecx
	xorb	%cl, (%rsi,%rdx)
	leal	6(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L62
	movslq	%edx, %rdx
	movzbl	-128(%rbp,%rdx), %ecx
	xorb	%cl, (%rsi,%rdx)
	leal	7(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L62
	movslq	%edx, %rdx
	movzbl	-128(%rbp,%rdx), %ecx
	xorb	%cl, (%rsi,%rdx)
	leal	8(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L62
	movslq	%edx, %rdx
	movzbl	-128(%rbp,%rdx), %ecx
	xorb	%cl, (%rsi,%rdx)
	leal	9(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L62
	movslq	%edx, %rdx
	movzbl	-128(%rbp,%rdx), %ecx
	xorb	%cl, (%rsi,%rdx)
	leal	10(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L62
	movslq	%edx, %rdx
	movzbl	-128(%rbp,%rdx), %ecx
	xorb	%cl, (%rsi,%rdx)
	leal	11(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L62
	movslq	%edx, %rdx
	movzbl	-128(%rbp,%rdx), %ecx
	xorb	%cl, (%rsi,%rdx)
	leal	12(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L62
	movslq	%edx, %rdx
	movzbl	-128(%rbp,%rdx), %ecx
	xorb	%cl, (%rsi,%rdx)
	leal	13(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L62
	movslq	%edx, %rdx
	addl	$14, %eax
	movzbl	-128(%rbp,%rdx), %ecx
	xorb	%cl, (%rsi,%rdx)
	cmpl	%eax, %r12d
	jle	.L62
	cltq
	movzbl	-128(%rbp,%rax), %edx
	xorb	%dl, (%rsi,%rax)
	.p2align 4,,10
	.p2align 3
.L62:
	movl	$1, %r15d
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L85:
	movl	$67, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$120, %edx
	xorl	%r15d, %r15d
	movl	$154, %esi
	movl	$4, %edi
	call	ERR_put_error@PLT
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L84:
	movl	$61, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$110, %edx
	xorl	%r15d, %r15d
	movl	$154, %esi
	movl	$4, %edi
	call	ERR_put_error@PLT
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L83:
	call	EVP_sha1@PLT
	movq	%rax, %r8
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L60:
	xorl	%r15d, %r15d
	leaq	-128(%rbp), %rdi
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L87:
	movl	$4, %edi
	movl	$87, %r8d
	movl	$65, %edx
	xorl	%r15d, %r15d
	leaq	.LC0(%rip), %rcx
	movl	$154, %esi
	call	ERR_put_error@PLT
	leaq	-128(%rbp), %rdi
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L49:
	movl	%eax, %edx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L54:
	movzbl	(%rbx,%rax), %ecx
	xorb	%cl, (%r10,%rax)
	movq	%rax, %rcx
	addq	$1, %rax
	cmpq	%rdx, %rcx
	jne	.L54
	jmp	.L52
.L63:
	xorl	%eax, %eax
	jmp	.L56
.L86:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE482:
	.size	RSA_padding_add_PKCS1_OAEP_mgf1, .-RSA_padding_add_PKCS1_OAEP_mgf1
	.p2align 4
	.globl	RSA_padding_check_PKCS1_OAEP_mgf1
	.type	RSA_padding_check_PKCS1_OAEP_mgf1, @function
RSA_padding_check_PKCS1_OAEP_mgf1:
.LFB484:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rbp), %rax
	movq	%rdi, -296(%rbp)
	movl	%esi, -240(%rbp)
	movq	32(%rbp), %rbx
	movq	%r9, -288(%rbp)
	movq	%rax, -232(%rbp)
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	testq	%rax, %rax
	je	.L166
.L89:
	movq	-232(%rbp), %rax
	testq	%rbx, %rbx
	movl	%r8d, -200(%rbp)
	movq	%rax, %rdi
	cmove	%rax, %rbx
	call	EVP_MD_size@PLT
	movl	-240(%rbp), %ecx
	movl	%eax, %r13d
	testl	%ecx, %ecx
	jle	.L131
	testl	%r15d, %r15d
	jle	.L131
	movl	-200(%rbp), %r8d
	cmpl	%r8d, %r15d
	jg	.L92
	leal	1(%rax), %eax
	movl	%eax, -216(%rbp)
	addl	%eax, %eax
	cmpl	%r8d, %eax
	jg	.L92
	movl	%r8d, %eax
	movl	$157, %edx
	leaq	.LC0(%rip), %rsi
	movl	%r8d, -200(%rbp)
	subl	%r13d, %eax
	movl	%eax, -248(%rbp)
	subl	$1, %eax
	movl	%eax, -244(%rbp)
	cltq
	movq	%rax, %rdi
	movq	%rax, -208(%rbp)
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	movslq	-200(%rbp), %rax
	movq	%rax, -200(%rbp)
	testq	%r12, %r12
	je	.L167
	movq	%rax, %rdi
	movl	$163, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, -212(%rbp)
	call	CRYPTO_malloc@PLT
	movl	-212(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, %r9
	je	.L168
	movq	-200(%rbp), %rax
	movslq	%r15d, %rcx
	addq	%r14, %rcx
	leaq	(%r9,%rax), %r14
	movq	%r14, %rdx
	.p2align 4,,10
	.p2align 3
.L97:
	movl	%r15d, %esi
	leal	-1(%r15), %eax
	subq	$1, %rdx
	notl	%esi
	andl	%esi, %eax
	sarl	$31, %eax
	movl	%eax, %esi
	addl	$1, %eax
	subq	%rax, %rcx
	subl	%eax, %r15d
	movl	%r14d, %eax
	notl	%esi
	subl	%edx, %eax
	andb	(%rcx), %sil
	movb	%sil, (%rdx)
	cmpl	%eax, %r8d
	jg	.L97
	leal	-1(%r8), %r10d
	testl	%r8d, %r8d
	movzbl	%sil, %esi
	movq	$-1, %rdx
	movq	%r10, %rax
	movq	-208(%rbp), %rcx
	movq	%r9, -280(%rbp)
	notq	%rax
	cmovle	%rdx, %rax
	negq	%r10
	addq	%r14, %rax
	testl	%r8d, %r8d
	movq	%rbx, %r8
	movq	%rax, -256(%rbp)
	movl	$0, %eax
	cmovle	%rax, %r10
	leal	-1(%rsi), %eax
	notl	%esi
	andl	%eax, %esi
	leaq	-192(%rbp), %rax
	sarl	$31, %esi
	addq	%r10, %r14
	movq	%rax, %rdi
	movq	%r10, -272(%rbp)
	movl	%esi, -212(%rbp)
	movslq	%r13d, %rsi
	leaq	(%r14,%rsi), %r15
	movq	%rsi, -264(%rbp)
	movq	%r15, %rdx
	movq	%rax, -224(%rbp)
	call	PKCS1_MGF1
	movq	-272(%rbp), %r10
	movq	-280(%rbp), %r9
	testl	%eax, %eax
	jne	.L109
	testl	%r13d, %r13d
	jle	.L104
	movq	-200(%rbp), %rdi
	leal	-1(%r13), %edx
	leaq	(%rdi,%r10), %rax
	leaq	16(%rdi,%r10), %rcx
	addq	%r9, %rax
	addq	%r9, %rcx
	cmpq	%rcx, -224(%rbp)
	leaq	-176(%rbp), %rcx
	setnb	%sil
	cmpq	%rcx, %rax
	setnb	%cl
	orb	%cl, %sil
	je	.L102
	cmpl	$14, %edx
	jbe	.L102
	movdqu	(%rax), %xmm0
	movl	%r13d, %edx
	pxor	-192(%rbp), %xmm0
	shrl	$4, %edx
	movaps	%xmm0, -192(%rbp)
	cmpl	$1, %edx
	je	.L103
	movdqu	16(%rax), %xmm0
	pxor	-176(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	cmpl	$2, %edx
	je	.L103
	movdqu	32(%rax), %xmm0
	pxor	-160(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	cmpl	$3, %edx
	je	.L103
	movdqu	48(%rax), %xmm0
	pxor	-144(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
.L103:
	movl	%r13d, %eax
	andl	$-16, %eax
	testb	$15, %r13b
	je	.L104
	movslq	%eax, %rdx
	movl	%eax, %ecx
	movzbl	(%r14,%rcx), %ecx
	xorb	%cl, -192(%rbp,%rdx)
	leal	1(%rax), %edx
	cmpl	%r13d, %edx
	jge	.L104
	movslq	%edx, %rdx
	movzbl	(%r14,%rdx), %ecx
	xorb	%cl, -192(%rbp,%rdx)
	leal	2(%rax), %edx
	cmpl	%r13d, %edx
	jge	.L104
	movslq	%edx, %rdx
	movzbl	(%r14,%rdx), %ecx
	xorb	%cl, -192(%rbp,%rdx)
	leal	3(%rax), %edx
	cmpl	%r13d, %edx
	jge	.L104
	movslq	%edx, %rdx
	movzbl	(%r14,%rdx), %ecx
	xorb	%cl, -192(%rbp,%rdx)
	leal	4(%rax), %edx
	cmpl	%edx, %r13d
	jle	.L104
	movslq	%edx, %rdx
	movzbl	(%r14,%rdx), %ecx
	xorb	%cl, -192(%rbp,%rdx)
	leal	5(%rax), %edx
	cmpl	%edx, %r13d
	jle	.L104
	movslq	%edx, %rdx
	movzbl	(%r14,%rdx), %ecx
	xorb	%cl, -192(%rbp,%rdx)
	leal	6(%rax), %edx
	cmpl	%edx, %r13d
	jle	.L104
	movslq	%edx, %rdx
	movzbl	(%r14,%rdx), %ecx
	xorb	%cl, -192(%rbp,%rdx)
	leal	7(%rax), %edx
	cmpl	%edx, %r13d
	jle	.L104
	movslq	%edx, %rdx
	movzbl	(%r14,%rdx), %ecx
	xorb	%cl, -192(%rbp,%rdx)
	leal	8(%rax), %edx
	cmpl	%r13d, %edx
	jge	.L104
	movslq	%edx, %rdx
	movzbl	(%r14,%rdx), %ecx
	xorb	%cl, -192(%rbp,%rdx)
	leal	9(%rax), %edx
	cmpl	%edx, %r13d
	jle	.L104
	movslq	%edx, %rdx
	movzbl	(%r14,%rdx), %ecx
	xorb	%cl, -192(%rbp,%rdx)
	leal	10(%rax), %edx
	cmpl	%r13d, %edx
	jge	.L104
	movslq	%edx, %rdx
	movzbl	(%r14,%rdx), %ecx
	xorb	%cl, -192(%rbp,%rdx)
	leal	11(%rax), %edx
	cmpl	%edx, %r13d
	jle	.L104
	movslq	%edx, %rdx
	movzbl	(%r14,%rdx), %ecx
	xorb	%cl, -192(%rbp,%rdx)
	leal	12(%rax), %edx
	cmpl	%edx, %r13d
	jle	.L104
	movslq	%edx, %rdx
	movzbl	(%r14,%rdx), %ecx
	xorb	%cl, -192(%rbp,%rdx)
	leal	13(%rax), %edx
	cmpl	%edx, %r13d
	jle	.L104
	movslq	%edx, %rdx
	addl	$14, %eax
	movzbl	(%r14,%rdx), %ecx
	xorb	%cl, -192(%rbp,%rdx)
	cmpl	%eax, %r13d
	jle	.L104
	cltq
	movzbl	(%r14,%rax), %edx
	xorb	%dl, -192(%rbp,%rax)
	.p2align 4,,10
	.p2align 3
.L104:
	movq	-264(%rbp), %rcx
	movq	-224(%rbp), %rdx
	movq	%rbx, %r8
	movq	%r12, %rdi
	movq	-208(%rbp), %rsi
	movq	%r10, -280(%rbp)
	movq	%r9, -272(%rbp)
	call	PKCS1_MGF1
	movq	-272(%rbp), %r9
	movq	-280(%rbp), %r10
	testl	%eax, %eax
	movl	%eax, %ebx
	jne	.L109
	movl	-244(%rbp), %edx
	testl	%edx, %edx
	jle	.L114
	movq	-200(%rbp), %rdi
	movq	-264(%rbp), %rsi
	movl	-248(%rbp), %eax
	leaq	(%rdi,%r10), %rdx
	leaq	16(%rdi,%r10), %rcx
	addq	%rsi, %rcx
	addq	%rsi, %rdx
	subl	$2, %eax
	addq	%r9, %rdx
	addq	%rcx, %r9
	leaq	16(%r12), %rcx
	cmpq	%r9, %r12
	setnb	%sil
	cmpq	%rcx, %rdx
	setnb	%cl
	orb	%cl, %sil
	je	.L111
	cmpl	$14, %eax
	jbe	.L111
	movl	-244(%rbp), %ecx
	xorl	%eax, %eax
	shrl	$4, %ecx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L112:
	movdqu	(%rdx,%rax), %xmm0
	movdqu	(%r12,%rax), %xmm1
	pxor	%xmm1, %xmm0
	movups	%xmm0, (%r12,%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L112
	movl	-244(%rbp), %edi
	movl	%edi, %eax
	andl	$-16, %eax
	testb	$15, %dil
	je	.L114
	movslq	%eax, %rdx
	movzbl	(%r15,%rdx), %ecx
	xorb	%cl, (%r12,%rdx)
	leal	1(%rax), %edx
	cmpl	%edi, %edx
	jge	.L114
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %ecx
	xorb	%cl, (%r12,%rdx)
	leal	2(%rax), %edx
	cmpl	%edx, %edi
	jle	.L114
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %ecx
	xorb	%cl, (%r12,%rdx)
	leal	3(%rax), %edx
	cmpl	%edx, %edi
	jle	.L114
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %ecx
	xorb	%cl, (%r12,%rdx)
	leal	4(%rax), %edx
	cmpl	%edx, %edi
	jle	.L114
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %ecx
	xorb	%cl, (%r12,%rdx)
	leal	5(%rax), %edx
	cmpl	%edx, %edi
	jle	.L114
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %ecx
	xorb	%cl, (%r12,%rdx)
	leal	6(%rax), %edx
	cmpl	%edx, %edi
	jle	.L114
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %ecx
	xorb	%cl, (%r12,%rdx)
	leal	7(%rax), %edx
	cmpl	%edx, %edi
	jle	.L114
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %ecx
	xorb	%cl, (%r12,%rdx)
	leal	8(%rax), %edx
	cmpl	%edx, %edi
	jle	.L114
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %ecx
	xorb	%cl, (%r12,%rdx)
	leal	9(%rax), %edx
	cmpl	%edx, %edi
	jle	.L114
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %ecx
	xorb	%cl, (%r12,%rdx)
	leal	10(%rax), %edx
	cmpl	%edx, %edi
	jle	.L114
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %ecx
	xorb	%cl, (%r12,%rdx)
	leal	11(%rax), %edx
	cmpl	%edx, %edi
	jle	.L114
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %ecx
	xorb	%cl, (%r12,%rdx)
	leal	12(%rax), %edx
	cmpl	%edx, %edi
	jle	.L114
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %ecx
	xorb	%cl, (%r12,%rdx)
	leal	13(%rax), %edx
	cmpl	%edx, %edi
	jle	.L114
	movslq	%edx, %rdx
	addl	$14, %eax
	movzbl	(%r15,%rdx), %ecx
	xorb	%cl, (%r12,%rdx)
	cmpl	%eax, %edi
	jle	.L114
	cltq
	movzbl	(%r15,%rax), %edx
	xorb	%dl, (%r12,%rax)
	.p2align 4,,10
	.p2align 3
.L114:
	leaq	-128(%rbp), %r14
	movslq	16(%rbp), %rsi
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	-232(%rbp), %r8
	movq	-288(%rbp), %rdi
	movq	%r14, %rdx
	call	EVP_Digest@PLT
	testl	%eax, %eax
	jne	.L169
.L109:
	movl	-212(%rbp), %eax
	movl	$-1, %r14d
	notl	%eax
	movl	%eax, -232(%rbp)
.L95:
	movq	-224(%rbp), %rdi
	movl	$64, %esi
	call	OPENSSL_cleanse@PLT
	movq	-208(%rbp), %rsi
	movl	$267, %ecx
	movq	%r12, %rdi
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movq	-200(%rbp), %rsi
	movq	-256(%rbp), %rdi
	movl	$268, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movl	-212(%rbp), %esi
	movl	-232(%rbp), %eax
	andl	%r14d, %esi
	orl	%esi, %eax
.L88:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L170
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L169:
	.cfi_restore_state
	movq	-264(%rbp), %r15
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r15, %rdx
	call	CRYPTO_memcmp@PLT
	movl	-244(%rbp), %r9d
	movl	%eax, %esi
	subl	$1, %eax
	notl	%esi
	andl	%esi, %eax
	movl	-212(%rbp), %esi
	sarl	$31, %eax
	andl	%eax, %esi
	cmpl	%r9d, %r13d
	jge	.L118
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L119:
	movzbl	(%r12,%rdx), %r8d
	movl	%r8d, %edi
	xorl	$1, %edi
	movzbl	%dil, %edi
	movl	%edi, %eax
	subl	$1, %edi
	notl	%eax
	andl	%eax, %edi
	movl	%ecx, %eax
	sarl	$31, %edi
	notl	%eax
	andl	%edi, %eax
	orl	%edi, %ecx
	movl	%eax, %r10d
	notl	%eax
	andl	%edx, %r10d
	addq	$1, %rdx
	andl	%ebx, %eax
	movl	%r10d, %ebx
	orl	%eax, %ebx
	movl	%r8d, %eax
	subl	$1, %r8d
	notl	%eax
	andl	%eax, %r8d
	sarl	$31, %r8d
	orl	%ecx, %r8d
	andl	%r8d, %esi
	cmpl	%edx, %r9d
	jg	.L119
	leal	1(%rbx), %eax
	movl	-244(%rbp), %r11d
	movl	-240(%rbp), %ebx
	andl	%esi, %ecx
	subl	%eax, %r11d
	movl	%ebx, %eax
	movl	%ebx, %edx
	subl	%r11d, %eax
	xorl	%r11d, %edx
	movl	%r11d, %r14d
	xorl	%r11d, %eax
	orl	%edx, %eax
	xorl	%ebx, %eax
	sarl	$31, %eax
	notl	%eax
	andl	%ecx, %eax
	movl	%eax, %ebx
	movl	%eax, -212(%rbp)
	notl	%eax
	movl	%eax, -232(%rbp)
	andl	$1, %ebx
.L120:
	movl	-244(%rbp), %ecx
	movl	-240(%rbp), %edi
	movl	$1, %r10d
	movl	%ecx, %eax
	subl	%r13d, %eax
	leal	-1(%rax), %r15d
	movl	%edi, %eax
	movl	%r15d, %esi
	xorl	%r15d, %eax
	subl	%edi, %esi
	xorl	%edi, %esi
	orl	%eax, %esi
	xorl	%r15d, %esi
	sarl	$31, %esi
	movl	%esi, %eax
	notl	%esi
	andl	%r15d, %eax
	andl	%edi, %esi
	movl	%r15d, %edi
	orl	%esi, %eax
	subl	%r11d, %edi
	movl	%eax, -244(%rbp)
	cmpl	$1, %r15d
	jle	.L126
	movq	-264(%rbp), %rax
	movslq	-216(%rbp), %rdx
	movl	%ebx, -272(%rbp)
	movl	%ecx, %ebx
	movl	%r14d, -280(%rbp)
	leaq	2(%r12,%rax), %rax
	movl	%r11d, -288(%rbp)
	movq	%rdx, %r14
	movl	%edi, %r11d
	movq	%rax, -240(%rbp)
	movl	$-2, %eax
	subl	%r13d, %eax
	movl	%eax, %r13d
	.p2align 4,,10
	.p2align 3
.L125:
	movl	%r10d, %eax
	movl	%ebx, %edx
	andl	%r11d, %eax
	subl	%r10d, %edx
	movl	%eax, %esi
	subl	$1, %eax
	notl	%esi
	andl	%eax, %esi
	sarl	$31, %esi
	movl	%esi, %edi
	notl	%edi
	cmpl	%edx, -216(%rbp)
	jge	.L128
	leal	0(%r13,%rdx), %r8d
	leaq	(%r12,%r14), %rax
	movslq	%r10d, %r9
	movzbl	%dil, %edi
	addq	-240(%rbp), %r8
	orl	$-256, %esi
	.p2align 4,,10
	.p2align 3
.L127:
	movzbl	(%rax,%r9), %edx
	movzbl	(%rax), %ecx
	addq	$1, %rax
	andl	%edi, %edx
	andl	%esi, %ecx
	orl	%ecx, %edx
	movb	%dl, -1(%rax)
	cmpq	%r8, %rax
	jne	.L127
.L128:
	addl	%r10d, %r10d
	cmpl	%r10d, %r15d
	jg	.L125
	movl	-272(%rbp), %ebx
	movl	-280(%rbp), %r14d
	movl	-288(%rbp), %r11d
.L126:
	movl	-244(%rbp), %eax
	testl	%eax, %eax
	jle	.L123
	movl	-244(%rbp), %eax
	movl	%r11d, %esi
	movl	-212(%rbp), %r9d
	xorl	%edx, %edx
	movq	-296(%rbp), %rcx
	negl	%esi
	leal	-1(%rax), %r8d
	movq	-264(%rbp), %rax
	leaq	(%r12,%rax), %rdi
	.p2align 4,,10
	.p2align 3
.L129:
	movl	%r11d, %eax
	movl	%r11d, %r10d
	movzbl	1(%rdi,%rdx), %r13d
	xorl	%esi, %r10d
	xorl	%edx, %eax
	addl	$1, %esi
	orl	%r10d, %eax
	xorl	%edx, %eax
	sarl	$31, %eax
	andl	%r9d, %eax
	movzbl	%al, %eax
	movl	%eax, %r10d
	notl	%eax
	andl	%r13d, %r10d
	movzbl	(%rcx,%rdx), %r13d
	andl	%r13d, %eax
	orl	%r10d, %eax
	movb	%al, (%rcx,%rdx)
	movq	%rdx, %rax
	addq	$1, %rdx
	cmpq	%r8, %rax
	jne	.L129
.L123:
	movl	$262, %r8d
	movl	$121, %edx
	movl	$153, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	%ebx, %edi
	call	err_clear_last_constant_time@PLT
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L167:
	movl	$159, %r8d
.L165:
	movl	$65, %edx
	movl	$153, %esi
	movl	$4, %edi
	movl	$-1, %r14d
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	leaq	-192(%rbp), %rax
	movq	$0, -256(%rbp)
	movl	$-1, -232(%rbp)
	movl	$0, -212(%rbp)
	movq	%rax, -224(%rbp)
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L166:
	movl	%r8d, -200(%rbp)
	call	EVP_sha1@PLT
	movl	-200(%rbp), %r8d
	movq	%rax, -232(%rbp)
	jmp	.L89
.L102:
	movq	-224(%rbp), %rax
	leaq	-191(%rbp,%rdx), %rcx
	.p2align 4,,10
	.p2align 3
.L106:
	movzbl	(%r14), %edx
	xorb	%dl, (%rax)
	addq	$1, %rax
	addq	$1, %r14
	cmpq	%rcx, %rax
	jne	.L106
	jmp	.L104
.L168:
	movl	$165, %r8d
	jmp	.L165
.L111:
	movl	%eax, %edx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L116:
	movzbl	(%r15,%rax), %ecx
	xorb	%cl, (%r12,%rax)
	movq	%rax, %rcx
	addq	$1, %rax
	cmpq	%rdx, %rcx
	jne	.L116
	jmp	.L114
.L92:
	movl	$151, %r8d
	movl	$121, %edx
	movl	$153, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L88
.L118:
	movl	-248(%rbp), %r11d
	movl	$-1, -232(%rbp)
	movl	$0, -212(%rbp)
	subl	$2, %r11d
	movl	%r11d, %r14d
	jmp	.L120
.L131:
	movl	$-1, %eax
	jmp	.L88
.L170:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE484:
	.size	RSA_padding_check_PKCS1_OAEP_mgf1, .-RSA_padding_check_PKCS1_OAEP_mgf1
	.p2align 4
	.globl	RSA_padding_check_PKCS1_OAEP
	.type	RSA_padding_check_PKCS1_OAEP, @function
RSA_padding_check_PKCS1_OAEP:
.LFB483:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	movl	16(%rbp), %eax
	pushq	$0
	pushq	$0
	pushq	%rax
	call	RSA_padding_check_PKCS1_OAEP_mgf1
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE483:
	.size	RSA_padding_check_PKCS1_OAEP, .-RSA_padding_check_PKCS1_OAEP
	.p2align 4
	.globl	RSA_padding_add_PKCS1_OAEP
	.type	RSA_padding_add_PKCS1_OAEP, @function
RSA_padding_add_PKCS1_OAEP:
.LFB481:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leal	-1(%rsi), %ebx
	subq	$152, %rsp
	movq	%rdi, -144(%rbp)
	movq	%rdx, -176(%rbp)
	movq	%r8, -152(%rbp)
	movl	%r9d, -160(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_sha1@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	EVP_MD_size@PLT
	movl	%ebx, %r11d
	leal	(%rax,%rax), %r10d
	subl	%r10d, %r11d
	cmpl	%r11d, %r14d
	jge	.L217
	cmpl	%r10d, %ebx
	jle	.L218
	movl	%eax, %r12d
	movq	-144(%rbp), %rax
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movslq	%r12d, %rsi
	movq	-152(%rbp), %rdi
	movq	%r13, %r8
	movl	%r11d, -184(%rbp)
	movb	$0, (%rax)
	leaq	1(%rax,%rsi), %r15
	movq	%rsi, -168(%rbp)
	movslq	-160(%rbp), %rsi
	movq	%r15, %rdx
	movl	%r10d, -180(%rbp)
	call	EVP_Digest@PLT
	movl	-180(%rbp), %r10d
	movl	-184(%rbp), %r11d
	testl	%eax, %eax
	jne	.L177
.L179:
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	leaq	-128(%rbp), %rdi
	xorl	%r15d, %r15d
.L178:
	movl	$64, %esi
	call	OPENSSL_cleanse@PLT
	movl	$104, %ecx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
.L173:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L219
	addq	$152, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	movl	%ebx, %ecx
	movq	-144(%rbp), %rax
	xorl	%esi, %esi
	movl	%r11d, -180(%rbp)
	subl	%r14d, %ecx
	subl	%r10d, %ecx
	addq	$1, %rax
	movl	%ecx, -160(%rbp)
	leal	-1(%rcx), %edx
	movq	-168(%rbp), %rcx
	movslq	%edx, %rdx
	movq	%rax, -152(%rbp)
	leaq	(%r15,%rcx), %rdi
	call	memset@PLT
	movslq	%r14d, %rax
	movslq	%ebx, %rdi
	movl	%r14d, %edx
	movl	-160(%rbp), %ecx
	subq	%rax, %rdi
	movq	-176(%rbp), %rsi
	subq	-168(%rbp), %rdi
	addl	%r12d, %ecx
	addq	%r15, %rdi
	movslq	%ecx, %rcx
	movb	$1, -1(%r15,%rcx)
	call	memcpy@PLT
	movq	-152(%rbp), %rdi
	movl	%r12d, %esi
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jle	.L179
	movl	-180(%rbp), %r11d
	movl	$85, %edx
	leaq	.LC0(%rip), %rsi
	addl	%r12d, %r11d
	movslq	%r11d, %r14
	movl	%r11d, -160(%rbp)
	movq	%r14, %rdi
	call	CRYPTO_malloc@PLT
	movl	-160(%rbp), %r11d
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L220
	movq	-168(%rbp), %rcx
	movq	%r13, %r8
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	-152(%rbp), %rdx
	movl	%r11d, -160(%rbp)
	call	PKCS1_MGF1
	testl	%eax, %eax
	js	.L194
	movl	-160(%rbp), %r11d
	testl	%r11d, %r11d
	jle	.L186
	movq	-144(%rbp), %rdi
	movq	-168(%rbp), %rsi
	leal	-1(%r11), %eax
	leaq	17(%rdi,%rsi), %rdx
	cmpq	%rdx, %rbx
	leaq	16(%rbx), %rdx
	setnb	%cl
	cmpq	%rdx, %r15
	setnb	%dl
	orb	%dl, %cl
	je	.L183
	cmpl	$14, %eax
	jbe	.L183
	movl	%r11d, %edx
	xorl	%eax, %eax
	shrl	$4, %edx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L184:
	movdqu	(%r15,%rax), %xmm0
	movdqu	(%rbx,%rax), %xmm1
	pxor	%xmm1, %xmm0
	movups	%xmm0, (%r15,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L184
	movl	%r11d, %eax
	andl	$-16, %eax
	testb	$15, %r11b
	je	.L186
	movl	%eax, %edx
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r15,%rdx)
	leal	1(%rax), %edx
	cmpl	%edx, %r11d
	jle	.L186
	movslq	%edx, %rdx
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r15,%rdx)
	leal	2(%rax), %edx
	cmpl	%edx, %r11d
	jle	.L186
	movslq	%edx, %rdx
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r15,%rdx)
	leal	3(%rax), %edx
	cmpl	%edx, %r11d
	jle	.L186
	movslq	%edx, %rdx
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r15,%rdx)
	leal	4(%rax), %edx
	cmpl	%edx, %r11d
	jle	.L186
	movslq	%edx, %rdx
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r15,%rdx)
	leal	5(%rax), %edx
	cmpl	%edx, %r11d
	jle	.L186
	movslq	%edx, %rdx
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r15,%rdx)
	leal	6(%rax), %edx
	cmpl	%edx, %r11d
	jle	.L186
	movslq	%edx, %rdx
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r15,%rdx)
	leal	7(%rax), %edx
	cmpl	%edx, %r11d
	jle	.L186
	movslq	%edx, %rdx
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r15,%rdx)
	leal	8(%rax), %edx
	cmpl	%edx, %r11d
	jle	.L186
	movslq	%edx, %rdx
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r15,%rdx)
	leal	9(%rax), %edx
	cmpl	%edx, %r11d
	jle	.L186
	movslq	%edx, %rdx
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r15,%rdx)
	leal	10(%rax), %edx
	cmpl	%edx, %r11d
	jle	.L186
	movslq	%edx, %rdx
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r15,%rdx)
	leal	11(%rax), %edx
	cmpl	%edx, %r11d
	jle	.L186
	movslq	%edx, %rdx
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r15,%rdx)
	leal	12(%rax), %edx
	cmpl	%edx, %r11d
	jle	.L186
	movslq	%edx, %rdx
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r15,%rdx)
	leal	13(%rax), %edx
	cmpl	%edx, %r11d
	jle	.L186
	movslq	%edx, %rdx
	addl	$14, %eax
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r15,%rdx)
	cmpl	%eax, %r11d
	jle	.L186
	cltq
	movzbl	(%rbx,%rax), %edx
	xorb	%dl, (%r15,%rax)
	.p2align 4,,10
	.p2align 3
.L186:
	movq	-168(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r13, %r8
	movq	%r14, %rcx
	leaq	-128(%rbp), %rdi
	xorl	%r15d, %r15d
	movq	%rdi, -160(%rbp)
	call	PKCS1_MGF1
	movq	-160(%rbp), %rdi
	testl	%eax, %eax
	js	.L178
	testl	%r12d, %r12d
	jle	.L196
	leal	-1(%r12), %eax
	cmpl	$14, %eax
	jbe	.L197
	movq	-144(%rbp), %rcx
	movdqa	-128(%rbp), %xmm0
	movl	%r12d, %eax
	shrl	$4, %eax
	movdqu	1(%rcx), %xmm2
	pxor	%xmm2, %xmm0
	movaps	%xmm2, -144(%rbp)
	movups	%xmm0, 1(%rcx)
	cmpl	$1, %eax
	je	.L191
	movdqu	17(%rcx), %xmm3
	movdqa	-112(%rbp), %xmm0
	pxor	%xmm3, %xmm0
	movaps	%xmm3, -144(%rbp)
	movups	%xmm0, 17(%rcx)
	cmpl	$2, %eax
	je	.L191
	movdqu	33(%rcx), %xmm4
	movdqa	-96(%rbp), %xmm0
	pxor	%xmm4, %xmm0
	movaps	%xmm4, -144(%rbp)
	movups	%xmm0, 33(%rcx)
	cmpl	$3, %eax
	je	.L191
	movdqu	49(%rcx), %xmm5
	movdqa	-80(%rbp), %xmm0
	pxor	%xmm5, %xmm0
	movaps	%xmm5, -144(%rbp)
	movups	%xmm0, 49(%rcx)
.L191:
	movl	%r12d, %eax
	andl	$-16, %eax
	testb	$15, %r12b
	je	.L196
.L190:
	movq	-152(%rbp), %rsi
	movslq	%eax, %rdx
	movzbl	-128(%rbp,%rdx), %ecx
	xorb	%cl, (%rsi,%rdx)
	leal	1(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L196
	movslq	%edx, %rdx
	movzbl	-128(%rbp,%rdx), %ecx
	xorb	%cl, (%rsi,%rdx)
	leal	2(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L196
	movslq	%edx, %rdx
	movzbl	-128(%rbp,%rdx), %ecx
	xorb	%cl, (%rsi,%rdx)
	leal	3(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L196
	movslq	%edx, %rdx
	movzbl	-128(%rbp,%rdx), %ecx
	xorb	%cl, (%rsi,%rdx)
	leal	4(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L196
	movslq	%edx, %rdx
	movzbl	-128(%rbp,%rdx), %ecx
	xorb	%cl, (%rsi,%rdx)
	leal	5(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L196
	movslq	%edx, %rdx
	movzbl	-128(%rbp,%rdx), %ecx
	xorb	%cl, (%rsi,%rdx)
	leal	6(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L196
	movslq	%edx, %rdx
	movzbl	-128(%rbp,%rdx), %ecx
	xorb	%cl, (%rsi,%rdx)
	leal	7(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L196
	movslq	%edx, %rdx
	movzbl	-128(%rbp,%rdx), %ecx
	xorb	%cl, (%rsi,%rdx)
	leal	8(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L196
	movslq	%edx, %rdx
	movzbl	-128(%rbp,%rdx), %ecx
	xorb	%cl, (%rsi,%rdx)
	leal	9(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L196
	movslq	%edx, %rdx
	movzbl	-128(%rbp,%rdx), %ecx
	xorb	%cl, (%rsi,%rdx)
	leal	10(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L196
	movslq	%edx, %rdx
	movzbl	-128(%rbp,%rdx), %ecx
	xorb	%cl, (%rsi,%rdx)
	leal	11(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L196
	movslq	%edx, %rdx
	movzbl	-128(%rbp,%rdx), %ecx
	xorb	%cl, (%rsi,%rdx)
	leal	12(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L196
	movslq	%edx, %rdx
	movzbl	-128(%rbp,%rdx), %ecx
	xorb	%cl, (%rsi,%rdx)
	leal	13(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L196
	movslq	%edx, %rdx
	addl	$14, %eax
	movzbl	-128(%rbp,%rdx), %ecx
	xorb	%cl, (%rsi,%rdx)
	cmpl	%eax, %r12d
	jle	.L196
	cltq
	movzbl	-128(%rbp,%rax), %edx
	xorb	%dl, (%rsi,%rax)
	.p2align 4,,10
	.p2align 3
.L196:
	movl	$1, %r15d
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L218:
	movl	$67, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$120, %edx
	xorl	%r15d, %r15d
	movl	$154, %esi
	movl	$4, %edi
	call	ERR_put_error@PLT
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L217:
	movl	$61, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$110, %edx
	xorl	%r15d, %r15d
	movl	$154, %esi
	movl	$4, %edi
	call	ERR_put_error@PLT
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L194:
	xorl	%r15d, %r15d
	leaq	-128(%rbp), %rdi
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L220:
	movl	$4, %edi
	movl	$87, %r8d
	movl	$65, %edx
	xorl	%r15d, %r15d
	leaq	.LC0(%rip), %rcx
	movl	$154, %esi
	call	ERR_put_error@PLT
	leaq	-128(%rbp), %rdi
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L183:
	movl	%eax, %edx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L188:
	movzbl	(%rbx,%rax), %ecx
	xorb	%cl, (%r15,%rax)
	movq	%rax, %rcx
	addq	$1, %rax
	cmpq	%rdx, %rcx
	jne	.L188
	jmp	.L186
.L197:
	xorl	%eax, %eax
	jmp	.L190
.L219:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE481:
	.size	RSA_padding_add_PKCS1_OAEP, .-RSA_padding_add_PKCS1_OAEP
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
