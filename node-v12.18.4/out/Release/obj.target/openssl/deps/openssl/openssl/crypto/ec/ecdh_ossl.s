	.file	"ecdh_ossl.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ec/ecdh_ossl.c"
	.text
	.p2align 4
	.globl	ossl_ecdh_compute_key
	.type	ossl_ecdh_compute_key, @function
ossl_ecdh_compute_key:
.LFB425:
	.cfi_startproc
	endbr64
	movq	24(%rcx), %rax
	movq	(%rax), %rax
	movq	384(%rax), %rax
	testq	%rax, %rax
	je	.L5
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L5:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$160, %edx
	movl	$247, %esi
	movl	$16, %edi
	movl	$26, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE425:
	.size	ossl_ecdh_compute_key, .-ossl_ecdh_compute_key
	.p2align 4
	.globl	ecdh_simple_compute_key
	.type	ecdh_simple_compute_key, @function
ecdh_simple_compute_key:
.LFB426:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -64(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rdx, -56(%rbp)
	call	BN_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L22
	movq	%rax, %rdi
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L32
	movq	%r14, %rdi
	call	EC_KEY_get0_private_key@PLT
	testq	%rax, %rax
	je	.L33
	movq	%r14, %rdi
	movq	%rax, -80(%rbp)
	call	EC_KEY_get0_group@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	EC_KEY_get_flags@PLT
	movq	-80(%rbp), %r8
	testb	$16, %ah
	je	.L13
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	EC_GROUP_get_cofactor@PLT
	movq	-80(%rbp), %r8
	testl	%eax, %eax
	je	.L15
	movq	%r12, %rcx
	movq	%r8, %rdx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	BN_mul@PLT
	testl	%eax, %eax
	je	.L15
	movq	%r13, %r8
.L13:
	movq	%rbx, %rdi
	movq	%r8, -80(%rbp)
	call	EC_POINT_new@PLT
	movq	-80(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L34
	movq	-56(%rbp), %rcx
	movq	%r12, %r9
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	EC_POINT_mul@PLT
	movl	$82, %r8d
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L31
	movq	%r12, %r8
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	EC_POINT_get_affine_coordinates@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L35
	movq	%rbx, %rdi
	call	EC_GROUP_get_degree@PLT
	movq	%r13, %rdi
	leal	14(%rax), %ebx
	addl	$7, %eax
	cmovs	%ebx, %eax
	sarl	$3, %eax
	movslq	%eax, %rbx
	call	BN_num_bits@PLT
	leal	14(%rax), %r14d
	addl	$7, %eax
	cmovns	%eax, %r14d
	sarl	$3, %r14d
	movslq	%r14d, %rcx
	movl	%r14d, -56(%rbp)
	cmpq	%rcx, %rbx
	jb	.L36
	movl	$97, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rbx, %rdi
	movq	%rcx, -80(%rbp)
	call	CRYPTO_malloc@PLT
	movq	-80(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L37
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	subq	%rcx, %rdx
	movq	%rdx, -80(%rbp)
	call	memset@PLT
	movq	-80(%rbp), %rdx
	movq	%r13, %rdi
	leaq	(%r14,%rdx), %rsi
	call	BN_bn2bin@PLT
	cmpl	%eax, -56(%rbp)
	jne	.L38
	movq	-64(%rbp), %rax
	xorl	%r13d, %r13d
	movq	%r14, (%rax)
	movq	-72(%rbp), %rax
	movl	$1, %r14d
	movq	%rbx, (%rax)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L22:
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
.L10:
	movq	%r15, %rdi
	call	EC_POINT_clear_free@PLT
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	movq	%r12, %rdi
	call	BN_CTX_free@PLT
	movq	%r13, %rdi
	movl	$118, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$40, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	movl	$125, %edx
	movl	$257, %esi
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	movl	$61, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$16, %edi
	xorl	%r14d, %r14d
	call	ERR_put_error@PLT
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L35:
	movl	$87, %r8d
.L31:
	leaq	.LC0(%rip), %rcx
	movl	$155, %edx
	movl	$257, %esi
	xorl	%r13d, %r13d
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L15:
	movl	$65, %edx
	movl	$257, %esi
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	movl	$70, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$16, %edi
	xorl	%r15d, %r15d
	call	ERR_put_error@PLT
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L32:
	movl	$65, %edx
	movl	$257, %esi
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	movl	$55, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L34:
	movl	$77, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
.L30:
	movl	$257, %esi
	movl	$16, %edi
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	call	ERR_put_error@PLT
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L38:
	movl	$104, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$3, %edx
	movq	%r14, %r13
	movl	$257, %esi
	movl	$16, %edi
	xorl	%r14d, %r14d
	call	ERR_put_error@PLT
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$94, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$68, %edx
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L37:
	movl	$98, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$257, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L10
	.cfi_endproc
.LFE426:
	.size	ecdh_simple_compute_key, .-ecdh_simple_compute_key
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
