	.file	"x509_meth.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/x509/x509_meth.c"
	.text
	.p2align 4
	.globl	X509_LOOKUP_meth_new
	.type	X509_LOOKUP_meth_new, @function
X509_LOOKUP_meth_new:
.LFB853:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$22, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$80, %edi
	pushq	%r12
	.cfi_offset 12, -32
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1
	movl	$25, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_strdup@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L8
.L1:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movl	$27, %r8d
	movl	$65, %edx
	movl	$160, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movl	$35, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L1
	.cfi_endproc
.LFE853:
	.size	X509_LOOKUP_meth_new, .-X509_LOOKUP_meth_new
	.p2align 4
	.globl	X509_LOOKUP_meth_free
	.type	X509_LOOKUP_meth_free, @function
X509_LOOKUP_meth_free:
.LFB854:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	testq	%rdi, %rdi
	je	.L10
	movq	(%rdi), %rdi
	movl	$42, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
.L10:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$43, %edx
	popq	%r12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE854:
	.size	X509_LOOKUP_meth_free, .-X509_LOOKUP_meth_free
	.p2align 4
	.globl	X509_LOOKUP_meth_set_new_item
	.type	X509_LOOKUP_meth_set_new_item, @function
X509_LOOKUP_meth_set_new_item:
.LFB855:
	.cfi_startproc
	endbr64
	movq	%rsi, 8(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE855:
	.size	X509_LOOKUP_meth_set_new_item, .-X509_LOOKUP_meth_set_new_item
	.p2align 4
	.globl	X509_LOOKUP_meth_get_new_item
	.type	X509_LOOKUP_meth_get_new_item, @function
X509_LOOKUP_meth_get_new_item:
.LFB856:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE856:
	.size	X509_LOOKUP_meth_get_new_item, .-X509_LOOKUP_meth_get_new_item
	.p2align 4
	.globl	X509_LOOKUP_meth_set_free
	.type	X509_LOOKUP_meth_set_free, @function
X509_LOOKUP_meth_set_free:
.LFB857:
	.cfi_startproc
	endbr64
	movq	%rsi, 16(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE857:
	.size	X509_LOOKUP_meth_set_free, .-X509_LOOKUP_meth_set_free
	.p2align 4
	.globl	X509_LOOKUP_meth_get_free
	.type	X509_LOOKUP_meth_get_free, @function
X509_LOOKUP_meth_get_free:
.LFB858:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE858:
	.size	X509_LOOKUP_meth_get_free, .-X509_LOOKUP_meth_get_free
	.p2align 4
	.globl	X509_LOOKUP_meth_set_init
	.type	X509_LOOKUP_meth_set_init, @function
X509_LOOKUP_meth_set_init:
.LFB859:
	.cfi_startproc
	endbr64
	movq	%rsi, 24(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE859:
	.size	X509_LOOKUP_meth_set_init, .-X509_LOOKUP_meth_set_init
	.p2align 4
	.globl	X509_LOOKUP_meth_get_init
	.type	X509_LOOKUP_meth_get_init, @function
X509_LOOKUP_meth_get_init:
.LFB860:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE860:
	.size	X509_LOOKUP_meth_get_init, .-X509_LOOKUP_meth_get_init
	.p2align 4
	.globl	X509_LOOKUP_meth_set_shutdown
	.type	X509_LOOKUP_meth_set_shutdown, @function
X509_LOOKUP_meth_set_shutdown:
.LFB861:
	.cfi_startproc
	endbr64
	movq	%rsi, 32(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE861:
	.size	X509_LOOKUP_meth_set_shutdown, .-X509_LOOKUP_meth_set_shutdown
	.p2align 4
	.globl	X509_LOOKUP_meth_get_shutdown
	.type	X509_LOOKUP_meth_get_shutdown, @function
X509_LOOKUP_meth_get_shutdown:
.LFB862:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE862:
	.size	X509_LOOKUP_meth_get_shutdown, .-X509_LOOKUP_meth_get_shutdown
	.p2align 4
	.globl	X509_LOOKUP_meth_set_ctrl
	.type	X509_LOOKUP_meth_set_ctrl, @function
X509_LOOKUP_meth_set_ctrl:
.LFB863:
	.cfi_startproc
	endbr64
	movq	%rsi, 40(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE863:
	.size	X509_LOOKUP_meth_set_ctrl, .-X509_LOOKUP_meth_set_ctrl
	.p2align 4
	.globl	X509_LOOKUP_meth_get_ctrl
	.type	X509_LOOKUP_meth_get_ctrl, @function
X509_LOOKUP_meth_get_ctrl:
.LFB864:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE864:
	.size	X509_LOOKUP_meth_get_ctrl, .-X509_LOOKUP_meth_get_ctrl
	.p2align 4
	.globl	X509_LOOKUP_meth_set_get_by_subject
	.type	X509_LOOKUP_meth_set_get_by_subject, @function
X509_LOOKUP_meth_set_get_by_subject:
.LFB865:
	.cfi_startproc
	endbr64
	movq	%rsi, 48(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE865:
	.size	X509_LOOKUP_meth_set_get_by_subject, .-X509_LOOKUP_meth_set_get_by_subject
	.p2align 4
	.globl	X509_LOOKUP_meth_get_get_by_subject
	.type	X509_LOOKUP_meth_get_get_by_subject, @function
X509_LOOKUP_meth_get_get_by_subject:
.LFB866:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	ret
	.cfi_endproc
.LFE866:
	.size	X509_LOOKUP_meth_get_get_by_subject, .-X509_LOOKUP_meth_get_get_by_subject
	.p2align 4
	.globl	X509_LOOKUP_meth_set_get_by_issuer_serial
	.type	X509_LOOKUP_meth_set_get_by_issuer_serial, @function
X509_LOOKUP_meth_set_get_by_issuer_serial:
.LFB867:
	.cfi_startproc
	endbr64
	movq	%rsi, 56(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE867:
	.size	X509_LOOKUP_meth_set_get_by_issuer_serial, .-X509_LOOKUP_meth_set_get_by_issuer_serial
	.p2align 4
	.globl	X509_LOOKUP_meth_get_get_by_issuer_serial
	.type	X509_LOOKUP_meth_get_get_by_issuer_serial, @function
X509_LOOKUP_meth_get_get_by_issuer_serial:
.LFB868:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rax
	ret
	.cfi_endproc
.LFE868:
	.size	X509_LOOKUP_meth_get_get_by_issuer_serial, .-X509_LOOKUP_meth_get_get_by_issuer_serial
	.p2align 4
	.globl	X509_LOOKUP_meth_set_get_by_fingerprint
	.type	X509_LOOKUP_meth_set_get_by_fingerprint, @function
X509_LOOKUP_meth_set_get_by_fingerprint:
.LFB869:
	.cfi_startproc
	endbr64
	movq	%rsi, 64(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE869:
	.size	X509_LOOKUP_meth_set_get_by_fingerprint, .-X509_LOOKUP_meth_set_get_by_fingerprint
	.p2align 4
	.globl	X509_LOOKUP_meth_get_get_by_fingerprint
	.type	X509_LOOKUP_meth_get_get_by_fingerprint, @function
X509_LOOKUP_meth_get_get_by_fingerprint:
.LFB870:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	ret
	.cfi_endproc
.LFE870:
	.size	X509_LOOKUP_meth_get_get_by_fingerprint, .-X509_LOOKUP_meth_get_get_by_fingerprint
	.p2align 4
	.globl	X509_LOOKUP_meth_set_get_by_alias
	.type	X509_LOOKUP_meth_set_get_by_alias, @function
X509_LOOKUP_meth_set_get_by_alias:
.LFB871:
	.cfi_startproc
	endbr64
	movq	%rsi, 72(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE871:
	.size	X509_LOOKUP_meth_set_get_by_alias, .-X509_LOOKUP_meth_set_get_by_alias
	.p2align 4
	.globl	X509_LOOKUP_meth_get_get_by_alias
	.type	X509_LOOKUP_meth_get_get_by_alias, @function
X509_LOOKUP_meth_get_get_by_alias:
.LFB872:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rax
	ret
	.cfi_endproc
.LFE872:
	.size	X509_LOOKUP_meth_get_get_by_alias, .-X509_LOOKUP_meth_get_get_by_alias
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
