	.file	"ssl_stat.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unknown state"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"SSLv3/TLS read certificate status"
	.section	.rodata.str1.1
.LC2:
	.string	"error"
.LC3:
	.string	"SSLv3/TLS read next proto"
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"SSLv3/TLS write certificate status"
	.section	.rodata.str1.1
.LC5:
	.string	"before SSL initialization"
	.section	.rodata.str1.8
	.align 8
.LC6:
	.string	"SSL negotiation finished successfully"
	.section	.rodata.str1.1
.LC7:
	.string	"SSLv3/TLS write client hello"
.LC8:
	.string	"SSLv3/TLS read server hello"
	.section	.rodata.str1.8
	.align 8
.LC9:
	.string	"SSLv3/TLS read server certificate"
	.align 8
.LC10:
	.string	"SSLv3/TLS read server key exchange"
	.align 8
.LC11:
	.string	"SSLv3/TLS read server certificate request"
	.align 8
.LC12:
	.string	"SSLv3/TLS read server session ticket"
	.section	.rodata.str1.1
.LC13:
	.string	"SSLv3/TLS read server done"
	.section	.rodata.str1.8
	.align 8
.LC14:
	.string	"SSLv3/TLS write client certificate"
	.align 8
.LC15:
	.string	"SSLv3/TLS write client key exchange"
	.align 8
.LC16:
	.string	"SSLv3/TLS write certificate verify"
	.align 8
.LC17:
	.string	"SSLv3/TLS write change cipher spec"
	.section	.rodata.str1.1
.LC18:
	.string	"SSLv3/TLS write finished"
	.section	.rodata.str1.8
	.align 8
.LC19:
	.string	"SSLv3/TLS read change cipher spec"
	.section	.rodata.str1.1
.LC20:
	.string	"SSLv3/TLS read finished"
.LC21:
	.string	"SSLv3/TLS read client hello"
.LC22:
	.string	"SSLv3/TLS write hello request"
.LC23:
	.string	"SSLv3/TLS write server hello"
.LC24:
	.string	"SSLv3/TLS write certificate"
.LC25:
	.string	"SSLv3/TLS write key exchange"
	.section	.rodata.str1.8
	.align 8
.LC26:
	.string	"SSLv3/TLS write certificate request"
	.align 8
.LC27:
	.string	"SSLv3/TLS write session ticket"
	.section	.rodata.str1.1
.LC28:
	.string	"SSLv3/TLS write server done"
	.section	.rodata.str1.8
	.align 8
.LC29:
	.string	"SSLv3/TLS read client certificate"
	.align 8
.LC30:
	.string	"SSLv3/TLS read client key exchange"
	.align 8
.LC31:
	.string	"SSLv3/TLS read certificate verify"
	.align 8
.LC32:
	.string	"DTLS1 read hello verify request"
	.align 8
.LC33:
	.string	"DTLS1 write hello verify request"
	.align 8
.LC34:
	.string	"TLSv1.3 write encrypted extensions"
	.align 8
.LC35:
	.string	"TLSv1.3 read encrypted extensions"
	.align 8
.LC36:
	.string	"TLSv1.3 read server certificate verify"
	.align 8
.LC37:
	.string	"TLSv1.3 write server certificate verify"
	.section	.rodata.str1.1
.LC38:
	.string	"SSLv3/TLS read hello request"
	.section	.rodata.str1.8
	.align 8
.LC39:
	.string	"TLSv1.3 write server key update"
	.align 8
.LC40:
	.string	"TLSv1.3 write client key update"
	.align 8
.LC41:
	.string	"TLSv1.3 read client key update"
	.align 8
.LC42:
	.string	"TLSv1.3 read server key update"
	.section	.rodata.str1.1
.LC43:
	.string	"TLSv1.3 early data"
	.section	.rodata.str1.8
	.align 8
.LC44:
	.string	"TLSv1.3 pending early data end"
	.align 8
.LC45:
	.string	"TLSv1.3 write end of early data"
	.align 8
.LC46:
	.string	"TLSv1.3 read end of early data"
	.section	.rodata.str1.1
.LC47:
	.string	"SSLv3/TLS write next proto"
	.text
	.p2align 4
	.globl	SSL_state_string_long
	.type	SSL_state_string_long, @function
SSL_state_string_long:
.LFB964:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	ossl_statem_in_error@PLT
	movl	%eax, %r8d
	leaq	.LC2(%rip), %rax
	testl	%r8d, %r8d
	je	.L54
.L1:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	movq	%r12, %rdi
	call	SSL_get_state@PLT
	cmpl	$49, %eax
	ja	.L3
	leaq	.L5(%rip), %rdx
	movl	%eax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L5:
	.long	.L49-.L5
	.long	.L48-.L5
	.long	.L47-.L5
	.long	.L46-.L5
	.long	.L45-.L5
	.long	.L51-.L5
	.long	.L44-.L5
	.long	.L43-.L5
	.long	.L42-.L5
	.long	.L41-.L5
	.long	.L23-.L5
	.long	.L22-.L5
	.long	.L40-.L5
	.long	.L39-.L5
	.long	.L38-.L5
	.long	.L37-.L5
	.long	.L19-.L5
	.long	.L36-.L5
	.long	.L18-.L5
	.long	.L35-.L5
	.long	.L34-.L5
	.long	.L33-.L5
	.long	.L32-.L5
	.long	.L31-.L5
	.long	.L30-.L5
	.long	.L29-.L5
	.long	.L28-.L5
	.long	.L27-.L5
	.long	.L26-.L5
	.long	.L25-.L5
	.long	.L24-.L5
	.long	.L23-.L5
	.long	.L22-.L5
	.long	.L21-.L5
	.long	.L20-.L5
	.long	.L19-.L5
	.long	.L18-.L5
	.long	.L17-.L5
	.long	.L16-.L5
	.long	.L15-.L5
	.long	.L14-.L5
	.long	.L13-.L5
	.long	.L12-.L5
	.long	.L11-.L5
	.long	.L10-.L5
	.long	.L9-.L5
	.long	.L8-.L5
	.long	.L7-.L5
	.long	.L6-.L5
	.long	.L4-.L5
	.text
	.p2align 4,,10
	.p2align 3
.L18:
	leaq	.LC18(%rip), %rax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L23:
	leaq	.LC19(%rip), %rax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L22:
	leaq	.LC20(%rip), %rax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L19:
	leaq	.LC17(%rip), %rax
	jmp	.L1
.L41:
	leaq	.LC12(%rip), %rax
	jmp	.L1
.L32:
	leaq	.LC23(%rip), %rax
	jmp	.L1
.L31:
	leaq	.LC24(%rip), %rax
	jmp	.L1
.L30:
	leaq	.LC25(%rip), %rax
	jmp	.L1
.L29:
	leaq	.LC26(%rip), %rax
	jmp	.L1
.L28:
	leaq	.LC28(%rip), %rax
	jmp	.L1
.L27:
	leaq	.LC29(%rip), %rax
	jmp	.L1
.L26:
	leaq	.LC30(%rip), %rax
	jmp	.L1
.L25:
	leaq	.LC31(%rip), %rax
	jmp	.L1
.L35:
	leaq	.LC22(%rip), %rax
	jmp	.L1
.L34:
	leaq	.LC21(%rip), %rax
	jmp	.L1
.L33:
	leaq	.LC33(%rip), %rax
	jmp	.L1
.L39:
	leaq	.LC14(%rip), %rax
	jmp	.L1
.L38:
	leaq	.LC15(%rip), %rax
	jmp	.L1
.L37:
	leaq	.LC16(%rip), %rax
	jmp	.L1
.L42:
	leaq	.LC13(%rip), %rax
	jmp	.L1
.L49:
	leaq	.LC5(%rip), %rax
	jmp	.L1
.L48:
	leaq	.LC6(%rip), %rax
	jmp	.L1
.L47:
	leaq	.LC32(%rip), %rax
	jmp	.L1
.L46:
	leaq	.LC8(%rip), %rax
	jmp	.L1
.L45:
	leaq	.LC9(%rip), %rax
	jmp	.L1
.L44:
	leaq	.LC10(%rip), %rax
	jmp	.L1
.L43:
	leaq	.LC11(%rip), %rax
	jmp	.L1
.L40:
	leaq	.LC7(%rip), %rax
	jmp	.L1
.L24:
	leaq	.LC3(%rip), %rax
	jmp	.L1
.L21:
	leaq	.LC27(%rip), %rax
	jmp	.L1
.L20:
	leaq	.LC4(%rip), %rax
	jmp	.L1
.L17:
	leaq	.LC34(%rip), %rax
	jmp	.L1
.L16:
	leaq	.LC35(%rip), %rax
	jmp	.L1
.L15:
	leaq	.LC36(%rip), %rax
	jmp	.L1
.L14:
	leaq	.LC37(%rip), %rax
	jmp	.L1
.L13:
	leaq	.LC38(%rip), %rax
	jmp	.L1
.L12:
	leaq	.LC39(%rip), %rax
	jmp	.L1
.L11:
	leaq	.LC40(%rip), %rax
	jmp	.L1
.L10:
	leaq	.LC41(%rip), %rax
	jmp	.L1
.L9:
	leaq	.LC42(%rip), %rax
	jmp	.L1
.L8:
	leaq	.LC43(%rip), %rax
	jmp	.L1
.L7:
	leaq	.LC44(%rip), %rax
	jmp	.L1
.L6:
	leaq	.LC45(%rip), %rax
	jmp	.L1
.L4:
	leaq	.LC46(%rip), %rax
	jmp	.L1
.L36:
	leaq	.LC47(%rip), %rax
	jmp	.L1
.L51:
	leaq	.LC1(%rip), %rax
	jmp	.L1
.L3:
	leaq	.LC0(%rip), %rax
	jmp	.L1
	.cfi_endproc
.LFE964:
	.size	SSL_state_string_long, .-SSL_state_string_long
	.section	.rodata.str1.1
.LC48:
	.string	"UNKWN "
.LC49:
	.string	"TWEOED"
.LC50:
	.string	"SSLERR"
.LC51:
	.string	"TWCS"
.LC52:
	.string	"TRCS"
.LC53:
	.string	"TRST"
.LC54:
	.string	"TWNP"
.LC55:
	.string	"PINIT "
.LC56:
	.string	"SSLOK "
.LC57:
	.string	"TWCH"
.LC58:
	.string	"TRSH"
.LC59:
	.string	"TRSC"
.LC60:
	.string	"TRSKE"
.LC61:
	.string	"TRCR"
.LC62:
	.string	"TRSD"
.LC63:
	.string	"TWCC"
.LC64:
	.string	"TWCKE"
.LC65:
	.string	"TWCV"
.LC66:
	.string	"TWCCS"
.LC67:
	.string	"TWFIN"
.LC68:
	.string	"TRCCS"
.LC69:
	.string	"TRFIN"
.LC70:
	.string	"TWHR"
.LC71:
	.string	"TRCH"
.LC72:
	.string	"TWSH"
.LC73:
	.string	"TWSC"
.LC74:
	.string	"TWSKE"
.LC75:
	.string	"TWCR"
.LC76:
	.string	"TWSD"
.LC77:
	.string	"TRCC"
.LC78:
	.string	"TRCKE"
.LC79:
	.string	"TRCV"
.LC80:
	.string	"DRCHV"
.LC81:
	.string	"DWCHV"
.LC82:
	.string	"TWEE"
.LC83:
	.string	"TREE"
.LC84:
	.string	"TRSCV"
.LC85:
	.string	"TRNP"
.LC86:
	.string	"TRHR"
.LC87:
	.string	"TWSKU"
.LC88:
	.string	"TWCKU"
.LC89:
	.string	"TRCKU"
.LC90:
	.string	"TRSKU"
.LC91:
	.string	"TED"
.LC92:
	.string	"TPEDE"
.LC93:
	.string	"TWST"
	.text
	.p2align 4
	.globl	SSL_state_string
	.type	SSL_state_string, @function
SSL_state_string:
.LFB965:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	ossl_statem_in_error@PLT
	movl	%eax, %r8d
	leaq	.LC50(%rip), %rax
	testl	%r8d, %r8d
	je	.L105
.L55:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore_state
	movq	%r12, %rdi
	call	SSL_get_state@PLT
	cmpl	$49, %eax
	ja	.L57
	leaq	.L59(%rip), %rdx
	movl	%eax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L59:
	.long	.L101-.L59
	.long	.L100-.L59
	.long	.L99-.L59
	.long	.L98-.L59
	.long	.L97-.L59
	.long	.L96-.L59
	.long	.L95-.L59
	.long	.L94-.L59
	.long	.L93-.L59
	.long	.L92-.L59
	.long	.L75-.L59
	.long	.L74-.L59
	.long	.L91-.L59
	.long	.L90-.L59
	.long	.L89-.L59
	.long	.L88-.L59
	.long	.L71-.L59
	.long	.L87-.L59
	.long	.L70-.L59
	.long	.L86-.L59
	.long	.L85-.L59
	.long	.L84-.L59
	.long	.L83-.L59
	.long	.L82-.L59
	.long	.L81-.L59
	.long	.L80-.L59
	.long	.L79-.L59
	.long	.L78-.L59
	.long	.L77-.L59
	.long	.L76-.L59
	.long	.L103-.L59
	.long	.L75-.L59
	.long	.L74-.L59
	.long	.L73-.L59
	.long	.L72-.L59
	.long	.L71-.L59
	.long	.L70-.L59
	.long	.L69-.L59
	.long	.L68-.L59
	.long	.L67-.L59
	.long	.L67-.L59
	.long	.L66-.L59
	.long	.L65-.L59
	.long	.L64-.L59
	.long	.L63-.L59
	.long	.L62-.L59
	.long	.L61-.L59
	.long	.L60-.L59
	.long	.L58-.L59
	.long	.L58-.L59
	.text
	.p2align 4,,10
	.p2align 3
.L67:
	leaq	.LC84(%rip), %rax
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L58:
	leaq	.LC49(%rip), %rax
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L70:
	leaq	.LC67(%rip), %rax
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L74:
	leaq	.LC69(%rip), %rax
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L71:
	leaq	.LC66(%rip), %rax
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L75:
	leaq	.LC68(%rip), %rax
	jmp	.L55
.L92:
	leaq	.LC53(%rip), %rax
	jmp	.L55
.L94:
	leaq	.LC61(%rip), %rax
	jmp	.L55
.L93:
	leaq	.LC62(%rip), %rax
	jmp	.L55
.L66:
	leaq	.LC86(%rip), %rax
	jmp	.L55
.L65:
	leaq	.LC87(%rip), %rax
	jmp	.L55
.L64:
	leaq	.LC88(%rip), %rax
	jmp	.L55
.L63:
	leaq	.LC89(%rip), %rax
	jmp	.L55
.L62:
	leaq	.LC90(%rip), %rax
	jmp	.L55
.L61:
	leaq	.LC91(%rip), %rax
	jmp	.L55
.L60:
	leaq	.LC92(%rip), %rax
	jmp	.L55
.L98:
	leaq	.LC58(%rip), %rax
	jmp	.L55
.L97:
	leaq	.LC59(%rip), %rax
	jmp	.L55
.L79:
	leaq	.LC76(%rip), %rax
	jmp	.L55
.L78:
	leaq	.LC77(%rip), %rax
	jmp	.L55
.L77:
	leaq	.LC78(%rip), %rax
	jmp	.L55
.L76:
	leaq	.LC79(%rip), %rax
	jmp	.L55
.L90:
	leaq	.LC63(%rip), %rax
	jmp	.L55
.L89:
	leaq	.LC64(%rip), %rax
	jmp	.L55
.L91:
	leaq	.LC57(%rip), %rax
	jmp	.L55
.L88:
	leaq	.LC65(%rip), %rax
	jmp	.L55
.L96:
	leaq	.LC52(%rip), %rax
	jmp	.L55
.L95:
	leaq	.LC60(%rip), %rax
	jmp	.L55
.L72:
	leaq	.LC51(%rip), %rax
	jmp	.L55
.L69:
	leaq	.LC82(%rip), %rax
	jmp	.L55
.L68:
	leaq	.LC83(%rip), %rax
	jmp	.L55
.L86:
	leaq	.LC70(%rip), %rax
	jmp	.L55
.L85:
	leaq	.LC71(%rip), %rax
	jmp	.L55
.L84:
	leaq	.LC81(%rip), %rax
	jmp	.L55
.L83:
	leaq	.LC72(%rip), %rax
	jmp	.L55
.L87:
	leaq	.LC54(%rip), %rax
	jmp	.L55
.L101:
	leaq	.LC55(%rip), %rax
	jmp	.L55
.L100:
	leaq	.LC56(%rip), %rax
	jmp	.L55
.L99:
	leaq	.LC80(%rip), %rax
	jmp	.L55
.L82:
	leaq	.LC73(%rip), %rax
	jmp	.L55
.L81:
	leaq	.LC74(%rip), %rax
	jmp	.L55
.L80:
	leaq	.LC75(%rip), %rax
	jmp	.L55
.L103:
	leaq	.LC85(%rip), %rax
	jmp	.L55
.L73:
	leaq	.LC93(%rip), %rax
	jmp	.L55
.L57:
	leaq	.LC48(%rip), %rax
	jmp	.L55
	.cfi_endproc
.LFE965:
	.size	SSL_state_string, .-SSL_state_string
	.section	.rodata.str1.1
.LC94:
	.string	"unknown"
.LC95:
	.string	"warning"
.LC96:
	.string	"fatal"
	.text
	.p2align 4
	.globl	SSL_alert_type_string_long
	.type	SSL_alert_type_string_long, @function
SSL_alert_type_string_long:
.LFB966:
	.cfi_startproc
	endbr64
	sarl	$8, %edi
	leaq	.LC95(%rip), %rax
	cmpl	$1, %edi
	je	.L106
	cmpl	$2, %edi
	leaq	.LC94(%rip), %rax
	leaq	.LC96(%rip), %rdx
	cmove	%rdx, %rax
.L106:
	ret
	.cfi_endproc
.LFE966:
	.size	SSL_alert_type_string_long, .-SSL_alert_type_string_long
	.section	.rodata.str1.1
.LC97:
	.string	"U"
.LC98:
	.string	"W"
.LC99:
	.string	"F"
	.text
	.p2align 4
	.globl	SSL_alert_type_string
	.type	SSL_alert_type_string, @function
SSL_alert_type_string:
.LFB967:
	.cfi_startproc
	endbr64
	sarl	$8, %edi
	leaq	.LC98(%rip), %rax
	cmpl	$1, %edi
	je	.L111
	cmpl	$2, %edi
	leaq	.LC97(%rip), %rax
	leaq	.LC99(%rip), %rdx
	cmove	%rdx, %rax
.L111:
	ret
	.cfi_endproc
.LFE967:
	.size	SSL_alert_type_string, .-SSL_alert_type_string
	.section	.rodata.str1.1
.LC100:
	.string	"UK"
.LC101:
	.string	"CN"
.LC102:
	.string	"BM"
.LC103:
	.string	"DF"
.LC104:
	.string	"HF"
.LC105:
	.string	"NC"
.LC106:
	.string	"BC"
.LC107:
	.string	"UC"
.LC108:
	.string	"CR"
.LC109:
	.string	"CE"
.LC110:
	.string	"CU"
.LC111:
	.string	"IP"
.LC112:
	.string	"DC"
.LC113:
	.string	"RO"
.LC114:
	.string	"CA"
.LC115:
	.string	"AD"
.LC116:
	.string	"DE"
.LC117:
	.string	"CY"
.LC118:
	.string	"ER"
.LC119:
	.string	"PV"
.LC120:
	.string	"IS"
.LC121:
	.string	"IE"
.LC122:
	.string	"US"
.LC123:
	.string	"NR"
.LC124:
	.string	"UE"
.LC125:
	.string	"CO"
.LC126:
	.string	"UN"
.LC127:
	.string	"BR"
.LC128:
	.string	"BH"
.LC129:
	.string	"UP"
.LC130:
	.string	"UM"
	.text
	.p2align 4
	.globl	SSL_alert_desc_string
	.type	SSL_alert_desc_string, @function
SSL_alert_desc_string:
.LFB968:
	.cfi_startproc
	endbr64
	movzbl	%dil, %eax
	cmpb	$115, %dil
	ja	.L117
	leaq	.L119(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L119:
	.long	.L149-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L147-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L146-.L119
	.long	.L145-.L119
	.long	.L144-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L143-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L142-.L119
	.long	.L141-.L119
	.long	.L140-.L119
	.long	.L139-.L119
	.long	.L138-.L119
	.long	.L137-.L119
	.long	.L136-.L119
	.long	.L135-.L119
	.long	.L134-.L119
	.long	.L133-.L119
	.long	.L132-.L119
	.long	.L131-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L130-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L129-.L119
	.long	.L128-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L127-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L126-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L125-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L124-.L119
	.long	.L123-.L119
	.long	.L122-.L119
	.long	.L121-.L119
	.long	.L120-.L119
	.long	.L118-.L119
	.text
	.p2align 4,,10
	.p2align 3
.L117:
	leaq	.LC100(%rip), %rax
	ret
.L118:
	leaq	.LC129(%rip), %rax
	ret
.L120:
	leaq	.LC128(%rip), %rax
	ret
.L121:
	leaq	.LC127(%rip), %rax
	ret
.L122:
	leaq	.LC126(%rip), %rax
	ret
.L123:
	leaq	.LC125(%rip), %rax
	ret
.L124:
	leaq	.LC124(%rip), %rax
	ret
.L125:
	leaq	.LC123(%rip), %rax
	ret
.L126:
	leaq	.LC122(%rip), %rax
	ret
.L127:
	leaq	.LC121(%rip), %rax
	ret
.L128:
	leaq	.LC120(%rip), %rax
	ret
.L129:
	leaq	.LC119(%rip), %rax
	ret
.L130:
	leaq	.LC118(%rip), %rax
	ret
.L131:
	leaq	.LC117(%rip), %rax
	ret
.L132:
	leaq	.LC116(%rip), %rax
	ret
.L133:
	leaq	.LC115(%rip), %rax
	ret
.L134:
	leaq	.LC114(%rip), %rax
	ret
.L135:
	leaq	.LC111(%rip), %rax
	ret
.L136:
	leaq	.LC110(%rip), %rax
	ret
.L137:
	leaq	.LC109(%rip), %rax
	ret
.L138:
	leaq	.LC108(%rip), %rax
	ret
.L139:
	leaq	.LC107(%rip), %rax
	ret
.L140:
	leaq	.LC106(%rip), %rax
	ret
.L141:
	leaq	.LC105(%rip), %rax
	ret
.L142:
	leaq	.LC104(%rip), %rax
	ret
.L143:
	leaq	.LC103(%rip), %rax
	ret
.L144:
	leaq	.LC113(%rip), %rax
	ret
.L145:
	leaq	.LC112(%rip), %rax
	ret
.L146:
	leaq	.LC102(%rip), %rax
	ret
.L149:
	leaq	.LC101(%rip), %rax
	ret
.L147:
	leaq	.LC130(%rip), %rax
	ret
	.cfi_endproc
.LFE968:
	.size	SSL_alert_desc_string, .-SSL_alert_desc_string
	.section	.rodata.str1.1
.LC131:
	.string	"close notify"
.LC132:
	.string	"bad record mac"
.LC133:
	.string	"decompression failure"
.LC134:
	.string	"handshake failure"
.LC135:
	.string	"no certificate"
.LC136:
	.string	"bad certificate"
.LC137:
	.string	"unsupported certificate"
.LC138:
	.string	"certificate revoked"
.LC139:
	.string	"certificate expired"
.LC140:
	.string	"certificate unknown"
.LC141:
	.string	"illegal parameter"
.LC142:
	.string	"decryption failed"
.LC143:
	.string	"record overflow"
.LC144:
	.string	"unknown CA"
.LC145:
	.string	"access denied"
.LC146:
	.string	"decode error"
.LC147:
	.string	"decrypt error"
.LC148:
	.string	"export restriction"
.LC149:
	.string	"protocol version"
.LC150:
	.string	"insufficient security"
.LC151:
	.string	"internal error"
.LC152:
	.string	"user canceled"
.LC153:
	.string	"no renegotiation"
.LC154:
	.string	"unsupported extension"
.LC155:
	.string	"certificate unobtainable"
.LC156:
	.string	"unrecognized name"
	.section	.rodata.str1.8
	.align 8
.LC157:
	.string	"bad certificate status response"
	.section	.rodata.str1.1
.LC158:
	.string	"bad certificate hash value"
.LC159:
	.string	"unknown PSK identity"
.LC160:
	.string	"no application protocol"
.LC161:
	.string	"unexpected_message"
	.text
	.p2align 4
	.globl	SSL_alert_desc_string_long
	.type	SSL_alert_desc_string_long, @function
SSL_alert_desc_string_long:
.LFB969:
	.cfi_startproc
	endbr64
	movzbl	%dil, %eax
	cmpb	$120, %dil
	ja	.L151
	leaq	.L153(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L153:
	.long	.L184-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L182-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L181-.L153
	.long	.L180-.L153
	.long	.L179-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L178-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L177-.L153
	.long	.L176-.L153
	.long	.L175-.L153
	.long	.L174-.L153
	.long	.L173-.L153
	.long	.L172-.L153
	.long	.L171-.L153
	.long	.L170-.L153
	.long	.L169-.L153
	.long	.L168-.L153
	.long	.L167-.L153
	.long	.L166-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L165-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L164-.L153
	.long	.L163-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L162-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L161-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L160-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L159-.L153
	.long	.L158-.L153
	.long	.L157-.L153
	.long	.L156-.L153
	.long	.L155-.L153
	.long	.L154-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L152-.L153
	.text
	.p2align 4,,10
	.p2align 3
.L151:
	leaq	.LC94(%rip), %rax
	ret
.L152:
	leaq	.LC160(%rip), %rax
	ret
.L154:
	leaq	.LC159(%rip), %rax
	ret
.L155:
	leaq	.LC158(%rip), %rax
	ret
.L156:
	leaq	.LC157(%rip), %rax
	ret
.L157:
	leaq	.LC156(%rip), %rax
	ret
.L158:
	leaq	.LC155(%rip), %rax
	ret
.L159:
	leaq	.LC154(%rip), %rax
	ret
.L160:
	leaq	.LC153(%rip), %rax
	ret
.L161:
	leaq	.LC152(%rip), %rax
	ret
.L162:
	leaq	.LC151(%rip), %rax
	ret
.L163:
	leaq	.LC150(%rip), %rax
	ret
.L164:
	leaq	.LC149(%rip), %rax
	ret
.L165:
	leaq	.LC148(%rip), %rax
	ret
.L166:
	leaq	.LC147(%rip), %rax
	ret
.L167:
	leaq	.LC146(%rip), %rax
	ret
.L168:
	leaq	.LC145(%rip), %rax
	ret
.L169:
	leaq	.LC144(%rip), %rax
	ret
.L170:
	leaq	.LC141(%rip), %rax
	ret
.L171:
	leaq	.LC140(%rip), %rax
	ret
.L172:
	leaq	.LC139(%rip), %rax
	ret
.L173:
	leaq	.LC138(%rip), %rax
	ret
.L174:
	leaq	.LC137(%rip), %rax
	ret
.L175:
	leaq	.LC136(%rip), %rax
	ret
.L176:
	leaq	.LC135(%rip), %rax
	ret
.L177:
	leaq	.LC134(%rip), %rax
	ret
.L178:
	leaq	.LC133(%rip), %rax
	ret
.L179:
	leaq	.LC143(%rip), %rax
	ret
.L180:
	leaq	.LC142(%rip), %rax
	ret
.L181:
	leaq	.LC132(%rip), %rax
	ret
.L184:
	leaq	.LC131(%rip), %rax
	ret
.L182:
	leaq	.LC161(%rip), %rax
	ret
	.cfi_endproc
.LFE969:
	.size	SSL_alert_desc_string_long, .-SSL_alert_desc_string_long
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
