	.file	"md5_one.c"
	.text
	.p2align 4
	.globl	MD5
	.type	MD5, @function
MD5:
.LFB151:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-144(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	subq	$112, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	leaq	m.4940(%rip), %rax
	cmove	%rax, %rbx
	call	MD5_Init@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	je	.L1
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	MD5_Update@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	MD5_Final@PLT
	movl	$92, %esi
	movq	%r12, %rdi
	call	OPENSSL_cleanse@PLT
	movq	%rbx, %rax
.L1:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L10
	addq	$112, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L10:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE151:
	.size	MD5, .-MD5
	.local	m.4940
	.comm	m.4940,16,16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
