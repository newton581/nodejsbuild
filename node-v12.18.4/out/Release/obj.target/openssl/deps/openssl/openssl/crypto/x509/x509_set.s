	.file	"x509_set.c"
	.text
	.p2align 4
	.globl	X509_set_version
	.type	X509_set_version, @function
X509_set_version:
.LFB1394:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L14
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rdi
	testq	%rsi, %rsi
	je	.L15
	testq	%rdi, %rdi
	je	.L16
.L5:
	addq	$24, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	ASN1_INTEGER_set@PLT
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movq	%rsi, -24(%rbp)
	call	ASN1_INTEGER_new@PLT
	movq	-24(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	jne	.L5
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	call	ASN1_INTEGER_free@PLT
	movq	$0, (%rbx)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1394:
	.size	X509_set_version, .-X509_set_version
	.p2align 4
	.globl	X509_set_serialNumber
	.type	X509_set_serialNumber, @function
X509_set_serialNumber:
.LFB1395:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L19
	addq	$8, %rdi
	movl	$1, %eax
	cmpq	%rsi, %rdi
	jne	.L21
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	jmp	ASN1_STRING_copy@PLT
	.p2align 4,,10
	.p2align 3
.L19:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1395:
	.size	X509_set_serialNumber, .-X509_set_serialNumber
	.p2align 4
	.globl	X509_set_issuer_name
	.type	X509_set_issuer_name, @function
X509_set_issuer_name:
.LFB1396:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L23
	addq	$48, %rdi
	jmp	X509_NAME_set@PLT
	.p2align 4,,10
	.p2align 3
.L23:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1396:
	.size	X509_set_issuer_name, .-X509_set_issuer_name
	.p2align 4
	.globl	X509_set_subject_name
	.type	X509_set_subject_name, @function
X509_set_subject_name:
.LFB1397:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L25
	addq	$72, %rdi
	jmp	X509_NAME_set@PLT
	.p2align 4,,10
	.p2align 3
.L25:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1397:
	.size	X509_set_subject_name, .-X509_set_subject_name
	.p2align 4
	.globl	x509_set1_time
	.type	x509_set1_time, @function
x509_set1_time:
.LFB1398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpq	%rsi, (%rdi)
	je	.L27
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	call	ASN1_STRING_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L27
	movq	(%rbx), %rdi
	call	ASN1_TIME_free@PLT
	movq	%r12, (%rbx)
.L27:
	xorl	%eax, %eax
	testq	%r12, %r12
	popq	%rbx
	popq	%r12
	setne	%al
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1398:
	.size	x509_set1_time, .-x509_set1_time
	.p2align 4
	.globl	X509_set1_notBefore
	.type	X509_set1_notBefore, @function
X509_set1_notBefore:
.LFB1399:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L40
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	56(%rdi), %r12
	movq	%rdi, %rbx
	cmpq	%r12, %rsi
	je	.L34
	movq	%rsi, %rdi
	call	ASN1_STRING_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L34
	movq	56(%rbx), %rdi
	call	ASN1_TIME_free@PLT
	movq	%r12, 56(%rbx)
.L34:
	xorl	%eax, %eax
	testq	%r12, %r12
	popq	%rbx
	popq	%r12
	setne	%al
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1399:
	.size	X509_set1_notBefore, .-X509_set1_notBefore
	.p2align 4
	.globl	X509_set1_notAfter
	.type	X509_set1_notAfter, @function
X509_set1_notAfter:
.LFB1400:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L53
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	64(%rdi), %r12
	movq	%rdi, %rbx
	cmpq	%r12, %rsi
	je	.L47
	movq	%rsi, %rdi
	call	ASN1_STRING_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L47
	movq	64(%rbx), %rdi
	call	ASN1_TIME_free@PLT
	movq	%r12, 64(%rbx)
.L47:
	xorl	%eax, %eax
	testq	%r12, %r12
	popq	%rbx
	popq	%r12
	setne	%al
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1400:
	.size	X509_set1_notAfter, .-X509_set1_notAfter
	.p2align 4
	.globl	X509_set_pubkey
	.type	X509_set_pubkey, @function
X509_set_pubkey:
.LFB1401:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L59
	addq	$80, %rdi
	jmp	X509_PUBKEY_set@PLT
	.p2align 4,,10
	.p2align 3
.L59:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1401:
	.size	X509_set_pubkey, .-X509_set_pubkey
	.p2align 4
	.globl	X509_up_ref
	.type	X509_up_ref, @function
X509_up_ref:
.LFB1402:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	lock xaddl	%eax, 192(%rdi)
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1402:
	.size	X509_up_ref, .-X509_up_ref
	.p2align 4
	.globl	X509_get_version
	.type	X509_get_version, @function
X509_get_version:
.LFB1403:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	jmp	ASN1_INTEGER_get@PLT
	.cfi_endproc
.LFE1403:
	.size	X509_get_version, .-X509_get_version
	.p2align 4
	.globl	X509_get0_notBefore
	.type	X509_get0_notBefore, @function
X509_get0_notBefore:
.LFB1404:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rax
	ret
	.cfi_endproc
.LFE1404:
	.size	X509_get0_notBefore, .-X509_get0_notBefore
	.p2align 4
	.globl	X509_get0_notAfter
	.type	X509_get0_notAfter, @function
X509_get0_notAfter:
.LFB1405:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	ret
	.cfi_endproc
.LFE1405:
	.size	X509_get0_notAfter, .-X509_get0_notAfter
	.p2align 4
	.globl	X509_getm_notBefore
	.type	X509_getm_notBefore, @function
X509_getm_notBefore:
.LFB1420:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rax
	ret
	.cfi_endproc
.LFE1420:
	.size	X509_getm_notBefore, .-X509_getm_notBefore
	.p2align 4
	.globl	X509_getm_notAfter
	.type	X509_getm_notAfter, @function
X509_getm_notAfter:
.LFB1422:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	ret
	.cfi_endproc
.LFE1422:
	.size	X509_getm_notAfter, .-X509_getm_notAfter
	.p2align 4
	.globl	X509_get_signature_type
	.type	X509_get_signature_type, @function
X509_get_signature_type:
.LFB1408:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	136(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	OBJ_obj2nid@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%eax, %edi
	jmp	EVP_PKEY_type@PLT
	.cfi_endproc
.LFE1408:
	.size	X509_get_signature_type, .-X509_get_signature_type
	.p2align 4
	.globl	X509_get_X509_PUBKEY
	.type	X509_get_X509_PUBKEY, @function
X509_get_X509_PUBKEY:
.LFB1409:
	.cfi_startproc
	endbr64
	movq	80(%rdi), %rax
	ret
	.cfi_endproc
.LFE1409:
	.size	X509_get_X509_PUBKEY, .-X509_get_X509_PUBKEY
	.p2align 4
	.globl	X509_get0_extensions
	.type	X509_get0_extensions, @function
X509_get0_extensions:
.LFB1410:
	.cfi_startproc
	endbr64
	movq	104(%rdi), %rax
	ret
	.cfi_endproc
.LFE1410:
	.size	X509_get0_extensions, .-X509_get0_extensions
	.p2align 4
	.globl	X509_get0_uids
	.type	X509_get0_uids, @function
X509_get0_uids:
.LFB1411:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L71
	movq	88(%rdi), %rax
	movq	%rax, (%rsi)
.L71:
	testq	%rdx, %rdx
	je	.L70
	movq	96(%rdi), %rax
	movq	%rax, (%rdx)
.L70:
	ret
	.cfi_endproc
.LFE1411:
	.size	X509_get0_uids, .-X509_get0_uids
	.p2align 4
	.globl	X509_get0_tbs_sigalg
	.type	X509_get0_tbs_sigalg, @function
X509_get0_tbs_sigalg:
.LFB1412:
	.cfi_startproc
	endbr64
	leaq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE1412:
	.size	X509_get0_tbs_sigalg, .-X509_get0_tbs_sigalg
	.p2align 4
	.globl	X509_SIG_INFO_get
	.type	X509_SIG_INFO_get, @function
X509_SIG_INFO_get:
.LFB1413:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L81
	movl	(%rdi), %eax
	movl	%eax, (%rsi)
.L81:
	testq	%rdx, %rdx
	je	.L82
	movl	4(%rdi), %eax
	movl	%eax, (%rdx)
.L82:
	testq	%rcx, %rcx
	je	.L83
	movl	8(%rdi), %eax
	movl	%eax, (%rcx)
.L83:
	movl	12(%rdi), %eax
	testq	%r8, %r8
	je	.L84
	movl	%eax, (%r8)
	movl	12(%rdi), %eax
.L84:
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE1413:
	.size	X509_SIG_INFO_get, .-X509_SIG_INFO_get
	.p2align 4
	.globl	X509_SIG_INFO_set
	.type	X509_SIG_INFO_set, @function
X509_SIG_INFO_set:
.LFB1414:
	.cfi_startproc
	endbr64
	movl	%esi, (%rdi)
	movl	%edx, 4(%rdi)
	movl	%ecx, 8(%rdi)
	movl	%r8d, 12(%rdi)
	ret
	.cfi_endproc
.LFE1414:
	.size	X509_SIG_INFO_set, .-X509_SIG_INFO_set
	.p2align 4
	.globl	X509_get_signature_info
	.type	X509_get_signature_info, @function
X509_get_signature_info:
.LFB1415:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$-1, %esi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	movl	$-1, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	X509_check_purpose@PLT
	testq	%r15, %r15
	je	.L99
	movl	176(%rbx), %eax
	movl	%eax, (%r15)
.L99:
	testq	%r14, %r14
	je	.L100
	movl	180(%rbx), %eax
	movl	%eax, (%r14)
.L100:
	testq	%r13, %r13
	je	.L101
	movl	184(%rbx), %eax
	movl	%eax, 0(%r13)
.L101:
	movl	188(%rbx), %eax
	testq	%r12, %r12
	je	.L102
	movl	%eax, (%r12)
	movl	188(%rbx), %eax
.L102:
	addq	$8, %rsp
	andl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1415:
	.size	X509_get_signature_info, .-X509_get_signature_info
	.p2align 4
	.globl	x509_init_sig_info
	.type	x509_init_sig_info, @function
x509_init_sig_info:
.LFB1417:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$4294967295, %eax
	movq	$0, 176(%rdi)
	movq	%rax, 184(%rdi)
	movq	136(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	leaq	-32(%rbp), %rdx
	leaq	-28(%rbp), %rsi
	movl	%eax, %edi
	call	OBJ_find_sigid_algs@PLT
	testl	%eax, %eax
	je	.L116
	movl	-32(%rbp), %esi
	testl	%esi, %esi
	je	.L116
	movl	-28(%rbp), %edi
	movl	%esi, 180(%rbx)
	testl	%edi, %edi
	jne	.L118
	xorl	%edi, %edi
	call	EVP_PKEY_asn1_find@PLT
	testq	%rax, %rax
	je	.L116
	movq	216(%rax), %rax
	testq	%rax, %rax
	je	.L116
	leaq	152(%rbx), %rdx
	leaq	136(%rbx), %rsi
	leaq	176(%rbx), %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L116
	orl	$1, 188(%rbx)
	.p2align 4,,10
	.p2align 3
.L116:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L146
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	orl	$1, 188(%rbx)
	movl	%edi, 176(%rbx)
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L116
	call	EVP_MD_size@PLT
	sall	$2, %eax
	movl	%eax, 184(%rbx)
	movl	-28(%rbp), %eax
	cmpl	$64, %eax
	je	.L120
	subl	$672, %eax
	cmpl	$2, %eax
	ja	.L116
.L120:
	orl	$2, 188(%rbx)
	jmp	.L116
.L146:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1417:
	.size	x509_init_sig_info, .-x509_init_sig_info
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
