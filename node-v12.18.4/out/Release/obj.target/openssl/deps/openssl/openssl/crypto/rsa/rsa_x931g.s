	.file	"rsa_x931g.c"
	.text
	.p2align 4
	.globl	RSA_X931_derive_ex
	.type	RSA_X931_derive_ex, @function
RSA_X931_derive_ex:
.LFB408:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -64(%rbp)
	movq	%r8, -72(%rbp)
	movq	%r9, -56(%rbp)
	testq	%rdi, %rdi
	je	.L4
	movq	%rdi, %rbx
	movq	%rsi, %r13
	movq	%rdx, %r14
	call	BN_CTX_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L4
	movq	%rax, %rdi
	call	BN_CTX_start@PLT
	movq	%r15, %rdi
	call	BN_CTX_get@PLT
	movq	%r15, %rdi
	movq	%rax, -96(%rbp)
	call	BN_CTX_get@PLT
	movq	%r15, %rdi
	movq	%rax, -80(%rbp)
	call	BN_CTX_get@PLT
	movq	%r15, %rdi
	movq	%rax, -88(%rbp)
	call	BN_CTX_get@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L33
	movq	32(%rbx), %r11
	testq	%r11, %r11
	je	.L52
.L5:
	cmpq	$0, 24(%rbp)
	je	.L10
	cmpq	$0, 48(%rbx)
	je	.L53
	cmpq	$0, 48(%rbp)
	je	.L12
	cmpq	$0, 56(%rbx)
	je	.L16
.L15:
	call	BN_new@PLT
	movq	%rax, 24(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L23
	movq	56(%rbx), %rdx
	movq	48(%rbx), %rsi
	movq	%r15, %rcx
	call	BN_mul@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L28
	call	BN_value_one@PLT
	movq	-80(%rbp), %r14
	movq	48(%rbx), %rsi
	movq	%rax, %rdx
	movq	%r14, %rdi
	call	BN_sub@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.L54
.L28:
	xorl	%r12d, %r12d
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L4:
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
.L3:
	movq	%r15, %rdi
	call	BN_CTX_end@PLT
	movq	%r15, %rdi
	call	BN_CTX_free@PLT
	movq	%r12, %rdi
	call	BN_CTX_free@PLT
.L1:
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movq	%r11, -56(%rbp)
	call	BN_new@PLT
	movq	%rax, 56(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L23
	movq	-56(%rbp), %r11
	subq	$8, %rsp
	movq	40(%rbp), %r9
	pushq	64(%rbp)
	movq	32(%rbp), %r8
	pushq	%r15
	movq	48(%rbp), %rcx
	movq	-72(%rbp), %rdx
	pushq	%r11
	movq	-64(%rbp), %rsi
	call	BN_X931_derive_prime_ex@PLT
	addq	$32, %rsp
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L28
.L8:
	cmpq	$0, 48(%rbx)
	je	.L11
.L12:
	cmpq	$0, 56(%rbx)
	jne	.L15
.L11:
	movq	%r15, %rdi
	movl	$2, %r13d
	call	BN_CTX_end@PLT
	movq	%r15, %rdi
	call	BN_CTX_free@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L53:
	movq	%r11, -104(%rbp)
	call	BN_new@PLT
	movq	%rax, 48(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L23
	movq	-104(%rbp), %r11
	subq	$8, %rsp
	pushq	64(%rbp)
	movq	%r13, %rsi
	movq	16(%rbp), %r9
	pushq	%r15
	movq	%r14, %rdx
	movq	-56(%rbp), %r8
	movq	24(%rbp), %rcx
	pushq	%r11
	call	BN_X931_derive_prime_ex@PLT
	addq	$32, %rsp
	movq	-104(%rbp), %r11
	testl	%eax, %eax
	movl	%eax, %r13d
	je	.L28
	.p2align 4,,10
	.p2align 3
.L10:
	cmpq	$0, 48(%rbp)
	je	.L8
	cmpq	$0, 56(%rbx)
	je	.L16
	cmpq	$0, 48(%rbx)
	jne	.L15
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L23:
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L33:
	xorl	%r13d, %r13d
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L52:
	movq	56(%rbp), %rdi
	call	BN_dup@PLT
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	je	.L23
	movq	56(%rbp), %r11
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L54:
	call	BN_value_one@PLT
	movq	56(%rbx), %rsi
	movq	-88(%rbp), %rdi
	movq	%rax, %rdx
	call	BN_sub@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L28
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rdi
	movq	%r15, %rcx
	movq	%r14, %rsi
	call	BN_mul@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L28
	movq	-88(%rbp), %rdx
	movq	%r15, %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	BN_gcd@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L28
	movq	-96(%rbp), %rax
	movq	%r12, %rcx
	movq	%r15, %r8
	xorl	%esi, %esi
	xorl	%r12d, %r12d
	movq	%rax, %rdx
	movq	%rax, %rdi
	call	BN_div@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L3
	call	BN_CTX_new@PLT
	xorl	%r13d, %r13d
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L3
	movq	32(%rbx), %rsi
	movq	-96(%rbp), %rdx
	movq	%rax, %rcx
	xorl	%edi, %edi
	call	BN_mod_inverse@PLT
	movq	%rax, 40(%rbx)
	testq	%rax, %rax
	je	.L3
	call	BN_new@PLT
	movq	%rax, 64(%rbx)
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L3
	movq	40(%rbx), %rdx
	movq	%r15, %r8
	movq	%r14, %rcx
	xorl	%edi, %edi
	call	BN_div@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L3
	call	BN_new@PLT
	movq	%rax, 72(%rbx)
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L33
	movq	40(%rbx), %rdx
	movq	-88(%rbp), %rcx
	movq	%r15, %r8
	xorl	%edi, %edi
	call	BN_div@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L3
	movq	48(%rbx), %rdx
	movq	56(%rbx), %rsi
	xorl	%edi, %edi
	movq	%r12, %rcx
	xorl	%r13d, %r13d
	call	BN_mod_inverse@PLT
	testq	%rax, %rax
	movq	%rax, 80(%rbx)
	setne	%r13b
	jmp	.L3
	.cfi_endproc
.LFE408:
	.size	RSA_X931_derive_ex, .-RSA_X931_derive_ex
	.p2align 4
	.globl	RSA_X931_generate_key_ex
	.type	RSA_X931_generate_key_ex, @function
RSA_X931_generate_key_ex:
.LFB409:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	BN_CTX_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L58
	movq	%rax, %rdi
	call	BN_CTX_start@PLT
	movq	%r14, %rdi
	call	BN_CTX_get@PLT
	movq	%r14, %rdi
	movq	%rax, %r15
	call	BN_CTX_get@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L58
	movq	%r14, %rcx
	movl	%r13d, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	BN_X931_generate_Xpq@PLT
	testl	%eax, %eax
	je	.L58
	call	BN_new@PLT
	movq	%rax, 48(%r12)
	call	BN_new@PLT
	movq	48(%r12), %rdi
	movq	%rax, 56(%r12)
	testq	%rax, %rax
	je	.L58
	testq	%rdi, %rdi
	je	.L58
	movq	-64(%rbp), %r13
	subq	$8, %rsp
	pushq	-56(%rbp)
	movq	%r15, %r9
	pushq	%r14
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%r13
	xorl	%esi, %esi
	call	BN_X931_generate_prime_ex@PLT
	addq	$32, %rsp
	testl	%eax, %eax
	je	.L58
	movq	-56(%rbp), %r15
	subq	$8, %rsp
	movq	56(%r12), %rdi
	movq	%rbx, %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	pushq	%r15
	pushq	%r14
	pushq	%r13
	call	BN_X931_generate_prime_ex@PLT
	addq	$32, %rsp
	testl	%eax, %eax
	jne	.L72
	.p2align 4,,10
	.p2align 3
.L58:
	xorl	%r12d, %r12d
.L57:
	movq	%r14, %rdi
	call	BN_CTX_end@PLT
	movq	%r14, %rdi
	call	BN_CTX_free@PLT
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore_state
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	pushq	%r15
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	pushq	%r13
	xorl	%r12d, %r12d
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	RSA_X931_derive_ex
	addq	$64, %rsp
	testl	%eax, %eax
	setne	%r12b
	jmp	.L57
	.cfi_endproc
.LFE409:
	.size	RSA_X931_generate_key_ex, .-RSA_X931_generate_key_ex
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
