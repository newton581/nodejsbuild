	.file	"f_impl.c"
	.text
	.p2align 4
	.globl	gf_mul
	.type	gf_mul, @function
gf_mul:
.LFB83:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-12(%rsi), %r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movdqu	32(%rsi), %xmm4
	movdqu	(%rsi), %xmm0
	movq	%rdi, -160(%rbp)
	movdqu	48(%rsi), %xmm5
	movdqu	16(%rsi), %xmm3
	xorl	%edi, %edi
	paddd	%xmm4, %xmm0
	movdqu	32(%rdx), %xmm4
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -128(%rbp)
	movdqu	(%rdx), %xmm0
	paddd	%xmm5, %xmm3
	movaps	%xmm3, -112(%rbp)
	pshufd	$27, %xmm3, %xmm3
	leaq	-140(%rbp), %rax
	paddd	%xmm4, %xmm0
	movdqu	48(%rdx), %xmm4
	movdqa	%xmm3, %xmm5
	movq	%rax, -144(%rbp)
	movaps	%xmm0, -96(%rbp)
	movdqu	16(%rdx), %xmm0
	punpckhdq	%xmm3, %xmm5
	xorl	%eax, %eax
	movq	%r13, -152(%rbp)
	punpckldq	%xmm3, %xmm3
	paddd	%xmm4, %xmm0
	pxor	%xmm4, %xmm4
	movaps	%xmm0, -80(%rbp)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L24:
	movl	$7, %r9d
	movl	$8, %r11d
	leal	16(%rdi), %r15d
	subl	%r10d, %r9d
	subl	%r10d, %r11d
	cmpl	$2, %r9d
	jbe	.L11
	movdqu	16(%rsi), %xmm7
	movdqu	(%rdx,%r8,4), %xmm1
	addl	$5, %edi
	pshufd	$27, %xmm7, %xmm0
	movdqa	%xmm1, %xmm2
	movdqu	32(%rdx,%r8,4), %xmm7
	movdqa	%xmm0, %xmm6
	punpckhdq	%xmm1, %xmm2
	punpckldq	%xmm1, %xmm1
	punpckhdq	%xmm0, %xmm6
	punpckldq	%xmm0, %xmm0
	movdqa	%xmm7, %xmm8
	pmuludq	%xmm2, %xmm6
	movdqu	-96(%rbp,%r8,4), %xmm2
	pmuludq	%xmm1, %xmm0
	punpckhdq	%xmm7, %xmm8
	punpckldq	%xmm7, %xmm7
	movdqa	%xmm2, %xmm1
	punpckhdq	%xmm2, %xmm1
	punpckldq	%xmm2, %xmm2
	pmuludq	%xmm3, %xmm2
	pmuludq	%xmm5, %xmm1
	paddq	%xmm0, %xmm6
	paddq	%xmm2, %xmm1
	movdqu	48(%rsi), %xmm2
	pshufd	$27, %xmm2, %xmm0
	movdqa	%xmm0, %xmm2
	punpckhdq	%xmm0, %xmm2
	punpckldq	%xmm0, %xmm0
	pmuludq	%xmm8, %xmm2
	pmuludq	%xmm7, %xmm0
	paddq	%xmm2, %xmm0
	movdqa	%xmm0, %xmm2
	psrldq	$8, %xmm2
	paddq	%xmm2, %xmm0
	movq	%xmm0, %r9
	movdqa	%xmm1, %xmm0
	psrldq	$8, %xmm0
	addq	%r9, %rcx
	paddq	%xmm0, %xmm1
	movdqa	%xmm4, %xmm0
	psubq	%xmm6, %xmm0
	movq	%xmm1, %r9
	psrldq	$8, %xmm0
	psubq	%xmm6, %xmm0
	movq	%xmm0, %rbx
	addq	%rbx, %rax
	cmpl	$4, %r11d
	je	.L6
.L5:
	movl	-132(%rbp), %ebx
	movslq	%edi, %r11
	movl	(%rdx,%r11,4), %r13d
	leaq	0(,%r11,4), %r12
	subl	%edi, %ebx
	movslq	%ebx, %rbx
	movl	(%rsi,%rbx,4), %r14d
	movl	-128(%rbp,%rbx,4), %ebx
	imulq	%r13, %r14
	subq	%r14, %rax
	movl	-96(%rbp,%r11,4), %r14d
	movl	32(%rdx,%r11,4), %r11d
	imulq	%rbx, %r14
	movl	%r15d, %ebx
	subl	%edi, %ebx
	movslq	%ebx, %rbx
	movl	(%rsi,%rbx,4), %ebx
	addq	%r14, %r9
	imulq	%r11, %rbx
	addq	%rbx, %rcx
	leal	1(%rdi), %ebx
	cmpl	$7, %edi
	je	.L6
	movl	-132(%rbp), %r11d
	movl	4(%rdx,%r12), %r13d
	subl	%ebx, %r11d
	movslq	%r11d, %r11
	movl	(%rsi,%r11,4), %r14d
	movl	-128(%rbp,%r11,4), %r11d
	imulq	%r13, %r14
	subq	%r14, %rax
	movslq	%ebx, %r14
	movl	-96(%rbp,%r14,4), %r14d
	imulq	%r14, %r11
	movl	%r15d, %r14d
	subl	%ebx, %r14d
	movl	36(%rdx,%r12), %ebx
	addq	%r11, %r9
	movslq	%r14d, %r11
	movl	(%rsi,%r11,4), %r11d
	imulq	%rbx, %r11
	addq	%r11, %rcx
	leal	2(%rdi), %r11d
	cmpl	$6, %edi
	je	.L6
	movl	-132(%rbp), %edi
	movl	8(%rdx,%r12), %ebx
	subl	%r11d, %edi
	movslq	%edi, %rdi
	movl	(%rsi,%rdi,4), %r14d
	movl	-128(%rbp,%rdi,4), %edi
	imulq	%r14, %rbx
	subq	%rbx, %rax
	movslq	%r11d, %rbx
	movl	-96(%rbp,%rbx,4), %ebx
	imulq	%rdi, %rbx
	movl	%r15d, %edi
	subl	%r11d, %edi
	movl	40(%rdx,%r12), %r11d
	movslq	%edi, %rdi
	movl	(%rsi,%rdi,4), %edi
	addq	%rbx, %r9
	imulq	%r11, %rdi
	addq	%rdi, %rcx
.L6:
	addq	%r9, %rax
	movq	-160(%rbp), %rbx
	addq	%r9, %rcx
	addq	$4, -152(%rbp)
	movl	%eax, %edi
	addq	$4, -144(%rbp)
	shrq	$28, %rax
	andl	$268435455, %edi
	movl	%edi, -4(%rbx,%r8,4)
	movl	%ecx, %edi
	shrq	$28, %rcx
	andl	$268435455, %edi
	movl	%edi, 28(%rbx,%r8,4)
	addq	$1, %r8
	movl	%r10d, %edi
.L2:
	leal	8(%rdi), %ebx
	movl	%r8d, %r11d
	movl	%ebx, -132(%rbp)
	cmpl	$2, %edi
	jle	.L12
	movq	-152(%rbp), %r15
	movdqu	(%rdx), %xmm0
	movl	%r8d, %r9d
	movq	-144(%rbp), %rbx
	shrl	$2, %r9d
	movdqu	(%r15), %xmm7
	movdqa	%xmm0, %xmm2
	leaq	32(%r15), %r10
	punpckldq	%xmm0, %xmm2
	punpckhdq	%xmm0, %xmm0
	pshufd	$27, %xmm7, %xmm1
	movdqu	(%rbx), %xmm7
	movdqa	%xmm1, %xmm6
	punpckldq	%xmm1, %xmm6
	punpckhdq	%xmm1, %xmm1
	movaps	%xmm7, -176(%rbp)
	pmuludq	%xmm1, %xmm0
	pmuludq	%xmm6, %xmm2
	pshufd	$27, %xmm7, %xmm1
	movdqa	%xmm1, %xmm6
	punpckldq	%xmm1, %xmm6
	punpckhdq	%xmm1, %xmm1
	paddq	%xmm0, %xmm2
	movdqa	-96(%rbp), %xmm0
	movdqa	%xmm0, %xmm7
	punpckldq	%xmm0, %xmm7
	punpckhdq	%xmm0, %xmm0
	pmuludq	%xmm1, %xmm0
	pmuludq	%xmm6, %xmm7
	movdqu	32(%r15), %xmm6
	pshufd	$27, %xmm6, %xmm1
	movdqa	%xmm1, %xmm8
	punpckldq	%xmm1, %xmm8
	punpckhdq	%xmm1, %xmm1
	paddq	%xmm0, %xmm7
	movdqu	32(%rdx), %xmm0
	movdqa	%xmm0, %xmm6
	punpckldq	%xmm0, %xmm6
	punpckhdq	%xmm0, %xmm0
	pmuludq	%xmm8, %xmm6
	pmuludq	%xmm1, %xmm0
	paddq	%xmm6, %xmm0
	cmpl	$2, %r9d
	jne	.L9
	movdqu	-16(%r15), %xmm1
	movdqu	16(%rdx), %xmm6
	movq	-144(%rbp), %rbx
	pshufd	$27, %xmm1, %xmm1
	movdqa	%xmm6, %xmm9
	movdqa	%xmm1, %xmm8
	punpckhdq	%xmm6, %xmm9
	punpckldq	%xmm6, %xmm6
	punpckhdq	%xmm1, %xmm8
	punpckldq	%xmm1, %xmm1
	pmuludq	%xmm9, %xmm8
	pmuludq	%xmm6, %xmm1
	movdqu	-16(%rbx), %xmm6
	movaps	%xmm6, -176(%rbp)
	pshufd	$27, %xmm6, %xmm6
	movdqa	%xmm6, %xmm9
	punpckldq	%xmm6, %xmm9
	punpckhdq	%xmm6, %xmm6
	paddq	%xmm8, %xmm1
	paddq	%xmm1, %xmm2
	movdqa	-80(%rbp), %xmm1
	movdqa	%xmm1, %xmm8
	punpckldq	%xmm1, %xmm8
	punpckhdq	%xmm1, %xmm1
	pmuludq	%xmm9, %xmm8
	pmuludq	%xmm6, %xmm1
	movdqu	-16(%r10), %xmm6
	pshufd	$27, %xmm6, %xmm6
	movdqa	%xmm6, %xmm9
	punpckhdq	%xmm6, %xmm9
	punpckldq	%xmm6, %xmm6
	paddq	%xmm8, %xmm1
	paddq	%xmm1, %xmm7
	movdqu	48(%rdx), %xmm1
	movdqa	%xmm1, %xmm8
	punpckhdq	%xmm1, %xmm8
	punpckldq	%xmm1, %xmm1
	pmuludq	%xmm9, %xmm8
	pmuludq	%xmm6, %xmm1
	paddq	%xmm8, %xmm1
	paddq	%xmm1, %xmm0
.L9:
	movdqa	%xmm0, %xmm1
	movl	%r11d, %r10d
	psrldq	$8, %xmm1
	andl	$-4, %r10d
	paddq	%xmm1, %xmm0
	movq	%xmm0, %r9
	movdqa	%xmm7, %xmm0
	psrldq	$8, %xmm0
	addq	%r9, %rax
	paddq	%xmm7, %xmm0
	movq	%xmm0, %r9
	movdqa	%xmm2, %xmm0
	psrldq	$8, %xmm0
	addq	%r9, %rcx
	andl	$3, %r11d
	paddq	%xmm2, %xmm0
	movq	%xmm0, %r9
	je	.L3
.L7:
	movl	%edi, %ebx
	movslq	%r10d, %r11
	leal	8(%rdi), %r13d
	subl	%r10d, %ebx
	movl	(%rdx,%r11,4), %r15d
	leaq	0(,%r11,4), %r12
	movslq	%ebx, %rbx
	movl	(%rsi,%rbx,4), %r14d
	movl	-128(%rbp,%rbx,4), %ebx
	imulq	%r15, %r14
	addq	%r14, %r9
	movl	-96(%rbp,%r11,4), %r14d
	movl	32(%rdx,%r11,4), %r11d
	imulq	%r14, %rbx
	addq	%rbx, %rcx
	movl	%r13d, %ebx
	subl	%r10d, %ebx
	movslq	%ebx, %rbx
	movl	(%rsi,%rbx,4), %ebx
	imulq	%r11, %rbx
	leal	1(%r10), %r11d
	addq	%rbx, %rax
	cmpl	%r11d, %edi
	jl	.L3
	movl	%edi, %r14d
	movl	4(%rdx,%r12), %r15d
	addl	$2, %r10d
	subl	%r11d, %r14d
	movslq	%r14d, %rbx
	movl	(%rsi,%rbx,4), %r14d
	movl	-128(%rbp,%rbx,4), %ebx
	imulq	%r15, %r14
	addq	%r14, %r9
	movslq	%r11d, %r14
	movl	-96(%rbp,%r14,4), %r14d
	imulq	%r14, %rbx
	addq	%rbx, %rcx
	movl	%r13d, %ebx
	subl	%r11d, %ebx
	movslq	%ebx, %r11
	movl	36(%rdx,%r12), %ebx
	movl	(%rsi,%r11,4), %r11d
	imulq	%rbx, %r11
	addq	%r11, %rax
	cmpl	%r10d, %edi
	jl	.L3
	movl	%edi, %r11d
	movl	8(%rdx,%r12), %ebx
	movl	%r13d, %r15d
	subl	%r10d, %r11d
	subl	%r10d, %r15d
	movslq	%r11d, %r11
	movl	(%rsi,%r11,4), %r14d
	movl	-128(%rbp,%r11,4), %r11d
	imulq	%r14, %rbx
	addq	%rbx, %r9
	movslq	%r10d, %rbx
	movslq	%r15d, %r10
	movl	-96(%rbp,%rbx,4), %ebx
	movl	(%rsi,%r10,4), %r10d
	imulq	%r11, %rbx
	movl	40(%rdx,%r12), %r11d
	imulq	%r11, %r10
	addq	%rbx, %rcx
	addq	%r10, %rax
.L3:
	subq	%r9, %rcx
	addq	%r9, %rax
	leal	1(%rdi), %r10d
	cmpl	$7, %edi
	jne	.L24
	movq	-160(%rbp), %rsi
	movq	%rax, %r15
	andl	$268435455, %eax
	movl	%eax, -4(%rsi,%r8,4)
	movl	%ecx, %eax
	shrq	$28, %rcx
	andl	$268435455, %eax
	movl	%eax, 28(%rsi,%r8,4)
	movq	%r15, %rax
	movl	32(%rsi), %edx
	shrq	$28, %rax
	addq	%rcx, %rax
	addq	%rax, %rdx
	movl	(%rsi), %eax
	addq	%rax, %rcx
	movl	%edx, %eax
	andl	$268435455, %eax
	movl	%eax, 32(%rsi)
	movl	%ecx, %eax
	shrq	$28, %rcx
	andl	$268435455, %eax
	addl	%ecx, 4(%rsi)
	movl	%eax, (%rsi)
	movq	%rdx, %rax
	shrq	$28, %rax
	addl	%eax, 36(%rsi)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L25
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	xorl	%r10d, %r10d
	xorl	%r9d, %r9d
	jmp	.L7
.L11:
	movl	%r10d, %edi
	xorl	%r9d, %r9d
	jmp	.L5
.L25:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE83:
	.size	gf_mul, .-gf_mul
	.p2align 4
	.globl	gf_mulw_unsigned
	.type	gf_mulw_unsigned, @function
gf_mulw_unsigned:
.LFB84:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rsi), %eax
	movl	32(%rsi), %ecx
	movl	4(%rsi), %r12d
	movl	8(%rsi), %r10d
	imulq	%rdx, %rax
	movl	12(%rsi), %r13d
	imulq	%rdx, %rcx
	imulq	%rdx, %r12
	imulq	%rdx, %r10
	movq	%rcx, %r14
	imulq	%rdx, %r13
	movq	%rax, %rcx
	shrq	$28, %rcx
	movq	%r14, %r8
	addq	%rcx, %r12
	movl	36(%rsi), %ecx
	shrq	$28, %r8
	imulq	%rdx, %rcx
	leaq	(%rcx,%r8), %rbx
	movq	%r12, %rcx
	shrq	$28, %rcx
	movq	%rbx, %r8
	movq	%rbx, -56(%rbp)
	addq	%rcx, %r10
	movl	40(%rsi), %ecx
	shrq	$28, %r8
	imulq	%rdx, %rcx
	leaq	(%rcx,%r8), %r15
	movq	%r10, %rcx
	shrq	$28, %rcx
	movq	%r15, %r11
	movq	%r15, -64(%rbp)
	addq	%rcx, %r13
	movl	44(%rsi), %ecx
	shrq	$28, %r11
	movq	%r13, %r9
	imulq	%rdx, %rcx
	shrq	$28, %r9
	leaq	(%rcx,%r11), %r15
	movl	52(%rsi), %r11d
	movq	%r15, %rcx
	shrq	$28, %rcx
	imulq	%rdx, %r11
	movq	%rcx, %r8
	movl	16(%rsi), %ecx
	imulq	%rdx, %rcx
	addq	%rcx, %r9
	movl	48(%rsi), %ecx
	movq	%r9, -72(%rbp)
	imulq	%rdx, %rcx
	leaq	(%rcx,%r8), %rbx
	movq	%r9, %rcx
	movl	24(%rsi), %r9d
	shrq	$28, %rcx
	movq	%rbx, -80(%rbp)
	movq	%rcx, %r8
	imulq	%rdx, %r9
	movq	%rbx, %rcx
	movl	20(%rsi), %ebx
	shrq	$28, %rcx
	imulq	%rdx, %rbx
	addq	%rcx, %r11
	movq	%r11, %rcx
	shrq	$28, %rcx
	addq	%r8, %rbx
	movq	%rbx, %r8
	shrq	$28, %r8
	addq	%r8, %r9
	movl	56(%rsi), %r8d
	imulq	%rdx, %r8
	andl	$268435455, %eax
	andl	$268435455, %r12d
	andl	$268435455, %r13d
	andl	$268435455, %r10d
	movd	%r13d, %xmm2
	movd	%r12d, %xmm3
	andl	$268435455, %ebx
	movd	%eax, %xmm0
	movd	%r10d, %xmm1
	movd	%ebx, %xmm5
	andl	$268435455, %r11d
	addq	%rcx, %r8
	punpckldq	%xmm2, %xmm1
	punpckldq	%xmm3, %xmm0
	movq	%r9, %rcx
	shrq	$28, %rcx
	punpcklqdq	%xmm1, %xmm0
	movd	%r11d, %xmm3
	andl	$268435455, %r9d
	movq	%rcx, -88(%rbp)
	movq	%r8, %rcx
	movd	%xmm0, %eax
	movd	%r9d, %xmm1
	shrq	$28, %rcx
	movl	%r15d, %r9d
	andl	$268435455, %r8d
	movq	%rcx, -96(%rbp)
	movl	28(%rsi), %ecx
	andl	$268435455, %r9d
	movl	60(%rsi), %esi
	movd	%r9d, %xmm6
	imulq	%rdx, %rcx
	addq	-88(%rbp), %rcx
	imulq	%rsi, %rdx
	addq	-96(%rbp), %rdx
	movups	%xmm0, (%rdi)
	movl	-72(%rbp), %esi
	movl	-80(%rbp), %ebx
	andl	$268435455, %esi
	movd	%esi, %xmm0
	movl	%ecx, %esi
	shrq	$28, %rcx
	andl	$268435455, %ebx
	andl	$268435455, %esi
	punpckldq	%xmm5, %xmm0
	movd	%esi, %xmm4
	movl	%r14d, %esi
	movl	-64(%rbp), %r14d
	punpckldq	%xmm4, %xmm1
	andl	$268435455, %esi
	punpcklqdq	%xmm1, %xmm0
	andl	$268435455, %r14d
	movups	%xmm0, 16(%rdi)
	movd	%esi, %xmm0
	movl	-56(%rbp), %esi
	movd	%r14d, %xmm1
	punpckldq	%xmm6, %xmm1
	andl	$268435455, %esi
	movd	%esi, %xmm7
	movl	%edx, %esi
	shrq	$28, %rdx
	punpckldq	%xmm7, %xmm0
	leaq	(%rcx,%rdx), %r15
	andl	$268435455, %esi
	addq	%rax, %rdx
	punpcklqdq	%xmm1, %xmm0
	movd	%esi, %xmm2
	movd	%r8d, %xmm1
	movl	%edx, %eax
	movd	%xmm0, %r9d
	movups	%xmm0, 32(%rdi)
	movd	%ebx, %xmm0
	punpckldq	%xmm2, %xmm1
	movl	%r9d, %ecx
	andl	$268435455, %eax
	punpckldq	%xmm3, %xmm0
	addq	%r15, %rcx
	movl	%eax, (%rdi)
	punpcklqdq	%xmm1, %xmm0
	movl	%ecx, %esi
	shrq	$28, %rcx
	addl	%ecx, 36(%rdi)
	andl	$268435455, %esi
	shrq	$28, %rdx
	addl	%edx, 4(%rdi)
	movl	%esi, 32(%rdi)
	movups	%xmm0, 48(%rdi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE84:
	.size	gf_mulw_unsigned, .-gf_mulw_unsigned
	.p2align 4
	.globl	gf_sqr
	.type	gf_sqr, @function
gf_sqr:
.LFB85:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	jmp	gf_mul
	.cfi_endproc
.LFE85:
	.size	gf_sqr, .-gf_sqr
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
