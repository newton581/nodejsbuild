	.file	"wp_dgst.c"
	.text
	.p2align 4
	.globl	WHIRLPOOL_Init
	.type	WHIRLPOOL_Init, @function
WHIRLPOOL_Init:
.LFB151:
	.cfi_startproc
	endbr64
	movq	$0, (%rdi)
	movq	%rdi, %rcx
	leaq	8(%rdi), %rdi
	xorl	%eax, %eax
	movq	$0, 152(%rdi)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$168, %ecx
	shrl	$3, %ecx
	rep stosq
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE151:
	.size	WHIRLPOOL_Init, .-WHIRLPOOL_Init
	.p2align 4
	.globl	WHIRLPOOL_BitUpdate
	.type	WHIRLPOOL_BitUpdate, @function
WHIRLPOOL_BitUpdate:
.LFB153:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %eax
	movq	%rdx, %rcx
	movq	%rdx, %r8
	negl	%eax
	movl	%eax, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	andl	$7, %r9d
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	%edx, %esi
	xorl	%edx, %edx
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movl	128(%rdi), %r12d
	movl	%r12d, %r15d
	andl	$7, %r15d
	addq	136(%rdi), %rcx
	setc	%dl
	orl	%r12d, %eax
	movq	%rcx, 136(%rdi)
	andl	$7, %eax
	testq	%rdx, %rdx
	je	.L6
	addq	$1, 144(%rdi)
	jne	.L7
	addq	$1, 152(%rdi)
	jne	.L7
	addq	$1, 160(%rdi)
.L7:
	testl	%eax, %eax
	je	.L20
.L36:
	movl	%r12d, %eax
	movzbl	(%r14), %edx
	shrl	$3, %eax
	cmpl	%r9d, %r15d
	je	.L10
	cmpq	$8, %r8
	jbe	.L12
	movl	$8, %edx
	movl	%edx, %edi
	subl	%r9d, %edi
	movl	%edi, -56(%rbp)
	testl	%r15d, %r15d
	jne	.L22
	leaq	-9(%r8), %r10
	movq	%r14, %r13
	leaq	64(%rbx), %rsi
	shrq	$3, %r10
	leaq	1(%r14,%r10), %r11
	.p2align 4,,10
	.p2align 3
.L26:
	movzbl	1(%r13), %edx
	movzbl	-56(%rbp), %ecx
	addl	$8, %r12d
	addq	$1, %r13
	movzbl	-1(%r13), %edi
	sarl	%cl, %edx
	movl	%r9d, %ecx
	sall	%cl, %edi
	orl	%edi, %edx
	movb	%dl, 64(%rbx,%rax)
	cmpl	$511, %r12d
	ja	.L52
	movl	%r12d, %eax
	movl	%r12d, 128(%rbx)
	shrl	$3, %eax
	cmpq	%r13, %r11
	jne	.L26
.L25:
	addq	%r10, %r14
	negq	%r10
	leaq	-8(%r8,%r10,8), %rsi
.L31:
	movzbl	1(%r14), %edx
.L12:
	movl	%r9d, %ecx
	addl	%esi, %r12d
	sall	%cl, %edx
	testl	%r15d, %r15d
	je	.L32
	movzbl	%dl, %r13d
	movl	%r15d, %ecx
	leal	1(%rax), %esi
	movl	%r13d, %edx
	sarl	%cl, %edx
	orb	%dl, 64(%rbx,%rax)
	cmpl	$512, %r12d
	je	.L53
.L34:
	movl	$8, %ecx
	movl	%r13d, %edx
	subl	%r15d, %ecx
	sall	%cl, %edx
	movb	%dl, 64(%rbx,%rsi)
.L51:
	movl	%r12d, 128(%rbx)
.L3:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movb	%dl, 64(%rbx,%rax)
	cmpl	$512, %r12d
	jne	.L51
	leaq	64(%rbx), %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	xorl	%r12d, %r12d
	call	whirlpool_block@PLT
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L52:
	movl	$1, %edx
	movq	%rbx, %rdi
	andl	$511, %r12d
	movq	%r8, -96(%rbp)
	movl	%r9d, -88(%rbp)
	movq	%r10, -80(%rbp)
	movq	%r11, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	whirlpool_block@PLT
	movq	-72(%rbp), %r11
	movl	%r12d, %eax
	movq	-64(%rbp), %rsi
	shrl	$3, %eax
	movl	%r12d, 128(%rbx)
	movq	-80(%rbp), %r10
	cmpq	%r13, %r11
	movl	-88(%rbp), %r9d
	movq	-96(%rbp), %r8
	jne	.L26
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L10:
	movl	$255, %esi
	movl	%r15d, %ecx
	addq	$1, %r14
	sarl	%cl, %esi
	andl	%esi, %edx
	orb	%dl, 64(%rbx,%rax)
	movl	$8, %eax
	subl	%r15d, %eax
	addl	%eax, %r12d
	subq	%rax, %r8
	cmpl	$512, %r12d
	je	.L54
	movl	%r12d, 128(%rbx)
.L8:
	testq	%r8, %r8
	je	.L3
.L20:
	testl	%r12d, %r12d
	je	.L18
	movl	$512, %eax
	movl	%r12d, %edi
	leaq	64(%rbx), %r13
	subl	%r12d, %eax
	shrl	$3, %edi
	movl	%eax, %edx
	addq	%r13, %rdi
	cmpq	%r8, %rdx
	ja	.L55
	shrl	$3, %eax
	subq	%rdx, %r8
	movq	%r14, %rsi
	movl	%eax, %r12d
	movq	%r8, -56(%rbp)
	movq	%r12, %rdx
	addq	%r12, %r14
	call	memcpy@PLT
	movl	$1, %edx
	movq	%r13, %rsi
.L50:
	movq	%rbx, %rdi
	call	whirlpool_block@PLT
	movq	-56(%rbp), %r8
	movl	$0, 128(%rbx)
	testq	%r8, %r8
	je	.L3
.L18:
	movabsq	$2305843009213693888, %r12
	movq	%r8, %r15
.L15:
	movq	%r15, %r13
	shrq	$3, %r13
	cmpq	$511, %r15
	ja	.L56
	movq	%r15, %r8
	leaq	64(%rbx), %rdi
	xorl	%r12d, %r12d
.L17:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r8, -56(%rbp)
	call	memcpy@PLT
	movq	-56(%rbp), %r8
	addl	%r8d, %r12d
	movl	%r12d, 128(%rbx)
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	leaq	64(%rbx), %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	xorl	%r12d, %r12d
	call	whirlpool_block@PLT
	xorl	%esi, %esi
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L22:
	leaq	-9(%r8), %r11
	subl	%r15d, %edx
	leaq	64(%rbx), %rsi
	movq	%r14, %r10
	shrq	$3, %r11
	movl	%edx, -72(%rbp)
	leaq	1(%r14,%r11), %rdi
	movq	%rdi, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L30:
	movzbl	(%r10), %r13d
	movl	%r9d, %ecx
	movzbl	1(%r10), %edx
	addl	$8, %r12d
	addq	$1, %r10
	sall	%cl, %r13d
	movzbl	-56(%rbp), %ecx
	sarl	%cl, %edx
	movl	%r15d, %ecx
	orl	%edx, %r13d
	movl	%eax, %edx
	movzbl	%r13b, %r13d
	movl	%r13d, %edi
	sarl	%cl, %edi
	orb	%dil, 64(%rbx,%rdx)
	cmpl	$511, %r12d
	ja	.L27
	movzbl	-72(%rbp), %ecx
	addl	$1, %eax
	sall	%cl, %r13d
	movb	%r13b, 64(%rbx,%rax)
	movl	%r12d, %eax
	movl	%r12d, 128(%rbx)
	shrl	$3, %eax
	cmpq	%r10, -64(%rbp)
	jne	.L30
.L29:
	addq	%r11, %r14
	negq	%r11
	leaq	-8(%r8,%r11,8), %rsi
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L27:
	movl	$1, %edx
	movq	%rbx, %rdi
	andl	$511, %r12d
	movq	%r8, -112(%rbp)
	movq	%r10, -104(%rbp)
	movl	%r9d, -96(%rbp)
	movq	%r11, -88(%rbp)
	movq	%rsi, -80(%rbp)
	call	whirlpool_block@PLT
	movzbl	-72(%rbp), %ecx
	movl	%r12d, %eax
	movq	-104(%rbp), %r10
	shrl	$3, %eax
	movl	%r12d, 128(%rbx)
	movq	-80(%rbp), %rsi
	sall	%cl, %r13d
	cmpq	%r10, -64(%rbp)
	movq	-88(%rbp), %r11
	movl	-96(%rbp), %r9d
	movq	-112(%rbp), %r8
	movb	%r13b, 64(%rbx)
	jne	.L30
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L6:
	testl	%eax, %eax
	je	.L8
	testq	%r8, %r8
	jne	.L36
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L54:
	movq	%r8, -56(%rbp)
	leaq	64(%rbx), %rsi
	movl	$1, %edx
	jmp	.L50
.L56:
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	andq	%r12, %r13
	shrq	$9, %rdx
	addq	%r13, %r14
	call	whirlpool_block@PLT
	andl	$511, %r15d
	jne	.L15
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L55:
	movq	%r8, %r13
	shrq	$3, %r13
	jmp	.L17
	.cfi_endproc
.LFE153:
	.size	WHIRLPOOL_BitUpdate, .-WHIRLPOOL_BitUpdate
	.p2align 4
	.globl	WHIRLPOOL_Update
	.type	WHIRLPOOL_Update, @function
WHIRLPOOL_Update:
.LFB152:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -56(%rbp)
	cmpq	%rax, %rdx
	jbe	.L58
	movabsq	$-1152921504606846976, %r14
	movabsq	$1152921504606846976, %r12
	leaq	(%rdx,%r14), %rax
	movq	%rsi, %r14
	movq	%rax, %rbx
	movq	%rax, -64(%rbp)
	shrq	$60, %rbx
	addq	$1, %rbx
	salq	$60, %rbx
	addq	%rsi, %rbx
	.p2align 4,,10
	.p2align 3
.L59:
	movabsq	$-9223372036854775808, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	addq	%r12, %r14
	call	WHIRLPOOL_BitUpdate
	cmpq	%rbx, %r14
	jne	.L59
	movabsq	$1152921504606846975, %rax
	movq	-64(%rbp), %r14
	andq	%rax, %r13
	movabsq	$-1152921504606846976, %rax
	andq	%rax, %r14
	addq	%r14, %r12
	addq	%r12, -56(%rbp)
.L58:
	testq	%r13, %r13
	jne	.L66
.L60:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movq	-56(%rbp), %rsi
	leaq	0(,%r13,8), %rdx
	movq	%r15, %rdi
	call	WHIRLPOOL_BitUpdate
	jmp	.L60
	.cfi_endproc
.LFE152:
	.size	WHIRLPOOL_Update, .-WHIRLPOOL_Update
	.p2align 4
	.globl	WHIRLPOOL_Final
	.type	WHIRLPOOL_Final, @function
WHIRLPOOL_Final:
.LFB154:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-128, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	128(%rsi), %ecx
	movl	%ecx, %eax
	shrl	$3, %eax
	andl	$7, %ecx
	movl	%eax, %esi
	je	.L68
	movl	$128, %edx
	sarl	%cl, %edx
	orb	64(%r12,%rsi), %dl
.L68:
	addl	$1, %eax
	movb	%dl, 64(%r12,%rsi)
	leaq	64(%r12), %r13
	cmpl	$32, %eax
	jbe	.L69
	cmpl	$63, %eax
	jbe	.L109
.L70:
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	whirlpool_block@PLT
	movl	$64, %eax
	movl	$32, %ecx
	xorl	%edi, %edi
	addq	%r12, %rax
	movl	%ecx, %edx
	cmpl	$8, %ecx
	jb	.L110
.L79:
	leaq	8(%rax), %rsi
	movq	$0, (%rax)
	movq	$0, -8(%rcx,%rax)
	andq	$-8, %rsi
	subq	%rsi, %rax
	leal	(%rcx,%rax), %edx
	andl	$-8, %edx
	cmpl	$8, %edx
	jb	.L78
	movl	%edx, %eax
	xorl	%edx, %edx
	andl	$-8, %eax
.L83:
	movl	%edx, %ecx
	addl	$8, %edx
	movq	%rdi, (%rsi,%rcx)
	cmpl	%eax, %edx
	jb	.L83
.L78:
	movq	136(%r12), %rax
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	bswap	%rax
	movq	%rax, 120(%r12)
	movq	144(%r12), %rax
	bswap	%rax
	movq	%rax, 112(%r12)
	movq	152(%r12), %rax
	bswap	%rax
	movq	%rax, 104(%r12)
	movq	160(%r12), %rax
	bswap	%rax
	movq	%rax, 96(%r12)
	call	whirlpool_block@PLT
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L67
	movdqu	(%r12), %xmm0
	movl	$168, %esi
	movq	%r12, %rdi
	movups	%xmm0, (%rbx)
	movdqu	16(%r12), %xmm1
	movups	%xmm1, 16(%rbx)
	movdqu	32(%r12), %xmm2
	movups	%xmm2, 32(%rbx)
	movdqu	48(%r12), %xmm3
	movups	%xmm3, 48(%rbx)
	call	OPENSSL_cleanse@PLT
	movl	$1, %eax
.L67:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	je	.L78
	movl	$32, %ecx
	xorl	%edi, %edi
	subl	%eax, %ecx
	addq	$64, %rax
	addq	%r12, %rax
	movl	%ecx, %edx
	cmpl	$8, %ecx
	jnb	.L79
.L110:
	andl	$4, %ecx
	jne	.L111
	testl	%edx, %edx
	je	.L78
	movb	$0, (%rax)
	testb	$2, %dl
	je	.L78
	xorl	%ecx, %ecx
	movw	%cx, -2(%rax,%rdx)
	jmp	.L78
.L109:
	movl	$64, %edx
	leaq	64(%r12,%rax), %rcx
	xorl	%edi, %edi
	subl	%eax, %edx
	cmpl	$8, %edx
	jnb	.L71
	testb	$4, %dl
	jne	.L112
	testl	%edx, %edx
	je	.L70
	movb	$0, (%rcx)
	testb	$2, %dl
	je	.L70
	movl	%edx, %eax
	xorl	%esi, %esi
	movw	%si, -2(%rcx,%rax)
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L71:
	leaq	8(%rcx), %rsi
	movl	%edx, %eax
	movq	$0, (%rcx)
	movq	$0, -8(%rcx,%rax)
	andq	$-8, %rsi
	subq	%rsi, %rcx
	addl	%ecx, %edx
	andl	$-8, %edx
	cmpl	$8, %edx
	jb	.L70
	andl	$-8, %edx
	xorl	%eax, %eax
.L75:
	movl	%eax, %ecx
	addl	$8, %eax
	movq	%rdi, (%rsi,%rcx)
	cmpl	%edx, %eax
	jb	.L75
	jmp	.L70
.L111:
	movl	$0, (%rax)
	movl	$0, -4(%rax,%rdx)
	jmp	.L78
.L112:
	movl	%edx, %eax
	movl	$0, (%rcx)
	movl	$0, -4(%rcx,%rax)
	jmp	.L70
	.cfi_endproc
.LFE154:
	.size	WHIRLPOOL_Final, .-WHIRLPOOL_Final
	.p2align 4
	.globl	WHIRLPOOL
	.type	WHIRLPOOL, @function
WHIRLPOOL:
.LFB155:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$21, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	leaq	-224(%rbp), %r8
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movq	%r8, %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	leaq	m.4995(%rip), %rax
	cmove	%rax, %r13
	xorl	%eax, %eax
	rep stosq
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rsi
	jbe	.L115
	movabsq	$-1152921504606846976, %r12
	addq	%rsi, %r12
	movq	%r12, %r14
	shrq	$60, %r14
	addq	$1, %r14
	salq	$60, %r14
	leaq	(%r15,%r14), %rax
	movabsq	$1152921504606846976, %r14
	movq	%rax, -248(%rbp)
	movq	%r15, %rax
	.p2align 4,,10
	.p2align 3
.L116:
	movq	%rax, %rsi
	movq	%r8, %rdi
	movabsq	$-9223372036854775808, %rdx
	movq	%rax, -240(%rbp)
	movq	%r8, -232(%rbp)
	call	WHIRLPOOL_BitUpdate
	movq	-240(%rbp), %rax
	movq	-232(%rbp), %r8
	addq	%r14, %rax
	cmpq	-248(%rbp), %rax
	jne	.L116
	movabsq	$1152921504606846975, %rax
	andq	%rax, %rbx
	movabsq	$-1152921504606846976, %rax
	andq	%rax, %r12
	addq	%r14, %r12
	addq	%r12, %r15
.L115:
	testq	%rbx, %rbx
	jne	.L124
.L117:
	movq	%r8, %rsi
	movq	%r13, %rdi
	call	WHIRLPOOL_Final
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L125
	addq	$216, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	movq	%r8, %rdi
	leaq	0(,%rbx,8), %rdx
	movq	%r15, %rsi
	movq	%r8, -232(%rbp)
	call	WHIRLPOOL_BitUpdate
	movq	-232(%rbp), %r8
	jmp	.L117
.L125:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE155:
	.size	WHIRLPOOL, .-WHIRLPOOL
	.local	m.4995
	.comm	m.4995,64,32
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
