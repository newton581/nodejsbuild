	.file	"e_rc4_hmac_md5.c"
	.text
	.p2align 4
	.type	rc4_hmac_md5_cipher, @function
rc4_hmac_md5_cipher:
.LFB370:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	$64, %r12d
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -88(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -104(%rbp)
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	-104(%rbp), %rdi
	movl	(%rax), %r15d
	movq	1312(%rax), %r14
	movq	%rax, %rbx
	subl	1304(%rax), %r12d
	notl	%r15d
	andl	$31, %r15d
	cmpq	$-1, %r14
	je	.L2
	leaq	16(%r14), %rdx
	xorl	%eax, %eax
	cmpq	%r13, %rdx
	je	.L40
.L1:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L41
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	call	EVP_CIPHER_CTX_encrypting@PLT
	testl	%eax, %eax
	jne	.L21
.L22:
	leaq	128(%r15), %r11
	leaq	64(%r15), %rax
	cmpq	%r12, %r15
	cmovnb	%rax, %r11
	leaq	1216(%rbx), %r15
	cmpq	%r13, %r11
	jnb	.L30
	movq	%r13, %r10
	subq	%r11, %r10
	cmpq	$63, %r10
	jbe	.L30
	testb	$16, 2+OPENSSL_ia32cap_P(%rip)
	je	.L42
	.p2align 4,,10
	.p2align 3
.L30:
	movq	-88(%rbp), %rcx
	movq	$0, -104(%rbp)
	movq	%r13, %rsi
	movq	%rcx, %r12
.L14:
	movq	-96(%rbp), %rdx
	movq	%rbx, %rdi
	call	RC4@PLT
	cmpq	$-1, %r14
	je	.L19
	movq	%r12, %rsi
	movq	%r15, %rdi
	leaq	-80(%rbp), %r12
	movq	%r14, %rdx
	subq	-104(%rbp), %rdx
	call	MD5_Update@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	MD5_Final@PLT
	movq	1204(%rbx), %rax
	movq	%r12, %rsi
	movdqu	1124(%rbx), %xmm5
	movdqu	1140(%rbx), %xmm6
	movdqu	1156(%rbx), %xmm7
	movl	$16, %edx
	movq	%r15, %rdi
	movq	%rax, 1296(%rbx)
	movl	1212(%rbx), %eax
	movups	%xmm5, 1216(%rbx)
	movdqu	1172(%rbx), %xmm5
	movups	%xmm6, 1232(%rbx)
	movdqu	1188(%rbx), %xmm6
	movl	%eax, 1304(%rbx)
	movups	%xmm7, 1248(%rbx)
	movups	%xmm5, 1264(%rbx)
	movups	%xmm6, 1280(%rbx)
	call	MD5_Update@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	MD5_Final@PLT
	movq	-88(%rbp), %rdi
	movl	$16, %edx
	movq	%r12, %rsi
	addq	%r14, %rdi
	call	CRYPTO_memcmp@PLT
	testl	%eax, %eax
	je	.L11
	xorl	%eax, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L2:
	call	EVP_CIPHER_CTX_encrypting@PLT
	testl	%eax, %eax
	je	.L22
	movq	%r13, %r14
.L21:
	leaq	64(%r12), %rax
	cmpq	%r12, %r15
	leaq	1216(%rbx), %r11
	cmova	%rax, %r12
	cmpq	%r14, %r12
	jb	.L43
.L27:
	movq	-88(%rbp), %r12
	movq	-96(%rbp), %rsi
	movq	%r13, %r15
	movq	%r14, %rdx
	xorl	%ecx, %ecx
.L6:
	movq	%r11, %rdi
	movq	%rcx, -104(%rbp)
	movq	%r11, -112(%rbp)
	call	MD5_Update@PLT
	cmpq	%r13, %r14
	movq	-104(%rbp), %rcx
	je	.L9
	movq	-96(%rbp), %rax
	movq	-88(%rbp), %rsi
	movq	-112(%rbp), %r11
	cmpq	%rsi, %rax
	je	.L10
	addq	%rcx, %rax
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%r11, -96(%rbp)
	subq	%rcx, %rdx
	movq	%rax, %rsi
	call	memcpy@PLT
	movq	-96(%rbp), %r11
.L10:
	addq	-88(%rbp), %r14
	movq	%r11, %rsi
	movq	%r11, -88(%rbp)
	movq	%r14, %rdi
	call	MD5_Final@PLT
	movq	1204(%rbx), %rax
	movq	%r14, %rsi
	movdqu	1124(%rbx), %xmm0
	movdqu	1140(%rbx), %xmm1
	movdqu	1156(%rbx), %xmm2
	movl	$16, %edx
	movq	%rax, 1296(%rbx)
	movl	1212(%rbx), %eax
	movdqu	1172(%rbx), %xmm3
	movdqu	1188(%rbx), %xmm4
	movups	%xmm0, 1216(%rbx)
	movq	-88(%rbp), %r11
	movl	%eax, 1304(%rbx)
	movups	%xmm1, 1232(%rbx)
	movq	%r11, %rdi
	movups	%xmm2, 1248(%rbx)
	movups	%xmm3, 1264(%rbx)
	movups	%xmm4, 1280(%rbx)
	call	MD5_Update@PLT
	movq	-88(%rbp), %r11
	movq	%r14, %rdi
	movq	%r11, %rsi
	call	MD5_Final@PLT
	movq	%r12, %rcx
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	RC4@PLT
.L11:
	movq	$-1, 1312(%rbx)
	movl	$1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L43:
	movq	%r14, %r10
	subq	%r12, %r10
	cmpq	$63, %r10
	jbe	.L27
	testb	$16, 2+OPENSSL_ia32cap_P(%rip)
	jne	.L27
	movq	-96(%rbp), %rsi
	movq	%r11, %rdi
	movq	%r12, %rdx
	movq	%r10, -112(%rbp)
	movq	%r11, -104(%rbp)
	call	MD5_Update@PLT
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	RC4@PLT
	movq	-112(%rbp), %r10
	movq	-96(%rbp), %rax
	movq	%rbx, %rdi
	movq	-88(%rbp), %rcx
	movq	-104(%rbp), %r11
	movq	%r10, %r9
	leaq	(%rax,%r15), %rsi
	leaq	(%rax,%r12), %r8
	leaq	(%rcx,%r15), %rdx
	shrq	$6, %r9
	movq	%r11, %rcx
	call	rc4_md5_enc@PLT
	movq	-112(%rbp), %r10
	xorl	%edi, %edi
	movq	-104(%rbp), %r11
	movl	1236(%rbx), %r8d
	andq	$-64, %r10
	movq	%r10, %rdx
	movq	%r10, %rax
	leaq	(%r15,%r10), %rcx
	addq	%r10, %r12
	shrq	$29, %rdx
	salq	$3, %rax
	movq	%r13, %r15
	addl	%edx, %r8d
	addl	1232(%rbx), %eax
	movq	%r14, %rdx
	movl	%eax, 1232(%rbx)
	movq	-96(%rbp), %rax
	setc	%dil
	subq	%r12, %rdx
	subq	%rcx, %r15
	leaq	(%rax,%r12), %rsi
	movq	-88(%rbp), %rax
	leaq	(%rax,%rcx), %r12
	leal	(%r8,%rdi), %eax
	movl	%eax, 1236(%rbx)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L19:
	movq	%r13, %rdx
	movq	%r12, %rsi
	subq	-104(%rbp), %rdx
	movq	%r15, %rdi
	call	MD5_Update@PLT
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L9:
	movq	-96(%rbp), %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	addq	%rcx, %rdx
	movq	%r12, %rcx
	call	RC4@PLT
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L42:
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %rdx
	movq	%r11, %rsi
	movq	%rbx, %rdi
	movq	%r10, -120(%rbp)
	movq	%r11, -104(%rbp)
	call	RC4@PLT
	movq	-88(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	MD5_Update@PLT
	movq	-120(%rbp), %r10
	movq	-104(%rbp), %r11
	movq	%rbx, %rdi
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %rcx
	movq	%r10, %r9
	movq	%r11, -112(%rbp)
	leaq	(%rax,%r11), %rdx
	leaq	(%rcx,%r11), %rsi
	shrq	$6, %r9
	movq	%r15, %rcx
	leaq	(%rax,%r12), %r8
	movq	%r10, -104(%rbp)
	call	rc4_md5_enc@PLT
	movq	-104(%rbp), %r10
	movq	-112(%rbp), %r11
	movq	%r10, %rax
	andq	$-64, %rax
	leaq	(%r12,%rax), %rcx
	addq	%rax, %r11
	leal	0(,%rax,8), %edx
	addl	1232(%rbx), %edx
	movq	%rcx, -104(%rbp)
	movl	1236(%rbx), %ecx
	jc	.L16
.L18:
	shrq	$29, %rax
	movq	%r13, %rsi
	addq	%r11, -96(%rbp)
	addl	%ecx, %eax
	movl	%edx, 1232(%rbx)
	subq	%r11, %rsi
	movl	%eax, 1236(%rbx)
	movq	-88(%rbp), %rax
	leaq	(%rax,%r11), %rcx
	addq	-104(%rbp), %rax
	movq	%rax, %r12
	jmp	.L14
.L41:
	call	__stack_chk_fail@PLT
.L16:
	addl	$1, %ecx
	jmp	.L18
	.cfi_endproc
.LFE370:
	.size	rc4_hmac_md5_cipher, .-rc4_hmac_md5_cipher
	.p2align 4
	.type	rc4_hmac_md5_init_key, @function
rc4_hmac_md5_init_key:
.LFB369:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movl	%eax, %esi
	call	RC4_set_key@PLT
	leaq	1032(%rbx), %rdi
	call	MD5_Init@PLT
	movl	1120(%rbx), %eax
	movdqu	1032(%rbx), %xmm4
	movq	$-1, 1312(%rbx)
	movdqu	1048(%rbx), %xmm3
	movdqu	1064(%rbx), %xmm2
	movdqu	1080(%rbx), %xmm1
	movdqu	1096(%rbx), %xmm0
	movl	%eax, 1212(%rbx)
	movq	1112(%rbx), %rdx
	movl	%eax, 1304(%rbx)
	movl	$1, %eax
	movups	%xmm4, 1124(%rbx)
	movq	%rdx, 1204(%rbx)
	movq	%rdx, 1296(%rbx)
	movups	%xmm3, 1140(%rbx)
	movups	%xmm2, 1156(%rbx)
	movups	%xmm1, 1172(%rbx)
	movups	%xmm0, 1188(%rbx)
	movups	%xmm4, 1216(%rbx)
	movups	%xmm3, 1232(%rbx)
	movups	%xmm2, 1248(%rbx)
	movups	%xmm1, 1264(%rbx)
	movups	%xmm0, 1280(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE369:
	.size	rc4_hmac_md5_init_key, .-rc4_hmac_md5_init_key
	.p2align 4
	.type	rc4_hmac_md5_ctrl, @function
rc4_hmac_md5_ctrl:
.LFB371:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%rax, %rbx
	cmpl	$22, %r12d
	je	.L47
	movl	$-1, %eax
	cmpl	$23, %r12d
	jne	.L46
	pxor	%xmm0, %xmm0
	leaq	-128(%rbp), %r12
	leaq	1032(%rbx), %r15
	movslq	%r14d, %rdx
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	cmpl	$64, %r14d
	jg	.L57
	movl	$64, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	__memcpy_chk@PLT
.L50:
	movdqa	.LC0(%rip), %xmm0
	movdqa	-128(%rbp), %xmm1
	movq	%r15, %rdi
	leaq	1124(%rbx), %r13
	pxor	%xmm0, %xmm1
	movaps	%xmm1, -128(%rbp)
	movdqa	-112(%rbp), %xmm1
	pxor	%xmm0, %xmm1
	movaps	%xmm1, -112(%rbp)
	movdqa	-96(%rbp), %xmm1
	pxor	%xmm0, %xmm1
	pxor	-80(%rbp), %xmm0
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	MD5_Init@PLT
	movl	$64, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	MD5_Update@PLT
	movdqa	.LC1(%rip), %xmm0
	movdqa	-128(%rbp), %xmm1
	movq	%r13, %rdi
	pxor	%xmm0, %xmm1
	movaps	%xmm1, -128(%rbp)
	movdqa	-112(%rbp), %xmm1
	pxor	%xmm0, %xmm1
	movaps	%xmm1, -112(%rbp)
	movdqa	-96(%rbp), %xmm1
	pxor	%xmm0, %xmm1
	pxor	-80(%rbp), %xmm0
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	MD5_Init@PLT
	movl	$64, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	MD5_Update@PLT
	movl	$64, %esi
	movq	%r12, %rdi
	call	OPENSSL_cleanse@PLT
	movl	$1, %eax
.L46:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L58
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	cmpl	$13, %r14d
	jne	.L53
	movzwl	11(%r13), %r12d
	movq	%r15, %rdi
	rolw	$8, %r12w
	movzwl	%r12w, %r14d
	call	EVP_CIPHER_CTX_encrypting@PLT
	testl	%eax, %eax
	jne	.L52
	cmpw	$15, %r12w
	jbe	.L53
	subl	$16, %r14d
	movl	%r14d, %eax
	rolw	$8, %ax
	movw	%ax, 11(%r13)
.L52:
	movl	%r14d, %eax
	movdqu	1032(%rbx), %xmm2
	movdqu	1048(%rbx), %xmm3
	leaq	1216(%rbx), %rdi
	movq	%rax, 1312(%rbx)
	movq	1112(%rbx), %rax
	movl	$13, %edx
	movq	%r13, %rsi
	movdqu	1064(%rbx), %xmm4
	movdqu	1080(%rbx), %xmm5
	movups	%xmm2, 1216(%rbx)
	movq	%rax, 1296(%rbx)
	movl	1120(%rbx), %eax
	movdqu	1096(%rbx), %xmm6
	movups	%xmm3, 1232(%rbx)
	movl	%eax, 1304(%rbx)
	movups	%xmm4, 1248(%rbx)
	movups	%xmm5, 1264(%rbx)
	movups	%xmm6, 1280(%rbx)
	call	MD5_Update@PLT
	movl	$16, %eax
	jmp	.L46
.L57:
	movq	%r15, %rdi
	movq	%rdx, -136(%rbp)
	call	MD5_Init@PLT
	movq	-136(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	MD5_Update@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	MD5_Final@PLT
	jmp	.L50
.L53:
	movl	$-1, %eax
	jmp	.L46
.L58:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE371:
	.size	rc4_hmac_md5_ctrl, .-rc4_hmac_md5_ctrl
	.p2align 4
	.globl	EVP_rc4_hmac_md5
	.type	EVP_rc4_hmac_md5, @function
EVP_rc4_hmac_md5:
.LFB372:
	.cfi_startproc
	endbr64
	leaq	r4_hmac_md5_cipher(%rip), %rax
	ret
	.cfi_endproc
.LFE372:
	.size	EVP_rc4_hmac_md5, .-EVP_rc4_hmac_md5
	.section	.data.rel.local,"aw"
	.align 32
	.type	r4_hmac_md5_cipher, @object
	.size	r4_hmac_md5_cipher, 88
r4_hmac_md5_cipher:
	.long	915
	.long	1
	.long	16
	.long	0
	.quad	2097160
	.quad	rc4_hmac_md5_init_key
	.quad	rc4_hmac_md5_cipher
	.quad	0
	.long	1320
	.zero	4
	.quad	0
	.quad	0
	.quad	rc4_hmac_md5_ctrl
	.quad	0
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.align 16
.LC1:
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
