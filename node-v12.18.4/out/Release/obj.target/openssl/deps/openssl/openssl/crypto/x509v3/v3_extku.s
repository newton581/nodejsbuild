	.file	"v3_extku.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/x509v3/v3_extku.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	",value:"
.LC2:
	.string	",name:"
.LC3:
	.string	"section:"
	.text
	.p2align 4
	.type	v2i_EXTENDED_KEY_USAGE, @function
v2i_EXTENDED_KEY_USAGE:
.LFB1325:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	call	OPENSSL_sk_num@PLT
	xorl	%edi, %edi
	movl	%eax, %esi
	movl	%eax, %r13d
	call	OPENSSL_sk_new_reserve@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L2
	xorl	%ebx, %ebx
	testl	%r13d, %r13d
	jg	.L3
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	xorl	%esi, %esi
	call	OBJ_txt2obj@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L12
.L6:
	movq	%r12, %rdi
	addl	$1, %ebx
	call	OPENSSL_sk_push@PLT
	cmpl	%r13d, %ebx
	je	.L1
.L3:
	movq	%r14, %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	16(%rax), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	jne	.L5
	movq	8(%rax), %rdi
	xorl	%esi, %esi
	call	OBJ_txt2obj@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L6
.L12:
	movq	ASN1_OBJECT_free@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	OPENSSL_sk_pop_free@PLT
	movl	$95, %r8d
	movl	$110, %edx
	leaq	.LC0(%rip), %rcx
	movl	$103, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	subq	$8, %rsp
	movq	8(%r15), %r8
	xorl	%eax, %eax
	pushq	16(%r15)
	movq	(%r15), %rdx
	leaq	.LC1(%rip), %r9
	leaq	.LC2(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	movl	$6, %edi
	call	ERR_add_error_data@PLT
	popq	%rax
	popq	%rdx
.L1:
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2:
	.cfi_restore_state
	movl	$82, %r8d
	movl	$65, %edx
	movl	$103, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%edi, %edi
	call	OPENSSL_sk_free@PLT
	jmp	.L1
	.cfi_endproc
.LFE1325:
	.size	v2i_EXTENDED_KEY_USAGE, .-v2i_EXTENDED_KEY_USAGE
	.p2align 4
	.type	i2v_EXTENDED_KEY_USAGE, @function
i2v_EXTENDED_KEY_USAGE:
.LFB1324:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-128(%rbp), %r13
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	xorl	%ebx, %ebx
	subq	$120, %rsp
	movq	%rdx, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L15:
	movl	%ebx, %esi
	movq	%r12, %rdi
	addl	$1, %ebx
	call	OPENSSL_sk_value@PLT
	movl	$80, %esi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	i2t_ASN1_OBJECT@PLT
	leaq	-136(%rbp), %rdx
	movq	%r13, %rsi
	xorl	%edi, %edi
	call	X509V3_add_value@PLT
.L14:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L15
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	movq	-136(%rbp), %rax
	jne	.L18
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L18:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1324:
	.size	i2v_EXTENDED_KEY_USAGE, .-i2v_EXTENDED_KEY_USAGE
	.p2align 4
	.globl	d2i_EXTENDED_KEY_USAGE
	.type	d2i_EXTENDED_KEY_USAGE, @function
d2i_EXTENDED_KEY_USAGE:
.LFB1320:
	.cfi_startproc
	endbr64
	leaq	EXTENDED_KEY_USAGE_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1320:
	.size	d2i_EXTENDED_KEY_USAGE, .-d2i_EXTENDED_KEY_USAGE
	.p2align 4
	.globl	i2d_EXTENDED_KEY_USAGE
	.type	i2d_EXTENDED_KEY_USAGE, @function
i2d_EXTENDED_KEY_USAGE:
.LFB1321:
	.cfi_startproc
	endbr64
	leaq	EXTENDED_KEY_USAGE_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1321:
	.size	i2d_EXTENDED_KEY_USAGE, .-i2d_EXTENDED_KEY_USAGE
	.p2align 4
	.globl	EXTENDED_KEY_USAGE_new
	.type	EXTENDED_KEY_USAGE_new, @function
EXTENDED_KEY_USAGE_new:
.LFB1322:
	.cfi_startproc
	endbr64
	leaq	EXTENDED_KEY_USAGE_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1322:
	.size	EXTENDED_KEY_USAGE_new, .-EXTENDED_KEY_USAGE_new
	.p2align 4
	.globl	EXTENDED_KEY_USAGE_free
	.type	EXTENDED_KEY_USAGE_free, @function
EXTENDED_KEY_USAGE_free:
.LFB1323:
	.cfi_startproc
	endbr64
	leaq	EXTENDED_KEY_USAGE_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1323:
	.size	EXTENDED_KEY_USAGE_free, .-EXTENDED_KEY_USAGE_free
	.globl	EXTENDED_KEY_USAGE_it
	.section	.rodata.str1.1
.LC4:
	.string	"EXTENDED_KEY_USAGE"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	EXTENDED_KEY_USAGE_it, @object
	.size	EXTENDED_KEY_USAGE_it, 56
EXTENDED_KEY_USAGE_it:
	.byte	0
	.zero	7
	.quad	-1
	.quad	EXTENDED_KEY_USAGE_item_tt
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC4
	.section	.data.rel.ro,"aw"
	.align 32
	.type	EXTENDED_KEY_USAGE_item_tt, @object
	.size	EXTENDED_KEY_USAGE_item_tt, 40
EXTENDED_KEY_USAGE_item_tt:
	.quad	4
	.quad	0
	.quad	0
	.quad	.LC4
	.quad	ASN1_OBJECT_it
	.globl	v3_ocsp_accresp
	.section	.data.rel.ro.local
	.align 32
	.type	v3_ocsp_accresp, @object
	.size	v3_ocsp_accresp, 104
v3_ocsp_accresp:
	.long	368
	.long	0
	.quad	EXTENDED_KEY_USAGE_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	i2v_EXTENDED_KEY_USAGE
	.quad	v2i_EXTENDED_KEY_USAGE
	.quad	0
	.quad	0
	.quad	0
	.globl	v3_ext_ku
	.align 32
	.type	v3_ext_ku, @object
	.size	v3_ext_ku, 104
v3_ext_ku:
	.long	126
	.long	0
	.quad	EXTENDED_KEY_USAGE_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	i2v_EXTENDED_KEY_USAGE
	.quad	v2i_EXTENDED_KEY_USAGE
	.quad	0
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
