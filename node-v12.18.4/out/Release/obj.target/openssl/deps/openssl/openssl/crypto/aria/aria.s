	.file	"aria.c"
	.text
	.p2align 4
	.globl	aria_encrypt
	.type	aria_encrypt, @function
aria_encrypt:
.LFB15:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	testq	%rsi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	sete	%dl
	pushq	%rbx
	testq	%r12, %r12
	sete	%al
	orb	%al, %dl
	.cfi_offset 3, -56
	jne	.L1
	testq	%rdi, %rdi
	je	.L1
	movl	272(%r12), %ebx
	movl	%ebx, %eax
	andl	$-3, %eax
	cmpl	$12, %eax
	je	.L6
	cmpl	$16, %ebx
	jne	.L1
.L6:
	movzbl	(%rdi), %eax
	movzbl	3(%rdi), %ecx
	leaq	S1(%rip), %r9
	leaq	X1(%rip), %r11
	xorl	(%r12), %ecx
	movzbl	4(%rdi), %edx
	sall	$24, %eax
	movzbl	8(%rdi), %r8d
	movzbl	15(%rdi), %r13d
	xorl	%eax, %ecx
	movzbl	1(%rdi), %eax
	sall	$24, %edx
	xorl	12(%r12), %r13d
	sall	$24, %r8d
	sall	$16, %eax
	xorl	%eax, %ecx
	movzbl	2(%rdi), %eax
	sall	$8, %eax
	xorl	%eax, %ecx
	movzbl	7(%rdi), %eax
	xorl	4(%r12), %eax
	xorl	%edx, %eax
	movzbl	5(%rdi), %edx
	movzbl	%cl, %r10d
	sall	$16, %edx
	xorl	%edx, %eax
	movzbl	6(%rdi), %edx
	sall	$8, %edx
	xorl	%edx, %eax
	movzbl	11(%rdi), %edx
	xorl	8(%r12), %edx
	xorl	%r8d, %edx
	movzbl	9(%rdi), %r8d
	sall	$16, %r8d
	xorl	%r8d, %edx
	movzbl	10(%rdi), %r8d
	sall	$8, %r8d
	xorl	%r8d, %edx
	movzbl	12(%rdi), %r8d
	sall	$24, %r8d
	xorl	%r8d, %r13d
	movzbl	13(%rdi), %r8d
	movzbl	14(%rdi), %edi
	sall	$16, %r8d
	sall	$8, %edi
	xorl	%r8d, %r13d
	leaq	X2(%rip), %r8
	xorl	%edi, %r13d
	movzbl	%ch, %edi
	movq	%rdi, %r15
	movl	%ecx, %edi
	shrl	$16, %ecx
	shrl	$24, %edi
	movl	(%r9,%rdi,4), %r14d
	movzbl	%cl, %edi
	xorl	(%r8,%r10,4), %r14d
	leaq	S2(%rip), %r10
	movl	(%r10,%rdi,4), %ecx
	movzbl	%ah, %edi
	xorl	%r14d, %ecx
	movq	%rdi, %r14
	xorl	(%r11,%r15,4), %ecx
	movl	%eax, %edi
	movzbl	%al, %r15d
	shrl	$16, %eax
	shrl	$24, %edi
	movzbl	%al, %eax
	movl	(%r9,%rdi,4), %edi
	xorl	(%r8,%r15,4), %edi
	movzbl	%dl, %r15d
	xorl	(%r10,%rax,4), %edi
	movzbl	%dh, %eax
	xorl	(%r11,%r14,4), %edi
	movq	%rax, %r14
	movl	%edx, %eax
	shrl	$16, %edx
	shrl	$24, %eax
	movzbl	%dl, %edx
	movl	(%r9,%rax,4), %eax
	xorl	(%r8,%r15,4), %eax
	movzbl	%r13b, %r15d
	xorl	(%r10,%rdx,4), %eax
	movl	%r13d, %edx
	xorl	(%r11,%r14,4), %eax
	movzbl	%dh, %edx
	xorl	%eax, %edi
	movq	%rdx, %r14
	movl	%r13d, %edx
	shrl	$16, %r13d
	xorl	%edi, %ecx
	shrl	$24, %edx
	movzbl	%r13b, %r13d
	movl	(%r9,%rdx,4), %edx
	xorl	(%r8,%r15,4), %edx
	xorl	(%r10,%r13,4), %edx
	xorl	(%r11,%r14,4), %edx
	movl	%edx, %r13d
	movl	%edi, %edx
	xorl	%r13d, %eax
	movl	%r13d, %edi
	xorl	%ecx, %eax
	xorl	%edx, %edi
	xorl	%eax, %edx
	movl	%edi, %r14d
	roll	$16, %eax
	movl	%edx, %r13d
	shrl	$8, %edx
	sall	$8, %r13d
	andl	$16711935, %edx
	shrl	$24, %r14d
	andl	$-16711936, %r13d
	orl	%r13d, %edx
	movl	%edi, %r13d
	sall	$24, %r13d
	xorl	%r14d, %r13d
	movl	%edi, %r14d
	sall	$8, %r14d
	andl	$16711680, %r14d
	shrl	$8, %edi
	xorl	%eax, %edx
	xorl	%r14d, %r13d
	andl	$65280, %edi
	xorl	%edx, %ecx
	xorl	%r13d, %edi
	movl	%edx, %r13d
	xorl	20(%r12), %edx
	xorl	%edi, %eax
	xorl	28(%r12), %edi
	xorl	%ecx, %eax
	xorl	%edi, %r13d
	leaq	32(%r12), %rdi
	xorl	16(%r12), %ecx
	xorl	%eax, %edx
	movq	%rdi, -48(%rbp)
	xorl	24(%r12), %eax
	cmpl	$2, %ebx
	je	.L4
	subl	$4, %ebx
	movq	%rsi, -64(%rbp)
	shrl	%ebx
	movq	%rbx, -56(%rbp)
	addq	$2, %rbx
	salq	$5, %rbx
	addq	%r12, %rbx
	movl	%r13d, %r12d
	movl	%edx, %r13d
	.p2align 4,,10
	.p2align 3
.L5:
	movzbl	%ch, %esi
	movzbl	%cl, %r15d
	movl	%ecx, %edx
	shrl	$16, %ecx
	shrl	$24, %edx
	movzbl	%cl, %ecx
	movl	(%r11,%rdx,4), %edx
	xorl	(%r10,%r15,4), %edx
	movzbl	%r13b, %r15d
	xorl	(%r8,%rcx,4), %edx
	movl	%r13d, %ecx
	shrl	$16, %r13d
	xorl	(%r9,%rsi,4), %edx
	movzbl	%r13b, %r13d
	movzbl	%ch, %esi
	shrl	$24, %ecx
	movl	(%r11,%rcx,4), %ecx
	xorl	(%r10,%r15,4), %ecx
	movzbl	%al, %r15d
	xorl	(%r8,%r13,4), %ecx
	movl	(%r9,%rsi,4), %r13d
	movzbl	%ah, %esi
	xorl	%ecx, %r13d
	movl	%eax, %ecx
	shrl	$16, %eax
	shrl	$24, %ecx
	movzbl	%al, %eax
	movl	(%r11,%rcx,4), %ecx
	xorl	(%r10,%r15,4), %ecx
	movzbl	%r12b, %r15d
	xorl	(%r8,%rax,4), %ecx
	movl	(%r9,%rsi,4), %eax
	xorl	%ecx, %eax
	movl	%r12d, %ecx
	shrl	$16, %r12d
	movzbl	%ch, %esi
	movzbl	%r12b, %r12d
	shrl	$24, %ecx
	xorl	%eax, %r13d
	movl	(%r11,%rcx,4), %ecx
	xorl	(%r10,%r15,4), %ecx
	xorl	%r13d, %edx
	xorl	(%r8,%r12,4), %ecx
	xorl	(%r9,%rsi,4), %ecx
	movl	%r13d, %r12d
	xorl	%ecx, %eax
	xorl	%r13d, %ecx
	movl	%ecx, %r13d
	xorl	%edx, %eax
	shrl	$8, %ecx
	sall	$8, %r13d
	xorl	%eax, %r12d
	andl	$16711935, %ecx
	roll	$16, %edx
	andl	$-16711936, %r13d
	movl	%r12d, %r14d
	movl	%r12d, %r15d
	orl	%r13d, %ecx
	movl	%r12d, %r13d
	shrl	$8, %r13d
	shrl	$24, %r15d
	sall	$24, %r14d
	sall	$8, %r12d
	andl	$65280, %r13d
	xorl	%r15d, %r14d
	andl	$16711680, %r12d
	xorl	%eax, %r14d
	xorl	%ecx, %eax
	xorl	12(%rdi), %ecx
	xorl	%r14d, %r12d
	movl	4(%rdi), %r14d
	xorl	%r12d, %r13d
	xorl	%r13d, %edx
	xorl	%r13d, %r14d
	xorl	%ecx, %r13d
	xorl	%edx, %eax
	xorl	(%rdi), %edx
	movzbl	%dh, %esi
	movzbl	%dl, %r15d
	movl	%edx, %ecx
	shrl	$16, %edx
	xorl	%eax, %r14d
	shrl	$24, %ecx
	movzbl	%dl, %edx
	xorl	8(%rdi), %eax
	movl	(%r9,%rcx,4), %ecx
	movl	%r14d, %r12d
	xorl	(%r8,%r15,4), %ecx
	movzbl	%r14b, %r15d
	xorl	(%r10,%rdx,4), %ecx
	movl	%r14d, %edx
	shrl	$16, %r14d
	xorl	(%r11,%rsi,4), %ecx
	movzbl	%dh, %edx
	shrl	$24, %r12d
	movzbl	%r14b, %r14d
	movzbl	%ah, %esi
	movl	(%r9,%r12,4), %r12d
	xorl	(%r8,%r15,4), %r12d
	movzbl	%r13b, %r15d
	xorl	(%r10,%r14,4), %r12d
	xorl	(%r11,%rdx,4), %r12d
	movl	%eax, %edx
	movzbl	%al, %r14d
	shrl	$24, %edx
	shrl	$16, %eax
	movl	(%r9,%rdx,4), %edx
	xorl	(%r8,%r14,4), %edx
	movl	%edx, %r14d
	movzbl	%al, %edx
	movl	(%r10,%rdx,4), %eax
	movl	%r13d, %edx
	xorl	%r14d, %eax
	shrl	$16, %r13d
	xorl	(%r11,%rsi,4), %eax
	movzbl	%dh, %esi
	movzbl	%r13b, %r13d
	shrl	$24, %edx
	xorl	%eax, %r12d
	movl	(%r9,%rdx,4), %edx
	xorl	(%r8,%r15,4), %edx
	xorl	%r12d, %ecx
	xorl	(%r10,%r13,4), %edx
	xorl	(%r11,%rsi,4), %edx
	xorl	%edx, %eax
	xorl	%r12d, %edx
	xorl	%ecx, %eax
	movl	%edx, %r14d
	xorl	%eax, %r12d
	shrl	$24, %r14d
	movl	%r12d, %r13d
	shrl	$8, %r12d
	sall	$8, %r13d
	andl	$16711935, %r12d
	roll	$16, %eax
	andl	$-16711936, %r13d
	orl	%r13d, %r12d
	movl	%edx, %r13d
	sall	$24, %r13d
	xorl	%eax, %r12d
	xorl	%r14d, %r13d
	movl	%edx, %r14d
	shrl	$8, %edx
	xorl	%r12d, %ecx
	sall	$8, %r14d
	andl	$65280, %edx
	andl	$16711680, %r14d
	xorl	%r14d, %r13d
	xorl	%r13d, %edx
	movl	20(%rdi), %r13d
	xorl	%edx, %eax
	xorl	%ecx, %eax
	xorl	16(%rdi), %ecx
	xorl	%r12d, %r13d
	xorl	28(%rdi), %edx
	xorl	%eax, %r13d
	addq	$32, %rdi
	xorl	-8(%rdi), %eax
	xorl	%edx, %r12d
	cmpq	%rdi, %rbx
	jne	.L5
	movq	-56(%rbp), %rdi
	movq	-64(%rbp), %rsi
	movl	%r13d, %edx
	movl	%r12d, %r13d
	addq	$1, %rdi
	salq	$5, %rdi
	addq	%rdi, -48(%rbp)
.L4:
	movl	%ecx, %edi
	movq	-48(%rbp), %r15
	shrl	$24, %edi
	movl	(%r11,%rdi,4), %ebx
	movzbl	%cl, %edi
	movzbl	(%r10,%rdi,4), %edi
	sall	$24, %ebx
	orl	%edi, %ebx
	movzbl	%ch, %edi
	movl	(%r9,%rdi,4), %edi
	sall	$8, %edi
	movzwl	%di, %edi
	orl	%edi, %ebx
	movl	%ecx, %edi
	shrl	$16, %edi
	movzbl	%dil, %edi
	movl	(%r8,%rdi,4), %ecx
	movl	%edx, %edi
	shrl	$24, %edi
	sall	$8, %ecx
	movl	(%r11,%rdi,4), %edi
	andl	$16711680, %ecx
	orl	%ebx, %ecx
	movzbl	%dl, %ebx
	sall	$24, %edi
	movzbl	(%r10,%rbx,4), %ebx
	orl	%ebx, %edi
	movzbl	%dh, %ebx
	shrl	$16, %edx
	movl	(%r9,%rbx,4), %ebx
	movzbl	%dl, %edx
	movl	(%r8,%rdx,4), %edx
	sall	$8, %ebx
	movzwl	%bx, %ebx
	sall	$8, %edx
	orl	%ebx, %edi
	andl	$16711680, %edx
	movzbl	%al, %ebx
	orl	%edi, %edx
	movl	%eax, %edi
	movzbl	(%r10,%rbx,4), %ebx
	xorl	4(%r15), %edx
	shrl	$24, %edi
	bswap	%edx
	movl	(%r11,%rdi,4), %edi
	sall	$24, %edi
	orl	%ebx, %edi
	movzbl	%ah, %ebx
	shrl	$16, %eax
	movl	(%r9,%rbx,4), %ebx
	movzbl	%al, %eax
	movl	(%r8,%rax,4), %eax
	sall	$8, %ebx
	movzwl	%bx, %ebx
	sall	$8, %eax
	orl	%ebx, %edi
	andl	$16711680, %eax
	movl	%r13d, %ebx
	orl	%eax, %edi
	movl	%r13d, %eax
	movzbl	%bh, %ebx
	xorl	8(%r15), %edi
	shrl	$24, %eax
	movl	(%r9,%rbx,4), %r9d
	bswap	%edi
	movl	(%r11,%rax,4), %eax
	movzbl	%r13b, %r11d
	movzbl	(%r10,%r11,4), %r10d
	sall	$24, %eax
	orl	%r10d, %eax
	sall	$8, %r9d
	xorl	(%r15), %ecx
	shrl	$16, %r13d
	movzwl	%r9w, %r9d
	bswap	%ecx
	movzbl	%r13b, %r13d
	orl	%r9d, %eax
	movl	(%r8,%r13,4), %r8d
	sall	$8, %r8d
	andl	$16711680, %r8d
	orl	%r8d, %eax
	xorl	12(%r15), %eax
	movl	%ecx, (%rsi)
	bswap	%eax
	movl	%edx, 4(%rsi)
	movl	%edi, 8(%rsi)
	movl	%eax, 12(%rsi)
.L1:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE15:
	.size	aria_encrypt, .-aria_encrypt
	.p2align 4
	.globl	aria_set_encrypt_key
	.type	aria_set_encrypt_key, @function
aria_set_encrypt_key:
.LFB16:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	leal	287(%rsi), %eax
	addl	$256, %ecx
	cmovns	%ecx, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	sarl	$5, %eax
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, -76(%rbp)
	testq	%rdi, %rdi
	je	.L22
	testq	%rdx, %rdx
	je	.L22
	movl	%esi, %ecx
	andl	$-65, %ecx
	cmpl	$128, %ecx
	je	.L24
	cmpl	$256, %esi
	jne	.L23
.L24:
	movl	-76(%rbp), %r15d
	movl	%eax, 272(%rdx)
	leaq	X2(%rip), %r11
	leaq	X1(%rip), %r9
	movzbl	1(%rdi), %ecx
	movl	%r15d, %eax
	leal	-65(%r15), %esi
	addl	$-128, %eax
	cmovns	%eax, %esi
	leaq	Key_RC(%rip), %rax
	sall	$16, %ecx
	sarl	$6, %esi
	movslq	%esi, %rsi
	salq	$4, %rsi
	addq	%rax, %rsi
	movzbl	(%rdi), %eax
	sall	$24, %eax
	xorl	%ecx, %eax
	movzbl	3(%rdi), %ecx
	xorl	%ecx, %eax
	movzbl	2(%rdi), %ecx
	movl	%eax, %ebx
	movzbl	4(%rdi), %eax
	sall	$8, %ecx
	xorl	%ecx, %ebx
	movzbl	5(%rdi), %ecx
	sall	$24, %eax
	movl	%ebx, -80(%rbp)
	xorl	(%rsi), %ebx
	sall	$16, %ecx
	movl	%ebx, %r13d
	movzbl	%bh, %ebx
	xorl	%ecx, %eax
	movzbl	7(%rdi), %ecx
	xorl	%ecx, %eax
	movzbl	6(%rdi), %ecx
	movl	%eax, %r10d
	movzbl	8(%rdi), %eax
	sall	$8, %ecx
	xorl	%ecx, %r10d
	movzbl	9(%rdi), %ecx
	sall	$24, %eax
	movl	%r10d, -84(%rbp)
	xorl	4(%rsi), %r10d
	sall	$16, %ecx
	xorl	%ecx, %eax
	movzbl	11(%rdi), %ecx
	xorl	%ecx, %eax
	movzbl	10(%rdi), %ecx
	movl	%eax, %r14d
	movzbl	12(%rdi), %eax
	sall	$8, %ecx
	xorl	%ecx, %r14d
	movzbl	13(%rdi), %ecx
	sall	$24, %eax
	movl	%r14d, -88(%rbp)
	sall	$16, %ecx
	xorl	%ecx, %eax
	movzbl	15(%rdi), %ecx
	xorl	%ecx, %eax
	movzbl	14(%rdi), %ecx
	movl	%eax, %r8d
	movl	%r10d, %eax
	movzbl	%r13b, %r10d
	sall	$8, %ecx
	movzbl	%al, %r15d
	xorl	%ecx, %r8d
	movl	8(%rsi), %ecx
	movl	%r8d, -52(%rbp)
	xorl	12(%rsi), %r8d
	movl	%r8d, %r12d
	movl	%r13d, %r8d
	xorl	%r14d, %ecx
	movq	%rbx, %r14
	shrl	$24, %r8d
	leaq	S1(%rip), %rbx
	movl	%ecx, -56(%rbp)
	movzbl	%ah, %ecx
	movl	(%rbx,%r8,4), %r8d
	xorl	(%r11,%r10,4), %r8d
	shrl	$16, %r13d
	leaq	S2(%rip), %r10
	movzbl	%r13b, %r13d
	xorl	(%r10,%r13,4), %r8d
	movl	%eax, %r13d
	shrl	$16, %eax
	xorl	(%r9,%r14,4), %r8d
	shrl	$24, %r13d
	movzbl	%al, %eax
	movl	(%rbx,%r13,4), %r13d
	xorl	(%r11,%r15,4), %r13d
	xorl	(%r10,%rax,4), %r13d
	xorl	(%r9,%rcx,4), %r13d
	movl	-56(%rbp), %ecx
	movzbl	%ch, %eax
	movl	%ecx, %r15d
	shrl	$24, %ecx
	movq	%rax, %r14
	movslq	%ecx, %rax
	movl	%r15d, %ecx
	movzbl	%r15b, %r15d
	shrl	$16, %ecx
	movl	(%rbx,%rax,4), %eax
	xorl	(%r11,%r15,4), %eax
	movzbl	%r12b, %r15d
	movzbl	%cl, %ecx
	xorl	(%r10,%rcx,4), %eax
	movl	%r12d, %ecx
	xorl	(%r9,%r14,4), %eax
	movzbl	%ch, %ecx
	xorl	%eax, %r13d
	movq	%rcx, %r14
	movl	%r12d, %ecx
	shrl	$16, %r12d
	xorl	%r13d, %r8d
	shrl	$24, %ecx
	movzbl	%r12b, %r12d
	movl	(%rbx,%rcx,4), %ecx
	xorl	(%r11,%r15,4), %ecx
	xorl	(%r10,%r12,4), %ecx
	xorl	(%r9,%r14,4), %ecx
	xorl	%ecx, %eax
	xorl	%r13d, %ecx
	movl	-76(%rbp), %r15d
	xorl	%r8d, %eax
	movl	%ecx, %r14d
	xorl	%eax, %r13d
	sall	$24, %r14d
	movl	%r13d, %r12d
	shrl	$8, %r13d
	sall	$8, %r12d
	andl	$16711935, %r13d
	roll	$16, %eax
	andl	$-16711936, %r12d
	orl	%r12d, %r13d
	movl	%ecx, %r12d
	shrl	$24, %r12d
	xorl	%eax, %r13d
	xorl	%r12d, %r14d
	movl	%ecx, %r12d
	shrl	$8, %ecx
	sall	$8, %r12d
	andl	$65280, %ecx
	andl	$16711680, %r12d
	xorl	%r14d, %r12d
	movl	%r8d, %r14d
	xorl	%r12d, %ecx
	xorl	%r13d, %r14d
	movl	%r13d, %r12d
	xorl	%ecx, %eax
	xorl	%r13d, %ecx
	movl	%r14d, -60(%rbp)
	movl	%eax, %r8d
	movl	%ecx, -68(%rbp)
	xorl	%r14d, %r8d
	xorl	%r8d, %r12d
	movl	%r8d, -72(%rbp)
	movl	%r12d, -64(%rbp)
	cmpl	$128, %r15d
	jle	.L19
	movzbl	16(%rdi), %eax
	sall	$24, %eax
	movl	%eax, %ecx
	movzbl	17(%rdi), %eax
	sall	$16, %eax
	xorl	%ecx, %eax
	movzbl	19(%rdi), %ecx
	xorl	%ecx, %eax
	movzbl	18(%rdi), %ecx
	sall	$8, %ecx
	xorl	%ecx, %eax
	xorl	%eax, %r14d
	movzbl	20(%rdi), %eax
	movl	%r14d, -60(%rbp)
	sall	$24, %eax
	movl	%eax, %ecx
	movzbl	21(%rdi), %eax
	sall	$16, %eax
	xorl	%ecx, %eax
	movzbl	23(%rdi), %ecx
	xorl	%ecx, %eax
	movzbl	22(%rdi), %ecx
	sall	$8, %ecx
	xorl	%ecx, %eax
	xorl	%eax, %r12d
	movl	%r12d, -64(%rbp)
	cmpl	$192, %r15d
	jle	.L19
	movzbl	24(%rdi), %eax
	sall	$24, %eax
	movl	%eax, %ecx
	movzbl	25(%rdi), %eax
	sall	$16, %eax
	xorl	%ecx, %eax
	movzbl	27(%rdi), %ecx
	xorl	%ecx, %eax
	movzbl	26(%rdi), %ecx
	sall	$8, %ecx
	xorl	%ecx, %eax
	movzbl	28(%rdi), %ecx
	xorl	%eax, %r8d
	movl	%ecx, %eax
	movzbl	29(%rdi), %ecx
	movl	%r8d, -72(%rbp)
	sall	$24, %eax
	sall	$16, %ecx
	xorl	%eax, %ecx
	movzbl	31(%rdi), %eax
	xorl	%ecx, %eax
	movzbl	30(%rdi), %ecx
	sall	$8, %ecx
	xorl	%ecx, %eax
	xorl	%eax, -68(%rbp)
.L19:
	movl	-60(%rbp), %ecx
	movl	-72(%rbp), %edi
	xorl	16(%rsi), %ecx
	xorl	24(%rsi), %edi
	movl	%edi, %r13d
	movl	-68(%rbp), %r8d
	movzbl	%ch, %edi
	xorl	28(%rsi), %r8d
	movl	%r8d, %r12d
	movzbl	%cl, %r14d
	movq	%rdi, %r8
	movl	%ecx, %edi
	shrl	$16, %ecx
	shrl	$24, %edi
	movl	-64(%rbp), %eax
	xorl	20(%rsi), %eax
	movzbl	%cl, %ecx
	movl	(%r9,%rdi,4), %edi
	xorl	(%r10,%r14,4), %edi
	movzbl	%al, %r14d
	xorl	(%r11,%rcx,4), %edi
	xorl	(%rbx,%r8,4), %edi
	movl	%eax, %r8d
	movl	%edi, %ecx
	movzbl	%ah, %edi
	shrl	$16, %eax
	movd	-84(%rbp), %xmm6
	shrl	$24, %r8d
	movzbl	%al, %eax
	movl	-80(%rbp), %r15d
	movd	-52(%rbp), %xmm7
	movl	(%r9,%r8,4), %r8d
	xorl	(%r10,%r14,4), %r8d
	movzbl	%r13b, %r14d
	xorl	(%r11,%rax,4), %r8d
	movl	%r13d, %eax
	movd	%r15d, %xmm3
	xorl	(%rbx,%rdi,4), %r8d
	movl	%r13d, %edi
	shrl	$16, %r13d
	movzbl	%ah, %eax
	movd	-88(%rbp), %xmm0
	shrl	$24, %edi
	movzbl	%r13b, %r13d
	punpckldq	%xmm6, %xmm3
	movl	(%r9,%rdi,4), %edi
	xorl	(%r10,%r14,4), %edi
	punpckldq	%xmm7, %xmm0
	xorl	(%r11,%r13,4), %edi
	xorl	(%rbx,%rax,4), %edi
	movl	%r12d, %eax
	movzbl	%r12b, %r13d
	movzbl	%ah, %eax
	xorl	%edi, %r8d
	punpcklqdq	%xmm0, %xmm3
	movq	%rax, %r14
	movl	%r12d, %eax
	shrl	$16, %r12d
	xorl	%r8d, %ecx
	shrl	$24, %eax
	movzbl	%r12b, %r12d
	movl	(%r9,%rax,4), %eax
	xorl	(%r10,%r13,4), %eax
	xorl	(%r11,%r12,4), %eax
	xorl	(%rbx,%r14,4), %eax
	xorl	%eax, %edi
	xorl	%r8d, %eax
	movl	%eax, %r14d
	xorl	%ecx, %edi
	sall	$8, %r14d
	xorl	%edi, %r8d
	movl	%r14d, %r13d
	movl	%r8d, %r12d
	andl	$-16711936, %r13d
	shrl	$8, %eax
	sall	$24, %r12d
	andl	$16711935, %eax
	roll	$16, %ecx
	orl	%eax, %r13d
	movl	%r8d, %eax
	shrl	$8, %eax
	movl	%eax, %r14d
	movl	%r8d, %eax
	shrl	$24, %eax
	andl	$65280, %r14d
	xorl	%eax, %r12d
	movl	%r8d, %eax
	movd	%xmm6, %r8d
	sall	$8, %eax
	xorl	%edi, %r12d
	xorl	%r13d, %edi
	xorl	-52(%rbp), %r13d
	andl	$16711680, %eax
	xorl	%r12d, %eax
	xorl	%r14d, %eax
	xorl	%eax, %ecx
	xorl	%eax, %r8d
	xorl	%r13d, %eax
	xorl	%ecx, %edi
	xorl	%r15d, %ecx
	movl	%eax, %r14d
	xorl	%edi, %r8d
	xorl	-88(%rbp), %edi
	movl	%ecx, -56(%rbp)
	movl	%edi, %r15d
	movl	32(%rsi), %edi
	movl	36(%rsi), %eax
	xorl	%r8d, %eax
	xorl	%ecx, %edi
	movl	%eax, -96(%rbp)
	movl	40(%rsi), %eax
	movzbl	%dil, %ecx
	xorl	%r15d, %eax
	movl	%eax, %r13d
	movl	44(%rsi), %eax
	movl	%edi, %esi
	shrl	$24, %esi
	xorl	%r14d, %eax
	movl	(%rbx,%rsi,4), %esi
	xorl	(%r11,%rcx,4), %esi
	movl	%eax, %r12d
	movl	-96(%rbp), %ecx
	movl	%edi, %eax
	shrl	$16, %edi
	movzbl	%ah, %eax
	movzbl	%dil, %edi
	xorl	(%r10,%rdi,4), %esi
	xorl	(%r9,%rax,4), %esi
	movzbl	%ch, %eax
	movl	%ecx, %edi
	movq	%rax, -96(%rbp)
	movl	%ecx, %eax
	shrl	$24, %edi
	movzbl	%cl, %ecx
	shrl	$16, %eax
	movl	(%rbx,%rdi,4), %edi
	xorl	(%r11,%rcx,4), %edi
	movzbl	%r13b, %ecx
	movzbl	%al, %eax
	xorl	(%r10,%rax,4), %edi
	movq	-96(%rbp), %rax
	xorl	(%r9,%rax,4), %edi
	movl	%r13d, %eax
	movzbl	%ah, %eax
	movq	%rax, -96(%rbp)
	movl	%r13d, %eax
	shrl	$16, %r13d
	shrl	$24, %eax
	movzbl	%r13b, %r13d
	movl	(%rbx,%rax,4), %eax
	xorl	(%r11,%rcx,4), %eax
	movq	-96(%rbp), %rcx
	xorl	(%r10,%r13,4), %eax
	movl	%r12d, %r13d
	shrl	$24, %r13d
	xorl	(%r9,%rcx,4), %eax
	movzbl	%r12b, %ecx
	movl	(%rbx,%r13,4), %ebx
	xorl	(%r11,%rcx,4), %ebx
	movl	-64(%rbp), %r13d
	movl	%eax, -96(%rbp)
	movl	%r12d, %eax
	shrl	$16, %r12d
	movzbl	%ah, %eax
	movl	-60(%rbp), %r11d
	movzbl	%r12b, %r12d
	movd	%r13d, %xmm7
	xorl	(%r10,%r12,4), %ebx
	xorl	(%r9,%rax,4), %ebx
	movd	%r11d, %xmm6
	movl	-96(%rbp), %eax
	movl	%ebx, %r10d
	movl	-72(%rbp), %r12d
	punpckldq	%xmm7, %xmm6
	movd	%r11d, %xmm5
	xorl	%eax, %edi
	movd	%r12d, %xmm2
	movd	%r12d, %xmm0
	xorl	%ebx, %eax
	xorl	%edi, %esi
	xorl	%edi, %r10d
	xorl	%esi, %eax
	xorl	%eax, %edi
	roll	$16, %eax
	movl	%edi, %r9d
	shrl	$8, %edi
	sall	$8, %r9d
	andl	$16711935, %edi
	andl	$-16711936, %r9d
	orl	%edi, %r9d
	movl	%r10d, %edi
	sall	$24, %edi
	movl	%edi, %ebx
	movl	%r10d, %edi
	shrl	$24, %edi
	xorl	%ebx, %edi
	movl	%r10d, %ebx
	sall	$8, %ebx
	andl	$16711680, %ebx
	xorl	%ebx, %edi
	shrl	$8, %r10d
	xorl	%eax, %r9d
	movl	%r10d, %ebx
	xorl	%r9d, %esi
	movl	%r13d, %r10d
	andl	$65280, %ebx
	xorl	%r9d, %r10d
	xorl	%edi, %ebx
	xorl	%ebx, %eax
	movl	%ebx, %ecx
	movl	-68(%rbp), %ebx
	xorl	%esi, %eax
	xorl	%r11d, %esi
	movd	%ebx, %xmm7
	movd	%ebx, %xmm4
	xorl	%ebx, %ecx
	xorl	%eax, %r10d
	punpckldq	%xmm5, %xmm7
	movd	%r13d, %xmm5
	punpckldq	%xmm4, %xmm0
	xorl	%r12d, %eax
	punpckldq	%xmm2, %xmm5
	movdqa	%xmm6, %xmm4
	movdqa	%xmm7, %xmm1
	movl	%ecx, %edi
	punpcklqdq	%xmm0, %xmm4
	punpcklqdq	%xmm5, %xmm1
	punpcklqdq	%xmm7, %xmm5
	xorl	%r9d, %edi
	movdqa	%xmm1, %xmm2
	movdqa	%xmm4, %xmm8
	punpcklqdq	%xmm6, %xmm0
	movl	%r14d, %r9d
	pslld	$13, %xmm2
	psrld	$19, %xmm8
	sall	$13, %r9d
	pxor	%xmm8, %xmm2
	pslld	$1, %xmm1
	xorl	%r11d, %r9d
	pxor	%xmm3, %xmm2
	psrld	$3, %xmm0
	movups	%xmm2, (%rdx)
	movl	-56(%rbp), %r12d
	movdqa	%xmm4, %xmm2
	psrld	$31, %xmm2
	movl	%r12d, %ecx
	pxor	%xmm2, %xmm1
	shrl	$19, %ecx
	pxor	%xmm3, %xmm1
	xorl	%ecx, %r9d
	movl	%r12d, %ecx
	movups	%xmm1, 64(%rdx)
	movdqa	%xmm5, %xmm1
	sall	$13, %ecx
	movl	%r9d, 16(%rdx)
	pslld	$29, %xmm1
	movl	%ecx, %r9d
	movl	%r8d, %ecx
	pxor	%xmm1, %xmm0
	xorl	%r13d, %r9d
	shrl	$19, %ecx
	movl	-88(%rbp), %r13d
	pxor	%xmm3, %xmm0
	xorl	%ecx, %r9d
	movl	%r8d, %ecx
	movl	%r9d, 20(%rdx)
	movl	%r15d, %r9d
	sall	$13, %ecx
	shrl	$19, %r9d
	xorl	%ecx, %r9d
	xorl	-72(%rbp), %r9d
	movl	%r15d, %ecx
	movl	%r9d, 24(%rdx)
	movl	%r14d, %r9d
	sall	$13, %ecx
	shrl	$19, %r9d
	xorl	%ebx, %r9d
	movl	-84(%rbp), %ebx
	xorl	%ecx, %r9d
	movl	%esi, %ecx
	movl	%r9d, 28(%rdx)
	movl	%edi, %r9d
	sall	$13, %r9d
	xorl	%r12d, %r9d
	shrl	$19, %ecx
	movl	-80(%rbp), %r12d
	xorl	%ecx, %r9d
	movl	%r10d, %ecx
	movl	%r9d, 32(%rdx)
	movl	%esi, %r9d
	shrl	$19, %ecx
	sall	$13, %r9d
	xorl	%r8d, %r9d
	xorl	%ecx, %r9d
	movl	%r10d, %ecx
	movl	%r9d, 36(%rdx)
	movl	%eax, %r9d
	sall	$13, %ecx
	shrl	$19, %r9d
	xorl	%ecx, %r9d
	movl	%eax, %ecx
	xorl	%r15d, %r9d
	sall	$13, %ecx
	movl	%r9d, 40(%rdx)
	movl	%edi, %r9d
	shrl	$19, %r9d
	xorl	%r14d, %r9d
	xorl	%ecx, %r9d
	movl	-52(%rbp), %ecx
	movl	%r9d, 44(%rdx)
	movl	%r12d, %r9d
	movl	%ecx, %r11d
	shrl	$19, %r9d
	sall	$13, %r11d
	xorl	%r11d, %r9d
	movl	%r12d, %r11d
	xorl	%esi, %r9d
	sall	$13, %r11d
	movl	%r9d, 48(%rdx)
	movl	%ebx, %r9d
	shrl	$19, %r9d
	xorl	%r11d, %r9d
	movl	%ebx, %r11d
	xorl	%r10d, %r9d
	sall	$13, %r11d
	movl	%r9d, 52(%rdx)
	movl	%r13d, %r9d
	shrl	$19, %r9d
	xorl	%r11d, %r9d
	movl	%r13d, %r11d
	xorl	%eax, %r9d
	sall	$13, %r11d
	movl	%r9d, 56(%rdx)
	movl	%ecx, %r9d
	movl	-56(%rbp), %ecx
	shrl	$19, %r9d
	xorl	%r11d, %r9d
	shrl	$31, %ecx
	leal	(%r8,%r8), %r11d
	xorl	%edi, %r9d
	movl	%r9d, 60(%rdx)
	leal	(%r14,%r14), %r9d
	xorl	-60(%rbp), %r9d
	xorl	%ecx, %r9d
	movl	-56(%rbp), %ecx
	movl	%r9d, 80(%rdx)
	leal	(%rcx,%rcx), %r9d
	movl	%r8d, %ecx
	xorl	-64(%rbp), %r9d
	shrl	$31, %ecx
	xorl	%ecx, %r9d
	movl	%r15d, %ecx
	shrl	$31, %ecx
	movl	%r9d, 84(%rdx)
	movl	%ecx, %r9d
	movl	%r14d, %ecx
	xorl	%r11d, %r9d
	xorl	-72(%rbp), %r9d
	shrl	$31, %ecx
	leal	(%r15,%r15), %r11d
	movl	%r9d, 88(%rdx)
	movl	-68(%rbp), %r9d
	xorl	%ecx, %r9d
	movl	%esi, %ecx
	xorl	%r11d, %r9d
	shrl	$31, %ecx
	leal	(%r10,%r10), %r11d
	movl	%r9d, 92(%rdx)
	leal	(%rdi,%rdi), %r9d
	xorl	-56(%rbp), %r9d
	xorl	%ecx, %r9d
	movl	%r10d, %ecx
	movl	%r9d, 96(%rdx)
	leal	(%rsi,%rsi), %r9d
	shrl	$31, %ecx
	xorl	%r8d, %r9d
	xorl	%ecx, %r9d
	movl	%eax, %ecx
	shrl	$31, %ecx
	movl	%r9d, 100(%rdx)
	movl	%ecx, %r9d
	movl	%edi, %ecx
	xorl	%r11d, %r9d
	shrl	$31, %ecx
	leal	(%rax,%rax), %r11d
	xorl	%r15d, %r9d
	movl	%r9d, 104(%rdx)
	movl	%ecx, %r9d
	movl	-52(%rbp), %ecx
	xorl	%r14d, %r9d
	movups	%xmm0, 128(%rdx)
	xorl	%r11d, %r9d
	movl	%ecx, %r11d
	movl	%r9d, 108(%rdx)
	movl	%r12d, %r9d
	addl	%r11d, %r11d
	shrl	$31, %r9d
	xorl	%r11d, %r9d
	leal	(%r12,%r12), %r11d
	xorl	%esi, %r9d
	movl	%r9d, 112(%rdx)
	movl	%ebx, %r9d
	shrl	$31, %r9d
	xorl	%r11d, %r9d
	leal	(%rbx,%rbx), %r11d
	xorl	%r10d, %r9d
	movl	%r9d, 116(%rdx)
	movl	%r13d, %r9d
	shrl	$31, %r9d
	xorl	%r11d, %r9d
	leal	(%r13,%r13), %r11d
	xorl	%eax, %r9d
	movl	%r9d, 120(%rdx)
	movl	%ecx, %r9d
	movl	%r15d, %ecx
	shrl	$31, %r9d
	shrl	$3, %ecx
	xorl	%r11d, %r9d
	movl	%ebx, %r11d
	xorl	%edi, %r9d
	movl	%r9d, 124(%rdx)
	movl	%ecx, %r9d
	movl	%r8d, %ecx
	sall	$29, %ecx
	xorl	%ecx, %r9d
	xorl	-60(%rbp), %r9d
	movl	%r14d, %ecx
	movl	%r9d, 144(%rdx)
	movl	-64(%rbp), %r9d
	shrl	$3, %ecx
	xorl	%ecx, %r9d
	movl	%r15d, %ecx
	sall	$29, %ecx
	xorl	%ecx, %r9d
	movl	%r14d, %ecx
	movl	%r9d, 148(%rdx)
	movl	-72(%rbp), %r9d
	sall	$29, %ecx
	xorl	%ecx, %r9d
	movl	-56(%rbp), %ecx
	shrl	$3, %ecx
	xorl	%ecx, %r9d
	movl	-56(%rbp), %ecx
	movl	%r9d, 152(%rdx)
	movl	-68(%rbp), %r9d
	sall	$29, %ecx
	xorl	%ecx, %r9d
	movl	%r8d, %ecx
	shrl	$3, %ecx
	xorl	%ecx, %r9d
	movl	%eax, %ecx
	shrl	$3, %ecx
	movl	%r9d, 156(%rdx)
	movl	%ecx, %r9d
	movl	%r10d, %ecx
	sall	$29, %ecx
	xorl	%ecx, %r9d
	movl	%edi, %ecx
	xorl	-56(%rbp), %r9d
	shrl	$3, %ecx
	movl	%r9d, 160(%rdx)
	movl	%ecx, %r9d
	movl	%eax, %ecx
	sall	$29, %ecx
	xorl	%r8d, %r9d
	xorl	%ecx, %r9d
	movl	%edi, %ecx
	sall	$29, %ecx
	movl	%r9d, 164(%rdx)
	movl	%ecx, %r9d
	movl	%esi, %ecx
	shrl	$3, %ecx
	xorl	%r15d, %r9d
	xorl	%ecx, %r9d
	movl	%esi, %ecx
	sall	$29, %ecx
	movl	%r9d, 168(%rdx)
	movl	%ecx, %r9d
	movl	%r10d, %ecx
	xorl	%r14d, %r9d
	shrl	$3, %ecx
	xorl	%ecx, %r9d
	sall	$29, %r11d
	movl	-52(%rbp), %ecx
	movl	%r9d, 172(%rdx)
	movl	%r13d, %r9d
	shrl	$3, %r9d
	xorl	%r11d, %r9d
	movl	%r13d, %r11d
	xorl	%esi, %r9d
	sall	$29, %r11d
	movl	%r9d, 176(%rdx)
	movl	%ecx, %r9d
	shrl	$3, %r9d
	xorl	%r11d, %r9d
	movl	%ecx, %r11d
	movl	-72(%rbp), %ecx
	xorl	%r10d, %r9d
	sall	$29, %r11d
	movl	%r9d, 180(%rdx)
	movl	%r12d, %r9d
	shrl	$3, %r9d
	xorl	%r11d, %r9d
	movl	%r12d, %r11d
	xorl	%eax, %r9d
	sall	$29, %r11d
	movl	%r9d, 184(%rdx)
	movl	%ebx, %r9d
	shrl	$3, %r9d
	xorl	%r11d, %r9d
	movl	-60(%rbp), %r11d
	xorl	%edi, %r9d
	movl	%r9d, 188(%rdx)
	movl	-64(%rbp), %r9d
	sall	$31, %r11d
	shrl	%r9d
	xorl	%r11d, %r9d
	xorl	%r12d, %r9d
	movl	%ecx, %r12d
	shrl	%r12d
	movl	%r9d, 192(%rdx)
	movl	%r12d, %r9d
	movl	-64(%rbp), %r12d
	sall	$31, %r12d
	xorl	%r12d, %r9d
	movl	%ecx, %r12d
	xorl	%ebx, %r9d
	sall	$31, %r12d
	movl	%r9d, 196(%rdx)
	movl	-68(%rbp), %ebx
	movl	%ebx, %r9d
	shrl	%r9d
	xorl	%r12d, %r9d
	xorl	%r13d, %r9d
	movl	%r9d, 200(%rdx)
	movl	-60(%rbp), %r9d
	shrl	%r9d
	sall	$31, %ebx
	xorl	%ebx, %r9d
	xorl	-52(%rbp), %r9d
	cmpl	$128, -76(%rbp)
	movl	%r9d, 204(%rdx)
	jle	.L29
	movl	-56(%rbp), %ebx
	movl	-60(%rbp), %r9d
	movl	%r8d, %r11d
	shrl	%r11d
	movl	%ebx, %ecx
	sall	$31, %ecx
	xorl	%ecx, %r9d
	movl	%ebx, %ecx
	xorl	%r11d, %r9d
	movl	%r8d, %r11d
	shrl	%ecx
	movl	%r9d, 208(%rdx)
	movl	%r15d, %r9d
	sall	$31, %r11d
	shrl	%r9d
	xorl	%r11d, %r9d
	xorl	-64(%rbp), %r9d
	movl	%r15d, %r11d
	movl	%r9d, 212(%rdx)
	movl	%r14d, %r9d
	sall	$31, %r11d
	shrl	%r9d
	xorl	-72(%rbp), %r9d
	xorl	%r11d, %r9d
	movl	%r9d, 216(%rdx)
	movl	%r14d, %r9d
	sall	$31, %r9d
	xorl	-68(%rbp), %r9d
	xorl	%ecx, %r9d
	movl	%r9d, 220(%rdx)
	movl	%esi, %r9d
	sall	$31, %r9d
	xorl	%r9d, %ebx
	movl	%r10d, %r9d
	shrl	%r9d
	movl	%ebx, %ecx
	xorl	%r9d, %ecx
	movl	%r10d, %r9d
	movl	%ecx, 224(%rdx)
	movl	%eax, %ecx
	sall	$31, %r9d
	shrl	%ecx
	xorl	%r9d, %ecx
	xorl	%ecx, %r8d
	movl	%edi, %ecx
	shrl	%ecx
	movl	%r8d, 228(%rdx)
	movl	%eax, %r8d
	sall	$31, %r8d
	xorl	%r15d, %ecx
	xorl	%r8d, %ecx
	movl	%esi, %r8d
	movl	%ecx, 232(%rdx)
	movl	%edi, %ecx
	shrl	%r8d
	sall	$31, %ecx
	xorl	%r14d, %ecx
	xorl	%r8d, %ecx
	cmpl	$192, -76(%rbp)
	movl	%ecx, 236(%rdx)
	jle	.L29
	movd	-80(%rbp), %xmm6
	movd	-52(%rbp), %xmm2
	movdqa	%xmm3, %xmm1
	movd	%r10d, %xmm7
	movd	-84(%rbp), %xmm0
	pslld	$31, %xmm1
	pslld	$19, %xmm4
	punpckldq	%xmm6, %xmm2
	movd	-88(%rbp), %xmm6
	psrld	$13, %xmm5
	pxor	%xmm4, %xmm5
	punpckldq	%xmm6, %xmm0
	movd	%edi, %xmm6
	pxor	%xmm5, %xmm3
	punpcklqdq	%xmm2, %xmm0
	movd	%eax, %xmm2
	movups	%xmm3, 256(%rdx)
	psrld	$1, %xmm0
	punpckldq	%xmm6, %xmm2
	pxor	%xmm1, %xmm0
	movd	%esi, %xmm1
	punpckldq	%xmm7, %xmm1
	punpcklqdq	%xmm2, %xmm1
	pxor	%xmm0, %xmm1
	movups	%xmm1, 240(%rdx)
.L29:
	xorl	%eax, %eax
.L16:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L23:
	.cfi_restore_state
	movl	$-2, %eax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L22:
	movl	$-1, %eax
	jmp	.L16
	.cfi_endproc
.LFE16:
	.size	aria_set_encrypt_key, .-aria_set_encrypt_key
	.p2align 4
	.globl	aria_set_decrypt_key
	.type	aria_set_decrypt_key, @function
aria_set_decrypt_key:
.LFB17:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	call	aria_set_encrypt_key
	movl	%eax, -52(%rbp)
	testl	%eax, %eax
	je	.L36
.L30:
	movl	-52(%rbp), %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	movl	272(%rdx), %eax
	movdqu	(%rdx), %xmm0
	leaq	16(%rdx), %r15
	salq	$4, %rax
	addq	%rdx, %rax
	movdqu	(%rax), %xmm1
	movq	%rax, -64(%rbp)
	subq	$16, %rax
	movups	%xmm1, (%rdx)
	movaps	%xmm1, -80(%rbp)
	movups	%xmm0, 16(%rax)
	movq	%rax, -80(%rbp)
	cmpq	%rax, %r15
	jnb	.L32
	movq	%rax, %r10
	movq	%r15, %r9
	.p2align 4,,10
	.p2align 3
.L33:
	movl	(%r9), %eax
	movl	%eax, %ecx
	rorl	$8, %ecx
	xorl	%ecx, %eax
	movl	%eax, %r11d
	movl	4(%r9), %eax
	roll	$16, %r11d
	movl	%eax, %edi
	xorl	%ecx, %r11d
	movl	8(%r9), %ecx
	rorl	$8, %edi
	xorl	%edi, %eax
	movl	%ecx, %esi
	roll	$16, %eax
	rorl	$8, %esi
	xorl	%eax, %edi
	movl	%ecx, %eax
	xorl	%esi, %eax
	roll	$16, %eax
	xorl	%esi, %eax
	movl	12(%r9), %esi
	movl	%esi, %ecx
	movl	%esi, %ebx
	rorl	$8, %ecx
	xorl	%ecx, %ebx
	roll	$16, %ebx
	xorl	%ecx, %ebx
	movl	%edi, %ecx
	xorl	%eax, %ecx
	xorl	%ebx, %eax
	xorl	%ecx, %r11d
	xorl	%ecx, %ebx
	xorl	%r11d, %eax
	movl	%ebx, %esi
	xorl	%eax, %ecx
	movl	%esi, %edi
	roll	$16, %eax
	movl	%ecx, %ebx
	shrl	$8, %ecx
	sall	$8, %ebx
	andl	$16711935, %ecx
	shrl	$24, %edi
	andl	$-16711936, %ebx
	orl	%ebx, %ecx
	movl	%esi, %ebx
	sall	$24, %ebx
	xorl	%edi, %ebx
	movl	%esi, %edi
	sall	$8, %edi
	andl	$16711680, %edi
	shrl	$8, %esi
	xorl	%eax, %ecx
	xorl	%edi, %ebx
	andl	$65280, %esi
	xorl	%ecx, %r11d
	xorl	%esi, %ebx
	movl	(%r10), %esi
	xorl	%ebx, %eax
	movl	%esi, %edi
	xorl	%r11d, %eax
	rorl	$8, %edi
	xorl	%edi, %esi
	movl	%esi, %r12d
	roll	$16, %r12d
	xorl	%edi, %r12d
	movl	4(%r10), %edi
	movl	%edi, %r8d
	movl	%edi, %esi
	movl	8(%r10), %edi
	rorl	$8, %r8d
	movl	%edi, %r13d
	xorl	%r8d, %esi
	rorl	$8, %r13d
	roll	$16, %esi
	xorl	%r13d, %edi
	xorl	%r8d, %esi
	movl	%edi, %r8d
	roll	$16, %r8d
	xorl	%r13d, %r8d
	movl	12(%r10), %r13d
	xorl	%r8d, %esi
	movl	%r13d, %edi
	xorl	%esi, %r12d
	rorl	$8, %edi
	xorl	%edi, %r13d
	roll	$16, %r13d
	xorl	%r13d, %edi
	xorl	%edi, %r8d
	xorl	%esi, %edi
	xorl	%r12d, %r8d
	movl	%edi, %r14d
	xorl	%r8d, %esi
	movl	%esi, %r13d
	sall	$8, %r13d
	andl	$-16711936, %r13d
	shrl	$8, %esi
	addq	$16, %r9
	subq	$16, %r10
	sall	$24, %r14d
	andl	$16711935, %esi
	roll	$16, %r8d
	orl	%r13d, %esi
	movl	%edi, %r13d
	shrl	$24, %r13d
	xorl	%r8d, %esi
	xorl	%r14d, %r13d
	movl	%edi, %r14d
	shrl	$8, %edi
	xorl	%esi, %r12d
	sall	$8, %r14d
	andl	$65280, %edi
	movl	%r12d, -16(%r9)
	andl	$16711680, %r14d
	xorl	%r14d, %r13d
	xorl	%r13d, %edi
	xorl	%edi, %r8d
	xorl	%esi, %edi
	xorl	%r12d, %r8d
	movl	%esi, %r12d
	movl	%ecx, %esi
	xorl	%ebx, %ecx
	xorl	%r8d, %r12d
	xorl	%eax, %esi
	movl	%r8d, -8(%r9)
	movl	%r12d, -12(%r9)
	movl	%edi, -4(%r9)
	movl	%r11d, 16(%r10)
	movl	%esi, 20(%r10)
	movl	%eax, 24(%r10)
	movl	%ecx, 28(%r10)
	cmpq	%r10, %r9
	jb	.L33
	movq	-64(%rbp), %rax
	subq	%rdx, %rax
	subq	$33, %rax
	shrq	$5, %rax
	addq	$1, %rax
	movq	%rax, %rdx
	salq	$4, %rdx
	subq	%rdx, -80(%rbp)
	addq	%rdx, %r15
.L32:
	movl	(%r15), %eax
	movl	12(%r15), %esi
	movq	-80(%rbp), %rbx
	movl	%eax, %edx
	rorl	$8, %edx
	xorl	%edx, %eax
	movl	%eax, %edi
	movl	4(%r15), %eax
	roll	$16, %edi
	movl	%eax, %r8d
	xorl	%edx, %edi
	movl	8(%r15), %edx
	rorl	$8, %r8d
	xorl	%r8d, %eax
	movl	%edx, %ecx
	roll	$16, %eax
	rorl	$8, %ecx
	xorl	%eax, %r8d
	movl	%edx, %eax
	movl	%esi, %edx
	xorl	%ecx, %eax
	rorl	$8, %edx
	roll	$16, %eax
	xorl	%ecx, %eax
	movl	%esi, %ecx
	xorl	%edx, %ecx
	roll	$16, %ecx
	xorl	%edx, %ecx
	movl	%r8d, %edx
	xorl	%eax, %edx
	xorl	%ecx, %eax
	xorl	%edx, %edi
	xorl	%edx, %ecx
	xorl	%edi, %eax
	movl	%ecx, %r8d
	xorl	%eax, %edx
	shrl	$24, %r8d
	movl	%edx, %esi
	shrl	$8, %edx
	sall	$8, %esi
	andl	$16711935, %edx
	roll	$16, %eax
	andl	$-16711936, %esi
	orl	%esi, %edx
	movl	%ecx, %esi
	sall	$24, %esi
	xorl	%r8d, %esi
	movl	%ecx, %r8d
	sall	$8, %r8d
	andl	$16711680, %r8d
	shrl	$8, %ecx
	xorl	%eax, %edx
	xorl	%r8d, %esi
	andl	$65280, %ecx
	xorl	%edx, %edi
	xorl	%esi, %ecx
	movl	%edx, %esi
	movl	%edi, (%rbx)
	xorl	%ecx, %eax
	xorl	%edx, %ecx
	xorl	%edi, %eax
	movl	%ecx, 12(%rbx)
	xorl	%eax, %esi
	movl	%eax, 8(%rbx)
	movl	%esi, 4(%rbx)
	jmp	.L30
	.cfi_endproc
.LFE17:
	.size	aria_set_decrypt_key, .-aria_set_decrypt_key
	.section	.rodata
	.align 32
	.type	X2, @object
	.size	X2, 1024
X2:
	.long	808464384
	.long	1751672832
	.long	-1717987072
	.long	454761216
	.long	-2021161216
	.long	-1179010816
	.long	555819264
	.long	2021160960
	.long	1347440640
	.long	960051456
	.long	-606348544
	.long	-505290496
	.long	1920102912
	.long	151587072
	.long	1650614784
	.long	1010580480
	.long	1044266496
	.long	2122219008
	.long	1583242752
	.long	-1903260160
	.long	-235802368
	.long	-1600086016
	.long	-858993664
	.long	-1549556992
	.long	707406336
	.long	488447232
	.long	-67372288
	.long	-1229539840
	.long	-690563584
	.long	538976256
	.long	-993737728
	.long	-1920103168
	.long	-2122219264
	.long	1701143808
	.long	-168430336
	.long	-1987475200
	.long	-875836672
	.long	-1650615040
	.long	2004317952
	.long	-960051712
	.long	1465341696
	.long	1128481536
	.long	1448498688
	.long	387389184
	.long	-724249600
	.long	1077952512
	.long	437918208
	.long	1296911616
	.long	-1061109760
	.long	1667457792
	.long	1819044864
	.long	-471604480
	.long	-1212696832
	.long	-926365696
	.long	1684300800
	.long	1785358848
	.long	1397969664
	.long	-1431655936
	.long	943208448
	.long	-1734830080
	.long	202116096
	.long	-185273344
	.long	-1684301056
	.long	-303174400
	.long	2139062016
	.long	572662272
	.long	1987474944
	.long	-1347440896
	.long	-572662528
	.long	976894464
	.long	185273088
	.long	1482184704
	.long	1734829824
	.long	-2004318208
	.long	101058048
	.long	-1010580736
	.long	892679424
	.long	218959104
	.long	16843008
	.long	-1953789184
	.long	-1936946176
	.long	-1027423744
	.long	-421075456
	.long	1600085760
	.long	33686016
	.long	606348288
	.long	1970631936
	.long	-1819045120
	.long	1717986816
	.long	505290240
	.long	-437918464
	.long	-488447488
	.long	1414812672
	.long	-656877568
	.long	269488128
	.long	-825307648
	.long	2054846976
	.long	-387389440
	.long	134744064
	.long	741092352
	.long	303174144
	.long	-1751673088
	.long	842150400
	.long	-1414812928
	.long	-1263225856
	.long	656877312
	.long	168430080
	.long	589505280
	.long	-538976512
	.long	-269488384
	.long	-892679680
	.long	-640034560
	.long	-1195853824
	.long	-84215296
	.long	-589505536
	.long	825307392
	.long	1802201856
	.long	-774778624
	.long	-1381126912
	.long	421075200
	.long	1229539584
	.long	-1111638784
	.long	1364283648
	.long	-1768516096
	.long	-286331392
	.long	-454761472
	.long	-1465341952
	.long	1094795520
	.long	-623191552
	.long	-256
	.long	-842150656
	.long	1431655680
	.long	-2038004224
	.long	909522432
	.long	-1094795776
	.long	1633771776
	.long	1381126656
	.long	-117901312
	.long	-1145324800
	.long	235802112
	.long	-2105376256
	.long	1212696576
	.long	1768515840
	.long	-1701144064
	.long	-522133504
	.long	1195853568
	.long	-1633772032
	.long	1549556736
	.long	67372032
	.long	1263225600
	.long	875836416
	.long	353703168
	.long	2038003968
	.long	640034304
	.long	-1482184960
	.long	-555819520
	.long	690563328
	.long	-1364283904
	.long	-1835888128
	.long	-673720576
	.long	-2071690240
	.long	-370546432
	.long	-757935616
	.long	-1162167808
	.long	1566399744
	.long	-202116352
	.long	-976894720
	.long	-1330597888
	.long	-1077952768
	.long	-1532713984
	.long	993737472
	.long	1903259904
	.long	1145324544
	.long	1179010560
	.long	724249344
	.long	-50529280
	.long	-336860416
	.long	1869573888
	.long	-707406592
	.long	-151587328
	.long	336860160
	.long	-16843264
	.long	2088532992
	.long	1886416896
	.long	1515870720
	.long	2105376000
	.long	-33686272
	.long	791621376
	.long	404232192
	.long	-2088533248
	.long	370546176
	.long	-1515870976
	.long	-1852731136
	.long	522133248
	.long	84215040
	.long	-1785359104
	.long	1953788928
	.long	-1448498944
	.long	-1044266752
	.long	1532713728
	.long	1246382592
	.long	-2054847232
	.long	1835887872
	.long	320017152
	.long	117901056
	.long	1330597632
	.long	1313754624
	.long	1162167552
	.long	-1296911872
	.long	252645120
	.long	-909522688
	.long	471604224
	.long	-1499027968
	.long	-1128481792
	.long	-320017408
	.long	1936945920
	.long	-1869574144
	.long	2071689984
	.long	-808464640
	.long	1499027712
	.long	-1886417152
	.long	-1583243008
	.long	-101058304
	.long	757935360
	.long	-218959360
	.long	-1313754880
	.long	0
	.long	-1802202112
	.long	926365440
	.long	-1616929024
	.long	-791621632
	.long	774778368
	.long	-1667458048
	.long	1852730880
	.long	673720320
	.long	1061109504
	.long	-2139062272
	.long	-252645376
	.long	1027423488
	.long	-741092608
	.long	623191296
	.long	-1970632192
	.long	-1246382848
	.long	-404232448
	.long	1111638528
	.long	-1280068864
	.long	-943208704
	.long	-353703424
	.long	-134744320
	.long	1280068608
	.long	286331136
	.long	858993408
	.long	50529024
	.long	-1566400000
	.long	-1397969920
	.long	1616928768
	.align 32
	.type	X1, @object
	.size	X1, 1024
X1:
	.long	1381105746
	.long	151584777
	.long	1785331818
	.long	-707460907
	.long	808452144
	.long	909508662
	.long	-1515913051
	.long	943194168
	.long	-1078001473
	.long	1077936192
	.long	-1549598557
	.long	-1633812322
	.long	-2122252159
	.long	-202178317
	.long	-673775401
	.long	-67436293
	.long	2088501372
	.long	-471662365
	.long	960036921
	.long	-2105409406
	.long	-1684340581
	.long	791609391
	.long	-65281
	.long	-2021195641
	.long	875823156
	.long	-1903296370
	.long	1128464451
	.long	1145307204
	.long	-993787708
	.long	-555876130
	.long	-370605847
	.long	-875888437
	.long	1414791252
	.long	2071658619
	.long	-1802239852
	.long	842137650
	.long	-1499070298
	.long	-1027473214
	.long	589496355
	.long	1027407933
	.long	-286392082
	.long	1280049228
	.long	-1785397099
	.long	185270283
	.long	1111621698
	.long	-84279046
	.long	-1010630461
	.long	1313734734
	.long	134742024
	.long	774766638
	.long	-1583284063
	.long	1717960806
	.long	673710120
	.long	-640089895
	.long	606339108
	.long	-1296957262
	.long	1987444854
	.long	1532690523
	.long	-1566441310
	.long	1229520969
	.long	1835860077
	.long	-1953824629
	.long	-774831919
	.long	623181861
	.long	1920073842
	.long	-117964552
	.long	-151650058
	.long	1684275300
	.long	-2038038394
	.long	1751646312
	.long	-1734868840
	.long	370540566
	.long	-724303660
	.long	-1532755804
	.long	1549533276
	.long	-859045684
	.long	1566376029
	.long	1701118053
	.long	-1229586250
	.long	-1835925358
	.long	1819017324
	.long	1886388336
	.long	1212678216
	.long	1347420240
	.long	-33750787
	.long	-303234835
	.long	-1179057991
	.long	-623247142
	.long	1583218782
	.long	353697813
	.long	1178992710
	.long	1465319511
	.long	-1482227545
	.long	-1920139123
	.long	-1650655075
	.long	-2071723900
	.long	-1869610864
	.long	-656932648
	.long	-1414856533
	.long	0
	.long	-1936981876
	.long	-1128529732
	.long	-741146413
	.long	168427530
	.long	-134807305
	.long	-454819612
	.long	1482162264
	.long	84213765
	.long	-1195900744
	.long	-1280114509
	.long	1162149957
	.long	101056518
	.long	-791674672
	.long	741081132
	.long	505282590
	.long	-1886453617
	.long	-892731190
	.long	1061093439
	.long	252641295
	.long	33685506
	.long	-1044315967
	.long	-1347485521
	.long	-1111686979
	.long	50528259
	.long	16842753
	.long	320012307
	.long	-1970667382
	.long	1802174571
	.long	976879674
	.long	-1852768111
	.long	286326801
	.long	1094778945
	.long	1330577487
	.long	1734803559
	.long	-589561636
	.long	-353763094
	.long	-1751711593
	.long	-219021070
	.long	-808517425
	.long	-825360178
	.long	-252706576
	.long	-1263271756
	.long	-421134106
	.long	1936916595
	.long	-1768554346
	.long	-1398013780
	.long	1953759348
	.long	572653602
	.long	-404291353
	.long	-1381171027
	.long	892665909
	.long	-2054881147
	.long	-488505118
	.long	-101121799
	.long	926351415
	.long	-387448600
	.long	471597084
	.long	1970602101
	.long	-539033377
	.long	1852702830
	.long	1195835463
	.long	-235863823
	.long	437911578
	.long	1903231089
	.long	488439837
	.long	690552873
	.long	-976944955
	.long	-1987510135
	.long	1869545583
	.long	-1212743497
	.long	1650589794
	.long	235798542
	.long	-1431699286
	.long	404226072
	.long	-1094844226
	.long	454754331
	.long	-50593540
	.long	1448476758
	.long	1044250686
	.long	1263206475
	.long	-960102202
	.long	-757989166
	.long	2037973113
	.long	538968096
	.long	-1701183334
	.long	-606404389
	.long	-1061158720
	.long	-16908034
	.long	2021130360
	.long	-842202931
	.long	1515847770
	.long	-185335564
	.long	522125343
	.long	-572718883
	.long	-1465384792
	.long	858980403
	.long	-2004352888
	.long	117899271
	.long	-943259449
	.long	825294897
	.long	-1313800015
	.long	303169554
	.long	269484048
	.long	1499005017
	.long	656867367
	.long	-2139094912
	.long	-320077588
	.long	1600061535
	.long	1616904288
	.long	1364262993
	.long	2139029631
	.long	-1448542039
	.long	421068825
	.long	-1246429003
	.long	1246363722
	.long	218955789
	.long	757923885
	.long	-437976859
	.long	2054815866
	.long	-1616969569
	.long	-1819082605
	.long	-909573943
	.long	-1667497828
	.long	-269549329
	.long	-1600126816
	.long	-522190624
	.long	993722427
	.long	1296891981
	.long	-1364328274
	.long	707395626
	.long	-168492811
	.long	-1330642768
	.long	-926416696
	.long	-336920341
	.long	-1145372485
	.long	1010565180
	.long	-2088566653
	.long	1397948499
	.long	-1718026087
	.long	1633747041
	.long	387383319
	.long	724238379
	.long	67371012
	.long	2122186878
	.long	-1162215238
	.long	2004287607
	.long	-690618154
	.long	640024614
	.long	-505347871
	.long	1768489065
	.long	336855060
	.long	1667432547
	.long	1431634005
	.long	555810849
	.long	202113036
	.long	2105344125
	.align 32
	.type	S2, @object
	.size	S2, 1024
S2:
	.long	-503258398
	.long	1308642894
	.long	1409307732
	.long	-67044100
	.long	-1811901292
	.long	-1040137534
	.long	1241533002
	.long	-872362804
	.long	1644192354
	.long	218107149
	.long	1778412138
	.long	1174423110
	.long	1006648380
	.long	1291865421
	.long	-1962898549
	.long	-788475439
	.long	1577082462
	.long	-100599046
	.long	1677747300
	.long	-889140277
	.long	-1275022156
	.long	-1761568873
	.long	-1107247426
	.long	721431339
	.long	-1140802372
	.long	1996519287
	.long	771763758
	.long	50332419
	.long	-754920493
	.long	419436825
	.long	1493195097
	.long	-1056915007
	.long	486546717
	.long	100664838
	.long	1090535745
	.long	1795189611
	.long	1426085205
	.long	-268373776
	.long	-1728013927
	.long	1761634665
	.long	-369038614
	.long	-1677681508
	.long	402659352
	.long	-1375686994
	.long	1660969827
	.long	-553590817
	.long	-419371033
	.long	-1157579845
	.long	0
	.long	1929409395
	.long	1711302246
	.long	-83821573
	.long	-1778346346
	.long	1275087948
	.long	-2063563387
	.long	-469703452
	.long	973093434
	.long	150997257
	.long	1157645637
	.long	-1442796886
	.long	251662095
	.long	-301928722
	.long	268439568
	.long	-352261141
	.long	754986285
	.long	2130739071
	.long	-201263884
	.long	687876393
	.long	-1409241940
	.long	-822030385
	.long	-1392464467
	.long	-1862233711
	.long	-1929343603
	.long	2013296760
	.long	-939472696
	.long	-1795123819
	.long	-117376519
	.long	788541231
	.long	-838807858
	.long	-855585331
	.long	134219784
	.long	2046851706
	.long	-2013230968
	.long	939538488
	.long	1543527516
	.long	-2097118333
	.long	704653866
	.long	671098920
	.long	1191200583
	.long	-620700709
	.long	-1207912264
	.long	-956250169
	.long	-1828678765
	.long	-1543461724
	.long	301994514
	.long	1392530259
	.long	-16711681
	.long	-2030008441
	.long	234884622
	.long	822096177
	.long	905983542
	.long	553656609
	.long	1476417624
	.long	1207978056
	.long	16777473
	.long	-1912566130
	.long	922761015
	.long	1946186868
	.long	838873650
	.long	-905917750
	.long	-385816087
	.long	-1325354575
	.long	-1224689737
	.long	-1426019413
	.long	201329676
	.long	-687810601
	.long	-1006582588
	.long	1442862678
	.long	1107313218
	.long	637543974
	.long	117442311
	.long	-1744791400
	.long	1610637408
	.long	-654255655
	.long	-1241467210
	.long	-1191134791
	.long	285217041
	.long	1073758272
	.long	-335483668
	.long	536879136
	.long	-1946121076
	.long	-1124024899
	.long	-1610571616
	.long	-922695223
	.long	-2080340860
	.long	67109892
	.long	1224755529
	.long	587211555
	.long	-251596303
	.long	1325420367
	.long	1342197840
	.long	520101663
	.long	318771987
	.long	-603923236
	.long	-671033128
	.long	-1073692480
	.long	-1644126562
	.long	1459640151
	.long	-486480925
	.long	-1023360061
	.long	2063629179
	.long	1694524773
	.long	989870907
	.long	33554946
	.long	-1895788657
	.long	1040203326
	.long	-402593560
	.long	620766501
	.long	-1845456238
	.long	-452925979
	.long	352326933
	.long	-587145763
	.long	-50266627
	.long	385881879
	.long	-1459574359
	.long	-1090469953
	.long	-738143020
	.long	-1711236454
	.long	2113961598
	.long	-989805115
	.long	956315961
	.long	1728079719
	.long	-33489154
	.long	1979741814
	.long	-1660904035
	.long	1124090691
	.long	-1493129305
	.long	-520035871
	.long	-805252912
	.long	-184486411
	.long	1744857192
	.long	-234818830
	.long	452991771
	.long	872428596
	.long	1879076976
	.long	83887365
	.long	-1560239197
	.long	-1979676022
	.long	-721365547
	.long	2030074233
	.long	-2046785914
	.long	-1476351832
	.long	805318704
	.long	-973027642
	.long	1358975313
	.long	1258310475
	.long	503324190
	.long	-1509906778
	.long	654321447
	.long	-167708938
	.long	889206069
	.long	-771697966
	.long	1845522030
	.long	603989028
	.long	369104406
	.long	-2113895806
	.long	1593859935
	.long	-637478182
	.long	-436148506
	.long	1962964341
	.long	-1577016670
	.long	-285151249
	.long	738208812
	.long	-1308577102
	.long	469769244
	.long	-1627349089
	.long	1560304989
	.long	1862299503
	.long	-2147450752
	.long	167774730
	.long	1912631922
	.long	1140868164
	.long	-1694458981
	.long	1811967084
	.long	-1879011184
	.long	184552203
	.long	1526750043
	.long	855651123
	.long	2097184125
	.long	1509972570
	.long	1375752786
	.long	-218041357
	.long	1627414881
	.long	-1593794143
	.long	-150931465
	.long	-1342132048
	.long	-704588074
	.long	1056980799
	.long	2080406652
	.long	1828744557
	.long	-318706195
	.long	335549460
	.long	-536813344
	.long	-1526684251
	.long	1023425853
	.long	570434082
	.long	-1291799629
	.long	-134153992
	.long	-1996453495
	.long	-570368290
	.long	1895854449
	.long	436214298
	.long	-1358909521
	.long	-1174357318
	.long	-1258244683
	.long	-2130673279
	.align 32
	.type	S1, @object
	.size	S1, 1024
S1:
	.long	6513507
	.long	8158332
	.long	7829367
	.long	8092539
	.long	15921906
	.long	7039851
	.long	7303023
	.long	12961221
	.long	3158064
	.long	65793
	.long	6776679
	.long	2829099
	.long	16711422
	.long	14145495
	.long	11250603
	.long	7763574
	.long	13290186
	.long	8553090
	.long	13224393
	.long	8224125
	.long	16448250
	.long	5855577
	.long	4671303
	.long	15790320
	.long	11382189
	.long	13948116
	.long	10658466
	.long	11513775
	.long	10263708
	.long	10790052
	.long	7500402
	.long	12632256
	.long	12040119
	.long	16645629
	.long	9671571
	.long	2500134
	.long	3552822
	.long	4144959
	.long	16250871
	.long	13421772
	.long	3421236
	.long	10855845
	.long	15066597
	.long	15856113
	.long	7434609
	.long	14211288
	.long	3223857
	.long	1381653
	.long	263172
	.long	13092807
	.long	2302755
	.long	12829635
	.long	1579032
	.long	9868950
	.long	328965
	.long	10132122
	.long	460551
	.long	1184274
	.long	8421504
	.long	14869218
	.long	15461355
	.long	2565927
	.long	11711154
	.long	7697781
	.long	592137
	.long	8618883
	.long	2894892
	.long	1710618
	.long	1776411
	.long	7237230
	.long	5921370
	.long	10526880
	.long	5395026
	.long	3881787
	.long	14079702
	.long	11776947
	.long	2697513
	.long	14935011
	.long	3092271
	.long	8684676
	.long	5460819
	.long	13750737
	.long	0
	.long	15592941
	.long	2105376
	.long	16579836
	.long	11645361
	.long	5987163
	.long	6974058
	.long	13355979
	.long	12500670
	.long	3750201
	.long	4868682
	.long	5000268
	.long	5789784
	.long	13619151
	.long	13684944
	.long	15724527
	.long	11184810
	.long	16514043
	.long	4408131
	.long	5066061
	.long	3355443
	.long	8750469
	.long	4539717
	.long	16382457
	.long	131586
	.long	8355711
	.long	5263440
	.long	3947580
	.long	10461087
	.long	11053224
	.long	5329233
	.long	10724259
	.long	4210752
	.long	9408399
	.long	9605778
	.long	10329501
	.long	3684408
	.long	16119285
	.long	12369084
	.long	11974326
	.long	14342874
	.long	2171169
	.long	1052688
	.long	16777215
	.long	15987699
	.long	13816530
	.long	13487565
	.long	789516
	.long	1250067
	.long	15527148
	.long	6250335
	.long	9934743
	.long	4473924
	.long	1513239
	.long	12895428
	.long	10987431
	.long	8289918
	.long	4013373
	.long	6579300
	.long	6118749
	.long	1644825
	.long	7566195
	.long	6316128
	.long	8487297
	.long	5197647
	.long	14474460
	.long	2236962
	.long	2763306
	.long	9474192
	.long	8947848
	.long	4605510
	.long	15658734
	.long	12105912
	.long	1315860
	.long	14606046
	.long	6184542
	.long	723723
	.long	14408667
	.long	14737632
	.long	3289650
	.long	3815994
	.long	657930
	.long	4802889
	.long	394758
	.long	2368548
	.long	6052956
	.long	12763842
	.long	13882323
	.long	11316396
	.long	6447714
	.long	9539985
	.long	9803157
	.long	15000804
	.long	7960953
	.long	15198183
	.long	13158600
	.long	3618615
	.long	7171437
	.long	9276813
	.long	14013909
	.long	5131854
	.long	11119017
	.long	7105644
	.long	5658198
	.long	16053492
	.long	15395562
	.long	6645093
	.long	8026746
	.long	11447982
	.long	526344
	.long	12237498
	.long	7895160
	.long	2434341
	.long	3026478
	.long	1842204
	.long	10921638
	.long	11842740
	.long	13027014
	.long	15263976
	.long	14540253
	.long	7631988
	.long	2039583
	.long	4934475
	.long	12434877
	.long	9145227
	.long	9079434
	.long	7368816
	.long	4079166
	.long	11908533
	.long	6710886
	.long	4737096
	.long	197379
	.long	16185078
	.long	921102
	.long	6381921
	.long	3487029
	.long	5723991
	.long	12171705
	.long	8816262
	.long	12698049
	.long	1907997
	.long	10395294
	.long	14803425
	.long	16316664
	.long	10000536
	.long	1118481
	.long	6908265
	.long	14277081
	.long	9342606
	.long	9737364
	.long	10197915
	.long	1973790
	.long	8882055
	.long	15329769
	.long	13553358
	.long	5592405
	.long	2631720
	.long	14671839
	.long	9211020
	.long	10592673
	.long	9013641
	.long	855309
	.long	12566463
	.long	15132390
	.long	4342338
	.long	6842472
	.long	4276545
	.long	10066329
	.long	2960685
	.long	986895
	.long	11579568
	.long	5526612
	.long	12303291
	.long	1447446
	.align 32
	.type	Key_RC, @object
	.size	Key_RC, 80
Key_RC:
	.long	1367130551
	.long	656542356
	.long	-32265240
	.long	-90542368
	.long	1840335564
	.long	-1641953248
	.long	-14110251
	.long	-279059792
	.long	-611174627
	.long	556198256
	.long	52729717
	.long	82364686
	.long	1367130551
	.long	656542356
	.long	-32265240
	.long	-90542368
	.long	1840335564
	.long	-1641953248
	.long	-14110251
	.long	-279059792
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
