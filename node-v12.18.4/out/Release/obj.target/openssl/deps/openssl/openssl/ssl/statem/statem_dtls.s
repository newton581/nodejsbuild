	.file	"statem_dtls.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/ssl/statem/statem_dtls.c"
	.text
	.p2align 4
	.type	dtls1_hm_fragment_new, @function
dtls1_hm_fragment_new:
.LFB1001:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$62, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	leaq	.LC0(%rip), %rsi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movl	$104, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L12
	xorl	%r14d, %r14d
	testq	%rbx, %rbx
	jne	.L13
.L4:
	movq	%r14, 88(%r12)
	xorl	%eax, %eax
	testl	%r13d, %r13d
	jne	.L14
.L5:
	movq	%rax, 96(%r12)
.L1:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	movl	$68, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rbx, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L4
	movl	$69, %r8d
	movl	$65, %edx
	movl	$623, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movl	$70, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L14:
	leaq	7(%rbx), %rdi
	movl	$80, %edx
	leaq	.LC0(%rip), %rsi
	shrq	$3, %rdi
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	jne	.L5
	movl	$82, %r8d
	movl	$65, %edx
	movl	$623, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$83, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movl	$84, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$63, %r8d
	movl	$65, %edx
	movl	$623, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1
	.cfi_endproc
.LFE1001:
	.size	dtls1_hm_fragment_new, .-dtls1_hm_fragment_new
	.p2align 4
	.type	dtls1_preprocess_fragment, @function
dtls1_preprocess_fragment:
.LFB1006:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	8(%rsi), %r13
	movq	32(%rsi), %rax
	addq	24(%rsi), %rax
	cmpq	%r13, %rax
	ja	.L16
	cmpq	$16716, 1512(%rdi)
	movl	$16716, %eax
	cmovnb	1512(%rdi), %rax
	cmpq	%rax, %r13
	ja	.L16
	movq	176(%rdi), %rdx
	cmpq	$0, 424(%rdx)
	je	.L24
	movl	$1, %eax
	cmpq	%r13, 408(%rdx)
	jne	.L25
.L15:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movl	$429, %r9d
.L23:
	movq	%r12, %rdi
	movl	$152, %ecx
	movl	$288, %edx
	movl	$47, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movq	136(%rdi), %rdi
	movq	%rsi, %rbx
	leaq	12(%r13), %rsi
	call	BUF_MEM_grow_clean@PLT
	testq	%rax, %rax
	je	.L26
	movq	168(%r12), %rcx
	movq	176(%r12), %rax
	movq	%r13, 552(%rcx)
	movq	%r13, 408(%rax)
	movzbl	(%rbx), %esi
	movl	%esi, 560(%rcx)
	movb	%sil, 400(%rax)
	movzwl	16(%rbx), %edx
	movw	%dx, 416(%rax)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	movl	$455, %r9d
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L26:
	movl	$440, %r9d
	leaq	.LC0(%rip), %r8
	movl	$7, %ecx
	movq	%r12, %rdi
	movl	$288, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L15
	.cfi_endproc
.LFE1006:
	.size	dtls1_preprocess_fragment, .-dtls1_preprocess_fragment
	.p2align 4
	.type	dtls1_reassemble_fragment, @function
dtls1_reassemble_fragment:
.LFB1008:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$312, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	32(%rsi), %r12
	movq	24(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	addq	%r12, %rdx
	cmpq	%rax, %rdx
	ja	.L74
	cmpq	$16716, 1512(%rdi)
	movl	$16716, %edx
	cmovnb	1512(%rdi), %rdx
	movq	%rdi, %rbx
	cmpq	%rdx, %rax
	ja	.L74
	testq	%r12, %r12
	je	.L37
	movzwl	16(%rsi), %eax
	leaq	-328(%rbp), %r13
	xorl	%r8d, %r8d
	movq	%rsi, %r14
	movq	%r13, %rsi
	movw	%r8w, -324(%rbp)
	movl	$0, -328(%rbp)
	rolw	$8, %ax
	movw	%ax, -322(%rbp)
	movq	176(%rdi), %rax
	movq	280(%rax), %rdi
	call	pqueue_find@PLT
	movq	%rax, -344(%rbp)
	testq	%rax, %rax
	je	.L75
	movq	8(%rax), %r15
	movq	8(%r14), %rax
	cmpq	%rax, 8(%r15)
	jne	.L74
.L33:
	movq	8(%rbx), %rax
	cmpq	$0, 96(%r15)
	movq	104(%rax), %rax
	je	.L76
	subq	$8, %rsp
	movq	24(%r14), %rcx
	xorl	%r9d, %r9d
	addq	88(%r15), %rcx
	leaq	-336(%rbp), %rdx
	movq	%r12, %r8
	movl	$22, %esi
	movq	%rbx, %rdi
	pushq	%rdx
	xorl	%edx, %edx
	call	*%rax
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	jle	.L36
	cmpq	%r12, -336(%rbp)
	jne	.L36
	movq	24(%r14), %rax
	cmpq	$8, %r12
	jg	.L40
	leaq	(%r12,%rax), %rdx
	movl	$1, %esi
	cmpq	%rdx, %rax
	jge	.L42
	.p2align 4,,10
	.p2align 3
.L41:
	movq	%rax, %rdx
	movl	%eax, %ecx
	movl	%esi, %edi
	addq	$1, %rax
	sarq	$3, %rdx
	andl	$7, %ecx
	addq	96(%r15), %rdx
	sall	%cl, %edi
	orb	%dil, (%rdx)
	movq	24(%r14), %rdx
	addq	%r12, %rdx
	cmpq	%rax, %rdx
	jg	.L41
.L42:
	movq	8(%r14), %rdx
	testq	%rdx, %rdx
	je	.L36
	leaq	-1(%rdx), %rax
	movq	96(%r15), %rdi
	andl	$7, %edx
	leaq	bitmask_end_values(%rip), %rcx
	sarq	$3, %rax
	movzbl	(%rcx,%rdx), %ecx
	cmpb	%cl, (%rdi,%rax)
	je	.L77
.L46:
	cmpq	$0, -344(%rbp)
	jne	.L37
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	pitem_new@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L50
	movq	176(%rbx), %rax
	movq	280(%rax), %rdi
	call	pqueue_insert@PLT
	testq	%rax, %rax
	je	.L50
	.p2align 4,,10
	.p2align 3
.L37:
	movl	$-3, %eax
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L36:
	cmpq	$0, -344(%rbp)
	jne	.L74
.L50:
	movl	40(%r15), %eax
	testl	%eax, %eax
	jne	.L78
.L52:
	movq	88(%r15), %rdi
	movl	$103, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	96(%r15), %rdi
	movl	$104, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$105, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	call	CRYPTO_free@PLT
.L74:
	movl	$-1, %eax
.L27:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L79
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	movq	48(%r15), %rdi
	call	EVP_CIPHER_CTX_free@PLT
	movq	56(%r15), %rdi
	call	EVP_MD_CTX_free@PLT
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L75:
	movq	8(%r14), %rdi
	movl	$1, %esi
	call	dtls1_hm_fragment_new
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L74
	movdqa	(%r14), %xmm0
	movups	%xmm0, (%rax)
	movdqa	16(%r14), %xmm1
	movups	%xmm1, 16(%rax)
	movdqa	32(%r14), %xmm2
	movups	%xmm2, 32(%rax)
	movdqa	48(%r14), %xmm3
	movups	%xmm3, 48(%rax)
	movdqa	64(%r14), %xmm4
	movups	%xmm4, 64(%rax)
	movq	80(%r14), %rax
	movq	$0, 24(%r15)
	movq	%rax, 80(%r15)
	movq	8(%r15), %rax
	movq	%rax, 32(%r15)
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L76:
	leaq	-320(%rbp), %r13
	leaq	-336(%rbp), %r14
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L80:
	movq	8(%rbx), %rax
	movq	104(%rax), %rax
.L38:
	cmpq	$256, %r12
	movl	$256, %r8d
	movq	%rbx, %rdi
	movq	%r13, %rcx
	cmovbe	%r12, %r8
	subq	$8, %rsp
	movl	$22, %esi
	xorl	%r9d, %r9d
	pushq	%r14
	xorl	%edx, %edx
	call	*%rax
	popq	%rsi
	popq	%rdi
	testl	%eax, %eax
	jle	.L36
	subq	-336(%rbp), %r12
	jne	.L80
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L40:
	movq	%rax, %rdx
	leaq	bitmask_start_values(%rip), %rcx
	andl	$7, %eax
	sarq	$3, %rdx
	addq	96(%r15), %rdx
	movzbl	(%rcx,%rax), %eax
	orb	%al, (%rdx)
	movq	24(%r14), %rcx
	movq	%rcx, %rdx
	addq	%r12, %rcx
	sarq	$3, %rdx
	leaq	-1(%rcx), %rax
	addq	$1, %rdx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jge	.L44
	.p2align 4,,10
	.p2align 3
.L45:
	movq	96(%r15), %rax
	movb	$-1, (%rax,%rdx)
	movq	24(%r14), %rcx
	addq	$1, %rdx
	addq	%r12, %rcx
	leaq	-1(%rcx), %rax
	sarq	$3, %rax
	cmpq	%rdx, %rax
	jg	.L45
.L44:
	addq	96(%r15), %rax
	andl	$7, %ecx
	leaq	bitmask_end_values(%rip), %rdx
	movzbl	(%rdx,%rcx), %edx
	orb	%dl, (%rax)
	jmp	.L42
.L77:
	subq	$1, %rax
	js	.L47
	.p2align 4,,10
	.p2align 3
.L48:
	cmpb	$-1, (%rdi,%rax)
	jne	.L46
	subq	$1, %rax
	cmpq	$-1, %rax
	jne	.L48
.L47:
	movl	$609, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 96(%r15)
	jmp	.L46
.L79:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1008:
	.size	dtls1_reassemble_fragment, .-dtls1_reassemble_fragment
	.p2align 4
	.type	dtls_get_reassembled_message, @function
dtls_get_reassembled_message:
.LFB1010:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	.LC0(%rip), %r12
	pushq	%rbx
	subq	$472, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -504(%rbp)
	movq	%rdx, -496(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, (%rsi)
	movq	176(%rdi), %rax
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L85:
	movq	88(%r14), %rdi
	movl	$103, %edx
	movq	%r12, %rsi
	call	CRYPTO_free@PLT
	movq	96(%r14), %rdi
	movl	$104, %edx
	movq	%r12, %rsi
	call	CRYPTO_free@PLT
	movl	$105, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	movq	%r13, %rdi
	call	pitem_free@PLT
	movq	176(%r15), %rax
.L86:
	movq	280(%rax), %rdi
	call	pqueue_peek@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L83
	movq	8(%rax), %r14
	movq	176(%r15), %rax
	movzwl	16(%r14), %ecx
	movzwl	272(%rax), %edx
	cmpw	%dx, %cx
	jnb	.L84
	movq	280(%rax), %rdi
	call	pqueue_pop@PLT
	movl	40(%r14), %ebx
	testl	%ebx, %ebx
	je	.L85
	movq	48(%r14), %rdi
	call	EVP_CIPHER_CTX_free@PLT
	movq	56(%r14), %rdi
	call	EVP_MD_CTX_free@PLT
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L84:
	cmpq	$0, 96(%r14)
	jne	.L83
	cmpw	%dx, %cx
	je	.L180
.L83:
	movq	8(%r15), %rax
	subq	$8, %rsp
	xorl	%r9d, %r9d
	movq	%r15, %rdi
	leaq	-448(%rbp), %rsi
	leaq	-332(%rbp), %r14
	movl	$12, %r8d
	pushq	%rsi
	movq	%r14, %rcx
	leaq	-452(%rbp), %rdx
	movq	%rsi, -480(%rbp)
	movl	$22, %esi
	call	*104(%rax)
	popq	%rcx
	popq	%rsi
	testl	%eax, %eax
	jle	.L117
	cmpl	$20, -452(%rbp)
	je	.L181
	cmpq	$12, -448(%rbp)
	jne	.L182
	xorl	%eax, %eax
	leaq	-432(%rbp), %rdi
	movl	$11, %ecx
	movzbl	-331(%rbp), %edx
	movq	%rdi, -488(%rbp)
	movzbl	-326(%rbp), %ebx
	rep stosq
	movzbl	-332(%rbp), %eax
	salq	$16, %rdx
	movzbl	-323(%rbp), %r8d
	salq	$16, %rbx
	leaq	2112(%r15), %rdi
	movb	%al, -432(%rbp)
	movzbl	-330(%rbp), %eax
	salq	$16, %r8
	salq	$8, %rax
	orq	%rax, %rdx
	movzbl	-329(%rbp), %eax
	orq	%rax, %rdx
	movzwl	-328(%rbp), %eax
	movq	%rdx, -424(%rbp)
	rolw	$8, %ax
	movq	%rdx, -472(%rbp)
	movw	%ax, -416(%rbp)
	movzbl	-325(%rbp), %eax
	salq	$8, %rax
	orq	%rax, %rbx
	movzbl	-324(%rbp), %eax
	orq	%rax, %rbx
	movzbl	-322(%rbp), %eax
	movq	%rbx, -408(%rbp)
	salq	$8, %rax
	orq	%rax, %r8
	movzbl	-321(%rbp), %eax
	orq	%rax, %r8
	movq	%r8, %r13
	movq	%r8, -400(%rbp)
	call	RECORD_LAYER_get_rrec_length@PLT
	movq	-472(%rbp), %rdx
	cmpq	%r13, %rax
	jb	.L183
	movzwl	-416(%rbp), %ecx
	movq	176(%r15), %rax
	movl	%ecx, %esi
	rolw	$8, %si
	cmpw	272(%rax), %cx
	jne	.L184
	cmpq	%r13, %rdx
	jbe	.L112
	testq	%r13, %r13
	jne	.L179
.L112:
	movl	56(%r15), %edx
	testl	%edx, %edx
	jne	.L113
	cmpq	$0, 424(%rax)
	jne	.L113
	cmpl	$1, 92(%r15)
	je	.L113
	cmpb	$0, -332(%rbp)
	jne	.L113
	movzbl	-331(%rbp), %edx
	orb	-330(%rbp), %dl
	orb	-329(%rbp), %dl
	jne	.L114
	movq	184(%r15), %r11
	testq	%r11, %r11
	je	.L115
	subq	$8, %rsp
	movl	(%r15), %esi
	pushq	192(%r15)
	xorl	%edi, %edi
	movq	%r15, %r9
	movl	$12, %r8d
	movq	%r14, %rcx
	movl	$22, %edx
	call	*%r11
	movq	176(%r15), %rax
	popq	%r10
	popq	%r11
.L115:
.L82:
	endbr64
	movq	$0, 152(%r15)
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L180:
	movq	280(%rax), %rdi
	movq	32(%r14), %rbx
	call	pqueue_pop@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	dtls1_preprocess_fragment
	testl	%eax, %eax
	je	.L87
	movq	32(%r14), %rdx
	testq	%rdx, %rdx
	jne	.L185
.L88:
	movl	40(%r14), %eax
	testl	%eax, %eax
	jne	.L186
.L177:
	movq	88(%r14), %rdi
	movl	$103, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	96(%r14), %rdi
	movl	$104, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$105, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	movq	%r13, %rdi
	call	pitem_free@PLT
	movq	-496(%rbp), %rax
	movq	%rbx, 152(%r15)
	movq	%rbx, (%rax)
	movl	$1, %eax
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L113:
	movq	-488(%rbp), %rsi
	movq	%r15, %rdi
	call	dtls1_preprocess_fragment
	testl	%eax, %eax
	je	.L92
	testq	%r13, %r13
	je	.L116
	movq	136(%r15), %rax
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	movq	%r13, %r8
	movl	$22, %esi
	movq	%r15, %rdi
	movq	8(%rax), %rax
	pushq	-480(%rbp)
	leaq	12(%rax,%rbx), %rcx
	movq	8(%r15), %rax
	call	*104(%rax)
	popq	%r8
	popq	%r9
	testl	%eax, %eax
	jle	.L117
	cmpq	%r13, -448(%rbp)
	je	.L116
	movl	$881, %r9d
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L183:
	movl	$804, %r9d
.L176:
	leaq	.LC0(%rip), %r8
	movl	$271, %ecx
	movl	$370, %edx
	movq	%r15, %rdi
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
.L92:
	movq	-496(%rbp), %rax
	movq	$0, 152(%r15)
	movq	$0, (%rax)
	xorl	%eax, %eax
.L81:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L187
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	movl	40(%r14), %edx
	testl	%edx, %edx
	jne	.L188
	movl	%eax, -472(%rbp)
.L172:
	movq	88(%r14), %rdi
	movl	$103, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	96(%r14), %rdi
	movl	$104, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$105, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	movq	%r13, %rdi
	call	pitem_free@PLT
	movl	-472(%rbp), %eax
	movq	$0, 152(%r15)
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L116:
	movq	-496(%rbp), %rax
	movq	%r13, 152(%r15)
	movq	%r13, (%rax)
	movl	$1, %eax
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L114:
	movl	$844, %r9d
.L175:
	leaq	.LC0(%rip), %r8
	movl	$244, %ecx
	movl	$370, %edx
	movq	%r15, %rdi
	movl	$10, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L117:
	movq	-496(%rbp), %rax
	movl	$3, 40(%r15)
	movq	$0, (%rax)
	xorl	%eax, %eax
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L181:
	cmpb	$1, -332(%rbp)
	je	.L91
	movl	$770, %r9d
	leaq	.LC0(%rip), %r8
	movl	$103, %ecx
	movq	%r15, %rdi
	movl	$370, %edx
	movl	$10, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L182:
	movl	$787, %r9d
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L184:
	movq	-400(%rbp), %r14
	movq	-408(%rbp), %rdx
	addq	%r14, %rdx
	cmpq	-424(%rbp), %rdx
	jbe	.L96
.L173:
	xorl	%eax, %eax
.L178:
	movq	-504(%rbp), %rbx
	movl	%eax, (%rbx)
	xorl	%eax, %eax
	jmp	.L81
.L96:
	movq	280(%rax), %rdi
	leaq	-340(%rbp), %r12
	xorl	%r11d, %r11d
	movw	%si, -334(%rbp)
	movq	%r12, %rsi
	movw	%r11w, -336(%rbp)
	movl	$0, -340(%rbp)
	call	pqueue_find@PLT
	movq	176(%r15), %rdx
	testq	%rax, %rax
	movzwl	-416(%rbp), %eax
	movzwl	272(%rdx), %edx
	je	.L99
	cmpq	-424(%rbp), %r14
	je	.L100
.L99:
	cmpw	%ax, %dx
	jnb	.L100
	movzwl	%dx, %ecx
	addl	$10, %ecx
	cmpl	%ecx, %eax
	jle	.L101
.L100:
	leaq	-320(%rbp), %r13
	leaq	-440(%rbp), %r12
	movl	$256, %ebx
	testq	%r14, %r14
	jne	.L102
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L104:
	subq	-440(%rbp), %r14
	je	.L174
.L102:
	cmpq	$256, %r14
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	%r15, %rdi
	movq	8(%r15), %rax
	cmovbe	%r14, %r8
	subq	$8, %rsp
	xorl	%r9d, %r9d
	pushq	%r12
	xorl	%edx, %edx
	movl	$22, %esi
	call	*104(%rax)
	popq	%r9
	popq	%r10
	testl	%eax, %eax
	jg	.L104
	jmp	.L173
.L91:
	movq	136(%r15), %rax
	movq	-448(%rbp), %rdx
	movq	%r14, %rsi
	movq	8(%rax), %rdi
	call	memcpy@PLT
	movq	136(%r15), %rdx
	movq	-448(%rbp), %rax
	movq	-496(%rbp), %rbx
	movq	8(%rdx), %rdx
	subq	$1, %rax
	movq	%rax, 152(%r15)
	addq	$1, %rdx
	movq	%rdx, 144(%r15)
	movq	168(%r15), %rdx
	movq	%rax, 552(%rdx)
	movl	$257, 560(%rdx)
	movq	%rax, (%rbx)
	movl	$1, %eax
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L185:
	movq	136(%r15), %rcx
	movq	24(%r14), %rax
	movq	88(%r14), %rsi
	movq	8(%rcx), %rcx
	leaq	12(%rcx,%rax), %rdi
	call	memcpy@PLT
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L186:
	movq	48(%r14), %rdi
	call	EVP_CIPHER_CTX_free@PLT
	movq	56(%r14), %rdi
	call	EVP_MD_CTX_free@PLT
	jmp	.L177
.L179:
	movq	-488(%rbp), %rsi
	movq	%r15, %rdi
	call	dtls1_reassemble_fragment
	jmp	.L178
.L188:
	movq	48(%r14), %rdi
	movl	%eax, -472(%rbp)
	call	EVP_CIPHER_CTX_free@PLT
	movq	56(%r14), %rdi
	call	EVP_MD_CTX_free@PLT
	jmp	.L172
.L174:
	movl	$-3, %eax
	jmp	.L178
.L101:
	testw	%dx, %dx
	jne	.L103
	cmpb	$20, -432(%rbp)
	je	.L100
.L103:
	cmpq	-424(%rbp), %r14
	jne	.L179
	cmpq	$16716, 1512(%r15)
	movl	$16716, %eax
	cmovnb	1512(%r15), %rax
	cmpq	%rax, %r14
	ja	.L173
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	dtls1_hm_fragment_new
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L173
	movq	-488(%rbp), %rsi
	movl	$22, %ecx
	movq	%rax, %rdi
	rep movsl
	testq	%r14, %r14
	je	.L111
	movq	88(%rax), %rcx
	movq	8(%r15), %rax
	movq	%r14, %r8
	movq	%r15, %rdi
	leaq	-440(%rbp), %rdx
	xorl	%r9d, %r9d
	pushq	%rsi
	movl	$22, %esi
	pushq	%rdx
	xorl	%edx, %edx
	call	*104(%rax)
	popq	%rdi
	popq	%r8
	testl	%eax, %eax
	jle	.L109
	cmpq	-440(%rbp), %r14
	jne	.L109
.L111:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	pitem_new@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L109
	movq	176(%r15), %rax
	movq	280(%rax), %rdi
	call	pqueue_insert@PLT
	testq	%rax, %rax
	jne	.L174
.L109:
	movl	40(%r13), %edi
	testl	%edi, %edi
	jne	.L189
.L119:
	movq	88(%r13), %rdi
	movl	$103, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	96(%r13), %rdi
	movl	$104, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$105, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	jmp	.L173
.L189:
	movq	48(%r13), %rdi
	call	EVP_CIPHER_CTX_free@PLT
	movq	56(%r13), %rdi
	call	EVP_MD_CTX_free@PLT
	jmp	.L119
.L187:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1010:
	.size	dtls_get_reassembled_message, .-dtls_get_reassembled_message
	.p2align 4
	.globl	dtls1_hm_fragment_free
	.type	dtls1_hm_fragment_free, @function
dtls1_hm_fragment_free:
.LFB1002:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L190
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movl	40(%rdi), %eax
	testl	%eax, %eax
	jne	.L196
.L192:
	movq	88(%r12), %rdi
	movl	$103, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	96(%r12), %rdi
	movl	$104, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$105, %edx
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L196:
	.cfi_restore_state
	movq	48(%rdi), %rdi
	call	EVP_CIPHER_CTX_free@PLT
	movq	56(%r12), %rdi
	call	EVP_MD_CTX_free@PLT
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L190:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE1002:
	.size	dtls1_hm_fragment_free, .-dtls1_hm_fragment_free
	.p2align 4
	.globl	dtls1_do_write
	.type	dtls1_do_write, @function
dtls1_do_write:
.LFB1003:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	dtls1_query_mtu@PLT
	testl	%eax, %eax
	je	.L198
	movq	176(%rbx), %rax
	movq	%rbx, %rdi
	movq	304(%rax), %r13
	call	dtls1_min_mtu@PLT
	cmpq	%rax, %r13
	jb	.L198
	cmpq	$0, 160(%rbx)
	jne	.L199
	cmpl	$22, %r12d
	je	.L261
.L199:
	movq	1160(%rbx), %r8
	movq	1136(%rbx), %rdi
	xorl	%r13d, %r13d
	testq	%r8, %r8
	je	.L200
	testq	%rdi, %rdi
	je	.L201
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	testl	$2097152, %eax
	jne	.L260
	movq	1160(%rbx), %r8
.L201:
	movq	%r8, %rdi
	call	EVP_MD_CTX_md@PLT
	movq	%rax, %rdi
	call	EVP_MD_size@PLT
	movslq	%eax, %r13
.L260:
	movq	1136(%rbx), %rdi
.L200:
	testq	%rdi, %rdi
	je	.L205
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpq	$2, %rax
	je	.L262
.L205:
	xorl	%edx, %edx
.L204:
	movq	152(%rbx), %rax
	movl	$1, 40(%rbx)
	testq	%rax, %rax
	je	.L223
	movl	$1, -68(%rbp)
	xorl	%r14d, %r14d
	addq	%rdx, %r13
.L206:
	cmpl	$22, %r12d
	jne	.L208
.L267:
	movq	160(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L208
	testq	%r14, %r14
	je	.L209
	cmpq	$12, %rdx
	jbe	.L198
	subq	$12, %rdx
	addq	$12, %rax
	movq	%rdx, 160(%rbx)
	movq	%rax, 152(%rbx)
	.p2align 4,,10
	.p2align 3
.L208:
	movq	24(%rbx), %rdi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	$13, %esi
	call	BIO_ctrl@PLT
	leal	13(%rax), %edx
	movq	176(%rbx), %rax
	movslq	%edx, %rdx
	movq	304(%rax), %rax
	addq	%r13, %rdx
	cmpq	%rdx, %rax
	jbe	.L210
	subq	%rdx, %rax
	cmpq	$12, %rax
	ja	.L211
.L210:
	movq	24(%rbx), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movl	%eax, %r8d
	testl	%eax, %eax
	jle	.L263
	movq	176(%rbx), %rax
	leaq	25(%r13), %rcx
	movq	304(%rax), %rdx
	leaq	-13(%rdx), %rax
	cmpq	%rcx, %rdx
	jbe	.L198
	subq	%r13, %rax
.L211:
	movq	152(%rbx), %rdx
	movl	%edx, %ecx
	cmpq	%rcx, %rax
	cmovnb	%rdx, %rax
	cmpq	%rax, 1536(%rbx)
	cmovbe	1536(%rbx), %rax
	movq	%rax, %r15
	cmpl	$22, %r12d
	jne	.L214
	cmpq	$11, %rax
	jbe	.L198
	movq	176(%rbx), %rdx
	movq	136(%rbx), %rsi
	leaq	-12(%rax), %rax
	movq	%rax, 344(%rdx)
	movq	160(%rbx), %rax
	addq	8(%rsi), %rax
	movzbl	312(%rdx), %esi
	movq	%r14, 336(%rdx)
	movb	%sil, (%rax)
	movq	320(%rdx), %rsi
	shrq	$16, %rsi
	movb	%sil, 1(%rax)
	movq	320(%rdx), %rsi
	shrq	$8, %rsi
	movb	%sil, 2(%rax)
	movq	320(%rdx), %rsi
	movb	%sil, 3(%rax)
	movzbl	329(%rdx), %esi
	movb	%sil, 4(%rax)
	movzwl	328(%rdx), %esi
	movb	%sil, 5(%rax)
	movq	336(%rdx), %rsi
	shrq	$16, %rsi
	movb	%sil, 6(%rax)
	movq	336(%rdx), %rsi
	shrq	$8, %rsi
	movb	%sil, 7(%rax)
	movq	336(%rdx), %rsi
	movb	%sil, 8(%rax)
	movq	344(%rdx), %rsi
	shrq	$16, %rsi
	movb	%sil, 9(%rax)
	movq	344(%rdx), %rsi
	shrq	$8, %rsi
	movb	%sil, 10(%rax)
	movq	344(%rdx), %rdx
	movb	%dl, 11(%rax)
.L214:
	movq	136(%rbx), %rax
	movq	%r15, %rcx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	movq	160(%rbx), %rdx
	leaq	-64(%rbp), %r8
	addq	8(%rax), %rdx
	call	dtls1_write_bytes@PLT
	testl	%eax, %eax
	js	.L264
	movq	-64(%rbp), %rdx
	cmpq	%rdx, %r15
	jne	.L198
	cmpl	$22, %r12d
	je	.L265
.L217:
	movq	152(%rbx), %rax
	cmpq	%rdx, %rax
	je	.L266
	addq	%rdx, 160(%rbx)
	subq	%rdx, %rax
	subq	$12, %rdx
	addq	%rdx, %r14
	movq	%rdx, -64(%rbp)
	movq	176(%rbx), %rdx
	movq	%rax, 152(%rbx)
	movq	%r14, 336(%rdx)
	movq	$0, 344(%rdx)
	cmpl	$22, %r12d
	je	.L267
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L264:
	movl	-68(%rbp), %edi
	testl	%edi, %edi
	je	.L198
	movq	%rbx, %rdi
	call	SSL_get_wbio@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$43, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	testq	%rax, %rax
	jle	.L198
	movq	%rbx, %rdi
	call	SSL_get_options@PLT
	testb	$16, %ah
	jne	.L198
	movq	%rbx, %rdi
	call	dtls1_query_mtu@PLT
	testl	%eax, %eax
	je	.L198
	movq	152(%rbx), %rax
	testq	%rax, %rax
	je	.L223
	movl	$0, -68(%rbp)
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L265:
	movq	176(%rbx), %rax
	movl	524(%rax), %esi
	testl	%esi, %esi
	jne	.L217
	movq	136(%rbx), %rcx
	movq	160(%rbx), %rsi
	addq	8(%rcx), %rsi
	testq	%r14, %r14
	je	.L268
.L218:
	addq	$12, %rsi
	subq	$12, %rdx
.L219:
	movq	%rbx, %rdi
	call	ssl3_finish_mac@PLT
	testl	%eax, %eax
	je	.L198
	movq	-64(%rbp), %rdx
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L209:
	movq	176(%rbx), %rax
	movq	336(%rax), %r14
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L261:
	movq	176(%rbx), %rax
	movq	320(%rax), %rax
	addq	$12, %rax
	cmpq	%rax, 152(%rbx)
	je	.L199
	.p2align 4,,10
	.p2align 3
.L198:
	movl	$-1, %r8d
.L197:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L269
	leaq	-40(%rbp), %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L268:
	.cfi_restore_state
	cmpl	$256, (%rbx)
	je	.L218
	movzbl	312(%rax), %edx
	xorl	%ecx, %ecx
	movb	%dl, (%rsi)
	movq	320(%rax), %rdx
	shrq	$16, %rdx
	movb	%dl, 1(%rsi)
	movq	320(%rax), %rdx
	shrq	$8, %rdx
	movb	%dl, 2(%rsi)
	movq	320(%rax), %rdx
	movb	%dl, 3(%rsi)
	movzbl	329(%rax), %edx
	movb	%dl, 4(%rsi)
	movzwl	328(%rax), %edx
	movw	%cx, 6(%rsi)
	movb	%dl, 5(%rsi)
	movb	$0, 8(%rsi)
	movq	320(%rax), %rdx
	shrq	$16, %rdx
	movb	%dl, 9(%rsi)
	movq	320(%rax), %rdx
	shrq	$8, %rdx
	movb	%dl, 10(%rsi)
	movq	320(%rax), %rax
	movq	-64(%rbp), %rdx
	movb	%al, 11(%rsi)
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L262:
	movq	1136(%rbx), %rdi
	call	EVP_CIPHER_CTX_block_size@PLT
	leal	(%rax,%rax), %edx
	movslq	%edx, %rdx
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L223:
	xorl	%r8d, %r8d
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L263:
	movl	$2, 40(%rbx)
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L266:
	movq	184(%rbx), %rax
	testq	%rax, %rax
	je	.L221
	movq	136(%rbx), %rcx
	subq	$8, %rsp
	movl	(%rbx), %esi
	movq	%rbx, %r9
	addq	160(%rbx), %rdx
	movl	$1, %edi
	movq	8(%rcx), %rcx
	movq	%rdx, %r8
	pushq	192(%rbx)
	movl	%r12d, %edx
	call	*%rax
	popq	%rax
	popq	%rdx
.L221:
	pxor	%xmm0, %xmm0
	movl	$1, %r8d
	movups	%xmm0, 152(%rbx)
	jmp	.L197
.L269:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1003:
	.size	dtls1_do_write, .-dtls1_do_write
	.p2align 4
	.globl	dtls_get_message
	.type	dtls_get_message, @function
dtls_get_message:
.LFB1004:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-64(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-68(%rbp), %rbx
	subq	$56, %rsp
	movq	%rsi, -88(%rbp)
	movq	176(%rdi), %r13
	movq	%rdx, -96(%rbp)
	leaq	400(%r13), %r14
	leaq	408(%r13), %rdi
	andq	$-8, %rdi
	movq	%r14, %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	subq	%rdi, %rcx
	movq	$0, 400(%r13)
	movq	$0, 480(%r13)
	addl	$88, %ecx
	shrl	$3, %ecx
	rep stosq
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L297:
	movl	-68(%rbp), %eax
	addl	$3, %eax
	cmpl	$1, %eax
	ja	.L278
.L271:
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	dtls_get_reassembled_message
	testl	%eax, %eax
	je	.L297
	movq	168(%r15), %rax
	movq	136(%r15), %rdx
	movq	-88(%rbp), %rbx
	movq	-96(%rbp), %rsi
	movl	560(%rax), %eax
	movq	8(%rdx), %r12
	movq	152(%r15), %rdx
	movl	%eax, (%rbx)
	movq	%rdx, (%rsi)
	cmpl	$257, %eax
	je	.L298
	movq	408(%r13), %rbx
	movzbl	400(%r13), %edx
	movzwl	408(%r13), %eax
	movb	%dl, (%r12)
	movq	%rbx, %rdx
	rolw	$8, %ax
	shrq	$16, %rdx
	movb	%dl, 1(%r12)
	movw	%ax, 2(%r12)
	movzbl	417(%r13), %ecx
	movb	%cl, 4(%r12)
	movzwl	416(%r13), %ecx
	movb	$0, 8(%r12)
	movb	%cl, 5(%r12)
	xorl	%ecx, %ecx
	movw	%cx, 6(%r12)
	movb	%dl, 9(%r12)
	movw	%ax, 10(%r12)
	cmpl	$256, (%r15)
	je	.L299
	addq	$12, %rbx
.L276:
	movq	-88(%rbp), %rax
	cmpl	$20, (%rax)
	je	.L277
.L280:
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	ssl3_finish_mac@PLT
	testl	%eax, %eax
	je	.L278
	movq	184(%r15), %rax
	testq	%rax, %rax
	je	.L281
	subq	$8, %rsp
	movl	$22, %edx
	movl	(%r15), %esi
	movq	%r15, %r9
	pushq	192(%r15)
	movq	%rbx, %r8
	movq	%r12, %rcx
	xorl	%edi, %edi
	call	*%rax
	popq	%rax
	popq	%rdx
.L281:
	leaq	8(%r14), %rdi
	xorl	%eax, %eax
	movq	$0, 400(%r13)
	andq	$-8, %rdi
	movq	$0, 80(%r14)
	subq	%rdi, %r14
	leal	88(%r14), %ecx
	shrl	$3, %ecx
	rep stosq
	movq	176(%r15), %rax
	addw	$1, 272(%rax)
	movq	136(%r15), %rax
	movq	8(%rax), %rax
	addq	$12, %rax
	movq	%rax, 144(%r15)
	movl	$1, %eax
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L277:
	movq	%r15, %rdi
	call	ssl3_take_mac@PLT
	testl	%eax, %eax
	jne	.L280
	.p2align 4,,10
	.p2align 3
.L278:
	xorl	%eax, %eax
.L270:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L300
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L298:
	.cfi_restore_state
	movq	184(%r15), %r10
	movl	$1, %eax
	testq	%r10, %r10
	je	.L270
	subq	$8, %rsp
	movl	%eax, -88(%rbp)
	movl	(%r15), %esi
	xorl	%edi, %edi
	pushq	192(%r15)
	movq	%r15, %r9
	movl	$1, %r8d
	movq	%r12, %rcx
	movl	$20, %edx
	call	*%r10
	popq	%rsi
	movl	-88(%rbp), %eax
	popq	%rdi
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L299:
	addq	$12, %r12
	jmp	.L276
.L300:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1004:
	.size	dtls_get_message, .-dtls_get_message
	.p2align 4
	.globl	dtls_construct_change_cipher_spec
	.type	dtls_construct_change_cipher_spec, @function
dtls_construct_change_cipher_spec:
.LFB1011:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	cmpl	$256, (%rdi)
	je	.L302
.L304:
	movl	$1, %eax
.L301:
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L302:
	.cfi_restore_state
	movq	176(%r12), %rax
	movq	%rsi, %rdi
	movl	$2, %edx
	addw	$1, 270(%rax)
	movzwl	268(%rax), %esi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	jne	.L304
	movl	$68, %ecx
	movl	$371, %edx
	movq	%r12, %rdi
	movl	%eax, -20(%rbp)
	movl	$916, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movl	-20(%rbp), %eax
	jmp	.L301
	.cfi_endproc
.LFE1011:
	.size	dtls_construct_change_cipher_spec, .-dtls_construct_change_cipher_spec
	.p2align 4
	.globl	dtls1_read_failed
	.type	dtls1_read_failed, @function
dtls1_read_failed:
.LFB1012:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testl	%esi, %esi
	jg	.L314
	movl	%esi, %ebx
	call	dtls1_is_timer_expired@PLT
	testl	%eax, %eax
	jne	.L309
.L313:
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L309:
	.cfi_restore_state
	movq	%r12, %rdi
	call	ossl_statem_in_error@PLT
	testl	%eax, %eax
	jne	.L313
	movq	%r12, %rdi
	call	SSL_in_init@PLT
	movq	%r12, %rdi
	testl	%eax, %eax
	je	.L315
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	dtls1_handle_timeout@PLT
	.p2align 4,,10
	.p2align 3
.L314:
	.cfi_restore_state
	movl	$971, %r9d
	movl	$68, %ecx
	movl	$339, %edx
	movl	$80, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L315:
	.cfi_restore_state
	call	SSL_get_rbio@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	BIO_set_flags@PLT
	jmp	.L313
	.cfi_endproc
.LFE1012:
	.size	dtls1_read_failed, .-dtls1_read_failed
	.p2align 4
	.globl	dtls1_get_queue_priority
	.type	dtls1_get_queue_priority, @function
dtls1_get_queue_priority:
.LFB1013:
	.cfi_startproc
	endbr64
	movzwl	%di, %edi
	leal	(%rdi,%rdi), %eax
	subl	%esi, %eax
	ret
	.cfi_endproc
.LFE1013:
	.size	dtls1_get_queue_priority, .-dtls1_get_queue_priority
	.p2align 4
	.globl	dtls1_buffer_message
	.type	dtls1_buffer_message, @function
dtls1_buffer_message:
.LFB1015:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 160(%rdi)
	je	.L337
.L317:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L338
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L337:
	.cfi_restore_state
	movq	152(%rdi), %r15
	movq	%rdi, %rbx
	movl	%esi, %r14d
	movl	$62, %edx
	leaq	.LC0(%rip), %rsi
	movl	$104, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L339
	xorl	%edi, %edi
	testq	%r15, %r15
	jne	.L340
.L321:
	movq	136(%rbx), %rax
	movq	%rdi, 88(%r13)
	movq	$0, 96(%r13)
	movq	152(%rbx), %rdx
	movq	8(%rax), %rsi
	call	memcpy@PLT
	testl	%r14d, %r14d
	jne	.L326
	movq	176(%rbx), %rcx
	movl	152(%rbx), %eax
	xorl	%r12d, %r12d
	movq	320(%rcx), %rdx
	leaq	12(%rdx), %rsi
	cmpq	%rax, %rsi
	jne	.L317
.L323:
	movzwl	328(%rcx), %eax
	movq	%rdx, 8(%r13)
	movq	%r13, %rsi
	leaq	-64(%rbp), %rdi
	movw	%ax, 16(%r13)
	movzbl	312(%rcx), %ecx
	addl	%eax, %eax
	movq	%rdx, 32(%r13)
	subl	%r14d, %eax
	movb	%cl, 0(%r13)
	rolw	$8, %ax
	movq	$0, 24(%r13)
	movl	%r14d, 40(%r13)
	movdqu	1120(%rbx), %xmm0
	movdqu	1296(%rbx), %xmm2
	movdqu	1136(%rbx), %xmm1
	movdqu	1152(%rbx), %xmm3
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 64(%r13)
	shufpd	$2, %xmm3, %xmm1
	movups	%xmm1, 48(%r13)
	movq	6128(%rbx), %rdx
	movzwl	2(%rdx), %edx
	movl	$0, -64(%rbp)
	movw	%ax, -58(%rbp)
	movw	%dx, 80(%r13)
	xorl	%edx, %edx
	movw	%dx, -60(%rbp)
	call	pitem_new@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L341
	movq	176(%rbx), %rax
	movl	$1, %r12d
	movq	288(%rax), %rdi
	call	pqueue_insert@PLT
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L326:
	movq	176(%rbx), %rcx
	xorl	%eax, %eax
	cmpl	$256, (%rbx)
	sete	%al
	xorl	%r12d, %r12d
	movq	320(%rcx), %rdx
	leaq	1(%rdx,%rax,2), %rsi
	movl	152(%rbx), %eax
	cmpq	%rax, %rsi
	jne	.L317
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L340:
	movq	%r15, %rdi
	movl	$68, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L321
	movl	$69, %r8d
	movl	$65, %edx
	movl	$623, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$70, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L341:
	movl	40(%r13), %eax
	testl	%eax, %eax
	jne	.L342
.L325:
	movq	88(%r13), %rdi
	movl	$103, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%r12d, %r12d
	call	CRYPTO_free@PLT
	movq	96(%r13), %rdi
	movl	$104, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$105, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L339:
	movl	$63, %r8d
	movl	$65, %edx
	movl	$623, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L317
.L342:
	movq	48(%r13), %rdi
	call	EVP_CIPHER_CTX_free@PLT
	movq	56(%r13), %rdi
	call	EVP_MD_CTX_free@PLT
	jmp	.L325
.L338:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1015:
	.size	dtls1_buffer_message, .-dtls1_buffer_message
	.p2align 4
	.globl	dtls1_retransmit_message
	.type	dtls1_retransmit_message, @function
dtls1_retransmit_message:
.LFB1016:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	rolw	$8, %si
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-64(%rbp), %r8
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	xorl	%eax, %eax
	movw	%si, -58(%rbp)
	movq	%r8, %rsi
	movw	%ax, -60(%rbp)
	movq	176(%rdi), %rax
	movl	$0, -64(%rbp)
	movq	288(%rax), %rdi
	call	pqueue_find@PLT
	testq	%rax, %rax
	je	.L352
	movl	$1, (%rbx)
	movq	8(%rax), %rbx
	leaq	2112(%r12), %r15
	movq	136(%r12), %rax
	cmpl	$1, 40(%rbx)
	movq	8(%rbx), %rdx
	sbbq	%r13, %r13
	movq	8(%rax), %rdi
	movq	88(%rbx), %rsi
	andl	$11, %r13d
	addq	$1, %r13
	addq	%r13, %rdx
	call	memcpy@PLT
	movq	8(%rbx), %rdx
	movq	32(%rbx), %rcx
	movzwl	16(%rbx), %esi
	movq	176(%r12), %rax
	addq	%rdx, %r13
	movq	%r13, 152(%r12)
	movzbl	(%rbx), %edi
	movw	%si, 328(%rax)
	movb	%dil, 312(%rax)
	movq	%r15, %rdi
	movq	6128(%r12), %rsi
	movq	%rcx, 344(%rax)
	movq	1120(%r12), %rcx
	movq	%rdx, 320(%rax)
	movq	1136(%r12), %r9
	movq	1160(%r12), %r8
	movq	1296(%r12), %r13
	movq	%rcx, -80(%rbp)
	movq	$0, 336(%rax)
	movzwl	2(%rsi), %ecx
	movl	$1, 524(%rax)
	movq	48(%rbx), %rax
	movzwl	80(%rbx), %esi
	movq	%r9, -96(%rbp)
	movq	%rax, 1136(%r12)
	movq	56(%rbx), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, 1160(%r12)
	movq	64(%rbx), %rax
	movw	%cx, -66(%rbp)
	movq	%rax, 1120(%r12)
	movq	72(%rbx), %rax
	movq	%rax, 1296(%r12)
	call	DTLS_RECORD_LAYER_set_saved_w_epoch@PLT
	cmpl	$1, 40(%rbx)
	movq	%r12, %rdi
	sbbl	%esi, %esi
	andl	$2, %esi
	addl	$20, %esi
	call	dtls1_do_write
	movq	-80(%rbp), %rcx
	movq	-96(%rbp), %r9
	movq	%r15, %rdi
	movq	-88(%rbp), %r8
	movq	%r13, 1296(%r12)
	movl	%eax, %r14d
	movq	%rcx, 1120(%r12)
	movzwl	-66(%rbp), %esi
	movq	%r9, 1136(%r12)
	movq	%r8, 1160(%r12)
	call	DTLS_RECORD_LAYER_set_saved_w_epoch@PLT
	movq	24(%r12), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	176(%r12), %rax
	movl	$11, %esi
	movl	$0, 524(%rax)
	call	BIO_ctrl@PLT
.L343:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L353
	addq	$56, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L352:
	.cfi_restore_state
	movl	$68, %ecx
	movl	$390, %edx
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	movl	$1114, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movl	$0, (%rbx)
	jmp	.L343
.L353:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1016:
	.size	dtls1_retransmit_message, .-dtls1_retransmit_message
	.p2align 4
	.globl	dtls1_retransmit_buffered_messages
	.type	dtls1_retransmit_buffered_messages, @function
dtls1_retransmit_buffered_messages:
.LFB1014:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	176(%rdi), %rax
	movl	$0, -52(%rbp)
	movq	288(%rax), %rdi
	call	pqueue_iterator@PLT
	movq	%r12, %rdi
	movq	%rax, -48(%rbp)
	call	pqueue_next@PLT
	testq	%rax, %rax
	je	.L355
	leaq	-52(%rbp), %r13
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L365:
	movq	%r12, %rdi
	call	pqueue_next@PLT
	testq	%rax, %rax
	je	.L355
.L357:
	movq	8(%rax), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movzwl	16(%rax), %esi
	addl	%esi, %esi
	subl	40(%rax), %esi
	movzwl	%si, %esi
	call	dtls1_retransmit_message
	testl	%eax, %eax
	jg	.L365
	movl	$-1, %eax
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L355:
	movl	$1, %eax
.L354:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L366
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L366:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1014:
	.size	dtls1_retransmit_buffered_messages, .-dtls1_retransmit_buffered_messages
	.p2align 4
	.globl	dtls1_set_message_header
	.type	dtls1_set_message_header, @function
dtls1_set_message_header:
.LFB1017:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%r8, -8(%rbp)
	movq	176(%rdi), %rax
	movzwl	268(%rax), %edi
	movzwl	270(%rax), %r8d
	testq	%rcx, %rcx
	jne	.L368
	movw	%r8w, 268(%rax)
	leal	1(%r8), %edi
	movw	%di, 270(%rax)
	movl	%r8d, %edi
.L368:
	movb	%sil, 312(%rax)
	movq	%rcx, %xmm0
	movq	%rdx, 320(%rax)
	movhps	-8(%rbp), %xmm0
	movw	%di, 328(%rax)
	movups	%xmm0, 336(%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1017:
	.size	dtls1_set_message_header, .-dtls1_set_message_header
	.p2align 4
	.globl	dtls1_get_message_header
	.type	dtls1_get_message_header, @function
dtls1_get_message_header:
.LFB1021:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	leaq	8(%rsi), %rdi
	movq	%rsi, %rcx
	xorl	%eax, %eax
	andq	$-8, %rdi
	movq	$0, (%rsi)
	subq	%rdi, %rcx
	movq	$0, 80(%rsi)
	addl	$88, %ecx
	shrl	$3, %ecx
	rep stosq
	movzbl	(%rdx), %eax
	movb	%al, (%rsi)
	movzbl	1(%rdx), %eax
	movzbl	2(%rdx), %ecx
	salq	$16, %rax
	salq	$8, %rcx
	orq	%rcx, %rax
	movzbl	3(%rdx), %ecx
	orq	%rcx, %rax
	movq	%rax, 8(%rsi)
	movzwl	4(%rdx), %eax
	rolw	$8, %ax
	movw	%ax, 16(%rsi)
	movzbl	6(%rdx), %eax
	movzbl	7(%rdx), %ecx
	salq	$16, %rax
	salq	$8, %rcx
	orq	%rcx, %rax
	movzbl	8(%rdx), %ecx
	orq	%rcx, %rax
	movq	%rax, 24(%rsi)
	movzbl	9(%rdx), %eax
	movzbl	10(%rdx), %ecx
	movzbl	11(%rdx), %edx
	salq	$16, %rax
	salq	$8, %rcx
	orq	%rcx, %rax
	orq	%rdx, %rax
	movq	%rax, 32(%rsi)
	ret
	.cfi_endproc
.LFE1021:
	.size	dtls1_get_message_header, .-dtls1_get_message_header
	.p2align 4
	.globl	dtls1_set_handshake_header
	.type	dtls1_set_handshake_header, @function
dtls1_set_handshake_header:
.LFB1022:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	176(%rdi), %rax
	movzwl	270(%rax), %ecx
	movw	%cx, 268(%rax)
	cmpl	$257, %edx
	je	.L379
	leal	1(%rcx), %esi
	movb	%dl, 312(%rax)
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movw	%si, 270(%rax)
	leaq	-32(%rbp), %rdx
	movl	$12, %esi
	movq	$0, 320(%rax)
	movw	%cx, 328(%rax)
	movups	%xmm0, 336(%rax)
	call	WPACKET_allocate_bytes@PLT
	testl	%eax, %eax
	jne	.L380
.L371:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L381
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L380:
	.cfi_restore_state
	movq	%r12, %rdi
	call	WPACKET_start_sub_packet@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L379:
	movb	$1, 312(%rax)
	pxor	%xmm0, %xmm0
	movl	$1, %edx
	movq	%r12, %rdi
	movq	$0, 320(%rax)
	movl	$1, %esi
	movw	%cx, 328(%rax)
	movups	%xmm0, 336(%rax)
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L371
.L381:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1022:
	.size	dtls1_set_handshake_header, .-dtls1_set_handshake_header
	.p2align 4
	.globl	dtls1_close_construct_packet
	.type	dtls1_close_construct_packet, @function
dtls1_close_construct_packet:
.LFB1023:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$257, %edx
	je	.L386
	movq	%rsi, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L411
.L386:
	leaq	-56(%rbp), %rsi
	movq	%r13, %rdi
	call	WPACKET_get_length@PLT
	testl	%eax, %eax
	je	.L411
	movq	-56(%rbp), %r13
	cmpq	$2147483647, %r13
	ja	.L411
	cmpl	$257, %r12d
	je	.L388
	movq	176(%rbx), %rax
	leaq	-12(%r13), %rdx
	movq	%rdx, 320(%rax)
	movq	%rdx, 344(%rax)
	movl	$1, %eax
	movq	%r13, 152(%rbx)
	movq	$0, 160(%rbx)
	cmpl	$3, %r12d
	je	.L382
.L398:
	movl	$62, %edx
	leaq	.LC0(%rip), %rsi
	movl	$104, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L412
	xorl	%edi, %edi
	testq	%r13, %r13
	jne	.L413
.L390:
	movq	136(%rbx), %rax
	movq	%rdi, 88(%r14)
	movq	$0, 96(%r14)
	movq	152(%rbx), %rdx
	movq	8(%rax), %rsi
	call	memcpy@PLT
	cmpl	$257, %r12d
	jne	.L414
	movq	176(%rbx), %rcx
	xorl	%eax, %eax
	cmpl	$256, (%rbx)
	sete	%al
	movq	320(%rcx), %rdx
	leaq	1(%rdx,%rax,2), %rsi
	movl	152(%rbx), %eax
	cmpq	%rax, %rsi
	je	.L393
	.p2align 4,,10
	.p2align 3
.L411:
	xorl	%eax, %eax
.L382:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L415
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L414:
	.cfi_restore_state
	movq	176(%rbx), %rcx
	movl	152(%rbx), %eax
	movq	320(%rcx), %rdx
	leaq	12(%rdx), %rsi
	cmpq	%rax, %rsi
	jne	.L411
.L393:
	movzwl	328(%rcx), %eax
	movq	%rdx, 8(%r14)
	movq	%r14, %rsi
	leaq	-48(%rbp), %rdi
	cmpl	$257, %r12d
	movw	%ax, 16(%r14)
	movzbl	312(%rcx), %ecx
	movq	%rdx, 32(%r14)
	sete	%dl
	addl	%eax, %eax
	movzbl	%dl, %edx
	movb	%cl, (%r14)
	movq	$0, 24(%r14)
	subl	%edx, %eax
	movl	%edx, 40(%r14)
	movdqu	1120(%rbx), %xmm0
	rolw	$8, %ax
	movdqu	1296(%rbx), %xmm2
	movdqu	1136(%rbx), %xmm1
	movdqu	1152(%rbx), %xmm3
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 64(%r14)
	shufpd	$2, %xmm3, %xmm1
	movups	%xmm1, 48(%r14)
	movq	6128(%rbx), %rcx
	movzwl	2(%rcx), %ecx
	movl	$0, -48(%rbp)
	movw	%ax, -42(%rbp)
	movw	%cx, 80(%r14)
	xorl	%ecx, %ecx
	movw	%cx, -44(%rbp)
	call	pitem_new@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L416
	movq	176(%rbx), %rax
	movq	288(%rax), %rdi
	call	pqueue_insert@PLT
	movl	$1, %eax
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L388:
	movq	%r13, 152(%rbx)
	movq	$0, 160(%rbx)
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L413:
	movq	%r13, %rdi
	movl	$68, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L390
	movl	$69, %r8d
	movl	$65, %edx
	movl	$623, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$70, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	xorl	%eax, %eax
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L416:
	movl	40(%r14), %eax
	testl	%eax, %eax
	jne	.L417
.L395:
	movq	88(%r14), %rdi
	movl	$103, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	96(%r14), %rdi
	movl	$104, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$105, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	xorl	%eax, %eax
	jmp	.L382
.L412:
	movl	$63, %r8d
	movl	$65, %edx
	movl	$623, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L411
.L417:
	movq	48(%r14), %rdi
	call	EVP_CIPHER_CTX_free@PLT
	movq	56(%r14), %rdi
	call	EVP_MD_CTX_free@PLT
	jmp	.L395
.L415:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1023:
	.size	dtls1_close_construct_packet, .-dtls1_close_construct_packet
	.section	.rodata
	.align 8
	.type	bitmask_end_values, @object
	.size	bitmask_end_values, 8
bitmask_end_values:
	.ascii	"\377\001\003\007\017\037?\177"
	.align 8
	.type	bitmask_start_values, @object
	.size	bitmask_start_values, 8
bitmask_start_values:
	.ascii	"\377\376\374\370\360\340\300\200"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
