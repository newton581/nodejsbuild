	.file	"m_sigver.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/evp/m_sigver.c"
	.text
	.p2align 4
	.type	update, @function
update:
.LFB829:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$177, %edx
	movl	$173, %esi
	movl	$6, %edi
	movl	$20, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE829:
	.size	update, .-update
	.p2align 4
	.globl	EVP_DigestSignInit
	.type	EVP_DigestSignInit, @function
EVP_DigestSignInit:
.LFB831:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	32(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L31
.L5:
	movq	(%rdi), %rax
	movl	4(%rax), %r13d
	andl	$4, %r13d
	je	.L32
.L7:
	movq	(%rdi), %rdx
	movq	112(%rdx), %rax
	testq	%rax, %rax
	je	.L10
	movq	%r12, %rsi
	call	*%rax
	testl	%eax, %eax
	jle	.L11
	movq	32(%r12), %rdi
	movl	$64, 32(%rdi)
.L12:
	xorl	%r8d, %r8d
	movq	%r15, %r9
	movl	$1, %ecx
	movl	$248, %edx
	movl	$-1, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L11
	movq	32(%r12), %rax
	testq	%rbx, %rbx
	je	.L14
	movq	%rax, (%rbx)
	movq	32(%r12), %rax
.L14:
	movq	(%rax), %rax
	testb	$4, 4(%rax)
	jne	.L16
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	je	.L11
	movq	32(%r12), %rdi
	movq	(%rdi), %rax
	movq	248(%rax), %rax
	testq	%rax, %rax
	je	.L16
	movq	%r12, %rsi
	call	*%rax
	movl	%eax, %r13d
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L31:
	movq	%r8, %rdi
	movq	%rcx, %rsi
	movq	%r8, -72(%rbp)
	call	EVP_PKEY_CTX_new@PLT
	movq	-72(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, 32(%r12)
	movq	%rax, %rdi
	jne	.L5
.L11:
	xorl	%r13d, %r13d
.L4:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L33
	addq	$40, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	testq	%r15, %r15
	jne	.L7
	leaq	-60(%rbp), %rsi
	movq	%r8, %rdi
	call	EVP_PKEY_get_default_digest_nid@PLT
	testl	%eax, %eax
	jg	.L34
.L9:
	movl	$42, %r8d
	movl	$158, %edx
	movl	$161, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L16:
	movl	$1, %r13d
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L10:
	cmpq	$0, 208(%rdx)
	je	.L13
	leaq	update(%rip), %rax
	movl	$8, 32(%rdi)
	movq	%rax, 40(%r12)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L13:
	call	EVP_PKEY_sign_init@PLT
	testl	%eax, %eax
	jle	.L11
	movq	32(%r12), %rdi
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L34:
	movl	-60(%rbp), %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L9
	movq	32(%r12), %rdi
	jmp	.L7
.L33:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE831:
	.size	EVP_DigestSignInit, .-EVP_DigestSignInit
	.p2align 4
	.globl	EVP_DigestVerifyInit
	.type	EVP_DigestVerifyInit, @function
EVP_DigestVerifyInit:
.LFB832:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	32(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L62
.L36:
	movq	(%rdi), %rax
	movl	4(%rax), %r13d
	andl	$4, %r13d
	je	.L63
.L38:
	movq	(%rdi), %rdx
	movq	128(%rdx), %rax
	testq	%rax, %rax
	je	.L41
	movq	%r12, %rsi
	call	*%rax
	testl	%eax, %eax
	jle	.L42
	movq	32(%r12), %rdi
	movl	$128, 32(%rdi)
.L43:
	xorl	%r8d, %r8d
	movq	%r15, %r9
	movl	$1, %ecx
	movl	$248, %edx
	movl	$-1, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L42
	movq	32(%r12), %rax
	testq	%rbx, %rbx
	je	.L45
	movq	%rax, (%rbx)
	movq	32(%r12), %rax
.L45:
	movq	(%rax), %rax
	testb	$4, 4(%rax)
	jne	.L47
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	je	.L42
	movq	32(%r12), %rdi
	movq	(%rdi), %rax
	movq	248(%rax), %rax
	testq	%rax, %rax
	je	.L47
	movq	%r12, %rsi
	call	*%rax
	movl	%eax, %r13d
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L62:
	movq	%r8, %rdi
	movq	%rcx, %rsi
	movq	%r8, -72(%rbp)
	call	EVP_PKEY_CTX_new@PLT
	movq	-72(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, 32(%r12)
	movq	%rax, %rdi
	jne	.L36
.L42:
	xorl	%r13d, %r13d
.L35:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L64
	addq	$40, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	testq	%r15, %r15
	jne	.L38
	leaq	-60(%rbp), %rsi
	movq	%r8, %rdi
	call	EVP_PKEY_get_default_digest_nid@PLT
	testl	%eax, %eax
	jg	.L65
.L40:
	movl	$42, %r8d
	movl	$158, %edx
	movl	$161, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L47:
	movl	$1, %r13d
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L41:
	cmpq	$0, 216(%rdx)
	je	.L44
	leaq	update(%rip), %rax
	movl	$16, 32(%rdi)
	movq	%rax, 40(%r12)
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L44:
	call	EVP_PKEY_verify_init@PLT
	testl	%eax, %eax
	jle	.L42
	movq	32(%r12), %rdi
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L65:
	movl	-60(%rbp), %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L40
	movq	32(%r12), %rdi
	jmp	.L38
.L64:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE832:
	.size	EVP_DigestVerifyInit, .-EVP_DigestVerifyInit
	.p2align 4
	.globl	EVP_DigestSignFinal
	.type	EVP_DigestSignFinal, @function
EVP_DigestSignFinal:
.LFB833:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	32(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%r15), %rax
	movl	4(%rax), %r12d
	andl	$4, %r12d
	je	.L67
	testq	%rsi, %rsi
	je	.L92
	testb	$2, 17(%rdi)
	je	.L70
	movq	%rdi, %rcx
	movq	%r15, %rdi
	call	*120(%rax)
	movl	%eax, %r12d
.L66:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L93
	addq	$120, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	movq	120(%rax), %rax
	testq	%rax, %rax
	je	.L72
	testq	%rsi, %rsi
	je	.L73
	movl	$0, -132(%rbp)
	testb	$2, 17(%rdi)
	je	.L94
	movq	%rdi, %rcx
	movq	%r15, %rdi
	call	*%rax
	movl	%eax, %r12d
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L70:
	movq	%r15, %rdi
	call	EVP_PKEY_CTX_dup@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L91
	movq	(%rax), %rax
	movq	%r15, %rdi
	movq	%rbx, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	*120(%rax)
	movq	%r15, %rdi
	movl	%eax, %r12d
	call	EVP_PKEY_CTX_free@PLT
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L92:
	movq	%rdi, %rcx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	*120(%rax)
	movl	%eax, %r12d
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L94:
	movl	$1, -152(%rbp)
	movl	$1, %r15d
.L75:
	call	EVP_MD_CTX_new@PLT
	testq	%rax, %rax
	je	.L66
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, -160(%rbp)
	call	EVP_MD_CTX_copy_ex@PLT
	movq	-160(%rbp), %r9
	testl	%eax, %eax
	je	.L95
	movl	-152(%rbp), %eax
	testl	%eax, %eax
	je	.L82
	movq	32(%r9), %rdi
	movq	%r9, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r9, -152(%rbp)
	movq	(%rdi), %rax
	call	*120(%rax)
	movq	-152(%rbp), %r9
	movl	%eax, %r12d
.L83:
	movq	%r9, %rdi
	call	EVP_MD_CTX_free@PLT
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L95:
	movq	%r9, %rdi
	call	EVP_MD_CTX_free@PLT
	.p2align 4,,10
	.p2align 3
.L91:
	xorl	%r12d, %r12d
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L72:
	testq	%rsi, %rsi
	je	.L76
	movl	$0, -132(%rbp)
	testb	$2, 17(%rdi)
	je	.L96
	leaq	-132(%rbp), %rdx
	leaq	-128(%rbp), %rsi
	xorl	%r15d, %r15d
	call	EVP_DigestFinal_ex@PLT
	movl	%eax, %r12d
.L79:
	testl	%r12d, %r12d
	je	.L66
	testb	%r15b, %r15b
	jne	.L66
	movq	32(%rbx), %rdi
	leaq	-128(%rbp), %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movl	-132(%rbp), %r8d
	xorl	%r12d, %r12d
	call	EVP_PKEY_sign@PLT
	testl	%eax, %eax
	setg	%r12b
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L73:
	movq	%rdi, %rcx
	xorl	%esi, %esi
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	*%rax
	testl	%eax, %eax
	setg	%r12b
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L82:
	movq	%r9, %rdi
	leaq	-132(%rbp), %rdx
	leaq	-128(%rbp), %rsi
	movq	%r9, -152(%rbp)
	call	EVP_DigestFinal_ex@PLT
	movq	-152(%rbp), %r9
	movl	%eax, %r12d
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L76:
	movq	(%rdi), %rdi
	call	EVP_MD_size@PLT
	testl	%eax, %eax
	js	.L91
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movslq	%eax, %r8
	movq	%r14, %rdx
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	EVP_PKEY_sign@PLT
	testl	%eax, %eax
	setg	%r12b
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L96:
	movl	$0, -152(%rbp)
	xorl	%r15d, %r15d
	jmp	.L75
.L93:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE833:
	.size	EVP_DigestSignFinal, .-EVP_DigestSignFinal
	.p2align 4
	.globl	EVP_DigestSign
	.type	EVP_DigestSign, @function
EVP_DigestSign:
.LFB834:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	subq	$16, %rsp
	movq	32(%rdi), %rax
	movq	(%rax), %rax
	movq	208(%rax), %rax
	testq	%rax, %rax
	je	.L98
	addq	$16, %rsp
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L100
	movq	%r8, %rdx
	movq	%rcx, %rsi
	movq	%rdi, -24(%rbp)
	call	EVP_DigestUpdate@PLT
	movq	-24(%rbp), %rdi
	testl	%eax, %eax
	jle	.L102
.L100:
	addq	$16, %rsp
	movq	%r14, %rdx
	movq	%r13, %rsi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	EVP_DigestSignFinal
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE834:
	.size	EVP_DigestSign, .-EVP_DigestSign
	.p2align 4
	.globl	EVP_DigestVerifyFinal
	.type	EVP_DigestVerifyFinal, @function
EVP_DigestVerifyFinal:
.LFB835:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	32(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -132(%rbp)
	movq	(%rdi), %rax
	movq	136(%rax), %r8
	movq	16(%r12), %rax
	andl	$512, %eax
	testq	%r8, %r8
	je	.L104
	testq	%rax, %rax
	je	.L116
	movq	%r12, %rcx
	call	*%r8
.L103:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L117
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
	movl	$1, -148(%rbp)
	movl	$1, %ebx
.L106:
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L114
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	EVP_MD_CTX_copy_ex@PLT
	testl	%eax, %eax
	je	.L118
	movl	-148(%rbp), %eax
	testl	%eax, %eax
	je	.L111
	movq	32(%r13), %rdi
	movq	%r13, %rcx
	movl	%r15d, %edx
	movq	%r14, %rsi
	movq	(%rdi), %rax
	call	*136(%rax)
.L112:
	movq	%r13, %rdi
	movl	%eax, -148(%rbp)
	call	EVP_MD_CTX_free@PLT
	movl	-148(%rbp), %eax
.L109:
	testl	%eax, %eax
	je	.L103
	testb	%bl, %bl
	jne	.L103
	movq	32(%r12), %rdi
	leaq	-128(%rbp), %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movl	-132(%rbp), %r8d
	call	EVP_PKEY_verify@PLT
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L104:
	testq	%rax, %rax
	je	.L119
	leaq	-132(%rbp), %rdx
	leaq	-128(%rbp), %rsi
	movq	%r12, %rdi
	xorl	%ebx, %ebx
	call	EVP_DigestFinal_ex@PLT
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L111:
	leaq	-132(%rbp), %rdx
	leaq	-128(%rbp), %rsi
	movq	%r13, %rdi
	call	EVP_DigestFinal_ex@PLT
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L119:
	movl	$0, -148(%rbp)
	xorl	%ebx, %ebx
	jmp	.L106
.L114:
	movl	$-1, %eax
	jmp	.L103
.L118:
	movq	%r13, %rdi
	call	EVP_MD_CTX_free@PLT
	movl	$-1, %eax
	jmp	.L103
.L117:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE835:
	.size	EVP_DigestVerifyFinal, .-EVP_DigestVerifyFinal
	.p2align 4
	.globl	EVP_DigestVerify
	.type	EVP_DigestVerify, @function
EVP_DigestVerify:
.LFB836:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	subq	$16, %rsp
	movq	32(%rdi), %rax
	movq	(%rax), %rax
	movq	216(%rax), %rax
	testq	%rax, %rax
	je	.L121
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	movq	%r8, %rdx
	movq	%rcx, %rsi
	movq	%rdi, -24(%rbp)
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jle	.L122
	movq	-24(%rbp), %rdi
	addq	$16, %rsp
	movq	%r13, %rdx
	movq	%r12, %rsi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	EVP_DigestVerifyFinal
.L122:
	.cfi_restore_state
	addq	$16, %rsp
	movl	$-1, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE836:
	.size	EVP_DigestVerify, .-EVP_DigestVerify
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
