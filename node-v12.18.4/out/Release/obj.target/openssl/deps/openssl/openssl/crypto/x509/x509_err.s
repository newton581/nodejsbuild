	.file	"x509_err.c"
	.text
	.p2align 4
	.globl	ERR_load_X509_strings
	.type	ERR_load_X509_strings, @function
ERR_load_X509_strings:
.LFB203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$184958976, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_func_error_string@PLT
	testq	%rax, %rax
	je	.L5
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	leaq	X509_str_functs(%rip), %rdi
	call	ERR_load_strings_const@PLT
	leaq	X509_str_reasons(%rip), %rdi
	call	ERR_load_strings_const@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE203:
	.size	ERR_load_X509_strings, .-ERR_load_X509_strings
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"akid mismatch"
.LC1:
	.string	"bad selector"
.LC2:
	.string	"bad x509 filetype"
.LC3:
	.string	"base64 decode error"
.LC4:
	.string	"cant check dh key"
.LC5:
	.string	"cert already in hash table"
.LC6:
	.string	"crl already delta"
.LC7:
	.string	"crl verify failure"
.LC8:
	.string	"idp mismatch"
.LC9:
	.string	"invalid attributes"
.LC10:
	.string	"invalid directory"
.LC11:
	.string	"invalid field name"
.LC12:
	.string	"invalid trust"
.LC13:
	.string	"issuer mismatch"
.LC14:
	.string	"key type mismatch"
.LC15:
	.string	"key values mismatch"
.LC16:
	.string	"loading cert dir"
.LC17:
	.string	"loading defaults"
.LC18:
	.string	"method not supported"
.LC19:
	.string	"name too long"
.LC20:
	.string	"newer crl not newer"
.LC21:
	.string	"no certificate found"
.LC22:
	.string	"no certificate or crl found"
.LC23:
	.string	"no cert set for us to verify"
.LC24:
	.string	"no crl found"
.LC25:
	.string	"no crl number"
.LC26:
	.string	"public key decode error"
.LC27:
	.string	"public key encode error"
.LC28:
	.string	"should retry"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC29:
	.string	"unable to find parameters in chain"
	.align 8
.LC30:
	.string	"unable to get certs public key"
	.section	.rodata.str1.1
.LC31:
	.string	"unknown key type"
.LC32:
	.string	"unknown nid"
.LC33:
	.string	"unknown purpose id"
.LC34:
	.string	"unknown trust id"
.LC35:
	.string	"unsupported algorithm"
.LC36:
	.string	"wrong lookup type"
.LC37:
	.string	"wrong type"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	X509_str_reasons, @object
	.size	X509_str_reasons, 624
X509_str_reasons:
	.quad	184549486
	.quad	.LC0
	.quad	184549509
	.quad	.LC1
	.quad	184549476
	.quad	.LC2
	.quad	184549494
	.quad	.LC3
	.quad	184549490
	.quad	.LC4
	.quad	184549477
	.quad	.LC5
	.quad	184549503
	.quad	.LC6
	.quad	184549507
	.quad	.LC7
	.quad	184549504
	.quad	.LC8
	.quad	184549514
	.quad	.LC9
	.quad	184549489
	.quad	.LC10
	.quad	184549495
	.quad	.LC11
	.quad	184549499
	.quad	.LC12
	.quad	184549505
	.quad	.LC13
	.quad	184549491
	.quad	.LC14
	.quad	184549492
	.quad	.LC15
	.quad	184549479
	.quad	.LC16
	.quad	184549480
	.quad	.LC17
	.quad	184549500
	.quad	.LC18
	.quad	184549510
	.quad	.LC19
	.quad	184549508
	.quad	.LC20
	.quad	184549511
	.quad	.LC21
	.quad	184549512
	.quad	.LC22
	.quad	184549481
	.quad	.LC23
	.quad	184549513
	.quad	.LC24
	.quad	184549506
	.quad	.LC25
	.quad	184549501
	.quad	.LC26
	.quad	184549502
	.quad	.LC27
	.quad	184549482
	.quad	.LC28
	.quad	184549483
	.quad	.LC29
	.quad	184549484
	.quad	.LC30
	.quad	184549493
	.quad	.LC31
	.quad	184549485
	.quad	.LC32
	.quad	184549497
	.quad	.LC33
	.quad	184549496
	.quad	.LC34
	.quad	184549487
	.quad	.LC35
	.quad	184549488
	.quad	.LC36
	.quad	184549498
	.quad	.LC37
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC38:
	.string	"add_cert_dir"
.LC39:
	.string	"build_chain"
.LC40:
	.string	"by_file_ctrl"
.LC41:
	.string	"check_name_constraints"
.LC42:
	.string	"check_policy"
.LC43:
	.string	"dane_i2d"
.LC44:
	.string	"dir_ctrl"
.LC45:
	.string	"get_cert_by_subject"
.LC46:
	.string	"i2d_X509_AUX"
.LC47:
	.string	"lookup_certs_sk"
.LC48:
	.string	"NETSCAPE_SPKI_b64_decode"
.LC49:
	.string	"NETSCAPE_SPKI_b64_encode"
.LC50:
	.string	"new_dir"
.LC51:
	.string	"X509at_add1_attr"
.LC52:
	.string	"X509v3_add_ext"
.LC53:
	.string	"X509_ATTRIBUTE_create_by_NID"
.LC54:
	.string	"X509_ATTRIBUTE_create_by_OBJ"
.LC55:
	.string	"X509_ATTRIBUTE_create_by_txt"
.LC56:
	.string	"X509_ATTRIBUTE_get0_data"
.LC57:
	.string	"X509_ATTRIBUTE_set1_data"
.LC58:
	.string	"X509_check_private_key"
.LC59:
	.string	"X509_CRL_diff"
.LC60:
	.string	"X509_CRL_METHOD_new"
.LC61:
	.string	"X509_CRL_print_fp"
.LC62:
	.string	"X509_EXTENSION_create_by_NID"
.LC63:
	.string	"X509_EXTENSION_create_by_OBJ"
.LC64:
	.string	"X509_get_pubkey_parameters"
.LC65:
	.string	"X509_load_cert_crl_file"
.LC66:
	.string	"X509_load_cert_file"
.LC67:
	.string	"X509_load_crl_file"
.LC68:
	.string	"X509_LOOKUP_meth_new"
.LC69:
	.string	"X509_LOOKUP_new"
.LC70:
	.string	"X509_NAME_add_entry"
.LC71:
	.string	"x509_name_canon"
.LC72:
	.string	"X509_NAME_ENTRY_create_by_NID"
.LC73:
	.string	"X509_NAME_ENTRY_create_by_txt"
.LC74:
	.string	"X509_NAME_ENTRY_set_object"
.LC75:
	.string	"X509_NAME_oneline"
.LC76:
	.string	"X509_NAME_print"
.LC77:
	.string	"X509_OBJECT_new"
.LC78:
	.string	"X509_print_ex_fp"
.LC79:
	.string	"x509_pubkey_decode"
.LC80:
	.string	"X509_PUBKEY_get0"
.LC81:
	.string	"X509_PUBKEY_set"
.LC82:
	.string	"X509_REQ_check_private_key"
.LC83:
	.string	"X509_REQ_print_ex"
.LC84:
	.string	"X509_REQ_print_fp"
.LC85:
	.string	"X509_REQ_to_X509"
.LC86:
	.string	"X509_STORE_add_cert"
.LC87:
	.string	"X509_STORE_add_crl"
.LC88:
	.string	"X509_STORE_add_lookup"
.LC89:
	.string	"X509_STORE_CTX_get1_issuer"
.LC90:
	.string	"X509_STORE_CTX_init"
.LC91:
	.string	"X509_STORE_CTX_new"
	.section	.rodata.str1.8
	.align 8
.LC92:
	.string	"X509_STORE_CTX_purpose_inherit"
	.section	.rodata.str1.1
.LC93:
	.string	"X509_STORE_new"
.LC94:
	.string	"X509_to_X509_REQ"
.LC95:
	.string	"X509_TRUST_add"
.LC96:
	.string	"X509_TRUST_set"
.LC97:
	.string	"X509_verify_cert"
.LC98:
	.string	"X509_VERIFY_PARAM_new"
	.section	.data.rel.ro.local
	.align 32
	.type	X509_str_functs, @object
	.size	X509_str_functs, 992
X509_str_functs:
	.quad	184958976
	.quad	.LC38
	.quad	184983552
	.quad	.LC39
	.quad	184963072
	.quad	.LC40
	.quad	185159680
	.quad	.LC41
	.quad	185143296
	.quad	.LC42
	.quad	184987648
	.quad	.LC43
	.quad	184967168
	.quad	.LC44
	.quad	184971264
	.quad	.LC45
	.quad	185167872
	.quad	.LC46
	.quad	185171968
	.quad	.LC47
	.quad	185077760
	.quad	.LC48
	.quad	185081856
	.quad	.LC49
	.quad	185176064
	.quad	.LC50
	.quad	185102336
	.quad	.LC51
	.quad	184975360
	.quad	.LC52
	.quad	185106432
	.quad	.LC53
	.quad	185110528
	.quad	.LC54
	.quad	185122816
	.quad	.LC55
	.quad	185118720
	.quad	.LC56
	.quad	185114624
	.quad	.LC57
	.quad	185073664
	.quad	.LC58
	.quad	184979456
	.quad	.LC59
	.quad	185180160
	.quad	.LC60
	.quad	185151488
	.quad	.LC61
	.quad	184991744
	.quad	.LC62
	.quad	184995840
	.quad	.LC63
	.quad	184999936
	.quad	.LC64
	.quad	185090048
	.quad	.LC65
	.quad	185004032
	.quad	.LC66
	.quad	185008128
	.quad	.LC67
	.quad	185204736
	.quad	.LC68
	.quad	185184256
	.quad	.LC69
	.quad	185012224
	.quad	.LC70
	.quad	185188352
	.quad	.LC71
	.quad	185016320
	.quad	.LC72
	.quad	185085952
	.quad	.LC73
	.quad	185020416
	.quad	.LC74
	.quad	185024512
	.quad	.LC75
	.quad	185028608
	.quad	.LC76
	.quad	185163776
	.quad	.LC77
	.quad	185032704
	.quad	.LC78
	.quad	185155584
	.quad	.LC79
	.quad	185036800
	.quad	.LC80
	.quad	185040896
	.quad	.LC81
	.quad	185139200
	.quad	.LC82
	.quad	185044992
	.quad	.LC83
	.quad	185049088
	.quad	.LC84
	.quad	185053184
	.quad	.LC85
	.quad	185057280
	.quad	.LC86
	.quad	185061376
	.quad	.LC87
	.quad	185192448
	.quad	.LC88
	.quad	185147392
	.quad	.LC89
	.quad	185135104
	.quad	.LC90
	.quad	185131008
	.quad	.LC91
	.quad	185098240
	.quad	.LC92
	.quad	185196544
	.quad	.LC93
	.quad	185065472
	.quad	.LC94
	.quad	185094144
	.quad	.LC95
	.quad	185126912
	.quad	.LC96
	.quad	185069568
	.quad	.LC97
	.quad	185200640
	.quad	.LC98
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
