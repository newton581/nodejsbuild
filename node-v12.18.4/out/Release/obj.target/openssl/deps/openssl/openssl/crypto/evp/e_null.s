	.file	"e_null.c"
	.text
	.p2align 4
	.type	null_init_key, @function
null_init_key:
.LFB446:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE446:
	.size	null_init_key, .-null_init_key
	.p2align 4
	.type	null_cipher, @function
null_cipher:
.LFB447:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	cmpq	%rdi, %rdx
	je	.L6
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	memcpy@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore 6
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE447:
	.size	null_cipher, .-null_cipher
	.p2align 4
	.globl	EVP_enc_null
	.type	EVP_enc_null, @function
EVP_enc_null:
.LFB445:
	.cfi_startproc
	endbr64
	leaq	n_cipher(%rip), %rax
	ret
	.cfi_endproc
.LFE445:
	.size	EVP_enc_null, .-EVP_enc_null
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	n_cipher, @object
	.size	n_cipher, 88
n_cipher:
	.long	0
	.long	1
	.long	0
	.long	0
	.quad	0
	.quad	null_init_key
	.quad	null_cipher
	.quad	0
	.long	0
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
