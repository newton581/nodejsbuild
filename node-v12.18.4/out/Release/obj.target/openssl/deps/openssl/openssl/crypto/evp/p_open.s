	.file	"p_open.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/evp/p_open.c"
	.text
	.p2align 4
	.globl	EVP_OpenInit
	.type	EVP_OpenInit, @function
EVP_OpenInit:
.LFB779:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movl	%ecx, -56(%rbp)
	movq	%r8, -64(%rbp)
	testq	%rsi, %rsi
	je	.L5
	movq	%rsi, %r12
	call	EVP_CIPHER_CTX_reset@PLT
	movq	%r12, %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	EVP_DecryptInit_ex@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L1
.L5:
	movl	$1, %r12d
	testq	%r15, %r15
	je	.L1
	movq	%r15, %rdi
	call	EVP_PKEY_id@PLT
	cmpl	$6, %eax
	je	.L6
	movl	$106, %edx
	movl	$102, %esi
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	movl	$38, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$6, %edi
	call	ERR_put_error@PLT
	xorl	%r10d, %r10d
.L7:
	movl	$60, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r14, %rsi
	movq	%r10, %rdi
	call	CRYPTO_clear_free@PLT
.L1:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movq	%r15, %rdi
	call	EVP_PKEY_size@PLT
	movl	$43, %edx
	leaq	.LC0(%rip), %rsi
	movslq	%eax, %r14
	movq	%r14, %rdi
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L21
	movl	-56(%rbp), %edx
	movq	%rbx, %rsi
	movq	%r15, %rcx
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	xorl	%r12d, %r12d
	call	EVP_PKEY_decrypt_old@PLT
	movq	-56(%rbp), %r10
	testl	%eax, %eax
	movl	%eax, %esi
	jle	.L7
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_set_key_length@PLT
	movq	-56(%rbp), %r10
	testl	%eax, %eax
	movl	%eax, %r12d
	je	.L7
	movq	-64(%rbp), %r8
	movq	%r10, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	call	EVP_DecryptInit_ex@PLT
	movq	-56(%rbp), %r10
	testl	%eax, %eax
	setne	%r12b
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L21:
	movl	$46, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$102, %esi
	movl	$6, %edi
	movq	%rax, -56(%rbp)
	xorl	%r12d, %r12d
	call	ERR_put_error@PLT
	movq	-56(%rbp), %r10
	jmp	.L7
	.cfi_endproc
.LFE779:
	.size	EVP_OpenInit, .-EVP_OpenInit
	.p2align 4
	.globl	EVP_OpenFinal
	.type	EVP_OpenFinal, @function
EVP_OpenFinal:
.LFB780:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	EVP_DecryptFinal_ex@PLT
	testl	%eax, %eax
	jne	.L25
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	popq	%r12
	xorl	%edx, %edx
	xorl	%esi, %esi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	EVP_DecryptInit_ex@PLT
	.cfi_endproc
.LFE780:
	.size	EVP_OpenFinal, .-EVP_OpenFinal
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
