	.file	"t_x509.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	":"
.LC1:
	.string	""
.LC2:
	.string	"%*sTrusted Uses:\n%*s"
.LC3:
	.string	", "
.LC4:
	.string	"\n"
.LC5:
	.string	"%*sNo Trusted Uses.\n"
.LC6:
	.string	"%*sRejected Uses:\n%*s"
.LC7:
	.string	"%*sNo Rejected Uses.\n"
.LC8:
	.string	"%*sAlias: %s\n"
.LC9:
	.string	"%*sKey Id: "
.LC10:
	.string	"%s%02X"
	.text
	.p2align 4
	.type	X509_aux_print.part.0, @function
X509_aux_print.part.0:
.LFB1330:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$136, %rsp
	movq	%rsi, -176(%rbp)
	movl	%edx, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	X509_get0_trust_objects@PLT
	movq	%rbx, %rdi
	movq	%rax, %r14
	call	X509_get0_reject_objects@PLT
	movq	%rax, %r13
	testq	%r14, %r14
	je	.L2
	leaq	.LC1(%rip), %r9
	leal	2(%r15), %r8d
	movl	%r15d, %edx
	xorl	%eax, %eax
	movq	%r9, %rcx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	xorl	%ebx, %ebx
	call	BIO_printf@PLT
	leaq	-144(%rbp), %r15
	movb	$1, -161(%rbp)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L4:
	movl	%ebx, %esi
	movq	%r14, %rdi
	addl	$1, %ebx
	call	OPENSSL_sk_value@PLT
	xorl	%ecx, %ecx
	movl	$80, %esi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	OBJ_obj2txt@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	movb	$0, -161(%rbp)
.L3:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L27
	cmpb	$0, -161(%rbp)
	jne	.L4
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L27:
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
.L6:
	testq	%r13, %r13
	je	.L7
	movl	-168(%rbp), %eax
	leaq	.LC1(%rip), %r9
	movq	%r12, %rdi
	xorl	%ebx, %ebx
	movq	%r9, %rcx
	leaq	.LC6(%rip), %rsi
	movl	$1, %r15d
	leal	2(%rax), %r8d
	movl	%eax, %edx
	leaq	-144(%rbp), %r14
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L9:
	movl	%ebx, %esi
	movq	%r13, %rdi
	addl	$1, %ebx
	xorl	%r15d, %r15d
	call	OPENSSL_sk_value@PLT
	xorl	%ecx, %ecx
	movl	$80, %esi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	OBJ_obj2txt@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
.L8:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L28
	testb	%r15b, %r15b
	jne	.L9
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L28:
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
.L11:
	movq	-176(%rbp), %rdi
	xorl	%esi, %esi
	call	X509_alias_get0@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L12
	movl	-168(%rbp), %edx
	leaq	.LC1(%rip), %rcx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC8(%rip), %rsi
	call	BIO_printf@PLT
.L12:
	movq	-176(%rbp), %rdi
	leaq	-148(%rbp), %rsi
	call	X509_keyid_get0@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1
	movl	-168(%rbp), %edx
	leaq	.LC1(%rip), %rcx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC9(%rip), %rsi
	call	BIO_printf@PLT
	movl	-148(%rbp), %eax
	testl	%eax, %eax
	jle	.L16
	movzbl	0(%r13), %ecx
	xorl	%ebx, %ebx
	leaq	.LC1(%rip), %rdx
	leaq	.LC0(%rip), %r14
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L15:
	movzbl	0(%r13,%rbx), %ecx
	movq	%r14, %rdx
.L17:
	leaq	.LC10(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	addq	$1, %rbx
	call	BIO_printf@PLT
	cmpl	%ebx, -148(%rbp)
	jg	.L15
.L16:
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L29
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	movl	-168(%rbp), %edx
	leaq	.LC1(%rip), %rcx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC7(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L2:
	movl	-168(%rbp), %edx
	leaq	.LC1(%rip), %rcx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC5(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L6
.L29:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1330:
	.size	X509_aux_print.part.0, .-X509_aux_print.part.0
	.section	.rodata.str1.1
.LC11:
	.string	"        Subject OCSP hash: "
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"../deps/openssl/openssl/crypto/x509/t_x509.c"
	.section	.rodata.str1.1
.LC13:
	.string	"%02X"
	.section	.rodata.str1.8
	.align 8
.LC14:
	.string	"\n        Public key OCSP hash: "
	.text
	.p2align 4
	.globl	X509_ocspid_print
	.type	X509_ocspid_print, @function
X509_ocspid_print:
.LFB1324:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	%rsi, -104(%rbp)
	leaq	.LC11(%rip), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L31
	movq	%rbx, %rdi
	call	X509_get_subject_name@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	i2d_X509_NAME@PLT
	movl	$238, %edx
	leaq	.LC12(%rip), %rsi
	movslq	%eax, %r15
	movq	%r15, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L31
	movq	%r14, %rdi
	leaq	-88(%rbp), %rsi
	leaq	-80(%rbp), %r14
	call	i2d_X509_NAME@PLT
	call	EVP_sha1@PLT
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%rax, %r8
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%r14, -120(%rbp)
	call	EVP_Digest@PLT
	testl	%eax, %eax
	je	.L32
	movq	%r14, -112(%rbp)
	leaq	-60(%rbp), %rbx
	leaq	.LC13(%rip), %r15
	.p2align 4,,10
	.p2align 3
.L33:
	movzbl	(%r14), %edx
	xorl	%eax, %eax
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L32
	addq	$1, %r14
	cmpq	%rbx, %r14
	jne	.L33
	leaq	.LC12(%rip), %rsi
	movq	%r13, %rdi
	movl	$248, %edx
	call	CRYPTO_free@PLT
	xorl	%eax, %eax
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L31
	movq	-104(%rbp), %rdi
	call	X509_get0_pubkey_bitstr@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L31
	call	EVP_sha1@PLT
	movq	%r14, %rdi
	movq	%rax, %r15
	call	ASN1_STRING_length@PLT
	movq	%r14, %rdi
	movl	%eax, %r13d
	call	ASN1_STRING_get0_data@PLT
	movq	-120(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	movslq	%r13d, %rsi
	movq	%r15, %r8
	call	EVP_Digest@PLT
	testl	%eax, %eax
	je	.L31
	leaq	.LC13(%rip), %r13
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L53:
	addq	$1, %r15
	movq	%r15, -112(%rbp)
	cmpq	%rbx, %r15
	je	.L52
.L34:
	movq	-112(%rbp), %r15
	xorl	%eax, %eax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movzbl	(%r15), %edx
	call	BIO_printf@PLT
	testl	%eax, %eax
	jg	.L53
	.p2align 4,,10
	.p2align 3
.L31:
	xorl	%r13d, %r13d
.L32:
	movl	$274, %edx
	leaq	.LC12(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	xorl	%eax, %eax
.L30:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L54
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L52:
	.cfi_restore_state
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$1, %eax
	jmp	.L30
.L54:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1324:
	.size	X509_ocspid_print, .-X509_ocspid_print
	.section	.rodata.str1.1
.LC15:
	.string	"%02x%s"
	.text
	.p2align 4
	.globl	X509_signature_dump
	.type	X509_signature_dump, @function
X509_signature_dump:
.LFB1325:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$24, %rsp
	movq	8(%rsi), %rax
	movl	(%rsi), %r12d
	movq	%rax, -56(%rbp)
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L63:
	leal	1(%r13), %ecx
	cmpl	%ecx, %r12d
	je	.L70
	movq	-56(%rbp), %rax
	leaq	.LC0(%rip), %rcx
	movzbl	(%rax,%rbx), %edx
.L69:
	leaq	.LC15(%rip), %rsi
	movq	%r15, %rdi
	xorl	%eax, %eax
	addq	$1, %rbx
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L62
.L56:
	movl	%ebx, %r13d
	cmpl	%ebx, %r12d
	jle	.L71
	imull	$954437177, %ebx, %eax
	rorl	%eax
	cmpl	$238609294, %eax
	ja	.L63
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r15, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jle	.L62
	movl	%r14d, %edx
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	BIO_indent@PLT
	testl	%eax, %eax
	jg	.L63
.L62:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	movq	-56(%rbp), %rax
	leaq	.LC1(%rip), %rcx
	movzbl	(%rax,%rbx), %edx
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L71:
	movq	%r15, %rdi
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	call	BIO_write@PLT
	cmpl	$1, %eax
	sete	%al
	addq	$24, %rsp
	popq	%rbx
	movzbl	%al, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1325:
	.size	X509_signature_dump, .-X509_signature_dump
	.section	.rodata.str1.1
.LC16:
	.string	"    Signature Algorithm: "
	.text
	.p2align 4
	.globl	X509_signature_print
	.type	X509_signature_print, @function
X509_signature_print:
.LFB1326:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	leaq	.LC16(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L75
	movq	0(%r13), %rsi
	movq	%r12, %rdi
	call	i2a_ASN1_OBJECT@PLT
	testl	%eax, %eax
	jle	.L75
	movq	0(%r13), %rdi
	call	OBJ_obj2nid@PLT
	testl	%eax, %eax
	jne	.L93
.L76:
	testq	%r14, %r14
	je	.L78
	movl	$9, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	X509_signature_dump
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L75:
	xorl	%eax, %eax
.L72:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L94
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L93:
	leaq	-48(%rbp), %rdx
	leaq	-44(%rbp), %rsi
	movl	%eax, %edi
	call	OBJ_find_sigid_algs@PLT
	testl	%eax, %eax
	je	.L76
	movl	-48(%rbp), %esi
	xorl	%edi, %edi
	call	EVP_PKEY_asn1_find@PLT
	testq	%rax, %rax
	je	.L76
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L76
	xorl	%r8d, %r8d
	movl	$9, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L72
.L94:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1326:
	.size	X509_signature_print, .-X509_signature_print
	.section	.rodata.str1.1
.LC17:
	.string	"-"
.LC18:
	.string	" (Negative)"
.LC19:
	.string	"Certificate:\n"
.LC20:
	.string	"    Data:\n"
.LC21:
	.string	"%8sVersion: %ld (0x%lx)\n"
.LC22:
	.string	"%8sVersion: Unknown (%ld)\n"
.LC23:
	.string	"        Serial Number:"
.LC24:
	.string	" %s%lu (%s0x%lx)\n"
.LC25:
	.string	"\n%12s%s"
.LC26:
	.string	"%02x%c"
.LC27:
	.string	"    "
.LC28:
	.string	"        Issuer:%c"
.LC29:
	.string	"        Validity\n"
.LC30:
	.string	"            Not Before: "
.LC31:
	.string	"\n            Not After : "
.LC32:
	.string	"        Subject:%c"
	.section	.rodata.str1.8
	.align 8
.LC33:
	.string	"        Subject Public Key Info:\n"
	.section	.rodata.str1.1
.LC34:
	.string	"%12sPublic Key Algorithm: "
	.section	.rodata.str1.8
	.align 8
.LC35:
	.string	"%12sUnable to load Public Key\n"
	.section	.rodata.str1.1
.LC36:
	.string	"%8sIssuer Unique ID: "
.LC37:
	.string	"%8sSubject Unique ID: "
.LC38:
	.string	"X509v3 extensions"
	.text
	.p2align 4
	.globl	X509_print_ex
	.type	X509_print_ex, @function
X509_print_ex:
.LFB1323:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, %rax
	andl	$983040, %eax
	cmpq	$262144, %rax
	je	.L139
	cmpq	$1, %rdx
	movl	$32, -84(%rbp)
	sbbl	%eax, %eax
	andl	$16, %eax
	movl	%eax, -88(%rbp)
	testb	$1, %bl
	je	.L97
.L103:
	testb	$2, %bl
	je	.L164
.L99:
	movq	%rbx, %r15
	andl	$4, %r15d
	je	.L165
.L105:
	testb	$8, %bl
	je	.L166
.L108:
	testb	$16, %bl
	je	.L167
.L117:
	testb	$32, %bl
	je	.L168
.L119:
	testb	$64, %bl
	je	.L169
.L121:
	testb	$-128, %bl
	je	.L170
.L123:
	testb	$16, %bh
	je	.L171
.L127:
	testb	$1, %bh
	je	.L172
.L133:
	testb	$2, %bh
	je	.L173
.L134:
	andb	$4, %bh
	je	.L136
.L162:
	movl	$1, %r12d
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L165:
	movl	$22, %edx
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jle	.L161
	movq	%r14, %rdi
	call	X509_get_serialNumber@PLT
	cmpl	$8, (%rax)
	movq	%rax, %r10
	jle	.L174
.L109:
	cmpl	$258, 4(%r10)
	movq	%r12, %rdi
	movq	%r10, -96(%rbp)
	leaq	.LC1(%rip), %rdx
	leaq	.LC18(%rip), %rcx
	leaq	.LC25(%rip), %rsi
	cmovne	%rdx, %rcx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L161
	movq	-96(%rbp), %r10
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L115:
	leal	1(%r15), %edx
	movq	%r10, -96(%rbp)
	movl	$58, %ecx
	cmpl	%edx, %eax
	movq	8(%r10), %rax
	movzbl	(%rax,%r15), %edx
	jne	.L160
	movl	$10, %ecx
.L160:
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rsi
	movq	%r12, %rdi
	addq	$1, %r15
	call	BIO_printf@PLT
	movq	-96(%rbp), %r10
	testl	%eax, %eax
	jle	.L161
.L112:
	movl	(%r10), %eax
	cmpl	%r15d, %eax
	jg	.L115
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L168:
	movl	$17, %edx
	leaq	.LC29(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jle	.L161
	movl	$24, %edx
	leaq	.LC30(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jg	.L175
	.p2align 4,,10
	.p2align 3
.L161:
	xorl	%r12d, %r12d
.L101:
	xorl	%edi, %edi
	movl	$217, %edx
	leaq	.LC12(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L176
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	movl	$10, -84(%rbp)
	movl	$12, -88(%rbp)
	testb	$1, %bl
	jne	.L103
.L97:
	movl	$13, %edx
	leaq	.LC19(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jle	.L161
	movl	$10, %edx
	leaq	.LC20(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jle	.L161
	testb	$2, %bl
	jne	.L99
	.p2align 4,,10
	.p2align 3
.L164:
	movq	%r14, %rdi
	call	X509_get_version@PLT
	movq	%rax, %r8
	cmpq	$2, %rax
	ja	.L106
	leaq	1(%rax), %rcx
	leaq	.LC1(%rip), %rdx
	xorl	%eax, %eax
	movq	%r12, %rdi
	leaq	.LC21(%rip), %rsi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jg	.L99
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L166:
	movq	%r14, %rdi
	call	X509_get0_tbs_sigalg@PLT
	leaq	.LC27(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L161
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	X509_signature_print
	testl	%eax, %eax
	jg	.L108
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L106:
	movq	%rax, %rcx
	leaq	.LC1(%rip), %rdx
	xorl	%eax, %eax
	movq	%r12, %rdi
	leaq	.LC22(%rip), %rsi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jg	.L99
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L169:
	movl	-84(%rbp), %edx
	xorl	%eax, %eax
	leaq	.LC32(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L161
	movq	%r14, %rdi
	call	X509_get_subject_name@PLT
	movl	-88(%rbp), %edx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	X509_NAME_print_ex@PLT
	testl	%eax, %eax
	js	.L161
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jg	.L121
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L167:
	movl	-84(%rbp), %edx
	xorl	%eax, %eax
	leaq	.LC28(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L161
	movq	%r14, %rdi
	call	X509_get_issuer_name@PLT
	movl	-88(%rbp), %edx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	X509_NAME_print_ex@PLT
	testl	%eax, %eax
	js	.L161
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jg	.L117
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L170:
	movq	%r14, %rdi
	call	X509_get_X509_PUBKEY@PLT
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	-64(%rbp), %rdi
	xorl	%ecx, %ecx
	movq	%rax, %r8
	call	X509_PUBKEY_get0_param@PLT
	movl	$33, %edx
	leaq	.LC33(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jle	.L161
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rdx
	leaq	.LC34(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L161
	movq	-64(%rbp), %rsi
	movq	%r12, %rdi
	call	i2a_ASN1_OBJECT@PLT
	testl	%eax, %eax
	jle	.L161
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L161
	movq	%r14, %rdi
	call	X509_get0_pubkey@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L177
	xorl	%ecx, %ecx
	movl	$16, %edx
	movq	%r12, %rdi
	call	EVP_PKEY_print_public@PLT
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L175:
	movq	%r14, %rdi
	call	X509_get0_notBefore@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	ASN1_TIME_print@PLT
	testl	%eax, %eax
	je	.L161
	movl	$25, %edx
	leaq	.LC31(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jle	.L161
	movq	%r14, %rdi
	call	X509_get0_notAfter@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	ASN1_TIME_print@PLT
	testl	%eax, %eax
	je	.L161
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jg	.L119
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L174:
	movq	%rax, -96(%rbp)
	call	ERR_set_mark@PLT
	movq	-96(%rbp), %r10
	movq	%r10, %rdi
	movq	%r10, -104(%rbp)
	call	ASN1_INTEGER_get@PLT
	movq	%rax, -96(%rbp)
	call	ERR_pop_to_mark@PLT
	movq	-96(%rbp), %r9
	movq	-104(%rbp), %r10
	cmpq	$-1, %r9
	je	.L109
	cmpl	$258, 4(%r10)
	leaq	.LC1(%rip), %rdx
	jne	.L110
	negq	%r9
	leaq	.LC17(%rip), %rdx
.L110:
	xorl	%eax, %eax
	movq	%rdx, %r8
	movq	%r9, %rcx
	movq	%r12, %rdi
	leaq	.LC24(%rip), %rsi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jg	.L105
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L171:
	leaq	-64(%rbp), %rdx
	leaq	-72(%rbp), %rsi
	movq	%r14, %rdi
	call	X509_get0_uids@PLT
	cmpq	$0, -72(%rbp)
	je	.L132
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rdx
	leaq	.LC36(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L161
	movq	-72(%rbp), %rsi
	movl	$12, %edx
	movq	%r12, %rdi
	call	X509_signature_dump
	testl	%eax, %eax
	je	.L161
.L132:
	cmpq	$0, -64(%rbp)
	je	.L127
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rdx
	leaq	.LC37(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L161
	movq	-64(%rbp), %rsi
	movl	$12, %edx
	movq	%r12, %rdi
	call	X509_signature_dump
	testl	%eax, %eax
	jne	.L127
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L173:
	leaq	-72(%rbp), %rsi
	leaq	-64(%rbp), %rdi
	movq	%r14, %rdx
	call	X509_get0_signature@PLT
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rsi
	movq	%r12, %rdi
	call	X509_signature_print
	testl	%eax, %eax
	jg	.L134
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L172:
	movq	%r14, %rdi
	call	X509_get0_extensions@PLT
	movl	$8, %r8d
	movq	%rbx, %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	leaq	.LC38(%rip), %rsi
	call	X509V3_extensions_print@PLT
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L136:
	movq	%r14, %rdi
	call	X509_trusted@PLT
	testl	%eax, %eax
	je	.L162
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	X509_aux_print.part.0
	jmp	.L162
.L177:
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC35(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r12, %rdi
	call	ERR_print_errors@PLT
	jmp	.L123
.L176:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1323:
	.size	X509_print_ex, .-X509_print_ex
	.p2align 4
	.globl	X509_print_ex_fp
	.type	X509_print_ex_fp, @function
X509_print_ex_fp:
.LFB1321:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	testq	%rax, %rax
	je	.L182
	movq	%rax, %r12
	movq	%rbx, %rcx
	xorl	%edx, %edx
	movl	$106, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r15, %rcx
	movq	%r14, %rdx
	call	X509_print_ex
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	BIO_free@PLT
.L178:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L182:
	.cfi_restore_state
	movl	$32, %r8d
	leaq	.LC12(%rip), %rcx
	movl	$7, %edx
	xorl	%r13d, %r13d
	movl	$118, %esi
	movl	$11, %edi
	call	ERR_put_error@PLT
	jmp	.L178
	.cfi_endproc
.LFE1321:
	.size	X509_print_ex_fp, .-X509_print_ex_fp
	.p2align 4
	.globl	X509_print
	.type	X509_print, @function
X509_print:
.LFB1322:
	.cfi_startproc
	endbr64
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	jmp	X509_print_ex
	.cfi_endproc
.LFE1322:
	.size	X509_print, .-X509_print
	.p2align 4
	.globl	X509_print_fp
	.type	X509_print_fp, @function
X509_print_fp:
.LFB1320:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	testq	%rax, %rax
	je	.L188
	movq	%rax, %r12
	movq	%r14, %rcx
	xorl	%edx, %edx
	movl	$106, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	X509_print_ex
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	BIO_free@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L188:
	.cfi_restore_state
	movl	$32, %r8d
	leaq	.LC12(%rip), %rcx
	movl	$7, %edx
	xorl	%r13d, %r13d
	movl	$118, %esi
	movl	$11, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1320:
	.size	X509_print_fp, .-X509_print_fp
	.p2align 4
	.globl	X509_aux_print
	.type	X509_aux_print, @function
X509_aux_print:
.LFB1327:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$8, %rsp
	call	X509_trusted@PLT
	testl	%eax, %eax
	je	.L190
	movl	%r14d, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	X509_aux_print.part.0
.L190:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1327:
	.size	X509_aux_print, .-X509_aux_print
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
