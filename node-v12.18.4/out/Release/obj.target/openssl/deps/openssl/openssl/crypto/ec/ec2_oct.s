	.file	"ec2_oct.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ec/ec2_oct.c"
	.text
	.p2align 4
	.globl	ec_GF2m_simple_set_compressed_coordinates
	.type	ec_GF2m_simple_set_compressed_coordinates, @function
ec_GF2m_simple_set_compressed_coordinates:
.LFB377:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$56, %rsp
	movq	%rsi, -72(%rbp)
	movq	%rdx, -56(%rbp)
	movl	%ecx, -76(%rbp)
	call	ERR_clear_error@PLT
	testq	%r12, %r12
	je	.L40
.L2:
	movq	%r12, %rdi
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	BN_CTX_get@PLT
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L6
	leaq	72(%r13), %rax
	movq	-56(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%rax, -96(%rbp)
	call	BN_GF2m_mod_arr@PLT
	testl	%eax, %eax
	jne	.L41
.L6:
	xorl	%r15d, %r15d
.L5:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	movq	%rbx, %rdi
	call	BN_CTX_free@PLT
.L1:
	addq	$56, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movq	%r14, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L42
	movq	0(%r13), %rax
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	*272(%rax)
	testl	%eax, %eax
	je	.L6
	movq	0(%r13), %rax
	movq	104(%r13), %rdx
	movq	%r12, %r8
	movq	%r15, %rcx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	*280(%rax)
	testl	%eax, %eax
	je	.L6
	movq	96(%r13), %rsi
	movq	%r15, %rdx
	movq	%r15, %rdi
	call	BN_GF2m_add@PLT
	testl	%eax, %eax
	je	.L6
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	BN_GF2m_add@PLT
	testl	%eax, %eax
	je	.L6
	movq	-96(%rbp), %rdx
	movq	-88(%rbp), %rdi
	movq	%r15, %rsi
	movq	%r12, %rcx
	call	BN_GF2m_mod_solve_quad_arr@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L43
	movq	-88(%rbp), %rdi
	call	BN_is_odd@PLT
	movq	-88(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	%r12, %r8
	movl	%eax, %r15d
	movq	0(%r13), %rax
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L6
	movl	-76(%rbp), %eax
	testl	%eax, %eax
	setne	%dl
	testl	%r15d, %r15d
	setne	%al
	cmpb	%al, %dl
	je	.L10
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	movq	%rsi, %rdi
	call	BN_GF2m_add@PLT
	testl	%eax, %eax
	jne	.L10
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L40:
	call	BN_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L12
	movq	%rax, %rbx
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L42:
	movq	104(%r13), %rsi
	movq	-96(%rbp), %rdx
	movq	%r12, %rcx
	movq	-64(%rbp), %rdi
	call	BN_GF2m_mod_sqrt_arr@PLT
	testl	%eax, %eax
	je	.L6
.L10:
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	%r12, %r8
	movq	%r14, %rdx
	movq	%r13, %rdi
	xorl	%r15d, %r15d
	call	EC_POINT_set_affine_coordinates@PLT
	testl	%eax, %eax
	setne	%r15b
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L12:
	xorl	%r15d, %r15d
	jmp	.L1
.L43:
	call	ERR_peek_last_error@PLT
	movl	%eax, %edx
	shrl	$24, %edx
	cmpl	$3, %edx
	jne	.L9
	andl	$4095, %eax
	cmpl	$116, %eax
	je	.L44
.L9:
	movl	$84, %r8d
	movl	$3, %edx
	movl	$164, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L5
.L44:
	call	ERR_clear_error@PLT
	movl	$81, %r8d
	movl	$110, %edx
	leaq	.LC0(%rip), %rcx
	movl	$164, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L5
	.cfi_endproc
.LFE377:
	.size	ec_GF2m_simple_set_compressed_coordinates, .-ec_GF2m_simple_set_compressed_coordinates
	.p2align 4
	.globl	ec_GF2m_simple_point2oct
	.type	ec_GF2m_simple_point2oct, @function
ec_GF2m_simple_point2oct:
.LFB378:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %eax
	andl	$-5, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$56, %rsp
	movq	%rcx, -56(%rbp)
	cmpl	$2, %eax
	je	.L46
	cmpl	$4, %edx
	jne	.L101
.L46:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	EC_POINT_is_at_infinity@PLT
	movq	-64(%rbp), %rsi
	testl	%eax, %eax
	je	.L48
	cmpq	$0, -56(%rbp)
	je	.L74
	testq	%r15, %r15
	je	.L102
	movq	-56(%rbp), %rax
	movl	$1, %r14d
	movb	$0, (%rax)
.L45:
	addq	$56, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	movl	$126, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$104, %edx
	xorl	%r15d, %r15d
	movl	$161, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
.L47:
	movq	%r15, %rdi
	xorl	%r14d, %r14d
	call	BN_CTX_free@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L48:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	EC_GROUP_get_degree@PLT
	leal	14(%rax), %r14d
	addl	$7, %eax
	cmovs	%r14d, %eax
	sarl	$3, %eax
	cmpl	$2, %ebx
	movslq	%eax, %rdx
	leaq	1(%rdx), %rax
	leaq	1(%rdx,%rdx), %r11
	movq	%rdx, -72(%rbp)
	cmovne	%r11, %rax
	cmpq	$0, -56(%rbp)
	movq	%rax, %r14
	je	.L53
	cmpq	%r15, %rax
	movq	-64(%rbp), %rsi
	ja	.L103
	xorl	%r15d, %r15d
	testq	%r13, %r13
	je	.L104
.L55:
	movq	%r13, %rdi
	movq	%rsi, -88(%rbp)
	call	BN_CTX_start@PLT
	movq	%r13, %rdi
	call	BN_CTX_get@PLT
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	call	BN_CTX_get@PLT
	movq	%r13, %rdi
	movq	%rax, -80(%rbp)
	call	BN_CTX_get@PLT
	testq	%rax, %rax
	movq	%rax, -96(%rbp)
	je	.L65
	movq	-80(%rbp), %rcx
	movq	-64(%rbp), %rdx
	movq	%r13, %r8
	movq	%r12, %rdi
	movq	-88(%rbp), %rsi
	call	EC_POINT_get_affine_coordinates@PLT
	testl	%eax, %eax
	je	.L65
	movq	-56(%rbp), %rax
	movb	%bl, (%rax)
	cmpl	$4, %ebx
	je	.L58
	movq	-64(%rbp), %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L58
	movq	-96(%rbp), %r9
	movq	(%r12), %rax
	movq	%r13, %r8
	movq	%r12, %rdi
	movq	-64(%rbp), %rcx
	movq	-80(%rbp), %rdx
	movq	%r9, -88(%rbp)
	movq	%r9, %rsi
	call	*280(%rax)
	testl	%eax, %eax
	je	.L65
	movq	-88(%rbp), %r9
	movq	%r9, %rdi
	call	BN_is_odd@PLT
	testl	%eax, %eax
	je	.L58
	movq	-56(%rbp), %rax
	addb	$1, (%rax)
	.p2align 4,,10
	.p2align 3
.L58:
	movq	-64(%rbp), %rdi
	call	BN_num_bits@PLT
	movq	-72(%rbp), %rcx
	movl	%eax, %edx
	leal	14(%rax), %eax
	addl	$7, %edx
	cmovns	%edx, %eax
	sarl	$3, %eax
	cltq
	subq	%rax, %rcx
	jb	.L61
	testq	%rcx, %rcx
	je	.L105
	movq	-56(%rbp), %rax
	movq	%rcx, %rdx
	xorl	%esi, %esi
	movq	%rcx, -88(%rbp)
	leaq	1(%rax), %rdi
	call	memset@PLT
	movq	-88(%rbp), %rcx
	addq	$1, %rcx
.L64:
	movq	-56(%rbp), %rax
	movq	-64(%rbp), %rdi
	movq	%rcx, -88(%rbp)
	leaq	(%rax,%rcx), %rsi
	call	BN_bn2bin@PLT
	movq	-88(%rbp), %rcx
	movl	$194, %r8d
	cltq
	addq	%rax, %rcx
	movq	-72(%rbp), %rax
	addq	$1, %rax
	cmpq	%rcx, %rax
	jne	.L100
	andl	$-3, %ebx
	cmpl	$4, %ebx
	je	.L106
.L67:
	cmpq	%r14, %rcx
	jne	.L107
	movq	%r13, %rdi
	call	BN_CTX_end@PLT
	movq	%r15, -56(%rbp)
.L53:
	movq	-56(%rbp), %rdi
	call	BN_CTX_free@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L74:
	movl	$1, %r14d
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L102:
	movl	$134, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$100, %edx
	xorl	%r14d, %r14d
	movl	$161, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L45
.L107:
	movl	$214, %r8d
.L100:
	movl	$68, %edx
	movl	$161, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L65:
	movq	%r13, %rdi
	call	BN_CTX_end@PLT
	jmp	.L47
.L105:
	movl	$1, %ecx
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L104:
	movq	%rsi, -64(%rbp)
	call	BN_CTX_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L76
	movq	-64(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L103:
	movl	$151, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$100, %edx
	xorl	%r15d, %r15d
	movl	$161, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L47
.L106:
	movq	-80(%rbp), %rdi
	movq	%rcx, -64(%rbp)
	call	BN_num_bits@PLT
	movq	-72(%rbp), %rbx
	movl	%eax, %edx
	leal	14(%rax), %eax
	addl	$7, %edx
	cmovns	%edx, %eax
	sarl	$3, %eax
	cltq
	subq	%rax, %rbx
	jb	.L69
	testq	%rbx, %rbx
	movq	-64(%rbp), %rcx
	je	.L72
	movq	-56(%rbp), %rax
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%rcx, -64(%rbp)
	leaq	(%rax,%rcx), %rdi
	call	memset@PLT
	movq	-64(%rbp), %rcx
	addq	%rbx, %rcx
.L72:
	movq	-56(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movq	%rcx, -56(%rbp)
	addq	%rcx, %rsi
	call	BN_bn2bin@PLT
	movq	-56(%rbp), %rcx
	cltq
	addq	%rax, %rcx
	jmp	.L67
.L76:
	xorl	%r14d, %r14d
	jmp	.L45
.L69:
	movl	$202, %r8d
	jmp	.L100
.L61:
	movl	$184, %r8d
	jmp	.L100
	.cfi_endproc
.LFE378:
	.size	ec_GF2m_simple_point2oct, .-ec_GF2m_simple_point2oct
	.p2align 4
	.globl	ec_GF2m_simple_oct2point
	.type	ec_GF2m_simple_oct2point, @function
ec_GF2m_simple_oct2point:
.LFB379:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rcx, %rcx
	je	.L149
	movq	%rdx, %rbx
	movzbl	(%rdx), %edx
	movq	%rsi, %r14
	movq	%rdi, %r13
	movq	%r8, %r12
	movl	%edx, %esi
	movl	%edx, %r15d
	movl	%edx, %eax
	andl	$254, %esi
	andl	$1, %r15d
	movl	%esi, -52(%rbp)
	andl	$252, %eax
	je	.L111
	movl	$256, %r8d
	cmpl	$4, %eax
	jne	.L147
	andl	$250, %edx
	jne	.L127
.L144:
	movl	-52(%rbp), %eax
	movl	$260, %r8d
	testl	%eax, %eax
	sete	%al
	testb	%r15b, %r15b
	jne	.L147
	testb	%al, %al
	je	.L113
	movl	$266, %r8d
	cmpq	$1, %rcx
	je	.L150
.L147:
	movl	$102, %edx
	movl	$160, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L148:
	addq	$72, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	andl	$250, %edx
	je	.L144
.L113:
	movq	%r13, %rdi
	movq	%rcx, -72(%rbp)
	call	EC_GROUP_get_degree@PLT
	movq	-72(%rbp), %rcx
	movl	%eax, %edi
	movl	%eax, -84(%rbp)
	leal	14(%rax), %eax
	movl	%edi, %edx
	addl	$7, %edx
	cmovns	%edx, %eax
	sarl	$3, %eax
	cmpl	$2, -52(%rbp)
	movl	%eax, -56(%rbp)
	cltq
	movq	%rax, -64(%rbp)
	jne	.L116
	addq	$1, %rax
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L149:
	movl	$247, %r8d
	movl	$100, %edx
	movl	$160, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$72, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	movq	%r13, %rdi
	movq	%rcx, -72(%rbp)
	call	EC_GROUP_get_degree@PLT
	movq	-72(%rbp), %rcx
	movl	%eax, %edi
	movl	%eax, -84(%rbp)
	leal	14(%rax), %eax
	movl	%edi, %edx
	addl	$7, %edx
	cmovns	%edx, %eax
	sarl	$3, %eax
	movl	%eax, -56(%rbp)
	cltq
	movq	%rax, -64(%rbp)
.L116:
	leaq	1(%rax,%rax), %rax
.L117:
	cmpq	%rcx, %rax
	jne	.L151
	movq	$0, -80(%rbp)
	testq	%r12, %r12
	je	.L152
.L119:
	movq	%r12, %rdi
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	call	BN_CTX_get@PLT
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L146
	movq	-72(%rbp), %rdx
	movl	-56(%rbp), %esi
	leaq	1(%rbx), %rdi
	call	BN_bin2bn@PLT
	testq	%rax, %rax
	je	.L146
	movq	-72(%rbp), %rdi
	call	BN_num_bits@PLT
	movl	$300, %r8d
	cmpl	-84(%rbp), %eax
	jg	.L145
	cmpl	$2, -52(%rbp)
	movzbl	%r15b, %r15d
	je	.L153
	movq	-64(%rbp), %rax
	movl	-56(%rbp), %esi
	leaq	1(%rbx,%rax), %rdi
	movq	-96(%rbp), %rbx
	movq	%rbx, %rdx
	call	BN_bin2bn@PLT
	testq	%rax, %rax
	je	.L146
	movq	%rbx, %rdi
	call	BN_num_bits@PLT
	cmpl	-84(%rbp), %eax
	jg	.L154
	cmpl	$6, -52(%rbp)
	jne	.L126
	movq	-104(%rbp), %rbx
	movq	0(%r13), %rax
	movq	%r12, %r8
	movq	%r13, %rdi
	movq	-72(%rbp), %rcx
	movq	-96(%rbp), %rdx
	movq	%rbx, %rsi
	call	*280(%rax)
	testl	%eax, %eax
	je	.L146
	movq	%rbx, %rdi
	call	BN_is_odd@PLT
	movl	$318, %r8d
	cmpl	%r15d, %eax
	jne	.L145
.L126:
	movq	-96(%rbp), %rcx
	movq	-72(%rbp), %rdx
	movq	%r12, %r8
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	EC_POINT_set_affine_coordinates@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L150:
	addq	$72, %rsp
	movq	%r14, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	EC_POINT_set_to_infinity@PLT
.L154:
	.cfi_restore_state
	movl	$311, %r8d
.L145:
	movl	$102, %edx
	movl	$160, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L146:
	xorl	%eax, %eax
.L121:
	movq	%r12, %rdi
	movl	%eax, -52(%rbp)
	call	BN_CTX_end@PLT
	movq	-80(%rbp), %rdi
	call	BN_CTX_free@PLT
	movl	-52(%rbp), %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_restore_state
	movl	$280, %r8d
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L153:
	movq	-72(%rbp), %rdx
	movq	%r12, %r8
	movl	%r15d, %ecx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	EC_POINT_set_compressed_coordinates@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L152:
	call	BN_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L148
	movq	%rax, -80(%rbp)
	jmp	.L119
	.cfi_endproc
.LFE379:
	.size	ec_GF2m_simple_oct2point, .-ec_GF2m_simple_oct2point
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
