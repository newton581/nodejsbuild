	.file	"bio_meth.c"
	.text
	.p2align 4
	.globl	bwrite_conv
	.type	bwrite_conv, @function
bwrite_conv:
.LFB274:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2147483647, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rcx, %rbx
	subq	$8, %rsp
	cmpq	$2147483647, %rdx
	cmova	%rax, %rdx
	movq	(%rdi), %rax
	call	*24(%rax)
	xorl	%edx, %edx
	testl	%eax, %eax
	jle	.L2
	movslq	%eax, %rdx
	movl	$1, %eax
.L2:
	movq	%rdx, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE274:
	.size	bwrite_conv, .-bwrite_conv
	.p2align 4
	.globl	bread_conv
	.type	bread_conv, @function
bread_conv:
.LFB279:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2147483647, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rcx, %rbx
	subq	$8, %rsp
	cmpq	$2147483647, %rdx
	cmova	%rax, %rdx
	movq	(%rdi), %rax
	call	*40(%rax)
	xorl	%edx, %edx
	testl	%eax, %eax
	jle	.L8
	movslq	%eax, %rdx
	movl	$1, %eax
.L8:
	movq	%rdx, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE279:
	.size	bread_conv, .-bread_conv
	.p2align 4
	.type	do_bio_type_init_ossl_, @function
do_bio_type_init_ossl_:
.LFB267:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	CRYPTO_THREAD_lock_new@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	movq	%rax, bio_type_lock(%rip)
	setne	%al
	movzbl	%al, %eax
	movl	%eax, do_bio_type_init_ossl_ret_(%rip)
	ret
	.cfi_endproc
.LFE267:
	.size	do_bio_type_init_ossl_, .-do_bio_type_init_ossl_
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/bio/bio_meth.c"
	.text
	.p2align 4
	.globl	BIO_get_new_index
	.type	BIO_get_new_index, @function
BIO_get_new_index:
.LFB269:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	do_bio_type_init_ossl_(%rip), %rsi
	leaq	bio_type_init(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L15
	movl	do_bio_type_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	je	.L15
	movl	$1, %eax
	lock xaddl	%eax, bio_count.8383(%rip)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	addl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	movl	$28, %r8d
	movl	$65, %edx
	movl	$102, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE269:
	.size	BIO_get_new_index, .-BIO_get_new_index
	.p2align 4
	.globl	BIO_meth_new
	.type	BIO_meth_new, @function
BIO_meth_new:
.LFB270:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$38, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	leaq	.LC0(%rip), %rsi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	%edi, %ebx
	movl	$96, %edi
	subq	$8, %rsp
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L25
	movl	$41, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_strdup@PLT
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L25
	movl	%ebx, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$42, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%r12d, %r12d
	call	CRYPTO_free@PLT
	movl	$43, %r8d
	movl	$65, %edx
	leaq	.LC0(%rip), %rcx
	movl	$146, %esi
	movl	$32, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE270:
	.size	BIO_meth_new, .-BIO_meth_new
	.p2align 4
	.globl	BIO_meth_free
	.type	BIO_meth_free, @function
BIO_meth_free:
.LFB271:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L30
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$53, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$54, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L30:
	ret
	.cfi_endproc
.LFE271:
	.size	BIO_meth_free, .-BIO_meth_free
	.p2align 4
	.globl	BIO_meth_get_write
	.type	BIO_meth_get_write, @function
BIO_meth_get_write:
.LFB272:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE272:
	.size	BIO_meth_get_write, .-BIO_meth_get_write
	.p2align 4
	.globl	BIO_meth_get_write_ex
	.type	BIO_meth_get_write_ex, @function
BIO_meth_get_write_ex:
.LFB273:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE273:
	.size	BIO_meth_get_write_ex, .-BIO_meth_get_write_ex
	.p2align 4
	.globl	BIO_meth_set_write
	.type	BIO_meth_set_write, @function
BIO_meth_set_write:
.LFB275:
	.cfi_startproc
	endbr64
	leaq	bwrite_conv(%rip), %rax
	movq	%rsi, 24(%rdi)
	movq	%rax, 16(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE275:
	.size	BIO_meth_set_write, .-BIO_meth_set_write
	.p2align 4
	.globl	BIO_meth_set_write_ex
	.type	BIO_meth_set_write_ex, @function
BIO_meth_set_write_ex:
.LFB276:
	.cfi_startproc
	endbr64
	movq	$0, 24(%rdi)
	movl	$1, %eax
	movq	%rsi, 16(%rdi)
	ret
	.cfi_endproc
.LFE276:
	.size	BIO_meth_set_write_ex, .-BIO_meth_set_write_ex
	.p2align 4
	.globl	BIO_meth_get_read
	.type	BIO_meth_get_read, @function
BIO_meth_get_read:
.LFB277:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE277:
	.size	BIO_meth_get_read, .-BIO_meth_get_read
	.p2align 4
	.globl	BIO_meth_get_read_ex
	.type	BIO_meth_get_read_ex, @function
BIO_meth_get_read_ex:
.LFB278:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE278:
	.size	BIO_meth_get_read_ex, .-BIO_meth_get_read_ex
	.p2align 4
	.globl	BIO_meth_set_read
	.type	BIO_meth_set_read, @function
BIO_meth_set_read:
.LFB280:
	.cfi_startproc
	endbr64
	leaq	bread_conv(%rip), %rax
	movq	%rsi, 40(%rdi)
	movq	%rax, 32(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE280:
	.size	BIO_meth_set_read, .-BIO_meth_set_read
	.p2align 4
	.globl	BIO_meth_set_read_ex
	.type	BIO_meth_set_read_ex, @function
BIO_meth_set_read_ex:
.LFB281:
	.cfi_startproc
	endbr64
	movq	$0, 40(%rdi)
	movl	$1, %eax
	movq	%rsi, 32(%rdi)
	ret
	.cfi_endproc
.LFE281:
	.size	BIO_meth_set_read_ex, .-BIO_meth_set_read_ex
	.p2align 4
	.globl	BIO_meth_get_puts
	.type	BIO_meth_get_puts, @function
BIO_meth_get_puts:
.LFB282:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	ret
	.cfi_endproc
.LFE282:
	.size	BIO_meth_get_puts, .-BIO_meth_get_puts
	.p2align 4
	.globl	BIO_meth_set_puts
	.type	BIO_meth_set_puts, @function
BIO_meth_set_puts:
.LFB283:
	.cfi_startproc
	endbr64
	movq	%rsi, 48(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE283:
	.size	BIO_meth_set_puts, .-BIO_meth_set_puts
	.p2align 4
	.globl	BIO_meth_get_gets
	.type	BIO_meth_get_gets, @function
BIO_meth_get_gets:
.LFB284:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rax
	ret
	.cfi_endproc
.LFE284:
	.size	BIO_meth_get_gets, .-BIO_meth_get_gets
	.p2align 4
	.globl	BIO_meth_set_gets
	.type	BIO_meth_set_gets, @function
BIO_meth_set_gets:
.LFB285:
	.cfi_startproc
	endbr64
	movq	%rsi, 56(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE285:
	.size	BIO_meth_set_gets, .-BIO_meth_set_gets
	.p2align 4
	.globl	BIO_meth_get_ctrl
	.type	BIO_meth_get_ctrl, @function
BIO_meth_get_ctrl:
.LFB286:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	ret
	.cfi_endproc
.LFE286:
	.size	BIO_meth_get_ctrl, .-BIO_meth_get_ctrl
	.p2align 4
	.globl	BIO_meth_set_ctrl
	.type	BIO_meth_set_ctrl, @function
BIO_meth_set_ctrl:
.LFB287:
	.cfi_startproc
	endbr64
	movq	%rsi, 64(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE287:
	.size	BIO_meth_set_ctrl, .-BIO_meth_set_ctrl
	.p2align 4
	.globl	BIO_meth_get_create
	.type	BIO_meth_get_create, @function
BIO_meth_get_create:
.LFB288:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rax
	ret
	.cfi_endproc
.LFE288:
	.size	BIO_meth_get_create, .-BIO_meth_get_create
	.p2align 4
	.globl	BIO_meth_set_create
	.type	BIO_meth_set_create, @function
BIO_meth_set_create:
.LFB289:
	.cfi_startproc
	endbr64
	movq	%rsi, 72(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE289:
	.size	BIO_meth_set_create, .-BIO_meth_set_create
	.p2align 4
	.globl	BIO_meth_get_destroy
	.type	BIO_meth_get_destroy, @function
BIO_meth_get_destroy:
.LFB290:
	.cfi_startproc
	endbr64
	movq	80(%rdi), %rax
	ret
	.cfi_endproc
.LFE290:
	.size	BIO_meth_get_destroy, .-BIO_meth_get_destroy
	.p2align 4
	.globl	BIO_meth_set_destroy
	.type	BIO_meth_set_destroy, @function
BIO_meth_set_destroy:
.LFB291:
	.cfi_startproc
	endbr64
	movq	%rsi, 80(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE291:
	.size	BIO_meth_set_destroy, .-BIO_meth_set_destroy
	.p2align 4
	.globl	BIO_meth_get_callback_ctrl
	.type	BIO_meth_get_callback_ctrl, @function
BIO_meth_get_callback_ctrl:
.LFB292:
	.cfi_startproc
	endbr64
	movq	88(%rdi), %rax
	ret
	.cfi_endproc
.LFE292:
	.size	BIO_meth_get_callback_ctrl, .-BIO_meth_get_callback_ctrl
	.p2align 4
	.globl	BIO_meth_set_callback_ctrl
	.type	BIO_meth_set_callback_ctrl, @function
BIO_meth_set_callback_ctrl:
.LFB293:
	.cfi_startproc
	endbr64
	movq	%rsi, 88(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE293:
	.size	BIO_meth_set_callback_ctrl, .-BIO_meth_set_callback_ctrl
	.data
	.align 4
	.type	bio_count.8383, @object
	.size	bio_count.8383, 4
bio_count.8383:
	.long	128
	.local	do_bio_type_init_ossl_ret_
	.comm	do_bio_type_init_ossl_ret_,4,4
	.local	bio_type_init
	.comm	bio_type_init,4,4
	.globl	bio_type_lock
	.bss
	.align 8
	.type	bio_type_lock, @object
	.size	bio_type_lock, 8
bio_type_lock:
	.zero	8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
