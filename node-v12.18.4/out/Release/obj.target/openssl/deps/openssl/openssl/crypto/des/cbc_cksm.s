	.file	"cbc_cksm.c"
	.text
	.p2align 4
	.globl	DES_cbc_cksum
	.type	DES_cbc_cksum, @function
DES_cbc_cksum:
.LFB54:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -80(%rbp)
	movl	(%r8), %ecx
	movl	4(%r8), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	jle	.L2
	movq	%rdx, %r12
	movq	%rdx, %r13
	leaq	-8(%rdx), %rdx
	movq	%rdi, %rbx
	leaq	-1(%r12), %rax
	andl	$7, %r13d
	leaq	-64(%rbp), %r15
	andq	$-8, %rax
	subq	%rax, %rdx
	movq	%rdx, -72(%rbp)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L26:
	movl	(%rbx), %edx
	movl	4(%rbx), %eax
	movq	%r14, %rsi
	movq	%r15, %rdi
	addq	$8, %rbx
	subq	$8, %r12
	xorl	%ecx, %edx
	xorl	%r8d, %eax
	movl	%edx, -64(%rbp)
	movl	$1, %edx
	movl	%eax, -60(%rbp)
	call	DES_encrypt1@PLT
	movl	-64(%rbp), %ecx
	movl	-60(%rbp), %r8d
	cmpq	%r12, -72(%rbp)
	je	.L2
.L4:
	cmpq	%r12, %r13
	jne	.L26
	leaq	.L7(%rip), %r9
	addq	%r13, %rbx
	movslq	(%r9,%r13,4), %rdx
	movzbl	-1(%rbx), %eax
	addq	%r9, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L7:
	.long	.L15-.L7
	.long	.L15-.L7
	.long	.L16-.L7
	.long	.L17-.L7
	.long	.L10-.L7
	.long	.L18-.L7
	.long	.L19-.L7
	.long	.L6-.L7
	.text
	.p2align 4,,10
	.p2align 3
.L6:
	sall	$16, %eax
	subq	$1, %rbx
	movl	%eax, %r10d
	movzbl	-1(%rbx), %eax
.L8:
	sall	$8, %eax
	leaq	-1(%rbx), %r9
	movl	%eax, %edx
	movzbl	-2(%rbx), %eax
	orl	%r10d, %edx
	orl	%eax, %edx
.L9:
	movzbl	-2(%r9), %eax
	leaq	-1(%r9), %rbx
	xorl	%edx, %r8d
.L10:
	sall	$24, %eax
	leaq	-1(%rbx), %r10
	movl	%eax, %r9d
	movzbl	-2(%rbx), %eax
.L11:
	sall	$16, %eax
	leaq	-1(%r10), %rbx
	orl	%eax, %r9d
	movzbl	-2(%r10), %eax
.L12:
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	-2(%rbx), %eax
	orl	%r9d, %edx
.L5:
	orl	%eax, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movl	%r8d, -60(%rbp)
	xorl	%ecx, %edx
	movl	%edx, -64(%rbp)
	movl	$1, %edx
	call	DES_encrypt1@PLT
	movl	-64(%rbp), %ecx
	movl	-60(%rbp), %r8d
.L2:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L13
	movl	%ecx, (%rax)
	movl	%r8d, 4(%rax)
.L13:
	movq	-56(%rbp), %rsi
	xorq	%fs:40, %rsi
	movl	%r8d, %eax
	bswap	%eax
	jne	.L27
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	xorl	%r10d, %r10d
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L18:
	movl	%eax, %edx
	movq	%rbx, %r9
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L17:
	movq	%rbx, %r10
	xorl	%r9d, %r9d
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L16:
	xorl	%r9d, %r9d
	jmp	.L12
.L15:
	xorl	%edx, %edx
	jmp	.L5
.L27:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE54:
	.size	DES_cbc_cksum, .-DES_cbc_cksum
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
