	.file	"v3_ocsp.c"
	.text
	.p2align 4
	.type	i2r_ocsp_nocheck, @function
i2r_ocsp_nocheck:
.LFB1400:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1400:
	.size	i2r_ocsp_nocheck, .-i2r_ocsp_nocheck
	.p2align 4
	.type	i2d_ocsp_nonce, @function
i2d_ocsp_nonce:
.LFB1396:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	testq	%rsi, %rsi
	je	.L9
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%eax, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movq	(%rsi), %rdi
	movq	8(%r12), %rsi
	call	memcpy@PLT
	movslq	(%r12), %rdx
	addq	%rdx, (%rbx)
	popq	%rbx
	popq	%r12
	movq	%rdx, %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE1396:
	.size	i2d_ocsp_nonce, .-i2d_ocsp_nonce
	.p2align 4
	.type	ocsp_nonce_new, @function
ocsp_nonce_new:
.LFB1395:
	.cfi_startproc
	endbr64
	jmp	ASN1_OCTET_STRING_new@PLT
	.cfi_endproc
.LFE1395:
	.size	ocsp_nonce_new, .-ocsp_nonce_new
	.p2align 4
	.type	ocsp_nonce_free, @function
ocsp_nonce_free:
.LFB1398:
	.cfi_startproc
	endbr64
	jmp	ASN1_OCTET_STRING_free@PLT
	.cfi_endproc
.LFE1398:
	.size	ocsp_nonce_free, .-ocsp_nonce_free
	.p2align 4
	.type	s2i_ocsp_nocheck, @function
s2i_ocsp_nocheck:
.LFB1401:
	.cfi_startproc
	endbr64
	jmp	ASN1_NULL_new@PLT
	.cfi_endproc
.LFE1401:
	.size	s2i_ocsp_nocheck, .-s2i_ocsp_nocheck
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	"%*scrlUrl: "
.LC2:
	.string	"\n"
.LC3:
	.string	"%*scrlNum: "
.LC4:
	.string	"%*scrlTime: "
	.text
	.p2align 4
	.type	i2r_ocsp_crlid, @function
i2r_ocsp_crlid:
.LFB1392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	cmpq	$0, (%rsi)
	je	.L22
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rcx
	movl	%r13d, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L21
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	ASN1_STRING_print@PLT
	testl	%eax, %eax
	jne	.L31
.L21:
	xorl	%eax, %eax
.L15:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jle	.L21
	.p2align 4,,10
	.p2align 3
.L22:
	cmpq	$0, 8(%rbx)
	je	.L18
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rcx
	movl	%r13d, %edx
	movq	%r12, %rdi
	leaq	.LC3(%rip), %rsi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L21
	movq	8(%rbx), %rsi
	movq	%r12, %rdi
	call	i2a_ASN1_INTEGER@PLT
	testl	%eax, %eax
	jle	.L21
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jle	.L21
.L18:
	cmpq	$0, 16(%rbx)
	movl	$1, %eax
	je	.L15
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rcx
	movl	%r13d, %edx
	movq	%r12, %rdi
	leaq	.LC4(%rip), %rsi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L21
	movq	16(%rbx), %rsi
	movq	%r12, %rdi
	call	ASN1_GENERALIZEDTIME_print@PLT
	testl	%eax, %eax
	je	.L21
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
	jmp	.L15
	.cfi_endproc
.LFE1392:
	.size	i2r_ocsp_crlid, .-i2r_ocsp_crlid
	.section	.rodata.str1.1
.LC5:
	.string	"%*s"
	.text
	.p2align 4
	.type	i2r_ocsp_acutoff, @function
i2r_ocsp_acutoff:
.LFB1393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	leaq	.LC5(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	movl	%ecx, %edx
	leaq	.LC0(%rip), %rcx
	movq	%r12, %rdi
	call	BIO_printf@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L32
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	ASN1_GENERALIZEDTIME_print@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
.L32:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1393:
	.size	i2r_ocsp_acutoff, .-i2r_ocsp_acutoff
	.p2align 4
	.type	i2r_object, @function
i2r_object:
.LFB1394:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	leaq	.LC5(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	movl	%ecx, %edx
	leaq	.LC0(%rip), %rcx
	movq	%r12, %rdi
	call	BIO_printf@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L37
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	i2a_ASN1_OBJECT@PLT
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
.L37:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1394:
	.size	i2r_object, .-i2r_object
	.p2align 4
	.type	i2r_ocsp_nonce, @function
i2r_ocsp_nonce:
.LFB1399:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	leaq	.LC5(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	movl	%ecx, %edx
	leaq	.LC0(%rip), %rcx
	movq	%r12, %rdi
	call	BIO_printf@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L42
	movl	$4, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	i2a_ASN1_STRING@PLT
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
.L42:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1399:
	.size	i2r_ocsp_nonce, .-i2r_ocsp_nonce
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"../deps/openssl/openssl/crypto/ocsp/v3_ocsp.c"
	.text
	.p2align 4
	.type	d2i_ocsp_nonce, @function
d2i_ocsp_nonce:
.LFB1397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	testq	%rdi, %rdi
	je	.L48
	movq	(%rdi), %r12
	movq	%rdi, %r14
	testq	%r12, %r12
	je	.L84
.L78:
	movq	(%rbx), %rsi
	movl	%r13d, %edx
	movq	%r12, %rdi
	call	ASN1_OCTET_STRING_set@PLT
	testl	%eax, %eax
	je	.L54
	addq	%r13, (%rbx)
	movq	%r12, %rax
	movq	%r12, (%r14)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	call	ASN1_OCTET_STRING_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L78
	.p2align 4,,10
	.p2align 3
.L54:
	cmpq	%r12, (%r14)
	je	.L52
.L53:
	movq	%r12, %rdi
	call	ASN1_OCTET_STRING_free@PLT
.L52:
	xorl	%r12d, %r12d
	movl	$206, %r8d
	movl	$65, %edx
	movl	$102, %esi
	leaq	.LC6(%rip), %rcx
	movl	$39, %edi
	call	ERR_put_error@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	call	ASN1_OCTET_STRING_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L53
	movq	(%rbx), %rsi
	movl	%r13d, %edx
	movq	%rax, %rdi
	call	ASN1_OCTET_STRING_set@PLT
	testl	%eax, %eax
	je	.L53
	addq	%r13, (%rbx)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1397:
	.size	d2i_ocsp_nonce, .-d2i_ocsp_nonce
	.section	.rodata.str1.1
.LC7:
	.string	"%*sIssuer: "
.LC8:
	.string	"\n%*s"
.LC9:
	.string	" - "
	.text
	.p2align 4
	.type	i2r_ocsp_serviceloc, @function
i2r_ocsp_serviceloc:
.LFB1402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	leaq	.LC7(%rip), %rsi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	leaq	.LC0(%rip), %rcx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	movl	%r12d, %edx
	movq	%rbx, %rdi
	subq	$24, %rsp
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L88
	movq	(%r14), %rsi
	xorl	%edx, %edx
	movl	$8520479, %ecx
	movq	%rbx, %rdi
	call	X509_NAME_print_ex@PLT
	testl	%eax, %eax
	jle	.L88
	leal	(%r12,%r12), %eax
	xorl	%r13d, %r13d
	leaq	.LC0(%rip), %r15
	movl	%eax, -52(%rbp)
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L90:
	movq	8(%r14), %rdi
	movl	%r13d, %esi
	call	OPENSSL_sk_value@PLT
	movl	-52(%rbp), %edx
	movq	%r15, %rcx
	movq	%rbx, %rdi
	movq	%rax, %r12
	leaq	.LC8(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L88
	movq	(%r12), %rsi
	movq	%rbx, %rdi
	call	i2a_ASN1_OBJECT@PLT
	testl	%eax, %eax
	jle	.L88
	leaq	.LC9(%rip), %rsi
	movq	%rbx, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L88
	movq	8(%r12), %rsi
	movq	%rbx, %rdi
	call	GENERAL_NAME_print@PLT
	testl	%eax, %eax
	jle	.L88
	addl	$1, %r13d
.L89:
	movq	8(%r14), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jl	.L90
	movl	$1, %eax
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L88:
	xorl	%eax, %eax
.L85:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1402:
	.size	i2r_ocsp_serviceloc, .-i2r_ocsp_serviceloc
	.globl	v3_ocsp_serviceloc
	.section	.data.rel.ro,"aw"
	.align 32
	.type	v3_ocsp_serviceloc, @object
	.size	v3_ocsp_serviceloc, 104
v3_ocsp_serviceloc:
	.long	371
	.long	0
	.quad	OCSP_SERVICELOC_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	i2r_ocsp_serviceloc
	.quad	0
	.quad	0
	.globl	v3_ocsp_nocheck
	.align 32
	.type	v3_ocsp_nocheck, @object
	.size	v3_ocsp_nocheck, 104
v3_ocsp_nocheck:
	.long	369
	.long	0
	.quad	ASN1_NULL_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	s2i_ocsp_nocheck
	.quad	0
	.quad	0
	.quad	i2r_ocsp_nocheck
	.quad	0
	.quad	0
	.globl	v3_ocsp_nonce
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	v3_ocsp_nonce, @object
	.size	v3_ocsp_nonce, 104
v3_ocsp_nonce:
	.long	366
	.long	0
	.quad	0
	.quad	ocsp_nonce_new
	.quad	ocsp_nonce_free
	.quad	d2i_ocsp_nonce
	.quad	i2d_ocsp_nonce
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	i2r_ocsp_nonce
	.quad	0
	.quad	0
	.globl	v3_crl_hold
	.section	.data.rel.ro
	.align 32
	.type	v3_crl_hold, @object
	.size	v3_crl_hold, 104
v3_crl_hold:
	.long	430
	.long	0
	.quad	ASN1_OBJECT_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	i2r_object
	.quad	0
	.quad	0
	.globl	v3_crl_invdate
	.align 32
	.type	v3_crl_invdate, @object
	.size	v3_crl_invdate, 104
v3_crl_invdate:
	.long	142
	.long	0
	.quad	ASN1_GENERALIZEDTIME_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	i2r_ocsp_acutoff
	.quad	0
	.quad	0
	.globl	v3_ocsp_acutoff
	.align 32
	.type	v3_ocsp_acutoff, @object
	.size	v3_ocsp_acutoff, 104
v3_ocsp_acutoff:
	.long	370
	.long	0
	.quad	ASN1_GENERALIZEDTIME_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	i2r_ocsp_acutoff
	.quad	0
	.quad	0
	.globl	v3_ocsp_crlid
	.align 32
	.type	v3_ocsp_crlid, @object
	.size	v3_ocsp_crlid, 104
v3_ocsp_crlid:
	.long	367
	.long	0
	.quad	OCSP_CRLID_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	i2r_ocsp_crlid
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
