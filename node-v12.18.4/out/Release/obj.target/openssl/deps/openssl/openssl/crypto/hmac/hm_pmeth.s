	.file	"hm_pmeth.c"
	.text
	.p2align 4
	.type	pkey_hmac_ctrl, @function
pkey_hmac_ctrl:
.LFB1329:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	40(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$6, %esi
	je	.L2
	cmpl	$7, %esi
	je	.L3
	cmpl	$1, %esi
	je	.L4
	movl	$-2, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	testq	%rcx, %rcx
	sete	%dil
	testl	%edx, %edx
	setg	%sil
	testb	%sil, %dil
	jne	.L6
	cmpl	$-1, %edx
	jl	.L6
	leaq	8(%rax), %rdi
	movq	%rcx, %rsi
	call	ASN1_OCTET_STRING_set@PLT
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	movq	%rcx, (%rax)
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	movq	16(%rdi), %rdx
	movq	32(%rax), %r9
	movq	8(%rdi), %r8
	movq	(%rax), %rcx
	movq	40(%rdx), %rdx
	movq	%r9, %rdi
	movq	8(%rdx), %rsi
	movl	(%rdx), %edx
	call	HMAC_Init_ex@PLT
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1329:
	.size	pkey_hmac_ctrl, .-pkey_hmac_ctrl
	.p2align 4
	.type	hmac_signctx, @function
hmac_signctx:
.LFB1328:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rcx, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	subq	$16, %rsp
	movq	40(%r8), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	EVP_MD_CTX_md@PLT
	movq	%rax, %rdi
	call	EVP_MD_size@PLT
	testl	%eax, %eax
	js	.L12
	cltq
	movl	$1, %r14d
	movq	%rax, (%rbx)
	testq	%r13, %r13
	je	.L9
	movq	32(%r12), %rdi
	leaq	-44(%rbp), %rdx
	movq	%r13, %rsi
	call	HMAC_Final@PLT
	testl	%eax, %eax
	je	.L12
	movl	-44(%rbp), %eax
	movq	%rax, (%rbx)
.L9:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L20
	addq	$16, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	xorl	%r14d, %r14d
	jmp	.L9
.L20:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1328:
	.size	hmac_signctx, .-hmac_signctx
	.p2align 4
	.type	hmac_signctx_init, @function
hmac_signctx_init:
.LFB1327:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	movl	$-257, %esi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	40(%rdi), %rbx
	movq	%r12, %rdi
	call	EVP_MD_CTX_test_flags@PLT
	movq	32(%rbx), %rdi
	movslq	%eax, %rsi
	call	HMAC_CTX_set_flags@PLT
	movq	%r12, %rdi
	movl	$256, %esi
	call	EVP_MD_CTX_set_flags@PLT
	movq	%r12, %rdi
	leaq	int_update(%rip), %rsi
	call	EVP_MD_CTX_set_update_fn@PLT
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1327:
	.size	hmac_signctx_init, .-hmac_signctx_init
	.p2align 4
	.type	int_update, @function
int_update:
.LFB1326:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	call	EVP_MD_CTX_pkey_ctx@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	40(%rax), %rax
	movq	32(%rax), %rdi
	call	HMAC_Update@PLT
	popq	%r12
	popq	%r13
	testl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1326:
	.size	int_update, .-int_update
	.p2align 4
	.type	pkey_hmac_keygen, @function
pkey_hmac_keygen:
.LFB1325:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	cmpq	$0, 16(%rdi)
	je	.L35
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	call	ASN1_OCTET_STRING_dup@PLT
	movq	%rax, %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L25
	movl	$855, %esi
	movq	%r12, %rdi
	call	EVP_PKEY_assign@PLT
	movl	$1, %eax
.L25:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1325:
	.size	pkey_hmac_keygen, .-pkey_hmac_keygen
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/hmac/hm_pmeth.c"
	.text
	.p2align 4
	.type	pkey_hmac_cleanup, @function
pkey_hmac_cleanup:
.LFB1324:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	call	EVP_PKEY_CTX_get_data@PLT
	testq	%rax, %rax
	je	.L36
	movq	32(%rax), %rdi
	movq	%rax, %r12
	call	HMAC_CTX_free@PLT
	movslq	8(%r12), %rsi
	movq	16(%r12), %rdi
	movl	$80, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movq	%r12, %rdi
	movl	$81, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	popq	%r12
	movq	%r13, %rdi
	xorl	%esi, %esi
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	EVP_PKEY_CTX_set_data@PLT
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1324:
	.size	pkey_hmac_cleanup, .-pkey_hmac_cleanup
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"key"
.LC2:
	.string	"hexkey"
	.text
	.p2align 4
	.type	pkey_hmac_ctrl_str, @function
pkey_hmac_ctrl_str:
.LFB1330:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rax
	testq	%rdx, %rdx
	je	.L42
	movl	$4, %ecx
	leaq	.LC1(%rip), %rdi
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L44
	movq	%rax, %rsi
	movl	$7, %ecx
	leaq	.LC2(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L43
	movl	$6, %esi
	movq	%r8, %rdi
	jmp	EVP_PKEY_CTX_hex2ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L42:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	movl	$6, %esi
	movq	%r8, %rdi
	jmp	EVP_PKEY_CTX_str2ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L43:
	movl	$-2, %eax
	ret
	.cfi_endproc
.LFE1330:
	.size	pkey_hmac_ctrl_str, .-pkey_hmac_ctrl_str
	.p2align 4
	.type	pkey_hmac_init, @function
pkey_hmac_init:
.LFB1322:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$31, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$40, %edi
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L50
	movl	$4, 12(%rax)
	movq	%rax, %r12
	call	HMAC_CTX_new@PLT
	movq	%rax, 32(%r12)
	testq	%rax, %rax
	je	.L51
	movq	%r12, 40(%rbx)
	movl	$1, %eax
	movl	$0, 72(%rbx)
.L45:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	movl	$32, %r8d
	movl	$65, %edx
	movl	$123, %esi
	movl	$15, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	movl	$38, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	xorl	%eax, %eax
	jmp	.L45
	.cfi_endproc
.LFE1322:
	.size	pkey_hmac_init, .-pkey_hmac_init
	.p2align 4
	.type	pkey_hmac_copy, @function
pkey_hmac_copy:
.LFB1323:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$31, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	leaq	.LC0(%rip), %rsi
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movl	$40, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L66
	movl	$4, 12(%rax)
	movq	%rax, %r12
	call	HMAC_CTX_new@PLT
	movq	%rax, 32(%r12)
	testq	%rax, %rax
	je	.L67
	movq	%r12, 40(%r13)
	movq	%r14, %rdi
	movl	$0, 72(%r13)
	call	EVP_PKEY_CTX_get_data@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	EVP_PKEY_CTX_get_data@PLT
	movq	32(%rbx), %rsi
	movq	%rax, %r12
	movq	(%rbx), %rax
	movq	32(%r12), %rdi
	movq	%rax, (%r12)
	call	HMAC_CTX_copy@PLT
	testl	%eax, %eax
	je	.L59
	movq	16(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L60
	movl	8(%rbx), %edx
	leaq	8(%r12), %rdi
	call	ASN1_OCTET_STRING_set@PLT
	testl	%eax, %eax
	je	.L59
.L60:
	movl	$1, %eax
.L52:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	movq	%r13, %rdi
	call	EVP_PKEY_CTX_get_data@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L65
	movq	32(%rax), %rdi
	call	HMAC_CTX_free@PLT
	movslq	8(%r12), %rsi
	movq	16(%r12), %rdi
	movl	$80, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movl	$81, %edx
	call	CRYPTO_free@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	EVP_PKEY_CTX_set_data@PLT
.L65:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movl	$32, %r8d
	movl	$65, %edx
	movl	$123, %esi
	movl	$15, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	movl	$38, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	xorl	%eax, %eax
	jmp	.L52
	.cfi_endproc
.LFE1323:
	.size	pkey_hmac_copy, .-pkey_hmac_copy
	.globl	hmac_pkey_meth
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	hmac_pkey_meth, @object
	.size	hmac_pkey_meth, 256
hmac_pkey_meth:
	.long	855
	.long	0
	.quad	pkey_hmac_init
	.quad	pkey_hmac_copy
	.quad	pkey_hmac_cleanup
	.quad	0
	.quad	0
	.quad	0
	.quad	pkey_hmac_keygen
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	hmac_signctx_init
	.quad	hmac_signctx
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	pkey_hmac_ctrl
	.quad	pkey_hmac_ctrl_str
	.zero	48
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
