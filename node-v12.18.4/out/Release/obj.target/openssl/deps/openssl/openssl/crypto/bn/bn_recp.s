	.file	"bn_recp.c"
	.text
	.p2align 4
	.globl	BN_RECP_CTX_init
	.type	BN_RECP_CTX_init, @function
BN_RECP_CTX_init:
.LFB252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movups	%xmm0, (%rdi)
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 48(%rdi)
	call	bn_init@PLT
	addq	$8, %rsp
	leaq	24(%rbx), %rdi
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	bn_init@PLT
	.cfi_endproc
.LFE252:
	.size	BN_RECP_CTX_init, .-BN_RECP_CTX_init
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/bn/bn_recp.c"
	.text
	.p2align 4
	.globl	BN_RECP_CTX_new
	.type	BN_RECP_CTX_new, @function
BN_RECP_CTX_new:
.LFB253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edx
	movl	$64, %edi
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L8
	movq	%rax, %rdi
	call	bn_init@PLT
	leaq	24(%r12), %rdi
	call	bn_init@PLT
	movl	$1, 56(%r12)
.L4:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movl	$25, %r8d
	movl	$65, %edx
	movl	$150, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L4
	.cfi_endproc
.LFE253:
	.size	BN_RECP_CTX_new, .-BN_RECP_CTX_new
	.p2align 4
	.globl	BN_RECP_CTX_free
	.type	BN_RECP_CTX_free, @function
BN_RECP_CTX_free:
.LFB254:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L17
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	BN_free@PLT
	leaq	24(%r12), %rdi
	call	BN_free@PLT
	testb	$1, 56(%r12)
	jne	.L20
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$42, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L17:
	ret
	.cfi_endproc
.LFE254:
	.size	BN_RECP_CTX_free, .-BN_RECP_CTX_free
	.p2align 4
	.globl	BN_RECP_CTX_set
	.type	BN_RECP_CTX_set, @function
BN_RECP_CTX_set:
.LFB255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	BN_copy@PLT
	movq	%rax, %r8
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L21
	leaq	24(%rbx), %rdi
	xorl	%esi, %esi
	call	BN_set_word@PLT
	movq	%r12, %rdi
	call	BN_num_bits@PLT
	movl	$0, 52(%rbx)
	movl	%eax, 48(%rbx)
	movl	$1, %eax
.L21:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE255:
	.size	BN_RECP_CTX_set, .-BN_RECP_CTX_set
	.p2align 4
	.globl	BN_div_recp
	.type	BN_div_recp, @function
BN_div_recp:
.LFB257:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r8, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$40, %rsp
	call	BN_CTX_start@PLT
	testq	%r13, %r13
	je	.L77
	testq	%r14, %r14
	je	.L78
.L29:
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	BN_CTX_get@PLT
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	je	.L76
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	js	.L79
	movq	%r15, %rdi
	call	BN_num_bits@PLT
	movl	48(%rbx), %ecx
	leal	(%rcx,%rcx), %edx
	cmpl	%eax, %edx
	cmovge	%edx, %eax
	movl	%eax, -60(%rbp)
	cmpl	%eax, 52(%rbx)
	jne	.L80
.L35:
	cmpl	$-1, -60(%rbp)
	je	.L76
	movl	48(%rbx), %edx
	movq	-56(%rbp), %rdi
	movq	%r15, %rsi
	call	BN_rshift@PLT
	testl	%eax, %eax
	jne	.L81
	.p2align 4,,10
	.p2align 3
.L76:
	xorl	%r13d, %r13d
.L31:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
.L27:
	addq	$40, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	BN_set_word@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	BN_copy@PLT
	movq	%r12, %rdi
	testq	%rax, %rax
	je	.L82
	call	BN_CTX_end@PLT
	movl	$1, %r13d
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L81:
	movq	-56(%rbp), %rsi
	movq	-72(%rbp), %rdi
	leaq	24(%rbx), %rdx
	movq	%r12, %rcx
	call	BN_mul@PLT
	testl	%eax, %eax
	je	.L76
	movq	-72(%rbp), %rsi
	movl	-60(%rbp), %edx
	movq	%r13, %rdi
	subl	48(%rbx), %edx
	call	BN_rshift@PLT
	testl	%eax, %eax
	je	.L76
	movq	-72(%rbp), %rdi
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movl	$0, 16(%r13)
	call	BN_mul@PLT
	testl	%eax, %eax
	je	.L76
	movq	-72(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	BN_usub@PLT
	testl	%eax, %eax
	je	.L76
	movl	$0, 16(%r14)
	movl	$4, -56(%rbp)
.L40:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	js	.L83
	subl	$1, -56(%rbp)
	je	.L84
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	BN_usub@PLT
	testl	%eax, %eax
	je	.L76
	movl	$1, %esi
	movq	%r13, %rdi
	call	BN_add_word@PLT
	testl	%eax, %eax
	jne	.L40
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L82:
	call	BN_CTX_end@PLT
	xorl	%r13d, %r13d
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L80:
	leaq	24(%rbx), %rax
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	testq	%rax, %rax
	je	.L38
	movl	-60(%rbp), %esi
	movq	%rax, %rdi
	movq	%rax, -80(%rbp)
	call	BN_set_bit@PLT
	testl	%eax, %eax
	je	.L38
	movq	-80(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r12, %r8
	movq	%rbx, %rcx
	leaq	24(%rbx), %rdi
	call	BN_div@PLT
	testl	%eax, %eax
	je	.L38
.L39:
	endbr64
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	movl	-60(%rbp), %eax
	movl	%eax, 52(%rbx)
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L77:
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, %r13
	testq	%r14, %r14
	jne	.L29
	.p2align 4,,10
	.p2align 3
.L78:
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, %r14
	jmp	.L29
.L38:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	movl	$-1, 52(%rbx)
	jmp	.L76
.L84:
	movl	$149, %r8d
	movl	$101, %edx
	movl	$130, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L76
.L83:
	movq	%r14, %rdi
	call	BN_is_zero@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jne	.L43
	movl	16(%r15), %eax
.L43:
	movl	%eax, 16(%r14)
	movl	16(%r15), %eax
	xorl	16(%rbx), %eax
	movl	%eax, 16(%r13)
	movl	$1, %r13d
	jmp	.L31
	.cfi_endproc
.LFE257:
	.size	BN_div_recp, .-BN_div_recp
	.p2align 4
	.globl	BN_mod_mul_reciprocal
	.type	BN_mod_mul_reciprocal, @function
BN_mod_mul_reciprocal:
.LFB256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%r8, %rdi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	%rsi, -56(%rbp)
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	-56(%rbp), %rsi
	testq	%rax, %rax
	je	.L90
	testq	%rbx, %rbx
	je	.L91
	movq	%rax, %r13
	cmpq	%rsi, %rbx
	je	.L99
	movq	%r12, %rcx
	movq	%rbx, %rdx
	movq	%rax, %rdi
	call	BN_mul@PLT
	testl	%eax, %eax
	je	.L90
.L88:
	movq	%r13, %rdx
	movq	%r12, %r8
	movq	%r15, %rcx
	movq	%r14, %rsi
	xorl	%edi, %edi
	call	BN_div_recp
	movl	%eax, %r13d
.L87:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	BN_sqr@PLT
	testl	%eax, %eax
	jne	.L88
	.p2align 4,,10
	.p2align 3
.L90:
	xorl	%r13d, %r13d
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L91:
	movq	%rsi, %r13
	jmp	.L88
	.cfi_endproc
.LFE256:
	.size	BN_mod_mul_reciprocal, .-BN_mod_mul_reciprocal
	.p2align 4
	.globl	BN_reciprocal
	.type	BN_reciprocal, @function
BN_reciprocal:
.LFB258:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%rcx, %rdi
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	testq	%rax, %rax
	je	.L101
	movl	%r14d, %esi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	BN_set_bit@PLT
	testl	%eax, %eax
	je	.L101
	xorl	%esi, %esi
	movq	%r12, %r8
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	BN_div@PLT
	testl	%eax, %eax
	je	.L101
.L102:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	addq	$8, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	movl	$-1, %r14d
	jmp	.L102
	.cfi_endproc
.LFE258:
	.size	BN_reciprocal, .-BN_reciprocal
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
