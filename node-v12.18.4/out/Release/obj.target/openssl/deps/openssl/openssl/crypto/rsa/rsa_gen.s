	.file	"rsa_gen.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/rsa/rsa_gen.c"
	.text
	.p2align 4
	.type	rsa_builtin_keygen, @function
rsa_builtin_keygen:
.LFB447:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$511, %esi
	jle	.L224
	movl	%edx, %r13d
	cmpl	$1, %edx
	jle	.L5
	movq	%rdi, %r12
	movl	%esi, %edi
	movl	%esi, %ebx
	movq	%rcx, %r14
	call	rsa_multip_cap@PLT
	cmpl	%r13d, %eax
	jge	.L225
.L5:
	movl	$84, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$165, %edx
.L222:
	movl	$129, %esi
	movl	$4, %edi
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	call	ERR_put_error@PLT
.L3:
	movq	%r15, %rdi
	call	BN_CTX_end@PLT
	movq	%r15, %rdi
	call	BN_CTX_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L226
	addq	$136, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L225:
	.cfi_restore_state
	call	BN_CTX_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L60
	movq	%rax, %rdi
	call	BN_CTX_start@PLT
	movq	%r15, %rdi
	call	BN_CTX_get@PLT
	movq	%r15, %rdi
	movq	%rax, -152(%rbp)
	call	BN_CTX_get@PLT
	movq	%r15, %rdi
	movq	%rax, -104(%rbp)
	call	BN_CTX_get@PLT
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L60
	movl	%ebx, %eax
	cltd
	idivl	%r13d
	testl	%edx, %edx
	jg	.L227
	movl	%eax, -80(%rbp)
	movl	$1, %edx
.L69:
	movslq	%edx, %rcx
	movl	%eax, -80(%rbp,%rcx,4)
	leal	1(%rdx), %ecx
	cmpl	%r13d, %ecx
	jge	.L10
	movslq	%ecx, %rcx
	movl	%eax, -80(%rbp,%rcx,4)
	leal	2(%rdx), %ecx
	cmpl	%r13d, %ecx
	jge	.L10
	movslq	%ecx, %rcx
	addl	$3, %edx
	movl	%eax, -80(%rbp,%rcx,4)
	cmpl	%edx, %r13d
	jle	.L10
	movslq	%edx, %rdx
	movl	%eax, -80(%rbp,%rdx,4)
.L10:
	cmpq	$0, 24(%r12)
	je	.L11
.L14:
	cmpq	$0, 40(%r12)
	je	.L228
.L13:
	cmpq	$0, 32(%r12)
	je	.L229
.L16:
	cmpq	$0, 48(%r12)
	je	.L230
.L18:
	cmpq	$0, 56(%r12)
	je	.L231
.L20:
	cmpq	$0, 64(%r12)
	je	.L232
.L22:
	cmpq	$0, 72(%r12)
	je	.L233
.L24:
	cmpq	$0, 80(%r12)
	je	.L234
.L26:
	cmpl	$2, %r13d
	je	.L235
	movl	$1, 4(%r12)
	leal	-2(%r13), %esi
	xorl	%edi, %edi
	call	OPENSSL_sk_new_reserve@PLT
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L60
	movq	88(%r12), %rdi
	testq	%rdi, %rdi
	je	.L29
	movq	rsa_multip_info_free@GOTPCREL(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
.L29:
	movq	-136(%rbp), %rax
	movq	%r12, -112(%rbp)
	movl	$2, %ebx
	movq	%r14, -120(%rbp)
	movq	%rax, 88(%r12)
	movq	%rax, %r12
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L237:
	movq	%rax, %rsi
	movq	%r12, %rdi
	addl	$1, %ebx
	call	OPENSSL_sk_push@PLT
	cmpl	%ebx, %r13d
	jle	.L236
.L30:
	call	rsa_multip_info_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L237
	.p2align 4,,10
	.p2align 3
.L60:
	movl	$387, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$3, %edx
	xorl	%r12d, %r12d
	movl	$129, %esi
	movl	$4, %edi
	call	ERR_put_error@PLT
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L224:
	movl	$78, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$120, %edx
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L227:
	cmpl	%edx, %r13d
	leal	1(%rax), %ecx
	cmovle	%r13d, %edx
	movl	%ecx, -80(%rbp)
	cmpl	$1, %edx
	jle	.L71
	movl	%ecx, -76(%rbp)
	cmpl	$2, %edx
	je	.L9
	movl	%ecx, -72(%rbp)
	cmpl	$3, %edx
	je	.L9
	movl	%ecx, -68(%rbp)
	cmpl	$4, %edx
	je	.L9
	movl	%ecx, -64(%rbp)
	movl	$5, %edx
.L9:
	cmpl	%edx, %r13d
	jle	.L10
.L8:
	movslq	%edx, %rcx
	addl	$1, %edx
	movl	%eax, -80(%rbp,%rcx,4)
	cmpl	%edx, %r13d
	jle	.L10
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L235:
	movq	$0, -136(%rbp)
	movq	$0, -168(%rbp)
.L28:
	movq	32(%r12), %rdi
	movq	%r14, %rsi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L60
	movl	$0, -124(%rbp)
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	movq	%r15, -120(%rbp)
	movq	-136(%rbp), %r15
	movl	%r13d, -144(%rbp)
	movl	%eax, %r13d
.L53:
	testl	%r13d, %r13d
	je	.L51
	cmpl	$1, %r13d
	jne	.L33
.L46:
	movq	56(%r12), %rbx
	movl	$1, %r13d
.L32:
	movl	$4, %esi
	movq	%rbx, %rdi
	call	BN_set_flags@PLT
	movslq	%r13d, %rax
	movl	$0, -128(%rbp)
	movl	$0, -156(%rbp)
	movl	-80(%rbp,%rax,4), %eax
	addl	%eax, %r14d
	movl	%eax, -160(%rbp)
	movl	%eax, -112(%rbp)
	movl	%r14d, -140(%rbp)
	.p2align 4,,10
	.p2align 3
.L34:
	movq	-88(%rbp), %r9
	movl	-112(%rbp), %esi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	BN_generate_prime_ex@PLT
	testl	%eax, %eax
	je	.L214
	xorl	%r14d, %r14d
	testl	%r13d, %r13d
	jne	.L35
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L37:
	cmpl	$1, %r14d
	je	.L238
	leal	-2(%r14), %esi
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	movq	(%rax), %rsi
.L38:
	movq	%rbx, %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	je	.L34
	addl	$1, %r14d
	cmpl	%r13d, %r14d
	je	.L41
.L35:
	testl	%r14d, %r14d
	jne	.L37
	movq	48(%r12), %rsi
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L211:
	movl	%eax, %r14d
.L51:
	movq	48(%r12), %rbx
	xorl	%r13d, %r13d
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L238:
	movq	56(%r12), %rsi
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L41:
	call	BN_value_one@PLT
	movq	-96(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	call	BN_sub@PLT
	testl	%eax, %eax
	je	.L214
	call	ERR_set_mark@PLT
	movq	-96(%rbp), %r14
	movl	$4, %esi
	movq	%r14, %rdi
	call	BN_set_flags@PLT
	movq	-120(%rbp), %rcx
	movq	-104(%rbp), %rdi
	movq	%r14, %rsi
	movq	32(%r12), %rdx
	call	BN_mod_inverse@PLT
	testq	%rax, %rax
	je	.L239
	cmpl	$1, %r13d
	je	.L240
	testl	%r13d, %r13d
	je	.L45
	movq	24(%r12), %rsi
	movq	-120(%rbp), %rcx
	movq	%rbx, %rdx
	movq	-104(%rbp), %rdi
	call	BN_mul@PLT
	testl	%eax, %eax
	je	.L214
.L44:
	movl	-140(%rbp), %eax
	movq	-96(%rbp), %r14
	movq	-104(%rbp), %rsi
	leal	-4(%rax), %edx
	movq	%r14, %rdi
	call	BN_rshift@PLT
	testl	%eax, %eax
	je	.L214
	movq	%r14, %rdi
	call	BN_get_word@PLT
	movq	%rax, %r14
	leaq	-9(%rax), %rax
	cmpq	$6, %rax
	jbe	.L47
	movl	-124(%rbp), %edx
	movq	-88(%rbp), %rdi
	movl	$2, %esi
	leal	1(%rdx), %eax
	movl	%eax, -124(%rbp)
	call	BN_GENCB_call@PLT
	testl	%eax, %eax
	je	.L214
	cmpl	$5, -144(%rbp)
	je	.L241
	xorl	%eax, %eax
	cmpl	$4, -128(%rbp)
	je	.L211
.L50:
	addl	$1, -128(%rbp)
	jmp	.L34
.L239:
	call	ERR_peek_last_error@PLT
	movl	%eax, %edx
	shrl	$24, %edx
	cmpl	$3, %edx
	jne	.L214
	andl	$4095, %eax
	cmpl	$108, %eax
	jne	.L214
	call	ERR_pop_to_mark@PLT
	movl	-124(%rbp), %edx
	movq	-88(%rbp), %rdi
	movl	$2, %esi
	leal	1(%rdx), %r14d
	call	BN_GENCB_call@PLT
	testl	%eax, %eax
	je	.L214
	movl	%r14d, -124(%rbp)
	jmp	.L34
.L71:
	movl	$1, %edx
	jmp	.L8
.L240:
	movq	56(%r12), %rdx
	movq	48(%r12), %rsi
	movq	-120(%rbp), %rcx
	movq	-104(%rbp), %rdi
	call	BN_mul@PLT
	testl	%eax, %eax
	jne	.L44
.L214:
	movq	-120(%rbp), %r15
	jmp	.L60
.L236:
	movq	%r14, -168(%rbp)
	movq	-112(%rbp), %r12
	movq	-120(%rbp), %r14
	jmp	.L28
.L241:
	cmpq	$8, %r14
	ja	.L49
	addl	$1, -156(%rbp)
	movl	-160(%rbp), %ecx
	movl	-156(%rbp), %eax
	addl	%ecx, %eax
	movl	%eax, -112(%rbp)
	jmp	.L50
.L11:
	call	BN_new@PLT
	movq	%rax, 24(%r12)
	testq	%rax, %rax
	jne	.L14
	jmp	.L60
.L228:
	call	BN_secure_new@PLT
	movq	%rax, 40(%r12)
	testq	%rax, %rax
	jne	.L13
	jmp	.L60
.L229:
	call	BN_new@PLT
	movq	%rax, 32(%r12)
	testq	%rax, %rax
	jne	.L16
	jmp	.L60
.L230:
	call	BN_secure_new@PLT
	movq	%rax, 48(%r12)
	testq	%rax, %rax
	jne	.L18
	jmp	.L60
.L231:
	call	BN_secure_new@PLT
	movq	%rax, 56(%r12)
	testq	%rax, %rax
	jne	.L20
	jmp	.L60
.L232:
	call	BN_secure_new@PLT
	movq	%rax, 64(%r12)
	testq	%rax, %rax
	jne	.L22
	jmp	.L60
.L233:
	call	BN_secure_new@PLT
	movq	%rax, 72(%r12)
	testq	%rax, %rax
	jne	.L24
	jmp	.L60
.L234:
	call	BN_secure_new@PLT
	movq	%rax, 80(%r12)
	testq	%rax, %rax
	jne	.L26
	jmp	.L60
.L33:
	leal	-2(%r13), %esi
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, -168(%rbp)
	movq	(%rax), %rbx
	jmp	.L32
.L49:
	subl	$1, -156(%rbp)
	movl	-160(%rbp), %ecx
	movl	-156(%rbp), %eax
	addl	%ecx, %eax
	movl	%eax, -112(%rbp)
	jmp	.L50
.L47:
	movq	24(%r12), %rax
	cmpl	$1, %r13d
	jle	.L52
	movq	-168(%rbp), %rcx
	movq	%rax, %rsi
	movq	24(%rcx), %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L214
	movq	24(%r12), %rax
.L52:
	movq	-104(%rbp), %rsi
	movq	%rax, %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L214
	movq	-88(%rbp), %rdi
	movl	%r13d, %edx
	movl	$3, %esi
	call	BN_GENCB_call@PLT
	testl	%eax, %eax
	je	.L214
	movl	-144(%rbp), %eax
	addl	$1, %r13d
	cmpl	%r13d, %eax
	jle	.L242
	movl	-140(%rbp), %r14d
	jmp	.L53
.L45:
	movq	-88(%rbp), %rdi
	xorl	%edx, %edx
	movl	$3, %esi
	call	BN_GENCB_call@PLT
	testl	%eax, %eax
	je	.L214
	movl	-140(%rbp), %r14d
	jmp	.L46
.L242:
	movq	56(%r12), %rsi
	movq	48(%r12), %rdi
	movl	%eax, %r13d
	movq	-120(%rbp), %r15
	call	BN_cmp@PLT
	testl	%eax, %eax
	jns	.L54
	movdqu	48(%r12), %xmm0
	shufpd	$1, %xmm0, %xmm0
	movups	%xmm0, 48(%r12)
.L54:
	call	BN_value_one@PLT
	movq	-104(%rbp), %r14
	movq	48(%r12), %rsi
	movq	%rax, %rdx
	movq	%r14, %rdi
	call	BN_sub@PLT
	testl	%eax, %eax
	je	.L60
	call	BN_value_one@PLT
	movq	-96(%rbp), %rbx
	movq	56(%r12), %rsi
	movq	%rax, %rdx
	movq	%rbx, %rdi
	call	BN_sub@PLT
	testl	%eax, %eax
	je	.L60
	movq	-152(%rbp), %rdi
	movq	%r15, %rcx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	BN_mul@PLT
	testl	%eax, %eax
	je	.L60
	leal	-2(%r13), %eax
	xorl	%r14d, %r14d
	movl	%eax, -88(%rbp)
	cmpl	$2, %r13d
	jle	.L58
	movl	%r13d, %eax
	movq	%r12, %r13
	movl	%r14d, %r12d
	movl	%eax, %r14d
	jmp	.L57
.L244:
	movq	-152(%rbp), %rdi
	movq	8(%rbx), %rdx
	movq	%r15, %rcx
	movq	%rdi, %rsi
	call	BN_mul@PLT
	testl	%eax, %eax
	je	.L60
	addl	$1, %r12d
	cmpl	-88(%rbp), %r12d
	je	.L243
.L57:
	movq	-136(%rbp), %rdi
	movl	%r12d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rbx
	call	BN_value_one@PLT
	movq	8(%rbx), %rdi
	movq	(%rbx), %rsi
	movq	%rax, %rdx
	call	BN_sub@PLT
	testl	%eax, %eax
	jne	.L244
	jmp	.L60
.L226:
	call	__stack_chk_fail@PLT
.L243:
	movq	%r13, %r12
	movl	%r14d, %r13d
.L58:
	call	BN_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L60
	movq	-152(%rbp), %rsi
	movq	%rax, %rdi
	movl	$4, %edx
	call	BN_with_flags@PLT
	movq	40(%r12), %rdi
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	32(%r12), %rsi
	call	BN_mod_inverse@PLT
	movq	%r14, %rdi
	testq	%rax, %rax
	je	.L221
	call	BN_free@PLT
	call	BN_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L60
	movq	40(%r12), %rsi
	movl	$4, %edx
	movq	%rax, %rdi
	call	BN_with_flags@PLT
	movq	64(%r12), %rsi
	movq	-104(%rbp), %rcx
	xorl	%edi, %edi
	movq	%r15, %r8
	movq	%r14, %rdx
	call	BN_div@PLT
	testl	%eax, %eax
	je	.L220
	movq	72(%r12), %rsi
	movq	-96(%rbp), %rcx
	xorl	%edi, %edi
	movq	%r15, %r8
	movq	%r14, %rdx
	call	BN_div@PLT
	testl	%eax, %eax
	je	.L220
	movl	$2, %ebx
	jmp	.L63
.L65:
	movq	-136(%rbp), %rdi
	leal	-2(%rbx), %esi
	call	OPENSSL_sk_value@PLT
	xorl	%edi, %edi
	movq	%r15, %r8
	movq	%r14, %rdx
	movq	8(%rax), %rsi
	movq	%rsi, %rcx
	call	BN_div@PLT
	testl	%eax, %eax
	je	.L220
	addl	$1, %ebx
.L63:
	cmpl	%r13d, %ebx
	jl	.L65
	movq	%r14, %rdi
	call	BN_free@PLT
	call	BN_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L60
	movq	48(%r12), %rsi
	movq	%rax, %rdi
	movl	$4, %edx
	call	BN_with_flags@PLT
	movq	56(%r12), %rsi
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	80(%r12), %rdi
	movl	$2, %r12d
	call	BN_mod_inverse@PLT
	testq	%rax, %rax
	jne	.L66
	jmp	.L220
.L68:
	movq	-136(%rbp), %rdi
	leal	-2(%r12), %esi
	call	OPENSSL_sk_value@PLT
	movl	$4, %edx
	movq	%r14, %rdi
	movq	(%rax), %rsi
	movq	%rax, %rbx
	call	BN_with_flags@PLT
	movq	24(%rbx), %rsi
	movq	16(%rbx), %rdi
	movq	%r15, %rcx
	movq	%r14, %rdx
	call	BN_mod_inverse@PLT
	testq	%rax, %rax
	je	.L220
	addl	$1, %r12d
.L66:
	cmpl	%r13d, %r12d
	jl	.L68
	movq	%r14, %rdi
	movl	$1, %r12d
	call	BN_free@PLT
	jmp	.L3
.L220:
	movq	%r14, %rdi
.L221:
	call	BN_free@PLT
	jmp	.L60
	.cfi_endproc
.LFE447:
	.size	rsa_builtin_keygen, .-rsa_builtin_keygen
	.p2align 4
	.globl	RSA_generate_key_ex
	.type	RSA_generate_key_ex, @function
RSA_generate_key_ex:
.LFB445:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	movq	104(%r8), %rax
	testq	%rax, %rax
	je	.L246
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L246:
	movq	112(%r8), %rax
	movq	%rcx, %r8
	movq	%rdx, %rcx
	movl	$2, %edx
	testq	%rax, %rax
	je	.L247
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L247:
	jmp	rsa_builtin_keygen
	.cfi_endproc
.LFE445:
	.size	RSA_generate_key_ex, .-RSA_generate_key_ex
	.p2align 4
	.globl	RSA_generate_multi_prime_key
	.type	RSA_generate_multi_prime_key, @function
RSA_generate_multi_prime_key:
.LFB446:
	.cfi_startproc
	endbr64
	movq	%rcx, %r9
	movq	8(%rdi), %rcx
	movq	112(%rcx), %rax
	testq	%rax, %rax
	je	.L249
	movq	%r9, %rcx
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L249:
	movq	104(%rcx), %rax
	testq	%rax, %rax
	je	.L250
	cmpl	$2, %edx
	je	.L252
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L252:
	movq	%r8, %rcx
	movq	%r9, %rdx
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L250:
	movq	%r9, %rcx
	jmp	rsa_builtin_keygen
	.cfi_endproc
.LFE446:
	.size	RSA_generate_multi_prime_key, .-RSA_generate_multi_prime_key
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
