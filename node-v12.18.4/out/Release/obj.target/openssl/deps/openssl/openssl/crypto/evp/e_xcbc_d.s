	.file	"e_xcbc_d.c"
	.text
	.p2align 4
	.type	desx_cbc_cipher, @function
desx_cbc_cipher:
.LFB447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -96(%rbp)
	movabsq	$4611686018427387903, %rdx
	movq	%rsi, -88(%rbp)
	cmpq	%rdx, %rcx
	jbe	.L2
	movq	%rcx, %r15
	addq	%r14, %rax
	leaq	(%rsi,%rcx), %rcx
	movq	%rdx, -64(%rbp)
	movq	%rcx, -72(%rbp)
	movq	%rax, -80(%rbp)
	movq	%r14, -104(%rbp)
.L3:
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	pushq	%rbx
	movq	-56(%rbp), %rbx
	movq	%r13, %r8
	movq	-72(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movq	%rax, %rcx
	leaq	128(%r14), %r9
	addq	$136, %rbx
	movabsq	$4611686018427387904, %rdx
	pushq	%rbx
	subq	%r15, %rsi
	subq	%r15, %rdi
	call	DES_xcbc_encrypt@PLT
	popq	%rcx
	popq	%rsi
	movabsq	$-4611686018427387904, %rax
	addq	%rax, %r15
	cmpq	-64(%rbp), %r15
	ja	.L3
	movq	-104(%rbp), %r15
	movq	-64(%rbp), %r14
	leaq	(%r15,%rax), %r10
	andq	%r15, %r14
	shrq	$62, %r10
	addq	$1, %r10
	salq	$62, %r10
	addq	%r10, -96(%rbp)
	addq	%r10, -88(%rbp)
.L2:
	testq	%r14, %r14
	jne	.L13
.L4:
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	pushq	%rbx
	movq	-56(%rbp), %rbx
	movq	%r14, %rdx
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	%rax, %rcx
	movq	%r13, %r8
	addq	$136, %rbx
	leaq	128(%r15), %r9
	pushq	%rbx
	call	DES_xcbc_encrypt@PLT
	popq	%rax
	popq	%rdx
	jmp	.L4
	.cfi_endproc
.LFE447:
	.size	desx_cbc_cipher, .-desx_cbc_cipher
	.p2align 4
	.type	desx_cbc_init_key, @function
desx_cbc_init_key:
.LFB446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	DES_set_key_unchecked@PLT
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	8(%rbx), %rdx
	movq	%r12, %rdi
	movq	%rdx, 128(%rax)
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	16(%rbx), %rdx
	movq	%rdx, 136(%rax)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE446:
	.size	desx_cbc_init_key, .-desx_cbc_init_key
	.p2align 4
	.globl	EVP_desx_cbc
	.type	EVP_desx_cbc, @function
EVP_desx_cbc:
.LFB445:
	.cfi_startproc
	endbr64
	leaq	d_xcbc_cipher(%rip), %rax
	ret
	.cfi_endproc
.LFE445:
	.size	EVP_desx_cbc, .-EVP_desx_cbc
	.section	.data.rel.ro,"aw"
	.align 32
	.type	d_xcbc_cipher, @object
	.size	d_xcbc_cipher, 88
d_xcbc_cipher:
	.long	80
	.long	8
	.long	24
	.long	8
	.quad	2
	.quad	desx_cbc_init_key
	.quad	desx_cbc_cipher
	.quad	0
	.long	144
	.zero	4
	.quad	EVP_CIPHER_set_asn1_iv
	.quad	EVP_CIPHER_get_asn1_iv
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
