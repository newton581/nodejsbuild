	.file	"i_skey.c"
	.text
	.p2align 4
	.globl	IDEA_set_encrypt_key
	.type	IDEA_set_encrypt_key, @function
IDEA_set_encrypt_key:
.LFB0:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movzbl	(%rdi), %eax
	sall	$8, %eax
	movl	%eax, (%rsi)
	movzbl	1(%rdi), %edx
	orl	%edx, %eax
	movl	%eax, (%rsi)
	movzbl	2(%rdi), %r9d
	sall	$8, %r9d
	movl	%r9d, 4(%rsi)
	movzbl	3(%rdi), %edx
	orl	%edx, %r9d
	movl	%r9d, 4(%rsi)
	movzbl	4(%rdi), %r11d
	sall	$8, %r11d
	movl	%r11d, 8(%rsi)
	movzbl	5(%rdi), %edx
	orl	%edx, %r11d
	movl	%r11d, 8(%rsi)
	movzbl	6(%rdi), %r8d
	movl	%r11d, %r12d
	sall	$9, %r11d
	shrl	$7, %r12d
	sall	$8, %r8d
	movl	%r8d, 12(%rsi)
	movzbl	7(%rdi), %edx
	orl	%edx, %r8d
	movl	%r8d, 12(%rsi)
	movzbl	8(%rdi), %ebx
	sall	$8, %ebx
	movl	%ebx, 16(%rsi)
	movzbl	9(%rdi), %edx
	orl	%edx, %ebx
	movl	%ebx, 16(%rsi)
	movzbl	10(%rdi), %edx
	sall	$8, %edx
	movl	%edx, 20(%rsi)
	movzbl	11(%rdi), %ecx
	orl	%ecx, %edx
	movl	%edx, 20(%rsi)
	movzbl	12(%rdi), %r10d
	sall	$8, %r10d
	movl	%r10d, 24(%rsi)
	movzbl	13(%rdi), %ecx
	orl	%ecx, %r10d
	movl	%r10d, 24(%rsi)
	movzbl	14(%rdi), %ecx
	sall	$8, %ecx
	movl	%ecx, 28(%rsi)
	movzbl	15(%rdi), %edi
	orl	%edi, %ecx
	movl	%r9d, %edi
	sall	$9, %edi
	movl	%ecx, 28(%rsi)
	orl	%edi, %r12d
	movl	%r8d, %edi
	sall	$9, %r8d
	shrl	$7, %edi
	andl	$65535, %r12d
	orl	%edi, %r11d
	movl	%r12d, 32(%rsi)
	movzwl	%r11w, %edi
	movl	%ebx, %r11d
	sall	$9, %ebx
	shrl	$7, %r11d
	movl	%edi, 36(%rsi)
	orl	%r11d, %r8d
	movl	%edx, %r11d
	sall	$9, %edx
	shrl	$7, %r11d
	andl	$65535, %r8d
	orl	%r11d, %ebx
	movl	%r10d, %r11d
	movl	%r8d, 40(%rsi)
	andl	$65535, %ebx
	shrl	$7, %r11d
	sall	$9, %r10d
	orl	%r11d, %edx
	movl	%ecx, %r11d
	shrl	$7, %r9d
	shrl	$7, %r11d
	sall	$9, %ecx
	andl	$65535, %edx
	movl	%ebx, 44(%rsi)
	orl	%r11d, %r10d
	movl	%eax, %r11d
	sall	$9, %eax
	movl	%edx, 48(%rsi)
	shrl	$7, %r11d
	orl	%r9d, %eax
	movl	%edi, %r9d
	andl	$65535, %r10d
	orl	%r11d, %ecx
	movl	%r8d, %r11d
	sall	$9, %r9d
	andl	$65535, %eax
	shrl	$7, %r11d
	sall	$9, %r8d
	andl	$65535, %ecx
	movl	%eax, 60(%rsi)
	orl	%r11d, %r9d
	movl	%ebx, %r11d
	sall	$9, %ebx
	movl	%r10d, 52(%rsi)
	shrl	$7, %r11d
	andl	$65535, %r9d
	movl	%ecx, 56(%rsi)
	orl	%r11d, %r8d
	movl	%edx, %r11d
	sall	$9, %edx
	movl	%r9d, 64(%rsi)
	shrl	$7, %r11d
	andl	$65535, %r8d
	orl	%r11d, %ebx
	movl	%r10d, %r11d
	sall	$9, %r10d
	movl	%r8d, 68(%rsi)
	shrl	$7, %r11d
	andl	$65535, %ebx
	orl	%r11d, %edx
	movl	%ecx, %r11d
	movl	%ebx, 72(%rsi)
	andl	$65535, %edx
	shrl	$7, %r11d
	sall	$9, %ecx
	orl	%r11d, %r10d
	movl	%eax, %r11d
	shrl	$7, %edi
	shrl	$7, %r11d
	sall	$9, %eax
	andl	$65535, %r10d
	movl	%edx, 76(%rsi)
	orl	%r11d, %ecx
	movl	%r12d, %r11d
	sall	$9, %r12d
	movl	%r10d, 80(%rsi)
	shrl	$7, %r11d
	orl	%edi, %r12d
	movl	%r8d, %edi
	andl	$65535, %ecx
	orl	%r11d, %eax
	movl	%ebx, %r11d
	sall	$9, %edi
	andl	$65535, %r12d
	shrl	$7, %r11d
	sall	$9, %ebx
	andl	$65535, %eax
	movl	%ecx, 84(%rsi)
	orl	%r11d, %edi
	movl	%edx, %r11d
	sall	$9, %edx
	movl	%eax, 88(%rsi)
	shrl	$7, %r11d
	andl	$65535, %edi
	movl	%r12d, 92(%rsi)
	orl	%r11d, %ebx
	movl	%r10d, %r11d
	sall	$9, %r10d
	movl	%edi, 96(%rsi)
	shrl	$7, %r11d
	andl	$65535, %ebx
	orl	%r11d, %edx
	movl	%ecx, %r11d
	sall	$9, %ecx
	movl	%ebx, 100(%rsi)
	shrl	$7, %r11d
	andl	$65535, %edx
	orl	%r11d, %r10d
	movl	%eax, %r11d
	movl	%edx, 104(%rsi)
	andl	$65535, %r10d
	shrl	$7, %r11d
	sall	$9, %eax
	orl	%r11d, %ecx
	movl	%r12d, %r11d
	shrl	$7, %r8d
	shrl	$7, %r11d
	sall	$9, %r12d
	andl	$65535, %ecx
	movl	%r10d, 108(%rsi)
	orl	%r11d, %eax
	movl	%r9d, %r11d
	sall	$9, %r9d
	movl	%ecx, 112(%rsi)
	shrl	$7, %r11d
	orl	%r8d, %r9d
	movl	%ebx, %r8d
	andl	$65535, %eax
	orl	%r11d, %r12d
	movl	%edx, %r11d
	sall	$9, %r8d
	movl	%eax, 116(%rsi)
	shrl	$7, %r11d
	sall	$9, %edx
	andl	$65535, %r12d
	andl	$65535, %r9d
	orl	%r11d, %r8d
	movl	%r10d, %r11d
	sall	$9, %r10d
	movl	%r12d, 120(%rsi)
	shrl	$7, %r11d
	andl	$65535, %r8d
	movl	%r9d, 124(%rsi)
	orl	%r11d, %edx
	movl	%r8d, 128(%rsi)
	movzwl	%dx, %r11d
	movl	%ecx, %edx
	sall	$9, %ecx
	shrl	$7, %edx
	movl	%r11d, 132(%rsi)
	orl	%edx, %r10d
	movl	%eax, %edx
	sall	$9, %eax
	shrl	$7, %edx
	andl	$65535, %r10d
	orl	%edx, %ecx
	movl	%r12d, %edx
	movl	%r10d, 136(%rsi)
	shrl	$7, %edx
	andl	$65535, %ecx
	orl	%edx, %eax
	movl	%r9d, %edx
	sall	$9, %r12d
	movl	%ecx, 140(%rsi)
	shrl	$7, %edx
	shrl	$7, %ebx
	andl	$65535, %eax
	orl	%edx, %r12d
	sall	$9, %r9d
	movl	%eax, 144(%rsi)
	movzwl	%r12w, %edx
	movl	%edi, %r12d
	sall	$9, %edi
	shrl	$7, %r12d
	orl	%ebx, %edi
	movl	%r11d, %ebx
	movl	%edx, 148(%rsi)
	orl	%r12d, %r9d
	movl	%r10d, %r12d
	sall	$9, %ebx
	andl	$65535, %edi
	shrl	$7, %r12d
	andl	$65535, %r9d
	sall	$9, %r10d
	orl	%r12d, %ebx
	movl	%r9d, 152(%rsi)
	andl	$65535, %ebx
	movl	%edi, 156(%rsi)
	movl	%ebx, 160(%rsi)
	movl	%ecx, %ebx
	sall	$9, %ecx
	shrl	$7, %ebx
	orl	%ebx, %r10d
	movl	%eax, %ebx
	sall	$9, %eax
	shrl	$7, %ebx
	andl	$65535, %r10d
	orl	%ebx, %ecx
	movl	%edx, %ebx
	sall	$9, %edx
	movl	%r10d, 164(%rsi)
	shrl	$7, %ebx
	andl	$65535, %ecx
	orl	%ebx, %eax
	movl	%r9d, %ebx
	movl	%ecx, 168(%rsi)
	shrl	$7, %ebx
	andl	$65535, %eax
	orl	%ebx, %edx
	movl	%edi, %ebx
	movl	%eax, 172(%rsi)
	andl	$65535, %edx
	shrl	$7, %ebx
	sall	$9, %r9d
	shrl	$7, %r11d
	movl	%edx, 176(%rsi)
	sall	$9, %r10d
	orl	%ebx, %r9d
	movl	%r8d, %ebx
	sall	$9, %r8d
	shrl	$7, %ebx
	orl	%r11d, %r8d
	movl	%ecx, %r11d
	sall	$9, %ecx
	shrl	$7, %r11d
	andl	$65535, %r9d
	sall	$9, %edi
	andl	$65535, %r8d
	orl	%r11d, %r10d
	orl	%ebx, %edi
	popq	%rbx
	movl	%r8d, 188(%rsi)
	andl	$65535, %r10d
	andl	$65535, %edi
	popq	%r12
	movl	%r9d, 180(%rsi)
	movl	%r10d, 192(%rsi)
	movl	%eax, %r10d
	sall	$9, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	shrl	$7, %r10d
	movl	%edi, 184(%rsi)
	orl	%r10d, %ecx
	andl	$65535, %ecx
	movl	%ecx, 196(%rsi)
	movl	%eax, %ecx
	movl	%edx, %eax
	sall	$9, %edx
	shrl	$7, %eax
	orl	%eax, %ecx
	movl	%r9d, %eax
	shrl	$7, %eax
	andl	$65535, %ecx
	orl	%eax, %edx
	movl	%r9d, %eax
	movl	%ecx, 200(%rsi)
	andl	$65535, %edx
	sall	$9, %eax
	movl	%edx, 204(%rsi)
	movl	%edi, %edx
	shrl	$7, %edx
	orl	%edx, %eax
	andl	$65535, %eax
	shrl	$7, %r8d
	sall	$9, %edi
	movl	%eax, 208(%rsi)
	orl	%r8d, %edi
	andl	$65535, %edi
	movl	%edi, 212(%rsi)
	ret
	.cfi_endproc
.LFE0:
	.size	IDEA_set_encrypt_key, .-IDEA_set_encrypt_key
	.p2align 4
	.globl	IDEA_set_decrypt_key
	.type	IDEA_set_decrypt_key, @function
IDEA_set_decrypt_key:
.LFB1:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r11
	leaq	192(%rdi), %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	movl	$65537, %esi
	.p2align 4,,10
	.p2align 3
.L5:
	movl	(%r10), %r12d
	testl	%r12d, %r12d
	je	.L6
	movq	%rsi, %rax
	movl	%r12d, %ecx
	movl	$1, %r12d
	cqto
	idivq	%rcx
	movq	%rdx, %r8
	testq	%rdx, %rdx
	je	.L6
	movq	%rsi, %rax
	xorl	%r14d, %r14d
	movl	$1, %r13d
	subq	%rdx, %rax
	cqto
	idivq	%rcx
	.p2align 4,,10
	.p2align 3
.L10:
	imulq	%r13, %rax
	subq	%rax, %r14
	movq	%rcx, %rax
	cqto
	movq	%r14, %r12
	movq	%r13, %r14
	idivq	%r8
	movq	%rcx, %rax
	subq	%rdx, %rax
	movq	%rdx, %r9
	cqto
	idivq	%r8
	testq	%r9, %r9
	je	.L9
	movq	%r8, %rcx
	movq	%r12, %r13
	movq	%r9, %r8
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L9:
	leaq	65537(%r12), %rax
	testq	%r12, %r12
	cmovs	%eax, %r12d
.L6:
	movl	%r12d, (%r11)
	movl	8(%r10), %eax
	negl	%eax
	andl	$65535, %eax
	movl	%eax, 4(%r11)
	movl	4(%r10), %eax
	negl	%eax
	andl	$65535, %eax
	movl	%eax, 8(%r11)
	movl	12(%r10), %r12d
	testl	%r12d, %r12d
	je	.L11
	movq	%rsi, %rax
	movl	%r12d, %ecx
	movl	$1, %r12d
	cqto
	idivq	%rcx
	movq	%rdx, %r8
	testq	%rdx, %rdx
	je	.L11
	movq	%rsi, %rax
	xorl	%r14d, %r14d
	movl	$1, %r13d
	subq	%rdx, %rax
	cqto
	idivq	%rcx
	.p2align 4,,10
	.p2align 3
.L15:
	imulq	%r13, %rax
	subq	%rax, %r14
	movq	%rcx, %rax
	cqto
	movq	%r14, %r12
	movq	%r13, %r14
	idivq	%r8
	movq	%rcx, %rax
	subq	%rdx, %rax
	movq	%rdx, %r9
	cqto
	idivq	%r8
	testq	%r9, %r9
	je	.L14
	movq	%r8, %rcx
	movq	%r12, %r13
	movq	%r9, %r8
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L14:
	leaq	65537(%r12), %rax
	testq	%r12, %r12
	cmovs	%eax, %r12d
.L11:
	movl	%r12d, 12(%r11)
	cmpq	%rdi, %r10
	je	.L16
	movl	-8(%r10), %eax
	addq	$24, %r11
	subq	$24, %r10
	movl	%eax, -8(%r11)
	movl	20(%r10), %eax
	movl	%eax, -4(%r11)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L16:
	movl	4(%rbx), %eax
	movl	8(%rbx), %edx
	movl	%eax, 8(%rbx)
	movl	196(%rbx), %eax
	movl	%edx, 4(%rbx)
	movl	200(%rbx), %edx
	movl	%eax, 200(%rbx)
	movl	%edx, 196(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1:
	.size	IDEA_set_decrypt_key, .-IDEA_set_decrypt_key
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
