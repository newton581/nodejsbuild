	.file	"ocsp_ext.c"
	.text
	.p2align 4
	.globl	OCSP_REQUEST_get_ext_count
	.type	OCSP_REQUEST_get_ext_count, @function
OCSP_REQUEST_get_ext_count:
.LFB1392:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdi
	jmp	X509v3_get_ext_count@PLT
	.cfi_endproc
.LFE1392:
	.size	OCSP_REQUEST_get_ext_count, .-OCSP_REQUEST_get_ext_count
	.p2align 4
	.globl	OCSP_REQUEST_get_ext_by_NID
	.type	OCSP_REQUEST_get_ext_by_NID, @function
OCSP_REQUEST_get_ext_by_NID:
.LFB1393:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdi
	jmp	X509v3_get_ext_by_NID@PLT
	.cfi_endproc
.LFE1393:
	.size	OCSP_REQUEST_get_ext_by_NID, .-OCSP_REQUEST_get_ext_by_NID
	.p2align 4
	.globl	OCSP_REQUEST_get_ext_by_OBJ
	.type	OCSP_REQUEST_get_ext_by_OBJ, @function
OCSP_REQUEST_get_ext_by_OBJ:
.LFB1394:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdi
	jmp	X509v3_get_ext_by_OBJ@PLT
	.cfi_endproc
.LFE1394:
	.size	OCSP_REQUEST_get_ext_by_OBJ, .-OCSP_REQUEST_get_ext_by_OBJ
	.p2align 4
	.globl	OCSP_REQUEST_get_ext_by_critical
	.type	OCSP_REQUEST_get_ext_by_critical, @function
OCSP_REQUEST_get_ext_by_critical:
.LFB1395:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdi
	jmp	X509v3_get_ext_by_critical@PLT
	.cfi_endproc
.LFE1395:
	.size	OCSP_REQUEST_get_ext_by_critical, .-OCSP_REQUEST_get_ext_by_critical
	.p2align 4
	.globl	OCSP_REQUEST_get_ext
	.type	OCSP_REQUEST_get_ext, @function
OCSP_REQUEST_get_ext:
.LFB1396:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdi
	jmp	X509v3_get_ext@PLT
	.cfi_endproc
.LFE1396:
	.size	OCSP_REQUEST_get_ext, .-OCSP_REQUEST_get_ext
	.p2align 4
	.globl	OCSP_REQUEST_delete_ext
	.type	OCSP_REQUEST_delete_ext, @function
OCSP_REQUEST_delete_ext:
.LFB1397:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdi
	jmp	X509v3_delete_ext@PLT
	.cfi_endproc
.LFE1397:
	.size	OCSP_REQUEST_delete_ext, .-OCSP_REQUEST_delete_ext
	.p2align 4
	.globl	OCSP_REQUEST_get1_ext_d2i
	.type	OCSP_REQUEST_get1_ext_d2i, @function
OCSP_REQUEST_get1_ext_d2i:
.LFB1398:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdi
	jmp	X509V3_get_d2i@PLT
	.cfi_endproc
.LFE1398:
	.size	OCSP_REQUEST_get1_ext_d2i, .-OCSP_REQUEST_get1_ext_d2i
	.p2align 4
	.globl	OCSP_REQUEST_add1_ext_i2d
	.type	OCSP_REQUEST_add1_ext_i2d, @function
OCSP_REQUEST_add1_ext_i2d:
.LFB1399:
	.cfi_startproc
	endbr64
	addq	$24, %rdi
	jmp	X509V3_add1_i2d@PLT
	.cfi_endproc
.LFE1399:
	.size	OCSP_REQUEST_add1_ext_i2d, .-OCSP_REQUEST_add1_ext_i2d
	.p2align 4
	.globl	OCSP_REQUEST_add_ext
	.type	OCSP_REQUEST_add_ext, @function
OCSP_REQUEST_add_ext:
.LFB1400:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$24, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509v3_add_ext@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1400:
	.size	OCSP_REQUEST_add_ext, .-OCSP_REQUEST_add_ext
	.p2align 4
	.globl	OCSP_ONEREQ_get_ext_count
	.type	OCSP_ONEREQ_get_ext_count, @function
OCSP_ONEREQ_get_ext_count:
.LFB1401:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	jmp	X509v3_get_ext_count@PLT
	.cfi_endproc
.LFE1401:
	.size	OCSP_ONEREQ_get_ext_count, .-OCSP_ONEREQ_get_ext_count
	.p2align 4
	.globl	OCSP_ONEREQ_get_ext_by_NID
	.type	OCSP_ONEREQ_get_ext_by_NID, @function
OCSP_ONEREQ_get_ext_by_NID:
.LFB1402:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	jmp	X509v3_get_ext_by_NID@PLT
	.cfi_endproc
.LFE1402:
	.size	OCSP_ONEREQ_get_ext_by_NID, .-OCSP_ONEREQ_get_ext_by_NID
	.p2align 4
	.globl	OCSP_ONEREQ_get_ext_by_OBJ
	.type	OCSP_ONEREQ_get_ext_by_OBJ, @function
OCSP_ONEREQ_get_ext_by_OBJ:
.LFB1403:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	jmp	X509v3_get_ext_by_OBJ@PLT
	.cfi_endproc
.LFE1403:
	.size	OCSP_ONEREQ_get_ext_by_OBJ, .-OCSP_ONEREQ_get_ext_by_OBJ
	.p2align 4
	.globl	OCSP_ONEREQ_get_ext_by_critical
	.type	OCSP_ONEREQ_get_ext_by_critical, @function
OCSP_ONEREQ_get_ext_by_critical:
.LFB1404:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	jmp	X509v3_get_ext_by_critical@PLT
	.cfi_endproc
.LFE1404:
	.size	OCSP_ONEREQ_get_ext_by_critical, .-OCSP_ONEREQ_get_ext_by_critical
	.p2align 4
	.globl	OCSP_ONEREQ_get_ext
	.type	OCSP_ONEREQ_get_ext, @function
OCSP_ONEREQ_get_ext:
.LFB1405:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	jmp	X509v3_get_ext@PLT
	.cfi_endproc
.LFE1405:
	.size	OCSP_ONEREQ_get_ext, .-OCSP_ONEREQ_get_ext
	.p2align 4
	.globl	OCSP_ONEREQ_delete_ext
	.type	OCSP_ONEREQ_delete_ext, @function
OCSP_ONEREQ_delete_ext:
.LFB1406:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	jmp	X509v3_delete_ext@PLT
	.cfi_endproc
.LFE1406:
	.size	OCSP_ONEREQ_delete_ext, .-OCSP_ONEREQ_delete_ext
	.p2align 4
	.globl	OCSP_ONEREQ_get1_ext_d2i
	.type	OCSP_ONEREQ_get1_ext_d2i, @function
OCSP_ONEREQ_get1_ext_d2i:
.LFB1407:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	jmp	X509V3_get_d2i@PLT
	.cfi_endproc
.LFE1407:
	.size	OCSP_ONEREQ_get1_ext_d2i, .-OCSP_ONEREQ_get1_ext_d2i
	.p2align 4
	.globl	OCSP_ONEREQ_add1_ext_i2d
	.type	OCSP_ONEREQ_add1_ext_i2d, @function
OCSP_ONEREQ_add1_ext_i2d:
.LFB1408:
	.cfi_startproc
	endbr64
	addq	$8, %rdi
	jmp	X509V3_add1_i2d@PLT
	.cfi_endproc
.LFE1408:
	.size	OCSP_ONEREQ_add1_ext_i2d, .-OCSP_ONEREQ_add1_ext_i2d
	.p2align 4
	.globl	OCSP_ONEREQ_add_ext
	.type	OCSP_ONEREQ_add_ext, @function
OCSP_ONEREQ_add_ext:
.LFB1409:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509v3_add_ext@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1409:
	.size	OCSP_ONEREQ_add_ext, .-OCSP_ONEREQ_add_ext
	.p2align 4
	.globl	OCSP_BASICRESP_get_ext_count
	.type	OCSP_BASICRESP_get_ext_count, @function
OCSP_BASICRESP_get_ext_count:
.LFB1410:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	jmp	X509v3_get_ext_count@PLT
	.cfi_endproc
.LFE1410:
	.size	OCSP_BASICRESP_get_ext_count, .-OCSP_BASICRESP_get_ext_count
	.p2align 4
	.globl	OCSP_BASICRESP_get_ext_by_NID
	.type	OCSP_BASICRESP_get_ext_by_NID, @function
OCSP_BASICRESP_get_ext_by_NID:
.LFB1411:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	jmp	X509v3_get_ext_by_NID@PLT
	.cfi_endproc
.LFE1411:
	.size	OCSP_BASICRESP_get_ext_by_NID, .-OCSP_BASICRESP_get_ext_by_NID
	.p2align 4
	.globl	OCSP_BASICRESP_get_ext_by_OBJ
	.type	OCSP_BASICRESP_get_ext_by_OBJ, @function
OCSP_BASICRESP_get_ext_by_OBJ:
.LFB1412:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	jmp	X509v3_get_ext_by_OBJ@PLT
	.cfi_endproc
.LFE1412:
	.size	OCSP_BASICRESP_get_ext_by_OBJ, .-OCSP_BASICRESP_get_ext_by_OBJ
	.p2align 4
	.globl	OCSP_BASICRESP_get_ext_by_critical
	.type	OCSP_BASICRESP_get_ext_by_critical, @function
OCSP_BASICRESP_get_ext_by_critical:
.LFB1413:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	jmp	X509v3_get_ext_by_critical@PLT
	.cfi_endproc
.LFE1413:
	.size	OCSP_BASICRESP_get_ext_by_critical, .-OCSP_BASICRESP_get_ext_by_critical
	.p2align 4
	.globl	OCSP_BASICRESP_get_ext
	.type	OCSP_BASICRESP_get_ext, @function
OCSP_BASICRESP_get_ext:
.LFB1414:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	jmp	X509v3_get_ext@PLT
	.cfi_endproc
.LFE1414:
	.size	OCSP_BASICRESP_get_ext, .-OCSP_BASICRESP_get_ext
	.p2align 4
	.globl	OCSP_BASICRESP_delete_ext
	.type	OCSP_BASICRESP_delete_ext, @function
OCSP_BASICRESP_delete_ext:
.LFB1415:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	jmp	X509v3_delete_ext@PLT
	.cfi_endproc
.LFE1415:
	.size	OCSP_BASICRESP_delete_ext, .-OCSP_BASICRESP_delete_ext
	.p2align 4
	.globl	OCSP_BASICRESP_get1_ext_d2i
	.type	OCSP_BASICRESP_get1_ext_d2i, @function
OCSP_BASICRESP_get1_ext_d2i:
.LFB1416:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	jmp	X509V3_get_d2i@PLT
	.cfi_endproc
.LFE1416:
	.size	OCSP_BASICRESP_get1_ext_d2i, .-OCSP_BASICRESP_get1_ext_d2i
	.p2align 4
	.globl	OCSP_BASICRESP_add1_ext_i2d
	.type	OCSP_BASICRESP_add1_ext_i2d, @function
OCSP_BASICRESP_add1_ext_i2d:
.LFB1417:
	.cfi_startproc
	endbr64
	addq	$40, %rdi
	jmp	X509V3_add1_i2d@PLT
	.cfi_endproc
.LFE1417:
	.size	OCSP_BASICRESP_add1_ext_i2d, .-OCSP_BASICRESP_add1_ext_i2d
	.p2align 4
	.globl	OCSP_BASICRESP_add_ext
	.type	OCSP_BASICRESP_add_ext, @function
OCSP_BASICRESP_add_ext:
.LFB1418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$40, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509v3_add_ext@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1418:
	.size	OCSP_BASICRESP_add_ext, .-OCSP_BASICRESP_add_ext
	.p2align 4
	.globl	OCSP_SINGLERESP_get_ext_count
	.type	OCSP_SINGLERESP_get_ext_count, @function
OCSP_SINGLERESP_get_ext_count:
.LFB1419:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdi
	jmp	X509v3_get_ext_count@PLT
	.cfi_endproc
.LFE1419:
	.size	OCSP_SINGLERESP_get_ext_count, .-OCSP_SINGLERESP_get_ext_count
	.p2align 4
	.globl	OCSP_SINGLERESP_get_ext_by_NID
	.type	OCSP_SINGLERESP_get_ext_by_NID, @function
OCSP_SINGLERESP_get_ext_by_NID:
.LFB1420:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdi
	jmp	X509v3_get_ext_by_NID@PLT
	.cfi_endproc
.LFE1420:
	.size	OCSP_SINGLERESP_get_ext_by_NID, .-OCSP_SINGLERESP_get_ext_by_NID
	.p2align 4
	.globl	OCSP_SINGLERESP_get_ext_by_OBJ
	.type	OCSP_SINGLERESP_get_ext_by_OBJ, @function
OCSP_SINGLERESP_get_ext_by_OBJ:
.LFB1421:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdi
	jmp	X509v3_get_ext_by_OBJ@PLT
	.cfi_endproc
.LFE1421:
	.size	OCSP_SINGLERESP_get_ext_by_OBJ, .-OCSP_SINGLERESP_get_ext_by_OBJ
	.p2align 4
	.globl	OCSP_SINGLERESP_get_ext_by_critical
	.type	OCSP_SINGLERESP_get_ext_by_critical, @function
OCSP_SINGLERESP_get_ext_by_critical:
.LFB1422:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdi
	jmp	X509v3_get_ext_by_critical@PLT
	.cfi_endproc
.LFE1422:
	.size	OCSP_SINGLERESP_get_ext_by_critical, .-OCSP_SINGLERESP_get_ext_by_critical
	.p2align 4
	.globl	OCSP_SINGLERESP_get_ext
	.type	OCSP_SINGLERESP_get_ext, @function
OCSP_SINGLERESP_get_ext:
.LFB1423:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdi
	jmp	X509v3_get_ext@PLT
	.cfi_endproc
.LFE1423:
	.size	OCSP_SINGLERESP_get_ext, .-OCSP_SINGLERESP_get_ext
	.p2align 4
	.globl	OCSP_SINGLERESP_delete_ext
	.type	OCSP_SINGLERESP_delete_ext, @function
OCSP_SINGLERESP_delete_ext:
.LFB1424:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdi
	jmp	X509v3_delete_ext@PLT
	.cfi_endproc
.LFE1424:
	.size	OCSP_SINGLERESP_delete_ext, .-OCSP_SINGLERESP_delete_ext
	.p2align 4
	.globl	OCSP_SINGLERESP_get1_ext_d2i
	.type	OCSP_SINGLERESP_get1_ext_d2i, @function
OCSP_SINGLERESP_get1_ext_d2i:
.LFB1425:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdi
	jmp	X509V3_get_d2i@PLT
	.cfi_endproc
.LFE1425:
	.size	OCSP_SINGLERESP_get1_ext_d2i, .-OCSP_SINGLERESP_get1_ext_d2i
	.p2align 4
	.globl	OCSP_SINGLERESP_add1_ext_i2d
	.type	OCSP_SINGLERESP_add1_ext_i2d, @function
OCSP_SINGLERESP_add1_ext_i2d:
.LFB1426:
	.cfi_startproc
	endbr64
	addq	$32, %rdi
	jmp	X509V3_add1_i2d@PLT
	.cfi_endproc
.LFE1426:
	.size	OCSP_SINGLERESP_add1_ext_i2d, .-OCSP_SINGLERESP_add1_ext_i2d
	.p2align 4
	.globl	OCSP_SINGLERESP_add_ext
	.type	OCSP_SINGLERESP_add_ext, @function
OCSP_SINGLERESP_add_ext:
.LFB1427:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$32, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509v3_add_ext@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1427:
	.size	OCSP_SINGLERESP_add_ext, .-OCSP_SINGLERESP_add_ext
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ocsp/ocsp_ext.c"
	.text
	.p2align 4
	.globl	OCSP_request_add1_nonce
	.type	OCSP_request_add1_nonce, @function
OCSP_request_add1_nonce:
.LFB1429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	movl	$16, %eax
	movl	$4, %edx
	cmovle	%eax, %r12d
	xorl	%edi, %edi
	xorl	%r14d, %r14d
	movl	%r12d, %esi
	call	ASN1_object_size@PLT
	movl	%eax, -64(%rbp)
	testl	%eax, %eax
	js	.L42
	movslq	%eax, %rdi
	movl	$262, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, -56(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L45
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	leaq	-72(%rbp), %rdi
	movl	$4, %ecx
	movl	%r12d, %edx
	movq	%rax, -72(%rbp)
	call	ASN1_put_object@PLT
	testq	%r13, %r13
	je	.L46
	movq	-72(%rbp), %rdi
	movslq	%r12d, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
.L47:
	xorl	%ecx, %ecx
	leaq	-64(%rbp), %rdx
	leaq	24(%rbx), %rdi
	movl	$2, %r8d
	movl	$366, %esi
	xorl	%r14d, %r14d
	call	X509V3_add1_i2d@PLT
	testl	%eax, %eax
	setne	%r14b
.L53:
	movq	-56(%rbp), %rdi
.L45:
	movl	$276, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
.L42:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L54
	addq	$48, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	movq	-72(%rbp), %rdi
	movl	%r12d, %esi
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jg	.L47
	jmp	.L53
.L54:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1429:
	.size	OCSP_request_add1_nonce, .-OCSP_request_add1_nonce
	.p2align 4
	.globl	OCSP_basic_add1_nonce
	.type	OCSP_basic_add1_nonce, @function
OCSP_basic_add1_nonce:
.LFB1430:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	movl	$16, %eax
	movl	$4, %edx
	cmovle	%eax, %r12d
	xorl	%edi, %edi
	xorl	%r14d, %r14d
	movl	%r12d, %esi
	call	ASN1_object_size@PLT
	movl	%eax, -64(%rbp)
	testl	%eax, %eax
	js	.L55
	movslq	%eax, %rdi
	movl	$262, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, -56(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L58
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	leaq	-72(%rbp), %rdi
	movl	$4, %ecx
	movl	%r12d, %edx
	movq	%rax, -72(%rbp)
	call	ASN1_put_object@PLT
	testq	%r13, %r13
	je	.L59
	movq	-72(%rbp), %rdi
	movslq	%r12d, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
.L60:
	xorl	%ecx, %ecx
	leaq	-64(%rbp), %rdx
	leaq	40(%rbx), %rdi
	movl	$2, %r8d
	movl	$366, %esi
	xorl	%r14d, %r14d
	call	X509V3_add1_i2d@PLT
	testl	%eax, %eax
	setne	%r14b
.L66:
	movq	-56(%rbp), %rdi
.L58:
	movl	$276, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
.L55:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L67
	addq	$48, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	movq	-72(%rbp), %rdi
	movl	%r12d, %esi
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jg	.L60
	jmp	.L66
.L67:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1430:
	.size	OCSP_basic_add1_nonce, .-OCSP_basic_add1_nonce
	.p2align 4
	.globl	OCSP_check_nonce
	.type	OCSP_check_nonce, @function
OCSP_check_nonce:
.LFB1431:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	24(%rdi), %rdi
	movq	%rsi, %rbx
	movl	$366, %esi
	call	X509v3_get_ext_by_NID@PLT
	movq	40(%rbx), %rdi
	movl	$-1, %edx
	movl	$366, %esi
	movl	%eax, %r12d
	call	X509v3_get_ext_by_NID@PLT
	movl	%r12d, %ecx
	movl	%eax, %edx
	movl	%eax, %r13d
	shrl	$31, %ecx
	shrl	$31, %edx
	testl	%r12d, %r12d
	jns	.L79
	movl	$2, %eax
	testb	%dl, %dl
	jne	.L68
	testl	%r12d, %r12d
	jns	.L79
.L77:
	testl	%r13d, %r13d
	js	.L78
	movl	$3, %eax
	testb	%cl, %cl
	jne	.L68
.L78:
	movq	24(%r14), %rdi
	movl	%r12d, %esi
	call	X509v3_get_ext@PLT
	movq	40(%rbx), %rdi
	movl	%r13d, %esi
	movq	%rax, %r14
	call	X509v3_get_ext@PLT
	movq	%rax, %rdi
	call	X509_EXTENSION_get_data@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	X509_EXTENSION_get_data@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	ASN1_OCTET_STRING_cmp@PLT
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
.L68:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	testb	%dl, %dl
	je	.L77
	movl	$-1, %eax
	jmp	.L68
	.cfi_endproc
.LFE1431:
	.size	OCSP_check_nonce, .-OCSP_check_nonce
	.p2align 4
	.globl	OCSP_copy_nonce
	.type	OCSP_copy_nonce, @function
OCSP_copy_nonce:
.LFB1432:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	24(%rsi), %rdi
	movl	$366, %esi
	call	X509v3_get_ext_by_NID@PLT
	movl	%eax, %esi
	movl	$2, %eax
	testl	%esi, %esi
	js	.L81
	movq	24(%r12), %rdi
	call	X509v3_get_ext@PLT
	leaq	40(%rbx), %rdi
	movl	$-1, %edx
	movq	%rax, %rsi
	call	X509v3_add_ext@PLT
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
.L81:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1432:
	.size	OCSP_copy_nonce, .-OCSP_copy_nonce
	.p2align 4
	.globl	OCSP_crlID_new
	.type	OCSP_crlID_new, @function
OCSP_crlID_new:
.LFB1433:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	call	OCSP_CRLID_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L92
	testq	%r13, %r13
	je	.L93
	call	ASN1_IA5STRING_new@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L92
	movl	$-1, %edx
	movq	%r13, %rsi
	call	ASN1_STRING_set@PLT
	testl	%eax, %eax
	je	.L92
.L93:
	testq	%rbx, %rbx
	je	.L91
	call	ASN1_INTEGER_new@PLT
	movq	%rax, 8(%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L92
	movq	(%rbx), %rsi
	call	ASN1_INTEGER_set@PLT
	testl	%eax, %eax
	je	.L92
.L91:
	testq	%r14, %r14
	je	.L95
	call	ASN1_GENERALIZEDTIME_new@PLT
	movq	%rax, 16(%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L92
	movq	%r14, %rsi
	call	ASN1_GENERALIZEDTIME_set_string@PLT
	testl	%eax, %eax
	je	.L92
.L95:
	movq	%r12, %rdx
	xorl	%esi, %esi
	movl	$367, %edi
	call	X509V3_EXT_i2d@PLT
	movq	%rax, %r13
.L88:
	movq	%r12, %rdi
	call	OCSP_CRLID_free@PLT
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	xorl	%r13d, %r13d
	jmp	.L88
	.cfi_endproc
.LFE1433:
	.size	OCSP_crlID_new, .-OCSP_crlID_new
	.p2align 4
	.globl	OCSP_accept_responses_new
	.type	OCSP_accept_responses_new, @function
OCSP_accept_responses_new:
.LFB1434:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L122
	testq	%rbx, %rbx
	jne	.L132
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L120:
	addq	$8, %rbx
.L132:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L117
	call	OBJ_txt2nid@PLT
	testl	%eax, %eax
	je	.L120
	movl	%eax, %edi
	call	OBJ_nid2obj@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L120
	movq	%r12, %rdi
	call	OPENSSL_sk_push@PLT
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L117:
	movq	%r12, %rdx
	xorl	%esi, %esi
	movl	$368, %edi
	call	X509V3_EXT_i2d@PLT
	movq	%rax, %r13
.L116:
	movq	ASN1_OBJECT_free@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	call	OPENSSL_sk_pop_free@PLT
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L122:
	.cfi_restore_state
	xorl	%r13d, %r13d
	jmp	.L116
	.cfi_endproc
.LFE1434:
	.size	OCSP_accept_responses_new, .-OCSP_accept_responses_new
	.p2align 4
	.globl	OCSP_archive_cutoff_new
	.type	OCSP_archive_cutoff_new, @function
OCSP_archive_cutoff_new:
.LFB1435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	call	ASN1_GENERALIZEDTIME_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L136
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	ASN1_GENERALIZEDTIME_set_string@PLT
	testl	%eax, %eax
	je	.L136
	movq	%r12, %rdx
	xorl	%esi, %esi
	movl	$370, %edi
	call	X509V3_EXT_i2d@PLT
	movq	%rax, %r13
.L135:
	movq	%r12, %rdi
	call	ASN1_GENERALIZEDTIME_free@PLT
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	.cfi_restore_state
	xorl	%r13d, %r13d
	jmp	.L135
	.cfi_endproc
.LFE1435:
	.size	OCSP_archive_cutoff_new, .-OCSP_archive_cutoff_new
	.p2align 4
	.globl	OCSP_url_svcloc_new
	.type	OCSP_url_svcloc_new, @function
OCSP_url_svcloc_new:
.LFB1436:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$16, %rsp
	call	OCSP_SERVICELOC_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L144
	movq	(%rax), %rdi
	call	X509_NAME_free@PLT
	movq	%r12, %rdi
	call	X509_NAME_dup@PLT
	movq	%rax, (%r14)
	testq	%rax, %rax
	je	.L144
	testq	%rbx, %rbx
	je	.L150
	cmpq	$0, (%rbx)
	je	.L147
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 8(%r14)
	testq	%rax, %rax
	jne	.L147
	.p2align 4,,10
	.p2align 3
.L144:
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L149:
	call	ACCESS_DESCRIPTION_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L144
	movl	$178, %edi
	call	OBJ_nid2obj@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L151
	call	ASN1_IA5STRING_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L165
	movq	(%rbx), %rsi
	movl	$-1, %edx
	movq	%rax, %rdi
	call	ASN1_STRING_set@PLT
	testl	%eax, %eax
	je	.L165
	movq	8(%r12), %rax
	movq	8(%r14), %rdi
	movq	%r12, %rsi
	movl	$6, (%rax)
	movq	%r13, 8(%rax)
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L154
	addq	$8, %rbx
.L147:
	cmpq	$0, (%rbx)
	jne	.L149
.L150:
	movq	%r14, %rdx
	xorl	%esi, %esi
	movl	$371, %edi
	xorl	%r12d, %r12d
	call	X509V3_EXT_i2d@PLT
	xorl	%r13d, %r13d
.L143:
	movq	%r13, %rdi
	movq	%rax, -40(%rbp)
	call	ASN1_IA5STRING_free@PLT
	movq	%r12, %rdi
	call	ACCESS_DESCRIPTION_free@PLT
	movq	%r14, %rdi
	call	OCSP_SERVICELOC_free@PLT
	movq	-40(%rbp), %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L154:
	.cfi_restore_state
	xorl	%r13d, %r13d
.L165:
	xorl	%eax, %eax
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L151:
	xorl	%r13d, %r13d
	jmp	.L143
	.cfi_endproc
.LFE1436:
	.size	OCSP_url_svcloc_new, .-OCSP_url_svcloc_new
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
