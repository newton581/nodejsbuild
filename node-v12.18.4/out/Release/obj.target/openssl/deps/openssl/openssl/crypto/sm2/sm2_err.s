	.file	"sm2_err.c"
	.text
	.p2align 4
	.globl	ERR_load_SM2_strings
	.type	ERR_load_SM2_strings, @function
ERR_load_SM2_strings:
.LFB203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$889663488, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_func_error_string@PLT
	testq	%rax, %rax
	je	.L5
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	leaq	SM2_str_functs(%rip), %rdi
	call	ERR_load_strings_const@PLT
	leaq	SM2_str_reasons(%rip), %rdi
	call	ERR_load_strings_const@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE203:
	.size	ERR_load_SM2_strings, .-ERR_load_SM2_strings
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"asn1 error"
.LC1:
	.string	"bad signature"
.LC2:
	.string	"buffer too small"
.LC3:
	.string	"dist id too large"
.LC4:
	.string	"id not set"
.LC5:
	.string	"id too large"
.LC6:
	.string	"invalid curve"
.LC7:
	.string	"invalid digest"
.LC8:
	.string	"invalid digest type"
.LC9:
	.string	"invalid encoding"
.LC10:
	.string	"invalid field"
.LC11:
	.string	"no parameters set"
.LC12:
	.string	"user id too large"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	SM2_str_reasons, @object
	.size	SM2_str_reasons, 224
SM2_str_reasons:
	.quad	889192548
	.quad	.LC0
	.quad	889192549
	.quad	.LC1
	.quad	889192555
	.quad	.LC2
	.quad	889192558
	.quad	.LC3
	.quad	889192560
	.quad	.LC4
	.quad	889192559
	.quad	.LC5
	.quad	889192556
	.quad	.LC6
	.quad	889192550
	.quad	.LC7
	.quad	889192551
	.quad	.LC8
	.quad	889192552
	.quad	.LC9
	.quad	889192553
	.quad	.LC10
	.quad	889192557
	.quad	.LC11
	.quad	889192554
	.quad	.LC12
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC13:
	.string	"pkey_sm2_copy"
.LC14:
	.string	"pkey_sm2_ctrl"
.LC15:
	.string	"pkey_sm2_ctrl_str"
.LC16:
	.string	"pkey_sm2_digest_custom"
.LC17:
	.string	"pkey_sm2_init"
.LC18:
	.string	"pkey_sm2_sign"
.LC19:
	.string	"sm2_compute_msg_hash"
.LC20:
	.string	"sm2_compute_userid_digest"
.LC21:
	.string	"sm2_compute_z_digest"
.LC22:
	.string	"sm2_decrypt"
.LC23:
	.string	"sm2_encrypt"
.LC24:
	.string	"sm2_plaintext_size"
.LC25:
	.string	"sm2_sign"
.LC26:
	.string	"sm2_sig_gen"
.LC27:
	.string	"sm2_sig_verify"
.LC28:
	.string	"sm2_verify"
	.section	.data.rel.ro.local
	.align 32
	.type	SM2_str_functs, @object
	.size	SM2_str_functs, 272
SM2_str_functs:
	.quad	889663488
	.quad	.LC13
	.quad	889638912
	.quad	.LC14
	.quad	889643008
	.quad	.LC15
	.quad	889659392
	.quad	.LC16
	.quad	889647104
	.quad	.LC17
	.quad	889651200
	.quad	.LC18
	.quad	889602048
	.quad	.LC19
	.quad	889606144
	.quad	.LC20
	.quad	889655296
	.quad	.LC21
	.quad	889610240
	.quad	.LC22
	.quad	889614336
	.quad	.LC23
	.quad	889618432
	.quad	.LC24
	.quad	889622528
	.quad	.LC25
	.quad	889626624
	.quad	.LC26
	.quad	889630720
	.quad	.LC27
	.quad	889634816
	.quad	.LC28
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
