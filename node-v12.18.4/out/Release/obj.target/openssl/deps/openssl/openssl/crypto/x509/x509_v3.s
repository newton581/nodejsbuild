	.file	"x509_v3.c"
	.text
	.p2align 4
	.globl	X509v3_get_ext_count
	.type	X509v3_get_ext_count, @function
X509v3_get_ext_count:
.LFB1370:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L2
	jmp	OPENSSL_sk_num@PLT
	.p2align 4,,10
	.p2align 3
.L2:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1370:
	.size	X509v3_get_ext_count, .-X509v3_get_ext_count
	.p2align 4
	.globl	X509v3_get_ext_by_NID
	.type	X509v3_get_ext_by_NID, @function
X509v3_get_ext_by_NID:
.LFB1371:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movl	%esi, %edi
	call	OBJ_nid2obj@PLT
	testq	%rax, %rax
	je	.L9
	testq	%rbx, %rbx
	je	.L7
	addl	$1, %r12d
	movl	$0, %edx
	movq	%rbx, %rdi
	movq	%rax, %r13
	cmovs	%edx, %r12d
	call	OPENSSL_sk_num@PLT
	movl	%eax, %r14d
	cmpl	%eax, %r12d
	jl	.L8
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L15:
	addl	$1, %r12d
	cmpl	%r12d, %r14d
	je	.L7
.L8:
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r13, %rsi
	movq	(%rax), %rdi
	call	OBJ_cmp@PLT
	testl	%eax, %eax
	jne	.L15
.L4:
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	movl	$-1, %r12d
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L9:
	.cfi_restore_state
	movl	$-2, %r12d
	jmp	.L4
	.cfi_endproc
.LFE1371:
	.size	X509v3_get_ext_by_NID, .-X509v3_get_ext_by_NID
	.p2align 4
	.globl	X509v3_get_ext_by_OBJ
	.type	X509v3_get_ext_by_OBJ, @function
X509v3_get_ext_by_OBJ:
.LFB1372:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testq	%rdi, %rdi
	je	.L17
	addl	$1, %edx
	movl	$0, %r12d
	movq	%rdi, %rbx
	movq	%rsi, %r13
	cmovns	%edx, %r12d
	call	OPENSSL_sk_num@PLT
	movl	%eax, %r14d
	cmpl	%eax, %r12d
	jl	.L19
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L28:
	addl	$1, %r12d
	cmpl	%r14d, %r12d
	je	.L17
.L19:
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r13, %rsi
	movq	(%rax), %rdi
	call	OBJ_cmp@PLT
	testl	%eax, %eax
	jne	.L28
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movl	$-1, %r12d
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1372:
	.size	X509v3_get_ext_by_OBJ, .-X509v3_get_ext_by_OBJ
	.p2align 4
	.globl	X509v3_get_ext_by_critical
	.type	X509v3_get_ext_by_critical, @function
X509v3_get_ext_by_critical:
.LFB1373:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testq	%rdi, %rdi
	je	.L30
	addl	$1, %edx
	movl	$0, %r12d
	movq	%rdi, %r13
	movl	%esi, %ebx
	cmovns	%edx, %r12d
	call	OPENSSL_sk_num@PLT
	movl	%eax, %r14d
	cmpl	%eax, %r12d
	jge	.L30
	testl	%ebx, %ebx
	je	.L31
.L33:
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movl	8(%rax), %edx
	testl	%edx, %edx
	jle	.L44
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	addl	$1, %r12d
	cmpl	%r12d, %r14d
	jne	.L33
.L30:
	movl	$-1, %r12d
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	addl	$1, %r12d
	cmpl	%r12d, %r14d
	je	.L30
.L31:
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movl	8(%rax), %eax
	testl	%eax, %eax
	jg	.L45
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1373:
	.size	X509v3_get_ext_by_critical, .-X509v3_get_ext_by_critical
	.p2align 4
	.globl	X509v3_get_ext
	.type	X509v3_get_ext, @function
X509v3_get_ext:
.LFB1374:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L51
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jge	.L48
	testl	%r13d, %r13d
	js	.L48
	movl	%r13d, %esi
	movq	%r12, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	OPENSSL_sk_value@PLT
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1374:
	.size	X509v3_get_ext, .-X509v3_get_ext
	.p2align 4
	.globl	X509v3_delete_ext
	.type	X509v3_delete_ext, @function
X509v3_delete_ext:
.LFB1375:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L59
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jge	.L56
	testl	%r13d, %r13d
	js	.L56
	movl	%r13d, %esi
	movq	%r12, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	OPENSSL_sk_delete@PLT
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1375:
	.size	X509v3_delete_ext, .-X509v3_delete_ext
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/x509/x509_v3.c"
	.text
	.p2align 4
	.globl	X509v3_add_ext
	.type	X509v3_add_ext, @function
X509v3_add_ext:
.LFB1376:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdi, %rdi
	je	.L80
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rsi, %r15
	movl	%edx, %r14d
	testq	%r12, %r12
	je	.L81
.L65:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	movl	%eax, %r13d
	cmpl	%eax, %r14d
	jg	.L67
	testl	%r14d, %r14d
	cmovns	%r14d, %r13d
.L67:
	movq	%r15, %rdi
	call	X509_EXTENSION_dup@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L69
	movl	%r13d, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	OPENSSL_sk_insert@PLT
	testl	%eax, %eax
	je	.L66
	cmpq	$0, (%rbx)
	jne	.L62
	movq	%r12, (%rbx)
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L81:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L65
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L66:
	movl	$128, %r8d
	movl	$65, %edx
	movl	$104, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L69:
	movq	%r14, %rdi
	call	X509_EXTENSION_free@PLT
	cmpq	$0, (%rbx)
	je	.L70
	xorl	%r12d, %r12d
.L62:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	OPENSSL_sk_free@PLT
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L80:
	movl	$104, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$67, %edx
	xorl	%r12d, %r12d
	movl	$104, %esi
	movl	$11, %edi
	call	ERR_put_error@PLT
	xorl	%edi, %edi
	call	X509_EXTENSION_free@PLT
	jmp	.L62
	.cfi_endproc
.LFE1376:
	.size	X509v3_add_ext, .-X509v3_add_ext
	.p2align 4
	.globl	X509_EXTENSION_create_by_NID
	.type	X509_EXTENSION_create_by_NID, @function
X509_EXTENSION_create_by_NID:
.LFB1377:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	%esi, %edi
	subq	$8, %rsp
	call	OBJ_nid2obj@PLT
	testq	%rax, %rax
	je	.L114
	movq	%rax, %r13
	testq	%rbx, %rbx
	je	.L85
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L85
	movq	(%r12), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	%r13, %rdi
	call	OBJ_dup@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L93
.L92:
	cmpl	$1, %r15d
	movq	8(%r14), %rsi
	movl	(%r14), %edx
	leaq	16(%r12), %rdi
	sbbl	%eax, %eax
	orb	$-1, %al
	movl	%eax, 8(%r12)
	call	ASN1_OCTET_STRING_set@PLT
	testl	%eax, %eax
	je	.L90
	testq	%rbx, %rbx
	je	.L82
	cmpq	$0, (%rbx)
	je	.L115
.L82:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	call	X509_EXTENSION_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L116
	movq	(%rax), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	%r13, %rdi
	call	OBJ_dup@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	jne	.L92
	.p2align 4,,10
	.p2align 3
.L90:
	testq	%rbx, %rbx
	je	.L91
.L93:
	cmpq	%r12, (%rbx)
	je	.L88
.L91:
	movq	%r12, %rdi
	call	X509_EXTENSION_free@PLT
.L88:
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	call	ASN1_OBJECT_free@PLT
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L114:
	movl	$145, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$109, %edx
	xorl	%r12d, %r12d
	movl	$108, %esi
	movl	$11, %edi
	call	ERR_put_error@PLT
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L115:
	movq	%r12, (%rbx)
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L116:
	movl	$162, %r8d
	movl	$65, %edx
	movl	$109, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L88
	.cfi_endproc
.LFE1377:
	.size	X509_EXTENSION_create_by_NID, .-X509_EXTENSION_create_by_NID
	.p2align 4
	.globl	X509_EXTENSION_create_by_OBJ
	.type	X509_EXTENSION_create_by_OBJ, @function
X509_EXTENSION_create_by_OBJ:
.LFB1378:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rdi, %rdi
	je	.L118
	movq	(%rdi), %r12
	testq	%r12, %r12
	je	.L118
	testq	%rsi, %rsi
	jne	.L155
.L154:
	xorl	%r12d, %r12d
.L117:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L155:
	.cfi_restore_state
	movq	(%r12), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	%r13, %rdi
	call	OBJ_dup@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L126
.L127:
	cmpl	$1, %r14d
	movq	8(%r15), %rsi
	movl	(%r15), %edx
	leaq	16(%r12), %rdi
	sbbl	%eax, %eax
	orb	$-1, %al
	movl	%eax, 8(%r12)
	call	ASN1_OCTET_STRING_set@PLT
	testl	%eax, %eax
	je	.L122
	testq	%rbx, %rbx
	je	.L117
	cmpq	$0, (%rbx)
	jne	.L117
	movq	%r12, (%rbx)
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L118:
	call	X509_EXTENSION_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L156
	testq	%r13, %r13
	jne	.L157
.L122:
	testq	%rbx, %rbx
	je	.L124
.L126:
	cmpq	%r12, (%rbx)
	je	.L154
.L124:
	movq	%r12, %rdi
	call	X509_EXTENSION_free@PLT
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L157:
	movq	(%rax), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	%r13, %rdi
	call	OBJ_dup@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	jne	.L127
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L156:
	movl	$162, %r8d
	movl	$65, %edx
	movl	$109, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L117
	.cfi_endproc
.LFE1378:
	.size	X509_EXTENSION_create_by_OBJ, .-X509_EXTENSION_create_by_OBJ
	.p2align 4
	.globl	X509_EXTENSION_set_object
	.type	X509_EXTENSION_set_object, @function
X509_EXTENSION_set_object:
.LFB1379:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L162
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testq	%rsi, %rsi
	je	.L158
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	%r12, %rdi
	call	OBJ_dup@PLT
	testq	%rax, %rax
	movq	%rax, (%rbx)
	setne	%al
	movzbl	%al, %eax
.L158:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1379:
	.size	X509_EXTENSION_set_object, .-X509_EXTENSION_set_object
	.p2align 4
	.globl	X509_EXTENSION_set_critical
	.type	X509_EXTENSION_set_critical, @function
X509_EXTENSION_set_critical:
.LFB1380:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L169
	cmpl	$1, %esi
	sbbl	%eax, %eax
	orb	$-1, %al
	movl	%eax, 8(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L169:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1380:
	.size	X509_EXTENSION_set_critical, .-X509_EXTENSION_set_critical
	.p2align 4
	.globl	X509_EXTENSION_set_data
	.type	X509_EXTENSION_set_data, @function
X509_EXTENSION_set_data:
.LFB1381:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L175
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	8(%rsi), %r8
	addq	$16, %rdi
	movl	(%rsi), %edx
	movq	%r8, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ASN1_OCTET_STRING_set@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L175:
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1381:
	.size	X509_EXTENSION_set_data, .-X509_EXTENSION_set_data
	.p2align 4
	.globl	X509_EXTENSION_get_object
	.type	X509_EXTENSION_get_object, @function
X509_EXTENSION_get_object:
.LFB1382:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L182
	movq	(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L182:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1382:
	.size	X509_EXTENSION_get_object, .-X509_EXTENSION_get_object
	.p2align 4
	.globl	X509_EXTENSION_get_data
	.type	X509_EXTENSION_get_data, @function
X509_EXTENSION_get_data:
.LFB1383:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rax
	testq	%rdi, %rdi
	movl	$0, %edx
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE1383:
	.size	X509_EXTENSION_get_data, .-X509_EXTENSION_get_data
	.p2align 4
	.globl	X509_EXTENSION_get_critical
	.type	X509_EXTENSION_get_critical, @function
X509_EXTENSION_get_critical:
.LFB1384:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L188
	movl	8(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	setg	%al
	ret
	.p2align 4,,10
	.p2align 3
.L188:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1384:
	.size	X509_EXTENSION_get_critical, .-X509_EXTENSION_get_critical
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
