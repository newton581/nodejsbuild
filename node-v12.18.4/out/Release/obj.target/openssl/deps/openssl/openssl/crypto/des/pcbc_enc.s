	.file	"pcbc_enc.c"
	.text
	.p2align 4
	.globl	DES_pcbc_encrypt
	.type	DES_pcbc_encrypt, @function
DES_pcbc_encrypt:
.LFB54:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rcx, %rsi
	subq	$72, %rsp
	movl	(%r8), %r12d
	movl	4(%r8), %r13d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	je	.L2
	testq	%rdx, %rdx
	jle	.L1
	leaq	-64(%rbp), %rdi
	cmpq	$7, %rdx
	jg	.L6
.L5:
	leaq	(%r14,%r15), %rax
	movzbl	-1(%rax), %edx
	leaq	-1(%rax), %r8
	cmpq	$7, %r15
	ja	.L8
	leaq	.L10(%rip), %r9
	movslq	(%r9,%r15,4), %rcx
	addq	%r9, %rcx
	notrack jmp	*%rcx
	.section	.rodata
	.align 4
	.align 4
.L10:
	.long	.L8-.L10
	.long	.L8-.L10
	.long	.L28-.L10
	.long	.L29-.L10
	.long	.L30-.L10
	.long	.L31-.L10
	.long	.L32-.L10
	.long	.L9-.L10
	.text
	.p2align 4,,10
	.p2align 3
.L9:
	sall	$16, %edx
	subq	$2, %rax
	movl	%edx, %r10d
	movzbl	(%rax), %edx
.L11:
	movzbl	-2(%r8), %r9d
	movl	%edx, %ecx
	leaq	-1(%rax), %r8
	sall	$8, %ecx
	orl	%r10d, %ecx
	movzbl	%r9b, %edx
	orl	%edx, %ecx
.L12:
	movzbl	-2(%rax), %edx
	leaq	-1(%r8), %r9
	xorl	%ecx, %r13d
.L13:
	movl	%edx, %ecx
	movzbl	-2(%r8), %edx
	leaq	-1(%r9), %rax
	sall	$24, %ecx
.L14:
	sall	$16, %edx
	orl	%edx, %ecx
	movzbl	-2(%r9), %edx
.L15:
	movzbl	-2(%rax), %eax
	sall	$8, %edx
	orl	%ecx, %edx
	orl	%eax, %edx
.L8:
	xorl	%edx, %r12d
	movl	$1, %edx
	movl	%r13d, -60(%rbp)
	movl	%r12d, -64(%rbp)
	call	DES_encrypt1@PLT
	movl	-64(%rbp), %edx
	movl	-60(%rbp), %eax
	movl	%edx, %ecx
	movb	%dl, (%rbx)
	movb	%dh, 1(%rbx)
	shrl	$24, %edx
	shrl	$16, %ecx
	movb	%dl, 3(%rbx)
	movl	%eax, %edx
	movb	%al, 4(%rbx)
	shrl	$16, %edx
	movb	%ah, 5(%rbx)
	shrl	$24, %eax
	movb	%cl, 2(%rbx)
	movb	%dl, 6(%rbx)
	movb	%al, 7(%rbx)
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L41
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	testq	%rdx, %rdx
	jle	.L1
	movq	%rdx, %rax
	leaq	-64(%rbp), %rdi
	andl	$7, %eax
	movq	%rax, -96(%rbp)
	leaq	-8(%rdx), %rax
	leaq	-1(%rdx), %rdx
	andq	$-8, %rdx
	subq	%rdx, %rax
	movq	%rax, -104(%rbp)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L42:
	movl	%r12d, %edx
	movl	%r12d, %eax
	movl	-88(%rbp), %r8d
	movl	-84(%rbp), %ecx
	shrl	$16, %edx
	movb	%ah, 1(%rbx)
	movl	%r13d, %eax
	addq	$8, %rbx
	movb	%dl, -6(%rbx)
	movl	%r12d, %edx
	subq	$8, %r15
	movq	-72(%rbp), %rdi
	shrl	$24, %edx
	movb	%r12b, -8(%rbx)
	xorl	%r8d, %r12d
	movq	-80(%rbp), %rsi
	movb	%dl, -5(%rbx)
	movl	%r13d, %edx
	movb	%r13b, -4(%rbx)
	shrl	$16, %edx
	movb	%dl, -2(%rbx)
	movl	%r13d, %edx
	xorl	%ecx, %r13d
	movb	%ah, -3(%rbx)
	shrl	$24, %edx
	cmpq	-104(%rbp), %r15
	movb	%dl, -1(%rbx)
	je	.L1
.L18:
	movl	(%r14), %r8d
	movl	4(%r14), %ecx
	xorl	%edx, %edx
	movq	%rsi, -80(%rbp)
	movq	%rdi, -72(%rbp)
	addq	$8, %r14
	movl	%r8d, -64(%rbp)
	movl	%r8d, -88(%rbp)
	movl	%ecx, -60(%rbp)
	movl	%ecx, -84(%rbp)
	call	DES_encrypt1@PLT
	xorl	-64(%rbp), %r12d
	xorl	-60(%rbp), %r13d
	movl	%r12d, %edx
	cmpq	-96(%rbp), %r15
	jne	.L42
	addq	%r15, %rbx
	cmpq	$7, %r15
	ja	.L33
	leaq	.L21(%rip), %rcx
	movslq	(%rcx,%r15,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L21:
	.long	.L33-.L21
	.long	.L33-.L21
	.long	.L26-.L21
	.long	.L34-.L21
	.long	.L24-.L21
	.long	.L35-.L21
	.long	.L22-.L21
	.long	.L20-.L21
	.text
	.p2align 4,,10
	.p2align 3
.L6:
	movl	(%r14), %r8d
	movl	4(%r14), %eax
	movl	$1, %edx
	movq	%rsi, -80(%rbp)
	movq	%rdi, -72(%rbp)
	subq	$8, %r15
	addq	$8, %rbx
	addq	$8, %r14
	xorl	%r8d, %r12d
	xorl	%eax, %r13d
	movl	%r8d, -88(%rbp)
	movl	%r12d, -64(%rbp)
	movl	%eax, -84(%rbp)
	movl	%r13d, -60(%rbp)
	call	DES_encrypt1@PLT
	movl	-64(%rbp), %ecx
	movl	-88(%rbp), %r8d
	movl	-60(%rbp), %edx
	movl	-84(%rbp), %eax
	xorl	%ecx, %r8d
	movq	-72(%rbp), %rdi
	movq	-80(%rbp), %rsi
	movb	%cl, -8(%rbx)
	movb	%ch, -7(%rbx)
	movl	%r8d, %r12d
	movl	%ecx, %r8d
	shrl	$24, %ecx
	movb	%cl, -5(%rbx)
	movl	%edx, %ecx
	shrl	$16, %r8d
	xorl	%edx, %eax
	movb	%dl, -4(%rbx)
	shrl	$16, %ecx
	movl	%eax, %r13d
	movb	%dh, -3(%rbx)
	shrl	$24, %edx
	movb	%r8b, -6(%rbx)
	movb	%cl, -2(%rbx)
	cmpq	$7, %r15
	movb	%dl, -1(%rbx)
	jg	.L6
	testq	%r15, %r15
	jne	.L5
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L20:
	movl	%r13d, %eax
	subq	$1, %rbx
	shrl	$16, %eax
	movb	%al, (%rbx)
.L22:
	movl	%r13d, %ecx
	leaq	-1(%rbx), %rax
	movb	%ch, -1(%rbx)
.L23:
	movb	%r13b, -1(%rax)
	leaq	-1(%rax), %rbx
.L24:
	movl	%r12d, %ecx
	leaq	-1(%rbx), %rax
	shrl	$24, %ecx
	movb	%cl, -1(%rbx)
.L25:
	movl	%r12d, %ecx
	leaq	-1(%rax), %rbx
	shrl	$16, %ecx
	movb	%cl, -1(%rax)
.L26:
	movl	%r12d, %ecx
	leaq	-1(%rbx), %rax
	movb	%ch, -1(%rbx)
.L19:
	movb	%dl, -1(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L35:
	movq	%rbx, %rax
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L34:
	movq	%rbx, %rax
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L32:
	movq	%r8, %rcx
	xorl	%r10d, %r10d
	movq	%rax, %r8
	movq	%rcx, %rax
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L31:
	movl	%edx, %ecx
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L30:
	movq	%r8, %r9
	movq	%rax, %r8
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L29:
	movq	%rax, %r9
	xorl	%ecx, %ecx
	movq	%r8, %rax
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L28:
	xorl	%ecx, %ecx
	jmp	.L15
.L41:
	call	__stack_chk_fail@PLT
.L33:
	movq	%rbx, %rax
	jmp	.L19
	.cfi_endproc
.LFE54:
	.size	DES_pcbc_encrypt, .-DES_pcbc_encrypt
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
