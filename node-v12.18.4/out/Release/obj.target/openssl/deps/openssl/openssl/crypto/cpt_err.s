	.file	"cpt_err.c"
	.text
	.p2align 4
	.globl	ERR_load_CRYPTO_strings
	.type	ERR_load_CRYPTO_strings, @function
ERR_load_CRYPTO_strings:
.LFB203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$252149760, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_func_error_string@PLT
	testq	%rax, %rax
	je	.L5
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	leaq	CRYPTO_str_functs(%rip), %rdi
	call	ERR_load_strings_const@PLT
	leaq	CRYPTO_str_reasons(%rip), %rdi
	call	ERR_load_strings_const@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE203:
	.size	ERR_load_CRYPTO_strings, .-ERR_load_CRYPTO_strings
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"fips mode not supported"
.LC1:
	.string	"illegal hex digit"
.LC2:
	.string	"odd number of digits"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	CRYPTO_str_reasons, @object
	.size	CRYPTO_str_reasons, 64
CRYPTO_str_reasons:
	.quad	251658341
	.quad	.LC0
	.quad	251658342
	.quad	.LC1
	.quad	251658343
	.quad	.LC2
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC3:
	.string	"CMAC_CTX_new"
.LC4:
	.string	"CRYPTO_dup_ex_data"
.LC5:
	.string	"CRYPTO_free_ex_data"
.LC6:
	.string	"CRYPTO_get_ex_new_index"
.LC7:
	.string	"CRYPTO_memdup"
.LC8:
	.string	"CRYPTO_new_ex_data"
.LC9:
	.string	"CRYPTO_ocb128_copy_ctx"
.LC10:
	.string	"CRYPTO_ocb128_init"
.LC11:
	.string	"CRYPTO_set_ex_data"
.LC12:
	.string	"FIPS_mode_set"
.LC13:
	.string	"get_and_lock"
.LC14:
	.string	"OPENSSL_atexit"
.LC15:
	.string	"OPENSSL_buf2hexstr"
.LC16:
	.string	"openssl_fopen"
.LC17:
	.string	"OPENSSL_hexstr2buf"
.LC18:
	.string	"OPENSSL_init_crypto"
.LC19:
	.string	"OPENSSL_LH_new"
.LC20:
	.string	"OPENSSL_sk_deep_copy"
.LC21:
	.string	"OPENSSL_sk_dup"
.LC22:
	.string	"pkey_hmac_init"
.LC23:
	.string	"pkey_poly1305_init"
.LC24:
	.string	"pkey_siphash_init"
.LC25:
	.string	"sk_reserve"
	.section	.data.rel.ro.local
	.align 32
	.type	CRYPTO_str_functs, @object
	.size	CRYPTO_str_functs, 384
CRYPTO_str_functs:
	.quad	252149760
	.quad	.LC3
	.quad	252108800
	.quad	.LC4
	.quad	252112896
	.quad	.LC5
	.quad	252067840
	.quad	.LC6
	.quad	252129280
	.quad	.LC7
	.quad	252116992
	.quad	.LC8
	.quad	252153856
	.quad	.LC9
	.quad	252157952
	.quad	.LC10
	.quad	252076032
	.quad	.LC11
	.quad	252104704
	.quad	.LC12
	.quad	252121088
	.quad	.LC13
	.quad	252125184
	.quad	.LC14
	.quad	252137472
	.quad	.LC15
	.quad	252145664
	.quad	.LC16
	.quad	252141568
	.quad	.LC17
	.quad	252133376
	.quad	.LC18
	.quad	252174336
	.quad	.LC19
	.quad	252178432
	.quad	.LC20
	.quad	252182528
	.quad	.LC21
	.quad	252162048
	.quad	.LC22
	.quad	252166144
	.quad	.LC23
	.quad	252170240
	.quad	.LC24
	.quad	252186624
	.quad	.LC25
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
