	.file	"rc2_cbc.c"
	.text
	.p2align 4
	.globl	RC2_encrypt
	.type	RC2_encrypt, @function
RC2_encrypt:
.LFB1:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rcx
	movq	%rsi, %r10
	movl	$5, %r11d
	movq	8(%rdi), %r8
	movzwl	%cx, %eax
	shrq	$16, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movzwl	%r8w, %r9d
	pushq	%r13
	shrq	$16, %r8
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	(%rsi), %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movl	$3, %ebx
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L9:
	xorl	%r11d, %r11d
	movl	%r8d, %edx
	cmpl	$2, %ebx
	sete	%r11b
	andl	$63, %edx
	addl	(%rsi,%rdx,4), %eax
	addl	$5, %r11d
	movl	%eax, %edx
	andl	$63, %edx
	addl	(%rsi,%rdx,4), %ecx
	movl	%ecx, %edx
	andl	$63, %edx
	addl	(%rsi,%rdx,4), %r9d
	movl	%r9d, %edx
	andl	$63, %edx
	addl	(%rsi,%rdx,4), %r8d
.L3:
	movl	(%r10), %r12d
.L2:
	movl	%r8d, %edx
	movl	%r9d, %r13d
	addq	$16, %r10
	notl	%edx
	andl	%r8d, %r13d
	andl	%ecx, %edx
	addl	-12(%r10), %ecx
	addl	%r13d, %edx
	addl	%r12d, %edx
	addl	%edx, %eax
	movzwl	%ax, %eax
	leal	(%rax,%rax), %edx
	shrl	$15, %eax
	orl	%edx, %eax
	movl	%r8d, %edx
	andl	%eax, %edx
	addl	%edx, %ecx
	movl	%eax, %edx
	notl	%edx
	andl	%r9d, %edx
	addl	-8(%r10), %r9d
	addl	%ecx, %edx
	movzwl	%dx, %edx
	leal	0(,%rdx,4), %ecx
	shrl	$14, %edx
	orl	%edx, %ecx
	movl	%eax, %edx
	andl	%ecx, %edx
	addl	%edx, %r9d
	movl	%ecx, %edx
	notl	%edx
	andl	%r8d, %edx
	addl	-4(%r10), %r8d
	addl	%r9d, %edx
	movzwl	%dx, %edx
	leal	0(,%rdx,8), %r9d
	shrl	$13, %edx
	orl	%edx, %r9d
	movl	%ecx, %edx
	andl	%r9d, %edx
	addl	%edx, %r8d
	movl	%r9d, %edx
	notl	%edx
	andl	%eax, %edx
	addl	%r8d, %edx
	movzwl	%dx, %edx
	movl	%edx, %r8d
	shrl	$11, %edx
	sall	$5, %r8d
	orl	%edx, %r8d
	subl	$1, %r11d
	jne	.L3
	subl	$1, %ebx
	jne	.L9
	salq	$16, %rcx
	movzwl	%ax, %eax
	salq	$16, %r8
	movzwl	%r9w, %r9d
	movl	%ecx, %edx
	popq	%rbx
	popq	%r12
	orq	%rax, %rdx
	movl	%r8d, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	orq	%rax, %r9
	movq	%rdx, %xmm0
	movq	%r9, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rdi)
	ret
	.cfi_endproc
.LFE1:
	.size	RC2_encrypt, .-RC2_encrypt
	.p2align 4
	.globl	RC2_decrypt
	.type	RC2_decrypt, @function
RC2_decrypt:
.LFB2:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	252(%rsi), %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	$3, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	252(%rsi), %edx
	movq	%rsi, %rdi
	movq	(%rbx), %rcx
	movq	8(%rbx), %r10
	movl	$5, %esi
	movzwl	%cx, %r13d
	movzwl	%r10w, %r9d
	shrq	$16, %rcx
	shrq	$16, %r10
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L17:
	xorl	%esi, %esi
	cmpl	$2, %r12d
	sete	%sil
	andl	$63, %r8d
	andl	$63, %edx
	andl	$63, %eax
	subl	(%rdi,%r8,4), %r10d
	subl	(%rdi,%rdx,4), %r9d
	addl	$5, %esi
	movl	%r10d, %r8d
	subl	(%rdi,%rax,4), %ecx
	movzwl	%r10w, %r10d
	movzwl	%r9w, %r9d
	andl	$63, %r8d
	movzwl	%cx, %ecx
	subl	(%rdi,%r8,4), %r13d
	movzwl	%r13w, %r13d
.L12:
	movl	(%r11), %edx
.L11:
	movl	%r10d, %eax
	shrl	$5, %r10d
	movl	%r9d, %r8d
	subq	$16, %r11
	sall	$11, %eax
	sall	$13, %r8d
	orl	%eax, %r10d
	movl	%ecx, %eax
	andl	%r9d, %eax
	movzwl	%r10w, %r10d
	addl	%edx, %eax
	movl	%ecx, %edx
	subl	%eax, %r10d
	movl	%r9d, %eax
	shrl	$3, %r9d
	notl	%eax
	orl	%r9d, %r8d
	sall	$14, %edx
	andl	%r13d, %eax
	movzwl	%r8w, %r8d
	subl	%eax, %r10d
	movl	%r13d, %eax
	andl	%ecx, %eax
	addl	12(%r11), %eax
	movzwl	%r10w, %r10d
	subl	%eax, %r8d
	movl	%ecx, %eax
	shrl	$2, %ecx
	notl	%eax
	orl	%ecx, %edx
	andl	%r10d, %eax
	movzwl	%dx, %edx
	subl	8(%r11), %edx
	subl	%eax, %r8d
	movl	%r13d, %eax
	andl	%r10d, %eax
	movzwl	%r8w, %r9d
	subl	%eax, %edx
	movl	%r13d, %eax
	notl	%eax
	andl	%r9d, %eax
	subl	%eax, %edx
	movl	%r13d, %eax
	shrl	%r13d
	sall	$15, %eax
	movzwl	%dx, %ecx
	orl	%r13d, %eax
	movl	%r10d, %r13d
	andl	%r9d, %r13d
	movzwl	%ax, %eax
	subl	4(%r11), %eax
	subl	%r13d, %eax
	movl	%r10d, %r13d
	notl	%r13d
	andl	%ecx, %r13d
	subl	%r13d, %eax
	movzwl	%ax, %r13d
	subl	$1, %esi
	jne	.L12
	subl	$1, %r12d
	jne	.L17
	salq	$16, %rcx
	salq	$16, %r10
	orq	%r13, %rcx
	orq	%r10, %r9
	movq	%rcx, %xmm0
	movq	%r9, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2:
	.size	RC2_decrypt, .-RC2_decrypt
	.p2align 4
	.globl	RC2_cbc_encrypt
	.type	RC2_cbc_encrypt, @function
RC2_cbc_encrypt:
.LFB0:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	leaq	-8(%rdx), %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movl	4(%r8), %r14d
	movl	(%r8), %r15d
	movq	%rsi, -128(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%r8, -144(%rbp)
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	movq	%rcx, -136(%rbp)
	testl	%r9d, %r9d
	je	.L19
	testq	%rcx, %rcx
	js	.L20
	shrq	$3, %rcx
	leaq	-80(%rbp), %r12
	leaq	8(,%rcx,8), %rax
	movq	%rcx, -96(%rbp)
	movq	%r12, %rdi
	movq	%rsi, %r12
	movq	%rax, -104(%rbp)
	addq	%rbx, %rax
	movq	%rax, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L21:
	movl	4(%rbx), %edx
	movl	(%rbx), %ecx
	movq	%r13, %rsi
	addq	$8, %r12
	addq	$8, %rbx
	xorq	%rdx, %r14
	xorq	%rcx, %r15
	movq	%r14, %xmm1
	movq	%r15, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	RC2_encrypt
	movq	-80(%rbp), %r15
	movq	-72(%rbp), %r14
	movq	%r15, %rdx
	movb	%r15b, -8(%r12)
	shrq	$8, %rdx
	movb	%r14b, -4(%r12)
	movb	%dl, -7(%r12)
	movq	%r15, %rdx
	shrq	$16, %rdx
	movb	%dl, -6(%r12)
	movq	%r15, %rdx
	shrq	$24, %rdx
	movb	%dl, -5(%r12)
	movq	%r14, %rdx
	shrq	$8, %rdx
	movb	%dl, -3(%r12)
	movq	%r14, %rdx
	shrq	$16, %rdx
	movb	%dl, -2(%r12)
	movq	%r14, %rdx
	shrq	$24, %rdx
	movb	%dl, -1(%r12)
	cmpq	%rbx, -88(%rbp)
	jne	.L21
	movq	-96(%rbp), %rax
	movq	-104(%rbp), %rdi
	addq	%rdi, -128(%rbp)
	movq	-120(%rbp), %rdi
	negq	%rax
	salq	$3, %rax
	leaq	-16(%rdi,%rax), %rcx
	addq	-136(%rbp), %rax
	movq	%rax, -120(%rbp)
.L20:
	cmpq	$-8, %rcx
	je	.L63
	movq	-120(%rbp), %rax
	leaq	8(%rbx,%rcx), %rcx
	cmpq	$7, %rax
	ja	.L24
	leaq	.L26(%rip), %r8
	movslq	(%r8,%rax,4), %rdi
	addq	%r8, %rdi
	notrack jmp	*%rdi
	.section	.rodata
	.align 4
	.align 4
.L26:
	.long	.L24-.L26
	.long	.L47-.L26
	.long	.L48-.L26
	.long	.L49-.L26
	.long	.L50-.L26
	.long	.L51-.L26
	.long	.L52-.L26
	.long	.L25-.L26
	.text
	.p2align 4,,10
	.p2align 3
.L19:
	movq	%rcx, %rax
	testq	%rcx, %rcx
	js	.L53
	shrq	$3, %rax
	leaq	-80(%rbp), %r12
	movq	%r14, -88(%rbp)
	movq	%rbx, %r14
	movq	%rax, -152(%rbp)
	leaq	8(,%rax,8), %rax
	movq	%rax, -160(%rbp)
	addq	%rbx, %rax
	movq	%r12, %rbx
	movq	%rsi, %r12
	movq	%rax, -112(%rbp)
	.p2align 4,,10
	.p2align 3
.L35:
	movq	-88(%rbp), %rax
	movq	%r15, -104(%rbp)
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movl	(%r14), %r15d
	addq	$8, %r12
	addq	$8, %r14
	movq	%rax, -96(%rbp)
	movl	-4(%r14), %eax
	movq	%r15, %xmm0
	movq	%rax, -88(%rbp)
	movhps	-88(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	RC2_decrypt
	movq	-104(%rbp), %rdx
	xorq	-80(%rbp), %rdx
	movq	%rdx, %rsi
	movb	%dl, -8(%r12)
	movq	-96(%rbp), %rax
	shrq	$8, %rsi
	xorq	-72(%rbp), %rax
	movb	%sil, -7(%r12)
	movq	%rdx, %rsi
	shrq	$24, %rdx
	movb	%dl, -5(%r12)
	movq	%rax, %rdx
	shrq	$16, %rsi
	shrq	$8, %rdx
	movb	%al, -4(%r12)
	movb	%dl, -3(%r12)
	movq	%rax, %rdx
	shrq	$24, %rax
	movb	%sil, -6(%r12)
	shrq	$16, %rdx
	movb	%dl, -2(%r12)
	movb	%al, -1(%r12)
	cmpq	%r14, -112(%rbp)
	jne	.L35
	movq	-152(%rbp), %rax
	movq	-160(%rbp), %rdi
	movq	%r14, %rbx
	addq	%rdi, -128(%rbp)
	movq	-120(%rbp), %rdi
	negq	%rax
	movq	-88(%rbp), %r14
	salq	$3, %rax
	leaq	-16(%rdi,%rax), %r12
	addq	-136(%rbp), %rax
	movq	%rax, -120(%rbp)
.L34:
	cmpq	$-8, %r12
	je	.L36
	movd	(%rbx), %xmm2
	movd	4(%rbx), %xmm3
	leaq	-80(%rbp), %rdi
	movq	%r13, %rsi
	movdqa	%xmm2, %xmm0
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	RC2_decrypt
	movq	-128(%rbp), %rbx
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rdx
	leaq	8(%rbx,%r12), %rcx
	movq	-120(%rbp), %rbx
	xorq	%r15, %rax
	xorq	%r14, %rdx
	cmpq	$7, %rbx
	ja	.L54
	leaq	.L38(%rip), %rdi
	movslq	(%rdi,%rbx,4), %rsi
	addq	%rdi, %rsi
	notrack jmp	*%rsi
	.section	.rodata
	.align 4
	.align 4
.L38:
	.long	.L54-.L38
	.long	.L44-.L38
	.long	.L55-.L38
	.long	.L42-.L38
	.long	.L56-.L38
	.long	.L40-.L38
	.long	.L39-.L38
	.long	.L37-.L38
	.text
.L37:
	movq	%rdx, %rsi
	subq	$1, %rcx
	shrq	$16, %rsi
	movb	%sil, (%rcx)
.L39:
	movq	%rdx, %rsi
	subq	$1, %rcx
	shrq	$8, %rsi
	movb	%sil, (%rcx)
.L40:
	movb	%dl, -1(%rcx)
	leaq	-1(%rcx), %rsi
.L41:
	movq	%rax, %rdx
	leaq	-1(%rsi), %rcx
	shrq	$24, %rdx
	movb	%dl, -1(%rsi)
.L42:
	movq	%rax, %rsi
	leaq	-1(%rcx), %rdx
	shrq	$16, %rsi
	movb	%sil, -1(%rcx)
.L43:
	movq	%rax, %rsi
	leaq	-1(%rdx), %rcx
	shrq	$8, %rsi
	movb	%sil, -1(%rdx)
.L44:
	movb	%al, -1(%rcx)
	movq	%xmm3, %r14
	movq	%xmm2, %r15
.L36:
	movq	-144(%rbp), %rax
	movl	%r15d, (%rax)
	movl	%r14d, 4(%rax)
.L18:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L64
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	xorl	%r8d, %r8d
.L27:
	movzbl	-1(%rcx), %edi
	subq	$1, %rcx
	salq	$8, %rdi
	orq	%r8, %rdi
.L28:
	leaq	-1(%rcx), %r8
	movzbl	-1(%rcx), %ecx
	orq	%rcx, %rdi
	xorq	%rdi, %r14
.L29:
	leaq	-1(%r8), %rcx
	movzbl	-1(%r8), %r8d
	salq	$24, %r8
.L30:
	movzbl	-1(%rcx), %edi
	leaq	-1(%rcx), %r9
	salq	$16, %rdi
	orq	%rdi, %r8
.L31:
	movzbl	-1(%r9), %edi
	leaq	-1(%r9), %rcx
	salq	$8, %rdi
	orq	%r8, %rdi
.L32:
	movzbl	-1(%rcx), %ecx
	orq	%rcx, %rdi
	xorq	%rdi, %r15
.L24:
	movq	%r14, %xmm4
	movq	%r15, %xmm0
	leaq	-80(%rbp), %rdi
	movq	%r13, %rsi
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	RC2_encrypt
	movl	-80(%rbp), %edx
	movq	-128(%rbp), %r9
	movl	-72(%rbp), %r11d
	movq	-80(%rbp), %rax
	movl	%edx, (%r9)
	movq	-72(%rbp), %rdx
	movl	%r11d, 4(%r9)
	movq	%rax, %rcx
	movl	%eax, %edi
	movzbl	%ah, %esi
	movq	%rdx, %r8
	movl	%edx, %r10d
	movzbl	%dh, %ebx
	shrq	$16, %rcx
	shrq	$24, %rax
	shrq	$16, %r8
	shrq	$24, %rdx
.L23:
	movq	-144(%rbp), %r11
	movb	%dil, (%r11)
	movb	%sil, 1(%r11)
	movb	%cl, 2(%r11)
	movb	%al, 3(%r11)
	movb	%r10b, 4(%r11)
	movb	%bl, 5(%r11)
	movb	%r8b, 6(%r11)
	movb	%dl, 7(%r11)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L63:
	movq	%r15, %rax
	movq	%r15, %rcx
	movq	%r14, %rbx
	movq	%r14, %r8
	movq	%r14, %rdx
	movzbl	%ah, %esi
	movl	%r15d, %edi
	shrq	$16, %rcx
	shrq	$24, %rax
	movl	%r14d, %r10d
	movzbl	%bh, %ebx
	shrq	$16, %r8
	shrq	$24, %rdx
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L53:
	movq	%rcx, %r12
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L47:
	xorl	%edi, %edi
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L48:
	movq	%rcx, %r9
	xorl	%r8d, %r8d
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L49:
	xorl	%r8d, %r8d
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L50:
	movq	%rcx, %r8
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L51:
	xorl	%edi, %edi
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L25:
	movzbl	-1(%rcx), %edi
	subq	$1, %rcx
	salq	$16, %rdi
	movq	%rdi, %r8
	jmp	.L27
.L55:
	movq	%rcx, %rdx
	jmp	.L43
.L56:
	movq	%rcx, %rsi
	jmp	.L41
.L64:
	call	__stack_chk_fail@PLT
.L54:
	movq	%xmm3, %r14
	movq	%xmm2, %r15
	jmp	.L36
	.cfi_endproc
.LFE0:
	.size	RC2_cbc_encrypt, .-RC2_cbc_encrypt
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
