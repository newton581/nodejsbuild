	.file	"ec_mult.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ec/ec_mult.c"
	.text
	.p2align 4
	.type	ec_scalar_mul_ladder.part.0, @function
ec_scalar_mul_ladder.part.0:
.LFB435:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	24(%rdi), %rdi
	movq	%rsi, -56(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L83
	movq	%r13, %rdi
	movl	%eax, %r15d
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	EC_POINT_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L6
	movq	%r12, %rdi
	call	EC_POINT_new@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L6
	cmpq	$0, -64(%rbp)
	je	.L84
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	EC_POINT_copy@PLT
	testl	%eax, %eax
	je	.L85
.L8:
	movq	16(%r14), %rdi
	movl	$4, %esi
	call	BN_set_flags@PLT
	movq	24(%r14), %rdi
	movl	$4, %esi
	call	BN_set_flags@PLT
	movq	32(%r14), %rdi
	movl	$4, %esi
	call	BN_set_flags@PLT
	movq	-56(%rbp), %rax
	movl	$4, %esi
	movq	16(%rax), %rdi
	call	BN_set_flags@PLT
	movq	-56(%rbp), %rax
	movl	$4, %esi
	movq	24(%rax), %rdi
	call	BN_set_flags@PLT
	movq	-56(%rbp), %rax
	movl	$4, %esi
	movq	32(%rax), %rdi
	call	BN_set_flags@PLT
	movq	16(%rbx), %rdi
	movl	$4, %esi
	call	BN_set_flags@PLT
	movq	24(%rbx), %rdi
	movl	$4, %esi
	call	BN_set_flags@PLT
	movq	32(%rbx), %rdi
	movl	$4, %esi
	call	BN_set_flags@PLT
	movq	%r13, %rdi
	call	BN_CTX_get@PLT
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	call	BN_CTX_get@PLT
	movq	%r13, %rdi
	movq	%rax, -88(%rbp)
	call	BN_CTX_get@PLT
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L86
	movq	24(%r12), %rdx
	movq	16(%r12), %rsi
	movq	%r13, %rcx
	movq	-64(%rbp), %rdi
	call	BN_mul@PLT
	movl	$197, %r8d
	testl	%eax, %eax
	je	.L80
	movq	-64(%rbp), %rdi
	call	BN_num_bits@PLT
	movq	-64(%rbp), %rdi
	movl	%eax, -104(%rbp)
	call	bn_get_top@PLT
	movq	-80(%rbp), %rdi
	addl	$2, %eax
	movl	%eax, %esi
	movl	%eax, -92(%rbp)
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L12
	movl	-92(%rbp), %esi
	movq	-88(%rbp), %rdi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L12
	movq	-72(%rbp), %rsi
	movq	-80(%rbp), %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L87
	movq	-80(%rbp), %rdi
	movl	$4, %esi
	call	BN_set_flags@PLT
	movq	-80(%rbp), %rdi
	call	BN_num_bits@PLT
	cmpl	%eax, -104(%rbp)
	jge	.L14
.L17:
	movq	-80(%rbp), %rsi
	movq	-64(%rbp), %rdx
	movq	%r13, %rcx
	movq	%rsi, %rdi
	call	BN_nnmod@PLT
	testl	%eax, %eax
	je	.L88
.L16:
	movq	-64(%rbp), %rdx
	movq	-80(%rbp), %rsi
	movq	-88(%rbp), %rdi
	call	BN_add@PLT
	movl	$234, %r8d
	testl	%eax, %eax
	je	.L80
	movq	-88(%rbp), %rdi
	movl	$4, %esi
	call	BN_set_flags@PLT
	movq	-64(%rbp), %rdx
	movq	-88(%rbp), %rsi
	movq	-80(%rbp), %rdi
	call	BN_add@PLT
	movl	$239, %r8d
	testl	%eax, %eax
	je	.L80
	movl	-104(%rbp), %esi
	movq	-88(%rbp), %rdi
	call	BN_is_bit_set@PLT
	movq	-80(%rbp), %rsi
	movl	-92(%rbp), %ecx
	movq	-88(%rbp), %rdx
	movslq	%eax, %rdi
	call	BN_consttime_swap@PLT
	movq	64(%r12), %rdi
	call	bn_get_top@PLT
	movq	16(%rbx), %rdi
	movl	%eax, %esi
	movl	%eax, -64(%rbp)
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L21
	movq	24(%rbx), %rdi
	movl	-64(%rbp), %esi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L21
	movq	32(%rbx), %rdi
	movl	-64(%rbp), %esi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L21
	movq	-56(%rbp), %rax
	movl	-64(%rbp), %esi
	movq	16(%rax), %rdi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L21
	movq	-56(%rbp), %rax
	movl	-64(%rbp), %esi
	movq	24(%rax), %rdi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L21
	movq	-56(%rbp), %rax
	movl	-64(%rbp), %esi
	movq	32(%rax), %rdi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L21
	movq	16(%r14), %rdi
	movl	-64(%rbp), %esi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L21
	movq	24(%r14), %rdi
	movl	-64(%rbp), %esi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L21
	movq	32(%r14), %rdi
	movl	-64(%rbp), %esi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L21
	movl	40(%r14), %eax
	testl	%eax, %eax
	jne	.L22
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	EC_POINT_make_affine@PLT
	testl	%eax, %eax
	je	.L89
.L22:
	movq	(%r12), %rax
	movq	408(%rax), %rax
	testq	%rax, %rax
	je	.L23
	movq	-56(%rbp), %rsi
	movq	%r13, %r8
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L25
.L24:
	movl	-104(%rbp), %eax
	movl	$1, -88(%rbp)
	subl	$1, %eax
	movl	%eax, -72(%rbp)
	js	.L26
	movl	%r15d, -96(%rbp)
	movq	-56(%rbp), %r15
	movq	%r13, -104(%rbp)
	movq	%rbx, %r13
	movq	%r12, %rbx
	movl	-64(%rbp), %r12d
	movq	%r14, -112(%rbp)
	jmp	.L30
.L91:
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L29
.L28:
	subl	$1, -72(%rbp)
	movl	-72(%rbp), %eax
	cmpl	$-1, %eax
	je	.L90
.L30:
	movl	-72(%rbp), %esi
	movq	-80(%rbp), %rdi
	movl	-88(%rbp), %r14d
	call	BN_is_bit_set@PLT
	movq	16(%r13), %rdx
	movq	16(%r15), %rsi
	movl	%r12d, %ecx
	xorl	%eax, %r14d
	movl	%eax, -88(%rbp)
	movl	%r14d, %r8d
	movslq	%r14d, %r14
	movq	%r14, %rdi
	movl	%r8d, -92(%rbp)
	call	BN_consttime_swap@PLT
	movq	24(%r13), %rdx
	movq	24(%r15), %rsi
	movl	%r12d, %ecx
	movq	%r14, %rdi
	call	BN_consttime_swap@PLT
	movq	32(%r13), %rdx
	movq	32(%r15), %rsi
	movl	%r12d, %ecx
	movq	%r14, %rdi
	call	BN_consttime_swap@PLT
	movl	40(%r15), %edx
	movl	40(%r13), %eax
	movl	-92(%rbp), %r8d
	xorl	%edx, %eax
	andl	%r8d, %eax
	xorl	%eax, %edx
	movl	%edx, 40(%r15)
	xorl	%eax, 40(%r13)
	movq	(%rbx), %rax
	movq	416(%rax), %rax
	testq	%rax, %rax
	jne	.L91
	movq	-104(%rbp), %r14
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%r14, %r8
	call	EC_POINT_add@PLT
	testl	%eax, %eax
	je	.L29
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	EC_POINT_dbl@PLT
	testl	%eax, %eax
	jne	.L28
.L29:
	movl	$351, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$162, %edx
	movq	%r13, %rbx
	movl	$284, %esi
	movl	$16, %edi
	movq	-112(%rbp), %r14
	movl	-96(%rbp), %r15d
	movq	-104(%rbp), %r13
	call	ERR_put_error@PLT
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$168, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%ebx, %ebx
	movl	$284, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
.L5:
	movq	%r14, %rdi
	call	EC_POINT_free@PLT
	movq	%rbx, %rdi
	call	EC_POINT_clear_free@PLT
	movq	%r13, %rdi
	call	BN_CTX_end@PLT
.L1:
	addq	$72, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	movl	$160, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$164, %edx
	xorl	%r15d, %r15d
	movl	$284, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L88:
	movl	$228, %r8d
	.p2align 4,,10
	.p2align 3
.L80:
	leaq	.LC0(%rip), %rcx
	movl	$3, %edx
	movl	$284, %esi
	xorl	%r15d, %r15d
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L84:
	movq	8(%r12), %rsi
	movq	%r14, %rdi
	call	EC_POINT_copy@PLT
	movl	$174, %r8d
	testl	%eax, %eax
	jne	.L8
.L79:
	movl	$16, %edx
	movl	$284, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$211, %r8d
.L81:
	movl	$3, %edx
	movl	$284, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L86:
	movl	$192, %r8d
	movl	$65, %edx
	movl	$284, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L85:
	movl	$179, %r8d
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L14:
	movq	-80(%rbp), %rdi
	call	BN_is_negative@PLT
	testl	%eax, %eax
	jne	.L17
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L21:
	movl	$259, %r8d
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L87:
	movl	$216, %r8d
	jmp	.L81
.L90:
	movq	%rbx, %r12
	movq	-112(%rbp), %r14
	movq	%r13, %rbx
	movq	-104(%rbp), %r13
.L26:
	movq	-56(%rbp), %rcx
	movslq	-88(%rbp), %r15
	movq	16(%rbx), %rdx
	movq	16(%rcx), %rsi
	movl	-64(%rbp), %ecx
	movq	%r15, %rdi
	call	BN_consttime_swap@PLT
	movq	-56(%rbp), %rcx
	movq	24(%rbx), %rdx
	movq	%r15, %rdi
	movq	24(%rcx), %rsi
	movl	-64(%rbp), %ecx
	call	BN_consttime_swap@PLT
	movq	-56(%rbp), %rcx
	movq	32(%rbx), %rdx
	movq	%r15, %rdi
	movq	32(%rcx), %rsi
	movl	-64(%rbp), %ecx
	call	BN_consttime_swap@PLT
	movq	-56(%rbp), %rsi
	movl	40(%rbx), %eax
	movl	-88(%rbp), %r15d
	movl	40(%rsi), %edx
	xorl	%edx, %eax
	andl	%eax, %r15d
	movq	(%r12), %rax
	xorl	%r15d, %edx
	movq	424(%rax), %rax
	movl	%edx, 40(%rsi)
	xorl	%r15d, 40(%rbx)
	movl	$1, %r15d
	testq	%rax, %rax
	je	.L5
	movq	%r13, %r8
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	*%rax
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L92
	movl	$1, %r15d
	jmp	.L5
.L23:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	EC_POINT_copy@PLT
	testl	%eax, %eax
	je	.L25
	movq	-56(%rbp), %rsi
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	EC_POINT_dbl@PLT
	testl	%eax, %eax
	jne	.L24
.L25:
	movl	$271, %r8d
	movl	$153, %edx
	movl	$284, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L5
.L89:
	movl	$265, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$16, %edx
	xorl	%r15d, %r15d
	movl	$284, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L5
.L92:
	movl	$366, %r8d
	movl	$136, %edx
	movl	$284, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L5
	.cfi_endproc
.LFE435:
	.size	ec_scalar_mul_ladder.part.0, .-ec_scalar_mul_ladder.part.0
	.p2align 4
	.globl	EC_ec_pre_comp_dup
	.type	EC_ec_pre_comp_dup, @function
EC_ec_pre_comp_dup:
.LFB426:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	testq	%rdi, %rdi
	je	.L94
	lock addl	$1, 48(%rdi)
.L94:
	ret
	.cfi_endproc
.LFE426:
	.size	EC_ec_pre_comp_dup, .-EC_ec_pre_comp_dup
	.p2align 4
	.globl	EC_ec_pre_comp_free
	.type	EC_ec_pre_comp_free, @function
EC_ec_pre_comp_free:
.LFB427:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L115
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	lock xaddl	%eax, 48(%rdi)
	subl	$1, %eax
	testl	%eax, %eax
	je	.L118
	jle	.L102
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
.L102:
	movq	32(%r13), %r12
	testq	%r12, %r12
	je	.L104
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L105
	.p2align 4,,10
	.p2align 3
.L106:
	call	EC_POINT_free@PLT
	movq	8(%r12), %rdi
	addq	$8, %r12
	testq	%rdi, %rdi
	jne	.L106
	movq	32(%r13), %r12
.L105:
	movl	$98, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
.L104:
	movq	56(%r13), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	popq	%r12
	.cfi_restore 12
	movq	%r13, %rdi
	movl	$101, %edx
	popq	%r13
	.cfi_restore 13
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L115:
	ret
	.cfi_endproc
.LFE427:
	.size	EC_ec_pre_comp_free, .-EC_ec_pre_comp_free
	.p2align 4
	.globl	ec_scalar_mul_ladder
	.type	ec_scalar_mul_ladder, @function
ec_scalar_mul_ladder:
.LFB428:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$8, %rsp
	testq	%rcx, %rcx
	je	.L120
	movq	%rcx, %rsi
	call	EC_POINT_is_at_infinity@PLT
	testl	%eax, %eax
	jne	.L132
.L120:
	movq	16(%r13), %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L130
	addq	$8, %rsp
	movq	%rbx, %r8
	movq	%r12, %rcx
	movq	%r15, %rdx
	popq	%rbx
	movq	%r14, %rsi
	popq	%r12
	movq	%r13, %rdi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ec_scalar_mul_ladder.part.0
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	movl	$156, %r8d
	movl	$114, %edx
	movl	$284, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r14, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	EC_POINT_set_to_infinity@PLT
	.cfi_endproc
.LFE428:
	.size	ec_scalar_mul_ladder, .-ec_scalar_mul_ladder
	.p2align 4
	.globl	ec_wNAF_mul
	.type	ec_wNAF_mul, @function
ec_wNAF_mul:
.LFB429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	%rdx, -80(%rbp)
	movq	%rsi, -184(%rbp)
	movq	16(%rdi), %rdi
	movq	%rcx, -72(%rbp)
	movq	%r8, -192(%rbp)
	movq	%r9, -104(%rbp)
	movq	%rax, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L335
.L134:
	cmpq	$0, -80(%rbp)
	je	.L138
.L315:
	movq	%r15, %rdi
	call	EC_GROUP_get0_generator@PLT
	movq	%rax, -176(%rbp)
	testq	%rax, %rax
	je	.L336
	movq	160(%r15), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L337
	movq	16(%rax), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	jne	.L146
	movq	-72(%rbp), %rax
	movq	$0, -120(%rbp)
	movq	$0, -200(%rbp)
	movl	$1, -160(%rbp)
	addq	$1, %rax
	movq	%rax, -96(%rbp)
	movq	%rax, -128(%rbp)
	movq	$1, -168(%rbp)
.L145:
	leaq	0(,%rax,8), %r13
	movl	$505, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_malloc@PLT
	movl	$506, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, -88(%rbp)
	movq	%rax, %r14
	call	CRYPTO_malloc@PLT
	leaq	8(%r13), %rdi
	movl	$508, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rax, -136(%rbp)
	movq	%rax, %rbx
	call	CRYPTO_malloc@PLT
	movl	$509, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, %r12
	call	CRYPTO_malloc@PLT
	movq	%rax, -112(%rbp)
	testq	%r12, %r12
	je	.L149
	movq	$0, (%r12)
	testq	%r14, %r14
	je	.L150
	testq	%rbx, %rbx
	je	.L150
	testq	%rax, %rax
	je	.L150
	cmpq	$0, -96(%rbp)
	je	.L218
	xorl	%eax, %eax
	movq	%r15, -208(%rbp)
	movq	%rbx, %r13
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	movq	%rax, %r15
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L340:
	movq	-104(%rbp), %rax
	movq	(%rax,%r14,8), %rdi
	call	BN_num_bits@PLT
	cltq
	cmpq	$1999, %rax
	ja	.L338
	cmpq	$799, %rax
	jbe	.L213
	movq	-88(%rbp), %rax
	addq	$16, %r15
	movl	$5, %esi
	movq	$5, (%rax,%r14,8)
	movq	$0, 8(%r12,%r14,8)
.L212:
	movq	-104(%rbp), %rax
	movq	(%rax,%r14,8), %rdi
.L159:
	movq	%r13, %rdx
	call	bn_compute_wNAF@PLT
	movq	%rax, (%r12,%r14,8)
	testq	%rax, %rax
	je	.L160
	movq	0(%r13), %rax
	cmpq	%rax, %rbx
	cmovb	%rax, %rbx
	addq	$1, %r14
	addq	$8, %r13
	cmpq	-96(%rbp), %r14
	je	.L339
.L161:
	cmpq	%r14, -72(%rbp)
	ja	.L340
	movq	-80(%rbp), %rdi
	call	BN_num_bits@PLT
	cltq
	cmpq	$1999, %rax
	ja	.L341
	cmpq	$799, %rax
	jbe	.L213
	movq	-88(%rbp), %rax
	addq	$16, %r15
	movl	$5, %esi
	movq	$5, (%rax,%r14,8)
	movq	$0, 8(%r12,%r14,8)
.L215:
	movq	-80(%rbp), %rdi
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L213:
	cmpq	$299, %rax
	ja	.L219
	cmpq	$69, %rax
	ja	.L220
	cmpq	$20, %rax
	sbbl	%esi, %esi
	addl	$2, %esi
	cmpq	$20, %rax
	sbbq	%rdi, %rdi
	addq	$2, %rdi
	movq	%rdi, %rax
.L157:
	movq	-88(%rbp), %rcx
	addq	%rdi, %r15
	movq	%rax, (%rcx,%r14,8)
	movq	$0, 8(%r12,%r14,8)
	cmpq	%r14, -72(%rbp)
	jbe	.L215
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L338:
	movq	-88(%rbp), %rax
	addq	$32, %r15
	movl	$6, %esi
	movq	$6, (%rax,%r14,8)
	movq	$0, 8(%r12,%r14,8)
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L341:
	movq	-88(%rbp), %rax
	movq	-80(%rbp), %rdi
	addq	$32, %r15
	movl	$6, %esi
	movq	$6, (%rax,%r14,8)
	movq	$0, 8(%r12,%r14,8)
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L219:
	movl	$4, %esi
	movl	$8, %edi
	movl	$4, %eax
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L335:
	movq	24(%r15), %rdi
	call	BN_is_zero@PLT
	movl	%eax, -96(%rbp)
	testl	%eax, %eax
	jne	.L134
	movq	16(%r15), %rdi
	movq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L135
	testq	%rax, %rax
	je	.L314
	cmpq	$0, -72(%rbp)
	je	.L342
.L135:
	cmpq	$0, -80(%rbp)
	jne	.L315
.L314:
	cmpq	$1, -72(%rbp)
	jne	.L134
	movq	-104(%rbp), %rax
	movq	(%rax), %r12
	cmpq	%r12, %rdi
	jne	.L343
	.p2align 4,,10
	.p2align 3
.L138:
	movq	-72(%rbp), %rax
	movl	$0, -160(%rbp)
	movq	$0, -120(%rbp)
	movq	%rax, -96(%rbp)
	movq	%rax, -128(%rbp)
	movq	$0, -200(%rbp)
	movq	$0, -168(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -176(%rbp)
	jmp	.L145
.L346:
	cmpl	$1, -160(%rbp)
	movl	$546, %r8d
	je	.L162
.L333:
	movl	$68, %edx
	movl	$187, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L160:
	xorl	%edi, %edi
	call	EC_POINT_free@PLT
	movq	-88(%rbp), %rdi
	movl	$781, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-136(%rbp), %rdi
	movl	$782, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L344
	movl	$0, -96(%rbp)
	movq	$0, -80(%rbp)
.L207:
	movq	%r12, %r14
	leaq	.LC0(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L203:
	movl	$787, %edx
	movq	%rbx, %rsi
	addq	$8, %r14
	call	CRYPTO_free@PLT
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	jne	.L203
	movl	$789, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	cmpq	$0, -80(%rbp)
	je	.L204
.L210:
	movq	-80(%rbp), %rbx
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L206
	.p2align 4,,10
	.p2align 3
.L205:
	call	EC_POINT_clear_free@PLT
	movq	8(%rbx), %rdi
	addq	$8, %rbx
	testq	%rdi, %rdi
	jne	.L205
.L206:
	movq	-80(%rbp), %rdi
	movl	$795, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
.L204:
	movq	-112(%rbp), %rdi
	movl	$797, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
.L133:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L345
	movl	-96(%rbp), %eax
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L149:
	.cfi_restore_state
	movl	$516, %r8d
	movl	$65, %edx
	movl	$187, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%edi, %edi
	call	EC_POINT_free@PLT
	movq	-88(%rbp), %rdi
	movl	$781, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-136(%rbp), %rdi
	movl	$782, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$0, -96(%rbp)
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L220:
	movl	$3, %esi
	movl	$4, %edi
	movl	$3, %eax
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L339:
	movq	%r15, -104(%rbp)
	movq	-208(%rbp), %r15
.L151:
	cmpq	$0, -168(%rbp)
	je	.L162
	cmpq	$0, -120(%rbp)
	je	.L346
	movl	-160(%rbp), %eax
	movq	$0, -64(%rbp)
	testl	%eax, %eax
	jne	.L347
	movq	-120(%rbp), %rax
	movq	-72(%rbp), %r13
	leaq	-64(%rbp), %rdx
	movq	-88(%rbp), %rcx
	movq	-80(%rbp), %rdi
	movq	24(%rax), %rsi
	leaq	0(,%r13,8), %r14
	movq	%rsi, (%rcx,%r13,8)
	call	bn_compute_wNAF@PLT
	movq	%rax, -208(%rbp)
	testq	%rax, %rax
	je	.L160
	movq	-64(%rbp), %rdx
	cmpq	%rbx, %rdx
	jbe	.L348
	movq	-168(%rbp), %rax
	movq	-144(%rbp), %rcx
	imulq	%rcx, %rax
	cmpq	%rax, %rdx
	jnb	.L168
	leaq	-1(%rcx,%rdx), %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	-72(%rbp), %rcx
	addq	%rax, %rcx
	movq	%rcx, -128(%rbp)
	movq	-120(%rbp), %rcx
	cmpq	%rax, 16(%rcx)
	jb	.L349
.L168:
	movq	-120(%rbp), %rax
	movq	-72(%rbp), %r14
	movq	32(%rax), %r13
	movq	-128(%rbp), %rax
	cmpq	%r14, %rax
	jbe	.L170
	subq	$1, %rax
	movq	-200(%rbp), %rcx
	movq	%r12, -80(%rbp)
	movq	%rax, -120(%rbp)
	movq	-208(%rbp), %rax
	salq	$3, %rcx
	movq	%r15, -168(%rbp)
	movq	-136(%rbp), %r12
	movq	%rcx, -160(%rbp)
	movq	%rax, %r15
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L354:
	movq	-144(%rbp), %rcx
	movq	%rcx, (%r12,%r14,8)
	movq	-64(%rbp), %rax
	cmpq	%rcx, %rax
	jb	.L350
	subq	-144(%rbp), %rax
	movq	%rax, -64(%rbp)
	movq	(%r12,%r14,8), %rdi
.L173:
	movq	-80(%rbp), %rax
	addq	$1, %r14
	movl	$626, %edx
	leaq	.LC0(%rip), %rsi
	movq	$0, (%rax,%r14,8)
	call	CRYPTO_malloc@PLT
	movq	%rax, %rdi
	movq	-80(%rbp), %rax
	movq	%rdi, -8(%rax,%r14,8)
	testq	%rdi, %rdi
	je	.L351
	movq	-8(%r12,%r14,8), %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-8(%r12,%r14,8), %rax
	cmpq	%rax, %rbx
	cmovb	%rax, %rbx
	cmpq	$0, 0(%r13)
	je	.L352
	movq	-112(%rbp), %rax
	addq	-144(%rbp), %r15
	movq	%r13, -8(%rax,%r14,8)
	addq	-160(%rbp), %r13
	cmpq	-128(%rbp), %r14
	jnb	.L353
.L176:
	cmpq	-120(%rbp), %r14
	jb	.L354
	movq	-64(%rbp), %rdi
	movq	%rdi, (%r12,%r14,8)
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L150:
	movl	$516, %r8d
	movl	$65, %edx
	movl	$187, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%edi, %edi
	call	EC_POINT_free@PLT
	movq	-88(%rbp), %rdi
	movl	$781, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-136(%rbp), %rdi
	movl	$782, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	(%r12), %rdi
	movl	$0, -96(%rbp)
	movq	$0, -80(%rbp)
	testq	%rdi, %rdi
	jne	.L207
	movl	$789, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L337:
	movl	$1, -160(%rbp)
	movq	-72(%rbp), %rax
	movq	$0, -200(%rbp)
	movq	$1, -168(%rbp)
	addq	$1, %rax
	movq	%rax, -96(%rbp)
	movq	%rax, -128(%rbp)
	movq	$0, -144(%rbp)
	jmp	.L145
.L353:
	movq	-80(%rbp), %r12
	movq	-168(%rbp), %r15
.L170:
	movq	-208(%rbp), %rdi
	movl	$645, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L162:
	movq	-104(%rbp), %rax
	movl	$655, %edx
	leaq	.LC0(%rip), %rsi
	leaq	8(,%rax,8), %r14
	movq	%r14, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L355
	movq	%rax, %rcx
	cmpq	$0, -96(%rbp)
	leaq	-8(%rax,%r14), %rax
	movq	%rax, -144(%rbp)
	movq	$0, (%rax)
	je	.L222
	movq	%r12, -120(%rbp)
	xorl	%eax, %eax
	movq	-88(%rbp), %r13
	movq	%rcx, %r14
	movq	%rbx, -160(%rbp)
	movq	%rax, %rbx
.L182:
	movq	-112(%rbp), %rax
	movq	%r14, (%rax,%rbx,8)
	movl	0(%r13), %eax
	leal	-1(%rax), %ecx
	movl	$1, %eax
	salq	%cl, %rax
	testq	%rax, %rax
	je	.L179
	xorl	%r12d, %r12d
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L356:
	movl	0(%r13), %eax
	addq	$1, %r12
	addq	$8, %r14
	leal	-1(%rax), %ecx
	movl	$1, %eax
	salq	%cl, %rax
	cmpq	%r12, %rax
	jbe	.L179
.L181:
	movq	%r15, %rdi
	call	EC_POINT_new@PLT
	movq	%rax, (%r14)
	testq	%rax, %rax
	jne	.L356
	movl	$0, -96(%rbp)
	movq	-120(%rbp), %r12
	movq	%rax, %r9
.L180:
	movq	%r9, %rdi
	call	EC_POINT_free@PLT
	movq	-88(%rbp), %rdi
	movl	$781, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-136(%rbp), %rdi
	movl	$782, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L207
	movl	$789, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L146:
	movq	-120(%rbp), %rax
	movq	-152(%rbp), %rcx
	movq	%r15, %rdi
	movq	-176(%rbp), %rsi
	movq	32(%rax), %rax
	movq	(%rax), %rdx
	call	EC_POINT_cmp@PLT
	movl	%eax, -160(%rbp)
	testl	%eax, %eax
	je	.L147
	movq	-72(%rbp), %rax
	movl	$1, -160(%rbp)
	movq	$0, -120(%rbp)
	movq	$0, -200(%rbp)
	addq	$1, %rax
	movq	%rax, -96(%rbp)
	movq	%rax, -128(%rbp)
	movq	$1, -168(%rbp)
	movq	$0, -144(%rbp)
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L179:
	addq	$1, %rbx
	addq	$8, %r13
	cmpq	%rbx, -96(%rbp)
	jne	.L182
	movq	-160(%rbp), %rbx
	movq	-120(%rbp), %r12
.L178:
	cmpq	%r14, -144(%rbp)
	je	.L183
	movl	$674, %r8d
	movl	$68, %edx
	movl	$187, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$0, -96(%rbp)
	xorl	%r9d, %r9d
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L336:
	movl	$463, %r8d
	movl	$113, %edx
	movl	$187, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L143:
	xorl	%edi, %edi
	call	EC_POINT_free@PLT
	movl	$781, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	CRYPTO_free@PLT
	movl	$782, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	CRYPTO_free@PLT
	movl	$0, -96(%rbp)
	movq	$0, -112(%rbp)
	jmp	.L204
.L348:
	leaq	1(%r13), %rcx
	movq	%rax, (%r12,%r13,8)
	movq	-120(%rbp), %rax
	movq	%rcx, -128(%rbp)
	movq	-136(%rbp), %rcx
	movq	$0, 8(%r12,%r14)
	movq	32(%rax), %rax
	movq	%rdx, (%rcx,%r13,8)
	movq	-112(%rbp), %rcx
	movq	%rax, (%rcx,%r13,8)
	jmp	.L162
.L343:
	movq	-192(%rbp), %rax
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L139
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	EC_POINT_is_at_infinity@PLT
	testl	%eax, %eax
	jne	.L140
	movq	16(%r15), %rdi
.L139:
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L141
	movq	-152(%rbp), %r8
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%r15, %rdi
	movq	-184(%rbp), %rsi
	call	ec_scalar_mul_ladder.part.0
	movl	%eax, -96(%rbp)
	jmp	.L133
.L342:
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L141
	movq	-152(%rbp), %r8
	movq	-80(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movq	-184(%rbp), %rsi
	call	ec_scalar_mul_ladder.part.0
	movl	%eax, -96(%rbp)
	jmp	.L133
.L147:
	movq	-120(%rbp), %rbx
	movq	-80(%rbp), %rdi
	movq	8(%rbx), %r14
	movq	%r14, -144(%rbp)
	call	BN_num_bits@PLT
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rsi
	movq	%rcx, -88(%rbp)
	movl	-88(%rbp), %ecx
	movq	%rsi, %rdx
	subl	$1, %ecx
	salq	%cl, %rdx
	cmpq	%rdx, 40(%rbx)
	jne	.L148
	xorl	%edx, %edx
	cltq
	divq	%r14
	addq	$1, %rax
	cmpq	%rax, %rsi
	cmovbe	%rsi, %rax
	movq	%rax, %rdx
	movq	%rax, -168(%rbp)
	movl	$1, %eax
	salq	%cl, %rax
	movq	%rax, -200(%rbp)
	movq	-72(%rbp), %rax
	addq	%rax, %rdx
	movq	%rax, -96(%rbp)
	movq	%rdx, -128(%rbp)
	movq	%rdx, %rax
	jmp	.L145
.L347:
	movl	$555, %r8d
	jmp	.L333
.L218:
	movq	$0, -104(%rbp)
	xorl	%ebx, %ebx
	jmp	.L151
.L141:
	movl	$156, %r8d
	movl	$114, %edx
	movl	$284, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L133
.L148:
	movl	$491, %r8d
	movl	$68, %edx
	movl	$187, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L143
.L355:
	movl	$657, %r8d
	movl	$65, %edx
	movl	$187, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L160
.L351:
	movl	$628, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movq	%rax, %r12
	movl	$187, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	movq	-208(%rbp), %rdi
	movl	$629, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L160
.L222:
	movq	%rcx, %r14
	jmp	.L178
.L352:
	movl	$637, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$68, %edx
	movl	$187, %esi
	movl	$16, %edi
	movq	-80(%rbp), %r12
	call	ERR_put_error@PLT
	movq	-208(%rbp), %rdi
	movl	$638, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L160
.L183:
	movq	%r15, %rdi
	call	EC_POINT_new@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L224
	xorl	%r14d, %r14d
	cmpq	$0, -96(%rbp)
	je	.L190
	movq	%rbx, -160(%rbp)
	movq	%rax, %r13
	movq	-112(%rbp), %rbx
	movq	%r12, -144(%rbp)
	movq	-88(%rbp), %r12
	jmp	.L184
.L359:
	movq	-192(%rbp), %rax
	movq	(%rax,%r14,8), %rsi
	call	EC_POINT_copy@PLT
	testl	%eax, %eax
	je	.L327
.L189:
	cmpq	$1, (%r12,%r14,8)
	ja	.L357
.L188:
	addq	$1, %r14
	cmpq	%r14, -96(%rbp)
	je	.L358
.L184:
	movq	(%rbx,%r14,8), %rax
	movq	(%rax), %rdi
	cmpq	%r14, -72(%rbp)
	ja	.L359
	movq	-176(%rbp), %rsi
	call	EC_POINT_copy@PLT
	testl	%eax, %eax
	jne	.L189
.L327:
	movl	%eax, -96(%rbp)
	movq	-144(%rbp), %r12
	movq	%r13, %r9
	jmp	.L180
.L357:
	movq	(%rbx,%r14,8), %rax
	movq	-152(%rbp), %rcx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	(%rax), %rdx
	call	EC_POINT_dbl@PLT
	testl	%eax, %eax
	je	.L327
	movl	(%r12,%r14,8), %eax
	leal	-1(%rax), %ecx
	movl	$1, %eax
	salq	%cl, %rax
	cmpq	$1, %rax
	jbe	.L188
	movl	$1, %r10d
	movq	%r13, -120(%rbp)
	movq	%rbx, %r13
	movq	%r10, %rbx
	jmp	.L191
.L361:
	movl	(%r12,%r14,8), %eax
	addq	$1, %rbx
	leal	-1(%rax), %ecx
	movl	$1, %eax
	salq	%cl, %rax
	cmpq	%rbx, %rax
	jbe	.L360
.L191:
	movq	0(%r13,%r14,8), %rax
	movq	-152(%rbp), %r8
	movq	%r15, %rdi
	movq	-120(%rbp), %rcx
	movq	-8(%rax,%rbx,8), %rdx
	movq	(%rax,%rbx,8), %rsi
	call	EC_POINT_add@PLT
	testl	%eax, %eax
	jne	.L361
	movq	-120(%rbp), %r9
	movq	-144(%rbp), %r12
	movl	%eax, -96(%rbp)
	jmp	.L180
.L350:
	movl	$613, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$68, %edx
	movl	$187, %esi
	movl	$16, %edi
	movq	-80(%rbp), %r12
	call	ERR_put_error@PLT
	movq	-208(%rbp), %rdi
	movl	$614, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L160
.L140:
	movq	-184(%rbp), %rsi
	movq	%r15, %rdi
	call	EC_POINT_set_to_infinity@PLT
	movl	%eax, -96(%rbp)
	jmp	.L133
.L360:
	movq	%r13, %rbx
	movq	-120(%rbp), %r13
	jmp	.L188
.L349:
	movl	$598, %r8d
	movl	$68, %edx
	movl	$187, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-208(%rbp), %rdi
	movl	$599, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L160
.L224:
	movl	$0, -96(%rbp)
	jmp	.L180
.L345:
	call	__stack_chk_fail@PLT
.L358:
	movq	-160(%rbp), %rbx
	movq	-144(%rbp), %r12
	movq	%r13, %r9
.L190:
	movq	-80(%rbp), %rdx
	movq	-104(%rbp), %rsi
	movq	%r15, %rdi
	movq	%r9, -72(%rbp)
	movq	-152(%rbp), %rcx
	call	EC_POINTs_make_affine@PLT
	movq	-72(%rbp), %r9
	testl	%eax, %eax
	movl	%eax, -96(%rbp)
	je	.L180
	subl	$1, %ebx
	js	.L192
	movslq	%ebx, %rbx
	movl	$1, %r10d
	movl	$0, -104(%rbp)
	movq	%rbx, %r14
	movq	%r12, %rbx
	movq	-184(%rbp), %r12
	movq	%r9, -72(%rbp)
	movl	%r10d, %r9d
.L193:
	xorl	%r13d, %r13d
	cmpq	$0, -128(%rbp)
	jne	.L200
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L363:
	movl	%ecx, %eax
	movq	-112(%rbp), %rcx
	movq	%r12, %rdi
	sarl	%eax
	movq	(%rcx,%r13,8), %rdx
	cltq
	movq	(%rdx,%rax,8), %rsi
	call	EC_POINT_copy@PLT
	testl	%eax, %eax
	je	.L329
	movq	-152(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	ec_point_blind_coordinates@PLT
	testl	%eax, %eax
	je	.L362
	xorl	%r9d, %r9d
.L195:
	addq	$1, %r13
	cmpq	%r13, -128(%rbp)
	je	.L201
.L200:
	movq	-136(%rbp), %rax
	cmpq	%r14, (%rax,%r13,8)
	jbe	.L195
	movq	(%rbx,%r13,8), %rax
	movsbl	(%rax,%r14), %edx
	movl	%edx, %eax
	testl	%edx, %edx
	je	.L195
	movl	%edx, %esi
	shrl	$31, %edx
	sarb	$7, %sil
	xorl	%esi, %eax
	movl	%eax, %ecx
	subl	%esi, %ecx
	movzbl	%cl, %ecx
	cmpl	-104(%rbp), %edx
	je	.L196
	testl	%r9d, %r9d
	je	.L197
.L198:
	xorl	$1, -104(%rbp)
.L196:
	testl	%r9d, %r9d
	jne	.L363
	movl	%ecx, %eax
	movq	-112(%rbp), %rcx
	movq	-152(%rbp), %r8
	movq	%r12, %rsi
	sarl	%eax
	movq	%r15, %rdi
	movl	%r9d, -96(%rbp)
	movq	(%rcx,%r13,8), %rdx
	cltq
	movq	(%rdx,%rax,8), %rcx
	movq	%r12, %rdx
	call	EC_POINT_add@PLT
	movl	-96(%rbp), %r9d
	testl	%eax, %eax
	jne	.L195
.L329:
	movl	%eax, -96(%rbp)
	movq	-72(%rbp), %r9
	movq	%rbx, %r12
	jmp	.L180
.L197:
	movq	-152(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movl	%ecx, -96(%rbp)
	movl	%r9d, -120(%rbp)
	call	EC_POINT_invert@PLT
	movl	-96(%rbp), %ecx
	movl	-120(%rbp), %r9d
	testl	%eax, %eax
	jne	.L198
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L201:
	movl	%r14d, %eax
	subl	$1, %eax
	js	.L364
	testl	%r9d, %r9d
	jne	.L194
	movq	-152(%rbp), %rcx
	movq	%r12, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movl	%r9d, -96(%rbp)
	call	EC_POINT_dbl@PLT
	movl	-96(%rbp), %r9d
	testl	%eax, %eax
	je	.L329
.L194:
	subq	$1, %r14
	jmp	.L193
.L364:
	movl	%r9d, %r10d
	movq	%rbx, %r12
	movq	-72(%rbp), %r9
	testl	%r10d, %r10d
	jne	.L192
	cmpl	$0, -104(%rbp)
	movl	$1, -96(%rbp)
	je	.L180
	movq	-152(%rbp), %rdx
	movq	-184(%rbp), %rsi
	movq	%r15, %rdi
	call	EC_POINT_invert@PLT
.L334:
	testl	%eax, %eax
	movq	-72(%rbp), %r9
	setne	%al
	movzbl	%al, %eax
	movl	%eax, -96(%rbp)
	jmp	.L180
.L344:
	movl	$789, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	movl	$0, -96(%rbp)
	jmp	.L204
.L192:
	movq	-184(%rbp), %rsi
	movq	%r15, %rdi
	movq	%r9, -72(%rbp)
	call	EC_POINT_set_to_infinity@PLT
	jmp	.L334
.L362:
	movl	$163, %edx
	movl	$187, %esi
	movl	%eax, -96(%rbp)
	movq	%rbx, %r12
	movl	$753, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$16, %edi
	call	ERR_put_error@PLT
	movq	-72(%rbp), %r9
	jmp	.L180
	.cfi_endproc
.LFE429:
	.size	ec_wNAF_mul, .-ec_wNAF_mul
	.p2align 4
	.globl	ec_wNAF_precompute_mult
	.type	ec_wNAF_precompute_mult, @function
ec_wNAF_precompute_mult:
.LFB430:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	call	EC_pre_comp_free@PLT
	testq	%rbx, %rbx
	je	.L392
	movl	$52, %edx
	leaq	.LC0(%rip), %rsi
	movl	$64, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L445
	movq	%rbx, (%rax)
	movq	$8, 8(%rax)
	movq	$4, 24(%rax)
	movl	$1, 48(%rax)
	mfence
	call	CRYPTO_THREAD_lock_new@PLT
	movq	%rax, 56(%r13)
	testq	%rax, %rax
	je	.L446
	movq	%rbx, %rdi
	call	EC_GROUP_get0_generator@PLT
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L447
	movq	$0, -80(%rbp)
	testq	%r15, %r15
	je	.L448
.L371:
	movq	%r15, %rdi
	call	BN_CTX_start@PLT
	movq	%rbx, %rdi
	call	EC_GROUP_get0_order@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L395
	movq	%rax, %rdi
	call	BN_is_zero@PLT
	movl	%eax, -52(%rbp)
	testl	%eax, %eax
	jne	.L449
	movq	%r12, %rdi
	call	BN_num_bits@PLT
	cltq
	cmpq	$1999, %rax
	jbe	.L450
	movq	$32, -112(%rbp)
	movl	$5, %ecx
	movq	$6, -120(%rbp)
.L373:
	addq	$7, %rax
	movl	$881, %edx
	leaq	.LC0(%rip), %rsi
	shrq	$3, %rax
	movq	%rax, -104(%rbp)
	salq	%cl, %rax
	leaq	8(,%rax,8), %r12
	movq	%rax, -96(%rbp)
	movq	%r12, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	je	.L451
	movq	$0, -8(%rax,%r12)
	xorl	%r14d, %r14d
	cmpq	$0, -96(%rbp)
	je	.L380
	movq	%r13, -64(%rbp)
	movq	%rax, %r12
	movq	%r14, %r13
	movq	-96(%rbp), %r14
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L378:
	addq	$1, %r13
	cmpq	%r13, %r14
	je	.L452
.L375:
	movq	%rbx, %rdi
	call	EC_POINT_new@PLT
	movq	%rax, (%r12,%r13,8)
	testq	%rax, %rax
	jne	.L378
	movq	-64(%rbp), %r13
	movq	%rax, %r12
	movl	$891, %r8d
.L444:
	movl	$65, %edx
	movl	$188, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	$0, -64(%rbp)
.L379:
	movq	%r15, %rdi
	call	BN_CTX_end@PLT
	movq	-80(%rbp), %rdi
	call	BN_CTX_free@PLT
	movq	%r13, %rdi
	call	EC_ec_pre_comp_free
	movq	-72(%rbp), %rbx
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L391
	.p2align 4,,10
	.p2align 3
.L389:
	call	EC_POINT_free@PLT
	movq	8(%rbx), %rdi
	addq	$8, %rbx
	testq	%rdi, %rdi
	jne	.L389
.L391:
	movq	-72(%rbp), %rdi
	movl	$966, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L448:
	call	BN_CTX_new@PLT
	movq	%rax, -80(%rbp)
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L371
	movq	$0, -64(%rbp)
	xorl	%r12d, %r12d
	movl	$0, -52(%rbp)
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L395:
	movq	$0, -64(%rbp)
	movl	$0, -52(%rbp)
.L370:
	movq	%r15, %rdi
	call	BN_CTX_end@PLT
	movq	-80(%rbp), %rdi
	call	BN_CTX_free@PLT
	movq	%r13, %rdi
	call	EC_ec_pre_comp_free
.L390:
	movq	%r12, %rdi
	call	EC_POINT_free@PLT
	movq	-64(%rbp), %rdi
	call	EC_POINT_free@PLT
.L365:
	movl	-52(%rbp), %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L450:
	.cfi_restore_state
	cmpq	$800, %rax
	sbbq	%rdx, %rdx
	andq	$-8, %rdx
	addq	$16, %rdx
	cmpq	$800, %rax
	sbbl	%ecx, %ecx
	movq	%rdx, -112(%rbp)
	addl	$4, %ecx
	cmpq	$800, %rax
	sbbq	%rdx, %rdx
	addq	$5, %rdx
	movq	%rdx, -120(%rbp)
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L392:
	movl	$0, -52(%rbp)
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L449:
	movl	$855, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$114, %edx
	xorl	%r12d, %r12d
	movl	$188, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	movl	$0, -52(%rbp)
	movq	$0, -64(%rbp)
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L452:
	movq	-64(%rbp), %r13
.L380:
	movq	%rbx, %rdi
	call	EC_POINT_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L376
	movq	%rbx, %rdi
	call	EC_POINT_new@PLT
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L376
	movq	-88(%rbp), %rsi
	movq	%rax, %rdi
	call	EC_POINT_copy@PLT
	testl	%eax, %eax
	je	.L379
	cmpq	$0, -104(%rbp)
	je	.L386
	movq	-112(%rbp), %rax
	movq	-104(%rbp), %rcx
	movq	$0, -88(%rbp)
	movq	%r13, -128(%rbp)
	salq	$3, %rax
	subq	$1, %rcx
	movq	%rax, -112(%rbp)
	movq	-72(%rbp), %rax
	movq	%rcx, -136(%rbp)
	movq	%rax, %r13
.L385:
	movq	-64(%rbp), %rdx
	movq	%r15, %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	EC_POINT_dbl@PLT
	testl	%eax, %eax
	je	.L440
	movq	-64(%rbp), %rsi
	movq	0(%r13), %rdi
	leaq	8(%r13), %r14
	call	EC_POINT_copy@PLT
	testl	%eax, %eax
	je	.L440
	addq	-112(%rbp), %r13
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L454:
	addq	$8, %r14
	cmpq	%r14, %r13
	je	.L453
.L383:
	movq	-8(%r14), %rcx
	movq	(%r14), %rsi
	movq	%r15, %r8
	movq	%r12, %rdx
	movq	%rbx, %rdi
	call	EC_POINT_add@PLT
	testl	%eax, %eax
	jne	.L454
.L440:
	movq	-128(%rbp), %r13
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L445:
	movl	$54, %r8d
	movl	$65, %edx
	movl	$196, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$0, -52(%rbp)
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L447:
	movl	$839, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$113, %edx
	xorl	%r12d, %r12d
	movl	$188, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	movq	$0, -80(%rbp)
	movq	$0, -64(%rbp)
	movl	$0, -52(%rbp)
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L376:
	movl	$898, %r8d
	jmp	.L444
.L453:
	movq	-88(%rbp), %rcx
	cmpq	%rcx, -136(%rbp)
	ja	.L384
.L388:
	addq	$1, -88(%rbp)
	movq	-88(%rbp), %rax
	cmpq	%rax, -104(%rbp)
	jne	.L385
	movq	-128(%rbp), %r13
.L386:
	movq	-72(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r15, %rcx
	movq	%rbx, %rdi
	call	EC_POINTs_make_affine@PLT
	testl	%eax, %eax
	je	.L379
	movq	-104(%rbp), %rax
	movq	%rbx, 0(%r13)
	movq	$8, 8(%r13)
	movq	%rax, 16(%r13)
	movq	-120(%rbp), %rax
	movl	$1, -52(%rbp)
	movq	%rax, 24(%r13)
	movq	-72(%rbp), %rax
	movq	%rax, 32(%r13)
	movq	-96(%rbp), %rax
	movq	%rax, 40(%r13)
	movq	%r13, 160(%rbx)
	xorl	%r13d, %r13d
	movl	$5, 152(%rbx)
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L446:
	movl	$65, %r8d
	movl	$65, %edx
	movl	$196, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$66, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	movl	$0, -52(%rbp)
	jmp	.L365
.L451:
	movl	$883, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$188, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	movq	$0, -64(%rbp)
	jmp	.L370
.L384:
	movq	-64(%rbp), %rsi
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%rbx, %rdi
	call	EC_POINT_dbl@PLT
	testl	%eax, %eax
	je	.L440
	movq	%r12, -144(%rbp)
	movq	%r15, %r12
	movq	%rbx, %r15
	movq	-64(%rbp), %rbx
	movl	$6, %r14d
.L387:
	movq	%r12, %rcx
	movq	%rbx, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	EC_POINT_dbl@PLT
	testl	%eax, %eax
	je	.L441
	subq	$1, %r14
	jne	.L387
	movq	%r15, %rbx
	movq	%r12, %r15
	movq	-144(%rbp), %r12
	jmp	.L388
.L441:
	movq	%r12, %r15
	movq	-128(%rbp), %r13
	movq	-144(%rbp), %r12
	jmp	.L379
	.cfi_endproc
.LFE430:
	.size	ec_wNAF_precompute_mult, .-ec_wNAF_precompute_mult
	.p2align 4
	.globl	ec_wNAF_have_precompute_mult
	.type	ec_wNAF_have_precompute_mult, @function
ec_wNAF_have_precompute_mult:
.LFB431:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$5, 152(%rdi)
	je	.L458
	ret
	.p2align 4,,10
	.p2align 3
.L458:
	xorl	%eax, %eax
	cmpq	$0, 160(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE431:
	.size	ec_wNAF_have_precompute_mult, .-ec_wNAF_have_precompute_mult
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
