	.file	"a_strnid.c"
	.text
	.p2align 4
	.type	sk_table_cmp, @function
sk_table_cmp:
.LFB423:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rsi), %rdx
	movl	(%rax), %eax
	subl	(%rdx), %eax
	ret
	.cfi_endproc
.LFE423:
	.size	sk_table_cmp, .-sk_table_cmp
	.p2align 4
	.type	table_cmp_BSEARCH_CMP_FN, @function
table_cmp_BSEARCH_CMP_FN:
.LFB425:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	subl	(%rsi), %eax
	ret
	.cfi_endproc
.LFE425:
	.size	table_cmp_BSEARCH_CMP_FN, .-table_cmp_BSEARCH_CMP_FN
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/asn1/a_strnid.c"
	.text
	.p2align 4
	.type	st_free, @function
st_free:
.LFB431:
	.cfi_startproc
	endbr64
	testb	$1, 32(%rdi)
	jne	.L6
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$218, %edx
	leaq	.LC0(%rip), %rsi
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE431:
	.size	st_free, .-st_free
	.p2align 4
	.globl	ASN1_STRING_set_default_mask
	.type	ASN1_STRING_set_default_mask, @function
ASN1_STRING_set_default_mask:
.LFB419:
	.cfi_startproc
	endbr64
	movq	%rdi, global_mask(%rip)
	ret
	.cfi_endproc
.LFE419:
	.size	ASN1_STRING_set_default_mask, .-ASN1_STRING_set_default_mask
	.p2align 4
	.globl	ASN1_STRING_get_default_mask
	.type	ASN1_STRING_get_default_mask, @function
ASN1_STRING_get_default_mask:
.LFB420:
	.cfi_startproc
	endbr64
	movq	global_mask(%rip), %rax
	ret
	.cfi_endproc
.LFE420:
	.size	ASN1_STRING_get_default_mask, .-ASN1_STRING_get_default_mask
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"MASK:"
.LC2:
	.string	"nombstr"
.LC3:
	.string	"pkix"
.LC4:
	.string	"utf8only"
.LC5:
	.string	"default"
	.text
	.p2align 4
	.globl	ASN1_STRING_set_default_mask_asc
	.type	ASN1_STRING_set_default_mask_asc, @function
ASN1_STRING_set_default_mask_asc:
.LFB421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	leaq	.LC1(%rip), %rdi
	movq	%rax, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rcx
	movq	%rcx, -8(%rbp)
	xorl	%ecx, %ecx
	movl	$5, %ecx
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	jne	.L10
	cmpb	$0, 5(%rax)
	jne	.L11
.L13:
	xorl	%eax, %eax
.L9:
	movq	-8(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L20
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movl	$8, %ecx
	leaq	.LC2(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	jne	.L21
	movq	$-10241, %rax
.L14:
	movq	%rax, global_mask(%rip)
	movl	$1, %eax
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L21:
	movl	$5, %ecx
	leaq	.LC3(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L17
	movl	$9, %ecx
	leaq	.LC4(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L18
	movq	%rax, %rsi
	movl	$8, %ecx
	leaq	.LC5(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L13
	movl	$4294967295, %eax
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L11:
	xorl	%edx, %edx
	leaq	-16(%rbp), %rsi
	leaq	5(%rax), %rdi
	call	strtoul@PLT
	movq	-16(%rbp), %rdx
	cmpb	$0, (%rdx)
	je	.L14
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L17:
	movq	$-5, %rax
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$8192, %eax
	jmp	.L14
.L20:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE421:
	.size	ASN1_STRING_set_default_mask_asc, .-ASN1_STRING_set_default_mask_asc
	.p2align 4
	.globl	ASN1_STRING_set_by_NID
	.type	ASN1_STRING_set_by_NID, @function
ASN1_STRING_set_by_NID:
.LFB422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r15
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	movq	stable(%rip), %rdi
	leaq	-104(%rbp), %rax
	movq	$0, -104(%rbp)
	cmove	%rax, %rbx
	movl	%r8d, -96(%rbp)
	testq	%rdi, %rdi
	je	.L24
	movq	%r15, %rsi
	call	OPENSSL_sk_find@PLT
	testl	%eax, %eax
	jns	.L36
.L24:
	leaq	table_cmp_BSEARCH_CMP_FN(%rip), %r8
	movl	$40, %ecx
	movl	$27, %edx
	movq	%r15, %rdi
	leaq	tbl_standard(%rip), %rsi
	call	OBJ_bsearch_@PLT
	testq	%rax, %rax
	je	.L26
.L39:
	movq	24(%rax), %r8
	testb	$2, 32(%rax)
	je	.L37
.L27:
	subq	$8, %rsp
	movq	8(%rax), %r9
	pushq	16(%rax)
	movl	%r14d, %ecx
	movl	%r13d, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	ASN1_mbstring_ncopy@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	jle	.L31
.L40:
	movq	(%rbx), %rax
.L22:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L38
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	andq	global_mask(%rip), %r8
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L36:
	movq	stable(%rip), %rdi
	movl	%eax, %esi
	call	OPENSSL_sk_value@PLT
	testq	%rax, %rax
	jne	.L39
	.p2align 4,,10
	.p2align 3
.L26:
	movq	global_mask(%rip), %r8
	movl	%r14d, %ecx
	movl	%r13d, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	andl	$10246, %r8d
	call	ASN1_mbstring_copy@PLT
	testl	%eax, %eax
	jg	.L40
	.p2align 4,,10
	.p2align 3
.L31:
	xorl	%eax, %eax
	jmp	.L22
.L38:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE422:
	.size	ASN1_STRING_set_by_NID, .-ASN1_STRING_set_by_NID
	.p2align 4
	.globl	ASN1_STRING_TABLE_get
	.type	ASN1_STRING_TABLE_get, @function
ASN1_STRING_TABLE_get:
.LFB427:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-64(%rbp), %r12
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	%edi, -64(%rbp)
	movq	stable(%rip), %rdi
	testq	%rdi, %rdi
	je	.L42
	movq	%r12, %rsi
	call	OPENSSL_sk_find@PLT
	testl	%eax, %eax
	jns	.L49
.L42:
	leaq	table_cmp_BSEARCH_CMP_FN(%rip), %r8
	movl	$40, %ecx
	movl	$27, %edx
	movq	%r12, %rdi
	leaq	tbl_standard(%rip), %rsi
	call	OBJ_bsearch_@PLT
.L41:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L50
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	movq	stable(%rip), %rdi
	movl	%eax, %esi
	call	OPENSSL_sk_value@PLT
	jmp	.L41
.L50:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE427:
	.size	ASN1_STRING_TABLE_get, .-ASN1_STRING_TABLE_get
	.p2align 4
	.globl	ASN1_STRING_TABLE_add
	.type	ASN1_STRING_TABLE_add, @function
ASN1_STRING_TABLE_add:
.LFB429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$72, %rsp
	movl	%edi, -100(%rbp)
	movq	stable(%rip), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L82
.L52:
	leaq	-96(%rbp), %r9
	movl	-100(%rbp), %eax
	movq	%r9, %rsi
	movq	%r9, -112(%rbp)
	movl	%eax, -96(%rbp)
	call	OPENSSL_sk_find@PLT
	movq	-112(%rbp), %r9
	testl	%eax, %eax
	jns	.L83
	leaq	table_cmp_BSEARCH_CMP_FN(%rip), %r8
	movl	$40, %ecx
	movl	$27, %edx
	movq	%r9, %rdi
	leaq	tbl_standard(%rip), %rsi
	call	OBJ_bsearch_@PLT
	movq	%rax, %r15
.L54:
	testq	%r15, %r15
	je	.L55
	testb	$1, 32(%r15)
	jne	.L68
.L55:
	movl	$159, %edx
	leaq	.LC0(%rip), %rsi
	movl	$40, %edi
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L84
	movq	stable(%rip), %rdi
	movq	%rax, %rsi
	movq	%rax, -112(%rbp)
	call	OPENSSL_sk_push@PLT
	movq	-112(%rbp), %r9
	testl	%eax, %eax
	je	.L85
	testq	%r15, %r15
	je	.L59
	movl	(%r15), %eax
	movdqu	8(%r15), %xmm1
	movl	%eax, (%r9)
	movups	%xmm1, 8(%r9)
	movq	24(%r15), %rax
	movq	%rax, 24(%r9)
	movq	32(%r15), %rax
	orq	$1, %rax
	movq	%rax, 32(%r9)
.L56:
	testq	%r12, %r12
	js	.L64
	movq	%r12, 8(%r9)
.L64:
	testq	%r13, %r13
	js	.L61
	movq	%r13, 16(%r9)
.L61:
	testq	%r14, %r14
	je	.L62
	movq	%r14, 24(%r9)
.L62:
	movl	$1, %eax
	testq	%rbx, %rbx
	je	.L51
	orq	$1, %rbx
	movq	%rbx, 32(%r9)
.L51:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L86
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	leaq	sk_table_cmp(%rip), %rdi
	call	OPENSSL_sk_new@PLT
	movq	%rax, stable(%rip)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L52
	.p2align 4,,10
	.p2align 3
.L53:
	movl	$190, %r8d
	movl	$65, %edx
	movl	$129, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L68:
	movq	%r15, %r9
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L59:
	movl	-100(%rbp), %eax
	pcmpeqd	%xmm0, %xmm0
	movq	$1, 32(%r9)
	movups	%xmm0, 8(%r9)
	movl	%eax, (%r9)
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L83:
	movq	stable(%rip), %rdi
	movl	%eax, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r15
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L85:
	movl	$164, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r9, %rdi
	call	CRYPTO_free@PLT
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L84:
	movl	$160, %r8d
	movl	$65, %edx
	movl	$138, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L53
.L86:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE429:
	.size	ASN1_STRING_TABLE_add, .-ASN1_STRING_TABLE_add
	.p2align 4
	.globl	ASN1_STRING_TABLE_cleanup
	.type	ASN1_STRING_TABLE_cleanup, @function
ASN1_STRING_TABLE_cleanup:
.LFB430:
	.cfi_startproc
	endbr64
	movq	stable(%rip), %rdi
	testq	%rdi, %rdi
	je	.L87
	leaq	st_free(%rip), %rsi
	movq	$0, stable(%rip)
	jmp	OPENSSL_sk_pop_free@PLT
	.p2align 4,,10
	.p2align 3
.L87:
	ret
	.cfi_endproc
.LFE430:
	.size	ASN1_STRING_TABLE_cleanup, .-ASN1_STRING_TABLE_cleanup
	.section	.rodata
	.align 32
	.type	tbl_standard, @object
	.size	tbl_standard, 1080
tbl_standard:
	.long	13
	.zero	4
	.quad	1
	.quad	64
	.quad	10246
	.quad	0
	.long	14
	.zero	4
	.quad	2
	.quad	2
	.quad	2
	.quad	2
	.long	15
	.zero	4
	.quad	1
	.quad	128
	.quad	10246
	.quad	0
	.long	16
	.zero	4
	.quad	1
	.quad	128
	.quad	10246
	.quad	0
	.long	17
	.zero	4
	.quad	1
	.quad	64
	.quad	10246
	.quad	0
	.long	18
	.zero	4
	.quad	1
	.quad	64
	.quad	10246
	.quad	0
	.long	48
	.zero	4
	.quad	1
	.quad	128
	.quad	16
	.quad	2
	.long	49
	.zero	4
	.quad	1
	.quad	-1
	.quad	10262
	.quad	0
	.long	54
	.zero	4
	.quad	1
	.quad	-1
	.quad	10262
	.quad	0
	.long	55
	.zero	4
	.quad	1
	.quad	-1
	.quad	10246
	.quad	0
	.long	99
	.zero	4
	.quad	1
	.quad	32768
	.quad	10246
	.quad	0
	.long	100
	.zero	4
	.quad	1
	.quad	32768
	.quad	10246
	.quad	0
	.long	101
	.zero	4
	.quad	1
	.quad	32768
	.quad	10246
	.quad	0
	.long	105
	.zero	4
	.quad	1
	.quad	64
	.quad	2
	.quad	2
	.long	156
	.zero	4
	.quad	-1
	.quad	-1
	.quad	2048
	.quad	2
	.long	173
	.zero	4
	.quad	1
	.quad	32768
	.quad	10246
	.quad	0
	.long	174
	.zero	4
	.quad	-1
	.quad	-1
	.quad	2
	.quad	2
	.long	391
	.zero	4
	.quad	1
	.quad	-1
	.quad	16
	.quad	2
	.long	417
	.zero	4
	.quad	-1
	.quad	-1
	.quad	2048
	.quad	2
	.long	460
	.zero	4
	.quad	1
	.quad	256
	.quad	16
	.quad	2
	.long	957
	.zero	4
	.quad	2
	.quad	2
	.quad	2
	.quad	2
	.long	1004
	.zero	4
	.quad	1
	.quad	12
	.quad	1
	.quad	2
	.long	1005
	.zero	4
	.quad	1
	.quad	13
	.quad	1
	.quad	2
	.long	1006
	.zero	4
	.quad	1
	.quad	11
	.quad	1
	.quad	2
	.long	1090
	.zero	4
	.quad	3
	.quad	3
	.quad	2
	.quad	2
	.long	1091
	.zero	4
	.quad	3
	.quad	3
	.quad	1
	.quad	2
	.long	1092
	.zero	4
	.quad	0
	.quad	-1
	.quad	8192
	.quad	2
	.data
	.align 8
	.type	global_mask, @object
	.size	global_mask, 8
global_mask:
	.quad	8192
	.local	stable
	.comm	stable,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
