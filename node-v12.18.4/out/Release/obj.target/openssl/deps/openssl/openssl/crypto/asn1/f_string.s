	.file	"f_string.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"0"
.LC1:
	.string	"\\\n"
.LC2:
	.string	"0123456789ABCDEF"
	.text
	.p2align 4
	.globl	i2a_ASN1_STRING
	.type	i2a_ASN1_STRING, @function
i2a_ASN1_STRING:
.LFB419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L9
	movl	(%rsi), %eax
	movq	%rdi, %r14
	movq	%rsi, %rbx
	testl	%eax, %eax
	je	.L3
	leaq	-58(%rbp), %rax
	movl	$0, %r15d
	movl	$0, %r12d
	movq	%rax, -72(%rbp)
	leaq	.LC2(%rip), %r13
	jg	.L4
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L5:
	addq	$1, %r15
	movl	%eax, %r12d
.L4:
	movq	8(%rbx), %rdx
	movq	-72(%rbp), %rsi
	movq	%r14, %rdi
	addq	%r15, %rdx
	movzbl	(%rdx), %eax
	shrb	$4, %al
	movzbl	%al, %eax
	movzbl	0(%r13,%rax), %eax
	movb	%al, -58(%rbp)
	movzbl	(%rdx), %eax
	movl	$2, %edx
	andl	$15, %eax
	movzbl	0(%r13,%rax), %eax
	movb	%al, -57(%rbp)
	call	BIO_write@PLT
	cmpl	$2, %eax
	jne	.L6
	leal	2(%r12), %eax
	leal	1(%r15), %edx
	cmpl	%edx, (%rbx)
	jle	.L1
	imull	$-1963413621, %edx, %edx
	addl	$61356675, %edx
	cmpl	$122713350, %edx
	ja	.L5
	movl	$2, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	call	BIO_write@PLT
	cmpl	$2, %eax
	jne	.L6
	leal	4(%r12), %eax
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L9:
	xorl	%eax, %eax
.L1:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L17
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	movl	$1, %edx
	leaq	.LC0(%rip), %rsi
	call	BIO_write@PLT
	cmpl	$1, %eax
	je	.L1
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$-1, %eax
	jmp	.L1
.L17:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE419:
	.size	i2a_ASN1_STRING, .-i2a_ASN1_STRING
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"../deps/openssl/openssl/crypto/asn1/f_string.c"
	.text
	.p2align 4
	.globl	a2i_ASN1_STRING
	.type	a2i_ASN1_STRING, @function
a2i_ASN1_STRING:
.LFB420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movl	%ecx, %edx
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -96(%rbp)
	movq	%r13, %rsi
	movq	%rdi, -88(%rbp)
	movl	%ecx, -80(%rbp)
	call	BIO_gets@PLT
	testl	%eax, %eax
	jle	.L37
	movl	$0, -76(%rbp)
	movl	%eax, %ebx
	movl	$0, -52(%rbp)
	movq	$0, -64(%rbp)
.L36:
	movslq	%ebx, %rax
	leal	-1(%rbx), %r15d
	movzbl	-1(%r13,%rax), %r12d
	movq	%rax, -72(%rbp)
	cmpb	$10, %r12b
	je	.L54
	cmpb	$13, %r12b
	je	.L55
.L22:
	xorl	%eax, %eax
	cmpb	$92, %r12b
	sete	%al
	movl	%eax, -56(%rbp)
	testl	%r15d, %r15d
	je	.L23
	movslq	%r15d, %rdx
	addq	%r13, %rdx
	movq	%rdx, %r14
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L57:
	subq	$1, %r14
	subl	$1, %r15d
	je	.L56
.L25:
	movsbl	(%r14), %edi
	movl	$16, %esi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	jne	.L57
.L24:
	movb	$0, (%r14)
	cmpl	$1, %r15d
	je	.L21
	movl	%r15d, %ecx
	subl	-56(%rbp), %ecx
	testb	$1, %cl
	jne	.L58
	movl	-52(%rbp), %eax
	sarl	%ecx
	movl	%ecx, %r15d
	addl	%ecx, %eax
	movl	%eax, -72(%rbp)
	cmpl	-76(%rbp), %eax
	jg	.L59
.L28:
	testl	%r15d, %r15d
	je	.L35
	movslq	-52(%rbp), %rax
	movq	-64(%rbp), %rsi
	leal	-1(%r15), %ecx
	movq	%r13, %rbx
	leaq	1(%rax,%rcx), %r15
	leaq	(%rsi,%rax), %r14
	addq	%rsi, %r15
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L60:
	movzbl	(%r14), %eax
	sall	$4, %eax
	orl	%edx, %eax
	movb	%al, (%r14)
	movzbl	1(%rbx), %edi
	call	OPENSSL_hexchar2int@PLT
	movl	%eax, %edx
	testl	%eax, %eax
	js	.L32
	movzbl	(%r14), %eax
	addq	$1, %r14
	addq	$2, %rbx
	sall	$4, %eax
	orl	%edx, %eax
	movb	%al, -1(%r14)
	cmpq	%r14, %r15
	je	.L35
.L34:
	movzbl	(%rbx), %edi
	call	OPENSSL_hexchar2int@PLT
	movl	%eax, %edx
	testl	%eax, %eax
	jns	.L60
.L32:
	movl	$113, %r8d
	movl	$141, %edx
	movl	$103, %esi
	movl	$13, %edi
	leaq	.LC3(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-64(%rbp), %rdi
	movl	$115, %edx
	leaq	.LC3(%rip), %rsi
	call	CRYPTO_free@PLT
	xorl	%eax, %eax
.L18:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	movq	-72(%rbp), %r14
	movl	%ebx, %r15d
	addq	%r13, %r14
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L54:
	movslq	%r15d, %rax
	movq	%rax, -72(%rbp)
	movb	$0, 0(%r13,%rax)
	testl	%r15d, %r15d
	je	.L21
	movzbl	-1(%r13,%rax), %r12d
	leal	-2(%rbx), %eax
	movl	%r15d, %ebx
	movl	%eax, %r15d
	cmpb	$13, %r12b
	jne	.L22
.L55:
	movslq	%r15d, %rax
	movq	%rax, -72(%rbp)
	movb	$0, 0(%r13,%rax)
	testl	%r15d, %r15d
	je	.L21
	movl	%r15d, %ebx
	movzbl	-1(%r13,%rax), %r12d
	subl	$1, %r15d
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L35:
	cmpb	$92, %r12b
	jne	.L19
	movl	-80(%rbp), %edx
	movq	-88(%rbp), %rdi
	movq	%r13, %rsi
	call	BIO_gets@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	jle	.L21
	movl	-72(%rbp), %eax
	movl	%eax, -52(%rbp)
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L59:
	movl	-52(%rbp), %eax
	movq	-64(%rbp), %rdi
	leaq	.LC3(%rip), %rdx
	leal	(%rax,%rcx,2), %esi
	movl	$100, %ecx
	call	CRYPTO_realloc@PLT
	testq	%rax, %rax
	je	.L61
	movl	-72(%rbp), %ecx
	movq	%rax, -64(%rbp)
	addl	%r15d, %ecx
	movl	%ecx, -76(%rbp)
	jmp	.L28
.L23:
	movb	$0, 1(%r13)
.L21:
	movl	$133, %r8d
	movl	$150, %edx
	movl	$103, %esi
	movl	$13, %edi
	leaq	.LC3(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-64(%rbp), %rdi
	movl	$134, %edx
	leaq	.LC3(%rip), %rsi
	call	CRYPTO_free@PLT
	xorl	%eax, %eax
	jmp	.L18
.L37:
	movl	$0, -72(%rbp)
	movq	$0, -64(%rbp)
.L19:
	movq	-96(%rbp), %rax
	movl	-72(%rbp), %ecx
	movl	%ecx, (%rax)
	movq	-64(%rbp), %rcx
	movq	%rcx, 8(%rax)
	movl	$1, %eax
	jmp	.L18
.L58:
	movl	$94, %r8d
	movl	$145, %edx
	movl	$103, %esi
	movl	$13, %edi
	leaq	.LC3(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-64(%rbp), %rdi
	movl	$95, %edx
	leaq	.LC3(%rip), %rsi
	call	CRYPTO_free@PLT
	xorl	%eax, %eax
	jmp	.L18
.L61:
	movl	$102, %r8d
	movl	$65, %edx
	movl	$103, %esi
	movl	$13, %edi
	leaq	.LC3(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-64(%rbp), %rdi
	movl	$103, %edx
	leaq	.LC3(%rip), %rsi
	call	CRYPTO_free@PLT
	xorl	%eax, %eax
	jmp	.L18
	.cfi_endproc
.LFE420:
	.size	a2i_ASN1_STRING, .-a2i_ASN1_STRING
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
