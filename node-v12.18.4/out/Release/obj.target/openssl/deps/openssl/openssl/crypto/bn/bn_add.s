	.file	"bn_add.c"
	.text
	.p2align 4
	.globl	BN_uadd
	.type	BN_uadd, @function
BN_uadd:
.LFB254:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movslq	8(%rsi), %r12
	movl	8(%rdx), %r15d
	cmpl	%r15d, %r12d
	jl	.L2
	movl	%r15d, %eax
	movq	%rsi, %rbx
	movl	%r12d, %r15d
	movq	%rdx, %r14
	movslq	%eax, %r12
.L2:
	leal	1(%r15), %esi
	movq	%r13, %rdi
	call	bn_wexpand@PLT
	movq	%rax, %r9
	xorl	%eax, %eax
	testq	%r9, %r9
	je	.L1
	movl	%r15d, 8(%r13)
	movl	%r15d, %r8d
	movq	(%rbx), %r15
	movl	%r12d, %ecx
	movq	0(%r13), %rbx
	movq	(%r14), %rdx
	subl	%r12d, %r8d
	salq	$3, %r12
	movq	%r15, %rsi
	movl	%r8d, -52(%rbp)
	movq	%rbx, %rdi
	call	bn_add_words@PLT
	movl	-52(%rbp), %r8d
	leaq	(%rbx,%r12), %rsi
	addq	%r15, %r12
	testl	%r8d, %r8d
	je	.L4
	leal	-1(%r8), %edi
	xorl	%edx, %edx
	movq	%rdi, %r14
	.p2align 4,,10
	.p2align 3
.L5:
	movq	%rax, %rcx
	addq	(%r12,%rdx,8), %rcx
	movq	%rcx, (%rsi,%rdx,8)
	sete	%cl
	movzbl	%cl, %ecx
	andq	%rcx, %rax
	movq	%rdx, %rcx
	addq	$1, %rdx
	cmpq	%rcx, %rdi
	jne	.L5
	leaq	8(%rsi,%r14,8), %rsi
.L4:
	movq	%rax, (%rsi)
	movl	$0, 16(%r13)
	addl	%eax, 8(%r13)
	movl	$1, %eax
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE254:
	.size	BN_uadd, .-BN_uadd
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/bn/bn_add.c"
	.text
	.p2align 4
	.globl	BN_usub
	.type	BN_usub, @function
BN_usub:
.LFB255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	8(%rsi), %r12d
	movl	8(%rdx), %r15d
	movl	%r12d, %r13d
	subl	%r15d, %r13d
	js	.L33
	movq	%rsi, %rbx
	movl	%r12d, %esi
	movq	%rdx, -56(%rbp)
	movq	%rdi, %r14
	call	bn_wexpand@PLT
	movq	%rax, %r8
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L14
	movq	-56(%rbp), %rdx
	movq	(%rbx), %r8
	movl	%r15d, %ecx
	movq	(%r14), %rbx
	movq	(%rdx), %rdx
	movq	%r8, %rsi
	movq	%r8, -56(%rbp)
	movq	%rbx, %rdi
	call	bn_sub_words@PLT
	movq	-56(%rbp), %r8
	movslq	%r15d, %rcx
	salq	$3, %rcx
	addq	%rcx, %r8
	addq	%rbx, %rcx
	testl	%r13d, %r13d
	je	.L17
	leal	-1(%r13), %r9d
	xorl	%edx, %edx
	movq	%r9, %r13
	.p2align 4,,10
	.p2align 3
.L18:
	movq	(%r8,%rdx,8), %rsi
	movq	%rsi, %rdi
	subq	%rax, %rdi
	testq	%rsi, %rsi
	sete	%sil
	movq	%rdi, (%rcx,%rdx,8)
	movzbl	%sil, %esi
	andq	%rsi, %rax
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%r9, %rsi
	jne	.L18
	movslq	%r13d, %r13
	leaq	8(%rcx,%r13,8), %rcx
.L17:
	testl	%r12d, %r12d
	jne	.L19
	.p2align 4,,10
	.p2align 3
.L20:
	movl	%r12d, 8(%r14)
	movl	$1, %eax
	movl	$0, 16(%r14)
.L14:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	subl	$1, %r12d
	je	.L20
.L19:
	subq	$8, %rcx
	cmpq	$0, (%rcx)
	je	.L21
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L33:
	movl	$139, %r8d
	movl	$100, %edx
	movl	$115, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE255:
	.size	BN_usub, .-BN_usub
	.p2align 4
	.globl	BN_add
	.type	BN_add, @function
BN_add:
.LFB252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	16(%rsi), %ebx
	cmpl	16(%rdx), %ebx
	je	.L40
	movq	%rdx, %rsi
	movq	%r12, %rdi
	call	BN_ucmp@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	jg	.L41
	je	.L38
	movl	16(%r13), %ebx
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	BN_usub
	movl	%ebx, 16(%r14)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movl	16(%r12), %ebx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	BN_usub
	movl	%ebx, 16(%r14)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	call	BN_uadd
	movl	%ebx, 16(%r14)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	movq	%r14, %rdi
	xorl	%esi, %esi
	call	BN_set_word@PLT
	movl	%ebx, 16(%r14)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE252:
	.size	BN_add, .-BN_add
	.p2align 4
	.globl	BN_sub
	.type	BN_sub, @function
BN_sub:
.LFB253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movl	16(%rsi), %r14d
	cmpl	16(%rdx), %r14d
	je	.L43
	call	BN_uadd
.L44:
	movl	%r14d, 16(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	movq	%rdx, %rsi
	movq	%r12, %rdi
	call	BN_ucmp@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	jg	.L48
	je	.L46
	movl	16(%r13), %eax
	xorl	%r14d, %r14d
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	testl	%eax, %eax
	sete	%r14b
	call	BN_usub
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L48:
	movl	16(%r12), %r14d
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	BN_usub
	movl	%r14d, 16(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	BN_set_word@PLT
	movl	$1, %eax
	jmp	.L44
	.cfi_endproc
.LFE253:
	.size	BN_sub, .-BN_sub
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
