	.file	"bss_acpt.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/bio/bss_acpt.c"
	.text
	.p2align 4
	.type	acpt_new, @function
acpt_new:
.LFB268:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$95, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	$0, 32(%rdi)
	movl	$-1, 48(%rdi)
	movl	$0, 40(%rdi)
	movl	$320, %edi
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L6
	movl	$256, 4(%rax)
	movl	$-1, 32(%rax)
	movq	%rax, 56(%rbx)
	movl	$1, (%rax)
	movl	$1, %eax
	movl	$1, 36(%rbx)
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movl	$96, %r8d
	movl	$65, %edx
	movl	$152, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L1
	.cfi_endproc
.LFE268:
	.size	acpt_new, .-acpt_new
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	" service="
.LC2:
	.string	"hostname="
	.text
	.p2align 4
	.type	acpt_state, @function
acpt_state:
.LFB273:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$6, (%rsi)
	ja	.L29
	movl	(%rsi), %eax
	leaq	.L10(%rip), %rdx
	movq	%rdi, %r13
	movq	%rsi, %rbx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L10:
	.long	.L29-.L10
	.long	.L15-.L10
	.long	.L14-.L10
	.long	.L13-.L10
	.long	.L12-.L10
	.long	.L11-.L10
	.long	.L9-.L10
	.text
	.p2align 4,,10
	.p2align 3
.L15:
	cmpq	$0, 16(%rsi)
	je	.L50
.L16:
	movq	168(%rbx), %rdi
	movl	$170, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	176(%rbx), %rdi
	movq	$0, 168(%rbx)
	movl	$172, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	296(%rbx), %rdi
	movq	$0, 176(%rbx)
	movl	$174, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	304(%rbx), %rdi
	movq	$0, 296(%rbx)
	movl	$176, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$2, (%rbx)
	movq	$0, 304(%rbx)
.L14:
	movl	4(%rbx), %eax
	cmpl	$6, %eax
	je	.L30
	cmpl	$256, %eax
	je	.L31
	cmpl	$4, %eax
	je	.L32
	movl	$206, %r8d
	movl	$146, %edx
	movl	$100, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L32:
	movl	$2, %ecx
.L17:
	movq	24(%rbx), %rsi
	movq	16(%rbx), %rdi
	leaq	40(%rbx), %r9
	movl	$1, %r8d
	movl	$1, %edx
	call	BIO_lookup@PLT
	testl	%eax, %eax
	je	.L46
	movq	40(%rbx), %rax
	testq	%rax, %rax
	je	.L51
	movq	%rax, 48(%rbx)
	movl	$3, (%rbx)
.L13:
	movq	48(%rbx), %rdi
	call	BIO_ADDRINFO_protocol@PLT
	movq	48(%rbx), %rdi
	movl	%eax, %r14d
	call	BIO_ADDRINFO_socktype@PLT
	movq	48(%rbx), %rdi
	movl	%eax, %r12d
	call	BIO_ADDRINFO_family@PLT
	movl	%r12d, %esi
	xorl	%ecx, %ecx
	movl	%r14d, %edx
	movl	%eax, %edi
	call	BIO_socket@PLT
	movl	%eax, %r12d
	cmpl	$-1, %eax
	je	.L52
	movl	%eax, 32(%rbx)
	movl	%eax, 48(%r13)
	movl	$4, (%rbx)
.L12:
	movq	48(%rbx), %rdi
	movl	8(%rbx), %r12d
	call	BIO_ADDRINFO_address@PLT
	movl	32(%rbx), %edi
	movl	%r12d, %edx
	movq	%rax, %rsi
	call	BIO_listen@PLT
	testl	%eax, %eax
	je	.L47
	movl	32(%rbx), %edi
	leaq	56(%rbx), %r12
	xorl	%esi, %esi
	leaq	-64(%rbp), %rdx
	movq	%r12, -64(%rbp)
	call	BIO_sock_info@PLT
	testl	%eax, %eax
	jne	.L21
.L47:
	movl	32(%rbx), %edi
	movl	$-1, %r12d
	call	BIO_closesocket@PLT
.L7:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L53
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L9:
	.cfi_restore_state
	cmpq	$0, 64(%rdi)
	jne	.L49
	movl	$5, (%rsi)
.L11:
	cmpq	$0, 64(%r13)
	je	.L22
.L48:
	movl	$6, (%rbx)
.L49:
	movl	$1, %r12d
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L21:
	movq	%r12, %rdi
	movl	$1, %esi
	call	BIO_ADDR_hostname_string@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movl	$1, %r12d
	movq	%rax, 168(%rbx)
	call	BIO_ADDR_service_string@PLT
	movl	$5, (%rbx)
	movq	%rax, 176(%rbx)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L50:
	cmpq	$0, 24(%rsi)
	jne	.L16
	movl	$159, %r8d
	movl	$143, %edx
	movl	$100, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	movq	16(%rbx), %rdx
	movq	24(%rbx), %r8
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rcx
	leaq	.LC2(%rip), %rsi
	movl	$4, %edi
	call	ERR_add_error_data@PLT
	jmp	.L7
.L8:
.L36:
	movq	%r14, %r12
.L25:
	movq	%r12, %rdi
	call	BIO_free@PLT
.L46:
	movl	$-1, %r12d
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L31:
	xorl	%ecx, %ecx
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L30:
	movl	$10, %ecx
	jmp	.L17
.L52:
	call	__errno_location@PLT
	movl	$229, %r8d
	movl	$4, %esi
	leaq	.LC0(%rip), %rcx
	movl	(%rax), %edx
	movl	$2, %edi
	call	ERR_put_error@PLT
	movq	16(%rbx), %rdx
	movq	24(%rbx), %r8
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rcx
	leaq	.LC2(%rip), %rsi
	movl	$4, %edi
	call	ERR_add_error_data@PLT
	movl	$233, %r8d
	movl	$118, %edx
	leaq	.LC0(%rip), %rcx
	movl	$100, %esi
	movl	$32, %edi
	call	ERR_put_error@PLT
	jmp	.L7
.L22:
	movl	$15, %esi
	movq	%r13, %rdi
	leaq	184(%rbx), %r15
	call	BIO_clear_flags@PLT
	movl	$0, 44(%r13)
	movq	296(%rbx), %rdi
	movl	$280, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	304(%rbx), %rdi
	movq	$0, 296(%rbx)
	movl	$282, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	12(%rbx), %edx
	movl	32(%rbx), %edi
	movq	%r15, %rsi
	movq	$0, 304(%rbx)
	call	BIO_accept_ex@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	js	.L54
	movl	$1, %esi
	movl	%eax, %edi
	call	BIO_new_socket@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L55
	movq	%r13, %rdi
	call	BIO_get_callback@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	BIO_set_callback@PLT
	movq	%r13, %rdi
	call	BIO_get_callback_arg@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	BIO_set_callback_arg@PLT
	movq	312(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L34
	call	BIO_dup_chain@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L36
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	BIO_push@PLT
	testq	%rax, %rax
	je	.L36
.L24:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	BIO_push@PLT
	testq	%rax, %rax
	je	.L25
	movl	$1, %esi
	movq	%r15, %rdi
	call	BIO_ADDR_hostname_string@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%rax, 296(%rbx)
	call	BIO_ADDR_service_string@PLT
	movq	%rax, 304(%rbx)
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L55:
	movl	%r12d, %edi
	movl	$-1, %r12d
	call	BIO_closesocket@PLT
	jmp	.L7
.L51:
	movl	$214, %r8d
	movl	$142, %edx
	movl	$100, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L7
.L54:
	movl	%eax, %edi
	call	BIO_sock_should_retry@PLT
	testl	%eax, %eax
	je	.L7
	movl	$12, %esi
	movq	%r13, %rdi
	orl	$-1, %r12d
	call	BIO_set_flags@PLT
	movl	$3, 44(%r13)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L34:
	movq	%r14, %r12
	jmp	.L24
.L53:
	call	__stack_chk_fail@PLT
.L29:
	xorl	%r12d, %r12d
	jmp	.L7
	.cfi_endproc
.LFE273:
	.size	acpt_state, .-acpt_state
	.p2align 4
	.type	acpt_read, @function
acpt_read:
.LFB274:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	movl	$15, %esi
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	call	BIO_clear_flags@PLT
	movq	56(%r14), %rbx
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L59:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	acpt_state
	testl	%eax, %eax
	jle	.L56
.L57:
	movq	64(%r14), %rdi
	testq	%rdi, %rdi
	je	.L59
	movl	%r13d, %edx
	movq	%r12, %rsi
	call	BIO_read@PLT
	movq	%r14, %rdi
	movl	%eax, -36(%rbp)
	call	BIO_copy_next_retry@PLT
	movl	-36(%rbp), %eax
.L56:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE274:
	.size	acpt_read, .-acpt_read
	.p2align 4
	.type	acpt_write, @function
acpt_write:
.LFB275:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	movl	$15, %esi
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	call	BIO_clear_flags@PLT
	movq	56(%r14), %rbx
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L64:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	acpt_state
	testl	%eax, %eax
	jle	.L61
.L62:
	movq	64(%r14), %rdi
	testq	%rdi, %rdi
	je	.L64
	movl	%r13d, %edx
	movq	%r12, %rsi
	call	BIO_write@PLT
	movq	%r14, %rdi
	movl	%eax, -36(%rbp)
	call	BIO_copy_next_retry@PLT
	movl	-36(%rbp), %eax
.L61:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE275:
	.size	acpt_write, .-acpt_write
	.p2align 4
	.type	acpt_ctrl, @function
acpt_ctrl:
.LFB276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	56(%rdi), %r13
	cmpl	$12, %esi
	jle	.L145
	subl	$101, %esi
	cmpl	$31, %esi
	ja	.L110
	leaq	.L71(%rip), %rcx
	movslq	(%rcx,%rsi,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L71:
	.long	.L78-.L71
	.long	.L77-.L71
	.long	.L110-.L71
	.long	.L76-.L71
	.long	.L75-.L71
	.long	.L110-.L71
	.long	.L110-.L71
	.long	.L110-.L71
	.long	.L110-.L71
	.long	.L110-.L71
	.long	.L110-.L71
	.long	.L110-.L71
	.long	.L110-.L71
	.long	.L110-.L71
	.long	.L110-.L71
	.long	.L110-.L71
	.long	.L110-.L71
	.long	.L74-.L71
	.long	.L110-.L71
	.long	.L110-.L71
	.long	.L110-.L71
	.long	.L110-.L71
	.long	.L110-.L71
	.long	.L73-.L71
	.long	.L110-.L71
	.long	.L110-.L71
	.long	.L110-.L71
	.long	.L110-.L71
	.long	.L110-.L71
	.long	.L110-.L71
	.long	.L72-.L71
	.long	.L70-.L71
	.text
	.p2align 4,,10
	.p2align 3
.L145:
	testl	%esi, %esi
	jg	.L68
.L110:
	xorl	%eax, %eax
.L66:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	cmpl	$12, %esi
	ja	.L110
	leaq	.L80(%rip), %rcx
	movl	%esi, %esi
	movslq	(%rcx,%rsi,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L80:
	.long	.L110-.L80
	.long	.L84-.L80
	.long	.L83-.L80
	.long	.L110-.L80
	.long	.L110-.L80
	.long	.L110-.L80
	.long	.L110-.L80
	.long	.L110-.L80
	.long	.L82-.L80
	.long	.L81-.L80
	.long	.L110-.L80
	.long	.L100-.L80
	.long	.L100-.L80
	.text
.L100:
	addq	$16, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L81:
	.cfi_restore_state
	movl	%edx, 36(%rdi)
	movl	$1, %eax
	jmp	.L66
.L78:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	acpt_state
	cltq
	jmp	.L66
.L77:
	movl	12(%r13), %eax
	testq	%rdx, %rdx
	je	.L93
	orl	$8, %eax
	movl	%eax, 12(%r13)
	movl	$1, %eax
	jmp	.L66
.L76:
	movl	(%r14), %eax
	movl	%eax, 48(%r12)
	movl	%eax, 32(%r13)
	movl	$1, %eax
	movl	$5, 0(%r13)
	movl	%edx, 36(%r12)
	movl	$1, 32(%r12)
	jmp	.L66
.L75:
	movl	32(%r12), %edx
	testl	%edx, %edx
	je	.L106
	movslq	32(%r13), %rax
	testq	%r14, %r14
	je	.L66
	movl	%eax, (%r14)
	movslq	32(%r13), %rax
	jmp	.L66
.L74:
	testq	%r14, %r14
	je	.L87
	testq	%rdx, %rdx
	je	.L146
	cmpq	$1, %rdx
	je	.L147
	cmpq	$2, %rdx
	je	.L148
	cmpq	$3, %rdx
	je	.L149
	movl	$1, %eax
	cmpq	$4, %rdx
	jne	.L66
	movl	(%r14), %edx
	movl	%edx, 4(%r13)
	jmp	.L66
.L73:
	movl	32(%r12), %eax
	testl	%eax, %eax
	je	.L106
	testq	%r14, %r14
	setne	%al
	testq	%rdx, %rdx
	jne	.L95
	testb	%al, %al
	je	.L95
	movq	168(%r13), %rax
	movq	%rax, (%r14)
	movl	$1, %eax
	jmp	.L66
.L72:
	movl	%edx, 8(%r13)
	movl	$1, %eax
	jmp	.L66
.L70:
	movslq	8(%r13), %rax
	jmp	.L66
.L84:
	movl	32(%r13), %edi
	movl	$1, 0(%r13)
	cmpl	$-1, %edi
	jne	.L150
.L86:
	movq	40(%r13), %rdi
	call	BIO_ADDRINFO_free@PLT
	movq	$0, 40(%r13)
	xorl	%eax, %eax
	movl	$0, 40(%r12)
	jmp	.L66
.L83:
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L110
	addq	$16, %rsp
	movq	%r14, %rcx
	movl	$2, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_ctrl@PLT
.L82:
	.cfi_restore_state
	movslq	36(%rdi), %rax
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L93:
	andl	$-9, %eax
	movl	%eax, 12(%r13)
	movl	$1, %eax
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L150:
	movl	$2, %esi
	call	shutdown@PLT
	movl	32(%r13), %edi
	call	close@PLT
	movl	$-1, 32(%r13)
	movl	$-1, 48(%r12)
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L87:
	movl	$1, %eax
	cmpq	$2, %rdx
	jne	.L66
	andl	$-9, 8(%r13)
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L95:
	cmpq	$1, %rdx
	jne	.L96
	testb	%al, %al
	je	.L96
	movq	176(%r13), %rax
	movq	%rax, (%r14)
	movl	$1, %eax
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L96:
	cmpq	$2, %rdx
	jne	.L97
	testb	%al, %al
	je	.L97
	movq	296(%r13), %rax
	movq	%rax, (%r14)
	movl	$1, %eax
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L146:
	movq	16(%r13), %rdi
	movl	$426, %edx
	leaq	.LC0(%rip), %rsi
	movq	24(%r13), %r15
	call	CRYPTO_free@PLT
	leaq	24(%r13), %rdx
	leaq	16(%r13), %rsi
	movq	%r14, %rdi
	movq	$0, 16(%r13)
	movl	$1, %ecx
	call	BIO_parse_hostserv@PLT
	cltq
	cmpq	%r15, 24(%r13)
	je	.L89
	movl	$433, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -40(%rbp)
	call	CRYPTO_free@PLT
	movq	-40(%rbp), %rax
.L89:
	movl	$1, 32(%r12)
	jmp	.L66
.L148:
	orl	$8, 8(%r13)
	movl	$1, %eax
	jmp	.L66
.L97:
	cmpq	$3, %rdx
	jne	.L98
	testb	%al, %al
	je	.L98
	movq	304(%r13), %rax
	movq	%rax, (%r14)
	movl	$1, %eax
	jmp	.L66
.L147:
	movq	24(%r13), %rdi
	movl	$436, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$437, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_strdup@PLT
	movq	%rax, 24(%r13)
	movl	$1, %eax
	movl	$1, 32(%r12)
	jmp	.L66
.L98:
	cmpq	$4, %rdx
	jne	.L106
	movq	48(%r13), %rdi
	call	BIO_ADDRINFO_family@PLT
	movl	%eax, %edx
	movl	$4, %eax
	cmpl	$2, %edx
	je	.L66
	movl	$6, %eax
	cmpl	$10, %edx
	je	.L66
	movq	$-1, %rax
	testl	%edx, %edx
	jne	.L66
	movslq	4(%r13), %rax
	jmp	.L66
.L149:
	movq	312(%r13), %rdi
	call	BIO_free@PLT
	movq	%r14, 312(%r13)
	movl	$1, %eax
	jmp	.L66
.L106:
	movq	$-1, %rax
	jmp	.L66
	.cfi_endproc
.LFE276:
	.size	acpt_ctrl, .-acpt_ctrl
	.p2align 4
	.type	acpt_free, @function
acpt_free:
.LFB272:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L154
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	36(%rdi), %edx
	movq	%rdi, %rbx
	testl	%edx, %edx
	jne	.L163
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L163:
	.cfi_restore_state
	movq	56(%rdi), %r12
	movl	32(%r12), %edi
	cmpl	$-1, %edi
	jne	.L164
.L153:
	movq	16(%r12), %rdi
	movl	$108, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	24(%r12), %rdi
	movl	$109, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	40(%r12), %rdi
	call	BIO_ADDRINFO_free@PLT
	movq	168(%r12), %rdi
	movl	$111, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	176(%r12), %rdi
	movl	$112, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	296(%r12), %rdi
	movl	$113, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	304(%r12), %rdi
	movl	$114, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	312(%r12), %rdi
	call	BIO_free@PLT
	movq	%r12, %rdi
	movl	$116, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 56(%rbx)
	movl	$1, %eax
	movl	$0, 40(%rbx)
	movl	$0, 32(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	movl	$2, %esi
	call	shutdown@PLT
	movl	32(%r12), %edi
	call	close@PLT
	movl	$-1, 32(%r12)
	movl	$-1, 48(%rbx)
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L154:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE272:
	.size	acpt_free, .-acpt_free
	.p2align 4
	.type	acpt_puts, @function
acpt_puts:
.LFB277:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	strlen@PLT
	movl	$15, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	BIO_clear_flags@PLT
	movq	56(%r12), %r15
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L168:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	acpt_state
	movl	%eax, %r13d
	testl	%eax, %eax
	jle	.L165
.L166:
	movq	64(%r12), %rdi
	testq	%rdi, %rdi
	je	.L168
	movl	%ebx, %edx
	movq	%r14, %rsi
	call	BIO_write@PLT
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	BIO_copy_next_retry@PLT
.L165:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE277:
	.size	acpt_puts, .-acpt_puts
	.p2align 4
	.globl	BIO_s_accept
	.type	BIO_s_accept, @function
BIO_s_accept:
.LFB267:
	.cfi_startproc
	endbr64
	leaq	methods_acceptp(%rip), %rax
	ret
	.cfi_endproc
.LFE267:
	.size	BIO_s_accept, .-BIO_s_accept
	.p2align 4
	.globl	BIO_new_accept
	.type	BIO_new_accept, @function
BIO_new_accept:
.LFB278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	leaq	methods_acceptp(%rip), %rdi
	pushq	%r12
	.cfi_offset 12, -32
	call	BIO_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L171
	xorl	%edx, %edx
	movq	%r13, %rcx
	movl	$118, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	testq	%rax, %rax
	je	.L177
.L171:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	BIO_free@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE278:
	.size	BIO_new_accept, .-BIO_new_accept
	.section	.rodata.str1.1
.LC3:
	.string	"socket accept"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	methods_acceptp, @object
	.size	methods_acceptp, 96
methods_acceptp:
	.long	1293
	.zero	4
	.quad	.LC3
	.quad	bwrite_conv
	.quad	acpt_write
	.quad	bread_conv
	.quad	acpt_read
	.quad	acpt_puts
	.quad	0
	.quad	acpt_ctrl
	.quad	acpt_new
	.quad	acpt_free
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
