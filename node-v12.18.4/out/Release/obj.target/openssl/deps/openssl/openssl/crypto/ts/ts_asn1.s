	.file	"ts_asn1.c"
	.text
	.p2align 4
	.globl	d2i_TS_MSG_IMPRINT
	.type	d2i_TS_MSG_IMPRINT, @function
d2i_TS_MSG_IMPRINT:
.LFB1344:
	.cfi_startproc
	endbr64
	leaq	TS_MSG_IMPRINT_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1344:
	.size	d2i_TS_MSG_IMPRINT, .-d2i_TS_MSG_IMPRINT
	.p2align 4
	.globl	d2i_TS_REQ
	.type	d2i_TS_REQ, @function
d2i_TS_REQ:
.LFB1353:
	.cfi_startproc
	endbr64
	leaq	TS_REQ_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1353:
	.size	d2i_TS_REQ, .-d2i_TS_REQ
	.p2align 4
	.globl	d2i_TS_TST_INFO
	.type	d2i_TS_TST_INFO, @function
d2i_TS_TST_INFO:
.LFB1367:
	.cfi_startproc
	endbr64
	leaq	TS_TST_INFO_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1367:
	.size	d2i_TS_TST_INFO, .-d2i_TS_TST_INFO
	.p2align 4
	.globl	d2i_TS_RESP
	.type	d2i_TS_RESP, @function
d2i_TS_RESP:
.LFB1383:
	.cfi_startproc
	endbr64
	leaq	TS_RESP_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1383:
	.size	d2i_TS_RESP, .-d2i_TS_RESP
	.p2align 4
	.globl	i2d_TS_MSG_IMPRINT
	.type	i2d_TS_MSG_IMPRINT, @function
i2d_TS_MSG_IMPRINT:
.LFB1345:
	.cfi_startproc
	endbr64
	leaq	TS_MSG_IMPRINT_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1345:
	.size	i2d_TS_MSG_IMPRINT, .-i2d_TS_MSG_IMPRINT
	.p2align 4
	.globl	i2d_TS_REQ
	.type	i2d_TS_REQ, @function
i2d_TS_REQ:
.LFB1354:
	.cfi_startproc
	endbr64
	leaq	TS_REQ_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1354:
	.size	i2d_TS_REQ, .-i2d_TS_REQ
	.p2align 4
	.globl	i2d_TS_TST_INFO
	.type	i2d_TS_TST_INFO, @function
i2d_TS_TST_INFO:
.LFB1368:
	.cfi_startproc
	endbr64
	leaq	TS_TST_INFO_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1368:
	.size	i2d_TS_TST_INFO, .-i2d_TS_TST_INFO
	.p2align 4
	.globl	i2d_TS_RESP
	.type	i2d_TS_RESP, @function
i2d_TS_RESP:
.LFB1384:
	.cfi_startproc
	endbr64
	leaq	TS_RESP_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1384:
	.size	i2d_TS_RESP, .-i2d_TS_RESP
	.p2align 4
	.globl	TS_MSG_IMPRINT_new
	.type	TS_MSG_IMPRINT_new, @function
TS_MSG_IMPRINT_new:
.LFB1346:
	.cfi_startproc
	endbr64
	leaq	TS_MSG_IMPRINT_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1346:
	.size	TS_MSG_IMPRINT_new, .-TS_MSG_IMPRINT_new
	.p2align 4
	.globl	TS_REQ_new
	.type	TS_REQ_new, @function
TS_REQ_new:
.LFB1355:
	.cfi_startproc
	endbr64
	leaq	TS_REQ_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1355:
	.size	TS_REQ_new, .-TS_REQ_new
	.p2align 4
	.globl	TS_TST_INFO_new
	.type	TS_TST_INFO_new, @function
TS_TST_INFO_new:
.LFB1369:
	.cfi_startproc
	endbr64
	leaq	TS_TST_INFO_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1369:
	.size	TS_TST_INFO_new, .-TS_TST_INFO_new
	.p2align 4
	.globl	TS_RESP_new
	.type	TS_RESP_new, @function
TS_RESP_new:
.LFB1385:
	.cfi_startproc
	endbr64
	leaq	TS_RESP_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1385:
	.size	TS_RESP_new, .-TS_RESP_new
	.p2align 4
	.globl	TS_MSG_IMPRINT_free
	.type	TS_MSG_IMPRINT_free, @function
TS_MSG_IMPRINT_free:
.LFB1347:
	.cfi_startproc
	endbr64
	leaq	TS_MSG_IMPRINT_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1347:
	.size	TS_MSG_IMPRINT_free, .-TS_MSG_IMPRINT_free
	.p2align 4
	.globl	TS_MSG_IMPRINT_dup
	.type	TS_MSG_IMPRINT_dup, @function
TS_MSG_IMPRINT_dup:
.LFB1348:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	leaq	TS_MSG_IMPRINT_it(%rip), %rdi
	jmp	ASN1_item_dup@PLT
	.cfi_endproc
.LFE1348:
	.size	TS_MSG_IMPRINT_dup, .-TS_MSG_IMPRINT_dup
	.p2align 4
	.globl	d2i_TS_MSG_IMPRINT_bio
	.type	d2i_TS_MSG_IMPRINT_bio, @function
d2i_TS_MSG_IMPRINT_bio:
.LFB1349:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	movq	%rsi, %rcx
	leaq	TS_MSG_IMPRINT_new(%rip), %rdi
	leaq	d2i_TS_MSG_IMPRINT(%rip), %rsi
	jmp	ASN1_d2i_bio@PLT
	.cfi_endproc
.LFE1349:
	.size	d2i_TS_MSG_IMPRINT_bio, .-d2i_TS_MSG_IMPRINT_bio
	.p2align 4
	.globl	i2d_TS_MSG_IMPRINT_bio
	.type	i2d_TS_MSG_IMPRINT_bio, @function
i2d_TS_MSG_IMPRINT_bio:
.LFB1350:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	i2d_TS_MSG_IMPRINT(%rip), %rdi
	jmp	ASN1_i2d_bio@PLT
	.cfi_endproc
.LFE1350:
	.size	i2d_TS_MSG_IMPRINT_bio, .-i2d_TS_MSG_IMPRINT_bio
	.p2align 4
	.globl	d2i_TS_MSG_IMPRINT_fp
	.type	d2i_TS_MSG_IMPRINT_fp, @function
d2i_TS_MSG_IMPRINT_fp:
.LFB1351:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	movq	%rsi, %rcx
	leaq	TS_MSG_IMPRINT_new(%rip), %rdi
	leaq	d2i_TS_MSG_IMPRINT(%rip), %rsi
	jmp	ASN1_d2i_fp@PLT
	.cfi_endproc
.LFE1351:
	.size	d2i_TS_MSG_IMPRINT_fp, .-d2i_TS_MSG_IMPRINT_fp
	.p2align 4
	.globl	i2d_TS_MSG_IMPRINT_fp
	.type	i2d_TS_MSG_IMPRINT_fp, @function
i2d_TS_MSG_IMPRINT_fp:
.LFB1352:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	i2d_TS_MSG_IMPRINT(%rip), %rdi
	jmp	ASN1_i2d_fp@PLT
	.cfi_endproc
.LFE1352:
	.size	i2d_TS_MSG_IMPRINT_fp, .-i2d_TS_MSG_IMPRINT_fp
	.p2align 4
	.globl	TS_REQ_free
	.type	TS_REQ_free, @function
TS_REQ_free:
.LFB1356:
	.cfi_startproc
	endbr64
	leaq	TS_REQ_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1356:
	.size	TS_REQ_free, .-TS_REQ_free
	.p2align 4
	.globl	TS_REQ_dup
	.type	TS_REQ_dup, @function
TS_REQ_dup:
.LFB1357:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	leaq	TS_REQ_it(%rip), %rdi
	jmp	ASN1_item_dup@PLT
	.cfi_endproc
.LFE1357:
	.size	TS_REQ_dup, .-TS_REQ_dup
	.p2align 4
	.globl	d2i_TS_REQ_bio
	.type	d2i_TS_REQ_bio, @function
d2i_TS_REQ_bio:
.LFB1358:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	movq	%rsi, %rcx
	leaq	TS_REQ_new(%rip), %rdi
	leaq	d2i_TS_REQ(%rip), %rsi
	jmp	ASN1_d2i_bio@PLT
	.cfi_endproc
.LFE1358:
	.size	d2i_TS_REQ_bio, .-d2i_TS_REQ_bio
	.p2align 4
	.globl	i2d_TS_REQ_bio
	.type	i2d_TS_REQ_bio, @function
i2d_TS_REQ_bio:
.LFB1359:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	i2d_TS_REQ(%rip), %rdi
	jmp	ASN1_i2d_bio@PLT
	.cfi_endproc
.LFE1359:
	.size	i2d_TS_REQ_bio, .-i2d_TS_REQ_bio
	.p2align 4
	.globl	d2i_TS_REQ_fp
	.type	d2i_TS_REQ_fp, @function
d2i_TS_REQ_fp:
.LFB1360:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	movq	%rsi, %rcx
	leaq	TS_REQ_new(%rip), %rdi
	leaq	d2i_TS_REQ(%rip), %rsi
	jmp	ASN1_d2i_fp@PLT
	.cfi_endproc
.LFE1360:
	.size	d2i_TS_REQ_fp, .-d2i_TS_REQ_fp
	.p2align 4
	.globl	i2d_TS_REQ_fp
	.type	i2d_TS_REQ_fp, @function
i2d_TS_REQ_fp:
.LFB1361:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	i2d_TS_REQ(%rip), %rdi
	jmp	ASN1_i2d_fp@PLT
	.cfi_endproc
.LFE1361:
	.size	i2d_TS_REQ_fp, .-i2d_TS_REQ_fp
	.p2align 4
	.globl	d2i_TS_ACCURACY
	.type	d2i_TS_ACCURACY, @function
d2i_TS_ACCURACY:
.LFB1362:
	.cfi_startproc
	endbr64
	leaq	TS_ACCURACY_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1362:
	.size	d2i_TS_ACCURACY, .-d2i_TS_ACCURACY
	.p2align 4
	.globl	i2d_TS_ACCURACY
	.type	i2d_TS_ACCURACY, @function
i2d_TS_ACCURACY:
.LFB1363:
	.cfi_startproc
	endbr64
	leaq	TS_ACCURACY_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1363:
	.size	i2d_TS_ACCURACY, .-i2d_TS_ACCURACY
	.p2align 4
	.globl	TS_ACCURACY_new
	.type	TS_ACCURACY_new, @function
TS_ACCURACY_new:
.LFB1364:
	.cfi_startproc
	endbr64
	leaq	TS_ACCURACY_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1364:
	.size	TS_ACCURACY_new, .-TS_ACCURACY_new
	.p2align 4
	.globl	TS_ACCURACY_free
	.type	TS_ACCURACY_free, @function
TS_ACCURACY_free:
.LFB1365:
	.cfi_startproc
	endbr64
	leaq	TS_ACCURACY_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1365:
	.size	TS_ACCURACY_free, .-TS_ACCURACY_free
	.p2align 4
	.globl	TS_ACCURACY_dup
	.type	TS_ACCURACY_dup, @function
TS_ACCURACY_dup:
.LFB1366:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	leaq	TS_ACCURACY_it(%rip), %rdi
	jmp	ASN1_item_dup@PLT
	.cfi_endproc
.LFE1366:
	.size	TS_ACCURACY_dup, .-TS_ACCURACY_dup
	.p2align 4
	.globl	TS_TST_INFO_free
	.type	TS_TST_INFO_free, @function
TS_TST_INFO_free:
.LFB1370:
	.cfi_startproc
	endbr64
	leaq	TS_TST_INFO_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1370:
	.size	TS_TST_INFO_free, .-TS_TST_INFO_free
	.p2align 4
	.globl	TS_TST_INFO_dup
	.type	TS_TST_INFO_dup, @function
TS_TST_INFO_dup:
.LFB1371:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	leaq	TS_TST_INFO_it(%rip), %rdi
	jmp	ASN1_item_dup@PLT
	.cfi_endproc
.LFE1371:
	.size	TS_TST_INFO_dup, .-TS_TST_INFO_dup
	.p2align 4
	.globl	d2i_TS_TST_INFO_bio
	.type	d2i_TS_TST_INFO_bio, @function
d2i_TS_TST_INFO_bio:
.LFB1372:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	movq	%rsi, %rcx
	leaq	TS_TST_INFO_new(%rip), %rdi
	leaq	d2i_TS_TST_INFO(%rip), %rsi
	jmp	ASN1_d2i_bio@PLT
	.cfi_endproc
.LFE1372:
	.size	d2i_TS_TST_INFO_bio, .-d2i_TS_TST_INFO_bio
	.p2align 4
	.globl	i2d_TS_TST_INFO_bio
	.type	i2d_TS_TST_INFO_bio, @function
i2d_TS_TST_INFO_bio:
.LFB1373:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	i2d_TS_TST_INFO(%rip), %rdi
	jmp	ASN1_i2d_bio@PLT
	.cfi_endproc
.LFE1373:
	.size	i2d_TS_TST_INFO_bio, .-i2d_TS_TST_INFO_bio
	.p2align 4
	.globl	d2i_TS_TST_INFO_fp
	.type	d2i_TS_TST_INFO_fp, @function
d2i_TS_TST_INFO_fp:
.LFB1374:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	movq	%rsi, %rcx
	leaq	TS_TST_INFO_new(%rip), %rdi
	leaq	d2i_TS_TST_INFO(%rip), %rsi
	jmp	ASN1_d2i_fp@PLT
	.cfi_endproc
.LFE1374:
	.size	d2i_TS_TST_INFO_fp, .-d2i_TS_TST_INFO_fp
	.p2align 4
	.globl	i2d_TS_TST_INFO_fp
	.type	i2d_TS_TST_INFO_fp, @function
i2d_TS_TST_INFO_fp:
.LFB1375:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	i2d_TS_TST_INFO(%rip), %rdi
	jmp	ASN1_i2d_fp@PLT
	.cfi_endproc
.LFE1375:
	.size	i2d_TS_TST_INFO_fp, .-i2d_TS_TST_INFO_fp
	.p2align 4
	.globl	d2i_TS_STATUS_INFO
	.type	d2i_TS_STATUS_INFO, @function
d2i_TS_STATUS_INFO:
.LFB1376:
	.cfi_startproc
	endbr64
	leaq	TS_STATUS_INFO_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1376:
	.size	d2i_TS_STATUS_INFO, .-d2i_TS_STATUS_INFO
	.p2align 4
	.globl	i2d_TS_STATUS_INFO
	.type	i2d_TS_STATUS_INFO, @function
i2d_TS_STATUS_INFO:
.LFB1377:
	.cfi_startproc
	endbr64
	leaq	TS_STATUS_INFO_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1377:
	.size	i2d_TS_STATUS_INFO, .-i2d_TS_STATUS_INFO
	.p2align 4
	.globl	TS_STATUS_INFO_new
	.type	TS_STATUS_INFO_new, @function
TS_STATUS_INFO_new:
.LFB1378:
	.cfi_startproc
	endbr64
	leaq	TS_STATUS_INFO_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1378:
	.size	TS_STATUS_INFO_new, .-TS_STATUS_INFO_new
	.p2align 4
	.globl	TS_STATUS_INFO_free
	.type	TS_STATUS_INFO_free, @function
TS_STATUS_INFO_free:
.LFB1379:
	.cfi_startproc
	endbr64
	leaq	TS_STATUS_INFO_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1379:
	.size	TS_STATUS_INFO_free, .-TS_STATUS_INFO_free
	.p2align 4
	.globl	TS_STATUS_INFO_dup
	.type	TS_STATUS_INFO_dup, @function
TS_STATUS_INFO_dup:
.LFB1380:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	leaq	TS_STATUS_INFO_it(%rip), %rdi
	jmp	ASN1_item_dup@PLT
	.cfi_endproc
.LFE1380:
	.size	TS_STATUS_INFO_dup, .-TS_STATUS_INFO_dup
	.p2align 4
	.globl	TS_RESP_free
	.type	TS_RESP_free, @function
TS_RESP_free:
.LFB1386:
	.cfi_startproc
	endbr64
	leaq	TS_RESP_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1386:
	.size	TS_RESP_free, .-TS_RESP_free
	.p2align 4
	.globl	TS_RESP_dup
	.type	TS_RESP_dup, @function
TS_RESP_dup:
.LFB1387:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	leaq	TS_RESP_it(%rip), %rdi
	jmp	ASN1_item_dup@PLT
	.cfi_endproc
.LFE1387:
	.size	TS_RESP_dup, .-TS_RESP_dup
	.p2align 4
	.globl	d2i_TS_RESP_bio
	.type	d2i_TS_RESP_bio, @function
d2i_TS_RESP_bio:
.LFB1388:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	movq	%rsi, %rcx
	leaq	TS_RESP_new(%rip), %rdi
	leaq	d2i_TS_RESP(%rip), %rsi
	jmp	ASN1_d2i_bio@PLT
	.cfi_endproc
.LFE1388:
	.size	d2i_TS_RESP_bio, .-d2i_TS_RESP_bio
	.p2align 4
	.globl	i2d_TS_RESP_bio
	.type	i2d_TS_RESP_bio, @function
i2d_TS_RESP_bio:
.LFB1389:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	i2d_TS_RESP(%rip), %rdi
	jmp	ASN1_i2d_bio@PLT
	.cfi_endproc
.LFE1389:
	.size	i2d_TS_RESP_bio, .-i2d_TS_RESP_bio
	.p2align 4
	.globl	d2i_TS_RESP_fp
	.type	d2i_TS_RESP_fp, @function
d2i_TS_RESP_fp:
.LFB1390:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	movq	%rsi, %rcx
	leaq	TS_RESP_new(%rip), %rdi
	leaq	d2i_TS_RESP(%rip), %rsi
	jmp	ASN1_d2i_fp@PLT
	.cfi_endproc
.LFE1390:
	.size	d2i_TS_RESP_fp, .-d2i_TS_RESP_fp
	.p2align 4
	.globl	i2d_TS_RESP_fp
	.type	i2d_TS_RESP_fp, @function
i2d_TS_RESP_fp:
.LFB1391:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	i2d_TS_RESP(%rip), %rdi
	jmp	ASN1_i2d_fp@PLT
	.cfi_endproc
.LFE1391:
	.size	i2d_TS_RESP_fp, .-i2d_TS_RESP_fp
	.p2align 4
	.globl	d2i_ESS_ISSUER_SERIAL
	.type	d2i_ESS_ISSUER_SERIAL, @function
d2i_ESS_ISSUER_SERIAL:
.LFB1392:
	.cfi_startproc
	endbr64
	leaq	ESS_ISSUER_SERIAL_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1392:
	.size	d2i_ESS_ISSUER_SERIAL, .-d2i_ESS_ISSUER_SERIAL
	.p2align 4
	.globl	i2d_ESS_ISSUER_SERIAL
	.type	i2d_ESS_ISSUER_SERIAL, @function
i2d_ESS_ISSUER_SERIAL:
.LFB1393:
	.cfi_startproc
	endbr64
	leaq	ESS_ISSUER_SERIAL_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1393:
	.size	i2d_ESS_ISSUER_SERIAL, .-i2d_ESS_ISSUER_SERIAL
	.p2align 4
	.globl	ESS_ISSUER_SERIAL_new
	.type	ESS_ISSUER_SERIAL_new, @function
ESS_ISSUER_SERIAL_new:
.LFB1394:
	.cfi_startproc
	endbr64
	leaq	ESS_ISSUER_SERIAL_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1394:
	.size	ESS_ISSUER_SERIAL_new, .-ESS_ISSUER_SERIAL_new
	.p2align 4
	.globl	ESS_ISSUER_SERIAL_free
	.type	ESS_ISSUER_SERIAL_free, @function
ESS_ISSUER_SERIAL_free:
.LFB1395:
	.cfi_startproc
	endbr64
	leaq	ESS_ISSUER_SERIAL_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1395:
	.size	ESS_ISSUER_SERIAL_free, .-ESS_ISSUER_SERIAL_free
	.p2align 4
	.globl	ESS_ISSUER_SERIAL_dup
	.type	ESS_ISSUER_SERIAL_dup, @function
ESS_ISSUER_SERIAL_dup:
.LFB1396:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	leaq	ESS_ISSUER_SERIAL_it(%rip), %rdi
	jmp	ASN1_item_dup@PLT
	.cfi_endproc
.LFE1396:
	.size	ESS_ISSUER_SERIAL_dup, .-ESS_ISSUER_SERIAL_dup
	.p2align 4
	.globl	d2i_ESS_CERT_ID
	.type	d2i_ESS_CERT_ID, @function
d2i_ESS_CERT_ID:
.LFB1397:
	.cfi_startproc
	endbr64
	leaq	ESS_CERT_ID_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1397:
	.size	d2i_ESS_CERT_ID, .-d2i_ESS_CERT_ID
	.p2align 4
	.globl	i2d_ESS_CERT_ID
	.type	i2d_ESS_CERT_ID, @function
i2d_ESS_CERT_ID:
.LFB1398:
	.cfi_startproc
	endbr64
	leaq	ESS_CERT_ID_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1398:
	.size	i2d_ESS_CERT_ID, .-i2d_ESS_CERT_ID
	.p2align 4
	.globl	ESS_CERT_ID_new
	.type	ESS_CERT_ID_new, @function
ESS_CERT_ID_new:
.LFB1399:
	.cfi_startproc
	endbr64
	leaq	ESS_CERT_ID_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1399:
	.size	ESS_CERT_ID_new, .-ESS_CERT_ID_new
	.p2align 4
	.globl	ESS_CERT_ID_free
	.type	ESS_CERT_ID_free, @function
ESS_CERT_ID_free:
.LFB1400:
	.cfi_startproc
	endbr64
	leaq	ESS_CERT_ID_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1400:
	.size	ESS_CERT_ID_free, .-ESS_CERT_ID_free
	.p2align 4
	.globl	ESS_CERT_ID_dup
	.type	ESS_CERT_ID_dup, @function
ESS_CERT_ID_dup:
.LFB1401:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	leaq	ESS_CERT_ID_it(%rip), %rdi
	jmp	ASN1_item_dup@PLT
	.cfi_endproc
.LFE1401:
	.size	ESS_CERT_ID_dup, .-ESS_CERT_ID_dup
	.p2align 4
	.globl	d2i_ESS_SIGNING_CERT
	.type	d2i_ESS_SIGNING_CERT, @function
d2i_ESS_SIGNING_CERT:
.LFB1402:
	.cfi_startproc
	endbr64
	leaq	ESS_SIGNING_CERT_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1402:
	.size	d2i_ESS_SIGNING_CERT, .-d2i_ESS_SIGNING_CERT
	.p2align 4
	.globl	i2d_ESS_SIGNING_CERT
	.type	i2d_ESS_SIGNING_CERT, @function
i2d_ESS_SIGNING_CERT:
.LFB1403:
	.cfi_startproc
	endbr64
	leaq	ESS_SIGNING_CERT_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1403:
	.size	i2d_ESS_SIGNING_CERT, .-i2d_ESS_SIGNING_CERT
	.p2align 4
	.globl	ESS_SIGNING_CERT_new
	.type	ESS_SIGNING_CERT_new, @function
ESS_SIGNING_CERT_new:
.LFB1404:
	.cfi_startproc
	endbr64
	leaq	ESS_SIGNING_CERT_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1404:
	.size	ESS_SIGNING_CERT_new, .-ESS_SIGNING_CERT_new
	.p2align 4
	.globl	ESS_SIGNING_CERT_free
	.type	ESS_SIGNING_CERT_free, @function
ESS_SIGNING_CERT_free:
.LFB1405:
	.cfi_startproc
	endbr64
	leaq	ESS_SIGNING_CERT_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1405:
	.size	ESS_SIGNING_CERT_free, .-ESS_SIGNING_CERT_free
	.p2align 4
	.globl	ESS_SIGNING_CERT_dup
	.type	ESS_SIGNING_CERT_dup, @function
ESS_SIGNING_CERT_dup:
.LFB1406:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	leaq	ESS_SIGNING_CERT_it(%rip), %rdi
	jmp	ASN1_item_dup@PLT
	.cfi_endproc
.LFE1406:
	.size	ESS_SIGNING_CERT_dup, .-ESS_SIGNING_CERT_dup
	.p2align 4
	.globl	d2i_ESS_CERT_ID_V2
	.type	d2i_ESS_CERT_ID_V2, @function
d2i_ESS_CERT_ID_V2:
.LFB1407:
	.cfi_startproc
	endbr64
	leaq	ESS_CERT_ID_V2_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1407:
	.size	d2i_ESS_CERT_ID_V2, .-d2i_ESS_CERT_ID_V2
	.p2align 4
	.globl	i2d_ESS_CERT_ID_V2
	.type	i2d_ESS_CERT_ID_V2, @function
i2d_ESS_CERT_ID_V2:
.LFB1408:
	.cfi_startproc
	endbr64
	leaq	ESS_CERT_ID_V2_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1408:
	.size	i2d_ESS_CERT_ID_V2, .-i2d_ESS_CERT_ID_V2
	.p2align 4
	.globl	ESS_CERT_ID_V2_new
	.type	ESS_CERT_ID_V2_new, @function
ESS_CERT_ID_V2_new:
.LFB1409:
	.cfi_startproc
	endbr64
	leaq	ESS_CERT_ID_V2_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1409:
	.size	ESS_CERT_ID_V2_new, .-ESS_CERT_ID_V2_new
	.p2align 4
	.globl	ESS_CERT_ID_V2_free
	.type	ESS_CERT_ID_V2_free, @function
ESS_CERT_ID_V2_free:
.LFB1410:
	.cfi_startproc
	endbr64
	leaq	ESS_CERT_ID_V2_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1410:
	.size	ESS_CERT_ID_V2_free, .-ESS_CERT_ID_V2_free
	.p2align 4
	.globl	ESS_CERT_ID_V2_dup
	.type	ESS_CERT_ID_V2_dup, @function
ESS_CERT_ID_V2_dup:
.LFB1411:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	leaq	ESS_CERT_ID_V2_it(%rip), %rdi
	jmp	ASN1_item_dup@PLT
	.cfi_endproc
.LFE1411:
	.size	ESS_CERT_ID_V2_dup, .-ESS_CERT_ID_V2_dup
	.p2align 4
	.globl	d2i_ESS_SIGNING_CERT_V2
	.type	d2i_ESS_SIGNING_CERT_V2, @function
d2i_ESS_SIGNING_CERT_V2:
.LFB1412:
	.cfi_startproc
	endbr64
	leaq	ESS_SIGNING_CERT_V2_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1412:
	.size	d2i_ESS_SIGNING_CERT_V2, .-d2i_ESS_SIGNING_CERT_V2
	.p2align 4
	.globl	i2d_ESS_SIGNING_CERT_V2
	.type	i2d_ESS_SIGNING_CERT_V2, @function
i2d_ESS_SIGNING_CERT_V2:
.LFB1413:
	.cfi_startproc
	endbr64
	leaq	ESS_SIGNING_CERT_V2_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1413:
	.size	i2d_ESS_SIGNING_CERT_V2, .-i2d_ESS_SIGNING_CERT_V2
	.p2align 4
	.globl	ESS_SIGNING_CERT_V2_new
	.type	ESS_SIGNING_CERT_V2_new, @function
ESS_SIGNING_CERT_V2_new:
.LFB1414:
	.cfi_startproc
	endbr64
	leaq	ESS_SIGNING_CERT_V2_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1414:
	.size	ESS_SIGNING_CERT_V2_new, .-ESS_SIGNING_CERT_V2_new
	.p2align 4
	.globl	ESS_SIGNING_CERT_V2_free
	.type	ESS_SIGNING_CERT_V2_free, @function
ESS_SIGNING_CERT_V2_free:
.LFB1415:
	.cfi_startproc
	endbr64
	leaq	ESS_SIGNING_CERT_V2_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1415:
	.size	ESS_SIGNING_CERT_V2_free, .-ESS_SIGNING_CERT_V2_free
	.p2align 4
	.globl	ESS_SIGNING_CERT_V2_dup
	.type	ESS_SIGNING_CERT_V2_dup, @function
ESS_SIGNING_CERT_V2_dup:
.LFB1416:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	leaq	ESS_SIGNING_CERT_V2_it(%rip), %rdi
	jmp	ASN1_item_dup@PLT
	.cfi_endproc
.LFE1416:
	.size	ESS_SIGNING_CERT_V2_dup, .-ESS_SIGNING_CERT_V2_dup
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ts/ts_asn1.c"
	.text
	.p2align 4
	.globl	PKCS7_to_TS_TST_INFO
	.type	PKCS7_to_TS_TST_INFO, @function
PKCS7_to_TS_TST_INFO:
.LFB1417:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	OBJ_obj2nid@PLT
	movl	$255, %r8d
	cmpl	$22, %eax
	jne	.L82
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$2, %esi
	movq	%rbx, %rdi
	call	PKCS7_ctrl@PLT
	testq	%rax, %rax
	jne	.L83
	movq	32(%rbx), %rax
	movq	40(%rax), %rbx
	movq	24(%rbx), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$207, %eax
	jne	.L84
	movq	32(%rbx), %rax
	cmpl	$4, (%rax)
	jne	.L85
	movq	8(%rax), %rax
	leaq	-32(%rbp), %rsi
	leaq	TS_TST_INFO_it(%rip), %rcx
	xorl	%edi, %edi
	movq	8(%rax), %rdx
	movq	%rdx, -32(%rbp)
	movslq	(%rax), %rdx
	call	ASN1_item_d2i@PLT
.L73:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L86
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	movl	$265, %r8d
.L82:
	movl	$132, %edx
	movl	$148, %esi
	movl	$47, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L85:
	movl	$270, %r8d
	movl	$133, %edx
	movl	$148, %esi
	movl	$47, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L83:
	movl	$259, %r8d
	movl	$134, %edx
	movl	$148, %esi
	movl	$47, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L73
.L86:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1417:
	.size	PKCS7_to_TS_TST_INFO, .-PKCS7_to_TS_TST_INFO
	.p2align 4
	.type	ts_resp_cb, @function
ts_resp_cb:
.LFB1382:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rsi), %rbx
	cmpl	$1, %edi
	je	.L96
	cmpl	$3, %edi
	je	.L97
	cmpl	$5, %edi
	je	.L91
.L94:
	movl	$1, %eax
.L87:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	movq	$0, 16(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	(%rax), %rdi
	call	ASN1_INTEGER_get@PLT
	cmpq	$0, 8(%rbx)
	je	.L92
	cmpq	$1, %rax
	ja	.L98
	movq	16(%rbx), %rdi
	leaq	TS_TST_INFO_it(%rip), %rsi
	call	ASN1_item_free@PLT
	movq	8(%rbx), %rdi
	call	PKCS7_to_TS_TST_INFO
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	jne	.L94
	movl	$147, %r8d
	movl	$129, %edx
	movl	$150, %esi
	movl	$47, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L97:
	movq	16(%rbx), %rdi
	leaq	TS_TST_INFO_it(%rip), %rsi
	call	ASN1_item_free@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	movl	$141, %r8d
	movl	$131, %edx
	movl	$150, %esi
	movl	$47, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L92:
	cmpq	$1, %rax
	ja	.L94
	movl	$152, %r8d
	movl	$130, %edx
	movl	$150, %esi
	movl	$47, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L87
	.cfi_endproc
.LFE1382:
	.size	ts_resp_cb, .-ts_resp_cb
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"ESS_SIGNING_CERT_V2"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	ESS_SIGNING_CERT_V2_it, @object
	.size	ESS_SIGNING_CERT_V2_it, 56
ESS_SIGNING_CERT_V2_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	ESS_SIGNING_CERT_V2_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC1
	.section	.rodata.str1.1
.LC2:
	.string	"cert_ids"
.LC3:
	.string	"policy_info"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	ESS_SIGNING_CERT_V2_seq_tt, @object
	.size	ESS_SIGNING_CERT_V2_seq_tt, 80
ESS_SIGNING_CERT_V2_seq_tt:
	.quad	4
	.quad	0
	.quad	0
	.quad	.LC2
	.quad	ESS_CERT_ID_V2_it
	.quad	5
	.quad	0
	.quad	8
	.quad	.LC3
	.quad	POLICYINFO_it
	.section	.rodata.str1.1
.LC4:
	.string	"ESS_CERT_ID_V2"
	.section	.data.rel.ro.local
	.align 32
	.type	ESS_CERT_ID_V2_it, @object
	.size	ESS_CERT_ID_V2_it, 56
ESS_CERT_ID_V2_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	ESS_CERT_ID_V2_seq_tt
	.quad	3
	.quad	0
	.quad	24
	.quad	.LC4
	.section	.rodata.str1.1
.LC5:
	.string	"hash_alg"
.LC6:
	.string	"hash"
.LC7:
	.string	"issuer_serial"
	.section	.data.rel.ro
	.align 32
	.type	ESS_CERT_ID_V2_seq_tt, @object
	.size	ESS_CERT_ID_V2_seq_tt, 120
ESS_CERT_ID_V2_seq_tt:
	.quad	1
	.quad	0
	.quad	0
	.quad	.LC5
	.quad	X509_ALGOR_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC6
	.quad	ASN1_OCTET_STRING_it
	.quad	1
	.quad	0
	.quad	16
	.quad	.LC7
	.quad	ESS_ISSUER_SERIAL_it
	.section	.rodata.str1.1
.LC8:
	.string	"ESS_SIGNING_CERT"
	.section	.data.rel.ro.local
	.align 32
	.type	ESS_SIGNING_CERT_it, @object
	.size	ESS_SIGNING_CERT_it, 56
ESS_SIGNING_CERT_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	ESS_SIGNING_CERT_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC8
	.section	.data.rel.ro
	.align 32
	.type	ESS_SIGNING_CERT_seq_tt, @object
	.size	ESS_SIGNING_CERT_seq_tt, 80
ESS_SIGNING_CERT_seq_tt:
	.quad	4
	.quad	0
	.quad	0
	.quad	.LC2
	.quad	ESS_CERT_ID_it
	.quad	5
	.quad	0
	.quad	8
	.quad	.LC3
	.quad	POLICYINFO_it
	.section	.rodata.str1.1
.LC9:
	.string	"ESS_CERT_ID"
	.section	.data.rel.ro.local
	.align 32
	.type	ESS_CERT_ID_it, @object
	.size	ESS_CERT_ID_it, 56
ESS_CERT_ID_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	ESS_CERT_ID_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC9
	.section	.data.rel.ro
	.align 32
	.type	ESS_CERT_ID_seq_tt, @object
	.size	ESS_CERT_ID_seq_tt, 80
ESS_CERT_ID_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC6
	.quad	ASN1_OCTET_STRING_it
	.quad	1
	.quad	0
	.quad	8
	.quad	.LC7
	.quad	ESS_ISSUER_SERIAL_it
	.section	.rodata.str1.1
.LC10:
	.string	"ESS_ISSUER_SERIAL"
	.section	.data.rel.ro.local
	.align 32
	.type	ESS_ISSUER_SERIAL_it, @object
	.size	ESS_ISSUER_SERIAL_it, 56
ESS_ISSUER_SERIAL_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	ESS_ISSUER_SERIAL_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC10
	.section	.rodata.str1.1
.LC11:
	.string	"issuer"
.LC12:
	.string	"serial"
	.section	.data.rel.ro
	.align 32
	.type	ESS_ISSUER_SERIAL_seq_tt, @object
	.size	ESS_ISSUER_SERIAL_seq_tt, 80
ESS_ISSUER_SERIAL_seq_tt:
	.quad	4
	.quad	0
	.quad	0
	.quad	.LC11
	.quad	GENERAL_NAME_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC12
	.quad	ASN1_INTEGER_it
	.section	.rodata.str1.1
.LC13:
	.string	"TS_RESP"
	.section	.data.rel.ro.local
	.align 32
	.type	TS_RESP_it, @object
	.size	TS_RESP_it, 56
TS_RESP_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	TS_RESP_seq_tt
	.quad	2
	.quad	TS_RESP_aux
	.quad	24
	.quad	.LC13
	.section	.rodata.str1.1
.LC14:
	.string	"status_info"
.LC15:
	.string	"token"
	.section	.data.rel.ro
	.align 32
	.type	TS_RESP_seq_tt, @object
	.size	TS_RESP_seq_tt, 80
TS_RESP_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC14
	.quad	TS_STATUS_INFO_it
	.quad	1
	.quad	0
	.quad	8
	.quad	.LC15
	.quad	PKCS7_it
	.section	.data.rel.ro.local
	.align 32
	.type	TS_RESP_aux, @object
	.size	TS_RESP_aux, 40
TS_RESP_aux:
	.quad	0
	.long	0
	.long	0
	.long	0
	.zero	4
	.quad	ts_resp_cb
	.long	0
	.zero	4
	.section	.rodata.str1.1
.LC16:
	.string	"TS_STATUS_INFO"
	.section	.data.rel.ro.local
	.align 32
	.type	TS_STATUS_INFO_it, @object
	.size	TS_STATUS_INFO_it, 56
TS_STATUS_INFO_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	TS_STATUS_INFO_seq_tt
	.quad	3
	.quad	0
	.quad	24
	.quad	.LC16
	.section	.rodata.str1.1
.LC17:
	.string	"status"
.LC18:
	.string	"text"
.LC19:
	.string	"failure_info"
	.section	.data.rel.ro
	.align 32
	.type	TS_STATUS_INFO_seq_tt, @object
	.size	TS_STATUS_INFO_seq_tt, 120
TS_STATUS_INFO_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC17
	.quad	ASN1_INTEGER_it
	.quad	5
	.quad	0
	.quad	8
	.quad	.LC18
	.quad	ASN1_UTF8STRING_it
	.quad	1
	.quad	0
	.quad	16
	.quad	.LC19
	.quad	ASN1_BIT_STRING_it
	.section	.rodata.str1.1
.LC20:
	.string	"TS_TST_INFO"
	.section	.data.rel.ro.local
	.align 32
	.type	TS_TST_INFO_it, @object
	.size	TS_TST_INFO_it, 56
TS_TST_INFO_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	TS_TST_INFO_seq_tt
	.quad	10
	.quad	0
	.quad	80
	.quad	.LC20
	.section	.rodata.str1.1
.LC21:
	.string	"version"
.LC22:
	.string	"policy_id"
.LC23:
	.string	"msg_imprint"
.LC24:
	.string	"time"
.LC25:
	.string	"accuracy"
.LC26:
	.string	"ordering"
.LC27:
	.string	"nonce"
.LC28:
	.string	"tsa"
.LC29:
	.string	"extensions"
	.section	.data.rel.ro
	.align 32
	.type	TS_TST_INFO_seq_tt, @object
	.size	TS_TST_INFO_seq_tt, 400
TS_TST_INFO_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC21
	.quad	ASN1_INTEGER_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC22
	.quad	ASN1_OBJECT_it
	.quad	0
	.quad	0
	.quad	16
	.quad	.LC23
	.quad	TS_MSG_IMPRINT_it
	.quad	0
	.quad	0
	.quad	24
	.quad	.LC12
	.quad	ASN1_INTEGER_it
	.quad	0
	.quad	0
	.quad	32
	.quad	.LC24
	.quad	ASN1_GENERALIZEDTIME_it
	.quad	1
	.quad	0
	.quad	40
	.quad	.LC25
	.quad	TS_ACCURACY_it
	.quad	1
	.quad	0
	.quad	48
	.quad	.LC26
	.quad	ASN1_FBOOLEAN_it
	.quad	1
	.quad	0
	.quad	56
	.quad	.LC27
	.quad	ASN1_INTEGER_it
	.quad	145
	.quad	0
	.quad	64
	.quad	.LC28
	.quad	GENERAL_NAME_it
	.quad	141
	.quad	1
	.quad	72
	.quad	.LC29
	.quad	X509_EXTENSION_it
	.section	.rodata.str1.1
.LC30:
	.string	"TS_ACCURACY"
	.section	.data.rel.ro.local
	.align 32
	.type	TS_ACCURACY_it, @object
	.size	TS_ACCURACY_it, 56
TS_ACCURACY_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	TS_ACCURACY_seq_tt
	.quad	3
	.quad	0
	.quad	24
	.quad	.LC30
	.section	.rodata.str1.1
.LC31:
	.string	"seconds"
.LC32:
	.string	"millis"
.LC33:
	.string	"micros"
	.section	.data.rel.ro
	.align 32
	.type	TS_ACCURACY_seq_tt, @object
	.size	TS_ACCURACY_seq_tt, 120
TS_ACCURACY_seq_tt:
	.quad	1
	.quad	0
	.quad	0
	.quad	.LC31
	.quad	ASN1_INTEGER_it
	.quad	137
	.quad	0
	.quad	8
	.quad	.LC32
	.quad	ASN1_INTEGER_it
	.quad	137
	.quad	1
	.quad	16
	.quad	.LC33
	.quad	ASN1_INTEGER_it
	.section	.rodata.str1.1
.LC34:
	.string	"TS_REQ"
	.section	.data.rel.ro.local
	.align 32
	.type	TS_REQ_it, @object
	.size	TS_REQ_it, 56
TS_REQ_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	TS_REQ_seq_tt
	.quad	6
	.quad	0
	.quad	48
	.quad	.LC34
	.section	.rodata.str1.1
.LC35:
	.string	"cert_req"
	.section	.data.rel.ro
	.align 32
	.type	TS_REQ_seq_tt, @object
	.size	TS_REQ_seq_tt, 240
TS_REQ_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC21
	.quad	ASN1_INTEGER_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC23
	.quad	TS_MSG_IMPRINT_it
	.quad	1
	.quad	0
	.quad	16
	.quad	.LC22
	.quad	ASN1_OBJECT_it
	.quad	1
	.quad	0
	.quad	24
	.quad	.LC27
	.quad	ASN1_INTEGER_it
	.quad	1
	.quad	0
	.quad	32
	.quad	.LC35
	.quad	ASN1_FBOOLEAN_it
	.quad	141
	.quad	0
	.quad	40
	.quad	.LC29
	.quad	X509_EXTENSION_it
	.section	.rodata.str1.1
.LC36:
	.string	"TS_MSG_IMPRINT"
	.section	.data.rel.ro.local
	.align 32
	.type	TS_MSG_IMPRINT_it, @object
	.size	TS_MSG_IMPRINT_it, 56
TS_MSG_IMPRINT_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	TS_MSG_IMPRINT_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC36
	.section	.rodata.str1.1
.LC37:
	.string	"hash_algo"
.LC38:
	.string	"hashed_msg"
	.section	.data.rel.ro
	.align 32
	.type	TS_MSG_IMPRINT_seq_tt, @object
	.size	TS_MSG_IMPRINT_seq_tt, 80
TS_MSG_IMPRINT_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC37
	.quad	X509_ALGOR_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC38
	.quad	ASN1_OCTET_STRING_it
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
