	.file	"cmll_cfb.c"
	.text
	.p2align 4
	.globl	Camellia_cfb128_encrypt
	.type	Camellia_cfb128_encrypt, @function
Camellia_cfb128_encrypt:
.LFB0:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	Camellia_encrypt@GOTPCREL(%rip)
	movl	16(%rbp), %eax
	pushq	%rax
	call	CRYPTO_cfb128_encrypt@PLT
	popq	%rax
	popq	%rdx
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	Camellia_cfb128_encrypt, .-Camellia_cfb128_encrypt
	.p2align 4
	.globl	Camellia_cfb1_encrypt
	.type	Camellia_cfb1_encrypt, @function
Camellia_cfb1_encrypt:
.LFB1:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	Camellia_encrypt@GOTPCREL(%rip)
	movl	16(%rbp), %eax
	pushq	%rax
	call	CRYPTO_cfb128_1_encrypt@PLT
	popq	%rax
	popq	%rdx
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1:
	.size	Camellia_cfb1_encrypt, .-Camellia_cfb1_encrypt
	.p2align 4
	.globl	Camellia_cfb8_encrypt
	.type	Camellia_cfb8_encrypt, @function
Camellia_cfb8_encrypt:
.LFB2:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	Camellia_encrypt@GOTPCREL(%rip)
	movl	16(%rbp), %eax
	pushq	%rax
	call	CRYPTO_cfb128_8_encrypt@PLT
	popq	%rax
	popq	%rdx
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2:
	.size	Camellia_cfb8_encrypt, .-Camellia_cfb8_encrypt
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
