	.file	"bn_nist.c"
	.text
	.p2align 4
	.globl	BN_nist_mod_192
	.type	BN_nist_mod_192, @function
BN_nist_mod_192:
.LFB259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	8(%rsi), %eax
	movl	%eax, -116(%rbp)
	call	BN_is_negative@PLT
	testl	%eax, %eax
	jne	.L4
	leaq	_bignum_nist_p_192_sqr.7184(%rip), %rsi
	movq	%r15, %rdi
	movl	%eax, %r12d
	call	BN_ucmp@PLT
	testl	%eax, %eax
	js	.L26
.L4:
	movq	%r14, %rcx
	leaq	_bignum_nist_p_192(%rip), %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	BN_nnmod@PLT
	movl	%eax, %r12d
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L27
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movq	%r15, %rsi
	leaq	_bignum_nist_p_192(%rip), %rdi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	je	.L28
	jg	.L29
	cmpq	%r13, %r15
	je	.L17
	movl	$3, %esi
	movq	%r13, %rdi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L1
	movq	(%rbx), %rax
	movq	0(%r13), %r14
	movq	%rax, (%r14)
	movq	8(%rbx), %rax
	movq	%rax, 8(%r14)
	movq	16(%rbx), %rax
	movq	%rax, 16(%r14)
.L7:
	cmpl	$3, -116(%rbp)
	leaq	-112(%rbp), %rcx
	jle	.L8
	movl	-116(%rbp), %r15d
	movq	%rcx, %rdi
	leaq	24(%rbx), %rsi
	leal	-4(%r15), %eax
	leal	-3(%r15), %r12d
	leaq	8(,%rax,8), %rdx
	call	memcpy@PLT
	movq	%rax, %rcx
	cmpl	$2, %r12d
	jg	.L14
.L8:
	movl	$2, %eax
	subl	%r12d, %eax
	cmpl	$2, %r12d
	movslq	%r12d, %r12
	leaq	8(,%rax,8), %rdx
	movl	$8, %eax
	cmovg	%rax, %rdx
	leaq	(%rcx,%r12,8), %rax
	xorl	%esi, %esi
	movq	$0, (%rax)
	leaq	8(%rax), %rdi
	movl	%edx, %ecx
	subl	$8, %edx
	movq	$0, -8(%rax,%rcx)
	cmpl	$8, %edx
	jb	.L14
	andl	$-8, %edx
	xorl	%eax, %eax
.L12:
	movl	%eax, %ecx
	addl	$8, %eax
	movq	%rsi, (%rdi,%rcx)
	cmpl	%edx, %eax
	jb	.L12
.L14:
	movl	(%r14), %edi
	movl	-112(%rbp), %edx
	movq	$-1, %r12
	movl	-96(%rbp), %esi
	movl	-92(%rbp), %ecx
	addq	%rdx, %rdi
	movl	8(%r14), %r8d
	addq	%rsi, %rdi
	movq	%rdi, %rax
	movl	%edi, (%r14)
	movl	4(%r14), %edi
	sarq	$32, %rax
	addq	%rax, %rdi
	movl	-108(%rbp), %eax
	addq	%rax, %rdi
	addq	%rcx, %rdi
	movl	%edi, 4(%r14)
	sarq	$32, %rdi
	addq	%r8, %rdi
	movl	12(%r14), %r8d
	addq	%rdi, %rdx
	movl	-104(%rbp), %edi
	addq	%rdi, %rdx
	addq	%rsi, %rdx
	movl	%edx, 8(%r14)
	sarq	$32, %rdx
	addq	%r8, %rdx
	movl	16(%r14), %r8d
	addq	%rdx, %rax
	movl	-100(%rbp), %edx
	addq	%rdx, %rax
	addq	%rcx, %rax
	movl	%eax, 12(%r14)
	sarq	$32, %rax
	addq	%r8, %rax
	addq	%rax, %rdi
	movl	20(%r14), %eax
	addq	%rdi, %rsi
	movl	%esi, 16(%r14)
	sarq	$32, %rsi
	addq	%rax, %rsi
	addq	%rsi, %rdx
	addq	%rdx, %rcx
	movl	%ecx, 20(%r14)
	sarq	$32, %rcx
	jne	.L30
.L10:
	leaq	-80(%rbp), %rbx
	movl	$3, %ecx
	leaq	_nist_p_192(%rip), %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	bn_sub_words@PLT
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%r14, %rax
	xorq	%rbx, %rax
	negq	%rdx
	andq	%r12, %rax
	movl	$1, %r12d
	andq	%rdx, %rax
	xorq	%rbx, %rax
	movq	(%rax), %rdx
	movq	%rdx, (%r14)
	movq	8(%rax), %rdx
	movq	%rdx, 8(%r14)
	movq	16(%rax), %rax
	movq	%rax, 16(%r14)
	movl	$3, 8(%r13)
	call	bn_correct_top@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L29:
	movl	$1, %r12d
	cmpq	%r13, %r15
	je	.L1
	movq	%r15, %rsi
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	call	BN_copy@PLT
	testq	%rax, %rax
	setne	%r12b
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L17:
	movq	%rbx, %r14
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L28:
	xorl	%esi, %esi
	movq	%r13, %rdi
	movl	$1, %r12d
	call	BN_set_word@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L30:
	subl	$1, %ecx
	leaq	_nist_p_192(%rip), %rax
	movq	%r14, %rsi
	movq	%r14, %rdi
	movslq	%ecx, %rcx
	leaq	(%rcx,%rcx,2), %rdx
	movl	$3, %ecx
	leaq	(%rax,%rdx,8), %rdx
	call	bn_sub_words@PLT
	cltq
	negq	%rax
	movq	%rax, %r12
	jmp	.L10
.L27:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE259:
	.size	BN_nist_mod_192, .-BN_nist_mod_192
	.p2align 4
	.globl	BN_nist_mod_224
	.type	BN_nist_mod_224, @function
BN_nist_mod_224:
.LFB260:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	8(%rsi), %eax
	movl	%eax, -144(%rbp)
	call	BN_is_negative@PLT
	testl	%eax, %eax
	jne	.L34
	leaq	_bignum_nist_p_224_sqr.7215(%rip), %rsi
	movq	%r15, %rdi
	movl	%eax, %r12d
	call	BN_ucmp@PLT
	testl	%eax, %eax
	js	.L75
.L34:
	movq	%r14, %rcx
	leaq	_bignum_nist_p_224(%rip), %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	BN_nnmod@PLT
	movl	%eax, %r12d
.L31:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L76
	addq	$120, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	movq	%r15, %rsi
	leaq	_bignum_nist_p_224(%rip), %rdi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	je	.L77
	jg	.L78
	cmpq	%r13, %r15
	je	.L57
	movl	$4, %esi
	movq	%r13, %rdi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L31
	movq	0(%r13), %r14
	leaq	15(%rbx), %rax
	subq	%r14, %rax
	cmpq	$30, %rax
	jbe	.L38
	movdqu	(%rbx), %xmm6
	movups	%xmm6, (%r14)
	movdqu	16(%rbx), %xmm7
	movups	%xmm7, 16(%r14)
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L78:
	movl	$1, %r12d
	cmpq	%r13, %r15
	je	.L31
	movq	%r15, %rsi
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	call	BN_copy@PLT
	testq	%rax, %rax
	setne	%r12b
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L57:
	movq	%rbx, %r14
.L37:
	cmpl	$3, -144(%rbp)
	leaq	-128(%rbp), %r8
	jle	.L40
	leaq	40(%rbx), %rdx
	leaq	-128(%rbp), %r8
	movl	-144(%rbp), %esi
	cmpq	%rdx, %r8
	leaq	-112(%rbp), %rdi
	leaq	24(%rbx), %rdx
	setnb	%cl
	cmpq	%rdx, %rdi
	leal	-4(%rsi), %eax
	setbe	%dl
	orb	%dl, %cl
	je	.L41
	cmpl	$3, %eax
	jbe	.L41
	subl	$3, %esi
	xorl	%edx, %edx
	movl	%esi, %eax
	movl	%esi, %r12d
	shrl	%eax
	salq	$4, %rax
	.p2align 4,,10
	.p2align 3
.L42:
	movdqu	24(%rbx,%rdx), %xmm2
	movaps	%xmm2, (%r8,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rax
	jne	.L42
	movl	%r12d, %edx
	andl	$-2, %edx
	testb	$1, %r12b
	je	.L45
	movslq	%edx, %rdx
	salq	$3, %rdx
	movq	24(%rbx,%rdx), %rax
	movq	%rax, -128(%rbp,%rdx)
.L45:
	cmpl	$3, %r12d
	jg	.L51
.L40:
	movl	$3, %eax
	subl	%r12d, %eax
	cmpl	$3, %r12d
	movslq	%r12d, %r12
	leaq	8(,%rax,8), %rdx
	movl	$8, %eax
	cmovg	%rax, %rdx
	leaq	(%r8,%r12,8), %rax
	xorl	%edi, %edi
	movq	$0, (%rax)
	leaq	8(%rax), %r9
	movl	%edx, %ecx
	subl	$8, %edx
	movq	$0, -8(%rax,%rcx)
	cmpl	$8, %edx
	jb	.L51
	andl	$-8, %edx
	xorl	%eax, %eax
.L49:
	movl	%eax, %ecx
	addl	$8, %eax
	movq	%rdi, (%r9,%rcx)
	cmpl	%edx, %eax
	jb	.L49
.L51:
	movq	-120(%rbp), %rax
	movl	-124(%rbp), %edi
	leaq	-96(%rbp), %r15
	movq	%r8, -160(%rbp)
	movq	-104(%rbp), %r12
	movq	%rax, %rdx
	movq	%rdi, %rbx
	shrq	$32, %rax
	salq	$32, %rdx
	movq	%rax, %rsi
	salq	$32, %rax
	movq	%r12, %r9
	salq	$32, %r9
	orq	%rdx, %rbx
	shrq	$32, %rdx
	movq	%rbx, -144(%rbp)
	movq	-112(%rbp), %rbx
	orq	%rdx, %rax
	salq	$32, %rdi
	movq	%r15, %rdx
	movq	%rbx, %rcx
	shrq	$32, %rbx
	salq	$32, %rcx
	orq	%r9, %rbx
	orq	%rcx, %rsi
	movq	%rsi, -152(%rbp)
	movl	$4294967295, %esi
	andq	%rsi, 24(%r14)
	movq	%r14, %rsi
	movq	%rax, -80(%rbp)
	movq	%rcx, %rax
	movl	$4, %ecx
	shrq	$32, %rax
	movq	%rdi, -88(%rbp)
	movq	%r14, %rdi
	movq	$0, -96(%rbp)
	movq	%rax, -72(%rbp)
	call	bn_add_words@PLT
	movq	%rbx, %rax
	movq	%r15, %rdx
	movq	%r14, %rsi
	salq	$32, %rax
	movq	%r14, %rdi
	movabsq	$-4294967296, %rcx
	movq	$0, -96(%rbp)
	movq	%rax, -88(%rbp)
	movq	%rbx, %rax
	andq	%r12, %rcx
	shrq	$32, %r12
	shrq	$32, %rax
	movq	$0, -72(%rbp)
	orq	%rcx, %rax
	movl	$4, %ecx
	movq	%rax, -80(%rbp)
	call	bn_add_words@PLT
	movq	%r12, %xmm3
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%rbx, %xmm0
	movl	$4, %ecx
	movq	%r14, %rdi
	movq	-144(%rbp), %xmm1
	punpcklqdq	%xmm3, %xmm0
	movhps	-152(%rbp), %xmm1
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -144(%rbp)
	call	bn_sub_words@PLT
	movl	$4, %ecx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movdqa	-144(%rbp), %xmm0
	movq	%r14, %rdi
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	bn_sub_words@PLT
	movl	28(%r14), %eax
	movq	-160(%rbp), %r8
	testl	%eax, %eax
	jg	.L79
	movq	bn_sub_words@GOTPCREL(%rip), %rbx
	movq	$-1, %r12
	jne	.L80
.L52:
	movq	%r8, -144(%rbp)
	movq	%r8, %rdi
	movl	$4, %ecx
	movq	%r14, %rsi
	leaq	_nist_p_224(%rip), %rdx
	call	*%rbx
	movq	-144(%rbp), %r8
	movq	%r14, %rdx
	negq	%rax
	xorq	%r8, %rdx
	andq	%r12, %rdx
	andq	%rax, %rdx
	leaq	15(%r14), %rax
	xorq	%rdx, %r8
	subq	%r8, %rax
	cmpq	$30, %rax
	jbe	.L53
	movdqu	(%r8), %xmm4
	movups	%xmm4, (%r14)
	movdqu	16(%r8), %xmm5
	movups	%xmm5, 16(%r14)
.L54:
	movl	$4, 8(%r13)
	movq	%r13, %rdi
	movl	$1, %r12d
	call	bn_correct_top@PLT
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L77:
	xorl	%esi, %esi
	movq	%r13, %rdi
	movl	$1, %r12d
	call	BN_set_word@PLT
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L80:
	notl	%eax
	movl	$4, %ecx
	movq	%r14, %rsi
	movq	%r14, %rdi
	movslq	%eax, %rdx
	leaq	_nist_p_224(%rip), %rax
	movq	%r8, -144(%rbp)
	salq	$5, %rdx
	addq	%rax, %rdx
	call	bn_add_words@PLT
	movq	-144(%rbp), %r8
	cltq
	movq	%rax, %r12
	subq	$1, %rax
	andq	bn_add_words@GOTPCREL(%rip), %rax
	negq	%r12
	andq	%r12, %rbx
	orq	%rax, %rbx
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L38:
	movq	(%rbx), %rax
	movq	%rax, (%r14)
	movq	8(%rbx), %rax
	movq	%rax, 8(%r14)
	movq	16(%rbx), %rax
	movq	%rax, 16(%r14)
	movq	24(%rbx), %rax
	movq	%rax, 24(%r14)
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L79:
	leal	-1(%rax), %edx
	leaq	_nist_p_224(%rip), %rax
	movq	%r14, %rsi
	movq	%r14, %rdi
	movslq	%edx, %rdx
	movl	$4, %ecx
	movq	%r8, -144(%rbp)
	salq	$5, %rdx
	addq	%rax, %rdx
	call	bn_sub_words@PLT
	movl	28(%r14), %edx
	movq	bn_sub_words@GOTPCREL(%rip), %rbx
	movq	-144(%rbp), %r8
	notl	%edx
	andl	$1, %edx
	negq	%rdx
	movq	%rdx, %r12
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L53:
	movq	(%r8), %rax
	movq	%rax, (%r14)
	movq	8(%r8), %rax
	movq	%rax, 8(%r14)
	movq	16(%r8), %rax
	movq	%rax, 16(%r14)
	movq	24(%r8), %rax
	movq	%rax, 24(%r14)
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L41:
	movl	%eax, %edx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L44:
	movq	24(%rbx,%rax,8), %rcx
	movq	%rcx, (%r8,%rax,8)
	movq	%rax, %rcx
	addq	$1, %rax
	cmpq	%rcx, %rdx
	jne	.L44
	movl	-144(%rbp), %eax
	leal	-3(%rax), %r12d
	jmp	.L45
.L76:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE260:
	.size	BN_nist_mod_224, .-BN_nist_mod_224
	.p2align 4
	.globl	BN_nist_mod_256
	.type	BN_nist_mod_256, @function
BN_nist_mod_256:
.LFB261:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	8(%rsi), %eax
	movl	%eax, -132(%rbp)
	call	BN_is_negative@PLT
	testl	%eax, %eax
	jne	.L84
	leaq	_bignum_nist_p_256_sqr.7239(%rip), %rsi
	movq	%r15, %rdi
	movl	%eax, %r12d
	call	BN_ucmp@PLT
	testl	%eax, %eax
	js	.L113
.L84:
	movq	%r14, %rcx
	leaq	_bignum_nist_p_256(%rip), %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	BN_nnmod@PLT
	movl	%eax, %r12d
.L81:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L114
	addq	$104, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore_state
	movq	%r15, %rsi
	leaq	_bignum_nist_p_256(%rip), %rdi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	je	.L115
	jg	.L116
	cmpq	%r13, %r15
	je	.L102
	movl	$4, %esi
	movq	%r13, %rdi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L81
	movq	0(%r13), %r14
	leaq	15(%rbx), %rax
	subq	%r14, %rax
	cmpq	$30, %rax
	jbe	.L88
	movdqu	(%rbx), %xmm2
	movups	%xmm2, (%r14)
	movdqu	16(%rbx), %xmm3
	movups	%xmm3, 16(%r14)
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L116:
	movl	$1, %r12d
	cmpq	%r13, %r15
	je	.L81
	movq	%r15, %rsi
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	call	BN_copy@PLT
	testq	%rax, %rax
	setne	%r12b
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L102:
	movq	%rbx, %r14
.L87:
	cmpl	$4, -132(%rbp)
	leaq	-128(%rbp), %rcx
	jle	.L90
	movl	-132(%rbp), %r15d
	movq	%rcx, %rdi
	leaq	32(%rbx), %rsi
	leal	-5(%r15), %eax
	leal	-4(%r15), %r12d
	leaq	8(,%rax,8), %rdx
	call	memcpy@PLT
	movq	%rax, %rcx
	cmpl	$3, %r12d
	jg	.L96
.L90:
	movl	$3, %eax
	subl	%r12d, %eax
	cmpl	$3, %r12d
	movslq	%r12d, %r12
	leaq	8(,%rax,8), %rdx
	movl	$8, %eax
	cmovg	%rax, %rdx
	leaq	(%rcx,%r12,8), %rax
	xorl	%esi, %esi
	movq	$0, (%rax)
	leaq	8(%rax), %rdi
	movl	%edx, %ecx
	subl	$8, %edx
	movq	$0, -8(%rax,%rcx)
	cmpl	$8, %edx
	jb	.L96
	andl	$-8, %edx
	xorl	%eax, %eax
.L94:
	movl	%eax, %ecx
	addl	$8, %eax
	movq	%rsi, (%rdi,%rcx)
	cmpl	%edx, %eax
	jb	.L94
.L96:
	movl	-128(%rbp), %eax
	movl	(%r14), %r10d
	movl	-124(%rbp), %r11d
	movl	-116(%rbp), %edi
	addq	%rax, %r10
	movl	-112(%rbp), %esi
	movl	-108(%rbp), %ecx
	addq	%r11, %r10
	movl	-104(%rbp), %edx
	movl	-100(%rbp), %r9d
	subq	%rdi, %r10
	movl	8(%r14), %ebx
	subq	%rsi, %r10
	subq	%rcx, %r10
	subq	%rdx, %r10
	movq	%r10, %r8
	movl	%r10d, (%r14)
	movl	4(%r14), %r10d
	sarq	$32, %r8
	addq	%r8, %r10
	movl	-120(%rbp), %r8d
	addq	%r11, %r10
	addq	%r8, %r10
	subq	%rsi, %r10
	subq	%rcx, %r10
	subq	%rdx, %r10
	subq	%r9, %r10
	movl	%r10d, 4(%r14)
	sarq	$32, %r10
	addq	%rbx, %r10
	movl	12(%r14), %ebx
	addq	%r8, %r10
	addq	%rdi, %r10
	subq	%rcx, %r10
	subq	%rdx, %r10
	subq	%r9, %r10
	movl	%r10d, 8(%r14)
	sarq	$32, %r10
	addq	%rbx, %r10
	movl	16(%r14), %ebx
	leaq	(%r10,%rdi,2), %r10
	leaq	(%r10,%rsi,2), %r10
	addq	%rcx, %r10
	subq	%r9, %r10
	subq	%rax, %r10
	subq	%r11, %r10
	movl	%r10d, 12(%r14)
	sarq	$32, %r10
	addq	%rbx, %r10
	movl	20(%r14), %ebx
	leaq	(%r10,%rsi,2), %r10
	leaq	(%r10,%rcx,2), %r10
	addq	%rdx, %r10
	subq	%r11, %r10
	subq	%r8, %r10
	movl	%r10d, 16(%r14)
	sarq	$32, %r10
	addq	%rbx, %r10
	movl	24(%r14), %ebx
	leaq	(%r10,%rcx,2), %r10
	leaq	(%r10,%rdx,2), %r10
	addq	%r9, %r10
	subq	%r8, %r10
	subq	%rdi, %r10
	movl	%r10d, 20(%r14)
	sarq	$32, %r10
	addq	%rbx, %r10
	leaq	(%r10,%rdx,2), %r10
	leaq	(%r10,%r9,2), %r10
	addq	%r10, %rdx
	movl	28(%r14), %r10d
	addq	%rcx, %rdx
	subq	%rax, %rdx
	subq	%r11, %rdx
	movl	%edx, 24(%r14)
	sarq	$32, %rdx
	addq	%r10, %rdx
	leaq	(%rdx,%r9,2), %rdx
	addq	%rdx, %r9
	addq	%r9, %rax
	subq	%r8, %rax
	subq	%rdi, %rax
	subq	%rsi, %rax
	subq	%rcx, %rax
	movl	%eax, 28(%r14)
	sarq	$32, %rax
	testq	%rax, %rax
	jg	.L117
	movq	bn_sub_words@GOTPCREL(%rip), %rbx
	movq	$-1, %r12
	jne	.L118
.L97:
	leaq	-96(%rbp), %r15
	leaq	_nist_p_256(%rip), %rdx
	movl	$4, %ecx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	*%rbx
	movq	%rax, %rdx
	movq	%r14, %rax
	xorq	%r15, %rax
	negq	%rdx
	andq	%r12, %rax
	andq	%rdx, %rax
	leaq	15(%r14), %rdx
	xorq	%r15, %rax
	subq	%rax, %rdx
	cmpq	$30, %rdx
	jbe	.L98
	movdqu	(%rax), %xmm0
	movups	%xmm0, (%r14)
	movdqu	16(%rax), %xmm1
	movups	%xmm1, 16(%r14)
.L99:
	movl	$4, 8(%r13)
	movq	%r13, %rdi
	movl	$1, %r12d
	call	bn_correct_top@PLT
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L115:
	xorl	%esi, %esi
	movq	%r13, %rdi
	movl	$1, %r12d
	call	BN_set_word@PLT
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L118:
	notl	%eax
	movl	$4, %ecx
	movq	%r14, %rsi
	movq	%r14, %rdi
	movslq	%eax, %rdx
	leaq	_nist_p_256(%rip), %rax
	salq	$5, %rdx
	addq	%rax, %rdx
	call	bn_add_words@PLT
	cltq
	movq	%rax, %r12
	subq	$1, %rax
	andq	bn_add_words@GOTPCREL(%rip), %rax
	negq	%r12
	andq	%r12, %rbx
	orq	%rax, %rbx
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L88:
	movq	(%rbx), %rax
	movq	%rax, (%r14)
	movq	8(%rbx), %rax
	movq	%rax, 8(%r14)
	movq	16(%rbx), %rax
	movq	%rax, 16(%r14)
	movq	24(%rbx), %rax
	movq	%rax, 24(%r14)
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L117:
	leal	-1(%rax), %edx
	leaq	_nist_p_256(%rip), %rax
	movq	%r14, %rsi
	movq	%r14, %rdi
	movslq	%edx, %rdx
	movl	$4, %ecx
	salq	$5, %rdx
	addq	%rax, %rdx
	call	bn_sub_words@PLT
	movq	bn_sub_words@GOTPCREL(%rip), %rbx
	cltq
	negq	%rax
	movq	%rax, %r12
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L98:
	movq	(%rax), %rdx
	movq	%rdx, (%r14)
	movq	8(%rax), %rdx
	movq	%rdx, 8(%r14)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
	movq	24(%rax), %rax
	movq	%rax, 24(%r14)
	jmp	.L99
.L114:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE261:
	.size	BN_nist_mod_256, .-BN_nist_mod_256
	.p2align 4
	.globl	BN_nist_mod_384
	.type	BN_nist_mod_384, @function
BN_nist_mod_384:
.LFB262:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$136, %rsp
	movq	(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	8(%rsi), %eax
	movl	%eax, -164(%rbp)
	call	BN_is_negative@PLT
	testl	%eax, %eax
	jne	.L122
	leaq	_bignum_nist_p_384_sqr.7265(%rip), %rsi
	movq	%r15, %rdi
	movl	%eax, %r12d
	call	BN_ucmp@PLT
	testl	%eax, %eax
	js	.L151
.L122:
	movq	%rbx, %rcx
	leaq	_bignum_nist_p_384(%rip), %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	BN_nnmod@PLT
	movl	%eax, %r12d
.L119:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L152
	addq	$136, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_restore_state
	movq	%r15, %rsi
	leaq	_bignum_nist_p_384(%rip), %rdi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	je	.L153
	jg	.L154
	cmpq	%r13, %r15
	je	.L140
	movl	$6, %esi
	movq	%r13, %rdi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L119
	movq	0(%r13), %rbx
	leaq	15(%r14), %rax
	subq	%rbx, %rax
	cmpq	$30, %rax
	jbe	.L126
	movdqu	(%r14), %xmm3
	movups	%xmm3, (%rbx)
	movdqu	16(%r14), %xmm4
	movups	%xmm4, 16(%rbx)
	movdqu	32(%r14), %xmm5
	movups	%xmm5, 32(%rbx)
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L154:
	movl	$1, %r12d
	cmpq	%r13, %r15
	je	.L119
	movq	%r15, %rsi
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	call	BN_copy@PLT
	testq	%rax, %rax
	setne	%r12b
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L140:
	movq	%r14, %rbx
.L125:
	cmpl	$6, -164(%rbp)
	leaq	-160(%rbp), %rcx
	jle	.L128
	movl	-164(%rbp), %r15d
	movq	%rcx, %rdi
	leaq	48(%r14), %rsi
	leal	-7(%r15), %eax
	leal	-6(%r15), %r12d
	leaq	8(,%rax,8), %rdx
	call	memcpy@PLT
	movq	%rax, %rcx
	cmpl	$5, %r12d
	jg	.L134
.L128:
	movl	$5, %eax
	subl	%r12d, %eax
	cmpl	$5, %r12d
	movslq	%r12d, %r12
	leaq	8(,%rax,8), %rdx
	movl	$8, %eax
	cmovg	%rax, %rdx
	leaq	(%rcx,%r12,8), %rax
	xorl	%esi, %esi
	movq	$0, (%rax)
	leaq	8(%rax), %rdi
	movl	%edx, %ecx
	subl	$8, %edx
	movq	$0, -8(%rax,%rcx)
	cmpl	$8, %edx
	jb	.L134
	andl	$-8, %edx
	xorl	%eax, %eax
.L132:
	movl	%eax, %ecx
	addl	$8, %eax
	movq	%rsi, (%rdi,%rcx)
	cmpl	%edx, %eax
	jb	.L132
.L134:
	movl	-160(%rbp), %edx
	movl	(%rbx), %eax
	movl	-124(%rbp), %r11d
	movl	-128(%rbp), %r9d
	addq	%rdx, %rax
	movl	-116(%rbp), %r8d
	movl	-120(%rbp), %r10d
	addq	%r11, %rax
	movl	8(%rbx), %esi
	movl	-152(%rbp), %edi
	addq	%r9, %rax
	movl	16(%rbx), %r12d
	subq	%r8, %rax
	movl	%eax, (%rbx)
	sarq	$32, %rax
	movq	%rax, %rcx
	movl	4(%rbx), %eax
	addq	%rcx, %rax
	movl	-156(%rbp), %ecx
	addq	%rcx, %rax
	addq	%r10, %rax
	addq	%r8, %rax
	subq	%rdx, %rax
	subq	%r9, %rax
	movl	%eax, 4(%rbx)
	sarq	$32, %rax
	addq	%rsi, %rax
	addq	%rdi, %rax
	addq	%r8, %rax
	subq	%rcx, %rax
	subq	%r11, %rax
	movl	%eax, 8(%rbx)
	sarq	$32, %rax
	movq	%rax, %rsi
	movl	12(%rbx), %eax
	addq	%rsi, %rax
	movl	-148(%rbp), %esi
	addq	%rsi, %rax
	addq	%rdx, %rax
	addq	%r9, %rax
	addq	%r11, %rax
	subq	%rdi, %rax
	subq	%r10, %rax
	subq	%r8, %rax
	movl	%eax, 12(%rbx)
	sarq	$32, %rax
	addq	%r12, %rax
	movl	-144(%rbp), %r12d
	leaq	(%rax,%r11,2), %rax
	addq	%r12, %rax
	addq	%rcx, %rax
	addq	%rdx, %rax
	movl	20(%rbx), %edx
	addq	%r9, %rax
	addq	%r10, %rax
	subq	%rsi, %rax
	subq	%r8, %rax
	subq	%r8, %rax
	movl	%eax, 16(%rbx)
	sarq	$32, %rax
	addq	%rdx, %rax
	leaq	(%rax,%r10,2), %rdx
	movl	-140(%rbp), %eax
	addq	%rax, %rdx
	addq	%rdi, %rdx
	addq	%rdx, %rcx
	movl	24(%rbx), %edx
	addq	%r11, %rcx
	addq	%r8, %rcx
	subq	%r12, %rcx
	movl	%ecx, 20(%rbx)
	sarq	$32, %rcx
	addq	%rdx, %rcx
	leaq	(%rcx,%r8,2), %rdx
	movl	-136(%rbp), %ecx
	addq	%rcx, %rdx
	addq	%rsi, %rdx
	addq	%rdx, %rdi
	movl	28(%rbx), %edx
	addq	%r10, %rdi
	subq	%rax, %rdi
	movl	%edi, 24(%rbx)
	sarq	$32, %rdi
	addq	%rdx, %rdi
	movl	-132(%rbp), %edx
	addq	%rdx, %rdi
	addq	%r12, %rdi
	addq	%rdi, %rsi
	movl	32(%rbx), %edi
	addq	%r8, %rsi
	subq	%rcx, %rsi
	movl	%esi, 28(%rbx)
	sarq	$32, %rsi
	addq	%rdi, %rsi
	addq	%r9, %rsi
	addq	%rax, %rsi
	addq	%rsi, %r12
	movl	36(%rbx), %esi
	subq	%rdx, %r12
	movl	%r12d, 32(%rbx)
	sarq	$32, %r12
	addq	%rsi, %r12
	addq	%r11, %r12
	addq	%rcx, %r12
	addq	%r12, %rax
	subq	%r9, %rax
	movl	%eax, 36(%rbx)
	movl	40(%rbx), %esi
	sarq	$32, %rax
	addq	%rsi, %rax
	addq	%r10, %rax
	addq	%rdx, %rax
	addq	%rax, %rcx
	movl	44(%rbx), %eax
	subq	%r11, %rcx
	movl	%ecx, 40(%rbx)
	sarq	$32, %rcx
	addq	%rax, %rcx
	addq	%rcx, %r8
	addq	%r9, %r8
	addq	%r8, %rdx
	subq	%r10, %rdx
	movl	%edx, 44(%rbx)
	sarq	$32, %rdx
	testq	%rdx, %rdx
	jg	.L155
	movq	bn_sub_words@GOTPCREL(%rip), %r12
	movq	$-1, %r14
	jne	.L156
.L135:
	leaq	-112(%rbp), %r15
	leaq	_nist_p_384(%rip), %rdx
	movl	$6, %ecx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	*%r12
	movq	%rax, %rdx
	movq	%rbx, %rax
	xorq	%r15, %rax
	negq	%rdx
	andq	%r14, %rax
	andq	%rdx, %rax
	leaq	15(%rbx), %rdx
	xorq	%r15, %rax
	subq	%rax, %rdx
	cmpq	$30, %rdx
	jbe	.L136
	movdqu	(%rax), %xmm0
	movups	%xmm0, (%rbx)
	movdqu	16(%rax), %xmm1
	movups	%xmm1, 16(%rbx)
	movdqu	32(%rax), %xmm2
	movups	%xmm2, 32(%rbx)
.L137:
	movl	$6, 8(%r13)
	movq	%r13, %rdi
	movl	$1, %r12d
	call	bn_correct_top@PLT
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L153:
	xorl	%esi, %esi
	movq	%r13, %rdi
	movl	$1, %r12d
	call	BN_set_word@PLT
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L156:
	notl	%edx
	leaq	_nist_p_384(%rip), %rax
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	movslq	%edx, %rdx
	movl	$6, %ecx
	leaq	(%rdx,%rdx,2), %rdx
	salq	$4, %rdx
	addq	%rax, %rdx
	call	bn_add_words@PLT
	cltq
	movq	%rax, %r14
	subq	$1, %rax
	andq	bn_add_words@GOTPCREL(%rip), %rax
	negq	%r14
	andq	%r14, %r12
	orq	%rax, %r12
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L126:
	movq	(%r14), %rax
	movq	%rax, (%rbx)
	movq	8(%r14), %rax
	movq	%rax, 8(%rbx)
	movq	16(%r14), %rax
	movq	%rax, 16(%rbx)
	movq	24(%r14), %rax
	movq	%rax, 24(%rbx)
	movq	32(%r14), %rax
	movq	%rax, 32(%rbx)
	movq	40(%r14), %rax
	movq	%rax, 40(%rbx)
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L155:
	subl	$1, %edx
	leaq	_nist_p_384(%rip), %rax
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	movslq	%edx, %rdx
	movl	$6, %ecx
	leaq	(%rdx,%rdx,2), %rdx
	salq	$4, %rdx
	addq	%rax, %rdx
	call	bn_sub_words@PLT
	movq	bn_sub_words@GOTPCREL(%rip), %r12
	cltq
	negq	%rax
	movq	%rax, %r14
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L136:
	movq	(%rax), %rdx
	movq	%rdx, (%rbx)
	movq	8(%rax), %rdx
	movq	%rdx, 8(%rbx)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	24(%rax), %rdx
	movq	%rdx, 24(%rbx)
	movq	32(%rax), %rdx
	movq	%rdx, 32(%rbx)
	movq	40(%rax), %rax
	movq	%rax, 40(%rbx)
	jmp	.L137
.L152:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE262:
	.size	BN_nist_mod_384, .-BN_nist_mod_384
	.p2align 4
	.globl	BN_nist_mod_521
	.type	BN_nist_mod_521, @function
BN_nist_mod_521:
.LFB263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rsi, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movq	(%rsi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	8(%rsi), %eax
	movl	%eax, -132(%rbp)
	call	BN_is_negative@PLT
	testl	%eax, %eax
	jne	.L160
	leaq	_bignum_nist_p_521_sqr.7284(%rip), %rsi
	movq	%rbx, %rdi
	movl	%eax, %r12d
	call	BN_ucmp@PLT
	testl	%eax, %eax
	js	.L192
.L160:
	movq	%r13, %rcx
	leaq	_bignum_nist_p_521(%rip), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	BN_nnmod@PLT
	movl	%eax, %r12d
.L157:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L193
	addq	$104, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L192:
	.cfi_restore_state
	movq	%rbx, %rsi
	leaq	_bignum_nist_p_521(%rip), %rdi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	je	.L194
	jg	.L195
	cmpq	%r14, %rbx
	je	.L177
	movl	$9, %esi
	movq	%r14, %rdi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L157
	movq	(%r14), %rbx
	leaq	15(%r15), %rax
	subq	%rbx, %rax
	cmpq	$30, %rax
	jbe	.L164
	movdqu	(%r15), %xmm5
	movups	%xmm5, (%rbx)
	movdqu	16(%r15), %xmm6
	movups	%xmm6, 16(%rbx)
	movdqu	32(%r15), %xmm7
	movups	%xmm7, 32(%rbx)
	movdqu	48(%r15), %xmm5
	movups	%xmm5, 48(%rbx)
	movq	64(%r15), %rax
	movq	%rax, 64(%rbx)
.L163:
	cmpl	$8, -132(%rbp)
	leaq	-128(%rbp), %r13
	jle	.L166
	leaq	80(%r15), %rcx
	leaq	-128(%rbp), %r13
	movl	-132(%rbp), %edx
	cmpq	%rcx, %r13
	leaq	-112(%rbp), %rdi
	leaq	64(%r15), %rcx
	setnb	%sil
	cmpq	%rcx, %rdi
	leal	-9(%rdx), %eax
	setbe	%cl
	orb	%cl, %sil
	je	.L167
	cmpl	$3, %eax
	jbe	.L167
	subl	$8, %edx
	xorl	%ecx, %ecx
	movl	%edx, %eax
	movl	%edx, %r12d
	shrl	%eax
	salq	$4, %rax
	.p2align 4,,10
	.p2align 3
.L168:
	movdqu	64(%r15,%rcx), %xmm4
	movaps	%xmm4, 0(%r13,%rcx)
	addq	$16, %rcx
	cmpq	%rcx, %rax
	jne	.L168
	movl	%r12d, %ecx
	andl	$-2, %ecx
	testb	$1, %r12b
	je	.L171
	movslq	%ecx, %rcx
	salq	$3, %rcx
	movq	64(%r15,%rcx), %rax
	movq	%rax, -128(%rbp,%rcx)
.L171:
	cmpl	$8, %r12d
	jg	.L172
.L166:
	movl	$8, %eax
	subl	%r12d, %eax
	cmpl	$8, %r12d
	movslq	%r12d, %r12
	leaq	8(,%rax,8), %rdx
	movl	$8, %eax
	cmovg	%rax, %rdx
	leaq	0(%r13,%r12,8), %rax
	movq	$0, (%rax)
	leaq	8(%rax), %rdi
	movl	%edx, %ecx
	movq	$0, -8(%rax,%rcx)
	leal	-8(%rdx), %ecx
	xorl	%eax, %eax
	movl	%ecx, %edx
	shrl	$3, %edx
	movl	%edx, %ecx
	rep stosq
.L172:
	movq	-120(%rbp), %rax
	movq	-128(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rcx
	shrq	$9, %rdx
	salq	$55, %rcx
	shrq	$9, %rax
	orq	%rcx, %rdx
	movq	%rdx, -128(%rbp)
	movq	-112(%rbp), %rdx
	movq	%rdx, %rcx
	shrq	$9, %rdx
	salq	$55, %rcx
	orq	%rcx, %rax
	movq	%rax, -120(%rbp)
	movq	-104(%rbp), %rax
	movq	%rax, %rcx
	shrq	$9, %rax
	salq	$55, %rcx
	orq	%rcx, %rdx
	movq	%rdx, -112(%rbp)
	movq	-96(%rbp), %rdx
	movq	%rdx, %rcx
	shrq	$9, %rdx
	salq	$55, %rcx
	orq	%rcx, %rax
	movq	%rax, -104(%rbp)
	movq	-88(%rbp), %rax
	movq	%rax, %rcx
	shrq	$9, %rax
	salq	$55, %rcx
	orq	%rcx, %rdx
	movq	%rdx, -96(%rbp)
	movq	-80(%rbp), %rdx
	movq	%rdx, %rcx
	shrq	$9, %rdx
	salq	$55, %rcx
	orq	%rcx, %rax
	movq	%rax, -88(%rbp)
	movq	-72(%rbp), %rax
	movq	%rax, %rcx
	shrq	$9, %rax
	salq	$55, %rcx
	orq	%rcx, %rdx
	movq	%rdx, -80(%rbp)
	movq	-64(%rbp), %rdx
	movq	%rdx, %rcx
	shrq	$9, %rdx
	salq	$55, %rcx
	movq	%rdx, -64(%rbp)
	movq	%r13, %rdx
	orq	%rcx, %rax
	movl	$9, %ecx
	movq	%rax, -72(%rbp)
	andq	$511, 64(%rbx)
	call	bn_add_words@PLT
	movq	%r13, %rdi
	movl	$9, %ecx
	movq	%rbx, %rsi
	leaq	_nist_p_521(%rip), %rdx
	call	bn_sub_words@PLT
	leaq	-1(%rax), %rdx
	negq	%rax
	andq	%rdx, %r13
	andq	%rbx, %rax
	leaq	15(%rbx), %rdx
	orq	%r13, %rax
	subq	%rax, %rdx
	cmpq	$30, %rdx
	jbe	.L173
	movdqu	(%rax), %xmm0
	movups	%xmm0, (%rbx)
	movdqu	16(%rax), %xmm1
	movups	%xmm1, 16(%rbx)
	movdqu	32(%rax), %xmm2
	movups	%xmm2, 32(%rbx)
	movdqu	48(%rax), %xmm3
	movups	%xmm3, 48(%rbx)
	movq	64(%rax), %rax
	movq	%rax, 64(%rbx)
.L174:
	movl	$9, 8(%r14)
	movq	%r14, %rdi
	movl	$1, %r12d
	call	bn_correct_top@PLT
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L195:
	movl	$1, %r12d
	cmpq	%r14, %rbx
	je	.L157
	movq	%rbx, %rsi
	movq	%r14, %rdi
	xorl	%r12d, %r12d
	call	BN_copy@PLT
	testq	%rax, %rax
	setne	%r12b
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L177:
	movq	%r15, %rbx
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L194:
	xorl	%esi, %esi
	movq	%r14, %rdi
	movl	$1, %r12d
	call	BN_set_word@PLT
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L164:
	movq	(%r15), %rax
	movq	%rax, (%rbx)
	movq	8(%r15), %rax
	movq	%rax, 8(%rbx)
	movq	16(%r15), %rax
	movq	%rax, 16(%rbx)
	movq	24(%r15), %rax
	movq	%rax, 24(%rbx)
	movq	32(%r15), %rax
	movq	%rax, 32(%rbx)
	movq	40(%r15), %rax
	movq	%rax, 40(%rbx)
	movq	48(%r15), %rax
	movq	%rax, 48(%rbx)
	movq	56(%r15), %rax
	movq	%rax, 56(%rbx)
	movq	64(%r15), %rax
	movq	%rax, 64(%rbx)
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L173:
	movq	(%rax), %rdx
	movq	%rdx, (%rbx)
	movq	8(%rax), %rdx
	movq	%rdx, 8(%rbx)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	24(%rax), %rdx
	movq	%rdx, 24(%rbx)
	movq	32(%rax), %rdx
	movq	%rdx, 32(%rbx)
	movq	40(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movq	48(%rax), %rdx
	movq	%rdx, 48(%rbx)
	movq	56(%rax), %rdx
	movq	%rdx, 56(%rbx)
	movq	64(%rax), %rax
	movq	%rax, 64(%rbx)
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L167:
	movl	%eax, %ecx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L170:
	movq	64(%r15,%rax,8), %rsi
	movq	%rsi, 0(%r13,%rax,8)
	movq	%rax, %rsi
	addq	$1, %rax
	cmpq	%rsi, %rcx
	jne	.L170
	movl	-132(%rbp), %eax
	leal	-8(%rax), %r12d
	jmp	.L171
.L193:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE263:
	.size	BN_nist_mod_521, .-BN_nist_mod_521
	.p2align 4
	.globl	BN_get0_nist_prime_192
	.type	BN_get0_nist_prime_192, @function
BN_get0_nist_prime_192:
.LFB252:
	.cfi_startproc
	endbr64
	leaq	_bignum_nist_p_192(%rip), %rax
	ret
	.cfi_endproc
.LFE252:
	.size	BN_get0_nist_prime_192, .-BN_get0_nist_prime_192
	.p2align 4
	.globl	BN_get0_nist_prime_224
	.type	BN_get0_nist_prime_224, @function
BN_get0_nist_prime_224:
.LFB253:
	.cfi_startproc
	endbr64
	leaq	_bignum_nist_p_224(%rip), %rax
	ret
	.cfi_endproc
.LFE253:
	.size	BN_get0_nist_prime_224, .-BN_get0_nist_prime_224
	.p2align 4
	.globl	BN_get0_nist_prime_256
	.type	BN_get0_nist_prime_256, @function
BN_get0_nist_prime_256:
.LFB254:
	.cfi_startproc
	endbr64
	leaq	_bignum_nist_p_256(%rip), %rax
	ret
	.cfi_endproc
.LFE254:
	.size	BN_get0_nist_prime_256, .-BN_get0_nist_prime_256
	.p2align 4
	.globl	BN_get0_nist_prime_384
	.type	BN_get0_nist_prime_384, @function
BN_get0_nist_prime_384:
.LFB255:
	.cfi_startproc
	endbr64
	leaq	_bignum_nist_p_384(%rip), %rax
	ret
	.cfi_endproc
.LFE255:
	.size	BN_get0_nist_prime_384, .-BN_get0_nist_prime_384
	.p2align 4
	.globl	BN_get0_nist_prime_521
	.type	BN_get0_nist_prime_521, @function
BN_get0_nist_prime_521:
.LFB256:
	.cfi_startproc
	endbr64
	leaq	_bignum_nist_p_521(%rip), %rax
	ret
	.cfi_endproc
.LFE256:
	.size	BN_get0_nist_prime_521, .-BN_get0_nist_prime_521
	.p2align 4
	.globl	BN_nist_mod_func
	.type	BN_nist_mod_func, @function
BN_nist_mod_func:
.LFB264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	_bignum_nist_p_192(%rip), %rdi
	subq	$8, %rsp
	call	BN_ucmp@PLT
	movl	%eax, %r8d
	leaq	BN_nist_mod_192(%rip), %rax
	testl	%r8d, %r8d
	jne	.L213
.L201:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L213:
	.cfi_restore_state
	movq	%r12, %rsi
	leaq	_bignum_nist_p_224(%rip), %rdi
	call	BN_ucmp@PLT
	movl	%eax, %r8d
	leaq	BN_nist_mod_224(%rip), %rax
	testl	%r8d, %r8d
	je	.L201
	movq	%r12, %rsi
	leaq	_bignum_nist_p_256(%rip), %rdi
	call	BN_ucmp@PLT
	movl	%eax, %r8d
	leaq	BN_nist_mod_256(%rip), %rax
	testl	%r8d, %r8d
	je	.L201
	movq	%r12, %rsi
	leaq	_bignum_nist_p_384(%rip), %rdi
	call	BN_ucmp@PLT
	movl	%eax, %r8d
	leaq	BN_nist_mod_384(%rip), %rax
	testl	%r8d, %r8d
	je	.L201
	movq	%r12, %rsi
	leaq	_bignum_nist_p_521(%rip), %rdi
	call	BN_ucmp@PLT
	movl	$0, %edx
	testl	%eax, %eax
	leaq	BN_nist_mod_521(%rip), %rax
	cmovne	%rdx, %rax
	jmp	.L201
	.cfi_endproc
.LFE264:
	.size	BN_nist_mod_func, .-BN_nist_mod_func
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_bignum_nist_p_521_sqr.7284, @object
	.size	_bignum_nist_p_521_sqr.7284, 24
_bignum_nist_p_521_sqr.7284:
	.quad	_nist_p_521_sqr
	.long	17
	.long	17
	.long	0
	.long	2
	.align 16
	.type	_bignum_nist_p_384_sqr.7265, @object
	.size	_bignum_nist_p_384_sqr.7265, 24
_bignum_nist_p_384_sqr.7265:
	.quad	_nist_p_384_sqr
	.long	12
	.long	12
	.long	0
	.long	2
	.align 16
	.type	_bignum_nist_p_256_sqr.7239, @object
	.size	_bignum_nist_p_256_sqr.7239, 24
_bignum_nist_p_256_sqr.7239:
	.quad	_nist_p_256_sqr
	.long	8
	.long	8
	.long	0
	.long	2
	.align 16
	.type	_bignum_nist_p_224_sqr.7215, @object
	.size	_bignum_nist_p_224_sqr.7215, 24
_bignum_nist_p_224_sqr.7215:
	.quad	_nist_p_224_sqr
	.long	7
	.long	7
	.long	0
	.long	2
	.align 16
	.type	_bignum_nist_p_192_sqr.7184, @object
	.size	_bignum_nist_p_192_sqr.7184, 24
_bignum_nist_p_192_sqr.7184:
	.quad	_nist_p_192_sqr
	.long	6
	.long	6
	.long	0
	.long	2
	.align 16
	.type	_bignum_nist_p_521, @object
	.size	_bignum_nist_p_521, 24
_bignum_nist_p_521:
	.quad	_nist_p_521
	.long	9
	.long	9
	.long	0
	.long	2
	.align 16
	.type	_bignum_nist_p_384, @object
	.size	_bignum_nist_p_384, 24
_bignum_nist_p_384:
	.quad	_nist_p_384
	.long	6
	.long	6
	.long	0
	.long	2
	.align 16
	.type	_bignum_nist_p_256, @object
	.size	_bignum_nist_p_256, 24
_bignum_nist_p_256:
	.quad	_nist_p_256
	.long	4
	.long	4
	.long	0
	.long	2
	.align 16
	.type	_bignum_nist_p_224, @object
	.size	_bignum_nist_p_224, 24
_bignum_nist_p_224:
	.quad	_nist_p_224
	.long	4
	.long	4
	.long	0
	.long	2
	.align 16
	.type	_bignum_nist_p_192, @object
	.size	_bignum_nist_p_192, 24
_bignum_nist_p_192:
	.quad	_nist_p_192
	.long	3
	.long	3
	.long	0
	.long	2
	.section	.rodata
	.align 32
	.type	_nist_p_521_sqr, @object
	.size	_nist_p_521_sqr, 136
_nist_p_521_sqr:
	.quad	1
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-1024
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	262143
	.align 32
	.type	_nist_p_521, @object
	.size	_nist_p_521, 72
_nist_p_521:
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	511
	.align 32
	.type	_nist_p_384_sqr, @object
	.size	_nist_p_384_sqr, 96
_nist_p_384_sqr:
	.quad	-8589934591
	.quad	8589934592
	.quad	-8589934592
	.quad	8589934592
	.quad	1
	.quad	0
	.quad	8589934590
	.quad	-8589934592
	.quad	-3
	.quad	-1
	.quad	-1
	.quad	-1
	.align 32
	.type	_nist_p_384, @object
	.size	_nist_p_384, 240
_nist_p_384:
	.quad	4294967295
	.quad	-4294967296
	.quad	-2
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	8589934590
	.quad	-8589934592
	.quad	-3
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	12884901885
	.quad	-12884901888
	.quad	-4
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	17179869180
	.quad	-17179869184
	.quad	-5
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	21474836475
	.quad	-21474836480
	.quad	-6
	.quad	-1
	.quad	-1
	.quad	-1
	.align 32
	.type	_nist_p_256_sqr, @object
	.size	_nist_p_256_sqr, 64
_nist_p_256_sqr:
	.quad	1
	.quad	-8589934592
	.quad	-1
	.quad	8589934590
	.quad	8589934590
	.quad	8589934590
	.quad	-8589934591
	.quad	-8589934590
	.align 32
	.type	_nist_p_256, @object
	.size	_nist_p_256, 160
_nist_p_256:
	.quad	-1
	.quad	4294967295
	.quad	0
	.quad	-4294967295
	.quad	-2
	.quad	8589934591
	.quad	0
	.quad	-8589934590
	.quad	-3
	.quad	12884901887
	.quad	0
	.quad	-12884901885
	.quad	-4
	.quad	17179869183
	.quad	0
	.quad	-17179869180
	.quad	-5
	.quad	21474836479
	.quad	0
	.quad	-21474836475
	.align 32
	.type	_nist_p_224_sqr, @object
	.size	_nist_p_224_sqr, 56
_nist_p_224_sqr:
	.quad	1
	.quad	-8589934592
	.quad	-1
	.quad	8589934592
	.quad	0
	.quad	-2
	.quad	-1
	.align 32
	.type	_nist_p_224, @object
	.size	_nist_p_224, 64
_nist_p_224:
	.quad	1
	.quad	-4294967296
	.quad	-1
	.quad	4294967295
	.quad	2
	.quad	-8589934592
	.quad	-1
	.quad	8589934591
	.align 32
	.type	_nist_p_192_sqr, @object
	.size	_nist_p_192_sqr, 48
_nist_p_192_sqr:
	.quad	1
	.quad	2
	.quad	1
	.quad	-2
	.quad	-3
	.quad	-1
	.align 32
	.type	_nist_p_192, @object
	.size	_nist_p_192, 72
_nist_p_192:
	.quad	-1
	.quad	-2
	.quad	-1
	.quad	-2
	.quad	-3
	.quad	-1
	.quad	-3
	.quad	-4
	.quad	-1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
