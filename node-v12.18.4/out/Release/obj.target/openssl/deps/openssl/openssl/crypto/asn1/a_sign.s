	.file	"a_sign.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/asn1/a_sign.c"
	.text
	.p2align 4
	.globl	ASN1_sign
	.type	ASN1_sign, @function
ASN1_sign:
.LFB829:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -96(%rbp)
	movq	16(%rbp), %rbx
	movq	%rcx, -120(%rbp)
	movq	%r8, -104(%rbp)
	movq	%r9, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_MD_CTX_new@PLT
	movl	$0, -68(%rbp)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2
	testq	%r14, %r14
	je	.L3
	testq	%r12, %r12
	je	.L4
	movl	$0, -88(%rbp)
.L12:
	movl	-88(%rbp), %esi
	movq	%r14, %r15
	testl	%esi, %esi
	cmovne	%r12, %r15
	cmpl	$113, 4(%rbx)
	movq	8(%r15), %rdi
	je	.L79
	testq	%rdi, %rdi
	je	.L8
	cmpl	$5, (%rdi)
	je	.L7
.L8:
	call	ASN1_TYPE_free@PLT
	call	ASN1_TYPE_new@PLT
	movq	%rax, 8(%r15)
	testq	%rax, %rax
	je	.L76
	movl	$5, (%rax)
.L7:
	movq	(%r15), %rdi
	call	ASN1_OBJECT_free@PLT
	movl	4(%rbx), %edi
	call	OBJ_nid2obj@PLT
	movq	%rax, (%r15)
	testq	%rax, %rax
	je	.L10
	movl	20(%rax), %ecx
	testl	%ecx, %ecx
	je	.L11
	cmpl	$1, -88(%rbp)
	jne	.L80
.L13:
	movq	-104(%rbp), %rdi
	movq	-96(%rbp), %rax
	xorl	%esi, %esi
	call	*%rax
	movl	$75, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$68, %edx
	testl	%eax, %eax
	jle	.L77
	movslq	%eax, %r12
	movl	$79, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_malloc@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r15
	call	EVP_PKEY_size@PLT
	movl	$81, %edx
	leaq	.LC0(%rip), %rsi
	movslq	%eax, %r14
	movl	%eax, -68(%rbp)
	movq	%r14, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, -88(%rbp)
	testq	%r15, %r15
	je	.L35
	testq	%rax, %rax
	je	.L35
	movq	-104(%rbp), %rdi
	leaq	-64(%rbp), %rsi
	movq	-96(%rbp), %rax
	movq	%r15, -64(%rbp)
	call	*%rax
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	je	.L31
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jne	.L81
.L31:
	movl	$95, %r8d
	movl	$6, %edx
	movl	$128, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	movl	$0, -68(%rbp)
	call	ERR_put_error@PLT
.L17:
	movq	%r13, %rdi
	call	EVP_MD_CTX_free@PLT
	movl	$110, %ecx
	movq	%r12, %rsi
	movq	%r15, %rdi
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movq	-88(%rbp), %rdi
	movl	$111, %ecx
	movq	%r14, %rsi
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movl	-68(%rbp), %eax
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L82
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	movl	$1, -88(%rbp)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L10:
	movl	$64, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$162, %edx
	.p2align 4,,10
	.p2align 3
.L77:
	movl	$128, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
.L76:
	movq	$0, -88(%rbp)
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L81:
	movq	-112(%rbp), %rcx
	movq	-88(%rbp), %rsi
	leaq	-68(%rbp), %rdx
	movq	%r13, %rdi
	call	EVP_SignFinal@PLT
	testl	%eax, %eax
	je	.L31
	movq	-120(%rbp), %rbx
	movl	$98, %edx
	leaq	.LC0(%rip), %rsi
	movq	8(%rbx), %rdi
	call	CRYPTO_free@PLT
	movq	-88(%rbp), %rax
	movq	$0, -88(%rbp)
	movq	%rax, 8(%rbx)
	movl	-68(%rbp), %eax
	movl	%eax, (%rbx)
	movq	16(%rbx), %rax
	andq	$-16, %rax
	orq	$8, %rax
	movq	%rax, 16(%rbx)
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L35:
	movl	$84, %r8d
	movl	$65, %edx
	movl	$128, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	movl	$0, -68(%rbp)
	call	ERR_put_error@PLT
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L79:
	call	ASN1_TYPE_free@PLT
	movq	$0, 8(%r15)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L3:
	testq	%r12, %r12
	je	.L13
	movl	$1, %r14d
	cmpl	$1, %r14d
	je	.L18
.L72:
	cmpl	$113, 4(%rbx)
	movq	8(%r12), %rdi
	je	.L83
	testq	%rdi, %rdi
	je	.L25
	cmpl	$5, (%rdi)
	je	.L24
.L25:
	call	ASN1_TYPE_free@PLT
	call	ASN1_TYPE_new@PLT
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L76
	movl	$5, (%rax)
.L24:
	movq	(%r12), %rdi
	call	ASN1_OBJECT_free@PLT
	movl	4(%rbx), %edi
	call	OBJ_nid2obj@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L10
	movl	20(%rax), %eax
	testl	%eax, %eax
	je	.L11
	cmpl	$2, %r14d
	je	.L13
.L18:
	addl	$1, %r14d
	cmpl	$1, %r14d
	jne	.L72
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L4:
	cmpl	$113, 4(%rbx)
	movq	8(%r14), %rdi
	je	.L84
	testq	%rdi, %rdi
	je	.L16
	cmpl	$5, (%rdi)
	je	.L15
.L16:
	call	ASN1_TYPE_free@PLT
	call	ASN1_TYPE_new@PLT
	movq	%rax, 8(%r14)
	testq	%rax, %rax
	je	.L76
	movl	$5, (%rax)
.L15:
	movq	(%r14), %rdi
	call	ASN1_OBJECT_free@PLT
	movl	4(%rbx), %edi
	call	OBJ_nid2obj@PLT
	movq	%rax, (%r14)
	testq	%rax, %rax
	je	.L10
	movl	20(%rax), %edx
	testl	%edx, %edx
	jne	.L13
	.p2align 4,,10
	.p2align 3
.L11:
	movl	$68, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$154, %edx
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L2:
	movl	$65, %edx
	movl	$128, %esi
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	movl	$37, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$13, %edi
	xorl	%r12d, %r12d
	call	ERR_put_error@PLT
	movq	$0, -88(%rbp)
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L83:
	call	ASN1_TYPE_free@PLT
	movq	$0, 8(%r12)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L84:
	call	ASN1_TYPE_free@PLT
	movq	$0, 8(%r14)
	jmp	.L15
.L82:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE829:
	.size	ASN1_sign, .-ASN1_sign
	.p2align 4
	.globl	ASN1_item_sign_ctx
	.type	ASN1_item_sign_ctx, @function
ASN1_item_sign_ctx:
.LFB831:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%r9, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$72, %rsp
	movq	%rsi, -88(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	call	EVP_MD_CTX_md@PLT
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	EVP_MD_CTX_pkey_ctx@PLT
	movq	%rax, %rdi
	call	EVP_PKEY_CTX_get0_pkey@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L120
	movq	16(%rax), %rax
	movl	$159, %r8d
	movl	$198, %edx
	leaq	.LC0(%rip), %rcx
	testq	%rax, %rax
	je	.L118
	movq	208(%rax), %rax
	testq	%rax, %rax
	je	.L94
	movq	-96(%rbp), %r8
	movq	-88(%rbp), %rcx
	movq	%rbx, %r9
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*%rax
	cmpl	$1, %eax
	je	.L121
	testl	%eax, %eax
	jle	.L122
	cmpl	$2, %eax
	je	.L94
.L95:
	leaq	-72(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	ASN1_item_i2d@PLT
	testl	%eax, %eax
	jle	.L123
	movq	%r15, %rdi
	movslq	%eax, %r13
	call	EVP_PKEY_size@PLT
	movl	$215, %edx
	leaq	.LC0(%rip), %rsi
	movslq	%eax, %r14
	movq	%r14, %rdi
	movq	%r14, -64(%rbp)
	call	CRYPTO_malloc@PLT
	movq	-72(%rbp), %rcx
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L106
	testq	%rcx, %rcx
	je	.L106
	leaq	-64(%rbp), %rdx
	movq	%r13, %r8
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	EVP_DigestSign@PLT
	testl	%eax, %eax
	je	.L124
	movq	8(%rbx), %rdi
	movl	$227, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-64(%rbp), %rax
	movq	%r15, 8(%rbx)
	xorl	%r15d, %r15d
	movl	%eax, (%rbx)
	movq	16(%rbx), %rax
	andq	$-16, %rax
	orq	$8, %rax
	movq	%rax, 16(%rbx)
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L94:
	cmpq	$0, -104(%rbp)
	je	.L125
	movq	16(%r15), %rax
	movq	-104(%rbp), %rdi
	movl	(%rax), %edx
	movl	%edx, -108(%rbp)
	call	EVP_MD_type@PLT
	movl	-108(%rbp), %edx
	leaq	-76(%rbp), %rdi
	movl	%eax, %esi
	call	OBJ_find_sigid_by_algs@PLT
	testl	%eax, %eax
	je	.L126
	movq	16(%r15), %rax
	movq	8(%rax), %rax
	andl	$4, %eax
	cmpq	$1, %rax
	sbbl	%edx, %edx
	orl	$5, %edx
	cmpq	$0, -88(%rbp)
	je	.L98
	movl	-76(%rbp), %edi
	movl	%edx, -104(%rbp)
	call	OBJ_nid2obj@PLT
	movl	-104(%rbp), %edx
	movq	-88(%rbp), %rdi
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	call	X509_ALGOR_set0@PLT
	movl	-104(%rbp), %edx
.L98:
	cmpq	$0, -96(%rbp)
	movl	%edx, -88(%rbp)
	je	.L95
	movl	-76(%rbp), %edi
	call	OBJ_nid2obj@PLT
	movl	-88(%rbp), %edx
	movq	-96(%rbp), %rdi
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	call	X509_ALGOR_set0@PLT
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L121:
	movslq	(%rbx), %rax
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	movq	%rax, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L87:
	movq	-72(%rbp), %rdi
	movl	$238, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r13, %rsi
	call	CRYPTO_clear_free@PLT
	movl	$239, %ecx
	movq	%r14, %rsi
	movq	%r15, %rdi
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L127
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	movl	$224, %r8d
	movl	$6, %edx
	movl	$220, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	movq	$0, -64(%rbp)
	call	ERR_put_error@PLT
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L125:
	movl	$184, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$217, %edx
.L118:
	movl	$220, %esi
	movl	$13, %edi
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	call	ERR_put_error@PLT
	xorl	%r13d, %r13d
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L106:
	movl	$218, %r8d
	movl	$65, %edx
	movl	$220, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	movq	$0, -64(%rbp)
	call	ERR_put_error@PLT
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L122:
	movl	$175, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$6, %edx
.L119:
	movl	$220, %esi
	movl	$13, %edi
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	call	ERR_put_error@PLT
	xorl	%r15d, %r15d
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L123:
	movq	$0, -64(%rbp)
	movl	$210, %r8d
	movl	$68, %edx
	leaq	.LC0(%rip), %rcx
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L120:
	movl	$217, %edx
	movl	$220, %esi
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	movl	$154, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$13, %edi
	call	ERR_put_error@PLT
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L126:
	movl	$190, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$198, %edx
	jmp	.L119
.L127:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE831:
	.size	ASN1_item_sign_ctx, .-ASN1_item_sign_ctx
	.p2align 4
	.globl	ASN1_item_sign
	.type	ASN1_item_sign, @function
ASN1_item_sign:
.LFB830:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$24, %rsp
	movq	%rdi, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	EVP_MD_CTX_new@PLT
	movq	-56(%rbp), %r8
	testq	%rax, %rax
	je	.L134
	movq	16(%rbp), %rdx
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	EVP_DigestSignInit@PLT
	testl	%eax, %eax
	je	.L133
	movq	-64(%rbp), %rdi
	movq	%r12, %r9
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	ASN1_item_sign_ctx
.L133:
	movq	%r12, %rdi
	movl	%eax, -56(%rbp)
	call	EVP_MD_CTX_free@PLT
	movl	-56(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	movl	$125, %r8d
	movl	$65, %edx
	movl	$195, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE830:
	.size	ASN1_item_sign, .-ASN1_item_sign
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
