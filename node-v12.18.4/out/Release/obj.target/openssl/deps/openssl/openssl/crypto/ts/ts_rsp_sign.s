	.file	"ts_rsp_sign.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ts/ts_rsp_sign.c"
	.text
	.p2align 4
	.type	ess_CERT_ID_new_init, @function
ess_CERT_ID_new_init:
.LFB1411:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	%esi, %ebx
	movl	$-1, %esi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	X509_check_purpose@PLT
	call	ESS_CERT_ID_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L4
	call	EVP_sha1@PLT
	leaq	-64(%rbp), %r14
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	movq	%rax, %rsi
	movq	%r14, %rdx
	call	X509_digest@PLT
	testl	%eax, %eax
	je	.L4
	movq	(%r12), %rdi
	movl	$20, %edx
	movq	%r14, %rsi
	call	ASN1_OCTET_STRING_set@PLT
	testl	%eax, %eax
	je	.L4
	testl	%ebx, %ebx
	je	.L1
	cmpq	$0, 8(%r12)
	je	.L6
.L8:
	call	GENERAL_NAME_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L4
	movl	$4, (%rax)
	movq	%r13, %rdi
	call	X509_get_issuer_name@PLT
	movq	%rax, %rdi
	call	X509_NAME_dup@PLT
	movq	%rax, 8(%r14)
	testq	%rax, %rax
	je	.L3
	movq	8(%r12), %rax
	movq	%r14, %rsi
	movq	(%rax), %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L3
	movq	8(%r12), %rax
	movq	8(%rax), %rdi
	call	ASN1_INTEGER_free@PLT
	movq	%r13, %rdi
	call	X509_get_serialNumber@PLT
	movq	8(%r12), %rbx
	movq	%rax, %rdi
	call	ASN1_INTEGER_dup@PLT
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	jne	.L1
	.p2align 4,,10
	.p2align 3
.L4:
	xorl	%r14d, %r14d
.L3:
	movq	%r14, %rdi
	call	GENERAL_NAME_free@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	ESS_CERT_ID_free@PLT
	movl	$802, %r8d
	movl	$65, %edx
	leaq	.LC0(%rip), %rcx
	movl	$113, %esi
	movl	$47, %edi
	call	ERR_put_error@PLT
.L1:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L37
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	call	ESS_ISSUER_SERIAL_new@PLT
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	jne	.L8
	jmp	.L4
.L37:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1411:
	.size	ess_CERT_ID_new_init, .-ess_CERT_ID_new_init
	.p2align 4
	.type	ess_cert_id_v2_new_init, @function
ess_cert_id_v2_new_init:
.LFB1415:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -148(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -128(%rbp)
	movl	$64, -132(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	ESS_CERT_ID_V2_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L42
	call	EVP_sha256@PLT
	cmpq	%rax, %r13
	je	.L41
	call	X509_ALGOR_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L42
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	X509_ALGOR_set_md@PLT
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	je	.L40
	movq	%r14, (%r12)
.L43:
	leaq	-128(%rbp), %r14
	leaq	-132(%rbp), %rcx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%r14, %rdx
	call	X509_digest@PLT
	testl	%eax, %eax
	je	.L42
	movq	8(%r12), %rdi
	movl	-132(%rbp), %edx
	movq	%r14, %rsi
	call	ASN1_OCTET_STRING_set@PLT
	testl	%eax, %eax
	je	.L42
	movl	-148(%rbp), %eax
	testl	%eax, %eax
	je	.L38
	call	ESS_ISSUER_SERIAL_new@PLT
	movq	%rax, 16(%r12)
	testq	%rax, %rax
	je	.L42
	call	GENERAL_NAME_new@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L42
	movl	$4, (%rax)
	movq	%r15, %rdi
	call	X509_get_issuer_name@PLT
	movq	%rax, %rdi
	call	X509_NAME_dup@PLT
	movq	%rax, 8(%rbx)
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L40
	movq	16(%r12), %rax
	movq	%rbx, %rsi
	movq	(%rax), %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L46
	movq	16(%r12), %rax
	movq	8(%rax), %rdi
	call	ASN1_INTEGER_free@PLT
	movq	%r15, %rdi
	call	X509_get_serialNumber@PLT
	movq	16(%r12), %rbx
	movq	%rax, %rdi
	call	ASN1_INTEGER_dup@PLT
	movq	%rax, 8(%rbx)
	movq	16(%r12), %rax
	cmpq	$0, 8(%rax)
	jne	.L38
	.p2align 4,,10
	.p2align 3
.L42:
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
.L40:
	movq	%r14, %rdi
	call	X509_ALGOR_free@PLT
	movq	%rbx, %rdi
	call	GENERAL_NAME_free@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	ESS_CERT_ID_V2_free@PLT
	movl	$952, %r8d
	movl	$65, %edx
	leaq	.LC0(%rip), %rcx
	movl	$156, %esi
	movl	$47, %edi
	call	ERR_put_error@PLT
.L38:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L72
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movq	$0, (%r12)
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L46:
	xorl	%r14d, %r14d
	jmp	.L40
.L72:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1415:
	.size	ess_cert_id_v2_new_init, .-ess_cert_id_v2_new_init
	.p2align 4
	.globl	TS_RESP_CTX_new
	.type	TS_RESP_CTX_new, @function
TS_RESP_CTX_new:
.LFB1382:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$124, %edx
	movl	$168, %edi
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L77
	call	EVP_sha256@PLT
	movq	%rax, 16(%r12)
	leaq	def_serial_cb(%rip), %rax
	movq	%rax, 96(%r12)
	leaq	def_time_cb(%rip), %rax
	movq	%rax, 112(%r12)
	leaq	def_extension_cb(%rip), %rax
	movq	%rax, 128(%r12)
.L73:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	movl	$125, %r8d
	movl	$65, %edx
	movl	$127, %esi
	movl	$47, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L73
	.cfi_endproc
.LFE1382:
	.size	TS_RESP_CTX_new, .-TS_RESP_CTX_new
	.p2align 4
	.globl	TS_RESP_CTX_free
	.type	TS_RESP_CTX_free, @function
TS_RESP_CTX_free:
.LFB1383:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L78
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	X509_free@PLT
	movq	8(%r12), %rdi
	call	EVP_PKEY_free@PLT
	movq	32(%r12), %rdi
	movq	X509_free@GOTPCREL(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	ASN1_OBJECT_free@GOTPCREL(%rip), %rsi
	movq	40(%r12), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	48(%r12), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	56(%r12), %rdi
	call	OPENSSL_sk_free@PLT
	movq	64(%r12), %rdi
	call	ASN1_INTEGER_free@PLT
	movq	72(%r12), %rdi
	call	ASN1_INTEGER_free@PLT
	movq	80(%r12), %rdi
	call	ASN1_INTEGER_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$152, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L78:
	ret
	.cfi_endproc
.LFE1383:
	.size	TS_RESP_CTX_free, .-TS_RESP_CTX_free
	.p2align 4
	.globl	TS_RESP_CTX_set_signer_cert
	.type	TS_RESP_CTX_set_signer_cert, @function
TS_RESP_CTX_set_signer_cert:
.LFB1384:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movl	$9, %esi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$8, %rsp
	call	X509_check_purpose@PLT
	cmpl	$1, %eax
	je	.L84
	movl	$158, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$117, %edx
	xorl	%r12d, %r12d
	movl	$131, %esi
	movl	$47, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	movq	(%rbx), %rdi
	movl	%eax, %r12d
	call	X509_free@PLT
	movq	%r13, (%rbx)
	movq	%r13, %rdi
	call	X509_up_ref@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1384:
	.size	TS_RESP_CTX_set_signer_cert, .-TS_RESP_CTX_set_signer_cert
	.p2align 4
	.globl	TS_RESP_CTX_set_signer_key
	.type	TS_RESP_CTX_set_signer_key, @function
TS_RESP_CTX_set_signer_key:
.LFB1385:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	call	EVP_PKEY_free@PLT
	movq	%r12, 8(%rbx)
	movq	%r12, %rdi
	call	EVP_PKEY_up_ref@PLT
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1385:
	.size	TS_RESP_CTX_set_signer_key, .-TS_RESP_CTX_set_signer_key
	.p2align 4
	.globl	TS_RESP_CTX_set_signer_digest
	.type	TS_RESP_CTX_set_signer_digest, @function
TS_RESP_CTX_set_signer_digest:
.LFB1386:
	.cfi_startproc
	endbr64
	movq	%rsi, 16(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1386:
	.size	TS_RESP_CTX_set_signer_digest, .-TS_RESP_CTX_set_signer_digest
	.p2align 4
	.globl	TS_RESP_CTX_set_def_policy
	.type	TS_RESP_CTX_set_def_policy, @function
TS_RESP_CTX_set_def_policy:
.LFB1387:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	48(%rdi), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	%r12, %rdi
	call	OBJ_dup@PLT
	movl	$1, %r8d
	movq	%rax, 48(%rbx)
	testq	%rax, %rax
	je	.L95
.L90:
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	movl	$190, %r8d
	movl	$65, %edx
	movl	$130, %esi
	movl	$47, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r8d, %r8d
	jmp	.L90
	.cfi_endproc
.LFE1387:
	.size	TS_RESP_CTX_set_def_policy, .-TS_RESP_CTX_set_def_policy
	.p2align 4
	.globl	TS_RESP_CTX_set_certs
	.type	TS_RESP_CTX_set_certs, @function
TS_RESP_CTX_set_certs:
.LFB1388:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	movq	X509_free@GOTPCREL(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	32(%rdi), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	$0, 32(%rbx)
	testq	%r12, %r12
	je	.L99
	movq	%r12, %rdi
	call	X509_chain_up_ref@PLT
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	je	.L101
.L99:
	movl	$1, %eax
.L96:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	movl	$202, %r8d
	movl	$65, %edx
	movl	$129, %esi
	movl	$47, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L96
	.cfi_endproc
.LFE1388:
	.size	TS_RESP_CTX_set_certs, .-TS_RESP_CTX_set_certs
	.p2align 4
	.globl	TS_RESP_CTX_add_policy
	.type	TS_RESP_CTX_add_policy, @function
TS_RESP_CTX_add_policy:
.LFB1389:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpq	$0, 40(%rdi)
	movq	%rdi, %rbx
	je	.L103
.L106:
	movq	%r12, %rdi
	call	OBJ_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L104
	movq	40(%rbx), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	movl	%eax, %r8d
	movl	$1, %eax
	testl	%r8d, %r8d
	je	.L107
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 40(%rbx)
	testq	%rax, %rax
	jne	.L106
	.p2align 4,,10
	.p2align 3
.L104:
	xorl	%r12d, %r12d
.L107:
	movl	$223, %r8d
	movl	$65, %edx
	movl	$126, %esi
	movl	$47, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdi
	call	ASN1_OBJECT_free@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1389:
	.size	TS_RESP_CTX_add_policy, .-TS_RESP_CTX_add_policy
	.p2align 4
	.globl	TS_RESP_CTX_add_md
	.type	TS_RESP_CTX_add_md, @function
TS_RESP_CTX_add_md:
.LFB1390:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	56(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L123
.L116:
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L118
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore_state
	movq	%rsi, -24(%rbp)
	call	OPENSSL_sk_new_null@PLT
	movq	-24(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, 56(%rbx)
	movq	%rax, %rdi
	jne	.L116
.L118:
	movl	$238, %r8d
	movl	$65, %edx
	movl	$125, %esi
	movl	$47, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1390:
	.size	TS_RESP_CTX_add_md, .-TS_RESP_CTX_add_md
	.p2align 4
	.globl	TS_RESP_CTX_set_accuracy
	.type	TS_RESP_CTX_set_accuracy, @function
TS_RESP_CTX_set_accuracy:
.LFB1391:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	64(%rdi), %rdi
	call	ASN1_INTEGER_free@PLT
	movq	$0, 64(%rbx)
	movq	72(%rbx), %rdi
	call	ASN1_INTEGER_free@PLT
	movq	$0, 72(%rbx)
	movq	80(%rbx), %rdi
	call	ASN1_INTEGER_free@PLT
	movq	$0, 80(%rbx)
	testl	%r12d, %r12d
	jne	.L145
	testl	%r13d, %r13d
	jne	.L146
.L127:
	testl	%r14d, %r14d
	jne	.L129
.L132:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	call	ASN1_INTEGER_new@PLT
	movq	%rax, 80(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L143
	movslq	%r14d, %rsi
	call	ASN1_INTEGER_set@PLT
	testl	%eax, %eax
	jne	.L132
	.p2align 4,,10
	.p2align 3
.L143:
	movq	64(%rbx), %rdi
.L126:
	call	ASN1_INTEGER_free@PLT
	movq	$0, 64(%rbx)
	movq	72(%rbx), %rdi
	call	ASN1_INTEGER_free@PLT
	movq	$0, 72(%rbx)
	movq	80(%rbx), %rdi
	call	ASN1_INTEGER_free@PLT
	movl	$271, %r8d
	movl	$65, %edx
	movq	$0, 80(%rbx)
	leaq	.LC0(%rip), %rcx
	movl	$128, %esi
	movl	$47, %edi
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	call	ASN1_INTEGER_new@PLT
	movq	%rax, 64(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L126
	movslq	%r12d, %rsi
	call	ASN1_INTEGER_set@PLT
	testl	%eax, %eax
	je	.L143
	testl	%r13d, %r13d
	je	.L127
	.p2align 4,,10
	.p2align 3
.L146:
	call	ASN1_INTEGER_new@PLT
	movq	%rax, 72(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L143
	movslq	%r13d, %rsi
	call	ASN1_INTEGER_set@PLT
	testl	%eax, %eax
	jne	.L127
	jmp	.L143
	.cfi_endproc
.LFE1391:
	.size	TS_RESP_CTX_set_accuracy, .-TS_RESP_CTX_set_accuracy
	.p2align 4
	.globl	TS_RESP_CTX_add_flags
	.type	TS_RESP_CTX_add_flags, @function
TS_RESP_CTX_add_flags:
.LFB1392:
	.cfi_startproc
	endbr64
	orl	%esi, 92(%rdi)
	ret
	.cfi_endproc
.LFE1392:
	.size	TS_RESP_CTX_add_flags, .-TS_RESP_CTX_add_flags
	.p2align 4
	.globl	TS_RESP_CTX_set_serial_cb
	.type	TS_RESP_CTX_set_serial_cb, @function
TS_RESP_CTX_set_serial_cb:
.LFB1393:
	.cfi_startproc
	endbr64
	movq	%rsi, 96(%rdi)
	movq	%rdx, 104(%rdi)
	ret
	.cfi_endproc
.LFE1393:
	.size	TS_RESP_CTX_set_serial_cb, .-TS_RESP_CTX_set_serial_cb
	.p2align 4
	.globl	TS_RESP_CTX_set_time_cb
	.type	TS_RESP_CTX_set_time_cb, @function
TS_RESP_CTX_set_time_cb:
.LFB1394:
	.cfi_startproc
	endbr64
	movq	%rsi, 112(%rdi)
	movq	%rdx, 120(%rdi)
	ret
	.cfi_endproc
.LFE1394:
	.size	TS_RESP_CTX_set_time_cb, .-TS_RESP_CTX_set_time_cb
	.p2align 4
	.globl	TS_RESP_CTX_set_extension_cb
	.type	TS_RESP_CTX_set_extension_cb, @function
TS_RESP_CTX_set_extension_cb:
.LFB1395:
	.cfi_startproc
	endbr64
	movq	%rsi, 128(%rdi)
	movq	%rdx, 136(%rdi)
	ret
	.cfi_endproc
.LFE1395:
	.size	TS_RESP_CTX_set_extension_cb, .-TS_RESP_CTX_set_extension_cb
	.p2align 4
	.globl	TS_RESP_CTX_set_status_info
	.type	TS_RESP_CTX_set_status_info, @function
TS_RESP_CTX_set_status_info:
.LFB1396:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$8, %rsp
	call	TS_STATUS_INFO_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L154
	movq	(%rax), %rdi
	movslq	%ebx, %rsi
	call	ASN1_INTEGER_set@PLT
	testl	%eax, %eax
	je	.L154
	testq	%r13, %r13
	je	.L158
	call	ASN1_UTF8STRING_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L154
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	ASN1_STRING_set@PLT
	testl	%eax, %eax
	je	.L153
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L175
.L157:
	movq	%r14, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L153
.L158:
	movq	152(%r15), %rdi
	movq	%r12, %rsi
	call	TS_RESP_set_status_info@PLT
	testl	%eax, %eax
	je	.L154
	movl	$1, %r13d
	xorl	%r14d, %r14d
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L154:
	xorl	%r14d, %r14d
.L153:
	movl	$326, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$132, %esi
	movl	$47, %edi
	call	ERR_put_error@PLT
.L156:
	movq	%r12, %rdi
	call	TS_STATUS_INFO_free@PLT
	movq	%r14, %rdi
	call	ASN1_UTF8STRING_free@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L175:
	.cfi_restore_state
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 8(%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L157
	jmp	.L153
	.cfi_endproc
.LFE1396:
	.size	TS_RESP_CTX_set_status_info, .-TS_RESP_CTX_set_status_info
	.section	.rodata.str1.8
	.align 8
.LC1:
	.string	"Error during serial number generation."
	.text
	.p2align 4
	.type	def_serial_cb, @function
def_serial_cb:
.LFB1379:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	call	ASN1_INTEGER_new@PLT
	testq	%rax, %rax
	je	.L179
	movl	$1, %esi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	ASN1_INTEGER_set@PLT
	testl	%eax, %eax
	je	.L179
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	.cfi_restore_state
	movl	$63, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$110, %esi
	movl	$47, %edi
	call	ERR_put_error@PLT
	movq	%r13, %rdi
	movl	$2, %esi
	leaq	.LC1(%rip), %rdx
	call	TS_RESP_CTX_set_status_info
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1379:
	.size	def_serial_cb, .-def_serial_cb
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	"Unsupported extension."
	.text
	.p2align 4
	.type	def_extension_cb, @function
def_extension_cb:
.LFB1381:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC2(%rip), %rdx
	movl	$2, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	TS_RESP_CTX_set_status_info
	movq	152(%rbx), %rax
	movq	(%rax), %rbx
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L192
.L185:
	movl	$1, %edx
	movl	$16, %esi
	call	ASN1_BIT_STRING_set_bit@PLT
	testl	%eax, %eax
	je	.L187
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L192:
	.cfi_restore_state
	call	ASN1_BIT_STRING_new@PLT
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L185
.L187:
	movl	$354, %r8d
	movl	$65, %edx
	movl	$124, %esi
	movl	$47, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1381:
	.size	def_extension_cb, .-def_extension_cb
	.section	.rodata.str1.1
.LC3:
	.string	"Time is not available."
	.text
	.p2align 4
	.type	def_time_cb, @function
def_time_cb:
.LFB1380:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	-64(%rbp), %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	gettimeofday@PLT
	testl	%eax, %eax
	jne	.L203
	movq	-64(%rbp), %rax
	movq	%rax, (%rbx)
	movq	-56(%rbp), %rax
	movq	%rax, 0(%r13)
	movl	$1, %eax
.L193:
	movq	-40(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L204
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L203:
	.cfi_restore_state
	movl	$76, %r8d
	movl	$122, %edx
	movl	$111, %esi
	movl	$47, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	leaq	.LC3(%rip), %rdx
	call	TS_RESP_CTX_set_status_info
	movq	152(%r12), %rax
	movq	(%rax), %rbx
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L205
.L195:
	movl	$1, %edx
	movl	$14, %esi
	call	ASN1_BIT_STRING_set_bit@PLT
	testl	%eax, %eax
	je	.L197
	xorl	%eax, %eax
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L205:
	call	ASN1_BIT_STRING_new@PLT
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L195
	.p2align 4,,10
	.p2align 3
.L197:
	movl	$354, %r8d
	movl	$65, %edx
	movl	$124, %esi
	movl	$47, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L193
.L204:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1380:
	.size	def_time_cb, .-def_time_cb
	.p2align 4
	.globl	TS_RESP_CTX_set_status_info_cond
	.type	TS_RESP_CTX_set_status_info_cond, @function
TS_RESP_CTX_set_status_info_cond:
.LFB1397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	152(%rdi), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	ASN1_INTEGER_get@PLT
	testq	%rax, %rax
	je	.L209
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r14, %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	TS_RESP_CTX_set_status_info
	.cfi_endproc
.LFE1397:
	.size	TS_RESP_CTX_set_status_info_cond, .-TS_RESP_CTX_set_status_info_cond
	.p2align 4
	.globl	TS_RESP_CTX_add_failure_info
	.type	TS_RESP_CTX_add_failure_info, @function
TS_RESP_CTX_add_failure_info:
.LFB1398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	152(%rdi), %rax
	movq	(%rax), %rbx
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L218
.L211:
	movl	$1, %edx
	call	ASN1_BIT_STRING_set_bit@PLT
	testl	%eax, %eax
	je	.L213
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L218:
	.cfi_restore_state
	movl	%esi, -20(%rbp)
	call	ASN1_BIT_STRING_new@PLT
	movl	-20(%rbp), %esi
	testq	%rax, %rax
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	jne	.L211
.L213:
	movl	$354, %r8d
	movl	$65, %edx
	movl	$124, %esi
	movl	$47, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1398:
	.size	TS_RESP_CTX_add_failure_info, .-TS_RESP_CTX_add_failure_info
	.p2align 4
	.globl	TS_RESP_CTX_get_request
	.type	TS_RESP_CTX_get_request, @function
TS_RESP_CTX_get_request:
.LFB1399:
	.cfi_startproc
	endbr64
	movq	144(%rdi), %rax
	ret
	.cfi_endproc
.LFE1399:
	.size	TS_RESP_CTX_get_request, .-TS_RESP_CTX_get_request
	.p2align 4
	.globl	TS_RESP_CTX_get_tst_info
	.type	TS_RESP_CTX_get_tst_info, @function
TS_RESP_CTX_get_tst_info:
.LFB1400:
	.cfi_startproc
	endbr64
	movq	160(%rdi), %rax
	ret
	.cfi_endproc
.LFE1400:
	.size	TS_RESP_CTX_get_tst_info, .-TS_RESP_CTX_get_tst_info
	.p2align 4
	.globl	TS_RESP_CTX_set_clock_precision_digits
	.type	TS_RESP_CTX_set_clock_precision_digits, @function
TS_RESP_CTX_set_clock_precision_digits:
.LFB1401:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$6, %esi
	ja	.L221
	movl	%esi, 88(%rdi)
	movl	$1, %eax
.L221:
	ret
	.cfi_endproc
.LFE1401:
	.size	TS_RESP_CTX_set_clock_precision_digits, .-TS_RESP_CTX_set_clock_precision_digits
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"Bad request format or system error."
	.section	.rodata.str1.1
.LC5:
	.string	"Bad request version."
	.section	.rodata.str1.8
	.align 8
.LC6:
	.string	"Superfluous message digest parameter."
	.section	.rodata.str1.1
.LC7:
	.string	"Bad message digest."
.LC8:
	.string	"%04d%02d%02d%02d%02d%02d"
.LC9:
	.string	".%06ld"
	.section	.rodata.str1.8
	.align 8
.LC10:
	.string	"Error during TSTInfo generation."
	.align 8
.LC11:
	.string	"Error during signature generation."
	.align 8
.LC12:
	.string	"Error during response generation."
	.align 8
.LC13:
	.string	"Requested policy is not supported."
	.align 8
.LC14:
	.string	"Message digest algorithm is not supported."
	.text
	.p2align 4
	.globl	TS_RESP_create_response
	.type	TS_RESP_create_response, @function
TS_RESP_create_response:
.LFB1402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movups	%xmm0, 144(%rdi)
	movq	$0, 160(%rdi)
	call	TS_RESP_new@PLT
	movq	%rax, 152(%rbx)
	testq	%rax, %rax
	je	.L529
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	d2i_TS_REQ_bio@PLT
	movq	%rax, 144(%rbx)
	testq	%rax, %rax
	je	.L530
	call	TS_STATUS_INFO_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L231
	movq	(%rax), %rdi
	xorl	%esi, %esi
	call	ASN1_INTEGER_set@PLT
	testl	%eax, %eax
	je	.L231
	movq	152(%rbx), %rdi
	movq	%r12, %rsi
	call	TS_RESP_set_status_info@PLT
	testl	%eax, %eax
	je	.L231
	movq	%r12, %rdi
	call	TS_STATUS_INFO_free@PLT
	xorl	%edi, %edi
	call	ASN1_UTF8STRING_free@PLT
	movq	144(%rbx), %r12
	movq	%r12, %rdi
	call	TS_REQ_get_version@PLT
	cmpq	$1, %rax
	je	.L531
	movq	%rbx, %rdi
	leaq	.LC5(%rip), %rdx
	movl	$2, %esi
	call	TS_RESP_CTX_set_status_info
	movq	152(%rbx), %rax
	movq	(%rax), %r12
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L532
.L234:
	movl	$1, %edx
	movl	$2, %esi
	call	ASN1_BIT_STRING_set_bit@PLT
	testl	%eax, %eax
	jne	.L226
.L229:
	movl	$354, %r8d
	movl	$65, %edx
	movl	$124, %esi
	movl	$47, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L231:
	movl	$326, %r8d
	movl	$65, %edx
	movl	$132, %esi
	movl	$47, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdi
	call	TS_STATUS_INFO_free@PLT
	xorl	%edi, %edi
	call	ASN1_UTF8STRING_free@PLT
.L226:
	movl	$412, %r8d
	movl	$121, %edx
	movl	$122, %esi
	movl	$47, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	152(%rbx), %r12
	testq	%r12, %r12
	je	.L317
	movq	(%r12), %rax
	movq	(%rax), %rdi
	call	ASN1_INTEGER_get@PLT
	testq	%rax, %rax
	je	.L319
.L523:
	movq	152(%rbx), %r12
.L317:
	movq	$0, 152(%rbx)
	movq	144(%rbx), %rdi
	call	TS_REQ_free@PLT
	movq	152(%rbx), %rdi
	movq	$0, 144(%rbx)
	call	TS_RESP_free@PLT
	movq	160(%rbx), %rdi
	movq	$0, 152(%rbx)
	call	TS_TST_INFO_free@PLT
	movq	$0, 160(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L533
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L531:
	.cfi_restore_state
	movq	8(%r12), %rax
	xorl	%r15d, %r15d
	movq	(%rax), %r14
	movq	%rax, -184(%rbp)
	movq	(%r14), %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %r13d
	.p2align 4,,10
	.p2align 3
.L236:
	movq	56(%rbx), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%r15d, %eax
	jle	.L534
	movq	56(%rbx), %rdi
	movl	%r15d, %esi
	addl	$1, %r15d
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	EVP_MD_type@PLT
	cmpl	%eax, %r13d
	jne	.L236
	testq	%r12, %r12
	je	.L236
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.L240
	call	ASN1_TYPE_get@PLT
	cmpl	$5, %eax
	jne	.L535
.L240:
	movq	-184(%rbp), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rax
	movl	(%rax), %r13d
	call	EVP_MD_size@PLT
	leaq	.LC7(%rip), %rdx
	cmpl	%eax, %r13d
	jne	.L528
	movq	144(%rbx), %rax
	movq	48(%rbx), %r12
	movq	16(%rax), %r14
	testq	%r12, %r12
	je	.L536
	testq	%r14, %r14
	je	.L245
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	OBJ_cmp@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.L332
	movq	48(%rbx), %r12
	testq	%r12, %r12
	jne	.L245
	.p2align 4,,10
	.p2align 3
.L247:
	movq	40(%rbx), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%r13d, %eax
	jle	.L537
	movq	40(%rbx), %rdi
	movl	%r13d, %esi
	addl	$1, %r13d
	call	OPENSSL_sk_value@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	%rax, %r12
	call	OBJ_cmp@PLT
	testl	%eax, %eax
	jne	.L247
	testq	%r12, %r12
	je	.L247
.L245:
	call	TS_TST_INFO_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L347
	movl	$1, %esi
	movq	%r13, %rdi
	call	TS_TST_INFO_set_version@PLT
	testl	%eax, %eax
	jne	.L251
.L347:
	xorl	%r15d, %r15d
.L517:
	xorl	%r9d, %r9d
.L519:
	xorl	%r14d, %r14d
.L518:
	xorl	%r12d, %r12d
.L252:
	movq	%r13, %rdi
	movq	%r9, -184(%rbp)
	call	TS_TST_INFO_free@PLT
	movl	$597, %r8d
	movl	$123, %edx
	leaq	.LC0(%rip), %rcx
	movl	$123, %esi
	movl	$47, %edi
	call	ERR_put_error@PLT
	movq	152(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	ASN1_INTEGER_get@PLT
	movq	-184(%rbp), %r9
	testq	%rax, %rax
	je	.L538
.L274:
	movq	%r12, %rdi
	movq	%r9, -184(%rbp)
	call	GENERAL_NAME_free@PLT
	movq	%r14, %rdi
	call	TS_ACCURACY_free@PLT
	movq	-184(%rbp), %r9
	movq	%r9, %rdi
	call	ASN1_GENERALIZEDTIME_free@PLT
	movq	%r15, %rdi
	call	ASN1_INTEGER_free@PLT
	movq	$0, 160(%rbx)
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L530:
	leaq	.LC4(%rip), %rdx
.L528:
	movq	%rbx, %rdi
	movl	$2, %esi
	call	TS_RESP_CTX_set_status_info
	movq	152(%rbx), %rax
	movq	(%rax), %r12
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L539
.L243:
	movl	$1, %edx
	movl	$5, %esi
	call	ASN1_BIT_STRING_set_bit@PLT
	testl	%eax, %eax
	jne	.L226
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L529:
	movl	$387, %r8d
	movl	$65, %edx
	movl	$122, %esi
	movl	$47, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L319:
	leaq	.LC12(%rip), %rdx
	movl	$2, %esi
	movq	%rbx, %rdi
	call	TS_RESP_CTX_set_status_info
	testl	%eax, %eax
	jne	.L523
	movq	152(%rbx), %rdi
	xorl	%r12d, %r12d
	call	TS_RESP_free@PLT
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L539:
	call	ASN1_BIT_STRING_new@PLT
	movq	%rax, 16(%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L243
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L332:
	xorl	%r13d, %r13d
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L251:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	TS_TST_INFO_set_policy_id@PLT
	testl	%eax, %eax
	je	.L347
	movq	144(%rbx), %rax
	movq	%r13, %rdi
	movq	8(%rax), %rsi
	call	TS_TST_INFO_set_msg_imprint@PLT
	testl	%eax, %eax
	je	.L347
	movq	104(%rbx), %rsi
	movq	%rbx, %rdi
	call	*96(%rbx)
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L347
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	TS_TST_INFO_set_serial@PLT
	testl	%eax, %eax
	je	.L517
	movq	120(%rbx), %rsi
	leaq	-160(%rbp), %rcx
	leaq	-168(%rbp), %rdx
	movq	%rbx, %rdi
	call	*112(%rbx)
	testl	%eax, %eax
	je	.L517
	movq	-160(%rbp), %rax
	movl	88(%rbx), %r14d
	movq	%rax, -184(%rbp)
	movq	-168(%rbp), %rax
	movq	%rax, -152(%rbp)
	cmpl	$6, %r14d
	ja	.L255
	leaq	-144(%rbp), %rax
	leaq	-152(%rbp), %rdi
	movq	%rax, %rsi
	movq	%rax, -208(%rbp)
	call	OPENSSL_gmtime@PLT
	testq	%rax, %rax
	je	.L255
	movl	(%rax), %esi
	subq	$8, %rsp
	movl	16(%rax), %edx
	leaq	-80(%rbp), %r12
	movl	20(%rax), %ecx
	movl	12(%rax), %r9d
	movq	%r12, %rdi
	pushq	%rsi
	movl	4(%rax), %esi
	leal	1(%rdx), %r8d
	leaq	.LC8(%rip), %rdx
	addl	$1900, %ecx
	pushq	%rsi
	movl	8(%rax), %eax
	movl	$23, %esi
	pushq	%rax
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	addq	$32, %rsp
	movslq	%eax, %r8
	addq	%r12, %r8
	testl	%r14d, %r14d
	jne	.L540
.L257:
	movl	$90, %edx
	movw	%dx, (%r8)
	call	ASN1_GENERALIZEDTIME_new@PLT
	testq	%rax, %rax
	je	.L255
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -184(%rbp)
	call	ASN1_GENERALIZEDTIME_set_string@PLT
	movq	-184(%rbp), %r9
	testl	%eax, %eax
	je	.L541
	movq	%r9, %rsi
	movq	%r13, %rdi
	movq	%r9, -184(%rbp)
	call	TS_TST_INFO_set_time@PLT
	movq	-184(%rbp), %r9
	testl	%eax, %eax
	je	.L519
	cmpq	$0, 64(%rbx)
	je	.L542
.L260:
	movq	%r9, -184(%rbp)
	call	TS_ACCURACY_new@PLT
	movq	-184(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L518
	movq	64(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L262
	movq	%rax, %rdi
	call	TS_ACCURACY_set_seconds@PLT
	movq	-184(%rbp), %r9
	testl	%eax, %eax
	je	.L518
.L262:
	movq	72(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L263
	movq	%r14, %rdi
	movq	%r9, -184(%rbp)
	call	TS_ACCURACY_set_millis@PLT
	movq	-184(%rbp), %r9
	testl	%eax, %eax
	je	.L518
.L263:
	movq	80(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L264
	movq	%r14, %rdi
	movq	%r9, -184(%rbp)
	call	TS_ACCURACY_set_micros@PLT
	movq	-184(%rbp), %r9
	testl	%eax, %eax
	je	.L518
	testq	%r14, %r14
	jne	.L264
.L269:
	testb	$2, 92(%rbx)
	jne	.L543
.L268:
	movq	144(%rbx), %rax
	movq	24(%rax), %rsi
	testq	%rsi, %rsi
	je	.L271
	movq	%r13, %rdi
	movq	%r9, -184(%rbp)
	call	TS_TST_INFO_set_nonce@PLT
	movq	-184(%rbp), %r9
	testl	%eax, %eax
	je	.L518
.L271:
	testb	$1, 92(%rbx)
	jne	.L272
	xorl	%r12d, %r12d
.L273:
	movq	%r12, %rdi
	movq	%r9, -184(%rbp)
	xorl	%r12d, %r12d
	call	GENERAL_NAME_free@PLT
	movq	%r14, %rdi
	call	TS_ACCURACY_free@PLT
	movq	-184(%rbp), %r9
	movq	%r9, %rdi
	call	ASN1_GENERALIZEDTIME_free@PLT
	movq	%r15, %rdi
	call	ASN1_INTEGER_free@PLT
	movq	144(%rbx), %rax
	movq	%r13, 160(%rbx)
	movq	40(%rax), %r13
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L275:
	movl	%r12d, %esi
	movq	%r13, %rdi
	addl	$1, %r12d
	call	OPENSSL_sk_value@PLT
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	*128(%rbx)
	testl	%eax, %eax
	je	.L226
.L323:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%r12d, %eax
	jg	.L275
	movq	8(%rbx), %rsi
	movq	(%rbx), %rdi
	call	X509_check_private_key@PLT
	testl	%eax, %eax
	je	.L544
	call	PKCS7_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L545
	movl	$22, %esi
	movq	%rax, %rdi
	call	PKCS7_set_type@PLT
	testl	%eax, %eax
	je	.L344
	movq	32(%r12), %rax
	movl	$3, %esi
	movq	(%rax), %rdi
	call	ASN1_INTEGER_set@PLT
	testl	%eax, %eax
	je	.L344
	movq	144(%rbx), %rax
	movq	(%rbx), %rsi
	movl	32(%rax), %eax
	testl	%eax, %eax
	jne	.L546
.L279:
	movq	16(%rbx), %rcx
	movq	8(%rbx), %rdx
	movq	%r12, %rdi
	call	PKCS7_add_signature@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L547
	movl	$207, %edi
	call	OBJ_nid2obj@PLT
	movl	$6, %edx
	movl	$50, %esi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	PKCS7_add_signed_attribute@PLT
	testl	%eax, %eax
	je	.L548
	xorl	%r15d, %r15d
	testb	$4, 92(%rbx)
	je	.L284
	movq	32(%rbx), %r15
.L284:
	movq	24(%rbx), %r14
	testq	%r14, %r14
	je	.L288
	call	EVP_sha1@PLT
	cmpq	%rax, %r14
	je	.L288
	movq	24(%rbx), %rax
	movq	(%rbx), %r14
	movq	%rax, -200(%rbp)
	call	ESS_SIGNING_CERT_V2_new@PLT
	movq	%rax, -184(%rbp)
	testq	%rax, %rax
	je	.L302
	movq	-200(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	ess_cert_id_v2_new_init
	testq	%rax, %rax
	je	.L302
	movq	%rax, -192(%rbp)
	movq	%rax, %rsi
	movq	-184(%rbp), %rax
	movq	(%rax), %rdi
	call	OPENSSL_sk_push@PLT
	movq	-192(%rbp), %r8
	testl	%eax, %eax
	je	.L303
	xorl	%r14d, %r14d
	jmp	.L304
.L305:
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	movq	-200(%rbp), %rdi
	movl	$1, %edx
	movq	%rax, %rsi
	call	ess_cert_id_v2_new_init
	testq	%rax, %rax
	je	.L302
	movq	%rax, -192(%rbp)
	movq	%rax, %rsi
	movq	-184(%rbp), %rax
	movq	(%rax), %rdi
	call	OPENSSL_sk_push@PLT
	movq	-192(%rbp), %r8
	testl	%eax, %eax
	je	.L303
	addl	$1, %r14d
.L304:
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r14d
	jl	.L305
	movq	-184(%rbp), %rdi
	xorl	%esi, %esi
	call	i2d_ESS_SIGNING_CERT_V2@PLT
	movl	$963, %edx
	leaq	.LC0(%rip), %rsi
	movslq	%eax, %rdi
	movl	%eax, -192(%rbp)
	call	CRYPTO_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L549
	movq	-208(%rbp), %rsi
	movq	-184(%rbp), %rdi
	movq	%rax, -144(%rbp)
	call	i2d_ESS_SIGNING_CERT_V2@PLT
	call	ASN1_STRING_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L310
	movl	-192(%rbp), %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	ASN1_STRING_set@PLT
	testl	%eax, %eax
	je	.L310
	movl	$975, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	movq	%r15, %rcx
	movl	$16, %edx
	movq	%r13, %rdi
	movl	$1086, %esi
	call	PKCS7_add_signed_attribute@PLT
	testl	%eax, %eax
	je	.L311
	xorl	%r14d, %r14d
.L301:
	call	PKCS7_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L313
	call	ASN1_TYPE_new@PLT
	movq	%rax, 32(%r13)
	testq	%rax, %rax
	je	.L313
	movl	$207, %edi
	call	OBJ_nid2obj@PLT
	movq	%rax, 24(%r13)
	call	ASN1_OCTET_STRING_new@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L313
	movq	32(%r13), %rdi
	movl	$4, %esi
	call	ASN1_TYPE_set@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	PKCS7_set_content@PLT
	testl	%eax, %eax
	je	.L313
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	PKCS7_dataInit@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L550
	movq	160(%rbx), %rsi
	movq	%rax, %rdi
	call	i2d_TS_TST_INFO_bio@PLT
	movl	$710, %r8d
	testl	%eax, %eax
	je	.L521
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	PKCS7_dataFinal@PLT
	testl	%eax, %eax
	je	.L551
	movq	160(%rbx), %rdx
	movq	152(%rbx), %rdi
	movq	%r12, %rsi
	call	TS_RESP_set_tst_info@PLT
	movq	%r13, %rdi
	movq	$0, 160(%rbx)
	call	BIO_free_all@PLT
	movq	-184(%rbp), %rdi
	call	ESS_SIGNING_CERT_V2_free@PLT
	movq	%r14, %rdi
	call	ESS_SIGNING_CERT_free@PLT
	xorl	%edi, %edi
	call	PKCS7_free@PLT
	movq	152(%rbx), %r12
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L538:
	leaq	.LC10(%rip), %rdx
	movl	$2, %esi
	movq	%rbx, %rdi
	call	TS_RESP_CTX_set_status_info
	movq	-184(%rbp), %r9
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L532:
	call	ASN1_BIT_STRING_new@PLT
	movq	%rax, 16(%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L234
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L535:
	leaq	.LC6(%rip), %rdx
.L526:
	movq	%rbx, %rdi
	movl	$2, %esi
	call	TS_RESP_CTX_set_status_info
	movq	152(%rbx), %rax
	movq	(%rax), %r12
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L552
.L241:
	xorl	%esi, %esi
	movl	$1, %edx
	call	ASN1_BIT_STRING_set_bit@PLT
	testl	%eax, %eax
	jne	.L226
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L537:
	movl	$521, %r8d
	movl	$125, %edx
	movl	$133, %esi
	movl	$47, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%rbx, %rdi
	movl	$2, %esi
	leaq	.LC13(%rip), %rdx
	call	TS_RESP_CTX_set_status_info
	movq	152(%rbx), %rax
	movq	(%rax), %r12
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L553
.L325:
	movl	$1, %edx
	movl	$15, %esi
	call	ASN1_BIT_STRING_set_bit@PLT
	testl	%eax, %eax
	jne	.L226
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L534:
	leaq	.LC14(%rip), %rdx
	jmp	.L526
	.p2align 4,,10
	.p2align 3
.L552:
	call	ASN1_BIT_STRING_new@PLT
	movq	%rax, 16(%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L241
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L344:
	movq	$0, -184(%rbp)
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
.L277:
	movq	152(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	ASN1_INTEGER_get@PLT
	testq	%rax, %rax
	je	.L554
.L318:
	movq	%r13, %rdi
	call	BIO_free_all@PLT
	movq	-184(%rbp), %rdi
	call	ESS_SIGNING_CERT_V2_free@PLT
	movq	%r14, %rdi
	call	ESS_SIGNING_CERT_free@PLT
	movq	%r12, %rdi
	call	PKCS7_free@PLT
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L544:
	movl	$120, %edx
	movl	$136, %esi
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	movl	$645, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$47, %edi
	xorl	%r13d, %r13d
	call	ERR_put_error@PLT
	movq	$0, -184(%rbp)
	jmp	.L277
.L553:
	call	ASN1_BIT_STRING_new@PLT
	movq	%rax, 16(%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L325
	jmp	.L229
.L536:
	movl	$508, %r8d
	movl	$102, %edx
	movl	$133, %esi
	movl	$47, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L226
.L545:
	movl	$650, %r8d
	movl	$65, %edx
	movl	$136, %esi
	leaq	.LC0(%rip), %rcx
.L522:
	movl	$47, %edi
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	call	ERR_put_error@PLT
	movq	$0, -184(%rbp)
	jmp	.L277
.L554:
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	movq	%rbx, %rdi
	call	TS_RESP_CTX_set_status_info
	jmp	.L318
.L288:
	movq	(%rbx), %rdi
	movq	%rdi, -184(%rbp)
	call	ESS_SIGNING_CERT_new@PLT
	movq	-184(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L286
	cmpq	$0, (%rax)
	je	.L290
.L292:
	xorl	%esi, %esi
	call	ess_CERT_ID_new_init
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L286
	movq	(%r14), %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L286
	movl	$0, -184(%rbp)
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L294:
	movl	-184(%rbp), %esi
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	ess_CERT_ID_new_init
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L286
	movq	(%r14), %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L286
	addl	$1, -184(%rbp)
.L293:
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, -184(%rbp)
	jl	.L294
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	i2d_ESS_SIGNING_CERT@PLT
	movl	$840, %edx
	leaq	.LC0(%rip), %rsi
	movslq	%eax, %rdi
	movl	%eax, -184(%rbp)
	call	CRYPTO_malloc@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L555
	movq	-208(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, -144(%rbp)
	call	i2d_ESS_SIGNING_CERT@PLT
	call	ASN1_STRING_new@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L299
	movl	-184(%rbp), %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -184(%rbp)
	call	ASN1_STRING_set@PLT
	movq	-184(%rbp), %r9
	testl	%eax, %eax
	je	.L299
	movl	$850, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	call	CRYPTO_free@PLT
	movq	-184(%rbp), %r9
	movl	$16, %edx
	movq	%r13, %rdi
	movl	$223, %esi
	movq	%r9, %rcx
	call	PKCS7_add_signed_attribute@PLT
	testl	%eax, %eax
	je	.L300
	movq	$0, -184(%rbp)
	jmp	.L301
.L546:
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	PKCS7_add_certificate@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L280
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L281:
	movq	32(%rbx), %rdi
	movl	%r13d, %esi
	addl	$1, %r13d
	call	OPENSSL_sk_value@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	PKCS7_add_certificate@PLT
	movq	32(%rbx), %rdi
.L280:
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jl	.L281
.L520:
	movq	(%rbx), %rsi
	jmp	.L279
.L290:
	movq	%rdi, -184(%rbp)
	call	OPENSSL_sk_new_null@PLT
	movq	-184(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, (%r14)
	jne	.L292
.L286:
	movq	%r14, %rdi
	call	ESS_SIGNING_CERT_free@PLT
	movl	$760, %r8d
	movl	$65, %edx
	leaq	.LC0(%rip), %rcx
	movl	$114, %esi
	jmp	.L522
.L313:
	xorl	%edi, %edi
	call	ASN1_OCTET_STRING_free@PLT
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	PKCS7_free@PLT
	jmp	.L277
.L541:
	movq	%r9, %rdi
	call	ASN1_GENERALIZEDTIME_free@PLT
.L255:
	movl	$115, %edx
	movl	$134, %esi
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	movl	$1050, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$47, %edi
	call	ERR_put_error@PLT
	xorl	%r9d, %r9d
	jmp	.L252
.L302:
	xorl	%r8d, %r8d
.L303:
	movq	-184(%rbp), %rdi
	movq	%r8, -192(%rbp)
	call	ESS_SIGNING_CERT_V2_free@PLT
	movq	-192(%rbp), %r8
	movq	%r8, %rdi
	call	ESS_CERT_ID_V2_free@PLT
	movl	$892, %r8d
	movl	$65, %edx
	leaq	.LC0(%rip), %rcx
	movl	$157, %esi
	jmp	.L522
.L547:
	movl	$670, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$118, %edx
	xorl	%r14d, %r14d
	movl	$136, %esi
	movl	$47, %edi
	call	ERR_put_error@PLT
	movq	$0, -184(%rbp)
	jmp	.L277
.L548:
	movl	$677, %r8d
	movl	$119, %edx
	movl	$136, %esi
	leaq	.LC0(%rip), %rcx
	jmp	.L522
.L299:
	movl	$847, %r8d
	movl	$65, %edx
	movl	$112, %esi
	movl	$47, %edi
	leaq	.LC0(%rip), %rcx
	movq	%r9, -184(%rbp)
	call	ERR_put_error@PLT
	movq	-184(%rbp), %r9
.L297:
	movq	%r9, %rdi
	call	ASN1_STRING_free@PLT
	movl	$857, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	call	CRYPTO_free@PLT
.L300:
	movl	$688, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$116, %edx
	xorl	%r13d, %r13d
	movl	$136, %esi
	movl	$47, %edi
	call	ERR_put_error@PLT
	movq	$0, -184(%rbp)
	jmp	.L277
.L542:
	cmpq	$0, 72(%rbx)
	jne	.L260
	cmpq	$0, 80(%rbx)
	jne	.L260
	xorl	%r14d, %r14d
	jmp	.L269
.L310:
	movl	$971, %r8d
	movl	$65, %edx
	movl	$147, %esi
	movl	$47, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L308:
	movq	%r15, %rdi
	call	ASN1_STRING_free@PLT
	movl	$982, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
.L311:
	movl	$139, %edx
	movl	$136, %esi
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	movl	$698, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$47, %edi
	call	ERR_put_error@PLT
	jmp	.L277
.L540:
	movq	-184(%rbp), %rcx
	movq	%r8, %rdi
	leal	2(%r14), %esi
	xorl	%eax, %eax
	leaq	.LC9(%rip), %rdx
	movq	%r8, -184(%rbp)
	call	BIO_snprintf@PLT
	movq	-184(%rbp), %r8
	xorl	%eax, %eax
	orq	$-1, %rcx
	movq	%r8, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	leaq	-1(%r8,%rax), %r8
.L258:
	movq	%r8, %rdx
	movzbl	-1(%r8), %eax
	subq	$1, %r8
	cmpb	$48, %al
	je	.L258
	cmpb	$46, %al
	cmovne	%rdx, %r8
	jmp	.L257
.L551:
	movl	$714, %r8d
.L521:
	movl	$124, %edx
	movl	$136, %esi
	movl	$47, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L277
.L533:
	call	__stack_chk_fail@PLT
.L555:
	movl	$841, %r8d
	movl	$65, %edx
	movl	$112, %esi
	movl	$47, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r9d, %r9d
	jmp	.L297
.L264:
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%r9, -184(%rbp)
	call	TS_TST_INFO_set_accuracy@PLT
	movq	-184(%rbp), %r9
	testl	%eax, %eax
	jne	.L269
	jmp	.L518
.L550:
	movl	$706, %r8d
	movl	$65, %edx
	movl	$136, %esi
	movl	$47, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L277
.L549:
	movl	$964, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r15d, %r15d
	movl	$147, %esi
	movl	$47, %edi
	call	ERR_put_error@PLT
	jmp	.L308
.L543:
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%r9, -184(%rbp)
	call	TS_TST_INFO_set_ordering@PLT
	movq	-184(%rbp), %r9
	testl	%eax, %eax
	jne	.L268
	jmp	.L518
.L272:
	movq	%r9, -184(%rbp)
	call	GENERAL_NAME_new@PLT
	movq	-184(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L252
	movl	$4, (%rax)
	movq	(%rbx), %rdi
	call	X509_get_subject_name@PLT
	movq	%rax, %rdi
	call	X509_NAME_dup@PLT
	movq	-184(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, 8(%r12)
	je	.L252
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	TS_TST_INFO_set_tsa@PLT
	movq	-184(%rbp), %r9
	testl	%eax, %eax
	jne	.L273
	jmp	.L252
	.cfi_endproc
.LFE1402:
	.size	TS_RESP_create_response, .-TS_RESP_create_response
	.p2align 4
	.globl	TS_RESP_CTX_set_ess_cert_id_digest
	.type	TS_RESP_CTX_set_ess_cert_id_digest, @function
TS_RESP_CTX_set_ess_cert_id_digest:
.LFB1418:
	.cfi_startproc
	endbr64
	movq	%rsi, 24(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1418:
	.size	TS_RESP_CTX_set_ess_cert_id_digest, .-TS_RESP_CTX_set_ess_cert_id_digest
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
