	.file	"i2d_pr.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/asn1/i2d_pr.c"
	.text
	.p2align 4
	.globl	i2d_PrivateKey
	.type	i2d_PrivateKey, @function
i2d_PrivateKey:
.LFB829:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L2
	movq	192(%rax), %rdx
	movq	%rsi, %r12
	testq	%rdx, %rdx
	je	.L3
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rdx
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	cmpq	$0, 72(%rax)
	je	.L2
	call	EVP_PKEY2PKCS8@PLT
	xorl	%r14d, %r14d
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1
	movq	%rax, %rdi
	movq	%r12, %rsi
	call	i2d_PKCS8_PRIV_KEY_INFO@PLT
	movq	%r13, %rdi
	movl	%eax, %r14d
	call	PKCS8_PRIV_KEY_INFO_free@PLT
.L1:
	addq	$8, %rsp
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movl	$31, %r8d
	movl	$167, %edx
	movl	$163, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r14d
	call	ERR_put_error@PLT
	jmp	.L1
	.cfi_endproc
.LFE829:
	.size	i2d_PrivateKey, .-i2d_PrivateKey
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
