	.file	"x509_req.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/x509/x509_req.c"
	.text
	.p2align 4
	.globl	X509_to_X509_REQ
	.type	X509_to_X509_REQ, @function
X509_to_X509_REQ:
.LFB805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	call	X509_REQ_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L24
	movq	24(%rax), %rbx
	movl	$38, %edx
	movl	$1, %edi
	leaq	.LC0(%rip), %rsi
	movl	$1, (%rbx)
	call	CRYPTO_malloc@PLT
	movq	%rax, 8(%rbx)
	movq	24(%r12), %rax
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L3
	movb	$0, (%rax)
	movq	%r13, %rdi
	call	X509_get_subject_name@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	X509_REQ_set_subject_name@PLT
	testl	%eax, %eax
	je	.L3
	movq	%r13, %rdi
	call	X509_get0_pubkey@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L3
	movq	%r12, %rdi
	call	X509_REQ_set_pubkey@PLT
	testl	%eax, %eax
	je	.L3
	testq	%r14, %r14
	je	.L1
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	X509_REQ_sign@PLT
	testl	%eax, %eax
	jne	.L1
	.p2align 4,,10
	.p2align 3
.L3:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	X509_REQ_free@PLT
.L1:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movl	$31, %r8d
	movl	$65, %edx
	movl	$126, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L3
	.cfi_endproc
.LFE805:
	.size	X509_to_X509_REQ, .-X509_to_X509_REQ
	.p2align 4
	.globl	X509_REQ_get_pubkey
	.type	X509_REQ_get_pubkey, @function
X509_REQ_get_pubkey:
.LFB806:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L26
	movq	40(%rdi), %rdi
	jmp	X509_PUBKEY_get@PLT
	.p2align 4,,10
	.p2align 3
.L26:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE806:
	.size	X509_REQ_get_pubkey, .-X509_REQ_get_pubkey
	.p2align 4
	.globl	X509_REQ_get0_pubkey
	.type	X509_REQ_get0_pubkey, @function
X509_REQ_get0_pubkey:
.LFB807:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L28
	movq	40(%rdi), %rdi
	jmp	X509_PUBKEY_get0@PLT
	.p2align 4,,10
	.p2align 3
.L28:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE807:
	.size	X509_REQ_get0_pubkey, .-X509_REQ_get0_pubkey
	.p2align 4
	.globl	X509_REQ_get_X509_PUBKEY
	.type	X509_REQ_get_X509_PUBKEY, @function
X509_REQ_get_X509_PUBKEY:
.LFB808:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE808:
	.size	X509_REQ_get_X509_PUBKEY, .-X509_REQ_get_X509_PUBKEY
	.p2align 4
	.globl	X509_REQ_check_private_key
	.type	X509_REQ_check_private_key, @function
X509_REQ_check_private_key:
.LFB809:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	testq	%rdi, %rdi
	je	.L31
	movq	40(%rdi), %rdi
	call	X509_PUBKEY_get@PLT
	movq	%rax, %r13
.L31:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	EVP_PKEY_cmp@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L32
	jg	.L33
	cmpl	$-2, %eax
	je	.L34
	cmpl	$-1, %eax
	jne	.L43
	movl	$97, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$115, %edx
	xorl	%r12d, %r12d
	movl	$144, %esi
	movl	$11, %edi
	call	ERR_put_error@PLT
.L36:
	movq	%r13, %rdi
	call	EVP_PKEY_free@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	cmpl	$1, %eax
	sete	%r12b
	movzbl	%r12b, %r12d
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L32:
	movl	$93, %r8d
	movl	$116, %edx
	movl	$144, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L34:
	movq	%r14, %rdi
	call	EVP_PKEY_id@PLT
	cmpl	$408, %eax
	je	.L44
	movq	%r14, %rdi
	call	EVP_PKEY_id@PLT
	cmpl	$28, %eax
	je	.L45
	movl	$114, %r8d
	movl	$117, %edx
	movl	$144, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L43:
	xorl	%r12d, %r12d
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L44:
	movl	$102, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$16, %edx
	xorl	%r12d, %r12d
	movl	$144, %esi
	movl	$11, %edi
	call	ERR_put_error@PLT
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L45:
	movl	$109, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$114, %edx
	xorl	%r12d, %r12d
	movl	$144, %esi
	movl	$11, %edi
	call	ERR_put_error@PLT
	jmp	.L36
	.cfi_endproc
.LFE809:
	.size	X509_REQ_check_private_key, .-X509_REQ_check_private_key
	.p2align 4
	.globl	X509_REQ_extension_nid
	.type	X509_REQ_extension_nid, @function
X509_REQ_extension_nid:
.LFB810:
	.cfi_startproc
	endbr64
	movq	ext_nids(%rip), %rdx
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L46
	addq	$4, %rdx
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L55:
	movl	(%rdx), %eax
	addq	$4, %rdx
	testl	%eax, %eax
	je	.L54
.L48:
	cmpl	%eax, %edi
	jne	.L55
	movl	$1, %eax
.L46:
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	ret
	.cfi_endproc
.LFE810:
	.size	X509_REQ_extension_nid, .-X509_REQ_extension_nid
	.p2align 4
	.globl	X509_REQ_get_extension_nids
	.type	X509_REQ_get_extension_nids, @function
X509_REQ_get_extension_nids:
.LFB811:
	.cfi_startproc
	endbr64
	movq	ext_nids(%rip), %rax
	ret
	.cfi_endproc
.LFE811:
	.size	X509_REQ_get_extension_nids, .-X509_REQ_get_extension_nids
	.p2align 4
	.globl	X509_REQ_set_extension_nids
	.type	X509_REQ_set_extension_nids, @function
X509_REQ_set_extension_nids:
.LFB812:
	.cfi_startproc
	endbr64
	movq	%rdi, ext_nids(%rip)
	ret
	.cfi_endproc
.LFE812:
	.size	X509_REQ_set_extension_nids, .-X509_REQ_set_extension_nids
	.p2align 4
	.globl	X509_REQ_get_extensions
	.type	X509_REQ_get_extensions, @function
X509_REQ_get_extensions:
.LFB813:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L61
	movq	ext_nids(%rip), %r12
	testq	%r12, %r12
	je	.L61
	movl	(%r12), %esi
	movq	%rdi, %rbx
	testl	%esi, %esi
	je	.L61
.L64:
	movq	48(%rbx), %rdi
	movl	$-1, %edx
	call	X509at_get_attr_by_NID@PLT
	movl	%eax, %esi
	cmpl	$-1, %eax
	je	.L78
	movq	48(%rbx), %rdi
	call	X509at_get_attr@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	X509_ATTRIBUTE_get0_type@PLT
	testq	%rax, %rax
	je	.L61
	cmpl	$16, (%rax)
	jne	.L61
	movq	8(%rax), %rax
	leaq	-32(%rbp), %rsi
	leaq	X509_EXTENSIONS_it(%rip), %rcx
	xorl	%edi, %edi
	movq	8(%rax), %rdx
	movq	%rdx, -32(%rbp)
	movslq	(%rax), %rdx
	call	ASN1_item_d2i@PLT
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L78:
	movl	4(%r12), %esi
	addq	$4, %r12
	testl	%esi, %esi
	jne	.L64
	.p2align 4,,10
	.p2align 3
.L61:
	xorl	%eax, %eax
.L58:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L79
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L79:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE813:
	.size	X509_REQ_get_extensions, .-X509_REQ_get_extensions
	.p2align 4
	.globl	X509_REQ_add_extensions_nid
	.type	X509_REQ_add_extensions_nid, @function
X509_REQ_add_extensions_nid:
.LFB814:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movl	%edx, %r12d
	leaq	X509_EXTENSIONS_it(%rip), %rdx
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	leaq	-48(%rbp), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	call	ASN1_item_i2d@PLT
	testl	%eax, %eax
	jle	.L80
	movq	-48(%rbp), %rcx
	leaq	48(%rbx), %rdi
	movl	$16, %edx
	movl	%r12d, %esi
	movl	%eax, %r8d
	xorl	%r13d, %r13d
	call	X509at_add1_attr_by_NID@PLT
	movq	-48(%rbp), %rdi
	movl	$195, %edx
	leaq	.LC0(%rip), %rsi
	testq	%rax, %rax
	setne	%r13b
	call	CRYPTO_free@PLT
.L80:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L86
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L86:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE814:
	.size	X509_REQ_add_extensions_nid, .-X509_REQ_add_extensions_nid
	.p2align 4
	.globl	X509_REQ_add_extensions
	.type	X509_REQ_add_extensions, @function
X509_REQ_add_extensions:
.LFB815:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	X509_EXTENSIONS_it(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	leaq	-32(%rbp), %rsi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$0, -32(%rbp)
	call	ASN1_item_i2d@PLT
	testl	%eax, %eax
	jle	.L87
	movq	-32(%rbp), %rcx
	leaq	48(%rbx), %rdi
	movl	$16, %edx
	movl	%eax, %r8d
	movl	$172, %esi
	xorl	%r12d, %r12d
	call	X509at_add1_attr_by_NID@PLT
	movq	-32(%rbp), %rdi
	movl	$195, %edx
	leaq	.LC0(%rip), %rsi
	testq	%rax, %rax
	setne	%r12b
	call	CRYPTO_free@PLT
.L87:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L93
	addq	$16, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L93:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE815:
	.size	X509_REQ_add_extensions, .-X509_REQ_add_extensions
	.p2align 4
	.globl	X509_REQ_get_attr_count
	.type	X509_REQ_get_attr_count, @function
X509_REQ_get_attr_count:
.LFB816:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rdi
	jmp	X509at_get_attr_count@PLT
	.cfi_endproc
.LFE816:
	.size	X509_REQ_get_attr_count, .-X509_REQ_get_attr_count
	.p2align 4
	.globl	X509_REQ_get_attr_by_NID
	.type	X509_REQ_get_attr_by_NID, @function
X509_REQ_get_attr_by_NID:
.LFB817:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rdi
	jmp	X509at_get_attr_by_NID@PLT
	.cfi_endproc
.LFE817:
	.size	X509_REQ_get_attr_by_NID, .-X509_REQ_get_attr_by_NID
	.p2align 4
	.globl	X509_REQ_get_attr_by_OBJ
	.type	X509_REQ_get_attr_by_OBJ, @function
X509_REQ_get_attr_by_OBJ:
.LFB818:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rdi
	jmp	X509at_get_attr_by_OBJ@PLT
	.cfi_endproc
.LFE818:
	.size	X509_REQ_get_attr_by_OBJ, .-X509_REQ_get_attr_by_OBJ
	.p2align 4
	.globl	X509_REQ_get_attr
	.type	X509_REQ_get_attr, @function
X509_REQ_get_attr:
.LFB819:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rdi
	jmp	X509at_get_attr@PLT
	.cfi_endproc
.LFE819:
	.size	X509_REQ_get_attr, .-X509_REQ_get_attr
	.p2align 4
	.globl	X509_REQ_delete_attr
	.type	X509_REQ_delete_attr, @function
X509_REQ_delete_attr:
.LFB820:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rdi
	jmp	X509at_delete_attr@PLT
	.cfi_endproc
.LFE820:
	.size	X509_REQ_delete_attr, .-X509_REQ_delete_attr
	.p2align 4
	.globl	X509_REQ_add1_attr
	.type	X509_REQ_add1_attr, @function
X509_REQ_add1_attr:
.LFB821:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$48, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509at_add1_attr@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE821:
	.size	X509_REQ_add1_attr, .-X509_REQ_add1_attr
	.p2align 4
	.globl	X509_REQ_add1_attr_by_OBJ
	.type	X509_REQ_add1_attr_by_OBJ, @function
X509_REQ_add1_attr_by_OBJ:
.LFB822:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$48, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509at_add1_attr_by_OBJ@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE822:
	.size	X509_REQ_add1_attr_by_OBJ, .-X509_REQ_add1_attr_by_OBJ
	.p2align 4
	.globl	X509_REQ_add1_attr_by_NID
	.type	X509_REQ_add1_attr_by_NID, @function
X509_REQ_add1_attr_by_NID:
.LFB823:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$48, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509at_add1_attr_by_NID@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE823:
	.size	X509_REQ_add1_attr_by_NID, .-X509_REQ_add1_attr_by_NID
	.p2align 4
	.globl	X509_REQ_add1_attr_by_txt
	.type	X509_REQ_add1_attr_by_txt, @function
X509_REQ_add1_attr_by_txt:
.LFB824:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$48, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509at_add1_attr_by_txt@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE824:
	.size	X509_REQ_add1_attr_by_txt, .-X509_REQ_add1_attr_by_txt
	.p2align 4
	.globl	X509_REQ_get_version
	.type	X509_REQ_get_version, @function
X509_REQ_get_version:
.LFB825:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdi
	jmp	ASN1_INTEGER_get@PLT
	.cfi_endproc
.LFE825:
	.size	X509_REQ_get_version, .-X509_REQ_get_version
	.p2align 4
	.globl	X509_REQ_get_subject_name
	.type	X509_REQ_get_subject_name, @function
X509_REQ_get_subject_name:
.LFB826:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE826:
	.size	X509_REQ_get_subject_name, .-X509_REQ_get_subject_name
	.p2align 4
	.globl	X509_REQ_get0_signature
	.type	X509_REQ_get0_signature, @function
X509_REQ_get0_signature:
.LFB827:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L110
	movq	72(%rdi), %rax
	movq	%rax, (%rsi)
.L110:
	testq	%rdx, %rdx
	je	.L109
	addq	$56, %rdi
	movq	%rdi, (%rdx)
.L109:
	ret
	.cfi_endproc
.LFE827:
	.size	X509_REQ_get0_signature, .-X509_REQ_get0_signature
	.p2align 4
	.globl	X509_REQ_get_signature_nid
	.type	X509_REQ_get_signature_nid, @function
X509_REQ_get_signature_nid:
.LFB828:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rdi
	jmp	OBJ_obj2nid@PLT
	.cfi_endproc
.LFE828:
	.size	X509_REQ_get_signature_nid, .-X509_REQ_get_signature_nid
	.p2align 4
	.globl	i2d_re_X509_REQ_tbs
	.type	i2d_re_X509_REQ_tbs, @function
i2d_re_X509_REQ_tbs:
.LFB829:
	.cfi_startproc
	endbr64
	movl	$1, 16(%rdi)
	jmp	i2d_X509_REQ_INFO@PLT
	.cfi_endproc
.LFE829:
	.size	i2d_re_X509_REQ_tbs, .-i2d_re_X509_REQ_tbs
	.section	.data.rel.local,"aw"
	.align 8
	.type	ext_nids, @object
	.size	ext_nids, 8
ext_nids:
	.quad	ext_nid_list
	.data
	.align 8
	.type	ext_nid_list, @object
	.size	ext_nid_list, 12
ext_nid_list:
	.long	172
	.long	171
	.long	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
