	.file	"dsa_depr.c"
	.text
	.p2align 4
	.globl	DSA_generate_parameters
	.type	DSA_generate_parameters, @function
DSA_generate_parameters:
.LFB419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	movl	%edi, -52(%rbp)
	movq	%rsi, -64(%rbp)
	movl	%edx, -56(%rbp)
	call	DSA_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1
	call	BN_GENCB_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L3
	movq	16(%rbp), %rdx
	movq	%rax, %rdi
	movq	%r15, %rsi
	call	BN_GENCB_set_old@PLT
	subq	$8, %rsp
	movl	-56(%rbp), %ecx
	movq	-64(%rbp), %rdx
	pushq	%r13
	movl	-52(%rbp), %esi
	movq	%r12, %rdi
	movq	%r14, %r9
	movq	%rbx, %r8
	call	DSA_generate_parameters_ex@PLT
	popq	%rdx
	movq	%r13, %rdi
	popq	%rcx
	testl	%eax, %eax
	je	.L4
	call	BN_GENCB_free@PLT
.L1:
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	call	BN_GENCB_free@PLT
.L3:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	DSA_free@PLT
	jmp	.L1
	.cfi_endproc
.LFE419:
	.size	DSA_generate_parameters, .-DSA_generate_parameters
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
