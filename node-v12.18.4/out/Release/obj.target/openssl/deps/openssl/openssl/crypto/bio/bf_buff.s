	.file	"bf_buff.c"
	.text
	.p2align 4
	.type	buffer_callback_ctrl, @function
buffer_callback_ctrl:
.LFB273:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2
	jmp	BIO_callback_ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L2:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE273:
	.size	buffer_callback_ctrl, .-buffer_callback_ctrl
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/bio/bf_buff.c"
	.text
	.p2align 4
	.type	buffer_new, @function
buffer_new:
.LFB268:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$49, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$40, %edi
	subq	$8, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L4
	movl	$4096, (%rax)
	movl	$54, %edx
	movl	$4096, %edi
	movq	%rax, %r12
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L11
	movl	$4096, 4(%r12)
	movl	$60, %edx
	movl	$4096, %edi
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, 24(%r12)
	testq	%rax, %rax
	je	.L12
	movl	$1, 32(%rbx)
	movl	$1, %r13d
	movq	%r12, 56(%rbx)
	movl	$0, 40(%rbx)
.L4:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$56, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	movq	8(%r12), %rdi
	movl	$62, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$63, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	jmp	.L4
	.cfi_endproc
.LFE268:
	.size	buffer_new, .-buffer_new
	.p2align 4
	.type	buffer_gets, @function
buffer_gets:
.LFB274:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leal	-1(%rdx), %r13d
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	$15, %esi
	subq	$24, %rsp
	movq	56(%rdi), %r14
	call	BIO_clear_flags@PLT
	movl	16(%r14), %eax
	.p2align 4,,10
	.p2align 3
.L14:
	movq	8(%r14), %rsi
	testl	%eax, %eax
	jle	.L15
.L28:
	movslq	20(%r14), %rdx
	movq	%rdx, %rcx
	testl	%r13d, %r13d
	jle	.L16
	addq	%rdx, %rsi
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L19:
	movzbl	(%rsi), %eax
	addq	$1, %rbx
	addl	$1, %edx
	movb	%al, -1(%rbx)
	cmpb	$10, %al
	je	.L27
	movl	16(%r14), %eax
	addq	$1, %rsi
	movl	%r13d, %ecx
	cmpl	%r13d, %eax
	cmovle	%eax, %ecx
	cmpl	%ecx, %edx
	jl	.L19
	movl	20(%r14), %ecx
	subl	%edx, %eax
	addl	%edx, %r12d
	subl	%edx, %r13d
	addl	%edx, %ecx
.L16:
	movl	%eax, 16(%r14)
	movl	%ecx, 20(%r14)
	testl	%r13d, %r13d
	je	.L18
	movq	8(%r14), %rsi
	testl	%eax, %eax
	jg	.L28
.L15:
	movq	64(%r15), %rdi
	movl	(%r14), %edx
	call	BIO_read@PLT
	testl	%eax, %eax
	jle	.L29
	movl	%eax, 16(%r14)
	movl	$0, 20(%r14)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L27:
	subl	%edx, 16(%r14)
	addl	%edx, %r12d
	addl	%edx, 20(%r14)
.L18:
	movb	$0, (%rbx)
.L13:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L29:
	.cfi_restore_state
	movq	%r15, %rdi
	movl	%eax, -52(%rbp)
	call	BIO_copy_next_retry@PLT
	movb	$0, (%rbx)
	testl	%r12d, %r12d
	jne	.L13
	movl	-52(%rbp), %eax
	testl	%eax, %eax
	cmovne	%eax, %r12d
	jmp	.L13
	.cfi_endproc
.LFE274:
	.size	buffer_gets, .-buffer_gets
	.p2align 4
	.type	buffer_read, @function
buffer_read:
.LFB270:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rsi, %rsi
	je	.L40
	movq	56(%rdi), %r14
	movq	%rdi, %r13
	xorl	%r12d, %r12d
	testq	%r14, %r14
	je	.L30
	cmpq	$0, 64(%rdi)
	je	.L30
	movq	%rsi, %rbx
	movl	$15, %esi
	movl	%edx, %r15d
	call	BIO_clear_flags@PLT
	movl	$0, -52(%rbp)
	movl	16(%r14), %r12d
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L33:
	movl	(%r14), %edx
	movq	64(%r13), %rdi
	cmpl	%r15d, %edx
	jl	.L37
	movq	8(%r14), %rsi
	call	BIO_read@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jle	.L59
	movl	$0, 20(%r14)
	movl	%eax, 16(%r14)
.L32:
	testl	%r12d, %r12d
	je	.L33
	cmpl	%r12d, %r15d
	movl	%r12d, %ecx
	movslq	20(%r14), %rsi
	movq	%rbx, %rdi
	cmovle	%r15d, %ecx
	addq	8(%r14), %rsi
	movslq	%ecx, %rdx
	movl	%ecx, -56(%rbp)
	movq	%rdx, -64(%rbp)
	call	memcpy@PLT
	movl	-56(%rbp), %ecx
	addl	%ecx, 20(%r14)
	subl	%ecx, 16(%r14)
	addl	%ecx, -52(%rbp)
	cmpl	%r12d, %r15d
	jle	.L44
	movq	-64(%rbp), %rdx
	subl	%ecx, %r15d
	addq	%rdx, %rbx
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L35:
	addl	%eax, -52(%rbp)
	cmpl	%eax, %r15d
	je	.L44
	cltq
	movq	64(%r13), %rdi
	subl	%r12d, %r15d
	addq	%rax, %rbx
.L37:
	movl	%r15d, %edx
	movq	%rbx, %rsi
	call	BIO_read@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jg	.L35
.L59:
	movq	%r13, %rdi
	call	BIO_copy_next_retry@PLT
	movl	-52(%rbp), %eax
	testl	%eax, %eax
	jg	.L44
	testl	%r12d, %r12d
	jne	.L30
.L44:
	movl	-52(%rbp), %r12d
.L30:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L30
	.cfi_endproc
.LFE270:
	.size	buffer_read, .-buffer_read
	.p2align 4
	.type	buffer_ctrl, @function
buffer_ctrl:
.LFB272:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	56(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$29, %esi
	jg	.L61
	testl	%esi, %esi
	jle	.L62
	cmpl	$29, %esi
	ja	.L62
	leaq	.L64(%rip), %rcx
	movl	%esi, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L64:
	.long	.L62-.L64
	.long	.L71-.L64
	.long	.L70-.L64
	.long	.L69-.L64
	.long	.L62-.L64
	.long	.L62-.L64
	.long	.L62-.L64
	.long	.L62-.L64
	.long	.L62-.L64
	.long	.L62-.L64
	.long	.L68-.L64
	.long	.L67-.L64
	.long	.L66-.L64
	.long	.L65-.L64
	.long	.L62-.L64
	.long	.L62-.L64
	.long	.L62-.L64
	.long	.L62-.L64
	.long	.L62-.L64
	.long	.L62-.L64
	.long	.L62-.L64
	.long	.L62-.L64
	.long	.L62-.L64
	.long	.L62-.L64
	.long	.L62-.L64
	.long	.L62-.L64
	.long	.L62-.L64
	.long	.L62-.L64
	.long	.L62-.L64
	.long	.L63-.L64
	.text
	.p2align 4,,10
	.p2align 3
.L61:
	cmpl	$117, %esi
	je	.L72
	jle	.L148
	cmpl	$122, %esi
	jne	.L62
	movslq	(%rbx), %rax
	movslq	%edx, %r15
	cmpq	%rdx, %rax
	jge	.L149
	movl	$292, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L107
	movq	8(%rbx), %rdi
	movl	$295, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, 8(%rbx)
.L101:
	movl	$0, 20(%rbx)
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%r13d, 16(%rbx)
	movl	$1, %r15d
	call	memcpy@PLT
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L148:
	cmpl	$101, %esi
	je	.L74
	cmpl	$116, %esi
	jne	.L62
	movl	16(%rbx), %edx
	movq	8(%rbx), %rdi
	testl	%edx, %edx
	jle	.L146
	leal	-1(%rdx), %eax
	movl	20(%rbx), %esi
	cmpl	$14, %eax
	jbe	.L116
	movl	%edx, %ecx
	pxor	%xmm0, %xmm0
	pxor	%xmm4, %xmm4
	movslq	%esi, %rax
	shrl	$4, %ecx
	pxor	%xmm3, %xmm3
	pxor	%xmm2, %xmm2
	addq	%rdi, %rax
	salq	$4, %rcx
	movdqa	.LC1(%rip), %xmm6
	movdqa	.LC2(%rip), %xmm5
	addq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L81:
	movdqu	(%rax), %xmm1
	movdqa	%xmm4, %xmm7
	movdqa	%xmm2, %xmm11
	addq	$16, %rax
	pcmpeqb	%xmm6, %xmm1
	pand	%xmm5, %xmm1
	pcmpgtb	%xmm1, %xmm7
	movdqa	%xmm1, %xmm8
	punpcklbw	%xmm7, %xmm8
	punpckhbw	%xmm7, %xmm1
	movdqa	%xmm3, %xmm7
	pcmpgtw	%xmm8, %xmm7
	movdqa	%xmm8, %xmm9
	movdqa	%xmm1, %xmm10
	punpcklwd	%xmm7, %xmm9
	punpckhwd	%xmm7, %xmm8
	movdqa	%xmm3, %xmm7
	pcmpgtw	%xmm1, %xmm7
	pcmpgtd	%xmm9, %xmm11
	punpckhwd	%xmm7, %xmm1
	punpcklwd	%xmm7, %xmm10
	movdqa	%xmm1, %xmm7
	movdqa	%xmm9, %xmm1
	punpckhdq	%xmm11, %xmm9
	punpckldq	%xmm11, %xmm1
	movdqa	%xmm10, %xmm12
	paddq	%xmm1, %xmm0
	movdqa	%xmm2, %xmm1
	pcmpgtd	%xmm8, %xmm1
	paddq	%xmm9, %xmm0
	movdqa	%xmm8, %xmm9
	punpckldq	%xmm1, %xmm9
	punpckhdq	%xmm1, %xmm8
	movdqa	%xmm2, %xmm1
	pcmpgtd	%xmm10, %xmm1
	paddq	%xmm9, %xmm0
	paddq	%xmm8, %xmm0
	movdqa	%xmm10, %xmm8
	punpckldq	%xmm1, %xmm8
	punpckhdq	%xmm1, %xmm12
	movdqa	%xmm2, %xmm1
	pcmpgtd	%xmm7, %xmm1
	paddq	%xmm8, %xmm0
	movdqa	%xmm7, %xmm8
	paddq	%xmm12, %xmm0
	punpckldq	%xmm1, %xmm8
	punpckhdq	%xmm1, %xmm7
	paddq	%xmm8, %xmm0
	paddq	%xmm7, %xmm0
	cmpq	%rcx, %rax
	jne	.L81
	movdqa	%xmm0, %xmm1
	movl	%edx, %eax
	psrldq	$8, %xmm1
	andl	$-16, %eax
	paddq	%xmm1, %xmm0
	movq	%xmm0, %r15
	testb	$15, %dl
	je	.L60
.L80:
	leal	(%rsi,%rax), %ecx
	movslq	%ecx, %rcx
	cmpb	$10, (%rdi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addq	%rcx, %r15
	leal	1(%rax), %ecx
	cmpl	%edx, %ecx
	jge	.L60
	addl	%esi, %ecx
	movslq	%ecx, %rcx
	cmpb	$10, (%rdi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addq	%rcx, %r15
	leal	2(%rax), %ecx
	cmpl	%edx, %ecx
	jge	.L60
	addl	%esi, %ecx
	movslq	%ecx, %rcx
	cmpb	$10, (%rdi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addq	%rcx, %r15
	leal	3(%rax), %ecx
	cmpl	%ecx, %edx
	jle	.L60
	addl	%esi, %ecx
	movslq	%ecx, %rcx
	cmpb	$10, (%rdi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addq	%rcx, %r15
	leal	4(%rax), %ecx
	cmpl	%ecx, %edx
	jle	.L60
	addl	%esi, %ecx
	movslq	%ecx, %rcx
	cmpb	$10, (%rdi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addq	%rcx, %r15
	leal	5(%rax), %ecx
	cmpl	%ecx, %edx
	jle	.L60
	addl	%esi, %ecx
	movslq	%ecx, %rcx
	cmpb	$10, (%rdi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addq	%rcx, %r15
	leal	6(%rax), %ecx
	cmpl	%ecx, %edx
	jle	.L60
	addl	%esi, %ecx
	movslq	%ecx, %rcx
	cmpb	$10, (%rdi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addq	%rcx, %r15
	leal	7(%rax), %ecx
	cmpl	%ecx, %edx
	jle	.L60
	addl	%esi, %ecx
	movslq	%ecx, %rcx
	cmpb	$10, (%rdi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addq	%rcx, %r15
	leal	8(%rax), %ecx
	cmpl	%ecx, %edx
	jle	.L60
	addl	%esi, %ecx
	movslq	%ecx, %rcx
	cmpb	$10, (%rdi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addq	%rcx, %r15
	leal	9(%rax), %ecx
	cmpl	%ecx, %edx
	jle	.L60
	addl	%esi, %ecx
	movslq	%ecx, %rcx
	cmpb	$10, (%rdi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addq	%rcx, %r15
	leal	10(%rax), %ecx
	cmpl	%ecx, %edx
	jle	.L60
	addl	%esi, %ecx
	movslq	%ecx, %rcx
	cmpb	$10, (%rdi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addq	%rcx, %r15
	leal	11(%rax), %ecx
	cmpl	%ecx, %edx
	jle	.L60
	addl	%esi, %ecx
	movslq	%ecx, %rcx
	cmpb	$10, (%rdi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addq	%rcx, %r15
	leal	12(%rax), %ecx
	cmpl	%ecx, %edx
	jle	.L60
	addl	%esi, %ecx
	movslq	%ecx, %rcx
	cmpb	$10, (%rdi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addq	%rcx, %r15
	leal	13(%rax), %ecx
	cmpl	%ecx, %edx
	jle	.L60
	addl	%esi, %ecx
	movslq	%ecx, %rcx
	cmpb	$10, (%rdi,%rcx)
	sete	%cl
	addl	$14, %eax
	movzbl	%cl, %ecx
	addq	%rcx, %r15
	cmpl	%eax, %edx
	jle	.L60
	addl	%esi, %eax
	cltq
	cmpb	$10, (%rdi,%rax)
	sete	%al
	movzbl	%al, %eax
	addq	%rax, %r15
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L105:
	movl	$1, %r15d
	cmpl	$4096, %r12d
	jle	.L60
	movq	8(%rbx), %r15
	movslq	%r13d, %r13
	cmpl	4(%rbx), %r12d
	je	.L109
.L113:
	movl	$326, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	movl	%ecx, -68(%rbp)
	call	CRYPTO_malloc@PLT
	movl	-68(%rbp), %ecx
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.L106
	cmpq	%r15, 8(%rbx)
	je	.L107
	movl	$329, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	call	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L107:
	movl	$407, %r8d
	movl	$65, %edx
	movl	$114, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L146:
	xorl	%r15d, %r15d
.L60:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L150
	addq	$40, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L63:
	.cfi_restore_state
	leaq	-57(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %r15
	call	buffer_read
	movslq	16(%rbx), %rax
	movslq	20(%rbx), %rsi
	movq	%r14, %rdi
	cmpq	%r13, %rax
	cmovle	%rax, %r15
	addq	8(%rbx), %rsi
	movq	%r15, %rdx
	call	memcpy@PLT
	jmp	.L60
.L62:
	movq	64(%r12), %rdi
	testq	%rdi, %rdi
	je	.L146
	movq	%r14, %rcx
	movq	%r13, %rdx
	call	BIO_ctrl@PLT
	movq	%rax, %r15
	jmp	.L60
.L71:
	movq	64(%rdi), %rdi
	movq	$0, 16(%rbx)
	movq	$0, 32(%rbx)
	testq	%rdi, %rdi
	je	.L146
	movq	%r14, %rcx
	movq	%r13, %rdx
	movl	$1, %esi
	call	BIO_ctrl@PLT
	movq	%rax, %r15
	jmp	.L60
.L70:
	movl	16(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L146
	movq	64(%rdi), %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movl	$2, %esi
	call	BIO_ctrl@PLT
	movq	%rax, %r15
	jmp	.L60
.L69:
	movslq	32(%rbx), %r15
	jmp	.L60
.L68:
	movslq	16(%rbx), %r15
	testq	%r15, %r15
	jne	.L60
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L146
	movq	%r14, %rcx
	movq	%r13, %rdx
	movl	$10, %esi
	call	BIO_ctrl@PLT
	movq	%rax, %r15
	jmp	.L60
.L67:
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L146
	movl	32(%rbx), %eax
	testl	%eax, %eax
	jg	.L110
	jmp	.L147
.L66:
	movslq	(%rbx), %rdx
	xorl	%ecx, %ecx
	movl	$117, %esi
	movq	%r14, %rdi
	call	BIO_int_ctrl@PLT
	testq	%rax, %rax
	je	.L146
	movslq	4(%rbx), %rdx
	movl	$1, %ecx
	movq	%r14, %rdi
	xorl	%r15d, %r15d
	movl	$117, %esi
	call	BIO_int_ctrl@PLT
	testq	%rax, %rax
	setne	%r15b
	jmp	.L60
.L65:
	movslq	32(%rbx), %r15
	testq	%r15, %r15
	jne	.L60
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L146
	movq	%r14, %rcx
	movq	%r13, %rdx
	movl	$13, %esi
	call	BIO_ctrl@PLT
	movq	%rax, %r15
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L151:
	movq	64(%r12), %rdi
	movslq	36(%rbx), %rsi
	addq	24(%rbx), %rsi
	call	BIO_write@PLT
	movq	%r12, %rdi
	movslq	%eax, %r15
	call	BIO_copy_next_retry@PLT
	testl	%r15d, %r15d
	jle	.L60
	addl	%r15d, 36(%rbx)
	subl	%r15d, 32(%rbx)
.L110:
	movl	$15, %esi
	movq	%r12, %rdi
	call	BIO_clear_flags@PLT
	movl	32(%rbx), %edx
	testl	%edx, %edx
	jg	.L151
	movq	$0, 32(%rbx)
	movq	64(%r12), %rdi
.L147:
	movq	%r14, %rcx
	movq	%r13, %rdx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movq	%rax, %r15
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L74:
	cmpq	$0, 64(%rdi)
	je	.L146
	movl	$15, %esi
	call	BIO_clear_flags@PLT
	movq	64(%r12), %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movl	$101, %esi
	call	BIO_ctrl@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	BIO_copy_next_retry@PLT
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L72:
	testq	%rcx, %rcx
	je	.L117
	movl	(%rcx), %edx
	testl	%edx, %edx
	je	.L152
	movl	(%rbx), %ecx
	movl	%r13d, %r12d
.L103:
	movq	24(%rbx), %r14
	movq	%r14, %rdi
	cmpl	$4096, %ecx
	jle	.L105
	cmpl	%ecx, (%rbx)
	je	.L105
	movslq	%r13d, %r13
	movl	$321, %edx
	leaq	.LC0(%rip), %rsi
	movl	%ecx, -68(%rbp)
	movq	%r13, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L107
	cmpl	$4096, %r12d
	movl	-68(%rbp), %ecx
	jle	.L106
	cmpl	%r12d, 4(%rbx)
	jne	.L113
.L106:
	movq	8(%rbx), %rdi
	cmpq	%r15, %rdi
	je	.L145
	movl	$334, %edx
	leaq	.LC0(%rip), %rsi
	movl	%ecx, -68(%rbp)
	call	CRYPTO_free@PLT
	movl	-68(%rbp), %ecx
	movq	%r15, 8(%rbx)
	movq	$0, 16(%rbx)
	movl	%ecx, (%rbx)
.L145:
	movq	24(%rbx), %rdi
.L109:
	cmpq	%r14, %rdi
	je	.L118
	movl	$341, %edx
	leaq	.LC0(%rip), %rsi
	movl	$1, %r15d
	call	CRYPTO_free@PLT
	movq	%r14, 24(%rbx)
	movq	$0, 32(%rbx)
	movl	%r12d, 4(%rbx)
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L149:
	movq	8(%rbx), %r12
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L152:
	movl	4(%rbx), %r12d
	movl	%r13d, %ecx
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L117:
	movl	%edx, %r12d
	movl	%edx, %ecx
	jmp	.L103
.L116:
	xorl	%eax, %eax
	xorl	%r15d, %r15d
	jmp	.L80
.L118:
	movl	$1, %r15d
	jmp	.L60
.L150:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE272:
	.size	buffer_ctrl, .-buffer_ctrl
	.p2align 4
	.type	buffer_free, @function
buffer_free:
.LFB269:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L157
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$80, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	56(%rdi), %r12
	movq	%rdi, %rbx
	movq	8(%r12), %rdi
	call	CRYPTO_free@PLT
	movq	24(%r12), %rdi
	movl	$81, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	56(%rbx), %rdi
	movl	$82, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 56(%rbx)
	movl	$1, %eax
	movl	$0, 32(%rbx)
	movl	$0, 40(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L157:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE269:
	.size	buffer_free, .-buffer_free
	.p2align 4
	.type	buffer_write.part.0, @function
buffer_write.part.0:
.LFB277:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	56(%rdi), %r14
	testq	%r14, %r14
	je	.L177
	xorl	%r13d, %r13d
	cmpq	$0, 64(%rdi)
	movq	%rdi, %r15
	je	.L162
	movq	%rsi, %r12
	movl	$15, %esi
	movl	%edx, %ebx
	call	BIO_clear_flags@PLT
	movl	4(%r14), %eax
	movl	32(%r14), %edx
	movl	36(%r14), %ecx
	movl	%eax, %r8d
	leal	(%rdx,%rcx), %edi
	subl	%edi, %r8d
	cmpl	%r8d, %ebx
	jle	.L191
.L165:
	testl	%edx, %edx
	jne	.L192
.L167:
	movl	$0, 36(%r14)
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L176:
	movq	64(%r15), %rdi
	movl	%ebx, %edx
	movq	%r12, %rsi
	call	BIO_write@PLT
	testl	%eax, %eax
	jle	.L193
	movslq	%eax, %rdx
	addl	%eax, %r13d
	addq	%rdx, %r12
	subl	%eax, %ebx
	je	.L162
	movl	4(%r14), %eax
.L173:
	cmpl	%eax, %ebx
	jge	.L176
.L166:
	endbr64
	movl	32(%r14), %edx
	movl	36(%r14), %ecx
	movl	%eax, %r8d
	leal	(%rdx,%rcx), %edi
	subl	%edi, %r8d
	cmpl	%ebx, %r8d
	jl	.L165
	addl	%ebx, %r13d
.L164:
	movslq	%edi, %rdi
	movslq	%ebx, %rdx
	addq	24(%r14), %rdi
	movq	%r12, %rsi
	call	memcpy@PLT
	addl	%ebx, 32(%r14)
.L162:
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L193:
	.cfi_restore_state
	movq	%r15, %rdi
	movl	%eax, -56(%rbp)
	call	BIO_copy_next_retry@PLT
	movl	-56(%rbp), %eax
	testl	%eax, %eax
	je	.L162
	testl	%r13d, %r13d
	cmove	%eax, %r13d
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L192:
	movq	24(%r14), %rsi
	testl	%r8d, %r8d
	jle	.L172
	movslq	%edi, %rdi
	movslq	%r8d, %rdx
	movl	%r8d, -60(%rbp)
	addq	%rsi, %rdi
	movq	%r12, %rsi
	movq	%rdx, -56(%rbp)
	call	memcpy@PLT
	movq	-56(%rbp), %rdx
	movl	-60(%rbp), %r8d
	movq	24(%r14), %rsi
	movl	36(%r14), %ecx
	addq	%rdx, %r12
	movl	32(%r14), %edx
	subl	%r8d, %ebx
	addl	%r8d, %r13d
	addl	%r8d, %edx
	movl	%edx, 32(%r14)
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L169:
	movl	36(%r14), %ecx
	movl	32(%r14), %edx
	addl	%eax, %ecx
	subl	%eax, %edx
	movl	%ecx, 36(%r14)
	movl	%edx, 32(%r14)
	je	.L194
	movq	24(%r14), %rsi
.L172:
	movslq	%ecx, %rcx
	movq	64(%r15), %rdi
	addq	%rcx, %rsi
	call	BIO_write@PLT
	testl	%eax, %eax
	jg	.L169
	movq	%r15, %rdi
	movl	%eax, -56(%rbp)
	call	BIO_copy_next_retry@PLT
	testl	%r13d, %r13d
	jne	.L162
	movl	-56(%rbp), %eax
	testl	%eax, %eax
	cmovne	%eax, %r13d
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L177:
	xorl	%r13d, %r13d
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L194:
	movl	4(%r14), %eax
	jmp	.L167
.L191:
	movl	%ebx, %r13d
	jmp	.L164
	.cfi_endproc
.LFE277:
	.size	buffer_write.part.0, .-buffer_write.part.0
	.p2align 4
	.type	buffer_write, @function
buffer_write:
.LFB271:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L196
	testl	%edx, %edx
	jle	.L196
	jmp	buffer_write.part.0
	.p2align 4,,10
	.p2align 3
.L196:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE271:
	.size	buffer_write, .-buffer_write
	.p2align 4
	.type	buffer_puts, @function
buffer_puts:
.LFB275:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	call	strlen@PLT
	testl	%eax, %eax
	jle	.L198
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	movl	%eax, %edx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	buffer_write.part.0
	.p2align 4,,10
	.p2align 3
.L198:
	.cfi_restore_state
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE275:
	.size	buffer_puts, .-buffer_puts
	.p2align 4
	.globl	BIO_f_buffer
	.type	BIO_f_buffer, @function
BIO_f_buffer:
.LFB267:
	.cfi_startproc
	endbr64
	leaq	methods_buffer(%rip), %rax
	ret
	.cfi_endproc
.LFE267:
	.size	BIO_f_buffer, .-BIO_f_buffer
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC3:
	.string	"buffer"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	methods_buffer, @object
	.size	methods_buffer, 96
methods_buffer:
	.long	521
	.zero	4
	.quad	.LC3
	.quad	bwrite_conv
	.quad	buffer_write
	.quad	bread_conv
	.quad	buffer_read
	.quad	buffer_puts
	.quad	buffer_gets
	.quad	buffer_ctrl
	.quad	buffer_new
	.quad	buffer_free
	.quad	buffer_callback_ctrl
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.align 16
.LC2:
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
