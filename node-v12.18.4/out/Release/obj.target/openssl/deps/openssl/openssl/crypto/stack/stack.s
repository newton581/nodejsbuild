	.file	"stack.c"
	.text
	.p2align 4
	.globl	OPENSSL_sk_set_cmp_func
	.type	OPENSSL_sk_set_cmp_func, @function
OPENSSL_sk_set_cmp_func:
.LFB419:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	cmpq	%rsi, %rax
	je	.L2
	movl	$0, 16(%rdi)
.L2:
	movq	%rsi, 24(%rdi)
	ret
	.cfi_endproc
.LFE419:
	.size	OPENSSL_sk_set_cmp_func, .-OPENSSL_sk_set_cmp_func
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/stack/stack.c"
	.text
	.p2align 4
	.globl	OPENSSL_sk_dup
	.type	OPENSSL_sk_dup, @function
OPENSSL_sk_dup:
.LFB420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$49, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$32, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L10
	movdqu	(%rbx), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%rbx), %xmm1
	movups	%xmm1, 16(%rax)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L7
	movq	$0, 8(%r12)
	movl	$0, 20(%r12)
.L4:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	movslq	20(%rbx), %rdi
	movl	$64, %edx
	leaq	.LC0(%rip), %rsi
	salq	$3, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, 8(%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L11
	movslq	(%rbx), %rdx
	movq	8(%rbx), %rsi
	salq	$3, %rdx
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movl	$50, %r8d
	movl	$65, %edx
	movl	$128, %esi
	movl	$15, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L11:
	movl	$376, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movl	$377, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L4
	.cfi_endproc
.LFE420:
	.size	OPENSSL_sk_dup, .-OPENSSL_sk_dup
	.p2align 4
	.globl	OPENSSL_sk_deep_copy
	.type	OPENSSL_sk_deep_copy, @function
OPENSSL_sk_deep_copy:
.LFB421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movl	$32, %edi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -56(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rdx, -64(%rbp)
	movl	$80, %edx
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L37
	movdqu	(%r15), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%r15), %xmm1
	movups	%xmm1, 16(%rax)
	movslq	(%r15), %rax
	testl	%eax, %eax
	jne	.L15
	movq	$0, 8(%r12)
	movl	$0, 20(%r12)
.L12:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	cmpl	$4, %eax
	movl	$4, %edi
	movl	$96, %edx
	cmovl	%rdi, %rax
	leaq	.LC0(%rip), %rsi
	movl	%eax, 20(%r12)
	leaq	0(,%rax,8), %rdi
	call	CRYPTO_zalloc@PLT
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L16
	movl	(%r12), %esi
	xorl	%ebx, %ebx
	testl	%esi, %esi
	jg	.L17
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L38:
	movl	(%r12), %esi
.L18:
	addq	$1, %rbx
	cmpl	%ebx, %esi
	jle	.L12
.L17:
	movq	8(%r15), %rdx
	movslq	%ebx, %r14
	leaq	0(,%rbx,8), %rax
	movq	(%rdx,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.L18
	addq	8(%r12), %rax
	movq	%rax, %r13
	movq	-56(%rbp), %rax
	call	*%rax
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	jne	.L38
	movq	8(%r12), %r8
	leal	-1(%r14), %r13d
	testl	%r14d, %r14d
	je	.L20
	leal	-1(%rbx), %eax
	movslq	%r13d, %r13
	subq	%rax, %r14
	salq	$3, %r13
	leaq	-16(,%r14,8), %rbx
	.p2align 4,,10
	.p2align 3
.L22:
	movq	(%r8,%r13), %rdi
	testq	%rdi, %rdi
	je	.L21
	movq	-64(%rbp), %rax
	call	*%rax
	movq	8(%r12), %r8
.L21:
	subq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L22
.L20:
	movq	%r8, %rdi
	movl	$376, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movl	$377, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L12
.L37:
	movl	$81, %r8d
	movl	$65, %edx
	movl	$127, %esi
	movl	$15, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L12
.L16:
	movq	%r12, %rdi
	movl	$98, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%r12d, %r12d
	call	CRYPTO_free@PLT
	jmp	.L12
	.cfi_endproc
.LFE421:
	.size	OPENSSL_sk_deep_copy, .-OPENSSL_sk_deep_copy
	.p2align 4
	.globl	OPENSSL_sk_new_null
	.type	OPENSSL_sk_new_null, @function
OPENSSL_sk_new_null:
.LFB422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$209, %edx
	movl	$32, %edi
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L39
	movq	$0, 24(%rax)
.L39:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE422:
	.size	OPENSSL_sk_new_null, .-OPENSSL_sk_new_null
	.p2align 4
	.globl	OPENSSL_sk_new
	.type	OPENSSL_sk_new, @function
OPENSSL_sk_new:
.LFB423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$209, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$32, %edi
	subq	$8, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L45
	movq	%rbx, 24(%rax)
.L45:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE423:
	.size	OPENSSL_sk_new, .-OPENSSL_sk_new
	.p2align 4
	.globl	OPENSSL_sk_new_reserve
	.type	OPENSSL_sk_new_reserve, @function
OPENSSL_sk_new_reserve:
.LFB426:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$209, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$32, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	%esi, %ebx
	leaq	.LC0(%rip), %rsi
	subq	$8, %rsp
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L51
	movq	%r13, 24(%rax)
	testl	%ebx, %ebx
	jle	.L51
	movl	(%rax), %esi
	movq	8(%rax), %rdi
	movl	$2147483647, %eax
	subl	%esi, %eax
	cmpl	%eax, %ebx
	jg	.L53
	addl	%esi, %ebx
	movl	$4, %esi
	cmpl	$4, %ebx
	cmovl	%esi, %ebx
	testq	%rdi, %rdi
	je	.L62
	cmpl	20(%r12), %ebx
	je	.L51
	movslq	%ebx, %rsi
	movl	$198, %ecx
	leaq	.LC0(%rip), %rdx
	salq	$3, %rsi
	call	CRYPTO_realloc@PLT
	testq	%rax, %rax
	je	.L63
	movq	%rax, 8(%r12)
.L61:
	movl	%ebx, 20(%r12)
.L51:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	movslq	%ebx, %rdi
	movl	$180, %edx
	leaq	.LC0(%rip), %rsi
	salq	$3, %rdi
	call	CRYPTO_zalloc@PLT
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	jne	.L61
	movl	$15, %edi
	movl	$181, %r8d
	movl	$65, %edx
	movl	$129, %esi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	8(%r12), %rdi
	.p2align 4,,10
	.p2align 3
.L53:
	movl	$376, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movl	$377, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L63:
	movq	8(%r12), %rdi
	jmp	.L53
	.cfi_endproc
.LFE426:
	.size	OPENSSL_sk_new_reserve, .-OPENSSL_sk_new_reserve
	.p2align 4
	.globl	OPENSSL_sk_reserve
	.type	OPENSSL_sk_reserve, @function
OPENSSL_sk_reserve:
.LFB427:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testq	%rdi, %rdi
	je	.L76
	movl	$1, %r13d
	testl	%esi, %esi
	js	.L64
	movl	(%rdi), %ebx
	movl	$2147483647, %eax
	movq	%rdi, %r12
	subl	%ebx, %eax
	cmpl	%eax, %esi
	jg	.L76
	addl	%ebx, %esi
	movq	8(%rdi), %rdi
	movl	$4, %ebx
	cmpl	$4, %esi
	cmovge	%esi, %ebx
	testq	%rdi, %rdi
	je	.L78
	cmpl	20(%r12), %ebx
	je	.L64
	movslq	%ebx, %rsi
	movl	$198, %ecx
	leaq	.LC0(%rip), %rdx
	salq	$3, %rsi
	call	CRYPTO_realloc@PLT
	testq	%rax, %rax
	je	.L76
	movq	%rax, 8(%r12)
.L77:
	movl	%ebx, 20(%r12)
.L64:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	movslq	%ebx, %rdi
	movl	$180, %edx
	leaq	.LC0(%rip), %rsi
	salq	$3, %rdi
	call	CRYPTO_zalloc@PLT
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	jne	.L77
	movl	$181, %r8d
	movl	$65, %edx
	movl	$129, %esi
	movl	$15, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L76:
	addq	$8, %rsp
	xorl	%r13d, %r13d
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE427:
	.size	OPENSSL_sk_reserve, .-OPENSSL_sk_reserve
	.p2align 4
	.globl	OPENSSL_sk_insert
	.type	OPENSSL_sk_insert, @function
OPENSSL_sk_insert:
.LFB428:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L104
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	(%rdi), %edx
	cmpl	$2147483647, %edx
	je	.L103
	leal	1(%rdx), %eax
	movq	8(%rdi), %rdi
	movl	$4, %r14d
	movq	%rsi, %r13
	cmpl	$4, %eax
	cmovge	%eax, %r14d
	testq	%rdi, %rdi
	je	.L105
	movl	20(%rbx), %r15d
	cmpl	%r15d, %r14d
	jle	.L85
.L87:
	cmpl	$1431655764, %r15d
	jle	.L106
	movabsq	$17179869176, %rsi
	movl	$2147483647, %r15d
.L86:
	movl	$198, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_realloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L103
	movq	%rax, 8(%rbx)
	movl	(%rbx), %edx
	movl	%r15d, 20(%rbx)
.L85:
	cmpl	%r12d, %edx
	jle	.L92
	testl	%r12d, %r12d
	jns	.L88
.L92:
	movslq	%edx, %rax
	movq	%r13, (%rdi,%rax,8)
.L90:
	leal	1(%rdx), %eax
	movl	$0, 16(%rbx)
	movl	%eax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	movl	$181, %r8d
	movl	$65, %edx
	movl	$129, %esi
	movl	$15, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L103:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore_state
	movslq	%r14d, %rdi
	movl	$180, %edx
	leaq	.LC0(%rip), %rsi
	salq	$3, %rdi
	call	CRYPTO_zalloc@PLT
	movq	%rax, 8(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L107
	movl	%r14d, 20(%rbx)
	movl	(%rbx), %edx
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L88:
	movslq	%r12d, %r14
	subl	%r12d, %edx
	leaq	0(,%r14,8), %rax
	movslq	%edx, %rdx
	salq	$3, %rdx
	leaq	(%rdi,%rax), %rsi
	leaq	8(%rdi,%rax), %rdi
	call	memmove@PLT
	movq	8(%rbx), %rax
	movl	(%rbx), %edx
	movq	%r13, (%rax,%r14,8)
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L106:
	movl	%r15d, %eax
	shrl	$31, %eax
	addl	%r15d, %eax
	sarl	%eax
	addl	%eax, %r15d
	cmpl	%r15d, %r14d
	jg	.L87
	movslq	%r15d, %rsi
	salq	$3, %rsi
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE428:
	.size	OPENSSL_sk_insert, .-OPENSSL_sk_insert
	.p2align 4
	.globl	OPENSSL_sk_delete_ptr
	.type	OPENSSL_sk_delete_ptr, @function
OPENSSL_sk_delete_ptr:
.LFB430:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movl	(%rdi), %esi
	testl	%esi, %esi
	jle	.L113
	movq	8(%rdi), %r9
	leal	-1(%rsi), %ecx
	movq	%rdi, %r12
	xorl	%eax, %eax
	movq	%rcx, %r8
	movq	%r9, %rdi
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L110:
	leaq	1(%rax), %rdx
	addq	$8, %rdi
	cmpq	%rcx, %rax
	je	.L113
	movq	%rdx, %rax
.L112:
	cmpq	%rbx, (%rdi)
	jne	.L110
	cmpl	%r8d, %eax
	je	.L111
	subl	%eax, %esi
	leal	-1(%rsi), %edx
	leaq	8(%r9,%rax,8), %rsi
	movslq	%edx, %rdx
	salq	$3, %rdx
	call	memmove@PLT
	movl	(%r12), %eax
	leal	-1(%rax), %r8d
.L111:
	movl	%r8d, (%r12)
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE430:
	.size	OPENSSL_sk_delete_ptr, .-OPENSSL_sk_delete_ptr
	.p2align 4
	.globl	OPENSSL_sk_delete
	.type	OPENSSL_sk_delete, @function
OPENSSL_sk_delete:
.LFB431:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	testq	%rdi, %rdi
	je	.L119
	testl	%esi, %esi
	js	.L119
	movl	(%rdi), %edx
	movq	%rdi, %rbx
	xorl	%r12d, %r12d
	cmpl	%esi, %edx
	jle	.L116
	movq	8(%rdi), %r8
	movslq	%esi, %rax
	leal	-1(%rdx), %ecx
	salq	$3, %rax
	leaq	(%r8,%rax), %rdi
	movq	(%rdi), %r12
	cmpl	%ecx, %esi
	jne	.L122
.L118:
	movl	%ecx, (%rbx)
.L116:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_restore_state
	subl	%esi, %edx
	leaq	8(%r8,%rax), %rsi
	subl	$1, %edx
	movslq	%edx, %rdx
	salq	$3, %rdx
	call	memmove@PLT
	movl	(%rbx), %eax
	leal	-1(%rax), %ecx
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L119:
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE431:
	.size	OPENSSL_sk_delete, .-OPENSSL_sk_delete
	.p2align 4
	.globl	OPENSSL_sk_find
	.type	OPENSSL_sk_find, @function
OPENSSL_sk_find:
.LFB433:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -32(%rbp)
	testq	%rdi, %rdi
	je	.L126
	movl	(%rdi), %eax
	movq	%rdi, %rbx
	testl	%eax, %eax
	je	.L126
	movq	24(%rdi), %rcx
	testq	%rcx, %rcx
	je	.L143
	movl	16(%rdi), %edx
	testl	%edx, %edx
	je	.L144
.L129:
	testq	%rsi, %rsi
	je	.L126
	movq	8(%rbx), %rsi
	movl	(%rbx), %edx
	leaq	-32(%rbp), %rdi
	movl	$2, %r9d
	movq	24(%rbx), %r8
	movl	$8, %ecx
	call	OBJ_bsearch_ex_@PLT
	testq	%rax, %rax
	je	.L126
	subq	8(%rbx), %rax
	sarq	$3, %rax
.L123:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L145
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	cmpl	$1, %eax
	jle	.L130
	movq	8(%rdi), %rdi
	movslq	%eax, %rsi
	movl	$8, %edx
	call	qsort@PLT
	movq	-32(%rbp), %rsi
.L130:
	movl	$1, 16(%rbx)
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L143:
	testl	%eax, %eax
	jle	.L126
	movq	8(%rdi), %rdi
	leal	-1(%rax), %ecx
	xorl	%edx, %edx
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L132:
	movq	%rax, %rdx
.L128:
	movl	%edx, %eax
	cmpq	(%rdi,%rdx,8), %rsi
	je	.L123
	leaq	1(%rdx), %rax
	cmpq	%rdx, %rcx
	jne	.L132
.L126:
	movl	$-1, %eax
	jmp	.L123
.L145:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE433:
	.size	OPENSSL_sk_find, .-OPENSSL_sk_find
	.p2align 4
	.globl	OPENSSL_sk_find_ex
	.type	OPENSSL_sk_find_ex, @function
OPENSSL_sk_find_ex:
.LFB434:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -32(%rbp)
	testq	%rdi, %rdi
	je	.L149
	movl	(%rdi), %eax
	movq	%rdi, %rbx
	testl	%eax, %eax
	je	.L149
	movq	24(%rdi), %rcx
	testq	%rcx, %rcx
	je	.L166
	movl	16(%rdi), %edx
	testl	%edx, %edx
	je	.L167
.L152:
	testq	%rsi, %rsi
	je	.L149
	movq	8(%rbx), %rsi
	movl	(%rbx), %edx
	leaq	-32(%rbp), %rdi
	movl	$1, %r9d
	movq	24(%rbx), %r8
	movl	$8, %ecx
	call	OBJ_bsearch_ex_@PLT
	testq	%rax, %rax
	je	.L149
	subq	8(%rbx), %rax
	sarq	$3, %rax
.L146:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L168
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L167:
	.cfi_restore_state
	cmpl	$1, %eax
	jle	.L153
	movq	8(%rdi), %rdi
	movslq	%eax, %rsi
	movl	$8, %edx
	call	qsort@PLT
	movq	-32(%rbp), %rsi
.L153:
	movl	$1, 16(%rbx)
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L166:
	testl	%eax, %eax
	jle	.L149
	movq	8(%rdi), %rdi
	leal	-1(%rax), %ecx
	xorl	%edx, %edx
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L155:
	movq	%rax, %rdx
.L151:
	movl	%edx, %eax
	cmpq	(%rdi,%rdx,8), %rsi
	je	.L146
	leaq	1(%rdx), %rax
	cmpq	%rdx, %rcx
	jne	.L155
.L149:
	movl	$-1, %eax
	jmp	.L146
.L168:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE434:
	.size	OPENSSL_sk_find_ex, .-OPENSSL_sk_find_ex
	.p2align 4
	.globl	OPENSSL_sk_push
	.type	OPENSSL_sk_push, @function
OPENSSL_sk_push:
.LFB435:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L182
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	(%rdi), %r12
	cmpl	$2147483647, %r12d
	je	.L193
	leal	1(%r12), %eax
	movq	8(%rdi), %rdi
	movl	$4, %r14d
	movq	%rsi, %r15
	cmpl	$4, %eax
	cmovge	%eax, %r14d
	testq	%rdi, %rdi
	je	.L194
	movl	20(%rbx), %r13d
	cmpl	%r13d, %r14d
	jle	.L175
.L177:
	cmpl	$1431655764, %r13d
	jle	.L195
	movabsq	$17179869176, %rsi
	movl	$2147483647, %r13d
.L176:
	movl	$198, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_realloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L193
	movq	%rax, 8(%rbx)
	movl	%r13d, 20(%rbx)
.L174:
	movl	(%rbx), %edx
	cmpl	%edx, %r12d
	jge	.L184
	testl	%r12d, %r12d
	jns	.L179
.L184:
	leal	1(%rdx), %eax
	movslq	%edx, %r12
.L175:
	movq	%r15, (%rdi,%r12,8)
.L181:
	movl	%eax, (%rbx)
	movl	$0, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L196:
	.cfi_restore_state
	movl	$181, %r8d
	movl	$65, %edx
	movl	$129, %esi
	movl	$15, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L193:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L194:
	.cfi_restore_state
	movslq	%r14d, %rdi
	movl	$180, %edx
	leaq	.LC0(%rip), %rsi
	salq	$3, %rdi
	call	CRYPTO_zalloc@PLT
	movq	%rax, 8(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L196
	movl	%r14d, 20(%rbx)
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L195:
	movl	%r13d, %eax
	shrl	$31, %eax
	addl	%r13d, %eax
	sarl	%eax
	addl	%eax, %r13d
	cmpl	%r13d, %r14d
	jg	.L177
	movslq	%r13d, %rsi
	salq	$3, %rsi
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L179:
	movslq	%r12d, %r13
	subl	%r12d, %edx
	leaq	0(,%r13,8), %rax
	movslq	%edx, %rdx
	leaq	(%rdi,%rax), %rsi
	salq	$3, %rdx
	leaq	8(%rdi,%rax), %rdi
	call	memmove@PLT
	movq	8(%rbx), %rax
	movq	%r15, (%rax,%r13,8)
	movl	(%rbx), %eax
	addl	$1, %eax
	jmp	.L181
.L182:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE435:
	.size	OPENSSL_sk_push, .-OPENSSL_sk_push
	.p2align 4
	.globl	OPENSSL_sk_unshift
	.type	OPENSSL_sk_unshift, @function
OPENSSL_sk_unshift:
.LFB436:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L220
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	(%rdi), %edx
	movq	%rdi, %rbx
	cmpl	$2147483647, %edx
	je	.L219
	leal	1(%rdx), %eax
	movq	8(%rdi), %rdi
	movl	$4, %r13d
	movq	%rsi, %r12
	cmpl	$4, %eax
	cmovge	%eax, %r13d
	testq	%rdi, %rdi
	je	.L221
	movl	20(%rbx), %r14d
	cmpl	%r14d, %r13d
	jle	.L203
.L205:
	cmpl	$1431655764, %r14d
	jle	.L222
	movabsq	$17179869176, %rsi
	movl	$2147483647, %r14d
.L204:
	movl	$198, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_realloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L219
	movq	%rax, 8(%rbx)
	movl	(%rbx), %edx
	movl	%r14d, 20(%rbx)
.L203:
	movslq	%edx, %rax
	leaq	0(,%rax,8), %r8
	testl	%edx, %edx
	jle	.L223
	leaq	8(%rdi), %r9
	movq	%r8, %rdx
	movq	%rdi, %rsi
	movq	%r9, %rdi
	call	memmove@PLT
	movq	8(%rbx), %rax
	movl	(%rbx), %edx
	movq	%r12, (%rax)
.L207:
	leal	1(%rdx), %eax
	movl	$0, 16(%rbx)
	movl	%eax, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L224:
	.cfi_restore_state
	movl	$181, %r8d
	movl	$65, %edx
	movl	$129, %esi
	movl	$15, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L219:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	.cfi_restore_state
	movq	%r12, (%rdi,%rax,8)
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L221:
	movslq	%r13d, %rdi
	movl	$180, %edx
	leaq	.LC0(%rip), %rsi
	salq	$3, %rdi
	call	CRYPTO_zalloc@PLT
	movq	%rax, 8(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L224
	movl	%r13d, 20(%rbx)
	movl	(%rbx), %edx
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L222:
	movl	%r14d, %eax
	shrl	$31, %eax
	addl	%r14d, %eax
	sarl	%eax
	addl	%eax, %r14d
	cmpl	%r14d, %r13d
	jg	.L205
	movslq	%r14d, %rsi
	salq	$3, %rsi
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L220:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE436:
	.size	OPENSSL_sk_unshift, .-OPENSSL_sk_unshift
	.p2align 4
	.globl	OPENSSL_sk_shift
	.type	OPENSSL_sk_shift, @function
OPENSSL_sk_shift:
.LFB437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	testq	%rdi, %rdi
	je	.L228
	movl	(%rdi), %eax
	movq	%rdi, %rbx
	xorl	%r12d, %r12d
	testl	%eax, %eax
	je	.L225
	movq	8(%rdi), %rdi
	leal	-1(%rax), %edx
	xorl	%ecx, %ecx
	movq	(%rdi), %r12
	cmpl	$1, %eax
	jne	.L234
.L227:
	movl	%ecx, (%rbx)
.L225:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L234:
	.cfi_restore_state
	movslq	%edx, %rdx
	leaq	8(%rdi), %rsi
	salq	$3, %rdx
	call	memmove@PLT
	movl	(%rbx), %eax
	leal	-1(%rax), %ecx
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L228:
	xorl	%r12d, %r12d
	jmp	.L225
	.cfi_endproc
.LFE437:
	.size	OPENSSL_sk_shift, .-OPENSSL_sk_shift
	.p2align 4
	.globl	OPENSSL_sk_pop
	.type	OPENSSL_sk_pop, @function
OPENSSL_sk_pop:
.LFB438:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L237
	movl	(%rdi), %eax
	xorl	%r8d, %r8d
	testl	%eax, %eax
	je	.L235
	subl	$1, %eax
	movq	8(%rdi), %rdx
	movslq	%eax, %rcx
	movq	(%rdx,%rcx,8), %r8
	movl	%eax, (%rdi)
.L235:
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L237:
	xorl	%r8d, %r8d
	jmp	.L235
	.cfi_endproc
.LFE438:
	.size	OPENSSL_sk_pop, .-OPENSSL_sk_pop
	.p2align 4
	.globl	OPENSSL_sk_zero
	.type	OPENSSL_sk_zero, @function
OPENSSL_sk_zero:
.LFB439:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L249
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	(%rdi), %rdx
	testl	%edx, %edx
	jne	.L252
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L252:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	salq	$3, %rdx
	xorl	%esi, %esi
	call	memset@PLT
	movl	$0, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L249:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE439:
	.size	OPENSSL_sk_zero, .-OPENSSL_sk_zero
	.p2align 4
	.globl	OPENSSL_sk_pop_free
	.type	OPENSSL_sk_pop_free, @function
OPENSSL_sk_pop_free:
.LFB440:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L253
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movl	(%rdi), %eax
	movq	8(%rdi), %r8
	testl	%eax, %eax
	jle	.L255
	movq	%rsi, %r13
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L259:
	movq	(%r8,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.L256
	call	*%r13
	movl	(%r12), %eax
	addq	$1, %rbx
	movq	8(%r12), %r8
	cmpl	%ebx, %eax
	jg	.L259
.L255:
	movl	$376, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r8, %rdi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$377, %edx
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	leaq	.LC0(%rip), %rsi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L256:
	.cfi_restore_state
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jg	.L259
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L253:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE440:
	.size	OPENSSL_sk_pop_free, .-OPENSSL_sk_pop_free
	.p2align 4
	.globl	OPENSSL_sk_free
	.type	OPENSSL_sk_free, @function
OPENSSL_sk_free:
.LFB441:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L263
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$376, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$377, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L263:
	ret
	.cfi_endproc
.LFE441:
	.size	OPENSSL_sk_free, .-OPENSSL_sk_free
	.p2align 4
	.globl	OPENSSL_sk_num
	.type	OPENSSL_sk_num, @function
OPENSSL_sk_num:
.LFB442:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L270
	movl	(%rdi), %eax
	ret
.L270:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE442:
	.size	OPENSSL_sk_num, .-OPENSSL_sk_num
	.p2align 4
	.globl	OPENSSL_sk_value
	.type	OPENSSL_sk_value, @function
OPENSSL_sk_value:
.LFB443:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L274
	testl	%esi, %esi
	js	.L274
	cmpl	%esi, (%rdi)
	jle	.L274
	movq	8(%rdi), %rax
	movslq	%esi, %rsi
	movq	(%rax,%rsi,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L274:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE443:
	.size	OPENSSL_sk_value, .-OPENSSL_sk_value
	.p2align 4
	.globl	OPENSSL_sk_set
	.type	OPENSSL_sk_set, @function
OPENSSL_sk_set:
.LFB444:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L278
	testl	%esi, %esi
	js	.L278
	cmpl	%esi, (%rdi)
	jle	.L278
	movq	8(%rdi), %rax
	movslq	%esi, %rsi
	movq	%rdx, (%rax,%rsi,8)
	movq	8(%rdi), %rax
	movl	$0, 16(%rdi)
	movq	(%rax,%rsi,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L278:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE444:
	.size	OPENSSL_sk_set, .-OPENSSL_sk_set
	.p2align 4
	.globl	OPENSSL_sk_sort
	.type	OPENSSL_sk_sort, @function
OPENSSL_sk_sort:
.LFB445:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L289
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	16(%rdi), %eax
	testl	%eax, %eax
	jne	.L279
	movq	24(%rdi), %rcx
	testq	%rcx, %rcx
	je	.L279
	movslq	(%rdi), %rsi
	cmpl	$1, %esi
	jg	.L292
	movl	$1, 16(%rbx)
.L279:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L289:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L292:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	8(%rdi), %rdi
	movl	$8, %edx
	call	qsort@PLT
	movl	$1, 16(%rbx)
	jmp	.L279
	.cfi_endproc
.LFE445:
	.size	OPENSSL_sk_sort, .-OPENSSL_sk_sort
	.p2align 4
	.globl	OPENSSL_sk_is_sorted
	.type	OPENSSL_sk_is_sorted, @function
OPENSSL_sk_is_sorted:
.LFB446:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	testq	%rdi, %rdi
	je	.L293
	movl	16(%rdi), %eax
.L293:
	ret
	.cfi_endproc
.LFE446:
	.size	OPENSSL_sk_is_sorted, .-OPENSSL_sk_is_sorted
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
