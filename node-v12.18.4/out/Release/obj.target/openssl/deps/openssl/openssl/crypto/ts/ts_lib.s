	.file	"ts_lib.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"0x"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../deps/openssl/openssl/crypto/ts/ts_lib.c"
	.text
	.p2align 4
	.globl	TS_ASN1_INTEGER_print_bio
	.type	TS_ASN1_INTEGER_print_bio, @function
TS_ASN1_INTEGER_print_bio:
.LFB1368:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	$-1, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -48
	call	ASN1_INTEGER_to_BN@PLT
	testq	%rax, %rax
	je	.L1
	movq	%rax, %rdi
	movq	%rax, %r12
	xorl	%r15d, %r15d
	call	BN_bn2hex@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L3
	movl	$2, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jle	.L4
	movq	%r14, %rdi
	xorl	%r15d, %r15d
	call	strlen@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	BIO_write@PLT
	testl	%eax, %eax
	setg	%r15b
.L4:
	movl	$31, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
.L3:
	movq	%r12, %rdi
	call	BN_free@PLT
.L1:
	popq	%r12
	movl	%r15d, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1368:
	.size	TS_ASN1_INTEGER_print_bio, .-TS_ASN1_INTEGER_print_bio
	.section	.rodata.str1.1
.LC2:
	.string	"%s\n"
	.text
	.p2align 4
	.globl	TS_OBJ_print_bio
	.type	TS_OBJ_print_bio, @function
TS_OBJ_print_bio:
.LFB1369:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	xorl	%ecx, %ecx
	movl	$128, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-160(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$144, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	OBJ_obj2txt@PLT
	xorl	%eax, %eax
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	BIO_printf@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L16
	addq	$144, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L16:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1369:
	.size	TS_OBJ_print_bio, .-TS_OBJ_print_bio
	.section	.rodata.str1.1
.LC3:
	.string	" critical"
.LC4:
	.string	""
.LC5:
	.string	"Extensions:\n"
.LC6:
	.string	":%s\n"
.LC7:
	.string	"%4s"
.LC8:
	.string	"\n"
	.text
	.p2align 4
	.globl	TS_ext_print_bio
	.type	TS_ext_print_bio, @function
TS_ext_print_bio:
.LFB1370:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	leaq	.LC5(%rip), %rsi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	BIO_printf@PLT
	movq	%r14, %rdi
	call	X509v3_get_ext_count@PLT
	movl	%eax, -52(%rbp)
	testl	%eax, %eax
	jle	.L18
	xorl	%r13d, %r13d
	leaq	.LC4(%rip), %r15
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L21:
	movl	$1, %edx
	leaq	.LC8(%rip), %rsi
	movq	%rbx, %rdi
	addl	$1, %r13d
	call	BIO_write@PLT
	cmpl	%r13d, -52(%rbp)
	je	.L18
.L23:
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	X509v3_get_ext@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	X509_EXTENSION_get_object@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	i2a_ASN1_OBJECT@PLT
	testl	%eax, %eax
	js	.L24
	movq	%r12, %rdi
	call	X509_EXTENSION_get_critical@PLT
	leaq	.LC3(%rip), %rdx
	leaq	.LC6(%rip), %rsi
	movq	%rbx, %rdi
	testl	%eax, %eax
	cmove	%r15, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	xorl	%edx, %edx
	movl	$4, %ecx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	X509V3_EXT_print@PLT
	testl	%eax, %eax
	jne	.L21
	movq	%r15, %rdx
	leaq	.LC7(%rip), %rsi
	movq	%rbx, %rdi
	call	BIO_printf@PLT
	movq	%r12, %rdi
	call	X509_EXTENSION_get_data@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	ASN1_STRING_print@PLT
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L18:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1370:
	.size	TS_ext_print_bio, .-TS_ext_print_bio
	.section	.rodata.str1.1
.LC9:
	.string	"UNKNOWN"
.LC10:
	.string	"Hash Algorithm: %s\n"
	.text
	.p2align 4
	.globl	TS_X509_ALGOR_print_bio
	.type	TS_X509_ALGOR_print_bio, @function
TS_X509_ALGOR_print_bio:
.LFB1371:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rsi), %rdi
	call	OBJ_obj2nid@PLT
	leaq	.LC9(%rip), %rdx
	testl	%eax, %eax
	jne	.L33
.L28:
	addq	$8, %rsp
	movq	%r12, %rdi
	leaq	.LC10(%rip), %rsi
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_printf@PLT
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	movl	%eax, %edi
	call	OBJ_nid2ln@PLT
	movq	%rax, %rdx
	jmp	.L28
	.cfi_endproc
.LFE1371:
	.size	TS_X509_ALGOR_print_bio, .-TS_X509_ALGOR_print_bio
	.section	.rodata.str1.1
.LC11:
	.string	"Message data:\n"
	.text
	.p2align 4
	.globl	TS_MSG_IMPRINT_print_bio
	.type	TS_MSG_IMPRINT_print_bio, @function
TS_MSG_IMPRINT_print_bio:
.LFB1372:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rsi), %rax
	movq	%rsi, %rbx
	movq	(%rax), %rdi
	call	OBJ_obj2nid@PLT
	leaq	.LC9(%rip), %rdx
	testl	%eax, %eax
	jne	.L40
.L35:
	movq	%r12, %rdi
	leaq	.LC10(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	8(%rbx), %r14
	movq	%r14, %rdi
	call	ASN1_STRING_length@PLT
	movq	%r14, %rdi
	movl	%eax, %r13d
	call	ASN1_STRING_get0_data@PLT
	movl	%r13d, %edx
	movq	%r12, %rdi
	movl	$4, %ecx
	movq	%rax, %rsi
	call	BIO_dump_indent@PLT
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	movl	%eax, %edi
	call	OBJ_nid2ln@PLT
	movq	%rax, %rdx
	jmp	.L35
	.cfi_endproc
.LFE1372:
	.size	TS_MSG_IMPRINT_print_bio, .-TS_MSG_IMPRINT_print_bio
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
