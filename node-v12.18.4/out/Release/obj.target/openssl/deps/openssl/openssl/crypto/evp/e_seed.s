	.file	"e_seed.c"
	.text
	.p2align 4
	.type	seed_cbc_cipher, @function
seed_cbc_cipher:
.LFB408:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -80(%rbp)
	movabsq	$4611686018427387903, %rdx
	movq	%rsi, -72(%rbp)
	cmpq	%rdx, %rcx
	jbe	.L2
	movq	%rcx, %r14
	addq	%r12, %rax
	leaq	(%rsi,%rcx), %rcx
	movq	%rdx, %r12
	movq	%rcx, -56(%rbp)
	movq	%rax, -64(%rbp)
	movq	%r14, -88(%rbp)
.L3:
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r13, %rdi
	movl	%eax, %r15d
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movl	%r15d, %r9d
	movabsq	$4611686018427387904, %rdx
	movq	%rax, %rcx
	movq	%rbx, %r8
	subq	%r14, %rsi
	subq	%r14, %rdi
	call	SEED_cbc_encrypt@PLT
	movabsq	$-4611686018427387904, %rax
	addq	%rax, %r14
	cmpq	%r12, %r14
	ja	.L3
	movq	-88(%rbp), %r14
	leaq	(%r14,%rax), %r10
	andq	%r14, %r12
	shrq	$62, %r10
	addq	$1, %r10
	salq	$62, %r10
	addq	%r10, -80(%rbp)
	addq	%r10, -72(%rbp)
.L2:
	testq	%r12, %r12
	jne	.L13
.L4:
	addq	$56, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r13, %rdi
	movl	%eax, %r15d
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	-72(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movl	%r15d, %r9d
	movq	%rax, %rcx
	movq	%rbx, %r8
	movq	%r12, %rdx
	call	SEED_cbc_encrypt@PLT
	jmp	.L4
	.cfi_endproc
.LFE408:
	.size	seed_cbc_cipher, .-seed_cbc_cipher
	.p2align 4
	.type	seed_init_key, @function
seed_init_key:
.LFB416:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	SEED_set_key@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE416:
	.size	seed_init_key, .-seed_init_key
	.p2align 4
	.type	seed_cfb128_cipher, @function
seed_cfb128_cipher:
.LFB409:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, %rcx
	ja	.L22
	testq	%rcx, %rcx
	je	.L19
	call	EVP_CIPHER_CTX_num@PLT
	movq	%r15, %rdi
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r15, %rdi
	movl	%eax, -72(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movl	-72(%rbp), %edx
	subq	$8, %rsp
	leaq	-60(%rbp), %r9
	movq	%rax, %rcx
	movq	%rbx, %r8
	movq	%r14, %rsi
	movq	%r13, %rdi
	pushq	%rdx
	movq	%r12, %rdx
	call	SEED_cfb128_encrypt@PLT
	movl	-60(%rbp), %esi
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	popq	%rax
	popq	%rdx
.L19:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L29
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	movabsq	$4611686018427387904, %rbx
	leaq	-60(%rbp), %rax
	movq	%rax, -88(%rbp)
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L30:
	cmpq	%rbx, %r12
	jb	.L19
.L17:
	movq	%r15, %rdi
	subq	%rbx, %r12
	call	EVP_CIPHER_CTX_num@PLT
	movq	%r15, %rdi
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r15, %rdi
	movl	%eax, -76(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r15, %rdi
	movq	%rax, -72(%rbp)
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movl	-76(%rbp), %edx
	subq	$8, %rsp
	movq	-88(%rbp), %r9
	movq	-72(%rbp), %r8
	movq	%rax, %rcx
	movq	%r14, %rsi
	movq	%r13, %rdi
	pushq	%rdx
	movq	%rbx, %rdx
	addq	%rbx, %r13
	addq	%rbx, %r14
	call	SEED_cfb128_encrypt@PLT
	movl	-60(%rbp), %esi
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	cmpq	%r12, %rbx
	popq	%rcx
	popq	%rsi
	cmova	%r12, %rbx
	testq	%r12, %r12
	jne	.L30
	jmp	.L19
.L29:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE409:
	.size	seed_cfb128_cipher, .-seed_cfb128_cipher
	.p2align 4
	.type	seed_ofb_cipher, @function
seed_ofb_cipher:
.LFB411:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -96(%rbp)
	movabsq	$4611686018427387903, %rdx
	movq	%rsi, -88(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	movq	%rcx, %rbx
	cmpq	%rdx, %rcx
	jbe	.L32
	addq	%rcx, %rax
	movq	%rcx, -104(%rbp)
	leaq	(%rsi,%rcx), %rbx
	movq	%rcx, %r13
	movq	%rax, -80(%rbp)
	leaq	-60(%rbp), %r15
	movq	%rbx, -72(%rbp)
	movq	%rdx, %rbx
.L33:
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_num@PLT
	movq	%r12, %rdi
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	-72(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movq	%r15, %r9
	movabsq	$4611686018427387904, %rdx
	movq	%rax, %rcx
	movq	%r14, %r8
	subq	%r13, %rsi
	subq	%r13, %rdi
	call	SEED_ofb128_encrypt@PLT
	movl	-60(%rbp), %esi
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	movabsq	$-4611686018427387904, %rax
	addq	%rax, %r13
	cmpq	%rbx, %r13
	ja	.L33
	movq	-104(%rbp), %r13
	leaq	0(%r13,%rax), %r11
	andq	%r13, %rbx
	shrq	$62, %r11
	addq	$1, %r11
	salq	$62, %r11
	addq	%r11, -96(%rbp)
	addq	%r11, -88(%rbp)
.L32:
	testq	%rbx, %rbx
	jne	.L43
.L34:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L44
	addq	$72, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_num@PLT
	movq	%r12, %rdi
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	%r14, %r8
	movq	%rax, %rcx
	leaq	-60(%rbp), %r9
	movq	%rbx, %rdx
	call	SEED_ofb128_encrypt@PLT
	movl	-60(%rbp), %esi
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	jmp	.L34
.L44:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE411:
	.size	seed_ofb_cipher, .-seed_ofb_cipher
	.p2align 4
	.type	seed_ecb_cipher, @function
seed_ecb_cipher:
.LFB410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%rsi, -56(%rbp)
	call	EVP_CIPHER_CTX_cipher@PLT
	movslq	4(%rax), %r15
	cmpq	%r12, %r15
	ja	.L46
	subq	%r15, %r12
	movq	%r12, -64(%rbp)
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L47:
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%rbx, %rdi
	movl	%eax, %r13d
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	leaq	(%r14,%r12), %rdi
	movl	%r13d, %ecx
	movq	%rax, %rdx
	movq	-56(%rbp), %rax
	leaq	(%rax,%r12), %rsi
	addq	%r15, %r12
	call	SEED_ecb_encrypt@PLT
	cmpq	%r12, -64(%rbp)
	jnb	.L47
.L46:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE410:
	.size	seed_ecb_cipher, .-seed_ecb_cipher
	.p2align 4
	.globl	EVP_seed_cbc
	.type	EVP_seed_cbc, @function
EVP_seed_cbc:
.LFB412:
	.cfi_startproc
	endbr64
	leaq	seed_cbc(%rip), %rax
	ret
	.cfi_endproc
.LFE412:
	.size	EVP_seed_cbc, .-EVP_seed_cbc
	.p2align 4
	.globl	EVP_seed_cfb128
	.type	EVP_seed_cfb128, @function
EVP_seed_cfb128:
.LFB413:
	.cfi_startproc
	endbr64
	leaq	seed_cfb128(%rip), %rax
	ret
	.cfi_endproc
.LFE413:
	.size	EVP_seed_cfb128, .-EVP_seed_cfb128
	.p2align 4
	.globl	EVP_seed_ofb
	.type	EVP_seed_ofb, @function
EVP_seed_ofb:
.LFB414:
	.cfi_startproc
	endbr64
	leaq	seed_ofb(%rip), %rax
	ret
	.cfi_endproc
.LFE414:
	.size	EVP_seed_ofb, .-EVP_seed_ofb
	.p2align 4
	.globl	EVP_seed_ecb
	.type	EVP_seed_ecb, @function
EVP_seed_ecb:
.LFB415:
	.cfi_startproc
	endbr64
	leaq	seed_ecb(%rip), %rax
	ret
	.cfi_endproc
.LFE415:
	.size	EVP_seed_ecb, .-EVP_seed_ecb
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	seed_ecb, @object
	.size	seed_ecb, 88
seed_ecb:
	.long	776
	.long	16
	.long	16
	.long	0
	.quad	4097
	.quad	seed_init_key
	.quad	seed_ecb_cipher
	.quad	0
	.long	128
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	seed_ofb, @object
	.size	seed_ofb, 88
seed_ofb:
	.long	778
	.long	1
	.long	16
	.long	16
	.quad	4100
	.quad	seed_init_key
	.quad	seed_ofb_cipher
	.quad	0
	.long	128
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	seed_cfb128, @object
	.size	seed_cfb128, 88
seed_cfb128:
	.long	779
	.long	1
	.long	16
	.long	16
	.quad	4099
	.quad	seed_init_key
	.quad	seed_cfb128_cipher
	.quad	0
	.long	128
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	seed_cbc, @object
	.size	seed_cbc, 88
seed_cbc:
	.long	777
	.long	16
	.long	16
	.long	16
	.quad	4098
	.quad	seed_init_key
	.quad	seed_cbc_cipher
	.quad	0
	.long	128
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
