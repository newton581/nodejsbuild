	.file	"a_int.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/asn1/a_int.c"
	.text
	.p2align 4
	.type	c2i_ibuf, @function
c2i_ibuf:
.LFB471:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rcx, %rcx
	je	.L31
	movzbl	(%rdx), %r8d
	movl	%r8d, %r10d
	andl	$-128, %r10d
	testq	%rsi, %rsi
	je	.L4
	movzbl	%r10b, %eax
	movl	%eax, (%rsi)
.L4:
	cmpq	$1, %rcx
	je	.L32
	movzbl	(%rdx), %eax
	testb	%al, %al
	jne	.L33
.L7:
	movzbl	1(%rdx), %eax
	andl	$-128, %eax
	cmpb	%al, %r10b
	je	.L10
	leaq	-1(%rcx), %r9
.L8:
	testq	%rdi, %rdi
	je	.L1
	movsbl	%r8b, %eax
	subq	%r9, %rcx
	sarb	$7, %r8b
	leaq	-1(%r9), %rsi
	shrl	$31, %eax
	addq	%rdx, %rcx
	.p2align 4,,10
	.p2align 3
.L12:
	movzbl	(%rcx,%rsi), %edx
	xorl	%r8d, %edx
	movzbl	%dl, %edx
	addl	%edx, %eax
	movb	%al, (%rdi,%rsi)
	subq	$1, %rsi
	shrl	$8, %eax
	cmpq	$-1, %rsi
	jne	.L12
.L1:
	movq	%r9, %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	movq	%rcx, %r9
	cmpb	$-1, %al
	jne	.L8
	movl	$1, %eax
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L9:
	movzbl	(%rdx,%rax), %r9d
	addq	$1, %rax
	orl	%r9d, %esi
	cmpq	%rax, %rcx
	ja	.L9
	testl	%esi, %esi
	jne	.L7
	movq	%rcx, %r9
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L31:
	movl	$154, %r8d
	movl	$222, %edx
	movl	$226, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r9d, %r9d
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	%r9, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movl	$1, %r9d
	testq	%rdi, %rdi
	je	.L1
	movzbl	(%rdx), %eax
	movl	%eax, %edx
	negl	%eax
	testb	%r8b, %r8b
	cmovns	%edx, %eax
	movb	%al, (%rdi)
	movq	%r9, %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movl	$187, %r8d
	movl	$221, %edx
	movl	$226, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r9d, %r9d
	jmp	.L1
	.cfi_endproc
.LFE471:
	.size	c2i_ibuf, .-c2i_ibuf
	.p2align 4
	.globl	ASN1_INTEGER_dup
	.type	ASN1_INTEGER_dup, @function
ASN1_INTEGER_dup:
.LFB467:
	.cfi_startproc
	endbr64
	jmp	ASN1_STRING_dup@PLT
	.cfi_endproc
.LFE467:
	.size	ASN1_INTEGER_dup, .-ASN1_INTEGER_dup
	.p2align 4
	.globl	ASN1_INTEGER_cmp
	.type	ASN1_INTEGER_cmp, @function
ASN1_INTEGER_cmp:
.LFB468:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	4(%rdi), %eax
	movl	4(%rsi), %ebx
	andl	$256, %eax
	andl	$256, %ebx
	cmpl	%eax, %ebx
	je	.L36
	cmpl	$1, %eax
	sbbl	%eax, %eax
	addq	$8, %rsp
	andl	$2, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	subl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	call	ASN1_STRING_cmp@PLT
	movl	%eax, %edx
	negl	%edx
	testl	%ebx, %ebx
	cmovne	%edx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE468:
	.size	ASN1_INTEGER_cmp, .-ASN1_INTEGER_cmp
	.p2align 4
	.globl	i2c_ASN1_INTEGER
	.type	i2c_ASN1_INTEGER, @function
i2c_ASN1_INTEGER:
.LFB472:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	8(%rdi), %r9
	movslq	(%rdi), %rcx
	testq	%r9, %r9
	je	.L52
	testq	%rcx, %rcx
	je	.L52
	movl	4(%rdi), %eax
	movzbl	(%r9), %edx
	movq	%rcx, %r8
	andl	$256, %eax
	jne	.L77
	cmpq	$127, %rdx
	jbe	.L44
	leaq	1(%rcx), %r11
	xorl	%r10d, %r10d
	movl	%r11d, %r8d
.L45:
	movq	%r11, %rbx
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L52:
	xorl	%ebx, %ebx
	movl	$1, %r8d
	xorl	%r10d, %r10d
	xorl	%ecx, %ecx
	movl	$1, %r11d
.L43:
	testq	%rsi, %rsi
	je	.L42
	movq	(%rsi), %rdi
	testq	%rdi, %rdi
	je	.L42
	movl	%r10d, %eax
	subq	%rcx, %rbx
	movb	%r10b, (%rdi)
	leaq	-1(%rcx), %rdx
	andl	$1, %eax
	addq	%rbx, %rdi
	movzbl	%al, %eax
	testq	%rcx, %rcx
	je	.L51
	.p2align 4,,10
	.p2align 3
.L50:
	movzbl	(%r9,%rdx), %ecx
	xorl	%r10d, %ecx
	movzbl	%cl, %ecx
	addl	%ecx, %eax
	movb	%al, (%rdi,%rdx)
	subq	$1, %rdx
	shrl	$8, %eax
	cmpq	$-1, %rdx
	jne	.L50
.L51:
	addq	%r11, (%rsi)
.L42:
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	movq	%rcx, %r11
	xorl	%r10d, %r10d
	testl	%eax, %eax
	je	.L45
	.p2align 4,,10
	.p2align 3
.L77:
	cmpq	$128, %rdx
	jbe	.L46
	leaq	1(%rcx), %r11
	movl	$-1, %r10d
	movl	%r11d, %r8d
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L46:
	movq	%rcx, %r11
	movl	$-1, %r10d
	jne	.L45
	cmpq	$1, %rcx
	jbe	.L55
	leaq	1(%r9), %rax
	leaq	(%r9,%rcx), %r8
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L48:
	movzbl	(%rax), %edi
	addq	$1, %rax
	orl	%edi, %edx
	cmpq	%rax, %r8
	jne	.L48
	testl	%edx, %edx
	setne	%r11b
	movl	%r11d, %r10d
	andl	$1, %r11d
	addq	%rcx, %r11
	negl	%r10d
	movl	%r11d, %r8d
	jmp	.L45
.L55:
	movl	$1, %r11d
	xorl	%r10d, %r10d
	jmp	.L45
	.cfi_endproc
.LFE472:
	.size	i2c_ASN1_INTEGER, .-i2c_ASN1_INTEGER
	.p2align 4
	.globl	c2i_ASN1_INTEGER
	.type	c2i_ASN1_INTEGER, @function
c2i_ASN1_INTEGER:
.LFB476:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L121
	movq	%rdi, %r13
	movq	%rsi, %r14
	movq	%rdx, %rbx
	cmpq	$1, %rdx
	je	.L101
	movq	(%rsi), %rsi
	movzbl	(%rsi), %ecx
	testb	%cl, %cl
	jne	.L122
.L83:
	movzbl	1(%rsi), %eax
	andl	$-128, %ecx
	andl	$-128, %eax
	cmpb	%al, %cl
	je	.L86
	leaq	-1(%rbx), %r15
.L82:
	testq	%r13, %r13
	je	.L87
	movq	0(%r13), %r12
	testq	%r12, %r12
	je	.L87
	xorl	%esi, %esi
	movl	%r15d, %edx
	movq	%r12, %rdi
	call	ASN1_STRING_set@PLT
	testl	%eax, %eax
	je	.L123
	movq	8(%r12), %rdi
	movq	(%r14), %rdx
	leaq	-60(%rbp), %rsi
	movq	%rbx, %rcx
	call	c2i_ibuf
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	je	.L124
.L98:
	orl	$256, 4(%r12)
.L93:
	addq	%rbx, (%r14)
	testq	%r13, %r13
	je	.L79
.L97:
	movq	%r12, 0(%r13)
.L79:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L125
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_restore_state
	movq	%rdx, %r15
	cmpb	$-1, %cl
	jne	.L82
	xorl	%edx, %edx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L85:
	movzbl	(%rsi,%rax), %edi
	addq	$1, %rax
	orl	%edi, %edx
	cmpq	%rax, %rbx
	ja	.L85
	testl	%edx, %edx
	jne	.L83
	movq	%rbx, %r15
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L87:
	call	ASN1_INTEGER_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L120
	movl	$2, 4(%rax)
	xorl	%esi, %esi
	movl	%r15d, %edx
	movq	%rax, %rdi
	call	ASN1_STRING_set@PLT
	testl	%eax, %eax
	je	.L126
	movq	(%r14), %rdx
	movq	8(%r12), %rdi
	leaq	-60(%rbp), %rsi
	movq	%rbx, %rcx
	call	c2i_ibuf
	movl	-60(%rbp), %edx
	testl	%edx, %edx
	je	.L93
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L123:
	movl	$313, %r8d
	movl	$65, %edx
	movl	$194, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L92:
	cmpq	%r12, 0(%r13)
	je	.L120
.L91:
	movq	%r12, %rdi
	call	ASN1_INTEGER_free@PLT
.L120:
	xorl	%r12d, %r12d
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L121:
	movl	$154, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$222, %edx
	xorl	%r12d, %r12d
	movl	$226, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L124:
	addq	%rbx, (%r14)
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L101:
	movl	$1, %r15d
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L126:
	movl	$313, %r8d
	movl	$65, %edx
	movl	$194, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	testq	%r13, %r13
	jne	.L92
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L86:
	movl	$187, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$221, %edx
	xorl	%r12d, %r12d
	movl	$226, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
	jmp	.L79
.L125:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE476:
	.size	c2i_ASN1_INTEGER, .-c2i_ASN1_INTEGER
	.p2align 4
	.globl	d2i_ASN1_UINTEGER
	.type	d2i_ASN1_UINTEGER, @function
d2i_ASN1_UINTEGER:
.LFB481:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L128
	movq	(%rdi), %r12
	testq	%r12, %r12
	je	.L128
	movq	(%rsi), %rax
	leaq	-60(%rbp), %rcx
	leaq	-64(%rbp), %rdx
	movq	%r14, %r8
	leaq	-48(%rbp), %rsi
	leaq	-56(%rbp), %rdi
	movq	%rax, -56(%rbp)
	call	ASN1_get_object@PLT
	testb	$-128, %al
	jne	.L171
	cmpl	$2, -64(%rbp)
	je	.L172
	movl	$443, %r8d
	movl	$115, %edx
	movl	$150, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L138:
	cmpq	%r12, (%rbx)
	je	.L170
.L136:
	movq	%r12, %rdi
	call	ASN1_INTEGER_free@PLT
.L170:
	xorl	%r12d, %r12d
.L127:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L173
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	call	ASN1_INTEGER_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L170
	movl	$2, 4(%rax)
	movq	0(%r13), %rax
	leaq	-60(%rbp), %rcx
	leaq	-64(%rbp), %rdx
	leaq	-48(%rbp), %rsi
	leaq	-56(%rbp), %rdi
	movq	%r14, %r8
	movq	%rax, -56(%rbp)
	call	ASN1_get_object@PLT
	testb	$-128, %al
	jne	.L146
	cmpl	$2, -64(%rbp)
	movl	$115, %edx
	je	.L174
.L132:
	movl	$443, %r8d
	movl	$150, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	testq	%rbx, %rbx
	jne	.L138
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L171:
	movl	$443, %r8d
	movl	$102, %edx
	movl	$150, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L174:
	movl	-48(%rbp), %eax
	movl	$420, %edx
	leaq	.LC0(%rip), %rsi
	leal	1(%rax), %edi
	movslq	%edi, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L148
.L144:
	movl	$2, 4(%r12)
	movq	-48(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L133
	movq	-56(%rbp), %rsi
	cmpb	$0, (%rsi)
	jne	.L134
	cmpq	$1, %rdx
	jne	.L175
.L134:
	movslq	%edx, %rdx
	movq	%r14, %rdi
	call	memcpy@PLT
	movq	-48(%rbp), %rax
	addq	%rax, -56(%rbp)
.L133:
	movq	8(%r12), %rdi
	movl	$435, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-48(%rbp), %rax
	movq	%r14, 8(%r12)
	movl	%eax, (%r12)
	testq	%rbx, %rbx
	je	.L135
	movq	%r12, (%rbx)
.L135:
	movq	-56(%rbp), %rax
	movq	%rax, 0(%r13)
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L146:
	movl	$102, %edx
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L172:
	movl	-48(%rbp), %eax
	movl	$420, %edx
	leaq	.LC0(%rip), %rsi
	leal	1(%rax), %edi
	movslq	%edi, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L144
	movl	$443, %r8d
	movl	$65, %edx
	movl	$150, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L175:
	addq	$1, %rsi
	subq	$1, %rdx
	movq	%rsi, -56(%rbp)
	movq	%rdx, -48(%rbp)
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L148:
	movl	$65, %edx
	jmp	.L132
.L173:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE481:
	.size	d2i_ASN1_UINTEGER, .-d2i_ASN1_UINTEGER
	.p2align 4
	.globl	ASN1_INTEGER_get_int64
	.type	ASN1_INTEGER_get_int64, @function
ASN1_INTEGER_get_int64:
.LFB484:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	testq	%rsi, %rsi
	je	.L216
	movl	4(%rsi), %eax
	movl	%eax, %edx
	andb	$-2, %dh
	cmpl	$2, %edx
	jne	.L217
	movslq	(%rsi), %r8
	cmpq	$8, %r8
	ja	.L218
	movq	8(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L194
	andl	$256, %eax
	testq	%r8, %r8
	je	.L181
	movzbl	(%rsi), %edx
	cmpq	$1, %r8
	je	.L182
	movzbl	1(%rsi), %ecx
	salq	$8, %rdx
	orq	%rcx, %rdx
	cmpq	$2, %r8
	je	.L182
	movzbl	2(%rsi), %ecx
	salq	$8, %rdx
	orq	%rcx, %rdx
	cmpq	$3, %r8
	je	.L182
	movzbl	3(%rsi), %ecx
	salq	$8, %rdx
	orq	%rcx, %rdx
	cmpq	$4, %r8
	je	.L182
	movzbl	4(%rsi), %ecx
	salq	$8, %rdx
	orq	%rcx, %rdx
	cmpq	$5, %r8
	je	.L182
	movzbl	5(%rsi), %ecx
	salq	$8, %rdx
	orq	%rcx, %rdx
	cmpq	$6, %r8
	je	.L182
	movzbl	6(%rsi), %ecx
	salq	$8, %rdx
	orq	%rcx, %rdx
	cmpq	$8, %r8
	jne	.L182
	movzbl	7(%rsi), %ecx
	salq	$8, %rdx
	orq	%rdx, %rcx
	movq	%rcx, %rdx
	testl	%eax, %eax
	je	.L219
	testq	%rcx, %rcx
	jns	.L191
	movabsq	$-9223372036854775808, %rax
	cmpq	%rax, %rcx
	jne	.L189
	movq	%rcx, (%rdi)
	movl	$1, %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L217:
	.cfi_restore_state
	movl	$326, %r8d
	movl	$225, %edx
	movl	$227, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L182:
	.cfi_restore_state
	testl	%eax, %eax
	je	.L187
.L191:
	negq	%rdx
.L187:
	movq	%rdx, (%rdi)
	movl	$1, %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L216:
	.cfi_restore_state
	movl	$322, %r8d
	movl	$67, %edx
	movl	$227, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L218:
	.cfi_restore_state
	movl	$213, %r8d
	movl	$223, %edx
	movl	$225, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L194:
	.cfi_restore_state
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L219:
	.cfi_restore_state
	testq	%rcx, %rcx
	jns	.L187
	movl	$272, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$223, %edx
	movl	%eax, -4(%rbp)
	movl	$224, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
	movl	-4(%rbp), %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore_state
	movl	$265, %r8d
	movl	$224, %edx
	movl	$224, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	.cfi_restore_state
	xorl	%edx, %edx
	jmp	.L187
	.cfi_endproc
.LFE484:
	.size	ASN1_INTEGER_get_int64, .-ASN1_INTEGER_get_int64
	.p2align 4
	.globl	ASN1_INTEGER_set_int64
	.type	ASN1_INTEGER_set_int64, @function
ASN1_INTEGER_set_int64:
.LFB485:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %ecx
	movq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	$2, 4(%rdi)
	leaq	-9(%rbp), %rax
	testq	%rsi, %rsi
	js	.L231
	.p2align 4,,10
	.p2align 3
.L224:
	movb	%dl, (%rax)
	subq	$1, %rcx
	movq	%rax, %r8
	subq	$1, %rax
	shrq	$8, %rdx
	jne	.L224
.L223:
	movl	$8, %edx
	movq	%r8, %rsi
	subl	%ecx, %edx
	call	ASN1_STRING_set@PLT
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L232
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L231:
	.cfi_restore_state
	negq	%rsi
	.p2align 4,,10
	.p2align 3
.L222:
	movb	%sil, (%rax)
	subq	$1, %rcx
	shrq	$8, %rsi
	movq	%rax, %r8
	leaq	-1(%rax), %rax
	jne	.L222
	movl	$258, 4(%rdi)
	jmp	.L223
.L232:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE485:
	.size	ASN1_INTEGER_set_int64, .-ASN1_INTEGER_set_int64
	.p2align 4
	.globl	ASN1_INTEGER_get_uint64
	.type	ASN1_INTEGER_get_uint64, @function
ASN1_INTEGER_get_uint64:
.LFB486:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	testq	%rsi, %rsi
	je	.L255
	movl	4(%rsi), %eax
	movl	%eax, %edx
	andb	$-2, %dh
	cmpl	$2, %edx
	jne	.L256
	andl	$256, %eax
	jne	.L257
	movslq	(%rsi), %rdx
	cmpq	$8, %rdx
	ja	.L258
	movq	8(%rsi), %rcx
	testq	%rcx, %rcx
	je	.L233
	testq	%rdx, %rdx
	je	.L239
	movzbl	(%rcx), %eax
	cmpq	$1, %rdx
	je	.L247
	salq	$8, %rax
	movq	%rax, %rsi
	movzbl	1(%rcx), %eax
	orq	%rsi, %rax
	cmpq	$2, %rdx
	je	.L247
	movzbl	2(%rcx), %esi
	salq	$8, %rax
	orq	%rsi, %rax
	cmpq	$3, %rdx
	je	.L247
	movzbl	3(%rcx), %esi
	salq	$8, %rax
	orq	%rsi, %rax
	cmpq	$4, %rdx
	je	.L247
	movzbl	4(%rcx), %esi
	salq	$8, %rax
	orq	%rsi, %rax
	cmpq	$5, %rdx
	je	.L247
	movzbl	5(%rcx), %esi
	salq	$8, %rax
	orq	%rsi, %rax
	cmpq	$6, %rdx
	je	.L247
	movzbl	6(%rcx), %esi
	salq	$8, %rax
	orq	%rsi, %rax
	cmpq	$8, %rdx
	jne	.L247
	movzbl	7(%rcx), %edx
	salq	$8, %rax
	orq	%rax, %rdx
.L239:
	movq	%rdx, (%rdi)
	movl	$1, %eax
.L233:
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L257:
	.cfi_restore_state
	movl	$365, %r8d
	movl	$226, %edx
	movl	$230, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L256:
	.cfi_restore_state
	movl	$361, %r8d
	movl	$225, %edx
	movl	$230, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L247:
	.cfi_restore_state
	movq	%rax, %rdx
	movl	$1, %eax
	movq	%rdx, (%rdi)
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L255:
	movl	$357, %r8d
	movl	$67, %edx
	movl	$230, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L258:
	.cfi_restore_state
	movl	$213, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$223, %edx
	movl	%eax, -4(%rbp)
	movl	$225, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
	movl	-4(%rbp), %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE486:
	.size	ASN1_INTEGER_get_uint64, .-ASN1_INTEGER_get_uint64
	.p2align 4
	.globl	ASN1_INTEGER_set_uint64
	.type	ASN1_INTEGER_set_uint64, @function
ASN1_INTEGER_set_uint64:
.LFB487:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	$2, 4(%rdi)
	leaq	-9(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L260:
	movb	%sil, (%rax)
	subq	$1, %rcx
	movq	%rax, %r8
	subq	$1, %rax
	shrq	$8, %rsi
	jne	.L260
	movl	$8, %edx
	movq	%r8, %rsi
	subl	%ecx, %edx
	call	ASN1_STRING_set@PLT
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L264
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L264:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE487:
	.size	ASN1_INTEGER_set_uint64, .-ASN1_INTEGER_set_uint64
	.p2align 4
	.globl	ASN1_INTEGER_set
	.type	ASN1_INTEGER_set, @function
ASN1_INTEGER_set:
.LFB504:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %ecx
	movq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	$2, 4(%rdi)
	leaq	-9(%rbp), %rax
	testq	%rsi, %rsi
	js	.L276
	.p2align 4,,10
	.p2align 3
.L269:
	movb	%dl, (%rax)
	subq	$1, %rcx
	movq	%rax, %r8
	subq	$1, %rax
	shrq	$8, %rdx
	jne	.L269
.L268:
	movl	$8, %edx
	movq	%r8, %rsi
	subl	%ecx, %edx
	call	ASN1_STRING_set@PLT
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L277
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L276:
	.cfi_restore_state
	negq	%rsi
	.p2align 4,,10
	.p2align 3
.L267:
	movb	%sil, (%rax)
	subq	$1, %rcx
	shrq	$8, %rsi
	movq	%rax, %r8
	leaq	-1(%rax), %rax
	jne	.L267
	movl	$258, 4(%rdi)
	jmp	.L268
.L277:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE504:
	.size	ASN1_INTEGER_set, .-ASN1_INTEGER_set
	.p2align 4
	.globl	ASN1_INTEGER_get
	.type	ASN1_INTEGER_get, @function
ASN1_INTEGER_get:
.LFB489:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L318
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	4(%rdi), %ecx
	movl	%ecx, %edx
	andb	$-2, %dh
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$2, %edx
	jne	.L321
	movslq	(%rdi), %rsi
	cmpq	$8, %rsi
	ja	.L322
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L294
	andl	$256, %ecx
	testq	%rsi, %rsi
	je	.L278
	movzbl	(%rdi), %eax
	cmpq	$1, %rsi
	je	.L284
	movzbl	1(%rdi), %edx
	salq	$8, %rax
	orq	%rdx, %rax
	cmpq	$2, %rsi
	je	.L284
	movzbl	2(%rdi), %edx
	salq	$8, %rax
	orq	%rdx, %rax
	cmpq	$3, %rsi
	je	.L284
	movzbl	3(%rdi), %edx
	salq	$8, %rax
	orq	%rdx, %rax
	cmpq	$4, %rsi
	je	.L284
	movzbl	4(%rdi), %edx
	salq	$8, %rax
	orq	%rdx, %rax
	cmpq	$5, %rsi
	je	.L284
	movzbl	5(%rdi), %edx
	salq	$8, %rax
	orq	%rdx, %rax
	cmpq	$6, %rsi
	je	.L284
	movzbl	6(%rdi), %edx
	salq	$8, %rax
	orq	%rdx, %rax
	cmpq	$8, %rsi
	jne	.L284
	movzbl	7(%rdi), %edx
	salq	$8, %rax
	orq	%rdx, %rax
	testl	%ecx, %ecx
	je	.L316
	testq	%rax, %rax
	jns	.L291
	movabsq	$-9223372036854775808, %rdx
	cmpq	%rdx, %rax
	je	.L278
	movl	$265, %r8d
	movl	$224, %edx
	movl	$224, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	$-1, %rax
.L278:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L321:
	.cfi_restore_state
	movl	$326, %r8d
	movl	$225, %edx
	movl	$227, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	$-1, %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L318:
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L322:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	movl	$213, %r8d
	movl	$223, %edx
	movl	$225, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	$-1, %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L284:
	.cfi_restore_state
	testl	%ecx, %ecx
	je	.L278
.L291:
	negq	%rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L316:
	.cfi_restore_state
	testq	%rax, %rax
	jns	.L278
	movl	$272, %r8d
	movl	$223, %edx
	movl	$224, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	$-1, %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L294:
	.cfi_restore_state
	movq	$-1, %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE489:
	.size	ASN1_INTEGER_get, .-ASN1_INTEGER_get
	.p2align 4
	.globl	BN_to_ASN1_INTEGER
	.type	BN_to_ASN1_INTEGER, @function
BN_to_ASN1_INTEGER:
.LFB490:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	testq	%rsi, %rsi
	je	.L338
	movl	$2, 4(%rsi)
	movq	%rsi, %r13
.L326:
	movq	%r14, %rdi
	call	BN_is_negative@PLT
	testl	%eax, %eax
	jne	.L339
.L329:
	movq	%r14, %rdi
	movl	$1, %ebx
	call	BN_num_bits@PLT
	movq	%r13, %rdi
	leal	14(%rax), %edx
	addl	$7, %eax
	cmovs	%edx, %eax
	sarl	$3, %eax
	cmovne	%eax, %ebx
	xorl	%esi, %esi
	movl	%ebx, %edx
	call	ASN1_STRING_set@PLT
	testl	%eax, %eax
	je	.L340
	movq	%r14, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L334
	movq	8(%r13), %rax
	movb	$0, (%rax)
.L335:
	movl	%ebx, 0(%r13)
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L340:
	.cfi_restore_state
	movl	$476, %r8d
	movl	$65, %edx
	movl	$229, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	cmpq	%r13, %r12
	je	.L337
	movq	%r13, %rdi
	call	ASN1_INTEGER_free@PLT
.L337:
	xorl	%r13d, %r13d
	popq	%rbx
	popq	%r12
	movq	%r13, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L339:
	.cfi_restore_state
	movq	%r14, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L329
	orl	$258, 4(%r13)
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L334:
	movq	8(%r13), %rsi
	movq	%r14, %rdi
	call	BN_bn2bin@PLT
	movl	%eax, %ebx
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L338:
	movl	$2, %edi
	call	ASN1_STRING_type_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L326
	movl	$463, %r8d
	movl	$58, %edx
	movl	$229, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE490:
	.size	BN_to_ASN1_INTEGER, .-BN_to_ASN1_INTEGER
	.p2align 4
	.globl	ASN1_INTEGER_to_BN
	.type	ASN1_INTEGER_to_BN, @function
ASN1_INTEGER_to_BN:
.LFB491:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movl	4(%rdi), %eax
	andb	$-2, %ah
	cmpl	$2, %eax
	jne	.L349
	movq	%rdi, %rbx
	movq	%rsi, %rdx
	movq	8(%rdi), %rdi
	movl	(%rbx), %esi
	call	BN_bin2bn@PLT
	testq	%rax, %rax
	je	.L350
	testb	$1, 5(%rbx)
	jne	.L351
.L341:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L351:
	.cfi_restore_state
	movq	%rax, %rdi
	movl	$1, %esi
	movq	%rax, -24(%rbp)
	call	BN_set_negative@PLT
	movq	-24(%rbp), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L349:
	.cfi_restore_state
	movl	$499, %r8d
	movl	$225, %edx
	movl	$228, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L350:
	.cfi_restore_state
	movl	$505, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$105, %edx
	movl	$228, %esi
	movl	$13, %edi
	movq	%rax, -24(%rbp)
	call	ERR_put_error@PLT
	movq	-24(%rbp), %rax
	jmp	.L341
	.cfi_endproc
.LFE491:
	.size	ASN1_INTEGER_to_BN, .-ASN1_INTEGER_to_BN
	.p2align 4
	.globl	ASN1_ENUMERATED_get_int64
	.type	ASN1_ENUMERATED_get_int64, @function
ASN1_ENUMERATED_get_int64:
.LFB492:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	testq	%rsi, %rsi
	je	.L392
	movl	4(%rsi), %eax
	movl	%eax, %edx
	andb	$-2, %dh
	cmpl	$10, %edx
	jne	.L393
	movslq	(%rsi), %r8
	cmpq	$8, %r8
	ja	.L394
	movq	8(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L370
	andl	$256, %eax
	testq	%r8, %r8
	je	.L357
	movzbl	(%rsi), %edx
	cmpq	$1, %r8
	je	.L358
	movzbl	1(%rsi), %ecx
	salq	$8, %rdx
	orq	%rcx, %rdx
	cmpq	$2, %r8
	je	.L358
	movzbl	2(%rsi), %ecx
	salq	$8, %rdx
	orq	%rcx, %rdx
	cmpq	$3, %r8
	je	.L358
	movzbl	3(%rsi), %ecx
	salq	$8, %rdx
	orq	%rcx, %rdx
	cmpq	$4, %r8
	je	.L358
	movzbl	4(%rsi), %ecx
	salq	$8, %rdx
	orq	%rcx, %rdx
	cmpq	$5, %r8
	je	.L358
	movzbl	5(%rsi), %ecx
	salq	$8, %rdx
	orq	%rcx, %rdx
	cmpq	$6, %r8
	je	.L358
	movzbl	6(%rsi), %ecx
	salq	$8, %rdx
	orq	%rcx, %rdx
	cmpq	$8, %r8
	jne	.L358
	movzbl	7(%rsi), %ecx
	salq	$8, %rdx
	orq	%rdx, %rcx
	movq	%rcx, %rdx
	testl	%eax, %eax
	je	.L395
	testq	%rcx, %rcx
	jns	.L367
	movabsq	$-9223372036854775808, %rax
	cmpq	%rax, %rcx
	jne	.L365
	movq	%rcx, (%rdi)
	movl	$1, %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L393:
	.cfi_restore_state
	movl	$326, %r8d
	movl	$225, %edx
	movl	$227, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L358:
	.cfi_restore_state
	testl	%eax, %eax
	je	.L363
.L367:
	negq	%rdx
.L363:
	movq	%rdx, (%rdi)
	movl	$1, %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L392:
	.cfi_restore_state
	movl	$322, %r8d
	movl	$67, %edx
	movl	$227, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L394:
	.cfi_restore_state
	movl	$213, %r8d
	movl	$223, %edx
	movl	$225, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L370:
	.cfi_restore_state
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L395:
	.cfi_restore_state
	testq	%rcx, %rcx
	jns	.L363
	movl	$272, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$223, %edx
	movl	%eax, -4(%rbp)
	movl	$224, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
	movl	-4(%rbp), %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L365:
	.cfi_restore_state
	movl	$265, %r8d
	movl	$224, %edx
	movl	$224, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L357:
	.cfi_restore_state
	xorl	%edx, %edx
	jmp	.L363
	.cfi_endproc
.LFE492:
	.size	ASN1_ENUMERATED_get_int64, .-ASN1_ENUMERATED_get_int64
	.p2align 4
	.globl	ASN1_ENUMERATED_set_int64
	.type	ASN1_ENUMERATED_set_int64, @function
ASN1_ENUMERATED_set_int64:
.LFB493:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %ecx
	movq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	$10, 4(%rdi)
	leaq	-9(%rbp), %rax
	testq	%rsi, %rsi
	js	.L407
	.p2align 4,,10
	.p2align 3
.L400:
	movb	%dl, (%rax)
	subq	$1, %rcx
	movq	%rax, %r8
	subq	$1, %rax
	shrq	$8, %rdx
	jne	.L400
.L399:
	movl	$8, %edx
	movq	%r8, %rsi
	subl	%ecx, %edx
	call	ASN1_STRING_set@PLT
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L408
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L407:
	.cfi_restore_state
	negq	%rsi
	.p2align 4,,10
	.p2align 3
.L398:
	movb	%sil, (%rax)
	subq	$1, %rcx
	shrq	$8, %rsi
	movq	%rax, %r8
	leaq	-1(%rax), %rax
	jne	.L398
	movl	$266, 4(%rdi)
	jmp	.L399
.L408:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE493:
	.size	ASN1_ENUMERATED_set_int64, .-ASN1_ENUMERATED_set_int64
	.p2align 4
	.globl	ASN1_ENUMERATED_set
	.type	ASN1_ENUMERATED_set, @function
ASN1_ENUMERATED_set:
.LFB506:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %ecx
	movq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	$10, 4(%rdi)
	leaq	-9(%rbp), %rax
	testq	%rsi, %rsi
	js	.L420
	.p2align 4,,10
	.p2align 3
.L413:
	movb	%dl, (%rax)
	subq	$1, %rcx
	movq	%rax, %r8
	subq	$1, %rax
	shrq	$8, %rdx
	jne	.L413
.L412:
	movl	$8, %edx
	movq	%r8, %rsi
	subl	%ecx, %edx
	call	ASN1_STRING_set@PLT
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L421
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L420:
	.cfi_restore_state
	negq	%rsi
	.p2align 4,,10
	.p2align 3
.L411:
	movb	%sil, (%rax)
	subq	$1, %rcx
	shrq	$8, %rsi
	movq	%rax, %r8
	leaq	-1(%rax), %rax
	jne	.L411
	movl	$266, 4(%rdi)
	jmp	.L412
.L421:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE506:
	.size	ASN1_ENUMERATED_set, .-ASN1_ENUMERATED_set
	.p2align 4
	.globl	ASN1_ENUMERATED_get
	.type	ASN1_ENUMERATED_get, @function
ASN1_ENUMERATED_get:
.LFB495:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L436
	movl	4(%rdi), %ecx
	movl	%ecx, %eax
	andb	$-2, %ah
	cmpl	$10, %eax
	jne	.L437
	movslq	(%rdi), %rdx
	movl	$4294967295, %eax
	cmpl	$8, %edx
	jg	.L462
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpq	$8, %rdx
	ja	.L466
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L439
	andl	$256, %ecx
	testq	%rdx, %rdx
	je	.L425
	movzbl	(%rdi), %eax
	cmpq	$1, %rdx
	je	.L427
	movzbl	1(%rdi), %esi
	salq	$8, %rax
	orq	%rsi, %rax
	cmpq	$2, %rdx
	je	.L427
	movzbl	2(%rdi), %esi
	salq	$8, %rax
	orq	%rsi, %rax
	cmpq	$3, %rdx
	je	.L427
	movzbl	3(%rdi), %esi
	salq	$8, %rax
	orq	%rsi, %rax
	cmpq	$4, %rdx
	je	.L427
	movzbl	4(%rdi), %esi
	salq	$8, %rax
	orq	%rsi, %rax
	cmpq	$5, %rdx
	je	.L427
	movzbl	5(%rdi), %esi
	salq	$8, %rax
	orq	%rsi, %rax
	cmpq	$6, %rdx
	je	.L427
	movzbl	6(%rdi), %esi
	salq	$8, %rax
	orq	%rsi, %rax
	cmpq	$8, %rdx
	jne	.L427
	movzbl	7(%rdi), %edx
	salq	$8, %rax
	orq	%rdx, %rax
	testl	%ecx, %ecx
	je	.L467
	testq	%rax, %rax
	jns	.L434
	movabsq	$-9223372036854775808, %rdx
	cmpq	%rdx, %rax
	je	.L422
	movl	$265, %r8d
	movl	$224, %edx
	movl	$224, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	$-1, %rax
.L422:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L466:
	.cfi_restore_state
	movl	$213, %r8d
	movl	$223, %edx
	movl	$225, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	$-1, %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L462:
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L436:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L427:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	testl	%ecx, %ecx
	je	.L422
.L434:
	negq	%rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L467:
	.cfi_restore_state
	testq	%rax, %rax
	jns	.L422
	movl	$272, %r8d
	movl	$223, %edx
	movl	$224, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	$-1, %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L439:
	.cfi_restore_state
	movq	$-1, %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L425:
	.cfi_restore_state
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L437:
	.cfi_restore 6
	movq	$-1, %rax
	ret
	.cfi_endproc
.LFE495:
	.size	ASN1_ENUMERATED_get, .-ASN1_ENUMERATED_get
	.p2align 4
	.globl	BN_to_ASN1_ENUMERATED
	.type	BN_to_ASN1_ENUMERATED, @function
BN_to_ASN1_ENUMERATED:
.LFB496:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	testq	%rsi, %rsi
	je	.L483
	movl	$10, 4(%rsi)
	movq	%rsi, %r13
.L471:
	movq	%r14, %rdi
	call	BN_is_negative@PLT
	testl	%eax, %eax
	jne	.L484
.L474:
	movq	%r14, %rdi
	movl	$1, %ebx
	call	BN_num_bits@PLT
	movq	%r13, %rdi
	leal	14(%rax), %edx
	addl	$7, %eax
	cmovs	%edx, %eax
	sarl	$3, %eax
	cmovne	%eax, %ebx
	xorl	%esi, %esi
	movl	%ebx, %edx
	call	ASN1_STRING_set@PLT
	testl	%eax, %eax
	je	.L485
	movq	%r14, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L479
	movq	8(%r13), %rax
	movb	$0, (%rax)
.L480:
	movl	%ebx, 0(%r13)
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L485:
	.cfi_restore_state
	movl	$476, %r8d
	movl	$65, %edx
	movl	$229, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	cmpq	%r13, %r12
	je	.L482
	movq	%r13, %rdi
	call	ASN1_INTEGER_free@PLT
.L482:
	xorl	%r13d, %r13d
	popq	%rbx
	popq	%r12
	movq	%r13, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L484:
	.cfi_restore_state
	movq	%r14, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L474
	orl	$258, 4(%r13)
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L479:
	movq	8(%r13), %rsi
	movq	%r14, %rdi
	call	BN_bn2bin@PLT
	movl	%eax, %ebx
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L483:
	movl	$10, %edi
	call	ASN1_STRING_type_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L471
	movl	$463, %r8d
	movl	$58, %edx
	movl	$229, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE496:
	.size	BN_to_ASN1_ENUMERATED, .-BN_to_ASN1_ENUMERATED
	.p2align 4
	.globl	ASN1_ENUMERATED_to_BN
	.type	ASN1_ENUMERATED_to_BN, @function
ASN1_ENUMERATED_to_BN:
.LFB497:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movl	4(%rdi), %eax
	andb	$-2, %ah
	cmpl	$10, %eax
	jne	.L494
	movq	%rdi, %rbx
	movq	%rsi, %rdx
	movq	8(%rdi), %rdi
	movl	(%rbx), %esi
	call	BN_bin2bn@PLT
	testq	%rax, %rax
	je	.L495
	testb	$1, 5(%rbx)
	jne	.L496
.L486:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L496:
	.cfi_restore_state
	movq	%rax, %rdi
	movl	$1, %esi
	movq	%rax, -24(%rbp)
	call	BN_set_negative@PLT
	movq	-24(%rbp), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L494:
	.cfi_restore_state
	movl	$499, %r8d
	movl	$225, %edx
	movl	$228, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L495:
	.cfi_restore_state
	movl	$505, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$105, %edx
	movl	$228, %esi
	movl	$13, %edi
	movq	%rax, -24(%rbp)
	call	ERR_put_error@PLT
	movq	-24(%rbp), %rax
	jmp	.L486
	.cfi_endproc
.LFE497:
	.size	ASN1_ENUMERATED_to_BN, .-ASN1_ENUMERATED_to_BN
	.p2align 4
	.globl	c2i_uint64_int
	.type	c2i_uint64_int, @function
c2i_uint64_int:
.LFB498:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L539
	movq	(%rdx), %rdx
	movq	%rdi, %rbx
	movq	%rcx, %r13
	cmpq	$1, %rcx
	je	.L500
	movzbl	(%rdx), %ecx
	testb	%cl, %cl
	jne	.L540
.L501:
	movzbl	1(%rdx), %eax
	andl	$-128, %ecx
	andl	$-128, %eax
	cmpb	%al, %cl
	je	.L504
	leaq	-1(%r13), %r12
	cmpq	$8, %r12
	ja	.L505
	leaq	-48(%rbp), %rdi
	movq	%r13, %rcx
	call	c2i_ibuf
	movzbl	-48(%rbp), %eax
	cmpq	$1, %r12
	je	.L507
.L508:
	movzbl	-47(%rbp), %edx
	salq	$8, %rax
	orq	%rdx, %rax
	cmpq	$2, %r12
	je	.L507
	movzbl	-46(%rbp), %edx
	salq	$8, %rax
	orq	%rdx, %rax
	cmpq	$3, %r12
	je	.L507
	movzbl	-45(%rbp), %edx
	salq	$8, %rax
	orq	%rdx, %rax
	cmpq	$4, %r12
	je	.L507
	movzbl	-44(%rbp), %edx
	salq	$8, %rax
	orq	%rdx, %rax
	cmpq	$5, %r12
	je	.L507
	movzbl	-43(%rbp), %edx
	salq	$8, %rax
	orq	%rdx, %rax
	cmpq	$6, %r12
	je	.L507
	movzbl	-42(%rbp), %edx
	salq	$8, %rax
	orq	%rdx, %rax
	cmpq	$8, %r12
	jne	.L507
	movzbl	-41(%rbp), %edx
	salq	$8, %rax
	orq	%rdx, %rax
.L507:
	movq	%rax, (%rbx)
	movl	$1, %eax
	jmp	.L497
	.p2align 4,,10
	.p2align 3
.L540:
	cmpb	$-1, %cl
	je	.L541
.L502:
	cmpq	$8, %r13
	ja	.L505
	leaq	-48(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r13, %r12
	call	c2i_ibuf
	movzbl	-48(%rbp), %eax
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L505:
	movl	$615, %r8d
	movl	$223, %edx
	movl	$101, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L497:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L542
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L539:
	.cfi_restore_state
	movl	$154, %r8d
	movl	$222, %edx
	movl	$226, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L497
	.p2align 4,,10
	.p2align 3
.L541:
	xorl	%edi, %edi
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L503:
	movzbl	(%rdx,%rax), %r8d
	addq	$1, %rax
	orl	%r8d, %edi
	cmpq	%rax, %r13
	ja	.L503
	testl	%edi, %edi
	jne	.L501
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L500:
	leaq	-48(%rbp), %rdi
	movl	$1, %ecx
	call	c2i_ibuf
	movzbl	-48(%rbp), %eax
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L504:
	movl	$187, %r8d
	movl	$221, %edx
	movl	$226, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L497
.L542:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE498:
	.size	c2i_uint64_int, .-c2i_uint64_int
	.p2align 4
	.globl	i2c_uint64_int
	.type	i2c_uint64_int, @function
i2c_uint64_int:
.LFB499:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	-25(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L544:
	movb	%sil, (%rax)
	movq	%r8, %r9
	movl	%esi, %ebx
	subq	$1, %r8
	movq	%rsi, %r10
	shrq	$8, %rsi
	movq	%rax, %rcx
	movq	%rax, %r11
	leaq	-1(%rax), %rax
	jne	.L544
	movl	$8, %r12d
	subq	%r8, %r12
	je	.L545
	testb	%bl, %bl
	jns	.L546
	testl	%edx, %edx
	jne	.L571
	movl	$9, %edx
	xorl	%ebx, %ebx
	subq	%r8, %rdx
.L547:
	movl	%edx, %r10d
	testq	%rdi, %rdi
	je	.L543
	movl	$7, %esi
	movl	%ebx, %eax
	movb	%bl, (%rdi)
	subq	%r8, %rsi
	leaq	-9(%r9), %r8
	andl	$1, %eax
	addq	%r8, %r12
	addq	%r8, %rdx
	movzbl	%al, %eax
	addq	%r12, %rcx
	addq	%rdx, %rdi
	.p2align 4,,10
	.p2align 3
.L553:
	movzbl	(%rcx,%rsi), %edx
	xorl	%ebx, %edx
	movzbl	%dl, %edx
	addl	%edx, %eax
	movb	%al, (%rdi,%rsi)
	subq	$1, %rsi
	shrl	$8, %eax
	cmpq	$-1, %rsi
	jne	.L553
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L545:
	testq	%rdi, %rdi
	je	.L558
	movb	$0, (%rdi)
	movl	$1, %r10d
.L543:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L574
	addq	$16, %rsp
	movl	%r10d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L546:
	.cfi_restore_state
	testl	%edx, %edx
	jne	.L571
.L555:
	movq	%r12, %rdx
	xorl	%ebx, %ebx
	jmp	.L547
.L558:
	movl	$1, %r10d
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L571:
	cmpb	$-128, %bl
	jbe	.L548
	movl	$9, %edx
	movl	$-1, %ebx
	subq	%r8, %rdx
	jmp	.L547
.L548:
	movq	%r12, %rdx
	movl	$-1, %ebx
	cmpq	$128, %r10
	jne	.L547
	cmpq	$1, %r12
	je	.L555
	leaq	1(%rcx), %rax
	addq	%r12, %r11
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L550:
	movzbl	(%rax), %esi
	addq	$1, %rax
	orl	%esi, %edx
	cmpq	%rax, %r11
	jne	.L550
	testl	%edx, %edx
	setne	%dl
	movl	%edx, %ebx
	andl	$1, %edx
	negl	%ebx
	addq	%r12, %rdx
	jmp	.L547
.L574:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE499:
	.size	i2c_uint64_int, .-i2c_uint64_int
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
