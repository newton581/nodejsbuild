	.file	"ecp_nistz256.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ec/ecp_nistz256.c"
	.text
	.p2align 4
	.type	ecp_nistz256_inv_mod_ord, @function
ecp_nistz256_inv_mod_ord:
.LFB443:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$4, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$584, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	bn_wexpand@PLT
	movl	$1531, %r8d
	testq	%rax, %rax
	je	.L21
	movq	%r12, %rdi
	call	BN_num_bits@PLT
	cmpl	$256, %eax
	jle	.L4
.L7:
	movq	%r14, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L5
	movq	16(%rbx), %rdx
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	BN_nnmod@PLT
	testl	%eax, %eax
	je	.L5
.L8:
	leaq	-576(%rbp), %r12
	movl	$4, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	bn_copy_words@PLT
	testl	%eax, %eax
	je	.L23
	leaq	-544(%rbp), %rbx
	movq	%r12, %rsi
	leaq	RR.10542(%rip), %rdx
	leaq	-512(%rbp), %r12
	movq	%rbx, %rdi
	leaq	-480(%rbp), %r15
	call	ecp_nistz256_ord_mul_mont@PLT
	movq	%r12, %rdi
	movl	$1, %edx
	movq	%rbx, %rsi
	call	ecp_nistz256_ord_sqr_mont@PLT
	movq	%r12, %rdx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	call	ecp_nistz256_ord_mul_mont@PLT
	leaq	-448(%rbp), %r14
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	ecp_nistz256_ord_mul_mont@PLT
	movq	%r12, %rdx
	movq	%r14, %rsi
	leaq	-416(%rbp), %rdi
	call	ecp_nistz256_ord_mul_mont@PLT
	leaq	-384(%rbp), %r8
	movq	%r14, %rsi
	movl	$1, %edx
	movq	%r8, %rdi
	movq	%r8, -616(%rbp)
	leaq	-320(%rbp), %r12
	call	ecp_nistz256_ord_sqr_mont@PLT
	movq	-616(%rbp), %r8
	movq	%r14, %rdx
	leaq	-352(%rbp), %rdi
	movq	%r8, %rsi
	call	ecp_nistz256_ord_mul_mont@PLT
	movq	-616(%rbp), %r8
	movq	%r12, %rdi
	movl	$1, %edx
	movq	%r8, %rsi
	call	ecp_nistz256_ord_sqr_mont@PLT
	movq	%r12, %rsi
	movq	%r12, %rdi
	movq	%rbx, %rdx
	call	ecp_nistz256_ord_mul_mont@PLT
	leaq	-288(%rbp), %r8
	movq	%r12, %rsi
	movl	$1, %edx
	movq	%r8, %rdi
	movq	%r8, -616(%rbp)
	call	ecp_nistz256_ord_sqr_mont@PLT
	movq	-616(%rbp), %r8
	movq	%r14, %rdx
	leaq	-256(%rbp), %rdi
	leaq	-224(%rbp), %r14
	movq	%r8, %rsi
	call	ecp_nistz256_ord_mul_mont@PLT
	movq	-616(%rbp), %r8
	movq	%r12, %rdx
	movq	%r14, %rdi
	leaq	-192(%rbp), %r12
	movq	%r8, %rsi
	call	ecp_nistz256_ord_mul_mont@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$2, %edx
	call	ecp_nistz256_ord_sqr_mont@PLT
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r12, %rdi
	leaq	-160(%rbp), %r14
	leaq	-608(%rbp), %r15
	call	ecp_nistz256_ord_mul_mont@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	$8, %edx
	call	ecp_nistz256_ord_sqr_mont@PLT
	movq	%r12, %rdx
	movq	%r14, %rsi
	leaq	-128(%rbp), %r12
	movq	%r14, %rdi
	call	ecp_nistz256_ord_mul_mont@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$16, %edx
	call	ecp_nistz256_ord_sqr_mont@PLT
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	ecp_nistz256_ord_mul_mont@PLT
	movq	%r12, %rsi
	movl	$64, %edx
	movq	%r15, %rdi
	call	ecp_nistz256_ord_sqr_mont@PLT
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	ecp_nistz256_ord_mul_mont@PLT
	leaq	chain.10569(%rip), %r14
	leaq	54(%r14), %r12
	.p2align 4,,10
	.p2align 3
.L10:
	movzbl	(%r14), %edx
	movq	%r15, %rsi
	movq	%r15, %rdi
	addq	$2, %r14
	call	ecp_nistz256_ord_sqr_mont@PLT
	movzbl	-1(%r14), %edx
	movq	%r15, %rsi
	movq	%r15, %rdi
	salq	$5, %rdx
	addq	%rbx, %rdx
	call	ecp_nistz256_ord_mul_mont@PLT
	cmpq	%r12, %r14
	jne	.L10
	leaq	one.10543(%rip), %rdx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	ecp_nistz256_ord_mul_mont@PLT
	movl	$4, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	bn_set_words@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
.L3:
	endbr64
.L1:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L24
	addq	$584, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%r12, %r15
	call	BN_is_negative@PLT
	testl	%eax, %eax
	je	.L8
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$1540, %r8d
.L21:
	movl	$3, %edx
	movl	$275, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L23:
	movl	$1547, %r8d
	movl	$146, %edx
	movl	$275, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	movl	%eax, -616(%rbp)
	call	ERR_put_error@PLT
	movl	-616(%rbp), %eax
	jmp	.L1
.L24:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE443:
	.size	ecp_nistz256_inv_mod_ord, .-ecp_nistz256_inv_mod_ord
	.p2align 4
	.type	ecp_nistz256_get_affine.part.0, @function
ecp_nistz256_get_affine.part.0:
.LFB450:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-352(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$408, %rsp
	movq	%rsi, -392(%rbp)
	movq	16(%rdi), %rsi
	movq	%r12, %rdi
	movq	%rdx, -400(%rbp)
	movl	$4, %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	bn_copy_words@PLT
	testl	%eax, %eax
	je	.L28
	leaq	-320(%rbp), %r13
	movq	24(%rbx), %rsi
	movl	$4, %edx
	movq	%r13, %rdi
	call	bn_copy_words@PLT
	testl	%eax, %eax
	jne	.L57
.L28:
	movl	$1397, %r8d
	movl	$146, %edx
	movl	$240, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L25:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L58
	addq	$408, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	leaq	-288(%rbp), %r14
	movq	32(%rbx), %rsi
	movl	$4, %edx
	movq	%r14, %rdi
	call	bn_copy_words@PLT
	testl	%eax, %eax
	je	.L28
	leaq	-96(%rbp), %r15
	leaq	-256(%rbp), %rbx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	ecp_nistz256_sqr_mont@PLT
	movq	%r14, %rdx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%rbx, -440(%rbp)
	call	ecp_nistz256_mul_mont@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	ecp_nistz256_sqr_mont@PLT
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	ecp_nistz256_sqr_mont@PLT
	leaq	-224(%rbp), %rax
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%rax, %rbx
	movq	%rax, %rdi
	movq	%rax, -432(%rbp)
	call	ecp_nistz256_mul_mont@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	ecp_nistz256_sqr_mont@PLT
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	ecp_nistz256_sqr_mont@PLT
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	ecp_nistz256_sqr_mont@PLT
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	ecp_nistz256_sqr_mont@PLT
	leaq	-192(%rbp), %rax
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%rax, %rbx
	movq	%rax, %rdi
	movq	%rax, -408(%rbp)
	call	ecp_nistz256_mul_mont@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	$7, %ebx
	call	ecp_nistz256_sqr_mont@PLT
	.p2align 4,,10
	.p2align 3
.L29:
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	ecp_nistz256_sqr_mont@PLT
	subl	$1, %ebx
	jne	.L29
	leaq	-160(%rbp), %rax
	movq	-408(%rbp), %rdx
	movq	%r15, %rsi
	movq	%rax, %rbx
	movq	%rax, %rdi
	movq	%rax, -424(%rbp)
	call	ecp_nistz256_mul_mont@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	$15, %ebx
	call	ecp_nistz256_sqr_mont@PLT
	.p2align 4,,10
	.p2align 3
.L30:
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	ecp_nistz256_sqr_mont@PLT
	subl	$1, %ebx
	jne	.L30
	leaq	-128(%rbp), %rax
	movq	-424(%rbp), %rdx
	movq	%r15, %rsi
	movq	%rax, %rbx
	movq	%rax, %rdi
	movq	%rax, -416(%rbp)
	call	ecp_nistz256_mul_mont@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	$31, %ebx
	call	ecp_nistz256_sqr_mont@PLT
	.p2align 4,,10
	.p2align 3
.L31:
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	ecp_nistz256_sqr_mont@PLT
	subl	$1, %ebx
	jne	.L31
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r15, %rdi
	movl	$128, %ebx
	call	ecp_nistz256_mul_mont@PLT
	.p2align 4,,10
	.p2align 3
.L32:
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	ecp_nistz256_sqr_mont@PLT
	subl	$1, %ebx
	jne	.L32
	movq	-416(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r15, %rdi
	movl	$32, %ebx
	call	ecp_nistz256_mul_mont@PLT
	.p2align 4,,10
	.p2align 3
.L33:
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	ecp_nistz256_sqr_mont@PLT
	subl	$1, %ebx
	jne	.L33
	movq	-416(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r15, %rdi
	movl	$16, %ebx
	call	ecp_nistz256_mul_mont@PLT
	.p2align 4,,10
	.p2align 3
.L34:
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	ecp_nistz256_sqr_mont@PLT
	subl	$1, %ebx
	jne	.L34
	movq	-424(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r15, %rdi
	movl	$8, %ebx
	call	ecp_nistz256_mul_mont@PLT
	.p2align 4,,10
	.p2align 3
.L35:
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	ecp_nistz256_sqr_mont@PLT
	subl	$1, %ebx
	jne	.L35
	movq	-408(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	ecp_nistz256_mul_mont@PLT
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	ecp_nistz256_sqr_mont@PLT
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	ecp_nistz256_sqr_mont@PLT
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	ecp_nistz256_sqr_mont@PLT
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	ecp_nistz256_sqr_mont@PLT
	movq	-432(%rbp), %rbx
	movq	%r15, %rsi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	call	ecp_nistz256_mul_mont@PLT
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	ecp_nistz256_sqr_mont@PLT
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	ecp_nistz256_sqr_mont@PLT
	movq	-440(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	ecp_nistz256_mul_mont@PLT
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	ecp_nistz256_sqr_mont@PLT
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	ecp_nistz256_sqr_mont@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	ecp_nistz256_mul_mont@PLT
	leaq	-384(%rbp), %r14
	movdqa	-96(%rbp), %xmm0
	movq	%rbx, %rdi
	movdqa	-80(%rbp), %xmm1
	movq	%r14, %rsi
	movaps	%xmm0, -384(%rbp)
	movaps	%xmm1, -368(%rbp)
	call	ecp_nistz256_sqr_mont@PLT
	movq	-408(%rbp), %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	ecp_nistz256_mul_mont@PLT
	cmpq	$0, -392(%rbp)
	je	.L38
	movq	-416(%rbp), %rbx
	movq	-408(%rbp), %rsi
	movq	%rbx, %rdi
	call	ecp_nistz256_from_mont@PLT
	movq	-392(%rbp), %rdi
	movl	$4, %edx
	movq	%rbx, %rsi
	call	bn_set_words@PLT
	testl	%eax, %eax
	je	.L25
.L38:
	cmpq	$0, -400(%rbp)
	movl	$1, %eax
	je	.L25
	movq	-432(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	ecp_nistz256_mul_mont@PLT
	movq	-424(%rbp), %rbx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	ecp_nistz256_mul_mont@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	ecp_nistz256_from_mont@PLT
	movq	-400(%rbp), %rdi
	movl	$4, %edx
	movq	%r15, %rsi
	call	bn_set_words@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L25
.L58:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE450:
	.size	ecp_nistz256_get_affine.part.0, .-ecp_nistz256_get_affine.part.0
	.p2align 4
	.type	ecp_nistz256_get_affine, @function
ecp_nistz256_get_affine:
.LFB438:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$8, %rsp
	call	EC_POINT_is_at_infinity@PLT
	testl	%eax, %eax
	jne	.L63
	addq	$8, %rsp
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ecp_nistz256_get_affine.part.0
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	movl	$1390, %r8d
	movl	$106, %edx
	movl	$240, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE438:
	.size	ecp_nistz256_get_affine, .-ecp_nistz256_get_affine
	.p2align 4
	.type	ecp_nistz256_window_have_precompute_mult, @function
ecp_nistz256_window_have_precompute_mult:
.LFB442:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	EC_GROUP_get0_generator@PLT
	testq	%rax, %rax
	je	.L69
	movq	16(%rax), %rdi
	movq	%rax, %r12
	call	bn_get_top@PLT
	cmpl	$4, %eax
	je	.L82
.L69:
	cmpl	$4, 152(%rbx)
	je	.L67
.L81:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	cmpq	$0, 160(%rbx)
	je	.L81
.L70:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	movq	24(%r12), %rdi
	call	bn_get_top@PLT
	cmpl	$4, %eax
	jne	.L69
	movq	16(%r12), %rdi
	call	bn_get_words@PLT
	movabsq	$8784043285714375740, %rcx
	movq	%rax, %rdx
	movq	(%rax), %rax
	xorq	%rcx, %rax
	movabsq	$8483257759279461889, %rcx
	xorq	8(%rdx), %rcx
	orq	%rax, %rcx
	movabsq	$8789745728267363600, %rax
	xorq	16(%rdx), %rax
	orq	%rax, %rcx
	movabsq	$1770019616739251654, %rax
	xorq	24(%rdx), %rax
	orq	%rcx, %rax
	movq	%rax, %rdx
	negq	%rdx
	orq	%rax, %rdx
	js	.L69
	movq	24(%r12), %rdi
	call	bn_get_words@PLT
	movabsq	$-2453807210370345462, %rcx
	movq	%rax, %rdx
	movq	(%rax), %rax
	xorq	%rcx, %rax
	movabsq	$-8409706061646666660, %rcx
	xorq	8(%rdx), %rcx
	orq	%rax, %rcx
	movabsq	$-3249199208764148955, %rax
	xorq	16(%rdx), %rax
	orq	%rax, %rcx
	movabsq	$-8830996915122840187, %rax
	xorq	24(%rdx), %rax
	orq	%rcx, %rax
	movq	%rax, %rdx
	negq	%rdx
	orq	%rax, %rdx
	js	.L69
	movq	32(%r12), %r13
	movq	%r13, %rdi
	call	bn_get_words@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	bn_get_top@PLT
	cmpl	$4, %eax
	jne	.L69
	movq	(%r12), %rax
	movl	$4294967295, %edx
	salq	$32, %rdx
	xorq	8(%r12), %rdx
	xorq	$1, %rax
	orq	%rax, %rdx
	movq	16(%r12), %rax
	notq	%rax
	orq	%rax, %rdx
	movl	$4294967294, %eax
	xorq	24(%r12), %rax
	orq	%rdx, %rax
	movq	%rax, %rdx
	negq	%rdx
	orq	%rax, %rdx
	jns	.L70
	jmp	.L69
	.cfi_endproc
.LFE442:
	.size	ecp_nistz256_window_have_precompute_mult, .-ecp_nistz256_window_have_precompute_mult
	.p2align 4
	.type	ecp_nistz256_is_affine_G, @function
ecp_nistz256_is_affine_G:
.LFB434:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	16(%rdi), %rdi
	call	bn_get_top@PLT
	cmpl	$4, %eax
	je	.L94
.L84:
	xorl	%eax, %eax
.L83:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	movq	24(%rbx), %rdi
	call	bn_get_top@PLT
	cmpl	$4, %eax
	jne	.L84
	movq	16(%rbx), %rdi
	call	bn_get_words@PLT
	movabsq	$8784043285714375740, %rcx
	movq	%rax, %rdx
	movq	(%rax), %rax
	xorq	%rcx, %rax
	movabsq	$8483257759279461889, %rcx
	xorq	8(%rdx), %rcx
	orq	%rax, %rcx
	movabsq	$8789745728267363600, %rax
	xorq	16(%rdx), %rax
	orq	%rax, %rcx
	movabsq	$1770019616739251654, %rax
	xorq	24(%rdx), %rax
	orq	%rcx, %rax
	movq	%rax, %rdx
	negq	%rdx
	orq	%rax, %rdx
	js	.L84
	movq	24(%rbx), %rdi
	call	bn_get_words@PLT
	movabsq	$-2453807210370345462, %rcx
	movq	%rax, %rdx
	movq	(%rax), %rax
	xorq	%rcx, %rax
	movabsq	$-8409706061646666660, %rcx
	xorq	8(%rdx), %rcx
	orq	%rax, %rcx
	movabsq	$-3249199208764148955, %rax
	xorq	16(%rdx), %rax
	orq	%rax, %rcx
	movabsq	$-8830996915122840187, %rax
	xorq	24(%rdx), %rax
	orq	%rcx, %rax
	movq	%rax, %rdx
	negq	%rdx
	orq	%rax, %rdx
	js	.L84
	movq	32(%rbx), %r12
	movq	%r12, %rdi
	call	bn_get_words@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	bn_get_top@PLT
	cmpl	$4, %eax
	jne	.L84
	movabsq	$-4294967296, %rax
	movq	(%rbx), %rdx
	xorq	8(%rbx), %rax
	xorq	$1, %rdx
	orq	%rax, %rdx
	movq	16(%rbx), %rax
	notq	%rax
	orq	%rdx, %rax
	movl	$4294967294, %edx
	xorq	24(%rbx), %rdx
	orq	%rax, %rdx
	movl	$1, %eax
	movq	%rdx, %rcx
	negq	%rcx
	orq	%rdx, %rcx
	jns	.L83
	jmp	.L84
	.cfi_endproc
.LFE434:
	.size	ecp_nistz256_is_affine_G, .-ecp_nistz256_is_affine_G
	.p2align 4
	.type	ecp_nistz256_points_mul, @function
ecp_nistz256_points_mul:
.LFB437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	andq	$-32, %rsp
	subq	$448, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	%rdi, 112(%rsp)
	movq	%rsi, 80(%rsp)
	movq	%rdx, 136(%rsp)
	movq	%rcx, 160(%rsp)
	movq	%r8, 120(%rsp)
	movq	%r9, 144(%rsp)
	movq	%rax, 152(%rsp)
	movq	%fs:40, %rax
	movq	%rax, 440(%rsp)
	xorl	%eax, %eax
	movb	$0, 432(%rsp)
	movaps	%xmm0, 400(%rsp)
	movaps	%xmm0, 416(%rsp)
	cmpq	$268435454, %rcx
	ja	.L228
	movq	152(%rsp), %rdi
	call	BN_CTX_start@PLT
	cmpq	$0, 136(%rsp)
	je	.L229
	movq	112(%rsp), %rdi
	call	EC_GROUP_get0_generator@PLT
	movl	$1182, %r8d
	movl	$113, %edx
	leaq	.LC0(%rip), %rcx
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L226
	movq	112(%rsp), %rax
	movq	160(%rax), %rbx
	testq	%rbx, %rbx
	je	.L225
	movq	%rax, %rdi
	call	EC_POINT_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L227
	movq	16(%rbx), %rsi
	leaq	288(%rsp), %r14
	movl	$1, %edx
	movq	%r14, %rdi
	call	ecp_nistz256_gather_w7@PLT
	movq	16(%r13), %rdi
	movl	$4, %edx
	movq	%r14, %rsi
	call	bn_set_words@PLT
	testl	%eax, %eax
	jne	.L230
.L108:
	movq	%r13, %rdi
	call	EC_POINT_free@PLT
.L227:
	movq	$0, 88(%rsp)
	xorl	%r12d, %r12d
	movq	$0, 136(%rsp)
.L102:
	movq	152(%rsp), %rdi
	call	BN_CTX_end@PLT
	movq	88(%rsp), %rdi
	movl	$1373, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	136(%rsp), %rdi
	movl	$1374, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
.L95:
	movq	440(%rsp), %rax
	xorq	%fs:40, %rax
	jne	.L231
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L229:
	.cfi_restore_state
	cmpq	$0, 160(%rsp)
	jne	.L162
	leaq	288(%rsp), %rax
	movq	%rax, 56(%rsp)
.L125:
	movq	$0, 88(%rsp)
	movq	$0, 136(%rsp)
.L100:
	movq	80(%rsp), %rbx
	movq	56(%rsp), %rsi
	movl	$4, %edx
	movq	16(%rbx), %rdi
	call	bn_set_words@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L102
	movq	24(%rbx), %rdi
	leaq	320(%rsp), %rsi
	movl	$4, %edx
	call	bn_set_words@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L102
	movq	32(%rbx), %rdi
	leaq	352(%rsp), %rsi
	movl	$4, %edx
	call	bn_set_words@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L102
	movq	32(%rbx), %r12
	movq	%r12, %rdi
	call	bn_get_words@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	bn_get_top@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	cmpl	$4, %r8d
	jne	.L157
	movabsq	$-4294967296, %rdx
	movq	(%rbx), %rax
	xorq	8(%rbx), %rdx
	xorq	$1, %rax
	orq	%rdx, %rax
	movq	16(%rbx), %rdx
	notq	%rdx
	orq	%rax, %rdx
	movl	$4294967294, %eax
	xorq	24(%rbx), %rax
	orq	%rax, %rdx
	movq	%rdx, %rax
	negq	%rax
	orq	%rdx, %rax
	notq	%rax
	shrq	$63, %rax
.L157:
	movq	80(%rsp), %rbx
	movl	$1, %r12d
	movl	%eax, 40(%rbx)
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L228:
	movl	$1173, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$241, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L162:
	leaq	288(%rsp), %rax
	movl	$1, 28(%rsp)
	movq	$0, 88(%rsp)
	movq	%rax, %r14
	movq	%rax, 56(%rsp)
.L99:
	movq	160(%rsp), %rax
	salq	$4, %rax
	addq	$6, %rax
	cmpq	$22369621, %rax
	ja	.L129
	movq	160(%rsp), %r15
	movl	$630, %edx
	leaq	.LC0(%rip), %rsi
	leaq	(%r15,%r15,2), %rbx
	salq	$9, %rbx
	leaq	544(%rbx), %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, 40(%rsp)
	testq	%rax, %rax
	je	.L129
	movq	%r15, %rax
	movl	$632, %edx
	leaq	.LC0(%rip), %rsi
	salq	$5, %rax
	addq	%r15, %rax
	movq	%rax, %rdi
	movq	%rax, 32(%rsp)
	call	CRYPTO_malloc@PLT
	movq	%rax, 72(%rsp)
	testq	%rax, %rax
	je	.L128
	leaq	0(,%r15,8), %rdi
	movl	$633, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, 64(%rsp)
	testq	%rax, %rax
	je	.L128
	movq	40(%rsp), %rax
	movq	%r14, (%rsp)
	xorl	%r12d, %r12d
	andq	$-64, %rax
	addq	$64, %rax
	leaq	(%rax,%rbx), %r15
	movq	%rax, 48(%rsp)
	movq	%rax, %rbx
	movq	%rax, 16(%rsp)
	movq	72(%rsp), %rax
	movq	%rax, 8(%rsp)
	movq	%rax, 176(%rsp)
	.p2align 4,,10
	.p2align 3
.L150:
	movq	144(%rsp), %rax
	movq	(%rax,%r12,8), %rdi
	call	BN_num_bits@PLT
	cmpl	$256, %eax
	jg	.L134
	movq	144(%rsp), %rax
	movq	(%rax,%r12,8), %rdi
	call	BN_is_negative@PLT
	testl	%eax, %eax
	jne	.L134
	movq	64(%rsp), %rdi
	leaq	(%rdi,%r12,8), %rax
	movq	%rax, %rsi
	movq	144(%rsp), %rax
	movq	(%rax,%r12,8), %rax
	movq	%rax, (%rdi,%r12,8)
.L137:
	movq	176(%rsp), %r13
	movq	%rbx, 168(%rsp)
	xorl	%r14d, %r14d
	movq	%r13, %rbx
	movq	%rsi, %r13
	.p2align 4,,10
	.p2align 3
.L138:
	movq	0(%r13), %rdi
	call	bn_get_top@PLT
	sall	$3, %eax
	cmpl	%eax, %r14d
	jge	.L232
	movq	0(%r13), %rdi
	addq	$8, %rbx
	call	bn_get_words@PLT
	movq	%rax, %r8
	movl	%r14d, %eax
	addl	$8, %r14d
	sarl	$3, %eax
	cltq
	movq	(%r8,%rax,8), %rax
	movq	%rax, %rcx
	movb	%al, -8(%rbx)
	shrq	$8, %rcx
	movb	%cl, -7(%rbx)
	movq	%rax, %rcx
	shrq	$16, %rcx
	movb	%cl, -6(%rbx)
	movq	%rax, %rcx
	shrq	$24, %rcx
	movb	%cl, -5(%rbx)
	movq	%rax, %rcx
	shrq	$32, %rcx
	movb	%cl, -4(%rbx)
	movq	%rax, %rcx
	shrq	$40, %rcx
	movb	%cl, -3(%rbx)
	movq	%rax, %rcx
	shrq	$56, %rax
	shrq	$48, %rcx
	movb	%al, -1(%rbx)
	movb	%cl, -2(%rbx)
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L232:
	movq	168(%rsp), %rbx
	cmpl	$32, %r14d
	jg	.L149
	movl	$32, %eax
	movslq	%r14d, %rcx
	xorl	%edi, %edi
	addq	176(%rsp), %rcx
	subl	%r14d, %eax
	addq	$1, %rax
	cmpl	$8, %eax
	jnb	.L143
	testb	$4, %al
	jne	.L233
	testl	%eax, %eax
	je	.L149
	movb	$0, (%rcx)
	testb	$2, %al
	jne	.L234
.L149:
	movq	120(%rsp), %rax
	movl	$4, %edx
	movq	%r15, %rdi
	movq	(%rax,%r12,8), %rax
	movq	16(%rax), %rsi
	call	bn_copy_words@PLT
	testl	%eax, %eax
	je	.L141
	movq	120(%rsp), %r14
	leaq	32(%r15), %rdi
	movl	$4, %edx
	movq	%rdi, 128(%rsp)
	movq	(%r14,%r12,8), %rax
	movq	24(%rax), %rsi
	call	bn_copy_words@PLT
	testl	%eax, %eax
	je	.L141
	movq	(%r14,%r12,8), %rax
	leaq	64(%r15), %rdi
	movl	$4, %edx
	movq	32(%rax), %rsi
	call	bn_copy_words@PLT
	testl	%eax, %eax
	je	.L141
	leaq	96(%r15), %r13
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	ecp_nistz256_scatter_w5@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	addq	$1, %r12
	call	ecp_nistz256_point_double@PLT
	movq	%rbx, %rdi
	movl	$2, %edx
	movq	%r13, %rsi
	call	ecp_nistz256_scatter_w5@PLT
	leaq	192(%r15), %r14
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	ecp_nistz256_point_add@PLT
	movl	$3, %edx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	call	ecp_nistz256_scatter_w5@PLT
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	ecp_nistz256_point_double@PLT
	movl	$4, %edx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	call	ecp_nistz256_scatter_w5@PLT
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	ecp_nistz256_point_double@PLT
	movq	%rbx, %rdi
	movl	$6, %edx
	movq	%r14, %rsi
	call	ecp_nistz256_scatter_w5@PLT
	leaq	288(%r15), %r11
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r11, %rdi
	movq	%r11, 168(%rsp)
	call	ecp_nistz256_point_add@PLT
	movq	168(%rsp), %r11
	movq	%rbx, %rdi
	movl	$5, %edx
	movq	%r11, %rsi
	movq	%r11, 96(%rsp)
	call	ecp_nistz256_scatter_w5@PLT
	leaq	384(%r15), %r10
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r10, %rdi
	movq	%r10, 168(%rsp)
	call	ecp_nistz256_point_add@PLT
	movq	168(%rsp), %r10
	movl	$7, %edx
	movq	%rbx, %rdi
	movq	%r10, %rsi
	movq	%r10, 104(%rsp)
	call	ecp_nistz256_scatter_w5@PLT
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	ecp_nistz256_point_double@PLT
	movl	$8, %edx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	call	ecp_nistz256_scatter_w5@PLT
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	ecp_nistz256_point_double@PLT
	movl	$12, %edx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	call	ecp_nistz256_scatter_w5@PLT
	movq	96(%rsp), %r11
	movq	%r11, %rsi
	movq	%r11, %rdi
	movq	%r11, 168(%rsp)
	call	ecp_nistz256_point_double@PLT
	movq	168(%rsp), %r11
	movl	$10, %edx
	movq	%rbx, %rdi
	movq	%r11, %rsi
	movq	%r11, 96(%rsp)
	call	ecp_nistz256_scatter_w5@PLT
	movq	104(%rsp), %r10
	movq	%r10, %rsi
	movq	%r10, %rdi
	movq	%r10, 168(%rsp)
	call	ecp_nistz256_point_double@PLT
	movq	168(%rsp), %r10
	movq	%rbx, %rdi
	movl	$14, %edx
	movq	%r10, %rsi
	movq	%r10, 104(%rsp)
	call	ecp_nistz256_scatter_w5@PLT
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	ecp_nistz256_point_add@PLT
	movq	%rbx, %rdi
	movl	$13, %edx
	movq	%r14, %rsi
	call	ecp_nistz256_scatter_w5@PLT
	movq	96(%rsp), %r11
	movq	%r15, %rdx
	movq	%r11, %rsi
	movq	%r11, %rdi
	movq	%r11, 168(%rsp)
	call	ecp_nistz256_point_add@PLT
	movq	168(%rsp), %r11
	movq	%rbx, %rdi
	movl	$11, %edx
	movq	%r11, %rsi
	call	ecp_nistz256_scatter_w5@PLT
	movq	104(%rsp), %r10
	movq	%r15, %rdx
	movq	%r10, %rsi
	movq	%r10, %rdi
	movq	%r10, 168(%rsp)
	call	ecp_nistz256_point_add@PLT
	movq	168(%rsp), %r10
	movq	%rbx, %rdi
	movl	$15, %edx
	movq	%r10, %rsi
	call	ecp_nistz256_scatter_w5@PLT
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	ecp_nistz256_point_add@PLT
	movl	$9, %edx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	call	ecp_nistz256_scatter_w5@PLT
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	ecp_nistz256_point_double@PLT
	movq	%rbx, %rdi
	movl	$16, %edx
	movq	%r13, %rsi
	call	ecp_nistz256_scatter_w5@PLT
	addq	$1536, %rbx
	addq	$33, 176(%rsp)
	cmpq	160(%rsp), %r12
	jb	.L150
	movq	72(%rsp), %r12
	movq	48(%rsp), %rsi
	movq	%r15, %rdi
	leaq	128(%r15), %rbx
	movq	(%rsp), %r14
	movzbl	31(%r12), %edx
	movl	%edx, %eax
	shrl	$7, %edx
	shrl	$6, %eax
	andl	$1, %eax
	addl	%eax, %edx
	call	ecp_nistz256_gather_w5@PLT
	movdqu	(%r15), %xmm4
	movq	32(%rsp), %rax
	movl	$255, 144(%rsp)
	movq	%rbx, 96(%rsp)
	movaps	%xmm4, (%r14)
	movdqu	16(%r15), %xmm5
	addq	%r12, %rax
	movq	%r14, %r12
	movq	%rax, 32(%rsp)
	movaps	%xmm5, 16(%r14)
	movdqu	32(%r15), %xmm4
	movaps	%xmm4, 32(%r14)
	movdqu	48(%r15), %xmm5
	movaps	%xmm5, 48(%r14)
	movdqu	64(%r15), %xmm4
	movaps	%xmm4, 64(%r14)
	movdqa	.LC1(%rip), %xmm4
	movdqu	80(%r15), %xmm5
	movaps	%xmm4, 176(%rsp)
	movaps	%xmm5, 80(%r14)
	movq	%r15, %r14
	.p2align 4,,10
	.p2align 3
.L152:
	xorl	%eax, %eax
	cmpl	$255, 144(%rsp)
	sete	%al
	cmpq	160(%rsp), %rax
	jnb	.L155
	movl	144(%rsp), %ebx
	movq	%rax, %r9
	movq	72(%rsp), %rsi
	leaq	(%rax,%rax), %r11
	salq	$5, %r9
	leal	-1(%rbx), %edx
	addq	%rax, %r9
	addq	%r11, %rax
	movl	%edx, %r13d
	salq	$9, %rax
	andl	$7, %edx
	shrl	$3, %r13d
	movl	%edx, 112(%rsp)
	movl	%r13d, %edi
	leal	1(%r13), %ebx
	addq	%rdi, %r9
	movq	%rdi, 120(%rsp)
	addq	32(%rsp), %rdi
	leaq	(%rsi,%r9), %r15
	movq	48(%rsp), %rsi
	movq	%rdi, 104(%rsp)
	leaq	(%rsi,%rax), %r13
	movq	%rbx, %rax
	movq	%r14, %rbx
	movq	%r13, %rdi
	movq	%rax, %r14
	movq	%r15, %r13
	movq	%rdi, %r15
	.p2align 4,,10
	.p2align 3
.L154:
	movq	%r13, %rax
	subq	120(%rsp), %rax
	movzbl	0(%r13), %esi
	addq	$33, %r13
	movzbl	(%rax,%r14), %edx
	movzbl	112(%rsp), %ecx
	sall	$8, %edx
	orl	%esi, %edx
	shrl	%cl, %edx
	movl	%edx, %esi
	movl	$63, %edx
	andl	$63, %esi
	movl	%esi, %eax
	subl	%esi, %edx
	shrl	$5, %eax
	movl	%edx, %edi
	movl	%eax, %ecx
	leal	-1(%rax), %edx
	negl	%ecx
	andl	%esi, %edx
	movq	%r15, %rsi
	addq	$1536, %r15
	andl	%ecx, %edi
	andl	$1, %ecx
	orl	%edi, %edx
	movq	%rbx, %rdi
	movl	%edx, %eax
	andl	$1, %edx
	shrl	%eax
	addl	%eax, %edx
	leal	(%rcx,%rdx,2), %eax
	andl	$2147483647, %edx
	movl	%eax, 168(%rsp)
	call	ecp_nistz256_gather_w5@PLT
	movq	128(%rsp), %rsi
	movq	96(%rsp), %rdi
	call	ecp_nistz256_neg@PLT
	pxor	%xmm1, %xmm1
	pxor	%xmm2, %xmm2
	movq	%rbx, %rdx
	movdqa	%xmm2, %xmm4
	movdqu	32(%rbx), %xmm5
	movq	%r12, %rsi
	movq	%r12, %rdi
	movd	168(%rsp), %xmm6
	movdqu	144(%rbx), %xmm7
	pshufd	$0, %xmm6, %xmm0
	movdqu	48(%rbx), %xmm6
	pand	176(%rsp), %xmm0
	movdqa	%xmm0, %xmm3
	punpckhdq	%xmm1, %xmm0
	punpckldq	%xmm1, %xmm3
	movdqa	.LC2(%rip), %xmm1
	psubq	%xmm0, %xmm4
	psubq	%xmm3, %xmm2
	paddq	%xmm0, %xmm1
	movdqa	%xmm4, %xmm0
	movdqu	128(%rbx), %xmm4
	pand	%xmm6, %xmm1
	pand	%xmm7, %xmm0
	pxor	%xmm1, %xmm0
	movdqa	.LC2(%rip), %xmm1
	pand	%xmm4, %xmm2
	movups	%xmm0, 48(%rbx)
	paddq	%xmm3, %xmm1
	pand	%xmm5, %xmm1
	pxor	%xmm2, %xmm1
	movups	%xmm1, 32(%rbx)
	call	ecp_nistz256_point_add@PLT
	cmpq	104(%rsp), %r13
	jne	.L154
	movq	%rbx, %r14
.L155:
	movq	%r12, %rsi
	movq	%r12, %rdi
	subl	$5, 144(%rsp)
	movl	144(%rsp), %ebx
	call	ecp_nistz256_point_double@PLT
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	ecp_nistz256_point_double@PLT
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	ecp_nistz256_point_double@PLT
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	ecp_nistz256_point_double@PLT
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	ecp_nistz256_point_double@PLT
	testl	%ebx, %ebx
	jne	.L152
	movq	96(%rsp), %rbx
	movq	%r14, %r15
	movq	%r12, %r14
	movq	16(%rsp), %r12
	xorl	%r13d, %r13d
	movq	%rbx, 144(%rsp)
	movq	8(%rsp), %rbx
	.p2align 4,,10
	.p2align 3
.L153:
	movzbl	(%rbx), %eax
	addq	$1, %r13
	addq	$33, %rbx
	addl	%eax, %eax
	andl	$63, %eax
	movl	%eax, %edx
	movl	%eax, %edi
	movl	$63, %eax
	shrl	$5, %edx
	subl	%edi, %eax
	movl	%edx, %esi
	negl	%esi
	andl	%esi, %eax
	andl	$1, %esi
	movl	%eax, %r11d
	leal	-1(%rdx), %eax
	andl	%edi, %eax
	orl	%r11d, %eax
	movl	%eax, %edx
	andl	$1, %eax
	shrl	%edx
	addl	%edx, %eax
	leal	(%rsi,%rax,2), %edi
	andl	$2147483647, %eax
	movq	%r12, %rsi
	addq	$1536, %r12
	movl	%eax, %edx
	movl	%edi, 168(%rsp)
	movq	%r15, %rdi
	call	ecp_nistz256_gather_w5@PLT
	movq	128(%rsp), %rsi
	movq	144(%rsp), %rdi
	call	ecp_nistz256_neg@PLT
	pxor	%xmm1, %xmm1
	pxor	%xmm2, %xmm2
	movq	%r15, %rdx
	movd	168(%rsp), %xmm4
	movdqa	%xmm2, %xmm5
	movq	%r14, %rsi
	movq	%r14, %rdi
	pshufd	$0, %xmm4, %xmm0
	movdqu	48(%r15), %xmm4
	pand	176(%rsp), %xmm0
	movdqa	%xmm0, %xmm3
	punpckhdq	%xmm1, %xmm0
	punpckldq	%xmm1, %xmm3
	movdqa	.LC2(%rip), %xmm1
	psubq	%xmm0, %xmm5
	psubq	%xmm3, %xmm2
	paddq	%xmm0, %xmm1
	movdqa	%xmm5, %xmm0
	movdqu	144(%r15), %xmm5
	pand	%xmm4, %xmm1
	movdqu	32(%r15), %xmm4
	pand	%xmm5, %xmm0
	movdqu	128(%r15), %xmm5
	pxor	%xmm1, %xmm0
	movdqa	.LC2(%rip), %xmm1
	pand	%xmm5, %xmm2
	movups	%xmm0, 48(%r15)
	paddq	%xmm3, %xmm1
	pand	%xmm4, %xmm1
	pxor	%xmm2, %xmm1
	movups	%xmm1, 32(%r15)
	call	ecp_nistz256_point_add@PLT
	cmpq	160(%rsp), %r13
	jb	.L153
	movq	40(%rsp), %rdi
	movl	$778, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	72(%rsp), %rdi
	movl	$779, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	64(%rsp), %rdi
	movl	$780, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	28(%rsp), %eax
	testl	%eax, %eax
	jne	.L100
	movq	56(%rsp), %rdi
	movq	%r14, %rdx
	movq	%rdi, %rsi
	call	ecp_nistz256_point_add@PLT
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L134:
	movq	152(%rsp), %rdi
	call	BN_CTX_get@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L131
	movq	112(%rsp), %rax
	movq	152(%rsp), %rcx
	movq	%r13, %rdi
	movq	16(%rax), %rdx
	movq	144(%rsp), %rax
	movq	(%rax,%r12,8), %rsi
	call	BN_nnmod@PLT
	testl	%eax, %eax
	je	.L235
	movq	64(%rsp), %rax
	leaq	(%rax,%r12,8), %rdi
	movq	%r13, (%rax,%r12,8)
	movq	%rdi, %rsi
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L143:
	movl	%eax, %edx
	movq	$0, (%rcx)
	movq	$0, -8(%rcx,%rdx)
	leaq	8(%rcx), %rdx
	andq	$-8, %rdx
	subq	%rdx, %rcx
	addl	%eax, %ecx
	andl	$-8, %ecx
	cmpl	$8, %ecx
	jb	.L149
	andl	$-8, %ecx
	xorl	%eax, %eax
.L147:
	movl	%eax, %esi
	addl	$8, %eax
	movq	%rdi, (%rdx,%rsi)
	cmpl	%ecx, %eax
	jb	.L147
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L129:
	movq	$0, 40(%rsp)
	movq	$0, 72(%rsp)
.L128:
	movl	$634, %r8d
	movl	$65, %edx
	movl	$242, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	$0, 64(%rsp)
.L131:
	movq	40(%rsp), %rdi
	movl	$778, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%r12d, %r12d
	call	CRYPTO_free@PLT
	movq	72(%rsp), %rdi
	movl	$779, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	64(%rsp), %rdi
	movl	$780, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L230:
	movq	24(%r13), %rdi
	leaq	320(%rsp), %rsi
	movl	$4, %edx
	call	bn_set_words@PLT
	testl	%eax, %eax
	je	.L108
	movq	32(%r13), %rdi
	movl	$4, %edx
	leaq	ONE(%rip), %rsi
	call	bn_set_words@PLT
	testl	%eax, %eax
	je	.L108
	movl	$1, 40(%r13)
	movq	112(%rsp), %rdi
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	152(%rsp), %rcx
	call	EC_POINT_cmp@PLT
	testl	%eax, %eax
	je	.L109
	movq	%r13, %rdi
	call	EC_POINT_free@PLT
.L225:
	movq	%r12, %rdi
	call	ecp_nistz256_is_affine_G
	testl	%eax, %eax
	je	.L236
	leaq	ecp_nistz256_precomputed(%rip), %rbx
.L111:
	movq	136(%rsp), %rdi
	call	BN_num_bits@PLT
	cmpl	$256, %eax
	jg	.L159
	movq	136(%rsp), %rdi
	call	BN_is_negative@PLT
	testl	%eax, %eax
	je	.L163
.L159:
	movq	152(%rsp), %r15
	movq	%r15, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L227
	movq	112(%rsp), %rax
	movq	136(%rsp), %rsi
	movq	%r15, %rcx
	movq	%r13, %rdi
	movq	16(%rax), %rdx
	call	BN_nnmod@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L237
.L113:
	leaq	400(%rsp), %r14
	xorl	%r12d, %r12d
	movq	%r14, %r15
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L116:
	movq	%r13, %rdi
	addq	$8, %r15
	call	bn_get_words@PLT
	movq	%rax, %r8
	movl	%r12d, %eax
	addl	$8, %r12d
	sarl	$3, %eax
	cltq
	movq	(%r8,%rax,8), %rax
	movq	%rax, %rdx
	movb	%al, -8(%r15)
	shrq	$8, %rdx
	movb	%dl, -7(%r15)
	movq	%rax, %rdx
	shrq	$16, %rdx
	movb	%dl, -6(%r15)
	movq	%rax, %rdx
	shrq	$24, %rdx
	movb	%dl, -5(%r15)
	movq	%rax, %rdx
	shrq	$32, %rdx
	movb	%dl, -4(%r15)
	movq	%rax, %rdx
	shrq	$40, %rdx
	movb	%dl, -3(%r15)
	movq	%rax, %rdx
	shrq	$56, %rax
	movb	%al, -1(%r15)
	shrq	$48, %rdx
	movb	%dl, -2(%r15)
.L115:
	movq	%r13, %rdi
	call	bn_get_top@PLT
	sall	$3, %eax
	cmpl	%r12d, %eax
	jg	.L116
	movl	$32, %eax
	xorl	%edi, %edi
	subl	%r12d, %eax
	movslq	%r12d, %r12
	addq	$1, %rax
	addq	%r14, %r12
	cmpl	$8, %eax
	jnb	.L117
	testb	$4, %al
	jne	.L238
	testl	%eax, %eax
	je	.L118
	movb	$0, (%r12)
	testb	$2, %al
	jne	.L239
.L118:
	movzbl	400(%rsp), %r10d
	movl	$255, %esi
	movl	$6, %r15d
	leaq	256(%rsp), %r13
	leaq	224(%rsp), %r14
	leal	(%r10,%r10), %eax
	movl	%r10d, 168(%rsp)
	movzbl	%al, %edi
	movl	%edi, %edx
	subl	%edi, %esi
	shrl	$7, %edx
	movl	%edx, %ecx
	leal	-1(%rdx), %eax
	negl	%ecx
	andl	%edi, %eax
	andl	%ecx, %esi
	andl	$1, %ecx
	orl	%esi, %eax
	movq	%rbx, %rsi
	movl	%eax, %edx
	andl	$1, %eax
	shrl	%edx
	addl	%edx, %eax
	leal	(%rcx,%rax,2), %r12d
	andl	$2147483647, %eax
	movl	%eax, %edx
	leaq	288(%rsp), %rax
	movq	%rax, %rdi
	movq	%rax, 56(%rsp)
	call	ecp_nistz256_gather_w7@PLT
	leaq	320(%rsp), %rsi
	leaq	352(%rsp), %rdi
	call	ecp_nistz256_neg@PLT
	movl	%r12d, %r8d
	movq	352(%rsp), %rax
	movq	360(%rsp), %rsi
	andl	$1, %r8d
	movq	368(%rsp), %rcx
	movdqa	.LC1(%rip), %xmm4
	movq	%r13, 136(%rsp)
	movq	%r8, %rdx
	subq	$1, %r8
	movl	%r15d, %r13d
	movl	168(%rsp), %r10d
	negq	%rdx
	leaq	192(%rsp), %r12
	movaps	%xmm4, 176(%rsp)
	andq	%rdx, %rax
	andq	%rdx, %rsi
	andq	%rdx, %rcx
	movq	%rax, %rdi
	movq	320(%rsp), %rax
	andq	%r8, %rax
	xorq	%rax, %rdi
	movq	328(%rsp), %rax
	movq	%rdi, 320(%rsp)
	andq	%r8, %rax
	xorq	%rax, %rsi
	movq	336(%rsp), %rax
	movq	%rsi, 328(%rsp)
	andq	%r8, %rax
	andq	344(%rsp), %r8
	xorq	%rax, %rcx
	movq	376(%rsp), %rax
	movq	%rcx, 336(%rsp)
	andq	%rdx, %rax
	xorq	%r8, %rax
	movq	%rax, %rdx
	movq	%rax, 344(%rsp)
	movq	288(%rsp), %rax
	orq	296(%rsp), %rax
	orq	304(%rsp), %rax
	orq	312(%rsp), %rax
	orq	%rdi, %rax
	orq	%rsi, %rax
	addq	$4096, %rbx
	orq	%rcx, %rax
	movq	%rbx, %r15
	movl	%r10d, %ecx
	movq	56(%rsp), %rbx
	orq	%rdx, %rax
	movq	%rax, %rdx
	negq	%rdx
	orq	%rdx, %rax
	cqto
	shrq	$63, %rax
	movq	%rax, 352(%rsp)
	movabsq	$-4294967296, %rax
	andq	%rdx, %rax
	movq	%rdx, 368(%rsp)
	andl	$4294967294, %edx
	movq	%rax, 360(%rsp)
	movq	%rdx, 376(%rsp)
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L240:
	movl	%r13d, %eax
	shrl	$3, %eax
	movzbl	400(%rsp,%rax), %ecx
.L124:
	movl	%r13d, %esi
	shrl	$3, %esi
	addl	$1, %esi
	movzbl	400(%rsp,%rsi), %edx
	sall	$8, %edx
	orl	%ecx, %edx
	movl	%r13d, %ecx
	addl	$7, %r13d
	andl	$7, %ecx
	shrl	%cl, %edx
	movzbl	%dl, %ecx
	movl	$255, %edx
	movl	%ecx, %eax
	subl	%ecx, %edx
	shrl	$7, %eax
	movl	%edx, %edi
	movl	%eax, %esi
	leal	-1(%rax), %edx
	negl	%esi
	andl	%ecx, %edx
	andl	%esi, %edi
	andl	$1, %esi
	orl	%edi, %edx
	movq	%r12, %rdi
	movl	%edx, %eax
	andl	$1, %edx
	shrl	%eax
	addl	%eax, %edx
	leal	(%rsi,%rdx,2), %eax
	andl	$2147483647, %edx
	movq	%r15, %rsi
	addq	$4096, %r15
	movl	%eax, 168(%rsp)
	call	ecp_nistz256_gather_w7@PLT
	movq	136(%rsp), %rdi
	movq	%r14, %rsi
	call	ecp_nistz256_neg@PLT
	pxor	%xmm1, %xmm1
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movd	168(%rsp), %xmm6
	movdqa	.LC2(%rip), %xmm2
	movq	%rbx, %rdi
	pshufd	$0, %xmm6, %xmm0
	pand	176(%rsp), %xmm0
	movdqa	%xmm0, %xmm3
	punpckhdq	%xmm1, %xmm0
	punpckldq	%xmm1, %xmm3
	pxor	%xmm1, %xmm1
	paddq	%xmm0, %xmm2
	pand	240(%rsp), %xmm2
	movdqa	%xmm1, %xmm5
	psubq	%xmm3, %xmm1
	pand	256(%rsp), %xmm1
	psubq	%xmm0, %xmm5
	movdqa	272(%rsp), %xmm0
	pand	%xmm5, %xmm0
	pxor	%xmm2, %xmm0
	movdqa	.LC2(%rip), %xmm2
	movaps	%xmm0, 240(%rsp)
	paddq	%xmm3, %xmm2
	pand	224(%rsp), %xmm2
	pxor	%xmm2, %xmm1
	movaps	%xmm1, 224(%rsp)
	call	ecp_nistz256_point_add_affine@PLT
	cmpl	$258, %r13d
	jne	.L240
	cmpq	$0, 160(%rsp)
	je	.L125
	movl	$0, 28(%rsp)
	movq	%r12, %r14
	movq	$0, 88(%rsp)
	movq	$0, 136(%rsp)
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L236:
	addq	$1, 160(%rsp)
	movq	160(%rsp), %rax
	movl	$1327, %edx
	leaq	.LC0(%rip), %rsi
	leaq	0(,%rax,8), %r13
	movq	%r13, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L241
	movl	$1333, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, 88(%rsp)
	testq	%rax, %rax
	je	.L242
	leaq	-8(%r13), %r14
	movq	144(%rsp), %rsi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	call	memcpy@PLT
	movq	136(%rsp), %rax
	movq	88(%rsp), %r15
	movq	%r14, %rdx
	movq	120(%rsp), %rsi
	movq	%rax, -8(%rbx,%r13)
	movq	%r15, %rdi
	call	memcpy@PLT
	leaq	288(%rsp), %rax
	movq	%r12, -8(%r15,%r13)
	movq	%rbx, 144(%rsp)
	movq	%rax, %r14
	movq	%r15, 120(%rsp)
	movq	%rbx, 136(%rsp)
	movl	$1, 28(%rsp)
	movq	%rax, 56(%rsp)
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L117:
	leaq	8(%r12), %rcx
	movl	%eax, %edx
	movq	$0, (%r12)
	movq	$0, -8(%r12,%rdx)
	andq	$-8, %rcx
	subq	%rcx, %r12
	addl	%r12d, %eax
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L118
	andl	$-8, %eax
	xorl	%edx, %edx
.L121:
	movl	%edx, %esi
	addl	$8, %edx
	movq	%rdi, (%rcx,%rsi)
	cmpl	%eax, %edx
	jb	.L121
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L163:
	movq	136(%rsp), %r13
	jmp	.L113
.L237:
	movl	$1228, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$3, %edx
.L226:
	movl	$241, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	movq	$0, 88(%rsp)
	movq	$0, 136(%rsp)
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L109:
	movq	16(%rbx), %rbx
	movq	%r13, %rdi
	call	EC_POINT_free@PLT
	testq	%rbx, %rbx
	jne	.L111
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L141:
	movl	$679, %r8d
	movl	$146, %edx
	movl	$242, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L233:
	movl	%eax, %eax
	movl	$0, (%rcx)
	movl	$0, -4(%rcx,%rax)
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L241:
	movl	$1329, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$241, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	movq	$0, 88(%rsp)
	movq	$0, 136(%rsp)
	jmp	.L102
.L242:
	movl	$1335, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$241, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	movq	%rbx, 136(%rsp)
	jmp	.L102
.L234:
	movl	%eax, %eax
	xorl	%edx, %edx
	movw	%dx, -2(%rcx,%rax)
	jmp	.L149
.L235:
	movl	$651, %r8d
	movl	$3, %edx
	movl	$242, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L131
.L238:
	movl	%eax, %edx
	movl	$0, (%r12)
	movl	$0, -4(%r12,%rdx)
	jmp	.L118
.L239:
	movl	%eax, %edx
	xorl	%ecx, %ecx
	movw	%cx, -2(%r12,%rdx)
	jmp	.L118
.L231:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE437:
	.size	ecp_nistz256_points_mul, .-ecp_nistz256_points_mul
	.p2align 4
	.type	ecp_nistz256_mult_precompute, @function
ecp_nistz256_mult_precompute:
.LFB435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$184, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EC_pre_comp_free@PLT
	movq	%rbx, %rdi
	call	EC_GROUP_get0_generator@PLT
	testq	%rax, %rax
	je	.L299
	movq	%rax, %rdi
	movq	%rax, %r15
	call	ecp_nistz256_is_affine_G
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L300
	movl	$1, %r13d
.L243:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L301
	addq	$184, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L300:
	.cfi_restore_state
	testq	%rbx, %rbx
	je	.L243
	movl	$1429, %edx
	leaq	.LC0(%rip), %rsi
	movl	$48, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L302
	movq	%rbx, (%rax)
	movq	%rax, %r14
	leaq	32(%rax), %rax
	movq	$6, -24(%rax)
	movq	%rax, -168(%rbp)
	movl	$1, 32(%r14)
	mfence
	call	CRYPTO_THREAD_lock_new@PLT
	movq	%rax, 40(%r14)
	testq	%rax, %rax
	je	.L303
	movq	$0, -160(%rbp)
	testq	%r12, %r12
	je	.L304
.L248:
	movq	%r12, %rdi
	call	BN_CTX_start@PLT
	movq	%rbx, %rdi
	call	EC_GROUP_get0_order@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L268
	movq	%rax, %rdi
	call	BN_is_zero@PLT
	movl	%eax, -172(%rbp)
	testl	%eax, %eax
	jne	.L305
	movl	$866, %edx
	leaq	.LC0(%rip), %rsi
	movl	$151616, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L306
	movq	%rbx, %rdi
	call	EC_POINT_new@PLT
	movq	%rbx, %rdi
	movq	%rax, %r14
	call	EC_POINT_new@PLT
	movq	%rax, -144(%rbp)
	testq	%r14, %r14
	je	.L249
	testq	%rax, %rax
	je	.L249
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	EC_POINT_copy@PLT
	testl	%eax, %eax
	je	.L249
	movq	-152(%rbp), %rax
	movq	%r15, -216(%rbp)
	movl	%r13d, -176(%rbp)
	andq	$-64, %rax
	leaq	64(%rax), %rcx
	addq	$151616, %rax
	movq	%rax, -200(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, -184(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rcx, -208(%rbp)
	movq	%rax, -192(%rbp)
.L259:
	movq	-144(%rbp), %rsi
	movq	%r14, %rdi
	call	EC_POINT_copy@PLT
	testl	%eax, %eax
	je	.L297
	movq	-208(%rbp), %r15
.L257:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	EC_POINT_make_affine@PLT
	testl	%eax, %eax
	je	.L297
	movq	16(%r14), %rsi
	movq	-184(%rbp), %rdi
	movl	$4, %edx
	call	bn_copy_words@PLT
	testl	%eax, %eax
	je	.L255
	movq	24(%r14), %rsi
	movq	-192(%rbp), %rdi
	movl	$4, %edx
	call	bn_copy_words@PLT
	testl	%eax, %eax
	je	.L255
	movl	-172(%rbp), %edx
	movq	-184(%rbp), %rsi
	movq	%r15, %rdi
	movl	$7, %r13d
	call	ecp_nistz256_scatter_w7@PLT
	.p2align 4,,10
	.p2align 3
.L256:
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	EC_POINT_dbl@PLT
	testl	%eax, %eax
	je	.L297
	subl	$1, %r13d
	jne	.L256
	addq	$4096, %r15
	cmpq	-200(%rbp), %r15
	jne	.L257
	movq	-144(%rbp), %rdx
	movq	-216(%rbp), %rcx
	movq	%r12, %r8
	movq	%rbx, %rdi
	movq	%rdx, %rsi
	call	EC_POINT_add@PLT
	testl	%eax, %eax
	je	.L297
	addl	$1, -172(%rbp)
	movl	-172(%rbp), %eax
	cmpl	$64, %eax
	jne	.L259
	movq	-136(%rbp), %rax
	movq	%r12, %rdi
	movl	$1, %r13d
	movq	-208(%rbp), %xmm0
	movhps	-152(%rbp), %xmm0
	movq	%rbx, (%rax)
	movq	$7, 8(%rax)
	movups	%xmm0, 16(%rax)
	movq	%rax, 160(%rbx)
	movl	$4, 152(%rbx)
	call	BN_CTX_end@PLT
	movq	-160(%rbp), %rdi
	call	BN_CTX_free@PLT
	movq	$0, -152(%rbp)
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L305:
	movl	$859, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$114, %edx
	xorl	%r14d, %r14d
	movl	$243, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	movq	$0, -152(%rbp)
	movq	$0, -144(%rbp)
.L249:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	movq	-160(%rbp), %rdi
	call	BN_CTX_free@PLT
	movq	-168(%rbp), %rcx
	movl	$-1, %eax
	lock xaddl	%eax, (%rcx)
	subl	$1, %eax
	testl	%eax, %eax
	je	.L307
	jle	.L261
.L260:
	movq	-152(%rbp), %rdi
	movl	$926, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r14, %rdi
	call	EC_POINT_free@PLT
	movq	-144(%rbp), %rdi
	call	EC_POINT_free@PLT
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L299:
	movl	$831, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$113, %edx
	xorl	%r13d, %r13d
	movl	$243, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L304:
	call	BN_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L267
	movq	%rax, -160(%rbp)
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L268:
	movq	$0, -152(%rbp)
	movq	$0, -144(%rbp)
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L307:
.L261:
	movq	-136(%rbp), %rbx
	movl	$1470, %edx
	leaq	.LC0(%rip), %rsi
	movq	24(%rbx), %rdi
	call	CRYPTO_free@PLT
	movq	40(%rbx), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	movl	$1472, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rbx, %rdi
	call	CRYPTO_free@PLT
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L297:
	movl	-176(%rbp), %r13d
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L302:
	movl	$1432, %r8d
	movl	$65, %edx
	movl	$244, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L306:
	movl	$867, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r14d, %r14d
	movl	$243, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	movq	$0, -144(%rbp)
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L267:
	movq	$0, -152(%rbp)
	xorl	%r14d, %r14d
	movq	%rax, -160(%rbp)
	movq	$0, -144(%rbp)
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L303:
	movl	$1442, %r8d
	movl	$65, %edx
	movl	$244, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-136(%rbp), %rdi
	movl	$1443, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L243
.L255:
	movl	$898, %r8d
	movl	$146, %edx
	movl	$243, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	movl	-176(%rbp), %r13d
	call	ERR_put_error@PLT
	jmp	.L249
.L301:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE435:
	.size	ecp_nistz256_mult_precompute, .-ecp_nistz256_mult_precompute
	.p2align 4
	.globl	EC_nistz256_pre_comp_dup
	.type	EC_nistz256_pre_comp_dup, @function
EC_nistz256_pre_comp_dup:
.LFB440:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	testq	%rdi, %rdi
	je	.L309
	lock addl	$1, 32(%rdi)
.L309:
	ret
	.cfi_endproc
.LFE440:
	.size	EC_nistz256_pre_comp_dup, .-EC_nistz256_pre_comp_dup
	.p2align 4
	.globl	EC_nistz256_pre_comp_free
	.type	EC_nistz256_pre_comp_free, @function
EC_nistz256_pre_comp_free:
.LFB441:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L320
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	lock xaddl	%eax, 32(%rdi)
	subl	$1, %eax
	testl	%eax, %eax
	je	.L323
	jle	.L317
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L323:
	.cfi_restore_state
.L317:
	movq	24(%r12), %rdi
	movl	$1470, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	40(%r12), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$1472, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L320:
	ret
	.cfi_endproc
.LFE441:
	.size	EC_nistz256_pre_comp_free, .-EC_nistz256_pre_comp_free
	.p2align 4
	.globl	EC_GFp_nistz256_method
	.type	EC_GFp_nistz256_method, @function
EC_GFp_nistz256_method:
.LFB444:
	.cfi_startproc
	endbr64
	leaq	ret.10576(%rip), %rax
	ret
	.cfi_endproc
.LFE444:
	.size	EC_GFp_nistz256_method, .-EC_GFp_nistz256_method
	.section	.rodata
	.align 32
	.type	one.10543, @object
	.size	one.10543, 32
one.10543:
	.quad	1
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	chain.10569, @object
	.size	chain.10569, 54
chain.10569:
	.byte	32
	.byte	13
	.byte	6
	.byte	9
	.byte	5
	.byte	4
	.byte	4
	.byte	2
	.byte	5
	.byte	6
	.byte	5
	.byte	7
	.byte	4
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	5
	.byte	4
	.byte	9
	.byte	9
	.byte	6
	.byte	6
	.byte	2
	.byte	0
	.byte	5
	.byte	0
	.byte	6
	.byte	6
	.byte	5
	.byte	4
	.byte	4
	.byte	4
	.byte	5
	.byte	4
	.byte	5
	.byte	3
	.byte	3
	.byte	2
	.byte	10
	.byte	9
	.byte	2
	.byte	2
	.byte	5
	.byte	2
	.byte	5
	.byte	2
	.byte	3
	.byte	0
	.byte	7
	.byte	7
	.byte	6
	.byte	6
	.align 32
	.type	RR.10542, @object
	.size	RR.10542, 32
RR.10542:
	.quad	-8996981949549908318
	.quad	5087230966250696614
	.quad	2901921493521525849
	.quad	7413256579398063648
	.section	.data.rel.ro,"aw"
	.align 32
	.type	ret.10576, @object
	.size	ret.10576, 432
ret.10576:
	.long	1
	.long	406
	.quad	ec_GFp_mont_group_init
	.quad	ec_GFp_mont_group_finish
	.quad	ec_GFp_mont_group_clear_finish
	.quad	ec_GFp_mont_group_copy
	.quad	ec_GFp_mont_group_set_curve
	.quad	ec_GFp_simple_group_get_curve
	.quad	ec_GFp_simple_group_get_degree
	.quad	ec_group_simple_order_bits
	.quad	ec_GFp_simple_group_check_discriminant
	.quad	ec_GFp_simple_point_init
	.quad	ec_GFp_simple_point_finish
	.quad	ec_GFp_simple_point_clear_finish
	.quad	ec_GFp_simple_point_copy
	.quad	ec_GFp_simple_point_set_to_infinity
	.quad	ec_GFp_simple_set_Jprojective_coordinates_GFp
	.quad	ec_GFp_simple_get_Jprojective_coordinates_GFp
	.quad	ec_GFp_simple_point_set_affine_coordinates
	.quad	ecp_nistz256_get_affine
	.quad	0
	.quad	0
	.quad	0
	.quad	ec_GFp_simple_add
	.quad	ec_GFp_simple_dbl
	.quad	ec_GFp_simple_invert
	.quad	ec_GFp_simple_is_at_infinity
	.quad	ec_GFp_simple_is_on_curve
	.quad	ec_GFp_simple_cmp
	.quad	ec_GFp_simple_make_affine
	.quad	ec_GFp_simple_points_make_affine
	.quad	ecp_nistz256_points_mul
	.quad	ecp_nistz256_mult_precompute
	.quad	ecp_nistz256_window_have_precompute_mult
	.quad	ec_GFp_mont_field_mul
	.quad	ec_GFp_mont_field_sqr
	.quad	0
	.quad	ec_GFp_mont_field_inv
	.quad	ec_GFp_mont_field_encode
	.quad	ec_GFp_mont_field_decode
	.quad	ec_GFp_mont_field_set_to_one
	.quad	ec_key_simple_priv2oct
	.quad	ec_key_simple_oct2priv
	.quad	0
	.quad	ec_key_simple_generate_key
	.quad	ec_key_simple_check_key
	.quad	ec_key_simple_generate_public_key
	.quad	0
	.quad	0
	.quad	ecdh_simple_compute_key
	.quad	ecp_nistz256_inv_mod_ord
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata
	.align 32
	.type	ONE, @object
	.size	ONE, 32
ONE:
	.quad	1
	.quad	-4294967296
	.quad	-1
	.quad	4294967294
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	1
	.long	1
	.long	1
	.long	1
	.align 16
.LC2:
	.quad	-1
	.quad	-1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
