	.file	"p12_decr.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/pkcs12/p12_decr.c"
	.text
	.p2align 4
	.globl	PKCS12_pbe_crypt
	.type	PKCS12_pbe_crypt, @function
PKCS12_pbe_crypt:
.LFB803:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$56, %rsp
	movq	%rsi, -80(%rbp)
	movq	16(%rbp), %r15
	movl	%edx, -72(%rbp)
	movq	%r9, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_CIPHER_CTX_new@PLT
	movl	-72(%rbp), %edx
	movq	-80(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L18
	movq	8(%r13), %rcx
	movl	24(%rbp), %r9d
	movq	%rax, %r8
	movq	0(%r13), %rdi
	call	EVP_PBE_CipherInit@PLT
	testl	%eax, %eax
	je	.L19
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_block_size@PLT
	movl	$45, %edx
	leaq	.LC0(%rip), %rsi
	leal	(%rax,%rbx), %edi
	movslq	%edi, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L20
	leaq	-60(%rbp), %rdx
	movl	%ebx, %r8d
	movq	%r14, %rcx
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	%rdx, -72(%rbp)
	call	EVP_CipherUpdate@PLT
	movq	-72(%rbp), %rdx
	testl	%eax, %eax
	je	.L21
	movslq	-60(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rsi, %rbx
	addq	%r13, %rsi
	call	EVP_CipherFinal_ex@PLT
	testl	%eax, %eax
	je	.L22
	testq	%r15, %r15
	je	.L8
	addl	-60(%rbp), %ebx
	movl	%ebx, (%r15)
.L8:
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	je	.L3
	movq	%r13, (%rax)
.L3:
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L23
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movl	$40, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$115, %edx
	xorl	%r13d, %r13d
	movl	$119, %esi
	movl	$35, %edi
	call	ERR_put_error@PLT
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L21:
	movq	%r13, %rdi
	movl	$52, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%r13d, %r13d
	call	CRYPTO_free@PLT
	movl	$54, %r8d
	movl	$6, %edx
	leaq	.LC0(%rip), %rcx
	movl	$119, %esi
	movl	$35, %edi
	call	ERR_put_error@PLT
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$33, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$119, %esi
	movl	$35, %edi
	call	ERR_put_error@PLT
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$47, %r8d
	movl	$65, %edx
	movl	$119, %esi
	movl	$35, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L22:
	movq	%r13, %rdi
	movl	$60, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%r13d, %r13d
	call	CRYPTO_free@PLT
	movl	$62, %r8d
	movl	$116, %edx
	leaq	.LC0(%rip), %rcx
	movl	$119, %esi
	movl	$35, %edi
	call	ERR_put_error@PLT
	jmp	.L3
.L23:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE803:
	.size	PKCS12_pbe_crypt, .-PKCS12_pbe_crypt
	.p2align 4
	.globl	PKCS12_item_decrypt_d2i
	.type	PKCS12_item_decrypt_d2i, @function
PKCS12_item_decrypt_d2i:
.LFB804:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movq	%rdx, %rsi
	pushq	%r12
	movl	%ecx, %edx
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	%r9d, %ebx
	leaq	-56(%rbp), %r9
	subq	$40, %rsp
	movq	8(%r8), %rcx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	-60(%rbp), %rax
	pushq	$0
	movl	(%r8), %r8d
	pushq	%rax
	call	PKCS12_pbe_crypt
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L34
	movq	-56(%rbp), %rax
	movslq	-60(%rbp), %rdx
	leaq	-48(%rbp), %rsi
	movq	%r13, %rcx
	xorl	%edi, %edi
	movq	%rax, -48(%rbp)
	call	ASN1_item_d2i@PLT
	movq	%rax, %r12
	testl	%ebx, %ebx
	jne	.L35
	testq	%r12, %r12
	je	.L36
.L28:
	movq	-56(%rbp), %rdi
	movl	$115, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
.L24:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L37
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movslq	-60(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	OPENSSL_cleanse@PLT
	testq	%r12, %r12
	jne	.L28
.L36:
	movl	$114, %r8d
	movl	$101, %edx
	movl	$106, %esi
	movl	$35, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L34:
	movl	$93, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$117, %edx
	movq	%rax, %r12
	movl	$106, %esi
	movl	$35, %edi
	call	ERR_put_error@PLT
	jmp	.L24
.L37:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE804:
	.size	PKCS12_item_decrypt_d2i, .-PKCS12_item_decrypt_d2i
	.p2align 4
	.globl	PKCS12_item_i2d_encrypt
	.type	PKCS12_item_i2d_encrypt, @function
PKCS12_item_i2d_encrypt:
.LFB805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r9d, %ebx
	subq	$40, %rsp
	movq	%rsi, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	call	ASN1_OCTET_STRING_new@PLT
	movq	-72(%rbp), %rdi
	movq	-80(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L50
	leaq	-64(%rbp), %rsi
	movq	%r10, %rdx
	call	ASN1_item_i2d@PLT
	movq	-64(%rbp), %rcx
	movl	%eax, %r8d
	testq	%rcx, %rcx
	je	.L51
	pushq	$1
	movl	%r15d, %edx
	leaq	8(%r12), %r9
	movq	%r14, %rsi
	pushq	%r12
	movq	%r13, %rdi
	movl	%eax, -72(%rbp)
	call	PKCS12_pbe_crypt
	popq	%rdx
	movl	-72(%rbp), %r8d
	testq	%rax, %rax
	popq	%rcx
	je	.L52
	movq	-64(%rbp), %rdi
	testl	%ebx, %ebx
	jne	.L53
.L43:
	movl	$150, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
.L38:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L54
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movslq	%r8d, %rsi
	call	OPENSSL_cleanse@PLT
	movq	-64(%rbp), %rdi
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L50:
	movl	$134, %r8d
	movl	$65, %edx
	movl	$108, %esi
	movl	$35, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L40:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	ASN1_OCTET_STRING_free@PLT
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L51:
	movl	$139, %r8d
	movl	$102, %edx
	movl	$108, %esi
	movl	$35, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L52:
	movl	$144, %r8d
	movl	$103, %edx
	movl	$108, %esi
	movl	$35, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-64(%rbp), %rdi
	movl	$145, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L40
.L54:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE805:
	.size	PKCS12_item_i2d_encrypt, .-PKCS12_item_i2d_encrypt
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
