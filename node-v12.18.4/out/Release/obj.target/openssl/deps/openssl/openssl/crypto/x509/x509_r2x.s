	.file	"x509_r2x.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/x509/x509_r2x.c"
	.text
	.p2align 4
	.globl	X509_REQ_to_X509
	.type	X509_REQ_to_X509, @function
X509_REQ_to_X509:
.LFB781:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$8, %rsp
	call	X509_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L36
	movq	48(%r13), %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	je	.L8
	call	ASN1_INTEGER_new@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L5
	movl	$2, %esi
	call	ASN1_INTEGER_set@PLT
	testl	%eax, %eax
	je	.L5
.L8:
	movq	%r13, %rdi
	call	X509_REQ_get_subject_name@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	X509_set_subject_name@PLT
	testl	%eax, %eax
	je	.L5
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	X509_set_issuer_name@PLT
	testl	%eax, %eax
	je	.L5
	movq	56(%r12), %rdi
	xorl	%esi, %esi
	call	X509_gmtime_adj@PLT
	testq	%rax, %rax
	je	.L5
	movslq	%ebx, %rsi
	movq	64(%r12), %rdi
	imulq	$86400, %rsi, %rsi
	call	X509_gmtime_adj@PLT
	testq	%rax, %rax
	je	.L5
	movq	%r13, %rdi
	call	X509_REQ_get0_pubkey@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L5
	movq	%r12, %rdi
	call	X509_set_pubkey@PLT
	testl	%eax, %eax
	je	.L5
	call	EVP_md5@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	X509_sign@PLT
	testl	%eax, %eax
	jne	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	X509_free@PLT
.L1:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	movl	$28, %r8d
	movl	$65, %edx
	movl	$123, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1
	.cfi_endproc
.LFE781:
	.size	X509_REQ_to_X509, .-X509_REQ_to_X509
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
