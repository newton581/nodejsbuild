	.file	"d1_lib.c"
	.text
	.p2align 4
	.type	dtls1_handshake_write, @function
dtls1_handshake_write:
.LFB981:
	.cfi_startproc
	endbr64
	movl	$22, %esi
	jmp	dtls1_do_write@PLT
	.cfi_endproc
.LFE981:
	.size	dtls1_handshake_write, .-dtls1_handshake_write
	.p2align 4
	.globl	dtls1_default_timeout
	.type	dtls1_default_timeout, @function
dtls1_default_timeout:
.LFB964:
	.cfi_startproc
	endbr64
	movl	$7200, %eax
	ret
	.cfi_endproc
.LFE964:
	.size	dtls1_default_timeout, .-dtls1_default_timeout
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/ssl/d1_lib.c"
	.text
	.p2align 4
	.globl	dtls1_new
	.type	dtls1_new, @function
dtls1_new:
.LFB965:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$2112, %rdi
	call	DTLS_RECORD_LAYER_new@PLT
	testl	%eax, %eax
	jne	.L5
.L18:
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	movq	%r12, %rdi
	call	ssl3_new@PLT
	testl	%eax, %eax
	je	.L18
	movl	$77, %edx
	leaq	.LC0(%rip), %rsi
	movl	$536, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L17
	call	pqueue_new@PLT
	movq	%rax, 280(%r13)
	call	pqueue_new@PLT
	movl	56(%r12), %edx
	movq	%rax, 288(%r13)
	testl	%edx, %edx
	je	.L9
	movq	$256, 256(%r13)
.L9:
	pxor	%xmm0, %xmm0
	movq	280(%r13), %rdi
	movups	%xmm0, 296(%r13)
	testq	%rax, %rax
	je	.L12
	testq	%rdi, %rdi
	je	.L12
	movq	8(%r12), %rax
	movq	%r13, 176(%r12)
	movq	%r12, %rdi
	call	*24(%rax)
	popq	%r12
	popq	%r13
	testl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	setne	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	call	pqueue_free@PLT
	movq	288(%r13), %rdi
	call	pqueue_free@PLT
	movl	$95, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
.L17:
	movq	%r12, %rdi
	call	ssl3_free@PLT
	jmp	.L18
	.cfi_endproc
.LFE965:
	.size	dtls1_new, .-dtls1_new
	.p2align 4
	.globl	dtls1_clear_received_buffer
	.type	dtls1_clear_received_buffer, @function
dtls1_clear_received_buffer:
.LFB967:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L21:
	movq	8(%r12), %rdi
	call	dtls1_hm_fragment_free@PLT
	movq	%r12, %rdi
	call	pitem_free@PLT
.L20:
	movq	176(%rbx), %rax
	movq	280(%rax), %rdi
	call	pqueue_pop@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L21
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE967:
	.size	dtls1_clear_received_buffer, .-dtls1_clear_received_buffer
	.p2align 4
	.globl	dtls1_clear_sent_buffer
	.type	dtls1_clear_sent_buffer, @function
dtls1_clear_sent_buffer:
.LFB968:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L25:
	movq	8(%r12), %rdi
	call	dtls1_hm_fragment_free@PLT
	movq	%r12, %rdi
	call	pitem_free@PLT
.L24:
	movq	176(%rbx), %rax
	movq	288(%rax), %rdi
	call	pqueue_pop@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L25
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE968:
	.size	dtls1_clear_sent_buffer, .-dtls1_clear_sent_buffer
	.p2align 4
	.globl	dtls1_free
	.type	dtls1_free, @function
dtls1_free:
.LFB969:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$2112, %rdi
	call	DTLS_RECORD_LAYER_free@PLT
	movq	%rbx, %rdi
	call	ssl3_free@PLT
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L29:
	movq	8(%r12), %rdi
	call	dtls1_hm_fragment_free@PLT
	movq	%r12, %rdi
	call	pitem_free@PLT
.L28:
	movq	176(%rbx), %rax
	movq	280(%rax), %rdi
	call	pqueue_pop@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L29
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L31:
	movq	8(%r12), %rdi
	call	dtls1_hm_fragment_free@PLT
	movq	%r12, %rdi
	call	pitem_free@PLT
.L30:
	movq	176(%rbx), %rax
	movq	288(%rax), %rdi
	call	pqueue_pop@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L31
	movq	176(%rbx), %rax
	movq	280(%rax), %rdi
	call	pqueue_free@PLT
	movq	176(%rbx), %rax
	movq	288(%rax), %rdi
	call	pqueue_free@PLT
	movq	176(%rbx), %rdi
	movl	$150, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 176(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE969:
	.size	dtls1_free, .-dtls1_free
	.p2align 4
	.globl	dtls1_clear
	.type	dtls1_clear, @function
dtls1_clear:
.LFB970:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	addq	$2112, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	call	DTLS_RECORD_LAYER_clear@PLT
	movq	176(%r13), %rax
	testq	%rax, %rax
	je	.L34
	movq	280(%rax), %rbx
	movq	288(%rax), %rdx
	movq	304(%rax), %rsi
	movq	528(%rax), %r14
	movq	%rdx, -56(%rbp)
	movq	296(%rax), %r12
	movq	%rbx, %rdi
	movq	%rsi, -64(%rbp)
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L36:
	movq	8(%r15), %rdi
	call	dtls1_hm_fragment_free@PLT
	movq	%r15, %rdi
	call	pitem_free@PLT
	movq	176(%r13), %rax
	movq	280(%rax), %rdi
.L35:
	call	pqueue_pop@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L36
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L38:
	movq	8(%r15), %rdi
	call	dtls1_hm_fragment_free@PLT
	movq	%r15, %rdi
	call	pitem_free@PLT
.L37:
	movq	176(%r13), %rax
	movq	288(%rax), %rdi
	call	pqueue_pop@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L38
	movq	176(%r13), %rcx
	leaq	8(%rcx), %rdi
	movq	$0, (%rcx)
	movq	$0, 528(%rcx)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$536, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	176(%r13), %rax
	movl	56(%r13), %edx
	movq	%r14, 528(%rax)
	testl	%edx, %edx
	je	.L39
	movq	$256, 256(%rax)
.L39:
	movq	%r13, %rdi
	call	SSL_get_options@PLT
	testb	$16, %ah
	movq	176(%r13), %rax
	jne	.L40
.L41:
	movq	%rbx, %xmm0
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, 280(%rax)
.L34:
	movq	%r13, %rdi
	call	ssl3_clear@PLT
	testl	%eax, %eax
	je	.L33
	movq	8(%r13), %rax
	movl	(%rax), %eax
	cmpl	$131071, %eax
	je	.L52
	testb	$-128, 1493(%r13)
	je	.L44
	movl	$256, 0(%r13)
	movl	$1, %eax
	movl	$256, 1524(%r13)
.L33:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	movq	%r12, %xmm0
	movhps	-64(%rbp), %xmm0
	movups	%xmm0, 296(%rax)
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L52:
	movl	$65277, 0(%r13)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	movl	%eax, 0(%r13)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE970:
	.size	dtls1_clear, .-dtls1_clear
	.p2align 4
	.globl	dtls1_start_timer
	.type	dtls1_start_timer, @function
dtls1_start_timer:
.LFB972:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	176(%rdi), %rbx
	cmpq	$0, 504(%rbx)
	jne	.L54
	cmpq	$0, 512(%rbx)
	je	.L58
.L54:
	leaq	504(%rbx), %rdi
	xorl	%esi, %esi
	call	gettimeofday@PLT
	movq	176(%r12), %rbx
	movl	520(%rbx), %edx
	movq	%rdx, %rax
	imulq	$1125899907, %rdx, %rdx
	shrq	$50, %rdx
	movl	%edx, %ecx
	imull	$1000000, %edx, %edx
	addq	504(%rbx), %rcx
	movq	%rcx, 504(%rbx)
	subl	%edx, %eax
	addq	512(%rbx), %rax
	movq	%rax, 512(%rbx)
	cmpq	$999999, %rax
	jle	.L56
	addq	$1, %rcx
	subq	$1000000, %rax
	movq	%rcx, 504(%rbx)
	movq	%rax, 512(%rbx)
.L56:
	movq	%r12, %rdi
	call	SSL_get_rbio@PLT
	leaq	504(%rbx), %rcx
	xorl	%edx, %edx
	popq	%rbx
	movq	%rax, %rdi
	popq	%r12
	movl	$45, %esi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	movq	528(%rbx), %rax
	testq	%rax, %rax
	je	.L55
	xorl	%esi, %esi
	call	*%rax
	movl	%eax, 520(%rbx)
	movq	176(%r12), %rbx
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L55:
	movl	$1000000, 520(%rbx)
	jmp	.L54
	.cfi_endproc
.LFE972:
	.size	dtls1_start_timer, .-dtls1_start_timer
	.p2align 4
	.globl	dtls1_get_timeout
	.type	dtls1_get_timeout, @function
dtls1_get_timeout:
.LFB973:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	176(%rdi), %rax
	cmpq	$0, 504(%rax)
	jne	.L60
	cmpq	$0, 512(%rax)
	je	.L66
.L60:
	leaq	-48(%rbp), %rdi
	xorl	%esi, %esi
	call	gettimeofday@PLT
	movq	176(%rbx), %rax
	movq	-48(%rbp), %rcx
	cmpq	%rcx, 504(%rax)
	jl	.L62
	movq	-40(%rbp), %rsi
	jne	.L63
	cmpq	%rsi, 512(%rax)
	jle	.L62
.L63:
	movdqu	504(%rax), %xmm1
	movups	%xmm1, (%r12)
	movq	(%r12), %rdx
	movq	8(%r12), %rax
	subq	%rcx, %rdx
	subq	%rsi, %rax
	movq	%rdx, (%r12)
	movq	%rax, 8(%r12)
	js	.L68
	testq	%rdx, %rdx
	je	.L69
	.p2align 4,,10
	.p2align 3
.L61:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L70
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	cmpq	$14999, %rax
	jg	.L61
.L62:
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%r12)
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L68:
	subq	$1, %rdx
	addq	$1000000, %rax
	movq	%rdx, (%r12)
	movq	%rax, 8(%r12)
	testq	%rdx, %rdx
	jne	.L61
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L66:
	xorl	%r12d, %r12d
	jmp	.L61
.L70:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE973:
	.size	dtls1_get_timeout, .-dtls1_get_timeout
	.p2align 4
	.globl	dtls1_is_timer_expired
	.type	dtls1_is_timer_expired, @function
dtls1_is_timer_expired:
.LFB974:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	176(%rdi), %rax
	cmpq	$0, 504(%rax)
	jne	.L72
	cmpq	$0, 512(%rax)
	je	.L84
.L72:
	leaq	-48(%rbp), %rdi
	xorl	%esi, %esi
	call	gettimeofday@PLT
	movq	176(%rbx), %rax
	movq	-48(%rbp), %rcx
	cmpq	%rcx, 504(%rax)
	jl	.L76
	movq	-40(%rbp), %rsi
	jne	.L75
	cmpq	%rsi, 512(%rax)
	jle	.L76
.L75:
	movq	504(%rax), %rdx
	movq	512(%rax), %rax
	subq	%rcx, %rdx
	subq	%rsi, %rax
	js	.L87
	testq	%rdx, %rdx
	jne	.L78
.L89:
	cmpq	$14999, %rax
	jle	.L76
.L84:
	xorl	%r8d, %r8d
.L71:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L88
	addq	$40, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	movl	$0, %r8d
	jg	.L71
	testq	%rax, %rax
	jg	.L71
	.p2align 4,,10
	.p2align 3
.L76:
	movl	$1, %r8d
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L87:
	subq	$1, %rdx
	addq	$1000000, %rax
	testq	%rdx, %rdx
	jne	.L78
	jmp	.L89
.L88:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE974:
	.size	dtls1_is_timer_expired, .-dtls1_is_timer_expired
	.p2align 4
	.globl	dtls1_double_timeout
	.type	dtls1_double_timeout, @function
dtls1_double_timeout:
.LFB975:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$60000000, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	176(%rdi), %rbx
	movl	520(%rbx), %eax
	addl	%eax, %eax
	cmpl	$60000000, %eax
	cmova	%edx, %eax
	cmpq	$0, 504(%rbx)
	movl	%eax, 520(%rbx)
	jne	.L93
	cmpq	$0, 512(%rbx)
	je	.L97
.L93:
	leaq	504(%rbx), %rdi
	xorl	%esi, %esi
	call	gettimeofday@PLT
	movq	176(%r12), %rbx
	movl	520(%rbx), %edx
	movq	%rdx, %rax
	imulq	$1125899907, %rdx, %rdx
	shrq	$50, %rdx
	movl	%edx, %ecx
	imull	$1000000, %edx, %edx
	addq	504(%rbx), %rcx
	movq	%rcx, 504(%rbx)
	subl	%edx, %eax
	addq	512(%rbx), %rax
	movq	%rax, 512(%rbx)
	cmpq	$999999, %rax
	jle	.L95
	addq	$1, %rcx
	subq	$1000000, %rax
	movq	%rcx, 504(%rbx)
	movq	%rax, 512(%rbx)
.L95:
	movq	%r12, %rdi
	call	SSL_get_rbio@PLT
	leaq	504(%rbx), %rcx
	xorl	%edx, %edx
	popq	%rbx
	movq	%rax, %rdi
	popq	%r12
	movl	$45, %esi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L97:
	.cfi_restore_state
	movq	528(%rbx), %rax
	testq	%rax, %rax
	je	.L94
	xorl	%esi, %esi
	call	*%rax
	movl	%eax, 520(%rbx)
	movq	176(%r12), %rbx
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L94:
	movl	$1000000, 520(%rbx)
	jmp	.L93
	.cfi_endproc
.LFE975:
	.size	dtls1_double_timeout, .-dtls1_double_timeout
	.p2align 4
	.globl	dtls1_stop_timer
	.type	dtls1_stop_timer, @function
dtls1_stop_timer:
.LFB976:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	176(%rdi), %rax
	movq	%rdi, %rbx
	movq	$0, 488(%rax)
	movl	$0, 496(%rax)
	movq	176(%rdi), %rax
	movups	%xmm0, 504(%rax)
	movq	176(%rdi), %r12
	movl	$1000000, 520(%r12)
	call	SSL_get_rbio@PLT
	leaq	504(%r12), %rcx
	xorl	%edx, %edx
	movl	$45, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L100:
	movq	8(%r12), %rdi
	call	dtls1_hm_fragment_free@PLT
	movq	%r12, %rdi
	call	pitem_free@PLT
.L99:
	movq	176(%rbx), %rax
	movq	288(%rax), %rdi
	call	pqueue_pop@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L100
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE976:
	.size	dtls1_stop_timer, .-dtls1_stop_timer
	.p2align 4
	.globl	dtls1_check_timeout_num
	.type	dtls1_check_timeout_num, @function
dtls1_check_timeout_num:
.LFB977:
	.cfi_startproc
	endbr64
	movq	176(%rdi), %rdx
	xorl	%r8d, %r8d
	movl	496(%rdx), %eax
	addl	$1, %eax
	movl	%eax, 496(%rdx)
	cmpl	$2, %eax
	ja	.L114
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	SSL_get_options@PLT
	testb	$16, %ah
	je	.L104
	movq	176(%r12), %rax
	movl	496(%rax), %eax
.L105:
	cmpl	$12, %eax
	ja	.L115
	xorl	%r8d, %r8d
.L102:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	movq	%r12, %rdi
	call	SSL_get_wbio@PLT
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	$47, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	movq	176(%r12), %rdx
	cmpq	%rax, 304(%rdx)
	jbe	.L113
	movq	%rax, 304(%rdx)
.L113:
	movl	496(%rdx), %eax
	jmp	.L105
.L115:
	leaq	.LC0(%rip), %r8
	movl	$381, %r9d
	movl	$312, %ecx
	movq	%r12, %rdi
	movl	$318, %edx
	movl	$-1, %esi
	call	ossl_statem_fatal@PLT
	movl	$-1, %r8d
	jmp	.L102
	.cfi_endproc
.LFE977:
	.size	dtls1_check_timeout_num, .-dtls1_check_timeout_num
	.p2align 4
	.globl	dtls1_handle_timeout
	.type	dtls1_handle_timeout, @function
dtls1_handle_timeout:
.LFB978:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	176(%rdi), %rax
	cmpq	$0, 504(%rax)
	jne	.L117
	cmpq	$0, 512(%rax)
	je	.L131
.L117:
	leaq	-48(%rbp), %rdi
	xorl	%esi, %esi
	call	gettimeofday@PLT
	movq	176(%r12), %rbx
	movq	-48(%rbp), %rax
	cmpq	%rax, 504(%rbx)
	jl	.L121
	movq	-40(%rbp), %rcx
	jne	.L120
	cmpq	%rcx, 512(%rbx)
	jle	.L121
.L120:
	movq	504(%rbx), %rdx
	subq	%rax, %rdx
	movq	512(%rbx), %rax
	movq	%rdx, -64(%rbp)
	subq	%rcx, %rax
	movq	%rax, -56(%rbp)
	js	.L137
	testq	%rdx, %rdx
	jne	.L123
.L139:
	cmpq	$14999, %rax
	jle	.L121
.L131:
	xorl	%eax, %eax
.L116:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L138
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore_state
	jg	.L131
	testq	%rax, %rax
	jg	.L131
	.p2align 4,,10
	.p2align 3
.L121:
	movq	528(%rbx), %rax
	movl	520(%rbx), %esi
	testq	%rax, %rax
	je	.L125
	movq	%r12, %rdi
	call	*%rax
	movl	%eax, 520(%rbx)
.L126:
	movq	%r12, %rdi
	call	dtls1_check_timeout_num
	testl	%eax, %eax
	js	.L134
	movq	176(%r12), %rdx
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	488(%rdx), %eax
	addl	$1, %eax
	cmpl	$2, %eax
	cmova	%ecx, %eax
	movl	%eax, 488(%rdx)
	call	dtls1_start_timer
	movq	%r12, %rdi
	call	dtls1_retransmit_buffered_messages@PLT
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L137:
	subq	$1, %rdx
	addq	$1000000, %rax
	movq	%rdx, -64(%rbp)
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jne	.L123
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L125:
	addl	%esi, %esi
	movl	$60000000, %eax
	movq	%r12, %rdi
	cmpl	$60000000, %esi
	cmova	%eax, %esi
	movl	%esi, 520(%rbx)
	call	dtls1_start_timer
	jmp	.L126
.L134:
	movl	$-1, %eax
	jmp	.L116
.L138:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE978:
	.size	dtls1_handle_timeout, .-dtls1_handle_timeout
	.p2align 4
	.globl	dtls1_ctrl
	.type	dtls1_ctrl, @function
dtls1_ctrl:
.LFB971:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$74, %esi
	je	.L141
	jle	.L153
	cmpl	$120, %esi
	je	.L146
	cmpl	$121, %esi
	jne	.L145
	movl	$256, %eax
.L140:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L153:
	.cfi_restore_state
	cmpl	$17, %esi
	je	.L143
	cmpl	$73, %esi
	jne	.L145
	movq	%rcx, %rsi
	call	dtls1_get_timeout
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	cltq
.L154:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	.cfi_restore_state
	call	dtls1_handle_timeout
	cltq
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L146:
	xorl	%eax, %eax
	cmpq	$255, %rdx
	jle	.L140
	movq	176(%rdi), %rax
	movq	%rdx, 296(%rax)
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L145:
	.cfi_restore_state
	call	ssl3_ctrl@PLT
	cltq
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L143:
	xorl	%eax, %eax
	cmpq	$207, %rdx
	jle	.L140
	movq	176(%rdi), %rax
	movq	%rdx, 304(%rax)
	movq	%rdx, %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE971:
	.size	dtls1_ctrl, .-dtls1_ctrl
	.p2align 4
	.globl	DTLSv1_listen
	.type	DTLSv1_listen, @function
DTLSv1_listen:
.LFB980:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$424, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 48(%rdi)
	je	.L301
.L156:
	movq	%r12, %rdi
	call	SSL_clear@PLT
	testl	%eax, %eax
	jne	.L157
.L193:
	movl	$-1, %eax
.L155:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L302
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L157:
	.cfi_restore_state
	call	ERR_clear_error@PLT
	movq	%r12, %rdi
	call	SSL_get_rbio@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	SSL_get_wbio@PLT
	movq	%rax, %r15
	testq	%r14, %r14
	je	.L212
	testq	%rax, %rax
	je	.L212
	movl	(%r12), %eax
	andl	$65280, %eax
	cmpl	$65024, %eax
	jne	.L303
	movq	%r12, %rdi
	call	ssl3_setup_buffers@PLT
	testl	%eax, %eax
	je	.L193
	movq	2184(%r12), %rax
	movq	2144(%r12), %rbx
	movq	%rax, -424(%rbp)
	movq	$-5, %rax
	subq	%rbx, %rax
	andl	$7, %eax
	addq	%rax, %rbx
	movq	%rax, -432(%rbp)
	call	__errno_location@PLT
	movq	%r15, -416(%rbp)
	movq	%rax, -408(%rbp)
	leaq	13(%rbx), %rax
	movq	%rax, -440(%rbp)
	movq	%r13, -448(%rbp)
	movq	%r12, %r13
	movq	%rbx, %r12
.L205:
	movq	-408(%rbp), %rax
	movl	$16397, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	$0, (%rax)
	call	BIO_read@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	jle	.L304
	movslq	%eax, %r15
	cmpl	$12, %eax
	jle	.L305
	movq	184(%r13), %rax
	testq	%rax, %rax
	je	.L166
	subq	$8, %rsp
	movq	%r13, %r9
	pushq	192(%r13)
	xorl	%esi, %esi
	movl	$13, %r8d
	movq	%r12, %rcx
	movl	$256, %edx
	xorl	%edi, %edi
	call	*%rax
	popq	%r10
	popq	%r11
.L166:
	cmpl	$1, %ebx
	je	.L167
	cmpb	$22, (%r12)
	movzbl	1(%r12), %eax
	jne	.L306
	cmpb	$-2, %al
	jne	.L307
	movq	3(%r12), %rax
	leaq	-11(%r15), %rdx
	movq	%rax, -328(%rbp)
	cmpq	$1, %rdx
	jbe	.L171
	movzwl	11(%r12), %edx
	leaq	-13(%r15), %r11
	rolw	$8, %dx
	movzwl	%dx, %ebx
	cmpq	%rbx, %r11
	jb	.L171
	movzbl	%ah, %ecx
	orb	%al, %cl
	jne	.L308
	cmpw	$11, %dx
	jbe	.L174
	movzbl	22(%r12), %eax
	movzbl	23(%r12), %edx
	salq	$16, %rax
	salq	$8, %rdx
	orq	%rdx, %rax
	movzbl	24(%r12), %edx
	orq	%rdx, %rax
	leaq	-12(%rbx), %rdx
	cmpq	%rdx, %rax
	jne	.L174
	cmpb	$1, 13(%r12)
	jne	.L309
	movzwl	17(%r12), %edx
	rolw	$8, %dx
	cmpw	$2, %dx
	ja	.L310
	movzbl	19(%r12), %edx
	movzbl	20(%r12), %ecx
	salq	$16, %rdx
	salq	$8, %rcx
	orq	%rcx, %rdx
	movzbl	21(%r12), %ecx
	orq	%rcx, %rdx
	jne	.L213
	movzbl	14(%r12), %edx
	movzbl	15(%r12), %ecx
	salq	$16, %rdx
	salq	$8, %rcx
	orq	%rcx, %rdx
	movzbl	16(%r12), %ecx
	orq	%rcx, %rdx
	cmpq	%rax, %rdx
	jb	.L213
	movq	184(%r13), %r10
	testq	%r10, %r10
	je	.L180
	leaq	12(%rax), %r8
	movq	%r13, %r9
	movq	%rax, -456(%rbp)
	movq	-440(%rbp), %rcx
	pushq	%rdi
	movl	$22, %edx
	movl	0(%r13), %esi
	xorl	%edi, %edi
	pushq	192(%r13)
	call	*%r10
	movq	-456(%rbp), %rax
	popq	%r8
	popq	%r9
.L180:
	cmpq	$1, %rax
	jbe	.L181
	movzwl	25(%r12), %ecx
	movq	8(%r13), %rdx
	rolw	$8, %cx
	movl	(%rdx), %edx
	movzwl	%cx, %esi
	cmpw	$256, %cx
	je	.L311
	cmpl	$256, %edx
	movl	$65280, %ecx
	cmovne	%edx, %ecx
.L184:
	cmpl	%esi, %ecx
	jnb	.L185
	cmpl	$131071, %edx
	jne	.L312
.L185:
	leaq	-2(%rax), %rdx
	cmpq	$32, %rdx
	jbe	.L186
	movzbl	59(%r12), %edx
	subq	$35, %rax
	cmpq	%rdx, %rax
	jb	.L186
	subq	%rdx, %rax
	je	.L186
	leaq	60(%r12,%rdx), %rdx
	subq	$1, %rax
	movzbl	(%rdx), %esi
	movq	%rsi, %rcx
	cmpq	%rsi, %rax
	jb	.L186
	testq	%rsi, %rsi
	je	.L191
	movq	1440(%r13), %rax
	movq	208(%rax), %rax
	testq	%rax, %rax
	je	.L313
	leaq	1(%rdx), %rsi
	movq	%r13, %rdi
	movl	%ecx, %edx
	call	*%rax
	testl	%eax, %eax
	je	.L191
	movq	%r13, %r12
	leaq	-328(%rbp), %r15
	movq	-448(%rbp), %r13
	movq	%rbx, -408(%rbp)
	movq	176(%r12), %rax
	leaq	2112(%r12), %rdi
	movq	%r15, %rsi
	movl	$65537, 268(%rax)
	movw	$1, 272(%rax)
	call	DTLS_RECORD_LAYER_set_write_sequence@PLT
	movl	$8192, %esi
	movq	%r12, %rdi
	call	SSL_set_options@PLT
	movq	%r12, %rdi
	call	ossl_statem_set_hello_verify_done@PLT
	xorl	%edx, %edx
	movq	%r13, %rcx
	movl	$46, %esi
	movq	%r14, %rdi
	call	BIO_ctrl@PLT
	movq	-408(%rbp), %r10
	testl	%eax, %eax
	jle	.L314
.L206:
	movq	-432(%rbp), %rcx
	movq	%r15, %rdx
	movq	%r10, %rsi
	movq	%r12, %rdi
	call	dtls_buffer_listen_record@PLT
	testl	%eax, %eax
	je	.L193
	movl	$1, %eax
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L303:
	movl	$486, %r8d
	movl	$259, %edx
	movl	$350, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L171:
	movl	$573, %r8d
.L298:
	movl	$159, %edx
	movl	$350, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L164:
	xorl	%edi, %edi
	movl	%eax, -408(%rbp)
	call	BIO_ADDR_free@PLT
	movl	-408(%rbp), %eax
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L301:
	call	SSL_set_accept_state@PLT
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L212:
	movl	$474, %r8d
	movl	$128, %edx
	movl	$350, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L167:
	movl	$551, %r8d
	jmp	.L298
.L174:
	movl	$599, %r8d
	jmp	.L298
.L304:
	movl	$8, %esi
	movq	%r14, %rdi
	call	BIO_test_flags@PLT
	testl	%eax, %eax
	je	.L193
	xorl	%eax, %eax
	jmp	.L164
.L305:
	movl	$540, %r8d
	movl	$298, %edx
	movl	$350, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L164
.L307:
	movl	$565, %r8d
	movl	$116, %edx
	movl	$350, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L164
.L306:
	movl	$556, %r8d
.L299:
	movl	$244, %edx
	movl	$350, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L164
.L186:
	movl	$653, %r8d
	jmp	.L298
.L308:
	movl	$584, %r8d
	jmp	.L299
.L302:
	call	__stack_chk_fail@PLT
.L181:
	movl	$633, %r8d
	jmp	.L298
.L311:
	cmpl	$256, %edx
	je	.L185
	movl	%edx, %ecx
	movl	$65280, %esi
	jmp	.L184
.L191:
	movq	1440(%r13), %rax
	movq	200(%rax), %rax
	testq	%rax, %rax
	je	.L188
	leaq	-320(%rbp), %rbx
	leaq	-396(%rbp), %rdx
	movq	%r13, %rdi
	movq	%rbx, -456(%rbp)
	movq	%rbx, %rsi
	call	*%rax
	testl	%eax, %eax
	je	.L188
	cmpl	$255, -396(%rbp)
	ja	.L188
	movq	8(%r13), %rax
	cmpl	$131071, (%rax)
	je	.L210
	movl	0(%r13), %eax
	movl	%eax, %r15d
.L194:
	movq	%r13, %rdi
	leaq	-384(%rbp), %rbx
	call	ssl_get_max_send_fragment@PLT
	movq	-424(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	leal	13(%rax), %edx
	call	WPACKET_init_static_len@PLT
	testl	%eax, %eax
	je	.L196
	movl	$1, %edx
	movl	$22, %esi
	movq	%rbx, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L196
	movl	$2, %edx
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L196
	leaq	-328(%rbp), %rsi
	movl	$8, %edx
	movq	%rbx, %rdi
	call	WPACKET_memcpy@PLT
	testl	%eax, %eax
	je	.L196
	movl	$2, %esi
	movq	%rbx, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L196
	movl	$1, %edx
	movl	$3, %esi
	movq	%rbx, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L196
	xorl	%esi, %esi
	movl	$3, %edx
	movq	%rbx, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L196
	xorl	%esi, %esi
	movl	$2, %edx
	movq	%rbx, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L196
	xorl	%esi, %esi
	movl	$3, %edx
	movq	%rbx, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L196
	movl	$3, %esi
	movq	%rbx, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L196
	movl	-396(%rbp), %edx
	movq	-456(%rbp), %rsi
	movq	%rbx, %rdi
	call	dtls_raw_hello_verify_request@PLT
	testl	%eax, %eax
	je	.L196
	movq	%rbx, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L196
	movq	%rbx, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L196
	leaq	-392(%rbp), %rsi
	movq	%rbx, %rdi
	call	WPACKET_get_total_written@PLT
	testl	%eax, %eax
	je	.L196
	movq	%rbx, %rdi
	call	WPACKET_finish@PLT
	testl	%eax, %eax
	je	.L196
	movq	-424(%rbp), %rdx
	movzwl	22(%rdx), %eax
	movw	%ax, 14(%rdx)
	movzbl	24(%rdx), %eax
	movb	%al, 16(%rdx)
	movq	184(%r13), %rax
	testq	%rax, %rax
	je	.L197
	movq	%r12, %rcx
	xorl	%esi, %esi
	movq	%r13, %r9
	pushq	%rdx
	movl	$13, %r8d
	pushq	192(%r13)
	movl	$256, %edx
	movl	$1, %edi
	call	*%rax
	popq	%rcx
	popq	%rsi
.L197:
	call	BIO_ADDR_new@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L315
	xorl	%edx, %edx
	movq	%rax, %rcx
	movl	$46, %esi
	movq	%r14, %rdi
	call	BIO_ctrl@PLT
	testl	%eax, %eax
	jle	.L200
	movq	-416(%rbp), %rdi
	movq	%rbx, %rcx
	xorl	%edx, %edx
	movl	$44, %esi
	call	BIO_ctrl@PLT
.L200:
	movq	%rbx, %rdi
	call	BIO_ADDR_free@PLT
	movl	-392(%rbp), %edx
	movq	-424(%rbp), %rsi
	movq	-416(%rbp), %rdi
	call	BIO_write@PLT
	cmpl	-392(%rbp), %eax
	jl	.L297
	movq	-416(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	testl	%eax, %eax
	jg	.L205
.L297:
	movq	-416(%rbp), %r15
	movl	$8, %esi
	movq	%r15, %rdi
	call	BIO_test_flags@PLT
	testl	%eax, %eax
	je	.L193
	xorl	%eax, %eax
	jmp	.L164
.L315:
	movl	$785, %r8d
	movl	$65, %edx
	movl	$350, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L164
.L196:
	movl	$763, %r8d
	movl	$68, %edx
	movl	$350, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	movq	%rbx, -408(%rbp)
	call	ERR_put_error@PLT
	movq	-408(%rbp), %r9
	movq	%r9, %rdi
	call	WPACKET_cleanup@PLT
	jmp	.L193
.L210:
	movl	$65279, %r15d
	jmp	.L194
.L188:
	movl	$700, %r8d
	movl	$400, %edx
	movl	$350, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L193
.L314:
	movq	%r13, %rdi
	call	BIO_ADDR_clear@PLT
	movq	-408(%rbp), %r10
	jmp	.L206
.L313:
	movl	$668, %r8d
	movl	$403, %edx
	movl	$350, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	orl	$-1, %eax
	jmp	.L155
.L310:
	movl	$610, %r8d
	movl	$402, %edx
	movl	$350, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L164
.L309:
	movl	$604, %r8d
	jmp	.L299
.L213:
	movl	$623, %r8d
	movl	$401, %edx
	movl	$350, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L164
.L312:
	movl	$642, %r8d
	movl	$267, %edx
	movl	$350, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L164
	.cfi_endproc
.LFE980:
	.size	DTLSv1_listen, .-DTLSv1_listen
	.p2align 4
	.globl	dtls1_shutdown
	.type	dtls1_shutdown, @function
dtls1_shutdown:
.LFB982:
	.cfi_startproc
	endbr64
	jmp	ssl3_shutdown@PLT
	.cfi_endproc
.LFE982:
	.size	dtls1_shutdown, .-dtls1_shutdown
	.p2align 4
	.globl	dtls1_query_mtu
	.type	dtls1_query_mtu, @function
dtls1_query_mtu:
.LFB983:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	176(%rdi), %rax
	movq	296(%rax), %rbx
	testq	%rbx, %rbx
	jne	.L318
	movq	304(%rax), %rbx
.L319:
	movq	%r12, %rdi
	movl	$256, %r13d
	call	SSL_get_wbio@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$49, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	movq	%r13, %rcx
	movl	%eax, %eax
	subq	%rax, %rcx
	cmpq	%rbx, %rcx
	ja	.L320
.L325:
	movl	$1, %eax
.L317:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L318:
	.cfi_restore_state
	call	SSL_get_wbio@PLT
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	$49, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	movq	176(%r12), %rdx
	movl	%eax, %eax
	movq	$0, 296(%rdx)
	subq	%rax, %rbx
	movq	%rbx, 304(%rdx)
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L320:
	movq	%r12, %rdi
	call	SSL_get_options@PLT
	movq	%rax, %r8
	xorl	%eax, %eax
	testl	$4096, %r8d
	jne	.L317
	movq	%r12, %rdi
	call	SSL_get_wbio@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$40, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	movq	176(%r12), %rax
	movq	%rbx, 304(%rax)
	call	SSL_get_wbio@PLT
	movl	$49, %esi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	movq	%r13, %rsi
	movl	%eax, %eax
	subq	%rax, %rsi
	cmpq	%rsi, %rbx
	jnb	.L325
	movq	%r12, %rdi
	movq	176(%r12), %rbx
	call	SSL_get_wbio@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$49, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	movq	%r12, %rdi
	movl	%eax, %eax
	subq	%rax, %r13
	movq	176(%r12), %rax
	movq	%r13, 304(%rbx)
	movq	304(%rax), %r13
	call	SSL_get_wbio@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$42, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	jmp	.L325
	.cfi_endproc
.LFE983:
	.size	dtls1_query_mtu, .-dtls1_query_mtu
	.p2align 4
	.globl	dtls1_min_mtu
	.type	dtls1_min_mtu, @function
dtls1_min_mtu:
.LFB985:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	SSL_get_wbio@PLT
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	$49, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%eax, %edx
	movl	$256, %eax
	subq	%rdx, %rax
	ret
	.cfi_endproc
.LFE985:
	.size	dtls1_min_mtu, .-dtls1_min_mtu
	.p2align 4
	.globl	DTLS_get_data_mtu
	.type	DTLS_get_data_mtu, @function
DTLS_get_data_mtu:
.LFB986:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	SSL_get_current_cipher@PLT
	testq	%rax, %rax
	je	.L331
	movq	%rax, %rdi
	movq	176(%r12), %rax
	leaq	-40(%rbp), %rcx
	leaq	-48(%rbp), %rdx
	leaq	-56(%rbp), %rsi
	leaq	-32(%rbp), %r8
	movq	304(%rax), %rbx
	call	ssl_cipher_get_overhead@PLT
	testl	%eax, %eax
	je	.L331
	movq	168(%r12), %rax
	movq	-32(%rbp), %rdx
	movq	-56(%rbp), %rcx
	movq	(%rax), %rax
	testb	$1, %ah
	je	.L332
	addq	%rcx, %rdx
	movq	%rdx, -32(%rbp)
.L333:
	leaq	13(%rdx), %rax
	cmpq	%rbx, %rax
	jnb	.L331
	leaq	-13(%rbx), %rax
	movq	-40(%rbp), %rcx
	subq	%rdx, %rax
	movq	%rax, %rbx
	testq	%rcx, %rcx
	je	.L334
	xorl	%edx, %edx
	divq	%rcx
	subq	%rdx, %rbx
.L334:
	movq	-48(%rbp), %rax
	cmpq	%rbx, %rax
	jnb	.L331
	subq	%rax, %rbx
	movq	%rbx, %rax
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L331:
	xorl	%eax, %eax
.L328:
	movq	-24(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L343
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L332:
	.cfi_restore_state
	addq	%rcx, -48(%rbp)
	jmp	.L333
.L343:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE986:
	.size	DTLS_get_data_mtu, .-DTLS_get_data_mtu
	.p2align 4
	.globl	DTLS_set_timer_cb
	.type	DTLS_set_timer_cb, @function
DTLS_set_timer_cb:
.LFB987:
	.cfi_startproc
	endbr64
	movq	176(%rdi), %rax
	movq	%rsi, 528(%rax)
	ret
	.cfi_endproc
.LFE987:
	.size	DTLS_set_timer_cb, .-DTLS_set_timer_cb
	.globl	DTLSv1_2_enc_data
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"client finished"
.LC2:
	.string	"server finished"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	DTLSv1_2_enc_data, @object
	.size	DTLSv1_2_enc_data, 128
DTLSv1_2_enc_data:
	.quad	tls1_enc
	.quad	tls1_mac
	.quad	tls1_setup_key_block
	.quad	tls1_generate_master_secret
	.quad	tls1_change_cipher_state
	.quad	tls1_final_finish_mac
	.quad	.LC1
	.quad	15
	.quad	.LC2
	.quad	15
	.quad	tls1_alert_code
	.quad	tls1_export_keying_material
	.long	31
	.zero	4
	.quad	dtls1_set_handshake_header
	.quad	dtls1_close_construct_packet
	.quad	dtls1_handshake_write
	.globl	DTLSv1_enc_data
	.align 32
	.type	DTLSv1_enc_data, @object
	.size	DTLSv1_enc_data, 128
DTLSv1_enc_data:
	.quad	tls1_enc
	.quad	tls1_mac
	.quad	tls1_setup_key_block
	.quad	tls1_generate_master_secret
	.quad	tls1_change_cipher_state
	.quad	tls1_final_finish_mac
	.quad	.LC1
	.quad	15
	.quad	.LC2
	.quad	15
	.quad	tls1_alert_code
	.quad	tls1_export_keying_material
	.long	9
	.zero	4
	.quad	dtls1_set_handshake_header
	.quad	dtls1_close_construct_packet
	.quad	dtls1_handshake_write
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
