	.file	"err.c"
	.text
	.p2align 4
	.type	err_string_data_hash, @function
err_string_data_hash:
.LFB298:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	%rax, %rdx
	movl	%eax, %ecx
	shrq	$12, %rdx
	shrl	$24, %ecx
	andl	$4095, %edx
	xorl	%edx, %ecx
	movabsq	$-2912643801112034465, %rdx
	movslq	%ecx, %rcx
	xorq	%rax, %rcx
	movq	%rcx, %rax
	movq	%rcx, %rsi
	mulq	%rdx
	shrq	$4, %rdx
	leaq	(%rdx,%rdx,8), %rax
	leaq	(%rdx,%rax,2), %rax
	subq	%rax, %rsi
	leaq	(%rsi,%rsi,2), %rdx
	leaq	(%rsi,%rdx,4), %rax
	xorq	%rcx, %rax
	ret
	.cfi_endproc
.LFE298:
	.size	err_string_data_hash, .-err_string_data_hash
	.p2align 4
	.type	err_string_data_cmp, @function
err_string_data_cmp:
.LFB299:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movq	(%rsi), %rcx
	xorl	%eax, %eax
	cmpq	%rcx, %rdx
	je	.L3
	cmpq	%rdx, %rcx
	sbbl	%eax, %eax
	andl	$2, %eax
	subl	$1, %eax
.L3:
	ret
	.cfi_endproc
.LFE299:
	.size	err_string_data_cmp, .-err_string_data_cmp
	.p2align 4
	.type	do_err_strings_init_ossl_, @function
do_err_strings_init_ossl_:
.LFB303:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	OPENSSL_init_crypto@PLT
	testl	%eax, %eax
	jne	.L8
.L16:
	xorl	%edx, %edx
.L9:
	movl	%edx, do_err_strings_init_ossl_ret_(%rip)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	call	CRYPTO_THREAD_lock_new@PLT
	movq	%rax, err_string_lock(%rip)
	testq	%rax, %rax
	je	.L16
	leaq	err_string_data_cmp(%rip), %rsi
	leaq	err_string_data_hash(%rip), %rdi
	call	OPENSSL_LH_new@PLT
	movl	$1, %edx
	movq	%rax, int_error_hash(%rip)
	testq	%rax, %rax
	jne	.L9
	movq	err_string_lock(%rip), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	movq	$0, err_string_lock(%rip)
	jmp	.L16
	.cfi_endproc
.LFE303:
	.size	do_err_strings_init_ossl_, .-do_err_strings_init_ossl_
	.p2align 4
	.type	err_do_init_ossl_, @function
err_do_init_ossl_:
.LFB333:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	leaq	err_thread_local(%rip), %rdi
	movl	$1, set_err_thread_local(%rip)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	CRYPTO_THREAD_init_local@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%eax, err_do_init_ossl_ret_(%rip)
	ret
	.cfi_endproc
.LFE333:
	.size	err_do_init_ossl_, .-err_do_init_ossl_
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unknown"
	.text
	.p2align 4
	.type	ERR_load_ERR_strings.part.0, @function
ERR_load_ERR_strings.part.0:
.LFB348:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	ERR_str_libraries(%rip), %rbx
	subq	$40, %rsp
	movq	err_string_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	cmpq	$0, ERR_str_libraries(%rip)
	je	.L23
	.p2align 4,,10
	.p2align 3
.L20:
	movq	int_error_hash(%rip), %rdi
	movq	%rbx, %rsi
	addq	$16, %rbx
	call	OPENSSL_LH_insert@PLT
	cmpq	$0, (%rbx)
	jne	.L20
.L23:
	movq	err_string_lock(%rip), %rdi
	leaq	ERR_str_reasons(%rip), %rbx
	call	CRYPTO_THREAD_unlock@PLT
	movq	err_string_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	cmpq	$0, ERR_str_reasons(%rip)
	je	.L22
	.p2align 4,,10
	.p2align 3
.L21:
	movq	int_error_hash(%rip), %rdi
	movq	%rbx, %rsi
	addq	$16, %rbx
	call	OPENSSL_LH_insert@PLT
	cmpq	$0, (%rbx)
	jne	.L21
.L22:
	movq	err_string_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movq	ERR_str_functs(%rip), %rax
	leaq	ERR_str_functs(%rip), %rdx
	testq	%rax, %rax
	je	.L25
	.p2align 4,,10
	.p2align 3
.L24:
	orq	$33554432, %rax
	addq	$16, %rdx
	movq	%rax, -16(%rdx)
	movq	(%rdx), %rax
	testq	%rax, %rax
	jne	.L24
.L25:
	movq	err_string_lock(%rip), %rdi
	leaq	ERR_str_functs(%rip), %rbx
	call	CRYPTO_THREAD_write_lock@PLT
	cmpq	$0, ERR_str_functs(%rip)
	je	.L27
	.p2align 4,,10
	.p2align 3
.L26:
	movq	int_error_hash(%rip), %rdi
	movq	%rbx, %rsi
	addq	$16, %rbx
	call	OPENSSL_LH_insert@PLT
	cmpq	$0, (%rbx)
	jne	.L26
.L27:
	movq	err_string_lock(%rip), %rdi
	leaq	8+SYS_str_reasons(%rip), %r13
	xorl	%ebx, %ebx
	movl	$1, %r15d
	leaq	strerror_pool.7259(%rip), %r14
	call	CRYPTO_THREAD_unlock@PLT
	call	__errno_location@PLT
	movq	err_string_lock(%rip), %rdi
	movq	%rax, -64(%rbp)
	movl	(%rax), %eax
	movl	%eax, -68(%rbp)
	call	CRYPTO_THREAD_write_lock@PLT
	movl	init.7262(%rip), %eax
	testl	%eax, %eax
	jne	.L37
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L70:
	testq	%rax, %rax
	je	.L68
.L36:
	addl	$1, %r15d
	addq	$16, %r13
	cmpl	$128, %r15d
	je	.L69
.L37:
	movl	%r15d, %eax
	orl	$33554432, %eax
	movq	%rax, -8(%r13)
	movq	0(%r13), %rax
	cmpq	$8191, %rbx
	jbe	.L70
.L31:
	testq	%rax, %rax
	jne	.L36
	leaq	.LC0(%rip), %rax
	movq	%rax, 0(%r13)
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L68:
	movl	$8192, %edx
	movq	%r14, %rsi
	movl	%r15d, %edi
	subq	%rbx, %rdx
	call	openssl_strerror_r@PLT
	testl	%eax, %eax
	jne	.L32
	movq	0(%r13), %rax
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L32:
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r14, 0(%r13)
	addq	%rax, %rbx
	leaq	(%r14,%rax), %r12
	movq	%rbx, %rax
	subq	%r12, %rax
	movq	%rax, -56(%rbp)
	leaq	strerror_pool.7259(%rip), %rax
	cmpq	%rax, %r12
	ja	.L33
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L35:
	leaq	-1(%r12), %rax
	leaq	strerror_pool.7259(%rip), %rcx
	cmpq	%rcx, %rax
	je	.L72
	movq	%rax, %r12
.L33:
	movq	-56(%rbp), %rax
	movsbl	-1(%r12), %edi
	movl	$8, %esi
	leaq	(%r12,%rax), %rbx
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	jne	.L35
	movq	0(%r13), %rax
	leaq	1(%r12), %r14
	addq	$1, %rbx
.L34:
	movb	$0, (%r12)
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L69:
	movq	err_string_lock(%rip), %rdi
	leaq	SYS_str_reasons(%rip), %rbx
	movl	$0, init.7262(%rip)
	call	CRYPTO_THREAD_unlock@PLT
	movq	-64(%rbp), %rax
	movl	-68(%rbp), %ecx
	movq	err_string_lock(%rip), %rdi
	movl	%ecx, (%rax)
	call	CRYPTO_THREAD_write_lock@PLT
	cmpq	$0, SYS_str_reasons(%rip)
	je	.L39
	.p2align 4,,10
	.p2align 3
.L38:
	movq	int_error_hash(%rip), %rdi
	movq	%rbx, %rsi
	addq	$16, %rbx
	call	OPENSSL_LH_insert@PLT
	cmpq	$0, (%rbx)
	jne	.L38
.L39:
	movq	err_string_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	addq	$40, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore_state
	movq	%r12, %r14
	movq	0(%r13), %rax
	movq	%rcx, %r12
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L71:
	movq	%r14, %rax
	addq	$1, %rbx
	leaq	1(%r12), %r14
	jmp	.L34
	.cfi_endproc
.LFE348:
	.size	ERR_load_ERR_strings.part.0, .-ERR_load_ERR_strings.part.0
	.p2align 4
	.globl	err_cleanup
	.type	err_cleanup, @function
err_cleanup:
.LFB305:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	set_err_thread_local(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%eax, %eax
	jne	.L76
.L74:
	movq	err_string_lock(%rip), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	movq	int_error_hash(%rip), %rdi
	movq	$0, err_string_lock(%rip)
	call	OPENSSL_LH_free@PLT
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	$0, int_error_hash(%rip)
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	leaq	err_thread_local(%rip), %rdi
	call	CRYPTO_THREAD_cleanup_local@PLT
	jmp	.L74
	.cfi_endproc
.LFE305:
	.size	err_cleanup, .-err_cleanup
	.p2align 4
	.globl	ERR_load_ERR_strings
	.type	ERR_load_ERR_strings, @function
ERR_load_ERR_strings:
.LFB308:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	do_err_strings_init_ossl_(%rip), %rsi
	leaq	err_string_init(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L77
	movl	do_err_strings_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	jne	.L86
.L77:
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	ERR_load_ERR_strings.part.0
	.cfi_endproc
.LFE308:
	.size	ERR_load_ERR_strings, .-ERR_load_ERR_strings
	.p2align 4
	.globl	ERR_load_strings
	.type	ERR_load_strings, @function
ERR_load_strings:
.LFB309:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%edi, %r12d
	leaq	err_string_init(%rip), %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	leaq	do_err_strings_init_ossl_(%rip), %rsi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L90
	movl	do_err_strings_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	jne	.L104
.L90:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	call	ERR_load_ERR_strings.part.0
	testl	%eax, %eax
	je	.L90
	movl	%r12d, %edi
	movq	(%rbx), %rax
	sall	$24, %edi
	movl	%edi, %ecx
	testq	%rax, %rax
	je	.L91
	movq	%rbx, %rdx
	.p2align 4,,10
	.p2align 3
.L92:
	orq	%rcx, %rax
	addq	$16, %rdx
	movq	%rax, -16(%rdx)
	movq	(%rdx), %rax
	testq	%rax, %rax
	jne	.L92
.L91:
	movq	err_string_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	cmpq	$0, (%rbx)
	je	.L93
	.p2align 4,,10
	.p2align 3
.L94:
	movq	int_error_hash(%rip), %rdi
	movq	%rbx, %rsi
	addq	$16, %rbx
	call	OPENSSL_LH_insert@PLT
	cmpq	$0, (%rbx)
	jne	.L94
.L93:
	movq	err_string_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE309:
	.size	ERR_load_strings, .-ERR_load_strings
	.p2align 4
	.globl	ERR_load_strings_const
	.type	ERR_load_strings_const, @function
ERR_load_strings_const:
.LFB310:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	do_err_strings_init_ossl_(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	err_string_init(%rip), %rdi
	subq	$8, %rsp
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L108
	movl	do_err_strings_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	jne	.L116
.L108:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
	call	ERR_load_ERR_strings.part.0
	testl	%eax, %eax
	je	.L108
	movq	err_string_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	cmpq	$0, (%rbx)
	je	.L109
	.p2align 4,,10
	.p2align 3
.L110:
	movq	int_error_hash(%rip), %rdi
	movq	%rbx, %rsi
	addq	$16, %rbx
	call	OPENSSL_LH_insert@PLT
	cmpq	$0, (%rbx)
	jne	.L110
.L109:
	movq	err_string_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE310:
	.size	ERR_load_strings_const, .-ERR_load_strings_const
	.p2align 4
	.globl	ERR_unload_strings
	.type	ERR_unload_strings, @function
ERR_unload_strings:
.LFB311:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	err_string_init(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	leaq	do_err_strings_init_ossl_(%rip), %rsi
	subq	$8, %rsp
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L117
	movl	do_err_strings_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	jne	.L129
.L117:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	movq	err_string_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	cmpq	$0, (%rbx)
	je	.L119
	.p2align 4,,10
	.p2align 3
.L120:
	movq	int_error_hash(%rip), %rdi
	movq	%rbx, %rsi
	addq	$16, %rbx
	call	OPENSSL_LH_delete@PLT
	cmpq	$0, (%rbx)
	jne	.L120
.L119:
	movq	err_string_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE311:
	.size	ERR_unload_strings, .-ERR_unload_strings
	.p2align 4
	.globl	err_free_strings_int
	.type	err_free_strings_int, @function
err_free_strings_int:
.LFB312:
	.cfi_startproc
	endbr64
	leaq	do_err_strings_init_ossl_(%rip), %rsi
	leaq	err_string_init(%rip), %rdi
	jmp	CRYPTO_THREAD_run_once@PLT
	.cfi_endproc
.LFE312:
	.size	err_free_strings_int, .-err_free_strings_int
	.p2align 4
	.globl	ERR_lib_error_string
	.type	ERR_lib_error_string, @function
ERR_lib_error_string:
.LFB327:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	do_err_strings_init_ossl_(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	err_string_init(%rip), %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L131
	movl	do_err_strings_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	je	.L131
	shrq	$24, %rbx
	movq	%rbx, %rdi
	sall	$24, %edi
	movl	%edi, %eax
	movq	err_string_lock(%rip), %rdi
	movq	%rax, -48(%rbp)
	call	CRYPTO_THREAD_read_lock@PLT
	movq	int_error_hash(%rip), %rdi
	leaq	-48(%rbp), %rsi
	call	OPENSSL_LH_retrieve@PLT
	movq	err_string_lock(%rip), %rdi
	movq	%rax, %r12
	call	CRYPTO_THREAD_unlock@PLT
	testq	%r12, %r12
	je	.L131
	movq	8(%r12), %r12
.L131:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L141
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L141:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE327:
	.size	ERR_lib_error_string, .-ERR_lib_error_string
	.p2align 4
	.globl	ERR_func_error_string
	.type	ERR_func_error_string, @function
ERR_func_error_string:
.LFB328:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	do_err_strings_init_ossl_(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	err_string_init(%rip), %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L142
	movl	do_err_strings_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	je	.L142
	movq	%rbx, %rax
	shrq	$24, %rbx
	movq	err_string_lock(%rip), %rdi
	shrq	$12, %rax
	sall	$24, %ebx
	sall	$12, %eax
	andl	$16773120, %eax
	orl	%eax, %ebx
	movl	%ebx, %eax
	movq	%rax, -48(%rbp)
	call	CRYPTO_THREAD_read_lock@PLT
	movq	int_error_hash(%rip), %rdi
	leaq	-48(%rbp), %rsi
	call	OPENSSL_LH_retrieve@PLT
	movq	err_string_lock(%rip), %rdi
	movq	%rax, %r12
	call	CRYPTO_THREAD_unlock@PLT
	testq	%r12, %r12
	je	.L142
	movq	8(%r12), %r12
.L142:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L152
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L152:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE328:
	.size	ERR_func_error_string, .-ERR_func_error_string
	.p2align 4
	.globl	ERR_reason_error_string
	.type	ERR_reason_error_string, @function
ERR_reason_error_string:
.LFB329:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	do_err_strings_init_ossl_(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	leaq	err_string_init(%rip), %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L153
	movl	do_err_strings_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	je	.L153
	movq	err_string_lock(%rip), %rdi
	movl	%ebx, %r13d
	andl	$-16773121, %ebx
	leaq	-64(%rbp), %r14
	movl	%ebx, %eax
	andl	$4095, %r13d
	movq	%rax, -64(%rbp)
	call	CRYPTO_THREAD_read_lock@PLT
	movq	int_error_hash(%rip), %rdi
	movq	%r14, %rsi
	call	OPENSSL_LH_retrieve@PLT
	movq	err_string_lock(%rip), %rdi
	movq	%rax, %r12
	call	CRYPTO_THREAD_unlock@PLT
	testq	%r12, %r12
	je	.L164
.L155:
	movq	8(%r12), %r12
.L153:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L165
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	movq	err_string_lock(%rip), %rdi
	movslq	%r13d, %r13
	movq	%r13, -64(%rbp)
	call	CRYPTO_THREAD_read_lock@PLT
	movq	int_error_hash(%rip), %rdi
	movq	%r14, %rsi
	call	OPENSSL_LH_retrieve@PLT
	movq	err_string_lock(%rip), %rdi
	movq	%rax, %r12
	call	CRYPTO_THREAD_unlock@PLT
	testq	%r12, %r12
	je	.L153
	jmp	.L155
.L165:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE329:
	.size	ERR_reason_error_string, .-ERR_reason_error_string
	.section	.rodata.str1.1
.LC1:
	.string	"lib(%lu)"
.LC2:
	.string	"func(%lu)"
.LC3:
	.string	"reason(%lu)"
.LC4:
	.string	"error:%08lX:%s:%s:%s"
.LC5:
	.string	"err:%lx:%lx:%lx:%lx"
	.text
	.p2align 4
	.type	ERR_error_string_n.part.0, @function
ERR_error_string_n.part.0:
.LFB352:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	leaq	do_err_strings_init_ossl_(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	shrq	$24, %rbx
	subq	$264, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%edi, %eax
	leaq	err_string_init(%rip), %rdi
	shrl	$24, %eax
	movq	%rax, -280(%rbp)
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L168
	movl	do_err_strings_init_ossl_ret_(%rip), %r8d
	testl	%r8d, %r8d
	je	.L168
	movq	err_string_lock(%rip), %rdi
	movl	%ebx, %eax
	sall	$24, %eax
	movq	%rax, -272(%rbp)
	call	CRYPTO_THREAD_read_lock@PLT
	movq	int_error_hash(%rip), %rdi
	leaq	-272(%rbp), %rsi
	call	OPENSSL_LH_retrieve@PLT
	movq	err_string_lock(%rip), %rdi
	movq	%rax, %r15
	call	CRYPTO_THREAD_unlock@PLT
	testq	%r15, %r15
	je	.L168
	movq	8(%r15), %r15
	testq	%r15, %r15
	je	.L168
.L171:
	leaq	do_err_strings_init_ossl_(%rip), %rsi
	leaq	err_string_init(%rip), %rdi
	call	CRYPTO_THREAD_run_once@PLT
	movl	%eax, %r8d
	movq	%r12, %rax
	shrq	$12, %rax
	movq	%rax, %rcx
	andl	$4095, %ecx
	movq	%rcx, -296(%rbp)
	testl	%r8d, %r8d
	je	.L173
	movl	do_err_strings_init_ossl_ret_(%rip), %edi
	testl	%edi, %edi
	je	.L173
	sall	$12, %eax
	sall	$24, %ebx
	movq	err_string_lock(%rip), %rdi
	andl	$16773120, %eax
	orl	%eax, %ebx
	movl	%ebx, %eax
	movq	%rax, -272(%rbp)
	call	CRYPTO_THREAD_read_lock@PLT
	movq	int_error_hash(%rip), %rdi
	leaq	-272(%rbp), %rsi
	call	OPENSSL_LH_retrieve@PLT
	movq	err_string_lock(%rip), %rdi
	movq	%rax, %rbx
	call	CRYPTO_THREAD_unlock@PLT
	testq	%rbx, %rbx
	je	.L173
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L173
.L176:
	movq	%r12, %rdi
	call	ERR_reason_error_string
	movq	%r12, %rdx
	andl	$4095, %edx
	movq	%rdx, -288(%rbp)
	testq	%rax, %rax
	je	.L187
.L177:
	subq	$8, %rsp
	movq	%r15, %r8
	movq	%r12, %rcx
	movq	%r14, %rsi
	pushq	%rax
	movq	%rbx, %r9
	leaq	.LC4(%rip), %rdx
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	movq	%r13, %rdi
	call	strlen@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r8
	leaq	-1(%r14), %rax
	cmpq	%rax, %r8
	je	.L188
.L166:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L189
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L188:
	.cfi_restore_state
	subq	$8, %rsp
	pushq	-288(%rbp)
	movq	%r12, %rcx
	movq	%r14, %rsi
	movq	-296(%rbp), %r9
	leaq	.LC5(%rip), %rdx
	movq	%r13, %rdi
	xorl	%eax, %eax
	movq	-280(%rbp), %r8
	call	BIO_snprintf@PLT
	popq	%rax
	popq	%rdx
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L168:
	leaq	-256(%rbp), %r15
	movq	-280(%rbp), %rcx
	leaq	.LC1(%rip), %rdx
	xorl	%eax, %eax
	movl	$64, %esi
	movq	%r15, %rdi
	call	BIO_snprintf@PLT
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L187:
	leaq	-128(%rbp), %rdi
	movq	%rdx, %rcx
	movl	$64, %esi
	leaq	.LC3(%rip), %rdx
	movq	%rdi, -304(%rbp)
	call	BIO_snprintf@PLT
	movq	-304(%rbp), %rdi
	movq	%rdi, %rax
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L173:
	leaq	-192(%rbp), %rbx
	movq	-296(%rbp), %rcx
	leaq	.LC2(%rip), %rdx
	xorl	%eax, %eax
	movl	$64, %esi
	movq	%rbx, %rdi
	call	BIO_snprintf@PLT
	jmp	.L176
.L189:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE352:
	.size	ERR_error_string_n.part.0, .-ERR_error_string_n.part.0
	.p2align 4
	.globl	ERR_error_string_n
	.type	ERR_error_string_n, @function
ERR_error_string_n:
.LFB325:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	jne	.L192
	ret
	.p2align 4,,10
	.p2align 3
.L192:
	jmp	ERR_error_string_n.part.0
	.cfi_endproc
.LFE325:
	.size	ERR_error_string_n, .-ERR_error_string_n
	.p2align 4
	.globl	ERR_error_string
	.type	ERR_error_string, @function
ERR_error_string:
.LFB326:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	buf.7413(%rip), %rax
	movl	$256, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	testq	%rsi, %rsi
	cmove	%rax, %r12
	movq	%r12, %rsi
	call	ERR_error_string_n.part.0
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE326:
	.size	ERR_error_string, .-ERR_error_string
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"../deps/openssl/openssl/crypto/err/err.c"
	.text
	.p2align 4
	.globl	err_delete_thread_state
	.type	err_delete_thread_state, @function
err_delete_thread_state:
.LFB330:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	err_thread_local(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	CRYPTO_THREAD_get_local@PLT
	testq	%rax, %rax
	je	.L196
	movq	%rax, %r12
	xorl	%esi, %esi
	leaq	err_thread_local(%rip), %rdi
	call	CRYPTO_THREAD_set_local@PLT
	testb	$1, 320(%r12)
	jne	.L263
.L198:
	movl	$0, 320(%r12)
	testb	$1, 324(%r12)
	jne	.L264
.L199:
	movl	$0, 324(%r12)
	testb	$1, 328(%r12)
	jne	.L265
.L200:
	movl	$0, 328(%r12)
	testb	$1, 332(%r12)
	jne	.L266
.L201:
	movl	$0, 332(%r12)
	testb	$1, 336(%r12)
	jne	.L267
.L202:
	movl	$0, 336(%r12)
	testb	$1, 340(%r12)
	jne	.L268
.L203:
	movl	$0, 340(%r12)
	testb	$1, 344(%r12)
	jne	.L269
.L204:
	movl	$0, 344(%r12)
	testb	$1, 348(%r12)
	jne	.L270
.L205:
	movl	$0, 348(%r12)
	testb	$1, 352(%r12)
	jne	.L271
.L206:
	movl	$0, 352(%r12)
	testb	$1, 356(%r12)
	jne	.L272
.L207:
	movl	$0, 356(%r12)
	testb	$1, 360(%r12)
	jne	.L273
.L208:
	movl	$0, 360(%r12)
	testb	$1, 364(%r12)
	jne	.L274
.L209:
	movl	$0, 364(%r12)
	testb	$1, 368(%r12)
	jne	.L275
.L210:
	movl	$0, 368(%r12)
	testb	$1, 372(%r12)
	jne	.L276
.L211:
	movl	$0, 372(%r12)
	testb	$1, 376(%r12)
	jne	.L277
.L212:
	movl	$0, 376(%r12)
	testb	$1, 380(%r12)
	jne	.L278
.L213:
	movq	%r12, %rdi
	movl	$291, %edx
	movl	$0, 380(%r12)
	addq	$8, %rsp
	popq	%r12
	leaq	.LC6(%rip), %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L278:
	.cfi_restore_state
	movq	312(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 312(%r12)
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L277:
	movq	304(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 304(%r12)
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L276:
	movq	296(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 296(%r12)
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L275:
	movq	288(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 288(%r12)
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L274:
	movq	280(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 280(%r12)
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L273:
	movq	272(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 272(%r12)
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L272:
	movq	264(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 264(%r12)
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L271:
	movq	256(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 256(%r12)
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L270:
	movq	248(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 248(%r12)
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L269:
	movq	240(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 240(%r12)
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L268:
	movq	232(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 232(%r12)
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L267:
	movq	224(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 224(%r12)
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L266:
	movq	216(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 216(%r12)
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L265:
	movq	208(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 208(%r12)
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L264:
	movq	200(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 200(%r12)
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L263:
	movq	192(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 192(%r12)
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L196:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE330:
	.size	err_delete_thread_state, .-err_delete_thread_state
	.p2align 4
	.globl	ERR_remove_thread_state
	.type	ERR_remove_thread_state, @function
ERR_remove_thread_state:
.LFB331:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE331:
	.size	ERR_remove_thread_state, .-ERR_remove_thread_state
	.p2align 4
	.globl	ERR_remove_state
	.type	ERR_remove_state, @function
ERR_remove_state:
.LFB332:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE332:
	.size	ERR_remove_state, .-ERR_remove_state
	.p2align 4
	.globl	ERR_get_state
	.type	ERR_get_state, @function
ERR_get_state:
.LFB335:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	call	__errno_location@PLT
	xorl	%esi, %esi
	movl	$262144, %edi
	movl	(%rax), %r13d
	movq	%rax, %rbx
	call	OPENSSL_init_crypto@PLT
	testl	%eax, %eax
	je	.L284
	leaq	err_do_init_ossl_(%rip), %rsi
	leaq	err_init(%rip), %rdi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L284
	movl	err_do_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	je	.L284
	leaq	err_thread_local(%rip), %rdi
	call	CRYPTO_THREAD_get_local@PLT
	movq	%rax, %r12
	cmpq	$-1, %rax
	je	.L284
	testq	%rax, %rax
	je	.L367
.L285:
	movl	%r13d, (%rbx)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L367:
	.cfi_restore_state
	movq	$-1, %rsi
	leaq	err_thread_local(%rip), %rdi
	call	CRYPTO_THREAD_set_local@PLT
	testl	%eax, %eax
	jne	.L368
	.p2align 4,,10
	.p2align 3
.L284:
	xorl	%r12d, %r12d
.L281:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L368:
	.cfi_restore_state
	movl	$734, %edx
	leaq	.LC6(%rip), %rsi
	movl	$584, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L366
	movl	$2, %edi
	call	ossl_init_thread_start@PLT
	testl	%eax, %eax
	je	.L305
	movq	%r14, %rsi
	leaq	err_thread_local(%rip), %rdi
	call	CRYPTO_THREAD_set_local@PLT
	testl	%eax, %eax
	je	.L305
	xorl	%esi, %esi
	movl	$2, %edi
	movq	%r14, %r12
	call	OPENSSL_init_crypto@PLT
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L305:
	testb	$1, 320(%r14)
	jne	.L369
.L288:
	movl	$0, 320(%r14)
	testb	$1, 324(%r14)
	jne	.L370
.L289:
	movl	$0, 324(%r14)
	testb	$1, 328(%r14)
	jne	.L371
.L290:
	movl	$0, 328(%r14)
	testb	$1, 332(%r14)
	jne	.L372
.L291:
	movl	$0, 332(%r14)
	testb	$1, 336(%r14)
	jne	.L373
.L292:
	movl	$0, 336(%r14)
	testb	$1, 340(%r14)
	jne	.L374
.L293:
	movl	$0, 340(%r14)
	testb	$1, 344(%r14)
	jne	.L375
.L294:
	movl	$0, 344(%r14)
	testb	$1, 348(%r14)
	jne	.L376
.L295:
	movl	$0, 348(%r14)
	testb	$1, 352(%r14)
	jne	.L377
.L296:
	movl	$0, 352(%r14)
	testb	$1, 356(%r14)
	jne	.L378
.L297:
	movl	$0, 356(%r14)
	testb	$1, 360(%r14)
	jne	.L379
.L298:
	movl	$0, 360(%r14)
	testb	$1, 364(%r14)
	jne	.L380
.L299:
	movl	$0, 364(%r14)
	testb	$1, 368(%r14)
	jne	.L381
.L300:
	movl	$0, 368(%r14)
	testb	$1, 372(%r14)
	jne	.L382
.L301:
	movl	$0, 372(%r14)
	testb	$1, 376(%r14)
	jne	.L383
.L302:
	movl	$0, 376(%r14)
	testb	$1, 380(%r14)
	jne	.L384
.L304:
	movl	$291, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r14, %rdi
	movl	$0, 380(%r14)
	call	CRYPTO_free@PLT
.L366:
	xorl	%esi, %esi
	leaq	err_thread_local(%rip), %rdi
	call	CRYPTO_THREAD_set_local@PLT
	jmp	.L281
.L369:
	movq	192(%r14), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 192(%r14)
	jmp	.L288
.L384:
	movq	312(%r14), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 312(%r14)
	jmp	.L304
.L383:
	movq	304(%r14), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 304(%r14)
	jmp	.L302
.L382:
	movq	296(%r14), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 296(%r14)
	jmp	.L301
.L381:
	movq	288(%r14), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 288(%r14)
	jmp	.L300
.L380:
	movq	280(%r14), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 280(%r14)
	jmp	.L299
.L379:
	movq	272(%r14), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 272(%r14)
	jmp	.L298
.L378:
	movq	264(%r14), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 264(%r14)
	jmp	.L297
.L377:
	movq	256(%r14), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 256(%r14)
	jmp	.L296
.L376:
	movq	248(%r14), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 248(%r14)
	jmp	.L295
.L375:
	movq	240(%r14), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 240(%r14)
	jmp	.L294
.L374:
	movq	232(%r14), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 232(%r14)
	jmp	.L293
.L373:
	movq	224(%r14), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 224(%r14)
	jmp	.L292
.L372:
	movq	216(%r14), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 216(%r14)
	jmp	.L291
.L371:
	movq	208(%r14), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 208(%r14)
	jmp	.L290
.L370:
	movq	200(%r14), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 200(%r14)
	jmp	.L289
	.cfi_endproc
.LFE335:
	.size	ERR_get_state, .-ERR_get_state
	.p2align 4
	.globl	ERR_put_error
	.type	ERR_put_error, @function
ERR_put_error:
.LFB313:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$24, %rsp
	call	ERR_get_state
	testq	%rax, %rax
	je	.L385
	movl	576(%rax), %edi
	leal	1(%rdi), %ecx
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$28, %edx
	addl	%edx, %ecx
	andl	$15, %ecx
	subl	%edx, %ecx
	movl	%ecx, 576(%rax)
	cmpl	580(%rax), %ecx
	jne	.L387
	leal	1(%rcx), %edx
	movl	%edx, %esi
	sarl	$31, %esi
	shrl	$28, %esi
	addl	%esi, %edx
	andl	$15, %edx
	subl	%esi, %edx
	movl	%edx, 580(%rax)
.L387:
	sall	$12, %ebx
	sall	$24, %r13d
	andl	$4095, %r12d
	movslq	%ecx, %rcx
	andl	$16773120, %ebx
	leaq	(%rax,%rcx,4), %r8
	leaq	(%rax,%rcx,8), %r9
	orl	%ebx, %r13d
	movl	$0, (%r8)
	orl	%r13d, %r12d
	movl	%r12d, %esi
	movq	%rsi, 64(%rax,%rcx,8)
	movq	%r15, 384(%r9)
	movl	%r14d, 512(%r8)
	testb	$1, 320(%r8)
	jne	.L396
.L388:
	movl	$0, 320(%rax,%rcx,4)
.L385:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L396:
	.cfi_restore_state
	movq	192(%r9), %rdi
	movl	$437, %edx
	leaq	.LC6(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	CRYPTO_free@PLT
	movq	-56(%rbp), %rax
	movslq	576(%rax), %rcx
	movq	$0, 192(%rax,%rcx,8)
	jmp	.L388
	.cfi_endproc
.LFE313:
	.size	ERR_put_error, .-ERR_put_error
	.p2align 4
	.globl	ERR_clear_error
	.type	ERR_clear_error, @function
ERR_clear_error:
.LFB314:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	call	ERR_get_state
	testq	%rax, %rax
	je	.L397
	movq	%rax, %rbx
	testb	$1, 320(%rax)
	jne	.L467
.L399:
	movl	$0, 320(%rbx)
	movl	$0, (%rbx)
	movq	$0, 64(%rbx)
	movq	$0, 384(%rbx)
	movl	$-1, 512(%rbx)
	testb	$1, 324(%rbx)
	jne	.L468
.L400:
	movl	$0, 324(%rbx)
	movl	$0, 4(%rbx)
	movq	$0, 72(%rbx)
	movq	$0, 392(%rbx)
	movl	$-1, 516(%rbx)
	testb	$1, 328(%rbx)
	jne	.L469
.L401:
	movl	$0, 328(%rbx)
	movl	$0, 8(%rbx)
	movq	$0, 80(%rbx)
	movq	$0, 400(%rbx)
	movl	$-1, 520(%rbx)
	testb	$1, 332(%rbx)
	jne	.L470
.L402:
	movl	$0, 332(%rbx)
	movl	$0, 12(%rbx)
	movq	$0, 88(%rbx)
	movq	$0, 408(%rbx)
	movl	$-1, 524(%rbx)
	testb	$1, 336(%rbx)
	jne	.L471
.L403:
	movl	$0, 336(%rbx)
	movl	$0, 16(%rbx)
	movq	$0, 96(%rbx)
	movq	$0, 416(%rbx)
	movl	$-1, 528(%rbx)
	testb	$1, 340(%rbx)
	jne	.L472
.L404:
	movl	$0, 340(%rbx)
	movl	$0, 20(%rbx)
	movq	$0, 104(%rbx)
	movq	$0, 424(%rbx)
	movl	$-1, 532(%rbx)
	testb	$1, 344(%rbx)
	jne	.L473
.L405:
	movl	$0, 344(%rbx)
	movl	$0, 24(%rbx)
	movq	$0, 112(%rbx)
	movq	$0, 432(%rbx)
	movl	$-1, 536(%rbx)
	testb	$1, 348(%rbx)
	jne	.L474
.L406:
	movl	$0, 348(%rbx)
	movl	$0, 28(%rbx)
	movq	$0, 120(%rbx)
	movq	$0, 440(%rbx)
	movl	$-1, 540(%rbx)
	testb	$1, 352(%rbx)
	jne	.L475
.L407:
	movl	$0, 352(%rbx)
	movl	$0, 32(%rbx)
	movq	$0, 128(%rbx)
	movq	$0, 448(%rbx)
	movl	$-1, 544(%rbx)
	testb	$1, 356(%rbx)
	jne	.L476
.L408:
	movl	$0, 356(%rbx)
	movl	$0, 36(%rbx)
	movq	$0, 136(%rbx)
	movq	$0, 456(%rbx)
	movl	$-1, 548(%rbx)
	testb	$1, 360(%rbx)
	jne	.L477
.L409:
	movl	$0, 360(%rbx)
	movl	$0, 40(%rbx)
	movq	$0, 144(%rbx)
	movq	$0, 464(%rbx)
	movl	$-1, 552(%rbx)
	testb	$1, 364(%rbx)
	jne	.L478
.L410:
	movl	$0, 364(%rbx)
	movl	$0, 44(%rbx)
	movq	$0, 152(%rbx)
	movq	$0, 472(%rbx)
	movl	$-1, 556(%rbx)
	testb	$1, 368(%rbx)
	jne	.L479
.L411:
	movl	$0, 368(%rbx)
	movl	$0, 48(%rbx)
	movq	$0, 160(%rbx)
	movq	$0, 480(%rbx)
	movl	$-1, 560(%rbx)
	testb	$1, 372(%rbx)
	jne	.L480
.L412:
	movl	$0, 372(%rbx)
	movl	$0, 52(%rbx)
	movq	$0, 168(%rbx)
	movq	$0, 488(%rbx)
	movl	$-1, 564(%rbx)
	testb	$1, 376(%rbx)
	jne	.L481
.L413:
	movl	$0, 376(%rbx)
	movl	$0, 56(%rbx)
	movq	$0, 176(%rbx)
	movq	$0, 496(%rbx)
	movl	$-1, 568(%rbx)
	testb	$1, 380(%rbx)
	jne	.L482
.L414:
	movl	$0, 380(%rbx)
	movl	$0, 60(%rbx)
	movq	$0, 184(%rbx)
	movq	$0, 504(%rbx)
	movl	$-1, 572(%rbx)
	movq	$0, 576(%rbx)
.L397:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L467:
	.cfi_restore_state
	movq	192(%rax), %rdi
	movl	$450, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 192(%rbx)
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L482:
	movq	312(%rbx), %rdi
	movl	$450, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 312(%rbx)
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L481:
	movq	304(%rbx), %rdi
	movl	$450, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 304(%rbx)
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L480:
	movq	296(%rbx), %rdi
	movl	$450, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 296(%rbx)
	jmp	.L412
	.p2align 4,,10
	.p2align 3
.L479:
	movq	288(%rbx), %rdi
	movl	$450, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 288(%rbx)
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L478:
	movq	280(%rbx), %rdi
	movl	$450, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 280(%rbx)
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L477:
	movq	272(%rbx), %rdi
	movl	$450, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 272(%rbx)
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L476:
	movq	264(%rbx), %rdi
	movl	$450, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 264(%rbx)
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L475:
	movq	256(%rbx), %rdi
	movl	$450, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 256(%rbx)
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L474:
	movq	248(%rbx), %rdi
	movl	$450, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 248(%rbx)
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L473:
	movq	240(%rbx), %rdi
	movl	$450, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 240(%rbx)
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L472:
	movq	232(%rbx), %rdi
	movl	$450, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 232(%rbx)
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L471:
	movq	224(%rbx), %rdi
	movl	$450, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 224(%rbx)
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L470:
	movq	216(%rbx), %rdi
	movl	$450, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 216(%rbx)
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L469:
	movq	208(%rbx), %rdi
	movl	$450, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 208(%rbx)
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L468:
	movq	200(%rbx), %rdi
	movl	$450, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 200(%rbx)
	jmp	.L400
	.cfi_endproc
.LFE314:
	.size	ERR_clear_error, .-ERR_clear_error
	.section	.rodata.str1.1
.LC7:
	.string	"NA"
.LC8:
	.string	""
	.text
	.p2align 4
	.globl	ERR_get_error_line_data
	.type	ERR_get_error_line_data, @function
ERR_get_error_line_data:
.LFB317:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	call	ERR_get_state
	testq	%rax, %rax
	je	.L486
	movq	%rax, %rbx
	movl	580(%rax), %eax
	movl	576(%rbx), %edx
	cmpl	%eax, %edx
	je	.L486
	movl	$15, %ecx
.L494:
	movslq	%edx, %rdx
	leaq	(%rbx,%rdx,4), %rsi
	testb	$2, (%rsi)
	jne	.L520
	addl	$1, %eax
	cltd
	shrl	$28, %edx
	addl	%edx, %eax
	andl	$15, %eax
	subl	%edx, %eax
	movslq	%eax, %rdx
	leaq	(%rbx,%rdx,4), %r8
	testb	$2, (%r8)
	jne	.L521
	leaq	(%rbx,%rdx,8), %rcx
	movq	64(%rcx), %r9
	movl	%eax, 580(%rbx)
	movq	$0, 64(%rcx)
	testq	%r13, %r13
	je	.L496
	testq	%r14, %r14
	je	.L496
	movq	384(%rcx), %rax
	testq	%rax, %rax
	je	.L522
	movq	%rax, 0(%r13)
	movl	512(%rbx,%rdx,4), %eax
	movl	%eax, (%r14)
.L496:
	testq	%r12, %r12
	je	.L523
	movq	192(%rcx), %rax
	testq	%rax, %rax
	je	.L524
	movq	%rax, (%r12)
	testq	%r15, %r15
	je	.L483
	movl	320(%rbx,%rdx,4), %eax
	movl	%eax, (%r15)
.L483:
	addq	$40, %rsp
	movq	%r9, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L520:
	.cfi_restore_state
	testb	$1, 320(%rsi)
	jne	.L488
	movslq	576(%rbx), %rdx
	movq	%rdx, %rsi
.L489:
	leaq	(%rbx,%rdx,4), %rax
	leaq	(%rbx,%rdx,8), %rdx
	testl	%esi, %esi
	movl	$0, 320(%rax)
	movl	$0, (%rax)
	movq	$0, 64(%rdx)
	movq	$0, 384(%rdx)
	leal	-1(%rsi), %edx
	movl	$-1, 512(%rax)
	cmovle	%ecx, %edx
	movl	580(%rbx), %eax
	movl	%edx, 576(%rbx)
.L491:
	cmpl	%eax, %edx
	jne	.L494
	.p2align 4,,10
	.p2align 3
.L486:
	xorl	%r9d, %r9d
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L521:
	movl	%eax, 580(%rbx)
	testb	$1, 320(%r8)
	jne	.L525
.L493:
	leaq	(%rbx,%rdx,4), %rsi
	leaq	(%rbx,%rdx,8), %rdx
	movl	$0, 320(%rsi)
	movl	$0, (%rsi)
	movq	$0, 64(%rdx)
	movq	$0, 384(%rdx)
	movl	$-1, 512(%rsi)
	movl	576(%rbx), %edx
	jmp	.L491
	.p2align 4,,10
	.p2align 3
.L523:
	testb	$1, 320(%r8)
	jne	.L526
.L498:
	movl	$0, 320(%r8)
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L525:
	movq	192(%rbx,%rdx,8), %rdi
	leaq	.LC6(%rip), %rsi
	movl	$537, %edx
	call	CRYPTO_free@PLT
	movslq	580(%rbx), %rdx
	movl	$15, %ecx
	movq	$0, 192(%rbx,%rdx,8)
	movq	%rdx, %rax
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L522:
	leaq	.LC7(%rip), %rax
	movq	%rax, 0(%r13)
	movl	$0, (%r14)
	jmp	.L496
	.p2align 4,,10
	.p2align 3
.L488:
	movq	192(%rbx,%rdx,8), %rdi
	leaq	.LC6(%rip), %rsi
	movl	$530, %edx
	call	CRYPTO_free@PLT
	movslq	576(%rbx), %rdx
	movl	$15, %ecx
	movq	$0, 192(%rbx,%rdx,8)
	movq	%rdx, %rsi
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L524:
	leaq	.LC8(%rip), %rax
	movq	%rax, (%r12)
	testq	%r15, %r15
	je	.L483
	movl	$0, (%r15)
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L526:
	movq	192(%rcx), %rdi
	movl	$569, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r9, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	CRYPTO_free@PLT
	movq	-56(%rbp), %rcx
	movq	-72(%rbp), %r9
	movq	-64(%rbp), %r8
	movq	$0, 192(%rcx)
	jmp	.L498
	.cfi_endproc
.LFE317:
	.size	ERR_get_error_line_data, .-ERR_get_error_line_data
	.p2align 4
	.globl	ERR_peek_last_error
	.type	ERR_peek_last_error, @function
ERR_peek_last_error:
.LFB321:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	call	ERR_get_state
	testq	%rax, %rax
	je	.L530
	movq	%rax, %rbx
	movl	580(%rax), %eax
	movl	576(%rbx), %edx
	cmpl	%eax, %edx
	je	.L530
	movl	$15, %r12d
.L538:
	movslq	%edx, %rdx
	leaq	(%rbx,%rdx,4), %rcx
	testb	$2, (%rcx)
	jne	.L545
	addl	$1, %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$28, %ecx
	addl	%ecx, %eax
	andl	$15, %eax
	subl	%ecx, %eax
	movslq	%eax, %rcx
	leaq	(%rbx,%rcx,4), %rsi
	testb	$2, (%rsi)
	jne	.L546
	movq	64(%rbx,%rdx,8), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L545:
	.cfi_restore_state
	testb	$1, 320(%rcx)
	jne	.L532
	movslq	576(%rbx), %rdx
	movq	%rdx, %rcx
.L533:
	leaq	(%rbx,%rdx,4), %rax
	leaq	(%rbx,%rdx,8), %rdx
	testl	%ecx, %ecx
	movl	$0, 320(%rax)
	movl	$0, (%rax)
	movq	$0, 64(%rdx)
	movq	$0, 384(%rdx)
	leal	-1(%rcx), %edx
	movl	$-1, 512(%rax)
	cmovle	%r12d, %edx
	movl	580(%rbx), %eax
	movl	%edx, 576(%rbx)
.L535:
	cmpl	%eax, %edx
	jne	.L538
.L530:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L546:
	.cfi_restore_state
	movl	%eax, 580(%rbx)
	testb	$1, 320(%rsi)
	jne	.L547
.L537:
	leaq	(%rbx,%rcx,4), %rdx
	leaq	(%rbx,%rcx,8), %rcx
	movl	$0, 320(%rdx)
	movl	$0, (%rdx)
	movq	$0, 64(%rcx)
	movq	$0, 384(%rcx)
	movl	$-1, 512(%rdx)
	movl	576(%rbx), %edx
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L547:
	movq	192(%rbx,%rcx,8), %rdi
	movl	$537, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movslq	580(%rbx), %rcx
	movq	$0, 192(%rbx,%rcx,8)
	movq	%rcx, %rax
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L532:
	movq	192(%rbx,%rdx,8), %rdi
	leaq	.LC6(%rip), %rsi
	movl	$530, %edx
	call	CRYPTO_free@PLT
	movslq	576(%rbx), %rdx
	movq	$0, 192(%rbx,%rdx,8)
	movq	%rdx, %rcx
	jmp	.L533
	.cfi_endproc
.LFE321:
	.size	ERR_peek_last_error, .-ERR_peek_last_error
	.p2align 4
	.globl	ERR_peek_error
	.type	ERR_peek_error, @function
ERR_peek_error:
.LFB318:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	call	ERR_get_state
	testq	%rax, %rax
	je	.L551
	movq	%rax, %rbx
	movl	580(%rax), %eax
	movl	576(%rbx), %edx
	cmpl	%eax, %edx
	je	.L551
	movl	$15, %r12d
.L559:
	movslq	%edx, %rdx
	leaq	(%rbx,%rdx,4), %rcx
	testb	$2, (%rcx)
	jne	.L566
	addl	$1, %eax
	cltd
	shrl	$28, %edx
	addl	%edx, %eax
	andl	$15, %eax
	subl	%edx, %eax
	movslq	%eax, %rdx
	leaq	(%rbx,%rdx,4), %rcx
	testb	$2, (%rcx)
	jne	.L567
	movq	64(%rbx,%rdx,8), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L566:
	.cfi_restore_state
	testb	$1, 320(%rcx)
	jne	.L553
	movslq	576(%rbx), %rdx
	movq	%rdx, %rcx
.L554:
	leaq	(%rbx,%rdx,4), %rax
	leaq	(%rbx,%rdx,8), %rdx
	testl	%ecx, %ecx
	movl	$0, 320(%rax)
	movl	$0, (%rax)
	movq	$0, 64(%rdx)
	movq	$0, 384(%rdx)
	leal	-1(%rcx), %edx
	movl	$-1, 512(%rax)
	cmovle	%r12d, %edx
	movl	580(%rbx), %eax
	movl	%edx, 576(%rbx)
.L556:
	cmpl	%edx, %eax
	jne	.L559
.L551:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L567:
	.cfi_restore_state
	movl	%eax, 580(%rbx)
	testb	$1, 320(%rcx)
	jne	.L568
.L558:
	leaq	(%rbx,%rdx,4), %rcx
	leaq	(%rbx,%rdx,8), %rdx
	movl	$0, 320(%rcx)
	movl	$0, (%rcx)
	movq	$0, 64(%rdx)
	movq	$0, 384(%rdx)
	movl	$-1, 512(%rcx)
	movl	576(%rbx), %edx
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L568:
	movq	192(%rbx,%rdx,8), %rdi
	leaq	.LC6(%rip), %rsi
	movl	$537, %edx
	call	CRYPTO_free@PLT
	movslq	580(%rbx), %rdx
	movq	$0, 192(%rbx,%rdx,8)
	movq	%rdx, %rax
	jmp	.L558
	.p2align 4,,10
	.p2align 3
.L553:
	movq	192(%rbx,%rdx,8), %rdi
	leaq	.LC6(%rip), %rsi
	movl	$530, %edx
	call	CRYPTO_free@PLT
	movslq	576(%rbx), %rdx
	movq	$0, 192(%rbx,%rdx,8)
	movq	%rdx, %rcx
	jmp	.L554
	.cfi_endproc
.LFE318:
	.size	ERR_peek_error, .-ERR_peek_error
	.p2align 4
	.globl	ERR_peek_last_error_line
	.type	ERR_peek_last_error_line, @function
ERR_peek_last_error_line:
.LFB322:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	call	ERR_get_state
	testq	%rax, %rax
	je	.L572
	movq	%rax, %rbx
	movl	580(%rax), %eax
	movl	576(%rbx), %edx
	cmpl	%eax, %edx
	je	.L572
	movl	$15, %r14d
.L580:
	movslq	%edx, %rdx
	leaq	(%rbx,%rdx,4), %rcx
	testb	$2, (%rcx)
	jne	.L593
	addl	$1, %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$28, %ecx
	addl	%ecx, %eax
	andl	$15, %eax
	subl	%ecx, %eax
	movslq	%eax, %rcx
	leaq	(%rbx,%rcx,4), %rsi
	testb	$2, (%rsi)
	jne	.L594
	leaq	(%rbx,%rdx,8), %rcx
	movq	64(%rcx), %rax
	testq	%r12, %r12
	je	.L569
	testq	%r13, %r13
	je	.L569
	movq	384(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L595
	movl	512(%rbx,%rdx,4), %edx
	movq	%rcx, (%r12)
	movl	%edx, 0(%r13)
.L569:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L593:
	.cfi_restore_state
	testb	$1, 320(%rcx)
	jne	.L574
	movslq	576(%rbx), %rdx
	movq	%rdx, %rcx
.L575:
	leaq	(%rbx,%rdx,4), %rax
	leaq	(%rbx,%rdx,8), %rdx
	testl	%ecx, %ecx
	movl	$0, 320(%rax)
	movl	$0, (%rax)
	movq	$0, 64(%rdx)
	movq	$0, 384(%rdx)
	leal	-1(%rcx), %edx
	movl	$-1, 512(%rax)
	cmovle	%r14d, %edx
	movl	580(%rbx), %eax
	movl	%edx, 576(%rbx)
.L577:
	cmpl	%edx, %eax
	jne	.L580
.L572:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L594:
	.cfi_restore_state
	movl	%eax, 580(%rbx)
	testb	$1, 320(%rsi)
	jne	.L596
.L579:
	leaq	(%rbx,%rcx,4), %rdx
	leaq	(%rbx,%rcx,8), %rcx
	movl	$0, 320(%rdx)
	movl	$0, (%rdx)
	movq	$0, 64(%rcx)
	movq	$0, 384(%rcx)
	movl	$-1, 512(%rdx)
	movl	576(%rbx), %edx
	jmp	.L577
	.p2align 4,,10
	.p2align 3
.L596:
	movq	192(%rbx,%rcx,8), %rdi
	movl	$537, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movslq	580(%rbx), %rcx
	movq	$0, 192(%rbx,%rcx,8)
	movq	%rcx, %rax
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L595:
	leaq	.LC7(%rip), %rdi
	movq	%rdi, (%r12)
	movl	$0, 0(%r13)
	jmp	.L569
	.p2align 4,,10
	.p2align 3
.L574:
	movq	192(%rbx,%rdx,8), %rdi
	leaq	.LC6(%rip), %rsi
	movl	$530, %edx
	call	CRYPTO_free@PLT
	movslq	576(%rbx), %rdx
	movq	$0, 192(%rbx,%rdx,8)
	movq	%rdx, %rcx
	jmp	.L575
	.cfi_endproc
.LFE322:
	.size	ERR_peek_last_error_line, .-ERR_peek_last_error_line
	.p2align 4
	.globl	ERR_get_error
	.type	ERR_get_error, @function
ERR_get_error:
.LFB315:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	call	ERR_get_state
	testq	%rax, %rax
	je	.L600
	movq	%rax, %rbx
	movl	580(%rax), %eax
	movl	576(%rbx), %edx
	cmpl	%edx, %eax
	je	.L600
	movl	$15, %r12d
.L608:
	movslq	%edx, %rdx
	leaq	(%rbx,%rdx,4), %rcx
	testb	$2, (%rcx)
	jne	.L617
	addl	$1, %eax
	cltd
	shrl	$28, %edx
	addl	%edx, %eax
	andl	$15, %eax
	subl	%edx, %eax
	movslq	%eax, %rdx
	leaq	(%rbx,%rdx,4), %r13
	testb	$2, 0(%r13)
	jne	.L618
	leaq	(%rbx,%rdx,8), %r12
	movq	64(%r12), %r14
	movl	%eax, 580(%rbx)
	movq	$0, 64(%r12)
	testb	$1, 320(%r13)
	jne	.L619
.L610:
	movl	$0, 320(%r13)
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L617:
	.cfi_restore_state
	testb	$1, 320(%rcx)
	jne	.L602
	movslq	576(%rbx), %rdx
	movq	%rdx, %rcx
.L603:
	leaq	(%rbx,%rdx,4), %rax
	leaq	(%rbx,%rdx,8), %rdx
	testl	%ecx, %ecx
	movl	$0, 320(%rax)
	movl	$0, (%rax)
	movq	$0, 64(%rdx)
	movq	$0, 384(%rdx)
	leal	-1(%rcx), %edx
	movl	$-1, 512(%rax)
	cmovle	%r12d, %edx
	movl	580(%rbx), %eax
	movl	%edx, 576(%rbx)
.L605:
	cmpl	%eax, %edx
	jne	.L608
.L600:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L618:
	.cfi_restore_state
	movl	%eax, 580(%rbx)
	testb	$1, 320(%r13)
	jne	.L620
.L607:
	leaq	(%rbx,%rdx,4), %rcx
	leaq	(%rbx,%rdx,8), %rdx
	movl	$0, 320(%rcx)
	movl	$0, (%rcx)
	movq	$0, 64(%rdx)
	movq	$0, 384(%rdx)
	movl	$-1, 512(%rcx)
	movl	576(%rbx), %edx
	jmp	.L605
	.p2align 4,,10
	.p2align 3
.L619:
	movq	192(%r12), %rdi
	movl	$569, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 192(%r12)
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L620:
	movq	192(%rbx,%rdx,8), %rdi
	leaq	.LC6(%rip), %rsi
	movl	$537, %edx
	call	CRYPTO_free@PLT
	movslq	580(%rbx), %rdx
	movq	$0, 192(%rbx,%rdx,8)
	movq	%rdx, %rax
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L602:
	movq	192(%rbx,%rdx,8), %rdi
	leaq	.LC6(%rip), %rsi
	movl	$530, %edx
	call	CRYPTO_free@PLT
	movslq	576(%rbx), %rdx
	movq	$0, 192(%rbx,%rdx,8)
	movq	%rdx, %rcx
	jmp	.L603
	.cfi_endproc
.LFE315:
	.size	ERR_get_error, .-ERR_get_error
	.p2align 4
	.globl	ERR_peek_error_line
	.type	ERR_peek_error_line, @function
ERR_peek_error_line:
.LFB319:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	call	ERR_get_state
	testq	%rax, %rax
	je	.L624
	movq	%rax, %rbx
	movl	580(%rax), %eax
	movl	576(%rbx), %edx
	cmpl	%eax, %edx
	je	.L624
	movl	$15, %r14d
.L632:
	movslq	%edx, %rdx
	leaq	(%rbx,%rdx,4), %rcx
	testb	$2, (%rcx)
	jne	.L645
	addl	$1, %eax
	cltd
	shrl	$28, %edx
	addl	%edx, %eax
	andl	$15, %eax
	subl	%edx, %eax
	movslq	%eax, %rdx
	leaq	(%rbx,%rdx,4), %rcx
	testb	$2, (%rcx)
	jne	.L646
	leaq	(%rbx,%rdx,8), %rcx
	movq	64(%rcx), %rax
	testq	%r12, %r12
	je	.L621
	testq	%r13, %r13
	je	.L621
	movq	384(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L647
	movl	512(%rbx,%rdx,4), %edx
	movq	%rcx, (%r12)
	movl	%edx, 0(%r13)
.L621:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L645:
	.cfi_restore_state
	testb	$1, 320(%rcx)
	jne	.L626
	movslq	576(%rbx), %rdx
	movq	%rdx, %rcx
.L627:
	leaq	(%rbx,%rdx,4), %rax
	leaq	(%rbx,%rdx,8), %rdx
	testl	%ecx, %ecx
	movl	$0, 320(%rax)
	movl	$0, (%rax)
	movq	$0, 64(%rdx)
	movq	$0, 384(%rdx)
	leal	-1(%rcx), %edx
	movl	$-1, 512(%rax)
	cmovle	%r14d, %edx
	movl	580(%rbx), %eax
	movl	%edx, 576(%rbx)
.L629:
	cmpl	%edx, %eax
	jne	.L632
.L624:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L646:
	.cfi_restore_state
	movl	%eax, 580(%rbx)
	testb	$1, 320(%rcx)
	jne	.L648
.L631:
	leaq	(%rbx,%rdx,4), %rcx
	leaq	(%rbx,%rdx,8), %rdx
	movl	$0, 320(%rcx)
	movl	$0, (%rcx)
	movq	$0, 64(%rdx)
	movq	$0, 384(%rdx)
	movl	$-1, 512(%rcx)
	movl	576(%rbx), %edx
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L648:
	movq	192(%rbx,%rdx,8), %rdi
	leaq	.LC6(%rip), %rsi
	movl	$537, %edx
	call	CRYPTO_free@PLT
	movslq	580(%rbx), %rdx
	movq	$0, 192(%rbx,%rdx,8)
	movq	%rdx, %rax
	jmp	.L631
	.p2align 4,,10
	.p2align 3
.L647:
	leaq	.LC7(%rip), %rsi
	movq	%rsi, (%r12)
	movl	$0, 0(%r13)
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L626:
	movq	192(%rbx,%rdx,8), %rdi
	leaq	.LC6(%rip), %rsi
	movl	$530, %edx
	call	CRYPTO_free@PLT
	movslq	576(%rbx), %rdx
	movq	$0, 192(%rbx,%rdx,8)
	movq	%rdx, %rcx
	jmp	.L627
	.cfi_endproc
.LFE319:
	.size	ERR_peek_error_line, .-ERR_peek_error_line
	.p2align 4
	.globl	ERR_get_error_line
	.type	ERR_get_error_line, @function
ERR_get_error_line:
.LFB316:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	call	ERR_get_state
	testq	%rax, %rax
	je	.L652
	movq	%rax, %rbx
	movl	580(%rax), %eax
	movl	576(%rbx), %edx
	cmpl	%eax, %edx
	je	.L652
	movl	$15, %r14d
.L660:
	movslq	%edx, %rdx
	leaq	(%rbx,%rdx,4), %rcx
	testb	$2, (%rcx)
	jne	.L678
	addl	$1, %eax
	cltd
	shrl	$28, %edx
	addl	%edx, %eax
	andl	$15, %eax
	subl	%edx, %eax
	movslq	%eax, %rdx
	leaq	(%rbx,%rdx,4), %r15
	testb	$2, (%r15)
	jne	.L679
	leaq	(%rbx,%rdx,8), %r14
	movq	64(%r14), %r8
	movl	%eax, 580(%rbx)
	movq	$0, 64(%r14)
	testq	%r12, %r12
	je	.L662
	testq	%r13, %r13
	je	.L662
	movq	384(%r14), %rax
	testq	%rax, %rax
	je	.L680
	movq	%rax, (%r12)
	movl	512(%rbx,%rdx,4), %eax
	movl	%eax, 0(%r13)
.L662:
	testb	$1, 320(%r15)
	jne	.L681
.L663:
	movl	$0, 320(%r15)
.L649:
	addq	$24, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L678:
	.cfi_restore_state
	testb	$1, 320(%rcx)
	jne	.L654
	movslq	576(%rbx), %rdx
	movq	%rdx, %rcx
.L655:
	leaq	(%rbx,%rdx,4), %rax
	leaq	(%rbx,%rdx,8), %rdx
	testl	%ecx, %ecx
	movl	$0, 320(%rax)
	movl	$0, (%rax)
	movq	$0, 64(%rdx)
	movq	$0, 384(%rdx)
	leal	-1(%rcx), %edx
	movl	$-1, 512(%rax)
	cmovle	%r14d, %edx
	movl	580(%rbx), %eax
	movl	%edx, 576(%rbx)
.L657:
	cmpl	%edx, %eax
	jne	.L660
	.p2align 4,,10
	.p2align 3
.L652:
	xorl	%r8d, %r8d
	jmp	.L649
	.p2align 4,,10
	.p2align 3
.L679:
	movl	%eax, 580(%rbx)
	testb	$1, 320(%r15)
	jne	.L682
.L659:
	leaq	(%rbx,%rdx,4), %rcx
	leaq	(%rbx,%rdx,8), %rdx
	movl	$0, 320(%rcx)
	movl	$0, (%rcx)
	movq	$0, 64(%rdx)
	movq	$0, 384(%rdx)
	movl	$-1, 512(%rcx)
	movl	576(%rbx), %edx
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L681:
	movq	192(%r14), %rdi
	movl	$569, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r8, -56(%rbp)
	call	CRYPTO_free@PLT
	movq	-56(%rbp), %r8
	movq	$0, 192(%r14)
	jmp	.L663
	.p2align 4,,10
	.p2align 3
.L682:
	movq	192(%rbx,%rdx,8), %rdi
	leaq	.LC6(%rip), %rsi
	movl	$537, %edx
	call	CRYPTO_free@PLT
	movslq	580(%rbx), %rdx
	movq	$0, 192(%rbx,%rdx,8)
	movq	%rdx, %rax
	jmp	.L659
	.p2align 4,,10
	.p2align 3
.L680:
	leaq	.LC7(%rip), %rax
	movq	%rax, (%r12)
	movl	$0, 0(%r13)
	jmp	.L662
	.p2align 4,,10
	.p2align 3
.L654:
	movq	192(%rbx,%rdx,8), %rdi
	leaq	.LC6(%rip), %rsi
	movl	$530, %edx
	call	CRYPTO_free@PLT
	movslq	576(%rbx), %rdx
	movq	$0, 192(%rbx,%rdx,8)
	movq	%rdx, %rcx
	jmp	.L655
	.cfi_endproc
.LFE316:
	.size	ERR_get_error_line, .-ERR_get_error_line
	.p2align 4
	.globl	ERR_peek_last_error_line_data
	.type	ERR_peek_last_error_line_data, @function
ERR_peek_last_error_line_data:
.LFB323:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	ERR_get_state
	testq	%rax, %rax
	je	.L686
	movq	%rax, %rbx
	movl	580(%rax), %eax
	movl	576(%rbx), %edx
	cmpl	%eax, %edx
	je	.L686
	movl	$15, %ecx
.L694:
	movslq	%edx, %rdx
	leaq	(%rbx,%rdx,4), %rsi
	testb	$2, (%rsi)
	jne	.L718
	addl	$1, %eax
	movl	%eax, %esi
	sarl	$31, %esi
	shrl	$28, %esi
	addl	%esi, %eax
	andl	$15, %eax
	subl	%esi, %eax
	movslq	%eax, %rsi
	leaq	(%rbx,%rsi,4), %rdi
	testb	$2, (%rdi)
	jne	.L719
	leaq	(%rbx,%rdx,8), %rcx
	movq	64(%rcx), %rax
	testq	%r13, %r13
	je	.L696
	testq	%r14, %r14
	je	.L696
	movq	384(%rcx), %rsi
	testq	%rsi, %rsi
	je	.L720
	movq	%rsi, 0(%r13)
	movl	512(%rbx,%rdx,4), %esi
	movl	%esi, (%r14)
.L696:
	testq	%r12, %r12
	je	.L683
	movq	192(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L721
	movq	%rcx, (%r12)
	testq	%r15, %r15
	je	.L683
	movl	320(%rbx,%rdx,4), %edx
	movl	%edx, (%r15)
.L683:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L718:
	.cfi_restore_state
	testb	$1, 320(%rsi)
	jne	.L688
	movslq	576(%rbx), %rdx
	movq	%rdx, %rsi
.L689:
	leaq	(%rbx,%rdx,4), %rax
	leaq	(%rbx,%rdx,8), %rdx
	testl	%esi, %esi
	movl	$0, 320(%rax)
	movl	$0, (%rax)
	movq	$0, 64(%rdx)
	movq	$0, 384(%rdx)
	leal	-1(%rsi), %edx
	movl	$-1, 512(%rax)
	cmovle	%ecx, %edx
	movl	580(%rbx), %eax
	movl	%edx, 576(%rbx)
.L691:
	cmpl	%eax, %edx
	jne	.L694
.L686:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L719:
	.cfi_restore_state
	movl	%eax, 580(%rbx)
	testb	$1, 320(%rdi)
	jne	.L722
.L693:
	leaq	(%rbx,%rsi,4), %rdx
	leaq	(%rbx,%rsi,8), %rsi
	movl	$0, 320(%rdx)
	movl	$0, (%rdx)
	movq	$0, 64(%rsi)
	movq	$0, 384(%rsi)
	movl	$-1, 512(%rdx)
	movl	576(%rbx), %edx
	jmp	.L691
	.p2align 4,,10
	.p2align 3
.L722:
	movq	192(%rbx,%rsi,8), %rdi
	movl	$537, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movslq	580(%rbx), %rsi
	movl	$15, %ecx
	movq	$0, 192(%rbx,%rsi,8)
	movq	%rsi, %rax
	jmp	.L693
	.p2align 4,,10
	.p2align 3
.L720:
	leaq	.LC7(%rip), %rdi
	movq	%rdi, 0(%r13)
	movl	$0, (%r14)
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L688:
	movq	192(%rbx,%rdx,8), %rdi
	leaq	.LC6(%rip), %rsi
	movl	$530, %edx
	call	CRYPTO_free@PLT
	movslq	576(%rbx), %rdx
	movl	$15, %ecx
	movq	$0, 192(%rbx,%rdx,8)
	movq	%rdx, %rsi
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L721:
	leaq	.LC8(%rip), %rdi
	movq	%rdi, (%r12)
	testq	%r15, %r15
	je	.L683
	movl	$0, (%r15)
	jmp	.L683
	.cfi_endproc
.LFE323:
	.size	ERR_peek_last_error_line_data, .-ERR_peek_last_error_line_data
	.p2align 4
	.globl	ERR_peek_error_line_data
	.type	ERR_peek_error_line_data, @function
ERR_peek_error_line_data:
.LFB320:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	ERR_get_state
	testq	%rax, %rax
	je	.L726
	movq	%rax, %rbx
	movl	580(%rax), %eax
	movl	576(%rbx), %edx
	cmpl	%eax, %edx
	je	.L726
	movl	$15, %ecx
.L734:
	movslq	%edx, %rdx
	leaq	(%rbx,%rdx,4), %rsi
	testb	$2, (%rsi)
	jne	.L758
	addl	$1, %eax
	cltd
	shrl	$28, %edx
	addl	%edx, %eax
	andl	$15, %eax
	subl	%edx, %eax
	movslq	%eax, %rdx
	leaq	(%rbx,%rdx,4), %rsi
	testb	$2, (%rsi)
	jne	.L759
	leaq	(%rbx,%rdx,8), %rcx
	movq	64(%rcx), %rax
	testq	%r13, %r13
	je	.L736
	testq	%r14, %r14
	je	.L736
	movq	384(%rcx), %rsi
	testq	%rsi, %rsi
	je	.L760
	movq	%rsi, 0(%r13)
	movl	512(%rbx,%rdx,4), %esi
	movl	%esi, (%r14)
.L736:
	testq	%r12, %r12
	je	.L723
	movq	192(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L761
	movq	%rcx, (%r12)
	testq	%r15, %r15
	je	.L723
	movl	320(%rbx,%rdx,4), %edx
	movl	%edx, (%r15)
.L723:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L758:
	.cfi_restore_state
	testb	$1, 320(%rsi)
	jne	.L728
	movslq	576(%rbx), %rdx
	movq	%rdx, %rsi
.L729:
	leaq	(%rbx,%rdx,4), %rax
	leaq	(%rbx,%rdx,8), %rdx
	testl	%esi, %esi
	movl	$0, 320(%rax)
	movl	$0, (%rax)
	movq	$0, 64(%rdx)
	movq	$0, 384(%rdx)
	leal	-1(%rsi), %edx
	movl	$-1, 512(%rax)
	cmovle	%ecx, %edx
	movl	580(%rbx), %eax
	movl	%edx, 576(%rbx)
.L731:
	cmpl	%edx, %eax
	jne	.L734
.L726:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L759:
	.cfi_restore_state
	movl	%eax, 580(%rbx)
	testb	$1, 320(%rsi)
	jne	.L762
.L733:
	leaq	(%rbx,%rdx,4), %rsi
	leaq	(%rbx,%rdx,8), %rdx
	movl	$0, 320(%rsi)
	movl	$0, (%rsi)
	movq	$0, 64(%rdx)
	movq	$0, 384(%rdx)
	movl	$-1, 512(%rsi)
	movl	576(%rbx), %edx
	jmp	.L731
	.p2align 4,,10
	.p2align 3
.L762:
	movq	192(%rbx,%rdx,8), %rdi
	leaq	.LC6(%rip), %rsi
	movl	$537, %edx
	call	CRYPTO_free@PLT
	movslq	580(%rbx), %rdx
	movl	$15, %ecx
	movq	$0, 192(%rbx,%rdx,8)
	movq	%rdx, %rax
	jmp	.L733
	.p2align 4,,10
	.p2align 3
.L760:
	leaq	.LC7(%rip), %rdi
	movq	%rdi, 0(%r13)
	movl	$0, (%r14)
	jmp	.L736
	.p2align 4,,10
	.p2align 3
.L728:
	movq	192(%rbx,%rdx,8), %rdi
	leaq	.LC6(%rip), %rsi
	movl	$530, %edx
	call	CRYPTO_free@PLT
	movslq	576(%rbx), %rdx
	movl	$15, %ecx
	movq	$0, 192(%rbx,%rdx,8)
	movq	%rdx, %rsi
	jmp	.L729
	.p2align 4,,10
	.p2align 3
.L761:
	leaq	.LC8(%rip), %rdi
	movq	%rdi, (%r12)
	testq	%r15, %r15
	je	.L723
	movl	$0, (%r15)
	jmp	.L723
	.cfi_endproc
.LFE320:
	.size	ERR_peek_error_line_data, .-ERR_peek_error_line_data
	.p2align 4
	.globl	err_shelve_state
	.type	err_shelve_state, @function
err_shelve_state:
.LFB336:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	__errno_location@PLT
	xorl	%esi, %esi
	movl	$262144, %edi
	movl	(%rax), %r13d
	movq	%rax, %rbx
	call	OPENSSL_init_crypto@PLT
	testl	%eax, %eax
	jne	.L764
.L766:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L764:
	.cfi_restore_state
	leaq	err_do_init_ossl_(%rip), %rsi
	leaq	err_init(%rip), %rdi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L766
	movl	err_do_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	je	.L766
	leaq	err_thread_local(%rip), %rdi
	call	CRYPTO_THREAD_get_local@PLT
	movq	$-1, %rsi
	leaq	err_thread_local(%rip), %rdi
	movq	%rax, (%r12)
	call	CRYPTO_THREAD_set_local@PLT
	testl	%eax, %eax
	je	.L766
	movl	%r13d, (%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE336:
	.size	err_shelve_state, .-err_shelve_state
	.p2align 4
	.globl	err_unshelve_state
	.type	err_unshelve_state, @function
err_unshelve_state:
.LFB337:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	cmpq	$-1, %rdi
	je	.L774
	leaq	err_thread_local(%rip), %rdi
	jmp	CRYPTO_THREAD_set_local@PLT
	.p2align 4,,10
	.p2align 3
.L774:
	ret
	.cfi_endproc
.LFE337:
	.size	err_unshelve_state, .-err_unshelve_state
	.p2align 4
	.globl	ERR_get_next_error_library
	.type	ERR_get_next_error_library, @function
ERR_get_next_error_library:
.LFB338:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	do_err_strings_init_ossl_(%rip), %rsi
	leaq	err_string_init(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	CRYPTO_THREAD_run_once@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L776
	movl	do_err_strings_init_ossl_ret_(%rip), %r12d
	testl	%r12d, %r12d
	jne	.L785
.L776:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L785:
	.cfi_restore_state
	movq	err_string_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movl	int_err_library_number(%rip), %r12d
	movq	err_string_lock(%rip), %rdi
	leal	1(%r12), %eax
	movl	%eax, int_err_library_number(%rip)
	call	CRYPTO_THREAD_unlock@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE338:
	.size	ERR_get_next_error_library, .-ERR_get_next_error_library
	.p2align 4
	.globl	ERR_set_error_data
	.type	ERR_set_error_data, @function
ERR_set_error_data:
.LFB340:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	call	__errno_location@PLT
	xorl	%esi, %esi
	movl	$262144, %edi
	movl	(%rax), %r15d
	movq	%rax, %rbx
	call	OPENSSL_init_crypto@PLT
	testl	%eax, %eax
	je	.L786
	leaq	err_do_init_ossl_(%rip), %rsi
	leaq	err_init(%rip), %rdi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L786
	movl	err_do_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	je	.L786
	leaq	err_thread_local(%rip), %rdi
	call	CRYPTO_THREAD_get_local@PLT
	movq	%rax, %r12
	cmpq	$-1, %rax
	je	.L786
	testq	%rax, %rax
	je	.L874
.L790:
	movl	%r15d, (%rbx)
	movslq	576(%r12), %rbx
	leaq	(%r12,%rbx,4), %r15
	testb	$1, 320(%r15)
	jne	.L875
.L813:
	movq	%r13, 192(%r12,%rbx,8)
	movl	%r14d, 320(%r15)
.L786:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L875:
	.cfi_restore_state
	movq	192(%r12,%rbx,8), %rdi
	movl	$822, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L813
	.p2align 4,,10
	.p2align 3
.L874:
	movq	$-1, %rsi
	leaq	err_thread_local(%rip), %rdi
	call	CRYPTO_THREAD_set_local@PLT
	testl	%eax, %eax
	je	.L786
	movl	$734, %edx
	leaq	.LC6(%rip), %rsi
	movl	$584, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L873
	movl	$2, %edi
	call	ossl_init_thread_start@PLT
	testl	%eax, %eax
	je	.L810
	movq	%r12, %rsi
	leaq	err_thread_local(%rip), %rdi
	call	CRYPTO_THREAD_set_local@PLT
	testl	%eax, %eax
	je	.L810
	xorl	%esi, %esi
	movl	$2, %edi
	call	OPENSSL_init_crypto@PLT
	jmp	.L790
	.p2align 4,,10
	.p2align 3
.L810:
	testb	$1, 320(%r12)
	jne	.L876
.L793:
	movl	$0, 320(%r12)
	testb	$1, 324(%r12)
	jne	.L877
.L794:
	movl	$0, 324(%r12)
	testb	$1, 328(%r12)
	jne	.L878
.L795:
	movl	$0, 328(%r12)
	testb	$1, 332(%r12)
	jne	.L879
.L796:
	movl	$0, 332(%r12)
	testb	$1, 336(%r12)
	jne	.L880
.L797:
	movl	$0, 336(%r12)
	testb	$1, 340(%r12)
	jne	.L881
.L798:
	movl	$0, 340(%r12)
	testb	$1, 344(%r12)
	jne	.L882
.L799:
	movl	$0, 344(%r12)
	testb	$1, 348(%r12)
	jne	.L883
.L800:
	movl	$0, 348(%r12)
	testb	$1, 352(%r12)
	jne	.L884
.L801:
	movl	$0, 352(%r12)
	testb	$1, 356(%r12)
	jne	.L885
.L802:
	movl	$0, 356(%r12)
	testb	$1, 360(%r12)
	jne	.L886
.L803:
	movl	$0, 360(%r12)
	testb	$1, 364(%r12)
	jne	.L887
.L804:
	movl	$0, 364(%r12)
	testb	$1, 368(%r12)
	jne	.L888
.L805:
	movl	$0, 368(%r12)
	testb	$1, 372(%r12)
	jne	.L889
.L806:
	movl	$0, 372(%r12)
	testb	$1, 376(%r12)
	jne	.L890
.L807:
	movl	$0, 376(%r12)
	testb	$1, 380(%r12)
	jne	.L891
.L809:
	movl	$291, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	movl	$0, 380(%r12)
	call	CRYPTO_free@PLT
.L873:
	addq	$8, %rsp
	xorl	%esi, %esi
	leaq	err_thread_local(%rip), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_THREAD_set_local@PLT
.L876:
	.cfi_restore_state
	movq	192(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 192(%r12)
	jmp	.L793
.L891:
	movq	312(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 312(%r12)
	jmp	.L809
.L890:
	movq	304(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 304(%r12)
	jmp	.L807
.L889:
	movq	296(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 296(%r12)
	jmp	.L806
.L888:
	movq	288(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 288(%r12)
	jmp	.L805
.L887:
	movq	280(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 280(%r12)
	jmp	.L804
.L886:
	movq	272(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 272(%r12)
	jmp	.L803
.L885:
	movq	264(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 264(%r12)
	jmp	.L802
.L884:
	movq	256(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 256(%r12)
	jmp	.L801
.L883:
	movq	248(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 248(%r12)
	jmp	.L800
.L882:
	movq	240(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 240(%r12)
	jmp	.L799
.L881:
	movq	232(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 232(%r12)
	jmp	.L798
.L880:
	movq	224(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 224(%r12)
	jmp	.L797
.L879:
	movq	216(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 216(%r12)
	jmp	.L796
.L878:
	movq	208(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 208(%r12)
	jmp	.L795
.L877:
	movq	200(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 200(%r12)
	jmp	.L794
	.cfi_endproc
.LFE340:
	.size	ERR_set_error_data, .-ERR_set_error_data
	.section	.rodata.str1.1
.LC9:
	.string	"<NULL>"
	.text
	.p2align 4
	.globl	ERR_add_error_vdata
	.type	ERR_add_error_vdata, @function
ERR_add_error_vdata:
.LFB342:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$852, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	leaq	.LC6(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edi, %ebx
	subq	$24, %rsp
	movl	%edi, -56(%rbp)
	movl	$81, %edi
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L892
	movb	$0, (%rax)
	movq	%rax, %r15
	testl	%ebx, %ebx
	jle	.L894
	movl	$80, -52(%rbp)
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	jmp	.L899
	.p2align 4,,10
	.p2align 3
.L914:
	movl	%eax, %edx
	addl	$8, %eax
	addq	16(%r12), %rdx
	movl	%eax, (%r12)
	movq	(%rdx), %r14
	testq	%r14, %r14
	je	.L902
.L915:
	movq	%r14, %rdi
	call	strlen@PLT
.L897:
	addl	%eax, %ebx
	cmpl	-52(%rbp), %ebx
	jle	.L898
	leal	21(%rbx), %esi
	leal	20(%rbx), %eax
	movl	$866, %ecx
	movq	%r15, %rdi
	movslq	%esi, %rsi
	leaq	.LC6(%rip), %rdx
	movl	%eax, -52(%rbp)
	call	CRYPTO_realloc@PLT
	testq	%rax, %rax
	je	.L913
	movq	%rax, %r15
.L898:
	movslq	-52(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	addl	$1, %r13d
	addq	$1, %rdx
	call	OPENSSL_strlcat@PLT
	cmpl	%r13d, -56(%rbp)
	je	.L894
.L899:
	movl	(%r12), %eax
	cmpl	$47, %eax
	jbe	.L914
	movq	8(%r12), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r12)
	movq	(%rdx), %r14
	testq	%r14, %r14
	jne	.L915
.L902:
	movl	$6, %eax
	leaq	.LC9(%rip), %r14
	jmp	.L897
	.p2align 4,,10
	.p2align 3
.L894:
	call	ERR_get_state
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L916
	movslq	576(%rax), %r12
	leaq	(%rax,%r12,4), %r13
	testb	$1, 320(%r13)
	jne	.L917
.L901:
	movq	%r15, 192(%rbx,%r12,8)
	movl	$3, 320(%r13)
.L892:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L913:
	.cfi_restore_state
	movl	$868, %edx
.L912:
	addq	$24, %rsp
	movq	%r15, %rdi
	leaq	.LC6(%rip), %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
.L916:
	.cfi_restore_state
	movl	$876, %edx
	jmp	.L912
	.p2align 4,,10
	.p2align 3
.L917:
	movq	192(%rax,%r12,8), %rdi
	movl	$822, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L901
	.cfi_endproc
.LFE342:
	.size	ERR_add_error_vdata, .-ERR_add_error_vdata
	.p2align 4
	.globl	ERR_add_error_data
	.type	ERR_add_error_data, @function
ERR_add_error_data:
.LFB341:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$208, %rsp
	movq	%rsi, -168(%rbp)
	movq	%rdx, -160(%rbp)
	movq	%rcx, -152(%rbp)
	movq	%r8, -144(%rbp)
	movq	%r9, -136(%rbp)
	testb	%al, %al
	je	.L919
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm4, -64(%rbp)
	movaps	%xmm5, -48(%rbp)
	movaps	%xmm6, -32(%rbp)
	movaps	%xmm7, -16(%rbp)
.L919:
	movq	%fs:40, %rax
	movq	%rax, -184(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	leaq	-208(%rbp), %rsi
	movl	$8, -208(%rbp)
	movq	%rax, -200(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rax, -192(%rbp)
	movl	$48, -204(%rbp)
	call	ERR_add_error_vdata
	movq	-184(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L922
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L922:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE341:
	.size	ERR_add_error_data, .-ERR_add_error_data
	.p2align 4
	.globl	ERR_set_mark
	.type	ERR_set_mark, @function
ERR_set_mark:
.LFB343:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	call	__errno_location@PLT
	xorl	%esi, %esi
	movl	$262144, %edi
	movl	(%rax), %r13d
	movq	%rax, %rbx
	call	OPENSSL_init_crypto@PLT
	testl	%eax, %eax
	je	.L1011
	leaq	err_do_init_ossl_(%rip), %rsi
	leaq	err_init(%rip), %rdi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L1011
	movl	err_do_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	je	.L1011
	leaq	err_thread_local(%rip), %rdi
	call	CRYPTO_THREAD_get_local@PLT
	movq	%rax, %r12
	cmpq	$-1, %rax
	je	.L1011
	testq	%rax, %rax
	je	.L1012
.L927:
	movl	%r13d, (%rbx)
	movslq	576(%r12), %rax
	cmpl	%eax, 580(%r12)
	je	.L1011
	orl	$1, (%r12,%rax,4)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L947:
	.cfi_restore_state
	testb	$1, 320(%r12)
	jne	.L1013
.L930:
	movl	$0, 320(%r12)
	testb	$1, 324(%r12)
	jne	.L1014
.L931:
	movl	$0, 324(%r12)
	testb	$1, 328(%r12)
	jne	.L1015
.L932:
	movl	$0, 328(%r12)
	testb	$1, 332(%r12)
	jne	.L1016
.L933:
	movl	$0, 332(%r12)
	testb	$1, 336(%r12)
	jne	.L1017
.L934:
	movl	$0, 336(%r12)
	testb	$1, 340(%r12)
	jne	.L1018
.L935:
	movl	$0, 340(%r12)
	testb	$1, 344(%r12)
	jne	.L1019
.L936:
	movl	$0, 344(%r12)
	testb	$1, 348(%r12)
	jne	.L1020
.L937:
	movl	$0, 348(%r12)
	testb	$1, 352(%r12)
	jne	.L1021
.L938:
	movl	$0, 352(%r12)
	testb	$1, 356(%r12)
	jne	.L1022
.L939:
	movl	$0, 356(%r12)
	testb	$1, 360(%r12)
	jne	.L1023
.L940:
	movl	$0, 360(%r12)
	testb	$1, 364(%r12)
	jne	.L1024
.L941:
	movl	$0, 364(%r12)
	testb	$1, 368(%r12)
	jne	.L1025
.L942:
	movl	$0, 368(%r12)
	testb	$1, 372(%r12)
	jne	.L1026
.L943:
	movl	$0, 372(%r12)
	testb	$1, 376(%r12)
	jne	.L1027
.L944:
	movl	$0, 376(%r12)
	testb	$1, 380(%r12)
	jne	.L1028
.L946:
	movl	$291, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	movl	$0, 380(%r12)
	call	CRYPTO_free@PLT
.L1010:
	xorl	%esi, %esi
	leaq	err_thread_local(%rip), %rdi
	call	CRYPTO_THREAD_set_local@PLT
.L1011:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1012:
	.cfi_restore_state
	movq	$-1, %rsi
	leaq	err_thread_local(%rip), %rdi
	call	CRYPTO_THREAD_set_local@PLT
	testl	%eax, %eax
	je	.L1011
	movl	$734, %edx
	leaq	.LC6(%rip), %rsi
	movl	$584, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1010
	movl	$2, %edi
	call	ossl_init_thread_start@PLT
	testl	%eax, %eax
	je	.L947
	movq	%r12, %rsi
	leaq	err_thread_local(%rip), %rdi
	call	CRYPTO_THREAD_set_local@PLT
	testl	%eax, %eax
	je	.L947
	xorl	%esi, %esi
	movl	$2, %edi
	call	OPENSSL_init_crypto@PLT
	jmp	.L927
.L1013:
	movq	192(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 192(%r12)
	jmp	.L930
.L1028:
	movq	312(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 312(%r12)
	jmp	.L946
.L1027:
	movq	304(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 304(%r12)
	jmp	.L944
.L1026:
	movq	296(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 296(%r12)
	jmp	.L943
.L1025:
	movq	288(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 288(%r12)
	jmp	.L942
.L1024:
	movq	280(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 280(%r12)
	jmp	.L941
.L1023:
	movq	272(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 272(%r12)
	jmp	.L940
.L1022:
	movq	264(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 264(%r12)
	jmp	.L939
.L1021:
	movq	256(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 256(%r12)
	jmp	.L938
.L1020:
	movq	248(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 248(%r12)
	jmp	.L937
.L1019:
	movq	240(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 240(%r12)
	jmp	.L936
.L1018:
	movq	232(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 232(%r12)
	jmp	.L935
.L1017:
	movq	224(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 224(%r12)
	jmp	.L934
.L1016:
	movq	216(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 216(%r12)
	jmp	.L933
.L1015:
	movq	208(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 208(%r12)
	jmp	.L932
.L1014:
	movq	200(%r12), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 200(%r12)
	jmp	.L931
	.cfi_endproc
.LFE343:
	.size	ERR_set_mark, .-ERR_set_mark
	.p2align 4
	.globl	ERR_pop_to_mark
	.type	ERR_pop_to_mark, @function
ERR_pop_to_mark:
.LFB344:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	call	ERR_get_state
	testq	%rax, %rax
	je	.L1030
	movl	576(%rax), %edx
	movq	%rax, %rbx
	cmpl	580(%rax), %edx
	je	.L1030
	.p2align 4,,10
	.p2align 3
.L1031:
	movslq	%edx, %rdx
	leaq	(%rbx,%rdx,4), %rcx
	movl	(%rcx), %eax
	movl	%eax, %r8d
	andl	$1, %r8d
	jne	.L1045
	testb	$1, 320(%rcx)
	jne	.L1032
	movslq	576(%rbx), %rcx
	movq	%rcx, %rdx
.L1033:
	leaq	(%rbx,%rcx,4), %rax
	leaq	(%rbx,%rcx,8), %rcx
	movl	$0, 320(%rax)
	movl	$0, (%rax)
	movq	$0, 64(%rcx)
	movq	$0, 384(%rcx)
	movl	$-1, 512(%rax)
	testl	%edx, %edx
	jle	.L1034
	subl	$1, %edx
	movl	%edx, 576(%rbx)
	cmpl	580(%rbx), %edx
	jne	.L1031
.L1030:
	addq	$8, %rsp
	xorl	%r8d, %r8d
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1034:
	.cfi_restore_state
	cmpl	$15, 580(%rbx)
	movl	$15, 576(%rbx)
	je	.L1030
	movl	$15, %edx
	jmp	.L1031
	.p2align 4,,10
	.p2align 3
.L1032:
	movq	192(%rbx,%rdx,8), %rdi
	leaq	.LC6(%rip), %rsi
	movl	$903, %edx
	call	CRYPTO_free@PLT
	movslq	576(%rbx), %rcx
	movq	$0, 192(%rbx,%rcx,8)
	movq	%rcx, %rdx
	jmp	.L1033
	.p2align 4,,10
	.p2align 3
.L1045:
	andl	$-2, %eax
	movl	%eax, (%rbx,%rdx,4)
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE344:
	.size	ERR_pop_to_mark, .-ERR_pop_to_mark
	.p2align 4
	.globl	ERR_clear_last_mark
	.type	ERR_clear_last_mark, @function
ERR_clear_last_mark:
.LFB345:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_get_state
	testq	%rax, %rax
	je	.L1055
	movl	576(%rax), %ecx
	movl	580(%rax), %esi
	cmpl	%esi, %ecx
	je	.L1055
	cmpl	$15, %esi
	je	.L1067
.L1048:
	movslq	%ecx, %rdi
	movl	(%rax,%rdi,4), %edx
	testb	$1, %dl
	jne	.L1049
	testl	%ecx, %ecx
	jg	.L1052
	movl	60(%rax), %edx
	movl	%edx, %r8d
	andl	$1, %r8d
	jne	.L1056
	cmpl	$14, %esi
	je	.L1046
	movl	56(%rax), %edx
	testb	$1, %dl
	jne	.L1068
	movl	$14, %ecx
.L1052:
	subl	$1, %ecx
	cmpl	%ecx, %esi
	jne	.L1048
	.p2align 4,,10
	.p2align 3
.L1055:
	xorl	%r8d, %r8d
.L1046:
	movl	%r8d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1070:
	.cfi_restore_state
	movslq	%esi, %rdi
.L1049:
	andl	$-2, %edx
	movl	$1, %r8d
	movl	%edx, (%rax,%rdi,4)
.L1069:
	movl	%r8d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1056:
	.cfi_restore_state
	movl	$15, %edi
	andl	$-2, %edx
	movl	$1, %r8d
	movl	%edx, (%rax,%rdi,4)
	jmp	.L1069
.L1067:
	movslq	%ecx, %rsi
.L1050:
	movl	(%rax,%rsi,4), %edx
	movl	%edx, %r8d
	andl	$1, %r8d
	jne	.L1070
	testl	%esi, %esi
	jle	.L1046
	subq	$1, %rsi
	cmpl	$15, %esi
	jne	.L1050
	jmp	.L1046
	.p2align 4,,10
	.p2align 3
.L1068:
	movl	$14, %edi
	jmp	.L1049
	.cfi_endproc
.LFE345:
	.size	ERR_clear_last_mark, .-ERR_clear_last_mark
	.p2align 4
	.globl	err_clear_last_constant_time
	.type	err_clear_last_constant_time, @function
err_clear_last_constant_time:
.LFB346:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	%edi, %ebx
	call	__errno_location@PLT
	xorl	%esi, %esi
	movl	$262144, %edi
	movl	(%rax), %r14d
	movq	%rax, %r12
	call	OPENSSL_init_crypto@PLT
	testl	%eax, %eax
	je	.L1071
	leaq	err_do_init_ossl_(%rip), %rsi
	leaq	err_init(%rip), %rdi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L1071
	movl	err_do_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	je	.L1071
	leaq	err_thread_local(%rip), %rdi
	call	CRYPTO_THREAD_get_local@PLT
	movq	%rax, %r13
	cmpq	$-1, %rax
	je	.L1071
	testq	%rax, %rax
	je	.L1159
.L1073:
	movl	%ebx, %edx
	subl	$1, %ebx
	movl	%r14d, (%r12)
	movslq	576(%r13), %rax
	notl	%edx
	andl	%ebx, %edx
	sarl	$31, %edx
	notl	%edx
	andl	$2, %edx
	orl	%edx, 0(%r13,%rax,4)
.L1071:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1159:
	.cfi_restore_state
	movq	$-1, %rsi
	leaq	err_thread_local(%rip), %rdi
	call	CRYPTO_THREAD_set_local@PLT
	testl	%eax, %eax
	je	.L1071
	movl	$734, %edx
	leaq	.LC6(%rip), %rsi
	movl	$584, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1158
	movl	$2, %edi
	call	ossl_init_thread_start@PLT
	testl	%eax, %eax
	je	.L1093
	movq	%r13, %rsi
	leaq	err_thread_local(%rip), %rdi
	call	CRYPTO_THREAD_set_local@PLT
	testl	%eax, %eax
	je	.L1093
	xorl	%esi, %esi
	movl	$2, %edi
	call	OPENSSL_init_crypto@PLT
	jmp	.L1073
	.p2align 4,,10
	.p2align 3
.L1093:
	testb	$1, 320(%r13)
	jne	.L1160
.L1076:
	movl	$0, 320(%r13)
	testb	$1, 324(%r13)
	jne	.L1161
.L1077:
	movl	$0, 324(%r13)
	testb	$1, 328(%r13)
	jne	.L1162
.L1078:
	movl	$0, 328(%r13)
	testb	$1, 332(%r13)
	jne	.L1163
.L1079:
	movl	$0, 332(%r13)
	testb	$1, 336(%r13)
	jne	.L1164
.L1080:
	movl	$0, 336(%r13)
	testb	$1, 340(%r13)
	jne	.L1165
.L1081:
	movl	$0, 340(%r13)
	testb	$1, 344(%r13)
	jne	.L1166
.L1082:
	movl	$0, 344(%r13)
	testb	$1, 348(%r13)
	jne	.L1167
.L1083:
	movl	$0, 348(%r13)
	testb	$1, 352(%r13)
	jne	.L1168
.L1084:
	movl	$0, 352(%r13)
	testb	$1, 356(%r13)
	jne	.L1169
.L1085:
	movl	$0, 356(%r13)
	testb	$1, 360(%r13)
	jne	.L1170
.L1086:
	movl	$0, 360(%r13)
	testb	$1, 364(%r13)
	jne	.L1171
.L1087:
	movl	$0, 364(%r13)
	testb	$1, 368(%r13)
	jne	.L1172
.L1088:
	movl	$0, 368(%r13)
	testb	$1, 372(%r13)
	jne	.L1173
.L1089:
	movl	$0, 372(%r13)
	testb	$1, 376(%r13)
	jne	.L1174
.L1090:
	movl	$0, 376(%r13)
	testb	$1, 380(%r13)
	jne	.L1175
.L1092:
	movl	$291, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r13, %rdi
	movl	$0, 380(%r13)
	call	CRYPTO_free@PLT
.L1158:
	popq	%rbx
	xorl	%esi, %esi
	leaq	err_thread_local(%rip), %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_THREAD_set_local@PLT
.L1160:
	.cfi_restore_state
	movq	192(%r13), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 192(%r13)
	jmp	.L1076
.L1175:
	movq	312(%r13), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 312(%r13)
	jmp	.L1092
.L1174:
	movq	304(%r13), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 304(%r13)
	jmp	.L1090
.L1173:
	movq	296(%r13), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 296(%r13)
	jmp	.L1089
.L1172:
	movq	288(%r13), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 288(%r13)
	jmp	.L1088
.L1171:
	movq	280(%r13), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 280(%r13)
	jmp	.L1087
.L1170:
	movq	272(%r13), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 272(%r13)
	jmp	.L1086
.L1169:
	movq	264(%r13), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 264(%r13)
	jmp	.L1085
.L1168:
	movq	256(%r13), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 256(%r13)
	jmp	.L1084
.L1167:
	movq	248(%r13), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 248(%r13)
	jmp	.L1083
.L1166:
	movq	240(%r13), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 240(%r13)
	jmp	.L1082
.L1165:
	movq	232(%r13), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 232(%r13)
	jmp	.L1081
.L1164:
	movq	224(%r13), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 224(%r13)
	jmp	.L1080
.L1163:
	movq	216(%r13), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 216(%r13)
	jmp	.L1079
.L1162:
	movq	208(%r13), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 208(%r13)
	jmp	.L1078
.L1161:
	movq	200(%r13), %rdi
	movl	$289, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 200(%r13)
	jmp	.L1077
	.cfi_endproc
.LFE346:
	.size	err_clear_last_constant_time, .-err_clear_last_constant_time
	.local	buf.7413
	.comm	buf.7413,256,32
	.data
	.align 4
	.type	init.7262, @object
	.size	init.7262, 4
init.7262:
	.long	1
	.local	strerror_pool.7259
	.comm	strerror_pool.7259,8192,32
	.local	err_do_init_ossl_ret_
	.comm	err_do_init_ossl_ret_,4,4
	.local	do_err_strings_init_ossl_ret_
	.comm	do_err_strings_init_ossl_ret_,4,4
	.local	SYS_str_reasons
	.comm	SYS_str_reasons,2048,32
	.align 4
	.type	int_err_library_number, @object
	.size	int_err_library_number, 4
int_err_library_number:
	.long	128
	.local	int_error_hash
	.comm	int_error_hash,8,8
	.local	err_string_lock
	.comm	err_string_lock,8,8
	.local	err_string_init
	.comm	err_string_init,4,4
	.local	err_thread_local
	.comm	err_thread_local,4,4
	.local	set_err_thread_local
	.comm	set_err_thread_local,4,4
	.local	err_init
	.comm	err_init,4,4
	.section	.rodata.str1.1
.LC10:
	.string	"system lib"
.LC11:
	.string	"BN lib"
.LC12:
	.string	"RSA lib"
.LC13:
	.string	"DH lib"
.LC14:
	.string	"EVP lib"
.LC15:
	.string	"BUF lib"
.LC16:
	.string	"OBJ lib"
.LC17:
	.string	"PEM lib"
.LC18:
	.string	"DSA lib"
.LC19:
	.string	"X509 lib"
.LC20:
	.string	"ASN1 lib"
.LC21:
	.string	"EC lib"
.LC22:
	.string	"BIO lib"
.LC23:
	.string	"PKCS7 lib"
.LC24:
	.string	"X509V3 lib"
.LC25:
	.string	"ENGINE lib"
.LC26:
	.string	"UI lib"
.LC27:
	.string	"STORE lib"
.LC28:
	.string	"ECDSA lib"
.LC29:
	.string	"nested asn1 error"
.LC30:
	.string	"missing asn1 eos"
.LC31:
	.string	"fatal"
.LC32:
	.string	"malloc failure"
	.section	.rodata.str1.8
	.align 8
.LC33:
	.string	"called a function you should not call"
	.section	.rodata.str1.1
.LC34:
	.string	"passed a null parameter"
.LC35:
	.string	"internal error"
	.section	.rodata.str1.8
	.align 8
.LC36:
	.string	"called a function that was disabled at compile-time"
	.section	.rodata.str1.1
.LC37:
	.string	"init fail"
.LC38:
	.string	"operation fail"
	.section	.data.rel.local,"aw"
	.align 32
	.type	ERR_str_reasons, @object
	.size	ERR_str_reasons, 480
ERR_str_reasons:
	.quad	2
	.quad	.LC10
	.quad	3
	.quad	.LC11
	.quad	4
	.quad	.LC12
	.quad	5
	.quad	.LC13
	.quad	6
	.quad	.LC14
	.quad	7
	.quad	.LC15
	.quad	8
	.quad	.LC16
	.quad	9
	.quad	.LC17
	.quad	10
	.quad	.LC18
	.quad	11
	.quad	.LC19
	.quad	13
	.quad	.LC20
	.quad	16
	.quad	.LC21
	.quad	32
	.quad	.LC22
	.quad	33
	.quad	.LC23
	.quad	34
	.quad	.LC24
	.quad	38
	.quad	.LC25
	.quad	40
	.quad	.LC26
	.quad	44
	.quad	.LC27
	.quad	42
	.quad	.LC28
	.quad	58
	.quad	.LC29
	.quad	63
	.quad	.LC30
	.quad	64
	.quad	.LC31
	.quad	65
	.quad	.LC32
	.quad	66
	.quad	.LC33
	.quad	67
	.quad	.LC34
	.quad	68
	.quad	.LC35
	.quad	69
	.quad	.LC36
	.quad	70
	.quad	.LC37
	.quad	72
	.quad	.LC38
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC39:
	.string	"fopen"
.LC40:
	.string	"connect"
.LC41:
	.string	"getservbyname"
.LC42:
	.string	"socket"
.LC43:
	.string	"ioctlsocket"
.LC44:
	.string	"bind"
.LC45:
	.string	"listen"
.LC46:
	.string	"accept"
.LC47:
	.string	"opendir"
.LC48:
	.string	"fread"
.LC49:
	.string	"getaddrinfo"
.LC50:
	.string	"getnameinfo"
.LC51:
	.string	"setsockopt"
.LC52:
	.string	"getsockopt"
.LC53:
	.string	"getsockname"
.LC54:
	.string	"gethostbyname"
.LC55:
	.string	"fflush"
.LC56:
	.string	"open"
.LC57:
	.string	"close"
.LC58:
	.string	"ioctl"
.LC59:
	.string	"stat"
.LC60:
	.string	"fcntl"
.LC61:
	.string	"fstat"
	.section	.data.rel.local
	.align 32
	.type	ERR_str_functs, @object
	.size	ERR_str_functs, 384
ERR_str_functs:
	.quad	4096
	.quad	.LC39
	.quad	8192
	.quad	.LC40
	.quad	12288
	.quad	.LC41
	.quad	16384
	.quad	.LC42
	.quad	20480
	.quad	.LC43
	.quad	24576
	.quad	.LC44
	.quad	28672
	.quad	.LC45
	.quad	32768
	.quad	.LC46
	.quad	40960
	.quad	.LC47
	.quad	45056
	.quad	.LC48
	.quad	49152
	.quad	.LC49
	.quad	53248
	.quad	.LC50
	.quad	57344
	.quad	.LC51
	.quad	61440
	.quad	.LC52
	.quad	65536
	.quad	.LC53
	.quad	69632
	.quad	.LC54
	.quad	73728
	.quad	.LC55
	.quad	77824
	.quad	.LC56
	.quad	81920
	.quad	.LC57
	.quad	86016
	.quad	.LC58
	.quad	90112
	.quad	.LC59
	.quad	94208
	.quad	.LC60
	.quad	98304
	.quad	.LC61
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC62:
	.string	"unknown library"
.LC63:
	.string	"system library"
.LC64:
	.string	"bignum routines"
.LC65:
	.string	"rsa routines"
.LC66:
	.string	"Diffie-Hellman routines"
.LC67:
	.string	"digital envelope routines"
.LC68:
	.string	"memory buffer routines"
.LC69:
	.string	"object identifier routines"
.LC70:
	.string	"PEM routines"
.LC71:
	.string	"dsa routines"
.LC72:
	.string	"x509 certificate routines"
.LC73:
	.string	"asn1 encoding routines"
.LC74:
	.string	"configuration file routines"
.LC75:
	.string	"common libcrypto routines"
.LC76:
	.string	"elliptic curve routines"
.LC77:
	.string	"ECDSA routines"
.LC78:
	.string	"ECDH routines"
.LC79:
	.string	"SSL routines"
.LC80:
	.string	"BIO routines"
.LC81:
	.string	"PKCS7 routines"
.LC82:
	.string	"X509 V3 routines"
.LC83:
	.string	"PKCS12 routines"
.LC84:
	.string	"random number generator"
.LC85:
	.string	"DSO support routines"
.LC86:
	.string	"time stamp routines"
.LC87:
	.string	"engine routines"
.LC88:
	.string	"OCSP routines"
.LC89:
	.string	"UI routines"
.LC90:
	.string	"FIPS routines"
.LC91:
	.string	"CMS routines"
.LC92:
	.string	"HMAC routines"
.LC93:
	.string	"CT routines"
.LC94:
	.string	"ASYNC routines"
.LC95:
	.string	"KDF routines"
.LC96:
	.string	"STORE routines"
.LC97:
	.string	"SM2 routines"
	.section	.data.rel.local
	.align 32
	.type	ERR_str_libraries, @object
	.size	ERR_str_libraries, 592
ERR_str_libraries:
	.quad	16777216
	.quad	.LC62
	.quad	33554432
	.quad	.LC63
	.quad	50331648
	.quad	.LC64
	.quad	67108864
	.quad	.LC65
	.quad	83886080
	.quad	.LC66
	.quad	100663296
	.quad	.LC67
	.quad	117440512
	.quad	.LC68
	.quad	134217728
	.quad	.LC69
	.quad	150994944
	.quad	.LC70
	.quad	167772160
	.quad	.LC71
	.quad	184549376
	.quad	.LC72
	.quad	218103808
	.quad	.LC73
	.quad	234881024
	.quad	.LC74
	.quad	251658240
	.quad	.LC75
	.quad	268435456
	.quad	.LC76
	.quad	704643072
	.quad	.LC77
	.quad	721420288
	.quad	.LC78
	.quad	335544320
	.quad	.LC79
	.quad	536870912
	.quad	.LC80
	.quad	553648128
	.quad	.LC81
	.quad	570425344
	.quad	.LC82
	.quad	587202560
	.quad	.LC83
	.quad	603979776
	.quad	.LC84
	.quad	620756992
	.quad	.LC85
	.quad	788529152
	.quad	.LC86
	.quad	637534208
	.quad	.LC87
	.quad	654311424
	.quad	.LC88
	.quad	671088640
	.quad	.LC89
	.quad	754974720
	.quad	.LC90
	.quad	771751936
	.quad	.LC91
	.quad	805306368
	.quad	.LC92
	.quad	838860800
	.quad	.LC93
	.quad	855638016
	.quad	.LC94
	.quad	872415232
	.quad	.LC95
	.quad	738197504
	.quad	.LC96
	.quad	889192448
	.quad	.LC97
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
