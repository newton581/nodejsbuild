	.file	"v3_lib.c"
	.text
	.p2align 4
	.type	ext_cmp, @function
ext_cmp:
.LFB1297:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rsi), %rdx
	movl	(%rax), %eax
	subl	(%rdx), %eax
	ret
	.cfi_endproc
.LFE1297:
	.size	ext_cmp, .-ext_cmp
	.p2align 4
	.type	ext_cmp_BSEARCH_CMP_FN, @function
ext_cmp_BSEARCH_CMP_FN:
.LFB1298:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rsi), %rdx
	movl	(%rax), %eax
	subl	(%rdx), %eax
	ret
	.cfi_endproc
.LFE1298:
	.size	ext_cmp_BSEARCH_CMP_FN, .-ext_cmp_BSEARCH_CMP_FN
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/x509v3/v3_lib.c"
	.text
	.p2align 4
	.type	ext_list_free, @function
ext_list_free:
.LFB1305:
	.cfi_startproc
	endbr64
	testb	$1, 4(%rdi)
	jne	.L6
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$114, %edx
	leaq	.LC0(%rip), %rsi
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE1305:
	.size	ext_list_free, .-ext_list_free
	.p2align 4
	.globl	X509V3_EXT_add
	.type	X509V3_EXT_add, @function
X509V3_EXT_add:
.LFB1296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	ext_list(%rip), %rdi
	testq	%rdi, %rdi
	je	.L12
.L8:
	movq	%r12, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L13
	movl	$1, %eax
.L7:
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	leaq	ext_cmp(%rip), %rdi
	call	OPENSSL_sk_new@PLT
	movq	%rax, ext_list(%rip)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L8
	movl	$29, %r8d
	movl	$65, %edx
	movl	$104, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$33, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	%eax, -20(%rbp)
	movl	$104, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	movl	-20(%rbp), %eax
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1296:
	.size	X509V3_EXT_add, .-X509V3_EXT_add
	.p2align 4
	.globl	X509V3_EXT_get_nid
	.type	X509V3_EXT_get_nid, @function
X509V3_EXT_get_nid:
.LFB1300:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-128(%rbp), %r12
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%r12, -136(%rbp)
	testl	%edi, %edi
	js	.L18
	movl	%edi, -128(%rbp)
	movl	$8, %ecx
	leaq	-136(%rbp), %rdi
	leaq	ext_cmp_BSEARCH_CMP_FN(%rip), %r8
	movl	$47, %edx
	leaq	standard_exts(%rip), %rsi
	call	OBJ_bsearch_@PLT
	testq	%rax, %rax
	je	.L17
	movq	(%rax), %rax
.L14:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L24
	addq	$136, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movq	ext_list(%rip), %rdi
	testq	%rdi, %rdi
	je	.L18
	movq	%r12, %rsi
	call	OPENSSL_sk_find@PLT
	movq	ext_list(%rip), %rdi
	movl	%eax, %esi
	call	OPENSSL_sk_value@PLT
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L18:
	xorl	%eax, %eax
	jmp	.L14
.L24:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1300:
	.size	X509V3_EXT_get_nid, .-X509V3_EXT_get_nid
	.p2align 4
	.globl	X509V3_EXT_get
	.type	X509V3_EXT_get, @function
X509V3_EXT_get:
.LFB1301:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$136, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	X509_EXTENSION_get_object@PLT
	movq	%rax, %rdi
	call	OBJ_obj2nid@PLT
	xorl	%r8d, %r8d
	testl	%eax, %eax
	je	.L25
	leaq	-128(%rbp), %r12
	movq	%r12, -136(%rbp)
	js	.L30
	leaq	-136(%rbp), %rdi
	leaq	ext_cmp_BSEARCH_CMP_FN(%rip), %r8
	movl	%eax, -128(%rbp)
	movl	$8, %ecx
	movl	$47, %edx
	leaq	standard_exts(%rip), %rsi
	call	OBJ_bsearch_@PLT
	testq	%rax, %rax
	je	.L29
	movq	(%rax), %r8
.L25:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L38
	addq	$136, %rsp
	movq	%r8, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movq	ext_list(%rip), %rdi
	testq	%rdi, %rdi
	je	.L30
	movq	%r12, %rsi
	call	OPENSSL_sk_find@PLT
	movq	ext_list(%rip), %rdi
	movl	%eax, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r8
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L30:
	xorl	%r8d, %r8d
	jmp	.L25
.L38:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1301:
	.size	X509V3_EXT_get, .-X509V3_EXT_get
	.p2align 4
	.globl	X509V3_EXT_add_list
	.type	X509V3_EXT_add_list, @function
X509V3_EXT_add_list:
.LFB1302:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	ext_cmp(%rip), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpl	$-1, (%rdi)
	jne	.L40
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L42:
	movq	%rbx, %rsi
	call	OPENSSL_sk_push@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L49
	addq	$104, %rbx
	cmpl	$-1, (%rbx)
	je	.L44
.L40:
	movq	ext_list(%rip), %rdi
	testq	%rdi, %rdi
	jne	.L42
	movq	%r13, %rdi
	call	OPENSSL_sk_new@PLT
	movq	%rax, ext_list(%rip)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L42
	movl	$29, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$104, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	movl	$33, %r8d
	movl	$65, %edx
	movl	$104, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$1, %r12d
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1302:
	.size	X509V3_EXT_add_list, .-X509V3_EXT_add_list
	.p2align 4
	.globl	X509V3_EXT_add_alias
	.type	X509V3_EXT_add_alias, @function
X509V3_EXT_add_alias:
.LFB1303:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-144(%rbp), %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%r12, -152(%rbp)
	testl	%esi, %esi
	js	.L52
	movl	%esi, -144(%rbp)
	movl	%edi, %r13d
	movl	$8, %ecx
	leaq	-152(%rbp), %rdi
	leaq	ext_cmp_BSEARCH_CMP_FN(%rip), %r8
	movl	$47, %edx
	leaq	standard_exts(%rip), %rsi
	call	OBJ_bsearch_@PLT
	testq	%rax, %rax
	je	.L53
	movq	(%rax), %rbx
.L54:
	testq	%rbx, %rbx
	je	.L52
	movl	$95, %edx
	leaq	.LC0(%rip), %rsi
	movl	$104, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L66
	movdqu	(%rbx), %xmm0
	movq	ext_list(%rip), %rdi
	movups	%xmm0, (%rax)
	movdqu	16(%rbx), %xmm1
	movups	%xmm1, 16(%rax)
	movdqu	32(%rbx), %xmm2
	movups	%xmm2, 32(%rax)
	movdqu	48(%rbx), %xmm3
	movups	%xmm3, 48(%rax)
	movdqu	64(%rbx), %xmm4
	movups	%xmm4, 64(%rax)
	movdqu	80(%rbx), %xmm5
	movups	%xmm5, 80(%rax)
	movq	96(%rbx), %rax
	orl	$1, 4(%r12)
	movq	%rax, 96(%r12)
	movl	%r13d, (%r12)
	testq	%rdi, %rdi
	je	.L67
.L59:
	movq	%r12, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L68
	movl	$1, %eax
.L50:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L69
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movq	ext_list(%rip), %rdi
	testq	%rdi, %rdi
	je	.L52
	movq	%r12, %rsi
	call	OPENSSL_sk_find@PLT
	movq	ext_list(%rip), %rdi
	movl	%eax, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rbx
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L67:
	leaq	ext_cmp(%rip), %rdi
	call	OPENSSL_sk_new@PLT
	movq	%rax, ext_list(%rip)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L59
	movl	$29, %r8d
	movl	$65, %edx
	movl	$104, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L52:
	movl	$92, %r8d
	movl	$102, %edx
	movl	$106, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L66:
	movl	$96, %r8d
	movl	$65, %edx
	movl	$106, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L68:
	movl	$33, %r8d
	movl	$65, %edx
	movl	$104, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	movl	%eax, -164(%rbp)
	call	ERR_put_error@PLT
	movl	-164(%rbp), %eax
	jmp	.L50
.L69:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1303:
	.size	X509V3_EXT_add_alias, .-X509V3_EXT_add_alias
	.p2align 4
	.globl	X509V3_EXT_cleanup
	.type	X509V3_EXT_cleanup, @function
X509V3_EXT_cleanup:
.LFB1304:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	ext_list(%rip), %rdi
	leaq	ext_list_free(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	OPENSSL_sk_pop_free@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	$0, ext_list(%rip)
	ret
	.cfi_endproc
.LFE1304:
	.size	X509V3_EXT_cleanup, .-X509V3_EXT_cleanup
	.p2align 4
	.globl	X509V3_add_standard_extensions
	.type	X509V3_add_standard_extensions, @function
X509V3_add_standard_extensions:
.LFB1306:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1306:
	.size	X509V3_add_standard_extensions, .-X509V3_add_standard_extensions
	.p2align 4
	.globl	X509V3_EXT_d2i
	.type	X509V3_EXT_d2i, @function
X509V3_EXT_d2i:
.LFB1307:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	X509_EXTENSION_get_object@PLT
	movq	%rax, %rdi
	call	OBJ_obj2nid@PLT
	testl	%eax, %eax
	je	.L79
	leaq	-144(%rbp), %r12
	movq	%r12, -152(%rbp)
	js	.L79
	movl	$8, %ecx
	movl	$47, %edx
	leaq	-152(%rbp), %rdi
	movl	%eax, -144(%rbp)
	leaq	ext_cmp_BSEARCH_CMP_FN(%rip), %r8
	leaq	standard_exts(%rip), %rsi
	call	OBJ_bsearch_@PLT
	testq	%rax, %rax
	je	.L77
	movq	(%rax), %rbx
.L78:
	testq	%rbx, %rbx
	je	.L79
	movq	%r13, %rdi
	call	X509_EXTENSION_get_data@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	ASN1_STRING_get0_data@PLT
	movq	%r13, %rdi
	movq	%rax, -144(%rbp)
	call	ASN1_STRING_length@PLT
	movq	8(%rbx), %rcx
	movq	%r12, %rsi
	xorl	%edi, %edi
	movslq	%eax, %rdx
	testq	%rcx, %rcx
	jne	.L90
	call	*32(%rbx)
.L73:
	movq	-40(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L91
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	movq	ext_list(%rip), %rdi
	testq	%rdi, %rdi
	je	.L79
	movq	%r12, %rsi
	call	OPENSSL_sk_find@PLT
	movq	ext_list(%rip), %rdi
	movl	%eax, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rbx
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L79:
	xorl	%eax, %eax
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L90:
	call	ASN1_item_d2i@PLT
	jmp	.L73
.L91:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1307:
	.size	X509V3_EXT_d2i, .-X509V3_EXT_d2i
	.p2align 4
	.globl	X509V3_get_d2i
	.type	X509V3_get_d2i, @function
X509V3_get_d2i:
.LFB1308:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -56(%rbp)
	testq	%rdi, %rdi
	je	.L120
	movq	%rdi, %r12
	movl	%esi, %r13d
	testq	%rcx, %rcx
	je	.L108
	movl	(%rcx), %ebx
	movl	$0, %eax
	addl	$1, %ebx
	cmovs	%eax, %ebx
.L98:
	movq	%r12, %rdi
	movq	%rcx, -64(%rbp)
	call	OPENSSL_sk_num@PLT
	movq	-64(%rbp), %rcx
	cmpl	%eax, %ebx
	jge	.L119
	movl	%ebx, %esi
	movq	%r12, %rdi
	movq	%rcx, -64(%rbp)
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	movq	%rax, %r14
	call	X509_EXTENSION_get_object@PLT
	movq	%rax, %rdi
	call	OBJ_obj2nid@PLT
	movq	-64(%rbp), %rcx
	cmpl	%r13d, %eax
	je	.L121
	addl	$1, %ebx
	jmp	.L98
.L120:
	testq	%rcx, %rcx
	je	.L94
.L119:
	movl	$-1, (%rcx)
.L94:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L92
	movl	$-1, (%rax)
.L92:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L100:
	addl	$1, %r15d
.L97:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r15d
	jge	.L99
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	X509_EXTENSION_get_object@PLT
	movq	%rax, %rdi
	call	OBJ_obj2nid@PLT
	cmpl	%eax, %r13d
	jne	.L100
	testq	%r14, %r14
	jne	.L101
	movq	%rbx, %r14
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L99:
	testq	%r14, %r14
	je	.L94
.L103:
	movq	-56(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L107
	movq	%r14, %rdi
	call	X509_EXTENSION_get_critical@PLT
	movl	%eax, (%rbx)
.L107:
	addq	$24, %rsp
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	X509V3_EXT_d2i
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	movl	%ebx, (%rcx)
	testq	%r14, %r14
	jne	.L103
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L101:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L92
	movl	$-2, (%rax)
	jmp	.L92
	.cfi_endproc
.LFE1308:
	.size	X509V3_get_d2i, .-X509V3_get_d2i
	.p2align 4
	.globl	X509V3_add1_i2d
	.type	X509V3_add1_i2d, @function
X509V3_add1_i2d:
.LFB1309:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	andl	$15, %r13d
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpq	$1, %r13
	jne	.L165
.L123:
	movq	%r15, %rdx
	movl	%ecx, %esi
	movl	%r14d, %edi
	call	X509V3_EXT_i2d@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L133
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.L166
.L130:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L131
	movq	%r13, (%rbx)
.L164:
	movl	$1, %eax
.L122:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	.cfi_restore_state
	movq	(%rdi), %rdi
	movl	$-1, %edx
	movl	%ecx, -52(%rbp)
	movq	%r8, %r12
	call	X509v3_get_ext_by_NID@PLT
	movl	-52(%rbp), %ecx
	testl	%eax, %eax
	js	.L124
	cmpq	$4, %r13
	je	.L164
	testq	%r13, %r13
	je	.L136
	cmpq	$5, %r13
	je	.L167
	movq	%r15, %rdx
	movl	%ecx, %esi
	movl	%r14d, %edi
	movl	%eax, -52(%rbp)
	call	X509V3_EXT_i2d@PLT
	movl	-52(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L133
	movq	(%rbx), %rdi
	movl	%r8d, %esi
	movl	%r8d, -52(%rbp)
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	call	X509_EXTENSION_free@PLT
	movl	-52(%rbp), %r8d
	movq	(%rbx), %rdi
	movq	%r12, %rdx
	movl	%r8d, %esi
	call	OPENSSL_sk_set@PLT
	testq	%rax, %rax
	jne	.L164
.L163:
	addq	$24, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	subq	$3, %r13
	testq	$-3, %r13
	jne	.L123
	movl	$102, %edx
.L126:
	xorl	%eax, %eax
	andl	$16, %r12d
	jne	.L122
	movl	$301, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$140, %esi
	movl	%eax, -52(%rbp)
	movl	$34, %edi
	call	ERR_put_error@PLT
	movl	-52(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore_state
	movl	$268, %r8d
	movl	$144, %edx
	movl	$140, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L166:
	.cfi_restore_state
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L130
	.p2align 4,,10
	.p2align 3
.L131:
	cmpq	%r13, (%rbx)
	je	.L132
	movq	%r13, %rdi
	call	OPENSSL_sk_free@PLT
.L132:
	movq	%r12, %rdi
	call	X509_EXTENSION_free@PLT
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L136:
	movl	$145, %edx
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L167:
	movq	(%rbx), %rdi
	movl	%eax, %esi
	call	OPENSSL_sk_delete@PLT
	testq	%rax, %rax
	jne	.L164
	jmp	.L163
	.cfi_endproc
.LFE1309:
	.size	X509V3_add1_i2d, .-X509V3_add1_i2d
	.section	.data.rel,"aw"
	.align 32
	.type	standard_exts, @object
	.size	standard_exts, 376
standard_exts:
	.quad	v3_nscert
	.quad	v3_ns_ia5_list
	.quad	v3_ns_ia5_list+104
	.quad	v3_ns_ia5_list+208
	.quad	v3_ns_ia5_list+312
	.quad	v3_ns_ia5_list+416
	.quad	v3_ns_ia5_list+520
	.quad	v3_ns_ia5_list+624
	.quad	v3_skey_id
	.quad	v3_key_usage
	.quad	v3_pkey_usage_period
	.quad	v3_alt
	.quad	v3_alt+104
	.quad	v3_bcons
	.quad	v3_crl_num
	.quad	v3_cpols
	.quad	v3_akey_id
	.quad	v3_crld
	.quad	v3_ext_ku
	.quad	v3_delta_crl
	.quad	v3_crl_reason
	.quad	v3_crl_invdate
	.quad	v3_sxnet
	.quad	v3_info
	.quad	v3_addr
	.quad	v3_asid
	.quad	v3_ocsp_nonce
	.quad	v3_ocsp_crlid
	.quad	v3_ocsp_accresp
	.quad	v3_ocsp_nocheck
	.quad	v3_ocsp_acutoff
	.quad	v3_ocsp_serviceloc
	.quad	v3_sinfo
	.quad	v3_policy_constraints
	.quad	v3_crl_hold
	.quad	v3_pci
	.quad	v3_name_constraints
	.quad	v3_policy_mappings
	.quad	v3_inhibit_anyp
	.quad	v3_idp
	.quad	v3_alt+208
	.quad	v3_freshest_crl
	.quad	v3_ct_scts
	.quad	v3_ct_scts+104
	.quad	v3_ct_scts+208
	.quad	v3_tls_feature
	.quad	v3_ext_admission
	.local	ext_list
	.comm	ext_list,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
