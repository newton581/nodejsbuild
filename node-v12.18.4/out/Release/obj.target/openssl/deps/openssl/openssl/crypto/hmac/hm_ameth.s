	.file	"hm_ameth.c"
	.text
	.p2align 4
	.type	hmac_size, @function
hmac_size:
.LFB469:
	.cfi_startproc
	endbr64
	movl	$64, %eax
	ret
	.cfi_endproc
.LFE469:
	.size	hmac_size, .-hmac_size
	.p2align 4
	.type	hmac_pkey_ctrl, @function
hmac_pkey_ctrl:
.LFB471:
	.cfi_startproc
	endbr64
	cmpl	$3, %esi
	jne	.L5
	movl	$672, (%rcx)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$-2, %eax
	ret
	.cfi_endproc
.LFE471:
	.size	hmac_pkey_ctrl, .-hmac_pkey_ctrl
	.p2align 4
	.type	hmac_get_priv_key, @function
hmac_get_priv_key:
.LFB474:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	movq	40(%rdi), %r13
	testq	%rsi, %rsi
	je	.L12
	testq	%r13, %r13
	je	.L10
	movq	%r13, %rdi
	movq	(%rdx), %r14
	movq	%rsi, %r12
	call	ASN1_STRING_length@PLT
	cltq
	cmpq	%rax, %r14
	jnb	.L13
.L10:
	xorl	%eax, %eax
.L6:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	movq	%r13, %rdi
	call	ASN1_STRING_length@PLT
	movq	%r13, %rdi
	movslq	%eax, %r14
	movq	%r14, (%rbx)
	call	ASN1_STRING_get0_data@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	memcpy@PLT
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	movq	%r13, %rdi
	call	ASN1_STRING_length@PLT
	cltq
	movq	%rax, (%rbx)
	movl	$1, %eax
	jmp	.L6
	.cfi_endproc
.LFE474:
	.size	hmac_get_priv_key, .-hmac_get_priv_key
	.p2align 4
	.type	hmac_key_free, @function
hmac_key_free:
.LFB470:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	EVP_PKEY_get0@PLT
	testq	%rax, %rax
	je	.L14
	movq	8(%rax), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L16
	movslq	(%rax), %rsi
	call	OPENSSL_cleanse@PLT
.L16:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ASN1_OCTET_STRING_free@PLT
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE470:
	.size	hmac_key_free, .-hmac_key_free
	.p2align 4
	.type	hmac_pkey_public_cmp, @function
hmac_pkey_public_cmp:
.LFB472:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -32
	call	EVP_PKEY_get0@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	EVP_PKEY_get0@PLT
	movq	%r12, %rsi
	popq	%r12
	popq	%r13
	movq	%rax, %rdi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	ASN1_OCTET_STRING_cmp@PLT
	.cfi_endproc
.LFE472:
	.size	hmac_pkey_public_cmp, .-hmac_pkey_public_cmp
	.p2align 4
	.type	hmac_set_priv_key, @function
hmac_set_priv_key:
.LFB473:
	.cfi_startproc
	endbr64
	cmpq	$0, 40(%rdi)
	je	.L24
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	call	ASN1_OCTET_STRING_new@PLT
	movq	%rax, %r13
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L23
	movl	%r12d, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	ASN1_OCTET_STRING_set@PLT
	testl	%eax, %eax
	je	.L34
	movq	%r13, 40(%rbx)
	movl	$1, %eax
.L23:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	movq	%r13, %rdi
	movl	%eax, -36(%rbp)
	call	ASN1_OCTET_STRING_free@PLT
	movl	-36(%rbp), %eax
	jmp	.L23
	.cfi_endproc
.LFE473:
	.size	hmac_set_priv_key, .-hmac_set_priv_key
	.globl	hmac_asn1_meth
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"HMAC"
.LC1:
	.string	"OpenSSL HMAC method"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	hmac_asn1_meth, @object
	.size	hmac_asn1_meth, 280
hmac_asn1_meth:
	.long	855
	.long	855
	.quad	0
	.quad	.LC0
	.quad	.LC1
	.quad	0
	.quad	0
	.quad	hmac_pkey_public_cmp
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	hmac_size
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	hmac_key_free
	.quad	hmac_pkey_ctrl
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	hmac_set_priv_key
	.quad	0
	.quad	hmac_get_priv_key
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
