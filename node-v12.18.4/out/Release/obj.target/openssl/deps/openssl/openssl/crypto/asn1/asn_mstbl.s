	.file	"asn_mstbl.c"
	.text
	.p2align 4
	.type	stbl_module_finish, @function
stbl_module_finish:
.LFB1297:
	.cfi_startproc
	endbr64
	jmp	ASN1_STRING_TABLE_cleanup@PLT
	.cfi_endproc
.LFE1297:
	.size	stbl_module_finish, .-stbl_module_finish
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/asn1/asn_mstbl.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"min"
.LC2:
	.string	"max"
.LC3:
	.string	"mask"
.LC4:
	.string	"flags"
.LC5:
	.string	"nomask"
.LC6:
	.string	"none"
.LC7:
	.string	", value="
.LC8:
	.string	"name="
.LC9:
	.string	"field="
	.text
	.p2align 4
	.type	stbl_module_init, @function
stbl_module_init:
.LFB1296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	CONF_imodule_get_value@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	NCONF_get_section@PLT
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L42
	movq	-88(%rbp), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r15d
	jge	.L43
	.p2align 4,,10
	.p2align 3
.L17:
	movq	-88(%rbp), %rdi
	movl	%r15d, %esi
	call	OPENSSL_sk_value@PLT
	movq	$0, -64(%rbp)
	movq	8(%rax), %r13
	movq	16(%rax), %rbx
	movq	%r13, %rdi
	call	OBJ_sn2nid@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L44
.L6:
	movq	%rbx, %rdi
	call	X509V3_parse_list@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L7
	movq	$-1, -96(%rbp)
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
	movq	$-1, -104(%rbp)
	movl	%r12d, -108(%rbp)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$4, %ecx
	movq	%rax, %rsi
	leaq	.LC2(%rip), %rdi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L45
	movl	$5, %ecx
	movq	%rax, %rsi
	leaq	.LC3(%rip), %rdi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L46
	movq	%rax, %rsi
	movl	$6, %ecx
	leaq	.LC4(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L38
	movq	16(%r12), %rax
	movl	$7, %ecx
	leaq	.LC5(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L22
	movq	%rax, %rsi
	movl	$5, %ecx
	leaq	.LC6(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L38
	movl	$1, %r13d
	.p2align 4,,10
	.p2align 3
.L11:
	addl	$1, %ebx
.L8:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L47
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movl	$4, %ecx
	leaq	.LC1(%rip), %rdi
	movq	%rax, %r12
	movq	8(%rax), %rax
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	jne	.L9
	movq	16(%r12), %rdi
	xorl	%edx, %edx
	leaq	-72(%rbp), %rsi
	call	strtoul@PLT
	movq	%rax, -104(%rbp)
	movq	-72(%rbp), %rax
	cmpb	$0, (%rax)
	je	.L11
.L38:
	movl	$99, %r8d
	movl	$218, %edx
	movl	$222, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	8(%r12), %rdx
	movq	16(%r12), %r8
	xorl	%eax, %eax
	leaq	.LC9(%rip), %rsi
	movl	$4, %edi
	leaq	.LC7(%rip), %rcx
	call	ERR_add_error_data@PLT
	movq	X509V3_conf_free@GOTPCREL(%rip), %rsi
	movq	%r14, %rdi
	call	OPENSSL_sk_pop_free@PLT
.L16:
	movl	$35, %r8d
	movl	$219, %edx
	movl	$223, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L3:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L48
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	movl	$2, %r13d
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L46:
	movq	16(%r12), %rdi
	leaq	-64(%rbp), %rsi
	call	ASN1_str2mask@PLT
	testl	%eax, %eax
	je	.L38
	cmpq	$0, -64(%rbp)
	jne	.L11
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L45:
	movq	16(%r12), %rdi
	xorl	%edx, %edx
	leaq	-72(%rbp), %rsi
	call	strtoul@PLT
	movq	%rax, -96(%rbp)
	movq	-72(%rbp), %rax
	cmpb	$0, (%rax)
	je	.L11
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L44:
	movq	%r13, %rdi
	call	OBJ_ln2nid@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L6
.L7:
	movl	$99, %r8d
	movl	$218, %edx
	movl	$222, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%rbx, %r8
	movq	%r13, %rdx
	movl	$4, %edi
	leaq	.LC7(%rip), %rcx
	leaq	.LC8(%rip), %rsi
	xorl	%eax, %eax
	call	ERR_add_error_data@PLT
	movq	X509V3_conf_free@GOTPCREL(%rip), %rsi
	xorl	%edi, %edi
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L47:
	movl	-108(%rbp), %r12d
	movq	-64(%rbp), %rcx
	movq	%r13, %r8
	movq	-96(%rbp), %rdx
	movq	-104(%rbp), %rsi
	movl	%r12d, %edi
	call	ASN1_STRING_TABLE_add@PLT
	testl	%eax, %eax
	je	.L49
	movq	X509V3_conf_free@GOTPCREL(%rip), %rsi
	movq	%r14, %rdi
	addl	$1, %r15d
	call	OPENSSL_sk_pop_free@PLT
	movq	-88(%rbp), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r15d
	jl	.L17
.L43:
	movl	$1, %eax
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L49:
	movl	$109, %r8d
	movl	$65, %edx
	movl	$222, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	X509V3_conf_free@GOTPCREL(%rip), %rsi
	movq	%r14, %rdi
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L42:
	movl	$29, %r8d
	movl	$172, %edx
	movl	$223, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L3
.L48:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1296:
	.size	stbl_module_init, .-stbl_module_init
	.section	.rodata.str1.1
.LC10:
	.string	"stbl_section"
	.text
	.p2align 4
	.globl	ASN1_add_stable_module
	.type	ASN1_add_stable_module, @function
ASN1_add_stable_module:
.LFB1298:
	.cfi_startproc
	endbr64
	leaq	stbl_module_finish(%rip), %rdx
	leaq	stbl_module_init(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	jmp	CONF_module_add@PLT
	.cfi_endproc
.LFE1298:
	.size	ASN1_add_stable_module, .-ASN1_add_stable_module
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
