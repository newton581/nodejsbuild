	.file	"x509_txt.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"unknown certificate verification error"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"ok"
	.section	.rodata.str1.8
	.align 8
.LC2:
	.string	"unable to get issuer certificate"
	.section	.rodata.str1.1
.LC3:
	.string	"unable to get certificate CRL"
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"unable to decrypt certificate's signature"
	.align 8
.LC5:
	.string	"unable to decrypt CRL's signature"
	.align 8
.LC6:
	.string	"unable to decode issuer public key"
	.section	.rodata.str1.1
.LC7:
	.string	"certificate signature failure"
.LC8:
	.string	"CRL signature failure"
.LC9:
	.string	"certificate is not yet valid"
.LC10:
	.string	"certificate has expired"
.LC11:
	.string	"CRL is not yet valid"
.LC12:
	.string	"CRL has expired"
	.section	.rodata.str1.8
	.align 8
.LC13:
	.string	"format error in certificate's notBefore field"
	.align 8
.LC14:
	.string	"format error in certificate's notAfter field"
	.align 8
.LC15:
	.string	"format error in CRL's lastUpdate field"
	.align 8
.LC16:
	.string	"format error in CRL's nextUpdate field"
	.section	.rodata.str1.1
.LC17:
	.string	"out of memory"
.LC18:
	.string	"self signed certificate"
	.section	.rodata.str1.8
	.align 8
.LC19:
	.string	"self signed certificate in certificate chain"
	.align 8
.LC20:
	.string	"unable to get local issuer certificate"
	.align 8
.LC21:
	.string	"unable to verify the first certificate"
	.section	.rodata.str1.1
.LC22:
	.string	"certificate chain too long"
.LC23:
	.string	"certificate revoked"
.LC24:
	.string	"invalid CA certificate"
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"path length constraint exceeded"
	.align 8
.LC26:
	.string	"unsupported certificate purpose"
	.section	.rodata.str1.1
.LC27:
	.string	"certificate not trusted"
.LC28:
	.string	"certificate rejected"
.LC29:
	.string	"subject issuer mismatch"
	.section	.rodata.str1.8
	.align 8
.LC30:
	.string	"authority and subject key identifier mismatch"
	.align 8
.LC31:
	.string	"authority and issuer serial number mismatch"
	.align 8
.LC32:
	.string	"key usage does not include certificate signing"
	.align 8
.LC33:
	.string	"unable to get CRL issuer certificate"
	.section	.rodata.str1.1
.LC34:
	.string	"unhandled critical extension"
	.section	.rodata.str1.8
	.align 8
.LC35:
	.string	"key usage does not include CRL signing"
	.align 8
.LC36:
	.string	"unhandled critical CRL extension"
	.align 8
.LC37:
	.string	"invalid non-CA certificate (has CA markings)"
	.align 8
.LC38:
	.string	"proxy path length constraint exceeded"
	.align 8
.LC39:
	.string	"key usage does not include digital signature"
	.align 8
.LC40:
	.string	"proxy certificates not allowed, please set the appropriate flag"
	.align 8
.LC41:
	.string	"invalid or inconsistent certificate extension"
	.align 8
.LC42:
	.string	"invalid or inconsistent certificate policy extension"
	.section	.rodata.str1.1
.LC43:
	.string	"no explicit policy"
.LC44:
	.string	"Different CRL scope"
.LC45:
	.string	"Unsupported extension feature"
	.section	.rodata.str1.8
	.align 8
.LC46:
	.string	"RFC 3779 resource not subset of parent's resources"
	.section	.rodata.str1.1
.LC47:
	.string	"permitted subtree violation"
.LC48:
	.string	"excluded subtree violation"
	.section	.rodata.str1.8
	.align 8
.LC49:
	.string	"name constraints minimum and maximum not supported"
	.align 8
.LC50:
	.string	"application verification failure"
	.align 8
.LC51:
	.string	"unsupported name constraint type"
	.align 8
.LC52:
	.string	"unsupported or invalid name constraint syntax"
	.align 8
.LC53:
	.string	"unsupported or invalid name syntax"
	.section	.rodata.str1.1
.LC54:
	.string	"CRL path validation error"
.LC55:
	.string	"Path Loop"
	.section	.rodata.str1.8
	.align 8
.LC56:
	.string	"Suite B: certificate version invalid"
	.align 8
.LC57:
	.string	"Suite B: invalid public key algorithm"
	.section	.rodata.str1.1
.LC58:
	.string	"Suite B: invalid ECC curve"
	.section	.rodata.str1.8
	.align 8
.LC59:
	.string	"Suite B: invalid signature algorithm"
	.align 8
.LC60:
	.string	"Suite B: curve not allowed for this LOS"
	.align 8
.LC61:
	.string	"Suite B: cannot sign P-384 with P-256"
	.section	.rodata.str1.1
.LC62:
	.string	"Hostname mismatch"
.LC63:
	.string	"Email address mismatch"
.LC64:
	.string	"IP address mismatch"
.LC65:
	.string	"No matching DANE TLSA records"
.LC66:
	.string	"EE certificate key too weak"
.LC67:
	.string	"CA certificate key too weak"
	.section	.rodata.str1.8
	.align 8
.LC68:
	.string	"CA signature digest algorithm too weak"
	.align 8
.LC69:
	.string	"Invalid certificate verification context"
	.align 8
.LC70:
	.string	"Issuer certificate lookup error"
	.align 8
.LC71:
	.string	"Certificate Transparency required, but no valid SCTs found"
	.section	.rodata.str1.1
.LC72:
	.string	"proxy subject name violation"
.LC73:
	.string	"OCSP verification needed"
.LC74:
	.string	"OCSP verification failed"
.LC75:
	.string	"OCSP unknown cert"
	.section	.rodata.str1.8
	.align 8
.LC76:
	.string	"unspecified certificate verification error"
	.text
	.p2align 4
	.globl	X509_verify_cert_error_string
	.type	X509_verify_cert_error_string, @function
X509_verify_cert_error_string:
.LFB779:
	.cfi_startproc
	endbr64
	cmpl	$75, %edi
	ja	.L2
	leaq	.L4(%rip), %rdx
	movl	%edi, %edi
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L4:
	.long	.L80-.L4
	.long	.L78-.L4
	.long	.L77-.L4
	.long	.L76-.L4
	.long	.L75-.L4
	.long	.L74-.L4
	.long	.L73-.L4
	.long	.L72-.L4
	.long	.L71-.L4
	.long	.L70-.L4
	.long	.L69-.L4
	.long	.L68-.L4
	.long	.L67-.L4
	.long	.L66-.L4
	.long	.L65-.L4
	.long	.L64-.L4
	.long	.L63-.L4
	.long	.L62-.L4
	.long	.L61-.L4
	.long	.L60-.L4
	.long	.L59-.L4
	.long	.L58-.L4
	.long	.L57-.L4
	.long	.L56-.L4
	.long	.L55-.L4
	.long	.L54-.L4
	.long	.L53-.L4
	.long	.L52-.L4
	.long	.L51-.L4
	.long	.L50-.L4
	.long	.L49-.L4
	.long	.L48-.L4
	.long	.L47-.L4
	.long	.L46-.L4
	.long	.L45-.L4
	.long	.L44-.L4
	.long	.L43-.L4
	.long	.L42-.L4
	.long	.L41-.L4
	.long	.L40-.L4
	.long	.L39-.L4
	.long	.L38-.L4
	.long	.L37-.L4
	.long	.L36-.L4
	.long	.L35-.L4
	.long	.L34-.L4
	.long	.L33-.L4
	.long	.L32-.L4
	.long	.L31-.L4
	.long	.L30-.L4
	.long	.L29-.L4
	.long	.L28-.L4
	.long	.L27-.L4
	.long	.L26-.L4
	.long	.L25-.L4
	.long	.L24-.L4
	.long	.L23-.L4
	.long	.L22-.L4
	.long	.L21-.L4
	.long	.L20-.L4
	.long	.L19-.L4
	.long	.L18-.L4
	.long	.L17-.L4
	.long	.L16-.L4
	.long	.L15-.L4
	.long	.L14-.L4
	.long	.L13-.L4
	.long	.L12-.L4
	.long	.L11-.L4
	.long	.L10-.L4
	.long	.L9-.L4
	.long	.L8-.L4
	.long	.L7-.L4
	.long	.L6-.L4
	.long	.L5-.L4
	.long	.L3-.L4
	.text
	.p2align 4,,10
	.p2align 3
.L78:
	leaq	.LC76(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	leaq	.LC1(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	leaq	.LC75(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	leaq	.LC74(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	leaq	.LC73(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	leaq	.LC41(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	leaq	.LC40(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	leaq	.LC39(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	leaq	.LC38(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	leaq	.LC37(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	leaq	.LC36(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	leaq	.LC35(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	leaq	.LC34(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	leaq	.LC44(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	leaq	.LC43(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	leaq	.LC42(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	leaq	.LC8(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	leaq	.LC7(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	leaq	.LC6(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	leaq	.LC5(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	leaq	.LC69(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	leaq	.LC4(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	leaq	.LC3(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	leaq	.LC2(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	leaq	.LC12(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	leaq	.LC11(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	leaq	.LC10(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	leaq	.LC9(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	leaq	.LC49(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	leaq	.LC48(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	leaq	.LC47(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	leaq	.LC46(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	leaq	.LC45(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	leaq	.LC68(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	leaq	.LC67(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	leaq	.LC66(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	leaq	.LC65(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	leaq	.LC64(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	leaq	.LC63(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	leaq	.LC62(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	leaq	.LC61(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	leaq	.LC72(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	leaq	.LC71(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	leaq	.LC70(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	leaq	.LC33(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	leaq	.LC32(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	leaq	.LC31(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	leaq	.LC30(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	leaq	.LC29(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	leaq	.LC28(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	leaq	.LC27(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	leaq	.LC26(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	leaq	.LC25(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	leaq	.LC24(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	leaq	.LC23(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	leaq	.LC22(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	leaq	.LC21(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	leaq	.LC20(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	leaq	.LC19(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	leaq	.LC18(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	leaq	.LC17(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	leaq	.LC16(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	leaq	.LC15(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	leaq	.LC14(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	leaq	.LC13(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	leaq	.LC60(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	leaq	.LC59(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	leaq	.LC58(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	leaq	.LC57(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	leaq	.LC56(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	leaq	.LC55(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	leaq	.LC54(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	leaq	.LC53(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	leaq	.LC52(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	leaq	.LC51(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	leaq	.LC50(%rip), %rax
	ret
.L2:
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE779:
	.size	X509_verify_cert_error_string, .-X509_verify_cert_error_string
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
