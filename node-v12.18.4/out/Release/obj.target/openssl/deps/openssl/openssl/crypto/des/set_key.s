	.file	"set_key.c"
	.text
	.p2align 4
	.globl	DES_set_odd_parity
	.type	DES_set_odd_parity, @function
DES_set_odd_parity:
.LFB151:
	.cfi_startproc
	endbr64
	movzbl	(%rdi), %edx
	leaq	odd_parity(%rip), %rax
	movzbl	(%rax,%rdx), %edx
	movb	%dl, (%rdi)
	movzbl	1(%rdi), %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 1(%rdi)
	movzbl	2(%rdi), %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 2(%rdi)
	movzbl	3(%rdi), %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 3(%rdi)
	movzbl	4(%rdi), %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 4(%rdi)
	movzbl	5(%rdi), %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 5(%rdi)
	movzbl	6(%rdi), %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 6(%rdi)
	movzbl	7(%rdi), %edx
	movzbl	(%rax,%rdx), %eax
	movb	%al, 7(%rdi)
	ret
	.cfi_endproc
.LFE151:
	.size	DES_set_odd_parity, .-DES_set_odd_parity
	.p2align 4
	.globl	DES_check_key_parity
	.type	DES_check_key_parity, @function
DES_check_key_parity:
.LFB152:
	.cfi_startproc
	endbr64
	movzbl	(%rdi), %edx
	leaq	odd_parity(%rip), %rcx
	cmpb	(%rcx,%rdx), %dl
	jne	.L11
	movzbl	1(%rdi), %edx
	cmpb	(%rcx,%rdx), %dl
	jne	.L11
	movzbl	2(%rdi), %edx
	cmpb	(%rcx,%rdx), %dl
	jne	.L11
	movzbl	3(%rdi), %edx
	cmpb	(%rcx,%rdx), %dl
	jne	.L11
	movzbl	4(%rdi), %edx
	cmpb	(%rcx,%rdx), %dl
	jne	.L11
	movzbl	5(%rdi), %edx
	cmpb	(%rcx,%rdx), %dl
	jne	.L11
	movzbl	6(%rdi), %edx
	cmpb	(%rcx,%rdx), %dl
	jne	.L11
	movzbl	7(%rdi), %edx
	cmpb	%dl, (%rcx,%rdx)
	sete	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE152:
	.size	DES_check_key_parity, .-DES_check_key_parity
	.p2align 4
	.globl	DES_is_weak_key
	.type	DES_is_weak_key, @function
DES_is_weak_key:
.LFB153:
	.cfi_startproc
	endbr64
	movabsq	$72340172838076673, %rax
	cmpq	%rax, (%rdi)
	je	.L28
	movabsq	$-72340172838076674, %rax
	cmpq	%rax, (%rdi)
	je	.L28
	movabsq	$1012762420019404575, %rax
	cmpq	%rax, (%rdi)
	je	.L28
	movabsq	$-1012762420019404576, %rax
	cmpq	%rax, (%rdi)
	je	.L28
	movabsq	$-143554428589179391, %rax
	cmpq	%rax, (%rdi)
	je	.L28
	movabsq	$143554428589179390, %rax
	cmpq	%rax, (%rdi)
	je	.L28
	movabsq	$-1076658214702948321, %rax
	cmpq	%rax, (%rdi)
	je	.L28
	movabsq	$1076658214702948320, %rax
	cmpq	%rax, (%rdi)
	je	.L28
	movabsq	$-1080317445236727807, %rax
	cmpq	%rax, (%rdi)
	je	.L28
	movabsq	$139895198055399904, %rax
	cmpq	%rax, (%rdi)
	je	.L28
	movabsq	$-139895198055399905, %rax
	cmpq	%rax, (%rdi)
	je	.L28
	movabsq	$1080317445236727806, %rax
	cmpq	%rax, (%rdi)
	je	.L28
	movabsq	$1009103189485625089, %rax
	cmpq	%rax, (%rdi)
	je	.L28
	movabsq	$75999403371856159, %rax
	cmpq	%rax, (%rdi)
	je	.L28
	movabsq	$-75999403371856160, %rax
	cmpq	%rax, (%rdi)
	je	.L28
	movabsq	$-1009103189485625090, %rax
	cmpq	%rax, (%rdi)
	sete	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE153:
	.size	DES_is_weak_key, .-DES_is_weak_key
	.p2align 4
	.globl	DES_set_key_unchecked
	.type	DES_set_key_unchecked, @function
DES_set_key_unchecked:
.LFB156:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	leaq	shifts2.5132(%rip), %r10
	leaq	des_skb(%rip), %r9
	leaq	64(%r10), %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	4(%rax), %eax
	movl	(%rdi), %edi
	movl	%eax, %r8d
	shrl	$4, %r8d
	xorl	%edi, %r8d
	andl	$252645135, %r8d
	xorl	%r8d, %edi
	sall	$4, %r8d
	xorl	%eax, %r8d
	movl	%edi, %edx
	movl	%edi, %ecx
	movl	%r8d, %eax
	sall	$18, %edx
	sall	$18, %eax
	xorl	%edi, %edx
	xorl	%r8d, %eax
	andl	$-859045888, %edx
	andl	$-859045888, %eax
	xorl	%edx, %ecx
	shrl	$18, %edx
	xorl	%eax, %r8d
	shrl	$18, %eax
	movl	%edx, %edi
	xorl	%eax, %r8d
	xorl	%ecx, %edi
	movl	%r8d, %edx
	shrl	%edx
	xorl	%edi, %edx
	andl	$1431655765, %edx
	xorl	%edx, %edi
	leal	(%rdx,%rdx), %eax
	movl	%edi, %ecx
	xorl	%r8d, %eax
	shrl	$8, %ecx
	xorl	%eax, %ecx
	andl	$16711935, %ecx
	xorl	%ecx, %eax
	sall	$8, %ecx
	movl	%eax, %edx
	xorl	%edi, %ecx
	shrl	%edx
	xorl	%ecx, %edx
	andl	$1431655765, %edx
	xorl	%edx, %ecx
	addl	%edx, %edx
	movl	%ecx, %r8d
	xorl	%edx, %eax
	andl	$268435455, %ecx
	shrl	$4, %r8d
	movl	%eax, %edx
	andl	$65280, %eax
	andl	$251658240, %r8d
	orl	%r8d, %eax
	movq	%rdx, %r8
	sarq	$16, %rdx
	salq	$16, %r8
	andl	$16711680, %r8d
	orl	%r8d, %eax
	movzbl	%dl, %r8d
	orl	%eax, %r8d
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L36:
	shrl	$2, %r12d
	movl	%r8d, %ebx
	sall	$26, %ecx
	shrl	$2, %ebx
	sall	$26, %r8d
	orl	%ecx, %r12d
	orl	%r8d, %ebx
.L31:
	movl	%r12d, %ecx
	movl	%ebx, %r8d
	andl	$63, %r12d
	andl	$63, %ebx
	andl	$268435455, %ecx
	andl	$268435455, %r8d
	movl	%ecx, %eax
	movl	%ecx, %edx
	movl	%ecx, %edi
	shrl	$6, %eax
	shrl	$7, %edx
	andl	$60, %edx
	andl	$3, %eax
	shrl	$13, %edi
	orl	%edx, %eax
	movl	%ecx, %edx
	andl	$15, %edi
	shrl	$14, %edx
	andl	$48, %edx
	orl	%edx, %edi
	movl	256(%r9,%rax,4), %edx
	movl	%ecx, %eax
	orl	512(%r9,%rdi,4), %edx
	movl	%ecx, %edi
	shrl	$21, %eax
	orl	(%r9,%r12,4), %edx
	shrl	$20, %edi
	andl	$6, %eax
	andl	$1, %edi
	orl	%eax, %edi
	movl	%ecx, %eax
	shrl	$22, %eax
	andl	$56, %eax
	orl	%eax, %edi
	movl	%r8d, %eax
	orl	768(%r9,%rdi,4), %edx
	movl	%r8d, %edi
	shrl	$7, %eax
	shrl	$8, %edi
	andl	$3, %eax
	andl	$60, %edi
	orl	%edi, %eax
	movl	%r8d, %edi
	shrl	$15, %edi
	andl	$63, %edi
	movl	1536(%r9,%rdi,4), %edi
	orl	1024(%r9,%rbx,4), %edi
	movl	%r8d, %ebx
	addq	$8, %rsi
	orl	1280(%r9,%rax,4), %edi
	movl	%r8d, %eax
	shrl	$22, %ebx
	addq	$4, %r10
	shrl	$21, %eax
	andl	$48, %ebx
	andl	$15, %eax
	orl	%ebx, %eax
	movzwl	%dx, %ebx
	orl	1792(%r9,%rax,4), %edi
	movl	%edi, %eax
	xorw	%di, %di
	sall	$16, %eax
	orl	%ebx, %eax
#APP
# 362 "../deps/openssl/openssl/crypto/des/set_key.c" 1
	rorl $30,%eax
# 0 "" 2
#NO_APP
	movl	%eax, -8(%rsi)
	movl	%edx, %eax
	shrl	$16, %eax
	orl	%edi, %eax
#APP
# 365 "../deps/openssl/openssl/crypto/des/set_key.c" 1
	rorl $26,%eax
# 0 "" 2
#NO_APP
	movl	%eax, -4(%rsi)
	cmpq	%r11, %r10
	je	.L35
.L32:
	movl	(%r10), %eax
	movl	%ecx, %r12d
	testl	%eax, %eax
	jne	.L36
	shrl	%r12d
	movl	%r8d, %ebx
	sall	$27, %ecx
	shrl	%ebx
	sall	$27, %r8d
	orl	%ecx, %r12d
	orl	%r8d, %ebx
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L35:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE156:
	.size	DES_set_key_unchecked, .-DES_set_key_unchecked
	.p2align 4
	.globl	DES_set_key_checked
	.type	DES_set_key_checked, @function
DES_set_key_checked:
.LFB155:
	.cfi_startproc
	endbr64
	movzbl	(%rdi), %ecx
	leaq	odd_parity(%rip), %rax
	cmpb	(%rax,%rcx), %cl
	jne	.L46
	movzbl	1(%rdi), %ecx
	cmpb	(%rax,%rcx), %cl
	jne	.L46
	movzbl	2(%rdi), %ecx
	cmpb	(%rax,%rcx), %cl
	jne	.L46
	movzbl	3(%rdi), %ecx
	cmpb	(%rax,%rcx), %cl
	jne	.L46
	movzbl	4(%rdi), %ecx
	cmpb	(%rax,%rcx), %cl
	jne	.L46
	movzbl	5(%rdi), %ecx
	cmpb	(%rax,%rcx), %cl
	jne	.L46
	movzbl	6(%rdi), %ecx
	cmpb	(%rax,%rcx), %cl
	jne	.L46
	movzbl	7(%rdi), %ecx
	cmpb	(%rax,%rcx), %cl
	jne	.L46
	movabsq	$72340172838076673, %rax
	cmpq	%rax, (%rdi)
	je	.L62
	movabsq	$-72340172838076674, %rax
	cmpq	%rax, (%rdi)
	je	.L62
	movabsq	$1012762420019404575, %rax
	cmpq	%rax, (%rdi)
	je	.L62
	movabsq	$-1012762420019404576, %rax
	cmpq	%rax, (%rdi)
	je	.L62
	movabsq	$-143554428589179391, %rax
	cmpq	%rax, (%rdi)
	je	.L62
	movabsq	$143554428589179390, %rax
	cmpq	%rax, (%rdi)
	je	.L62
	movabsq	$-1076658214702948321, %rax
	cmpq	%rax, (%rdi)
	je	.L62
	movabsq	$1076658214702948320, %rax
	cmpq	%rax, (%rdi)
	je	.L62
	movabsq	$-1080317445236727807, %rax
	cmpq	%rax, (%rdi)
	je	.L62
	movabsq	$139895198055399904, %rax
	cmpq	%rax, (%rdi)
	je	.L62
	movabsq	$-139895198055399905, %rax
	cmpq	%rax, (%rdi)
	je	.L62
	movabsq	$1080317445236727806, %rax
	cmpq	%rax, (%rdi)
	je	.L62
	movabsq	$1009103189485625089, %rax
	cmpq	%rax, (%rdi)
	je	.L62
	movabsq	$75999403371856159, %rax
	cmpq	%rax, (%rdi)
	je	.L62
	movabsq	$-75999403371856160, %rax
	cmpq	%rax, (%rdi)
	je	.L62
	movabsq	$-1009103189485625090, %rax
	cmpq	%rax, (%rdi)
	je	.L62
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	DES_set_key_unchecked
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore 6
	movl	$-2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE155:
	.size	DES_set_key_checked, .-DES_set_key_checked
	.p2align 4
	.globl	DES_set_key
	.type	DES_set_key, @function
DES_set_key:
.LFB154:
	.cfi_startproc
	endbr64
	movl	_shadow_DES_check_key(%rip), %eax
	testl	%eax, %eax
	je	.L68
	movzbl	(%rdi), %ecx
	leaq	odd_parity(%rip), %rax
	cmpb	(%rax,%rcx), %cl
	jne	.L77
	movzbl	1(%rdi), %ecx
	cmpb	(%rax,%rcx), %cl
	jne	.L77
	movzbl	2(%rdi), %ecx
	cmpb	(%rax,%rcx), %cl
	jne	.L77
	movzbl	3(%rdi), %ecx
	cmpb	(%rax,%rcx), %cl
	jne	.L77
	movzbl	4(%rdi), %ecx
	cmpb	(%rax,%rcx), %cl
	jne	.L77
	movzbl	5(%rdi), %ecx
	cmpb	(%rax,%rcx), %cl
	jne	.L77
	movzbl	6(%rdi), %ecx
	cmpb	(%rax,%rcx), %cl
	jne	.L77
	movzbl	7(%rdi), %ecx
	cmpb	(%rax,%rcx), %cl
	jne	.L77
	movabsq	$72340172838076673, %rax
	cmpq	%rax, (%rdi)
	je	.L93
	movabsq	$-72340172838076674, %rax
	cmpq	%rax, (%rdi)
	je	.L93
	movabsq	$1012762420019404575, %rax
	cmpq	%rax, (%rdi)
	je	.L93
	movabsq	$-1012762420019404576, %rax
	cmpq	%rax, (%rdi)
	je	.L93
	movabsq	$-143554428589179391, %rax
	cmpq	%rax, (%rdi)
	je	.L93
	movabsq	$143554428589179390, %rax
	cmpq	%rax, (%rdi)
	je	.L93
	movabsq	$-1076658214702948321, %rax
	cmpq	%rax, (%rdi)
	je	.L93
	movabsq	$1076658214702948320, %rax
	cmpq	%rax, (%rdi)
	je	.L93
	movabsq	$-1080317445236727807, %rax
	cmpq	%rax, (%rdi)
	je	.L93
	movabsq	$139895198055399904, %rax
	cmpq	%rax, (%rdi)
	je	.L93
	movabsq	$-139895198055399905, %rax
	cmpq	%rax, (%rdi)
	je	.L93
	movabsq	$1080317445236727806, %rax
	cmpq	%rax, (%rdi)
	je	.L93
	movabsq	$1009103189485625089, %rax
	cmpq	%rax, (%rdi)
	je	.L93
	movabsq	$75999403371856159, %rax
	cmpq	%rax, (%rdi)
	je	.L93
	movabsq	$-75999403371856160, %rax
	cmpq	%rax, (%rdi)
	je	.L93
	movabsq	$-1009103189485625090, %rax
	cmpq	%rax, (%rdi)
	je	.L93
.L68:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	DES_set_key_unchecked
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore 6
	movl	$-2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE154:
	.size	DES_set_key, .-DES_set_key
	.p2align 4
	.globl	DES_key_sched
	.type	DES_key_sched, @function
DES_key_sched:
.LFB157:
	.cfi_startproc
	endbr64
	movl	_shadow_DES_check_key(%rip), %eax
	testl	%eax, %eax
	je	.L98
	movzbl	(%rdi), %ecx
	leaq	odd_parity(%rip), %rax
	cmpb	(%rax,%rcx), %cl
	jne	.L107
	movzbl	1(%rdi), %ecx
	cmpb	(%rax,%rcx), %cl
	jne	.L107
	movzbl	2(%rdi), %ecx
	cmpb	(%rax,%rcx), %cl
	jne	.L107
	movzbl	3(%rdi), %ecx
	cmpb	(%rax,%rcx), %cl
	jne	.L107
	movzbl	4(%rdi), %ecx
	cmpb	(%rax,%rcx), %cl
	jne	.L107
	movzbl	5(%rdi), %ecx
	cmpb	(%rax,%rcx), %cl
	jne	.L107
	movzbl	6(%rdi), %ecx
	cmpb	(%rax,%rcx), %cl
	jne	.L107
	movzbl	7(%rdi), %ecx
	cmpb	(%rax,%rcx), %cl
	jne	.L107
	movabsq	$72340172838076673, %rax
	cmpq	%rax, (%rdi)
	je	.L123
	movabsq	$-72340172838076674, %rax
	cmpq	%rax, (%rdi)
	je	.L123
	movabsq	$1012762420019404575, %rax
	cmpq	%rax, (%rdi)
	je	.L123
	movabsq	$-1012762420019404576, %rax
	cmpq	%rax, (%rdi)
	je	.L123
	movabsq	$-143554428589179391, %rax
	cmpq	%rax, (%rdi)
	je	.L123
	movabsq	$143554428589179390, %rax
	cmpq	%rax, (%rdi)
	je	.L123
	movabsq	$-1076658214702948321, %rax
	cmpq	%rax, (%rdi)
	je	.L123
	movabsq	$1076658214702948320, %rax
	cmpq	%rax, (%rdi)
	je	.L123
	movabsq	$-1080317445236727807, %rax
	cmpq	%rax, (%rdi)
	je	.L123
	movabsq	$139895198055399904, %rax
	cmpq	%rax, (%rdi)
	je	.L123
	movabsq	$-139895198055399905, %rax
	cmpq	%rax, (%rdi)
	je	.L123
	movabsq	$1080317445236727806, %rax
	cmpq	%rax, (%rdi)
	je	.L123
	movabsq	$1009103189485625089, %rax
	cmpq	%rax, (%rdi)
	je	.L123
	movabsq	$75999403371856159, %rax
	cmpq	%rax, (%rdi)
	je	.L123
	movabsq	$-75999403371856160, %rax
	cmpq	%rax, (%rdi)
	je	.L123
	movabsq	$-1009103189485625090, %rax
	cmpq	%rax, (%rdi)
	je	.L123
.L98:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	DES_set_key_unchecked
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore 6
	movl	$-2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE157:
	.size	DES_key_sched, .-DES_key_sched
	.section	.rodata
	.align 32
	.type	shifts2.5132, @object
	.size	shifts2.5132, 64
shifts2.5132:
	.long	0
	.long	0
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	0
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	0
	.align 32
	.type	des_skb, @object
	.size	des_skb, 2048
des_skb:
	.long	0
	.long	16
	.long	536870912
	.long	536870928
	.long	65536
	.long	65552
	.long	536936448
	.long	536936464
	.long	2048
	.long	2064
	.long	536872960
	.long	536872976
	.long	67584
	.long	67600
	.long	536938496
	.long	536938512
	.long	32
	.long	48
	.long	536870944
	.long	536870960
	.long	65568
	.long	65584
	.long	536936480
	.long	536936496
	.long	2080
	.long	2096
	.long	536872992
	.long	536873008
	.long	67616
	.long	67632
	.long	536938528
	.long	536938544
	.long	524288
	.long	524304
	.long	537395200
	.long	537395216
	.long	589824
	.long	589840
	.long	537460736
	.long	537460752
	.long	526336
	.long	526352
	.long	537397248
	.long	537397264
	.long	591872
	.long	591888
	.long	537462784
	.long	537462800
	.long	524320
	.long	524336
	.long	537395232
	.long	537395248
	.long	589856
	.long	589872
	.long	537460768
	.long	537460784
	.long	526368
	.long	526384
	.long	537397280
	.long	537397296
	.long	591904
	.long	591920
	.long	537462816
	.long	537462832
	.long	0
	.long	33554432
	.long	8192
	.long	33562624
	.long	2097152
	.long	35651584
	.long	2105344
	.long	35659776
	.long	4
	.long	33554436
	.long	8196
	.long	33562628
	.long	2097156
	.long	35651588
	.long	2105348
	.long	35659780
	.long	1024
	.long	33555456
	.long	9216
	.long	33563648
	.long	2098176
	.long	35652608
	.long	2106368
	.long	35660800
	.long	1028
	.long	33555460
	.long	9220
	.long	33563652
	.long	2098180
	.long	35652612
	.long	2106372
	.long	35660804
	.long	268435456
	.long	301989888
	.long	268443648
	.long	301998080
	.long	270532608
	.long	304087040
	.long	270540800
	.long	304095232
	.long	268435460
	.long	301989892
	.long	268443652
	.long	301998084
	.long	270532612
	.long	304087044
	.long	270540804
	.long	304095236
	.long	268436480
	.long	301990912
	.long	268444672
	.long	301999104
	.long	270533632
	.long	304088064
	.long	270541824
	.long	304096256
	.long	268436484
	.long	301990916
	.long	268444676
	.long	301999108
	.long	270533636
	.long	304088068
	.long	270541828
	.long	304096260
	.long	0
	.long	1
	.long	262144
	.long	262145
	.long	16777216
	.long	16777217
	.long	17039360
	.long	17039361
	.long	2
	.long	3
	.long	262146
	.long	262147
	.long	16777218
	.long	16777219
	.long	17039362
	.long	17039363
	.long	512
	.long	513
	.long	262656
	.long	262657
	.long	16777728
	.long	16777729
	.long	17039872
	.long	17039873
	.long	514
	.long	515
	.long	262658
	.long	262659
	.long	16777730
	.long	16777731
	.long	17039874
	.long	17039875
	.long	134217728
	.long	134217729
	.long	134479872
	.long	134479873
	.long	150994944
	.long	150994945
	.long	151257088
	.long	151257089
	.long	134217730
	.long	134217731
	.long	134479874
	.long	134479875
	.long	150994946
	.long	150994947
	.long	151257090
	.long	151257091
	.long	134218240
	.long	134218241
	.long	134480384
	.long	134480385
	.long	150995456
	.long	150995457
	.long	151257600
	.long	151257601
	.long	134218242
	.long	134218243
	.long	134480386
	.long	134480387
	.long	150995458
	.long	150995459
	.long	151257602
	.long	151257603
	.long	0
	.long	1048576
	.long	256
	.long	1048832
	.long	8
	.long	1048584
	.long	264
	.long	1048840
	.long	4096
	.long	1052672
	.long	4352
	.long	1052928
	.long	4104
	.long	1052680
	.long	4360
	.long	1052936
	.long	67108864
	.long	68157440
	.long	67109120
	.long	68157696
	.long	67108872
	.long	68157448
	.long	67109128
	.long	68157704
	.long	67112960
	.long	68161536
	.long	67113216
	.long	68161792
	.long	67112968
	.long	68161544
	.long	67113224
	.long	68161800
	.long	131072
	.long	1179648
	.long	131328
	.long	1179904
	.long	131080
	.long	1179656
	.long	131336
	.long	1179912
	.long	135168
	.long	1183744
	.long	135424
	.long	1184000
	.long	135176
	.long	1183752
	.long	135432
	.long	1184008
	.long	67239936
	.long	68288512
	.long	67240192
	.long	68288768
	.long	67239944
	.long	68288520
	.long	67240200
	.long	68288776
	.long	67244032
	.long	68292608
	.long	67244288
	.long	68292864
	.long	67244040
	.long	68292616
	.long	67244296
	.long	68292872
	.long	0
	.long	268435456
	.long	65536
	.long	268500992
	.long	4
	.long	268435460
	.long	65540
	.long	268500996
	.long	536870912
	.long	805306368
	.long	536936448
	.long	805371904
	.long	536870916
	.long	805306372
	.long	536936452
	.long	805371908
	.long	1048576
	.long	269484032
	.long	1114112
	.long	269549568
	.long	1048580
	.long	269484036
	.long	1114116
	.long	269549572
	.long	537919488
	.long	806354944
	.long	537985024
	.long	806420480
	.long	537919492
	.long	806354948
	.long	537985028
	.long	806420484
	.long	4096
	.long	268439552
	.long	69632
	.long	268505088
	.long	4100
	.long	268439556
	.long	69636
	.long	268505092
	.long	536875008
	.long	805310464
	.long	536940544
	.long	805376000
	.long	536875012
	.long	805310468
	.long	536940548
	.long	805376004
	.long	1052672
	.long	269488128
	.long	1118208
	.long	269553664
	.long	1052676
	.long	269488132
	.long	1118212
	.long	269553668
	.long	537923584
	.long	806359040
	.long	537989120
	.long	806424576
	.long	537923588
	.long	806359044
	.long	537989124
	.long	806424580
	.long	0
	.long	134217728
	.long	8
	.long	134217736
	.long	1024
	.long	134218752
	.long	1032
	.long	134218760
	.long	131072
	.long	134348800
	.long	131080
	.long	134348808
	.long	132096
	.long	134349824
	.long	132104
	.long	134349832
	.long	1
	.long	134217729
	.long	9
	.long	134217737
	.long	1025
	.long	134218753
	.long	1033
	.long	134218761
	.long	131073
	.long	134348801
	.long	131081
	.long	134348809
	.long	132097
	.long	134349825
	.long	132105
	.long	134349833
	.long	33554432
	.long	167772160
	.long	33554440
	.long	167772168
	.long	33555456
	.long	167773184
	.long	33555464
	.long	167773192
	.long	33685504
	.long	167903232
	.long	33685512
	.long	167903240
	.long	33686528
	.long	167904256
	.long	33686536
	.long	167904264
	.long	33554433
	.long	167772161
	.long	33554441
	.long	167772169
	.long	33555457
	.long	167773185
	.long	33555465
	.long	167773193
	.long	33685505
	.long	167903233
	.long	33685513
	.long	167903241
	.long	33686529
	.long	167904257
	.long	33686537
	.long	167904265
	.long	0
	.long	256
	.long	524288
	.long	524544
	.long	16777216
	.long	16777472
	.long	17301504
	.long	17301760
	.long	16
	.long	272
	.long	524304
	.long	524560
	.long	16777232
	.long	16777488
	.long	17301520
	.long	17301776
	.long	2097152
	.long	2097408
	.long	2621440
	.long	2621696
	.long	18874368
	.long	18874624
	.long	19398656
	.long	19398912
	.long	2097168
	.long	2097424
	.long	2621456
	.long	2621712
	.long	18874384
	.long	18874640
	.long	19398672
	.long	19398928
	.long	512
	.long	768
	.long	524800
	.long	525056
	.long	16777728
	.long	16777984
	.long	17302016
	.long	17302272
	.long	528
	.long	784
	.long	524816
	.long	525072
	.long	16777744
	.long	16778000
	.long	17302032
	.long	17302288
	.long	2097664
	.long	2097920
	.long	2621952
	.long	2622208
	.long	18874880
	.long	18875136
	.long	19399168
	.long	19399424
	.long	2097680
	.long	2097936
	.long	2621968
	.long	2622224
	.long	18874896
	.long	18875152
	.long	19399184
	.long	19399440
	.long	0
	.long	67108864
	.long	262144
	.long	67371008
	.long	2
	.long	67108866
	.long	262146
	.long	67371010
	.long	8192
	.long	67117056
	.long	270336
	.long	67379200
	.long	8194
	.long	67117058
	.long	270338
	.long	67379202
	.long	32
	.long	67108896
	.long	262176
	.long	67371040
	.long	34
	.long	67108898
	.long	262178
	.long	67371042
	.long	8224
	.long	67117088
	.long	270368
	.long	67379232
	.long	8226
	.long	67117090
	.long	270370
	.long	67379234
	.long	2048
	.long	67110912
	.long	264192
	.long	67373056
	.long	2050
	.long	67110914
	.long	264194
	.long	67373058
	.long	10240
	.long	67119104
	.long	272384
	.long	67381248
	.long	10242
	.long	67119106
	.long	272386
	.long	67381250
	.long	2080
	.long	67110944
	.long	264224
	.long	67373088
	.long	2082
	.long	67110946
	.long	264226
	.long	67373090
	.long	10272
	.long	67119136
	.long	272416
	.long	67381280
	.long	10274
	.long	67119138
	.long	272418
	.long	67381282
	.align 32
	.type	odd_parity, @object
	.size	odd_parity, 256
odd_parity:
	.ascii	"\001\001\002\002\004\004\007\007\b\b\013\013\r\r\016\016\020"
	.ascii	"\020\023\023\025\025\026\026\031\031\032\032\034\034\037\037"
	.ascii	"  ##%%&&))**,,//1122447788;;==>>@@CCEEFFIIJJLLOOQQRRTTWWXX[["
	.ascii	"]]^^aabbddgghhkkmmnnppssuuvvyyzz||\177\177\200\200\203\203\205"
	.ascii	"\205\206\206\211\211\212\212\214\214\217\217\221\221\222\222"
	.ascii	"\224\224\227\227\230\230\233\233\235\235\236\236\241\241\242"
	.ascii	"\242\244\244\247\247\250\250\253\253\255\255\256\256\260\260"
	.ascii	"\263\263\265\265\266\266\271\271\272\272\274\274\277\277\301"
	.ascii	"\301\302\302\304\304\307\307\310\310\313\313\315\315\316\316"
	.ascii	"\320\320\323\323\325\325\326\326\331\331\332\332\334\334\337"
	.ascii	"\337\340\340\343\343\345\345\346\346\351\351\352\352\354\354"
	.ascii	"\357\357\361\361\362\362\364\364\367\367\370\370\373\373\375"
	.ascii	"\375\376\376"
	.globl	_shadow_DES_check_key
	.bss
	.align 4
	.type	_shadow_DES_check_key, @object
	.size	_shadow_DES_check_key, 4
_shadow_DES_check_key:
	.zero	4
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
