	.file	"rsa_mp.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/rsa/rsa_mp.c"
	.text
	.p2align 4
	.globl	rsa_multip_info_free_ex
	.type	rsa_multip_info_free_ex, @function
rsa_multip_info_free_ex:
.LFB397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	24(%rdi), %rdi
	call	BN_clear_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$19, %edx
	popq	%r12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE397:
	.size	rsa_multip_info_free_ex, .-rsa_multip_info_free_ex
	.p2align 4
	.globl	rsa_multip_info_free
	.type	rsa_multip_info_free, @function
rsa_multip_info_free:
.LFB398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	BN_clear_free@PLT
	movq	8(%r12), %rdi
	call	BN_clear_free@PLT
	movq	16(%r12), %rdi
	call	BN_clear_free@PLT
	movq	24(%r12), %rdi
	call	BN_clear_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$19, %edx
	popq	%r12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE398:
	.size	rsa_multip_info_free, .-rsa_multip_info_free
	.p2align 4
	.globl	rsa_multip_info_new
	.type	rsa_multip_info_new, @function
rsa_multip_info_new:
.LFB399:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$36, %edx
	movl	$40, %edi
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L17
	call	BN_secure_new@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L9
	call	BN_secure_new@PLT
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L16
	call	BN_secure_new@PLT
	movq	%rax, 16(%r12)
	testq	%rax, %rax
	je	.L16
	call	BN_secure_new@PLT
	movq	%rax, 24(%r12)
	testq	%rax, %rax
	je	.L16
.L6:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movq	(%r12), %rdi
.L9:
	call	BN_free@PLT
	movq	8(%r12), %rdi
	call	BN_free@PLT
	movq	16(%r12), %rdi
	call	BN_free@PLT
	movq	24(%r12), %rdi
	call	BN_free@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movl	$56, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movl	$37, %r8d
	movl	$65, %edx
	movl	$166, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L6
	.cfi_endproc
.LFE399:
	.size	rsa_multip_info_new, .-rsa_multip_info_new
	.p2align 4
	.globl	rsa_multip_calc_product
	.type	rsa_multip_calc_product, @function
rsa_multip_calc_product:
.LFB400:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	88(%rdi), %rdi
	call	OPENSSL_sk_num@PLT
	movl	%eax, -60(%rbp)
	testl	%eax, %eax
	jle	.L21
	call	BN_CTX_new@PLT
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L21
	movq	48(%r13), %r14
	movq	56(%r13), %r15
	xorl	%r12d, %r12d
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L22:
	movq	-56(%rbp), %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	BN_mul@PLT
	testl	%eax, %eax
	je	.L20
	movq	24(%rbx), %r14
	movq	(%rbx), %r15
	addl	$1, %r12d
	cmpl	-60(%rbp), %r12d
	je	.L33
.L23:
	movq	88(%r13), %rdi
	movl	%r12d, %esi
	call	OPENSSL_sk_value@PLT
	movq	24(%rax), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	jne	.L22
	call	BN_secure_new@PLT
	movq	%rax, 24(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L22
	xorl	%eax, %eax
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L21:
	movq	$0, -56(%rbp)
	xorl	%eax, %eax
.L20:
	movq	-56(%rbp), %rdi
	movl	%eax, -60(%rbp)
	call	BN_CTX_free@PLT
	movl	-60(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	movl	$1, %eax
	jmp	.L20
	.cfi_endproc
.LFE400:
	.size	rsa_multip_calc_product, .-rsa_multip_calc_product
	.p2align 4
	.globl	rsa_multip_cap
	.type	rsa_multip_cap, @function
rsa_multip_cap:
.LFB401:
	.cfi_startproc
	endbr64
	movl	$2, %eax
	cmpl	$1023, %edi
	jle	.L34
	movl	$3, %eax
	cmpl	$4095, %edi
	jg	.L39
.L34:
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	xorl	%eax, %eax
	cmpl	$8191, %edi
	setg	%al
	addl	$4, %eax
	ret
	.cfi_endproc
.LFE401:
	.size	rsa_multip_cap, .-rsa_multip_cap
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
