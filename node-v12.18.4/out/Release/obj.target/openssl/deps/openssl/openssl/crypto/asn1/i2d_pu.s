	.file	"i2d_pu.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/asn1/i2d_pu.c"
	.text
	.p2align 4
	.globl	i2d_PublicKey
	.type	i2d_PublicKey, @function
i2d_PublicKey:
.LFB419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	EVP_PKEY_id@PLT
	cmpl	$116, %eax
	je	.L2
	cmpl	$408, %eax
	je	.L3
	cmpl	$6, %eax
	je	.L7
	movl	$35, %r8d
	movl	$167, %edx
	movl	$164, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%r12
	movl	$-1, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movq	%r12, %rdi
	call	EVP_PKEY_get0_DSA@PLT
	popq	%r12
	movq	%r13, %rsi
	popq	%r13
	movq	%rax, %rdi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	i2d_DSAPublicKey@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	movq	%r12, %rdi
	call	EVP_PKEY_get0_RSA@PLT
	popq	%r12
	movq	%r13, %rsi
	popq	%r13
	movq	%rax, %rdi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	i2d_RSAPublicKey@PLT
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	movq	%r12, %rdi
	call	EVP_PKEY_get0_EC_KEY@PLT
	popq	%r12
	movq	%r13, %rsi
	popq	%r13
	movq	%rax, %rdi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	i2o_ECPublicKey@PLT
	.cfi_endproc
.LFE419:
	.size	i2d_PublicKey, .-i2d_PublicKey
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
