	.file	"bss_log.c"
	.text
	.p2align 4
	.type	slg_free, @function
slg_free:
.LFB271:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L5
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	closelog@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE271:
	.size	slg_free, .-slg_free
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"application"
	.text
	.p2align 4
	.type	slg_new, @function
slg_new:
.LFB270:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edx
	movl	$3, %esi
	movl	$1, 32(%rdi)
	movl	$0, 48(%rdi)
	movq	$0, 56(%rdi)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	leaq	.LC0(%rip), %rdi
	call	openlog@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE270:
	.size	slg_new, .-slg_new
	.p2align 4
	.type	slg_ctrl, @function
slg_ctrl:
.LFB273:
	.cfi_startproc
	endbr64
	cmpl	$4, %esi
	je	.L19
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	call	closelog@PLT
	movl	%ebx, %edx
	movq	%r12, %rdi
	movl	$3, %esi
	call	openlog@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE273:
	.size	slg_ctrl, .-slg_ctrl
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../deps/openssl/openssl/crypto/bio/bss_log.c"
	.section	.rodata.str1.1
.LC2:
	.string	"%s"
	.text
	.p2align 4
	.type	slg_write, @function
slg_write:
.LFB272:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	1(%rdx), %edi
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	movl	$199, %edx
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	leaq	.LC1(%rip), %rsi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L26
	movslq	%r14d, %rbx
	movq	%rax, %r15
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rbx, %rdx
	xorl	%r13d, %r13d
	call	memcpy@PLT
	movb	$0, (%r15,%rbx)
	leaq	4+mapping.8452(%rip), %rbx
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L24:
	addl	$1, %r13d
.L23:
	movslq	-4(%rbx), %r12
	movq	%rbx, %rsi
	movq	%r15, %rdi
	addq	$20, %rbx
	movq	%r12, %rdx
	call	strncmp@PLT
	testl	%eax, %eax
	jne	.L24
	movslq	%r13d, %r13
	leaq	(%r15,%r12), %rcx
	leaq	mapping.8452(%rip), %rax
	movl	$1, %esi
	leaq	0(%r13,%r13,4), %rdi
	leaq	.LC2(%rip), %rdx
	movl	16(%rax,%rdi,4), %edi
	xorl	%eax, %eax
	call	__syslog_chk@PLT
	movq	%r15, %rdi
	movl	$214, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movl	$200, %r8d
	movl	$65, %edx
	movl	$155, %esi
	movl	$32, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE272:
	.size	slg_write, .-slg_write
	.p2align 4
	.type	slg_puts, @function
slg_puts:
.LFB274:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	call	strlen@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	slg_write
	.cfi_endproc
.LFE274:
	.size	slg_puts, .-slg_puts
	.p2align 4
	.globl	BIO_s_log
	.type	BIO_s_log, @function
BIO_s_log:
.LFB269:
	.cfi_startproc
	endbr64
	leaq	methods_slg(%rip), %rax
	ret
	.cfi_endproc
.LFE269:
	.size	BIO_s_log, .-BIO_s_log
	.section	.rodata
	.align 32
	.type	mapping.8452, @object
	.size	mapping.8452, 400
mapping.8452:
	.long	6
	.string	"PANIC "
	.zero	3
	.zero	2
	.long	0
	.long	6
	.string	"EMERG "
	.zero	3
	.zero	2
	.long	0
	.long	4
	.string	"EMR "
	.zero	5
	.zero	2
	.long	0
	.long	6
	.string	"ALERT "
	.zero	3
	.zero	2
	.long	1
	.long	4
	.string	"ALR "
	.zero	5
	.zero	2
	.long	1
	.long	5
	.string	"CRIT "
	.zero	4
	.zero	2
	.long	2
	.long	4
	.string	"CRI "
	.zero	5
	.zero	2
	.long	2
	.long	6
	.string	"ERROR "
	.zero	3
	.zero	2
	.long	3
	.long	4
	.string	"ERR "
	.zero	5
	.zero	2
	.long	3
	.long	8
	.string	"WARNING "
	.zero	1
	.zero	2
	.long	4
	.long	5
	.string	"WARN "
	.zero	4
	.zero	2
	.long	4
	.long	4
	.string	"WAR "
	.zero	5
	.zero	2
	.long	4
	.long	7
	.string	"NOTICE "
	.zero	2
	.zero	2
	.long	5
	.long	5
	.string	"NOTE "
	.zero	4
	.zero	2
	.long	5
	.long	4
	.string	"NOT "
	.zero	5
	.zero	2
	.long	5
	.long	5
	.string	"INFO "
	.zero	4
	.zero	2
	.long	6
	.long	4
	.string	"INF "
	.zero	5
	.zero	2
	.long	6
	.long	6
	.string	"DEBUG "
	.zero	3
	.zero	2
	.long	7
	.long	4
	.string	"DBG "
	.zero	5
	.zero	2
	.long	7
	.long	0
	.string	""
	.zero	9
	.zero	2
	.long	3
	.section	.rodata.str1.1
.LC3:
	.string	"syslog"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	methods_slg, @object
	.size	methods_slg, 96
methods_slg:
	.long	1025
	.zero	4
	.quad	.LC3
	.quad	bwrite_conv
	.quad	slg_write
	.quad	0
	.quad	0
	.quad	slg_puts
	.quad	0
	.quad	slg_ctrl
	.quad	slg_new
	.quad	slg_free
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
