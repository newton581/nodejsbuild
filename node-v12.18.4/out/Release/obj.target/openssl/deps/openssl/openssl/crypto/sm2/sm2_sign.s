	.file	"sm2_sign.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/sm2/sm2_sign.c"
	.text
	.p2align 4
	.type	sm2_sig_gen, @function
sm2_sig_gen:
.LFB384:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -72(%rbp)
	call	EC_KEY_get0_private_key@PLT
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	call	EC_KEY_get0_group@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	EC_GROUP_get0_order@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	EC_POINT_new@PLT
	movq	%rax, %r14
	call	BN_CTX_new@PLT
	movq	%rax, %r12
	testq	%r14, %r14
	je	.L17
	testq	%rax, %rax
	je	.L17
	movq	%rax, %rdi
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	BN_CTX_get@PLT
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L49
	call	BN_new@PLT
	movq	%rax, %r15
	call	BN_new@PLT
	movq	%rax, -88(%rbp)
	testq	%r15, %r15
	je	.L18
	testq	%rax, %rax
	je	.L18
.L11:
	movq	-56(%rbp), %rdi
	movq	%rbx, %rsi
	call	BN_priv_rand_range@PLT
	testl	%eax, %eax
	je	.L50
	movq	-56(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %r9
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	EC_POINT_mul@PLT
	testl	%eax, %eax
	je	.L10
	movq	-64(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%r12, %r8
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	EC_POINT_get_affine_coordinates@PLT
	testl	%eax, %eax
	je	.L10
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rsi
	movq	%r12, %r8
	movq	%rbx, %rcx
	movq	%r15, %rdi
	call	BN_mod_add@PLT
	testl	%eax, %eax
	je	.L10
	movq	%r15, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L11
	movq	-56(%rbp), %rdx
	movq	-80(%rbp), %rdi
	movq	%r15, %rsi
	call	BN_add@PLT
	testl	%eax, %eax
	je	.L51
	movq	-80(%rbp), %rdi
	movq	%rbx, %rsi
	call	BN_cmp@PLT
	testl	%eax, %eax
	je	.L11
	call	BN_value_one@PLT
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rdi
	movq	%rax, %rdx
	call	BN_add@PLT
	testl	%eax, %eax
	je	.L14
	movq	-88(%rbp), %rdx
	movq	%r12, %rcx
	movq	%r13, %rdi
	movq	%rdx, %rsi
	call	ec_group_do_inverse_ord@PLT
	testl	%eax, %eax
	je	.L14
	movq	-104(%rbp), %r13
	movq	-96(%rbp), %rsi
	movq	%r12, %r8
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	BN_mod_mul@PLT
	testl	%eax, %eax
	je	.L14
	movq	-56(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r13, %rdi
	call	BN_sub@PLT
	testl	%eax, %eax
	je	.L14
	movq	-88(%rbp), %rsi
	movq	%r12, %r8
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%rsi, %rdi
	call	BN_mod_mul@PLT
	testl	%eax, %eax
	je	.L14
	call	ECDSA_SIG_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L52
	movq	-88(%rbp), %rdx
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	ECDSA_SIG_set0@PLT
.L16:
	movq	%r12, %rdi
	call	BN_CTX_free@PLT
	movq	%r14, %rdi
	call	EC_POINT_free@PLT
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	movl	$260, %r8d
	movl	$3, %edx
	movl	$106, %esi
	movl	$53, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L4:
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	BN_free@PLT
	movq	-88(%rbp), %rdi
	call	BN_free@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L17:
	movl	$203, %r8d
.L45:
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$106, %esi
	xorl	%r15d, %r15d
	movl	$53, %edi
	call	ERR_put_error@PLT
	movq	$0, -88(%rbp)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L10:
	movl	$239, %r8d
.L46:
	movl	$68, %edx
	movl	$106, %esi
	movl	$53, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$225, %r8d
.L47:
	movl	$65, %edx
	movl	$106, %esi
	movl	$53, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L49:
	movl	$213, %r8d
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L50:
	movl	$231, %r8d
	jmp	.L46
.L51:
	movl	$248, %r8d
	jmp	.L46
.L52:
	movl	$266, %r8d
	jmp	.L47
	.cfi_endproc
.LFE384:
	.size	sm2_sig_gen, .-sm2_sig_gen
	.p2align 4
	.type	sm2_sig_verify, @function
sm2_sig_verify:
.LFB385:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	%rdx, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EC_KEY_get0_group@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	EC_GROUP_get0_order@PLT
	movq	$0, -72(%rbp)
	movq	%rax, -88(%rbp)
	movq	$0, -64(%rbp)
	call	BN_CTX_new@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	EC_POINT_new@PLT
	movq	%rax, %r14
	testq	%r12, %r12
	je	.L66
	testq	%rax, %rax
	je	.L66
	movq	%r12, %rdi
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	BN_CTX_get@PLT
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L74
	leaq	-72(%rbp), %rsi
	movq	%rbx, %rdi
	leaq	-64(%rbp), %rdx
	call	ECDSA_SIG_get0@PLT
	call	BN_value_one@PLT
	movq	-72(%rbp), %rdi
	movq	%rax, %rsi
	call	BN_cmp@PLT
	testl	%eax, %eax
	js	.L59
	call	BN_value_one@PLT
	movq	-64(%rbp), %rdi
	movq	%rax, %rsi
	call	BN_cmp@PLT
	testl	%eax, %eax
	js	.L59
	movq	-88(%rbp), %rbx
	movq	-72(%rbp), %rsi
	movq	%rbx, %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	jle	.L59
	movq	-64(%rbp), %rsi
	movq	%rbx, %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	jle	.L59
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rsi
	movq	%r12, %r8
	movq	%rbx, %rcx
	movq	-104(%rbp), %rdi
	call	BN_mod_add@PLT
	movl	$335, %r8d
	movl	%eax, %ebx
	testl	%eax, %eax
	je	.L73
	movq	-104(%rbp), %rdi
	call	BN_is_zero@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	jne	.L75
	movq	%r15, %rdi
	call	EC_KEY_get0_public_key@PLT
	movq	-104(%rbp), %r8
	movq	-64(%rbp), %rdx
	movq	%r12, %r9
	movq	%rax, %rcx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	EC_POINT_mul@PLT
	testl	%eax, %eax
	je	.L63
	movq	-112(%rbp), %r15
	xorl	%ecx, %ecx
	movq	%r12, %r8
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%r15, %rdx
	call	EC_POINT_get_affine_coordinates@PLT
	testl	%eax, %eax
	je	.L63
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %rsi
	movq	%r12, %r8
	movq	%r15, %rdx
	movq	-104(%rbp), %rdi
	call	BN_mod_add@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	je	.L76
	movq	-104(%rbp), %rsi
	movq	-72(%rbp), %rdi
	xorl	%ebx, %ebx
	call	BN_cmp@PLT
	testl	%eax, %eax
	sete	%bl
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L59:
	movl	$330, %r8d
.L72:
	leaq	.LC0(%rip), %rcx
	movl	$101, %edx
	movl	$107, %esi
	xorl	%ebx, %ebx
	movl	$53, %edi
	call	ERR_put_error@PLT
.L56:
	movq	%r14, %rdi
	call	EC_POINT_free@PLT
	movq	%r12, %rdi
	call	BN_CTX_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L77
	addq	$72, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movl	$302, %r8d
.L71:
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$107, %esi
	xorl	%ebx, %ebx
	movl	$53, %edi
	call	ERR_put_error@PLT
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L74:
	movl	$310, %r8d
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L75:
	movl	$340, %r8d
	jmp	.L72
.L76:
	movl	$351, %r8d
	.p2align 4,,10
	.p2align 3
.L73:
	movl	$3, %edx
	movl	$107, %esi
	movl	$53, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L63:
	movl	$346, %r8d
	movl	$16, %edx
	movl	$107, %esi
	movl	$53, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L56
.L77:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE385:
	.size	sm2_sig_verify, .-sm2_sig_verify
	.p2align 4
	.globl	sm2_compute_z_digest
	.type	sm2_compute_z_digest, @function
sm2_compute_z_digest:
.LFB382:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%rdx, -88(%rbp)
	movq	%rcx, -72(%rbp)
	movq	%rdi, -96(%rbp)
	movq	%r8, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EC_KEY_get0_group@PLT
	movb	$0, -57(%rbp)
	movq	%rax, -80(%rbp)
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r13
	call	BN_CTX_new@PLT
	movq	%rax, %r12
	testq	%r13, %r13
	je	.L93
	testq	%rax, %rax
	je	.L93
	movq	%rax, %rdi
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -120(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -128(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -144(%rbp)
	call	BN_CTX_get@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L127
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	EVP_DigestInit@PLT
	movl	$65, %r8d
	movl	%eax, %ebx
	testl	%eax, %eax
	je	.L126
	cmpq	$8190, -72(%rbp)
	ja	.L128
	movzwl	-72(%rbp), %eax
	leaq	-57(%rbp), %rsi
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%rsi, -152(%rbp)
	leal	0(,%rax,8), %ecx
	movl	%ecx, %eax
	movl	%ecx, -156(%rbp)
	shrw	$8, %ax
	movb	%al, -57(%rbp)
	call	EVP_DigestUpdate@PLT
	movq	-152(%rbp), %rsi
	movl	-156(%rbp), %ecx
	testl	%eax, %eax
	movl	%eax, %ebx
	je	.L129
	movl	$1, %edx
	movq	%r13, %rdi
	movb	%cl, -57(%rbp)
	call	EVP_DigestUpdate@PLT
	movl	$86, %r8d
	movl	%eax, %ebx
	testl	%eax, %eax
	je	.L126
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L87
	movq	-88(%rbp), %rsi
	movq	%rax, %rdx
	movq	%r13, %rdi
	call	EVP_DigestUpdate@PLT
	movl	$91, %r8d
	movl	%eax, %ebx
	testl	%eax, %eax
	je	.L126
.L87:
	movq	-120(%rbp), %rcx
	movq	-112(%rbp), %rdx
	movq	%r12, %r8
	movq	-104(%rbp), %rsi
	movq	-80(%rbp), %rdi
	call	EC_GROUP_get_curve@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	je	.L130
	movq	-104(%rbp), %rdi
	call	BN_num_bits@PLT
	movl	$101, %edx
	leaq	.LC0(%rip), %rsi
	leal	14(%rax), %ebx
	addl	$7, %eax
	cmovns	%eax, %ebx
	sarl	$3, %ebx
	movslq	%ebx, %rax
	movl	%ebx, -72(%rbp)
	movq	%rax, %rdi
	movq	%rax, -88(%rbp)
	call	CRYPTO_zalloc@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L131
	movl	-72(%rbp), %edx
	movq	-112(%rbp), %rdi
	movq	%rax, %rsi
	call	BN_bn2binpad@PLT
	testl	%eax, %eax
	js	.L91
	movq	-88(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jne	.L132
.L91:
	movl	$126, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$68, %edx
	movq	%rbx, %r15
	movl	$113, %esi
	movl	$53, %edi
	xorl	%ebx, %ebx
	call	ERR_put_error@PLT
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L129:
	movl	$81, %r8d
.L126:
	leaq	.LC0(%rip), %rcx
	movl	$6, %edx
	movl	$113, %esi
	xorl	%r15d, %r15d
	movl	$53, %edi
	call	ERR_put_error@PLT
.L81:
	movl	$133, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	call	BN_CTX_free@PLT
	movq	%r13, %rdi
	call	EVP_MD_CTX_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L133
	addq	$120, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	movl	$47, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
.L125:
	movl	$113, %esi
	movl	$53, %edi
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	call	ERR_put_error@PLT
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L128:
	movl	$73, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$111, %edx
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L127:
	movl	$60, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%ebx, %ebx
	movl	$113, %esi
	movl	$53, %edi
	call	ERR_put_error@PLT
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L132:
	movl	-72(%rbp), %edx
	movq	-120(%rbp), %rdi
	movq	%rbx, %rsi
	call	BN_bn2binpad@PLT
	testl	%eax, %eax
	js	.L91
	movq	-88(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L91
	movq	-80(%rbp), %rdi
	call	EC_GROUP_get0_generator@PLT
	movq	-128(%rbp), %rdx
	movq	-80(%rbp), %rdi
	movq	%r12, %r8
	movq	-136(%rbp), %rcx
	movq	%rax, %rsi
	call	EC_POINT_get_affine_coordinates@PLT
	testl	%eax, %eax
	je	.L91
	movl	-72(%rbp), %edx
	movq	-128(%rbp), %rdi
	movq	%rbx, %rsi
	call	BN_bn2binpad@PLT
	testl	%eax, %eax
	js	.L91
	movq	-88(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L91
	movl	-72(%rbp), %edx
	movq	-136(%rbp), %rdi
	movq	%rbx, %rsi
	call	BN_bn2binpad@PLT
	testl	%eax, %eax
	js	.L91
	movq	-88(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L91
	movq	%r14, %rdi
	call	EC_KEY_get0_public_key@PLT
	movq	-80(%rbp), %rdi
	movq	%r12, %r8
	movq	%r15, %rcx
	movq	-144(%rbp), %r14
	movq	%rax, %rsi
	movq	%r14, %rdx
	call	EC_POINT_get_affine_coordinates@PLT
	testl	%eax, %eax
	je	.L91
	movl	-72(%rbp), %edx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	BN_bn2binpad@PLT
	testl	%eax, %eax
	js	.L91
	movq	-88(%rbp), %r14
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r14, %rdx
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L91
	movl	-72(%rbp), %edx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	BN_bn2binpad@PLT
	testl	%eax, %eax
	js	.L91
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L91
	movq	-96(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	EVP_DigestFinal@PLT
	testl	%eax, %eax
	je	.L91
	movq	%rbx, %r15
	movl	$1, %ebx
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L130:
	movl	$96, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$16, %edx
	xorl	%r15d, %r15d
	movl	$113, %esi
	movl	$53, %edi
	call	ERR_put_error@PLT
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L131:
	movl	$103, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r15d, %r15d
	movl	$113, %esi
	movl	$53, %edi
	call	ERR_put_error@PLT
	jmp	.L81
.L133:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE382:
	.size	sm2_compute_z_digest, .-sm2_compute_z_digest
	.p2align 4
	.type	sm2_compute_msg_hash, @function
sm2_compute_msg_hash:
.LFB383:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%rdx, -64(%rbp)
	movq	%rcx, -72(%rbp)
	movq	%r8, -80(%rbp)
	movq	%r9, -88(%rbp)
	call	EVP_MD_CTX_new@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	EVP_MD_size@PLT
	movl	%eax, -52(%rbp)
	testl	%eax, %eax
	js	.L154
	movslq	-52(%rbp), %r15
	movl	$155, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r14
	testq	%r12, %r12
	je	.L142
	testq	%rax, %rax
	je	.L142
	movq	-72(%rbp), %rcx
	movq	-64(%rbp), %rdx
	movq	%rbx, %r8
	movq	%r13, %rsi
	movq	%rax, %rdi
	xorl	%ebx, %ebx
	call	sm2_compute_z_digest
	testl	%eax, %eax
	je	.L136
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	EVP_DigestInit@PLT
	testl	%eax, %eax
	je	.L140
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L140
	movq	-88(%rbp), %rdx
	movq	-80(%rbp), %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L140
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	EVP_DigestFinal@PLT
	testl	%eax, %eax
	je	.L140
	movl	-52(%rbp), %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	BN_bin2bn@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L155
.L136:
	movq	%r14, %rdi
	movl	$180, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	call	EVP_MD_CTX_free@PLT
	addq	$56, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	movl	$171, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$6, %edx
	xorl	%ebx, %ebx
	movl	$100, %esi
	movl	$53, %edi
	call	ERR_put_error@PLT
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L142:
	movl	$157, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%ebx, %ebx
	movl	$100, %esi
	movl	$53, %edi
	call	ERR_put_error@PLT
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L154:
	movl	$151, %r8d
	movl	$102, %edx
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	leaq	.LC0(%rip), %rcx
	movl	$100, %esi
	movl	$53, %edi
	call	ERR_put_error@PLT
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L155:
	movl	$177, %r8d
	movl	$68, %edx
	movl	$100, %esi
	movl	$53, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L136
	.cfi_endproc
.LFE383:
	.size	sm2_compute_msg_hash, .-sm2_compute_msg_hash
	.p2align 4
	.globl	sm2_do_sign
	.type	sm2_do_sign, @function
sm2_do_sign:
.LFB386:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%r13, %rsi
	call	sm2_compute_msg_hash
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L158
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	sm2_sig_gen
	movq	%rax, %r13
.L157:
	movq	%r12, %rdi
	call	BN_free@PLT
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	xorl	%r13d, %r13d
	jmp	.L157
	.cfi_endproc
.LFE386:
	.size	sm2_do_sign, .-sm2_do_sign
	.p2align 4
	.globl	sm2_do_verify
	.type	sm2_do_verify, @function
sm2_do_verify:
.LFB387:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movq	%rcx, %rdx
	pushq	%r14
	movq	%r8, %rcx
	movq	%r9, %r8
	movq	16(%rbp), %r9
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	xorl	%r14d, %r14d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r13, %rsi
	call	sm2_compute_msg_hash
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L161
	movq	%rax, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	sm2_sig_verify
	movl	%eax, %r14d
.L161:
	movq	%r12, %rdi
	call	BN_free@PLT
	popq	%r12
	movl	%r14d, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE387:
	.size	sm2_do_verify, .-sm2_do_verify
	.p2align 4
	.globl	sm2_sign
	.type	sm2_sign, @function
sm2_sign:
.LFB388:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%r8, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rcx, %rbx
	subq	$16, %rsp
	movq	%rdx, -40(%rbp)
	xorl	%edx, %edx
	call	BN_bin2bn@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L171
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	sm2_sig_gen
	leaq	-40(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	i2d_ECDSA_SIG@PLT
	testl	%eax, %eax
	js	.L172
	movl	%eax, (%rbx)
	movl	$1, %r14d
.L168:
	movq	%r13, %rdi
	call	ECDSA_SIG_free@PLT
	movq	%r12, %rdi
	call	BN_free@PLT
	addq	$16, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	movl	$419, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$3, %edx
	xorl	%r13d, %r13d
	movl	$105, %esi
	movl	$53, %edi
	movl	$-1, %r14d
	call	ERR_put_error@PLT
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L172:
	movl	$427, %r8d
	movl	$68, %edx
	movl	$105, %esi
	movl	$53, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r14d
	call	ERR_put_error@PLT
	jmp	.L168
	.cfi_endproc
.LFE388:
	.size	sm2_sign, .-sm2_sign
	.p2align 4
	.globl	sm2_verify
	.type	sm2_verify, @function
sm2_verify:
.LFB389:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$56, %rsp
	movq	%rdi, -88(%rbp)
	movq	%r8, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	$0, -64(%rbp)
	call	ECDSA_SIG_new@PLT
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L182
	movslq	%ebx, %r13
	leaq	-72(%rbp), %rsi
	leaq	-80(%rbp), %rdi
	movq	%r13, %rdx
	call	d2i_ECDSA_SIG@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L183
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rsi
	call	i2d_ECDSA_SIG@PLT
	cmpl	%eax, %ebx
	jne	.L177
	movq	-64(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L177
	movq	-88(%rbp), %rdi
	xorl	%edx, %edx
	movl	%r14d, %esi
	call	BN_bin2bn@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L184
	movq	-80(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	%rax, %rdx
	call	sm2_sig_verify
	movl	%eax, %r12d
.L175:
	movq	-64(%rbp), %rdi
	movl	$475, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r15, %rdi
	call	BN_free@PLT
	movq	-80(%rbp), %rdi
	call	ECDSA_SIG_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L185
	addq	$56, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	movl	$462, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$104, %edx
	movl	$108, %esi
	movl	$53, %edi
	movl	$-1, %r12d
	xorl	%r15d, %r15d
	call	ERR_put_error@PLT
	jmp	.L175
.L183:
	movl	$456, %r8d
	movl	$104, %edx
	movl	$108, %esi
	movl	$53, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L175
.L182:
	movl	$452, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movq	%rax, %r15
	movl	$108, %esi
	movl	$53, %edi
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L175
.L184:
	movl	$468, %r8d
	movl	$3, %edx
	movl	$108, %esi
	movl	$53, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L175
.L185:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE389:
	.size	sm2_verify, .-sm2_verify
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
