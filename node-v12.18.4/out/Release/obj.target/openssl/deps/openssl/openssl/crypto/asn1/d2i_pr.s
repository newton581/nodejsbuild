	.file	"d2i_pr.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/asn1/d2i_pr.c"
	.text
	.p2align 4
	.globl	d2i_PrivateKey
	.type	d2i_PrivateKey, @function
d2i_PrivateKey:
.LFB853:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	%rax, -64(%rbp)
	testq	%rsi, %rsi
	je	.L2
	movq	(%rsi), %r12
	testq	%r12, %r12
	je	.L2
	movq	24(%r12), %rdi
	call	ENGINE_finish@PLT
	movl	%r15d, %esi
	movq	%r12, %rdi
	movq	$0, 24(%r12)
	call	EVP_PKEY_set_type@PLT
	testl	%eax, %eax
	jne	.L6
.L36:
	movl	$41, %r8d
	movl	$163, %edx
	movl	$154, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L7:
	testq	%rbx, %rbx
	je	.L12
	cmpq	%r12, (%rbx)
	je	.L14
.L12:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	EVP_PKEY_free@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L2:
	call	EVP_PKEY_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L35
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	EVP_PKEY_set_type@PLT
	testl	%eax, %eax
	je	.L36
.L6:
	movq	16(%r12), %rax
	movq	184(%rax), %rcx
	testq	%rcx, %rcx
	je	.L8
	leaq	-64(%rbp), %rsi
	movl	%r14d, %edx
	movq	%r12, %rdi
	call	*%rcx
	testl	%eax, %eax
	je	.L37
.L9:
	movq	-64(%rbp), %rax
	movq	%rax, 0(%r13)
	testq	%rbx, %rbx
	je	.L1
	movq	%r12, (%rbx)
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L38
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	movq	16(%r12), %rax
.L8:
	cmpq	$0, 64(%rax)
	je	.L10
	movq	%r14, %rdx
	leaq	-64(%rbp), %rsi
	xorl	%edi, %edi
	call	d2i_PKCS8_PRIV_KEY_INFO@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L7
	movq	%rax, %rdi
	call	EVP_PKCS82PKEY@PLT
	movq	%r14, %rdi
	movq	%rax, %r15
	call	PKCS8_PRIV_KEY_INFO_free@PLT
	testq	%r15, %r15
	je	.L7
	movq	%r12, %rdi
	movq	%r15, %r12
	call	EVP_PKEY_free@PLT
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L10:
	movl	$60, %r8d
	movl	$13, %edx
	movl	$154, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L14:
	xorl	%r12d, %r12d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L35:
	movl	$29, %r8d
	movl	$6, %edx
	movl	$154, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1
.L38:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE853:
	.size	d2i_PrivateKey, .-d2i_PrivateKey
	.p2align 4
	.globl	d2i_AutoPrivateKey
	.type	d2i_AutoPrivateKey, @function
d2i_AutoPrivateKey:
.LFB854:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	xorl	%edi, %edi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-64(%rbp), %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rbx, %rsi
	movq	%rax, -64(%rbp)
	call	d2i_ASN1_SEQUENCE_ANY@PLT
	movq	%rax, %r13
	movq	(%r12), %rax
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	call	OPENSSL_sk_num@PLT
	cmpl	$6, %eax
	je	.L44
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	movl	$408, %r8d
	cmpl	$4, %eax
	jne	.L55
.L40:
	movq	ASN1_TYPE_free@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	movl	%r8d, -72(%rbp)
	call	OPENSSL_sk_pop_free@PLT
	movl	-72(%rbp), %r8d
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	%r15, %rsi
	movl	%r8d, %edi
	call	d2i_PrivateKey
.L39:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L56
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	movl	$6, %r8d
	cmpl	$3, %eax
	jne	.L40
	xorl	%edi, %edi
	movq	%r14, %rdx
	movq	%rbx, %rsi
	call	d2i_PKCS8_PRIV_KEY_INFO@PLT
	movq	ASN1_TYPE_free@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	OPENSSL_sk_pop_free@PLT
	testq	%r14, %r14
	je	.L57
	movq	%r14, %rdi
	call	EVP_PKCS82PKEY@PLT
	movq	%r14, %rdi
	movq	%rax, -72(%rbp)
	call	PKCS8_PRIV_KEY_INFO_free@PLT
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L39
	movq	-64(%rbp), %rdx
	movq	%rdx, (%r12)
	testq	%r15, %r15
	je	.L39
	movq	%rax, (%r15)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L44:
	movl	$116, %r8d
	jmp	.L40
.L57:
	movl	$108, %r8d
	movl	$167, %edx
	movl	$207, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L39
.L56:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE854:
	.size	d2i_AutoPrivateKey, .-d2i_AutoPrivateKey
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
