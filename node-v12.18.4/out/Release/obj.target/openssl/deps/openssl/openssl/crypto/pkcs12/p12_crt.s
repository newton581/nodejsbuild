	.file	"p12_crt.c"
	.text
	.p2align 4
	.globl	PKCS12_add_cert
	.type	PKCS12_add_cert, @function
PKCS12_add_cert:
.LFB805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$-1, -48(%rbp)
	movl	$-1, -44(%rbp)
	call	PKCS12_SAFEBAG_create_cert@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L3
	leaq	-48(%rbp), %rsi
	movq	%r13, %rdi
	call	X509_alias_get0@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L8
	movl	-48(%rbp), %edx
	movq	%r12, %rdi
	call	PKCS12_add_friendlyname_utf8@PLT
	testl	%eax, %eax
	je	.L3
.L8:
	leaq	-44(%rbp), %rsi
	movq	%r13, %rdi
	call	X509_keyid_get0@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L6
	movl	-44(%rbp), %edx
	movq	%r12, %rdi
	call	PKCS12_add_localkeyid@PLT
	testl	%eax, %eax
	je	.L3
.L6:
	testq	%rbx, %rbx
	je	.L1
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L11
	movq	%r12, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L3
.L1:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L31
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3
	movq	%r12, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L1
	movq	(%rbx), %rdi
	call	OPENSSL_sk_free@PLT
	movq	$0, (%rbx)
	.p2align 4,,10
	.p2align 3
.L3:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	PKCS12_SAFEBAG_free@PLT
	jmp	.L1
.L31:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE805:
	.size	PKCS12_add_cert, .-PKCS12_add_cert
	.p2align 4
	.globl	PKCS12_add_key
	.type	PKCS12_add_key, @function
PKCS12_add_key:
.LFB806:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%r8d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$24, %rsp
	movl	%edx, -52(%rbp)
	call	EVP_PKEY2PKCS8@PLT
	movl	-52(%rbp), %edx
	testq	%rax, %rax
	je	.L38
	movq	%rax, %r13
	testl	%edx, %edx
	jne	.L35
.L39:
	cmpl	$-1, %r12d
	je	.L60
	subq	$8, %rsp
	movl	$-1, %edx
	movl	%r12d, %edi
	movl	%r15d, %r9d
	pushq	%r13
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	call	PKCS12_SAFEBAG_create_pkcs8_encrypt@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	PKCS8_PRIV_KEY_INFO_free@PLT
	popq	%rax
	popq	%rdx
.L40:
	testq	%r12, %r12
	je	.L38
	testq	%rbx, %rbx
	je	.L32
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L42
	movq	%r12, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L34
.L32:
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L34
	movq	%r12, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L32
	movq	(%rbx), %rdi
	call	OPENSSL_sk_free@PLT
	movq	$0, (%rbx)
.L34:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	PKCS12_SAFEBAG_free@PLT
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L35:
	movl	%edx, %esi
	movq	%rax, %rdi
	call	PKCS8_add_keyusage@PLT
	testl	%eax, %eax
	jne	.L39
	.p2align 4,,10
	.p2align 3
.L38:
	xorl	%r12d, %r12d
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	PKCS12_SAFEBAG_free@PLT
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L60:
	movq	%r13, %rdi
	call	PKCS12_SAFEBAG_create0_p8inf@PLT
	movq	%rax, %r12
	jmp	.L40
	.cfi_endproc
.LFE806:
	.size	PKCS12_add_key, .-PKCS12_add_key
	.p2align 4
	.globl	PKCS12_add_safe
	.type	PKCS12_add_safe, @function
PKCS12_add_safe:
.LFB807:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	movq	%r8, %rsi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	%edx, %edi
	subq	$24, %rsp
	cmpq	$0, (%rbx)
	je	.L78
.L62:
	testl	%edi, %edi
	je	.L72
	cmpl	$-1, %edi
	je	.L79
.L64:
	subq	$8, %rsp
	movl	$-1, %edx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	pushq	%r12
	call	PKCS12_pack_p7encdata@PLT
	movq	%rax, %r12
	popq	%rax
	popq	%rdx
.L65:
	testq	%r12, %r12
	je	.L69
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L69
	leaq	-24(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	testl	%r13d, %r13d
	jne	.L80
.L68:
	movq	%r12, %rdi
	call	PKCS7_free@PLT
	xorl	%eax, %eax
.L61:
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore_state
	movl	$149, %edi
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L80:
	movq	(%rbx), %rdi
	call	OPENSSL_sk_free@PLT
	movq	$0, (%rbx)
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L78:
	movq	%r8, -48(%rbp)
	movl	%ecx, -40(%rbp)
	movl	%edx, -36(%rbp)
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L71
	movq	-48(%rbp), %rsi
	movl	-40(%rbp), %r9d
	movl	$1, %r13d
	movl	-36(%rbp), %edi
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L79:
	movq	%r12, %rdi
	call	PKCS12_pack_p7data@PLT
	movq	%rax, %r12
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L71:
	xorl	%eax, %eax
	jmp	.L61
	.cfi_endproc
.LFE807:
	.size	PKCS12_add_safe, .-PKCS12_add_safe
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/pkcs12/p12_crt.c"
	.text
	.p2align 4
	.globl	PKCS12_create
	.type	PKCS12_create, @function
PKCS12_create:
.LFB804:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%r9d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$136, %rsp
	movq	%rdi, -176(%rbp)
	movl	24(%rbp), %edi
	movq	%rsi, -168(%rbp)
	movl	16(%rbp), %esi
	movl	32(%rbp), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	movl	$149, %eax
	cmovne	16(%rbp), %eax
	movq	$0, -144(%rbp)
	testl	%r9d, %r9d
	movl	%eax, 16(%rbp)
	movl	$146, %eax
	movq	$0, -136(%rbp)
	cmove	%eax, %r14d
	testl	%edi, %edi
	movl	$2048, %eax
	movl	$0, -148(%rbp)
	cmovne	24(%rbp), %eax
	testl	%r8d, %r8d
	movl	%eax, 24(%rbp)
	movl	$1, %eax
	cmovne	32(%rbp), %eax
	movl	%eax, 32(%rbp)
	movq	%rdx, %rax
	orq	%rcx, %rax
	jne	.L86
	testq	%rbx, %rbx
	je	.L87
.L94:
	xorl	%r15d, %r15d
	leaq	-136(%rbp), %r12
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L100:
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	PKCS12_add_cert
	testq	%rax, %rax
	je	.L188
	addl	$1, %r15d
.L88:
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r15d
	jl	.L100
	movq	-136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L101
	leaq	-144(%rbp), %r9
	movl	24(%rbp), %ecx
	movl	16(%rbp), %edx
	movq	%rdi, %rsi
	movq	-176(%rbp), %r8
	movq	%r9, %rdi
	call	PKCS12_add_safe
	movq	-136(%rbp), %rdi
	testl	%eax, %eax
	je	.L188
.L101:
	movq	PKCS12_SAFEBAG_free@GOTPCREL(%rip), %r12
	movq	%r12, %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	$0, -136(%rbp)
	testq	%r13, %r13
	je	.L102
	movq	-176(%rbp), %r9
	movl	24(%rbp), %ecx
	movl	%r14d, %r8d
	movq	%r13, %rsi
	movl	40(%rbp), %edx
	leaq	-136(%rbp), %rdi
	call	PKCS12_add_key
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L189
	movl	$417, %esi
	movl	$-1, %edx
	movq	%r13, %rdi
	call	EVP_PKEY_get_attr_by_NID@PLT
	movl	%eax, %esi
	testl	%eax, %eax
	js	.L106
	movq	%r13, %rdi
	call	EVP_PKEY_get_attr@PLT
	leaq	16(%r14), %rdi
	movq	%rax, %rsi
	call	X509at_add1_attr@PLT
	testq	%rax, %rax
	je	.L189
.L106:
	movl	$856, %esi
	movl	$-1, %edx
	movq	%r13, %rdi
	call	EVP_PKEY_get_attr_by_NID@PLT
	movl	%eax, %esi
	testl	%eax, %eax
	js	.L104
	movq	%r13, %rdi
	call	EVP_PKEY_get_attr@PLT
	leaq	16(%r14), %rdi
	movq	%rax, %rsi
	call	X509at_add1_attr@PLT
	testq	%rax, %rax
	je	.L189
.L104:
	cmpq	$0, -168(%rbp)
	je	.L108
	movq	-168(%rbp), %rsi
	movl	$-1, %edx
	movq	%r14, %rdi
	call	PKCS12_add_friendlyname_utf8@PLT
	testl	%eax, %eax
	je	.L189
.L108:
	movl	-148(%rbp), %edx
	testl	%edx, %edx
	jne	.L191
.L110:
	movq	-136(%rbp), %r13
	testq	%r13, %r13
	je	.L102
	cmpq	$0, -144(%rbp)
	je	.L192
	movq	%r13, %rdi
	call	PKCS12_pack_p7data@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L117
	movq	-144(%rbp), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L117
.L119:
	movq	-136(%rbp), %r13
.L102:
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	OPENSSL_sk_pop_free@PLT
	movl	$21, %edi
	movq	$0, -136(%rbp)
	movq	-144(%rbp), %r14
	call	PKCS12_init@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L189
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	PKCS12_pack_authsafes@PLT
	testl	%eax, %eax
	je	.L193
	movq	PKCS7_free@GOTPCREL(%rip), %r14
	movq	-144(%rbp), %rdi
	movq	%r14, %rsi
	call	OPENSSL_sk_pop_free@PLT
	cmpl	$-1, 32(%rbp)
	movq	$0, -144(%rbp)
	je	.L81
	subq	$8, %rsp
	movl	32(%rbp), %r9d
	xorl	%ecx, %ecx
	movl	$-1, %edx
	pushq	$0
	movq	-176(%rbp), %rsi
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	call	PKCS12_set_mac@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	jne	.L81
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L86:
	movq	%rcx, %r12
	testq	%rdx, %rdx
	je	.L90
	testq	%rcx, %rcx
	je	.L94
	movq	%rdx, %rsi
	movq	%rcx, %rdi
	call	X509_check_private_key@PLT
	testl	%eax, %eax
	je	.L190
	call	EVP_sha1@PLT
	leaq	-148(%rbp), %rcx
	leaq	-128(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	X509_digest@PLT
	testl	%eax, %eax
	je	.L190
.L93:
	movq	%r12, %rsi
	leaq	-136(%rbp), %rdi
	call	PKCS12_add_cert
	cmpq	$0, -168(%rbp)
	movq	%rax, %r12
	je	.L97
	movq	-168(%rbp), %rsi
	movl	$-1, %edx
	movq	%rax, %rdi
	call	PKCS12_add_friendlyname_utf8@PLT
	testl	%eax, %eax
	je	.L188
.L97:
	movl	-148(%rbp), %edx
	testl	%edx, %edx
	je	.L94
	leaq	-128(%rbp), %rsi
	movq	%r12, %rdi
	call	PKCS12_add_localkeyid@PLT
	testl	%eax, %eax
	jne	.L94
	.p2align 4,,10
	.p2align 3
.L188:
	movq	PKCS12_SAFEBAG_free@GOTPCREL(%rip), %r12
.L189:
	movq	PKCS7_free@GOTPCREL(%rip), %r14
	xorl	%r13d, %r13d
.L98:
	movq	%r13, %rdi
	call	PKCS12_free@PLT
	movq	-144(%rbp), %rdi
	movq	%r14, %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	-136(%rbp), %rdi
	movq	%r12, %rsi
	call	OPENSSL_sk_pop_free@PLT
.L190:
	xorl	%r13d, %r13d
.L81:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L194
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	testq	%rcx, %rcx
	jne	.L93
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L192:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L189
	movq	%r13, %rdi
	call	PKCS12_pack_p7data@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L115
	movq	-144(%rbp), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L119
.L115:
	movq	-144(%rbp), %rdi
	call	OPENSSL_sk_free@PLT
	movq	$0, -144(%rbp)
.L117:
	movq	%r13, %rdi
	call	PKCS7_free@PLT
	jmp	.L189
.L87:
	movl	$58, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$104, %edx
	xorl	%r13d, %r13d
	movl	$105, %esi
	movl	$35, %edi
	call	ERR_put_error@PLT
	jmp	.L81
.L191:
	leaq	-128(%rbp), %rsi
	movq	%r14, %rdi
	call	PKCS12_add_localkeyid@PLT
	testl	%eax, %eax
	jne	.L110
	jmp	.L189
.L193:
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	PKCS12_free@PLT
	movq	PKCS7_free@GOTPCREL(%rip), %r14
	jmp	.L98
.L194:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE804:
	.size	PKCS12_create, .-PKCS12_create
	.p2align 4
	.globl	PKCS12_add_safes
	.type	PKCS12_add_safes, @function
PKCS12_add_safes:
.LFB809:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	testl	%esi, %esi
	movl	$21, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	%esi, %edi
	pushq	%r12
	.cfi_offset 12, -32
	cmovle	%eax, %edi
	call	PKCS12_init@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L195
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	PKCS12_pack_authsafes@PLT
	testl	%eax, %eax
	je	.L202
.L195:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L202:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	PKCS12_free@PLT
	jmp	.L195
	.cfi_endproc
.LFE809:
	.size	PKCS12_add_safes, .-PKCS12_add_safes
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
