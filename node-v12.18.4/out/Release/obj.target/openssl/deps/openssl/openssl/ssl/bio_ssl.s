	.file	"bio_ssl.c"
	.text
	.p2align 4
	.type	ssl_callback_ctrl, @function
ssl_callback_ctrl:
.LFB970:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	call	BIO_get_data@PLT
	cmpl	$14, %ebx
	je	.L5
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	movq	(%rax), %rax
	popq	%rbx
	movq	%r12, %rdx
	movl	$14, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	16(%rax), %rdi
	jmp	BIO_callback_ctrl@PLT
	.cfi_endproc
.LFE970:
	.size	ssl_callback_ctrl, .-ssl_callback_ctrl
	.p2align 4
	.type	ssl_puts, @function
ssl_puts:
.LFB971:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	call	strlen@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	BIO_write@PLT
	.cfi_endproc
.LFE971:
	.size	ssl_puts, .-ssl_puts
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/ssl/bio_ssl.c"
	.text
	.p2align 4
	.type	ssl_new, @function
ssl_new:
.LFB965:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$58, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	$48, %edi
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L12
	movq	%rax, %r13
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	BIO_set_init@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	BIO_set_data@PLT
	movq	%r12, %rdi
	movl	$-1, %esi
	call	BIO_clear_flags@PLT
	popq	%r12
	movl	$1, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	movl	$61, %r8d
	movl	$65, %edx
	movl	$118, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE965:
	.size	ssl_new, .-ssl_new
	.p2align 4
	.type	ssl_read, @function
ssl_read:
.LFB967:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -56(%rbp)
	testq	%rsi, %rsi
	je	.L13
	movq	%rsi, %r14
	movq	%rcx, %rbx
	movq	%rdi, %r12
	call	BIO_get_data@PLT
	movl	$15, %esi
	movq	%r12, %rdi
	movq	(%rax), %r15
	movq	%rax, -64(%rbp)
	call	BIO_clear_flags@PLT
	movq	-56(%rbp), %rdx
	movq	%rbx, %rcx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	ssl_read_internal@PLT
	movq	%r15, %rdi
	movl	%eax, %esi
	movl	%eax, %r13d
	call	SSL_get_error@PLT
	movl	%eax, %esi
	cmpl	$8, %eax
	ja	.L29
	leaq	.L17(%rip), %rcx
	movl	%eax, %edx
	movq	-64(%rbp), %r8
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L17:
	.long	.L22-.L17
	.long	.L29-.L17
	.long	.L21-.L17
	.long	.L20-.L17
	.long	.L19-.L17
	.long	.L29-.L17
	.long	.L29-.L17
	.long	.L18-.L17
	.long	.L16-.L17
	.text
	.p2align 4,,10
	.p2align 3
.L29:
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L15:
	movq	%r12, %rdi
	call	BIO_set_retry_reason@PLT
.L13:
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	movq	16(%r8), %rdx
	testq	%rdx, %rdx
	je	.L33
	movq	(%rbx), %rax
	addq	24(%r8), %rax
	movq	%rax, 24(%r8)
	cmpq	%rax, %rdx
	jnb	.L33
	addl	$1, 8(%r8)
	movq	%r15, %rdi
	movq	$0, 24(%r8)
	movl	%esi, -56(%rbp)
	call	SSL_renegotiate@PLT
	movl	-56(%rbp), %esi
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L21:
	movl	$9, %esi
	movq	%r12, %rdi
	call	BIO_set_flags@PLT
	xorl	%esi, %esi
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$10, %esi
	movq	%r12, %rdi
	call	BIO_set_flags@PLT
	xorl	%esi, %esi
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L19:
	movl	$12, %esi
	movq	%r12, %rdi
	call	BIO_set_flags@PLT
	movl	$1, %esi
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$12, %esi
	movq	%r12, %rdi
	call	BIO_set_flags@PLT
	movl	$2, %esi
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L16:
	movl	$12, %esi
	movq	%r12, %rdi
	call	BIO_set_flags@PLT
	movl	$3, %esi
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L33:
	cmpq	$0, 32(%r8)
	je	.L15
	xorl	%edi, %edi
	movl	%esi, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	time@PLT
	movq	-56(%rbp), %r8
	movl	-64(%rbp), %esi
	movq	32(%r8), %rdx
	addq	40(%r8), %rdx
	cmpq	%rdx, %rax
	jbe	.L15
	addl	$1, 8(%r8)
	movq	%r15, %rdi
	movq	%rax, 40(%r8)
	movl	%esi, -56(%rbp)
	call	SSL_renegotiate@PLT
	movl	-56(%rbp), %esi
	jmp	.L15
	.cfi_endproc
.LFE967:
	.size	ssl_read, .-ssl_read
	.p2align 4
	.type	ssl_write, @function
ssl_write:
.LFB968:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -56(%rbp)
	testq	%rsi, %rsi
	je	.L34
	movq	%rsi, %r14
	movq	%rcx, %rbx
	movq	%rdi, %r12
	call	BIO_get_data@PLT
	movl	$15, %esi
	movq	%r12, %rdi
	movq	(%rax), %r15
	movq	%rax, -64(%rbp)
	call	BIO_clear_flags@PLT
	movq	-56(%rbp), %rdx
	movq	%rbx, %rcx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	ssl_write_internal@PLT
	movq	%r15, %rdi
	movl	%eax, %esi
	movl	%eax, %r13d
	call	SSL_get_error@PLT
	movl	%eax, %esi
	cmpl	$7, %eax
	ja	.L49
	leaq	.L38(%rip), %rcx
	movl	%eax, %edx
	movq	-64(%rbp), %r8
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L38:
	.long	.L42-.L38
	.long	.L49-.L38
	.long	.L41-.L38
	.long	.L40-.L38
	.long	.L39-.L38
	.long	.L49-.L38
	.long	.L49-.L38
	.long	.L37-.L38
	.text
	.p2align 4,,10
	.p2align 3
.L49:
	xorl	%esi, %esi
.L36:
	movq	%r12, %rdi
	call	BIO_set_retry_reason@PLT
.L34:
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	movq	16(%r8), %rax
	testq	%rax, %rax
	je	.L53
	movq	(%rbx), %rdx
	addq	24(%r8), %rdx
	movq	%rdx, 24(%r8)
	cmpq	%rdx, %rax
	jb	.L54
.L53:
	cmpq	$0, 32(%r8)
	je	.L36
	xorl	%edi, %edi
	movl	%esi, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	time@PLT
	movq	-56(%rbp), %r8
	movl	-64(%rbp), %esi
	movq	32(%r8), %rdx
	addq	40(%r8), %rdx
	cmpq	%rdx, %rax
	jbe	.L36
	addl	$1, 8(%r8)
	movq	%r15, %rdi
	movq	%rax, 40(%r8)
	movl	%esi, -56(%rbp)
	call	SSL_renegotiate@PLT
	movl	-56(%rbp), %esi
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L41:
	movl	$9, %esi
	movq	%r12, %rdi
	call	BIO_set_flags@PLT
	xorl	%esi, %esi
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L39:
	movl	$12, %esi
	movq	%r12, %rdi
	call	BIO_set_flags@PLT
	movl	$1, %esi
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L37:
	movl	$12, %esi
	movq	%r12, %rdi
	call	BIO_set_flags@PLT
	movl	$2, %esi
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L40:
	movl	$10, %esi
	movq	%r12, %rdi
	call	BIO_set_flags@PLT
	xorl	%esi, %esi
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L54:
	addl	$1, 8(%r8)
	movq	%r15, %rdi
	movq	$0, 24(%r8)
	movl	%esi, -56(%rbp)
	call	SSL_renegotiate@PLT
	movl	-56(%rbp), %esi
	jmp	.L36
	.cfi_endproc
.LFE968:
	.size	ssl_write, .-ssl_write
	.p2align 4
	.type	ssl_free, @function
ssl_free:
.LFB966:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L71
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	BIO_get_data@PLT
	movq	(%rax), %rdi
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L57
	call	SSL_shutdown@PLT
.L57:
	movq	%r12, %rdi
	call	BIO_get_shutdown@PLT
	testl	%eax, %eax
	jne	.L76
.L58:
	movq	%r13, %rdi
	movl	$88, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	popq	%r12
	movl	$1, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	movq	%r12, %rdi
	call	BIO_get_init@PLT
	testl	%eax, %eax
	jne	.L77
.L59:
	movq	%r12, %rdi
	movl	$-1, %esi
	call	BIO_clear_flags@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	BIO_set_init@PLT
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	0(%r13), %rdi
	call	SSL_free@PLT
	jmp	.L59
	.cfi_endproc
.LFE966:
	.size	ssl_free, .-ssl_free
	.p2align 4
	.type	ssl_ctrl, @function
ssl_ctrl:
.LFB969:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	call	BIO_get_data@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	BIO_next@PLT
	movq	(%rbx), %r9
	movq	%rax, %r8
	testq	%r9, %r9
	jne	.L79
	cmpl	$109, %r12d
	je	.L79
.L121:
	xorl	%r12d, %r12d
.L78:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	cmpl	$127, %r12d
	ja	.L81
	leaq	.L83(%rip), %rcx
	movl	%r12d, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L83:
	.long	.L81-.L83
	.long	.L99-.L83
	.long	.L81-.L83
	.long	.L121-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L98-.L83
	.long	.L97-.L83
	.long	.L96-.L83
	.long	.L95-.L83
	.long	.L94-.L83
	.long	.L93-.L83
	.long	.L92-.L83
	.long	.L91-.L83
	.long	.L121-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L90-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L89-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L88-.L83
	.long	.L87-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L86-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L85-.L83
	.long	.L84-.L83
	.long	.L82-.L83
	.text
.L82:
	cmpq	$59, %r14
	movl	$5, %eax
	movq	32(%rbx), %r12
	cmovle	%rax, %r14
	xorl	%edi, %edi
	movq	%r14, 32(%rbx)
	call	time@PLT
	movq	%rax, 40(%rbx)
	jmp	.L78
.L84:
	movslq	8(%rbx), %r12
	jmp	.L78
.L85:
	movq	16(%rbx), %r12
	cmpq	$511, %r14
	jle	.L78
	movq	%r14, 16(%rbx)
	jmp	.L78
.L86:
	movq	%r9, %rdi
	testq	%r14, %r14
	je	.L105
	call	SSL_set_connect_state@PLT
.L159:
	movl	$1, %r12d
	jmp	.L78
.L87:
	testq	%r15, %r15
	je	.L121
	movq	%r9, (%r15)
	movl	$1, %r12d
	jmp	.L78
.L88:
	testq	%r9, %r9
	je	.L107
	testq	%r13, %r13
	je	.L108
	movq	%r13, %rdi
	movq	%r8, -56(%rbp)
	call	BIO_get_data@PLT
	movq	-56(%rbp), %r8
	movq	(%rax), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L109
	call	SSL_shutdown@PLT
	movq	-56(%rbp), %r8
.L109:
	movq	%r13, %rdi
	movq	%r8, -56(%rbp)
	call	BIO_get_shutdown@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	jne	.L161
.L110:
	movl	$88, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	CRYPTO_free@PLT
	movq	-56(%rbp), %r8
.L108:
	movl	$58, %edx
	leaq	.LC0(%rip), %rsi
	movl	$48, %edi
	movq	%r8, -56(%rbp)
	call	CRYPTO_zalloc@PLT
	movq	-56(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L162
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%r8, -56(%rbp)
	call	BIO_set_init@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	BIO_set_data@PLT
	movl	$-1, %esi
	movq	%r13, %rdi
	call	BIO_clear_flags@PLT
	movq	-56(%rbp), %r8
.L107:
	movl	%r14d, %esi
	movq	%r13, %rdi
	movq	%r8, -56(%rbp)
	call	BIO_set_shutdown@PLT
	movq	%r15, (%rbx)
	movq	%r15, %rdi
	call	SSL_get_rbio@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L113
	movq	-56(%rbp), %r8
	testq	%r8, %r8
	je	.L114
	movq	%r8, %rsi
	movq	%rax, %rdi
	call	BIO_push@PLT
.L114:
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	BIO_set_next@PLT
	movq	%r12, %rdi
	call	BIO_up_ref@PLT
.L113:
	movl	$1, %esi
	movq	%r13, %rdi
	movl	$1, %r12d
	call	BIO_set_init@PLT
	jmp	.L78
.L89:
	movq	16(%r9), %rdi
	movq	%r15, %rcx
	movq	%r14, %rdx
	movl	$105, %esi
.L160:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_ctrl@PLT
.L90:
	.cfi_restore_state
	movl	$15, %esi
	movq	%r13, %rdi
	movq	%r9, -56(%rbp)
	movq	%r8, -64(%rbp)
	call	BIO_clear_flags@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	BIO_set_retry_reason@PLT
	movq	-56(%rbp), %r9
	movq	%r9, %rdi
	call	SSL_do_handshake@PLT
	movq	-56(%rbp), %r9
	movslq	%eax, %r12
	movq	%r12, %rsi
	movq	%r9, %rdi
	call	SSL_get_error@PLT
	cmpl	$4, %eax
	je	.L116
	movq	-64(%rbp), %r8
	jg	.L117
	cmpl	$2, %eax
	je	.L118
	cmpl	$3, %eax
	jne	.L78
	movl	$10, %esi
	movq	%r13, %rdi
	call	BIO_set_flags@PLT
	jmp	.L78
.L91:
	movq	24(%r9), %rdi
	movq	%r15, %rcx
	movq	%r14, %rdx
	movl	$13, %esi
	jmp	.L160
.L92:
	movq	%r15, %rdi
	movq	%r9, -56(%rbp)
	call	BIO_get_data@PLT
	movq	(%rax), %rdi
	movq	%rax, %r12
	call	SSL_free@PLT
	movq	-56(%rbp), %r9
	movq	%r9, %rdi
	call	SSL_dup@PLT
	movl	8(%rbx), %edx
	movdqu	16(%rbx), %xmm0
	movdqu	32(%rbx), %xmm1
	movq	%rax, (%r12)
	movl	%edx, 8(%r12)
	movups	%xmm1, 32(%r12)
	movups	%xmm0, 16(%r12)
	xorl	%r12d, %r12d
	testq	%rax, %rax
	setne	%r12b
	jmp	.L78
.L93:
	movq	%r13, %rdi
	movl	$15, %esi
	movq	%r9, -56(%rbp)
	call	BIO_clear_flags@PLT
	movq	-56(%rbp), %r9
	movq	%r15, %rcx
	movq	%r14, %rdx
	movl	$11, %esi
	movq	24(%r9), %rdi
	call	BIO_ctrl@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	BIO_copy_next_retry@PLT
	jmp	.L78
.L94:
	movq	%r9, %rdi
	movq	%r9, -56(%rbp)
	call	SSL_pending@PLT
	movq	-56(%rbp), %r9
	movslq	%eax, %r12
	testq	%r12, %r12
	jne	.L78
	movq	16(%r9), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$10, %esi
	call	BIO_ctrl@PLT
	movslq	%eax, %r12
	jmp	.L78
.L95:
	movl	%r14d, %esi
	movq	%r13, %rdi
	movl	$1, %r12d
	call	BIO_set_shutdown@PLT
	jmp	.L78
.L96:
	movq	%r13, %rdi
	call	BIO_get_shutdown@PLT
	movslq	%eax, %r12
	jmp	.L78
.L97:
	cmpq	%r15, %r13
	jne	.L159
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r9, %rdi
	movl	$1, %r12d
	call	SSL_set_bio@PLT
	jmp	.L78
.L98:
	testq	%r8, %r8
	je	.L159
	cmpq	%r8, 16(%r9)
	movq	%r9, -56(%rbp)
	je	.L159
	movq	%r8, %rdi
	movq	%r8, -64(%rbp)
	movl	$1, %r12d
	call	BIO_up_ref@PLT
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %r9
	movq	%r8, %rdx
	movq	%r8, %rsi
	movq	%r9, %rdi
	call	SSL_set_bio@PLT
	jmp	.L78
.L99:
	movq	%r9, %rdi
	movq	%r8, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	SSL_shutdown@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %r8
	movq	8(%r9), %rax
	movq	48(%r9), %rdx
	cmpq	48(%rax), %rdx
	je	.L163
	cmpq	40(%rax), %rdx
	je	.L164
.L101:
	movq	%r9, %rdi
	movq	%r8, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	SSL_clear@PLT
	testl	%eax, %eax
	je	.L121
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %r9
	testq	%r8, %r8
	je	.L103
	movq	%r15, %rcx
	movq	%r14, %rdx
	movl	$1, %esi
	movq	%r8, %rdi
	jmp	.L160
.L81:
	movq	16(%r9), %rdi
	movq	%r15, %rcx
	movq	%r14, %rdx
	movl	%r12d, %esi
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L117:
	cmpl	$7, %eax
	jne	.L78
	movl	$12, %esi
	movq	%r13, %rdi
	movq	%r8, -56(%rbp)
	call	BIO_set_flags@PLT
	movq	-56(%rbp), %r8
	movq	%r8, %rdi
	call	BIO_get_retry_reason@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	BIO_set_retry_reason@PLT
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L105:
	call	SSL_set_accept_state@PLT
	movl	$1, %r12d
	jmp	.L78
.L103:
	movq	16(%r9), %rdi
	testq	%rdi, %rdi
	je	.L159
	movq	%r15, %rcx
	movq	%r14, %rdx
	movl	$1, %esi
	jmp	.L160
.L116:
	movq	%r13, %rdi
	movl	$12, %esi
	call	BIO_set_flags@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	BIO_set_retry_reason@PLT
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L118:
	movl	$9, %esi
	movq	%r13, %rdi
	call	BIO_set_flags@PLT
	jmp	.L78
.L161:
	movq	%r13, %rdi
	call	BIO_get_init@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	jne	.L165
.L111:
	movl	$-1, %esi
	movq	%r13, %rdi
	movq	%r8, -56(%rbp)
	call	BIO_clear_flags@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	BIO_set_init@PLT
	movq	-56(%rbp), %r8
	jmp	.L110
.L163:
	movq	%r9, %rdi
	call	SSL_set_connect_state@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %r8
	jmp	.L101
.L162:
	movl	$61, %r8d
	movl	$65, %edx
	movl	$118, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L78
.L164:
	movq	%r9, %rdi
	movq	%r8, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	SSL_set_accept_state@PLT
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %r9
	jmp	.L101
.L165:
	movq	(%r12), %rdi
	call	SSL_free@PLT
	movq	-56(%rbp), %r8
	jmp	.L111
	.cfi_endproc
.LFE969:
	.size	ssl_ctrl, .-ssl_ctrl
	.p2align 4
	.globl	BIO_f_ssl
	.type	BIO_f_ssl, @function
BIO_f_ssl:
.LFB964:
	.cfi_startproc
	endbr64
	leaq	methods_sslp(%rip), %rax
	ret
	.cfi_endproc
.LFE964:
	.size	BIO_f_ssl, .-BIO_f_ssl
	.p2align 4
	.globl	BIO_new_ssl_connect
	.type	BIO_new_ssl_connect, @function
BIO_new_ssl_connect:
.LFB973:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	call	BIO_s_connect@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	testq	%rax, %rax
	je	.L173
	leaq	methods_sslp(%rip), %rdi
	movq	%rax, %r12
	call	BIO_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L170
	movq	%r14, %rdi
	call	SSL_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L178
	movq	%rax, %rdi
	call	SSL_set_connect_state@PLT
	movl	$109, %esi
	movq	%r13, %rdi
	movq	%r14, %rcx
	movl	$1, %edx
	call	BIO_ctrl@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	BIO_push@PLT
	testq	%rax, %rax
	je	.L170
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	movq	%r13, %rdi
	call	BIO_free@PLT
.L170:
	movq	%r12, %rdi
	call	BIO_free@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L173:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE973:
	.size	BIO_new_ssl_connect, .-BIO_new_ssl_connect
	.p2align 4
	.globl	BIO_new_buffer_ssl_connect
	.type	BIO_new_buffer_ssl_connect, @function
BIO_new_buffer_ssl_connect:
.LFB972:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	call	BIO_f_buffer@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	testq	%rax, %rax
	je	.L183
	movq	%r13, %rdi
	movq	%rax, %r12
	call	BIO_new_ssl_connect
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L182
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	BIO_push@PLT
	testq	%rax, %rax
	je	.L182
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L182:
	.cfi_restore_state
	movq	%r12, %rdi
	call	BIO_free@PLT
	movq	%r13, %rdi
	call	BIO_free@PLT
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L183:
	.cfi_restore_state
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE972:
	.size	BIO_new_buffer_ssl_connect, .-BIO_new_buffer_ssl_connect
	.p2align 4
	.globl	BIO_new_ssl
	.type	BIO_new_ssl, @function
BIO_new_ssl:
.LFB974:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	leaq	methods_sslp(%rip), %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$8, %rsp
	call	BIO_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L188
	movq	%r13, %rdi
	call	SSL_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L197
	movq	%rax, %rdi
	testl	%ebx, %ebx
	jne	.L198
	call	SSL_set_accept_state@PLT
.L192:
	movq	%r13, %rcx
	movl	$1, %edx
	movl	$109, %esi
	movq	%r12, %rdi
	call	BIO_ctrl@PLT
.L188:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L198:
	.cfi_restore_state
	call	SSL_set_connect_state@PLT
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L197:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	BIO_free@PLT
	jmp	.L188
	.cfi_endproc
.LFE974:
	.size	BIO_new_ssl, .-BIO_new_ssl
	.p2align 4
	.globl	BIO_ssl_copy_session_id
	.type	BIO_ssl_copy_session_id, @function
BIO_ssl_copy_session_id:
.LFB975:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movl	$519, %esi
	pushq	%r12
	.cfi_offset 12, -32
	call	BIO_find_type@PLT
	movl	$519, %esi
	movq	%r13, %rdi
	movq	%rax, %r12
	call	BIO_find_type@PLT
	testq	%r12, %r12
	je	.L203
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L203
	movq	%r12, %rdi
	call	BIO_get_data@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	BIO_get_data@PLT
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L203
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L203
	call	SSL_copy_session_id@PLT
	popq	%r12
	popq	%r13
	testl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	setne	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L203:
	.cfi_restore_state
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE975:
	.size	BIO_ssl_copy_session_id, .-BIO_ssl_copy_session_id
	.p2align 4
	.globl	BIO_ssl_shutdown
	.type	BIO_ssl_shutdown, @function
BIO_ssl_shutdown:
.LFB976:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L228
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L214:
	movq	%r12, %rdi
	call	BIO_next@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L231
.L216:
	movq	%r12, %rdi
	call	BIO_method_type@PLT
	cmpl	$519, %eax
	jne	.L214
	movq	%r12, %rdi
	call	BIO_get_data@PLT
	testq	%rax, %rax
	je	.L214
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L214
	call	SSL_shutdown@PLT
	movq	%r12, %rdi
	call	BIO_next@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L216
.L231:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L228:
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE976:
	.size	BIO_ssl_shutdown, .-BIO_ssl_shutdown
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"ssl"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	methods_sslp, @object
	.size	methods_sslp, 96
methods_sslp:
	.long	519
	.zero	4
	.quad	.LC1
	.quad	ssl_write
	.quad	0
	.quad	ssl_read
	.quad	0
	.quad	ssl_puts
	.quad	0
	.quad	ssl_ctrl
	.quad	ssl_new
	.quad	ssl_free
	.quad	ssl_callback_ctrl
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
