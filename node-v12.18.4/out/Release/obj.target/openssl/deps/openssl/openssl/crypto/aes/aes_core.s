	.file	"aes_core.c"
	.text
	.p2align 4
	.globl	AES_set_encrypt_key
	.type	AES_set_encrypt_key, @function
AES_set_encrypt_key:
.LFB151:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L12
	testq	%rdx, %rdx
	je	.L12
	movl	%esi, %eax
	movq	%rdi, %r8
	movl	%esi, %r9d
	andl	$-65, %eax
	cmpl	$128, %eax
	jne	.L19
	cmpl	$128, %esi
	je	.L20
	cmpl	$192, %esi
	jne	.L7
	movl	$12, 240(%rdx)
.L6:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movzbl	(%r8), %esi
	movzbl	1(%r8), %eax
	sall	$24, %esi
	sall	$16, %eax
	xorl	%eax, %esi
	movzbl	3(%r8), %eax
	xorl	%eax, %esi
	movzbl	2(%r8), %eax
	sall	$8, %eax
	xorl	%eax, %esi
	movl	%esi, (%rdx)
	movzbl	4(%r8), %ecx
	movzbl	5(%r8), %eax
	sall	$24, %ecx
	sall	$16, %eax
	xorl	%eax, %ecx
	movzbl	7(%r8), %eax
	xorl	%eax, %ecx
	movzbl	6(%r8), %eax
	sall	$8, %eax
	xorl	%eax, %ecx
	movl	%ecx, 4(%rdx)
	movzbl	8(%r8), %edi
	movzbl	9(%r8), %eax
	sall	$24, %edi
	sall	$16, %eax
	xorl	%eax, %edi
	movzbl	11(%r8), %eax
	xorl	%eax, %edi
	movzbl	10(%r8), %eax
	sall	$8, %eax
	xorl	%eax, %edi
	movl	%edi, 8(%rdx)
	movzbl	12(%r8), %ebx
	movzbl	13(%r8), %eax
	sall	$24, %ebx
	sall	$16, %eax
	xorl	%eax, %ebx
	movzbl	15(%r8), %eax
	xorl	%eax, %ebx
	movzbl	14(%r8), %eax
	sall	$8, %eax
	xorl	%eax, %ebx
	movl	%ebx, 12(%rdx)
	cmpl	$128, %r9d
	je	.L21
	movzbl	16(%r8), %eax
	movzbl	17(%r8), %r10d
	sall	$24, %eax
	sall	$16, %r10d
	xorl	%r10d, %eax
	movzbl	19(%r8), %r10d
	xorl	%r10d, %eax
	movzbl	18(%r8), %r10d
	sall	$8, %r10d
	xorl	%r10d, %eax
	movl	%eax, 16(%rdx)
	movzbl	20(%r8), %eax
	movzbl	21(%r8), %r10d
	sall	$24, %eax
	sall	$16, %r10d
	xorl	%r10d, %eax
	movzbl	23(%r8), %r10d
	xorl	%r10d, %eax
	movzbl	22(%r8), %r10d
	sall	$8, %r10d
	xorl	%r10d, %eax
	movl	%eax, 20(%rdx)
	cmpl	$192, %r9d
	je	.L22
	movzbl	24(%r8), %eax
	movzbl	25(%r8), %r10d
	sall	$24, %eax
	sall	$16, %r10d
	xorl	%r10d, %eax
	movzbl	27(%r8), %r10d
	xorl	%r10d, %eax
	movzbl	26(%r8), %r10d
	sall	$8, %r10d
	xorl	%r10d, %eax
	movl	%eax, 24(%rdx)
	movzbl	28(%r8), %eax
	movzbl	29(%r8), %r10d
	sall	$24, %eax
	sall	$16, %r10d
	xorl	%r10d, %eax
	movzbl	31(%r8), %r10d
	movzbl	30(%r8), %r8d
	xorl	%r10d, %eax
	sall	$8, %r8d
	xorl	%r8d, %eax
	movl	%eax, 28(%rdx)
	cmpl	$256, %r9d
	je	.L23
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	cmpl	$256, %esi
	jne	.L24
.L7:
	movl	$14, 240(%rdx)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$10, 240(%rdx)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	Te0(%rip), %r10
	movzbl	%bl, %eax
	leaq	Te1(%rip), %r11
	xorl	$16777216, %esi
	movl	(%r10,%rax,4), %r12d
	movl	%ebx, %eax
	leaq	Te2(%rip), %r9
	leaq	Te3(%rip), %r8
	shrl	$24, %eax
	andl	$65280, %r12d
	xorl	%esi, %r12d
	movzbl	(%r11,%rax,4), %esi
	movl	%ebx, %eax
	shrl	$16, %eax
	movzbl	%al, %eax
	xorl	%esi, %r12d
	movl	(%r9,%rax,4), %esi
	movzbl	%bh, %eax
	movl	(%r8,%rax,4), %eax
	andl	$-16777216, %esi
	xorl	%esi, %r12d
	andl	$16711680, %eax
	xorl	%eax, %r12d
	movl	%ecx, %eax
	xorl	%r12d, %eax
	movl	%r12d, 16(%rdx)
	xorl	$33554432, %r12d
	movl	%eax, %ecx
	movl	%eax, 20(%rdx)
	xorl	%edi, %ecx
	xorl	%ecx, %ebx
	movl	%ecx, 24(%rdx)
	movzbl	%bl, %ecx
	movl	%ebx, 28(%rdx)
	movl	(%r10,%rcx,4), %esi
	movl	%ebx, %ecx
	shrl	$24, %ecx
	andl	$65280, %esi
	xorl	%r12d, %esi
	movzbl	(%r11,%rcx,4), %r12d
	movl	%ebx, %ecx
	shrl	$16, %ecx
	movzbl	%cl, %ecx
	xorl	%r12d, %esi
	movl	(%r9,%rcx,4), %r12d
	movzbl	%bh, %ecx
	movl	(%r8,%rcx,4), %ecx
	andl	$-16777216, %r12d
	xorl	%r12d, %esi
	andl	$16711680, %ecx
	xorl	%ecx, %esi
	xorl	%esi, %edi
	xorl	%esi, %eax
	movl	%esi, 32(%rdx)
	xorl	$67108864, %esi
	movl	%edi, %ecx
	movl	%eax, 36(%rdx)
	xorl	%ebx, %ecx
	movl	%edi, 40(%rdx)
	movzbl	%cl, %r12d
	movl	%ecx, 44(%rdx)
	movl	(%r10,%r12,4), %r12d
	andl	$65280, %r12d
	xorl	%r12d, %esi
	movl	%ecx, %r12d
	shrl	$24, %r12d
	movzbl	(%r11,%r12,4), %r12d
	xorl	%r12d, %esi
	movl	%ecx, %r12d
	movzbl	%ch, %ecx
	shrl	$16, %r12d
	movl	(%r8,%rcx,4), %ecx
	movzbl	%r12b, %r12d
	movl	(%r9,%r12,4), %r12d
	andl	$-16777216, %r12d
	andl	$16711680, %ecx
	xorl	%r12d, %esi
	xorl	%esi, %ecx
	xorl	%ecx, %eax
	movl	%ecx, 48(%rdx)
	xorl	$134217728, %ecx
	movl	%eax, %esi
	xorl	%eax, %ebx
	movl	%eax, 52(%rdx)
	xorl	%edi, %esi
	movl	%ebx, 60(%rdx)
	movl	%esi, 56(%rdx)
	movzbl	%bl, %esi
	movl	(%r10,%rsi,4), %esi
	andl	$65280, %esi
	xorl	%ecx, %esi
	movl	%ebx, %ecx
	shrl	$24, %ecx
	movzbl	(%r11,%rcx,4), %ecx
	xorl	%ecx, %esi
	movl	%ebx, %ecx
	shrl	$16, %ecx
	movzbl	%cl, %ecx
	movl	(%r9,%rcx,4), %ecx
	andl	$-16777216, %ecx
	xorl	%ecx, %esi
	movzbl	%bh, %ecx
	movl	(%r8,%rcx,4), %ecx
	andl	$16711680, %ecx
	xorl	%ecx, %esi
	xorl	%esi, %edi
	xorl	%esi, %eax
	movl	%esi, 64(%rdx)
	xorl	$268435456, %esi
	movl	%edi, %r12d
	movl	%eax, 68(%rdx)
	xorl	%ebx, %r12d
	movl	%edi, 72(%rdx)
	movzbl	%r12b, %ecx
	movl	%r12d, 76(%rdx)
	movl	(%r10,%rcx,4), %ecx
	andl	$65280, %ecx
	xorl	%esi, %ecx
	movl	%r12d, %esi
	shrl	$24, %esi
	movzbl	(%r11,%rsi,4), %esi
	xorl	%esi, %ecx
	movl	%r12d, %esi
	shrl	$16, %esi
	movzbl	%sil, %esi
	movl	(%r9,%rsi,4), %esi
	andl	$-16777216, %esi
	xorl	%esi, %ecx
	movl	%ecx, %r15d
	movl	%r12d, %ecx
	movzbl	%ch, %esi
	movl	(%r8,%rsi,4), %esi
	andl	$16711680, %esi
	xorl	%r15d, %esi
	xorl	%esi, %eax
	movl	%esi, 80(%rdx)
	movl	%eax, %ecx
	movl	%eax, 84(%rdx)
	xorl	%edi, %ecx
	xorl	%eax, %ebx
	xorl	$536870912, %esi
	movl	%ecx, 88(%rdx)
	movzbl	%bl, %ecx
	movl	%ebx, %r12d
	movl	(%r10,%rcx,4), %ecx
	movl	%ebx, 92(%rdx)
	andl	$65280, %ecx
	xorl	%esi, %ecx
	movl	%ebx, %esi
	shrl	$24, %esi
	movzbl	(%r11,%rsi,4), %esi
	xorl	%esi, %ecx
	movl	%ebx, %esi
	shrl	$16, %esi
	movzbl	%sil, %esi
	movl	(%r9,%rsi,4), %esi
	andl	$-16777216, %esi
	xorl	%esi, %ecx
	movzbl	%bh, %esi
	movl	(%r8,%rsi,4), %esi
	andl	$16711680, %esi
	xorl	%esi, %ecx
	xorl	%ecx, %edi
	xorl	%ecx, %eax
	movl	%ecx, 96(%rdx)
	xorl	$1073741824, %ecx
	xorl	%edi, %r12d
	movl	%eax, 100(%rdx)
	movzbl	%r12b, %esi
	movl	%edi, 104(%rdx)
	movl	(%r10,%rsi,4), %esi
	movl	%r12d, 108(%rdx)
	andl	$65280, %esi
	xorl	%esi, %ecx
	movl	%r12d, %esi
	shrl	$24, %esi
	movzbl	(%r11,%rsi,4), %esi
	xorl	%esi, %ecx
	movl	%r12d, %esi
	shrl	$16, %esi
	movzbl	%sil, %esi
	movl	(%r9,%rsi,4), %esi
	andl	$-16777216, %esi
	xorl	%esi, %ecx
	movl	%ecx, %r14d
	movl	%r12d, %ecx
	movzbl	%ch, %esi
	movl	%edi, %ecx
	movl	(%r8,%rsi,4), %esi
	andl	$16711680, %esi
	xorl	%r14d, %esi
	xorl	%esi, %eax
	movl	%esi, 112(%rdx)
	addl	$-2147483648, %esi
	xorl	%eax, %ebx
	xorl	%eax, %ecx
	movl	%eax, 116(%rdx)
	movl	%ecx, 120(%rdx)
	movzbl	%bl, %ecx
	movl	%ebx, %r12d
	movl	(%r10,%rcx,4), %ecx
	movl	%ebx, 124(%rdx)
	andl	$65280, %ecx
	xorl	%esi, %ecx
	movl	%ebx, %esi
	shrl	$24, %esi
	movzbl	(%r11,%rsi,4), %esi
	xorl	%esi, %ecx
	movl	%ebx, %esi
	shrl	$16, %esi
	movzbl	%sil, %esi
	movl	(%r9,%rsi,4), %esi
	andl	$-16777216, %esi
	xorl	%esi, %ecx
	movzbl	%bh, %esi
	movl	(%r8,%rsi,4), %esi
	andl	$16711680, %esi
	xorl	%esi, %ecx
	xorl	%ecx, %edi
	xorl	%ecx, %eax
	movl	%ecx, 128(%rdx)
	xorl	$452984832, %ecx
	xorl	%edi, %r12d
	movl	%eax, 132(%rdx)
	movzbl	%r12b, %esi
	movl	%edi, 136(%rdx)
	movl	%r12d, 140(%rdx)
	movl	(%r10,%rsi,4), %esi
	andl	$65280, %esi
	xorl	%esi, %ecx
	movl	%r12d, %esi
	shrl	$24, %esi
	movzbl	(%r11,%rsi,4), %esi
	xorl	%esi, %ecx
	movl	%r12d, %esi
	shrl	$16, %esi
	movzbl	%sil, %esi
	movl	(%r9,%rsi,4), %esi
	andl	$-16777216, %esi
	xorl	%esi, %ecx
	movl	%ecx, %r15d
	movl	%r12d, %ecx
	movzbl	%ch, %esi
	movl	%edi, %ecx
	movl	(%r8,%rsi,4), %esi
	andl	$16711680, %esi
	xorl	%r15d, %esi
	xorl	%esi, %eax
	movl	%esi, 144(%rdx)
	xorl	$905969664, %esi
	xorl	%eax, %ebx
	xorl	%eax, %ecx
	movl	%eax, 148(%rdx)
	movl	%ecx, 152(%rdx)
	movzbl	%bl, %ecx
	movl	(%r10,%rcx,4), %ecx
	movl	%ebx, 156(%rdx)
	andl	$65280, %ecx
	xorl	%esi, %ecx
	movl	%ebx, %esi
	shrl	$24, %esi
	movzbl	(%r11,%rsi,4), %esi
	xorl	%esi, %ecx
	movl	%ebx, %esi
	shrl	$16, %esi
	movzbl	%sil, %esi
	movl	(%r9,%rsi,4), %esi
	andl	$-16777216, %esi
	xorl	%esi, %ecx
	movzbl	%bh, %esi
	movl	(%r8,%rsi,4), %esi
	andl	$16711680, %esi
	xorl	%esi, %ecx
	xorl	%ecx, %eax
	xorl	%ecx, %edi
	movl	%ecx, 160(%rdx)
	xorl	%edi, %ebx
	movl	%eax, 164(%rdx)
	xorl	%eax, %eax
	movl	%edi, 168(%rdx)
	movl	%ebx, 172(%rdx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	movzbl	%al, %r9d
	xorl	$16777216, %esi
	movl	%ecx, %r12d
	movl	%ebx, %r13d
	leaq	Te0(%rip), %r8
	leaq	Te1(%rip), %r11
	movl	(%r8,%r9,4), %r9d
	leaq	Te2(%rip), %r10
	andl	$65280, %r9d
	xorl	%r9d, %esi
	movl	%eax, %r9d
	shrl	$24, %r9d
	movzbl	(%r11,%r9,4), %r9d
	xorl	%r9d, %esi
	movl	%eax, %r9d
	shrl	$16, %r9d
	movzbl	%r9b, %r9d
	movl	(%r10,%r9,4), %r9d
	andl	$-16777216, %r9d
	xorl	%r9d, %esi
	leaq	Te3(%rip), %r9
	movl	%esi, %r15d
	movzbl	%ah, %esi
	movl	(%r9,%rsi,4), %r14d
	movl	%r15d, %esi
	andl	$16711680, %r14d
	xorl	%r14d, %esi
	xorl	%esi, %r12d
	movl	%esi, 24(%rdx)
	xorl	%r12d, %edi
	movl	%r12d, 28(%rdx)
	xorl	%edi, %r13d
	movl	%edi, %r15d
	movl	%edi, 32(%rdx)
	movl	%r13d, 36(%rdx)
	xorl	16(%rdx), %r13d
	xorl	%r13d, %eax
	movl	%r13d, 40(%rdx)
	movzbl	%al, %r12d
	movl	%eax, %r14d
	movzbl	%ah, %edi
	movl	%eax, 44(%rdx)
	movl	(%r8,%r12,4), %r12d
	shrl	$24, %r14d
	movl	%r15d, -44(%rbp)
	movzbl	(%r11,%r14,4), %r14d
	andl	$65280, %r12d
	orl	%r14d, %r12d
	movl	%r12d, %r14d
	movl	%eax, %r12d
	shrl	$16, %r12d
	xorl	$33554432, %r14d
	movzbl	%r12b, %r12d
	movl	(%r10,%r12,4), %r12d
	andl	$-16777216, %r12d
	xorl	%r14d, %r12d
	movl	(%r9,%rdi,4), %r14d
	movl	%r15d, %edi
	andl	$16711680, %r14d
	xorl	%r14d, %r12d
	xorl	%r12d, %ecx
	xorl	%r12d, %esi
	movl	%ebx, %r12d
	xorl	%ecx, %r12d
	movl	%esi, %r14d
	movl	%esi, 48(%rdx)
	xorl	%ecx, %edi
	xorl	%r12d, %r13d
	movl	%edi, 56(%rdx)
	xorl	%r13d, %eax
	movl	%r12d, 60(%rdx)
	movzbl	%al, %esi
	movl	%eax, %r15d
	movzbl	%ah, %edi
	movl	%eax, 68(%rdx)
	movl	(%r8,%rsi,4), %esi
	shrl	$24, %r15d
	movl	%r13d, 64(%rdx)
	movzbl	(%r11,%r15,4), %r15d
	movl	%ecx, 52(%rdx)
	andl	$65280, %esi
	orl	%r15d, %esi
	movl	%eax, %r15d
	shrl	$16, %r15d
	xorl	$67108864, %esi
	movzbl	%r15b, %r15d
	movl	(%r10,%r15,4), %r15d
	andl	$-16777216, %r15d
	xorl	%r15d, %esi
	movl	(%r9,%rdi,4), %r15d
	movl	-44(%rbp), %edi
	andl	$16711680, %r15d
	xorl	%r15d, %esi
	xorl	%r14d, %esi
	movl	%ecx, %r14d
	xorl	%esi, %edi
	xorl	%esi, %r14d
	movl	%esi, 72(%rdx)
	xorl	%edi, %r12d
	movl	%r14d, 76(%rdx)
	movl	%r12d, 84(%rdx)
	xorl	%r13d, %r12d
	xorl	%r12d, %eax
	movl	%edi, 80(%rdx)
	movl	%eax, %r13d
	movl	%r12d, 88(%rdx)
	movl	%r13d, %r15d
	movl	%eax, 92(%rdx)
	movzbl	%al, %eax
	movl	(%r8,%rax,4), %r14d
	shrl	$24, %r15d
	movzbl	(%r11,%r15,4), %eax
	andl	$65280, %r14d
	orl	%eax, %r14d
	movl	%r13d, %eax
	shrl	$16, %eax
	xorl	$134217728, %r14d
	movzbl	%al, %eax
	movl	(%r10,%rax,4), %eax
	andl	$-16777216, %eax
	xorl	%r14d, %eax
	movl	%eax, %r15d
	movl	%r13d, %eax
	movzbl	%ah, %eax
	movl	(%r9,%rax,4), %r14d
	movl	%r15d, %eax
	andl	$16711680, %r14d
	xorl	%r14d, %eax
	xorl	%eax, %ebx
	xorl	%eax, %esi
	xorl	%eax, %ecx
	movl	%r13d, %eax
	xorl	%ebx, %r12d
	movl	%esi, %r14d
	movl	%esi, 96(%rdx)
	movl	%edi, %esi
	xorl	%r12d, %eax
	xorl	%ecx, %esi
	movl	%r12d, 112(%rdx)
	movl	%esi, 104(%rdx)
	movl	%eax, %r15d
	movzbl	%al, %esi
	movl	(%r8,%rsi,4), %r13d
	shrl	$24, %r15d
	movl	%eax, 116(%rdx)
	movzbl	(%r11,%r15,4), %esi
	movl	%ecx, 100(%rdx)
	andl	$65280, %r13d
	movl	%ebx, 108(%rdx)
	orl	%esi, %r13d
	movl	%eax, %esi
	xorl	$268435456, %r13d
	shrl	$16, %esi
	movzbl	%sil, %esi
	movl	(%r10,%rsi,4), %esi
	andl	$-16777216, %esi
	xorl	%r13d, %esi
	movl	%esi, %r15d
	movzbl	%ah, %esi
	movl	(%r9,%rsi,4), %r13d
	movl	%r15d, %esi
	andl	$16711680, %r13d
	xorl	%r13d, %esi
	movl	%ecx, %r13d
	xorl	%r14d, %esi
	xorl	%esi, %r13d
	xorl	%esi, %edi
	movl	%esi, 120(%rdx)
	movl	%r13d, 124(%rdx)
	movl	%ebx, %r13d
	xorl	%edi, %r13d
	movl	%edi, -44(%rbp)
	movl	%r13d, 132(%rdx)
	xorl	%r12d, %r13d
	xorl	%r13d, %eax
	movl	%edi, 128(%rdx)
	movzbl	%al, %r12d
	movl	%eax, %r15d
	movzbl	%ah, %edi
	movl	%eax, 140(%rdx)
	movl	(%r8,%r12,4), %r14d
	shrl	$24, %r15d
	movl	%r13d, 136(%rdx)
	movzbl	(%r11,%r15,4), %r12d
	andl	$65280, %r14d
	orl	%r12d, %r14d
	movl	%eax, %r12d
	shrl	$16, %r12d
	xorl	$536870912, %r14d
	movzbl	%r12b, %r12d
	movl	(%r10,%r12,4), %r12d
	andl	$-16777216, %r12d
	xorl	%r14d, %r12d
	movl	(%r9,%rdi,4), %r14d
	movl	-44(%rbp), %edi
	andl	$16711680, %r14d
	xorl	%r14d, %r12d
	xorl	%r12d, %ecx
	xorl	%r12d, %esi
	movl	%ebx, %r12d
	xorl	%ecx, %r12d
	xorl	%ecx, %edi
	movl	%esi, 144(%rdx)
	xorl	%r12d, %r13d
	movl	%ecx, 148(%rdx)
	xorl	%r13d, %eax
	movl	%edi, 152(%rdx)
	movzbl	%al, %r14d
	movl	%eax, %r15d
	movl	%eax, 164(%rdx)
	movzbl	%ah, %edi
	movl	%r12d, 156(%rdx)
	shrl	$24, %r15d
	movl	%r13d, 160(%rdx)
	movl	(%r8,%r14,4), %r14d
	movzbl	(%r11,%r15,4), %r15d
	andl	$65280, %r14d
	orl	%r15d, %r14d
	movl	%eax, %r15d
	shrl	$16, %r15d
	xorl	$1073741824, %r14d
	movzbl	%r15b, %r15d
	movl	(%r10,%r15,4), %r15d
	andl	$-16777216, %r15d
	xorl	%r15d, %r14d
	movl	(%r9,%rdi,4), %r15d
	movl	-44(%rbp), %edi
	andl	$16711680, %r15d
	xorl	%r15d, %r14d
	xorl	%r14d, %esi
	movl	%ecx, %r14d
	xorl	%esi, %edi
	movl	%esi, 168(%rdx)
	xorl	%esi, %r14d
	xorl	%edi, %r12d
	movl	%r14d, 172(%rdx)
	movl	%r12d, 180(%rdx)
	xorl	%r13d, %r12d
	xorl	%r12d, %eax
	movl	%r12d, 184(%rdx)
	movzbl	%al, %r12d
	movl	%eax, 188(%rdx)
	movl	(%r8,%r12,4), %r8d
	movl	%eax, %r12d
	movl	%edi, 176(%rdx)
	shrl	$24, %r12d
	movzbl	(%r11,%r12,4), %r11d
	andl	$65280, %r8d
	orl	%r11d, %r8d
	movl	%eax, %r11d
	movzbl	%ah, %eax
	shrl	$16, %r11d
	movl	(%r9,%rax,4), %eax
	addl	$-2147483648, %r8d
	movzbl	%r11b, %r11d
	movl	(%r10,%r11,4), %r10d
	andl	$16711680, %eax
	andl	$-16777216, %r10d
	xorl	%r10d, %r8d
	xorl	%r8d, %eax
	xorl	%eax, %ecx
	xorl	%eax, %esi
	xorl	%ebx, %eax
	movl	%ecx, 196(%rdx)
	xorl	%edi, %ecx
	movl	%eax, 204(%rdx)
	xorl	%eax, %eax
	movl	%esi, 192(%rdx)
	movl	%ecx, 200(%rdx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	movl	%ecx, -44(%rbp)
	leaq	32(%rdx), %rax
	leaq	rcon(%rip), %r10
	leaq	Te0(%rip), %r14
	leaq	Te1(%rip), %r13
	leaq	Te2(%rip), %r12
	leaq	Te3(%rip), %r11
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L25:
	movl	%ebx, %edx
	movzbl	%bh, %ecx
	movzbl	%bl, %r15d
	shrl	$16, %edx
	movl	(%r14,%rcx,4), %r9d
	movzbl	0(%r13,%r15,4), %r15d
	movzbl	%dl, %edx
	movl	(%r11,%rdx,4), %edx
	andl	$65280, %r9d
	andl	$16711680, %edx
	orl	%r9d, %edx
	movl	%ebx, %r9d
	xorl	-48(%rax), %edx
	shrl	$24, %r9d
	movl	(%r12,%r9,4), %r9d
	andl	$-16777216, %r9d
	orl	%r15d, %r9d
	xorl	%r9d, %edx
	movl	%edx, -16(%rax)
	xorl	-44(%rax), %edx
	movl	%edx, -12(%rax)
	xorl	-40(%rax), %edx
	movl	%edx, -8(%rax)
	xorl	%r8d, %edx
	movl	%edx, -4(%rax)
.L11:
	movl	-4(%rax), %r8d
	xorl	(%r10), %esi
	addq	$4, %r10
	addq	$32, %rax
	movzbl	%r8b, %edx
	movl	%r8d, %ecx
	movl	(%r14,%rdx,4), %edx
	andl	$65280, %edx
	xorl	%edx, %esi
	movl	%r8d, %edx
	shrl	$24, %edx
	movzbl	0(%r13,%rdx,4), %edx
	xorl	%edx, %esi
	movl	%r8d, %edx
	shrl	$16, %edx
	movzbl	%dl, %edx
	movl	(%r12,%rdx,4), %edx
	andl	$-16777216, %edx
	xorl	%esi, %edx
	movzbl	%ch, %esi
	movl	(%r11,%rsi,4), %esi
	andl	$16711680, %esi
	xorl	%edx, %esi
	xorl	%esi, -44(%rbp)
	movl	-44(%rbp), %ecx
	movl	%esi, -32(%rax)
	xorl	%ecx, %edi
	movl	%ecx, -28(%rax)
	leaq	28+rcon(%rip), %rcx
	xorl	%edi, %ebx
	movl	%edi, -24(%rax)
	movl	%ebx, -20(%rax)
	cmpq	%rcx, %r10
	jne	.L25
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
.L12:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	$-1, %eax
	ret
.L24:
	movl	$-2, %eax
	ret
	.cfi_endproc
.LFE151:
	.size	AES_set_encrypt_key, .-AES_set_encrypt_key
	.p2align 4
	.globl	AES_set_decrypt_key
	.type	AES_set_decrypt_key, @function
AES_set_decrypt_key:
.LFB152:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	AES_set_encrypt_key
	testl	%eax, %eax
	js	.L26
	movl	240(%r12), %edx
	movq	%r12, %rsi
	leal	0(,%rdx,4), %r9d
	testl	%r9d, %r9d
	jle	.L28
	movslq	%r9d, %rax
	movq	%r12, %rcx
	xorl	%edx, %edx
	leaq	(%r12,%rax,4), %rax
	.p2align 4,,10
	.p2align 3
.L29:
	movl	(%rcx), %edi
	movl	(%rax), %r8d
	addl	$4, %edx
	addq	$16, %rcx
	subq	$16, %rax
	movl	%r8d, -16(%rcx)
	movl	20(%rax), %r8d
	movl	%edi, 16(%rax)
	movl	-12(%rcx), %edi
	movl	%r8d, -12(%rcx)
	movl	24(%rax), %r8d
	movl	%edi, 20(%rax)
	movl	-8(%rcx), %edi
	movl	%r8d, -8(%rcx)
	movl	28(%rax), %r8d
	movl	%edi, 24(%rax)
	movl	-4(%rcx), %edi
	movl	%r8d, -4(%rcx)
	movl	%edi, 28(%rax)
	movl	%r9d, %edi
	subl	%edx, %edi
	cmpl	%edi, %edx
	jl	.L29
	movl	240(%r12), %edx
.L28:
	movl	$1, %r11d
	leaq	Td0(%rip), %r10
	leaq	Te1(%rip), %rax
	leaq	Td3(%rip), %r9
	leaq	Td1(%rip), %r8
	leaq	Td2(%rip), %rdi
	cmpl	$1, %edx
	jle	.L31
	.p2align 4,,10
	.p2align 3
.L30:
	movl	16(%rsi), %r14d
	movl	20(%rsi), %r13d
	addq	$16, %rsi
	addl	$1, %r11d
	movl	8(%rsi), %edx
	movl	12(%rsi), %ebx
	movl	%r14d, %ecx
	movzbl	%r14b, %r15d
	shrl	$24, %ecx
	movzbl	(%rax,%r15,4), %r15d
	movzbl	(%rax,%rcx,4), %ecx
	movl	(%r10,%rcx,4), %ecx
	xorl	(%r9,%r15,4), %ecx
	movl	%r14d, %r15d
	shrl	$16, %r15d
	movzbl	%r15b, %r15d
	movzbl	(%rax,%r15,4), %r15d
	xorl	(%r8,%r15,4), %ecx
	movl	%ecx, %r15d
	movl	%r14d, %ecx
	movzbl	%ch, %ecx
	movzbl	(%rax,%rcx,4), %r14d
	movl	(%rdi,%r14,4), %ecx
	xorl	%r15d, %ecx
	movd	%ecx, %xmm0
	movl	%r13d, %ecx
	shrl	$24, %ecx
	movzbl	(%rax,%rcx,4), %r14d
	movzbl	%r13b, %ecx
	movzbl	(%rax,%rcx,4), %ecx
	movl	(%r10,%r14,4), %r14d
	xorl	(%r9,%rcx,4), %r14d
	movl	%r13d, %ecx
	shrl	$16, %ecx
	movzbl	%cl, %ecx
	movzbl	(%rax,%rcx,4), %ecx
	xorl	(%r8,%rcx,4), %r14d
	movl	%r13d, %ecx
	movzbl	%dl, %r13d
	movzbl	%ch, %ecx
	movzbl	(%rax,%r13,4), %r13d
	movzbl	(%rax,%rcx,4), %ecx
	xorl	(%rdi,%rcx,4), %r14d
	movl	%edx, %ecx
	shrl	$24, %ecx
	movd	%r14d, %xmm3
	movzbl	(%rax,%rcx,4), %ecx
	punpckldq	%xmm3, %xmm0
	movl	(%r10,%rcx,4), %ecx
	xorl	(%r9,%r13,4), %ecx
	movl	%edx, %r13d
	movzbl	%dh, %edx
	shrl	$16, %r13d
	movzbl	(%rax,%rdx,4), %edx
	movzbl	%r13b, %r13d
	movzbl	(%rax,%r13,4), %r13d
	xorl	(%r8,%r13,4), %ecx
	xorl	(%rdi,%rdx,4), %ecx
	movl	%ebx, %edx
	movzbl	%bl, %r13d
	shrl	$24, %edx
	movzbl	(%rax,%r13,4), %r13d
	movd	%ecx, %xmm1
	movzbl	(%rax,%rdx,4), %edx
	movl	(%r10,%rdx,4), %edx
	xorl	(%r9,%r13,4), %edx
	movl	%ebx, %r13d
	movzbl	%bh, %ebx
	shrl	$16, %r13d
	movzbl	%r13b, %r13d
	movzbl	(%rax,%r13,4), %r13d
	movzbl	(%rax,%rbx,4), %ebx
	xorl	(%r8,%r13,4), %edx
	xorl	(%rdi,%rbx,4), %edx
	movd	%edx, %xmm2
	punpckldq	%xmm2, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rsi)
	cmpl	%r11d, 240(%r12)
	jg	.L30
.L31:
	xorl	%eax, %eax
.L26:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE152:
	.size	AES_set_decrypt_key, .-AES_set_decrypt_key
	.p2align 4
	.globl	AES_encrypt
	.type	AES_encrypt, @function
AES_encrypt:
.LFB153:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rcx
	movq	%rdx, %rdi
	leaq	Te0(%rip), %r11
	leaq	Te3(%rip), %r8
	leaq	Te1(%rip), %r10
	leaq	Te2(%rip), %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -96(%rbp)
	movq	%rsi, -104(%rbp)
	movzbl	(%rcx), %eax
	movzbl	1(%rcx), %edx
	movzbl	4(%rcx), %r12d
	movzbl	8(%rcx), %r13d
	sall	$16, %edx
	sall	$24, %eax
	movzbl	12(%rcx), %esi
	sall	$24, %r12d
	xorl	%edx, %eax
	movzbl	3(%rcx), %edx
	sall	$24, %r13d
	sall	$24, %esi
	xorl	%edx, %eax
	movzbl	2(%rcx), %edx
	sall	$8, %edx
	xorl	%edx, %eax
	movzbl	5(%rcx), %edx
	xorl	(%rdi), %eax
	sall	$16, %edx
	xorl	%edx, %r12d
	movzbl	7(%rcx), %edx
	xorl	%edx, %r12d
	movzbl	6(%rcx), %edx
	sall	$8, %edx
	xorl	%edx, %r12d
	movzbl	9(%rcx), %edx
	xorl	4(%rdi), %r12d
	sall	$16, %edx
	xorl	%edx, %r13d
	movzbl	11(%rcx), %edx
	xorl	%edx, %r13d
	movzbl	10(%rcx), %edx
	sall	$8, %edx
	xorl	%edx, %r13d
	movzbl	13(%rcx), %edx
	xorl	8(%rdi), %r13d
	sall	$16, %edx
	xorl	%edx, %esi
	movzbl	15(%rcx), %edx
	xorl	%edx, %esi
	movzbl	14(%rcx), %edx
	movl	240(%rdi), %ecx
	sall	$8, %edx
	movl	%ecx, -44(%rbp)
	xorl	%edx, %esi
	movl	%ecx, %edx
	xorl	12(%rdi), %esi
	sarl	%edx
	subl	$1, %edx
	movl	%edx, -108(%rbp)
	addq	$1, %rdx
	salq	$5, %rdx
	addq	%rdi, %rdx
	movq	%rdx, -88(%rbp)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L41:
	movl	-48(%rbp), %ecx
	movl	(%r11,%r12,4), %eax
	movl	%edx, %edx
	xorl	(%r8,%r13,4), %eax
	xorl	%esi, %eax
	xorl	(%r10,%r14,4), %eax
	xorl	(%r9,%rcx,4), %eax
	movl	-52(%rbp), %ecx
	movl	-64(%rbp), %esi
	movl	(%r11,%rcx,4), %r12d
	movl	-56(%rbp), %ecx
	xorl	(%r8,%rsi,4), %r12d
	xorl	4(%rdi), %r12d
	xorl	(%r10,%rcx,4), %r12d
	movl	-60(%rbp), %ecx
	xorl	(%r9,%rcx,4), %r12d
	movl	-68(%rbp), %ecx
	movl	(%r11,%rcx,4), %r13d
	movl	-76(%rbp), %ecx
	xorl	(%r8,%r15,4), %r13d
	movl	-72(%rbp), %r15d
	xorl	8(%rdi), %r13d
	xorl	(%r10,%r15,4), %r13d
	xorl	(%r9,%rcx,4), %r13d
	movl	-44(%rbp), %ecx
	movl	(%r11,%rcx,4), %esi
	movl	-80(%rbp), %ecx
	xorl	(%r8,%rbx,4), %esi
	xorl	12(%rdi), %esi
	xorl	(%r10,%rcx,4), %esi
	xorl	(%r9,%rdx,4), %esi
.L39:
	movl	%eax, %edx
	movzbl	%sil, %ebx
	movzbl	%r12b, %r14d
	addq	$32, %rdi
	shrl	$24, %edx
	movl	(%r11,%rdx,4), %ecx
	movl	%r12d, %edx
	xorl	(%r8,%rbx,4), %ecx
	movl	%r13d, %ebx
	shrl	$16, %edx
	xorl	-16(%rdi), %ecx
	movzbl	%dl, %edx
	xorl	(%r10,%rdx,4), %ecx
	movzbl	%bh, %edx
	movzbl	%al, %ebx
	xorl	(%r9,%rdx,4), %ecx
	movl	%r12d, %edx
	shrl	$24, %edx
	movl	(%r11,%rdx,4), %edx
	xorl	(%r8,%rbx,4), %edx
	movl	%r13d, %ebx
	shrl	$16, %ebx
	xorl	-12(%rdi), %edx
	movzbl	%bl, %ebx
	xorl	(%r10,%rbx,4), %edx
	movl	%esi, %ebx
	movzbl	%bh, %ebx
	xorl	(%r9,%rbx,4), %edx
	movl	%r13d, %ebx
	shrl	$24, %ebx
	movl	%edx, %r15d
	movzbl	%ah, %edx
	shrl	$16, %eax
	movl	(%r11,%rbx,4), %ebx
	xorl	(%r8,%r14,4), %ebx
	movl	%esi, %r14d
	shrl	$24, %esi
	shrl	$16, %r14d
	xorl	-8(%rdi), %ebx
	movl	%r15d, -44(%rbp)
	movzbl	%al, %eax
	movzbl	%r14b, %r14d
	xorl	(%r10,%r14,4), %ebx
	xorl	(%r9,%rdx,4), %ebx
	movl	%r12d, %edx
	movzbl	%r13b, %r14d
	movl	(%r11,%rsi,4), %r13d
	movzbl	%dh, %esi
	movl	%r15d, %edx
	xorl	(%r8,%r14,4), %r13d
	shrl	$16, %edx
	xorl	-4(%rdi), %r13d
	xorl	(%r10,%rax,4), %r13d
	movl	%ecx, %r12d
	movzbl	%dl, %r14d
	movl	%r15d, %edx
	movl	%ebx, %r15d
	movl	%r13d, %eax
	shrl	$16, %r15d
	xorl	(%r9,%rsi,4), %eax
	shrl	$24, %edx
	movzbl	%bh, %esi
	movzbl	%r15b, %r15d
	movl	%edx, -52(%rbp)
	movzbl	%ah, %edx
	shrl	$24, %r12d
	movl	%r15d, -56(%rbp)
	movzbl	%cl, %r15d
	movzbl	%al, %r13d
	movl	%r15d, -64(%rbp)
	movl	%ebx, %r15d
	movzbl	%bl, %ebx
	shrl	$24, %r15d
	movl	%edx, -60(%rbp)
	movl	%r15d, -68(%rbp)
	movl	%eax, %r15d
	shrl	$24, %eax
	shrl	$16, %r15d
	movl	%esi, -48(%rbp)
	movl	(%rdi), %esi
	movzbl	%r15b, %edx
	movl	%edx, -72(%rbp)
	movzbl	%ch, %edx
	shrl	$16, %ecx
	movl	%edx, -76(%rbp)
	movl	-44(%rbp), %edx
	movl	%eax, -44(%rbp)
	movzbl	%cl, %eax
	movzbl	%dl, %r15d
	movl	%eax, -80(%rbp)
	movzbl	%dh, %edx
	cmpq	-88(%rbp), %rdi
	jne	.L41
	movl	%esi, %eax
	movl	%edx, %esi
	movl	-108(%rbp), %edx
	movq	-96(%rbp), %rdi
	movzbl	(%r10,%r13,4), %ecx
	addq	$1, %rdx
	salq	$5, %rdx
	addq	%rdx, %rdi
	movl	-48(%rbp), %edx
	movl	(%r11,%rdx,4), %edx
	andl	$65280, %edx
	orl	%ecx, %edx
	movl	%edx, %ecx
	movl	%r14d, %edx
	movq	-104(%rbp), %r14
	xorl	%eax, %ecx
	movl	(%r8,%rdx,4), %edx
	movl	(%r9,%r12,4), %eax
	andl	$16711680, %edx
	andl	$-16777216, %eax
	orl	%edx, %eax
	movl	-64(%rbp), %edx
	xorl	%ecx, %eax
	movl	-56(%rbp), %ecx
	bswap	%eax
	movl	%eax, (%r14)
	movl	-60(%rbp), %eax
	movzbl	(%r10,%rdx,4), %edx
	movl	(%r8,%rcx,4), %ecx
	movl	(%r11,%rax,4), %eax
	andl	$16711680, %ecx
	andl	$65280, %eax
	orl	%edx, %eax
	movl	-52(%rbp), %edx
	xorl	4(%rdi), %eax
	movl	(%r9,%rdx,4), %edx
	andl	$-16777216, %edx
	orl	%ecx, %edx
	xorl	%eax, %edx
	movl	-76(%rbp), %eax
	bswap	%edx
	movl	%edx, 4(%r14)
	movzbl	(%r10,%r15,4), %edx
	movl	(%r11,%rax,4), %eax
	movl	-72(%rbp), %r15d
	andl	$65280, %eax
	movl	(%r8,%r15,4), %ecx
	orl	%edx, %eax
	movl	-68(%rbp), %edx
	xorl	8(%rdi), %eax
	andl	$16711680, %ecx
	movl	(%r9,%rdx,4), %edx
	andl	$-16777216, %edx
	orl	%ecx, %edx
	movl	%ebx, %ecx
	xorl	%edx, %eax
	movzbl	(%r10,%rcx,4), %edx
	bswap	%eax
	movl	%eax, 8(%r14)
	movl	(%r11,%rsi,4), %eax
	movl	-44(%rbp), %esi
	andl	$65280, %eax
	orl	%edx, %eax
	xorl	12(%rdi), %eax
	movl	(%r9,%rsi,4), %edx
	movl	%edx, %ecx
	movl	-80(%rbp), %edx
	andl	$-16777216, %ecx
	movl	(%r8,%rdx,4), %edx
	andl	$16711680, %edx
	orl	%ecx, %edx
	xorl	%edx, %eax
	bswap	%eax
	movl	%eax, 12(%r14)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE153:
	.size	AES_encrypt, .-AES_encrypt
	.p2align 4
	.globl	AES_decrypt
	.type	AES_decrypt, @function
AES_decrypt:
.LFB154:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	Td0(%rip), %r11
	leaq	Td3(%rip), %r10
	leaq	Td1(%rip), %r9
	leaq	Td2(%rip), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	movq	%rdx, -104(%rbp)
	movq	%rsi, -112(%rbp)
	movzbl	(%rdi), %eax
	movzbl	1(%rdi), %edx
	movzbl	4(%rdi), %r12d
	movzbl	8(%rdi), %r13d
	sall	$16, %edx
	sall	$24, %eax
	movzbl	12(%rdi), %esi
	sall	$24, %r12d
	xorl	%edx, %eax
	movzbl	3(%rdi), %edx
	sall	$24, %r13d
	sall	$24, %esi
	xorl	%edx, %eax
	movzbl	2(%rdi), %edx
	sall	$8, %edx
	xorl	%edx, %eax
	movzbl	5(%rdi), %edx
	xorl	(%rbx), %eax
	sall	$16, %edx
	xorl	%edx, %r12d
	movzbl	7(%rdi), %edx
	xorl	%edx, %r12d
	movzbl	6(%rdi), %edx
	sall	$8, %edx
	xorl	%edx, %r12d
	movzbl	9(%rdi), %edx
	xorl	4(%rbx), %r12d
	sall	$16, %edx
	xorl	%edx, %r13d
	movzbl	11(%rdi), %edx
	xorl	%edx, %r13d
	movzbl	10(%rdi), %edx
	sall	$8, %edx
	xorl	%edx, %r13d
	movzbl	13(%rdi), %edx
	xorl	8(%rbx), %r13d
	sall	$16, %edx
	xorl	%edx, %esi
	movzbl	15(%rdi), %edx
	xorl	%edx, %esi
	movzbl	14(%rdi), %edx
	movq	%rbx, %rdi
	sall	$8, %edx
	xorl	%edx, %esi
	xorl	12(%rbx), %esi
	movl	240(%rbx), %ebx
	movl	%ebx, %edx
	movl	%ebx, -44(%rbp)
	sarl	%edx
	subl	$1, %edx
	movl	%edx, -88(%rbp)
	addq	$1, %rdx
	salq	$5, %rdx
	addq	%rdi, %rdx
	movq	%rdx, -96(%rbp)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L46:
	movl	-48(%rbp), %edx
	movl	(%r11,%r14,4), %eax
	movl	%ebx, %ebx
	xorl	(%r10,%r13,4), %eax
	xorl	%esi, %eax
	xorl	(%r9,%r12,4), %eax
	xorl	(%r8,%rdx,4), %eax
	movl	-52(%rbp), %edx
	movl	-64(%rbp), %esi
	movl	(%r11,%rdx,4), %r12d
	movl	-56(%rbp), %edx
	xorl	(%r10,%rsi,4), %r12d
	xorl	4(%rdi), %r12d
	xorl	(%r9,%rdx,4), %r12d
	movl	-60(%rbp), %edx
	xorl	(%r8,%rdx,4), %r12d
	movl	-68(%rbp), %edx
	movl	(%r11,%rdx,4), %r13d
	movl	-76(%rbp), %edx
	xorl	(%r10,%r15,4), %r13d
	movl	-72(%rbp), %r15d
	xorl	8(%rdi), %r13d
	xorl	(%r9,%r15,4), %r13d
	xorl	(%r8,%rdx,4), %r13d
	movl	-80(%rbp), %edx
	movl	(%r11,%rdx,4), %esi
	movl	-84(%rbp), %edx
	xorl	(%r10,%rcx,4), %esi
	xorl	12(%rdi), %esi
	xorl	(%r9,%rdx,4), %esi
	xorl	(%r8,%rbx,4), %esi
.L44:
	movl	%eax, %edx
	movzbl	%r12b, %ebx
	movzbl	%r13b, %r14d
	addq	$32, %rdi
	shrl	$24, %edx
	movl	(%r11,%rdx,4), %ecx
	movl	%esi, %edx
	xorl	(%r10,%rbx,4), %ecx
	movl	%r13d, %ebx
	shrl	$16, %edx
	xorl	-16(%rdi), %ecx
	movzbl	%dl, %edx
	xorl	(%r9,%rdx,4), %ecx
	movzbl	%bh, %edx
	xorl	(%r8,%rdx,4), %ecx
	movl	%r12d, %edx
	shrl	$24, %edx
	movl	%ecx, %r15d
	movzbl	%ah, %ecx
	movl	(%r11,%rdx,4), %ebx
	movl	%eax, %edx
	xorl	(%r10,%r14,4), %ebx
	movzbl	%sil, %r14d
	shrl	$16, %edx
	xorl	-12(%rdi), %ebx
	movl	%r15d, -44(%rbp)
	movzbl	%dl, %edx
	xorl	(%r9,%rdx,4), %ebx
	movl	%esi, %edx
	shrl	$24, %esi
	movzbl	%dh, %edx
	xorl	(%r8,%rdx,4), %ebx
	movl	%r13d, %edx
	shrl	$16, %r13d
	shrl	$24, %edx
	movzbl	%r13b, %r13d
	movl	(%r11,%rdx,4), %edx
	xorl	(%r10,%r14,4), %edx
	movl	%r12d, %r14d
	shrl	$16, %r14d
	xorl	-8(%rdi), %edx
	movzbl	%r14b, %r14d
	xorl	(%r9,%r14,4), %edx
	xorl	(%r8,%rcx,4), %edx
	movl	%r12d, %ecx
	movzbl	%al, %r14d
	movl	(%r11,%rsi,4), %eax
	movzbl	%ch, %esi
	movl	%r15d, %ecx
	movl	%ebx, %r15d
	shrl	$24, %ecx
	xorl	(%r10,%r14,4), %eax
	shrl	$24, %r15d
	xorl	-4(%rdi), %eax
	movl	%ecx, %r14d
	movzbl	%dh, %ecx
	movl	%r15d, -52(%rbp)
	movzbl	%dl, %r15d
	movl	%ecx, -48(%rbp)
	movl	-44(%rbp), %ecx
	movl	%r15d, -64(%rbp)
	movl	%edx, %r15d
	xorl	(%r9,%r13,4), %eax
	movzbl	%bl, %r13d
	shrl	$24, %r15d
	shrl	$16, %ecx
	xorl	(%r8,%rsi,4), %eax
	movl	(%rdi), %esi
	movl	%r15d, -68(%rbp)
	movzbl	%cl, %ecx
	movl	%ebx, %r15d
	movl	%eax, %r12d
	shrl	$16, %r15d
	movl	%ecx, -56(%rbp)
	movzbl	%ah, %ecx
	shrl	$16, %r12d
	movl	%ecx, -60(%rbp)
	movzbl	%r15b, %ecx
	movzbl	%al, %r15d
	shrl	$24, %eax
	movl	%ecx, -72(%rbp)
	movl	-44(%rbp), %ecx
	shrl	$16, %edx
	movzbl	%r12b, %r12d
	movzbl	%bh, %ebx
	movzbl	%ch, %ecx
	movl	%ecx, -76(%rbp)
	movl	%eax, -80(%rbp)
	movzbl	%dl, %eax
	movzbl	-44(%rbp), %ecx
	movl	%eax, -84(%rbp)
	cmpq	-96(%rbp), %rdi
	jne	.L46
	movl	-88(%rbp), %edx
	movq	-104(%rbp), %r8
	leaq	Td4(%rip), %rdi
	movl	%esi, %eax
	movzbl	(%rdi,%r13), %esi
	movq	-112(%rbp), %r9
	movl	%ebx, %ebx
	addq	$1, %rdx
	salq	$5, %rdx
	xorl	%esi, %eax
	movl	-60(%rbp), %esi
	addq	%rdx, %r8
	movzbl	(%rdi,%r14), %edx
	sall	$24, %edx
	xorl	%edx, %eax
	movzbl	(%rdi,%r12), %edx
	sall	$16, %edx
	xorl	%edx, %eax
	movl	-48(%rbp), %edx
	movzbl	(%rdi,%rdx), %edx
	sall	$8, %edx
	xorl	%edx, %eax
	movl	-52(%rbp), %edx
	bswap	%eax
	movl	%eax, (%r9)
	movl	-64(%rbp), %eax
	movzbl	(%rdi,%rdx), %edx
	movzbl	(%rdi,%rax), %eax
	xorl	4(%r8), %eax
	sall	$24, %edx
	xorl	%edx, %eax
	movl	-56(%rbp), %edx
	movzbl	(%rdi,%rdx), %edx
	sall	$16, %edx
	xorl	%edx, %eax
	movzbl	(%rdi,%rsi), %edx
	movl	-80(%rbp), %esi
	sall	$8, %edx
	xorl	%edx, %eax
	movzbl	(%rdi,%r15), %edx
	movl	-72(%rbp), %r15d
	bswap	%eax
	movl	%eax, 4(%r9)
	movl	-68(%rbp), %eax
	xorl	8(%r8), %edx
	movzbl	(%rdi,%rax), %eax
	sall	$24, %eax
	xorl	%edx, %eax
	movzbl	(%rdi,%r15), %edx
	sall	$16, %edx
	xorl	%eax, %edx
	movl	-76(%rbp), %eax
	movzbl	(%rdi,%rax), %eax
	sall	$8, %eax
	xorl	%edx, %eax
	movzbl	(%rdi,%rsi), %edx
	bswap	%eax
	movl	%eax, 8(%r9)
	movzbl	(%rdi,%rcx), %eax
	sall	$24, %edx
	xorl	12(%r8), %eax
	xorl	%edx, %eax
	movl	-84(%rbp), %edx
	movzbl	(%rdi,%rdx), %edx
	sall	$16, %edx
	xorl	%edx, %eax
	movzbl	(%rdi,%rbx), %edx
	sall	$8, %edx
	xorl	%edx, %eax
	bswap	%eax
	movl	%eax, 12(%r9)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE154:
	.size	AES_decrypt, .-AES_decrypt
	.section	.rodata
	.align 32
	.type	rcon, @object
	.size	rcon, 40
rcon:
	.long	16777216
	.long	33554432
	.long	67108864
	.long	134217728
	.long	268435456
	.long	536870912
	.long	1073741824
	.long	-2147483648
	.long	452984832
	.long	905969664
	.align 32
	.type	Td4, @object
	.size	Td4, 256
Td4:
	.string	"R\tj\32506\2458\277@\243\236\201\363\327\373|\3439\202\233/\377\2074\216CD\304\336\351\313T{\2242\246\302#=\356L\225\013B\372\303N\b.\241f(\331$\262v[\242Im\213\321%r\370\366d\206h\230\026\324\244\\\314]e\266\222lpHP\375\355\271\332^\025FW\247\215\235\204\220\330\253"
	.ascii	"\214\274\323\n\367\344X\005\270\263E\006\320,\036\217\312?\017"
	.ascii	"\002\301\257\275\003\001\023\212k:\221\021AOg\334\352\227\362"
	.ascii	"\317\316\360\264\346s\226\254t\"\347\2555\205\342\3717\350\034"
	.ascii	"u\337nG\361\032q\035)\305\211o\267b\016\252\030\276\033\374V"
	.ascii	">K\306\322y \232\333\300\376x\315Z\364\037\335\2503\210\007\307"
	.ascii	"1\261\022\020Y'\200\354_`Q\177\251\031\265J\r-\345z\237\223\311"
	.ascii	"\234\357\240\340;M\256*\365\260\310\353\273<\203S\231a\027+\004"
	.ascii	"~\272w\326&\341i\024cU!\f}"
	.align 32
	.type	Td3, @object
	.size	Td3, 1024
Td3:
	.long	-190361519
	.long	1097159550
	.long	396673818
	.long	660510266
	.long	-1418998981
	.long	-1656360673
	.long	-94852180
	.long	-486304949
	.long	821712160
	.long	1986918061
	.long	-864644728
	.long	38544885
	.long	-438830001
	.long	718002117
	.long	893681702
	.long	1654886325
	.long	-1319482914
	.long	-1172609243
	.long	-368142267
	.long	-20913827
	.long	796197571
	.long	1290801793
	.long	1184342925
	.long	-738605461
	.long	-1889540349
	.long	-1835231979
	.long	1836772287
	.long	1381620373
	.long	-1098699308
	.long	1948373848
	.long	-529979063
	.long	-909622130
	.long	-1031181707
	.long	-1904641804
	.long	1480485785
	.long	-1183720153
	.long	-514869570
	.long	-2001922064
	.long	548169417
	.long	-835013507
	.long	-548792221
	.long	439452389
	.long	1362321559
	.long	1400849762
	.long	1685577905
	.long	1806599355
	.long	-2120213250
	.long	137073913
	.long	1214797936
	.long	1174215055
	.long	-563312748
	.long	2079897426
	.long	1943217067
	.long	1258480242
	.long	529487843
	.long	1437280870
	.long	-349698126
	.long	-1245576401
	.long	-981755258
	.long	923313619
	.long	679998000
	.long	-1079659997
	.long	57326082
	.long	377642221
	.long	-820237430
	.long	2041877159
	.long	133361907
	.long	1776460110
	.long	-621490843
	.long	96392454
	.long	878845905
	.long	-1493267772
	.long	777231668
	.long	-212492126
	.long	-1964953083
	.long	-152341084
	.long	-2081670901
	.long	1626319424
	.long	1906247262
	.long	1846563261
	.long	562755902
	.long	-586793578
	.long	1040559837
	.long	-423803315
	.long	1418573201
	.long	-1000536719
	.long	114585348
	.long	1343618912
	.long	-1728371687
	.long	-1108764714
	.long	1078185097
	.long	-643926169
	.long	-398279248
	.long	-1987344377
	.long	425408743
	.long	-923870343
	.long	2081048481
	.long	1108339068
	.long	-2078357000
	.long	0
	.long	-2138668279
	.long	736970802
	.long	292596766
	.long	1517440620
	.long	251657213
	.long	-2059905521
	.long	-1361764803
	.long	758720310
	.long	265905162
	.long	1554391400
	.long	1532285339
	.long	908999204
	.long	174567692
	.long	1474760595
	.long	-292105548
	.long	-1684955621
	.long	-1060810880
	.long	-601841055
	.long	2001430874
	.long	303699484
	.long	-1816524062
	.long	-1607801408
	.long	585122620
	.long	454499602
	.long	151849742
	.long	-1949848078
	.long	-1230456531
	.long	514443284
	.long	-249985705
	.long	1963412655
	.long	-1713521682
	.long	2137062819
	.long	19308535
	.long	1928707164
	.long	1715193156
	.long	-75615141
	.long	1126790795
	.long	600235211
	.long	-302225226
	.long	-453942344
	.long	836553431
	.long	1669664834
	.long	-1759363053
	.long	-971956092
	.long	1243905413
	.long	-1153566510
	.long	-114159186
	.long	698445255
	.long	-1641067747
	.long	-1305414692
	.long	-2041385971
	.long	-1042034569
	.long	-1290376149
	.long	1891211689
	.long	-1807156719
	.long	-379313593
	.long	-57883480
	.long	-264299872
	.long	2100090966
	.long	865136418
	.long	1229899655
	.long	953270745
	.long	-895287668
	.long	-737462632
	.long	-176042074
	.long	2061379749
	.long	-1215420710
	.long	-1379949505
	.long	983426092
	.long	2022837584
	.long	1607244650
	.long	2118541908
	.long	-1928084746
	.long	-658970480
	.long	972512814
	.long	-1011878526
	.long	1568718495
	.long	-795640727
	.long	-718427793
	.long	621982671
	.long	-1399243832
	.long	410887952
	.long	-1671205144
	.long	1002142683
	.long	645401037
	.long	1494807662
	.long	-1699282452
	.long	1335535747
	.long	-1787927066
	.long	-1671510
	.long	-1127282655
	.long	367585007
	.long	-409216582
	.long	1865862730
	.long	-1626745622
	.long	-1333995991
	.long	-1531793615
	.long	1059270954
	.long	-1517014842
	.long	-1570324427
	.long	1320957812
	.long	-2100648196
	.long	-1865371424
	.long	-1479011021
	.long	77089521
	.long	-321194175
	.long	-850391425
	.long	-1846137065
	.long	1305906550
	.long	-273658557
	.long	-1437772596
	.long	-1778065436
	.long	-776608866
	.long	1787304780
	.long	740276417
	.long	1699839814
	.long	1592394909
	.long	-1942659839
	.long	-2022411270
	.long	188821243
	.long	1729977011
	.long	-606973294
	.long	274084841
	.long	-699985043
	.long	-681472870
	.long	-1593017801
	.long	-132870567
	.long	322734571
	.long	-1457000754
	.long	1640576439
	.long	484830689
	.long	1202797690
	.long	-757114468
	.long	-227328171
	.long	349075736
	.long	-952647821
	.long	-137500077
	.long	-39167137
	.long	1030690015
	.long	1155237496
	.long	-1342996022
	.long	1757691577
	.long	607398968
	.long	-1556062270
	.long	499347990
	.long	-500888388
	.long	1011452712
	.long	227885567
	.long	-1476300487
	.long	213114376
	.long	-1260086056
	.long	1455525988
	.long	-880516741
	.long	850817237
	.long	1817998408
	.long	-1202240816
	.align 32
	.type	Td2, @object
	.size	Td2, 1024
Td2:
	.long	-1487908364
	.long	1699970625
	.long	-1530717673
	.long	1586903591
	.long	1808481195
	.long	1173430173
	.long	1487645946
	.long	59984867
	.long	-95084496
	.long	1844882806
	.long	1989249228
	.long	1277555970
	.long	-671330331
	.long	-875051734
	.long	1149249077
	.long	-1550863006
	.long	1514790577
	.long	459744698
	.long	244860394
	.long	-1058972162
	.long	1963115311
	.long	-267222708
	.long	-1750889146
	.long	-104436781
	.long	1608975247
	.long	-1667951214
	.long	2062270317
	.long	1507497298
	.long	-2094148418
	.long	567498868
	.long	1764313568
	.long	-935031095
	.long	-1989511742
	.long	2037970062
	.long	1047239000
	.long	1910319033
	.long	1337376481
	.long	-1390940024
	.long	-1402549984
	.long	984907214
	.long	1243112415
	.long	830661914
	.long	861968209
	.long	2135253587
	.long	2011214180
	.long	-1367032981
	.long	-1608712575
	.long	731183368
	.long	1750626376
	.long	-48656571
	.long	1820824798
	.long	-122203525
	.long	-752637069
	.long	48394827
	.long	-1890065633
	.long	-1423284651
	.long	671593195
	.long	-1039978571
	.long	2073724613
	.long	145085239
	.long	-2014171096
	.long	-1515052097
	.long	1790575107
	.long	-2107839210
	.long	472615631
	.long	-1265457287
	.long	-219090169
	.long	-492745111
	.long	-187865638
	.long	-1093335547
	.long	1646252340
	.long	-24460122
	.long	1402811438
	.long	1436590835
	.long	-516815478
	.long	-344611594
	.long	-331805821
	.long	-274055072
	.long	-1626972559
	.long	273792366
	.long	-1963377119
	.long	104699613
	.long	95345982
	.long	-1119466010
	.long	-1917480620
	.long	1560637892
	.long	-730921978
	.long	369057872
	.long	-81520232
	.long	-375925059
	.long	1137477952
	.long	-1636341799
	.long	1119727848
	.long	-1954019447
	.long	1530455833
	.long	-287606328
	.long	172466556
	.long	266959938
	.long	516552836
	.long	0
	.long	-2038232704
	.long	-314035669
	.long	1890328081
	.long	1917742170
	.long	-262898
	.long	945164165
	.long	-719438418
	.long	958871085
	.long	-647755249
	.long	-1507760036
	.long	1423022939
	.long	775562294
	.long	1739656202
	.long	-418409641
	.long	-1764576018
	.long	-1851909221
	.long	-984645440
	.long	547512796
	.long	1265195639
	.long	437656594
	.long	-1173691757
	.long	719700128
	.long	-532464606
	.long	387781147
	.long	218828297
	.long	-944901493
	.long	-1464259146
	.long	-1446505442
	.long	428169201
	.long	122466165
	.long	-574886247
	.long	1627235199
	.long	648017665
	.long	-172204942
	.long	1002783846
	.long	2117360635
	.long	695634755
	.long	-958608605
	.long	-60246291
	.long	-245122844
	.long	-590686415
	.long	-2062531997
	.long	574624663
	.long	287343814
	.long	612205898
	.long	1039717051
	.long	840019705
	.long	-1586641111
	.long	793451934
	.long	821288114
	.long	1391201670
	.long	-472877119
	.long	376187827
	.long	-1181111952
	.long	1224348052
	.long	1679968233
	.long	-1933268740
	.long	1058709744
	.long	752375421
	.long	-1863376333
	.long	1321699145
	.long	-775825096
	.long	-1560376118
	.long	188127444
	.long	-2117097739
	.long	-567761542
	.long	-1910056265
	.long	-1079754835
	.long	-1645990854
	.long	-1844621192
	.long	-862229921
	.long	1180849278
	.long	331544205
	.long	-1192718120
	.long	-144822727
	.long	-1342864701
	.long	-2134991011
	.long	-1820562992
	.long	766078933
	.long	313773861
	.long	-1724135252
	.long	2108100632
	.long	1668212892
	.long	-1149510853
	.long	2013908262
	.long	418672217
	.long	-1224610662
	.long	-1700232369
	.long	1852171925
	.long	-427906305
	.long	-821550660
	.long	-387518699
	.long	-1680229657
	.long	919489135
	.long	164948639
	.long	2094410160
	.long	-1297141340
	.long	590424639
	.long	-1808742747
	.long	1723872674
	.long	-1137216434
	.long	-895026046
	.long	-793714544
	.long	-669699161
	.long	-1739919100
	.long	-621329940
	.long	1343127501
	.long	-164685935
	.long	-695372211
	.long	-1337113617
	.long	1297403050
	.long	81781910
	.long	-1243373871
	.long	-2011476886
	.long	532201772
	.long	1367295589
	.long	-368796322
	.long	895287692
	.long	1953757831
	.long	1093597963
	.long	492483431
	.long	-766340389
	.long	1446242576
	.long	1192455638
	.long	1636604631
	.long	209336225
	.long	344873464
	.long	1015671571
	.long	669961897
	.long	-919226527
	.long	-437395172
	.long	-1321436601
	.long	-547775278
	.long	1933530610
	.long	-830924780
	.long	935293895
	.long	-840281097
	.long	-1436852227
	.long	1863638845
	.long	-611944380
	.long	-209597777
	.long	-1002522264
	.long	875313188
	.long	1080017571
	.long	-1015933411
	.long	621591778
	.long	1233856572
	.long	-1790836979
	.long	24197544
	.long	-1277294580
	.long	-459482956
	.long	-1047501738
	.long	-2073986101
	.long	-1234119374
	.long	1551124588
	.long	1463996600
	.align 32
	.type	Td1, @object
	.size	Td1, 1024
Td1:
	.long	1347548327
	.long	1400783205
	.long	-1021700188
	.long	-1774573730
	.long	-885281941
	.long	-249586363
	.long	-1414727080
	.long	-1823743229
	.long	1428173050
	.long	-156404115
	.long	-1853305738
	.long	636813900
	.long	-61872681
	.long	-674944309
	.long	-2144979644
	.long	-1883938141
	.long	1239331162
	.long	1730525723
	.long	-1740248562
	.long	-513933632
	.long	46346101
	.long	310463728
	.long	-1551022441
	.long	-966011911
	.long	-419197089
	.long	-1793748324
	.long	-339776134
	.long	-627748263
	.long	768917123
	.long	-749177823
	.long	692707433
	.long	1150208456
	.long	1786102409
	.long	2029293177
	.long	1805211710
	.long	-584599183
	.long	-1229004465
	.long	401639597
	.long	1724457132
	.long	-1266823622
	.long	409198410
	.long	-2098914767
	.long	1620529459
	.long	1164071807
	.long	-525245321
	.long	-2068091986
	.long	486441376
	.long	-1795618773
	.long	1483753576
	.long	428819965
	.long	-2020286868
	.long	-1219331080
	.long	598438867
	.long	-495826174
	.long	1474502543
	.long	711349675
	.long	129166120
	.long	53458370
	.long	-1702443653
	.long	-1512884472
	.long	-231724921
	.long	-1306280027
	.long	-1174273174
	.long	1559041666
	.long	730517276
	.long	-1834518092
	.long	-252508174
	.long	-1588696606
	.long	-848962828
	.long	-721025602
	.long	533804130
	.long	-1966823682
	.long	-1657524653
	.long	-1599933611
	.long	839224033
	.long	1973745387
	.long	957055980
	.long	-1438621457
	.long	106852767
	.long	1371368976
	.long	-113368694
	.long	1033297158
	.long	-1361232379
	.long	1179510461
	.long	-1248766835
	.long	91341917
	.long	1862534868
	.long	-10465259
	.long	605657339
	.long	-1747534359
	.long	-863420349
	.long	2003294622
	.long	-1112479678
	.long	-2012771957
	.long	954669403
	.long	-612775698
	.long	1201765386
	.long	-377732593
	.long	-906460130
	.long	0
	.long	-2096529274
	.long	1211247597
	.long	-1407315600
	.long	1315723890
	.long	-67301633
	.long	1443857720
	.long	507358933
	.long	657861945
	.long	1678381017
	.long	560487590
	.long	-778347692
	.long	975451694
	.long	-1324610969
	.long	261314535
	.long	-759894378
	.long	-1642357871
	.long	1333838021
	.long	-1570644960
	.long	1767536459
	.long	370938394
	.long	182621114
	.long	-440360918
	.long	1128014560
	.long	487725847
	.long	185469197
	.long	-1376613433
	.long	-1188186456
	.long	-938205527
	.long	-2057834215
	.long	1286567175
	.long	-1141990947
	.long	-39616672
	.long	-1611202266
	.long	-1134791947
	.long	-985373125
	.long	878443390
	.long	1988838185
	.long	-590666810
	.long	1756818940
	.long	1673061617
	.long	-891866660
	.long	272786309
	.long	1075025698
	.long	545572369
	.long	2105887268
	.long	-120407235
	.long	296679730
	.long	1841768865
	.long	1260232239
	.long	-203640272
	.long	-334657966
	.long	-797457949
	.long	1814803222
	.long	-1716948807
	.long	-99511224
	.long	575138148
	.long	-995558260
	.long	446754879
	.long	-665420500
	.long	-282971248
	.long	-947435186
	.long	-1042728751
	.long	-24327518
	.long	915985419
	.long	-811141759
	.long	681933534
	.long	651868046
	.long	-1539330625
	.long	-466863459
	.long	223377554
	.long	-1687527476
	.long	1649704518
	.long	-1024029421
	.long	-393160520
	.long	1580087799
	.long	-175979601
	.long	-1096852096
	.long	2087309459
	.long	-1452288723
	.long	-1278270190
	.long	1003007129
	.long	-1492117379
	.long	1860738147
	.long	2077965243
	.long	164439672
	.long	-194094824
	.long	32283319
	.long	-1467789414
	.long	1709610350
	.long	2125135846
	.long	136428751
	.long	-420538904
	.long	-642062437
	.long	-833982666
	.long	-722821367
	.long	-701910916
	.long	-1355701070
	.long	824852259
	.long	818324884
	.long	-1070226842
	.long	930369212
	.long	-1493400886
	.long	-1327460144
	.long	355706840
	.long	1257309336
	.long	-146674470
	.long	243256656
	.long	790073846
	.long	-1921626666
	.long	1296297904
	.long	1422699085
	.long	-538667516
	.long	-476130891
	.long	457992840
	.long	-1195299809
	.long	2135319889
	.long	77422314
	.long	1560382517
	.long	1945798516
	.long	788204353
	.long	1521706781
	.long	1385356242
	.long	870912086
	.long	325965383
	.long	-1936009375
	.long	2050466060
	.long	-1906706412
	.long	-1981082820
	.long	-288446169
	.long	901210569
	.long	-304014107
	.long	1014646705
	.long	1503449823
	.long	1062597235
	.long	2031621326
	.long	-1082931401
	.long	-363595827
	.long	1533017514
	.long	350174575
	.long	-2038938405
	.long	-2117423117
	.long	1052338372
	.long	741876788
	.long	1606591296
	.long	1914052035
	.long	213705253
	.long	-1960297399
	.long	1107234197
	.long	1899603969
	.long	-569897805
	.long	-1663519516
	.long	-1872472383
	.long	1635502980
	.long	1893020342
	.long	1950903388
	.long	1120974935
	.align 32
	.type	Td0, @object
	.size	Td0, 1024
Td0:
	.long	1374988112
	.long	2118214995
	.long	437757123
	.long	975658646
	.long	1001089995
	.long	530400753
	.long	-1392879445
	.long	1273168787
	.long	540080725
	.long	-1384747530
	.long	-1999866223
	.long	-184398811
	.long	1340463100
	.long	-987051049
	.long	641025152
	.long	-1251826801
	.long	-558802359
	.long	632953703
	.long	1172967064
	.long	1576976609
	.long	-1020300030
	.long	-2125664238
	.long	-1924753501
	.long	1809054150
	.long	59727847
	.long	361929877
	.long	-1083344149
	.long	-1789765158
	.long	-725712083
	.long	1484005843
	.long	1239443753
	.long	-1899378620
	.long	1975683434
	.long	-191989384
	.long	-1722270101
	.long	666464733
	.long	-1092530250
	.long	-259478249
	.long	-920605594
	.long	2110667444
	.long	1675577880
	.long	-451268222
	.long	-1756286112
	.long	1649639237
	.long	-1318815776
	.long	-1150570876
	.long	-25059300
	.long	-116905068
	.long	1883793496
	.long	-1891238631
	.long	-1797362553
	.long	1383856311
	.long	-1418472669
	.long	1917518562
	.long	-484470953
	.long	1716890410
	.long	-1293211641
	.long	800440835
	.long	-2033878118
	.long	-751368027
	.long	807962610
	.long	599762354
	.long	33778362
	.long	-317291940
	.long	-1966138325
	.long	-1485196142
	.long	-217582864
	.long	1315562145
	.long	1708848333
	.long	101039829
	.long	-785096161
	.long	-995688822
	.long	875451293
	.long	-1561111136
	.long	92987698
	.long	-1527321739
	.long	193195065
	.long	1080094634
	.long	1584504582
	.long	-1116860335
	.long	1042385657
	.long	-1763899843
	.long	-583137874
	.long	1306967366
	.long	-1856729675
	.long	1908694277
	.long	67556463
	.long	1615861247
	.long	429456164
	.long	-692196969
	.long	-1992277044
	.long	1742315127
	.long	-1326955843
	.long	126454664
	.long	-417768648
	.long	2043211483
	.long	-1585706425
	.long	2084704233
	.long	-125559095
	.long	0
	.long	159417987
	.long	841739592
	.long	504459436
	.long	1817866830
	.long	-49348613
	.long	260388950
	.long	1034867998
	.long	908933415
	.long	168810852
	.long	1750902305
	.long	-1688513327
	.long	607530554
	.long	202008497
	.long	-1822955761
	.long	-1259432238
	.long	463180190
	.long	-2134850225
	.long	1641816226
	.long	1517767529
	.long	470948374
	.long	-493635062
	.long	-1063245083
	.long	1008918595
	.long	303765277
	.long	235474187
	.long	-225720403
	.long	766945465
	.long	337553864
	.long	1475418501
	.long	-1351284916
	.long	-291906117
	.long	-1551933187
	.long	-150919521
	.long	1551037884
	.long	1147550661
	.long	1543208500
	.long	-1958532746
	.long	-886847780
	.long	-1225917336
	.long	-1192955549
	.long	-684598070
	.long	1113818384
	.long	328671808
	.long	-2067394272
	.long	-2058738563
	.long	-759480840
	.long	-1359400431
	.long	-953573011
	.long	496906059
	.long	-592301837
	.long	226906860
	.long	2009195472
	.long	733156972
	.long	-1452230247
	.long	294930682
	.long	1206477858
	.long	-1459843900
	.long	-1594867942
	.long	1451044056
	.long	573804783
	.long	-2025238841
	.long	-650587711
	.long	-1932877058
	.long	-1730933962
	.long	-1493859889
	.long	-1518674392
	.long	-625504730
	.long	1068351396
	.long	742039012
	.long	1350078989
	.long	1784663195
	.long	1417561698
	.long	-158526526
	.long	-1864845080
	.long	775550814
	.long	-2101104651
	.long	-1621262146
	.long	1775276924
	.long	1876241833
	.long	-819653965
	.long	-928212677
	.long	270040487
	.long	-392404114
	.long	-616842373
	.long	-853116919
	.long	1851332852
	.long	-325404927
	.long	-2091935064
	.long	-426414491
	.long	-1426069890
	.long	566021896
	.long	-283776794
	.long	-1159226407
	.long	1248802510
	.long	-358676012
	.long	699432150
	.long	832877231
	.long	708780849
	.long	-962227152
	.long	899835584
	.long	1951317047
	.long	-58537306
	.long	-527380304
	.long	866637845
	.long	-251357110
	.long	1106041591
	.long	2144161806
	.long	395441711
	.long	1984812685
	.long	1139781709
	.long	-861254316
	.long	-459930401
	.long	-1630423581
	.long	1282050075
	.long	-1054072904
	.long	1181045119
	.long	-1654724092
	.long	25965917
	.long	-91786125
	.long	-83148498
	.long	-1285087910
	.long	-1831087534
	.long	-384805325
	.long	1842759443
	.long	-1697160820
	.long	933301370
	.long	1509430414
	.long	-351060855
	.long	-827774994
	.long	-1218328267
	.long	-518199827
	.long	2051518780
	.long	-1663901863
	.long	1441952575
	.long	404016761
	.long	1942435775
	.long	1408749034
	.long	1610459739
	.long	-549621996
	.long	2017778566
	.long	-894438527
	.long	-1184316354
	.long	941896748
	.long	-1029488545
	.long	371049330
	.long	-1126030068
	.long	675039627
	.long	-15887039
	.long	967311729
	.long	135050206
	.long	-659233636
	.long	1683407248
	.long	2076935265
	.long	-718096784
	.long	1215061108
	.long	-793225406
	.align 32
	.type	Te3, @object
	.size	Te3, 1024
Te3:
	.long	1667474886
	.long	2088535288
	.long	2004326894
	.long	2071694838
	.long	-219017729
	.long	1802223062
	.long	1869591006
	.long	-976923503
	.long	808472672
	.long	16843522
	.long	1734846926
	.long	724270422
	.long	-16901657
	.long	-673750347
	.long	-1414797747
	.long	1987484396
	.long	-892713585
	.long	-2105369313
	.long	-909557623
	.long	2105378810
	.long	-84273681
	.long	1499065266
	.long	1195886990
	.long	-252703749
	.long	-1381110719
	.long	-724277325
	.long	-1566376609
	.long	-1347425723
	.long	-1667449053
	.long	-1532692653
	.long	1920112356
	.long	-1061135461
	.long	-1212693899
	.long	-33743647
	.long	-1819038147
	.long	640051788
	.long	909531756
	.long	1061110142
	.long	-134806795
	.long	-859025533
	.long	875846760
	.long	-1515850671
	.long	-437963567
	.long	-235861767
	.long	1903268834
	.long	-656903253
	.long	825316194
	.long	353713962
	.long	67374088
	.long	-943238507
	.long	589522246
	.long	-1010606435
	.long	404236336
	.long	-1768513225
	.long	84217610
	.long	-1701137105
	.long	117901582
	.long	303183396
	.long	-2139055333
	.long	-488489505
	.long	-336910643
	.long	656894286
	.long	-1296904833
	.long	1970642922
	.long	151591698
	.long	-2088526307
	.long	741110872
	.long	437923380
	.long	454765878
	.long	1852748508
	.long	1515908788
	.long	-1600062629
	.long	1381168804
	.long	993742198
	.long	-690593353
	.long	-1280061827
	.long	690584402
	.long	-471646499
	.long	791638366
	.long	-2071685357
	.long	1398011302
	.long	-774805319
	.long	0
	.long	-303223615
	.long	538992704
	.long	-50585629
	.long	-1313748871
	.long	1532751286
	.long	1785380564
	.long	-875870579
	.long	-1094788761
	.long	960056178
	.long	1246420628
	.long	1280103576
	.long	1482221744
	.long	-808498555
	.long	-791647301
	.long	-269538619
	.long	-1431640753
	.long	-67430675
	.long	1128514950
	.long	1296947098
	.long	859002214
	.long	-2054843375
	.long	1162203018
	.long	-101117719
	.long	33687044
	.long	2139062782
	.long	1347481760
	.long	1010582648
	.long	-1616922075
	.long	-1465326773
	.long	1364325282
	.long	-1549533603
	.long	1077985408
	.long	-1886418427
	.long	-1835881153
	.long	-1650607071
	.long	943212656
	.long	-168491791
	.long	-1128472733
	.long	-1229536905
	.long	-623217233
	.long	555836226
	.long	269496352
	.long	-58651
	.long	-202174723
	.long	-757961281
	.long	-842183551
	.long	202118168
	.long	320025894
	.long	-320065597
	.long	1600119230
	.long	-1751670219
	.long	1145359496
	.long	387397934
	.long	-993765485
	.long	-1482165675
	.long	2122220284
	.long	1027426170
	.long	1684319432
	.long	1566435258
	.long	421079858
	.long	1936954854
	.long	1616945344
	.long	-2122213351
	.long	1330631070
	.long	-589529181
	.long	572679748
	.long	707427924
	.long	-1869567173
	.long	-2004319477
	.long	1179044492
	.long	-286381625
	.long	-1195846805
	.long	336870440
	.long	-555845209
	.long	1583276732
	.long	185277718
	.long	-606374227
	.long	-522175525
	.long	842159716
	.long	976899700
	.long	168435220
	.long	1229577106
	.long	101059084
	.long	606366792
	.long	1549591736
	.long	-1027449441
	.long	-741118275
	.long	-1397952701
	.long	1650632388
	.long	-1852725191
	.long	-1785355215
	.long	-454805549
	.long	2038008818
	.long	-404278571
	.long	-926399605
	.long	926374254
	.long	1835907034
	.long	-1920103423
	.long	-707435343
	.long	1313788572
	.long	-1448484791
	.long	1819063512
	.long	1448540844
	.long	-185333773
	.long	-353753649
	.long	1701162954
	.long	2054852340
	.long	-1364268729
	.long	134748176
	.long	-1162160785
	.long	2021165296
	.long	623210314
	.long	774795868
	.long	471606328
	.long	-1499008681
	.long	-1263220877
	.long	-960081513
	.long	-387439669
	.long	-572687199
	.long	1953799400
	.long	522133822
	.long	1263263126
	.long	-1111630751
	.long	-1953790451
	.long	-1970633457
	.long	1886425312
	.long	1044267644
	.long	-1246378895
	.long	1718004428
	.long	1212733584
	.long	50529542
	.long	-151649801
	.long	235803164
	.long	1633788866
	.long	892690282
	.long	1465383342
	.long	-1179004823
	.long	-2038001385
	.long	-1044293479
	.long	488449850
	.long	-1633765081
	.long	-505333543
	.long	-117959701
	.long	-1734823125
	.long	286339874
	.long	1768537042
	.long	-640061271
	.long	-1903261433
	.long	-1802197197
	.long	-1684294099
	.long	505291324
	.long	-2021158379
	.long	-370597687
	.long	-825341561
	.long	1431699370
	.long	673740880
	.long	-539002203
	.long	-1936945405
	.long	-1583220647
	.long	-1987477495
	.long	218961690
	.long	-1077945755
	.long	-421121577
	.long	1111672452
	.long	1751693520
	.long	1094828930
	.long	-1717981143
	.long	757954394
	.long	252645662
	.long	-1330590853
	.long	1414855848
	.long	-1145317779
	.long	370555436
	.align 32
	.type	Te2, @object
	.size	Te2, 1024
Te2:
	.long	1671808611
	.long	2089089148
	.long	2006576759
	.long	2072901243
	.long	-233963534
	.long	1807603307
	.long	1873927791
	.long	-984313403
	.long	810573872
	.long	16974337
	.long	1739181671
	.long	729634347
	.long	-31856642
	.long	-681396777
	.long	-1410970197
	.long	1989864566
	.long	-901410870
	.long	-2103631998
	.long	-918517303
	.long	2106063485
	.long	-99225606
	.long	1508618841
	.long	1204391495
	.long	-267650064
	.long	-1377025619
	.long	-731401260
	.long	-1560453214
	.long	-1343601233
	.long	-1665195108
	.long	-1527295068
	.long	1922491506
	.long	-1067738176
	.long	-1211992649
	.long	-48438787
	.long	-1817297517
	.long	644500518
	.long	911895606
	.long	1061256767
	.long	-150800905
	.long	-867204148
	.long	878471220
	.long	-1510714971
	.long	-449523227
	.long	-251069967
	.long	1905517169
	.long	-663508008
	.long	827548209
	.long	356461077
	.long	67897348
	.long	-950889017
	.long	593839651
	.long	-1017209405
	.long	405286936
	.long	-1767819370
	.long	84871685
	.long	-1699401830
	.long	118033927
	.long	305538066
	.long	-2137318528
	.long	-499261470
	.long	-349778453
	.long	661212711
	.long	-1295155278
	.long	1973414517
	.long	152769033
	.long	-2086789757
	.long	745822252
	.long	439235610
	.long	455947803
	.long	1857215598
	.long	1525593178
	.long	-1594139744
	.long	1391895634
	.long	994932283
	.long	-698239018
	.long	-1278313037
	.long	695947817
	.long	-482419229
	.long	795958831
	.long	-2070473852
	.long	1408607827
	.long	-781665839
	.long	0
	.long	-315833875
	.long	543178784
	.long	-65018884
	.long	-1312261711
	.long	1542305371
	.long	1790891114
	.long	-884568629
	.long	-1093048386
	.long	961245753
	.long	1256100938
	.long	1289001036
	.long	1491644504
	.long	-817199665
	.long	-798245936
	.long	-282409489
	.long	-1427812438
	.long	-82383365
	.long	1137018435
	.long	1305975373
	.long	861234739
	.long	-2053893755
	.long	1171229253
	.long	-116332039
	.long	33948674
	.long	2139225727
	.long	1357946960
	.long	1011120188
	.long	-1615190625
	.long	-1461498968
	.long	1374921297
	.long	-1543610973
	.long	1086357568
	.long	-1886780017
	.long	-1834139758
	.long	-1648615011
	.long	944271416
	.long	-184225291
	.long	-1126210628
	.long	-1228834890
	.long	-629821478
	.long	560153121
	.long	271589392
	.long	-15014401
	.long	-217121293
	.long	-764559406
	.long	-850624051
	.long	202643468
	.long	322250259
	.long	-332413972
	.long	1608629855
	.long	-1750977129
	.long	1154254916
	.long	389623319
	.long	-1000893500
	.long	-1477290585
	.long	2122513534
	.long	1028094525
	.long	1689045092
	.long	1575467613
	.long	422261273
	.long	1939203699
	.long	1621147744
	.long	-2120738431
	.long	1339137615
	.long	-595614756
	.long	577127458
	.long	712922154
	.long	-1867826288
	.long	-2004677752
	.long	1187679302
	.long	-299251730
	.long	-1194103880
	.long	339486740
	.long	-562452514
	.long	1591917662
	.long	186455563
	.long	-612979237
	.long	-532948000
	.long	844522546
	.long	978220090
	.long	169743370
	.long	1239126601
	.long	101321734
	.long	611076132
	.long	1558493276
	.long	-1034051646
	.long	-747717165
	.long	-1393605716
	.long	1655096418
	.long	-1851246191
	.long	-1784401515
	.long	-466103324
	.long	2039214713
	.long	-416098841
	.long	-935097400
	.long	928607799
	.long	1840765549
	.long	-1920204403
	.long	-714821163
	.long	1322425422
	.long	-1444918871
	.long	1823791212
	.long	1459268694
	.long	-200805388
	.long	-366620694
	.long	1706019429
	.long	2056189050
	.long	-1360443474
	.long	135794696
	.long	-1160417350
	.long	2022240376
	.long	628050469
	.long	779246638
	.long	472135708
	.long	-1494132826
	.long	-1261997132
	.long	-967731258
	.long	-400307224
	.long	-579034659
	.long	1956440180
	.long	522272287
	.long	1272813131
	.long	-1109630531
	.long	-1954148981
	.long	-1970991222
	.long	1888542832
	.long	1044544574
	.long	-1245417035
	.long	1722469478
	.long	1222152264
	.long	50660867
	.long	-167643146
	.long	236067854
	.long	1638122081
	.long	895445557
	.long	1475980887
	.long	-1177523783
	.long	-2037311610
	.long	-1051158079
	.long	489110045
	.long	-1632032866
	.long	-516367903
	.long	-132912136
	.long	-1733088360
	.long	288563729
	.long	1773916777
	.long	-646927911
	.long	-1903622258
	.long	-1800981612
	.long	-1682559589
	.long	505560094
	.long	-2020469369
	.long	-383727127
	.long	-834041906
	.long	1442818645
	.long	678973480
	.long	-545610273
	.long	-1936784500
	.long	-1577559647
	.long	-1988097655
	.long	219617805
	.long	-1076206145
	.long	-432941082
	.long	1120306242
	.long	1756942440
	.long	1103331905
	.long	-1716508263
	.long	762796589
	.long	252780047
	.long	-1328841808
	.long	1425844308
	.long	-1143575109
	.long	372911126
	.align 32
	.type	Te1, @object
	.size	Te1, 1024
Te1:
	.long	-1513725085
	.long	-2064089988
	.long	-1712425097
	.long	-1913226373
	.long	234877682
	.long	-1110021269
	.long	-1310822545
	.long	1418839493
	.long	1348481072
	.long	50462977
	.long	-1446090905
	.long	2102799147
	.long	434634494
	.long	1656084439
	.long	-431117397
	.long	-1695779210
	.long	1167051466
	.long	-1658879358
	.long	1082771913
	.long	-2013627011
	.long	368048890
	.long	-340633255
	.long	-913422521
	.long	201060592
	.long	-331240019
	.long	1739838676
	.long	-44064094
	.long	-364531793
	.long	-1088185188
	.long	-145513308
	.long	-1763413390
	.long	1536934080
	.long	-1032472649
	.long	484572669
	.long	-1371696237
	.long	1783375398
	.long	1517041206
	.long	1098792767
	.long	49674231
	.long	1334037708
	.long	1550332980
	.long	-195975771
	.long	886171109
	.long	150598129
	.long	-1813876367
	.long	1940642008
	.long	1398944049
	.long	1059722517
	.long	201851908
	.long	1385547719
	.long	1699095331
	.long	1587397571
	.long	674240536
	.long	-1590192490
	.long	252314885
	.long	-1255171430
	.long	151914247
	.long	908333586
	.long	-1692696448
	.long	1038082786
	.long	651029483
	.long	1766729511
	.long	-847269198
	.long	-1612024459
	.long	454166793
	.long	-1642232957
	.long	1951935532
	.long	775166490
	.long	758520603
	.long	-1294176658
	.long	-290170278
	.long	-77881184
	.long	-157003182
	.long	1299594043
	.long	1639438038
	.long	-830622797
	.long	2068982057
	.long	1054729187
	.long	1901997871
	.long	-1760328572
	.long	-173649069
	.long	1757008337
	.long	0
	.long	750906861
	.long	1614815264
	.long	535035132
	.long	-931548751
	.long	-306816165
	.long	-1093375382
	.long	1183697867
	.long	-647512386
	.long	1265776953
	.long	-560706998
	.long	-728216500
	.long	-391096232
	.long	1250283471
	.long	1807470800
	.long	717615087
	.long	-447763798
	.long	384695291
	.long	-981056701
	.long	-677753523
	.long	1432761139
	.long	-1810791035
	.long	-813021883
	.long	283769337
	.long	100925954
	.long	-2114027649
	.long	-257929136
	.long	1148730428
	.long	-1171939425
	.long	-481580888
	.long	-207466159
	.long	-27417693
	.long	-1065336768
	.long	-1979347057
	.long	-1388342638
	.long	-1138647651
	.long	1215313976
	.long	82966005
	.long	-547111748
	.long	-1049119050
	.long	1974459098
	.long	1665278241
	.long	807407632
	.long	451280895
	.long	251524083
	.long	1841287890
	.long	1283575245
	.long	337120268
	.long	891687699
	.long	801369324
	.long	-507617441
	.long	-1573546089
	.long	-863484860
	.long	959321879
	.long	1469301956
	.long	-229267545
	.long	-2097381762
	.long	1199193405
	.long	-1396153244
	.long	-407216803
	.long	724703513
	.long	-1780059277
	.long	-1598005152
	.long	-1743158911
	.long	-778154161
	.long	2141445340
	.long	1715741218
	.long	2119445034
	.long	-1422159728
	.long	-2096396152
	.long	-896776634
	.long	700968686
	.long	-747915080
	.long	1009259540
	.long	2041044702
	.long	-490971554
	.long	487983883
	.long	1991105499
	.long	1004265696
	.long	1449407026
	.long	1316239930
	.long	504629770
	.long	-611169975
	.long	168560134
	.long	1816667172
	.long	-457679780
	.long	1570751170
	.long	1857934291
	.long	-280777556
	.long	-1497079198
	.long	-1472622191
	.long	-1540254315
	.long	936633572
	.long	-1947043463
	.long	852879335
	.long	1133234376
	.long	1500395319
	.long	-1210421907
	.long	-1946055283
	.long	1689376213
	.long	-761508274
	.long	-532043351
	.long	-1260884884
	.long	-89369002
	.long	133428468
	.long	634383082
	.long	-1345690267
	.long	-1896580486
	.long	-381178194
	.long	403703816
	.long	-714097990
	.long	-1997506440
	.long	1867130149
	.long	1918643758
	.long	607656988
	.long	-245913946
	.long	-948718412
	.long	1368901318
	.long	600565992
	.long	2090982877
	.long	-1662487436
	.long	557719327
	.long	-577352885
	.long	-597574211
	.long	-2045932661
	.long	-2062579062
	.long	-1864339344
	.long	1115438654
	.long	-999180875
	.long	-1429445018
	.long	-661632952
	.long	84280067
	.long	33027830
	.long	303828494
	.long	-1547542175
	.long	1600795957
	.long	-106014889
	.long	-798377543
	.long	-1860729210
	.long	1486471617
	.long	658119965
	.long	-1188585826
	.long	953803233
	.long	334231800
	.long	-1288988520
	.long	857870609
	.long	-1143838359
	.long	1890179545
	.long	-1995993458
	.long	-1489791852
	.long	-1238525029
	.long	574365214
	.long	-1844082809
	.long	550103529
	.long	1233637070
	.long	-5614251
	.long	2018519080
	.long	2057691103
	.long	-1895592820
	.long	-128343647
	.long	-2146858615
	.long	387583245
	.long	-630865985
	.long	836232934
	.long	-964410814
	.long	-1194301336
	.long	-1014873791
	.long	-1339450983
	.long	2002398509
	.long	287182607
	.long	-881086288
	.long	-56077228
	.long	-697451589
	.long	975967766
	.align 32
	.type	Te0, @object
	.size	Te0, 1024
Te0:
	.long	-966564955
	.long	-126059388
	.long	-294160487
	.long	-159679603
	.long	-855539
	.long	-697603139
	.long	-563122255
	.long	-1849309868
	.long	1613770832
	.long	33620227
	.long	-832084055
	.long	1445669757
	.long	-402719207
	.long	-1244145822
	.long	1303096294
	.long	-327780710
	.long	-1882535355
	.long	528646813
	.long	-1983264448
	.long	-92439161
	.long	-268764651
	.long	-1302767125
	.long	-1907931191
	.long	-68095989
	.long	1101901292
	.long	-1277897625
	.long	1604494077
	.long	1169141738
	.long	597466303
	.long	1403299063
	.long	-462261610
	.long	-1681866661
	.long	1974974402
	.long	-503448292
	.long	1033081774
	.long	1277568618
	.long	1815492186
	.long	2118074177
	.long	-168298750
	.long	-2083730353
	.long	1748251740
	.long	1369810420
	.long	-773462732
	.long	-101584632
	.long	-495881837
	.long	-1411852173
	.long	1647391059
	.long	706024767
	.long	134480908
	.long	-1782069422
	.long	1176707941
	.long	-1648114850
	.long	806885416
	.long	932615841
	.long	168101135
	.long	798661301
	.long	235341577
	.long	605164086
	.long	461406363
	.long	-538779075
	.long	-840176858
	.long	1311188841
	.long	2142417613
	.long	-361400929
	.long	302582043
	.long	495158174
	.long	1479289972
	.long	874125870
	.long	907746093
	.long	-596742478
	.long	-1269146898
	.long	1537253627
	.long	-1538108682
	.long	1983593293
	.long	-1210657183
	.long	2108928974
	.long	1378429307
	.long	-572267714
	.long	1580150641
	.long	327451799
	.long	-1504488459
	.long	-1177431704
	.long	0
	.long	-1041371860
	.long	1075847264
	.long	-469959649
	.long	2041688520
	.long	-1235526675
	.long	-731223362
	.long	-1916023994
	.long	1740553945
	.long	1916352843
	.long	-1807070498
	.long	-1739830060
	.long	-1336387352
	.long	-2049978550
	.long	-1143943061
	.long	-974131414
	.long	1336584933
	.long	-302253290
	.long	-2042412091
	.long	-1706209833
	.long	1714631509
	.long	293963156
	.long	-1975171633
	.long	-369493744
	.long	67240454
	.long	-25198719
	.long	-1605349136
	.long	2017213508
	.long	631218106
	.long	1269344483
	.long	-1571728909
	.long	1571005438
	.long	-2143272768
	.long	93294474
	.long	1066570413
	.long	563977660
	.long	1882732616
	.long	-235539196
	.long	1673313503
	.long	2008463041
	.long	-1344611723
	.long	1109467491
	.long	537923632
	.long	-436207846
	.long	-34344178
	.long	-1076702611
	.long	-2117218996
	.long	403442708
	.long	638784309
	.long	-1007883217
	.long	-1101045791
	.long	899127202
	.long	-2008791860
	.long	773265209
	.long	-1815821225
	.long	1437050866
	.long	-58818942
	.long	2050833735
	.long	-932944724
	.long	-1168286233
	.long	840505643
	.long	-428641387
	.long	-1067425632
	.long	427917720
	.long	-1638969391
	.long	-1545806721
	.long	1143087718
	.long	1412049534
	.long	999329963
	.long	193497219
	.long	-1941551414
	.long	-940642775
	.long	1807268051
	.long	672404540
	.long	-1478566279
	.long	-1134666014
	.long	369822493
	.long	-1378100362
	.long	-606019525
	.long	1681011286
	.long	1949973070
	.long	336202270
	.long	-1840690725
	.long	201721354
	.long	1210328172
	.long	-1201906460
	.long	-1614626211
	.long	-1110191250
	.long	1135389935
	.long	-1000185178
	.long	965841320
	.long	831886756
	.long	-739974089
	.long	-226920053
	.long	-706222286
	.long	-1949775805
	.long	1849112409
	.long	-630362697
	.long	26054028
	.long	-1311386268
	.long	-1672589614
	.long	1235855840
	.long	-663982924
	.long	-1403627782
	.long	-202050553
	.long	-806688219
	.long	-899324497
	.long	-193299826
	.long	1202630377
	.long	268961816
	.long	1874508501
	.long	-260540280
	.long	1243948399
	.long	1546530418
	.long	941366308
	.long	1470539505
	.long	1941222599
	.long	-1748580783
	.long	-873928669
	.long	-1579295364
	.long	-395021156
	.long	1042226977
	.long	-1773450275
	.long	1639824860
	.long	227249030
	.long	260737669
	.long	-529502064
	.long	2084453954
	.long	1907733956
	.long	-865704278
	.long	-1874310952
	.long	100860677
	.long	-134810111
	.long	470683154
	.long	-1033805405
	.long	1781871967
	.long	-1370007559
	.long	1773779408
	.long	394692241
	.long	-1715355304
	.long	974986535
	.long	664706745
	.long	-639508168
	.long	-336005101
	.long	731420851
	.long	571543859
	.long	-764843589
	.long	-1445340816
	.long	126783113
	.long	865375399
	.long	765172662
	.long	1008606754
	.long	361203602
	.long	-907417312
	.long	-2016489911
	.long	-1437248001
	.long	1344809080
	.long	-1512054918
	.long	59542671
	.long	1503764984
	.long	160008576
	.long	437062935
	.long	1707065306
	.long	-672733647
	.long	-2076032314
	.long	-798463816
	.long	-2109652541
	.long	697932208
	.long	1512910199
	.long	504303377
	.long	2075177163
	.long	-1470868228
	.long	1841019862
	.long	739644986
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
