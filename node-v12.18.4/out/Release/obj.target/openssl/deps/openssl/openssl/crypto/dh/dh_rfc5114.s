	.file	"dh_rfc5114.c"
	.text
	.p2align 4
	.globl	DH_get_1024_160
	.type	DH_get_1024_160, @function
DH_get_1024_160:
.LFB421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	DH_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1
	leaq	_bignum_dh1024_160_p(%rip), %rdi
	call	BN_dup@PLT
	leaq	_bignum_dh1024_160_g(%rip), %rdi
	movq	%rax, 8(%r12)
	call	BN_dup@PLT
	leaq	_bignum_dh1024_160_q(%rip), %rdi
	movq	%rax, 16(%r12)
	call	BN_dup@PLT
	cmpq	$0, 8(%r12)
	movq	%rax, 64(%r12)
	je	.L3
	testq	%rax, %rax
	je	.L3
	cmpq	$0, 16(%r12)
	je	.L3
.L1:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	DH_free@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE421:
	.size	DH_get_1024_160, .-DH_get_1024_160
	.p2align 4
	.globl	DH_get_2048_224
	.type	DH_get_2048_224, @function
DH_get_2048_224:
.LFB422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	DH_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L9
	leaq	_bignum_dh2048_224_p(%rip), %rdi
	call	BN_dup@PLT
	leaq	_bignum_dh2048_224_g(%rip), %rdi
	movq	%rax, 8(%r12)
	call	BN_dup@PLT
	leaq	_bignum_dh2048_224_q(%rip), %rdi
	movq	%rax, 16(%r12)
	call	BN_dup@PLT
	cmpq	$0, 8(%r12)
	movq	%rax, 64(%r12)
	je	.L11
	testq	%rax, %rax
	je	.L11
	cmpq	$0, 16(%r12)
	je	.L11
.L9:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	DH_free@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE422:
	.size	DH_get_2048_224, .-DH_get_2048_224
	.p2align 4
	.globl	DH_get_2048_256
	.type	DH_get_2048_256, @function
DH_get_2048_256:
.LFB423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	DH_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L16
	leaq	_bignum_dh2048_256_p(%rip), %rdi
	call	BN_dup@PLT
	leaq	_bignum_dh2048_256_g(%rip), %rdi
	movq	%rax, 8(%r12)
	call	BN_dup@PLT
	leaq	_bignum_dh2048_256_q(%rip), %rdi
	movq	%rax, 16(%r12)
	call	BN_dup@PLT
	cmpq	$0, 8(%r12)
	movq	%rax, 64(%r12)
	je	.L18
	testq	%rax, %rax
	je	.L18
	cmpq	$0, 16(%r12)
	je	.L18
.L16:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	DH_free@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE423:
	.size	DH_get_2048_256, .-DH_get_2048_256
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
