	.file	"store_lib.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"file"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../deps/openssl/openssl/crypto/store/store_lib.c"
	.text
	.p2align 4
	.globl	OSSL_STORE_open
	.type	OSSL_STORE_open, @function
OSSL_STORE_open:
.LFB814:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	.LC0(%rip), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-320(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdi, %rsi
	movq	%r13, %rdi
	subq	$328, %rsp
	movq	%rdx, -352(%rbp)
	movl	$256, %edx
	movq	%rcx, -360(%rbp)
	movq	%r8, -368(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -336(%rbp)
	call	OPENSSL_strlcpy@PLT
	movl	$58, %esi
	movq	%r13, %rdi
	call	strchr@PLT
	testq	%rax, %rax
	je	.L4
	movb	$0, (%rax)
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	strcasecmp@PLT
	testl	%eax, %eax
	jne	.L28
.L4:
	movq	$1, -344(%rbp)
.L3:
	call	ERR_set_mark@PLT
	xorl	%r13d, %r13d
	leaq	-336(%rbp), %r14
.L11:
	movq	(%r14,%r13,8), %rdi
	call	ossl_store_get0_loader_int@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L6
	movq	-352(%rbp), %rcx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	*16(%rax)
	leaq	1(%r13), %r8
	movq	%rax, %r10
	cmpq	%r8, -344(%rbp)
	jbe	.L9
	testq	%rax, %rax
	jne	.L25
	movl	$1, %r13d
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L9:
	testq	%rax, %rax
	je	.L10
.L25:
	movl	$83, %edx
	leaq	.LC1(%rip), %rsi
	movl	$56, %edi
	movq	%r10, -344(%rbp)
	call	CRYPTO_zalloc@PLT
	movq	-344(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L29
	movq	%r15, %xmm0
	movq	%r10, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rax)
	movq	%rbx, %xmm0
	movhps	-352(%rbp), %xmm0
	movups	%xmm0, 16(%rax)
	movq	-360(%rbp), %rax
	movq	%rax, 32(%r12)
	movq	-368(%rbp), %rax
	movq	%rax, 40(%r12)
	call	ERR_pop_to_mark@PLT
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L30
	addq	$328, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	addq	$1, %r13
	cmpq	%r13, -344(%rbp)
	ja	.L11
.L10:
	call	ERR_clear_last_mark@PLT
	xorl	%r12d, %r12d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L28:
	movzbl	1(%r14), %eax
	subl	$47, %eax
	je	.L31
.L5:
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	leaq	1(%rax), %rcx
	movq	%r13, -336(%rbp,%rax,8)
	movq	%rcx, -344(%rbp)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L29:
	movl	$84, %r8d
	movl	$65, %edx
	movl	$114, %esi
	movl	$44, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	call	ERR_clear_last_mark@PLT
	movq	-344(%rbp), %r10
	movq	%r10, %rdi
	call	*72(%r15)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L31:
	movzbl	2(%r14), %eax
	subl	$47, %eax
	jmp	.L5
.L30:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE814:
	.size	OSSL_STORE_open, .-OSSL_STORE_open
	.p2align 4
	.globl	OSSL_STORE_ctrl
	.type	OSSL_STORE_ctrl, @function
OSSL_STORE_ctrl:
.LFB815:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$208, %rsp
	movq	%rdx, -160(%rbp)
	movq	%rcx, -152(%rbp)
	movq	%r8, -144(%rbp)
	movq	%r9, -136(%rbp)
	testb	%al, %al
	je	.L33
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm4, -64(%rbp)
	movaps	%xmm5, -48(%rbp)
	movaps	%xmm6, -32(%rbp)
	movaps	%xmm7, -16(%rbp)
.L33:
	movq	%fs:40, %rax
	movq	%rax, -184(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	movl	$16, -208(%rbp)
	movq	%rax, -200(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	(%rdi), %rax
	movl	$48, -204(%rbp)
	movq	24(%rax), %rcx
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L32
	movq	8(%rdi), %rdi
	leaq	-208(%rbp), %rdx
	call	*%rcx
.L32:
	movq	-184(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L39
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L39:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE815:
	.size	OSSL_STORE_ctrl, .-OSSL_STORE_ctrl
	.p2align 4
	.globl	OSSL_STORE_vctrl
	.type	OSSL_STORE_vctrl, @function
OSSL_STORE_vctrl:
.LFB816:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L41
	movq	8(%rdi), %rdi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L41:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE816:
	.size	OSSL_STORE_vctrl, .-OSSL_STORE_vctrl
	.p2align 4
	.globl	OSSL_STORE_expect
	.type	OSSL_STORE_expect, @function
OSSL_STORE_expect:
.LFB817:
	.cfi_startproc
	endbr64
	movl	52(%rdi), %eax
	testl	%eax, %eax
	jne	.L50
	movq	(%rdi), %rax
	movl	%esi, 48(%rdi)
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.L45
	movq	8(%rdi), %rdi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L45:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$117, %edx
	movl	$130, %esi
	movl	$44, %edi
	movl	$139, %r8d
	leaq	.LC1(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE817:
	.size	OSSL_STORE_expect, .-OSSL_STORE_expect
	.p2align 4
	.globl	OSSL_STORE_find
	.type	OSSL_STORE_find, @function
OSSL_STORE_find:
.LFB818:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	52(%rdi), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%eax, %eax
	jne	.L56
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L57
	movq	8(%rdi), %rdi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	movl	$153, %r8d
	movl	$117, %edx
	movl	$131, %esi
	movl	$44, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
.L51:
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	movl	$158, %r8d
	movl	$118, %edx
	movl	$131, %esi
	movl	$44, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L51
	.cfi_endproc
.LFE818:
	.size	OSSL_STORE_find, .-OSSL_STORE_find
	.p2align 4
	.globl	OSSL_STORE_error
	.type	OSSL_STORE_error, @function
OSSL_STORE_error:
.LFB820:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	8(%rdi), %r8
	movq	64(%rax), %rax
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE820:
	.size	OSSL_STORE_error, .-OSSL_STORE_error
	.p2align 4
	.globl	OSSL_STORE_eof
	.type	OSSL_STORE_eof, @function
OSSL_STORE_eof:
.LFB821:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	8(%rdi), %r8
	movq	56(%rax), %rax
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE821:
	.size	OSSL_STORE_eof, .-OSSL_STORE_eof
	.p2align 4
	.globl	OSSL_STORE_close
	.type	OSSL_STORE_close, @function
OSSL_STORE_close:
.LFB822:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	8(%rdi), %rdi
	movq	(%r12), %rax
	call	*72(%rax)
	movq	%r12, %rdi
	movl	$223, %edx
	leaq	.LC1(%rip), %rsi
	movl	%eax, %r13d
	call	CRYPTO_free@PLT
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE822:
	.size	OSSL_STORE_close, .-OSSL_STORE_close
	.p2align 4
	.globl	OSSL_STORE_INFO_new_NAME
	.type	OSSL_STORE_INFO_new_NAME, @function
OSSL_STORE_INFO_new_NAME:
.LFB824:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$236, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$24, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L66
	movl	$1, (%rax)
	movq	%rbx, 8(%rax)
	movq	$0, 16(%rax)
.L62:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movl	$251, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$65, %edx
	movl	$109, %esi
	movl	$44, %edi
	movq	%rax, -24(%rbp)
	call	ERR_put_error@PLT
	movq	-24(%rbp), %rax
	jmp	.L62
	.cfi_endproc
.LFE824:
	.size	OSSL_STORE_INFO_new_NAME, .-OSSL_STORE_INFO_new_NAME
	.p2align 4
	.globl	OSSL_STORE_INFO_set0_NAME_description
	.type	OSSL_STORE_INFO_set0_NAME_description, @function
OSSL_STORE_INFO_set0_NAME_description:
.LFB825:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	cmpl	$1, %eax
	jne	.L74
	movq	%rsi, 16(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$7, %edx
	movl	$134, %esi
	movl	$44, %edi
	movl	$265, %r8d
	leaq	.LC1(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE825:
	.size	OSSL_STORE_INFO_set0_NAME_description, .-OSSL_STORE_INFO_set0_NAME_description
	.p2align 4
	.globl	OSSL_STORE_INFO_new_PARAMS
	.type	OSSL_STORE_INFO_new_PARAMS, @function
OSSL_STORE_INFO_new_PARAMS:
.LFB826:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$236, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$24, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L79
	movl	$2, (%rax)
	movq	%rbx, 8(%rax)
.L75:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	movl	$279, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$65, %edx
	movl	$110, %esi
	movl	$44, %edi
	movq	%rax, -24(%rbp)
	call	ERR_put_error@PLT
	movq	-24(%rbp), %rax
	jmp	.L75
	.cfi_endproc
.LFE826:
	.size	OSSL_STORE_INFO_new_PARAMS, .-OSSL_STORE_INFO_new_PARAMS
	.p2align 4
	.globl	OSSL_STORE_INFO_new_PKEY
	.type	OSSL_STORE_INFO_new_PKEY, @function
OSSL_STORE_INFO_new_PKEY:
.LFB827:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$236, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$24, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L84
	movl	$3, (%rax)
	movq	%rbx, 8(%rax)
.L80:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	movl	$289, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$65, %edx
	movl	$111, %esi
	movl	$44, %edi
	movq	%rax, -24(%rbp)
	call	ERR_put_error@PLT
	movq	-24(%rbp), %rax
	jmp	.L80
	.cfi_endproc
.LFE827:
	.size	OSSL_STORE_INFO_new_PKEY, .-OSSL_STORE_INFO_new_PKEY
	.p2align 4
	.globl	OSSL_STORE_INFO_new_CERT
	.type	OSSL_STORE_INFO_new_CERT, @function
OSSL_STORE_INFO_new_CERT:
.LFB828:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$236, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$24, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L89
	movl	$4, (%rax)
	movq	%rbx, 8(%rax)
.L85:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	movl	$299, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$65, %edx
	movl	$106, %esi
	movl	$44, %edi
	movq	%rax, -24(%rbp)
	call	ERR_put_error@PLT
	movq	-24(%rbp), %rax
	jmp	.L85
	.cfi_endproc
.LFE828:
	.size	OSSL_STORE_INFO_new_CERT, .-OSSL_STORE_INFO_new_CERT
	.p2align 4
	.globl	OSSL_STORE_INFO_new_CRL
	.type	OSSL_STORE_INFO_new_CRL, @function
OSSL_STORE_INFO_new_CRL:
.LFB829:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$236, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$24, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L94
	movl	$5, (%rax)
	movq	%rbx, 8(%rax)
.L90:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	movl	$309, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$65, %edx
	movl	$107, %esi
	movl	$44, %edi
	movq	%rax, -24(%rbp)
	call	ERR_put_error@PLT
	movq	-24(%rbp), %rax
	jmp	.L90
	.cfi_endproc
.LFE829:
	.size	OSSL_STORE_INFO_new_CRL, .-OSSL_STORE_INFO_new_CRL
	.p2align 4
	.globl	OSSL_STORE_INFO_get_type
	.type	OSSL_STORE_INFO_get_type, @function
OSSL_STORE_INFO_get_type:
.LFB830:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	ret
	.cfi_endproc
.LFE830:
	.size	OSSL_STORE_INFO_get_type, .-OSSL_STORE_INFO_get_type
	.p2align 4
	.globl	OSSL_STORE_INFO_get0_NAME
	.type	OSSL_STORE_INFO_get0_NAME, @function
OSSL_STORE_INFO_get0_NAME:
.LFB831:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$1, (%rdi)
	je	.L99
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE831:
	.size	OSSL_STORE_INFO_get0_NAME, .-OSSL_STORE_INFO_get0_NAME
	.p2align 4
	.globl	OSSL_STORE_INFO_get1_NAME
	.type	OSSL_STORE_INFO_get1_NAME, @function
OSSL_STORE_INFO_get1_NAME:
.LFB832:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	cmpl	$1, (%rdi)
	je	.L104
	movl	$339, %r8d
	movl	$103, %edx
	movl	$103, %esi
	movl	$44, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L100:
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	movl	$332, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_strdup@PLT
	testq	%rax, %rax
	jne	.L100
	movl	$335, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$65, %edx
	movl	$103, %esi
	movl	$44, %edi
	movq	%rax, -8(%rbp)
	call	ERR_put_error@PLT
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE832:
	.size	OSSL_STORE_INFO_get1_NAME, .-OSSL_STORE_INFO_get1_NAME
	.p2align 4
	.globl	OSSL_STORE_INFO_get0_NAME_description
	.type	OSSL_STORE_INFO_get0_NAME_description, @function
OSSL_STORE_INFO_get0_NAME_description:
.LFB833:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$1, (%rdi)
	je	.L108
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE833:
	.size	OSSL_STORE_INFO_get0_NAME_description, .-OSSL_STORE_INFO_get0_NAME_description
	.section	.rodata.str1.1
.LC2:
	.string	""
	.text
	.p2align 4
	.globl	OSSL_STORE_INFO_get1_NAME_description
	.type	OSSL_STORE_INFO_get1_NAME_description, @function
OSSL_STORE_INFO_get1_NAME_description:
.LFB834:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	cmpl	$1, (%rdi)
	jne	.L110
	movq	16(%rdi), %rdi
	leaq	.LC2(%rip), %rax
	movl	$354, %edx
	leaq	.LC1(%rip), %rsi
	testq	%rdi, %rdi
	cmove	%rax, %rdi
	call	CRYPTO_strdup@PLT
	testq	%rax, %rax
	je	.L115
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	movl	$362, %r8d
	movl	$103, %edx
	movl	$135, %esi
	movl	$44, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	movl	$358, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$65, %edx
	movl	$135, %esi
	movl	$44, %edi
	movq	%rax, -8(%rbp)
	call	ERR_put_error@PLT
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE834:
	.size	OSSL_STORE_INFO_get1_NAME_description, .-OSSL_STORE_INFO_get1_NAME_description
	.p2align 4
	.globl	OSSL_STORE_INFO_get0_PARAMS
	.type	OSSL_STORE_INFO_get0_PARAMS, @function
OSSL_STORE_INFO_get0_PARAMS:
.LFB835:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$2, (%rdi)
	je	.L119
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE835:
	.size	OSSL_STORE_INFO_get0_PARAMS, .-OSSL_STORE_INFO_get0_PARAMS
	.p2align 4
	.globl	OSSL_STORE_INFO_get1_PARAMS
	.type	OSSL_STORE_INFO_get1_PARAMS, @function
OSSL_STORE_INFO_get1_PARAMS:
.LFB836:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	cmpl	$2, (%rdi)
	je	.L124
	movl	$380, %r8d
	movl	$104, %edx
	movl	$104, %esi
	movl	$44, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	call	EVP_PKEY_up_ref@PLT
	movq	8(%rbx), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE836:
	.size	OSSL_STORE_INFO_get1_PARAMS, .-OSSL_STORE_INFO_get1_PARAMS
	.p2align 4
	.globl	OSSL_STORE_INFO_get0_PKEY
	.type	OSSL_STORE_INFO_get0_PKEY, @function
OSSL_STORE_INFO_get0_PKEY:
.LFB837:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$3, (%rdi)
	je	.L128
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE837:
	.size	OSSL_STORE_INFO_get0_PKEY, .-OSSL_STORE_INFO_get0_PKEY
	.p2align 4
	.globl	OSSL_STORE_INFO_get1_PKEY
	.type	OSSL_STORE_INFO_get1_PKEY, @function
OSSL_STORE_INFO_get1_PKEY:
.LFB838:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	cmpl	$3, (%rdi)
	je	.L133
	movl	$398, %r8d
	movl	$102, %edx
	movl	$105, %esi
	movl	$44, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore_state
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	call	EVP_PKEY_up_ref@PLT
	movq	8(%rbx), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE838:
	.size	OSSL_STORE_INFO_get1_PKEY, .-OSSL_STORE_INFO_get1_PKEY
	.p2align 4
	.globl	OSSL_STORE_INFO_get0_CERT
	.type	OSSL_STORE_INFO_get0_CERT, @function
OSSL_STORE_INFO_get0_CERT:
.LFB839:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$4, (%rdi)
	je	.L137
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE839:
	.size	OSSL_STORE_INFO_get0_CERT, .-OSSL_STORE_INFO_get0_CERT
	.p2align 4
	.globl	OSSL_STORE_INFO_get1_CERT
	.type	OSSL_STORE_INFO_get1_CERT, @function
OSSL_STORE_INFO_get1_CERT:
.LFB840:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	cmpl	$4, (%rdi)
	je	.L142
	movl	$416, %r8d
	movl	$100, %edx
	movl	$101, %esi
	movl	$44, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore_state
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	call	X509_up_ref@PLT
	movq	8(%rbx), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE840:
	.size	OSSL_STORE_INFO_get1_CERT, .-OSSL_STORE_INFO_get1_CERT
	.p2align 4
	.globl	OSSL_STORE_INFO_get0_CRL
	.type	OSSL_STORE_INFO_get0_CRL, @function
OSSL_STORE_INFO_get0_CRL:
.LFB841:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$5, (%rdi)
	je	.L146
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE841:
	.size	OSSL_STORE_INFO_get0_CRL, .-OSSL_STORE_INFO_get0_CRL
	.p2align 4
	.globl	OSSL_STORE_INFO_get1_CRL
	.type	OSSL_STORE_INFO_get1_CRL, @function
OSSL_STORE_INFO_get1_CRL:
.LFB842:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	cmpl	$5, (%rdi)
	je	.L151
	movl	$434, %r8d
	movl	$101, %edx
	movl	$102, %esi
	movl	$44, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_restore_state
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	call	X509_CRL_up_ref@PLT
	movq	8(%rbx), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE842:
	.size	OSSL_STORE_INFO_get1_CRL, .-OSSL_STORE_INFO_get1_CRL
	.p2align 4
	.globl	OSSL_STORE_INFO_free
	.type	OSSL_STORE_INFO_free, @function
OSSL_STORE_INFO_free:
.LFB843:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L152
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movl	(%rdi), %eax
	addl	$1, %eax
	cmpl	$6, %eax
	ja	.L154
	leaq	.L156(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L156:
	.long	.L161-.L156
	.long	.L154-.L156
	.long	.L160-.L156
	.long	.L158-.L156
	.long	.L158-.L156
	.long	.L157-.L156
	.long	.L155-.L156
	.text
	.p2align 4,,10
	.p2align 3
.L155:
	movq	8(%rdi), %rdi
	call	X509_CRL_free@PLT
.L154:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$467, %edx
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	leaq	.LC1(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	EVP_PKEY_free@PLT
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	ret
	.p2align 4,,10
	.p2align 3
.L157:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	8(%rdi), %rdi
	call	X509_free@PLT
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L161:
	movq	8(%rdi), %rdi
	call	BUF_MEM_free@PLT
	movq	16(%r12), %rdi
	movl	$448, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L160:
	movq	8(%rdi), %rdi
	movl	$451, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	16(%r12), %rdi
	movl	$452, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L154
	.cfi_endproc
.LFE843:
	.size	OSSL_STORE_INFO_free, .-OSSL_STORE_INFO_free
	.p2align 4
	.globl	OSSL_STORE_load
	.type	OSSL_STORE_load, @function
OSSL_STORE_load:
.LFB819:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	$1, 52(%rdi)
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L191:
	testq	%rdi, %rdi
	je	.L167
	movq	40(%rbx), %rsi
	call	*%rax
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L166
.L170:
	movl	48(%rbx), %eax
	testl	%eax, %eax
	je	.L165
	movl	(%rdi), %edx
	cmpl	%edx, %eax
	je	.L165
	cmpl	$1, %edx
	jbe	.L165
	call	OSSL_STORE_INFO_free
.L166:
	movq	(%rbx), %rax
	movq	8(%rbx), %rdi
	call	*56(%rax)
	testl	%eax, %eax
	jne	.L167
	movq	(%rbx), %rax
	movq	8(%rbx), %rdi
	movq	24(%rbx), %rdx
	movq	16(%rbx), %rsi
	call	*48(%rax)
	movq	%rax, %rdi
	movq	32(%rbx), %rax
	testq	%rax, %rax
	jne	.L191
	testq	%rdi, %rdi
	jne	.L170
	.p2align 4,,10
	.p2align 3
.L167:
	xorl	%edi, %edi
.L165:
	addq	$8, %rsp
	movq	%rdi, %rax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE819:
	.size	OSSL_STORE_load, .-OSSL_STORE_load
	.p2align 4
	.globl	OSSL_STORE_supports_search
	.type	OSSL_STORE_supports_search, @function
OSSL_STORE_supports_search:
.LFB844:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L195
	movl	%esi, -64(%rbp)
	xorl	%edi, %edi
	leaq	-64(%rbp), %rsi
	call	*%rax
.L192:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L197
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L195:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L192
.L197:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE844:
	.size	OSSL_STORE_supports_search, .-OSSL_STORE_supports_search
	.p2align 4
	.globl	OSSL_STORE_SEARCH_by_name
	.type	OSSL_STORE_SEARCH_by_name, @function
OSSL_STORE_SEARCH_by_name:
.LFB845:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$484, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$48, %edi
	subq	$24, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L202
	movl	$1, (%rax)
	movq	%rbx, 8(%rax)
.L198:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L202:
	.cfi_restore_state
	movl	$487, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$65, %edx
	movl	$137, %esi
	movl	$44, %edi
	movq	%rax, -24(%rbp)
	call	ERR_put_error@PLT
	movq	-24(%rbp), %rax
	jmp	.L198
	.cfi_endproc
.LFE845:
	.size	OSSL_STORE_SEARCH_by_name, .-OSSL_STORE_SEARCH_by_name
	.p2align 4
	.globl	OSSL_STORE_SEARCH_by_issuer_serial
	.type	OSSL_STORE_SEARCH_by_issuer_serial, @function
OSSL_STORE_SEARCH_by_issuer_serial:
.LFB846:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$500, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movl	$48, %edi
	movq	%rsi, -16(%rbp)
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L207
	movq	-8(%rbp), %xmm0
	movl	$2, (%rax)
	movhps	-16(%rbp), %xmm0
	movups	%xmm0, 8(%rax)
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L207:
	.cfi_restore_state
	movl	$503, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$65, %edx
	movl	$133, %esi
	movl	$44, %edi
	movq	%rax, -8(%rbp)
	call	ERR_put_error@PLT
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE846:
	.size	OSSL_STORE_SEARCH_by_issuer_serial, .-OSSL_STORE_SEARCH_by_issuer_serial
	.section	.rodata.str1.1
.LC3:
	.string	"%d"
.LC4:
	.string	"%zu"
.LC5:
	.string	", fingerprint size is "
.LC6:
	.string	" size is "
	.text
	.p2align 4
	.globl	OSSL_STORE_SEARCH_by_key_fingerprint
	.type	OSSL_STORE_SEARCH_by_key_fingerprint, @function
OSSL_STORE_SEARCH_by_key_fingerprint:
.LFB847:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movl	$518, %edx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$48, %edi
	subq	$88, %rsp
	movq	%rsi, -120(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L216
	testq	%rbx, %rbx
	je	.L212
	movq	%rbx, %rdi
	call	EVP_MD_size@PLT
	cltq
	cmpq	%r13, %rax
	jne	.L217
.L212:
	movq	%rbx, %xmm0
	movl	$3, (%r12)
	movq	%r13, 40(%r12)
	movhps	-120(%rbp), %xmm0
	movups	%xmm0, 24(%r12)
.L208:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L218
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L217:
	.cfi_restore_state
	movq	%rbx, %rdi
	leaq	-112(%rbp), %r14
	leaq	-80(%rbp), %r15
	call	EVP_MD_size@PLT
	movq	%r14, %rdi
	movl	$20, %esi
	leaq	.LC3(%rip), %rdx
	movl	%eax, %ecx
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	movq	%r13, %rcx
	movq	%r15, %rdi
	movl	$20, %esi
	leaq	.LC4(%rip), %rdx
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	movl	$531, %r8d
	movl	$121, %edx
	leaq	.LC1(%rip), %rcx
	movl	$136, %esi
	movl	$44, %edi
	call	ERR_put_error@PLT
	movq	%rbx, %rdi
	call	EVP_MD_type@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movq	%r15, %r9
	movq	%r14, %rcx
	movl	$5, %edi
	movq	%rax, %rsi
	leaq	.LC5(%rip), %r8
	leaq	.LC6(%rip), %rdx
	xorl	%eax, %eax
	call	ERR_add_error_data@PLT
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L216:
	movl	$521, %r8d
	movl	$65, %edx
	movl	$136, %esi
	movl	$44, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L208
.L218:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE847:
	.size	OSSL_STORE_SEARCH_by_key_fingerprint, .-OSSL_STORE_SEARCH_by_key_fingerprint
	.p2align 4
	.globl	OSSL_STORE_SEARCH_by_alias
	.type	OSSL_STORE_SEARCH_by_alias, @function
OSSL_STORE_SEARCH_by_alias:
.LFB848:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$546, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$48, %edi
	pushq	%r12
	.cfi_offset 12, -32
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L223
	movl	$4, (%rax)
	movq	%r13, %rdi
	movq	%r13, 32(%rax)
	call	strlen@PLT
	movq	%rax, 40(%r12)
.L219:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	.cfi_restore_state
	movl	$549, %r8d
	movl	$65, %edx
	movl	$132, %esi
	movl	$44, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L219
	.cfi_endproc
.LFE848:
	.size	OSSL_STORE_SEARCH_by_alias, .-OSSL_STORE_SEARCH_by_alias
	.p2align 4
	.globl	OSSL_STORE_SEARCH_free
	.type	OSSL_STORE_SEARCH_free, @function
OSSL_STORE_SEARCH_free:
.LFB849:
	.cfi_startproc
	endbr64
	movl	$563, %edx
	leaq	.LC1(%rip), %rsi
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE849:
	.size	OSSL_STORE_SEARCH_free, .-OSSL_STORE_SEARCH_free
	.p2align 4
	.globl	OSSL_STORE_SEARCH_get_type
	.type	OSSL_STORE_SEARCH_get_type, @function
OSSL_STORE_SEARCH_get_type:
.LFB850:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	ret
	.cfi_endproc
.LFE850:
	.size	OSSL_STORE_SEARCH_get_type, .-OSSL_STORE_SEARCH_get_type
	.p2align 4
	.globl	OSSL_STORE_SEARCH_get0_name
	.type	OSSL_STORE_SEARCH_get0_name, @function
OSSL_STORE_SEARCH_get0_name:
.LFB851:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE851:
	.size	OSSL_STORE_SEARCH_get0_name, .-OSSL_STORE_SEARCH_get0_name
	.p2align 4
	.globl	OSSL_STORE_SEARCH_get0_serial
	.type	OSSL_STORE_SEARCH_get0_serial, @function
OSSL_STORE_SEARCH_get0_serial:
.LFB852:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE852:
	.size	OSSL_STORE_SEARCH_get0_serial, .-OSSL_STORE_SEARCH_get0_serial
	.p2align 4
	.globl	OSSL_STORE_SEARCH_get0_bytes
	.type	OSSL_STORE_SEARCH_get0_bytes, @function
OSSL_STORE_SEARCH_get0_bytes:
.LFB853:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movq	%rax, (%rsi)
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE853:
	.size	OSSL_STORE_SEARCH_get0_bytes, .-OSSL_STORE_SEARCH_get0_bytes
	.p2align 4
	.globl	OSSL_STORE_SEARCH_get0_string
	.type	OSSL_STORE_SEARCH_get0_string, @function
OSSL_STORE_SEARCH_get0_string:
.LFB854:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE854:
	.size	OSSL_STORE_SEARCH_get0_string, .-OSSL_STORE_SEARCH_get0_string
	.p2align 4
	.globl	OSSL_STORE_SEARCH_get0_digest
	.type	OSSL_STORE_SEARCH_get0_digest, @function
OSSL_STORE_SEARCH_get0_digest:
.LFB855:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE855:
	.size	OSSL_STORE_SEARCH_get0_digest, .-OSSL_STORE_SEARCH_get0_digest
	.p2align 4
	.globl	ossl_store_info_new_EMBEDDED
	.type	ossl_store_info_new_EMBEDDED, @function
ossl_store_info_new_EMBEDDED:
.LFB856:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$236, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$24, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	leaq	.LC1(%rip), %rsi
	subq	$8, %rsp
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L236
	movl	$-1, (%rax)
	movq	%rbx, 8(%rax)
	testq	%r13, %r13
	je	.L237
	movl	$614, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_strdup@PLT
	movq	%rax, 16(%r12)
	testq	%rax, %rax
	je	.L238
.L231:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L237:
	.cfi_restore_state
	movq	$0, 16(%rax)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L236:
	.cfi_restore_state
	movl	$607, %r8d
	movl	$65, %edx
	movl	$123, %esi
	movl	$44, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L238:
	movl	$617, %r8d
	movl	$65, %edx
	movl	$123, %esi
	movl	$44, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	OSSL_STORE_INFO_free
	jmp	.L231
	.cfi_endproc
.LFE856:
	.size	ossl_store_info_new_EMBEDDED, .-ossl_store_info_new_EMBEDDED
	.p2align 4
	.globl	ossl_store_info_get0_EMBEDDED_buffer
	.type	ossl_store_info_get0_EMBEDDED_buffer, @function
ossl_store_info_get0_EMBEDDED_buffer:
.LFB857:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$-1, (%rdi)
	je	.L242
	ret
	.p2align 4,,10
	.p2align 3
.L242:
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE857:
	.size	ossl_store_info_get0_EMBEDDED_buffer, .-ossl_store_info_get0_EMBEDDED_buffer
	.p2align 4
	.globl	ossl_store_info_get0_EMBEDDED_pem_name
	.type	ossl_store_info_get0_EMBEDDED_pem_name, @function
ossl_store_info_get0_EMBEDDED_pem_name:
.LFB858:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$-1, (%rdi)
	je	.L246
	ret
	.p2align 4,,10
	.p2align 3
.L246:
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE858:
	.size	ossl_store_info_get0_EMBEDDED_pem_name, .-ossl_store_info_get0_EMBEDDED_pem_name
	.p2align 4
	.globl	ossl_store_attach_pem_bio
	.type	ossl_store_attach_pem_bio, @function
ossl_store_attach_pem_bio:
.LFB859:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	.LC0(%rip), %rdi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	%rsi, -40(%rbp)
	movq	%rdx, -48(%rbp)
	call	ossl_store_get0_loader_int@PLT
	testq	%rax, %rax
	je	.L250
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	ossl_store_file_attach_pem_bio_int@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L250
	movl	$650, %edx
	leaq	.LC1(%rip), %rsi
	movl	$56, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L256
	movq	%rbx, %xmm0
	movq	%r13, %xmm1
	movq	$0, 32(%rax)
	punpcklqdq	%xmm1, %xmm0
	movq	$0, 40(%rax)
	movups	%xmm0, (%rax)
	movq	-40(%rbp), %xmm0
	movhps	-48(%rbp), %xmm0
	movups	%xmm0, 16(%rax)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L250:
	.cfi_restore_state
	addq	$24, %rsp
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L256:
	.cfi_restore_state
	movl	$651, %r8d
	movl	$65, %edx
	movl	$127, %esi
	movl	$44, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r13, %rdi
	call	*72(%rbx)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE859:
	.size	ossl_store_attach_pem_bio, .-ossl_store_attach_pem_bio
	.p2align 4
	.globl	ossl_store_detach_pem_bio
	.type	ossl_store_detach_pem_bio, @function
ossl_store_detach_pem_bio:
.LFB860:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	8(%rdi), %rdi
	call	ossl_store_file_detach_pem_bio_int@PLT
	movq	%r12, %rdi
	movl	$679, %edx
	leaq	.LC1(%rip), %rsi
	movl	%eax, %r13d
	call	CRYPTO_free@PLT
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE860:
	.size	ossl_store_detach_pem_bio, .-ossl_store_detach_pem_bio
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
