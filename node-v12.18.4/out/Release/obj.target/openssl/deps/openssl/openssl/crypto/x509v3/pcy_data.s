	.file	"pcy_data.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/x509v3/pcy_data.c"
	.text
	.p2align 4
	.globl	policy_data_free
	.type	policy_data_free, @function
policy_data_free:
.LFB1320:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	call	ASN1_OBJECT_free@PLT
	testb	$4, (%r12)
	je	.L8
.L3:
	movq	24(%r12), %rdi
	movq	ASN1_OBJECT_free@GOTPCREL(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$27, %edx
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movq	16(%r12), %rdi
	movq	POLICYQUALINFO_free@GOTPCREL(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L1:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE1320:
	.size	policy_data_free, .-policy_data_free
	.p2align 4
	.globl	policy_data_new
	.type	policy_data_new, @function
policy_data_new:
.LFB1321:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	orq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	je	.L29
	movq	%rdi, %r14
	movq	%rsi, %r12
	movl	%edx, %ebx
	testq	%rsi, %rsi
	je	.L12
	movq	%rsi, %rdi
	call	OBJ_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L29
.L12:
	movl	$52, %edx
	leaq	.LC0(%rip), %rsi
	movl	$32, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L30
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 24(%r13)
	testq	%rax, %rax
	je	.L31
	testl	%ebx, %ebx
	je	.L16
	movl	$16, 0(%r13)
.L16:
	testq	%r12, %r12
	je	.L17
	movq	%r12, 8(%r13)
	testq	%r14, %r14
	je	.L9
.L18:
	movq	8(%r14), %rax
	movq	%rax, 16(%r13)
	movq	$0, 8(%r14)
.L9:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movq	(%r14), %rax
	movq	%rax, 8(%r13)
	movq	$0, (%r14)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L31:
	movl	$59, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	call	ASN1_OBJECT_free@PLT
	movl	$61, %r8d
	movl	$65, %edx
	leaq	.LC0(%rip), %rcx
	movl	$171, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
.L29:
	xorl	%r13d, %r13d
	popq	%rbx
	popq	%r12
	movq	%r13, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movl	$54, %r8d
	movl	$65, %edx
	movl	$171, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L9
	.cfi_endproc
.LFE1321:
	.size	policy_data_new, .-policy_data_new
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
