	.file	"tasn_dec.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/asn1/tasn_dec.c"
	.text
	.p2align 4
	.type	asn1_check_tlen, @function
asn1_check_tlen:
.LFB466:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r10
	movq	%r8, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movl	40(%rbp), %eax
	movq	(%r9), %r9
	movq	48(%rbp), %rbx
	movl	%eax, -84(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r9, -64(%rbp)
	testq	%rbx, %rbx
	je	.L2
	cmpb	$0, (%rbx)
	je	.L3
	movq	8(%rbx), %rax
	movl	4(%rbx), %edx
	movq	%rax, -72(%rbp)
	movl	20(%rbx), %eax
	movl	%eax, -76(%rbp)
	movl	16(%rbx), %eax
	movl	%eax, -80(%rbp)
	movslq	24(%rbx), %rax
	addq	%r9, %rax
	movq	%rax, -64(%rbp)
.L4:
	testb	$-128, %dl
	jne	.L48
.L17:
	movl	24(%rbp), %eax
	testl	%eax, %eax
	js	.L6
	movl	24(%rbp), %eax
	cmpl	%eax, -80(%rbp)
	je	.L49
.L7:
	cmpb	$0, -84(%rbp)
	movl	$-1, %eax
	jne	.L1
	testq	%rbx, %rbx
	je	.L9
	movb	$0, (%rbx)
.L9:
	movl	$1130, %r8d
	movl	$168, %edx
	movl	$104, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L50
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	movq	%r8, -112(%rbp)
	movq	16(%rbp), %r8
	leaq	-80(%rbp), %rdx
	leaq	-72(%rbp), %rsi
	movq	%rcx, -104(%rbp)
	leaq	-64(%rbp), %rdi
	leaq	-76(%rbp), %rcx
	movq	%r9, -96(%rbp)
	call	ASN1_get_object@PLT
	movq	-64(%rbp), %rcx
	movq	-96(%rbp), %r9
	movb	$1, (%rbx)
	movl	%eax, %edx
	movq	-72(%rbp), %rsi
	movq	-104(%rbp), %r10
	movl	%eax, 4(%rbx)
	movl	-76(%rbp), %eax
	subq	%r9, %rcx
	movq	-112(%rbp), %r11
	movq	%rsi, 8(%rbx)
	movl	%eax, 20(%rbx)
	movl	-80(%rbp), %eax
	movl	%ecx, 24(%rbx)
	movl	%eax, 16(%rbx)
	movl	%edx, %eax
	andl	$129, %eax
	jne	.L4
	movslq	%ecx, %rcx
	addq	%rsi, %rcx
	cmpq	16(%rbp), %rcx
	jle	.L4
	movl	$1110, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$155, %edx
	movl	%eax, -84(%rbp)
	movl	$104, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
	movb	$0, (%rbx)
	movl	-84(%rbp), %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L49:
	movl	32(%rbp), %eax
	cmpl	%eax, -76(%rbp)
	jne	.L7
	testq	%rbx, %rbx
	je	.L6
	movb	$0, (%rbx)
	.p2align 4,,10
	.p2align 3
.L6:
	movq	-64(%rbp), %rcx
	testb	$1, %dl
	jne	.L51
.L10:
	testq	%r10, %r10
	je	.L11
	movl	%edx, %eax
	andl	$1, %eax
	movb	%al, (%r10)
.L11:
	testq	%r11, %r11
	je	.L12
	andl	$32, %edx
	movb	%dl, (%r11)
.L12:
	testq	%r13, %r13
	je	.L13
	movq	-72(%rbp), %rax
	movq	%rax, 0(%r13)
.L13:
	testq	%r15, %r15
	je	.L14
	movl	-76(%rbp), %eax
	movb	%al, (%r15)
.L14:
	testq	%r14, %r14
	je	.L15
	movl	-80(%rbp), %eax
	movl	%eax, (%r14)
.L15:
	movq	%rcx, (%r12)
	movl	$1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L51:
	movq	16(%rbp), %rdi
	movq	%rcx, %rax
	subq	%r9, %rax
	subq	%rax, %rdi
	movq	%rdi, -72(%rbp)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L2:
	movq	%r8, -112(%rbp)
	movq	16(%rbp), %r8
	leaq	-80(%rbp), %rdx
	leaq	-72(%rbp), %rsi
	movq	%rcx, -104(%rbp)
	leaq	-64(%rbp), %rdi
	leaq	-76(%rbp), %rcx
	movq	%r9, -96(%rbp)
	call	ASN1_get_object@PLT
	movq	-96(%rbp), %r9
	movq	-104(%rbp), %r10
	testb	$-128, %al
	movq	-112(%rbp), %r11
	movl	%eax, %edx
	je	.L17
	movl	$1118, %r8d
	movl	$102, %edx
	movl	$104, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L48:
	movl	$1118, %r8d
	movl	$102, %edx
	movl	$104, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movb	$0, (%rbx)
	xorl	%eax, %eax
	jmp	.L1
.L50:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE466:
	.size	asn1_check_tlen, .-asn1_check_tlen
	.p2align 4
	.type	asn1_collect.constprop.0, @function
asn1_collect.constprop.0:
.LFB467:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -112(%rbp)
	movq	(%rsi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%ecx, %eax
	movq	%r15, -80(%rbp)
	andl	$1, %eax
	movb	%al, -100(%rbp)
	testq	%rdx, %rdx
	jle	.L66
	movq	%rdx, %r13
	movq	%rdi, %r12
	leaq	-84(%rbp), %r14
	movl	%r8d, %ebx
	cmpq	$1, %r13
	je	.L54
.L84:
	cmpb	$0, (%r15)
	jne	.L54
	cmpb	$0, 1(%r15)
	je	.L81
.L54:
	leaq	-88(%rbp), %rdx
	leaq	-72(%rbp), %rsi
	movq	%r13, %r8
	movq	%r14, %rcx
	leaq	-64(%rbp), %rdi
	movq	%r15, -64(%rbp)
	call	ASN1_get_object@PLT
	movl	%eax, %r11d
	andl	$128, %r11d
	jne	.L82
	movq	-64(%rbp), %rcx
	testb	$1, %al
	jne	.L58
	movq	-72(%rbp), %rdx
.L59:
	movq	%rcx, -80(%rbp)
	testb	$32, %al
	je	.L60
	cmpl	$5, %ebx
	je	.L83
	andl	$1, %eax
	leaq	-80(%rbp), %rsi
	leal	1(%rbx), %r8d
	movq	%r12, %rdi
	movl	%eax, %ecx
	call	asn1_collect.constprop.0
	movl	%eax, %r11d
	testl	%eax, %eax
	je	.L52
	movq	-80(%rbp), %rcx
.L62:
	movq	%rcx, %rax
	subq	%r15, %rax
	subq	%rax, %r13
	testq	%r13, %r13
	jle	.L53
	movq	%rcx, %r15
	cmpq	$1, %r13
	jne	.L84
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L60:
	testq	%rdx, %rdx
	je	.L62
	movslq	(%r12), %rcx
	movq	%r12, %rdi
	movl	%r11d, -104(%rbp)
	movq	%rdx, -128(%rbp)
	leaq	(%rcx,%rdx), %rsi
	movq	%rcx, -120(%rbp)
	call	BUF_MEM_grow_clean@PLT
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %rdx
	testq	%rax, %rax
	movl	-104(%rbp), %r11d
	je	.L85
	movq	8(%r12), %rdi
	movq	-80(%rbp), %rsi
	movq	%rdx, -120(%rbp)
	addq	%rcx, %rdi
	call	memcpy@PLT
	movq	-80(%rbp), %rcx
	movq	-120(%rbp), %rdx
	addq	%rdx, %rcx
	movq	%rcx, -80(%rbp)
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L81:
	leaq	2(%r15), %rcx
	cmpb	$0, -100(%rbp)
	movq	%rcx, -80(%rbp)
	je	.L86
.L55:
	movq	-112(%rbp), %rax
	movl	$1, %r11d
	movq	%rcx, (%rax)
.L52:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L87
	addq	$88, %rsp
	movl	%r11d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	movq	%rcx, %rdx
	movq	%r13, %rdi
	subq	%r15, %rdx
	subq	%rdx, %rdi
	movq	%rdi, %rdx
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L82:
	movl	$1118, %r8d
	movl	$102, %edx
	movl	$104, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$1017, %r8d
	movl	$58, %edx
	leaq	.LC0(%rip), %rcx
	movl	$106, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
	xorl	%r11d, %r11d
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L85:
	movl	$1047, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$140, %esi
	movl	$13, %edi
	movl	%r11d, -100(%rbp)
	call	ERR_put_error@PLT
	movl	-100(%rbp), %r11d
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L66:
	movq	%r15, %rcx
.L53:
	cmpb	$0, -100(%rbp)
	je	.L55
	movl	$1034, %r8d
	movl	$137, %edx
	movl	$106, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r11d, %r11d
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L86:
	movl	$1008, %r8d
	movl	$159, %edx
	movl	$106, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r11d, %r11d
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L83:
	movl	$1024, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$197, %edx
	movl	$106, %esi
	movl	$13, %edi
	movl	%r11d, -100(%rbp)
	call	ERR_put_error@PLT
	movl	-100(%rbp), %r11d
	jmp	.L52
.L87:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE467:
	.size	asn1_collect.constprop.0, .-asn1_collect.constprop.0
	.p2align 4
	.type	asn1_d2i_ex_primitive, @function
asn1_d2i_ex_primitive:
.LFB460:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	16(%rbp), %r11d
	movq	24(%rbp), %r12
	movq	%rsi, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$0, -133(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movaps	%xmm0, -80(%rbp)
	testq	%rdi, %rdi
	je	.L220
	cmpb	$5, (%rcx)
	movq	%rdi, %r15
	movq	%rdx, %rbx
	movq	%rcx, %r14
	movl	%r8d, %r13d
	movl	%r9d, %r10d
	je	.L221
	movq	8(%rcx), %rax
	movl	%eax, -132(%rbp)
	cmpl	$-4, %eax
	je	.L222
.L94:
	cmpl	$-1, %r13d
	je	.L223
.L99:
	movq	-152(%rbp), %rax
	movq	(%rax), %r9
	movq	%r9, -120(%rbp)
	movq	%r9, -104(%rbp)
	testq	%r12, %r12
	je	.L100
	cmpb	$0, (%r12)
	je	.L101
	movq	8(%r12), %rdx
	movl	4(%r12), %eax
	movq	%rdx, -112(%rbp)
	movl	20(%r12), %edx
	movl	%edx, -124(%rbp)
	movl	16(%r12), %edx
	movl	%edx, -128(%rbp)
	movslq	24(%r12), %rdx
	addq	%r9, %rdx
	movq	%rdx, -104(%rbp)
.L102:
	testb	$-128, %al
	jne	.L224
.L165:
	testl	%r13d, %r13d
	js	.L104
	cmpl	-128(%rbp), %r13d
	je	.L225
.L105:
	movl	$-1, %eax
	testb	%r11b, %r11b
	jne	.L88
	testq	%r12, %r12
	je	.L108
	movb	$0, (%r12)
.L108:
	movl	$1130, %r8d
	movl	$168, %edx
	movl	$104, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L103:
	movl	$694, %r8d
	movl	$58, %edx
	movl	$108, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L88:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L226
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	leaq	-124(%rbp), %rcx
	leaq	-128(%rbp), %rdx
	movq	%rbx, %r8
	movl	%r11d, -172(%rbp)
	leaq	-112(%rbp), %rsi
	leaq	-104(%rbp), %rdi
	movl	%r10d, -168(%rbp)
	movq	%r9, -160(%rbp)
	call	ASN1_get_object@PLT
	movl	-124(%rbp), %edx
	movq	-112(%rbp), %rcx
	movb	$1, (%r12)
	movq	-160(%rbp), %r9
	movl	%eax, 4(%r12)
	movl	%edx, 20(%r12)
	movl	-128(%rbp), %edx
	movq	%rcx, 8(%r12)
	movl	-168(%rbp), %r10d
	movl	%edx, 16(%r12)
	movq	-104(%rbp), %rdx
	movl	-172(%rbp), %r11d
	subq	%r9, %rdx
	testb	$-127, %al
	movl	%edx, 24(%r12)
	jne	.L102
	movslq	%edx, %rdx
	addq	%rcx, %rdx
	cmpq	%rdx, %rbx
	jge	.L102
	movl	$1110, %r8d
	movl	$155, %edx
	movl	$104, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movb	$0, (%r12)
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L222:
	testl	%r8d, %r8d
	js	.L92
	movl	$667, %r8d
	movl	$127, %edx
	movl	$108, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L229:
	movl	$-1, %r13d
.L92:
	testb	%r11b, %r11b
	jne	.L227
	movq	-152(%rbp), %rax
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	leaq	-104(%rbp), %rdx
	leaq	-120(%rbp), %r9
	movl	%r11d, -168(%rbp)
	movq	(%rax), %rax
	pushq	%r12
	leaq	-132(%rbp), %rsi
	pushq	$0
	pushq	$0
	pushq	$-1
	pushq	%rbx
	movl	%r10d, -160(%rbp)
	movq	%rax, -120(%rbp)
	call	asn1_check_tlen
	addq	$48, %rsp
	movl	-160(%rbp), %r10d
	movl	-168(%rbp), %r11d
	testl	%eax, %eax
	je	.L228
	cmpb	$0, -104(%rbp)
	je	.L94
	movl	$-3, -132(%rbp)
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L221:
	movl	%r8d, -132(%rbp)
	cmpl	$-4, %r8d
	je	.L229
.L93:
	xorl	%r10d, %r10d
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L225:
	cmpl	-124(%rbp), %r10d
	jne	.L105
	testq	%r12, %r12
	je	.L104
	movb	$0, (%r12)
	.p2align 4,,10
	.p2align 3
.L104:
	movq	-104(%rbp), %r13
	testb	$1, %al
	jne	.L109
	movq	-112(%rbp), %rbx
.L110:
	movl	-132(%rbp), %ecx
	movl	%eax, %esi
	movq	%r13, -120(%rbp)
	andl	$32, %eax
	andl	$1, %esi
	leal	-16(%rcx), %edx
	cmpl	$1, %edx
	jbe	.L111
	cmpl	$-3, %ecx
	je	.L215
	testb	%al, %al
	je	.L129
	movl	%ecx, %eax
	andl	$-5, %eax
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L171
	cmpl	$10, %ecx
	jne	.L130
.L171:
	movl	$730, %r8d
	movl	$195, %edx
	movl	$108, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L109:
	movq	%r13, %rdx
	subq	%r9, %rdx
	subq	%rdx, %rbx
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L227:
	movl	$671, %r8d
	movl	$126, %edx
	movl	$108, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L100:
	leaq	-124(%rbp), %rcx
	leaq	-128(%rbp), %rdx
	movq	%rbx, %r8
	movl	%r11d, -172(%rbp)
	leaq	-112(%rbp), %rsi
	leaq	-104(%rbp), %rdi
	movl	%r10d, -168(%rbp)
	movq	%r9, -160(%rbp)
	call	ASN1_get_object@PLT
	movq	-160(%rbp), %r9
	movl	-168(%rbp), %r10d
	testb	$-128, %al
	movl	-172(%rbp), %r11d
	je	.L165
	movl	$1118, %r8d
	movl	$102, %edx
	movl	$104, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L129:
	leaq	0(%r13,%rbx), %rax
	movq	%rbx, %rdx
	movq	%r13, %r12
	movq	%rax, -120(%rbp)
.L128:
	movq	32(%r14), %rax
	movq	%r12, -104(%rbp)
	movl	%edx, %r13d
	testq	%rax, %rax
	je	.L134
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L134
	movq	%r14, %r9
	leaq	-133(%rbp), %r8
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L218
.L136:
	movq	-120(%rbp), %rax
	movq	-152(%rbp), %rbx
	movq	%rax, (%rbx)
	movl	$1, %eax
.L121:
	cmpb	$0, -133(%rbp)
	je	.L88
	movq	-88(%rbp), %rdi
	movl	$768, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, -152(%rbp)
	call	CRYPTO_free@PLT
	movl	-152(%rbp), %eax
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L134:
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	cmpq	$-4, 8(%r14)
	je	.L230
.L137:
	cmpl	$10, %ecx
	ja	.L141
	leaq	.L143(%rip), %rsi
	movl	%ecx, %edx
	movslq	(%rsi,%rdx,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L143:
	.long	.L141-.L143
	.long	.L147-.L143
	.long	.L142-.L143
	.long	.L146-.L143
	.long	.L141-.L143
	.long	.L145-.L143
	.long	.L144-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L142-.L143
	.text
	.p2align 4,,10
	.p2align 3
.L111:
	cmpl	$-3, %ecx
	je	.L215
	testb	%al, %al
	je	.L231
.L114:
	movq	-152(%rbp), %rax
	movq	(%rax), %r12
	testb	%sil, %sil
	jne	.L232
	movq	%r13, %rdx
	subq	%r12, %rdx
	addq	%rbx, %rdx
	addq	%r13, %rbx
	movq	%rbx, -120(%rbp)
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L146:
	movslq	%r13d, %rdx
	leaq	-104(%rbp), %rsi
	movq	%r15, %rdi
	call	c2i_ASN1_BIT_STRING@PLT
	testq	%rax, %rax
	jne	.L136
	.p2align 4,,10
	.p2align 3
.L151:
	movq	%r12, %rdi
	call	ASN1_TYPE_free@PLT
	testq	%rbx, %rbx
	je	.L218
	movq	$0, (%rbx)
.L218:
	xorl	%eax, %eax
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L220:
	movl	$653, %r8d
	movl	$125, %edx
	movl	$108, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L224:
	movl	$1118, %r8d
	movl	$102, %edx
	movl	$104, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movb	$0, (%r12)
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L215:
	testq	%r12, %r12
	je	.L114
	movb	$0, (%r12)
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L232:
	testq	%rbx, %rbx
	jle	.L116
	movl	$1, -160(%rbp)
	leaq	-124(%rbp), %rax
	movq	%rax, -168(%rbp)
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L236:
	cmpl	$-1, -160(%rbp)
	je	.L233
	addl	$1, -160(%rbp)
.L126:
	movq	%rcx, %rax
	subq	%r13, %rax
	movq	%rcx, %r13
	subq	%rax, %rbx
.L119:
	testq	%rbx, %rbx
	jle	.L116
.L127:
	cmpq	$1, %rbx
	je	.L117
	cmpb	$0, 0(%r13)
	jne	.L117
	cmpb	$0, 1(%r13)
	je	.L234
.L117:
	leaq	-128(%rbp), %rdx
	leaq	-112(%rbp), %rsi
	movq	%rbx, %r8
	movq	%r13, -104(%rbp)
	movq	-168(%rbp), %rcx
	leaq	-104(%rbp), %rdi
	call	ASN1_get_object@PLT
	movl	%eax, %edx
	andl	$128, %eax
	jne	.L235
	movq	-104(%rbp), %rcx
	testb	$1, %dl
	jne	.L122
	movq	-112(%rbp), %rsi
.L123:
	andl	$1, %edx
	jne	.L236
	addq	%rsi, %rcx
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L122:
	movq	%rcx, %rsi
	movq	%rbx, %rdi
	subq	%r13, %rsi
	subq	%rsi, %rdi
	movq	%rdi, %rsi
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L230:
	movq	(%r15), %r12
	testq	%r12, %r12
	je	.L237
.L138:
	cmpl	%ecx, (%r12)
	jne	.L238
.L140:
	movq	%r15, %rbx
	leaq	8(%r12), %r15
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L235:
	movl	$1118, %r8d
	movl	$102, %edx
	movl	$104, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$946, %r8d
	movl	$58, %edx
	leaq	.LC0(%rip), %rcx
	movl	$190, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L223:
	movl	-132(%rbp), %r13d
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L142:
	movslq	%r13d, %rdx
	leaq	-104(%rbp), %rsi
	movq	%r15, %rdi
	movl	%ecx, -160(%rbp)
	call	c2i_ASN1_INTEGER@PLT
	testq	%rax, %rax
	je	.L151
	movq	(%r15), %rdx
	movl	-160(%rbp), %ecx
	movl	4(%rdx), %eax
	andl	$256, %eax
	orl	%ecx, %eax
	movl	%eax, 4(%rdx)
.L152:
	cmpl	$5, %ecx
	jne	.L136
	testq	%r12, %r12
	je	.L136
	movq	$0, 8(%r12)
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L141:
	cmpl	$30, %ecx
	je	.L239
	cmpl	$28, %ecx
	jne	.L156
	testb	$3, %r13b
	jne	.L240
.L156:
	movq	(%r15), %r14
	testq	%r14, %r14
	je	.L241
	movl	%ecx, 4(%r14)
.L159:
	cmpb	$0, -133(%rbp)
	movl	%ecx, -160(%rbp)
	je	.L160
	movq	8(%r14), %rdi
	movl	$882, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-104(%rbp), %rax
	movl	-160(%rbp), %ecx
	movl	%r13d, (%r14)
	movb	$0, -133(%rbp)
	movq	%rax, 8(%r14)
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L130:
	leaq	-96(%rbp), %r12
	movsbl	%sil, %ecx
	xorl	%r8d, %r8d
	movq	%rbx, %rdx
	leaq	-120(%rbp), %rsi
	movq	%r12, %rdi
	movb	$1, -133(%rbp)
	call	asn1_collect.constprop.0
	testl	%eax, %eax
	je	.L218
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	leaq	1(%rdx), %rsi
	movq	%rdx, -160(%rbp)
	call	BUF_MEM_grow_clean@PLT
	movq	-160(%rbp), %rdx
	testq	%rax, %rax
	je	.L242
	movq	-88(%rbp), %rax
	movb	$0, (%rax,%rdx)
	movq	-88(%rbp), %r12
	movl	-132(%rbp), %ecx
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L238:
	movl	%ecx, %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	%ecx, -160(%rbp)
	call	ASN1_TYPE_set@PLT
	movl	-160(%rbp), %ecx
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L147:
	cmpl	$1, %r13d
	jne	.L243
	movq	-104(%rbp), %rax
	movzbl	(%rax), %eax
	movl	%eax, (%r15)
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L144:
	movslq	%r13d, %rdx
	leaq	-104(%rbp), %rsi
	movq	%r15, %rdi
	call	c2i_ASN1_OBJECT@PLT
	testq	%rax, %rax
	jne	.L136
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L145:
	testl	%r13d, %r13d
	jne	.L244
	movq	$1, (%r15)
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L234:
	addq	$2, %r13
	subl	$1, -160(%rbp)
	jne	.L245
	movq	%r13, %rdx
	movq	%r13, -120(%rbp)
	movl	-132(%rbp), %ecx
	subq	%r12, %rdx
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L228:
	movl	$679, %r8d
	movl	$58, %edx
	movl	$108, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L231:
	movl	$711, %r8d
	movl	$156, %edx
	movl	$108, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L160:
	movq	-104(%rbp), %rsi
	movl	%r13d, %edx
	movq	%r14, %rdi
	call	ASN1_STRING_set@PLT
	movl	-160(%rbp), %ecx
	testl	%eax, %eax
	jne	.L152
	movl	$888, %r8d
	movl	$65, %edx
	movl	$204, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r14, %rdi
	call	ASN1_STRING_free@PLT
	movq	$0, (%r15)
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L245:
	subq	$2, %rbx
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L237:
	movl	%ecx, -160(%rbp)
	call	ASN1_TYPE_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L139
	movq	%rax, (%r15)
	movl	-160(%rbp), %ecx
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L239:
	testb	$1, %r13b
	je	.L156
	movl	$860, %r8d
	movl	$214, %edx
	movl	$204, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L243:
	movl	$818, %r8d
	movl	$106, %edx
	movl	$204, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L151
.L116:
	movl	$961, %r8d
	movl	$137, %edx
	movl	$190, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L121
.L244:
	movl	$810, %r8d
	movl	$144, %edx
	movl	$204, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L151
.L241:
	movl	%ecx, %edi
	movl	%ecx, -160(%rbp)
	call	ASN1_STRING_type_new@PLT
	movl	-160(%rbp), %ecx
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L246
	movq	%rax, (%r15)
	jmp	.L159
.L242:
	movl	$748, %r8d
	movl	$65, %edx
	movl	$108, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L121
.L233:
	movl	$951, %r8d
	movl	$58, %edx
	movl	$190, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	movl	%eax, -152(%rbp)
	call	ERR_put_error@PLT
	movl	-152(%rbp), %eax
	jmp	.L121
.L139:
	xorl	%edi, %edi
	call	ASN1_TYPE_free@PLT
	xorl	%eax, %eax
	jmp	.L121
.L240:
	movl	$864, %r8d
	movl	$215, %edx
	movl	$204, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L151
.L246:
	movl	$872, %r8d
	movl	$65, %edx
	movl	$204, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L151
.L226:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE460:
	.size	asn1_d2i_ex_primitive, .-asn1_d2i_ex_primitive
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	", Type="
.LC2:
	.string	"Field="
.LC3:
	.string	"Type="
	.text
	.p2align 4
	.type	asn1_item_embed_d2i, @function
asn1_item_embed_d2i:
.LFB457:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	16(%rbp), %r15d
	movq	24(%rbp), %rbx
	movq	%rsi, -104(%rbp)
	movq	%rdx, -88(%rbp)
	movq	32(%rcx), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	testq	%rdi, %rdi
	je	.L321
	movq	$0, -96(%rbp)
	movq	%rdi, %r13
	movq	%rcx, %r12
	testq	%r14, %r14
	je	.L249
	movq	24(%r14), %rax
	movq	%rax, -96(%rbp)
.L249:
	movl	32(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -108(%rbp)
	cmpl	$30, %eax
	jg	.L388
	cmpb	$6, (%r12)
	ja	.L321
	movzbl	(%r12), %eax
	leaq	.L253(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L253:
	.long	.L257-.L253
	.long	.L252-.L253
	.long	.L256-.L253
	.long	.L321-.L253
	.long	.L255-.L253
	.long	.L254-.L253
	.long	.L252-.L253
	.text
	.p2align 4,,10
	.p2align 3
.L321:
	xorl	%eax, %eax
.L247:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L389
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L252:
	.cfi_restore_state
	movq	-104(%rbp), %rax
	movq	-88(%rbp), %r10
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	cmpl	$-1, %r8d
	jne	.L285
	xorl	%r9d, %r9d
	movl	$16, %r8d
.L285:
	subq	$8, %rsp
	movsbl	%r15b, %r15d
	leaq	-64(%rbp), %rax
	xorl	%edx, %edx
	pushq	%rbx
	xorl	%esi, %esi
	leaq	-70(%rbp), %rcx
	leaq	-88(%rbp), %rdi
	pushq	%r15
	pushq	%r9
	movq	%rax, %r9
	pushq	%r8
	leaq	-69(%rbp), %r8
	pushq	%r10
	movq	%r10, -120(%rbp)
	movq	%rax, -128(%rbp)
	call	asn1_check_tlen
	addq	$48, %rsp
	movq	-120(%rbp), %r10
	movl	$290, %r8d
	testl	%eax, %eax
	je	.L382
	cmpl	$-1, %eax
	je	.L384
	testq	%r14, %r14
	je	.L287
	testb	$4, 8(%r14)
	jne	.L390
.L287:
	movzbl	-70(%rbp), %eax
	movb	%al, -109(%rbp)
.L288:
	cmpb	$0, -69(%rbp)
	je	.L391
	cmpq	$0, 0(%r13)
	je	.L392
.L290:
	cmpq	$0, -96(%rbp)
	je	.L294
	xorl	%ecx, %ecx
	movq	-96(%rbp), %rax
	movq	%r12, %rdx
	movq	%r13, %rsi
	movl	$4, %edi
	call	*%rax
	testl	%eax, %eax
	je	.L270
.L294:
	movq	24(%r12), %rax
	movq	16(%r12), %r15
	xorl	%r14d, %r14d
	testq	%rax, %rax
	jle	.L324
	movq	%rbx, -120(%rbp)
	movq	%r12, %rbx
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L295:
	addq	$1, %r14
	addq	$40, %r15
	cmpq	%rax, %r14
	jge	.L393
.L292:
	testq	$768, (%r15)
	je	.L295
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	asn1_do_adb@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L380
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	asn1_get_field_ptr@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	asn1_template_free@PLT
.L380:
	movq	24(%rbx), %rax
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L256:
	cmpq	$0, -96(%rbp)
	je	.L271
	xorl	%ecx, %ecx
	movq	-96(%rbp), %rax
	movq	%r12, %rdx
	movq	%r13, %rsi
	movl	$4, %edi
	call	*%rax
	testl	%eax, %eax
	je	.L270
.L271:
	cmpq	$0, 0(%r13)
	je	.L394
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	asn1_get_choice_selector@PLT
	movq	24(%r12), %rdx
	movq	16(%r12), %r14
	testl	%eax, %eax
	js	.L273
	cltq
	cmpq	%rdx, %rax
	jge	.L273
	leaq	(%rax,%rax,4), %rax
	movq	%r13, %rdi
	leaq	(%r14,%rax,8), %r14
	movq	%r14, %rsi
	call	asn1_get_field_ptr@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	asn1_template_free@PLT
	movq	%r12, %rdx
	movl	$-1, %esi
	movq	%r13, %rdi
	call	asn1_set_choice_selector@PLT
	movq	24(%r12), %rdx
	movq	16(%r12), %r14
	.p2align 4,,10
	.p2align 3
.L273:
	movq	-104(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	testq	%rdx, %rdx
	jle	.L322
	leaq	-64(%rbp), %rax
	xorl	%r10d, %r10d
	movq	%r12, -136(%rbp)
	movq	%rax, -128(%rbp)
	movq	%rbx, %rax
	movq	%r10, %r12
	movq	%r14, %rbx
	movl	%r15d, -144(%rbp)
	movq	%rax, %r14
	movq	%r13, %r15
.L277:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	%r12d, -152(%rbp)
	movq	%r12, -120(%rbp)
	call	asn1_get_field_ptr@PLT
	subq	$8, %rsp
	movq	-128(%rbp), %rsi
	movq	%r14, %r9
	movq	%rax, %r13
	movl	-108(%rbp), %eax
	movq	-88(%rbp), %rdx
	movl	$1, %r8d
	movq	%r13, %rdi
	movq	%rbx, %rcx
	pushq	%rax
	call	asn1_template_ex_d2i
	popq	%rsi
	popq	%rdi
	cmpl	$-1, %eax
	je	.L395
	movq	%r13, %rdi
	movq	-136(%rbp), %r12
	movq	%r15, %r13
	movq	%rbx, %r14
	movl	-144(%rbp), %r15d
	testl	%eax, %eax
	jle	.L278
	movq	24(%r12), %rdx
.L275:
	cmpq	%rdx, -120(%rbp)
	jne	.L281
	testb	%r15b, %r15b
	jne	.L396
	movl	$265, %r8d
	movl	$143, %edx
	movl	$120, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L254:
	movq	-104(%rbp), %rax
	subq	$8, %rsp
	xorl	%r8d, %r8d
	leaq	-71(%rbp), %rdx
	leaq	-68(%rbp), %rsi
	leaq	-64(%rbp), %r9
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	movq	(%rax), %rax
	pushq	%rbx
	pushq	$1
	pushq	$0
	pushq	$-1
	pushq	-88(%rbp)
	movq	%rax, -64(%rbp)
	call	asn1_check_tlen
	addq	$48, %rsp
	movl	$190, %r8d
	testl	%eax, %eax
	je	.L382
	cmpb	$0, -71(%rbp)
	jne	.L397
	movl	-68(%rbp), %r8d
	cmpl	$30, %r8d
	ja	.L264
	movslq	%r8d, %rax
	leaq	tag2bit(%rip), %rdx
	movq	8(%r12), %rcx
	testq	%rcx, (%rdx,%rax,8)
	je	.L264
	pushq	%rbx
	xorl	%r9d, %r9d
	pushq	$0
.L383:
	movq	-88(%rbp), %rdx
	movq	-104(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r13, %rdi
	call	asn1_d2i_ex_primitive
	popq	%r10
	popq	%r11
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L255:
	movsbl	%r15b, %r15d
	pushq	%rbx
	movq	-88(%rbp), %rdx
	movq	%r12, %rcx
	pushq	%r15
	movq	-104(%rbp), %rsi
	movq	%r13, %rdi
	call	*32(%r14)
	popq	%r8
	popq	%r9
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L257:
	movq	16(%r12), %rcx
	testq	%rcx, %rcx
	je	.L258
	cmpl	$-1, %r8d
	jne	.L326
	testb	%r15b, %r15b
	jne	.L326
	movl	-108(%rbp), %eax
	subq	$8, %rsp
	movq	-88(%rbp), %rdx
	movq	%rbx, %r9
	movq	-104(%rbp), %rsi
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	pushq	%rax
	call	asn1_template_ex_d2i
	popq	%rbx
	popq	%r12
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L388:
	movl	$160, %r8d
	movl	$201, %edx
	movl	$120, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L251:
	movq	48(%r12), %rdx
	xorl	%eax, %eax
	movl	$2, %edi
	leaq	.LC3(%rip), %rsi
	call	ERR_add_error_data@PLT
	xorl	%eax, %eax
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L270:
	movl	$422, %r8d
	movl	$100, %edx
	movl	$120, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L392:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	ASN1_item_ex_new@PLT
	testl	%eax, %eax
	jne	.L290
	movl	$307, %r8d
	.p2align 4,,10
	.p2align 3
.L382:
	movl	$58, %edx
	movl	$120, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L397:
	testb	%r15b, %r15b
	jne	.L384
	movl	$199, %r8d
	movl	$139, %edx
	movl	$120, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L281:
	movl	-152(%rbp), %esi
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	asn1_set_choice_selector@PLT
	cmpq	$0, -96(%rbp)
	je	.L315
.L314:
	xorl	%ecx, %ecx
	movq	-96(%rbp), %rax
	movq	%r12, %rdx
	movq	%r13, %rsi
	movl	$5, %edi
	call	*%rax
	testl	%eax, %eax
	je	.L270
.L315:
	movq	-64(%rbp), %rax
	movq	-104(%rbp), %rbx
	movq	%rax, (%rbx)
	movl	$1, %eax
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L390:
	movq	-104(%rbp), %rcx
	movq	-64(%rbp), %rax
	movb	$1, -109(%rbp)
	subq	(%rcx), %rax
	subq	%rax, %r10
	movq	%r10, -88(%rbp)
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L326:
	movl	$174, %r8d
	movl	$170, %edx
	movl	$120, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L251
.L324:
	movl	$0, -120(%rbp)
	xorl	%ecx, %ecx
.L293:
	cmpb	$0, -70(%rbp)
	je	.L300
	cmpq	$1, -88(%rbp)
	jle	.L305
	movq	-64(%rbp), %rax
	cmpb	$0, (%rax)
	jne	.L305
	cmpb	$0, 1(%rax)
	je	.L398
.L305:
	movl	$381, %r8d
	movl	$137, %edx
	movl	$120, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L393:
	movq	%rbx, %r12
	movq	-120(%rbp), %rbx
	movq	16(%r12), %r15
	testq	%rax, %rax
	jle	.L324
	movq	%rbx, -152(%rbp)
	xorl	%r14d, %r14d
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L298:
	movq	24(%r12), %rax
	xorl	%r8d, %r8d
	subq	$1, %rax
	cmpq	%r14, %rax
	je	.L301
	movl	(%rbx), %r8d
	andl	$1, %r8d
.L301:
	movl	-108(%rbp), %eax
	subq	$8, %rsp
	movq	-128(%rbp), %rsi
	movq	%rbx, %rcx
	movq	-152(%rbp), %r9
	movq	%r11, -144(%rbp)
	pushq	%rax
	movq	%rdi, -120(%rbp)
	call	asn1_template_ex_d2i
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	je	.L376
	cmpl	$-1, %eax
	movq	-120(%rbp), %rdi
	movq	-144(%rbp), %r11
	je	.L399
	subq	-64(%rbp), %r11
	addq	%r11, -88(%rbp)
	movl	-136(%rbp), %eax
.L386:
	addl	$1, %eax
	addq	$40, %r15
	addq	$1, %r14
	movl	%eax, -120(%rbp)
	cmpq	%r14, 24(%r12)
	jle	.L400
.L304:
	movl	$1, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	%r14d, -120(%rbp)
	movl	%r14d, -136(%rbp)
	call	asn1_do_adb@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L251
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	asn1_get_field_ptr@PLT
	movq	-88(%rbp), %rdx
	movq	%rax, %rdi
	testq	%rdx, %rdx
	je	.L297
	movq	-64(%rbp), %r11
	cmpq	$1, %rdx
	jle	.L298
	cmpb	$0, (%r11)
	jne	.L298
	cmpb	$0, 1(%r11)
	jne	.L298
	addq	$2, %r11
	cmpb	$0, -70(%rbp)
	movq	%r11, -64(%rbp)
	je	.L401
	subq	$2, %rdx
	movb	$0, -70(%rbp)
	movq	%rdx, -88(%rbp)
.L300:
	cmpb	$0, -109(%rbp)
	jne	.L316
	cmpq	$0, -88(%rbp)
	jne	.L306
.L316:
	movslq	-120(%rbp), %rax
	movq	%rax, %rcx
.L307:
	movslq	-120(%rbp), %rbx
	cmpq	%rcx, 24(%r12)
	jg	.L310
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L402:
	movq	%rax, %rsi
	movq	%r13, %rdi
	addq	$40, %r15
	addq	$1, %rbx
	call	asn1_get_field_ptr@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	asn1_template_free@PLT
	cmpq	%rbx, 24(%r12)
	jle	.L313
.L310:
	movl	$1, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	asn1_do_adb@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L251
	testb	$1, (%rax)
	jne	.L402
	movl	$406, %r8d
	movl	$121, %edx
	movl	$120, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L279:
	movq	24(%r14), %rdx
	movq	48(%r12), %r8
	xorl	%eax, %eax
	movl	$4, %edi
	leaq	.LC1(%rip), %rcx
	leaq	.LC2(%rip), %rsi
	call	ERR_add_error_data@PLT
	xorl	%eax, %eax
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L264:
	testb	%r15b, %r15b
	jne	.L384
	movl	$207, %r8d
	movl	$140, %edx
	movl	$120, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L394:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	ASN1_item_ex_new@PLT
	testl	%eax, %eax
	je	.L274
	movq	24(%r12), %rdx
	movq	16(%r12), %r14
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L399:
	movq	%rbx, %rsi
	call	asn1_template_free@PLT
	movl	%r14d, %eax
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L258:
	movsbl	%r15b, %r15d
	pushq	%rbx
	pushq	%r15
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L395:
	leal	1(%r12), %eax
	addq	$1, %r12
	addq	$40, %rbx
	movl	%eax, -152(%rbp)
	movq	-136(%rbp), %rax
	movq	%r12, -120(%rbp)
	movq	24(%rax), %rdx
	cmpq	%r12, %rdx
	jg	.L277
	movq	%r15, %r13
	movq	%rax, %r12
	movl	-144(%rbp), %r15d
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L391:
	movl	$302, %r8d
	movl	$149, %edx
	movl	$120, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L278:
	movq	%rbx, %rsi
	call	asn1_template_free@PLT
	movl	$253, %r8d
	movl	$58, %edx
	leaq	.LC0(%rip), %rcx
	movl	$120, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
	testq	%rbx, %rbx
	je	.L251
	jmp	.L279
.L313:
	movq	-104(%rbp), %rax
	movq	-64(%rbp), %rdx
	movq	%r12, %rcx
	movq	%r13, %rdi
	movq	(%rax), %rsi
	subq	%rsi, %rdx
	call	asn1_enc_save@PLT
	testl	%eax, %eax
	je	.L270
	cmpq	$0, -96(%rbp)
	jne	.L314
	jmp	.L315
.L401:
	movl	$341, %r8d
	movl	$159, %edx
	movl	$120, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L251
.L297:
	cmpb	$0, -70(%rbp)
	jne	.L305
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L274:
	movl	$230, %r8d
	jmp	.L382
.L376:
	movq	%rbx, %r14
	jmp	.L279
.L400:
	movq	%r14, %rcx
	jmp	.L293
.L322:
	movl	$0, -152(%rbp)
	movq	$0, -120(%rbp)
	jmp	.L275
.L396:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	ASN1_item_ex_free@PLT
.L384:
	movl	$-1, %eax
	jmp	.L247
.L398:
	addq	$2, %rax
	cmpb	$0, -109(%rbp)
	movq	%rax, -64(%rbp)
	jne	.L307
.L306:
	movl	$386, %r8d
	movl	$148, %edx
	movl	$120, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L251
.L389:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE457:
	.size	asn1_item_embed_d2i, .-asn1_item_embed_d2i
	.p2align 4
	.type	asn1_template_noexp_d2i, @function
asn1_template_noexp_d2i:
.LFB459:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -120(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%r9, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L433
	movq	(%rcx), %rax
	movq	(%rsi), %rdx
	movq	%rdi, %r12
	movq	%rcx, %rbx
	movl	%eax, %r9d
	movq	%rdx, -72(%rbp)
	andl	$192, %r9d
	testb	$16, %ah
	je	.L405
	movq	%rdi, -80(%rbp)
	leaq	-80(%rbp), %r12
.L405:
	movl	%eax, %edx
	movq	-104(%rbp), %r10
	movsbl	%r8b, %r8d
	andl	$8, %edx
	testb	$6, %al
	je	.L406
	testl	%edx, %edx
	je	.L407
	movl	8(%rbx), %eax
.L408:
	subq	$8, %rsp
	pushq	-112(%rbp)
	leaq	-72(%rbp), %r15
	leaq	-81(%rbp), %rcx
	pushq	%r8
	leaq	-104(%rbp), %rdi
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	pushq	%r9
	xorl	%esi, %esi
	movq	%r15, %r9
	pushq	%rax
	pushq	%r10
	call	asn1_check_tlen
	addq	$48, %rsp
	testl	%eax, %eax
	je	.L455
	cmpl	$-1, %eax
	je	.L403
	movq	(%r12), %r13
	leaq	-64(%rbp), %r14
	testq	%r13, %r13
	jne	.L411
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L413:
	movq	%r13, %rdi
	call	OPENSSL_sk_pop@PLT
	movq	32(%rbx), %rsi
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	call	ASN1_item_ex_free@PLT
.L411:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jg	.L413
	movq	(%r12), %rax
.L412:
	testq	%rax, %rax
	je	.L457
	movq	-104(%rbp), %rdx
	leaq	-64(%rbp), %r13
	testq	%rdx, %rdx
	jg	.L422
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L416:
	movl	16(%rbp), %eax
	subq	$8, %rsp
	movq	32(%rbx), %rcx
	xorl	%r9d, %r9d
	movl	$-1, %r8d
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	$0, -64(%rbp)
	pushq	%rax
	pushq	-112(%rbp)
	pushq	$0
	call	asn1_item_embed_d2i
	addq	$32, %rsp
	testl	%eax, %eax
	je	.L458
	movq	-64(%rbp), %rsi
	movq	(%r12), %rdi
	subq	-72(%rbp), %r14
	addq	%r14, -104(%rbp)
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L459
	movq	-104(%rbp), %rdx
	testq	%rdx, %rdx
	jle	.L425
.L422:
	movq	-72(%rbp), %r14
	cmpq	$1, %rdx
	je	.L416
	cmpb	$0, (%r14)
	jne	.L416
	cmpb	$0, 1(%r14)
	jne	.L416
	addq	$2, %r14
	cmpb	$0, -81(%rbp)
	movq	%r14, -72(%rbp)
	jne	.L426
	movl	$583, %r8d
	movl	$159, %edx
	movl	$131, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L433:
	xorl	%eax, %eax
.L403:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L460
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L406:
	.cfi_restore_state
	movq	32(%rbx), %rcx
	leaq	-72(%rbp), %rsi
	testl	%edx, %edx
	je	.L427
	movl	16(%rbp), %eax
	subq	$8, %rsp
	movq	%r10, %rdx
	movq	%r12, %rdi
	pushq	%rax
	pushq	-112(%rbp)
	pushq	%r8
	movl	8(%rbx), %r8d
	call	asn1_item_embed_d2i
	addq	$32, %rsp
	testl	%eax, %eax
	je	.L461
	movq	-72(%rbp), %r14
	cmpl	$-1, %eax
	jne	.L426
.L431:
	movl	$-1, %eax
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L407:
	movl	%eax, %r9d
	movl	$16, %eax
	andl	$2, %r9d
	je	.L408
	xorl	%r9d, %r9d
	movl	$17, %eax
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L427:
	movl	16(%rbp), %eax
	subq	$8, %rsp
	xorl	%r9d, %r9d
	movq	%r10, %rdx
	movq	%r12, %rdi
	pushq	%rax
	pushq	-112(%rbp)
	pushq	%r8
	movl	$-1, %r8d
	call	asn1_item_embed_d2i
	addq	$32, %rsp
	testl	%eax, %eax
	je	.L462
	cmpl	$-1, %eax
	je	.L431
.L453:
	movq	-72(%rbp), %r14
.L426:
	movq	-120(%rbp), %rax
	movq	%r14, (%rax)
	movl	$1, %eax
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L455:
	movl	%eax, -112(%rbp)
	movl	$553, %r8d
.L454:
	movl	$58, %edx
	movl	$131, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	-112(%rbp), %eax
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L462:
	movl	%eax, -112(%rbp)
	movl	$627, %r8d
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L461:
	movl	%eax, -112(%rbp)
	movl	$618, %r8d
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L456:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, (%r12)
	jmp	.L412
	.p2align 4,,10
	.p2align 3
.L425:
	cmpb	$0, -81(%rbp)
	je	.L453
	movl	$609, %r8d
	movl	$137, %edx
	movl	$131, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L458:
	movl	$595, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$58, %edx
.L452:
	movl	$131, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
	movq	32(%rbx), %rsi
	movq	-64(%rbp), %rdi
	call	ASN1_item_free@PLT
	xorl	%eax, %eax
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L459:
	movl	$603, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L457:
	movl	$572, %r8d
	movl	$65, %edx
	movl	$131, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L403
.L460:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE459:
	.size	asn1_template_noexp_d2i, .-asn1_template_noexp_d2i
	.p2align 4
	.type	asn1_template_ex_d2i, @function
asn1_template_ex_d2i:
.LFB458:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L489
	movq	%r9, %rbx
	movq	(%rcx), %r11
	movq	(%rsi), %r9
	movq	%rdi, %r14
	movq	%rsi, %r15
	movq	%rdx, %r13
	movq	%rcx, %r12
	movl	%r8d, %r10d
	movq	%r9, -80(%rbp)
	testb	$16, %r11b
	je	.L465
	movq	8(%rcx), %rax
	movq	%r9, -64(%rbp)
	movq	%rax, -104(%rbp)
	testq	%rbx, %rbx
	je	.L466
	cmpb	$0, (%rbx)
	je	.L467
	movq	8(%rbx), %rdx
	movl	4(%rbx), %eax
	movq	%rdx, -72(%rbp)
	movl	20(%rbx), %edx
	movl	%edx, -84(%rbp)
	movl	16(%rbx), %edx
	movl	%edx, -88(%rbp)
	movslq	24(%rbx), %rdx
	addq	%r9, %rdx
	movq	%rdx, -64(%rbp)
.L468:
	testb	$-128, %al
	jne	.L503
.L485:
	movq	-104(%rbp), %rdi
	testl	%edi, %edi
	js	.L470
	cmpl	-88(%rbp), %edi
	je	.L504
.L471:
	movl	$-1, %eax
	testb	%r10b, %r10b
	jne	.L463
	testq	%rbx, %rbx
	je	.L473
	movb	$0, (%rbx)
.L473:
	movl	$1130, %r8d
	movl	$168, %edx
	movl	$104, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L469:
	movl	$465, %r8d
	movl	$58, %edx
	movl	$132, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L463:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L505
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L465:
	.cfi_restore_state
	movl	16(%rbp), %eax
	subq	$8, %rsp
	movq	%rbx, %r9
	movsbl	%r8b, %r8d
	pushq	%rax
	call	asn1_template_noexp_d2i
	popq	%rdx
	popq	%rcx
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L467:
	movl	%r8d, -116(%rbp)
	leaq	-84(%rbp), %rcx
	leaq	-88(%rbp), %rdx
	movq	%r13, %r8
	leaq	-72(%rbp), %rsi
	leaq	-64(%rbp), %rdi
	movq	%r11, -128(%rbp)
	movq	%r9, -112(%rbp)
	call	ASN1_get_object@PLT
	movl	-84(%rbp), %edx
	movq	-112(%rbp), %r9
	movb	$1, (%rbx)
	movq	-72(%rbp), %rcx
	movl	-116(%rbp), %r10d
	movl	%eax, 4(%rbx)
	movl	%edx, 20(%rbx)
	movl	-88(%rbp), %edx
	movq	%rcx, 8(%rbx)
	movq	-128(%rbp), %r11
	movl	%edx, 16(%rbx)
	movq	-64(%rbp), %rdx
	subq	%r9, %rdx
	testb	$-127, %al
	movl	%edx, 24(%rbx)
	jne	.L468
	movslq	%edx, %rdx
	addq	%rcx, %rdx
	cmpq	%rdx, %r13
	jge	.L468
	movl	$1110, %r8d
	movl	$155, %edx
	movl	$104, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movb	$0, (%rbx)
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L466:
	movl	%r8d, -116(%rbp)
	leaq	-84(%rbp), %rcx
	leaq	-88(%rbp), %rdx
	movq	%r13, %r8
	leaq	-72(%rbp), %rsi
	leaq	-64(%rbp), %rdi
	movq	%r11, -128(%rbp)
	movq	%r9, -112(%rbp)
	call	ASN1_get_object@PLT
	movq	-112(%rbp), %r9
	movl	-116(%rbp), %r10d
	testb	$-128, %al
	movq	-128(%rbp), %r11
	je	.L485
	movl	$1118, %r8d
	movl	$102, %edx
	movl	$104, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L504:
	andl	$192, %r11d
	cmpl	-84(%rbp), %r11d
	jne	.L471
	testq	%rbx, %rbx
	je	.L470
	movb	$0, (%rbx)
	.p2align 4,,10
	.p2align 3
.L470:
	movq	-64(%rbp), %r10
	testb	$1, %al
	jne	.L474
	movq	-72(%rbp), %rdx
.L475:
	movq	%r10, -80(%rbp)
	movl	%eax, %r13d
	testb	$32, %al
	je	.L506
	movl	16(%rbp), %eax
	subq	$8, %rsp
	leaq	-80(%rbp), %rsi
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%rbx, %r9
	movq	%r12, %rcx
	movq	%r10, -112(%rbp)
	pushq	%rax
	movq	%rdx, -104(%rbp)
	call	asn1_template_noexp_d2i
	popq	%rsi
	movq	-104(%rbp), %rdx
	testl	%eax, %eax
	movq	-112(%rbp), %r10
	popq	%rdi
	je	.L507
	movq	-80(%rbp), %rax
	movq	%rax, %rcx
	subq	%r10, %rcx
	subq	%rcx, %rdx
	andl	$1, %r13d
	je	.L479
	cmpq	$1, %rdx
	jle	.L480
	cmpb	$0, (%rax)
	jne	.L480
	cmpb	$0, 1(%rax)
	jne	.L480
	addq	$2, %rax
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L480:
	movl	$485, %r8d
	movl	$137, %edx
	movl	$132, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L489:
	xorl	%eax, %eax
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L506:
	movl	$470, %r8d
	movl	$120, %edx
	movl	$132, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L474:
	movq	%r10, %rdx
	subq	%r9, %rdx
	subq	%rdx, %r13
	movq	%r13, %rdx
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L479:
	testq	%rdx, %rdx
	jne	.L508
.L481:
	movq	%rax, (%r15)
	movl	$1, %eax
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L503:
	movl	$1118, %r8d
	movl	$102, %edx
	movl	$104, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movb	$0, (%rbx)
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L507:
	movl	$477, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$58, %edx
	movl	%eax, -104(%rbp)
	movl	$132, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
	movl	-104(%rbp), %eax
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L508:
	movl	$493, %r8d
	movl	$119, %edx
	movl	$132, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L463
.L505:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE458:
	.size	asn1_template_ex_d2i, .-asn1_template_ex_d2i
	.p2align 4
	.globl	ASN1_tag2bit
	.type	ASN1_tag2bit, @function
ASN1_tag2bit:
.LFB454:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$30, %edi
	ja	.L509
	movslq	%edi, %rdi
	leaq	tag2bit(%rip), %rax
	movq	(%rax,%rdi,8), %rax
.L509:
	ret
	.cfi_endproc
.LFE454:
	.size	ASN1_tag2bit, .-ASN1_tag2bit
	.p2align 4
	.globl	ASN1_item_d2i
	.type	ASN1_item_d2i, @function
ASN1_item_d2i:
.LFB455:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -136(%rbp)
	movq	32(%rcx), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-120(%rbp), %rax
	testq	%rdi, %rdi
	movb	$0, -96(%rbp)
	cmove	%rax, %r12
	movq	%rdx, -112(%rbp)
	movzbl	(%rcx), %eax
	movq	$0, -120(%rbp)
	movq	$0, -104(%rbp)
	testq	%r15, %r15
	je	.L635
	movq	24(%r15), %rbx
	cmpb	$6, %al
	ja	.L547
	leaq	.L517(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L517:
	.long	.L521-.L517
	.long	.L516-.L517
	.long	.L520-.L517
	.long	.L547-.L517
	.long	.L519-.L517
	.long	.L518-.L517
	.long	.L516-.L517
	.text
	.p2align 4,,10
	.p2align 3
.L635:
	cmpb	$6, %al
	ja	.L547
	leaq	.L581(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L581:
	.long	.L521-.L581
	.long	.L589-.L581
	.long	.L590-.L581
	.long	.L547-.L581
	.long	.L519-.L581
	.long	.L518-.L581
	.long	.L589-.L581
	.text
.L590:
	xorl	%ebx, %ebx
.L529:
	cmpq	$0, (%r12)
	movq	%r13, %rsi
	movq	%r12, %rdi
	je	.L531
	call	asn1_get_choice_selector@PLT
	movq	24(%r13), %rdx
	movq	16(%r13), %r14
	testl	%eax, %eax
	js	.L533
	cltq
	cmpq	%rdx, %rax
	jge	.L533
	leaq	(%rax,%rax,4), %rax
	movq	%r12, %rdi
	leaq	(%r14,%rax,8), %r14
	movq	%r14, %rsi
	call	asn1_get_field_ptr@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	asn1_template_free@PLT
	movq	%r13, %rdx
	movl	$-1, %esi
	movq	%r12, %rdi
	call	asn1_set_choice_selector@PLT
	movq	24(%r13), %rdx
	movq	16(%r13), %r14
	.p2align 4,,10
	.p2align 3
.L533:
	movq	-136(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -104(%rbp)
	testq	%rdx, %rdx
	jle	.L585
	leaq	-96(%rbp), %rax
	xorl	%r10d, %r10d
	movq	%rbx, -168(%rbp)
	movq	%r14, %rbx
	movq	%rax, -160(%rbp)
	leaq	-104(%rbp), %rax
	movq	%r10, %r15
	movq	%r12, %r14
	movq	%rax, -152(%rbp)
.L537:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movl	%r15d, -176(%rbp)
	movq	%r15, -144(%rbp)
	call	asn1_get_field_ptr@PLT
	subq	$8, %rsp
	movq	-112(%rbp), %rdx
	movq	%rbx, %rcx
	pushq	$1
	movq	-152(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	movq	-160(%rbp), %r9
	movl	$1, %r8d
	call	asn1_template_ex_d2i
	popq	%rsi
	popq	%rdi
	cmpl	$-1, %eax
	je	.L650
	movq	%r12, %r15
	movq	%r14, %r12
	movq	%rbx, %r14
	movq	-168(%rbp), %rbx
	testl	%eax, %eax
	jle	.L538
	movq	24(%r13), %rdx
.L535:
	cmpq	%rdx, -144(%rbp)
	jne	.L541
	movl	$265, %r8d
	movl	$143, %edx
	movl	$120, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L525
.L589:
	xorl	%ebx, %ebx
.L516:
	subq	$8, %rsp
	leaq	-104(%rbp), %r9
	leaq	-125(%rbp), %r8
	xorl	%edx, %edx
	movq	-136(%rbp), %rax
	leaq	-126(%rbp), %rcx
	leaq	-112(%rbp), %rdi
	xorl	%esi, %esi
	movq	%r9, -152(%rbp)
	movq	(%rax), %rax
	movq	%rax, -104(%rbp)
	leaq	-96(%rbp), %rax
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$16
	pushq	%r14
	movq	%rax, -160(%rbp)
	call	asn1_check_tlen
	addq	$48, %rsp
	movl	$290, %r8d
	testl	%eax, %eax
	je	.L646
	cmpl	$-1, %eax
	je	.L547
	testq	%r15, %r15
	je	.L548
	testb	$4, 8(%r15)
	jne	.L651
.L548:
	movzbl	-126(%rbp), %eax
	movb	%al, -177(%rbp)
.L549:
	cmpb	$0, -125(%rbp)
	je	.L652
	cmpq	$0, (%r12)
	je	.L653
.L551:
	testq	%rbx, %rbx
	je	.L555
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movl	$4, %edi
	call	*%rbx
	testl	%eax, %eax
	je	.L530
.L555:
	movq	24(%r13), %rax
	movq	16(%r13), %r15
	xorl	%r14d, %r14d
	testq	%rax, %rax
	jle	.L587
	movq	%rbx, -144(%rbp)
	movq	%r12, %rbx
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L556:
	addq	$1, %r14
	addq	$40, %r15
	cmpq	%rax, %r14
	jge	.L654
.L553:
	testq	$768, (%r15)
	je	.L556
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	asn1_do_adb@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L644
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	asn1_get_field_ptr@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	asn1_template_free@PLT
.L644:
	movq	24(%r13), %rax
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L654:
	movq	%rbx, %r12
	movq	16(%r13), %r15
	movq	-144(%rbp), %rbx
	testq	%rax, %rax
	jle	.L587
	xorl	%eax, %eax
	movq	%rbx, -192(%rbp)
	movq	%rax, %rbx
	jmp	.L565
	.p2align 4,,10
	.p2align 3
.L559:
	movq	24(%r13), %rax
	xorl	%r8d, %r8d
	subq	$1, %rax
	cmpq	%rbx, %rax
	je	.L562
	movl	(%r14), %r8d
	andl	$1, %r8d
.L562:
	subq	$8, %rsp
	movq	-160(%rbp), %r9
	movq	%r14, %rcx
	movq	-152(%rbp), %rsi
	pushq	$1
	movq	%r11, -176(%rbp)
	movq	%rdi, -144(%rbp)
	call	asn1_template_ex_d2i
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	je	.L539
	cmpl	$-1, %eax
	movq	-144(%rbp), %rdi
	movq	-176(%rbp), %r11
	je	.L655
	subq	-104(%rbp), %r11
	addq	%r11, -112(%rbp)
.L648:
	movl	-168(%rbp), %eax
	addq	$40, %r15
	addq	$1, %rbx
	addl	$1, %eax
	movl	%eax, -144(%rbp)
	cmpq	%rbx, 24(%r13)
	jle	.L656
.L565:
	movl	$1, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%ebx, -168(%rbp)
	movl	%ebx, -144(%rbp)
	call	asn1_do_adb@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L525
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	asn1_get_field_ptr@PLT
	movq	-112(%rbp), %rdx
	movq	%rax, %rdi
	testq	%rdx, %rdx
	je	.L558
	movq	-104(%rbp), %r11
	cmpq	$1, %rdx
	jle	.L559
	cmpb	$0, (%r11)
	jne	.L559
	cmpb	$0, 1(%r11)
	jne	.L559
	addq	$2, %r11
	cmpb	$0, -126(%rbp)
	movq	-192(%rbp), %rbx
	movq	%r11, -104(%rbp)
	je	.L657
	subq	$2, %rdx
	movb	$0, -126(%rbp)
	movq	%rdx, -112(%rbp)
.L561:
	cmpb	$0, -177(%rbp)
	jne	.L582
	cmpq	$0, -112(%rbp)
	jne	.L567
.L582:
	movslq	-144(%rbp), %rax
	movq	%rax, %rcx
.L568:
	movslq	-144(%rbp), %rax
	movq	%rax, %rsi
	cmpq	%rcx, 24(%r13)
	jle	.L574
	movq	%rbx, -144(%rbp)
	movq	%rsi, %r14
	jmp	.L571
	.p2align 4,,10
	.p2align 3
.L659:
	movq	%rax, %rsi
	movq	%r12, %rdi
	addq	$40, %r15
	addq	$1, %r14
	call	asn1_get_field_ptr@PLT
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	asn1_template_free@PLT
	cmpq	%r14, 24(%r13)
	jle	.L658
.L571:
	movl	$1, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	asn1_do_adb@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L525
	testb	$1, (%rax)
	jne	.L659
	movl	$406, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$121, %edx
	movq	%rax, %r14
	movl	$120, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
.L539:
	movq	24(%r14), %rdx
	movq	48(%r13), %r8
	movl	$4, %edi
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rcx
	leaq	.LC2(%rip), %rsi
	call	ERR_add_error_data@PLT
.L547:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	ASN1_item_ex_free@PLT
	xorl	%eax, %eax
.L512:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L660
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L521:
	.cfi_restore_state
	movq	16(%r13), %rcx
	testq	%rcx, %rcx
	je	.L522
	subq	$8, %rsp
	movq	%r14, %rdx
	leaq	-96(%rbp), %r9
	xorl	%r8d, %r8d
	pushq	$1
	movq	-136(%rbp), %rsi
	movq	%r12, %rdi
	call	asn1_template_ex_d2i
	popq	%r15
	popq	%rdx
	jmp	.L523
.L519:
	leaq	-96(%rbp), %rax
	xorl	%r9d, %r9d
	movl	$-1, %r8d
	movq	%r13, %rcx
	pushq	%rax
	movq	-136(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r12, %rdi
	pushq	$0
	call	*32(%r15)
	popq	%r8
	popq	%r9
.L523:
	testl	%eax, %eax
	jle	.L547
.L543:
	movq	(%r12), %rax
	jmp	.L512
.L518:
	movq	-136(%rbp), %rax
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	-96(%rbp), %rbx
	leaq	-127(%rbp), %rdx
	xorl	%edi, %edi
	movq	(%rax), %rax
	pushq	%rbx
	leaq	-124(%rbp), %rsi
	leaq	-104(%rbp), %r9
	pushq	$1
	pushq	$0
	pushq	$-1
	pushq	%r14
	movq	%rax, -104(%rbp)
	call	asn1_check_tlen
	addq	$48, %rsp
	movl	$190, %r8d
	testl	%eax, %eax
	je	.L646
	cmpb	$0, -127(%rbp)
	jne	.L661
	movl	-124(%rbp), %r8d
	cmpl	$30, %r8d
	ja	.L527
	movslq	%r8d, %rax
	leaq	tag2bit(%rip), %rdx
	movq	(%rdx,%rax,8), %rax
	testq	%rax, 8(%r13)
	je	.L527
	pushq	%rbx
	movq	-112(%rbp), %rdx
	xorl	%r9d, %r9d
	movq	%r13, %rcx
	pushq	$0
	movq	-136(%rbp), %rsi
	movq	%r12, %rdi
	call	asn1_d2i_ex_primitive
	popq	%r10
	popq	%r11
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L520:
	testq	%rbx, %rbx
	je	.L529
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movl	$4, %edi
	call	*%rbx
	testl	%eax, %eax
	jne	.L529
.L530:
	movl	$422, %r8d
	movl	$100, %edx
	movl	$120, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L525:
	movq	48(%r13), %rdx
	leaq	.LC3(%rip), %rsi
	movl	$2, %edi
	xorl	%eax, %eax
	call	ERR_add_error_data@PLT
	jmp	.L547
.L534:
	movl	$230, %r8d
	.p2align 4,,10
	.p2align 3
.L646:
	movl	$58, %edx
	movl	$120, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L661:
	movl	$199, %r8d
	movl	$139, %edx
	movl	$120, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L541:
	movl	-176(%rbp), %esi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	asn1_set_choice_selector@PLT
	testq	%rbx, %rbx
	je	.L576
.L575:
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movl	$5, %edi
	call	*%rbx
	testl	%eax, %eax
	je	.L530
.L576:
	movq	-104(%rbp), %rax
	movq	-136(%rbp), %rcx
	movq	%rax, (%rcx)
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L651:
	movq	-136(%rbp), %rcx
	movq	-104(%rbp), %rax
	movb	$1, -177(%rbp)
	subq	(%rcx), %rax
	subq	%rax, %r14
	movq	%r14, -112(%rbp)
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L531:
	call	ASN1_item_ex_new@PLT
	testl	%eax, %eax
	je	.L534
	movq	24(%r13), %rdx
	movq	16(%r13), %r14
	jmp	.L533
.L587:
	movl	$0, -144(%rbp)
	xorl	%ecx, %ecx
.L554:
	cmpb	$0, -126(%rbp)
	je	.L561
	cmpq	$1, -112(%rbp)
	jle	.L566
	movq	-104(%rbp), %rax
	cmpb	$0, (%rax)
	jne	.L566
	cmpb	$0, 1(%rax)
	je	.L662
.L566:
	movl	$381, %r8d
	movl	$137, %edx
	movl	$120, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L527:
	movl	$207, %r8d
	movl	$140, %edx
	movl	$120, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L655:
	movq	%r14, %rsi
	call	asn1_template_free@PLT
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L522:
	leaq	-96(%rbp), %rax
	movq	%r14, %rdx
	xorl	%r9d, %r9d
	movl	$-1, %r8d
	pushq	%rax
	movq	-136(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	pushq	$0
	call	asn1_d2i_ex_primitive
	popq	%rbx
	popq	%r14
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L650:
	movq	24(%r13), %rdx
	leal	1(%r15), %eax
	addq	$1, %r15
	addq	$40, %rbx
	movl	%eax, -176(%rbp)
	movq	%r15, -144(%rbp)
	cmpq	%r15, %rdx
	jg	.L537
	movq	-168(%rbp), %rbx
	movq	%r14, %r12
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L652:
	movl	$302, %r8d
	movl	$149, %edx
	movl	$120, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L653:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	ASN1_item_ex_new@PLT
	movl	$307, %r8d
	testl	%eax, %eax
	jne	.L551
	jmp	.L646
	.p2align 4,,10
	.p2align 3
.L538:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	asn1_template_free@PLT
	movl	$253, %r8d
	movl	$58, %edx
	leaq	.LC0(%rip), %rcx
	movl	$120, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
	testq	%r14, %r14
	je	.L525
	jmp	.L539
.L658:
	movq	-144(%rbp), %rbx
.L574:
	movq	-136(%rbp), %rax
	movq	-104(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	subq	%rsi, %rdx
	call	asn1_enc_save@PLT
	testl	%eax, %eax
	je	.L530
	testq	%rbx, %rbx
	jne	.L575
	jmp	.L576
.L657:
	movl	$341, %r8d
	movl	$159, %edx
	movl	$120, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L525
.L558:
	cmpb	$0, -126(%rbp)
	movq	-192(%rbp), %rbx
	jne	.L566
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L656:
	movq	%rbx, %rcx
	movq	-192(%rbp), %rbx
	jmp	.L554
.L585:
	movq	$0, -144(%rbp)
	movl	$0, -176(%rbp)
	jmp	.L535
.L662:
	addq	$2, %rax
	cmpb	$0, -177(%rbp)
	movq	%rax, -104(%rbp)
	jne	.L568
.L567:
	movl	$386, %r8d
	movl	$148, %edx
	movl	$120, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L525
.L660:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE455:
	.size	ASN1_item_d2i, .-ASN1_item_d2i
	.p2align 4
	.globl	ASN1_item_ex_d2i
	.type	ASN1_item_ex_d2i, @function
ASN1_item_ex_d2i:
.LFB456:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	24(%rbp), %rax
	movl	16(%rbp), %r15d
	movq	%rsi, -96(%rbp)
	movq	32(%rcx), %r10
	movq	%rax, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, -72(%rbp)
	movq	$0, -64(%rbp)
	testq	%rdi, %rdi
	je	.L665
	movzbl	(%rcx), %eax
	movsbl	%r15b, %edx
	testq	%r10, %r10
	je	.L666
	movq	24(%r10), %rbx
	cmpb	$6, %al
	ja	.L667
	leaq	.L669(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L669:
	.long	.L673-.L669
	.long	.L668-.L669
	.long	.L672-.L669
	.long	.L667-.L669
	.long	.L671-.L669
	.long	.L670-.L669
	.long	.L668-.L669
	.text
	.p2align 4,,10
	.p2align 3
.L666:
	cmpb	$6, %al
	ja	.L667
	leaq	.L733(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L733:
	.long	.L673-.L733
	.long	.L741-.L733
	.long	.L742-.L733
	.long	.L667-.L733
	.long	.L671-.L733
	.long	.L670-.L733
	.long	.L741-.L733
	.text
.L742:
	xorl	%ebx, %ebx
.L684:
	cmpq	$0, 0(%r13)
	movq	%r12, %rsi
	movq	%r13, %rdi
	je	.L686
	call	asn1_get_choice_selector@PLT
	movq	24(%r12), %rdx
	movq	16(%r12), %r14
	testl	%eax, %eax
	js	.L688
	cltq
	cmpq	%rdx, %rax
	jge	.L688
	leaq	(%rax,%rax,4), %rax
	movq	%r13, %rdi
	leaq	(%r14,%rax,8), %r14
	movq	%r14, %rsi
	call	asn1_get_field_ptr@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	asn1_template_free@PLT
	movq	%r12, %rdx
	movl	$-1, %esi
	movq	%r13, %rdi
	call	asn1_set_choice_selector@PLT
	movq	24(%r12), %rdx
	movq	16(%r12), %r14
	.p2align 4,,10
	.p2align 3
.L688:
	movq	-96(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	testq	%rdx, %rdx
	jle	.L737
	leaq	-64(%rbp), %rax
	movl	%r15d, -128(%rbp)
	xorl	%r10d, %r10d
	movq	%rax, -112(%rbp)
	movq	%rbx, -120(%rbp)
	movq	%r14, %rbx
	movq	%r10, %r14
.L692:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movl	%r14d, -132(%rbp)
	movq	%r14, -104(%rbp)
	call	asn1_get_field_ptr@PLT
	subq	$8, %rsp
	movq	-112(%rbp), %rsi
	movq	%rbx, %rcx
	pushq	$1
	movq	-88(%rbp), %r9
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	-72(%rbp), %rdx
	movq	%rax, %r15
	call	asn1_template_ex_d2i
	popq	%rsi
	popq	%rdi
	cmpl	$-1, %eax
	je	.L804
	movq	%r15, %rdi
	movq	%rbx, %r14
	movl	-128(%rbp), %r15d
	movq	-120(%rbp), %rbx
	testl	%eax, %eax
	jle	.L693
	movq	24(%r12), %rdx
.L690:
	cmpq	%rdx, -104(%rbp)
	jne	.L696
	testb	%r15b, %r15b
	jne	.L805
	movl	$265, %r8d
	movl	$143, %edx
	movl	$120, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L677
.L741:
	xorl	%ebx, %ebx
.L668:
	movq	-96(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	cmpl	$-1, %r8d
	je	.L806
.L701:
	subq	$8, %rsp
	pushq	-88(%rbp)
	leaq	-64(%rbp), %rax
	xorl	%esi, %esi
	pushq	%rdx
	leaq	-78(%rbp), %rcx
	xorl	%edx, %edx
	leaq	-72(%rbp), %rdi
	pushq	%r9
	movq	%rax, %r9
	pushq	%r8
	leaq	-77(%rbp), %r8
	pushq	%r14
	movq	%r10, -104(%rbp)
	movq	%rax, -112(%rbp)
	call	asn1_check_tlen
	addq	$48, %rsp
	movq	-104(%rbp), %r10
	movl	$290, %r8d
	testl	%eax, %eax
	je	.L801
	cmpl	$-1, %eax
	je	.L681
	testq	%r10, %r10
	je	.L703
	testb	$4, 8(%r10)
	jne	.L807
.L703:
	movzbl	-78(%rbp), %eax
	movb	%al, -132(%rbp)
.L704:
	cmpb	$0, -77(%rbp)
	je	.L808
	cmpq	$0, 0(%r13)
	je	.L809
.L706:
	testq	%rbx, %rbx
	je	.L710
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r13, %rsi
	movl	$4, %edi
	call	*%rbx
	testl	%eax, %eax
	je	.L685
.L710:
	movq	24(%r12), %rax
	movq	16(%r12), %r15
	xorl	%r14d, %r14d
	testq	%rax, %rax
	jle	.L739
	movq	%rbx, -104(%rbp)
	movq	%r12, %rbx
	jmp	.L708
	.p2align 4,,10
	.p2align 3
.L711:
	addq	$1, %r14
	addq	$40, %r15
	cmpq	%rax, %r14
	jge	.L810
.L708:
	testq	$768, (%r15)
	je	.L711
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	asn1_do_adb@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L799
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	asn1_get_field_ptr@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	asn1_template_free@PLT
.L799:
	movq	24(%rbx), %rax
	jmp	.L711
	.p2align 4,,10
	.p2align 3
.L810:
	movq	%rbx, %r12
	movq	-104(%rbp), %rbx
	movq	16(%r12), %r15
	testq	%rax, %rax
	jle	.L739
	xorl	%eax, %eax
	movq	%rbx, -144(%rbp)
	movq	%rax, %rbx
	jmp	.L720
	.p2align 4,,10
	.p2align 3
.L714:
	movq	24(%r12), %rax
	xorl	%r8d, %r8d
	subq	$1, %rax
	cmpq	%rbx, %rax
	je	.L717
	movl	(%r14), %r8d
	andl	$1, %r8d
.L717:
	subq	$8, %rsp
	movq	-88(%rbp), %r9
	movq	-112(%rbp), %rsi
	movq	%r14, %rcx
	pushq	$1
	movq	%r11, -128(%rbp)
	movq	%rdi, -104(%rbp)
	call	asn1_template_ex_d2i
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	je	.L694
	cmpl	$-1, %eax
	movq	-104(%rbp), %rdi
	movq	-128(%rbp), %r11
	je	.L811
	subq	-64(%rbp), %r11
	addq	%r11, -72(%rbp)
	movl	-120(%rbp), %eax
.L803:
	addl	$1, %eax
	addq	$40, %r15
	addq	$1, %rbx
	movl	%eax, -104(%rbp)
	cmpq	%rbx, 24(%r12)
	jle	.L812
.L720:
	movl	$1, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	%ebx, -120(%rbp)
	movl	%ebx, -104(%rbp)
	call	asn1_do_adb@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L677
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	asn1_get_field_ptr@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, %rdi
	testq	%rdx, %rdx
	je	.L713
	movq	-64(%rbp), %r11
	cmpq	$1, %rdx
	jle	.L714
	cmpb	$0, (%r11)
	jne	.L714
	cmpb	$0, 1(%r11)
	jne	.L714
	addq	$2, %r11
	cmpb	$0, -78(%rbp)
	movq	-144(%rbp), %rbx
	movq	%r11, -64(%rbp)
	je	.L813
	subq	$2, %rdx
	movb	$0, -78(%rbp)
	movq	%rdx, -72(%rbp)
.L716:
	cmpb	$0, -132(%rbp)
	jne	.L734
	cmpq	$0, -72(%rbp)
	jne	.L722
.L734:
	movslq	-104(%rbp), %rax
	movq	%rax, %rsi
.L723:
	movslq	-104(%rbp), %rax
	movq	%rax, %rdi
	cmpq	%rsi, 24(%r12)
	jle	.L729
	movq	%rbx, -88(%rbp)
	movq	%rdi, %r14
	jmp	.L726
	.p2align 4,,10
	.p2align 3
.L815:
	movq	%rax, %rsi
	movq	%r13, %rdi
	addq	$40, %r15
	addq	$1, %r14
	call	asn1_get_field_ptr@PLT
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	asn1_template_free@PLT
	cmpq	%r14, 24(%r12)
	jle	.L814
.L726:
	movl	$1, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	asn1_do_adb@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L677
	testb	$1, (%rax)
	jne	.L815
	movl	$406, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$121, %edx
	movq	%rax, %r14
	movl	$120, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
.L694:
	movq	24(%r14), %rdx
	movq	48(%r12), %r8
	xorl	%eax, %eax
	movl	$4, %edi
	leaq	.LC1(%rip), %rcx
	leaq	.LC2(%rip), %rsi
	call	ERR_add_error_data@PLT
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L665:
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, -88(%rbp)
	call	ASN1_item_ex_free@PLT
	movl	-88(%rbp), %eax
.L663:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L816
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L667:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L665
.L671:
	pushq	-88(%rbp)
	movq	-96(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r13, %rdi
	pushq	%rdx
	movq	%r14, %rdx
	call	*32(%r10)
	popq	%r8
	popq	%r9
.L678:
	testl	%eax, %eax
	jg	.L663
	jmp	.L665
.L670:
	movq	-96(%rbp), %rax
	subq	$8, %rsp
	xorl	%r8d, %r8d
	leaq	-79(%rbp), %rdx
	leaq	-76(%rbp), %rsi
	leaq	-64(%rbp), %r9
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	movq	(%rax), %rax
	pushq	-88(%rbp)
	pushq	$1
	pushq	$0
	pushq	$-1
	pushq	%r14
	movq	%rax, -64(%rbp)
	call	asn1_check_tlen
	addq	$48, %rsp
	movl	$190, %r8d
	testl	%eax, %eax
	je	.L801
	cmpb	$0, -79(%rbp)
	jne	.L817
	movl	-76(%rbp), %r8d
	cmpl	$30, %r8d
	ja	.L682
	movslq	%r8d, %rax
	leaq	tag2bit(%rip), %rdx
	movq	(%rdx,%rax,8), %rax
	testq	%rax, 8(%r12)
	je	.L682
	pushq	-88(%rbp)
	movq	-72(%rbp), %rdx
	xorl	%r9d, %r9d
	movq	%r12, %rcx
	movq	-96(%rbp), %rsi
	pushq	$0
	movq	%r13, %rdi
	call	asn1_d2i_ex_primitive
	popq	%r10
	popq	%r11
	jmp	.L678
.L673:
	movq	16(%r12), %rcx
	testq	%rcx, %rcx
	je	.L674
	cmpl	$-1, %r8d
	jne	.L743
	testb	%r15b, %r15b
	jne	.L743
	subq	$8, %rsp
	movq	-88(%rbp), %r9
	movq	-96(%rbp), %rsi
	movq	%r14, %rdx
	pushq	$1
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	call	asn1_template_ex_d2i
	popq	%r15
	popq	%rdx
	jmp	.L678
	.p2align 4,,10
	.p2align 3
.L672:
	testq	%rbx, %rbx
	je	.L684
	xorl	%ecx, %ecx
	movq	%rdi, %rsi
	movq	%r12, %rdx
	movl	$4, %edi
	call	*%rbx
	testl	%eax, %eax
	jne	.L684
.L685:
	movl	$422, %r8d
	movl	$100, %edx
	movl	$120, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L677:
	movq	48(%r12), %rdx
	xorl	%eax, %eax
	movl	$2, %edi
	leaq	.LC3(%rip), %rsi
	call	ERR_add_error_data@PLT
	xorl	%eax, %eax
	jmp	.L665
.L689:
	movl	$230, %r8d
	.p2align 4,,10
	.p2align 3
.L801:
	movl	$58, %edx
	movl	$120, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L677
	.p2align 4,,10
	.p2align 3
.L817:
	testb	%r15b, %r15b
	jne	.L681
	movl	$199, %r8d
	movl	$139, %edx
	movl	$120, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L677
	.p2align 4,,10
	.p2align 3
.L806:
	xorl	%r9d, %r9d
	movl	$16, %r8d
	jmp	.L701
	.p2align 4,,10
	.p2align 3
.L696:
	movl	-132(%rbp), %esi
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	asn1_set_choice_selector@PLT
	testq	%rbx, %rbx
	je	.L700
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r13, %rsi
	movl	$5, %edi
	call	*%rbx
	testl	%eax, %eax
	je	.L685
.L700:
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rsi
	movq	%rax, (%rsi)
	movl	$1, %eax
	jmp	.L663
	.p2align 4,,10
	.p2align 3
.L807:
	movq	-96(%rbp), %rsi
	movq	-64(%rbp), %rax
	movb	$1, -132(%rbp)
	subq	(%rsi), %rax
	subq	%rax, %r14
	movq	%r14, -72(%rbp)
	jmp	.L704
	.p2align 4,,10
	.p2align 3
.L686:
	call	ASN1_item_ex_new@PLT
	testl	%eax, %eax
	je	.L689
	movq	24(%r12), %rdx
	movq	16(%r12), %r14
	jmp	.L688
	.p2align 4,,10
	.p2align 3
.L743:
	movl	$174, %r8d
	movl	$170, %edx
	movl	$120, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L677
.L739:
	movl	$0, -104(%rbp)
	xorl	%esi, %esi
.L709:
	cmpb	$0, -78(%rbp)
	je	.L716
	cmpq	$1, -72(%rbp)
	jle	.L721
	movq	-64(%rbp), %rax
	cmpb	$0, (%rax)
	jne	.L721
	cmpb	$0, 1(%rax)
	je	.L818
.L721:
	movl	$381, %r8d
	movl	$137, %edx
	movl	$120, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L677
	.p2align 4,,10
	.p2align 3
.L682:
	testb	%r15b, %r15b
	jne	.L681
	movl	$207, %r8d
	movl	$140, %edx
	movl	$120, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L677
	.p2align 4,,10
	.p2align 3
.L811:
	movq	%r14, %rsi
	call	asn1_template_free@PLT
	movl	%ebx, %eax
	jmp	.L803
	.p2align 4,,10
	.p2align 3
.L674:
	pushq	-88(%rbp)
	movq	-96(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r13, %rdi
	pushq	%rdx
	movq	%r14, %rdx
	call	asn1_d2i_ex_primitive
	popq	%rbx
	popq	%r14
	jmp	.L678
	.p2align 4,,10
	.p2align 3
.L804:
	movq	24(%r12), %rdx
	leal	1(%r14), %eax
	addq	$1, %r14
	addq	$40, %rbx
	movl	%eax, -132(%rbp)
	movq	%r14, -104(%rbp)
	cmpq	%r14, %rdx
	jg	.L692
	movq	-120(%rbp), %rbx
	movl	-128(%rbp), %r15d
	jmp	.L690
	.p2align 4,,10
	.p2align 3
.L808:
	movl	$302, %r8d
	movl	$149, %edx
	movl	$120, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L677
	.p2align 4,,10
	.p2align 3
.L809:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	ASN1_item_ex_new@PLT
	movl	$307, %r8d
	testl	%eax, %eax
	jne	.L706
	jmp	.L801
	.p2align 4,,10
	.p2align 3
.L693:
	movq	%r14, %rsi
	call	asn1_template_free@PLT
	movl	$253, %r8d
	movl	$58, %edx
	leaq	.LC0(%rip), %rcx
	movl	$120, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
	testq	%r14, %r14
	je	.L677
	jmp	.L694
.L814:
	movq	-88(%rbp), %rbx
.L729:
	movq	-96(%rbp), %rax
	movq	-64(%rbp), %rdx
	movq	%r12, %rcx
	movq	%r13, %rdi
	movq	(%rax), %rsi
	subq	%rsi, %rdx
	call	asn1_enc_save@PLT
	testl	%eax, %eax
	je	.L685
	testq	%rbx, %rbx
	je	.L731
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r13, %rsi
	movl	$5, %edi
	call	*%rbx
	testl	%eax, %eax
	je	.L685
.L731:
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rbx
	movq	%rax, (%rbx)
	movl	$1, %eax
	jmp	.L663
.L813:
	movl	$341, %r8d
	movl	$159, %edx
	movl	$120, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L677
.L713:
	cmpb	$0, -78(%rbp)
	movq	-144(%rbp), %rbx
	jne	.L721
	jmp	.L734
	.p2align 4,,10
	.p2align 3
.L812:
	movq	%rbx, %rsi
	movq	-144(%rbp), %rbx
	jmp	.L709
.L737:
	movq	$0, -104(%rbp)
	movl	$0, -132(%rbp)
	jmp	.L690
.L681:
	movl	$-1, %eax
	jmp	.L665
.L818:
	addq	$2, %rax
	cmpb	$0, -132(%rbp)
	movq	%rax, -64(%rbp)
	jne	.L723
.L722:
	movl	$386, %r8d
	movl	$148, %edx
	movl	$120, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L677
.L805:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	ASN1_item_ex_free@PLT
	orl	$-1, %eax
	jmp	.L665
.L816:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE456:
	.size	ASN1_item_ex_d2i, .-ASN1_item_ex_d2i
	.section	.rodata
	.align 32
	.type	tag2bit, @object
	.size	tag2bit, 256
tag2bit:
	.quad	0
	.quad	0
	.quad	0
	.quad	1024
	.quad	512
	.quad	0
	.quad	0
	.quad	4096
	.quad	4096
	.quad	4096
	.quad	4096
	.quad	4096
	.quad	8192
	.quad	4096
	.quad	4096
	.quad	4096
	.quad	65536
	.quad	0
	.quad	1
	.quad	2
	.quad	4
	.quad	8
	.quad	16
	.quad	16384
	.quad	32768
	.quad	32
	.quad	64
	.quad	128
	.quad	256
	.quad	4096
	.quad	2048
	.quad	4096
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
