	.file	"scrypt.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/kdf/scrypt.c"
	.text
	.p2align 4
	.type	pkey_scrypt_cleanup, @function
pkey_scrypt_cleanup:
.LFB447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$84, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	40(%rdi), %r12
	movq	24(%r12), %rsi
	movq	16(%r12), %rdi
	call	CRYPTO_clear_free@PLT
	movq	8(%r12), %rsi
	movq	(%r12), %rdi
	leaq	.LC0(%rip), %rdx
	movl	$85, %ecx
	call	CRYPTO_clear_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$86, %edx
	popq	%r12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE447:
	.size	pkey_scrypt_cleanup, .-pkey_scrypt_cleanup
	.p2align 4
	.type	pkey_scrypt_derive, @function
pkey_scrypt_derive:
.LFB453:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	40(%rdi), %rax
	movq	(%rax), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rdi, %rdi
	je	.L9
	movq	16(%rax), %r10
	testq	%r10, %r10
	je	.L10
	movq	8(%rax), %r11
	movq	40(%rax), %r9
	movq	32(%rax), %r8
	movq	24(%rax), %rcx
	pushq	(%rdx)
	movq	%r10, %rdx
	pushq	%rsi
	movq	%r11, %rsi
	pushq	56(%rax)
	pushq	48(%rax)
	call	EVP_PBE_scrypt@PLT
	addq	$32, %rsp
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movl	$227, %r8d
	movl	$111, %edx
	movl	$109, %esi
	movl	$52, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	movl	$222, %r8d
	movl	$110, %edx
	movl	$109, %esi
	movl	$52, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE453:
	.size	pkey_scrypt_derive, .-pkey_scrypt_derive
	.p2align 4
	.type	pkey_scrypt_init, @function
pkey_scrypt_init:
.LFB446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$60, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$64, %edi
	subq	$8, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L15
	movdqa	.LC1(%rip), %xmm0
	movups	%xmm0, 32(%rax)
	movdqa	.LC2(%rip), %xmm0
	movups	%xmm0, 48(%rax)
	movq	%rax, 40(%rbx)
	movl	$1, %eax
.L11:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	movl	$62, %r8d
	movl	$65, %edx
	movl	$106, %esi
	movl	$52, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L11
	.cfi_endproc
.LFE446:
	.size	pkey_scrypt_init, .-pkey_scrypt_init
	.section	.text.unlikely,"ax",@progbits
.LCOLDB3:
	.text
.LHOTB3:
	.p2align 4
	.type	pkey_scrypt_ctrl, @function
pkey_scrypt_ctrl:
.LFB450:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subl	$4104, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	40(%rdi), %r13
	cmpl	$5, %esi
	ja	.L33
	movslq	%edx, %rbx
	leaq	.L19(%rip), %rdx
	movq	%rcx, %r12
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L19:
	.long	.L24-.L19
	.long	.L23-.L19
	.long	.L22-.L19
	.long	.L21-.L19
	.long	.L20-.L19
	.long	.L18-.L19
	.text
	.p2align 4,,10
	.p2align 3
.L20:
	movq	(%rcx), %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L16
	movq	%rdx, 48(%r13)
	movl	$1, %eax
.L16:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	movq	(%rcx), %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L16
	movq	%rdx, 56(%r13)
	movl	$1, %eax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L24:
	testq	%rcx, %rcx
	je	.L36
	xorl	%eax, %eax
	testl	%ebx, %ebx
	js	.L16
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L25
	movq	8(%r13), %rsi
	movl	$100, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
.L25:
	testl	%ebx, %ebx
	jne	.L56
	movl	$105, %edx
	leaq	.LC0(%rip), %rsi
	movl	$1, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, 0(%r13)
.L27:
	testq	%rax, %rax
	je	.L32
	movq	%rbx, 8(%r13)
	movl	$1, %eax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L23:
	testq	%rcx, %rcx
	je	.L36
	xorl	%eax, %eax
	testl	%ebx, %ebx
	js	.L16
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.L29
	movq	24(%r13), %rsi
	movl	$100, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
.L29:
	testl	%ebx, %ebx
	jne	.L57
	movl	$105, %edx
	leaq	.LC0(%rip), %rsi
	movl	$1, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, 16(%r13)
.L31:
	testq	%rax, %rax
	je	.L32
	movq	%rbx, 24(%r13)
	movl	$1, %eax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L22:
	movq	(%rcx), %rdx
	xorl	%eax, %eax
	cmpq	$1, %rdx
	jbe	.L16
	leaq	-1(%rdx), %rcx
	testq	%rdx, %rcx
	jne	.L16
	movq	%rdx, 32(%r13)
	movl	$1, %eax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L21:
	movq	(%rcx), %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L16
	movq	%rdx, 40(%r13)
	movl	$1, %eax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$1, %eax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L56:
	movslq	%ebx, %rsi
	movl	$103, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r12, %rdi
	call	CRYPTO_memdup@PLT
	movq	%rax, 0(%r13)
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L57:
	movslq	%ebx, %rsi
	movl	$103, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r12, %rdi
	call	CRYPTO_memdup@PLT
	movq	%rax, 16(%r13)
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L32:
	movl	$108, %r8d
	movl	$65, %edx
	movl	$107, %esi
	movl	$52, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L16
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	pkey_scrypt_ctrl.cold, @function
pkey_scrypt_ctrl.cold:
.LFSB450:
.L33:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	$-2, %eax
	jmp	.L16
	.cfi_endproc
.LFE450:
	.text
	.size	pkey_scrypt_ctrl, .-pkey_scrypt_ctrl
	.section	.text.unlikely
	.size	pkey_scrypt_ctrl.cold, .-pkey_scrypt_ctrl.cold
.LCOLDE3:
	.text
.LHOTE3:
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC4:
	.string	"pass"
.LC5:
	.string	"hexpass"
.LC6:
	.string	"salt"
.LC7:
	.string	"hexsalt"
.LC8:
	.string	"maxmem_bytes"
	.text
	.p2align 4
	.type	pkey_scrypt_ctrl_str, @function
pkey_scrypt_ctrl_str:
.LFB452:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rdx, %rdx
	je	.L111
	movq	%rdi, %r9
	movl	$5, %ecx
	movq	%rsi, %rax
	movq	%rdx, %r8
	leaq	.LC4(%rip), %rdi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L112
	movl	$8, %ecx
	leaq	.LC5(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L113
	movl	$5, %ecx
	leaq	.LC6(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L114
	movl	$8, %ecx
	leaq	.LC7(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L115
	movzbl	(%rax), %edx
	cmpl	$78, %edx
	jne	.L66
	movzbl	1(%rax), %r10d
	testl	%r10d, %r10d
	jne	.L66
	movsbl	(%r8), %eax
	testb	%al, %al
	je	.L58
	movabsq	$-3689348814741910323, %rsi
	xorl	%ecx, %ecx
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L117:
	subl	$48, %eax
	leaq	(%rcx,%rcx,4), %rdx
	addq	%rdx, %rdx
	cltq
	addq	%rdx, %rax
	movq	%rax, %rdi
	jc	.L67
	movq	%rdx, %rax
	mulq	%rsi
	shrq	$3, %rdx
	cmpq	%rcx, %rdx
	jne	.L67
	movsbl	1(%r8), %eax
	addq	$1, %r8
	testb	%al, %al
	je	.L116
	movq	%rdi, %rcx
.L70:
	leal	-48(%rax), %edx
	cmpb	$9, %dl
	jbe	.L117
	.p2align 4,,10
	.p2align 3
.L67:
	movl	$173, %r8d
	movl	$108, %edx
	movl	$105, %esi
	movl	$52, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r10d, %r10d
.L58:
	movl	%r10d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	cmpl	$114, %edx
	jne	.L72
	movzbl	1(%rax), %r10d
	testl	%r10d, %r10d
	jne	.L72
	movsbl	(%r8), %eax
	testb	%al, %al
	je	.L58
	movabsq	$-3689348814741910323, %rsi
	xorl	%ecx, %ecx
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L119:
	subl	$48, %eax
	leaq	(%rcx,%rcx,4), %rdx
	addq	%rdx, %rdx
	cltq
	addq	%rdx, %rax
	movq	%rax, %rdi
	jc	.L67
	movq	%rdx, %rax
	mulq	%rsi
	shrq	$3, %rdx
	cmpq	%rcx, %rdx
	jne	.L67
	movsbl	1(%r8), %eax
	addq	$1, %r8
	testb	%al, %al
	je	.L118
	movq	%rdi, %rcx
.L75:
	leal	-48(%rax), %edx
	cmpb	$9, %dl
	jbe	.L119
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L72:
	cmpl	$112, %edx
	je	.L120
.L77:
	movl	$13, %ecx
	leaq	.LC8(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%r10b
	sbbb	$0, %r10b
	movsbl	%r10b, %r10d
	testl	%r10d, %r10d
	jne	.L81
	movsbl	(%r8), %eax
	testb	%al, %al
	je	.L58
	movabsq	$-3689348814741910323, %rsi
	xorl	%ecx, %ecx
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L122:
	subl	$48, %eax
	leaq	(%rcx,%rcx,4), %rdx
	addq	%rdx, %rdx
	cltq
	addq	%rdx, %rax
	movq	%rax, %rdi
	jc	.L67
	movq	%rdx, %rax
	mulq	%rsi
	shrq	$3, %rdx
	cmpq	%rcx, %rdx
	jne	.L67
	movsbl	1(%r8), %eax
	addq	$1, %r8
	testb	%al, %al
	je	.L121
	movq	%rdi, %rcx
.L84:
	leal	-48(%rax), %edx
	cmpb	$9, %dl
	jbe	.L122
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L114:
	movq	%r8, %rdx
	movl	$4105, %esi
	movq	%r9, %rdi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	EVP_PKEY_CTX_str2ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore_state
	movzbl	1(%rax), %r10d
	testl	%r10d, %r10d
	jne	.L77
	movsbl	(%r8), %eax
	testb	%al, %al
	je	.L58
	movabsq	$-3689348814741910323, %rsi
	xorl	%ecx, %ecx
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L124:
	subl	$48, %eax
	leaq	(%rcx,%rcx,4), %rdx
	addq	%rdx, %rdx
	cltq
	addq	%rdx, %rax
	movq	%rax, %rdi
	jc	.L67
	movq	%rdx, %rax
	mulq	%rsi
	shrq	$3, %rdx
	cmpq	%rcx, %rdx
	jne	.L67
	movsbl	1(%r8), %eax
	addq	$1, %r8
	testb	%al, %al
	je	.L123
	movq	%rdi, %rcx
.L80:
	leal	-48(%rax), %edx
	cmpb	$9, %dl
	jbe	.L124
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L112:
	movq	%r8, %rdx
	movl	$4104, %esi
	movq	%r9, %rdi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	EVP_PKEY_CTX_str2ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore_state
	movq	%r8, %rdx
	movl	$4104, %esi
	movq	%r9, %rdi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	EVP_PKEY_CTX_hex2ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	movl	$183, %r8d
	movl	$102, %edx
	movl	$104, %esi
	movl	$52, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r10d, %r10d
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L115:
	movq	%r8, %rdx
	movl	$4105, %esi
	movq	%r9, %rdi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	EVP_PKEY_CTX_hex2ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	testq	%rdi, %rdi
	je	.L58
	movq	40(%r9), %rax
	movl	$1, %r10d
	movq	%rdi, 56(%rax)
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L116:
	cmpq	$1, %rdi
	jbe	.L58
	leaq	-1(%rdi), %rax
	testq	%rdi, %rax
	jne	.L58
	movq	40(%r9), %rax
	movl	$1, %r10d
	movq	%rdi, 32(%rax)
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L118:
	testq	%rdi, %rdi
	je	.L58
	movq	40(%r9), %rax
	movl	$1, %r10d
	movq	%rdi, 40(%rax)
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L123:
	testq	%rdi, %rdi
	je	.L58
	movq	40(%r9), %rax
	movl	$1, %r10d
	movq	%rdi, 48(%rax)
	jmp	.L58
.L81:
	movl	$212, %r8d
	movl	$103, %edx
	movl	$104, %esi
	movl	$52, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %r10d
	jmp	.L58
	.cfi_endproc
.LFE452:
	.size	pkey_scrypt_ctrl_str, .-pkey_scrypt_ctrl_str
	.globl	scrypt_pkey_meth
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	scrypt_pkey_meth, @object
	.size	scrypt_pkey_meth, 256
scrypt_pkey_meth:
	.long	973
	.long	0
	.quad	pkey_scrypt_init
	.quad	0
	.quad	pkey_scrypt_cleanup
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	pkey_scrypt_derive
	.quad	pkey_scrypt_ctrl
	.quad	pkey_scrypt_ctrl_str
	.zero	48
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.quad	1048576
	.quad	8
	.align 16
.LC2:
	.quad	1
	.quad	1074790400
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
