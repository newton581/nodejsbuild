	.file	"buf_err.c"
	.text
	.p2align 4
	.globl	ERR_load_BUF_strings
	.type	ERR_load_BUF_strings, @function
ERR_load_BUF_strings:
.LFB203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$117850112, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_func_error_string@PLT
	testq	%rax, %rax
	je	.L5
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	leaq	BUF_str_functs(%rip), %rdi
	call	ERR_load_strings_const@PLT
	leaq	BUF_str_reasons(%rip), %rdi
	call	ERR_load_strings_const@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE203:
	.size	ERR_load_BUF_strings, .-ERR_load_BUF_strings
	.section	.rodata
	.align 16
	.type	BUF_str_reasons, @object
	.size	BUF_str_reasons, 16
BUF_str_reasons:
	.zero	16
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"BUF_MEM_grow"
.LC1:
	.string	"BUF_MEM_grow_clean"
.LC2:
	.string	"BUF_MEM_new"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	BUF_str_functs, @object
	.size	BUF_str_functs, 64
BUF_str_functs:
	.quad	117850112
	.quad	.LC0
	.quad	117870592
	.quad	.LC1
	.quad	117854208
	.quad	.LC2
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
