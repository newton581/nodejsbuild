	.file	"rec_layer_d1.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/ssl/record/rec_layer_d1.c"
	.text
	.p2align 4
	.type	dtls1_buffer_record.part.0, @function
dtls1_buffer_record.part.0:
.LFB1019:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	movl	$148, %edx
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movl	$136, %edi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	leaq	.LC0(%rip), %rsi
	subq	$16, %rsp
	call	CRYPTO_malloc@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	pitem_new@PLT
	movq	%rax, %r14
	testq	%r13, %r13
	je	.L7
	testq	%rax, %rax
	je	.L7
	movq	6024(%r12), %rax
	leaq	3472(%r12), %rdi
	pxor	%xmm0, %xmm0
	andq	$-8, %rdi
	movq	%rax, 0(%r13)
	movq	6032(%r12), %rax
	movq	%rax, 8(%r13)
	xorl	%eax, %eax
	movdqu	2144(%r12), %xmm1
	movups	%xmm1, 16(%r13)
	movdqu	2160(%r12), %xmm2
	movups	%xmm2, 32(%r13)
	movq	2176(%r12), %rcx
	movq	%rcx, 48(%r13)
	leaq	3464(%r12), %rcx
	movdqu	3464(%r12), %xmm3
	subq	%rdi, %rcx
	movups	%xmm3, 56(%r13)
	addl	$2560, %ecx
	movdqu	3480(%r12), %xmm4
	shrl	$3, %ecx
	movups	%xmm4, 72(%r13)
	movdqu	3496(%r12), %xmm5
	movups	%xmm5, 88(%r13)
	movdqu	3512(%r12), %xmm6
	movups	%xmm6, 104(%r13)
	movdqu	3528(%r12), %xmm7
	movups	%xmm7, 120(%r13)
	movq	%r13, 8(%r14)
	movq	$0, 6024(%r12)
	movq	$0, 6032(%r12)
	movq	$0, 2176(%r12)
	movq	$0, 3464(%r12)
	movq	$0, 6016(%r12)
	movups	%xmm0, 2144(%r12)
	movups	%xmm0, 2160(%r12)
	rep stosq
	movq	%r12, %rdi
	call	ssl3_setup_buffers@PLT
	testl	%eax, %eax
	je	.L10
	movq	8(%rbx), %rdi
	movq	%r14, %rsi
	call	pqueue_insert@PLT
	movq	%rax, %r8
	movl	$1, %eax
	testq	%r8, %r8
	je	.L11
.L1:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movq	16(%r13), %rdi
	movl	$182, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r13, %rdi
	movl	$183, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r14, %rdi
	call	pitem_free@PLT
	addq	$16, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	movl	$151, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	movq	%r14, %rdi
	call	pitem_free@PLT
	movq	%r12, %rdi
	movl	$153, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	$247, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	addq	$16, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movq	16(%r13), %rdi
	movl	$190, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, -36(%rbp)
	call	CRYPTO_free@PLT
	movl	$191, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	movq	%r14, %rdi
	call	pitem_free@PLT
	movl	-36(%rbp), %eax
	jmp	.L1
	.cfi_endproc
.LFE1019:
	.size	dtls1_buffer_record.part.0, .-dtls1_buffer_record.part.0
	.p2align 4
	.type	do_dtls1_write.part.0, @function
do_dtls1_write.part.0:
.LFB1020:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -160(%rbp)
	movl	%r8d, -172(%rbp)
	movq	%r9, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	ssl_get_max_send_fragment@PLT
	movl	%eax, %eax
	cmpq	%r12, %rax
	jb	.L59
	cmpq	$0, 1296(%r15)
	je	.L15
	cmpq	$0, 1136(%r15)
	je	.L15
	movq	1160(%r15), %rdi
	call	EVP_MD_CTX_md@PLT
	testq	%rax, %rax
	je	.L15
	movq	1160(%r15), %rdi
	call	EVP_MD_CTX_md@PLT
	movq	%rax, %rdi
	call	EVP_MD_size@PLT
	movl	%eax, -152(%rbp)
	testl	%eax, %eax
	jns	.L16
	movl	$848, %r9d
	leaq	.LC0(%rip), %r8
	movl	$194, %ecx
.L58:
	movl	$245, %edx
	movl	$80, %esi
	movq	%r15, %rdi
	call	ossl_statem_fatal@PLT
	movl	$-1, %eax
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L15:
	movl	$0, -152(%rbp)
.L16:
	movq	2184(%r15), %rbx
	movb	%r13b, (%rbx)
	movq	8(%r15), %rax
	movl	%r13d, -140(%rbp)
	cmpl	$131071, (%rax)
	je	.L60
.L17:
	movl	(%r15), %eax
	movb	%ah, 1(%rbx)
	movl	(%r15), %eax
	movb	%al, 2(%rbx)
.L18:
	movq	1136(%r15), %rdi
	leaq	13(%rbx), %rax
	movq	%rax, -168(%rbp)
	testq	%rdi, %rdi
	je	.L34
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpl	$2, %eax
	je	.L61
	subl	$6, %eax
	cmpl	$1, %eax
	ja	.L34
	movq	$8, -192(%rbp)
	leaq	21(%rbx), %rdi
	movl	$8, -176(%rbp)
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L34:
	movq	$0, -192(%rbp)
	leaq	13(%rbx), %rdi
	movl	$0, -176(%rbp)
.L19:
	movq	%rdi, %xmm0
	cmpq	$0, 1120(%r15)
	movq	%r12, -136(%rbp)
	movhps	-160(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	je	.L21
	leaq	-144(%rbp), %r14
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	ssl3_do_compress@PLT
	testl	%eax, %eax
	je	.L62
.L22:
	movq	168(%r15), %rdx
	movl	-152(%rbp), %r8d
	movq	8(%r15), %rax
	movq	(%rdx), %rdx
	testl	%r8d, %r8d
	setne	-148(%rbp)
	movq	192(%rax), %rax
	movzbl	-148(%rbp), %ecx
	andb	$4, %dh
	jne	.L23
	testb	%cl, %cl
	jne	.L63
.L23:
	movq	-168(%rbp), %xmm0
	movl	-176(%rbp), %edi
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -112(%rbp)
	testl	%edi, %edi
	jne	.L64
.L25:
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	*(%rax)
	testl	%eax, %eax
	jle	.L65
	movq	168(%r15), %rax
	movq	(%rax), %rax
	testb	$4, %ah
	je	.L27
	cmpb	$0, -148(%rbp)
	je	.L27
	movq	8(%r15), %rax
	leaq	13(%rbx), %rdx
	movl	$1, %ecx
	movq	%r14, %rsi
	addq	-136(%rbp), %rdx
	movq	%r15, %rdi
	movq	192(%rax), %rax
	call	*8(%rax)
	testl	%eax, %eax
	je	.L66
	movslq	-152(%rbp), %r14
	addq	%r14, -136(%rbp)
.L27:
	movq	6128(%r15), %rax
	movzbl	3(%rax), %eax
	movb	%al, 3(%rbx)
	movq	6128(%r15), %rax
	movzwl	2(%rax), %eax
	movb	%al, 4(%rbx)
	movl	6114(%r15), %eax
	movl	%eax, 5(%rbx)
	movzwl	6118(%r15), %eax
	movw	%ax, 9(%rbx)
	movq	-136(%rbp), %rax
	shrq	$8, %rax
	movb	%al, 11(%rbx)
	movq	-136(%rbp), %rax
	movb	%al, 12(%rbx)
	movq	184(%r15), %rax
	testq	%rax, %rax
	je	.L29
	subq	$8, %rsp
	movq	%rbx, %rcx
	pushq	192(%r15)
	xorl	%esi, %esi
	movq	%r15, %r9
	movl	$13, %r8d
	movl	$256, %edx
	movl	$1, %edi
	call	*%rax
	popq	%rcx
	popq	%rsi
.L29:
	leaq	6112(%r15), %rdi
	addq	$13, -136(%rbp)
	movl	%r13d, -140(%rbp)
	call	ssl3_record_sequence_update@PLT
	movl	-172(%rbp), %edx
	movq	-136(%rbp), %rax
	testl	%edx, %edx
	je	.L30
	movq	-184(%rbp), %rsi
	movq	%rax, (%rsi)
	movl	$1, %eax
.L12:
	movq	-56(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L67
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
	movq	-192(%rbp), %rsi
	addq	%rsi, -136(%rbp)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L30:
	movq	%rax, 2216(%r15)
	movq	%r12, %rcx
	movl	%r13d, %esi
	movq	%r15, %rdi
	movq	-160(%rbp), %rax
	movq	%r12, 6072(%r15)
	movl	%r13d, 6080(%r15)
	movq	-184(%rbp), %r8
	movq	$0, 2208(%r15)
	movq	%rax, %rdx
	movq	%rax, 6096(%r15)
	movq	%r12, 6088(%r15)
	call	ssl3_write_pending@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L60:
	cmpl	$256, 1504(%r15)
	je	.L17
	movl	$-2, %r9d
	movw	%r9w, 1(%rbx)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L21:
	movq	-160(%rbp), %rsi
	movq	%r12, %rdx
	leaq	-144(%rbp), %r14
	call	memcpy@PLT
	movq	-112(%rbp), %rax
	movq	%rax, -104(%rbp)
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L59:
	movl	$832, %r9d
	leaq	.LC0(%rip), %r8
	movl	$194, %ecx
	movq	%r15, %rdi
	movl	$245, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L63:
	movq	-192(%rbp), %rdx
	movl	$1, %ecx
	movq	%r14, %rsi
	movq	%r15, %rdi
	addq	-136(%rbp), %rdx
	addq	-168(%rbp), %rdx
	call	*8(%rax)
	testl	%eax, %eax
	je	.L68
	movslq	-152(%rbp), %rax
	addq	%rax, -136(%rbp)
	movq	8(%r15), %rax
	movq	192(%rax), %rax
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L61:
	movq	1136(%r15), %rdi
	call	EVP_CIPHER_CTX_iv_length@PLT
	movl	%eax, -176(%rbp)
	cmpl	$1, %eax
	jle	.L34
	cltq
	leaq	13(%rbx), %rdi
	movq	%rax, -192(%rbp)
	addq	%rax, %rdi
	jmp	.L19
.L62:
	movl	$908, %r9d
	leaq	.LC0(%rip), %r8
	movl	$141, %ecx
	jmp	.L58
.L65:
	movq	%r15, %rdi
	call	ossl_statem_in_error@PLT
	movl	%eax, %r8d
	movl	$-1, %eax
	testl	%r8d, %r8d
	jne	.L12
	movl	$944, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r15, %rdi
	movl	$245, %edx
	movl	$80, %esi
	movl	%eax, -148(%rbp)
	call	ossl_statem_fatal@PLT
	movl	-148(%rbp), %eax
	jmp	.L12
.L66:
	movl	$953, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	jmp	.L58
.L68:
	movl	$928, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	jmp	.L58
.L67:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1020:
	.size	do_dtls1_write.part.0, .-do_dtls1_write.part.0
	.p2align 4
	.globl	DTLS_RECORD_LAYER_new
	.type	DTLS_RECORD_LAYER_new, @function
DTLS_RECORD_LAYER_new:
.LFB1001:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$23, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$104, %edi
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L80
	movq	%rax, 4016(%rbx)
	movq	%rax, %r12
	call	pqueue_new@PLT
	movq	%rax, 48(%r12)
	call	pqueue_new@PLT
	movq	%rax, 64(%r12)
	call	pqueue_new@PLT
	movq	48(%r12), %rdi
	movq	%rax, 80(%r12)
	testq	%rdi, %rdi
	je	.L72
	cmpq	$0, 64(%r12)
	je	.L72
	movl	$1, %r8d
	testq	%rax, %rax
	je	.L72
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore_state
	call	pqueue_free@PLT
	movq	64(%r12), %rdi
	call	pqueue_free@PLT
	movq	80(%r12), %rdi
	call	pqueue_free@PLT
	movq	%r12, %rdi
	movl	$39, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	xorl	%r8d, %r8d
	movq	$0, 4016(%rbx)
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	movl	$24, %r8d
	movl	$65, %edx
	movl	$635, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movl	%r8d, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1001:
	.size	DTLS_RECORD_LAYER_new, .-DTLS_RECORD_LAYER_new
	.p2align 4
	.globl	DTLS_RECORD_LAYER_clear
	.type	DTLS_RECORD_LAYER_clear, @function
DTLS_RECORD_LAYER_clear:
.LFB1003:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	.LC0(%rip), %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	4016(%rdi), %rbx
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L83:
	movq	8(%r12), %rax
	movl	$70, %edx
	movq	%r13, %rsi
	movq	16(%rax), %rdi
	call	CRYPTO_free@PLT
	movq	8(%r12), %rdi
	movl	$71, %edx
	movq	%r13, %rsi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	call	pitem_free@PLT
.L82:
	movq	48(%rbx), %rdi
	call	pqueue_pop@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L83
	leaq	.LC0(%rip), %r13
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L85:
	movq	8(%r12), %rax
	movl	$77, %edx
	movq	%r13, %rsi
	movq	16(%rax), %rdi
	call	CRYPTO_free@PLT
	movq	8(%r12), %rdi
	movl	$78, %edx
	movq	%r13, %rsi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	call	pitem_free@PLT
.L84:
	movq	64(%rbx), %rdi
	call	pqueue_pop@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L85
	leaq	.LC0(%rip), %r13
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L87:
	movq	8(%r12), %rax
	movl	$84, %edx
	movq	%r13, %rsi
	movq	16(%rax), %rdi
	call	CRYPTO_free@PLT
	movq	8(%r12), %rdi
	movl	$85, %edx
	movq	%r13, %rsi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	call	pitem_free@PLT
.L86:
	movq	80(%rbx), %rdi
	call	pqueue_pop@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L87
	leaq	8(%rbx), %rdi
	movq	%rbx, %rcx
	movq	48(%rbx), %r8
	movq	64(%rbx), %rsi
	andq	$-8, %rdi
	movq	80(%rbx), %rdx
	movq	$0, (%rbx)
	subq	%rdi, %rcx
	movq	$0, 96(%rbx)
	addl	$104, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	%r8, 48(%rbx)
	movq	%rsi, 64(%rbx)
	movq	%rdx, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1003:
	.size	DTLS_RECORD_LAYER_clear, .-DTLS_RECORD_LAYER_clear
	.p2align 4
	.globl	DTLS_RECORD_LAYER_free
	.type	DTLS_RECORD_LAYER_free, @function
DTLS_RECORD_LAYER_free:
.LFB1002:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	DTLS_RECORD_LAYER_clear
	movq	4016(%rbx), %rax
	movq	48(%rax), %rdi
	call	pqueue_free@PLT
	movq	4016(%rbx), %rax
	movq	64(%rax), %rdi
	call	pqueue_free@PLT
	movq	4016(%rbx), %rax
	movq	80(%rax), %rdi
	call	pqueue_free@PLT
	movq	4016(%rbx), %rdi
	movl	$53, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 4016(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1002:
	.size	DTLS_RECORD_LAYER_free, .-DTLS_RECORD_LAYER_free
	.p2align 4
	.globl	DTLS_RECORD_LAYER_set_saved_w_epoch
	.type	DTLS_RECORD_LAYER_set_saved_w_epoch, @function
DTLS_RECORD_LAYER_set_saved_w_epoch:
.LFB1004:
	.cfi_startproc
	endbr64
	movq	4016(%rdi), %rdx
	movzwl	%si, %ecx
	movzwl	2(%rdx), %eax
	leal	-1(%rax), %r8d
	cmpl	%r8d, %ecx
	je	.L94
	addl	$1, %eax
	cmpl	%eax, %ecx
	je	.L95
	movw	%si, 2(%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	movq	4000(%rdi), %rax
	movq	%rax, 96(%rdx)
	movq	4016(%rdi), %rdx
	movq	88(%rdx), %rax
	movq	%rax, 4000(%rdi)
	movw	%si, 2(%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	movq	4000(%rdi), %rax
	movq	%rax, 88(%rdx)
	movq	4016(%rdi), %rdx
	movq	96(%rdx), %rax
	movq	%rax, 4000(%rdi)
	movw	%si, 2(%rdx)
	ret
	.cfi_endproc
.LFE1004:
	.size	DTLS_RECORD_LAYER_set_saved_w_epoch, .-DTLS_RECORD_LAYER_set_saved_w_epoch
	.p2align 4
	.globl	DTLS_RECORD_LAYER_set_write_sequence
	.type	DTLS_RECORD_LAYER_set_write_sequence, @function
DTLS_RECORD_LAYER_set_write_sequence:
.LFB1005:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movq	%rax, 4000(%rdi)
	ret
	.cfi_endproc
.LFE1005:
	.size	DTLS_RECORD_LAYER_set_write_sequence, .-DTLS_RECORD_LAYER_set_write_sequence
	.p2align 4
	.globl	dtls1_buffer_record
	.type	dtls1_buffer_record, @function
dtls1_buffer_record:
.LFB1007:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$8, %rsp
	movq	8(%rsi), %rdi
	call	pqueue_size@PLT
	cmpq	$99, %rax
	jbe	.L100
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	dtls1_buffer_record.part.0
	.cfi_endproc
.LFE1007:
	.size	dtls1_buffer_record, .-dtls1_buffer_record
	.p2align 4
	.globl	dtls1_retrieve_buffered_record
	.type	dtls1_retrieve_buffered_record, @function
dtls1_retrieve_buffered_record:
.LFB1008:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	8(%rsi), %rdi
	call	pqueue_pop@PLT
	movq	%rax, %r12
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L101
	movq	8(%r12), %r13
	leaq	2144(%rbx), %r14
	movq	%r14, %rdi
	call	SSL3_BUFFER_release@PLT
	movq	0(%r13), %rax
	leaq	.LC0(%rip), %rsi
	movq	%rax, 6024(%rbx)
	movq	8(%r13), %rax
	movq	%rax, 6032(%rbx)
	movdqu	16(%r13), %xmm0
	movups	%xmm0, 2144(%rbx)
	movdqu	32(%r13), %xmm1
	movups	%xmm1, 2160(%rbx)
	movq	48(%r13), %rax
	movq	%rax, 2176(%rbx)
	movdqu	56(%r13), %xmm2
	movups	%xmm2, 3464(%rbx)
	movdqu	72(%r13), %xmm3
	movups	%xmm3, 3480(%rbx)
	movdqu	88(%r13), %xmm4
	movups	%xmm4, 3496(%rbx)
	movdqu	104(%r13), %xmm5
	movups	%xmm5, 3512(%rbx)
	movdqu	120(%r13), %xmm6
	movups	%xmm6, 3528(%rbx)
	movq	0(%r13), %rax
	movl	5(%rax), %edx
	movl	%edx, 6106(%rbx)
	movzwl	9(%rax), %eax
	movl	$206, %edx
	movw	%ax, 6110(%rbx)
	movq	8(%r12), %rdi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	call	pitem_free@PLT
	movl	$1, %eax
.L101:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1008:
	.size	dtls1_retrieve_buffered_record, .-dtls1_retrieve_buffered_record
	.p2align 4
	.globl	dtls1_process_buffered_records
	.type	dtls1_process_buffered_records, @function
dtls1_process_buffered_records:
.LFB1009:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	6128(%rdi), %rax
	movq	48(%rax), %rdi
	call	pqueue_peek@PLT
	testq	%rax, %rax
	je	.L122
	movq	6128(%r12), %rax
	movl	$1, %r8d
	movzwl	(%rax), %ebx
	cmpw	%bx, 40(%rax)
	jne	.L107
	cmpq	$0, 2176(%r12)
	leaq	3536(%r12), %rbx
	jne	.L107
.L110:
	movq	48(%rax), %rdi
	call	pqueue_peek@PLT
	testq	%rax, %rax
	je	.L122
	movq	6128(%r12), %rax
	movq	%r12, %rdi
	leaq	40(%rax), %rsi
	call	dtls1_retrieve_buffered_record
	movq	6128(%r12), %rsi
	movq	3528(%r12), %rdx
	movzwl	(%rsi), %ecx
	leaq	8(%rsi), %r13
	movq	%rcx, %rax
	cmpq	%rcx, %rdx
	je	.L112
	movzwl	%cx, %ecx
	addq	$1, %rcx
	cmpq	%rcx, %rdx
	jne	.L113
	cmpw	40(%rsi), %ax
	je	.L113
	movl	3468(%r12), %eax
	subl	$21, %eax
	cmpl	$1, %eax
	jbe	.L133
.L113:
	leaq	.LC0(%rip), %r8
	movl	$262, %r9d
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$424, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%r8d, %r8d
.L107:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_restore_state
	movq	6128(%r12), %rdx
	movl	$1, %r8d
	movzwl	(%rdx), %eax
	movw	%ax, 56(%rdx)
	addl	$1, %eax
	movw	%ax, 40(%rdx)
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore_state
	leaq	24(%rsi), %r13
.L112:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	dtls1_record_replay_check@PLT
	testl	%eax, %eax
	je	.L116
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	dtls1_process_record@PLT
	testl	%eax, %eax
	jne	.L134
.L116:
	movq	%r12, %rdi
	call	ossl_statem_in_error@PLT
	testl	%eax, %eax
	je	.L135
	movl	$-1, %r8d
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L135:
	movq	$0, 3472(%r12)
	movq	$0, 6032(%r12)
.L118:
	movq	6128(%r12), %rax
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L134:
	movq	6128(%r12), %r13
	movq	64(%r13), %rdi
	call	pqueue_size@PLT
	cmpq	$99, %rax
	ja	.L118
	leaq	56(%r13), %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	dtls1_buffer_record.part.0
	testl	%eax, %eax
	jns	.L118
	xorl	%r8d, %r8d
	jmp	.L107
	.cfi_endproc
.LFE1009:
	.size	dtls1_process_buffered_records, .-dtls1_process_buffered_records
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"%d"
.LC2:
	.string	"SSL alert number "
	.text
	.p2align 4
	.globl	dtls1_read_bytes
	.type	dtls1_read_bytes, @function
dtls1_read_bytes:
.LFB1010:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$200, %rsp
	movq	16(%rbp), %rax
	movq	%rcx, -216(%rbp)
	movq	%r8, -208(%rbp)
	movl	%r9d, -180(%rbp)
	movq	%rax, -224(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 2144(%rdi)
	je	.L137
.L141:
	cmpl	$23, %ebx
	leal	-22(%rbx), %eax
	setne	%dl
	cmpl	$1, %eax
	jbe	.L140
	testl	%ebx, %ebx
	jne	.L138
.L140:
	movl	-180(%rbp), %r10d
	testl	%r10d, %r10d
	je	.L143
	testb	%dl, %dl
	jne	.L138
.L143:
	movq	%r12, %rdi
	call	ossl_statem_get_in_handshake@PLT
	testl	%eax, %eax
	je	.L144
.L146:
	cmpl	$22, %ebx
	leaq	2144(%r12), %r15
	movq	$0, -200(%rbp)
	sete	%dl
	testq	%r14, %r14
	setne	%al
	andl	%eax, %edx
	movb	%dl, -181(%rbp)
	.p2align 4,,10
	.p2align 3
.L151:
	movl	$1, 40(%r12)
	movq	%r12, %rdi
	call	SSL_is_init_finished@PLT
	testl	%eax, %eax
	je	.L149
	cmpq	$0, 3472(%r12)
	je	.L272
.L149:
	movq	%r12, %rdi
	call	dtls1_handle_timeout@PLT
	testl	%eax, %eax
	jg	.L151
	movq	%r12, %rdi
	call	ossl_statem_in_error@PLT
	testl	%eax, %eax
	jne	.L267
	cmpq	$0, 3472(%r12)
	je	.L152
	cmpl	$241, 2124(%r12)
	je	.L152
	movl	3468(%r12), %eax
	cmpl	$21, %eax
	je	.L156
.L199:
	movl	$0, 6124(%r12)
.L156:
	movq	168(%r12), %rdx
	movl	240(%rdx), %r9d
	testl	%r9d, %r9d
	je	.L157
	cmpl	$22, %eax
	jne	.L273
.L157:
	movl	68(%r12), %edx
	movl	%edx, %r13d
	andl	$2, %r13d
	jne	.L274
	cmpl	%eax, %ebx
	je	.L161
	cmpl	$20, %eax
	je	.L275
	cmpl	$21, %eax
	jne	.L172
	movq	3472(%r12), %rax
	testq	%rax, %rax
	jg	.L276
.L173:
	movl	$557, %r9d
	leaq	.LC0(%rip), %r8
	movl	$205, %ecx
.L263:
	movl	$258, %edx
	movl	$10, %esi
	movq	%r12, %rdi
	movl	$-1, %r13d
	call	ossl_statem_fatal@PLT
.L136:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L277
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore_state
	movq	$0, 2128(%r12)
	movq	%r12, %rdi
	call	dtls1_get_record@PLT
	testl	%eax, %eax
	jle	.L278
	movl	3468(%r12), %eax
	movq	$1, 2128(%r12)
	cmpl	$21, %eax
	je	.L156
	cmpq	$0, 3472(%r12)
	je	.L156
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L272:
	movq	6128(%r12), %rax
	movq	80(%rax), %rdi
	call	pqueue_pop@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L149
	movq	8(%rax), %rax
	movq	%r15, %rdi
	movq	%rax, -192(%rbp)
	call	SSL3_BUFFER_release@PLT
	movq	-192(%rbp), %rax
	leaq	.LC0(%rip), %rsi
	movq	(%rax), %rdx
	movq	%rdx, 6024(%r12)
	movq	8(%rax), %rdx
	movq	%rdx, 6032(%r12)
	movdqu	16(%rax), %xmm0
	movups	%xmm0, (%r15)
	movdqu	32(%rax), %xmm1
	movups	%xmm1, 16(%r15)
	movq	48(%rax), %rdx
	movq	%rdx, 32(%r15)
	movdqu	56(%rax), %xmm2
	movups	%xmm2, 3464(%r12)
	movdqu	72(%rax), %xmm3
	movups	%xmm3, 3480(%r12)
	movdqu	88(%rax), %xmm4
	movups	%xmm4, 3496(%r12)
	movdqu	104(%rax), %xmm5
	movups	%xmm5, 3512(%r12)
	movdqu	120(%rax), %xmm6
	movups	%xmm6, 3528(%r12)
	movq	(%rax), %rax
	movl	5(%rax), %edx
	movl	%edx, 6106(%r12)
	movzwl	9(%rax), %eax
	movl	$403, %edx
	movw	%ax, 6110(%r12)
	movq	8(%r13), %rdi
	call	CRYPTO_free@PLT
	movq	%r13, %rdi
	call	pitem_free@PLT
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L275:
	cmpb	$0, -181(%rbp)
	jne	.L279
	andl	$1, %edx
	je	.L182
.L200:
	movl	$1, 40(%r12)
	movq	$0, 3472(%r12)
.L269:
	movl	$1, 3520(%r12)
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L273:
	movq	6128(%r12), %r13
	movq	80(%r13), %rdi
	call	pqueue_size@PLT
	cmpq	$99, %rax
	ja	.L182
	leaq	3536(%r12), %rdx
	leaq	72(%r13), %rsi
	movq	%r12, %rdi
	call	dtls1_buffer_record.part.0
	testl	%eax, %eax
	js	.L267
	.p2align 4,,10
	.p2align 3
.L182:
	movq	$0, 3472(%r12)
	movl	$1, 3520(%r12)
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L172:
	andl	$1, %edx
	jne	.L200
	cmpl	$22, %eax
	jne	.L181
	movq	%r12, %rdi
	call	ossl_statem_get_in_handshake@PLT
	testl	%eax, %eax
	jne	.L280
	movq	6128(%r12), %rax
	movzwl	(%rax), %eax
	cmpq	%rax, 3528(%r12)
	jne	.L182
	cmpq	$11, 3472(%r12)
	jbe	.L182
	movq	3496(%r12), %rdi
	leaq	-176(%rbp), %rsi
	call	dtls1_get_message_header@PLT
	cmpb	$20, -176(%rbp)
	movq	%r12, %rdi
	je	.L281
	call	SSL_is_init_finished@PLT
	testl	%eax, %eax
	je	.L282
	movl	$1, %esi
	movq	%r12, %rdi
	call	ossl_statem_set_in_init@PLT
	movq	%r12, %rdi
	call	*48(%r12)
	movl	%eax, %r13d
	testl	%eax, %eax
	js	.L136
	je	.L267
	testb	$4, 1496(%r12)
	jne	.L151
.L271:
	cmpq	$0, 2176(%r12)
	jne	.L151
	movl	$3, 40(%r12)
	movq	%r12, %rdi
	movl	$-1, %r13d
	call	SSL_get_rbio@PLT
	movl	$15, %esi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	BIO_clear_flags@PLT
	movl	$9, %esi
	movq	%r12, %rdi
	call	BIO_set_flags@PLT
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L278:
	movl	%eax, %esi
	movq	%r12, %rdi
	call	dtls1_read_failed@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jg	.L151
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L276:
	subq	$1, %rax
	je	.L173
	movq	3488(%r12), %rcx
	addq	3496(%r12), %rcx
	movzbl	1(%rcx), %esi
	movzwl	(%rcx), %r11d
	movl	%esi, -192(%rbp)
	cmpq	$1, %rax
	jne	.L173
	movq	184(%r12), %r10
	movzbl	(%rcx), %eax
	testq	%r10, %r10
	je	.L175
	subq	$8, %rsp
	movl	%r11d, -232(%rbp)
	xorl	%edi, %edi
	movq	%r12, %r9
	movb	%al, -228(%rbp)
	movl	(%r12), %esi
	movl	$21, %edx
	movl	$2, %r8d
	pushq	192(%r12)
	call	*%r10
	movl	-232(%rbp), %r11d
	movzbl	-228(%rbp), %eax
	popq	%rsi
	popq	%rdi
.L175:
	movq	1392(%r12), %rcx
	testq	%rcx, %rcx
	je	.L283
.L176:
	movl	%r11d, %edx
	movl	%eax, -228(%rbp)
	movl	$16388, %esi
	movq	%r12, %rdi
	rolw	$8, %dx
	movq	%rcx, -200(%rbp)
	movzwl	%dx, %edx
	call	*%rcx
	movl	-228(%rbp), %eax
.L177:
	cmpl	$1, %eax
	jne	.L178
	movq	168(%r12), %rax
	movl	-192(%rbp), %esi
	movl	%esi, 244(%rax)
	movl	6124(%r12), %eax
	movl	$1, 3520(%r12)
	addl	$1, %eax
	movl	%eax, 6124(%r12)
	cmpl	$5, %eax
	je	.L284
	movl	-192(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L151
	orl	$2, 68(%r12)
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L144:
	movq	%r12, %rdi
	call	SSL_in_init@PLT
	testl	%eax, %eax
	je	.L146
	movq	%r12, %rdi
	call	*48(%r12)
	movl	%eax, %r13d
	testl	%eax, %eax
	js	.L136
	jne	.L146
	.p2align 4,,10
	.p2align 3
.L267:
	movl	$-1, %r13d
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L281:
	call	dtls1_check_timeout_num@PLT
	testl	%eax, %eax
	js	.L267
	movq	%r12, %rdi
	call	dtls1_retransmit_buffered_messages@PLT
	testl	%eax, %eax
	jle	.L187
.L190:
	movq	$0, 3472(%r12)
	movl	$1, 3520(%r12)
	testb	$4, 1496(%r12)
	je	.L271
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L283:
	movq	1440(%r12), %rdx
	movq	272(%rdx), %rcx
	testq	%rcx, %rcx
	jne	.L176
	cmpq	$0, -200(%rbp)
	je	.L177
	movq	-200(%rbp), %rcx
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L137:
	call	ssl3_setup_buffers@PLT
	testl	%eax, %eax
	jne	.L141
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L274:
	movq	$0, 3472(%r12)
	xorl	%r13d, %r13d
	movl	$1, 3520(%r12)
	movl	$1, 40(%r12)
	jmp	.L136
.L161:
	movq	%r12, %rdi
	call	SSL_in_init@PLT
	testl	%eax, %eax
	je	.L165
	cmpl	$23, %ebx
	jne	.L165
	cmpq	$0, 1088(%r12)
	jne	.L165
	movl	$488, %r9d
	leaq	.LC0(%rip), %r8
	movl	$100, %ecx
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L165:
	testq	%r14, %r14
	je	.L166
.L198:
	movl	3468(%r12), %eax
	movl	%eax, (%r14)
.L166:
	cmpq	$0, -208(%rbp)
	movq	3472(%r12), %rax
	jne	.L167
	testq	%rax, %rax
	jne	.L136
	jmp	.L269
.L280:
	movl	3468(%r12), %eax
.L181:
	cmpl	$22, %eax
	jle	.L285
	cmpl	$23, %eax
	jne	.L195
	movq	168(%r12), %rax
	movl	272(%rax), %edx
	testl	%edx, %edx
	je	.L197
	movl	264(%rax), %eax
	testl	%eax, %eax
	je	.L197
	movq	%r12, %rdi
	call	ossl_statem_app_data_allowed@PLT
	testl	%eax, %eax
	je	.L197
	movq	168(%r12), %rax
	movl	$2, 272(%rax)
	jmp	.L267
.L285:
	cmpl	$19, %eax
	jg	.L286
.L195:
	movl	$742, %r9d
.L265:
	leaq	.LC0(%rip), %r8
	movl	$245, %ecx
	jmp	.L263
.L167:
	movq	-208(%rbp), %rbx
	movq	-216(%rbp), %rdi
	movq	3488(%r12), %rsi
	cmpq	%rax, %rbx
	cmova	%rax, %rbx
	addq	3496(%r12), %rsi
	movq	%rbx, %rdx
	call	memcpy@PLT
	movl	-180(%rbp), %r8d
	testl	%r8d, %r8d
	je	.L168
	cmpq	$0, 3472(%r12)
	jne	.L170
.L262:
	movl	$1, 3520(%r12)
.L170:
	movq	-224(%rbp), %rax
	movl	$1, %r13d
	movq	%rbx, (%rax)
	jmp	.L136
.L138:
	movl	$358, %r9d
.L266:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	$258, %edx
	movq	%r12, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L267
.L286:
	movl	$753, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	jmp	.L263
.L187:
	movq	%r12, %rdi
	call	ossl_statem_in_error@PLT
	testl	%eax, %eax
	je	.L190
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L279:
	movq	%r12, %rdi
	call	SSL_in_init@PLT
	jmp	.L198
.L168:
	movq	3472(%r12), %rax
	addq	%rbx, 3488(%r12)
	subq	%rbx, %rax
	movq	%rax, 3472(%r12)
	testq	%rax, %rax
	jne	.L170
	movl	$240, 2124(%r12)
	movq	$0, 3488(%r12)
	jmp	.L262
.L197:
	movl	$770, %r9d
	jmp	.L265
.L284:
	movl	$582, %r9d
	leaq	.LC0(%rip), %r8
	movl	$409, %ecx
	jmp	.L263
.L178:
	cmpl	$2, %eax
	je	.L287
	movl	$620, %r9d
	leaq	.LC0(%rip), %r8
	movl	$246, %ecx
	movq	%r12, %rdi
	movl	$258, %edx
	movl	$47, %esi
	movl	$-1, %r13d
	call	ossl_statem_fatal@PLT
	jmp	.L136
.L282:
	movl	$705, %r9d
	jmp	.L266
.L287:
	movl	-192(%rbp), %ebx
	movl	$611, %r9d
	movq	%r12, %rdi
	leaq	-80(%rbp), %r14
	movq	168(%r12), %rax
	movl	$258, %edx
	movl	$-1, %esi
	movl	$1, 40(%r12)
	leaq	.LC0(%rip), %r8
	leal	1000(%rbx), %ecx
	movl	%ebx, 248(%rax)
	call	ossl_statem_fatal@PLT
	movl	%ebx, %ecx
	movq	%r14, %rdi
	movl	$16, %esi
	leaq	.LC1(%rip), %rdx
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	movq	%r14, %rdx
	movl	$2, %edi
	xorl	%eax, %eax
	leaq	.LC2(%rip), %rsi
	call	ERR_add_error_data@PLT
	orl	$2, 68(%r12)
	movq	1296(%r12), %rsi
	movl	$1, 3520(%r12)
	movq	1904(%r12), %rdi
	call	SSL_CTX_remove_session@PLT
	jmp	.L136
.L277:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1010:
	.size	dtls1_read_bytes, .-dtls1_read_bytes
	.p2align 4
	.globl	dtls1_write_bytes
	.type	dtls1_write_bytes, @function
dtls1_write_bytes:
.LFB1011:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	cmpq	$16384, %rcx
	ja	.L299
	cmpq	$0, 2216(%rdi)
	movl	$1, 40(%rdi)
	jne	.L300
	movq	168(%rdi), %rax
	movl	%esi, %r14d
	movq	%rdx, %r15
	movq	%rcx, %r13
	movq	%r8, %rbx
	movl	252(%rax), %eax
	testl	%eax, %eax
	je	.L294
	movq	8(%rdi), %rax
	call	*120(%rax)
	testl	%eax, %eax
	jle	.L288
.L294:
	xorl	%eax, %eax
	testq	%r13, %r13
	jne	.L301
.L288:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L301:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%rbx, %r9
	movq	%r13, %rcx
	movq	%r15, %rdx
	popq	%rbx
	movl	%r14d, %esi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	do_dtls1_write.part.0
	.p2align 4,,10
	.p2align 3
.L300:
	.cfi_restore_state
	movl	$815, %r9d
	movl	$68, %ecx
	movl	$245, %edx
	movl	$80, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L299:
	.cfi_restore_state
	movl	$788, %r9d
	movl	$68, %ecx
	movl	$545, %edx
	movl	$80, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	movl	$-1, %eax
	jmp	.L288
	.cfi_endproc
.LFE1011:
	.size	dtls1_write_bytes, .-dtls1_write_bytes
	.p2align 4
	.globl	do_dtls1_write
	.type	do_dtls1_write, @function
do_dtls1_write:
.LFB1012:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	cmpq	$0, 2216(%rdi)
	jne	.L315
	movq	168(%rdi), %rax
	movl	%esi, %r14d
	movq	%rdx, %r15
	movq	%rcx, %r13
	movl	%r8d, %ebx
	movl	252(%rax), %eax
	testl	%eax, %eax
	jne	.L305
	testq	%r13, %r13
	jne	.L307
.L316:
	xorl	%eax, %eax
	testl	%ebx, %ebx
	jne	.L307
.L302:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L305:
	.cfi_restore_state
	movq	8(%rdi), %rax
	movq	%r9, -56(%rbp)
	call	*120(%rax)
	movq	-56(%rbp), %r9
	testl	%eax, %eax
	jle	.L302
	testq	%r13, %r13
	je	.L316
.L307:
	addq	$24, %rsp
	movl	%ebx, %r8d
	movq	%r13, %rcx
	movq	%r15, %rdx
	popq	%rbx
	movl	%r14d, %esi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	do_dtls1_write.part.0
	.p2align 4,,10
	.p2align 3
.L315:
	.cfi_restore_state
	movl	$815, %r9d
	movl	$68, %ecx
	movl	$245, %edx
	movl	$80, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1012:
	.size	do_dtls1_write, .-do_dtls1_write
	.p2align 4
	.globl	dtls1_get_bitmap
	.type	dtls1_get_bitmap, @function
dtls1_get_bitmap:
.LFB1013:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	64(%rsi), %rdi
	movl	$0, (%rdx)
	movq	6128(%r8), %rcx
	movzwl	(%rcx), %r8d
	movq	%r8, %rax
	cmpq	%r8, %rdi
	je	.L323
	movzwl	%r8w, %r8d
	leaq	1(%r8), %r9
	xorl	%r8d, %r8d
	cmpq	%r9, %rdi
	jne	.L317
	cmpw	40(%rcx), %ax
	je	.L317
	movl	4(%rsi), %eax
	subl	$21, %eax
	cmpl	$1, %eax
	jbe	.L324
.L317:
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L323:
	leaq	8(%rcx), %r8
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L324:
	leaq	24(%rcx), %r8
	movl	$1, (%rdx)
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE1013:
	.size	dtls1_get_bitmap, .-dtls1_get_bitmap
	.p2align 4
	.globl	dtls1_reset_seq_numbers
	.type	dtls1_reset_seq_numbers, @function
dtls1_reset_seq_numbers:
.LFB1014:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	andl	$1, %esi
	movq	6128(%rdi), %rax
	je	.L326
	movdqu	24(%rax), %xmm1
	addw	$1, (%rax)
	pxor	%xmm0, %xmm0
	leaq	6104(%rdi), %rbx
	movups	%xmm1, 8(%rax)
	movq	6128(%rdi), %rax
	movups	%xmm0, 24(%rax)
	call	dtls1_clear_received_buffer@PLT
	movq	$0, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L326:
	.cfi_restore_state
	movq	6112(%rdi), %rdx
	leaq	6112(%rdi), %rbx
	movq	%rdx, 88(%rax)
	movq	6128(%rdi), %rax
	addw	$1, 2(%rax)
	movq	$0, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1014:
	.size	dtls1_reset_seq_numbers, .-dtls1_reset_seq_numbers
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
