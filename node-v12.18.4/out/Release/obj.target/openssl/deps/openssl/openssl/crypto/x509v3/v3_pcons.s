	.file	"v3_pcons.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Require Explicit Policy"
.LC1:
	.string	"Inhibit Policy Mapping"
	.text
	.p2align 4
	.type	i2v_POLICY_CONSTRAINTS, @function
i2v_POLICY_CONSTRAINTS:
.LFB1322:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-24(%rbp), %r12
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	(%rsi), %rsi
	movq	%rdx, -24(%rbp)
	movq	%r12, %rdx
	call	X509V3_add_value_int@PLT
	movq	8(%rbx), %rsi
	movq	%r12, %rdx
	leaq	.LC1(%rip), %rdi
	call	X509V3_add_value_int@PLT
	movq	-24(%rbp), %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1322:
	.size	i2v_POLICY_CONSTRAINTS, .-i2v_POLICY_CONSTRAINTS
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"../deps/openssl/openssl/crypto/x509v3/v3_pcons.c"
	.section	.rodata.str1.1
.LC3:
	.string	"requireExplicitPolicy"
.LC4:
	.string	"inhibitPolicyMapping"
.LC5:
	.string	",value:"
.LC6:
	.string	",name:"
.LC7:
	.string	"section:"
	.text
	.p2align 4
	.type	v2i_POLICY_CONSTRAINTS, @function
v2i_POLICY_CONSTRAINTS:
.LFB1323:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	POLICY_CONSTRAINTS_it(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	.LC3(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$24, %rsp
	call	ASN1_item_new@PLT
	movq	%rax, %r14
	leaq	8(%rax), %r15
	testq	%rax, %rax
	jne	.L5
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L7:
	movl	$21, %ecx
	movq	%rdx, %rsi
	leaq	.LC4(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L10
	movq	%r15, %rsi
	movq	%r10, %rdi
	call	X509V3_get_value_int@PLT
	testl	%eax, %eax
	je	.L9
.L8:
	addl	$1, %ebx
.L5:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L21
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	movl	$22, %ecx
	movq	%r13, %rdi
	movq	8(%rax), %rdx
	movq	%rax, %r10
	movq	%rdx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L7
	movq	%r14, %rsi
	movq	%r10, %rdi
	call	X509V3_get_value_int@PLT
	testl	%eax, %eax
	jne	.L8
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%r14, %rdi
	leaq	POLICY_CONSTRAINTS_it(%rip), %rsi
	xorl	%r14d, %r14d
	call	ASN1_item_free@PLT
.L4:
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	cmpq	$0, 8(%r14)
	jne	.L4
	cmpq	$0, (%r14)
	jne	.L4
	movl	$82, %r8d
	movl	$151, %edx
	movl	$146, %esi
	movl	$34, %edi
	leaq	.LC2(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L10:
	movl	$76, %r8d
	leaq	.LC2(%rip), %rcx
	movl	$106, %edx
	movl	$146, %esi
	movl	$34, %edi
	movq	%r10, -56(%rbp)
	call	ERR_put_error@PLT
	movq	-56(%rbp), %r10
	subq	$8, %rsp
	xorl	%eax, %eax
	leaq	.LC5(%rip), %r9
	leaq	.LC6(%rip), %rcx
	movl	$6, %edi
	movq	8(%r10), %r8
	pushq	16(%r10)
	leaq	.LC7(%rip), %rsi
	movq	(%r10), %rdx
	call	ERR_add_error_data@PLT
	popq	%rax
	popq	%rdx
	jmp	.L9
.L20:
	movl	$64, %r8d
	movl	$65, %edx
	movl	$146, %esi
	movl	$34, %edi
	leaq	.LC2(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L4
	.cfi_endproc
.LFE1323:
	.size	v2i_POLICY_CONSTRAINTS, .-v2i_POLICY_CONSTRAINTS
	.p2align 4
	.globl	POLICY_CONSTRAINTS_new
	.type	POLICY_CONSTRAINTS_new, @function
POLICY_CONSTRAINTS_new:
.LFB1320:
	.cfi_startproc
	endbr64
	leaq	POLICY_CONSTRAINTS_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1320:
	.size	POLICY_CONSTRAINTS_new, .-POLICY_CONSTRAINTS_new
	.p2align 4
	.globl	POLICY_CONSTRAINTS_free
	.type	POLICY_CONSTRAINTS_free, @function
POLICY_CONSTRAINTS_free:
.LFB1321:
	.cfi_startproc
	endbr64
	leaq	POLICY_CONSTRAINTS_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1321:
	.size	POLICY_CONSTRAINTS_free, .-POLICY_CONSTRAINTS_free
	.globl	POLICY_CONSTRAINTS_it
	.section	.rodata.str1.1
.LC8:
	.string	"POLICY_CONSTRAINTS"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	POLICY_CONSTRAINTS_it, @object
	.size	POLICY_CONSTRAINTS_it, 56
POLICY_CONSTRAINTS_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	POLICY_CONSTRAINTS_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC8
	.section	.data.rel.ro,"aw"
	.align 32
	.type	POLICY_CONSTRAINTS_seq_tt, @object
	.size	POLICY_CONSTRAINTS_seq_tt, 80
POLICY_CONSTRAINTS_seq_tt:
	.quad	137
	.quad	0
	.quad	0
	.quad	.LC3
	.quad	ASN1_INTEGER_it
	.quad	137
	.quad	1
	.quad	8
	.quad	.LC4
	.quad	ASN1_INTEGER_it
	.globl	v3_policy_constraints
	.section	.data.rel.ro.local
	.align 32
	.type	v3_policy_constraints, @object
	.size	v3_policy_constraints, 104
v3_policy_constraints:
	.long	401
	.long	0
	.quad	POLICY_CONSTRAINTS_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	i2v_POLICY_CONSTRAINTS
	.quad	v2i_POLICY_CONSTRAINTS
	.quad	0
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
