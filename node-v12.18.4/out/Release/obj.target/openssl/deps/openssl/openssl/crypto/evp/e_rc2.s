	.file	"e_rc2.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/evp/e_rc2.c"
	.align 8
.LC1:
	.string	"assertion failed: l <= sizeof(iv)"
	.text
	.p2align 4
	.type	rc2_get_asn1_type_and_iv, @function
rc2_get_asn1_type_and_iv:
.LFB458:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -88(%rbp)
	testq	%rsi, %rsi
	je	.L11
	movq	%rdi, %r12
	movq	%rsi, %r13
	call	EVP_CIPHER_CTX_iv_length@PLT
	movl	%eax, %ebx
	cmpl	$16, %eax
	ja	.L22
	leaq	-80(%rbp), %r14
	movq	%r13, %rdi
	leaq	-88(%rbp), %rsi
	movl	%eax, %ecx
	movq	%r14, %rdx
	call	ASN1_TYPE_get_int_octetstring@PLT
	movl	%eax, %r13d
	cmpl	%eax, %ebx
	jne	.L20
	movq	-88(%rbp), %rax
	cmpl	$58, %eax
	je	.L12
	cmpl	$120, %eax
	je	.L13
	cmpl	$160, %eax
	je	.L14
	movl	$116, %r8d
	movl	$108, %edx
	movl	$109, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L20:
	movl	$-1, %r13d
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L23
	addq	$56, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	xorl	%r13d, %r13d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$16, %r15d
	movl	$128, %ebx
.L5:
	testl	%r13d, %r13d
	je	.L9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$-1, %r9d
	movq	%r14, %r8
	movq	%r12, %rdi
	call	EVP_CipherInit_ex@PLT
	testl	%eax, %eax
	je	.L20
.L9:
	xorl	%ecx, %ecx
	movl	%ebx, %edx
	movl	$3, %esi
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L20
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_set_key_length@PLT
	testl	%eax, %eax
	jg	.L1
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L14:
	movl	$5, %r15d
	movl	$40, %ebx
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$8, %r15d
	movl	$64, %ebx
	jmp	.L5
.L23:
	call	__stack_chk_fail@PLT
.L22:
	movl	$131, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE458:
	.size	rc2_get_asn1_type_and_iv, .-rc2_get_asn1_type_and_iv
	.p2align 4
	.type	rc2_cbc_cipher, @function
rc2_cbc_cipher:
.LFB445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -80(%rbp)
	movabsq	$4611686018427387903, %rdx
	movq	%rsi, -72(%rbp)
	cmpq	%rdx, %rcx
	jbe	.L25
	movq	%rcx, %r14
	addq	%r12, %rax
	leaq	(%rsi,%rcx), %rcx
	movq	%rdx, %r12
	movq	%rcx, -56(%rbp)
	movq	%rax, -64(%rbp)
	movq	%r14, -88(%rbp)
.L26:
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r13, %rdi
	movl	%eax, %r15d
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movl	%r15d, %r9d
	leaq	4(%rax), %rcx
	movq	%rbx, %r8
	movabsq	$4611686018427387904, %rdx
	subq	%r14, %rsi
	subq	%r14, %rdi
	call	RC2_cbc_encrypt@PLT
	movabsq	$-4611686018427387904, %rax
	addq	%rax, %r14
	cmpq	%r12, %r14
	ja	.L26
	movq	-88(%rbp), %r14
	leaq	(%r14,%rax), %r10
	andq	%r14, %r12
	shrq	$62, %r10
	addq	$1, %r10
	salq	$62, %r10
	addq	%r10, -80(%rbp)
	addq	%r10, -72(%rbp)
.L25:
	testq	%r12, %r12
	jne	.L35
.L27:
	addq	$56, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r13, %rdi
	movl	%eax, %r15d
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	-72(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movl	%r15d, %r9d
	leaq	4(%rax), %rcx
	movq	%rbx, %r8
	movq	%r12, %rdx
	call	RC2_cbc_encrypt@PLT
	jmp	.L27
	.cfi_endproc
.LFE445:
	.size	rc2_cbc_cipher, .-rc2_cbc_cipher
	.p2align 4
	.type	rc2_init_key, @function
rc2_init_key:
.LFB455:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movl	(%rax), %r15d
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movl	%r15d, %ecx
	movq	%r14, %rdx
	movl	%r13d, %esi
	leaq	4(%rax), %rdi
	call	RC2_set_key@PLT
	popq	%r12
	movl	$1, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE455:
	.size	rc2_init_key, .-rc2_init_key
	.p2align 4
	.type	rc2_cfb64_cipher, @function
rc2_cfb64_cipher:
.LFB446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, %rcx
	ja	.L44
	movq	%rcx, %rbx
	testq	%rcx, %rcx
	je	.L40
.L39:
	leaq	-60(%rbp), %rax
	movq	%rax, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L41:
	movq	%r12, %rdi
	subq	%rbx, %r15
	call	EVP_CIPHER_CTX_num@PLT
	movq	%r12, %rdi
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r12, %rdi
	movl	%eax, -76(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movl	-76(%rbp), %edx
	subq	$8, %rsp
	movq	-88(%rbp), %r9
	movq	-72(%rbp), %r8
	leaq	4(%rax), %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	pushq	%rdx
	movq	%rbx, %rdx
	addq	%rbx, %r14
	addq	%rbx, %r13
	call	RC2_cfb64_encrypt@PLT
	movl	-60(%rbp), %esi
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	cmpq	%r15, %rbx
	popq	%rax
	popq	%rdx
	cmova	%r15, %rbx
	testq	%r15, %r15
	je	.L40
	cmpq	%rbx, %r15
	jnb	.L41
.L40:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L53
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	movabsq	$4611686018427387904, %rbx
	jmp	.L39
.L53:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE446:
	.size	rc2_cfb64_cipher, .-rc2_cfb64_cipher
	.p2align 4
	.type	rc2_ofb_cipher, @function
rc2_ofb_cipher:
.LFB448:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -96(%rbp)
	movabsq	$4611686018427387903, %rdx
	movq	%rsi, -88(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	movq	%rcx, %rbx
	cmpq	%rdx, %rcx
	jbe	.L55
	addq	%rcx, %rax
	movq	%rcx, -104(%rbp)
	leaq	(%rsi,%rcx), %rbx
	movq	%rcx, %r13
	movq	%rax, -80(%rbp)
	leaq	-60(%rbp), %r15
	movq	%rbx, -72(%rbp)
	movq	%rdx, %rbx
.L56:
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_num@PLT
	movq	%r12, %rdi
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	-72(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movq	%r15, %r9
	leaq	4(%rax), %rcx
	movq	%r14, %r8
	movabsq	$4611686018427387904, %rdx
	subq	%r13, %rsi
	subq	%r13, %rdi
	call	RC2_ofb64_encrypt@PLT
	movl	-60(%rbp), %esi
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	movabsq	$-4611686018427387904, %rax
	addq	%rax, %r13
	cmpq	%rbx, %r13
	ja	.L56
	movq	-104(%rbp), %r13
	leaq	0(%r13,%rax), %r11
	andq	%r13, %rbx
	shrq	$62, %r11
	addq	$1, %r11
	salq	$62, %r11
	addq	%r11, -96(%rbp)
	addq	%r11, -88(%rbp)
.L55:
	testq	%rbx, %rbx
	jne	.L66
.L57:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L67
	addq	$72, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_num@PLT
	movq	%r12, %rdi
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	%r14, %r8
	leaq	4(%rax), %rcx
	leaq	-60(%rbp), %r9
	movq	%rbx, %rdx
	call	RC2_ofb64_encrypt@PLT
	movl	-60(%rbp), %esi
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	jmp	.L57
.L67:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE448:
	.size	rc2_ofb_cipher, .-rc2_ofb_cipher
	.p2align 4
	.type	rc2_ecb_cipher, @function
rc2_ecb_cipher:
.LFB447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%rsi, -56(%rbp)
	call	EVP_CIPHER_CTX_cipher@PLT
	movslq	4(%rax), %r15
	cmpq	%r12, %r15
	ja	.L69
	subq	%r15, %r12
	movq	%r12, -64(%rbp)
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L70:
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%rbx, %rdi
	movl	%eax, %r13d
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	leaq	(%r14,%r12), %rdi
	movl	%r13d, %ecx
	leaq	4(%rax), %rdx
	movq	-56(%rbp), %rax
	leaq	(%rax,%r12), %rsi
	addq	%r15, %r12
	call	RC2_ecb_encrypt@PLT
	cmpq	%r12, -64(%rbp)
	jnb	.L70
.L69:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE447:
	.size	rc2_ecb_cipher, .-rc2_ecb_cipher
	.p2align 4
	.type	rc2_ctrl, @function
rc2_ctrl:
.LFB460:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	cmpl	$2, %esi
	je	.L74
	cmpl	$3, %esi
	je	.L75
	movl	$-1, %eax
	testl	%esi, %esi
	je	.L80
.L73:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	movq	%rcx, %rbx
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movl	(%rax), %eax
	movl	%eax, (%rbx)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	movq	%rdi, -24(%rbp)
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	-24(%rbp), %rdi
	movl	%eax, %ebx
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	sall	$3, %ebx
	movl	%ebx, (%rax)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L73
	movl	%edx, -24(%rbp)
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movl	-24(%rbp), %edx
	movl	%edx, (%rax)
	movl	$1, %eax
	jmp	.L73
	.cfi_endproc
.LFE460:
	.size	rc2_ctrl, .-rc2_ctrl
	.p2align 4
	.type	rc2_set_asn1_type_and_iv, @function
rc2_set_asn1_type_and_iv:
.LFB459:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L81
	movq	%rsi, %r14
	leaq	-44(%rbp), %rcx
	xorl	%edx, %edx
	movl	$2, %esi
	movq	%rdi, %r12
	xorl	%r15d, %r15d
	call	EVP_CIPHER_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L83
	movl	-44(%rbp), %eax
	cmpl	$128, %eax
	je	.L87
	cmpl	$64, %eax
	je	.L88
	cmpl	$40, %eax
	movl	$160, %eax
	cmove	%rax, %r15
.L83:
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_iv_length@PLT
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	EVP_CIPHER_CTX_original_iv@PLT
	movl	%r13d, %ecx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	ASN1_TYPE_set_int_octetstring@PLT
.L81:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L92
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	movl	$58, %r15d
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L88:
	movl	$120, %r15d
	jmp	.L83
.L92:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE459:
	.size	rc2_set_asn1_type_and_iv, .-rc2_set_asn1_type_and_iv
	.p2align 4
	.globl	EVP_rc2_cbc
	.type	EVP_rc2_cbc, @function
EVP_rc2_cbc:
.LFB449:
	.cfi_startproc
	endbr64
	leaq	rc2_cbc(%rip), %rax
	ret
	.cfi_endproc
.LFE449:
	.size	EVP_rc2_cbc, .-EVP_rc2_cbc
	.p2align 4
	.globl	EVP_rc2_cfb64
	.type	EVP_rc2_cfb64, @function
EVP_rc2_cfb64:
.LFB450:
	.cfi_startproc
	endbr64
	leaq	rc2_cfb64(%rip), %rax
	ret
	.cfi_endproc
.LFE450:
	.size	EVP_rc2_cfb64, .-EVP_rc2_cfb64
	.p2align 4
	.globl	EVP_rc2_ofb
	.type	EVP_rc2_ofb, @function
EVP_rc2_ofb:
.LFB451:
	.cfi_startproc
	endbr64
	leaq	rc2_ofb(%rip), %rax
	ret
	.cfi_endproc
.LFE451:
	.size	EVP_rc2_ofb, .-EVP_rc2_ofb
	.p2align 4
	.globl	EVP_rc2_ecb
	.type	EVP_rc2_ecb, @function
EVP_rc2_ecb:
.LFB452:
	.cfi_startproc
	endbr64
	leaq	rc2_ecb(%rip), %rax
	ret
	.cfi_endproc
.LFE452:
	.size	EVP_rc2_ecb, .-EVP_rc2_ecb
	.p2align 4
	.globl	EVP_rc2_64_cbc
	.type	EVP_rc2_64_cbc, @function
EVP_rc2_64_cbc:
.LFB453:
	.cfi_startproc
	endbr64
	leaq	r2_64_cbc_cipher(%rip), %rax
	ret
	.cfi_endproc
.LFE453:
	.size	EVP_rc2_64_cbc, .-EVP_rc2_64_cbc
	.p2align 4
	.globl	EVP_rc2_40_cbc
	.type	EVP_rc2_40_cbc, @function
EVP_rc2_40_cbc:
.LFB454:
	.cfi_startproc
	endbr64
	leaq	r2_40_cbc_cipher(%rip), %rax
	ret
	.cfi_endproc
.LFE454:
	.size	EVP_rc2_40_cbc, .-EVP_rc2_40_cbc
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	r2_40_cbc_cipher, @object
	.size	r2_40_cbc_cipher, 88
r2_40_cbc_cipher:
	.long	98
	.long	8
	.long	5
	.long	8
	.quad	74
	.quad	rc2_init_key
	.quad	rc2_cbc_cipher
	.quad	0
	.long	260
	.zero	4
	.quad	rc2_set_asn1_type_and_iv
	.quad	rc2_get_asn1_type_and_iv
	.quad	rc2_ctrl
	.quad	0
	.align 32
	.type	r2_64_cbc_cipher, @object
	.size	r2_64_cbc_cipher, 88
r2_64_cbc_cipher:
	.long	166
	.long	8
	.long	8
	.long	8
	.quad	74
	.quad	rc2_init_key
	.quad	rc2_cbc_cipher
	.quad	0
	.long	260
	.zero	4
	.quad	rc2_set_asn1_type_and_iv
	.quad	rc2_get_asn1_type_and_iv
	.quad	rc2_ctrl
	.quad	0
	.align 32
	.type	rc2_ecb, @object
	.size	rc2_ecb, 88
rc2_ecb:
	.long	38
	.long	8
	.long	16
	.long	0
	.quad	73
	.quad	rc2_init_key
	.quad	rc2_ecb_cipher
	.quad	0
	.long	260
	.zero	4
	.quad	rc2_set_asn1_type_and_iv
	.quad	rc2_get_asn1_type_and_iv
	.quad	rc2_ctrl
	.quad	0
	.align 32
	.type	rc2_ofb, @object
	.size	rc2_ofb, 88
rc2_ofb:
	.long	40
	.long	1
	.long	16
	.long	8
	.quad	76
	.quad	rc2_init_key
	.quad	rc2_ofb_cipher
	.quad	0
	.long	260
	.zero	4
	.quad	rc2_set_asn1_type_and_iv
	.quad	rc2_get_asn1_type_and_iv
	.quad	rc2_ctrl
	.quad	0
	.align 32
	.type	rc2_cfb64, @object
	.size	rc2_cfb64, 88
rc2_cfb64:
	.long	39
	.long	1
	.long	16
	.long	8
	.quad	75
	.quad	rc2_init_key
	.quad	rc2_cfb64_cipher
	.quad	0
	.long	260
	.zero	4
	.quad	rc2_set_asn1_type_and_iv
	.quad	rc2_get_asn1_type_and_iv
	.quad	rc2_ctrl
	.quad	0
	.align 32
	.type	rc2_cbc, @object
	.size	rc2_cbc, 88
rc2_cbc:
	.long	37
	.long	8
	.long	16
	.long	8
	.quad	74
	.quad	rc2_init_key
	.quad	rc2_cbc_cipher
	.quad	0
	.long	260
	.zero	4
	.quad	rc2_set_asn1_type_and_iv
	.quad	rc2_get_asn1_type_and_iv
	.quad	rc2_ctrl
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
