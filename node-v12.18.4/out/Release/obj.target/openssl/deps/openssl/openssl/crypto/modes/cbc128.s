	.file	"cbc128.c"
	.text
	.p2align 4
	.globl	CRYPTO_cbc128_encrypt
	.type	CRYPTO_cbc128_encrypt, @function
CRYPTO_cbc128_encrypt:
.LFB151:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -80(%rbp)
	movq	%r8, -104(%rbp)
	testq	%rdx, %rdx
	je	.L1
	movq	%rdi, %r13
	movq	%rdx, %r14
	movq	%rcx, %r15
	cmpq	$15, %rdx
	jbe	.L9
	leaq	-16(%rdx), %rbx
	movq	%rsi, %r12
	shrq	$4, %rbx
	leaq	1(%rbx), %rax
	movq	%rbx, -88(%rbp)
	movq	%rdi, %rbx
	salq	$4, %rax
	movq	%rax, %rcx
	movq	%rax, -96(%rbp)
	movq	%rsi, %rax
	movq	%r8, %rsi
	addq	%rcx, %r12
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L10:
	movq	%rdx, %rax
.L4:
	movq	(%rbx), %rdx
	xorq	(%rsi), %rdx
	movq	%rax, -72(%rbp)
	movq	%rax, %rdi
	movq	%rdx, (%rax)
	movq	8(%rbx), %rdx
	addq	$16, %rbx
	xorq	8(%rsi), %rdx
	movq	%r9, -64(%rbp)
	movq	%rax, %rsi
	movq	%rdx, 8(%rax)
	movq	%r15, %rdx
	call	*%r9
	movq	-72(%rbp), %rax
	movq	-64(%rbp), %r9
	leaq	16(%rax), %rdx
	movq	%rax, %rsi
	cmpq	%r12, %rdx
	jne	.L10
	movq	-88(%rbp), %rbx
	addq	-96(%rbp), %r13
	salq	$4, %rbx
	addq	-80(%rbp), %rbx
	andl	$15, %r14d
	je	.L57
.L3:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L6:
	movzbl	0(%r13,%rax), %edx
	xorb	(%rbx,%rax), %dl
	movb	%dl, (%r12,%rax)
	addq	$1, %rax
	cmpq	%rax, %r14
	ja	.L6
	testq	%r14, %r14
	movl	$1, %edx
	cmovne	%r14, %rdx
	movzbl	(%rbx,%rdx), %eax
	movb	%al, (%r12,%rdx)
	cmpq	$15, %rdx
	je	.L7
	movzbl	1(%rbx,%rdx), %eax
	movb	%al, 1(%r12,%rdx)
	cmpq	$14, %rdx
	je	.L7
	movzbl	2(%rbx,%rdx), %eax
	movb	%al, 2(%r12,%rdx)
	cmpq	$13, %rdx
	je	.L7
	movzbl	3(%rbx,%rdx), %eax
	movb	%al, 3(%r12,%rdx)
	cmpq	$12, %rdx
	je	.L7
	movzbl	4(%rbx,%rdx), %eax
	movb	%al, 4(%r12,%rdx)
	cmpq	$11, %rdx
	je	.L7
	movzbl	5(%rbx,%rdx), %eax
	movb	%al, 5(%r12,%rdx)
	cmpq	$10, %rdx
	je	.L7
	movzbl	6(%rbx,%rdx), %eax
	movb	%al, 6(%r12,%rdx)
	cmpq	$9, %rdx
	je	.L7
	movzbl	7(%rbx,%rdx), %eax
	movb	%al, 7(%r12,%rdx)
	cmpq	$8, %rdx
	je	.L7
	movzbl	8(%rbx,%rdx), %eax
	movb	%al, 8(%r12,%rdx)
	cmpq	$7, %rdx
	je	.L7
	movzbl	9(%rbx,%rdx), %eax
	movb	%al, 9(%r12,%rdx)
	cmpq	$6, %rdx
	je	.L7
	movzbl	10(%rbx,%rdx), %eax
	movb	%al, 10(%r12,%rdx)
	cmpq	$5, %rdx
	je	.L7
	movzbl	11(%rbx,%rdx), %eax
	movb	%al, 11(%r12,%rdx)
	cmpq	$4, %rdx
	je	.L7
	movzbl	12(%rbx,%rdx), %eax
	movb	%al, 12(%r12,%rdx)
	cmpq	$3, %rdx
	je	.L7
	movzbl	13(%rbx,%rdx), %eax
	movb	%al, 13(%r12,%rdx)
	cmpq	$1, %rdx
	jne	.L7
	movzbl	15(%rbx), %eax
	movb	%al, 15(%r12)
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%r9
.L8:
	movdqu	(%r12), %xmm0
	movq	-104(%rbp), %rax
	movaps	%xmm0, -64(%rbp)
	movups	%xmm0, (%rax)
.L1:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L57:
	.cfi_restore_state
	movq	%rbx, %r12
	jmp	.L8
.L9:
	movq	%rsi, %r12
	movq	%r8, %rbx
	jmp	.L3
	.cfi_endproc
.LFE151:
	.size	CRYPTO_cbc128_encrypt, .-CRYPTO_cbc128_encrypt
	.p2align 4
	.globl	CRYPTO_cbc128_decrypt
	.type	CRYPTO_cbc128_decrypt, @function
CRYPTO_cbc128_decrypt:
.LFB152:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L58
	movq	%rsi, %r12
	movq	%rcx, %rdx
	movq	%r8, %rbx
	cmpq	%rsi, %rdi
	je	.L156
	cmpq	$15, %r13
	jbe	.L71
	leaq	-16(%r13), %r14
	movq	%rdi, %r15
	movq	%r8, %rcx
	shrq	$4, %r14
	leaq	1(%r14), %rax
	movq	%r14, -120(%rbp)
	salq	$4, %rax
	movq	%rax, -128(%rbp)
	leaq	(%rsi,%rax), %r14
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L72:
	movq	%rsi, %r15
.L64:
	movq	%r12, %rsi
	movq	%rcx, -104(%rbp)
	movq	%r15, %rdi
	movq	%rdx, -96(%rbp)
	movq	%r9, -88(%rbp)
	call	*%r9
	movq	-104(%rbp), %rcx
	movq	-88(%rbp), %r9
	movq	-96(%rbp), %rdx
	movq	(%rcx), %rsi
	xorq	%rsi, (%r12)
	leaq	16(%r15), %rsi
	movq	8(%rcx), %rcx
	xorq	%rcx, 8(%r12)
	addq	$16, %r12
	movq	%r15, %rcx
	cmpq	%r14, %r12
	jne	.L72
	movq	-120(%rbp), %rax
	movq	-112(%rbp), %rdi
	andl	$15, %r13d
	movq	-128(%rbp), %r12
	salq	$4, %rax
	addq	%rdi, %r12
	leaq	(%rdi,%rax), %rcx
.L63:
	movdqu	(%rcx), %xmm0
	movups	%xmm0, (%rbx)
.L67:
	testq	%r13, %r13
	je	.L58
.L62:
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	call	*%r9
	movzbl	(%r12), %ecx
	movzbl	-80(%rbp), %edx
	xorb	(%rbx), %dl
	movb	%dl, (%r14)
	movb	%cl, (%rbx)
	cmpq	$1, %r13
	je	.L68
	movzbl	1(%r12), %ecx
	movzbl	-79(%rbp), %edx
	xorb	1(%rbx), %dl
	movb	%dl, 1(%r14)
	movb	%cl, 1(%rbx)
	cmpq	$2, %r13
	je	.L68
	movzbl	2(%r12), %ecx
	movzbl	-78(%rbp), %edx
	xorb	2(%rbx), %dl
	movb	%dl, 2(%r14)
	movb	%cl, 2(%rbx)
	cmpq	$3, %r13
	je	.L68
	movzbl	3(%r12), %ecx
	movzbl	-77(%rbp), %edx
	xorb	3(%rbx), %dl
	movb	%dl, 3(%r14)
	movb	%cl, 3(%rbx)
	cmpq	$4, %r13
	je	.L68
	movzbl	4(%r12), %ecx
	movzbl	-76(%rbp), %edx
	xorb	4(%rbx), %dl
	movb	%dl, 4(%r14)
	movb	%cl, 4(%rbx)
	cmpq	$5, %r13
	je	.L68
	movzbl	5(%r12), %ecx
	movzbl	-75(%rbp), %edx
	xorb	5(%rbx), %dl
	movb	%dl, 5(%r14)
	movb	%cl, 5(%rbx)
	cmpq	$6, %r13
	je	.L68
	movzbl	6(%r12), %ecx
	movzbl	-74(%rbp), %edx
	xorb	6(%rbx), %dl
	movb	%dl, 6(%r14)
	movb	%cl, 6(%rbx)
	cmpq	$7, %r13
	je	.L68
	movzbl	7(%r12), %ecx
	movzbl	-73(%rbp), %edx
	xorb	7(%rbx), %dl
	movb	%dl, 7(%r14)
	movb	%cl, 7(%rbx)
	cmpq	$8, %r13
	je	.L68
	movzbl	8(%r12), %ecx
	movzbl	-72(%rbp), %edx
	xorb	8(%rbx), %dl
	movb	%dl, 8(%r14)
	movb	%cl, 8(%rbx)
	cmpq	$9, %r13
	je	.L68
	movzbl	9(%r12), %ecx
	movzbl	-71(%rbp), %edx
	xorb	9(%rbx), %dl
	movb	%dl, 9(%r14)
	movb	%cl, 9(%rbx)
	cmpq	$10, %r13
	je	.L68
	movzbl	10(%r12), %ecx
	movzbl	-70(%rbp), %edx
	xorb	10(%rbx), %dl
	movb	%dl, 10(%r14)
	movb	%cl, 10(%rbx)
	cmpq	$11, %r13
	je	.L68
	movzbl	11(%r12), %ecx
	movzbl	-69(%rbp), %edx
	xorb	11(%rbx), %dl
	movb	%dl, 11(%r14)
	movb	%cl, 11(%rbx)
	cmpq	$12, %r13
	je	.L68
	movzbl	12(%r12), %ecx
	movzbl	-68(%rbp), %edx
	xorb	12(%rbx), %dl
	movb	%dl, 12(%r14)
	movb	%cl, 12(%rbx)
	cmpq	$13, %r13
	je	.L68
	movzbl	13(%r12), %ecx
	movzbl	-67(%rbp), %edx
	xorb	13(%rbx), %dl
	movb	%dl, 13(%r14)
	movb	%cl, 13(%rbx)
	cmpq	$15, %r13
	jne	.L68
	movzbl	14(%r12), %edx
	movzbl	-66(%rbp), %eax
	xorb	14(%rbx), %al
	movb	%al, 14(%r14)
	movb	%dl, 14(%rbx)
	movzbl	15(%r12), %eax
	movb	%al, 15(%rbx)
.L58:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L157
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	testq	%r13, %r13
	movl	$1, %edx
	cmovne	%r13, %rdx
	movzbl	(%r12,%rdx), %eax
	movb	%al, (%rbx,%rdx)
	cmpq	$15, %rdx
	je	.L58
	movzbl	1(%r12,%rdx), %eax
	movb	%al, 1(%rbx,%rdx)
	cmpq	$14, %rdx
	je	.L58
	movzbl	2(%r12,%rdx), %eax
	movb	%al, 2(%rbx,%rdx)
	cmpq	$13, %rdx
	je	.L58
	movzbl	3(%r12,%rdx), %eax
	movb	%al, 3(%rbx,%rdx)
	cmpq	$12, %rdx
	je	.L58
	movzbl	4(%r12,%rdx), %eax
	movb	%al, 4(%rbx,%rdx)
	cmpq	$11, %rdx
	je	.L58
	movzbl	5(%r12,%rdx), %eax
	movb	%al, 5(%rbx,%rdx)
	cmpq	$10, %rdx
	je	.L58
	movzbl	6(%r12,%rdx), %eax
	movb	%al, 6(%rbx,%rdx)
	cmpq	$9, %rdx
	je	.L58
	movzbl	7(%r12,%rdx), %eax
	movb	%al, 7(%rbx,%rdx)
	cmpq	$8, %rdx
	je	.L58
	movzbl	8(%r12,%rdx), %eax
	movb	%al, 8(%rbx,%rdx)
	cmpq	$7, %rdx
	je	.L58
	movzbl	9(%r12,%rdx), %eax
	movb	%al, 9(%rbx,%rdx)
	cmpq	$6, %rdx
	je	.L58
	movzbl	10(%r12,%rdx), %eax
	movb	%al, 10(%rbx,%rdx)
	cmpq	$5, %rdx
	je	.L58
	movzbl	11(%r12,%rdx), %eax
	movb	%al, 11(%rbx,%rdx)
	cmpq	$4, %rdx
	je	.L58
	movzbl	12(%r12,%rdx), %eax
	movb	%al, 12(%rbx,%rdx)
	cmpq	$3, %rdx
	je	.L58
	movzbl	13(%r12,%rdx), %eax
	movb	%al, 13(%rbx,%rdx)
	cmpq	$2, %rdx
	je	.L58
	movzbl	15(%r12), %eax
	movb	%al, 15(%rbx)
	jmp	.L58
.L156:
	cmpq	$15, %r13
	jbe	.L158
	leaq	-16(%r13), %rcx
	movq	%rsi, %r15
	andq	$-16, %rcx
	leaq	16(%rsi,%rcx), %rax
	movq	%rax, -112(%rbp)
	movq	%rax, %r14
	leaq	-80(%rbp), %rax
	movq	%rax, -104(%rbp)
	.p2align 4,,10
	.p2align 3
.L66:
	movq	%rdx, -96(%rbp)
	movq	-104(%rbp), %rsi
	movq	%r15, %rdi
	addq	$16, %r12
	movq	%r9, -88(%rbp)
	addq	$16, %r15
	call	*%r9
	movq	-16(%r15), %rsi
	movq	-80(%rbp), %rcx
	xorq	(%rbx), %rcx
	movq	-88(%rbp), %r9
	movq	%rcx, -16(%r12)
	movq	-72(%rbp), %rcx
	xorq	8(%rbx), %rcx
	movq	-96(%rbp), %rdx
	movq	%rsi, (%rbx)
	movq	-8(%r15), %rsi
	movq	%rcx, -8(%r12)
	cmpq	-112(%rbp), %r12
	movq	%rsi, 8(%rbx)
	jne	.L66
	andl	$15, %r13d
	jmp	.L67
.L71:
	movq	-112(%rbp), %r12
	movq	%r8, %rcx
	movq	%rsi, %r14
	jmp	.L63
.L158:
	movq	%rdi, %r14
	movq	%rdi, %r12
	jmp	.L62
.L157:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE152:
	.size	CRYPTO_cbc128_decrypt, .-CRYPTO_cbc128_decrypt
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
