	.file	"rc2cfb64.c"
	.text
	.p2align 4
	.globl	RC2_cfb64_encrypt
	.type	RC2_cfb64_encrypt, @function
RC2_cfb64_encrypt:
.LFB0:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%r9, -96(%rbp)
	movl	(%r9), %eax
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movl	16(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L2
	leaq	(%rdi,%rdx), %r14
	leaq	-80(%rbp), %rdi
	testq	%rdx, %rdx
	je	.L4
	.p2align 4,,10
	.p2align 3
.L10:
	testl	%eax, %eax
	jne	.L8
	movd	(%r15), %xmm0
	movd	4(%r15), %xmm1
	movq	%r13, %rsi
	movq	%rdi, -88(%rbp)
	addq	$1, %rbx
	addq	$1, %r12
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	RC2_encrypt@PLT
	movl	-80(%rbp), %eax
	movl	%eax, (%r15)
	movl	-72(%rbp), %eax
	movl	%eax, 4(%r15)
	movzbl	-1(%rbx), %eax
	movb	%al, (%r15)
	xorb	-80(%rbp), %al
	movb	%al, -1(%r12)
	movl	$1, %eax
	cmpq	%r14, %rbx
	je	.L4
	movq	-88(%rbp), %rdi
.L8:
	movslq	%eax, %rsi
	movzbl	(%rbx), %edx
	addq	$1, %rbx
	addq	$1, %r12
	addq	%r15, %rsi
	addl	$1, %eax
	movzbl	(%rsi), %r8d
	movb	%dl, (%rsi)
	andl	$7, %eax
	xorl	%r8d, %edx
	movb	%dl, -1(%r12)
	cmpq	%r14, %rbx
	jne	.L10
.L4:
	movq	-96(%rbp), %rcx
	movl	%eax, (%rcx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L22
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	testq	%rdx, %rdx
	je	.L4
	leaq	-80(%rbp), %rcx
	leaq	(%rdi,%rdx), %r14
	movq	%rcx, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L7:
	testl	%eax, %eax
	jne	.L5
	movd	(%r15), %xmm0
	movd	4(%r15), %xmm1
	movq	%r13, %rsi
	addq	$1, %rbx
	movq	-88(%rbp), %rdi
	addq	$1, %r12
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	RC2_encrypt@PLT
	movl	-80(%rbp), %eax
	movl	%eax, (%r15)
	movl	-72(%rbp), %eax
	movl	%eax, 4(%r15)
	movq	-80(%rbp), %rax
	xorb	-1(%rbx), %al
	movb	%al, -1(%r12)
	movb	%al, (%r15)
	movl	$1, %eax
	cmpq	%r14, %rbx
	je	.L4
.L5:
	movslq	%eax, %rdx
	movzbl	(%rbx), %esi
	addq	$1, %rbx
	addq	$1, %r12
	addq	%r15, %rdx
	addl	$1, %eax
	xorb	(%rdx), %sil
	andl	$7, %eax
	movb	%sil, -1(%r12)
	movb	%sil, (%rdx)
	cmpq	%r14, %rbx
	jne	.L7
	jmp	.L4
.L22:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE0:
	.size	RC2_cfb64_encrypt, .-RC2_cfb64_encrypt
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
