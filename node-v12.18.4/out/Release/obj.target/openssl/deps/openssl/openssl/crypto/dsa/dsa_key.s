	.file	"dsa_key.c"
	.text
	.p2align 4
	.globl	DSA_generate_key
	.type	DSA_generate_key, @function
DSA_generate_key:
.LFB421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	80(%rdi), %rax
	movq	88(%rax), %rax
	testq	%rax, %rax
	je	.L2
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	call	BN_CTX_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L3
	movq	40(%r12), %r13
	testq	%r13, %r13
	jne	.L6
	call	BN_secure_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L6
	.p2align 4,,10
	.p2align 3
.L3:
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L31:
	movq	%r13, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L30
.L6:
	movq	16(%r12), %rsi
	movq	%r13, %rdi
	call	BN_priv_rand_range@PLT
	testl	%eax, %eax
	jne	.L31
	xorl	%r15d, %r15d
.L5:
	cmpq	%r15, 32(%r12)
	je	.L28
	movq	%r15, %rdi
	call	BN_free@PLT
.L28:
	movq	40(%r12), %rax
	xorl	%r12d, %r12d
	cmpq	%rax, %r13
	je	.L9
	movq	%r13, %rdi
	call	BN_free@PLT
.L9:
	movq	%r14, %rdi
	call	BN_CTX_free@PLT
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movq	32(%r12), %r15
	testq	%r15, %r15
	je	.L32
.L7:
	call	BN_new@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L5
	movl	$4, %edx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	BN_with_flags@PLT
	movq	8(%r12), %rcx
	movq	%r14, %r8
	movq	%rbx, %rdx
	movq	24(%r12), %rsi
	movq	%r15, %rdi
	call	BN_mod_exp@PLT
	testl	%eax, %eax
	je	.L33
	movq	%r13, %xmm1
	movq	%r15, %xmm0
	movq	%rbx, %rdi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	BN_free@PLT
	movdqa	-64(%rbp), %xmm0
	movups	%xmm0, 32(%r12)
	movl	$1, %r12d
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L33:
	movq	%rbx, %rdi
	call	BN_free@PLT
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L32:
	call	BN_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L7
	jmp	.L5
	.cfi_endproc
.LFE421:
	.size	DSA_generate_key, .-DSA_generate_key
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
