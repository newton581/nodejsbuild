	.file	"ecp_oct.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ec/ecp_oct.c"
	.text
	.p2align 4
	.globl	ec_GFp_simple_set_compressed_coordinates
	.type	ec_GFp_simple_set_compressed_coordinates, @function
ec_GFp_simple_set_compressed_coordinates:
.LFB377:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$56, %rsp
	movq	%rsi, -80(%rbp)
	movl	%ecx, -68(%rbp)
	call	ERR_clear_error@PLT
	testq	%r12, %r12
	je	.L89
.L2:
	movq	%r12, %rdi
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	BN_CTX_get@PLT
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L87
	movq	64(%r13), %rdx
	movq	-56(%rbp), %rdi
	movq	%r12, %rcx
	movq	%r14, %rsi
	call	BN_nnmod@PLT
	testl	%eax, %eax
	je	.L87
	movq	0(%r13), %rax
	cmpq	$0, 304(%rax)
	je	.L90
	movq	64(%r13), %rdx
	movq	%r12, %rcx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	BN_mod_sqr@PLT
	testl	%eax, %eax
	jne	.L91
	.p2align 4,,10
	.p2align 3
.L87:
	xorl	%r13d, %r13d
.L5:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	movq	%rbx, %rdi
	call	BN_CTX_free@PLT
.L1:
	addq	$56, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	call	BN_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L27
	movq	%rax, %rbx
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L90:
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	*272(%rax)
	testl	%eax, %eax
	je	.L87
	movq	0(%r13), %rax
	movq	-64(%rbp), %rsi
	movq	%r12, %r8
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L87
.L10:
	movl	112(%r13), %edx
	testl	%edx, %edx
	jne	.L92
	movq	0(%r13), %r9
	movq	96(%r13), %rdx
	movq	304(%r9), %rax
	testq	%rax, %rax
	je	.L13
	movq	%r12, %rcx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L87
	movq	64(%r13), %rcx
	movq	-56(%rbp), %rdx
	movq	%r12, %r8
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	BN_mod_mul@PLT
	testl	%eax, %eax
	je	.L87
.L15:
	movq	-64(%rbp), %rsi
	movq	64(%r13), %rcx
	movq	%r15, %rdx
	movq	%rsi, %rdi
	call	BN_mod_add_quick@PLT
	testl	%eax, %eax
	je	.L87
.L14:
	movq	0(%r13), %rax
	movq	104(%r13), %rdx
	movq	304(%rax), %rax
	testq	%rax, %rax
	je	.L93
	movq	%r12, %rcx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L87
	movq	64(%r13), %rcx
	movq	%r15, %rdx
.L85:
	movq	-64(%rbp), %rsi
	movq	%rsi, %rdi
	call	BN_mod_add_quick@PLT
	testl	%eax, %eax
	je	.L87
	movq	64(%r13), %rdx
	movq	-64(%rbp), %rsi
	movq	%r12, %rcx
	movq	-88(%rbp), %rdi
	call	BN_mod_sqrt@PLT
	testq	%rax, %rax
	je	.L94
	movl	-68(%rbp), %eax
	movq	-88(%rbp), %rdi
	xorl	%r14d, %r14d
	testl	%eax, %eax
	setne	%r14b
	call	BN_is_odd@PLT
	cmpl	%r14d, %eax
	je	.L25
	movq	-88(%rbp), %rdi
	call	BN_is_zero@PLT
	movq	64(%r13), %rsi
	testl	%eax, %eax
	je	.L23
	movq	-56(%rbp), %rdi
	movq	%r12, %rdx
	call	BN_kronecker@PLT
	cmpl	$-2, %eax
	je	.L87
	movl	$130, %r8d
	cmpl	$1, %eax
	je	.L95
.L86:
	leaq	.LC0(%rip), %rcx
	movl	$110, %edx
	movl	$169, %esi
	xorl	%r13d, %r13d
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L91:
	movq	64(%r13), %rcx
	movq	-64(%rbp), %rdi
	movq	%r12, %r8
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	BN_mod_mul@PLT
	testl	%eax, %eax
	jne	.L10
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L27:
	xorl	%r13d, %r13d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L92:
	movq	-56(%rbp), %r14
	movq	64(%r13), %rdx
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	BN_mod_lshift1_quick@PLT
	testl	%eax, %eax
	je	.L87
	movq	64(%r13), %rcx
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	BN_mod_add_quick@PLT
	testl	%eax, %eax
	je	.L87
	movq	-64(%rbp), %rsi
	movq	64(%r13), %rcx
	movq	%r15, %rdx
	movq	%rsi, %rdi
	call	BN_mod_sub_quick@PLT
	testl	%eax, %eax
	jne	.L14
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L13:
	movq	-56(%rbp), %rcx
	movq	%r12, %r8
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	*264(%r9)
	testl	%eax, %eax
	jne	.L15
	jmp	.L87
.L23:
	movq	-88(%rbp), %rdx
	movq	%rdx, %rdi
	call	BN_usub@PLT
	testl	%eax, %eax
	je	.L87
.L25:
	movq	-88(%rbp), %rdi
	call	BN_is_odd@PLT
	cmpl	%r14d, %eax
	je	.L96
	movl	$138, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$68, %edx
	xorl	%r13d, %r13d
	movl	$169, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L5
.L93:
	movq	64(%r13), %rcx
	jmp	.L85
.L96:
	movq	-88(%rbp), %rcx
	movq	-56(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r12, %r8
	movq	-80(%rbp), %rsi
	xorl	%r13d, %r13d
	call	EC_POINT_set_affine_coordinates@PLT
	testl	%eax, %eax
	setne	%r13b
	jmp	.L5
.L94:
	call	ERR_peek_last_error@PLT
	movl	%eax, %edx
	shrl	$24, %edx
	cmpl	$3, %edx
	jne	.L19
	andl	$4095, %eax
	cmpl	$111, %eax
	je	.L97
.L19:
	movl	$110, %r8d
	movl	$3, %edx
	movl	$169, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L87
.L95:
	movl	$124, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$109, %edx
	xorl	%r13d, %r13d
	movl	$169, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L5
.L97:
	call	ERR_clear_error@PLT
	movl	$107, %r8d
	jmp	.L86
	.cfi_endproc
.LFE377:
	.size	ec_GFp_simple_set_compressed_coordinates, .-ec_GFp_simple_set_compressed_coordinates
	.p2align 4
	.globl	ec_GFp_simple_point2oct
	.type	ec_GFp_simple_point2oct, @function
ec_GFp_simple_point2oct:
.LFB378:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %eax
	andl	$-5, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$56, %rsp
	movl	%eax, -64(%rbp)
	cmpl	$4, %edx
	je	.L99
	cmpl	$2, %eax
	jne	.L149
.L99:
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	EC_POINT_is_at_infinity@PLT
	movq	-56(%rbp), %rsi
	testl	%eax, %eax
	je	.L101
	testq	%r12, %r12
	je	.L126
	testq	%r15, %r15
	je	.L150
	movb	$0, (%r12)
	movl	$1, %r9d
.L98:
	addq	$56, %rsp
	movq	%r9, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L149:
	.cfi_restore_state
	movl	$167, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$104, %edx
	xorl	%r15d, %r15d
	movl	$104, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
.L100:
	movq	%r15, %rdi
	call	BN_CTX_free@PLT
	xorl	%r9d, %r9d
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L101:
	movq	64(%r14), %rdi
	movq	%rsi, -56(%rbp)
	call	BN_num_bits@PLT
	leal	14(%rax), %edx
	addl	$7, %eax
	cmovs	%edx, %eax
	sarl	$3, %eax
	cmpl	$2, %ebx
	movslq	%eax, %rcx
	leaq	1(%rcx), %rax
	leaq	1(%rcx,%rcx), %r9
	movq	%rcx, -72(%rbp)
	cmove	%rax, %r9
	testq	%r12, %r12
	je	.L106
	cmpq	%r15, %r9
	movq	-56(%rbp), %rsi
	ja	.L151
	xorl	%r15d, %r15d
	testq	%r13, %r13
	je	.L152
.L108:
	movq	%r13, %rdi
	movq	%rsi, -80(%rbp)
	movq	%r9, -88(%rbp)
	call	BN_CTX_start@PLT
	movq	%r13, %rdi
	call	BN_CTX_get@PLT
	movq	%r13, %rdi
	movq	%rax, -56(%rbp)
	call	BN_CTX_get@PLT
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L117
	movq	-56(%rbp), %rdx
	movq	-80(%rbp), %rsi
	movq	%r13, %r8
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	EC_POINT_get_affine_coordinates@PLT
	testl	%eax, %eax
	je	.L117
	cmpl	$2, -64(%rbp)
	movq	-88(%rbp), %r9
	movl	%ebx, %edx
	je	.L153
.L111:
	movb	%dl, (%r12)
	movq	-56(%rbp), %rdi
	movq	%r9, -64(%rbp)
	call	BN_num_bits@PLT
	movq	-72(%rbp), %r14
	movl	%eax, %edx
	leal	14(%rax), %eax
	addl	$7, %edx
	cmovns	%edx, %eax
	sarl	$3, %eax
	cltq
	subq	%rax, %r14
	jb	.L113
	testq	%r14, %r14
	movq	-64(%rbp), %r9
	je	.L154
	movq	%r14, %rdx
	leaq	1(%r12), %rdi
	xorl	%esi, %esi
	movq	%r9, -64(%rbp)
	call	memset@PLT
	movq	-64(%rbp), %r9
	addq	$1, %r14
.L116:
	movq	-56(%rbp), %rdi
	leaq	(%r12,%r14), %rsi
	movq	%r9, -64(%rbp)
	call	BN_bn2bin@PLT
	movq	-64(%rbp), %r9
	movl	$232, %r8d
	cltq
	addq	%rax, %r14
	movq	-72(%rbp), %rax
	addq	$1, %rax
	cmpq	%r14, %rax
	jne	.L148
	andl	$-3, %ebx
	cmpl	$4, %ebx
	je	.L155
.L119:
	cmpq	%r9, %r14
	jne	.L156
	movq	%r13, %rdi
	movq	%r9, -56(%rbp)
	movq	%r15, %r12
	call	BN_CTX_end@PLT
	movq	-56(%rbp), %r9
.L106:
	movq	%r12, %rdi
	movq	%r9, -56(%rbp)
	call	BN_CTX_free@PLT
	movq	-56(%rbp), %r9
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L126:
	movl	$1, %r9d
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L150:
	movl	$175, %r8d
	movl	$100, %edx
	movl	$104, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r9d, %r9d
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L156:
	movl	$252, %r8d
.L148:
	movl	$68, %edx
	movl	$104, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L117:
	movq	%r13, %rdi
	call	BN_CTX_end@PLT
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L153:
	movq	-96(%rbp), %rdi
	movq	%r9, -80(%rbp)
	movb	%bl, -64(%rbp)
	call	BN_is_odd@PLT
	movzbl	-64(%rbp), %edx
	movq	-80(%rbp), %r9
	movl	%eax, %r8d
	leal	1(%rbx), %eax
	testl	%r8d, %r8d
	cmovne	%eax, %edx
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L152:
	movq	%rsi, -80(%rbp)
	movq	%r9, -56(%rbp)
	call	BN_CTX_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L128
	movq	-80(%rbp), %rsi
	movq	-56(%rbp), %r9
	movq	%rax, %r15
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L151:
	movl	$192, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$100, %edx
	xorl	%r15d, %r15d
	movl	$104, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L155:
	movq	-96(%rbp), %rdi
	movq	%r9, -56(%rbp)
	call	BN_num_bits@PLT
	movq	-72(%rbp), %rbx
	movl	%eax, %edx
	leal	14(%rax), %eax
	addl	$7, %edx
	cmovns	%edx, %eax
	sarl	$3, %eax
	cltq
	subq	%rax, %rbx
	jb	.L121
	testq	%rbx, %rbx
	movq	-56(%rbp), %r9
	je	.L124
	leaq	(%r12,%r14), %rdi
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%r9, -56(%rbp)
	call	memset@PLT
	movq	-56(%rbp), %r9
	addq	%rbx, %r14
.L124:
	movq	-96(%rbp), %rdi
	leaq	(%r12,%r14), %rsi
	movq	%r9, -56(%rbp)
	call	BN_bn2bin@PLT
	movq	-56(%rbp), %r9
	cltq
	addq	%rax, %r14
	jmp	.L119
.L128:
	xorl	%r9d, %r9d
	jmp	.L98
.L154:
	movl	$1, %r14d
	jmp	.L116
.L121:
	movl	$240, %r8d
	jmp	.L148
.L113:
	movl	$222, %r8d
	jmp	.L148
	.cfi_endproc
.LFE378:
	.size	ec_GFp_simple_point2oct, .-ec_GFp_simple_point2oct
	.p2align 4
	.globl	ec_GFp_simple_oct2point
	.type	ec_GFp_simple_oct2point, @function
ec_GFp_simple_oct2point:
.LFB379:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rcx, %rcx
	je	.L200
	movq	%rdx, %rbx
	movzbl	(%rdx), %edx
	movq	%rcx, %r14
	movq	%rdi, %r9
	movq	%rsi, %r10
	movq	%r8, %r12
	movl	%edx, %ecx
	movl	%edx, %r15d
	movl	%edx, %eax
	andl	$1, %ecx
	andl	$254, %r15d
	andl	$252, %eax
	je	.L160
	movl	$289, %r8d
	cmpl	$4, %eax
	jne	.L198
	andl	$250, %edx
	jne	.L163
	testb	%cl, %cl
	jne	.L176
.L163:
	movq	64(%r9), %rdi
	movq	%r10, -72(%rbp)
	movb	%cl, -60(%rbp)
	movq	%r9, -56(%rbp)
	call	BN_num_bits@PLT
	movq	-56(%rbp), %r9
	movzbl	-60(%rbp), %ecx
	leal	14(%rax), %r13d
	addl	$7, %eax
	movq	-72(%rbp), %r10
	cmovns	%eax, %r13d
	sarl	$3, %r13d
	cmpl	$2, %r15d
	movl	%r13d, -80(%rbp)
	movslq	%r13d, %r13
	leaq	1(%r13), %rdx
	leaq	1(%r13,%r13), %rax
	cmove	%rdx, %rax
	cmpq	%r14, %rax
	jne	.L201
	movq	$0, -56(%rbp)
	testq	%r12, %r12
	je	.L202
.L168:
	movq	%r12, %rdi
	movq	%r10, -88(%rbp)
	movq	%r9, -72(%rbp)
	movb	%cl, -60(%rbp)
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	BN_CTX_get@PLT
	movzbl	-60(%rbp), %ecx
	movq	-72(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, -96(%rbp)
	movq	-88(%rbp), %r10
	je	.L197
	movl	-80(%rbp), %esi
	leaq	1(%rbx), %rdi
	movq	%r14, %rdx
	movq	%r10, -88(%rbp)
	movq	%r9, -72(%rbp)
	movb	%cl, -60(%rbp)
	call	BN_bin2bn@PLT
	testq	%rax, %rax
	je	.L197
	movq	-72(%rbp), %r9
	movq	%r14, %rdi
	movq	64(%r9), %rsi
	call	BN_ucmp@PLT
	movq	-72(%rbp), %r9
	movzbl	-60(%rbp), %ecx
	movl	$331, %r8d
	testl	%eax, %eax
	movq	-88(%rbp), %r10
	jns	.L196
	movzbl	%cl, %eax
	movl	%eax, -60(%rbp)
	cmpl	$2, %r15d
	je	.L203
	leaq	1(%rbx,%r13), %rdi
	movq	-96(%rbp), %rbx
	movl	-80(%rbp), %esi
	movq	%r10, -88(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rbx, %rdx
	call	BN_bin2bn@PLT
	testq	%rax, %rax
	je	.L197
	movq	-72(%rbp), %r9
	movq	%rbx, %rdi
	movq	64(%r9), %rsi
	call	BN_ucmp@PLT
	movq	-72(%rbp), %r9
	movq	-88(%rbp), %r10
	testl	%eax, %eax
	jns	.L204
	cmpl	$6, %r15d
	je	.L205
.L175:
	movq	-96(%rbp), %rcx
	movq	%r12, %r8
	movq	%r14, %rdx
	movq	%r10, %rsi
	movq	%r9, %rdi
	call	EC_POINT_set_affine_coordinates@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L160:
	andl	$250, %edx
	je	.L206
.L162:
	testl	%r15d, %r15d
	jne	.L163
	movl	$299, %r8d
	cmpq	$1, %r14
	je	.L207
.L198:
	movl	$102, %edx
	movl	$103, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L199:
	addq	$56, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L200:
	.cfi_restore_state
	movl	$280, %r8d
	movl	$100, %edx
	movl	$103, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$56, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L206:
	.cfi_restore_state
	testb	%cl, %cl
	je	.L162
	.p2align 4,,10
	.p2align 3
.L176:
	movl	$293, %r8d
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L201:
	movl	$312, %r8d
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L207:
	addq	$56, %rsp
	movq	%r10, %rsi
	movq	%r9, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	EC_POINT_set_to_infinity@PLT
.L204:
	.cfi_restore_state
	movl	$342, %r8d
.L196:
	movl	$102, %edx
	movl	$103, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L197:
	xorl	%eax, %eax
.L170:
	movq	%r12, %rdi
	movl	%eax, -60(%rbp)
	call	BN_CTX_end@PLT
	movq	-56(%rbp), %rdi
	call	BN_CTX_free@PLT
	movl	-60(%rbp), %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L202:
	.cfi_restore_state
	movq	%r10, -88(%rbp)
	movq	%r9, -72(%rbp)
	movb	%cl, -60(%rbp)
	call	BN_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L199
	movq	-88(%rbp), %r10
	movq	-72(%rbp), %r9
	movq	%rax, -56(%rbp)
	movzbl	-60(%rbp), %ecx
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L203:
	movq	%r12, %r8
	movl	%eax, %ecx
	movq	%r14, %rdx
	movq	%r10, %rsi
	movq	%r9, %rdi
	call	EC_POINT_set_compressed_coordinates@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L170
.L205:
	movq	-96(%rbp), %rdi
	movq	%r10, -80(%rbp)
	movq	%r9, -72(%rbp)
	call	BN_is_odd@PLT
	cmpl	-60(%rbp), %eax
	movq	-72(%rbp), %r9
	movl	$347, %r8d
	movq	-80(%rbp), %r10
	je	.L175
	jmp	.L196
	.cfi_endproc
.LFE379:
	.size	ec_GFp_simple_oct2point, .-ec_GFp_simple_oct2point
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
