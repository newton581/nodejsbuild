	.file	"pem_oth.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/pem/pem_oth.c"
	.text
	.p2align 4
	.globl	PEM_ASN1_read_bio
	.type	PEM_ASN1_read_bio, @function
PEM_ASN1_read_bio:
.LFB779:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	movq	%r10, %rcx
	pushq	%r12
	leaq	-32(%rbp), %rsi
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	-40(%rbp), %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	pushq	%r9
	movq	%r8, %r9
	movq	%rdx, %r8
	xorl	%edx, %edx
	movq	$0, -48(%rbp)
	movq	$0, -40(%rbp)
	call	PEM_bytes_read_bio@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	je	.L5
	movq	-40(%rbp), %rax
	movq	-32(%rbp), %rdx
	leaq	-48(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, -48(%rbp)
	call	*%r12
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L8
.L3:
	movq	-40(%rbp), %rdi
	movl	$34, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
.L1:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L9
	leaq	-16(%rbp), %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$33, %r8d
	movl	$13, %edx
	movl	$103, %esi
	movl	$9, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L3
.L9:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE779:
	.size	PEM_ASN1_read_bio, .-PEM_ASN1_read_bio
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
