	.file	"lhash.c"
	.text
	.p2align 4
	.type	getrn, @function
getrn:
.LFB224:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	call	*16(%r13)
	movq	%rax, %rcx
	lock addq	$1, 96(%r13)
	xorl	%edx, %edx
	movl	36(%r13), %esi
	movq	%rax, (%rbx)
	divq	%rsi
	movl	32(%r13), %eax
	cmpq	%rdx, %rax
	jbe	.L2
	movl	28(%r13), %esi
	movq	%rcx, %rax
	xorl	%edx, %edx
	divq	%rsi
.L2:
	movq	8(%r13), %rax
	movq	%rax, -64(%rbp)
	movq	0(%r13), %rax
	movslq	%edx, %rdx
	leaq	(%rax,%rdx,8), %r15
	movq	(%r15), %rbx
	testq	%rbx, %rbx
	je	.L1
	leaq	160(%r13), %r12
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L16:
	leaq	8(%rbx), %r15
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L1
.L6:
	lock addq	$1, (%r12)
	cmpq	%rcx, 16(%rbx)
	jne	.L16
	movq	%rcx, -56(%rbp)
	lock addq	$1, 104(%r13)
	movq	(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	%r14, %rsi
	call	*%rax
	testl	%eax, %eax
	je	.L1
	leaq	8(%rbx), %r15
	movq	8(%rbx), %rbx
	movq	-56(%rbp), %rcx
	testq	%rbx, %rbx
	jne	.L6
.L1:
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE224:
	.size	getrn, .-getrn
	.p2align 4
	.globl	OPENSSL_LH_strhash
	.type	OPENSSL_LH_strhash, @function
OPENSSL_LH_strhash:
.LFB225:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L20
	movzbl	(%rdi), %ecx
	xorl	%eax, %eax
	testb	%cl, %cl
	je	.L17
	movl	$256, %r8d
	movl	$32, %r9d
	.p2align 4,,10
	.p2align 3
.L19:
	movsbq	%cl, %rdx
	movl	%r9d, %r10d
	movq	%rax, %rsi
	addq	$1, %rdi
	orq	%r8, %rdx
	addq	$256, %r8
	movq	%rdx, %rcx
	shrq	$2, %rcx
	xorl	%edx, %ecx
	imulq	%rdx, %rdx
	andl	$15, %ecx
	subl	%ecx, %r10d
	salq	%cl, %rsi
	movl	%r10d, %ecx
	shrq	%cl, %rax
	movzbl	(%rdi), %ecx
	orl	%esi, %eax
	xorq	%rdx, %rax
	testb	%cl, %cl
	jne	.L19
	movq	%rax, %rdx
	shrq	$16, %rdx
	xorq	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	xorl	%eax, %eax
.L17:
	ret
	.cfi_endproc
.LFE225:
	.size	OPENSSL_LH_strhash, .-OPENSSL_LH_strhash
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/lhash/lhash.c"
	.text
	.p2align 4
	.globl	OPENSSL_LH_new
	.type	OPENSSL_LH_new, @function
OPENSSL_LH_new:
.LFB214:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$51, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	leaq	.LC0(%rip), %rsi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$176, %edi
	subq	$8, %rsp
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L24
	movl	$59, %edx
	leaq	.LC0(%rip), %rsi
	movl	$128, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L33
	testq	%r13, %r13
	je	.L34
.L27:
	leaq	OPENSSL_LH_strhash(%rip), %rax
	testq	%rbx, %rbx
	movq	%r13, 8(%r12)
	movdqa	.LC1(%rip), %xmm0
	movl	$8, 36(%r12)
	cmove	%rax, %rbx
	movabsq	$68719476744, %rax
	movq	%rax, 24(%r12)
	movq	%rbx, 16(%r12)
	movups	%xmm0, 40(%r12)
.L24:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	movq	strcmp@GOTPCREL(%rip), %r13
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L33:
	movl	$71, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movl	$72, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L24
	.cfi_endproc
.LFE214:
	.size	OPENSSL_LH_new, .-OPENSSL_LH_new
	.p2align 4
	.globl	OPENSSL_LH_free
	.type	OPENSSL_LH_free, @function
OPENSSL_LH_free:
.LFB215:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L35
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	24(%rdi), %eax
	movq	(%rdi), %rdi
	testl	%eax, %eax
	je	.L37
	xorl	%r13d, %r13d
	leaq	.LC0(%rip), %r12
	.p2align 4,,10
	.p2align 3
.L40:
	movl	%r13d, %edx
	movq	(%rdi,%rdx,8), %rbx
	testq	%rbx, %rbx
	je	.L38
	.p2align 4,,10
	.p2align 3
.L39:
	movq	%rbx, %rdi
	movq	8(%rbx), %rbx
	movl	$88, %edx
	movq	%r12, %rsi
	call	CRYPTO_free@PLT
	testq	%rbx, %rbx
	jne	.L39
	movl	24(%r14), %eax
	movq	(%r14), %rdi
.L38:
	addl	$1, %r13d
	cmpl	%eax, %r13d
	jb	.L40
.L37:
	movl	$92, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	popq	%rbx
	.cfi_restore 3
	movq	%r14, %rdi
	popq	%r12
	.cfi_restore 12
	movl	$93, %edx
	popq	%r13
	.cfi_restore 13
	leaq	.LC0(%rip), %rsi
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
.L35:
	ret
	.cfi_endproc
.LFE215:
	.size	OPENSSL_LH_free, .-OPENSSL_LH_free
	.p2align 4
	.globl	OPENSSL_LH_insert
	.type	OPENSSL_LH_insert, @function
OPENSSL_LH_insert:
.LFB216:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movl	24(%rdi), %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	56(%rdi), %rax
	movl	$0, 168(%rdi)
	salq	$8, %rax
	divq	%rsi
	cmpq	%rax, 40(%rdi)
	ja	.L60
	movl	32(%rdi), %r15d
	movl	36(%rdi), %r13d
	movl	28(%rdi), %r14d
	movq	(%rdi), %rdi
	leal	1(%r15), %eax
	cmpl	%eax, %r13d
	jbe	.L75
	movl	%eax, 32(%rbx)
	movq	%rsi, %rcx
.L59:
	movl	%r15d, %r10d
	leal	(%r15,%r13), %eax
	addl	$1, %ecx
	addq	$1, 64(%rbx)
	leaq	(%rdi,%r10,8), %r8
	leaq	(%rdi,%rax,8), %r11
	movl	%ecx, 24(%rbx)
	movq	$0, (%r11)
	movq	(%r8), %rsi
	testq	%rsi, %rsi
	je	.L60
	movl	%r14d, %r9d
	movq	%rsi, %rdi
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L76:
	movq	%rcx, (%r8)
	movq	(%r11), %rax
	movq	%rax, 8(%rsi)
	movq	%rsi, (%r11)
	movq	(%r8), %rcx
	testq	%rcx, %rcx
	je	.L60
.L62:
	movq	%rcx, %rdi
	movq	%rcx, %rsi
.L63:
	movq	16(%rsi), %rax
	xorl	%edx, %edx
	movq	8(%rdi), %rcx
	divq	%r9
	cmpq	%rdx, %r10
	jne	.L76
	leaq	8(%rdi), %r8
	testq	%rcx, %rcx
	jne	.L62
	.p2align 4,,10
	.p2align 3
.L60:
	leaq	-64(%rbp), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	getrn
	movq	(%rax), %rdx
	movq	%rax, %r13
	testq	%rdx, %rdx
	je	.L77
	movq	(%rdx), %rax
	movq	%r12, (%rdx)
	addq	$1, 120(%rbx)
.L52:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L78
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	leal	(%r14,%r14), %esi
	movl	$225, %ecx
	leaq	.LC0(%rip), %rdx
	movl	%esi, -68(%rbp)
	salq	$3, %rsi
	call	CRYPTO_realloc@PLT
	testq	%rax, %rax
	je	.L74
	movl	-68(%rbp), %edx
	movq	%rax, (%rbx)
	movl	%r14d, %ecx
	xorl	%esi, %esi
	leaq	(%rax,%rcx,8), %rdi
	subl	%r14d, %edx
	salq	$3, %rdx
	call	memset@PLT
	movl	-68(%rbp), %eax
	movl	24(%rbx), %ecx
	movl	%r14d, 36(%rbx)
	addq	$1, 72(%rbx)
	movq	(%rbx), %rdi
	movl	%eax, 28(%rbx)
	movl	$0, 32(%rbx)
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L77:
	movl	$109, %edx
	leaq	.LC0(%rip), %rsi
	movl	$24, %edi
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L74
	movq	-64(%rbp), %rdx
	movq	%r12, (%rax)
	movq	$0, 8(%rax)
	movq	%rdx, 16(%rax)
	movq	%rax, 0(%r13)
	xorl	%eax, %eax
	addq	$1, 112(%rbx)
	addq	$1, 56(%rbx)
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L74:
	addl	$1, 168(%rbx)
	jmp	.L52
.L78:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE216:
	.size	OPENSSL_LH_insert, .-OPENSSL_LH_insert
	.p2align 4
	.globl	OPENSSL_LH_delete
	.type	OPENSSL_LH_delete, @function
OPENSSL_LH_delete:
.LFB217:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-48(%rbp), %rdx
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, 168(%rdi)
	call	getrn
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L90
	movq	8(%rdi), %rdx
	leaq	.LC0(%rip), %rsi
	movq	%rdx, (%rax)
	movl	$144, %edx
	movq	(%rdi), %r12
	call	CRYPTO_free@PLT
	movq	56(%rbx), %rax
	movl	24(%rbx), %ecx
	addq	$1, 128(%rbx)
	subq	$1, %rax
	movq	%rax, 56(%rbx)
	cmpl	$16, %ecx
	ja	.L91
.L79:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L92
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	salq	$8, %rax
	movl	%ecx, %esi
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rax, 48(%rbx)
	jb	.L79
	movl	32(%rbx), %edx
	movl	36(%rbx), %eax
	movq	(%rbx), %rdi
	leal	-1(%rdx,%rax), %esi
	leaq	(%rdi,%rsi,8), %rsi
	movq	(%rsi), %r13
	movq	$0, (%rsi)
	testl	%edx, %edx
	je	.L93
	subl	$1, %edx
	movl	%edx, 32(%rbx)
.L84:
	movslq	%edx, %rdx
	subl	$1, %ecx
	addq	$1, 80(%rbx)
	leaq	(%rdi,%rdx,8), %rdx
	movl	%ecx, 24(%rbx)
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L94
	.p2align 4,,10
	.p2align 3
.L85:
	movq	%rax, %rdx
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.L85
	movq	%r13, 8(%rdx)
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L90:
	addq	$1, 136(%rbx)
	xorl	%r12d, %r12d
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L93:
	leal	0(,%rax,8), %esi
	movl	$267, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_realloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L95
	movl	36(%rbx), %edx
	addq	$1, 88(%rbx)
	movq	%rax, (%rbx)
	movl	24(%rbx), %ecx
	shrl	%edx
	shrl	28(%rbx)
	movl	%edx, 36(%rbx)
	subl	$1, %edx
	movl	%edx, 32(%rbx)
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L94:
	movq	%r13, (%rdx)
	jmp	.L79
.L95:
	addl	$1, 168(%rbx)
	jmp	.L79
.L92:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE217:
	.size	OPENSSL_LH_delete, .-OPENSSL_LH_delete
	.p2align 4
	.globl	OPENSSL_LH_retrieve
	.type	OPENSSL_LH_retrieve, @function
OPENSSL_LH_retrieve:
.LFB218:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	-32(%rbp), %rdx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, 168(%rdi)
	call	getrn
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L101
	movq	(%rax), %rax
	lock addq	$1, 144(%rbx)
.L96:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L102
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	lock addq	$1, 152(%rbx)
	jmp	.L96
.L102:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE218:
	.size	OPENSSL_LH_retrieve, .-OPENSSL_LH_retrieve
	.p2align 4
	.globl	OPENSSL_LH_doall
	.type	OPENSSL_LH_doall, @function
OPENSSL_LH_doall:
.LFB220:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L117
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	24(%rdi), %r13d
	subl	$1, %r13d
	js	.L103
	movslq	%r13d, %r14
	movq	%rsi, %r12
	salq	$3, %r14
	.p2align 4,,10
	.p2align 3
.L107:
	movq	(%r15), %rax
	movq	(%rax,%r14), %rbx
	testq	%rbx, %rbx
	je	.L105
	.p2align 4,,10
	.p2align 3
.L106:
	movq	%rbx, %rax
	movq	8(%rbx), %rbx
	movq	(%rax), %rdi
	call	*%r12
	testq	%rbx, %rbx
	jne	.L106
.L105:
	subl	$1, %r13d
	subq	$8, %r14
	cmpl	$-1, %r13d
	jne	.L107
.L103:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
.L117:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE220:
	.size	OPENSSL_LH_doall, .-OPENSSL_LH_doall
	.p2align 4
	.globl	OPENSSL_LH_doall_arg
	.type	OPENSSL_LH_doall_arg, @function
OPENSSL_LH_doall_arg:
.LFB221:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -56(%rbp)
	testq	%rdi, %rdi
	je	.L120
	movl	24(%rdi), %eax
	movl	%eax, %r13d
	movl	%eax, -60(%rbp)
	subl	$1, %r13d
	js	.L120
	movslq	%r13d, %r14
	movq	%rsi, %rbx
	movq	%rdx, %r12
	salq	$3, %r14
	.p2align 4,,10
	.p2align 3
.L124:
	movq	-56(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax,%r14), %r15
	testq	%r15, %r15
	je	.L122
	.p2align 4,,10
	.p2align 3
.L123:
	movq	%r15, %rdx
	movq	8(%r15), %r15
	movq	%r12, %rsi
	movq	(%rdx), %rdi
	call	*%rbx
	testq	%r15, %r15
	jne	.L123
.L122:
	subl	$1, %r13d
	subq	$8, %r14
	cmpl	$-1, %r13d
	jne	.L124
.L120:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE221:
	.size	OPENSSL_LH_doall_arg, .-OPENSSL_LH_doall_arg
	.p2align 4
	.globl	openssl_lh_strcasehash
	.type	openssl_lh_strcasehash, @function
openssl_lh_strcasehash:
.LFB226:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L137
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movsbl	(%rdi), %edi
	testb	%dil, %dil
	je	.L134
	movl	$256, %r13d
	xorl	%r14d, %r14d
	movl	$32, %ebx
	.p2align 4,,10
	.p2align 3
.L136:
	call	ossl_tolower@PLT
	movl	%ebx, %esi
	movq	%r14, %rdx
	movsbl	1(%r12), %edi
	cltq
	addq	$1, %r12
	orq	%r13, %rax
	addq	$256, %r13
	movq	%rax, %rcx
	shrq	$2, %rcx
	xorl	%eax, %ecx
	imulq	%rax, %rax
	andl	$15, %ecx
	subl	%ecx, %esi
	salq	%cl, %rdx
	movl	%esi, %ecx
	shrq	%cl, %r14
	orl	%edx, %r14d
	xorq	%rax, %r14
	testb	%dil, %dil
	jne	.L136
	movq	%r14, %rax
	shrq	$16, %rax
	xorq	%r14, %rax
.L134:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE226:
	.size	openssl_lh_strcasehash, .-openssl_lh_strcasehash
	.p2align 4
	.globl	OPENSSL_LH_num_items
	.type	OPENSSL_LH_num_items, @function
OPENSSL_LH_num_items:
.LFB227:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L145
	movq	56(%rdi), %rax
.L145:
	ret
	.cfi_endproc
.LFE227:
	.size	OPENSSL_LH_num_items, .-OPENSSL_LH_num_items
	.p2align 4
	.globl	OPENSSL_LH_get_down_load
	.type	OPENSSL_LH_get_down_load, @function
OPENSSL_LH_get_down_load:
.LFB228:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	ret
	.cfi_endproc
.LFE228:
	.size	OPENSSL_LH_get_down_load, .-OPENSSL_LH_get_down_load
	.p2align 4
	.globl	OPENSSL_LH_set_down_load
	.type	OPENSSL_LH_set_down_load, @function
OPENSSL_LH_set_down_load:
.LFB229:
	.cfi_startproc
	endbr64
	movq	%rsi, 48(%rdi)
	ret
	.cfi_endproc
.LFE229:
	.size	OPENSSL_LH_set_down_load, .-OPENSSL_LH_set_down_load
	.p2align 4
	.globl	OPENSSL_LH_error
	.type	OPENSSL_LH_error, @function
OPENSSL_LH_error:
.LFB230:
	.cfi_startproc
	endbr64
	movl	168(%rdi), %eax
	ret
	.cfi_endproc
.LFE230:
	.size	OPENSSL_LH_error, .-OPENSSL_LH_error
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.quad	512
	.quad	256
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
