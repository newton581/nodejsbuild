	.file	"b_print.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/bio/b_print.c"
	.text
	.p2align 4
	.type	doapr_outch, @function
doapr_outch:
.LFB258:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	subq	$16, %rsp
	movq	(%rdi), %rcx
	testq	%rcx, %rcx
	je	.L32
	movq	(%rdx), %rdx
	movq	0(%r13), %rax
	cmpq	%rax, %rdx
	ja	.L29
	testq	%rsi, %rsi
	je	.L6
	cmpq	%rax, %rdx
	je	.L33
.L7:
	cmpq	%rax, %rdx
	jnb	.L28
	movq	(%r12), %rcx
	testq	%rcx, %rcx
	je	.L11
.L14:
	leaq	1(%rdx), %rax
	movq	%rax, (%rbx)
	movb	%r8b, (%rcx,%rdx)
.L28:
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L35:
	.cfi_restore_state
	movl	$823, %r8d
	movl	$65, %edx
	movl	$150, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L29:
	xorl	%eax, %eax
.L1:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L29
	movq	(%rdx), %rdx
	movq	0(%r13), %rax
	cmpq	%rax, %rdx
	ja	.L29
	cmpq	%rax, %rdx
	jne	.L7
.L33:
	cmpq	$2147482623, %rdx
	ja	.L29
	movq	(%r14), %rdi
	leaq	1024(%rdx), %r9
	movl	%r8d, -36(%rbp)
	movq	%r9, 0(%r13)
	testq	%rdi, %rdi
	je	.L34
	movl	$834, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r9, %rsi
	call	CRYPTO_realloc@PLT
	testq	%rax, %rax
	je	.L29
	movq	(%rbx), %rdx
	movl	-36(%rbp), %r8d
	movq	%rax, (%r14)
	movq	0(%r13), %rax
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L6:
	cmpq	%rax, %rdx
	jb	.L14
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L34:
	movl	$822, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r9, %rdi
	call	CRYPTO_malloc@PLT
	movl	-36(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, (%r14)
	je	.L35
	movq	(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L10
	movq	(%r12), %rsi
	movl	%r8d, -36(%rbp)
	testq	%rsi, %rsi
	je	.L29
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	(%rbx), %rdx
	movl	-36(%rbp), %r8d
.L10:
	movq	$0, (%r12)
	cmpq	%rdx, 0(%r13)
	jbe	.L28
	.p2align 4,,10
	.p2align 3
.L11:
	movq	(%r14), %rax
	leaq	1(%rdx), %rcx
	movq	%rcx, (%rbx)
	movb	%r8b, (%rax,%rdx)
	movl	$1, %eax
	jmp	.L1
	.cfi_endproc
.LFE258:
	.size	doapr_outch, .-doapr_outch
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"0"
.LC2:
	.string	"0x"
.LC3:
	.string	""
.LC4:
	.string	"0123456789ABCDEF"
.LC5:
	.string	"0123456789abcdef"
	.text
	.p2align 4
	.type	fmtint, @function
fmtint:
.LFB253:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$120, %rsp
	movl	24(%rbp), %r15d
	movl	32(%rbp), %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r15d, %r15d
	cmovns	%r15d, %eax
	movl	%eax, -104(%rbp)
	testb	$64, %sil
	jne	.L83
	testq	%r8, %r8
	js	.L153
	testb	$2, %sil
	jne	.L84
	movl	%esi, %eax
	movl	$0, -112(%rbp)
	andl	$4, %eax
	movl	%eax, -140(%rbp)
	je	.L37
	movl	$1, -112(%rbp)
	movl	$32, -140(%rbp)
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L83:
	movl	$0, -112(%rbp)
	movl	$0, -140(%rbp)
.L37:
	leaq	.LC3(%rip), %r11
	testb	$8, %sil
	je	.L39
	leaq	.LC1(%rip), %r11
	cmpl	$8, %r9d
	je	.L39
	cmpl	$16, %r9d
	leaq	.LC2(%rip), %r11
	leaq	.LC3(%rip), %rax
	cmovne	%rax, %r11
.L39:
	testb	$32, %sil
	leaq	.LC4(%rip), %r14
	movslq	%r9d, %rdi
	leaq	.LC5(%rip), %rax
	movl	$1, %ecx
	leaq	-97(%rbp), %r9
	cmove	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L40:
	movq	%r8, %rax
	xorl	%edx, %edx
	movl	%ecx, %r15d
	divq	%rdi
	movzbl	(%r14,%rdx), %edx
	movb	%dl, (%r9,%rcx)
	movq	%r8, %rdx
	movq	%rax, %r8
	cmpq	%rdi, %rdx
	setnb	%dl
	cmpl	$25, %ecx
	setle	%al
	addq	$1, %rcx
	testb	%al, %dl
	jne	.L40
	cmpl	$26, %r15d
	movl	$25, %eax
	movq	%r11, %rdi
	movl	%esi, 32(%rbp)
	cmove	%eax, %r15d
	movq	%r10, -136(%rbp)
	movl	-104(%rbp), %r14d
	movq	%r9, -128(%rbp)
	movslq	%r15d, %rax
	movq	%r11, -120(%rbp)
	subl	%r15d, %r14d
	movq	%rax, -152(%rbp)
	movb	$0, -96(%rbp,%rax)
	call	strlen@PLT
	movl	-104(%rbp), %esi
	movl	16(%rbp), %edx
	movq	-120(%rbp), %r11
	movq	-128(%rbp), %r9
	cmpl	%esi, %r15d
	movq	-136(%rbp), %r10
	cmovl	%esi, %r15d
	movl	32(%rbp), %esi
	xorl	%r8d, %r8d
	subl	%r15d, %edx
	subl	-112(%rbp), %edx
	testl	%r14d, %r14d
	movl	%esi, %r15d
	cmovs	%r8d, %r14d
	subl	%eax, %edx
	cmovns	%edx, %r8d
	andl	$1, %r15d
	andl	$16, %esi
	je	.L42
	cmpl	%r8d, %r14d
	cmovl	%r8d, %r14d
	xorl	%r15d, %r15d
.L44:
	movl	-140(%rbp), %eax
	testl	%eax, %eax
	jne	.L154
.L59:
	movzbl	(%r11), %r8d
	testb	%r8b, %r8b
	je	.L73
	movl	%r14d, -104(%rbp)
	movq	%r10, %r14
	movl	%r15d, -112(%rbp)
	movq	%r13, %r15
	movq	%rbx, %r13
	movq	%r11, %rbx
	movq	%r9, -120(%rbp)
	movq	%r12, %r9
	movl	%r8d, %r12d
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L60:
	movq	0(%r13), %rdx
	movq	(%r15), %rax
	cmpq	%rax, %rdx
	ja	.L151
	testq	%r14, %r14
	je	.L62
.L79:
	cmpq	%rax, %rdx
	je	.L155
.L63:
	cmpq	%rdx, %rax
	jbe	.L68
	movq	(%r9), %rcx
	testq	%rcx, %rcx
	je	.L67
.L80:
	leaq	1(%rdx), %rax
	movq	%rax, 0(%r13)
	movb	%r12b, (%rcx,%rdx)
.L68:
	movzbl	1(%rbx), %r12d
	addq	$1, %rbx
	testb	%r12b, %r12b
	je	.L156
.L70:
	movq	(%r9), %rcx
	testq	%rcx, %rcx
	jne	.L60
	testq	%r14, %r14
	je	.L151
	movq	0(%r13), %rdx
	movq	(%r15), %rax
	cmpq	%rax, %rdx
	jbe	.L79
	.p2align 4,,10
	.p2align 3
.L151:
	xorl	%eax, %eax
.L36:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L157
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	testl	%r15d, %r15d
	jne	.L158
	testl	%edx, %edx
	jle	.L145
	movq	%r11, -112(%rbp)
	leaq	.LC0(%rip), %r15
	movq	%r13, %r11
	movq	%r10, %r13
	movl	%r14d, -120(%rbp)
	movq	%rbx, %r14
	movl	%r8d, %ebx
	movq	%r9, -128(%rbp)
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L47:
	movq	(%r14), %rdx
	movq	(%r11), %rax
	cmpq	%rax, %rdx
	ja	.L151
	testq	%r13, %r13
	je	.L51
	cmpq	%rax, %rdx
	je	.L159
.L52:
	cmpq	%rdx, %rax
	jbe	.L57
	movq	(%r12), %rcx
	testq	%rcx, %rcx
	je	.L56
.L81:
	leaq	1(%rdx), %rax
	movq	%rax, (%r14)
	movb	$32, (%rcx,%rdx)
.L57:
	subl	$1, %ebx
	je	.L160
.L46:
	movq	(%r12), %rcx
	testq	%rcx, %rcx
	jne	.L47
	testq	%r13, %r13
	je	.L151
	movq	(%r14), %rdx
	movq	(%r11), %rax
	cmpq	%rax, %rdx
	ja	.L151
	cmpq	%rax, %rdx
	jne	.L52
.L159:
	cmpq	$2147482623, %rdx
	ja	.L151
	movq	0(%r13), %r8
	leaq	1024(%rdx), %r9
	movq	%r11, -104(%rbp)
	movq	%r9, (%r11)
	testq	%r8, %r8
	je	.L161
	movl	$834, %ecx
	movq	%r15, %rdx
	movq	%r9, %rsi
	movq	%r8, %rdi
	call	CRYPTO_realloc@PLT
	testq	%rax, %rax
	je	.L151
	movq	-104(%rbp), %r11
	movq	(%r14), %rdx
	movq	%rax, 0(%r13)
	movq	(%r11), %rax
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L158:
	movl	-140(%rbp), %eax
	movl	%r8d, %r15d
	negl	%r15d
	testl	%eax, %eax
	je	.L59
.L154:
	movl	-140(%rbp), %r8d
	movq	%r10, %rsi
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%r9, -120(%rbp)
	movq	%r11, -112(%rbp)
	movq	%r10, -104(%rbp)
	call	doapr_outch
	movq	-104(%rbp), %r10
	movq	-112(%rbp), %r11
	testl	%eax, %eax
	movq	-120(%rbp), %r9
	jne	.L59
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L153:
	movl	$1, -112(%rbp)
	negq	%r8
	movl	$45, -140(%rbp)
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L51:
	cmpq	%rax, %rdx
	jb	.L81
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L62:
	cmpq	%rax, %rdx
	jb	.L80
	movzbl	1(%rbx), %r12d
	addq	$1, %rbx
	testb	%r12b, %r12b
	jne	.L70
	.p2align 4,,10
	.p2align 3
.L156:
	movq	%r9, %r12
	movq	%r14, %r10
	movq	%r13, %rbx
	movl	-104(%rbp), %r14d
	movq	%r15, %r13
	movq	-120(%rbp), %r9
	movl	-112(%rbp), %r15d
.L73:
	testl	%r14d, %r14d
	je	.L72
	movq	%r9, -112(%rbp)
	movl	%r15d, -104(%rbp)
	movq	%r10, %r15
	.p2align 4,,10
	.p2align 3
.L71:
	movl	$48, %r8d
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	doapr_outch
	testl	%eax, %eax
	je	.L151
	subl	$1, %r14d
	jne	.L71
	movq	%r15, %r10
	movq	-112(%rbp), %r9
	movl	-104(%rbp), %r15d
.L72:
	movq	%r9, -104(%rbp)
	movq	-152(%rbp), %r14
	movl	%r15d, -112(%rbp)
	movq	%r10, %r15
	.p2align 4,,10
	.p2align 3
.L75:
	movq	-104(%rbp), %rax
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movsbl	(%rax,%r14), %r8d
	call	doapr_outch
	testl	%eax, %eax
	je	.L151
	subq	$1, %r14
	testl	%r14d, %r14d
	jne	.L75
	movq	%r15, %r10
	movl	-112(%rbp), %r15d
	movq	%r10, %r14
	testl	%r15d, %r15d
	jne	.L76
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L162:
	addl	$1, %r15d
	je	.L77
.L76:
	movl	$32, %r8d
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	doapr_outch
	testl	%eax, %eax
	jne	.L162
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L161:
	movl	$822, %edx
	movq	%r15, %rsi
	movq	%r9, %rdi
	call	CRYPTO_malloc@PLT
	movq	-104(%rbp), %r11
	testq	%rax, %rax
	movq	%rax, 0(%r13)
	je	.L65
	movq	(%r14), %rdx
	testq	%rdx, %rdx
	je	.L55
	movq	(%r12), %rsi
	movq	%r11, -104(%rbp)
	testq	%rsi, %rsi
	je	.L151
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	(%r14), %rdx
	movq	-104(%rbp), %r11
.L55:
	movq	$0, (%r12)
	cmpq	(%r11), %rdx
	jnb	.L57
	.p2align 4,,10
	.p2align 3
.L56:
	movq	0(%r13), %rax
	leaq	1(%rdx), %rcx
	movq	%rcx, (%r14)
	movb	$32, (%rax,%rdx)
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L155:
	cmpq	$2147482623, %rdx
	ja	.L151
	movq	(%r14), %r8
	leaq	1024(%rdx), %r10
	movq	%r9, -128(%rbp)
	movq	%r10, (%r15)
	testq	%r8, %r8
	je	.L163
	movl	$834, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r10, %rsi
	movq	%r8, %rdi
	call	CRYPTO_realloc@PLT
	testq	%rax, %rax
	je	.L151
	movq	0(%r13), %rdx
	movq	-128(%rbp), %r9
	movq	%rax, (%r14)
	movq	(%r15), %rax
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L163:
	movl	$822, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r10, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, (%r14)
	testq	%rax, %rax
	je	.L65
	movq	0(%r13), %rdx
	movq	-128(%rbp), %r9
	testq	%rdx, %rdx
	je	.L66
	movq	(%r9), %rsi
	testq	%rsi, %rsi
	je	.L151
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	0(%r13), %rdx
	movq	-128(%rbp), %r9
.L66:
	movq	$0, (%r9)
	cmpq	%rdx, (%r15)
	jbe	.L68
	.p2align 4,,10
	.p2align 3
.L67:
	movq	(%r14), %rax
	leaq	1(%rdx), %rcx
	movq	%rcx, 0(%r13)
	movb	%r12b, (%rax,%rdx)
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L84:
	movl	$1, -112(%rbp)
	movl	$43, -140(%rbp)
	jmp	.L37
.L77:
	movl	$1, %eax
	jmp	.L36
.L65:
	movl	$823, %r8d
	movl	$65, %edx
	movl	$150, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L151
.L160:
	movq	%r13, %r10
	movq	%r14, %rbx
	movq	%r11, %r13
	movl	-120(%rbp), %r14d
	movq	-112(%rbp), %r11
	movq	-128(%rbp), %r9
	xorl	%r15d, %r15d
	jmp	.L44
.L145:
	movl	%r8d, %r15d
	jmp	.L44
.L157:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE253:
	.size	fmtint, .-fmtint
	.section	.rodata.str1.1
.LC22:
	.string	"0123456789"
	.text
	.p2align 4
	.type	fmtfp, @function
fmtfp:
.LFB257:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm3, %xmm3
	movl	%r8d, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	movl	$6, %eax
	cmovs	%eax, %r9d
	comisd	%xmm0, %xmm3
	ja	.L166
	testb	$2, 16(%rbp)
	je	.L382
	movl	$1, -168(%rbp)
	movl	$43, -180(%rbp)
.L167:
	cmpl	$2, 24(%rbp)
	je	.L383
.L168:
	movl	24(%rbp), %esi
	testl	%esi, %esi
	jne	.L173
	movl	$0, -164(%rbp)
	movq	$0, -176(%rbp)
.L174:
	comisd	%xmm0, %xmm3
	jbe	.L185
	xorpd	.LC18(%rip), %xmm0
.L185:
	comisd	.LC19(%rip), %xmm0
	ja	.L187
	movsd	.LC20(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L188
	cvttsd2siq	%xmm0, %rsi
.L189:
	movl	$9, %eax
	cmpl	$9, %r9d
	movl	%eax, %ebx
	cmovle	%r9d, %ebx
	movl	%ebx, -152(%rbp)
	testl	%r9d, %r9d
	je	.L260
	movl	%ebx, %eax
	subl	$1, %eax
	je	.L261
	cmpl	$2, %ebx
	je	.L262
	cmpl	$3, %ebx
	je	.L263
	cmpl	$4, %ebx
	je	.L264
	cmpl	$5, %ebx
	je	.L265
	cmpl	$6, %ebx
	je	.L266
	subl	$7, %ebx
	je	.L267
	movsd	.LC7(%rip), %xmm1
	cmpl	$1, %ebx
	je	.L384
.L191:
	cvttsd2siq	%xmm1, %rdx
	movsd	.LC15(%rip), %xmm2
	testl	%eax, %eax
	je	.L190
	movl	-152(%rbp), %eax
	cmpl	$2, %eax
	je	.L269
	cmpl	$3, %eax
	je	.L270
	cmpl	$4, %eax
	je	.L271
	cmpl	$5, %eax
	je	.L272
	cmpl	$6, %eax
	je	.L273
	subl	$7, %eax
	je	.L274
	movsd	.LC7(%rip), %xmm2
	cmpl	$1, %eax
	je	.L385
.L190:
	testq	%rsi, %rsi
	js	.L193
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rsi, %xmm1
.L194:
	subsd	%xmm1, %xmm0
	pxor	%xmm1, %xmm1
	mulsd	%xmm2, %xmm0
	cvttsd2siq	%xmm0, %rdi
	cvtsi2sdq	%rdi, %xmm1
	subsd	%xmm1, %xmm0
	comisd	.LC21(%rip), %xmm0
	sbbq	$-1, %rdi
	cmpq	%rdx, %rdi
	jb	.L197
	addq	$1, %rsi
	subq	%rdx, %rdi
.L197:
	leaq	-145(%rbp), %rax
	movl	%r9d, -200(%rbp)
	movl	$1, %ecx
	leaq	.LC22(%rip), %r8
	movq	%rax, -160(%rbp)
	movabsq	$-3689348814741910323, %rbx
	movq	%rdi, -192(%rbp)
	movq	%rax, %rdi
	.p2align 4,,10
	.p2align 3
.L198:
	movq	%rsi, %rax
	movq	%rsi, %r9
	movl	%ecx, %r11d
	mulq	%rbx
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %r9
	movzbl	(%r8,%r9), %eax
	movb	%al, (%rdi,%rcx)
	movq	%rsi, %rax
	movq	%rdx, %rsi
	cmpq	$9, %rax
	seta	%dl
	cmpl	$19, %ecx
	setle	%al
	addq	$1, %rcx
	testb	%al, %dl
	jne	.L198
	movl	$19, %eax
	cmpl	$20, %r11d
	movl	-200(%rbp), %r9d
	movq	-192(%rbp), %rdi
	cmove	%eax, %r11d
	movslq	%r11d, %rax
	movq	%rax, -200(%rbp)
	movb	$0, -144(%rbp,%rax)
	testl	%r9d, %r9d
	je	.L200
	cmpl	$2, 24(%rbp)
	movl	-152(%rbp), %r9d
	movabsq	$-3689348814741910323, %rsi
	sete	%bl
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L204:
	movq	%rdi, %rax
	mulq	%rsi
	shrq	$3, %rdx
	movq	%rdx, %rax
	leaq	(%rdx,%rdx,4), %rdx
	addq	%rdx, %rdx
	subq	%rdx, %rdi
	movq	%rdi, %rdx
	movq	%rax, %rdi
	testl	%ecx, %ecx
	jne	.L201
	testb	%bl, %bl
	je	.L201
	testq	%rdx, %rdx
	jne	.L201
	subl	$1, %r9d
	jne	.L276
	movl	%r9d, -152(%rbp)
	movl	$0, -192(%rbp)
.L203:
	movslq	-152(%rbp), %rax
	cmpl	$1, -164(%rbp)
	movq	%rax, -208(%rbp)
	movb	$0, -112(%rbp,%rax)
	je	.L386
	movl	-192(%rbp), %edi
	movl	%r10d, %eax
	xorl	%edx, %edx
	subl	%r11d, %eax
	subl	%edi, %eax
	testl	%edi, %edi
	movl	-164(%rbp), %edi
	setg	%dl
	movl	%edi, -184(%rbp)
	subl	%edx, %eax
	subl	-168(%rbp), %eax
.L246:
	testl	%eax, %eax
	movl	$0, %ebx
	cmovns	%eax, %ebx
	testb	$1, 16(%rbp)
	je	.L211
	negl	%ebx
.L212:
	movl	-180(%rbp), %eax
	testl	%eax, %eax
	jne	.L387
.L228:
	movq	-200(%rbp), %r9
	movl	%ebx, -168(%rbp)
	movq	%r9, %rbx
	.p2align 4,,10
	.p2align 3
.L230:
	movq	-160(%rbp), %rax
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movsbl	(%rax,%rbx), %r8d
	call	doapr_outch
	testl	%eax, %eax
	je	.L187
	subq	$1, %rbx
	testl	%ebx, %ebx
	jne	.L230
	movl	16(%rbp), %eax
	movl	-168(%rbp), %ebx
	andl	$8, %eax
	orl	-192(%rbp), %eax
	je	.L231
	movl	$46, %r8d
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	doapr_outch
	testl	%eax, %eax
	je	.L187
	movq	-208(%rbp), %rax
	movl	%ebx, -168(%rbp)
	leaq	-112(%rbp,%rax), %r9
	leaq	-112(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	%r9, %rbx
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L233:
	movsbl	-1(%rbx), %r8d
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	subq	$1, %rbx
	call	doapr_outch
	testl	%eax, %eax
	je	.L187
.L232:
	cmpq	%rbx, -160(%rbp)
	jne	.L233
	movl	-168(%rbp), %ebx
.L231:
	movl	-192(%rbp), %eax
	subl	-152(%rbp), %eax
	testl	%eax, %eax
	jle	.L237
	movl	%ebx, -152(%rbp)
	movl	%eax, %ebx
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L389:
	subl	$1, %ebx
	je	.L388
.L234:
	movl	$48, %r8d
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	doapr_outch
	testl	%eax, %eax
	jne	.L389
	.p2align 4,,10
	.p2align 3
.L187:
	xorl	%r9d, %r9d
.L164:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L390
	addq	$168, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L382:
	.cfi_restore_state
	movl	$0, -168(%rbp)
	movl	16(%rbp), %eax
	andl	$4, %eax
	movl	%eax, -180(%rbp)
	je	.L167
	movl	$1, -168(%rbp)
	cmpl	$2, 24(%rbp)
	movl	$32, -180(%rbp)
	jne	.L168
.L383:
	ucomisd	%xmm3, %xmm0
	jp	.L249
	jne	.L249
	movl	$0, -164(%rbp)
	movapd	%xmm0, %xmm1
	movq	$0, -176(%rbp)
.L169:
	movl	-164(%rbp), %ecx
	testl	%r9d, %r9d
	movl	$1, %eax
	cmove	%eax, %r9d
	testl	%ecx, %ecx
	jne	.L183
	movl	-176(%rbp), %eax
	notl	%eax
	addl	%eax, %r9d
	jns	.L174
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L201:
	movzbl	(%r8,%rdx), %edx
	leal	1(%rcx), %eax
	movb	%dl, -112(%rbp,%rcx)
	cmpl	%eax, %r9d
	jle	.L391
.L202:
	movslq	%eax, %rcx
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L166:
	cmpl	$2, 24(%rbp)
	je	.L392
	movl	24(%rbp), %eax
	testl	%eax, %eax
	jne	.L280
	movl	$0, -164(%rbp)
	movl	$1, -168(%rbp)
	movl	$45, -180(%rbp)
	movq	$0, -176(%rbp)
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L276:
	xorl	%eax, %eax
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L188:
	movapd	%xmm0, %xmm5
	subsd	%xmm1, %xmm5
	cvttsd2siq	%xmm5, %rsi
	btcq	$63, %rsi
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L211:
	movl	16(%rbp), %r9d
	andl	$16, %r9d
	je	.L213
	testl	%eax, %eax
	jle	.L212
	movl	-180(%rbp), %edx
	testl	%edx, %edx
	je	.L216
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L394:
	subl	$1, %ebx
	je	.L215
.L216:
	movl	$48, %r8d
.L380:
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	doapr_outch
	testl	%eax, %eax
	jne	.L394
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L213:
	testl	%eax, %eax
	jle	.L212
	movq	%r14, %rax
	movl	%r9d, -168(%rbp)
	movq	%r13, %r14
	movq	%r15, %r13
	leaq	.LC0(%rip), %r8
	movq	%rax, %r15
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L217:
	movq	(%r12), %rdx
	movq	(%r15), %rax
	cmpq	%rax, %rdx
	ja	.L187
	testq	%r13, %r13
	je	.L219
	cmpq	%rax, %rdx
	je	.L395
.L220:
	cmpq	%rax, %rdx
	jnb	.L225
	movq	(%r14), %rcx
	testq	%rcx, %rcx
	je	.L224
.L247:
	leaq	1(%rdx), %rax
	movq	%rax, (%r12)
	movb	$32, (%rcx,%rdx)
.L225:
	subl	$1, %ebx
	je	.L396
.L227:
	movq	(%r14), %rcx
	testq	%rcx, %rcx
	jne	.L217
	testq	%r13, %r13
	je	.L187
	movq	(%r12), %rdx
	movq	(%r15), %rax
	cmpq	%rax, %rdx
	ja	.L187
	cmpq	%rax, %rdx
	jne	.L220
.L395:
	cmpq	$2147482623, %rdx
	ja	.L187
	movq	0(%r13), %rdi
	leaq	1024(%rdx), %r10
	movq	%r10, (%r15)
	testq	%rdi, %rdi
	je	.L397
	movl	$834, %ecx
	movq	%r8, %rdx
	movq	%r10, %rsi
	call	CRYPTO_realloc@PLT
	testq	%rax, %rax
	je	.L187
	movq	%rax, 0(%r13)
	movq	(%r12), %rdx
	leaq	.LC0(%rip), %r8
	movq	(%r15), %rax
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L219:
	cmpq	%rax, %rdx
	jb	.L247
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L397:
	movq	%r8, %rsi
	movl	$822, %edx
	movq	%r10, %rdi
	call	CRYPTO_malloc@PLT
	leaq	.LC0(%rip), %r8
	testq	%rax, %rax
	movq	%rax, 0(%r13)
	je	.L398
	movq	(%r12), %rdx
	testq	%rdx, %rdx
	je	.L223
	movq	(%r14), %rsi
	testq	%rsi, %rsi
	je	.L187
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	(%r12), %rdx
	leaq	.LC0(%rip), %r8
.L223:
	movq	$0, (%r14)
	cmpq	%rdx, (%r15)
	jbe	.L225
	.p2align 4,,10
	.p2align 3
.L224:
	movq	0(%r13), %rax
	leaq	1(%rdx), %rcx
	movq	%rcx, (%r12)
	movb	$32, (%rax,%rdx)
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L193:
	movq	%rsi, %rax
	movq	%rsi, %rcx
	pxor	%xmm1, %xmm1
	shrq	%rax
	andl	$1, %ecx
	orq	%rcx, %rax
	cvtsi2sdq	%rax, %xmm1
	addsd	%xmm1, %xmm1
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L386:
	movq	-176(%rbp), %rdi
	movl	$1, %edx
	leaq	-81(%rbp), %r9
	movl	%edi, %eax
	negl	%eax
	testq	%rdi, %rdi
	cmovns	%edi, %eax
	.p2align 4,,10
	.p2align 3
.L208:
	movslq	%eax, %rsi
	movl	%eax, %ecx
	movl	%eax, %ebx
	imulq	$1717986919, %rsi, %rsi
	sarl	$31, %ecx
	movl	%edx, %edi
	sarq	$34, %rsi
	subl	%ecx, %esi
	leal	(%rsi,%rsi,4), %ecx
	addl	%ecx, %ecx
	subl	%ecx, %ebx
	movslq	%ebx, %rcx
	movzbl	(%r8,%rcx), %ecx
	movb	%cl, (%r9,%rdx)
	movl	%eax, %ecx
	movl	%esi, %eax
	cmpl	$9, %ecx
	setg	%bl
	cmpl	$19, %edx
	setle	%sil
	addq	$1, %rdx
	testb	%sil, %bl
	jne	.L208
	movl	%edi, -184(%rbp)
	cmpl	$9, %ecx
	jg	.L187
	leal	2(%rdi), %edx
	cmpl	$1, %edi
	jne	.L210
	movb	$48, -79(%rbp)
	movl	$4, %edx
	movl	$2, -184(%rbp)
.L210:
	movl	-192(%rbp), %edi
	movl	%r10d, %eax
	xorl	%ecx, %ecx
	subl	%r11d, %eax
	subl	%edi, %eax
	testl	%edi, %edi
	setg	%cl
	subl	%ecx, %eax
	subl	-168(%rbp), %eax
	subl	%edx, %eax
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L387:
	movl	%eax, %r8d
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	doapr_outch
	testl	%eax, %eax
	jne	.L228
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L392:
	movl	$1, -168(%rbp)
	movl	$45, -180(%rbp)
.L249:
	movsd	.LC17(%rip), %xmm1
	comisd	%xmm0, %xmm1
	ja	.L256
	testl	%r9d, %r9d
	je	.L399
	movsd	.LC6(%rip), %xmm4
	movsd	.LC15(%rip), %xmm2
	movl	%r9d, %eax
	movapd	%xmm4, %xmm1
	.p2align 4,,10
	.p2align 3
.L172:
	mulsd	%xmm2, %xmm1
	subl	$1, %eax
	jne	.L172
	xorl	%eax, %eax
	comisd	%xmm1, %xmm0
	setnb	%al
	movl	%eax, -164(%rbp)
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L391:
	movl	%r9d, -152(%rbp)
	movl	%eax, %r9d
.L200:
	movl	-152(%rbp), %eax
	movl	%r9d, -152(%rbp)
	movl	%eax, -192(%rbp)
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L256:
	movsd	.LC6(%rip), %xmm4
	movsd	.LC15(%rip), %xmm2
	movl	$1, -164(%rbp)
.L248:
	xorl	%eax, %eax
	comisd	%xmm0, %xmm4
	movapd	%xmm0, %xmm1
	movq	%rax, -176(%rbp)
	jbe	.L175
	.p2align 4,,10
	.p2align 3
.L177:
	mulsd	%xmm2, %xmm1
	subq	$1, %rax
	comisd	%xmm1, %xmm4
	ja	.L177
	movq	%rax, -176(%rbp)
.L175:
	comisd	%xmm2, %xmm1
	jbe	.L178
	movq	-176(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L180:
	divsd	%xmm2, %xmm1
	addq	$1, %rax
	comisd	%xmm2, %xmm1
	ja	.L180
	movq	%rax, -176(%rbp)
.L178:
	cmpl	$2, 24(%rbp)
	je	.L169
	cmpl	$1, -164(%rbp)
	jne	.L174
	movapd	%xmm1, %xmm0
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L173:
	ucomisd	%xmm3, %xmm0
	jp	.L256
	jne	.L256
	movl	$1, -164(%rbp)
	movq	$0, -176(%rbp)
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L183:
	subl	$1, %r9d
	movapd	%xmm1, %xmm0
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L215:
	xorl	%ebx, %ebx
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L260:
	movsd	.LC6(%rip), %xmm2
	movl	$1, %edx
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L280:
	movsd	.LC6(%rip), %xmm4
	movsd	.LC15(%rip), %xmm2
	movl	$1, -168(%rbp)
	movl	$1, -164(%rbp)
	movl	$45, -180(%rbp)
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L261:
	movsd	.LC15(%rip), %xmm1
	jmp	.L191
.L262:
	movsd	.LC10(%rip), %xmm1
	jmp	.L191
.L269:
	movsd	.LC10(%rip), %xmm2
	jmp	.L190
.L393:
	movl	-180(%rbp), %r8d
	jmp	.L380
.L270:
	movsd	.LC11(%rip), %xmm2
	jmp	.L190
.L263:
	movsd	.LC11(%rip), %xmm1
	jmp	.L191
.L399:
	xorl	%eax, %eax
	comisd	.LC15(%rip), %xmm0
	movsd	.LC6(%rip), %xmm4
	movsd	.LC15(%rip), %xmm2
	setnb	%al
	movl	%eax, -164(%rbp)
	jmp	.L248
.L396:
	movq	%r15, %rax
	movq	%r13, %r15
	movq	%r14, %r13
	movq	%rax, %r14
	jmp	.L212
.L264:
	movsd	.LC12(%rip), %xmm1
	jmp	.L191
.L271:
	movsd	.LC12(%rip), %xmm2
	jmp	.L190
.L272:
	movsd	.LC13(%rip), %xmm2
	jmp	.L190
.L265:
	movsd	.LC13(%rip), %xmm1
	jmp	.L191
.L266:
	movsd	.LC14(%rip), %xmm1
	jmp	.L191
.L273:
	movsd	.LC14(%rip), %xmm2
	jmp	.L190
.L274:
	movsd	.LC8(%rip), %xmm2
	jmp	.L190
.L267:
	movsd	.LC8(%rip), %xmm1
	jmp	.L191
.L388:
	movl	-152(%rbp), %ebx
.L237:
	cmpl	$1, -164(%rbp)
	je	.L400
.L236:
	testl	%ebx, %ebx
	jne	.L243
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L401:
	addl	$1, %ebx
	je	.L244
.L243:
	movl	$32, %r8d
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	doapr_outch
	testl	%eax, %eax
	jne	.L401
	jmp	.L187
.L384:
	movsd	.LC9(%rip), %xmm1
	jmp	.L191
.L385:
	movsd	.LC9(%rip), %xmm2
	jmp	.L190
.L398:
	movl	$823, %r8d
	movl	$65, %edx
	movl	$150, %esi
	movl	-168(%rbp), %r9d
	leaq	.LC0(%rip), %rcx
	movl	$32, %edi
	movl	%r9d, -152(%rbp)
	call	ERR_put_error@PLT
	movl	-152(%rbp), %r9d
	jmp	.L164
.L244:
	movl	$1, %r9d
	jmp	.L164
.L400:
	movl	16(%rbp), %eax
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	andl	$32, %eax
	cmpl	$1, %eax
	sbbl	%r8d, %r8d
	andl	$32, %r8d
	addl	$69, %r8d
	call	doapr_outch
	testl	%eax, %eax
	je	.L187
	cmpq	$0, -176(%rbp)
	movl	$45, %r8d
	js	.L377
	movl	$43, %r8d
.L377:
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	doapr_outch
	testl	%eax, %eax
	je	.L187
	movslq	-184(%rbp), %rax
	movl	%ebx, -160(%rbp)
	leaq	-80(%rbp,%rax), %r9
	leaq	-80(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	%r14, %rax
	movq	%r9, %rbx
	movq	%r12, %r14
	movq	%rax, %r12
	jmp	.L241
.L242:
	movsbl	-1(%rbx), %r8d
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	subq	$1, %rbx
	call	doapr_outch
	testl	%eax, %eax
	je	.L187
.L241:
	cmpq	%rbx, -152(%rbp)
	jne	.L242
	movq	%r12, %rax
	movl	-160(%rbp), %ebx
	movq	%r14, %r12
	movq	%rax, %r14
	jmp	.L236
.L390:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE257:
	.size	fmtfp, .-fmtfp
	.section	.rodata.str1.1
.LC23:
	.string	"<NULL>"
	.text
	.p2align 4
	.type	_dopr, @function
_dopr:
.LFB251:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	1(%r9), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	16(%rbp), %r13
	movzbl	(%r9), %r15d
	movq	%rcx, -72(%rbp)
	movq	%r8, -96(%rbp)
	movl	$-1, %r9d
	movq	%rdx, %rcx
	movq	%r13, %r11
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	movl	$0, -80(%rbp)
	movl	$0, -88(%rbp)
.L700:
	testb	%r15b, %r15b
	jne	.L722
.L796:
	movq	-64(%rbp), %rsi
	movq	%r14, %r10
	movq	%rcx, %r11
.L404:
	movq	(%r11), %rcx
	movq	(%rbx), %rax
	movq	%rsi, %rdx
	testq	%r10, %r10
	je	.L406
	cmpq	%rsi, %rcx
	jb	.L798
	cmpq	%rsi, %rcx
	je	.L819
.L567:
	cmpq	%rdx, %rcx
	jbe	.L572
	movq	(%rbx), %rax
	leaq	1(%rdx), %rcx
	testq	%rax, %rax
	je	.L573
.L797:
	movq	%rcx, -64(%rbp)
	movb	$0, (%rax,%rdx)
	movq	-64(%rbp), %rdx
.L572:
	movq	-72(%rbp), %rax
	subq	$1, %rdx
	movq	%rdx, (%rax)
	movl	$1, %eax
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L722:
	testq	%r14, %r14
	je	.L820
.L407:
	testq	%r14, %r14
	je	.L408
	cmpl	$6, %eax
	ja	.L409
	leaq	.L411(%rip), %rdx
	movl	%eax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L411:
	.long	.L409-.L411
	.long	.L425-.L411
	.long	.L564-.L411
	.long	.L414-.L411
	.long	.L413-.L411
	.long	.L412-.L411
	.long	.L588-.L411
	.text
.L449:
	movzbl	(%r12), %r15d
	leaq	1(%r12), %rax
	testb	%r15b, %r15b
	je	.L796
	movq	%rax, %r12
	testq	%r14, %r14
	jne	.L413
	movq	(%rcx), %rsi
	movq	-64(%rbp), %rdx
	cmpq	%rsi, %rdx
	jnb	.L777
.L413:
	xorl	%r13d, %r13d
.L563:
	movsbl	%r15b, %edx
	movl	$4, %esi
	movq	%r11, -120(%rbp)
	movl	%edx, %edi
	movq	%rcx, -112(%rbp)
	movl	%r9d, -104(%rbp)
	movl	%edx, -128(%rbp)
	call	ossl_ctype_check@PLT
	movl	-104(%rbp), %r9d
	movq	-112(%rbp), %rcx
	testl	%eax, %eax
	movq	-120(%rbp), %r11
	je	.L450
	testl	%r9d, %r9d
	movl	-128(%rbp), %edx
	movzbl	(%r12), %r15d
	cmovs	%r13d, %r9d
	leal	(%r9,%r9,4), %eax
	leal	-48(%rdx,%rax,2), %r9d
	leaq	1(%r12), %rax
	testb	%r15b, %r15b
	je	.L796
	testq	%r14, %r14
	je	.L821
	movq	%rax, %r12
	movl	$4, %eax
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L450:
	cmpb	$42, %r15b
	jne	.L452
	movl	(%r11), %edx
	cmpl	$47, %edx
	ja	.L453
	movl	%edx, %eax
	addl	$8, %edx
	addq	16(%r11), %rax
	movl	%edx, (%r11)
.L454:
	movzbl	(%r12), %r15d
	movl	(%rax), %r9d
	leaq	1(%r12), %rax
	testb	%r15b, %r15b
	je	.L796
	movq	%rax, %r12
	testq	%r14, %r14
	je	.L801
.L412:
	leal	-76(%r15), %edx
	movl	$6, %eax
	cmpb	$46, %dl
	ja	.L700
	leaq	.L418(%rip), %rsi
	movzbl	%dl, %edx
	movslq	(%rsi,%rdx,4), %rdx
	addq	%rsi, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L418:
	.long	.L422-.L418
	.long	.L700-.L418
	.long	.L700-.L418
	.long	.L700-.L418
	.long	.L700-.L418
	.long	.L700-.L418
	.long	.L700-.L418
	.long	.L700-.L418
	.long	.L700-.L418
	.long	.L700-.L418
	.long	.L700-.L418
	.long	.L700-.L418
	.long	.L700-.L418
	.long	.L700-.L418
	.long	.L700-.L418
	.long	.L700-.L418
	.long	.L700-.L418
	.long	.L700-.L418
	.long	.L700-.L418
	.long	.L700-.L418
	.long	.L700-.L418
	.long	.L700-.L418
	.long	.L700-.L418
	.long	.L700-.L418
	.long	.L700-.L418
	.long	.L700-.L418
	.long	.L700-.L418
	.long	.L700-.L418
	.long	.L421-.L418
	.long	.L700-.L418
	.long	.L419-.L418
	.long	.L700-.L418
	.long	.L420-.L418
	.long	.L700-.L418
	.long	.L700-.L418
	.long	.L700-.L418
	.long	.L700-.L418
	.long	.L419-.L418
	.long	.L700-.L418
	.long	.L700-.L418
	.long	.L700-.L418
	.long	.L700-.L418
	.long	.L700-.L418
	.long	.L700-.L418
	.long	.L700-.L418
	.long	.L700-.L418
	.long	.L417-.L418
	.text
.L455:
	movq	(%rcx), %rsi
	movq	-64(%rbp), %rdx
	cmpq	%rdx, %rsi
	jbe	.L777
.L588:
	xorl	%eax, %eax
.L410:
	leal	-37(%r15), %edx
	cmpb	$83, %dl
	ja	.L459
	leaq	.L480(%rip), %rdi
	movzbl	%dl, %edx
	movslq	(%rdi,%rdx,4), %rdx
	addq	%rdi, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L480:
	.long	.L474-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L486-.L480
	.long	.L459-.L480
	.long	.L485-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L471-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L470-.L480
	.long	.L481-.L480
	.long	.L484-.L480
	.long	.L483-.L480
	.long	.L482-.L480
	.long	.L459-.L480
	.long	.L481-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L677-.L480
	.long	.L479-.L480
	.long	.L464-.L480
	.long	.L459-.L480
	.long	.L459-.L480
	.long	.L463-.L480
	.long	.L459-.L480
	.long	.L479-.L480
	.long	.L459-.L480
	.long	.L462-.L480
	.long	.L479-.L480
	.text
.L677:
	movq	-64(%rbp), %rsi
.L465:
	movl	(%r11), %edx
	cmpl	$47, %edx
	ja	.L561
	movl	%edx, %eax
	addl	$8, %edx
	addq	16(%r11), %rax
	movl	%edx, (%r11)
.L562:
	movq	(%rax), %rax
	movl	%esi, (%rax)
.L459:
	movzbl	(%r12), %r15d
	leaq	1(%r12), %rax
	testb	%r15b, %r15b
	je	.L796
	movl	$0, -88(%rbp)
	movl	$-1, %r9d
	movq	%rax, %r12
	movl	$0, -80(%rbp)
	testq	%r14, %r14
	jne	.L409
	movq	(%rcx), %rsi
	movq	-64(%rbp), %rdx
	cmpq	%rdx, %rsi
	jbe	.L777
.L409:
	movsbl	%r15b, %r8d
	leaq	-64(%rbp), %r13
	movq	%rcx, %r15
.L431:
	cmpb	$37, %r8b
	jne	.L822
	movq	%r15, %rcx
	movzbl	(%r12), %r15d
	leaq	1(%r12), %rax
	testb	%r15b, %r15b
	je	.L796
	movq	%rax, %r12
	testq	%r14, %r14
	jne	.L425
	movq	(%rcx), %rsi
	movq	-64(%rbp), %rdx
	cmpq	%rdx, %rsi
	ja	.L425
	.p2align 4,,10
	.p2align 3
.L777:
	movq	(%rbx), %rax
	movq	%rsi, %rcx
	movq	%r14, %r10
	.p2align 4,,10
	.p2align 3
.L406:
	leaq	-1(%rcx), %rsi
	xorl	%edi, %edi
	movq	-96(%rbp), %r8
	cmpq	%rdx, %rsi
	setb	%dil
	movl	%edi, (%r8)
	jb	.L823
.L566:
	testq	%rax, %rax
	je	.L798
	cmpq	%rdx, %rcx
	jnb	.L567
	.p2align 4,,10
	.p2align 3
.L798:
	xorl	%eax, %eax
.L402:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L824
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L408:
	.cfi_restore_state
	cmpl	$6, %eax
	ja	.L409
	leaq	.L423(%rip), %rdx
	movl	%eax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L423:
	.long	.L409-.L423
	.long	.L425-.L423
	.long	.L564-.L423
	.long	.L414-.L423
	.long	.L413-.L423
	.long	.L424-.L423
	.long	.L588-.L423
	.text
.L444:
	cmpb	$42, %r15b
	jne	.L446
	movl	(%r11), %edx
	cmpl	$47, %edx
	ja	.L447
	movl	%edx, %eax
	addl	$8, %edx
	addq	16(%r11), %rax
	movl	%edx, (%r11)
.L448:
	movl	(%rax), %eax
	movzbl	(%r12), %r15d
	movl	%eax, -88(%rbp)
	leaq	1(%r12), %rax
	testb	%r15b, %r15b
	je	.L796
	movq	%rax, %r12
	testq	%r14, %r14
	je	.L800
.L414:
	cmpb	$46, %r15b
	je	.L449
.L452:
	testq	%r14, %r14
	jne	.L412
.L801:
	movq	(%rcx), %rsi
	movq	-64(%rbp), %rdx
	cmpq	%rdx, %rsi
	jbe	.L777
.L424:
	leal	-76(%r15), %eax
	cmpb	$46, %al
	ja	.L455
	leaq	.L456(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L456:
	.long	.L422-.L456
	.long	.L455-.L456
	.long	.L455-.L456
	.long	.L455-.L456
	.long	.L455-.L456
	.long	.L455-.L456
	.long	.L455-.L456
	.long	.L455-.L456
	.long	.L455-.L456
	.long	.L455-.L456
	.long	.L455-.L456
	.long	.L455-.L456
	.long	.L455-.L456
	.long	.L455-.L456
	.long	.L455-.L456
	.long	.L455-.L456
	.long	.L455-.L456
	.long	.L455-.L456
	.long	.L455-.L456
	.long	.L455-.L456
	.long	.L455-.L456
	.long	.L455-.L456
	.long	.L455-.L456
	.long	.L455-.L456
	.long	.L455-.L456
	.long	.L455-.L456
	.long	.L455-.L456
	.long	.L455-.L456
	.long	.L421-.L456
	.long	.L455-.L456
	.long	.L457-.L456
	.long	.L455-.L456
	.long	.L420-.L456
	.long	.L455-.L456
	.long	.L455-.L456
	.long	.L455-.L456
	.long	.L455-.L456
	.long	.L457-.L456
	.long	.L455-.L456
	.long	.L455-.L456
	.long	.L455-.L456
	.long	.L455-.L456
	.long	.L455-.L456
	.long	.L455-.L456
	.long	.L455-.L456
	.long	.L455-.L456
	.long	.L417-.L456
	.text
	.p2align 4,,10
	.p2align 3
.L820:
	movq	(%rcx), %rsi
	movq	-64(%rbp), %rdx
	cmpq	%rdx, %rsi
	ja	.L407
	jmp	.L777
.L436:
	movzbl	(%r12), %r15d
	orl	$2, -80(%rbp)
	leaq	1(%r12), %rax
	testb	%r15b, %r15b
	je	.L796
	testq	%r14, %r14
	jne	.L611
	movq	(%rcx), %rsi
	movq	-64(%rbp), %rdx
	cmpq	%rsi, %rdx
	jnb	.L777
.L440:
	movq	%rax, %r12
.L425:
	leal	-32(%r15), %eax
	cmpb	$16, %al
	ja	.L432
	leaq	.L434(%rip), %rdi
	movzbl	%al, %eax
	movslq	(%rdi,%rax,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L434:
	.long	.L438-.L434
	.long	.L432-.L434
	.long	.L432-.L434
	.long	.L437-.L434
	.long	.L432-.L434
	.long	.L432-.L434
	.long	.L432-.L434
	.long	.L432-.L434
	.long	.L432-.L434
	.long	.L432-.L434
	.long	.L432-.L434
	.long	.L436-.L434
	.long	.L432-.L434
	.long	.L435-.L434
	.long	.L432-.L434
	.long	.L432-.L434
	.long	.L433-.L434
	.text
.L825:
	movq	(%rcx), %rsi
	movq	-64(%rbp), %rdx
	cmpq	%rdx, %rsi
	jbe	.L777
	movq	%rax, %r12
.L564:
	movsbl	%r15b, %r13d
	movl	$4, %esi
	movq	%r11, -120(%rbp)
	movl	%r13d, %edi
	movq	%rcx, -112(%rbp)
	movl	%r9d, -104(%rbp)
	call	ossl_ctype_check@PLT
	movl	-104(%rbp), %r9d
	movq	-112(%rbp), %rcx
	testl	%eax, %eax
	movq	-120(%rbp), %r11
	je	.L444
	movl	-88(%rbp), %eax
	movzbl	(%r12), %r15d
	leal	(%rax,%rax,4), %eax
	leal	-48(%r13,%rax,2), %eax
	movl	%eax, -88(%rbp)
	leaq	1(%r12), %rax
	testb	%r15b, %r15b
	je	.L796
	testq	%r14, %r14
	je	.L825
	movq	%rax, %r12
	movl	$2, %eax
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L823:
	movq	%rsi, -64(%rbp)
	movq	%rsi, %rdx
	jmp	.L566
.L827:
	movq	%r8, %rdi
	movl	$822, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	-80(%rbp), %r10
	movq	-88(%rbp), %r11
	testq	%rax, %rax
	movq	%rax, %rdi
	movq	%rax, (%r10)
	je	.L826
	movq	-64(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L570
	movq	(%rbx), %rsi
	movq	%r11, -88(%rbp)
	movq	%r10, -80(%rbp)
	testq	%rsi, %rsi
	je	.L798
	call	memcpy@PLT
	movq	-64(%rbp), %rdx
	movq	-88(%rbp), %r11
	movq	-80(%rbp), %r10
.L570:
	movq	$0, (%rbx)
	cmpq	%rdx, (%r11)
	jbe	.L572
	leaq	1(%rdx), %rcx
.L573:
	movq	(%r10), %rax
	jmp	.L797
.L432:
	testq	%r14, %r14
	jne	.L564
	movq	(%rcx), %rsi
	movq	-64(%rbp), %rdx
	cmpq	%rdx, %rsi
	jbe	.L777
	jmp	.L564
	.p2align 4,,10
	.p2align 3
.L819:
	cmpq	$2147482623, %rcx
	ja	.L798
	movq	(%r10), %rdi
	leaq	1024(%rcx), %r8
	movq	%r11, -88(%rbp)
	movq	%r8, (%r11)
	movq	%r10, -80(%rbp)
	testq	%rdi, %rdi
	je	.L827
	movl	$834, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r8, %rsi
	call	CRYPTO_realloc@PLT
	testq	%rax, %rax
	je	.L798
	movq	-80(%rbp), %r10
	movq	-88(%rbp), %r11
	movq	-64(%rbp), %rdx
	movq	%rax, (%r10)
	movq	(%r11), %rcx
	jmp	.L567
.L420:
	movzbl	(%r12), %r15d
	leaq	1(%r12), %rdx
	movl	$2, %eax
	cmpb	$108, %r15b
	jne	.L458
	movzbl	1(%r12), %r15d
	leaq	2(%r12), %rdx
	movl	$4, %eax
.L458:
	testb	%r15b, %r15b
	je	.L796
	movq	%rdx, %r12
	testq	%r14, %r14
	jne	.L410
	movq	(%rcx), %rsi
	movq	-64(%rbp), %rdx
	cmpq	%rsi, %rdx
	jnb	.L777
	jmp	.L410
.L417:
	movzbl	(%r12), %r15d
	leaq	1(%r12), %rax
	testb	%r15b, %r15b
	je	.L796
	testq	%r14, %r14
	je	.L828
.L600:
	movq	%rax, %r12
	movl	$5, %eax
	jmp	.L410
.L421:
	movzbl	(%r12), %r15d
	leaq	1(%r12), %rax
	testb	%r15b, %r15b
	je	.L796
	testq	%r14, %r14
	je	.L829
.L596:
	movq	%rax, %r12
	movl	$1, %eax
	jmp	.L410
.L422:
	movzbl	(%r12), %r15d
	leaq	1(%r12), %rax
	testb	%r15b, %r15b
	je	.L796
	testq	%r14, %r14
	je	.L830
.L613:
	movq	%rax, %r12
	movl	$3, %eax
	jmp	.L410
.L419:
	movzbl	(%r12), %r15d
	testb	%r15b, %r15b
	je	.L796
	addq	$1, %r12
	movl	$4, %eax
	jmp	.L410
.L457:
	movzbl	(%r12), %r15d
	testb	%r15b, %r15b
	je	.L796
	movq	-64(%rbp), %rsi
	addq	$1, %r12
	cmpq	(%rcx), %rsi
	jnb	.L764
	leal	-37(%r15), %eax
	cmpb	$83, %al
	ja	.L459
	leaq	.L461(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L461:
	.long	.L474-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L486-.L461
	.long	.L459-.L461
	.long	.L485-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L599-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L470-.L461
	.long	.L466-.L461
	.long	.L484-.L461
	.long	.L483-.L461
	.long	.L482-.L461
	.long	.L459-.L461
	.long	.L466-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L465-.L461
	.long	.L460-.L461
	.long	.L464-.L461
	.long	.L459-.L461
	.long	.L459-.L461
	.long	.L463-.L461
	.long	.L459-.L461
	.long	.L460-.L461
	.long	.L459-.L461
	.long	.L462-.L461
	.long	.L460-.L461
	.text
.L599:
	movl	$4, %eax
.L471:
	orl	$32, -80(%rbp)
	movl	$88, %r15d
.L479:
	movl	-80(%rbp), %esi
	movl	(%r11), %edx
	orl	$64, %esi
	cmpl	$4, %eax
	je	.L505
	cmpl	$5, %eax
	je	.L505
	cmpl	$1, %eax
	je	.L506
	cmpl	$2, %eax
	jne	.L831
.L505:
	cmpl	$47, %edx
	ja	.L516
	movl	%edx, %eax
	addl	$8, %edx
	addq	16(%r11), %rax
	movl	%edx, (%r11)
.L517:
	movq	(%rax), %r8
.L511:
	movl	$8, %eax
	cmpb	$111, %r15b
	je	.L520
	cmpb	$117, %r15b
	movl	$16, %eax
	movl	$10, %edx
	cmove	%edx, %eax
.L520:
	movl	-88(%rbp), %edi
	subq	$8, %rsp
	movq	%r11, -104(%rbp)
	leaq	-64(%rbp), %rdx
	pushq	%rsi
	pushq	%r9
	movl	%eax, %r9d
	pushq	%rdi
.L783:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%rcx, -80(%rbp)
	call	fmtint
	addq	$32, %rsp
	movq	-80(%rbp), %rcx
	movq	-104(%rbp), %r11
	testl	%eax, %eax
	jne	.L459
	jmp	.L798
.L485:
	orl	$32, -80(%rbp)
.L482:
	movl	4(%r11), %edx
	cmpl	$175, %edx
	ja	.L535
	movl	%edx, %eax
	addl	$16, %edx
	addq	16(%r11), %rax
	movl	%edx, 4(%r11)
.L536:
	movsd	(%rax), %xmm0
	movq	%r11, -104(%rbp)
	leaq	-64(%rbp), %rdx
	pushq	$2
.L787:
	movl	-80(%rbp), %eax
	movl	-88(%rbp), %r8d
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%rcx, -80(%rbp)
	pushq	%rax
	call	fmtfp
	popq	%rdx
	movq	-104(%rbp), %r11
	testl	%eax, %eax
	popq	%rcx
	movq	-80(%rbp), %rcx
	jne	.L459
	jmp	.L798
.L486:
	orl	$32, -80(%rbp)
.L484:
	movl	4(%r11), %edx
	cmpl	$175, %edx
	ja	.L530
	movl	%edx, %eax
	addl	$16, %edx
	addq	16(%r11), %rax
	movl	%edx, 4(%r11)
.L531:
	movsd	(%rax), %xmm0
	leaq	-64(%rbp), %rdx
	movq	%r11, -104(%rbp)
	pushq	$1
	jmp	.L787
.L483:
	movl	4(%r11), %edx
	cmpl	$175, %edx
	ja	.L525
	movl	%edx, %eax
	addl	$16, %edx
	addq	16(%r11), %rax
	movl	%edx, 4(%r11)
.L526:
	movsd	(%rax), %xmm0
	leaq	-64(%rbp), %rdx
	movq	%r11, -104(%rbp)
	pushq	$0
	jmp	.L787
.L460:
	movl	-80(%rbp), %esi
	movl	(%r11), %edx
	orl	$64, %esi
	jmp	.L505
.L474:
	movq	%r11, -88(%rbp)
	leaq	-64(%rbp), %rdx
	movl	$37, %r8d
.L789:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%rcx, -80(%rbp)
	call	doapr_outch
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %r11
	testl	%eax, %eax
	jne	.L459
	jmp	.L798
.L470:
	movl	(%r11), %edx
	cmpl	$47, %edx
	ja	.L537
	movl	%edx, %eax
	addl	$8, %edx
	addq	16(%r11), %rax
	movl	%edx, (%r11)
.L538:
	movq	%r11, -88(%rbp)
	movl	(%rax), %r8d
	leaq	-64(%rbp), %rdx
	jmp	.L789
.L462:
	addq	$1, %r12
	jmp	.L459
.L463:
	movl	(%r11), %edx
	cmpl	$47, %edx
	ja	.L539
	movl	%edx, %eax
	addl	$8, %edx
	addq	16(%r11), %rax
	movl	%edx, (%r11)
.L540:
	movq	(%rax), %r10
	movl	-88(%rbp), %eax
	movl	-80(%rbp), %r13d
	shrl	$31, %eax
	movl	%eax, -104(%rbp)
	andl	$1, %r13d
	testl	%r9d, %r9d
	js	.L832
	testq	%r10, %r10
	leaq	.LC23(%rip), %rax
	cmove	%rax, %r10
.L583:
	movslq	%r9d, %rsi
	movq	%r10, %rdi
	movq	%r11, -136(%rbp)
	movq	%rcx, -128(%rbp)
	movl	%r9d, -120(%rbp)
	movq	%r10, -112(%rbp)
	call	OPENSSL_strnlen@PLT
	movl	-88(%rbp), %r15d
	movq	-112(%rbp), %r10
	movl	-120(%rbp), %r9d
	movq	-128(%rbp), %rcx
	movq	%rax, -80(%rbp)
	subl	%eax, %r15d
	movq	-136(%rbp), %r11
	js	.L618
	cmpb	$0, -104(%rbp)
	jne	.L618
.L545:
	movl	$2147483647, %eax
	subl	%r15d, %eax
.L580:
	cmpl	%r9d, %eax
	jle	.L833
	addl	%r15d, %r9d
.L578:
	testl	%r13d, %r13d
	jne	.L551
	testl	%r15d, %r15d
	je	.L604
	movl	%r9d, %r13d
	testl	%r9d, %r9d
	je	.L550
.L549:
	movl	%r15d, %eax
	leaq	-64(%rbp), %rdx
	movq	%r12, -112(%rbp)
	movq	%rcx, %r13
	subl	%r9d, %eax
	movq	%r14, %r12
	movl	%r15d, -88(%rbp)
	movq	%rdx, %r14
	movl	%eax, -104(%rbp)
	movl	%r9d, -128(%rbp)
	movq	%r10, -136(%rbp)
	movq	%r11, -120(%rbp)
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L836:
	movl	-88(%rbp), %eax
	subl	$1, %r15d
	subl	%r15d, %eax
	testl	%r15d, %r15d
	je	.L834
	cmpl	-104(%rbp), %r15d
	je	.L835
.L556:
	movl	$32, %r8d
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	doapr_outch
	testl	%eax, %eax
	jne	.L836
	jmp	.L798
.L464:
	movl	(%r11), %edx
	cmpl	$47, %edx
	ja	.L559
	movl	%edx, %eax
	addl	$8, %edx
	addq	16(%r11), %rax
	movl	%edx, (%r11)
.L560:
	movl	-80(%rbp), %esi
	movl	-88(%rbp), %edi
	subq	$8, %rsp
	movq	%r11, -104(%rbp)
	leaq	-64(%rbp), %rdx
	orl	$8, %esi
	pushq	%rsi
	pushq	%r9
	movl	$16, %r9d
	pushq	%rdi
	movq	(%rax), %r8
	jmp	.L783
.L466:
	movl	(%r11), %edx
.L492:
	cmpl	$47, %edx
	ja	.L500
	movl	%edx, %eax
	addl	$8, %edx
	addq	16(%r11), %rax
	movl	%edx, (%r11)
.L501:
	movq	(%rax), %r8
.L495:
	movl	-80(%rbp), %eax
	subq	$8, %rsp
	movq	%r11, -104(%rbp)
	leaq	-64(%rbp), %rdx
	pushq	%rax
	movl	-88(%rbp), %eax
	pushq	%r9
	movl	$10, %r9d
	pushq	%rax
	jmp	.L783
.L822:
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r11, -112(%rbp)
	movl	%r9d, -104(%rbp)
	call	doapr_outch
	movl	-104(%rbp), %r9d
	movq	-112(%rbp), %r11
	testl	%eax, %eax
	je	.L798
	movsbl	(%r12), %r8d
	leaq	1(%r12), %rax
	testb	%r8b, %r8b
	jne	.L724
	movq	-64(%rbp), %rsi
	movq	%r14, %r10
	movq	%r15, %r11
	jmp	.L404
.L446:
	testq	%r14, %r14
	jne	.L414
.L800:
	movq	(%rcx), %rsi
	movq	-64(%rbp), %rdx
	cmpq	%rdx, %rsi
	jbe	.L777
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L433:
	movzbl	(%r12), %r15d
	orl	$16, -80(%rbp)
	leaq	1(%r12), %rax
	testb	%r15b, %r15b
	je	.L796
.L728:
	testq	%r14, %r14
	je	.L837
.L611:
	movq	%rax, %r12
	movl	$1, %eax
	jmp	.L407
.L438:
	movzbl	(%r12), %r15d
	orl	$4, -80(%rbp)
	leaq	1(%r12), %rax
	testb	%r15b, %r15b
	je	.L796
	jmp	.L728
	.p2align 4,,10
	.p2align 3
.L437:
	movzbl	(%r12), %r15d
	orl	$8, -80(%rbp)
	leaq	1(%r12), %rax
	testb	%r15b, %r15b
	je	.L796
	jmp	.L728
	.p2align 4,,10
	.p2align 3
.L435:
	movzbl	(%r12), %r15d
	orl	$1, -80(%rbp)
	leaq	1(%r12), %rax
	testb	%r15b, %r15b
	je	.L796
	jmp	.L728
.L481:
	movl	(%r11), %edx
	cmpl	$2, %eax
	je	.L492
	jle	.L838
	cmpl	$4, %eax
	je	.L492
	cmpl	$5, %eax
	je	.L492
.L490:
	cmpl	$47, %edx
	ja	.L502
	movl	%edx, %eax
	addl	$8, %edx
	addq	16(%r11), %rax
	movl	%edx, (%r11)
.L503:
	movslq	(%rax), %r8
	jmp	.L495
.L453:
	movq	8(%r11), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 8(%r11)
	jmp	.L454
.L447:
	movq	8(%r11), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 8(%r11)
	jmp	.L448
.L535:
	movq	8(%r11), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 8(%r11)
	jmp	.L536
.L530:
	movq	8(%r11), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 8(%r11)
	jmp	.L531
.L838:
	cmpl	$1, %eax
	jne	.L490
	cmpl	$47, %edx
	ja	.L493
	movl	%edx, %eax
	addl	$8, %edx
	addq	16(%r11), %rax
	movl	%edx, (%r11)
.L494:
	movswq	(%rax), %r8
	jmp	.L495
.L826:
	movl	$823, %r8d
	movl	$65, %edx
	movl	$150, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L798
.L561:
	movq	8(%r11), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 8(%r11)
	jmp	.L562
.L516:
	movq	8(%r11), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 8(%r11)
	jmp	.L517
.L537:
	movq	8(%r11), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 8(%r11)
	jmp	.L538
.L525:
	movq	8(%r11), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 8(%r11)
	jmp	.L526
.L559:
	movq	8(%r11), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 8(%r11)
	jmp	.L560
.L832:
	testq	%r14, %r14
	je	.L839
	testq	%r10, %r10
	leaq	.LC23(%rip), %rax
	movl	$2147483647, %esi
	movq	%r11, -128(%rbp)
	cmove	%rax, %r10
	movq	%rcx, -120(%rbp)
	movq	%r10, %rdi
	movq	%r10, -112(%rbp)
	call	OPENSSL_strnlen@PLT
	movl	-88(%rbp), %r15d
	movq	-112(%rbp), %r10
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %r11
	movq	%rax, -80(%rbp)
	subl	%eax, %r15d
	js	.L585
	cmpb	$0, -104(%rbp)
	movl	$2147483647, %r9d
	je	.L545
.L585:
	testl	%r13d, %r13d
	jne	.L616
	xorl	%r15d, %r15d
	movl	$2147483647, %r9d
.L550:
	cmpq	$0, -80(%rbp)
	je	.L459
	movl	%r13d, %eax
.L552:
	movl	%r9d, %edi
	shrl	$31, %edi
	movl	%edi, -88(%rbp)
	cmpl	%eax, %r9d
	jbe	.L553
.L555:
	movq	-80(%rbp), %rax
	movl	%r15d, -136(%rbp)
	leaq	-64(%rbp), %rdx
	movq	%rcx, %r15
	movq	%r12, -120(%rbp)
	movl	%r9d, %r12d
	leaq	-1(%r10,%rax), %rax
	movq	%r11, -128(%rbp)
	movq	%rbx, %r11
	movq	%r10, %rbx
	movq	%rax, -112(%rbp)
.L701:
	movsbl	(%rbx), %r8d
	movq	%r15, %rcx
	movq	%r14, %rsi
	movq	%r11, %rdi
	movq	%rdx, -104(%rbp)
	movq	%r11, -80(%rbp)
	call	doapr_outch
	testl	%eax, %eax
	je	.L798
	addl	$1, %r13d
	cmpq	-112(%rbp), %rbx
	movq	-80(%rbp), %r11
	je	.L766
	addq	$1, %rbx
	cmpl	%r13d, %r12d
	movq	-104(%rbp), %rdx
	jg	.L701
	cmpb	$0, -88(%rbp)
	jne	.L701
	movq	%r11, %rbx
	movq	-120(%rbp), %r12
	movq	-128(%rbp), %r11
	movq	%r15, %rcx
	jmp	.L459
.L539:
	movq	8(%r11), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 8(%r11)
	jmp	.L540
.L835:
	movq	%r12, %r14
	movq	-120(%rbp), %r11
	movq	-112(%rbp), %r12
	movq	%r13, %rcx
	jmp	.L459
.L500:
	movq	8(%r11), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 8(%r11)
	jmp	.L501
.L766:
	movl	%r12d, %r9d
	movq	%r11, %rbx
	movq	%r15, %rcx
	movq	-120(%rbp), %r12
	movl	-136(%rbp), %r15d
	movq	-128(%rbp), %r11
.L553:
	testl	%r15d, %r15d
	jns	.L459
	cmpl	%r13d, %r9d
	jbe	.L459
	movl	%r13d, %eax
	movq	%r12, -80(%rbp)
	leaq	-64(%rbp), %rdx
	movq	%rbx, %r12
	subl	%r15d, %eax
	movl	%r9d, %ebx
	movl	%eax, %r15d
	jmp	.L558
.L840:
	addl	$1, %r13d
	movq	-104(%rbp), %rcx
	movq	-112(%rbp), %r11
	cmpl	%r15d, %r13d
	je	.L767
	cmpl	%ebx, %r13d
	movq	-88(%rbp), %rdx
	jnb	.L767
.L558:
	movl	$32, %r8d
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r11, -112(%rbp)
	movq	%rcx, -104(%rbp)
	movq	%rdx, -88(%rbp)
	call	doapr_outch
	testl	%eax, %eax
	jne	.L840
	jmp	.L798
.L833:
	testl	%r13d, %r13d
	jne	.L548
	movl	$2147483647, %r9d
	testl	%r15d, %r15d
	jne	.L549
.L604:
	movl	%r15d, %r13d
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L831:
	cmpl	$47, %edx
	ja	.L518
	movl	%edx, %eax
	addl	$8, %edx
	addq	16(%r11), %rax
	movl	%edx, (%r11)
.L519:
	movl	(%rax), %r8d
	jmp	.L511
.L551:
	negl	%r15d
	cmpq	$0, -80(%rbp)
	je	.L794
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	jmp	.L552
.L518:
	movq	8(%r11), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 8(%r11)
	jmp	.L519
.L837:
	movq	(%rcx), %rsi
	movq	-64(%rbp), %rdx
	cmpq	%rdx, %rsi
	jbe	.L777
	jmp	.L440
.L724:
	testq	%r14, %r14
	je	.L841
	movq	%r15, %rcx
	movq	%rax, %r12
	movl	%r8d, %r15d
	xorl	%eax, %eax
	jmp	.L407
.L506:
	cmpl	$47, %edx
	ja	.L509
	movl	%edx, %eax
	addl	$8, %edx
	addq	16(%r11), %rax
	movl	%edx, (%r11)
.L510:
	movzwl	(%rax), %r8d
	jmp	.L511
.L618:
	movl	$2147483647, %eax
	xorl	%r15d, %r15d
	jmp	.L580
.L767:
	movq	%r12, %rbx
	movq	-80(%rbp), %r12
	jmp	.L459
.L509:
	movq	8(%r11), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 8(%r11)
	jmp	.L510
.L824:
	call	__stack_chk_fail@PLT
.L493:
	movq	8(%r11), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 8(%r11)
	jmp	.L494
.L828:
	movq	(%rcx), %rsi
	movq	-64(%rbp), %rdx
	cmpq	%rsi, %rdx
	jnb	.L777
	jmp	.L600
.L829:
	movq	(%rcx), %rsi
	movq	-64(%rbp), %rdx
	cmpq	%rsi, %rdx
	jnb	.L777
	jmp	.L596
.L821:
	movq	(%rcx), %rsi
	movq	-64(%rbp), %rdx
	cmpq	%rdx, %rsi
	jbe	.L777
	movq	%rax, %r12
	jmp	.L563
.L834:
	movq	%r12, %r14
	movq	%r13, %rcx
	movl	-128(%rbp), %r9d
	movq	-112(%rbp), %r12
	movq	-136(%rbp), %r10
	movq	-120(%rbp), %r11
	movl	%eax, %r13d
	jmp	.L550
.L548:
	negl	%r15d
	cmpq	$0, -80(%rbp)
	movl	$2147483647, %r9d
	jne	.L584
.L794:
	xorl	%r13d, %r13d
	jmp	.L553
.L502:
	movq	8(%r11), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 8(%r11)
	jmp	.L503
.L616:
	movl	$2147483647, %r9d
.L579:
	cmpq	$0, -80(%rbp)
	je	.L459
	xorl	%r15d, %r15d
.L584:
	movl	%r9d, %eax
	xorl	%r13d, %r13d
	shrl	$31, %eax
	movl	%eax, -88(%rbp)
	jmp	.L555
.L839:
	movq	(%rcx), %rax
	movl	%eax, %r9d
	testq	%r10, %r10
	je	.L842
.L543:
	testl	%eax, %eax
	jns	.L583
	movq	%r10, %rdi
	movq	$-1, %rsi
	movq	%r11, -136(%rbp)
	movq	%rcx, -128(%rbp)
	movl	%r9d, -120(%rbp)
	movq	%r10, -112(%rbp)
	call	OPENSSL_strnlen@PLT
	movl	-88(%rbp), %r15d
	movq	-112(%rbp), %r10
	movq	%rax, %rdi
	movl	-120(%rbp), %r9d
	movq	-128(%rbp), %rcx
	movq	%rax, -80(%rbp)
	subl	%eax, %r15d
	movq	-136(%rbp), %r11
	movl	%r15d, %eax
	shrl	$31, %eax
	orb	-104(%rbp), %al
	movb	%al, -88(%rbp)
	je	.L578
	testl	%r13d, %r13d
	jne	.L579
	xorl	%r15d, %r15d
	testq	%rdi, %rdi
	jne	.L555
	jmp	.L459
.L830:
	movq	(%rcx), %rsi
	movq	-64(%rbp), %rdx
	cmpq	%rdx, %rsi
	jbe	.L777
	jmp	.L613
.L841:
	movq	(%r15), %rsi
	movq	-64(%rbp), %rdx
	cmpq	%rdx, %rsi
	jbe	.L777
	movq	%rax, %r12
	jmp	.L431
.L764:
	movq	%r14, %r10
	movq	%rcx, %r11
	jmp	.L404
.L842:
	leaq	.LC23(%rip), %r10
	jmp	.L543
	.cfi_endproc
.LFE251:
	.size	_dopr, .-_dopr
	.p2align 4
	.globl	BIO_vprintf
	.type	BIO_vprintf, @function
BIO_vprintf:
.LFB260:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r8
	movq	%rsi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-2112(%rbp), %rcx
	leaq	-2096(%rbp), %rdx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	-2080(%rbp), %r13
	leaq	-2088(%rbp), %rsi
	leaq	-2104(%rbp), %rdi
	subq	$2120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	pushq	%r8
	leaq	-2116(%rbp), %r8
	movq	%r13, -2104(%rbp)
	movq	$2048, -2096(%rbp)
	movq	$0, -2088(%rbp)
	call	_dopr
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	je	.L849
	movq	-2088(%rbp), %rsi
	movq	-2112(%rbp), %rdx
	testq	%rsi, %rsi
	je	.L846
	movq	%r12, %rdi
	call	BIO_write@PLT
	movq	-2088(%rbp), %rdi
	movl	$886, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, %r12d
	call	CRYPTO_free@PLT
.L843:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L850
	leaq	-16(%rbp), %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L846:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	BIO_write@PLT
	movl	%eax, %r12d
	jmp	.L843
.L849:
	movq	-2088(%rbp), %rdi
	movl	$881, %edx
	leaq	.LC0(%rip), %rsi
	movl	$-1, %r12d
	call	CRYPTO_free@PLT
	jmp	.L843
.L850:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE260:
	.size	BIO_vprintf, .-BIO_vprintf
	.p2align 4
	.globl	BIO_printf
	.type	BIO_printf, @function
BIO_printf:
.LFB259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$208, %rsp
	movq	%rdx, -160(%rbp)
	movq	%rcx, -152(%rbp)
	movq	%r8, -144(%rbp)
	movq	%r9, -136(%rbp)
	testb	%al, %al
	je	.L852
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm4, -64(%rbp)
	movaps	%xmm5, -48(%rbp)
	movaps	%xmm6, -32(%rbp)
	movaps	%xmm7, -16(%rbp)
.L852:
	movq	%fs:40, %rax
	movq	%rax, -184(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	leaq	-208(%rbp), %rdx
	movl	$16, -208(%rbp)
	movq	%rax, -200(%rbp)
	leaq	-176(%rbp), %rax
	movl	$48, -204(%rbp)
	movq	%rax, -192(%rbp)
	call	BIO_vprintf
	movq	-184(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L855
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L855:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE259:
	.size	BIO_printf, .-BIO_printf
	.p2align 4
	.globl	BIO_snprintf
	.type	BIO_snprintf, @function
BIO_snprintf:
.LFB261:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$240, %rsp
	movq	%rcx, -152(%rbp)
	movq	%r8, -144(%rbp)
	movq	%r9, -136(%rbp)
	testb	%al, %al
	je	.L857
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm4, -64(%rbp)
	movaps	%xmm5, -48(%rbp)
	movaps	%xmm6, -32(%rbp)
	movaps	%xmm7, -16(%rbp)
.L857:
	movq	%fs:40, %rax
	movq	%rax, -184(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	subq	$8, %rsp
	movq	%rdx, %r9
	movq	%rax, -200(%rbp)
	leaq	-176(%rbp), %rax
	leaq	-224(%rbp), %r10
	movq	%rax, -192(%rbp)
	leaq	-208(%rbp), %rax
	leaq	-216(%rbp), %rcx
	movq	%r10, %rdx
	pushq	%rax
	leaq	-236(%rbp), %r8
	movq	%rdi, -232(%rbp)
	leaq	-232(%rbp), %rdi
	movq	%rsi, -224(%rbp)
	xorl	%esi, %esi
	movl	$24, -208(%rbp)
	movl	$48, -204(%rbp)
	call	_dopr
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	je	.L861
	movl	-236(%rbp), %eax
	testl	%eax, %eax
	jne	.L861
	movq	-216(%rbp), %rax
	movl	$-1, %edx
	cmpq	$2147483647, %rax
	cmova	%edx, %eax
.L856:
	movq	-184(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L864
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L861:
	.cfi_restore_state
	movl	$-1, %eax
	jmp	.L856
.L864:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE261:
	.size	BIO_snprintf, .-BIO_snprintf
	.p2align 4
	.globl	BIO_vsnprintf
	.type	BIO_vsnprintf, @function
BIO_vsnprintf:
.LFB262:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r8
	movq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$56, %rsp
	movq	%rdi, -40(%rbp)
	leaq	-16(%rbp), %rcx
	leaq	-48(%rbp), %rdx
	movq	%rsi, -48(%rbp)
	leaq	-40(%rbp), %rdi
	xorl	%esi, %esi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	pushq	%r8
	leaq	-20(%rbp), %r8
	call	_dopr
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	je	.L869
	movl	-20(%rbp), %eax
	testl	%eax, %eax
	jne	.L869
	movq	-16(%rbp), %rax
	movl	$-1, %edx
	cmpq	$2147483647, %rax
	cmova	%edx, %eax
.L865:
	movq	-8(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L872
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L869:
	.cfi_restore_state
	movl	$-1, %eax
	jmp	.L865
.L872:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE262:
	.size	BIO_vsnprintf, .-BIO_vsnprintf
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC6:
	.long	0
	.long	1072693248
	.align 8
.LC7:
	.long	0
	.long	1104006501
	.align 8
.LC8:
	.long	0
	.long	1097011920
	.align 8
.LC9:
	.long	0
	.long	1100470148
	.align 8
.LC10:
	.long	0
	.long	1079574528
	.align 8
.LC11:
	.long	0
	.long	1083129856
	.align 8
.LC12:
	.long	0
	.long	1086556160
	.align 8
.LC13:
	.long	0
	.long	1090021888
	.align 8
.LC14:
	.long	0
	.long	1093567616
	.align 8
.LC15:
	.long	0
	.long	1076101120
	.align 8
.LC17:
	.long	3944497965
	.long	1058682594
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC18:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC19:
	.long	0
	.long	1139802112
	.align 8
.LC20:
	.long	0
	.long	1138753536
	.align 8
.LC21:
	.long	0
	.long	1071644672
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
