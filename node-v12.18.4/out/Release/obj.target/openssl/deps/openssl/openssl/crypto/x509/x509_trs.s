	.file	"x509_trs.c"
	.text
	.p2align 4
	.type	tr_cmp, @function
tr_cmp:
.LFB1298:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rsi), %rdx
	movl	(%rax), %eax
	subl	(%rdx), %eax
	ret
	.cfi_endproc
.LFE1298:
	.size	tr_cmp, .-tr_cmp
	.p2align 4
	.type	trust_compat, @function
trust_compat:
.LFB1313:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%edx, %r12d
	xorl	%edx, %edx
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movl	$-1, %esi
	movq	%rbx, %rdi
	call	X509_check_purpose@PLT
	cmpl	$1, %eax
	jne	.L6
	andl	$4, %r12d
	jne	.L6
	testb	$32, 225(%rbx)
	movl	$3, %edx
	popq	%rbx
	cmove	%edx, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	popq	%rbx
	movl	$3, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1313:
	.size	trust_compat, .-trust_compat
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/x509/x509_trs.c"
	.text
	.p2align 4
	.type	trtable_free, @function
trtable_free:
.LFB1306:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L22
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movl	4(%rdi), %eax
	testb	$1, %al
	je	.L9
	testb	$2, %al
	jne	.L25
.L11:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$192, %edx
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	movq	16(%rdi), %rdi
	movl	$191, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE1306:
	.size	trtable_free, .-trtable_free
	.p2align 4
	.type	obj_trust, @function
obj_trust:
.LFB1314:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	328(%rsi), %rbx
	testq	%rbx, %rbx
	je	.L27
	movl	%edi, %r13d
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L28
	xorl	%r12d, %r12d
	testb	$16, %dl
	je	.L32
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L66:
	movq	8(%rbx), %rdi
	movl	%r12d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	call	OBJ_obj2nid@PLT
	cmpl	%eax, %r13d
	je	.L35
	movq	8(%rbx), %rdi
	addl	$1, %r12d
.L32:
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jl	.L66
.L28:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L27
	xorl	%r12d, %r12d
	andl	$16, %r14d
	je	.L44
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L67:
	movq	(%rbx), %rdi
	movl	%r12d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	call	OBJ_obj2nid@PLT
	cmpl	%eax, %r13d
	je	.L38
	movq	(%rbx), %rdi
	addl	$1, %r12d
.L44:
	call	OPENSSL_sk_num@PLT
	cmpl	%r12d, %eax
	jg	.L67
.L35:
	addq	$8, %rsp
	movl	$2, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	cmpl	$910, %eax
	je	.L35
	movq	8(%rbx), %rdi
	addl	$1, %r12d
.L29:
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L28
	movq	8(%rbx), %rdi
	movl	%r12d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	call	OBJ_obj2nid@PLT
	cmpl	%r13d, %eax
	jne	.L33
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L27:
	testb	$8, %r14b
	je	.L40
	xorl	%edx, %edx
	movl	$-1, %esi
	movq	%r15, %rdi
	call	X509_check_purpose@PLT
	cmpl	$1, %eax
	jne	.L40
	andl	$4, %r14d
	jne	.L40
	testb	$32, 225(%r15)
	je	.L40
.L38:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$3, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	movq	(%rbx), %rdi
	addl	$1, %r12d
.L41:
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L35
	movq	(%rbx), %rdi
	movl	%r12d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	call	OBJ_obj2nid@PLT
	cmpl	%eax, %r13d
	je	.L38
	cmpl	$910, %eax
	jne	.L68
	jmp	.L38
	.cfi_endproc
.LFE1314:
	.size	obj_trust, .-obj_trust
	.p2align 4
	.type	trust_1oid, @function
trust_1oid:
.LFB1312:
	.cfi_startproc
	endbr64
	movl	24(%rdi), %edi
	andl	$-25, %edx
	jmp	obj_trust
	.cfi_endproc
.LFE1312:
	.size	trust_1oid, .-trust_1oid
	.p2align 4
	.type	trust_1oidany, @function
trust_1oidany:
.LFB1311:
	.cfi_startproc
	endbr64
	movl	24(%rdi), %edi
	orl	$24, %edx
	jmp	obj_trust
	.cfi_endproc
.LFE1311:
	.size	trust_1oidany, .-trust_1oidany
	.p2align 4
	.globl	X509_TRUST_set_default
	.type	X509_TRUST_set_default, @function
X509_TRUST_set_default:
.LFB1299:
	.cfi_startproc
	endbr64
	movq	default_trust(%rip), %rax
	movq	%rdi, default_trust(%rip)
	ret
	.cfi_endproc
.LFE1299:
	.size	X509_TRUST_set_default, .-X509_TRUST_set_default
	.section	.text.unlikely,"ax",@progbits
.LCOLDB1:
	.text
.LHOTB1:
	.p2align 4
	.globl	X509_check_trust
	.type	X509_check_trust, @function
X509_check_trust:
.LFB1300:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	je	.L82
	leal	-1(%rsi), %eax
	movl	%esi, %r12d
	cmpl	$7, %eax
	jbe	.L83
	movq	trtable(%rip), %rdi
	testq	%rdi, %rdi
	je	.L78
	movl	%esi, -80(%rbp)
	leaq	-80(%rbp), %rsi
	call	OPENSSL_sk_find@PLT
	movl	%eax, %esi
	testl	%eax, %eax
	js	.L78
	addl	$8, %eax
	js	.L79
	movq	trtable(%rip), %rdi
	call	OPENSSL_sk_value@PLT
.L76:
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	*8(%rax)
.L72:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L84
	addq	$56, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	cltq
	leaq	(%rax,%rax,4), %rdx
	leaq	trstandard(%rip), %rax
	leaq	(%rax,%rdx,8), %rax
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L82:
	movq	%rdi, %rsi
	orl	$8, %edx
	movl	$910, %edi
	call	obj_trust
	jmp	.L72
.L78:
	movl	%r14d, %edx
	movq	%r13, %rsi
	movl	%r12d, %edi
	call	*default_trust(%rip)
	jmp	.L72
.L84:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	X509_check_trust.cold, @function
X509_check_trust.cold:
.LFSB1300:
.L79:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	8, %rax
	ud2
	.cfi_endproc
.LFE1300:
	.text
	.size	X509_check_trust, .-X509_check_trust
	.section	.text.unlikely
	.size	X509_check_trust.cold, .-X509_check_trust.cold
.LCOLDE1:
	.text
.LHOTE1:
	.p2align 4
	.globl	X509_TRUST_get_count
	.type	X509_TRUST_get_count, @function
X509_TRUST_get_count:
.LFB1301:
	.cfi_startproc
	endbr64
	movq	trtable(%rip), %rdi
	testq	%rdi, %rdi
	je	.L87
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	OPENSSL_sk_num@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	addl	$8, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore 6
	movl	$8, %eax
	ret
	.cfi_endproc
.LFE1301:
	.size	X509_TRUST_get_count, .-X509_TRUST_get_count
	.p2align 4
	.globl	X509_TRUST_get0
	.type	X509_TRUST_get0, @function
X509_TRUST_get0:
.LFB1302:
	.cfi_startproc
	endbr64
	testl	%edi, %edi
	js	.L95
	cmpl	$7, %edi
	jg	.L94
	movslq	%edi, %rdi
	leaq	trstandard(%rip), %rax
	leaq	(%rdi,%rdi,4), %rdx
	leaq	(%rax,%rdx,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	leal	-8(%rdi), %esi
	movq	trtable(%rip), %rdi
	jmp	OPENSSL_sk_value@PLT
	.p2align 4,,10
	.p2align 3
.L95:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1302:
	.size	X509_TRUST_get0, .-X509_TRUST_get0
	.p2align 4
	.globl	X509_TRUST_get_by_id
	.type	X509_TRUST_get_by_id, @function
X509_TRUST_get_by_id:
.LFB1303:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leal	-1(%rdi), %eax
	cmpl	$7, %eax
	jbe	.L96
	movq	trtable(%rip), %r8
	testq	%r8, %r8
	je	.L101
	movl	%edi, -48(%rbp)
	leaq	-48(%rbp), %rsi
	movq	%r8, %rdi
	call	OPENSSL_sk_find@PLT
	testl	%eax, %eax
	js	.L101
	addl	$8, %eax
.L96:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L104
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	movl	$-1, %eax
	jmp	.L96
.L104:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1303:
	.size	X509_TRUST_get_by_id, .-X509_TRUST_get_by_id
	.p2align 4
	.globl	X509_TRUST_set
	.type	X509_TRUST_set, @function
X509_TRUST_set:
.LFB1304:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leal	-1(%rsi), %eax
	cmpl	$7, %eax
	jbe	.L106
	movq	trtable(%rip), %rdi
	testq	%rdi, %rdi
	je	.L109
	movl	%esi, -64(%rbp)
	leaq	-64(%rbp), %rsi
	call	OPENSSL_sk_find@PLT
	testl	%eax, %eax
	js	.L109
.L106:
	movl	%ebx, (%r12)
	movl	$1, %eax
.L105:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L112
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	movl	$116, %r8d
	movl	$123, %edx
	movl	$141, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L105
.L112:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1304:
	.size	X509_TRUST_set, .-X509_TRUST_set
	.section	.text.unlikely
.LCOLDB2:
	.text
.LHOTB2:
	.p2align 4
	.globl	X509_TRUST_add
	.type	X509_TRUST_add, @function
X509_TRUST_add:
.LFB1305:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	andl	$-2, %esi
	orl	$2, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edi, %ebx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leal	-1(%rdi), %eax
	movl	%esi, -104(%rbp)
	cmpl	$7, %eax
	jbe	.L145
	movq	trtable(%rip), %rdi
	testq	%rdi, %rdi
	je	.L119
	leaq	-96(%rbp), %rsi
	movl	%ebx, -96(%rbp)
	call	OPENSSL_sk_find@PLT
	movl	%eax, %esi
	testl	%eax, %eax
	js	.L119
	addl	$8, %eax
	js	.L146
	movq	trtable(%rip), %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rcx
	testb	$2, 4(%rcx)
	jne	.L147
.L125:
	movq	%rcx, -112(%rbp)
.L144:
	movl	$150, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_strdup@PLT
	movq	-112(%rbp), %rcx
	movq	%rax, 16(%rcx)
	testq	%rax, %rax
	je	.L148
	movl	4(%rcx), %esi
	movl	%ebx, (%rcx)
	movq	%r15, 8(%rcx)
	andl	$1, %esi
	orl	-104(%rbp), %esi
	movl	%r14d, 24(%rcx)
	movl	%esi, 4(%rcx)
	movq	%r13, 32(%rcx)
.L130:
	movl	$1, %eax
.L113:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L149
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	.cfi_restore_state
	movq	16(%rcx), %rdi
	movl	$148, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rcx, -112(%rbp)
	call	CRYPTO_free@PLT
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L145:
	cltq
	leaq	(%rax,%rax,4), %rdx
	leaq	trstandard(%rip), %rax
	leaq	(%rax,%rdx,8), %rcx
	testb	$2, 4(%rcx)
	je	.L125
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L119:
	movl	$138, %edx
	leaq	.LC0(%rip), %rsi
	movl	$40, %edi
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L150
	movl	$1, 4(%rax)
	movl	$150, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	CRYPTO_strdup@PLT
	movq	-112(%rbp), %r9
	movq	%rax, 16(%r9)
	testq	%rax, %rax
	je	.L151
	movl	4(%r9), %esi
	movq	trtable(%rip), %rdi
	movl	%ebx, (%r9)
	movq	%r15, 8(%r9)
	andl	$1, %esi
	orl	-104(%rbp), %esi
	movl	%r14d, 24(%r9)
	movl	%esi, 4(%r9)
	movq	%r13, 32(%r9)
	testq	%rdi, %rdi
	je	.L152
.L128:
	movq	%r9, %rsi
	movq	%r9, -104(%rbp)
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L130
	movl	$172, %r8d
.L141:
	movl	$65, %edx
	movl	$133, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-104(%rbp), %r9
	movl	$179, %edx
	leaq	.LC0(%rip), %rsi
	movq	16(%r9), %rdi
	movq	%r9, -104(%rbp)
	call	CRYPTO_free@PLT
	movq	-104(%rbp), %r9
	movl	$180, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r9, %rdi
	call	CRYPTO_free@PLT
	xorl	%eax, %eax
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L148:
	movl	$151, %r8d
.L142:
	movl	$65, %edx
	movl	$133, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L152:
	leaq	tr_cmp(%rip), %rdi
	movq	%r9, -104(%rbp)
	call	OPENSSL_sk_new@PLT
	movq	-104(%rbp), %r9
	movl	$168, %r8d
	testq	%rax, %rax
	movq	%rax, trtable(%rip)
	movq	%rax, %rdi
	jne	.L128
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L150:
	movl	$139, %r8d
	jmp	.L142
.L149:
	call	__stack_chk_fail@PLT
.L151:
	movq	%r9, -104(%rbp)
	movl	$151, %r8d
	jmp	.L141
.L146:
	jmp	.L120
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	X509_TRUST_add.cold, @function
X509_TRUST_add.cold:
.LFSB1305:
.L120:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	4, %eax
	ud2
	.cfi_endproc
.LFE1305:
	.text
	.size	X509_TRUST_add, .-X509_TRUST_add
	.section	.text.unlikely
	.size	X509_TRUST_add.cold, .-X509_TRUST_add.cold
.LCOLDE2:
	.text
.LHOTE2:
	.p2align 4
	.globl	X509_TRUST_cleanup
	.type	X509_TRUST_cleanup, @function
X509_TRUST_cleanup:
.LFB1307:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	trtable(%rip), %rdi
	leaq	trtable_free(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	OPENSSL_sk_pop_free@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	$0, trtable(%rip)
	ret
	.cfi_endproc
.LFE1307:
	.size	X509_TRUST_cleanup, .-X509_TRUST_cleanup
	.p2align 4
	.globl	X509_TRUST_get_flags
	.type	X509_TRUST_get_flags, @function
X509_TRUST_get_flags:
.LFB1308:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	ret
	.cfi_endproc
.LFE1308:
	.size	X509_TRUST_get_flags, .-X509_TRUST_get_flags
	.p2align 4
	.globl	X509_TRUST_get0_name
	.type	X509_TRUST_get0_name, @function
X509_TRUST_get0_name:
.LFB1309:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE1309:
	.size	X509_TRUST_get0_name, .-X509_TRUST_get0_name
	.p2align 4
	.globl	X509_TRUST_get_trust
	.type	X509_TRUST_get_trust, @function
X509_TRUST_get_trust:
.LFB1310:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	ret
	.cfi_endproc
.LFE1310:
	.size	X509_TRUST_get_trust, .-X509_TRUST_get_trust
	.local	trtable
	.comm	trtable,8,8
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC3:
	.string	"compatible"
.LC4:
	.string	"SSL Client"
.LC5:
	.string	"SSL Server"
.LC6:
	.string	"S/MIME email"
.LC7:
	.string	"Object Signer"
.LC8:
	.string	"OCSP responder"
.LC9:
	.string	"OCSP request"
.LC10:
	.string	"TSA server"
	.section	.data.rel.local,"aw"
	.align 32
	.type	trstandard, @object
	.size	trstandard, 320
trstandard:
	.long	1
	.long	0
	.quad	trust_compat
	.quad	.LC3
	.long	0
	.zero	4
	.quad	0
	.long	2
	.long	0
	.quad	trust_1oidany
	.quad	.LC4
	.long	130
	.zero	4
	.quad	0
	.long	3
	.long	0
	.quad	trust_1oidany
	.quad	.LC5
	.long	129
	.zero	4
	.quad	0
	.long	4
	.long	0
	.quad	trust_1oidany
	.quad	.LC6
	.long	132
	.zero	4
	.quad	0
	.long	5
	.long	0
	.quad	trust_1oidany
	.quad	.LC7
	.long	131
	.zero	4
	.quad	0
	.long	6
	.long	0
	.quad	trust_1oid
	.quad	.LC8
	.long	180
	.zero	4
	.quad	0
	.long	7
	.long	0
	.quad	trust_1oid
	.quad	.LC9
	.long	178
	.zero	4
	.quad	0
	.long	8
	.long	0
	.quad	trust_1oidany
	.quad	.LC10
	.long	133
	.zero	4
	.quad	0
	.align 8
	.type	default_trust, @object
	.size	default_trust, 8
default_trust:
	.quad	obj_trust
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
