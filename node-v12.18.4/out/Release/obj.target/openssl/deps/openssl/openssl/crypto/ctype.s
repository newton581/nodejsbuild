	.file	"ctype.c"
	.text
	.p2align 4
	.globl	ossl_ctype_check
	.type	ossl_ctype_check, @function
ossl_ctype_check:
.LFB50:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$127, %edi
	ja	.L1
	movslq	%edi, %rdi
	leaq	ctype_char_map(%rip), %rax
	movzwl	(%rax,%rdi,2), %eax
	testl	%esi, %eax
	setne	%al
	movzbl	%al, %eax
.L1:
	ret
	.cfi_endproc
.LFE50:
	.size	ossl_ctype_check, .-ossl_ctype_check
	.p2align 4
	.globl	ossl_tolower
	.type	ossl_tolower, @function
ossl_tolower:
.LFB51:
	.cfi_startproc
	endbr64
	movl	%edi, %eax
	cmpl	$127, %edi
	ja	.L6
	movslq	%edi, %rsi
	movl	%edi, %edx
	leaq	ctype_char_map(%rip), %rcx
	xorl	$32, %edx
	testb	$2, (%rcx,%rsi,2)
	cmovne	%edx, %eax
.L6:
	ret
	.cfi_endproc
.LFE51:
	.size	ossl_tolower, .-ossl_tolower
	.p2align 4
	.globl	ossl_toupper
	.type	ossl_toupper, @function
ossl_toupper:
.LFB52:
	.cfi_startproc
	endbr64
	movl	%edi, %eax
	cmpl	$127, %edi
	ja	.L10
	movslq	%edi, %rsi
	movl	%edi, %edx
	leaq	ctype_char_map(%rip), %rcx
	xorl	$32, %edx
	testb	$1, (%rcx,%rsi,2)
	cmovne	%edx, %eax
.L10:
	ret
	.cfi_endproc
.LFE52:
	.size	ossl_toupper, .-ossl_toupper
	.p2align 4
	.globl	ascii_isdigit
	.type	ascii_isdigit, @function
ascii_isdigit:
.LFB53:
	.cfi_startproc
	endbr64
	subl	$48, %edi
	xorl	%eax, %eax
	cmpb	$9, %dil
	setbe	%al
	ret
	.cfi_endproc
.LFE53:
	.size	ascii_isdigit, .-ascii_isdigit
	.section	.rodata
	.align 32
	.type	ctype_char_map, @object
	.size	ctype_char_map, 256
ctype_char_map:
	.value	64
	.value	64
	.value	64
	.value	64
	.value	64
	.value	64
	.value	64
	.value	64
	.value	64
	.value	104
	.value	72
	.value	72
	.value	72
	.value	72
	.value	64
	.value	64
	.value	64
	.value	64
	.value	64
	.value	64
	.value	64
	.value	64
	.value	64
	.value	64
	.value	64
	.value	64
	.value	64
	.value	64
	.value	64
	.value	64
	.value	64
	.value	64
	.value	2344
	.value	896
	.value	896
	.value	896
	.value	896
	.value	896
	.value	896
	.value	2944
	.value	2944
	.value	2944
	.value	896
	.value	3968
	.value	2944
	.value	2944
	.value	2944
	.value	3968
	.value	3476
	.value	3476
	.value	3476
	.value	3476
	.value	3476
	.value	3476
	.value	3476
	.value	3476
	.value	3476
	.value	3476
	.value	2944
	.value	896
	.value	896
	.value	3968
	.value	896
	.value	2944
	.value	896
	.value	3474
	.value	3474
	.value	3474
	.value	3474
	.value	3474
	.value	3474
	.value	3458
	.value	3458
	.value	3458
	.value	3458
	.value	3458
	.value	3458
	.value	3458
	.value	3458
	.value	3458
	.value	3458
	.value	3458
	.value	3458
	.value	3458
	.value	3458
	.value	3458
	.value	3458
	.value	3458
	.value	3458
	.value	3458
	.value	3458
	.value	896
	.value	896
	.value	896
	.value	896
	.value	896
	.value	896
	.value	3473
	.value	3473
	.value	3473
	.value	3473
	.value	3473
	.value	3473
	.value	3457
	.value	3457
	.value	3457
	.value	3457
	.value	3457
	.value	3457
	.value	3457
	.value	3457
	.value	3457
	.value	3457
	.value	3457
	.value	3457
	.value	3457
	.value	3457
	.value	3457
	.value	3457
	.value	3457
	.value	3457
	.value	3457
	.value	3457
	.value	896
	.value	896
	.value	896
	.value	896
	.value	64
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
