	.file	"o_str.c"
	.text
	.p2align 4
	.globl	OPENSSL_memcmp
	.type	OPENSSL_memcmp, @function
OPENSSL_memcmp:
.LFB262:
	.cfi_startproc
	endbr64
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	jne	.L2
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L4:
	addq	$1, %rcx
	cmpq	%rcx, %rdx
	je	.L1
.L2:
	movzbl	(%rdi,%rcx), %eax
	movzbl	(%rsi,%rcx), %r8d
	subl	%r8d, %eax
	je	.L4
.L1:
	ret
.L8:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE262:
	.size	OPENSSL_memcmp, .-OPENSSL_memcmp
	.p2align 4
	.globl	CRYPTO_strdup
	.type	CRYPTO_strdup, @function
CRYPTO_strdup:
.LFB263:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L11
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	strlen@PLT
	movl	%r14d, %edx
	movq	%r13, %rsi
	leaq	1(%rax), %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L9
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	strcpy@PLT
	movq	%rax, %r8
.L9:
	addq	$8, %rsp
	movq	%r8, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%r8d, %r8d
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE263:
	.size	CRYPTO_strdup, .-CRYPTO_strdup
	.p2align 4
	.globl	CRYPTO_strndup
	.type	CRYPTO_strndup, @function
CRYPTO_strndup:
.LFB264:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L25
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movq	%r12, %rax
	movq	%rdx, %rsi
	leaq	(%rdi,%rbx), %rdi
	movl	%ecx, %edx
	testq	%rbx, %rbx
	jne	.L23
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L24:
	addq	$1, %rax
	cmpq	%rdi, %rax
	je	.L35
.L23:
	cmpb	$0, (%rax)
	jne	.L24
	subq	%r12, %rax
	movq	%rax, %rbx
.L35:
	leaq	1(%rbx), %rdi
.L22:
	call	CRYPTO_malloc@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L19
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movb	$0, (%rax,%rbx)
	movq	%rax, %r8
.L19:
	popq	%rbx
	movq	%r8, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
.L25:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	xorl	%r8d, %r8d
	movq	%r8, %rax
	ret
.L36:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	$1, %edi
	jmp	.L22
	.cfi_endproc
.LFE264:
	.size	CRYPTO_strndup, .-CRYPTO_strndup
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/o_str.c"
	.text
	.p2align 4
	.globl	CRYPTO_memdup
	.type	CRYPTO_memdup, @function
CRYPTO_memdup:
.LFB265:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L42
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	cmpq	$2147483646, %rsi
	ja	.L37
	movq	%rdx, %rsi
	movq	%rdi, %r13
	movl	%ecx, %edx
	movq	%r12, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L45
	movq	%r12, %rdx
	movq	%r13, %rsi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	memcpy@PLT
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	movl	$66, %r8d
	movl	$65, %edx
	movl	$115, %esi
	movl	$15, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L37:
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE265:
	.size	CRYPTO_memdup, .-CRYPTO_memdup
	.p2align 4
	.globl	OPENSSL_strnlen
	.type	OPENSSL_strnlen, @function
OPENSSL_strnlen:
.LFB266:
	.cfi_startproc
	endbr64
	movq	%rsi, %rax
	leaq	(%rdi,%rsi), %rcx
	movq	%rdi, %rdx
	testq	%rsi, %rsi
	jne	.L49
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L50:
	addq	$1, %rdx
	cmpq	%rdx, %rcx
	je	.L48
.L49:
	cmpb	$0, (%rdx)
	jne	.L50
	movq	%rdx, %rax
	subq	%rdi, %rax
.L48:
	ret
.L53:
	ret
	.cfi_endproc
.LFE266:
	.size	OPENSSL_strnlen, .-OPENSSL_strnlen
	.p2align 4
	.globl	OPENSSL_strlcpy
	.type	OPENSSL_strlcpy, @function
OPENSSL_strlcpy:
.LFB267:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdx, %rbx
	subq	$8, %rsp
	cmpq	$1, %rdx
	jbe	.L55
	leaq	-1(%rdx), %rcx
	xorl	%ebx, %ebx
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L58:
	addq	$1, %rbx
	addq	$1, %rax
	addq	$1, %rdi
	movb	%dl, -1(%rax)
	cmpq	%rbx, %rcx
	je	.L57
.L56:
	movzbl	(%rdi), %edx
	testb	%dl, %dl
	jne	.L58
.L57:
	movb	$0, (%rax)
.L59:
	call	strlen@PLT
	addq	$8, %rsp
	addq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L55:
	.cfi_restore_state
	testq	%rdx, %rdx
	je	.L59
	xorl	%ebx, %ebx
	jmp	.L57
	.cfi_endproc
.LFE267:
	.size	OPENSSL_strlcpy, .-OPENSSL_strlcpy
	.p2align 4
	.globl	OPENSSL_strlcat
	.type	OPENSSL_strlcat, @function
OPENSSL_strlcat:
.LFB268:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdx, %rbx
	xorl	%edx, %edx
	subq	$8, %rsp
	testq	%rbx, %rbx
	jne	.L66
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L68:
	addq	$1, %rdx
	addq	$1, %rdi
	cmpq	%rdx, %rbx
	je	.L67
.L66:
	movq	%rbx, %rax
	subq	%rdx, %rax
	cmpb	$0, (%rdi)
	jne	.L68
	cmpq	$1, %rax
	jbe	.L82
	leaq	-1(%rax), %rsi
	xorl	%eax, %eax
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L73:
	addq	$1, %rax
	addq	$1, %rdi
	addq	$1, %r8
	movb	%cl, -1(%rdi)
	cmpq	%rsi, %rax
	je	.L81
.L71:
	movzbl	(%r8), %ecx
	testb	%cl, %cl
	jne	.L73
.L81:
	leaq	(%rdx,%rax), %rbx
.L72:
	movb	$0, (%rdi)
.L67:
	movq	%r8, %rdi
	call	strlen@PLT
	addq	$8, %rsp
	addq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L82:
	.cfi_restore_state
	movq	%rdx, %rbx
	testq	%rax, %rax
	jne	.L72
	jmp	.L67
	.cfi_endproc
.LFE268:
	.size	OPENSSL_strlcat, .-OPENSSL_strlcat
	.p2align 4
	.globl	OPENSSL_hexchar2int
	.type	OPENSSL_hexchar2int, @function
OPENSSL_hexchar2int:
.LFB269:
	.cfi_startproc
	endbr64
	subl	$48, %edi
	movl	$-1, %eax
	cmpb	$54, %dil
	ja	.L83
	movzbl	%dil, %edi
	leaq	CSWTCH.19(%rip), %rax
	movsbl	(%rax,%rdi), %eax
.L83:
	ret
	.cfi_endproc
.LFE269:
	.size	OPENSSL_hexchar2int, .-OPENSSL_hexchar2int
	.p2align 4
	.globl	OPENSSL_hexstr2buf
	.type	OPENSSL_hexstr2buf, @function
OPENSSL_hexstr2buf:
.LFB270:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	strlen@PLT
	leaq	.LC0(%rip), %rsi
	movl	$156, %edx
	shrq	%rax
	movq	%rax, %rdi
	call	CRYPTO_malloc@PLT
	leaq	CSWTCH.19(%rip), %r8
	movq	%rax, %r13
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L102
.L89:
	movzbl	(%r12), %edx
	testb	%dl, %dl
	je	.L103
.L94:
	leaq	1(%r12), %rax
	cmpb	$58, %dl
	je	.L96
	movzbl	1(%r12), %ecx
	leaq	2(%r12), %rdi
	testb	%cl, %cl
	je	.L104
	subl	$48, %ecx
	cmpb	$54, %cl
	jbe	.L105
.L91:
	movq	%r13, %rdi
	movl	$174, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%r13d, %r13d
	call	CRYPTO_free@PLT
	movl	$175, %r8d
	movl	$102, %edx
	leaq	.LC0(%rip), %rcx
	movl	$118, %esi
	movl	$15, %edi
	call	ERR_put_error@PLT
.L86:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore_state
	movzbl	%cl, %ecx
	subl	$48, %edx
	movzbl	(%r8,%rcx), %ecx
	cmpb	$54, %dl
	ja	.L91
	movzbl	%dl, %edx
	movsbl	(%r8,%rdx), %eax
	testb	%cl, %cl
	js	.L91
	testl	%eax, %eax
	js	.L91
	sall	$4, %eax
	addq	$1, %rsi
	movq	%rdi, %r12
	orl	%ecx, %eax
	movb	%al, -1(%rsi)
	movzbl	(%r12), %edx
	testb	%dl, %dl
	jne	.L94
.L103:
	testq	%rbx, %rbx
	je	.L86
	subq	%r13, %rsi
	movq	%r13, %rax
	movq	%rsi, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	movq	%rax, %r12
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L104:
	movl	$166, %r8d
	movl	$103, %edx
	movl	$118, %esi
	movl	$15, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	movl	$168, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L102:
	.cfi_restore_state
	movl	$157, %r8d
	movl	$65, %edx
	movl	$118, %esi
	movl	$15, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L86
	.cfi_endproc
.LFE270:
	.size	OPENSSL_hexstr2buf, .-OPENSSL_hexstr2buf
	.p2align 4
	.globl	OPENSSL_buf2hexstr
	.type	OPENSSL_buf2hexstr, @function
OPENSSL_buf2hexstr:
.LFB271:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L117
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$203, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	(%rsi,%rsi,2), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	leaq	.LC0(%rip), %rsi
	subq	$24, %rsp
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L108
	addq	%rax, %r13
	movq	%rax, %rdx
	leaq	hexdig.7074(%rip), %rsi
	testq	%rbx, %rbx
	jle	.L118
.L112:
	movzbl	(%r12), %ecx
	addq	$3, %rdx
	addq	$1, %r12
	shrb	$4, %cl
	andl	$15, %ecx
	movzbl	(%rsi,%rcx), %ecx
	movb	%cl, -3(%rdx)
	movzbl	-1(%r12), %ecx
	andl	$15, %ecx
	movzbl	(%rsi,%rcx), %ecx
	movb	%cl, -2(%rdx)
	movb	$58, -1(%rdx)
	cmpq	%r13, %rdx
	jne	.L112
.L110:
	movb	$0, -1(%r13)
.L106:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$200, %edx
	leaq	.LC0(%rip), %rsi
	movl	$1, %edi
	jmp	CRYPTO_zalloc@PLT
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	%rax, %r13
	jmp	.L110
.L108:
	movl	$204, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$117, %esi
	movl	$15, %edi
	movq	%rax, -40(%rbp)
	call	ERR_put_error@PLT
	movq	-40(%rbp), %rax
	jmp	.L106
	.cfi_endproc
.LFE271:
	.size	OPENSSL_buf2hexstr, .-OPENSSL_buf2hexstr
	.p2align 4
	.globl	openssl_strerror_r
	.type	openssl_strerror_r, @function
openssl_strerror_r:
.LFB272:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__xpg_strerror_r@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE272:
	.size	openssl_strerror_r, .-openssl_strerror_r
	.section	.rodata
	.align 32
	.type	CSWTCH.19, @object
	.size	CSWTCH.19, 55
CSWTCH.19:
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	4
	.byte	5
	.byte	6
	.byte	7
	.byte	8
	.byte	9
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	10
	.byte	11
	.byte	12
	.byte	13
	.byte	14
	.byte	15
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	10
	.byte	11
	.byte	12
	.byte	13
	.byte	14
	.byte	15
	.align 16
	.type	hexdig.7074, @object
	.size	hexdig.7074, 17
hexdig.7074:
	.string	"0123456789ABCDEF"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
