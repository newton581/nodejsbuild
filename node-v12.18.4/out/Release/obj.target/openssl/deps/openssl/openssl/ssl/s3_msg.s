	.file	"s3_msg.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/ssl/s3_msg.c"
	.text
	.p2align 4
	.globl	ssl3_do_change_cipher_spec
	.type	ssl3_do_change_cipher_spec, @function
ssl3_do_change_cipher_spec:
.LFB964:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	cmpl	$1, 56(%rdi)
	movq	168(%rdi), %rax
	sbbl	%esi, %esi
	andl	$-16, %esi
	addl	$33, %esi
	cmpq	$0, 624(%rax)
	je	.L3
.L7:
	movq	8(%rdi), %rax
	movq	192(%rax), %rax
	call	*32(%rax)
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	movq	1296(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L5
	cmpq	$0, 8(%rdx)
	je	.L5
	movq	568(%rax), %rax
	movl	%esi, -12(%rbp)
	movq	%rdi, -8(%rbp)
	movq	%rax, 504(%rdx)
	movq	8(%rdi), %rax
	movq	192(%rax), %rax
	call	*16(%rax)
	movq	-8(%rbp), %rdi
	movl	-12(%rbp), %esi
	testl	%eax, %eax
	jne	.L7
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	movl	$24, %r8d
	movl	$133, %edx
	movl	$292, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE964:
	.size	ssl3_do_change_cipher_spec, .-ssl3_do_change_cipher_spec
	.p2align 4
	.globl	ssl3_send_alert
	.type	ssl3_send_alert, @function
ssl3_send_alert:
.LFB965:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	%edx, %edi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	8(%r12), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L15
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L15
	cmpl	$65536, %eax
	jne	.L16
.L15:
	movl	132(%r12), %eax
	subl	$1, %eax
	cmpl	$3, %eax
	ja	.L38
.L16:
	call	tls13_alert_code@PLT
.L19:
	cmpl	$768, (%r12)
	jne	.L27
	cmpl	$70, %eax
	je	.L26
.L27:
	testl	%eax, %eax
	js	.L23
	movl	%eax, %ebx
.L20:
	cmpl	$2, %r13d
	je	.L39
.L24:
	movq	168(%r12), %rax
	leaq	2112(%r12), %rdi
	movl	$1, 252(%rax)
	movb	%r13b, 256(%rax)
	movb	%bl, 257(%rax)
	call	RECORD_LAYER_write_pending@PLT
	testl	%eax, %eax
	jne	.L23
	movq	8(%r12), %rax
	movq	%r12, %rdi
	movq	120(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	movq	1296(%r12), %rsi
	testq	%rsi, %rsi
	je	.L24
	movq	1904(%r12), %rdi
	call	SSL_CTX_remove_session@PLT
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L26:
	movl	$40, %ebx
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L38:
	cmpl	$1, 1248(%r12)
	je	.L16
	call	*80(%rdx)
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L23:
	addq	$8, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE965:
	.size	ssl3_send_alert, .-ssl3_send_alert
	.p2align 4
	.globl	ssl3_dispatch_alert
	.type	ssl3_dispatch_alert, @function
ssl3_dispatch_alert:
.LFB966:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$21, %esi
	xorl	%r9d, %r9d
	movl	$1, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-40(%rbp), %rcx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$40, %rsp
	movq	168(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	-32(%rbp), %rax
	movq	$2, -40(%rbp)
	movl	$0, 252(%rdx)
	addq	$256, %rdx
	pushq	%rax
	call	do_ssl3_write@PLT
	popq	%rcx
	popq	%rsi
	movl	%eax, %r13d
	testl	%eax, %eax
	jg	.L41
	movq	168(%r12), %rax
	movl	$1, 252(%rax)
.L40:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L50
	leaq	-16(%rbp), %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movq	24(%r12), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movq	184(%r12), %rax
	testq	%rax, %rax
	je	.L43
	movq	168(%r12), %rsi
	subq	$8, %rsp
	movl	$21, %edx
	movq	%r12, %r9
	pushq	192(%r12)
	movl	$2, %r8d
	movl	$1, %edi
	leaq	256(%rsi), %rcx
	movl	(%r12), %esi
	call	*%rax
	popq	%rax
	popq	%rdx
.L43:
	movq	1392(%r12), %rax
	testq	%rax, %rax
	je	.L51
.L44:
	movq	168(%r12), %rdx
	movl	$16392, %esi
	movq	%r12, %rdi
	movzwl	256(%rdx), %edx
	rolw	$8, %dx
	movzwl	%dx, %edx
	call	*%rax
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L51:
	movq	1440(%r12), %rax
	movq	272(%rax), %rax
	testq	%rax, %rax
	jne	.L44
	jmp	.L40
.L50:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE966:
	.size	ssl3_dispatch_alert, .-ssl3_dispatch_alert
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
