	.file	"x_attrib.c"
	.text
	.p2align 4
	.globl	d2i_X509_ATTRIBUTE
	.type	d2i_X509_ATTRIBUTE, @function
d2i_X509_ATTRIBUTE:
.LFB877:
	.cfi_startproc
	endbr64
	leaq	X509_ATTRIBUTE_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE877:
	.size	d2i_X509_ATTRIBUTE, .-d2i_X509_ATTRIBUTE
	.p2align 4
	.globl	i2d_X509_ATTRIBUTE
	.type	i2d_X509_ATTRIBUTE, @function
i2d_X509_ATTRIBUTE:
.LFB878:
	.cfi_startproc
	endbr64
	leaq	X509_ATTRIBUTE_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE878:
	.size	i2d_X509_ATTRIBUTE, .-i2d_X509_ATTRIBUTE
	.p2align 4
	.globl	X509_ATTRIBUTE_new
	.type	X509_ATTRIBUTE_new, @function
X509_ATTRIBUTE_new:
.LFB879:
	.cfi_startproc
	endbr64
	leaq	X509_ATTRIBUTE_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE879:
	.size	X509_ATTRIBUTE_new, .-X509_ATTRIBUTE_new
	.p2align 4
	.globl	X509_ATTRIBUTE_free
	.type	X509_ATTRIBUTE_free, @function
X509_ATTRIBUTE_free:
.LFB880:
	.cfi_startproc
	endbr64
	leaq	X509_ATTRIBUTE_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE880:
	.size	X509_ATTRIBUTE_free, .-X509_ATTRIBUTE_free
	.p2align 4
	.globl	X509_ATTRIBUTE_dup
	.type	X509_ATTRIBUTE_dup, @function
X509_ATTRIBUTE_dup:
.LFB881:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	leaq	X509_ATTRIBUTE_it(%rip), %rdi
	jmp	ASN1_item_dup@PLT
	.cfi_endproc
.LFE881:
	.size	X509_ATTRIBUTE_dup, .-X509_ATTRIBUTE_dup
	.p2align 4
	.globl	X509_ATTRIBUTE_create
	.type	X509_ATTRIBUTE_create, @function
X509_ATTRIBUTE_create:
.LFB882:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edi, %r13d
	leaq	X509_ATTRIBUTE_it(%rip), %rdi
	pushq	%r12
	.cfi_offset 12, -48
	call	ASN1_item_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L7
	movl	%r13d, %edi
	call	OBJ_nid2obj@PLT
	movq	%rax, (%r12)
	call	ASN1_TYPE_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L9
	movq	8(%r12), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L9
	movq	%r15, %rdx
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	ASN1_TYPE_set@PLT
.L7:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	movq	%r12, %rdi
	leaq	X509_ATTRIBUTE_it(%rip), %rsi
	xorl	%r12d, %r12d
	call	ASN1_item_free@PLT
	movq	%r13, %rdi
	call	ASN1_TYPE_free@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE882:
	.size	X509_ATTRIBUTE_create, .-X509_ATTRIBUTE_create
	.globl	X509_ATTRIBUTE_it
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"X509_ATTRIBUTE"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	X509_ATTRIBUTE_it, @object
	.size	X509_ATTRIBUTE_it, 56
X509_ATTRIBUTE_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	X509_ATTRIBUTE_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC0
	.section	.rodata.str1.1
.LC1:
	.string	"object"
.LC2:
	.string	"set"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	X509_ATTRIBUTE_seq_tt, @object
	.size	X509_ATTRIBUTE_seq_tt, 80
X509_ATTRIBUTE_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC1
	.quad	ASN1_OBJECT_it
	.quad	2
	.quad	0
	.quad	8
	.quad	.LC2
	.quad	ASN1_ANY_it
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
