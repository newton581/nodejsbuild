	.file	"fcrypt.c"
	.text
	.p2align 4
	.globl	DES_fcrypt
	.type	DES_fcrypt, @function
DES_fcrypt:
.LFB152:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	subq	$176, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movsbl	(%rsi), %eax
	movb	%al, (%rdx)
	leal	-1(%rax), %edx
	cmpl	$126, %edx
	ja	.L4
	movl	%eax, %eax
	leaq	con_salt(%rip), %rdx
	movzbl	(%rdx,%rax), %r12d
	movsbl	1(%rsi), %eax
	leal	-1(%rax), %ecx
	movb	%al, 1(%rbx)
	sall	$2, %r12d
	cmpl	$126, %ecx
	ja	.L4
	movl	%eax, %eax
	movzbl	(%rdx,%rax), %r13d
	movzbl	(%rdi), %eax
	sall	$6, %r13d
	testb	%al, %al
	je	.L60
	addl	%eax, %eax
	movb	%al, -57(%rbp)
	movzbl	1(%rdi), %eax
	testb	%al, %al
	je	.L61
	addl	%eax, %eax
	movb	%al, -56(%rbp)
	movzbl	2(%rdi), %eax
	testb	%al, %al
	je	.L62
	addl	%eax, %eax
	movb	%al, -55(%rbp)
	movzbl	3(%rdi), %eax
	testb	%al, %al
	je	.L63
	addl	%eax, %eax
	movb	%al, -54(%rbp)
	movzbl	4(%rdi), %eax
	testb	%al, %al
	je	.L64
	addl	%eax, %eax
	movb	%al, -53(%rbp)
	movzbl	5(%rdi), %eax
	testb	%al, %al
	je	.L65
	addl	%eax, %eax
	movb	%al, -52(%rbp)
	movzbl	6(%rdi), %eax
	testb	%al, %al
	je	.L66
	addl	%eax, %eax
	movl	$7, %edx
	movb	%al, -51(%rbp)
	movzbl	7(%rdi), %eax
	testb	%al, %al
	je	.L5
	addl	%eax, %eax
	leaq	-57(%rbp), %rdi
	movb	%al, -50(%rbp)
.L9:
	leaq	-192(%rbp), %r14
	movq	%r14, %rsi
	call	DES_set_key_unchecked@PLT
	movl	%r13d, %ecx
	movl	%r12d, %edx
	movq	%r14, %rsi
	leaq	-200(%rbp), %rdi
	call	fcrypt_body@PLT
	movb	$0, -41(%rbp)
	leaq	2(%rbx), %rsi
	xorl	%ecx, %ecx
	movq	-200(%rbp), %rdx
	leaq	13(%rbx), %r9
	movzbl	-200(%rbp), %eax
	movl	$-128, %r10d
	leaq	cov_2char(%rip), %r8
	movq	%rdx, -49(%rbp)
	.p2align 4,,10
	.p2align 3
.L8:
	testb	%al, %r10b
	movl	%r10d, %r11d
	setne	%dl
	shrb	%r11b
	leal	(%rdx,%rdx), %edi
	jne	.L16
	leal	1(%rcx), %eax
	movzbl	-49(%rbp,%rax), %r10d
	movq	%rax, %rcx
	testb	%r10b, %r10b
	jns	.L17
	leal	0(,%rdx,4), %edi
	orl	$2, %edi
	andl	$64, %r10d
	je	.L84
	sall	$3, %edx
	movl	$32, %edi
	orl	$6, %edx
.L25:
	movl	%edi, %r10d
	shrb	%r10b
	testb	%dil, -49(%rbp,%rax)
	je	.L85
.L27:
	orl	$1, %edx
.L28:
	leal	(%rdx,%rdx), %edi
	testb	%r10b, %r10b
	jne	.L57
	leal	1(%rcx), %eax
	movzbl	-49(%rbp,%rax), %edi
	movq	%rax, %rcx
	testb	%dil, %dil
	jns	.L34
	leal	2(,%rdx,4), %edx
	movl	$64, %r11d
.L35:
	movl	%r11d, %r10d
	shrb	%r10b
	testb	%r11b, -49(%rbp,%rax)
	je	.L39
.L37:
	orl	$1, %edx
.L38:
	testb	%r10b, %r10b
	jne	.L39
	addl	$1, %ecx
	movl	$-128, %r10d
.L39:
	movzbl	%dl, %edx
	addq	$1, %rsi
	movzbl	(%r8,%rdx), %eax
	movb	%al, -1(%rsi)
	cmpq	%rsi, %r9
	je	.L41
	movl	%ecx, %eax
	movzbl	-49(%rbp,%rax), %eax
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L16:
	movl	%edi, %edx
	orl	$1, %edx
	testb	%al, %r11b
	cmovne	%edx, %edi
	movl	%r10d, %edx
	shrb	$2, %dl
	leal	(%rdi,%rdi), %r11d
	jne	.L86
	leal	1(%rcx), %eax
	movzbl	-49(%rbp,%rax), %r10d
	movq	%rax, %rcx
	testb	%r10b, %r10b
	jns	.L24
	leal	2(,%rdi,4), %edx
	movl	$64, %edi
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L34:
	sall	$2, %edx
	andl	$64, %edi
	movl	$32, %r10d
	je	.L39
	orl	$1, %edx
	movl	$32, %r10d
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L17:
	testb	$64, %r10b
	je	.L87
	sall	$3, %edx
	movl	$32, %edi
	orl	$2, %edx
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L24:
	testb	$64, %r10b
	je	.L88
	leal	2(,%rdi,8), %edi
	movl	$32, %r11d
.L30:
	movl	%r11d, %r12d
	shrb	%r12b
	testb	%r11b, -49(%rbp,%rax)
	je	.L89
.L32:
	orl	$1, %edi
.L33:
	leal	(%rdi,%rdi), %edx
	testb	%r12b, %r12b
	jne	.L55
	leal	1(%rcx), %eax
	movl	$64, %r10d
	cmpb	$0, -49(%rbp,%rax)
	movq	%rax, %rcx
	jns	.L39
	orl	$1, %edx
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L88:
	sall	$4, %edi
	movl	$16, %r11d
	movl	%edi, %edx
	orl	$2, %edx
	andl	$32, %r10d
	jne	.L35
	movl	%edi, %edx
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L87:
	sall	$4, %edx
	movl	$16, %r11d
	movl	%edx, %edi
	orl	$2, %edi
	andl	$32, %r10d
	jne	.L30
	movl	%edx, %edi
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L41:
	movb	$0, 13(%rbx)
	movq	%rbx, %rax
.L1:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L90
	addq	$176, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L1
.L65:
	movl	$5, %edx
.L5:
	movl	$7, %eax
	leaq	-57(%rbp), %rdi
	xorl	%r8d, %r8d
	subl	%edx, %eax
	addq	%rdi, %rdx
	addq	$1, %rax
	cmpl	$8, %eax
	jb	.L91
	movl	%eax, %ecx
	movq	$0, (%rdx)
	movq	$0, -8(%rdx,%rcx)
	leaq	8(%rdx), %rcx
	andq	$-8, %rcx
	subq	%rcx, %rdx
	addl	%edx, %eax
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L9
	andl	$-8, %eax
	xorl	%edx, %edx
.L14:
	movl	%edx, %esi
	addl	$8, %edx
	movq	%r8, (%rcx,%rsi)
	cmpl	%eax, %edx
	jb	.L14
	jmp	.L9
.L91:
	testb	$4, %al
	jne	.L92
	testl	%eax, %eax
	je	.L9
	movb	$0, (%rdx)
	testb	$2, %al
	je	.L9
	movl	%eax, %eax
	xorl	%ecx, %ecx
	movw	%cx, -2(%rdx,%rax)
	jmp	.L9
.L61:
	movl	$1, %edx
	jmp	.L5
.L60:
	xorl	%edx, %edx
	jmp	.L5
.L64:
	movl	$4, %edx
	jmp	.L5
.L63:
	movl	$3, %edx
	jmp	.L5
.L62:
	movl	$2, %edx
	jmp	.L5
.L84:
	leal	(%rdi,%rdi), %edx
	movl	$32, %edi
	jmp	.L25
.L66:
	movl	$6, %edx
	jmp	.L5
.L92:
	movl	%eax, %eax
	movl	$0, (%rdx)
	movl	$0, -4(%rdx,%rax)
	jmp	.L9
.L85:
	leal	(%rdx,%rdx), %edi
.L57:
	movl	%r10d, %r12d
	shrb	%r12b
	testb	%r10b, -49(%rbp,%rax)
	je	.L33
	jmp	.L32
.L86:
	movl	%r10d, %edi
	movl	%ecx, %eax
	shrb	$3, %dil
	testb	%dl, -49(%rbp,%rax)
	je	.L23
	orl	$1, %r11d
.L23:
	leal	(%r11,%r11), %edx
	testb	%dil, %dil
	jne	.L93
	leal	1(%rcx), %eax
	movzbl	-49(%rbp,%rax), %edi
	movq	%rax, %rcx
	testb	%dil, %dil
	jns	.L29
	leal	2(,%r11,4), %edi
	movl	$64, %r11d
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L29:
	testb	$64, %dil
	je	.L94
	leal	2(,%r11,8), %edx
	movl	$32, %r11d
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L94:
	andl	$32, %edi
	leal	0(,%r11,8), %edx
	movl	$16, %r10d
	je	.L39
	orl	$1, %edx
	movl	$16, %r10d
	jmp	.L39
.L93:
	shrb	$4, %r10b
	testb	%dil, -49(%rbp,%rax)
	je	.L28
	jmp	.L27
.L89:
	leal	(%rdi,%rdi), %edx
.L55:
	movl	%r12d, %r10d
	shrb	%r10b
	testb	%r12b, -49(%rbp,%rax)
	je	.L38
	jmp	.L37
.L90:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE152:
	.size	DES_fcrypt, .-DES_fcrypt
	.p2align 4
	.globl	DES_crypt
	.type	DES_crypt, @function
DES_crypt:
.LFB151:
	.cfi_startproc
	endbr64
	leaq	buff.5101(%rip), %rdx
	jmp	DES_fcrypt
	.cfi_endproc
.LFE151:
	.size	DES_crypt, .-DES_crypt
	.local	buff.5101
	.comm	buff.5101,14,8
	.section	.rodata
	.align 32
	.type	cov_2char, @object
	.size	cov_2char, 64
cov_2char:
	.ascii	"./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuv"
	.ascii	"wxyz"
	.align 32
	.type	con_salt, @object
	.size	con_salt, 128
con_salt:
	.string	"\322\323\324\325\326\327\330\331\332\333\334\335\336\337\340\341\342\343\344\345\346\347\350\351\352\353\354\355\356\357\360\361\362\363\364\365\366\367\370\371\372\373\374\375\376\377"
	.ascii	"\001\002\003\004\005\006\007\b\t\n\013\005\006\007\b\t\n\013"
	.ascii	"\f\r\016\017\020\021\022\023\024\025\026\027\030\031\032\033"
	.ascii	"\034\035\036\037 !\"#$% !\"#$%&'()*+,-./0123456789:;<=>?@ABC"
	.ascii	"D"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
