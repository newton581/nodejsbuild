	.file	"v3_ia5.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/x509v3/v3_ia5.c"
	.text
	.p2align 4
	.globl	i2s_ASN1_IA5STRING
	.type	i2s_ASN1_IA5STRING, @function
i2s_ASN1_IA5STRING:
.LFB1296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	testq	%rsi, %rsi
	je	.L4
	movl	(%rsi), %edi
	movq	%rsi, %rbx
	xorl	%r12d, %r12d
	testl	%edi, %edi
	je	.L1
	addl	$1, %edi
	movl	$34, %edx
	leaq	.LC0(%rip), %rsi
	movslq	%edi, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L9
	movslq	(%rbx), %rdx
	movq	8(%rbx), %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movslq	(%rbx), %rax
	movb	$0, (%r12,%rax)
.L1:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$35, %r8d
	movl	$65, %edx
	movl	$149, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1
	.cfi_endproc
.LFE1296:
	.size	i2s_ASN1_IA5STRING, .-i2s_ASN1_IA5STRING
	.p2align 4
	.globl	s2i_ASN1_IA5STRING
	.type	s2i_ASN1_IA5STRING, @function
s2i_ASN1_IA5STRING:
.LFB1297:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	testq	%rdx, %rdx
	je	.L15
	movq	%rdx, %r13
	call	ASN1_IA5STRING_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L16
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	ASN1_STRING_set@PLT
	testl	%eax, %eax
	je	.L17
.L10:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	movl	$48, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$107, %edx
	xorl	%r12d, %r12d
	movl	$100, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L17:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	ASN1_IA5STRING_free@PLT
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L16:
	movl	$63, %r8d
	movl	$65, %edx
	movl	$100, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L10
	.cfi_endproc
.LFE1297:
	.size	s2i_ASN1_IA5STRING, .-s2i_ASN1_IA5STRING
	.globl	v3_ns_ia5_list
	.section	.data.rel.ro,"aw"
	.align 32
	.type	v3_ns_ia5_list, @object
	.size	v3_ns_ia5_list, 832
v3_ns_ia5_list:
	.long	72
	.long	0
	.quad	ASN1_IA5STRING_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	i2s_ASN1_IA5STRING
	.quad	s2i_ASN1_IA5STRING
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.long	73
	.long	0
	.quad	ASN1_IA5STRING_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	i2s_ASN1_IA5STRING
	.quad	s2i_ASN1_IA5STRING
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.long	74
	.long	0
	.quad	ASN1_IA5STRING_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	i2s_ASN1_IA5STRING
	.quad	s2i_ASN1_IA5STRING
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.long	75
	.long	0
	.quad	ASN1_IA5STRING_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	i2s_ASN1_IA5STRING
	.quad	s2i_ASN1_IA5STRING
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.long	76
	.long	0
	.quad	ASN1_IA5STRING_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	i2s_ASN1_IA5STRING
	.quad	s2i_ASN1_IA5STRING
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.long	77
	.long	0
	.quad	ASN1_IA5STRING_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	i2s_ASN1_IA5STRING
	.quad	s2i_ASN1_IA5STRING
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.long	78
	.long	0
	.quad	ASN1_IA5STRING_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	i2s_ASN1_IA5STRING
	.quad	s2i_ASN1_IA5STRING
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.long	-1
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
