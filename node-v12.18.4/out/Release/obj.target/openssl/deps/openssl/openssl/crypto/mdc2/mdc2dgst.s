	.file	"mdc2dgst.c"
	.text
	.p2align 4
	.type	mdc2_body, @function
mdc2_body:
.LFB153:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -232(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	testq	%rdx, %rdx
	je	.L1
	leaq	-208(%rbp), %rax
	movq	%rdi, %r15
	movq	%rsi, %r10
	xorl	%r14d, %r14d
	movq	%rax, -240(%rbp)
	movzbl	12(%rdi), %edx
	leaq	12(%r15), %r13
	leaq	20(%r15), %r12
	leaq	-200(%rbp), %rax
	movzbl	20(%rdi), %edi
	leaq	-192(%rbp), %rbx
	movq	%rax, -248(%rbp)
	.p2align 4,,10
	.p2align 3
.L3:
	andl	$-97, %edx
	andl	$-97, %edi
	movl	(%r10,%r14), %eax
	movl	4(%r10,%r14), %ecx
	orl	$64, %edx
	orl	$32, %edi
	movq	%r10, -224(%rbp)
	addq	$8, %r14
	movb	%dl, 12(%r15)
	movb	%dil, 20(%r15)
	movq	%r13, %rdi
	movl	%eax, -200(%rbp)
	movl	%eax, -208(%rbp)
	movl	%eax, -216(%rbp)
	movl	%ecx, -196(%rbp)
	movl	%ecx, -204(%rbp)
	movl	%ecx, -212(%rbp)
	call	DES_set_odd_parity@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	DES_set_key_unchecked@PLT
	movq	-240(%rbp), %rdi
	movl	$1, %edx
	movq	%rbx, %rsi
	call	DES_encrypt1@PLT
	movq	%r12, %rdi
	call	DES_set_odd_parity@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	DES_set_key_unchecked@PLT
	movq	-248(%rbp), %rdi
	movl	$1, %edx
	movq	%rbx, %rsi
	call	DES_encrypt1@PLT
	movl	-216(%rbp), %eax
	movl	-212(%rbp), %ecx
	movl	-200(%rbp), %esi
	movl	-196(%rbp), %edi
	movq	-224(%rbp), %r10
	xorl	%eax, %esi
	xorl	%ecx, %edi
	xorl	-208(%rbp), %eax
	xorl	-204(%rbp), %ecx
	cmpq	%r14, -232(%rbp)
	movl	%edi, 16(%r15)
	movl	%eax, %edx
	movl	%esi, %edi
	movl	%eax, 12(%r15)
	movl	%esi, 20(%r15)
	movl	%ecx, 24(%r15)
	ja	.L3
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L9
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L9:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE153:
	.size	mdc2_body, .-mdc2_body
	.p2align 4
	.globl	MDC2_Init
	.type	MDC2_Init, @function
MDC2_Init:
.LFB151:
	.cfi_startproc
	endbr64
	movabsq	$5931894172722287186, %rax
	movl	$0, (%rdi)
	movq	%rax, 12(%rdi)
	movabsq	$2676586395008836901, %rax
	movq	%rax, 20(%rdi)
	movl	$1, %eax
	movl	$1, 28(%rdi)
	ret
	.cfi_endproc
.LFE151:
	.size	MDC2_Init, .-MDC2_Init
	.p2align 4
	.globl	MDC2_Update
	.type	MDC2_Update, @function
MDC2_Update:
.LFB152:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	(%rdi), %ecx
	movq	%rdx, %rbx
	testq	%rcx, %rcx
	je	.L12
	movl	$8, %edx
	leaq	4(%rdi,%rcx), %rdi
	subq	%rcx, %rdx
	cmpq	%rbx, %rdx
	ja	.L32
	testq	%rdx, %rdx
	je	.L16
	xorl	%eax, %eax
.L15:
	movzbl	(%r14,%rax), %esi
	movb	%sil, (%rdi,%rax)
	addq	$1, %rax
	cmpq	%rdx, %rax
	jb	.L15
.L16:
	addq	%rdx, %r14
	leaq	4(%r13), %rsi
	movl	$8, %edx
	movq	%r13, %rdi
	movl	$0, 0(%r13)
	leaq	-8(%rcx,%rbx), %rbx
	call	mdc2_body
.L12:
	movq	%rbx, %r12
	andq	$-8, %r12
	jne	.L33
.L17:
	subq	%r12, %rbx
	jne	.L34
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	leaq	(%r14,%r12), %rsi
	leaq	4(%r13), %rdi
	movq	%rbx, %rdx
	call	memcpy@PLT
	movl	%ebx, 0(%r13)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	mdc2_body
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L32:
	movq	%rbx, %rdx
	call	memcpy@PLT
	addl	%ebx, 0(%r13)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE152:
	.size	MDC2_Update, .-MDC2_Update
	.p2align 4
	.globl	MDC2_Final
	.type	MDC2_Final, @function
MDC2_Final:
.LFB154:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	(%rsi), %eax
	movq	%rsi, %rbx
	movl	28(%rsi), %edx
	testl	%eax, %eax
	jne	.L39
	cmpl	$2, %edx
	je	.L43
.L36:
	movq	12(%rbx), %rax
	movq	%rax, (%r12)
	movq	20(%rbx), %rax
	popq	%rbx
	movq	%rax, 8(%r12)
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	cmpl	$2, %edx
	je	.L43
.L38:
	movl	$8, %edx
	leaq	4(%rbx,%rax), %rdi
	xorl	%esi, %esi
	subl	%eax, %edx
	call	memset@PLT
	leaq	4(%rbx), %rsi
	movl	$8, %edx
	movq	%rbx, %rdi
	call	mdc2_body
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L43:
	movl	%eax, %edx
	addl	$1, %eax
	movb	$-128, 4(%rbx,%rdx)
	jmp	.L38
	.cfi_endproc
.LFE154:
	.size	MDC2_Final, .-MDC2_Final
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
