	.file	"dso_err.c"
	.text
	.p2align 4
	.globl	ERR_load_DSO_strings
	.type	ERR_load_DSO_strings, @function
ERR_load_DSO_strings:
.LFB203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$621166592, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_func_error_string@PLT
	testq	%rax, %rax
	je	.L5
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	leaq	DSO_str_functs(%rip), %rdi
	call	ERR_load_strings_const@PLT
	leaq	DSO_str_reasons(%rip), %rdi
	call	ERR_load_strings_const@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE203:
	.size	ERR_load_DSO_strings, .-ERR_load_DSO_strings
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"control command failed"
.LC1:
	.string	"dso already loaded"
.LC2:
	.string	"empty file structure"
.LC3:
	.string	"failure"
.LC4:
	.string	"filename too big"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"cleanup method function failed"
	.section	.rodata.str1.1
.LC6:
	.string	"incorrect file syntax"
	.section	.rodata.str1.8
	.align 8
.LC7:
	.string	"could not load the shared library"
	.section	.rodata.str1.1
.LC8:
	.string	"name translation failed"
.LC9:
	.string	"no filename"
	.section	.rodata.str1.8
	.align 8
.LC10:
	.string	"a null shared library handle was used"
	.section	.rodata.str1.1
.LC11:
	.string	"set filename failed"
	.section	.rodata.str1.8
	.align 8
.LC12:
	.string	"the meth_data stack is corrupt"
	.align 8
.LC13:
	.string	"could not bind to the requested symbol name"
	.align 8
.LC14:
	.string	"could not unload the shared library"
	.section	.rodata.str1.1
.LC15:
	.string	"functionality not supported"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	DSO_str_reasons, @object
	.size	DSO_str_reasons, 272
DSO_str_reasons:
	.quad	620757092
	.quad	.LC0
	.quad	620757102
	.quad	.LC1
	.quad	620757105
	.quad	.LC2
	.quad	620757106
	.quad	.LC3
	.quad	620757093
	.quad	.LC4
	.quad	620757094
	.quad	.LC5
	.quad	620757107
	.quad	.LC6
	.quad	620757095
	.quad	.LC7
	.quad	620757101
	.quad	.LC8
	.quad	620757103
	.quad	.LC9
	.quad	620757096
	.quad	.LC10
	.quad	620757104
	.quad	.LC11
	.quad	620757097
	.quad	.LC12
	.quad	620757098
	.quad	.LC13
	.quad	620757099
	.quad	.LC14
	.quad	620757100
	.quad	.LC15
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC16:
	.string	"dlfcn_bind_func"
.LC17:
	.string	"dlfcn_load"
.LC18:
	.string	"dlfcn_merger"
.LC19:
	.string	"dlfcn_name_converter"
.LC20:
	.string	"dlfcn_unload"
.LC21:
	.string	"dl_bind_func"
.LC22:
	.string	"dl_load"
.LC23:
	.string	"dl_merger"
.LC24:
	.string	"dl_name_converter"
.LC25:
	.string	"dl_unload"
.LC26:
	.string	"DSO_bind_func"
.LC27:
	.string	"DSO_convert_filename"
.LC28:
	.string	"DSO_ctrl"
.LC29:
	.string	"DSO_free"
.LC30:
	.string	"DSO_get_filename"
.LC31:
	.string	"DSO_global_lookup"
.LC32:
	.string	"DSO_load"
.LC33:
	.string	"DSO_merge"
.LC34:
	.string	"DSO_new_method"
.LC35:
	.string	"DSO_pathbyaddr"
.LC36:
	.string	"DSO_set_filename"
.LC37:
	.string	"DSO_up_ref"
.LC38:
	.string	"vms_bind_sym"
.LC39:
	.string	"vms_load"
.LC40:
	.string	"vms_merger"
.LC41:
	.string	"vms_unload"
.LC42:
	.string	"win32_bind_func"
.LC43:
	.string	"win32_globallookup"
.LC44:
	.string	"win32_joiner"
.LC45:
	.string	"win32_load"
.LC46:
	.string	"win32_merger"
.LC47:
	.string	"win32_name_converter"
.LC48:
	.string	""
.LC49:
	.string	"win32_splitter"
.LC50:
	.string	"win32_unload"
	.section	.data.rel.ro.local
	.align 32
	.type	DSO_str_functs, @object
	.size	DSO_str_functs, 576
DSO_str_functs:
	.quad	621166592
	.quad	.LC16
	.quad	621174784
	.quad	.LC17
	.quad	621289472
	.quad	.LC18
	.quad	621260800
	.quad	.LC19
	.quad	621178880
	.quad	.LC20
	.quad	621182976
	.quad	.LC21
	.quad	621191168
	.quad	.LC22
	.quad	621293568
	.quad	.LC23
	.quad	621264896
	.quad	.LC24
	.quad	621195264
	.quad	.LC25
	.quad	621199360
	.quad	.LC26
	.quad	621273088
	.quad	.LC27
	.quad	621207552
	.quad	.LC28
	.quad	621211648
	.quad	.LC29
	.quad	621277184
	.quad	.LC30
	.quad	621326336
	.quad	.LC31
	.quad	621215744
	.quad	.LC32
	.quad	621297664
	.quad	.LC33
	.quad	621219840
	.quad	.LC34
	.quad	621187072
	.quad	.LC35
	.quad	621285376
	.quad	.LC36
	.quad	621223936
	.quad	.LC37
	.quad	621228032
	.quad	.LC38
	.quad	621232128
	.quad	.LC39
	.quad	621301760
	.quad	.LC40
	.quad	621236224
	.quad	.LC41
	.quad	621170688
	.quad	.LC42
	.quad	621338624
	.quad	.LC43
	.quad	621309952
	.quad	.LC44
	.quad	621248512
	.quad	.LC45
	.quad	621305856
	.quad	.LC46
	.quad	621268992
	.quad	.LC47
	.quad	621203456
	.quad	.LC48
	.quad	621314048
	.quad	.LC49
	.quad	621252608
	.quad	.LC50
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
