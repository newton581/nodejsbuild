	.file	"a_type.c"
	.text
	.p2align 4
	.globl	ASN1_TYPE_get
	.type	ASN1_TYPE_get, @function
ASN1_TYPE_get:
.LFB491:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	movl	%eax, %edx
	andl	$-5, %edx
	cmpl	$1, %edx
	je	.L1
	cmpq	$0, 8(%rdi)
	movl	$0, %edx
	cmove	%edx, %eax
.L1:
	ret
	.cfi_endproc
.LFE491:
	.size	ASN1_TYPE_get, .-ASN1_TYPE_get
	.p2align 4
	.globl	ASN1_TYPE_set
	.type	ASN1_TYPE_set, @function
ASN1_TYPE_set:
.LFB492:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	subq	$16, %rsp
	movl	(%rdi), %edx
	movq	%rdi, -24(%rbp)
	andl	$-5, %edx
	cmpl	$1, %edx
	je	.L8
	cmpq	$0, 8(%rdi)
	je	.L8
	leaq	-24(%rbp), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	asn1_primitive_free@PLT
	movq	-24(%rbp), %rax
.L8:
	movl	%ebx, (%rax)
	cmpl	$1, %ebx
	je	.L17
	movq	%r12, 8(%rax)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	cmpq	$1, %r12
	sbbl	%edx, %edx
	notl	%edx
	movzbl	%dl, %edx
	movl	%edx, 8(%rax)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE492:
	.size	ASN1_TYPE_set, .-ASN1_TYPE_set
	.p2align 4
	.globl	ASN1_TYPE_set1
	.type	ASN1_TYPE_set1, @function
ASN1_TYPE_set1:
.LFB493:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L32
	cmpl	$1, %esi
	je	.L32
	movq	%rdx, %rdi
	cmpl	$6, %esi
	je	.L51
	call	ASN1_STRING_dup@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L27
	movl	(%rbx), %eax
	movq	%rbx, -48(%rbp)
	andl	$-5, %eax
	cmpl	$1, %eax
	jne	.L52
.L29:
	movl	%r12d, (%rbx)
.L50:
	movq	%r13, 8(%rbx)
.L49:
	movl	$1, %eax
.L18:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L53
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	call	OBJ_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L27
	movl	(%rbx), %eax
	movq	%rbx, -48(%rbp)
	andl	$-5, %eax
	cmpl	$1, %eax
	je	.L28
	cmpq	$0, 8(%rbx)
	je	.L28
	leaq	-48(%rbp), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	asn1_primitive_free@PLT
	movq	-48(%rbp), %rbx
.L28:
	movl	$6, (%rbx)
	movl	$1, %eax
	movq	%r12, 8(%rbx)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L27:
	xorl	%eax, %eax
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L32:
	movl	(%rbx), %eax
	movq	%rbx, -48(%rbp)
	andl	$-5, %eax
	cmpl	$1, %eax
	je	.L21
	cmpq	$0, 8(%rbx)
	je	.L21
	leaq	-48(%rbp), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	asn1_primitive_free@PLT
	movq	-48(%rbp), %rbx
.L21:
	movl	%r12d, (%rbx)
	cmpl	$1, %r12d
	jne	.L50
	cmpq	$1, %r13
	sbbl	%eax, %eax
	notl	%eax
	movzbl	%al, %eax
	movl	%eax, 8(%rbx)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L52:
	cmpq	$0, 8(%rbx)
	je	.L29
	leaq	-48(%rbp), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	asn1_primitive_free@PLT
	movq	-48(%rbp), %rbx
	jmp	.L29
.L53:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE493:
	.size	ASN1_TYPE_set1, .-ASN1_TYPE_set1
	.p2align 4
	.globl	ASN1_TYPE_cmp
	.type	ASN1_TYPE_cmp, @function
ASN1_TYPE_cmp:
.LFB494:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L60
	testq	%rsi, %rsi
	je	.L60
	movl	(%rdi), %eax
	cmpl	(%rsi), %eax
	jne	.L60
	cmpl	$5, %eax
	je	.L61
	cmpl	$6, %eax
	je	.L56
	cmpl	$1, %eax
	je	.L63
	movq	8(%rsi), %rsi
	movq	8(%rdi), %rdi
	jmp	ASN1_STRING_cmp@PLT
	.p2align 4,,10
	.p2align 3
.L61:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	movl	8(%rdi), %eax
	subl	8(%rsi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	movq	8(%rsi), %rsi
	movq	8(%rdi), %rdi
	jmp	OBJ_cmp@PLT
	.p2align 4,,10
	.p2align 3
.L60:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE494:
	.size	ASN1_TYPE_cmp, .-ASN1_TYPE_cmp
	.p2align 4
	.globl	ASN1_TYPE_pack_sequence
	.type	ASN1_TYPE_pack_sequence, @function
ASN1_TYPE_pack_sequence:
.LFB495:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	%r8, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	xorl	%edx, %edx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	ASN1_item_pack@PLT
	testq	%rax, %rax
	je	.L73
	movq	%rax, %r12
	testq	%rbx, %rbx
	je	.L66
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L86
.L67:
	movl	(%rax), %edx
	movq	%rax, -32(%rbp)
	movq	%rax, %rcx
	andl	$-5, %edx
	cmpl	$1, %edx
	jne	.L87
.L69:
	movl	$16, (%rcx)
	movq	%r12, 8(%rcx)
.L64:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L88
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	cmpq	$0, 8(%rax)
	je	.L69
	leaq	-32(%rbp), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, -40(%rbp)
	call	asn1_primitive_free@PLT
	movq	-32(%rbp), %rcx
	movq	-40(%rbp), %rax
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L86:
	call	ASN1_TYPE_new@PLT
	testq	%rax, %rax
	je	.L70
	movq	%rax, (%rbx)
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L66:
	call	ASN1_TYPE_new@PLT
	testq	%rax, %rax
	jne	.L67
.L70:
	movq	%r12, %rdi
	call	ASN1_OCTET_STRING_free@PLT
	xorl	%eax, %eax
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L73:
	xorl	%eax, %eax
	jmp	.L64
.L88:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE495:
	.size	ASN1_TYPE_pack_sequence, .-ASN1_TYPE_pack_sequence
	.p2align 4
	.globl	ASN1_TYPE_unpack_sequence
	.type	ASN1_TYPE_unpack_sequence, @function
ASN1_TYPE_unpack_sequence:
.LFB496:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L89
	cmpl	$16, (%rsi)
	jne	.L89
	movq	8(%rsi), %r8
	testq	%r8, %r8
	je	.L89
	movq	%rdi, %rsi
	movq	%r8, %rdi
	jmp	ASN1_item_unpack@PLT
	.p2align 4,,10
	.p2align 3
.L89:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE496:
	.size	ASN1_TYPE_unpack_sequence, .-ASN1_TYPE_unpack_sequence
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
