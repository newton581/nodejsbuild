	.file	"rsa_pk1.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/rsa/rsa_pk1.c"
	.text
	.p2align 4
	.globl	RSA_padding_add_PKCS1_type_1
	.type	RSA_padding_add_PKCS1_type_1, @function
RSA_padding_add_PKCS1_type_1:
.LFB455:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	-10(%rsi), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	cmpl	%ecx, %eax
	jle	.L6
	movl	%ecx, %r12d
	movl	$256, %eax
	subl	$3, %esi
	movq	%rdx, %r13
	subl	%r12d, %esi
	movw	%ax, (%rdi)
	leaq	2(%rdi), %rcx
	movslq	%esi, %rbx
	movq	%rcx, %rdi
	movl	$255, %esi
	movq	%rbx, %rdx
	call	memset@PLT
	movl	%r12d, %edx
	movq	%r13, %rsi
	addq	%rax, %rbx
	movb	$0, (%rbx)
	leaq	1(%rbx), %rdi
	call	memcpy@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movl	$25, %r8d
	movl	$110, %edx
	movl	$108, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE455:
	.size	RSA_padding_add_PKCS1_type_1, .-RSA_padding_add_PKCS1_type_1
	.p2align 4
	.globl	RSA_padding_check_PKCS1_type_1
	.type	RSA_padding_check_PKCS1_type_1, @function
RSA_padding_check_PKCS1_type_1:
.LFB456:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	cmpl	$10, %r8d
	jle	.L24
	movl	%esi, %r9d
	cmpl	%ecx, %r8d
	je	.L9
	leal	1(%rcx), %eax
	cmpl	%r8d, %eax
	jne	.L10
.L11:
	cmpb	$1, (%rdx)
	leaq	1(%rdx), %rsi
	jne	.L10
	subl	$1, %ecx
	testl	%ecx, %ecx
	jle	.L14
	movl	%ecx, %r8d
	xorl	%eax, %eax
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L15:
	addl	$1, %eax
	addq	$1, %rsi
	cmpl	%ecx, %eax
	je	.L17
.L18:
	movzbl	(%rsi), %edx
	cmpb	$-1, %dl
	je	.L15
	testb	%dl, %dl
	jne	.L16
	addq	$1, %rsi
	movl	%eax, %r8d
.L17:
	cmpl	%ecx, %r8d
	je	.L19
	cmpl	$7, %r8d
	jle	.L20
	addl	$1, %r8d
	subl	%r8d, %ecx
	movl	%ecx, %r12d
	cmpl	%r9d, %ecx
	jg	.L30
	movl	%ecx, %edx
	call	memcpy@PLT
.L7:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	cmpb	$0, (%rdx)
	leaq	1(%rdx), %rax
	jne	.L31
	subl	$1, %ecx
	movq	%rax, %rdx
	jmp	.L11
.L14:
	je	.L19
.L20:
	movl	$102, %r8d
	movl	$103, %edx
	movl	$112, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L7
.L10:
	movl	$74, %r8d
	movl	$106, %edx
	movl	$112, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L7
.L24:
	movl	$-1, %r12d
	jmp	.L7
.L30:
	movl	$109, %r8d
	movl	$109, %edx
	movl	$112, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L7
.L19:
	movl	$96, %r8d
	movl	$113, %edx
	movl	$112, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L7
.L16:
	movl	$87, %r8d
	movl	$102, %edx
	movl	$112, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L7
.L31:
	movl	$66, %r8d
	movl	$138, %edx
	movl	$112, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L7
	.cfi_endproc
.LFE456:
	.size	RSA_padding_check_PKCS1_type_1, .-RSA_padding_check_PKCS1_type_1
	.p2align 4
	.globl	RSA_padding_add_PKCS1_type_2
	.type	RSA_padding_add_PKCS1_type_2, @function
RSA_padding_add_PKCS1_type_2:
.LFB457:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	-10(%rsi), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpl	%ecx, %eax
	jle	.L46
	movl	$512, %eax
	leal	-3(%rsi), %r14d
	leaq	2(%rdi), %rbx
	movq	%rdi, %r15
	movw	%ax, (%rdi)
	subl	%ecx, %r14d
	movq	%rbx, %rdi
	movq	%rdx, %r13
	movl	%r14d, %esi
	movl	%ecx, %r12d
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jle	.L38
	testl	%r14d, %r14d
	jle	.L41
	leal	-1(%r14), %eax
	leaq	3(%r15,%rax), %r14
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L47:
	addq	$1, %rbx
	cmpq	%rbx, %r14
	je	.L36
	.p2align 4,,10
	.p2align 3
.L40:
	cmpb	$0, (%rbx)
	je	.L39
	addq	$1, %rbx
	cmpq	%rbx, %r14
	jne	.L40
.L36:
	movb	$0, (%r14)
	leaq	1(%r14), %rdi
	movl	%r12d, %edx
	movq	%r13, %rsi
	call	memcpy@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	cmpb	$0, (%rbx)
	jne	.L47
.L39:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jg	.L48
.L38:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	movl	$124, %r8d
	movl	$110, %edx
	movl	$109, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L41:
	.cfi_restore_state
	movq	%rbx, %r14
	jmp	.L36
	.cfi_endproc
.LFE457:
	.size	RSA_padding_add_PKCS1_type_2, .-RSA_padding_add_PKCS1_type_2
	.p2align 4
	.globl	RSA_padding_check_PKCS1_type_2
	.type	RSA_padding_check_PKCS1_type_2, @function
RSA_padding_check_PKCS1_type_2:
.LFB458:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	jle	.L75
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testl	%ecx, %ecx
	jle	.L65
	movl	%r8d, %r14d
	cmpl	%r8d, %ecx
	jg	.L66
	cmpl	$10, %r8d
	jle	.L66
	movslq	%r8d, %rax
	movl	%esi, -60(%rbp)
	movq	%rdi, %r12
	movq	%rdx, %rbx
	leaq	.LC0(%rip), %rsi
	movl	$178, %edx
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	CRYPTO_malloc@PLT
	movl	-60(%rbp), %r9d
	testq	%rax, %rax
	je	.L79
	movq	-56(%rbp), %rdi
	movslq	%r13d, %rdx
	addq	%rdx, %rbx
	leaq	(%rax,%rdi), %rcx
	movq	%rcx, %rdx
	.p2align 4,,10
	.p2align 3
.L54:
	movl	%r13d, %esi
	leal	-1(%r13), %eax
	subq	$1, %rdx
	notl	%esi
	andl	%eax, %esi
	sarl	$31, %esi
	movl	%esi, %eax
	addl	$1, %esi
	subq	%rsi, %rbx
	subl	%esi, %r13d
	movl	%ecx, %esi
	notl	%eax
	subl	%edx, %esi
	andb	(%rbx), %al
	movb	%al, (%rdx)
	cmpl	%esi, %r14d
	jg	.L54
	leal	-1(%r14), %r8d
	testl	%r14d, %r14d
	movzbl	%al, %eax
	movl	$2, %edi
	movq	$-1, %rdx
	notq	%r8
	cmovle	%rdx, %r8
	xorl	%r11d, %r11d
	addq	%rcx, %r8
	xorl	%ecx, %ecx
	movzbl	1(%r8), %edx
	xorl	$2, %edx
	movzbl	%dl, %edx
	leal	-1(%rdx), %r10d
	notl	%edx
	andl	%r10d, %edx
	movl	%eax, %r10d
	subl	$1, %eax
	notl	%r10d
	andl	%r10d, %eax
	andl	%edx, %eax
	movl	%eax, %r10d
	sarl	$31, %r10d
	.p2align 4,,10
	.p2align 3
.L55:
	movzbl	(%r8,%rdi), %eax
	movl	%eax, %edx
	subl	$1, %eax
	notl	%edx
	andl	%edx, %eax
	cltd
	movl	%r11d, %eax
	notl	%eax
	orl	%edx, %r11d
	andl	%edx, %eax
	movl	%eax, %esi
	notl	%eax
	andl	%edi, %esi
	addq	$1, %rdi
	andl	%ecx, %eax
	movl	%esi, %ecx
	orl	%eax, %ecx
	cmpl	%edi, %r14d
	jg	.L55
	leal	-10(%rcx), %eax
	movl	%r14d, %r13d
	leal	-11(%r14), %r15d
	movl	$1, %r11d
	movl	%eax, %ebx
	movl	%eax, %edi
	movl	%ecx, %eax
	movl	%r15d, %esi
	xorl	$10, %eax
	xorl	$10, %ebx
	subl	%r9d, %esi
	orl	%eax, %ebx
	xorl	%r9d, %esi
	xorl	%ecx, %ebx
	addl	$1, %ecx
	sarl	$31, %ebx
	subl	%ecx, %r13d
	notl	%ebx
	movl	%r13d, %eax
	andl	%ebx, %r10d
	movl	%r9d, %ebx
	xorl	%r9d, %eax
	subl	%r13d, %ebx
	xorl	%r13d, %ebx
	orl	%eax, %ebx
	movl	%r9d, %eax
	xorl	%r15d, %eax
	xorl	%r9d, %ebx
	orl	%eax, %esi
	sarl	$31, %ebx
	xorl	%r15d, %esi
	notl	%ebx
	sarl	$31, %esi
	andl	%r10d, %ebx
	movl	%esi, %eax
	notl	%esi
	andl	%r15d, %eax
	andl	%esi, %r9d
	orl	%r9d, %eax
	movl	%eax, -60(%rbp)
	leaq	12(%r8), %rax
	cmpl	$1, %r15d
	jle	.L61
	movl	%r13d, -64(%rbp)
	movq	%rax, %r13
	movl	%ebx, -68(%rbp)
	movl	%edi, %ebx
	.p2align 4,,10
	.p2align 3
.L60:
	movl	%r11d, %eax
	movl	%r14d, %edx
	andl	%ebx, %eax
	subl	%r11d, %edx
	movl	%eax, %esi
	subl	$1, %eax
	notl	%esi
	andl	%eax, %esi
	sarl	$31, %esi
	movl	%esi, %edi
	notl	%edi
	cmpl	$11, %edx
	jle	.L63
	leal	-12(%rdx), %r9d
	leaq	11(%r8), %rax
	movslq	%r11d, %r10
	movzbl	%dil, %edi
	addq	%r13, %r9
	orl	$-256, %esi
	.p2align 4,,10
	.p2align 3
.L62:
	movzbl	(%rax,%r10), %edx
	movzbl	(%rax), %ecx
	addq	$1, %rax
	andl	%edi, %edx
	andl	%esi, %ecx
	orl	%ecx, %edx
	movb	%dl, -1(%rax)
	cmpq	%rax, %r9
	jne	.L62
.L63:
	addl	%r11d, %r11d
	cmpl	%r11d, %r15d
	jg	.L60
	movl	-64(%rbp), %r13d
	movl	-68(%rbp), %ebx
.L61:
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	je	.L58
	movl	-60(%rbp), %eax
	movl	%r13d, %ecx
	xorl	%edx, %edx
	negl	%ecx
	leal	-1(%rax), %esi
	.p2align 4,,10
	.p2align 3
.L64:
	movl	%r13d, %eax
	movl	%r13d, %edi
	xorl	%ecx, %edi
	xorl	%edx, %eax
	addl	$1, %ecx
	orl	%edi, %eax
	movzbl	11(%r8,%rdx), %edi
	xorl	%edx, %eax
	sarl	$31, %eax
	andl	%ebx, %eax
	movzbl	%al, %eax
	movl	%eax, %r9d
	notl	%eax
	andl	%r9d, %edi
	movzbl	(%r12,%rdx), %r9d
	andl	%r9d, %eax
	orl	%edi, %eax
	movb	%al, (%r12,%rdx)
	movq	%rdx, %rax
	addq	$1, %rdx
	cmpq	%rsi, %rax
	jne	.L64
.L58:
	movq	-56(%rbp), %rsi
	movq	%r8, %rdi
	movl	$250, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movl	$251, %r8d
	movl	$159, %edx
	leaq	.LC0(%rip), %rcx
	movl	$113, %esi
	movl	$4, %edi
	call	ERR_put_error@PLT
	movl	%ebx, %edi
	andl	$1, %edi
	call	err_clear_last_constant_time@PLT
	movl	%ebx, %eax
	notl	%ebx
	andl	%r13d, %eax
	orl	%ebx, %eax
.L49:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L65:
	.cfi_restore_state
	movl	$-1, %eax
	jmp	.L49
.L66:
	movl	$173, %r8d
	movl	$159, %edx
	movl	$113, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L49
.L75:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	orl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$180, %r8d
	movl	$65, %edx
	movl	$113, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L49
	.cfi_endproc
.LFE458:
	.size	RSA_padding_check_PKCS1_type_2, .-RSA_padding_check_PKCS1_type_2
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
