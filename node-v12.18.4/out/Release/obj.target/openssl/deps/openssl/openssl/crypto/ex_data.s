	.file	"ex_data.c"
	.text
	.p2align 4
	.type	dummy_new, @function
dummy_new:
.LFB256:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE256:
	.size	dummy_new, .-dummy_new
	.p2align 4
	.type	dummy_dup, @function
dummy_dup:
.LFB258:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE258:
	.size	dummy_dup, .-dummy_dup
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ex_data.c"
	.text
	.p2align 4
	.type	cleanup_cb, @function
cleanup_cb:
.LFB254:
	.cfi_startproc
	endbr64
	movl	$84, %edx
	leaq	.LC0(%rip), %rsi
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE254:
	.size	cleanup_cb, .-cleanup_cb
	.p2align 4
	.type	do_ex_data_init_ossl_, @function
do_ex_data_init_ossl_:
.LFB251:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	OPENSSL_init_crypto@PLT
	testl	%eax, %eax
	je	.L6
	call	CRYPTO_THREAD_lock_new@PLT
	testq	%rax, %rax
	movq	%rax, ex_data_lock(%rip)
	setne	%al
	movzbl	%al, %eax
.L6:
	movl	%eax, do_ex_data_init_ossl_ret_(%rip)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE251:
	.size	do_ex_data_init_ossl_, .-do_ex_data_init_ossl_
	.p2align 4
	.type	dummy_free, @function
dummy_free:
.LFB270:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE270:
	.size	dummy_free, .-dummy_free
	.p2align 4
	.globl	crypto_cleanup_all_ex_data_int
	.type	crypto_cleanup_all_ex_data_int, @function
crypto_cleanup_all_ex_data_int:
.LFB255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	cleanup_cb(%rip), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	ex_data(%rip), %rbx
	leaq	128(%rbx), %r13
	subq	$8, %rsp
	.p2align 4,,10
	.p2align 3
.L13:
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	addq	$8, %rbx
	call	OPENSSL_sk_pop_free@PLT
	movq	$0, -8(%rbx)
	cmpq	%r13, %rbx
	jne	.L13
	movq	ex_data_lock(%rip), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	movq	$0, ex_data_lock(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE255:
	.size	crypto_cleanup_all_ex_data_int, .-crypto_cleanup_all_ex_data_int
	.p2align 4
	.globl	CRYPTO_free_ex_index
	.type	CRYPTO_free_ex_index, @function
CRYPTO_free_ex_index:
.LFB259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movslq	%edi, %rbx
	subq	$8, %rsp
	cmpl	$15, %ebx
	ja	.L32
	movl	%esi, %r12d
	leaq	ex_data_init(%rip), %rdi
	leaq	do_ex_data_init_ossl_(%rip), %rsi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L19
	movl	do_ex_data_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	je	.L19
	movq	ex_data_lock(%rip), %rdi
	testq	%rdi, %rdi
	je	.L24
	call	CRYPTO_THREAD_write_lock@PLT
	testl	%r12d, %r12d
	js	.L23
	leaq	ex_data(%rip), %r13
	movq	0(%r13,%rbx,8), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L23
	movq	0(%r13,%rbx,8), %rdi
	movl	%r12d, %esi
	call	OPENSSL_sk_value@PLT
	testq	%rax, %rax
	je	.L23
	leaq	dummy_dup(%rip), %rcx
	leaq	dummy_free(%rip), %rdx
	movl	$1, %r12d
	movq	%rcx, 32(%rax)
	leaq	dummy_new(%rip), %rcx
	movq	%rdx, %xmm1
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 16(%rax)
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L23:
	xorl	%r12d, %r12d
.L22:
	movq	ex_data_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
.L16:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movl	$60, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$113, %esi
	movl	$15, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movl	$55, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$7, %edx
	xorl	%r12d, %r12d
	movl	$113, %esi
	movl	$15, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L16
	.cfi_endproc
.LFE259:
	.size	CRYPTO_free_ex_index, .-CRYPTO_free_ex_index
	.p2align 4
	.globl	CRYPTO_get_ex_new_index
	.type	CRYPTO_get_ex_new_index, @function
CRYPTO_get_ex_new_index:
.LFB260:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movslq	%edi, %rbx
	subq	$40, %rsp
	movq	%rdx, -56(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%r9, -72(%rbp)
	cmpl	$15, %ebx
	ja	.L53
	movq	%rsi, %r15
	leaq	ex_data_init(%rip), %rdi
	leaq	do_ex_data_init_ossl_(%rip), %rsi
	movq	%r8, %r13
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L36
	movl	do_ex_data_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	je	.L36
	movq	ex_data_lock(%rip), %rdi
	testq	%rdi, %rdi
	je	.L44
	call	CRYPTO_THREAD_write_lock@PLT
	leaq	ex_data(%rip), %r12
	cmpq	$0, (%r12,%rbx,8)
	je	.L54
.L38:
	movl	$177, %edx
	leaq	.LC0(%rip), %rsi
	movl	$40, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L55
	movq	-64(%rbp), %xmm0
	movq	(%r12,%rbx,8), %rdi
	movq	%r15, (%rax)
	xorl	%esi, %esi
	movq	-56(%rbp), %rax
	movq	%r13, 32(%r14)
	movhps	-72(%rbp), %xmm0
	movq	%rax, 8(%r14)
	movups	%xmm0, 16(%r14)
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L56
	movq	(%r12,%rbx,8), %rdi
	call	OPENSSL_sk_num@PLT
	movq	(%r12,%rbx,8), %rdi
	movq	%r14, %rdx
	leal	-1(%rax), %r13d
	movl	%r13d, %esi
	call	OPENSSL_sk_set@PLT
.L40:
	movq	ex_data_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
.L33:
	addq	$40, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	movl	$189, %r8d
	movl	$65, %edx
	movl	$100, %esi
	movl	$15, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r13d
	call	ERR_put_error@PLT
	movl	$190, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L54:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, (%r12,%rbx,8)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L41
	xorl	%esi, %esi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L38
.L41:
	movl	$172, %r8d
.L52:
	movl	$65, %edx
	movl	$100, %esi
	movl	$15, %edi
	movl	$-1, %r13d
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$60, %r8d
	movl	$65, %edx
	movl	$113, %esi
	movl	$15, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r13d
	call	ERR_put_error@PLT
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L53:
	movl	$55, %r8d
	movl	$7, %edx
	movl	$113, %esi
	movl	$15, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r13d
	call	ERR_put_error@PLT
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L55:
	movl	$179, %r8d
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L44:
	movl	$-1, %r13d
	jmp	.L33
	.cfi_endproc
.LFE260:
	.size	CRYPTO_get_ex_new_index, .-CRYPTO_get_ex_new_index
	.p2align 4
	.globl	CRYPTO_new_ex_data
	.type	CRYPTO_new_ex_data, @function
CRYPTO_new_ex_data:
.LFB261:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$15, %r14d
	ja	.L93
	leaq	do_ex_data_init_ossl_(%rip), %rsi
	leaq	ex_data_init(%rip), %rdi
	movq	%rdx, %rbx
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L60
	movl	do_ex_data_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	je	.L60
	movq	ex_data_lock(%rip), %rdi
	testq	%rdi, %rdi
	je	.L92
	call	CRYPTO_THREAD_write_lock@PLT
	leaq	ex_data(%rip), %r8
	movq	$0, (%rbx)
	movq	(%r8,%r14,8), %rdi
	call	OPENSSL_sk_num@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jle	.L62
	leaq	-144(%rbp), %rax
	cmpl	$9, %r13d
	leaq	ex_data(%rip), %r8
	movq	%rax, -160(%rbp)
	movq	%rax, %r12
	jg	.L94
.L63:
	subl	$1, %r13d
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L65:
	movq	(%r8,%r14,8), %rdi
	movl	%r15d, %esi
	call	OPENSSL_sk_value@PLT
	leaq	ex_data(%rip), %r8
	movq	%rax, (%r12,%r15,8)
	movq	%r15, %rax
	addq	$1, %r15
	cmpq	%r13, %rax
	jne	.L65
	movq	ex_data_lock(%rip), %rdi
	xorl	%r15d, %r15d
	call	CRYPTO_THREAD_unlock@PLT
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L75:
	movq	%rax, %r15
.L70:
	movq	(%r12,%r15,8), %rdx
	movl	%r15d, %r14d
	testq	%rdx, %rdx
	je	.L67
	movq	16(%rdx), %r11
	testq	%r11, %r11
	je	.L67
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L68
	movq	%rsi, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%r15d, %eax
	jle	.L95
	movq	(%rbx), %rdi
	movl	%r15d, %esi
	call	OPENSSL_sk_value@PLT
	movq	(%r12,%r15,8), %rdx
	movq	%rax, %rsi
	movq	16(%rdx), %r11
.L68:
	movq	8(%rdx), %r9
	movq	(%rdx), %r8
	movl	%r14d, %ecx
	movq	%rbx, %rdx
	movq	-152(%rbp), %rdi
	call	*%r11
.L67:
	leaq	1(%r15), %rax
	cmpq	%r15, %r13
	jne	.L75
	movl	$1, %eax
	cmpq	-160(%rbp), %r12
	je	.L57
.L71:
	movl	$245, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	movl	$1, %eax
.L57:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L96
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	movq	(%r12,%r15,8), %rdx
	xorl	%esi, %esi
	movq	16(%rdx), %r11
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L94:
	movslq	%r13d, %rdi
	movl	$226, %edx
	leaq	.LC0(%rip), %rsi
	salq	$3, %rdi
	call	CRYPTO_malloc@PLT
	leaq	ex_data(%rip), %r8
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.L63
	movq	ex_data_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movl	$234, %r8d
	movl	$65, %edx
	leaq	.LC0(%rip), %rcx
	movl	$112, %esi
	movl	$15, %edi
	call	ERR_put_error@PLT
.L92:
	xorl	%eax, %eax
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L60:
	movl	$60, %r8d
	movl	$65, %edx
	movl	$113, %esi
	movl	$15, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L62:
	movq	ex_data_lock(%rip), %rdi
	xorl	%r12d, %r12d
	call	CRYPTO_THREAD_unlock@PLT
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L93:
	movl	$55, %r8d
	movl	$7, %edx
	movl	$113, %esi
	movl	$15, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L57
.L96:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE261:
	.size	CRYPTO_new_ex_data, .-CRYPTO_new_ex_data
	.p2align 4
	.globl	CRYPTO_free_ex_data
	.type	CRYPTO_free_ex_data, @function
CRYPTO_free_ex_data:
.LFB263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movslq	%edi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$136, %rsp
	movq	%rsi, -160(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$15, %r12d
	ja	.L145
	leaq	do_ex_data_init_ossl_(%rip), %rsi
	leaq	ex_data_init(%rip), %rdi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L100
	movl	do_ex_data_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	je	.L100
	movq	ex_data_lock(%rip), %rdi
	testq	%rdi, %rdi
	je	.L99
	call	CRYPTO_THREAD_write_lock@PLT
	leaq	ex_data(%rip), %r15
	movq	(%r15,%r12,8), %rdi
	call	OPENSSL_sk_num@PLT
	movl	%eax, -152(%rbp)
	testl	%eax, %eax
	jle	.L103
	leaq	-144(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	movq	%rcx, %r13
	cmpl	$9, %eax
	jg	.L146
.L104:
	movl	-152(%rbp), %eax
	xorl	%edx, %edx
	leal	-1(%rax), %r14d
	.p2align 4,,10
	.p2align 3
.L106:
	movq	(%r15,%r12,8), %rdi
	movl	%edx, %esi
	movq	%rdx, -152(%rbp)
	call	OPENSSL_sk_value@PLT
	movq	-152(%rbp), %rdx
	movq	%rax, 0(%r13,%rdx,8)
	movq	%rdx, %rax
	addq	$1, %rdx
	cmpq	%r14, %rax
	jne	.L106
	movq	ex_data_lock(%rip), %rdi
	xorl	%r12d, %r12d
	call	CRYPTO_THREAD_unlock@PLT
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L121:
	movq	%rax, %r12
.L111:
	movq	0(%r13,%r12,8), %r15
	movl	%r12d, %ecx
	testq	%r15, %r15
	je	.L108
	movq	24(%r15), %r10
	testq	%r10, %r10
	je	.L108
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L109
	movq	%rsi, %rdi
	movl	%r12d, -152(%rbp)
	call	OPENSSL_sk_num@PLT
	movl	-152(%rbp), %ecx
	cmpl	%r12d, %eax
	jle	.L147
	movq	(%rbx), %rdi
	movl	%r12d, %esi
	movl	%ecx, -152(%rbp)
	call	OPENSSL_sk_value@PLT
	movq	24(%r15), %r10
	movl	-152(%rbp), %ecx
	movq	%rax, %rsi
.L109:
	movq	8(%r15), %r9
	movq	(%r15), %r8
	movq	%rbx, %rdx
	movq	-160(%rbp), %rdi
	call	*%r10
.L108:
	leaq	1(%r12), %rax
	cmpq	%r12, %r14
	jne	.L121
	cmpq	-168(%rbp), %r13
	jne	.L118
.L99:
	movq	(%rbx), %rdi
	call	OPENSSL_sk_free@PLT
	movq	$0, (%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L148
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	movq	ex_data_lock(%rip), %rdi
	xorl	%r13d, %r13d
	call	CRYPTO_THREAD_unlock@PLT
.L118:
	movl	$359, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L147:
	movq	24(%r15), %r10
	xorl	%esi, %esi
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L100:
	movl	$60, %r8d
	movl	$65, %edx
	movl	$113, %esi
	movl	$15, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L145:
	movl	$55, %r8d
	movl	$7, %edx
	movl	$113, %esi
	movl	$15, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L146:
	movslq	%eax, %rdi
	movl	$337, %edx
	leaq	.LC0(%rip), %rsi
	salq	$3, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L104
	movq	ex_data_lock(%rip), %rdi
	xorl	%r14d, %r14d
	call	CRYPTO_THREAD_unlock@PLT
	.p2align 4,,10
	.p2align 3
.L113:
	movq	ex_data_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	(%r15,%r12,8), %rdi
	movl	%r14d, %esi
	call	OPENSSL_sk_value@PLT
	movq	ex_data_lock(%rip), %rdi
	movq	%rax, -168(%rbp)
	call	CRYPTO_THREAD_unlock@PLT
	movq	-168(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L114
	movq	24(%rdx), %rax
	testq	%rax, %rax
	je	.L114
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L117
	movq	%rsi, %rdi
	movq	%rdx, -168(%rbp)
	call	OPENSSL_sk_num@PLT
	movq	-168(%rbp), %rdx
	cmpl	%eax, %r14d
	jge	.L116
	movq	(%rbx), %rdi
	movl	%r14d, %esi
	call	OPENSSL_sk_value@PLT
	movq	-168(%rbp), %rdx
	movq	%rax, %rsi
	movq	24(%rdx), %rax
.L117:
	movq	8(%rdx), %r9
	movq	(%rdx), %r8
	movl	%r14d, %ecx
	movq	%rbx, %rdx
	movq	-160(%rbp), %rdi
	call	*%rax
.L114:
	addl	$1, %r14d
	cmpl	%r14d, -152(%rbp)
	jne	.L113
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L116:
	movq	24(%rdx), %rax
	xorl	%esi, %esi
	jmp	.L117
.L148:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE263:
	.size	CRYPTO_free_ex_data, .-CRYPTO_free_ex_data
	.p2align 4
	.globl	CRYPTO_set_ex_data
	.type	CRYPTO_set_ex_data, @function
CRYPTO_set_ex_data:
.LFB264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L158
.L150:
	call	OPENSSL_sk_num@PLT
	movl	%eax, %r14d
	cmpl	%eax, %r12d
	jge	.L152
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L153:
	addl	$1, %r14d
	cmpl	%r14d, %r12d
	jl	.L154
.L152:
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L153
	movl	$382, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	%eax, -36(%rbp)
	movl	$102, %esi
	movl	$15, %edi
	call	ERR_put_error@PLT
	movl	-36(%rbp), %eax
.L149:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L154:
	.cfi_restore_state
	movq	(%rbx), %rdi
	movq	%r13, %rdx
	movl	%r12d, %esi
	call	OPENSSL_sk_set@PLT
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L150
	movl	$375, %r8d
	movl	$65, %edx
	movl	$102, %esi
	movl	$15, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L149
	.cfi_endproc
.LFE264:
	.size	CRYPTO_set_ex_data, .-CRYPTO_set_ex_data
	.p2align 4
	.globl	CRYPTO_dup_ex_data
	.type	CRYPTO_dup_ex_data, @function
CRYPTO_dup_ex_data:
.LFB262:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, (%rdx)
	je	.L166
	movl	%edi, %ebx
	cmpl	$15, %edi
	ja	.L210
	movq	%rsi, %r13
	leaq	ex_data_init(%rip), %rdi
	leaq	do_ex_data_init_ossl_(%rip), %rsi
	movq	%rdx, %r12
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L163
	movl	do_ex_data_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	je	.L163
	movq	ex_data_lock(%rip), %rdi
	testq	%rdi, %rdi
	je	.L183
	call	CRYPTO_THREAD_write_lock@PLT
	movslq	%ebx, %rcx
	leaq	ex_data(%rip), %r8
	movq	(%r8,%rcx,8), %rdi
	movq	%rcx, -168(%rbp)
	call	OPENSSL_sk_num@PLT
	movq	(%r12), %rdi
	movl	%eax, %r14d
	call	OPENSSL_sk_num@PLT
	movq	-168(%rbp), %rcx
	leaq	ex_data(%rip), %r8
	cmpl	%r14d, %eax
	cmovle	%eax, %r14d
	testl	%r14d, %r14d
	jle	.L211
	leaq	-144(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	%rax, %r15
	cmpl	$9, %r14d
	jg	.L212
.L168:
	leal	-1(%r14), %eax
	xorl	%ebx, %ebx
	movq	%rax, -168(%rbp)
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L170:
	movq	(%r8,%rcx,8), %rdi
	movl	%ebx, %esi
	movq	%rcx, -176(%rbp)
	call	OPENSSL_sk_value@PLT
	movq	-176(%rbp), %rcx
	leaq	ex_data(%rip), %r8
	movq	%rax, (%r15,%rbx,8)
	movq	%rbx, %rax
	addq	$1, %rbx
	cmpq	%rax, -168(%rbp)
	jne	.L170
	movq	ex_data_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L173
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r14d
	jge	.L173
	movq	0(%r13), %rdi
	movl	%r14d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdx
.L172:
	movl	%r14d, %esi
	movq	%r13, %rdi
	xorl	%ebx, %ebx
	call	CRYPTO_set_ex_data
	testl	%eax, %eax
	jne	.L174
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L214:
	movq	(%r12), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdx
.L177:
	movq	%rdx, -152(%rbp)
	movq	(%r15,%rbx,8), %rax
	testq	%rax, %rax
	je	.L179
	movq	32(%rax), %r10
	testq	%r10, %r10
	je	.L179
	movq	8(%rax), %r9
	movq	(%rax), %r8
	movl	%r14d, %ecx
	movq	%r12, %rsi
	leaq	-152(%rbp), %rdx
	movq	%r13, %rdi
	call	*%r10
	testl	%eax, %eax
	je	.L180
	movq	-152(%rbp), %rdx
.L179:
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	CRYPTO_set_ex_data
	leaq	1(%rbx), %rax
	cmpq	%rbx, -168(%rbp)
	je	.L213
	movq	%rax, %rbx
.L174:
	movq	(%r12), %rdi
	movl	%ebx, %r14d
	testq	%rdi, %rdi
	je	.L178
	call	OPENSSL_sk_num@PLT
	cmpl	%ebx, %eax
	jg	.L214
.L178:
	xorl	%edx, %edx
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L211:
	movq	ex_data_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	testl	%r14d, %r14d
	jne	.L167
	.p2align 4,,10
	.p2align 3
.L166:
	movl	$1, %eax
.L159:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L215
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L212:
	.cfi_restore_state
	movslq	%r14d, %rdi
	movl	$277, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rcx, -168(%rbp)
	salq	$3, %rdi
	call	CRYPTO_malloc@PLT
	movq	-168(%rbp), %rcx
	leaq	ex_data(%rip), %r8
	testq	%rax, %rax
	movq	%rax, %r15
	jne	.L168
	movq	ex_data_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
.L167:
	movl	$287, %r8d
	movl	$65, %edx
	movl	$110, %esi
	movl	$15, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L163:
	movl	$60, %r8d
	movl	$65, %edx
	movl	$113, %esi
	movl	$15, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L180:
	xorl	%eax, %eax
.L175:
	cmpq	-184(%rbp), %r15
	je	.L159
	movl	$311, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	movl	%eax, -168(%rbp)
	call	CRYPTO_free@PLT
	movl	-168(%rbp), %eax
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L210:
	movl	$55, %r8d
	movl	$7, %edx
	movl	$113, %esi
	movl	$15, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L213:
	movl	$1, %eax
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L173:
	xorl	%edx, %edx
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L183:
	xorl	%eax, %eax
	jmp	.L159
.L215:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE262:
	.size	CRYPTO_dup_ex_data, .-CRYPTO_dup_ex_data
	.p2align 4
	.globl	CRYPTO_get_ex_data
	.type	CRYPTO_get_ex_data, @function
CRYPTO_get_ex_data:
.LFB265:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L218
	movl	%esi, %r12d
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L218
	movq	(%rbx), %rdi
	movl	%r12d, %esi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	OPENSSL_sk_value@PLT
	.p2align 4,,10
	.p2align 3
.L218:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE265:
	.size	CRYPTO_get_ex_data, .-CRYPTO_get_ex_data
	.local	do_ex_data_init_ossl_ret_
	.comm	do_ex_data_init_ossl_ret_,4,4
	.local	ex_data_init
	.comm	ex_data_init,4,4
	.local	ex_data_lock
	.comm	ex_data_lock,8,8
	.local	ex_data
	.comm	ex_data,128,32
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
