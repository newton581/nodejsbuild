	.file	"ec_print.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ec/ec_print.c"
	.text
	.p2align 4
	.globl	EC_POINT_point2bn
	.type	EC_POINT_point2bn, @function
EC_POINT_point2bn:
.LFB377:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	pushq	%r12
	leaq	-32(%rbp), %rcx
	.cfi_offset 12, -32
	xorl	%r12d, %r12d
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	EC_POINT_point2buf@PLT
	testq	%rax, %rax
	je	.L1
	movq	-32(%rbp), %rdi
	movq	%r13, %rdx
	movl	%eax, %esi
	call	BN_bin2bn@PLT
	movq	-32(%rbp), %rdi
	movl	$29, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rax, %r12
	call	CRYPTO_free@PLT
.L1:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L9
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L9:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE377:
	.size	EC_POINT_point2bn, .-EC_POINT_point2bn
	.p2align 4
	.globl	EC_POINT_bn2point
	.type	EC_POINT_bn2point, @function
EC_POINT_bn2point:
.LFB378:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$24, %rsp
	movq	%rcx, -56(%rbp)
	call	BN_num_bits@PLT
	movl	$1, %ecx
	movl	$43, %edx
	leaq	.LC0(%rip), %rsi
	leal	14(%rax), %r12d
	addl	$7, %eax
	cmovns	%eax, %r12d
	sarl	$3, %r12d
	movslq	%r12d, %r12
	testq	%r12, %r12
	cmove	%rcx, %r12
	movq	%r12, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L22
	movl	%r12d, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	BN_bn2binpad@PLT
	testl	%eax, %eax
	je	.L23
	testq	%r13, %r13
	je	.L24
	movq	-56(%rbp), %r8
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	EC_POINT_oct2point@PLT
	testl	%eax, %eax
	je	.L17
.L16:
	movq	%r14, %rdi
	movl	$68, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %r14
	call	CRYPTO_free@PLT
.L10:
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	EC_POINT_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L25
	movq	-56(%rbp), %r8
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	EC_POINT_oct2point@PLT
	testl	%eax, %eax
	jne	.L16
	movq	%r13, %rdi
	call	EC_POINT_clear_free@PLT
.L17:
	movq	%r14, %rdi
	movl	$64, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%r14d, %r14d
	call	CRYPTO_free@PLT
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L23:
	movq	%r14, %rdi
	movl	$49, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%r14d, %r14d
	call	CRYPTO_free@PLT
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L22:
	movl	$44, %r8d
	movl	$65, %edx
	movl	$280, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L25:
	movq	%r14, %rdi
	movl	$55, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%r14d, %r14d
	call	CRYPTO_free@PLT
	jmp	.L10
	.cfi_endproc
.LFE378:
	.size	EC_POINT_bn2point, .-EC_POINT_bn2point
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"0123456789ABCDEF"
	.text
	.p2align 4
	.globl	EC_POINT_point2hex
	.type	EC_POINT_point2hex, @function
EC_POINT_point2hex:
.LFB379:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	xorl	%r12d, %r12d
	pushq	%rbx
	leaq	-32(%rbp), %rcx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$0, -32(%rbp)
	call	EC_POINT_point2buf@PLT
	testq	%rax, %rax
	je	.L26
	leaq	2(%rax,%rax), %rdi
	movl	$88, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rbx
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L35
	movq	-32(%rbp), %rcx
	movq	%r12, %rsi
	leaq	.LC1(%rip), %r8
	leaq	(%rcx,%rbx), %rax
	.p2align 4,,10
	.p2align 3
.L29:
	movzbl	(%rcx), %edx
	addq	$1, %rcx
	addq	$2, %rsi
	movq	%rdx, %rdi
	andl	$15, %edx
	shrq	$4, %rdi
	movzbl	(%r8,%rdx), %edx
	andl	$15, %edi
	movzbl	(%r8,%rdi), %edi
	movb	%dil, -2(%rsi)
	movb	%dl, -1(%rsi)
	cmpq	%rax, %rcx
	jne	.L29
	movb	$0, (%r12,%rbx,2)
	movq	-32(%rbp), %rdi
	movl	$102, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
.L26:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L36
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L35:
	.cfi_restore_state
	movq	-32(%rbp), %rdi
	movl	$90, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L26
.L36:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE379:
	.size	EC_POINT_point2hex, .-EC_POINT_point2hex
	.p2align 4
	.globl	EC_POINT_hex2point
	.type	EC_POINT_hex2point, @function
EC_POINT_hex2point:
.LFB380:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	leaq	-48(%rbp), %rdi
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	call	BN_hex2bn@PLT
	testl	%eax, %eax
	je	.L37
	movq	-48(%rbp), %rsi
	movq	%r13, %rdi
	movq	%r15, %rcx
	movq	%r14, %rdx
	call	EC_POINT_bn2point
	movq	-48(%rbp), %rdi
	movq	%rax, %r12
	call	BN_clear_free@PLT
.L37:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L44
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L44:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE380:
	.size	EC_POINT_hex2point, .-EC_POINT_hex2point
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
