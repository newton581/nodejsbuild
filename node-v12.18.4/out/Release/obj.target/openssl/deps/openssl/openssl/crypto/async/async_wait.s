	.file	"async_wait.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/async/async_wait.c"
	.text
	.p2align 4
	.globl	ASYNC_WAIT_CTX_new
	.type	ASYNC_WAIT_CTX_new, @function
ASYNC_WAIT_CTX_new:
.LFB239:
	.cfi_startproc
	endbr64
	movl	$17, %edx
	leaq	.LC0(%rip), %rsi
	movl	$24, %edi
	jmp	CRYPTO_zalloc@PLT
	.cfi_endproc
.LFE239:
	.size	ASYNC_WAIT_CTX_new, .-ASYNC_WAIT_CTX_new
	.p2align 4
	.globl	ASYNC_WAIT_CTX_free
	.type	ASYNC_WAIT_CTX_free, @function
ASYNC_WAIT_CTX_free:
.LFB240:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L3
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	.LC0(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %r12
	testq	%r12, %r12
	jne	.L7
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%rbx, %r12
.L7:
	movl	36(%r12), %eax
	testl	%eax, %eax
	jne	.L6
	movq	24(%r12), %rax
	testq	%rax, %rax
	je	.L6
	movq	16(%r12), %rcx
	movl	8(%r12), %edx
	movq	%r13, %rdi
	movq	(%r12), %rsi
	call	*%rax
.L6:
	movq	40(%r12), %rbx
	movl	$37, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	testq	%rbx, %rbx
	jne	.L8
.L5:
	popq	%rbx
	.cfi_restore 3
	movq	%r13, %rdi
	popq	%r12
	.cfi_restore 12
	movl	$41, %edx
	popq	%r13
	.cfi_restore 13
	leaq	.LC0(%rip), %rsi
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L3:
	ret
	.cfi_endproc
.LFE240:
	.size	ASYNC_WAIT_CTX_free, .-ASYNC_WAIT_CTX_free
	.p2align 4
	.globl	ASYNC_WAIT_CTX_set_wait_fd
	.type	ASYNC_WAIT_CTX_set_wait_fd, @function
ASYNC_WAIT_CTX_set_wait_fd:
.LFB241:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movl	$48, %edi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	leaq	.LC0(%rip), %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	movl	$50, %edx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$8, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L22
	movq	(%r15), %rdx
	movq	%r14, (%rax)
	movl	%r13d, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rbx, 24(%rax)
	movl	$1, 32(%rax)
	movq	%rdx, 40(%rax)
	addq	$1, 8(%r15)
	movq	%rax, (%r15)
	movl	$1, %eax
.L18:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	movl	$51, %r8d
	movl	$65, %edx
	movl	$106, %esi
	movl	$51, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L18
	.cfi_endproc
.LFE241:
	.size	ASYNC_WAIT_CTX_set_wait_fd, .-ASYNC_WAIT_CTX_set_wait_fd
	.p2align 4
	.globl	ASYNC_WAIT_CTX_get_fd
	.type	ASYNC_WAIT_CTX_get_fd, @function
ASYNC_WAIT_CTX_get_fd:
.LFB242:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L29
	.p2align 4,,10
	.p2align 3
.L28:
	movl	36(%rax), %edi
	testl	%edi, %edi
	jne	.L27
	cmpq	%rsi, (%rax)
	je	.L31
.L27:
	movq	40(%rax), %rax
	testq	%rax, %rax
	jne	.L28
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	movl	8(%rax), %esi
	movq	16(%rax), %rax
	movl	%esi, (%rdx)
	movq	%rax, (%rcx)
	movl	$1, %eax
	ret
.L29:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE242:
	.size	ASYNC_WAIT_CTX_get_fd, .-ASYNC_WAIT_CTX_get_fd
	.p2align 4
	.globl	ASYNC_WAIT_CTX_get_all_fds
	.type	ASYNC_WAIT_CTX_get_all_fds, @function
ASYNC_WAIT_CTX_get_all_fds:
.LFB243:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	$0, (%rdx)
.L43:
	testq	%rax, %rax
	je	.L42
.L44:
	movl	36(%rax), %ecx
	movq	%rax, %rdi
	movq	40(%rax), %rax
	testl	%ecx, %ecx
	jne	.L43
	testq	%rsi, %rsi
	je	.L35
	movl	8(%rdi), %ecx
	addq	$4, %rsi
	movl	%ecx, -4(%rsi)
.L35:
	addq	$1, (%rdx)
	testq	%rax, %rax
	jne	.L44
.L42:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE243:
	.size	ASYNC_WAIT_CTX_get_all_fds, .-ASYNC_WAIT_CTX_get_all_fds
	.p2align 4
	.globl	ASYNC_WAIT_CTX_get_changed_fds
	.type	ASYNC_WAIT_CTX_get_changed_fds, @function
ASYNC_WAIT_CTX_get_changed_fds:
.LFB244:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	%rax, (%rdx)
	movq	16(%rdi), %rax
	movq	%rax, (%r8)
	movq	%rsi, %rax
	orq	%rcx, %rax
	je	.L55
	movq	(%rdi), %rax
	testq	%rax, %rax
	jne	.L50
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L64:
	testl	%edx, %edx
	jne	.L49
	testq	%rcx, %rcx
	je	.L49
	movl	8(%rax), %edx
	addq	$4, %rcx
	movl	%edx, -4(%rcx)
	movl	32(%rax), %edi
	testl	%edi, %edi
	jne	.L63
	.p2align 4,,10
	.p2align 3
.L49:
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L55
.L50:
	movl	36(%rax), %r8d
	movl	32(%rax), %edx
	testl	%r8d, %r8d
	jne	.L64
	testl	%edx, %edx
	je	.L49
.L51:
	testq	%rsi, %rsi
	je	.L49
	movl	8(%rax), %edx
	movq	40(%rax), %rax
	addq	$4, %rsi
	movl	%edx, -4(%rsi)
	testq	%rax, %rax
	jne	.L50
.L55:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	movl	36(%rax), %edx
	testl	%edx, %edx
	je	.L51
	jmp	.L49
	.cfi_endproc
.LFE244:
	.size	ASYNC_WAIT_CTX_get_changed_fds, .-ASYNC_WAIT_CTX_get_changed_fds
	.p2align 4
	.globl	ASYNC_WAIT_CTX_clear_fd
	.type	ASYNC_WAIT_CTX_clear_fd, @function
ASYNC_WAIT_CTX_clear_fd:
.LFB245:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rcx
	testq	%rcx, %rcx
	je	.L74
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rcx, %rdi
	subq	$24, %rsp
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L75:
	movq	%rax, %rdi
.L73:
	cmpl	$1, 36(%rdi)
	je	.L69
	cmpq	%rsi, (%rdi)
	je	.L80
.L69:
	movq	40(%rdi), %rax
	movq	%rdi, %rdx
	testq	%rax, %rax
	jne	.L75
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	movl	32(%rdi), %eax
	cmpl	$1, %eax
	je	.L81
	movl	$1, 36(%rdi)
	movl	$1, %eax
	addq	$1, 16(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	movq	40(%rdi), %rsi
	cmpq	%rdi, %rcx
	je	.L82
	movq	%rsi, 40(%rdx)
.L72:
	movl	$165, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, -20(%rbp)
	call	CRYPTO_free@PLT
	subq	$1, 8(%rbx)
	movl	-20(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
.L74:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
.L82:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	%rsi, (%rbx)
	jmp	.L72
	.cfi_endproc
.LFE245:
	.size	ASYNC_WAIT_CTX_clear_fd, .-ASYNC_WAIT_CTX_clear_fd
	.p2align 4
	.globl	async_wait_ctx_reset_counts
	.type	async_wait_ctx_reset_counts, @function
async_wait_ctx_reset_counts:
.LFB246:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movups	%xmm0, 8(%rdi)
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L83
	xorl	%ebx, %ebx
	leaq	.LC0(%rip), %r13
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L85:
	movl	32(%rdi), %edx
	testl	%edx, %edx
	je	.L90
	movl	$0, 32(%rdi)
.L90:
	movq	%rdi, %rbx
	movq	%rax, %rdi
	testq	%rdi, %rdi
	je	.L83
.L84:
	movl	36(%rdi), %esi
	movq	40(%rdi), %rax
	testl	%esi, %esi
	je	.L85
	testq	%rbx, %rbx
	je	.L88
	movq	%rax, 40(%rbx)
	movl	$200, %edx
	movq	%r13, %rsi
	call	CRYPTO_free@PLT
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L84
.L83:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	movq	%rax, (%r12)
	movl	$200, %edx
	movq	%r13, %rsi
	call	CRYPTO_free@PLT
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L83
	movl	36(%rdi), %ecx
	movq	40(%rdi), %rax
	testl	%ecx, %ecx
	je	.L85
	jmp	.L88
	.cfi_endproc
.LFE246:
	.size	async_wait_ctx_reset_counts, .-async_wait_ctx_reset_counts
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
