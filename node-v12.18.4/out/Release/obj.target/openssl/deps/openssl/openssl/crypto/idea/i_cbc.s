	.file	"i_cbc.c"
	.text
	.p2align 4
	.globl	IDEA_encrypt
	.type	IDEA_encrypt, @function
IDEA_encrypt:
.LFB1:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	8(%rdi), %r9
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rax, %r11
	movq	%r9, %rdx
	shrq	$16, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	shrq	$16, %rdx
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	(%rsi), %ecx
	movzwl	%r11w, %r11d
	movq	%rcx, %r8
	imulq	%r11, %rcx
	testq	%rcx, %rcx
	je	.L2
	movzwl	%cx, %r10d
	shrq	$16, %rcx
	subq	%rcx, %r10
	movq	%r10, %rcx
	shrq	$16, %rcx
	subq	%rcx, %r10
.L3:
	movl	4(%rsi), %r12d
	movl	8(%rsi), %ebx
	addq	%rax, %r12
	movl	12(%rsi), %eax
	addq	%rdx, %rbx
	movzwl	%r9w, %edx
	movq	%rax, %rcx
	imulq	%rdx, %rax
	testq	%rax, %rax
	je	.L4
	movzwl	%ax, %r9d
	shrq	$16, %rax
	subq	%rax, %r9
	movq	%r9, %rax
	shrq	$16, %rax
	subq	%rax, %r9
.L5:
	movq	%r10, %rcx
	movl	16(%rsi), %edx
	xorq	%rbx, %rcx
	movzwl	%cx, %ecx
	movq	%rdx, %r11
	imulq	%rcx, %rdx
	testq	%rdx, %rdx
	je	.L6
	movzwl	%dx, %r8d
	shrq	$16, %rdx
	subq	%rdx, %r8
	movq	%r8, %rax
	shrq	$16, %rax
	subq	%rax, %r8
.L7:
	movq	%r9, %r11
	movl	20(%rsi), %eax
	xorq	%r12, %r11
	addq	%r8, %r11
	movq	%rax, %rdx
	movzwl	%r11w, %r11d
	imulq	%r11, %rax
	testq	%rax, %rax
	je	.L8
	movzwl	%ax, %ecx
	shrq	$16, %rax
	subq	%rax, %rcx
	movq	%rcx, %rax
	movq	%rcx, %rdx
	shrq	$16, %rax
	subq	%rax, %rdx
.L9:
	movl	24(%rsi), %r11d
	xorq	%rdx, %r10
	addq	%rdx, %r8
	xorq	%rbx, %rdx
	movzwl	%r10w, %ecx
	xorq	%r8, %r9
	xorq	%r12, %r8
	movq	%r11, %rax
	imulq	%rcx, %r11
	testq	%r11, %r11
	je	.L10
	movzwl	%r11w, %r10d
	shrq	$16, %r11
	subq	%r11, %r10
	movq	%r10, %rax
	shrq	$16, %rax
	subq	%rax, %r10
.L11:
	movl	32(%rsi), %eax
	movzwl	%r9w, %ecx
	movl	28(%rsi), %ebx
	leaq	(%rax,%r8), %r12
	movl	36(%rsi), %eax
	addq	%rbx, %rdx
	movq	%rax, %r8
	imulq	%rcx, %rax
	testq	%rax, %rax
	je	.L12
	movzwl	%ax, %r9d
	shrq	$16, %rax
	subq	%rax, %r9
	movq	%r9, %rax
	shrq	$16, %rax
	subq	%rax, %r9
.L13:
	movq	%r10, %rcx
	movl	40(%rsi), %eax
	xorq	%r12, %rcx
	movzwl	%cx, %ecx
	movq	%rax, %r11
	imulq	%rcx, %rax
	testq	%rax, %rax
	je	.L14
	movzwl	%ax, %r8d
	shrq	$16, %rax
	subq	%rax, %r8
	movq	%r8, %rax
	shrq	$16, %rax
	subq	%rax, %r8
.L15:
	movq	%r9, %r11
	movl	44(%rsi), %eax
	xorq	%rdx, %r11
	addq	%r8, %r11
	movq	%rax, %rbx
	movzwl	%r11w, %r11d
	imulq	%r11, %rax
	testq	%rax, %rax
	je	.L16
	movzwl	%ax, %ecx
	shrq	$16, %rax
	subq	%rax, %rcx
	movq	%rcx, %rax
	shrq	$16, %rax
	subq	%rax, %rcx
	movq	%rcx, %rax
.L17:
	movl	48(%rsi), %r11d
	xorq	%rax, %r10
	leaq	(%r8,%rax), %rbx
	xorq	%r12, %rax
	movzwl	%r10w, %ecx
	xorq	%rbx, %r9
	xorq	%rbx, %rdx
	movq	%r11, %r8
	imulq	%rcx, %r11
	testq	%r11, %r11
	je	.L18
	movzwl	%r11w, %r10d
	shrq	$16, %r11
	subq	%r11, %r10
	movq	%r10, %rcx
	shrq	$16, %rcx
	subq	%rcx, %r10
.L19:
	movl	56(%rsi), %ebx
	movzwl	%r9w, %ecx
	movl	52(%rsi), %r12d
	addq	%rdx, %rbx
	movl	60(%rsi), %edx
	addq	%r12, %rax
	movq	%rdx, %r8
	imulq	%rcx, %rdx
	testq	%rdx, %rdx
	je	.L20
	movzwl	%dx, %r9d
	shrq	$16, %rdx
	subq	%rdx, %r9
	movq	%r9, %rdx
	shrq	$16, %rdx
	subq	%rdx, %r9
.L21:
	movq	%r10, %rcx
	movl	64(%rsi), %edx
	xorq	%rbx, %rcx
	movzwl	%cx, %ecx
	movq	%rdx, %r11
	imulq	%rcx, %rdx
	testq	%rdx, %rdx
	je	.L22
	movzwl	%dx, %r8d
	shrq	$16, %rdx
	subq	%rdx, %r8
	movq	%r8, %rdx
	shrq	$16, %rdx
	subq	%rdx, %r8
.L23:
	movq	%r9, %r11
	movl	68(%rsi), %edx
	xorq	%rax, %r11
	addq	%r8, %r11
	movq	%rdx, %r12
	movzwl	%r11w, %r11d
	imulq	%r11, %rdx
	testq	%rdx, %rdx
	je	.L24
	movzwl	%dx, %ecx
	shrq	$16, %rdx
	subq	%rdx, %rcx
	movq	%rcx, %rdx
	shrq	$16, %rdx
	subq	%rdx, %rcx
	movq	%rcx, %rdx
.L25:
	movl	72(%rsi), %r11d
	xorq	%rdx, %r10
	leaq	(%r8,%rdx), %r12
	xorq	%rbx, %rdx
	movzwl	%r10w, %ecx
	xorq	%r12, %r9
	xorq	%r12, %rax
	movq	%r11, %r8
	imulq	%rcx, %r11
	testq	%r11, %r11
	je	.L26
	movzwl	%r11w, %r10d
	shrq	$16, %r11
	subq	%r11, %r10
	movq	%r10, %rcx
	shrq	$16, %rcx
	subq	%rcx, %r10
.L27:
	movl	80(%rsi), %r12d
	movzwl	%r9w, %ecx
	movl	76(%rsi), %ebx
	addq	%rax, %r12
	movl	84(%rsi), %eax
	addq	%rbx, %rdx
	movq	%rax, %r8
	imulq	%rcx, %rax
	testq	%rax, %rax
	je	.L28
	movzwl	%ax, %r9d
	shrq	$16, %rax
	subq	%rax, %r9
	movq	%r9, %rax
	shrq	$16, %rax
	subq	%rax, %r9
.L29:
	movq	%r10, %rcx
	movl	88(%rsi), %eax
	xorq	%r12, %rcx
	movzwl	%cx, %ecx
	movq	%rax, %r11
	imulq	%rcx, %rax
	testq	%rax, %rax
	je	.L30
	movzwl	%ax, %r8d
	shrq	$16, %rax
	subq	%rax, %r8
	movq	%r8, %rax
	shrq	$16, %rax
	subq	%rax, %r8
.L31:
	movq	%r9, %r11
	movl	92(%rsi), %eax
	xorq	%rdx, %r11
	addq	%r8, %r11
	movq	%rax, %rbx
	movzwl	%r11w, %r11d
	imulq	%r11, %rax
	testq	%rax, %rax
	je	.L32
	movzwl	%ax, %ecx
	shrq	$16, %rax
	subq	%rax, %rcx
	movq	%rcx, %rax
	shrq	$16, %rax
	subq	%rax, %rcx
	movq	%rcx, %rax
.L33:
	movl	96(%rsi), %r11d
	xorq	%rax, %r10
	leaq	(%r8,%rax), %rbx
	xorq	%r12, %rax
	movzwl	%r10w, %ecx
	xorq	%rbx, %r9
	xorq	%rbx, %rdx
	movq	%r11, %r8
	imulq	%rcx, %r11
	testq	%r11, %r11
	je	.L34
	movzwl	%r11w, %r10d
	shrq	$16, %r11
	subq	%r11, %r10
	movq	%r10, %rcx
	shrq	$16, %rcx
	subq	%rcx, %r10
.L35:
	movl	104(%rsi), %ebx
	movzwl	%r9w, %ecx
	movl	100(%rsi), %r12d
	addq	%rdx, %rbx
	movl	108(%rsi), %edx
	addq	%r12, %rax
	movq	%rdx, %r8
	imulq	%rcx, %rdx
	testq	%rdx, %rdx
	je	.L36
	movzwl	%dx, %r9d
	shrq	$16, %rdx
	subq	%rdx, %r9
	movq	%r9, %rdx
	shrq	$16, %rdx
	subq	%rdx, %r9
.L37:
	movq	%r10, %rcx
	movl	112(%rsi), %edx
	xorq	%rbx, %rcx
	movzwl	%cx, %ecx
	movq	%rdx, %r11
	imulq	%rcx, %rdx
	testq	%rdx, %rdx
	je	.L38
	movzwl	%dx, %r8d
	shrq	$16, %rdx
	subq	%rdx, %r8
	movq	%r8, %rdx
	shrq	$16, %rdx
	subq	%rdx, %r8
.L39:
	movq	%r9, %r11
	movl	116(%rsi), %edx
	xorq	%rax, %r11
	addq	%r8, %r11
	movq	%rdx, %r12
	movzwl	%r11w, %r11d
	imulq	%r11, %rdx
	testq	%rdx, %rdx
	je	.L40
	movzwl	%dx, %ecx
	shrq	$16, %rdx
	subq	%rdx, %rcx
	movq	%rcx, %rdx
	shrq	$16, %rdx
	subq	%rdx, %rcx
	movq	%rcx, %rdx
.L41:
	movl	120(%rsi), %r11d
	xorq	%rdx, %r10
	leaq	(%r8,%rdx), %r12
	xorq	%rbx, %rdx
	movzwl	%r10w, %ecx
	xorq	%r12, %r9
	xorq	%r12, %rax
	movq	%r11, %r8
	imulq	%rcx, %r11
	testq	%r11, %r11
	je	.L42
	movzwl	%r11w, %r10d
	shrq	$16, %r11
	subq	%r11, %r10
	movq	%r10, %rcx
	shrq	$16, %rcx
	subq	%rcx, %r10
.L43:
	movl	128(%rsi), %r12d
	movzwl	%r9w, %ecx
	movl	124(%rsi), %ebx
	addq	%rax, %r12
	movl	132(%rsi), %eax
	addq	%rbx, %rdx
	movq	%rax, %r8
	imulq	%rcx, %rax
	testq	%rax, %rax
	je	.L44
	movzwl	%ax, %r9d
	shrq	$16, %rax
	subq	%rax, %r9
	movq	%r9, %rax
	shrq	$16, %rax
	subq	%rax, %r9
.L45:
	movq	%r10, %rcx
	movl	136(%rsi), %eax
	xorq	%r12, %rcx
	movzwl	%cx, %ecx
	movq	%rax, %r11
	imulq	%rcx, %rax
	testq	%rax, %rax
	je	.L46
	movzwl	%ax, %r8d
	shrq	$16, %rax
	subq	%rax, %r8
	movq	%r8, %rax
	shrq	$16, %rax
	subq	%rax, %r8
.L47:
	movq	%r9, %r11
	movl	140(%rsi), %eax
	xorq	%rdx, %r11
	addq	%r8, %r11
	movq	%rax, %rbx
	movzwl	%r11w, %r11d
	imulq	%r11, %rax
	testq	%rax, %rax
	je	.L48
	movzwl	%ax, %ecx
	shrq	$16, %rax
	subq	%rax, %rcx
	movq	%rcx, %rax
	shrq	$16, %rax
	subq	%rax, %rcx
	movq	%rcx, %rax
.L49:
	movl	144(%rsi), %r11d
	xorq	%rax, %r10
	leaq	(%r8,%rax), %rbx
	xorq	%r12, %rax
	movzwl	%r10w, %ecx
	xorq	%rbx, %r9
	xorq	%rbx, %rdx
	movq	%r11, %r8
	imulq	%rcx, %r11
	testq	%r11, %r11
	je	.L50
	movzwl	%r11w, %r10d
	shrq	$16, %r11
	subq	%r11, %r10
	movq	%r10, %rcx
	shrq	$16, %rcx
	subq	%rcx, %r10
.L51:
	movl	152(%rsi), %ebx
	movl	148(%rsi), %r8d
	movzwl	%r9w, %ecx
	addq	%rdx, %rbx
	movl	156(%rsi), %edx
	addq	%r8, %rax
	movq	%rdx, %r8
	imulq	%rcx, %rdx
	testq	%rdx, %rdx
	je	.L52
	movzwl	%dx, %r9d
	shrq	$16, %rdx
	subq	%rdx, %r9
	movq	%r9, %rdx
	shrq	$16, %rdx
	subq	%rdx, %r9
.L53:
	movq	%r10, %rcx
	movl	160(%rsi), %edx
	xorq	%rbx, %rcx
	movzwl	%cx, %ecx
	movq	%rdx, %r11
	imulq	%rcx, %rdx
	testq	%rdx, %rdx
	je	.L54
	movzwl	%dx, %r8d
	shrq	$16, %rdx
	subq	%rdx, %r8
	movq	%r8, %rdx
	shrq	$16, %rdx
	subq	%rdx, %r8
.L55:
	movq	%r9, %r11
	movl	164(%rsi), %edx
	xorq	%rax, %r11
	addq	%r8, %r11
	movq	%rdx, %r12
	movzwl	%r11w, %r11d
	imulq	%r11, %rdx
	testq	%rdx, %rdx
	je	.L56
	movzwl	%dx, %ecx
	shrq	$16, %rdx
	subq	%rdx, %rcx
	movq	%rcx, %rdx
	shrq	$16, %rdx
	subq	%rdx, %rcx
	movq	%rcx, %rdx
.L57:
	movl	168(%rsi), %r11d
	xorq	%rdx, %r10
	addq	%rdx, %r8
	xorq	%rbx, %rdx
	movzwl	%r10w, %ecx
	xorq	%r8, %r9
	xorq	%r8, %rax
	movq	%r11, %r8
	imulq	%rcx, %r11
	testq	%r11, %r11
	je	.L58
	movzwl	%r11w, %r10d
	shrq	$16, %r11
	subq	%r11, %r10
	movq	%r10, %rcx
	shrq	$16, %rcx
	subq	%rcx, %r10
.L59:
	movl	176(%rsi), %r8d
	movl	172(%rsi), %ebx
	addq	%rax, %r8
	movl	180(%rsi), %eax
	addq	%rdx, %rbx
	movzwl	%r9w, %edx
	movq	%rax, %rcx
	imulq	%rdx, %rax
	testq	%rax, %rax
	je	.L60
	movzwl	%ax, %r9d
	shrq	$16, %rax
	subq	%rax, %r9
	movq	%r9, %rax
	shrq	$16, %rax
	subq	%rax, %r9
.L61:
	movq	%r10, %r11
	movl	184(%rsi), %eax
	xorq	%r8, %r11
	movzwl	%r11w, %r11d
	movq	%rax, %r12
	imulq	%r11, %rax
	testq	%rax, %rax
	je	.L62
	movzwl	%ax, %ecx
	shrq	$16, %rax
	subq	%rax, %rcx
	movq	%rcx, %rax
	shrq	$16, %rax
	subq	%rax, %rcx
.L63:
	movq	%r9, %r11
	movl	188(%rsi), %eax
	xorq	%rbx, %r11
	addq	%rcx, %r11
	movq	%rax, %r12
	movzwl	%r11w, %r11d
	imulq	%r11, %rax
	testq	%rax, %rax
	je	.L64
	movzwl	%ax, %edx
	shrq	$16, %rax
	subq	%rax, %rdx
	movq	%rdx, %rax
	shrq	$16, %rax
	subq	%rax, %rdx
	movq	%rdx, %rax
.L65:
	movl	192(%rsi), %r11d
	xorq	%rax, %r10
	leaq	(%rcx,%rax), %rdx
	xorq	%r8, %rax
	movzwl	%r10w, %r10d
	xorq	%rdx, %r9
	xorq	%rbx, %rdx
	movq	%r11, %rcx
	imulq	%r10, %r11
	testq	%r11, %r11
	je	.L66
	movzwl	%r11w, %r8d
	shrq	$16, %r11
	subq	%r11, %r8
	movq	%r8, %rcx
	shrq	$16, %rcx
	subq	%rcx, %r8
.L67:
	movl	196(%rsi), %ebx
	movzwl	%r9w, %r9d
	movl	200(%rsi), %ecx
	addq	%rdx, %rbx
	movl	204(%rsi), %edx
	addq	%rax, %rcx
	movq	%rdx, %rsi
	imulq	%r9, %rdx
	testq	%rdx, %rdx
	je	.L68
	movzwl	%dx, %eax
	shrq	$16, %rdx
	subq	%rdx, %rax
	movq	%rax, %rdx
	shrq	$16, %rdx
	subq	%rdx, %rax
	movq	%rax, %rdx
.L69:
	movq	%rcx, %rax
	movq	%r8, %rsi
	movzwl	%bx, %ebx
	salq	$16, %rax
	salq	$16, %rsi
	movl	%eax, %ecx
	movl	%esi, %esi
	movzwl	%dx, %eax
	orq	%rbx, %rsi
	orq	%rcx, %rax
	popq	%rbx
	popq	%r12
	movq	%rsi, (%rdi)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	%rax, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movl	$1, %r10d
	subl	%r8d, %r10d
	subl	%r11d, %r10d
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L68:
	movl	$1, %eax
	subl	%esi, %eax
	movl	%eax, %edx
	subl	%r9d, %edx
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L66:
	movl	$1, %r8d
	subl	%ecx, %r8d
	subl	%r10d, %r8d
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L64:
	movl	$1, %edx
	subl	%r12d, %edx
	movl	%edx, %eax
	subl	%r11d, %eax
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L62:
	movl	$1, %ecx
	subl	%r12d, %ecx
	subl	%r11d, %ecx
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L60:
	movl	$1, %r9d
	subl	%ecx, %r9d
	subl	%edx, %r9d
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L58:
	movl	$1, %r10d
	subl	%r8d, %r10d
	subl	%ecx, %r10d
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L56:
	movl	$1, %ecx
	subl	%r12d, %ecx
	movl	%ecx, %edx
	subl	%r11d, %edx
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L54:
	movl	$1, %r8d
	subl	%r11d, %r8d
	subl	%ecx, %r8d
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L52:
	movl	$1, %r9d
	subl	%r8d, %r9d
	subl	%ecx, %r9d
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L50:
	movl	$1, %r10d
	subl	%r8d, %r10d
	subl	%ecx, %r10d
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L48:
	movl	$1, %ecx
	subl	%ebx, %ecx
	movl	%ecx, %eax
	subl	%r11d, %eax
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L46:
	movl	$1, %r8d
	subl	%r11d, %r8d
	subl	%ecx, %r8d
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L44:
	movl	$1, %r9d
	subl	%r8d, %r9d
	subl	%ecx, %r9d
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L42:
	movl	$1, %r10d
	subl	%r8d, %r10d
	subl	%ecx, %r10d
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L40:
	movl	$1, %ecx
	subl	%r12d, %ecx
	movl	%ecx, %edx
	subl	%r11d, %edx
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L38:
	movl	$1, %r8d
	subl	%r11d, %r8d
	subl	%ecx, %r8d
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$1, %r9d
	subl	%r8d, %r9d
	subl	%ecx, %r9d
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L34:
	movl	$1, %r10d
	subl	%r8d, %r10d
	subl	%ecx, %r10d
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L32:
	movl	$1, %ecx
	subl	%ebx, %ecx
	movl	%ecx, %eax
	subl	%r11d, %eax
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L30:
	movl	$1, %r8d
	subl	%r11d, %r8d
	subl	%ecx, %r8d
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L28:
	movl	$1, %r9d
	subl	%r8d, %r9d
	subl	%ecx, %r9d
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L26:
	movl	$1, %r10d
	subl	%r8d, %r10d
	subl	%ecx, %r10d
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L24:
	movl	$1, %ecx
	subl	%r12d, %ecx
	movl	%ecx, %edx
	subl	%r11d, %edx
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L22:
	movl	$1, %r8d
	subl	%r11d, %r8d
	subl	%ecx, %r8d
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$1, %r9d
	subl	%r8d, %r9d
	subl	%ecx, %r9d
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$1, %r10d
	subl	%r8d, %r10d
	subl	%ecx, %r10d
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L16:
	movl	$1, %ecx
	subl	%ebx, %ecx
	movl	%ecx, %eax
	subl	%r11d, %eax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L14:
	movl	$1, %r8d
	subl	%r11d, %r8d
	subl	%ecx, %r8d
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$1, %r9d
	subl	%r8d, %r9d
	subl	%ecx, %r9d
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L10:
	movl	$1, %r10d
	subl	%eax, %r10d
	subl	%ecx, %r10d
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$1, %ecx
	subl	%edx, %ecx
	movl	%ecx, %edx
	subl	%r11d, %edx
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$1, %r8d
	subl	%r11d, %r8d
	subl	%ecx, %r8d
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L4:
	movl	$1, %r9d
	subl	%ecx, %r9d
	subl	%edx, %r9d
	jmp	.L5
	.cfi_endproc
.LFE1:
	.size	IDEA_encrypt, .-IDEA_encrypt
	.p2align 4
	.globl	IDEA_cbc_encrypt
	.type	IDEA_cbc_encrypt, @function
IDEA_cbc_encrypt:
.LFB0:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-8(%rdx), %rdi
	subq	$120, %rsp
	movq	%rdx, -112(%rbp)
	movl	4(%r8), %edx
	movq	%rsi, -120(%rbp)
	movq	%r8, -136(%rbp)
	bswap	%edx
	movl	%edx, %r12d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r8), %eax
	movq	%rdi, -128(%rbp)
	bswap	%eax
	movl	%eax, %eax
	testl	%r9d, %r9d
	je	.L73
	testq	%rdi, %rdi
	js	.L100
	shrq	$3, %rdi
	movq	%rsi, %r15
	movq	%r12, %rsi
	movq	%rdi, -88(%rbp)
	leaq	8(,%rdi,8), %rdi
	movq	%rdi, -96(%rbp)
	leaq	(%rbx,%rdi), %r14
	leaq	-80(%rbp), %rdi
	.p2align 4,,10
	.p2align 3
.L75:
	movl	4(%rbx), %r12d
	movl	(%rbx), %edx
	addq	$8, %r15
	addq	$8, %rbx
	bswap	%edx
	movl	%edx, %ecx
	movl	%r12d, %edx
	bswap	%edx
	movl	%edx, %r12d
	xorq	%rax, %rcx
	xorq	%rsi, %r12
	movq	%rcx, %xmm0
	movq	%r13, %rsi
	movq	%r12, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	IDEA_encrypt
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rsi
	movq	%rax, %rdx
	movb	%al, -5(%r15)
	shrq	$24, %rdx
	movb	%dl, -8(%r15)
	movq	%rax, %rdx
	shrq	$16, %rdx
	movb	%dl, -7(%r15)
	movq	%rax, %rdx
	shrq	$8, %rdx
	movb	%dl, -6(%r15)
	movq	%rsi, %rdx
	shrq	$24, %rdx
	movb	%dl, -4(%r15)
	movq	%rsi, %rdx
	shrq	$16, %rdx
	movb	%dl, -3(%r15)
	movq	%rsi, %rdx
	shrq	$8, %rdx
	movb	%dl, -2(%r15)
	movb	%sil, -1(%r15)
	cmpq	%rbx, %r14
	jne	.L75
	movq	-88(%rbp), %r14
	movq	-96(%rbp), %rdi
	movq	%rsi, %r12
	addq	%rdi, -120(%rbp)
	movq	-112(%rbp), %rdi
	negq	%r14
	salq	$3, %r14
	leaq	-16(%rdi,%r14), %rcx
	addq	-128(%rbp), %r14
	movq	%r14, -112(%rbp)
.L74:
	cmpq	$-8, %rcx
	je	.L117
	leaq	8(%rbx,%rcx), %rcx
	movq	-112(%rbp), %rbx
	cmpq	$7, %rbx
	ja	.L78
	leaq	.L80(%rip), %r8
	movslq	(%r8,%rbx,4), %rdi
	addq	%r8, %rdi
	notrack jmp	*%rdi
	.section	.rodata
	.align 4
	.align 4
.L80:
	.long	.L78-.L80
	.long	.L101-.L80
	.long	.L102-.L80
	.long	.L103-.L80
	.long	.L104-.L80
	.long	.L105-.L80
	.long	.L106-.L80
	.long	.L79-.L80
	.text
	.p2align 4,,10
	.p2align 3
.L73:
	movq	%rdi, %rcx
	testq	%rdi, %rdi
	js	.L107
	shrq	$3, %rcx
	movq	%r12, -88(%rbp)
	movq	%rsi, %r14
	movq	%rax, %r15
	leaq	8(,%rcx,8), %rdi
	movq	%rcx, -144(%rbp)
	leaq	(%rbx,%rdi), %rcx
	movq	%rdi, -152(%rbp)
	leaq	-80(%rbp), %rdi
	movq	%rcx, -104(%rbp)
	.p2align 4,,10
	.p2align 3
.L89:
	movl	(%rbx), %eax
	movl	4(%rbx), %esi
	movq	%r15, -96(%rbp)
	addq	$8, %r14
	movq	-88(%rbp), %r12
	addq	$8, %rbx
	bswap	%eax
	bswap	%esi
	movl	%eax, %r15d
	movl	%esi, %eax
	movq	%rax, -88(%rbp)
	movq	%r15, %xmm0
	movq	%r13, %rsi
	movhps	-88(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	IDEA_encrypt
	movq	-96(%rbp), %rdx
	xorq	-80(%rbp), %rdx
	movq	%rdx, %rsi
	movq	-72(%rbp), %rax
	movb	%dl, -5(%r14)
	shrq	$24, %rsi
	movb	%sil, -8(%r14)
	movq	%rdx, %rsi
	xorq	%r12, %rax
	shrq	$16, %rsi
	movb	%sil, -7(%r14)
	movq	%rdx, %rsi
	movq	%rax, %rdx
	shrq	$24, %rdx
	shrq	$8, %rsi
	movb	%dl, -4(%r14)
	movq	%rax, %rdx
	shrq	$16, %rdx
	movb	%sil, -6(%r14)
	movb	%dl, -3(%r14)
	movq	%rax, %rdx
	shrq	$8, %rdx
	movb	%dl, -2(%r14)
	movb	%al, -1(%r14)
	cmpq	%rbx, -104(%rbp)
	jne	.L89
	movq	-144(%rbp), %rcx
	movq	-152(%rbp), %rdi
	movq	%r15, %rax
	addq	%rdi, -120(%rbp)
	movq	-112(%rbp), %rdi
	negq	%rcx
	movq	-88(%rbp), %r12
	salq	$3, %rcx
	leaq	-16(%rdi,%rcx), %r14
	addq	-128(%rbp), %rcx
	movq	%rcx, -112(%rbp)
.L88:
	cmpq	$-8, %r14
	je	.L90
	movl	(%rbx), %ecx
	leaq	-80(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, -88(%rbp)
	bswap	%ecx
	movd	%ecx, %xmm0
	movl	4(%rbx), %ecx
	movdqa	%xmm0, %xmm2
	bswap	%ecx
	movd	%ecx, %xmm1
	punpcklqdq	%xmm1, %xmm2
	movaps	%xmm2, -80(%rbp)
	call	IDEA_encrypt
	movq	-120(%rbp), %rbx
	movq	-72(%rbp), %rdx
	movq	-88(%rbp), %rax
	xorq	-80(%rbp), %rax
	leaq	8(%rbx,%r14), %rcx
	movq	-112(%rbp), %rbx
	xorq	%r12, %rdx
	cmpq	$7, %rbx
	ja	.L108
	leaq	.L92(%rip), %rdi
	movslq	(%rdi,%rbx,4), %rsi
	addq	%rdi, %rsi
	notrack jmp	*%rsi
	.section	.rodata
	.align 4
	.align 4
.L92:
	.long	.L108-.L92
	.long	.L98-.L92
	.long	.L109-.L92
	.long	.L96-.L92
	.long	.L110-.L92
	.long	.L94-.L92
	.long	.L93-.L92
	.long	.L91-.L92
	.text
.L91:
	movq	%rdx, %rsi
	subq	$1, %rcx
	shrq	$8, %rsi
	movb	%sil, (%rcx)
.L93:
	movq	%rdx, %rsi
	subq	$1, %rcx
	shrq	$16, %rsi
	movb	%sil, (%rcx)
.L94:
	shrq	$24, %rdx
	leaq	-1(%rcx), %rsi
	movb	%dl, -1(%rcx)
.L95:
	movb	%al, -1(%rsi)
	leaq	-1(%rsi), %rcx
.L96:
	movq	%rax, %rsi
	leaq	-1(%rcx), %rdx
	shrq	$8, %rsi
	movb	%sil, -1(%rcx)
.L97:
	movq	%rax, %rsi
	leaq	-1(%rdx), %rcx
	shrq	$16, %rsi
	movb	%sil, -1(%rdx)
.L98:
	shrq	$24, %rax
	movq	%xmm1, %r12
	movb	%al, -1(%rcx)
	movq	%xmm0, %rax
.L90:
	movq	-136(%rbp), %rcx
	movl	%r12d, %edx
	bswap	%eax
	bswap	%edx
	movl	%eax, (%rcx)
	movl	%edx, 4(%rcx)
.L72:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L118
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	xorl	%r8d, %r8d
.L81:
	movzbl	-1(%rcx), %edi
	subq	$1, %rcx
	salq	$16, %rdi
	orq	%r8, %rdi
.L82:
	leaq	-1(%rcx), %r8
	movzbl	-1(%rcx), %ecx
	salq	$24, %rcx
	orq	%rdi, %rcx
	xorq	%rcx, %r12
.L83:
	leaq	-1(%r8), %rcx
	movzbl	-1(%r8), %r8d
.L84:
	movzbl	-1(%rcx), %edi
	leaq	-1(%rcx), %r9
	salq	$8, %rdi
	orq	%rdi, %r8
.L85:
	movzbl	-1(%r9), %edi
	leaq	-1(%r9), %rcx
	salq	$16, %rdi
	orq	%r8, %rdi
.L86:
	movzbl	-1(%rcx), %ecx
	salq	$24, %rcx
	orq	%rdi, %rcx
	xorq	%rcx, %rax
.L78:
	movq	%rax, %xmm0
	movq	%r12, %xmm4
	leaq	-80(%rbp), %rdi
	movq	%r13, %rsi
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	IDEA_encrypt
	movq	-120(%rbp), %r11
	movl	-80(%rbp), %edx
	movq	-80(%rbp), %rax
	movl	-72(%rbp), %r10d
	bswap	%edx
	movl	%edx, (%r11)
	movq	-72(%rbp), %rdx
	movzbl	%ah, %ecx
	bswap	%r10d
	movl	%r10d, 4(%r11)
	movq	%rax, %r8
	movq	%rax, %rdi
	movq	%rcx, %r9
	movq	%rdx, %rsi
	movq	%rdx, %rcx
	shrq	$24, %r8
	shrq	$16, %rdi
	movzbl	%dh, %ebx
	shrq	$24, %rsi
	shrq	$16, %rcx
.L77:
	movq	-136(%rbp), %r11
	movb	%r8b, (%r11)
	movb	%dil, 1(%r11)
	movb	%r9b, 2(%r11)
	movb	%al, 3(%r11)
	movb	%sil, 4(%r11)
	movb	%cl, 5(%r11)
	movb	%bl, 6(%r11)
	movb	%dl, 7(%r11)
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L100:
	movq	%rdi, %rcx
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L107:
	movq	%rdi, %r14
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L117:
	movzbl	%ah, %ecx
	movq	%rax, %r8
	movq	%rax, %rdi
	movq	%r12, %rsi
	movq	%rcx, %r9
	movq	%r12, %rbx
	movq	%r12, %rcx
	shrq	$24, %r8
	shrq	$16, %rdi
	shrq	$24, %rsi
	movzbl	%bh, %ebx
	movl	%r12d, %edx
	shrq	$16, %rcx
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L101:
	xorl	%edi, %edi
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L102:
	movq	%rcx, %r9
	xorl	%r8d, %r8d
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L103:
	xorl	%r8d, %r8d
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L104:
	movq	%rcx, %r8
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L105:
	xorl	%edi, %edi
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L79:
	movzbl	-1(%rcx), %edi
	subq	$1, %rcx
	salq	$8, %rdi
	movq	%rdi, %r8
	jmp	.L81
.L109:
	movq	%rcx, %rdx
	jmp	.L97
.L110:
	movq	%rcx, %rsi
	jmp	.L95
.L118:
	call	__stack_chk_fail@PLT
.L108:
	movq	%xmm1, %r12
	movq	%xmm0, %rax
	jmp	.L90
	.cfi_endproc
.LFE0:
	.size	IDEA_cbc_encrypt, .-IDEA_cbc_encrypt
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
