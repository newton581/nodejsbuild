	.file	"tasn_new.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/asn1/tasn_new.c"
	.text
	.p2align 4
	.type	asn1_primitive_new, @function
asn1_primitive_new:
.LFB460:
	.cfi_startproc
	testq	%rsi, %rsi
	je	.L23
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	32(%rsi), %rax
	testq	%rax, %rax
	je	.L3
	testl	%edx, %edx
	je	.L4
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.L5
	call	*%rax
	addq	$24, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L6
	addq	$24, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	cmpb	$5, (%rsi)
	je	.L24
	movq	8(%rsi), %rax
	movl	%eax, %edi
	cmpl	$5, %eax
	je	.L8
	jg	.L9
	cmpl	$-4, %eax
	je	.L10
	cmpl	$1, %eax
	jne	.L7
.L11:
	movq	40(%rsi), %rax
	movl	%eax, (%r12)
	movl	$1, %eax
.L1:
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	cmpb	$5, (%rsi)
	je	.L40
	movq	8(%rsi), %rax
	movl	%eax, %edi
	cmpl	$5, %eax
	je	.L8
	jg	.L22
	cmpl	$-4, %eax
	je	.L10
	cmpl	$1, %eax
	je	.L11
	.p2align 4,,10
	.p2align 3
.L14:
	movq	%rsi, -24(%rbp)
	call	ASN1_STRING_type_new@PLT
	movq	-24(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, (%r12)
	movq	%rax, %rdx
	setne	%al
	cmpb	$5, (%rsi)
	sete	%cl
	andb	%al, %cl
	je	.L13
	movq	16(%rdx), %rsi
	movl	%ecx, %eax
	orq	$64, %rsi
.L16:
	movq	%rsi, 16(%rdx)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L5:
	cmpb	$5, (%rsi)
	je	.L41
	movq	8(%rsi), %rax
	movl	%eax, %edi
	cmpl	$5, %eax
	je	.L8
	jg	.L20
	cmpl	$-4, %eax
	je	.L10
	cmpl	$1, %eax
	je	.L11
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L24:
	movl	$-1, %edi
.L7:
	testl	%edx, %edx
	je	.L14
.L17:
	movq	(%r12), %rdx
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rdx)
	movq	$128, 16(%rdx)
	movl	%edi, 4(%rdx)
	cmpb	$5, (%rsi)
	je	.L15
	cmpq	$0, (%r12)
	setne	%al
.L13:
	addq	$24, %rsp
	movzbl	%al, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	cmpl	$6, %eax
	jne	.L7
.L21:
	xorl	%edi, %edi
	call	OBJ_nid2obj@PLT
	movq	%rax, (%r12)
	movl	$1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L22:
	cmpl	$6, %eax
	je	.L21
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L20:
	cmpl	$6, %eax
	je	.L21
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L8:
	movq	$1, (%r12)
	movl	$1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L10:
	movl	$302, %edx
	leaq	.LC0(%rip), %rsi
	movl	$16, %edi
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L42
	movq	$0, 8(%rax)
	movl	$-1, (%rax)
	movq	%rax, (%r12)
	movl	$1, %eax
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L41:
	movl	$-1, %edi
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L40:
	movl	$-1, %edi
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L15:
	cmpq	$0, (%r12)
	movl	$192, %esi
	setne	%al
	jmp	.L16
.L42:
	movl	$303, %r8d
	movl	$65, %edx
	movl	$119, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L1
	.cfi_endproc
.LFE460:
	.size	asn1_primitive_new, .-asn1_primitive_new
	.p2align 4
	.type	asn1_item_clear, @function
asn1_item_clear:
.LFB457:
	.cfi_startproc
	leaq	.L46(%rip), %rdx
.L52:
	cmpb	$6, (%rsi)
	ja	.L43
	movzbl	(%rsi), %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L46:
	.long	.L49-.L46
	.long	.L45-.L46
	.long	.L45-.L46
	.long	.L43-.L46
	.long	.L48-.L46
	.long	.L47-.L46
	.long	.L45-.L46
	.text
	.p2align 4,,10
	.p2align 3
.L49:
	movq	16(%rsi), %rax
	testq	%rax, %rax
	je	.L51
	testq	$774, (%rax)
	je	.L72
	.p2align 4,,10
	.p2align 3
.L45:
	movq	$0, (%rdi)
.L43:
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	movq	32(%rsi), %rax
	testq	%rax, %rax
	je	.L45
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L45
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L47:
	movq	32(%rsi), %rax
	testq	%rax, %rax
	je	.L45
.L71:
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.L45
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L51:
	movq	32(%rsi), %rax
	testq	%rax, %rax
	jne	.L71
	cmpl	$1, 8(%rsi)
	jne	.L45
	movq	40(%rsi), %rax
	movl	%eax, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	movq	32(%rax), %rsi
	jmp	.L52
	.cfi_endproc
.LFE457:
	.size	asn1_item_clear, .-asn1_item_clear
	.p2align 4
	.type	asn1_template_new.isra.0, @function
asn1_template_new.isra.0:
.LFB465:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	andl	$4096, %r14d
	je	.L74
	movq	%rdi, -64(%rbp)
	leaq	-64(%rbp), %r12
.L74:
	testb	$1, %sil
	jne	.L166
	testl	$768, %esi
	jne	.L165
	movq	%rsi, %rbx
	andl	$6, %ebx
	je	.L79
	call	OPENSSL_sk_new_null@PLT
	testq	%rax, %rax
	je	.L167
	movq	%rax, (%r12)
	movl	$1, %eax
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L166:
	testl	$774, %esi
	je	.L76
.L165:
	movq	$0, (%r12)
	movl	$1, %eax
.L73:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L168
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	movq	32(%r13), %rcx
	movzbl	0(%r13), %eax
	testq	%rcx, %rcx
	je	.L81
	movq	24(%rcx), %r15
	testq	%r15, %r15
	je	.L169
	cmpb	$6, %al
	ja	.L116
	leaq	.L110(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L110:
	.long	.L88-.L110
	.long	.L98-.L110
	.long	.L91-.L110
	.long	.L116-.L110
	.long	.L84-.L110
	.long	.L90-.L110
	.long	.L98-.L110
	.text
	.p2align 4,,10
	.p2align 3
.L81:
	cmpb	$6, %al
	ja	.L116
	leaq	.L112(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L112:
	.long	.L88-.L112
	.long	.L117-.L112
	.long	.L118-.L112
	.long	.L116-.L112
	.long	.L116-.L112
	.long	.L90-.L112
	.long	.L117-.L112
	.text
.L118:
	xorl	%r15d, %r15d
.L94:
	movq	40(%r13), %rdi
	testl	%r14d, %r14d
	je	.L95
	movq	(%r12), %r8
	movq	%rdi, %rdx
	xorl	%esi, %esi
	movq	%r8, %rdi
	call	memset@PLT
.L96:
	movq	%r13, %rdx
	movl	$-1, %esi
	movq	%r12, %rdi
	call	asn1_set_choice_selector@PLT
	testq	%r15, %r15
	je	.L116
.L106:
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movl	$1, %edi
	call	*%r15
	testl	%eax, %eax
	jne	.L116
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	asn1_item_embed_free@PLT
.L93:
	movl	$161, %r8d
	movl	$100, %edx
	movl	$121, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L73
.L117:
	xorl	%r15d, %r15d
.L100:
	movq	40(%r13), %rdi
	testl	%r14d, %r14d
	jne	.L170
	movl	$122, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_zalloc@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L104
	xorl	%esi, %esi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	asn1_do_lock@PLT
	testl	%eax, %eax
	js	.L171
.L102:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	asn1_enc_init@PLT
	cmpq	$0, 24(%r13)
	movq	16(%r13), %rcx
	jg	.L105
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L107:
	addq	$40, %rcx
	addq	$1, %rbx
	cmpq	%rbx, 24(%r13)
	jle	.L108
.L105:
	movq	%rcx, %rsi
	movq	%r12, %rdi
	movq	%rcx, -72(%rbp)
	call	asn1_get_field_ptr@PLT
	movq	-72(%rbp), %rcx
	movq	%rax, %rdi
	movq	32(%rcx), %rdx
	movq	(%rcx), %rsi
	call	asn1_template_new.isra.0
	movq	-72(%rbp), %rcx
	testl	%eax, %eax
	jne	.L107
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	asn1_item_embed_free@PLT
	jmp	.L104
.L84:
	movq	8(%rcx), %rax
	testq	%rax, %rax
	je	.L116
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L104
.L116:
	movl	$1, %eax
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L76:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	asn1_item_clear
	movl	$1, %eax
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L169:
	cmpb	$6, %al
	ja	.L116
	leaq	.L111(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L111:
	.long	.L88-.L111
	.long	.L100-.L111
	.long	.L94-.L111
	.long	.L116-.L111
	.long	.L84-.L111
	.long	.L90-.L111
	.long	.L100-.L111
	.text
.L90:
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	asn1_primitive_new
	testl	%eax, %eax
	jne	.L116
.L104:
	movl	$152, %r8d
	movl	$65, %edx
	movl	$121, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L73
.L88:
	movq	16(%r13), %rax
	testq	%rax, %rax
	je	.L90
	movq	32(%rax), %rdx
	movq	(%rax), %rsi
	movq	%r12, %rdi
	call	asn1_template_new.isra.0
	testl	%eax, %eax
	jne	.L116
	jmp	.L104
.L98:
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	*%r15
	testl	%eax, %eax
	je	.L93
	cmpl	$2, %eax
	jne	.L100
	jmp	.L116
.L91:
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	*%r15
	testl	%eax, %eax
	je	.L93
	cmpl	$2, %eax
	jne	.L94
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L167:
	movl	$231, %r8d
	movl	$65, %edx
	movl	$133, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L95:
	movl	$97, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_zalloc@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	jne	.L96
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L170:
	movq	(%r12), %r8
	movq	%rdi, %rdx
	xorl	%esi, %esi
	movq	%r8, %rdi
	call	memset@PLT
	xorl	%esi, %esi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	asn1_do_lock@PLT
	testl	%eax, %eax
	js	.L104
	jmp	.L102
.L108:
	testq	%r15, %r15
	jne	.L106
	jmp	.L116
.L171:
	movq	(%r12), %rdi
	movl	$129, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, (%r12)
	jmp	.L104
.L168:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE465:
	.size	asn1_template_new.isra.0, .-asn1_template_new.isra.0
	.p2align 4
	.globl	ASN1_item_new
	.type	ASN1_item_new, @function
ASN1_item_new:
.LFB454:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	32(%rdi), %r8
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	movzbl	(%rdi), %eax
	testq	%r8, %r8
	je	.L173
	movq	24(%r8), %rbx
	testq	%rbx, %rbx
	je	.L252
	cmpb	$6, %al
	ja	.L209
	leaq	.L201(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L201:
	.long	.L179-.L201
	.long	.L188-.L201
	.long	.L184-.L201
	.long	.L209-.L201
	.long	.L176-.L201
	.long	.L181-.L201
	.long	.L188-.L201
	.text
.L209:
	xorl	%r8d, %r8d
.L172:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L253
	addq	$24, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L252:
	.cfi_restore_state
	cmpb	$6, %al
	ja	.L209
	leaq	.L203(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L203:
	.long	.L179-.L203
	.long	.L190-.L203
	.long	.L182-.L203
	.long	.L209-.L203
	.long	.L176-.L203
	.long	.L181-.L203
	.long	.L190-.L203
	.text
	.p2align 4,,10
	.p2align 3
.L173:
	cmpb	$6, %al
	ja	.L172
	leaq	.L202(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L202:
	.long	.L179-.L202
	.long	.L208-.L202
	.long	.L182-.L202
	.long	.L172-.L202
	.long	.L172-.L202
	.long	.L181-.L202
	.long	.L208-.L202
	.text
.L208:
	xorl	%ebx, %ebx
.L190:
	movq	40(%r12), %rdi
	movl	$122, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_zalloc@PLT
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L192
	leaq	-64(%rbp), %r15
	xorl	%esi, %esi
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	asn1_do_lock@PLT
	testl	%eax, %eax
	js	.L254
	movq	%r12, %rsi
	movq	%r15, %rdi
	xorl	%r14d, %r14d
	call	asn1_enc_init@PLT
	cmpq	$0, 24(%r12)
	movq	16(%r12), %r13
	jg	.L193
	.p2align 4,,10
	.p2align 3
.L197:
	testq	%rbx, %rbx
	jne	.L251
	.p2align 4,,10
	.p2align 3
.L248:
	movq	-64(%rbp), %r8
	jmp	.L172
.L181:
	xorl	%edx, %edx
	leaq	-64(%rbp), %rdi
	movq	%r12, %rsi
	call	asn1_primitive_new
	testl	%eax, %eax
	jne	.L248
.L192:
	movl	$152, %r8d
	movl	$65, %edx
	movl	$121, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r8d, %r8d
	jmp	.L172
.L179:
	movq	16(%r12), %rax
	testq	%rax, %rax
	je	.L181
	movq	32(%rax), %rdx
	movq	(%rax), %rsi
	leaq	-64(%rbp), %rdi
	call	asn1_template_new.isra.0
	testl	%eax, %eax
	jne	.L248
	jmp	.L192
.L188:
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdx
	call	*%rbx
	testl	%eax, %eax
	je	.L186
	cmpl	$2, %eax
	jne	.L190
	jmp	.L248
.L184:
	leaq	-64(%rbp), %r15
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	*%rbx
	testl	%eax, %eax
	je	.L186
	cmpl	$2, %eax
	je	.L248
	movq	40(%r12), %rdi
	movl	$97, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_zalloc@PLT
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L192
	movq	%r12, %rdx
	movl	$-1, %esi
	movq	%r15, %rdi
	call	asn1_set_choice_selector@PLT
.L251:
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r15, %rsi
	movl	$1, %edi
	call	*%rbx
	testl	%eax, %eax
	jne	.L248
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	asn1_item_embed_free@PLT
.L186:
	movl	$161, %r8d
	movl	$100, %edx
	movl	$121, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r8d, %r8d
	jmp	.L172
.L176:
	movq	8(%r8), %rax
	xorl	%r8d, %r8d
	testq	%rax, %rax
	je	.L172
	leaq	-64(%rbp), %rdi
	movq	%r12, %rsi
	call	*%rax
	testl	%eax, %eax
	jne	.L248
	jmp	.L192
.L182:
	movq	40(%r12), %rdi
	movl	$97, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_zalloc@PLT
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L192
	leaq	-64(%rbp), %rdi
	movq	%r12, %rdx
	movl	$-1, %esi
	call	asn1_set_choice_selector@PLT
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L196:
	addq	$40, %r13
	addq	$1, %r14
	cmpq	%r14, 24(%r12)
	jle	.L197
.L193:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	asn1_get_field_ptr@PLT
	movq	32(%r13), %rdx
	movq	0(%r13), %rsi
	movq	%rax, %rdi
	call	asn1_template_new.isra.0
	testl	%eax, %eax
	jne	.L196
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	asn1_item_embed_free@PLT
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L254:
	movq	-64(%rbp), %rdi
	movl	$129, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, -64(%rbp)
	jmp	.L192
.L253:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE454:
	.size	ASN1_item_new, .-ASN1_item_new
	.p2align 4
	.globl	ASN1_item_ex_new
	.type	ASN1_item_ex_new, @function
ASN1_item_ex_new:
.LFB455:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	32(%rsi), %rcx
	movzbl	(%rsi), %eax
	testq	%rcx, %rcx
	je	.L256
	movq	24(%rcx), %rbx
	testq	%rbx, %rbx
	je	.L339
	cmpb	$6, %al
	ja	.L338
	leaq	.L284(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L284:
	.long	.L264-.L284
	.long	.L273-.L284
	.long	.L269-.L284
	.long	.L338-.L284
	.long	.L259-.L284
	.long	.L266-.L284
	.long	.L273-.L284
	.text
.L267:
	movq	40(%r12), %rdi
	movl	$97, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_zalloc@PLT
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	je	.L277
	movq	%r12, %rdx
	movl	$-1, %esi
	movq	%r13, %rdi
	call	asn1_set_choice_selector@PLT
.L338:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L339:
	.cfi_restore_state
	cmpb	$6, %al
	ja	.L338
	leaq	.L286(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L286:
	.long	.L264-.L286
	.long	.L275-.L286
	.long	.L267-.L286
	.long	.L338-.L286
	.long	.L259-.L286
	.long	.L266-.L286
	.long	.L275-.L286
	.text
	.p2align 4,,10
	.p2align 3
.L256:
	cmpb	$6, %al
	ja	.L338
	leaq	.L285(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L285:
	.long	.L264-.L285
	.long	.L290-.L285
	.long	.L267-.L285
	.long	.L338-.L285
	.long	.L338-.L285
	.long	.L266-.L285
	.long	.L290-.L285
	.text
.L290:
	xorl	%ebx, %ebx
.L275:
	movq	40(%r12), %rdi
	movl	$122, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_zalloc@PLT
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	je	.L277
	xorl	%esi, %esi
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	asn1_do_lock@PLT
	testl	%eax, %eax
	js	.L340
	movq	%r12, %rsi
	movq	%r13, %rdi
	xorl	%r15d, %r15d
	call	asn1_enc_init@PLT
	cmpq	$0, 24(%r12)
	movq	16(%r12), %r14
	jg	.L278
	.p2align 4,,10
	.p2align 3
.L281:
	testq	%rbx, %rbx
	jne	.L337
	jmp	.L338
.L266:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	asn1_primitive_new
	testl	%eax, %eax
	jne	.L338
.L277:
	movl	$152, %r8d
	movl	$65, %edx
	movl	$121, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L264:
	.cfi_restore_state
	movq	16(%r12), %rax
	testq	%rax, %rax
	je	.L266
	movq	32(%rax), %rdx
	movq	(%rax), %rsi
	movq	%r13, %rdi
	call	asn1_template_new.isra.0
	testl	%eax, %eax
	jne	.L338
	jmp	.L277
.L273:
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	*%rbx
	testl	%eax, %eax
	je	.L271
	cmpl	$2, %eax
	jne	.L275
	jmp	.L338
.L269:
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	*%rbx
	testl	%eax, %eax
	je	.L271
	cmpl	$2, %eax
	je	.L338
	movq	40(%r12), %rdi
	movl	$97, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_zalloc@PLT
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	je	.L277
	movq	%r12, %rdx
	movl	$-1, %esi
	movq	%r13, %rdi
	call	asn1_set_choice_selector@PLT
.L337:
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r13, %rsi
	movl	$1, %edi
	call	*%rbx
	testl	%eax, %eax
	jne	.L338
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	asn1_item_embed_free@PLT
.L271:
	movl	$161, %r8d
	movl	$100, %edx
	movl	$121, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L259:
	.cfi_restore_state
	movq	8(%rcx), %rax
	testq	%rax, %rax
	je	.L338
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L338
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L280:
	addq	$40, %r14
	addq	$1, %r15
	cmpq	%r15, 24(%r12)
	jle	.L281
.L278:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	asn1_get_field_ptr@PLT
	movq	32(%r14), %rdx
	movq	(%r14), %rsi
	movq	%rax, %rdi
	call	asn1_template_new.isra.0
	testl	%eax, %eax
	jne	.L280
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	asn1_item_embed_free@PLT
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L340:
	movq	0(%r13), %rdi
	movl	$129, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 0(%r13)
	jmp	.L277
	.cfi_endproc
.LFE455:
	.size	ASN1_item_ex_new, .-ASN1_item_ex_new
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
