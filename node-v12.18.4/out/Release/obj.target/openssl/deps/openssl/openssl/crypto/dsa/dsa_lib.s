	.file	"dsa_lib.c"
	.text
	.p2align 4
	.globl	DSA_set_method
	.type	DSA_set_method, @function
DSA_set_method:
.LFB806:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	80(%rdi), %rax
	movq	%rsi, %rbx
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L2
	call	*%rax
.L2:
	movq	88(%r12), %rdi
	call	ENGINE_finish@PLT
	movq	48(%rbx), %rax
	movq	%rbx, 80(%r12)
	movq	$0, 88(%r12)
	testq	%rax, %rax
	je	.L3
	movq	%r12, %rdi
	call	*%rax
.L3:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE806:
	.size	DSA_set_method, .-DSA_set_method
	.p2align 4
	.globl	DSA_get_method
	.type	DSA_get_method, @function
DSA_get_method:
.LFB807:
	.cfi_startproc
	endbr64
	movq	80(%rdi), %rax
	ret
	.cfi_endproc
.LFE807:
	.size	DSA_get_method, .-DSA_get_method
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/dsa/dsa_lib.c"
	.text
	.p2align 4
	.globl	DSA_free
	.type	DSA_free, @function
DSA_free:
.LFB809:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L27
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	lock xaddl	%eax, 64(%rdi)
	subl	$1, %eax
	testl	%eax, %eax
	je	.L30
	jle	.L17
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
.L17:
	movq	80(%r12), %rax
	testq	%rax, %rax
	je	.L19
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L19
	movq	%r12, %rdi
	call	*%rax
.L19:
	movq	88(%r12), %rdi
	call	ENGINE_finish@PLT
	leaq	72(%r12), %rdx
	movq	%r12, %rsi
	movl	$7, %edi
	call	CRYPTO_free_ex_data@PLT
	movq	96(%r12), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	movq	8(%r12), %rdi
	call	BN_clear_free@PLT
	movq	16(%r12), %rdi
	call	BN_clear_free@PLT
	movq	24(%r12), %rdi
	call	BN_clear_free@PLT
	movq	32(%r12), %rdi
	call	BN_clear_free@PLT
	movq	40(%r12), %rdi
	call	BN_clear_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$131, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L27:
	ret
	.cfi_endproc
.LFE809:
	.size	DSA_free, .-DSA_free
	.p2align 4
	.globl	DSA_new_method
	.type	DSA_new_method, @function
DSA_new_method:
.LFB808:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$51, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$104, %edi
	pushq	%r12
	.cfi_offset 12, -32
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L49
	movl	$1, 64(%rax)
	mfence
	call	CRYPTO_THREAD_lock_new@PLT
	movq	%rax, 96(%r12)
	testq	%rax, %rax
	je	.L50
	call	DSA_get_default_method@PLT
	movq	%rax, 80(%r12)
	movl	64(%rax), %eax
	andb	$-5, %ah
	movl	%eax, 48(%r12)
	testq	%r13, %r13
	je	.L35
	movq	%r13, %rdi
	call	ENGINE_init@PLT
	movl	$71, %r8d
	testl	%eax, %eax
	je	.L48
	movq	%r13, 88(%r12)
.L38:
	movq	%r13, %rdi
	call	ENGINE_get_DSA@PLT
	movq	%rax, 80(%r12)
	testq	%rax, %rax
	je	.L51
.L40:
	movl	64(%rax), %eax
	leaq	72(%r12), %rdx
	movq	%r12, %rsi
	movl	$7, %edi
	andb	$-5, %ah
	movl	%eax, 48(%r12)
	call	CRYPTO_new_ex_data@PLT
	testl	%eax, %eax
	je	.L37
	movq	80(%r12), %rax
	movq	48(%rax), %rax
	testq	%rax, %rax
	je	.L31
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L52
.L31:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	movl	$80, %r8d
.L48:
	movl	$38, %edx
	movl	$103, %esi
	movl	$10, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L37:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	DSA_free
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	call	ENGINE_get_default_DSA@PLT
	movq	%rax, 88(%r12)
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L38
	movq	80(%r12), %rax
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L49:
	movl	$54, %r8d
	movl	$65, %edx
	movl	$103, %esi
	movl	$10, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L50:
	movl	$61, %r8d
	movl	$65, %edx
	movl	$103, %esi
	movl	$10, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movl	$62, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L52:
	movl	$92, %r8d
	movl	$70, %edx
	movl	$103, %esi
	movl	$10, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L37
	.cfi_endproc
.LFE808:
	.size	DSA_new_method, .-DSA_new_method
	.p2align 4
	.globl	DSA_new
	.type	DSA_new, @function
DSA_new:
.LFB805:
	.cfi_startproc
	endbr64
	xorl	%edi, %edi
	jmp	DSA_new_method
	.cfi_endproc
.LFE805:
	.size	DSA_new, .-DSA_new
	.p2align 4
	.globl	DSA_up_ref
	.type	DSA_up_ref, @function
DSA_up_ref:
.LFB810:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	lock xaddl	%eax, 64(%rdi)
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE810:
	.size	DSA_up_ref, .-DSA_up_ref
	.p2align 4
	.globl	DSA_size
	.type	DSA_size, @function
DSA_size:
.LFB811:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	call	BN_num_bits@PLT
	leaq	-48(%rbp), %rdi
	movl	$2, -44(%rbp)
	leal	14(%rax), %edx
	addl	$7, %eax
	movb	$-1, -12(%rbp)
	cmovs	%edx, %eax
	xorl	%esi, %esi
	sarl	$3, %eax
	movl	%eax, -48(%rbp)
	leaq	-12(%rbp), %rax
	movq	%rax, -40(%rbp)
	call	i2d_ASN1_INTEGER@PLT
	movl	$16, %edx
	movl	$1, %edi
	leal	(%rax,%rax), %esi
	call	ASN1_object_size@PLT
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L58
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L58:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE811:
	.size	DSA_size, .-DSA_size
	.p2align 4
	.globl	DSA_set_ex_data
	.type	DSA_set_ex_data, @function
DSA_set_ex_data:
.LFB812:
	.cfi_startproc
	endbr64
	addq	$72, %rdi
	jmp	CRYPTO_set_ex_data@PLT
	.cfi_endproc
.LFE812:
	.size	DSA_set_ex_data, .-DSA_set_ex_data
	.p2align 4
	.globl	DSA_get_ex_data
	.type	DSA_get_ex_data, @function
DSA_get_ex_data:
.LFB813:
	.cfi_startproc
	endbr64
	addq	$72, %rdi
	jmp	CRYPTO_get_ex_data@PLT
	.cfi_endproc
.LFE813:
	.size	DSA_get_ex_data, .-DSA_get_ex_data
	.p2align 4
	.globl	DSA_security_bits
	.type	DSA_security_bits, @function
DSA_security_bits:
.LFB814:
	.cfi_startproc
	endbr64
	cmpq	$0, 8(%rdi)
	je	.L67
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L61
	call	BN_num_bits@PLT
	movq	8(%rbx), %rdi
	movl	%eax, %r12d
	call	BN_num_bits@PLT
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	movl	%r12d, %esi
	popq	%r12
	.cfi_restore 12
	movl	%eax, %edi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	BN_security_bits@PLT
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	popq	%rbx
	movl	$-1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
.L67:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	orl	$-1, %eax
	ret
	.cfi_endproc
.LFE814:
	.size	DSA_security_bits, .-DSA_security_bits
	.p2align 4
	.globl	DSA_dup_DH
	.type	DSA_dup_DH, @function
DSA_dup_DH:
.LFB815:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdi, %rdi
	je	.L73
	movq	%rdi, %rbx
	call	DH_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L73
	movq	8(%rbx), %r13
	movq	24(%rbx), %r12
	testq	%r13, %r13
	je	.L98
	testq	%r12, %r12
	je	.L83
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L83
	movq	%r13, %rdi
	call	BN_dup@PLT
	movq	24(%rbx), %rdi
	movq	%rax, %r13
	call	BN_dup@PLT
	movq	16(%rbx), %rdi
	movq	%rax, %r15
	call	BN_dup@PLT
	movq	%rax, %r12
	testq	%r13, %r13
	je	.L86
	testq	%r15, %r15
	je	.L86
	testq	%rax, %rax
	je	.L86
	movq	%r15, %rcx
	movq	%rax, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	DH_set0_pqg@PLT
	testl	%eax, %eax
	je	.L86
.L77:
	movq	32(%rbx), %r13
	testq	%r13, %r13
	je	.L99
	movq	%r13, %rdi
	call	BN_dup@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L87
	movq	40(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L78
	movq	%rbx, %rdi
	movq	%rax, -56(%rbp)
	call	BN_dup@PLT
	movq	-56(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L89
.L78:
	movq	%r8, %rsi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	movq	%r8, -56(%rbp)
	call	DH_set0_key@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	jne	.L70
.L89:
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L86:
	xorl	%ebx, %ebx
	xorl	%r8d, %r8d
.L72:
	movq	%r13, %rdi
	movq	%r8, -56(%rbp)
	call	BN_free@PLT
	movq	%r15, %rdi
	call	BN_free@PLT
	movq	%r12, %rdi
	call	BN_free@PLT
	movq	-56(%rbp), %r8
	movq	%r8, %rdi
	call	BN_free@PLT
	movq	%rbx, %rdi
	call	BN_free@PLT
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	DH_free@PLT
.L70:
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L100
.L80:
	xorl	%ebx, %ebx
	xorl	%r8d, %r8d
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L73:
	xorl	%ebx, %ebx
	xorl	%r8d, %r8d
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L99:
	cmpq	$0, 40(%rbx)
	je	.L70
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L100:
	cmpq	$0, 16(%rbx)
	je	.L77
	xorl	%ebx, %ebx
	xorl	%r8d, %r8d
	xorl	%r15d, %r15d
	movq	%r12, %r13
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L83:
	xorl	%ebx, %ebx
	xorl	%r8d, %r8d
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L87:
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	jmp	.L72
	.cfi_endproc
.LFE815:
	.size	DSA_dup_DH, .-DSA_dup_DH
	.p2align 4
	.globl	DSA_get0_pqg
	.type	DSA_get0_pqg, @function
DSA_get0_pqg:
.LFB816:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L102
	movq	8(%rdi), %rax
	movq	%rax, (%rsi)
.L102:
	testq	%rdx, %rdx
	je	.L103
	movq	16(%rdi), %rax
	movq	%rax, (%rdx)
.L103:
	testq	%rcx, %rcx
	je	.L101
	movq	24(%rdi), %rax
	movq	%rax, (%rcx)
.L101:
	ret
	.cfi_endproc
.LFE816:
	.size	DSA_get0_pqg, .-DSA_get0_pqg
	.p2align 4
	.globl	DSA_set0_pqg
	.type	DSA_set0_pqg, @function
DSA_set0_pqg:
.LFB817:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	movq	%rdi, %rax
	orq	%rsi, %rax
	je	.L120
	movq	%rdx, %rax
	orq	16(%rbx), %rax
	movq	%rdx, %r13
	je	.L120
	movq	%rcx, %rax
	orq	24(%rbx), %rax
	movq	%rcx, %r14
	je	.L120
	movq	%rsi, %r12
	testq	%rsi, %rsi
	je	.L116
	call	BN_free@PLT
	movq	%r12, 8(%rbx)
.L116:
	testq	%r13, %r13
	je	.L117
	movq	16(%rbx), %rdi
	call	BN_free@PLT
	movq	%r13, 16(%rbx)
.L117:
	movl	$1, %r12d
	testq	%r14, %r14
	je	.L114
	movq	24(%rbx), %rdi
	call	BN_free@PLT
	movq	%r14, 24(%rbx)
.L114:
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore_state
	xorl	%r12d, %r12d
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE817:
	.size	DSA_set0_pqg, .-DSA_set0_pqg
	.p2align 4
	.globl	DSA_get0_key
	.type	DSA_get0_key, @function
DSA_get0_key:
.LFB818:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L131
	movq	32(%rdi), %rax
	movq	%rax, (%rsi)
.L131:
	testq	%rdx, %rdx
	je	.L130
	movq	40(%rdi), %rax
	movq	%rax, (%rdx)
.L130:
	ret
	.cfi_endproc
.LFE818:
	.size	DSA_get0_key, .-DSA_get0_key
	.p2align 4
	.globl	DSA_set0_key
	.type	DSA_set0_key, @function
DSA_set0_key:
.LFB819:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L151
	testq	%rsi, %rsi
	jne	.L142
.L143:
	movl	$1, %r12d
	testq	%r13, %r13
	je	.L139
	movq	40(%rbx), %rdi
	call	BN_free@PLT
	movq	%r13, 40(%rbx)
.L139:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L152
.L142:
	call	BN_free@PLT
	movq	%r12, 32(%rbx)
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L152:
	xorl	%r12d, %r12d
	jmp	.L139
	.cfi_endproc
.LFE819:
	.size	DSA_set0_key, .-DSA_set0_key
	.p2align 4
	.globl	DSA_get0_p
	.type	DSA_get0_p, @function
DSA_get0_p:
.LFB820:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE820:
	.size	DSA_get0_p, .-DSA_get0_p
	.p2align 4
	.globl	DSA_get0_q
	.type	DSA_get0_q, @function
DSA_get0_q:
.LFB821:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE821:
	.size	DSA_get0_q, .-DSA_get0_q
	.p2align 4
	.globl	DSA_get0_g
	.type	DSA_get0_g, @function
DSA_get0_g:
.LFB822:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE822:
	.size	DSA_get0_g, .-DSA_get0_g
	.p2align 4
	.globl	DSA_get0_pub_key
	.type	DSA_get0_pub_key, @function
DSA_get0_pub_key:
.LFB823:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE823:
	.size	DSA_get0_pub_key, .-DSA_get0_pub_key
	.p2align 4
	.globl	DSA_get0_priv_key
	.type	DSA_get0_priv_key, @function
DSA_get0_priv_key:
.LFB824:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE824:
	.size	DSA_get0_priv_key, .-DSA_get0_priv_key
	.p2align 4
	.globl	DSA_clear_flags
	.type	DSA_clear_flags, @function
DSA_clear_flags:
.LFB825:
	.cfi_startproc
	endbr64
	notl	%esi
	andl	%esi, 48(%rdi)
	ret
	.cfi_endproc
.LFE825:
	.size	DSA_clear_flags, .-DSA_clear_flags
	.p2align 4
	.globl	DSA_test_flags
	.type	DSA_test_flags, @function
DSA_test_flags:
.LFB826:
	.cfi_startproc
	endbr64
	movl	48(%rdi), %eax
	andl	%esi, %eax
	ret
	.cfi_endproc
.LFE826:
	.size	DSA_test_flags, .-DSA_test_flags
	.p2align 4
	.globl	DSA_set_flags
	.type	DSA_set_flags, @function
DSA_set_flags:
.LFB827:
	.cfi_startproc
	endbr64
	orl	%esi, 48(%rdi)
	ret
	.cfi_endproc
.LFE827:
	.size	DSA_set_flags, .-DSA_set_flags
	.p2align 4
	.globl	DSA_get0_engine
	.type	DSA_get0_engine, @function
DSA_get0_engine:
.LFB828:
	.cfi_startproc
	endbr64
	movq	88(%rdi), %rax
	ret
	.cfi_endproc
.LFE828:
	.size	DSA_get0_engine, .-DSA_get0_engine
	.p2align 4
	.globl	DSA_bits
	.type	DSA_bits, @function
DSA_bits:
.LFB829:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	jmp	BN_num_bits@PLT
	.cfi_endproc
.LFE829:
	.size	DSA_bits, .-DSA_bits
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
