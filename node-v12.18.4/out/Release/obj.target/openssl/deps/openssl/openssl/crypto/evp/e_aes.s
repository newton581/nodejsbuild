	.file	"e_aes.c"
	.text
	.p2align 4
	.type	aes_xts_ctrl, @function
aes_xts_ctrl:
.LFB477:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$8, %rsp
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%rax, %r12
	cmpl	$8, %ebx
	je	.L14
	movl	$-1, %eax
	testl	%ebx, %ebx
	jne	.L1
	pxor	%xmm0, %xmm0
	movl	$1, %eax
	movups	%xmm0, 496(%r12)
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	496(%r12), %rdx
	testq	%rdx, %rdx
	je	.L3
	cmpq	%r12, %rdx
	je	.L4
.L6:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	movq	%rax, 496(%rax)
.L3:
	movq	504(%r12), %rdx
	testq	%rdx, %rdx
	je	.L7
	addq	$248, %r12
	cmpq	%r12, %rdx
	jne	.L6
	leaq	248(%rax), %rdx
	movq	%rdx, 504(%rax)
	movl	$1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L7:
	movl	$1, %eax
	jmp	.L1
	.cfi_endproc
.LFE477:
	.size	aes_xts_ctrl, .-aes_xts_ctrl
	.p2align 4
	.type	aes_cbc_cipher, @function
aes_cbc_cipher:
.LFB441:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	256(%rax), %r10
	movq	%rax, %r13
	testq	%r10, %r10
	movq	%r10, -56(%rbp)
	je	.L16
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r12, %rdi
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movl	-60(%rbp), %r9d
	movq	-56(%rbp), %r10
	movq	%r13, %rcx
	movq	%rax, %r8
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	*%r10
.L17:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	248(%r13), %r9
	movq	%r12, %rdi
	movq	%r9, -56(%rbp)
	testl	%eax, %eax
	je	.L18
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	-56(%rbp), %r9
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%rax, %r8
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	CRYPTO_cbc128_encrypt@PLT
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L18:
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	-56(%rbp), %r9
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%rax, %r8
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	CRYPTO_cbc128_decrypt@PLT
	jmp	.L17
	.cfi_endproc
.LFE441:
	.size	aes_cbc_cipher, .-aes_cbc_cipher
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/evp/e_aes.c"
	.text
	.p2align 4
	.type	aes_init_key, @function
aes_init_key:
.LFB440:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	movq	%r12, %rdi
	movl	%eax, %r13d
	andl	$983047, %r13d
	leal	-1(%r13), %eax
	cmpl	$1, %eax
	ja	.L21
	testl	%r15d, %r15d
	je	.L43
.L21:
	testb	$2, 5+OPENSSL_ia32cap_P(%rip)
	je	.L26
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%rbx, %rdx
	movq	%r14, %rdi
	leal	0(,%rax,8), %esi
	call	vpaes_set_encrypt_key@PLT
	movq	vpaes_encrypt@GOTPCREL(%rip), %rdx
	movq	%rdx, 248(%rbx)
	cmpl	$2, %r13d
	je	.L32
.L42:
	xorl	%edx, %edx
.L28:
	movq	%rdx, 256(%rbx)
	movl	$1, %r8d
	testl	%eax, %eax
	js	.L44
.L20:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%rbx, %rdx
	movq	%r14, %rdi
	leal	0(,%rax,8), %esi
	call	AES_set_encrypt_key@PLT
	movq	AES_encrypt@GOTPCREL(%rip), %rdx
	movq	%rdx, 248(%rbx)
	cmpl	$2, %r13d
	jne	.L42
.L33:
	movq	AES_cbc_encrypt@GOTPCREL(%rip), %rdx
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L43:
	testb	$2, 5+OPENSSL_ia32cap_P(%rip)
	je	.L22
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%rbx, %rdx
	movq	%r14, %rdi
	leal	0(,%rax,8), %esi
	call	vpaes_set_decrypt_key@PLT
	movq	vpaes_decrypt@GOTPCREL(%rip), %rdx
	movq	%rdx, 248(%rbx)
	cmpl	$2, %r13d
	jne	.L42
.L32:
	movq	vpaes_cbc_encrypt@GOTPCREL(%rip), %rdx
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L22:
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%rbx, %rdx
	movq	%r14, %rdi
	leal	0(,%rax,8), %esi
	call	AES_set_decrypt_key@PLT
	movq	AES_decrypt@GOTPCREL(%rip), %rdx
	movq	%rdx, 248(%rbx)
	cmpl	$2, %r13d
	jne	.L42
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L44:
	movl	$2706, %r8d
	movl	$143, %edx
	movl	$133, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r8d, %r8d
	jmp	.L20
	.cfi_endproc
.LFE440:
	.size	aes_init_key, .-aes_init_key
	.p2align 4
	.type	aesni_cbc_cipher, @function
aesni_cbc_cipher:
.LFB434:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r12, %rdi
	movl	%eax, -52(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movl	-52(%rbp), %r9d
	movq	%rbx, %r8
	movq	%r15, %rdx
	movq	%rax, %rcx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	aesni_cbc_encrypt@PLT
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE434:
	.size	aesni_cbc_cipher, .-aesni_cbc_cipher
	.p2align 4
	.type	aesni_init_key, @function
aesni_init_key:
.LFB433:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	movq	%r12, %rdi
	movl	%eax, %r13d
	andl	$983047, %r13d
	leal	-1(%r13), %eax
	cmpl	$1, %eax
	ja	.L48
	testl	%r15d, %r15d
	je	.L64
.L48:
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%rbx, %rdx
	movq	%r14, %rdi
	leal	0(,%rax,8), %esi
	call	aesni_set_encrypt_key@PLT
	movq	aesni_encrypt@GOTPCREL(%rip), %rdx
	movq	%rdx, 248(%rbx)
	cmpl	$2, %r13d
	je	.L54
	cmpl	$5, %r13d
	je	.L65
	movq	$0, 256(%rbx)
	movl	$1, %r8d
	testl	%eax, %eax
	js	.L66
.L47:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%rbx, %rdx
	movq	%r14, %rdi
	leal	0(,%rax,8), %esi
	call	aesni_set_decrypt_key@PLT
	movq	aesni_decrypt@GOTPCREL(%rip), %rdx
	movq	%rdx, 248(%rbx)
	xorl	%edx, %edx
	cmpl	$2, %r13d
	jne	.L63
	.p2align 4,,10
	.p2align 3
.L54:
	movq	aesni_cbc_encrypt@GOTPCREL(%rip), %rdx
.L63:
	movq	%rdx, 256(%rbx)
	movl	$1, %r8d
	testl	%eax, %eax
	jns	.L47
.L66:
	movl	$299, %r8d
	movl	$143, %edx
	movl	$165, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r8d, %r8d
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L65:
	movq	aesni_ctr32_encrypt_blocks@GOTPCREL(%rip), %rdx
	jmp	.L63
	.cfi_endproc
.LFE433:
	.size	aesni_init_key, .-aesni_init_key
	.p2align 4
	.type	aes_ecb_cipher, @function
aes_ecb_cipher:
.LFB442:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -56(%rbp)
	call	EVP_CIPHER_CTX_block_size@PLT
	movq	%r15, %rdi
	movslq	%eax, %r12
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %r15
	movq	%rcx, %rax
	subq	%r12, %rax
	movq	%rax, -56(%rbp)
	cmpq	%rcx, %r12
	ja	.L73
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L69:
	leaq	(%r14,%rbx), %rsi
	leaq	0(%r13,%rbx), %rdi
	movq	%r15, %rdx
	addq	%r12, %rbx
	call	*248(%r15)
	cmpq	%rbx, -56(%rbp)
	jnb	.L69
.L73:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE442:
	.size	aes_ecb_cipher, .-aes_ecb_cipher
	.p2align 4
	.type	aes_ofb_cipher, @function
aes_ofb_cipher:
.LFB443:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	EVP_CIPHER_CTX_num@PLT
	movq	248(%r13), %rdx
	movq	%r12, %rdi
	movl	%eax, -60(%rbp)
	movq	%rdx, -72(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	-72(%rbp), %rdx
	subq	$8, %rsp
	movq	%r13, %rcx
	movq	%rax, %r8
	leaq	-60(%rbp), %r9
	movq	%r15, %rsi
	movq	%r14, %rdi
	pushq	%rdx
	movq	%rbx, %rdx
	call	CRYPTO_ofb128_encrypt@PLT
	movl	-60(%rbp), %esi
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L78
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L78:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE443:
	.size	aes_ofb_cipher, .-aes_ofb_cipher
	.p2align 4
	.type	aes_cfb_cipher, @function
aes_cfb_cipher:
.LFB444:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	EVP_CIPHER_CTX_num@PLT
	movq	248(%r13), %rcx
	movq	%r12, %rdi
	movl	%eax, -60(%rbp)
	movq	%rcx, -80(%rbp)
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r12, %rdi
	movl	%eax, -68(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	-80(%rbp), %rcx
	movl	-68(%rbp), %edx
	leaq	-60(%rbp), %r9
	movq	%rax, %r8
	movq	%r15, %rsi
	movq	%r14, %rdi
	pushq	%rcx
	movq	%r13, %rcx
	pushq	%rdx
	movq	%rbx, %rdx
	call	CRYPTO_cfb128_encrypt@PLT
	movl	-60(%rbp), %esi
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L82
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L82:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE444:
	.size	aes_cfb_cipher, .-aes_cfb_cipher
	.p2align 4
	.type	aes_cfb1_cipher, @function
aes_cfb1_cipher:
.LFB446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$72, %rsp
	movq	%rdx, -104(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movl	$8192, %esi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	EVP_CIPHER_CTX_test_flags@PLT
	testl	%eax, %eax
	jne	.L84
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rbx
	jbe	.L96
	movabsq	$-1152921504606846976, %rax
	addq	-96(%rbp), %rax
	movq	-104(%rbp), %rbx
	movq	%r15, %r13
	shrq	$60, %rax
	addq	$1, %rax
	salq	$60, %rax
	movq	%rax, -112(%rbp)
	addq	%r15, %rax
	movq	%rax, -80(%rbp)
	leaq	-60(%rbp), %rax
	movq	%rax, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L88:
	movq	%r14, %rdi
	call	EVP_CIPHER_CTX_num@PLT
	movq	248(%r12), %rcx
	movq	%r14, %rdi
	movl	%eax, -60(%rbp)
	movq	%rcx, -72(%rbp)
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r14, %rdi
	movl	%eax, %r15d
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	-72(%rbp), %rcx
	movq	-88(%rbp), %r9
	movq	%r13, %rsi
	movabsq	$-9223372036854775808, %rdx
	movq	%rax, %r8
	movq	%rbx, %rdi
	pushq	%rcx
	movq	%r12, %rcx
	pushq	%r15
	call	CRYPTO_cfb128_1_encrypt@PLT
	movl	-60(%rbp), %esi
	movq	%r14, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	popq	%rcx
	popq	%rsi
	movabsq	$1152921504606846976, %rax
	addq	%rax, %r13
	addq	%rax, %rbx
	cmpq	-80(%rbp), %r13
	jne	.L88
	movq	-112(%rbp), %rdx
	subq	$1, %rax
	addq	%rdx, -104(%rbp)
	andq	%rax, -96(%rbp)
.L86:
	cmpq	$0, -96(%rbp)
	jne	.L97
.L87:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L98
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	.cfi_restore_state
	movq	%r14, %rdi
	call	EVP_CIPHER_CTX_num@PLT
	movq	%r14, %rdi
	movq	248(%r12), %r13
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r14, %rdi
	movl	%eax, %ebx
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	pushq	%r13
	movq	-96(%rbp), %rdx
	leaq	-60(%rbp), %r9
	pushq	%rbx
	movq	-80(%rbp), %rsi
	movq	%rax, %r8
	movq	%r12, %rcx
	salq	$3, %rdx
.L95:
	movq	-104(%rbp), %rdi
	call	CRYPTO_cfb128_1_encrypt@PLT
	movl	-60(%rbp), %esi
	movq	%r14, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	popq	%rax
	popq	%rdx
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L84:
	movq	%r14, %rdi
	call	EVP_CIPHER_CTX_num@PLT
	movq	%r14, %rdi
	movq	248(%r12), %r13
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r14, %rdi
	movl	%eax, %ebx
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	pushq	%r13
	movq	-96(%rbp), %rdx
	leaq	-60(%rbp), %r9
	pushq	%rbx
	movq	%rax, %r8
	movq	%r12, %rcx
	movq	%r15, %rsi
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L96:
	movq	%r15, -80(%rbp)
	jmp	.L86
.L98:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE446:
	.size	aes_cfb1_cipher, .-aes_cfb1_cipher
	.p2align 4
	.type	aes_cfb8_cipher, @function
aes_cfb8_cipher:
.LFB445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	EVP_CIPHER_CTX_num@PLT
	movq	248(%r13), %rcx
	movq	%r12, %rdi
	movl	%eax, -60(%rbp)
	movq	%rcx, -80(%rbp)
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r12, %rdi
	movl	%eax, -68(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	-80(%rbp), %rcx
	movl	-68(%rbp), %edx
	leaq	-60(%rbp), %r9
	movq	%rax, %r8
	movq	%r15, %rsi
	movq	%r14, %rdi
	pushq	%rcx
	movq	%r13, %rcx
	pushq	%rdx
	movq	%rbx, %rdx
	call	CRYPTO_cfb128_8_encrypt@PLT
	movl	-60(%rbp), %esi
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L102
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L102:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE445:
	.size	aes_cfb8_cipher, .-aes_cfb8_cipher
	.p2align 4
	.type	aes_ctr_cipher, @function
aes_ctr_cipher:
.LFB447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_CIPHER_CTX_num@PLT
	movq	%r12, %rdi
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	256(%rax), %rdx
	movq	%rax, %r13
	testq	%rdx, %rdx
	movq	%rdx, -72(%rbp)
	je	.L104
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_buf_noconst@PLT
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %r9
	movq	%r13, %rcx
	movq	%rax, %r8
	leaq	-60(%rbp), %rax
	movq	%r15, %rsi
	movq	%r14, %rdi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	%rax
	call	CRYPTO_ctr128_encrypt_ctr32@PLT
	popq	%rcx
	popq	%rsi
.L105:
	movl	-60(%rbp), %esi
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L108
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	movq	248(%rax), %rdx
	movq	%r12, %rdi
	movq	%rdx, -80(%rbp)
	call	EVP_CIPHER_CTX_buf_noconst@PLT
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	-80(%rbp), %rdx
	movq	-72(%rbp), %r9
	movq	%r13, %rcx
	movq	%rax, %r8
	leaq	-60(%rbp), %rax
	movq	%r15, %rsi
	movq	%r14, %rdi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	%rax
	call	CRYPTO_ctr128_encrypt@PLT
	popq	%rax
	popq	%rdx
	jmp	.L105
.L108:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE447:
	.size	aes_ctr_cipher, .-aes_ctr_cipher
	.p2align 4
	.type	aes_gcm_ctrl, @function
aes_gcm_ctrl:
.LFB470:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$8, %rsp
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	cmpl	$37, %ebx
	ja	.L149
	leaq	.L112(%rip), %rdx
	movq	%rax, %r15
	movslq	(%rdx,%rbx,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L112:
	.long	.L121-.L112
	.long	.L149-.L112
	.long	.L149-.L112
	.long	.L149-.L112
	.long	.L149-.L112
	.long	.L149-.L112
	.long	.L149-.L112
	.long	.L149-.L112
	.long	.L120-.L112
	.long	.L119-.L112
	.long	.L149-.L112
	.long	.L149-.L112
	.long	.L149-.L112
	.long	.L149-.L112
	.long	.L149-.L112
	.long	.L149-.L112
	.long	.L118-.L112
	.long	.L117-.L112
	.long	.L116-.L112
	.long	.L115-.L112
	.long	.L149-.L112
	.long	.L149-.L112
	.long	.L114-.L112
	.long	.L149-.L112
	.long	.L113-.L112
	.long	.L149-.L112
	.long	.L149-.L112
	.long	.L149-.L112
	.long	.L149-.L112
	.long	.L149-.L112
	.long	.L149-.L112
	.long	.L149-.L112
	.long	.L149-.L112
	.long	.L149-.L112
	.long	.L149-.L112
	.long	.L149-.L112
	.long	.L149-.L112
	.long	.L111-.L112
	.text
	.p2align 4,,10
	.p2align 3
.L149:
	movl	$-1, %eax
.L109:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	movl	704(%r15), %eax
	movl	%eax, (%r14)
	movl	$1, %eax
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L121:
	movq	$0, 248(%r15)
	movq	(%r12), %rdi
	addq	$40, %r12
	call	EVP_CIPHER_iv_length@PLT
	movq	%r12, 696(%r15)
	movl	%eax, 704(%r15)
	movabsq	$-4294967296, %rax
	movq	%rax, 712(%r15)
	movl	$1, %eax
	movl	$-1, 708(%r15)
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L120:
	movq	%r14, %rdi
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%rax, %rbx
	movq	640(%r15), %rax
	testq	%rax, %rax
	je	.L146
	cmpq	%r15, %rax
	je	.L174
	.p2align 4,,10
	.p2align 3
.L169:
	xorl	%eax, %eax
.L175:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	.cfi_restore_state
	testl	%r13d, %r13d
	jle	.L169
	cmpl	$16, %r13d
	jle	.L123
	cmpl	%r13d, 704(%r15)
	jge	.L123
	movq	696(%r15), %rdi
	addq	$40, %r12
	cmpq	%r12, %rdi
	je	.L124
	movl	$2880, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
.L124:
	movslq	%r13d, %rdi
	movl	$2881, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movl	$2882, %r8d
	movq	%rax, 696(%r15)
	testq	%rax, %rax
	jne	.L123
.L170:
	movl	$65, %edx
	movl	$196, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L123:
	movl	%r13d, 704(%r15)
	movl	$1, %eax
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L118:
	leal	-1(%r13), %eax
	cmpl	$15, %eax
	ja	.L169
	movl	16(%r12), %eax
	testl	%eax, %eax
	je	.L169
	movl	708(%r15), %ebx
	testl	%ebx, %ebx
	js	.L169
	leaq	56(%r12), %rax
	cmpl	$8, %r13d
	jnb	.L132
	testb	$4, %r13b
	jne	.L176
	testl	%r13d, %r13d
	je	.L173
	movzbl	56(%r12), %edx
	movb	%dl, (%r14)
	testb	$2, %r13b
	je	.L173
	movl	%r13d, %r13d
	movzwl	-2(%rax,%r13), %eax
	movw	%ax, -2(%r14,%r13)
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L117:
	leal	-1(%r13), %eax
	cmpl	$15, %eax
	ja	.L169
	movl	16(%r12), %eax
	testl	%eax, %eax
	jne	.L169
	leaq	56(%r12), %rax
	cmpl	$8, %r13d
	jnb	.L126
	testb	$4, %r13b
	jne	.L177
	testl	%r13d, %r13d
	je	.L127
	movzbl	(%r14), %edx
	movb	%dl, 56(%r12)
	testb	$2, %r13b
	jne	.L178
.L127:
	movl	%r13d, 708(%r15)
	movl	$1, %eax
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L116:
	cmpl	$-1, %r13d
	je	.L179
	cmpl	$3, %r13d
	jle	.L169
	movl	704(%r15), %eax
	subl	%r13d, %eax
	cmpl	$7, %eax
	jle	.L169
	movq	696(%r15), %rdi
	movslq	%r13d, %rbx
	movq	%r14, %rsi
	movq	%rbx, %rdx
	call	memcpy@PLT
	movl	16(%r12), %r11d
	testl	%r11d, %r11d
	jne	.L139
.L140:
	movl	$1, 712(%r15)
	movl	$1, %eax
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L115:
	movl	712(%r15), %r10d
	testl	%r10d, %r10d
	je	.L169
	movl	248(%r15), %r9d
	testl	%r9d, %r9d
	je	.L169
	movslq	704(%r15), %rdx
	movq	696(%r15), %rsi
	leaq	256(%r15), %rdi
	call	CRYPTO_gcm128_setiv@PLT
	movslq	704(%r15), %rdx
	testl	%r13d, %r13d
	jle	.L142
	cmpl	%r13d, %edx
	jl	.L142
	movslq	%edx, %rsi
	movslq	%r13d, %rdx
.L143:
	subq	%rdx, %rsi
	movq	%r14, %rdi
	addq	696(%r15), %rsi
	call	memcpy@PLT
	movslq	704(%r15), %rax
	movq	696(%r15), %rdx
	leaq	-1(%rdx,%rax), %rsi
	leaq	-8(%rax), %rdi
	addb	$1, (%rsi)
	jne	.L172
	leaq	-2(%rdx,%rax), %rsi
	addb	$1, (%rsi)
	jne	.L172
	leaq	-3(%rdx,%rax), %rsi
	addb	$1, (%rsi)
	jne	.L172
	leaq	-4(%rdx,%rax), %rsi
	addb	$1, (%rsi)
	jne	.L172
	leaq	-5(%rdx,%rax), %rsi
	addb	$1, (%rsi)
	jne	.L172
	leaq	-6(%rdx,%rax), %rsi
	addb	$1, (%rsi)
	jne	.L172
	leaq	-7(%rdx,%rax), %rcx
	addb	$1, (%rcx)
	jne	.L172
	addb	$1, (%rdx,%rdi)
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L114:
	cmpl	$13, %r13d
	jne	.L169
	movq	(%r14), %rdx
	movq	%rdx, 56(%r12)
	movl	8(%r14), %edx
	movl	%edx, 64(%r12)
	movzbl	12(%r14), %edx
	movb	%dl, 68(%r12)
	movl	$13, 716(%r15)
	movzwl	67(%r12), %eax
	rolw	$8, %ax
	cmpw	$7, %ax
	jbe	.L169
	movl	16(%r12), %ecx
	movzwl	%ax, %eax
	leal	-8(%rax), %edx
	testl	%ecx, %ecx
	jne	.L145
	cmpl	$15, %edx
	jbe	.L169
	leal	-24(%rax), %edx
.L145:
	rolw	$8, %dx
	movl	$16, %eax
	movw	%dx, 67(%r12)
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L113:
	movl	712(%r15), %r8d
	testl	%r8d, %r8d
	je	.L169
	movl	248(%r15), %edi
	testl	%edi, %edi
	je	.L169
	movl	16(%r12), %esi
	testl	%esi, %esi
	jne	.L169
	movslq	704(%r15), %rdi
	movslq	%r13d, %rdx
	movq	%r14, %rsi
	subq	%rdx, %rdi
	addq	696(%r15), %rdi
	call	memcpy@PLT
	movslq	704(%r15), %rdx
	movq	696(%r15), %rsi
	leaq	256(%r15), %rdi
	call	CRYPTO_gcm128_setiv@PLT
.L172:
	movl	$1, 252(%r15)
.L173:
	movl	$1, %eax
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L174:
	movq	%rbx, 640(%rbx)
.L146:
	addq	$40, %r12
	cmpq	%r12, 696(%r15)
	je	.L180
	movslq	704(%r15), %rdi
	movl	$2982, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, 696(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L181
	movslq	704(%r15), %rdx
	movq	696(%r15), %rsi
	call	memcpy@PLT
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L180:
	addq	$40, %r14
	movl	$1, %eax
	movq	%r14, 696(%rbx)
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L142:
	movslq	%edx, %rsi
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L126:
	movq	(%r14), %rdx
	addq	$64, %r12
	movq	%rdx, -8(%r12)
	movl	%r13d, %edx
	andq	$-8, %r12
	movq	-8(%r14,%rdx), %rcx
	movq	%rcx, -8(%rax,%rdx)
	subq	%r12, %rax
	subq	%rax, %r14
	addl	%r13d, %eax
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L127
	andl	$-8, %eax
	xorl	%edx, %edx
.L130:
	movl	%edx, %esi
	addl	$8, %edx
	movq	(%r14,%rsi), %rdi
	movq	%rdi, (%r12,%rsi)
	cmpl	%eax, %edx
	jb	.L130
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L132:
	movq	56(%r12), %rdx
	leaq	8(%r14), %rsi
	andq	$-8, %rsi
	movq	%rdx, (%r14)
	movl	%r13d, %edx
	movq	-8(%rax,%rdx), %rcx
	movq	%rcx, -8(%r14,%rdx)
	subq	%rsi, %r14
	addl	%r14d, %r13d
	subq	%r14, %rax
	andl	$-8, %r13d
	movq	%rax, %rdx
	cmpl	$8, %r13d
	jb	.L173
	andl	$-8, %r13d
	xorl	%eax, %eax
.L136:
	movl	%eax, %ecx
	addl	$8, %eax
	movq	(%rdx,%rcx), %rdi
	movq	%rdi, (%rsi,%rcx)
	cmpl	%r13d, %eax
	jb	.L136
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L179:
	movq	696(%r15), %rdi
	movslq	704(%r15), %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movl	$1, %eax
	movl	$1, 712(%r15)
	jmp	.L109
.L139:
	movl	704(%r15), %esi
	addq	696(%r15), %rbx
	movq	%rbx, %rdi
	subl	%r13d, %esi
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jg	.L140
	xorl	%eax, %eax
	jmp	.L175
.L181:
	movl	$2983, %r8d
	jmp	.L170
.L177:
	movl	(%r14), %edx
	movl	%edx, 56(%r12)
	movl	%r13d, %edx
	movl	-4(%r14,%rdx), %ecx
	movl	%ecx, -4(%rax,%rdx)
	jmp	.L127
.L176:
	movl	56(%r12), %edx
	movl	%r13d, %r13d
	movl	%edx, (%r14)
	movl	-4(%rax,%r13), %eax
	movl	%eax, -4(%r14,%r13)
	jmp	.L173
.L178:
	movl	%r13d, %edx
	movzwl	-2(%r14,%rdx), %ecx
	movw	%cx, -2(%rax,%rdx)
	jmp	.L127
	.cfi_endproc
.LFE470:
	.size	aes_gcm_ctrl, .-aes_gcm_ctrl
	.p2align 4
	.type	aes_gcm_cleanup, @function
aes_gcm_cleanup:
.LFB469:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	testq	%rax, %rax
	je	.L182
	movq	%rax, %rbx
	leaq	256(%rax), %rdi
	movl	$440, %esi
	movl	$1, %r13d
	call	OPENSSL_cleanse@PLT
	movq	696(%rbx), %r14
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	cmpq	%rax, %r14
	je	.L182
	movq	696(%rbx), %rdi
	movl	$2852, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
.L182:
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE469:
	.size	aes_gcm_cleanup, .-aes_gcm_cleanup
	.p2align 4
	.type	aes_gcm_cipher, @function
aes_gcm_cipher:
.LFB473:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movl	248(%rax), %r8d
	testl	%r8d, %r8d
	je	.L192
	movl	716(%rax), %edi
	movq	%rax, %r14
	testl	%edi, %edi
	jns	.L253
	movl	252(%rax), %ecx
	testl	%ecx, %ecx
	je	.L192
	testq	%r12, %r12
	je	.L206
	leaq	256(%rax), %r11
	testq	%r13, %r13
	je	.L254
	movl	16(%r15), %edx
	movq	720(%rax), %r8
	testl	%edx, %edx
	je	.L208
	movq	%rbx, %rcx
	testq	%r8, %r8
	je	.L209
	cmpq	$31, %rbx
	jbe	.L210
	movq	aesni_ctr32_encrypt_blocks@GOTPCREL(%rip), %rax
	cmpq	%rax, %r8
	je	.L255
.L210:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r11, %rdi
	call	CRYPTO_gcm128_encrypt_ctr32@PLT
	testl	%eax, %eax
	jne	.L192
.L211:
	movl	%ebx, %eax
.L258:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L253:
	.cfi_restore_state
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%rax, %r14
	cmpq	%r13, %r12
	jne	.L192
	cmpq	$23, %rbx
	jbe	.L192
	cmpl	$1, 16(%r15)
	movq	%r13, %rcx
	movl	$8, %edx
	movq	%r15, %rdi
	sbbl	%esi, %esi
	andl	$5, %esi
	addl	$19, %esi
	call	EVP_CIPHER_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L250
	leaq	56(%r15), %r10
	leaq	256(%r14), %r11
	movslq	716(%r14), %rdx
	movq	%r10, %rsi
	movq	%r11, %rdi
	movq	%r10, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	CRYPTO_gcm128_aad@PLT
	testl	%eax, %eax
	jne	.L250
	movl	16(%r15), %esi
	addq	$8, %r12
	addq	$8, %r13
	subq	$24, %rbx
	movq	720(%r14), %r8
	movq	-56(%rbp), %r11
	testl	%esi, %esi
	movq	-64(%rbp), %r10
	je	.L197
	testq	%r8, %r8
	je	.L198
	cmpq	$31, %rbx
	jbe	.L216
	movq	aesni_ctr32_encrypt_blocks@GOTPCREL(%rip), %rax
	cmpq	%rax, %r8
	je	.L256
.L216:
	movq	%r13, %rdx
	movq	%rbx, %rcx
.L199:
	movq	%r11, %rdi
	movq	%r12, %rsi
	movq	%r11, -56(%rbp)
	call	CRYPTO_gcm128_encrypt_ctr32@PLT
	movq	-56(%rbp), %r11
	testl	%eax, %eax
	jne	.L250
.L200:
	leaq	0(%r13,%rbx), %rsi
	movl	$16, %edx
	movq	%r11, %rdi
	call	CRYPTO_gcm128_tag@PLT
	leal	24(%rbx), %eax
	jmp	.L195
.L260:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	OPENSSL_cleanse@PLT
	.p2align 4,,10
	.p2align 3
.L250:
	movl	$-1, %eax
.L195:
	movl	$0, 252(%r14)
	movl	$-1, 716(%r14)
.L188:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L206:
	.cfi_restore_state
	movl	16(%r15), %eax
	testl	%eax, %eax
	jne	.L214
	movslq	708(%r14), %rdx
	testl	%edx, %edx
	js	.L192
	leaq	56(%r15), %rsi
	leaq	256(%r14), %rdi
	call	CRYPTO_gcm128_finish@PLT
	testl	%eax, %eax
	jne	.L192
	movl	$0, 252(%r14)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	.cfi_restore_state
	testq	%r8, %r8
	je	.L212
	movq	aesni_ctr32_encrypt_blocks@GOTPCREL(%rip), %rax
	movq	%rbx, %rcx
	cmpq	%rax, %r8
	jne	.L213
	cmpq	$15, %rbx
	ja	.L257
.L213:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r11, %rdi
	call	CRYPTO_gcm128_decrypt_ctr32@PLT
	testl	%eax, %eax
	jne	.L192
	movl	%ebx, %eax
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L254:
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r11, %rdi
	call	CRYPTO_gcm128_aad@PLT
	testl	%eax, %eax
	jne	.L192
	movl	%ebx, %eax
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L197:
	testq	%r8, %r8
	je	.L201
	cmpq	$15, %rbx
	jbe	.L218
	movq	aesni_ctr32_encrypt_blocks@GOTPCREL(%rip), %rax
	cmpq	%rax, %r8
	je	.L259
.L218:
	movq	%r12, %rsi
	movq	%r13, %rdx
	movq	%rbx, %rcx
.L202:
	movq	%r11, %rdi
	movq	%r10, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	CRYPTO_gcm128_decrypt_ctr32@PLT
	movq	-56(%rbp), %r11
	movq	-64(%rbp), %r10
	testl	%eax, %eax
	jne	.L250
.L205:
	movq	%r10, %rsi
	movl	$16, %edx
	movq	%r11, %rdi
	movq	%r10, -56(%rbp)
	call	CRYPTO_gcm128_tag@PLT
	movq	-56(%rbp), %r10
	leaq	(%r12,%rbx), %rsi
	movl	$16, %edx
	movq	%r10, %rdi
	call	CRYPTO_memcmp@PLT
	testl	%eax, %eax
	jne	.L260
	movl	%ebx, %eax
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L214:
	leaq	56(%r15), %rsi
	leaq	256(%r14), %rdi
	movl	$16, %edx
	call	CRYPTO_gcm128_tag@PLT
	xorl	%eax, %eax
	movl	$16, 708(%r14)
	movl	$0, 252(%r14)
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L212:
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r11, %rdi
	call	CRYPTO_gcm128_decrypt@PLT
	testl	%eax, %eax
	je	.L211
	.p2align 4,,10
	.p2align 3
.L192:
	movl	$-1, %eax
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L209:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r11, %rdi
	call	CRYPTO_gcm128_encrypt@PLT
	testl	%eax, %eax
	jne	.L192
	movl	%ebx, %eax
	jmp	.L258
.L198:
	movq	%r11, %rdi
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r11, -56(%rbp)
	call	CRYPTO_gcm128_encrypt@PLT
	movq	-56(%rbp), %r11
	testl	%eax, %eax
	je	.L200
	jmp	.L250
.L201:
	movq	%r11, %rdi
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r10, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	CRYPTO_gcm128_decrypt@PLT
	movq	-56(%rbp), %r11
	movq	-64(%rbp), %r10
	testl	%eax, %eax
	je	.L205
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L255:
	movq	%rax, %r8
	movq	gcm_ghash_avx@GOTPCREL(%rip), %rax
	cmpq	%rax, 616(%r14)
	jne	.L210
	movl	624(%r14), %r10d
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r11, %rdi
	movq	%r11, -56(%rbp)
	negl	%r10d
	movl	%r10d, %r15d
	andl	$15, %r15d
	movq	%r15, %rcx
	call	CRYPTO_gcm128_encrypt@PLT
	testl	%eax, %eax
	jne	.L192
	movq	-56(%rbp), %r11
	movq	%rbx, %rdx
	leaq	0(%r13,%r15), %rsi
	leaq	(%r12,%r15), %rdi
	movq	640(%r14), %rcx
	subq	%r15, %rdx
	leaq	320(%r14), %r9
	movq	%r11, %r8
	call	aesni_gcm_encrypt@PLT
	movq	%rbx, %rcx
	movq	-56(%rbp), %r11
	addq	%rax, 312(%r14)
	leaq	(%r15,%rax), %r10
	movq	720(%r14), %r8
	subq	%r10, %rcx
	addq	%r10, %r13
	addq	%r10, %r12
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L257:
	movq	%rax, %r8
	movq	gcm_ghash_avx@GOTPCREL(%rip), %rax
	cmpq	%rax, 616(%r14)
	jne	.L213
	movl	624(%r14), %r10d
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r11, %rdi
	movq	%r11, -56(%rbp)
	negl	%r10d
	movl	%r10d, %r15d
	andl	$15, %r15d
	movq	%r15, %rcx
	call	CRYPTO_gcm128_decrypt@PLT
	testl	%eax, %eax
	jne	.L192
	movq	-56(%rbp), %r11
	movq	%rbx, %rdx
	leaq	0(%r13,%r15), %rsi
	leaq	(%r12,%r15), %rdi
	movq	640(%r14), %rcx
	subq	%r15, %rdx
	leaq	320(%r14), %r9
	movq	%r11, %r8
	call	aesni_gcm_decrypt@PLT
	movq	%rbx, %rcx
	movq	-56(%rbp), %r11
	addq	%rax, 312(%r14)
	leaq	(%r15,%rax), %r10
	movq	720(%r14), %r8
	subq	%r10, %rcx
	addq	%r10, %r13
	addq	%r10, %r12
	jmp	.L213
.L259:
	movq	gcm_ghash_avx@GOTPCREL(%rip), %rcx
	cmpq	%rcx, 616(%r14)
	je	.L261
	movq	%r12, %rsi
	movq	%r13, %rdx
	movq	%rbx, %rcx
	movq	%rax, %r8
	jmp	.L202
.L256:
	movq	gcm_ghash_avx@GOTPCREL(%rip), %rcx
	cmpq	%rcx, 616(%r14)
	je	.L262
	movq	%r13, %rdx
	movq	%rbx, %rcx
	movq	%rax, %r8
	jmp	.L199
.L261:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r11, %rdi
	movq	%r10, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	CRYPTO_gcm128_decrypt@PLT
	testl	%eax, %eax
	jne	.L192
	movq	-56(%rbp), %r11
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	640(%r14), %rcx
	leaq	320(%r14), %r9
	movq	%r11, %r8
	call	aesni_gcm_decrypt@PLT
	movq	%rbx, %rcx
	movq	-56(%rbp), %r11
	movq	-64(%rbp), %r10
	movq	%rax, %rsi
	addq	%rax, 312(%r14)
	leaq	0(%r13,%rax), %rdx
	subq	%rax, %rcx
	movq	720(%r14), %r8
	addq	%r12, %rsi
	jmp	.L202
.L262:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r11, %rdi
	call	CRYPTO_gcm128_encrypt@PLT
	testl	%eax, %eax
	jne	.L192
	movq	-56(%rbp), %r11
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	640(%r14), %rcx
	leaq	320(%r14), %r9
	movq	%r11, %r8
	call	aesni_gcm_encrypt@PLT
	movq	%rbx, %rcx
	movq	-56(%rbp), %r11
	addq	%rax, 312(%r14)
	movq	720(%r14), %r8
	subq	%rax, %rcx
	leaq	0(%r13,%rax), %rdx
	addq	%rax, %r12
	jmp	.L199
	.cfi_endproc
.LFE473:
	.size	aes_gcm_cipher, .-aes_gcm_cipher
	.p2align 4
	.type	aes_xts_init_key, @function
aes_xts_init_key:
.LFB478:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%rax, %rbx
	movq	%r13, %rax
	orq	%r12, %rax
	je	.L285
	testq	%r12, %r12
	je	.L266
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_key_length@PLT
	testl	%r14d, %r14d
	jne	.L286
	movq	$0, 528(%rbx)
	testb	$2, 5+OPENSSL_ia32cap_P(%rip)
	jne	.L287
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	leal	0(,%rax,4), %esi
	call	AES_set_decrypt_key@PLT
	movq	AES_decrypt@GOTPCREL(%rip), %rax
	movq	AES_encrypt@GOTPCREL(%rip), %r14
	movq	%rax, 512(%rbx)
.L270:
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%r15, %rdi
	movl	%eax, -52(%rbp)
	call	EVP_CIPHER_CTX_key_length@PLT
	movl	-52(%rbp), %esi
	leaq	248(%rbx), %rdx
	movl	%eax, %edi
	shrl	$31, %edi
	sall	$2, %esi
	addl	%eax, %edi
	sarl	%edi
	movslq	%edi, %rdi
	addq	%r12, %rdi
	call	AES_set_encrypt_key@PLT
	movq	%r14, 520(%rbx)
	movq	%rbx, 496(%rbx)
.L266:
	testq	%r13, %r13
	je	.L285
.L288:
	leaq	248(%rbx), %rax
	movq	%r15, %rdi
	movq	%rax, 504(%rbx)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movdqu	0(%r13), %xmm0
	movups	%xmm0, (%rax)
.L285:
	movl	$1, %eax
.L263:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L287:
	.cfi_restore_state
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	leal	0(,%rax,4), %esi
	call	vpaes_set_decrypt_key@PLT
	movq	vpaes_decrypt@GOTPCREL(%rip), %rax
	movq	vpaes_encrypt@GOTPCREL(%rip), %r14
	movq	%rax, 512(%rbx)
.L269:
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%r15, %rdi
	movl	%eax, -52(%rbp)
	call	EVP_CIPHER_CTX_key_length@PLT
	movl	-52(%rbp), %esi
	leaq	248(%rbx), %rdx
	movl	%eax, %edi
	shrl	$31, %edi
	sall	$2, %esi
	addl	%eax, %edi
	sarl	%edi
	movslq	%edi, %rdi
	addq	%r12, %rdi
	call	vpaes_set_encrypt_key@PLT
	movq	%r14, 520(%rbx)
	movq	%rbx, 496(%rbx)
	testq	%r13, %r13
	jne	.L288
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L286:
	movl	%eax, %edx
	movq	%r12, %rdi
	shrl	$31, %edx
	addl	%edx, %eax
	sarl	%eax
	movslq	%eax, %rdx
	leaq	(%r12,%rdx), %rsi
	call	CRYPTO_memcmp@PLT
	testl	%eax, %eax
	je	.L289
	movq	$0, 528(%rbx)
	testb	$2, 5+OPENSSL_ia32cap_P(%rip)
	jne	.L290
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	leal	0(,%rax,4), %esi
	call	AES_set_encrypt_key@PLT
	movq	AES_encrypt@GOTPCREL(%rip), %r14
	movq	%r14, 512(%rbx)
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L290:
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	leal	0(,%rax,4), %esi
	call	vpaes_set_encrypt_key@PLT
	movq	vpaes_encrypt@GOTPCREL(%rip), %r14
	movq	%r14, 512(%rbx)
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L289:
	movl	$3389, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$183, %edx
	movl	%eax, -52(%rbp)
	movl	$209, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	movl	-52(%rbp), %eax
	jmp	.L263
	.cfi_endproc
.LFE478:
	.size	aes_xts_init_key, .-aes_xts_init_key
	.p2align 4
	.type	aesni_xts_init_key, @function
aesni_xts_init_key:
.LFB437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%rax, %rbx
	movq	%r14, %rax
	orq	%r13, %rax
	je	.L306
	testq	%r13, %r13
	je	.L294
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_key_length@PLT
	testl	%r15d, %r15d
	jne	.L307
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%rbx, %rdx
	movq	%r13, %rdi
	leal	0(,%rax,4), %esi
	call	aesni_set_decrypt_key@PLT
	movq	aesni_decrypt@GOTPCREL(%rip), %rax
	movq	aesni_encrypt@GOTPCREL(%rip), %r15
	movq	%rax, 512(%rbx)
	movq	aesni_xts_decrypt@GOTPCREL(%rip), %rax
	movq	%rax, 528(%rbx)
.L298:
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%r12, %rdi
	movl	%eax, -52(%rbp)
	call	EVP_CIPHER_CTX_key_length@PLT
	movl	-52(%rbp), %esi
	leaq	248(%rbx), %rdx
	movl	%eax, %edi
	shrl	$31, %edi
	sall	$2, %esi
	addl	%eax, %edi
	sarl	%edi
	movslq	%edi, %rdi
	addq	%r13, %rdi
	call	aesni_set_encrypt_key@PLT
	movq	%r15, 520(%rbx)
	movq	%rbx, 496(%rbx)
.L294:
	testq	%r14, %r14
	je	.L306
	leaq	248(%rbx), %rax
	movq	%r12, %rdi
	movq	%rax, 504(%rbx)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movdqu	(%r14), %xmm0
	movups	%xmm0, (%rax)
.L306:
	movl	$1, %eax
.L291:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L307:
	.cfi_restore_state
	movl	%eax, %edx
	movq	%r13, %rdi
	shrl	$31, %edx
	addl	%edx, %eax
	sarl	%eax
	movslq	%eax, %rdx
	leaq	0(%r13,%rdx), %rsi
	call	CRYPTO_memcmp@PLT
	testl	%eax, %eax
	je	.L308
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%rbx, %rdx
	movq	%r13, %rdi
	leal	0(,%rax,4), %esi
	call	aesni_set_encrypt_key@PLT
	movq	aesni_encrypt@GOTPCREL(%rip), %r15
	movq	aesni_xts_encrypt@GOTPCREL(%rip), %rax
	movq	%r15, 512(%rbx)
	movq	%rax, 528(%rbx)
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L308:
	movl	$406, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$183, %edx
	movl	%eax, -52(%rbp)
	movl	$207, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	movl	-52(%rbp), %eax
	jmp	.L291
	.cfi_endproc
.LFE437:
	.size	aesni_xts_init_key, .-aesni_xts_init_key
	.p2align 4
	.type	aes_gcm_init_key, @function
aes_gcm_init_key:
.LFB471:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%rax, %rbx
	movq	%r14, %rax
	orq	%r12, %rax
	je	.L310
	testq	%r12, %r12
	je	.L311
	movl	104(%r13), %esi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	leaq	256(%rbx), %r13
	sall	$3, %esi
	testb	$2, 5+OPENSSL_ia32cap_P(%rip)
	je	.L312
	call	vpaes_set_encrypt_key@PLT
	movq	vpaes_encrypt@GOTPCREL(%rip), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	CRYPTO_gcm128_init@PLT
	movq	$0, 720(%rbx)
	testq	%r14, %r14
	je	.L325
.L314:
	movslq	704(%rbx), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	CRYPTO_gcm128_setiv@PLT
	movl	$1, 252(%rbx)
.L315:
	movl	$1, 248(%rbx)
.L310:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L312:
	.cfi_restore_state
	call	AES_set_encrypt_key@PLT
	movq	AES_encrypt@GOTPCREL(%rip), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	CRYPTO_gcm128_init@PLT
	movq	$0, 720(%rbx)
	testq	%r14, %r14
	jne	.L314
.L325:
	movl	252(%rbx), %edx
	testl	%edx, %edx
	je	.L315
	movq	696(%rbx), %r14
	testq	%r14, %r14
	je	.L315
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L311:
	movl	248(%rbx), %eax
	movslq	704(%rbx), %rdx
	testl	%eax, %eax
	je	.L316
	leaq	256(%rbx), %rdi
	movq	%r14, %rsi
	call	CRYPTO_gcm128_setiv@PLT
.L317:
	movl	$1, 252(%rbx)
	movl	$1, %eax
	movl	$0, 712(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L316:
	.cfi_restore_state
	movq	696(%rbx), %rdi
	movq	%r14, %rsi
	call	memcpy@PLT
	jmp	.L317
	.cfi_endproc
.LFE471:
	.size	aes_gcm_init_key, .-aes_gcm_init_key
	.p2align 4
	.type	aesni_gcm_init_key, @function
aesni_gcm_init_key:
.LFB436:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%rax, %rbx
	movq	%r14, %rax
	orq	%r12, %rax
	je	.L327
	testq	%r12, %r12
	je	.L328
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%r12, %rdi
	movq	%rbx, %rdx
	leaq	256(%rbx), %r12
	leal	0(,%rax,8), %esi
	call	aesni_set_encrypt_key@PLT
	movq	aesni_encrypt@GOTPCREL(%rip), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	CRYPTO_gcm128_init@PLT
	movq	aesni_ctr32_encrypt_blocks@GOTPCREL(%rip), %rax
	movq	%rax, 720(%rbx)
	testq	%r14, %r14
	je	.L341
.L329:
	movslq	704(%rbx), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	CRYPTO_gcm128_setiv@PLT
	movl	$1, 252(%rbx)
.L330:
	movl	$1, 248(%rbx)
.L327:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L328:
	.cfi_restore_state
	movl	248(%rbx), %eax
	movslq	704(%rbx), %rdx
	testl	%eax, %eax
	jne	.L342
	movq	696(%rbx), %rdi
	movq	%r14, %rsi
	call	memcpy@PLT
.L332:
	movl	$1, 252(%rbx)
	movl	$1, %eax
	movl	$0, 712(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L342:
	.cfi_restore_state
	leaq	256(%rbx), %rdi
	movq	%r14, %rsi
	call	CRYPTO_gcm128_setiv@PLT
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L341:
	movl	252(%rbx), %edx
	testl	%edx, %edx
	je	.L330
	movq	696(%rbx), %r14
	testq	%r14, %r14
	jne	.L329
	movl	$1, 248(%rbx)
	jmp	.L327
	.cfi_endproc
.LFE436:
	.size	aesni_gcm_init_key, .-aesni_gcm_init_key
	.p2align 4
	.type	aes_xts_cipher, @function
aes_xts_cipher:
.LFB479:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	cmpq	$0, 496(%rax)
	je	.L348
	cmpq	$0, 504(%rax)
	movq	%rax, %rbx
	je	.L348
	testq	%r13, %r13
	sete	%dl
	testq	%r12, %r12
	sete	%al
	orb	%al, %dl
	jne	.L348
	cmpq	$15, %r14
	jbe	.L348
	movq	528(%rbx), %r10
	movq	%r15, %rdi
	testq	%r10, %r10
	movq	%r10, -56(%rbp)
	je	.L345
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	-56(%rbp), %r10
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	496(%rbx), %rcx
	movq	504(%rbx), %r8
	movq	%rax, %r9
	movq	%r12, %rdi
	call	*%r10
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L348:
	.cfi_restore_state
	xorl	%eax, %eax
.L343:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L345:
	.cfi_restore_state
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r15, %rdi
	movl	%eax, -56(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movl	-56(%rbp), %r9d
	movq	%r14, %r8
	movq	%r13, %rcx
	movq	%rax, %rsi
	leaq	496(%rbx), %rdi
	movq	%r12, %rdx
	call	CRYPTO_xts128_encrypt@PLT
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
	jmp	.L343
	.cfi_endproc
.LFE479:
	.size	aes_xts_cipher, .-aes_xts_cipher
	.p2align 4
	.type	aes_ccm_ctrl, @function
aes_ccm_ctrl:
.LFB482:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$8, %rsp
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	cmpl	$37, %ebx
	ja	.L373
	leaq	.L353(%rip), %rdx
	movq	%rax, %r15
	movslq	(%rdx,%rbx,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L353:
	.long	.L361-.L353
	.long	.L373-.L353
	.long	.L373-.L353
	.long	.L373-.L353
	.long	.L373-.L353
	.long	.L373-.L353
	.long	.L373-.L353
	.long	.L373-.L353
	.long	.L360-.L353
	.long	.L359-.L353
	.long	.L373-.L353
	.long	.L373-.L353
	.long	.L373-.L353
	.long	.L373-.L353
	.long	.L373-.L353
	.long	.L373-.L353
	.long	.L358-.L353
	.long	.L357-.L353
	.long	.L356-.L353
	.long	.L373-.L353
	.long	.L355-.L353
	.long	.L373-.L353
	.long	.L354-.L353
	.long	.L373-.L353
	.long	.L373-.L353
	.long	.L373-.L353
	.long	.L373-.L353
	.long	.L373-.L353
	.long	.L373-.L353
	.long	.L373-.L353
	.long	.L373-.L353
	.long	.L373-.L353
	.long	.L373-.L353
	.long	.L373-.L353
	.long	.L373-.L353
	.long	.L373-.L353
	.long	.L373-.L353
	.long	.L352-.L353
	.text
	.p2align 4,,10
	.p2align 3
.L373:
	movl	$-1, %eax
.L350:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L359:
	.cfi_restore_state
	movl	$15, %eax
	subl	%r13d, %eax
	movl	%eax, %r13d
.L355:
	leal	-2(%r13), %eax
	cmpl	$6, %eax
	ja	.L363
	movl	%r13d, 264(%r15)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L354:
	.cfi_restore_state
	cmpl	$13, %r13d
	je	.L362
.L363:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L352:
	.cfi_restore_state
	movl	$15, %eax
	subl	264(%r15), %eax
	movl	%eax, (%r14)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L361:
	.cfi_restore_state
	movabsq	$51539607560, %rax
	pxor	%xmm0, %xmm0
	movl	$-1, 272(%r15)
	movq	%rax, 264(%r15)
	movl	$1, %eax
	movups	%xmm0, 248(%r15)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L360:
	.cfi_restore_state
	movq	%r14, %rdi
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	328(%r15), %rcx
	movq	%rax, %rdx
	movl	$1, %eax
	testq	%rcx, %rcx
	je	.L350
	cmpq	%r15, %rcx
	jne	.L363
	movq	%rdx, 328(%rdx)
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L358:
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	testl	%eax, %eax
	je	.L363
	movl	256(%r15), %eax
	testl	%eax, %eax
	je	.L363
	movslq	%r13d, %rdx
	leaq	280(%r15), %rdi
	movq	%r14, %rsi
	call	CRYPTO_ccm128_tag@PLT
	testq	%rax, %rax
	je	.L363
	movl	$0, 252(%r15)
	movl	$1, %eax
	movq	$0, 256(%r15)
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L357:
	testb	$1, %r13b
	jne	.L363
	leal	-4(%r13), %eax
	cmpl	$12, %eax
	ja	.L363
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	testl	%eax, %eax
	je	.L365
	testq	%r14, %r14
	jne	.L363
.L366:
	movl	%r13d, 268(%r15)
	movl	$1, %eax
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L356:
	cmpl	$4, %r13d
	jne	.L363
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movl	(%r14), %edx
	movl	%edx, (%rax)
	movl	$1, %eax
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L362:
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_buf_noconst@PLT
	movq	(%r14), %rdx
	movq	%r12, %rdi
	movq	%rdx, (%rax)
	movl	8(%r14), %edx
	movl	%edx, 8(%rax)
	movzbl	12(%r14), %edx
	movb	%dl, 12(%rax)
	movl	$13, 272(%r15)
	call	EVP_CIPHER_CTX_buf_noconst@PLT
	movq	%r12, %rdi
	movzbl	11(%rax), %eax
	sall	$8, %eax
	movl	%eax, %ebx
	call	EVP_CIPHER_CTX_buf_noconst@PLT
	movzbl	12(%rax), %eax
	orl	%ebx, %eax
	cmpw	$7, %ax
	jbe	.L363
	movq	%r12, %rdi
	leal	-8(%rax), %ebx
	call	EVP_CIPHER_CTX_encrypting@PLT
	testl	%eax, %eax
	jne	.L364
	movl	268(%r15), %eax
	movzwl	%bx, %edx
	cmpl	%eax, %edx
	jl	.L363
	subl	%eax, %ebx
.L364:
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_buf_noconst@PLT
	movl	%ebx, %edx
	movq	%r12, %rdi
	shrw	$8, %dx
	movb	%dl, 11(%rax)
	call	EVP_CIPHER_CTX_buf_noconst@PLT
	movb	%bl, 12(%rax)
	movl	268(%r15), %eax
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L365:
	testq	%r14, %r14
	je	.L366
	movl	$1, 256(%r15)
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_buf_noconst@PLT
	cmpl	$8, %r13d
	jnb	.L367
	testb	$4, %r13b
	jne	.L393
	testl	%r13d, %r13d
	je	.L366
	movzbl	(%r14), %edx
	movb	%dl, (%rax)
	testb	$2, %r13b
	je	.L366
	movl	%r13d, %edx
	movzwl	-2(%r14,%rdx), %ecx
	movw	%cx, -2(%rax,%rdx)
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L367:
	movq	(%r14), %rdx
	leaq	8(%rax), %rdi
	andq	$-8, %rdi
	movq	%rdx, (%rax)
	movl	%r13d, %edx
	movq	-8(%r14,%rdx), %rcx
	movq	%rcx, -8(%rax,%rdx)
	subq	%rdi, %rax
	subq	%rax, %r14
	addl	%r13d, %eax
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L366
	andl	$-8, %eax
	xorl	%edx, %edx
.L371:
	movl	%edx, %esi
	addl	$8, %edx
	movq	(%r14,%rsi), %r8
	movq	%r8, (%rdi,%rsi)
	cmpl	%eax, %edx
	jb	.L371
	jmp	.L366
.L393:
	movl	(%r14), %edx
	movl	%edx, (%rax)
	movl	%r13d, %edx
	movl	-4(%r14,%rdx), %ecx
	movl	%ecx, -4(%rax,%rdx)
	jmp	.L366
	.cfi_endproc
.LFE482:
	.size	aes_ccm_ctrl, .-aes_ccm_ctrl
	.p2align 4
	.type	aes_ccm_cipher, @function
aes_ccm_cipher:
.LFB485:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movl	248(%rax), %r8d
	testl	%r8d, %r8d
	je	.L447
	movl	272(%rax), %edi
	movq	%rax, %rbx
	testl	%edi, %edi
	jns	.L449
	testq	%r12, %r12
	jne	.L426
	xorl	%eax, %eax
	testq	%r13, %r13
	jne	.L394
.L426:
	movl	252(%rbx), %esi
	testl	%esi, %esi
	je	.L447
	leaq	280(%rbx), %rax
	movq	%rax, -88(%rbp)
	testq	%r13, %r13
	je	.L450
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	testl	%eax, %eax
	jne	.L411
	movl	256(%rbx), %edx
	testl	%edx, %edx
	je	.L447
.L411:
	movl	260(%rbx), %eax
	testl	%eax, %eax
	jne	.L412
	movl	$15, %edx
	subl	264(%rbx), %edx
	movq	%r15, %rdi
	movslq	%edx, %rdx
	movq	%rdx, -88(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	-88(%rbp), %rdx
	movq	%r14, %rcx
	leaq	280(%rbx), %rdi
	movq	%rax, %rsi
	call	CRYPTO_ccm128_setiv@PLT
	testl	%eax, %eax
	jne	.L447
	movl	$1, 260(%rbx)
.L412:
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	336(%rbx), %r8
	leaq	280(%rbx), %rdi
	testl	%eax, %eax
	je	.L413
	testq	%r8, %r8
	je	.L414
	call	CRYPTO_ccm128_encrypt_ccm64@PLT
	testl	%eax, %eax
	setne	%al
.L415:
	testb	%al, %al
	jne	.L447
	movl	$1, 256(%rbx)
.L448:
	movl	%r14d, %eax
.L394:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L451
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L449:
	.cfi_restore_state
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%rax, %rbx
	leaq	280(%rax), %rax
	movq	%rax, -88(%rbp)
	cmpq	%r13, %r12
	jne	.L447
	movslq	268(%rbx), %rax
	addq	$8, %rax
	cmpq	%rax, %r14
	jb	.L447
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	testl	%eax, %eax
	jne	.L452
.L399:
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	(%r12), %rdx
	movq	%r15, %rdi
	movq	%rdx, 4(%rax)
	movl	268(%rbx), %eax
	movl	$15, %edx
	subl	264(%rbx), %edx
	addl	$8, %eax
	movslq	%edx, %rdx
	cltq
	movq	%rdx, -88(%rbp)
	subq	%rax, %r14
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	-88(%rbp), %rdx
	movq	%r14, %rcx
	leaq	280(%rbx), %rdi
	movq	%rax, %rsi
	call	CRYPTO_ccm128_setiv@PLT
	testl	%eax, %eax
	jne	.L447
	movslq	272(%rbx), %rdx
	movq	%r15, %rdi
	addq	$8, %r12
	addq	$8, %r13
	movq	%rdx, -88(%rbp)
	call	EVP_CIPHER_CTX_buf_noconst@PLT
	movq	-88(%rbp), %rdx
	leaq	280(%rbx), %rdi
	movq	%rax, %rsi
	call	CRYPTO_ccm128_aad@PLT
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	336(%rbx), %r8
	leaq	280(%rbx), %rdi
	testl	%eax, %eax
	je	.L400
	testq	%r8, %r8
	je	.L401
	call	CRYPTO_ccm128_encrypt_ccm64@PLT
	testl	%eax, %eax
	setne	%al
.L402:
	testb	%al, %al
	jne	.L447
	movslq	268(%rbx), %rdx
	leaq	0(%r13,%r14), %rsi
	leaq	280(%rbx), %rdi
	call	CRYPTO_ccm128_tag@PLT
	testq	%rax, %rax
	je	.L447
	movl	268(%rbx), %eax
	leal	8(%rax,%r14), %eax
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L450:
	testq	%r12, %r12
	je	.L453
	movl	260(%rbx), %ecx
	testl	%ecx, %ecx
	jne	.L427
	testq	%r14, %r14
	jne	.L447
.L427:
	movq	-88(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	CRYPTO_ccm128_aad@PLT
	movl	%r14d, %eax
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L400:
	testq	%r8, %r8
	je	.L403
	call	CRYPTO_ccm128_decrypt_ccm64@PLT
	testl	%eax, %eax
	sete	%al
.L404:
	testb	%al, %al
	je	.L405
	leaq	-80(%rbp), %r15
	movslq	268(%rbx), %rdx
	leaq	280(%rbx), %rdi
	movq	%r15, %rsi
	call	CRYPTO_ccm128_tag@PLT
	testq	%rax, %rax
	je	.L405
	movslq	268(%rbx), %rdx
	leaq	(%r12,%r14), %rsi
	movq	%r15, %rdi
	call	CRYPTO_memcmp@PLT
	testl	%eax, %eax
	je	.L448
.L405:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	OPENSSL_cleanse@PLT
	.p2align 4,,10
	.p2align 3
.L447:
	movl	$-1, %eax
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L413:
	testq	%r8, %r8
	je	.L416
	call	CRYPTO_ccm128_decrypt_ccm64@PLT
	testl	%eax, %eax
	sete	%al
.L417:
	testb	%al, %al
	je	.L419
	leaq	-80(%rbp), %r12
	movslq	268(%rbx), %rdx
	leaq	280(%rbx), %rdi
	movq	%r12, %rsi
	call	CRYPTO_ccm128_tag@PLT
	testq	%rax, %rax
	je	.L419
	movslq	268(%rbx), %rdx
	movq	%r15, %rdi
	movq	%rdx, -88(%rbp)
	call	EVP_CIPHER_CTX_buf_noconst@PLT
	movq	-88(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	CRYPTO_memcmp@PLT
	testl	%eax, %eax
	jne	.L419
	movl	%r14d, %eax
	cmpl	$-1, %r14d
	jne	.L423
	.p2align 4,,10
	.p2align 3
.L419:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	OPENSSL_cleanse@PLT
	movl	$-1, %eax
.L423:
	movl	$0, 252(%rbx)
	movq	$0, 256(%rbx)
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L452:
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_buf_noconst@PLT
	movq	(%rax), %rax
	movq	%rax, (%r12)
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L453:
	movq	%r15, %rdi
	movl	$15, %r12d
	subl	264(%rbx), %r12d
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movslq	%r12d, %r12
	movq	%r14, %rcx
	leaq	280(%rbx), %rdi
	movq	%rax, %rsi
	movq	%r12, %rdx
	call	CRYPTO_ccm128_setiv@PLT
	testl	%eax, %eax
	jne	.L447
	movl	$1, 260(%rbx)
	movl	%r14d, %eax
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L414:
	call	CRYPTO_ccm128_encrypt@PLT
	testl	%eax, %eax
	setne	%al
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L401:
	call	CRYPTO_ccm128_encrypt@PLT
	testl	%eax, %eax
	setne	%al
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L403:
	call	CRYPTO_ccm128_decrypt@PLT
	testl	%eax, %eax
	sete	%al
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L416:
	call	CRYPTO_ccm128_decrypt@PLT
	testl	%eax, %eax
	sete	%al
	jmp	.L417
.L451:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE485:
	.size	aes_ccm_cipher, .-aes_ccm_cipher
	.p2align 4
	.type	aes_ccm_init_key, @function
aes_ccm_init_key:
.LFB483:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%rax, %rbx
	movq	%r13, %rax
	orq	%r12, %rax
	je	.L456
	testq	%r12, %r12
	je	.L457
	leaq	280(%rbx), %r15
	movq	%r14, %rdi
	testb	$2, 5+OPENSSL_ia32cap_P(%rip)
	je	.L458
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	leal	0(,%rax,8), %esi
	call	vpaes_set_encrypt_key@PLT
	movl	264(%rbx), %edx
	movl	268(%rbx), %esi
	movq	vpaes_encrypt@GOTPCREL(%rip), %r8
.L467:
	movq	%rbx, %rcx
	movq	%r15, %rdi
	call	CRYPTO_ccm128_init@PLT
	movq	$0, 336(%rbx)
	movl	$1, 248(%rbx)
.L457:
	testq	%r13, %r13
	je	.L456
	movq	%r14, %rdi
	movl	$15, %r12d
	subl	264(%rbx), %r12d
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movslq	%r12d, %r12
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%r12, %rdx
	call	memcpy@PLT
	movl	$1, 252(%rbx)
.L456:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L458:
	.cfi_restore_state
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	leal	0(,%rax,8), %esi
	call	AES_set_encrypt_key@PLT
	movl	264(%rbx), %edx
	movl	268(%rbx), %esi
	movq	AES_encrypt@GOTPCREL(%rip), %r8
	jmp	.L467
	.cfi_endproc
.LFE483:
	.size	aes_ccm_init_key, .-aes_ccm_init_key
	.p2align 4
	.type	aesni_ccm_init_key, @function
aesni_ccm_init_key:
.LFB438:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%rax, %rbx
	movq	%r12, %rax
	orq	%r13, %rax
	je	.L470
	testq	%r13, %r13
	je	.L471
	movq	%r14, %rdi
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%rbx, %rdx
	movq	%r13, %rdi
	leal	0(,%rax,8), %esi
	call	aesni_set_encrypt_key@PLT
	movl	264(%rbx), %edx
	movq	%rbx, %rcx
	movl	268(%rbx), %esi
	movq	aesni_encrypt@GOTPCREL(%rip), %r8
	leaq	280(%rbx), %rdi
	call	CRYPTO_ccm128_init@PLT
	testl	%r15d, %r15d
	jne	.L474
	movq	aesni_ccm64_decrypt_blocks@GOTPCREL(%rip), %rax
.L472:
	movq	%rax, 336(%rbx)
	movl	$1, 248(%rbx)
.L471:
	testq	%r12, %r12
	je	.L470
	movq	%r14, %rdi
	movl	$15, %r13d
	subl	264(%rbx), %r13d
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movslq	%r13d, %r13
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%r13, %rdx
	call	memcpy@PLT
	movl	$1, 252(%rbx)
.L470:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L474:
	.cfi_restore_state
	movq	aesni_ccm64_encrypt_blocks@GOTPCREL(%rip), %rax
	jmp	.L472
	.cfi_endproc
.LFE438:
	.size	aesni_ccm_init_key, .-aesni_ccm_init_key
	.p2align 4
	.type	aes_wrap_init_key, @function
aes_wrap_init_key:
.LFB489:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%rax, %rbx
	movq	%r14, %rax
	orq	%r13, %rax
	je	.L483
	testq	%r13, %r13
	je	.L484
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r12, %rdi
	testl	%eax, %eax
	jne	.L493
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%rbx, %rdx
	movq	%r13, %rdi
	leal	0(,%rax,8), %esi
	call	AES_set_decrypt_key@PLT
.L486:
	testq	%r14, %r14
	jne	.L488
	movq	$0, 248(%rbx)
.L483:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L484:
	.cfi_restore_state
	testq	%r14, %r14
	je	.L483
.L488:
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_iv_length@PLT
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movslq	%r13d, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%rax, 248(%rbx)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L493:
	.cfi_restore_state
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%rbx, %rdx
	movq	%r13, %rdi
	leal	0(,%rax,8), %esi
	call	AES_set_encrypt_key@PLT
	jmp	.L486
	.cfi_endproc
.LFE489:
	.size	aes_wrap_init_key, .-aes_wrap_init_key
	.p2align 4
	.type	aes_wrap_cipher, @function
aes_wrap_cipher:
.LFB490:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	EVP_CIPHER_CTX_iv_length@PLT
	movl	%eax, -52(%rbp)
	testq	%r13, %r13
	je	.L509
	testq	%rbx, %rbx
	je	.L498
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	testl	%eax, %eax
	je	.L515
	cmpl	$4, -52(%rbp)
	je	.L499
.L500:
	testb	$7, %bl
	jne	.L498
.L499:
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	is_partially_overlapping@PLT
	testl	%eax, %eax
	jne	.L516
	movq	%r12, %rdi
	testq	%r14, %r14
	je	.L517
	cmpl	$4, -52(%rbp)
	je	.L518
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	248(%r15), %rsi
	testl	%eax, %eax
	jne	.L519
	movq	AES_decrypt@GOTPCREL(%rip), %r9
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	CRYPTO_128_unwrap@PLT
.L507:
	testq	%rax, %rax
	je	.L498
.L494:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L515:
	.cfi_restore_state
	cmpq	$15, %rbx
	ja	.L500
	.p2align 4,,10
	.p2align 3
.L498:
	movl	$-1, %eax
	jmp	.L494
	.p2align 4,,10
	.p2align 3
.L519:
	movq	AES_encrypt@GOTPCREL(%rip), %r9
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	CRYPTO_128_wrap@PLT
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L509:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L518:
	.cfi_restore_state
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	248(%r15), %rsi
	testl	%eax, %eax
	je	.L506
	movq	AES_encrypt@GOTPCREL(%rip), %r9
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	CRYPTO_128_wrap_pad@PLT
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L516:
	movl	$3838, %r8d
	movl	$162, %edx
	movl	$170, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L517:
	.cfi_restore_state
	call	EVP_CIPHER_CTX_encrypting@PLT
	movl	%eax, %r8d
	leal	-8(%rbx), %eax
	testl	%r8d, %r8d
	je	.L494
	leaq	7(%rbx), %rax
	andq	$-8, %rax
	cmpl	$4, -52(%rbp)
	cmove	%rax, %rbx
	leal	8(%rbx), %eax
	jmp	.L494
	.p2align 4,,10
	.p2align 3
.L506:
	movq	AES_decrypt@GOTPCREL(%rip), %r9
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	CRYPTO_128_unwrap_pad@PLT
	jmp	.L507
	.cfi_endproc
.LFE490:
	.size	aes_wrap_cipher, .-aes_wrap_cipher
	.p2align 4
	.type	aes_ocb_ctrl, @function
aes_ocb_ctrl:
.LFB497:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$8, %rsp
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	cmpl	$37, %ebx
	ja	.L532
	leaq	.L523(%rip), %rdx
	movq	%rax, %r12
	movslq	(%rdx,%rbx,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L523:
	.long	.L528-.L523
	.long	.L532-.L523
	.long	.L532-.L523
	.long	.L532-.L523
	.long	.L532-.L523
	.long	.L532-.L523
	.long	.L532-.L523
	.long	.L532-.L523
	.long	.L527-.L523
	.long	.L526-.L523
	.long	.L532-.L523
	.long	.L532-.L523
	.long	.L532-.L523
	.long	.L532-.L523
	.long	.L532-.L523
	.long	.L532-.L523
	.long	.L525-.L523
	.long	.L524-.L523
	.long	.L532-.L523
	.long	.L532-.L523
	.long	.L532-.L523
	.long	.L532-.L523
	.long	.L532-.L523
	.long	.L532-.L523
	.long	.L532-.L523
	.long	.L532-.L523
	.long	.L532-.L523
	.long	.L532-.L523
	.long	.L532-.L523
	.long	.L532-.L523
	.long	.L532-.L523
	.long	.L532-.L523
	.long	.L532-.L523
	.long	.L532-.L523
	.long	.L532-.L523
	.long	.L532-.L523
	.long	.L532-.L523
	.long	.L522-.L523
	.text
	.p2align 4,,10
	.p2align 3
.L532:
	movl	$-1, %eax
.L520:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L522:
	.cfi_restore_state
	movl	744(%r12), %eax
	movl	%eax, (%r14)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L528:
	.cfi_restore_state
	movq	$0, 496(%r12)
	movq	(%r15), %rdi
	call	EVP_CIPHER_iv_length@PLT
	movq	%r15, %rdi
	movl	%eax, 744(%r12)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movl	$16, 748(%r12)
	movq	%rax, 680(%r12)
	movl	$1, %eax
	movq	$0, 736(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L527:
	.cfi_restore_state
	movq	%r14, %rdi
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	addq	$8, %rsp
	leaq	504(%r12), %rsi
	popq	%rbx
	movq	%rax, %rdx
	popq	%r12
	leaq	248(%rax), %rcx
	popq	%r13
	leaq	504(%rax), %rdi
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_ocb128_copy_ctx@PLT
	.p2align 4,,10
	.p2align 3
.L526:
	.cfi_restore_state
	leal	-1(%r13), %eax
	cmpl	$14, %eax
	ja	.L531
	movl	%r13d, 744(%r12)
	movl	$1, %eax
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L525:
	cmpl	%r13d, 748(%r12)
	je	.L537
.L531:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L524:
	.cfi_restore_state
	testq	%r14, %r14
	je	.L538
	cmpl	%r13d, 748(%r12)
	jne	.L531
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	testl	%eax, %eax
	jne	.L531
	leaq	688(%r12), %rdi
	movslq	%r13d, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movl	$1, %eax
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L537:
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	testl	%eax, %eax
	je	.L531
	movslq	%r13d, %rdx
	leaq	688(%r12), %rsi
	movq	%r14, %rdi
	call	memcpy@PLT
	movl	$1, %eax
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L538:
	cmpl	$16, %r13d
	ja	.L531
	movl	%r13d, 748(%r12)
	movl	$1, %eax
	jmp	.L520
	.cfi_endproc
.LFE497:
	.size	aes_ocb_ctrl, .-aes_ocb_ctrl
	.p2align 4
	.type	aes_ocb_cleanup, @function
aes_ocb_cleanup:
.LFB500:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	leaq	504(%rax), %rdi
	call	CRYPTO_ocb128_cleanup@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE500:
	.size	aes_ocb_cleanup, .-aes_ocb_cleanup
	.p2align 4
	.type	aes_ocb_cipher, @function
aes_ocb_cipher:
.LFB499:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$40, %rsp
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movl	500(%rax), %ecx
	testl	%ecx, %ecx
	je	.L544
	movl	496(%rax), %edx
	movq	%rax, %r15
	testl	%edx, %edx
	je	.L544
	testq	%r14, %r14
	je	.L545
	testq	%r12, %r12
	je	.L619
	movslq	736(%r15), %rdi
	leaq	704(%rax), %rax
	movl	%ebx, %edx
	movq	%r14, %rsi
	movq	%rax, -64(%rbp)
	leaq	736(%r15), %rax
	addq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	is_partially_overlapping@PLT
	testl	%eax, %eax
	jne	.L620
	movq	-56(%rbp), %rax
	movslq	(%rax), %rdi
	testl	%edi, %edi
	jle	.L548
.L623:
	movl	$16, %edx
	subl	%edi, %edx
	addq	-64(%rbp), %rdi
	cmpq	%rbx, %rdx
	ja	.L621
	movq	%r14, %rsi
	movq	%rdx, -72(%rbp)
	call	memcpy@PLT
	movq	-72(%rbp), %rdx
	leaq	504(%r15), %r8
	subq	%rdx, %rbx
	addq	%rdx, %r14
	testq	%r12, %r12
	je	.L622
	movq	%r13, %rdi
	movq	%r8, -72(%rbp)
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %rsi
	movq	%r12, %rdx
	testl	%eax, %eax
	movl	$16, %ecx
	movq	%r8, %rdi
	je	.L552
	call	CRYPTO_ocb128_encrypt@PLT
	movq	-72(%rbp), %r8
	testl	%eax, %eax
	je	.L544
.L553:
	movq	-56(%rbp), %rax
	movq	%rbx, %r10
	addq	$16, %r12
	movl	$16, %r15d
	andl	$15, %r10d
	movl	$0, (%rax)
	testq	$-16, %rbx
	jne	.L555
.L617:
	movl	$16, %ebx
	jmp	.L554
	.p2align 4,,10
	.p2align 3
.L619:
	leaq	720(%rax), %rax
	movq	%rax, -64(%rbp)
	leaq	740(%r15), %rax
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	movslq	(%rax), %rdi
	testl	%edi, %edi
	jg	.L623
.L548:
	movq	%rbx, %r10
	andl	$15, %r10d
	testq	$-16, %rbx
	je	.L570
	leaq	504(%r15), %r8
	xorl	%r15d, %r15d
	testq	%r12, %r12
	je	.L569
.L555:
	movq	%r13, %rdi
	movq	%r10, -80(%rbp)
	movq	%rbx, %r13
	movq	%r8, -72(%rbp)
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	-80(%rbp), %r10
	movq	-72(%rbp), %r8
	movq	%r12, %rdx
	movq	%r14, %rsi
	subq	%r10, %r13
	testl	%eax, %eax
	movq	%r10, -72(%rbp)
	movq	%r8, %rdi
	movq	%r13, %rcx
	je	.L557
	call	CRYPTO_ocb128_encrypt@PLT
	movq	-72(%rbp), %r10
	testl	%eax, %eax
	je	.L544
.L556:
	addl	%r15d, %ebx
	addq	%r13, %r14
	subl	%r10d, %ebx
.L554:
	testq	%r10, %r10
	jne	.L624
.L541:
	addq	$40, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L545:
	.cfi_restore_state
	movl	736(%rax), %eax
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jg	.L625
	movslq	740(%r15), %rdx
	testl	%edx, %edx
	jle	.L567
.L626:
	leaq	720(%r15), %rsi
	leaq	504(%r15), %rdi
	call	CRYPTO_ocb128_aad@PLT
	testl	%eax, %eax
	je	.L544
	movl	$0, 740(%r15)
.L567:
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	testl	%eax, %eax
	jne	.L568
	movslq	748(%r15), %rdx
	testl	%edx, %edx
	js	.L544
	leaq	688(%r15), %rsi
	leaq	504(%r15), %rdi
	call	CRYPTO_ocb128_finish@PLT
	testl	%eax, %eax
	jne	.L544
.L618:
	movl	$0, 500(%r15)
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L625:
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	movslq	736(%r15), %rcx
	leaq	704(%r15), %rsi
	movq	%r12, %rdx
	leaq	504(%r15), %rdi
	testl	%eax, %eax
	je	.L565
	call	CRYPTO_ocb128_encrypt@PLT
	testl	%eax, %eax
	je	.L544
.L566:
	movslq	740(%r15), %rdx
	movl	736(%r15), %ebx
	movl	$0, 736(%r15)
	testl	%edx, %edx
	jg	.L626
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L568:
	leaq	688(%r15), %rsi
	leaq	504(%r15), %rdi
	movl	$16, %edx
	call	CRYPTO_ocb128_tag@PLT
	cmpl	$1, %eax
	je	.L618
	.p2align 4,,10
	.p2align 3
.L544:
	movl	$-1, %ebx
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L622:
	movq	-64(%rbp), %rsi
	movq	%r8, %rdi
	movl	$16, %edx
	movq	%r8, -72(%rbp)
	call	CRYPTO_ocb128_aad@PLT
	movq	-72(%rbp), %r8
	testl	%eax, %eax
	je	.L544
	movq	-56(%rbp), %rax
	movq	%rbx, %r10
	movl	$16, %r15d
	andl	$15, %r10d
	movl	$0, (%rax)
	testq	$-16, %rbx
	je	.L617
.L569:
	movq	%rbx, %r13
	movq	%r14, %rsi
	movq	%r8, %rdi
	movq	%r10, -72(%rbp)
	subq	%r10, %r13
	movq	%r13, %rdx
	call	CRYPTO_ocb128_aad@PLT
	movq	-72(%rbp), %r10
	testl	%eax, %eax
	jne	.L556
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L570:
	xorl	%ebx, %ebx
	jmp	.L554
	.p2align 4,,10
	.p2align 3
.L624:
	movl	%r10d, %eax
	cmpl	$8, %r10d
	jnb	.L558
	testb	$4, %r10b
	jne	.L627
	testl	%eax, %eax
	je	.L559
	movzbl	(%r14), %edx
	movq	-64(%rbp), %rcx
	movb	%dl, (%rcx)
	testb	$2, %al
	je	.L559
	movzwl	-2(%r14,%rax), %edx
	movq	-64(%rbp), %rcx
	movw	%dx, -2(%rcx,%rax)
	.p2align 4,,10
	.p2align 3
.L559:
	movq	-56(%rbp), %rax
	movl	%r10d, (%rax)
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L557:
	call	CRYPTO_ocb128_decrypt@PLT
	movq	-72(%rbp), %r10
	testl	%eax, %eax
	jne	.L556
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L621:
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-56(%rbp), %rax
	addl	%ebx, (%rax)
	xorl	%ebx, %ebx
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L552:
	movq	%r8, -72(%rbp)
	call	CRYPTO_ocb128_decrypt@PLT
	movq	-72(%rbp), %r8
	testl	%eax, %eax
	jne	.L553
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L558:
	movq	-64(%rbp), %rcx
	movq	(%r14), %rax
	movq	%r14, %r9
	movq	%rax, (%rcx)
	movq	-8(%r14,%r10), %rax
	leaq	8(%rcx), %rdi
	andq	$-8, %rdi
	movq	%rax, -8(%rcx,%r10)
	movq	%rcx, %rax
	subq	%rdi, %rax
	subq	%rax, %r9
	leal	(%r10,%rax), %eax
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L559
	andl	$-8, %eax
	xorl	%ecx, %ecx
.L562:
	movl	%ecx, %esi
	addl	$8, %ecx
	movq	(%r9,%rsi), %r8
	movq	%r8, (%rdi,%rsi)
	cmpl	%eax, %ecx
	jb	.L562
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L620:
	movl	$4165, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$162, %edx
	xorl	%ebx, %ebx
	movl	$169, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L565:
	call	CRYPTO_ocb128_decrypt@PLT
	testl	%eax, %eax
	jne	.L566
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L627:
	movl	(%r14), %edx
	movq	-64(%rbp), %rcx
	movl	%edx, (%rcx)
	movl	-4(%r14,%rax), %edx
	movl	%edx, -4(%rcx,%rax)
	jmp	.L559
	.cfi_endproc
.LFE499:
	.size	aes_ocb_cipher, .-aes_ocb_cipher
	.p2align 4
	.type	aes_ocb_init_key, @function
aes_ocb_init_key:
.LFB498:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r13, %rcx
	orq	%r14, %rcx
	movq	%rax, %rbx
	movl	$1, %eax
	je	.L628
	testq	%r14, %r14
	je	.L630
	leaq	504(%rbx), %rax
	leaq	248(%rbx), %r15
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	testb	$2, 5+OPENSSL_ia32cap_P(%rip)
	je	.L631
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%rbx, %rdx
	movq	%r14, %rdi
	leal	0(,%rax,8), %esi
	call	vpaes_set_encrypt_key@PLT
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%r15, %rdx
	movq	%r14, %rdi
	leal	0(,%rax,8), %esi
	call	vpaes_set_decrypt_key@PLT
	movq	vpaes_decrypt@GOTPCREL(%rip), %r8
	movq	vpaes_encrypt@GOTPCREL(%rip), %rcx
	xorl	%r9d, %r9d
.L651:
	movq	%r15, %rdx
	movq	%rbx, %rsi
	leaq	504(%rbx), %rdi
	call	CRYPTO_ocb128_init@PLT
	testl	%eax, %eax
	je	.L632
	testq	%r13, %r13
	je	.L652
.L634:
	movslq	748(%rbx), %rcx
	movslq	744(%rbx), %rdx
	movq	%r13, %rsi
	leaq	504(%rbx), %rdi
	call	CRYPTO_ocb128_setiv@PLT
	cmpl	$1, %eax
	jne	.L632
	movl	$1, 500(%rbx)
.L636:
	movl	$1, 496(%rbx)
	movl	$1, %eax
.L628:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L632:
	.cfi_restore_state
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L631:
	.cfi_restore_state
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%rbx, %rdx
	movq	%r14, %rdi
	leal	0(,%rax,8), %esi
	call	AES_set_encrypt_key@PLT
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%r15, %rdx
	movq	%r14, %rdi
	leal	0(,%rax,8), %esi
	call	AES_set_decrypt_key@PLT
	movq	AES_decrypt@GOTPCREL(%rip), %r8
	movq	AES_encrypt@GOTPCREL(%rip), %rcx
	xorl	%r9d, %r9d
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L630:
	movl	496(%rbx), %eax
	movslq	744(%rbx), %rdx
	testl	%eax, %eax
	je	.L637
	movslq	748(%rbx), %rcx
	leaq	504(%rbx), %rdi
	movq	%r13, %rsi
	call	CRYPTO_ocb128_setiv@PLT
.L638:
	movl	$1, 500(%rbx)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L637:
	.cfi_restore_state
	movq	680(%rbx), %rdi
	movq	%r13, %rsi
	call	memcpy@PLT
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L652:
	movl	500(%rbx), %edx
	testl	%edx, %edx
	je	.L636
	movq	680(%rbx), %r13
	testq	%r13, %r13
	je	.L636
	jmp	.L634
	.cfi_endproc
.LFE498:
	.size	aes_ocb_init_key, .-aes_ocb_init_key
	.p2align 4
	.type	aesni_ocb_init_key, @function
aesni_ocb_init_key:
.LFB439:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rcx
	orq	%r14, %rcx
	movq	%rax, %rbx
	movl	$1, %eax
	je	.L653
	testq	%r14, %r14
	je	.L655
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%rbx, %rdx
	movq	%r14, %rdi
	leal	0(,%rax,8), %esi
	call	aesni_set_encrypt_key@PLT
	leaq	248(%rbx), %rdx
	movq	%r15, %rdi
	movq	%rdx, -56(%rbp)
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	-56(%rbp), %rdx
	movq	%r14, %rdi
	leal	0(,%rax,8), %esi
	call	aesni_set_decrypt_key@PLT
	testl	%r13d, %r13d
	movq	-56(%rbp), %rdx
	jne	.L664
	movq	aesni_ocb_decrypt@GOTPCREL(%rip), %r9
.L656:
	leaq	504(%rbx), %r13
	movq	aesni_decrypt@GOTPCREL(%rip), %r8
	movq	aesni_encrypt@GOTPCREL(%rip), %rcx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	CRYPTO_ocb128_init@PLT
	testl	%eax, %eax
	je	.L660
	testq	%r12, %r12
	je	.L670
.L658:
	movslq	748(%rbx), %rcx
	movslq	744(%rbx), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	CRYPTO_ocb128_setiv@PLT
	cmpl	$1, %eax
	jne	.L660
	movl	$1, 500(%rbx)
.L659:
	movl	$1, 496(%rbx)
	movl	$1, %eax
.L653:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L655:
	.cfi_restore_state
	movl	496(%rbx), %eax
	movslq	744(%rbx), %rdx
	testl	%eax, %eax
	jne	.L671
	movq	680(%rbx), %rdi
	movq	%r12, %rsi
	call	memcpy@PLT
.L662:
	movl	$1, 500(%rbx)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L664:
	.cfi_restore_state
	movq	aesni_ocb_encrypt@GOTPCREL(%rip), %r9
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L671:
	movslq	748(%rbx), %rcx
	leaq	504(%rbx), %rdi
	movq	%r12, %rsi
	call	CRYPTO_ocb128_setiv@PLT
	jmp	.L662
	.p2align 4,,10
	.p2align 3
.L660:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L670:
	.cfi_restore_state
	movl	500(%rbx), %edx
	testl	%edx, %edx
	je	.L659
	movq	680(%rbx), %r12
	testq	%r12, %r12
	je	.L659
	jmp	.L658
	.cfi_endproc
.LFE439:
	.size	aesni_ocb_init_key, .-aesni_ocb_init_key
	.p2align 4
	.type	aesni_ecb_cipher, @function
aesni_ecb_cipher:
.LFB435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	EVP_CIPHER_CTX_block_size@PLT
	cltq
	cmpq	%r13, %rax
	ja	.L673
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movl	%ebx, %r8d
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	aesni_ecb_encrypt@PLT
.L673:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE435:
	.size	aesni_ecb_cipher, .-aesni_ecb_cipher
	.p2align 4
	.globl	EVP_aes_128_cbc
	.type	EVP_aes_128_cbc, @function
EVP_aes_128_cbc:
.LFB448:
	.cfi_startproc
	endbr64
	testb	$2, 7+OPENSSL_ia32cap_P(%rip)
	leaq	aes_128_cbc(%rip), %rdx
	leaq	aesni_128_cbc(%rip), %rax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE448:
	.size	EVP_aes_128_cbc, .-EVP_aes_128_cbc
	.p2align 4
	.globl	EVP_aes_128_ecb
	.type	EVP_aes_128_ecb, @function
EVP_aes_128_ecb:
.LFB449:
	.cfi_startproc
	endbr64
	testb	$2, 7+OPENSSL_ia32cap_P(%rip)
	leaq	aes_128_ecb(%rip), %rdx
	leaq	aesni_128_ecb(%rip), %rax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE449:
	.size	EVP_aes_128_ecb, .-EVP_aes_128_ecb
	.p2align 4
	.globl	EVP_aes_128_ofb
	.type	EVP_aes_128_ofb, @function
EVP_aes_128_ofb:
.LFB450:
	.cfi_startproc
	endbr64
	testb	$2, 7+OPENSSL_ia32cap_P(%rip)
	leaq	aes_128_ofb(%rip), %rdx
	leaq	aesni_128_ofb(%rip), %rax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE450:
	.size	EVP_aes_128_ofb, .-EVP_aes_128_ofb
	.p2align 4
	.globl	EVP_aes_128_cfb128
	.type	EVP_aes_128_cfb128, @function
EVP_aes_128_cfb128:
.LFB451:
	.cfi_startproc
	endbr64
	testb	$2, 7+OPENSSL_ia32cap_P(%rip)
	leaq	aes_128_cfb(%rip), %rdx
	leaq	aesni_128_cfb(%rip), %rax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE451:
	.size	EVP_aes_128_cfb128, .-EVP_aes_128_cfb128
	.p2align 4
	.globl	EVP_aes_128_cfb1
	.type	EVP_aes_128_cfb1, @function
EVP_aes_128_cfb1:
.LFB452:
	.cfi_startproc
	endbr64
	testb	$2, 7+OPENSSL_ia32cap_P(%rip)
	leaq	aes_128_cfb1(%rip), %rdx
	leaq	aesni_128_cfb1(%rip), %rax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE452:
	.size	EVP_aes_128_cfb1, .-EVP_aes_128_cfb1
	.p2align 4
	.globl	EVP_aes_128_cfb8
	.type	EVP_aes_128_cfb8, @function
EVP_aes_128_cfb8:
.LFB453:
	.cfi_startproc
	endbr64
	testb	$2, 7+OPENSSL_ia32cap_P(%rip)
	leaq	aes_128_cfb8(%rip), %rdx
	leaq	aesni_128_cfb8(%rip), %rax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE453:
	.size	EVP_aes_128_cfb8, .-EVP_aes_128_cfb8
	.p2align 4
	.globl	EVP_aes_128_ctr
	.type	EVP_aes_128_ctr, @function
EVP_aes_128_ctr:
.LFB454:
	.cfi_startproc
	endbr64
	testb	$2, 7+OPENSSL_ia32cap_P(%rip)
	leaq	aes_128_ctr(%rip), %rdx
	leaq	aesni_128_ctr(%rip), %rax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE454:
	.size	EVP_aes_128_ctr, .-EVP_aes_128_ctr
	.p2align 4
	.globl	EVP_aes_192_cbc
	.type	EVP_aes_192_cbc, @function
EVP_aes_192_cbc:
.LFB455:
	.cfi_startproc
	endbr64
	testb	$2, 7+OPENSSL_ia32cap_P(%rip)
	leaq	aes_192_cbc(%rip), %rdx
	leaq	aesni_192_cbc(%rip), %rax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE455:
	.size	EVP_aes_192_cbc, .-EVP_aes_192_cbc
	.p2align 4
	.globl	EVP_aes_192_ecb
	.type	EVP_aes_192_ecb, @function
EVP_aes_192_ecb:
.LFB456:
	.cfi_startproc
	endbr64
	testb	$2, 7+OPENSSL_ia32cap_P(%rip)
	leaq	aes_192_ecb(%rip), %rdx
	leaq	aesni_192_ecb(%rip), %rax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE456:
	.size	EVP_aes_192_ecb, .-EVP_aes_192_ecb
	.p2align 4
	.globl	EVP_aes_192_ofb
	.type	EVP_aes_192_ofb, @function
EVP_aes_192_ofb:
.LFB457:
	.cfi_startproc
	endbr64
	testb	$2, 7+OPENSSL_ia32cap_P(%rip)
	leaq	aes_192_ofb(%rip), %rdx
	leaq	aesni_192_ofb(%rip), %rax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE457:
	.size	EVP_aes_192_ofb, .-EVP_aes_192_ofb
	.p2align 4
	.globl	EVP_aes_192_cfb128
	.type	EVP_aes_192_cfb128, @function
EVP_aes_192_cfb128:
.LFB458:
	.cfi_startproc
	endbr64
	testb	$2, 7+OPENSSL_ia32cap_P(%rip)
	leaq	aes_192_cfb(%rip), %rdx
	leaq	aesni_192_cfb(%rip), %rax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE458:
	.size	EVP_aes_192_cfb128, .-EVP_aes_192_cfb128
	.p2align 4
	.globl	EVP_aes_192_cfb1
	.type	EVP_aes_192_cfb1, @function
EVP_aes_192_cfb1:
.LFB459:
	.cfi_startproc
	endbr64
	testb	$2, 7+OPENSSL_ia32cap_P(%rip)
	leaq	aes_192_cfb1(%rip), %rdx
	leaq	aesni_192_cfb1(%rip), %rax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE459:
	.size	EVP_aes_192_cfb1, .-EVP_aes_192_cfb1
	.p2align 4
	.globl	EVP_aes_192_cfb8
	.type	EVP_aes_192_cfb8, @function
EVP_aes_192_cfb8:
.LFB460:
	.cfi_startproc
	endbr64
	testb	$2, 7+OPENSSL_ia32cap_P(%rip)
	leaq	aes_192_cfb8(%rip), %rdx
	leaq	aesni_192_cfb8(%rip), %rax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE460:
	.size	EVP_aes_192_cfb8, .-EVP_aes_192_cfb8
	.p2align 4
	.globl	EVP_aes_192_ctr
	.type	EVP_aes_192_ctr, @function
EVP_aes_192_ctr:
.LFB461:
	.cfi_startproc
	endbr64
	testb	$2, 7+OPENSSL_ia32cap_P(%rip)
	leaq	aes_192_ctr(%rip), %rdx
	leaq	aesni_192_ctr(%rip), %rax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE461:
	.size	EVP_aes_192_ctr, .-EVP_aes_192_ctr
	.p2align 4
	.globl	EVP_aes_256_cbc
	.type	EVP_aes_256_cbc, @function
EVP_aes_256_cbc:
.LFB462:
	.cfi_startproc
	endbr64
	testb	$2, 7+OPENSSL_ia32cap_P(%rip)
	leaq	aes_256_cbc(%rip), %rdx
	leaq	aesni_256_cbc(%rip), %rax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE462:
	.size	EVP_aes_256_cbc, .-EVP_aes_256_cbc
	.p2align 4
	.globl	EVP_aes_256_ecb
	.type	EVP_aes_256_ecb, @function
EVP_aes_256_ecb:
.LFB463:
	.cfi_startproc
	endbr64
	testb	$2, 7+OPENSSL_ia32cap_P(%rip)
	leaq	aes_256_ecb(%rip), %rdx
	leaq	aesni_256_ecb(%rip), %rax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE463:
	.size	EVP_aes_256_ecb, .-EVP_aes_256_ecb
	.p2align 4
	.globl	EVP_aes_256_ofb
	.type	EVP_aes_256_ofb, @function
EVP_aes_256_ofb:
.LFB464:
	.cfi_startproc
	endbr64
	testb	$2, 7+OPENSSL_ia32cap_P(%rip)
	leaq	aes_256_ofb(%rip), %rdx
	leaq	aesni_256_ofb(%rip), %rax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE464:
	.size	EVP_aes_256_ofb, .-EVP_aes_256_ofb
	.p2align 4
	.globl	EVP_aes_256_cfb128
	.type	EVP_aes_256_cfb128, @function
EVP_aes_256_cfb128:
.LFB465:
	.cfi_startproc
	endbr64
	testb	$2, 7+OPENSSL_ia32cap_P(%rip)
	leaq	aes_256_cfb(%rip), %rdx
	leaq	aesni_256_cfb(%rip), %rax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE465:
	.size	EVP_aes_256_cfb128, .-EVP_aes_256_cfb128
	.p2align 4
	.globl	EVP_aes_256_cfb1
	.type	EVP_aes_256_cfb1, @function
EVP_aes_256_cfb1:
.LFB466:
	.cfi_startproc
	endbr64
	testb	$2, 7+OPENSSL_ia32cap_P(%rip)
	leaq	aes_256_cfb1(%rip), %rdx
	leaq	aesni_256_cfb1(%rip), %rax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE466:
	.size	EVP_aes_256_cfb1, .-EVP_aes_256_cfb1
	.p2align 4
	.globl	EVP_aes_256_cfb8
	.type	EVP_aes_256_cfb8, @function
EVP_aes_256_cfb8:
.LFB467:
	.cfi_startproc
	endbr64
	testb	$2, 7+OPENSSL_ia32cap_P(%rip)
	leaq	aes_256_cfb8(%rip), %rdx
	leaq	aesni_256_cfb8(%rip), %rax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE467:
	.size	EVP_aes_256_cfb8, .-EVP_aes_256_cfb8
	.p2align 4
	.globl	EVP_aes_256_ctr
	.type	EVP_aes_256_ctr, @function
EVP_aes_256_ctr:
.LFB468:
	.cfi_startproc
	endbr64
	testb	$2, 7+OPENSSL_ia32cap_P(%rip)
	leaq	aes_256_ctr(%rip), %rdx
	leaq	aesni_256_ctr(%rip), %rax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE468:
	.size	EVP_aes_256_ctr, .-EVP_aes_256_ctr
	.p2align 4
	.globl	EVP_aes_128_gcm
	.type	EVP_aes_128_gcm, @function
EVP_aes_128_gcm:
.LFB474:
	.cfi_startproc
	endbr64
	testb	$2, 7+OPENSSL_ia32cap_P(%rip)
	leaq	aes_128_gcm(%rip), %rdx
	leaq	aesni_128_gcm(%rip), %rax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE474:
	.size	EVP_aes_128_gcm, .-EVP_aes_128_gcm
	.p2align 4
	.globl	EVP_aes_192_gcm
	.type	EVP_aes_192_gcm, @function
EVP_aes_192_gcm:
.LFB475:
	.cfi_startproc
	endbr64
	testb	$2, 7+OPENSSL_ia32cap_P(%rip)
	leaq	aes_192_gcm(%rip), %rdx
	leaq	aesni_192_gcm(%rip), %rax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE475:
	.size	EVP_aes_192_gcm, .-EVP_aes_192_gcm
	.p2align 4
	.globl	EVP_aes_256_gcm
	.type	EVP_aes_256_gcm, @function
EVP_aes_256_gcm:
.LFB476:
	.cfi_startproc
	endbr64
	testb	$2, 7+OPENSSL_ia32cap_P(%rip)
	leaq	aes_256_gcm(%rip), %rdx
	leaq	aesni_256_gcm(%rip), %rax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE476:
	.size	EVP_aes_256_gcm, .-EVP_aes_256_gcm
	.p2align 4
	.globl	EVP_aes_128_xts
	.type	EVP_aes_128_xts, @function
EVP_aes_128_xts:
.LFB480:
	.cfi_startproc
	endbr64
	testb	$2, 7+OPENSSL_ia32cap_P(%rip)
	leaq	aes_128_xts(%rip), %rdx
	leaq	aesni_128_xts(%rip), %rax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE480:
	.size	EVP_aes_128_xts, .-EVP_aes_128_xts
	.p2align 4
	.globl	EVP_aes_256_xts
	.type	EVP_aes_256_xts, @function
EVP_aes_256_xts:
.LFB481:
	.cfi_startproc
	endbr64
	testb	$2, 7+OPENSSL_ia32cap_P(%rip)
	leaq	aes_256_xts(%rip), %rdx
	leaq	aesni_256_xts(%rip), %rax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE481:
	.size	EVP_aes_256_xts, .-EVP_aes_256_xts
	.p2align 4
	.globl	EVP_aes_128_ccm
	.type	EVP_aes_128_ccm, @function
EVP_aes_128_ccm:
.LFB486:
	.cfi_startproc
	endbr64
	testb	$2, 7+OPENSSL_ia32cap_P(%rip)
	leaq	aes_128_ccm(%rip), %rdx
	leaq	aesni_128_ccm(%rip), %rax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE486:
	.size	EVP_aes_128_ccm, .-EVP_aes_128_ccm
	.p2align 4
	.globl	EVP_aes_192_ccm
	.type	EVP_aes_192_ccm, @function
EVP_aes_192_ccm:
.LFB487:
	.cfi_startproc
	endbr64
	testb	$2, 7+OPENSSL_ia32cap_P(%rip)
	leaq	aes_192_ccm(%rip), %rdx
	leaq	aesni_192_ccm(%rip), %rax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE487:
	.size	EVP_aes_192_ccm, .-EVP_aes_192_ccm
	.p2align 4
	.globl	EVP_aes_256_ccm
	.type	EVP_aes_256_ccm, @function
EVP_aes_256_ccm:
.LFB488:
	.cfi_startproc
	endbr64
	testb	$2, 7+OPENSSL_ia32cap_P(%rip)
	leaq	aes_256_ccm(%rip), %rdx
	leaq	aesni_256_ccm(%rip), %rax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE488:
	.size	EVP_aes_256_ccm, .-EVP_aes_256_ccm
	.p2align 4
	.globl	EVP_aes_128_wrap
	.type	EVP_aes_128_wrap, @function
EVP_aes_128_wrap:
.LFB491:
	.cfi_startproc
	endbr64
	leaq	aes_128_wrap(%rip), %rax
	ret
	.cfi_endproc
.LFE491:
	.size	EVP_aes_128_wrap, .-EVP_aes_128_wrap
	.p2align 4
	.globl	EVP_aes_192_wrap
	.type	EVP_aes_192_wrap, @function
EVP_aes_192_wrap:
.LFB492:
	.cfi_startproc
	endbr64
	leaq	aes_192_wrap(%rip), %rax
	ret
	.cfi_endproc
.LFE492:
	.size	EVP_aes_192_wrap, .-EVP_aes_192_wrap
	.p2align 4
	.globl	EVP_aes_256_wrap
	.type	EVP_aes_256_wrap, @function
EVP_aes_256_wrap:
.LFB493:
	.cfi_startproc
	endbr64
	leaq	aes_256_wrap(%rip), %rax
	ret
	.cfi_endproc
.LFE493:
	.size	EVP_aes_256_wrap, .-EVP_aes_256_wrap
	.p2align 4
	.globl	EVP_aes_128_wrap_pad
	.type	EVP_aes_128_wrap_pad, @function
EVP_aes_128_wrap_pad:
.LFB494:
	.cfi_startproc
	endbr64
	leaq	aes_128_wrap_pad(%rip), %rax
	ret
	.cfi_endproc
.LFE494:
	.size	EVP_aes_128_wrap_pad, .-EVP_aes_128_wrap_pad
	.p2align 4
	.globl	EVP_aes_192_wrap_pad
	.type	EVP_aes_192_wrap_pad, @function
EVP_aes_192_wrap_pad:
.LFB495:
	.cfi_startproc
	endbr64
	leaq	aes_192_wrap_pad(%rip), %rax
	ret
	.cfi_endproc
.LFE495:
	.size	EVP_aes_192_wrap_pad, .-EVP_aes_192_wrap_pad
	.p2align 4
	.globl	EVP_aes_256_wrap_pad
	.type	EVP_aes_256_wrap_pad, @function
EVP_aes_256_wrap_pad:
.LFB496:
	.cfi_startproc
	endbr64
	leaq	aes_256_wrap_pad(%rip), %rax
	ret
	.cfi_endproc
.LFE496:
	.size	EVP_aes_256_wrap_pad, .-EVP_aes_256_wrap_pad
	.p2align 4
	.globl	EVP_aes_128_ocb
	.type	EVP_aes_128_ocb, @function
EVP_aes_128_ocb:
.LFB501:
	.cfi_startproc
	endbr64
	testb	$2, 7+OPENSSL_ia32cap_P(%rip)
	leaq	aes_128_ocb(%rip), %rdx
	leaq	aesni_128_ocb(%rip), %rax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE501:
	.size	EVP_aes_128_ocb, .-EVP_aes_128_ocb
	.p2align 4
	.globl	EVP_aes_192_ocb
	.type	EVP_aes_192_ocb, @function
EVP_aes_192_ocb:
.LFB502:
	.cfi_startproc
	endbr64
	testb	$2, 7+OPENSSL_ia32cap_P(%rip)
	leaq	aes_192_ocb(%rip), %rdx
	leaq	aesni_192_ocb(%rip), %rax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE502:
	.size	EVP_aes_192_ocb, .-EVP_aes_192_ocb
	.p2align 4
	.globl	EVP_aes_256_ocb
	.type	EVP_aes_256_ocb, @function
EVP_aes_256_ocb:
.LFB503:
	.cfi_startproc
	endbr64
	testb	$2, 7+OPENSSL_ia32cap_P(%rip)
	leaq	aes_256_ocb(%rip), %rdx
	leaq	aesni_256_ocb(%rip), %rax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE503:
	.size	EVP_aes_256_ocb, .-EVP_aes_256_ocb
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	aes_256_ocb, @object
	.size	aes_256_ocb, 88
aes_256_ocb:
	.long	960
	.long	16
	.long	32
	.long	12
	.quad	3218547
	.quad	aes_ocb_init_key
	.quad	aes_ocb_cipher
	.quad	aes_ocb_cleanup
	.long	752
	.zero	4
	.quad	0
	.quad	0
	.quad	aes_ocb_ctrl
	.quad	0
	.align 32
	.type	aesni_256_ocb, @object
	.size	aesni_256_ocb, 88
aesni_256_ocb:
	.long	960
	.long	16
	.long	32
	.long	12
	.quad	3218547
	.quad	aesni_ocb_init_key
	.quad	aes_ocb_cipher
	.quad	aes_ocb_cleanup
	.long	752
	.zero	4
	.quad	0
	.quad	0
	.quad	aes_ocb_ctrl
	.quad	0
	.align 32
	.type	aes_192_ocb, @object
	.size	aes_192_ocb, 88
aes_192_ocb:
	.long	959
	.long	16
	.long	24
	.long	12
	.quad	3218547
	.quad	aes_ocb_init_key
	.quad	aes_ocb_cipher
	.quad	aes_ocb_cleanup
	.long	752
	.zero	4
	.quad	0
	.quad	0
	.quad	aes_ocb_ctrl
	.quad	0
	.align 32
	.type	aesni_192_ocb, @object
	.size	aesni_192_ocb, 88
aesni_192_ocb:
	.long	959
	.long	16
	.long	24
	.long	12
	.quad	3218547
	.quad	aesni_ocb_init_key
	.quad	aes_ocb_cipher
	.quad	aes_ocb_cleanup
	.long	752
	.zero	4
	.quad	0
	.quad	0
	.quad	aes_ocb_ctrl
	.quad	0
	.align 32
	.type	aes_128_ocb, @object
	.size	aes_128_ocb, 88
aes_128_ocb:
	.long	958
	.long	16
	.long	16
	.long	12
	.quad	3218547
	.quad	aes_ocb_init_key
	.quad	aes_ocb_cipher
	.quad	aes_ocb_cleanup
	.long	752
	.zero	4
	.quad	0
	.quad	0
	.quad	aes_ocb_ctrl
	.quad	0
	.align 32
	.type	aesni_128_ocb, @object
	.size	aesni_128_ocb, 88
aesni_128_ocb:
	.long	958
	.long	16
	.long	16
	.long	12
	.quad	3218547
	.quad	aesni_ocb_init_key
	.quad	aes_ocb_cipher
	.quad	aes_ocb_cleanup
	.long	752
	.zero	4
	.quad	0
	.quad	0
	.quad	aes_ocb_ctrl
	.quad	0
	.align 32
	.type	aes_256_wrap_pad, @object
	.size	aes_256_wrap_pad, 88
aes_256_wrap_pad:
	.long	903
	.long	8
	.long	32
	.long	4
	.quad	1118258
	.quad	aes_wrap_init_key
	.quad	aes_wrap_cipher
	.quad	0
	.long	256
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aes_192_wrap_pad, @object
	.size	aes_192_wrap_pad, 88
aes_192_wrap_pad:
	.long	900
	.long	8
	.long	24
	.long	4
	.quad	1118258
	.quad	aes_wrap_init_key
	.quad	aes_wrap_cipher
	.quad	0
	.long	256
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aes_128_wrap_pad, @object
	.size	aes_128_wrap_pad, 88
aes_128_wrap_pad:
	.long	897
	.long	8
	.long	16
	.long	4
	.quad	1118258
	.quad	aes_wrap_init_key
	.quad	aes_wrap_cipher
	.quad	0
	.long	256
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aes_256_wrap, @object
	.size	aes_256_wrap, 88
aes_256_wrap:
	.long	790
	.long	8
	.long	32
	.long	8
	.quad	1118258
	.quad	aes_wrap_init_key
	.quad	aes_wrap_cipher
	.quad	0
	.long	256
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aes_192_wrap, @object
	.size	aes_192_wrap, 88
aes_192_wrap:
	.long	789
	.long	8
	.long	24
	.long	8
	.quad	1118258
	.quad	aes_wrap_init_key
	.quad	aes_wrap_cipher
	.quad	0
	.long	256
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aes_128_wrap, @object
	.size	aes_128_wrap, 88
aes_128_wrap:
	.long	788
	.long	8
	.long	16
	.long	8
	.quad	1118258
	.quad	aes_wrap_init_key
	.quad	aes_wrap_cipher
	.quad	0
	.long	256
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aes_256_ccm, @object
	.size	aes_256_ccm, 88
aes_256_ccm:
	.long	902
	.long	1
	.long	32
	.long	12
	.quad	3153015
	.quad	aes_ccm_init_key
	.quad	aes_ccm_cipher
	.quad	0
	.long	344
	.zero	4
	.quad	0
	.quad	0
	.quad	aes_ccm_ctrl
	.quad	0
	.align 32
	.type	aesni_256_ccm, @object
	.size	aesni_256_ccm, 88
aesni_256_ccm:
	.long	902
	.long	1
	.long	32
	.long	12
	.quad	3153015
	.quad	aesni_ccm_init_key
	.quad	aes_ccm_cipher
	.quad	0
	.long	344
	.zero	4
	.quad	0
	.quad	0
	.quad	aes_ccm_ctrl
	.quad	0
	.align 32
	.type	aes_192_ccm, @object
	.size	aes_192_ccm, 88
aes_192_ccm:
	.long	899
	.long	1
	.long	24
	.long	12
	.quad	3153015
	.quad	aes_ccm_init_key
	.quad	aes_ccm_cipher
	.quad	0
	.long	344
	.zero	4
	.quad	0
	.quad	0
	.quad	aes_ccm_ctrl
	.quad	0
	.align 32
	.type	aesni_192_ccm, @object
	.size	aesni_192_ccm, 88
aesni_192_ccm:
	.long	899
	.long	1
	.long	24
	.long	12
	.quad	3153015
	.quad	aesni_ccm_init_key
	.quad	aes_ccm_cipher
	.quad	0
	.long	344
	.zero	4
	.quad	0
	.quad	0
	.quad	aes_ccm_ctrl
	.quad	0
	.align 32
	.type	aes_128_ccm, @object
	.size	aes_128_ccm, 88
aes_128_ccm:
	.long	896
	.long	1
	.long	16
	.long	12
	.quad	3153015
	.quad	aes_ccm_init_key
	.quad	aes_ccm_cipher
	.quad	0
	.long	344
	.zero	4
	.quad	0
	.quad	0
	.quad	aes_ccm_ctrl
	.quad	0
	.align 32
	.type	aesni_128_ccm, @object
	.size	aesni_128_ccm, 88
aesni_128_ccm:
	.long	896
	.long	1
	.long	16
	.long	12
	.quad	3153015
	.quad	aesni_ccm_init_key
	.quad	aes_ccm_cipher
	.quad	0
	.long	344
	.zero	4
	.quad	0
	.quad	0
	.quad	aes_ccm_ctrl
	.quad	0
	.align 32
	.type	aes_256_xts, @object
	.size	aes_256_xts, 88
aes_256_xts:
	.long	914
	.long	1
	.long	64
	.long	16
	.quad	70769
	.quad	aes_xts_init_key
	.quad	aes_xts_cipher
	.quad	0
	.long	536
	.zero	4
	.quad	0
	.quad	0
	.quad	aes_xts_ctrl
	.quad	0
	.align 32
	.type	aesni_256_xts, @object
	.size	aesni_256_xts, 88
aesni_256_xts:
	.long	914
	.long	1
	.long	64
	.long	16
	.quad	70769
	.quad	aesni_xts_init_key
	.quad	aes_xts_cipher
	.quad	0
	.long	536
	.zero	4
	.quad	0
	.quad	0
	.quad	aes_xts_ctrl
	.quad	0
	.align 32
	.type	aes_128_xts, @object
	.size	aes_128_xts, 88
aes_128_xts:
	.long	913
	.long	1
	.long	32
	.long	16
	.quad	70769
	.quad	aes_xts_init_key
	.quad	aes_xts_cipher
	.quad	0
	.long	536
	.zero	4
	.quad	0
	.quad	0
	.quad	aes_xts_ctrl
	.quad	0
	.align 32
	.type	aesni_128_xts, @object
	.size	aesni_128_xts, 88
aesni_128_xts:
	.long	913
	.long	1
	.long	32
	.long	16
	.quad	70769
	.quad	aesni_xts_init_key
	.quad	aes_xts_cipher
	.quad	0
	.long	536
	.zero	4
	.quad	0
	.quad	0
	.quad	aes_xts_ctrl
	.quad	0
	.align 32
	.type	aes_256_gcm, @object
	.size	aes_256_gcm, 88
aes_256_gcm:
	.long	901
	.long	1
	.long	32
	.long	12
	.quad	3153014
	.quad	aes_gcm_init_key
	.quad	aes_gcm_cipher
	.quad	aes_gcm_cleanup
	.long	728
	.zero	4
	.quad	0
	.quad	0
	.quad	aes_gcm_ctrl
	.quad	0
	.align 32
	.type	aesni_256_gcm, @object
	.size	aesni_256_gcm, 88
aesni_256_gcm:
	.long	901
	.long	1
	.long	32
	.long	12
	.quad	3153014
	.quad	aesni_gcm_init_key
	.quad	aes_gcm_cipher
	.quad	aes_gcm_cleanup
	.long	728
	.zero	4
	.quad	0
	.quad	0
	.quad	aes_gcm_ctrl
	.quad	0
	.align 32
	.type	aes_192_gcm, @object
	.size	aes_192_gcm, 88
aes_192_gcm:
	.long	898
	.long	1
	.long	24
	.long	12
	.quad	3153014
	.quad	aes_gcm_init_key
	.quad	aes_gcm_cipher
	.quad	aes_gcm_cleanup
	.long	728
	.zero	4
	.quad	0
	.quad	0
	.quad	aes_gcm_ctrl
	.quad	0
	.align 32
	.type	aesni_192_gcm, @object
	.size	aesni_192_gcm, 88
aesni_192_gcm:
	.long	898
	.long	1
	.long	24
	.long	12
	.quad	3153014
	.quad	aesni_gcm_init_key
	.quad	aes_gcm_cipher
	.quad	aes_gcm_cleanup
	.long	728
	.zero	4
	.quad	0
	.quad	0
	.quad	aes_gcm_ctrl
	.quad	0
	.align 32
	.type	aes_128_gcm, @object
	.size	aes_128_gcm, 88
aes_128_gcm:
	.long	895
	.long	1
	.long	16
	.long	12
	.quad	3153014
	.quad	aes_gcm_init_key
	.quad	aes_gcm_cipher
	.quad	aes_gcm_cleanup
	.long	728
	.zero	4
	.quad	0
	.quad	0
	.quad	aes_gcm_ctrl
	.quad	0
	.align 32
	.type	aesni_128_gcm, @object
	.size	aesni_128_gcm, 88
aesni_128_gcm:
	.long	895
	.long	1
	.long	16
	.long	12
	.quad	3153014
	.quad	aesni_gcm_init_key
	.quad	aes_gcm_cipher
	.quad	aes_gcm_cleanup
	.long	728
	.zero	4
	.quad	0
	.quad	0
	.quad	aes_gcm_ctrl
	.quad	0
	.align 32
	.type	aes_256_ctr, @object
	.size	aes_256_ctr, 88
aes_256_ctr:
	.long	906
	.long	1
	.long	32
	.long	16
	.quad	5
	.quad	aes_init_key
	.quad	aes_ctr_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aesni_256_ctr, @object
	.size	aesni_256_ctr, 88
aesni_256_ctr:
	.long	906
	.long	1
	.long	32
	.long	16
	.quad	5
	.quad	aesni_init_key
	.quad	aes_ctr_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aes_256_cfb8, @object
	.size	aes_256_cfb8, 88
aes_256_cfb8:
	.long	655
	.long	1
	.long	32
	.long	16
	.quad	3
	.quad	aes_init_key
	.quad	aes_cfb8_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aesni_256_cfb8, @object
	.size	aesni_256_cfb8, 88
aesni_256_cfb8:
	.long	655
	.long	1
	.long	32
	.long	16
	.quad	3
	.quad	aesni_init_key
	.quad	aes_cfb8_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aes_256_cfb1, @object
	.size	aes_256_cfb1, 88
aes_256_cfb1:
	.long	652
	.long	1
	.long	32
	.long	16
	.quad	3
	.quad	aes_init_key
	.quad	aes_cfb1_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aesni_256_cfb1, @object
	.size	aesni_256_cfb1, 88
aesni_256_cfb1:
	.long	652
	.long	1
	.long	32
	.long	16
	.quad	3
	.quad	aesni_init_key
	.quad	aes_cfb1_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aes_256_cfb, @object
	.size	aes_256_cfb, 88
aes_256_cfb:
	.long	429
	.long	1
	.long	32
	.long	16
	.quad	4099
	.quad	aes_init_key
	.quad	aes_cfb_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aesni_256_cfb, @object
	.size	aesni_256_cfb, 88
aesni_256_cfb:
	.long	429
	.long	1
	.long	32
	.long	16
	.quad	4099
	.quad	aesni_init_key
	.quad	aes_cfb_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aes_256_ofb, @object
	.size	aes_256_ofb, 88
aes_256_ofb:
	.long	428
	.long	1
	.long	32
	.long	16
	.quad	4100
	.quad	aes_init_key
	.quad	aes_ofb_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aesni_256_ofb, @object
	.size	aesni_256_ofb, 88
aesni_256_ofb:
	.long	428
	.long	1
	.long	32
	.long	16
	.quad	4100
	.quad	aesni_init_key
	.quad	aes_ofb_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aes_256_ecb, @object
	.size	aes_256_ecb, 88
aes_256_ecb:
	.long	426
	.long	16
	.long	32
	.long	0
	.quad	4097
	.quad	aes_init_key
	.quad	aes_ecb_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aesni_256_ecb, @object
	.size	aesni_256_ecb, 88
aesni_256_ecb:
	.long	426
	.long	16
	.long	32
	.long	0
	.quad	4097
	.quad	aesni_init_key
	.quad	aesni_ecb_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aes_256_cbc, @object
	.size	aes_256_cbc, 88
aes_256_cbc:
	.long	427
	.long	16
	.long	32
	.long	16
	.quad	4098
	.quad	aes_init_key
	.quad	aes_cbc_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aesni_256_cbc, @object
	.size	aesni_256_cbc, 88
aesni_256_cbc:
	.long	427
	.long	16
	.long	32
	.long	16
	.quad	4098
	.quad	aesni_init_key
	.quad	aesni_cbc_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aes_192_ctr, @object
	.size	aes_192_ctr, 88
aes_192_ctr:
	.long	905
	.long	1
	.long	24
	.long	16
	.quad	5
	.quad	aes_init_key
	.quad	aes_ctr_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aesni_192_ctr, @object
	.size	aesni_192_ctr, 88
aesni_192_ctr:
	.long	905
	.long	1
	.long	24
	.long	16
	.quad	5
	.quad	aesni_init_key
	.quad	aes_ctr_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aes_192_cfb8, @object
	.size	aes_192_cfb8, 88
aes_192_cfb8:
	.long	654
	.long	1
	.long	24
	.long	16
	.quad	3
	.quad	aes_init_key
	.quad	aes_cfb8_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aesni_192_cfb8, @object
	.size	aesni_192_cfb8, 88
aesni_192_cfb8:
	.long	654
	.long	1
	.long	24
	.long	16
	.quad	3
	.quad	aesni_init_key
	.quad	aes_cfb8_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aes_192_cfb1, @object
	.size	aes_192_cfb1, 88
aes_192_cfb1:
	.long	651
	.long	1
	.long	24
	.long	16
	.quad	3
	.quad	aes_init_key
	.quad	aes_cfb1_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aesni_192_cfb1, @object
	.size	aesni_192_cfb1, 88
aesni_192_cfb1:
	.long	651
	.long	1
	.long	24
	.long	16
	.quad	3
	.quad	aesni_init_key
	.quad	aes_cfb1_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aes_192_cfb, @object
	.size	aes_192_cfb, 88
aes_192_cfb:
	.long	425
	.long	1
	.long	24
	.long	16
	.quad	4099
	.quad	aes_init_key
	.quad	aes_cfb_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aesni_192_cfb, @object
	.size	aesni_192_cfb, 88
aesni_192_cfb:
	.long	425
	.long	1
	.long	24
	.long	16
	.quad	4099
	.quad	aesni_init_key
	.quad	aes_cfb_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aes_192_ofb, @object
	.size	aes_192_ofb, 88
aes_192_ofb:
	.long	424
	.long	1
	.long	24
	.long	16
	.quad	4100
	.quad	aes_init_key
	.quad	aes_ofb_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aesni_192_ofb, @object
	.size	aesni_192_ofb, 88
aesni_192_ofb:
	.long	424
	.long	1
	.long	24
	.long	16
	.quad	4100
	.quad	aesni_init_key
	.quad	aes_ofb_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aes_192_ecb, @object
	.size	aes_192_ecb, 88
aes_192_ecb:
	.long	422
	.long	16
	.long	24
	.long	0
	.quad	4097
	.quad	aes_init_key
	.quad	aes_ecb_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aesni_192_ecb, @object
	.size	aesni_192_ecb, 88
aesni_192_ecb:
	.long	422
	.long	16
	.long	24
	.long	0
	.quad	4097
	.quad	aesni_init_key
	.quad	aesni_ecb_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aes_192_cbc, @object
	.size	aes_192_cbc, 88
aes_192_cbc:
	.long	423
	.long	16
	.long	24
	.long	16
	.quad	4098
	.quad	aes_init_key
	.quad	aes_cbc_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aesni_192_cbc, @object
	.size	aesni_192_cbc, 88
aesni_192_cbc:
	.long	423
	.long	16
	.long	24
	.long	16
	.quad	4098
	.quad	aesni_init_key
	.quad	aesni_cbc_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aes_128_ctr, @object
	.size	aes_128_ctr, 88
aes_128_ctr:
	.long	904
	.long	1
	.long	16
	.long	16
	.quad	5
	.quad	aes_init_key
	.quad	aes_ctr_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aesni_128_ctr, @object
	.size	aesni_128_ctr, 88
aesni_128_ctr:
	.long	904
	.long	1
	.long	16
	.long	16
	.quad	5
	.quad	aesni_init_key
	.quad	aes_ctr_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aes_128_cfb8, @object
	.size	aes_128_cfb8, 88
aes_128_cfb8:
	.long	653
	.long	1
	.long	16
	.long	16
	.quad	3
	.quad	aes_init_key
	.quad	aes_cfb8_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aesni_128_cfb8, @object
	.size	aesni_128_cfb8, 88
aesni_128_cfb8:
	.long	653
	.long	1
	.long	16
	.long	16
	.quad	3
	.quad	aesni_init_key
	.quad	aes_cfb8_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aes_128_cfb1, @object
	.size	aes_128_cfb1, 88
aes_128_cfb1:
	.long	650
	.long	1
	.long	16
	.long	16
	.quad	3
	.quad	aes_init_key
	.quad	aes_cfb1_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aesni_128_cfb1, @object
	.size	aesni_128_cfb1, 88
aesni_128_cfb1:
	.long	650
	.long	1
	.long	16
	.long	16
	.quad	3
	.quad	aesni_init_key
	.quad	aes_cfb1_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aes_128_cfb, @object
	.size	aes_128_cfb, 88
aes_128_cfb:
	.long	421
	.long	1
	.long	16
	.long	16
	.quad	4099
	.quad	aes_init_key
	.quad	aes_cfb_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aesni_128_cfb, @object
	.size	aesni_128_cfb, 88
aesni_128_cfb:
	.long	421
	.long	1
	.long	16
	.long	16
	.quad	4099
	.quad	aesni_init_key
	.quad	aes_cfb_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aes_128_ofb, @object
	.size	aes_128_ofb, 88
aes_128_ofb:
	.long	420
	.long	1
	.long	16
	.long	16
	.quad	4100
	.quad	aes_init_key
	.quad	aes_ofb_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aesni_128_ofb, @object
	.size	aesni_128_ofb, 88
aesni_128_ofb:
	.long	420
	.long	1
	.long	16
	.long	16
	.quad	4100
	.quad	aesni_init_key
	.quad	aes_ofb_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aes_128_ecb, @object
	.size	aes_128_ecb, 88
aes_128_ecb:
	.long	418
	.long	16
	.long	16
	.long	0
	.quad	4097
	.quad	aes_init_key
	.quad	aes_ecb_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aesni_128_ecb, @object
	.size	aesni_128_ecb, 88
aesni_128_ecb:
	.long	418
	.long	16
	.long	16
	.long	0
	.quad	4097
	.quad	aesni_init_key
	.quad	aesni_ecb_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aes_128_cbc, @object
	.size	aes_128_cbc, 88
aes_128_cbc:
	.long	419
	.long	16
	.long	16
	.long	16
	.quad	4098
	.quad	aes_init_key
	.quad	aes_cbc_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aesni_128_cbc, @object
	.size	aesni_128_cbc, 88
aesni_128_cbc:
	.long	419
	.long	16
	.long	16
	.long	16
	.quad	4098
	.quad	aesni_init_key
	.quad	aesni_cbc_cipher
	.quad	0
	.long	264
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
