	.file	"ameth_lib.c"
	.text
	.p2align 4
	.type	ameth_cmp, @function
ameth_cmp:
.LFB888:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rsi), %rdx
	movl	(%rax), %eax
	subl	(%rdx), %eax
	ret
	.cfi_endproc
.LFE888:
	.size	ameth_cmp, .-ameth_cmp
	.p2align 4
	.type	ameth_cmp_BSEARCH_CMP_FN, @function
ameth_cmp_BSEARCH_CMP_FN:
.LFB889:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rsi), %rdx
	movl	(%rax), %eax
	subl	(%rdx), %eax
	ret
	.cfi_endproc
.LFE889:
	.size	ameth_cmp_BSEARCH_CMP_FN, .-ameth_cmp_BSEARCH_CMP_FN
	.p2align 4
	.type	pkey_asn1_find, @function
pkey_asn1_find:
.LFB893:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$304, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	%edi, -288(%rbp)
	movq	app_methods(%rip), %rdi
	leaq	-288(%rbp), %rsi
	movq	%rsi, -296(%rbp)
	testq	%rdi, %rdi
	je	.L5
	call	OPENSSL_sk_find@PLT
	testl	%eax, %eax
	jns	.L15
.L5:
	leaq	-296(%rbp), %rdi
	movl	$8, %ecx
	leaq	ameth_cmp_BSEARCH_CMP_FN(%rip), %r8
	movl	$20, %edx
	leaq	standard_methods(%rip), %rsi
	call	OBJ_bsearch_@PLT
	testq	%rax, %rax
	je	.L4
	movq	(%rax), %rax
.L4:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L16
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	movq	app_methods(%rip), %rdi
	movl	%eax, %esi
	call	OPENSSL_sk_value@PLT
	jmp	.L4
.L16:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE893:
	.size	pkey_asn1_find, .-pkey_asn1_find
	.p2align 4
	.globl	EVP_PKEY_asn1_get_count
	.type	EVP_PKEY_asn1_get_count, @function
EVP_PKEY_asn1_get_count:
.LFB891:
	.cfi_startproc
	endbr64
	movq	app_methods(%rip), %rdi
	testq	%rdi, %rdi
	je	.L21
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	OPENSSL_sk_num@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	addl	$20, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore 6
	movl	$20, %eax
	ret
	.cfi_endproc
.LFE891:
	.size	EVP_PKEY_asn1_get_count, .-EVP_PKEY_asn1_get_count
	.p2align 4
	.globl	EVP_PKEY_asn1_get0
	.type	EVP_PKEY_asn1_get0, @function
EVP_PKEY_asn1_get0:
.LFB892:
	.cfi_startproc
	endbr64
	testl	%edi, %edi
	js	.L29
	cmpl	$19, %edi
	jg	.L28
	movslq	%edi, %rdi
	leaq	standard_methods(%rip), %rax
	movq	(%rax,%rdi,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	leal	-20(%rdi), %esi
	movq	app_methods(%rip), %rdi
	jmp	OPENSSL_sk_value@PLT
	.p2align 4,,10
	.p2align 3
.L29:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE892:
	.size	EVP_PKEY_asn1_get0, .-EVP_PKEY_asn1_get0
	.p2align 4
	.globl	EVP_PKEY_asn1_find
	.type	EVP_PKEY_asn1_find, @function
EVP_PKEY_asn1_find:
.LFB894:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L45:
	testb	$1, 8(%rax)
	je	.L31
	movl	4(%rax), %r13d
.L32:
	movl	%r13d, %edi
	call	pkey_asn1_find
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L45
.L31:
	testq	%rbx, %rbx
	je	.L30
	movl	%r13d, %edi
	call	ENGINE_get_pkey_asn1_meth_engine@PLT
	testq	%rax, %rax
	jne	.L46
	movq	$0, (%rbx)
.L30:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	movq	%rax, (%rbx)
	addq	$8, %rsp
	movl	%r13d, %esi
	movq	%rax, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	ENGINE_get_pkey_asn1_meth@PLT
	.cfi_endproc
.LFE894:
	.size	EVP_PKEY_asn1_find, .-EVP_PKEY_asn1_find
	.section	.text.unlikely,"ax",@progbits
.LCOLDB0:
	.text
.LHOTB0:
	.p2align 4
	.globl	EVP_PKEY_asn1_find_str
	.type	EVP_PKEY_asn1_find_str, @function
EVP_PKEY_asn1_find_str:
.LFB895:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%rsi, -80(%rbp)
	movl	%edx, -68(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	cmpl	$-1, %edx
	jne	.L48
	movq	%rsi, %rdi
	call	strlen@PLT
	movl	%eax, -68(%rbp)
.L48:
	testq	%rbx, %rbx
	je	.L49
	movl	-68(%rbp), %edx
	movq	-80(%rbp), %rsi
	leaq	-64(%rbp), %rdi
	call	ENGINE_pkey_asn1_find_str@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L73
	movq	$0, (%rbx)
.L49:
	movq	app_methods(%rip), %rdi
	testq	%rdi, %rdi
	je	.L64
	call	OPENSSL_sk_num@PLT
	leal	20(%rax), %esi
	leal	19(%rax), %ecx
	testl	%esi, %esi
	jle	.L54
.L53:
	movslq	%ecx, %rdx
	leaq	standard_methods(%rip), %rax
	leaq	(%rax,%rdx,8), %rbx
	movslq	-68(%rbp), %rax
	movq	%rdx, %r14
	movq	%rax, -88(%rbp)
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L74:
	movq	(%rbx), %r15
	testb	$1, 8(%r15)
	jne	.L60
	movq	16(%r15), %r13
	movq	%r13, %rdi
	call	strlen@PLT
	cmpl	%eax, -68(%rbp)
	je	.L62
.L60:
	testl	%r14d, %r14d
	je	.L54
.L58:
	subl	$1, %r14d
	subq	$8, %rbx
	movl	%r12d, %esi
.L61:
	movl	%r14d, %r12d
	testl	%r14d, %r14d
	js	.L55
	cmpl	$19, %r14d
	jle	.L74
	movq	app_methods(%rip), %rdi
	subl	$21, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r15
	testb	$1, 8(%rax)
	jne	.L58
	movq	16(%rax), %r13
	movq	%r13, %rdi
	call	strlen@PLT
	cmpl	%eax, -68(%rbp)
	jne	.L58
.L62:
	movq	-88(%rbp), %rdx
	movq	-80(%rbp), %rsi
	movq	%r13, %rdi
	call	strncasecmp@PLT
	testl	%eax, %eax
	jne	.L60
	.p2align 4,,10
	.p2align 3
.L47:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L75
	addq	$56, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	xorl	%r15d, %r15d
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L64:
	movl	$19, %ecx
	movl	$20, %esi
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L73:
	movq	-64(%rbp), %rdi
	call	ENGINE_init@PLT
	movq	-64(%rbp), %rdi
	testl	%eax, %eax
	movl	$0, %eax
	cmove	%rax, %r15
	call	ENGINE_free@PLT
	movq	-64(%rbp), %rax
	movq	%rax, (%rbx)
	jmp	.L47
.L75:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	EVP_PKEY_asn1_find_str.cold, @function
EVP_PKEY_asn1_find_str.cold:
.LFSB895:
.L55:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	8, %rax
	ud2
	.cfi_endproc
.LFE895:
	.text
	.size	EVP_PKEY_asn1_find_str, .-EVP_PKEY_asn1_find_str
	.section	.text.unlikely
	.size	EVP_PKEY_asn1_find_str.cold, .-EVP_PKEY_asn1_find_str.cold
.LCOLDE0:
	.text
.LHOTE0:
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../deps/openssl/openssl/crypto/asn1/ameth_lib.c"
	.text
	.p2align 4
	.globl	EVP_PKEY_asn1_add0
	.type	EVP_PKEY_asn1_add0, @function
EVP_PKEY_asn1_add0:
.LFB896:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$35, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-304(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$288, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	rep stosq
	movq	8(%r12), %rax
	andl	$1, %eax
	cmpq	$0, 16(%r12)
	je	.L96
	testq	%rax, %rax
	jne	.L78
.L79:
	movq	app_methods(%rip), %rdi
	testq	%rdi, %rdi
	je	.L97
.L81:
	movl	(%r12), %eax
	movq	%r13, %rsi
	movl	%eax, -304(%rbp)
	call	OPENSSL_sk_find@PLT
	testl	%eax, %eax
	jns	.L98
	movq	app_methods(%rip), %rdi
	movq	%r12, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L99
.L95:
	xorl	%eax, %eax
.L76:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L100
	addq	$288, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	movl	$167, %r8d
	movl	$179, %edx
	movl	$188, %esi
	movl	$6, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L96:
	testq	%rax, %rax
	jne	.L79
.L78:
	movl	$155, %r8d
	movl	$7, %edx
	movl	$188, %esi
	movl	$6, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L97:
	leaq	ameth_cmp(%rip), %rdi
	call	OPENSSL_sk_new@PLT
	movq	%rax, app_methods(%rip)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L81
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L99:
	movq	app_methods(%rip), %rdi
	call	OPENSSL_sk_sort@PLT
	movl	$1, %eax
	jmp	.L76
.L100:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE896:
	.size	EVP_PKEY_asn1_add0, .-EVP_PKEY_asn1_add0
	.p2align 4
	.globl	EVP_PKEY_asn1_add_alias
	.type	EVP_PKEY_asn1_add_alias, @function
EVP_PKEY_asn1_add_alias:
.LFB897:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$220, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	leaq	.LC1(%rip), %rsi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	%edi, %ebx
	movl	$280, %edi
	subq	$24, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L104
	movl	%r13d, (%rax)
	movq	%rax, %rdi
	movq	%rax, %r12
	movq	$3, 8(%rax)
	movl	%ebx, 4(%rax)
	call	EVP_PKEY_asn1_add0
	testl	%eax, %eax
	je	.L110
	movl	$1, %eax
.L101:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	testb	$2, 8(%r12)
	jne	.L111
.L104:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	movq	16(%r12), %rdi
	movl	$290, %edx
	leaq	.LC1(%rip), %rsi
	movl	%eax, -36(%rbp)
	call	CRYPTO_free@PLT
	movq	24(%r12), %rdi
	movl	$291, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$292, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	movl	-36(%rbp), %eax
	jmp	.L101
	.cfi_endproc
.LFE897:
	.size	EVP_PKEY_asn1_add_alias, .-EVP_PKEY_asn1_add_alias
	.p2align 4
	.globl	EVP_PKEY_asn1_get0_info
	.type	EVP_PKEY_asn1_get0_info, @function
EVP_PKEY_asn1_get0_info:
.LFB898:
	.cfi_startproc
	endbr64
	testq	%r9, %r9
	je	.L118
	testq	%rdi, %rdi
	je	.L114
	movl	(%r9), %eax
	movl	%eax, (%rdi)
.L114:
	testq	%rsi, %rsi
	je	.L115
	movl	4(%r9), %eax
	movl	%eax, (%rsi)
.L115:
	testq	%rdx, %rdx
	je	.L116
	movq	8(%r9), %rax
	movl	%eax, (%rdx)
.L116:
	testq	%rcx, %rcx
	je	.L117
	movq	24(%r9), %rax
	movq	%rax, (%rcx)
.L117:
	movl	$1, %eax
	testq	%r8, %r8
	je	.L112
	movq	16(%r9), %rdx
	movq	%rdx, (%r8)
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	xorl	%eax, %eax
.L112:
	ret
	.cfi_endproc
.LFE898:
	.size	EVP_PKEY_asn1_get0_info, .-EVP_PKEY_asn1_get0_info
	.p2align 4
	.globl	EVP_PKEY_get0_asn1
	.type	EVP_PKEY_get0_asn1, @function
EVP_PKEY_get0_asn1:
.LFB899:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE899:
	.size	EVP_PKEY_get0_asn1, .-EVP_PKEY_get0_asn1
	.p2align 4
	.globl	EVP_PKEY_asn1_new
	.type	EVP_PKEY_asn1_new, @function
EVP_PKEY_asn1_new:
.LFB900:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edi, %r14d
	movl	$280, %edi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movl	$220, %edx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	leaq	.LC1(%rip), %rsi
	subq	$8, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L147
	orl	$2, %ebx
	movl	%r14d, (%rax)
	movq	%rax, %r12
	movslq	%ebx, %rbx
	movl	%r14d, 4(%rax)
	movq	%rbx, 8(%rax)
	testq	%r15, %r15
	je	.L139
	movl	$230, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	CRYPTO_strdup@PLT
	movq	%rax, 24(%r12)
	testq	%rax, %rax
	je	.L142
.L139:
	testq	%r13, %r13
	je	.L134
	movl	$236, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_strdup@PLT
	movq	%rax, 16(%r12)
	testq	%rax, %rax
	je	.L142
.L134:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore_state
	testb	$2, 8(%r12)
	je	.L147
	movq	16(%r12), %rdi
	movl	$290, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	24(%r12), %rdi
	movl	$291, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$292, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
.L147:
	xorl	%r12d, %r12d
	jmp	.L134
	.cfi_endproc
.LFE900:
	.size	EVP_PKEY_asn1_new, .-EVP_PKEY_asn1_new
	.p2align 4
	.globl	EVP_PKEY_asn1_copy
	.type	EVP_PKEY_asn1_copy, @function
EVP_PKEY_asn1_copy:
.LFB901:
	.cfi_startproc
	endbr64
	movq	32(%rsi), %rax
	movq	%rax, 32(%rdi)
	movq	40(%rsi), %rax
	movq	%rax, 40(%rdi)
	movq	48(%rsi), %rax
	movq	%rax, 48(%rdi)
	movq	56(%rsi), %rax
	movq	%rax, 56(%rdi)
	movq	64(%rsi), %rax
	movq	%rax, 64(%rdi)
	movq	72(%rsi), %rax
	movq	%rax, 72(%rdi)
	movq	80(%rsi), %rax
	movq	%rax, 80(%rdi)
	movq	192(%rsi), %rax
	movq	%rax, 192(%rdi)
	movq	184(%rsi), %rax
	movq	%rax, 184(%rdi)
	movq	88(%rsi), %rax
	movq	%rax, 88(%rdi)
	movq	96(%rsi), %rax
	movq	%rax, 96(%rdi)
	movdqu	128(%rsi), %xmm1
	movdqu	144(%rsi), %xmm0
	movdqu	112(%rsi), %xmm2
	movups	%xmm1, 128(%rdi)
	movups	%xmm2, 112(%rdi)
	movups	%xmm0, 144(%rdi)
	movq	168(%rsi), %rax
	movq	%rax, 168(%rdi)
	movq	176(%rsi), %rax
	movq	%rax, 176(%rdi)
	movq	208(%rsi), %rax
	movq	%rax, 208(%rdi)
	movq	200(%rsi), %rax
	movq	%rax, 200(%rdi)
	movq	216(%rsi), %rax
	movq	%rax, 216(%rdi)
	movq	224(%rsi), %rax
	movq	%rax, 224(%rdi)
	ret
	.cfi_endproc
.LFE901:
	.size	EVP_PKEY_asn1_copy, .-EVP_PKEY_asn1_copy
	.p2align 4
	.globl	EVP_PKEY_asn1_free
	.type	EVP_PKEY_asn1_free, @function
EVP_PKEY_asn1_free:
.LFB902:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L158
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	testb	$2, 8(%rdi)
	jne	.L161
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	movq	16(%rdi), %rdi
	movl	$290, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	24(%r12), %rdi
	movl	$291, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$292, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC1(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L158:
	ret
	.cfi_endproc
.LFE902:
	.size	EVP_PKEY_asn1_free, .-EVP_PKEY_asn1_free
	.p2align 4
	.globl	EVP_PKEY_asn1_set_public
	.type	EVP_PKEY_asn1_set_public, @function
EVP_PKEY_asn1_set_public:
.LFB903:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movups	%xmm0, 32(%rdi)
	movq	%rcx, %xmm0
	movq	%r8, -8(%rbp)
	movhps	-8(%rbp), %xmm0
	movups	%xmm0, 48(%rdi)
	movq	%r9, %xmm0
	movhps	16(%rbp), %xmm0
	movups	%xmm0, 88(%rdi)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE903:
	.size	EVP_PKEY_asn1_set_public, .-EVP_PKEY_asn1_set_public
	.p2align 4
	.globl	EVP_PKEY_asn1_set_private
	.type	EVP_PKEY_asn1_set_private, @function
EVP_PKEY_asn1_set_private:
.LFB904:
	.cfi_startproc
	endbr64
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	movq	%rcx, 80(%rdi)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 64(%rdi)
	ret
	.cfi_endproc
.LFE904:
	.size	EVP_PKEY_asn1_set_private, .-EVP_PKEY_asn1_set_private
	.p2align 4
	.globl	EVP_PKEY_asn1_set_param
	.type	EVP_PKEY_asn1_set_param, @function
EVP_PKEY_asn1_set_param:
.LFB905:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movups	%xmm0, 112(%rdi)
	movq	%rcx, %xmm0
	movq	%r8, -8(%rbp)
	movhps	-8(%rbp), %xmm0
	movups	%xmm0, 128(%rdi)
	movq	%r9, %xmm0
	movhps	16(%rbp), %xmm0
	movups	%xmm0, 144(%rdi)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE905:
	.size	EVP_PKEY_asn1_set_param, .-EVP_PKEY_asn1_set_param
	.p2align 4
	.globl	EVP_PKEY_asn1_set_free
	.type	EVP_PKEY_asn1_set_free, @function
EVP_PKEY_asn1_set_free:
.LFB906:
	.cfi_startproc
	endbr64
	movq	%rsi, 168(%rdi)
	ret
	.cfi_endproc
.LFE906:
	.size	EVP_PKEY_asn1_set_free, .-EVP_PKEY_asn1_set_free
	.p2align 4
	.globl	EVP_PKEY_asn1_set_ctrl
	.type	EVP_PKEY_asn1_set_ctrl, @function
EVP_PKEY_asn1_set_ctrl:
.LFB907:
	.cfi_startproc
	endbr64
	movq	%rsi, 176(%rdi)
	ret
	.cfi_endproc
.LFE907:
	.size	EVP_PKEY_asn1_set_ctrl, .-EVP_PKEY_asn1_set_ctrl
	.p2align 4
	.globl	EVP_PKEY_asn1_set_security_bits
	.type	EVP_PKEY_asn1_set_security_bits, @function
EVP_PKEY_asn1_set_security_bits:
.LFB908:
	.cfi_startproc
	endbr64
	movq	%rsi, 104(%rdi)
	ret
	.cfi_endproc
.LFE908:
	.size	EVP_PKEY_asn1_set_security_bits, .-EVP_PKEY_asn1_set_security_bits
	.p2align 4
	.globl	EVP_PKEY_asn1_set_item
	.type	EVP_PKEY_asn1_set_item, @function
EVP_PKEY_asn1_set_item:
.LFB909:
	.cfi_startproc
	endbr64
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 200(%rdi)
	ret
	.cfi_endproc
.LFE909:
	.size	EVP_PKEY_asn1_set_item, .-EVP_PKEY_asn1_set_item
	.p2align 4
	.globl	EVP_PKEY_asn1_set_siginf
	.type	EVP_PKEY_asn1_set_siginf, @function
EVP_PKEY_asn1_set_siginf:
.LFB910:
	.cfi_startproc
	endbr64
	movq	%rsi, 216(%rdi)
	ret
	.cfi_endproc
.LFE910:
	.size	EVP_PKEY_asn1_set_siginf, .-EVP_PKEY_asn1_set_siginf
	.p2align 4
	.globl	EVP_PKEY_asn1_set_check
	.type	EVP_PKEY_asn1_set_check, @function
EVP_PKEY_asn1_set_check:
.LFB911:
	.cfi_startproc
	endbr64
	movq	%rsi, 224(%rdi)
	ret
	.cfi_endproc
.LFE911:
	.size	EVP_PKEY_asn1_set_check, .-EVP_PKEY_asn1_set_check
	.p2align 4
	.globl	EVP_PKEY_asn1_set_public_check
	.type	EVP_PKEY_asn1_set_public_check, @function
EVP_PKEY_asn1_set_public_check:
.LFB912:
	.cfi_startproc
	endbr64
	movq	%rsi, 232(%rdi)
	ret
	.cfi_endproc
.LFE912:
	.size	EVP_PKEY_asn1_set_public_check, .-EVP_PKEY_asn1_set_public_check
	.p2align 4
	.globl	EVP_PKEY_asn1_set_param_check
	.type	EVP_PKEY_asn1_set_param_check, @function
EVP_PKEY_asn1_set_param_check:
.LFB913:
	.cfi_startproc
	endbr64
	movq	%rsi, 240(%rdi)
	ret
	.cfi_endproc
.LFE913:
	.size	EVP_PKEY_asn1_set_param_check, .-EVP_PKEY_asn1_set_param_check
	.p2align 4
	.globl	EVP_PKEY_asn1_set_set_priv_key
	.type	EVP_PKEY_asn1_set_set_priv_key, @function
EVP_PKEY_asn1_set_set_priv_key:
.LFB914:
	.cfi_startproc
	endbr64
	movq	%rsi, 248(%rdi)
	ret
	.cfi_endproc
.LFE914:
	.size	EVP_PKEY_asn1_set_set_priv_key, .-EVP_PKEY_asn1_set_set_priv_key
	.p2align 4
	.globl	EVP_PKEY_asn1_set_set_pub_key
	.type	EVP_PKEY_asn1_set_set_pub_key, @function
EVP_PKEY_asn1_set_set_pub_key:
.LFB915:
	.cfi_startproc
	endbr64
	movq	%rsi, 256(%rdi)
	ret
	.cfi_endproc
.LFE915:
	.size	EVP_PKEY_asn1_set_set_pub_key, .-EVP_PKEY_asn1_set_set_pub_key
	.p2align 4
	.globl	EVP_PKEY_asn1_set_get_priv_key
	.type	EVP_PKEY_asn1_set_get_priv_key, @function
EVP_PKEY_asn1_set_get_priv_key:
.LFB916:
	.cfi_startproc
	endbr64
	movq	%rsi, 264(%rdi)
	ret
	.cfi_endproc
.LFE916:
	.size	EVP_PKEY_asn1_set_get_priv_key, .-EVP_PKEY_asn1_set_get_priv_key
	.p2align 4
	.globl	EVP_PKEY_asn1_set_get_pub_key
	.type	EVP_PKEY_asn1_set_get_pub_key, @function
EVP_PKEY_asn1_set_get_pub_key:
.LFB917:
	.cfi_startproc
	endbr64
	movq	%rsi, 272(%rdi)
	ret
	.cfi_endproc
.LFE917:
	.size	EVP_PKEY_asn1_set_get_pub_key, .-EVP_PKEY_asn1_set_get_pub_key
	.local	app_methods
	.comm	app_methods,8,8
	.section	.data.rel,"aw"
	.align 32
	.type	standard_methods, @object
	.size	standard_methods, 160
standard_methods:
	.quad	rsa_asn1_meths
	.quad	rsa_asn1_meths+280
	.quad	dh_asn1_meth
	.quad	dsa_asn1_meths
	.quad	dsa_asn1_meths+280
	.quad	dsa_asn1_meths+560
	.quad	dsa_asn1_meths+840
	.quad	dsa_asn1_meths+1120
	.quad	eckey_asn1_meth
	.quad	hmac_asn1_meth
	.quad	cmac_asn1_meth
	.quad	rsa_pss_asn1_meth
	.quad	dhx_asn1_meth
	.quad	ecx25519_asn1_meth
	.quad	ecx448_asn1_meth
	.quad	poly1305_asn1_meth
	.quad	siphash_asn1_meth
	.quad	ed25519_asn1_meth
	.quad	ed448_asn1_meth
	.quad	sm2_asn1_meth
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
