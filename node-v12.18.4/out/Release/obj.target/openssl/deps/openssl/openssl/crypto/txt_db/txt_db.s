	.file	"txt_db.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/txt_db/txt_db.c"
	.text
	.p2align 4
	.globl	TXT_DB_read
	.type	TXT_DB_read, @function
TXT_DB_read:
.LFB275:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -56(%rbp)
	movl	%esi, -60(%rbp)
	call	BUF_MEM_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L2
	movl	$512, %esi
	movq	%rax, %rdi
	call	BUF_MEM_grow@PLT
	testq	%rax, %rax
	jne	.L57
.L3:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	BUF_MEM_free@PLT
.L1:
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	movl	$37, %edx
	leaq	.LC0(%rip), %rsi
	movl	$64, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L3
	movl	%r15d, (%rax)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 8(%r14)
	testq	%rax, %rax
	je	.L27
	movslq	%r15d, %rax
	movl	$44, %edx
	leaq	.LC0(%rip), %rsi
	leaq	0(,%rax,8), %r13
	movq	%rax, -80(%rbp)
	movq	%r13, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, 16(%r14)
	testq	%rax, %rax
	je	.L27
	movl	$46, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, 24(%r14)
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L27
	testl	%r15d, %r15d
	jle	.L10
	movl	-60(%rbp), %eax
	movq	16(%r14), %rdi
	leal	-1(%rax), %ecx
	leaq	8(,%rcx,8), %rdx
	leaq	(%rdi,%rdx), %rax
	cmpq	%rax, %rbx
	jnb	.L32
	leaq	(%rbx,%rdx), %rax
	cmpq	%rax, %rdi
	jnb	.L32
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%rax, %rdx
	movq	$0, (%rdi,%rax,8)
	movq	$0, (%rbx,%rax,8)
	addq	$1, %rax
	cmpq	%rdx, %rcx
	jne	.L8
	jmp	.L10
.L28:
	movl	$113, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	movq	-72(%rbp), %r14
	call	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L27:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	BUF_MEM_free@PLT
	movq	8(%r14), %rdi
	call	OPENSSL_sk_free@PLT
	movq	16(%r14), %rdi
	movl	$123, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	24(%r14), %rdi
	movl	$124, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$125, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L2:
	xorl	%edi, %edi
	call	BUF_MEM_free@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L32:
	xorl	%esi, %esi
	movq	%rdx, -72(%rbp)
	call	memset@PLT
	movq	-72(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	memset@PLT
.L10:
	movl	-60(%rbp), %eax
	movq	%r14, -72(%rbp)
	xorl	%r13d, %r13d
	movl	$512, %ebx
	xorl	%r15d, %r15d
	leal	8(,%rax,8), %ecx
	movq	8(%r12), %rax
	movl	%ecx, -64(%rbp)
	movb	$0, 511(%rax)
	movslq	%ecx, %rax
	movq	8(%r12), %rdx
	movq	%rax, -88(%rbp)
	movq	%rdx, %r14
	.p2align 4,,10
	.p2align 3
.L13:
	movb	$0, (%r14,%r15)
	movq	8(%r12), %rsi
	movl	%ebx, %edx
	movq	-56(%rbp), %rdi
	subl	%r13d, %edx
	addq	%r15, %rsi
	call	BIO_gets@PLT
	movq	8(%r12), %r14
	leaq	(%r14,%r15), %rdi
	cmpb	$0, (%rdi)
	je	.L14
	testl	%r13d, %r13d
	jne	.L15
	cmpb	$35, (%r14)
	je	.L16
.L15:
	call	strlen@PLT
	addl	%eax, %r13d
	movslq	%r13d, %r15
	leaq	-1(%r14,%r15), %rax
	cmpb	$10, (%rax)
	je	.L58
	testl	%r13d, %r13d
	je	.L13
	addl	$512, %ebx
	movq	%r12, %rdi
	movslq	%ebx, %rsi
	call	BUF_MEM_grow_clean@PLT
	testq	%rax, %rax
	je	.L53
	movq	8(%r12), %r14
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L22:
	movb	$0, (%rdx)
	leaq	2(%rdx), %r9
	cmpl	-60(%rbp), %r10d
	jl	.L59
.L20:
	movb	$0, (%rsi)
	cmpl	%r10d, -60(%rbp)
	jne	.L25
	cmpb	$0, (%rcx)
	jne	.L25
	movq	-80(%rbp), %rax
	movq	%r15, %rsi
	movq	%r9, (%r15,%rax,8)
	movq	-72(%rbp), %rax
	movq	8(%rax), %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L28
	movq	8(%r12), %r14
.L16:
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L58:
	movb	$0, (%rax)
	movl	-64(%rbp), %eax
	movl	$75, %edx
	leaq	.LC0(%rip), %rsi
	leal	(%rax,%r13), %edi
	movslq	%edi, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L53
	movq	-88(%rbp), %rax
	leaq	(%r15,%rax), %rdx
	movq	%rdx, (%r15)
	movq	8(%r12), %rcx
	movzbl	(%rcx), %eax
	testb	%al, %al
	je	.L18
	movl	$1, %r10d
	xorl	%r8d, %r8d
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L21:
	leaq	2(%rdx), %r9
	movq	%rdx, %rdi
	movq	%rsi, %rdx
.L23:
	movb	%al, (%rdi)
	xorl	%r8d, %r8d
	cmpb	$92, %al
	sete	%r8b
.L24:
	movzbl	(%rcx), %eax
	testb	%al, %al
	je	.L31
.L19:
	leaq	1(%rdx), %rsi
	addq	$1, %rcx
	cmpb	$9, %al
	jne	.L21
	testl	%r8d, %r8d
	je	.L22
	leaq	-1(%rdx), %rdi
	movq	%rsi, %r9
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L59:
	movslq	%r10d, %rax
	movq	%rsi, %rdx
	addl	$1, %r10d
	movq	%rsi, (%r15,%rax,8)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L31:
	movq	%rdx, %rsi
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L14:
	movq	-72(%rbp), %r14
	movq	%r12, %rdi
	call	BUF_MEM_free@PLT
	movq	%r14, %r12
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L53:
	movq	-72(%rbp), %r14
	jmp	.L27
.L18:
	leaq	1(%rdx), %r9
	movq	%rdx, %rsi
	movl	$1, %r10d
	jmp	.L20
.L25:
	movq	-72(%rbp), %r14
	movl	$107, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	call	CRYPTO_free@PLT
	movq	$6, 32(%r14)
	jmp	.L27
	.cfi_endproc
.LFE275:
	.size	TXT_DB_read, .-TXT_DB_read
	.p2align 4
	.globl	TXT_DB_get_by_index
	.type	TXT_DB_get_by_index, @function
TXT_DB_get_by_index:
.LFB276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpl	%esi, (%rdi)
	jle	.L65
	movq	16(%rdi), %rax
	movslq	%esi, %rsi
	movq	(%rax,%rsi,8), %rax
	testq	%rax, %rax
	je	.L66
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	OPENSSL_LH_retrieve@PLT
	movq	$0, 32(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	movq	$3, 32(%rdi)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movq	$4, 32(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE276:
	.size	TXT_DB_get_by_index, .-TXT_DB_get_by_index
	.p2align 4
	.globl	TXT_DB_create_index
	.type	TXT_DB_create_index, @function
TXT_DB_create_index:
.LFB277:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	%esi, -56(%rbp)
	cmpl	%esi, (%rdi)
	jg	.L68
	movq	$3, 32(%rdi)
	xorl	%eax, %eax
.L67:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	movq	%rcx, %rdi
	movq	%r8, %rsi
	movq	%rdx, %r14
	call	OPENSSL_LH_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L89
	movq	8(%r12), %rdi
	call	OPENSSL_sk_num@PLT
	movl	%eax, -52(%rbp)
	testl	%eax, %eax
	jle	.L71
	xorl	%ebx, %ebx
	testq	%r14, %r14
	je	.L75
	.p2align 4,,10
	.p2align 3
.L72:
	movq	8(%r12), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	*%r14
	testl	%eax, %eax
	je	.L76
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	OPENSSL_LH_insert@PLT
	testq	%rax, %rax
	jne	.L73
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	OPENSSL_LH_retrieve@PLT
	testq	%rax, %rax
	je	.L74
.L76:
	addl	$1, %ebx
	cmpl	-52(%rbp), %ebx
	jne	.L72
.L71:
	movslq	-56(%rbp), %rbx
	movq	16(%r12), %rax
	movq	(%rax,%rbx,8), %rdi
	call	OPENSSL_LH_free@PLT
	movq	16(%r12), %rax
	movq	%r13, (%rax,%rbx,8)
	movq	24(%r12), %rax
	movq	%r14, (%rax,%rbx,8)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	OPENSSL_LH_retrieve@PLT
	testq	%rax, %rax
	je	.L74
	addl	$1, %ebx
	cmpl	%ebx, -52(%rbp)
	je	.L71
.L75:
	movq	8(%r12), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	OPENSSL_LH_insert@PLT
	testq	%rax, %rax
	je	.L90
.L73:
	movq	$2, 32(%r12)
	movq	8(%r12), %rdi
	movq	%rax, %rsi
	movslq	%ebx, %rbx
	call	OPENSSL_sk_find@PLT
	movq	%rbx, %xmm1
	movq	%r13, %rdi
	cltq
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%r12)
	call	OPENSSL_LH_free@PLT
	xorl	%eax, %eax
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L89:
	movq	$1, 32(%r12)
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	movq	$1, 32(%r12)
	movq	%r13, %rdi
	call	OPENSSL_LH_free@PLT
	xorl	%eax, %eax
	jmp	.L67
	.cfi_endproc
.LFE277:
	.size	TXT_DB_create_index, .-TXT_DB_create_index
	.p2align 4
	.globl	TXT_DB_write
	.type	TXT_DB_write, @function
TXT_DB_write:
.LFB278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -88(%rbp)
	movq	%rsi, -80(%rbp)
	call	BUF_MEM_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L104
	movq	-80(%rbp), %rbx
	movq	8(%rbx), %rdi
	call	OPENSSL_sk_num@PLT
	movl	(%rbx), %ebx
	cltq
	movq	%rax, -96(%rbp)
	movl	%ebx, -60(%rbp)
	testq	%rax, %rax
	jle	.L109
	movslq	%ebx, %rax
	movq	$0, -72(%rbp)
	movq	%rax, -112(%rbp)
	salq	$3, %rax
	movq	%rax, -104(%rbp)
	movq	$0, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L105:
	movq	-80(%rbp), %rax
	movl	-56(%rbp), %esi
	movq	8(%rax), %rdi
	call	OPENSSL_sk_value@PLT
	movl	-60(%rbp), %edx
	testl	%edx, %edx
	jle	.L94
	movq	%rax, %r15
	movq	-104(%rbp), %rax
	xorl	%ebx, %ebx
	movq	%r15, %r12
	leaq	(%r15,%rax), %r14
	.p2align 4,,10
	.p2align 3
.L96:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L95
	call	strlen@PLT
	addq	%rax, %rbx
.L95:
	addq	$8, %r12
	cmpq	%r12, %r14
	jne	.L96
	movl	-60(%rbp), %eax
	movq	%r13, %rdi
	leal	(%rax,%rbx,2), %esi
	movslq	%esi, %rsi
	call	BUF_MEM_grow_clean@PLT
	testq	%rax, %rax
	je	.L104
	movq	8(%r13), %rax
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L111:
	movq	%rax, %rsi
.L135:
	addq	$8, %r15
	movb	$9, (%rsi)
	movq	%rdi, %rax
	cmpq	%r15, %r14
	je	.L106
.L103:
	movq	(%r15), %rdx
	leaq	1(%rax), %rdi
	testq	%rdx, %rdx
	je	.L111
	movzbl	(%rdx), %ecx
	testb	%cl, %cl
	je	.L111
	movq	%rdi, %rsi
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L99:
	addq	$1, %rdx
	movb	%cl, (%rax)
	movzbl	(%rdx), %ecx
	testb	%cl, %cl
	je	.L135
.L101:
	movq	%rsi, %rax
	leaq	1(%rsi), %rsi
.L102:
	leaq	2(%rax), %rdi
	cmpb	$9, %cl
	jne	.L99
	movb	$92, (%rax)
	movzbl	(%rdx), %ecx
	addq	$1, %rdx
	leaq	3(%rax), %r8
	movq	%rdi, %rsi
	movb	%cl, 1(%rax)
	movzbl	(%rdx), %ecx
	testb	%cl, %cl
	jne	.L101
	movq	%r8, %rdi
	addq	$8, %r15
	movb	$9, (%rsi)
	movq	%rdi, %rax
	cmpq	%r15, %r14
	jne	.L103
	.p2align 4,,10
	.p2align 3
.L106:
	movb	$10, -1(%rax)
	movq	8(%r13), %rsi
	movq	-88(%rbp), %rdi
	subq	%rsi, %rax
	movl	%eax, %edx
	movq	%rax, %rbx
	call	BIO_write@PLT
	cltq
	cmpq	%rbx, %rax
	jne	.L104
	addq	%rax, -72(%rbp)
	addq	$1, -56(%rbp)
	movq	-56(%rbp), %rax
	cmpq	%rax, -96(%rbp)
	jne	.L105
.L93:
	movq	%r13, %rdi
	call	BUF_MEM_free@PLT
	movq	-72(%rbp), %rax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L94:
	.cfi_restore_state
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	BUF_MEM_grow_clean@PLT
	testq	%rax, %rax
	je	.L104
	movq	8(%r13), %rax
	jmp	.L106
.L104:
	movq	$-1, -72(%rbp)
	jmp	.L93
.L109:
	movq	$0, -72(%rbp)
	jmp	.L93
	.cfi_endproc
.LFE278:
	.size	TXT_DB_write, .-TXT_DB_write
	.p2align 4
	.globl	TXT_DB_insert
	.type	TXT_DB_insert, @function
TXT_DB_insert:
.LFB279:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	(%rdi), %eax
	testl	%eax, %eax
	jle	.L137
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L143:
	movq	16(%r12), %rdx
	movslq	%ebx, %r13
	leaq	0(,%rbx,8), %r14
	movq	(%rdx,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.L138
	movq	24(%r12), %rax
	movq	(%rax,%rbx,8), %rax
	testq	%rax, %rax
	je	.L139
	movq	%r15, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L183
	movq	16(%r12), %rax
	movq	(%rax,%r14), %rdi
.L139:
	movq	%r15, %rsi
	call	OPENSSL_LH_retrieve@PLT
	testq	%rax, %rax
	jne	.L141
.L183:
	movl	(%r12), %eax
.L138:
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jg	.L143
	testl	%eax, %eax
	jle	.L137
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L150:
	movq	16(%r12), %rdx
	movl	%ebx, %r14d
	leaq	0(,%rbx,8), %r13
	movq	(%rdx,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.L144
	movq	24(%r12), %rax
	movq	(%rax,%rbx,8), %rax
	testq	%rax, %rax
	je	.L145
	movq	%r15, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L147
	movq	16(%r12), %rax
	movq	(%rax,%r13), %rdi
.L145:
	movq	%r15, %rsi
	call	OPENSSL_LH_insert@PLT
	movq	16(%r12), %rax
	movq	%r15, %rsi
	movq	(%rax,%r13), %rdi
	call	OPENSSL_LH_retrieve@PLT
	testq	%rax, %rax
	je	.L184
.L147:
	movl	(%r12), %eax
.L144:
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jg	.L150
	movq	8(%r12), %rdi
	movq	%r15, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L185
.L159:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	.cfi_restore_state
	movq	%r13, 40(%r12)
	movq	%rax, 56(%r12)
	xorl	%eax, %eax
	movq	$2, 32(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	movq	$1, 32(%r12)
	leal	-1(%r14), %r13d
	testl	%r14d, %r14d
	je	.L149
.L148:
	movslq	%r13d, %rbx
	salq	$3, %rbx
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L160:
	movl	%eax, %r13d
.L157:
	movq	16(%r12), %rax
	movq	(%rax,%rbx), %rdi
	testq	%rdi, %rdi
	je	.L154
	movq	24(%r12), %rax
	movq	(%rax,%rbx), %rax
	testq	%rax, %rax
	je	.L155
	movq	%r15, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L154
	movq	16(%r12), %rax
	movq	(%rax,%rbx), %rdi
.L155:
	movq	%r15, %rsi
	call	OPENSSL_LH_delete@PLT
.L154:
	leal	-1(%r13), %eax
	subq	$8, %rbx
	testl	%r13d, %r13d
	jg	.L160
.L149:
	xorl	%eax, %eax
.L186:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	.cfi_restore_state
	movq	$1, 32(%r12)
	movl	%r14d, %r13d
	jmp	.L148
.L137:
	movq	8(%r12), %rdi
	movq	%r15, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L159
	movq	$1, 32(%r12)
	xorl	%eax, %eax
	jmp	.L186
	.cfi_endproc
.LFE279:
	.size	TXT_DB_insert, .-TXT_DB_insert
	.p2align 4
	.globl	TXT_DB_free
	.type	TXT_DB_free, @function
TXT_DB_free:
.LFB280:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L187
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L189
	movslq	(%r12), %rax
	movl	%eax, %edx
	subl	$1, %edx
	js	.L190
	movslq	%edx, %rbx
	movl	%edx, %edx
	subq	%rdx, %rax
	salq	$3, %rbx
	leaq	-16(,%rax,8), %r13
	.p2align 4,,10
	.p2align 3
.L192:
	movq	(%rdi,%rbx), %rdi
	subq	$8, %rbx
	call	OPENSSL_LH_free@PLT
	movq	16(%r12), %rdi
	cmpq	%rbx, %r13
	jne	.L192
.L190:
	movl	$292, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
.L189:
	movq	24(%r12), %rdi
	movl	$294, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L193
	call	OPENSSL_sk_num@PLT
	leaq	.LC0(%rip), %r13
	subl	$1, %eax
	movl	%eax, %r15d
	js	.L197
	.p2align 4,,10
	.p2align 3
.L194:
	movq	8(%r12), %rdi
	movl	%r15d, %esi
	call	OPENSSL_sk_value@PLT
	movslq	(%r12), %rcx
	movq	%rax, %r14
	movq	(%rax,%rcx,8), %rax
	movq	%rcx, %rdx
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L195
	xorl	%ebx, %ebx
	testl	%ecx, %ecx
	jle	.L198
	.p2align 4,,10
	.p2align 3
.L196:
	movq	(%r14,%rbx,8), %rdi
	cmpq	%r14, %rdi
	jb	.L204
	cmpq	-56(%rbp), %rdi
	ja	.L204
	addq	$1, %rbx
	cmpl	%ebx, %edx
	jg	.L196
.L198:
	movq	8(%r12), %rdi
	movl	%r15d, %esi
	subl	$1, %r15d
	call	OPENSSL_sk_value@PLT
	movl	$312, %edx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	CRYPTO_free@PLT
	cmpl	$-1, %r15d
	jne	.L194
.L197:
	movq	8(%r12), %rdi
	call	OPENSSL_sk_free@PLT
.L193:
	addq	$24, %rsp
	movq	%r12, %rdi
	movl	$316, %edx
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	leaq	.LC0(%rip), %rsi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L204:
	.cfi_restore_state
	movl	$309, %edx
	movq	%r13, %rsi
	addq	$1, %rbx
	call	CRYPTO_free@PLT
	movl	(%r12), %edx
	cmpl	%ebx, %edx
	jg	.L196
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L195:
	testl	%ecx, %ecx
	jle	.L198
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L199:
	movq	(%r14,%rbx,8), %rdi
	movl	$305, %edx
	movq	%r13, %rsi
	addq	$1, %rbx
	call	CRYPTO_free@PLT
	cmpl	%ebx, (%r12)
	jg	.L199
	jmp	.L198
.L187:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE280:
	.size	TXT_DB_free, .-TXT_DB_free
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
