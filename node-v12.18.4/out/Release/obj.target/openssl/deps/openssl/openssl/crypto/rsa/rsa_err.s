	.file	"rsa_err.c"
	.text
	.p2align 4
	.globl	ERR_load_RSA_strings
	.type	ERR_load_RSA_strings, @function
ERR_load_RSA_strings:
.LFB203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$67682304, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_func_error_string@PLT
	testq	%rax, %rax
	je	.L5
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	leaq	RSA_str_functs(%rip), %rdi
	call	ERR_load_strings_const@PLT
	leaq	RSA_str_reasons(%rip), %rdi
	call	ERR_load_strings_const@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE203:
	.size	ERR_load_RSA_strings, .-ERR_load_RSA_strings
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"algorithm mismatch"
.LC1:
	.string	"bad e value"
.LC2:
	.string	"bad fixed header decrypt"
.LC3:
	.string	"bad pad byte count"
.LC4:
	.string	"bad signature"
.LC5:
	.string	"block type is not 01"
.LC6:
	.string	"block type is not 02"
.LC7:
	.string	"data greater than mod len"
.LC8:
	.string	"data too large"
.LC9:
	.string	"data too large for key size"
.LC10:
	.string	"data too large for modulus"
.LC11:
	.string	"data too small"
.LC12:
	.string	"data too small for key size"
.LC13:
	.string	"digest does not match"
.LC14:
	.string	"digest not allowed"
.LC15:
	.string	"digest too big for rsa key"
.LC16:
	.string	"dmp1 not congruent to d"
.LC17:
	.string	"dmq1 not congruent to d"
.LC18:
	.string	"d e not congruent to 1"
.LC19:
	.string	"first octet invalid"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC20:
	.string	"illegal or unsupported padding mode"
	.section	.rodata.str1.1
.LC21:
	.string	"invalid digest"
.LC22:
	.string	"invalid digest length"
.LC23:
	.string	"invalid header"
.LC24:
	.string	"invalid label"
.LC25:
	.string	"invalid message length"
.LC26:
	.string	"invalid mgf1 md"
.LC27:
	.string	"invalid multi prime key"
.LC28:
	.string	"invalid oaep parameters"
.LC29:
	.string	"invalid padding"
.LC30:
	.string	"invalid padding mode"
.LC31:
	.string	"invalid pss parameters"
.LC32:
	.string	"invalid pss saltlen"
.LC33:
	.string	"invalid salt length"
.LC34:
	.string	"invalid trailer"
.LC35:
	.string	"invalid x931 digest"
.LC36:
	.string	"iqmp not inverse of q"
.LC37:
	.string	"key prime num invalid"
.LC38:
	.string	"key size too small"
.LC39:
	.string	"last octet invalid"
.LC40:
	.string	"mgf1 digest not allowed"
.LC41:
	.string	"missing private key"
.LC42:
	.string	"modulus too large"
	.section	.rodata.str1.8
	.align 8
.LC43:
	.string	"mp coefficient not inverse of r"
	.align 8
.LC44:
	.string	"mp exponent not congruent to d"
	.section	.rodata.str1.1
.LC45:
	.string	"mp r not prime"
.LC46:
	.string	"no public exponent"
.LC47:
	.string	"null before block missing"
	.section	.rodata.str1.8
	.align 8
.LC48:
	.string	"n does not equal product of primes"
	.section	.rodata.str1.1
.LC49:
	.string	"n does not equal p q"
.LC50:
	.string	"oaep decoding error"
	.section	.rodata.str1.8
	.align 8
.LC51:
	.string	"operation not supported for this keytype"
	.section	.rodata.str1.1
.LC52:
	.string	"padding check failed"
.LC53:
	.string	"pkcs decoding error"
.LC54:
	.string	"pss saltlen too small"
.LC55:
	.string	"p not prime"
.LC56:
	.string	"q not prime"
.LC57:
	.string	"rsa operations not supported"
.LC58:
	.string	"salt length check failed"
.LC59:
	.string	"salt length recovery failed"
.LC60:
	.string	"sslv3 rollback attack"
	.section	.rodata.str1.8
	.align 8
.LC61:
	.string	"the asn1 object identifier is not known for this md"
	.section	.rodata.str1.1
.LC62:
	.string	"unknown algorithm type"
.LC63:
	.string	"unknown digest"
.LC64:
	.string	"unknown mask digest"
.LC65:
	.string	"unknown padding type"
.LC66:
	.string	"unsupported encryption type"
.LC67:
	.string	"unsupported label source"
.LC68:
	.string	"unsupported mask algorithm"
.LC69:
	.string	"unsupported mask parameter"
.LC70:
	.string	"unsupported signature type"
.LC71:
	.string	"value missing"
.LC72:
	.string	"wrong signature length"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	RSA_str_reasons, @object
	.size	RSA_str_reasons, 1184
RSA_str_reasons:
	.quad	67108964
	.quad	.LC0
	.quad	67108965
	.quad	.LC1
	.quad	67108966
	.quad	.LC2
	.quad	67108967
	.quad	.LC3
	.quad	67108968
	.quad	.LC4
	.quad	67108970
	.quad	.LC5
	.quad	67108971
	.quad	.LC6
	.quad	67108972
	.quad	.LC7
	.quad	67108973
	.quad	.LC8
	.quad	67108974
	.quad	.LC9
	.quad	67108996
	.quad	.LC10
	.quad	67108975
	.quad	.LC11
	.quad	67108986
	.quad	.LC12
	.quad	67109022
	.quad	.LC13
	.quad	67109009
	.quad	.LC14
	.quad	67108976
	.quad	.LC15
	.quad	67108988
	.quad	.LC16
	.quad	67108989
	.quad	.LC17
	.quad	67108987
	.quad	.LC18
	.quad	67108997
	.quad	.LC19
	.quad	67109008
	.quad	.LC20
	.quad	67109021
	.quad	.LC21
	.quad	67109007
	.quad	.LC22
	.quad	67109001
	.quad	.LC23
	.quad	67109024
	.quad	.LC24
	.quad	67108995
	.quad	.LC25
	.quad	67109020
	.quad	.LC26
	.quad	67109031
	.quad	.LC27
	.quad	67109025
	.quad	.LC28
	.quad	67109002
	.quad	.LC29
	.quad	67109005
	.quad	.LC30
	.quad	67109013
	.quad	.LC31
	.quad	67109010
	.quad	.LC32
	.quad	67109014
	.quad	.LC33
	.quad	67109003
	.quad	.LC34
	.quad	67109006
	.quad	.LC35
	.quad	67108990
	.quad	.LC36
	.quad	67109029
	.quad	.LC37
	.quad	67108984
	.quad	.LC38
	.quad	67108998
	.quad	.LC39
	.quad	67109016
	.quad	.LC40
	.quad	67109043
	.quad	.LC41
	.quad	67108969
	.quad	.LC42
	.quad	67109032
	.quad	.LC43
	.quad	67109033
	.quad	.LC44
	.quad	67109034
	.quad	.LC45
	.quad	67109004
	.quad	.LC46
	.quad	67108977
	.quad	.LC47
	.quad	67109036
	.quad	.LC48
	.quad	67108991
	.quad	.LC49
	.quad	67108985
	.quad	.LC50
	.quad	67109012
	.quad	.LC51
	.quad	67108978
	.quad	.LC52
	.quad	67109023
	.quad	.LC53
	.quad	67109028
	.quad	.LC54
	.quad	67108992
	.quad	.LC55
	.quad	67108993
	.quad	.LC56
	.quad	67108994
	.quad	.LC57
	.quad	67109000
	.quad	.LC58
	.quad	67108999
	.quad	.LC59
	.quad	67108979
	.quad	.LC60
	.quad	67108980
	.quad	.LC61
	.quad	67108981
	.quad	.LC62
	.quad	67109030
	.quad	.LC63
	.quad	67109015
	.quad	.LC64
	.quad	67108982
	.quad	.LC65
	.quad	67109026
	.quad	.LC66
	.quad	67109027
	.quad	.LC67
	.quad	67109017
	.quad	.LC68
	.quad	67109018
	.quad	.LC69
	.quad	67109019
	.quad	.LC70
	.quad	67109011
	.quad	.LC71
	.quad	67108983
	.quad	.LC72
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC73:
	.string	"check_padding_md"
.LC74:
	.string	"encode_pkcs1"
.LC75:
	.string	"int_rsa_verify"
.LC76:
	.string	"old_rsa_priv_decode"
.LC77:
	.string	"pkey_pss_init"
.LC78:
	.string	"pkey_rsa_ctrl"
.LC79:
	.string	"pkey_rsa_ctrl_str"
.LC80:
	.string	"pkey_rsa_sign"
.LC81:
	.string	"pkey_rsa_verify"
.LC82:
	.string	"pkey_rsa_verifyrecover"
.LC83:
	.string	"rsa_algor_to_md"
.LC84:
	.string	"rsa_builtin_keygen"
.LC85:
	.string	"RSA_check_key"
.LC86:
	.string	"RSA_check_key_ex"
.LC87:
	.string	"rsa_cms_decrypt"
.LC88:
	.string	"rsa_cms_verify"
.LC89:
	.string	"rsa_item_verify"
.LC90:
	.string	"RSA_meth_dup"
.LC91:
	.string	"RSA_meth_new"
.LC92:
	.string	"RSA_meth_set1_name"
.LC93:
	.string	""
.LC94:
	.string	"rsa_multip_info_new"
.LC95:
	.string	"RSA_new_method"
.LC96:
	.string	"rsa_ossl_private_decrypt"
.LC97:
	.string	"rsa_ossl_private_encrypt"
.LC98:
	.string	"rsa_ossl_public_decrypt"
.LC99:
	.string	"rsa_ossl_public_encrypt"
.LC100:
	.string	"RSA_padding_add_none"
.LC101:
	.string	"RSA_padding_add_PKCS1_OAEP"
	.section	.rodata.str1.8
	.align 8
.LC102:
	.string	"RSA_padding_add_PKCS1_OAEP_mgf1"
	.section	.rodata.str1.1
.LC103:
	.string	"RSA_padding_add_PKCS1_PSS"
	.section	.rodata.str1.8
	.align 8
.LC104:
	.string	"RSA_padding_add_PKCS1_PSS_mgf1"
	.section	.rodata.str1.1
.LC105:
	.string	"RSA_padding_add_PKCS1_type_1"
.LC106:
	.string	"RSA_padding_add_PKCS1_type_2"
.LC107:
	.string	"RSA_padding_add_SSLv23"
.LC108:
	.string	"RSA_padding_add_X931"
.LC109:
	.string	"RSA_padding_check_none"
.LC110:
	.string	"RSA_padding_check_PKCS1_OAEP"
	.section	.rodata.str1.8
	.align 8
.LC111:
	.string	"RSA_padding_check_PKCS1_OAEP_mgf1"
	.align 8
.LC112:
	.string	"RSA_padding_check_PKCS1_type_1"
	.align 8
.LC113:
	.string	"RSA_padding_check_PKCS1_type_2"
	.section	.rodata.str1.1
.LC114:
	.string	"RSA_padding_check_SSLv23"
.LC115:
	.string	"RSA_padding_check_X931"
.LC116:
	.string	"rsa_param_decode"
.LC117:
	.string	"RSA_print"
.LC118:
	.string	"RSA_print_fp"
.LC119:
	.string	"rsa_priv_decode"
.LC120:
	.string	"rsa_priv_encode"
.LC121:
	.string	"rsa_pss_get_param"
.LC122:
	.string	"rsa_pss_to_ctx"
.LC123:
	.string	"rsa_pub_decode"
.LC124:
	.string	"RSA_setup_blinding"
.LC125:
	.string	"RSA_sign"
.LC126:
	.string	"RSA_sign_ASN1_OCTET_STRING"
.LC127:
	.string	"RSA_verify"
.LC128:
	.string	"RSA_verify_ASN1_OCTET_STRING"
.LC129:
	.string	"RSA_verify_PKCS1_PSS_mgf1"
.LC130:
	.string	"setup_tbuf"
	.section	.data.rel.ro.local
	.align 32
	.type	RSA_str_functs, @object
	.size	RSA_str_functs, 1024
RSA_str_functs:
	.quad	67682304
	.quad	.LC73
	.quad	67706880
	.quad	.LC74
	.quad	67702784
	.quad	.LC75
	.quad	67710976
	.quad	.LC76
	.quad	67784704
	.quad	.LC77
	.quad	67694592
	.quad	.LC78
	.quad	67698688
	.quad	.LC79
	.quad	67690496
	.quad	.LC80
	.quad	67719168
	.quad	.LC81
	.quad	67686400
	.quad	.LC82
	.quad	67747840
	.quad	.LC83
	.quad	67637248
	.quad	.LC84
	.quad	67612672
	.quad	.LC85
	.quad	67764224
	.quad	.LC86
	.quad	67760128
	.quad	.LC87
	.quad	67756032
	.quad	.LC88
	.quad	67715072
	.quad	.LC89
	.quad	67768320
	.quad	.LC90
	.quad	67772416
	.quad	.LC91
	.quad	67776512
	.quad	.LC92
	.quad	67751936
	.quad	.LC93
	.quad	67788800
	.quad	.LC94
	.quad	67543040
	.quad	.LC95
	.quad	67616768
	.quad	.LC93
	.quad	67649536
	.quad	.LC93
	.quad	67653632
	.quad	.LC93
	.quad	67657728
	.quad	.LC93
	.quad	67661824
	.quad	.LC93
	.quad	67522560
	.quad	.LC96
	.quad	67526656
	.quad	.LC97
	.quad	67530752
	.quad	.LC98
	.quad	67534848
	.quad	.LC99
	.quad	67547136
	.quad	.LC100
	.quad	67604480
	.quad	.LC101
	.quad	67739648
	.quad	.LC102
	.quad	67620864
	.quad	.LC103
	.quad	67731456
	.quad	.LC104
	.quad	67551232
	.quad	.LC105
	.quad	67555328
	.quad	.LC106
	.quad	67559424
	.quad	.LC107
	.quad	67629056
	.quad	.LC108
	.quad	67563520
	.quad	.LC109
	.quad	67608576
	.quad	.LC110
	.quad	67735552
	.quad	.LC111
	.quad	67567616
	.quad	.LC112
	.quad	67571712
	.quad	.LC113
	.quad	67575808
	.quad	.LC114
	.quad	67633152
	.quad	.LC115
	.quad	67780608
	.quad	.LC116
	.quad	67579904
	.quad	.LC117
	.quad	67584000
	.quad	.LC118
	.quad	67723264
	.quad	.LC119
	.quad	67674112
	.quad	.LC120
	.quad	67727360
	.quad	.LC121
	.quad	67743744
	.quad	.LC122
	.quad	67678208
	.quad	.LC123
	.quad	67665920
	.quad	.LC124
	.quad	67588096
	.quad	.LC125
	.quad	67592192
	.quad	.LC126
	.quad	67596288
	.quad	.LC127
	.quad	67600384
	.quad	.LC128
	.quad	67624960
	.quad	.LC129
	.quad	67792896
	.quad	.LC130
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
