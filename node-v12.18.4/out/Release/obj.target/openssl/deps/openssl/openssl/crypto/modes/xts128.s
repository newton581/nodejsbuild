	.file	"xts128.c"
	.text
	.p2align 4
	.globl	CRYPTO_xts128_encrypt
	.type	CRYPTO_xts128_encrypt, @function
CRYPTO_xts128_encrypt:
.LFB151:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r9d, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$15, %r8
	jbe	.L41
	movdqu	(%rsi), %xmm3
	movq	%rdi, %r13
	movq	%rdx, %r12
	movl	%r9d, %ebx
	movq	8(%rdi), %rdx
	leaq	-96(%rbp), %rdi
	movq	%rcx, %r14
	movq	%r8, %r15
	movaps	%xmm3, -96(%rbp)
	movq	%rdi, %rsi
	call	*24(%r13)
	testl	%ebx, %ebx
	je	.L164
.L3:
	movq	-96(%rbp), %rax
	movq	-88(%rbp), %rdx
	leaq	-80(%rbp), %rbx
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L166:
	movl	-84(%rbp), %eax
	leaq	(%rdx,%rdx), %rsi
	addq	%rcx, %rcx
	shrq	$63, %rdx
	orq	%rcx, %rdx
	sarl	$31, %eax
	movq	%rdx, -88(%rbp)
	andl	$135, %eax
	xorq	%rsi, %rax
	movq	%rax, -96(%rbp)
	cmpq	$15, %r15
	jbe	.L165
.L5:
	movdqu	(%r12), %xmm2
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	addq	$16, %r12
	movq	%rax, %xmm0
	movq	%rdx, %xmm1
	movq	0(%r13), %rdx
	addq	$16, %r14
	punpcklqdq	%xmm1, %xmm0
	pxor	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	*16(%r13)
	movq	-80(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	-88(%rbp), %rcx
	xorq	%rdx, %rax
	movq	%rax, -16(%r14)
	movq	%rax, -80(%rbp)
	movq	-72(%rbp), %rax
	xorq	%rcx, %rax
	movq	%rax, -8(%r14)
	movq	%rax, -72(%rbp)
	subq	$16, %r15
	jne	.L166
	xorl	%eax, %eax
.L1:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L167
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	testb	$15, %r15b
	je	.L3
	subq	$16, %r15
	cmpq	$15, %r15
	ja	.L3
	movq	-96(%rbp), %rax
	movq	-88(%rbp), %rdx
	leaq	-80(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L39:
	movl	-84(%rbp), %ecx
	leaq	(%rax,%rax), %rsi
	addq	%rdx, %rdx
	shrq	$63, %rax
	orq	%rdx, %rax
	movq	%rbx, %rdi
	sarl	$31, %ecx
	movq	%rax, -104(%rbp)
	andl	$135, %ecx
	xorq	%rsi, %rcx
	movq	%rbx, %rsi
	movq	%rcx, %rdx
	xorq	(%r12), %rdx
	movq	%rcx, -112(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%rax, %rdx
	xorq	8(%r12), %rdx
	movq	%rdx, -72(%rbp)
	movq	0(%r13), %rdx
	call	*16(%r13)
	movq	-112(%rbp), %rcx
	movq	-104(%rbp), %rax
	xorq	-80(%rbp), %rcx
	xorq	-72(%rbp), %rax
	movq	%rcx, -80(%rbp)
	movq	%rax, %rdx
	movq	%rax, -72(%rbp)
	testq	%r15, %r15
	je	.L22
	leaq	16(%r15), %rdx
	leaq	16(%r14), %rax
	leaq	(%r12,%rdx), %rsi
	leaq	16(%r12), %rcx
	cmpq	%rsi, %rax
	jnb	.L44
	addq	%r14, %rdx
	cmpq	%rdx, %rcx
	jb	.L23
.L44:
	movl	%r15d, %edx
	cmpl	$8, %r15d
	jb	.L168
	movq	-80(%rbp), %rdx
	leaq	8(%rax), %r8
	movq	%rbx, %r10
	andq	$-8, %r8
	movq	%rdx, (%rax)
	movl	%r15d, %edx
	movq	-8(%rbx,%rdx), %rsi
	movq	%rsi, -8(%rax,%rdx)
	subq	%r8, %rax
	leal	(%r15,%rax), %edx
	subq	%rax, %r10
	andl	$-8, %edx
	cmpl	$8, %edx
	jb	.L26
	movl	%edx, %eax
	xorl	%edx, %edx
	andl	$-8, %eax
.L29:
	movl	%edx, %esi
	addl	$8, %edx
	movq	(%r10,%rsi), %r9
	movq	%r9, (%r8,%rsi)
	cmpl	%eax, %edx
	jb	.L29
.L26:
	movl	%r15d, %esi
	movq	%rbx, %r8
	movq	%rcx, %rax
	cmpl	$8, %r15d
	jnb	.L169
.L31:
	xorl	%edx, %edx
	testb	$4, %sil
	je	.L34
	movl	(%rax), %edx
	movl	%edx, (%r8)
	movl	$4, %edx
.L34:
	testb	$2, %sil
	je	.L35
	movzwl	(%rax,%rdx), %ecx
	movw	%cx, (%r8,%rdx)
	addq	$2, %rdx
.L35:
	andl	$1, %esi
	je	.L37
	movzbl	(%rax,%rdx), %eax
	movb	%al, (%r8,%rdx)
.L37:
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %rdx
.L22:
	movq	%rdx, %xmm4
	movq	%rcx, %xmm0
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	punpcklqdq	%xmm4, %xmm0
	pxor	-96(%rbp), %xmm0
	movq	0(%r13), %rdx
	movaps	%xmm0, -80(%rbp)
	call	*16(%r13)
	movdqa	-80(%rbp), %xmm0
	pxor	-96(%rbp), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%r14)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L165:
	movl	-104(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L39
	testq	%r15, %r15
	movl	$1, %eax
	cmovne	%r15, %rax
	leaq	(%r12,%rax), %rdx
	cmpq	%rdx, %r14
	jnb	.L43
	leaq	(%r14,%rax), %rdx
	cmpq	%rdx, %r12
	jb	.L7
.L43:
	cmpl	$8, %eax
	jb	.L170
	movq	-80(%rbp), %rdx
	leaq	8(%r14), %r8
	movq	%rbx, %r10
	andq	$-8, %r8
	movq	%rdx, (%r14)
	movl	%eax, %edx
	movq	-8(%rbx,%rdx), %rcx
	movq	%rcx, -8(%r14,%rdx)
	movq	%r14, %rdx
	subq	%r8, %rdx
	subq	%rdx, %r10
	addl	%eax, %edx
	andl	$-8, %edx
	cmpl	$8, %edx
	jb	.L10
	andl	$-8, %edx
	xorl	%ecx, %ecx
.L13:
	movl	%ecx, %esi
	addl	$8, %ecx
	movq	(%r10,%rsi), %r9
	movq	%r9, (%r8,%rsi)
	cmpl	%edx, %ecx
	jb	.L13
.L10:
	movq	%rbx, %rsi
	movq	%r12, %rdx
	cmpl	$8, %eax
	jnb	.L171
.L15:
	xorl	%ecx, %ecx
	testb	$4, %al
	je	.L18
	movl	(%rdx), %ecx
	movl	%ecx, (%rsi)
	movl	$4, %ecx
.L18:
	testb	$2, %al
	je	.L19
	movzwl	(%rdx,%rcx), %r8d
	movw	%r8w, (%rsi,%rcx)
	addq	$2, %rcx
.L19:
	testb	$1, %al
	je	.L21
	movzbl	(%rdx,%rcx), %eax
	movb	%al, (%rsi,%rcx)
.L21:
	movdqa	-80(%rbp), %xmm0
	pxor	-96(%rbp), %xmm0
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	movq	0(%r13), %rdx
	movaps	%xmm0, -80(%rbp)
	call	*16(%r13)
	movdqa	-80(%rbp), %xmm0
	pxor	-96(%rbp), %xmm0
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movups	%xmm0, -16(%r14)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L170:
	testb	$4, %al
	jne	.L172
	testl	%eax, %eax
	je	.L10
	movzbl	(%rbx), %edx
	movb	%dl, (%r14)
	testb	$2, %al
	je	.L10
	movl	%eax, %edx
	movzwl	-2(%rbx,%rdx), %ecx
	movw	%cx, -2(%r14,%rdx)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L168:
	testb	$4, %r15b
	jne	.L173
	testl	%edx, %edx
	je	.L26
	movzbl	(%rbx), %esi
	movb	%sil, (%rax)
	testb	$2, %dl
	je	.L26
	movzwl	-2(%rbx,%rdx), %esi
	movw	%si, -2(%rax,%rdx)
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L171:
	movl	%eax, %r8d
	xorl	%edx, %edx
	andl	$-8, %r8d
.L16:
	movl	%edx, %ecx
	addl	$8, %edx
	movq	(%r12,%rcx), %rsi
	movq	%rsi, (%rbx,%rcx)
	cmpl	%r8d, %edx
	jb	.L16
	leaq	(%rbx,%rdx), %rsi
	addq	%r12, %rdx
	jmp	.L15
.L169:
	andl	$-8, %r15d
	xorl	%eax, %eax
.L32:
	movl	%eax, %edx
	addl	$8, %eax
	movq	(%rcx,%rdx), %r9
	movq	%r9, (%rbx,%rdx)
	cmpl	%r15d, %eax
	jb	.L32
	leaq	(%rbx,%rax), %r8
	addq	%rcx, %rax
	jmp	.L31
.L7:
	movzbl	(%r12), %eax
	movzbl	-80(%rbp), %edx
	movb	%al, -80(%rbp)
	movb	%dl, (%r14)
	cmpq	$1, %r15
	je	.L21
	movzbl	1(%r12), %eax
	movzbl	-79(%rbp), %edx
	movb	%al, -79(%rbp)
	movb	%dl, 1(%r14)
	cmpq	$2, %r15
	je	.L21
	movzbl	2(%r12), %eax
	movzbl	-78(%rbp), %edx
	movb	%al, -78(%rbp)
	movb	%dl, 2(%r14)
	cmpq	$3, %r15
	je	.L21
	movzbl	3(%r12), %eax
	movzbl	-77(%rbp), %edx
	movb	%al, -77(%rbp)
	movb	%dl, 3(%r14)
	cmpq	$4, %r15
	je	.L21
	movzbl	4(%r12), %eax
	movzbl	-76(%rbp), %edx
	movb	%al, -76(%rbp)
	movb	%dl, 4(%r14)
	cmpq	$5, %r15
	je	.L21
	movzbl	5(%r12), %eax
	movzbl	-75(%rbp), %edx
	movb	%al, -75(%rbp)
	movb	%dl, 5(%r14)
	cmpq	$6, %r15
	je	.L21
	movzbl	6(%r12), %eax
	movzbl	-74(%rbp), %edx
	movb	%al, -74(%rbp)
	movb	%dl, 6(%r14)
	cmpq	$7, %r15
	je	.L21
	movzbl	7(%r12), %eax
	movzbl	-73(%rbp), %edx
	movb	%al, -73(%rbp)
	movb	%dl, 7(%r14)
	cmpq	$8, %r15
	je	.L21
	movzbl	8(%r12), %eax
	movzbl	-72(%rbp), %edx
	movb	%al, -72(%rbp)
	movb	%dl, 8(%r14)
	cmpq	$9, %r15
	je	.L21
	movzbl	9(%r12), %eax
	movzbl	-71(%rbp), %edx
	movb	%al, -71(%rbp)
	movb	%dl, 9(%r14)
	cmpq	$10, %r15
	je	.L21
	movzbl	10(%r12), %eax
	movzbl	-70(%rbp), %edx
	movb	%al, -70(%rbp)
	movb	%dl, 10(%r14)
	cmpq	$11, %r15
	je	.L21
	movzbl	11(%r12), %eax
	movzbl	-69(%rbp), %edx
	movb	%al, -69(%rbp)
	movb	%dl, 11(%r14)
	cmpq	$12, %r15
	je	.L21
	movzbl	12(%r12), %eax
	movzbl	-68(%rbp), %edx
	movb	%al, -68(%rbp)
	movb	%dl, 12(%r14)
	cmpq	$13, %r15
	je	.L21
	movzbl	13(%r12), %eax
	movzbl	-67(%rbp), %edx
	movb	%al, -67(%rbp)
	movb	%dl, 13(%r14)
	cmpq	$15, %r15
	jne	.L21
	movzbl	14(%r12), %eax
	movzbl	-66(%rbp), %edx
	movb	%al, -66(%rbp)
	movb	%dl, 14(%r14)
	jmp	.L21
.L23:
	movzbl	16(%r12), %eax
	movzbl	-80(%rbp), %edx
	movb	%al, -80(%rbp)
	movb	%dl, 16(%r14)
	cmpq	$1, %r15
	je	.L37
	movzbl	17(%r12), %eax
	movzbl	-79(%rbp), %edx
	movb	%al, -79(%rbp)
	movb	%dl, 17(%r14)
	cmpq	$2, %r15
	je	.L37
	movzbl	18(%r12), %eax
	movzbl	-78(%rbp), %edx
	movb	%al, -78(%rbp)
	movb	%dl, 18(%r14)
	cmpq	$3, %r15
	je	.L37
	movzbl	19(%r12), %eax
	movzbl	-77(%rbp), %edx
	movb	%al, -77(%rbp)
	movb	%dl, 19(%r14)
	cmpq	$4, %r15
	je	.L37
	movzbl	20(%r12), %eax
	movzbl	-76(%rbp), %edx
	movb	%al, -76(%rbp)
	movb	%dl, 20(%r14)
	cmpq	$5, %r15
	je	.L37
	movzbl	21(%r12), %eax
	movzbl	-75(%rbp), %edx
	movb	%al, -75(%rbp)
	movb	%dl, 21(%r14)
	cmpq	$6, %r15
	je	.L37
	movzbl	22(%r12), %eax
	movzbl	-74(%rbp), %edx
	movb	%al, -74(%rbp)
	movb	%dl, 22(%r14)
	cmpq	$7, %r15
	je	.L37
	movzbl	23(%r12), %eax
	movzbl	-73(%rbp), %edx
	movb	%al, -73(%rbp)
	movb	%dl, 23(%r14)
	cmpq	$8, %r15
	je	.L37
	movzbl	24(%r12), %eax
	movzbl	-72(%rbp), %edx
	movb	%al, -72(%rbp)
	movb	%dl, 24(%r14)
	cmpq	$9, %r15
	je	.L37
	movzbl	25(%r12), %eax
	movzbl	-71(%rbp), %edx
	movb	%al, -71(%rbp)
	movb	%dl, 25(%r14)
	cmpq	$10, %r15
	je	.L37
	movzbl	26(%r12), %eax
	movzbl	-70(%rbp), %edx
	movb	%al, -70(%rbp)
	movb	%dl, 26(%r14)
	cmpq	$11, %r15
	je	.L37
	movzbl	27(%r12), %eax
	movzbl	-69(%rbp), %edx
	movb	%al, -69(%rbp)
	movb	%dl, 27(%r14)
	cmpq	$12, %r15
	je	.L37
	movzbl	28(%r12), %eax
	movzbl	-68(%rbp), %edx
	movb	%al, -68(%rbp)
	movb	%dl, 28(%r14)
	cmpq	$13, %r15
	je	.L37
	movzbl	29(%r12), %eax
	movzbl	-67(%rbp), %edx
	movb	%al, -67(%rbp)
	movb	%dl, 29(%r14)
	cmpq	$15, %r15
	jne	.L37
	movzbl	30(%r12), %eax
	movzbl	-66(%rbp), %edx
	movb	%al, -66(%rbp)
	movb	%dl, 30(%r14)
	jmp	.L37
.L41:
	movl	$-1, %eax
	jmp	.L1
.L173:
	movl	(%rbx), %esi
	movl	%esi, (%rax)
	movl	-4(%rbx,%rdx), %esi
	movl	%esi, -4(%rax,%rdx)
	jmp	.L26
.L172:
	movl	(%rbx), %edx
	movl	%edx, (%r14)
	movl	%eax, %edx
	movl	-4(%rbx,%rdx), %ecx
	movl	%ecx, -4(%r14,%rdx)
	jmp	.L10
.L167:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE151:
	.size	CRYPTO_xts128_encrypt, .-CRYPTO_xts128_encrypt
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
