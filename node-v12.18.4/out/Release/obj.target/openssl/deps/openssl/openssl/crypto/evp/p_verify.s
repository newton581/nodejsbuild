	.file	"p_verify.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/evp/p_verify.c"
	.text
	.p2align 4
	.globl	EVP_VerifyFinal
	.type	EVP_VerifyFinal, @function
EVP_VerifyFinal:
.LFB805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -152(%rbp)
	movl	$512, %esi
	movl	%edx, -156(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -132(%rbp)
	call	EVP_MD_CTX_test_flags@PLT
	testl	%eax, %eax
	je	.L2
	leaq	-128(%rbp), %r15
	leaq	-132(%rbp), %rdx
	movq	%r12, %rdi
	xorl	%ebx, %ebx
	movq	%r15, %rsi
	call	EVP_DigestFinal_ex@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L3
.L8:
	xorl	%esi, %esi
	movq	%r13, %rdi
	movl	$-1, %r14d
	call	EVP_PKEY_CTX_new@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L3
	movq	%rax, %rdi
	call	EVP_PKEY_verify_init@PLT
	testl	%eax, %eax
	jle	.L3
	movq	%r12, %rdi
	call	EVP_MD_CTX_md@PLT
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%rbx, %rdi
	movq	%rax, %r9
	movl	$248, %edx
	movl	$-1, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L3
	movl	-156(%rbp), %edx
	movl	-132(%rbp), %r8d
	movq	%r15, %rcx
	movq	%rbx, %rdi
	movq	-152(%rbp), %rsi
	call	EVP_PKEY_verify@PLT
	movl	%eax, %r14d
	.p2align 4,,10
	.p2align 3
.L3:
	movq	%rbx, %rdi
	call	EVP_PKEY_CTX_free@PLT
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L22
	addq	$136, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movl	%eax, %r14d
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L23
	movq	%r12, %rsi
	movq	%rax, -168(%rbp)
	call	EVP_MD_CTX_copy_ex@PLT
	movq	-168(%rbp), %rdi
	testl	%eax, %eax
	movl	%eax, %r14d
	jne	.L24
	call	EVP_MD_CTX_free@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L24:
	leaq	-128(%rbp), %r15
	leaq	-132(%rbp), %rdx
	movq	%r15, %rsi
	call	EVP_DigestFinal_ex@PLT
	movq	-168(%rbp), %rdi
	movl	%eax, %r14d
	call	EVP_MD_CTX_free@PLT
	testl	%r14d, %r14d
	jne	.L8
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L23:
	movl	$32, %r8d
	movl	$65, %edx
	movl	$108, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1
.L22:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE805:
	.size	EVP_VerifyFinal, .-EVP_VerifyFinal
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
