	.file	"ssl_mcnf.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/ssl/ssl_mcnf.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"system_default"
.LC2:
	.string	"name="
.LC3:
	.string	", arg="
.LC4:
	.string	", cmd="
.LC5:
	.string	"section="
	.text
	.p2align 4
	.type	ssl_do_config, @function
ssl_do_config:
.LFB1050:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	orq	%rsi, %r12
	je	.L36
	movq	%rdi, %r13
	movq	%rsi, %r14
	movl	%ecx, %r15d
	movq	%rdx, %rdi
	testq	%rdx, %rdx
	jne	.L4
	testb	$1, %cl
	je	.L4
	leaq	.LC1(%rip), %rdi
	leaq	-88(%rbp), %rsi
	movq	%rdi, -104(%rbp)
	call	conf_ssl_name_find@PLT
	testl	%eax, %eax
	jne	.L5
.L34:
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
.L3:
	movq	%r12, %rdi
	call	SSL_CONF_CTX_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L37
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	leaq	-88(%rbp), %rsi
	call	conf_ssl_name_find@PLT
	testl	%eax, %eax
	jne	.L5
	testl	%r15d, %r15d
	jne	.L34
	movl	$41, %r8d
	movl	$113, %edx
	movl	$391, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-104(%rbp), %rdx
	movl	$2, %edi
	xorl	%eax, %eax
	leaq	.LC2(%rip), %rsi
	call	ERR_add_error_data@PLT
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L5:
	movq	-88(%rbp), %rdi
	leaq	-80(%rbp), %rdx
	leaq	-104(%rbp), %rsi
	call	conf_ssl_get@PLT
	movq	%rax, %rbx
	call	SSL_CONF_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L34
	cmpl	$1, %r15d
	sbbl	%r8d, %r8d
	andl	$96, %r8d
	addl	$14, %r8d
	cmpl	$1, %r15d
	sbbl	%edx, %edx
	movl	%r8d, -120(%rbp)
	andl	$96, %edx
	addl	$6, %edx
	cmpl	$1, %r15d
	sbbl	%eax, %eax
	movl	%edx, -124(%rbp)
	andl	$96, %eax
	addl	$10, %eax
	cmpl	$1, %r15d
	sbbl	%r15d, %r15d
	movl	%eax, -116(%rbp)
	andl	$96, %r15d
	addl	$2, %r15d
	testq	%r13, %r13
	je	.L8
	movq	8(%r13), %r9
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r9, -112(%rbp)
	call	SSL_CONF_CTX_set_ssl@PLT
	movq	-112(%rbp), %r9
	movl	-116(%rbp), %eax
	movl	-120(%rbp), %r8d
	movl	-124(%rbp), %edx
.L9:
	movq	ssl_undefined_function@GOTPCREL(%rip), %rcx
	cmpq	%rcx, 40(%r9)
	movq	%r12, %rdi
	leaq	-64(%rbp), %r14
	cmove	%r15d, %eax
	cmove	%edx, %r8d
	leaq	-72(%rbp), %r13
	cmpq	%rcx, 48(%r9)
	cmove	%eax, %r8d
	xorl	%r15d, %r15d
	movl	%r8d, %esi
	call	SSL_CONF_CTX_set_flags@PLT
	cmpq	$0, -80(%rbp)
	jne	.L12
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L13:
	addq	$1, %r15
	cmpq	%r15, -80(%rbp)
	jbe	.L16
.L12:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r14, %rcx
	call	conf_ssl_get_cmd@PLT
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rsi
	movq	%r12, %rdi
	call	SSL_CONF_cmd@PLT
	testl	%eax, %eax
	jg	.L13
	cmpl	$-2, %eax
	je	.L38
	movl	$74, %r8d
	movl	$384, %edx
	movl	$391, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L15:
	subq	$8, %rsp
	pushq	-64(%rbp)
	movq	-104(%rbp), %rdx
	xorl	%eax, %eax
	movq	-72(%rbp), %r8
	movl	$6, %edi
	xorl	%r13d, %r13d
	leaq	.LC3(%rip), %r9
	leaq	.LC4(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	call	ERR_add_error_data@PLT
	popq	%rax
	popq	%rdx
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$33, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$67, %edx
	xorl	%r13d, %r13d
	movl	$391, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L16:
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	SSL_CONF_CTX_finish@PLT
	testl	%eax, %eax
	setg	%r13b
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L8:
	movq	(%r14), %r9
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r9, -112(%rbp)
	call	SSL_CONF_CTX_set_ssl_ctx@PLT
	movl	-124(%rbp), %edx
	movl	-120(%rbp), %r8d
	movl	-116(%rbp), %eax
	movq	-112(%rbp), %r9
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L38:
	movl	$72, %r8d
	movl	$139, %edx
	movl	$391, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L15
.L37:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1050:
	.size	ssl_do_config, .-ssl_do_config
	.p2align 4
	.globl	SSL_add_ssl_module
	.type	SSL_add_ssl_module, @function
SSL_add_ssl_module:
.LFB1049:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE1049:
	.size	SSL_add_ssl_module, .-SSL_add_ssl_module
	.p2align 4
	.globl	SSL_config
	.type	SSL_config, @function
SSL_config:
.LFB1051:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	jmp	ssl_do_config
	.cfi_endproc
.LFE1051:
	.size	SSL_config, .-SSL_config
	.p2align 4
	.globl	SSL_CTX_config
	.type	SSL_CTX_config, @function
SSL_CTX_config:
.LFB1052:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	xorl	%ecx, %ecx
	movq	%rdi, %rsi
	xorl	%edi, %edi
	jmp	ssl_do_config
	.cfi_endproc
.LFE1052:
	.size	SSL_CTX_config, .-SSL_CTX_config
	.p2align 4
	.globl	ssl_ctx_system_config
	.type	ssl_ctx_system_config, @function
ssl_ctx_system_config:
.LFB1053:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	movl	$1, %ecx
	xorl	%edx, %edx
	xorl	%edi, %edi
	jmp	ssl_do_config
	.cfi_endproc
.LFE1053:
	.size	ssl_ctx_system_config, .-ssl_ctx_system_config
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
