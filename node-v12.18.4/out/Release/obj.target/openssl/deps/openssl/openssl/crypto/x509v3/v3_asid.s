	.file	"v3_asid.c"
	.text
	.p2align 4
	.type	ASIdOrRange_cmp, @function
ASIdOrRange_cmp:
.LFB1340:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rbx
	movq	(%rsi), %r12
	movl	(%rbx), %eax
	movq	8(%rbx), %rdi
	movq	8(%r12), %rsi
	testl	%eax, %eax
	jne	.L2
	movl	(%r12), %eax
	testl	%eax, %eax
	je	.L4
	movq	(%rsi), %rsi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ASN1_INTEGER_cmp@PLT
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movq	(%rdi), %rdi
	cmpl	$1, %eax
	jne	.L4
	cmpl	$1, (%r12)
	je	.L8
.L4:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ASN1_INTEGER_cmp@PLT
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movq	(%rsi), %rsi
	call	ASN1_INTEGER_cmp@PLT
	testl	%eax, %eax
	je	.L9
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	movq	8(%r12), %rax
	movq	8(%rax), %rsi
	movq	8(%rbx), %rax
	popq	%rbx
	popq	%r12
	movq	8(%rax), %rdi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	ASN1_INTEGER_cmp@PLT
	.cfi_endproc
.LFE1340:
	.size	ASIdOrRange_cmp, .-ASIdOrRange_cmp
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	"%*s%s\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"../deps/openssl/openssl/crypto/x509v3/v3_asid.c"
	.section	.rodata.str1.1
.LC3:
	.string	"%*s%s-"
.LC4:
	.string	"%s\n"
	.text
	.p2align 4
	.type	i2r_ASIdentifierChoice.part.0, @function
i2r_ASIdentifierChoice.part.0:
.LFB1357:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leal	2(%rdx), %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	8(%r13), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L26
.L18:
	movq	8(%r13), %rdi
	movl	%r12d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rbx
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L12
	cmpl	$1, %eax
	jne	.L17
	movq	8(%rbx), %rax
	xorl	%edi, %edi
	movq	(%rax), %rsi
	call	i2s_ASN1_INTEGER@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L17
	leaq	.LC0(%rip), %rcx
	movq	%rax, -56(%rbp)
	movl	%r15d, %edx
	movq	%r14, %rdi
	leaq	.LC3(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-56(%rbp), %r8
	movl	$89, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r8, %rdi
	call	CRYPTO_free@PLT
	movq	8(%rbx), %rax
	xorl	%edi, %edi
	movq	8(%rax), %rsi
	call	i2s_ASN1_INTEGER@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L17
	movq	%rax, %rdx
	leaq	.LC4(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$93, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rbx, %rdi
	call	CRYPTO_free@PLT
.L16:
	movq	8(%r13), %rdi
	addl	$1, %r12d
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jl	.L18
.L26:
	movl	$1, %eax
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L17:
	xorl	%eax, %eax
.L10:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	movq	8(%rbx), %rsi
	xorl	%edi, %edi
	call	i2s_ASN1_INTEGER@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L17
	movq	%rax, %r8
	movl	%r15d, %edx
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	call	BIO_printf@PLT
	movl	$83, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rbx, %rdi
	call	CRYPTO_free@PLT
	jmp	.L16
	.cfi_endproc
.LFE1357:
	.size	i2r_ASIdentifierChoice.part.0, .-i2r_ASIdentifierChoice.part.0
	.section	.rodata.str1.1
.LC5:
	.string	"Autonomous System Numbers"
.LC6:
	.string	"%*s%s:\n"
.LC7:
	.string	"%*sinherit\n"
.LC8:
	.string	"Routing Domain Identifiers"
	.text
	.p2align 4
	.type	i2r_ASIdentifiers, @function
i2r_ASIdentifiers:
.LFB1339:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %r13
	testq	%r13, %r13
	je	.L29
	leaq	.LC5(%rip), %r8
	movl	%r12d, %edx
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	call	BIO_printf@PLT
	movl	0(%r13), %eax
	testl	%eax, %eax
	je	.L30
	cmpl	$1, %eax
	je	.L31
.L36:
	xorl	%r13d, %r13d
.L27:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	movq	%r13, %rsi
	movl	%r12d, %edx
	movq	%r14, %rdi
	call	i2r_ASIdentifierChoice.part.0
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L27
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L30:
	leal	2(%r12), %edx
	leaq	.LC0(%rip), %rcx
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC7(%rip), %rsi
	call	BIO_printf@PLT
.L29:
	movq	8(%rbx), %r15
	movl	$1, %r13d
	testq	%r15, %r15
	je	.L27
	leaq	.LC8(%rip), %r8
	movl	%r12d, %edx
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	call	BIO_printf@PLT
	movl	(%r15), %eax
	testl	%eax, %eax
	je	.L34
	cmpl	$1, %eax
	jne	.L36
	movl	%r12d, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	xorl	%r13d, %r13d
	call	i2r_ASIdentifierChoice.part.0
	testl	%eax, %eax
	setne	%r13b
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L34:
	leal	2(%r12), %edx
	leaq	.LC0(%rip), %rcx
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC7(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L27
	.cfi_endproc
.LFE1339:
	.size	i2r_ASIdentifiers, .-i2r_ASIdentifiers
	.p2align 4
	.type	asid_contains, @function
asid_contains:
.LFB1350:
	.cfi_startproc
	testq	%rsi, %rsi
	je	.L66
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpq	%rdi, %rsi
	je	.L56
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	testq	%rdi, %rdi
	jne	.L44
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L50:
	movq	8(%rax), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rdi
	movq	%rcx, -56(%rbp)
.L52:
	movq	%r15, %rsi
	call	ASN1_INTEGER_cmp@PLT
	testl	%eax, %eax
	js	.L69
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	ASN1_INTEGER_cmp@PLT
	testl	%eax, %eax
	jg	.L51
	addl	$1, %r13d
.L44:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jge	.L56
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	testq	%rax, %rax
	je	.L51
	movl	(%rax), %edx
	testl	%edx, %edx
	je	.L46
	cmpl	$1, %edx
	jne	.L51
	movq	8(%rax), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %r15
	movq	%rcx, -64(%rbp)
.L54:
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L51
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	testq	%rax, %rax
	je	.L51
	movl	(%rax), %edx
	testl	%edx, %edx
	je	.L49
	cmpl	$1, %edx
	je	.L50
.L51:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	addl	$1, %r12d
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L49:
	movq	8(%rax), %rax
	movq	%rax, -56(%rbp)
	movq	%rax, %rdi
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L46:
	movq	8(%rax), %rax
	movq	%rax, -64(%rbp)
	movq	%rax, %r15
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L56:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1350:
	.size	asid_contains, .-asid_contains
	.p2align 4
	.type	ASIdentifierChoice_is_canonical.part.0, @function
ASIdentifierChoice_is_canonical.part.0:
.LFB1356:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$40, %rsp
	movq	$0, -56(%rbp)
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L74:
	movq	8(%r14), %rdx
	movq	(%rdx), %rdi
	movq	8(%rdx), %r15
.L75:
	testq	%rax, %rax
	je	.L90
	movl	(%rax), %edx
	testl	%edx, %edx
	je	.L76
	cmpl	$1, %edx
	jne	.L90
	movq	8(%rax), %rax
	movq	(%rax), %r14
	movq	8(%rax), %rax
	movq	%rax, -72(%rbp)
.L78:
	movq	%r14, %rsi
	movq	%rdi, -64(%rbp)
	call	ASN1_INTEGER_cmp@PLT
	testl	%eax, %eax
	jns	.L90
	movq	-64(%rbp), %rdi
	movq	%r15, %rsi
	call	ASN1_INTEGER_cmp@PLT
	testl	%eax, %eax
	jg	.L90
	movq	-72(%rbp), %rsi
	movq	%r14, %rdi
	call	ASN1_INTEGER_cmp@PLT
	testl	%eax, %eax
	jg	.L90
	testq	%r12, %r12
	je	.L102
.L79:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	ASN1_INTEGER_to_BN@PLT
	testq	%rax, %rax
	je	.L80
	movl	$1, %esi
	movq	%r12, %rdi
	call	BN_add_word@PLT
	testl	%eax, %eax
	je	.L80
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	call	BN_to_ASN1_INTEGER@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L103
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	ASN1_INTEGER_cmp@PLT
	movq	%r15, -56(%rbp)
	testl	%eax, %eax
	jns	.L90
.L83:
	movq	8(%r13), %rdi
	call	OPENSSL_sk_num@PLT
	movq	8(%r13), %rdi
	subl	$1, %eax
	cmpl	%eax, %ebx
	jge	.L104
	movl	%ebx, %esi
	addl	$1, %ebx
	call	OPENSSL_sk_value@PLT
	movq	8(%r13), %rdi
	movl	%ebx, %esi
	movq	%rax, %r14
	call	OPENSSL_sk_value@PLT
	testq	%r14, %r14
	je	.L90
	movl	(%r14), %edx
	testl	%edx, %edx
	je	.L73
	cmpl	$1, %edx
	je	.L74
.L90:
	xorl	%r13d, %r13d
.L72:
	movq	-56(%rbp), %rdi
	call	ASN1_INTEGER_free@PLT
	movq	%r12, %rdi
	call	BN_free@PLT
	addq	$40, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	movq	8(%r14), %rdi
	movq	%rdi, %r15
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L102:
	call	BN_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L79
	.p2align 4,,10
	.p2align 3
.L80:
	movl	$303, %r8d
.L101:
	leaq	.LC2(%rip), %rcx
	movl	$65, %edx
	movl	$162, %esi
	xorl	%r13d, %r13d
	movl	$34, %edi
	call	ERR_put_error@PLT
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L76:
	movq	8(%rax), %r14
	movq	%r14, -72(%rbp)
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L104:
	call	OPENSSL_sk_num@PLT
	movq	8(%r13), %rdi
	movl	$1, %r13d
	leal	-1(%rax), %esi
	call	OPENSSL_sk_value@PLT
	testq	%rax, %rax
	je	.L72
	cmpl	$1, (%rax)
	jne	.L72
	movq	8(%rax), %rax
	xorl	%r13d, %r13d
	movq	8(%rax), %rsi
	movq	(%rax), %rdi
	call	ASN1_INTEGER_cmp@PLT
	testl	%eax, %eax
	setle	%r13b
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L103:
	movl	$311, %r8d
	jmp	.L101
	.cfi_endproc
.LFE1356:
	.size	ASIdentifierChoice_is_canonical.part.0, .-ASIdentifierChoice_is_canonical.part.0
	.p2align 4
	.type	ASIdentifierChoice_canonize.part.0, @function
ASIdentifierChoice_canonize.part.0:
.LFB1359:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rdi
	call	OPENSSL_sk_sort@PLT
	movl	$0, -52(%rbp)
	movq	$0, -80(%rbp)
.L106:
	movq	8(%r15), %rdi
	call	OPENSSL_sk_num@PLT
	movq	8(%r15), %rdi
	subl	$1, %eax
	cmpl	%eax, -52(%rbp)
	jge	.L157
	movl	-52(%rbp), %r14d
	movl	%r14d, %esi
	call	OPENSSL_sk_value@PLT
	movq	8(%r15), %rdi
	movq	%rax, -72(%rbp)
	movq	%rax, %rbx
	leal	1(%r14), %eax
	movl	%eax, %esi
	movl	%eax, -56(%rbp)
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r12
	testq	%rbx, %rbx
	je	.L156
	movl	(%rbx), %eax
	testl	%eax, %eax
	je	.L108
	cmpl	$1, %eax
	jne	.L156
	movq	-72(%rbp), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rdx
	movq	8(%rax), %rbx
	movq	%rdx, -64(%rbp)
.L110:
	testq	%r12, %r12
	je	.L156
	movl	(%r12), %eax
	testl	%eax, %eax
	je	.L111
	cmpl	$1, %eax
	jne	.L156
	movq	8(%r12), %rax
	movq	(%rax), %r14
	movq	8(%rax), %rax
	movq	%rax, -88(%rbp)
.L113:
	movq	-64(%rbp), %rdi
	movq	%r14, %rsi
	call	ASN1_INTEGER_cmp@PLT
	testl	%eax, %eax
	jg	.L156
	movq	-64(%rbp), %rdi
	movq	%rbx, %rsi
	call	ASN1_INTEGER_cmp@PLT
	testl	%eax, %eax
	jg	.L156
	movq	-88(%rbp), %rsi
	movq	%r14, %rdi
	call	ASN1_INTEGER_cmp@PLT
	testl	%eax, %eax
	jg	.L156
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	ASN1_INTEGER_cmp@PLT
	testl	%eax, %eax
	jns	.L158
	testq	%r13, %r13
	je	.L159
.L115:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	ASN1_INTEGER_to_BN@PLT
	testq	%rax, %rax
	je	.L116
	movl	$1, %esi
	movq	%r13, %rdi
	call	BN_add_word@PLT
	testl	%eax, %eax
	je	.L116
	movq	-80(%rbp), %rsi
	movq	%r13, %rdi
	call	BN_to_ASN1_INTEGER@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L160
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	ASN1_INTEGER_cmp@PLT
	testl	%eax, %eax
	jne	.L135
	movq	-72(%rbp), %rax
	movl	(%rax), %r14d
	testl	%r14d, %r14d
	je	.L120
	cmpl	$1, %r14d
	jne	.L122
	movq	%rax, %r14
	movq	8(%rax), %rax
	movq	8(%rax), %rdi
	call	ASN1_INTEGER_free@PLT
	movq	8(%r14), %rax
	movq	-88(%rbp), %rdx
	movq	%rdx, 8(%rax)
.L122:
	movl	(%r12), %eax
	testl	%eax, %eax
	je	.L124
.L162:
	cmpl	$1, %eax
	jne	.L126
	movq	8(%r12), %rax
	movq	$0, 8(%rax)
.L126:
	leaq	ASIdOrRange_it(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_item_free@PLT
	movq	8(%r15), %rdi
	movl	-56(%rbp), %esi
	call	OPENSSL_sk_delete@PLT
.L119:
	movq	%rbx, -80(%rbp)
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L159:
	call	BN_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L115
	.p2align 4,,10
	.p2align 3
.L116:
	movl	$428, %r8d
.L155:
	movl	$65, %edx
	movl	$161, %esi
	movl	$34, %edi
	leaq	.LC2(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L156:
	xorl	%r14d, %r14d
.L107:
	movq	-80(%rbp), %rdi
	call	ASN1_INTEGER_free@PLT
	movq	%r13, %rdi
	call	BN_free@PLT
	addq	$56, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	.cfi_restore_state
	movl	-56(%rbp), %eax
	movl	%eax, -52(%rbp)
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L108:
	movq	-72(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rax, -64(%rbp)
	movq	%rax, %rbx
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L111:
	movq	8(%r12), %r14
	movq	%r14, -88(%rbp)
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L120:
	movl	$448, %edx
	leaq	.LC2(%rip), %rsi
	movl	$16, %edi
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L161
	movq	-64(%rbp), %xmm0
	movq	-72(%rbp), %rdx
	movhps	-88(%rbp), %xmm0
	movups	%xmm0, (%rax)
	movl	$1, (%rdx)
	movq	%rax, 8(%rdx)
	movl	(%r12), %eax
	testl	%eax, %eax
	jne	.L162
.L124:
	movq	$0, 8(%r12)
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L161:
	movl	$449, %r8d
	movl	$65, %edx
	movl	$161, %esi
	movl	$34, %edi
	leaq	.LC2(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%rbx, -80(%rbp)
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L157:
	call	OPENSSL_sk_num@PLT
	movq	8(%r15), %rdi
	leal	-1(%rax), %esi
	call	OPENSSL_sk_value@PLT
	testq	%rax, %rax
	je	.L128
	cmpl	$1, (%rax)
	je	.L163
.L128:
	movl	(%r15), %eax
	movl	$1, %r14d
	testl	%eax, %eax
	je	.L107
	xorl	%r14d, %r14d
	cmpl	$1, %eax
	jne	.L107
	movq	8(%r15), %rdi
	call	OPENSSL_sk_num@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L107
	movq	%r15, %rdi
	xorl	%r14d, %r14d
	call	ASIdentifierChoice_is_canonical.part.0
	testl	%eax, %eax
	setne	%r14b
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L158:
	movl	$417, %r8d
	leaq	.LC2(%rip), %rcx
	movl	$116, %edx
	xorl	%r14d, %r14d
	movl	$161, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L160:
	movl	$436, %r8d
	jmp	.L155
.L163:
	movq	8(%rax), %rax
	xorl	%r14d, %r14d
	movq	8(%rax), %rsi
	movq	(%rax), %rdi
	call	ASN1_INTEGER_cmp@PLT
	testl	%eax, %eax
	jle	.L128
	jmp	.L107
	.cfi_endproc
.LFE1359:
	.size	ASIdentifierChoice_canonize.part.0, .-ASIdentifierChoice_canonize.part.0
	.p2align 4
	.globl	d2i_ASRange
	.type	d2i_ASRange, @function
d2i_ASRange:
.LFB1322:
	.cfi_startproc
	endbr64
	leaq	ASRange_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1322:
	.size	d2i_ASRange, .-d2i_ASRange
	.p2align 4
	.globl	i2d_ASRange
	.type	i2d_ASRange, @function
i2d_ASRange:
.LFB1323:
	.cfi_startproc
	endbr64
	leaq	ASRange_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1323:
	.size	i2d_ASRange, .-i2d_ASRange
	.p2align 4
	.globl	ASRange_new
	.type	ASRange_new, @function
ASRange_new:
.LFB1324:
	.cfi_startproc
	endbr64
	leaq	ASRange_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1324:
	.size	ASRange_new, .-ASRange_new
	.p2align 4
	.globl	ASRange_free
	.type	ASRange_free, @function
ASRange_free:
.LFB1325:
	.cfi_startproc
	endbr64
	leaq	ASRange_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1325:
	.size	ASRange_free, .-ASRange_free
	.p2align 4
	.globl	d2i_ASIdOrRange
	.type	d2i_ASIdOrRange, @function
d2i_ASIdOrRange:
.LFB1326:
	.cfi_startproc
	endbr64
	leaq	ASIdOrRange_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1326:
	.size	d2i_ASIdOrRange, .-d2i_ASIdOrRange
	.p2align 4
	.globl	i2d_ASIdOrRange
	.type	i2d_ASIdOrRange, @function
i2d_ASIdOrRange:
.LFB1327:
	.cfi_startproc
	endbr64
	leaq	ASIdOrRange_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1327:
	.size	i2d_ASIdOrRange, .-i2d_ASIdOrRange
	.p2align 4
	.globl	ASIdOrRange_new
	.type	ASIdOrRange_new, @function
ASIdOrRange_new:
.LFB1328:
	.cfi_startproc
	endbr64
	leaq	ASIdOrRange_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1328:
	.size	ASIdOrRange_new, .-ASIdOrRange_new
	.p2align 4
	.globl	ASIdOrRange_free
	.type	ASIdOrRange_free, @function
ASIdOrRange_free:
.LFB1329:
	.cfi_startproc
	endbr64
	leaq	ASIdOrRange_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1329:
	.size	ASIdOrRange_free, .-ASIdOrRange_free
	.p2align 4
	.globl	d2i_ASIdentifierChoice
	.type	d2i_ASIdentifierChoice, @function
d2i_ASIdentifierChoice:
.LFB1330:
	.cfi_startproc
	endbr64
	leaq	ASIdentifierChoice_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1330:
	.size	d2i_ASIdentifierChoice, .-d2i_ASIdentifierChoice
	.p2align 4
	.globl	i2d_ASIdentifierChoice
	.type	i2d_ASIdentifierChoice, @function
i2d_ASIdentifierChoice:
.LFB1331:
	.cfi_startproc
	endbr64
	leaq	ASIdentifierChoice_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1331:
	.size	i2d_ASIdentifierChoice, .-i2d_ASIdentifierChoice
	.p2align 4
	.globl	ASIdentifierChoice_new
	.type	ASIdentifierChoice_new, @function
ASIdentifierChoice_new:
.LFB1332:
	.cfi_startproc
	endbr64
	leaq	ASIdentifierChoice_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1332:
	.size	ASIdentifierChoice_new, .-ASIdentifierChoice_new
	.p2align 4
	.globl	ASIdentifierChoice_free
	.type	ASIdentifierChoice_free, @function
ASIdentifierChoice_free:
.LFB1333:
	.cfi_startproc
	endbr64
	leaq	ASIdentifierChoice_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1333:
	.size	ASIdentifierChoice_free, .-ASIdentifierChoice_free
	.p2align 4
	.globl	d2i_ASIdentifiers
	.type	d2i_ASIdentifiers, @function
d2i_ASIdentifiers:
.LFB1334:
	.cfi_startproc
	endbr64
	leaq	ASIdentifiers_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1334:
	.size	d2i_ASIdentifiers, .-d2i_ASIdentifiers
	.p2align 4
	.globl	i2d_ASIdentifiers
	.type	i2d_ASIdentifiers, @function
i2d_ASIdentifiers:
.LFB1335:
	.cfi_startproc
	endbr64
	leaq	ASIdentifiers_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1335:
	.size	i2d_ASIdentifiers, .-i2d_ASIdentifiers
	.p2align 4
	.globl	ASIdentifiers_new
	.type	ASIdentifiers_new, @function
ASIdentifiers_new:
.LFB1336:
	.cfi_startproc
	endbr64
	leaq	ASIdentifiers_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1336:
	.size	ASIdentifiers_new, .-ASIdentifiers_new
	.p2align 4
	.globl	ASIdentifiers_free
	.type	ASIdentifiers_free, @function
ASIdentifiers_free:
.LFB1337:
	.cfi_startproc
	endbr64
	leaq	ASIdentifiers_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1337:
	.size	ASIdentifiers_free, .-ASIdentifiers_free
	.p2align 4
	.globl	X509v3_asid_add_inherit
	.type	X509v3_asid_add_inherit, @function
X509v3_asid_add_inherit:
.LFB1341:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L199
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	testl	%esi, %esi
	je	.L183
	cmpl	$1, %esi
	jne	.L198
	movq	8(%rdi), %rax
	leaq	8(%rdi), %rbx
.L185:
	testq	%rax, %rax
	je	.L186
	movl	(%rax), %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L198:
	.cfi_restore_state
	xorl	%eax, %eax
.L180:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L183:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L186:
	leaq	ASIdentifierChoice_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, (%rbx)
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L198
	call	ASN1_NULL_new@PLT
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L198
	movq	(%rbx), %rax
	movl	$0, (%rax)
	movl	$1, %eax
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L199:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1341:
	.size	X509v3_asid_add_inherit, .-X509v3_asid_add_inherit
	.p2align 4
	.globl	X509v3_asid_add_id_or_range
	.type	X509v3_asid_add_id_or_range, @function
X509v3_asid_add_id_or_range:
.LFB1342:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L226
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	testl	%esi, %esi
	je	.L203
	cmpl	$1, %esi
	jne	.L225
	movq	8(%rdi), %rax
	leaq	8(%rdi), %r14
	testq	%rax, %rax
	je	.L206
.L228:
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L225
.L208:
	leaq	ASIdOrRange_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L225
	testq	%r13, %r13
	je	.L227
	movl	$1, (%rax)
	leaq	ASRange_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L212
	movq	(%rax), %rdi
	call	ASN1_INTEGER_free@PLT
	movq	8(%r12), %rax
	movq	%rbx, (%rax)
	movq	8(%rax), %rdi
	call	ASN1_INTEGER_free@PLT
	movq	8(%r12), %rax
	movq	%r13, 8(%rax)
.L210:
	movq	(%r14), %rax
	movq	%r12, %rsi
	movq	8(%rax), %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L212
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L212:
	.cfi_restore_state
	leaq	ASIdOrRange_it(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_item_free@PLT
.L225:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L203:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdi, %r14
	testq	%rax, %rax
	jne	.L228
.L206:
	leaq	ASIdentifierChoice_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, (%r14)
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L225
	leaq	ASIdOrRange_cmp(%rip), %rdi
	call	OPENSSL_sk_new@PLT
	movq	%rax, 8(%r12)
	movq	(%r14), %rax
	cmpq	$0, 8(%rax)
	je	.L225
	movl	$1, (%rax)
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L227:
	movl	$0, (%rax)
	movq	%rbx, 8(%rax)
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L226:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1342:
	.size	X509v3_asid_add_id_or_range, .-X509v3_asid_add_id_or_range
	.p2align 4
	.globl	X509v3_asid_is_canonical
	.type	X509v3_asid_is_canonical, @function
X509v3_asid_is_canonical:
.LFB1345:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L257
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	testq	%r12, %r12
	je	.L235
	movl	(%r12), %eax
	testl	%eax, %eax
	je	.L235
	cmpl	$1, %eax
	je	.L236
.L237:
	xorl	%eax, %eax
.L229:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L236:
	.cfi_restore_state
	movq	8(%r12), %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	je	.L237
	movq	%r12, %rdi
	call	ASIdentifierChoice_is_canonical.part.0
	testl	%eax, %eax
	je	.L237
	.p2align 4,,10
	.p2align 3
.L235:
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L233
	movl	(%r12), %eax
	testl	%eax, %eax
	je	.L233
	cmpl	$1, %eax
	jne	.L237
	movq	8(%r12), %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	je	.L237
	movq	%r12, %rdi
	call	ASIdentifierChoice_is_canonical.part.0
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L233:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L257:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1345:
	.size	X509v3_asid_is_canonical, .-X509v3_asid_is_canonical
	.p2align 4
	.type	asid_validate_path_internal.part.0, @function
asid_validate_path_internal.part.0:
.LFB1361:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L373
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
	movl	$-1, %r14d
.L259:
	movq	%rdx, %rdi
	movq	%rdx, -56(%rbp)
	call	X509v3_asid_is_canonical
	movq	-56(%rbp), %rdx
	testl	%eax, %eax
	je	.L374
	movl	$1, %r14d
.L261:
	movq	(%rdx), %rcx
	movl	$0, -68(%rbp)
	movq	%rcx, -56(%rbp)
	testq	%rcx, %rcx
	je	.L264
	movl	(%rcx), %eax
	testl	%eax, %eax
	je	.L291
	cmpl	$1, %eax
	jne	.L292
	movq	8(%rcx), %rax
	movq	%rax, -56(%rbp)
.L264:
	movq	8(%rdx), %rsi
	movl	$0, -72(%rbp)
	movq	%rsi, -64(%rbp)
	testq	%rsi, %rsi
	je	.L266
	movl	(%rsi), %eax
	testl	%eax, %eax
	je	.L294
	cmpl	$1, %eax
	jne	.L295
	movq	8(%rsi), %rax
	movq	%rax, -64(%rbp)
.L266:
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jge	.L375
.L280:
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L281
	movq	296(%rax), %rdi
	testq	%rdi, %rdi
	je	.L376
	call	X509v3_asid_is_canonical
	testl	%eax, %eax
	jne	.L270
	testq	%r12, %r12
	je	.L372
	movl	%r13d, 172(%r12)
	movq	%r12, %rsi
	xorl	%edi, %edi
	movl	$41, 176(%r12)
	movq	%rbx, 184(%r12)
	call	*56(%r12)
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L372
.L270:
	movq	296(%rbx), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L377
	cmpl	$1, (%rdx)
	je	.L378
.L272:
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L379
.L276:
	cmpl	$1, (%rax)
	je	.L380
.L269:
	movq	%r15, %rdi
	addl	$1, %r13d
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jl	.L280
.L375:
	testq	%rbx, %rbx
	je	.L281
	movq	296(%rbx), %rax
	testq	%rax, %rax
	je	.L258
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L282
	movl	(%rdx), %edx
	testl	%edx, %edx
	jne	.L282
	testq	%r12, %r12
	je	.L372
	movl	%r13d, 172(%r12)
	movq	%r12, %rsi
	xorl	%edi, %edi
	movl	$46, 176(%r12)
	movq	%rbx, 184(%r12)
	call	*56(%r12)
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L372
	movq	296(%rbx), %rax
.L282:
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L258
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L258
	testq	%r12, %r12
	je	.L372
	movl	%r13d, 172(%r12)
	movq	56(%r12), %rax
	movq	%r12, %rsi
	xorl	%edi, %edi
	movq	%rbx, 184(%r12)
	movl	$46, 176(%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L374:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L372
	movq	%rdx, -56(%rbp)
	xorl	%edi, %edi
	movq	%r12, %rsi
	movl	%r14d, 172(%r12)
	movl	$41, 176(%r12)
	movq	%rbx, 184(%r12)
	call	*56(%r12)
	movq	-56(%rbp), %rdx
	testl	%eax, %eax
	movl	%eax, %r14d
	jne	.L261
	.p2align 4,,10
	.p2align 3
.L372:
	xorl	%r14d, %r14d
.L258:
	addq	$40, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L376:
	.cfi_restore_state
	movq	-56(%rbp), %rax
	orq	-64(%rbp), %rax
	je	.L296
	testq	%r12, %r12
	je	.L372
	movl	$46, 176(%r12)
	movq	%r12, %rsi
	movl	%r13d, 172(%r12)
	movq	%rbx, 184(%r12)
	call	*56(%r12)
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L372
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L377:
	cmpq	$0, -56(%rbp)
	je	.L272
	testq	%r12, %r12
	je	.L372
	movl	%r13d, 172(%r12)
	movq	%r12, %rsi
	xorl	%edi, %edi
	movl	$46, 176(%r12)
	movq	%rbx, 184(%r12)
	call	*56(%r12)
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L372
	movq	296(%rbx), %rax
	movq	(%rax), %rcx
	movq	%rcx, -56(%rbp)
	testq	%rcx, %rcx
	jne	.L273
.L371:
	movq	8(%rax), %rax
	movl	$0, -68(%rbp)
	testq	%rax, %rax
	jne	.L276
.L379:
	cmpq	$0, -64(%rbp)
	je	.L269
	testq	%r12, %r12
	je	.L372
	movl	%r13d, 172(%r12)
	movq	%r12, %rsi
	xorl	%edi, %edi
	movl	$46, 176(%r12)
	movq	%rbx, 184(%r12)
	call	*56(%r12)
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L372
	movq	296(%rbx), %rax
	movq	8(%rax), %rax
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	jne	.L381
.L297:
	movl	$0, -72(%rbp)
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L296:
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L378:
	movl	-68(%rbp), %esi
	testl	%esi, %esi
	je	.L382
.L274:
	movq	8(%rdx), %rdx
	movl	$0, -68(%rbp)
	movq	%rdx, -56(%rbp)
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L380:
	movl	-72(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L383
.L278:
	movq	8(%rax), %rax
	movl	$0, -72(%rbp)
	movq	%rax, -64(%rbp)
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L373:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	movq	296(%rax), %rdx
	movq	%rax, %rbx
	testq	%rdx, %rdx
	je	.L288
	movl	$1, %r13d
	xorl	%r14d, %r14d
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L292:
	movq	$0, -56(%rbp)
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L295:
	movq	$0, -64(%rbp)
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L294:
	movl	$1, -72(%rbp)
	movq	$0, -64(%rbp)
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L291:
	movl	$1, -68(%rbp)
	movq	$0, -56(%rbp)
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L382:
	movq	8(%rdx), %rdi
	movq	-56(%rbp), %rsi
	call	asid_contains
	testl	%eax, %eax
	je	.L275
.L285:
	movq	296(%rbx), %rax
	movq	(%rax), %rdx
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L383:
	movq	8(%rax), %rdi
	movq	-64(%rbp), %rsi
	call	asid_contains
	testl	%eax, %eax
	je	.L279
.L283:
	movq	296(%rbx), %rax
	movq	8(%rax), %rax
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L281:
	testq	%r12, %r12
	je	.L372
	movl	$1, 176(%r12)
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L275:
	testq	%r12, %r12
	je	.L372
.L286:
	movl	%r13d, 172(%r12)
	movq	%r12, %rsi
	xorl	%edi, %edi
	movl	$46, 176(%r12)
	movq	%rbx, 184(%r12)
	call	*56(%r12)
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L372
	movl	$0, -68(%rbp)
	movq	296(%rbx), %rax
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L279:
	testq	%r12, %r12
	je	.L372
.L284:
	movl	%r13d, 172(%r12)
	movq	%r12, %rsi
	xorl	%edi, %edi
	movl	$46, 176(%r12)
	movq	%rbx, 184(%r12)
	call	*56(%r12)
	movl	%eax, %r14d
	testl	%eax, %eax
	jne	.L297
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L288:
	movl	$1, %r14d
	jmp	.L258
.L381:
	cmpl	$1, (%rax)
	je	.L277
	movq	$0, -64(%rbp)
	movl	$0, -72(%rbp)
	jmp	.L269
.L273:
	movq	-56(%rbp), %rcx
	cmpl	$1, (%rcx)
	je	.L384
	movq	$0, -56(%rbp)
	jmp	.L371
.L277:
	movq	-64(%rbp), %rax
	xorl	%esi, %esi
	movq	8(%rax), %rdi
	call	asid_contains
	testl	%eax, %eax
	jne	.L283
	movq	$0, -64(%rbp)
	jmp	.L284
.L384:
	movq	8(%rcx), %rdi
	xorl	%esi, %esi
	call	asid_contains
	testl	%eax, %eax
	jne	.L285
	movq	$0, -56(%rbp)
	jmp	.L286
	.cfi_endproc
.LFE1361:
	.size	asid_validate_path_internal.part.0, .-asid_validate_path_internal.part.0
	.p2align 4
	.globl	X509v3_asid_canonize
	.type	X509v3_asid_canonize, @function
X509v3_asid_canonize:
.LFB1347:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L413
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	testq	%r12, %r12
	je	.L391
	movl	(%r12), %eax
	testl	%eax, %eax
	je	.L391
	cmpl	$1, %eax
	je	.L392
.L393:
	movl	$376, %r8d
	movl	$116, %edx
	movl	$161, %esi
	movl	$34, %edi
	leaq	.LC2(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L385:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L392:
	.cfi_restore_state
	movq	8(%r12), %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	je	.L393
	movq	%r12, %rdi
	call	ASIdentifierChoice_canonize.part.0
	testl	%eax, %eax
	je	.L385
	.p2align 4,,10
	.p2align 3
.L391:
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L389
	movl	(%r12), %eax
	testl	%eax, %eax
	je	.L389
	cmpl	$1, %eax
	jne	.L393
	movq	8(%r12), %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	je	.L393
	movq	%r12, %rdi
	call	ASIdentifierChoice_canonize.part.0
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L389:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L413:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1347:
	.size	X509v3_asid_canonize, .-X509v3_asid_canonize
	.section	.rodata.str1.1
.LC9:
	.string	"AS"
.LC10:
	.string	"RDI"
.LC11:
	.string	",value:"
.LC12:
	.string	",name:"
.LC13:
	.string	"section:"
.LC14:
	.string	"inherit"
.LC15:
	.string	"0123456789"
.LC16:
	.string	" \t"
	.text
	.p2align 4
	.type	v2i_ASIdentifiers, @function
v2i_ASIdentifiers:
.LFB1348:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	ASIdentifiers_it(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	call	ASN1_item_new@PLT
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	jne	.L415
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L417:
	movq	16(%r12), %r15
	movl	$8, %ecx
	leaq	.LC14(%rip), %rdi
	movq	%r15, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L455
.L421:
	movq	%r15, %rdi
	leaq	.LC15(%rip), %rsi
	call	strspn@PLT
	movq	%rax, %rdx
	cltq
	leaq	(%r15,%rax), %rdi
	movq	%rdx, -80(%rbp)
	cmpb	$0, (%rdi)
	movq	%rax, -88(%rbp)
	je	.L428
	leaq	.LC16(%rip), %rsi
	call	strspn@PLT
	movq	-80(%rbp), %rdx
	addl	%edx, %eax
	movslq	%eax, %rdx
	cmpb	$45, (%r15,%rdx)
	jne	.L456
	leal	1(%rax), %edx
	leaq	.LC16(%rip), %rsi
	movslq	%edx, %rdi
	movl	%edx, -80(%rbp)
	addq	%r15, %rdi
	call	strspn@PLT
	movl	-80(%rbp), %edx
	leaq	.LC15(%rip), %rsi
	addl	%eax, %edx
	movslq	%edx, %rax
	movl	%edx, -80(%rbp)
	leaq	(%r15,%rax), %rdi
	movq	%rax, -96(%rbp)
	call	strspn@PLT
	movl	-80(%rbp), %edx
	addl	%eax, %edx
	movslq	%edx, %rdx
	cmpb	$0, (%r15,%rdx)
	jne	.L457
	movl	$595, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	CRYPTO_strdup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L458
	movq	-88(%rbp), %rax
	movq	%r12, %rsi
	xorl	%edi, %edi
	movb	$0, (%r12,%rax)
	call	s2i_ASN1_INTEGER@PLT
	movq	-96(%rbp), %rsi
	xorl	%edi, %edi
	movq	%rax, -64(%rbp)
	addq	%r12, %rsi
	call	s2i_ASN1_INTEGER@PLT
	movq	%r12, %rdi
	movl	$603, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %r15
	call	CRYPTO_free@PLT
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L435
	testq	%r15, %r15
	je	.L435
	movq	%r15, %rsi
	call	ASN1_INTEGER_cmp@PLT
	testl	%eax, %eax
	jg	.L459
.L432:
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rdi
	movq	%r15, %rcx
	movl	%r13d, %esi
	call	X509v3_asid_add_id_or_range
	testl	%eax, %eax
	je	.L460
	movq	$0, -64(%rbp)
.L427:
	addl	$1, %ebx
.L415:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L461
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	leaq	.LC9(%rip), %rsi
	movq	8(%rax), %rdi
	movq	%rax, %r12
	call	name_cmp@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L417
	movq	8(%r12), %rdi
	leaq	.LC10(%rip), %rsi
	call	name_cmp@PLT
	testl	%eax, %eax
	jne	.L462
	movq	16(%r12), %r15
	movl	$8, %ecx
	leaq	.LC14(%rip), %rdi
	movl	$1, %r13d
	movq	%r15, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L421
	movq	-72(%rbp), %rax
	leaq	8(%rax), %r13
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L463
.L424:
	movq	0(%r13), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L427
.L426:
	movl	$554, %r8d
	leaq	.LC2(%rip), %rcx
	movl	$165, %edx
.L451:
	movl	$163, %esi
	movl	$34, %edi
	xorl	%r15d, %r15d
	call	ERR_put_error@PLT
	subq	$8, %rsp
	movq	8(%r12), %r8
	xorl	%eax, %eax
	pushq	16(%r12)
	movq	(%r12), %rdx
	leaq	.LC12(%rip), %rcx
	leaq	.LC11(%rip), %r9
	leaq	.LC13(%rip), %rsi
	movl	$6, %edi
	call	ERR_add_error_data@PLT
	popq	%rdx
	popq	%rcx
.L419:
	movq	-72(%rbp), %rdi
	leaq	ASIdentifiers_it(%rip), %rsi
	call	ASN1_item_free@PLT
	movq	-64(%rbp), %rdi
	call	ASN1_INTEGER_free@PLT
	movq	%r15, %rdi
	call	ASN1_INTEGER_free@PLT
	movq	$0, -72(%rbp)
.L414:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L464
	movq	-72(%rbp), %rax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L428:
	.cfi_restore_state
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	call	X509V3_get_value_int@PLT
	testl	%eax, %eax
	je	.L431
	xorl	%r15d, %r15d
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L455:
	movq	-72(%rbp), %rax
	movq	%rax, %r13
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L424
.L463:
	leaq	ASIdentifierChoice_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, 0(%r13)
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L426
	call	ASN1_NULL_new@PLT
	movq	%rax, 8(%r15)
	testq	%rax, %rax
	je	.L426
	movq	0(%r13), %rax
	movl	$0, (%rax)
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L461:
	movq	-72(%rbp), %rdi
	call	X509v3_asid_canonize
	testl	%eax, %eax
	jne	.L414
	xorl	%r15d, %r15d
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L435:
	movl	$605, %r8d
.L452:
	movl	$65, %edx
	movl	$163, %esi
	movl	$34, %edi
	leaq	.LC2(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L460:
	movl	$615, %r8d
	jmp	.L452
.L456:
	movl	$570, %r8d
	leaq	.LC2(%rip), %rcx
	movl	$162, %edx
	jmp	.L451
.L457:
	movl	$579, %r8d
	leaq	.LC2(%rip), %rcx
	movl	$163, %edx
	jmp	.L451
.L458:
	movl	$597, %r8d
.L453:
	leaq	.LC2(%rip), %rcx
	movl	$65, %edx
	movl	$163, %esi
	xorl	%r15d, %r15d
	movl	$34, %edi
	call	ERR_put_error@PLT
	jmp	.L419
.L459:
	movl	$609, %r8d
	movl	$116, %edx
	movl	$163, %esi
	movl	$34, %edi
	leaq	.LC2(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L419
.L454:
	movl	$526, %r8d
	movl	$65, %edx
	movl	$163, %esi
	movl	$34, %edi
	leaq	.LC2(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L414
.L462:
	movl	$542, %r8d
	leaq	.LC2(%rip), %rcx
	movl	$115, %edx
	jmp	.L451
.L431:
	movl	$591, %r8d
	jmp	.L453
.L464:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1348:
	.size	v2i_ASIdentifiers, .-v2i_ASIdentifiers
	.p2align 4
	.globl	X509v3_asid_inherits
	.type	X509v3_asid_inherits, @function
X509v3_asid_inherits:
.LFB1349:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L465
	movq	(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L467
	movl	(%rdx), %ecx
	movl	$1, %eax
	testl	%ecx, %ecx
	je	.L465
.L467:
	movq	8(%rdi), %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L465
	movl	(%rdx), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	sete	%al
.L465:
	ret
	.cfi_endproc
.LFE1349:
	.size	X509v3_asid_inherits, .-X509v3_asid_inherits
	.p2align 4
	.globl	X509v3_asid_subset
	.type	X509v3_asid_subset, @function
X509v3_asid_subset:
.LFB1351:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L501
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpq	%rsi, %rdi
	je	.L476
	testq	%rsi, %rsi
	je	.L480
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L479
	movl	(%rax), %r8d
	testl	%r8d, %r8d
	jne	.L479
.L480:
	xorl	%eax, %eax
.L476:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L479:
	.cfi_restore_state
	movq	8(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L481
	movl	(%rdx), %edi
	testl	%edi, %edi
	je	.L480
.L481:
	movq	(%r12), %rdx
	testq	%rdx, %rdx
	je	.L482
	movl	(%rdx), %esi
	testl	%esi, %esi
	je	.L480
.L482:
	movq	8(%r12), %rcx
	testq	%rcx, %rcx
	je	.L483
	movl	(%rcx), %ecx
	testl	%ecx, %ecx
	je	.L480
.L483:
	movq	8(%rax), %rsi
	movq	8(%rdx), %rdi
	call	asid_contains
	testl	%eax, %eax
	je	.L480
	movq	8(%rbx), %rax
	movq	8(%rax), %rsi
	movq	8(%r12), %rax
	movq	8(%rax), %rdi
	call	asid_contains
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L501:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1351:
	.size	X509v3_asid_subset, .-X509v3_asid_subset
	.p2align 4
	.globl	X509v3_asid_validate_path
	.type	X509v3_asid_validate_path, @function
X509v3_asid_validate_path:
.LFB1353:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	152(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L508
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	je	.L508
	cmpq	$0, 56(%r12)
	je	.L508
	movq	152(%r12), %r13
	testq	%r13, %r13
	je	.L508
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jle	.L508
	cmpq	$0, 56(%r12)
	je	.L508
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%edx, %edx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	asid_validate_path_internal.part.0
	.p2align 4,,10
	.p2align 3
.L508:
	.cfi_restore_state
	movl	$1, 176(%r12)
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1353:
	.size	X509v3_asid_validate_path, .-X509v3_asid_validate_path
	.p2align 4
	.globl	X509v3_asid_validate_resource_set
	.type	X509v3_asid_validate_resource_set, @function
X509v3_asid_validate_resource_set:
.LFB1354:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L523
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	testq	%rdi, %rdi
	je	.L520
	movq	%rsi, %r13
	movl	%edx, %ebx
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	je	.L520
	testl	%ebx, %ebx
	jne	.L521
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L522
	movl	(%rax), %edx
	testl	%edx, %edx
	je	.L520
.L522:
	movq	8(%r13), %rax
	testq	%rax, %rax
	je	.L521
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L520
.L521:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jle	.L520
	addq	$8, %rsp
	movq	%r13, %rdx
	movq	%r12, %rsi
	xorl	%edi, %edi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	asid_validate_path_internal.part.0
	.p2align 4,,10
	.p2align 3
.L520:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L523:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1354:
	.size	X509v3_asid_validate_resource_set, .-X509v3_asid_validate_resource_set
	.globl	v3_asid
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	v3_asid, @object
	.size	v3_asid, 104
v3_asid:
	.long	291
	.long	0
	.quad	ASIdentifiers_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	v2i_ASIdentifiers
	.quad	i2r_ASIdentifiers
	.quad	0
	.quad	0
	.globl	ASIdentifiers_it
	.section	.rodata.str1.1
.LC17:
	.string	"ASIdentifiers"
	.section	.data.rel.ro.local
	.align 32
	.type	ASIdentifiers_it, @object
	.size	ASIdentifiers_it, 56
ASIdentifiers_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	ASIdentifiers_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC17
	.section	.rodata.str1.1
.LC18:
	.string	"asnum"
.LC19:
	.string	"rdi"
	.section	.data.rel.ro.local
	.align 32
	.type	ASIdentifiers_seq_tt, @object
	.size	ASIdentifiers_seq_tt, 80
ASIdentifiers_seq_tt:
	.quad	145
	.quad	0
	.quad	0
	.quad	.LC18
	.quad	ASIdentifierChoice_it
	.quad	145
	.quad	1
	.quad	8
	.quad	.LC19
	.quad	ASIdentifierChoice_it
	.globl	ASIdentifierChoice_it
	.section	.rodata.str1.1
.LC20:
	.string	"ASIdentifierChoice"
	.section	.data.rel.ro.local
	.align 32
	.type	ASIdentifierChoice_it, @object
	.size	ASIdentifierChoice_it, 56
ASIdentifierChoice_it:
	.byte	2
	.zero	7
	.quad	0
	.quad	ASIdentifierChoice_ch_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC20
	.section	.rodata.str1.1
.LC21:
	.string	"u.inherit"
.LC22:
	.string	"u.asIdsOrRanges"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	ASIdentifierChoice_ch_tt, @object
	.size	ASIdentifierChoice_ch_tt, 80
ASIdentifierChoice_ch_tt:
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC21
	.quad	ASN1_NULL_it
	.quad	4
	.quad	0
	.quad	8
	.quad	.LC22
	.quad	ASIdOrRange_it
	.globl	ASIdOrRange_it
	.section	.rodata.str1.1
.LC23:
	.string	"ASIdOrRange"
	.section	.data.rel.ro.local
	.align 32
	.type	ASIdOrRange_it, @object
	.size	ASIdOrRange_it, 56
ASIdOrRange_it:
	.byte	2
	.zero	7
	.quad	0
	.quad	ASIdOrRange_ch_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC23
	.section	.rodata.str1.1
.LC24:
	.string	"u.id"
.LC25:
	.string	"u.range"
	.section	.data.rel.ro
	.align 32
	.type	ASIdOrRange_ch_tt, @object
	.size	ASIdOrRange_ch_tt, 80
ASIdOrRange_ch_tt:
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC24
	.quad	ASN1_INTEGER_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC25
	.quad	ASRange_it
	.globl	ASRange_it
	.section	.rodata.str1.1
.LC26:
	.string	"ASRange"
	.section	.data.rel.ro.local
	.align 32
	.type	ASRange_it, @object
	.size	ASRange_it, 56
ASRange_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	ASRange_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC26
	.section	.rodata.str1.1
.LC27:
	.string	"min"
.LC28:
	.string	"max"
	.section	.data.rel.ro
	.align 32
	.type	ASRange_seq_tt, @object
	.size	ASRange_seq_tt, 80
ASRange_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC27
	.quad	ASN1_INTEGER_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC28
	.quad	ASN1_INTEGER_it
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
