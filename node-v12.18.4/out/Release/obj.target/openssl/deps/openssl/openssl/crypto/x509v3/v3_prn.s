	.file	"v3_prn.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	"%*s"
.LC2:
	.string	"<EMPTY>\n"
.LC3:
	.string	", "
.LC4:
	.string	"%s:%s"
.LC5:
	.string	"\n"
	.text
	.p2align 4
	.globl	X509V3_EXT_val_prn
	.type	X509V3_EXT_val_prn, @function
X509V3_EXT_val_prn:
.LFB1296:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L26
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$8, %rsp
	testl	%ecx, %ecx
	je	.L6
	movq	%rsi, %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	je	.L6
.L7:
	xorl	%ebx, %ebx
	leaq	.LC0(%rip), %r15
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L16:
	leaq	.LC4(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L15:
	leaq	.LC5(%rip), %rsi
	movq	%r13, %rdi
	addl	$1, %ebx
	call	BIO_puts@PLT
.L17:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L1
	movl	%r14d, %edx
	movq	%r15, %rcx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L30
	movq	16(%rax), %rcx
	testq	%rcx, %rcx
	jne	.L16
	movq	%rdx, %rsi
	movq	%r13, %rdi
	call	BIO_puts@PLT
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L6:
	movq	%r13, %rdi
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rcx
	movl	%r14d, %edx
	leaq	.LC1(%rip), %rsi
	call	BIO_printf@PLT
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	je	.L31
.L5:
	testl	%ebx, %ebx
	jne	.L7
	leaq	.LC3(%rip), %r14
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L33:
	leaq	.LC4(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L12:
	addl	$1, %ebx
.L13:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L1
	testl	%ebx, %ebx
	jne	.L32
.L9:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L10
	movq	16(%rax), %rcx
	testq	%rcx, %rcx
	jne	.L33
	movq	%rdx, %rsi
	movq	%r13, %rdi
	call	BIO_puts@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L32:
	movq	%r14, %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movq	16(%rax), %rsi
	movq	%r13, %rdi
	call	BIO_puts@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L31:
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_puts@PLT
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L30:
	movq	16(%rax), %rsi
	movq	%r13, %rdi
	call	BIO_puts@PLT
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE1296:
	.size	X509V3_EXT_val_prn, .-X509V3_EXT_val_prn
	.section	.rodata.str1.1
.LC6:
	.string	"%*s<Not Supported>"
.LC7:
	.string	"%*s<Parse Error>"
.LC8:
	.string	"%*s%s"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"../deps/openssl/openssl/crypto/x509v3/v3_prn.c"
	.text
	.p2align 4
	.globl	X509V3_EXT_print
	.type	X509V3_EXT_print, @function
X509V3_EXT_print:
.LFB1297:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rsi, %rdi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -80(%rbp)
	movl	%ecx, -68(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	X509_EXTENSION_get_data@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	ASN1_STRING_get0_data@PLT
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	call	ASN1_STRING_length@PLT
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	X509V3_EXT_get@PLT
	testq	%rax, %rax
	je	.L66
	movq	8(%rax), %rcx
	movslq	%r13d, %r12
	movq	%rax, %rbx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdx
	xorl	%edi, %edi
	testq	%rcx, %rcx
	je	.L43
	call	ASN1_item_d2i@PLT
	movq	%rax, %r15
.L44:
	testq	%r15, %r15
	je	.L67
	movq	48(%rbx), %rax
	testq	%rax, %rax
	je	.L50
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	*%rax
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L55
	movl	-68(%rbp), %edx
	movq	%rax, %r8
	movq	%r14, %rdi
	leaq	.LC0(%rip), %rcx
	leaq	.LC8(%rip), %rsi
	xorl	%eax, %eax
	movl	$1, %r14d
	xorl	%r13d, %r13d
	call	BIO_printf@PLT
.L51:
	movq	X509V3_conf_free@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_pop_free@PLT
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	movl	$130, %edx
	call	CRYPTO_free@PLT
	movq	8(%rbx), %rsi
	movq	%r15, %rdi
	testq	%rsi, %rsi
	je	.L53
	call	ASN1_item_free@PLT
.L34:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L68
	addq	$40, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	call	*32(%rax)
	movq	%rax, %r15
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L53:
	call	*24(%rbx)
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L50:
	movq	64(%rbx), %rax
	testq	%rax, %rax
	je	.L52
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	*%rax
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L56
	movl	4(%rbx), %ecx
	movl	-68(%rbp), %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	movl	$1, %r14d
	xorl	%r12d, %r12d
	andl	$4, %ecx
	call	X509V3_EXT_val_prn
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L52:
	movq	80(%rbx), %rax
	testq	%rax, %rax
	je	.L57
	movq	%r14, %rdx
	movl	-68(%rbp), %ecx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	*%rax
	xorl	%r14d, %r14d
	testl	%eax, %eax
	setne	%r14b
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L55:
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L66:
	movq	-80(%rbp), %r12
	movq	-64(%rbp), %rsi
	andl	$983040, %r12d
	cmpq	$131072, %r12
	je	.L36
	ja	.L37
	testq	%r12, %r12
	je	.L38
	cmpq	$65536, %r12
	jne	.L65
	movl	-68(%rbp), %edx
	leaq	.LC0(%rip), %rcx
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC6(%rip), %rsi
	call	BIO_printf@PLT
.L65:
	movl	$1, %r14d
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L37:
	cmpq	$196608, %r12
	jne	.L65
.L49:
	movl	-68(%rbp), %ecx
	movq	%r14, %rdi
	movl	%r13d, %edx
	call	BIO_dump_indent@PLT
	movl	%eax, %r14d
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L67:
	movq	-80(%rbp), %rax
	movq	-64(%rbp), %rsi
	andl	$983040, %eax
	cmpq	$131072, %rax
	je	.L46
	ja	.L47
	testq	%rax, %rax
	je	.L38
	cmpq	$65536, %rax
	jne	.L65
	movl	-68(%rbp), %edx
	movq	%r14, %rdi
	leaq	.LC0(%rip), %rcx
	xorl	%eax, %eax
	leaq	.LC7(%rip), %rsi
	movl	$1, %r14d
	call	BIO_printf@PLT
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L47:
	cmpq	$196608, %rax
	je	.L49
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L56:
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L38:
	xorl	%r14d, %r14d
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L57:
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L36:
	movl	-68(%rbp), %ecx
	movq	%r14, %rdi
	movslq	%r13d, %rdx
	movl	$-1, %r8d
	call	ASN1_parse_dump@PLT
	movl	%eax, %r14d
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L46:
	movl	-68(%rbp), %ecx
	movq	%r14, %rdi
	movl	$-1, %r8d
	movq	%r12, %rdx
	call	ASN1_parse_dump@PLT
	movl	%eax, %r14d
	jmp	.L34
.L68:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1297:
	.size	X509V3_EXT_print, .-X509V3_EXT_print
	.section	.rodata.str1.1
.LC10:
	.string	"critical"
.LC11:
	.string	"%*s%s:\n"
.LC12:
	.string	": %s\n"
	.text
	.p2align 4
	.globl	X509V3_extensions_print
	.type	X509V3_extensions_print, @function
X509V3_extensions_print:
.LFB1298:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	subq	$24, %rsp
	movq	%rcx, -64(%rbp)
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jle	.L81
	testq	%r12, %r12
	je	.L72
	movl	%r14d, %edx
	movq	%r12, %r8
	leaq	.LC0(%rip), %rcx
	movq	%rbx, %rdi
	leaq	.LC11(%rip), %rsi
	xorl	%eax, %eax
	addl	$4, %r14d
	call	BIO_printf@PLT
.L72:
	xorl	%r13d, %r13d
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L78:
	movl	$1, %edx
	leaq	.LC5(%rip), %rsi
	movq	%rbx, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jle	.L77
	addl	$1, %r13d
.L73:
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jge	.L81
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r12
	testl	%r14d, %r14d
	je	.L76
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rcx
	movl	%r14d, %edx
	movq	%rbx, %rdi
	leaq	.LC1(%rip), %rsi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L77
.L76:
	movq	%r12, %rdi
	call	X509_EXTENSION_get_object@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	i2a_ASN1_OBJECT@PLT
	movq	%r12, %rdi
	call	X509_EXTENSION_get_critical@PLT
	leaq	.LC0(%rip), %rdx
	leaq	.LC12(%rip), %rsi
	movq	%rbx, %rdi
	testl	%eax, %eax
	leaq	.LC10(%rip), %rax
	cmovne	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L77
	leal	4(%r14), %r8d
	movq	-64(%rbp), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movl	%r8d, %ecx
	movl	%r8d, -52(%rbp)
	call	X509V3_EXT_print
	movl	-52(%rbp), %r8d
	testl	%eax, %eax
	jne	.L78
	leaq	.LC0(%rip), %rcx
	movl	%r8d, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rbx, %rdi
	call	BIO_printf@PLT
	movq	%r12, %rdi
	call	X509_EXTENSION_get_data@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	ASN1_STRING_print@PLT
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L77:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1298:
	.size	X509V3_extensions_print, .-X509V3_extensions_print
	.p2align 4
	.globl	X509V3_EXT_print_fp
	.type	X509V3_EXT_print_fp, @function
X509V3_EXT_print_fp:
.LFB1300:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	xorl	%esi, %esi
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$8, %rsp
	call	BIO_new_fp@PLT
	testq	%rax, %rax
	je	.L89
	movq	%rax, %r12
	movq	%rax, %rdi
	movslq	%ebx, %rdx
	movl	%r15d, %ecx
	movq	%r14, %rsi
	call	X509V3_EXT_print
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	BIO_free@PLT
.L89:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1300:
	.size	X509V3_EXT_print_fp, .-X509V3_EXT_print_fp
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
