	.file	"pbe_scrypt.c"
	.text
	.p2align 4
	.type	scryptBlockMix, @function
scryptBlockMix:
.LFB383:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -304(%rbp)
	movq	%rdi, -296(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, %rax
	movq	$0, -240(%rbp)
	salq	$7, %rax
	addq	%rdx, %rdx
	leaq	-64(%rsi,%rax), %rax
	movq	%rdx, -288(%rbp)
	movdqu	(%rax), %xmm3
	movdqu	16(%rax), %xmm4
	movdqu	32(%rax), %xmm5
	movaps	%xmm3, -192(%rbp)
	movdqu	48(%rax), %xmm3
	leaq	-128(%rbp), %rax
	movq	%rax, -312(%rbp)
	leaq	-192(%rbp), %rax
	movq	%rax, -280(%rbp)
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm3, -144(%rbp)
	je	.L3
	.p2align 4,,10
	.p2align 3
.L2:
	movl	0(%r13), %eax
	xorl	-192(%rbp), %eax
	addq	$64, %r13
	movd	%eax, %xmm2
	movl	-60(%r13), %eax
	xorl	-188(%rbp), %eax
	movd	%eax, %xmm13
	movl	-56(%r13), %eax
	xorl	-184(%rbp), %eax
	movd	%eax, %xmm5
	movl	-52(%r13), %eax
	xorl	-180(%rbp), %eax
	movd	%eax, %xmm14
	movl	-48(%r13), %eax
	xorl	-176(%rbp), %eax
	movd	%eax, %xmm1
	movl	-44(%r13), %eax
	xorl	-172(%rbp), %eax
	movd	%eax, %xmm11
	movl	-40(%r13), %eax
	xorl	-168(%rbp), %eax
	movd	%eax, %xmm4
	movl	-36(%r13), %eax
	xorl	-164(%rbp), %eax
	movd	%eax, %xmm12
	movl	-32(%r13), %eax
	xorl	-160(%rbp), %eax
	movd	%eax, %xmm0
	movl	-28(%r13), %eax
	xorl	-156(%rbp), %eax
	movd	%eax, %xmm10
	movl	-24(%r13), %eax
	xorl	-152(%rbp), %eax
	movd	%eax, %xmm3
	movl	-20(%r13), %eax
	xorl	-148(%rbp), %eax
	movl	%eax, -244(%rbp)
	movl	-16(%r13), %eax
	xorl	-144(%rbp), %eax
	movl	%eax, -248(%rbp)
	movl	-12(%r13), %eax
	xorl	-140(%rbp), %eax
	movl	%eax, -252(%rbp)
	movl	-8(%r13), %eax
	xorl	-136(%rbp), %eax
	movdqu	-64(%r13), %xmm7
	movdqu	-32(%r13), %xmm6
	pxor	-192(%rbp), %xmm7
	movl	%eax, -256(%rbp)
	pxor	-160(%rbp), %xmm6
	movl	-4(%r13), %eax
	movaps	%xmm7, -128(%rbp)
	movd	%xmm7, %ebx
	movl	-120(%rbp), %r11d
	movl	-124(%rbp), %r12d
	movdqu	-48(%r13), %xmm7
	movaps	%xmm6, -96(%rbp)
	movl	-84(%rbp), %r14d
	movd	%xmm6, %r9d
	xorl	-132(%rbp), %eax
	movl	-92(%rbp), %edx
	movl	%r11d, -208(%rbp)
	movl	-116(%rbp), %r11d
	movl	-88(%rbp), %esi
	movl	%r14d, -220(%rbp)
	pxor	-176(%rbp), %xmm7
	movl	%eax, -260(%rbp)
	movl	%r11d, -212(%rbp)
	movaps	%xmm7, -112(%rbp)
	movd	%xmm7, %ecx
	movl	-100(%rbp), %r14d
	movl	-108(%rbp), %r10d
	movdqu	-16(%r13), %xmm7
	pxor	-144(%rbp), %xmm7
	movl	-104(%rbp), %r15d
	movaps	%xmm7, -80(%rbp)
	movl	-76(%rbp), %edi
	movl	-72(%rbp), %eax
	movd	%xmm7, %r8d
	movl	-68(%rbp), %r11d
	movl	%r14d, -216(%rbp)
	movl	$4, -232(%rbp)
	movq	%r13, -272(%rbp)
	movl	-220(%rbp), %r13d
.L4:
	leal	(%rbx,%r8), %r14d
	roll	$7, %r14d
	xorl	%r14d, %ecx
	leal	(%rcx,%rbx), %r14d
	roll	$9, %r14d
	xorl	%r9d, %r14d
	leal	(%rcx,%r14), %r9d
	movl	%r14d, -220(%rbp)
	roll	$13, %r9d
	xorl	%r8d, %r9d
	movl	%r14d, %r8d
	leal	(%r11,%r13), %r14d
	addl	%r9d, %r8d
	movl	%r9d, -224(%rbp)
	roll	$7, %r14d
	xorl	-212(%rbp), %r14d
	rorl	$14, %r8d
	xorl	%r8d, %ebx
	leal	(%r10,%r12), %r8d
	roll	$7, %r8d
	xorl	%r8d, %edx
	leal	(%rdx,%r10), %r8d
	roll	$9, %r8d
	movl	%r8d, %r9d
	xorl	%edi, %r9d
	leal	(%rdx,%r9), %edi
	movl	%r9d, -228(%rbp)
	roll	$13, %edi
	xorl	%edi, %r12d
	leal	(%r9,%r12), %edi
	rorl	$14, %edi
	xorl	%edi, %r10d
	leal	(%rsi,%r15), %edi
	roll	$7, %edi
	xorl	%edi, %eax
	leal	(%rax,%rsi), %r8d
	roll	$9, %r8d
	xorl	-208(%rbp), %r8d
	leal	(%rax,%r8), %edi
	roll	$13, %edi
	xorl	%edi, %r15d
	leal	(%r8,%r15), %edi
	rorl	$14, %edi
	xorl	%edi, %esi
	leal	(%r14,%r11), %edi
	roll	$9, %edi
	xorl	-216(%rbp), %edi
	leal	(%r14,%rdi), %r9d
	roll	$13, %r9d
	xorl	%r13d, %r9d
	leal	(%rdi,%r9), %r13d
	rorl	$14, %r13d
	xorl	%r13d, %r11d
	leal	(%rbx,%r14), %r13d
	roll	$7, %r13d
	xorl	%r13d, %r12d
	leal	(%rbx,%r12), %r13d
	roll	$9, %r13d
	xorl	%r8d, %r13d
	leal	(%r12,%r13), %r8d
	movl	%r13d, -208(%rbp)
	roll	$13, %r8d
	xorl	%r14d, %r8d
	addl	%r8d, %r13d
	movl	%r8d, -212(%rbp)
	movl	%r13d, %r8d
	rorl	$14, %r8d
	xorl	%r8d, %ebx
	leal	(%rcx,%r10), %r8d
	roll	$7, %r8d
	xorl	%r8d, %r15d
	leal	(%r10,%r15), %r8d
	roll	$9, %r8d
	movl	%r8d, %r13d
	leal	(%rax,%r11), %r8d
	xorl	%edi, %r13d
	roll	$7, %r8d
	xorl	-224(%rbp), %r8d
	leal	(%r15,%r13), %edi
	movl	%r13d, -216(%rbp)
	roll	$13, %edi
	xorl	%edi, %ecx
	leal	0(%r13,%rcx), %edi
	leal	(%rdx,%rsi), %r13d
	roll	$7, %r13d
	rorl	$14, %edi
	xorl	%r9d, %r13d
	xorl	%edi, %r10d
	leal	(%rsi,%r13), %r9d
	roll	$9, %r9d
	xorl	-220(%rbp), %r9d
	leal	0(%r13,%r9), %edi
	roll	$13, %edi
	xorl	%edi, %edx
	leal	(%r9,%rdx), %edi
	rorl	$14, %edi
	xorl	%edi, %esi
	leal	(%r11,%r8), %edi
	roll	$9, %edi
	xorl	-228(%rbp), %edi
	leal	(%r8,%rdi), %r14d
	roll	$13, %r14d
	xorl	%r14d, %eax
	leal	(%rdi,%rax), %r14d
	rorl	$14, %r14d
	xorl	%r14d, %r11d
	subl	$1, -232(%rbp)
	jne	.L4
	movd	%eax, %xmm7
	movd	%r11d, %xmm6
	movd	%edi, %xmm15
	movl	%r13d, %r14d
	punpckldq	%xmm6, %xmm7
	movd	%r8d, %xmm6
	movd	%esi, %xmm8
	movq	-312(%rbp), %rdi
	punpckldq	%xmm15, %xmm6
	movd	%edx, %xmm15
	punpckldq	%xmm12, %xmm4
	movl	$64, %esi
	punpcklqdq	%xmm7, %xmm6
	movd	%r14d, %xmm7
	punpckldq	%xmm11, %xmm1
	movq	-272(%rbp), %r13
	punpckldq	%xmm7, %xmm8
	movd	%r9d, %xmm7
	movd	%r15d, %xmm9
	movaps	%xmm6, -80(%rbp)
	punpckldq	%xmm15, %xmm7
	punpcklqdq	%xmm4, %xmm1
	punpckldq	%xmm10, %xmm0
	movd	-216(%rbp), %xmm15
	punpcklqdq	%xmm8, %xmm7
	movd	%ecx, %xmm8
	punpckldq	%xmm14, %xmm5
	movd	-244(%rbp), %xmm4
	punpckldq	%xmm15, %xmm9
	movd	%r10d, %xmm15
	punpckldq	%xmm13, %xmm2
	movaps	%xmm7, -96(%rbp)
	punpckldq	%xmm15, %xmm8
	punpckldq	%xmm4, %xmm3
	punpcklqdq	%xmm5, %xmm2
	movd	-208(%rbp), %xmm15
	punpcklqdq	%xmm9, %xmm8
	punpcklqdq	%xmm3, %xmm0
	movd	-212(%rbp), %xmm9
	movd	-260(%rbp), %xmm3
	movd	-252(%rbp), %xmm5
	paddd	%xmm0, %xmm7
	movaps	%xmm8, -112(%rbp)
	movd	-248(%rbp), %xmm0
	punpckldq	%xmm9, %xmm15
	paddd	%xmm1, %xmm8
	movd	-256(%rbp), %xmm1
	movd	%ebx, %xmm9
	movq	%xmm15, -208(%rbp)
	movd	%r12d, %xmm15
	punpckldq	%xmm5, %xmm0
	punpckldq	%xmm3, %xmm1
	punpckldq	%xmm15, %xmm9
	movaps	%xmm8, -176(%rbp)
	movhps	-208(%rbp), %xmm9
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm7, -160(%rbp)
	paddd	%xmm9, %xmm2
	paddd	%xmm0, %xmm6
	movaps	%xmm9, -128(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm6, -144(%rbp)
	call	OPENSSL_cleanse@PLT
	movq	-240(%rbp), %rsi
	movq	-280(%rbp), %rcx
	movq	%rsi, %rax
	movq	%rsi, %rdx
	movdqa	(%rcx), %xmm3
	movdqa	16(%rcx), %xmm5
	andl	$1, %eax
	imulq	-304(%rbp), %rax
	shrq	%rdx
	addq	$1, %rsi
	movq	%rsi, -240(%rbp)
	addq	%rdx, %rax
	salq	$6, %rax
	addq	-296(%rbp), %rax
	movups	%xmm3, (%rax)
	movdqa	32(%rcx), %xmm3
	movups	%xmm5, 16(%rax)
	movdqa	48(%rcx), %xmm5
	movups	%xmm3, 32(%rax)
	movaps	%xmm5, -208(%rbp)
	movups	%xmm5, 48(%rax)
	cmpq	-288(%rbp), %rsi
	jne	.L2
.L3:
	movq	-280(%rbp), %rdi
	movl	$64, %esi
	call	OPENSSL_cleanse@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L12
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L12:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE383:
	.size	scryptBlockMix, .-scryptBlockMix
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/evp/pbe_scrypt.c"
	.text
	.p2align 4
	.globl	EVP_PBE_scrypt
	.type	EVP_PBE_scrypt, @function
EVP_PBE_scrypt:
.LFB385:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	$0, 16(%rbp)
	movq	%rdi, -96(%rbp)
	sete	%dl
	cmpq	$1, %r8
	movq	%rsi, -104(%rbp)
	setbe	%al
	orb	%al, %dl
	jne	.L46
	movq	%r9, %r13
	testq	%r9, %r9
	je	.L46
	leaq	-1(%r8), %rbx
	movq	%r8, %r12
	xorl	%r15d, %r15d
	testq	%r8, %rbx
	jne	.L13
	movl	$1073741823, %eax
	xorl	%edx, %edx
	movl	$174, %r8d
	divq	%r9
	cmpq	16(%rbp), %rax
	jb	.L87
	movq	%r9, %rax
	movq	%rcx, %r14
	salq	$4, %rax
	cmpq	$63, %rax
	ja	.L16
	movl	%r9d, %ecx
	movl	$1, %eax
	sall	$4, %ecx
	salq	%cl, %rax
	cmpq	%r12, %rax
	jbe	.L89
.L16:
	movq	16(%rbp), %r11
	movl	$203, %r8d
	imulq	%r13, %r11
	movq	%r11, %rax
	salq	$7, %rax
	movq	%rax, -176(%rbp)
	cmpq	$2147483647, %rax
	ja	.L88
	movabsq	$144115188075855871, %rax
	xorl	%edx, %edx
	leaq	2(%r12), %rcx
	divq	%r13
	cmpq	%rax, %rcx
	ja	.L90
	imulq	%r13, %rcx
	movl	$220, %r8d
	salq	$7, %rcx
	movq	%rcx, %rax
	notq	%rax
	cmpq	-176(%rbp), %rax
	jb	.L88
	cmpq	$0, 24(%rbp)
	movl	$33554432, %eax
	cmovne	24(%rbp), %rax
	movl	$232, %r8d
	addq	-176(%rbp), %rcx
	movq	%rax, 24(%rbp)
	movq	%rcx, -280(%rbp)
	cmpq	%rax, %rcx
	ja	.L88
	cmpq	$0, 32(%rbp)
	movq	%r10, -64(%rbp)
	movl	$1, %r15d
	movq	%r11, -56(%rbp)
	je	.L13
	movq	%rcx, %rdi
	movl	$240, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	-56(%rbp), %r11
	movq	-64(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, -72(%rbp)
	je	.L91
	movq	%r11, -56(%rbp)
	movq	%r10, -64(%rbp)
	call	EVP_sha256@PLT
	pushq	-72(%rbp)
	movq	-64(%rbp), %r10
	movl	%r14d, %ecx
	movq	%rax, %r9
	movl	-104(%rbp), %esi
	movl	$1, %r8d
	movl	-176(%rbp), %eax
	movq	-96(%rbp), %rdi
	movq	%r10, %rdx
	pushq	%rax
	call	PKCS5_PBKDF2_HMAC@PLT
	popq	%rsi
	movq	-56(%rbp), %r11
	testl	%eax, %eax
	popq	%rdi
	je	.L45
	movq	-72(%rbp), %rsi
	imulq	%r13, %rbx
	movq	%r13, %rax
	leaq	(%r11,%r13), %rdi
	salq	$7, %rax
	movq	%r13, %rcx
	movq	%r12, -56(%rbp)
	movq	%r13, %r15
	movq	-176(%rbp), %r10
	movq	%rsi, %r14
	salq	$5, %rcx
	movq	%rdi, -144(%rbp)
	salq	$7, %rbx
	salq	$7, %rdi
	movq	%rax, -64(%rbp)
	movdqa	.LC1(%rip), %xmm4
	addq	%r10, %r14
	movq	%rcx, -192(%rbp)
	shrq	$4, %rcx
	leaq	(%r14,%rax), %rdx
	movq	%r14, %r13
	movq	%rcx, -264(%rbp)
	movq	%rsi, %rcx
	movq	%rdx, -216(%rbp)
	addq	%rax, %rdx
	addq	%rdx, %rbx
	movq	%rdx, -88(%rbp)
	movq	%rbx, -224(%rbp)
	leaq	-64(%r14,%rax), %rbx
	movq	%rbx, -112(%rbp)
	leaq	64(%rax,%rdi), %rbx
	movq	%rbx, -248(%rbp)
	movq	%rax, %rbx
	addq	%rdi, %rax
	addq	%rax, %rcx
	movq	%rax, -256(%rbp)
	leaq	16(%r10), %rax
	addq	%rbx, %rdx
	movq	%rcx, -272(%rbp)
	cmpq	%rax, %rdi
	leaq	16(%rdi), %rcx
	movq	%rdx, -232(%rbp)
	setge	%dl
	cmpq	%r10, %rcx
	setle	%al
	addq	%rdi, %rsi
	movq	%rdi, -152(%rbp)
	orl	%eax, %edx
	leaq	64(%r10), %rax
	movq	%rcx, -120(%rbp)
	movq	%rax, -240(%rbp)
	leaq	16(%rbx), %rax
	movb	%dl, -121(%rbp)
	movq	%rsi, -136(%rbp)
	movq	$0, -184(%rbp)
	movq	$0, -168(%rbp)
	movq	%rax, -160(%rbp)
	.p2align 4,,10
	.p2align 3
.L38:
	movq	-72(%rbp), %r12
	movq	-184(%rbp), %rbx
	movq	-192(%rbp), %rsi
	addq	%rbx, %r12
	movq	%r12, -208(%rbp)
	testq	%rsi, %rsi
	je	.L49
	leaq	64(%rbx), %rax
	cmpq	-256(%rbp), %rax
	setle	%dl
	cmpq	-248(%rbp), %rbx
	setge	%al
	orb	%al, %dl
	je	.L50
	leaq	-1(%rsi), %rax
	cmpq	$14, %rax
	jbe	.L50
	movq	-264(%rbp), %rsi
	xorl	%eax, %eax
	xorl	%edx, %edx
	pxor	%xmm7, %xmm7
	movq	-272(%rbp), %rcx
	pxor	%xmm6, %xmm6
	.p2align 4,,10
	.p2align 3
.L27:
	movdqu	(%r12,%rax), %xmm3
	movdqu	16(%r12,%rax), %xmm1
	movdqa	%xmm4, %xmm11
	movdqa	%xmm4, %xmm10
	movdqu	16(%r12,%rax), %xmm0
	movdqu	32(%r12,%rax), %xmm9
	addq	$1, %rdx
	pand	%xmm4, %xmm1
	pand	%xmm4, %xmm3
	movdqu	32(%r12,%rax), %xmm2
	packuswb	%xmm1, %xmm3
	movdqu	(%r12,%rax), %xmm1
	psrlw	$8, %xmm0
	pand	%xmm4, %xmm9
	pand	%xmm3, %xmm11
	psrlw	$8, %xmm2
	psrlw	$8, %xmm1
	psrlw	$8, %xmm3
	packuswb	%xmm0, %xmm1
	movdqu	48(%r12,%rax), %xmm0
	pand	%xmm4, %xmm0
	packuswb	%xmm0, %xmm9
	movdqu	48(%r12,%rax), %xmm0
	pand	%xmm9, %xmm10
	psrlw	$8, %xmm9
	psrlw	$8, %xmm0
	packuswb	%xmm10, %xmm11
	movdqa	%xmm4, %xmm10
	packuswb	%xmm0, %xmm2
	movdqa	%xmm4, %xmm0
	pand	%xmm1, %xmm10
	pand	%xmm2, %xmm0
	psrlw	$8, %xmm1
	packuswb	%xmm9, %xmm3
	packuswb	%xmm0, %xmm10
	psrlw	$8, %xmm2
	movdqa	%xmm3, %xmm0
	movdqa	%xmm10, %xmm9
	packuswb	%xmm2, %xmm1
	movdqa	%xmm11, %xmm2
	punpcklbw	%xmm7, %xmm9
	punpcklbw	%xmm7, %xmm2
	movdqa	%xmm10, %xmm8
	psllw	$8, %xmm9
	punpckhbw	%xmm7, %xmm8
	punpckhbw	%xmm7, %xmm11
	por	%xmm2, %xmm9
	movdqa	%xmm3, %xmm2
	movdqa	%xmm1, %xmm10
	punpcklbw	%xmm7, %xmm2
	psllw	$8, %xmm8
	punpcklbw	%xmm7, %xmm10
	movdqa	%xmm2, %xmm3
	por	%xmm11, %xmm8
	movdqa	%xmm9, %xmm11
	punpcklwd	%xmm6, %xmm3
	punpcklwd	%xmm6, %xmm11
	punpckhwd	%xmm6, %xmm2
	pslld	$16, %xmm3
	pslld	$16, %xmm2
	punpckhwd	%xmm6, %xmm9
	por	%xmm11, %xmm3
	movdqa	%xmm10, %xmm11
	punpckhwd	%xmm6, %xmm10
	punpcklwd	%xmm6, %xmm11
	por	%xmm9, %xmm2
	punpckhbw	%xmm7, %xmm0
	pslld	$24, %xmm10
	pslld	$24, %xmm11
	punpckhbw	%xmm7, %xmm1
	por	%xmm10, %xmm2
	por	%xmm11, %xmm3
	movups	%xmm2, 16(%rcx,%rax)
	movdqa	%xmm0, %xmm2
	punpckhwd	%xmm6, %xmm0
	movups	%xmm3, (%rcx,%rax)
	punpcklwd	%xmm6, %xmm2
	movdqa	%xmm8, %xmm3
	punpckhwd	%xmm6, %xmm8
	pslld	$16, %xmm2
	pslld	$16, %xmm0
	punpcklwd	%xmm6, %xmm3
	por	%xmm3, %xmm2
	movdqa	%xmm1, %xmm3
	punpckhwd	%xmm6, %xmm1
	punpcklwd	%xmm6, %xmm3
	pslld	$24, %xmm1
	por	%xmm8, %xmm0
	pslld	$24, %xmm3
	por	%xmm1, %xmm0
	por	%xmm3, %xmm2
	movups	%xmm0, 48(%rcx,%rax)
	movups	%xmm2, 32(%rcx,%rax)
	addq	$64, %rax
	cmpq	%rsi, %rdx
	jne	.L27
.L28:
	movq	-192(%rbp), %rbx
	movq	-232(%rbp), %r14
.L25:
	movq	%r12, -200(%rbp)
	movl	$1, %eax
	movq	%r14, %r12
	movq	-64(%rbp), %r14
	movq	%r13, -80(%rbp)
	movq	%r15, %r13
	movq	%rbx, %r15
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L29:
	movq	%r12, %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	addq	$1, %rbx
	subq	%r14, %rsi
	addq	%r14, %r12
	call	scryptBlockMix
	cmpq	%rbx, -56(%rbp)
	ja	.L29
	movq	%r15, %rbx
	movq	%r13, %r15
	movq	-80(%rbp), %r13
	xorl	%r14d, %r14d
	movq	-224(%rbp), %rsi
	movq	-200(%rbp), %r12
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	scryptBlockMix
	leaq	0(,%rbx,4), %rax
	movq	%r12, -200(%rbp)
	movq	%r13, %r12
	movq	%rax, -80(%rbp)
	movq	-216(%rbp), %r13
	movq	%r15, %rax
	movq	%rbx, %r15
	movq	%r14, %rbx
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L31:
	movq	-112(%rbp), %rax
	xorl	%edx, %edx
	movl	(%rax), %eax
	divq	-56(%rbp)
	imulq	%r14, %rdx
	movq	%rdx, %rdi
	salq	$7, %rdi
	addq	-88(%rbp), %rdi
	testq	%r15, %r15
	je	.L35
	addq	-144(%rbp), %rdx
	movq	-64(%rbp), %rax
	salq	$7, %rdx
	addq	%rdx, %rax
	addq	-160(%rbp), %rdx
	cmpq	-152(%rbp), %rdx
	setle	%dl
	cmpq	%rax, -120(%rbp)
	setle	%r11b
	orl	%r11d, %edx
	testb	%dl, -121(%rbp)
	je	.L51
	leaq	-1(%r15), %rdx
	cmpq	$2, %rdx
	jbe	.L51
	movq	-72(%rbp), %rdi
	leaq	(%rdi,%rax), %rdx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L34:
	movdqu	(%r12,%rax), %xmm0
	movdqu	(%rdx,%rax), %xmm7
	movq	-136(%rbp), %rdi
	pxor	%xmm7, %xmm0
	movups	%xmm0, (%rdi,%rax)
	addq	$16, %rax
	cmpq	-80(%rbp), %rax
	jne	.L34
.L35:
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	addq	$1, %rbx
	call	scryptBlockMix
	movdqa	.LC1(%rip), %xmm4
	cmpq	%rbx, -56(%rbp)
	movdqa	%xmm4, %xmm5
	ja	.L31
	movq	%r15, %rbx
	movq	%r12, %r13
	movq	%r14, %r15
	movq	-200(%rbp), %r12
	testq	%rbx, %rbx
	je	.L42
	movq	-184(%rbp), %rcx
	leaq	64(%rcx), %rax
	cmpq	%rax, -176(%rbp)
	setge	%dl
	cmpq	-240(%rbp), %rcx
	setge	%al
	orb	%al, %dl
	je	.L40
	leaq	-1(%rbx), %rax
	cmpq	$14, %rax
	jbe	.L40
	leaq	0(%r13,%rbx,4), %rdx
	movq	%r13, %rax
	.p2align 4,,10
	.p2align 3
.L41:
	movdqu	(%rax), %xmm0
	movdqu	16(%rax), %xmm9
	addq	$64, %rax
	addq	$64, %r12
	movdqu	-32(%rax), %xmm1
	movdqu	-16(%rax), %xmm8
	movdqa	%xmm0, %xmm2
	movdqa	%xmm0, %xmm3
	movdqa	%xmm9, %xmm10
	punpcklwd	%xmm9, %xmm2
	punpckhwd	%xmm9, %xmm3
	movdqa	%xmm8, %xmm11
	psrld	$16, %xmm10
	psrld	$16, %xmm11
	movdqa	%xmm2, %xmm6
	punpckhwd	%xmm3, %xmm6
	psrld	$24, %xmm9
	punpcklwd	%xmm3, %xmm2
	movdqa	%xmm1, %xmm3
	punpcklwd	%xmm6, %xmm2
	movdqa	%xmm1, %xmm6
	punpcklwd	%xmm8, %xmm3
	punpckhwd	%xmm8, %xmm6
	movdqa	%xmm3, %xmm7
	psrld	$24, %xmm8
	punpcklwd	%xmm6, %xmm3
	punpckhwd	%xmm6, %xmm7
	movdqa	%xmm5, %xmm6
	punpcklwd	%xmm7, %xmm3
	movdqa	%xmm5, %xmm7
	pand	%xmm2, %xmm6
	pand	%xmm3, %xmm7
	psrlw	$8, %xmm2
	packuswb	%xmm7, %xmm6
	psrlw	$8, %xmm3
	movdqa	%xmm0, %xmm7
	pand	%xmm5, %xmm3
	psrld	$16, %xmm7
	pand	%xmm5, %xmm2
	packuswb	%xmm3, %xmm2
	psrld	$24, %xmm0
	movdqa	%xmm7, %xmm3
	punpcklwd	%xmm10, %xmm7
	punpckhwd	%xmm10, %xmm3
	movdqa	%xmm7, %xmm10
	punpcklwd	%xmm3, %xmm7
	punpckhwd	%xmm3, %xmm10
	movdqa	%xmm1, %xmm3
	psrld	$16, %xmm3
	psrld	$24, %xmm1
	punpcklwd	%xmm10, %xmm7
	movdqa	%xmm3, %xmm10
	punpcklwd	%xmm11, %xmm3
	pand	%xmm5, %xmm7
	punpckhwd	%xmm11, %xmm10
	movdqa	%xmm3, %xmm11
	punpckhwd	%xmm10, %xmm11
	punpcklwd	%xmm10, %xmm3
	punpcklwd	%xmm11, %xmm3
	pand	%xmm5, %xmm3
	packuswb	%xmm3, %xmm7
	movdqa	%xmm0, %xmm3
	punpcklwd	%xmm9, %xmm0
	punpckhwd	%xmm9, %xmm3
	movdqa	%xmm0, %xmm9
	punpckhwd	%xmm3, %xmm9
	punpcklwd	%xmm3, %xmm0
	movdqa	%xmm1, %xmm3
	punpcklwd	%xmm8, %xmm1
	punpckhwd	%xmm8, %xmm3
	punpcklwd	%xmm9, %xmm0
	movdqa	%xmm1, %xmm8
	punpcklwd	%xmm3, %xmm1
	pand	%xmm5, %xmm0
	punpckhwd	%xmm3, %xmm8
	movdqa	%xmm2, %xmm3
	punpcklwd	%xmm8, %xmm1
	pand	%xmm5, %xmm1
	packuswb	%xmm1, %xmm0
	movdqa	%xmm6, %xmm1
	punpckhbw	%xmm7, %xmm6
	punpcklbw	%xmm7, %xmm1
	punpcklbw	%xmm0, %xmm3
	punpckhbw	%xmm0, %xmm2
	movdqa	%xmm1, %xmm0
	punpckhbw	%xmm3, %xmm1
	punpcklbw	%xmm3, %xmm0
	movups	%xmm1, -48(%r12)
	movups	%xmm0, -64(%r12)
	movdqa	%xmm6, %xmm0
	punpckhbw	%xmm2, %xmm6
	punpcklbw	%xmm2, %xmm0
	movups	%xmm6, -16(%r12)
	movups	%xmm0, -32(%r12)
	cmpq	%rax, %rdx
	jne	.L41
.L42:
	addq	$1, -168(%rbp)
	movq	-64(%rbp), %rsi
	movq	-168(%rbp), %rax
	addq	%rsi, -184(%rbp)
	cmpq	%rax, 16(%rbp)
	ja	.L38
	call	EVP_sha256@PLT
	movq	-72(%rbp), %rdx
	movl	-104(%rbp), %esi
	pushq	32(%rbp)
	movq	%rax, %r9
	movq	40(%rbp), %rax
	movl	$1, %r8d
	movl	-176(%rbp), %ecx
	movq	-96(%rbp), %rdi
	pushq	%rax
	call	PKCS5_PBKDF2_HMAC@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	je	.L45
	movl	$1, %r15d
.L24:
	movq	-280(%rbp), %rsi
	movq	-72(%rbp), %rdi
	movl	$263, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L46:
	xorl	%r15d, %r15d
.L13:
	leaq	-40(%rbp), %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	movl	$213, %r8d
.L88:
	leaq	.LC0(%rip), %rcx
	movl	$172, %edx
	movl	$181, %esi
	xorl	%r15d, %r15d
	movl	$6, %edi
	call	ERR_put_error@PLT
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L89:
	movl	$185, %r8d
.L87:
	movl	$172, %edx
	movl	$181, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L45:
	movl	$261, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$181, %edx
	xorl	%r15d, %r15d
	movl	$181, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L51:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L33:
	movl	(%r12,%rax,4), %edx
	xorl	(%rdi,%rax,4), %edx
	movl	%edx, 0(%r13,%rax,4)
	addq	$1, %rax
	cmpq	%rax, %r15
	jne	.L33
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L40:
	movq	-208(%rbp), %rax
	leaq	(%r12,%rbx,4), %rsi
	movq	%r13, %rdx
	.p2align 4,,10
	.p2align 3
.L43:
	movl	(%rdx), %ecx
	addq	$4, %rax
	addq	$4, %rdx
	movb	%cl, -4(%rax)
	movl	%ecx, %edi
	movb	%ch, -3(%rax)
	shrl	$16, %edi
	shrl	$24, %ecx
	movb	%dil, -2(%rax)
	movb	%cl, -1(%rax)
	cmpq	%rsi, %rax
	jne	.L43
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L50:
	movq	-88(%rbp), %rsi
	movq	-192(%rbp), %rdi
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L26:
	movzbl	(%r12,%rax,4), %ecx
	movl	%ecx, (%rsi,%rax,4)
	movzbl	1(%r12,%rax,4), %edx
	sall	$8, %edx
	orl	%edx, %ecx
	movl	%ecx, (%rsi,%rax,4)
	movzbl	2(%r12,%rax,4), %edx
	sall	$16, %edx
	orl	%ecx, %edx
	movl	%edx, (%rsi,%rax,4)
	movzbl	3(%r12,%rax,4), %ecx
	sall	$24, %ecx
	orl	%ecx, %edx
	movl	%edx, (%rsi,%rax,4)
	addq	$1, %rax
	cmpq	%rax, %rdi
	jne	.L26
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L49:
	movq	-88(%rbp), %r14
	xorl	%ebx, %ebx
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L91:
	movl	$242, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r15d, %r15d
	movl	$181, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	jmp	.L13
	.cfi_endproc
.LFE385:
	.size	EVP_PBE_scrypt, .-EVP_PBE_scrypt
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
