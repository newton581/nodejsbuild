	.file	"ui_openssl.c"
	.text
	.p2align 4
	.type	recsig, @function
recsig:
.LFB824:
	.cfi_startproc
	endbr64
	movl	%edi, intr_signal(%rip)
	ret
	.cfi_endproc
.LFE824:
	.size	recsig, .-recsig
	.p2align 4
	.type	close_console, @function
close_console:
.LFB821:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	tty_in(%rip), %rdi
	cmpq	stdin(%rip), %rdi
	je	.L4
	call	fclose@PLT
.L4:
	movq	tty_out(%rip), %rdi
	cmpq	stderr(%rip), %rdi
	je	.L5
	call	fclose@PLT
.L5:
	movq	40(%rbx), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE821:
	.size	close_console, .-close_console
	.p2align 4
	.type	write_string, @function
write_string:
.LFB814:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	call	UI_get_string_type@PLT
	subl	$4, %eax
	cmpl	$1, %eax
	jbe	.L10
	popq	%r12
	movl	$1, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movq	tty_out(%rip), %r13
	movq	%r12, %rdi
	call	UI_get0_output_string@PLT
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	fputs@PLT
	movq	tty_out(%rip), %rdi
	call	fflush@PLT
	popq	%r12
	movl	$1, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE814:
	.size	write_string, .-write_string
	.p2align 4
	.type	read_string_inner, @function
read_string_inner:
.LFB817:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$216, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	152+savsig(%rip), %rbx
	movl	$1, %r12d
	movq	%rbx, %r14
	movl	%ecx, -8440(%rbp)
	movl	%edx, %r13d
	movl	$18, %ecx
	leaq	-8408(%rbp), %rdx
	movq	%rdi, -8424(%rbp)
	movq	%rdx, %rdi
	leaq	-8416(%rbp), %r15
	movq	%rsi, -8432(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, intr_signal(%rip)
	rep stosq
	leaq	recsig(%rip), %rax
	movq	%rax, -8416(%rbp)
	.p2align 4,,10
	.p2align 3
.L13:
	leal	-9(%r12), %eax
	cmpl	$1, %eax
	jbe	.L12
	cmpl	$12, %r12d
	je	.L12
	movq	%r14, %rdx
	movq	%r15, %rsi
	movl	%r12d, %edi
	call	sigaction@PLT
.L12:
	addl	$1, %r12d
	addq	$152, %r14
	cmpl	$32, %r12d
	jne	.L13
	xorl	%esi, %esi
	movl	$28, %edi
	call	signal@PLT
	movq	tty_in(%rip), %r12
	movl	$1, ps.18968(%rip)
	testl	%r13d, %r13d
	jne	.L14
	movq	48+tty_orig(%rip), %rax
	movdqa	tty_orig(%rip), %xmm0
	movdqa	16+tty_orig(%rip), %xmm1
	movdqa	32+tty_orig(%rip), %xmm2
	movq	%rax, 48+tty_new(%rip)
	movl	is_a_tty(%rip), %ecx
	movl	56+tty_orig(%rip), %eax
	movaps	%xmm0, tty_new(%rip)
	movaps	%xmm1, 16+tty_new(%rip)
	andl	$-9, 12+tty_new(%rip)
	movl	%eax, 56+tty_new(%rip)
	movaps	%xmm2, 32+tty_new(%rip)
	testl	%ecx, %ecx
	jne	.L52
.L14:
	leaq	-8256(%rbp), %r14
	movq	%r12, %rdx
	movl	$8191, %esi
	movl	$2, ps.18968(%rip)
	movq	%r14, %rdi
	movb	$0, -8256(%rbp)
	call	fgets@PLT
	testq	%rax, %rax
	je	.L18
	movq	tty_in(%rip), %r15
	movq	%r15, %rdi
	call	feof@PLT
	testl	%eax, %eax
	je	.L53
.L18:
	xorl	%r12d, %r12d
.L17:
	movl	intr_signal(%rip), %eax
	cmpl	$2, %eax
	movl	$-1, %eax
	cmove	%eax, %r12d
	testl	%r13d, %r13d
	je	.L15
	movl	ps.18968(%rip), %eax
	cmpl	$1, %eax
	jle	.L26
.L27:
	movl	$1, %r13d
	.p2align 4,,10
	.p2align 3
.L30:
	leal	-10(%r13), %eax
	andl	$-3, %eax
	je	.L29
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movl	%r13d, %edi
	call	sigaction@PLT
.L29:
	addl	$1, %r13d
	addq	$152, %rbx
	cmpl	$32, %r13d
	jne	.L30
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L52:
	movq	%r12, %rdi
	call	fileno@PLT
	xorl	%esi, %esi
	leaq	tty_new(%rip), %rdx
	movl	%eax, %edi
	call	tcsetattr@PLT
	cmpl	$-1, %eax
	jne	.L14
	movl	intr_signal(%rip), %eax
	xorl	%r12d, %r12d
	leaq	-8256(%rbp), %r14
	cmpl	$2, %eax
	sete	%r12b
	negl	%r12d
	.p2align 4,,10
	.p2align 3
.L15:
	movq	tty_out(%rip), %rsi
	movl	$10, %edi
	call	fputc@PLT
	movl	ps.18968(%rip), %eax
	cmpl	$1, %eax
	jg	.L25
.L26:
	cmpl	$1, %eax
	je	.L27
.L28:
	movl	$8192, %esi
	movq	%r14, %rdi
	call	OPENSSL_cleanse@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L54
	addq	$8408, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movq	%r15, %rdi
	call	ferror@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L18
	movl	$10, %esi
	movq	%r14, %rdi
	call	strchr@PLT
	testq	%rax, %rax
	je	.L55
	movl	-8440(%rbp), %edx
	testl	%edx, %edx
	je	.L20
	movb	$0, (%rax)
.L20:
	movq	-8432(%rbp), %rsi
	movq	-8424(%rbp), %rdi
	movq	%r14, %rdx
	call	UI_set_result@PLT
	notl	%eax
	movl	%eax, %r12d
	shrl	$31, %r12d
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L25:
	movq	48+tty_orig(%rip), %rax
	movdqa	tty_orig(%rip), %xmm3
	movdqa	16+tty_orig(%rip), %xmm4
	movdqa	32+tty_orig(%rip), %xmm5
	movq	%rax, 48+tty_new(%rip)
	movl	56+tty_orig(%rip), %eax
	movaps	%xmm3, tty_new(%rip)
	movl	%eax, 56+tty_new(%rip)
	movl	is_a_tty(%rip), %eax
	movaps	%xmm4, 16+tty_new(%rip)
	movaps	%xmm5, 32+tty_new(%rip)
	testl	%eax, %eax
	je	.L27
	movq	tty_in(%rip), %rdi
	call	fileno@PLT
	xorl	%esi, %esi
	leaq	tty_new(%rip), %rdx
	movl	%eax, %edi
	call	tcsetattr@PLT
	cmpl	$-1, %eax
	movl	$0, %eax
	cmove	%eax, %r12d
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L55:
	leaq	-8261(%rbp), %rax
	movq	%rax, -8440(%rbp)
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L56:
	movq	-8440(%rbp), %rdi
	movl	$10, %esi
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L20
.L19:
	movq	-8440(%rbp), %rdi
	movq	%r15, %rdx
	movl	$4, %esi
	call	fgets@PLT
	testq	%rax, %rax
	jne	.L56
	jmp	.L17
.L54:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE817:
	.size	read_string_inner, .-read_string_inner
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Verifying - %s"
.LC1:
	.string	"Verify failure\n"
	.text
	.p2align 4
	.type	read_string, @function
read_string:
.LFB815:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$8, %rsp
	call	UI_get_string_type@PLT
	cmpl	$2, %eax
	je	.L58
	cmpl	$3, %eax
	je	.L59
	cmpl	$1, %eax
	je	.L60
	movl	$1, %eax
.L57:
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	movq	%r12, %rdi
	call	UI_get0_output_string@PLT
	movq	tty_out(%rip), %rdi
	movl	$1, %esi
	leaq	.LC0(%rip), %rdx
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	__fprintf_chk@PLT
	movq	tty_out(%rip), %rdi
	call	fflush@PLT
	movq	%r12, %rdi
	call	UI_get_input_flags@PLT
	movl	$1, %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	andl	$1, %eax
	movl	%eax, %edx
	call	read_string_inner
	testl	%eax, %eax
	jle	.L57
	movq	%r12, %rdi
	call	UI_get0_test_string@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	UI_get0_result_string@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	strcmp@PLT
	movl	%eax, %r8d
	movl	$1, %eax
	testl	%r8d, %r8d
	je	.L57
	movq	tty_out(%rip), %rcx
	movl	$15, %edx
	movl	$1, %esi
	leaq	.LC1(%rip), %rdi
	call	fwrite@PLT
	movq	tty_out(%rip), %rdi
	call	fflush@PLT
	xorl	%eax, %eax
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L60:
	movq	tty_out(%rip), %r14
	movq	%r12, %rdi
	call	UI_get0_output_string@PLT
	movq	%rax, %rdi
	movq	%r14, %rsi
	call	fputs@PLT
	movq	tty_out(%rip), %rdi
	call	fflush@PLT
	movq	%r12, %rdi
	call	UI_get_input_flags@PLT
	movl	$1, %ecx
	andl	$1, %eax
	movl	%eax, %edx
.L65:
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	read_string_inner
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	movq	tty_out(%rip), %r14
	movq	%r12, %rdi
	call	UI_get0_output_string@PLT
	movq	%rax, %rdi
	movq	%r14, %rsi
	call	fputs@PLT
	movq	tty_out(%rip), %r14
	movq	%r12, %rdi
	call	UI_get0_action_string@PLT
	movq	%rax, %rdi
	movq	%r14, %rsi
	call	fputs@PLT
	movq	tty_out(%rip), %rdi
	call	fflush@PLT
	movq	%r12, %rdi
	call	UI_get_input_flags@PLT
	xorl	%ecx, %ecx
	andl	$1, %eax
	movl	%eax, %edx
	jmp	.L65
	.cfi_endproc
.LFE815:
	.size	read_string, .-read_string
	.section	.rodata.str1.1
.LC2:
	.string	"r"
.LC3:
	.string	"/dev/tty"
.LC4:
	.string	"w"
.LC5:
	.string	"%d"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"../deps/openssl/openssl/crypto/ui/ui_openssl.c"
	.section	.rodata.str1.1
.LC7:
	.string	"errno="
	.text
	.p2align 4
	.type	open_console, @function
open_console:
.LFB818:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 12, -24
	movq	40(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	CRYPTO_THREAD_write_lock@PLT
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$1, is_a_tty(%rip)
	call	fopen@PLT
	movq	%rax, tty_in(%rip)
	testq	%rax, %rax
	je	.L77
.L67:
	leaq	.LC4(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	fopen@PLT
	movq	%rax, tty_out(%rip)
	testq	%rax, %rax
	je	.L78
.L68:
	movq	tty_in(%rip), %rdi
	movl	$1, %r12d
	call	fileno@PLT
	leaq	tty_orig(%rip), %rsi
	movl	%eax, %edi
	call	tcgetattr@PLT
	cmpl	$-1, %eax
	je	.L79
.L66:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L80
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	call	__errno_location@PLT
	movl	(%rax), %ecx
	cmpl	$25, %ecx
	ja	.L70
	movl	$38273120, %eax
	btq	%rcx, %rax
	jnc	.L70
	movl	$0, is_a_tty(%rip)
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L78:
	movq	stderr(%rip), %rax
	movq	%rax, tty_out(%rip)
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L77:
	movq	stdin(%rip), %rax
	movq	%rax, tty_in(%rip)
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L70:
	leaq	-34(%rbp), %r12
	leaq	.LC5(%rip), %rdx
	movl	$9, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	BIO_snprintf@PLT
	movl	$454, %r8d
	movl	$108, %edx
	leaq	.LC6(%rip), %rcx
	movl	$114, %esi
	movl	$40, %edi
	call	ERR_put_error@PLT
	movq	%r12, %rdx
	movl	$2, %edi
	xorl	%eax, %eax
	leaq	.LC7(%rip), %rsi
	xorl	%r12d, %r12d
	call	ERR_add_error_data@PLT
	jmp	.L66
.L80:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE818:
	.size	open_console, .-open_console
	.p2align 4
	.globl	UI_OpenSSL
	.type	UI_OpenSSL, @function
UI_OpenSSL:
.LFB825:
	.cfi_startproc
	endbr64
	leaq	ui_openssl(%rip), %rax
	ret
	.cfi_endproc
.LFE825:
	.size	UI_OpenSSL, .-UI_OpenSSL
	.p2align 4
	.globl	UI_set_default_method
	.type	UI_set_default_method, @function
UI_set_default_method:
.LFB826:
	.cfi_startproc
	endbr64
	movq	%rdi, default_UI_meth(%rip)
	ret
	.cfi_endproc
.LFE826:
	.size	UI_set_default_method, .-UI_set_default_method
	.p2align 4
	.globl	UI_get_default_method
	.type	UI_get_default_method, @function
UI_get_default_method:
.LFB827:
	.cfi_startproc
	endbr64
	movq	default_UI_meth(%rip), %rax
	ret
	.cfi_endproc
.LFE827:
	.size	UI_get_default_method, .-UI_get_default_method
	.local	ps.18968
	.comm	ps.18968,4,4
	.section	.data.rel.local,"aw"
	.align 8
	.type	default_UI_meth, @object
	.size	default_UI_meth, 8
default_UI_meth:
	.quad	ui_openssl
	.section	.rodata.str1.8
	.align 8
.LC8:
	.string	"OpenSSL default user interface"
	.section	.data.rel.local
	.align 32
	.type	ui_openssl, @object
	.size	ui_openssl, 80
ui_openssl:
	.quad	.LC8
	.quad	open_console
	.quad	write_string
	.quad	0
	.quad	read_string
	.quad	close_console
	.quad	0
	.zero	24
	.local	intr_signal
	.comm	intr_signal,4,4
	.local	is_a_tty
	.comm	is_a_tty,4,4
	.local	tty_out
	.comm	tty_out,8,8
	.local	tty_in
	.comm	tty_in,8,8
	.local	tty_new
	.comm	tty_new,60,32
	.local	tty_orig
	.comm	tty_orig,60,32
	.local	savsig
	.comm	savsig,4864,32
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
