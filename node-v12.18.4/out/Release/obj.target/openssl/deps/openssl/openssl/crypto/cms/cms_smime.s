	.file	"cms_smime.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/cms/cms_smime.c"
	.text
	.p2align 4
	.type	cms_copy_content, @function
cms_copy_content:
.LFB1465:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -4164(%rbp)
	movq	%rdi, %r14
	movq	%rsi, %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L25
	movq	%rdi, %r12
	testb	$1, -4164(%rbp)
	jne	.L26
.L4:
	leaq	-4160(%rbp), %rbx
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L6:
	movl	%eax, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	cmpl	%r15d, %eax
	jne	.L27
.L10:
	movl	$4096, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	BIO_read@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	jg	.L6
	movq	%r13, %rdi
	call	BIO_method_type@PLT
	cmpl	$522, %eax
	je	.L7
.L9:
	xorl	%r13d, %r13d
	testl	%r15d, %r15d
	jne	.L5
	movl	$1, %r13d
	testb	$1, -4164(%rbp)
	jne	.L28
	.p2align 4,,10
	.p2align 3
.L5:
	cmpq	%r12, %r14
	je	.L1
	movq	%r12, %rdi
	call	BIO_free@PLT
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L29
	addq	$4136, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	xorl	%r13d, %r13d
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L26:
	call	BIO_s_mem@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$130, %esi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	BIO_ctrl@PLT
.L3:
	testq	%r12, %r12
	jne	.L4
	movl	$41, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$107, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L28:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	SMIME_text@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L30
	movl	$1, %r13d
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L25:
	call	BIO_s_null@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, %r12
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%r13, %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$113, %esi
	call	BIO_ctrl@PLT
	xorl	%r13d, %r13d
	testq	%rax, %rax
	jne	.L9
	jmp	.L5
.L30:
	movl	$64, %r8d
	movl	$140, %edx
	movl	$107, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L5
.L29:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1465:
	.size	cms_copy_content, .-cms_copy_content
	.p2align 4
	.globl	CMS_data
	.type	CMS_data, @function
CMS_data:
.LFB1468:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	call	CMS_get0_type@PLT
	movq	%rax, %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$21, %eax
	je	.L32
	movl	$107, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$143, %edx
	xorl	%r12d, %r12d
	movl	$109, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
.L31:
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%esi, %esi
	xorl	%r12d, %r12d
	call	CMS_dataInit@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L31
	movl	%r14d, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	cms_copy_content
	movq	%r15, %rdi
	movl	%eax, %r12d
	call	BIO_free_all@PLT
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1468:
	.size	CMS_data, .-CMS_data
	.p2align 4
	.globl	CMS_data_create
	.type	CMS_data_create, @function
CMS_data_create:
.LFB1469:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	call	cms_Data_create@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L37
	testl	$4096, %r13d
	je	.L46
.L37:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	CMS_dataInit@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L47
	movl	%r13d, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	SMIME_crlf_copy@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%r15, %rdi
	call	BIO_ctrl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	CMS_dataFinal@PLT
	testl	%eax, %eax
	je	.L48
	movq	%r15, %rdi
	call	BIO_free_all@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	movl	$768, %r8d
	movl	$104, %edx
	movl	$127, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L40:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	CMS_ContentInfo_free@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	movl	$777, %r8d
	movl	$103, %edx
	movl	$127, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r15, %rdi
	call	BIO_free_all@PLT
	jmp	.L40
	.cfi_endproc
.LFE1469:
	.size	CMS_data_create, .-CMS_data_create
	.p2align 4
	.globl	CMS_digest_verify
	.type	CMS_digest_verify, @function
CMS_digest_verify:
.LFB1470:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	call	CMS_get0_type@PLT
	movq	%rax, %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$25, %eax
	jne	.L71
	testq	%rbx, %rbx
	je	.L72
.L52:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	xorl	%r14d, %r14d
	call	CMS_dataInit@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L49
	movq	-56(%rbp), %rdi
	movl	%r15d, %edx
	movq	%rax, %rsi
	call	cms_copy_content
	movl	%eax, %r14d
	testl	%eax, %eax
	jne	.L73
.L54:
	testq	%rbx, %rbx
	je	.L55
	.p2align 4,,10
	.p2align 3
.L56:
	movq	%r12, %rdi
	movq	%r12, %r13
	call	BIO_pop@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	BIO_free@PLT
	testq	%r12, %r12
	je	.L49
	cmpq	%r12, %rbx
	jne	.L56
.L49:
	addq	$24, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore_state
	movq	%r13, %rdi
	call	CMS_get0_content@PLT
	testq	%rax, %rax
	je	.L53
	cmpq	$0, (%rax)
	jne	.L52
.L53:
	movl	$82, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$127, %edx
	xorl	%r14d, %r14d
	movl	$99, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L71:
	movl	$139, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$144, %edx
	xorl	%r14d, %r14d
	movl	$118, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L73:
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	cms_DigestedData_do_final@PLT
	movl	%eax, %r14d
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L55:
	movq	%r12, %rdi
	call	BIO_free_all@PLT
	jmp	.L49
	.cfi_endproc
.LFE1470:
	.size	CMS_digest_verify, .-CMS_digest_verify
	.p2align 4
	.globl	CMS_digest_create
	.type	CMS_digest_create, @function
CMS_digest_create:
.LFB1471:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	testq	%rsi, %rsi
	je	.L86
.L75:
	call	cms_DigestedData_create@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L74
	testb	$64, %r14b
	je	.L87
.L77:
	testl	$4096, %r14d
	je	.L88
.L74:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	CMS_dataInit@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L89
	movl	%r14d, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	SMIME_crlf_copy@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%r15, %rdi
	call	BIO_ctrl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	CMS_dataFinal@PLT
	testl	%eax, %eax
	je	.L90
	movq	%r15, %rdi
	call	BIO_free_all@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	CMS_set_detached@PLT
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L86:
	call	EVP_sha1@PLT
	movq	%rax, %rdi
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L89:
	movl	$768, %r8d
	movl	$104, %edx
	movl	$127, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L79:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	CMS_ContentInfo_free@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	movl	$777, %r8d
	movl	$103, %edx
	movl	$127, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r15, %rdi
	call	BIO_free_all@PLT
	jmp	.L79
	.cfi_endproc
.LFE1471:
	.size	CMS_digest_create, .-CMS_digest_create
	.p2align 4
	.globl	CMS_EncryptedData_decrypt
	.type	CMS_EncryptedData_decrypt, @function
CMS_EncryptedData_decrypt:
.LFB1472:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r9d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	movq	%r8, -56(%rbp)
	call	CMS_get0_type@PLT
	movq	%rax, %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$26, %eax
	jne	.L112
	testq	%rbx, %rbx
	je	.L113
.L94:
	xorl	%esi, %esi
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	CMS_EncryptedData_set1_key@PLT
	testl	%eax, %eax
	jle	.L97
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	CMS_dataInit@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L97
	movq	-56(%rbp), %rdi
	movl	%r14d, %edx
	movq	%rax, %rsi
	call	cms_copy_content
	movl	%eax, %r14d
	testq	%rbx, %rbx
	je	.L98
	.p2align 4,,10
	.p2align 3
.L99:
	movq	%r12, %rdi
	movq	%r12, %r13
	call	BIO_pop@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	BIO_free@PLT
	testq	%r12, %r12
	je	.L91
	cmpq	%r12, %rbx
	jne	.L99
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L97:
	xorl	%r14d, %r14d
.L91:
	addq	$24, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore_state
	movq	%r13, %rdi
	call	CMS_get0_content@PLT
	testq	%rax, %rax
	je	.L95
	cmpq	$0, (%rax)
	jne	.L94
.L95:
	movl	$82, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$127, %edx
	xorl	%r14d, %r14d
	movl	$99, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L112:
	movl	$183, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$145, %edx
	xorl	%r14d, %r14d
	movl	$121, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L98:
	movq	%r12, %rdi
	call	BIO_free_all@PLT
	jmp	.L91
	.cfi_endproc
.LFE1472:
	.size	CMS_EncryptedData_decrypt, .-CMS_EncryptedData_decrypt
	.p2align 4
	.globl	CMS_EncryptedData_encrypt
	.type	CMS_EncryptedData_encrypt, @function
CMS_EncryptedData_encrypt:
.LFB1473:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rsi, %rsi
	je	.L128
	movq	%rcx, -56(%rbp)
	movq	%rdi, %r15
	movq	%rsi, %r13
	movq	%rdx, %rbx
	movl	%r8d, %r14d
	call	CMS_ContentInfo_new@PLT
	movq	-56(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L127
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	CMS_EncryptedData_set1_key@PLT
	testl	%eax, %eax
	je	.L127
	testb	$64, %r14b
	je	.L129
	testl	$20480, %r14d
	je	.L130
.L114:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	CMS_dataInit@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L131
	movl	%r14d, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	SMIME_crlf_copy@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%r13, %rdi
	call	BIO_ctrl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	CMS_dataFinal@PLT
	testl	%eax, %eax
	je	.L132
	movq	%r13, %rdi
	call	BIO_free_all@PLT
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L129:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	CMS_set_detached@PLT
	testl	$20480, %r14d
	jne	.L114
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L131:
	movl	$768, %r8d
	movl	$104, %edx
	movl	$127, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L121:
	movq	%r12, %rdi
	call	CMS_ContentInfo_free@PLT
.L127:
	xorl	%r12d, %r12d
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L128:
	movl	$207, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$126, %edx
	xorl	%r12d, %r12d
	movl	$122, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L132:
	movl	$777, %r8d
	movl	$103, %edx
	movl	$127, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r13, %rdi
	call	BIO_free_all@PLT
	jmp	.L121
	.cfi_endproc
.LFE1473:
	.size	CMS_EncryptedData_encrypt, .-CMS_EncryptedData_encrypt
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"smime_sign"
.LC2:
	.string	"Verify error:"
	.text
	.p2align 4
	.globl	CMS_verify
	.type	CMS_verify, @function
CMS_verify:
.LFB1475:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r9d, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -96(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%rcx, -120(%rbp)
	movq	%r8, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L255
	testb	$-128, %r9b
	je	.L256
.L136:
	movq	-96(%rbp), %rdi
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	leaq	-72(%rbp), %r13
	call	CMS_get0_SignerInfos@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jg	.L139
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L142:
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%r13, %rdx
	call	CMS_SignerInfo_get0_algs@PLT
	cmpq	$1, -72(%rbp)
	sbbl	$-1, %r12d
	addl	$1, %r14d
.L139:
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r14d
	jl	.L142
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	movl	%eax, %r13d
	cmpl	%eax, %r12d
	jne	.L258
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jne	.L259
.L144:
	movq	$0, -112(%rbp)
	movq	$0, -88(%rbp)
	testb	$32, %r15b
	je	.L260
.L145:
	testb	$8, %r15b
	jne	.L159
	xorl	%r12d, %r12d
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L158:
	movq	%rbx, %rdi
	movl	%r12d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	CMS_signed_get_attr_count@PLT
	testl	%eax, %eax
	js	.L157
	movq	%r13, %rdi
	call	CMS_SignerInfo_verify@PLT
	testl	%eax, %eax
	jle	.L254
.L157:
	addl	$1, %r12d
.L154:
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jl	.L158
.L159:
	cmpq	$0, -120(%rbp)
	je	.L261
	movq	-120(%rbp), %r14
	movq	%r14, %rdi
	call	BIO_method_type@PLT
	cmpl	$1025, %eax
	je	.L262
.L160:
	movl	%r15d, %eax
	andl	$128, %eax
	movl	%eax, -124(%rbp)
	jne	.L263
	cmpq	$0, -136(%rbp)
	je	.L264
	testb	$1, %r15b
	jne	.L265
	movq	-136(%rbp), %rsi
	movq	-96(%rbp), %rdi
	call	CMS_dataInit@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L266
	movq	-120(%rbp), %rdi
	movl	%r15d, %edx
	movq	%r13, %rsi
	andl	$-2, %edx
	call	SMIME_crlf_copy@PLT
	movq	-136(%rbp), %rax
	movq	%rax, -104(%rbp)
.L169:
	andl	$4, %r15d
	movl	%r15d, -124(%rbp)
	je	.L188
	movq	-104(%rbp), %rbx
	movl	$1, %r12d
	.p2align 4,,10
	.p2align 3
.L176:
	movq	%r13, %rdi
	movq	%r13, %r15
	call	BIO_pop@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	BIO_free@PLT
	testq	%r13, %r13
	je	.L178
	cmpq	%rbx, %r13
	jne	.L176
.L178:
	cmpq	%r14, -120(%rbp)
	je	.L179
	movq	%r14, %rdi
	call	BIO_free@PLT
	.p2align 4,,10
	.p2align 3
.L179:
	movq	-104(%rbp), %rax
	cmpq	%rax, -136(%rbp)
	je	.L162
	movq	%rax, %rdi
	call	BIO_free_all@PLT
.L162:
	movq	X509_free@GOTPCREL(%rip), %rsi
	movq	-88(%rbp), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	X509_CRL_free@GOTPCREL(%rip), %rsi
	movq	-112(%rbp), %rdi
	call	OPENSSL_sk_pop_free@PLT
.L133:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L267
	addq	$104, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L258:
	.cfi_restore_state
	movq	-96(%rbp), %rdi
	movq	-88(%rbp), %rsi
	movl	%r15d, %edx
	call	CMS_set1_signers_certs@PLT
	movq	%rbx, %rdi
	leal	(%r12,%rax), %r13d
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	je	.L144
.L259:
	movl	$138, %edx
	movl	$157, %esi
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	movl	$304, %r8d
	movl	$46, %edi
	xorl	%r12d, %r12d
	andl	$128, %r15d
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	%r15d, -124(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L140:
	movl	-124(%rbp), %eax
	testl	%eax, %eax
	jne	.L170
	cmpq	$0, -120(%rbp)
	je	.L175
	movq	-104(%rbp), %rbx
	testq	%rbx, %rbx
	jne	.L176
.L167:
	movq	%r13, %rdi
	call	BIO_free_all@PLT
	movq	$0, -104(%rbp)
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L256:
	call	CMS_get0_eContentType@PLT
	movq	%rax, %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %r8d
	movl	%r15d, %eax
	orl	$524288, %eax
	cmpl	$787, %r8d
	cmove	%eax, %r15d
	jmp	.L136
.L276:
	movq	$0, -104(%rbp)
	.p2align 4,,10
	.p2align 3
.L170:
	movq	-120(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L175
	cmpq	%r14, %rbx
	jne	.L175
	.p2align 4,,10
	.p2align 3
.L180:
	movq	%r13, %rdi
	movq	%r13, %r14
	call	BIO_pop@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	BIO_free@PLT
	testq	%r13, %r13
	je	.L179
	cmpq	%r13, %rbx
	jne	.L180
	jmp	.L179
.L271:
	movq	-96(%rbp), %rdi
	xorl	%esi, %esi
	xorl	%r14d, %r14d
	call	CMS_dataInit@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L189
.L205:
	movq	$0, -104(%rbp)
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L175:
	movq	%r13, %rdi
	call	BIO_free_all@PLT
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L150:
	call	X509_verify_cert@PLT
	testl	%eax, %eax
	jg	.L152
.L253:
	movq	%r15, %r13
	movl	-124(%rbp), %r15d
	movq	%r13, %rdi
	call	X509_STORE_CTX_get_error@PLT
	movl	$252, %r8d
	movl	$100, %edx
	leaq	.LC0(%rip), %rcx
	movl	%eax, %ebx
	movl	$153, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	movslq	%ebx, %rdi
	call	X509_verify_cert_error_string@PLT
	movl	$2, %edi
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	ERR_add_error_data@PLT
	movq	%r13, %rdi
	call	X509_STORE_CTX_free@PLT
.L254:
	andl	$128, %r15d
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	movl	%r15d, -124(%rbp)
	movq	$0, -104(%rbp)
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L260:
	movq	-96(%rbp), %rdi
	call	CMS_get1_certs@PLT
	movq	%rax, -88(%rbp)
	testl	$8192, %r15d
	je	.L268
.L146:
	movl	%r15d, -124(%rbp)
	xorl	%r14d, %r14d
	leaq	-64(%rbp), %r13
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L149:
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	X509_STORE_CTX_set_default@PLT
	movq	-112(%rbp), %rsi
	movq	%r15, %rdi
	testq	%rsi, %rsi
	je	.L150
	call	X509_STORE_CTX_set0_crls@PLT
	movq	%r15, %rdi
	call	X509_verify_cert@PLT
	testl	%eax, %eax
	jle	.L253
.L152:
	movq	%r15, %rdi
	addl	$1, %r14d
	call	X509_STORE_CTX_free@PLT
.L147:
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r14d
	jge	.L269
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r12
	call	X509_STORE_CTX_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L270
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	xorl	%r8d, %r8d
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	CMS_SignerInfo_get0_algs@PLT
	movq	-88(%rbp), %rcx
	movq	-64(%rbp), %rdx
	movq	%r15, %rdi
	movq	-104(%rbp), %rsi
	call	X509_STORE_CTX_init@PLT
	testl	%eax, %eax
	jne	.L149
	movq	%r15, %r13
	movl	-124(%rbp), %r15d
	movl	$242, %r8d
	movl	%eax, %r12d
	leaq	.LC0(%rip), %rcx
	movl	$141, %edx
	movl	$153, %esi
	xorl	%r14d, %r14d
	movl	$46, %edi
	andl	$128, %r15d
	call	ERR_put_error@PLT
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	X509_STORE_CTX_free@PLT
	movl	%r15d, -124(%rbp)
	movq	$0, -104(%rbp)
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L261:
	movl	%r15d, %eax
	andl	$128, %eax
	movl	%eax, -124(%rbp)
	jne	.L271
	movq	-96(%rbp), %rdi
	xorl	%esi, %esi
	call	CMS_dataInit@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L205
	movq	-136(%rbp), %rdi
	movl	%r15d, %edx
	movq	%rax, %rsi
	call	cms_copy_content
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L272
	andl	$4, %r15d
	movl	%r15d, -124(%rbp)
	jne	.L273
	movq	$0, -104(%rbp)
	xorl	%r14d, %r14d
.L188:
	xorl	%r12d, %r12d
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L174:
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	CMS_SignerInfo_verify_content@PLT
	testl	%eax, %eax
	jle	.L274
	addl	$1, %r12d
.L172:
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jl	.L174
	movl	$1, %r12d
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L270:
	movl	-124(%rbp), %r15d
	movl	$237, %r8d
	movq	%rax, %r13
	xorl	%r14d, %r14d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$153, %esi
	xorl	%r12d, %r12d
	movl	$46, %edi
	andl	$128, %r15d
	call	ERR_put_error@PLT
	xorl	%edi, %edi
	call	X509_STORE_CTX_free@PLT
	movl	%r15d, -124(%rbp)
	movq	$0, -104(%rbp)
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L255:
	call	CMS_get0_content@PLT
	testq	%rax, %rax
	je	.L135
	cmpq	$0, (%rax)
	jne	.L136
.L135:
	movl	$82, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$127, %edx
	xorl	%r12d, %r12d
	movl	$99, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L257:
	movl	$135, %edx
	movl	$157, %esi
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	movl	$289, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$46, %edi
	andl	$128, %r15d
	call	ERR_put_error@PLT
	movl	%r15d, -124(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -88(%rbp)
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L269:
	movl	-124(%rbp), %r15d
	jmp	.L145
.L263:
	movq	-96(%rbp), %rdi
	movq	%r14, %rsi
	call	CMS_dataInit@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L275
.L189:
	movq	-136(%rbp), %rdi
	movl	%r15d, %edx
	movq	%r13, %rsi
	call	cms_copy_content
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L276
	andl	$4, %r15d
	movq	$0, -104(%rbp)
	movl	$1, %r12d
	je	.L188
	jmp	.L170
.L268:
	movq	-96(%rbp), %rdi
	call	CMS_get1_crls@PLT
	movq	%rax, -112(%rbp)
	jmp	.L146
.L273:
	movq	$0, -104(%rbp)
	movl	$1, %r12d
	jmp	.L175
.L266:
	movq	-136(%rbp), %rax
	movq	%rax, -104(%rbp)
.L168:
	movq	-104(%rbp), %rbx
	xorl	%r12d, %r12d
	jmp	.L176
.L275:
	movq	$0, -104(%rbp)
	xorl	%r12d, %r12d
	jmp	.L170
.L274:
	movl	$393, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$109, %edx
	xorl	%r12d, %r12d
	movl	$157, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L140
.L262:
	movq	-120(%rbp), %rdi
	leaq	-64(%rbp), %rcx
	xorl	%edx, %edx
	movl	$3, %esi
	call	BIO_ctrl@PLT
	movq	-64(%rbp), %rdi
	movq	%rax, %rsi
	call	BIO_new_mem_buf@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L160
	movl	$346, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$157, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L162
.L265:
	call	BIO_s_mem@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$130, %esi
	movq	%rax, %rdi
	movq	%rax, -104(%rbp)
	call	BIO_ctrl@PLT
.L164:
	cmpq	$0, -104(%rbp)
	je	.L277
	movq	-104(%rbp), %r12
	movq	-96(%rbp), %rdi
	movq	%r12, %rsi
	call	CMS_dataInit@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L168
	movl	%r15d, %edx
	movq	-120(%rbp), %rdi
	movq	%rax, %rsi
	andl	$-2, %edx
	call	SMIME_crlf_copy@PLT
	testb	$1, %r15b
	je	.L169
	movq	-136(%rbp), %rsi
	movq	%r12, %rdi
	call	SMIME_text@PLT
	testl	%eax, %eax
	jne	.L169
	movl	$376, %r8d
	movl	$140, %edx
	movl	$157, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L168
.L272:
	movq	$0, -104(%rbp)
	jmp	.L175
.L277:
	movl	$65, %edx
	movl	$157, %esi
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	movl	$362, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L167
.L264:
	call	BIO_s_null@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, -104(%rbp)
	jmp	.L164
.L267:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1475:
	.size	CMS_verify, .-CMS_verify
	.p2align 4
	.globl	CMS_verify_receipt
	.type	CMS_verify_receipt, @function
CMS_verify_receipt:
.LFB1476:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%r8d, %r9d
	xorl	%r8d, %r8d
	andl	$-66, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movq	%rdx, %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rcx, %rdx
	xorl	%ecx, %ecx
	movq	%rdi, %r12
	call	CMS_verify
	testl	%eax, %eax
	jle	.L278
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	cms_Receipt_verify@PLT
	.p2align 4,,10
	.p2align 3
.L278:
	.cfi_restore_state
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1476:
	.size	CMS_verify_receipt, .-CMS_verify_receipt
	.p2align 4
	.globl	CMS_sign
	.type	CMS_sign, @function
CMS_sign:
.LFB1477:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	%rcx, -56(%rbp)
	call	CMS_ContentInfo_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L282
	movq	%rax, %rdi
	call	CMS_SignedData_init@PLT
	testl	%eax, %eax
	je	.L282
	testl	$524288, %r13d
	jne	.L283
	testq	%r14, %r14
	je	.L285
.L284:
	xorl	%ecx, %ecx
	movl	%r13d, %r8d
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	CMS_add1_signer@PLT
	testq	%rax, %rax
	je	.L308
.L285:
	xorl	%r14d, %r14d
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L289:
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	CMS_add1_cert@PLT
	testl	%eax, %eax
	je	.L282
	addl	$1, %r14d
.L288:
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r14d
	jl	.L289
	testb	$64, %r13b
	je	.L309
.L290:
	testl	$20480, %r13d
	jne	.L281
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	CMS_dataInit@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L310
	movq	-56(%rbp), %rdi
	movl	%r13d, %edx
	movq	%rax, %rsi
	call	SMIME_crlf_copy@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%r14, %rdi
	call	BIO_ctrl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	CMS_dataFinal@PLT
	testl	%eax, %eax
	je	.L311
	movq	%r14, %rdi
	call	BIO_free_all@PLT
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L282:
	movl	$471, %r8d
	movl	$65, %edx
	movl	$148, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L286:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	CMS_ContentInfo_free@PLT
.L281:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L283:
	.cfi_restore_state
	movl	$787, %edi
	call	OBJ_nid2obj@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	CMS_set1_eContentType@PLT
	testl	%eax, %eax
	je	.L286
	testq	%r14, %r14
	jne	.L284
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L309:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	CMS_set_detached@PLT
	jmp	.L290
.L308:
	movl	$451, %r8d
	movl	$99, %edx
	movl	$148, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L286
.L310:
	movl	$768, %r8d
	movl	$104, %edx
	movl	$127, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L286
.L311:
	movl	$777, %r8d
	movl	$103, %edx
	movl	$127, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r14, %rdi
	call	BIO_free_all@PLT
	jmp	.L286
	.cfi_endproc
.LFE1477:
	.size	CMS_sign, .-CMS_sign
	.p2align 4
	.globl	CMS_sign_receipt
	.type	CMS_sign_receipt, @function
CMS_sign_receipt:
.LFB1478:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	andl	$-4098, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%r8d, %r12d
	pushq	%rbx
	orl	$16576, %r12d
	subq	$24, %rsp
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L322
	movq	%rsi, %r15
	testq	%rsi, %rsi
	je	.L322
	movq	%rcx, %rdx
	movq	%rdi, %rbx
	movl	%r12d, %r8d
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	CMS_sign
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L316
	movl	$204, %edi
	call	OBJ_nid2obj@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	CMS_set1_eContentType@PLT
	testl	%eax, %eax
	jne	.L336
.L316:
	xorl	%edi, %edi
	call	BIO_free@PLT
.L318:
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	CMS_ContentInfo_free@PLT
.L312:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L336:
	.cfi_restore_state
	movq	%r15, %rsi
	movl	%r12d, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	CMS_add1_signer@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L337
	movq	%rbx, %rdi
	call	cms_encode_Receipt@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L316
	movq	8(%rax), %rdi
	movl	(%rax), %esi
	call	BIO_new_mem_buf@PLT
	testq	%rax, %rax
	movq	%rax, -56(%rbp)
	je	.L316
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	cms_msgSigDigest_add1@PLT
	movq	-56(%rbp), %r9
	testl	%eax, %eax
	je	.L319
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	CMS_dataInit@PLT
	movq	-56(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L338
	movq	%r9, %rdi
	movl	%r12d, %edx
	movq	%rax, %rsi
	movq	%r9, -56(%rbp)
	call	SMIME_crlf_copy@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%r15, %rdi
	call	BIO_ctrl@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	CMS_dataFinal@PLT
	movq	-56(%rbp), %r9
	testl	%eax, %eax
	je	.L339
	movq	%r15, %rdi
	movq	%r9, -56(%rbp)
	call	BIO_free_all@PLT
	movq	%r13, %rdi
	call	CMS_get0_content@PLT
	movq	-56(%rbp), %r9
	movq	%r14, (%rax)
	movq	%r9, %rdi
	call	BIO_free@PLT
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L322:
	movl	$492, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$174, %edx
	xorl	%r13d, %r13d
	movl	$163, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L319:
	movq	%r9, %rdi
	call	BIO_free@PLT
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L337:
	movl	$508, %r8d
	movl	$99, %edx
	movl	$163, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%edi, %edi
	call	BIO_free@PLT
	jmp	.L318
.L338:
	movl	$768, %r8d
	movl	$104, %edx
	movl	$127, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-56(%rbp), %r9
	movq	%r9, %rdi
	call	BIO_free@PLT
	jmp	.L318
.L339:
	movl	$777, %r8d
	movl	$103, %edx
	movl	$127, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r15, %rdi
	call	BIO_free_all@PLT
	movq	-56(%rbp), %r9
	movq	%r9, %rdi
	call	BIO_free@PLT
	jmp	.L318
	.cfi_endproc
.LFE1478:
	.size	CMS_sign_receipt, .-CMS_sign_receipt
	.p2align 4
	.globl	CMS_encrypt
	.type	CMS_encrypt, @function
CMS_encrypt:
.LFB1479:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$8, %rsp
	call	CMS_EnvelopedData_create@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L341
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L344:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	movl	%r14d, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	CMS_add1_recipient_cert@PLT
	testq	%rax, %rax
	je	.L354
	addl	$1, %ebx
.L341:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L344
	testb	$64, %r14b
	je	.L355
.L345:
	testl	$20480, %r14d
	jne	.L340
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	CMS_dataInit@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L356
	movl	%r14d, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	SMIME_crlf_copy@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%r12, %rdi
	call	BIO_ctrl@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	CMS_dataFinal@PLT
	testl	%eax, %eax
	je	.L357
	movq	%r12, %rdi
	call	BIO_free_all@PLT
.L340:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L354:
	.cfi_restore_state
	movl	$558, %r8d
	movl	$137, %edx
	movl	$119, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L342:
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	CMS_ContentInfo_free@PLT
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L355:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	CMS_set_detached@PLT
	jmp	.L345
.L356:
	movl	$768, %r8d
	movl	$104, %edx
	movl	$127, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L342
.L353:
	movl	$573, %r8d
	movl	$65, %edx
	movl	$119, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L342
.L357:
	movl	$777, %r8d
	movl	$103, %edx
	movl	$127, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdi
	call	BIO_free_all@PLT
	jmp	.L342
	.cfi_endproc
.LFE1479:
	.size	CMS_encrypt, .-CMS_encrypt
	.p2align 4
	.globl	CMS_decrypt_set1_pkey
	.type	CMS_decrypt_set1_pkey, @function
CMS_decrypt_set1_pkey:
.LFB1481:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%rdi, -72(%rbp)
	movq	%rdx, -56(%rbp)
	call	CMS_get0_RecipientInfos@PLT
	movl	$0, -92(%rbp)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L359
	movq	8(%rbx), %rax
	movq	24(%rax), %rax
	movl	48(%rax), %eax
	movl	%eax, -92(%rbp)
.L359:
	movq	%r15, %rdi
	xorl	%ebx, %ebx
	call	cms_pkey_get_ri_type@PLT
	movl	$0, -64(%rbp)
	movl	%eax, %r14d
	cmpl	$-1, %eax
	je	.L398
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L399
	.p2align 4,,10
	.p2align 3
.L373:
	movq	%r13, %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	CMS_RecipientInfo_type@PLT
	cmpl	%r14d, %eax
	jne	.L362
	cmpl	$1, %r14d
	je	.L400
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L370
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	CMS_RecipientInfo_ktri_cert_cmp@PLT
	testl	%eax, %eax
	jne	.L367
	movq	%r15, %rdi
	movl	%eax, -56(%rbp)
	call	EVP_PKEY_up_ref@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	CMS_RecipientInfo_set0_pkey@PLT
	movq	-72(%rbp), %rdi
	movq	%r12, %rsi
	call	CMS_RecipientInfo_decrypt@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	CMS_RecipientInfo_set0_pkey@PLT
	movl	-92(%rbp), %edx
	movl	-56(%rbp), %r9d
	testl	%edx, %edx
	je	.L375
	testl	%ebx, %ebx
	jle	.L372
.L366:
	movl	$1, %r9d
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L370:
	movq	%r15, %rdi
	call	EVP_PKEY_up_ref@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	CMS_RecipientInfo_set0_pkey@PLT
	movq	-72(%rbp), %rdi
	movq	%r12, %rsi
	call	CMS_RecipientInfo_decrypt@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	%eax, -64(%rbp)
	call	CMS_RecipientInfo_set0_pkey@PLT
	movl	-64(%rbp), %edx
	testl	%edx, %edx
	jle	.L367
	movl	-92(%rbp), %eax
	testl	%eax, %eax
	jne	.L366
.L367:
	movl	$1, -64(%rbp)
.L362:
	movq	%r13, %rdi
	addl	$1, %ebx
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L373
.L399:
	movl	-92(%rbp), %eax
	orl	%r14d, %eax
	jne	.L374
	cmpq	$0, -56(%rbp)
	jne	.L374
	movl	-64(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L375
.L374:
	movl	$667, %r8d
	movl	$132, %edx
	movl	$114, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r9d, %r9d
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L400:
	movq	%r12, %rdi
	call	CMS_RecipientInfo_kari_get0_reks@PLT
	cmpq	$0, -56(%rbp)
	movl	$0, -64(%rbp)
	movq	%rax, %r8
	je	.L401
.L364:
	movq	%r8, %rdi
	movq	%r8, -80(%rbp)
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, -64(%rbp)
	movq	-80(%rbp), %r8
	jge	.L367
	movl	-64(%rbp), %esi
	movq	%r8, %rdi
	movq	%r8, -88(%rbp)
	call	OPENSSL_sk_value@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, -80(%rbp)
	call	CMS_RecipientEncryptedKey_cert_cmp@PLT
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %r8
	testl	%eax, %eax
	jne	.L402
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rdx, -56(%rbp)
	call	CMS_RecipientInfo_kari_set0_pkey@PLT
	movq	-56(%rbp), %rdx
	movq	-72(%rbp), %rdi
	movq	%r12, %rsi
	call	CMS_RecipientInfo_kari_decrypt@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	CMS_RecipientInfo_kari_set0_pkey@PLT
	xorl	%r9d, %r9d
	testl	%ebx, %ebx
	setg	%r9b
.L358:
	addq	$56, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L402:
	.cfi_restore_state
	addl	$1, -64(%rbp)
	jmp	.L364
.L375:
	call	ERR_clear_error@PLT
	movl	$1, %r9d
	jmp	.L358
.L401:
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jle	.L367
	movq	-64(%rbp), %r8
	xorl	%esi, %esi
	movq	%r8, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	CMS_RecipientInfo_kari_set0_pkey@PLT
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rdi
	movq	%r12, %rsi
	call	CMS_RecipientInfo_kari_decrypt@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	%eax, -64(%rbp)
	call	CMS_RecipientInfo_kari_set0_pkey@PLT
	movl	-64(%rbp), %edx
	testl	%edx, %edx
	jle	.L367
	jmp	.L366
.L398:
	movl	$612, %r8d
	movl	$125, %edx
	movl	$114, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r9d, %r9d
	jmp	.L358
.L372:
	movl	$649, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$112, %edx
	movl	$114, %esi
	movl	$46, %edi
	movl	%r9d, -56(%rbp)
	call	ERR_put_error@PLT
	movl	-56(%rbp), %r9d
	jmp	.L358
	.cfi_endproc
.LFE1481:
	.size	CMS_decrypt_set1_pkey, .-CMS_decrypt_set1_pkey
	.p2align 4
	.globl	CMS_decrypt_set1_key
	.type	CMS_decrypt_set1_key, @function
CMS_decrypt_set1_key:
.LFB1482:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$40, %rsp
	movq	%rsi, -64(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%r8, -56(%rbp)
	call	CMS_get0_RecipientInfos@PLT
	movq	%rax, %r12
	testq	%r13, %r13
	je	.L404
	.p2align 4,,10
	.p2align 3
.L408:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%ebx, %eax
	jle	.L405
	movq	%r12, %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	movq	%rax, %r15
	call	CMS_RecipientInfo_type@PLT
	cmpl	$2, %eax
	jne	.L406
	movq	-56(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	CMS_RecipientInfo_kekri_id_cmp@PLT
	testl	%eax, %eax
	jne	.L406
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	%r15, %rdi
	movl	%eax, -56(%rbp)
	call	CMS_RecipientInfo_set0_key@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	CMS_RecipientInfo_decrypt@PLT
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movl	%eax, %ebx
	call	CMS_RecipientInfo_set0_key@PLT
	testl	%ebx, %ebx
	movl	-56(%rbp), %r9d
	jle	.L413
.L414:
	movl	$1, %r9d
.L403:
	addq	$40, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L412:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	CMS_RecipientInfo_type@PLT
	cmpl	$2, %eax
	jne	.L409
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	%r13, %rdi
	call	CMS_RecipientInfo_set0_key@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	CMS_RecipientInfo_decrypt@PLT
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movl	%eax, %r15d
	call	CMS_RecipientInfo_set0_key@PLT
	testl	%r15d, %r15d
	jg	.L414
	call	ERR_clear_error@PLT
.L409:
	addl	$1, %ebx
.L404:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L412
.L405:
	movl	$703, %r8d
	movl	$132, %edx
	movl	$113, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r9d, %r9d
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L406:
	addl	$1, %ebx
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L413:
	movl	$696, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$112, %edx
	movl	$113, %esi
	movl	$46, %edi
	movl	%r9d, -56(%rbp)
	call	ERR_put_error@PLT
	movl	-56(%rbp), %r9d
	jmp	.L403
	.cfi_endproc
.LFE1482:
	.size	CMS_decrypt_set1_key, .-CMS_decrypt_set1_key
	.p2align 4
	.globl	CMS_decrypt_set1_password
	.type	CMS_decrypt_set1_password, @function
CMS_decrypt_set1_password:
.LFB1483:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$24, %rsp
	movq	%rsi, -56(%rbp)
	movq	%rdx, -64(%rbp)
	call	CMS_get0_RecipientInfos@PLT
	movq	%rax, %r12
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L424:
	addl	$1, %ebx
.L421:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L428
	movq	%r12, %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	movq	%rax, %r15
	call	CMS_RecipientInfo_type@PLT
	cmpl	$3, %eax
	jne	.L424
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r15, %rdi
	call	CMS_RecipientInfo_set0_password@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	CMS_RecipientInfo_decrypt@PLT
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movl	%eax, %r14d
	call	CMS_RecipientInfo_set0_password@PLT
	testl	%r14d, %r14d
	jle	.L424
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L428:
	.cfi_restore_state
	movl	$726, %r8d
	movl	$132, %edx
	movl	$166, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1483:
	.size	CMS_decrypt_set1_password, .-CMS_decrypt_set1_password
	.p2align 4
	.globl	CMS_decrypt
	.type	CMS_decrypt, @function
CMS_decrypt:
.LFB1484:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r9d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	call	CMS_get0_type@PLT
	movq	%rax, %rdi
	call	OBJ_obj2nid@PLT
	movq	-56(%rbp), %rdx
	cmpl	$23, %eax
	jne	.L459
	testq	%rbx, %rbx
	je	.L460
.L432:
	movq	8(%r12), %rax
	xorl	%ecx, %ecx
	testl	$131072, %r13d
	setne	%cl
	movq	24(%rax), %rax
	movl	%ecx, 48(%rax)
	testq	%rdx, %rdx
	je	.L461
	movl	$0, 52(%rax)
	testq	%r15, %r15
	je	.L441
.L438:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	CMS_decrypt_set1_pkey
	testl	%eax, %eax
	je	.L439
.L441:
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	CMS_dataInit@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L439
	movl	%r13d, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	cms_copy_content
	movl	%eax, %r15d
	testq	%rbx, %rbx
	je	.L442
	.p2align 4,,10
	.p2align 3
.L443:
	movq	%r12, %rdi
	movq	%r12, %r13
	call	BIO_pop@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	BIO_free@PLT
	testq	%r12, %r12
	je	.L429
	cmpq	%r12, %rbx
	jne	.L443
.L429:
	addq	$24, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L461:
	.cfi_restore_state
	movl	$1, 52(%rax)
	testq	%r15, %r15
	jne	.L438
	movq	%rbx, %rax
	movl	$1, %r15d
	orq	%r14, %rax
	jne	.L441
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L459:
	movl	$737, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$146, %edx
	xorl	%r15d, %r15d
	movl	$112, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L439:
	xorl	%r15d, %r15d
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L460:
	movq	%r12, %rdi
	movq	%rdx, -56(%rbp)
	call	CMS_get0_content@PLT
	testq	%rax, %rax
	je	.L433
	cmpq	$0, (%rax)
	movq	-56(%rbp), %rdx
	jne	.L432
.L433:
	movl	$82, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$127, %edx
	xorl	%r15d, %r15d
	movl	$99, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L442:
	movq	%r12, %rdi
	call	BIO_free_all@PLT
	jmp	.L429
	.cfi_endproc
.LFE1484:
	.size	CMS_decrypt, .-CMS_decrypt
	.p2align 4
	.globl	CMS_final
	.type	CMS_final, @function
CMS_final:
.LFB1485:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%rdx, %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	CMS_dataInit@PLT
	testq	%rax, %rax
	je	.L475
	movq	%rax, %r12
	movl	%r15d, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	SMIME_crlf_copy@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%r12, %rdi
	call	BIO_ctrl@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	CMS_dataFinal@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L476
.L465:
	endbr64
	movl	$1, %r13d
	testq	%rbx, %rbx
	je	.L466
	.p2align 4,,10
	.p2align 3
.L467:
	movq	%r12, %rdi
	movq	%r12, %r14
	call	BIO_pop@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	BIO_free@PLT
	testq	%r12, %r12
	je	.L462
	cmpq	%r12, %rbx
	jne	.L467
.L462:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L475:
	.cfi_restore_state
	movl	$768, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$104, %edx
	xorl	%r13d, %r13d
	movl	$127, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L462
	.p2align 4,,10
	.p2align 3
.L476:
	movl	$777, %r8d
	movl	$103, %edx
	movl	$127, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	testq	%rbx, %rbx
	jne	.L467
.L466:
	movq	%r12, %rdi
	call	BIO_free_all@PLT
	jmp	.L462
	.cfi_endproc
.LFE1485:
	.size	CMS_final, .-CMS_final
	.p2align 4
	.globl	CMS_uncompress
	.type	CMS_uncompress, @function
CMS_uncompress:
.LFB1486:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$151, %edx
	movl	$156, %esi
	movl	$46, %edi
	movl	$837, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1486:
	.size	CMS_uncompress, .-CMS_uncompress
	.p2align 4
	.globl	CMS_compress
	.type	CMS_compress, @function
CMS_compress:
.LFB1487:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$151, %edx
	movl	$104, %esi
	movl	$46, %edi
	movl	$843, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1487:
	.size	CMS_compress, .-CMS_compress
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
