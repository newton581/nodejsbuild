	.file	"x509type.c"
	.text
	.p2align 4
	.globl	X509_certificate_type
	.type	X509_certificate_type, @function
X509_certificate_type:
.LFB779:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L5
	movq	%rsi, %rdi
	testq	%rsi, %rsi
	je	.L33
.L4:
	call	EVP_PKEY_id@PLT
	cmpl	$408, %eax
	je	.L11
	jg	.L7
	cmpl	$28, %eax
	je	.L12
	cmpl	$116, %eax
	jne	.L34
	movq	%r15, %rdi
	movl	$274, %ebx
	movl	$530, %r14d
	movl	$1042, %r13d
	call	X509_get_signature_nid@PLT
	movl	$18, %r12d
	movl	%eax, -60(%rbp)
	testl	%eax, %eax
	jne	.L35
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L36
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	cmpl	$980, %eax
	jg	.L8
	cmpl	$978, %eax
	jg	.L16
	cmpl	$811, %eax
	je	.L16
	cmpl	$912, %eax
	movl	$273, %edx
	movl	$256, %ebx
	movl	$512, %r14d
	cmove	%edx, %ebx
	movl	$529, %edx
	movl	$1024, %r13d
	movl	$0, %r12d
	cmove	%edx, %r14d
	movl	$17, %eax
	movl	$1041, %edx
	cmove	%edx, %r13d
	cmove	%eax, %r12d
.L6:
	movq	%r15, %rdi
	call	X509_get_signature_nid@PLT
	movl	%eax, -60(%rbp)
	testl	%eax, %eax
	je	.L1
	.p2align 4,,10
	.p2align 3
.L35:
	xorl	%esi, %esi
	leaq	-60(%rbp), %rdx
	movl	%eax, %edi
	call	OBJ_find_sigid_algs@PLT
	testl	%eax, %eax
	je	.L1
	movl	-60(%rbp), %eax
	cmpl	$116, %eax
	je	.L21
	jg	.L9
	cmpl	$19, %eax
	je	.L20
	cmpl	$67, %eax
	je	.L21
	cmpl	$6, %eax
	cmove	%ebx, %r12d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L8:
	subl	$1087, %eax
	cmpl	$2, %eax
	sbbl	%ebx, %ebx
	andl	$16, %ebx
	addl	$256, %ebx
	cmpl	$2, %eax
	sbbl	%r14d, %r14d
	andl	$16, %r14d
	addl	$512, %r14d
	cmpl	$2, %eax
	sbbl	%r13d, %r13d
	andl	$16, %r13d
	addl	$1024, %r13d
	cmpl	$2, %eax
	sbbl	%r12d, %r12d
	andl	$16, %r12d
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L34:
	cmpl	$6, %eax
	movl	$305, %edx
	movl	$256, %ebx
	movl	$512, %r14d
	cmove	%edx, %ebx
	movl	$561, %edx
	movl	$1024, %r13d
	movl	$0, %r12d
	cmove	%edx, %r14d
	movl	$49, %eax
	movl	$1073, %edx
	cmove	%edx, %r13d
	cmove	%eax, %r12d
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L16:
	movl	$336, %ebx
	movl	$592, %r14d
	movl	$1104, %r13d
	movl	$80, %r12d
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L33:
	movq	%r15, %rdi
	call	X509_get0_pubkey@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L4
.L5:
	xorl	%r12d, %r12d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$324, %ebx
	movl	$580, %r14d
	movl	$1092, %r13d
	movl	$68, %r12d
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L11:
	movl	$344, %ebx
	movl	$600, %r14d
	movl	$1112, %r13d
	movl	$88, %r12d
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L21:
	movl	%r14d, %r12d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L9:
	cmpl	$408, %eax
	cmove	%r13d, %r12d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L20:
	movl	%ebx, %r12d
	jmp	.L1
.L36:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE779:
	.size	X509_certificate_type, .-X509_certificate_type
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
