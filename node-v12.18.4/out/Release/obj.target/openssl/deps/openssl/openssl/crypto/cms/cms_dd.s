	.file	"cms_dd.c"
	.text
	.p2align 4
	.globl	cms_DigestedData_create
	.type	cms_DigestedData_create, @function
cms_DigestedData_create:
.LFB1440:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	call	CMS_ContentInfo_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1
	leaq	CMS_DigestedData_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L9
	movl	$25, %edi
	call	OBJ_nid2obj@PLT
	movq	%rbx, %xmm1
	movl	$21, %edi
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r12)
	movq	16(%rbx), %r14
	movl	$0, (%rbx)
	call	OBJ_nid2obj@PLT
	movq	8(%rbx), %rdi
	movq	%r13, %rsi
	movq	%rax, (%r14)
	call	X509_ALGOR_set_md@PLT
.L1:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	CMS_ContentInfo_free@PLT
	jmp	.L1
	.cfi_endproc
.LFE1440:
	.size	cms_DigestedData_create, .-cms_DigestedData_create
	.p2align 4
	.globl	cms_DigestedData_init_bio
	.type	cms_DigestedData_init_bio, @function
cms_DigestedData_init_bio:
.LFB1441:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	8(%rax), %rdi
	jmp	cms_DigestAlgorithm_init_bio@PLT
	.cfi_endproc
.LFE1441:
	.size	cms_DigestedData_init_bio, .-cms_DigestedData_init_bio
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/cms/cms_dd.c"
	.text
	.p2align 4
	.globl	cms_DigestedData_do_final
	.type	cms_DigestedData_do_final, @function
cms_DigestedData_do_final:
.LFB1442:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L23
	movq	8(%rbx), %rbx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	8(%rbx), %rdx
	call	cms_DigestAlgorithm_find_ctx@PLT
	testl	%eax, %eax
	jne	.L14
.L22:
	xorl	%r13d, %r13d
.L13:
	movq	%r12, %rdi
	call	EVP_MD_CTX_free@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L24
	addq	$96, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	leaq	-112(%rbp), %r13
	leaq	-116(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	jle	.L22
	movl	-116(%rbp), %edx
	movq	24(%rbx), %rdi
	testl	%r14d, %r14d
	je	.L16
	cmpl	%edx, (%rdi)
	jne	.L25
	movq	8(%rdi), %rsi
	movq	%r13, %rdi
	movl	$1, %r13d
	call	memcmp@PLT
	testl	%eax, %eax
	je	.L13
	movl	$84, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$158, %edx
	xorl	%r13d, %r13d
	movl	$117, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L16:
	movq	%r13, %rsi
	xorl	%r13d, %r13d
	call	ASN1_STRING_set@PLT
	testl	%eax, %eax
	setne	%r13b
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L23:
	movl	$64, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$117, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L25:
	movl	$78, %r8d
	movl	$121, %edx
	movl	$117, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L22
.L24:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1442:
	.size	cms_DigestedData_do_final, .-cms_DigestedData_do_final
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
