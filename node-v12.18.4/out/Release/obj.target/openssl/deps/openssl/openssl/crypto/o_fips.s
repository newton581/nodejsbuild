	.file	"o_fips.c"
	.text
	.p2align 4
	.globl	FIPS_mode
	.type	FIPS_mode, @function
FIPS_mode:
.LFB251:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE251:
	.size	FIPS_mode, .-FIPS_mode
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/o_fips.c"
	.text
	.p2align 4
	.globl	FIPS_mode_set
	.type	FIPS_mode_set, @function
FIPS_mode_set:
.LFB252:
	.cfi_startproc
	endbr64
	testl	%edi, %edi
	jne	.L12
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$101, %edx
	movl	$109, %esi
	movl	$15, %edi
	movl	$22, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE252:
	.size	FIPS_mode_set, .-FIPS_mode_set
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
