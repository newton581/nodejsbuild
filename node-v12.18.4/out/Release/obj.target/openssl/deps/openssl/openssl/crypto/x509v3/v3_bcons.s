	.file	"v3_bcons.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"CA"
.LC1:
	.string	"pathlen"
	.text
	.p2align 4
	.type	i2v_BASIC_CONSTRAINTS, @function
i2v_BASIC_CONSTRAINTS:
.LFB1324:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-24(%rbp), %r12
	movq	%rsi, %rbx
	subq	$16, %rsp
	movl	(%rsi), %esi
	movq	%rdx, -24(%rbp)
	movq	%r12, %rdx
	call	X509V3_add_value_bool@PLT
	movq	8(%rbx), %rsi
	movq	%r12, %rdx
	leaq	.LC1(%rip), %rdi
	call	X509V3_add_value_int@PLT
	movq	-24(%rbp), %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1324:
	.size	i2v_BASIC_CONSTRAINTS, .-i2v_BASIC_CONSTRAINTS
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"../deps/openssl/openssl/crypto/x509v3/v3_bcons.c"
	.section	.rodata.str1.1
.LC3:
	.string	",value:"
.LC4:
	.string	",name:"
.LC5:
	.string	"section:"
	.text
	.p2align 4
	.type	v2i_BASIC_CONSTRAINTS, @function
v2i_BASIC_CONSTRAINTS:
.LFB1325:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	BASIC_CONSTRAINTS_it(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	.LC1(%rip), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$8, %rsp
	call	ASN1_item_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L5
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L25:
	cmpb	$65, 1(%rsi)
	jne	.L8
	cmpb	$0, 2(%rsi)
	jne	.L8
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	X509V3_get_value_bool@PLT
	testl	%eax, %eax
	je	.L12
.L10:
	addl	$1, %ebx
.L5:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L4
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movq	8(%rax), %rsi
	movq	%rax, %r12
	cmpb	$67, (%rsi)
	je	.L25
.L8:
	movl	$8, %ecx
	movq	%r15, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L11
	leaq	8(%r14), %rsi
	movq	%r12, %rdi
	call	X509V3_get_value_int@PLT
	testl	%eax, %eax
	jne	.L10
.L12:
	movq	%r14, %rdi
	leaq	BASIC_CONSTRAINTS_it(%rip), %rsi
	xorl	%r14d, %r14d
	call	ASN1_item_free@PLT
.L4:
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movl	$75, %r8d
	movl	$106, %edx
	movl	$102, %esi
	movl	$34, %edi
	leaq	.LC2(%rip), %rcx
	call	ERR_put_error@PLT
	subq	$8, %rsp
	movq	8(%r12), %r8
	xorl	%eax, %eax
	pushq	16(%r12)
	movq	(%r12), %rdx
	leaq	.LC3(%rip), %r9
	leaq	.LC4(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	movl	$6, %edi
	call	ERR_add_error_data@PLT
	popq	%rax
	popq	%rdx
	jmp	.L12
.L24:
	movl	$63, %r8d
	movl	$65, %edx
	movl	$102, %esi
	movl	$34, %edi
	leaq	.LC2(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L4
	.cfi_endproc
.LFE1325:
	.size	v2i_BASIC_CONSTRAINTS, .-v2i_BASIC_CONSTRAINTS
	.p2align 4
	.globl	d2i_BASIC_CONSTRAINTS
	.type	d2i_BASIC_CONSTRAINTS, @function
d2i_BASIC_CONSTRAINTS:
.LFB1320:
	.cfi_startproc
	endbr64
	leaq	BASIC_CONSTRAINTS_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1320:
	.size	d2i_BASIC_CONSTRAINTS, .-d2i_BASIC_CONSTRAINTS
	.p2align 4
	.globl	i2d_BASIC_CONSTRAINTS
	.type	i2d_BASIC_CONSTRAINTS, @function
i2d_BASIC_CONSTRAINTS:
.LFB1321:
	.cfi_startproc
	endbr64
	leaq	BASIC_CONSTRAINTS_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1321:
	.size	i2d_BASIC_CONSTRAINTS, .-i2d_BASIC_CONSTRAINTS
	.p2align 4
	.globl	BASIC_CONSTRAINTS_new
	.type	BASIC_CONSTRAINTS_new, @function
BASIC_CONSTRAINTS_new:
.LFB1322:
	.cfi_startproc
	endbr64
	leaq	BASIC_CONSTRAINTS_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1322:
	.size	BASIC_CONSTRAINTS_new, .-BASIC_CONSTRAINTS_new
	.p2align 4
	.globl	BASIC_CONSTRAINTS_free
	.type	BASIC_CONSTRAINTS_free, @function
BASIC_CONSTRAINTS_free:
.LFB1323:
	.cfi_startproc
	endbr64
	leaq	BASIC_CONSTRAINTS_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1323:
	.size	BASIC_CONSTRAINTS_free, .-BASIC_CONSTRAINTS_free
	.globl	BASIC_CONSTRAINTS_it
	.section	.rodata.str1.1
.LC6:
	.string	"BASIC_CONSTRAINTS"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	BASIC_CONSTRAINTS_it, @object
	.size	BASIC_CONSTRAINTS_it, 56
BASIC_CONSTRAINTS_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	BASIC_CONSTRAINTS_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC6
	.section	.rodata.str1.1
.LC7:
	.string	"ca"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	BASIC_CONSTRAINTS_seq_tt, @object
	.size	BASIC_CONSTRAINTS_seq_tt, 80
BASIC_CONSTRAINTS_seq_tt:
	.quad	1
	.quad	0
	.quad	0
	.quad	.LC7
	.quad	ASN1_FBOOLEAN_it
	.quad	1
	.quad	0
	.quad	8
	.quad	.LC1
	.quad	ASN1_INTEGER_it
	.globl	v3_bcons
	.section	.data.rel.ro.local
	.align 32
	.type	v3_bcons, @object
	.size	v3_bcons, 104
v3_bcons:
	.long	87
	.long	0
	.quad	BASIC_CONSTRAINTS_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	i2v_BASIC_CONSTRAINTS
	.quad	v2i_BASIC_CONSTRAINTS
	.quad	0
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
