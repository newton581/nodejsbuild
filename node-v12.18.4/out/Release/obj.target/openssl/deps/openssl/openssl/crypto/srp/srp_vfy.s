	.file	"srp_vfy.c"
	.text
	.p2align 4
	.type	t_tob64, @function
t_tob64:
.LFB542:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_ENCODE_CTX_new@PLT
	movl	$0, -68(%rbp)
	movq	%rax, %r12
	xorl	%eax, %eax
	movl	$0, -64(%rbp)
	movw	%ax, -58(%rbp)
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L1
	movq	%r12, %rdi
	call	EVP_EncodeInit@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	call	evp_encode_ctx_set_flags@PLT
	movslq	%ebx, %rax
	movl	%ebx, %edx
	imulq	$1431655766, %rax, %rax
	sarl	$31, %edx
	shrq	$32, %rax
	subl	%edx, %eax
	leal	(%rax,%rax,2), %eax
	subl	%ebx, %eax
	leal	3(%rax), %r15d
	je	.L3
	leaq	-58(%rbp), %rcx
	leaq	-68(%rbp), %rdx
	movl	%r15d, %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	EVP_EncodeUpdate@PLT
	testl	%eax, %eax
	je	.L16
.L3:
	movslq	-68(%rbp), %rsi
	leaq	-64(%rbp), %rdx
	movl	%ebx, %r8d
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	%rdx, -88(%rbp)
	addq	%r13, %rsi
	call	EVP_EncodeUpdate@PLT
	movq	-88(%rbp), %rdx
	testl	%eax, %eax
	je	.L16
	movl	-64(%rbp), %esi
	addl	-68(%rbp), %esi
	movq	%r12, %rdi
	movl	%esi, -68(%rbp)
	movslq	%esi, %rsi
	addq	%r13, %rsi
	call	EVP_EncodeFinal@PLT
	movl	-64(%rbp), %eax
	addl	-68(%rbp), %eax
	movl	%eax, -68(%rbp)
	cmpl	$3, %r15d
	je	.L5
	movslq	%r15d, %r15
	movslq	%eax, %rbx
	movq	%r13, %rdi
	subq	%r15, %rbx
	leaq	0(%r13,%r15), %rsi
	movq	%rbx, %rdx
	call	memmove@PLT
	movb	$0, 0(%r13,%rbx)
.L5:
	movq	%r12, %rdi
	call	EVP_ENCODE_CTX_free@PLT
	movl	$1, %eax
.L1:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L18
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	%eax, -88(%rbp)
	call	EVP_ENCODE_CTX_free@PLT
	movl	-88(%rbp), %eax
	jmp	.L1
.L18:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE542:
	.size	t_tob64, .-t_tob64
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"00"
	.text
	.p2align 4
	.type	t_fromb64.constprop.0, @function
t_fromb64.constprop.0:
.LFB570:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	(%rsi), %eax
	movl	$0, -64(%rbp)
	movl	$0, -60(%rbp)
	leal	-9(%rax), %edx
	cmpb	$1, %dl
	ja	.L48
	.p2align 4,,10
	.p2align 3
.L40:
	movzbl	1(%r12), %eax
	addq	$1, %r12
	leal	-9(%rax), %edx
	cmpb	$1, %dl
	jbe	.L40
.L48:
	cmpb	$32, %al
	je	.L40
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%rax, %r15
	movq	%rax, %rbx
	negq	%r15
	andl	$3, %r15d
	cmpq	$2147483647, %rax
	ja	.L25
	leaq	(%rax,%r15), %rax
	shrq	$2, %rax
	leaq	(%rax,%rax,2), %rax
	cmpq	$2500, %rax
	ja	.L25
	call	EVP_ENCODE_CTX_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L25
	cmpq	$3, %r15
	je	.L29
	movq	%rax, %rdi
	call	EVP_DecodeInit@PLT
	movl	$2, %esi
	movq	%r13, %rdi
	call	evp_encode_ctx_set_flags@PLT
	testq	%r15, %r15
	je	.L28
	leaq	-64(%rbp), %rdx
	movl	%r15d, %r8d
	movq	%r14, %rsi
	movq	%r13, %rdi
	leaq	.LC0(%rip), %rcx
	call	EVP_DecodeUpdate@PLT
	testl	%eax, %eax
	js	.L29
	leaq	-60(%rbp), %rdx
	movl	%ebx, %r8d
	movq	%r12, %rcx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rdx, -72(%rbp)
	call	EVP_DecodeUpdate@PLT
	movq	-72(%rbp), %rdx
	testl	%eax, %eax
	js	.L29
	movl	-60(%rbp), %eax
	addl	-64(%rbp), %eax
	movq	%r13, %rdi
	movl	%eax, -64(%rbp)
	cltq
	leaq	(%r14,%rax), %rsi
	call	EVP_DecodeFinal@PLT
	movl	-60(%rbp), %ebx
	addl	-64(%rbp), %ebx
	cmpl	%ebx, %r15d
	jge	.L29
	movslq	%ebx, %rdx
	leaq	(%r14,%r15), %rsi
	movq	%r14, %rdi
	subl	%r15d, %ebx
	subq	%r15, %rdx
	call	memmove@PLT
	movl	%ebx, -64(%rbp)
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L28:
	leaq	-60(%rbp), %r15
	movl	%ebx, %r8d
	movq	%r12, %rcx
	movq	%r14, %rsi
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	EVP_DecodeUpdate@PLT
	testl	%eax, %eax
	jns	.L46
.L29:
	movl	$-1, -64(%rbp)
.L27:
	movq	%r13, %rdi
	call	EVP_ENCODE_CTX_free@PLT
	movl	-64(%rbp), %eax
.L19:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L49
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L46:
	.cfi_restore_state
	movl	-60(%rbp), %eax
	addl	-64(%rbp), %eax
	movq	%r15, %rdx
	movq	%r13, %rdi
	movl	%eax, -64(%rbp)
	cltq
	leaq	(%r14,%rax), %rsi
	call	EVP_DecodeFinal@PLT
	movl	-60(%rbp), %eax
	addl	%eax, -64(%rbp)
	jmp	.L27
.L25:
	movl	$-1, %eax
	jmp	.L19
.L49:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE570:
	.size	t_fromb64.constprop.0, .-t_fromb64.constprop.0
	.p2align 4
	.type	SRP_user_pwd_set_sv.isra.0, @function
SRP_user_pwd_set_sv.isra.0:
.LFB568:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-2544(%rbp), %r14
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	movq	%rcx, %rsi
	subq	$2512, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, (%rbx)
	movq	$0, (%rdi)
	movq	%r14, %rdi
	call	t_fromb64.constprop.0
	testl	%eax, %eax
	js	.L64
	movl	%eax, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	BN_bin2bn@PLT
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L64
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	t_fromb64.constprop.0
	movl	%eax, %esi
	testl	%eax, %eax
	js	.L55
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	BN_bin2bn@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L55
	movl	$1, %eax
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L55:
	movq	(%rbx), %rdi
	call	BN_free@PLT
	movq	$0, (%rbx)
.L64:
	xorl	%eax, %eax
.L50:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L65
	addq	$2512, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L65:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE568:
	.size	SRP_user_pwd_set_sv.isra.0, .-SRP_user_pwd_set_sv.isra.0
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../deps/openssl/openssl/crypto/srp/srp_vfy.c"
	.text
	.p2align 4
	.type	SRP_gN_new_init, @function
SRP_gN_new_init:
.LFB552:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$308, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$16, %edi
	pushq	%r12
	subq	$2512, %rsp
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L66
	leaq	-2544(%rbp), %r15
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	t_fromb64.constprop.0
	movl	%eax, %r14d
	testl	%eax, %eax
	js	.L69
	movl	$317, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_strdup@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L69
	xorl	%edx, %edx
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	BN_bin2bn@PLT
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L79
.L66:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L80
	addq	$2512, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	movq	(%r12), %rdi
	movl	$323, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
.L69:
	movq	%r12, %rdi
	movl	$325, %edx
	leaq	.LC1(%rip), %rsi
	xorl	%r12d, %r12d
	call	CRYPTO_free@PLT
	jmp	.L66
.L80:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE552:
	.size	SRP_gN_new_init, .-SRP_gN_new_init
	.p2align 4
	.type	SRP_gN_place_bn, @function
SRP_gN_place_bn:
.LFB555:
	.cfi_startproc
	testq	%rdi, %rdi
	je	.L99
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	xorl	%ebx, %ebx
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L85:
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r14, %rsi
	movq	(%rax), %rdi
	movq	%rax, %r12
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L97
	addl	$1, %ebx
.L82:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L85
	movq	%r14, %rdi
	call	SRP_gN_new_init
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L98
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_insert@PLT
	testl	%eax, %eax
	jle	.L87
.L97:
	popq	%rbx
	movq	8(%r12), %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L87:
	.cfi_restore_state
	movq	(%r12), %rdi
	movl	$333, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	8(%r12), %rdi
	call	BN_free@PLT
	movl	$335, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
.L98:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
.L99:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE555:
	.size	SRP_gN_place_bn, .-SRP_gN_place_bn
	.p2align 4
	.globl	SRP_user_pwd_free
	.type	SRP_user_pwd_free, @function
SRP_user_pwd_free:
.LFB543:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L100
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	call	BN_free@PLT
	movq	16(%r12), %rdi
	call	BN_clear_free@PLT
	movq	(%r12), %rdi
	movl	$182, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	40(%r12), %rdi
	movl	$183, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$184, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC1(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L100:
	ret
	.cfi_endproc
.LFE543:
	.size	SRP_user_pwd_free, .-SRP_user_pwd_free
	.p2align 4
	.globl	SRP_VBASE_new
	.type	SRP_VBASE_new, @function
SRP_VBASE_new:
.LFB550:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$273, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$40, %edi
	pushq	%r12
	.cfi_offset 12, -32
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L105
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L108
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L108
	movq	$0, 32(%r12)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 16(%r12)
	testq	%r13, %r13
	je	.L105
	movl	$285, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_strdup@PLT
	movq	%rax, 16(%r12)
	testq	%rax, %rax
	je	.L119
.L105:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$279, %edx
	leaq	.LC1(%rip), %rsi
	xorl	%r12d, %r12d
	call	CRYPTO_free@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	.cfi_restore_state
	movq	(%r12), %rdi
	call	OPENSSL_sk_free@PLT
	movq	8(%r12), %rdi
	call	OPENSSL_sk_free@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movl	$288, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L105
	.cfi_endproc
.LFE550:
	.size	SRP_VBASE_new, .-SRP_VBASE_new
	.p2align 4
	.globl	SRP_VBASE_free
	.type	SRP_VBASE_free, @function
SRP_VBASE_free:
.LFB551:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L120
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	SRP_user_pwd_free(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	8(%r12), %rdi
	call	OPENSSL_sk_free@PLT
	movq	16(%r12), %rdi
	movl	$300, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$301, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC1(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L120:
	ret
	.cfi_endproc
.LFE551:
	.size	SRP_VBASE_free, .-SRP_VBASE_free
	.p2align 4
	.globl	SRP_VBASE_init
	.type	SRP_VBASE_init, @function
SRP_VBASE_init:
.LFB556:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -80(%rbp)
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %r14
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L128
	movq	%r12, %rcx
	movl	$3, %edx
	movl	$108, %esi
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	BIO_ctrl@PLT
	testl	%eax, %eax
	jle	.L128
	movl	$6, %esi
	movq	%rbx, %rdi
	call	TXT_DB_read@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L159
	movq	-80(%rbp), %rax
	movq	16(%rax), %rax
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L129
	xorl	%edi, %edi
	call	SRP_get_default_gN@PLT
	movq	(%rax), %rax
	movq	%rax, -96(%rbp)
.L129:
	movl	$4, -84(%rbp)
	xorl	%r13d, %r13d
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L131:
	cmpb	$86, %al
	je	.L209
.L137:
	addl	$1, %r13d
.L130:
	movq	8(%r15), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jge	.L210
	movq	8(%r15), %rdi
	movl	%r13d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rbx
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	cmpb	$73, %al
	jne	.L131
	movl	$418, %edx
	leaq	.LC1(%rip), %rsi
	movl	$24, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L127
	movq	24(%rbx), %rdi
	movl	$421, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L133
	movq	-80(%rbp), %rax
	movq	8(%rbx), %rsi
	movq	8(%rax), %rdi
	call	SRP_gN_place_bn
	movq	%rax, 16(%r12)
	testq	%rax, %rax
	je	.L208
	movq	-80(%rbp), %rax
	movq	16(%rbx), %rsi
	movq	8(%rax), %rdi
	call	SRP_gN_place_bn
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L208
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	OPENSSL_sk_insert@PLT
	testl	%eax, %eax
	je	.L208
	movq	-80(%rbp), %rax
	cmpq	$0, 16(%rax)
	je	.L137
	movq	24(%rbx), %rax
	movq	%rax, -96(%rbp)
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L209:
	movq	32(%rbx), %rax
	movq	%rax, -72(%rbp)
	testq	%r14, %r14
	je	.L138
	xorl	%r12d, %r12d
	testq	%rax, %rax
	jne	.L143
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L211:
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	testq	%rax, %rax
	je	.L141
	movq	-72(%rbp), %rsi
	movq	(%rax), %rdi
	movq	%rax, -64(%rbp)
	call	strcmp@PLT
	movq	-64(%rbp), %rcx
	testl	%eax, %eax
	je	.L142
.L141:
	addl	$1, %r12d
.L143:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%r12d, %eax
	jg	.L211
.L138:
	movq	-72(%rbp), %rdi
	call	SRP_get_default_gN@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L137
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L212:
	addl	$1, %r12d
.L139:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L138
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L212
.L142:
	movl	$191, %edx
	leaq	.LC1(%rip), %rsi
	movl	$48, %edi
	movq	%rcx, -64(%rbp)
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L161
	movq	-64(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	$0, 40(%rax)
	movups	%xmm0, (%rax)
	movq	24(%rbx), %rdi
	movq	40(%rbx), %r8
	movq	$0, 16(%rax)
	movq	16(%rcx), %rdx
	movq	8(%rcx), %rax
	movq	%rdx, 32(%r12)
	movq	%rax, 24(%r12)
	testq	%rdi, %rdi
	je	.L149
	movl	$214, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r8, -64(%rbp)
	call	CRYPTO_strdup@PLT
	movq	-64(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, (%r12)
	je	.L148
.L149:
	testq	%r8, %r8
	je	.L146
	movl	$216, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r8, %rdi
	call	CRYPTO_strdup@PLT
	movq	%rax, 40(%r12)
	testq	%rax, %rax
	je	.L148
.L146:
	movq	8(%rbx), %rcx
	movq	16(%rbx), %rdx
	leaq	16(%r12), %rsi
	leaq	8(%r12), %rdi
	call	SRP_user_pwd_set_sv.isra.0
	testl	%eax, %eax
	je	.L150
	movq	-80(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	(%rax), %rdi
	call	OPENSSL_sk_insert@PLT
	testl	%eax, %eax
	je	.L150
	movl	$2, -84(%rbp)
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L148:
	movl	$4, -84(%rbp)
.L152:
	movq	8(%r12), %rdi
	call	BN_free@PLT
	movq	16(%r12), %rdi
	call	BN_clear_free@PLT
	movq	(%r12), %rdi
	movl	$182, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	40(%r12), %rdi
	movl	$183, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$184, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
.L127:
	movq	%r15, %rdi
	call	TXT_DB_free@PLT
	movq	-104(%rbp), %rdi
	call	BIO_free_all@PLT
	movq	%r14, %rdi
	call	OPENSSL_sk_free@PLT
	movl	-84(%rbp), %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	.cfi_restore_state
	movq	(%r12), %rax
.L133:
	movq	%rax, %rdi
	movl	$480, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$481, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L128:
	movl	$3, -84(%rbp)
	xorl	%r15d, %r15d
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L210:
	cmpq	$0, -96(%rbp)
	movl	$0, -84(%rbp)
	je	.L127
	xorl	%ebx, %ebx
	testq	%r14, %r14
	jne	.L154
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L157:
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L156
	movq	-96(%rbp), %rsi
	movq	(%rax), %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L155
.L156:
	addl	$1, %ebx
.L154:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L157
.L158:
	movq	-96(%rbp), %rdi
	call	SRP_get_default_gN@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L213
.L155:
	movdqu	8(%r12), %xmm1
	movq	-80(%rbp), %rax
	movl	$0, -84(%rbp)
	movaps	%xmm1, -64(%rbp)
	movups	%xmm1, 24(%rax)
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L159:
	movl	$1, -84(%rbp)
	jmp	.L127
.L161:
	movl	$4, -84(%rbp)
	jmp	.L127
.L150:
	movl	$2, -84(%rbp)
	jmp	.L152
.L213:
	movl	$2, -84(%rbp)
	jmp	.L127
	.cfi_endproc
.LFE556:
	.size	SRP_VBASE_init, .-SRP_VBASE_init
	.p2align 4
	.globl	SRP_VBASE_get_by_user
	.type	SRP_VBASE_get_by_user, @function
SRP_VBASE_get_by_user:
.LFB558:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	jne	.L215
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L217:
	movq	(%rbx), %rdi
	movl	%r13d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%r14, %rsi
	movq	(%rax), %rdi
	movq	%rax, %r12
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L214
	addl	$1, %r13d
.L215:
	movq	(%rbx), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jl	.L217
.L218:
	xorl	%r12d, %r12d
.L214:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE558:
	.size	SRP_VBASE_get_by_user, .-SRP_VBASE_get_by_user
	.p2align 4
	.globl	SRP_VBASE_get1_by_user
	.type	SRP_VBASE_get1_by_user, @function
SRP_VBASE_get1_by_user:
.LFB559:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L289
	movq	%rdi, %rbx
	movq	%rsi, %r14
	xorl	%r12d, %r12d
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L230:
	movq	(%rbx), %rdi
	movl	%r12d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%r14, %rsi
	movq	(%rax), %rdi
	movq	%rax, %r13
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L229
	addl	$1, %r12d
.L227:
	movq	(%rbx), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jl	.L230
	cmpq	$0, 16(%rbx)
	je	.L289
	cmpq	$0, 24(%rbx)
	je	.L289
	cmpq	$0, 32(%rbx)
	je	.L289
	movl	$191, %edx
	leaq	.LC1(%rip), %rsi
	movl	$48, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L289
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	32(%rbx), %rdx
	movq	$0, 40(%rax)
	movq	24(%rbx), %rax
	movq	%rdx, 32(%r12)
	movq	%rax, 24(%r12)
	testq	%r14, %r14
	je	.L240
	movl	$214, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_strdup@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L238
.L240:
	leaq	-112(%rbp), %r15
	movl	$20, %esi
	movq	%r15, %rdi
	call	RAND_priv_bytes@PLT
	testl	%eax, %eax
	jle	.L238
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L238
	call	EVP_sha1@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	je	.L241
	movq	16(%rbx), %rbx
	movq	%rbx, %rdi
	call	strlen@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L241
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L241
	leaq	-80(%rbp), %r14
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	je	.L241
	movq	%r13, %rdi
	call	EVP_MD_CTX_free@PLT
	xorl	%edx, %edx
	movl	$20, %esi
	movq	%r15, %rdi
	call	BN_bin2bn@PLT
	xorl	%edx, %edx
	movl	$20, %esi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	BN_bin2bn@PLT
	movq	%rbx, %xmm3
	movq	%rax, %xmm0
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r12)
	testq	%rax, %rax
	je	.L238
	testq	%rbx, %rbx
	jne	.L226
	.p2align 4,,10
	.p2align 3
.L238:
	xorl	%r13d, %r13d
.L241:
	movq	%r13, %rdi
	call	EVP_MD_CTX_free@PLT
.L290:
	movq	8(%r12), %rdi
.L288:
	call	BN_free@PLT
	movq	16(%r12), %rdi
	call	BN_clear_free@PLT
	movq	(%r12), %rdi
	movl	$182, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	40(%r12), %rdi
	movl	$183, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$184, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
.L289:
	xorl	%r12d, %r12d
.L226:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L291
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L229:
	.cfi_restore_state
	movl	$191, %edx
	leaq	.LC1(%rip), %rsi
	movl	$48, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L289
	pxor	%xmm0, %xmm0
	movups	%xmm0, 16(%r12)
	movups	%xmm0, 32(%r12)
	movups	%xmm0, (%r12)
	movdqu	24(%r13), %xmm1
	movq	0(%r13), %rdi
	movq	40(%r13), %r14
	movups	%xmm1, 24(%r12)
	testq	%rdi, %rdi
	je	.L232
	movl	$214, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L290
.L232:
	testq	%r14, %r14
	je	.L234
	movl	$216, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_strdup@PLT
	movq	%rax, 40(%r12)
	testq	%rax, %rax
	je	.L290
.L234:
	movq	16(%r13), %rdi
	call	BN_dup@PLT
	movq	8(%r13), %rdi
	movq	%rax, %rbx
	call	BN_dup@PLT
	movq	%rbx, %xmm2
	movq	%rax, %xmm0
	movq	%rax, %rdi
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%r12)
	testq	%rbx, %rbx
	je	.L288
	testq	%rax, %rax
	jne	.L226
	jmp	.L288
.L291:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE559:
	.size	SRP_VBASE_get1_by_user, .-SRP_VBASE_get1_by_user
	.p2align 4
	.globl	SRP_create_verifier_BN
	.type	SRP_create_verifier_BN, @function
SRP_create_verifier_BN:
.LFB561:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$2552, %rsp
	movq	%r8, -2568(%rbp)
	movq	%r9, -2576(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	call	BN_CTX_new@PLT
	movq	%rax, %r12
	testq	%r15, %r15
	je	.L293
	testq	%r13, %r13
	je	.L293
	testq	%r14, %r14
	je	.L293
	testq	%rbx, %rbx
	je	.L293
	cmpq	$0, -2568(%rbp)
	movq	(%r14), %r9
	sete	%dl
	cmpq	$0, -2576(%rbp)
	sete	%al
	orb	%al, %dl
	jne	.L302
	testq	%r12, %r12
	je	.L302
	testq	%r9, %r9
	je	.L310
.L295:
	movq	%r13, %rdx
	movq	%r9, %rdi
	movq	%r15, %rsi
	movq	%r9, -2584(%rbp)
	call	SRP_Calc_x@PLT
	movq	-2584(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L311
	movq	%r9, -2584(%rbp)
	call	BN_new@PLT
	movq	-2584(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	je	.L308
	movq	-2568(%rbp), %rcx
	movq	-2576(%rbp), %rsi
	movq	%r12, %r8
	movq	%r13, %rdx
	movq	%r9, -2584(%rbp)
	call	BN_mod_exp@PLT
	movq	-2584(%rbp), %r9
	testl	%eax, %eax
	je	.L312
	movq	%r9, (%r14)
	movq	%r13, %r14
	movl	$1, %r13d
	.p2align 4,,10
	.p2align 3
.L300:
	movq	%r14, %rdi
	call	BN_clear_free@PLT
	movq	%r12, %rdi
	call	BN_CTX_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L313
	addq	$2552, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L293:
	.cfi_restore_state
	xorl	%r13d, %r13d
	testq	%r14, %r14
	je	.L300
.L309:
	movq	(%r14), %r9
	xorl	%edi, %edi
	xorl	%r14d, %r14d
.L294:
	xorl	%r13d, %r13d
	cmpq	%rdi, %r9
	je	.L300
	call	BN_clear_free@PLT
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L302:
	xorl	%edi, %edi
	xorl	%r14d, %r14d
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L310:
	leaq	-2560(%rbp), %rdi
	movl	$20, %esi
	movq	%rdi, -2584(%rbp)
	call	RAND_bytes@PLT
	movq	-2584(%rbp), %rdi
	testl	%eax, %eax
	jle	.L309
	xorl	%edx, %edx
	movl	$20, %esi
	call	BN_bin2bn@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	jne	.L295
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L312:
	movq	(%rbx), %rdi
	movq	%r9, -2568(%rbp)
	call	BN_clear_free@PLT
	movq	-2568(%rbp), %r9
.L308:
	movq	%r9, %rdi
	movq	(%r14), %r9
	movq	%r13, %r14
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L311:
	movq	%r9, %rdi
	movq	(%r14), %r9
	xorl	%r14d, %r14d
	jmp	.L294
.L313:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE561:
	.size	SRP_create_verifier_BN, .-SRP_create_verifier_BN
	.section	.rodata.str1.1
.LC2:
	.string	"*"
	.text
	.p2align 4
	.globl	SRP_create_verifier
	.type	SRP_create_verifier, @function
SRP_create_verifier:
.LFB560:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$1000, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -5096(%rbp)
	movq	%rcx, -5112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -5088(%rbp)
	movq	$0, -5080(%rbp)
	testq	%rdi, %rdi
	je	.L318
	movq	%rsi, %r13
	testq	%rsi, %rsi
	je	.L318
	cmpq	$0, -5096(%rbp)
	je	.L318
	cmpq	$0, -5112(%rbp)
	je	.L318
	movq	%rdi, %r12
	movq	%r8, %rbx
	movq	%r9, %r15
	testq	%r8, %r8
	je	.L319
	leaq	-5072(%rbp), %r14
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	t_fromb64.constprop.0
	movl	%eax, %esi
	testl	%eax, %eax
	jle	.L318
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	BN_bin2bn@PLT
	movq	%rax, -5120(%rbp)
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L318
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	t_fromb64.constprop.0
	movl	%eax, %esi
	testl	%eax, %eax
	jle	.L326
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	BN_bin2bn@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L327
	movq	%rax, -5104(%rbp)
	leaq	.LC2(%rip), %rax
	movq	%rax, -5136(%rbp)
.L320:
	movq	-5096(%rbp), %rax
	movq	(%rax), %r14
	leaq	-2560(%rbp), %rax
	testq	%r14, %r14
	je	.L345
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -5128(%rbp)
	movq	%rax, %r14
	call	t_fromb64.constprop.0
	movl	%eax, %esi
	testl	%eax, %eax
	jle	.L332
	movq	%r14, %rdi
	xorl	%edx, %edx
	call	BN_bin2bn@PLT
	movq	%rax, -5088(%rbp)
	movq	%rax, %r14
.L322:
	testq	%r14, %r14
	je	.L330
	movq	-5120(%rbp), %r8
	movq	%r15, %r9
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-5080(%rbp), %rcx
	leaq	-5088(%rbp), %rdx
	call	SRP_create_verifier_BN
	testl	%eax, %eax
	je	.L332
	leaq	-5072(%rbp), %r12
	movq	-5080(%rbp), %rdi
	movq	%r12, %rsi
	call	BN_bn2bin@PLT
	testl	%eax, %eax
	js	.L332
	movq	-5080(%rbp), %rdi
	call	BN_num_bits@PLT
	movl	$639, %edx
	leaq	.LC1(%rip), %rsi
	leal	14(%rax), %r13d
	addl	$7, %eax
	cmovns	%eax, %r13d
	sarl	$3, %r13d
	addl	%r13d, %r13d
	movslq	%r13d, %r13
	movq	%r13, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L334
	movq	-5080(%rbp), %rdi
	call	BN_num_bits@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	leal	14(%rax), %edx
	addl	$7, %eax
	cmovns	%eax, %edx
	sarl	$3, %edx
	call	t_tob64
	testl	%eax, %eax
	je	.L334
	movq	-5096(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L346
.L323:
	movq	-5112(%rbp), %rax
	movq	-5136(%rbp), %r14
	movq	%r15, (%rax)
	xorl	%r15d, %r15d
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L319:
	movq	%r9, %rdi
	call	SRP_get_default_gN@PLT
	testq	%rax, %rax
	jne	.L347
	.p2align 4,,10
	.p2align 3
.L318:
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	movq	$0, -5104(%rbp)
.L317:
	movq	%rbx, %rdi
	call	BN_free@PLT
	movq	-5104(%rbp), %rdi
	call	BN_free@PLT
	movl	$664, %ecx
	movq	%r13, %rsi
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movq	-5088(%rbp), %rdi
	call	BN_clear_free@PLT
	movq	-5080(%rbp), %rdi
	call	BN_clear_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L348
	addq	$5096, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L347:
	.cfi_restore_state
	movq	16(%rax), %rcx
	movq	8(%rax), %r15
	movq	$0, -5104(%rbp)
	movq	(%rax), %rax
	movq	%rcx, -5120(%rbp)
	movq	%rax, -5136(%rbp)
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L332:
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L345:
	movl	$20, %esi
	movq	%rax, %rdi
	movq	%rax, -5128(%rbp)
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jle	.L330
	movq	-5128(%rbp), %rdi
	xorl	%edx, %edx
	movl	$20, %esi
	call	BN_bin2bn@PLT
	movq	%rax, -5088(%rbp)
	movq	%rax, %r14
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L330:
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L326:
	movq	-5120(%rbp), %rbx
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	movq	$0, -5104(%rbp)
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L334:
	xorl	%r14d, %r14d
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L327:
	movq	-5120(%rbp), %rbx
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	movq	$0, -5104(%rbp)
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L346:
	movl	$647, %edx
	leaq	.LC1(%rip), %rsi
	movl	$40, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L317
	movq	-5128(%rbp), %rsi
	movl	$20, %edx
	movq	%rax, %rdi
	call	t_tob64
	testl	%eax, %eax
	je	.L349
	movq	-5096(%rbp), %rax
	movq	%r14, (%rax)
	jmp	.L323
.L349:
	movq	%r14, %rdi
	movl	$651, %edx
	leaq	.LC1(%rip), %rsi
	xorl	%r14d, %r14d
	call	CRYPTO_free@PLT
	jmp	.L317
.L348:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE560:
	.size	SRP_create_verifier, .-SRP_create_verifier
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
