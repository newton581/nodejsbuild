	.file	"ec_cvt.c"
	.text
	.p2align 4
	.globl	EC_GROUP_new_curve_GFp
	.type	EC_GROUP_new_curve_GFp, @function
EC_GROUP_new_curve_GFp:
.LFB377:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$8, %rsp
	call	EC_GFp_mont_method@PLT
	movq	%rax, %rdi
	call	EC_GROUP_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	EC_GROUP_set_curve@PLT
	testl	%eax, %eax
	je	.L8
.L1:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	EC_GROUP_clear_free@PLT
	jmp	.L1
	.cfi_endproc
.LFE377:
	.size	EC_GROUP_new_curve_GFp, .-EC_GROUP_new_curve_GFp
	.p2align 4
	.globl	EC_GROUP_new_curve_GF2m
	.type	EC_GROUP_new_curve_GF2m, @function
EC_GROUP_new_curve_GF2m:
.LFB378:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$8, %rsp
	call	EC_GF2m_simple_method@PLT
	movq	%rax, %rdi
	call	EC_GROUP_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L9
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	EC_GROUP_set_curve@PLT
	testl	%eax, %eax
	je	.L15
.L9:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	EC_GROUP_clear_free@PLT
	jmp	.L9
	.cfi_endproc
.LFE378:
	.size	EC_GROUP_new_curve_GF2m, .-EC_GROUP_new_curve_GF2m
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
