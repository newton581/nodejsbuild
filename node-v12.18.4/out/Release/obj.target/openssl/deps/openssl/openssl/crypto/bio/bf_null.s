	.file	"bf_null.c"
	.text
	.p2align 4
	.type	nullf_callback_ctrl, @function
nullf_callback_ctrl:
.LFB271:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2
	jmp	BIO_callback_ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L2:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE271:
	.size	nullf_callback_ctrl, .-nullf_callback_ctrl
	.p2align 4
	.type	nullf_gets, @function
nullf_gets:
.LFB272:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L5
	jmp	BIO_gets@PLT
	.p2align 4,,10
	.p2align 3
.L5:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE272:
	.size	nullf_gets, .-nullf_gets
	.p2align 4
	.type	nullf_puts, @function
nullf_puts:
.LFB273:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L7
	jmp	BIO_puts@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE273:
	.size	nullf_puts, .-nullf_puts
	.p2align 4
	.type	nullf_read, @function
nullf_read:
.LFB268:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	testq	%rsi, %rsi
	je	.L10
	movq	%rdi, %r12
	movq	64(%rdi), %rdi
	xorl	%r13d, %r13d
	testq	%rdi, %rdi
	je	.L8
	call	BIO_read@PLT
	movq	%r12, %rdi
	movl	$15, %esi
	movl	%eax, %r13d
	call	BIO_clear_flags@PLT
	movq	%r12, %rdi
	call	BIO_copy_next_retry@PLT
.L8:
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	xorl	%r13d, %r13d
	popq	%r12
	movl	%r13d, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE268:
	.size	nullf_read, .-nullf_read
	.p2align 4
	.type	nullf_ctrl, @function
nullf_ctrl:
.LFB270:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L18
	cmpl	$12, %esi
	je	.L18
	cmpl	$101, %esi
	jne	.L16
	movq	%r12, %rdi
	movl	$15, %esi
	movq	%rcx, -32(%rbp)
	movq	%rdx, -24(%rbp)
	call	BIO_clear_flags@PLT
	movq	64(%r12), %rdi
	movq	-32(%rbp), %rcx
	movl	$101, %esi
	movq	-24(%rbp), %rdx
	call	BIO_ctrl@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	BIO_copy_next_retry@PLT
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	addq	$16, %rsp
	xorl	%r13d, %r13d
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE270:
	.size	nullf_ctrl, .-nullf_ctrl
	.p2align 4
	.type	nullf_write, @function
nullf_write:
.LFB269:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	testq	%rsi, %rsi
	je	.L22
	testl	%edx, %edx
	jle	.L22
	movq	%rdi, %r12
	movq	64(%rdi), %rdi
	xorl	%r13d, %r13d
	testq	%rdi, %rdi
	je	.L20
	call	BIO_write@PLT
	movq	%r12, %rdi
	movl	$15, %esi
	movl	%eax, %r13d
	call	BIO_clear_flags@PLT
	movq	%r12, %rdi
	call	BIO_copy_next_retry@PLT
.L20:
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	xorl	%r13d, %r13d
	popq	%r12
	movl	%r13d, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE269:
	.size	nullf_write, .-nullf_write
	.p2align 4
	.globl	BIO_f_null
	.type	BIO_f_null, @function
BIO_f_null:
.LFB267:
	.cfi_startproc
	endbr64
	leaq	methods_nullf(%rip), %rax
	ret
	.cfi_endproc
.LFE267:
	.size	BIO_f_null, .-BIO_f_null
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"NULL filter"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	methods_nullf, @object
	.size	methods_nullf, 96
methods_nullf:
	.long	529
	.zero	4
	.quad	.LC0
	.quad	bwrite_conv
	.quad	nullf_write
	.quad	bread_conv
	.quad	nullf_read
	.quad	nullf_puts
	.quad	nullf_gets
	.quad	nullf_ctrl
	.quad	0
	.quad	0
	.quad	nullf_callback_ctrl
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
