	.file	"md4_dgst.c"
	.text
	.p2align 4
	.globl	MD4_Init
	.type	MD4_Init, @function
MD4_Init:
.LFB154:
	.cfi_startproc
	endbr64
	movq	$0, 16(%rdi)
	movq	%rdi, %rdx
	leaq	24(%rdi), %rdi
	movdqa	.LC0(%rip), %xmm0
	movq	$0, 60(%rdi)
	movl	%edx, %eax
	andq	$-8, %rdi
	subl	%edi, %eax
	leal	92(%rax), %ecx
	xorl	%eax, %eax
	shrl	$3, %ecx
	rep stosq
	movups	%xmm0, (%rdx)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE154:
	.size	MD4_Init, .-MD4_Init
	.p2align 4
	.globl	md4_block_data_order
	.type	md4_block_data_order, @function
md4_block_data_order:
.LFB155:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -104(%rbp)
	movl	(%rdi), %edi
	movl	8(%rax), %r8d
	movl	%edi, -48(%rbp)
	movl	4(%rax), %edi
	movl	12(%rax), %eax
	movl	%eax, -44(%rbp)
	leaq	-1(%rdx), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	je	.L3
	movq	%rsi, %r12
	.p2align 4,,10
	.p2align 3
.L4:
	movl	-44(%rbp), %eax
	movl	(%r12), %edx
	movl	%edi, %ecx
	movl	4(%r12), %ebx
	xorl	%r8d, %ecx
	movl	8(%r12), %r11d
	movl	%eax, %esi
	movl	12(%r12), %r10d
	movl	16(%r12), %r13d
	movl	%edx, -60(%rbp)
	xorl	%r8d, %esi
	movl	%ebx, %r9d
	movl	%ebx, -64(%rbp)
	andl	%edi, %esi
	addl	%eax, %r9d
	movl	%r10d, -68(%rbp)
	addl	%edi, %r10d
	xorl	%eax, %esi
	movl	%ecx, %eax
	movl	%r13d, -72(%rbp)
	addl	%edx, %esi
	addl	-48(%rbp), %esi
	roll	$3, %esi
	andl	%esi, %eax
	movl	%esi, %r15d
	xorl	%r8d, %eax
	addl	%r9d, %eax
	movl	%eax, %ecx
	leal	(%r11,%r8), %eax
	roll	$7, %ecx
	movl	%ecx, %r9d
	movl	%esi, %ecx
	xorl	%edi, %ecx
	xorl	%r9d, %r15d
	andl	%r9d, %ecx
	xorl	%edi, %ecx
	addl	%eax, %ecx
	movl	%r15d, %eax
	roll	$11, %ecx
	andl	%ecx, %eax
	xorl	%esi, %eax
	addl	%r13d, %esi
	movl	32(%r12), %r13d
	addl	%r10d, %eax
	movl	%r9d, %r10d
	movl	%eax, %r15d
	xorl	%ecx, %r10d
	rorl	$13, %r15d
	movl	%r15d, %edx
	movl	20(%r12), %r15d
	andl	%edx, %r10d
	xorl	%r9d, %r10d
	leal	(%r9,%r15), %eax
	movl	%ecx, %r9d
	movl	%r15d, -76(%rbp)
	addl	%esi, %r10d
	xorl	%edx, %r9d
	movl	24(%r12), %esi
	roll	$3, %r10d
	andl	%r10d, %r9d
	movl	%esi, %r14d
	movl	%r10d, %ebx
	movl	28(%r12), %esi
	xorl	%ecx, %r9d
	movl	%r14d, -80(%rbp)
	addl	%eax, %r9d
	leal	(%rcx,%r14), %eax
	movl	%edx, %ecx
	movl	%esi, -84(%rbp)
	roll	$7, %r9d
	xorl	%r10d, %ecx
	andl	%r9d, %ecx
	xorl	%r9d, %ebx
	xorl	%edx, %ecx
	addl	%eax, %ecx
	leal	(%rdx,%rsi), %eax
	movl	%ebx, %edx
	movl	36(%r12), %ebx
	roll	$11, %ecx
	andl	%ecx, %edx
	movl	%ecx, %esi
	movl	%ebx, -88(%rbp)
	xorl	%r10d, %edx
	addl	%r13d, %r10d
	addl	%eax, %edx
	movl	%r9d, %eax
	rorl	$13, %edx
	xorl	%ecx, %eax
	andl	%edx, %eax
	xorl	%edx, %esi
	xorl	%r9d, %eax
	movl	%esi, %r14d
	addl	%ebx, %r9d
	movl	48(%r12), %ebx
	addl	%r10d, %eax
	movl	40(%r12), %r10d
	roll	$3, %eax
	andl	%eax, %r14d
	leal	(%rcx,%r10), %esi
	movl	%r10d, -92(%rbp)
	movl	56(%r12), %r10d
	xorl	%ecx, %r14d
	movl	%edx, %ecx
	addl	%r9d, %r14d
	xorl	%eax, %ecx
	movl	44(%r12), %r9d
	roll	$7, %r14d
	andl	%r14d, %ecx
	movl	%r9d, -96(%rbp)
	xorl	%edx, %ecx
	addl	%esi, %ecx
	leal	(%rdx,%r9), %esi
	movl	%eax, %edx
	movl	52(%r12), %r9d
	roll	$11, %ecx
	xorl	%r14d, %edx
	andl	%ecx, %edx
	xorl	%eax, %edx
	addl	%esi, %edx
	leal	(%rax,%rbx), %esi
	movl	%r14d, %eax
	addq	$64, %r12
	rorl	$13, %edx
	xorl	%ecx, %eax
	andl	%edx, %eax
	xorl	%r14d, %eax
	addl	%r9d, %r14d
	addl	%esi, %eax
	movl	%ecx, %esi
	roll	$3, %eax
	xorl	%edx, %esi
	andl	%eax, %esi
	xorl	%ecx, %esi
	addl	%r10d, %ecx
	addl	%r14d, %esi
	movl	%edx, %r14d
	roll	$7, %esi
	xorl	%eax, %r14d
	andl	%esi, %r14d
	xorl	%edx, %r14d
	addl	-4(%r12), %edx
	movl	%edx, %r15d
	movl	%eax, %edx
	addl	%r14d, %ecx
	xorl	%esi, %edx
	roll	$11, %ecx
	movl	%edx, %r14d
	andl	%ecx, %r14d
	xorl	%eax, %r14d
	leal	(%r14,%r15), %edx
	movl	-60(%rbp), %r15d
	rorl	$13, %edx
	leal	1518500249(%r15,%rax), %r14d
	movl	%esi, %eax
	movl	%esi, %r15d
	orl	%ecx, %eax
	andl	%ecx, %r15d
	andl	%edx, %eax
	orl	%r15d, %eax
	movl	-72(%rbp), %r15d
	addl	%r14d, %eax
	leal	1518500249(%r15,%rsi), %r14d
	movl	%ecx, %esi
	roll	$3, %eax
	movl	%ecx, %r15d
	orl	%edx, %esi
	andl	%eax, %esi
	andl	%edx, %r15d
	orl	%r15d, %esi
	movl	%edx, %r15d
	addl	%r14d, %esi
	leal	1518500249(%r13,%rcx), %r14d
	movl	%edx, %ecx
	andl	%eax, %r15d
	roll	$5, %esi
	orl	%eax, %ecx
	andl	%esi, %ecx
	orl	%r15d, %ecx
	movl	%eax, %r15d
	addl	%r14d, %ecx
	leal	1518500249(%rbx,%rdx), %r14d
	movl	%eax, %edx
	andl	%esi, %r15d
	roll	$9, %ecx
	orl	%esi, %edx
	andl	%ecx, %edx
	orl	%r15d, %edx
	movl	-64(%rbp), %r15d
	addl	%r14d, %edx
	leal	1518500249(%r15,%rax), %r14d
	movl	%esi, %eax
	roll	$13, %edx
	movl	%esi, %r15d
	orl	%ecx, %eax
	andl	%ecx, %r15d
	andl	%edx, %eax
	orl	%r15d, %eax
	movl	%ecx, %r15d
	addl	%r14d, %eax
	movl	-76(%rbp), %r14d
	andl	%edx, %r15d
	roll	$3, %eax
	leal	1518500249(%r14,%rsi), %r14d
	movl	%ecx, %esi
	orl	%edx, %esi
	andl	%eax, %esi
	orl	%r15d, %esi
	movl	-88(%rbp), %r15d
	addl	%r14d, %esi
	leal	1518500249(%r15,%rcx), %r14d
	movl	%edx, %ecx
	roll	$5, %esi
	movl	%edx, %r15d
	orl	%eax, %ecx
	andl	%eax, %r15d
	andl	%esi, %ecx
	orl	%r15d, %ecx
	movl	%eax, %r15d
	addl	%r14d, %ecx
	leal	1518500249(%r9,%rdx), %r14d
	movl	%eax, %edx
	andl	%esi, %r15d
	roll	$9, %ecx
	orl	%esi, %edx
	andl	%ecx, %edx
	orl	%r15d, %edx
	movl	%esi, %r15d
	addl	%r14d, %edx
	leal	1518500249(%r11,%rax), %r14d
	movl	%esi, %eax
	andl	%ecx, %r15d
	roll	$13, %edx
	orl	%ecx, %eax
	andl	%edx, %eax
	orl	%r15d, %eax
	movl	%ecx, %r15d
	addl	%r14d, %eax
	movl	-80(%rbp), %r14d
	andl	%edx, %r15d
	roll	$3, %eax
	leal	1518500249(%r14,%rsi), %r14d
	movl	%ecx, %esi
	orl	%edx, %esi
	andl	%eax, %esi
	orl	%r15d, %esi
	movl	%edx, %r15d
	addl	%r14d, %esi
	movl	-92(%rbp), %r14d
	andl	%eax, %r15d
	roll	$5, %esi
	leal	1518500249(%r14,%rcx), %r14d
	movl	%edx, %ecx
	orl	%eax, %ecx
	andl	%esi, %ecx
	orl	%r15d, %ecx
	movl	%eax, %r15d
	addl	%r14d, %ecx
	leal	1518500249(%r10,%rdx), %r14d
	movl	%eax, %edx
	andl	%esi, %r15d
	roll	$9, %ecx
	orl	%esi, %edx
	andl	%ecx, %edx
	orl	%r15d, %edx
	movl	%esi, %r15d
	addl	%r14d, %edx
	movl	-68(%rbp), %r14d
	roll	$13, %edx
	andl	%ecx, %r15d
	leal	1518500249(%r14,%rax), %r14d
	movl	%esi, %eax
	orl	%ecx, %eax
	andl	%edx, %eax
	orl	%r15d, %eax
	movl	%ecx, %r15d
	addl	%r14d, %eax
	movl	-84(%rbp), %r14d
	andl	%edx, %r15d
	roll	$3, %eax
	leal	1518500249(%r14,%rsi), %r14d
	movl	%ecx, %esi
	orl	%edx, %esi
	andl	%eax, %esi
	orl	%r15d, %esi
	movl	-96(%rbp), %r15d
	addl	%r14d, %esi
	leal	1518500249(%r15,%rcx), %r14d
	movl	%edx, %ecx
	roll	$5, %esi
	movl	%edx, %r15d
	orl	%eax, %ecx
	andl	%eax, %r15d
	leal	1859775393(%r13,%rsi), %r13d
	andl	%esi, %ecx
	orl	%r15d, %ecx
	movl	%eax, %r15d
	addl	%r14d, %ecx
	movl	-4(%r12), %r14d
	andl	%esi, %r15d
	roll	$9, %ecx
	leal	1518500249(%r14,%rdx), %r14d
	movl	%eax, %edx
	orl	%esi, %edx
	andl	%ecx, %edx
	orl	%r15d, %edx
	movl	-64(%rbp), %r15d
	addl	%r14d, %edx
	movl	-60(%rbp), %r14d
	roll	$13, %edx
	leal	1859775393(%r14,%rax), %r14d
	movl	%esi, %eax
	movl	%ecx, %esi
	xorl	%ecx, %eax
	xorl	%edx, %esi
	leal	1859775393(%rbx,%rdx), %ebx
	xorl	%edx, %eax
	addl	%r14d, %eax
	movl	-68(%rbp), %r14d
	roll	$3, %eax
	xorl	%eax, %esi
	addl	%r13d, %esi
	movl	-72(%rbp), %r13d
	roll	$9, %esi
	leal	1859775393(%r13,%rcx), %r13d
	movl	%edx, %ecx
	movl	%eax, %edx
	xorl	%eax, %ecx
	xorl	%esi, %edx
	xorl	%esi, %ecx
	addl	%r13d, %ecx
	movl	-92(%rbp), %r13d
	roll	$11, %ecx
	xorl	%ecx, %edx
	addl	%ebx, %edx
	leal	1859775393(%r11,%rax), %ebx
	movl	%esi, %r11d
	xorl	%ecx, %r11d
	roll	$15, %edx
	movl	%r11d, %eax
	leal	1859775393(%r13,%rsi), %r11d
	movl	%ecx, %esi
	xorl	%edx, %eax
	xorl	%edx, %esi
	leal	1859775393(%r10,%rdx), %r10d
	addl	%ebx, %eax
	roll	$3, %eax
	xorl	%eax, %esi
	addl	%r11d, %esi
	movl	-80(%rbp), %r11d
	roll	$9, %esi
	leal	1859775393(%r11,%rcx), %r11d
	movl	%edx, %ecx
	movl	%eax, %edx
	xorl	%eax, %ecx
	xorl	%esi, %edx
	xorl	%esi, %ecx
	addl	%r11d, %ecx
	movl	%esi, %r11d
	roll	$11, %ecx
	xorl	%ecx, %edx
	xorl	%ecx, %r11d
	addl	%r10d, %edx
	leal	1859775393(%r15,%rax), %r10d
	movl	%r11d, %eax
	movl	-88(%rbp), %r11d
	roll	$15, %edx
	movl	-76(%rbp), %r15d
	xorl	%edx, %eax
	leal	1859775393(%r9,%rdx), %r9d
	addl	%r10d, %eax
	leal	1859775393(%r11,%rsi), %r10d
	movl	%ecx, %esi
	roll	$3, %eax
	xorl	%edx, %esi
	xorl	%eax, %esi
	addl	%r10d, %esi
	leal	1859775393(%r15,%rcx), %r10d
	movl	%edx, %ecx
	movl	%eax, %edx
	roll	$9, %esi
	xorl	%eax, %ecx
	movl	-96(%rbp), %r15d
	subq	$1, -56(%rbp)
	xorl	%esi, %ecx
	xorl	%esi, %edx
	movl	%esi, %r11d
	addl	%r10d, %ecx
	roll	$11, %ecx
	xorl	%ecx, %edx
	xorl	%ecx, %r11d
	addl	%r9d, %edx
	leal	1859775393(%r14,%rax), %r9d
	movl	%r11d, %eax
	movl	-84(%rbp), %r14d
	roll	$15, %edx
	movl	-4(%r12), %r11d
	xorl	%edx, %eax
	addl	%r9d, %eax
	leal	1859775393(%r15,%rsi), %r9d
	movl	%ecx, %esi
	roll	$3, %eax
	xorl	%edx, %esi
	addl	%eax, -48(%rbp)
	movl	-48(%rbp), %ebx
	xorl	%eax, %esi
	addl	%r9d, %esi
	leal	1859775393(%r14,%rcx), %r9d
	movl	%edx, %ecx
	roll	$9, %esi
	xorl	%eax, %ecx
	addl	%esi, -44(%rbp)
	xorl	%esi, %ecx
	addl	%r9d, %ecx
	leal	1859775393(%r11,%rdx), %r9d
	movl	%eax, %edx
	movq	-104(%rbp), %r11
	roll	$11, %ecx
	xorl	%esi, %edx
	movl	-44(%rbp), %eax
	xorl	%ecx, %edx
	addl	%ecx, %r8d
	movl	%ebx, (%r11)
	addl	%r9d, %edx
	movl	%eax, 12(%r11)
	movq	-56(%rbp), %rax
	roll	$15, %edx
	movl	%r8d, 8(%r11)
	addl	%edx, %edi
	movl	%edi, 4(%r11)
	cmpq	$-1, %rax
	jne	.L4
.L3:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE155:
	.size	md4_block_data_order, .-md4_block_data_order
	.p2align 4
	.globl	MD4_Update
	.type	MD4_Update, @function
MD4_Update:
.LFB151:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L44
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	0(,%rdx,8), %eax
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	addl	16(%rdi), %eax
	movl	20(%rdi), %edx
	setc	%cl
	movl	%eax, 16(%rdi)
	movl	88(%rdi), %eax
	cmpl	$1, %ecx
	movq	%r12, %rcx
	sbbl	$-1, %edx
	shrq	$29, %rcx
	addl	%ecx, %edx
	movl	%edx, 20(%rdi)
	testq	%rax, %rax
	jne	.L47
	cmpq	$63, %r12
	ja	.L25
.L28:
	movl	%r14d, 88(%rbx)
	leaq	24(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
.L34:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	leaq	24(%rdi), %rcx
	leaq	(%r12,%rax), %r15
	leaq	(%rcx,%rax), %rdi
	cmpq	$63, %r12
	ja	.L17
	cmpq	$63, %r15
	jbe	.L18
.L17:
	movl	$64, %r12d
	subq	%rax, %r12
	cmpq	$8, %r12
	jnb	.L19
	testb	$4, %r12b
	jne	.L48
	testq	%r12, %r12
	je	.L20
	movzbl	0(%r13), %eax
	movb	%al, (%rdi)
	testb	$2, %r12b
	jne	.L49
.L20:
	movq	%rcx, %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%rcx, -56(%rbp)
	call	md4_block_data_order
	movq	-56(%rbp), %rcx
	pxor	%xmm0, %xmm0
	addq	%r12, %r13
	leaq	-64(%r15), %r12
	movl	$0, 88(%rbx)
	movups	%xmm0, 24(%rbx)
	movups	%xmm0, 16(%rcx)
	movups	%xmm0, 32(%rcx)
	movups	%xmm0, 48(%rcx)
	cmpq	$63, %r12
	jbe	.L26
.L25:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	shrq	$6, %rdx
	call	md4_block_data_order
	movq	%r12, %rax
	andq	$-64, %rax
	addq	%rax, %r13
	subq	%rax, %r12
.L26:
	testq	%r12, %r12
	je	.L34
	movl	%r12d, %r14d
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	0(%r13), %rax
	leaq	8(%rdi), %rdx
	movq	%r13, %r8
	andq	$-8, %rdx
	movq	%rax, (%rdi)
	movq	-8(%r13,%r12), %rax
	movq	%rax, -8(%rdi,%r12)
	subq	%rdx, %rdi
	subq	%rdi, %r8
	addq	%r12, %rdi
	andq	$-8, %rdi
	cmpq	$8, %rdi
	jb	.L20
	andq	$-8, %rdi
	xorl	%eax, %eax
.L23:
	movq	(%r8,%rax), %rsi
	movq	%rsi, (%rdx,%rax)
	addq	$8, %rax
	cmpq	%rdi, %rax
	jb	.L23
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L18:
	movq	%r12, %rdx
	call	memcpy@PLT
	addl	%r12d, 88(%rbx)
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L48:
	movl	0(%r13), %eax
	movl	%eax, (%rdi)
	movl	-4(%r13,%r12), %eax
	movl	%eax, -4(%rdi,%r12)
	jmp	.L20
.L49:
	movzwl	-2(%r13,%r12), %eax
	movw	%ax, -2(%rdi,%r12)
	jmp	.L20
	.cfi_endproc
.LFE151:
	.size	MD4_Update, .-MD4_Update
	.p2align 4
	.globl	MD4_Transform
	.type	MD4_Transform, @function
MD4_Transform:
.LFB152:
	.cfi_startproc
	endbr64
	movl	$1, %edx
	jmp	md4_block_data_order
	.cfi_endproc
.LFE152:
	.size	MD4_Transform, .-MD4_Transform
	.p2align 4
	.globl	MD4_Final
	.type	MD4_Final, @function
MD4_Final:
.LFB153:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	24(%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movl	88(%rsi), %eax
	movb	$-128, 24(%rsi,%rax)
	addq	$1, %rax
	leaq	0(%r13,%rax), %rcx
	cmpq	$56, %rax
	ja	.L52
	movl	$56, %edx
	subq	%rax, %rdx
	movq	%rdx, %rax
.L53:
	xorl	%edi, %edi
	cmpl	$8, %eax
	jnb	.L56
	testb	$4, %al
	jne	.L74
	testl	%eax, %eax
	je	.L57
	movb	$0, (%rcx)
	testb	$2, %al
	jne	.L75
.L57:
	movq	16(%rbx), %rax
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movl	$1, %edx
	movq	%rax, 80(%rbx)
	call	md4_block_data_order
	movl	$0, 88(%rbx)
	movq	%r13, %rdi
	movl	$64, %esi
	call	OPENSSL_cleanse@PLT
	movl	(%rbx), %eax
	movl	%eax, (%r12)
	movl	4(%rbx), %eax
	movl	%eax, 4(%r12)
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movl	12(%rbx), %eax
	movl	%eax, 12(%r12)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	leaq	8(%rcx), %rsi
	movl	%eax, %edx
	movq	$0, (%rcx)
	movq	$0, -8(%rcx,%rdx)
	andq	$-8, %rsi
	subq	%rsi, %rcx
	leal	(%rax,%rcx), %edx
	andl	$-8, %edx
	cmpl	$8, %edx
	jb	.L57
	andl	$-8, %edx
	xorl	%eax, %eax
.L60:
	movl	%eax, %ecx
	addl	$8, %eax
	movq	%rdi, (%rsi,%rcx)
	cmpl	%edx, %eax
	jb	.L60
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L52:
	movl	$64, %edx
	subq	%rax, %rdx
	je	.L55
	xorl	%eax, %eax
.L54:
	movb	$0, (%rcx,%rax)
	addq	$1, %rax
	cmpq	%rdx, %rax
	jb	.L54
.L55:
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	md4_block_data_order
	movq	%r13, %rcx
	movl	$56, %eax
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L74:
	movl	%eax, %eax
	movl	$0, (%rcx)
	movl	$0, -4(%rcx,%rax)
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L75:
	movl	%eax, %eax
	xorl	%edx, %edx
	movw	%dx, -2(%rcx,%rax)
	jmp	.L57
	.cfi_endproc
.LFE153:
	.size	MD4_Final, .-MD4_Final
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	1732584193
	.long	-271733879
	.long	-1732584194
	.long	271733878
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
