	.file	"evp_pkey.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/evp/evp_pkey.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"TYPE="
	.text
	.p2align 4
	.globl	EVP_PKCS82PKEY
	.type	EVP_PKCS82PKEY, @function
EVP_PKCS82PKEY:
.LFB829:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	leaq	-120(%rbp), %rdi
	movq	%r13, %r8
	.cfi_offset 12, -32
	xorl	%r12d, %r12d
	subq	$112, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	PKCS8_pkey_get0@PLT
	testl	%eax, %eax
	je	.L1
	call	EVP_PKEY_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L12
	movq	-120(%rbp), %rdi
	call	OBJ_obj2nid@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	EVP_PKEY_set_type@PLT
	testl	%eax, %eax
	je	.L13
	movq	16(%r12), %rax
	movq	64(%rax), %rax
	testq	%rax, %rax
	je	.L6
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L14
.L1:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L15
	addq	$112, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	movl	$36, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$118, %edx
	movl	$111, %esi
	movl	$6, %edi
	leaq	-112(%rbp), %r13
	call	ERR_put_error@PLT
	movq	-120(%rbp), %rdx
	movl	$80, %esi
	movq	%r13, %rdi
	call	i2t_ASN1_OBJECT@PLT
	movq	%r13, %rdx
	movl	$2, %edi
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rsi
	call	ERR_add_error_data@PLT
.L5:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	EVP_PKEY_free@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$31, %r8d
	movl	$65, %edx
	movl	$111, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L14:
	movl	$44, %r8d
	movl	$145, %edx
	movl	$111, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$48, %r8d
	movl	$144, %edx
	movl	$111, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L5
.L15:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE829:
	.size	EVP_PKCS82PKEY, .-EVP_PKCS82PKEY
	.p2align 4
	.globl	EVP_PKEY2PKCS8
	.type	EVP_PKEY2PKCS8, @function
EVP_PKEY2PKCS8:
.LFB830:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	call	PKCS8_PRIV_KEY_INFO_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L23
	movq	16(%r13), %rax
	testq	%rax, %rax
	je	.L19
	movq	72(%rax), %rax
	testq	%rax, %rax
	je	.L20
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L24
.L16:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movl	$72, %r8d
	movl	$146, %edx
	movl	$113, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L21:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	PKCS8_PRIV_KEY_INFO_free@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	movl	$65, %r8d
	movl	$65, %edx
	movl	$113, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L19:
	movl	$80, %r8d
	movl	$118, %edx
	movl	$113, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$76, %r8d
	movl	$144, %edx
	movl	$113, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L21
	.cfi_endproc
.LFE830:
	.size	EVP_PKEY2PKCS8, .-EVP_PKEY2PKCS8
	.p2align 4
	.globl	EVP_PKEY_get_attr_count
	.type	EVP_PKEY_get_attr_count, @function
EVP_PKEY_get_attr_count:
.LFB831:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rdi
	jmp	X509at_get_attr_count@PLT
	.cfi_endproc
.LFE831:
	.size	EVP_PKEY_get_attr_count, .-EVP_PKEY_get_attr_count
	.p2align 4
	.globl	EVP_PKEY_get_attr_by_NID
	.type	EVP_PKEY_get_attr_by_NID, @function
EVP_PKEY_get_attr_by_NID:
.LFB832:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rdi
	jmp	X509at_get_attr_by_NID@PLT
	.cfi_endproc
.LFE832:
	.size	EVP_PKEY_get_attr_by_NID, .-EVP_PKEY_get_attr_by_NID
	.p2align 4
	.globl	EVP_PKEY_get_attr_by_OBJ
	.type	EVP_PKEY_get_attr_by_OBJ, @function
EVP_PKEY_get_attr_by_OBJ:
.LFB833:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rdi
	jmp	X509at_get_attr_by_OBJ@PLT
	.cfi_endproc
.LFE833:
	.size	EVP_PKEY_get_attr_by_OBJ, .-EVP_PKEY_get_attr_by_OBJ
	.p2align 4
	.globl	EVP_PKEY_get_attr
	.type	EVP_PKEY_get_attr, @function
EVP_PKEY_get_attr:
.LFB834:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rdi
	jmp	X509at_get_attr@PLT
	.cfi_endproc
.LFE834:
	.size	EVP_PKEY_get_attr, .-EVP_PKEY_get_attr
	.p2align 4
	.globl	EVP_PKEY_delete_attr
	.type	EVP_PKEY_delete_attr, @function
EVP_PKEY_delete_attr:
.LFB835:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rdi
	jmp	X509at_delete_attr@PLT
	.cfi_endproc
.LFE835:
	.size	EVP_PKEY_delete_attr, .-EVP_PKEY_delete_attr
	.p2align 4
	.globl	EVP_PKEY_add1_attr
	.type	EVP_PKEY_add1_attr, @function
EVP_PKEY_add1_attr:
.LFB836:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$56, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509at_add1_attr@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE836:
	.size	EVP_PKEY_add1_attr, .-EVP_PKEY_add1_attr
	.p2align 4
	.globl	EVP_PKEY_add1_attr_by_OBJ
	.type	EVP_PKEY_add1_attr_by_OBJ, @function
EVP_PKEY_add1_attr_by_OBJ:
.LFB837:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$56, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509at_add1_attr_by_OBJ@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE837:
	.size	EVP_PKEY_add1_attr_by_OBJ, .-EVP_PKEY_add1_attr_by_OBJ
	.p2align 4
	.globl	EVP_PKEY_add1_attr_by_NID
	.type	EVP_PKEY_add1_attr_by_NID, @function
EVP_PKEY_add1_attr_by_NID:
.LFB838:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$56, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509at_add1_attr_by_NID@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE838:
	.size	EVP_PKEY_add1_attr_by_NID, .-EVP_PKEY_add1_attr_by_NID
	.p2align 4
	.globl	EVP_PKEY_add1_attr_by_txt
	.type	EVP_PKEY_add1_attr_by_txt, @function
EVP_PKEY_add1_attr_by_txt:
.LFB839:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$56, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509at_add1_attr_by_txt@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE839:
	.size	EVP_PKEY_add1_attr_by_txt, .-EVP_PKEY_add1_attr_by_txt
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
