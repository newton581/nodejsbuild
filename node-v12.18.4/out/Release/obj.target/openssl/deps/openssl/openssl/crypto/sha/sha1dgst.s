	.file	"sha1dgst.c"
	.text
	.p2align 4
	.globl	SHA1_Update
	.type	SHA1_Update, @function
SHA1_Update:
.LFB151:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L35
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	0(,%rdx,8), %eax
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	addl	20(%rdi), %eax
	movl	24(%rdi), %edx
	setc	%cl
	movl	%eax, 20(%rdi)
	movl	92(%rdi), %eax
	cmpl	$1, %ecx
	movq	%r12, %rcx
	sbbl	$-1, %edx
	shrq	$29, %rcx
	addl	%ecx, %edx
	movl	%edx, 24(%rdi)
	testq	%rax, %rax
	jne	.L39
	cmpq	$63, %r12
	ja	.L16
.L19:
	movl	%r14d, 92(%rbx)
	leaq	28(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
.L25:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	leaq	28(%rdi), %rcx
	leaq	(%r12,%rax), %r15
	leaq	(%rcx,%rax), %rdi
	cmpq	$63, %r12
	ja	.L8
	cmpq	$63, %r15
	jbe	.L9
.L8:
	movl	$64, %r12d
	subq	%rax, %r12
	cmpq	$8, %r12
	jnb	.L10
	testb	$4, %r12b
	jne	.L40
	testq	%r12, %r12
	je	.L11
	movzbl	0(%r13), %eax
	movb	%al, (%rdi)
	testb	$2, %r12b
	jne	.L41
.L11:
	movq	%rcx, %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%rcx, -56(%rbp)
	call	sha1_block_data_order@PLT
	movq	-56(%rbp), %rcx
	pxor	%xmm0, %xmm0
	addq	%r12, %r13
	leaq	-64(%r15), %r12
	movl	$0, 92(%rbx)
	movups	%xmm0, 28(%rbx)
	movups	%xmm0, 16(%rcx)
	movups	%xmm0, 32(%rcx)
	movups	%xmm0, 48(%rcx)
	cmpq	$63, %r12
	jbe	.L17
.L16:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	shrq	$6, %rdx
	call	sha1_block_data_order@PLT
	movq	%r12, %rax
	andq	$-64, %rax
	addq	%rax, %r13
	subq	%rax, %r12
.L17:
	testq	%r12, %r12
	je	.L25
	movl	%r12d, %r14d
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	0(%r13), %rax
	leaq	8(%rdi), %rdx
	movq	%r13, %r8
	andq	$-8, %rdx
	movq	%rax, (%rdi)
	movq	-8(%r13,%r12), %rax
	movq	%rax, -8(%rdi,%r12)
	subq	%rdx, %rdi
	subq	%rdi, %r8
	addq	%r12, %rdi
	andq	$-8, %rdi
	cmpq	$8, %rdi
	jb	.L11
	andq	$-8, %rdi
	xorl	%eax, %eax
.L14:
	movq	(%r8,%rax), %rsi
	movq	%rsi, (%rdx,%rax)
	addq	$8, %rax
	cmpq	%rdi, %rax
	jb	.L14
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%r12, %rdx
	call	memcpy@PLT
	addl	%r12d, 92(%rbx)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L40:
	movl	0(%r13), %eax
	movl	%eax, (%rdi)
	movl	-4(%r13,%r12), %eax
	movl	%eax, -4(%rdi,%r12)
	jmp	.L11
.L41:
	movzwl	-2(%r13,%r12), %eax
	movw	%ax, -2(%rdi,%r12)
	jmp	.L11
	.cfi_endproc
.LFE151:
	.size	SHA1_Update, .-SHA1_Update
	.p2align 4
	.globl	SHA1_Transform
	.type	SHA1_Transform, @function
SHA1_Transform:
.LFB152:
	.cfi_startproc
	endbr64
	movl	$1, %edx
	jmp	sha1_block_data_order@PLT
	.cfi_endproc
.LFE152:
	.size	SHA1_Transform, .-SHA1_Transform
	.p2align 4
	.globl	SHA1_Final
	.type	SHA1_Final, @function
SHA1_Final:
.LFB153:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	28(%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movl	92(%rsi), %eax
	movb	$-128, 28(%rsi,%rax)
	addq	$1, %rax
	leaq	0(%r13,%rax), %rcx
	cmpq	$56, %rax
	ja	.L44
	movl	$56, %edx
	subq	%rax, %rdx
	movq	%rdx, %rax
.L45:
	xorl	%edi, %edi
	cmpl	$8, %eax
	jnb	.L48
	testb	$4, %al
	jne	.L66
	testl	%eax, %eax
	je	.L49
	movb	$0, (%rcx)
	testb	$2, %al
	jne	.L67
.L49:
	movq	20(%rbx), %rax
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movl	$1, %edx
	bswap	%rax
	movq	%rax, 84(%rbx)
	call	sha1_block_data_order@PLT
	movl	$0, 92(%rbx)
	movq	%r13, %rdi
	movl	$64, %esi
	call	OPENSSL_cleanse@PLT
	movl	(%rbx), %eax
	bswap	%eax
	movl	%eax, (%r12)
	movl	4(%rbx), %eax
	bswap	%eax
	movl	%eax, 4(%r12)
	movl	8(%rbx), %eax
	bswap	%eax
	movl	%eax, 8(%r12)
	movl	12(%rbx), %eax
	bswap	%eax
	movl	%eax, 12(%r12)
	movl	16(%rbx), %eax
	bswap	%eax
	movl	%eax, 16(%r12)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	leaq	8(%rcx), %rsi
	movl	%eax, %edx
	movq	$0, (%rcx)
	movq	$0, -8(%rcx,%rdx)
	andq	$-8, %rsi
	subq	%rsi, %rcx
	leal	(%rax,%rcx), %edx
	andl	$-8, %edx
	cmpl	$8, %edx
	jb	.L49
	andl	$-8, %edx
	xorl	%eax, %eax
.L52:
	movl	%eax, %ecx
	addl	$8, %eax
	movq	%rdi, (%rsi,%rcx)
	cmpl	%edx, %eax
	jb	.L52
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L44:
	movl	$64, %edx
	subq	%rax, %rdx
	je	.L47
	xorl	%eax, %eax
.L46:
	movb	$0, (%rcx,%rax)
	addq	$1, %rax
	cmpq	%rdx, %rax
	jb	.L46
.L47:
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	sha1_block_data_order@PLT
	movq	%r13, %rcx
	movl	$56, %eax
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L66:
	movl	%eax, %eax
	movl	$0, (%rcx)
	movl	$0, -4(%rcx,%rax)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L67:
	movl	%eax, %eax
	xorl	%edx, %edx
	movw	%dx, -2(%rcx,%rax)
	jmp	.L49
	.cfi_endproc
.LFE153:
	.size	SHA1_Final, .-SHA1_Final
	.p2align 4
	.globl	SHA1_Init
	.type	SHA1_Init, @function
SHA1_Init:
.LFB154:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1, %eax
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm0, 64(%rdi)
	movups	%xmm0, 80(%rdi)
	movdqa	.LC0(%rip), %xmm0
	movl	$-1009589776, 16(%rdi)
	movups	%xmm0, (%rdi)
	ret
	.cfi_endproc
.LFE154:
	.size	SHA1_Init, .-SHA1_Init
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	1732584193
	.long	-271733879
	.long	-1732584194
	.long	271733878
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
