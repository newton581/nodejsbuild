	.file	"ocsp_lib.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ocsp/ocsp_lib.c"
	.text
	.p2align 4
	.globl	OCSP_cert_id_new
	.type	OCSP_cert_id_new, @function
OCSP_cert_id_new:
.LFB1417:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%rdx, -152(%rbp)
	movq	%rcx, -160(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	OCSP_CERTID_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L3
	movq	(%rax), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	%rbx, %rdi
	call	EVP_MD_type@PLT
	movl	%eax, %edi
	testl	%eax, %eax
	je	.L32
	call	OBJ_nid2obj@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L3
	call	ASN1_TYPE_new@PLT
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L3
	movl	$5, (%rax)
	leaq	-128(%rbp), %r15
	movq	%rbx, %rsi
	movq	%r14, %rdi
	leaq	-132(%rbp), %r13
	movq	%r15, %rdx
	movq	%r13, %rcx
	call	X509_NAME_digest@PLT
	testl	%eax, %eax
	je	.L33
	movl	-132(%rbp), %edx
	leaq	16(%r12), %rdi
	movq	%r15, %rsi
	call	ASN1_OCTET_STRING_set@PLT
	testl	%eax, %eax
	je	.L3
	movq	-152(%rbp), %rax
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	%r15, %rdx
	movslq	(%rax), %rsi
	movq	8(%rax), %rdi
	call	EVP_Digest@PLT
	testl	%eax, %eax
	je	.L3
	movl	-132(%rbp), %edx
	leaq	40(%r12), %rdi
	movq	%r15, %rsi
	call	ASN1_OCTET_STRING_set@PLT
	testl	%eax, %eax
	je	.L3
	movq	-160(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L1
	leaq	64(%r12), %rdi
	call	ASN1_STRING_copy@PLT
	testl	%eax, %eax
	jne	.L1
	.p2align 4,,10
	.p2align 3
.L3:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	OCSP_CERTID_free@PLT
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L34
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movl	$58, %r8d
	movl	$120, %edx
	movl	$101, %esi
	movl	$39, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L33:
	movl	$85, %r8d
	movl	$102, %edx
	movl	$101, %esi
	movl	$39, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L3
.L34:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1417:
	.size	OCSP_cert_id_new, .-OCSP_cert_id_new
	.p2align 4
	.globl	OCSP_cert_to_id
	.type	OCSP_cert_to_id, @function
OCSP_cert_to_id:
.LFB1416:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	testq	%rdi, %rdi
	je	.L41
.L36:
	testq	%r12, %r12
	je	.L37
	movq	%r12, %rdi
	call	X509_get_issuer_name@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	X509_get0_serialNumber@PLT
	movq	%rax, %r12
.L38:
	movq	%r14, %rdi
	call	X509_get0_pubkey_bitstr@PLT
	movq	%r12, %rcx
	movq	%r15, %rsi
	popq	%r12
	movq	%r13, %rdi
	movq	%rax, %rdx
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	OCSP_cert_id_new
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	movq	%r14, %rdi
	call	X509_get_subject_name@PLT
	movq	%rax, %r15
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L41:
	call	EVP_sha1@PLT
	movq	%rax, %r13
	jmp	.L36
	.cfi_endproc
.LFE1416:
	.size	OCSP_cert_to_id, .-OCSP_cert_to_id
	.p2align 4
	.globl	OCSP_id_issuer_cmp
	.type	OCSP_id_issuer_cmp, @function
OCSP_id_issuer_cmp:
.LFB1418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rsi), %rsi
	movq	(%rdi), %rdi
	call	OBJ_cmp@PLT
	testl	%eax, %eax
	je	.L45
.L42:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	leaq	16(%r12), %rsi
	leaq	16(%rbx), %rdi
	call	ASN1_OCTET_STRING_cmp@PLT
	testl	%eax, %eax
	jne	.L42
	leaq	40(%r12), %rsi
	leaq	40(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	ASN1_OCTET_STRING_cmp@PLT
	.cfi_endproc
.LFE1418:
	.size	OCSP_id_issuer_cmp, .-OCSP_id_issuer_cmp
	.p2align 4
	.globl	OCSP_id_cmp
	.type	OCSP_id_cmp, @function
OCSP_id_cmp:
.LFB1419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rsi), %rsi
	movq	(%rdi), %rdi
	call	OBJ_cmp@PLT
	testl	%eax, %eax
	je	.L49
.L46:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	leaq	16(%r12), %rsi
	leaq	16(%rbx), %rdi
	call	ASN1_OCTET_STRING_cmp@PLT
	testl	%eax, %eax
	jne	.L46
	leaq	40(%r12), %rsi
	leaq	40(%rbx), %rdi
	call	ASN1_OCTET_STRING_cmp@PLT
	testl	%eax, %eax
	jne	.L46
	leaq	64(%r12), %rsi
	leaq	64(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	ASN1_INTEGER_cmp@PLT
	.cfi_endproc
.LFE1419:
	.size	OCSP_id_cmp, .-OCSP_id_cmp
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"80"
.LC2:
	.string	"443"
.LC3:
	.string	"http"
.LC4:
	.string	"https"
.LC5:
	.string	"/"
	.text
	.p2align 4
	.globl	OCSP_parse_url
	.type	OCSP_parse_url, @function
OCSP_parse_url:
.LFB1420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$40, %rsp
	movq	$0, (%rsi)
	leaq	.LC0(%rip), %rsi
	movq	$0, (%rdx)
	movl	$129, %edx
	movq	$0, (%rcx)
	call	CRYPTO_strdup@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L51
	movl	$58, %esi
	movq	%rax, %rdi
	call	strchr@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L52
	movb	$0, (%rax)
	movl	$5, %ecx
	leaq	.LC3(%rip), %rdi
	movq	%r15, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L84
	movl	$6, %ecx
	leaq	.LC4(%rip), %rdi
	movq	%r15, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L52
	movl	$1, (%r14)
	leaq	.LC2(%rip), %r14
.L54:
	cmpb	$47, 1(%r8)
	jne	.L52
	cmpb	$47, 2(%r8)
	jne	.L52
	leaq	3(%r8), %r10
	movl	$47, %esi
	movq	%r8, -64(%rbp)
	movq	%r10, %rdi
	movq	%r10, -56(%rbp)
	call	strchr@PLT
	movq	-56(%rbp), %r10
	movq	-64(%rbp), %r8
	testq	%rax, %rax
	je	.L85
	movl	$165, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	CRYPTO_strdup@PLT
	movq	-56(%rbp), %rcx
	movq	-72(%rbp), %r8
	movq	%rax, (%rbx)
	movq	-64(%rbp), %r10
	movb	$0, (%rcx)
	movq	(%rbx), %rax
.L56:
	testq	%rax, %rax
	je	.L51
	cmpb	$91, 3(%r8)
	movq	%r10, -56(%rbp)
	je	.L86
.L57:
	movl	$58, %esi
	movq	%r10, %rdi
	call	strchr@PLT
	testq	%rax, %rax
	je	.L58
	movb	$0, (%rax)
	leaq	1(%rax), %r14
.L58:
	movl	$190, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_strdup@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L51
	movq	-56(%rbp), %rdi
	movl	$194, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	je	.L51
	movl	$199, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	call	CRYPTO_free@PLT
	movl	$1, %eax
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L52:
	movl	$208, %r8d
	movl	$121, %edx
	movl	$114, %esi
	movl	$39, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L60:
	movl	$211, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	call	CRYPTO_free@PLT
	movq	(%rbx), %rdi
	movl	$212, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, (%rbx)
	movq	(%r12), %rdi
	movl	$214, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, (%r12)
	movq	0(%r13), %rdi
	movl	$216, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 0(%r13)
	xorl	%eax, %eax
.L50:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	movl	$0, (%r14)
	leaq	.LC1(%rip), %r14
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L51:
	movl	$204, %r8d
	movl	$65, %edx
	movl	$114, %esi
	movl	$39, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L85:
	movl	$163, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	call	CRYPTO_strdup@PLT
	movq	-56(%rbp), %r10
	movq	-64(%rbp), %r8
	movq	%rax, (%rbx)
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L86:
	leaq	4(%r8), %rax
	movl	$93, %esi
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	strchr@PLT
	testq	%rax, %rax
	je	.L52
	movb	$0, (%rax)
	leaq	1(%rax), %r10
	jmp	.L57
	.cfi_endproc
.LFE1420:
	.size	OCSP_parse_url, .-OCSP_parse_url
	.p2align 4
	.globl	OCSP_CERTID_dup
	.type	OCSP_CERTID_dup, @function
OCSP_CERTID_dup:
.LFB1421:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	leaq	OCSP_CERTID_it(%rip), %rdi
	jmp	ASN1_item_dup@PLT
	.cfi_endproc
.LFE1421:
	.size	OCSP_CERTID_dup, .-OCSP_CERTID_dup
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
