	.file	"ecp_smpl.c"
	.text
	.p2align 4
	.globl	ec_GFp_simple_group_init
	.type	ec_GFp_simple_group_init, @function
ec_GFp_simple_group_init:
.LFB378:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	BN_new@PLT
	movq	%rax, 64(%rbx)
	call	BN_new@PLT
	movq	%rax, 96(%rbx)
	call	BN_new@PLT
	movq	64(%rbx), %rdi
	movq	%rax, 104(%rbx)
	testq	%rdi, %rdi
	je	.L2
	cmpq	$0, 96(%rbx)
	je	.L2
	testq	%rax, %rax
	je	.L2
	movl	$0, 112(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	call	BN_free@PLT
	movq	96(%rbx), %rdi
	call	BN_free@PLT
	movq	104(%rbx), %rdi
	call	BN_free@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE378:
	.size	ec_GFp_simple_group_init, .-ec_GFp_simple_group_init
	.p2align 4
	.globl	ec_GFp_simple_group_finish
	.type	ec_GFp_simple_group_finish, @function
ec_GFp_simple_group_finish:
.LFB379:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	64(%rdi), %rdi
	call	BN_free@PLT
	movq	96(%rbx), %rdi
	call	BN_free@PLT
	movq	104(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	BN_free@PLT
	.cfi_endproc
.LFE379:
	.size	ec_GFp_simple_group_finish, .-ec_GFp_simple_group_finish
	.p2align 4
	.globl	ec_GFp_simple_point_init
	.type	ec_GFp_simple_point_init, @function
ec_GFp_simple_point_init:
.LFB386:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	BN_new@PLT
	movq	%rax, 16(%rbx)
	call	BN_new@PLT
	movq	%rax, 24(%rbx)
	call	BN_new@PLT
	movq	16(%rbx), %rdi
	movl	$0, 40(%rbx)
	movq	%rax, 32(%rbx)
	testq	%rdi, %rdi
	je	.L14
	cmpq	$0, 24(%rbx)
	je	.L14
	movl	$1, %r8d
	testq	%rax, %rax
	je	.L14
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	call	BN_free@PLT
	movq	24(%rbx), %rdi
	call	BN_free@PLT
	movq	32(%rbx), %rdi
	call	BN_free@PLT
	addq	$8, %rsp
	xorl	%r8d, %r8d
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE386:
	.size	ec_GFp_simple_point_init, .-ec_GFp_simple_point_init
	.p2align 4
	.globl	ec_GFp_simple_point_finish
	.type	ec_GFp_simple_point_finish, @function
ec_GFp_simple_point_finish:
.LFB387:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	call	BN_free@PLT
	movq	24(%rbx), %rdi
	call	BN_free@PLT
	movq	32(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	BN_free@PLT
	.cfi_endproc
.LFE387:
	.size	ec_GFp_simple_point_finish, .-ec_GFp_simple_point_finish
	.p2align 4
	.globl	ec_GFp_simple_group_clear_finish
	.type	ec_GFp_simple_group_clear_finish, @function
ec_GFp_simple_group_clear_finish:
.LFB380:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	64(%rdi), %rdi
	call	BN_clear_free@PLT
	movq	96(%rbx), %rdi
	call	BN_clear_free@PLT
	movq	104(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	BN_clear_free@PLT
	.cfi_endproc
.LFE380:
	.size	ec_GFp_simple_group_clear_finish, .-ec_GFp_simple_group_clear_finish
	.p2align 4
	.globl	ec_GFp_simple_point_clear_finish
	.type	ec_GFp_simple_point_clear_finish, @function
ec_GFp_simple_point_clear_finish:
.LFB388:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	call	BN_clear_free@PLT
	movq	24(%rbx), %rdi
	call	BN_clear_free@PLT
	movq	32(%rbx), %rdi
	call	BN_clear_free@PLT
	movl	$0, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE388:
	.size	ec_GFp_simple_point_clear_finish, .-ec_GFp_simple_point_clear_finish
	.p2align 4
	.globl	ec_GFp_simple_group_get_degree
	.type	ec_GFp_simple_group_get_degree, @function
ec_GFp_simple_group_get_degree:
.LFB384:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rdi
	jmp	BN_num_bits@PLT
	.cfi_endproc
.LFE384:
	.size	ec_GFp_simple_group_get_degree, .-ec_GFp_simple_group_get_degree
	.p2align 4
	.globl	ec_GFp_simple_group_get_curve
	.type	ec_GFp_simple_group_get_curve, @function
ec_GFp_simple_group_get_curve:
.LFB383:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	testq	%rsi, %rsi
	je	.L34
	movq	%rsi, %rdi
	movq	64(%r12), %rsi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L39
.L34:
	movq	%r13, %rax
	orq	%r14, %rax
	je	.L33
	movq	(%r12), %rax
	movq	304(%rax), %rax
	testq	%rax, %rax
	je	.L37
	xorl	%ebx, %ebx
	testq	%r15, %r15
	je	.L57
.L38:
	testq	%r13, %r13
	je	.L40
	movq	%r13, %rsi
	movq	96(%r12), %rdx
	movq	%r15, %rcx
	movq	%r12, %rdi
	call	*%rax
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L36
	movl	$1, %r13d
	testq	%r14, %r14
	je	.L36
	movq	(%r12), %rax
	movq	304(%rax), %rax
.L40:
	movq	104(%r12), %rdx
	movq	%r15, %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*%rax
	xorl	%r13d, %r13d
	testl	%eax, %eax
	setne	%r13b
.L36:
	movq	%rbx, %rdi
	call	BN_CTX_free@PLT
.L30:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L42
	movq	96(%r12), %rsi
	movq	%r13, %rdi
	call	BN_copy@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L45
	testq	%r14, %r14
	jne	.L42
.L33:
	xorl	%ebx, %ebx
	movl	$1, %r13d
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L39:
	xorl	%r13d, %r13d
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L42:
	movq	104(%r12), %rsi
	movq	%r14, %rdi
	xorl	%r13d, %r13d
	call	BN_copy@PLT
	testq	%rax, %rax
	setne	%r13b
	xorl	%ebx, %ebx
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L57:
	call	BN_CTX_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L39
	movq	(%r12), %rax
	movq	%r15, %rbx
	movq	304(%rax), %rax
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L45:
	xorl	%r13d, %r13d
	jmp	.L36
	.cfi_endproc
.LFE383:
	.size	ec_GFp_simple_group_get_curve, .-ec_GFp_simple_group_get_curve
	.p2align 4
	.globl	ec_GFp_simple_get_Jprojective_coordinates_GFp
	.type	ec_GFp_simple_get_Jprojective_coordinates_GFp, @function
ec_GFp_simple_get_Jprojective_coordinates_GFp:
.LFB392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rax
	cmpq	$0, 304(%rax)
	je	.L59
	movq	%rdi, %r12
	movq	%r9, %r13
	xorl	%r8d, %r8d
	testq	%r9, %r9
	je	.L87
.L60:
	testq	%r14, %r14
	je	.L66
	movq	(%r12), %rax
	movq	%r10, -64(%rbp)
	movq	%r14, %rsi
	movq	%r13, %rcx
	movq	%r8, -56(%rbp)
	movq	16(%rbx), %rdx
	movq	%r12, %rdi
	call	*304(%rax)
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %r10
	testl	%eax, %eax
	movl	%eax, %r14d
	je	.L65
.L66:
	testq	%r10, %r10
	je	.L64
	movq	(%r12), %rax
	movq	%r8, -56(%rbp)
	movq	%r13, %rcx
	movq	%r10, %rsi
	movq	24(%rbx), %rdx
	movq	%r12, %rdi
	call	*304(%rax)
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	movl	%eax, %r14d
	je	.L65
.L64:
	testq	%r15, %r15
	je	.L86
	movq	(%r12), %rax
	movq	%r8, -56(%rbp)
	movq	%r13, %rcx
	movq	%r15, %rsi
	movq	32(%rbx), %rdx
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	*304(%rax)
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	setne	%r14b
.L65:
	movq	%r8, %rdi
	call	BN_CTX_free@PLT
.L58:
	addq	$24, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	testq	%rdx, %rdx
	je	.L71
	movq	16(%rsi), %rsi
	movq	%rdx, %rdi
	movq	%rcx, -56(%rbp)
	call	BN_copy@PLT
	movq	-56(%rbp), %r10
	testq	%rax, %rax
	je	.L73
.L71:
	testq	%r10, %r10
	je	.L70
	movq	24(%rbx), %rsi
	movq	%r10, %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L73
.L70:
	testq	%r15, %r15
	je	.L88
	movq	32(%rbx), %rsi
	movq	%r15, %rdi
	xorl	%r14d, %r14d
	call	BN_copy@PLT
	testq	%rax, %rax
	setne	%r14b
	xorl	%r8d, %r8d
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L88:
	xorl	%r8d, %r8d
.L86:
	movl	$1, %r14d
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L73:
	xorl	%r14d, %r14d
	xorl	%r8d, %r8d
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L87:
	movq	%rcx, -56(%rbp)
	call	BN_CTX_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L75
	movq	-56(%rbp), %r10
	movq	%rax, %r8
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L75:
	xorl	%r14d, %r14d
	jmp	.L58
	.cfi_endproc
.LFE392:
	.size	ec_GFp_simple_get_Jprojective_coordinates_GFp, .-ec_GFp_simple_get_Jprojective_coordinates_GFp
	.p2align 4
	.globl	ec_GFp_simple_is_at_infinity
	.type	ec_GFp_simple_is_at_infinity, @function
ec_GFp_simple_is_at_infinity:
.LFB398:
	.cfi_startproc
	endbr64
	movq	32(%rsi), %rdi
	jmp	BN_is_zero@PLT
	.cfi_endproc
.LFE398:
	.size	ec_GFp_simple_is_at_infinity, .-ec_GFp_simple_is_at_infinity
	.p2align 4
	.globl	ec_GFp_simple_field_sqr
	.type	ec_GFp_simple_field_sqr, @function
ec_GFp_simple_field_sqr:
.LFB404:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	movq	64(%r8), %rdx
	jmp	BN_mod_sqr@PLT
	.cfi_endproc
.LFE404:
	.size	ec_GFp_simple_field_sqr, .-ec_GFp_simple_field_sqr
	.p2align 4
	.globl	ec_GFp_simple_field_mul
	.type	ec_GFp_simple_field_mul, @function
ec_GFp_simple_field_mul:
.LFB403:
	.cfi_startproc
	endbr64
	movq	%rdi, %r9
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	movq	64(%r9), %rcx
	jmp	BN_mod_mul@PLT
	.cfi_endproc
.LFE403:
	.size	ec_GFp_simple_field_mul, .-ec_GFp_simple_field_mul
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ec/ecp_smpl.c"
	.text
	.p2align 4
	.globl	ec_GFp_simple_group_check_discriminant
	.type	ec_GFp_simple_group_check_discriminant, @function
ec_GFp_simple_group_check_discriminant:
.LFB385:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	64(%rdi), %rax
	movq	%rax, -64(%rbp)
	testq	%rsi, %rsi
	je	.L133
.L93:
	movq	%r13, %rdi
	call	BN_CTX_start@PLT
	movq	%r13, %rdi
	call	BN_CTX_get@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	BN_CTX_get@PLT
	movq	%r13, %rdi
	movq	%rax, -56(%rbp)
	call	BN_CTX_get@PLT
	movq	%r13, %rdi
	movq	%rax, -72(%rbp)
	call	BN_CTX_get@PLT
	movq	%r13, %rdi
	movq	%rax, -80(%rbp)
	call	BN_CTX_get@PLT
	testq	%rax, %rax
	je	.L101
	movq	(%rbx), %rax
	movq	96(%rbx), %rdx
	movq	304(%rax), %rax
	testq	%rax, %rax
	je	.L95
	movq	%r13, %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	*%rax
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L94
	movq	(%rbx), %rax
	movq	104(%rbx), %rdx
	movq	%r13, %rcx
	movq	%rbx, %rdi
	movq	-56(%rbp), %rsi
	call	*304(%rax)
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L94
.L98:
	movq	%r12, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L96
	movq	-56(%rbp), %rdi
	movl	$1, %r14d
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L94
	movq	-72(%rbp), %rbx
	movq	-64(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	BN_mod_sqr@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	jne	.L134
.L94:
	movq	%r13, %rdi
	call	BN_CTX_end@PLT
	movq	%r15, %rdi
	call	BN_CTX_free@PLT
	addq	$40, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore_state
	call	BN_CTX_new@PLT
	movq	%rax, %r13
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L93
	movl	$253, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r14d, %r14d
	movl	$165, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L96:
	movq	-56(%rbp), %rdi
	xorl	%r14d, %r14d
	call	BN_is_zero@PLT
	testl	%eax, %eax
	sete	%r14b
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L101:
	xorl	%r14d, %r14d
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L95:
	movq	%rdx, %rsi
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L94
	movq	104(%rbx), %rsi
	movq	-56(%rbp), %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	jne	.L98
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L134:
	movq	-64(%rbp), %rcx
	movq	-80(%rbp), %rdi
	movq	%r13, %r8
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	BN_mod_mul@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L94
	movq	-80(%rbp), %rsi
	movl	$2, %edx
	movq	%rbx, %rdi
	call	BN_lshift@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L94
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r13, %rcx
	movq	-80(%rbp), %rdi
	call	BN_mod_sqr@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L94
	movq	-80(%rbp), %rdi
	movl	$27, %esi
	call	BN_mul_word@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L94
	movq	-64(%rbp), %rcx
	movq	-80(%rbp), %rdx
	movq	%r13, %r8
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	BN_mod_add@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L94
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	BN_is_zero@PLT
	testl	%eax, %eax
	sete	%r14b
	jmp	.L94
	.cfi_endproc
.LFE385:
	.size	ec_GFp_simple_group_check_discriminant, .-ec_GFp_simple_group_check_discriminant
	.p2align 4
	.globl	ec_GFp_simple_point_set_to_infinity
	.type	ec_GFp_simple_point_set_to_infinity, @function
ec_GFp_simple_point_set_to_infinity:
.LFB390:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	32(%rsi), %rdi
	movl	$0, 40(%rsi)
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	BN_set_word@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE390:
	.size	ec_GFp_simple_point_set_to_infinity, .-ec_GFp_simple_point_set_to_infinity
	.p2align 4
	.globl	ec_GFp_simple_set_Jprojective_coordinates_GFp
	.type	ec_GFp_simple_set_Jprojective_coordinates_GFp, @function
ec_GFp_simple_set_Jprojective_coordinates_GFp:
.LFB391:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	subq	$24, %rsp
	testq	%r9, %r9
	je	.L186
.L138:
	testq	%rsi, %rsi
	je	.L145
	movq	64(%r13), %rdx
	movq	16(%rbx), %rdi
	movq	%r12, %rcx
	movq	%r10, -56(%rbp)
	call	BN_nnmod@PLT
	movq	-56(%rbp), %r10
	testl	%eax, %eax
	je	.L146
	movq	0(%r13), %rax
	movq	296(%rax), %rax
	testq	%rax, %rax
	je	.L145
	movq	16(%rbx), %rsi
	movq	%r10, -56(%rbp)
	movq	%r12, %rcx
	movq	%r13, %rdi
	movq	%rsi, %rdx
	call	*%rax
	movq	-56(%rbp), %r10
	testl	%eax, %eax
	je	.L146
.L145:
	testq	%r10, %r10
	je	.L142
	movq	64(%r13), %rdx
	movq	24(%rbx), %rdi
	movq	%r12, %rcx
	movq	%r10, %rsi
	call	BN_nnmod@PLT
	testl	%eax, %eax
	je	.L146
	movq	0(%r13), %rax
	movq	296(%rax), %rax
	testq	%rax, %rax
	je	.L142
	movq	24(%rbx), %rsi
	movq	%r12, %rcx
	movq	%r13, %rdi
	movq	%rsi, %rdx
	call	*%rax
	testl	%eax, %eax
	je	.L146
.L142:
	testq	%r14, %r14
	je	.L185
	movq	64(%r13), %rdx
	movq	32(%rbx), %rdi
	movq	%r12, %rcx
	movq	%r14, %rsi
	call	BN_nnmod@PLT
	testl	%eax, %eax
	jne	.L187
.L146:
	xorl	%r12d, %r12d
.L144:
	movq	%r15, %rdi
	call	BN_CTX_free@PLT
.L137:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L187:
	.cfi_restore_state
	movq	32(%rbx), %rdi
	call	BN_is_one@PLT
	movq	0(%r13), %rdx
	movl	%eax, %r14d
	movq	296(%rdx), %rax
	testq	%rax, %rax
	je	.L150
	movq	32(%rbx), %rsi
	testl	%r14d, %r14d
	je	.L149
	movq	312(%rdx), %rcx
	testq	%rcx, %rcx
	je	.L149
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	*%rcx
	testl	%eax, %eax
	je	.L146
.L150:
	movl	%r14d, 40(%rbx)
.L185:
	movl	$1, %r12d
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L149:
	movq	%r12, %rcx
	movq	%rsi, %rdx
	movq	%r13, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L150
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L186:
	movq	%rcx, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	BN_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L152
	movq	-64(%rbp), %r10
	movq	-56(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L152:
	xorl	%r12d, %r12d
	jmp	.L137
	.cfi_endproc
.LFE391:
	.size	ec_GFp_simple_set_Jprojective_coordinates_GFp, .-ec_GFp_simple_set_Jprojective_coordinates_GFp
	.p2align 4
	.globl	ec_GFp_simple_point_get_affine_coordinates
	.type	ec_GFp_simple_point_get_affine_coordinates, @function
ec_GFp_simple_point_get_affine_coordinates:
.LFB394:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%rdx, -64(%rbp)
	movq	%rcx, -72(%rbp)
	call	EC_POINT_is_at_infinity@PLT
	testl	%eax, %eax
	jne	.L243
	movq	$0, -56(%rbp)
	movl	%eax, %r13d
	testq	%r12, %r12
	je	.L244
.L191:
	movq	%r12, %rdi
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -88(%rbp)
	call	BN_CTX_get@PLT
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L193
	movq	(%r15), %rax
	movq	32(%rbx), %rdx
	movq	304(%rax), %rax
	testq	%rax, %rax
	je	.L214
	movq	%r12, %rcx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L193
.L194:
	movq	%r14, %rdi
	call	BN_is_one@PLT
	testl	%eax, %eax
	movq	(%r15), %rax
	je	.L196
	movq	304(%rax), %rax
	testq	%rax, %rax
	je	.L197
	cmpq	$0, -64(%rbp)
	je	.L201
	movq	16(%rbx), %rdx
	movq	-64(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r15, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L193
.L201:
	cmpq	$0, -72(%rbp)
	je	.L200
	movq	(%r15), %rax
	movq	24(%rbx), %rdx
	movq	%r12, %rcx
	movq	%r15, %rdi
	movq	-72(%rbp), %rsi
	xorl	%r13d, %r13d
	call	*304(%rax)
	testl	%eax, %eax
	setne	%r13b
	.p2align 4,,10
	.p2align 3
.L193:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	movq	-56(%rbp), %rdi
	call	BN_CTX_free@PLT
.L188:
	addq	$56, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L244:
	.cfi_restore_state
	call	BN_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L188
	movq	%rax, -56(%rbp)
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L196:
	movq	-80(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	*288(%rax)
	testl	%eax, %eax
	je	.L245
	movq	(%r15), %rax
	cmpq	$0, 296(%rax)
	je	.L246
	movq	64(%r15), %rdx
	movq	-80(%rbp), %rsi
	movq	%r12, %rcx
	movq	-88(%rbp), %rdi
	call	BN_mod_sqr@PLT
	testl	%eax, %eax
	je	.L193
.L209:
	cmpq	$0, -64(%rbp)
	je	.L208
	movq	(%r15), %rax
	movq	16(%rbx), %rdx
	movq	%r12, %r8
	movq	%r15, %rdi
	movq	-88(%rbp), %rcx
	movq	-64(%rbp), %rsi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L193
.L208:
	cmpq	$0, -72(%rbp)
	je	.L200
	movq	(%r15), %rax
	cmpq	$0, 296(%rax)
	je	.L247
	movq	64(%r15), %rcx
	movq	-80(%rbp), %rdx
	movq	%r12, %r8
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %rdi
	call	BN_mod_mul@PLT
	testl	%eax, %eax
	je	.L193
.L212:
	movq	(%r15), %rax
	movq	24(%rbx), %rdx
	movq	%r12, %r8
	movq	%r15, %rdi
	movq	-96(%rbp), %rcx
	movq	-72(%rbp), %rsi
	xorl	%r13d, %r13d
	call	*264(%rax)
	testl	%eax, %eax
	setne	%r13b
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L243:
	movl	$506, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$106, %edx
	xorl	%r13d, %r13d
	movl	$167, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L214:
	movq	%rdx, %r14
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L197:
	cmpq	$0, -64(%rbp)
	je	.L204
	movq	16(%rbx), %rsi
	movq	-64(%rbp), %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L193
.L204:
	cmpq	$0, -72(%rbp)
	je	.L200
	movq	24(%rbx), %rsi
	movq	-72(%rbp), %rdi
	xorl	%r13d, %r13d
	call	BN_copy@PLT
	testq	%rax, %rax
	setne	%r13b
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L200:
	movl	$1, %r13d
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L246:
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r15, %rdi
	call	*272(%rax)
	testl	%eax, %eax
	jne	.L209
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L245:
	movl	$557, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$3, %edx
	xorl	%r13d, %r13d
	movl	$167, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L247:
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rdx
	movq	%r12, %r8
	movq	%r15, %rdi
	movq	-96(%rbp), %rsi
	call	*264(%rax)
	testl	%eax, %eax
	jne	.L212
	jmp	.L193
	.cfi_endproc
.LFE394:
	.size	ec_GFp_simple_point_get_affine_coordinates, .-ec_GFp_simple_point_get_affine_coordinates
	.p2align 4
	.globl	ec_GFp_simple_dbl
	.type	ec_GFp_simple_dbl, @function
ec_GFp_simple_dbl:
.LFB396:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$72, %rsp
	movq	%rsi, -88(%rbp)
	movq	%rdx, %rsi
	call	EC_POINT_is_at_infinity@PLT
	testl	%eax, %eax
	jne	.L359
	movl	%eax, %r13d
	movq	(%r15), %rax
	movq	$0, -56(%rbp)
	movq	264(%rax), %rsi
	movq	272(%rax), %rax
	movq	%rax, -72(%rbp)
	movq	64(%r15), %rax
	movq	%rsi, -96(%rbp)
	movq	%rax, -80(%rbp)
	testq	%r12, %r12
	je	.L360
.L251:
	movq	%r12, %rdi
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	BN_CTX_get@PLT
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L253
	movl	40(%rbx), %esi
	testl	%esi, %esi
	je	.L254
	movq	16(%rbx), %rdx
	movq	-72(%rbp), %r9
	movq	%r12, %rcx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	*%r9
	testl	%eax, %eax
	jne	.L361
.L253:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	movq	-56(%rbp), %rdi
	call	BN_CTX_free@PLT
.L248:
	addq	$72, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L254:
	.cfi_restore_state
	movl	112(%r15), %eax
	testl	%eax, %eax
	je	.L258
	movq	32(%rbx), %rdx
	movq	-64(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r15, %rdi
	movq	-72(%rbp), %rax
	call	*%rax
	testl	%eax, %eax
	je	.L253
	movq	16(%rbx), %rsi
	movq	-80(%rbp), %rcx
	movq	%r14, %rdi
	movq	-64(%rbp), %rdx
	call	BN_mod_add_quick@PLT
	testl	%eax, %eax
	je	.L253
	movq	16(%rbx), %rsi
	movq	-80(%rbp), %rcx
	movq	-64(%rbp), %rdx
	movq	-104(%rbp), %rdi
	call	BN_mod_sub_quick@PLT
	testl	%eax, %eax
	je	.L253
	movq	-104(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	%r12, %r8
	movq	%r14, %rdx
	movq	-96(%rbp), %rax
	movq	%r15, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L253
	movq	-80(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	BN_mod_lshift1_quick@PLT
	testl	%eax, %eax
	je	.L253
	movq	-64(%rbp), %rdx
	movq	-80(%rbp), %rcx
	movq	%r14, %rsi
	movq	%rdx, %rdi
	call	BN_mod_add_quick@PLT
	testl	%eax, %eax
	je	.L253
	.p2align 4,,10
	.p2align 3
.L259:
	movl	40(%rbx), %ecx
	movq	24(%rbx), %rdx
	testl	%ecx, %ecx
	jne	.L362
	movq	32(%rbx), %rcx
	movq	-96(%rbp), %rax
	movq	%r12, %r8
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L253
.L261:
	movq	-88(%rbp), %rax
	movq	-80(%rbp), %rdx
	movq	%r14, %rsi
	movq	32(%rax), %rdi
	call	BN_mod_lshift1_quick@PLT
	testl	%eax, %eax
	je	.L253
	movq	-88(%rbp), %rax
	movq	24(%rbx), %rdx
	movq	%r12, %rcx
	movq	%r15, %rdi
	movq	-112(%rbp), %rsi
	movq	-72(%rbp), %r10
	movl	$0, 40(%rax)
	call	*%r10
	testl	%eax, %eax
	je	.L253
	movq	16(%rbx), %rdx
	movq	-112(%rbp), %rbx
	movq	%r12, %r8
	movq	%r15, %rdi
	movq	-104(%rbp), %rsi
	movq	-96(%rbp), %r11
	movq	%rbx, %rcx
	call	*%r11
	testl	%eax, %eax
	je	.L253
	movq	-104(%rbp), %rsi
	movq	-80(%rbp), %rcx
	movl	$2, %edx
	movq	%rsi, %rdi
	call	BN_mod_lshift_quick@PLT
	testl	%eax, %eax
	je	.L253
	movq	-80(%rbp), %rdx
	movq	-104(%rbp), %rsi
	movq	%r14, %rdi
	call	BN_mod_lshift1_quick@PLT
	testl	%eax, %eax
	je	.L253
	movq	-88(%rbp), %rax
	movq	-64(%rbp), %rdx
	movq	%r12, %rcx
	movq	%r15, %rdi
	movq	-72(%rbp), %r10
	movq	16(%rax), %rsi
	call	*%r10
	testl	%eax, %eax
	je	.L253
	movq	-88(%rbp), %rax
	movq	-80(%rbp), %rcx
	movq	%r14, %rdx
	movq	16(%rax), %rdi
	movq	%rdi, %rsi
	call	BN_mod_sub_quick@PLT
	testl	%eax, %eax
	je	.L253
	movq	-72(%rbp), %r10
	movq	%r12, %rcx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	*%r10
	testl	%eax, %eax
	je	.L253
	movq	-80(%rbp), %rcx
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	BN_mod_lshift_quick@PLT
	testl	%eax, %eax
	je	.L253
	movq	-88(%rbp), %rax
	movq	-80(%rbp), %rcx
	movq	%r14, %rdi
	movq	-104(%rbp), %rsi
	movq	16(%rax), %rdx
	call	BN_mod_sub_quick@PLT
	testl	%eax, %eax
	je	.L253
	movq	-64(%rbp), %rdx
	movq	-96(%rbp), %r11
	movq	%r12, %r8
	movq	%r14, %rcx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	*%r11
	testl	%eax, %eax
	je	.L253
	movq	-88(%rbp), %rax
	movq	-80(%rbp), %rcx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	xorl	%r13d, %r13d
	movq	24(%rax), %rdi
	call	BN_mod_sub_quick@PLT
	testl	%eax, %eax
	setne	%r13b
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L360:
	call	BN_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L248
	movq	%rax, -56(%rbp)
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L359:
	movq	-88(%rbp), %rbx
	xorl	%esi, %esi
	movl	$1, %r13d
	movq	32(%rbx), %rdi
	call	BN_set_word@PLT
	movl	$0, 40(%rbx)
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L258:
	movq	16(%rbx), %rdx
	movq	-72(%rbp), %rax
	movq	%r12, %rcx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L253
	movq	-80(%rbp), %rdx
	movq	-64(%rbp), %rdi
	movq	%r14, %rsi
	call	BN_mod_lshift1_quick@PLT
	testl	%eax, %eax
	je	.L253
	movq	-80(%rbp), %rcx
	movq	-64(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	BN_mod_add_quick@PLT
	testl	%eax, %eax
	je	.L253
	movq	32(%rbx), %rdx
	movq	-64(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r15, %rdi
	movq	-72(%rbp), %rax
	call	*%rax
	testl	%eax, %eax
	je	.L253
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rax
	movq	%r12, %rcx
	movq	%r15, %rdi
	movq	%rdx, %rsi
	call	*%rax
	testl	%eax, %eax
	je	.L253
	movq	-64(%rbp), %rdx
	movq	96(%r15), %rcx
	movq	%r12, %r8
	movq	%r15, %rdi
	movq	-96(%rbp), %rax
	movq	%rdx, %rsi
	call	*%rax
	testl	%eax, %eax
	je	.L253
	movq	-64(%rbp), %rsi
	movq	-80(%rbp), %rcx
	movq	%r14, %rdx
	movq	%rsi, %rdi
	call	BN_mod_add_quick@PLT
	testl	%eax, %eax
	jne	.L259
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L361:
	movq	-80(%rbp), %rdx
	movq	-64(%rbp), %rdi
	movq	%r14, %rsi
	call	BN_mod_lshift1_quick@PLT
	testl	%eax, %eax
	je	.L253
	movq	-80(%rbp), %rcx
	movq	-64(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	BN_mod_add_quick@PLT
	testl	%eax, %eax
	je	.L253
	movq	96(%r15), %rdx
	movq	-80(%rbp), %rcx
	movq	%r14, %rsi
	movq	-64(%rbp), %rdi
	call	BN_mod_add_quick@PLT
	testl	%eax, %eax
	jne	.L259
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L362:
	movq	%rdx, %rsi
	movq	%r14, %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	jne	.L261
	jmp	.L253
	.cfi_endproc
.LFE396:
	.size	ec_GFp_simple_dbl, .-ec_GFp_simple_dbl
	.p2align 4
	.globl	ec_GFp_simple_ladder_step
	.type	ec_GFp_simple_ladder_step, @function
ec_GFp_simple_ladder_step:
.LFB408:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%r8, %rdi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	%rcx, -104(%rbp)
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -88(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	BN_CTX_get@PLT
	testq	%rax, %rax
	je	.L366
	movq	%rax, %r13
	movq	(%r15), %rax
	movq	16(%r14), %rcx
	movq	%r12, %r8
	movq	16(%rbx), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L366
	movq	(%r15), %rax
	movq	32(%r14), %rcx
	movq	%r12, %r8
	movq	%r15, %rdi
	movq	32(%rbx), %rdx
	movq	-56(%rbp), %rsi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L366
	movq	(%r15), %rax
	movq	32(%r14), %rcx
	movq	%r12, %r8
	movq	%r15, %rdi
	movq	16(%rbx), %rdx
	movq	-64(%rbp), %rsi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L366
	movq	(%r15), %rax
	movq	16(%r14), %rcx
	movq	%r12, %r8
	movq	%r15, %rdi
	movq	32(%rbx), %rdx
	movq	-80(%rbp), %rsi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L366
	movq	(%r15), %rax
	movq	96(%r15), %rdx
	movq	%r12, %r8
	movq	%r15, %rdi
	movq	-56(%rbp), %rcx
	movq	-72(%rbp), %rsi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L366
	movq	-72(%rbp), %rdx
	movq	64(%r15), %rcx
	movq	%r13, %rsi
	movq	%rdx, %rdi
	call	BN_mod_add_quick@PLT
	testl	%eax, %eax
	jne	.L470
	.p2align 4,,10
	.p2align 3
.L366:
	xorl	%r13d, %r13d
.L365:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	addq	$72, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L470:
	.cfi_restore_state
	movq	64(%r15), %rcx
	movq	-64(%rbp), %rdx
	movq	%r13, %rdi
	movq	-80(%rbp), %rsi
	call	BN_mod_add_quick@PLT
	testl	%eax, %eax
	je	.L366
	movq	-72(%rbp), %r9
	movq	(%r15), %rax
	movq	%r12, %r8
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%r9, %rcx
	movq	%r9, %rsi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L366
	movq	-56(%rbp), %rdi
	movq	(%r15), %rax
	movq	%r12, %rcx
	movq	%rdi, %rdx
	movq	%rdi, %rsi
	movq	%r15, %rdi
	call	*272(%rax)
	testl	%eax, %eax
	je	.L366
	movq	64(%r15), %rcx
	movq	104(%r15), %rsi
	movl	$2, %edx
	movq	-88(%rbp), %rdi
	call	BN_mod_lshift_quick@PLT
	testl	%eax, %eax
	je	.L366
	movq	-56(%rbp), %rcx
	movq	(%r15), %rax
	movq	%r12, %r8
	movq	%r15, %rdi
	movq	-88(%rbp), %rdx
	movq	%rcx, %rsi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L366
	movq	-72(%rbp), %rax
	movq	64(%r15), %rdx
	movq	%rax, %rsi
	movq	%rax, %rdi
	call	BN_mod_lshift1_quick@PLT
	testl	%eax, %eax
	je	.L366
	movq	-80(%rbp), %r10
	movq	64(%r15), %rcx
	movq	-64(%rbp), %rsi
	movq	%r10, %rdx
	movq	%r10, %rdi
	call	BN_mod_sub_quick@PLT
	testl	%eax, %eax
	je	.L366
	movq	(%r15), %rax
	movq	32(%r14), %rsi
	movq	%r12, %rcx
	movq	%r15, %rdi
	movq	-80(%rbp), %rdx
	call	*272(%rax)
	testl	%eax, %eax
	je	.L366
	movq	-104(%rbp), %r10
	movq	(%r15), %rax
	movq	%r12, %r8
	movq	%r15, %rdi
	movq	32(%r14), %rdx
	movq	-64(%rbp), %rsi
	movq	16(%r10), %rcx
	call	*264(%rax)
	testl	%eax, %eax
	je	.L366
	movq	-56(%rbp), %rax
	movq	64(%r15), %rcx
	movq	-72(%rbp), %rdx
	movq	%rax, %rsi
	movq	%rax, %rdi
	call	BN_mod_add_quick@PLT
	testl	%eax, %eax
	je	.L366
	movq	16(%r14), %rdi
	movq	-56(%rbp), %r14
	movq	64(%r15), %rcx
	movq	-64(%rbp), %rdx
	movq	%r14, %rsi
	call	BN_mod_sub_quick@PLT
	testl	%eax, %eax
	je	.L366
	movq	(%r15), %rax
	movq	16(%rbx), %rdx
	movq	%r12, %rcx
	movq	%r15, %rdi
	movq	-64(%rbp), %rsi
	call	*272(%rax)
	testl	%eax, %eax
	je	.L366
	movq	(%r15), %rax
	movq	32(%rbx), %rdx
	movq	%r12, %rcx
	movq	%r15, %rdi
	movq	-72(%rbp), %rsi
	call	*272(%rax)
	testl	%eax, %eax
	je	.L366
	movq	(%r15), %rax
	movq	96(%r15), %rcx
	movq	%r12, %r8
	movq	%r13, %rsi
	movq	-72(%rbp), %rdx
	movq	%r15, %rdi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L366
	movq	64(%r15), %rcx
	movq	32(%rbx), %rdx
	movq	16(%rbx), %rsi
	movq	-96(%rbp), %rdi
	call	BN_mod_add_quick@PLT
	testl	%eax, %eax
	je	.L366
	movq	-96(%rbp), %rsi
	movq	(%r15), %rax
	movq	%r12, %rcx
	movq	%r15, %rdi
	movq	%rsi, %rdx
	call	*272(%rax)
	testl	%eax, %eax
	je	.L366
	movq	-96(%rbp), %rsi
	movq	64(%r15), %rcx
	movq	-64(%rbp), %rdx
	movq	%rsi, %rdi
	call	BN_mod_sub_quick@PLT
	testl	%eax, %eax
	je	.L366
	movq	-96(%rbp), %rsi
	movq	64(%r15), %rcx
	movq	-72(%rbp), %rdx
	movq	%rsi, %rdi
	call	BN_mod_sub_quick@PLT
	testl	%eax, %eax
	je	.L366
	movq	64(%r15), %rcx
	movq	-64(%rbp), %rsi
	movq	%r13, %rdx
	movq	-80(%rbp), %rdi
	call	BN_mod_sub_quick@PLT
	testl	%eax, %eax
	je	.L366
	movq	-80(%rbp), %r11
	movq	(%r15), %rax
	movq	%r12, %rcx
	movq	%r15, %rdi
	movq	%r11, %rdx
	movq	%r11, %rsi
	call	*272(%rax)
	testl	%eax, %eax
	je	.L366
	movq	(%r15), %rax
	movq	-96(%rbp), %rcx
	movq	%r12, %r8
	movq	%r14, %rsi
	movq	-72(%rbp), %rdx
	movq	%r15, %rdi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L366
	movq	(%r15), %rax
	movq	-88(%rbp), %rdx
	movq	%r12, %r8
	movq	%r14, %rcx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L366
	movq	64(%r15), %rcx
	movq	16(%rbx), %rdi
	movq	%r14, %rdx
	movq	-80(%rbp), %rsi
	call	BN_mod_sub_quick@PLT
	testl	%eax, %eax
	je	.L366
	movq	-64(%rbp), %r14
	movq	%r13, %rdx
	movq	-80(%rbp), %r13
	movq	64(%r15), %rcx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	BN_mod_add_quick@PLT
	testl	%eax, %eax
	je	.L366
	movq	(%r15), %rax
	movq	-72(%rbp), %rdx
	movq	%r12, %rcx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	*272(%rax)
	testl	%eax, %eax
	je	.L366
	movq	(%r15), %rax
	movq	-88(%rbp), %rcx
	movq	%r12, %r8
	movq	%r14, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L366
	movq	%r13, %rcx
	movq	(%r15), %rax
	movq	-96(%rbp), %r13
	movq	%r12, %r8
	movq	%r15, %rdi
	movq	%r13, %rdx
	movq	%r13, %rsi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L366
	movq	64(%r15), %rdx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	BN_mod_lshift1_quick@PLT
	testl	%eax, %eax
	je	.L366
	movq	64(%r15), %rcx
	movq	32(%rbx), %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	xorl	%r13d, %r13d
	call	BN_mod_add_quick@PLT
	testl	%eax, %eax
	setne	%r13b
	jmp	.L365
	.cfi_endproc
.LFE408:
	.size	ec_GFp_simple_ladder_step, .-ec_GFp_simple_ladder_step
	.p2align 4
	.globl	ec_GFp_simple_ladder_pre
	.type	ec_GFp_simple_ladder_pre, @function
ec_GFp_simple_ladder_pre:
.LFB407:
	.cfi_startproc
	endbr64
	movl	40(%rcx), %eax
	testl	%eax, %eax
	je	.L537
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	movq	32(%rdx), %rax
	movq	32(%rsi), %rcx
	movq	24(%rdx), %rdi
	movq	16(%rdx), %rsi
	movq	%rax, -80(%rbp)
	movq	16(%r12), %rax
	movq	%rcx, -88(%rbp)
	movq	16(%r14), %rdx
	movq	%r8, %rcx
	movq	%rax, -56(%rbp)
	movq	(%r15), %rax
	movq	%rdi, -72(%rbp)
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	*272(%rax)
	testl	%eax, %eax
	jne	.L538
.L474:
	xorl	%eax, %eax
.L471:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L538:
	.cfi_restore_state
	movq	64(%r15), %rcx
	movq	96(%r15), %rdx
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	BN_mod_sub_quick@PLT
	testl	%eax, %eax
	je	.L474
	movq	-56(%rbp), %rdx
	movq	(%r15), %rax
	movq	%r13, %rcx
	movq	%r15, %rdi
	movq	%rdx, %rsi
	call	*272(%rax)
	testl	%eax, %eax
	je	.L474
	movq	(%r15), %rax
	movq	104(%r15), %rcx
	movq	%r13, %r8
	movq	%r15, %rdi
	movq	16(%r14), %rdx
	movq	-72(%rbp), %rsi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L474
	movq	-72(%rbp), %rdi
	movq	64(%r15), %rcx
	movl	$3, %edx
	movq	%rdi, %rsi
	call	BN_mod_lshift_quick@PLT
	testl	%eax, %eax
	je	.L474
	movq	64(%r15), %rcx
	movq	16(%r12), %rdi
	movq	-72(%rbp), %rdx
	movq	-56(%rbp), %rsi
	call	BN_mod_sub_quick@PLT
	testl	%eax, %eax
	je	.L474
	movq	64(%r15), %rcx
	movq	96(%r15), %rdx
	movq	-64(%rbp), %rsi
	movq	-80(%rbp), %rdi
	call	BN_mod_add_quick@PLT
	testl	%eax, %eax
	je	.L474
	movq	(%r15), %rax
	movq	16(%r14), %rdx
	movq	%r13, %r8
	movq	%r15, %rdi
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rsi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L474
	movq	-88(%rbp), %rdx
	movq	64(%r15), %rcx
	movq	104(%r15), %rsi
	movq	%rdx, %rdi
	call	BN_mod_add_quick@PLT
	testl	%eax, %eax
	je	.L474
	movq	64(%r15), %rcx
	movq	32(%r12), %rdi
	movl	$2, %edx
	movq	-88(%rbp), %rsi
	call	BN_mod_lshift_quick@PLT
	testl	%eax, %eax
	jne	.L475
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L539:
	movq	24(%r12), %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L476
.L475:
	movq	64(%r15), %rsi
	movq	24(%r12), %rdi
	call	BN_priv_rand_range@PLT
	testl	%eax, %eax
	jne	.L539
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L537:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	xorl	%eax, %eax
	ret
.L541:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	32(%rbx), %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L540
.L476:
	movq	64(%r15), %rsi
	movq	32(%rbx), %rdi
	call	BN_priv_rand_range@PLT
	testl	%eax, %eax
	jne	.L541
	jmp	.L474
.L540:
	movq	(%r15), %rax
	movq	296(%rax), %r8
	testq	%r8, %r8
	je	.L479
	movq	24(%r12), %rsi
	movq	%r13, %rcx
	movq	%r15, %rdi
	movq	%rsi, %rdx
	call	*%r8
	testl	%eax, %eax
	je	.L474
	movq	32(%rbx), %rsi
	movq	(%r15), %rax
	movq	%r13, %rcx
	movq	%r15, %rdi
	movq	%rsi, %rdx
	call	*296(%rax)
	testl	%eax, %eax
	je	.L474
	movq	(%r15), %rax
.L479:
	movq	32(%r12), %rsi
	movq	24(%r12), %rcx
	movq	%r13, %r8
	movq	%r15, %rdi
	movq	%rsi, %rdx
	call	*264(%rax)
	testl	%eax, %eax
	je	.L474
	movq	16(%r12), %rsi
	movq	(%r15), %rax
	movq	%r13, %r8
	movq	%r15, %rdi
	movq	24(%r12), %rcx
	movq	%rsi, %rdx
	call	*264(%rax)
	testl	%eax, %eax
	je	.L474
	movq	(%r15), %rax
	movq	32(%rbx), %rcx
	movq	%r13, %r8
	movq	%r15, %rdi
	movq	16(%r14), %rdx
	movq	16(%rbx), %rsi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L474
	movl	$0, 40(%r12)
	movl	$1, %eax
	movl	$0, 40(%rbx)
	jmp	.L471
	.cfi_endproc
.LFE407:
	.size	ec_GFp_simple_ladder_pre, .-ec_GFp_simple_ladder_pre
	.p2align 4
	.globl	ec_GFp_simple_field_inv
	.type	ec_GFp_simple_field_inv, @function
ec_GFp_simple_field_inv:
.LFB405:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -64(%rbp)
	movq	$0, -56(%rbp)
	testq	%rcx, %rcx
	je	.L561
.L543:
	movq	%r13, %rdi
	call	BN_CTX_start@PLT
	movq	%r13, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	jne	.L545
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L563:
	movq	%rbx, %rdi
	call	BN_is_zero@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L562
.L545:
	movq	64(%r12), %rsi
	movq	%rbx, %rdi
	call	BN_priv_rand_range@PLT
	testl	%eax, %eax
	jne	.L563
.L547:
	xorl	%r15d, %r15d
.L546:
	movq	%r13, %rdi
	call	BN_CTX_end@PLT
	movq	-56(%rbp), %rdi
	call	BN_CTX_free@PLT
.L542:
	addq	$24, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L562:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	-64(%rbp), %rdx
	movq	%r13, %r8
	movq	%rbx, %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L547
	movq	64(%r12), %rdx
	movq	%r13, %rcx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	BN_mod_inverse@PLT
	testq	%rax, %rax
	je	.L564
	movq	(%r12), %rax
	movq	%r13, %r8
	movq	%rbx, %rcx
	movq	%r14, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	call	*264(%rax)
	testl	%eax, %eax
	setne	%r15b
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L561:
	call	BN_CTX_secure_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L550
	movq	%rax, -56(%rbp)
	jmp	.L543
.L550:
	xorl	%r15d, %r15d
	jmp	.L542
.L564:
	movl	$1401, %r8d
	movl	$165, %edx
	movl	$298, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L546
	.cfi_endproc
.LFE405:
	.size	ec_GFp_simple_field_inv, .-ec_GFp_simple_field_inv
	.p2align 4
	.globl	ec_GFp_simple_blind_coordinates
	.type	ec_GFp_simple_blind_coordinates, @function
ec_GFp_simple_blind_coordinates:
.LFB406:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rdx, %rdi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -56(%rbp)
	call	BN_CTX_start@PLT
	movq	%r15, %rdi
	call	BN_CTX_get@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	BN_CTX_get@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	jne	.L566
	jmp	.L595
	.p2align 4,,10
	.p2align 3
.L597:
	movq	%r13, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L596
.L566:
	call	ERR_set_mark@PLT
	movq	64(%r14), %rsi
	movq	%r13, %rdi
	call	BN_priv_rand_range@PLT
	movl	%eax, %r12d
	call	ERR_pop_to_mark@PLT
	testl	%r12d, %r12d
	jne	.L597
	movl	$1, %r12d
.L567:
	movq	%r15, %rdi
	call	BN_CTX_end@PLT
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L596:
	.cfi_restore_state
	movq	(%r14), %rax
	movq	296(%rax), %r8
	testq	%r8, %r8
	je	.L568
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	*%r8
	testl	%eax, %eax
	je	.L567
	movq	(%r14), %rax
.L568:
	movq	-56(%rbp), %rcx
	movq	%r15, %r8
	movq	%r14, %rdi
	movq	32(%rcx), %rsi
	movq	%r13, %rcx
	movq	%rsi, %rdx
	call	*264(%rax)
	testl	%eax, %eax
	je	.L567
	movq	(%r14), %rax
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	*272(%rax)
	testl	%eax, %eax
	je	.L567
	movq	-56(%rbp), %rcx
	movq	(%r14), %rax
	movq	%r15, %r8
	movq	%r14, %rdi
	movq	16(%rcx), %rsi
	movq	%rbx, %rcx
	movq	%rsi, %rdx
	call	*264(%rax)
	testl	%eax, %eax
	je	.L567
	movq	(%r14), %rax
	movq	%r15, %r8
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L567
	movq	-56(%rbp), %r13
	movq	(%r14), %rax
	movq	%r15, %r8
	movq	%rbx, %rcx
	movq	%r14, %rdi
	movq	24(%r13), %rsi
	movq	%rsi, %rdx
	call	*264(%rax)
	testl	%eax, %eax
	je	.L567
	movl	$0, 40(%r13)
	movl	$1, %r12d
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L595:
	movl	$1434, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$287, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L567
	.cfi_endproc
.LFE406:
	.size	ec_GFp_simple_blind_coordinates, .-ec_GFp_simple_blind_coordinates
	.p2align 4
	.globl	ec_GFp_simple_group_copy
	.type	ec_GFp_simple_group_copy, @function
ec_GFp_simple_group_copy:
.LFB381:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	64(%rsi), %rsi
	movq	64(%rdi), %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L601
	movq	96(%r12), %rsi
	movq	96(%rbx), %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L601
	movq	104(%r12), %rsi
	movq	104(%rbx), %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L601
	movl	112(%r12), %eax
	movl	%eax, 112(%rbx)
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L601:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE381:
	.size	ec_GFp_simple_group_copy, .-ec_GFp_simple_group_copy
	.p2align 4
	.globl	ec_GFp_simple_point_copy
	.type	ec_GFp_simple_point_copy, @function
ec_GFp_simple_point_copy:
.LFB389:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	16(%rsi), %rsi
	movq	16(%rdi), %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L612
	movq	24(%r12), %rsi
	movq	24(%rbx), %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L612
	movq	32(%r12), %rsi
	movq	32(%rbx), %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L612
	movl	40(%r12), %eax
	movl	%eax, 40(%rbx)
	movl	8(%r12), %eax
	movl	%eax, 8(%rbx)
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L612:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE389:
	.size	ec_GFp_simple_point_copy, .-ec_GFp_simple_point_copy
	.p2align 4
	.globl	ec_GFp_simple_group_set_curve
	.type	ec_GFp_simple_group_set_curve, @function
ec_GFp_simple_group_set_curve:
.LFB382:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$24, %rsp
	movq	%rcx, -56(%rbp)
	call	BN_num_bits@PLT
	cmpl	$2, %eax
	jle	.L623
	movq	%r13, %rdi
	call	BN_is_odd@PLT
	testl	%eax, %eax
	je	.L623
	movq	$0, -64(%rbp)
	testq	%r12, %r12
	je	.L663
.L624:
	movq	%r12, %rdi
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L627
	movq	64(%rbx), %rdi
	movq	%r13, %rsi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L627
	movq	64(%rbx), %rdi
	xorl	%esi, %esi
	call	BN_set_negative@PLT
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	BN_nnmod@PLT
	testl	%eax, %eax
	je	.L627
	movq	(%rbx), %rax
	movq	96(%rbx), %r8
	movq	296(%rax), %rax
	testq	%rax, %rax
	je	.L628
	movq	%r12, %rcx
	movq	%r15, %rdx
	movq	%r8, %rsi
	movq	%rbx, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L627
.L630:
	movq	104(%rbx), %rdi
	movq	-56(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r13, %rdx
	call	BN_nnmod@PLT
	testl	%eax, %eax
	je	.L627
	movq	(%rbx), %rax
	movq	296(%rax), %rax
	testq	%rax, %rax
	je	.L633
	movq	104(%rbx), %rsi
	movq	%r12, %rcx
	movq	%rbx, %rdi
	movq	%rsi, %rdx
	call	*%rax
	testl	%eax, %eax
	je	.L627
.L633:
	movl	$3, %esi
	movq	%r15, %rdi
	call	BN_add_word@PLT
	testl	%eax, %eax
	je	.L627
	movq	64(%rbx), %rsi
	movq	%r15, %rdi
	movl	$1, %r13d
	call	BN_cmp@PLT
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
	movl	%eax, 112(%rbx)
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L623:
	movl	$144, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$103, %edx
	xorl	%r13d, %r13d
	movl	$166, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
.L620:
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L628:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	jne	.L630
	.p2align 4,,10
	.p2align 3
.L627:
	xorl	%r13d, %r13d
.L626:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	movq	-64(%rbp), %rdi
	call	BN_CTX_free@PLT
	jmp	.L620
	.p2align 4,,10
	.p2align 3
.L663:
	call	BN_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L635
	movq	%rax, -64(%rbp)
	jmp	.L624
	.p2align 4,,10
	.p2align 3
.L635:
	xorl	%r13d, %r13d
	jmp	.L620
	.cfi_endproc
.LFE382:
	.size	ec_GFp_simple_group_set_curve, .-ec_GFp_simple_group_set_curve
	.p2align 4
	.globl	ec_GFp_simple_point_set_affine_coordinates
	.type	ec_GFp_simple_point_set_affine_coordinates, @function
ec_GFp_simple_point_set_affine_coordinates:
.LFB393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L668
	movq	%rcx, %r13
	testq	%rcx, %rcx
	je	.L668
	movq	%r8, %rbx
	movq	%rdi, %r14
	movq	%rsi, %r15
	movq	%rdx, %r12
	call	BN_value_one@PLT
	addq	$8, %rsp
	movq	%rbx, %r9
	movq	%r13, %rcx
	popq	%rbx
	movq	%r12, %rdx
	movq	%r15, %rsi
	popq	%r12
	movq	%r14, %rdi
	popq	%r13
	movq	%rax, %r8
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	EC_POINT_set_Jprojective_coordinates_GFp@PLT
	.p2align 4,,10
	.p2align 3
.L668:
	.cfi_restore_state
	movl	$486, %r8d
	movl	$67, %edx
	movl	$168, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE393:
	.size	ec_GFp_simple_point_set_affine_coordinates, .-ec_GFp_simple_point_set_affine_coordinates
	.p2align 4
	.globl	ec_GFp_simple_cmp
	.type	ec_GFp_simple_cmp, @function
ec_GFp_simple_cmp:
.LFB400:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	call	EC_POINT_is_at_infinity@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	testl	%eax, %eax
	jne	.L717
	call	EC_POINT_is_at_infinity@PLT
	testl	%eax, %eax
	jne	.L676
	movl	40(%rbx), %r8d
	testl	%r8d, %r8d
	je	.L675
	movl	40(%r13), %edi
	testl	%edi, %edi
	jne	.L718
.L675:
	movq	(%r14), %rax
	movq	$0, -56(%rbp)
	movq	264(%rax), %rcx
	movq	272(%rax), %rax
	movq	%rcx, -64(%rbp)
	movq	%rax, -72(%rbp)
	testq	%r12, %r12
	je	.L719
.L677:
	movq	%r12, %rdi
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -88(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	call	BN_CTX_get@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L681
	movl	40(%r13), %esi
	testl	%esi, %esi
	je	.L720
	movq	16(%rbx), %rax
	movq	%rax, -104(%rbp)
.L682:
	movl	40(%rbx), %ecx
	testl	%ecx, %ecx
	jne	.L683
	movq	32(%rbx), %rdx
	movq	-80(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r14, %rdi
	movq	-72(%rbp), %rax
	call	*%rax
	testl	%eax, %eax
	je	.L681
	movq	16(%r13), %rdx
	movq	-80(%rbp), %rcx
	movq	%r12, %r8
	movq	%r14, %rdi
	movq	-96(%rbp), %rsi
	movq	-64(%rbp), %r10
	call	*%r10
	testl	%eax, %eax
	je	.L681
	movq	-96(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	%rax, %rsi
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L676:
	movl	$1, %eax
.L671:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L717:
	.cfi_restore_state
	call	EC_POINT_is_at_infinity@PLT
	testl	%eax, %eax
	sete	%al
	addq	$72, %rsp
	popq	%rbx
	movzbl	%al, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L683:
	.cfi_restore_state
	movq	16(%r13), %rax
	movq	%rax, -72(%rbp)
	movq	%rax, %rsi
.L684:
	movq	-104(%rbp), %rdi
	call	BN_cmp@PLT
	movl	%eax, %r8d
	movl	$1, %eax
	testl	%r8d, %r8d
	jne	.L679
	movl	40(%r13), %edx
	testl	%edx, %edx
	jne	.L685
	movq	32(%r13), %rcx
	movq	-64(%rbp), %rax
	movq	%r12, %r8
	movq	%r15, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L721
	.p2align 4,,10
	.p2align 3
.L681:
	movl	$-1, %eax
.L679:
	movq	%r12, %rdi
	movl	%eax, -64(%rbp)
	call	BN_CTX_end@PLT
	movq	-56(%rbp), %rdi
	call	BN_CTX_free@PLT
	movl	-64(%rbp), %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L720:
	.cfi_restore_state
	movq	%rax, %rsi
	movq	32(%r13), %rdx
	movq	-72(%rbp), %rax
	movq	%r12, %rcx
	movq	%r14, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L681
	movq	16(%rbx), %rdx
	movq	-88(%rbp), %rsi
	movq	%r12, %r8
	movq	%r15, %rcx
	movq	-64(%rbp), %r9
	movq	%r14, %rdi
	call	*%r9
	testl	%eax, %eax
	je	.L681
	movq	-88(%rbp), %rax
	movq	%rax, -104(%rbp)
	jmp	.L682
	.p2align 4,,10
	.p2align 3
.L719:
	call	BN_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L690
	movq	%rax, -56(%rbp)
	jmp	.L677
	.p2align 4,,10
	.p2align 3
.L685:
	movq	24(%rbx), %rax
	movq	%rax, -104(%rbp)
.L686:
	movl	40(%rbx), %eax
	testl	%eax, %eax
	jne	.L687
	movq	32(%rbx), %rcx
	movq	-80(%rbp), %rbx
	movq	%r12, %r8
	movq	%r14, %rdi
	movq	-64(%rbp), %r15
	movq	%rbx, %rdx
	movq	%rbx, %rsi
	call	*%r15
	testl	%eax, %eax
	je	.L681
	movq	24(%r13), %rdx
	movq	-96(%rbp), %rsi
	movq	%r12, %r8
	movq	%rbx, %rcx
	movq	%r14, %rdi
	call	*%r15
	testl	%eax, %eax
	jne	.L688
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L718:
	movq	16(%r13), %rsi
	movq	16(%rbx), %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	jne	.L676
	movq	24(%r13), %rsi
	movq	24(%rbx), %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L671
	.p2align 4,,10
	.p2align 3
.L687:
	movq	24(%r13), %rax
	movq	%rax, -72(%rbp)
.L688:
	movq	-72(%rbp), %rsi
	movq	-104(%rbp), %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L721:
	movq	24(%rbx), %rdx
	movq	-88(%rbp), %rsi
	movq	%r12, %r8
	movq	%r15, %rcx
	movq	-64(%rbp), %rax
	movq	%r14, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L686
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L690:
	movl	$-1, %eax
	jmp	.L671
	.cfi_endproc
.LFE400:
	.size	ec_GFp_simple_cmp, .-ec_GFp_simple_cmp
	.p2align 4
	.globl	ec_GFp_simple_add
	.type	ec_GFp_simple_add, @function
ec_GFp_simple_add:
.LFB395:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	cmpq	%rcx, %rdx
	je	.L838
	movq	%rdx, %rsi
	movq	%rcx, %r14
	call	EC_POINT_is_at_infinity@PLT
	movq	%r14, %rsi
	testl	%eax, %eax
	jne	.L837
	movq	%r15, %rdi
	call	EC_POINT_is_at_infinity@PLT
	movl	%eax, -52(%rbp)
	testl	%eax, %eax
	jne	.L839
	movq	(%r15), %rax
	movq	$0, -72(%rbp)
	movq	264(%rax), %rcx
	movq	272(%rax), %rax
	movq	%rax, -88(%rbp)
	movq	64(%r15), %rax
	movq	%rcx, -80(%rbp)
	movq	%rax, -120(%rbp)
	testq	%r12, %r12
	je	.L840
.L726:
	movq	%r12, %rdi
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -128(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	BN_CTX_get@PLT
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L728
	movl	40(%r14), %esi
	testl	%esi, %esi
	jne	.L841
	movq	32(%r14), %rdx
	movq	-64(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r15, %rdi
	movq	-88(%rbp), %rax
	call	*%rax
	testl	%eax, %eax
	je	.L751
	movq	16(%r13), %rdx
	movq	-64(%rbp), %rcx
	movq	%r12, %r8
	movq	%r15, %rdi
	movq	-96(%rbp), %rsi
	movq	-80(%rbp), %rax
	call	*%rax
	testl	%eax, %eax
	je	.L751
	movq	-64(%rbp), %rdx
	movq	32(%r14), %rcx
	movq	%r12, %r8
	movq	%r15, %rdi
	movq	-80(%rbp), %rax
	movq	%rdx, %rsi
	call	*%rax
	testl	%eax, %eax
	je	.L751
	movq	24(%r13), %rdx
	movq	-64(%rbp), %rcx
	movq	%r12, %r8
	movq	%r15, %rdi
	movq	-104(%rbp), %rsi
	movq	-80(%rbp), %rax
	call	*%rax
	testl	%eax, %eax
	je	.L751
.L732:
	movl	40(%r13), %ecx
	testl	%ecx, %ecx
	jne	.L842
	movq	32(%r13), %rdx
	movq	-64(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r15, %rdi
	movq	-88(%rbp), %rax
	call	*%rax
	movl	%eax, -52(%rbp)
	testl	%eax, %eax
	je	.L728
	movq	16(%r14), %rdx
	movq	-64(%rbp), %rcx
	movq	%r12, %r8
	movq	%r15, %rdi
	movq	-112(%rbp), %rsi
	movq	-80(%rbp), %rax
	call	*%rax
	movl	%eax, -52(%rbp)
	testl	%eax, %eax
	je	.L728
	movq	-64(%rbp), %rdx
	movq	32(%r13), %rcx
	movq	%r12, %r8
	movq	%r15, %rdi
	movq	-80(%rbp), %rax
	movq	%rdx, %rsi
	call	*%rax
	movl	%eax, -52(%rbp)
	testl	%eax, %eax
	je	.L728
	movq	24(%r14), %rdx
	movq	-64(%rbp), %rcx
	movq	%r12, %r8
	movq	%r15, %rdi
	movq	-128(%rbp), %rsi
	movq	-80(%rbp), %rax
	call	*%rax
	movl	%eax, -52(%rbp)
	testl	%eax, %eax
	je	.L728
.L734:
	movq	-120(%rbp), %rcx
	movq	-112(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	-136(%rbp), %rdi
	call	BN_mod_sub_quick@PLT
	movl	%eax, -52(%rbp)
	testl	%eax, %eax
	je	.L728
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %rdx
	movq	-104(%rbp), %rsi
	movq	-144(%rbp), %rdi
	call	BN_mod_sub_quick@PLT
	movl	%eax, -52(%rbp)
	testl	%eax, %eax
	je	.L728
	movq	-136(%rbp), %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L735
	movq	-144(%rbp), %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L736
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	EC_POINT_dbl@PLT
	movl	%eax, -52(%rbp)
	.p2align 4,,10
	.p2align 3
.L728:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	movq	-72(%rbp), %rdi
	call	BN_CTX_free@PLT
.L722:
	movl	-52(%rbp), %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L839:
	.cfi_restore_state
	movq	%r13, %rsi
.L837:
	addq	$104, %rsp
	movq	%rbx, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	EC_POINT_copy@PLT
.L844:
	.cfi_restore_state
	movq	-64(%rbp), %rdi
	movq	%rdx, %rsi
	call	BN_copy@PLT
	testq	%rax, %rax
	jne	.L742
	.p2align 4,,10
	.p2align 3
.L751:
	movl	$0, -52(%rbp)
	jmp	.L728
	.p2align 4,,10
	.p2align 3
.L838:
	addq	$104, %rsp
	movq	%r8, %rcx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	EC_POINT_dbl@PLT
	.p2align 4,,10
	.p2align 3
.L840:
	.cfi_restore_state
	call	BN_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L722
	movq	%rax, -72(%rbp)
	jmp	.L726
	.p2align 4,,10
	.p2align 3
.L841:
	movq	16(%r13), %rsi
	movq	-96(%rbp), %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L728
	movq	24(%r13), %rsi
	movq	-104(%rbp), %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	jne	.L732
	jmp	.L728
	.p2align 4,,10
	.p2align 3
.L842:
	movq	16(%r14), %rsi
	movq	-112(%rbp), %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L728
	movq	24(%r14), %rsi
	movq	-128(%rbp), %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	jne	.L734
	jmp	.L728
.L735:
	movq	-96(%rbp), %rsi
	movq	-120(%rbp), %rcx
	movq	-112(%rbp), %rdx
	movq	%rsi, %rdi
	call	BN_mod_add_quick@PLT
	movl	%eax, -52(%rbp)
	testl	%eax, %eax
	je	.L728
	movq	-104(%rbp), %rsi
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %rdx
	movq	%rsi, %rdi
	call	BN_mod_add_quick@PLT
	movl	%eax, -52(%rbp)
	testl	%eax, %eax
	je	.L728
	movl	40(%r14), %eax
	movl	40(%r13), %edx
	movl	%eax, -52(%rbp)
	testl	%edx, %edx
	je	.L737
	testl	%eax, %eax
	jne	.L843
	movq	32(%r14), %rsi
	movq	-64(%rbp), %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L728
.L742:
	movq	32(%rbx), %rsi
	movq	-64(%rbp), %rdx
	movq	%r12, %r8
	movq	%r15, %rdi
	movq	-136(%rbp), %rcx
	movq	-80(%rbp), %rax
	call	*%rax
	movl	%eax, -52(%rbp)
	testl	%eax, %eax
	je	.L728
.L740:
	movq	-64(%rbp), %rsi
	movq	-88(%rbp), %rax
	movq	%r12, %rcx
	movq	%r15, %rdi
	movl	$0, 40(%rbx)
	movq	-144(%rbp), %rdx
	call	*%rax
	movl	%eax, -52(%rbp)
	testl	%eax, %eax
	je	.L728
	movq	-128(%rbp), %r14
	movq	-88(%rbp), %r9
	movq	%r12, %rcx
	movq	%r15, %rdi
	movq	-136(%rbp), %rdx
	movq	%r14, %rsi
	call	*%r9
	movl	%eax, -52(%rbp)
	testl	%eax, %eax
	je	.L728
	movq	%r14, -128(%rbp)
	movq	%r14, %rcx
	movq	-112(%rbp), %r14
	movq	%r12, %r8
	movq	-96(%rbp), %rdx
	movq	-80(%rbp), %r13
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	*%r13
	movl	%eax, -52(%rbp)
	testl	%eax, %eax
	je	.L728
	movq	%r14, %rdx
	movq	16(%rbx), %rdi
	movq	-120(%rbp), %rcx
	movq	%r14, -112(%rbp)
	movq	-64(%rbp), %r14
	movq	%r14, %rsi
	call	BN_mod_sub_quick@PLT
	movl	%eax, -52(%rbp)
	testl	%eax, %eax
	je	.L728
	movq	16(%rbx), %rsi
	movq	-120(%rbp), %rdx
	movq	%r14, %rdi
	call	BN_mod_lshift1_quick@PLT
	movl	%eax, -52(%rbp)
	testl	%eax, %eax
	je	.L728
	movq	-120(%rbp), %rcx
	movq	-112(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r14, %rdi
	call	BN_mod_sub_quick@PLT
	movl	%eax, -52(%rbp)
	testl	%eax, %eax
	je	.L728
	movq	-144(%rbp), %rcx
	movq	%r12, %r8
	movq	%r14, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	*%r13
	movl	%eax, -52(%rbp)
	testl	%eax, %eax
	je	.L728
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rdx
	movq	%r12, %r8
	movq	%r15, %rdi
	movq	%rax, %rcx
	movq	%rax, %rsi
	call	*%r13
	movl	%eax, -52(%rbp)
	testl	%eax, %eax
	je	.L728
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r12, %r8
	movq	%r15, %rdi
	movq	-136(%rbp), %rcx
	call	*%r13
	movl	%eax, -52(%rbp)
	testl	%eax, %eax
	je	.L728
	movq	-120(%rbp), %rcx
	movq	-96(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	BN_mod_sub_quick@PLT
	movl	%eax, -52(%rbp)
	testl	%eax, %eax
	je	.L728
	movq	%r14, %rdi
	call	BN_is_odd@PLT
	testl	%eax, %eax
	je	.L744
	movq	-64(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%rsi, %rdi
	call	BN_add@PLT
	movl	%eax, -52(%rbp)
	testl	%eax, %eax
	je	.L728
.L744:
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rsi
	call	BN_rshift1@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	movl	%eax, -52(%rbp)
	jmp	.L728
	.p2align 4,,10
	.p2align 3
.L736:
	movq	32(%rbx), %rdi
	xorl	%esi, %esi
	call	BN_set_word@PLT
	movl	$0, 40(%rbx)
	movl	$1, -52(%rbp)
	jmp	.L728
.L737:
	cmpl	$0, -52(%rbp)
	movq	32(%r13), %rdx
	jne	.L844
	movq	32(%r14), %rcx
	movq	-64(%rbp), %rsi
	movq	%r12, %r8
	movq	%r15, %rdi
	movq	-80(%rbp), %rax
	call	*%rax
	movl	%eax, -52(%rbp)
	testl	%eax, %eax
	jne	.L742
	jmp	.L728
.L843:
	movq	32(%rbx), %rdi
	movq	-136(%rbp), %rsi
	call	BN_copy@PLT
	testq	%rax, %rax
	jne	.L740
	jmp	.L751
	.cfi_endproc
.LFE395:
	.size	ec_GFp_simple_add, .-ec_GFp_simple_add
	.p2align 4
	.globl	ec_GFp_simple_invert
	.type	ec_GFp_simple_invert, @function
ec_GFp_simple_invert:
.LFB397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	EC_POINT_is_at_infinity@PLT
	testl	%eax, %eax
	je	.L850
.L847:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L850:
	.cfi_restore_state
	movq	24(%rbx), %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L847
	movq	24(%rbx), %rdi
	movq	64(%r12), %rsi
	popq	%rbx
	popq	%r12
	movq	%rdi, %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	BN_usub@PLT
	.cfi_endproc
.LFE397:
	.size	ec_GFp_simple_invert, .-ec_GFp_simple_invert
	.p2align 4
	.globl	ec_GFp_simple_is_on_curve
	.type	ec_GFp_simple_is_on_curve, @function
ec_GFp_simple_is_on_curve:
.LFB399:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	$1, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	call	EC_POINT_is_at_infinity@PLT
	testl	%eax, %eax
	jne	.L851
	movq	(%r14), %rax
	xorl	%r15d, %r15d
	movq	264(%rax), %rcx
	movq	272(%rax), %rax
	movq	%rax, -56(%rbp)
	movq	64(%r14), %rax
	movq	%rcx, -80(%rbp)
	movq	%rax, -88(%rbp)
	testq	%r12, %r12
	je	.L922
.L853:
	movq	%r12, %rdi
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	BN_CTX_get@PLT
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L856
	movq	16(%rbx), %rdx
	movq	-56(%rbp), %rax
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L856
	movl	40(%rbx), %edx
	testl	%edx, %edx
	jne	.L857
	movq	32(%rbx), %rdx
	movq	-64(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r14, %rdi
	movq	-56(%rbp), %rax
	call	*%rax
	testl	%eax, %eax
	je	.L856
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r14, %rdi
	movq	-56(%rbp), %rax
	call	*%rax
	testl	%eax, %eax
	je	.L856
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rdx
	movq	%r12, %r8
	movq	%r14, %rdi
	movq	-96(%rbp), %rsi
	movq	-80(%rbp), %rax
	call	*%rax
	testl	%eax, %eax
	je	.L856
	movl	112(%r14), %eax
	testl	%eax, %eax
	jne	.L923
	movq	96(%r14), %rcx
	movq	-72(%rbp), %rdx
	movq	%r12, %r8
	movq	%r14, %rdi
	movq	-64(%rbp), %rsi
	movq	-80(%rbp), %rax
	call	*%rax
	testl	%eax, %eax
	je	.L856
	movq	-88(%rbp), %rcx
	movq	-64(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	BN_mod_add_quick@PLT
	testl	%eax, %eax
	je	.L856
.L921:
	movq	16(%rbx), %rcx
	movq	-80(%rbp), %rax
	movq	%r12, %r8
	movq	%r13, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L856
	movq	104(%r14), %rdx
	movq	-96(%rbp), %rcx
	movq	%r12, %r8
	movq	%r14, %rdi
	movq	-64(%rbp), %rsi
	movq	-80(%rbp), %rax
	call	*%rax
	testl	%eax, %eax
	je	.L856
	movq	-88(%rbp), %rcx
	movq	-64(%rbp), %rdx
	.p2align 4,,10
	.p2align 3
.L920:
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	BN_mod_add_quick@PLT
	testl	%eax, %eax
	je	.L856
	movq	24(%rbx), %rdx
	movq	-64(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r14, %rdi
	movq	-56(%rbp), %rax
	call	*%rax
	testl	%eax, %eax
	je	.L856
	movq	-64(%rbp), %rdi
	movq	%r13, %rsi
	xorl	%r13d, %r13d
	call	BN_ucmp@PLT
	testl	%eax, %eax
	sete	%r13b
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L857:
	movq	96(%r14), %rdx
	movq	-88(%rbp), %rcx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	BN_mod_add_quick@PLT
	testl	%eax, %eax
	je	.L856
	movq	16(%rbx), %rcx
	movq	-80(%rbp), %rax
	movq	%r12, %r8
	movq	%r13, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L924
	.p2align 4,,10
	.p2align 3
.L856:
	movl	$-1, %r13d
.L855:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	movq	%r15, %rdi
	call	BN_CTX_free@PLT
.L851:
	addq	$56, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L922:
	.cfi_restore_state
	call	BN_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L865
	movq	%rax, %r15
	jmp	.L853
	.p2align 4,,10
	.p2align 3
.L924:
	movq	104(%r14), %rdx
	movq	-88(%rbp), %rcx
	jmp	.L920
	.p2align 4,,10
	.p2align 3
.L923:
	movq	-88(%rbp), %rdx
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rdi
	call	BN_mod_lshift1_quick@PLT
	testl	%eax, %eax
	je	.L856
	movq	-64(%rbp), %rsi
	movq	-88(%rbp), %rcx
	movq	-72(%rbp), %rdx
	movq	%rsi, %rdi
	call	BN_mod_add_quick@PLT
	testl	%eax, %eax
	je	.L856
	movq	-88(%rbp), %rcx
	movq	-64(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	BN_mod_sub_quick@PLT
	testl	%eax, %eax
	jne	.L921
	jmp	.L856
	.p2align 4,,10
	.p2align 3
.L865:
	movl	$-1, %r13d
	jmp	.L851
	.cfi_endproc
.LFE399:
	.size	ec_GFp_simple_is_on_curve, .-ec_GFp_simple_is_on_curve
	.p2align 4
	.globl	ec_GFp_simple_make_affine
	.type	ec_GFp_simple_make_affine, @function
ec_GFp_simple_make_affine:
.LFB401:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	40(%rsi), %eax
	testl	%eax, %eax
	je	.L926
.L928:
	movl	$1, %r13d
.L925:
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L926:
	.cfi_restore_state
	movq	%rdi, %r14
	movq	%rsi, %rbx
	movq	%rdx, %r12
	call	EC_POINT_is_at_infinity@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.L928
	xorl	%r15d, %r15d
	testq	%r12, %r12
	je	.L945
.L929:
	movq	%r12, %rdi
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	BN_CTX_get@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L931
	movq	-56(%rbp), %rdx
	movq	%r12, %r8
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	call	EC_POINT_get_affine_coordinates@PLT
	testl	%eax, %eax
	jne	.L946
.L931:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	movq	%r15, %rdi
	call	BN_CTX_free@PLT
	jmp	.L925
	.p2align 4,,10
	.p2align 3
.L946:
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %rdx
	movq	%r12, %r8
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	EC_POINT_set_affine_coordinates@PLT
	testl	%eax, %eax
	je	.L931
	movl	40(%rbx), %r13d
	testl	%r13d, %r13d
	je	.L947
	movl	$1, %r13d
	jmp	.L931
	.p2align 4,,10
	.p2align 3
.L945:
	call	BN_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L925
	movq	%rax, %r15
	jmp	.L929
	.p2align 4,,10
	.p2align 3
.L947:
	movl	$1188, %r8d
	movl	$68, %edx
	movl	$102, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L931
	.cfi_endproc
.LFE401:
	.size	ec_GFp_simple_make_affine, .-ec_GFp_simple_make_affine
	.p2align 4
	.globl	ec_GFp_simple_points_make_affine
	.type	ec_GFp_simple_points_make_affine, @function
ec_GFp_simple_points_make_affine:
.LFB402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	$1, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -56(%rbp)
	movq	%rdx, -64(%rbp)
	testq	%rsi, %rsi
	je	.L948
	movq	$0, -72(%rbp)
	movq	%rdi, %r14
	movq	%rcx, %r13
	testq	%rcx, %rcx
	je	.L1063
.L950:
	movq	%r13, %rdi
	call	BN_CTX_start@PLT
	movq	%r13, %rdi
	call	BN_CTX_get@PLT
	movq	%r13, %rdi
	movq	%rax, -88(%rbp)
	call	BN_CTX_get@PLT
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L1062
	movq	-56(%rbp), %rax
	movl	$1224, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%ebx, %ebx
	salq	$3, %rax
	movq	%rax, %rdi
	movq	%rax, -96(%rbp)
	call	CRYPTO_malloc@PLT
	movq	-56(%rbp), %r15
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L952
	jmp	.L1062
	.p2align 4,,10
	.p2align 3
.L1065:
	leaq	1(%rbx), %rcx
	cmpq	%rcx, %r15
	je	.L1064
	movq	%rcx, %rbx
.L952:
	call	BN_new@PLT
	movq	%rax, (%r12,%rbx,8)
	testq	%rax, %rax
	jne	.L1065
.L953:
	xorl	%r15d, %r15d
.L964:
	movq	%r13, %rdi
	xorl	%ebx, %ebx
	call	BN_CTX_end@PLT
	movq	-72(%rbp), %rdi
	call	BN_CTX_free@PLT
	movq	-56(%rbp), %r13
	jmp	.L978
	.p2align 4,,10
	.p2align 3
.L977:
	call	BN_clear_free@PLT
	addq	$1, %rbx
	cmpq	%rbx, %r13
	je	.L979
.L978:
	movq	(%r12,%rbx,8), %rdi
	testq	%rdi, %rdi
	jne	.L977
.L979:
	movl	$1354, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
.L948:
	addq	$72, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1062:
	.cfi_restore_state
	movq	%r13, %rdi
	xorl	%r15d, %r15d
	call	BN_CTX_end@PLT
	movq	-72(%rbp), %rdi
	call	BN_CTX_free@PLT
	jmp	.L948
	.p2align 4,,10
	.p2align 3
.L1063:
	call	BN_CTX_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L982
	movq	%rax, -72(%rbp)
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L1064:
	movq	-64(%rbp), %rax
	movq	%rcx, -104(%rbp)
	movq	(%rax), %rax
	movq	32(%rax), %rdi
	call	BN_is_zero@PLT
	movq	-104(%rbp), %rcx
	testl	%eax, %eax
	je	.L1066
	movq	(%r14), %rax
	movq	(%r12), %r8
	movq	%rcx, -104(%rbp)
	movq	312(%rax), %rax
	testq	%rax, %rax
	je	.L957
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	*%rax
	movq	-104(%rbp), %rcx
	testl	%eax, %eax
	je	.L953
.L958:
	movq	-64(%rbp), %rax
	leaq	8(%r12), %r15
	leaq	8(%rax), %r9
	addq	-96(%rbp), %rax
	cmpq	$1, %rcx
	je	.L956
	movq	%r12, -104(%rbp)
	movq	%r9, %r12
	movq	%rbx, -112(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	jmp	.L962
	.p2align 4,,10
	.p2align 3
.L1068:
	movq	(%r12), %rax
	movq	-8(%rbx), %rdx
	movq	%r13, %r8
	movq	%r14, %rdi
	movq	(%rbx), %rsi
	movq	32(%rax), %rcx
	movq	(%r14), %rax
	call	*264(%rax)
	testl	%eax, %eax
	je	.L1052
.L963:
	addq	$8, %r12
	addq	$8, %rbx
	cmpq	%r15, %r12
	je	.L1067
.L962:
	movq	(%r12), %rax
	movq	32(%rax), %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L1068
	movq	-8(%rbx), %rsi
	movq	(%rbx), %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	jne	.L963
.L1052:
	movq	-104(%rbp), %r12
	jmp	.L953
	.p2align 4,,10
	.p2align 3
.L1067:
	movq	-104(%rbp), %r12
	movq	-112(%rbp), %rbx
.L956:
	movq	-96(%rbp), %rax
	movq	-88(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r14, %rdi
	movq	-8(%r12,%rax), %rdx
	movq	(%r14), %rax
	call	*288(%rax)
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L1069
	movq	(%r14), %rax
	movq	296(%rax), %rax
	testq	%rax, %rax
	je	.L968
	movq	-88(%rbp), %r15
	movq	%r13, %rcx
	movq	%r14, %rdi
	movq	%r15, %rdx
	movq	%r15, %rsi
	call	*%rax
	testl	%eax, %eax
	je	.L953
	movq	(%r14), %rax
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	*296(%rax)
	testl	%eax, %eax
	je	.L953
.L968:
	movq	%rbx, %r15
	testq	%rbx, %rbx
	je	.L967
	movq	%r12, -96(%rbp)
	movq	-88(%rbp), %r12
	movq	%rbx, -104(%rbp)
	movq	-64(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L966:
	movq	(%rbx,%r15,8), %rax
	movq	32(%rax), %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L972
	movq	-96(%rbp), %rax
	movq	-80(%rbp), %rsi
	movq	%r13, %r8
	movq	%r12, %rcx
	movq	%r14, %rdi
	movq	-8(%rax,%r15,8), %rdx
	movq	(%r14), %rax
	call	*264(%rax)
	testl	%eax, %eax
	je	.L1055
	movq	(%rbx,%r15,8), %rax
	movq	%r13, %r8
	movq	%r12, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	32(%rax), %rcx
	movq	(%r14), %rax
	call	*264(%rax)
	testl	%eax, %eax
	je	.L1055
	movq	(%rbx,%r15,8), %rax
	movq	-80(%rbp), %rsi
	movq	32(%rax), %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L1055
.L972:
	subq	$1, %r15
	jne	.L966
	movq	-96(%rbp), %r12
	movq	-104(%rbp), %rbx
.L967:
	movq	-64(%rbp), %rax
	movq	(%rax), %rax
	movq	32(%rax), %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L1070
.L970:
	xorl	%eax, %eax
	movq	-88(%rbp), %r15
	movq	%rbx, -80(%rbp)
	movq	%r12, -88(%rbp)
	movq	%rax, %r12
	jmp	.L973
	.p2align 4,,10
	.p2align 3
.L1072:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L1060
.L976:
	movl	$1, 40(%rbx)
.L974:
	leaq	1(%r12), %rax
	cmpq	-80(%rbp), %r12
	je	.L1071
	movq	%rax, %r12
.L973:
	movq	-64(%rbp), %rax
	movq	(%rax,%r12,8), %rbx
	movq	32(%rbx), %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L974
	movq	(%r14), %rax
	movq	32(%rbx), %rdx
	movq	%r13, %rcx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	*272(%rax)
	testl	%eax, %eax
	je	.L1060
	movq	16(%rbx), %rsi
	movq	(%r14), %rax
	movq	%r13, %r8
	movq	%r15, %rcx
	movq	%r14, %rdi
	movq	%rsi, %rdx
	call	*264(%rax)
	testl	%eax, %eax
	je	.L1060
	movq	(%r14), %rax
	movq	32(%rbx), %rcx
	movq	%r13, %r8
	movq	%r15, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L1060
	movq	24(%rbx), %rsi
	movq	(%r14), %rax
	movq	%r13, %r8
	movq	%r15, %rcx
	movq	%r14, %rdi
	movq	%rsi, %rdx
	call	*264(%rax)
	testl	%eax, %eax
	je	.L1060
	movq	(%r14), %rax
	movq	32(%rbx), %r8
	movq	312(%rax), %rax
	testq	%rax, %rax
	jne	.L1072
	movl	$1, %esi
	movq	%r8, %rdi
	call	BN_set_word@PLT
	testl	%eax, %eax
	jne	.L976
.L1060:
	movq	-88(%rbp), %r12
	jmp	.L953
	.p2align 4,,10
	.p2align 3
.L1066:
	movq	-64(%rbp), %rax
	movq	(%r12), %rdi
	movq	(%rax), %rax
	movq	32(%rax), %rsi
	call	BN_copy@PLT
	movq	-104(%rbp), %rcx
	testq	%rax, %rax
	jne	.L958
	jmp	.L953
.L982:
	xorl	%r15d, %r15d
	jmp	.L948
.L957:
	movl	$1, %esi
	movq	%r8, %rdi
	call	BN_set_word@PLT
	movq	-104(%rbp), %rcx
	testl	%eax, %eax
	jne	.L958
	jmp	.L953
.L1055:
	movq	-96(%rbp), %r12
	jmp	.L953
.L1069:
	movl	$1269, %r8d
	movl	$3, %edx
	movl	$137, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L964
.L1070:
	movq	-64(%rbp), %rax
	movq	-88(%rbp), %rsi
	movq	(%rax), %rax
	movq	32(%rax), %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	jne	.L970
	jmp	.L953
.L1071:
	movq	-88(%rbp), %r12
	movl	$1, %r15d
	jmp	.L964
	.cfi_endproc
.LFE402:
	.size	ec_GFp_simple_points_make_affine, .-ec_GFp_simple_points_make_affine
	.p2align 4
	.globl	ec_GFp_simple_ladder_post
	.type	ec_GFp_simple_ladder_post, @function
ec_GFp_simple_ladder_post:
.LFB409:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$72, %rsp
	movq	32(%rsi), %rdi
	movq	%rcx, -56(%rbp)
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L1181
	movq	32(%rbx), %rdi
	call	BN_is_zero@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	jne	.L1182
	movq	%r12, %rdi
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -88(%rbp)
	call	BN_CTX_get@PLT
	testq	%rax, %rax
	movq	%rax, -72(%rbp)
	je	.L1078
	movq	-56(%rbp), %rcx
	movq	64(%r14), %rdx
	movq	-64(%rbp), %rdi
	movq	24(%rcx), %rsi
	call	BN_mod_lshift1_quick@PLT
	testl	%eax, %eax
	je	.L1078
	movq	-72(%rbp), %r10
	movq	(%r14), %rax
	movq	%r12, %r8
	movq	%r14, %rdi
	movq	16(%r13), %rdx
	movq	-64(%rbp), %rcx
	movq	%r10, %rsi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L1078
	movq	-72(%rbp), %r10
	movq	(%r14), %rax
	movq	%r12, %r8
	movq	%r14, %rdi
	movq	32(%rbx), %rdx
	movq	%r10, %rcx
	movq	%r10, %rsi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L1078
	movq	-72(%rbp), %r10
	movq	(%r14), %rax
	movq	%r12, %r8
	movq	%r14, %rdi
	movq	32(%r13), %rdx
	movq	-88(%rbp), %rsi
	movq	%r10, %rcx
	call	*264(%rax)
	testl	%eax, %eax
	jne	.L1183
	.p2align 4,,10
	.p2align 3
.L1078:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
.L1073:
	addq	$72, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1182:
	.cfi_restore_state
	movq	-56(%rbp), %rsi
	movq	%r13, %rdi
	call	EC_POINT_copy@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L1073
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	xorl	%r15d, %r15d
	call	EC_POINT_invert@PLT
	testl	%eax, %eax
	setne	%r15b
	jmp	.L1073
	.p2align 4,,10
	.p2align 3
.L1181:
	addq	$72, %rsp
	movq	%r13, %rsi
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	EC_POINT_set_to_infinity@PLT
	.p2align 4,,10
	.p2align 3
.L1183:
	.cfi_restore_state
	movq	64(%r14), %rdx
	movq	104(%r14), %rsi
	movq	-80(%rbp), %rdi
	call	BN_mod_lshift1_quick@PLT
	testl	%eax, %eax
	je	.L1078
	movq	-80(%rbp), %rdi
	movq	(%r14), %rax
	movq	%r12, %r8
	movq	32(%rbx), %rdx
	movq	%rdi, %rcx
	movq	%rdi, %rsi
	movq	%r14, %rdi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L1078
	movq	(%r14), %rax
	movq	32(%r13), %rdx
	movq	%r12, %rcx
	movq	%r14, %rdi
	movq	-96(%rbp), %rsi
	call	*272(%rax)
	testl	%eax, %eax
	je	.L1078
	movq	(%r14), %rax
	movq	-80(%rbp), %rcx
	movq	%r12, %r8
	movq	%r14, %rdi
	movq	-96(%rbp), %rdx
	movq	-112(%rbp), %rsi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L1078
	movq	-72(%rbp), %r10
	movq	(%r14), %rax
	movq	%r12, %r8
	movq	%r14, %rdi
	movq	96(%r14), %rcx
	movq	32(%r13), %rdx
	movq	%r10, %rsi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L1078
	movq	-56(%rbp), %r9
	movq	(%r14), %rax
	movq	%r12, %r8
	movq	%r14, %rdi
	movq	16(%r13), %rcx
	movq	-80(%rbp), %rsi
	movq	16(%r9), %rdx
	call	*264(%rax)
	testl	%eax, %eax
	je	.L1078
	movq	-72(%rbp), %r10
	movq	-80(%rbp), %rdi
	movq	64(%r14), %rcx
	movq	%r10, %rdx
	movq	%rdi, %rsi
	call	BN_mod_add_quick@PLT
	testl	%eax, %eax
	je	.L1078
	movq	-80(%rbp), %rdi
	movq	(%r14), %rax
	movq	%r12, %r8
	movq	32(%rbx), %rdx
	movq	%rdi, %rcx
	movq	%rdi, %rsi
	movq	%r14, %rdi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L1078
	movq	-56(%rbp), %r9
	movq	(%r14), %rax
	movq	%r12, %r8
	movq	%r14, %rdi
	movq	32(%r13), %rcx
	movq	-104(%rbp), %rsi
	movq	16(%r9), %rdx
	call	*264(%rax)
	testl	%eax, %eax
	je	.L1078
	movq	-72(%rbp), %r10
	movq	64(%r14), %rcx
	movq	16(%r13), %rsi
	movq	-104(%rbp), %rdx
	movq	%r10, %rdi
	movq	%r10, -56(%rbp)
	call	BN_mod_add_quick@PLT
	testl	%eax, %eax
	je	.L1078
	movq	-56(%rbp), %r10
	movq	(%r14), %rax
	movq	%r12, %r8
	movq	%r14, %rdi
	movq	-80(%rbp), %rcx
	movq	%r10, %rdx
	movq	%r10, %rsi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L1078
	movq	-56(%rbp), %r10
	movq	64(%r14), %rcx
	movq	-112(%rbp), %rdx
	movq	%r10, %rsi
	movq	%r10, %rdi
	call	BN_mod_add_quick@PLT
	testl	%eax, %eax
	je	.L1078
	movq	-104(%rbp), %rdi
	movq	64(%r14), %rcx
	movq	16(%r13), %rdx
	movq	%rdi, %rsi
	call	BN_mod_sub_quick@PLT
	testl	%eax, %eax
	je	.L1078
	movq	-104(%rbp), %rdi
	movq	(%r14), %rax
	movq	%r12, %rcx
	movq	%rdi, %rdx
	movq	%rdi, %rsi
	movq	%r14, %rdi
	call	*272(%rax)
	testl	%eax, %eax
	je	.L1078
	movq	-104(%rbp), %rdi
	movq	(%r14), %rax
	movq	%r12, %r8
	movq	16(%rbx), %rcx
	movq	%rdi, %rdx
	movq	%rdi, %rsi
	movq	%r14, %rdi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L1078
	movq	-104(%rbp), %rdi
	movq	-56(%rbp), %r10
	movq	64(%r14), %rcx
	movq	%rdi, %rdx
	movq	%r10, %rsi
	call	BN_mod_sub_quick@PLT
	testl	%eax, %eax
	je	.L1078
	movq	32(%rbx), %rdx
	movq	(%r14), %rax
	movq	%r12, %r8
	movq	%r14, %rdi
	movq	-80(%rbp), %rbx
	movq	-64(%rbp), %rcx
	movq	%rbx, %rsi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L1078
	movq	(%r14), %rax
	movq	-96(%rbp), %rdx
	movq	%r12, %r8
	movq	%rbx, %rcx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L1078
	movq	(%r14), %rax
	movq	304(%rax), %r8
	testq	%r8, %r8
	je	.L1082
	movq	-80(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r14, %rdi
	movq	%rsi, %rdx
	call	*%r8
	testl	%eax, %eax
	je	.L1078
	movq	(%r14), %rax
.L1082:
	movq	-80(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r14, %rdi
	movq	%rsi, %rdx
	call	*288(%rax)
	testl	%eax, %eax
	je	.L1078
	movq	(%r14), %rax
	movq	296(%rax), %r8
	testq	%r8, %r8
	je	.L1085
	movq	-80(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r14, %rdi
	movq	%rsi, %rdx
	call	*%r8
	testl	%eax, %eax
	je	.L1078
	movq	(%r14), %rax
.L1085:
	movq	16(%r13), %rsi
	movq	-80(%rbp), %rcx
	movq	%r12, %r8
	movq	%r14, %rdi
	movq	-88(%rbp), %rdx
	call	*264(%rax)
	testl	%eax, %eax
	je	.L1078
	movq	(%r14), %rax
	movq	24(%r13), %rsi
	movq	%r12, %r8
	movq	%r14, %rdi
	movq	-80(%rbp), %rcx
	movq	-104(%rbp), %rdx
	call	*264(%rax)
	testl	%eax, %eax
	je	.L1078
	movq	(%r14), %rax
	movq	312(%rax), %rcx
	movq	32(%r13), %rax
	testq	%rcx, %rcx
	je	.L1086
	movq	%r12, %rdx
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	*%rcx
	testl	%eax, %eax
	je	.L1078
.L1087:
	movl	$1, 40(%r13)
	movl	$1, %r15d
	jmp	.L1078
.L1086:
	movl	$1, %esi
	movq	%rax, %rdi
	call	BN_set_word@PLT
	testl	%eax, %eax
	jne	.L1087
	jmp	.L1078
	.cfi_endproc
.LFE409:
	.size	ec_GFp_simple_ladder_post, .-ec_GFp_simple_ladder_post
	.p2align 4
	.globl	EC_GFp_simple_method
	.type	EC_GFp_simple_method, @function
EC_GFp_simple_method:
.LFB377:
	.cfi_startproc
	endbr64
	leaq	ret.9690(%rip), %rax
	ret
	.cfi_endproc
.LFE377:
	.size	EC_GFp_simple_method, .-EC_GFp_simple_method
	.section	.data.rel.ro,"aw"
	.align 32
	.type	ret.9690, @object
	.size	ret.9690, 432
ret.9690:
	.long	1
	.long	406
	.quad	ec_GFp_simple_group_init
	.quad	ec_GFp_simple_group_finish
	.quad	ec_GFp_simple_group_clear_finish
	.quad	ec_GFp_simple_group_copy
	.quad	ec_GFp_simple_group_set_curve
	.quad	ec_GFp_simple_group_get_curve
	.quad	ec_GFp_simple_group_get_degree
	.quad	ec_group_simple_order_bits
	.quad	ec_GFp_simple_group_check_discriminant
	.quad	ec_GFp_simple_point_init
	.quad	ec_GFp_simple_point_finish
	.quad	ec_GFp_simple_point_clear_finish
	.quad	ec_GFp_simple_point_copy
	.quad	ec_GFp_simple_point_set_to_infinity
	.quad	ec_GFp_simple_set_Jprojective_coordinates_GFp
	.quad	ec_GFp_simple_get_Jprojective_coordinates_GFp
	.quad	ec_GFp_simple_point_set_affine_coordinates
	.quad	ec_GFp_simple_point_get_affine_coordinates
	.quad	0
	.quad	0
	.quad	0
	.quad	ec_GFp_simple_add
	.quad	ec_GFp_simple_dbl
	.quad	ec_GFp_simple_invert
	.quad	ec_GFp_simple_is_at_infinity
	.quad	ec_GFp_simple_is_on_curve
	.quad	ec_GFp_simple_cmp
	.quad	ec_GFp_simple_make_affine
	.quad	ec_GFp_simple_points_make_affine
	.quad	0
	.quad	0
	.quad	0
	.quad	ec_GFp_simple_field_mul
	.quad	ec_GFp_simple_field_sqr
	.quad	0
	.quad	ec_GFp_simple_field_inv
	.quad	0
	.quad	0
	.quad	0
	.quad	ec_key_simple_priv2oct
	.quad	ec_key_simple_oct2priv
	.quad	0
	.quad	ec_key_simple_generate_key
	.quad	ec_key_simple_check_key
	.quad	ec_key_simple_generate_public_key
	.quad	0
	.quad	0
	.quad	ecdh_simple_compute_key
	.quad	0
	.quad	ec_GFp_simple_blind_coordinates
	.quad	ec_GFp_simple_ladder_pre
	.quad	ec_GFp_simple_ladder_step
	.quad	ec_GFp_simple_ladder_post
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
