	.file	"bn_word.c"
	.text
	.p2align 4
	.type	BN_div_word.part.0, @function
BN_div_word.part.0:
.LFB257:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	$64, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	call	BN_num_bits_word@PLT
	movq	%r12, %rsi
	movq	%r12, %rdi
	subl	%eax, %r14d
	movl	%r14d, %edx
	movl	%r14d, -52(%rbp)
	call	BN_lshift@PLT
	movl	%eax, %r8d
	movq	$-1, %rax
	testl	%r8d, %r8d
	je	.L1
	movslq	8(%r12), %rsi
	movl	%esi, %eax
	subl	$1, %eax
	js	.L7
	movl	%r14d, %ecx
	movq	(%r12), %rdx
	xorl	%edi, %edi
	salq	%cl, %rbx
	movq	%rbx, %r14
	movslq	%eax, %rbx
	movl	%eax, %eax
	subq	%rax, %rsi
	salq	$3, %rbx
	leaq	-16(,%rsi,8), %r13
	.p2align 4,,10
	.p2align 3
.L4:
	movq	(%rdx,%rbx), %r15
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	bn_div_words@PLT
	movq	%r14, %rdx
	movq	%r15, %rdi
	imulq	%rax, %rdx
	subq	%rdx, %rdi
	movq	(%r12), %rdx
	movq	%rax, (%rdx,%rbx)
	subq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L4
	movzbl	-52(%rbp), %ecx
	movq	%rdi, %rax
	movl	8(%r12), %esi
	shrq	%cl, %rax
.L3:
	testl	%esi, %esi
	jle	.L5
	movq	(%r12), %rcx
	movslq	%esi, %rdx
	cmpq	$0, -8(%rcx,%rdx,8)
	jne	.L1
	subl	$1, %esi
	movl	%esi, 8(%r12)
.L5:
	testl	%esi, %esi
	jne	.L1
	movl	$0, 16(%r12)
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L3
	.cfi_endproc
.LFE257:
	.size	BN_div_word.part.0, .-BN_div_word.part.0
	.p2align 4
	.globl	BN_mod_word
	.type	BN_mod_word, @function
BN_mod_word:
.LFB252:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L28
	movabsq	$4294967296, %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	subq	$16, %rsp
	cmpq	%rax, %rsi
	ja	.L31
	movl	8(%rdi), %esi
	subl	$1, %esi
	js	.L19
	movq	(%rdi), %rdi
	movslq	%esi, %rsi
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L17:
	movq	(%rdi,%rsi,8), %rcx
	movq	%rdx, %rax
	subq	$1, %rsi
	salq	$32, %rax
	movq	%rcx, %rdx
	shrq	$32, %rdx
	orq	%rdx, %rax
	xorl	%edx, %edx
	divq	%r12
	movq	%rdx, %rax
	movl	%ecx, %edx
	salq	$32, %rax
	orq	%rdx, %rax
	xorl	%edx, %edx
	divq	%r12
	testl	%esi, %esi
	jns	.L17
.L12:
	addq	$16, %rsp
	movq	%rdx, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	call	BN_dup@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L13
	movl	8(%rax), %eax
	xorl	%edx, %edx
	testl	%eax, %eax
	jne	.L32
	movq	%r13, %rdi
	movq	%rdx, -24(%rbp)
	call	BN_free@PLT
	movq	-24(%rbp), %rdx
.L33:
	addq	$16, %rsp
	movq	%rdx, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	addq	$16, %rsp
	movq	$-1, %rdx
	popq	%r12
	movq	%rdx, %rax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	BN_div_word.part.0
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%rdx, -24(%rbp)
	call	BN_free@PLT
	movq	-24(%rbp), %rdx
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L19:
	xorl	%edx, %edx
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movq	$-1, %rdx
	movq	%rdx, %rax
	ret
	.cfi_endproc
.LFE252:
	.size	BN_mod_word, .-BN_mod_word
	.p2align 4
	.globl	BN_div_word
	.type	BN_div_word, @function
BN_div_word:
.LFB253:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L36
	movl	8(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jne	.L38
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	jmp	BN_div_word.part.0
	.p2align 4,,10
	.p2align 3
.L36:
	movq	$-1, %rax
	ret
	.cfi_endproc
.LFE253:
	.size	BN_div_word, .-BN_div_word
	.p2align 4
	.globl	BN_sub_word
	.type	BN_sub_word, @function
BN_sub_word:
.LFB255:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	jne	.L40
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	subq	$16, %rsp
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L64
	movl	16(%r13), %esi
	testl	%esi, %esi
	jne	.L65
	movq	0(%r13), %rcx
	movl	8(%r13), %eax
	movq	(%rcx), %rdx
	cmpl	$1, %eax
	je	.L66
	cmpq	%rdx, %r12
	jbe	.L47
	.p2align 4,,10
	.p2align 3
.L48:
	subq	%r12, %rdx
	addq	$8, %rcx
	addl	$1, %esi
	movl	$1, %r12d
	movq	%rdx, -8(%rcx)
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	je	.L48
.L47:
	subq	%r12, %rdx
	movq	%rdx, (%rcx)
	je	.L49
.L63:
	movl	$1, %eax
.L39:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	subl	$1, %eax
	cmpl	%esi, %eax
	jne	.L63
.L51:
	movl	%esi, 8(%r13)
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L66:
	cmpq	%rdx, %r12
	ja	.L67
	subq	%r12, %rdx
	movq	%rdx, (%rcx)
	je	.L51
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L64:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	BN_set_word@PLT
	testl	%eax, %eax
	je	.L39
	movl	$1, %esi
	movq	%r13, %rdi
	movl	%eax, -20(%rbp)
	call	BN_set_negative@PLT
	movl	-20(%rbp), %eax
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L65:
	movl	$0, 16(%r13)
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	BN_add_word
	movl	$1, 16(%r13)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L67:
	subq	%rdx, %r12
	movq	%r12, (%rcx)
	movl	$1, 16(%r13)
	jmp	.L39
	.cfi_endproc
.LFE255:
	.size	BN_sub_word, .-BN_sub_word
	.p2align 4
	.globl	BN_add_word
	.type	BN_add_word, @function
BN_add_word:
.LFB254:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testq	%rsi, %rsi
	jne	.L69
.L88:
	movl	$1, %r14d
.L68:
	popq	%rbx
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	movq	%rdi, %r13
	movq	%rsi, %r12
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L89
	movl	16(%r13), %r14d
	testl	%r14d, %r14d
	jne	.L72
	movslq	8(%r13), %rbx
	xorl	%eax, %eax
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L77:
	movq	0(%r13), %rdx
	xorl	%ecx, %ecx
	leaq	(%rdx,%rax,8), %rdx
	addq	(%rdx), %r12
	setc	%cl
	movq	%r12, (%rdx)
	addq	$1, %rax
	movl	$1, %r12d
	testq	%rcx, %rcx
	je	.L88
.L73:
	cmpl	%eax, %ebx
	jg	.L77
	cmpl	%eax, %ebx
	jne	.L88
	leal	1(%rbx), %esi
	movq	%r13, %rdi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L68
	movq	0(%r13), %rax
	addl	$1, 8(%r13)
	movq	%r12, (%rax,%rbx,8)
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L89:
	popq	%rbx
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BN_set_word@PLT
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore_state
	movl	$0, 16(%r13)
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	BN_sub_word
	movq	%r13, %rdi
	movl	%eax, %r14d
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L68
	movl	16(%r13), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	sete	%al
	movl	%eax, 16(%r13)
	jmp	.L68
	.cfi_endproc
.LFE254:
	.size	BN_add_word, .-BN_add_word
	.p2align 4
	.globl	BN_mul_word
	.type	BN_mul_word, @function
BN_mul_word:
.LFB256:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %edx
	testl	%edx, %edx
	jne	.L91
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testq	%rsi, %rsi
	jne	.L93
	call	BN_set_word@PLT
.L102:
	movl	$1, %eax
.L90:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	movq	(%rdi), %rdi
	movq	%rsi, %rcx
	movq	%rdi, %rsi
	call	bn_mul_words@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L102
	movl	8(%r12), %eax
	movq	%r12, %rdi
	leal	1(%rax), %esi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L95
	movslq	8(%r12), %rax
	movq	(%r12), %rdx
	leal	1(%rax), %ecx
	movl	%ecx, 8(%r12)
	movq	%rbx, (%rdx,%rax,8)
	movl	$1, %eax
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L95:
	xorl	%eax, %eax
	jmp	.L90
	.cfi_endproc
.LFE256:
	.size	BN_mul_word, .-BN_mul_word
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
