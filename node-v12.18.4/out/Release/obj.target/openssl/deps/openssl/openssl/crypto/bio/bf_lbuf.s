	.file	"bf_lbuf.c"
	.text
	.p2align 4
	.type	linebuffer_callback_ctrl, @function
linebuffer_callback_ctrl:
.LFB441:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2
	jmp	BIO_callback_ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L2:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE441:
	.size	linebuffer_callback_ctrl, .-linebuffer_callback_ctrl
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/bio/bf_lbuf.c"
	.text
	.p2align 4
	.type	linebuffer_ctrl, @function
linebuffer_ctrl:
.LFB440:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	56(%rdi), %rbx
	cmpl	$13, %esi
	jg	.L5
	testl	%esi, %esi
	jle	.L6
	cmpl	$13, %esi
	ja	.L6
	leaq	.L8(%rip), %rcx
	movl	%esi, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L8:
	.long	.L6-.L8
	.long	.L12-.L8
	.long	.L6-.L8
	.long	.L11-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L10-.L8
	.long	.L9-.L8
	.long	.L7-.L8
	.text
	.p2align 4,,10
	.p2align 3
.L5:
	cmpl	$101, %esi
	je	.L13
	cmpl	$117, %esi
	jne	.L6
	cmpl	$10240, %edx
	jle	.L39
	cmpl	%edx, 8(%rbx)
	jne	.L42
.L39:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L7:
	.cfi_restore_state
	movslq	12(%rbx), %rax
	testq	%rax, %rax
	jne	.L4
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L38
	movq	%r14, %rcx
	movq	%r13, %rdx
	movl	$13, %esi
	jmp	.L40
.L6:
	movq	64(%r12), %rdi
	testq	%rdi, %rdi
	je	.L38
	movq	%r14, %rcx
	movq	%r13, %rdx
.L40:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_ctrl@PLT
.L12:
	.cfi_restore_state
	movq	64(%rdi), %rdi
	movl	$0, 12(%rbx)
	testq	%rdi, %rdi
	je	.L38
	movq	%r14, %rcx
	movq	%r13, %rdx
	movl	$1, %esi
	jmp	.L40
.L11:
	movslq	12(%rbx), %rax
.L4:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L10:
	.cfi_restore_state
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L38
	movl	12(%rbx), %eax
	testl	%eax, %eax
	jg	.L23
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L26:
	movl	%edx, 12(%rbx)
.L23:
	movl	$15, %esi
	movq	%r12, %rdi
	call	BIO_clear_flags@PLT
	movl	12(%rbx), %edx
	testl	%edx, %edx
	jle	.L24
	movq	64(%r12), %rdi
	movq	(%rbx), %rsi
	call	BIO_write@PLT
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	BIO_copy_next_retry@PLT
	testl	%r15d, %r15d
	jle	.L43
	movl	12(%rbx), %eax
	movl	%eax, %edx
	subl	%r15d, %edx
	cmpl	%r15d, %eax
	jle	.L26
	movq	(%rbx), %rdi
	movslq	%r15d, %rsi
	movslq	%edx, %rdx
	addq	%rdi, %rsi
	call	memmove@PLT
	movl	12(%rbx), %edx
	subl	%r15d, %edx
	jmp	.L26
.L9:
	movslq	8(%rbx), %rdx
	movq	%r14, %rdi
	movl	$1, %ecx
	movl	$117, %esi
	call	BIO_int_ctrl@PLT
	testq	%rax, %rax
	setne	%al
	addq	$24, %rsp
	popq	%rbx
	movzbl	%al, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L44:
	.cfi_restore_state
	movl	$298, %r8d
	movl	$65, %edx
	movl	$129, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L38:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	cmpq	$0, 64(%rdi)
	je	.L38
	movl	$15, %esi
	call	BIO_clear_flags@PLT
	movq	64(%r12), %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movl	$101, %esi
	call	BIO_ctrl@PLT
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	BIO_copy_next_retry@PLT
	movq	-56(%rbp), %rax
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L24:
	movl	$0, 12(%rbx)
	movq	64(%r12), %rdi
.L41:
	movq	%r14, %rcx
	movq	%r13, %rdx
	movl	$11, %esi
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L42:
	movslq	%edx, %r14
	leaq	.LC0(%rip), %rsi
	movl	$238, %edx
	movq	%r14, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L44
	movq	(%rbx), %rsi
	cmpq	%rax, %rsi
	je	.L39
	movl	12(%rbx), %eax
	cmpl	%r13d, %eax
	jg	.L21
	movslq	%eax, %r14
.L22:
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	memcpy@PLT
	movq	(%rbx), %rdi
	movl	$247, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, (%rbx)
	movl	%r13d, 8(%rbx)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L21:
	movl	%r13d, 12(%rbx)
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L43:
	movslq	%r15d, %rax
	jmp	.L4
	.cfi_endproc
.LFE440:
	.size	linebuffer_ctrl, .-linebuffer_ctrl
	.p2align 4
	.type	linebuffer_gets, @function
linebuffer_gets:
.LFB442:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L46
	jmp	BIO_gets@PLT
	.p2align 4,,10
	.p2align 3
.L46:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE442:
	.size	linebuffer_gets, .-linebuffer_gets
	.p2align 4
	.type	linebuffer_read, @function
linebuffer_read:
.LFB438:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	testq	%rsi, %rsi
	je	.L49
	movq	%rdi, %r12
	movq	64(%rdi), %rdi
	xorl	%r13d, %r13d
	testq	%rdi, %rdi
	je	.L47
	call	BIO_read@PLT
	movq	%r12, %rdi
	movl	$15, %esi
	movl	%eax, %r13d
	call	BIO_clear_flags@PLT
	movq	%r12, %rdi
	call	BIO_copy_next_retry@PLT
.L47:
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	xorl	%r13d, %r13d
	popq	%r12
	movl	%r13d, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE438:
	.size	linebuffer_read, .-linebuffer_read
	.p2align 4
	.type	linebuffer_free, @function
linebuffer_free:
.LFB437:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L57
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$88, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	56(%rdi), %rax
	movq	(%rax), %rdi
	call	CRYPTO_free@PLT
	movq	56(%rbx), %rdi
	movl	$89, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 56(%rbx)
	movl	$1, %eax
	movl	$0, 32(%rbx)
	movl	$0, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE437:
	.size	linebuffer_free, .-linebuffer_free
	.p2align 4
	.type	linebuffer_new, @function
linebuffer_new:
.LFB436:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$62, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$16, %edi
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L67
	movl	$66, %edx
	leaq	.LC0(%rip), %rsi
	movl	$10240, %edi
	movq	%rax, %r12
	call	CRYPTO_malloc@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L68
	movq	$10240, 8(%r12)
	movl	$1, %eax
	movl	$1, 32(%rbx)
	movq	%r12, 56(%rbx)
	movl	$0, 40(%rbx)
.L62:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	movl	$63, %r8d
	movl	$65, %edx
	movl	$151, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	movl	$68, %r8d
	movl	$65, %edx
	movl	$151, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$69, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	xorl	%eax, %eax
	jmp	.L62
	.cfi_endproc
.LFE436:
	.size	linebuffer_new, .-linebuffer_new
	.p2align 4
	.type	linebuffer_write.part.0, @function
linebuffer_write.part.0:
.LFB446:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	56(%rdi), %r14
	movl	%edx, -56(%rbp)
	testq	%r14, %r14
	je	.L91
	xorl	%r8d, %r8d
	cmpq	$0, 64(%rdi)
	movq	%rdi, %r13
	je	.L69
	movq	%rsi, %r12
	movl	$15, %esi
	call	BIO_clear_flags@PLT
	movl	$0, -60(%rbp)
.L87:
	movslq	-56(%rbp), %rdx
	movq	%r12, %r15
	addq	%r12, %rdx
	cmpq	%rdx, %r12
	jb	.L71
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L73:
	cmpq	%rdx, %r15
	je	.L94
.L71:
	movzbl	(%r15), %eax
	addq	$1, %r15
	cmpb	$10, %al
	jne	.L73
	movl	$1, -52(%rbp)
.L72:
	movl	12(%r14), %ebx
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L78:
	movl	%ebx, 12(%r14)
.L79:
	movl	-52(%rbp), %r8d
	movq	%r15, %rdx
	subq	%r12, %rdx
	testl	%r8d, %r8d
	jne	.L80
	movslq	8(%r14), %rsi
	movl	%esi, %eax
	subl	%ebx, %eax
	cltq
	cmpq	%rdx, %rax
	jge	.L81
.L80:
	testl	%ebx, %ebx
	jle	.L116
	movq	(%r14), %rsi
	movl	%ebx, %r9d
	testq	%rdx, %rdx
	jle	.L74
	movl	8(%r14), %r8d
	movslq	%ebx, %rdi
	addq	%rsi, %rdi
	subl	%ebx, %r8d
	movslq	%r8d, %r11
	cmpq	%rdx, %r11
	jl	.L75
	movq	%r12, %rsi
	movq	%rdx, -72(%rbp)
	movq	%r15, %r12
	call	memcpy@PLT
	movq	-72(%rbp), %rdx
	movl	12(%r14), %r9d
	subl	%edx, -56(%rbp)
	movq	(%r14), %rsi
	addl	%edx, %r9d
	addl	%edx, -60(%rbp)
	movl	%r9d, 12(%r14)
.L74:
	movq	64(%r13), %rdi
	movl	%r9d, %edx
	call	BIO_write@PLT
	movl	%eax, %r8d
	testl	%eax, %eax
	jle	.L117
	movl	12(%r14), %eax
	movl	%eax, %ebx
	subl	%r8d, %ebx
	cmpl	%eax, %r8d
	jge	.L78
	movq	(%r14), %rdi
	movslq	%r8d, %rsi
	movslq	%ebx, %rdx
	movl	%r8d, -72(%rbp)
	addq	%rdi, %rsi
	call	memmove@PLT
	movl	12(%r14), %ebx
	movl	-72(%rbp), %r8d
	subl	%r8d, %ebx
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L75:
	movq	%r12, %rsi
	movq	%r11, %rdx
	movl	%r8d, -64(%rbp)
	movq	%r11, -72(%rbp)
	call	memcpy@PLT
	movl	-64(%rbp), %r8d
	movl	12(%r14), %r9d
	movq	-72(%rbp), %r11
	subl	%r8d, -56(%rbp)
	addl	%r8d, %r9d
	addl	%r8d, -60(%rbp)
	movq	(%r14), %rsi
	movl	%r9d, 12(%r14)
	addq	%r11, %r12
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L116:
	movl	-52(%rbp), %edi
	testl	%edi, %edi
	jne	.L83
	movslq	8(%r14), %rsi
.L81:
	cmpq	%rdx, %rsi
	jl	.L83
	movl	-56(%rbp), %eax
	movl	-60(%rbp), %r8d
	testl	%eax, %eax
	setg	%al
.L89:
	testb	%al, %al
	je	.L69
	movl	-56(%rbp), %ebx
	movslq	12(%r14), %rdi
	movq	%r12, %rsi
	addq	(%r14), %rdi
	movslq	%ebx, %rdx
	call	memcpy@PLT
	addl	%ebx, 12(%r14)
	movl	-60(%rbp), %r8d
	addl	%ebx, %r8d
.L69:
	addq	$40, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	movl	$0, -52(%rbp)
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L83:
	testq	%rdx, %rdx
	jg	.L118
.L84:
	movl	-56(%rbp), %edx
	movl	-52(%rbp), %ecx
	testl	%edx, %edx
	setg	%al
	testl	%ecx, %ecx
	je	.L98
	testb	%al, %al
	jne	.L87
.L98:
	movl	-60(%rbp), %r8d
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L117:
	movl	%ebx, 12(%r14)
	movq	%r13, %rdi
	movl	%eax, -52(%rbp)
	call	BIO_copy_next_retry@PLT
	movl	-60(%rbp), %r9d
	testl	%r9d, %r9d
	jg	.L97
.L115:
	movl	-52(%rbp), %r8d
	testl	%r8d, %r8d
	jne	.L69
.L97:
	movl	-60(%rbp), %r8d
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L118:
	movq	64(%r13), %rdi
	movq	%r12, %rsi
	call	BIO_write@PLT
	movl	%eax, %r8d
	testl	%eax, %eax
	jle	.L119
	addl	%eax, -60(%rbp)
	cltq
	subl	%r8d, -56(%rbp)
	addq	%rax, %r12
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L91:
	xorl	%r8d, %r8d
	jmp	.L69
.L119:
	movq	%r13, %rdi
	movl	%eax, -52(%rbp)
	call	BIO_copy_next_retry@PLT
	movl	-60(%rbp), %esi
	testl	%esi, %esi
	jle	.L115
	jmp	.L97
	.cfi_endproc
.LFE446:
	.size	linebuffer_write.part.0, .-linebuffer_write.part.0
	.p2align 4
	.type	linebuffer_write, @function
linebuffer_write:
.LFB439:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L121
	testl	%edx, %edx
	jle	.L121
	jmp	linebuffer_write.part.0
	.p2align 4,,10
	.p2align 3
.L121:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE439:
	.size	linebuffer_write, .-linebuffer_write
	.p2align 4
	.type	linebuffer_puts, @function
linebuffer_puts:
.LFB443:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	call	strlen@PLT
	testl	%eax, %eax
	jle	.L123
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	movl	%eax, %edx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	linebuffer_write.part.0
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore_state
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE443:
	.size	linebuffer_puts, .-linebuffer_puts
	.p2align 4
	.globl	BIO_f_linebuffer
	.type	BIO_f_linebuffer, @function
BIO_f_linebuffer:
.LFB435:
	.cfi_startproc
	endbr64
	leaq	methods_linebuffer(%rip), %rax
	ret
	.cfi_endproc
.LFE435:
	.size	BIO_f_linebuffer, .-BIO_f_linebuffer
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"linebuffer"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	methods_linebuffer, @object
	.size	methods_linebuffer, 96
methods_linebuffer:
	.long	532
	.zero	4
	.quad	.LC1
	.quad	bwrite_conv
	.quad	linebuffer_write
	.quad	bread_conv
	.quad	linebuffer_read
	.quad	linebuffer_puts
	.quad	linebuffer_gets
	.quad	linebuffer_ctrl
	.quad	linebuffer_new
	.quad	linebuffer_free
	.quad	linebuffer_callback_ctrl
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
