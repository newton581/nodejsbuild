	.file	"p_lib.c"
	.text
	.p2align 4
	.globl	EVP_PKEY_bits
	.type	EVP_PKEY_bits, @function
EVP_PKEY_bits:
.LFB853:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L1
	movq	96(%rax), %rax
	testq	%rax, %rax
	je	.L1
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE853:
	.size	EVP_PKEY_bits, .-EVP_PKEY_bits
	.p2align 4
	.globl	EVP_PKEY_security_bits
	.type	EVP_PKEY_security_bits, @function
EVP_PKEY_security_bits:
.LFB854:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L12
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L14
	movq	104(%rax), %rax
	testq	%rax, %rax
	je	.L14
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L12:
	xorl	%eax, %eax
	ret
.L14:
	movl	$-2, %eax
	ret
	.cfi_endproc
.LFE854:
	.size	EVP_PKEY_security_bits, .-EVP_PKEY_security_bits
	.p2align 4
	.globl	EVP_PKEY_size
	.type	EVP_PKEY_size, @function
EVP_PKEY_size:
.LFB855:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L15
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L15
	movq	88(%rax), %rax
	testq	%rax, %rax
	je	.L15
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L15:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE855:
	.size	EVP_PKEY_size, .-EVP_PKEY_size
	.p2align 4
	.globl	EVP_PKEY_save_parameters
	.type	EVP_PKEY_save_parameters, @function
EVP_PKEY_save_parameters:
.LFB856:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	cmpl	$116, %eax
	je	.L29
	xorl	%r8d, %r8d
	cmpl	$408, %eax
	je	.L29
.L23:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	movl	48(%rdi), %r8d
	testl	%esi, %esi
	js	.L23
	movl	%esi, 48(%rdi)
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE856:
	.size	EVP_PKEY_save_parameters, .-EVP_PKEY_save_parameters
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/evp/p_lib.c"
	.text
	.p2align 4
	.globl	EVP_PKEY_copy_parameters
	.type	EVP_PKEY_copy_parameters, @function
EVP_PKEY_copy_parameters:
.LFB857:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movl	(%rdi), %r13d
	movl	(%rsi), %ebx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%r13d, %r13d
	je	.L82
	cmpl	%ebx, %r13d
	jne	.L83
	movq	16(%rsi), %rdx
	testq	%rdx, %rdx
	je	.L39
	movq	128(%rdx), %rax
	testq	%rax, %rax
	je	.L39
.L87:
	movq	%r14, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L84
.L39:
	movq	16(%r12), %rax
	testq	%rax, %rax
	je	.L40
.L47:
	movq	128(%rax), %rdx
	testq	%rdx, %rdx
	je	.L41
	movq	%r12, %rdi
	call	*%rdx
	testl	%eax, %eax
	jne	.L42
	movl	(%r14), %eax
	cmpl	%eax, (%r12)
	jne	.L40
	movq	16(%r12), %rax
	testq	%rax, %rax
	jne	.L46
	.p2align 4,,10
	.p2align 3
.L40:
	movl	$93, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$153, %edx
	xorl	%r13d, %r13d
	movl	$103, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L83:
	movl	$81, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$101, %edx
	xorl	%r13d, %r13d
	movl	$103, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
.L30:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L85
	addq	$16, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	cmpq	$0, 40(%rdi)
	movq	$0, -48(%rbp)
	je	.L32
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L33
	movq	168(%rax), %rax
	testq	%rax, %rax
	je	.L33
	call	*%rax
	movq	$0, 40(%r12)
.L33:
	movq	24(%r12), %rdi
	call	ENGINE_finish@PLT
	movq	32(%r12), %rdi
	movq	$0, 24(%r12)
	call	ENGINE_finish@PLT
	movq	$0, 32(%r12)
.L32:
	cmpl	%ebx, 4(%r12)
	jne	.L34
	movq	16(%r12), %rax
	testq	%rax, %rax
	jne	.L35
.L34:
	movq	24(%r12), %rdi
	call	ENGINE_finish@PLT
	movq	32(%r12), %rdi
	movq	$0, 24(%r12)
	call	ENGINE_finish@PLT
	leaq	-48(%rbp), %rdi
	movl	%ebx, %esi
	movq	$0, 32(%r12)
	call	EVP_PKEY_asn1_find@PLT
	testq	%rax, %rax
	je	.L86
	movq	-48(%rbp), %rdx
	movq	%rax, 16(%r12)
	movq	%rdx, 24(%r12)
	movl	(%rax), %edx
	movl	%ebx, 4(%r12)
	movl	%edx, (%r12)
.L35:
	movq	16(%r14), %rdx
	testq	%rdx, %rdx
	je	.L47
	movq	128(%rdx), %rax
	testq	%rax, %rax
	jne	.L87
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L41:
	movl	(%r12), %ecx
	cmpl	%ecx, (%r14)
	jne	.L40
.L46:
	movq	144(%rax), %rax
	testq	%rax, %rax
	je	.L40
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*%rax
	movl	%eax, %r13d
	cmpl	$1, %eax
	jne	.L40
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L42:
	movq	16(%r14), %rax
	xorl	%r13d, %r13d
	testq	%rax, %rax
	je	.L30
	movq	136(%rax), %rax
	testq	%rax, %rax
	je	.L30
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*%rax
	movl	%eax, %r13d
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L86:
	movl	$210, %r8d
	movl	$156, %edx
	movl	$158, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L84:
	movl	$86, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$103, %edx
	xorl	%r13d, %r13d
	movl	$103, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	jmp	.L30
.L85:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE857:
	.size	EVP_PKEY_copy_parameters, .-EVP_PKEY_copy_parameters
	.p2align 4
	.globl	EVP_PKEY_missing_parameters
	.type	EVP_PKEY_missing_parameters, @function
EVP_PKEY_missing_parameters:
.LFB858:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L88
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L88
	movq	128(%rax), %rax
	testq	%rax, %rax
	je	.L88
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L88:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE858:
	.size	EVP_PKEY_missing_parameters, .-EVP_PKEY_missing_parameters
	.p2align 4
	.globl	EVP_PKEY_cmp_parameters
	.type	EVP_PKEY_cmp_parameters, @function
EVP_PKEY_cmp_parameters:
.LFB859:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	cmpl	%eax, (%rdi)
	jne	.L98
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L100
	movq	144(%rax), %rax
	testq	%rax, %rax
	je	.L100
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L100:
	movl	$-2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE859:
	.size	EVP_PKEY_cmp_parameters, .-EVP_PKEY_cmp_parameters
	.p2align 4
	.globl	EVP_PKEY_cmp
	.type	EVP_PKEY_cmp, @function
EVP_PKEY_cmp:
.LFB860:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	cmpl	%eax, (%rdi)
	jne	.L106
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	16(%rdi), %rax
	movq	%rdi, %r12
	testq	%rax, %rax
	je	.L105
	movq	144(%rax), %rdx
	movq	%rsi, %r13
	testq	%rdx, %rdx
	je	.L104
	call	*%rdx
	testl	%eax, %eax
	jle	.L101
	movq	16(%r12), %rax
.L104:
	movq	48(%rax), %rax
	testq	%rax, %rax
	je	.L105
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore_state
	movl	$-2, %eax
.L101:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE860:
	.size	EVP_PKEY_cmp, .-EVP_PKEY_cmp
	.p2align 4
	.globl	EVP_PKEY_new
	.type	EVP_PKEY_new, @function
EVP_PKEY_new:
.LFB861:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$142, %edx
	movl	$72, %edi
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L121
	movq	$0, (%rax)
	movl	$1, 8(%rax)
	mfence
	movl	$1, 48(%rax)
	call	CRYPTO_THREAD_lock_new@PLT
	movq	%rax, 64(%r12)
	testq	%rax, %rax
	je	.L122
.L117:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	movl	$145, %r8d
	movl	$65, %edx
	movl	$106, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L122:
	movl	$154, %r8d
	movl	$65, %edx
	movl	$106, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movl	$155, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L117
	.cfi_endproc
.LFE861:
	.size	EVP_PKEY_new, .-EVP_PKEY_new
	.p2align 4
	.globl	EVP_PKEY_up_ref
	.type	EVP_PKEY_up_ref, @function
EVP_PKEY_up_ref:
.LFB862:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	lock xaddl	%eax, 8(%rdi)
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE862:
	.size	EVP_PKEY_up_ref, .-EVP_PKEY_up_ref
	.p2align 4
	.globl	EVP_PKEY_get_raw_private_key
	.type	EVP_PKEY_get_raw_private_key, @function
EVP_PKEY_get_raw_private_key:
.LFB866:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	16(%rdi), %rax
	movq	264(%rax), %rax
	testq	%rax, %rax
	je	.L129
	call	*%rax
	testl	%eax, %eax
	je	.L130
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	movl	$293, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$182, %edx
	movl	%eax, -4(%rbp)
	movl	$202, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	movl	-4(%rbp), %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	movl	$287, %r8d
	movl	$150, %edx
	movl	$202, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE866:
	.size	EVP_PKEY_get_raw_private_key, .-EVP_PKEY_get_raw_private_key
	.p2align 4
	.globl	EVP_PKEY_get_raw_public_key
	.type	EVP_PKEY_get_raw_public_key, @function
EVP_PKEY_get_raw_public_key:
.LFB867:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	16(%rdi), %rax
	movq	272(%rax), %rax
	testq	%rax, %rax
	je	.L136
	call	*%rax
	testl	%eax, %eax
	je	.L137
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
	movl	$310, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$182, %edx
	movl	%eax, -4(%rbp)
	movl	$203, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	movl	-4(%rbp), %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	.cfi_restore_state
	movl	$304, %r8d
	movl	$150, %edx
	movl	$203, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE867:
	.size	EVP_PKEY_get_raw_public_key, .-EVP_PKEY_get_raw_public_key
	.p2align 4
	.globl	EVP_PKEY_set_type
	.type	EVP_PKEY_set_type, @function
EVP_PKEY_set_type:
.LFB869:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$0, -32(%rbp)
	testq	%rdi, %rdi
	je	.L139
	cmpq	$0, 40(%rdi)
	movq	%rdi, %rbx
	je	.L140
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L141
	movq	168(%rax), %rax
	testq	%rax, %rax
	je	.L141
	call	*%rax
	movq	$0, 40(%rbx)
.L141:
	movq	24(%rbx), %rdi
	call	ENGINE_finish@PLT
	movq	$0, 24(%rbx)
	movq	32(%rbx), %rdi
	call	ENGINE_finish@PLT
	movq	$0, 32(%rbx)
.L140:
	cmpl	4(%rbx), %r12d
	jne	.L142
	cmpq	$0, 16(%rbx)
	jne	.L158
.L142:
	movq	24(%rbx), %rdi
	call	ENGINE_finish@PLT
	movq	$0, 24(%rbx)
	movq	32(%rbx), %rdi
	call	ENGINE_finish@PLT
	movq	$0, 32(%rbx)
	leaq	-32(%rbp), %rdi
	movl	%r12d, %esi
	call	EVP_PKEY_asn1_find@PLT
	testq	%rax, %rax
	je	.L145
	movq	%rax, 16(%rbx)
	movq	-32(%rbp), %rdx
	movl	(%rax), %eax
	movl	%r12d, 4(%rbx)
	movq	%rdx, 24(%rbx)
	movl	%eax, (%rbx)
.L158:
	movl	$1, %eax
.L138:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L159
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	leaq	-32(%rbp), %rdi
	call	EVP_PKEY_asn1_find@PLT
	movq	-32(%rbp), %rdi
	movq	%rax, %rbx
	call	ENGINE_finish@PLT
	testq	%rbx, %rbx
	jne	.L158
.L145:
	movl	$210, %r8d
	movl	$156, %edx
	movl	$158, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L138
.L159:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE869:
	.size	EVP_PKEY_set_type, .-EVP_PKEY_set_type
	.p2align 4
	.globl	EVP_PKEY_set_type_str
	.type	EVP_PKEY_set_type_str, @function
EVP_PKEY_set_type_str:
.LFB870:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	testq	%rdi, %rdi
	je	.L161
	cmpq	$0, 40(%rdi)
	je	.L162
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L163
	movq	168(%rax), %rax
	testq	%rax, %rax
	je	.L163
	call	*%rax
	movq	$0, 40(%rbx)
.L163:
	movq	24(%rbx), %rdi
	call	ENGINE_finish@PLT
	movq	$0, 24(%rbx)
	movq	32(%rbx), %rdi
	call	ENGINE_finish@PLT
	movq	$0, 32(%rbx)
.L162:
	movl	4(%rbx), %eax
	testl	%eax, %eax
	jne	.L164
	cmpq	$0, 16(%rbx)
	jne	.L187
.L164:
	movq	24(%rbx), %rdi
	call	ENGINE_finish@PLT
	movq	$0, 24(%rbx)
	movq	32(%rbx), %rdi
	call	ENGINE_finish@PLT
	movq	$0, 32(%rbx)
.L161:
	leaq	-48(%rbp), %rdi
	testq	%r12, %r12
	je	.L166
	movq	%r12, %rsi
	movl	%r13d, %edx
	call	EVP_PKEY_asn1_find_str@PLT
	movq	%rax, %r12
.L167:
	testq	%rbx, %rbx
	je	.L188
	testq	%r12, %r12
	je	.L170
	movq	-48(%rbp), %rax
	movq	%r12, 16(%rbx)
	movq	%rax, 24(%rbx)
	movl	(%r12), %eax
	movl	$0, 4(%rbx)
	movl	%eax, (%rbx)
.L187:
	movl	$1, %eax
.L160:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L189
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L188:
	.cfi_restore_state
	movq	-48(%rbp), %rdi
	call	ENGINE_finish@PLT
	testq	%r12, %r12
	jne	.L187
.L170:
	movl	$210, %r8d
	movl	$156, %edx
	movl	$158, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L166:
	xorl	%esi, %esi
	call	EVP_PKEY_asn1_find@PLT
	movq	%rax, %r12
	jmp	.L167
.L189:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE870:
	.size	EVP_PKEY_set_type_str, .-EVP_PKEY_set_type_str
	.p2align 4
	.globl	EVP_PKEY_set_alias_type
	.type	EVP_PKEY_set_alias_type, @function
EVP_PKEY_set_alias_type:
.LFB871:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$1, %eax
	cmpl	%esi, (%rdi)
	je	.L190
	leaq	-48(%rbp), %r14
	movq	%rdi, %rbx
	movl	%esi, %r12d
	movq	%r14, %rdi
	call	EVP_PKEY_asn1_find@PLT
	testq	%rax, %rax
	je	.L192
	movq	-48(%rbp), %rdi
	movl	(%rax), %r13d
	call	ENGINE_finish@PLT
	movl	(%rbx), %esi
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	EVP_PKEY_asn1_find@PLT
	testq	%rax, %rax
	je	.L193
.L195:
	movl	(%rax), %r14d
.L193:
	movq	-48(%rbp), %rdi
	call	ENGINE_finish@PLT
	cmpl	%r13d, %r14d
	jne	.L204
.L194:
	movl	%r12d, (%rbx)
	movl	$1, %eax
.L190:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L205
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L204:
	.cfi_restore_state
	movl	$371, %r8d
	movl	$156, %edx
	movl	$206, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L192:
	movq	-48(%rbp), %rdi
	xorl	%r13d, %r13d
	call	ENGINE_finish@PLT
	movl	(%rbx), %esi
	movq	%r14, %rdi
	call	EVP_PKEY_asn1_find@PLT
	testq	%rax, %rax
	jne	.L195
	movq	-48(%rbp), %rdi
	call	ENGINE_finish@PLT
	jmp	.L194
.L205:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE871:
	.size	EVP_PKEY_set_alias_type, .-EVP_PKEY_set_alias_type
	.p2align 4
	.globl	EVP_PKEY_set1_engine
	.type	EVP_PKEY_set1_engine, @function
EVP_PKEY_set1_engine:
.LFB872:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	testq	%rsi, %rsi
	je	.L207
	movq	%rsi, %rdi
	call	ENGINE_init@PLT
	testl	%eax, %eax
	je	.L214
	movl	(%rbx), %esi
	movq	%r12, %rdi
	call	ENGINE_get_pkey_meth@PLT
	testq	%rax, %rax
	je	.L215
.L207:
	movq	32(%rbx), %rdi
	call	ENGINE_finish@PLT
	movq	%r12, 32(%rbx)
	movl	$1, %eax
.L206:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L214:
	.cfi_restore_state
	movl	$384, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$38, %edx
	movl	%eax, -20(%rbp)
	movl	$187, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	movl	-20(%rbp), %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L215:
	.cfi_restore_state
	movq	%r12, %rdi
	call	ENGINE_finish@PLT
	movl	$389, %r8d
	movl	$156, %edx
	leaq	.LC0(%rip), %rcx
	movl	$187, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L206
	.cfi_endproc
.LFE872:
	.size	EVP_PKEY_set1_engine, .-EVP_PKEY_set1_engine
	.p2align 4
	.globl	EVP_PKEY_get0_engine
	.type	EVP_PKEY_get0_engine, @function
EVP_PKEY_get0_engine:
.LFB873:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE873:
	.size	EVP_PKEY_get0_engine, .-EVP_PKEY_get0_engine
	.p2align 4
	.globl	EVP_PKEY_assign
	.type	EVP_PKEY_assign, @function
EVP_PKEY_assign:
.LFB874:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L217
	cmpq	$0, 40(%rdi)
	movq	%rdi, %rbx
	movl	%esi, %r12d
	movq	%rdx, %r13
	movq	$0, -48(%rbp)
	je	.L219
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L220
	movq	168(%rax), %rax
	testq	%rax, %rax
	je	.L220
	call	*%rax
	movq	$0, 40(%rbx)
.L220:
	movq	24(%rbx), %rdi
	call	ENGINE_finish@PLT
	movq	$0, 24(%rbx)
	movq	32(%rbx), %rdi
	call	ENGINE_finish@PLT
	movq	$0, 32(%rbx)
.L219:
	cmpl	4(%rbx), %r12d
	jne	.L221
	cmpq	$0, 16(%rbx)
	je	.L221
	movq	%r13, 40(%rbx)
	xorl	%eax, %eax
	testq	%r13, %r13
	setne	%al
	.p2align 4,,10
	.p2align 3
.L217:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L234
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L221:
	.cfi_restore_state
	movq	24(%rbx), %rdi
	call	ENGINE_finish@PLT
	movq	$0, 24(%rbx)
	movq	32(%rbx), %rdi
	call	ENGINE_finish@PLT
	movq	$0, 32(%rbx)
	leaq	-48(%rbp), %rdi
	movl	%r12d, %esi
	call	EVP_PKEY_asn1_find@PLT
	testq	%rax, %rax
	je	.L235
	movq	%rax, 16(%rbx)
	movl	(%rax), %eax
	movq	-48(%rbp), %rdx
	movl	%r12d, 4(%rbx)
	movl	%eax, (%rbx)
	xorl	%eax, %eax
	testq	%r13, %r13
	movq	%rdx, 24(%rbx)
	setne	%al
	movq	%r13, 40(%rbx)
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L235:
	movl	$210, %r8d
	movl	$156, %edx
	movl	$158, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L217
.L234:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE874:
	.size	EVP_PKEY_assign, .-EVP_PKEY_assign
	.p2align 4
	.globl	EVP_PKEY_get0
	.type	EVP_PKEY_get0, @function
EVP_PKEY_get0:
.LFB875:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE875:
	.size	EVP_PKEY_get0, .-EVP_PKEY_get0
	.p2align 4
	.globl	EVP_PKEY_get0_hmac
	.type	EVP_PKEY_get0_hmac, @function
EVP_PKEY_get0_hmac:
.LFB876:
	.cfi_startproc
	endbr64
	cmpl	$855, (%rdi)
	jne	.L244
	movq	40(%rdi), %rax
	movslq	(%rax), %rdx
	movq	8(%rax), %rax
	movq	%rdx, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L244:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$174, %edx
	movl	$183, %esi
	movl	$6, %edi
	movl	$420, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE876:
	.size	EVP_PKEY_get0_hmac, .-EVP_PKEY_get0_hmac
	.p2align 4
	.globl	EVP_PKEY_get0_poly1305
	.type	EVP_PKEY_get0_poly1305, @function
EVP_PKEY_get0_poly1305:
.LFB877:
	.cfi_startproc
	endbr64
	cmpl	$1061, (%rdi)
	jne	.L252
	movq	40(%rdi), %rax
	movslq	(%rax), %rdx
	movq	8(%rax), %rax
	movq	%rdx, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L252:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$164, %edx
	movl	$184, %esi
	movl	$6, %edi
	movl	$433, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE877:
	.size	EVP_PKEY_get0_poly1305, .-EVP_PKEY_get0_poly1305
	.p2align 4
	.globl	EVP_PKEY_get0_siphash
	.type	EVP_PKEY_get0_siphash, @function
EVP_PKEY_get0_siphash:
.LFB878:
	.cfi_startproc
	endbr64
	cmpl	$1062, (%rdi)
	jne	.L260
	movq	40(%rdi), %rax
	movslq	(%rax), %rdx
	movq	8(%rax), %rax
	movq	%rdx, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L260:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$175, %edx
	movl	$172, %esi
	movl	$6, %edi
	movl	$448, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE878:
	.size	EVP_PKEY_get0_siphash, .-EVP_PKEY_get0_siphash
	.p2align 4
	.globl	EVP_PKEY_set1_RSA
	.type	EVP_PKEY_set1_RSA, @function
EVP_PKEY_set1_RSA:
.LFB879:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L281
	cmpq	$0, 40(%rdi)
	movq	$0, -32(%rbp)
	movq	%rdi, %rbx
	movq	%rsi, %r12
	je	.L264
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L265
	movq	168(%rax), %rax
	testq	%rax, %rax
	je	.L265
	call	*%rax
	movq	$0, 40(%rbx)
.L265:
	movq	24(%rbx), %rdi
	call	ENGINE_finish@PLT
	movq	$0, 24(%rbx)
	movq	32(%rbx), %rdi
	call	ENGINE_finish@PLT
	movq	$0, 32(%rbx)
.L264:
	cmpl	$6, 4(%rbx)
	jne	.L266
	cmpq	$0, 16(%rbx)
	je	.L266
.L267:
	movq	%r12, 40(%rbx)
	testq	%r12, %r12
	je	.L281
	movq	%r12, %rdi
	call	RSA_up_ref@PLT
	movl	$1, %eax
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L283:
	movl	$210, %r8d
	movl	$156, %edx
	movl	$158, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L281:
	xorl	%eax, %eax
.L261:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L282
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L266:
	.cfi_restore_state
	movq	24(%rbx), %rdi
	call	ENGINE_finish@PLT
	movq	$0, 24(%rbx)
	movq	32(%rbx), %rdi
	call	ENGINE_finish@PLT
	leaq	-32(%rbp), %rdi
	movl	$6, %esi
	movq	$0, 32(%rbx)
	call	EVP_PKEY_asn1_find@PLT
	testq	%rax, %rax
	je	.L283
	movq	%rax, 16(%rbx)
	movq	-32(%rbp), %rdx
	movl	(%rax), %eax
	movl	$6, 4(%rbx)
	movq	%rdx, 24(%rbx)
	movl	%eax, (%rbx)
	jmp	.L267
.L282:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE879:
	.size	EVP_PKEY_set1_RSA, .-EVP_PKEY_set1_RSA
	.p2align 4
	.globl	EVP_PKEY_get0_RSA
	.type	EVP_PKEY_get0_RSA, @function
EVP_PKEY_get0_RSA:
.LFB880:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	cmpl	$6, %eax
	je	.L285
	cmpl	$912, %eax
	jne	.L297
.L285:
	movq	40(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L297:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$127, %edx
	movl	$121, %esi
	movl	$6, %edi
	movl	$469, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE880:
	.size	EVP_PKEY_get0_RSA, .-EVP_PKEY_get0_RSA
	.p2align 4
	.globl	EVP_PKEY_get1_RSA
	.type	EVP_PKEY_get1_RSA, @function
EVP_PKEY_get1_RSA:
.LFB881:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movl	(%rdi), %eax
	cmpl	$6, %eax
	je	.L299
	cmpl	$912, %eax
	jne	.L311
.L299:
	movq	40(%rdi), %r12
	testq	%r12, %r12
	je	.L298
	movq	%r12, %rdi
	call	RSA_up_ref@PLT
.L298:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L311:
	.cfi_restore_state
	movl	$469, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$127, %edx
	xorl	%r12d, %r12d
	movl	$121, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	jmp	.L298
	.cfi_endproc
.LFE881:
	.size	EVP_PKEY_get1_RSA, .-EVP_PKEY_get1_RSA
	.p2align 4
	.globl	EVP_PKEY_set1_DSA
	.type	EVP_PKEY_set1_DSA, @function
EVP_PKEY_set1_DSA:
.LFB882:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L332
	cmpq	$0, 40(%rdi)
	movq	$0, -32(%rbp)
	movq	%rdi, %rbx
	movq	%rsi, %r12
	je	.L315
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L316
	movq	168(%rax), %rax
	testq	%rax, %rax
	je	.L316
	call	*%rax
	movq	$0, 40(%rbx)
.L316:
	movq	24(%rbx), %rdi
	call	ENGINE_finish@PLT
	movq	$0, 24(%rbx)
	movq	32(%rbx), %rdi
	call	ENGINE_finish@PLT
	movq	$0, 32(%rbx)
.L315:
	cmpl	$116, 4(%rbx)
	jne	.L317
	cmpq	$0, 16(%rbx)
	je	.L317
.L318:
	movq	%r12, 40(%rbx)
	testq	%r12, %r12
	je	.L332
	movq	%r12, %rdi
	call	DSA_up_ref@PLT
	movl	$1, %eax
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L334:
	movl	$210, %r8d
	movl	$156, %edx
	movl	$158, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L332:
	xorl	%eax, %eax
.L312:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L333
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L317:
	.cfi_restore_state
	movq	24(%rbx), %rdi
	call	ENGINE_finish@PLT
	movq	$0, 24(%rbx)
	movq	32(%rbx), %rdi
	call	ENGINE_finish@PLT
	leaq	-32(%rbp), %rdi
	movl	$116, %esi
	movq	$0, 32(%rbx)
	call	EVP_PKEY_asn1_find@PLT
	testq	%rax, %rax
	je	.L334
	movq	%rax, 16(%rbx)
	movq	-32(%rbp), %rdx
	movl	(%rax), %eax
	movl	$116, 4(%rbx)
	movq	%rdx, 24(%rbx)
	movl	%eax, (%rbx)
	jmp	.L318
.L333:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE882:
	.size	EVP_PKEY_set1_DSA, .-EVP_PKEY_set1_DSA
	.p2align 4
	.globl	EVP_PKEY_get0_DSA
	.type	EVP_PKEY_get0_DSA, @function
EVP_PKEY_get0_DSA:
.LFB883:
	.cfi_startproc
	endbr64
	cmpl	$116, (%rdi)
	jne	.L342
	movq	40(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L342:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$129, %edx
	movl	$120, %esi
	movl	$6, %edi
	movl	$496, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE883:
	.size	EVP_PKEY_get0_DSA, .-EVP_PKEY_get0_DSA
	.p2align 4
	.globl	EVP_PKEY_get1_DSA
	.type	EVP_PKEY_get1_DSA, @function
EVP_PKEY_get1_DSA:
.LFB884:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	cmpl	$116, (%rdi)
	jne	.L350
	movq	40(%rdi), %r12
	testq	%r12, %r12
	je	.L343
	movq	%r12, %rdi
	call	DSA_up_ref@PLT
.L343:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L350:
	.cfi_restore_state
	movl	$496, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$129, %edx
	xorl	%r12d, %r12d
	movl	$120, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE884:
	.size	EVP_PKEY_get1_DSA, .-EVP_PKEY_get1_DSA
	.p2align 4
	.globl	EVP_PKEY_set1_EC_KEY
	.type	EVP_PKEY_set1_EC_KEY, @function
EVP_PKEY_set1_EC_KEY:
.LFB885:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L371
	cmpq	$0, 40(%rdi)
	movq	$0, -32(%rbp)
	movq	%rdi, %rbx
	movq	%rsi, %r12
	je	.L354
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L355
	movq	168(%rax), %rax
	testq	%rax, %rax
	je	.L355
	call	*%rax
	movq	$0, 40(%rbx)
.L355:
	movq	24(%rbx), %rdi
	call	ENGINE_finish@PLT
	movq	$0, 24(%rbx)
	movq	32(%rbx), %rdi
	call	ENGINE_finish@PLT
	movq	$0, 32(%rbx)
.L354:
	cmpl	$408, 4(%rbx)
	jne	.L356
	cmpq	$0, 16(%rbx)
	je	.L356
.L357:
	movq	%r12, 40(%rbx)
	testq	%r12, %r12
	je	.L371
	movq	%r12, %rdi
	call	EC_KEY_up_ref@PLT
	movl	$1, %eax
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L373:
	movl	$210, %r8d
	movl	$156, %edx
	movl	$158, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L371:
	xorl	%eax, %eax
.L351:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L372
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L356:
	.cfi_restore_state
	movq	24(%rbx), %rdi
	call	ENGINE_finish@PLT
	movq	$0, 24(%rbx)
	movq	32(%rbx), %rdi
	call	ENGINE_finish@PLT
	leaq	-32(%rbp), %rdi
	movl	$408, %esi
	movq	$0, 32(%rbx)
	call	EVP_PKEY_asn1_find@PLT
	testq	%rax, %rax
	je	.L373
	movq	%rax, 16(%rbx)
	movq	-32(%rbp), %rdx
	movl	(%rax), %eax
	movl	$408, 4(%rbx)
	movq	%rdx, 24(%rbx)
	movl	%eax, (%rbx)
	jmp	.L357
.L372:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE885:
	.size	EVP_PKEY_set1_EC_KEY, .-EVP_PKEY_set1_EC_KEY
	.p2align 4
	.globl	EVP_PKEY_get0_EC_KEY
	.type	EVP_PKEY_get0_EC_KEY, @function
EVP_PKEY_get0_EC_KEY:
.LFB886:
	.cfi_startproc
	endbr64
	cmpl	$408, (%rdi)
	jne	.L381
	movq	40(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L381:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$142, %edx
	movl	$131, %esi
	movl	$6, %edi
	movl	$524, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE886:
	.size	EVP_PKEY_get0_EC_KEY, .-EVP_PKEY_get0_EC_KEY
	.p2align 4
	.globl	EVP_PKEY_get1_EC_KEY
	.type	EVP_PKEY_get1_EC_KEY, @function
EVP_PKEY_get1_EC_KEY:
.LFB887:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	cmpl	$408, (%rdi)
	jne	.L389
	movq	40(%rdi), %r12
	testq	%r12, %r12
	je	.L382
	movq	%r12, %rdi
	call	EC_KEY_up_ref@PLT
.L382:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L389:
	.cfi_restore_state
	movl	$524, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$142, %edx
	xorl	%r12d, %r12d
	movl	$131, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE887:
	.size	EVP_PKEY_get1_EC_KEY, .-EVP_PKEY_get1_EC_KEY
	.p2align 4
	.globl	EVP_PKEY_set1_DH
	.type	EVP_PKEY_set1_DH, @function
EVP_PKEY_set1_DH:
.LFB888:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	DH_get0_q@PLT
	cmpq	$1, %rax
	sbbl	%r12d, %r12d
	andl	$-892, %r12d
	addl	$920, %r12d
	testq	%rbx, %rbx
	je	.L412
	cmpq	$0, 40(%rbx)
	movq	$0, -48(%rbp)
	je	.L394
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.L395
	movq	168(%rax), %rax
	testq	%rax, %rax
	je	.L395
	movq	%rbx, %rdi
	call	*%rax
	movq	$0, 40(%rbx)
.L395:
	movq	24(%rbx), %rdi
	call	ENGINE_finish@PLT
	movq	$0, 24(%rbx)
	movq	32(%rbx), %rdi
	call	ENGINE_finish@PLT
	movq	$0, 32(%rbx)
.L394:
	cmpl	4(%rbx), %r12d
	jne	.L396
	cmpq	$0, 16(%rbx)
	je	.L396
.L397:
	movq	%r13, 40(%rbx)
	testq	%r13, %r13
	je	.L412
	movq	%r13, %rdi
	call	DH_up_ref@PLT
	movl	$1, %eax
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L414:
	movl	$210, %r8d
	movl	$156, %edx
	movl	$158, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L412:
	xorl	%eax, %eax
.L390:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L413
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L396:
	.cfi_restore_state
	movq	24(%rbx), %rdi
	call	ENGINE_finish@PLT
	movq	$0, 24(%rbx)
	movq	32(%rbx), %rdi
	call	ENGINE_finish@PLT
	movq	$0, 32(%rbx)
	leaq	-48(%rbp), %rdi
	movl	%r12d, %esi
	call	EVP_PKEY_asn1_find@PLT
	testq	%rax, %rax
	je	.L414
	movq	%rax, 16(%rbx)
	movq	-48(%rbp), %rdx
	movl	(%rax), %eax
	movl	%r12d, 4(%rbx)
	movq	%rdx, 24(%rbx)
	movl	%eax, (%rbx)
	jmp	.L397
.L413:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE888:
	.size	EVP_PKEY_set1_DH, .-EVP_PKEY_set1_DH
	.p2align 4
	.globl	EVP_PKEY_get0_DH
	.type	EVP_PKEY_get0_DH, @function
EVP_PKEY_get0_DH:
.LFB889:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	cmpl	$28, %eax
	je	.L416
	cmpl	$920, %eax
	jne	.L428
.L416:
	movq	40(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L428:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$128, %edx
	movl	$119, %esi
	movl	$6, %edi
	movl	$554, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE889:
	.size	EVP_PKEY_get0_DH, .-EVP_PKEY_get0_DH
	.p2align 4
	.globl	EVP_PKEY_get1_DH
	.type	EVP_PKEY_get1_DH, @function
EVP_PKEY_get1_DH:
.LFB890:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movl	(%rdi), %eax
	cmpl	$28, %eax
	je	.L430
	cmpl	$920, %eax
	jne	.L442
.L430:
	movq	40(%rdi), %r12
	testq	%r12, %r12
	je	.L429
	movq	%r12, %rdi
	call	DH_up_ref@PLT
.L429:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L442:
	.cfi_restore_state
	movl	$554, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$128, %edx
	xorl	%r12d, %r12d
	movl	$119, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	jmp	.L429
	.cfi_endproc
.LFE890:
	.size	EVP_PKEY_get1_DH, .-EVP_PKEY_get1_DH
	.p2align 4
	.globl	EVP_PKEY_type
	.type	EVP_PKEY_type, @function
EVP_PKEY_type:
.LFB891:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	xorl	%r12d, %r12d
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	EVP_PKEY_asn1_find@PLT
	testq	%rax, %rax
	je	.L444
	movl	(%rax), %r12d
.L444:
	movq	-32(%rbp), %rdi
	call	ENGINE_finish@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L450
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L450:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE891:
	.size	EVP_PKEY_type, .-EVP_PKEY_type
	.p2align 4
	.globl	EVP_PKEY_id
	.type	EVP_PKEY_id, @function
EVP_PKEY_id:
.LFB892:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	ret
	.cfi_endproc
.LFE892:
	.size	EVP_PKEY_id, .-EVP_PKEY_id
	.p2align 4
	.globl	EVP_PKEY_base_id
	.type	EVP_PKEY_base_id, @function
EVP_PKEY_base_id:
.LFB893:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	xorl	%r12d, %r12d
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movl	(%r8), %esi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	EVP_PKEY_asn1_find@PLT
	testq	%rax, %rax
	je	.L453
	movl	(%rax), %r12d
.L453:
	movq	-32(%rbp), %rdi
	call	ENGINE_finish@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L459
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L459:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE893:
	.size	EVP_PKEY_base_id, .-EVP_PKEY_base_id
	.p2align 4
	.globl	EVP_PKEY_free
	.type	EVP_PKEY_free, @function
EVP_PKEY_free:
.LFB894:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L474
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	lock xaddl	%eax, 8(%rdi)
	subl	$1, %eax
	testl	%eax, %eax
	je	.L477
	jle	.L464
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L477:
	.cfi_restore_state
.L464:
	movq	16(%r12), %rax
	testq	%rax, %rax
	je	.L466
	movq	168(%rax), %rax
	testq	%rax, %rax
	je	.L466
	movq	%r12, %rdi
	call	*%rax
	movq	$0, 40(%r12)
.L466:
	movq	24(%r12), %rdi
	call	ENGINE_finish@PLT
	movq	32(%r12), %rdi
	movq	$0, 24(%r12)
	call	ENGINE_finish@PLT
	movq	64(%r12), %rdi
	movq	$0, 32(%r12)
	call	CRYPTO_THREAD_lock_free@PLT
	movq	56(%r12), %rdi
	movq	X509_ATTRIBUTE_free@GOTPCREL(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$610, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L474:
	ret
	.cfi_endproc
.LFE894:
	.size	EVP_PKEY_free, .-EVP_PKEY_free
	.p2align 4
	.globl	EVP_PKEY_new_raw_private_key
	.type	EVP_PKEY_new_raw_private_key, @function
EVP_PKEY_new_raw_private_key:
.LFB864:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edi, %ebx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_PKEY_new
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L479
	testq	%r15, %r15
	leaq	-64(%rbp), %rdx
	movl	$0, %eax
	movq	%r15, -64(%rbp)
	cmove	%rdx, %rax
	cmpq	$0, 40(%r12)
	movq	%rax, %r15
	je	.L481
	movq	16(%r12), %rax
	testq	%rax, %rax
	je	.L482
	movq	168(%rax), %rax
	testq	%rax, %rax
	je	.L482
	movq	%r12, %rdi
	call	*%rax
	movq	$0, 40(%r12)
.L482:
	movq	24(%r12), %rdi
	call	ENGINE_finish@PLT
	movq	32(%r12), %rdi
	movq	$0, 24(%r12)
	call	ENGINE_finish@PLT
	movq	$0, 32(%r12)
.L481:
	cmpl	4(%r12), %ebx
	jne	.L483
	movq	16(%r12), %rax
	testq	%rax, %rax
	je	.L483
.L484:
	movq	248(%rax), %rax
	testq	%rax, %rax
	je	.L503
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L504
.L478:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L505
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L483:
	.cfi_restore_state
	movq	24(%r12), %rdi
	call	ENGINE_finish@PLT
	movq	32(%r12), %rdi
	movq	$0, 24(%r12)
	call	ENGINE_finish@PLT
	movl	%ebx, %esi
	movq	%r15, %rdi
	movq	$0, 32(%r12)
	call	EVP_PKEY_asn1_find@PLT
	testq	%rax, %rax
	je	.L506
	movq	-64(%rbp), %rdx
	movq	%rax, 16(%r12)
	movq	%rdx, 24(%r12)
	movl	(%rax), %edx
	movl	%ebx, 4(%r12)
	movl	%edx, (%r12)
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L506:
	movl	$210, %r8d
	movl	$156, %edx
	movl	$158, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L479:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	EVP_PKEY_free
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L504:
	movl	$242, %r8d
	movl	$180, %edx
	movl	$191, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L503:
	movl	$236, %r8d
	movl	$150, %edx
	movl	$191, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L479
.L505:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE864:
	.size	EVP_PKEY_new_raw_private_key, .-EVP_PKEY_new_raw_private_key
	.p2align 4
	.globl	EVP_PKEY_new_raw_public_key
	.type	EVP_PKEY_new_raw_public_key, @function
EVP_PKEY_new_raw_public_key:
.LFB865:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edi, %ebx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_PKEY_new
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L508
	testq	%r15, %r15
	leaq	-64(%rbp), %rdx
	movl	$0, %eax
	movq	%r15, -64(%rbp)
	cmove	%rdx, %rax
	cmpq	$0, 40(%r12)
	movq	%rax, %r15
	je	.L510
	movq	16(%r12), %rax
	testq	%rax, %rax
	je	.L511
	movq	168(%rax), %rax
	testq	%rax, %rax
	je	.L511
	movq	%r12, %rdi
	call	*%rax
	movq	$0, 40(%r12)
.L511:
	movq	24(%r12), %rdi
	call	ENGINE_finish@PLT
	movq	32(%r12), %rdi
	movq	$0, 24(%r12)
	call	ENGINE_finish@PLT
	movq	$0, 32(%r12)
.L510:
	cmpl	4(%r12), %ebx
	jne	.L512
	movq	16(%r12), %rax
	testq	%rax, %rax
	je	.L512
.L513:
	movq	256(%rax), %rax
	testq	%rax, %rax
	je	.L532
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L533
.L507:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L534
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L512:
	.cfi_restore_state
	movq	24(%r12), %rdi
	call	ENGINE_finish@PLT
	movq	32(%r12), %rdi
	movq	$0, 24(%r12)
	call	ENGINE_finish@PLT
	movl	%ebx, %esi
	movq	%r15, %rdi
	movq	$0, 32(%r12)
	call	EVP_PKEY_asn1_find@PLT
	testq	%rax, %rax
	je	.L535
	movq	-64(%rbp), %rdx
	movq	%rax, 16(%r12)
	movq	%rdx, 24(%r12)
	movl	(%rax), %edx
	movl	%ebx, 4(%r12)
	movl	%edx, (%r12)
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L535:
	movl	$210, %r8d
	movl	$156, %edx
	movl	$158, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L508:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	EVP_PKEY_free
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L533:
	movl	$272, %r8d
	movl	$180, %edx
	movl	$192, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L532:
	movl	$266, %r8d
	movl	$150, %edx
	movl	$192, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L508
.L534:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE865:
	.size	EVP_PKEY_new_raw_public_key, .-EVP_PKEY_new_raw_public_key
	.p2align 4
	.globl	EVP_PKEY_new_CMAC_key
	.type	EVP_PKEY_new_CMAC_key, @function
EVP_PKEY_new_CMAC_key:
.LFB868:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%rsi, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_PKEY_new
	movq	%rax, %r13
	call	CMAC_CTX_new@PLT
	movq	%rax, %r12
	testq	%r13, %r13
	je	.L537
	testq	%rax, %rax
	je	.L537
	testq	%rbx, %rbx
	leaq	-64(%rbp), %rdx
	movl	$0, %eax
	movq	%rbx, -64(%rbp)
	cmove	%rdx, %rax
	cmpq	$0, 40(%r13)
	movq	%rax, -80(%rbp)
	je	.L547
	movq	16(%r13), %rax
	testq	%rax, %rax
	je	.L540
	movq	168(%rax), %rax
	testq	%rax, %rax
	je	.L540
	movq	%r13, %rdi
	call	*%rax
	movq	$0, 40(%r13)
.L540:
	movq	24(%r13), %rdi
	call	ENGINE_finish@PLT
	movq	$0, 24(%r13)
	movq	32(%r13), %rdi
	call	ENGINE_finish@PLT
	movq	$0, 32(%r13)
.L547:
	cmpl	$894, 4(%r13)
	jne	.L541
	cmpq	$0, 16(%r13)
	je	.L541
.L542:
	movq	-72(%rbp), %rsi
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	CMAC_Init@PLT
	testl	%eax, %eax
	je	.L557
	movq	%r12, 40(%r13)
	jmp	.L536
	.p2align 4,,10
	.p2align 3
.L559:
	movl	$210, %r8d
	movl	$156, %edx
	movl	$158, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L537:
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	EVP_PKEY_free
	movq	%r12, %rdi
	call	CMAC_CTX_free@PLT
.L536:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L558
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L541:
	.cfi_restore_state
	movq	24(%r13), %rdi
	call	ENGINE_finish@PLT
	movq	$0, 24(%r13)
	movq	32(%r13), %rdi
	call	ENGINE_finish@PLT
	movq	-80(%rbp), %rdi
	movl	$894, %esi
	movq	$0, 32(%r13)
	call	EVP_PKEY_asn1_find@PLT
	testq	%rax, %rax
	je	.L559
	movq	-64(%rbp), %rdx
	movq	%rax, 16(%r13)
	movl	(%rax), %eax
	movl	$894, 4(%r13)
	movq	%rdx, 24(%r13)
	movl	%eax, 0(%r13)
	jmp	.L542
	.p2align 4,,10
	.p2align 3
.L557:
	movl	$332, %r8d
	movl	$180, %edx
	movl	$193, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L537
.L558:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE868:
	.size	EVP_PKEY_new_CMAC_key, .-EVP_PKEY_new_CMAC_key
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Public Key"
	.section	.rodata.str1.8
	.align 8
.LC2:
	.string	"%s algorithm \"%s\" unsupported\n"
	.text
	.p2align 4
	.globl	EVP_PKEY_print_public
	.type	EVP_PKEY_print_public, @function
EVP_PKEY_print_public:
.LFB897:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	movl	%edx, %esi
	movq	16(%r12), %rax
	testq	%rax, %rax
	je	.L561
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L561
	movq	%r12, %rsi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L561:
	.cfi_restore_state
	movl	$128, %edx
	movq	%r13, %rdi
	call	BIO_indent@PLT
	movl	(%r12), %edi
	call	OBJ_nid2ln@PLT
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	popq	%r12
	movl	$1, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE897:
	.size	EVP_PKEY_print_public, .-EVP_PKEY_print_public
	.section	.rodata.str1.1
.LC3:
	.string	"Private Key"
	.text
	.p2align 4
	.globl	EVP_PKEY_print_private
	.type	EVP_PKEY_print_private, @function
EVP_PKEY_print_private:
.LFB898:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	movl	%edx, %esi
	movq	16(%r12), %rax
	testq	%rax, %rax
	je	.L570
	movq	80(%rax), %rax
	testq	%rax, %rax
	je	.L570
	movq	%r12, %rsi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L570:
	.cfi_restore_state
	movl	$128, %edx
	movq	%r13, %rdi
	call	BIO_indent@PLT
	movl	(%r12), %edi
	call	OBJ_nid2ln@PLT
	movq	%r13, %rdi
	leaq	.LC3(%rip), %rdx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	popq	%r12
	movl	$1, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE898:
	.size	EVP_PKEY_print_private, .-EVP_PKEY_print_private
	.section	.rodata.str1.1
.LC4:
	.string	"Parameters"
	.text
	.p2align 4
	.globl	EVP_PKEY_print_params
	.type	EVP_PKEY_print_params, @function
EVP_PKEY_print_params:
.LFB899:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	movl	%edx, %esi
	movq	16(%r12), %rax
	testq	%rax, %rax
	je	.L579
	movq	152(%rax), %rax
	testq	%rax, %rax
	je	.L579
	movq	%r12, %rsi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L579:
	.cfi_restore_state
	movl	$128, %edx
	movq	%r13, %rdi
	call	BIO_indent@PLT
	movl	(%r12), %edi
	call	OBJ_nid2ln@PLT
	movq	%r13, %rdi
	leaq	.LC4(%rip), %rdx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	popq	%r12
	movl	$1, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE899:
	.size	EVP_PKEY_print_params, .-EVP_PKEY_print_params
	.p2align 4
	.globl	EVP_PKEY_get_default_digest_nid
	.type	EVP_PKEY_get_default_digest_nid, @function
EVP_PKEY_get_default_digest_nid:
.LFB901:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	%rsi, %rcx
	testq	%rax, %rax
	je	.L587
	movq	176(%rax), %rax
	testq	%rax, %rax
	je	.L587
	xorl	%edx, %edx
	movl	$3, %esi
	jmp	*%rax
.L587:
	movl	$-2, %eax
	ret
	.cfi_endproc
.LFE901:
	.size	EVP_PKEY_get_default_digest_nid, .-EVP_PKEY_get_default_digest_nid
	.p2align 4
	.globl	EVP_PKEY_set1_tls_encodedpoint
	.type	EVP_PKEY_set1_tls_encodedpoint, @function
EVP_PKEY_set1_tls_encodedpoint:
.LFB902:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	$2147483647, %rdx
	ja	.L599
	movq	%rsi, %rcx
	movq	16(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L599
	movq	176(%rsi), %r8
	testq	%r8, %r8
	je	.L599
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$9, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	*%r8
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L599:
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE902:
	.size	EVP_PKEY_set1_tls_encodedpoint, .-EVP_PKEY_set1_tls_encodedpoint
	.p2align 4
	.globl	EVP_PKEY_get1_tls_encodedpoint
	.type	EVP_PKEY_get1_tls_encodedpoint, @function
EVP_PKEY_get1_tls_encodedpoint:
.LFB903:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L610
	movq	176(%rax), %rax
	testq	%rax, %rax
	je	.L610
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movl	$10, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	*%rax
	testl	%eax, %eax
	jle	.L605
	cltq
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L605:
	.cfi_restore_state
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
.L610:
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE903:
	.size	EVP_PKEY_get1_tls_encodedpoint, .-EVP_PKEY_get1_tls_encodedpoint
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
