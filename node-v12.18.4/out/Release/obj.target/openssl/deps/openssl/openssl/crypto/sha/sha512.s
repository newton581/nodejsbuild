	.file	"sha512.c"
	.text
	.p2align 4
	.globl	sha512_224_init
	.type	sha512_224_init, @function
sha512_224_init:
.LFB251:
	.cfi_startproc
	endbr64
	movdqa	.LC0(%rip), %xmm0
	movabsq	$120259084288, %rax
	movq	%rax, 208(%rdi)
	movl	$1, %eax
	movups	%xmm0, (%rdi)
	movdqa	.LC1(%rip), %xmm0
	movups	%xmm0, 16(%rdi)
	movdqa	.LC2(%rip), %xmm0
	movups	%xmm0, 32(%rdi)
	movdqa	.LC3(%rip), %xmm0
	movups	%xmm0, 48(%rdi)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 64(%rdi)
	ret
	.cfi_endproc
.LFE251:
	.size	sha512_224_init, .-sha512_224_init
	.p2align 4
	.globl	sha512_256_init
	.type	sha512_256_init, @function
sha512_256_init:
.LFB252:
	.cfi_startproc
	endbr64
	movdqa	.LC4(%rip), %xmm0
	movabsq	$137438953472, %rax
	movq	%rax, 208(%rdi)
	movl	$1, %eax
	movups	%xmm0, (%rdi)
	movdqa	.LC5(%rip), %xmm0
	movups	%xmm0, 16(%rdi)
	movdqa	.LC6(%rip), %xmm0
	movups	%xmm0, 32(%rdi)
	movdqa	.LC7(%rip), %xmm0
	movups	%xmm0, 48(%rdi)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 64(%rdi)
	ret
	.cfi_endproc
.LFE252:
	.size	sha512_256_init, .-sha512_256_init
	.p2align 4
	.globl	SHA384_Init
	.type	SHA384_Init, @function
SHA384_Init:
.LFB253:
	.cfi_startproc
	endbr64
	movdqa	.LC8(%rip), %xmm0
	movabsq	$206158430208, %rax
	movq	%rax, 208(%rdi)
	movl	$1, %eax
	movups	%xmm0, (%rdi)
	movdqa	.LC9(%rip), %xmm0
	movups	%xmm0, 16(%rdi)
	movdqa	.LC10(%rip), %xmm0
	movups	%xmm0, 32(%rdi)
	movdqa	.LC11(%rip), %xmm0
	movups	%xmm0, 48(%rdi)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 64(%rdi)
	ret
	.cfi_endproc
.LFE253:
	.size	SHA384_Init, .-SHA384_Init
	.p2align 4
	.globl	SHA512_Init
	.type	SHA512_Init, @function
SHA512_Init:
.LFB254:
	.cfi_startproc
	endbr64
	movdqa	.LC12(%rip), %xmm0
	movabsq	$274877906944, %rax
	movq	%rax, 208(%rdi)
	movl	$1, %eax
	movups	%xmm0, (%rdi)
	movdqa	.LC13(%rip), %xmm0
	movups	%xmm0, 16(%rdi)
	movdqa	.LC14(%rip), %xmm0
	movups	%xmm0, 32(%rdi)
	movdqa	.LC15(%rip), %xmm0
	movups	%xmm0, 48(%rdi)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 64(%rdi)
	ret
	.cfi_endproc
.LFE254:
	.size	SHA512_Init, .-SHA512_Init
	.p2align 4
	.globl	SHA512_Final
	.type	SHA512_Final, @function
SHA512_Final:
.LFB255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	80(%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movl	208(%rsi), %eax
	movb	$-128, 80(%rsi,%rax)
	addq	$1, %rax
	leaq	0(%r13,%rax), %rsi
	cmpq	$112, %rax
	ja	.L7
	movl	$112, %edx
	subq	%rax, %rdx
	movq	%rdx, %rax
.L8:
	cmpl	$8, %eax
	jnb	.L15
	testb	$4, %al
	jne	.L41
	testl	%eax, %eax
	je	.L16
	movb	$0, (%rsi)
	testb	$2, %al
	je	.L16
	movl	%eax, %eax
	xorl	%edx, %edx
	movw	%dx, -2(%rsi,%rax)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L15:
	leaq	8(%rsi), %rdi
	movl	%eax, %edx
	movq	$0, (%rsi)
	movq	$0, -8(%rsi,%rdx)
	andq	$-8, %rdi
	subq	%rdi, %rsi
	leal	(%rax,%rsi), %ecx
	xorl	%eax, %eax
	shrl	$3, %ecx
	rep stosq
.L16:
	movq	64(%rbx), %rax
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	bswap	%rax
	movq	%rax, 200(%rbx)
	movq	72(%rbx), %rax
	bswap	%rax
	movq	%rax, 192(%rbx)
	call	sha512_block_data_order@PLT
	testq	%r12, %r12
	je	.L26
	movl	212(%rbx), %eax
	cmpl	$48, %eax
	je	.L20
	ja	.L21
	cmpl	$28, %eax
	je	.L22
	cmpl	$32, %eax
	jne	.L26
	movq	(%rbx), %rax
	bswap	%rax
	movq	%rax, (%r12)
	movq	8(%rbx), %rax
	bswap	%rax
	movq	%rax, 8(%r12)
	movq	16(%rbx), %rax
	bswap	%rax
	movq	%rax, 16(%r12)
	movq	24(%rbx), %rax
	bswap	%rax
	movq	%rax, 24(%r12)
.L25:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	cmpl	$64, %eax
	jne	.L26
	movq	(%rbx), %rax
	bswap	%rax
	movq	%rax, (%r12)
	movq	8(%rbx), %rax
	bswap	%rax
	movq	%rax, 8(%r12)
	movq	16(%rbx), %rax
	bswap	%rax
	movq	%rax, 16(%r12)
	movq	24(%rbx), %rax
	bswap	%rax
	movq	%rax, 24(%r12)
	movq	32(%rbx), %rax
	bswap	%rax
	movq	%rax, 32(%r12)
	movq	40(%rbx), %rax
	bswap	%rax
	movq	%rax, 40(%r12)
	movq	48(%rbx), %rax
	bswap	%rax
	movq	%rax, 48(%r12)
	movq	56(%rbx), %rax
	bswap	%rax
	movq	%rax, 56(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L26:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	movl	$128, %edx
	xorl	%r8d, %r8d
	subq	%rax, %rdx
	movq	%rdx, %rax
	cmpq	$8, %rdx
	jnb	.L9
	testb	$4, %al
	jne	.L42
	testq	%rdx, %rdx
	je	.L10
	movb	$0, (%rsi)
	testb	$2, %al
	jne	.L43
.L10:
	movq	%r13, %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	call	sha512_block_data_order@PLT
	movq	%r13, %rsi
	movl	$112, %eax
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L22:
	movq	(%rbx), %rax
	bswap	%rax
	movq	%rax, (%r12)
	movq	8(%rbx), %rax
	bswap	%rax
	movq	%rax, 8(%r12)
	movq	16(%rbx), %rax
	bswap	%rax
	movq	%rax, 16(%r12)
	movq	24(%rbx), %rax
	movq	%rax, %rdx
	shrq	$56, %rdx
	movb	%dl, 24(%r12)
	movq	%rax, %rdx
	shrq	$48, %rdx
	movb	%dl, 25(%r12)
	movq	%rax, %rdx
	shrq	$32, %rax
	shrq	$40, %rdx
	movb	%al, 27(%r12)
	movl	$1, %eax
	movb	%dl, 26(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	leaq	8(%rsi), %rdi
	movq	$0, (%rsi)
	movq	$0, -8(%rsi,%rdx)
	andq	$-8, %rdi
	subq	%rdi, %rsi
	addq	%rsi, %rax
	andq	$-8, %rax
	cmpq	$8, %rax
	jb	.L10
	andq	$-8, %rax
	xorl	%edx, %edx
.L13:
	movq	%r8, (%rdi,%rdx)
	addq	$8, %rdx
	cmpq	%rax, %rdx
	jb	.L13
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L41:
	movl	%eax, %eax
	movl	$0, (%rsi)
	movl	$0, -4(%rsi,%rax)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L20:
	movq	(%rbx), %rax
	bswap	%rax
	movq	%rax, (%r12)
	movq	8(%rbx), %rax
	bswap	%rax
	movq	%rax, 8(%r12)
	movq	16(%rbx), %rax
	bswap	%rax
	movq	%rax, 16(%r12)
	movq	24(%rbx), %rax
	bswap	%rax
	movq	%rax, 24(%r12)
	movq	32(%rbx), %rax
	bswap	%rax
	movq	%rax, 32(%r12)
	movq	40(%rbx), %rax
	bswap	%rax
	movq	%rax, 40(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L42:
	movl	$0, (%rsi)
	movl	$0, -4(%rsi,%rdx)
	jmp	.L10
.L43:
	xorl	%ecx, %ecx
	movw	%cx, -2(%rsi,%rdx)
	jmp	.L10
	.cfi_endproc
.LFE255:
	.size	SHA512_Final, .-SHA512_Final
	.p2align 4
	.globl	SHA384_Final
	.type	SHA384_Final, @function
SHA384_Final:
.LFB256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	80(%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movl	208(%rsi), %eax
	movb	$-128, 80(%rsi,%rax)
	addq	$1, %rax
	leaq	0(%r13,%rax), %rsi
	cmpq	$112, %rax
	ja	.L45
	movl	$112, %edx
	subq	%rax, %rdx
	movq	%rdx, %rax
.L46:
	cmpl	$8, %eax
	jnb	.L53
	testb	$4, %al
	jne	.L79
	testl	%eax, %eax
	je	.L54
	movb	$0, (%rsi)
	testb	$2, %al
	je	.L54
	movl	%eax, %eax
	xorl	%edx, %edx
	movw	%dx, -2(%rsi,%rax)
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L53:
	leaq	8(%rsi), %rdi
	movl	%eax, %edx
	movq	$0, (%rsi)
	movq	$0, -8(%rsi,%rdx)
	andq	$-8, %rdi
	subq	%rdi, %rsi
	leal	(%rax,%rsi), %ecx
	xorl	%eax, %eax
	shrl	$3, %ecx
	rep stosq
.L54:
	movq	64(%rbx), %rax
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	bswap	%rax
	movq	%rax, 200(%rbx)
	movq	72(%rbx), %rax
	bswap	%rax
	movq	%rax, 192(%rbx)
	call	sha512_block_data_order@PLT
	testq	%r12, %r12
	je	.L64
	movl	212(%rbx), %eax
	cmpl	$48, %eax
	je	.L58
	ja	.L59
	cmpl	$28, %eax
	je	.L60
	cmpl	$32, %eax
	jne	.L64
	movq	(%rbx), %rax
	bswap	%rax
	movq	%rax, (%r12)
	movq	8(%rbx), %rax
	bswap	%rax
	movq	%rax, 8(%r12)
	movq	16(%rbx), %rax
	bswap	%rax
	movq	%rax, 16(%r12)
	movq	24(%rbx), %rax
	bswap	%rax
	movq	%rax, 24(%r12)
.L63:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	cmpl	$64, %eax
	jne	.L64
	movq	(%rbx), %rax
	bswap	%rax
	movq	%rax, (%r12)
	movq	8(%rbx), %rax
	bswap	%rax
	movq	%rax, 8(%r12)
	movq	16(%rbx), %rax
	bswap	%rax
	movq	%rax, 16(%r12)
	movq	24(%rbx), %rax
	bswap	%rax
	movq	%rax, 24(%r12)
	movq	32(%rbx), %rax
	bswap	%rax
	movq	%rax, 32(%r12)
	movq	40(%rbx), %rax
	bswap	%rax
	movq	%rax, 40(%r12)
	movq	48(%rbx), %rax
	bswap	%rax
	movq	%rax, 48(%r12)
	movq	56(%rbx), %rax
	bswap	%rax
	movq	%rax, 56(%r12)
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L64:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	movl	$128, %edx
	xorl	%r8d, %r8d
	subq	%rax, %rdx
	movq	%rdx, %rax
	cmpq	$8, %rdx
	jnb	.L47
	testb	$4, %al
	jne	.L80
	testq	%rdx, %rdx
	je	.L48
	movb	$0, (%rsi)
	testb	$2, %al
	jne	.L81
.L48:
	movq	%r13, %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	call	sha512_block_data_order@PLT
	movq	%r13, %rsi
	movl	$112, %eax
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L60:
	movq	(%rbx), %rax
	bswap	%rax
	movq	%rax, (%r12)
	movq	8(%rbx), %rax
	bswap	%rax
	movq	%rax, 8(%r12)
	movq	16(%rbx), %rax
	bswap	%rax
	movq	%rax, 16(%r12)
	movq	24(%rbx), %rax
	movq	%rax, %rdx
	shrq	$56, %rdx
	movb	%dl, 24(%r12)
	movq	%rax, %rdx
	shrq	$48, %rdx
	movb	%dl, 25(%r12)
	movq	%rax, %rdx
	shrq	$32, %rax
	shrq	$40, %rdx
	movb	%al, 27(%r12)
	movl	$1, %eax
	movb	%dl, 26(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	leaq	8(%rsi), %rdi
	movq	$0, (%rsi)
	movq	$0, -8(%rsi,%rdx)
	andq	$-8, %rdi
	subq	%rdi, %rsi
	addq	%rsi, %rax
	andq	$-8, %rax
	cmpq	$8, %rax
	jb	.L48
	andq	$-8, %rax
	xorl	%edx, %edx
.L51:
	movq	%r8, (%rdi,%rdx)
	addq	$8, %rdx
	cmpq	%rax, %rdx
	jb	.L51
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L79:
	movl	%eax, %eax
	movl	$0, (%rsi)
	movl	$0, -4(%rsi,%rax)
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L58:
	movq	(%rbx), %rax
	bswap	%rax
	movq	%rax, (%r12)
	movq	8(%rbx), %rax
	bswap	%rax
	movq	%rax, 8(%r12)
	movq	16(%rbx), %rax
	bswap	%rax
	movq	%rax, 16(%r12)
	movq	24(%rbx), %rax
	bswap	%rax
	movq	%rax, 24(%r12)
	movq	32(%rbx), %rax
	bswap	%rax
	movq	%rax, 32(%r12)
	movq	40(%rbx), %rax
	bswap	%rax
	movq	%rax, 40(%r12)
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L80:
	movl	$0, (%rsi)
	movl	$0, -4(%rsi,%rdx)
	jmp	.L48
.L81:
	xorl	%ecx, %ecx
	movw	%cx, -2(%rsi,%rdx)
	jmp	.L48
	.cfi_endproc
.LFE256:
	.size	SHA384_Final, .-SHA384_Final
	.p2align 4
	.globl	SHA512_Update
	.type	SHA512_Update, @function
SHA512_Update:
.LFB257:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L121
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	0(,%rdx,8), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	80(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	xorl	%edx, %edx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	addq	64(%rdi), %rax
	movq	72(%rdi), %rcx
	setc	%dl
	movq	%rax, 64(%rdi)
	movl	208(%rdi), %eax
	cmpq	$1, %rdx
	movq	%r12, %rdx
	sbbq	$-1, %rcx
	shrq	$61, %rdx
	addq	%rcx, %rdx
	movq	%rdx, 72(%rdi)
	testl	%eax, %eax
	je	.L88
	movl	$128, %edx
	leaq	(%r14,%rax), %r8
	subq	%rax, %rdx
	cmpq	%rdx, %r12
	jb	.L124
	cmpq	$8, %rdx
	jb	.L125
	movq	(%rsi), %rcx
	leaq	8(%r8), %rdi
	andq	$-8, %rdi
	movq	%rcx, (%r8)
	movq	-8(%rsi,%rdx), %rcx
	movq	%rcx, -8(%r8,%rdx)
	subq	%rdi, %r8
	leaq	(%rdx,%r8), %rcx
	subq	%r8, %rsi
	shrq	$3, %rcx
	rep movsq
.L91:
	addq	%rdx, %r13
	leaq	-128(%rax,%r12), %r15
	movl	$1, %edx
	movq	%r14, %rsi
	movl	$0, 208(%rbx)
	movq	%rbx, %rdi
	call	sha512_block_data_order@PLT
	cmpq	$127, %r15
	jbe	.L94
	movq	%r15, %r12
	.p2align 4,,10
	.p2align 3
.L100:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r12, %r15
	movq	%rbx, %rdi
	shrq	$7, %rdx
	andq	$-128, %r12
	andl	$127, %r15d
	call	sha512_block_data_order@PLT
	addq	%r12, %r13
.L94:
	testq	%r15, %r15
	jne	.L126
.L108:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	cmpq	$127, %r12
	ja	.L100
.L101:
	movl	%r12d, %ecx
	cmpl	$8, %r12d
	jnb	.L96
	testb	$4, %r12b
	jne	.L127
	testl	%ecx, %ecx
	je	.L97
	movzbl	0(%r13), %eax
	movb	%al, 80(%rbx)
	testb	$2, %cl
	je	.L97
	movzwl	-2(%r13,%rcx), %eax
	movw	%ax, -2(%r14,%rcx)
	.p2align 4,,10
	.p2align 3
.L97:
	movl	%r12d, 208(%rbx)
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	testb	$4, %dl
	jne	.L128
	testq	%rdx, %rdx
	je	.L91
	movzbl	(%rsi), %ecx
	movb	%cl, (%r8)
	testb	$2, %dl
	je	.L91
	movzwl	-2(%rsi,%rdx), %ecx
	movw	%cx, -2(%r8,%rdx)
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L124:
	movq	%r12, %rdx
	movq	%r8, %rdi
	call	memcpy@PLT
	addl	%r12d, 208(%rbx)
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L96:
	movq	0(%r13), %rax
	leaq	8(%r14), %rdi
	movq	%r13, %rsi
	andq	$-8, %rdi
	movq	%rax, 80(%rbx)
	movl	%r12d, %eax
	movq	-8(%r13,%rax), %rdx
	movq	%rdx, -8(%r14,%rax)
	subq	%rdi, %r14
	leal	(%r12,%r14), %ecx
	subq	%r14, %rsi
	shrl	$3, %ecx
	rep movsq
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L128:
	movl	(%rsi), %ecx
	movl	%ecx, (%r8)
	movl	-4(%rsi,%rdx), %ecx
	movl	%ecx, -4(%r8,%rdx)
	jmp	.L91
.L127:
	movl	0(%r13), %eax
	movl	%eax, 80(%rbx)
	movl	-4(%r13,%rcx), %eax
	movl	%eax, -4(%r14,%rcx)
	jmp	.L97
.L126:
	movq	%r15, %r12
	jmp	.L101
	.cfi_endproc
.LFE257:
	.size	SHA512_Update, .-SHA512_Update
	.p2align 4
	.globl	SHA384_Update
	.type	SHA384_Update, @function
SHA384_Update:
.LFB258:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L168
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	0(,%rdx,8), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	80(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	xorl	%edx, %edx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	addq	64(%rdi), %rax
	movq	72(%rdi), %rcx
	setc	%dl
	movq	%rax, 64(%rdi)
	movl	208(%rdi), %eax
	cmpq	$1, %rdx
	movq	%r12, %rdx
	sbbq	$-1, %rcx
	shrq	$61, %rdx
	addq	%rcx, %rdx
	movq	%rdx, 72(%rdi)
	testl	%eax, %eax
	je	.L135
	movl	$128, %edx
	leaq	(%r14,%rax), %r8
	subq	%rax, %rdx
	cmpq	%rdx, %r12
	jb	.L171
	cmpq	$8, %rdx
	jb	.L172
	movq	(%rsi), %rcx
	leaq	8(%r8), %rdi
	andq	$-8, %rdi
	movq	%rcx, (%r8)
	movq	-8(%rsi,%rdx), %rcx
	movq	%rcx, -8(%r8,%rdx)
	subq	%rdi, %r8
	leaq	(%rdx,%r8), %rcx
	subq	%r8, %rsi
	shrq	$3, %rcx
	rep movsq
.L138:
	addq	%rdx, %r13
	leaq	-128(%rax,%r12), %r15
	movl	$1, %edx
	movq	%r14, %rsi
	movl	$0, 208(%rbx)
	movq	%rbx, %rdi
	call	sha512_block_data_order@PLT
	cmpq	$127, %r15
	jbe	.L141
	movq	%r15, %r12
	.p2align 4,,10
	.p2align 3
.L147:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r12, %r15
	movq	%rbx, %rdi
	shrq	$7, %rdx
	andq	$-128, %r12
	andl	$127, %r15d
	call	sha512_block_data_order@PLT
	addq	%r12, %r13
.L141:
	testq	%r15, %r15
	jne	.L173
.L155:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	.cfi_restore_state
	cmpq	$127, %r12
	ja	.L147
.L148:
	movl	%r12d, %ecx
	cmpl	$8, %r12d
	jnb	.L143
	testb	$4, %r12b
	jne	.L174
	testl	%ecx, %ecx
	je	.L144
	movzbl	0(%r13), %eax
	movb	%al, 80(%rbx)
	testb	$2, %cl
	je	.L144
	movzwl	-2(%r13,%rcx), %eax
	movw	%ax, -2(%r14,%rcx)
	.p2align 4,,10
	.p2align 3
.L144:
	movl	%r12d, 208(%rbx)
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L168:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	testb	$4, %dl
	jne	.L175
	testq	%rdx, %rdx
	je	.L138
	movzbl	(%rsi), %ecx
	movb	%cl, (%r8)
	testb	$2, %dl
	je	.L138
	movzwl	-2(%rsi,%rdx), %ecx
	movw	%cx, -2(%r8,%rdx)
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L171:
	movq	%r12, %rdx
	movq	%r8, %rdi
	call	memcpy@PLT
	addl	%r12d, 208(%rbx)
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L143:
	movq	0(%r13), %rax
	leaq	8(%r14), %rdi
	movq	%r13, %rsi
	andq	$-8, %rdi
	movq	%rax, 80(%rbx)
	movl	%r12d, %eax
	movq	-8(%r13,%rax), %rdx
	movq	%rdx, -8(%r14,%rax)
	subq	%rdi, %r14
	leal	(%r12,%r14), %ecx
	subq	%r14, %rsi
	shrl	$3, %ecx
	rep movsq
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L175:
	movl	(%rsi), %ecx
	movl	%ecx, (%r8)
	movl	-4(%rsi,%rdx), %ecx
	movl	%ecx, -4(%r8,%rdx)
	jmp	.L138
.L174:
	movl	0(%r13), %eax
	movl	%eax, 80(%rbx)
	movl	-4(%r13,%rcx), %eax
	movl	%eax, -4(%r14,%rcx)
	jmp	.L144
.L173:
	movq	%r15, %r12
	jmp	.L148
	.cfi_endproc
.LFE258:
	.size	SHA384_Update, .-SHA384_Update
	.p2align 4
	.globl	SHA512_Transform
	.type	SHA512_Transform, @function
SHA512_Transform:
.LFB259:
	.cfi_startproc
	endbr64
	movl	$1, %edx
	jmp	sha512_block_data_order@PLT
	.cfi_endproc
.LFE259:
	.size	SHA512_Transform, .-SHA512_Transform
	.p2align 4
	.globl	SHA384
	.type	SHA384, @function
SHA384:
.LFB260:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$224, %rsp
	.cfi_offset 3, -48
	movdqa	.LC8(%rip), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	m.6326(%rip), %rax
	testq	%rdx, %rdx
	movaps	%xmm0, -256(%rbp)
	movdqa	.LC9(%rip), %xmm0
	cmove	%rax, %r12
	movabsq	$206158430208, %rax
	movq	%rax, -48(%rbp)
	movaps	%xmm0, -240(%rbp)
	movdqa	.LC10(%rip), %xmm0
	movaps	%xmm0, -224(%rbp)
	movdqa	.LC11(%rip), %xmm0
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -192(%rbp)
	testq	%rsi, %rsi
	je	.L179
	movq	%rsi, %rax
	movq	%rdi, %r14
	leaq	-256(%rbp), %r13
	movq	%rsi, %rbx
	shrq	$61, %rax
	movq	%rax, -184(%rbp)
	leaq	0(,%rsi,8), %rax
	movq	%rax, -192(%rbp)
	cmpq	$127, %rsi
	ja	.L219
.L180:
	leaq	-176(%rbp), %r8
	movl	$136, %ecx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r8, %rdi
	call	__memcpy_chk@PLT
	movl	%ebx, -48(%rbp)
	movq	%rax, %r8
.L182:
	movb	$-128, -176(%rbp,%rbx)
	addq	$1, %rbx
	leaq	(%r8,%rbx), %rdx
	cmpq	$112, %rbx
	ja	.L183
	movl	$112, %eax
	subq	%rbx, %rax
.L184:
	cmpl	$8, %eax
	jnb	.L191
	testb	$4, %al
	jne	.L220
	testl	%eax, %eax
	je	.L192
	movb	$0, (%rdx)
	testb	$2, %al
	jne	.L221
.L192:
	movq	-192(%rbp), %rax
	movl	$1, %edx
	movq	%r8, %rsi
	movq	%r13, %rdi
	bswap	%rax
	movq	%rax, -56(%rbp)
	movq	-184(%rbp), %rax
	bswap	%rax
	movq	%rax, -64(%rbp)
	call	sha512_block_data_order@PLT
	movl	-44(%rbp), %eax
	cmpl	$48, %eax
	je	.L195
	ja	.L196
	cmpl	$28, %eax
	je	.L197
	cmpl	$32, %eax
	jne	.L199
	movq	-256(%rbp), %rax
	bswap	%rax
	movq	%rax, (%r12)
	movq	-248(%rbp), %rax
	bswap	%rax
	movq	%rax, 8(%r12)
	movq	-240(%rbp), %rax
	bswap	%rax
	movq	%rax, 16(%r12)
	movq	-232(%rbp), %rax
	bswap	%rax
	movq	%rax, 24(%r12)
	.p2align 4,,10
	.p2align 3
.L199:
	movl	$216, %esi
	movq	%r13, %rdi
	call	OPENSSL_cleanse@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L222
	addq	$224, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L219:
	.cfi_restore_state
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	%r13, %rdi
	shrq	$7, %rdx
	call	sha512_block_data_order@PLT
	movq	%rbx, %rax
	andl	$127, %eax
	jne	.L223
	movl	-48(%rbp), %ebx
	leaq	-176(%rbp), %r8
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L196:
	cmpl	$64, %eax
	jne	.L199
	movq	-256(%rbp), %rax
	bswap	%rax
	movq	%rax, (%r12)
	movq	-248(%rbp), %rax
	bswap	%rax
	movq	%rax, 8(%r12)
	movq	-240(%rbp), %rax
	bswap	%rax
	movq	%rax, 16(%r12)
	movq	-232(%rbp), %rax
	bswap	%rax
	movq	%rax, 24(%r12)
	movq	-224(%rbp), %rax
	bswap	%rax
	movq	%rax, 32(%r12)
	movq	-216(%rbp), %rax
	bswap	%rax
	movq	%rax, 40(%r12)
	movq	-208(%rbp), %rax
	bswap	%rax
	movq	%rax, 48(%r12)
	movq	-200(%rbp), %rax
	bswap	%rax
	movq	%rax, 56(%r12)
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L191:
	movl	%eax, %ecx
	leaq	8(%rdx), %rdi
	movq	$0, (%rdx)
	movq	$0, -8(%rdx,%rcx)
	andq	$-8, %rdi
	movq	%rdx, %rcx
	subq	%rdi, %rcx
	addl	%eax, %ecx
	xorl	%eax, %eax
	shrl	$3, %ecx
	rep stosq
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L179:
	movb	$-128, -176(%rbp)
	leaq	-175(%rbp), %rdx
	movl	$111, %eax
	leaq	-256(%rbp), %r13
	leaq	-176(%rbp), %r8
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L183:
	movl	$128, %eax
	xorl	%edi, %edi
	subq	%rbx, %rax
	cmpq	$8, %rax
	jnb	.L185
	testb	$4, %al
	jne	.L224
	testq	%rax, %rax
	je	.L186
	movb	$0, (%rdx)
	testb	$2, %al
	jne	.L225
.L186:
	movl	$1, %edx
	movq	%r8, %rsi
	movq	%r13, %rdi
	call	sha512_block_data_order@PLT
	leaq	-176(%rbp), %r8
	movl	$112, %eax
	movq	%r8, %rdx
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L197:
	movq	-256(%rbp), %rax
	bswap	%rax
	movq	%rax, (%r12)
	movq	-248(%rbp), %rax
	bswap	%rax
	movq	%rax, 8(%r12)
	movq	-240(%rbp), %rax
	bswap	%rax
	movq	%rax, 16(%r12)
	movq	-232(%rbp), %rax
	movq	%rax, %rdx
	shrq	$56, %rdx
	movb	%dl, 24(%r12)
	movq	%rax, %rdx
	shrq	$48, %rdx
	movb	%dl, 25(%r12)
	movq	%rax, %rdx
	shrq	$32, %rax
	shrq	$40, %rdx
	movb	%al, 27(%r12)
	movb	%dl, 26(%r12)
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L195:
	movq	-256(%rbp), %rax
	bswap	%rax
	movq	%rax, (%r12)
	movq	-248(%rbp), %rax
	bswap	%rax
	movq	%rax, 8(%r12)
	movq	-240(%rbp), %rax
	bswap	%rax
	movq	%rax, 16(%r12)
	movq	-232(%rbp), %rax
	bswap	%rax
	movq	%rax, 24(%r12)
	movq	-224(%rbp), %rax
	bswap	%rax
	movq	%rax, 32(%r12)
	movq	-216(%rbp), %rax
	bswap	%rax
	movq	%rax, 40(%r12)
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L185:
	leaq	8(%rdx), %rsi
	movq	%rdx, %rcx
	movq	$0, (%rdx)
	movq	$0, -8(%rdx,%rax)
	andq	$-8, %rsi
	subq	%rsi, %rcx
	addq	%rcx, %rax
	andq	$-8, %rax
	cmpq	$8, %rax
	jb	.L186
	andq	$-8, %rax
	xorl	%edx, %edx
.L189:
	movq	%rdi, (%rsi,%rdx)
	addq	$8, %rdx
	cmpq	%rax, %rdx
	jb	.L189
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L220:
	movl	%eax, %eax
	movl	$0, (%rdx)
	movl	$0, -4(%rdx,%rax)
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L221:
	movl	%eax, %eax
	xorl	%ecx, %ecx
	movw	%cx, -2(%rdx,%rax)
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L224:
	movl	$0, (%rdx)
	movl	$0, -4(%rdx,%rax)
	jmp	.L186
.L225:
	xorl	%esi, %esi
	movw	%si, -2(%rdx,%rax)
	jmp	.L186
.L222:
	call	__stack_chk_fail@PLT
.L223:
	subq	%rax, %rbx
	addq	%rbx, %r14
	movq	%rax, %rbx
	jmp	.L180
	.cfi_endproc
.LFE260:
	.size	SHA384, .-SHA384
	.p2align 4
	.globl	SHA512
	.type	SHA512, @function
SHA512:
.LFB261:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$224, %rsp
	.cfi_offset 3, -48
	movdqa	.LC12(%rip), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	m.6333(%rip), %rax
	testq	%rdx, %rdx
	movaps	%xmm0, -256(%rbp)
	movdqa	.LC13(%rip), %xmm0
	cmove	%rax, %r12
	movabsq	$274877906944, %rax
	movq	%rax, -48(%rbp)
	movaps	%xmm0, -240(%rbp)
	movdqa	.LC14(%rip), %xmm0
	movaps	%xmm0, -224(%rbp)
	movdqa	.LC15(%rip), %xmm0
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -192(%rbp)
	testq	%rsi, %rsi
	je	.L228
	movq	%rsi, %rax
	movq	%rdi, %r14
	leaq	-256(%rbp), %r13
	movq	%rsi, %rbx
	shrq	$61, %rax
	movq	%rax, -184(%rbp)
	leaq	0(,%rsi,8), %rax
	movq	%rax, -192(%rbp)
	cmpq	$127, %rsi
	ja	.L268
.L229:
	leaq	-176(%rbp), %r8
	movl	$136, %ecx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r8, %rdi
	call	__memcpy_chk@PLT
	movl	%ebx, -48(%rbp)
	movq	%rax, %r8
.L231:
	movb	$-128, -176(%rbp,%rbx)
	addq	$1, %rbx
	leaq	(%r8,%rbx), %rdx
	cmpq	$112, %rbx
	ja	.L232
	movl	$112, %eax
	subq	%rbx, %rax
.L233:
	cmpl	$8, %eax
	jnb	.L240
	testb	$4, %al
	jne	.L269
	testl	%eax, %eax
	je	.L241
	movb	$0, (%rdx)
	testb	$2, %al
	jne	.L270
.L241:
	movq	-192(%rbp), %rax
	movl	$1, %edx
	movq	%r8, %rsi
	movq	%r13, %rdi
	bswap	%rax
	movq	%rax, -56(%rbp)
	movq	-184(%rbp), %rax
	bswap	%rax
	movq	%rax, -64(%rbp)
	call	sha512_block_data_order@PLT
	movl	-44(%rbp), %eax
	cmpl	$48, %eax
	je	.L244
	ja	.L245
	cmpl	$28, %eax
	je	.L246
	cmpl	$32, %eax
	jne	.L248
	movq	-256(%rbp), %rax
	bswap	%rax
	movq	%rax, (%r12)
	movq	-248(%rbp), %rax
	bswap	%rax
	movq	%rax, 8(%r12)
	movq	-240(%rbp), %rax
	bswap	%rax
	movq	%rax, 16(%r12)
	movq	-232(%rbp), %rax
	bswap	%rax
	movq	%rax, 24(%r12)
	.p2align 4,,10
	.p2align 3
.L248:
	movl	$216, %esi
	movq	%r13, %rdi
	call	OPENSSL_cleanse@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L271
	addq	$224, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L268:
	.cfi_restore_state
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	%r13, %rdi
	shrq	$7, %rdx
	call	sha512_block_data_order@PLT
	movq	%rbx, %rax
	andl	$127, %eax
	jne	.L272
	movl	-48(%rbp), %ebx
	leaq	-176(%rbp), %r8
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L245:
	cmpl	$64, %eax
	jne	.L248
	movq	-256(%rbp), %rax
	bswap	%rax
	movq	%rax, (%r12)
	movq	-248(%rbp), %rax
	bswap	%rax
	movq	%rax, 8(%r12)
	movq	-240(%rbp), %rax
	bswap	%rax
	movq	%rax, 16(%r12)
	movq	-232(%rbp), %rax
	bswap	%rax
	movq	%rax, 24(%r12)
	movq	-224(%rbp), %rax
	bswap	%rax
	movq	%rax, 32(%r12)
	movq	-216(%rbp), %rax
	bswap	%rax
	movq	%rax, 40(%r12)
	movq	-208(%rbp), %rax
	bswap	%rax
	movq	%rax, 48(%r12)
	movq	-200(%rbp), %rax
	bswap	%rax
	movq	%rax, 56(%r12)
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L240:
	movl	%eax, %ecx
	leaq	8(%rdx), %rdi
	movq	$0, (%rdx)
	movq	$0, -8(%rdx,%rcx)
	andq	$-8, %rdi
	movq	%rdx, %rcx
	subq	%rdi, %rcx
	addl	%eax, %ecx
	xorl	%eax, %eax
	shrl	$3, %ecx
	rep stosq
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L228:
	movb	$-128, -176(%rbp)
	leaq	-175(%rbp), %rdx
	movl	$111, %eax
	leaq	-256(%rbp), %r13
	leaq	-176(%rbp), %r8
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L232:
	movl	$128, %eax
	xorl	%edi, %edi
	subq	%rbx, %rax
	cmpq	$8, %rax
	jnb	.L234
	testb	$4, %al
	jne	.L273
	testq	%rax, %rax
	je	.L235
	movb	$0, (%rdx)
	testb	$2, %al
	jne	.L274
.L235:
	movl	$1, %edx
	movq	%r8, %rsi
	movq	%r13, %rdi
	call	sha512_block_data_order@PLT
	leaq	-176(%rbp), %r8
	movl	$112, %eax
	movq	%r8, %rdx
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L246:
	movq	-256(%rbp), %rax
	bswap	%rax
	movq	%rax, (%r12)
	movq	-248(%rbp), %rax
	bswap	%rax
	movq	%rax, 8(%r12)
	movq	-240(%rbp), %rax
	bswap	%rax
	movq	%rax, 16(%r12)
	movq	-232(%rbp), %rax
	movq	%rax, %rdx
	shrq	$56, %rdx
	movb	%dl, 24(%r12)
	movq	%rax, %rdx
	shrq	$48, %rdx
	movb	%dl, 25(%r12)
	movq	%rax, %rdx
	shrq	$32, %rax
	shrq	$40, %rdx
	movb	%al, 27(%r12)
	movb	%dl, 26(%r12)
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L244:
	movq	-256(%rbp), %rax
	bswap	%rax
	movq	%rax, (%r12)
	movq	-248(%rbp), %rax
	bswap	%rax
	movq	%rax, 8(%r12)
	movq	-240(%rbp), %rax
	bswap	%rax
	movq	%rax, 16(%r12)
	movq	-232(%rbp), %rax
	bswap	%rax
	movq	%rax, 24(%r12)
	movq	-224(%rbp), %rax
	bswap	%rax
	movq	%rax, 32(%r12)
	movq	-216(%rbp), %rax
	bswap	%rax
	movq	%rax, 40(%r12)
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L234:
	leaq	8(%rdx), %rsi
	movq	%rdx, %rcx
	movq	$0, (%rdx)
	movq	$0, -8(%rdx,%rax)
	andq	$-8, %rsi
	subq	%rsi, %rcx
	addq	%rcx, %rax
	andq	$-8, %rax
	cmpq	$8, %rax
	jb	.L235
	andq	$-8, %rax
	xorl	%edx, %edx
.L238:
	movq	%rdi, (%rsi,%rdx)
	addq	$8, %rdx
	cmpq	%rax, %rdx
	jb	.L238
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L269:
	movl	%eax, %eax
	movl	$0, (%rdx)
	movl	$0, -4(%rdx,%rax)
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L270:
	movl	%eax, %eax
	xorl	%ecx, %ecx
	movw	%cx, -2(%rdx,%rax)
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L273:
	movl	$0, (%rdx)
	movl	$0, -4(%rdx,%rax)
	jmp	.L235
.L274:
	xorl	%esi, %esi
	movw	%si, -2(%rdx,%rax)
	jmp	.L235
.L271:
	call	__stack_chk_fail@PLT
.L272:
	subq	%rax, %rbx
	addq	%rbx, %r14
	movq	%rax, %rbx
	jmp	.L229
	.cfi_endproc
.LFE261:
	.size	SHA512, .-SHA512
	.local	m.6333
	.comm	m.6333,64,32
	.local	m.6326
	.comm	m.6326,48,32
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.quad	-8341449602262348382
	.quad	8350123849800275158
	.align 16
.LC1:
	.quad	2160240930085379202
	.quad	7466358040605728719
	.align 16
.LC2:
	.quad	1111592415079452072
	.quad	8638871050018654530
	.align 16
.LC3:
	.quad	4583966954114332360
	.quad	1230299281376055969
	.align 16
.LC4:
	.quad	2463787394917988140
	.quad	-6965556091613846334
	.align 16
.LC5:
	.quad	2563595384472711505
	.quad	-7622211418569250115
	.align 16
.LC6:
	.quad	-7626776825740460061
	.quad	-4729309413028513390
	.align 16
.LC7:
	.quad	3098927326965381290
	.quad	1060366662362279074
	.align 16
.LC8:
	.quad	-3766243637369397544
	.quad	7105036623409894663
	.align 16
.LC9:
	.quad	-7973340178411365097
	.quad	1526699215303891257
	.align 16
.LC10:
	.quad	7436329637833083697
	.quad	-8163818279084223215
	.align 16
.LC11:
	.quad	-2662702644619276377
	.quad	5167115440072839076
	.align 16
.LC12:
	.quad	7640891576956012808
	.quad	-4942790177534073029
	.align 16
.LC13:
	.quad	4354685564936845355
	.quad	-6534734903238641935
	.align 16
.LC14:
	.quad	5840696475078001361
	.quad	-7276294671716946913
	.align 16
.LC15:
	.quad	2270897969802886507
	.quad	6620516959819538809
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
