	.file	"b_sock.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/bio/b_sock.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"host="
	.text
	.p2align 4
	.globl	BIO_get_host_ip
	.type	BIO_get_host_ip, @function
BIO_get_host_ip:
.LFB267:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movl	$1, %r8d
	movl	$2, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	xorl	%esi, %esi
	pushq	%r13
	leaq	-56(%rbp), %r9
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -56(%rbp)
	call	BIO_lookup@PLT
	testl	%eax, %eax
	je	.L2
	movq	-56(%rbp), %rdi
	call	BIO_ADDRINFO_family@PLT
	cmpl	$2, %eax
	je	.L3
	movl	$32, %edi
	movl	$40, %r8d
	movl	$107, %edx
	xorl	%r12d, %r12d
	leaq	.LC0(%rip), %rcx
	movl	$106, %esi
	call	ERR_put_error@PLT
	movq	-56(%rbp), %rdi
.L4:
	call	BIO_ADDRINFO_free@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L2:
	movl	%eax, %r12d
	movq	%r13, %rdx
	movl	$2, %edi
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rsi
	call	ERR_add_error_data@PLT
.L1:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L11
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	movq	-56(%rbp), %rdi
	leaq	-48(%rbp), %r13
	call	BIO_ADDRINFO_address@PLT
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	BIO_ADDR_rawaddress@PLT
	movq	-56(%rbp), %rdi
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L4
	xorl	%r12d, %r12d
	cmpq	$4, -48(%rbp)
	jne	.L4
	call	BIO_ADDRINFO_address@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	BIO_ADDR_rawaddress@PLT
	movq	-56(%rbp), %rdi
	movl	%eax, %r12d
	jmp	.L4
.L11:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE267:
	.size	BIO_get_host_ip, .-BIO_get_host_ip
	.p2align 4
	.globl	BIO_get_port
	.type	BIO_get_port, @function
BIO_get_port:
.LFB268:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	testq	%rdi, %rdi
	je	.L20
	movq	%rdi, %r12
	movq	%rsi, %rbx
	leaq	-48(%rbp), %r9
	movq	%rdi, %rsi
	movl	$1, %r8d
	movl	$2, %ecx
	xorl	%edx, %edx
	xorl	%edi, %edi
	call	BIO_lookup@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L15
	movq	-48(%rbp), %rdi
	call	BIO_ADDRINFO_family@PLT
	cmpl	$2, %eax
	je	.L16
	movl	$73, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$141, %edx
	xorl	%r13d, %r13d
	movl	$107, %esi
	movl	$32, %edi
	call	ERR_put_error@PLT
.L17:
	movq	-48(%rbp), %rdi
	call	BIO_ADDRINFO_free@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L15:
	movq	%r12, %rdx
	leaq	.LC1(%rip), %rsi
	movl	$2, %edi
	xorl	%eax, %eax
	call	ERR_add_error_data@PLT
.L12:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L21
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movl	$64, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$113, %edx
	xorl	%r13d, %r13d
	movl	$107, %esi
	movl	$32, %edi
	call	ERR_put_error@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L16:
	movq	-48(%rbp), %rdi
	movl	$1, %r13d
	call	BIO_ADDRINFO_address@PLT
	movq	%rax, %rdi
	call	BIO_ADDR_rawport@PLT
	rolw	$8, %ax
	movw	%ax, (%rbx)
	jmp	.L17
.L21:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE268:
	.size	BIO_get_port, .-BIO_get_port
	.p2align 4
	.globl	BIO_sock_error
	.type	BIO_sock_error, @function
BIO_sock_error:
.LFB269:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %edx
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-12(%rbp), %r8
	leaq	-16(%rbp), %rcx
	movl	$0, -16(%rbp)
	movl	$4, -12(%rbp)
	call	getsockopt@PLT
	movl	%eax, %r8d
	movl	-16(%rbp), %eax
	testl	%r8d, %r8d
	jns	.L22
	call	__errno_location@PLT
	movl	(%rax), %eax
.L22:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L27
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L27:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE269:
	.size	BIO_sock_error, .-BIO_sock_error
	.p2align 4
	.globl	BIO_gethostbyname
	.type	BIO_gethostbyname, @function
BIO_gethostbyname:
.LFB270:
	.cfi_startproc
	endbr64
	jmp	gethostbyname@PLT
	.cfi_endproc
.LFE270:
	.size	BIO_gethostbyname, .-BIO_gethostbyname
	.p2align 4
	.globl	BIO_sock_init
	.type	BIO_sock_init, @function
BIO_sock_init:
.LFB271:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE271:
	.size	BIO_sock_init, .-BIO_sock_init
	.p2align 4
	.globl	bio_sock_cleanup_int
	.type	bio_sock_cleanup_int, @function
bio_sock_cleanup_int:
.LFB272:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE272:
	.size	bio_sock_cleanup_int, .-bio_sock_cleanup_int
	.p2align 4
	.globl	BIO_socket_ioctl
	.type	BIO_socket_ioctl, @function
BIO_socket_ioctl:
.LFB273:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	ioctl@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	js	.L34
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	call	__errno_location@PLT
	movl	$195, %r8d
	movl	$5, %esi
	leaq	.LC0(%rip), %rcx
	movl	(%rax), %edx
	movl	$2, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE273:
	.size	BIO_socket_ioctl, .-BIO_socket_ioctl
	.p2align 4
	.globl	BIO_get_accept_socket
	.type	BIO_get_accept_socket, @function
BIO_get_accept_socket:
.LFB274:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-56(%rbp), %rdx
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	%esi, %ebx
	leaq	-64(%rbp), %rsi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	call	BIO_parse_hostserv@PLT
	testl	%eax, %eax
	je	.L41
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rdi
	xorl	%ecx, %ecx
	leaq	-48(%rbp), %r9
	movl	$1, %r8d
	movl	$1, %edx
	call	BIO_lookup@PLT
	testl	%eax, %eax
	je	.L37
.L46:
	movl	$-1, %r12d
.L38:
	movq	-48(%rbp), %rdi
	call	BIO_ADDRINFO_free@PLT
	movq	-64(%rbp), %rdi
	movl	$229, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-56(%rbp), %rdi
	movl	$230, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
.L35:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L47
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	movq	-48(%rbp), %rdi
	call	BIO_ADDRINFO_protocol@PLT
	movq	-48(%rbp), %rdi
	movl	%eax, %r13d
	call	BIO_ADDRINFO_socktype@PLT
	movq	-48(%rbp), %rdi
	movl	%eax, %r12d
	call	BIO_ADDRINFO_family@PLT
	movl	%r12d, %esi
	xorl	%ecx, %ecx
	movl	%r13d, %edx
	movl	%eax, %edi
	call	BIO_socket@PLT
	movl	%eax, %r12d
	cmpl	$-1, %eax
	je	.L46
	movq	-48(%rbp), %rdi
	call	BIO_ADDRINFO_address@PLT
	xorl	%edx, %edx
	testl	%ebx, %ebx
	movl	%r12d, %edi
	setne	%dl
	movq	%rax, %rsi
	call	BIO_listen@PLT
	testl	%eax, %eax
	jne	.L38
	movl	%r12d, %edi
	call	BIO_closesocket@PLT
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L41:
	movl	$-1, %r12d
	jmp	.L35
.L47:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE274:
	.size	BIO_get_accept_socket, .-BIO_get_accept_socket
	.p2align 4
	.globl	BIO_accept
	.type	BIO_accept, @function
BIO_accept:
.LFB275:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-176(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%r13, %rsi
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	BIO_accept_ex@PLT
	movl	%eax, %r12d
	cmpl	$-1, %eax
	je	.L71
	testq	%rbx, %rbx
	je	.L48
	movq	%r13, %rdi
	movl	$1, %esi
	call	BIO_ADDR_hostname_string@PLT
	movq	%r13, %rdi
	movl	$1, %esi
	movq	%rax, %r14
	call	BIO_ADDR_service_string@PLT
	movq	%rax, %r13
	testq	%r14, %r14
	je	.L51
	testq	%rax, %rax
	jne	.L72
.L51:
	movq	$0, (%rbx)
.L52:
	movl	$260, %r8d
	movl	$65, %edx
	movl	$101, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	%r12d, %edi
	movl	$-1, %r12d
	call	BIO_closesocket@PLT
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L72:
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	strlen@PLT
	movl	$255, %edx
	leaq	.LC0(%rip), %rsi
	leaq	2(%r15,%rax), %rdi
	call	CRYPTO_zalloc@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L52
	movq	%r14, %rsi
	call	strcpy@PLT
	movq	(%rbx), %r15
	movq	%r15, %rdi
	call	strlen@PLT
	movl	$58, %edx
	movq	%r13, %rsi
	movw	%dx, (%r15,%rax)
	movq	(%rbx), %rdi
	call	strcat@PLT
.L54:
	movl	$268, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	movl	$269, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
.L48:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L73
	addq	$136, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	movl	$-1, %edi
	call	BIO_sock_should_retry@PLT
	testl	%eax, %eax
	je	.L74
.L50:
	endbr64
	movl	$-2, %r12d
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L74:
	call	__errno_location@PLT
	movl	$246, %r8d
	movl	$8, %esi
	leaq	.LC0(%rip), %rcx
	movl	(%rax), %edx
	movl	$2, %edi
	call	ERR_put_error@PLT
	movl	$247, %r8d
	movl	$100, %edx
	leaq	.LC0(%rip), %rcx
	movl	$101, %esi
	movl	$32, %edi
	call	ERR_put_error@PLT
	jmp	.L48
.L73:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE275:
	.size	BIO_accept, .-BIO_accept
	.p2align 4
	.globl	BIO_set_tcp_ndelay
	.type	BIO_set_tcp_ndelay, @function
BIO_set_tcp_ndelay:
.LFB276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %r8d
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	leaq	-4(%rbp), %rcx
	movl	%esi, -4(%rbp)
	movl	$6, %esi
	call	setsockopt@PLT
	leave
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE276:
	.size	BIO_set_tcp_ndelay, .-BIO_set_tcp_ndelay
	.p2align 4
	.globl	BIO_socket_nbio
	.type	BIO_socket_nbio, @function
BIO_socket_nbio:
.LFB277:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	leaq	-28(%rbp), %rdx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	%esi, -28(%rbp)
	movl	$21537, %esi
	call	ioctl@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	js	.L81
.L78:
	xorl	%eax, %eax
	testl	%ebx, %ebx
	sete	%al
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L82
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	call	__errno_location@PLT
	movl	$195, %r8d
	movl	$5, %esi
	leaq	.LC0(%rip), %rcx
	movl	(%rax), %edx
	movl	$2, %edi
	call	ERR_put_error@PLT
	jmp	.L78
.L82:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE277:
	.size	BIO_socket_nbio, .-BIO_socket_nbio
	.p2align 4
	.globl	BIO_sock_info
	.type	BIO_sock_info, @function
BIO_sock_info:
.LFB278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	jne	.L84
	movl	%edi, %r12d
	movq	(%rdx), %rdi
	movl	$112, -28(%rbp)
	call	BIO_ADDR_sockaddr_noconst@PLT
	leaq	-28(%rbp), %rdx
	movl	%r12d, %edi
	movq	%rax, %rsi
	call	getsockname@PLT
	cmpl	$-1, %eax
	je	.L92
	cmpl	$112, -28(%rbp)
	movl	$1, %eax
	jbe	.L83
	movl	$357, %r8d
	movl	$133, %edx
	movl	$141, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L91:
	xorl	%eax, %eax
.L83:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L93
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	movl	$363, %r8d
	movl	$140, %edx
	movl	$141, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L92:
	call	__errno_location@PLT
	movl	$352, %r8d
	movl	$16, %esi
	leaq	.LC0(%rip), %rcx
	movl	(%rax), %edx
	movl	$2, %edi
	call	ERR_put_error@PLT
	movl	$353, %r8d
	movl	$132, %edx
	leaq	.LC0(%rip), %rcx
	movl	$141, %esi
	movl	$32, %edi
	call	ERR_put_error@PLT
	jmp	.L91
.L93:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE278:
	.size	BIO_sock_info, .-BIO_sock_info
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
