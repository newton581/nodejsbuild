	.file	"dsa_gen.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/dsa/dsa_gen.c"
	.text
	.p2align 4
	.globl	dsa_builtin_paramgen
	.type	dsa_builtin_paramgen, @function
dsa_builtin_paramgen:
.LFB422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	shrq	$3, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$376, %rsp
	movq	16(%rbp), %rax
	movq	%rcx, -200(%rbp)
	movq	%rax, -264(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -272(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -280(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%edx, %eax
	movl	%edx, -216(%rbp)
	andl	$-9, %eax
	cmpl	$20, %eax
	je	.L54
	xorl	%r15d, %r15d
	cmpl	$32, %edx
	jne	.L1
.L54:
	cmpq	$0, -200(%rbp)
	je	.L178
	movq	-200(%rbp), %rdi
	call	EVP_MD_size@PLT
	movl	%eax, -216(%rbp)
.L6:
	testq	%r13, %r13
	je	.L8
	movslq	-216(%rbp), %rdx
	cmpq	%r14, %rdx
	ja	.L179
	leaq	-192(%rbp), %rax
	movl	$32, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	__memcpy_chk@PLT
.L8:
	call	BN_MONT_CTX_new@PLT
	movq	%rax, -304(%rbp)
	testq	%rax, %rax
	je	.L12
	call	BN_CTX_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L12
	movq	%rax, %rdi
	xorl	%r15d, %r15d
	call	BN_CTX_start@PLT
	movq	%r14, %rdi
	call	BN_CTX_get@PLT
	movq	%r14, %rdi
	movq	%rax, -368(%rbp)
	call	BN_CTX_get@PLT
	movq	%r14, %rdi
	movq	%rax, -240(%rbp)
	call	BN_CTX_get@PLT
	movq	%r14, %rdi
	movq	%rax, -376(%rbp)
	call	BN_CTX_get@PLT
	movq	%r14, %rdi
	movq	%rax, -208(%rbp)
	call	BN_CTX_get@PLT
	movq	%r14, %rdi
	movq	%rax, -392(%rbp)
	call	BN_CTX_get@PLT
	movq	%r14, %rdi
	movq	%rax, -384(%rbp)
	call	BN_CTX_get@PLT
	movq	%r14, %rdi
	movq	%rax, -232(%rbp)
	call	BN_CTX_get@PLT
	movq	%rax, -352(%rbp)
	testq	%rax, %rax
	je	.L11
	cmpq	$512, %r12
	movl	$512, %eax
	cmovb	%rax, %r12
	addq	$63, %r12
	andq	$-64, %r12
	leal	-1(%r12), %r15d
	movl	%r15d, -396(%rbp)
	call	BN_value_one@PLT
	movq	-352(%rbp), %rdi
	movl	%r15d, %edx
	movq	%rax, %rsi
	call	BN_lshift@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L11
	movl	-216(%rbp), %r15d
	leaq	-1(%r12), %rdx
	movabsq	$-3689348814741910323, %rcx
	movl	$0, -324(%rbp)
	movq	%rdx, %rax
	movq	%r14, -336(%rbp)
	mulq	%rcx
	leal	-1(%r15), %esi
	movq	%rbx, -408(%rbp)
	movq	-368(%rbp), %rbx
	leal	0(,%r15,8), %ecx
	movslq	%esi, %rdi
	movl	%esi, -252(%rbp)
	movl	%ecx, -320(%rbp)
	movl	%r15d, %ecx
	shrl	$4, %ecx
	shrq	$7, %rdx
	movq	%rdi, -344(%rbp)
	movl	%ecx, -360(%rbp)
	movl	%r15d, %ecx
	andl	$-16, %ecx
	movl	%edx, -256(%rbp)
	movl	%ecx, -356(%rbp)
	leaq	-128(%rbp), %rcx
	movq	%rcx, -248(%rbp)
	addq	%rdi, %rcx
	movq	%rcx, -296(%rbp)
	subl	%ecx, %esi
	movl	%esi, %r12d
.L13:
	xorl	%eax, %eax
	movl	-324(%rbp), %edx
	testq	%r13, %r13
	movq	-224(%rbp), %rdi
	sete	%al
	xorl	%esi, %esi
	movl	%eax, -312(%rbp)
	call	BN_GENCB_call@PLT
	testl	%eax, %eax
	je	.L173
	leaq	-192(%rbp), %rax
	movq	%rax, -288(%rbp)
	testq	%r13, %r13
	je	.L14
.L17:
	movq	-288(%rbp), %r13
	movq	-248(%rbp), %rdi
	movslq	%r15d, %r14
	movl	$32, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	__memcpy_chk@PLT
	leaq	-96(%rbp), %r10
	movq	%r14, %rdx
	movq	%r13, %rsi
	movl	$32, %ecx
	movq	%r10, %rdi
	call	__memcpy_chk@PLT
	movl	-252(%rbp), %ecx
	movq	-296(%rbp), %rdx
	movq	%rax, %r10
	testl	%ecx, %ecx
	jns	.L18
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L180:
	subq	$1, %rdx
	movl	%r12d, %eax
	addl	%edx, %eax
	js	.L16
.L18:
	addb	$1, (%rdx)
	je	.L180
.L16:
	leaq	-160(%rbp), %r13
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	-200(%rbp), %r8
	movq	-288(%rbp), %rdi
	movq	%r13, %rdx
	movq	%r10, -216(%rbp)
	call	EVP_Digest@PLT
	testl	%eax, %eax
	je	.L173
	movq	-216(%rbp), %r10
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	-200(%rbp), %r8
	movq	-248(%rbp), %rdi
	movq	%r10, %rdx
	call	EVP_Digest@PLT
	testl	%eax, %eax
	je	.L173
	testl	%r15d, %r15d
	jle	.L19
	cmpl	$14, -252(%rbp)
	jbe	.L46
	movdqa	-160(%rbp), %xmm0
	pxor	-96(%rbp), %xmm0
	cmpl	$1, -360(%rbp)
	movaps	%xmm0, -160(%rbp)
	jbe	.L21
	movdqa	-144(%rbp), %xmm0
	pxor	-80(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
.L21:
	movl	-356(%rbp), %eax
	cmpl	%eax, %r15d
	je	.L19
.L20:
	movslq	%eax, %rdx
	movzbl	-160(%rbp,%rdx), %ecx
	xorb	-96(%rbp,%rdx), %cl
	movb	%cl, -160(%rbp,%rdx)
	leal	1(%rax), %edx
	cmpl	%edx, %r15d
	jle	.L19
	movslq	%edx, %rdx
	movzbl	-160(%rbp,%rdx), %ecx
	xorb	-96(%rbp,%rdx), %cl
	movb	%cl, -160(%rbp,%rdx)
	leal	2(%rax), %edx
	cmpl	%r15d, %edx
	jge	.L19
	movslq	%edx, %rdx
	movzbl	-96(%rbp,%rdx), %ecx
	xorb	%cl, -160(%rbp,%rdx)
	leal	3(%rax), %edx
	cmpl	%r15d, %edx
	jge	.L19
	movslq	%edx, %rdx
	movzbl	-160(%rbp,%rdx), %ecx
	xorb	-96(%rbp,%rdx), %cl
	movb	%cl, -160(%rbp,%rdx)
	leal	4(%rax), %edx
	cmpl	%r15d, %edx
	jge	.L19
	movslq	%edx, %rdx
	movzbl	-96(%rbp,%rdx), %ecx
	xorb	%cl, -160(%rbp,%rdx)
	leal	5(%rax), %edx
	cmpl	%r15d, %edx
	jge	.L19
	movslq	%edx, %rdx
	movzbl	-96(%rbp,%rdx), %ecx
	xorb	%cl, -160(%rbp,%rdx)
	leal	6(%rax), %edx
	cmpl	%edx, %r15d
	jle	.L19
	movslq	%edx, %rdx
	movzbl	-160(%rbp,%rdx), %ecx
	xorb	-96(%rbp,%rdx), %cl
	movb	%cl, -160(%rbp,%rdx)
	leal	7(%rax), %edx
	cmpl	%edx, %r15d
	jle	.L19
	movslq	%edx, %rdx
	movzbl	-160(%rbp,%rdx), %ecx
	xorb	-96(%rbp,%rdx), %cl
	movb	%cl, -160(%rbp,%rdx)
	leal	8(%rax), %edx
	cmpl	%edx, %r15d
	jle	.L19
	movslq	%edx, %rdx
	movzbl	-160(%rbp,%rdx), %ecx
	xorb	-96(%rbp,%rdx), %cl
	movb	%cl, -160(%rbp,%rdx)
	leal	9(%rax), %edx
	cmpl	%edx, %r15d
	jle	.L19
	movslq	%edx, %rdx
	movzbl	-160(%rbp,%rdx), %ecx
	xorb	-96(%rbp,%rdx), %cl
	movb	%cl, -160(%rbp,%rdx)
	leal	10(%rax), %edx
	cmpl	%edx, %r15d
	jle	.L19
	movslq	%edx, %rdx
	movzbl	-160(%rbp,%rdx), %ecx
	xorb	-96(%rbp,%rdx), %cl
	movb	%cl, -160(%rbp,%rdx)
	leal	11(%rax), %edx
	cmpl	%edx, %r15d
	jle	.L19
	movslq	%edx, %rdx
	movzbl	-160(%rbp,%rdx), %ecx
	xorb	-96(%rbp,%rdx), %cl
	movb	%cl, -160(%rbp,%rdx)
	leal	12(%rax), %edx
	cmpl	%edx, %r15d
	jle	.L19
	movslq	%edx, %rdx
	movzbl	-160(%rbp,%rdx), %ecx
	xorb	-96(%rbp,%rdx), %cl
	movb	%cl, -160(%rbp,%rdx)
	leal	13(%rax), %edx
	cmpl	%edx, %r15d
	jle	.L19
	movslq	%edx, %rdx
	addl	$14, %eax
	movzbl	-160(%rbp,%rdx), %ecx
	xorb	-96(%rbp,%rdx), %cl
	movb	%cl, -160(%rbp,%rdx)
	cmpl	%eax, %r15d
	jle	.L19
	cltq
	movzbl	-96(%rbp,%rax), %edx
	xorb	%dl, -160(%rbp,%rax)
	.p2align 4,,10
	.p2align 3
.L19:
	movq	-344(%rbp), %rax
	movq	-208(%rbp), %rdx
	movl	%r15d, %esi
	movq	%r13, %rdi
	orb	$-128, -160(%rbp)
	orb	$1, -160(%rbp,%rax)
	call	BN_bin2bn@PLT
	testq	%rax, %rax
	je	.L50
	movq	-224(%rbp), %r8
	movl	-312(%rbp), %ecx
	movl	$64, %esi
	movq	-336(%rbp), %rdx
	movq	-208(%rbp), %rdi
	call	BN_is_prime_fasttest_ex@PLT
	testl	%eax, %eax
	jg	.L23
	jne	.L50
.L24:
	addl	$1, -324(%rbp)
	xorl	%r13d, %r13d
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L179:
	movl	$85, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$110, %edx
	xorl	%r15d, %r15d
	movl	$125, %esi
	movl	$10, %edi
	call	ERR_put_error@PLT
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L181
	addq	$376, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	cmpl	$20, %edx
	je	.L182
	cmpl	$28, %edx
	je	.L183
	call	EVP_sha256@PLT
	movq	%rax, -200(%rbp)
	jmp	.L6
.L36:
	movq	-224(%rbp), %rdi
	movl	$1, %edx
	movl	$3, %esi
	movq	%r15, %rbx
	movl	%eax, %r15d
	call	BN_GENCB_call@PLT
	testl	%eax, %eax
	je	.L11
	movq	8(%rbx), %rdi
	call	BN_free@PLT
	movq	16(%rbx), %rdi
	call	BN_free@PLT
	movq	24(%rbx), %rdi
	call	BN_free@PLT
	movq	-232(%rbp), %rdi
	call	BN_dup@PLT
	movq	-208(%rbp), %rdi
	movq	%rax, 8(%rbx)
	call	BN_dup@PLT
	movq	-240(%rbp), %rdi
	movq	%rax, 16(%rbx)
	call	BN_dup@PLT
	cmpq	$0, 8(%rbx)
	movq	%rax, 24(%rbx)
	je	.L11
	cmpq	$0, 16(%rbx)
	je	.L11
	testq	%rax, %rax
	je	.L11
	movq	-272(%rbp), %rax
	testq	%rax, %rax
	je	.L39
	movl	-328(%rbp), %esi
	movl	%esi, (%rax)
.L39:
	movq	-280(%rbp), %rax
	testq	%rax, %rax
	je	.L40
	movl	%r12d, %ecx
	movq	%rcx, (%rax)
.L40:
	movq	-264(%rbp), %rdi
	movl	$1, %r15d
	testq	%rdi, %rdi
	je	.L11
	movq	-288(%rbp), %rsi
	movq	%r13, %rdx
	call	memcpy@PLT
	.p2align 4,,10
	.p2align 3
.L11:
	movq	%r14, %rdi
	call	BN_CTX_end@PLT
	movq	%r14, %rdi
	call	BN_CTX_free@PLT
	movq	-304(%rbp), %rdi
	call	BN_MONT_CTX_free@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L12:
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L183:
	call	EVP_sha224@PLT
	movq	%rax, -200(%rbp)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L14:
	movl	%r15d, %esi
	movq	%rax, %rdi
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jg	.L17
.L50:
	movq	-336(%rbp), %r14
	xorl	%r15d, %r15d
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L23:
	movq	-224(%rbp), %rdi
	xorl	%edx, %edx
	movl	$2, %esi
	call	BN_GENCB_call@PLT
	testl	%eax, %eax
	je	.L173
	movq	-224(%rbp), %rdi
	xorl	%edx, %edx
	movl	$3, %esi
	call	BN_GENCB_call@PLT
	testl	%eax, %eax
	je	.L173
	movl	$0, -328(%rbp)
	movq	%r14, -312(%rbp)
	movl	%r15d, -316(%rbp)
	movq	-376(%rbp), %r15
.L25:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	BN_set_word@PLT
	movl	-256(%rbp), %edx
	testl	%edx, %edx
	js	.L26
	movl	$0, -216(%rbp)
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L27:
	movl	-252(%rbp), %eax
	movq	-296(%rbp), %rdx
	testl	%eax, %eax
	jns	.L29
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L184:
	subq	$1, %rdx
	movl	%r12d, %eax
	addl	%edx, %eax
	js	.L28
.L29:
	addb	$1, (%rdx)
	je	.L184
.L28:
	movq	-200(%rbp), %r8
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	-312(%rbp), %rsi
	movq	-248(%rbp), %rdi
	call	EVP_Digest@PLT
	testl	%eax, %eax
	je	.L173
	movl	-316(%rbp), %esi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	BN_bin2bn@PLT
	testq	%rax, %rax
	je	.L50
	movl	%r14d, %edx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	BN_lshift@PLT
	testl	%eax, %eax
	je	.L173
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	BN_add@PLT
	testl	%eax, %eax
	je	.L173
	addl	$1, -216(%rbp)
	addl	-320(%rbp), %r14d
	movl	-216(%rbp), %eax
	cmpl	%eax, -256(%rbp)
	jge	.L27
.L26:
	movl	-396(%rbp), %esi
	movq	%r15, %rdi
	call	BN_mask_bits@PLT
	testl	%eax, %eax
	je	.L173
	movq	-392(%rbp), %r14
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L50
	movq	-352(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	BN_add@PLT
	testl	%eax, %eax
	je	.L173
	movq	-208(%rbp), %rsi
	movq	%rbx, %rdi
	call	BN_lshift1@PLT
	testl	%eax, %eax
	je	.L173
	movq	-336(%rbp), %r8
	xorl	%edi, %edi
	movq	%rbx, %rcx
	movq	%r14, %rdx
	movq	-384(%rbp), %rsi
	call	BN_div@PLT
	testl	%eax, %eax
	je	.L173
	call	BN_value_one@PLT
	movq	-384(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	BN_sub@PLT
	testl	%eax, %eax
	je	.L173
	movq	%r14, %rsi
	movq	-232(%rbp), %r14
	movq	%rbx, %rdx
	movq	%r14, %rdi
	call	BN_sub@PLT
	testl	%eax, %eax
	je	.L173
	movq	-352(%rbp), %rsi
	movq	%r14, %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	jns	.L31
.L35:
	addl	$1, -328(%rbp)
	movl	-328(%rbp), %eax
	cmpl	$4096, %eax
	je	.L185
	movq	-224(%rbp), %rdi
	xorl	%esi, %esi
	movl	%eax, %edx
	call	BN_GENCB_call@PLT
	testl	%eax, %eax
	jne	.L25
	.p2align 4,,10
	.p2align 3
.L173:
	movq	-336(%rbp), %r14
	movl	%eax, %r15d
	jmp	.L11
.L182:
	call	EVP_sha1@PLT
	movq	%rax, -200(%rbp)
	jmp	.L6
.L46:
	xorl	%eax, %eax
	jmp	.L20
.L31:
	movq	-224(%rbp), %r8
	movl	$1, %ecx
	movl	$64, %esi
	movq	-336(%rbp), %rdx
	movq	-232(%rbp), %rdi
	call	BN_is_prime_fasttest_ex@PLT
	testl	%eax, %eax
	jg	.L186
	je	.L35
	jmp	.L50
.L181:
	call	__stack_chk_fail@PLT
.L185:
	movl	-316(%rbp), %r15d
	jmp	.L24
.L186:
	movq	-224(%rbp), %rdi
	movl	$1, %edx
	movl	$2, %esi
	movq	-312(%rbp), %r13
	movq	-336(%rbp), %r14
	movq	-408(%rbp), %rbx
	call	BN_GENCB_call@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L11
	call	BN_value_one@PLT
	movq	-352(%rbp), %r12
	movq	-232(%rbp), %rsi
	movq	%rax, %rdx
	movq	%r12, %rdi
	call	BN_sub@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L11
	movq	-208(%rbp), %rcx
	movq	%r14, %r8
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	-368(%rbp), %rdi
	call	BN_div@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L11
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%r12, -352(%rbp)
	call	BN_set_word@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L11
	movq	-232(%rbp), %rsi
	movq	-304(%rbp), %rdi
	movq	%r14, %rdx
	call	BN_MONT_CTX_set@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L11
	movq	%rbx, %r15
	movl	$2, %r12d
	movq	-352(%rbp), %rbx
	jmp	.L37
.L187:
	movq	-240(%rbp), %rdi
	call	BN_is_one@PLT
	testl	%eax, %eax
	je	.L36
	call	BN_value_one@PLT
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	BN_add@PLT
	testl	%eax, %eax
	je	.L175
	addl	$1, %r12d
.L37:
	movq	-304(%rbp), %r9
	movq	%r14, %r8
	movq	%rbx, %rsi
	movq	-232(%rbp), %rcx
	movq	-368(%rbp), %rdx
	movq	-240(%rbp), %rdi
	call	BN_mod_exp_mont@PLT
	testl	%eax, %eax
	jne	.L187
.L175:
	movl	%eax, %r15d
	jmp	.L11
	.cfi_endproc
.LFE422:
	.size	dsa_builtin_paramgen, .-dsa_builtin_paramgen
	.p2align 4
	.globl	DSA_generate_parameters_ex
	.type	DSA_generate_parameters_ex, @function
DSA_generate_parameters_ex:
.LFB421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$40, %rsp
	movq	80(%rdi), %rax
	movq	16(%rbp), %r8
	movq	80(%rax), %rax
	testq	%rax, %rax
	je	.L189
	leaq	-40(%rbp), %rsp
	movq	%rbx, %r8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore_state
	movq	%r8, -64(%rbp)
	movq	%r9, -56(%rbp)
	cmpl	$2047, %esi
	jg	.L193
	call	EVP_sha1@PLT
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %r9
	movq	%rax, %rcx
.L191:
	movq	%rcx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	EVP_MD_size@PLT
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %r9
	movslq	%r13d, %rsi
	movq	-56(%rbp), %rcx
	leal	0(,%rax,8), %edx
	movq	%r12, %rdi
	pushq	%r8
	movslq	%edx, %rdx
	movq	%r14, %r8
	pushq	%r9
	movslq	%r15d, %r9
	pushq	%rbx
	pushq	$0
	call	dsa_builtin_paramgen
	addq	$32, %rsp
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L193:
	.cfi_restore_state
	call	EVP_sha256@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %r8
	movq	%rax, %rcx
	jmp	.L191
	.cfi_endproc
.LFE421:
	.size	DSA_generate_parameters_ex, .-DSA_generate_parameters_ex
	.p2align 4
	.globl	dsa_builtin_paramgen2
	.type	dsa_builtin_paramgen2, @function
dsa_builtin_paramgen2:
.LFB423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$344, %rsp
	movq	24(%rbp), %rax
	movq	%rsi, -248(%rbp)
	movq	%rdx, -216(%rbp)
	movq	%rax, -152(%rbp)
	movq	32(%rbp), %rax
	movq	%rcx, -136(%rbp)
	movq	%rax, -224(%rbp)
	movq	40(%rbp), %rax
	movq	%r8, -208(%rbp)
	movq	%rax, -232(%rbp)
	movq	48(%rbp), %rax
	movq	%r9, -144(%rbp)
	movq	%rax, -240(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_MD_CTX_new@PLT
	movq	%rax, -184(%rbp)
	testq	%rax, %rax
	je	.L247
	cmpq	%r15, %r14
	jnb	.L342
	cmpq	$0, -136(%rbp)
	je	.L343
.L197:
	movq	-136(%rbp), %rdi
	call	EVP_MD_size@PLT
	cmpq	$0, 8(%rbx)
	movl	%eax, -196(%rbp)
	je	.L200
	cmpq	$0, 16(%rbx)
	je	.L200
	movl	16(%rbp), %r8d
	testl	%r8d, %r8d
	js	.L248
.L200:
	movq	-144(%rbp), %rcx
	movslq	-196(%rbp), %rax
	movl	$350, %edx
	leaq	.LC0(%rip), %rsi
	testq	%rcx, %rcx
	cmovne	%rcx, %rax
	movq	%rax, %rdi
	movq	%rax, -144(%rbp)
	call	CRYPTO_malloc@PLT
	cmpq	$0, -152(%rbp)
	movq	%rax, -160(%rbp)
	je	.L344
	cmpq	$0, -160(%rbp)
	je	.L244
	movq	-152(%rbp), %rax
	movq	%rax, -192(%rbp)
.L245:
	movq	-208(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L201
	movq	-144(%rbp), %rdx
	movq	-160(%rbp), %rdi
	call	memcpy@PLT
.L201:
	call	BN_CTX_new@PLT
	movq	%rax, -168(%rbp)
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L250
	call	BN_MONT_CTX_new@PLT
	movl	$-1, %r14d
	movq	%rax, -176(%rbp)
	testq	%rax, %rax
	je	.L195
	movq	%r15, %rdi
	call	BN_CTX_start@PLT
	movq	%r15, %rdi
	call	BN_CTX_get@PLT
	movq	%r15, %rdi
	movq	%rax, -280(%rbp)
	call	BN_CTX_get@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	BN_CTX_get@PLT
	movq	%r15, %rdi
	movq	%rax, -296(%rbp)
	call	BN_CTX_get@PLT
	movq	%r15, %rdi
	movq	%rax, -312(%rbp)
	call	BN_CTX_get@PLT
	movq	%r15, %rdi
	movq	%rax, -304(%rbp)
	call	BN_CTX_get@PLT
	movq	%rax, -256(%rbp)
	testq	%rax, %rax
	je	.L195
	movq	8(%rbx), %r15
	testq	%r15, %r15
	je	.L205
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L205
	movl	$0, -200(%rbp)
	movl	16(%rbp), %edi
	testl	%edi, %edi
	jns	.L345
.L206:
	call	BN_value_one@PLT
	movq	-256(%rbp), %rdi
	movq	%r15, %rsi
	movl	$-1, %r14d
	movq	%rax, %rdx
	call	BN_sub@PLT
	testl	%eax, %eax
	jne	.L346
	.p2align 4,,10
	.p2align 3
.L195:
	movq	-160(%rbp), %rdi
	movl	$606, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-192(%rbp), %rax
	cmpq	%rax, -152(%rbp)
	je	.L241
	movl	$608, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	call	CRYPTO_free@PLT
.L241:
	movq	-168(%rbp), %rbx
	movq	%rbx, %rdi
	call	BN_CTX_end@PLT
	movq	%rbx, %rdi
	call	BN_CTX_free@PLT
	movq	-176(%rbp), %rdi
	call	BN_MONT_CTX_free@PLT
	movq	-184(%rbp), %rdi
	call	EVP_MD_CTX_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L347
	addq	$344, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L342:
	.cfi_restore_state
	movl	$331, %r8d
	movl	$112, %edx
	movl	$126, %esi
	movl	$10, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L247:
	movq	$0, -168(%rbp)
	movl	$-1, %r14d
	movq	$0, -176(%rbp)
	movq	$0, -192(%rbp)
	movq	$0, -160(%rbp)
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L343:
	cmpq	$160, -216(%rbp)
	je	.L348
	cmpq	$224, -216(%rbp)
	je	.L349
	call	EVP_sha256@PLT
	movq	%rax, -136(%rbp)
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L248:
	movq	$0, -192(%rbp)
	movq	$0, -160(%rbp)
	jmp	.L201
.L344:
	movq	-144(%rbp), %rdi
	movl	$355, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	cmpq	$0, -160(%rbp)
	movq	%rax, -192(%rbp)
	je	.L249
	testq	%rax, %rax
	jne	.L245
.L249:
	movq	$0, -168(%rbp)
	movl	$-1, %r14d
	movq	$0, -176(%rbp)
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L244:
	movl	$606, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	movl	$-1, %r14d
	call	CRYPTO_free@PLT
	movq	$0, -168(%rbp)
	movq	$0, -176(%rbp)
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L250:
	movq	$0, -176(%rbp)
	movl	$-1, %r14d
	jmp	.L195
.L205:
	movq	-168(%rbp), %r14
	movq	%r14, %rdi
	call	BN_CTX_get@PLT
	movq	%r14, %rdi
	movl	$-1, %r14d
	movq	%rax, %r15
	call	BN_CTX_get@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L195
	movl	-248(%rbp), %eax
	subl	$1, %eax
	movl	%eax, -332(%rbp)
	call	BN_value_one@PLT
	movl	-332(%rbp), %edx
	movq	-256(%rbp), %rdi
	movq	%rax, %rsi
	call	BN_lshift@PLT
	testl	%eax, %eax
	je	.L195
	movq	-216(%rbp), %rax
	movslq	-196(%rbp), %rcx
	movl	$0, -288(%rbp)
	movq	%r12, -328(%rbp)
	shrq	$3, %rax
	movq	%rcx, -344(%rbp)
	movslq	%eax, %rsi
	leal	0(,%rcx,8), %eax
	movq	%rbx, -384(%rbp)
	movl	%eax, -264(%rbp)
	cltq
	movq	%rax, -352(%rbp)
	movl	-144(%rbp), %eax
	movl	%esi, -284(%rbp)
	subl	$1, %eax
	movq	%rsi, -320(%rbp)
	movl	%eax, -260(%rbp)
	movl	-248(%rbp), %eax
	movq	%r13, -376(%rbp)
	movq	-160(%rbp), %r13
	sall	$2, %eax
	movq	%r15, -368(%rbp)
	movl	%eax, -336(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, %r15
	addq	%rcx, %rsi
	movq	%rsi, -360(%rbp)
.L207:
	movl	-288(%rbp), %edx
	movq	-240(%rbp), %rdi
	xorl	%esi, %esi
	call	BN_GENCB_call@PLT
	testl	%eax, %eax
	je	.L294
	cmpq	$0, -208(%rbp)
	je	.L208
.L210:
	movq	-136(%rbp), %r8
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	-144(%rbp), %rsi
	movq	%r13, %rdi
	call	EVP_Digest@PLT
	testl	%eax, %eax
	je	.L294
	movl	-196(%rbp), %esi
	cmpl	%esi, -284(%rbp)
	jge	.L211
	movq	-344(%rbp), %rdi
	subq	-320(%rbp), %rdi
	addq	%r15, %rdi
.L212:
	movq	-320(%rbp), %rax
	orb	$-128, (%rdi)
	movq	-328(%rbp), %rbx
	movl	-284(%rbp), %esi
	orb	$1, -1(%rdi,%rax)
	movq	%rbx, %rdx
	call	BN_bin2bn@PLT
	testq	%rax, %rax
	je	.L294
	movq	-208(%rbp), %r14
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movq	-240(%rbp), %r8
	movq	-168(%rbp), %rdx
	movl	$64, %esi
	testq	%r14, %r14
	setne	%cl
	call	BN_is_prime_fasttest_ex@PLT
	testl	%eax, %eax
	jg	.L213
	jne	.L294
	testq	%r14, %r14
	jne	.L350
.L214:
	addl	$1, -288(%rbp)
	jmp	.L207
.L349:
	call	EVP_sha224@PLT
	movq	%rax, -136(%rbp)
	jmp	.L197
.L346:
	movq	-168(%rbp), %r8
	movq	-256(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r12, %rcx
	movq	-280(%rbp), %rdi
	call	BN_div@PLT
	testl	%eax, %eax
	je	.L195
	movl	$1, -208(%rbp)
	movl	16(%rbp), %ecx
	testl	%ecx, %ecx
	js	.L351
.L230:
	movq	-168(%rbp), %rdx
	movq	-176(%rbp), %rdi
	movq	%r15, %rsi
	movl	$-1, %r14d
	call	BN_MONT_CTX_set@PLT
	testl	%eax, %eax
	je	.L195
	movq	%r15, -216(%rbp)
	leaq	-128(%rbp), %r14
	movl	-208(%rbp), %r15d
	movq	%r12, -248(%rbp)
	movq	-256(%rbp), %r12
	movq	%rbx, -256(%rbp)
	movq	-184(%rbp), %rbx
	jmp	.L231
.L236:
	leal	1(%r15), %eax
	cmpl	$65535, %eax
	ja	.L294
.L237:
	movl	%eax, %r15d
.L231:
	movl	16(%rbp), %edx
	testl	%edx, %edx
	js	.L234
	movzbl	16(%rbp), %eax
	movq	-136(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movb	%al, -128(%rbp)
	movl	%r15d, %eax
	rolw	$8, %ax
	movw	%ax, -127(%rbp)
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	je	.L294
	movq	-144(%rbp), %rdx
	movq	-192(%rbp), %rsi
	movq	%rbx, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L294
	movl	$4, %edx
	leaq	ggen.11416(%rip), %rsi
	movq	%rbx, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L294
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L294
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	je	.L294
	movl	-196(%rbp), %esi
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	BN_bin2bn@PLT
	testq	%rax, %rax
	je	.L294
.L234:
	movq	-176(%rbp), %r9
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	-168(%rbp), %r8
	movq	-216(%rbp), %rcx
	movq	-280(%rbp), %rdx
	call	BN_mod_exp_mont@PLT
	testl	%eax, %eax
	je	.L294
	movq	%r13, %rdi
	call	BN_is_one@PLT
	testl	%eax, %eax
	je	.L235
	movl	16(%rbp), %eax
	testl	%eax, %eax
	jns	.L236
	call	BN_value_one@PLT
	movq	%r12, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	BN_add@PLT
	testl	%eax, %eax
	je	.L294
	leal	1(%r15), %eax
	jmp	.L237
.L345:
	movq	-144(%rbp), %rdx
	movq	-160(%rbp), %rsi
	movq	-192(%rbp), %rdi
	call	memcpy@PLT
	jmp	.L206
.L354:
	movq	-240(%rbp), %rdi
	movl	$1, %edx
	movl	$2, %esi
	movq	-368(%rbp), %r15
	movq	-328(%rbp), %r12
	movq	-376(%rbp), %r13
	movq	-384(%rbp), %rbx
	call	BN_GENCB_call@PLT
	testl	%eax, %eax
	jne	.L206
	.p2align 4,,10
	.p2align 3
.L294:
	movl	$-1, %r14d
	jmp	.L195
.L211:
	movq	%r15, %rdi
	jle	.L212
	movl	-284(%rbp), %edx
	movq	-360(%rbp), %rdi
	xorl	%esi, %esi
	subl	-196(%rbp), %edx
	movslq	%edx, %rdx
	call	memset@PLT
	movq	%r15, %rdi
	jmp	.L212
.L348:
	call	EVP_sha1@PLT
	movq	%rax, -136(%rbp)
	jmp	.L197
.L213:
	movq	-152(%rbp), %rax
	testq	%rax, %rax
	je	.L215
	movq	-144(%rbp), %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
.L215:
	movq	-240(%rbp), %rbx
	xorl	%edx, %edx
	movl	$2, %esi
	movq	%rbx, %rdi
	call	BN_GENCB_call@PLT
	testl	%eax, %eax
	je	.L294
	xorl	%edx, %edx
	movl	$3, %esi
	movq	%rbx, %rdi
	call	BN_GENCB_call@PLT
	testl	%eax, %eax
	je	.L294
	movq	-248(%rbp), %rax
	xorl	%edx, %edx
	movq	%r13, %r14
	movq	-280(%rbp), %rbx
	movl	$0, -200(%rbp)
	movq	%r15, %r13
	movq	-296(%rbp), %r15
	subq	$1, %rax
	divq	-352(%rbp)
	movq	%rax, -272(%rbp)
.L216:
	xorl	%esi, %esi
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	BN_set_word@PLT
	movl	-272(%rbp), %esi
	movl	$0, -216(%rbp)
	testl	%esi, %esi
	js	.L224
	movq	%r13, %rax
	movq	%r14, %r13
	movq	%r15, %r14
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L223:
	movslq	-260(%rbp), %rdx
	testl	%edx, %edx
	jns	.L222
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L352:
	subq	$1, %rdx
	testl	%edx, %edx
	js	.L221
.L222:
	addb	$1, 0(%r13,%rdx)
	je	.L352
.L221:
	movq	-136(%rbp), %r8
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	-144(%rbp), %rsi
	movq	%r13, %rdi
	call	EVP_Digest@PLT
	testl	%eax, %eax
	je	.L294
	movl	-196(%rbp), %esi
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	BN_bin2bn@PLT
	testq	%rax, %rax
	je	.L294
	movl	%r12d, %edx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	BN_lshift@PLT
	testl	%eax, %eax
	je	.L294
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	BN_add@PLT
	testl	%eax, %eax
	je	.L294
	addl	$1, -216(%rbp)
	addl	-264(%rbp), %r12d
	movl	-216(%rbp), %eax
	cmpl	%eax, -272(%rbp)
	jge	.L223
	movq	%r15, %rax
	movq	%r14, %r15
	movq	%r13, %r14
	movq	%rax, %r13
.L224:
	movl	-332(%rbp), %esi
	movq	%r15, %rdi
	call	BN_mask_bits@PLT
	testl	%eax, %eax
	je	.L294
	movq	-312(%rbp), %r12
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L294
	movq	-256(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	BN_add@PLT
	testl	%eax, %eax
	je	.L294
	movq	-328(%rbp), %rsi
	movq	%rbx, %rdi
	call	BN_lshift1@PLT
	testl	%eax, %eax
	je	.L294
	movq	-168(%rbp), %r8
	xorl	%edi, %edi
	movq	%rbx, %rcx
	movq	%r12, %rdx
	movq	-304(%rbp), %rsi
	call	BN_div@PLT
	testl	%eax, %eax
	je	.L294
	call	BN_value_one@PLT
	movq	-304(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	BN_sub@PLT
	testl	%eax, %eax
	je	.L294
	movq	%r12, %rsi
	movq	-368(%rbp), %r12
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	BN_sub@PLT
	testl	%eax, %eax
	je	.L294
	movq	-256(%rbp), %rsi
	movq	%r12, %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	jns	.L225
.L229:
	addl	$1, -200(%rbp)
	movl	-200(%rbp), %eax
	cmpl	%eax, -336(%rbp)
	jle	.L353
	movq	-240(%rbp), %rdi
	xorl	%esi, %esi
	movl	%eax, %edx
	call	BN_GENCB_call@PLT
	testl	%eax, %eax
	jne	.L216
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L208:
	movl	-144(%rbp), %esi
	movq	%r13, %rdi
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jg	.L210
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L351:
	movq	-256(%rbp), %rdi
	movl	$2, %esi
	call	BN_set_word@PLT
	testl	%eax, %eax
	je	.L195
	movl	$2, -208(%rbp)
	jmp	.L230
.L225:
	movq	-240(%rbp), %r8
	movl	$1, %ecx
	movl	$64, %esi
	movq	-168(%rbp), %rdx
	movq	-368(%rbp), %rdi
	call	BN_is_prime_fasttest_ex@PLT
	testl	%eax, %eax
	jg	.L354
	je	.L229
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L235:
	movl	$1, %edx
	movq	-240(%rbp), %rdi
	movl	$3, %esi
	movl	%r15d, -208(%rbp)
	movq	-248(%rbp), %r12
	movq	-216(%rbp), %r15
	movl	$-1, %r14d
	movq	-256(%rbp), %rbx
	call	BN_GENCB_call@PLT
	testl	%eax, %eax
	je	.L195
	movq	8(%rbx), %rdi
	cmpq	%r15, %rdi
	je	.L243
	call	BN_free@PLT
	movq	%r15, %rdi
	call	BN_dup@PLT
	movq	%rax, 8(%rbx)
.L243:
	movq	16(%rbx), %rdi
	cmpq	%r12, %rdi
	je	.L239
	call	BN_free@PLT
	movq	%r12, %rdi
	call	BN_dup@PLT
	movq	%rax, 16(%rbx)
.L239:
	movq	24(%rbx), %rdi
	call	BN_free@PLT
	movq	%r13, %rdi
	call	BN_dup@PLT
	cmpq	$0, 8(%rbx)
	movq	%rax, 24(%rbx)
	je	.L294
	cmpq	$0, 16(%rbx)
	je	.L294
	testq	%rax, %rax
	je	.L294
	movq	-224(%rbp), %rax
	testq	%rax, %rax
	je	.L240
	movl	-200(%rbp), %esi
	movl	%esi, (%rax)
.L240:
	movq	-232(%rbp), %rax
	movl	$1, %r14d
	testq	%rax, %rax
	je	.L195
	movl	-208(%rbp), %esi
	movq	%rsi, (%rax)
	jmp	.L195
.L350:
	movl	$436, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$113, %edx
	movl	%eax, %r14d
	movl	$126, %esi
	movl	$10, %edi
	call	ERR_put_error@PLT
	jmp	.L195
.L353:
	cmpq	$0, -208(%rbp)
	movq	%r13, %r15
	movq	%r14, %r13
	je	.L214
	movl	$525, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$112, %edx
	xorl	%r14d, %r14d
	movl	$126, %esi
	movl	$10, %edi
	call	ERR_put_error@PLT
	jmp	.L195
.L347:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE423:
	.size	dsa_builtin_paramgen2, .-dsa_builtin_paramgen2
	.section	.rodata
	.type	ggen.11416, @object
	.size	ggen.11416, 4
ggen.11416:
	.ascii	"ggen"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
