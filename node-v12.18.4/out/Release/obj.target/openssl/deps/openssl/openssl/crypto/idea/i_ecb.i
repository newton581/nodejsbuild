# 1 "../deps/openssl/openssl/crypto/idea/i_ecb.c"
# 1 "<built-in>"
# 1 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 1 "<command-line>" 2
# 1 "../deps/openssl/openssl/crypto/idea/i_ecb.c"
# 10 "../deps/openssl/openssl/crypto/idea/i_ecb.c"
# 1 "../deps/openssl/openssl/include/openssl/idea.h" 1
# 13 "../deps/openssl/openssl/include/openssl/idea.h"
# 1 "../deps/openssl/openssl/include/openssl/opensslconf.h" 1
# 1 "../deps/openssl/openssl/include/../../config/opensslconf.h" 1
# 9 "../deps/openssl/openssl/include/../../config/opensslconf.h"
# 1 "../deps/openssl/openssl/include/../../config/./opensslconf_asm.h" 1
# 98 "../deps/openssl/openssl/include/../../config/./opensslconf_asm.h"
# 1 "../deps/openssl/openssl/include/../../config/././archs/linux-x86_64/asm/include/openssl/opensslconf.h" 1
# 13 "../deps/openssl/openssl/include/../../config/././archs/linux-x86_64/asm/include/openssl/opensslconf.h"
# 1 "../deps/openssl/openssl/include/openssl/opensslv.h" 1
# 14 "../deps/openssl/openssl/include/../../config/././archs/linux-x86_64/asm/include/openssl/opensslconf.h" 2
# 99 "../deps/openssl/openssl/include/../../config/./opensslconf_asm.h" 2
# 10 "../deps/openssl/openssl/include/../../config/opensslconf.h" 2
# 1 "../deps/openssl/openssl/include/openssl/opensslconf.h" 2
# 14 "../deps/openssl/openssl/include/openssl/idea.h" 2






typedef unsigned int IDEA_INT;







typedef struct idea_key_st {
    IDEA_INT data[9][6];
} IDEA_KEY_SCHEDULE;

const char *IDEA_options(void);
void IDEA_ecb_encrypt(const unsigned char *in, unsigned char *out,
                      IDEA_KEY_SCHEDULE *ks);
void IDEA_set_encrypt_key(const unsigned char *key, IDEA_KEY_SCHEDULE *ks);
void IDEA_set_decrypt_key(IDEA_KEY_SCHEDULE *ek, IDEA_KEY_SCHEDULE *dk);
void IDEA_cbc_encrypt(const unsigned char *in, unsigned char *out,
                      long length, IDEA_KEY_SCHEDULE *ks, unsigned char *iv,
                      int enc);
void IDEA_cfb64_encrypt(const unsigned char *in, unsigned char *out,
                        long length, IDEA_KEY_SCHEDULE *ks, unsigned char *iv,
                        int *num, int enc);
void IDEA_ofb64_encrypt(const unsigned char *in, unsigned char *out,
                        long length, IDEA_KEY_SCHEDULE *ks, unsigned char *iv,
                        int *num);
void IDEA_encrypt(unsigned long *in, IDEA_KEY_SCHEDULE *ks);
# 11 "../deps/openssl/openssl/crypto/idea/i_ecb.c" 2
# 1 "../deps/openssl/openssl/crypto/idea/idea_local.h" 1
# 12 "../deps/openssl/openssl/crypto/idea/i_ecb.c" 2


const char *IDEA_options(void)
{
    return "idea(int)";
}

void IDEA_ecb_encrypt(const unsigned char *in, unsigned char *out,
                      IDEA_KEY_SCHEDULE *ks)
{
    unsigned long l0, l1, d[2];

    (l0 =((unsigned long)(*((in)++)))<<24L, l0|=((unsigned long)(*((in)++)))<<16L, l0|=((unsigned long)(*((in)++)))<< 8L, l0|=((unsigned long)(*((in)++))));
    d[0] = l0;
    (l1 =((unsigned long)(*((in)++)))<<24L, l1|=((unsigned long)(*((in)++)))<<16L, l1|=((unsigned long)(*((in)++)))<< 8L, l1|=((unsigned long)(*((in)++))));
    d[1] = l1;
    IDEA_encrypt(d, ks);
    l0 = d[0];
    (*((out)++)=(unsigned char)(((l0)>>24L)&0xff), *((out)++)=(unsigned char)(((l0)>>16L)&0xff), *((out)++)=(unsigned char)(((l0)>> 8L)&0xff), *((out)++)=(unsigned char)(((l0) )&0xff));
    l1 = d[1];
    (*((out)++)=(unsigned char)(((l1)>>24L)&0xff), *((out)++)=(unsigned char)(((l1)>>16L)&0xff), *((out)++)=(unsigned char)(((l1)>> 8L)&0xff), *((out)++)=(unsigned char)(((l1) )&0xff));
    l0 = l1 = d[0] = d[1] = 0;
}
