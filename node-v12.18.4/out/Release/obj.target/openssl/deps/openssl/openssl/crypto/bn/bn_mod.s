	.file	"bn_mod.c"
	.text
	.p2align 4
	.globl	BN_nnmod
	.type	BN_nnmod, @function
BN_nnmod:
.LFB252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r8
	movq	%rdx, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	movq	%rsi, %rdx
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %rsi
	movq	%rdi, %r12
	xorl	%edi, %edi
	call	BN_div@PLT
	testl	%eax, %eax
	je	.L1
	movl	16(%r12), %edx
	testl	%edx, %edx
	je	.L4
	movl	16(%r13), %eax
	testl	%eax, %eax
	jne	.L5
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r12, %rdi
	movq	BN_add@GOTPCREL(%rip), %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	movl	$1, %eax
.L1:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r12, %rdi
	movq	BN_sub@GOTPCREL(%rip), %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE252:
	.size	BN_nnmod, .-BN_nnmod
	.p2align 4
	.globl	BN_mod_add
	.type	BN_mod_add, @function
BN_mod_add:
.LFB253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%r8, %rbx
	subq	$8, %rsp
	call	BN_add@PLT
	testl	%eax, %eax
	jne	.L12
.L14:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	xorl	%edi, %edi
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%r12, %rsi
	call	BN_div@PLT
	testl	%eax, %eax
	je	.L14
	movl	16(%r12), %edx
	testl	%edx, %edx
	je	.L16
	movl	16(%r13), %eax
	testl	%eax, %eax
	jne	.L17
	movq	BN_add@GOTPCREL(%rip), %rax
.L15:
	addq	$8, %rsp
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movq	BN_sub@GOTPCREL(%rip), %rax
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L16:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE253:
	.size	BN_mod_add, .-BN_mod_add
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/bn/bn_mod.c"
	.text
	.p2align 4
	.globl	bn_mod_add_fixed_top
	.type	bn_mod_add_fixed_top, @function
bn_mod_add_fixed_top:
.LFB254:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$216, %rsp
	movq	%rdi, -208(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movslq	8(%rcx), %rax
	movq	%rax, %rsi
	movl	%eax, -212(%rbp)
	movq	%rax, -200(%rbp)
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L26
	leaq	-192(%rbp), %rax
	cmpq	$16, -200(%rbp)
	movq	%rax, -224(%rbp)
	movq	%rax, %r12
	ja	.L50
.L25:
	movq	(%r14), %rax
	movq	0(%r13), %r8
	movq	(%rbx), %r9
	movq	-200(%rbp), %rcx
	movq	%rax, -232(%rbp)
	movq	-208(%rbp), %rax
	testq	%r8, %r8
	cmove	%r12, %r8
	testq	%r9, %r9
	movq	(%rax), %rax
	cmove	%r12, %r9
	movq	%rax, -240(%rbp)
	testq	%rcx, %rcx
	je	.L29
	movslq	8(%r13), %rdi
	movslq	12(%rbx), %rsi
	movslq	8(%rbx), %rax
	movslq	12(%r13), %r15
	xorl	%ebx, %ebx
	subq	%rdi, %rcx
	movq	%rsi, -248(%rbp)
	movq	%rdi, %rdx
	movq	%rdi, %r11
	leaq	1(%rdi), %r10
	leaq	(%r12,%rdi,8), %r13
	movq	%rcx, %r14
	negq	%rdx
	subq	-248(%rbp), %rdi
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	subq	%rax, %r11
	subq	%r15, %r10
	addq	$1, %rdi
	.p2align 4,,10
	.p2align 3
.L34:
	movq	(%r8,%rcx,8), %rax
	movq	%rdx, %r15
	sarq	$63, %r15
	andq	%r15, %rax
	addq	%rbx, %rax
	movq	%rax, %r15
	leaq	(%r11,%rdx), %rax
	setc	%bl
	sarq	$63, %rax
	andq	(%r9,%rsi,8), %rax
	movzbl	%bl, %ebx
	addq	%r15, %rax
	movq	%rax, 0(%r13,%rdx,8)
	leaq	(%r10,%rdx), %rax
	adcq	$0, %rbx
	shrq	$63, %rax
	addq	%rax, %rcx
	leaq	(%rdi,%rdx), %rax
	addq	$1, %rdx
	shrq	$63, %rax
	addq	%rax, %rsi
	cmpq	%rdx, %r14
	jne	.L34
	movq	-240(%rbp), %r14
	movq	-232(%rbp), %rdx
	movq	%r12, %rsi
	movl	-212(%rbp), %ecx
	movq	%r14, %rdi
	call	bn_sub_words@PLT
	movq	%r12, %rdx
	movq	%r14, %rdi
	subq	%rax, %rbx
	movq	-200(%rbp), %rax
	leaq	(%r12,%rax,8), %rsi
	.p2align 4,,10
	.p2align 3
.L36:
	movq	(%rdi), %rcx
	movq	(%rdx), %rax
	addq	$8, %rdx
	addq	$8, %rdi
	xorq	%rcx, %rax
	andq	%rbx, %rax
	xorq	%rcx, %rax
	movq	%rax, -8(%rdi)
	movq	$0, -8(%rdx)
	cmpq	%rdx, %rsi
	jne	.L36
.L37:
	movq	-208(%rbp), %rax
	movl	-212(%rbp), %esi
	movl	$1, %r13d
	movl	%esi, 8(%rax)
	movl	$0, 16(%rax)
	cmpq	-224(%rbp), %r12
	je	.L22
	movl	$90, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
.L22:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L51
	addq	$216, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	movq	-200(%rbp), %rax
	movl	$60, %edx
	leaq	.LC0(%rip), %rsi
	leaq	0(,%rax,8), %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L25
.L26:
	xorl	%r13d, %r13d
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L29:
	movq	-232(%rbp), %rdx
	movq	%rax, %rdi
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	call	bn_sub_words@PLT
	jmp	.L37
.L51:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE254:
	.size	bn_mod_add_fixed_top, .-bn_mod_add_fixed_top
	.p2align 4
	.globl	BN_mod_add_quick
	.type	BN_mod_add_quick, @function
BN_mod_add_quick:
.LFB255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	subq	$24, %rsp
	call	bn_mod_add_fixed_top
	testl	%eax, %eax
	jne	.L58
	addq	$24, %rsp
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	movq	%r13, %rdi
	movl	%eax, -20(%rbp)
	call	bn_correct_top@PLT
	movl	-20(%rbp), %eax
	addq	$24, %rsp
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE255:
	.size	BN_mod_add_quick, .-BN_mod_add_quick
	.p2align 4
	.globl	BN_mod_sub
	.type	BN_mod_sub, @function
BN_mod_sub:
.LFB256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%r8, %rbx
	subq	$8, %rsp
	call	BN_sub@PLT
	testl	%eax, %eax
	jne	.L60
.L62:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	xorl	%edi, %edi
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%r12, %rsi
	call	BN_div@PLT
	testl	%eax, %eax
	je	.L62
	movl	16(%r12), %edx
	testl	%edx, %edx
	je	.L64
	movl	16(%r13), %eax
	testl	%eax, %eax
	jne	.L65
	movq	BN_add@GOTPCREL(%rip), %rax
.L63:
	addq	$8, %rsp
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	movq	BN_sub@GOTPCREL(%rip), %rax
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L64:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE256:
	.size	BN_mod_sub, .-BN_mod_sub
	.p2align 4
	.globl	bn_mod_sub_fixed_top
	.type	bn_mod_sub_fixed_top, @function
bn_mod_sub_fixed_top:
.LFB257:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movslq	8(%rcx), %r12
	movq	%rdi, -64(%rbp)
	movq	%rcx, -72(%rbp)
	movq	%r12, %rsi
	movl	%r12d, -52(%rbp)
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L90
	movq	(%rbx), %rcx
	movq	(%r15), %rbx
	movq	(%r14), %r13
	testq	%rbx, %rbx
	cmove	%rcx, %rbx
	testq	%r13, %r13
	cmove	%rcx, %r13
	testq	%r12, %r12
	je	.L89
	movslq	8(%r15), %r11
	movslq	8(%r14), %r8
	movq	%r12, %rdi
	movq	%rcx, -104(%rbp)
	movslq	12(%r14), %rdx
	movslq	12(%r15), %rsi
	xorl	%r10d, %r10d
	xorl	%r9d, %r9d
	movq	%r11, %rax
	leaq	1(%r11), %r14
	leaq	(%rcx,%r11,8), %r15
	subq	%r11, %rdi
	negq	%rax
	movq	%rdi, -80(%rbp)
	subq	%rsi, %r14
	xorl	%edi, %edi
	movq	%rax, -88(%rbp)
	movq	%r11, %rax
	subq	%rdx, %r11
	subq	%r8, %rax
	movq	-80(%rbp), %r8
	movq	%r12, -80(%rbp)
	addq	$1, %r11
	movq	%rax, -96(%rbp)
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L76:
	movq	%rax, %rcx
	leaq	(%r12,%rax), %rdx
	sarq	$63, %rcx
	andq	(%rbx,%r9,8), %rcx
	sarq	$63, %rdx
	andq	0(%r13,%r10,8), %rdx
	movq	%rcx, %rsi
	subq	%rdi, %rsi
	subq	%rdx, %rsi
	movq	%rsi, (%r15,%rax,8)
	xorl	%esi, %esi
	cmpq	%rcx, %rdx
	leaq	(%r14,%rax), %rdx
	seta	%sil
	cmovne	%rsi, %rdi
	shrq	$63, %rdx
	addq	%rdx, %r9
	leaq	(%r11,%rax), %rdx
	addq	$1, %rax
	shrq	$63, %rdx
	addq	%rdx, %r10
	cmpq	%rax, %r8
	jne	.L76
	movq	-72(%rbp), %rax
	movq	%rdi, %r10
	movq	-80(%rbp), %r12
	xorl	%esi, %esi
	movq	-104(%rbp), %rcx
	negq	%r10
	movq	(%rax), %r8
	xorl	%eax, %eax
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L82:
	movq	(%r8,%rsi,8), %rax
	movq	%rsi, %r9
	andq	%r10, %rax
	addq	%rdx, %rax
	setc	%dl
	addq	(%rcx,%rsi,8), %rax
	movq	%rax, (%rcx,%rsi,8)
	movzbl	%dl, %edx
	leaq	1(%rsi), %rsi
	adcq	$0, %rdx
	cmpq	%rsi, %r12
	jne	.L82
	movq	%rdx, %rax
	xorl	%edx, %edx
	subq	%rdi, %rax
	movq	%rdx, %rsi
	xorl	%edi, %edi
	.p2align 4,,10
	.p2align 3
.L88:
	movq	(%r8,%rdi,8), %rdx
	andq	%rax, %rdx
	addq	%rsi, %rdx
	setc	%sil
	addq	(%rcx,%rdi,8), %rdx
	movq	%rdx, (%rcx,%rdi,8)
	movzbl	%sil, %esi
	movq	%rdi, %rdx
	adcq	$0, %rsi
	addq	$1, %rdi
	cmpq	%rdx, %r9
	jne	.L88
.L89:
	movq	-64(%rbp), %rax
	movl	-52(%rbp), %ebx
	movl	$0, 16(%rax)
	movl	%ebx, 8(%rax)
	addq	$72, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	addq	$72, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE257:
	.size	bn_mod_sub_fixed_top, .-bn_mod_sub_fixed_top
	.p2align 4
	.globl	BN_mod_sub_quick
	.type	BN_mod_sub_quick, @function
BN_mod_sub_quick:
.LFB258:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	BN_sub@PLT
	testl	%eax, %eax
	je	.L99
	movl	16(%r12), %edx
	movl	$1, %eax
	testl	%edx, %edx
	jne	.L106
.L99:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	BN_add@PLT
	.cfi_endproc
.LFE258:
	.size	BN_mod_sub_quick, .-BN_mod_sub_quick
	.p2align 4
	.globl	BN_mod_mul
	.type	BN_mod_mul, @function
BN_mod_mul:
.LFB259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%r8, %rdi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	%rsi, -56(%rbp)
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	-56(%rbp), %rsi
	testq	%rax, %rax
	je	.L111
	movq	%rax, %r13
	cmpq	%rbx, %rsi
	je	.L128
	movq	%r12, %rcx
	movq	%rbx, %rdx
	movq	%rax, %rdi
	call	BN_mul@PLT
	testl	%eax, %eax
	je	.L111
.L113:
	xorl	%edi, %edi
	movq	%r12, %r8
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	BN_div@PLT
	testl	%eax, %eax
	je	.L111
	movl	16(%r14), %edx
	movl	$1, %r13d
	testl	%edx, %edx
	je	.L109
	movl	16(%r15), %eax
	testl	%eax, %eax
	jne	.L116
	movq	BN_add@GOTPCREL(%rip), %rax
.L114:
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	xorl	%r13d, %r13d
	call	*%rax
	testl	%eax, %eax
	setne	%r13b
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L128:
	movq	%r12, %rdx
	movq	%rax, %rdi
	call	BN_sqr@PLT
	testl	%eax, %eax
	jne	.L113
	.p2align 4,,10
	.p2align 3
.L111:
	xorl	%r13d, %r13d
.L109:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
	movq	BN_sub@GOTPCREL(%rip), %rax
	jmp	.L114
	.cfi_endproc
.LFE259:
	.size	BN_mod_mul, .-BN_mod_mul
	.p2align 4
	.globl	BN_mod_sqr
	.type	BN_mod_sqr, @function
BN_mod_sqr:
.LFB260:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	movq	%rcx, %rdx
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rcx, %rbx
	subq	$8, %rsp
	call	BN_sqr@PLT
	testl	%eax, %eax
	jne	.L132
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	%r12, %rdx
	popq	%rbx
	movq	%r12, %rsi
	xorl	%edi, %edi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	BN_div@PLT
	.cfi_endproc
.LFE260:
	.size	BN_mod_sqr, .-BN_mod_sqr
	.p2align 4
	.globl	BN_mod_lshift1
	.type	BN_mod_lshift1, @function
BN_mod_lshift1:
.LFB261:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rcx, %rbx
	subq	$8, %rsp
	call	BN_lshift1@PLT
	testl	%eax, %eax
	jne	.L134
.L136:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	xorl	%edi, %edi
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%r12, %rsi
	call	BN_div@PLT
	testl	%eax, %eax
	je	.L136
	movl	16(%r12), %edx
	testl	%edx, %edx
	je	.L138
	movl	16(%r13), %eax
	testl	%eax, %eax
	jne	.L139
	movq	BN_add@GOTPCREL(%rip), %rax
.L137:
	addq	$8, %rsp
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	movq	BN_sub@GOTPCREL(%rip), %rax
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L138:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE261:
	.size	BN_mod_lshift1, .-BN_mod_lshift1
	.p2align 4
	.globl	BN_mod_lshift1_quick
	.type	BN_mod_lshift1_quick, @function
BN_mod_lshift1_quick:
.LFB262:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	BN_lshift1@PLT
	testl	%eax, %eax
	jne	.L151
.L144:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	BN_cmp@PLT
	movl	%eax, %r8d
	movl	$1, %eax
	testl	%r8d, %r8d
	js	.L144
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	BN_sub@PLT
	.cfi_endproc
.LFE262:
	.size	BN_mod_lshift1_quick, .-BN_mod_lshift1_quick
	.p2align 4
	.globl	BN_mod_lshift_quick
	.type	BN_mod_lshift_quick, @function
BN_mod_lshift_quick:
.LFB264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpq	%rsi, %rdi
	je	.L163
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L156
	.p2align 4,,10
	.p2align 3
.L163:
	testl	%r12d, %r12d
	jle	.L177
	movq	%r14, %rdi
	call	BN_num_bits@PLT
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	BN_num_bits@PLT
	subl	%eax, %ebx
	js	.L178
	cmpl	%r12d, %ebx
	cmovg	%r12d, %ebx
	testl	%ebx, %ebx
	je	.L160
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	BN_lshift@PLT
	testl	%eax, %eax
	je	.L156
	subl	%ebx, %r12d
.L161:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	js	.L163
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	BN_sub@PLT
	testl	%eax, %eax
	jne	.L163
.L156:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L160:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	BN_lshift1@PLT
	testl	%eax, %eax
	je	.L156
	subl	$1, %r12d
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L177:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	movl	$294, %r8d
	movl	$110, %edx
	movl	$119, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE264:
	.size	BN_mod_lshift_quick, .-BN_mod_lshift_quick
	.p2align 4
	.globl	BN_mod_lshift
	.type	BN_mod_lshift, @function
BN_mod_lshift:
.LFB263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	xorl	%edi, %edi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	call	BN_div@PLT
	testl	%eax, %eax
	je	.L184
	movl	16(%r13), %edx
	movl	16(%r12), %eax
	testl	%edx, %edx
	je	.L182
	testl	%eax, %eax
	je	.L186
	movq	BN_sub@GOTPCREL(%rip), %rax
.L183:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L184
	movl	16(%r12), %eax
.L182:
	testl	%eax, %eax
	je	.L187
	movq	%r12, %rdi
	call	BN_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L184
	movl	$0, 16(%rax)
	movq	%rax, %r15
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L184:
	xorl	%r12d, %r12d
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L187:
	.cfi_restore_state
	xorl	%r15d, %r15d
.L185:
	movq	%r12, %rcx
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	BN_mod_lshift_quick
	movq	%r15, %rdi
	movl	%eax, %r12d
	call	BN_free@PLT
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L186:
	.cfi_restore_state
	movq	BN_add@GOTPCREL(%rip), %rax
	jmp	.L183
	.cfi_endproc
.LFE263:
	.size	BN_mod_lshift, .-BN_mod_lshift
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
