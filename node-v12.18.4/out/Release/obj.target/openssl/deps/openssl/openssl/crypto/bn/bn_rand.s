	.file	"bn_rand.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/bn/bn_rand.c"
	.text
	.p2align 4
	.type	bnrand.part.0, @function
bnrand.part.0:
.LFB262:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leal	7(%rdx), %r12d
	pushq	%rbx
	sarl	$3, %r12d
	movslq	%r12d, %r13
	movq	%r13, %rdi
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -80(%rbp)
	leaq	.LC0(%rip), %rsi
	movl	%edx, -84(%rbp)
	movl	$39, %edx
	movl	%ecx, -88(%rbp)
	movl	%r8d, -92(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	CRYPTO_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L29
	movl	%r12d, %esi
	movq	%rax, %rdi
	testl	%r15d, %r15d
	je	.L30
	call	RAND_priv_bytes@PLT
	testl	%eax, %eax
	jle	.L27
	cmpl	$1, %r15d
	jne	.L6
	leal	-1(%r12), %eax
	xorl	%ebx, %ebx
	leaq	-57(%rbp), %r15
	movq	%rax, -72(%rbp)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L9:
	cmpb	$41, %al
	ja	.L11
	movb	$0, (%r14,%rbx)
.L10:
	leaq	1(%rbx), %rax
	cmpq	%rbx, -72(%rbp)
	je	.L6
	movq	%rax, %rbx
.L12:
	movl	$1, %esi
	movq	%r15, %rdi
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jle	.L27
	movzbl	-57(%rbp), %eax
	testb	%al, %al
	jns	.L9
	testq	%rbx, %rbx
	je	.L10
	movzbl	-1(%r14,%rbx), %eax
	movb	%al, (%r14,%rbx)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L30:
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jle	.L27
.L6:
	movl	-84(%rbp), %ebx
	movl	-88(%rbp), %edx
	subl	$1, %ebx
	andl	$7, %ebx
	testl	%edx, %edx
	js	.L31
	je	.L15
	testl	%ebx, %ebx
	jne	.L16
	orb	$-128, 1(%r14)
	movl	$1, %edx
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L11:
	cmpb	$83, %al
	ja	.L10
	movb	$-1, (%r14,%rbx)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L27:
	xorl	%r12d, %r12d
.L3:
	movl	$88, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	CRYPTO_clear_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L32
	addq	$56, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	movzbl	(%r14), %edx
.L14:
	leal	1(%rbx), %ecx
	movl	$255, %eax
	sall	%cl, %eax
	notl	%eax
	andl	%edx, %eax
	movb	%al, (%r14)
	movl	-92(%rbp), %eax
	testl	%eax, %eax
	je	.L17
	orb	$1, -1(%r14,%r13)
.L17:
	movq	-80(%rbp), %rdx
	movl	%r12d, %esi
	movq	%r14, %rdi
	xorl	%r12d, %r12d
	call	BN_bin2bn@PLT
	testq	%rax, %rax
	setne	%r12b
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L15:
	movzbl	(%r14), %edx
	movl	$1, %eax
	movl	%ebx, %ecx
	sall	%cl, %eax
	orl	%eax, %edx
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L29:
	movl	$41, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$127, %esi
	movl	$3, %edi
	call	ERR_put_error@PLT
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L16:
	movzbl	(%r14), %edx
	leal	-1(%rbx), %ecx
	movl	$3, %eax
	sall	%cl, %eax
	orl	%eax, %edx
	jmp	.L14
.L32:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE262:
	.size	bnrand.part.0, .-bnrand.part.0
	.p2align 4
	.type	bnrand_range.part.0, @function
bnrand_range.part.0:
.LFB263:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edi, %r14d
	movq	%rdx, %rdi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	BN_num_bits@PLT
	movl	%eax, %r13d
	cmpl	$1, %eax
	je	.L74
	leal	-2(%rax), %esi
	movq	%rbx, %rdi
	call	BN_is_bit_set@PLT
	testl	%eax, %eax
	je	.L36
.L43:
	testl	%r13d, %r13d
	je	.L53
	js	.L46
	movl	$100, %r15d
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L75:
	subl	$1, %r15d
	je	.L40
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	js	.L42
.L41:
	xorl	%r8d, %r8d
	movl	$-1, %ecx
	movl	%r13d, %edx
	movq	%r12, %rsi
	movl	%r14d, %edi
	call	bnrand.part.0
	testl	%eax, %eax
	jne	.L75
.L72:
	xorl	%r13d, %r13d
.L33:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movl	$100, %r13d
	.p2align 4,,10
	.p2align 3
.L37:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	BN_set_word@PLT
	subl	$1, %r13d
	je	.L40
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	jns	.L37
.L42:
	movl	$1, %r13d
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L36:
	leal	-3(%r13), %esi
	movq	%rbx, %rdi
	call	BN_is_bit_set@PLT
	testl	%eax, %eax
	jne	.L43
	addl	$1, %r13d
	movl	$100, %r15d
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L77:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	BN_set_word@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	jns	.L47
	.p2align 4,,10
	.p2align 3
.L50:
	subl	$1, %r15d
	je	.L76
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	js	.L42
.L51:
	testl	%r13d, %r13d
	je	.L77
	js	.L46
	xorl	%r8d, %r8d
	movl	$-1, %ecx
	movl	%r13d, %edx
	movq	%r12, %rsi
	movl	%r14d, %edi
	call	bnrand.part.0
	testl	%eax, %eax
	je	.L72
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	js	.L50
.L47:
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	BN_sub@PLT
	testl	%eax, %eax
	je	.L72
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	js	.L50
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	BN_sub@PLT
	testl	%eax, %eax
	jne	.L50
	jmp	.L72
.L38:
	.p2align 4,,10
	.p2align 3
.L46:
	movl	$93, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$118, %edx
	xorl	%r13d, %r13d
	movl	$127, %esi
	movl	$3, %edi
	call	ERR_put_error@PLT
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L74:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	BN_set_word@PLT
	jmp	.L33
.L40:
	movl	$166, %r8d
.L71:
	movl	$113, %edx
	movl	$138, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L72
.L76:
	movl	$153, %r8d
	jmp	.L71
	.cfi_endproc
.LFE263:
	.size	bnrand_range.part.0, .-bnrand_range.part.0
	.p2align 4
	.globl	BN_rand
	.type	BN_rand, @function
BN_rand:
.LFB253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testl	%esi, %esi
	jne	.L79
	cmpl	$-1, %edx
	jne	.L80
	testl	%ecx, %ecx
	je	.L99
.L80:
	movl	$93, %r8d
	movl	$118, %edx
	movl	$127, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	js	.L80
	movl	%esi, %ebx
	cmpl	$1, %esi
	jne	.L91
	testl	%edx, %edx
	jg	.L80
.L91:
	leal	7(%rbx), %r8d
	movl	$39, %edx
	leaq	.LC0(%rip), %rsi
	movl	%r9d, -56(%rbp)
	sarl	$3, %r8d
	movslq	%r8d, %r12
	movl	%r8d, -52(%rbp)
	movq	%r12, %rdi
	call	CRYPTO_malloc@PLT
	movl	-52(%rbp), %r8d
	movl	-56(%rbp), %r9d
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L100
	movl	%r8d, %esi
	movq	%rax, %rdi
	movl	%r9d, -56(%rbp)
	movl	%r8d, -52(%rbp)
	call	RAND_bytes@PLT
	movl	%eax, %r10d
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jle	.L84
	movl	-56(%rbp), %r9d
	subl	$1, %ebx
	movl	-52(%rbp), %r8d
	andl	$7, %ebx
	testl	%r9d, %r9d
	js	.L101
	jne	.L102
	movzbl	(%r15), %edx
	movl	$1, %eax
	movl	%ebx, %ecx
	sall	%cl, %eax
	orl	%eax, %edx
.L86:
	leal	1(%rbx), %ecx
	movl	$255, %eax
	sall	%cl, %eax
	notl	%eax
	andl	%edx, %eax
	movb	%al, (%r15)
	testl	%r13d, %r13d
	je	.L89
	orb	$1, -1(%r15,%r12)
.L89:
	movq	%r14, %rdx
	movl	%r8d, %esi
	movq	%r15, %rdi
	call	BN_bin2bn@PLT
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
.L84:
	movq	%r12, %rsi
	movq	%r15, %rdi
	movl	$88, %ecx
	movl	%eax, -52(%rbp)
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movl	-52(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore_state
	xorl	%esi, %esi
	call	BN_set_word@PLT
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	testl	%ebx, %ebx
	jne	.L88
	orb	$-128, 1(%r15)
	movl	$1, %edx
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L101:
	movzbl	(%r15), %edx
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L100:
	movl	$41, %r8d
	movl	$65, %edx
	movl	$127, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L88:
	movzbl	(%r15), %edx
	leal	-1(%rbx), %ecx
	movl	$3, %eax
	sall	%cl, %eax
	orl	%eax, %edx
	jmp	.L86
	.cfi_endproc
.LFE253:
	.size	BN_rand, .-BN_rand
	.p2align 4
	.globl	BN_bntest_rand
	.type	BN_bntest_rand, @function
BN_bntest_rand:
.LFB254:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%esi, %esi
	jne	.L104
	cmpl	$-1, %edx
	jne	.L105
	testl	%ecx, %ecx
	je	.L113
.L105:
	movl	$93, %r8d
	movl	$118, %edx
	movl	$127, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	js	.L105
	cmpl	$1, %esi
	jne	.L108
	testl	%edx, %edx
	jg	.L105
.L108:
	movl	%ecx, %r8d
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	%edx, %ecx
	movl	%esi, %edx
	movq	%rdi, %rsi
	movl	$1, %edi
	jmp	bnrand.part.0
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore_state
	xorl	%esi, %esi
	call	BN_set_word@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE254:
	.size	BN_bntest_rand, .-BN_bntest_rand
	.p2align 4
	.globl	BN_priv_rand
	.type	BN_priv_rand, @function
BN_priv_rand:
.LFB255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testl	%esi, %esi
	jne	.L115
	cmpl	$-1, %edx
	jne	.L116
	testl	%ecx, %ecx
	je	.L135
.L116:
	movl	$93, %r8d
	movl	$118, %edx
	movl	$127, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	js	.L116
	movl	%esi, %ebx
	cmpl	$1, %esi
	jne	.L127
	testl	%edx, %edx
	jg	.L116
.L127:
	leal	7(%rbx), %r8d
	movl	$39, %edx
	leaq	.LC0(%rip), %rsi
	movl	%r9d, -56(%rbp)
	sarl	$3, %r8d
	movslq	%r8d, %r12
	movl	%r8d, -52(%rbp)
	movq	%r12, %rdi
	call	CRYPTO_malloc@PLT
	movl	-52(%rbp), %r8d
	movl	-56(%rbp), %r9d
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L136
	movl	%r8d, %esi
	movq	%rax, %rdi
	movl	%r9d, -56(%rbp)
	movl	%r8d, -52(%rbp)
	call	RAND_priv_bytes@PLT
	movl	%eax, %r10d
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jle	.L120
	movl	-56(%rbp), %r9d
	subl	$1, %ebx
	movl	-52(%rbp), %r8d
	andl	$7, %ebx
	testl	%r9d, %r9d
	js	.L137
	jne	.L138
	movzbl	(%r15), %edx
	movl	$1, %eax
	movl	%ebx, %ecx
	sall	%cl, %eax
	orl	%eax, %edx
.L122:
	leal	1(%rbx), %ecx
	movl	$255, %eax
	sall	%cl, %eax
	notl	%eax
	andl	%edx, %eax
	movb	%al, (%r15)
	testl	%r13d, %r13d
	je	.L125
	orb	$1, -1(%r15,%r12)
.L125:
	movq	%r14, %rdx
	movl	%r8d, %esi
	movq	%r15, %rdi
	call	BN_bin2bn@PLT
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
.L120:
	movq	%r12, %rsi
	movq	%r15, %rdi
	movl	$88, %ecx
	movl	%eax, -52(%rbp)
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movl	-52(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	.cfi_restore_state
	xorl	%esi, %esi
	call	BN_set_word@PLT
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	testl	%ebx, %ebx
	jne	.L124
	orb	$-128, 1(%r15)
	movl	$1, %edx
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L137:
	movzbl	(%r15), %edx
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L136:
	movl	$41, %r8d
	movl	$65, %edx
	movl	$127, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L124:
	movzbl	(%r15), %edx
	leal	-1(%rbx), %ecx
	movl	$3, %eax
	sall	%cl, %eax
	orl	%eax, %edx
	jmp	.L122
	.cfi_endproc
.LFE255:
	.size	BN_priv_rand, .-BN_priv_rand
	.p2align 4
	.globl	BN_rand_range
	.type	BN_rand_range, @function
BN_rand_range:
.LFB257:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	16(%rsi), %eax
	testl	%eax, %eax
	jne	.L142
	movq	%rdi, %r13
	movq	%rsi, %rdi
	movq	%rsi, %r12
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L145
.L142:
	movl	$119, %r8d
	movl	$115, %edx
	movl	$138, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r13, %rsi
	popq	%r12
	xorl	%edi, %edi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	bnrand_range.part.0
	.cfi_endproc
.LFE257:
	.size	BN_rand_range, .-BN_rand_range
	.p2align 4
	.globl	BN_priv_rand_range
	.type	BN_priv_rand_range, @function
BN_priv_rand_range:
.LFB258:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	16(%rsi), %eax
	testl	%eax, %eax
	jne	.L149
	movq	%rdi, %r13
	movq	%rsi, %rdi
	movq	%rsi, %r12
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L152
.L149:
	movl	$119, %r8d
	movl	$115, %edx
	movl	$138, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r13, %rsi
	popq	%r12
	movl	$2, %edi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	bnrand_range.part.0
	.cfi_endproc
.LFE258:
	.size	BN_priv_rand_range, .-BN_priv_rand_range
	.p2align 4
	.globl	BN_pseudo_rand
	.type	BN_pseudo_rand, @function
BN_pseudo_rand:
.LFB265:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testl	%esi, %esi
	jne	.L154
	cmpl	$-1, %edx
	jne	.L155
	testl	%ecx, %ecx
	je	.L174
.L155:
	movl	$93, %r8d
	movl	$118, %edx
	movl	$127, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L154:
	.cfi_restore_state
	js	.L155
	movl	%esi, %ebx
	cmpl	$1, %esi
	jne	.L166
	testl	%edx, %edx
	jg	.L155
.L166:
	leal	7(%rbx), %r8d
	movl	$39, %edx
	leaq	.LC0(%rip), %rsi
	movl	%r9d, -56(%rbp)
	sarl	$3, %r8d
	movslq	%r8d, %r12
	movl	%r8d, -52(%rbp)
	movq	%r12, %rdi
	call	CRYPTO_malloc@PLT
	movl	-52(%rbp), %r8d
	movl	-56(%rbp), %r9d
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L175
	movl	%r8d, %esi
	movq	%rax, %rdi
	movl	%r9d, -56(%rbp)
	movl	%r8d, -52(%rbp)
	call	RAND_bytes@PLT
	movl	%eax, %r10d
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jle	.L159
	movl	-56(%rbp), %r9d
	subl	$1, %ebx
	movl	-52(%rbp), %r8d
	andl	$7, %ebx
	testl	%r9d, %r9d
	js	.L176
	jne	.L177
	movzbl	(%r15), %edx
	movl	$1, %eax
	movl	%ebx, %ecx
	sall	%cl, %eax
	orl	%eax, %edx
.L161:
	leal	1(%rbx), %ecx
	movl	$255, %eax
	sall	%cl, %eax
	notl	%eax
	andl	%edx, %eax
	movb	%al, (%r15)
	testl	%r13d, %r13d
	je	.L164
	orb	$1, -1(%r15,%r12)
.L164:
	movq	%r14, %rdx
	movl	%r8d, %esi
	movq	%r15, %rdi
	call	BN_bin2bn@PLT
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
.L159:
	movq	%r12, %rsi
	movq	%r15, %rdi
	movl	$88, %ecx
	movl	%eax, -52(%rbp)
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movl	-52(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L174:
	.cfi_restore_state
	xorl	%esi, %esi
	call	BN_set_word@PLT
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	testl	%ebx, %ebx
	jne	.L163
	orb	$-128, 1(%r15)
	movl	$1, %edx
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L176:
	movzbl	(%r15), %edx
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L175:
	movl	$41, %r8d
	movl	$65, %edx
	movl	$127, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L163:
	movzbl	(%r15), %edx
	leal	-1(%rbx), %ecx
	movl	$3, %eax
	sall	%cl, %eax
	orl	%eax, %edx
	jmp	.L161
	.cfi_endproc
.LFE265:
	.size	BN_pseudo_rand, .-BN_pseudo_rand
	.p2align 4
	.globl	BN_pseudo_rand_range
	.type	BN_pseudo_rand_range, @function
BN_pseudo_rand_range:
.LFB267:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	16(%rsi), %eax
	testl	%eax, %eax
	jne	.L181
	movq	%rdi, %r13
	movq	%rsi, %rdi
	movq	%rsi, %r12
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L184
.L181:
	movl	$119, %r8d
	movl	$115, %edx
	movl	$138, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r13, %rsi
	popq	%r12
	xorl	%edi, %edi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	bnrand_range.part.0
	.cfi_endproc
.LFE267:
	.size	BN_pseudo_rand_range, .-BN_pseudo_rand_range
	.p2align 4
	.globl	BN_generate_dsa_nonce
	.type	BN_generate_dsa_nonce, @function
BN_generate_dsa_nonce:
.LFB261:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	leaq	-160(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$536, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -560(%rbp)
	movq	%rsi, %rdi
	movq	%rsi, -568(%rbp)
	movq	%rcx, -552(%rbp)
	movq	%r9, -576(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	BN_num_bits@PLT
	movl	$223, %edx
	leaq	.LC0(%rip), %rsi
	leal	14(%rax), %r12d
	addl	$7, %eax
	cmovs	%r12d, %eax
	sarl	$3, %eax
	leal	8(%rax), %edi
	movq	%rdi, %r12
	call	CRYPTO_malloc@PLT
	movq	%rax, -544(%rbp)
	testq	%rax, %rax
	je	.L218
	movl	$96, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	BN_bn2binpad@PLT
	testl	%eax, %eax
	js	.L219
	movl	$0, -516(%rbp)
	testl	%r12d, %r12d
	je	.L189
	movl	%r12d, -532(%rbp)
	leaq	-288(%rbp), %r13
	leaq	-512(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L197:
	movl	$64, %esi
	movq	%r13, %rdi
	call	RAND_priv_bytes@PLT
	cmpl	$1, %eax
	jne	.L218
	movq	%rbx, %rdi
	leaq	-224(%rbp), %r12
	call	SHA512_Init@PLT
	leaq	-516(%rbp), %rsi
	movl	$4, %edx
	movq	%rbx, %rdi
	call	SHA512_Update@PLT
	movl	$96, %edx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	SHA512_Update@PLT
	movq	-552(%rbp), %rsi
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	SHA512_Update@PLT
	movl	$64, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	SHA512_Update@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	SHA512_Final@PLT
	movl	-516(%rbp), %esi
	movl	-532(%rbp), %edx
	movl	$64, %eax
	subl	%esi, %edx
	cmpl	$64, %edx
	cmova	%eax, %edx
	movl	%esi, %eax
	addq	-544(%rbp), %rax
	cmpl	$8, %edx
	jnb	.L191
	testb	$4, %dl
	jne	.L220
	testl	%edx, %edx
	je	.L192
	movzbl	(%r12), %esi
	movb	%sil, (%rax)
	movl	-516(%rbp), %esi
	testb	$2, %dl
	jne	.L221
.L192:
	addl	%esi, %edx
	movl	%edx, -516(%rbp)
	cmpl	-532(%rbp), %edx
	jb	.L197
	movl	-532(%rbp), %r12d
.L189:
	movq	-560(%rbp), %rbx
	movq	-544(%rbp), %rdi
	movl	%r12d, %esi
	movq	%rbx, %rdx
	call	BN_bin2bn@PLT
	testq	%rax, %rax
	je	.L218
	movq	-576(%rbp), %r8
	xorl	%edi, %edi
	movq	%rbx, %rdx
	movq	%rbx, %rsi
	movq	-568(%rbp), %rcx
	xorl	%r12d, %r12d
	call	BN_div@PLT
	cmpl	$1, %eax
	sete	%r12b
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L191:
	movq	-224(%rbp), %rsi
	movq	%r12, %rcx
	movq	%rsi, (%rax)
	movl	%edx, %esi
	movq	-8(%r12,%rsi), %rdi
	movq	%rdi, -8(%rax,%rsi)
	leaq	8(%rax), %rdi
	andq	$-8, %rdi
	subq	%rdi, %rax
	subq	%rax, %rcx
	addl	%edx, %eax
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L217
	andl	$-8, %eax
	xorl	%esi, %esi
.L195:
	movl	%esi, %r8d
	addl	$8, %esi
	movq	(%rcx,%r8), %r9
	movq	%r9, (%rdi,%r8)
	cmpl	%eax, %esi
	jb	.L195
.L217:
	movl	-516(%rbp), %esi
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L219:
	movl	$234, %r8d
	movl	$117, %edx
	movl	$140, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L218:
	xorl	%r12d, %r12d
.L187:
	movq	-544(%rbp), %rdi
	movl	$262, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$96, %esi
	movq	%r15, %rdi
	call	OPENSSL_cleanse@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L222
	addq	$536, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L220:
	.cfi_restore_state
	movl	(%r12), %esi
	movl	%esi, (%rax)
	movl	%edx, %esi
	movl	-4(%r12,%rsi), %ecx
	movl	%ecx, -4(%rax,%rsi)
	movl	-516(%rbp), %esi
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L221:
	movl	%edx, %esi
	movzwl	-2(%r12,%rsi), %ecx
	movw	%cx, -2(%rax,%rsi)
	movl	-516(%rbp), %esi
	jmp	.L192
.L222:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE261:
	.size	BN_generate_dsa_nonce, .-BN_generate_dsa_nonce
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
