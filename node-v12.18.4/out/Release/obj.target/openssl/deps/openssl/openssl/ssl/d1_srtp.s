	.file	"d1_srtp.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/ssl/d1_srtp.c"
	.text
	.p2align 4
	.type	ssl_ctx_make_profiles, @function
ssl_ctx_make_profiles:
.LFB965:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -64(%rbp)
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L25
	.p2align 4,,10
	.p2align 3
.L2:
	movl	$58, %esi
	movq	%r13, %rdi
	call	strchr@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L4
	subq	%r13, %rax
	movq	%rax, -56(%rbp)
	leaq	srtp_known_profiles(%rip), %rax
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.L6
.L28:
	movq	%rax, %r14
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L7:
	movq	16(%r14), %r12
	addq	$16, %r14
	testq	%r12, %r12
	je	.L6
.L9:
	movq	%r12, %rdi
	call	strlen@PLT
	cmpq	%rax, -56(%rbp)
	jne	.L7
	movq	-56(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	strncmp@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L7
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	OPENSSL_sk_find@PLT
	testl	%eax, %eax
	jns	.L26
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L27
	testq	%rbx, %rbx
	je	.L12
	leaq	1(%rbx), %r13
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L4:
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%rax, -56(%rbp)
	leaq	srtp_known_profiles(%rip), %rax
	movq	(%rax), %r12
	testq	%r12, %r12
	jne	.L28
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$93, %r8d
	movl	$364, %edx
	movl	$309, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L10:
	movq	%r15, %rdi
	movl	$1, %r12d
	call	OPENSSL_sk_free@PLT
.L1:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movl	$82, %r8d
	movl	$353, %edx
	movl	$309, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L27:
	movl	$88, %r8d
	movl	$362, %edx
	movl	$309, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L12:
	movq	-64(%rbp), %rbx
	movq	(%rbx), %rdi
	call	OPENSSL_sk_free@PLT
	movq	%r15, (%rbx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L25:
	movl	$71, %r8d
	movl	$362, %edx
	movl	$309, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	movl	$1, %r12d
	call	ERR_put_error@PLT
	jmp	.L1
	.cfi_endproc
.LFE965:
	.size	ssl_ctx_make_profiles, .-ssl_ctx_make_profiles
	.p2align 4
	.globl	SSL_CTX_set_tlsext_use_srtp
	.type	SSL_CTX_set_tlsext_use_srtp, @function
SSL_CTX_set_tlsext_use_srtp:
.LFB966:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	leaq	904(%r8), %rsi
	jmp	ssl_ctx_make_profiles
	.cfi_endproc
.LFE966:
	.size	SSL_CTX_set_tlsext_use_srtp, .-SSL_CTX_set_tlsext_use_srtp
	.p2align 4
	.globl	SSL_set_tlsext_use_srtp
	.type	SSL_set_tlsext_use_srtp, @function
SSL_set_tlsext_use_srtp:
.LFB967:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	leaq	1912(%r8), %rsi
	jmp	ssl_ctx_make_profiles
	.cfi_endproc
.LFE967:
	.size	SSL_set_tlsext_use_srtp, .-SSL_set_tlsext_use_srtp
	.p2align 4
	.globl	SSL_get_srtp_profiles
	.type	SSL_get_srtp_profiles, @function
SSL_get_srtp_profiles:
.LFB968:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L33
	movq	1912(%rdi), %rax
	testq	%rax, %rax
	je	.L37
.L31:
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	movq	1440(%rdi), %rax
	testq	%rax, %rax
	je	.L31
	movq	904(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE968:
	.size	SSL_get_srtp_profiles, .-SSL_get_srtp_profiles
	.p2align 4
	.globl	SSL_get_selected_srtp_profile
	.type	SSL_get_selected_srtp_profile, @function
SSL_get_selected_srtp_profile:
.LFB969:
	.cfi_startproc
	endbr64
	movq	1920(%rdi), %rax
	ret
	.cfi_endproc
.LFE969:
	.size	SSL_get_selected_srtp_profile, .-SSL_get_selected_srtp_profile
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"SRTP_AES128_CM_SHA1_80"
.LC2:
	.string	"SRTP_AES128_CM_SHA1_32"
.LC3:
	.string	"SRTP_AEAD_AES_128_GCM"
.LC4:
	.string	"SRTP_AEAD_AES_256_GCM"
	.section	.data.rel.local,"aw"
	.align 32
	.type	srtp_known_profiles, @object
	.size	srtp_known_profiles, 80
srtp_known_profiles:
	.quad	.LC1
	.quad	1
	.quad	.LC2
	.quad	2
	.quad	.LC3
	.quad	7
	.quad	.LC4
	.quad	8
	.quad	0
	.zero	8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
