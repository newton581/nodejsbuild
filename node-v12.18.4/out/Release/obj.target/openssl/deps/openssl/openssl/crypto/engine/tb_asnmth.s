	.file	"tb_asnmth.c"
	.text
	.p2align 4
	.type	engine_unregister_all_pkey_asn1_meths, @function
engine_unregister_all_pkey_asn1_meths:
.LFB902:
	.cfi_startproc
	endbr64
	leaq	pkey_asn1_meth_table(%rip), %rdi
	jmp	engine_table_cleanup@PLT
	.cfi_endproc
.LFE902:
	.size	engine_unregister_all_pkey_asn1_meths, .-engine_unregister_all_pkey_asn1_meths
	.p2align 4
	.type	look_str_cb, @function
look_str_cb:
.LFB912:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -80(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	cmpq	$0, 8(%rax)
	je	.L15
.L3:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L16
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	leaq	-64(%rbp), %rax
	movl	%edi, %r14d
	movq	%rsi, %r13
	xorl	%r12d, %r12d
	movq	%rax, -88(%rbp)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L6:
	addl	$1, %r12d
.L4:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L3
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movq	-88(%rbp), %rsi
	movl	%r14d, %ecx
	xorl	%edx, %edx
	movq	%rax, %rbx
	movq	%rax, %rdi
	call	*80(%rbx)
	movq	-64(%rbp), %r15
	testq	%r15, %r15
	je	.L6
	movq	16(%r15), %rdi
	movq	%rdi, -72(%rbp)
	call	strlen@PLT
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %rdi
	movslq	24(%rcx), %rdx
	cmpl	%eax, %edx
	jne	.L6
	movq	16(%rcx), %rsi
	call	strncasecmp@PLT
	testl	%eax, %eax
	jne	.L6
	movq	-80(%rbp), %rax
	movq	%rbx, %xmm0
	movq	%r15, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rax)
	jmp	.L3
.L16:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE912:
	.size	look_str_cb, .-look_str_cb
	.p2align 4
	.globl	ENGINE_unregister_pkey_asn1_meths
	.type	ENGINE_unregister_pkey_asn1_meths, @function
ENGINE_unregister_pkey_asn1_meths:
.LFB901:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	leaq	pkey_asn1_meth_table(%rip), %rdi
	jmp	engine_table_unregister@PLT
	.cfi_endproc
.LFE901:
	.size	ENGINE_unregister_pkey_asn1_meths, .-ENGINE_unregister_pkey_asn1_meths
	.p2align 4
	.globl	ENGINE_register_pkey_asn1_meths
	.type	ENGINE_register_pkey_asn1_meths, @function
ENGINE_register_pkey_asn1_meths:
.LFB903:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	$1, %r13d
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	80(%rdi), %rax
	testq	%rax, %rax
	je	.L18
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	leaq	-32(%rbp), %rdx
	movq	%rdi, %r12
	call	*%rax
	testl	%eax, %eax
	jg	.L25
.L18:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L26
	addq	$16, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	movq	-32(%rbp), %rcx
	xorl	%r9d, %r9d
	movl	%eax, %r8d
	movq	%r12, %rdx
	leaq	engine_unregister_all_pkey_asn1_meths(%rip), %rsi
	leaq	pkey_asn1_meth_table(%rip), %rdi
	call	engine_table_register@PLT
	movl	%eax, %r13d
	jmp	.L18
.L26:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE903:
	.size	ENGINE_register_pkey_asn1_meths, .-ENGINE_register_pkey_asn1_meths
	.p2align 4
	.globl	ENGINE_register_all_pkey_asn1_meths
	.type	ENGINE_register_all_pkey_asn1_meths, @function
ENGINE_register_all_pkey_asn1_meths:
.LFB904:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	ENGINE_get_first@PLT
	testq	%rax, %rax
	je	.L27
	movq	%rax, %r12
	leaq	-32(%rbp), %rbx
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L29:
	movq	%r12, %rdi
	call	ENGINE_get_next@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L27
.L31:
	movq	80(%r12), %rax
	testq	%rax, %rax
	je	.L29
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	jle	.L29
	movq	-32(%rbp), %rcx
	movq	%r12, %rdx
	xorl	%r9d, %r9d
	movl	%eax, %r8d
	leaq	engine_unregister_all_pkey_asn1_meths(%rip), %rsi
	leaq	pkey_asn1_meth_table(%rip), %rdi
	call	engine_table_register@PLT
	movq	%r12, %rdi
	call	ENGINE_get_next@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L31
	.p2align 4,,10
	.p2align 3
.L27:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L41
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L41:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE904:
	.size	ENGINE_register_all_pkey_asn1_meths, .-ENGINE_register_all_pkey_asn1_meths
	.p2align 4
	.globl	ENGINE_set_default_pkey_asn1_meths
	.type	ENGINE_set_default_pkey_asn1_meths, @function
ENGINE_set_default_pkey_asn1_meths:
.LFB905:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	$1, %r13d
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	80(%rdi), %rax
	testq	%rax, %rax
	je	.L42
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	leaq	-32(%rbp), %rdx
	movq	%rdi, %r12
	call	*%rax
	testl	%eax, %eax
	jg	.L49
.L42:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L50
	addq	$16, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	movq	-32(%rbp), %rcx
	movl	$1, %r9d
	movl	%eax, %r8d
	movq	%r12, %rdx
	leaq	engine_unregister_all_pkey_asn1_meths(%rip), %rsi
	leaq	pkey_asn1_meth_table(%rip), %rdi
	call	engine_table_register@PLT
	movl	%eax, %r13d
	jmp	.L42
.L50:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE905:
	.size	ENGINE_set_default_pkey_asn1_meths, .-ENGINE_set_default_pkey_asn1_meths
	.p2align 4
	.globl	ENGINE_get_pkey_asn1_meth_engine
	.type	ENGINE_get_pkey_asn1_meth_engine, @function
ENGINE_get_pkey_asn1_meth_engine:
.LFB906:
	.cfi_startproc
	endbr64
	movl	%edi, %esi
	leaq	pkey_asn1_meth_table(%rip), %rdi
	jmp	engine_table_select@PLT
	.cfi_endproc
.LFE906:
	.size	ENGINE_get_pkey_asn1_meth_engine, .-ENGINE_get_pkey_asn1_meth_engine
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/engine/tb_asnmth.c"
	.text
	.p2align 4
	.globl	ENGINE_get_pkey_asn1_meth
	.type	ENGINE_get_pkey_asn1_meth, @function
ENGINE_get_pkey_asn1_meth:
.LFB907:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	80(%rdi), %rax
	testq	%rax, %rax
	je	.L55
	movl	%esi, %ecx
	xorl	%edx, %edx
	leaq	-16(%rbp), %rsi
	call	*%rax
	testl	%eax, %eax
	je	.L55
	movq	-16(%rbp), %rax
.L52:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L61
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	movl	$88, %r8d
	movl	$101, %edx
	movl	$193, %esi
	movl	$38, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L52
.L61:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE907:
	.size	ENGINE_get_pkey_asn1_meth, .-ENGINE_get_pkey_asn1_meth
	.p2align 4
	.globl	ENGINE_get_pkey_asn1_meths
	.type	ENGINE_get_pkey_asn1_meths, @function
ENGINE_get_pkey_asn1_meths:
.LFB908:
	.cfi_startproc
	endbr64
	movq	80(%rdi), %rax
	ret
	.cfi_endproc
.LFE908:
	.size	ENGINE_get_pkey_asn1_meths, .-ENGINE_get_pkey_asn1_meths
	.p2align 4
	.globl	ENGINE_set_pkey_asn1_meths
	.type	ENGINE_set_pkey_asn1_meths, @function
ENGINE_set_pkey_asn1_meths:
.LFB909:
	.cfi_startproc
	endbr64
	movq	%rsi, 80(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE909:
	.size	ENGINE_set_pkey_asn1_meths, .-ENGINE_set_pkey_asn1_meths
	.p2align 4
	.globl	engine_pkey_asn1_meths_free
	.type	engine_pkey_asn1_meths_free, @function
engine_pkey_asn1_meths_free:
.LFB910:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	80(%rdi), %rax
	testq	%rax, %rax
	je	.L64
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	leaq	-48(%rbp), %rdx
	movq	%rdi, %r12
	call	*%rax
	testl	%eax, %eax
	jle	.L64
	subl	$1, %eax
	xorl	%ebx, %ebx
	leaq	-56(%rbp), %r13
	leaq	4(,%rax,4), %r14
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L67:
	addq	$4, %rbx
	cmpq	%rbx, %r14
	je	.L64
.L68:
	movq	-48(%rbp), %rax
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	(%rax,%rbx), %ecx
	call	*80(%r12)
	testl	%eax, %eax
	je	.L67
	movq	-56(%rbp), %rdi
	addq	$4, %rbx
	call	EVP_PKEY_asn1_free@PLT
	cmpq	%rbx, %r14
	jne	.L68
.L64:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L78
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L78:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE910:
	.size	engine_pkey_asn1_meths_free, .-engine_pkey_asn1_meths_free
	.p2align 4
	.globl	ENGINE_get_pkey_asn1_meth_str
	.type	ENGINE_get_pkey_asn1_meth_str, @function
ENGINE_get_pkey_asn1_meth_str:
.LFB911:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -104(%rbp)
	movq	80(%rdi), %rbx
	movl	%edx, -92(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L80
	movq	%rdi, %r12
	cmpl	$-1, %edx
	jne	.L81
	movq	%rsi, %rdi
	call	strlen@PLT
	movl	%eax, -92(%rbp)
.L81:
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	leaq	-72(%rbp), %rdx
	movq	%r12, %rdi
	call	*%rbx
	testl	%eax, %eax
	jle	.L80
	subl	$1, %eax
	xorl	%ebx, %ebx
	leaq	-64(%rbp), %r14
	leaq	4(,%rax,4), %rax
	movq	%rax, -88(%rbp)
	movslq	-92(%rbp), %rax
	movq	%rax, -112(%rbp)
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L82:
	addq	$4, %rbx
	cmpq	-88(%rbp), %rbx
	je	.L80
.L84:
	movq	-72(%rbp), %rax
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	(%rax,%rbx), %ecx
	call	*80(%r12)
	movq	-64(%rbp), %r13
	testq	%r13, %r13
	je	.L82
	movq	16(%r13), %r15
	movq	%r15, %rdi
	call	strlen@PLT
	cmpl	%eax, -92(%rbp)
	jne	.L82
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %rsi
	movq	%r15, %rdi
	call	strncasecmp@PLT
	testl	%eax, %eax
	jne	.L82
	.p2align 4,,10
	.p2align 3
.L79:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L97
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	xorl	%r13d, %r13d
	jmp	.L79
.L97:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE911:
	.size	ENGINE_get_pkey_asn1_meth_str, .-ENGINE_get_pkey_asn1_meth_str
	.p2align 4
	.globl	ENGINE_pkey_asn1_find_str
	.type	ENGINE_pkey_asn1_find_str, @function
ENGINE_pkey_asn1_find_str:
.LFB913:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	engine_lock_init(%rip), %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -48(%rbp)
	movq	do_engine_lock_init_ossl_@GOTPCREL(%rip), %rsi
	movl	%edx, -40(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L99
	movl	do_engine_lock_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	je	.L99
	movq	global_engine_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	pkey_asn1_meth_table(%rip), %rdi
	leaq	-64(%rbp), %rdx
	leaq	look_str_cb(%rip), %rsi
	call	engine_table_doall@PLT
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L102
	lock addl	$1, 156(%rax)
	movq	-64(%rbp), %rax
.L102:
	movq	%rax, (%rbx)
	movq	global_engine_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movq	-56(%rbp), %rax
.L98:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L111
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore_state
	movl	$196, %r8d
	movl	$65, %edx
	movl	$197, %esi
	movl	$38, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L98
.L111:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE913:
	.size	ENGINE_pkey_asn1_find_str, .-ENGINE_pkey_asn1_find_str
	.local	pkey_asn1_meth_table
	.comm	pkey_asn1_meth_table,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
