	.file	"pk7_mime.c"
	.text
	.p2align 4
	.globl	i2d_PKCS7_bio_stream
	.type	i2d_PKCS7_bio_stream, @function
i2d_PKCS7_bio_stream:
.LFB779:
	.cfi_startproc
	endbr64
	leaq	PKCS7_it(%rip), %r8
	jmp	i2d_ASN1_bio_stream@PLT
	.cfi_endproc
.LFE779:
	.size	i2d_PKCS7_bio_stream, .-i2d_PKCS7_bio_stream
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"PKCS7"
	.text
	.p2align 4
	.globl	PEM_write_bio_PKCS7_stream
	.type	PEM_write_bio_PKCS7_stream, @function
PEM_write_bio_PKCS7_stream:
.LFB780:
	.cfi_startproc
	endbr64
	leaq	PKCS7_it(%rip), %r9
	leaq	.LC0(%rip), %r8
	jmp	PEM_write_bio_ASN1_stream@PLT
	.cfi_endproc
.LFE780:
	.size	PEM_write_bio_PKCS7_stream, .-PEM_write_bio_PKCS7_stream
	.p2align 4
	.globl	SMIME_write_PKCS7
	.type	SMIME_write_PKCS7, @function
SMIME_write_PKCS7:
.LFB781:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	24(%rsi), %rdi
	movl	%ecx, %ebx
	call	OBJ_obj2nid@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	cmpl	$22, %r8d
	jne	.L5
	movq	32(%r12), %rax
	movq	8(%rax), %rax
.L5:
	leaq	PKCS7_it(%rip), %rdx
	xorb	$4, %bh
	movq	%r12, %rsi
	movq	%r13, %rdi
	pushq	%rdx
	movl	%ebx, %ecx
	movq	%r14, %rdx
	xorl	%r9d, %r9d
	pushq	%rax
	call	SMIME_write_ASN1@PLT
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE781:
	.size	SMIME_write_PKCS7, .-SMIME_write_PKCS7
	.p2align 4
	.globl	SMIME_read_PKCS7
	.type	SMIME_read_PKCS7, @function
SMIME_read_PKCS7:
.LFB782:
	.cfi_startproc
	endbr64
	leaq	PKCS7_it(%rip), %rdx
	jmp	SMIME_read_ASN1@PLT
	.cfi_endproc
.LFE782:
	.size	SMIME_read_PKCS7, .-SMIME_read_PKCS7
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
