	.file	"ssl_conf.c"
	.text
	.p2align 4
	.type	cmd_NumTickets, @function
cmd_NumTickets:
.LFB1080:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$10, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	xorl	%esi, %esi
	call	strtol@PLT
	testl	%eax, %eax
	js	.L4
	movq	24(%r12), %rdi
	movq	%rax, %rbx
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L3
	movslq	%ebx, %rsi
	call	SSL_CTX_set_num_tickets@PLT
.L3:
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1
	movslq	%ebx, %rsi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	SSL_set_num_tickets@PLT
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	xorl	%eax, %eax
.L1:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1080:
	.size	cmd_NumTickets, .-cmd_NumTickets
	.p2align 4
	.type	cmd_RecordPadding, @function
cmd_RecordPadding:
.LFB1079:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$10, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	xorl	%esi, %esi
	call	strtol@PLT
	testl	%eax, %eax
	js	.L15
	movq	24(%r12), %rdi
	movq	%rax, %rbx
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L14
	movslq	%ebx, %rsi
	call	SSL_CTX_set_block_padding@PLT
.L14:
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L12
	movslq	%ebx, %rsi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	SSL_set_block_padding@PLT
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	xorl	%eax, %eax
.L12:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1079:
	.size	cmd_RecordPadding, .-cmd_RecordPadding
	.p2align 4
	.type	cmd_Groups, @function
cmd_Groups:
.LFB1054:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	32(%rdi), %r8
	movq	%rsi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%r8, %r8
	je	.L23
	xorl	%edx, %edx
	movl	$92, %esi
	movq	%r8, %rdi
	call	SSL_ctrl@PLT
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	movq	24(%rdi), %rdi
	xorl	%edx, %edx
	movl	$92, %esi
	call	SSL_CTX_ctrl@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1054:
	.size	cmd_Groups, .-cmd_Groups
	.p2align 4
	.type	cmd_ClientSignatureAlgorithms, @function
cmd_ClientSignatureAlgorithms:
.LFB1053:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	32(%rdi), %r8
	movq	%rsi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%r8, %r8
	je	.L27
	xorl	%edx, %edx
	movl	$102, %esi
	movq	%r8, %rdi
	call	SSL_ctrl@PLT
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	movq	24(%rdi), %rdi
	xorl	%edx, %edx
	movl	$102, %esi
	call	SSL_CTX_ctrl@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1053:
	.size	cmd_ClientSignatureAlgorithms, .-cmd_ClientSignatureAlgorithms
	.p2align 4
	.type	cmd_SignatureAlgorithms, @function
cmd_SignatureAlgorithms:
.LFB1052:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	32(%rdi), %r8
	movq	%rsi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%r8, %r8
	je	.L31
	xorl	%edx, %edx
	movl	$98, %esi
	movq	%r8, %rdi
	call	SSL_ctrl@PLT
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	movq	24(%rdi), %rdi
	xorl	%edx, %edx
	movl	$98, %esi
	call	SSL_CTX_ctrl@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1052:
	.size	cmd_SignatureAlgorithms, .-cmd_SignatureAlgorithms
	.p2align 4
	.type	cmd_DHParameters, @function
cmd_DHParameters:
.LFB1078:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L50
.L35:
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L39
	movq	%r13, %rcx
	movl	$3, %edx
	movl	$108, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	testl	%eax, %eax
	jle	.L39
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	PEM_read_bio_DHparams@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L39
	movq	24(%rbx), %rdi
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L40
	movq	%r13, %rcx
	xorl	%edx, %edx
	movl	$3, %esi
	call	SSL_CTX_ctrl@PLT
.L40:
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L49
	movq	%r13, %rcx
	xorl	%edx, %edx
	movl	$3, %esi
	call	SSL_ctrl@PLT
.L49:
	xorl	%r14d, %r14d
	testl	%eax, %eax
	setg	%r14b
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L39:
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
.L38:
	movq	%r13, %rdi
	call	DH_free@PLT
	movq	%r12, %rdi
	call	BIO_free@PLT
.L34:
	popq	%rbx
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	cmpq	$0, 32(%rdi)
	jne	.L35
	movl	$1, %r14d
	jmp	.L34
	.cfi_endproc
.LFE1078:
	.size	cmd_DHParameters, .-cmd_DHParameters
	.p2align 4
	.type	cmd_RequestCAPath, @function
cmd_RequestCAPath:
.LFB1076:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	168(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L55
.L52:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	SSL_add_dir_cert_subjects_to_stack@PLT
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	movq	%rsi, -24(%rbp)
	call	OPENSSL_sk_new_null@PLT
	movq	-24(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, 168(%rbx)
	movq	%rax, %rdi
	jne	.L52
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1076:
	.size	cmd_RequestCAPath, .-cmd_RequestCAPath
	.p2align 4
	.type	cmd_RequestCAFile, @function
cmd_RequestCAFile:
.LFB1074:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	168(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L60
.L57:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	SSL_add_file_cert_subjects_to_stack@PLT
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	movq	%rsi, -24(%rbp)
	call	OPENSSL_sk_new_null@PLT
	movq	-24(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, 168(%rbx)
	movq	%rax, %rdi
	jne	.L57
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1074:
	.size	cmd_RequestCAFile, .-cmd_RequestCAFile
	.p2align 4
	.type	cmd_ServerInfoFile, @function
cmd_ServerInfoFile:
.LFB1068:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L65
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	SSL_CTX_use_serverinfo_file@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore 6
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1068:
	.size	cmd_ServerInfoFile, .-cmd_ServerInfoFile
	.p2align 4
	.type	cmd_PrivateKey, @function
cmd_PrivateKey:
.LFB1067:
	.cfi_startproc
	endbr64
	testb	$32, (%rdi)
	je	.L74
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L72
	movl	$1, %edx
	call	SSL_CTX_use_PrivateKey_file@PLT
.L72:
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L73
	movl	$1, %edx
	movq	%r12, %rsi
	call	SSL_use_PrivateKey_file@PLT
.L73:
	testl	%eax, %eax
	popq	%rbx
	popq	%r12
	setg	%al
	popq	%rbp
	.cfi_def_cfa 7, 8
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$-2, %eax
	ret
	.cfi_endproc
.LFE1067:
	.size	cmd_PrivateKey, .-cmd_PrivateKey
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/ssl/ssl_conf.c"
	.text
	.p2align 4
	.type	cmd_Certificate, @function
cmd_Certificate:
.LFB1066:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L85
	call	SSL_CTX_use_certificate_chain_file@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L99
.L86:
	movq	%r12, %rsi
	call	SSL_use_certificate_chain_file@PLT
	movq	32(%rbx), %rdx
	testl	%eax, %eax
	movq	1168(%rdx), %rdx
	setg	%al
	testq	%rdx, %rdx
	je	.L98
.L101:
	testb	%al, %al
	je	.L98
	movl	$1, %eax
	testb	$64, (%rbx)
	jne	.L100
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore_state
	movq	24(%rbx), %rdx
	testl	%eax, %eax
	setg	%al
	movq	320(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L101
	.p2align 4,,10
	.p2align 3
.L98:
	movzbl	%al, %eax
.L102:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L86
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	movq	(%rdx), %rax
	leaq	32(%rdx), %rcx
	leaq	.LC0(%rip), %rsi
	movabsq	$-3689348814741910323, %rdx
	subq	%rcx, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	movl	$431, %edx
	leaq	(%rbx,%rax,8), %rbx
	movq	48(%rbx), %rdi
	call	CRYPTO_free@PLT
	movl	$432, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_strdup@PLT
	testq	%rax, %rax
	movq	%rax, 48(%rbx)
	setne	%al
	movzbl	%al, %eax
	jmp	.L102
	.cfi_endproc
.LFE1066:
	.size	cmd_Certificate, .-cmd_Certificate
	.p2align 4
	.type	cmd_VerifyMode, @function
cmd_VerifyMode:
.LFB1065:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	testq	%rsi, %rsi
	je	.L104
	leaq	ssl_vfy_list.23754(%rip), %rax
	movl	$1, %edx
	movl	$44, %esi
	movq	$6, 160(%r8)
	movq	%rax, 152(%r8)
	leaq	ssl_set_option_list(%rip), %rcx
	jmp	CONF_parse_list@PLT
.L104:
	movl	$-3, %eax
	ret
	.cfi_endproc
.LFE1065:
	.size	cmd_VerifyMode, .-cmd_VerifyMode
	.p2align 4
	.type	cmd_Options, @function
cmd_Options:
.LFB1064:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	testq	%rsi, %rsi
	je	.L106
	leaq	ssl_option_list.23749(%rip), %rax
	movl	$1, %edx
	movl	$44, %esi
	movq	$15, 160(%r8)
	movq	%rax, 152(%r8)
	leaq	ssl_set_option_list(%rip), %rcx
	jmp	CONF_parse_list@PLT
.L106:
	movl	$-3, %eax
	ret
	.cfi_endproc
.LFE1064:
	.size	cmd_Options, .-cmd_Options
	.p2align 4
	.type	cmd_Protocol, @function
cmd_Protocol:
.LFB1059:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	leaq	ssl_protocol_list.23717(%rip), %rax
	movq	%rsi, %rdi
	movl	$1, %edx
	movq	%rax, 152(%r8)
	leaq	ssl_set_option_list(%rip), %rcx
	movl	$44, %esi
	movq	$9, 160(%r8)
	jmp	CONF_parse_list@PLT
	.cfi_endproc
.LFE1059:
	.size	cmd_Protocol, .-cmd_Protocol
	.p2align 4
	.type	cmd_Ciphersuites, @function
cmd_Ciphersuites:
.LFB1058:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L109
	call	SSL_CTX_set_ciphersuites@PLT
.L109:
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L110
	movq	%r12, %rsi
	call	SSL_set_ciphersuites@PLT
.L110:
	testl	%eax, %eax
	popq	%rbx
	popq	%r12
	setg	%al
	popq	%rbp
	.cfi_def_cfa 7, 8
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1058:
	.size	cmd_Ciphersuites, .-cmd_Ciphersuites
	.p2align 4
	.type	cmd_CipherString, @function
cmd_CipherString:
.LFB1057:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L119
	call	SSL_CTX_set_cipher_list@PLT
.L119:
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L120
	movq	%r12, %rsi
	call	SSL_set_cipher_list@PLT
.L120:
	testl	%eax, %eax
	popq	%rbx
	popq	%r12
	setg	%al
	popq	%rbp
	.cfi_def_cfa 7, 8
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1057:
	.size	cmd_CipherString, .-cmd_CipherString
	.p2align 4
	.type	ssl_conf_cmd_lookup.part.0, @function
ssl_conf_cmd_lookup.part.0:
.LFB1097:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	(%rdi), %eax
	movq	%rsi, %rbx
	movl	%eax, %r13d
	movl	%eax, %r14d
	movl	%eax, %ecx
	movl	%eax, %edx
	andl	$8, %r13d
	andl	$4, %r14d
	andl	$32, %ecx
	andl	$1, %edx
	testb	$2, %al
	jne	.L129
	xorl	%r12d, %r12d
	testl	%edx, %edx
	jne	.L337
.L128:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	leaq	ssl_conf_cmds(%rip), %r12
	testl	%edx, %edx
	jne	.L147
	testl	%ecx, %ecx
	je	.L148
	testl	%r13d, %r13d
	leaq	1536(%r12), %r13
	je	.L149
	.p2align 4,,10
	.p2align 3
.L153:
	testb	$4, 24(%r12)
	je	.L150
	testl	%r14d, %r14d
	je	.L151
.L150:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L151
	movq	%rbx, %rsi
	call	strcasecmp@PLT
	testl	%eax, %eax
	je	.L128
.L151:
	addq	$32, %r12
	cmpq	%r13, %r12
	jne	.L153
.L336:
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L147:
	.cfi_restore_state
	testl	%ecx, %ecx
	je	.L164
	testl	%r14d, %r14d
	leaq	1536(%r12), %r14
	je	.L165
	.p2align 4,,10
	.p2align 3
.L171:
	testb	$8, 24(%r12)
	je	.L170
	testl	%r13d, %r13d
	je	.L168
.L170:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L169
	movq	%rbx, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L128
.L169:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L168
	movq	%rbx, %rsi
	call	strcasecmp@PLT
	testl	%eax, %eax
	je	.L128
.L168:
	addq	$32, %r12
	cmpq	%r14, %r12
	jne	.L171
	jmp	.L336
.L337:
	leaq	ssl_conf_cmds(%rip), %r12
	testl	%ecx, %ecx
	je	.L131
	testl	%r13d, %r13d
	leaq	1536(%r12), %r13
	je	.L132
	.p2align 4,,10
	.p2align 3
.L136:
	testb	$4, 24(%r12)
	je	.L133
	testl	%r14d, %r14d
	je	.L134
.L133:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L134
	movq	%rbx, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L128
.L134:
	addq	$32, %r12
	cmpq	%r13, %r12
	jne	.L136
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L132:
	movzwl	24(%r12), %eax
	testb	$8, %al
	jne	.L137
	testb	$4, %al
	je	.L139
	testl	%r14d, %r14d
	je	.L137
.L139:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L137
	movq	%rbx, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L128
	.p2align 4,,10
	.p2align 3
.L137:
	addq	$32, %r12
	cmpq	%r13, %r12
	jne	.L132
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L149:
	movzwl	24(%r12), %eax
	testb	$8, %al
	jne	.L154
	testb	$4, %al
	je	.L156
	testl	%r14d, %r14d
	je	.L154
.L156:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L154
	movq	%rbx, %rsi
	call	strcasecmp@PLT
	testl	%eax, %eax
	je	.L128
	.p2align 4,,10
	.p2align 3
.L154:
	addq	$32, %r12
	cmpq	%r13, %r12
	jne	.L149
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L165:
	movzwl	24(%r12), %eax
	testb	$8, %al
	je	.L172
	testl	%r13d, %r13d
	je	.L173
.L172:
	testb	$4, %al
	jne	.L173
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L174
	movq	%rbx, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L128
.L174:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L173
	movq	%rbx, %rsi
	call	strcasecmp@PLT
	testl	%eax, %eax
	je	.L128
.L173:
	addq	$32, %r12
	cmpq	%r14, %r12
	jne	.L165
	jmp	.L336
.L164:
	testl	%r14d, %r14d
	leaq	1536(%r12), %r14
	je	.L175
	.p2align 4,,10
	.p2align 3
.L179:
	movzwl	24(%r12), %eax
	testb	$8, %al
	je	.L176
	testl	%r13d, %r13d
	je	.L177
.L176:
	testb	$32, %al
	jne	.L177
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L178
	movq	%rbx, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L128
.L178:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L177
	movq	%rbx, %rsi
	call	strcasecmp@PLT
	testl	%eax, %eax
	je	.L128
.L177:
	addq	$32, %r12
	cmpq	%r14, %r12
	jne	.L179
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L175:
	movzwl	24(%r12), %eax
	testb	$8, %al
	je	.L180
	testl	%r13d, %r13d
	je	.L181
.L180:
	testb	$36, %al
	jne	.L181
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L182
	movq	%rbx, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L128
.L182:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L181
	movq	%rbx, %rsi
	call	strcasecmp@PLT
	testl	%eax, %eax
	je	.L128
.L181:
	addq	$32, %r12
	cmpq	%r14, %r12
	jne	.L175
	jmp	.L336
.L148:
	testl	%r14d, %r14d
	leaq	1536(%r12), %r14
	je	.L157
	.p2align 4,,10
	.p2align 3
.L161:
	movzwl	24(%r12), %eax
	testb	$8, %al
	je	.L160
	testl	%r13d, %r13d
	je	.L159
.L160:
	testb	$32, %al
	jne	.L159
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L159
	movq	%rbx, %rsi
	call	strcasecmp@PLT
	testl	%eax, %eax
	je	.L128
.L159:
	addq	$32, %r12
	cmpq	%r14, %r12
	jne	.L161
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L157:
	movzwl	24(%r12), %eax
	testb	$8, %al
	je	.L162
	testl	%r13d, %r13d
	je	.L163
.L162:
	testb	$36, %al
	jne	.L163
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L163
	movq	%rbx, %rsi
	call	strcasecmp@PLT
	testl	%eax, %eax
	je	.L128
.L163:
	addq	$32, %r12
	cmpq	%r14, %r12
	jne	.L157
	jmp	.L336
.L131:
	testl	%r14d, %r14d
	leaq	1536(%r12), %r14
	je	.L140
	.p2align 4,,10
	.p2align 3
.L144:
	movzwl	24(%r12), %eax
	testb	$8, %al
	je	.L143
	testl	%r13d, %r13d
	je	.L142
.L143:
	testb	$32, %al
	jne	.L142
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L142
	movq	%rbx, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L128
.L142:
	addq	$32, %r12
	cmpq	%r14, %r12
	jne	.L144
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L140:
	movzwl	24(%r12), %eax
	testb	$8, %al
	je	.L145
	testl	%r13d, %r13d
	je	.L146
.L145:
	testb	$36, %al
	jne	.L146
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L146
	movq	%rbx, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L128
.L146:
	addq	$32, %r12
	cmpq	%r14, %r12
	jne	.L140
	jmp	.L336
	.cfi_endproc
.LFE1097:
	.size	ssl_conf_cmd_lookup.part.0, .-ssl_conf_cmd_lookup.part.0
	.p2align 4
	.type	cmd_ChainCAFile, @function
cmd_ChainCAFile:
.LFB1071:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L339
	movq	320(%rax), %rbx
	movq	456(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L346
.L342:
	xorl	%edx, %edx
	call	X509_STORE_load_locations@PLT
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
.L338:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L339:
	.cfi_restore_state
	movq	32(%rdi), %rax
	testq	%rax, %rax
	je	.L343
	movq	1168(%rax), %rbx
	movq	456(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L342
.L346:
	movq	%rsi, -24(%rbp)
	call	X509_STORE_new@PLT
	movq	-24(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, 456(%rbx)
	movq	%rax, %rdi
	jne	.L342
	xorl	%eax, %eax
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L343:
	movl	$1, %eax
	jmp	.L338
	.cfi_endproc
.LFE1071:
	.size	cmd_ChainCAFile, .-cmd_ChainCAFile
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"None"
.LC2:
	.string	"SSLv3"
.LC3:
	.string	"TLSv1"
.LC4:
	.string	"TLSv1.1"
.LC5:
	.string	"TLSv1.2"
.LC6:
	.string	"TLSv1.3"
.LC7:
	.string	"DTLSv1"
.LC8:
	.string	"DTLSv1.2"
	.text
	.p2align 4
	.type	cmd_MaxProtocol, @function
cmd_MaxProtocol:
.LFB1063:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rcx
	movq	%rdi, %rdx
	movq	%rsi, %rax
	testq	%rcx, %rcx
	je	.L348
.L363:
	movq	(%rcx), %rcx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rdi
	movl	(%rcx), %r8d
	movl	$5, %ecx
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L353
	movl	$6, %ecx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L354
	movl	$6, %ecx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L355
	movl	$8, %ecx
	leaq	.LC4(%rip), %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L356
	movl	$8, %ecx
	leaq	.LC5(%rip), %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L357
	movl	$8, %ecx
	leaq	.LC6(%rip), %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L358
	movl	$7, %ecx
	leaq	.LC7(%rip), %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L359
	movq	%rax, %rdi
	movl	$9, %ecx
	leaq	.LC8(%rip), %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L364
.L347:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L348:
	movq	32(%rdi), %rcx
	testq	%rcx, %rcx
	je	.L347
	movq	1440(%rcx), %rcx
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L353:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L351:
	salq	$4, %rax
	leaq	versions.23724(%rip), %rcx
	movl	8(%rcx,%rax), %esi
	testl	%esi, %esi
	js	.L347
	movq	144(%rdx), %rdx
	movl	%r8d, %edi
	jmp	ssl_set_version_bound@PLT
	.p2align 4,,10
	.p2align 3
.L354:
	movl	$1, %eax
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L355:
	movl	$2, %eax
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L356:
	movl	$3, %eax
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L357:
	movl	$4, %eax
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L358:
	movl	$5, %eax
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L359:
	movl	$6, %eax
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L364:
	movl	$7, %eax
	jmp	.L351
	.cfi_endproc
.LFE1063:
	.size	cmd_MaxProtocol, .-cmd_MaxProtocol
	.section	.rodata.str1.1
.LC9:
	.string	"+automatic"
.LC10:
	.string	"automatic"
.LC11:
	.string	"auto"
	.text
	.p2align 4
	.type	cmd_ECDHParameters, @function
cmd_ECDHParameters:
.LFB1056:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	(%rdi), %r13d
	movq	%rdi, %rbx
	testb	$2, %r13b
	je	.L366
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	movl	$1, %r14d
	call	strcasecmp@PLT
	testl	%eax, %eax
	je	.L365
	leaq	.LC10(%rip), %rsi
	movq	%r12, %rdi
	call	strcasecmp@PLT
	testl	%eax, %eax
	je	.L365
.L366:
	andl	$1, %r13d
	je	.L368
	movl	$5, %ecx
	leaq	.LC11(%rip), %rdi
	movq	%r12, %rsi
	movl	$1, %r14d
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L365
.L368:
	movq	%r12, %rdi
	call	EC_curve_nist2nid@PLT
	testl	%eax, %eax
	je	.L391
.L369:
	movl	%eax, %edi
	call	EC_KEY_new_by_curve_name@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L370
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L371
	xorl	%edx, %edx
	movq	%rax, %rcx
	movl	$4, %esi
	xorl	%r14d, %r14d
	call	SSL_CTX_ctrl@PLT
	testl	%eax, %eax
	setg	%r14b
.L372:
	movq	%r12, %rdi
	call	EC_KEY_free@PLT
.L365:
	popq	%rbx
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L391:
	.cfi_restore_state
	movq	%r12, %rdi
	call	OBJ_sn2nid@PLT
	testl	%eax, %eax
	jne	.L369
.L370:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movl	%r14d, %eax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L371:
	.cfi_restore_state
	movq	32(%rbx), %rdi
	movl	$1, %r14d
	testq	%rdi, %rdi
	je	.L372
	xorl	%edx, %edx
	movq	%rax, %rcx
	movl	$4, %esi
	xorl	%r14d, %r14d
	call	SSL_ctrl@PLT
	testl	%eax, %eax
	setg	%r14b
	jmp	.L372
	.cfi_endproc
.LFE1056:
	.size	cmd_ECDHParameters, .-cmd_ECDHParameters
	.p2align 4
	.type	cmd_MinProtocol, @function
cmd_MinProtocol:
.LFB1062:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rcx
	movq	%rdi, %rdx
	movq	%rsi, %rax
	testq	%rcx, %rcx
	je	.L393
.L408:
	movq	(%rcx), %rcx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rdi
	movl	(%rcx), %r8d
	movl	$5, %ecx
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L398
	movl	$6, %ecx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L399
	movl	$6, %ecx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L400
	movl	$8, %ecx
	leaq	.LC4(%rip), %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L401
	movl	$8, %ecx
	leaq	.LC5(%rip), %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L402
	movl	$8, %ecx
	leaq	.LC6(%rip), %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L403
	movl	$7, %ecx
	leaq	.LC7(%rip), %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L404
	movq	%rax, %rdi
	movl	$9, %ecx
	leaq	.LC8(%rip), %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L409
.L392:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L393:
	movq	32(%rdi), %rcx
	testq	%rcx, %rcx
	je	.L392
	movq	1440(%rcx), %rcx
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L398:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L396:
	salq	$4, %rax
	leaq	versions.23724(%rip), %rcx
	movl	8(%rcx,%rax), %esi
	testl	%esi, %esi
	js	.L392
	movq	136(%rdx), %rdx
	movl	%r8d, %edi
	jmp	ssl_set_version_bound@PLT
	.p2align 4,,10
	.p2align 3
.L399:
	movl	$1, %eax
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L400:
	movl	$2, %eax
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L401:
	movl	$3, %eax
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L402:
	movl	$4, %eax
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L403:
	movl	$5, %eax
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L404:
	movl	$6, %eax
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L409:
	movl	$7, %eax
	jmp	.L396
	.cfi_endproc
.LFE1062:
	.size	cmd_MinProtocol, .-cmd_MinProtocol
	.p2align 4
	.type	cmd_ClientCAFile, @function
cmd_ClientCAFile:
.LFB1075:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	168(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L414
.L411:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	SSL_add_file_cert_subjects_to_stack@PLT
	.p2align 4,,10
	.p2align 3
.L414:
	.cfi_restore_state
	movq	%rsi, -24(%rbp)
	call	OPENSSL_sk_new_null@PLT
	movq	-24(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, 168(%rbx)
	movq	%rax, %rdi
	jne	.L411
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1075:
	.size	cmd_ClientCAFile, .-cmd_ClientCAFile
	.p2align 4
	.type	cmd_ClientCAPath, @function
cmd_ClientCAPath:
.LFB1077:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	168(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L419
.L416:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	SSL_add_dir_cert_subjects_to_stack@PLT
	.p2align 4,,10
	.p2align 3
.L419:
	.cfi_restore_state
	movq	%rsi, -24(%rbp)
	call	OPENSSL_sk_new_null@PLT
	movq	-24(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, 168(%rbx)
	movq	%rax, %rdi
	jne	.L416
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1077:
	.size	cmd_ClientCAPath, .-cmd_ClientCAPath
	.p2align 4
	.type	cmd_Curves, @function
cmd_Curves:
.LFB1055:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	32(%rdi), %r8
	movq	%rsi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%r8, %r8
	je	.L421
	xorl	%edx, %edx
	movl	$92, %esi
	movq	%r8, %rdi
	call	SSL_ctrl@PLT
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L421:
	.cfi_restore_state
	movq	24(%rdi), %rdi
	xorl	%edx, %edx
	movl	$92, %esi
	call	SSL_CTX_ctrl@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1055:
	.size	cmd_Curves, .-cmd_Curves
	.p2align 4
	.type	ssl_set_option_list, @function
ssl_set_option_list:
.LFB1051:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdi, %rdi
	je	.L465
	movq	%rdi, %r11
	movl	%esi, %r10d
	movq	%rdx, %r8
	cmpl	$-1, %esi
	je	.L426
	movzbl	(%rdi), %eax
	cmpb	$43, %al
	je	.L466
	cmpb	$45, %al
	je	.L467
	movq	160(%rdx), %r15
	movq	152(%rdx), %rbx
	testq	%r15, %r15
	je	.L465
	movl	$1, -52(%rbp)
	movl	(%r8), %r9d
	xorl	%r14d, %r14d
.L442:
	xorl	%r13d, %r13d
	movslq	%r10d, %rdx
	.p2align 4,,10
	.p2align 3
.L433:
	movl	12(%rbx), %r12d
	movl	%r12d, %eax
	andl	%r9d, %eax
	testb	$12, %al
	je	.L431
	cmpl	%r10d, 8(%rbx)
	jne	.L431
	movq	(%rbx), %rdi
	movq	%r11, %rsi
	movq	%r8, -88(%rbp)
	movl	%r10d, -56(%rbp)
	movl	%r9d, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%r11, -64(%rbp)
	call	strncasecmp@PLT
	movq	-64(%rbp), %r11
	movq	-72(%rbp), %rdx
	testl	%eax, %eax
	movl	-80(%rbp), %r9d
	movl	-56(%rbp), %r10d
	movq	-88(%rbp), %r8
	jne	.L431
.L432:
	movq	40(%r8), %rax
	testq	%rax, %rax
	je	.L446
	testb	$1, %r12b
	cmove	-52(%rbp), %r14d
	andl	$3840, %r12d
	cmpl	$256, %r12d
	je	.L436
	cmpl	$512, %r12d
	je	.L437
	testl	%r12d, %r12d
	je	.L438
.L446:
	movl	$1, %r14d
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L431:
	addq	$1, %r13
	addq	$24, %rbx
	cmpq	%r15, %r13
	jne	.L433
.L465:
	xorl	%r14d, %r14d
.L424:
	addq	$56, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L437:
	.cfi_restore_state
	movq	128(%r8), %rax
.L438:
	movl	(%rax), %ecx
	movq	16(%rbx), %rdx
	testl	%r14d, %r14d
	je	.L439
	orl	%ecx, %edx
	movl	%edx, (%rax)
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L439:
	notl	%edx
	movl	$1, %r14d
	andl	%ecx, %edx
	movl	%edx, (%rax)
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L436:
	movq	120(%r8), %rax
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L426:
	movq	160(%rdx), %r15
	movq	152(%rdx), %rbx
	testq	%r15, %r15
	je	.L465
	movl	$1, -52(%rbp)
	movl	(%r8), %r9d
	xorl	%r14d, %r14d
.L430:
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L440:
	movl	12(%rbx), %r12d
	movl	%r12d, %eax
	andl	%r9d, %eax
	testb	$12, %al
	je	.L434
	movq	(%rbx), %rdi
	movq	%r11, %rsi
	movq	%r8, -80(%rbp)
	movl	%r9d, -72(%rbp)
	movq	%r11, -64(%rbp)
	call	strcmp@PLT
	movq	-64(%rbp), %r11
	movl	-72(%rbp), %r9d
	testl	%eax, %eax
	movq	-80(%rbp), %r8
	je	.L432
	.p2align 4,,10
	.p2align 3
.L434:
	addq	$1, %r13
	addq	$24, %rbx
	cmpq	%r15, %r13
	jne	.L440
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L466:
	movl	$1, -52(%rbp)
	addq	$1, %r11
	subl	$1, %r10d
	xorl	%r14d, %r14d
.L428:
	movq	160(%r8), %r15
	movq	152(%r8), %rbx
	testq	%r15, %r15
	je	.L465
	movl	(%r8), %r9d
	cmpl	$-1, %r10d
	jne	.L442
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L467:
	movl	$0, -52(%rbp)
	addq	$1, %r11
	subl	$1, %r10d
	movl	$1, %r14d
	jmp	.L428
	.cfi_endproc
.LFE1051:
	.size	ssl_set_option_list, .-ssl_set_option_list
	.p2align 4
	.type	cmd_ChainCAPath, @function
cmd_ChainCAPath:
.LFB1070:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L469
	movq	320(%rax), %rbx
	movq	456(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L476
.L472:
	xorl	%esi, %esi
	movq	%r12, %rdx
	call	X509_STORE_load_locations@PLT
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
.L468:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L469:
	.cfi_restore_state
	movq	32(%rdi), %rax
	testq	%rax, %rax
	je	.L473
	movq	1168(%rax), %rbx
	movq	456(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L472
.L476:
	call	X509_STORE_new@PLT
	movq	%rax, 456(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L472
	xorl	%eax, %eax
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L473:
	movl	$1, %eax
	jmp	.L468
	.cfi_endproc
.LFE1070:
	.size	cmd_ChainCAPath, .-cmd_ChainCAPath
	.p2align 4
	.type	cmd_VerifyCAPath, @function
cmd_VerifyCAPath:
.LFB1072:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L478
	movq	320(%rax), %rbx
	movq	464(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L485
.L481:
	xorl	%esi, %esi
	movq	%r12, %rdx
	call	X509_STORE_load_locations@PLT
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
.L477:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L478:
	.cfi_restore_state
	movq	32(%rdi), %rax
	testq	%rax, %rax
	je	.L482
	movq	1168(%rax), %rbx
	movq	464(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L481
.L485:
	call	X509_STORE_new@PLT
	movq	%rax, 464(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L481
	xorl	%eax, %eax
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L482:
	movl	$1, %eax
	jmp	.L477
	.cfi_endproc
.LFE1072:
	.size	cmd_VerifyCAPath, .-cmd_VerifyCAPath
	.p2align 4
	.type	cmd_VerifyCAFile, @function
cmd_VerifyCAFile:
.LFB1073:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L487
	movq	320(%rax), %rbx
	movq	464(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L494
.L490:
	xorl	%edx, %edx
	call	X509_STORE_load_locations@PLT
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
.L486:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L487:
	.cfi_restore_state
	movq	32(%rdi), %rax
	testq	%rax, %rax
	je	.L491
	movq	1168(%rax), %rbx
	movq	464(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L490
.L494:
	movq	%rsi, -24(%rbp)
	call	X509_STORE_new@PLT
	movq	-24(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, 464(%rbx)
	movq	%rax, %rdi
	jne	.L490
	xorl	%eax, %eax
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L491:
	movl	$1, %eax
	jmp	.L486
	.cfi_endproc
.LFE1073:
	.size	cmd_VerifyCAFile, .-cmd_VerifyCAFile
	.section	.rodata.str1.1
.LC12:
	.string	", value="
.LC13:
	.string	"cmd="
	.text
	.p2align 4
	.globl	SSL_CONF_cmd
	.type	SSL_CONF_cmd, @function
SSL_CONF_cmd:
.LFB1085:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rsi, %rsi
	je	.L550
	movq	8(%rdi), %r15
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	%rdx, %r13
	testq	%r15, %r15
	je	.L498
	movq	%rsi, %rdi
	call	strlen@PLT
	movq	16(%rbx), %rdx
	cmpq	%rdx, %rax
	jbe	.L548
	movl	(%rbx), %r14d
	testb	$1, %r14b
	je	.L500
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rdx, -56(%rbp)
	call	strncmp@PLT
	movq	-56(%rbp), %rdx
	testl	%eax, %eax
	jne	.L548
.L500:
	testb	$2, %r14b
	jne	.L551
.L502:
	addq	%rdx, %r12
.L503:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	ssl_conf_cmd_lookup.part.0
	testq	%rax, %rax
	je	.L552
	cmpw	$4, 26(%rax)
	je	.L553
	testq	%r13, %r13
	je	.L518
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	*(%rax)
	movl	%eax, %edx
	movl	$2, %eax
	testl	%edx, %edx
	jg	.L495
	cmpl	$-2, %edx
	je	.L548
	testb	$16, (%rbx)
	jne	.L554
.L549:
	xorl	%eax, %eax
.L495:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L498:
	.cfi_restore_state
	movl	(%rdi), %r14d
	testb	$1, %r14b
	je	.L503
	cmpb	$45, (%rsi)
	jne	.L548
	cmpb	$0, 1(%rsi)
	je	.L548
	addq	$1, %r12
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L552:
	andl	$16, %r14d
	je	.L548
	movl	$386, %edx
	movl	$334, %esi
	movl	$20, %edi
	movl	$825, %r8d
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdx
	movl	$2, %edi
	xorl	%eax, %eax
	leaq	.LC13(%rip), %rsi
	call	ERR_add_error_data@PLT
.L548:
	addq	$24, %rsp
	movl	$-2, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L551:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rdx, -56(%rbp)
	call	strncasecmp@PLT
	movq	-56(%rbp), %rdx
	testl	%eax, %eax
	je	.L502
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L554:
	movl	$818, %r8d
	movl	$384, %edx
	movl	$334, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r13, %r8
	movq	%r12, %rdx
	movl	$4, %edi
	leaq	.LC12(%rip), %rcx
	leaq	.LC13(%rip), %rsi
	xorl	%eax, %eax
	call	ERR_add_error_data@PLT
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L550:
	movl	$796, %r8d
	movl	$385, %edx
	movl	$334, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L553:
	.cfi_restore_state
	leaq	ssl_conf_cmds(%rip), %rdx
	subq	%rdx, %rax
	movq	%rax, %rdx
	sarq	$5, %rdx
	cmpq	$703, %rax
	ja	.L549
	leaq	ssl_cmd_switches(%rip), %rax
	salq	$4, %rdx
	addq	%rax, %rdx
	movq	40(%rbx), %rax
	testq	%rax, %rax
	je	.L547
	movl	8(%rdx), %ecx
	movq	(%rdx), %rsi
	movl	%ecx, %edx
	andl	$3840, %edx
	andl	$1, %ecx
	je	.L555
	cmpl	$256, %edx
	je	.L513
	cmpl	$512, %edx
	je	.L514
	testl	%edx, %edx
	je	.L509
.L547:
	movl	$1, %eax
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L555:
	cmpl	$256, %edx
	je	.L511
	cmpl	$512, %edx
	je	.L512
	testl	%edx, %edx
	jne	.L547
.L508:
	orl	%esi, (%rax)
	movl	$1, %eax
	jmp	.L495
.L513:
	movq	120(%rbx), %rax
.L509:
	notl	%esi
	andl	%esi, (%rax)
	movl	$1, %eax
	jmp	.L495
.L514:
	movq	128(%rbx), %rax
	jmp	.L509
.L512:
	movq	128(%rbx), %rax
	jmp	.L508
.L511:
	movq	120(%rbx), %rax
	jmp	.L508
.L518:
	movl	$-3, %eax
	jmp	.L495
	.cfi_endproc
.LFE1085:
	.size	SSL_CONF_cmd, .-SSL_CONF_cmd
	.p2align 4
	.globl	SSL_CONF_cmd_argv
	.type	SSL_CONF_cmd_argv, @function
SSL_CONF_cmd_argv:
.LFB1086:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	testq	%rsi, %rsi
	je	.L557
	movl	(%rsi), %eax
	testl	%eax, %eax
	jle	.L603
	movq	(%rdx), %r15
	movq	(%r15), %r13
	testq	%r13, %r13
	je	.L603
	movq	$0, -56(%rbp)
	cmpl	$1, %eax
	je	.L560
.L577:
	movq	8(%r15), %rax
	movq	%rax, -56(%rbp)
.L560:
	movl	(%rbx), %eax
	movq	8(%rbx), %rsi
	movl	%eax, -60(%rbp)
	andl	$-3, %eax
	orl	$1, %eax
	testq	%rsi, %rsi
	movq	%rsi, -72(%rbp)
	movl	%eax, (%rbx)
	je	.L561
	movq	%r13, %rdi
	call	strlen@PLT
	movq	16(%rbx), %rdx
	cmpq	%rdx, %rax
	jbe	.L603
	movq	-72(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rdx, -80(%rbp)
	call	strncmp@PLT
	testl	%eax, %eax
	jne	.L603
	movq	-80(%rbp), %rdx
	addq	%rdx, %r13
.L563:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	ssl_conf_cmd_lookup.part.0
	testq	%rax, %rax
	je	.L605
	cmpw	$4, 26(%rax)
	je	.L606
	movq	-56(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L580
	movq	%rcx, %rsi
	movq	%rbx, %rdi
	call	*(%rax)
	testl	%eax, %eax
	jg	.L607
	cmpl	$-2, %eax
	je	.L603
	testb	$16, (%rbx)
	jne	.L608
.L604:
	addq	$40, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L605:
	.cfi_restore_state
	testb	$16, -60(%rbp)
	je	.L603
	movl	$386, %edx
	movl	$334, %esi
	movl	$20, %edi
	movl	$825, %r8d
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r13, %rdx
	movl	$2, %edi
	xorl	%eax, %eax
	leaq	.LC13(%rip), %rsi
	call	ERR_add_error_data@PLT
	.p2align 4,,10
	.p2align 3
.L603:
	xorl	%eax, %eax
.L556:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L557:
	.cfi_restore_state
	movq	(%rdx), %r15
	movq	(%r15), %r13
	testq	%r13, %r13
	jne	.L577
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L607:
	movq	(%r14), %r15
	movl	$16, %edx
	movl	$2, %eax
.L566:
	leaq	(%r15,%rdx), %r8
	movq	%r8, (%r14)
	testq	%r12, %r12
	je	.L556
	subl	%eax, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L608:
	.cfi_restore_state
	movl	$818, %r8d
	movl	$384, %edx
	movl	$334, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-56(%rbp), %r8
	movq	%r13, %rdx
	xorl	%eax, %eax
	leaq	.LC12(%rip), %rcx
	leaq	.LC13(%rip), %rsi
	movl	$4, %edi
	call	ERR_add_error_data@PLT
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L561:
	cmpb	$45, 0(%r13)
	jne	.L603
	cmpb	$0, 1(%r13)
	je	.L603
	addq	$1, %r13
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L606:
	leaq	ssl_conf_cmds(%rip), %rdx
	subq	%rdx, %rax
	movq	%rax, %rdx
	sarq	$5, %rdx
	cmpq	$703, %rax
	ja	.L604
	movq	40(%rbx), %rsi
	salq	$4, %rdx
	leaq	ssl_cmd_switches(%rip), %rax
	addq	%rdx, %rax
	testq	%rsi, %rsi
	je	.L579
	movl	8(%rax), %edx
	movl	%edx, %edi
	andl	$3840, %edx
	notl	%edi
	andl	$1, %edi
	cmpl	$256, %edx
	je	.L567
	cmpl	$512, %edx
	je	.L568
	testl	%edx, %edx
	je	.L569
.L579:
	movl	$8, %edx
	movl	$1, %eax
	jmp	.L566
.L568:
	movq	128(%rbx), %rsi
.L569:
	movl	(%rsi), %edx
	movq	(%rax), %rax
	testl	%edi, %edi
	je	.L570
	orl	%edx, %eax
	movl	$8, %edx
	movl	%eax, (%rsi)
	movl	$1, %eax
	jmp	.L566
.L570:
	notl	%eax
	andl	%edx, %eax
	movl	$8, %edx
	movl	%eax, (%rsi)
	movl	$1, %eax
	jmp	.L566
.L567:
	movq	120(%rbx), %rsi
	jmp	.L569
.L580:
	movl	$-3, %eax
	jmp	.L556
	.cfi_endproc
.LFE1086:
	.size	SSL_CONF_cmd_argv, .-SSL_CONF_cmd_argv
	.p2align 4
	.globl	SSL_CONF_cmd_value_type
	.type	SSL_CONF_cmd_value_type, @function
SSL_CONF_cmd_value_type:
.LFB1087:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rsi, %rsi
	je	.L632
	movq	8(%rdi), %r15
	movq	%rdi, %r13
	movq	%rsi, %r12
	testq	%r15, %r15
	je	.L611
	movq	%rsi, %rdi
	xorl	%r14d, %r14d
	call	strlen@PLT
	movq	16(%r13), %rbx
	cmpq	%rbx, %rax
	jbe	.L609
	movl	0(%r13), %ecx
	testb	$1, %cl
	je	.L612
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%ecx, -52(%rbp)
	call	strncmp@PLT
	movl	-52(%rbp), %ecx
	testl	%eax, %eax
	jne	.L609
.L612:
	andl	$2, %ecx
	jne	.L633
.L613:
	addq	%rbx, %r12
.L614:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	ssl_conf_cmd_lookup.part.0
	testq	%rax, %rax
	je	.L632
	movzwl	26(%rax), %r14d
.L609:
	addq	$24, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L632:
	.cfi_restore_state
	xorl	%r14d, %r14d
	jmp	.L609
	.p2align 4,,10
	.p2align 3
.L611:
	testb	$1, (%rdi)
	je	.L614
	xorl	%r14d, %r14d
	cmpb	$45, (%rsi)
	jne	.L609
	cmpb	$0, 1(%rsi)
	je	.L609
	addq	$1, %r12
	jmp	.L614
	.p2align 4,,10
	.p2align 3
.L633:
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	strncasecmp@PLT
	testl	%eax, %eax
	je	.L613
	jmp	.L609
	.cfi_endproc
.LFE1087:
	.size	SSL_CONF_cmd_value_type, .-SSL_CONF_cmd_value_type
	.p2align 4
	.globl	SSL_CONF_CTX_new
	.type	SSL_CONF_CTX_new, @function
SSL_CONF_CTX_new:
.LFB1088:
	.cfi_startproc
	endbr64
	movl	$878, %edx
	leaq	.LC0(%rip), %rsi
	movl	$176, %edi
	jmp	CRYPTO_zalloc@PLT
	.cfi_endproc
.LFE1088:
	.size	SSL_CONF_CTX_new, .-SSL_CONF_CTX_new
	.p2align 4
	.globl	SSL_CONF_CTX_finish
	.type	SSL_CONF_CTX_finish, @function
SSL_CONF_CTX_finish:
.LFB1089:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L636
	movq	320(%rdi), %r12
.L637:
	testq	%r12, %r12
	je	.L642
	movl	(%rbx), %eax
	testb	$64, %al
	je	.L642
	movq	48(%rbx), %r13
	testq	%r13, %r13
	je	.L643
	cmpq	$0, 40(%r12)
	je	.L747
.L643:
	movq	56(%rbx), %r13
	testq	%r13, %r13
	je	.L648
	cmpq	$0, 80(%r12)
	je	.L748
.L648:
	movq	64(%rbx), %r13
	testq	%r13, %r13
	je	.L652
	cmpq	$0, 120(%r12)
	je	.L749
.L652:
	movq	72(%rbx), %r13
	testq	%r13, %r13
	je	.L656
	cmpq	$0, 160(%r12)
	je	.L750
.L656:
	movq	80(%rbx), %r13
	testq	%r13, %r13
	je	.L660
	cmpq	$0, 200(%r12)
	je	.L751
.L660:
	movq	88(%rbx), %r13
	testq	%r13, %r13
	je	.L664
	cmpq	$0, 240(%r12)
	je	.L752
.L664:
	movq	96(%rbx), %r13
	testq	%r13, %r13
	je	.L668
	cmpq	$0, 280(%r12)
	je	.L753
.L668:
	movq	104(%rbx), %r13
	testq	%r13, %r13
	je	.L672
	cmpq	$0, 320(%r12)
	je	.L754
.L672:
	movq	112(%rbx), %r13
	testq	%r13, %r13
	je	.L642
	cmpq	$0, 360(%r12)
	je	.L755
	.p2align 4,,10
	.p2align 3
.L642:
	movq	168(%rbx), %r8
	testq	%r8, %r8
	je	.L746
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L681
	movq	%r8, %rsi
	call	SSL_set0_CA_list@PLT
.L682:
	movq	$0, 168(%rbx)
.L746:
	movl	$1, %eax
.L635:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L681:
	.cfi_restore_state
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L683
	movq	%r8, %rsi
	call	SSL_CTX_set0_CA_list@PLT
	jmp	.L682
	.p2align 4,,10
	.p2align 3
.L636:
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.L638
	movq	1168(%rax), %r12
	jmp	.L637
	.p2align 4,,10
	.p2align 3
.L638:
	movq	168(%rbx), %r8
	testq	%r8, %r8
	je	.L746
.L683:
	movq	X509_NAME_free@GOTPCREL(%rip), %rsi
	movq	%r8, %rdi
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L682
	.p2align 4,,10
	.p2align 3
.L748:
	testb	$32, (%rbx)
	je	.L648
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L756
	movl	$1, %edx
	movq	%r13, %rsi
	call	SSL_CTX_use_PrivateKey_file@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L651
.L650:
	movl	$1, %edx
	movq	%r13, %rsi
	call	SSL_use_PrivateKey_file@PLT
.L651:
	testl	%eax, %eax
	jg	.L648
	xorl	%eax, %eax
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L747:
	testb	$32, %al
	je	.L643
	testq	%rdi, %rdi
	je	.L757
	movl	$1, %edx
	movq	%r13, %rsi
	call	SSL_CTX_use_PrivateKey_file@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L647
.L645:
	movl	$1, %edx
	movq	%r13, %rsi
	call	SSL_use_PrivateKey_file@PLT
.L647:
	testl	%eax, %eax
	jg	.L643
	xorl	%eax, %eax
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L749:
	testb	$32, (%rbx)
	je	.L652
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L758
	movl	$1, %edx
	movq	%r13, %rsi
	call	SSL_CTX_use_PrivateKey_file@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L655
.L654:
	movl	$1, %edx
	movq	%r13, %rsi
	call	SSL_use_PrivateKey_file@PLT
.L655:
	testl	%eax, %eax
	jg	.L652
	xorl	%eax, %eax
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L750:
	testb	$32, (%rbx)
	je	.L656
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L759
	movl	$1, %edx
	movq	%r13, %rsi
	call	SSL_CTX_use_PrivateKey_file@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L659
.L658:
	movl	$1, %edx
	movq	%r13, %rsi
	call	SSL_use_PrivateKey_file@PLT
.L659:
	testl	%eax, %eax
	jg	.L656
	xorl	%eax, %eax
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L751:
	testb	$32, (%rbx)
	je	.L660
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L760
	movl	$1, %edx
	movq	%r13, %rsi
	call	SSL_CTX_use_PrivateKey_file@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L663
.L662:
	movl	$1, %edx
	movq	%r13, %rsi
	call	SSL_use_PrivateKey_file@PLT
.L663:
	testl	%eax, %eax
	jg	.L660
	xorl	%eax, %eax
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L752:
	testb	$32, (%rbx)
	je	.L664
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L761
	movl	$1, %edx
	movq	%r13, %rsi
	call	SSL_CTX_use_PrivateKey_file@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L667
.L666:
	movl	$1, %edx
	movq	%r13, %rsi
	call	SSL_use_PrivateKey_file@PLT
.L667:
	testl	%eax, %eax
	jg	.L664
	xorl	%eax, %eax
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L753:
	testb	$32, (%rbx)
	je	.L668
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L762
	movl	$1, %edx
	movq	%r13, %rsi
	call	SSL_CTX_use_PrivateKey_file@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L671
.L670:
	movl	$1, %edx
	movq	%r13, %rsi
	call	SSL_use_PrivateKey_file@PLT
.L671:
	testl	%eax, %eax
	jg	.L668
	xorl	%eax, %eax
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L755:
	testb	$32, (%rbx)
	je	.L642
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L678
	movl	$1, %edx
	movq	%r13, %rsi
	call	SSL_CTX_use_PrivateKey_file@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L679
.L684:
	movl	$1, %edx
	movq	%r13, %rsi
	call	SSL_use_PrivateKey_file@PLT
.L679:
	testl	%eax, %eax
	jg	.L642
	xorl	%eax, %eax
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L754:
	testb	$32, (%rbx)
	je	.L672
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L763
	movl	$1, %edx
	movq	%r13, %rsi
	call	SSL_CTX_use_PrivateKey_file@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L675
.L674:
	movl	$1, %edx
	movq	%r13, %rsi
	call	SSL_use_PrivateKey_file@PLT
.L675:
	testl	%eax, %eax
	jg	.L672
	xorl	%eax, %eax
	jmp	.L635
.L757:
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L645
	jmp	.L643
.L758:
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L654
	jmp	.L652
.L756:
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L650
	jmp	.L648
.L761:
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L666
	jmp	.L664
.L760:
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L662
	jmp	.L660
.L759:
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L658
	jmp	.L656
.L762:
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L670
	jmp	.L668
.L763:
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L674
	jmp	.L672
.L678:
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L684
	jmp	.L642
	.cfi_endproc
.LFE1089:
	.size	SSL_CONF_CTX_finish, .-SSL_CONF_CTX_finish
	.p2align 4
	.globl	SSL_CONF_CTX_free
	.type	SSL_CONF_CTX_free, @function
SSL_CONF_CTX_free:
.LFB1090:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L764
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	120(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	.LC0(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	48(%rdi), %rbx
	.p2align 4,,10
	.p2align 3
.L766:
	movq	(%rbx), %rdi
	movl	$921, %edx
	movq	%r13, %rsi
	addq	$8, %rbx
	call	CRYPTO_free@PLT
	cmpq	%r14, %rbx
	jne	.L766
	movq	8(%r12), %rdi
	movl	$922, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	168(%r12), %rdi
	movq	X509_NAME_free@GOTPCREL(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	popq	%rbx
	.cfi_restore 3
	movq	%r12, %rdi
	movl	$924, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L764:
	ret
	.cfi_endproc
.LFE1090:
	.size	SSL_CONF_CTX_free, .-SSL_CONF_CTX_free
	.p2align 4
	.globl	SSL_CONF_CTX_set_flags
	.type	SSL_CONF_CTX_set_flags, @function
SSL_CONF_CTX_set_flags:
.LFB1091:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	orl	%esi, %eax
	movl	%eax, (%rdi)
	ret
	.cfi_endproc
.LFE1091:
	.size	SSL_CONF_CTX_set_flags, .-SSL_CONF_CTX_set_flags
	.p2align 4
	.globl	SSL_CONF_CTX_clear_flags
	.type	SSL_CONF_CTX_clear_flags, @function
SSL_CONF_CTX_clear_flags:
.LFB1092:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	notl	%eax
	andl	(%rdi), %eax
	movl	%eax, (%rdi)
	ret
	.cfi_endproc
.LFE1092:
	.size	SSL_CONF_CTX_clear_flags, .-SSL_CONF_CTX_clear_flags
	.p2align 4
	.globl	SSL_CONF_CTX_set1_prefix
	.type	SSL_CONF_CTX_set1_prefix, @function
SSL_CONF_CTX_set1_prefix:
.LFB1093:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	je	.L778
	movq	%rsi, %rdi
	movl	$944, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L776
	movq	8(%rbx), %rdi
	movl	$948, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, 8(%rbx)
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%rax, 16(%rbx)
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L778:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	movl	$948, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 8(%rbx)
	movl	$1, %eax
	movq	$0, 16(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L776:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1093:
	.size	SSL_CONF_CTX_set1_prefix, .-SSL_CONF_CTX_set1_prefix
	.p2align 4
	.globl	SSL_CONF_CTX_set_ssl
	.type	SSL_CONF_CTX_set_ssl, @function
SSL_CONF_CTX_set_ssl:
.LFB1094:
	.cfi_startproc
	endbr64
	movq	%rsi, 32(%rdi)
	movq	$0, 24(%rdi)
	testq	%rsi, %rsi
	je	.L780
	leaq	1492(%rsi), %rax
	movq	%rax, 40(%rdi)
	movq	1168(%rsi), %rax
	addq	$28, %rax
	movq	%rax, %xmm0
	leaq	1376(%rsi), %rax
	movq	%rax, %xmm1
	leaq	1500(%rsi), %rax
	addq	$1504, %rsi
	punpcklqdq	%xmm1, %xmm0
	movq	%rsi, %xmm2
	movups	%xmm0, 120(%rdi)
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 136(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L780:
	pxor	%xmm0, %xmm0
	movq	$0, 40(%rdi)
	movups	%xmm0, 120(%rdi)
	movups	%xmm0, 136(%rdi)
	ret
	.cfi_endproc
.LFE1094:
	.size	SSL_CONF_CTX_set_ssl, .-SSL_CONF_CTX_set_ssl
	.p2align 4
	.globl	SSL_CONF_CTX_set_ssl_ctx
	.type	SSL_CONF_CTX_set_ssl_ctx, @function
SSL_CONF_CTX_set_ssl_ctx:
.LFB1095:
	.cfi_startproc
	endbr64
	movq	%rsi, 24(%rdi)
	movq	$0, 32(%rdi)
	testq	%rsi, %rsi
	je	.L783
	leaq	296(%rsi), %rax
	movq	%rax, 40(%rdi)
	movq	320(%rsi), %rax
	addq	$28, %rax
	movq	%rax, %xmm0
	leaq	352(%rsi), %rax
	movq	%rax, %xmm1
	leaq	304(%rsi), %rax
	addq	$308, %rsi
	punpcklqdq	%xmm1, %xmm0
	movq	%rsi, %xmm2
	movups	%xmm0, 120(%rdi)
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 136(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L783:
	pxor	%xmm0, %xmm0
	movq	$0, 40(%rdi)
	movups	%xmm0, 120(%rdi)
	movups	%xmm0, 136(%rdi)
	ret
	.cfi_endproc
.LFE1095:
	.size	SSL_CONF_CTX_set_ssl_ctx, .-SSL_CONF_CTX_set_ssl_ctx
	.section	.rodata.str1.1
.LC14:
	.string	"ALL"
.LC15:
	.string	"SSLv2"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	ssl_protocol_list.23717, @object
	.size	ssl_protocol_list.23717, 216
ssl_protocol_list.23717:
	.quad	.LC14
	.long	3
	.long	13
	.quad	1040187392
	.quad	.LC15
	.long	5
	.long	13
	.quad	0
	.quad	.LC2
	.long	5
	.long	13
	.quad	33554432
	.quad	.LC3
	.long	5
	.long	13
	.quad	67108864
	.quad	.LC4
	.long	7
	.long	13
	.quad	268435456
	.quad	.LC5
	.long	7
	.long	13
	.quad	134217728
	.quad	.LC6
	.long	7
	.long	13
	.quad	536870912
	.quad	.LC7
	.long	6
	.long	13
	.quad	67108864
	.quad	.LC8
	.long	8
	.long	13
	.quad	134217728
	.align 32
	.type	versions.23724, @object
	.size	versions.23724, 128
versions.23724:
	.quad	.LC1
	.long	0
	.zero	4
	.quad	.LC2
	.long	768
	.zero	4
	.quad	.LC3
	.long	769
	.zero	4
	.quad	.LC4
	.long	770
	.zero	4
	.quad	.LC5
	.long	771
	.zero	4
	.quad	.LC6
	.long	772
	.zero	4
	.quad	.LC7
	.long	65279
	.zero	4
	.quad	.LC8
	.long	65277
	.zero	4
	.section	.rodata.str1.1
.LC16:
	.string	"SessionTicket"
.LC17:
	.string	"EmptyFragments"
.LC18:
	.string	"Bugs"
.LC19:
	.string	"Compression"
.LC20:
	.string	"ServerPreference"
.LC21:
	.string	"NoResumptionOnRenegotiation"
.LC22:
	.string	"DHSingle"
.LC23:
	.string	"ECDHSingle"
.LC24:
	.string	"UnsafeLegacyRenegotiation"
.LC25:
	.string	"EncryptThenMac"
.LC26:
	.string	"NoRenegotiation"
.LC27:
	.string	"AllowNoDHEKEX"
.LC28:
	.string	"PrioritizeChaCha"
.LC29:
	.string	"MiddleboxCompat"
.LC30:
	.string	"AntiReplay"
	.section	.data.rel.ro.local
	.align 32
	.type	ssl_option_list.23749, @object
	.size	ssl_option_list.23749, 360
ssl_option_list.23749:
	.quad	.LC16
	.long	13
	.long	13
	.quad	16384
	.quad	.LC17
	.long	14
	.long	13
	.quad	2048
	.quad	.LC18
	.long	4
	.long	12
	.quad	2147485780
	.quad	.LC19
	.long	11
	.long	13
	.quad	131072
	.quad	.LC20
	.long	16
	.long	8
	.quad	4194304
	.quad	.LC21
	.long	27
	.long	8
	.quad	65536
	.quad	.LC22
	.long	8
	.long	8
	.quad	0
	.quad	.LC23
	.long	10
	.long	8
	.quad	0
	.quad	.LC24
	.long	25
	.long	12
	.quad	262144
	.quad	.LC25
	.long	14
	.long	13
	.quad	524288
	.quad	.LC26
	.long	15
	.long	12
	.quad	1073741824
	.quad	.LC27
	.long	13
	.long	12
	.quad	1024
	.quad	.LC28
	.long	16
	.long	12
	.quad	2097152
	.quad	.LC29
	.long	15
	.long	12
	.quad	1048576
	.quad	.LC30
	.long	10
	.long	13
	.quad	16777216
	.section	.rodata.str1.1
.LC31:
	.string	"Peer"
.LC32:
	.string	"Request"
.LC33:
	.string	"Require"
.LC34:
	.string	"Once"
.LC35:
	.string	"RequestPostHandshake"
.LC36:
	.string	"RequirePostHandshake"
	.section	.data.rel.ro.local
	.align 32
	.type	ssl_vfy_list.23754, @object
	.size	ssl_vfy_list.23754, 144
ssl_vfy_list.23754:
	.quad	.LC31
	.long	4
	.long	516
	.quad	1
	.quad	.LC32
	.long	7
	.long	520
	.quad	1
	.quad	.LC33
	.long	7
	.long	520
	.quad	3
	.quad	.LC34
	.long	4
	.long	520
	.quad	5
	.quad	.LC35
	.long	20
	.long	520
	.quad	9
	.quad	.LC36
	.long	20
	.long	520
	.quad	11
	.section	.rodata
	.align 32
	.type	ssl_cmd_switches, @object
	.size	ssl_cmd_switches, 352
ssl_cmd_switches:
	.quad	33554432
	.long	0
	.zero	4
	.quad	67108864
	.long	0
	.zero	4
	.quad	268435456
	.long	0
	.zero	4
	.quad	134217728
	.long	0
	.zero	4
	.quad	536870912
	.long	0
	.zero	4
	.quad	2147485780
	.long	0
	.zero	4
	.quad	131072
	.long	0
	.zero	4
	.quad	131072
	.long	1
	.zero	4
	.quad	0
	.long	0
	.zero	4
	.quad	16384
	.long	0
	.zero	4
	.quad	4194304
	.long	0
	.zero	4
	.quad	262144
	.long	0
	.zero	4
	.quad	4
	.long	0
	.zero	4
	.quad	1073741824
	.long	0
	.zero	4
	.quad	65536
	.long	0
	.zero	4
	.quad	4
	.long	1
	.zero	4
	.quad	1024
	.long	0
	.zero	4
	.quad	2097152
	.long	0
	.zero	4
	.quad	1
	.long	256
	.zero	4
	.quad	1048576
	.long	1
	.zero	4
	.quad	16777216
	.long	1
	.zero	4
	.quad	16777216
	.long	0
	.zero	4
	.section	.rodata.str1.1
.LC37:
	.string	"no_ssl3"
.LC38:
	.string	"no_tls1"
.LC39:
	.string	"no_tls1_1"
.LC40:
	.string	"no_tls1_2"
.LC41:
	.string	"no_tls1_3"
.LC42:
	.string	"bugs"
.LC43:
	.string	"no_comp"
.LC44:
	.string	"comp"
.LC45:
	.string	"ecdh_single"
.LC46:
	.string	"no_ticket"
.LC47:
	.string	"serverpref"
.LC48:
	.string	"legacy_renegotiation"
.LC49:
	.string	"legacy_server_connect"
.LC50:
	.string	"no_renegotiation"
.LC51:
	.string	"no_resumption_on_reneg"
.LC52:
	.string	"no_legacy_server_connect"
.LC53:
	.string	"allow_no_dhe_kex"
.LC54:
	.string	"prioritize_chacha"
.LC55:
	.string	"strict"
.LC56:
	.string	"no_middlebox"
.LC57:
	.string	"anti_replay"
.LC58:
	.string	"no_anti_replay"
.LC59:
	.string	"SignatureAlgorithms"
.LC60:
	.string	"sigalgs"
.LC61:
	.string	"ClientSignatureAlgorithms"
.LC62:
	.string	"client_sigalgs"
.LC63:
	.string	"Curves"
.LC64:
	.string	"curves"
.LC65:
	.string	"Groups"
.LC66:
	.string	"groups"
.LC67:
	.string	"ECDHParameters"
.LC68:
	.string	"named_curve"
.LC69:
	.string	"CipherString"
.LC70:
	.string	"cipher"
.LC71:
	.string	"Ciphersuites"
.LC72:
	.string	"ciphersuites"
.LC73:
	.string	"Protocol"
.LC74:
	.string	"MinProtocol"
.LC75:
	.string	"min_protocol"
.LC76:
	.string	"MaxProtocol"
.LC77:
	.string	"max_protocol"
.LC78:
	.string	"Options"
.LC79:
	.string	"VerifyMode"
.LC80:
	.string	"Certificate"
.LC81:
	.string	"cert"
.LC82:
	.string	"PrivateKey"
.LC83:
	.string	"key"
.LC84:
	.string	"ServerInfoFile"
.LC85:
	.string	"ChainCAPath"
.LC86:
	.string	"chainCApath"
.LC87:
	.string	"ChainCAFile"
.LC88:
	.string	"chainCAfile"
.LC89:
	.string	"VerifyCAPath"
.LC90:
	.string	"verifyCApath"
.LC91:
	.string	"VerifyCAFile"
.LC92:
	.string	"verifyCAfile"
.LC93:
	.string	"RequestCAFile"
.LC94:
	.string	"requestCAFile"
.LC95:
	.string	"ClientCAFile"
.LC96:
	.string	"RequestCAPath"
.LC97:
	.string	"ClientCAPath"
.LC98:
	.string	"DHParameters"
.LC99:
	.string	"dhparam"
.LC100:
	.string	"RecordPadding"
.LC101:
	.string	"record_padding"
.LC102:
	.string	"NumTickets"
.LC103:
	.string	"num_tickets"
	.section	.data.rel.ro.local
	.align 32
	.type	ssl_conf_cmds, @object
	.size	ssl_conf_cmds, 1536
ssl_conf_cmds:
	.quad	0
	.quad	0
	.quad	.LC37
	.value	0
	.value	4
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC38
	.value	0
	.value	4
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC39
	.value	0
	.value	4
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC40
	.value	0
	.value	4
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC41
	.value	0
	.value	4
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC42
	.value	0
	.value	4
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC43
	.value	0
	.value	4
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC44
	.value	0
	.value	4
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC45
	.value	8
	.value	4
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC46
	.value	0
	.value	4
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC47
	.value	8
	.value	4
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC48
	.value	0
	.value	4
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC49
	.value	8
	.value	4
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC50
	.value	0
	.value	4
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC51
	.value	8
	.value	4
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC52
	.value	8
	.value	4
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC53
	.value	0
	.value	4
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC54
	.value	8
	.value	4
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC55
	.value	0
	.value	4
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC56
	.value	0
	.value	4
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC57
	.value	8
	.value	4
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.value	8
	.value	4
	.zero	4
	.quad	cmd_SignatureAlgorithms
	.quad	.LC59
	.quad	.LC60
	.value	0
	.value	1
	.zero	4
	.quad	cmd_ClientSignatureAlgorithms
	.quad	.LC61
	.quad	.LC62
	.value	0
	.value	1
	.zero	4
	.quad	cmd_Curves
	.quad	.LC63
	.quad	.LC64
	.value	0
	.value	1
	.zero	4
	.quad	cmd_Groups
	.quad	.LC65
	.quad	.LC66
	.value	0
	.value	1
	.zero	4
	.quad	cmd_ECDHParameters
	.quad	.LC67
	.quad	.LC68
	.value	8
	.value	1
	.zero	4
	.quad	cmd_CipherString
	.quad	.LC69
	.quad	.LC70
	.value	0
	.value	1
	.zero	4
	.quad	cmd_Ciphersuites
	.quad	.LC71
	.quad	.LC72
	.value	0
	.value	1
	.zero	4
	.quad	cmd_Protocol
	.quad	.LC73
	.quad	0
	.value	0
	.value	1
	.zero	4
	.quad	cmd_MinProtocol
	.quad	.LC74
	.quad	.LC75
	.value	0
	.value	1
	.zero	4
	.quad	cmd_MaxProtocol
	.quad	.LC76
	.quad	.LC77
	.value	0
	.value	1
	.zero	4
	.quad	cmd_Options
	.quad	.LC78
	.quad	0
	.value	0
	.value	1
	.zero	4
	.quad	cmd_VerifyMode
	.quad	.LC79
	.quad	0
	.value	0
	.value	1
	.zero	4
	.quad	cmd_Certificate
	.quad	.LC80
	.quad	.LC81
	.value	32
	.value	2
	.zero	4
	.quad	cmd_PrivateKey
	.quad	.LC82
	.quad	.LC83
	.value	32
	.value	2
	.zero	4
	.quad	cmd_ServerInfoFile
	.quad	.LC84
	.quad	0
	.value	40
	.value	2
	.zero	4
	.quad	cmd_ChainCAPath
	.quad	.LC85
	.quad	.LC86
	.value	32
	.value	3
	.zero	4
	.quad	cmd_ChainCAFile
	.quad	.LC87
	.quad	.LC88
	.value	32
	.value	2
	.zero	4
	.quad	cmd_VerifyCAPath
	.quad	.LC89
	.quad	.LC90
	.value	32
	.value	3
	.zero	4
	.quad	cmd_VerifyCAFile
	.quad	.LC91
	.quad	.LC92
	.value	32
	.value	2
	.zero	4
	.quad	cmd_RequestCAFile
	.quad	.LC93
	.quad	.LC94
	.value	32
	.value	2
	.zero	4
	.quad	cmd_ClientCAFile
	.quad	.LC95
	.quad	0
	.value	40
	.value	2
	.zero	4
	.quad	cmd_RequestCAPath
	.quad	.LC96
	.quad	0
	.value	32
	.value	3
	.zero	4
	.quad	cmd_ClientCAPath
	.quad	.LC97
	.quad	0
	.value	40
	.value	3
	.zero	4
	.quad	cmd_DHParameters
	.quad	.LC98
	.quad	.LC99
	.value	40
	.value	2
	.zero	4
	.quad	cmd_RecordPadding
	.quad	.LC100
	.quad	.LC101
	.value	0
	.value	1
	.zero	4
	.quad	cmd_NumTickets
	.quad	.LC102
	.quad	.LC103
	.value	8
	.value	1
	.zero	4
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
