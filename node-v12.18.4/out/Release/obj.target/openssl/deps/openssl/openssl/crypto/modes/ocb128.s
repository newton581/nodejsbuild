	.file	"ocb128.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/modes/ocb128.c"
	.text
	.p2align 4
	.type	ocb_lookup_l, @function
ocb_lookup_l:
.LFB218:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	40(%rdi), %r13
	movq	88(%rdi), %rax
	cmpq	%rsi, %r13
	jnb	.L12
	movq	48(%rdi), %rdx
	movq	%rdi, %rbx
	cmpq	%rsi, %rdx
	jbe	.L13
.L4:
	leaq	1(%r13), %r9
	movq	%r9, %r8
	salq	$4, %r8
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L14:
	movq	88(%rbx), %rax
	addq	$1, %r9
.L6:
	leaq	(%rax,%r8), %rdx
	leaq	-16(%rax,%r8), %rax
	movzbl	15(%rax), %edi
	movzbl	14(%rax), %esi
	movzbl	(%rax), %ecx
	leal	(%rsi,%rsi), %r11d
	leal	(%rdi,%rdi), %r10d
	sarl	$7, %edi
	sarl	$7, %esi
	orl	%r11d, %edi
	sarb	$7, %cl
	movb	%dil, 14(%rdx)
	movzbl	13(%rax), %edi
	andl	$-121, %ecx
	leal	(%rdi,%rdi), %r11d
	sarl	$7, %edi
	orl	%r11d, %esi
	movb	%sil, 13(%rdx)
	movzbl	12(%rax), %esi
	leal	(%rsi,%rsi), %r11d
	sarl	$7, %esi
	orl	%r11d, %edi
	movb	%dil, 12(%rdx)
	movzbl	11(%rax), %edi
	leal	(%rdi,%rdi), %r11d
	sarl	$7, %edi
	orl	%r11d, %esi
	movb	%sil, 11(%rdx)
	movzbl	10(%rax), %esi
	leal	(%rsi,%rsi), %r11d
	sarl	$7, %esi
	orl	%r11d, %edi
	movb	%dil, 10(%rdx)
	movzbl	9(%rax), %edi
	leal	(%rdi,%rdi), %r11d
	sarl	$7, %edi
	orl	%r11d, %esi
	movb	%sil, 9(%rdx)
	movzbl	8(%rax), %esi
	leal	(%rsi,%rsi), %r11d
	sarl	$7, %esi
	orl	%r11d, %edi
	movb	%dil, 8(%rdx)
	movzbl	7(%rax), %edi
	leal	(%rdi,%rdi), %r11d
	sarl	$7, %edi
	orl	%r11d, %esi
	movb	%sil, 7(%rdx)
	movzbl	6(%rax), %esi
	leal	(%rsi,%rsi), %r11d
	sarl	$7, %esi
	orl	%r11d, %edi
	movb	%dil, 6(%rdx)
	movzbl	5(%rax), %edi
	leal	(%rdi,%rdi), %r11d
	sarl	$7, %edi
	orl	%r11d, %esi
	movb	%sil, 5(%rdx)
	movzbl	4(%rax), %esi
	leal	(%rsi,%rsi), %r11d
	sarl	$7, %esi
	orl	%r11d, %edi
	movb	%dil, 4(%rdx)
	movzbl	3(%rax), %edi
	leal	(%rdi,%rdi), %r11d
	sarl	$7, %edi
	orl	%r11d, %esi
	movb	%sil, 3(%rdx)
	movzbl	2(%rax), %esi
	leal	(%rsi,%rsi), %r11d
	sarl	$7, %esi
	orl	%r11d, %edi
	movb	%dil, 2(%rdx)
	movzbl	1(%rax), %edi
	leal	(%rdi,%rdi), %r11d
	sarl	$7, %edi
	orl	%r11d, %esi
	movb	%sil, 1(%rdx)
	movzbl	(%rax), %esi
	leal	(%rsi,%rsi), %eax
	orl	%edi, %eax
	xorl	%r10d, %ecx
	addq	$16, %r8
	movb	%al, (%rdx)
	movb	%cl, 15(%rdx)
	cmpq	%r12, %r9
	jne	.L14
	movq	88(%rbx), %rax
	movq	%r9, 40(%rbx)
	salq	$4, %r9
	addq	%r9, %rax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L13:
	.cfi_restore_state
	leaq	4(%rsi), %rsi
	movl	$113, %ecx
	subq	%rdx, %rsi
	andq	$-4, %rsi
	addq	%rdx, %rsi
	leaq	.LC0(%rip), %rdx
	movq	%rsi, 48(%rdi)
	salq	$4, %rsi
	movq	%rax, %rdi
	call	CRYPTO_realloc@PLT
	testq	%rax, %rax
	je	.L1
	movq	%rax, 88(%rbx)
	jmp	.L4
.L12:
	addq	$8, %rsp
	salq	$4, %r12
	popq	%rbx
	addq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE218:
	.size	ocb_lookup_l, .-ocb_lookup_l
	.p2align 4
	.globl	CRYPTO_ocb128_init
	.type	CRYPTO_ocb128_init, @function
CRYPTO_ocb128_init:
.LFB220:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	leaq	.LC0(%rip), %rsi
	pushq	%r13
	.cfi_offset 13, -32
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	addq	$8, %rdi
	movq	%rbx, %rcx
	subq	$16, %rsp
	movq	%rdx, -40(%rbp)
	movdqa	.LC1(%rip), %xmm0
	movl	$158, %edx
	movq	%r8, -48(%rbp)
	movq	$0, -8(%rdi)
	movq	$0, 160(%rdi)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$176, %ecx
	shrl	$3, %ecx
	rep stosq
	movups	%xmm0, 40(%rbx)
	movl	$80, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, 88(%rbx)
	testq	%rax, %rax
	je	.L19
	movq	%r12, %xmm0
	leaq	56(%rbx), %rdi
	movq	%r14, %rdx
	movq	%r13, 32(%rbx)
	movhps	-48(%rbp), %xmm0
	movq	%rdi, %rsi
	movups	%xmm0, (%rbx)
	movq	%r14, %xmm0
	movhps	-40(%rbp), %xmm0
	movups	%xmm0, 16(%rbx)
	call	*%r12
	movzbl	71(%rbx), %edx
	movzbl	56(%rbx), %ecx
	leal	(%rdx,%rdx), %eax
	sarl	$7, %edx
	sarb	$7, %cl
	movb	%al, 87(%rbx)
	movzbl	70(%rbx), %eax
	andl	$-121, %ecx
	leal	(%rax,%rax), %esi
	sarl	$7, %eax
	orl	%esi, %edx
	movb	%dl, 86(%rbx)
	movzbl	69(%rbx), %edx
	leal	(%rdx,%rdx), %esi
	sarl	$7, %edx
	orl	%esi, %eax
	movb	%al, 85(%rbx)
	movzbl	68(%rbx), %eax
	leal	(%rax,%rax), %esi
	sarl	$7, %eax
	orl	%esi, %edx
	movb	%dl, 84(%rbx)
	movzbl	67(%rbx), %edx
	leal	(%rdx,%rdx), %esi
	sarl	$7, %edx
	orl	%esi, %eax
	movb	%al, 83(%rbx)
	movzbl	66(%rbx), %eax
	leal	(%rax,%rax), %esi
	sarl	$7, %eax
	orl	%esi, %edx
	movb	%dl, 82(%rbx)
	movzbl	65(%rbx), %edx
	leal	(%rdx,%rdx), %esi
	sarl	$7, %edx
	orl	%esi, %eax
	movb	%al, 81(%rbx)
	movzbl	64(%rbx), %eax
	leal	(%rax,%rax), %esi
	sarl	$7, %eax
	orl	%esi, %edx
	movb	%dl, 80(%rbx)
	movzbl	63(%rbx), %edx
	leal	(%rdx,%rdx), %esi
	sarl	$7, %edx
	orl	%esi, %eax
	movb	%al, 79(%rbx)
	movzbl	62(%rbx), %eax
	leal	(%rax,%rax), %esi
	sarl	$7, %eax
	orl	%esi, %edx
	movb	%dl, 78(%rbx)
	movzbl	61(%rbx), %edx
	leal	(%rdx,%rdx), %esi
	sarl	$7, %edx
	orl	%esi, %eax
	movb	%al, 77(%rbx)
	movzbl	60(%rbx), %eax
	leal	(%rax,%rax), %esi
	sarl	$7, %eax
	orl	%esi, %edx
	movb	%dl, 76(%rbx)
	movzbl	59(%rbx), %edx
	leal	(%rdx,%rdx), %esi
	sarl	$7, %edx
	orl	%esi, %eax
	movb	%al, 75(%rbx)
	movzbl	58(%rbx), %eax
	leal	(%rax,%rax), %esi
	sarl	$7, %eax
	orl	%esi, %edx
	movzbl	57(%rbx), %esi
	movb	%dl, 74(%rbx)
	leal	(%rsi,%rsi), %edx
	sarl	$7, %esi
	orl	%edx, %eax
	movb	%al, 73(%rbx)
	movzbl	56(%rbx), %eax
	leal	(%rax,%rax), %edx
	orl	%esi, %edx
	movb	%dl, 72(%rbx)
	movq	88(%rbx), %rax
	sarb	$7, %dl
	xorb	%cl, 87(%rbx)
	movzbl	87(%rbx), %esi
	andl	$-121, %edx
	leal	(%rsi,%rsi), %ecx
	sarl	$7, %esi
	movb	%cl, 15(%rax)
	movzbl	86(%rbx), %ecx
	leal	(%rcx,%rcx), %edi
	sarl	$7, %ecx
	orl	%edi, %esi
	movb	%sil, 14(%rax)
	movzbl	85(%rbx), %esi
	leal	(%rsi,%rsi), %edi
	sarl	$7, %esi
	orl	%edi, %ecx
	movb	%cl, 13(%rax)
	movzbl	84(%rbx), %ecx
	leal	(%rcx,%rcx), %edi
	sarl	$7, %ecx
	orl	%edi, %esi
	movb	%sil, 12(%rax)
	movzbl	83(%rbx), %esi
	leal	(%rsi,%rsi), %edi
	sarl	$7, %esi
	orl	%edi, %ecx
	movb	%cl, 11(%rax)
	movzbl	82(%rbx), %ecx
	leal	(%rcx,%rcx), %edi
	sarl	$7, %ecx
	orl	%edi, %esi
	movb	%sil, 10(%rax)
	movzbl	81(%rbx), %esi
	leal	(%rsi,%rsi), %edi
	sarl	$7, %esi
	orl	%edi, %ecx
	movb	%cl, 9(%rax)
	movzbl	80(%rbx), %ecx
	leal	(%rcx,%rcx), %edi
	sarl	$7, %ecx
	orl	%edi, %esi
	movb	%sil, 8(%rax)
	movzbl	79(%rbx), %esi
	leal	(%rsi,%rsi), %edi
	sarl	$7, %esi
	orl	%edi, %ecx
	movb	%cl, 7(%rax)
	movzbl	78(%rbx), %ecx
	leal	(%rcx,%rcx), %edi
	sarl	$7, %ecx
	orl	%edi, %esi
	movb	%sil, 6(%rax)
	movzbl	77(%rbx), %esi
	leal	(%rsi,%rsi), %edi
	sarl	$7, %esi
	orl	%edi, %ecx
	movb	%cl, 5(%rax)
	movzbl	76(%rbx), %ecx
	leal	(%rcx,%rcx), %edi
	sarl	$7, %ecx
	orl	%edi, %esi
	movb	%sil, 4(%rax)
	movzbl	75(%rbx), %esi
	leal	(%rsi,%rsi), %edi
	sarl	$7, %esi
	orl	%edi, %ecx
	movb	%cl, 3(%rax)
	movzbl	74(%rbx), %ecx
	leal	(%rcx,%rcx), %edi
	sarl	$7, %ecx
	orl	%edi, %esi
	movb	%sil, 2(%rax)
	movzbl	73(%rbx), %edi
	leal	(%rdi,%rdi), %esi
	orl	%esi, %ecx
	sarl	$7, %edi
	movb	%cl, 1(%rax)
	movzbl	72(%rbx), %esi
	xorb	%dl, 15(%rax)
	leal	(%rsi,%rsi), %ecx
	orl	%edi, %ecx
	movb	%cl, (%rax)
	movq	88(%rbx), %rax
	movzbl	15(%rax), %esi
	movzbl	(%rax), %edx
	leal	(%rsi,%rsi), %ecx
	sarl	$7, %esi
	sarb	$7, %dl
	movb	%cl, 31(%rax)
	movzbl	14(%rax), %ecx
	andl	$-121, %edx
	leal	(%rcx,%rcx), %edi
	sarl	$7, %ecx
	orl	%edi, %esi
	movb	%sil, 30(%rax)
	movzbl	13(%rax), %esi
	leal	(%rsi,%rsi), %edi
	sarl	$7, %esi
	orl	%edi, %ecx
	movb	%cl, 29(%rax)
	movzbl	12(%rax), %ecx
	leal	(%rcx,%rcx), %edi
	sarl	$7, %ecx
	orl	%edi, %esi
	movb	%sil, 28(%rax)
	movzbl	11(%rax), %esi
	leal	(%rsi,%rsi), %edi
	sarl	$7, %esi
	orl	%edi, %ecx
	movb	%cl, 27(%rax)
	movzbl	10(%rax), %ecx
	leal	(%rcx,%rcx), %edi
	sarl	$7, %ecx
	orl	%edi, %esi
	movb	%sil, 26(%rax)
	movzbl	9(%rax), %esi
	leal	(%rsi,%rsi), %edi
	sarl	$7, %esi
	orl	%edi, %ecx
	movb	%cl, 25(%rax)
	movzbl	8(%rax), %ecx
	leal	(%rcx,%rcx), %edi
	sarl	$7, %ecx
	orl	%edi, %esi
	movb	%sil, 24(%rax)
	movzbl	7(%rax), %esi
	leal	(%rsi,%rsi), %edi
	sarl	$7, %esi
	orl	%edi, %ecx
	movb	%cl, 23(%rax)
	movzbl	6(%rax), %ecx
	leal	(%rcx,%rcx), %edi
	sarl	$7, %ecx
	orl	%edi, %esi
	movb	%sil, 22(%rax)
	movzbl	5(%rax), %esi
	leal	(%rsi,%rsi), %edi
	sarl	$7, %esi
	orl	%edi, %ecx
	movb	%cl, 21(%rax)
	movzbl	4(%rax), %ecx
	leal	(%rcx,%rcx), %edi
	sarl	$7, %ecx
	orl	%edi, %esi
	movb	%sil, 20(%rax)
	movzbl	3(%rax), %esi
	leal	(%rsi,%rsi), %edi
	sarl	$7, %esi
	orl	%edi, %ecx
	movb	%cl, 19(%rax)
	movzbl	2(%rax), %ecx
	leal	(%rcx,%rcx), %edi
	sarl	$7, %ecx
	orl	%edi, %esi
	movzbl	1(%rax), %edi
	movb	%sil, 18(%rax)
	leal	(%rdi,%rdi), %esi
	orl	%esi, %ecx
	movzbl	(%rax), %esi
	sarl	$7, %edi
	xorb	%dl, 31(%rax)
	movb	%cl, 17(%rax)
	leal	(%rsi,%rsi), %ecx
	orl	%edi, %ecx
	movb	%cl, 16(%rax)
	movq	88(%rbx), %rax
	movzbl	31(%rax), %esi
	movzbl	16(%rax), %edx
	leal	(%rsi,%rsi), %ecx
	sarl	$7, %esi
	sarb	$7, %dl
	movb	%cl, 47(%rax)
	movzbl	30(%rax), %ecx
	andl	$-121, %edx
	leal	(%rcx,%rcx), %edi
	sarl	$7, %ecx
	orl	%edi, %esi
	movb	%sil, 46(%rax)
	movzbl	29(%rax), %esi
	leal	(%rsi,%rsi), %edi
	sarl	$7, %esi
	orl	%edi, %ecx
	movb	%cl, 45(%rax)
	movzbl	28(%rax), %ecx
	leal	(%rcx,%rcx), %edi
	sarl	$7, %ecx
	orl	%edi, %esi
	movb	%sil, 44(%rax)
	movzbl	27(%rax), %esi
	leal	(%rsi,%rsi), %edi
	sarl	$7, %esi
	orl	%edi, %ecx
	movb	%cl, 43(%rax)
	movzbl	26(%rax), %ecx
	leal	(%rcx,%rcx), %edi
	sarl	$7, %ecx
	orl	%edi, %esi
	movb	%sil, 42(%rax)
	movzbl	25(%rax), %esi
	leal	(%rsi,%rsi), %edi
	sarl	$7, %esi
	orl	%edi, %ecx
	movb	%cl, 41(%rax)
	movzbl	24(%rax), %ecx
	leal	(%rcx,%rcx), %edi
	sarl	$7, %ecx
	orl	%edi, %esi
	movb	%sil, 40(%rax)
	movzbl	23(%rax), %esi
	leal	(%rsi,%rsi), %edi
	sarl	$7, %esi
	orl	%edi, %ecx
	movb	%cl, 39(%rax)
	movzbl	22(%rax), %ecx
	leal	(%rcx,%rcx), %edi
	sarl	$7, %ecx
	orl	%edi, %esi
	movb	%sil, 38(%rax)
	movzbl	21(%rax), %esi
	leal	(%rsi,%rsi), %edi
	sarl	$7, %esi
	orl	%edi, %ecx
	movb	%cl, 37(%rax)
	movzbl	20(%rax), %ecx
	leal	(%rcx,%rcx), %edi
	sarl	$7, %ecx
	orl	%edi, %esi
	movb	%sil, 36(%rax)
	movzbl	19(%rax), %esi
	leal	(%rsi,%rsi), %edi
	sarl	$7, %esi
	orl	%edi, %ecx
	movb	%cl, 35(%rax)
	movzbl	18(%rax), %ecx
	leal	(%rcx,%rcx), %edi
	orl	%edi, %esi
	movzbl	17(%rax), %edi
	sarl	$7, %ecx
	xorb	%dl, 47(%rax)
	movb	%sil, 34(%rax)
	leal	(%rdi,%rdi), %esi
	sarl	$7, %edi
	orl	%esi, %ecx
	movzbl	16(%rax), %esi
	movb	%cl, 33(%rax)
	leal	(%rsi,%rsi), %ecx
	orl	%edi, %ecx
	movb	%cl, 32(%rax)
	movq	88(%rbx), %rax
	movzbl	47(%rax), %esi
	movzbl	32(%rax), %edx
	leal	(%rsi,%rsi), %ecx
	sarl	$7, %esi
	sarb	$7, %dl
	movb	%cl, 63(%rax)
	movzbl	46(%rax), %ecx
	andl	$-121, %edx
	leal	(%rcx,%rcx), %edi
	sarl	$7, %ecx
	orl	%edi, %esi
	movb	%sil, 62(%rax)
	movzbl	45(%rax), %esi
	leal	(%rsi,%rsi), %edi
	sarl	$7, %esi
	orl	%edi, %ecx
	movb	%cl, 61(%rax)
	movzbl	44(%rax), %ecx
	leal	(%rcx,%rcx), %edi
	sarl	$7, %ecx
	orl	%edi, %esi
	movb	%sil, 60(%rax)
	movzbl	43(%rax), %esi
	leal	(%rsi,%rsi), %edi
	sarl	$7, %esi
	orl	%edi, %ecx
	movb	%cl, 59(%rax)
	movzbl	42(%rax), %ecx
	leal	(%rcx,%rcx), %edi
	sarl	$7, %ecx
	orl	%edi, %esi
	movb	%sil, 58(%rax)
	movzbl	41(%rax), %esi
	leal	(%rsi,%rsi), %edi
	sarl	$7, %esi
	orl	%edi, %ecx
	movb	%cl, 57(%rax)
	movzbl	40(%rax), %ecx
	leal	(%rcx,%rcx), %edi
	sarl	$7, %ecx
	orl	%edi, %esi
	movb	%sil, 56(%rax)
	movzbl	39(%rax), %esi
	leal	(%rsi,%rsi), %edi
	sarl	$7, %esi
	orl	%edi, %ecx
	movb	%cl, 55(%rax)
	movzbl	38(%rax), %ecx
	leal	(%rcx,%rcx), %edi
	sarl	$7, %ecx
	orl	%edi, %esi
	movb	%sil, 54(%rax)
	movzbl	37(%rax), %esi
	leal	(%rsi,%rsi), %edi
	sarl	$7, %esi
	orl	%edi, %ecx
	movb	%cl, 53(%rax)
	movzbl	36(%rax), %ecx
	leal	(%rcx,%rcx), %edi
	sarl	$7, %ecx
	orl	%edi, %esi
	movb	%sil, 52(%rax)
	movzbl	35(%rax), %esi
	leal	(%rsi,%rsi), %edi
	sarl	$7, %esi
	orl	%edi, %ecx
	movb	%cl, 51(%rax)
	movzbl	34(%rax), %ecx
	leal	(%rcx,%rcx), %edi
	orl	%edi, %esi
	movzbl	33(%rax), %edi
	sarl	$7, %ecx
	xorb	%dl, 63(%rax)
	movb	%sil, 50(%rax)
	leal	(%rdi,%rdi), %esi
	sarl	$7, %edi
	orl	%esi, %ecx
	movzbl	32(%rax), %esi
	movb	%cl, 49(%rax)
	leal	(%rsi,%rsi), %ecx
	orl	%edi, %ecx
	movb	%cl, 48(%rax)
	movq	88(%rbx), %rax
	movzbl	63(%rax), %ecx
	movzbl	48(%rax), %esi
	leal	(%rcx,%rcx), %edx
	sarl	$7, %ecx
	sarb	$7, %sil
	movb	%dl, 79(%rax)
	movzbl	62(%rax), %edx
	andl	$-121, %esi
	leal	(%rdx,%rdx), %edi
	sarl	$7, %edx
	orl	%edi, %ecx
	movb	%cl, 78(%rax)
	movzbl	61(%rax), %ecx
	leal	(%rcx,%rcx), %edi
	sarl	$7, %ecx
	orl	%edi, %edx
	movb	%dl, 77(%rax)
	movzbl	60(%rax), %edx
	leal	(%rdx,%rdx), %edi
	sarl	$7, %edx
	orl	%edi, %ecx
	movb	%cl, 76(%rax)
	movzbl	59(%rax), %ecx
	leal	(%rcx,%rcx), %edi
	sarl	$7, %ecx
	orl	%edi, %edx
	movb	%dl, 75(%rax)
	movzbl	58(%rax), %edx
	leal	(%rdx,%rdx), %edi
	sarl	$7, %edx
	orl	%edi, %ecx
	movb	%cl, 74(%rax)
	movzbl	57(%rax), %ecx
	leal	(%rcx,%rcx), %edi
	sarl	$7, %ecx
	orl	%edi, %edx
	movb	%dl, 73(%rax)
	movzbl	56(%rax), %edx
	leal	(%rdx,%rdx), %edi
	sarl	$7, %edx
	orl	%edi, %ecx
	movb	%cl, 72(%rax)
	movzbl	55(%rax), %ecx
	leal	(%rcx,%rcx), %edi
	sarl	$7, %ecx
	orl	%edi, %edx
	movb	%dl, 71(%rax)
	movzbl	54(%rax), %edx
	leal	(%rdx,%rdx), %edi
	sarl	$7, %edx
	orl	%edi, %ecx
	movb	%cl, 70(%rax)
	movzbl	53(%rax), %ecx
	leal	(%rcx,%rcx), %edi
	sarl	$7, %ecx
	orl	%edi, %edx
	movb	%dl, 69(%rax)
	movzbl	52(%rax), %edx
	leal	(%rdx,%rdx), %edi
	sarl	$7, %edx
	orl	%edi, %ecx
	movb	%cl, 68(%rax)
	movzbl	51(%rax), %ecx
	leal	(%rcx,%rcx), %edi
	orl	%edi, %edx
	sarl	$7, %ecx
	xorb	%sil, 79(%rax)
	movb	%dl, 67(%rax)
	movzbl	50(%rax), %edx
	leal	(%rdx,%rdx), %edi
	sarl	$7, %edx
	orl	%edi, %ecx
	movzbl	49(%rax), %edi
	movb	%cl, 66(%rax)
	leal	(%rdi,%rdi), %ecx
	sarl	$7, %edi
	orl	%ecx, %edx
	movzbl	48(%rax), %ecx
	movb	%dl, 65(%rax)
	leal	(%rcx,%rcx), %edx
	orl	%edi, %edx
	movb	%dl, 64(%rax)
	movl	$1, %eax
	movq	$4, 40(%rbx)
.L15:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L19:
	.cfi_restore_state
	movl	$159, %r8d
	movl	$65, %edx
	movl	$122, %esi
	movl	$15, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L15
	.cfi_endproc
.LFE220:
	.size	CRYPTO_ocb128_init, .-CRYPTO_ocb128_init
	.p2align 4
	.globl	CRYPTO_ocb128_new
	.type	CRYPTO_ocb128_new, @function
CRYPTO_ocb128_new:
.LFB219:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movl	$137, %edx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	leaq	.LC0(%rip), %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$176, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	movq	%r8, -56(%rbp)
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L20
	movq	-56(%rbp), %r9
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	CRYPTO_ocb128_init
	testl	%eax, %eax
	je	.L26
.L20:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$142, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%r12d, %r12d
	call	CRYPTO_free@PLT
	jmp	.L20
	.cfi_endproc
.LFE219:
	.size	CRYPTO_ocb128_new, .-CRYPTO_ocb128_new
	.p2align 4
	.globl	CRYPTO_ocb128_copy_ctx
	.type	CRYPTO_ocb128_copy_ctx, @function
CRYPTO_ocb128_copy_ctx:
.LFB221:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movdqu	(%rsi), %xmm0
	movups	%xmm0, (%rdi)
	movdqu	16(%rsi), %xmm1
	movups	%xmm1, 16(%rdi)
	movdqu	32(%rsi), %xmm2
	movups	%xmm2, 32(%rdi)
	movdqu	48(%rsi), %xmm3
	movups	%xmm3, 48(%rdi)
	movdqu	64(%rsi), %xmm4
	movups	%xmm4, 64(%rdi)
	movdqu	80(%rsi), %xmm5
	movups	%xmm5, 80(%rdi)
	movdqu	96(%rsi), %xmm6
	movups	%xmm6, 96(%rdi)
	movdqu	112(%rsi), %xmm7
	movups	%xmm7, 112(%rdi)
	movdqu	128(%rsi), %xmm0
	movups	%xmm0, 128(%rdi)
	movdqu	144(%rsi), %xmm1
	movups	%xmm1, 144(%rdi)
	movdqu	160(%rsi), %xmm2
	movups	%xmm2, 160(%rdi)
	testq	%rdx, %rdx
	je	.L28
	movq	%rdx, 16(%rdi)
.L28:
	testq	%rcx, %rcx
	je	.L29
	movq	%rcx, 24(%r12)
.L29:
	cmpq	$0, 88(%rbx)
	movl	$1, %r13d
	je	.L27
	movq	48(%rbx), %rdi
	movl	$205, %edx
	leaq	.LC0(%rip), %rsi
	salq	$4, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, 88(%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L40
	movq	40(%rbx), %rax
	movq	88(%rbx), %rsi
	leaq	1(%rax), %rdx
	salq	$4, %rdx
	call	memcpy@PLT
.L27:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	movl	$206, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$121, %esi
	movl	$15, %edi
	call	ERR_put_error@PLT
	jmp	.L27
	.cfi_endproc
.LFE221:
	.size	CRYPTO_ocb128_copy_ctx, .-CRYPTO_ocb128_copy_ctx
	.p2align 4
	.globl	CRYPTO_ocb128_setiv
	.type	CRYPTO_ocb128_setiv, @function
CRYPTO_ocb128_setiv:
.LFB222:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$1, %rdx
	pushq	%rbx
	subq	$80, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$-1, %eax
	cmpq	$14, %rdx
	ja	.L41
	leaq	-1(%rcx), %rdx
	cmpq	$15, %rdx
	jbe	.L47
.L41:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L48
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	movq	%rdi, %rbx
	sall	$4, %ecx
	movw	%ax, -51(%rbp)
	movl	$16, %eax
	movq	%r12, %rdx
	subq	%r12, %rax
	movups	%xmm0, 96(%rdi)
	movups	%xmm0, 112(%rdi)
	movups	%xmm0, 128(%rdi)
	movups	%xmm0, 144(%rdi)
	movups	%xmm0, 160(%rdi)
	leaq	-64(%rbp,%rax), %rdi
	movb	%cl, -64(%rbp)
	movq	$0, -63(%rbp)
	movl	$0, -55(%rbp)
	movb	$0, -49(%rbp)
	call	memcpy@PLT
	movl	$15, %eax
	leaq	-96(%rbp), %rsi
	leaq	-80(%rbp), %rdi
	subq	%r12, %rax
	orb	$1, -64(%rbp,%rax)
	movq	-56(%rbp), %rax
	movq	-64(%rbp), %rdx
	movq	%rax, -72(%rbp)
	shrq	$56, %rax
	andl	$-64, %eax
	movq	%rdx, -80(%rbp)
	movq	16(%rbx), %rdx
	movb	%al, -65(%rbp)
	call	*(%rbx)
	movzwl	-96(%rbp), %eax
	xorw	-95(%rbp), %ax
	movl	$8, %ecx
	movzbl	-94(%rbp), %edx
	movdqa	-96(%rbp), %xmm1
	movw	%ax, -32(%rbp)
	movzbl	-93(%rbp), %eax
	movaps	%xmm1, -48(%rbp)
	xorl	%eax, %edx
	xorb	-92(%rbp), %al
	movb	%al, -29(%rbp)
	movzbl	-92(%rbp), %eax
	xorb	-91(%rbp), %al
	movb	%dl, -30(%rbp)
	movb	%al, -28(%rbp)
	movzbl	-91(%rbp), %eax
	xorb	-90(%rbp), %al
	movzbl	-90(%rbp), %edx
	movb	%al, -27(%rbp)
	movzbl	-89(%rbp), %eax
	xorl	%eax, %edx
	xorb	-88(%rbp), %al
	movb	%al, -25(%rbp)
	movzbl	-49(%rbp), %eax
	movb	%dl, -26(%rbp)
	movq	%rax, %rdx
	movl	%eax, %esi
	andl	$7, %eax
	andl	$63, %edx
	andl	$7, %esi
	subl	%eax, %ecx
	shrq	$3, %rdx
	movl	%ecx, %eax
	movl	%esi, %ecx
	movzbl	-33(%rbp,%rdx), %edi
	movzbl	-34(%rbp,%rdx), %r9d
	movzbl	-35(%rbp,%rdx), %r8d
	movl	%edi, %r10d
	movl	%r9d, %r11d
	sall	%cl, %r10d
	movl	%eax, %ecx
	sarl	%cl, %edi
	movl	%esi, %ecx
	movb	%r10b, 159(%rbx)
	sall	%cl, %r11d
	orl	%r11d, %edi
	movb	%dil, 158(%rbx)
	movl	%r8d, %edi
	sall	%cl, %edi
	movl	%eax, %ecx
	sarl	%cl, %r9d
	movl	%esi, %ecx
	orl	%edi, %r9d
	movzbl	-36(%rbp,%rdx), %edi
	movb	%r9b, 157(%rbx)
	movl	%edi, %r9d
	sall	%cl, %r9d
	movl	%eax, %ecx
	sarl	%cl, %r8d
	movl	%esi, %ecx
	orl	%r9d, %r8d
	movb	%r8b, 156(%rbx)
	movzbl	-37(%rbp,%rdx), %r8d
	movl	%r8d, %r9d
	sall	%cl, %r9d
	movl	%eax, %ecx
	sarl	%cl, %edi
	movl	%esi, %ecx
	orl	%r9d, %edi
	movb	%dil, 155(%rbx)
	movzbl	-38(%rbp,%rdx), %edi
	movl	%edi, %r9d
	sall	%cl, %r9d
	movl	%eax, %ecx
	sarl	%cl, %r8d
	movl	%esi, %ecx
	orl	%r9d, %r8d
	movb	%r8b, 154(%rbx)
	movzbl	-39(%rbp,%rdx), %r8d
	movl	%r8d, %r9d
	sall	%cl, %r9d
	movl	%eax, %ecx
	sarl	%cl, %edi
	movl	%esi, %ecx
	orl	%r9d, %edi
	movb	%dil, 153(%rbx)
	movzbl	-40(%rbp,%rdx), %edi
	movl	%edi, %r9d
	sall	%cl, %r9d
	movl	%eax, %ecx
	sarl	%cl, %r8d
	movl	%esi, %ecx
	orl	%r9d, %r8d
	movb	%r8b, 152(%rbx)
	movzbl	-41(%rbp,%rdx), %r8d
	movl	%r8d, %r9d
	sall	%cl, %r9d
	movl	%eax, %ecx
	sarl	%cl, %edi
	movl	%esi, %ecx
	orl	%r9d, %edi
	movb	%dil, 151(%rbx)
	movzbl	-42(%rbp,%rdx), %edi
	movl	%edi, %r9d
	sall	%cl, %r9d
	movl	%eax, %ecx
	sarl	%cl, %r8d
	movl	%esi, %ecx
	orl	%r9d, %r8d
	movb	%r8b, 150(%rbx)
	movzbl	-43(%rbp,%rdx), %r8d
	movl	%r8d, %r9d
	sall	%cl, %r9d
	movl	%eax, %ecx
	sarl	%cl, %edi
	movl	%esi, %ecx
	orl	%r9d, %edi
	movb	%dil, 149(%rbx)
	movzbl	-44(%rbp,%rdx), %edi
	movl	%edi, %r9d
	sall	%cl, %r9d
	movl	%eax, %ecx
	sarl	%cl, %r8d
	movl	%esi, %ecx
	orl	%r9d, %r8d
	movb	%r8b, 148(%rbx)
	movzbl	-45(%rbp,%rdx), %r8d
	movl	%r8d, %r9d
	sall	%cl, %r9d
	movl	%eax, %ecx
	sarl	%cl, %edi
	movl	%esi, %ecx
	orl	%r9d, %edi
	movb	%dil, 147(%rbx)
	movzbl	-46(%rbp,%rdx), %edi
	movl	%edi, %r9d
	sall	%cl, %r9d
	movl	%eax, %ecx
	sarl	%cl, %r8d
	movl	%esi, %ecx
	orl	%r9d, %r8d
	movb	%r8b, 146(%rbx)
	movzbl	-47(%rbp,%rdx), %r8d
	movl	%r8d, %r9d
	sall	%cl, %r9d
	movl	%eax, %ecx
	sarl	%cl, %edi
	movl	%esi, %ecx
	orl	%r9d, %edi
	movb	%dil, 145(%rbx)
	movzbl	-48(%rbp,%rdx), %edi
	sall	%cl, %edi
	movl	%eax, %ecx
	movl	$1, %eax
	movl	%edi, %esi
	sarl	%cl, %r8d
	orl	%r8d, %esi
	movb	%sil, 144(%rbx)
	movl	$255, %esi
	sall	%cl, %esi
	andb	-32(%rbp,%rdx), %sil
	movzbl	%sil, %edx
	sarl	%cl, %edx
	orb	%dl, 159(%rbx)
	jmp	.L41
.L48:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE222:
	.size	CRYPTO_ocb128_setiv, .-CRYPTO_ocb128_setiv
	.p2align 4
	.globl	CRYPTO_ocb128_aad
	.type	CRYPTO_ocb128_aad, @function
CRYPTO_ocb128_aad:
.LFB223:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	shrq	$4, %rbx
	subq	$56, %rsp
	movq	96(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addq	%r15, %rbx
	addq	$1, %r15
	cmpq	%r15, %rbx
	jb	.L51
	.p2align 4,,10
	.p2align 3
.L50:
	movq	%r15, %rax
	xorl	%esi, %esi
	testb	$1, %r15b
	jne	.L68
	.p2align 4,,10
	.p2align 3
.L52:
	shrq	%rax
	addl	$1, %esi
	testb	$1, %al
	je	.L52
	movq	%r14, %rdi
	call	ocb_lookup_l
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L57
.L71:
	movq	112(%r14), %rdx
	xorq	(%rax), %rdx
	leaq	-80(%rbp), %rdi
	addq	$1, %r15
	movq	%rdx, 112(%r14)
	movq	120(%r14), %rax
	addq	$16, %r12
	xorq	8(%rcx), %rax
	movq	%rax, 120(%r14)
	movq	-16(%r12), %rsi
	movq	-8(%r12), %rcx
	xorq	%rsi, %rdx
	movq	%rdi, %rsi
	xorq	%rcx, %rax
	movq	%rdx, -80(%rbp)
	movq	16(%r14), %rdx
	movq	%rax, -72(%rbp)
	call	*(%r14)
	movdqu	128(%r14), %xmm0
	pxor	-80(%rbp), %xmm0
	movups	%xmm0, 128(%r14)
	cmpq	%r15, %rbx
	jnb	.L50
.L51:
	andl	$15, %r13d
	jne	.L69
.L55:
	movq	%rbx, 96(%r14)
	movl	$1, %eax
.L49:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L70
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	ocb_lookup_l
	movq	%rax, %rcx
	testq	%rax, %rax
	jne	.L71
.L57:
	xorl	%eax, %eax
	jmp	.L49
.L69:
	movq	112(%r14), %r15
	movq	120(%r14), %rax
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rdi
	xorq	56(%r14), %r15
	xorq	64(%r14), %rax
	movl	$16, %ecx
	movq	%r13, %rdx
	movq	%r15, 112(%r14)
	movq	%r12, %rsi
	movq	%rax, 120(%r14)
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -88(%rbp)
	call	__memcpy_chk@PLT
	movq	%r15, %xmm0
	movb	$-128, -80(%rbp,%r13)
	movq	16(%r14), %rdx
	movhps	-88(%rbp), %xmm0
	pxor	-80(%rbp), %xmm0
	movq	%rax, %rdi
	movq	%rax, %rsi
	movaps	%xmm0, -80(%rbp)
	call	*(%r14)
	movdqu	128(%r14), %xmm0
	pxor	-80(%rbp), %xmm0
	movups	%xmm0, 128(%r14)
	jmp	.L55
.L70:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE223:
	.size	CRYPTO_ocb128_aad, .-CRYPTO_ocb128_aad
	.p2align 4
	.globl	CRYPTO_ocb128_encrypt
	.type	CRYPTO_ocb128_encrypt, @function
CRYPTO_ocb128_encrypt:
.LFB224:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rax, %r12
	pushq	%rbx
	shrq	$4, %r12
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	movq	%rcx, -88(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	104(%rdi), %rcx
	leaq	(%rcx,%r12), %r13
	cmpq	$15, %rax
	jbe	.L73
	cmpq	$0, 32(%rdi)
	je	.L73
	movq	%r13, %rax
	xorl	%esi, %esi
	shrq	%rax
	je	.L74
	.p2align 4,,10
	.p2align 3
.L75:
	addq	$1, %rsi
	shrq	%rax
	jne	.L75
.L74:
	movq	%r14, %rdi
	call	ocb_lookup_l
	testq	%rax, %rax
	je	.L86
	movq	104(%r14), %rax
	movq	%r12, %rdx
	movq	16(%r14), %rcx
	movq	%r15, %rdi
	leaq	160(%r14), %rsi
	leaq	144(%r14), %r9
	pushq	%rsi
	leaq	1(%rax), %r8
	movq	%rbx, %rsi
	pushq	88(%r14)
	call	*32(%r14)
	popq	%rax
	popq	%rdx
.L77:
	movq	-88(%rbp), %r12
	andl	$15, %r12d
	je	.L82
	movdqu	56(%r14), %xmm1
	leaq	-80(%rbp), %r8
	movq	16(%r14), %rdx
	leaq	144(%r14), %rdi
	movdqu	144(%r14), %xmm0
	movq	%r8, -88(%rbp)
	movq	%r8, %rsi
	pxor	%xmm1, %xmm0
	movups	%xmm0, 144(%r14)
	call	*(%r14)
	movzbl	-80(%rbp), %eax
	xorb	(%r15), %al
	cmpq	$1, %r12
	movb	%al, (%rbx)
	movq	-88(%rbp), %r8
	je	.L83
	movzbl	-79(%rbp), %eax
	xorb	1(%r15), %al
	movb	%al, 1(%rbx)
	cmpq	$2, %r12
	je	.L83
	movzbl	2(%r15), %eax
	xorb	-78(%rbp), %al
	movb	%al, 2(%rbx)
	cmpq	$3, %r12
	je	.L83
	movzbl	3(%r15), %eax
	xorb	-77(%rbp), %al
	movb	%al, 3(%rbx)
	cmpq	$4, %r12
	je	.L83
	movzbl	4(%r15), %eax
	xorb	-76(%rbp), %al
	movb	%al, 4(%rbx)
	cmpq	$5, %r12
	je	.L83
	movzbl	5(%r15), %eax
	xorb	-75(%rbp), %al
	movb	%al, 5(%rbx)
	cmpq	$6, %r12
	je	.L83
	movzbl	6(%r15), %eax
	xorb	-74(%rbp), %al
	movb	%al, 6(%rbx)
	cmpq	$7, %r12
	je	.L83
	movzbl	7(%r15), %eax
	xorb	-73(%rbp), %al
	movb	%al, 7(%rbx)
	cmpq	$8, %r12
	je	.L83
	movzbl	8(%r15), %eax
	xorb	-72(%rbp), %al
	movb	%al, 8(%rbx)
	cmpq	$9, %r12
	je	.L83
	movzbl	9(%r15), %eax
	xorb	-71(%rbp), %al
	movb	%al, 9(%rbx)
	cmpq	$10, %r12
	je	.L83
	movzbl	10(%r15), %eax
	xorb	-70(%rbp), %al
	movb	%al, 10(%rbx)
	cmpq	$11, %r12
	je	.L83
	movzbl	11(%r15), %eax
	xorb	-69(%rbp), %al
	movb	%al, 11(%rbx)
	cmpq	$12, %r12
	je	.L83
	movzbl	12(%r15), %eax
	xorb	-68(%rbp), %al
	movb	%al, 12(%rbx)
	cmpq	$13, %r12
	je	.L83
	movzbl	13(%r15), %eax
	xorb	-67(%rbp), %al
	movb	%al, 13(%rbx)
	cmpq	$15, %r12
	jne	.L83
	movzbl	14(%r15), %eax
	xorb	-66(%rbp), %al
	movb	%al, 14(%rbx)
.L83:
	pxor	%xmm0, %xmm0
	movl	$16, %ecx
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r8, %rdi
	movaps	%xmm0, -80(%rbp)
	call	__memcpy_chk@PLT
	movb	$-128, -80(%rbp,%r12)
	movdqu	160(%r14), %xmm0
	pxor	-80(%rbp), %xmm0
	movups	%xmm0, 160(%r14)
.L82:
	movq	%r13, 104(%r14)
	movl	$1, %eax
.L72:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L137
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	leaq	1(%rcx), %r12
	cmpq	%r12, %r13
	jb	.L77
	movq	%r12, %rax
	xorl	%esi, %esi
	testb	$1, %r12b
	jne	.L138
	.p2align 4,,10
	.p2align 3
.L79:
	shrq	%rax
	addl	$1, %esi
	testb	$1, %al
	je	.L79
	movq	%r14, %rdi
	call	ocb_lookup_l
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L86
.L80:
	movq	144(%r14), %rdx
	xorq	(%rax), %rdx
	addq	$1, %r12
	addq	$16, %r15
	movq	%rdx, 144(%r14)
	movq	152(%r14), %rax
	addq	$16, %rbx
	xorq	8(%rsi), %rax
	movq	%rax, 152(%r14)
	movq	-16(%r15), %rdi
	movq	-8(%r15), %rsi
	xorq	%rdi, 160(%r14)
	xorq	%rdi, %rdx
	leaq	-80(%rbp), %rdi
	xorq	%rsi, 168(%r14)
	xorq	%rsi, %rax
	movq	%rdx, -80(%rbp)
	movq	%rdi, %rsi
	movq	16(%r14), %rdx
	movq	%rax, -72(%rbp)
	call	*(%r14)
	movdqu	144(%r14), %xmm0
	pxor	-80(%rbp), %xmm0
	movups	%xmm0, -16(%rbx)
	movaps	%xmm0, -80(%rbp)
	cmpq	%r12, %r13
	jb	.L77
	movq	%r12, %rax
	xorl	%esi, %esi
	testb	$1, %r12b
	je	.L79
.L138:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	ocb_lookup_l
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L80
.L86:
	xorl	%eax, %eax
	jmp	.L72
.L137:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE224:
	.size	CRYPTO_ocb128_encrypt, .-CRYPTO_ocb128_encrypt
	.p2align 4
	.globl	CRYPTO_ocb128_decrypt
	.type	CRYPTO_ocb128_decrypt, @function
CRYPTO_ocb128_decrypt:
.LFB225:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rax, %rbx
	shrq	$4, %rbx
	subq	$56, %rsp
	movq	%rcx, -88(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	104(%rdi), %rcx
	leaq	(%rcx,%rbx), %r13
	cmpq	$15, %rax
	jbe	.L140
	cmpq	$0, 32(%rdi)
	je	.L140
	movq	%r13, %rax
	xorl	%esi, %esi
	shrq	%rax
	je	.L141
	.p2align 4,,10
	.p2align 3
.L142:
	addq	$1, %rsi
	shrq	%rax
	jne	.L142
.L141:
	movq	%r14, %rdi
	call	ocb_lookup_l
	testq	%rax, %rax
	je	.L154
	movq	104(%r14), %rax
	movq	%rbx, %rdx
	movq	24(%r14), %rcx
	movq	%r15, %rdi
	leaq	160(%r14), %rsi
	leaq	144(%r14), %r9
	pushq	%rsi
	leaq	1(%rax), %r8
	movq	%r12, %rsi
	pushq	88(%r14)
	call	*32(%r14)
	popq	%rax
	popq	%rdx
.L144:
	movq	-88(%rbp), %rbx
	andl	$15, %ebx
	je	.L150
	movdqu	56(%r14), %xmm2
	leaq	-80(%rbp), %r8
	movq	16(%r14), %rdx
	leaq	144(%r14), %rdi
	movdqu	144(%r14), %xmm0
	movq	%r8, -88(%rbp)
	movq	%r8, %rsi
	pxor	%xmm2, %xmm0
	movups	%xmm0, 144(%r14)
	call	*(%r14)
	movzbl	-80(%rbp), %eax
	xorb	(%r15), %al
	cmpq	$1, %rbx
	movb	%al, (%r12)
	movq	-88(%rbp), %r8
	je	.L151
	movzbl	-79(%rbp), %eax
	xorb	1(%r15), %al
	movb	%al, 1(%r12)
	cmpq	$2, %rbx
	je	.L151
	movzbl	2(%r15), %eax
	xorb	-78(%rbp), %al
	movb	%al, 2(%r12)
	cmpq	$3, %rbx
	je	.L151
	movzbl	3(%r15), %eax
	xorb	-77(%rbp), %al
	movb	%al, 3(%r12)
	cmpq	$4, %rbx
	je	.L151
	movzbl	4(%r15), %eax
	xorb	-76(%rbp), %al
	movb	%al, 4(%r12)
	cmpq	$5, %rbx
	je	.L151
	movzbl	5(%r15), %eax
	xorb	-75(%rbp), %al
	movb	%al, 5(%r12)
	cmpq	$6, %rbx
	je	.L151
	movzbl	6(%r15), %eax
	xorb	-74(%rbp), %al
	movb	%al, 6(%r12)
	cmpq	$7, %rbx
	je	.L151
	movzbl	7(%r15), %eax
	xorb	-73(%rbp), %al
	movb	%al, 7(%r12)
	cmpq	$8, %rbx
	je	.L151
	movzbl	8(%r15), %eax
	xorb	-72(%rbp), %al
	movb	%al, 8(%r12)
	cmpq	$9, %rbx
	je	.L151
	movzbl	9(%r15), %eax
	xorb	-71(%rbp), %al
	movb	%al, 9(%r12)
	cmpq	$10, %rbx
	je	.L151
	movzbl	10(%r15), %eax
	xorb	-70(%rbp), %al
	movb	%al, 10(%r12)
	cmpq	$11, %rbx
	je	.L151
	movzbl	11(%r15), %eax
	xorb	-69(%rbp), %al
	movb	%al, 11(%r12)
	cmpq	$12, %rbx
	je	.L151
	movzbl	12(%r15), %eax
	xorb	-68(%rbp), %al
	movb	%al, 12(%r12)
	cmpq	$13, %rbx
	je	.L151
	movzbl	13(%r15), %eax
	xorb	-67(%rbp), %al
	movb	%al, 13(%r12)
	cmpq	$15, %rbx
	jne	.L151
	movzbl	14(%r15), %eax
	xorb	-66(%rbp), %al
	movb	%al, 14(%r12)
.L151:
	pxor	%xmm0, %xmm0
	movl	$16, %ecx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	movaps	%xmm0, -80(%rbp)
	call	__memcpy_chk@PLT
	movb	$-128, -80(%rbp,%rbx)
	movdqu	160(%r14), %xmm0
	pxor	-80(%rbp), %xmm0
	movups	%xmm0, 160(%r14)
.L150:
	movq	%r13, 104(%r14)
	movl	$1, %eax
.L139:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L205
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	leaq	1(%rcx), %rbx
	cmpq	%rbx, %r13
	jb	.L144
	movq	%rbx, %rax
	xorl	%esi, %esi
	testb	$1, %bl
	jne	.L206
	.p2align 4,,10
	.p2align 3
.L147:
	shrq	%rax
	addl	$1, %esi
	testb	$1, %al
	je	.L147
	movq	%r14, %rdi
	call	ocb_lookup_l
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L154
.L148:
	movq	144(%r14), %rdx
	xorq	(%rax), %rdx
	addq	$1, %rbx
	addq	$16, %r15
	movq	%rdx, 144(%r14)
	movq	152(%r14), %rax
	addq	$16, %r12
	xorq	8(%rsi), %rax
	movq	%rax, 152(%r14)
	movq	-16(%r15), %rdi
	movq	-8(%r15), %rsi
	xorq	%rdi, %rdx
	leaq	-80(%rbp), %rdi
	xorq	%rsi, %rax
	movq	%rdx, -80(%rbp)
	movq	%rdi, %rsi
	movq	24(%r14), %rdx
	movq	%rax, -72(%rbp)
	call	*8(%r14)
	movq	144(%r14), %rdx
	xorq	-80(%rbp), %rdx
	movq	152(%r14), %rax
	xorq	-72(%rbp), %rax
	movq	%rdx, -80(%rbp)
	movq	%rax, -72(%rbp)
	movdqa	-80(%rbp), %xmm1
	xorq	%rdx, 160(%r14)
	xorq	%rax, 168(%r14)
	movups	%xmm1, -16(%r12)
	cmpq	%rbx, %r13
	jb	.L144
	movq	%rbx, %rax
	xorl	%esi, %esi
	testb	$1, %bl
	je	.L147
.L206:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	ocb_lookup_l
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L148
.L154:
	xorl	%eax, %eax
	jmp	.L139
.L205:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE225:
	.size	CRYPTO_ocb128_decrypt, .-CRYPTO_ocb128_decrypt
	.p2align 4
	.globl	CRYPTO_ocb128_finish
	.type	CRYPTO_ocb128_finish, @function
CRYPTO_ocb128_finish:
.LFB227:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	-1(%rdx), %rax
	cmpq	$15, %rax
	ja	.L210
	movdqu	72(%rdi), %xmm2
	movq	%rdi, %rbx
	leaq	-64(%rbp), %r14
	movq	%rsi, %r13
	movdqu	144(%rdi), %xmm0
	movdqu	160(%rdi), %xmm1
	movq	%rdx, %r12
	movq	%r14, %rsi
	movq	16(%rdi), %rdx
	movq	%r14, %rdi
	pxor	%xmm1, %xmm0
	pxor	%xmm2, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	*(%rbx)
	movdqu	128(%rbx), %xmm0
	movq	%r12, %rdx
	movq	%r13, %rsi
	pxor	-64(%rbp), %xmm0
	movq	%r14, %rdi
	movaps	%xmm0, -64(%rbp)
	call	CRYPTO_memcmp@PLT
.L207:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L212
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L210:
	.cfi_restore_state
	movl	$-1, %eax
	jmp	.L207
.L212:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE227:
	.size	CRYPTO_ocb128_finish, .-CRYPTO_ocb128_finish
	.p2align 4
	.globl	CRYPTO_ocb128_tag
	.type	CRYPTO_ocb128_tag, @function
CRYPTO_ocb128_tag:
.LFB228:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	-1(%rdx), %rax
	cmpq	$15, %rax
	ja	.L216
	movdqu	72(%rdi), %xmm2
	movq	%rdi, %rbx
	leaq	-64(%rbp), %r14
	movq	%rsi, %r13
	movdqu	160(%rdi), %xmm1
	movdqu	144(%rdi), %xmm0
	movq	%rdx, %r12
	movq	%r14, %rsi
	movq	16(%rdi), %rdx
	movq	%r14, %rdi
	pxor	%xmm1, %xmm0
	pxor	%xmm2, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	*(%rbx)
	movdqu	128(%rbx), %xmm0
	movq	%r12, %rdx
	movq	%r14, %rsi
	pxor	-64(%rbp), %xmm0
	movq	%r13, %rdi
	movaps	%xmm0, -64(%rbp)
	call	memcpy@PLT
	movl	$1, %eax
.L213:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L218
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L216:
	.cfi_restore_state
	movl	$-1, %eax
	jmp	.L213
.L218:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE228:
	.size	CRYPTO_ocb128_tag, .-CRYPTO_ocb128_tag
	.p2align 4
	.globl	CRYPTO_ocb128_cleanup
	.type	CRYPTO_ocb128_cleanup, @function
CRYPTO_ocb128_cleanup:
.LFB229:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L219
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$557, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	48(%rdi), %rsi
	movq	88(%rdi), %rdi
	salq	$4, %rsi
	call	CRYPTO_clear_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$176, %esi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	OPENSSL_cleanse@PLT
	.p2align 4,,10
	.p2align 3
.L219:
	ret
	.cfi_endproc
.LFE229:
	.size	CRYPTO_ocb128_cleanup, .-CRYPTO_ocb128_cleanup
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.quad	0
	.quad	5
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
