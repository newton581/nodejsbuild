	.file	"a_mbstr.c"
	.text
	.p2align 4
	.type	in_utf8, @function
in_utf8:
.LFB422:
	.cfi_startproc
	endbr64
	addl	$1, (%rsi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE422:
	.size	in_utf8, .-in_utf8
	.p2align 4
	.type	cpy_asc, @function
cpy_asc:
.LFB425:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movb	%dil, (%rax)
	movl	$1, %eax
	addq	$1, (%rsi)
	ret
	.cfi_endproc
.LFE425:
	.size	cpy_asc, .-cpy_asc
	.p2align 4
	.type	cpy_bmp, @function
cpy_bmp:
.LFB426:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	rolw	$8, %di
	movw	%di, (%rax)
	movl	$1, %eax
	addq	$2, (%rsi)
	ret
	.cfi_endproc
.LFE426:
	.size	cpy_bmp, .-cpy_bmp
	.p2align 4
	.type	cpy_univ, @function
cpy_univ:
.LFB427:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	bswap	%edi
	movl	%edi, (%rax)
	movl	$1, %eax
	addq	$4, (%rsi)
	ret
	.cfi_endproc
.LFE427:
	.size	cpy_univ, .-cpy_univ
	.p2align 4
	.type	cpy_utf8, @function
cpy_utf8:
.LFB428:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	movl	$255, %esi
	subq	$8, %rsp
	movq	(%rbx), %rdi
	call	UTF8_putc@PLT
	cltq
	addq	%rax, (%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE428:
	.size	cpy_utf8, .-cpy_utf8
	.p2align 4
	.type	out_utf8, @function
out_utf8:
.LFB423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	xorl	%edi, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	movl	$-1, %esi
	subq	$8, %rsp
	call	UTF8_putc@PLT
	addl	%eax, (%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE423:
	.size	out_utf8, .-out_utf8
	.p2align 4
	.type	type_str, @function
type_str:
.LFB424:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpq	$2147483647, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	$2147483647, %r14d
	pushq	%r13
	cmovbe	%rdi, %r14
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rsi), %rbx
	testb	$1, %bl
	jne	.L46
.L11:
	testb	$2, %bl
	jne	.L47
.L12:
	testb	$16, %bl
	je	.L13
	movq	%rbx, %rax
	andq	$-17, %rax
	andl	$-128, %r14d
	cmovne	%rax, %rbx
.L13:
	testb	$4, %bl
	je	.L14
	cmpq	$255, %r12
	movq	%rbx, %rax
	seta	%dl
	andq	$-5, %rax
	testb	%dl, %dl
	cmovne	%rax, %rbx
.L14:
	testb	$8, %bh
	je	.L15
	cmpq	$65535, %r12
	ja	.L48
.L16:
	movq	%rbx, 0(%r13)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	andb	$-9, %bh
.L15:
	movl	$-1, %eax
	testq	%rbx, %rbx
	jne	.L16
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	movl	$2048, %esi
	movl	%r14d, %edi
	call	ossl_ctype_check@PLT
	movl	%eax, %r8d
	movq	%rbx, %rax
	andq	$-3, %rax
	testl	%r8d, %r8d
	cmove	%rax, %rbx
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L46:
	movl	$4, %esi
	movl	%r14d, %edi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	jne	.L11
	cmpq	$32, %r12
	movq	%rbx, %rax
	setne	%dl
	andq	$-2, %rax
	testb	%dl, %dl
	cmovne	%rax, %rbx
	jmp	.L11
	.cfi_endproc
.LFE424:
	.size	type_str, .-type_str
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/asn1/a_mbstr.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"%ld"
.LC2:
	.string	"minsize="
.LC3:
	.string	"maxsize="
	.text
	.p2align 4
	.globl	ASN1_mbstring_ncopy
	.type	ASN1_mbstring_ncopy, @function
ASN1_mbstring_ncopy:
.LFB420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$120, %rsp
	movq	%rdi, -160(%rbp)
	movq	%r9, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$-1, %edx
	jne	.L50
	movq	%rsi, %rdi
	call	strlen@PLT
	movl	%eax, %r13d
.L50:
	testq	%r15, %r15
	movl	$10246, %eax
	cmove	%rax, %r15
	cmpl	$4098, %ebx
	je	.L52
	jg	.L53
	cmpl	$4096, %ebx
	je	.L54
	cmpl	$4097, %ebx
	jne	.L56
	movl	%r13d, %r14d
.L55:
	movq	-144(%rbp), %rsi
	testq	%rsi, %rsi
	jle	.L64
	movslq	%r14d, %rax
	cmpq	%rsi, %rax
	jl	.L182
.L64:
	cmpq	$0, 16(%rbp)
	jle	.L68
	movslq	%r14d, %rax
	cmpq	16(%rbp), %rax
	jg	.L183
.L68:
	movl	%r13d, %r9d
	movq	%r12, %rcx
	testl	%r13d, %r13d
	jne	.L66
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L69:
	cmpl	$4098, %ebx
	je	.L184
	cmpl	$4100, %ebx
	jne	.L72
	movl	(%rcx), %edx
	subl	$4, %r9d
	addq	$4, %rcx
	bswap	%edx
	movl	%edx, %edx
	movq	%rdx, -104(%rbp)
	cmpq	$255, %rdx
	seta	%r10b
.L70:
	movl	$2147483648, %eax
	cmpq	%rax, %rdx
	movl	$2147483647, %eax
	cmovb	%rdx, %rax
	movq	%rax, -120(%rbp)
	testb	$1, %r15b
	jne	.L185
.L74:
	testb	$2, %r15b
	jne	.L186
.L75:
	testb	$16, %r15b
	je	.L76
	movq	%r15, %rax
	andq	$-17, %rax
	testl	$-128, -120(%rbp)
	cmovne	%rax, %r15
.L76:
	testb	$4, %r15b
	je	.L77
	movq	%r15, %rax
	andq	$-5, %rax
	testb	%r10b, %r10b
	cmovne	%rax, %r15
.L77:
	testl	$2048, %r15d
	je	.L78
	cmpq	$65535, %rdx
	jbe	.L79
	andq	$-2049, %r15
.L78:
	testq	%r15, %r15
	je	.L73
.L79:
	testl	%r9d, %r9d
	je	.L67
.L66:
	cmpl	$4097, %ebx
	jne	.L69
	movzbl	(%rcx), %edx
	subl	$1, %r9d
	addq	$1, %rcx
	xorl	%r10d, %r10d
	movq	%rdx, -104(%rbp)
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L73:
	movl	$115, %r8d
	movl	$124, %edx
	movl	$122, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, -120(%rbp)
.L49:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L187
	movl	-120(%rbp), %eax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	movzwl	(%rcx), %edx
	subl	$2, %r9d
	addq	$2, %rcx
	rolw	$8, %dx
	movzwl	%dx, %edx
	cmpq	$255, %rdx
	movq	%rdx, -104(%rbp)
	seta	%r10b
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L53:
	cmpl	$4100, %ebx
	jne	.L56
	testb	$3, %r13b
	jne	.L188
	movl	%r13d, %r14d
	sarl	$2, %r14d
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L185:
	movl	-120(%rbp), %edi
	movl	$4, %esi
	movb	%r10b, -152(%rbp)
	movq	%rcx, -144(%rbp)
	movl	%r9d, -136(%rbp)
	movq	%rdx, -128(%rbp)
	call	ossl_ctype_check@PLT
	movq	-128(%rbp), %rdx
	movl	-136(%rbp), %r9d
	movq	-144(%rbp), %rcx
	movzbl	-152(%rbp), %r10d
	cmpq	$32, %rdx
	je	.L74
	testl	%eax, %eax
	movq	%r15, %rax
	sete	%sil
	andq	$-2, %rax
	testb	%sil, %sil
	cmovne	%rax, %r15
	testb	$2, %r15b
	je	.L75
.L186:
	movl	-120(%rbp), %edi
	movl	$2048, %esi
	movb	%r10b, -152(%rbp)
	movq	%rcx, -144(%rbp)
	movl	%r9d, -136(%rbp)
	movq	%rdx, -128(%rbp)
	call	ossl_ctype_check@PLT
	movq	-144(%rbp), %rcx
	movl	-136(%rbp), %r9d
	movl	%eax, %r11d
	movq	%r15, %rax
	movzbl	-152(%rbp), %r10d
	movq	-128(%rbp), %rdx
	andq	$-3, %rax
	testl	%r11d, %r11d
	cmove	%rax, %r15
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L72:
	leaq	-104(%rbp), %r10
	movl	%r9d, %esi
	movq	%rcx, %rdi
	movl	%r9d, -128(%rbp)
	movq	%r10, %rdx
	movq	%rcx, -120(%rbp)
	call	UTF8_getc@PLT
	testl	%eax, %eax
	js	.L73
	movl	-128(%rbp), %r9d
	movq	-120(%rbp), %rcx
	movq	-104(%rbp), %rdx
	subl	%eax, %r9d
	cltq
	addq	%rax, %rcx
	cmpq	$255, %rdx
	seta	%r10b
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L67:
	testb	$1, %r15b
	je	.L189
	movl	$18, -120(%rbp)
	movl	$4097, %r15d
.L80:
	movq	-160(%rbp), %rax
	testq	%rax, %rax
	je	.L49
	movq	(%rax), %r8
	testq	%r8, %r8
	je	.L81
	movq	8(%r8), %rdi
	movl	$144, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r8, -128(%rbp)
	call	CRYPTO_free@PLT
	movq	-128(%rbp), %r8
	movl	-120(%rbp), %eax
	movb	$0, -136(%rbp)
	movq	$0, 8(%r8)
	movl	$0, (%r8)
	movl	%eax, 4(%r8)
	cmpl	%ebx, %r15d
	je	.L190
.L84:
	cmpl	$4098, %r15d
	je	.L85
	jg	.L86
	cmpl	$4096, %r15d
	je	.L87
	cmpl	$4097, %r15d
	jne	.L180
	leal	1(%r14), %edi
	leaq	cpy_asc(%rip), %r15
	movslq	%edi, %rdi
	.p2align 4,,10
	.p2align 3
.L89:
	movl	$189, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r8, -128(%rbp)
	call	CRYPTO_malloc@PLT
	movq	-128(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, -112(%rbp)
	je	.L191
	movl	%r14d, (%r8)
	movslq	%r14d, %r14
	movq	%rax, 8(%r8)
	movb	$0, (%rax,%r14)
	testl	%r13d, %r13d
	je	.L49
	cmpl	$4097, %ebx
	je	.L105
	testq	%r15, %r15
	je	.L192
	cmpl	$4098, %ebx
	je	.L123
	cmpl	$4100, %ebx
	je	.L124
	leaq	-104(%rbp), %r10
	leaq	-112(%rbp), %rbx
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L193:
	subl	%eax, %r13d
	cltq
	movq	-104(%rbp), %rdi
	movq	%rbx, %rsi
	addq	%rax, %r12
	call	*%r15
	testl	%eax, %eax
	jle	.L49
	testl	%r13d, %r13d
	movq	-128(%rbp), %r10
	je	.L49
.L111:
	movq	%r10, %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	%r10, -128(%rbp)
	call	UTF8_getc@PLT
	testl	%eax, %eax
	jns	.L193
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L189:
	testb	$2, %r15b
	jne	.L118
	testb	$16, %r15b
	jne	.L119
	testb	$4, %r15b
	jne	.L120
	testl	$2048, %r15d
	jne	.L121
	movq	%r15, %r8
	andl	$256, %r8d
	cmpq	$1, %r8
	sbbl	%r15d, %r15d
	andl	$-4, %r15d
	addl	$4100, %r15d
	cmpq	$1, %r8
	sbbl	%eax, %eax
	andl	$-16, %eax
	addl	$28, %eax
	movl	%eax, -120(%rbp)
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L86:
	cmpl	$4100, %r15d
	jne	.L180
	sall	$2, %r14d
	leaq	cpy_univ(%rip), %r15
	leal	1(%r14), %edi
	movslq	%edi, %rdi
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L56:
	movl	$95, %r8d
	movl	$160, %edx
	movl	$122, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, -120(%rbp)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L54:
	xorl	%r14d, %r14d
	testl	%r13d, %r13d
	je	.L55
	movl	%r13d, %ecx
	movq	%r12, %rdi
	leaq	-104(%rbp), %r10
	xorl	%r14d, %r14d
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L194:
	movq	-120(%rbp), %rdi
	movl	-128(%rbp), %ecx
	movslq	%eax, %rdx
	addl	$1, %r14d
	movq	-136(%rbp), %r10
	addq	%rdx, %rdi
	subl	%eax, %ecx
	je	.L55
.L58:
	movq	%r10, %rdx
	movl	%ecx, %esi
	movq	%r10, -136(%rbp)
	movl	%ecx, -128(%rbp)
	movq	%rdi, -120(%rbp)
	call	UTF8_getc@PLT
	testl	%eax, %eax
	jns	.L194
	movl	$85, %r8d
	movl	$134, %edx
	movl	$122, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, -120(%rbp)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L52:
	testb	$1, %r13b
	jne	.L195
	movl	%r13d, %r14d
	sarl	%r14d
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L118:
	movl	$19, -120(%rbp)
	movl	$4097, %r15d
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L123:
	leaq	-112(%rbp), %rbx
	addl	%r12d, %r13d
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L196:
	cmpl	%r12d, %r13d
	je	.L49
.L109:
	movzwl	(%r12), %edi
	movq	%rbx, %rsi
	addq	$2, %r12
	rolw	$8, %di
	movzwl	%di, %edi
	movq	%rdi, -104(%rbp)
	call	*%r15
	testl	%eax, %eax
	jg	.L196
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L81:
	movl	-120(%rbp), %edi
	call	ASN1_STRING_type_new@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L197
	movq	-160(%rbp), %rax
	movb	$1, -136(%rbp)
	movq	%r8, (%rax)
	cmpl	%ebx, %r15d
	jne	.L84
.L190:
	movl	%r13d, %edx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	ASN1_STRING_set@PLT
	testl	%eax, %eax
	jne	.L49
	movl	$160, %r8d
.L181:
	movl	$65, %edx
	movl	$122, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, -120(%rbp)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L180:
	movl	$1, %edi
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L105:
	testq	%r15, %r15
	je	.L49
	leal	-1(%r13), %eax
	leaq	-112(%rbp), %rbx
	leaq	1(%r12,%rax), %r13
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L198:
	cmpq	%r13, %r12
	je	.L49
.L112:
	movzbl	(%r12), %edi
	addq	$1, %r12
	movq	%rbx, %rsi
	movq	%rdi, -104(%rbp)
	call	*%r15
	testl	%eax, %eax
	jg	.L198
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L85:
	addl	%r14d, %r14d
	leaq	cpy_bmp(%rip), %r15
	leal	1(%r14), %edi
	movslq	%edi, %rdi
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L87:
	xorl	%r14d, %r14d
	movl	$1, %edi
	testl	%r13d, %r13d
	je	.L92
	cmpl	$4097, %ebx
	je	.L93
	cmpl	$4098, %ebx
	je	.L94
	cmpl	$4100, %ebx
	je	.L95
	movq	%r12, -128(%rbp)
	movl	%r13d, %r15d
	xorl	%r14d, %r14d
	leaq	-104(%rbp), %r10
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L199:
	movq	-104(%rbp), %rdx
	xorl	%edi, %edi
	movl	$-1, %esi
	subl	%eax, %r15d
	cltq
	addq	%rax, -128(%rbp)
	call	UTF8_putc@PLT
	movq	-144(%rbp), %r8
	movq	-152(%rbp), %r10
	addl	%eax, %r14d
	testl	%r15d, %r15d
	je	.L98
.L97:
	movq	-128(%rbp), %rdi
	movq	%r10, %rdx
	movl	%r15d, %esi
	movq	%r8, -144(%rbp)
	movq	%r10, -152(%rbp)
	call	UTF8_getc@PLT
	movq	-144(%rbp), %r8
	testl	%eax, %eax
	jns	.L199
	.p2align 4,,10
	.p2align 3
.L98:
	leal	1(%r14), %edi
	movslq	%edi, %rdi
.L92:
	leaq	cpy_utf8(%rip), %r15
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L119:
	movl	$22, -120(%rbp)
	movl	$4097, %r15d
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L124:
	leaq	-112(%rbp), %rbx
	addl	%r12d, %r13d
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L200:
	cmpl	%r12d, %r13d
	je	.L49
.L110:
	movl	(%r12), %edi
	movq	%rbx, %rsi
	addq	$4, %r12
	bswap	%edi
	movl	%edi, %edi
	movq	%rdi, -104(%rbp)
	call	*%r15
	testl	%eax, %eax
	jg	.L200
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L120:
	movl	$20, -120(%rbp)
	movl	$4097, %r15d
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L94:
	leal	-2(%r13), %eax
	movq	%r12, %r15
	xorl	%r14d, %r14d
	shrl	%eax
	leaq	2(%r12,%rax,2), %rax
	movq	%rax, -144(%rbp)
	.p2align 4,,10
	.p2align 3
.L100:
	movzwl	(%r15), %edx
	xorl	%edi, %edi
	movl	$-1, %esi
	movq	%r8, -128(%rbp)
	addq	$2, %r15
	rolw	$8, %dx
	movzwl	%dx, %edx
	movq	%rdx, -104(%rbp)
	call	UTF8_putc@PLT
	movq	-128(%rbp), %r8
	addl	%eax, %r14d
	cmpq	%r15, -144(%rbp)
	jne	.L100
	jmp	.L98
.L182:
	movl	$100, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$152, %edx
	movl	$122, %esi
	movl	$13, %edi
	leaq	-96(%rbp), %r12
	call	ERR_put_error@PLT
	movl	$32, %esi
	movq	%r12, %rdi
	xorl	%eax, %eax
	movq	-144(%rbp), %rcx
	leaq	.LC1(%rip), %rdx
	call	BIO_snprintf@PLT
	movq	%r12, %rdx
	movl	$2, %edi
	xorl	%eax, %eax
	leaq	.LC2(%rip), %rsi
	call	ERR_add_error_data@PLT
	movl	$-1, -120(%rbp)
	jmp	.L49
.L188:
	movl	$73, %r8d
	movl	$133, %edx
	movl	$122, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, -120(%rbp)
	jmp	.L49
.L195:
	movl	$64, %r8d
	movl	$129, %edx
	movl	$122, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, -120(%rbp)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L192:
	subl	$4098, %ebx
	andl	$-3, %ebx
	je	.L49
	leaq	-104(%rbp), %r10
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L201:
	movslq	%eax, %rdx
	movq	-128(%rbp), %r10
	addq	%rdx, %r12
	subl	%eax, %r13d
	je	.L49
.L108:
	movq	%r10, %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	%r10, -128(%rbp)
	call	UTF8_getc@PLT
	testl	%eax, %eax
	jns	.L201
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L93:
	leal	-1(%r13), %eax
	movq	%r12, %r15
	xorl	%r14d, %r14d
	leaq	1(%r12,%rax), %rax
	movq	%rax, -144(%rbp)
	.p2align 4,,10
	.p2align 3
.L101:
	movzbl	(%r15), %edx
	xorl	%edi, %edi
	movl	$-1, %esi
	movq	%r8, -128(%rbp)
	addq	$1, %r15
	movq	%rdx, -104(%rbp)
	call	UTF8_putc@PLT
	movq	-128(%rbp), %r8
	addl	%eax, %r14d
	cmpq	-144(%rbp), %r15
	jne	.L101
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L121:
	movl	$30, -120(%rbp)
	movl	$4098, %r15d
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L95:
	leal	-4(%r13), %eax
	movq	%r12, %r15
	xorl	%r14d, %r14d
	shrl	$2, %eax
	leaq	4(%r12,%rax,4), %rax
	movq	%rax, -144(%rbp)
	.p2align 4,,10
	.p2align 3
.L99:
	movl	(%r15), %edx
	xorl	%edi, %edi
	movl	$-1, %esi
	movq	%r8, -128(%rbp)
	addq	$4, %r15
	bswap	%edx
	movl	%edx, %edx
	movq	%rdx, -104(%rbp)
	call	UTF8_putc@PLT
	movq	-128(%rbp), %r8
	addl	%eax, %r14d
	cmpq	%r15, -144(%rbp)
	jne	.L99
	jmp	.L98
.L191:
	cmpb	$0, -136(%rbp)
	jne	.L202
.L103:
	movl	$192, %r8d
	jmp	.L181
.L183:
	movl	$107, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$151, %edx
	movl	$122, %esi
	movl	$13, %edi
	leaq	-96(%rbp), %r12
	call	ERR_put_error@PLT
	movq	16(%rbp), %rcx
	movl	$32, %esi
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rdx
	movq	%r12, %rdi
	call	BIO_snprintf@PLT
	movq	%r12, %rdx
	movl	$2, %edi
	xorl	%eax, %eax
	leaq	.LC3(%rip), %rsi
	call	ERR_add_error_data@PLT
	movl	$-1, -120(%rbp)
	jmp	.L49
.L202:
	movq	%r8, %rdi
	call	ASN1_STRING_free@PLT
	jmp	.L103
.L197:
	movl	$152, %r8d
	jmp	.L181
.L187:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE420:
	.size	ASN1_mbstring_ncopy, .-ASN1_mbstring_ncopy
	.p2align 4
	.globl	ASN1_mbstring_copy
	.type	ASN1_mbstring_copy, @function
ASN1_mbstring_copy:
.LFB419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r9d, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	call	ASN1_mbstring_ncopy
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE419:
	.size	ASN1_mbstring_copy, .-ASN1_mbstring_copy
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
