	.file	"asn1_lib.c"
	.text
	.p2align 4
	.globl	ASN1_check_infinite_end
	.type	ASN1_check_infinite_end, @function
ASN1_check_infinite_end:
.LFB468:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	testq	%rsi, %rsi
	jle	.L1
	xorl	%eax, %eax
	cmpq	$1, %rsi
	je	.L1
	movq	(%rdi), %rdx
	cmpb	$0, (%rdx)
	jne	.L1
	cmpb	$0, 1(%rdx)
	je	.L9
.L1:
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	addq	$2, %rdx
	movl	$1, %eax
	movq	%rdx, (%rdi)
	ret
	.cfi_endproc
.LFE468:
	.size	ASN1_check_infinite_end, .-ASN1_check_infinite_end
	.p2align 4
	.globl	ASN1_const_check_infinite_end
	.type	ASN1_const_check_infinite_end, @function
ASN1_const_check_infinite_end:
.LFB499:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	testq	%rsi, %rsi
	jle	.L10
	xorl	%eax, %eax
	cmpq	$1, %rsi
	je	.L10
	movq	(%rdi), %rdx
	cmpb	$0, (%rdx)
	jne	.L10
	cmpb	$0, 1(%rdx)
	je	.L17
.L10:
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	addq	$2, %rdx
	movl	$1, %eax
	movq	%rdx, (%rdi)
	ret
	.cfi_endproc
.LFE499:
	.size	ASN1_const_check_infinite_end, .-ASN1_const_check_infinite_end
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/asn1/asn1_lib.c"
	.text
	.p2align 4
	.globl	ASN1_get_object
	.type	ASN1_get_object, @function
ASN1_get_object:
.LFB470:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%r8, %r8
	je	.L19
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	leaq	-1(%r8), %r11
	movzbl	(%r12), %eax
	leaq	1(%r12), %r10
	movl	%eax, %edi
	andl	$31, %edi
	cmpb	$31, %dil
	je	.L81
	testq	%r11, %r11
	je	.L19
	movzbl	%dil, %edi
.L23:
	movl	%edi, (%rdx)
	movl	%eax, %edx
	andl	$192, %edx
	movl	%edx, (%rcx)
	testq	%r11, %r11
	jle	.L19
	andl	$32, %eax
	leaq	1(%r10), %r14
	movl	%eax, %r13d
	movzbl	(%r10), %eax
	cmpb	$-128, %al
	je	.L82
	movl	%eax, %edx
	andl	$127, %edx
	testb	%al, %al
	js	.L83
	movzbl	%dl, %edx
	xorl	%r15d, %r15d
	movq	%rdx, (%rsi)
.L25:
	movq	%r14, %rax
	subq	%r12, %rax
	subq	%rax, %r8
	cmpq	%rdx, %r8
	jl	.L33
.L80:
	movq	%r14, (%rbx)
	movzbl	%r13b, %eax
	addq	$8, %rsp
	popq	%rbx
	orl	%r15d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	xorl	%r9d, %r9d
	testq	%r11, %r11
	jne	.L21
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L22:
	andl	$127, %edi
	orq	%r13, %rdi
	movq	%rdi, %r9
	testq	%r11, %r11
	je	.L19
	cmpq	$16777215, %rdi
	jg	.L19
.L21:
	movzbl	(%r10), %edi
	salq	$7, %r9
	addq	$1, %r10
	subq	$1, %r11
	movq	%r9, %r13
	testb	%dil, %dil
	js	.L22
	movzbl	%dil, %r9d
	movl	%r9d, %edi
	orl	%r13d, %edi
	testq	%r11, %r11
	jne	.L23
.L19:
	movl	$101, %r8d
	movl	$123, %edx
	movl	$114, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	$128, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	movzbl	%dl, %eax
	leal	1(%rax), %ecx
	movslq	%ecx, %rcx
	cmpq	%rcx, %r11
	jle	.L19
	testb	%dl, %dl
	jne	.L27
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L29:
	addq	$1, %r14
	subl	$1, %eax
	je	.L28
.L27:
	cmpb	$0, (%r14)
	je	.L29
	cmpl	$8, %eax
	jg	.L19
	movl	%eax, %ecx
	movzbl	(%r14), %edx
	subl	$1, %ecx
	je	.L31
	movzbl	1(%r14), %edi
	salq	$8, %rdx
	orq	%rdi, %rdx
	cmpl	$2, %eax
	je	.L31
	movzbl	2(%r14), %edi
	salq	$8, %rdx
	orq	%rdi, %rdx
	cmpl	$3, %eax
	je	.L31
	movzbl	3(%r14), %edi
	salq	$8, %rdx
	orq	%rdi, %rdx
	cmpl	$4, %eax
	je	.L31
	movzbl	4(%r14), %edi
	salq	$8, %rdx
	orq	%rdi, %rdx
	cmpl	$5, %eax
	je	.L31
	movzbl	5(%r14), %edi
	salq	$8, %rdx
	orq	%rdi, %rdx
	subl	$6, %eax
	je	.L31
	movzbl	6(%r14), %edi
	salq	$8, %rdx
	orq	%rdi, %rdx
	cmpl	$1, %eax
	je	.L31
	movzbl	7(%r14), %eax
	movslq	%ecx, %rcx
	salq	$8, %rdx
	leaq	1(%r14,%rcx), %r14
	orq	%rax, %rdx
	js	.L19
	.p2align 4,,10
	.p2align 3
.L32:
	movq	%rdx, (%rsi)
	xorl	%r15d, %r15d
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L82:
	movq	$0, (%rsi)
	testb	%r13b, %r13b
	je	.L19
	movq	%r14, %rax
	xorl	%edx, %edx
	movl	$1, %r15d
	subq	%r12, %rax
	subq	%rax, %r8
	cmpq	%rdx, %r8
	jge	.L80
.L33:
	movl	$91, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$155, %edx
	movl	$114, %esi
	movl	$13, %edi
	orl	$-128, %r13d
	call	ERR_put_error@PLT
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L31:
	movslq	%ecx, %rcx
	leaq	1(%r14,%rcx), %r14
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L28:
	xorl	%edx, %edx
	jmp	.L32
	.cfi_endproc
.LFE470:
	.size	ASN1_get_object, .-ASN1_get_object
	.p2align 4
	.globl	ASN1_put_object
	.type	ASN1_put_object, @function
ASN1_put_object:
.LFB472:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testl	%esi, %esi
	movq	(%rdi), %r9
	setne	%al
	andl	$192, %r8d
	sall	$5, %eax
	leaq	1(%r9), %r11
	orl	%eax, %r8d
	cmpl	$30, %ecx
	jle	.L118
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	orl	$31, %r8d
	movl	%ecx, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movb	%r8b, (%r9)
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L88:
	movl	%r8d, %eax
	addl	$1, %r8d
	sarl	$7, %r10d
	jne	.L88
	movl	%ecx, %ebx
	movslq	%eax, %r10
	sarl	$7, %ecx
	andl	$127, %ebx
	movb	%bl, 1(%r9,%r10)
	leal	-1(%rax), %r10d
	testl	%eax, %eax
	je	.L89
	movslq	%r10d, %r10
	addq	%r10, %r9
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L119:
	orl	$-128, %r10d
	sarl	$7, %ecx
	subq	$1, %r9
	movb	%r10b, 2(%r9)
	subl	$1, %eax
	je	.L89
.L92:
	movl	%ecx, %r10d
	andl	$127, %r10d
	cmpl	%r8d, %eax
	jne	.L119
	movb	%r10b, 1(%r9)
	sarl	$7, %ecx
	subq	$1, %r9
	subl	$1, %eax
	jne	.L92
	.p2align 4,,10
	.p2align 3
.L89:
	movslq	%r8d, %r8
	addq	%r8, %r11
	leaq	1(%r11), %r8
	cmpl	$2, %esi
	jne	.L93
	movb	$-128, (%r11)
	popq	%rbx
	movq	%r8, (%rdi)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore 3
	.cfi_restore 6
	andl	$31, %ecx
	orl	%ecx, %r8d
	movb	%r8b, (%r9)
	leaq	1(%r11), %r8
	cmpl	$2, %esi
	jne	.L109
	movb	$-128, (%r11)
	movq	%r8, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	cmpl	$127, %edx
	jle	.L120
	movl	%edx, %eax
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L113:
	movslq	%ecx, %rsi
	addl	$1, %ecx
	sarl	$8, %eax
	jne	.L113
	movl	%ecx, %eax
	orl	$-128, %eax
	movb	%al, (%r11)
	leaq	1(%r11,%rsi), %rax
	.p2align 4,,10
	.p2align 3
.L115:
	movb	%dl, (%rax)
	subq	$1, %rax
	sarl	$8, %edx
	cmpq	%rax, %r11
	jne	.L115
	movslq	%ecx, %rcx
	addq	%rcx, %r8
	movq	%r8, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	cmpl	$127, %edx
	jle	.L121
	movl	%edx, %eax
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L95:
	movslq	%ecx, %rsi
	addl	$1, %ecx
	sarl	$8, %eax
	jne	.L95
	movl	%ecx, %eax
	orl	$-128, %eax
	movb	%al, (%r11)
	leaq	1(%r11,%rsi), %rax
	.p2align 4,,10
	.p2align 3
.L96:
	movb	%dl, (%rax)
	subq	$1, %rax
	sarl	$8, %edx
	cmpq	%rax, %r11
	jne	.L96
	movslq	%ecx, %rcx
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	addq	%rcx, %r8
	movq	%r8, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	movb	%dl, (%r11)
	popq	%rbx
	movq	%r8, (%rdi)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore 3
	.cfi_restore 6
	movb	%dl, (%r11)
	movq	%r8, (%rdi)
	ret
	.cfi_endproc
.LFE472:
	.size	ASN1_put_object, .-ASN1_put_object
	.p2align 4
	.globl	ASN1_put_eoc
	.type	ASN1_put_eoc, @function
ASN1_put_eoc:
.LFB473:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	xorl	%edx, %edx
	movw	%dx, (%rax)
	addq	$2, %rax
	movq	%rax, (%rdi)
	movl	$2, %eax
	ret
	.cfi_endproc
.LFE473:
	.size	ASN1_put_eoc, .-ASN1_put_eoc
	.p2align 4
	.globl	ASN1_object_size
	.type	ASN1_object_size, @function
ASN1_object_size:
.LFB475:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	js	.L132
	movl	$1, %eax
	cmpl	$30, %edx
	jg	.L126
.L125:
	cmpl	$2, %edi
	je	.L136
	addl	$1, %eax
	cmpl	$127, %esi
	jle	.L128
	movl	%esi, %edx
	.p2align 4,,10
	.p2align 3
.L129:
	addl	$1, %eax
	sarl	$8, %edx
	jne	.L129
.L128:
	movl	$2147483647, %edx
	subl	%esi, %edx
	cmpl	%eax, %edx
	jle	.L132
	addl	%esi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L126:
	addl	$1, %eax
	sarl	$7, %edx
	je	.L125
	addl	$1, %eax
	sarl	$7, %edx
	jne	.L126
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L136:
	addl	$3, %eax
	jmp	.L128
.L132:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE475:
	.size	ASN1_object_size, .-ASN1_object_size
	.p2align 4
	.globl	ASN1_STRING_copy
	.type	ASN1_STRING_copy, @function
ASN1_STRING_copy:
.LFB476:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L158
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	4(%rsi), %eax
	movslq	(%rsi), %r13
	movq	8(%rsi), %r14
	movl	%eax, 4(%rdi)
	testl	%r13d, %r13d
	js	.L159
	cmpq	$2147483646, %r13
	ja	.L160
.L143:
	movslq	(%rbx), %rax
	movq	8(%rbx), %r15
	cmpq	%rax, %r13
	jnb	.L144
	movq	%r15, %rdi
	testq	%r15, %r15
	je	.L144
.L145:
	movl	%r13d, (%rbx)
	testq	%r14, %r14
	je	.L146
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	8(%rbx), %rax
	movb	$0, (%rax,%r13)
.L146:
	movq	16(%rbx), %rdx
	andl	$128, %edx
	movq	%rdx, 16(%rbx)
	movq	16(%r12), %rax
	andb	$127, %al
	orq	%rdx, %rax
	movq	%rax, 16(%rbx)
	movl	$1, %eax
.L137:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore_state
	testq	%r14, %r14
	je	.L157
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rax, %r13
	cmpq	$2147483646, %r13
	jbe	.L143
.L160:
	movl	$290, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$223, %edx
	xorl	%esi, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
.L157:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	movq	%r15, %rdi
	leaq	1(%r13), %rsi
	movl	$295, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_realloc@PLT
	movq	%rax, 8(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L145
	movl	$297, %r8d
	movl	$65, %edx
	movl	$186, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r15, 8(%rbx)
	xorl	%eax, %eax
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE476:
	.size	ASN1_STRING_copy, .-ASN1_STRING_copy
	.p2align 4
	.globl	ASN1_STRING_dup
	.type	ASN1_STRING_dup, @function
ASN1_STRING_dup:
.LFB477:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdi, %rdi
	je	.L183
	movq	%rdi, %rbx
	movl	$327, %edx
	leaq	.LC0(%rip), %rsi
	movl	$24, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L184
	movslq	(%rbx), %r13
	movq	8(%rbx), %r14
	movl	$4, 4(%rax)
	movl	4(%rbx), %eax
	movl	%eax, 4(%r12)
	testl	%r13d, %r13d
	js	.L185
.L167:
	cmpq	$2147483646, %r13
	ja	.L186
	movslq	(%r12), %rax
	movq	8(%r12), %r15
	cmpq	%rax, %r13
	jnb	.L169
	movq	%r15, %rdi
	testq	%r15, %r15
	je	.L169
.L170:
	movl	%r13d, (%r12)
	testq	%r14, %r14
	je	.L171
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	8(%r12), %rax
	movb	$0, (%rax,%r13)
.L171:
	movq	16(%r12), %rdx
	andl	$128, %edx
	movq	%rdx, 16(%r12)
	movq	16(%rbx), %rax
	andb	$127, %al
	orq	%rdx, %rax
	movq	%rax, 16(%r12)
.L161:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	.cfi_restore_state
	testq	%r14, %r14
	je	.L166
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rax, %r13
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L186:
	movl	$290, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$223, %edx
	xorl	%esi, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
.L166:
	movq	16(%r12), %rbx
	testb	$16, %bl
	je	.L187
.L173:
	andl	$128, %ebx
	jne	.L183
	movl	$343, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L183:
	xorl	%r12d, %r12d
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L169:
	movq	%r15, %rdi
	leaq	1(%r13), %rsi
	movl	$295, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_realloc@PLT
	movq	%rax, 8(%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L170
	movl	$297, %r8d
	movl	$65, %edx
	movl	$186, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r15, 8(%r12)
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L184:
	movl	$329, %r8d
	movl	$65, %edx
	movl	$130, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L187:
	movq	8(%r12), %rdi
	movl	$341, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L173
	.cfi_endproc
.LFE477:
	.size	ASN1_STRING_dup, .-ASN1_STRING_dup
	.p2align 4
	.globl	ASN1_STRING_set
	.type	ASN1_STRING_set, @function
ASN1_STRING_set:
.LFB478:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movslq	%edx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	testl	%edx, %edx
	js	.L201
	cmpq	$2147483646, %r12
	ja	.L202
.L192:
	movslq	(%rbx), %rax
	movq	8(%rbx), %r14
	cmpq	%r12, %rax
	jbe	.L193
	movq	%r14, %rdi
	testq	%r14, %r14
	je	.L193
.L194:
	movl	%r12d, (%rbx)
	movl	$1, %r14d
	testq	%r13, %r13
	je	.L188
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	8(%rbx), %rax
	movb	$0, (%rax,%r12)
.L188:
	popq	%rbx
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L193:
	.cfi_restore_state
	movq	%r14, %rdi
	leaq	1(%r12), %rsi
	movl	$295, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_realloc@PLT
	movq	%rax, 8(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L194
	movl	$297, %r8d
	movl	$65, %edx
	movl	$186, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r14, 8(%rbx)
	xorl	%r14d, %r14d
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L201:
	testq	%rsi, %rsi
	je	.L195
	movq	%rsi, %rdi
	call	strlen@PLT
	movq	%rax, %r12
	cmpq	$2147483646, %r12
	jbe	.L192
.L202:
	xorl	%r14d, %r14d
	movl	$290, %r8d
	movl	$223, %edx
	xorl	%esi, %esi
	leaq	.LC0(%rip), %rcx
	movl	$13, %edi
	call	ERR_put_error@PLT
	popq	%rbx
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L195:
	.cfi_restore_state
	xorl	%r14d, %r14d
	jmp	.L188
	.cfi_endproc
.LFE478:
	.size	ASN1_STRING_set, .-ASN1_STRING_set
	.p2align 4
	.globl	ASN1_STRING_set0
	.type	ASN1_STRING_set0, @function
ASN1_STRING_set0:
.LFB479:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	leaq	.LC0(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movl	%edx, %r12d
	movl	$313, %edx
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	call	CRYPTO_free@PLT
	movq	%r13, 8(%rbx)
	movl	%r12d, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE479:
	.size	ASN1_STRING_set0, .-ASN1_STRING_set0
	.p2align 4
	.globl	ASN1_STRING_new
	.type	ASN1_STRING_new, @function
ASN1_STRING_new:
.LFB480:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$327, %edx
	movl	$24, %edi
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L209
	movl	$4, 4(%rax)
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	.cfi_restore_state
	movl	$329, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$130, %esi
	movl	$13, %edi
	movq	%rax, -8(%rbp)
	call	ERR_put_error@PLT
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE480:
	.size	ASN1_STRING_new, .-ASN1_STRING_new
	.p2align 4
	.globl	ASN1_STRING_type_new
	.type	ASN1_STRING_type_new, @function
ASN1_STRING_type_new:
.LFB481:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$327, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%edi, %ebx
	movl	$24, %edi
	subq	$24, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L214
	movl	%ebx, 4(%rax)
.L210:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L214:
	.cfi_restore_state
	movl	$329, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$130, %esi
	movl	$13, %edi
	movq	%rax, -24(%rbp)
	call	ERR_put_error@PLT
	movq	-24(%rbp), %rax
	jmp	.L210
	.cfi_endproc
.LFE481:
	.size	ASN1_STRING_type_new, .-ASN1_STRING_type_new
	.p2align 4
	.globl	asn1_string_embed_free
	.type	asn1_string_embed_free, @function
asn1_string_embed_free:
.LFB482:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L221
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	testb	$16, 16(%rdi)
	je	.L224
	testl	%ebx, %ebx
	je	.L225
.L215:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L224:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	movl	$341, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	testl	%ebx, %ebx
	jne	.L215
.L225:
	popq	%rbx
	.cfi_restore 3
	movq	%r12, %rdi
	movl	$343, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L221:
	ret
	.cfi_endproc
.LFE482:
	.size	asn1_string_embed_free, .-asn1_string_embed_free
	.p2align 4
	.globl	ASN1_STRING_free
	.type	ASN1_STRING_free, @function
ASN1_STRING_free:
.LFB483:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L232
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	16(%rdi), %rax
	movl	%eax, %ebx
	andl	$128, %ebx
	testb	$16, %al
	je	.L235
	testl	%ebx, %ebx
	je	.L236
.L226:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L232:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.p2align 4,,10
	.p2align 3
.L235:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	8(%rdi), %rdi
	movl	$341, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	testl	%ebx, %ebx
	jne	.L226
.L236:
	popq	%rbx
	.cfi_restore 3
	movq	%r12, %rdi
	movl	$343, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE483:
	.size	ASN1_STRING_free, .-ASN1_STRING_free
	.p2align 4
	.globl	ASN1_STRING_clear_free
	.type	ASN1_STRING_clear_free, @function
ASN1_STRING_clear_free:
.LFB484:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L248
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	16(%r12), %rax
	movq	8(%rdi), %rdi
	movq	%rax, %rdx
	andl	$16, %edx
	testq	%rdi, %rdi
	je	.L240
	movl	%eax, %ebx
	andl	$128, %ebx
	testq	%rdx, %rdx
	je	.L251
.L242:
	testl	%ebx, %ebx
	je	.L252
.L237:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L252:
	.cfi_restore_state
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	movq	%r12, %rdi
	movl	$343, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L251:
	.cfi_restore_state
	movslq	(%r12), %rsi
	call	OPENSSL_cleanse@PLT
	movq	16(%r12), %rax
	movq	%rax, %rdx
	andl	$16, %edx
.L240:
	movl	%eax, %ebx
	andl	$128, %ebx
	testq	%rdx, %rdx
	jne	.L242
	movq	8(%r12), %rdi
	movl	$341, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	testl	%ebx, %ebx
	jne	.L237
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L248:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE484:
	.size	ASN1_STRING_clear_free, .-ASN1_STRING_clear_free
	.p2align 4
	.globl	ASN1_STRING_cmp
	.type	ASN1_STRING_cmp, @function
ASN1_STRING_cmp:
.LFB485:
	.cfi_startproc
	endbr64
	movslq	(%rdi), %rdx
	movl	%edx, %eax
	subl	(%rsi), %eax
	jne	.L256
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movq	8(%rdi), %rdi
	movq	8(%rsi), %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L253
	movl	4(%r12), %eax
	subl	4(%rbx), %eax
.L253:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L256:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE485:
	.size	ASN1_STRING_cmp, .-ASN1_STRING_cmp
	.p2align 4
	.globl	ASN1_STRING_length
	.type	ASN1_STRING_length, @function
ASN1_STRING_length:
.LFB486:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	ret
	.cfi_endproc
.LFE486:
	.size	ASN1_STRING_length, .-ASN1_STRING_length
	.p2align 4
	.globl	ASN1_STRING_length_set
	.type	ASN1_STRING_length_set, @function
ASN1_STRING_length_set:
.LFB487:
	.cfi_startproc
	endbr64
	movl	%esi, (%rdi)
	ret
	.cfi_endproc
.LFE487:
	.size	ASN1_STRING_length_set, .-ASN1_STRING_length_set
	.p2align 4
	.globl	ASN1_STRING_type
	.type	ASN1_STRING_type, @function
ASN1_STRING_type:
.LFB488:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	ret
	.cfi_endproc
.LFE488:
	.size	ASN1_STRING_type, .-ASN1_STRING_type
	.p2align 4
	.globl	ASN1_STRING_get0_data
	.type	ASN1_STRING_get0_data, @function
ASN1_STRING_get0_data:
.LFB497:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE497:
	.size	ASN1_STRING_get0_data, .-ASN1_STRING_get0_data
	.p2align 4
	.globl	ASN1_STRING_data
	.type	ASN1_STRING_data, @function
ASN1_STRING_data:
.LFB490:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE490:
	.size	ASN1_STRING_data, .-ASN1_STRING_data
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
