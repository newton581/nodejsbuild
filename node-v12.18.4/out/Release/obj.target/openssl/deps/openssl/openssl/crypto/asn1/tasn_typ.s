	.file	"tasn_typ.c"
	.text
	.p2align 4
	.globl	d2i_ASN1_OCTET_STRING
	.type	d2i_ASN1_OCTET_STRING, @function
d2i_ASN1_OCTET_STRING:
.LFB356:
	.cfi_startproc
	endbr64
	leaq	ASN1_OCTET_STRING_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE356:
	.size	d2i_ASN1_OCTET_STRING, .-d2i_ASN1_OCTET_STRING
	.p2align 4
	.globl	i2d_ASN1_OCTET_STRING
	.type	i2d_ASN1_OCTET_STRING, @function
i2d_ASN1_OCTET_STRING:
.LFB357:
	.cfi_startproc
	endbr64
	leaq	ASN1_OCTET_STRING_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE357:
	.size	i2d_ASN1_OCTET_STRING, .-i2d_ASN1_OCTET_STRING
	.p2align 4
	.globl	ASN1_OCTET_STRING_new
	.type	ASN1_OCTET_STRING_new, @function
ASN1_OCTET_STRING_new:
.LFB358:
	.cfi_startproc
	endbr64
	movl	$4, %edi
	jmp	ASN1_STRING_type_new@PLT
	.cfi_endproc
.LFE358:
	.size	ASN1_OCTET_STRING_new, .-ASN1_OCTET_STRING_new
	.p2align 4
	.globl	ASN1_OCTET_STRING_free
	.type	ASN1_OCTET_STRING_free, @function
ASN1_OCTET_STRING_free:
.LFB441:
	.cfi_startproc
	endbr64
	jmp	ASN1_STRING_free@PLT
	.cfi_endproc
.LFE441:
	.size	ASN1_OCTET_STRING_free, .-ASN1_OCTET_STRING_free
	.p2align 4
	.globl	d2i_ASN1_INTEGER
	.type	d2i_ASN1_INTEGER, @function
d2i_ASN1_INTEGER:
.LFB360:
	.cfi_startproc
	endbr64
	leaq	ASN1_INTEGER_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE360:
	.size	d2i_ASN1_INTEGER, .-d2i_ASN1_INTEGER
	.p2align 4
	.globl	i2d_ASN1_INTEGER
	.type	i2d_ASN1_INTEGER, @function
i2d_ASN1_INTEGER:
.LFB361:
	.cfi_startproc
	endbr64
	leaq	ASN1_INTEGER_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE361:
	.size	i2d_ASN1_INTEGER, .-i2d_ASN1_INTEGER
	.p2align 4
	.globl	ASN1_INTEGER_new
	.type	ASN1_INTEGER_new, @function
ASN1_INTEGER_new:
.LFB362:
	.cfi_startproc
	endbr64
	movl	$2, %edi
	jmp	ASN1_STRING_type_new@PLT
	.cfi_endproc
.LFE362:
	.size	ASN1_INTEGER_new, .-ASN1_INTEGER_new
	.p2align 4
	.globl	ASN1_INTEGER_free
	.type	ASN1_INTEGER_free, @function
ASN1_INTEGER_free:
.LFB437:
	.cfi_startproc
	endbr64
	jmp	ASN1_STRING_free@PLT
	.cfi_endproc
.LFE437:
	.size	ASN1_INTEGER_free, .-ASN1_INTEGER_free
	.p2align 4
	.globl	d2i_ASN1_ENUMERATED
	.type	d2i_ASN1_ENUMERATED, @function
d2i_ASN1_ENUMERATED:
.LFB364:
	.cfi_startproc
	endbr64
	leaq	ASN1_ENUMERATED_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE364:
	.size	d2i_ASN1_ENUMERATED, .-d2i_ASN1_ENUMERATED
	.p2align 4
	.globl	i2d_ASN1_ENUMERATED
	.type	i2d_ASN1_ENUMERATED, @function
i2d_ASN1_ENUMERATED:
.LFB365:
	.cfi_startproc
	endbr64
	leaq	ASN1_ENUMERATED_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE365:
	.size	i2d_ASN1_ENUMERATED, .-i2d_ASN1_ENUMERATED
	.p2align 4
	.globl	ASN1_ENUMERATED_new
	.type	ASN1_ENUMERATED_new, @function
ASN1_ENUMERATED_new:
.LFB366:
	.cfi_startproc
	endbr64
	movl	$10, %edi
	jmp	ASN1_STRING_type_new@PLT
	.cfi_endproc
.LFE366:
	.size	ASN1_ENUMERATED_new, .-ASN1_ENUMERATED_new
	.p2align 4
	.globl	ASN1_ENUMERATED_free
	.type	ASN1_ENUMERATED_free, @function
ASN1_ENUMERATED_free:
.LFB439:
	.cfi_startproc
	endbr64
	jmp	ASN1_STRING_free@PLT
	.cfi_endproc
.LFE439:
	.size	ASN1_ENUMERATED_free, .-ASN1_ENUMERATED_free
	.p2align 4
	.globl	d2i_ASN1_BIT_STRING
	.type	d2i_ASN1_BIT_STRING, @function
d2i_ASN1_BIT_STRING:
.LFB368:
	.cfi_startproc
	endbr64
	leaq	ASN1_BIT_STRING_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE368:
	.size	d2i_ASN1_BIT_STRING, .-d2i_ASN1_BIT_STRING
	.p2align 4
	.globl	i2d_ASN1_BIT_STRING
	.type	i2d_ASN1_BIT_STRING, @function
i2d_ASN1_BIT_STRING:
.LFB369:
	.cfi_startproc
	endbr64
	leaq	ASN1_BIT_STRING_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE369:
	.size	i2d_ASN1_BIT_STRING, .-i2d_ASN1_BIT_STRING
	.p2align 4
	.globl	ASN1_BIT_STRING_new
	.type	ASN1_BIT_STRING_new, @function
ASN1_BIT_STRING_new:
.LFB370:
	.cfi_startproc
	endbr64
	movl	$3, %edi
	jmp	ASN1_STRING_type_new@PLT
	.cfi_endproc
.LFE370:
	.size	ASN1_BIT_STRING_new, .-ASN1_BIT_STRING_new
	.p2align 4
	.globl	ASN1_BIT_STRING_free
	.type	ASN1_BIT_STRING_free, @function
ASN1_BIT_STRING_free:
.LFB371:
	.cfi_startproc
	endbr64
	jmp	ASN1_STRING_free@PLT
	.cfi_endproc
.LFE371:
	.size	ASN1_BIT_STRING_free, .-ASN1_BIT_STRING_free
	.p2align 4
	.globl	d2i_ASN1_UTF8STRING
	.type	d2i_ASN1_UTF8STRING, @function
d2i_ASN1_UTF8STRING:
.LFB372:
	.cfi_startproc
	endbr64
	leaq	ASN1_UTF8STRING_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE372:
	.size	d2i_ASN1_UTF8STRING, .-d2i_ASN1_UTF8STRING
	.p2align 4
	.globl	i2d_ASN1_UTF8STRING
	.type	i2d_ASN1_UTF8STRING, @function
i2d_ASN1_UTF8STRING:
.LFB373:
	.cfi_startproc
	endbr64
	leaq	ASN1_UTF8STRING_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE373:
	.size	i2d_ASN1_UTF8STRING, .-i2d_ASN1_UTF8STRING
	.p2align 4
	.globl	ASN1_UTF8STRING_new
	.type	ASN1_UTF8STRING_new, @function
ASN1_UTF8STRING_new:
.LFB374:
	.cfi_startproc
	endbr64
	movl	$12, %edi
	jmp	ASN1_STRING_type_new@PLT
	.cfi_endproc
.LFE374:
	.size	ASN1_UTF8STRING_new, .-ASN1_UTF8STRING_new
	.p2align 4
	.globl	ASN1_UTF8STRING_free
	.type	ASN1_UTF8STRING_free, @function
ASN1_UTF8STRING_free:
.LFB447:
	.cfi_startproc
	endbr64
	jmp	ASN1_STRING_free@PLT
	.cfi_endproc
.LFE447:
	.size	ASN1_UTF8STRING_free, .-ASN1_UTF8STRING_free
	.p2align 4
	.globl	d2i_ASN1_PRINTABLESTRING
	.type	d2i_ASN1_PRINTABLESTRING, @function
d2i_ASN1_PRINTABLESTRING:
.LFB376:
	.cfi_startproc
	endbr64
	leaq	ASN1_PRINTABLESTRING_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE376:
	.size	d2i_ASN1_PRINTABLESTRING, .-d2i_ASN1_PRINTABLESTRING
	.p2align 4
	.globl	i2d_ASN1_PRINTABLESTRING
	.type	i2d_ASN1_PRINTABLESTRING, @function
i2d_ASN1_PRINTABLESTRING:
.LFB377:
	.cfi_startproc
	endbr64
	leaq	ASN1_PRINTABLESTRING_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE377:
	.size	i2d_ASN1_PRINTABLESTRING, .-i2d_ASN1_PRINTABLESTRING
	.p2align 4
	.globl	ASN1_PRINTABLESTRING_new
	.type	ASN1_PRINTABLESTRING_new, @function
ASN1_PRINTABLESTRING_new:
.LFB378:
	.cfi_startproc
	endbr64
	movl	$19, %edi
	jmp	ASN1_STRING_type_new@PLT
	.cfi_endproc
.LFE378:
	.size	ASN1_PRINTABLESTRING_new, .-ASN1_PRINTABLESTRING_new
	.p2align 4
	.globl	ASN1_PRINTABLESTRING_free
	.type	ASN1_PRINTABLESTRING_free, @function
ASN1_PRINTABLESTRING_free:
.LFB451:
	.cfi_startproc
	endbr64
	jmp	ASN1_STRING_free@PLT
	.cfi_endproc
.LFE451:
	.size	ASN1_PRINTABLESTRING_free, .-ASN1_PRINTABLESTRING_free
	.p2align 4
	.globl	d2i_ASN1_T61STRING
	.type	d2i_ASN1_T61STRING, @function
d2i_ASN1_T61STRING:
.LFB380:
	.cfi_startproc
	endbr64
	leaq	ASN1_T61STRING_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE380:
	.size	d2i_ASN1_T61STRING, .-d2i_ASN1_T61STRING
	.p2align 4
	.globl	i2d_ASN1_T61STRING
	.type	i2d_ASN1_T61STRING, @function
i2d_ASN1_T61STRING:
.LFB381:
	.cfi_startproc
	endbr64
	leaq	ASN1_T61STRING_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE381:
	.size	i2d_ASN1_T61STRING, .-i2d_ASN1_T61STRING
	.p2align 4
	.globl	ASN1_T61STRING_new
	.type	ASN1_T61STRING_new, @function
ASN1_T61STRING_new:
.LFB382:
	.cfi_startproc
	endbr64
	movl	$20, %edi
	jmp	ASN1_STRING_type_new@PLT
	.cfi_endproc
.LFE382:
	.size	ASN1_T61STRING_new, .-ASN1_T61STRING_new
	.p2align 4
	.globl	ASN1_T61STRING_free
	.type	ASN1_T61STRING_free, @function
ASN1_T61STRING_free:
.LFB453:
	.cfi_startproc
	endbr64
	jmp	ASN1_STRING_free@PLT
	.cfi_endproc
.LFE453:
	.size	ASN1_T61STRING_free, .-ASN1_T61STRING_free
	.p2align 4
	.globl	d2i_ASN1_IA5STRING
	.type	d2i_ASN1_IA5STRING, @function
d2i_ASN1_IA5STRING:
.LFB384:
	.cfi_startproc
	endbr64
	leaq	ASN1_IA5STRING_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE384:
	.size	d2i_ASN1_IA5STRING, .-d2i_ASN1_IA5STRING
	.p2align 4
	.globl	i2d_ASN1_IA5STRING
	.type	i2d_ASN1_IA5STRING, @function
i2d_ASN1_IA5STRING:
.LFB385:
	.cfi_startproc
	endbr64
	leaq	ASN1_IA5STRING_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE385:
	.size	i2d_ASN1_IA5STRING, .-i2d_ASN1_IA5STRING
	.p2align 4
	.globl	ASN1_IA5STRING_new
	.type	ASN1_IA5STRING_new, @function
ASN1_IA5STRING_new:
.LFB386:
	.cfi_startproc
	endbr64
	movl	$22, %edi
	jmp	ASN1_STRING_type_new@PLT
	.cfi_endproc
.LFE386:
	.size	ASN1_IA5STRING_new, .-ASN1_IA5STRING_new
	.p2align 4
	.globl	ASN1_IA5STRING_free
	.type	ASN1_IA5STRING_free, @function
ASN1_IA5STRING_free:
.LFB455:
	.cfi_startproc
	endbr64
	jmp	ASN1_STRING_free@PLT
	.cfi_endproc
.LFE455:
	.size	ASN1_IA5STRING_free, .-ASN1_IA5STRING_free
	.p2align 4
	.globl	d2i_ASN1_GENERALSTRING
	.type	d2i_ASN1_GENERALSTRING, @function
d2i_ASN1_GENERALSTRING:
.LFB388:
	.cfi_startproc
	endbr64
	leaq	ASN1_GENERALSTRING_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE388:
	.size	d2i_ASN1_GENERALSTRING, .-d2i_ASN1_GENERALSTRING
	.p2align 4
	.globl	i2d_ASN1_GENERALSTRING
	.type	i2d_ASN1_GENERALSTRING, @function
i2d_ASN1_GENERALSTRING:
.LFB389:
	.cfi_startproc
	endbr64
	leaq	ASN1_GENERALSTRING_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE389:
	.size	i2d_ASN1_GENERALSTRING, .-i2d_ASN1_GENERALSTRING
	.p2align 4
	.globl	ASN1_GENERALSTRING_new
	.type	ASN1_GENERALSTRING_new, @function
ASN1_GENERALSTRING_new:
.LFB390:
	.cfi_startproc
	endbr64
	movl	$27, %edi
	jmp	ASN1_STRING_type_new@PLT
	.cfi_endproc
.LFE390:
	.size	ASN1_GENERALSTRING_new, .-ASN1_GENERALSTRING_new
	.p2align 4
	.globl	ASN1_GENERALSTRING_free
	.type	ASN1_GENERALSTRING_free, @function
ASN1_GENERALSTRING_free:
.LFB457:
	.cfi_startproc
	endbr64
	jmp	ASN1_STRING_free@PLT
	.cfi_endproc
.LFE457:
	.size	ASN1_GENERALSTRING_free, .-ASN1_GENERALSTRING_free
	.p2align 4
	.globl	d2i_ASN1_UTCTIME
	.type	d2i_ASN1_UTCTIME, @function
d2i_ASN1_UTCTIME:
.LFB392:
	.cfi_startproc
	endbr64
	leaq	ASN1_UTCTIME_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE392:
	.size	d2i_ASN1_UTCTIME, .-d2i_ASN1_UTCTIME
	.p2align 4
	.globl	i2d_ASN1_UTCTIME
	.type	i2d_ASN1_UTCTIME, @function
i2d_ASN1_UTCTIME:
.LFB393:
	.cfi_startproc
	endbr64
	leaq	ASN1_UTCTIME_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE393:
	.size	i2d_ASN1_UTCTIME, .-i2d_ASN1_UTCTIME
	.p2align 4
	.globl	ASN1_UTCTIME_new
	.type	ASN1_UTCTIME_new, @function
ASN1_UTCTIME_new:
.LFB394:
	.cfi_startproc
	endbr64
	movl	$23, %edi
	jmp	ASN1_STRING_type_new@PLT
	.cfi_endproc
.LFE394:
	.size	ASN1_UTCTIME_new, .-ASN1_UTCTIME_new
	.p2align 4
	.globl	ASN1_UTCTIME_free
	.type	ASN1_UTCTIME_free, @function
ASN1_UTCTIME_free:
.LFB459:
	.cfi_startproc
	endbr64
	jmp	ASN1_STRING_free@PLT
	.cfi_endproc
.LFE459:
	.size	ASN1_UTCTIME_free, .-ASN1_UTCTIME_free
	.p2align 4
	.globl	d2i_ASN1_GENERALIZEDTIME
	.type	d2i_ASN1_GENERALIZEDTIME, @function
d2i_ASN1_GENERALIZEDTIME:
.LFB396:
	.cfi_startproc
	endbr64
	leaq	ASN1_GENERALIZEDTIME_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE396:
	.size	d2i_ASN1_GENERALIZEDTIME, .-d2i_ASN1_GENERALIZEDTIME
	.p2align 4
	.globl	i2d_ASN1_GENERALIZEDTIME
	.type	i2d_ASN1_GENERALIZEDTIME, @function
i2d_ASN1_GENERALIZEDTIME:
.LFB397:
	.cfi_startproc
	endbr64
	leaq	ASN1_GENERALIZEDTIME_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE397:
	.size	i2d_ASN1_GENERALIZEDTIME, .-i2d_ASN1_GENERALIZEDTIME
	.p2align 4
	.globl	ASN1_GENERALIZEDTIME_new
	.type	ASN1_GENERALIZEDTIME_new, @function
ASN1_GENERALIZEDTIME_new:
.LFB398:
	.cfi_startproc
	endbr64
	movl	$24, %edi
	jmp	ASN1_STRING_type_new@PLT
	.cfi_endproc
.LFE398:
	.size	ASN1_GENERALIZEDTIME_new, .-ASN1_GENERALIZEDTIME_new
	.p2align 4
	.globl	ASN1_GENERALIZEDTIME_free
	.type	ASN1_GENERALIZEDTIME_free, @function
ASN1_GENERALIZEDTIME_free:
.LFB461:
	.cfi_startproc
	endbr64
	jmp	ASN1_STRING_free@PLT
	.cfi_endproc
.LFE461:
	.size	ASN1_GENERALIZEDTIME_free, .-ASN1_GENERALIZEDTIME_free
	.p2align 4
	.globl	d2i_ASN1_VISIBLESTRING
	.type	d2i_ASN1_VISIBLESTRING, @function
d2i_ASN1_VISIBLESTRING:
.LFB400:
	.cfi_startproc
	endbr64
	leaq	ASN1_VISIBLESTRING_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE400:
	.size	d2i_ASN1_VISIBLESTRING, .-d2i_ASN1_VISIBLESTRING
	.p2align 4
	.globl	i2d_ASN1_VISIBLESTRING
	.type	i2d_ASN1_VISIBLESTRING, @function
i2d_ASN1_VISIBLESTRING:
.LFB401:
	.cfi_startproc
	endbr64
	leaq	ASN1_VISIBLESTRING_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE401:
	.size	i2d_ASN1_VISIBLESTRING, .-i2d_ASN1_VISIBLESTRING
	.p2align 4
	.globl	ASN1_VISIBLESTRING_new
	.type	ASN1_VISIBLESTRING_new, @function
ASN1_VISIBLESTRING_new:
.LFB402:
	.cfi_startproc
	endbr64
	movl	$26, %edi
	jmp	ASN1_STRING_type_new@PLT
	.cfi_endproc
.LFE402:
	.size	ASN1_VISIBLESTRING_new, .-ASN1_VISIBLESTRING_new
	.p2align 4
	.globl	ASN1_VISIBLESTRING_free
	.type	ASN1_VISIBLESTRING_free, @function
ASN1_VISIBLESTRING_free:
.LFB443:
	.cfi_startproc
	endbr64
	jmp	ASN1_STRING_free@PLT
	.cfi_endproc
.LFE443:
	.size	ASN1_VISIBLESTRING_free, .-ASN1_VISIBLESTRING_free
	.p2align 4
	.globl	d2i_ASN1_UNIVERSALSTRING
	.type	d2i_ASN1_UNIVERSALSTRING, @function
d2i_ASN1_UNIVERSALSTRING:
.LFB404:
	.cfi_startproc
	endbr64
	leaq	ASN1_UNIVERSALSTRING_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE404:
	.size	d2i_ASN1_UNIVERSALSTRING, .-d2i_ASN1_UNIVERSALSTRING
	.p2align 4
	.globl	i2d_ASN1_UNIVERSALSTRING
	.type	i2d_ASN1_UNIVERSALSTRING, @function
i2d_ASN1_UNIVERSALSTRING:
.LFB405:
	.cfi_startproc
	endbr64
	leaq	ASN1_UNIVERSALSTRING_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE405:
	.size	i2d_ASN1_UNIVERSALSTRING, .-i2d_ASN1_UNIVERSALSTRING
	.p2align 4
	.globl	ASN1_UNIVERSALSTRING_new
	.type	ASN1_UNIVERSALSTRING_new, @function
ASN1_UNIVERSALSTRING_new:
.LFB406:
	.cfi_startproc
	endbr64
	movl	$28, %edi
	jmp	ASN1_STRING_type_new@PLT
	.cfi_endproc
.LFE406:
	.size	ASN1_UNIVERSALSTRING_new, .-ASN1_UNIVERSALSTRING_new
	.p2align 4
	.globl	ASN1_UNIVERSALSTRING_free
	.type	ASN1_UNIVERSALSTRING_free, @function
ASN1_UNIVERSALSTRING_free:
.LFB445:
	.cfi_startproc
	endbr64
	jmp	ASN1_STRING_free@PLT
	.cfi_endproc
.LFE445:
	.size	ASN1_UNIVERSALSTRING_free, .-ASN1_UNIVERSALSTRING_free
	.p2align 4
	.globl	d2i_ASN1_BMPSTRING
	.type	d2i_ASN1_BMPSTRING, @function
d2i_ASN1_BMPSTRING:
.LFB408:
	.cfi_startproc
	endbr64
	leaq	ASN1_BMPSTRING_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE408:
	.size	d2i_ASN1_BMPSTRING, .-d2i_ASN1_BMPSTRING
	.p2align 4
	.globl	i2d_ASN1_BMPSTRING
	.type	i2d_ASN1_BMPSTRING, @function
i2d_ASN1_BMPSTRING:
.LFB409:
	.cfi_startproc
	endbr64
	leaq	ASN1_BMPSTRING_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE409:
	.size	i2d_ASN1_BMPSTRING, .-i2d_ASN1_BMPSTRING
	.p2align 4
	.globl	ASN1_BMPSTRING_new
	.type	ASN1_BMPSTRING_new, @function
ASN1_BMPSTRING_new:
.LFB410:
	.cfi_startproc
	endbr64
	movl	$30, %edi
	jmp	ASN1_STRING_type_new@PLT
	.cfi_endproc
.LFE410:
	.size	ASN1_BMPSTRING_new, .-ASN1_BMPSTRING_new
	.p2align 4
	.globl	ASN1_BMPSTRING_free
	.type	ASN1_BMPSTRING_free, @function
ASN1_BMPSTRING_free:
.LFB449:
	.cfi_startproc
	endbr64
	jmp	ASN1_STRING_free@PLT
	.cfi_endproc
.LFE449:
	.size	ASN1_BMPSTRING_free, .-ASN1_BMPSTRING_free
	.p2align 4
	.globl	d2i_ASN1_NULL
	.type	d2i_ASN1_NULL, @function
d2i_ASN1_NULL:
.LFB412:
	.cfi_startproc
	endbr64
	leaq	ASN1_NULL_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE412:
	.size	d2i_ASN1_NULL, .-d2i_ASN1_NULL
	.p2align 4
	.globl	i2d_ASN1_NULL
	.type	i2d_ASN1_NULL, @function
i2d_ASN1_NULL:
.LFB413:
	.cfi_startproc
	endbr64
	leaq	ASN1_NULL_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE413:
	.size	i2d_ASN1_NULL, .-i2d_ASN1_NULL
	.p2align 4
	.globl	ASN1_NULL_new
	.type	ASN1_NULL_new, @function
ASN1_NULL_new:
.LFB414:
	.cfi_startproc
	endbr64
	leaq	ASN1_NULL_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE414:
	.size	ASN1_NULL_new, .-ASN1_NULL_new
	.p2align 4
	.globl	ASN1_NULL_free
	.type	ASN1_NULL_free, @function
ASN1_NULL_free:
.LFB415:
	.cfi_startproc
	endbr64
	leaq	ASN1_NULL_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE415:
	.size	ASN1_NULL_free, .-ASN1_NULL_free
	.p2align 4
	.globl	d2i_ASN1_TYPE
	.type	d2i_ASN1_TYPE, @function
d2i_ASN1_TYPE:
.LFB416:
	.cfi_startproc
	endbr64
	leaq	ASN1_ANY_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE416:
	.size	d2i_ASN1_TYPE, .-d2i_ASN1_TYPE
	.p2align 4
	.globl	i2d_ASN1_TYPE
	.type	i2d_ASN1_TYPE, @function
i2d_ASN1_TYPE:
.LFB417:
	.cfi_startproc
	endbr64
	leaq	ASN1_ANY_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE417:
	.size	i2d_ASN1_TYPE, .-i2d_ASN1_TYPE
	.p2align 4
	.globl	ASN1_TYPE_new
	.type	ASN1_TYPE_new, @function
ASN1_TYPE_new:
.LFB418:
	.cfi_startproc
	endbr64
	leaq	ASN1_ANY_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE418:
	.size	ASN1_TYPE_new, .-ASN1_TYPE_new
	.p2align 4
	.globl	ASN1_TYPE_free
	.type	ASN1_TYPE_free, @function
ASN1_TYPE_free:
.LFB419:
	.cfi_startproc
	endbr64
	leaq	ASN1_ANY_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE419:
	.size	ASN1_TYPE_free, .-ASN1_TYPE_free
	.p2align 4
	.globl	d2i_ASN1_PRINTABLE
	.type	d2i_ASN1_PRINTABLE, @function
d2i_ASN1_PRINTABLE:
.LFB420:
	.cfi_startproc
	endbr64
	leaq	ASN1_PRINTABLE_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE420:
	.size	d2i_ASN1_PRINTABLE, .-d2i_ASN1_PRINTABLE
	.p2align 4
	.globl	i2d_ASN1_PRINTABLE
	.type	i2d_ASN1_PRINTABLE, @function
i2d_ASN1_PRINTABLE:
.LFB421:
	.cfi_startproc
	endbr64
	leaq	ASN1_PRINTABLE_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE421:
	.size	i2d_ASN1_PRINTABLE, .-i2d_ASN1_PRINTABLE
	.p2align 4
	.globl	ASN1_PRINTABLE_new
	.type	ASN1_PRINTABLE_new, @function
ASN1_PRINTABLE_new:
.LFB422:
	.cfi_startproc
	endbr64
	leaq	ASN1_PRINTABLE_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE422:
	.size	ASN1_PRINTABLE_new, .-ASN1_PRINTABLE_new
	.p2align 4
	.globl	ASN1_PRINTABLE_free
	.type	ASN1_PRINTABLE_free, @function
ASN1_PRINTABLE_free:
.LFB423:
	.cfi_startproc
	endbr64
	leaq	ASN1_PRINTABLE_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE423:
	.size	ASN1_PRINTABLE_free, .-ASN1_PRINTABLE_free
	.p2align 4
	.globl	d2i_DISPLAYTEXT
	.type	d2i_DISPLAYTEXT, @function
d2i_DISPLAYTEXT:
.LFB424:
	.cfi_startproc
	endbr64
	leaq	DISPLAYTEXT_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE424:
	.size	d2i_DISPLAYTEXT, .-d2i_DISPLAYTEXT
	.p2align 4
	.globl	i2d_DISPLAYTEXT
	.type	i2d_DISPLAYTEXT, @function
i2d_DISPLAYTEXT:
.LFB425:
	.cfi_startproc
	endbr64
	leaq	DISPLAYTEXT_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE425:
	.size	i2d_DISPLAYTEXT, .-i2d_DISPLAYTEXT
	.p2align 4
	.globl	DISPLAYTEXT_new
	.type	DISPLAYTEXT_new, @function
DISPLAYTEXT_new:
.LFB426:
	.cfi_startproc
	endbr64
	leaq	DISPLAYTEXT_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE426:
	.size	DISPLAYTEXT_new, .-DISPLAYTEXT_new
	.p2align 4
	.globl	DISPLAYTEXT_free
	.type	DISPLAYTEXT_free, @function
DISPLAYTEXT_free:
.LFB427:
	.cfi_startproc
	endbr64
	leaq	DISPLAYTEXT_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE427:
	.size	DISPLAYTEXT_free, .-DISPLAYTEXT_free
	.p2align 4
	.globl	d2i_DIRECTORYSTRING
	.type	d2i_DIRECTORYSTRING, @function
d2i_DIRECTORYSTRING:
.LFB428:
	.cfi_startproc
	endbr64
	leaq	DIRECTORYSTRING_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE428:
	.size	d2i_DIRECTORYSTRING, .-d2i_DIRECTORYSTRING
	.p2align 4
	.globl	i2d_DIRECTORYSTRING
	.type	i2d_DIRECTORYSTRING, @function
i2d_DIRECTORYSTRING:
.LFB429:
	.cfi_startproc
	endbr64
	leaq	DIRECTORYSTRING_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE429:
	.size	i2d_DIRECTORYSTRING, .-i2d_DIRECTORYSTRING
	.p2align 4
	.globl	DIRECTORYSTRING_new
	.type	DIRECTORYSTRING_new, @function
DIRECTORYSTRING_new:
.LFB430:
	.cfi_startproc
	endbr64
	leaq	DIRECTORYSTRING_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE430:
	.size	DIRECTORYSTRING_new, .-DIRECTORYSTRING_new
	.p2align 4
	.globl	DIRECTORYSTRING_free
	.type	DIRECTORYSTRING_free, @function
DIRECTORYSTRING_free:
.LFB431:
	.cfi_startproc
	endbr64
	leaq	DIRECTORYSTRING_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE431:
	.size	DIRECTORYSTRING_free, .-DIRECTORYSTRING_free
	.p2align 4
	.globl	d2i_ASN1_SEQUENCE_ANY
	.type	d2i_ASN1_SEQUENCE_ANY, @function
d2i_ASN1_SEQUENCE_ANY:
.LFB432:
	.cfi_startproc
	endbr64
	leaq	ASN1_SEQUENCE_ANY_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE432:
	.size	d2i_ASN1_SEQUENCE_ANY, .-d2i_ASN1_SEQUENCE_ANY
	.p2align 4
	.globl	i2d_ASN1_SEQUENCE_ANY
	.type	i2d_ASN1_SEQUENCE_ANY, @function
i2d_ASN1_SEQUENCE_ANY:
.LFB433:
	.cfi_startproc
	endbr64
	leaq	ASN1_SEQUENCE_ANY_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE433:
	.size	i2d_ASN1_SEQUENCE_ANY, .-i2d_ASN1_SEQUENCE_ANY
	.p2align 4
	.globl	d2i_ASN1_SET_ANY
	.type	d2i_ASN1_SET_ANY, @function
d2i_ASN1_SET_ANY:
.LFB434:
	.cfi_startproc
	endbr64
	leaq	ASN1_SET_ANY_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE434:
	.size	d2i_ASN1_SET_ANY, .-d2i_ASN1_SET_ANY
	.p2align 4
	.globl	i2d_ASN1_SET_ANY
	.type	i2d_ASN1_SET_ANY, @function
i2d_ASN1_SET_ANY:
.LFB435:
	.cfi_startproc
	endbr64
	leaq	ASN1_SET_ANY_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE435:
	.size	i2d_ASN1_SET_ANY, .-i2d_ASN1_SET_ANY
	.globl	ASN1_SET_ANY_it
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"ASN1_SET_ANY"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	ASN1_SET_ANY_it, @object
	.size	ASN1_SET_ANY_it, 56
ASN1_SET_ANY_it:
	.byte	0
	.zero	7
	.quad	-1
	.quad	ASN1_SET_ANY_item_tt
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC0
	.align 32
	.type	ASN1_SET_ANY_item_tt, @object
	.size	ASN1_SET_ANY_item_tt, 40
ASN1_SET_ANY_item_tt:
	.quad	2
	.quad	0
	.quad	0
	.quad	.LC0
	.quad	ASN1_ANY_it
	.globl	ASN1_SEQUENCE_ANY_it
	.section	.rodata.str1.1
.LC1:
	.string	"ASN1_SEQUENCE_ANY"
	.section	.data.rel.ro.local
	.align 32
	.type	ASN1_SEQUENCE_ANY_it, @object
	.size	ASN1_SEQUENCE_ANY_it, 56
ASN1_SEQUENCE_ANY_it:
	.byte	0
	.zero	7
	.quad	-1
	.quad	ASN1_SEQUENCE_ANY_item_tt
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC1
	.align 32
	.type	ASN1_SEQUENCE_ANY_item_tt, @object
	.size	ASN1_SEQUENCE_ANY_item_tt, 40
ASN1_SEQUENCE_ANY_item_tt:
	.quad	4
	.quad	0
	.quad	0
	.quad	.LC1
	.quad	ASN1_ANY_it
	.globl	ASN1_OCTET_STRING_NDEF_it
	.section	.rodata.str1.1
.LC2:
	.string	"ASN1_OCTET_STRING_NDEF"
	.section	.data.rel.ro.local
	.align 32
	.type	ASN1_OCTET_STRING_NDEF_it, @object
	.size	ASN1_OCTET_STRING_NDEF_it, 56
ASN1_OCTET_STRING_NDEF_it:
	.byte	0
	.zero	7
	.quad	4
	.quad	0
	.quad	0
	.quad	0
	.quad	2048
	.quad	.LC2
	.globl	ASN1_FBOOLEAN_it
	.section	.rodata.str1.1
.LC3:
	.string	"ASN1_FBOOLEAN"
	.section	.data.rel.ro.local
	.align 32
	.type	ASN1_FBOOLEAN_it, @object
	.size	ASN1_FBOOLEAN_it, 56
ASN1_FBOOLEAN_it:
	.byte	0
	.zero	7
	.quad	1
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC3
	.globl	ASN1_TBOOLEAN_it
	.section	.rodata.str1.1
.LC4:
	.string	"ASN1_TBOOLEAN"
	.section	.data.rel.ro.local
	.align 32
	.type	ASN1_TBOOLEAN_it, @object
	.size	ASN1_TBOOLEAN_it, 56
ASN1_TBOOLEAN_it:
	.byte	0
	.zero	7
	.quad	1
	.quad	0
	.quad	0
	.quad	0
	.quad	1
	.quad	.LC4
	.globl	ASN1_BOOLEAN_it
	.section	.rodata.str1.1
.LC5:
	.string	"ASN1_BOOLEAN"
	.section	.data.rel.ro.local
	.align 32
	.type	ASN1_BOOLEAN_it, @object
	.size	ASN1_BOOLEAN_it, 56
ASN1_BOOLEAN_it:
	.byte	0
	.zero	7
	.quad	1
	.quad	0
	.quad	0
	.quad	0
	.quad	-1
	.quad	.LC5
	.globl	DIRECTORYSTRING_it
	.section	.rodata.str1.1
.LC6:
	.string	"DIRECTORYSTRING"
	.section	.data.rel.ro.local
	.align 32
	.type	DIRECTORYSTRING_it, @object
	.size	DIRECTORYSTRING_it, 56
DIRECTORYSTRING_it:
	.byte	5
	.zero	7
	.quad	10502
	.quad	0
	.quad	0
	.quad	0
	.quad	24
	.quad	.LC6
	.globl	DISPLAYTEXT_it
	.section	.rodata.str1.1
.LC7:
	.string	"DISPLAYTEXT"
	.section	.data.rel.ro.local
	.align 32
	.type	DISPLAYTEXT_it, @object
	.size	DISPLAYTEXT_it, 56
DISPLAYTEXT_it:
	.byte	5
	.zero	7
	.quad	10320
	.quad	0
	.quad	0
	.quad	0
	.quad	24
	.quad	.LC7
	.globl	ASN1_PRINTABLE_it
	.section	.rodata.str1.1
.LC8:
	.string	"ASN1_PRINTABLE"
	.section	.data.rel.ro.local
	.align 32
	.type	ASN1_PRINTABLE_it, @object
	.size	ASN1_PRINTABLE_it, 56
ASN1_PRINTABLE_it:
	.byte	5
	.zero	7
	.quad	81175
	.quad	0
	.quad	0
	.quad	0
	.quad	24
	.quad	.LC8
	.globl	ASN1_SEQUENCE_it
	.section	.rodata.str1.1
.LC9:
	.string	"ASN1_SEQUENCE"
	.section	.data.rel.ro.local
	.align 32
	.type	ASN1_SEQUENCE_it, @object
	.size	ASN1_SEQUENCE_it, 56
ASN1_SEQUENCE_it:
	.byte	0
	.zero	7
	.quad	16
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC9
	.globl	ASN1_ANY_it
	.section	.rodata.str1.1
.LC10:
	.string	"ASN1_ANY"
	.section	.data.rel.ro.local
	.align 32
	.type	ASN1_ANY_it, @object
	.size	ASN1_ANY_it, 56
ASN1_ANY_it:
	.byte	0
	.zero	7
	.quad	-4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC10
	.globl	ASN1_OBJECT_it
	.section	.rodata.str1.1
.LC11:
	.string	"ASN1_OBJECT"
	.section	.data.rel.ro.local
	.align 32
	.type	ASN1_OBJECT_it, @object
	.size	ASN1_OBJECT_it, 56
ASN1_OBJECT_it:
	.byte	0
	.zero	7
	.quad	6
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC11
	.globl	ASN1_NULL_it
	.section	.rodata.str1.1
.LC12:
	.string	"ASN1_NULL"
	.section	.data.rel.ro.local
	.align 32
	.type	ASN1_NULL_it, @object
	.size	ASN1_NULL_it, 56
ASN1_NULL_it:
	.byte	0
	.zero	7
	.quad	5
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC12
	.globl	ASN1_BMPSTRING_it
	.section	.rodata.str1.1
.LC13:
	.string	"ASN1_BMPSTRING"
	.section	.data.rel.ro.local
	.align 32
	.type	ASN1_BMPSTRING_it, @object
	.size	ASN1_BMPSTRING_it, 56
ASN1_BMPSTRING_it:
	.byte	0
	.zero	7
	.quad	30
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC13
	.globl	ASN1_UNIVERSALSTRING_it
	.section	.rodata.str1.1
.LC14:
	.string	"ASN1_UNIVERSALSTRING"
	.section	.data.rel.ro.local
	.align 32
	.type	ASN1_UNIVERSALSTRING_it, @object
	.size	ASN1_UNIVERSALSTRING_it, 56
ASN1_UNIVERSALSTRING_it:
	.byte	0
	.zero	7
	.quad	28
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC14
	.globl	ASN1_VISIBLESTRING_it
	.section	.rodata.str1.1
.LC15:
	.string	"ASN1_VISIBLESTRING"
	.section	.data.rel.ro.local
	.align 32
	.type	ASN1_VISIBLESTRING_it, @object
	.size	ASN1_VISIBLESTRING_it, 56
ASN1_VISIBLESTRING_it:
	.byte	0
	.zero	7
	.quad	26
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC15
	.globl	ASN1_GENERALIZEDTIME_it
	.section	.rodata.str1.1
.LC16:
	.string	"ASN1_GENERALIZEDTIME"
	.section	.data.rel.ro.local
	.align 32
	.type	ASN1_GENERALIZEDTIME_it, @object
	.size	ASN1_GENERALIZEDTIME_it, 56
ASN1_GENERALIZEDTIME_it:
	.byte	0
	.zero	7
	.quad	24
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC16
	.globl	ASN1_UTCTIME_it
	.section	.rodata.str1.1
.LC17:
	.string	"ASN1_UTCTIME"
	.section	.data.rel.ro.local
	.align 32
	.type	ASN1_UTCTIME_it, @object
	.size	ASN1_UTCTIME_it, 56
ASN1_UTCTIME_it:
	.byte	0
	.zero	7
	.quad	23
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC17
	.globl	ASN1_GENERALSTRING_it
	.section	.rodata.str1.1
.LC18:
	.string	"ASN1_GENERALSTRING"
	.section	.data.rel.ro.local
	.align 32
	.type	ASN1_GENERALSTRING_it, @object
	.size	ASN1_GENERALSTRING_it, 56
ASN1_GENERALSTRING_it:
	.byte	0
	.zero	7
	.quad	27
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC18
	.globl	ASN1_IA5STRING_it
	.section	.rodata.str1.1
.LC19:
	.string	"ASN1_IA5STRING"
	.section	.data.rel.ro.local
	.align 32
	.type	ASN1_IA5STRING_it, @object
	.size	ASN1_IA5STRING_it, 56
ASN1_IA5STRING_it:
	.byte	0
	.zero	7
	.quad	22
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC19
	.globl	ASN1_T61STRING_it
	.section	.rodata.str1.1
.LC20:
	.string	"ASN1_T61STRING"
	.section	.data.rel.ro.local
	.align 32
	.type	ASN1_T61STRING_it, @object
	.size	ASN1_T61STRING_it, 56
ASN1_T61STRING_it:
	.byte	0
	.zero	7
	.quad	20
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC20
	.globl	ASN1_PRINTABLESTRING_it
	.section	.rodata.str1.1
.LC21:
	.string	"ASN1_PRINTABLESTRING"
	.section	.data.rel.ro.local
	.align 32
	.type	ASN1_PRINTABLESTRING_it, @object
	.size	ASN1_PRINTABLESTRING_it, 56
ASN1_PRINTABLESTRING_it:
	.byte	0
	.zero	7
	.quad	19
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC21
	.globl	ASN1_UTF8STRING_it
	.section	.rodata.str1.1
.LC22:
	.string	"ASN1_UTF8STRING"
	.section	.data.rel.ro.local
	.align 32
	.type	ASN1_UTF8STRING_it, @object
	.size	ASN1_UTF8STRING_it, 56
ASN1_UTF8STRING_it:
	.byte	0
	.zero	7
	.quad	12
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC22
	.globl	ASN1_BIT_STRING_it
	.section	.rodata.str1.1
.LC23:
	.string	"ASN1_BIT_STRING"
	.section	.data.rel.ro.local
	.align 32
	.type	ASN1_BIT_STRING_it, @object
	.size	ASN1_BIT_STRING_it, 56
ASN1_BIT_STRING_it:
	.byte	0
	.zero	7
	.quad	3
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC23
	.globl	ASN1_ENUMERATED_it
	.section	.rodata.str1.1
.LC24:
	.string	"ASN1_ENUMERATED"
	.section	.data.rel.ro.local
	.align 32
	.type	ASN1_ENUMERATED_it, @object
	.size	ASN1_ENUMERATED_it, 56
ASN1_ENUMERATED_it:
	.byte	0
	.zero	7
	.quad	10
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC24
	.globl	ASN1_INTEGER_it
	.section	.rodata.str1.1
.LC25:
	.string	"ASN1_INTEGER"
	.section	.data.rel.ro.local
	.align 32
	.type	ASN1_INTEGER_it, @object
	.size	ASN1_INTEGER_it, 56
ASN1_INTEGER_it:
	.byte	0
	.zero	7
	.quad	2
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC25
	.globl	ASN1_OCTET_STRING_it
	.section	.rodata.str1.1
.LC26:
	.string	"ASN1_OCTET_STRING"
	.section	.data.rel.ro.local
	.align 32
	.type	ASN1_OCTET_STRING_it, @object
	.size	ASN1_OCTET_STRING_it, 56
ASN1_OCTET_STRING_it:
	.byte	0
	.zero	7
	.quad	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC26
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
