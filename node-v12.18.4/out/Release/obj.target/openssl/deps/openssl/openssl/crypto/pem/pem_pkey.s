	.file	"pem_pkey.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"ANY PRIVATE KEY"
.LC1:
	.string	"PRIVATE KEY"
.LC2:
	.string	"ENCRYPTED PRIVATE KEY"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"../deps/openssl/openssl/crypto/pem/pem_pkey.c"
	.text
	.p2align 4
	.globl	PEM_read_bio_PrivateKey
	.type	PEM_read_bio_PrivateKey, @function
PEM_read_bio_PrivateKey:
.LFB877:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-1104(%rbp), %rdi
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	leaq	-1096(%rbp), %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	leaq	-1120(%rbp), %rdx
	movq	%rbx, %r9
	subq	$1104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	pushq	%rcx
	leaq	.LC0(%rip), %rcx
	movq	$0, -1120(%rbp)
	movq	$0, -1112(%rbp)
	movq	$0, -1104(%rbp)
	call	PEM_bytes_read_bio_secmem@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	je	.L17
	movq	-1104(%rbp), %rax
	movq	-1120(%rbp), %r8
	movl	$12, %ecx
	leaq	.LC1(%rip), %r9
	movq	%r9, %rdi
	movq	%rax, -1112(%rbp)
	movq	%r8, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L38
	movl	$22, %ecx
	leaq	.LC2(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L8
	movq	-1096(%rbp), %rdx
	leaq	-1112(%rbp), %rsi
	xorl	%edi, %edi
	call	d2i_X509_SIG@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L5
	leaq	-1088(%rbp), %r15
	movq	%r12, %rcx
	xorl	%edx, %edx
	movl	$1024, %esi
	movq	%r15, %rdi
	testq	%rbx, %rbx
	je	.L10
	call	*%rbx
	movl	%eax, %ebx
.L11:
	testl	%ebx, %ebx
	js	.L39
	movl	%ebx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	PKCS8_decrypt@PLT
	movq	%r14, %rdi
	movq	%rax, -1128(%rbp)
	call	X509_SIG_free@PLT
	movslq	%ebx, %rsi
	movq	%r15, %rdi
	call	OPENSSL_cleanse@PLT
	movq	-1128(%rbp), %r8
	testq	%r8, %r8
	je	.L5
	movq	%r8, %rdi
	call	EVP_PKCS82PKEY@PLT
	testq	%r13, %r13
	movq	-1128(%rbp), %r8
	movq	%rax, %r12
	je	.L14
	movq	0(%r13), %rdi
	call	EVP_PKEY_free@PLT
	movq	%r12, 0(%r13)
	movq	-1128(%rbp), %r8
.L14:
	movq	%r8, %rdi
	call	PKCS8_PRIV_KEY_INFO_free@PLT
	testq	%r12, %r12
	jne	.L13
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$88, %r8d
	leaq	.LC3(%rip), %rcx
	movl	$13, %edx
	xorl	%r12d, %r12d
	movl	$123, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
.L13:
	movq	-1120(%rbp), %rdi
	movl	$90, %edx
	leaq	.LC3(%rip), %rsi
	call	CRYPTO_secure_free@PLT
	movq	-1096(%rbp), %rsi
	movq	-1104(%rbp), %rdi
	movl	$91, %ecx
	leaq	.LC3(%rip), %rdx
	call	CRYPTO_secure_clear_free@PLT
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L40
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%r8, %rdi
	call	pem_check_suffix@PLT
	movl	%eax, %edx
	testl	%eax, %eax
	jle	.L5
	movq	-1120(%rbp), %rsi
	xorl	%edi, %edi
	call	EVP_PKEY_asn1_find_str@PLT
	testq	%rax, %rax
	je	.L5
	cmpq	$0, 184(%rax)
	je	.L5
	movq	-1096(%rbp), %rcx
	movl	(%rax), %edi
	leaq	-1112(%rbp), %rdx
	movq	%r13, %rsi
	call	d2i_PrivateKey@PLT
	movq	%rax, %r12
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L38:
	movq	-1096(%rbp), %rdx
	leaq	-1112(%rbp), %rsi
	xorl	%edi, %edi
	call	d2i_PKCS8_PRIV_KEY_INFO@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L5
	movq	%rax, %rdi
	call	EVP_PKCS82PKEY@PLT
	movq	%rax, %r12
	testq	%r13, %r13
	je	.L6
	movq	0(%r13), %rdi
	call	EVP_PKEY_free@PLT
	movq	%r12, 0(%r13)
.L6:
	movq	%r14, %rdi
	call	PKCS8_PRIV_KEY_INFO_free@PLT
.L7:
	testq	%r12, %r12
	jne	.L13
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L17:
	xorl	%r12d, %r12d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L39:
	movl	$64, %r8d
	leaq	.LC3(%rip), %rcx
	movl	$104, %edx
	xorl	%r12d, %r12d
	movl	$123, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
	movq	%r14, %rdi
	call	X509_SIG_free@PLT
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L10:
	call	PEM_def_callback@PLT
	movl	%eax, %ebx
	jmp	.L11
.L40:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE877:
	.size	PEM_read_bio_PrivateKey, .-PEM_read_bio_PrivateKey
	.section	.rodata.str1.1
.LC4:
	.string	"%s PRIVATE KEY"
	.text
	.p2align 4
	.globl	PEM_write_bio_PrivateKey
	.type	PEM_write_bio_PrivateKey, @function
PEM_write_bio_PrivateKey:
.LFB878:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rsi), %rax
	testq	%rax, %rax
	je	.L42
	cmpq	$0, 72(%rax)
	je	.L43
.L42:
	subq	$8, %rsp
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r12, %rsi
	pushq	%rbx
	movq	%r13, %rdi
	call	PEM_write_bio_PKCS8PrivateKey@PLT
	popq	%rdx
	popq	%rcx
.L41:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L50
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	leaq	-144(%rbp), %r10
	movq	16(%rax), %rcx
	xorl	%eax, %eax
	leaq	.LC4(%rip), %rdx
	movq	%r10, %rdi
	movl	$80, %esi
	movq	%r9, -168(%rbp)
	movl	%r8d, -156(%rbp)
	movq	%r10, -152(%rbp)
	call	BIO_snprintf@PLT
	movq	-168(%rbp), %r9
	subq	$8, %rsp
	movl	-156(%rbp), %r8d
	pushq	%rbx
	movq	-152(%rbp), %r10
	movq	%r12, %rcx
	movq	%r13, %rdx
	pushq	%r9
	movq	i2d_PrivateKey@GOTPCREL(%rip), %rdi
	movq	%r15, %r9
	pushq	%r8
	movq	%r10, %rsi
	movq	%r14, %r8
	call	PEM_ASN1_write_bio@PLT
	addq	$32, %rsp
	jmp	.L41
.L50:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE878:
	.size	PEM_write_bio_PrivateKey, .-PEM_write_bio_PrivateKey
	.p2align 4
	.globl	PEM_write_bio_PrivateKey_traditional
	.type	PEM_write_bio_PrivateKey_traditional, @function
PEM_write_bio_PrivateKey_traditional:
.LFB879:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	leaq	-144(%rbp), %r10
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r10, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	leaq	.LC4(%rip), %rdx
	subq	$136, %rsp
	movq	16(%rbp), %r8
	movq	%r9, -168(%rbp)
	movq	%r8, -160(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rsi), %rax
	movl	$80, %esi
	movq	%r10, -152(%rbp)
	movq	16(%rax), %rcx
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	movq	-160(%rbp), %r8
	subq	$8, %rsp
	movq	-168(%rbp), %r9
	movq	-152(%rbp), %r10
	movq	i2d_PrivateKey@GOTPCREL(%rip), %rdi
	movq	%r12, %rcx
	movq	%r13, %rdx
	pushq	%r8
	movq	%rbx, %r8
	pushq	%r9
	movq	%r10, %rsi
	movq	%r14, %r9
	pushq	%r15
	call	PEM_ASN1_write_bio@PLT
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L54
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L54:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE879:
	.size	PEM_write_bio_PrivateKey_traditional, .-PEM_write_bio_PrivateKey_traditional
	.section	.rodata.str1.1
.LC5:
	.string	"PARAMETERS"
	.text
	.p2align 4
	.globl	PEM_read_bio_Parameters
	.type	PEM_read_bio_Parameters, @function
PEM_read_bio_Parameters:
.LFB880:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	leaq	.LC5(%rip), %rcx
	xorl	%r9d, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-72(%rbp), %rdx
	leaq	-56(%rbp), %rdi
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	leaq	-48(%rbp), %rsi
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	pushq	$0
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	call	PEM_bytes_read_bio@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	je	.L55
	movq	-56(%rbp), %rax
	movq	-72(%rbp), %rdi
	leaq	.LC5(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	pem_check_suffix@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jle	.L59
	call	EVP_PKEY_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L59
	movq	-72(%rbp), %rsi
	movl	%r13d, %edx
	movq	%rax, %rdi
	call	EVP_PKEY_set_type_str@PLT
	testl	%eax, %eax
	je	.L60
	movq	16(%r12), %rax
	movq	112(%rax), %rax
	testq	%rax, %rax
	je	.L60
	movl	-48(%rbp), %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L60
	testq	%rbx, %rbx
	je	.L61
	movq	(%rbx), %rdi
	call	EVP_PKEY_free@PLT
	movq	%r12, (%rbx)
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L60:
	movq	%r12, %rdi
	call	EVP_PKEY_free@PLT
.L59:
	movl	$148, %r8d
	leaq	.LC3(%rip), %rcx
	movl	$13, %edx
	xorl	%r12d, %r12d
	movl	$140, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
.L61:
	movq	-72(%rbp), %rdi
	movl	$149, %edx
	leaq	.LC3(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-56(%rbp), %rdi
	movl	$150, %edx
	leaq	.LC3(%rip), %rsi
	call	CRYPTO_free@PLT
.L55:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L79
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L79:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE880:
	.size	PEM_read_bio_Parameters, .-PEM_read_bio_Parameters
	.section	.rodata.str1.1
.LC6:
	.string	"%s PARAMETERS"
	.text
	.p2align 4
	.globl	PEM_write_bio_Parameters
	.type	PEM_write_bio_Parameters, @function
PEM_write_bio_Parameters:
.LFB881:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$104, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	16(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L83
	cmpq	$0, 120(%rdx)
	je	.L80
	movq	16(%rdx), %rcx
	leaq	-128(%rbp), %r14
	movq	%rsi, %r12
	movq	%rdi, %r13
	leaq	.LC6(%rip), %rdx
	movl	$80, %esi
	movq	%r14, %rdi
	call	BIO_snprintf@PLT
	movq	16(%r12), %rax
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	subq	$8, %rsp
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	120(%rax), %rdi
	pushq	$0
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write_bio@PLT
	addq	$32, %rsp
.L80:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L86
	leaq	-24(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L80
.L86:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE881:
	.size	PEM_write_bio_Parameters, .-PEM_write_bio_Parameters
	.p2align 4
	.globl	PEM_read_PrivateKey
	.type	PEM_read_PrivateKey, @function
PEM_read_PrivateKey:
.LFB882:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	testq	%rax, %rax
	je	.L91
	movq	%rax, %r12
	movq	%rbx, %rcx
	xorl	%edx, %edx
	movl	$106, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r15, %rcx
	movq	%r14, %rdx
	call	PEM_read_bio_PrivateKey
	movq	%r12, %rdi
	movq	%rax, %r13
	call	BIO_free@PLT
.L87:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	movl	$173, %r8d
	leaq	.LC3(%rip), %rcx
	movl	$7, %edx
	xorl	%r13d, %r13d
	movl	$124, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
	jmp	.L87
	.cfi_endproc
.LFE882:
	.size	PEM_read_PrivateKey, .-PEM_read_PrivateKey
	.p2align 4
	.globl	PEM_write_PrivateKey
	.type	PEM_write_PrivateKey, @function
PEM_write_PrivateKey:
.LFB883:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r8d, -156(%rbp)
	movq	16(%rbp), %rbx
	movq	%r9, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	BIO_new_fp@PLT
	movq	-152(%rbp), %r9
	movl	-156(%rbp), %r8d
	testq	%rax, %rax
	je	.L103
	movq	%rax, %r12
	movq	16(%r13), %rax
	testq	%rax, %rax
	je	.L95
	cmpq	$0, 72(%rax)
	je	.L96
.L95:
	subq	$8, %rsp
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r15, %rcx
	pushq	%rbx
	movq	%r12, %rdi
	call	PEM_write_bio_PKCS8PrivateKey@PLT
	movl	%eax, %r13d
	popq	%rax
	popq	%rdx
.L97:
	movq	%r12, %rdi
	call	BIO_free@PLT
.L92:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L104
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	leaq	-144(%rbp), %r10
	movq	16(%rax), %rcx
	xorl	%eax, %eax
	leaq	.LC4(%rip), %rdx
	movq	%r10, %rdi
	movl	$80, %esi
	movq	%r9, -168(%rbp)
	movl	%r8d, -156(%rbp)
	movq	%r10, -152(%rbp)
	call	BIO_snprintf@PLT
	movq	-168(%rbp), %r9
	subq	$8, %rsp
	movl	-156(%rbp), %r8d
	pushq	%rbx
	movq	-152(%rbp), %r10
	movq	%r13, %rcx
	movq	%r12, %rdx
	pushq	%r9
	movq	i2d_PrivateKey@GOTPCREL(%rip), %rdi
	movq	%r15, %r9
	pushq	%r8
	movq	%r10, %rsi
	movq	%r14, %r8
	call	PEM_ASN1_write_bio@PLT
	addq	$32, %rsp
	movl	%eax, %r13d
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L103:
	movl	$190, %r8d
	leaq	.LC3(%rip), %rcx
	movl	$7, %edx
	xorl	%r13d, %r13d
	movl	$139, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
	jmp	.L92
.L104:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE883:
	.size	PEM_write_PrivateKey, .-PEM_write_PrivateKey
	.section	.rodata.str1.1
.LC7:
	.string	"DH PARAMETERS"
.LC8:
	.string	"X9.42 DH PARAMETERS"
	.text
	.p2align 4
	.globl	PEM_read_bio_DHparams
	.type	PEM_read_bio_DHparams, @function
PEM_read_bio_DHparams:
.LFB884:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	leaq	-56(%rbp), %rdx
	leaq	-32(%rbp), %rsi
	leaq	-40(%rbp), %rdi
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	pushq	%rcx
	leaq	.LC7(%rip), %rcx
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	movq	$0, -40(%rbp)
	call	PEM_bytes_read_bio@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	je	.L111
	movq	-40(%rbp), %rax
	movq	-56(%rbp), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$20, %ecx
	movq	-32(%rbp), %rdx
	movq	%rax, -48(%rbp)
	repz cmpsb
	leaq	-48(%rbp), %rsi
	movq	%r12, %rdi
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L113
	call	d2i_DHparams@PLT
	movq	%rax, %r12
	testq	%r12, %r12
	je	.L114
.L109:
	movq	-56(%rbp), %rdi
	movl	$223, %edx
	leaq	.LC3(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-40(%rbp), %rdi
	movl	$224, %edx
	leaq	.LC3(%rip), %rsi
	call	CRYPTO_free@PLT
.L105:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L115
	movq	%r12, %rax
	movq	-8(%rbp), %r12
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore_state
	call	d2i_DHxparams@PLT
	movq	%rax, %r12
	testq	%r12, %r12
	jne	.L109
.L114:
	movl	$222, %r8d
	movl	$13, %edx
	movl	$141, %esi
	movl	$9, %edi
	leaq	.LC3(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L111:
	xorl	%r12d, %r12d
	jmp	.L105
.L115:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE884:
	.size	PEM_read_bio_DHparams, .-PEM_read_bio_DHparams
	.p2align 4
	.globl	PEM_read_DHparams
	.type	PEM_read_DHparams, @function
PEM_read_DHparams:
.LFB885:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	testq	%rax, %rax
	je	.L120
	movq	%rax, %r12
	movq	%rbx, %rcx
	xorl	%edx, %edx
	movl	$106, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r15, %rcx
	movq	%r14, %rdx
	call	PEM_read_bio_DHparams
	movq	%r12, %rdi
	movq	%rax, %r13
	call	BIO_free@PLT
.L116:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore_state
	movl	$235, %r8d
	leaq	.LC3(%rip), %rcx
	movl	$7, %edx
	xorl	%r13d, %r13d
	movl	$142, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
	jmp	.L116
	.cfi_endproc
.LFE885:
	.size	PEM_read_DHparams, .-PEM_read_DHparams
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
