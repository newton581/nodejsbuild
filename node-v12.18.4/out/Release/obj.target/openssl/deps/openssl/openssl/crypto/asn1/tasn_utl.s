	.file	"tasn_utl.c"
	.text
	.p2align 4
	.globl	asn1_get_choice_selector
	.type	asn1_get_choice_selector, @function
asn1_get_choice_selector:
.LFB493:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movq	8(%rsi), %rax
	movl	(%rdx,%rax), %eax
	ret
	.cfi_endproc
.LFE493:
	.size	asn1_get_choice_selector, .-asn1_get_choice_selector
	.p2align 4
	.globl	asn1_set_choice_selector
	.type	asn1_set_choice_selector, @function
asn1_set_choice_selector:
.LFB494:
	.cfi_startproc
	endbr64
	movq	8(%rdx), %rax
	addq	(%rdi), %rax
	movl	(%rax), %r8d
	movl	%esi, (%rax)
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE494:
	.size	asn1_set_choice_selector, .-asn1_set_choice_selector
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/asn1/tasn_utl.c"
	.text
	.p2align 4
	.globl	asn1_do_lock
	.type	asn1_do_lock, @function
asn1_do_lock:
.LFB495:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movzbl	(%rdx), %eax
	cmpb	$1, %al
	je	.L12
	xorl	%r12d, %r12d
	cmpb	$6, %al
	je	.L12
.L4:
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	movq	32(%rdx), %rax
	testq	%rax, %rax
	je	.L11
	movl	8(%rax), %r12d
	andl	$1, %r12d
	je	.L4
	movq	(%rdi), %rcx
	movslq	12(%rax), %rdx
	movslq	16(%rax), %rbx
	addq	%rcx, %rdx
	addq	%rcx, %rbx
	testl	%esi, %esi
	je	.L7
	cmpl	$1, %esi
	je	.L8
	movl	$-1, %r12d
	cmpl	$-1, %esi
	jne	.L4
	lock xaddl	%r12d, (%rdx)
	subl	$1, %r12d
	jne	.L4
	movq	(%rbx), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	movq	$0, (%rbx)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L11:
	xorl	%r12d, %r12d
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movl	$1, %r12d
	lock xaddl	%r12d, (%rdx)
	addl	$1, %r12d
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L7:
	movl	$1, (%rdx)
	mfence
	call	CRYPTO_THREAD_lock_new@PLT
	movq	%rax, (%rbx)
	testq	%rax, %rax
	jne	.L4
	movl	$79, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$233, %esi
	movl	$13, %edi
	orl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L4
	.cfi_endproc
.LFE495:
	.size	asn1_do_lock, .-asn1_do_lock
	.p2align 4
	.globl	asn1_enc_init
	.type	asn1_enc_init, @function
asn1_enc_init:
.LFB497:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L20
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L20
	movq	32(%rsi), %rdx
	testq	%rdx, %rdx
	je	.L20
	testb	$2, 8(%rdx)
	je	.L20
	movslq	32(%rdx), %rdx
	addq	%rdx, %rax
	movq	$0, (%rax)
	movq	$0, 8(%rax)
	movl	$1, 16(%rax)
.L20:
	ret
	.cfi_endproc
.LFE497:
	.size	asn1_enc_init, .-asn1_enc_init
	.p2align 4
	.globl	asn1_enc_free
	.type	asn1_enc_free, @function
asn1_enc_free:
.LFB498:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L49
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L34
	movq	32(%rsi), %rax
	testq	%rax, %rax
	je	.L34
	testb	$2, 8(%rax)
	je	.L34
	movslq	32(%rax), %rax
	movl	$131, %edx
	leaq	.LC0(%rip), %rsi
	addq	%rax, %rbx
	movq	(%rbx), %rdi
	call	CRYPTO_free@PLT
	movq	$0, (%rbx)
	movq	$0, 8(%rbx)
	movl	$1, 16(%rbx)
.L34:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE498:
	.size	asn1_enc_free, .-asn1_enc_free
	.p2align 4
	.globl	asn1_enc_save
	.type	asn1_enc_save, @function
asn1_enc_save:
.LFB499:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L55
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %r12
	testq	%r12, %r12
	je	.L58
	movq	32(%rcx), %rax
	testq	%rax, %rax
	je	.L58
	testb	$2, 8(%rax)
	je	.L58
	movslq	32(%rax), %rax
	movslq	%edx, %rbx
	movq	%rsi, %r13
	movl	$146, %edx
	leaq	.LC0(%rip), %rsi
	addq	%rax, %r12
	movq	(%r12), %rdi
	call	CRYPTO_free@PLT
	movq	%rbx, %rdi
	movl	$147, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L63
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	%rbx, 8(%r12)
	movl	$1, %eax
	movl	$0, 16(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	movl	$148, %r8d
	movl	$65, %edx
	movl	$115, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE499:
	.size	asn1_enc_save, .-asn1_enc_save
	.p2align 4
	.globl	asn1_enc_restore
	.type	asn1_enc_restore, @function
asn1_enc_restore:
.LFB500:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L69
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L69
	movq	32(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L69
	movl	8(%rcx), %eax
	andl	$2, %eax
	je	.L80
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movslq	32(%rcx), %rbx
	addq	%rdx, %rbx
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jne	.L64
	movq	%rdi, %r13
	movq	%rsi, %r12
	testq	%rsi, %rsi
	je	.L66
	movq	(%rsi), %rdi
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	call	memcpy@PLT
	movq	8(%rbx), %rax
	addq	%rax, (%r12)
.L66:
	movl	$1, %eax
	testq	%r13, %r13
	je	.L64
	movq	8(%rbx), %rdx
	movl	%edx, 0(%r13)
.L64:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	ret
	.cfi_endproc
.LFE500:
	.size	asn1_enc_restore, .-asn1_enc_restore
	.p2align 4
	.globl	asn1_get_field_ptr
	.type	asn1_get_field_ptr, @function
asn1_get_field_ptr:
.LFB501:
	.cfi_startproc
	endbr64
	movq	16(%rsi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE501:
	.size	asn1_get_field_ptr, .-asn1_get_field_ptr
	.p2align 4
	.globl	asn1_do_adb
	.type	asn1_do_adb, @function
asn1_do_adb:
.LFB502:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	%edx, %ebx
	subq	$16, %rsp
	movq	(%rsi), %rdx
	movq	%fs:40, %rcx
	movq	%rcx, -24(%rbp)
	xorl	%ecx, %ecx
	testb	$3, %dh
	je	.L84
	movq	32(%rsi), %r12
	movq	(%rdi), %rcx
	movq	8(%r12), %rax
	movq	(%rcx,%rax), %rdi
	testq	%rdi, %rdi
	je	.L108
	andb	$1, %dh
	je	.L88
	call	OBJ_obj2nid@PLT
	cltq
	movq	%rax, -32(%rbp)
.L89:
	movq	16(%r12), %rax
	testq	%rax, %rax
	je	.L90
	leaq	-32(%rbp), %rdi
	call	*%rax
	movl	$226, %r8d
	testl	%eax, %eax
	je	.L107
.L90:
	movq	32(%r12), %rcx
	movq	24(%r12), %rax
	testq	%rcx, %rcx
	jle	.L91
	movq	-32(%rbp), %rsi
	xorl	%edx, %edx
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L92:
	addq	$1, %rdx
	addq	$48, %rax
	cmpq	%rdx, %rcx
	je	.L91
.L93:
	cmpq	%rsi, (%rax)
	jne	.L92
	addq	$8, %rax
.L84:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L109
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	call	ASN1_INTEGER_get@PLT
	movq	%rax, -32(%rbp)
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L91:
	movq	40(%r12), %rax
	testq	%rax, %rax
	jne	.L84
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L108:
	movq	48(%r12), %rax
	testq	%rax, %rax
	jne	.L84
.L94:
	testl	%ebx, %ebx
	jne	.L87
	xorl	%eax, %eax
	jmp	.L84
.L87:
	movl	$251, %r8d
.L107:
	movl	$164, %edx
	movl	$110, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L84
.L109:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE502:
	.size	asn1_do_adb, .-asn1_do_adb
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
