	.file	"a_gentm.c"
	.text
	.p2align 4
	.globl	asn1_generalizedtime_to_tm
	.type	asn1_generalizedtime_to_tm, @function
asn1_generalizedtime_to_tm:
.LFB467:
	.cfi_startproc
	endbr64
	cmpl	$24, 4(%rsi)
	je	.L4
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	jmp	asn1_time_to_tm@PLT
	.cfi_endproc
.LFE467:
	.size	asn1_generalizedtime_to_tm, .-asn1_generalizedtime_to_tm
	.p2align 4
	.globl	ASN1_GENERALIZEDTIME_check
	.type	ASN1_GENERALIZEDTIME_check, @function
ASN1_GENERALIZEDTIME_check:
.LFB468:
	.cfi_startproc
	endbr64
	cmpl	$24, 4(%rdi)
	je	.L7
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%rdi, %rsi
	xorl	%edi, %edi
	jmp	asn1_time_to_tm@PLT
	.cfi_endproc
.LFE468:
	.size	ASN1_GENERALIZEDTIME_check, .-ASN1_GENERALIZEDTIME_check
	.p2align 4
	.globl	ASN1_GENERALIZEDTIME_set_string
	.type	ASN1_GENERALIZEDTIME_set_string, @function
ASN1_GENERALIZEDTIME_set_string:
.LFB469:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$24, -60(%rbp)
	call	strlen@PLT
	xorl	%edi, %edi
	movq	%r13, %rsi
	movq	%rbx, -56(%rbp)
	movl	%eax, -64(%rbp)
	movq	$0, -48(%rbp)
	call	asn1_time_to_tm@PLT
	testl	%eax, %eax
	je	.L8
	movl	$1, %eax
	testq	%r12, %r12
	je	.L8
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	ASN1_STRING_copy@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
.L8:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L17
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L17:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE469:
	.size	ASN1_GENERALIZEDTIME_set_string, .-ASN1_GENERALIZEDTIME_set_string
	.p2align 4
	.globl	ASN1_GENERALIZEDTIME_set
	.type	ASN1_GENERALIZEDTIME_set, @function
ASN1_GENERALIZEDTIME_set:
.LFB470:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-88(%rbp), %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -88(%rbp)
	leaq	-80(%rbp), %rsi
	call	OPENSSL_gmtime@PLT
	testq	%rax, %rax
	je	.L18
	movl	$24, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	asn1_time_from_tm@PLT
.L18:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L25
	addq	$88, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L25:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE470:
	.size	ASN1_GENERALIZEDTIME_set, .-ASN1_GENERALIZEDTIME_set
	.p2align 4
	.globl	ASN1_GENERALIZEDTIME_adj
	.type	ASN1_GENERALIZEDTIME_adj, @function
ASN1_GENERALIZEDTIME_adj:
.LFB471:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-104(%rbp), %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	subq	$80, %rsp
	.cfi_offset 12, -48
	movq	%rsi, -104(%rbp)
	leaq	-96(%rbp), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	OPENSSL_gmtime@PLT
	testq	%rax, %rax
	je	.L30
	movq	%rax, %r12
	testl	%r13d, %r13d
	jne	.L29
	testq	%r15, %r15
	jne	.L29
.L31:
	movl	$24, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	asn1_time_from_tm@PLT
.L26:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L37
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movq	%r15, %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	OPENSSL_gmtime_adj@PLT
	testl	%eax, %eax
	jne	.L31
.L30:
	xorl	%eax, %eax
	jmp	.L26
.L37:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE471:
	.size	ASN1_GENERALIZEDTIME_adj, .-ASN1_GENERALIZEDTIME_adj
	.p2align 4
	.globl	ASN1_GENERALIZEDTIME_print
	.type	ASN1_GENERALIZEDTIME_print, @function
ASN1_GENERALIZEDTIME_print:
.LFB472:
	.cfi_startproc
	endbr64
	cmpl	$24, 4(%rsi)
	je	.L40
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	jmp	ASN1_TIME_print@PLT
	.cfi_endproc
.LFE472:
	.size	ASN1_GENERALIZEDTIME_print, .-ASN1_GENERALIZEDTIME_print
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
