	.file	"pkcs7err.c"
	.text
	.p2align 4
	.globl	ERR_load_PKCS7_strings
	.type	ERR_load_PKCS7_strings, @function
ERR_load_PKCS7_strings:
.LFB203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$554205184, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_func_error_string@PLT
	testq	%rax, %rax
	je	.L5
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	leaq	PKCS7_str_functs(%rip), %rdi
	call	ERR_load_strings_const@PLT
	leaq	PKCS7_str_reasons(%rip), %rdi
	call	ERR_load_strings_const@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE203:
	.size	ERR_load_PKCS7_strings, .-ERR_load_PKCS7_strings
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"certificate verify error"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"cipher has no object identifier"
	.section	.rodata.str1.1
.LC2:
	.string	"cipher not initialized"
.LC3:
	.string	"content and data present"
.LC4:
	.string	"ctrl error"
.LC5:
	.string	"decrypt error"
.LC6:
	.string	"digest failure"
.LC7:
	.string	"encryption ctrl failure"
	.section	.rodata.str1.8
	.align 8
.LC8:
	.string	"encryption not supported for this key type"
	.section	.rodata.str1.1
.LC9:
	.string	"error adding recipient"
.LC10:
	.string	"error setting cipher"
.LC11:
	.string	"invalid null pointer"
.LC12:
	.string	"invalid signed data type"
.LC13:
	.string	"no content"
.LC14:
	.string	"no default digest"
.LC15:
	.string	"no matching digest type found"
	.section	.rodata.str1.8
	.align 8
.LC16:
	.string	"no recipient matches certificate"
	.section	.rodata.str1.1
.LC17:
	.string	"no signatures on data"
.LC18:
	.string	"no signers"
	.section	.rodata.str1.8
	.align 8
.LC19:
	.string	"operation not supported on this type"
	.section	.rodata.str1.1
.LC20:
	.string	"pkcs7 add signature error"
.LC21:
	.string	"pkcs7 add signer error"
.LC22:
	.string	"pkcs7 datasign"
	.section	.rodata.str1.8
	.align 8
.LC23:
	.string	"private key does not match certificate"
	.section	.rodata.str1.1
.LC24:
	.string	"signature failure"
.LC25:
	.string	"signer certificate not found"
.LC26:
	.string	"signing ctrl failure"
	.section	.rodata.str1.8
	.align 8
.LC27:
	.string	"signing not supported for this key type"
	.section	.rodata.str1.1
.LC28:
	.string	"smime text error"
.LC29:
	.string	"unable to find certificate"
.LC30:
	.string	"unable to find mem bio"
.LC31:
	.string	"unable to find message digest"
.LC32:
	.string	"unknown digest type"
.LC33:
	.string	"unknown operation"
.LC34:
	.string	"unsupported cipher type"
.LC35:
	.string	"unsupported content type"
.LC36:
	.string	"wrong content type"
.LC37:
	.string	"wrong pkcs7 type"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	PKCS7_str_reasons, @object
	.size	PKCS7_str_reasons, 624
PKCS7_str_reasons:
	.quad	553648245
	.quad	.LC0
	.quad	553648272
	.quad	.LC1
	.quad	553648244
	.quad	.LC2
	.quad	553648246
	.quad	.LC3
	.quad	553648280
	.quad	.LC4
	.quad	553648247
	.quad	.LC5
	.quad	553648229
	.quad	.LC6
	.quad	553648277
	.quad	.LC7
	.quad	553648278
	.quad	.LC8
	.quad	553648248
	.quad	.LC9
	.quad	553648249
	.quad	.LC10
	.quad	553648271
	.quad	.LC11
	.quad	553648283
	.quad	.LC12
	.quad	553648250
	.quad	.LC13
	.quad	553648279
	.quad	.LC14
	.quad	553648282
	.quad	.LC15
	.quad	553648243
	.quad	.LC16
	.quad	553648251
	.quad	.LC17
	.quad	553648270
	.quad	.LC18
	.quad	553648232
	.quad	.LC19
	.quad	553648252
	.quad	.LC20
	.quad	553648281
	.quad	.LC21
	.quad	553648273
	.quad	.LC22
	.quad	553648255
	.quad	.LC23
	.quad	553648233
	.quad	.LC24
	.quad	553648256
	.quad	.LC25
	.quad	553648275
	.quad	.LC26
	.quad	553648276
	.quad	.LC27
	.quad	553648257
	.quad	.LC28
	.quad	553648234
	.quad	.LC29
	.quad	553648235
	.quad	.LC30
	.quad	553648236
	.quad	.LC31
	.quad	553648237
	.quad	.LC32
	.quad	553648238
	.quad	.LC33
	.quad	553648239
	.quad	.LC34
	.quad	553648240
	.quad	.LC35
	.quad	553648241
	.quad	.LC36
	.quad	553648242
	.quad	.LC37
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC38:
	.string	"do_pkcs7_signed_attrib"
	.section	.rodata.str1.8
	.align 8
.LC39:
	.string	"PKCS7_add0_attrib_signing_time"
	.section	.rodata.str1.1
.LC40:
	.string	"PKCS7_add_attrib_smimecap"
.LC41:
	.string	"PKCS7_add_certificate"
.LC42:
	.string	"PKCS7_add_crl"
.LC43:
	.string	"PKCS7_add_recipient_info"
.LC44:
	.string	"PKCS7_add_signature"
.LC45:
	.string	"PKCS7_add_signer"
.LC46:
	.string	"PKCS7_bio_add_digest"
.LC47:
	.string	"pkcs7_copy_existing_digest"
.LC48:
	.string	"PKCS7_ctrl"
.LC49:
	.string	"PKCS7_dataDecode"
.LC50:
	.string	"PKCS7_dataFinal"
.LC51:
	.string	"PKCS7_dataInit"
.LC52:
	.string	"PKCS7_dataVerify"
.LC53:
	.string	"PKCS7_decrypt"
.LC54:
	.string	"pkcs7_decrypt_rinfo"
.LC55:
	.string	"pkcs7_encode_rinfo"
.LC56:
	.string	"PKCS7_encrypt"
.LC57:
	.string	"PKCS7_final"
.LC58:
	.string	"PKCS7_find_digest"
.LC59:
	.string	"PKCS7_get0_signers"
.LC60:
	.string	"PKCS7_RECIP_INFO_set"
.LC61:
	.string	"PKCS7_set_cipher"
.LC62:
	.string	"PKCS7_set_content"
.LC63:
	.string	"PKCS7_set_digest"
.LC64:
	.string	"PKCS7_set_type"
.LC65:
	.string	"PKCS7_sign"
.LC66:
	.string	"PKCS7_signatureVerify"
.LC67:
	.string	"PKCS7_SIGNER_INFO_set"
.LC68:
	.string	"PKCS7_SIGNER_INFO_sign"
.LC69:
	.string	"PKCS7_sign_add_signer"
.LC70:
	.string	"PKCS7_simple_smimecap"
.LC71:
	.string	"PKCS7_verify"
	.section	.data.rel.ro.local
	.align 32
	.type	PKCS7_str_functs, @object
	.size	PKCS7_str_functs, 560
PKCS7_str_functs:
	.quad	554205184
	.quad	.LC38
	.quad	554201088
	.quad	.LC39
	.quad	554131456
	.quad	.LC40
	.quad	554057728
	.quad	.LC41
	.quad	554061824
	.quad	.LC42
	.quad	554065920
	.quad	.LC43
	.quad	554184704
	.quad	.LC44
	.quad	554070016
	.quad	.LC45
	.quad	554160128
	.quad	.LC46
	.quad	554213376
	.quad	.LC47
	.quad	554074112
	.quad	.LC48
	.quad	554106880
	.quad	.LC49
	.quad	554172416
	.quad	.LC50
	.quad	554078208
	.quad	.LC51
	.quad	554086400
	.quad	.LC52
	.quad	554115072
	.quad	.LC53
	.quad	554192896
	.quad	.LC54
	.quad	554188800
	.quad	.LC55
	.quad	554119168
	.quad	.LC56
	.quad	554196992
	.quad	.LC57
	.quad	554168320
	.quad	.LC58
	.quad	554156032
	.quad	.LC59
	.quad	554180608
	.quad	.LC60
	.quad	554090496
	.quad	.LC61
	.quad	554094592
	.quad	.LC62
	.quad	554164224
	.quad	.LC63
	.quad	554098688
	.quad	.LC64
	.quad	554123264
	.quad	.LC65
	.quad	554110976
	.quad	.LC66
	.quad	554176512
	.quad	.LC67
	.quad	554217472
	.quad	.LC68
	.quad	554209280
	.quad	.LC69
	.quad	554135552
	.quad	.LC70
	.quad	554127360
	.quad	.LC71
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
