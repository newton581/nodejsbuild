	.file	"x509_d2.c"
	.text
	.p2align 4
	.globl	X509_STORE_set_default_paths
	.type	X509_STORE_set_default_paths, @function
X509_STORE_set_default_paths:
.LFB779:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	X509_LOOKUP_file@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	X509_STORE_add_lookup@PLT
	testq	%rax, %rax
	je	.L4
	movq	%rax, %rdi
	xorl	%r8d, %r8d
	movl	$3, %ecx
	xorl	%edx, %edx
	movl	$1, %esi
	call	X509_LOOKUP_ctrl@PLT
	call	X509_LOOKUP_hash_dir@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	X509_STORE_add_lookup@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L4
	xorl	%r8d, %r8d
	movl	$3, %ecx
	xorl	%edx, %edx
	movl	$2, %esi
	call	X509_LOOKUP_ctrl@PLT
	call	ERR_clear_error@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE779:
	.size	X509_STORE_set_default_paths, .-X509_STORE_set_default_paths
	.p2align 4
	.globl	X509_STORE_load_locations
	.type	X509_STORE_load_locations, @function
X509_STORE_load_locations:
.LFB780:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L17
	call	X509_LOOKUP_file@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	X509_STORE_add_lookup@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L16
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%rbx, %rdx
	movl	$1, %esi
	call	X509_LOOKUP_ctrl@PLT
	cmpl	$1, %eax
	je	.L17
.L16:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L23
	call	X509_LOOKUP_hash_dir@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	X509_STORE_add_lookup@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L16
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	movl	$1, %ecx
	movl	$2, %esi
	call	X509_LOOKUP_ctrl@PLT
	cmpl	$1, %eax
	sete	%al
	addq	$8, %rsp
	popq	%rbx
	movzbl	%al, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	xorl	%eax, %eax
	testq	%rbx, %rbx
	setne	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE780:
	.size	X509_STORE_load_locations, .-X509_STORE_load_locations
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
