	.file	"sm2_pmeth.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/sm2/sm2_pmeth.c"
	.text
	.p2align 4
	.type	pkey_sm2_cleanup, @function
pkey_sm2_cleanup:
.LFB470:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	40(%rdi), %r12
	testq	%r12, %r12
	je	.L1
	movq	%rdi, %rbx
	movq	(%r12), %rdi
	call	EC_GROUP_free@PLT
	movq	16(%r12), %rdi
	movl	$51, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$52, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	movq	$0, 40(%rbx)
.L1:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE470:
	.size	pkey_sm2_cleanup, .-pkey_sm2_cleanup
	.p2align 4
	.type	pkey_sm2_ctrl, @function
pkey_sm2_ctrl:
.LFB476:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	40(%rdi), %rbx
	cmpl	$4109, %esi
	jg	.L26
	movq	%rcx, %r12
	cmpl	$4096, %esi
	jle	.L29
	subl	$4097, %esi
	cmpl	$12, %esi
	ja	.L26
	leaq	.L14(%rip), %rcx
	movslq	(%rcx,%rsi,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L14:
	.long	.L18-.L14
	.long	.L17-.L14
	.long	.L26-.L14
	.long	.L26-.L14
	.long	.L26-.L14
	.long	.L26-.L14
	.long	.L26-.L14
	.long	.L26-.L14
	.long	.L26-.L14
	.long	.L26-.L14
	.long	.L16-.L14
	.long	.L15-.L14
	.long	.L13-.L14
	.text
.L26:
	movl	$-2, %eax
.L8:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movl	$1, %eax
	cmpl	$7, %esi
	je	.L8
	cmpl	$13, %esi
	jne	.L30
	movq	8(%rbx), %rdx
	movq	%rdx, (%rcx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L30:
	.cfi_restore_state
	cmpl	$1, %esi
	jne	.L26
	movq	%rcx, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L13:
	.cfi_restore_state
	movq	24(%rbx), %rax
	movq	%rax, (%r12)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L18:
	.cfi_restore_state
	movl	%edx, %edi
	call	EC_GROUP_new_by_curve_name@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L31
	movq	(%rbx), %rdi
	call	EC_GROUP_free@PLT
	movq	%r12, (%rbx)
	movl	$1, %eax
	jmp	.L8
.L17:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L32
	movl	%edx, %esi
	call	EC_GROUP_set_asn1_flag@PLT
	movl	$1, %eax
	jmp	.L8
.L16:
	movslq	%edx, %r14
	testl	%edx, %edx
	jle	.L21
	movl	$198, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L33
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	16(%rbx), %rdi
	movl	$204, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r13, 16(%rbx)
.L23:
	movq	%r14, 24(%rbx)
	movl	$1, %eax
	movl	$1, 32(%rbx)
	jmp	.L8
.L15:
	movq	16(%rbx), %rsi
	movq	24(%rbx), %rdx
	movq	%r12, %rdi
	call	memcpy@PLT
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	movq	16(%rbx), %rdi
	movl	$208, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 16(%rbx)
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L32:
	movl	$182, %r8d
	movl	$109, %edx
	movl	$109, %esi
	movl	$53, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L31:
	movl	$173, %r8d
	movl	$108, %edx
	movl	$109, %esi
	movl	$53, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L33:
	movl	$200, %r8d
	movl	$65, %edx
	movl	$109, %esi
	movl	$53, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L8
	.cfi_endproc
.LFE476:
	.size	pkey_sm2_ctrl, .-pkey_sm2_ctrl
	.p2align 4
	.type	pkey_sm2_decrypt, @function
pkey_sm2_decrypt:
.LFB475:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	movq	%rcx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %rax
	movq	40(%rax), %r13
	movq	40(%rdi), %rax
	movq	8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L41
.L35:
	testq	%rbx, %rbx
	je	.L42
	addq	$24, %rsp
	movq	%rbx, %r8
	movq	%r12, %rcx
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	sm2_decrypt@PLT
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%r9, %rcx
	call	sm2_plaintext_size@PLT
	cmpl	$1, %eax
	sbbl	%eax, %eax
	addq	$24, %rsp
	popq	%rbx
	orl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movq	%r9, -40(%rbp)
	movq	%rcx, -48(%rbp)
	call	EVP_sm3@PLT
	movq	-48(%rbp), %rdx
	movq	-40(%rbp), %r9
	movq	%rax, %rsi
	jmp	.L35
	.cfi_endproc
.LFE475:
	.size	pkey_sm2_decrypt, .-pkey_sm2_decrypt
	.p2align 4
	.type	pkey_sm2_encrypt, @function
pkey_sm2_encrypt:
.LFB474:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	movq	%rcx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %rax
	movq	40(%rax), %r13
	movq	40(%rdi), %rax
	movq	8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L50
.L44:
	testq	%rbx, %rbx
	je	.L51
	addq	$24, %rsp
	movq	%rbx, %r8
	movq	%r12, %rcx
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	sm2_encrypt@PLT
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%r9, %rcx
	call	sm2_ciphertext_size@PLT
	cmpl	$1, %eax
	sbbl	%eax, %eax
	addq	$24, %rsp
	popq	%rbx
	orl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	movq	%r9, -40(%rbp)
	movq	%rcx, -48(%rbp)
	call	EVP_sm3@PLT
	movq	-48(%rbp), %rdx
	movq	-40(%rbp), %r9
	movq	%rax, %rsi
	jmp	.L44
	.cfi_endproc
.LFE474:
	.size	pkey_sm2_encrypt, .-pkey_sm2_encrypt
	.p2align 4
	.type	pkey_sm2_verify, @function
pkey_sm2_verify:
.LFB473:
	.cfi_startproc
	endbr64
	movq	%rdi, %r10
	movq	%rsi, %r9
	movq	%r8, %rsi
	movq	%rcx, %rdi
	movq	16(%r10), %rax
	movl	%edx, %ecx
	movq	%r9, %rdx
	movq	40(%rax), %r8
	jmp	sm2_verify@PLT
	.cfi_endproc
.LFE473:
	.size	pkey_sm2_verify, .-pkey_sm2_verify
	.p2align 4
	.type	pkey_sm2_digest_custom, @function
pkey_sm2_digest_custom:
.LFB478:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	40(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%rsi, %rdi
	movq	40(%rax), %r14
	call	EVP_MD_CTX_md@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	EVP_MD_size@PLT
	movl	%eax, %ebx
	movl	32(%r15), %eax
	testl	%eax, %eax
	je	.L62
	testl	%ebx, %ebx
	js	.L63
	movq	24(%r15), %rcx
	movq	16(%r15), %rdx
	leaq	-128(%rbp), %r15
	movq	%r14, %r8
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	sm2_compute_z_digest@PLT
	testl	%eax, %eax
	jne	.L64
.L53:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L65
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
	movslq	%ebx, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L63:
	movl	$279, %r8d
	movl	$102, %edx
	movl	$114, %esi
	movl	$53, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L62:
	movl	$274, %r8d
	movl	$112, %edx
	movl	$114, %esi
	movl	$53, %edi
	leaq	.LC0(%rip), %rcx
	movl	%eax, -132(%rbp)
	call	ERR_put_error@PLT
	movl	-132(%rbp), %eax
	jmp	.L53
.L65:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE478:
	.size	pkey_sm2_digest_custom, .-pkey_sm2_digest_custom
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"ec_paramgen_curve"
.LC2:
	.string	"ec_param_enc"
.LC3:
	.string	"explicit"
.LC4:
	.string	"named_curve"
	.text
	.p2align 4
	.type	pkey_sm2_ctrl_str, @function
pkey_sm2_ctrl_str:
.LFB477:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$18, %ecx
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	leaq	.LC1(%rip), %rdi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	subq	$16, %rsp
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L78
	movq	%rax, %rsi
	movl	$13, %ecx
	leaq	.LC2(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L72
	movl	$9, %ecx
	leaq	.LC3(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%r8b
	sbbb	$0, %r8b
	movsbl	%r8b, %r8d
	testl	%r8d, %r8d
	je	.L70
	movl	$12, %ecx
	leaq	.LC4(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L72
	movl	$1, %r8d
.L70:
	xorl	%r9d, %r9d
	movl	$4098, %ecx
.L77:
	addq	$16, %rsp
	movq	%r13, %rdi
	movl	$6, %edx
	movl	$408, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	EVP_PKEY_CTX_ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	movq	%r12, %rdi
	call	EC_curve_nist2nid@PLT
	testl	%eax, %eax
	jne	.L68
	movq	%r12, %rdi
	call	OBJ_sn2nid@PLT
	testl	%eax, %eax
	je	.L79
.L68:
	xorl	%r9d, %r9d
	movl	%eax, %r8d
	movl	$4097, %ecx
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L79:
	movq	%r12, %rdi
	call	OBJ_ln2nid@PLT
	testl	%eax, %eax
	jne	.L68
	movl	$241, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$108, %edx
	movl	%eax, -20(%rbp)
	movl	$110, %esi
	movl	$53, %edi
	call	ERR_put_error@PLT
	movl	-20(%rbp), %eax
.L66:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore_state
	movl	$-2, %eax
	jmp	.L66
	.cfi_endproc
.LFE477:
	.size	pkey_sm2_ctrl_str, .-pkey_sm2_ctrl_str
	.p2align 4
	.type	pkey_sm2_sign, @function
pkey_sm2_sign:
.LFB472:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	%r8, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	40(%rax), %r13
	movq	%r13, %rdi
	call	ECDSA_size@PLT
	testl	%eax, %eax
	jle	.L80
	cltq
	testq	%r14, %r14
	je	.L87
	cmpq	%rax, (%rbx)
	jb	.L88
	movl	-72(%rbp), %esi
	leaq	-60(%rbp), %rcx
	movq	%r13, %r8
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	sm2_sign@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	jle	.L80
	movl	-60(%rbp), %eax
.L87:
	movq	%rax, (%rbx)
	movl	$1, %r15d
.L80:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L89
	addq	$40, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	movl	$106, %r8d
	movl	$107, %edx
	movl	$112, %esi
	movl	$53, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L80
.L89:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE472:
	.size	pkey_sm2_sign, .-pkey_sm2_sign
	.p2align 4
	.type	pkey_sm2_init, @function
pkey_sm2_init:
.LFB469:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$36, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$40, %edi
	subq	$8, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L94
	movq	%rax, 40(%rbx)
	movl	$1, %eax
.L90:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	movl	$37, %r8d
	movl	$65, %edx
	movl	$111, %esi
	movl	$53, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L90
	.cfi_endproc
.LFE469:
	.size	pkey_sm2_init, .-pkey_sm2_init
	.p2align 4
	.type	pkey_sm2_copy, @function
pkey_sm2_copy:
.LFB471:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$36, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$40, %edi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	leaq	.LC0(%rip), %rsi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L113
	movq	%rax, 40(%r13)
	movq	40(%r12), %r12
	movq	%rax, %rbx
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L98
	call	EC_GROUP_dup@PLT
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L112
.L98:
	cmpq	$0, 16(%r12)
	movq	24(%r12), %rdi
	je	.L100
	movl	$73, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L114
	movq	16(%r12), %rsi
	movq	24(%r12), %rdx
	call	memcpy@PLT
	movq	24(%r12), %rdi
.L100:
	movl	32(%r12), %eax
	movq	%rdi, 24(%rbx)
	movl	%eax, 32(%rbx)
	movq	8(%r12), %rax
	movq	%rax, 8(%rbx)
	movl	$1, %eax
.L95:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore_state
	movl	$75, %r8d
	movl	$65, %edx
	movl	$115, %esi
	movl	$53, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L112:
	movq	40(%r13), %r12
	testq	%r12, %r12
	je	.L111
	movq	(%r12), %rdi
	call	EC_GROUP_free@PLT
	movq	16(%r12), %rdi
	movl	$51, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$52, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	movq	$0, 40(%r13)
.L111:
	xorl	%eax, %eax
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L113:
	movl	$37, %r8d
	movl	$65, %edx
	movl	$111, %esi
	movl	$53, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L95
	.cfi_endproc
.LFE471:
	.size	pkey_sm2_copy, .-pkey_sm2_copy
	.globl	sm2_pkey_meth
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	sm2_pkey_meth, @object
	.size	sm2_pkey_meth, 256
sm2_pkey_meth:
	.long	1172
	.long	0
	.quad	pkey_sm2_init
	.quad	pkey_sm2_copy
	.quad	pkey_sm2_cleanup
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	pkey_sm2_sign
	.quad	0
	.quad	pkey_sm2_verify
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	pkey_sm2_encrypt
	.quad	0
	.quad	pkey_sm2_decrypt
	.quad	0
	.quad	0
	.quad	pkey_sm2_ctrl
	.quad	pkey_sm2_ctrl_str
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	pkey_sm2_digest_custom
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
