	.file	"pcy_tree.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/x509v3/pcy_tree.c"
	.text
	.p2align 4
	.type	exnode_free, @function
exnode_free:
.LFB1331:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L1
	testb	$8, (%rax)
	jne	.L10
.L1:
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movl	$604, %edx
	leaq	.LC0(%rip), %rsi
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE1331:
	.size	exnode_free, .-exnode_free
	.p2align 4
	.type	X509_policy_tree_free.part.0, @function
X509_policy_tree_free.part.0:
.LFB1339:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	24(%rdi), %rdi
	call	OPENSSL_sk_free@PLT
	movq	32(%r13), %rdi
	leaq	exnode_free(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movl	8(%r13), %eax
	movq	0(%r13), %rbx
	testl	%eax, %eax
	jle	.L12
	movq	policy_node_free@GOTPCREL(%rip), %r14
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L13:
	movq	(%rbx), %rdi
	addl	$1, %r12d
	addq	$32, %rbx
	call	X509_free@PLT
	movq	-24(%rbx), %rdi
	movq	%r14, %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	-16(%rbx), %rdi
	call	policy_node_free@PLT
	cmpl	8(%r13), %r12d
	jl	.L13
.L12:
	movq	16(%r13), %rdi
	movq	policy_data_free@GOTPCREL(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	0(%r13), %rdi
	movl	$625, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	movl	$626, %edx
	popq	%r13
	leaq	.LC0(%rip), %rsi
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE1339:
	.size	X509_policy_tree_free.part.0, .-X509_policy_tree_free.part.0
	.p2align 4
	.globl	X509_policy_tree_free
	.type	X509_policy_tree_free, @function
X509_policy_tree_free:
.LFB1332:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L16
	jmp	X509_policy_tree_free.part.0
	.p2align 4,,10
	.p2align 3
.L16:
	ret
	.cfi_endproc
.LFE1332:
	.size	X509_policy_tree_free, .-X509_policy_tree_free
	.p2align 4
	.globl	X509_policy_check
	.type	X509_policy_check, @function
X509_policy_check:
.LFB1333:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -128(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, (%rdi)
	movq	%rdx, %rdi
	movl	$0, (%rsi)
	movq	$0, -64(%rbp)
	call	OPENSSL_sk_num@PLT
	movl	%eax, %ebx
	xorl	%eax, %eax
	testl	$256, %r14d
	movl	%eax, %r12d
	movl	%eax, %ecx
	cmove	%ebx, %r12d
	testl	$512, %r14d
	cmove	%ebx, %ecx
	andl	$1024, %r14d
	cmove	%ebx, %eax
	movl	%ecx, -96(%rbp)
	movl	%eax, -104(%rbp)
	cmpl	$1, %ebx
	je	.L22
	movl	%ebx, %eax
	subl	$2, %eax
	movl	%eax, -80(%rbp)
	js	.L23
	movl	%eax, %r14d
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L235:
	subl	$1, %r14d
	cmpl	$-1, %r14d
	je	.L234
.L25:
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	xorl	%edx, %edx
	movl	$-1, %esi
	movq	%rax, %r15
	movq	%rax, %rdi
	call	X509_check_purpose@PLT
	movq	%r15, %rdi
	call	policy_cache_set@PLT
	testq	%rax, %rax
	jne	.L235
.L233:
	xorl	%r14d, %r14d
.L18:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L236
	addq	$88, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	movq	%rax, %rdi
	call	X509_policy_tree_free.part.0
	movl	-116(%rbp), %edx
	testl	%edx, %edx
	jne	.L54
.L22:
	movl	$1, %r14d
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L234:
	movl	$1, %r8d
	movl	%ebx, -120(%rbp)
	movl	-80(%rbp), %r15d
	movl	%r14d, -116(%rbp)
	movl	%r8d, %ebx
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L239:
	cmpl	$1, %ebx
	jne	.L237
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	X509_get_extension_flags@PLT
	movl	%eax, %r14d
	testb	$8, %ah
	jne	.L226
	movq	%rbx, %rdi
	movl	$2, %ebx
	call	policy_cache_set@PLT
	cmpq	$0, 8(%rax)
	je	.L29
.L128:
	movl	$1, %ebx
	testl	%r12d, %r12d
	jg	.L28
.L29:
	subl	$1, %r15d
	cmpl	$-1, %r15d
	je	.L238
.L26:
	testl	%r12d, %r12d
	jle	.L239
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	movq	%rax, -72(%rbp)
	call	X509_get_extension_flags@PLT
	movl	%eax, %r14d
	testb	$8, %ah
	jne	.L226
	movq	-72(%rbp), %rdi
	call	policy_cache_set@PLT
	cmpl	$1, %ebx
	je	.L27
.L144:
	movl	$2, %ebx
.L28:
	movl	%r14d, %edx
	movq	24(%rax), %rax
	andl	$32, %edx
	cmpl	$1, %edx
	sbbl	$0, %r12d
	testq	%rax, %rax
	js	.L29
	movslq	%r12d, %rdx
	cmpq	%rdx, %rax
	cmovl	%eax, %r12d
	subl	$1, %r15d
	cmpl	$-1, %r15d
	jne	.L26
.L238:
	movl	%ebx, %r8d
	movl	-120(%rbp), %ebx
	movl	%r8d, %eax
	andl	$1, %eax
	movl	%eax, -116(%rbp)
	testl	%r12d, %r12d
	jne	.L240
	movl	-116(%rbp), %r11d
	orl	$4, %r8d
	testl	%r11d, %r11d
	je	.L37
.L36:
	movl	$166, %edx
	leaq	.LC0(%rip), %rsi
	movl	$48, %edi
	movl	%r8d, -116(%rbp)
	call	CRYPTO_zalloc@PLT
	movl	-116(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, -72(%rbp)
	je	.L241
	movslq	%ebx, %rdi
	movl	$178, %edx
	leaq	.LC0(%rip), %rsi
	movl	%r8d, -116(%rbp)
	salq	$5, %rdi
	call	CRYPTO_zalloc@PLT
	movl	-116(%rbp), %r8d
	movq	%rax, %r14
	movq	-72(%rbp), %rax
	testq	%r14, %r14
	movq	%r14, (%rax)
	je	.L242
	movl	%ebx, 8(%rax)
	movl	$746, %edi
	movq	%rax, %r15
	movl	%r8d, -116(%rbp)
	call	OBJ_nid2obj@PLT
	xorl	%edx, %edx
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	policy_data_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L71
	xorl	%edx, %edx
	movq	%r15, %rcx
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	level_add_node@PLT
	testq	%rax, %rax
	je	.L41
	movl	-80(%rbp), %r10d
	movl	-116(%rbp), %r8d
	testl	%r10d, %r10d
	js	.L43
	movl	-104(%rbp), %ecx
	movl	-80(%rbp), %r15d
	movl	%r8d, -116(%rbp)
	movq	%r13, -104(%rbp)
	movl	-96(%rbp), %r13d
	.p2align 4,,10
	.p2align 3
.L42:
	movq	-104(%rbp), %rdi
	movl	%r15d, %esi
	movl	%ecx, -96(%rbp)
	addq	$32, %r14
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rbx
	movq	%rax, %rdi
	call	X509_get_extension_flags@PLT
	movq	%rbx, %rdi
	movl	%eax, %r12d
	call	policy_cache_set@PLT
	movq	%rbx, %rdi
	movq	%rax, -80(%rbp)
	call	X509_up_ref@PLT
	movq	-80(%rbp), %rdx
	movl	-96(%rbp), %ecx
	movq	%rbx, (%r14)
	cmpq	$0, (%rdx)
	je	.L243
.L44:
	andl	$32, %r12d
	testl	%r13d, %r13d
	jne	.L45
	testl	%r12d, %r12d
	je	.L145
	testl	%r15d, %r15d
	je	.L145
	testl	%ecx, %ecx
	jne	.L52
	.p2align 4,,10
	.p2align 3
.L125:
	orl	$1024, 24(%r14)
	xorl	%ecx, %ecx
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L226:
	movl	-116(%rbp), %r14d
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L45:
	movq	16(%rdx), %rax
	cmpl	$1, %r12d
	sbbl	$0, %r13d
	testq	%rax, %rax
	js	.L48
	movslq	%r13d, %rdi
	cmpq	%rdi, %rax
	cmovl	%eax, %r13d
.L48:
	testl	%ecx, %ecx
	je	.L125
	cmpl	$1, %r12d
	sbbl	$0, %ecx
.L52:
	movq	32(%rdx), %rax
	testq	%rax, %rax
	js	.L51
	movslq	%ecx, %rdx
	cmpq	%rdx, %rax
	cmovl	%eax, %ecx
.L51:
	subl	$1, %r15d
	cmpl	$-1, %r15d
	jne	.L42
	movl	-116(%rbp), %r8d
.L43:
	movl	%r8d, %eax
	andl	$2, %eax
	andl	$4, %r8d
	movl	%r8d, -116(%rbp)
	je	.L244
.L123:
	movq	-88(%rbp), %rcx
	movl	$1, (%rcx)
	testl	%eax, %eax
	jne	.L54
.L53:
	movq	-72(%rbp), %rax
	movq	(%rax), %rdx
	movslq	8(%rax), %rax
	leaq	32(%rdx), %r13
	cmpl	$1, %eax
	jle	.L55
	movl	$1, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L91:
	movq	0(%r13), %rdi
	xorl	%r15d, %r15d
	call	policy_cache_set@PLT
	movq	%rax, -80(%rbp)
	leaq	-32(%r13), %rax
	movq	%rax, -96(%rbp)
	.p2align 4,,10
	.p2align 3
.L56:
	movq	-80(%rbp), %rax
	movq	8(%rax), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r15d
	jge	.L245
	movq	-80(%rbp), %rax
	movl	%r15d, %esi
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	movq	8(%rax), %rdi
	call	OPENSSL_sk_value@PLT
	movl	%r15d, -104(%rbp)
	movq	%r13, %r15
	movq	%rax, %r13
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L58:
	addl	$1, %ebx
.L57:
	movq	-24(%r15), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L246
	movq	-24(%r15), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	8(%r13), %rdx
	movq	-96(%rbp), %rdi
	movq	%rax, %rsi
	movq	%rax, %r12
	call	policy_node_match@PLT
	testl	%eax, %eax
	je	.L58
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	level_add_node@PLT
	testq	%rax, %rax
	je	.L71
	movl	$1, %r14d
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L237:
	movl	$6, %r8d
	testl	%r12d, %r12d
	jne	.L22
.L37:
	movl	%r8d, %eax
	movq	$0, -72(%rbp)
	movl	$4, -116(%rbp)
	andl	$2, %eax
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L246:
	movq	%r13, %r12
	movq	%r15, %r13
	movl	-104(%rbp), %r15d
	testl	%r14d, %r14d
	jne	.L62
	movq	-16(%r13), %rdx
	testq	%rdx, %rdx
	je	.L62
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	level_add_node@PLT
	testq	%rax, %rax
	je	.L71
.L62:
	addl	$1, %r15d
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L245:
	movl	24(%r13), %eax
	testb	$2, %ah
	jne	.L78
	xorl	%ebx, %ebx
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L249:
	movl	16(%rax), %r14d
.L66:
	testl	%r14d, %r14d
	jne	.L70
	movq	(%r12), %rax
	xorl	%edi, %edi
	movl	(%rax), %edx
	movq	8(%rax), %rsi
	andl	$16, %edx
	call	policy_data_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L71
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	orl	$4, (%r14)
	movq	%rax, 16(%r14)
	call	level_add_node@PLT
	testq	%rax, %rax
	je	.L247
.L70:
	addl	$1, %ebx
.L69:
	movq	-24(%r13), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L248
	movq	-24(%r13), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r12
	testb	$4, -7(%r13)
	jne	.L249
	movq	(%rax), %rax
	movl	16(%r12), %r14d
	testb	$1, (%rax)
	je	.L66
	movq	24(%rax), %r15
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%r14d, %eax
	je	.L70
	xorl	%r14d, %r14d
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L73:
	addl	$1, %r14d
.L72:
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r14d
	jge	.L70
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%rax, -96(%rbp)
	call	level_find_node@PLT
	movq	-96(%rbp), %r8
	testq	%rax, %rax
	jne	.L73
	movq	(%r12), %rax
	testq	%r8, %r8
	je	.L250
.L74:
	movl	(%rax), %edx
	movq	%r8, %rsi
	xorl	%edi, %edi
	andl	$16, %edx
	call	policy_data_new@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L71
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rcx
	movq	%r8, %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%r8, -96(%rbp)
	movq	(%rax), %rax
	movq	16(%rax), %rax
	orl	$4, (%r8)
	movq	%rax, 16(%r8)
	call	level_add_node@PLT
	movq	-96(%rbp), %r8
	testq	%rax, %rax
	jne	.L73
	movq	%r8, %rdi
	call	policy_data_free@PLT
	.p2align 4,,10
	.p2align 3
.L71:
	movq	-72(%rbp), %rdi
	call	X509_policy_tree_free.part.0
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L248:
	movq	-16(%r13), %rdx
	testq	%rdx, %rdx
	je	.L231
	movq	-80(%rbp), %rax
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	level_add_node@PLT
	testq	%rax, %rax
	je	.L71
.L231:
	movl	24(%r13), %eax
.L78:
	testb	$4, %ah
	jne	.L79
.L81:
	movq	%r13, %r12
	.p2align 4,,10
	.p2align 3
.L80:
	movq	-24(%r12), %r14
	subq	$32, %r12
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	subl	$1, %eax
	movl	%eax, %ebx
	jns	.L84
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L87:
	subl	$1, %ebx
	cmpl	$-1, %ebx
	je	.L88
.L84:
	movq	%r14, %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movl	16(%rax), %r9d
	movq	%rax, %rdi
	testl	%r9d, %r9d
	jne	.L87
	movq	8(%rax), %rax
	movl	$416, %edx
	leaq	.LC0(%rip), %rsi
	subl	$1, 16(%rax)
	call	CRYPTO_free@PLT
	movl	%ebx, %esi
	movq	%r14, %rdi
	subl	$1, %ebx
	call	OPENSSL_sk_delete@PLT
	cmpl	$-1, %ebx
	jne	.L84
.L88:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L86
	movl	16(%rdi), %r8d
	testl	%r8d, %r8d
	jne	.L86
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L89
	subl	$1, 16(%rax)
.L89:
	movl	$423, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 16(%r12)
.L86:
	movq	-72(%rbp), %rax
	movq	(%rax), %r15
	cmpq	%r15, %r12
	jne	.L80
	cmpq	$0, 16(%r15)
	je	.L90
	addl	$1, -88(%rbp)
	movl	8(%rax), %eax
	addq	$32, %r13
	movl	-88(%rbp), %ecx
	cmpl	%eax, %ecx
	jl	.L91
	movslq	%eax, %rcx
	salq	$5, %rcx
	movq	-16(%r15,%rcx), %r12
	testq	%r12, %r12
	jne	.L126
	movq	-72(%rbp), %rcx
	leaq	24(%rcx), %r12
	cmpl	$1, %eax
	jle	.L100
	leaq	-64(%rbp), %rax
	movq	%rax, -88(%rbp)
.L95:
	movl	$1, -80(%rbp)
.L107:
	movq	16(%r15), %r13
	testq	%r13, %r13
	je	.L108
	addq	$32, %r15
	xorl	%ebx, %ebx
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L102:
	addl	$1, %ebx
.L101:
	movq	8(%r15), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L251
	movq	8(%r15), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r14
	cmpq	8(%rax), %r13
	jne	.L102
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L252
.L103:
	movq	%r14, %rsi
	call	OPENSSL_sk_find@PLT
	testl	%eax, %eax
	jns	.L102
	movq	(%r12), %rdi
	movq	%r14, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L102
	cmpq	-88(%rbp), %r12
	jne	.L71
.L256:
	movq	-64(%rbp), %rdi
	call	OPENSSL_sk_free@PLT
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L79:
	movq	8(%r13), %r12
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	subl	$1, %eax
	movl	%eax, %ebx
	jns	.L83
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L82:
	subl	$1, %ebx
	cmpl	$-1, %ebx
	je	.L81
.L83:
	movq	%r12, %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	testb	$3, (%rax)
	je	.L82
	movq	8(%rdi), %rax
	movl	$403, %edx
	leaq	.LC0(%rip), %rsi
	subl	$1, 16(%rax)
	call	CRYPTO_free@PLT
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_delete@PLT
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L250:
	movq	8(%rax), %r8
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L247:
	movq	%r14, %rdi
	call	policy_data_free@PLT
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L251:
	movq	-72(%rbp), %rcx
	addl	$1, -80(%rbp)
	movl	-80(%rbp), %eax
	cmpl	8(%rcx), %eax
	jl	.L107
.L108:
	cmpq	-88(%rbp), %r12
	je	.L253
.L100:
	movq	-72(%rbp), %rax
	movq	-112(%rbp), %rdi
	movq	24(%rax), %r12
	movq	%r12, -64(%rbp)
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jle	.L109
	movl	$1, -88(%rbp)
.L97:
	movq	-72(%rbp), %rcx
	movq	-112(%rbp), %r14
	xorl	%ebx, %ebx
	movslq	8(%rcx), %rax
	salq	$5, %rax
	addq	(%rcx), %rax
	movq	-16(%rax), %r13
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L113:
	movq	%r14, %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$746, %eax
	je	.L254
	addl	$1, %ebx
.L110:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L113
	movq	-112(%rbp), %r14
	xorl	%ebx, %ebx
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L115:
	movq	-72(%rbp), %rax
	movq	32(%rax), %rdi
	testq	%rdi, %rdi
	je	.L255
.L118:
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L117
.L120:
	addl	$1, %ebx
.L114:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L112
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	tree_find_sk@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L115
	testq	%r13, %r13
	je	.L120
	movq	0(%r13), %rax
	movq	%r15, %rsi
	xorl	%edi, %edi
	movl	(%rax), %edx
	andl	$16, %edx
	call	policy_data_new@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L117
	movq	0(%r13), %rax
	movq	8(%r13), %rdx
	xorl	%edi, %edi
	movq	-72(%rbp), %rcx
	movq	16(%rax), %rax
	movl	$12, (%rsi)
	movq	%rax, 16(%rsi)
	call	level_add_node@PLT
	movq	%rax, %rsi
	movq	-72(%rbp), %rax
	movq	32(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L118
.L255:
	movq	%rsi, -80(%rbp)
	call	OPENSSL_sk_new_null@PLT
	movq	-80(%rbp), %rsi
	movq	%rax, %rdi
	movq	-72(%rbp), %rax
	testq	%rdi, %rdi
	movq	%rdi, 32(%rax)
	jne	.L118
	.p2align 4,,10
	.p2align 3
.L112:
	cmpl	$2, -88(%rbp)
	je	.L232
.L109:
	movq	-128(%rbp), %rax
	movq	-72(%rbp), %rcx
	movl	-116(%rbp), %esi
	movq	%rcx, (%rax)
	testl	%esi, %esi
	je	.L22
	movq	%rcx, %rdi
	call	X509_policy_tree_get0_user_policies@PLT
	movq	%rax, %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jg	.L22
.L54:
	movl	$-2, %r14d
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L252:
	call	policy_node_cmp_new@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L103
	cmpq	-88(%rbp), %r12
	jne	.L71
	jmp	.L256
.L40:
	.p2align 4,,10
	.p2align 3
.L244:
	testl	%eax, %eax
	je	.L53
	movq	-72(%rbp), %rdi
	movl	$1, %r14d
	call	X509_policy_tree_free.part.0
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L145:
	orl	$512, 24(%r14)
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L243:
	orl	$512, 24(%r14)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L55:
	salq	$5, %rax
	movq	-16(%rdx,%rax), %r12
	testq	%r12, %r12
	je	.L100
	.p2align 4,,10
	.p2align 3
.L126:
	movq	-72(%rbp), %rax
	movq	24(%rax), %rdi
	testq	%rdi, %rdi
	je	.L257
.L93:
	movq	%r12, %rsi
	call	OPENSSL_sk_find@PLT
	testl	%eax, %eax
	js	.L258
.L94:
	movq	-72(%rbp), %rax
	leaq	-64(%rbp), %rcx
	movq	%rcx, -88(%rbp)
	movq	%rcx, %r12
	cmpl	$1, 8(%rax)
	movq	(%rax), %r15
	jg	.L95
	xorl	%r12d, %r12d
.L124:
	movq	-112(%rbp), %rdi
	call	OPENSSL_sk_num@PLT
	movl	$2, -88(%rbp)
	testl	%eax, %eax
	jg	.L97
	.p2align 4,,10
	.p2align 3
.L232:
	movq	%r12, %rdi
	call	OPENSSL_sk_free@PLT
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L240:
	movl	-116(%rbp), %eax
	testl	%eax, %eax
	jne	.L36
	subl	$1, %r8d
	jne	.L22
	movq	$0, -72(%rbp)
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L23:
	cmpl	$1, %r12d
	sbbl	%r8d, %r8d
	andl	$4, %r8d
	addl	$1, %r8d
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L258:
	movq	-72(%rbp), %rax
	movq	%r12, %rsi
	movq	24(%rax), %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L71
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L254:
	movq	-72(%rbp), %rax
	orl	$2, 40(%rax)
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L257:
	call	policy_node_cmp_new@PLT
	movq	%rax, %rdi
	movq	-72(%rbp), %rax
	movq	%rdi, 24(%rax)
	testq	%rdi, %rdi
	jne	.L93
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L117:
	cmpl	$2, -88(%rbp)
	jne	.L71
	movq	%r12, %rdi
	call	OPENSSL_sk_free@PLT
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L241:
	movl	$167, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r14d, %r14d
	movl	$172, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L242:
	movq	%rax, %rdi
	movl	$179, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$180, %r8d
	movl	$65, %edx
	leaq	.LC0(%rip), %rcx
	movl	$172, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	jmp	.L18
.L41:
	movq	%r12, %rdi
	call	policy_data_free@PLT
	jmp	.L71
.L253:
	movq	-64(%rbp), %r12
	jmp	.L124
.L236:
	call	__stack_chk_fail@PLT
.L27:
	cmpq	$0, 8(%rax)
	jne	.L128
	jmp	.L144
	.cfi_endproc
.LFE1333:
	.size	X509_policy_check, .-X509_policy_check
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
