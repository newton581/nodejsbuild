	.file	"pqueue.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/ssl/pqueue.c"
	.text
	.p2align 4
	.globl	pitem_new
	.type	pitem_new, @function
pitem_new:
.LFB964:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$20, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$24, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	leaq	.LC0(%rip), %rsi
	subq	$16, %rsp
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L6
	movq	(%r12), %rdx
	movq	%rbx, 8(%rax)
	movq	$0, 16(%rax)
	movq	%rdx, (%rax)
.L1:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movl	$23, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$624, %esi
	movl	$20, %edi
	movq	%rax, -24(%rbp)
	call	ERR_put_error@PLT
	movq	-24(%rbp), %rax
	jmp	.L1
	.cfi_endproc
.LFE964:
	.size	pitem_new, .-pitem_new
	.p2align 4
	.globl	pitem_free
	.type	pitem_free, @function
pitem_free:
.LFB965:
	.cfi_startproc
	endbr64
	movl	$35, %edx
	leaq	.LC0(%rip), %rsi
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE965:
	.size	pitem_free, .-pitem_free
	.p2align 4
	.globl	pqueue_new
	.type	pqueue_new, @function
pqueue_new:
.LFB966:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$40, %edx
	movl	$16, %edi
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L11
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movl	$43, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$625, %esi
	movl	$20, %edi
	movq	%rax, -8(%rbp)
	call	ERR_put_error@PLT
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE966:
	.size	pqueue_new, .-pqueue_new
	.p2align 4
	.globl	pqueue_free
	.type	pqueue_free, @function
pqueue_free:
.LFB967:
	.cfi_startproc
	endbr64
	movl	$50, %edx
	leaq	.LC0(%rip), %rsi
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE967:
	.size	pqueue_free, .-pqueue_free
	.p2align 4
	.globl	pqueue_insert
	.type	pqueue_insert, @function
pqueue_insert:
.LFB968:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rbx
	testq	%rbx, %rbx
	jne	.L18
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L20:
	movq	%rax, %rbx
.L18:
	movl	$8, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jg	.L27
	je	.L19
	movq	16(%rbx), %rax
	movq	%rbx, %r12
	testq	%rax, %rax
	jne	.L20
	movq	$0, 16(%r14)
	movq	%r14, %rax
	movq	%r14, 16(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	movq	%rbx, 16(%r14)
	testq	%r12, %r12
	je	.L17
	movq	%r14, 16(%r12)
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	movq	%r14, 0(%r13)
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE968:
	.size	pqueue_insert, .-pqueue_insert
	.p2align 4
	.globl	pqueue_peek
	.type	pqueue_peek, @function
pqueue_peek:
.LFB969:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE969:
	.size	pqueue_peek, .-pqueue_peek
	.p2align 4
	.globl	pqueue_pop
	.type	pqueue_pop, @function
pqueue_pop:
.LFB970:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L29
	movq	16(%rax), %rdx
	movq	%rdx, (%rdi)
.L29:
	ret
	.cfi_endproc
.LFE970:
	.size	pqueue_pop, .-pqueue_pop
	.p2align 4
	.globl	pqueue_find
	.type	pqueue_find, @function
pqueue_find:
.LFB971:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %rbx
	testq	%rbx, %rbx
	jne	.L36
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L37:
	testl	%eax, %eax
	je	.L34
.L36:
	movq	%rbx, %r12
	movq	16(%rbx), %rbx
	movl	$8, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	memcmp@PLT
	testq	%rbx, %rbx
	jne	.L37
	testl	%eax, %eax
	cmovne	%rbx, %r12
.L34:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L47:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L34
	.cfi_endproc
.LFE971:
	.size	pqueue_find, .-pqueue_find
	.p2align 4
	.globl	pqueue_iterator
	.type	pqueue_iterator, @function
pqueue_iterator:
.LFB976:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE976:
	.size	pqueue_iterator, .-pqueue_iterator
	.p2align 4
	.globl	pqueue_next
	.type	pqueue_next, @function
pqueue_next:
.LFB973:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L51
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L49
	movq	16(%rax), %rdx
	movq	%rdx, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	xorl	%eax, %eax
.L49:
	ret
	.cfi_endproc
.LFE973:
	.size	pqueue_next, .-pqueue_next
	.p2align 4
	.globl	pqueue_size
	.type	pqueue_size, @function
pqueue_size:
.LFB974:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	xorl	%r8d, %r8d
	testq	%rax, %rax
	je	.L55
	.p2align 4,,10
	.p2align 3
.L57:
	movq	16(%rax), %rax
	addq	$1, %r8
	testq	%rax, %rax
	jne	.L57
.L55:
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE974:
	.size	pqueue_size, .-pqueue_size
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
