	.file	"dh_ameth.c"
	.text
	.p2align 4
	.type	dh_missing_parameters, @function
dh_missing_parameters:
.LFB1460:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	testq	%rax, %rax
	je	.L4
	cmpq	$0, 8(%rax)
	je	.L4
	cmpq	$0, 16(%rax)
	sete	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1460:
	.size	dh_missing_parameters, .-dh_missing_parameters
	.p2align 4
	.type	int_dh_free, @function
int_dh_free:
.LFB1444:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	jmp	DH_free@PLT
	.cfi_endproc
.LFE1444:
	.size	int_dh_free, .-int_dh_free
	.p2align 4
	.type	dh_bits, @function
dh_bits:
.LFB1453:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movq	8(%rax), %rdi
	jmp	BN_num_bits@PLT
	.cfi_endproc
.LFE1453:
	.size	dh_bits, .-dh_bits
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"DH Private-Key"
.LC1:
	.string	"%s: (%d bit)\n"
.LC2:
	.string	"private-key:"
.LC3:
	.string	"public-key:"
.LC4:
	.string	"prime:"
.LC5:
	.string	"generator:"
.LC6:
	.string	"subgroup order:"
.LC7:
	.string	"subgroup factor:"
.LC8:
	.string	"seed:"
.LC9:
	.string	"\n"
.LC10:
	.string	":"
.LC11:
	.string	"%02x%s"
.LC12:
	.string	""
.LC13:
	.string	"counter:"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC14:
	.string	"recommended-private-length: %d bits\n"
	.align 8
.LC15:
	.string	"../deps/openssl/openssl/crypto/dh/dh_ameth.c"
	.text
	.p2align 4
	.type	dh_private_print, @function
dh_private_print:
.LFB1464:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	40(%rsi), %rbx
	movq	40(%rbx), %r13
	cmpq	$0, 8(%rbx)
	sete	%dl
	movq	32(%rbx), %r14
	testq	%r13, %r13
	sete	%al
	orb	%al, %dl
	jne	.L28
	testq	%r14, %r14
	je	.L28
	movl	$128, %edx
	movl	%r15d, %esi
	movq	%rdi, %r12
	call	BIO_indent@PLT
	movq	8(%rbx), %rdi
	call	BN_num_bits@PLT
	leaq	.LC0(%rip), %rdx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, %ecx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L11
	leal	4(%r15), %eax
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	%eax, %r8d
	leaq	.LC2(%rip), %rsi
	movl	%eax, -52(%rbp)
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	jne	.L57
.L11:
	movl	$7, %edx
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L28:
	movl	$67, %edx
.L9:
	movl	$343, %r8d
	movl	$100, %esi
	movl	$5, %edi
	leaq	.LC15(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L8:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	movl	-52(%rbp), %r13d
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	movl	%r13d, %r8d
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L11
	movq	8(%rbx), %rdx
	xorl	%ecx, %ecx
	movl	%r13d, %r8d
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L11
	movq	16(%rbx), %rdx
	xorl	%ecx, %ecx
	movl	%r13d, %r8d
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L11
	movq	64(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L15
	movl	-52(%rbp), %r8d
	xorl	%ecx, %ecx
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L11
.L15:
	movq	72(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L14
	movl	-52(%rbp), %r8d
	xorl	%ecx, %ecx
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L11
.L14:
	cmpq	$0, 80(%rbx)
	je	.L17
	movl	-52(%rbp), %esi
	movl	$128, %edx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	addl	$8, %r15d
	call	BIO_indent@PLT
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L21:
	leal	1(%r14), %ecx
	cmpl	%eax, %ecx
	movq	80(%rbx), %rax
	leaq	.LC10(%rip), %rcx
	movzbl	(%rax,%r13), %edx
	jne	.L56
	leaq	.LC12(%rip), %rcx
.L56:
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	addq	$1, %r13
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L11
.L20:
	movl	88(%rbx), %eax
	movl	%r13d, %r14d
	cmpl	%r13d, %eax
	jle	.L58
	imull	$-286331153, %r13d, %edx
	cmpl	$286331153, %edx
	ja	.L21
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L11
	movl	$128, %edx
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	BIO_indent@PLT
	testl	%eax, %eax
	je	.L11
	movl	88(%rbx), %eax
	jmp	.L21
.L58:
	movl	$1, %edx
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L8
.L17:
	movq	96(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L19
	movl	-52(%rbp), %r8d
	xorl	%ecx, %ecx
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L11
.L19:
	movl	24(%rbx), %eax
	testl	%eax, %eax
	jne	.L59
.L27:
	movl	$1, %eax
	jmp	.L8
.L59:
	movl	-52(%rbp), %esi
	movl	$128, %edx
	movq	%r12, %rdi
	call	BIO_indent@PLT
	movl	24(%rbx), %edx
	xorl	%eax, %eax
	movq	%r12, %rdi
	leaq	.LC14(%rip), %rsi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jg	.L27
	jmp	.L11
	.cfi_endproc
.LFE1464:
	.size	dh_private_print, .-dh_private_print
	.p2align 4
	.type	dh_pkey_param_check, @function
dh_pkey_param_check:
.LFB1468:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	jmp	DH_check_ex@PLT
	.cfi_endproc
.LFE1468:
	.size	dh_pkey_param_check, .-dh_pkey_param_check
	.p2align 4
	.type	dh_security_bits, @function
dh_security_bits:
.LFB1454:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	jmp	DH_security_bits@PLT
	.cfi_endproc
.LFE1454:
	.size	dh_security_bits, .-dh_security_bits
	.p2align 4
	.type	int_dh_size, @function
int_dh_size:
.LFB1452:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	jmp	DH_size@PLT
	.cfi_endproc
.LFE1452:
	.size	int_dh_size, .-int_dh_size
	.p2align 4
	.type	dh_pkey_public_check, @function
dh_pkey_public_check:
.LFB1467:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	movq	32(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L67
	jmp	DH_check_pub_key_ex@PLT
	.p2align 4,,10
	.p2align 3
.L67:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$125, %edx
	movl	$124, %esi
	movl	$5, %edi
	movl	$517, %r8d
	leaq	.LC15(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1467:
	.size	dh_pkey_public_check, .-dh_pkey_public_check
	.p2align 4
	.type	dh_cmp_parameters, @function
dh_cmp_parameters:
.LFB1455:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	40(%rsi), %rax
	movq	%rdi, %rbx
	movq	8(%rax), %rsi
	movq	40(%rdi), %rax
	movq	8(%rax), %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	je	.L71
.L73:
	xorl	%eax, %eax
.L70:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	movq	40(%r12), %rax
	movq	16(%rax), %rsi
	movq	40(%rbx), %rax
	movq	16(%rax), %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	jne	.L73
	movl	$1, %eax
	leaq	dhx_asn1_meth(%rip), %rdx
	cmpq	%rdx, 16(%rbx)
	jne	.L70
	movq	40(%r12), %rax
	movq	64(%rax), %rsi
	movq	40(%rbx), %rax
	movq	64(%rax), %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
	jmp	.L70
	.cfi_endproc
.LFE1455:
	.size	dh_cmp_parameters, .-dh_cmp_parameters
	.p2align 4
	.type	dh_priv_encode, @function
dh_priv_encode:
.LFB1448:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	call	ASN1_STRING_new@PLT
	movl	$212, %r8d
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L87
	leaq	8(%rax), %rsi
	movq	40(%rbx), %rdi
	leaq	dhx_asn1_meth(%rip), %rax
	cmpq	%rax, 16(%rbx)
	je	.L88
	call	i2d_DHparams@PLT
.L80:
	movl	%eax, (%r12)
	testl	%eax, %eax
	jle	.L89
	movl	$16, 4(%r12)
	movq	40(%rbx), %rax
	xorl	%esi, %esi
	movq	40(%rax), %rdi
	call	BN_to_ASN1_INTEGER@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L90
	leaq	-64(%rbp), %rsi
	movq	%rax, %rdi
	call	i2d_ASN1_INTEGER@PLT
	movq	%r14, %rdi
	movl	%eax, %r15d
	call	ASN1_STRING_clear_free@PLT
	movq	16(%rbx), %rax
	movq	-64(%rbp), %r14
	movl	(%rax), %edi
	call	OBJ_nid2obj@PLT
	subq	$8, %rsp
	movq	%r12, %r8
	xorl	%edx, %edx
	pushq	%r15
	movq	%rax, %rsi
	movq	%r14, %r9
	movl	$16, %ecx
	movq	%r13, %rdi
	call	PKCS8_pkey_set0@PLT
	movl	%eax, %r8d
	popq	%rax
	movl	$1, %eax
	popq	%rdx
	testl	%r8d, %r8d
	je	.L78
.L76:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L91
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	movl	$227, %r8d
	movl	$106, %edx
	movl	$111, %esi
	movl	$5, %edi
	leaq	.LC15(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L78:
	movq	-64(%rbp), %rdi
	movl	$243, %edx
	leaq	.LC15(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	call	ASN1_STRING_free@PLT
	xorl	%edi, %edi
	call	ASN1_STRING_clear_free@PLT
	xorl	%eax, %eax
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L89:
	movl	$218, %r8d
.L87:
	movl	$65, %edx
	movl	$111, %esi
	movl	$5, %edi
	leaq	.LC15(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L88:
	call	i2d_DHxparams@PLT
	jmp	.L80
.L91:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1448:
	.size	dh_priv_encode, .-dh_priv_encode
	.p2align 4
	.type	dh_priv_decode, @function
dh_priv_decode:
.LFB1447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-72(%rbp), %r13
	leaq	-48(%rbp), %rcx
	pushq	%r12
	leaq	-80(%rbp), %rdx
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rsi
	xorl	%edi, %edi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	PKCS8_pkey_get0@PLT
	testl	%eax, %eax
	jne	.L118
.L92:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L119
	addq	$56, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	movq	-48(%rbp), %rcx
	xorl	%edi, %edi
	leaq	-56(%rbp), %rdx
	leaq	-76(%rbp), %rsi
	call	X509_ALGOR_get0@PLT
	cmpl	$16, -76(%rbp)
	je	.L120
.L94:
	xorl	%r13d, %r13d
.L97:
	movl	$195, %r8d
	leaq	.LC15(%rip), %rcx
	movl	$114, %edx
	xorl	%r14d, %r14d
	movl	$110, %esi
	movl	$5, %edi
	call	ERR_put_error@PLT
.L99:
	movq	%r14, %rdi
	call	DH_free@PLT
	movq	%r13, %rdi
	call	ASN1_STRING_clear_free@PLT
	xorl	%eax, %eax
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L120:
	movslq	-80(%rbp), %rdx
	movq	%r13, %rsi
	xorl	%edi, %edi
	call	d2i_ASN1_INTEGER@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L94
	movq	-56(%rbp), %rax
	leaq	-64(%rbp), %rsi
	xorl	%edi, %edi
	movq	8(%rax), %rdx
	movq	%rdx, -64(%rbp)
	movslq	(%rax), %rdx
	leaq	dhx_asn1_meth(%rip), %rax
	cmpq	%rax, 16(%r12)
	je	.L121
	call	d2i_DHparams@PLT
	movq	%rax, %r14
.L96:
	testq	%r14, %r14
	je	.L97
	call	BN_secure_new@PLT
	movq	%rax, 40(%r14)
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L100
	movq	%r13, %rdi
	call	ASN1_INTEGER_to_BN@PLT
	testq	%rax, %rax
	je	.L100
	movq	%r14, %rdi
	call	DH_generate_key@PLT
	testl	%eax, %eax
	je	.L99
	movq	16(%r12), %rax
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	(%rax), %esi
	call	EVP_PKEY_assign@PLT
	movq	%r13, %rdi
	call	ASN1_STRING_clear_free@PLT
	movl	$1, %eax
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L100:
	movl	$181, %r8d
	movl	$106, %edx
	movl	$110, %esi
	movl	$5, %edi
	leaq	.LC15(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L121:
	call	d2i_DHxparams@PLT
	movq	%rax, %r14
	jmp	.L96
.L119:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1447:
	.size	dh_priv_decode, .-dh_priv_decode
	.p2align 4
	.type	dh_param_encode, @function
dh_param_encode:
.LFB1450:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %r8
	leaq	dhx_asn1_meth(%rip), %rax
	cmpq	%rax, 16(%rdi)
	movq	%r8, %rdi
	je	.L124
	jmp	i2d_DHparams@PLT
	.p2align 4,,10
	.p2align 3
.L124:
	jmp	i2d_DHxparams@PLT
	.cfi_endproc
.LFE1450:
	.size	dh_param_encode, .-dh_param_encode
	.p2align 4
	.type	dh_pub_encode, @function
dh_pub_encode:
.LFB1446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	40(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	call	ASN1_STRING_new@PLT
	movl	$110, %r8d
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L138
	leaq	8(%rax), %rsi
	movq	%r14, %rdi
	leaq	dhx_asn1_meth(%rip), %rax
	cmpq	%rax, 16(%rbx)
	je	.L139
	call	i2d_DHparams@PLT
.L129:
	movl	%eax, (%r12)
	testl	%eax, %eax
	jle	.L140
	movq	32(%r14), %rdi
	xorl	%esi, %esi
	call	BN_to_ASN1_INTEGER@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L127
	leaq	-64(%rbp), %rsi
	movq	%rax, %rdi
	call	i2d_ASN1_INTEGER@PLT
	movq	%r14, %rdi
	movl	%eax, %r15d
	call	ASN1_INTEGER_free@PLT
	movl	$129, %r8d
	testl	%r15d, %r15d
	jle	.L138
	movq	16(%rbx), %rax
	movq	-64(%rbp), %r14
	movl	(%rax), %edi
	call	OBJ_nid2obj@PLT
	movq	%r14, %r8
	movl	%r15d, %r9d
	movq	%r12, %rcx
	movq	%rax, %rsi
	movl	$16, %edx
	movq	%r13, %rdi
	call	X509_PUBKEY_set0_param@PLT
	movl	%eax, %r8d
	movl	$1, %eax
	testl	%r8d, %r8d
	jne	.L125
.L127:
	movq	-64(%rbp), %rdi
	movl	$138, %edx
	leaq	.LC15(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	call	ASN1_STRING_free@PLT
	xorl	%eax, %eax
.L125:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L141
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	movl	$115, %r8d
.L138:
	movl	$65, %edx
	movl	$109, %esi
	movl	$5, %edi
	leaq	.LC15(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L139:
	call	i2d_DHxparams@PLT
	jmp	.L129
.L141:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1446:
	.size	dh_pub_encode, .-dh_pub_encode
	.p2align 4
	.type	dh_param_decode, @function
dh_param_decode:
.LFB1449:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	dhx_asn1_meth(%rip), %rax
	movslq	%edx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpq	%rax, 16(%rdi)
	movl	$0, %edi
	je	.L148
	call	d2i_DHparams@PLT
	movq	%rax, %rdx
	testq	%rdx, %rdx
	je	.L149
.L145:
	movq	16(%r12), %rax
	movq	%r12, %rdi
	movl	(%rax), %esi
	call	EVP_PKEY_assign@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	.cfi_restore_state
	call	d2i_DHxparams@PLT
	movq	%rax, %rdx
	testq	%rdx, %rdx
	jne	.L145
.L149:
	movl	$255, %r8d
	movl	$5, %edx
	movl	$107, %esi
	movl	$5, %edi
	leaq	.LC15(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1449:
	.size	dh_param_decode, .-dh_param_decode
	.p2align 4
	.type	dh_pub_decode, @function
dh_pub_decode:
.LFB1445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-72(%rbp), %r13
	leaq	-48(%rbp), %rcx
	pushq	%r12
	leaq	-80(%rbp), %rdx
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rsi
	xorl	%edi, %edi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	X509_PUBKEY_get0_param@PLT
	testl	%eax, %eax
	jne	.L164
.L150:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L165
	addq	$56, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	movq	-48(%rbp), %rcx
	xorl	%edi, %edi
	leaq	-56(%rbp), %rdx
	leaq	-76(%rbp), %rsi
	call	X509_ALGOR_get0@PLT
	cmpl	$16, -76(%rbp)
	je	.L152
	movl	$105, %edx
	movl	$108, %esi
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	movl	$62, %r8d
	leaq	.LC15(%rip), %rcx
	movl	$5, %edi
	call	ERR_put_error@PLT
.L153:
	movq	%r13, %rdi
	call	ASN1_INTEGER_free@PLT
	movq	%r14, %rdi
	call	DH_free@PLT
	xorl	%eax, %eax
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L152:
	movq	-56(%rbp), %rax
	leaq	-64(%rbp), %rsi
	xorl	%edi, %edi
	movq	8(%rax), %rdx
	movq	%rdx, -64(%rbp)
	movslq	(%rax), %rdx
	leaq	dhx_asn1_meth(%rip), %rax
	cmpq	%rax, 16(%r12)
	je	.L166
	call	d2i_DHparams@PLT
	movq	%rax, %r14
.L155:
	testq	%r14, %r14
	je	.L167
	movslq	-80(%rbp), %rdx
	movq	%r13, %rsi
	xorl	%edi, %edi
	call	d2i_ASN1_INTEGER@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L168
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	ASN1_INTEGER_to_BN@PLT
	movq	%rax, 32(%r14)
	testq	%rax, %rax
	je	.L169
	movq	%r13, %rdi
	call	ASN1_INTEGER_free@PLT
	movq	16(%r12), %rax
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	(%rax), %esi
	call	EVP_PKEY_assign@PLT
	movl	$1, %eax
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L168:
	movl	$76, %r8d
	movl	$104, %edx
	movl	$108, %esi
	movl	$5, %edi
	leaq	.LC15(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L166:
	call	d2i_DHxparams@PLT
	movq	%rax, %r14
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L167:
	movl	$71, %r8d
	leaq	.LC15(%rip), %rcx
	movl	$104, %edx
	xorl	%r13d, %r13d
	movl	$108, %esi
	movl	$5, %edi
	call	ERR_put_error@PLT
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L169:
	movl	$82, %r8d
	movl	$109, %edx
	movl	$108, %esi
	movl	$5, %edi
	leaq	.LC15(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L153
.L165:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1445:
	.size	dh_pub_decode, .-dh_pub_decode
	.p2align 4
	.type	dh_pub_cmp, @function
dh_pub_cmp:
.LFB1461:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	40(%rsi), %rax
	movq	%rdi, %rbx
	movq	8(%rax), %rsi
	movq	40(%rdi), %rax
	movq	8(%rax), %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	je	.L171
.L173:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	movq	40(%r12), %rax
	movq	16(%rax), %rsi
	movq	40(%rbx), %rax
	movq	16(%rax), %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	jne	.L173
	movq	40(%r12), %rax
	movq	40(%rbx), %rdx
	leaq	dhx_asn1_meth(%rip), %rcx
	cmpq	%rcx, 16(%rbx)
	je	.L176
.L174:
	movq	32(%rdx), %rsi
	movq	32(%rax), %rdi
	call	BN_cmp@PLT
	popq	%rbx
	popq	%r12
	testl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	sete	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_restore_state
	movq	64(%rax), %rsi
	movq	64(%rdx), %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	jne	.L173
	movq	40(%rbx), %rdx
	movq	40(%r12), %rax
	jmp	.L174
	.cfi_endproc
.LFE1461:
	.size	dh_pub_cmp, .-dh_pub_cmp
	.p2align 4
	.type	dh_copy_parameters, @function
dh_copy_parameters:
.LFB1459:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	40(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L224
.L178:
	movq	40(%r12), %r14
	movq	16(%r12), %r13
	movq	8(%r14), %r12
	testq	%r12, %r12
	je	.L180
	movl	$2, %esi
	movq	%r12, %rdi
	call	BN_get_flags@PLT
	testl	%eax, %eax
	je	.L183
	movl	$1, %esi
	movq	%r12, %rdi
	call	BN_get_flags@PLT
	testl	%eax, %eax
	jne	.L183
	.p2align 4,,10
	.p2align 3
.L180:
	movq	8(%rbx), %rdi
	call	BN_clear_free@PLT
	movq	%r12, 8(%rbx)
	movq	16(%r14), %r12
	testq	%r12, %r12
	je	.L184
	movl	$2, %esi
	movq	%r12, %rdi
	call	BN_get_flags@PLT
	testl	%eax, %eax
	je	.L186
	movl	$1, %esi
	movq	%r12, %rdi
	call	BN_get_flags@PLT
	testl	%eax, %eax
	jne	.L186
	.p2align 4,,10
	.p2align 3
.L184:
	movq	16(%rbx), %rdi
	call	BN_clear_free@PLT
	leaq	dhx_asn1_meth(%rip), %rax
	movq	%r12, 16(%rbx)
	cmpq	%rax, %r13
	je	.L225
	movl	24(%r14), %eax
	movl	$1, %r12d
	movl	%eax, 24(%rbx)
.L177:
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L183:
	.cfi_restore_state
	movq	%r12, %rdi
	call	BN_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L180
.L182:
	xorl	%r12d, %r12d
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L186:
	.cfi_restore_state
	movq	%r12, %rdi
	call	BN_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L184
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L224:
	movq	%rdi, %r13
	call	DH_new@PLT
	movq	%rax, 40(%r13)
	movq	%rax, %rbx
	testq	%rax, %rax
	jne	.L178
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L225:
	movq	64(%r14), %r12
	testq	%r12, %r12
	je	.L188
	movl	$2, %esi
	movq	%r12, %rdi
	call	BN_get_flags@PLT
	testl	%eax, %eax
	je	.L190
	movl	$1, %esi
	movq	%r12, %rdi
	call	BN_get_flags@PLT
	testl	%eax, %eax
	jne	.L190
	.p2align 4,,10
	.p2align 3
.L188:
	movq	64(%rbx), %rdi
	call	BN_clear_free@PLT
	movq	%r12, 64(%rbx)
	movq	72(%r14), %r12
	testq	%r12, %r12
	je	.L191
	movl	$2, %esi
	movq	%r12, %rdi
	call	BN_get_flags@PLT
	testl	%eax, %eax
	jne	.L192
.L193:
	movq	%r12, %rdi
	call	BN_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L182
.L191:
	movq	72(%rbx), %rdi
	call	BN_clear_free@PLT
	movq	%r12, 72(%rbx)
	movq	80(%rbx), %rdi
	movl	$407, %edx
	leaq	.LC15(%rip), %rsi
	movl	$1, %r12d
	call	CRYPTO_free@PLT
	movq	$0, 80(%rbx)
	movq	80(%r14), %rdi
	movl	$0, 88(%rbx)
	testq	%rdi, %rdi
	je	.L177
	movslq	88(%r14), %rsi
	movl	$411, %ecx
	leaq	.LC15(%rip), %rdx
	call	CRYPTO_memdup@PLT
	movq	%rax, 80(%rbx)
	testq	%rax, %rax
	je	.L182
	movl	88(%r14), %eax
	movl	%eax, 88(%rbx)
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L190:
	movq	%r12, %rdi
	call	BN_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L188
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L192:
	movl	$1, %esi
	movq	%r12, %rdi
	call	BN_get_flags@PLT
	testl	%eax, %eax
	jne	.L193
	jmp	.L191
	.cfi_endproc
.LFE1459:
	.size	dh_copy_parameters, .-dh_copy_parameters
	.section	.rodata.str1.1
.LC16:
	.string	"DH Public-Key"
	.text
	.p2align 4
	.type	dh_public_print, @function
dh_public_print:
.LFB1463:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	40(%rsi), %rbx
	cmpq	$0, 8(%rbx)
	movq	32(%rbx), %r15
	je	.L246
	testq	%r15, %r15
	je	.L246
	movl	%edx, %r13d
	movl	$128, %edx
	movq	%rdi, %r12
	movl	%r13d, %esi
	call	BIO_indent@PLT
	movq	8(%rbx), %rdi
	call	BN_num_bits@PLT
	leaq	.LC16(%rip), %rdx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, %ecx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L229
	leal	4(%r13), %r14d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	%r14d, %r8d
	leaq	.LC2(%rip), %rsi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	jne	.L275
.L229:
	movl	$7, %edx
.L227:
	movl	$343, %r8d
	movl	$100, %esi
	movl	$5, %edi
	leaq	.LC15(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L226:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L246:
	.cfi_restore_state
	movl	$67, %edx
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L275:
	xorl	%ecx, %ecx
	movl	%r14d, %r8d
	movq	%r15, %rdx
	movq	%r12, %rdi
	leaq	.LC3(%rip), %rsi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L229
	movq	8(%rbx), %rdx
	xorl	%ecx, %ecx
	movl	%r14d, %r8d
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L229
	movq	16(%rbx), %rdx
	xorl	%ecx, %ecx
	movl	%r14d, %r8d
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L229
	movq	64(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L233
	xorl	%ecx, %ecx
	movl	%r14d, %r8d
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L229
.L233:
	movq	72(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L232
	xorl	%ecx, %ecx
	movl	%r14d, %r8d
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L229
.L232:
	cmpq	$0, 80(%rbx)
	je	.L235
	movl	$128, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	call	BIO_indent@PLT
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	leal	8(%r13), %eax
	movl	%eax, -52(%rbp)
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L239:
	leal	1(%r13), %ecx
	cmpl	%ecx, %eax
	movq	80(%rbx), %rax
	leaq	.LC10(%rip), %rcx
	movzbl	(%rax,%r15), %edx
	jne	.L274
	leaq	.LC12(%rip), %rcx
.L274:
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	addq	$1, %r15
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L229
.L238:
	movl	88(%rbx), %eax
	movl	%r15d, %r13d
	cmpl	%r15d, %eax
	jle	.L276
	imull	$-286331153, %r15d, %edx
	cmpl	$286331153, %edx
	ja	.L239
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L229
	movl	-52(%rbp), %esi
	movl	$128, %edx
	movq	%r12, %rdi
	call	BIO_indent@PLT
	testl	%eax, %eax
	je	.L229
	movl	88(%rbx), %eax
	jmp	.L239
.L276:
	movl	$1, %edx
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L226
.L235:
	movq	96(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L237
	xorl	%ecx, %ecx
	movl	%r14d, %r8d
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L229
.L237:
	movl	24(%rbx), %eax
	testl	%eax, %eax
	jne	.L277
.L245:
	movl	$1, %eax
	jmp	.L226
.L277:
	movl	$128, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	BIO_indent@PLT
	movl	24(%rbx), %edx
	xorl	%eax, %eax
	movq	%r12, %rdi
	leaq	.LC14(%rip), %rsi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jg	.L245
	jmp	.L229
	.cfi_endproc
.LFE1463:
	.size	dh_public_print, .-dh_public_print
	.section	.rodata.str1.1
.LC17:
	.string	"DH Parameters"
	.text
	.p2align 4
	.type	dh_param_print, @function
dh_param_print:
.LFB1462:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	40(%rsi), %rbx
	cmpq	$0, 8(%rbx)
	je	.L298
	movl	%edx, %r13d
	movl	$128, %edx
	movq	%rdi, %r12
	movl	%r13d, %esi
	call	BIO_indent@PLT
	movq	8(%rbx), %rdi
	call	BN_num_bits@PLT
	leaq	.LC17(%rip), %rdx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, %ecx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L281
	leal	4(%r13), %r14d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	%r14d, %r8d
	leaq	.LC2(%rip), %rsi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	jne	.L327
.L281:
	movl	$7, %edx
.L279:
	movl	$343, %r8d
	movl	$100, %esi
	movl	$5, %edi
	leaq	.LC15(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L278:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L327:
	.cfi_restore_state
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%r14d, %r8d
	movq	%r12, %rdi
	leaq	.LC3(%rip), %rsi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L281
	movq	8(%rbx), %rdx
	xorl	%ecx, %ecx
	movl	%r14d, %r8d
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L281
	movq	16(%rbx), %rdx
	xorl	%ecx, %ecx
	movl	%r14d, %r8d
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L281
	movq	64(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L285
	xorl	%ecx, %ecx
	movl	%r14d, %r8d
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L281
.L285:
	movq	72(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L284
	xorl	%ecx, %ecx
	movl	%r14d, %r8d
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L281
.L284:
	cmpq	$0, 80(%rbx)
	je	.L287
	movl	$128, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	call	BIO_indent@PLT
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	leal	8(%r13), %eax
	movl	%eax, -52(%rbp)
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L291:
	leal	1(%r13), %ecx
	cmpl	%ecx, %eax
	movq	80(%rbx), %rax
	leaq	.LC10(%rip), %rcx
	movzbl	(%rax,%r15), %edx
	jne	.L326
	leaq	.LC12(%rip), %rcx
.L326:
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	addq	$1, %r15
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L281
.L290:
	movl	88(%rbx), %eax
	movl	%r15d, %r13d
	cmpl	%r15d, %eax
	jle	.L328
	imull	$-286331153, %r15d, %edx
	cmpl	$286331153, %edx
	ja	.L291
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L281
	movl	-52(%rbp), %esi
	movl	$128, %edx
	movq	%r12, %rdi
	call	BIO_indent@PLT
	testl	%eax, %eax
	je	.L281
	movl	88(%rbx), %eax
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L298:
	movl	$67, %edx
	jmp	.L279
.L328:
	movl	$1, %edx
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L278
.L287:
	movq	96(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L289
	xorl	%ecx, %ecx
	movl	%r14d, %r8d
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L281
.L289:
	movl	24(%rbx), %eax
	testl	%eax, %eax
	jne	.L329
.L297:
	movl	$1, %eax
	jmp	.L278
.L329:
	movl	$128, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	BIO_indent@PLT
	movl	24(%rbx), %edx
	xorl	%eax, %eax
	movq	%r12, %rdi
	leaq	.LC14(%rip), %rsi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jg	.L297
	jmp	.L281
	.cfi_endproc
.LFE1462:
	.size	dh_param_print, .-dh_param_print
	.p2align 4
	.globl	DHparams_dup
	.type	DHparams_dup, @function
DHparams_dup:
.LFB1458:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	call	DH_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L330
	movq	8(%rbx), %r13
	movq	64(%rbx), %r14
	testq	%r13, %r13
	je	.L332
	movl	$2, %esi
	movq	%r13, %rdi
	call	BN_get_flags@PLT
	testl	%eax, %eax
	je	.L335
	movl	$1, %esi
	movq	%r13, %rdi
	call	BN_get_flags@PLT
	testl	%eax, %eax
	jne	.L335
	.p2align 4,,10
	.p2align 3
.L332:
	movq	8(%r12), %rdi
	call	BN_clear_free@PLT
	movq	%r13, 8(%r12)
	movq	16(%rbx), %r13
	testq	%r13, %r13
	je	.L336
	movl	$2, %esi
	movq	%r13, %rdi
	call	BN_get_flags@PLT
	testl	%eax, %eax
	je	.L338
	movl	$1, %esi
	movq	%r13, %rdi
	call	BN_get_flags@PLT
	testl	%eax, %eax
	jne	.L338
	.p2align 4,,10
	.p2align 3
.L336:
	movq	16(%r12), %rdi
	call	BN_clear_free@PLT
	movq	%r13, 16(%r12)
	testq	%r14, %r14
	jne	.L380
	movl	24(%rbx), %eax
	movl	%eax, 24(%r12)
.L330:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L335:
	.cfi_restore_state
	movq	%r13, %rdi
	call	BN_dup@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L332
.L334:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	DH_free@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L338:
	.cfi_restore_state
	movq	%r13, %rdi
	call	BN_dup@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L336
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L380:
	movq	64(%rbx), %r13
	testq	%r13, %r13
	je	.L340
	movl	$2, %esi
	movq	%r13, %rdi
	call	BN_get_flags@PLT
	testl	%eax, %eax
	je	.L342
	movl	$1, %esi
	movq	%r13, %rdi
	call	BN_get_flags@PLT
	testl	%eax, %eax
	jne	.L342
	.p2align 4,,10
	.p2align 3
.L340:
	movq	64(%r12), %rdi
	call	BN_clear_free@PLT
	movq	%r13, 64(%r12)
	movq	72(%rbx), %r13
	testq	%r13, %r13
	je	.L343
	movl	$2, %esi
	movq	%r13, %rdi
	call	BN_get_flags@PLT
	testl	%eax, %eax
	jne	.L344
.L345:
	movq	%r13, %rdi
	call	BN_dup@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L334
.L343:
	movq	72(%r12), %rdi
	call	BN_clear_free@PLT
	movq	%r13, 72(%r12)
	movq	80(%r12), %rdi
	movl	$407, %edx
	leaq	.LC15(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 80(%r12)
	movq	80(%rbx), %rdi
	movl	$0, 88(%r12)
	testq	%rdi, %rdi
	je	.L330
	movslq	88(%rbx), %rsi
	movl	$411, %ecx
	leaq	.LC15(%rip), %rdx
	call	CRYPTO_memdup@PLT
	movq	%rax, 80(%r12)
	testq	%rax, %rax
	je	.L334
	movl	88(%rbx), %eax
	movl	%eax, 88(%r12)
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L342:
	movq	%r13, %rdi
	call	BN_dup@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L340
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L344:
	movl	$1, %esi
	movq	%r13, %rdi
	call	BN_get_flags@PLT
	testl	%eax, %eax
	jne	.L345
	jmp	.L343
	.cfi_endproc
.LFE1458:
	.size	DHparams_dup, .-DHparams_dup
	.p2align 4
	.type	dh_pkey_ctrl, @function
dh_pkey_ctrl:
.LFB1466:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$7, %esi
	je	.L382
	cmpl	$8, %esi
	jne	.L422
	movl	$1, (%rcx)
	movl	$1, %r14d
.L381:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L477
	addq	$104, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L422:
	.cfi_restore_state
	movl	$-2, %r14d
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L382:
	cmpq	$1, %rdx
	je	.L478
	testq	%rdx, %rdx
	jne	.L422
	movq	%rcx, %rdi
	movq	$0, -72(%rbp)
	xorl	%r14d, %r14d
	call	CMS_RecipientInfo_get0_pkey_ctx@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L381
	movq	%rax, %rdi
	leaq	-104(%rbp), %r14
	call	EVP_PKEY_CTX_get0_pkey@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	-88(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	CMS_RecipientInfo_kari_get0_orig_id@PLT
	testl	%eax, %eax
	je	.L402
	movq	-104(%rbp), %rcx
	leaq	-96(%rbp), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	X509_ALGOR_get0@PLT
	xorl	%edi, %edi
	call	OBJ_nid2obj@PLT
	cmpq	%rax, -96(%rbp)
	je	.L479
.L401:
	xorl	%r9d, %r9d
	movl	$4102, %ecx
	movl	$1024, %edx
	movq	%r13, %rdi
	movl	$-2, %r8d
	movl	$920, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	jle	.L402
	xorl	%r8d, %r8d
	leaq	-64(%rbp), %r9
	movl	$4104, %ecx
	movq	%r13, %rdi
	movl	$1024, %edx
	movl	$920, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	je	.L402
	cmpl	$1, %ebx
	je	.L480
	cmpl	$2, %ebx
	jne	.L402
.L406:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L481
	call	EVP_MD_type@PLT
	cmpl	$64, %eax
	jne	.L402
.L408:
	leaq	-80(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	CMS_RecipientInfo_kari_get0_alg@PLT
	testl	%eax, %eax
	je	.L402
	movq	%r12, %rdi
	call	CMS_RecipientInfo_kari_get0_ctx@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_type@PLT
	movl	%eax, %edi
	movl	%eax, %r12d
	call	OBJ_nid2obj@PLT
	xorl	%r8d, %r8d
	movl	$4109, %ecx
	movq	%r13, %rdi
	movq	%rax, %r9
	movl	$1024, %edx
	movl	$920, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L402
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_key_length@PLT
	movl	%eax, -120(%rbp)
	call	X509_ALGOR_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L402
	movl	%r12d, %edi
	xorl	%r14d, %r14d
	call	OBJ_nid2obj@PLT
	movq	%rax, (%r15)
	call	ASN1_TYPE_new@PLT
	movq	%rax, 8(%r15)
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L400
	movq	%rax, %rsi
	movq	%rbx, %rdi
	xorl	%r12d, %r12d
	call	EVP_CIPHER_param_to_asn1@PLT
	testl	%eax, %eax
	jle	.L400
	movq	8(%r15), %rdi
	call	ASN1_TYPE_get@PLT
	testl	%eax, %eax
	je	.L482
.L409:
	movl	-120(%rbp), %r8d
	xorl	%r9d, %r9d
	movl	$4105, %ecx
	movq	%r13, %rdi
	movl	$1024, %edx
	movl	$920, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L430
	movq	-80(%rbp), %r12
	testq	%r12, %r12
	je	.L427
	movq	%r12, %rdi
	call	ASN1_STRING_length@PLT
	movq	-80(%rbp), %rdi
	movslq	%eax, %rbx
	call	ASN1_STRING_get0_data@PLT
	movl	$874, %ecx
	leaq	.LC15(%rip), %rdx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	CRYPTO_memdup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L483
.L410:
	movq	%r12, %r9
	movl	%ebx, %r8d
	movl	$4107, %ecx
	movl	$1024, %edx
	movl	$920, %esi
	movq	%r13, %rdi
	xorl	%r14d, %r14d
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L400
	leaq	-72(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -72(%rbp)
	call	i2d_X509_ALGOR@PLT
	cmpq	$0, -72(%rbp)
	movl	%eax, %r13d
	je	.L430
	testl	%eax, %eax
	je	.L430
	call	ASN1_STRING_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L400
	movq	-72(%rbp), %rsi
	movl	%r13d, %edx
	movq	%rax, %rdi
	movl	$1, %r14d
	call	ASN1_STRING_set0@PLT
	movl	$245, %edi
	movq	$0, -72(%rbp)
	call	OBJ_nid2obj@PLT
	movq	-104(%rbp), %rdi
	movq	%r12, %rcx
	movl	$16, %edx
	movq	%rax, %rsi
	xorl	%r12d, %r12d
	call	X509_ALGOR_set0@PLT
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L480:
	xorl	%r9d, %r9d
	movl	$4102, %ecx
	movl	$1024, %edx
	movq	%r13, %rdi
	movl	$2, %r8d
	movl	$920, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jg	.L406
	.p2align 4,,10
	.p2align 3
.L402:
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
.L400:
	movq	-72(%rbp), %rdi
	movl	$902, %edx
	leaq	.LC15(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r15, %rdi
	call	X509_ALGOR_free@PLT
	movl	$904, %edx
	leaq	.LC15(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L478:
	movq	%rcx, %rdi
	xorl	%r14d, %r14d
	call	CMS_RecipientInfo_get0_pkey_ctx@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L381
	movq	%rax, %rdi
	leaq	-72(%rbp), %r15
	leaq	-80(%rbp), %rbx
	call	EVP_PKEY_CTX_get0_peerkey@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L484
.L386:
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	CMS_RecipientInfo_kari_get0_alg@PLT
	testl	%eax, %eax
	je	.L393
	movq	-80(%rbp), %rax
	movq	(%rax), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$245, %eax
	je	.L394
	movl	$112, %edx
	movl	$116, %esi
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	movl	$690, %r8d
	leaq	.LC15(%rip), %rcx
	movl	$5, %edi
	call	ERR_put_error@PLT
.L395:
	movq	%r14, %rdi
	call	X509_ALGOR_free@PLT
	movl	$742, %edx
	leaq	.LC15(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	movl	$768, %r8d
	movl	$113, %edx
	leaq	.LC15(%rip), %rcx
	movl	$114, %esi
	movl	$5, %edi
	call	ERR_put_error@PLT
.L476:
	xorl	%r14d, %r14d
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L479:
	movq	40(%rbx), %rax
	xorl	%esi, %esi
	movq	32(%rax), %rdi
	call	BN_to_ASN1_INTEGER@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L402
	movq	%rax, %rdi
	leaq	-72(%rbp), %rsi
	call	i2d_ASN1_INTEGER@PLT
	movq	%r15, %rdi
	movl	%eax, %ebx
	call	ASN1_INTEGER_free@PLT
	testl	%ebx, %ebx
	jle	.L402
	movq	-72(%rbp), %rsi
	movq	-88(%rbp), %rdi
	movl	%ebx, %edx
	call	ASN1_STRING_set0@PLT
	movq	-88(%rbp), %rdx
	movl	$920, %edi
	movq	16(%rdx), %rax
	andq	$-16, %rax
	orq	$8, %rax
	movq	%rax, 16(%rdx)
	movq	$0, -72(%rbp)
	call	OBJ_nid2obj@PLT
	movq	-104(%rbp), %rdi
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%rax, %rsi
	call	X509_ALGOR_set0@PLT
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L393:
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	jmp	.L395
.L484:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	-88(%rbp), %rdx
	leaq	-96(%rbp), %rsi
	call	CMS_RecipientInfo_kari_get0_orig_id@PLT
	testl	%eax, %eax
	je	.L476
	movq	-96(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L476
	movq	-88(%rbp), %rax
	movq	%rax, -128(%rbp)
	testq	%rax, %rax
	je	.L476
	movq	%rbx, %rdi
	leaq	-104(%rbp), %rsi
	movq	%r15, %rdx
	call	X509_ALGOR_get0@PLT
	movq	-80(%rbp), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$920, %eax
	jne	.L388
	cmpl	$5, -104(%rbp)
	je	.L388
	movq	%r13, %rdi
	call	EVP_PKEY_CTX_get0_pkey@PLT
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L388
	cmpl	$920, (%rax)
	je	.L485
.L388:
	movq	$0, -120(%rbp)
	xorl	%r9d, %r9d
.L389:
	movq	%r14, %rdi
	movq	%r9, -128(%rbp)
	call	ASN1_INTEGER_free@PLT
	movq	-128(%rbp), %r9
	movq	%r9, %rdi
	call	EVP_PKEY_free@PLT
	movq	-120(%rbp), %rdi
	call	DH_free@PLT
	movl	$762, %r8d
	movl	$111, %edx
	leaq	.LC15(%rip), %rcx
	movl	$114, %esi
	movl	$5, %edi
	call	ERR_put_error@PLT
	jmp	.L476
.L394:
	xorl	%r9d, %r9d
	movl	$4102, %ecx
	movl	$1024, %edx
	movq	%r13, %rdi
	movl	$2, %r8d
	movl	$920, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L393
	call	EVP_sha1@PLT
	xorl	%r8d, %r8d
	movl	$4103, %ecx
	movq	%r13, %rdi
	movq	%rax, %r9
	movl	$1024, %edx
	movl	$920, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L393
	movq	-80(%rbp), %rax
	movq	8(%rax), %rax
	cmpl	$16, (%rax)
	jne	.L393
	movq	8(%rax), %rax
	leaq	-64(%rbp), %rsi
	xorl	%edi, %edi
	movq	8(%rax), %rdx
	movq	%rdx, -64(%rbp)
	movslq	(%rax), %rdx
	call	d2i_X509_ALGOR@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L393
	movq	%r12, %rdi
	call	CMS_RecipientInfo_kari_get0_ctx@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L420
	movq	(%r14), %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_cipherbyname@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L395
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpq	$65538, %rax
	je	.L486
.L420:
	xorl	%r12d, %r12d
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L481:
	call	EVP_sha1@PLT
	xorl	%r8d, %r8d
	movl	$4103, %ecx
	movq	%r13, %rdi
	movq	%rax, %r9
	movl	$1024, %edx
	movl	$920, %esi
	movq	%rax, -64(%rbp)
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jg	.L408
	jmp	.L402
.L477:
	call	__stack_chk_fail@PLT
.L430:
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	jmp	.L400
.L485:
	movq	40(%rax), %rdi
	call	DHparams_dup
	movq	-128(%rbp), %rdi
	movq	%rax, -120(%rbp)
	call	ASN1_STRING_length@PLT
	movq	-128(%rbp), %rdi
	movl	%eax, -140(%rbp)
	call	ASN1_STRING_get0_data@PLT
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L413
	movslq	-140(%rbp), %rdx
	testl	%edx, %edx
	je	.L413
	leaq	-64(%rbp), %rsi
	xorl	%edi, %edi
	call	d2i_ASN1_INTEGER@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L487
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	ASN1_INTEGER_to_BN@PLT
	movq	%rax, %r9
	movq	-120(%rbp), %rax
	movq	%r9, 32(%rax)
	testq	%r9, %r9
	je	.L488
	call	EVP_PKEY_new@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L389
	movq	-136(%rbp), %rax
	movq	-120(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r9, -120(%rbp)
	movq	16(%rax), %rax
	movl	(%rax), %esi
	call	EVP_PKEY_assign@PLT
	movq	-120(%rbp), %r9
	movq	%r13, %rdi
	movq	%r9, %rsi
	call	EVP_PKEY_derive_set_peer@PLT
	movq	-120(%rbp), %r9
	testl	%eax, %eax
	jle	.L489
	movq	%r14, %rdi
	movq	%r9, -120(%rbp)
	call	ASN1_INTEGER_free@PLT
	movq	-120(%rbp), %r9
	movq	%r9, %rdi
	call	EVP_PKEY_free@PLT
	xorl	%edi, %edi
	call	DH_free@PLT
	jmp	.L386
.L482:
	movq	8(%r15), %rdi
	call	ASN1_TYPE_free@PLT
	movq	$0, 8(%r15)
	jmp	.L409
.L427:
	xorl	%ebx, %ebx
	jmp	.L410
.L486:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	EVP_EncryptInit_ex@PLT
	testl	%eax, %eax
	je	.L420
	movq	8(%r14), %rsi
	movq	%r15, %rdi
	call	EVP_CIPHER_asn1_to_param@PLT
	testl	%eax, %eax
	jle	.L420
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_key_length@PLT
	xorl	%r9d, %r9d
	movl	$4105, %ecx
	movq	%r13, %rdi
	movl	%eax, %r8d
	movl	$1024, %edx
	movl	$920, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L420
	movq	%r12, %rdi
	call	EVP_CIPHER_type@PLT
	movl	%eax, %edi
	call	OBJ_nid2obj@PLT
	xorl	%r8d, %r8d
	movl	$4109, %ecx
	movq	%r13, %rdi
	movq	%rax, %r9
	movl	$1024, %edx
	movl	$920, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L420
	movq	-72(%rbp), %r12
	testq	%r12, %r12
	je	.L421
	movq	%r12, %rdi
	call	ASN1_STRING_length@PLT
	movq	-72(%rbp), %rdi
	movslq	%eax, %rbx
	call	ASN1_STRING_get0_data@PLT
	movl	$730, %ecx
	leaq	.LC15(%rip), %rdx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	CRYPTO_memdup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L395
.L396:
	movq	%r12, %r9
	movl	%ebx, %r8d
	movl	$4107, %ecx
	movl	$1024, %edx
	movl	$920, %esi
	movq	%r13, %rdi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L395
	movq	%r14, %rdi
	movl	$1, %r14d
	call	X509_ALGOR_free@PLT
	movl	$742, %edx
	leaq	.LC15(%rip), %rsi
	xorl	%edi, %edi
	call	CRYPTO_free@PLT
	jmp	.L381
.L483:
	xorl	%r14d, %r14d
	jmp	.L400
.L413:
	xorl	%r9d, %r9d
	jmp	.L389
.L421:
	xorl	%ebx, %ebx
	jmp	.L396
.L487:
	movl	$645, %r8d
	movl	$104, %edx
	movl	$115, %esi
	movl	$5, %edi
	leaq	.LC15(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r9d, %r9d
	jmp	.L389
.L489:
	movq	$0, -120(%rbp)
	jmp	.L389
.L488:
	movl	$651, %r8d
	leaq	.LC15(%rip), %rcx
	movl	$109, %edx
	movl	$115, %esi
	movl	$5, %edi
	movq	%r9, -128(%rbp)
	call	ERR_put_error@PLT
	movq	-128(%rbp), %r9
	jmp	.L389
	.cfi_endproc
.LFE1466:
	.size	dh_pkey_ctrl, .-dh_pkey_ctrl
	.p2align 4
	.globl	DHparams_print
	.type	DHparams_print, @function
DHparams_print:
.LFB1465:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	$0, 8(%rsi)
	je	.L510
	movl	$128, %edx
	movq	%rsi, %rbx
	movl	$4, %esi
	movq	%rdi, %r12
	call	BIO_indent@PLT
	movq	8(%rbx), %rdi
	call	BN_num_bits@PLT
	leaq	.LC17(%rip), %rdx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, %ecx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L493
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$8, %r8d
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	jne	.L539
.L493:
	movl	$7, %edx
.L491:
	movl	$343, %r8d
	movl	$100, %esi
	movl	$5, %edi
	leaq	.LC15(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L490:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L539:
	.cfi_restore_state
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$8, %r8d
	movq	%r12, %rdi
	leaq	.LC3(%rip), %rsi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L493
	movq	8(%rbx), %rdx
	xorl	%ecx, %ecx
	movl	$8, %r8d
	movq	%r12, %rdi
	leaq	.LC4(%rip), %rsi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L493
	movq	16(%rbx), %rdx
	xorl	%ecx, %ecx
	movl	$8, %r8d
	movq	%r12, %rdi
	leaq	.LC5(%rip), %rsi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L493
	movq	64(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L497
	xorl	%ecx, %ecx
	movl	$8, %r8d
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L493
.L497:
	movq	72(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L496
	xorl	%ecx, %ecx
	movl	$8, %r8d
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L493
.L496:
	cmpq	$0, 80(%rbx)
	je	.L499
	movl	$128, %edx
	movl	$8, %esi
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	BIO_indent@PLT
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	leaq	.LC9(%rip), %r15
	call	BIO_puts@PLT
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L503:
	addl	$1, %r14d
	leaq	.LC10(%rip), %rcx
	cmpl	%r14d, %eax
	movq	80(%rbx), %rax
	movzbl	(%rax,%r13), %edx
	jne	.L538
	leaq	.LC12(%rip), %rcx
.L538:
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	addq	$1, %r13
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L493
.L502:
	movl	88(%rbx), %eax
	movl	%r13d, %r14d
	cmpl	%r13d, %eax
	jle	.L540
	imull	$-286331153, %r13d, %edx
	cmpl	$286331153, %edx
	ja	.L503
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L493
	movl	$128, %edx
	movl	$12, %esi
	movq	%r12, %rdi
	call	BIO_indent@PLT
	testl	%eax, %eax
	je	.L493
	movl	88(%rbx), %eax
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L510:
	movl	$67, %edx
	jmp	.L491
.L540:
	movl	$1, %edx
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L490
.L499:
	movq	96(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L501
	xorl	%ecx, %ecx
	movl	$8, %r8d
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L493
.L501:
	movl	24(%rbx), %eax
	testl	%eax, %eax
	jne	.L541
.L509:
	movl	$1, %eax
	jmp	.L490
.L541:
	movl	$128, %edx
	movl	$8, %esi
	movq	%r12, %rdi
	call	BIO_indent@PLT
	movl	24(%rbx), %edx
	xorl	%eax, %eax
	movq	%r12, %rdi
	leaq	.LC14(%rip), %rsi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jg	.L509
	jmp	.L493
	.cfi_endproc
.LFE1465:
	.size	DHparams_print, .-DHparams_print
	.globl	dhx_asn1_meth
	.section	.rodata.str1.1
.LC18:
	.string	"X9.42 DH"
.LC19:
	.string	"OpenSSL X9.42 DH method"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	dhx_asn1_meth, @object
	.size	dhx_asn1_meth, 280
dhx_asn1_meth:
	.long	920
	.long	920
	.quad	0
	.quad	.LC18
	.quad	.LC19
	.quad	dh_pub_decode
	.quad	dh_pub_encode
	.quad	dh_pub_cmp
	.quad	dh_public_print
	.quad	dh_priv_decode
	.quad	dh_priv_encode
	.quad	dh_private_print
	.quad	int_dh_size
	.quad	dh_bits
	.quad	dh_security_bits
	.quad	dh_param_decode
	.quad	dh_param_encode
	.quad	dh_missing_parameters
	.quad	dh_copy_parameters
	.quad	dh_cmp_parameters
	.quad	dh_param_print
	.quad	0
	.quad	int_dh_free
	.quad	dh_pkey_ctrl
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	dh_pkey_public_check
	.quad	dh_pkey_param_check
	.zero	32
	.globl	dh_asn1_meth
	.section	.rodata.str1.1
.LC20:
	.string	"DH"
.LC21:
	.string	"OpenSSL PKCS#3 DH method"
	.section	.data.rel.ro.local
	.align 32
	.type	dh_asn1_meth, @object
	.size	dh_asn1_meth, 280
dh_asn1_meth:
	.long	28
	.long	28
	.quad	0
	.quad	.LC20
	.quad	.LC21
	.quad	dh_pub_decode
	.quad	dh_pub_encode
	.quad	dh_pub_cmp
	.quad	dh_public_print
	.quad	dh_priv_decode
	.quad	dh_priv_encode
	.quad	dh_private_print
	.quad	int_dh_size
	.quad	dh_bits
	.quad	dh_security_bits
	.quad	dh_param_decode
	.quad	dh_param_encode
	.quad	dh_missing_parameters
	.quad	dh_copy_parameters
	.quad	dh_cmp_parameters
	.quad	dh_param_print
	.quad	0
	.quad	int_dh_free
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	dh_pkey_public_check
	.quad	dh_pkey_param_check
	.zero	32
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
