	.file	"by_dir.c"
	.text
	.p2align 4
	.type	by_dir_hash_cmp, @function
by_dir_hash_cmp:
.LFB874:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rdx
	movq	(%rdi), %rcx
	movl	$1, %eax
	movq	(%rdx), %rdx
	cmpq	%rdx, (%rcx)
	ja	.L1
	sbbl	%eax, %eax
.L1:
	ret
	.cfi_endproc
.LFE874:
	.size	by_dir_hash_cmp, .-by_dir_hash_cmp
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/x509/by_dir.c"
	.text
	.p2align 4
	.type	by_dir_hash_free, @function
by_dir_hash_free:
.LFB873:
	.cfi_startproc
	endbr64
	movl	$123, %edx
	leaq	.LC0(%rip), %rsi
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE873:
	.size	by_dir_hash_free, .-by_dir_hash_free
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	""
.LC2:
	.string	"r"
.LC3:
	.string	"%s%c%08lx.%s%d"
	.text
	.p2align 4
	.type	get_cert_by_subject, @function
get_cert_by_subject:
.LFB878:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$600, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -608(%rbp)
	movl	%esi, -616(%rbp)
	movq	%rcx, -640(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L36
	movl	%esi, -592(%rbp)
	movq	%rdx, %r13
	cmpl	$1, %esi
	je	.L45
	cmpl	$2, -616(%rbp)
	jne	.L10
	leaq	-416(%rbp), %rax
	movq	%rdx, -392(%rbp)
	movq	%rax, -584(%rbp)
	leaq	.LC2(%rip), %rax
	movq	%rax, -600(%rbp)
	call	BUF_MEM_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L46
.L12:
	movq	-608(%rbp), %rax
	movq	%r13, %rdi
	movq	16(%rax), %rax
	movq	%rax, -624(%rbp)
	call	X509_NAME_hash@PLT
	movl	$0, -612(%rbp)
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L13:
	movq	-624(%rbp), %rax
	movq	8(%rax), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, -612(%rbp)
	jge	.L47
	movq	-624(%rbp), %rax
	movl	-612(%rbp), %esi
	movq	8(%rax), %rdi
	call	OPENSSL_sk_value@PLT
	movq	(%rax), %rdi
	movq	%rax, %rbx
	call	strlen@PLT
	movq	%r12, %rdi
	leal	17(%rax), %esi
	movslq	%esi, %rsi
	call	BUF_MEM_grow@PLT
	testq	%rax, %rax
	je	.L48
	cmpl	$2, -616(%rbp)
	je	.L49
	xorl	%r14d, %r14d
	cmpl	$1, -616(%rbp)
	movq	8(%r12), %rdi
	leaq	-560(%rbp), %r13
	je	.L22
	movq	%r13, %rax
	movq	%rbx, %r13
	movq	%r15, %rbx
	movq	%rax, %r15
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L50:
	movq	8(%r12), %rdi
	addl	$1, %r14d
.L20:
	movq	16(%r12), %rsi
	pushq	%r14
	movq	%rbx, %r9
	movl	$47, %r8d
	pushq	-600(%rbp)
	movq	0(%r13), %rcx
	leaq	.LC3(%rip), %rdx
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	movq	8(%r12), %rsi
	movq	%r15, %rdx
	movl	$1, %edi
	call	__xstat@PLT
	popq	%r8
	popq	%r9
	testl	%eax, %eax
	jns	.L50
	movq	%rbx, %r15
.L19:
	movq	-608(%rbp), %rbx
	movq	24(%rbx), %rdi
	call	X509_STORE_lock@PLT
	movq	24(%rbx), %rax
	leaq	-592(%rbp), %rsi
	movq	8(%rax), %rdi
	call	OPENSSL_sk_find@PLT
	movl	%eax, %esi
	movq	24(%rbx), %rax
	movq	8(%rax), %rdi
	call	OPENSSL_sk_value@PLT
	movq	24(%rbx), %rdi
	movq	%rax, %r13
	call	X509_STORE_unlock@PLT
	testq	%r13, %r13
	jne	.L51
.L31:
	addl	$1, -612(%rbp)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L52:
	movl	8(%rbx), %edx
	movq	8(%r12), %rsi
	movq	-608(%rbp), %rdi
	call	X509_load_cert_file@PLT
	testl	%eax, %eax
	je	.L19
	movq	8(%r12), %rdi
	addl	$1, %r14d
.L22:
	movq	16(%r12), %rsi
	pushq	%r14
	movq	%r15, %r9
	movl	$47, %r8d
	pushq	-600(%rbp)
	movq	(%rbx), %rcx
	leaq	.LC3(%rip), %rdx
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	movq	8(%r12), %rsi
	movl	$1, %edi
	movq	%r13, %rdx
	call	__xstat@PLT
	popq	%rsi
	popq	%rdi
	testl	%eax, %eax
	jns	.L52
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L49:
	movq	16(%rbx), %rax
	xorl	%r14d, %r14d
	movq	%rax, -632(%rbp)
	testq	%rax, %rax
	je	.L17
	movq	-624(%rbp), %rax
	movq	%r15, -576(%rbp)
	movq	16(%rax), %rdi
	call	CRYPTO_THREAD_read_lock@PLT
	movq	16(%rbx), %rdi
	leaq	-576(%rbp), %rsi
	call	OPENSSL_sk_find@PLT
	movq	$0, -632(%rbp)
	testl	%eax, %eax
	js	.L18
	movq	16(%rbx), %rdi
	movl	%eax, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, -632(%rbp)
	movl	8(%rax), %r14d
.L18:
	movq	-624(%rbp), %rax
	movq	16(%rax), %rdi
	call	CRYPTO_THREAD_unlock@PLT
.L17:
	movq	8(%r12), %rdi
	leaq	-560(%rbp), %r13
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L53:
	movl	8(%rbx), %edx
	movq	8(%r12), %rsi
	movq	-608(%rbp), %rdi
	call	X509_load_crl_file@PLT
	testl	%eax, %eax
	je	.L23
	movq	8(%r12), %rdi
	addl	$1, %r14d
.L25:
	movq	16(%r12), %rsi
	pushq	%r14
	movq	%r15, %r9
	movl	$47, %r8d
	pushq	-600(%rbp)
	movq	(%rbx), %rcx
	leaq	.LC3(%rip), %rdx
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	movq	8(%r12), %rsi
	movq	%r13, %rdx
	movl	$1, %edi
	call	__xstat@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	jns	.L53
.L23:
	movq	-608(%rbp), %r13
	movq	24(%r13), %rdi
	call	X509_STORE_lock@PLT
	movq	24(%r13), %rax
	leaq	-592(%rbp), %rsi
	movq	8(%rax), %rdi
	call	OPENSSL_sk_find@PLT
	movq	%r13, -608(%rbp)
	movl	%eax, %esi
	movq	24(%r13), %rax
	movq	8(%rax), %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r13
	movq	-608(%rbp), %rax
	movq	24(%rax), %rdi
	call	X509_STORE_unlock@PLT
	movq	-624(%rbp), %rax
	movq	16(%rax), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	cmpq	$0, -632(%rbp)
	je	.L54
.L26:
	movq	-632(%rbp), %rax
	cmpl	%r14d, 8(%rax)
	jge	.L29
	movl	%r14d, 8(%rax)
.L29:
	movq	-624(%rbp), %rax
	movq	16(%rax), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	testq	%r13, %r13
	je	.L31
.L51:
	movl	0(%r13), %eax
	movq	-640(%rbp), %rcx
	movl	$1, %r14d
	movl	%eax, (%rcx)
	movq	8(%r13), %rax
	movq	%rax, 8(%rcx)
	call	ERR_clear_error@PLT
	jmp	.L11
.L45:
	leaq	-416(%rbp), %rax
	movq	%rdx, -344(%rbp)
	movq	%rax, -584(%rbp)
	leaq	.LC1(%rip), %rax
	movq	%rax, -600(%rbp)
	call	BUF_MEM_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L12
.L46:
	movl	$244, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$7, %edx
	xorl	%r14d, %r14d
	movl	$103, %esi
	movl	$11, %edi
	call	ERR_put_error@PLT
	jmp	.L11
.L54:
	movq	16(%rbx), %rdi
	leaq	-576(%rbp), %rsi
	movq	%r15, -576(%rbp)
	call	OPENSSL_sk_find@PLT
	movq	16(%rbx), %rdi
	movl	%eax, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, -632(%rbp)
	testq	%rax, %rax
	jne	.L26
	movl	$349, %edx
	leaq	.LC0(%rip), %rsi
	movl	$16, %edi
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L55
	movl	%r14d, 8(%rax)
	movq	16(%rbx), %rdi
	movq	%rax, %rsi
	movq	%r15, (%rax)
	movq	%rax, -632(%rbp)
	call	OPENSSL_sk_push@PLT
	movq	-632(%rbp), %r8
	testl	%eax, %eax
	movl	%eax, %r14d
	jne	.L29
	movq	-624(%rbp), %rax
	movq	%r8, -600(%rbp)
	movq	16(%rax), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movq	-600(%rbp), %r8
	movl	$360, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r8, %rdi
	call	CRYPTO_free@PLT
	movl	$361, %r8d
	movl	$65, %edx
	leaq	.LC0(%rip), %rcx
	movl	$103, %esi
	movl	$11, %edi
	call	ERR_put_error@PLT
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L10:
	movl	$112, %edx
	movl	$103, %esi
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	movl	$239, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$11, %edi
	call	ERR_put_error@PLT
.L11:
	movq	%r12, %rdi
	call	BUF_MEM_free@PLT
.L6:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L56
	leaq	-40(%rbp), %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L36:
	.cfi_restore_state
	xorl	%r14d, %r14d
	jmp	.L6
.L48:
	movl	$259, %r8d
.L44:
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$103, %esi
	xorl	%r14d, %r14d
	movl	$11, %edi
	call	ERR_put_error@PLT
	jmp	.L11
.L47:
	xorl	%r14d, %r14d
	jmp	.L11
.L56:
	call	__stack_chk_fail@PLT
	.p2align 4,,10
	.p2align 3
.L55:
	movq	-624(%rbp), %rax
	movq	16(%rax), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movl	$352, %r8d
	jmp	.L44
	.cfi_endproc
.LFE878:
	.size	get_cert_by_subject, .-get_cert_by_subject
	.p2align 4
	.type	by_dir_entry_free, @function
by_dir_entry_free:
.LFB875:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$138, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	CRYPTO_free@PLT
	movq	16(%r12), %rdi
	leaq	by_dir_hash_free(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$140, %edx
	popq	%r12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE875:
	.size	by_dir_entry_free, .-by_dir_entry_free
	.p2align 4
	.type	free_dir, @function
free_dir:
.LFB876:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	by_dir_entry_free(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	16(%rdi), %r12
	movq	8(%r12), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	(%r12), %rdi
	call	BUF_MEM_free@PLT
	movq	16(%r12), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$150, %edx
	popq	%r12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE876:
	.size	free_dir, .-free_dir
	.p2align 4
	.type	new_dir, @function
new_dir:
.LFB872:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$95, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$24, %edi
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L69
	movq	%rax, %r12
	call	BUF_MEM_new@PLT
	movl	$103, %r8d
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L68
	movq	$0, 8(%r12)
	call	CRYPTO_THREAD_lock_new@PLT
	movq	%rax, 16(%r12)
	testq	%rax, %rax
	je	.L70
	movq	%r12, 16(%rbx)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	movq	(%r12), %rdi
	call	BUF_MEM_free@PLT
	movl	$110, %r8d
.L68:
.L65:
	endbr64
	movl	$65, %edx
	movl	$153, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdi
	movl	$117, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	movl	$98, %r8d
	movl	$65, %edx
	movl	$153, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE872:
	.size	new_dir, .-new_dir
	.p2align 4
	.type	add_cert_dir.isra.0, @function
add_cert_dir.isra.0:
.LFB879:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -56(%rbp)
	movl	%edx, -60(%rbp)
	testq	%rsi, %rsi
	je	.L72
	movzbl	(%rsi), %eax
	movq	%rdi, %r13
	movq	%rsi, %r12
	testb	%al, %al
	jne	.L73
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L109:
	testb	%al, %al
	je	.L91
.L75:
	movzbl	(%r14), %eax
	movq	%r14, %r12
.L73:
	leaq	1(%r12), %r14
	cmpb	$58, %al
	jne	.L109
.L91:
	subq	-56(%rbp), %r12
	jne	.L110
	testb	%al, %al
	je	.L90
.L113:
	movq	%r14, -56(%rbp)
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L110:
	xorl	%ebx, %ebx
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L79:
	addl	$1, %ebx
.L78:
	movq	0(%r13), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L83
	movq	0(%r13), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	(%rax), %r15
	movq	%r15, %rdi
	call	strlen@PLT
	cmpq	%rax, %r12
	jne	.L79
	movq	-56(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	strncmp@PLT
	testl	%eax, %eax
	jne	.L79
.L83:
	movq	0(%r13), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L107
	cmpq	$0, 0(%r13)
	je	.L111
.L84:
	movl	$189, %edx
	leaq	.LC0(%rip), %rsi
	movl	$24, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L112
	movl	-60(%rbp), %eax
	leaq	by_dir_hash_cmp(%rip), %rdi
	movl	%eax, 8(%rbx)
	call	OPENSSL_sk_new@PLT
	movq	-56(%rbp), %rdi
	movl	$196, %ecx
	movq	%r12, %rsi
	movq	%rax, 16(%rbx)
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_strndup@PLT
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L86
	cmpq	$0, 16(%rbx)
	je	.L86
	movq	0(%r13), %rdi
	movq	%rbx, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L88
.L107:
	movzbl	-1(%r14), %eax
	testb	%al, %al
	jne	.L113
.L90:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	jne	.L84
	movl	$185, %r8d
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L72:
	movl	$160, %r8d
	movl	$113, %edx
	movl	$100, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L71:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L86:
	.cfi_restore_state
	movq	%rax, %rdi
	movl	$138, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	16(%rbx), %rdi
	leaq	by_dir_hash_free(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	%rbx, %rdi
	movl	$140, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L112:
	.cfi_restore_state
	movl	$191, %r8d
.L108:
	movl	$65, %edx
	movl	$100, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L71
.L88:
	movq	(%rbx), %rdi
	movl	$138, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, -56(%rbp)
	call	CRYPTO_free@PLT
	movq	16(%rbx), %rdi
	leaq	by_dir_hash_free(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movl	$140, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rbx, %rdi
	call	CRYPTO_free@PLT
	movl	$203, %r8d
	movl	$65, %edx
	leaq	.LC0(%rip), %rcx
	movl	$100, %esi
	movl	$11, %edi
	call	ERR_put_error@PLT
	movl	-56(%rbp), %eax
	jmp	.L71
	.cfi_endproc
.LFE879:
	.size	add_cert_dir.isra.0, .-add_cert_dir.isra.0
	.p2align 4
	.type	dir_ctrl, @function
dir_ctrl:
.LFB871:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$2, %esi
	je	.L125
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movq	16(%rdi), %rax
	leaq	8(%rax), %r12
	cmpq	$3, %rcx
	je	.L126
	addq	$24, %rsp
	movq	%rdx, %rsi
	movq	%r12, %rdi
	movl	%ecx, %edx
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	add_cert_dir.isra.0
	.p2align 4,,10
	.p2align 3
.L126:
	.cfi_restore_state
	call	X509_get_default_cert_dir_env@PLT
	movq	%rax, %rdi
	call	ossl_safe_getenv@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L117
.L124:
	movl	$1, %edx
	movq	%r12, %rdi
	call	add_cert_dir.isra.0
	testl	%eax, %eax
	je	.L127
.L114:
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	movl	$84, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$103, %edx
	movl	%eax, -20(%rbp)
	movl	$102, %esi
	movl	$11, %edi
	call	ERR_put_error@PLT
	movl	-20(%rbp), %eax
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L117:
	call	X509_get_default_cert_dir@PLT
	movq	%rax, %rsi
	jmp	.L124
	.cfi_endproc
.LFE871:
	.size	dir_ctrl, .-dir_ctrl
	.p2align 4
	.globl	X509_LOOKUP_hash_dir
	.type	X509_LOOKUP_hash_dir, @function
X509_LOOKUP_hash_dir:
.LFB870:
	.cfi_startproc
	endbr64
	leaq	x509_dir_lookup(%rip), %rax
	ret
	.cfi_endproc
.LFE870:
	.size	X509_LOOKUP_hash_dir, .-X509_LOOKUP_hash_dir
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"Load certs from files in a directory"
	.section	.data.rel.local,"aw"
	.align 32
	.type	x509_dir_lookup, @object
	.size	x509_dir_lookup, 80
x509_dir_lookup:
	.quad	.LC4
	.quad	new_dir
	.quad	free_dir
	.quad	0
	.quad	0
	.quad	dir_ctrl
	.quad	get_cert_by_subject
	.quad	0
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
