	.file	"eng_err.c"
	.text
	.p2align 4
	.globl	ERR_load_ENGINE_strings
	.type	ERR_load_ENGINE_strings, @function
ERR_load_ENGINE_strings:
.LFB203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$638345216, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_func_error_string@PLT
	testq	%rax, %rax
	je	.L5
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	leaq	ENGINE_str_functs(%rip), %rdi
	call	ERR_load_strings_const@PLT
	leaq	ENGINE_str_reasons(%rip), %rdi
	call	ERR_load_strings_const@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE203:
	.size	ERR_load_ENGINE_strings, .-ERR_load_ENGINE_strings
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"already loaded"
.LC1:
	.string	"argument is not a number"
.LC2:
	.string	"cmd not executable"
.LC3:
	.string	"command takes input"
.LC4:
	.string	"command takes no input"
.LC5:
	.string	"conflicting engine id"
.LC6:
	.string	"ctrl command not implemented"
.LC7:
	.string	"DSO failure"
.LC8:
	.string	"dso not found"
.LC9:
	.string	"engines section error"
.LC10:
	.string	"engine configuration error"
.LC11:
	.string	"engine is not in the list"
.LC12:
	.string	"engine section error"
.LC13:
	.string	"failed loading private key"
.LC14:
	.string	"failed loading public key"
.LC15:
	.string	"finish failed"
.LC16:
	.string	"'id' or 'name' missing"
.LC17:
	.string	"init failed"
.LC18:
	.string	"internal list error"
.LC19:
	.string	"invalid argument"
.LC20:
	.string	"invalid cmd name"
.LC21:
	.string	"invalid cmd number"
.LC22:
	.string	"invalid init value"
.LC23:
	.string	"invalid string"
.LC24:
	.string	"not initialised"
.LC25:
	.string	"not loaded"
.LC26:
	.string	"no control function"
.LC27:
	.string	"no index"
.LC28:
	.string	"no load function"
.LC29:
	.string	"no reference"
.LC30:
	.string	"no such engine"
.LC31:
	.string	"unimplemented cipher"
.LC32:
	.string	"unimplemented digest"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC33:
	.string	"unimplemented public key method"
	.section	.rodata.str1.1
.LC34:
	.string	"version incompatibility"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	ENGINE_str_reasons, @object
	.size	ENGINE_str_reasons, 576
ENGINE_str_reasons:
	.quad	637534308
	.quad	.LC0
	.quad	637534341
	.quad	.LC1
	.quad	637534342
	.quad	.LC2
	.quad	637534343
	.quad	.LC3
	.quad	637534344
	.quad	.LC4
	.quad	637534311
	.quad	.LC5
	.quad	637534327
	.quad	.LC6
	.quad	637534312
	.quad	.LC7
	.quad	637534340
	.quad	.LC8
	.quad	637534356
	.quad	.LC9
	.quad	637534310
	.quad	.LC10
	.quad	637534313
	.quad	.LC11
	.quad	637534357
	.quad	.LC12
	.quad	637534336
	.quad	.LC13
	.quad	637534337
	.quad	.LC14
	.quad	637534314
	.quad	.LC15
	.quad	637534316
	.quad	.LC16
	.quad	637534317
	.quad	.LC17
	.quad	637534318
	.quad	.LC18
	.quad	637534351
	.quad	.LC19
	.quad	637534345
	.quad	.LC20
	.quad	637534346
	.quad	.LC21
	.quad	637534359
	.quad	.LC22
	.quad	637534358
	.quad	.LC23
	.quad	637534325
	.quad	.LC24
	.quad	637534320
	.quad	.LC25
	.quad	637534328
	.quad	.LC26
	.quad	637534352
	.quad	.LC27
	.quad	637534333
	.quad	.LC28
	.quad	637534338
	.quad	.LC29
	.quad	637534324
	.quad	.LC30
	.quad	637534354
	.quad	.LC31
	.quad	637534355
	.quad	.LC32
	.quad	637534309
	.quad	.LC33
	.quad	637534353
	.quad	.LC34
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC35:
	.string	"digest_update"
.LC36:
	.string	"dynamic_ctrl"
.LC37:
	.string	"dynamic_get_data_ctx"
.LC38:
	.string	"dynamic_load"
.LC39:
	.string	"dynamic_set_data_ctx"
.LC40:
	.string	"ENGINE_add"
.LC41:
	.string	"ENGINE_by_id"
.LC42:
	.string	"ENGINE_cmd_is_executable"
.LC43:
	.string	"ENGINE_ctrl"
.LC44:
	.string	"ENGINE_ctrl_cmd"
.LC45:
	.string	"ENGINE_ctrl_cmd_string"
.LC46:
	.string	"ENGINE_finish"
.LC47:
	.string	"ENGINE_get_cipher"
.LC48:
	.string	"ENGINE_get_digest"
.LC49:
	.string	"ENGINE_get_first"
.LC50:
	.string	"ENGINE_get_last"
.LC51:
	.string	"ENGINE_get_next"
.LC52:
	.string	"ENGINE_get_pkey_asn1_meth"
.LC53:
	.string	"ENGINE_get_pkey_meth"
.LC54:
	.string	"ENGINE_get_prev"
.LC55:
	.string	"ENGINE_init"
.LC56:
	.string	"engine_list_add"
.LC57:
	.string	"engine_list_remove"
.LC58:
	.string	"ENGINE_load_private_key"
.LC59:
	.string	"ENGINE_load_public_key"
.LC60:
	.string	"ENGINE_load_ssl_client_cert"
.LC61:
	.string	"ENGINE_new"
.LC62:
	.string	"ENGINE_pkey_asn1_find_str"
.LC63:
	.string	"ENGINE_remove"
.LC64:
	.string	"ENGINE_set_default_string"
.LC65:
	.string	"ENGINE_set_id"
.LC66:
	.string	"ENGINE_set_name"
.LC67:
	.string	"engine_table_register"
.LC68:
	.string	"engine_unlocked_finish"
.LC69:
	.string	"ENGINE_up_ref"
.LC70:
	.string	"int_cleanup_item"
.LC71:
	.string	"int_ctrl_helper"
.LC72:
	.string	"int_engine_configure"
.LC73:
	.string	"int_engine_module_init"
.LC74:
	.string	"ossl_hmac_init"
	.section	.data.rel.ro.local
	.align 32
	.type	ENGINE_str_functs, @object
	.size	ENGINE_str_functs, 656
ENGINE_str_functs:
	.quad	638345216
	.quad	.LC35
	.quad	638271488
	.quad	.LC36
	.quad	638275584
	.quad	.LC37
	.quad	638279680
	.quad	.LC38
	.quad	638283776
	.quad	.LC39
	.quad	637964288
	.quad	.LC40
	.quad	637968384
	.quad	.LC41
	.quad	638230528
	.quad	.LC42
	.quad	638115840
	.quad	.LC43
	.quad	638263296
	.quad	.LC44
	.quad	638234624
	.quad	.LC45
	.quad	637972480
	.quad	.LC46
	.quad	638291968
	.quad	.LC47
	.quad	638296064
	.quad	.LC48
	.quad	638332928
	.quad	.LC49
	.quad	638337024
	.quad	.LC50
	.quad	638005248
	.quad	.LC51
	.quad	638324736
	.quad	.LC52
	.quad	638320640
	.quad	.LC53
	.quad	638009344
	.quad	.LC54
	.quad	638021632
	.quad	.LC55
	.quad	638025728
	.quad	.LC56
	.quad	638029824
	.quad	.LC57
	.quad	638148608
	.quad	.LC58
	.quad	638152704
	.quad	.LC59
	.quad	638328832
	.quad	.LC60
	.quad	638033920
	.quad	.LC61
	.quad	638341120
	.quad	.LC62
	.quad	638038016
	.quad	.LC63
	.quad	638308352
	.quad	.LC64
	.quad	638062592
	.quad	.LC65
	.quad	638066688
	.quad	.LC66
	.quad	638287872
	.quad	.LC67
	.quad	638316544
	.quad	.LC68
	.quad	638312448
	.quad	.LC69
	.quad	638349312
	.quad	.LC70
	.quad	638238720
	.quad	.LC71
	.quad	638304256
	.quad	.LC72
	.quad	638300160
	.quad	.LC73
	.quad	638353408
	.quad	.LC74
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
