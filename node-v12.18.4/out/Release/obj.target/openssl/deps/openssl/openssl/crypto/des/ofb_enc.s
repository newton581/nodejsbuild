	.file	"ofb_enc.c"
	.text
	.p2align 4
	.globl	DES_ofb_encrypt
	.type	DES_ofb_encrypt, @function
DES_ofb_encrypt:
.LFB54:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	%r8, -88(%rbp)
	movq	%r9, -112(%rbp)
	movq	%fs:40, %rsi
	movq	%rsi, -56(%rbp)
	xorl	%esi, %esi
	cmpl	$64, %edx
	jg	.L1
	movq	%rdi, %r12
	movl	%edx, %r14d
	movq	%rcx, %rax
	cmpl	$32, %edx
	jg	.L48
	je	.L32
	movl	$1, %edx
	movl	%r14d, %ecx
	movl	$0, -92(%rbp)
	salq	%cl, %rdx
	leal	-1(%rdx), %esi
	movl	%esi, -80(%rbp)
	movq	%r9, %rsi
.L5:
	movl	4(%rsi), %r8d
	movl	(%rsi), %r9d
	leaq	-1(%rax), %r13
	movl	%r8d, -60(%rbp)
	testq	%rax, %rax
	jle	.L6
	movl	%r14d, %edx
	leal	14(%r14), %eax
	movl	%r14d, %r11d
	addl	$7, %edx
	cmovns	%edx, %eax
	sarl	$3, %eax
	movl	%eax, -68(%rbp)
	movl	%eax, %r15d
	leaq	-64(%rbp), %rax
	movq	%rax, -104(%rbp)
	movl	$32, %eax
	subl	%r14d, %eax
	movq	%r13, %r14
	movl	%r9d, %r13d
	movl	%eax, -96(%rbp)
	.p2align 4,,10
	.p2align 3
.L29:
	movq	-88(%rbp), %rsi
	movq	-104(%rbp), %rdi
	movl	$1, %edx
	addq	%r15, %r12
	movl	%r11d, -76(%rbp)
	movl	%r8d, -60(%rbp)
	movl	%r8d, -72(%rbp)
	movl	%r13d, -64(%rbp)
	call	DES_encrypt1@PLT
	cmpl	$8, -68(%rbp)
	movl	-64(%rbp), %r10d
	movl	-60(%rbp), %esi
	movl	-72(%rbp), %r8d
	movl	-76(%rbp), %r11d
	ja	.L7
	leaq	.L9(%rip), %rdx
	movslq	(%rdx,%r15,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L9:
	.long	.L7-.L9
	.long	.L33-.L9
	.long	.L34-.L9
	.long	.L35-.L9
	.long	.L36-.L9
	.long	.L37-.L9
	.long	.L38-.L9
	.long	.L39-.L9
	.long	.L8-.L9
	.text
	.p2align 4,,10
	.p2align 3
.L18:
	movl	%ecx, %edi
	subq	$1, %rbx
	shrl	$24, %edi
	movb	%dil, (%rbx)
.L20:
	movl	%ecx, %edi
	subq	$1, %rbx
	shrl	$16, %edi
	movb	%dil, (%rbx)
.L21:
	movb	%ch, -1(%rbx)
	leaq	-1(%rbx), %rdi
.L22:
	movb	%cl, -1(%rdi)
	leaq	-1(%rdi), %rbx
.L23:
	movl	%eax, %edi
	leaq	-1(%rbx), %rcx
	shrl	$24, %edi
	movb	%dil, -1(%rbx)
.L24:
	movl	%eax, %edi
	leaq	-1(%rcx), %rbx
	shrl	$16, %edi
	movb	%dil, -1(%rcx)
.L25:
	movb	%ah, -1(%rbx)
	leaq	-1(%rbx), %rcx
.L26:
	movb	%al, -1(%rcx)
	leaq	-1(%rcx), %rbx
.L17:
	addq	%r15, %rbx
	cmpl	$32, %r11d
	je	.L43
	cmpl	$64, %r11d
	je	.L44
	cmpl	$32, %r11d
	jle	.L28
	leal	-32(%r11), %edi
	movl	$64, %eax
	movl	%r10d, %edx
	subl	%r11d, %eax
	movl	%edi, %ecx
	shrl	%cl, %r8d
	movl	%eax, %ecx
	sall	%cl, %edx
	movl	%edi, %ecx
	orl	%edx, %r8d
	movl	%r10d, %edx
	shrl	%cl, %edx
	movl	%eax, %ecx
	movl	%r8d, %r13d
	sall	%cl, %esi
	movl	%edx, %r8d
	orl	%esi, %r8d
.L27:
	subq	$1, %r14
	cmpq	$-1, %r14
	jne	.L29
	movl	%r13d, %r9d
.L6:
	movq	-112(%rbp), %rax
	movl	%r9d, (%rax)
	movl	%r8d, 4(%rax)
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L49
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	xorl	%eax, %eax
.L10:
	movzbl	-1(%r12), %ecx
	subq	$1, %r12
	sall	$16, %ecx
	orl	%eax, %ecx
	movl	%ecx, %edi
.L11:
	movzbl	-1(%r12), %ecx
	leaq	-1(%r12), %rax
	sall	$8, %ecx
	orl	%edi, %ecx
.L12:
	leaq	-1(%rax), %r12
	movzbl	-1(%rax), %eax
	orl	%eax, %ecx
	xorl	%esi, %ecx
.L13:
	leaq	-1(%r12), %rax
	movzbl	-1(%r12), %r12d
	sall	$24, %r12d
.L14:
	leaq	-1(%rax), %rdi
	movzbl	-1(%rax), %eax
	sall	$16, %eax
	orl	%r12d, %eax
.L15:
	leaq	-1(%rdi), %r12
	movzbl	-1(%rdi), %edi
	sall	$8, %edi
	orl	%eax, %edi
.L16:
	movzbl	-1(%r12), %eax
	andl	-92(%rbp), %ecx
	addq	%r15, %rbx
	leaq	-1(%r12,%r15), %r12
	orl	%edi, %eax
	xorl	%r10d, %eax
	andl	-80(%rbp), %eax
	cmpl	$8, -68(%rbp)
	ja	.L17
	leaq	.L19(%rip), %rdx
	movslq	(%rdx,%r15,4), %rdi
	addq	%rdx, %rdi
	notrack jmp	*%rdi
	.section	.rodata
	.align 4
	.align 4
.L19:
	.long	.L17-.L19
	.long	.L40-.L19
	.long	.L25-.L19
	.long	.L41-.L19
	.long	.L23-.L19
	.long	.L42-.L19
	.long	.L21-.L19
	.long	.L20-.L19
	.long	.L18-.L19
	.text
	.p2align 4,,10
	.p2align 3
.L40:
	movq	%rbx, %rcx
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L41:
	movq	%rbx, %rcx
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L42:
	movq	%rbx, %rdi
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L38:
	xorl	%edi, %edi
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L37:
	movq	%r12, %rax
	xorl	%ecx, %ecx
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L36:
	movl	%esi, %ecx
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L35:
	movq	%r12, %rax
	movl	%esi, %ecx
	xorl	%r12d, %r12d
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L34:
	movl	%esi, %ecx
	movq	%r12, %rdi
	xorl	%eax, %eax
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L33:
	movl	%esi, %ecx
	xorl	%edi, %edi
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L8:
	movzbl	-1(%r12), %edi
	subq	$1, %r12
	movl	%edi, %eax
	sall	$24, %eax
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L48:
	cmpl	$64, %edx
	je	.L31
	leal	-32(%rdx), %ecx
	movl	$1, %edx
	movl	$-1, -80(%rbp)
	salq	%cl, %rdx
	leal	-1(%rdx), %esi
	movl	%esi, -92(%rbp)
	movq	%r9, %rsi
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L43:
	movl	%r8d, %r13d
	movl	%r10d, %r8d
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L44:
	movl	%esi, %r8d
	movl	%r10d, %r13d
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L28:
	movl	-96(%rbp), %esi
	movl	%r13d, %r9d
	movl	%r11d, %ecx
	movl	%r8d, %eax
	shrl	%cl, %r9d
	movl	%r10d, %edx
	movl	%esi, %ecx
	sall	%cl, %eax
	movl	%r11d, %ecx
	shrl	%cl, %r8d
	movl	%esi, %ecx
	orl	%r9d, %eax
	sall	%cl, %edx
	movl	%eax, %r13d
	orl	%edx, %r8d
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L32:
	movl	$0, -92(%rbp)
	movq	%r9, %rsi
	movl	$-1, -80(%rbp)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L31:
	movl	$-1, -92(%rbp)
	movq	%r9, %rsi
	movl	$-1, -80(%rbp)
	jmp	.L5
.L49:
	call	__stack_chk_fail@PLT
.L7:
	addq	%r15, %r12
	addq	%r15, %rbx
	jmp	.L17
	.cfi_endproc
.LFE54:
	.size	DES_ofb_encrypt, .-DES_ofb_encrypt
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
