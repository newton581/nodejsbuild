	.file	"encode.c"
	.text
	.p2align 4
	.type	evp_encodeblock_int, @function
evp_encodeblock_int:
.LFB478:
	.cfi_startproc
	leaq	data_bin2ascii(%rip), %r8
	testq	%rdi, %rdi
	je	.L2
	testb	$2, 92(%rdi)
	leaq	srpdata_bin2ascii(%rip), %rax
	cmovne	%rax, %r8
.L2:
	xorl	%r9d, %r9d
	testl	%ecx, %ecx
	jg	.L5
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L14:
	movzbl	1(%rdx), %eax
	addq	$3, %rdx
	salq	$8, %rax
	orq	%rdi, %rax
	movzbl	-1(%rdx), %edi
	movq	%rax, %r10
	shrq	$18, %r10
	orq	%rax, %rdi
	shrq	$12, %rax
	movzbl	(%r8,%r10), %r10d
	andl	$63, %eax
	movb	%r10b, -4(%rsi)
	movzbl	(%r8,%rax), %eax
	movb	%al, -3(%rsi)
	movq	%rdi, %rax
	andl	$63, %edi
	shrq	$6, %rax
	andl	$63, %eax
	movzbl	(%r8,%rax), %eax
	movb	%al, -2(%rsi)
	movzbl	(%r8,%rdi), %eax
	movb	%al, -1(%rsi)
	subl	$3, %ecx
	je	.L3
.L5:
	movzbl	(%rdx), %eax
	movq	%rsi, %r10
	addl	$4, %r9d
	addq	$4, %rsi
	salq	$16, %rax
	movq	%rax, %rdi
	cmpl	$2, %ecx
	jg	.L14
	je	.L15
	shrq	$18, %rax
	movzbl	(%r8,%rax), %eax
	movb	%al, (%r10)
	movq	%rdi, %rax
	shrq	$12, %rax
	andl	$63, %eax
	movzbl	(%r8,%rax), %eax
	movb	%al, 1(%r10)
	movl	$61, %eax
.L7:
	movb	%al, 2(%r10)
	movb	$61, 3(%r10)
.L3:
	movb	$0, (%rsi)
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movzbl	1(%rdx), %edx
	salq	$8, %rdx
	orq	%rax, %rdx
	shrq	$18, %rax
	movzbl	(%r8,%rax), %eax
	movb	%al, (%r10)
	movq	%rdx, %rax
	shrq	$6, %rdx
	shrq	$12, %rax
	andl	$63, %edx
	andl	$63, %eax
	movzbl	(%r8,%rax), %eax
	movb	%al, 1(%r10)
	movzbl	(%r8,%rdx), %eax
	jmp	.L7
	.cfi_endproc
.LFE478:
	.size	evp_encodeblock_int, .-evp_encodeblock_int
	.p2align 4
	.type	evp_decodeblock_int, @function
evp_decodeblock_int:
.LFB482:
	.cfi_startproc
	leaq	data_ascii2bin(%rip), %r9
	testq	%rdi, %rdi
	je	.L17
	testb	$2, 92(%rdi)
	leaq	srpdata_ascii2bin(%rip), %rax
	cmovne	%rax, %r9
.L17:
	movzbl	(%rdx), %eax
	testb	%al, %al
	js	.L18
.L53:
	cmpb	$-32, (%r9,%rax)
	jne	.L18
	testl	%ecx, %ecx
	jle	.L18
	movzbl	1(%rdx), %eax
	addq	$1, %rdx
	subl	$1, %ecx
	testb	%al, %al
	jns	.L53
.L18:
	cmpl	$3, %ecx
	jle	.L23
	movslq	%ecx, %rax
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L60:
	movzbl	(%r9,%rdi), %edi
	orl	$19, %edi
	cmpb	$-13, %dil
	jne	.L23
	subq	$1, %rax
	subl	$1, %ecx
	cmpl	$3, %eax
	jle	.L25
.L24:
	movzbl	-1(%rdx,%rax), %edi
	movl	%eax, %ecx
	testb	%dil, %dil
	jns	.L60
.L23:
	testb	$3, %cl
	jne	.L55
	testl	%ecx, %ecx
	jle	.L38
.L35:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r11d, %r11d
	xorl	%r10d, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L61:
	movzbl	(%r9,%rax), %edi
	movzbl	1(%rdx), %eax
	movl	%edi, %r13d
	testb	%al, %al
	js	.L40
.L62:
	movzbl	(%r9,%rax), %r8d
	movzbl	2(%rdx), %eax
	testb	%al, %al
	js	.L41
.L63:
	addq	$4, %rdx
	movzbl	(%r9,%rax), %r12d
	movzbl	-1(%rdx), %eax
	testb	%al, %al
	js	.L42
.L64:
	movzbl	(%r9,%rax), %ebx
	testb	%r13b, %r13b
	js	.L33
.L65:
	testb	%r8b, %r8b
	js	.L33
	testb	%r12b, %r12b
	js	.L33
	testb	%bl, %bl
	js	.L33
	movzbl	%dil, %eax
	salq	$12, %r8
	addq	$3, %rsi
	addl	$4, %r10d
	salq	$18, %rax
	salq	$6, %r12
	addl	$3, %r11d
	orq	%r8, %rax
	orq	%r12, %rax
	movq	%rax, %rdi
	orq	%rax, %rbx
	shrq	$8, %rax
	movb	%al, -2(%rsi)
	shrq	$16, %rdi
	movb	%dil, -3(%rsi)
	movb	%bl, -1(%rsi)
	cmpl	%ecx, %r10d
	jge	.L16
.L34:
	movzbl	(%rdx), %eax
	testb	%al, %al
	jns	.L61
	movzbl	1(%rdx), %eax
	movl	$-1, %r13d
	movl	$-1, %edi
	testb	%al, %al
	jns	.L62
	.p2align 4,,10
	.p2align 3
.L40:
	movzbl	2(%rdx), %eax
	movl	$255, %r8d
	testb	%al, %al
	jns	.L63
	.p2align 4,,10
	.p2align 3
.L41:
	movzbl	3(%rdx), %eax
	addq	$4, %rdx
	movl	$255, %r12d
	testb	%al, %al
	jns	.L64
	.p2align 4,,10
	.p2align 3
.L42:
	movl	$255, %ebx
	testb	%r13b, %r13b
	jns	.L65
	.p2align 4,,10
	.p2align 3
.L33:
	movl	$-1, %r11d
.L16:
	popq	%rbx
	movl	%r11d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	testb	$3, %cl
	je	.L35
.L55:
	movl	$-1, %r11d
.L56:
	movl	%r11d, %eax
	ret
.L38:
	xorl	%r11d, %r11d
	jmp	.L56
	.cfi_endproc
.LFE482:
	.size	evp_decodeblock_int, .-evp_decodeblock_int
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/evp/encode.c"
	.text
	.p2align 4
	.globl	EVP_ENCODE_CTX_new
	.type	EVP_ENCODE_CTX_new, @function
EVP_ENCODE_CTX_new:
.LFB470:
	.cfi_startproc
	endbr64
	movl	$129, %edx
	leaq	.LC0(%rip), %rsi
	movl	$96, %edi
	jmp	CRYPTO_zalloc@PLT
	.cfi_endproc
.LFE470:
	.size	EVP_ENCODE_CTX_new, .-EVP_ENCODE_CTX_new
	.p2align 4
	.globl	EVP_ENCODE_CTX_free
	.type	EVP_ENCODE_CTX_free, @function
EVP_ENCODE_CTX_free:
.LFB471:
	.cfi_startproc
	endbr64
	movl	$134, %edx
	leaq	.LC0(%rip), %rsi
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE471:
	.size	EVP_ENCODE_CTX_free, .-EVP_ENCODE_CTX_free
	.p2align 4
	.globl	EVP_ENCODE_CTX_copy
	.type	EVP_ENCODE_CTX_copy, @function
EVP_ENCODE_CTX_copy:
.LFB472:
	.cfi_startproc
	endbr64
	movdqu	(%rsi), %xmm0
	movl	$1, %eax
	movups	%xmm0, (%rdi)
	movdqu	16(%rsi), %xmm1
	movups	%xmm1, 16(%rdi)
	movdqu	32(%rsi), %xmm2
	movups	%xmm2, 32(%rdi)
	movdqu	48(%rsi), %xmm3
	movups	%xmm3, 48(%rdi)
	movdqu	64(%rsi), %xmm4
	movups	%xmm4, 64(%rdi)
	movdqu	80(%rsi), %xmm5
	movups	%xmm5, 80(%rdi)
	ret
	.cfi_endproc
.LFE472:
	.size	EVP_ENCODE_CTX_copy, .-EVP_ENCODE_CTX_copy
	.p2align 4
	.globl	EVP_ENCODE_CTX_num
	.type	EVP_ENCODE_CTX_num, @function
EVP_ENCODE_CTX_num:
.LFB473:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	ret
	.cfi_endproc
.LFE473:
	.size	EVP_ENCODE_CTX_num, .-EVP_ENCODE_CTX_num
	.p2align 4
	.globl	evp_encode_ctx_set_flags
	.type	evp_encode_ctx_set_flags, @function
evp_encode_ctx_set_flags:
.LFB474:
	.cfi_startproc
	endbr64
	movl	%esi, 92(%rdi)
	ret
	.cfi_endproc
.LFE474:
	.size	evp_encode_ctx_set_flags, .-evp_encode_ctx_set_flags
	.p2align 4
	.globl	EVP_EncodeInit
	.type	EVP_EncodeInit, @function
EVP_EncodeInit:
.LFB475:
	.cfi_startproc
	endbr64
	movabsq	$206158430208, %rax
	movq	$0, 88(%rdi)
	movq	%rax, (%rdi)
	ret
	.cfi_endproc
.LFE475:
	.size	EVP_EncodeInit, .-EVP_EncodeInit
	.section	.rodata.str1.8
	.align 8
.LC1:
	.string	"assertion failed: ctx->length <= (int)sizeof(ctx->enc_data)"
	.text
	.p2align 4
	.globl	EVP_EncodeUpdate
	.type	EVP_EncodeUpdate, @function
EVP_EncodeUpdate:
.LFB476:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -56(%rbp)
	movl	$0, (%rdx)
	testl	%r8d, %r8d
	jle	.L83
	movq	%rcx, %r15
	movl	4(%rdi), %ecx
	movq	%rdi, %r12
	cmpl	$80, %ecx
	jg	.L91
	movslq	(%rdi), %rax
	movl	%r8d, %r13d
	subl	%eax, %ecx
	cmpl	%r8d, %ecx
	jg	.L92
	movq	%rsi, %rbx
	xorl	%r14d, %r14d
	testl	%eax, %eax
	jne	.L93
.L76:
	movl	$2147483648, %r11d
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L94:
	cmpq	%r11, %r14
	jnb	.L81
.L80:
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	evp_encodeblock_int
	movslq	4(%r12), %rcx
	cltq
	addq	%rcx, %r15
	subl	%ecx, %r13d
	addq	%rax, %rbx
	addq	%rax, %r14
	testb	$1, 92(%r12)
	jne	.L79
	movb	$10, (%rbx)
	addq	$1, %r14
	addq	$1, %rbx
.L79:
	movb	$0, (%rbx)
	movl	4(%r12), %ecx
	cmpl	%r13d, %ecx
	jle	.L94
.L78:
	cmpq	$2147483647, %r14
	ja	.L81
	testl	%r13d, %r13d
	jne	.L95
.L82:
	movq	-56(%rbp), %rax
	movl	%r13d, (%r12)
	movl	%r14d, (%rax)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	movslq	%ecx, %r14
	movq	%r15, %rsi
	leaq	8(%rdi,%rax), %rdi
	movl	%ecx, -60(%rbp)
	movq	%r14, %rdx
	addq	%r14, %r15
	call	memcpy@PLT
	movl	-60(%rbp), %ecx
	movq	%rbx, %rsi
	leaq	8(%r12), %rdx
	movq	%r12, %rdi
	subl	%ecx, %r13d
	movl	4(%r12), %ecx
	call	evp_encodeblock_int
	movl	$0, (%r12)
	movslq	%eax, %r14
	addq	%r14, %rbx
	testb	$1, 92(%r12)
	jne	.L77
	movb	$10, (%rbx)
	addq	$1, %r14
	addq	$1, %rbx
.L77:
	movb	$0, (%rbx)
	movl	4(%r12), %ecx
	cmpl	%ecx, %r13d
	jl	.L78
	cmpq	$2147483647, %r14
	jbe	.L76
.L81:
	movq	-56(%rbp), %rax
	movl	$0, (%rax)
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	movq	%r15, %rsi
	leaq	8(%rdi,%rax), %rdi
	movslq	%r8d, %rdx
	call	memcpy@PLT
	addl	%r13d, (%r12)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	leaq	8(%r12), %rdi
	movslq	%r13d, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	jmp	.L82
.L91:
	movl	$171, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE476:
	.size	EVP_EncodeUpdate, .-EVP_EncodeUpdate
	.p2align 4
	.globl	EVP_EncodeFinal
	.type	EVP_EncodeFinal, @function
EVP_EncodeFinal:
.LFB477:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	(%rdi), %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	testl	%ecx, %ecx
	jne	.L103
	movl	%ecx, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	leaq	8(%rdi), %rdx
	movq	%rsi, %r12
	movq	%rdi, %r11
	call	evp_encodeblock_int
	movl	%eax, %ecx
	movl	%eax, %eax
	addq	%r12, %rax
	testb	$1, 92(%r11)
	jne	.L98
	addl	$1, %ecx
	movb	$10, (%rax)
	movl	%ecx, %eax
	addq	%r12, %rax
.L98:
	movb	$0, (%rax)
	movl	$0, (%r11)
	movl	%ecx, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE477:
	.size	EVP_EncodeFinal, .-EVP_EncodeFinal
	.p2align 4
	.globl	EVP_EncodeBlock
	.type	EVP_EncodeBlock, @function
EVP_EncodeBlock:
.LFB479:
	.cfi_startproc
	endbr64
	movl	%edx, %ecx
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	xorl	%edi, %edi
	jmp	evp_encodeblock_int
	.cfi_endproc
.LFE479:
	.size	EVP_EncodeBlock, .-EVP_EncodeBlock
	.p2align 4
	.globl	EVP_DecodeInit
	.type	EVP_DecodeInit, @function
EVP_DecodeInit:
.LFB480:
	.cfi_startproc
	endbr64
	movq	$0, (%rdi)
	movq	$0, 88(%rdi)
	ret
	.cfi_endproc
.LFE480:
	.size	EVP_DecodeInit, .-EVP_DecodeInit
	.p2align 4
	.globl	EVP_DecodeUpdate
	.type	EVP_DecodeUpdate, @function
EVP_DecodeUpdate:
.LFB481:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	8(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$72, %rsp
	movl	(%rdi), %r9d
	movq	%rdi, -80(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%rax, -72(%rbp)
	testl	%r9d, %r9d
	jle	.L107
	movq	%rax, %rsi
	movslq	%r9d, %rax
	cmpb	$61, -1(%rsi,%rax)
	je	.L192
.L107:
	xorl	%r12d, %r12d
	testl	%r8d, %r8d
	je	.L108
	movq	-80(%rbp), %rsi
	leaq	data_ascii2bin(%rip), %rax
	leaq	srpdata_ascii2bin(%rip), %rdx
	testb	$2, 92(%rsi)
	movq	%rax, %rsi
	cmovne	%rdx, %rsi
	movq	%rsi, -56(%rbp)
	testl	%r8d, %r8d
	jle	.L146
	leal	-1(%r8), %esi
	movq	%rdx, -104(%rbp)
	xorl	%r8d, %r8d
	leaq	1(%rcx,%rsi), %rsi
	movq	%rax, -112(%rbp)
	movq	%rsi, -64(%rbp)
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L111:
	testl	%ebx, %ebx
	je	.L112
	orl	$19, %eax
	cmpb	$-13, %al
	jne	.L191
	cmpl	$242, %esi
	je	.L114
.L115:
	cmpl	$64, %r9d
	je	.L193
.L136:
	cmpq	-64(%rbp), %rcx
	je	.L110
.L131:
	movzbl	(%rcx), %edx
	addq	$1, %rcx
	testb	%dl, %dl
	js	.L191
	movq	-56(%rbp), %rsi
	movzbl	%dl, %eax
	movzbl	(%rsi,%rax), %esi
	movl	%esi, %eax
	cmpl	$255, %esi
	je	.L191
	cmpb	$61, %dl
	jne	.L111
	addl	$1, %ebx
	cmpl	$3, %ebx
	je	.L191
.L112:
	cmpl	$242, %esi
	je	.L114
	orl	$19, %eax
	cmpb	$-13, %al
	je	.L115
	cmpl	$63, %r9d
	jg	.L191
	movq	-72(%rbp), %rdi
	movslq	%r9d, %rax
	addl	$1, %r9d
	movb	%dl, (%rdi,%rax)
	cmpl	$64, %r9d
	jne	.L136
.L193:
	movq	-80(%rbp), %rax
	movq	-112(%rbp), %r11
	movq	-72(%rbp), %rsi
	testb	$2, 92(%rax)
	movzbl	8(%rax), %eax
	cmovne	-104(%rbp), %r11
	testb	%al, %al
	js	.L194
	.p2align 4,,10
	.p2align 3
.L118:
	cmpb	$-32, (%r11,%rax)
	jne	.L117
	testl	%r9d, %r9d
	je	.L121
	movzbl	1(%rsi), %eax
	addq	$1, %rsi
	subl	$1, %r9d
	testb	%al, %al
	jns	.L118
.L117:
	cmpl	$3, %r9d
	jle	.L123
.L119:
	movslq	%r9d, %rax
	leaq	-1(%rsi,%rax), %rdx
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L195:
	movzbl	(%r11,%rax), %eax
	orl	$19, %eax
	cmpb	$-13, %al
	jne	.L123
	subl	$1, %r9d
	subq	$1, %rdx
	cmpl	$3, %r9d
	je	.L161
.L122:
	movzbl	(%rdx), %eax
	testb	%al, %al
	jns	.L195
.L123:
	testb	$3, %r9b
	jne	.L161
	testl	%r9d, %r9d
	jle	.L121
	movq	-88(%rbp), %r12
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L196:
	movzbl	(%r11,%rax), %eax
	movzbl	1(%rsi), %edx
	movl	%eax, %r15d
	testb	%dl, %dl
	js	.L156
.L197:
	movzbl	(%r11,%rdx), %r10d
	movzbl	2(%rsi), %edx
	testb	%dl, %dl
	js	.L157
.L198:
	addq	$4, %rsi
	movzbl	(%r11,%rdx), %edi
	movzbl	-1(%rsi), %edx
	testb	%dl, %dl
	js	.L158
.L199:
	movzbl	(%r11,%rdx), %edx
	testb	%r15b, %r15b
	js	.L161
.L200:
	testb	%r10b, %r10b
	js	.L161
	testb	%dil, %dil
	js	.L161
	testb	%dl, %dl
	js	.L161
	salq	$18, %rax
	salq	$12, %r10
	addq	$3, %r12
	addl	$4, %r14d
	salq	$6, %rdi
	orq	%r10, %rax
	addl	$3, %r13d
	orq	%rdi, %rax
	movq	%rax, %rdi
	orq	%rax, %rdx
	shrq	$8, %rax
	movb	%al, -2(%r12)
	shrq	$16, %rdi
	movb	%dil, -3(%r12)
	movb	%dl, -1(%r12)
	cmpl	%r9d, %r14d
	jge	.L130
.L129:
	movzbl	(%rsi), %eax
	testb	%al, %al
	jns	.L196
	movzbl	1(%rsi), %edx
	movl	$-1, %r15d
	movl	$255, %eax
	testb	%dl, %dl
	jns	.L197
	.p2align 4,,10
	.p2align 3
.L156:
	movzbl	2(%rsi), %edx
	movl	$255, %r10d
	testb	%dl, %dl
	jns	.L198
	.p2align 4,,10
	.p2align 3
.L157:
	movzbl	3(%rsi), %edx
	addq	$4, %rsi
	movl	$255, %edi
	testb	%dl, %dl
	jns	.L199
	.p2align 4,,10
	.p2align 3
.L158:
	movl	$255, %edx
	testb	%r15b, %r15b
	jns	.L200
	.p2align 4,,10
	.p2align 3
.L161:
	xorl	%r9d, %r9d
.L191:
	movl	$-1, %r12d
.L108:
	movq	-96(%rbp), %rax
	movl	%r8d, (%rax)
	movq	-80(%rbp), %rax
	movl	%r9d, (%rax)
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	testl	%ebx, %ebx
	jne	.L161
	xorl	%r13d, %r13d
.L130:
	subl	%ebx, %r13d
	xorl	%r9d, %r9d
	addl	%r13d, %r8d
	movslq	%r13d, %r13
	addq	%r13, -88(%rbp)
	cmpq	-64(%rbp), %rcx
	jne	.L131
	.p2align 4,,10
	.p2align 3
.L110:
	testl	%r9d, %r9d
	jle	.L134
	testb	$3, %r9b
	je	.L201
.L134:
	testl	%r9d, %r9d
	setne	%r12b
	testl	%ebx, %ebx
	sete	%al
	orl	%eax, %r12d
	movzbl	%r12b, %r12d
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L192:
	movl	$1, %ebx
	cmpl	$1, %r9d
	je	.L107
	cmpb	$61, -2(%rsi,%rax)
	sete	%bl
	movzbl	%bl, %ebx
	addl	$1, %ebx
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L194:
	movq	-72(%rbp), %rsi
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L114:
	xorl	%r12d, %r12d
	testl	%r9d, %r9d
	jle	.L108
	movl	%r9d, %r12d
	andl	$3, %r12d
	jne	.L191
	movq	-72(%rbp), %rdx
	movq	-88(%rbp), %rsi
	movl	%r9d, %ecx
	movl	%r8d, -56(%rbp)
	movq	-80(%rbp), %rdi
	call	evp_decodeblock_int
	movl	-56(%rbp), %r8d
	cmpl	%eax, %ebx
	jg	.L161
	subl	%ebx, %eax
	xorl	%r9d, %r9d
	addl	%eax, %r8d
	jmp	.L108
.L201:
	movq	-72(%rbp), %rdx
	movq	-88(%rbp), %rsi
	movl	%r9d, %ecx
	movl	%r8d, -56(%rbp)
	movq	-80(%rbp), %rdi
	call	evp_decodeblock_int
	movl	-56(%rbp), %r8d
	cmpl	%eax, %ebx
	jg	.L161
	subl	%ebx, %eax
	xorl	%r9d, %r9d
	addl	%eax, %r8d
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L146:
	xorl	%r8d, %r8d
	jmp	.L110
	.cfi_endproc
.LFE481:
	.size	EVP_DecodeUpdate, .-EVP_DecodeUpdate
	.p2align 4
	.globl	EVP_DecodeBlock
	.type	EVP_DecodeBlock, @function
EVP_DecodeBlock:
.LFB483:
	.cfi_startproc
	endbr64
	movl	%edx, %ecx
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	xorl	%edi, %edi
	jmp	evp_decodeblock_int
	.cfi_endproc
.LFE483:
	.size	EVP_DecodeBlock, .-EVP_DecodeBlock
	.p2align 4
	.globl	EVP_DecodeFinal
	.type	EVP_DecodeFinal, @function
EVP_DecodeFinal:
.LFB484:
	.cfi_startproc
	endbr64
	movl	$0, (%rdx)
	movl	(%rdi), %ecx
	movl	$1, %eax
	testl	%ecx, %ecx
	jne	.L212
	ret
	.p2align 4,,10
	.p2align 3
.L212:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	leaq	8(%rdi), %rdx
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	evp_decodeblock_int
	movl	%eax, %edx
	movl	$-1, %eax
	testl	%edx, %edx
	js	.L203
	movl	$0, (%rbx)
	movl	$1, %eax
	movl	%edx, (%r12)
.L203:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE484:
	.size	EVP_DecodeFinal, .-EVP_DecodeFinal
	.section	.rodata
	.align 32
	.type	srpdata_ascii2bin, @object
	.size	srpdata_ascii2bin, 128
srpdata_ascii2bin:
	.string	"\377\377\377\377\377\377\377\377\377\340\360\377\377\361\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\340\377\377\377\377\377\377\377\377\377\377\377\377\362>?"
	.string	"\001\002\003\004\005\006\007\b\t\377\377\377"
	.ascii	"\377\377\377\n\013\f\r\016\017\020\021\022\023\024\025\026\027"
	.ascii	"\030\031\032\033\034\035\036\037 !\"#\377\377\377\377\377\377"
	.ascii	"$%&'()*+,-./0123456789:;<=\377\377\377\377\377"
	.align 32
	.type	data_ascii2bin, @object
	.size	data_ascii2bin, 128
data_ascii2bin:
	.string	"\377\377\377\377\377\377\377\377\377\340\360\377\377\361\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\340\377\377\377\377\377\377\377\377\377\377>\377\362\377?456789:;<=\377\377\377"
	.string	"\377\377\377"
	.ascii	"\001\002\003\004\005\006\007\b\t\n\013\f\r\016\017\020\021\022"
	.ascii	"\023\024\025\026\027\030\031\377\377\377\377\377\377\032\033"
	.ascii	"\034\035\036\037 !\"#$%&'()*+,-./0123\377\377\377\377\377"
	.align 32
	.type	srpdata_bin2ascii, @object
	.size	srpdata_bin2ascii, 65
srpdata_bin2ascii:
	.string	"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz./"
	.align 32
	.type	data_bin2ascii, @object
	.size	data_bin2ascii, 65
data_bin2ascii:
	.string	"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
