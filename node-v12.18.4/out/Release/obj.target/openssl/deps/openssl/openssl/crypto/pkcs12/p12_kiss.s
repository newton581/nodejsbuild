	.file	"p12_kiss.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/pkcs12/p12_kiss.c"
	.text
	.p2align 4
	.type	parse_bag.constprop.0, @function
parse_bag.constprop.0:
.LFB807:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$156, %esi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	PKCS12_SAFEBAG_get0_attr@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L2
	movq	8(%rax), %r12
.L2:
	movl	$157, %esi
	movq	%r15, %rdi
	call	PKCS12_SAFEBAG_get0_attr@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L3
	movq	8(%rax), %rdx
.L3:
	movq	%r15, %rdi
	movq	%rdx, -72(%rbp)
	call	PKCS12_SAFEBAG_get_nid@PLT
	movq	-72(%rbp), %rdx
	cmpl	$152, %eax
	je	.L4
	jg	.L5
	cmpl	$150, %eax
	je	.L6
	cmpl	$151, %eax
	jne	.L11
	testq	%rbx, %rbx
	je	.L11
	cmpq	$0, (%rbx)
	je	.L46
	.p2align 4,,10
	.p2align 3
.L11:
	movl	$1, %eax
.L1:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L47
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	cmpl	$155, %eax
	jne	.L11
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	PKCS12_SAFEBAG_get0_safes@PLT
	movq	%rax, %r15
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L11
	.p2align 4,,10
	.p2align 3
.L18:
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	parse_bag.constprop.0
	testl	%eax, %eax
	je	.L13
	movq	%r15, %rdi
	addl	$1, %r12d
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jl	.L18
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L4:
	movq	%r15, %rdi
	movq	%rdx, -72(%rbp)
	call	PKCS12_SAFEBAG_get_bag_nid@PLT
	cmpl	$158, %eax
	jne	.L11
	movq	%r15, %rdi
	call	PKCS12_SAFEBAG_get1_cert@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L13
	movq	-72(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L14
	movq	8(%rdx), %rsi
	movl	(%rdx), %edx
	movq	%rax, %rdi
	call	X509_keyid_set1@PLT
	testl	%eax, %eax
	je	.L43
.L14:
	testq	%r12, %r12
	je	.L15
	leaq	-64(%rbp), %rdi
	movq	%r12, %rsi
	call	ASN1_STRING_to_UTF8@PLT
	movl	%eax, %edx
	testl	%eax, %eax
	jns	.L48
.L15:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L11
.L43:
	movl	%eax, -72(%rbp)
.L42:
	movq	%r13, %rdi
	call	X509_free@PLT
	movl	-72(%rbp), %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L6:
	testq	%rbx, %rbx
	je	.L11
	cmpq	$0, (%rbx)
	jne	.L11
	movq	%r15, %rdi
	call	PKCS12_SAFEBAG_get0_p8inf@PLT
	movq	%rax, %rdi
	call	EVP_PKCS82PKEY@PLT
	testq	%rax, %rax
	movq	%rax, (%rbx)
	setne	%al
	movzbl	%al, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L13:
	xorl	%eax, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L46:
	movl	$-1, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	PKCS12_decrypt_skey@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L13
	movq	%rax, %rdi
	call	EVP_PKCS82PKEY@PLT
	movq	%r12, %rdi
	movq	%rax, (%rbx)
	call	PKCS8_PRIV_KEY_INFO_free@PLT
	xorl	%eax, %eax
	cmpq	$0, (%rbx)
	setne	%al
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L48:
	movq	-64(%rbp), %rsi
	movq	%r13, %rdi
	call	X509_alias_set1@PLT
	movq	-64(%rbp), %rdi
	movl	$227, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, -72(%rbp)
	call	CRYPTO_free@PLT
	movl	-72(%rbp), %eax
	testl	%eax, %eax
	jne	.L15
	jmp	.L42
.L47:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE807:
	.size	parse_bag.constprop.0, .-parse_bag.constprop.0
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	""
	.text
	.p2align 4
	.globl	PKCS12_parse
	.type	PKCS12_parse, @function
PKCS12_parse:
.LFB803:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -64(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%r8, -72(%rbp)
	testq	%rdx, %rdx
	je	.L50
	movq	$0, (%rdx)
.L50:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L51
	movq	$0, (%rax)
.L51:
	testq	%r14, %r14
	je	.L120
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L54
	cmpb	$0, (%rax)
	jne	.L55
.L54:
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	PKCS12_verify_mac@PLT
	movq	$0, -64(%rbp)
	testl	%eax, %eax
	je	.L121
.L56:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L122
.L58:
	movq	%r14, %rdi
	call	PKCS12_unpack_authsafes@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L59
	xorl	%ebx, %ebx
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L63:
	addl	$1, %ebx
.L60:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L123
	movq	%r14, %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	24(%rax), %rdi
	movq	%rax, %r15
	call	OBJ_obj2nid@PLT
	cmpl	$21, %eax
	je	.L124
	cmpl	$26, %eax
	jne	.L63
	movq	-64(%rbp), %rsi
	movq	%r15, %rdi
	movl	$-1, %edx
	call	PKCS12_unpack_p7encdata@PLT
	movq	%rax, %r15
	testq	%r15, %r15
	je	.L118
.L79:
	xorl	%r9d, %r9d
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L66:
	movl	%r9d, %esi
	movq	%r15, %rdi
	movl	%r9d, -52(%rbp)
	call	OPENSSL_sk_value@PLT
	movq	-64(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%rax, %rdi
	call	parse_bag.constprop.0
	testl	%eax, %eax
	je	.L65
	movl	-52(%rbp), %r9d
	addl	$1, %r9d
.L64:
	movq	%r15, %rdi
	movl	%r9d, -52(%rbp)
	call	OPENSSL_sk_num@PLT
	movl	-52(%rbp), %r9d
	cmpl	%eax, %r9d
	jl	.L66
	movq	PKCS12_SAFEBAG_free@GOTPCREL(%rip), %rsi
	movq	%r15, %rdi
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L124:
	movq	%r15, %rdi
	call	PKCS12_unpack_p7data@PLT
	movq	%rax, %r15
	testq	%r15, %r15
	jne	.L79
.L118:
	movq	PKCS7_free@GOTPCREL(%rip), %rsi
	movq	%r14, %rdi
	call	OPENSSL_sk_pop_free@PLT
.L59:
	movl	$83, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$114, %edx
	xorl	%r14d, %r14d
	movl	$118, %esi
	movl	$35, %edi
	call	ERR_put_error@PLT
.L57:
	testq	%r12, %r12
	je	.L75
	movq	(%r12), %rdi
	call	EVP_PKEY_free@PLT
	movq	$0, (%r12)
.L75:
	movq	-80(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L76
	movq	(%rbx), %rdi
	call	X509_free@PLT
	movq	$0, (%rbx)
.L76:
	movq	%r14, %rdi
	call	X509_free@PLT
	movq	X509_free@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_pop_free@PLT
	xorl	%eax, %eax
.L49:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	movq	%rax, %rsi
	movl	$-1, %edx
	movq	%r14, %rdi
	call	PKCS12_verify_mac@PLT
	testl	%eax, %eax
	jne	.L56
	movl	$70, %r8d
.L119:
	movl	$113, %edx
	movl	$118, %esi
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	leaq	.LC0(%rip), %rcx
	movl	$35, %edi
	call	ERR_put_error@PLT
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L121:
	xorl	%edx, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	call	PKCS12_verify_mac@PLT
	leaq	.LC1(%rip), %rcx
	movl	$66, %r8d
	movq	%rcx, -64(%rbp)
	testl	%eax, %eax
	je	.L119
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L58
.L122:
	movl	$78, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r14d, %r14d
	movl	$118, %esi
	movl	$35, %edi
	call	ERR_put_error@PLT
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L123:
	movq	PKCS7_free@GOTPCREL(%rip), %rsi
	movq	%r14, %rdi
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L73:
	movq	%r14, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L57
	xorl	%r14d, %r14d
.L72:
	movq	%r14, %rdi
	call	X509_free@PLT
.L69:
	movq	%r13, %rdi
	call	OPENSSL_sk_pop@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L125
	testq	%r12, %r12
	je	.L70
	cmpq	$0, (%r12)
	je	.L70
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L70
	cmpq	$0, (%rax)
	je	.L126
.L70:
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L72
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L73
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %rdi
	movq	-72(%rbp), %rax
	movq	%rdi, (%rax)
	testq	%rdi, %rdi
	jne	.L73
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L126:
	call	ERR_set_mark@PLT
	movq	(%r12), %rsi
	movq	%r14, %rdi
	call	X509_check_private_key@PLT
	testl	%eax, %eax
	je	.L127
	movq	-80(%rbp), %rax
	movq	%r14, (%rax)
	xorl	%r14d, %r14d
	call	ERR_pop_to_mark@PLT
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L125:
	movq	X509_free@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_pop_free@PLT
	addq	$40, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	call	ERR_pop_to_mark@PLT
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L65:
	movq	PKCS12_SAFEBAG_free@GOTPCREL(%rip), %rsi
	movq	%r15, %rdi
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L120:
	movl	$46, %r8d
	movl	$105, %edx
	movl	$118, %esi
	movl	$35, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L49
	.cfi_endproc
.LFE803:
	.size	PKCS12_parse, .-PKCS12_parse
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
