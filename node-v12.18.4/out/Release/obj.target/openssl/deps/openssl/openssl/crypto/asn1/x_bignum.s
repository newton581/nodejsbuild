	.file	"x_bignum.c"
	.text
	.p2align 4
	.type	bn_i2c, @function
bn_i2c:
.LFB446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %r13
	testq	%r13, %r13
	je	.L6
	movq	%r13, %rdi
	movq	%rsi, %r12
	call	BN_num_bits@PLT
	testb	$7, %al
	jne	.L12
	movl	$1, %ebx
	testq	%r12, %r12
	je	.L5
	movb	$0, (%r12)
	addq	$1, %r12
.L4:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	BN_bn2bin@PLT
.L5:
	movq	%r13, %rdi
	call	BN_num_bits@PLT
	movl	%eax, %edx
	leal	14(%rax), %eax
	addl	$7, %edx
	cmovns	%edx, %eax
	sarl	$3, %eax
	addl	%ebx, %eax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	xorl	%ebx, %ebx
	testq	%r12, %r12
	jne	.L4
	jmp	.L5
.L6:
	movl	$-1, %eax
	jmp	.L1
	.cfi_endproc
.LFE446:
	.size	bn_i2c, .-bn_i2c
	.p2align 4
	.type	bn_free, @function
bn_free:
.LFB445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L13
	testb	$1, 40(%rsi)
	jne	.L21
	call	BN_free@PLT
.L16:
	movq	$0, (%rbx)
.L13:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	call	BN_clear_free@PLT
	jmp	.L16
	.cfi_endproc
.LFE445:
	.size	bn_free, .-bn_free
	.p2align 4
	.type	bn_new, @function
bn_new:
.LFB443:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	BN_new@PLT
	testq	%rax, %rax
	movq	%rax, (%rbx)
	setne	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE443:
	.size	bn_new, .-bn_new
	.p2align 4
	.type	bn_secure_new, @function
bn_secure_new:
.LFB444:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	BN_secure_new@PLT
	testq	%rax, %rax
	movq	%rax, (%rbx)
	setne	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE444:
	.size	bn_secure_new, .-bn_secure_new
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"\n"
	.text
	.p2align 4
	.type	bn_print, @function
bn_print:
.LFB449:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rsi), %rsi
	call	BN_print@PLT
	testl	%eax, %eax
	je	.L26
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
.L26:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE449:
	.size	bn_print, .-bn_print
	.p2align 4
	.type	bn_c2i, @function
bn_c2i:
.LFB447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rdx
	movq	%rdi, %rbx
	testq	%rdx, %rdx
	je	.L44
.L33:
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	BN_bin2bn@PLT
	movq	%rax, %r8
	movl	$1, %eax
	testq	%r8, %r8
	je	.L45
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	call	BN_new@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L33
.L43:
	xorl	%eax, %eax
.L47:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L43
	testb	$1, 40(%r13)
	jne	.L46
	call	BN_free@PLT
.L37:
	movq	$0, (%rbx)
	xorl	%eax, %eax
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L46:
	call	BN_clear_free@PLT
	jmp	.L37
	.cfi_endproc
.LFE447:
	.size	bn_c2i, .-bn_c2i
	.p2align 4
	.type	bn_secure_c2i, @function
bn_secure_c2i:
.LFB448:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rdx
	movq	%rdi, %rbx
	testq	%rdx, %rdx
	je	.L62
.L49:
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	BN_bin2bn@PLT
	testq	%rax, %rax
	je	.L63
	movq	(%rbx), %rdi
	movl	$4, %esi
	call	BN_set_flags@PLT
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	call	BN_secure_new@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L49
.L61:
	xorl	%eax, %eax
.L65:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L61
	testb	$1, 40(%r13)
	jne	.L64
	call	BN_free@PLT
.L53:
	movq	$0, (%rbx)
	xorl	%eax, %eax
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L64:
	call	BN_clear_free@PLT
	jmp	.L53
	.cfi_endproc
.LFE448:
	.size	bn_secure_c2i, .-bn_secure_c2i
	.globl	CBIGNUM_it
	.section	.rodata.str1.1
.LC1:
	.string	"CBIGNUM"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	CBIGNUM_it, @object
	.size	CBIGNUM_it, 56
CBIGNUM_it:
	.byte	0
	.zero	7
	.quad	2
	.quad	0
	.quad	0
	.quad	cbignum_pf
	.quad	1
	.quad	.LC1
	.globl	BIGNUM_it
	.section	.rodata.str1.1
.LC2:
	.string	"BIGNUM"
	.section	.data.rel.ro.local
	.align 32
	.type	BIGNUM_it, @object
	.size	BIGNUM_it, 56
BIGNUM_it:
	.byte	0
	.zero	7
	.quad	2
	.quad	0
	.quad	0
	.quad	bignum_pf
	.quad	0
	.quad	.LC2
	.section	.data.rel.local,"aw"
	.align 32
	.type	cbignum_pf, @object
	.size	cbignum_pf, 64
cbignum_pf:
	.quad	0
	.quad	0
	.quad	bn_secure_new
	.quad	bn_free
	.quad	0
	.quad	bn_secure_c2i
	.quad	bn_i2c
	.quad	bn_print
	.align 32
	.type	bignum_pf, @object
	.size	bignum_pf, 64
bignum_pf:
	.quad	0
	.quad	0
	.quad	bn_new
	.quad	bn_free
	.quad	0
	.quad	bn_c2i
	.quad	bn_i2c
	.quad	bn_print
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
