	.file	"dso_dlfcn.c"
	.text
	.p2align 4
	.type	dlfcn_globallookup, @function
dlfcn_globallookup:
.LFB282:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	xorl	%edi, %edi
	pushq	%r13
	.cfi_offset 13, -32
	xorl	%r13d, %r13d
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	call	dlopen@PLT
	testq	%rax, %rax
	je	.L1
	movq	%rax, %r12
	movq	%rax, %rdi
	movq	%r14, %rsi
	call	dlsym@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	dlclose@PLT
.L1:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE282:
	.size	dlfcn_globallookup, .-dlfcn_globallookup
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/dso/dso_dlfcn.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"lib%s.so"
.LC2:
	.string	"%s.so"
	.text
	.p2align 4
	.type	dlfcn_name_converter, @function
dlfcn_name_converter:
.LFB280:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	movq	%rsi, %rdi
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	call	strlen@PLT
	movl	$47, %esi
	movq	%r13, %rdi
	movq	%rax, %r12
	leal	1(%rax), %ebx
	call	strchr@PLT
	testq	%rax, %rax
	je	.L27
	movslq	%ebx, %rdi
	movl	$267, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L14
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	strcpy@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	movq	%r14, %rdi
	call	DSO_flags@PLT
	leal	7(%r12), %edi
	addl	$4, %r12d
	movl	$267, %edx
	testb	$2, %al
	leaq	.LC0(%rip), %rsi
	cmove	%edi, %r12d
	movslq	%r12d, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L14
	movq	%r14, %rdi
	call	DSO_flags@PLT
	movq	%r13, %r8
	leaq	.LC1(%rip), %rcx
	testb	$2, %al
	je	.L26
	leaq	.LC2(%rip), %rcx
.L26:
	movq	%r12, %rdi
	movq	$-1, %rdx
	movl	$1, %esi
	xorl	%eax, %eax
	call	__sprintf_chk@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	xorl	%r12d, %r12d
	movl	$269, %r8d
	movl	$109, %edx
	movl	$123, %esi
	leaq	.LC0(%rip), %rcx
	movl	$37, %edi
	call	ERR_put_error@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE280:
	.size	dlfcn_name_converter, .-dlfcn_name_converter
	.section	.rodata.str1.1
.LC3:
	.string	"): "
.LC4:
	.string	"symname("
	.text
	.p2align 4
	.type	dlfcn_bind_func, @function
dlfcn_bind_func:
.LFB278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testq	%rdi, %rdi
	je	.L35
	movq	%rsi, %r12
	testq	%rsi, %rsi
	je	.L35
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jle	.L37
	movq	8(%rbx), %rdi
	call	OPENSSL_sk_num@PLT
	movq	8(%rbx), %rdi
	leal	-1(%rax), %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L38
	movq	%r12, %rsi
	call	dlsym@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L39
.L28:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movl	$174, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$67, %edx
	xorl	%r13d, %r13d
	movl	$100, %esi
	movl	$37, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	movl	$178, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$105, %edx
	xorl	%r13d, %r13d
	movl	$100, %esi
	movl	$37, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	movl	$183, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$104, %edx
	xorl	%r13d, %r13d
	movl	$100, %esi
	movl	$37, %edi
	call	ERR_put_error@PLT
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L39:
	movl	$188, %r8d
	movl	$106, %edx
	movl	$100, %esi
	movl	$37, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	call	dlerror@PLT
	leaq	.LC3(%rip), %rcx
	movq	%r12, %rdx
	leaq	.LC4(%rip), %rsi
	movq	%rax, %r8
	movl	$4, %edi
	xorl	%eax, %eax
	call	ERR_add_error_data@PLT
	jmp	.L28
	.cfi_endproc
.LFE278:
	.size	dlfcn_bind_func, .-dlfcn_bind_func
	.section	.rodata.str1.1
.LC5:
	.string	"filename("
	.text
	.p2align 4
	.type	dlfcn_load, @function
dlfcn_load:
.LFB276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	DSO_convert_filename@PLT
	movq	%rax, %r12
	call	__errno_location@PLT
	movl	(%rax), %r14d
	testq	%r12, %r12
	je	.L48
	movq	%rax, %r13
	movl	20(%rbx), %eax
	movq	%r12, %rdi
	andl	$32, %eax
	cmpl	$1, %eax
	sbbl	%esi, %esi
	xorb	%sil, %sil
	addl	$258, %esi
	call	dlopen@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L49
	movl	%r14d, 0(%r13)
	movq	8(%rbx), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L50
	movq	%r12, 56(%rbx)
	movl	$1, %r13d
.L40:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	movl	$128, %r8d
	movl	$105, %edx
	movl	$102, %esi
	movl	$37, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdi
	movl	$136, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r15, %rdi
	call	dlclose@PLT
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L49:
	movl	$118, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$103, %edx
	xorl	%r13d, %r13d
	movl	$102, %esi
	movl	$37, %edi
	call	ERR_put_error@PLT
	call	dlerror@PLT
	leaq	.LC3(%rip), %rcx
	movq	%r12, %rdx
	leaq	.LC5(%rip), %rsi
	movq	%rax, %r8
	movl	$4, %edi
	xorl	%eax, %eax
	call	ERR_add_error_data@PLT
	movl	$136, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L48:
	movl	$105, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$111, %edx
	xorl	%r13d, %r13d
	movl	$102, %esi
	movl	$37, %edi
	call	ERR_put_error@PLT
	movl	$136, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	CRYPTO_free@PLT
	jmp	.L40
	.cfi_endproc
.LFE276:
	.size	dlfcn_load, .-dlfcn_load
	.section	.rodata.str1.1
.LC6:
	.string	"dlfcn_pathbyaddr(): "
	.text
	.p2align 4
	.type	dlfcn_pathbyaddr, @function
dlfcn_pathbyaddr:
.LFB281:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	leaq	-80(%rbp), %rsi
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	leaq	dlfcn_pathbyaddr(%rip), %rax
	cmove	%rax, %rdi
	call	dladdr@PLT
	testl	%eax, %eax
	je	.L53
	movq	-80(%rbp), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	movl	%eax, %edx
	testl	%r13d, %r13d
	jle	.L60
	cmpl	%r13d, %eax
	jge	.L56
	leal	1(%rax), %r13d
.L57:
	movslq	%edx, %rbx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rbx, %rdx
	call	memcpy@PLT
	movb	$0, (%r12,%rbx)
.L51:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L61
	addq	$48, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	leal	-1(%r13), %edx
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L60:
	leal	1(%rax), %r13d
	jmp	.L51
.L53:
	call	dlerror@PLT
	movl	$2, %edi
	leaq	.LC6(%rip), %rsi
	movl	$-1, %r13d
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	ERR_add_error_data@PLT
	jmp	.L51
.L61:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE281:
	.size	dlfcn_pathbyaddr, .-dlfcn_pathbyaddr
	.p2align 4
	.type	dlfcn_merger, @function
dlfcn_merger:
.LFB279:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	orq	%rdx, %r14
	je	.L75
	movq	%rsi, %r12
	movq	%rdx, %r13
	testq	%rdx, %rdx
	je	.L65
	testq	%rsi, %rsi
	je	.L66
	cmpb	$47, (%rsi)
	je	.L65
	movq	%rdx, %rdi
	call	strlen@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	movl	%eax, %ebx
	call	strlen@PLT
	addl	%r14d, %eax
	testl	%r14d, %r14d
	je	.L68
	movslq	%r14d, %rdx
	cmpb	$47, -1(%r13,%rdx)
	je	.L76
.L68:
	addl	$2, %eax
	movl	$241, %edx
	leaq	.LC0(%rip), %rsi
	movslq	%eax, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L77
	movq	%r13, %rsi
	movslq	%ebx, %rbx
	movq	%rax, %rdi
	call	strcpy@PLT
	movb	$47, (%r14,%rbx)
	leaq	1(%r14,%rbx), %rdi
	movq	%r12, %rsi
	call	strcpy@PLT
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	movl	$209, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_strdup@PLT
	movl	$211, %r8d
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L74
.L62:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	leal	-1(%r14), %ebx
	subl	$1, %eax
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L66:
	movl	$219, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_strdup@PLT
	movl	$221, %r8d
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L62
	.p2align 4,,10
	.p2align 3
.L74:
	movl	$65, %edx
	movl	$130, %esi
	movl	$37, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L75:
	movl	$201, %r8d
	movl	$67, %edx
	movl	$130, %esi
	movl	$37, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L77:
	movl	$243, %r8d
	jmp	.L74
	.cfi_endproc
.LFE279:
	.size	dlfcn_merger, .-dlfcn_merger
	.p2align 4
	.type	dlfcn_unload, @function
dlfcn_unload:
.LFB277:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	testq	%rdi, %rdi
	je	.L84
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	movl	$1, %r12d
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jle	.L78
	movq	8(%rbx), %rdi
	call	OPENSSL_sk_pop@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L85
	call	dlclose@PLT
.L78:
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	xorl	%r12d, %r12d
	movl	$146, %r8d
	movl	$67, %edx
	movl	$103, %esi
	leaq	.LC0(%rip), %rcx
	movl	$37, %edi
	call	ERR_put_error@PLT
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	movl	$153, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$104, %edx
	xorl	%r12d, %r12d
	movl	$103, %esi
	movl	$37, %edi
	call	ERR_put_error@PLT
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	call	OPENSSL_sk_push@PLT
	jmp	.L78
	.cfi_endproc
.LFE277:
	.size	dlfcn_unload, .-dlfcn_unload
	.p2align 4
	.globl	DSO_METHOD_openssl
	.type	DSO_METHOD_openssl, @function
DSO_METHOD_openssl:
.LFB275:
	.cfi_startproc
	endbr64
	leaq	dso_meth_dlfcn(%rip), %rax
	ret
	.cfi_endproc
.LFE275:
	.size	DSO_METHOD_openssl, .-DSO_METHOD_openssl
	.section	.rodata.str1.8
	.align 8
.LC7:
	.string	"OpenSSL 'dlfcn' shared library method"
	.section	.data.rel.local,"aw"
	.align 32
	.type	dso_meth_dlfcn, @object
	.size	dso_meth_dlfcn, 88
dso_meth_dlfcn:
	.quad	.LC7
	.quad	dlfcn_load
	.quad	dlfcn_unload
	.quad	dlfcn_bind_func
	.quad	0
	.quad	dlfcn_name_converter
	.quad	dlfcn_merger
	.quad	0
	.quad	0
	.quad	dlfcn_pathbyaddr
	.quad	dlfcn_globallookup
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
