	.file	"asn1_err.c"
	.text
	.p2align 4
	.globl	ERR_load_ASN1_strings
	.type	ERR_load_ASN1_strings, @function
ERR_load_ASN1_strings:
.LFB203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$218513408, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_func_error_string@PLT
	testq	%rax, %rax
	je	.L5
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	leaq	ASN1_str_functs(%rip), %rdi
	call	ERR_load_strings_const@PLT
	leaq	ASN1_str_reasons(%rip), %rdi
	call	ERR_load_strings_const@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE203:
	.size	ERR_load_ASN1_strings, .-ERR_load_ASN1_strings
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"adding object"
.LC1:
	.string	"asn1 parse error"
.LC2:
	.string	"asn1 sig parse error"
.LC3:
	.string	"aux error"
.LC4:
	.string	"bad object header"
.LC5:
	.string	"bmpstring is wrong length"
.LC6:
	.string	"bn lib"
.LC7:
	.string	"boolean is wrong length"
.LC8:
	.string	"buffer too small"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"cipher has no object identifier"
	.section	.rodata.str1.1
.LC10:
	.string	"context not initialised"
.LC11:
	.string	"data is wrong"
.LC12:
	.string	"decode error"
.LC13:
	.string	"depth exceeded"
	.section	.rodata.str1.8
	.align 8
.LC14:
	.string	"digest and key type not supported"
	.section	.rodata.str1.1
.LC15:
	.string	"encode error"
.LC16:
	.string	"error getting time"
.LC17:
	.string	"error loading section"
.LC18:
	.string	"error setting cipher params"
.LC19:
	.string	"expecting an integer"
.LC20:
	.string	"expecting an object"
.LC21:
	.string	"explicit length mismatch"
.LC22:
	.string	"explicit tag not constructed"
.LC23:
	.string	"field missing"
.LC24:
	.string	"first num too large"
.LC25:
	.string	"header too long"
.LC26:
	.string	"illegal bitstring format"
.LC27:
	.string	"illegal boolean"
.LC28:
	.string	"illegal characters"
.LC29:
	.string	"illegal format"
.LC30:
	.string	"illegal hex"
.LC31:
	.string	"illegal implicit tag"
.LC32:
	.string	"illegal integer"
.LC33:
	.string	"illegal negative value"
.LC34:
	.string	"illegal nested tagging"
.LC35:
	.string	"illegal null"
.LC36:
	.string	"illegal null value"
.LC37:
	.string	"illegal object"
.LC38:
	.string	"illegal optional any"
	.section	.rodata.str1.8
	.align 8
.LC39:
	.string	"illegal options on item template"
	.section	.rodata.str1.1
.LC40:
	.string	"illegal padding"
.LC41:
	.string	"illegal tagged any"
.LC42:
	.string	"illegal time value"
.LC43:
	.string	"illegal zero content"
.LC44:
	.string	"integer not ascii format"
.LC45:
	.string	"integer too large for long"
.LC46:
	.string	"invalid bit string bits left"
.LC47:
	.string	"invalid bmpstring length"
.LC48:
	.string	"invalid digit"
.LC49:
	.string	"invalid mime type"
.LC50:
	.string	"invalid modifier"
.LC51:
	.string	"invalid number"
.LC52:
	.string	"invalid object encoding"
.LC53:
	.string	"invalid scrypt parameters"
.LC54:
	.string	"invalid separator"
.LC55:
	.string	"invalid string table value"
	.section	.rodata.str1.8
	.align 8
.LC56:
	.string	"invalid universalstring length"
	.section	.rodata.str1.1
.LC57:
	.string	"invalid utf8string"
.LC58:
	.string	"invalid value"
.LC59:
	.string	"list error"
.LC60:
	.string	"mime no content type"
.LC61:
	.string	"mime parse error"
.LC62:
	.string	"mime sig parse error"
.LC63:
	.string	"missing eoc"
.LC64:
	.string	"missing second number"
.LC65:
	.string	"missing value"
.LC66:
	.string	"mstring not universal"
.LC67:
	.string	"mstring wrong tag"
.LC68:
	.string	"nested asn1 string"
.LC69:
	.string	"nested too deep"
.LC70:
	.string	"non hex characters"
.LC71:
	.string	"not ascii format"
.LC72:
	.string	"not enough data"
.LC73:
	.string	"no content type"
.LC74:
	.string	"no matching choice type"
.LC75:
	.string	"no multipart body failure"
.LC76:
	.string	"no multipart boundary"
.LC77:
	.string	"no sig content type"
.LC78:
	.string	"null is wrong length"
.LC79:
	.string	"object not ascii format"
.LC80:
	.string	"odd number of chars"
.LC81:
	.string	"second number too large"
.LC82:
	.string	"sequence length mismatch"
.LC83:
	.string	"sequence not constructed"
.LC84:
	.string	"sequence or set needs config"
.LC85:
	.string	"short line"
.LC86:
	.string	"sig invalid mime type"
.LC87:
	.string	"streaming not supported"
.LC88:
	.string	"string too long"
.LC89:
	.string	"string too short"
	.section	.rodata.str1.8
	.align 8
.LC90:
	.string	"the asn1 object identifier is not known for this md"
	.section	.rodata.str1.1
.LC91:
	.string	"time not ascii format"
.LC92:
	.string	"too large"
.LC93:
	.string	"too long"
.LC94:
	.string	"too small"
.LC95:
	.string	"type not constructed"
.LC96:
	.string	"type not primitive"
.LC97:
	.string	"unexpected eoc"
	.section	.rodata.str1.8
	.align 8
.LC98:
	.string	"universalstring is wrong length"
	.section	.rodata.str1.1
.LC99:
	.string	"unknown format"
	.section	.rodata.str1.8
	.align 8
.LC100:
	.string	"unknown message digest algorithm"
	.section	.rodata.str1.1
.LC101:
	.string	"unknown object type"
.LC102:
	.string	"unknown public key type"
.LC103:
	.string	"unknown signature algorithm"
.LC104:
	.string	"unknown tag"
	.section	.rodata.str1.8
	.align 8
.LC105:
	.string	"unsupported any defined by type"
	.section	.rodata.str1.1
.LC106:
	.string	"unsupported cipher"
.LC107:
	.string	"unsupported public key type"
.LC108:
	.string	"unsupported type"
.LC109:
	.string	"wrong integer type"
.LC110:
	.string	"wrong public key type"
.LC111:
	.string	"wrong tag"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	ASN1_str_reasons, @object
	.size	ASN1_str_reasons, 1808
ASN1_str_reasons:
	.quad	218103979
	.quad	.LC0
	.quad	218104011
	.quad	.LC1
	.quad	218104012
	.quad	.LC2
	.quad	218103908
	.quad	.LC3
	.quad	218103910
	.quad	.LC4
	.quad	218104022
	.quad	.LC5
	.quad	218103913
	.quad	.LC6
	.quad	218103914
	.quad	.LC7
	.quad	218103915
	.quad	.LC8
	.quad	218103916
	.quad	.LC9
	.quad	218104025
	.quad	.LC10
	.quad	218103917
	.quad	.LC11
	.quad	218103918
	.quad	.LC12
	.quad	218103982
	.quad	.LC13
	.quad	218104006
	.quad	.LC14
	.quad	218103920
	.quad	.LC15
	.quad	218103981
	.quad	.LC16
	.quad	218103980
	.quad	.LC17
	.quad	218103922
	.quad	.LC18
	.quad	218103923
	.quad	.LC19
	.quad	218103924
	.quad	.LC20
	.quad	218103927
	.quad	.LC21
	.quad	218103928
	.quad	.LC22
	.quad	218103929
	.quad	.LC23
	.quad	218103930
	.quad	.LC24
	.quad	218103931
	.quad	.LC25
	.quad	218103983
	.quad	.LC26
	.quad	218103984
	.quad	.LC27
	.quad	218103932
	.quad	.LC28
	.quad	218103985
	.quad	.LC29
	.quad	218103986
	.quad	.LC30
	.quad	218103987
	.quad	.LC31
	.quad	218103988
	.quad	.LC32
	.quad	218104034
	.quad	.LC33
	.quad	218103989
	.quad	.LC34
	.quad	218103933
	.quad	.LC35
	.quad	218103990
	.quad	.LC36
	.quad	218103991
	.quad	.LC37
	.quad	218103934
	.quad	.LC38
	.quad	218103978
	.quad	.LC39
	.quad	218104029
	.quad	.LC40
	.quad	218103935
	.quad	.LC41
	.quad	218103992
	.quad	.LC42
	.quad	218104030
	.quad	.LC43
	.quad	218103993
	.quad	.LC44
	.quad	218103936
	.quad	.LC45
	.quad	218104028
	.quad	.LC46
	.quad	218103937
	.quad	.LC47
	.quad	218103938
	.quad	.LC48
	.quad	218104013
	.quad	.LC49
	.quad	218103994
	.quad	.LC50
	.quad	218103995
	.quad	.LC51
	.quad	218104024
	.quad	.LC52
	.quad	218104035
	.quad	.LC53
	.quad	218103939
	.quad	.LC54
	.quad	218104026
	.quad	.LC55
	.quad	218103941
	.quad	.LC56
	.quad	218103942
	.quad	.LC57
	.quad	218104027
	.quad	.LC58
	.quad	218103996
	.quad	.LC59
	.quad	218104014
	.quad	.LC60
	.quad	218104015
	.quad	.LC61
	.quad	218104016
	.quad	.LC62
	.quad	218103945
	.quad	.LC63
	.quad	218103946
	.quad	.LC64
	.quad	218103997
	.quad	.LC65
	.quad	218103947
	.quad	.LC66
	.quad	218103948
	.quad	.LC67
	.quad	218104005
	.quad	.LC68
	.quad	218104009
	.quad	.LC69
	.quad	218103949
	.quad	.LC70
	.quad	218103998
	.quad	.LC71
	.quad	218103950
	.quad	.LC72
	.quad	218104017
	.quad	.LC73
	.quad	218103951
	.quad	.LC74
	.quad	218104018
	.quad	.LC75
	.quad	218104019
	.quad	.LC76
	.quad	218104020
	.quad	.LC77
	.quad	218103952
	.quad	.LC78
	.quad	218103999
	.quad	.LC79
	.quad	218103953
	.quad	.LC80
	.quad	218103955
	.quad	.LC81
	.quad	218103956
	.quad	.LC82
	.quad	218103957
	.quad	.LC83
	.quad	218104000
	.quad	.LC84
	.quad	218103958
	.quad	.LC85
	.quad	218104021
	.quad	.LC86
	.quad	218104010
	.quad	.LC87
	.quad	218103959
	.quad	.LC88
	.quad	218103960
	.quad	.LC89
	.quad	218103962
	.quad	.LC90
	.quad	218104001
	.quad	.LC91
	.quad	218104031
	.quad	.LC92
	.quad	218103963
	.quad	.LC93
	.quad	218104032
	.quad	.LC94
	.quad	218103964
	.quad	.LC95
	.quad	218104003
	.quad	.LC96
	.quad	218103967
	.quad	.LC97
	.quad	218104023
	.quad	.LC98
	.quad	218103968
	.quad	.LC99
	.quad	218103969
	.quad	.LC100
	.quad	218103970
	.quad	.LC101
	.quad	218103971
	.quad	.LC102
	.quad	218104007
	.quad	.LC103
	.quad	218104002
	.quad	.LC104
	.quad	218103972
	.quad	.LC105
	.quad	218104036
	.quad	.LC106
	.quad	218103975
	.quad	.LC107
	.quad	218104004
	.quad	.LC108
	.quad	218104033
	.quad	.LC109
	.quad	218104008
	.quad	.LC110
	.quad	218103976
	.quad	.LC111
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC112:
	.string	"a2d_ASN1_OBJECT"
.LC113:
	.string	"a2i_ASN1_INTEGER"
.LC114:
	.string	"a2i_ASN1_STRING"
.LC115:
	.string	"append_exp"
.LC116:
	.string	"asn1_bio_init"
.LC117:
	.string	"ASN1_BIT_STRING_set_bit"
.LC118:
	.string	"asn1_cb"
.LC119:
	.string	"asn1_check_tlen"
.LC120:
	.string	"asn1_collect"
.LC121:
	.string	"asn1_d2i_ex_primitive"
.LC122:
	.string	"ASN1_d2i_fp"
.LC123:
	.string	"asn1_d2i_read_bio"
.LC124:
	.string	"ASN1_digest"
.LC125:
	.string	"asn1_do_adb"
.LC126:
	.string	"asn1_do_lock"
.LC127:
	.string	"ASN1_dup"
.LC128:
	.string	"asn1_enc_save"
.LC129:
	.string	"asn1_ex_c2i"
.LC130:
	.string	"asn1_find_end"
.LC131:
	.string	"ASN1_GENERALIZEDTIME_adj"
.LC132:
	.string	"ASN1_generate_v3"
.LC133:
	.string	"asn1_get_int64"
.LC134:
	.string	"ASN1_get_object"
.LC135:
	.string	"asn1_get_uint64"
.LC136:
	.string	"ASN1_i2d_bio"
.LC137:
	.string	"ASN1_i2d_fp"
.LC138:
	.string	"ASN1_item_d2i_fp"
.LC139:
	.string	"ASN1_item_dup"
.LC140:
	.string	"asn1_item_embed_d2i"
.LC141:
	.string	"asn1_item_embed_new"
.LC142:
	.string	"asn1_item_flags_i2d"
.LC143:
	.string	"ASN1_item_i2d_bio"
.LC144:
	.string	"ASN1_item_i2d_fp"
.LC145:
	.string	"ASN1_item_pack"
.LC146:
	.string	"ASN1_item_sign"
.LC147:
	.string	"ASN1_item_sign_ctx"
.LC148:
	.string	"ASN1_item_unpack"
.LC149:
	.string	"ASN1_item_verify"
.LC150:
	.string	"ASN1_mbstring_ncopy"
.LC151:
	.string	"ASN1_OBJECT_new"
.LC152:
	.string	"asn1_output_data"
.LC153:
	.string	"ASN1_PCTX_new"
.LC154:
	.string	"asn1_primitive_new"
.LC155:
	.string	"ASN1_SCTX_new"
.LC156:
	.string	"ASN1_sign"
.LC157:
	.string	"asn1_str2type"
.LC158:
	.string	"asn1_string_get_int64"
.LC159:
	.string	"asn1_string_get_uint64"
.LC160:
	.string	"ASN1_STRING_set"
.LC161:
	.string	"ASN1_STRING_TABLE_add"
.LC162:
	.string	"asn1_string_to_bn"
.LC163:
	.string	"ASN1_STRING_type_new"
.LC164:
	.string	"asn1_template_ex_d2i"
.LC165:
	.string	"asn1_template_new"
.LC166:
	.string	"asn1_template_noexp_d2i"
.LC167:
	.string	"ASN1_TIME_adj"
.LC168:
	.string	"ASN1_TYPE_get_int_octetstring"
.LC169:
	.string	"ASN1_TYPE_get_octetstring"
.LC170:
	.string	"ASN1_UTCTIME_adj"
.LC171:
	.string	"ASN1_verify"
.LC172:
	.string	"b64_read_asn1"
.LC173:
	.string	"B64_write_ASN1"
.LC174:
	.string	"BIO_new_NDEF"
.LC175:
	.string	"bitstr_cb"
.LC176:
	.string	"bn_to_asn1_string"
.LC177:
	.string	"c2i_ASN1_BIT_STRING"
.LC178:
	.string	"c2i_ASN1_INTEGER"
.LC179:
	.string	"c2i_ASN1_OBJECT"
.LC180:
	.string	"c2i_ibuf"
.LC181:
	.string	"c2i_uint64_int"
.LC182:
	.string	"collect_data"
.LC183:
	.string	"d2i_ASN1_OBJECT"
.LC184:
	.string	"d2i_ASN1_UINTEGER"
.LC185:
	.string	"d2i_AutoPrivateKey"
.LC186:
	.string	"d2i_PrivateKey"
.LC187:
	.string	"d2i_PublicKey"
.LC188:
	.string	"do_buf"
.LC189:
	.string	"do_create"
.LC190:
	.string	"do_dump"
.LC191:
	.string	"do_tcreate"
.LC192:
	.string	"i2a_ASN1_OBJECT"
.LC193:
	.string	"i2d_ASN1_bio_stream"
.LC194:
	.string	"i2d_ASN1_OBJECT"
.LC195:
	.string	"i2d_DSA_PUBKEY"
.LC196:
	.string	"i2d_EC_PUBKEY"
.LC197:
	.string	"i2d_PrivateKey"
.LC198:
	.string	"i2d_PublicKey"
.LC199:
	.string	"i2d_RSA_PUBKEY"
.LC200:
	.string	"long_c2i"
.LC201:
	.string	"ndef_prefix"
.LC202:
	.string	"ndef_suffix"
.LC203:
	.string	"oid_module_init"
.LC204:
	.string	"parse_tagging"
.LC205:
	.string	"PKCS5_pbe2_set_iv"
.LC206:
	.string	"PKCS5_pbe2_set_scrypt"
.LC207:
	.string	"PKCS5_pbe_set"
.LC208:
	.string	"PKCS5_pbe_set0_algor"
.LC209:
	.string	"PKCS5_pbkdf2_set"
.LC210:
	.string	"pkcs5_scrypt_set"
.LC211:
	.string	"SMIME_read_ASN1"
.LC212:
	.string	"SMIME_text"
.LC213:
	.string	"stable_get"
.LC214:
	.string	"stbl_module_init"
.LC215:
	.string	"uint32_c2i"
.LC216:
	.string	"uint32_new"
.LC217:
	.string	"uint64_c2i"
.LC218:
	.string	"uint64_new"
.LC219:
	.string	"X509_CRL_add0_revoked"
.LC220:
	.string	"X509_INFO_new"
.LC221:
	.string	"x509_name_encode"
.LC222:
	.string	"x509_name_ex_d2i"
.LC223:
	.string	"x509_name_ex_new"
.LC224:
	.string	"X509_PKEY_new"
	.section	.data.rel.ro.local
	.align 32
	.type	ASN1_str_functs, @object
	.size	ASN1_str_functs, 1824
ASN1_str_functs:
	.quad	218513408
	.quad	.LC112
	.quad	218521600
	.quad	.LC113
	.quad	218525696
	.quad	.LC114
	.quad	218824704
	.quad	.LC115
	.quad	218566656
	.quad	.LC116
	.quad	218853376
	.quad	.LC117
	.quad	218828800
	.quad	.LC118
	.quad	218529792
	.quad	.LC119
	.quad	218537984
	.quad	.LC120
	.quad	218546176
	.quad	.LC121
	.quad	218550272
	.quad	.LC122
	.quad	218542080
	.quad	.LC123
	.quad	218857472
	.quad	.LC124
	.quad	218554368
	.quad	.LC125
	.quad	219058176
	.quad	.LC126
	.quad	218558464
	.quad	.LC127
	.quad	218574848
	.quad	.LC128
	.quad	218939392
	.quad	.LC129
	.quad	218882048
	.quad	.LC130
	.quad	218988544
	.quad	.LC131
	.quad	218832896
	.quad	.LC132
	.quad	219021312
	.quad	.LC133
	.quad	218570752
	.quad	.LC134
	.quad	219025408
	.quad	.LC135
	.quad	218578944
	.quad	.LC136
	.quad	218583040
	.quad	.LC137
	.quad	218947584
	.quad	.LC138
	.quad	218886144
	.quad	.LC139
	.quad	218595328
	.quad	.LC140
	.quad	218599424
	.quad	.LC141
	.quad	218587136
	.quad	.LC142
	.quad	218890240
	.quad	.LC143
	.quad	218894336
	.quad	.LC144
	.quad	218914816
	.quad	.LC145
	.quad	218902528
	.quad	.LC146
	.quad	219004928
	.quad	.LC147
	.quad	218918912
	.quad	.LC148
	.quad	218910720
	.quad	.LC149
	.quad	218603520
	.quad	.LC150
	.quad	218607616
	.quad	.LC151
	.quad	218980352
	.quad	.LC152
	.quad	218943488
	.quad	.LC153
	.quad	218591232
	.quad	.LC154
	.quad	219009024
	.quad	.LC155
	.quad	218628096
	.quad	.LC156
	.quad	218836992
	.quad	.LC157
	.quad	219033600
	.quad	.LC158
	.quad	219045888
	.quad	.LC159
	.quad	218865664
	.quad	.LC160
	.quad	218632192
	.quad	.LC161
	.quad	219037696
	.quad	.LC162
	.quad	218636288
	.quad	.LC163
	.quad	218644480
	.quad	.LC164
	.quad	218648576
	.quad	.LC165
	.quad	218640384
	.quad	.LC166
	.quad	218992640
	.quad	.LC167
	.quad	218652672
	.quad	.LC168
	.quad	218656768
	.quad	.LC169
	.quad	218996736
	.quad	.LC170
	.quad	218664960
	.quad	.LC171
	.quad	218959872
	.quad	.LC172
	.quad	218963968
	.quad	.LC173
	.quad	218955776
	.quad	.LC174
	.quad	218841088
	.quad	.LC175
	.quad	219041792
	.quad	.LC176
	.quad	218877952
	.quad	.LC177
	.quad	218898432
	.quad	.LC178
	.quad	218906624
	.quad	.LC179
	.quad	219029504
	.quad	.LC180
	.quad	218517504
	.quad	.LC181
	.quad	218677248
	.quad	.LC182
	.quad	218705920
	.quad	.LC183
	.quad	218718208
	.quad	.LC184
	.quad	218951680
	.quad	.LC185
	.quad	218734592
	.quad	.LC186
	.quad	218738688
	.quad	.LC187
	.quad	218685440
	.quad	.LC188
	.quad	218611712
	.quad	.LC189
	.quad	218615808
	.quad	.LC190
	.quad	219013120
	.quad	.LC191
	.quad	218619904
	.quad	.LC192
	.quad	218968064
	.quad	.LC193
	.quad	218689536
	.quad	.LC194
	.quad	218763264
	.quad	.LC195
	.quad	218845184
	.quad	.LC196
	.quad	218771456
	.quad	.LC197
	.quad	218775552
	.quad	.LC198
	.quad	218779648
	.quad	.LC199
	.quad	218783744
	.quad	.LC200
	.quad	218624000
	.quad	.LC201
	.quad	218660864
	.quad	.LC202
	.quad	218816512
	.quad	.LC203
	.quad	218849280
	.quad	.LC204
	.quad	218787840
	.quad	.LC205
	.quad	219049984
	.quad	.LC206
	.quad	218931200
	.quad	.LC207
	.quad	218984448
	.quad	.LC208
	.quad	219000832
	.quad	.LC209
	.quad	219054080
	.quad	.LC210
	.quad	218972160
	.quad	.LC211
	.quad	218976256
	.quad	.LC212
	.quad	218669056
	.quad	.LC213
	.quad	219017216
	.quad	.LC214
	.quad	218533888
	.quad	.LC215
	.quad	218673152
	.quad	.LC216
	.quad	218562560
	.quad	.LC217
	.quad	218681344
	.quad	.LC218
	.quad	218796032
	.quad	.LC219
	.quad	218800128
	.quad	.LC220
	.quad	218935296
	.quad	.LC221
	.quad	218750976
	.quad	.LC222
	.quad	218804224
	.quad	.LC223
	.quad	218812416
	.quad	.LC224
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
