	.file	"ocsp_vfy.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ocsp/ocsp_vfy.c"
	.text
	.p2align 4
	.type	ocsp_match_issuerid, @function
ocsp_match_issuerid:
.LFB1361:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L12
	movq	(%rsi), %rdi
	movq	%rsi, %rbx
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L18
	movq	%rax, %rdi
	call	EVP_MD_size@PLT
	movslq	%eax, %r14
	testl	%r14d, %r14d
	js	.L8
	cmpl	16(%rbx), %r14d
	je	.L6
.L7:
	xorl	%eax, %eax
.L1:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L19
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	movq	%rdx, %r12
	xorl	%ebx, %ebx
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L20
.L10:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	ocsp_match_issuerid
	testl	%eax, %eax
	jle	.L1
	movq	%r12, %rdi
	addl	$1, %ebx
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L10
.L20:
	movl	$1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L6:
	cmpl	40(%rbx), %r14d
	jne	.L7
	movq	%r13, %rdi
	leaq	-128(%rbp), %r15
	call	X509_get_subject_name@PLT
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	X509_NAME_digest@PLT
	testl	%eax, %eax
	je	.L8
	movq	24(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L7
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	X509_pubkey_digest@PLT
	movq	48(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$287, %r8d
	movl	$119, %edx
	movl	$109, %esi
	movl	$39, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$-1, %eax
	jmp	.L1
.L19:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1361:
	.size	ocsp_match_issuerid, .-ocsp_match_issuerid
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Verify error:"
	.text
	.p2align 4
	.globl	OCSP_basic_verify
	.type	OCSP_basic_verify, @function
OCSP_basic_verify:
.LFB1355:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	%rdx, -112(%rbp)
	movl	8(%rdi), %edx
	movq	%rcx, -88(%rbp)
	movq	16(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L113
	cmpl	$20, (%rsi)
	jne	.L37
	movq	8(%rsi), %r8
	xorl	%r15d, %r15d
	leaq	-80(%rbp), %r14
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L97:
	addl	$1, %r15d
.L26:
	movq	%rbx, %rdi
	movq	%r8, -96(%rbp)
	call	OPENSSL_sk_num@PLT
	movq	-96(%rbp), %r8
	cmpl	%eax, %r15d
	jge	.L31
	movl	%r15d, %esi
	movq	%rbx, %rdi
	movq	%r8, -96(%rbp)
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r12
	call	EVP_sha1@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	X509_pubkey_digest@PLT
	movq	-96(%rbp), %r8
	movq	(%r8), %rax
	movq	8(%r8), %rdx
	xorq	(%r14), %rax
	xorq	8(%r14), %rdx
	orq	%rax, %rdx
	jne	.L97
	movl	16(%r14), %eax
	cmpl	%eax, 16(%r8)
	jne	.L97
.L23:
	testq	%r12, %r12
	je	.L31
	call	X509_STORE_CTX_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L32
	movq	-88(%rbp), %rax
	movl	$2, %r9d
	testb	$2, %ah
	jne	.L114
.L43:
	testb	$4, -88(%rbp)
	je	.L115
.L45:
	movq	-88(%rbp), %rax
	testb	$16, %al
	jne	.L67
	testb	$8, %al
	jne	.L68
	movq	72(%r13), %rax
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L69
	testq	%rbx, %rbx
	je	.L47
	movq	%rax, %rdi
	xorl	%r15d, %r15d
	call	OPENSSL_sk_dup@PLT
	movq	%rax, -96(%rbp)
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L52:
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L116
	addl	$1, %r15d
.L48:
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r15d
	jl	.L52
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L31:
	testb	$2, -88(%rbp)
	je	.L117
.L37:
	movl	$118, %edx
	movl	$105, %esi
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	movl	$40, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$39, %edi
	call	ERR_put_error@PLT
	movq	$0, -96(%rbp)
	xorl	%r9d, %r9d
.L44:
	movq	%r14, %rdi
	movl	%r9d, -88(%rbp)
	call	X509_STORE_CTX_free@PLT
	movq	X509_free@GOTPCREL(%rip), %rsi
	movq	%r15, %rdi
	call	OPENSSL_sk_pop_free@PLT
	cmpq	$0, 72(%r13)
	movl	-88(%rbp), %r9d
	je	.L21
	testq	%rbx, %rbx
	je	.L21
.L50:
	movq	-96(%rbp), %rdi
	movl	%r9d, -88(%rbp)
	call	OPENSSL_sk_free@PLT
	movl	-88(%rbp), %r9d
.L21:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L118
	addq	$88, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	movq	$0, -96(%rbp)
	xorl	%r15d, %r15d
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L115:
	movq	%r12, %rdi
	call	X509_get0_pubkey@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L119
	movq	64(%r13), %rdx
	leaq	48(%r13), %rsi
	movq	%rax, %r8
	movq	%r13, %rcx
	leaq	OCSP_RESPDATA_it(%rip), %rdi
	call	ASN1_item_verify@PLT
	movl	%eax, %r9d
	testl	%eax, %eax
	jg	.L45
	movl	$117, %edx
	movl	$105, %esi
	movl	%eax, -88(%rbp)
	xorl	%r15d, %r15d
	movl	$60, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$39, %edi
	call	ERR_put_error@PLT
	movq	$0, -96(%rbp)
	movl	-88(%rbp), %r9d
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L113:
	movq	%rbx, %rdi
	call	X509_find_by_subject@PLT
	movq	%rax, %r12
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L114:
	orq	$16, %rax
	movq	%rax, -88(%rbp)
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L68:
	movq	$0, -96(%rbp)
.L47:
	movq	-96(%rbp), %rcx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	X509_STORE_CTX_init@PLT
	testl	%eax, %eax
	je	.L120
	movl	$8, %esi
	movq	%r14, %rdi
	call	X509_STORE_CTX_set_purpose@PLT
	movq	%r14, %rdi
	call	X509_verify_cert@PLT
	movq	%r14, %rdi
	movl	%eax, -104(%rbp)
	call	X509_STORE_CTX_get1_chain@PLT
	movl	-104(%rbp), %r9d
	movq	%rax, %r15
	testl	%r9d, %r9d
	jle	.L121
	movl	$1, %r9d
	testq	$256, -88(%rbp)
	jne	.L44
	movq	%rax, %rdi
	movq	32(%r13), %r12
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jle	.L122
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	movl	%eax, -124(%rbp)
	testl	%eax, %eax
	jle	.L123
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	cmpl	$1, -124(%rbp)
	movq	(%rax), %rax
	movq	%rax, -120(%rbp)
	je	.L57
	movl	$1, %edx
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L58:
	addl	$1, %edx
	cmpl	%edx, -124(%rbp)
	je	.L57
.L60:
	movl	%edx, %esi
	movq	%r12, %rdi
	movl	%edx, -112(%rbp)
	call	OPENSSL_sk_value@PLT
	movq	-120(%rbp), %rdi
	movq	(%rax), %rcx
	movq	%rcx, %rsi
	movq	%rcx, -104(%rbp)
	call	OCSP_id_issuer_cmp@PLT
	movq	-104(%rbp), %rcx
	movl	-112(%rbp), %edx
	testl	%eax, %eax
	je	.L58
	movq	-120(%rbp), %rax
	movq	(%rcx), %rdi
	movq	(%rax), %rsi
	call	OBJ_cmp@PLT
	testl	%eax, %eax
	jne	.L124
.L59:
	xorl	%r9d, %r9d
	testb	$32, -88(%rbp)
	jne	.L44
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	movq	%r15, %rdi
	leal	-1(%rax), %esi
	call	OPENSSL_sk_value@PLT
	xorl	%edx, %edx
	movl	$180, %esi
	movq	%rax, %rdi
	call	X509_check_trust@PLT
	movl	%eax, %r9d
	cmpl	$1, %eax
	je	.L44
	movl	$121, %r8d
	movl	$112, %edx
	movl	$105, %esi
	movl	$39, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r9d, %r9d
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L117:
	movl	8(%r13), %eax
	movq	72(%r13), %r9
	movq	16(%r13), %rsi
	testl	%eax, %eax
	je	.L125
	cmpl	$20, (%rsi)
	jne	.L37
	movq	8(%rsi), %r8
	xorl	%r15d, %r15d
	leaq	-80(%rbp), %r14
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L98:
	addl	$1, %r15d
.L38:
	movq	%r9, %rdi
	movq	%r8, -104(%rbp)
	movq	%r9, -96(%rbp)
	call	OPENSSL_sk_num@PLT
	movq	-96(%rbp), %r9
	movq	-104(%rbp), %r8
	cmpl	%eax, %r15d
	jge	.L37
	movq	%r9, %rdi
	movl	%r15d, %esi
	movq	%r8, -104(%rbp)
	movq	%r9, -96(%rbp)
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r12
	call	EVP_sha1@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	X509_pubkey_digest@PLT
	movq	-104(%rbp), %r8
	movq	-96(%rbp), %r9
	movq	(%r8), %rax
	movq	8(%r8), %rdx
	xorq	(%r14), %rax
	xorq	8(%r14), %rdx
	orq	%rax, %rdx
	jne	.L98
	movl	16(%r14), %eax
	cmpl	%eax, 16(%r8)
	jne	.L98
.L35:
	testq	%r12, %r12
	je	.L37
	call	X509_STORE_CTX_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L32
	movl	$1, %r9d
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L124:
	movq	$0, -120(%rbp)
.L57:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r15, %rdi
	movq	%rax, -104(%rbp)
	call	OPENSSL_sk_num@PLT
	cmpl	$1, %eax
	jle	.L61
	movl	$1, %esi
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	movq	-120(%rbp), %rsi
	movq	%r12, %rdx
	movq	%rax, %rdi
	call	ocsp_match_issuerid
	movl	%eax, %r9d
	testl	%eax, %eax
	js	.L44
	je	.L61
	movq	-104(%rbp), %rdi
	call	X509_get_extension_flags@PLT
	testb	$4, %al
	je	.L63
	movq	-104(%rbp), %rdi
	call	X509_get_extended_key_usage@PLT
	movl	$1, %r9d
	testb	$32, %al
	jne	.L44
.L63:
	movl	$329, %r8d
	movl	$103, %edx
	movl	$106, %esi
	movl	$39, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L61:
	movq	-120(%rbp), %rsi
	movq	-104(%rbp), %rdi
	movq	%r12, %rdx
	call	ocsp_match_issuerid
	movl	%eax, %r9d
	testl	%eax, %eax
	je	.L59
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L69:
	movq	%rbx, -96(%rbp)
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L119:
	movl	$55, %r8d
	movl	$130, %edx
	movl	$105, %esi
	movl	$39, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	$0, -96(%rbp)
	xorl	%r9d, %r9d
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L125:
	movq	%r9, %rdi
	call	X509_find_by_subject@PLT
	movq	%rax, %r12
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L121:
	movq	%r14, %rdi
	movl	%r9d, -88(%rbp)
	call	X509_STORE_CTX_get_error@PLT
	movl	$92, %r8d
	movl	$101, %edx
	leaq	.LC0(%rip), %rcx
	movl	%eax, %r12d
	movl	$105, %esi
	movl	$39, %edi
	call	ERR_put_error@PLT
	movslq	%r12d, %rdi
	call	X509_verify_cert_error_string@PLT
	leaq	.LC1(%rip), %rsi
	movl	$2, %edi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	ERR_add_error_data@PLT
	movl	-88(%rbp), %r9d
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L116:
	movl	$72, %r8d
	movl	$65, %edx
	movl	$105, %esi
	movl	$39, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r14, %rdi
	call	X509_STORE_CTX_free@PLT
	movq	X509_free@GOTPCREL(%rip), %rsi
	xorl	%edi, %edi
	call	OPENSSL_sk_pop_free@PLT
	cmpq	$0, 72(%r13)
	movl	$-1, %r9d
	jne	.L50
	jmp	.L21
.L120:
	movl	$83, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$11, %edx
	xorl	%r15d, %r15d
	movl	$105, %esi
	movl	$39, %edi
	call	ERR_put_error@PLT
	movl	$-1, %r9d
	jmp	.L44
.L32:
	movl	$65, %edx
	movl	$105, %esi
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	movl	$46, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$39, %edi
	call	ERR_put_error@PLT
	movq	$0, -96(%rbp)
	movl	$-1, %r9d
	jmp	.L44
.L122:
	movl	$205, %r8d
	movl	$105, %edx
	movl	$108, %esi
	movl	$39, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	orl	$-1, %r9d
	jmp	.L44
.L123:
	movl	$249, %r8d
	movl	$111, %edx
	movl	$107, %esi
	movl	$39, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	orl	$-1, %r9d
	jmp	.L44
.L118:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1355:
	.size	OCSP_basic_verify, .-OCSP_basic_verify
	.p2align 4
	.globl	OCSP_resp_get0_signer
	.type	OCSP_resp_get0_signer, @function
OCSP_resp_get0_signer:
.LFB1356:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -88(%rbp)
	movl	8(%rdi), %edx
	movq	%rdi, -96(%rbp)
	movq	16(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L159
	cmpl	$20, (%rsi)
	jne	.L141
	movq	8(%rsi), %r13
	xorl	%r15d, %r15d
	leaq	-80(%rbp), %r14
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L153:
	addl	$1, %r15d
.L131:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r15d
	jge	.L136
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rbx
	call	EVP_sha1@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	X509_pubkey_digest@PLT
	movq	0(%r13), %rax
	movq	8(%r13), %rdx
	xorq	(%r14), %rax
	xorq	8(%r14), %rdx
	orq	%rax, %rdx
	jne	.L153
	movl	16(%r14), %eax
	cmpl	%eax, 16(%r13)
	jne	.L153
.L128:
	testq	%rbx, %rbx
	je	.L136
.L158:
	movq	-88(%rbp), %rax
	movq	%rbx, (%rax)
	movl	$1, %eax
.L126:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L160
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L138:
	.cfi_restore_state
	cmpl	$20, (%rsi)
	jne	.L141
	movq	8(%rsi), %r15
	xorl	%r12d, %r12d
	leaq	-80(%rbp), %r13
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L145:
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rbx
	call	EVP_sha1@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	X509_pubkey_digest@PLT
	movq	(%r15), %rax
	movq	8(%r15), %rdx
	xorq	0(%r13), %rax
	xorq	8(%r13), %rdx
	orq	%rax, %rdx
	jne	.L154
	movl	16(%r13), %eax
	cmpl	%eax, 16(%r15)
	je	.L139
.L154:
	addl	$1, %r12d
.L142:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jl	.L145
.L141:
	movq	-88(%rbp), %rax
	movq	$0, (%rax)
	xorl	%eax, %eax
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L159:
	movq	%r12, %rdi
	call	X509_find_by_subject@PLT
	movq	%rax, %rbx
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L136:
	movq	-96(%rbp), %rax
	movq	72(%rax), %r14
	movq	16(%rax), %rsi
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L138
	movq	%r14, %rdi
	call	X509_find_by_subject@PLT
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L139:
	testq	%rbx, %rbx
	jne	.L158
	jmp	.L141
.L160:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1356:
	.size	OCSP_resp_get0_signer, .-OCSP_resp_get0_signer
	.p2align 4
	.globl	OCSP_request_verify
	.type	OCSP_request_verify, @function
OCSP_request_verify:
.LFB1363:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	X509_STORE_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L186
	movq	32(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L187
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L165
	cmpl	$4, (%rax)
	je	.L166
.L165:
	movl	$359, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$129, %edx
	xorl	%r13d, %r13d
	movl	$116, %esi
	movl	$39, %edi
	call	ERR_put_error@PLT
.L163:
	movq	%r12, %rdi
	call	X509_STORE_CTX_free@PLT
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L166:
	.cfi_restore_state
	movq	8(%rax), %rsi
	testb	$2, %r13b
	je	.L188
.L167:
	movq	%r14, %rdi
	call	X509_find_by_subject@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L189
	movq	%r13, %rax
	orq	$16, %rax
	testl	$512, %r13d
	cmovne	%rax, %r13
.L168:
	testb	$4, %r13b
	je	.L190
.L169:
	testb	$16, %r13b
	jne	.L174
	andl	$8, %r13d
	je	.L171
	xorl	%ecx, %ecx
.L185:
	movq	%r9, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	X509_STORE_CTX_init@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L191
	movl	$8, %esi
	movq	%r12, %rdi
	call	X509_STORE_CTX_set_purpose@PLT
	movq	%r12, %rdi
	movl	$7, %esi
	call	X509_STORE_CTX_set_trust@PLT
	movq	%r12, %rdi
	call	X509_verify_cert@PLT
	testl	%eax, %eax
	jle	.L192
.L174:
	movl	$1, %r13d
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L188:
	movq	24(%rdx), %rdi
	movq	%rsi, -56(%rbp)
	call	X509_find_by_subject@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r9
	testq	%rax, %rax
	jne	.L168
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L186:
	movl	$349, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$116, %esi
	movl	$39, %edi
	call	ERR_put_error@PLT
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L187:
	movl	$354, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$128, %edx
	xorl	%r13d, %r13d
	movl	$116, %esi
	movl	$39, %edi
	call	ERR_put_error@PLT
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L190:
	movq	%r9, %rdi
	movq	%r9, -56(%rbp)
	call	X509_get0_pubkey@PLT
	movq	32(%rbx), %rsi
	movq	%rbx, %rcx
	leaq	OCSP_REQINFO_it(%rip), %rdi
	movq	%rax, %r8
	movq	16(%rsi), %rdx
	call	ASN1_item_verify@PLT
	movq	-56(%rbp), %r9
	testl	%eax, %eax
	jg	.L169
	movl	$377, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$117, %edx
	xorl	%r13d, %r13d
	movl	$116, %esi
	movl	$39, %edi
	call	ERR_put_error@PLT
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L171:
	movq	32(%rbx), %rax
	movq	24(%rax), %rcx
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L191:
	movl	$389, %r8d
	movl	$11, %edx
	movl	$116, %esi
	movl	$39, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L189:
	movl	$366, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$118, %edx
	xorl	%r13d, %r13d
	movl	$116, %esi
	movl	$39, %edi
	call	ERR_put_error@PLT
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L192:
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	X509_STORE_CTX_get_error@PLT
	movl	$398, %r8d
	movl	$101, %edx
	leaq	.LC0(%rip), %rcx
	movl	%eax, %ebx
	movl	$116, %esi
	movl	$39, %edi
	call	ERR_put_error@PLT
	movslq	%ebx, %rdi
	call	X509_verify_cert_error_string@PLT
	leaq	.LC1(%rip), %rsi
	movl	$2, %edi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	ERR_add_error_data@PLT
	jmp	.L163
	.cfi_endproc
.LFE1363:
	.size	OCSP_request_verify, .-OCSP_request_verify
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
