	.file	"bn_print.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/bn/bn_print.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"0"
	.text
	.p2align 4
	.globl	BN_bn2hex
	.type	BN_bn2hex, @function
BN_bn2hex:
.LFB252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L30
	movl	8(%rbx), %edi
	movl	$28, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, %r12d
	sall	$4, %edi
	addl	$2, %edi
	movslq	%edi, %rdi
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L31
	movl	16(%rbx), %ecx
	movq	%rax, %rdx
	testl	%ecx, %ecx
	jne	.L32
.L5:
	movslq	8(%rbx), %rcx
	movl	%ecx, %esi
	subl	$1, %esi
	js	.L6
	movslq	%esi, %rdi
	movl	%esi, %esi
	subq	%rsi, %rcx
	salq	$3, %rdi
	leaq	-16(,%rcx,8), %r8
	leaq	Hex(%rip), %rcx
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L33:
	movq	%rsi, %r9
	andl	$15, %esi
	shrq	$4, %r9
	movzbl	(%rcx,%rsi), %esi
	movzbl	(%rcx,%r9), %r9d
	movb	%sil, 1(%rdx)
	movb	%r9b, (%rdx)
	leaq	2(%rdx), %r9
	movq	(%rbx), %rdx
	movq	(%rdx,%rdi), %rsi
	shrq	$48, %rsi
	movl	%esi, %r10d
	movzbl	%sil, %esi
.L8:
	sarl	$4, %esi
	andl	$15, %r10d
	movslq	%esi, %rsi
	movzbl	(%rcx,%rsi), %edx
	movzbl	(%rcx,%r10), %esi
	movb	%dl, (%r9)
	leaq	2(%r9), %rdx
	movb	%sil, 1(%r9)
	movq	(%rbx), %rsi
	movq	(%rsi,%rdi), %rsi
	shrq	$40, %rsi
	movl	%esi, %r10d
	movzbl	%sil, %esi
.L10:
	sarl	$4, %esi
	andl	$15, %r10d
	leaq	2(%rdx), %r9
	movslq	%esi, %rsi
	movzbl	(%rcx,%rsi), %esi
	movb	%sil, (%rdx)
	movzbl	(%rcx,%r10), %esi
	movb	%sil, 1(%rdx)
	movq	(%rbx), %rdx
	movl	4(%rdx,%rdi), %esi
	movl	%esi, %r10d
	movzbl	%sil, %esi
.L12:
	sarl	$4, %esi
	andl	$15, %r10d
	movslq	%esi, %rsi
	movzbl	(%rcx,%rsi), %edx
	movzbl	(%rcx,%r10), %esi
	movb	%dl, (%r9)
	leaq	2(%r9), %rdx
	movb	%sil, 1(%r9)
	movq	(%rbx), %rsi
	movq	(%rsi,%rdi), %rsi
	shrq	$24, %rsi
	movl	%esi, %r10d
	movzbl	%sil, %esi
.L14:
	sarl	$4, %esi
	andl	$15, %r10d
	leaq	2(%rdx), %r9
	movslq	%esi, %rsi
	movzbl	(%rcx,%rsi), %esi
	movb	%sil, (%rdx)
	movzbl	(%rcx,%r10), %esi
	movb	%sil, 1(%rdx)
	movq	(%rbx), %rdx
	movq	(%rdx,%rdi), %rsi
	shrq	$16, %rsi
	movl	%esi, %r10d
	movzbl	%sil, %esi
.L16:
	sarl	$4, %esi
	andl	$15, %r10d
	movslq	%esi, %rsi
	movzbl	(%rcx,%rsi), %edx
	movzbl	(%rcx,%r10), %esi
	movb	%dl, (%r9)
	leaq	2(%r9), %rdx
	movb	%sil, 1(%r9)
	movq	(%rbx), %rsi
	movq	(%rsi,%rdi), %rsi
	shrq	$8, %rsi
	movl	%esi, %r10d
	movzbl	%sil, %esi
.L18:
	sarl	$4, %esi
	andl	$15, %r10d
	leaq	2(%rdx), %r9
	movslq	%esi, %rsi
	movzbl	(%rcx,%rsi), %esi
	movb	%sil, (%rdx)
	movzbl	(%rcx,%r10), %esi
	movb	%sil, 1(%rdx)
	movq	(%rbx), %rdx
	movq	(%rdx,%rdi), %r12
	movl	%r12d, %esi
	movzbl	%r12b, %r12d
.L20:
	andl	$15, %esi
	sarl	$4, %r12d
	movzbl	(%rcx,%rsi), %esi
	movslq	%r12d, %r12
	movzbl	(%rcx,%r12), %edx
	movl	$1, %r12d
	movb	%sil, 1(%r9)
	movb	%dl, (%r9)
	leaq	2(%r9), %rdx
.L21:
	subq	$8, %rdi
	cmpq	%r8, %rdi
	je	.L6
.L22:
	movq	(%rbx), %rsi
	movq	(%rsi,%rdi), %r9
	movq	%r9, %rsi
	shrq	$56, %rsi
	orl	%esi, %r12d
	jne	.L33
	movq	%r9, %rsi
	shrq	$48, %rsi
	movl	%esi, %r10d
	andl	$255, %esi
	jne	.L34
	movq	%r9, %rsi
	shrq	$40, %rsi
	movl	%esi, %r10d
	andl	$255, %esi
	jne	.L10
	movq	%r9, %rsi
	shrq	$32, %rsi
	movl	%esi, %r10d
	andl	$255, %esi
	jne	.L35
	movq	%r9, %rsi
	shrq	$24, %rsi
	movl	%esi, %r10d
	andl	$255, %esi
	jne	.L14
	movq	%r9, %rsi
	shrq	$16, %rsi
	movl	%esi, %r10d
	andl	$255, %esi
	jne	.L36
	movq	%r9, %rsi
	shrq	$8, %rsi
	movl	%esi, %r10d
	andl	$255, %esi
	jne	.L18
	movl	%r9d, %r12d
	movl	%r9d, %esi
	andl	$255, %r12d
	je	.L21
	movq	%rdx, %r9
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L6:
	movb	$0, (%rdx)
.L1:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L32:
	.cfi_restore_state
	movb	$45, (%rax)
	leaq	1(%rax), %rdx
	jmp	.L5
.L30:
	addq	$16, %rsp
	movl	$27, %edx
	leaq	.LC0(%rip), %rsi
	popq	%rbx
	leaq	.LC1(%rip), %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_strdup@PLT
.L31:
	.cfi_restore_state
	movl	$30, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$105, %esi
	movl	$3, %edi
	movq	%rax, -24(%rbp)
	call	ERR_put_error@PLT
	movq	-24(%rbp), %rax
	jmp	.L1
.L34:
	movq	%rdx, %r9
	jmp	.L8
.L36:
	movq	%rdx, %r9
	jmp	.L16
.L35:
	movq	%rdx, %r9
	jmp	.L12
	.cfi_endproc
.LFE252:
	.size	BN_bn2hex, .-BN_bn2hex
	.section	.rodata.str1.1
.LC2:
	.string	"%lu"
.LC3:
	.string	"%019lu"
	.text
	.p2align 4
	.globl	BN_bn2dec
	.type	BN_bn2dec, @function
BN_bn2dec:
.LFB253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	call	BN_num_bits@PLT
	leaq	.LC0(%rip), %rsi
	leal	(%rax,%rax,2), %edx
	movslq	%edx, %rax
	sarl	$31, %edx
	imulq	$1717986919, %rax, %rbx
	imulq	$274877907, %rax, %rax
	sarq	$34, %rbx
	sarq	$38, %rax
	subl	%edx, %ebx
	subl	%edx, %eax
	movl	$72, %edx
	addl	%eax, %ebx
	leal	2(%rbx), %eax
	addl	$5, %ebx
	movslq	%eax, %r12
	sarl	$31, %eax
	imulq	$1808407283, %r12, %r12
	sarq	$35, %r12
	subl	%eax, %r12d
	addl	$1, %r12d
	movslq	%r12d, %r12
	leaq	0(,%r12,8), %rdi
	call	CRYPTO_malloc@PLT
	movl	$73, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rax, %r14
	movslq	%ebx, %rax
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	CRYPTO_malloc@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L52
	testq	%r14, %r14
	je	.L52
	movq	%r13, %rdi
	call	BN_dup@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L58
	movq	%rax, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L42
	movl	$48, %eax
	movw	%ax, (%r15)
.L49:
	movq	%r14, %rdi
	movl	$118, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%rbx, %rdi
	call	BN_free@PLT
.L37:
	addq	$40, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	movl	$75, %r8d
	movl	$65, %edx
	movl	$104, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L58:
	movq	%r14, %rdi
	movl	$118, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	xorl	%edi, %edi
	call	BN_free@PLT
.L40:
	movq	%r15, %rdi
	movl	$122, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%r15d, %r15d
	call	CRYPTO_free@PLT
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L42:
	movq	%rbx, %rdi
	call	BN_is_negative@PLT
	testl	%eax, %eax
	je	.L51
	leaq	1(%r15), %rax
	movb	$45, (%r15)
	movq	%rax, -64(%rbp)
	movq	$1, -72(%rbp)
.L44:
	movq	%r14, %r13
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L47:
	movq	%r13, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	cmpq	%r12, %rax
	jge	.L45
	movabsq	$-8446744073709551616, %rsi
	movq	%rbx, %rdi
	call	BN_div_word@PLT
	movq	%rax, 0(%r13)
	cmpq	$-1, %rax
	je	.L45
	addq	$8, %r13
.L46:
	movq	%rbx, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L47
	movq	-8(%r13), %rcx
	movq	-64(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC2(%rip), %rdx
	movq	-56(%rbp), %rsi
	subq	-72(%rbp), %rsi
	call	BIO_snprintf@PLT
	testl	%eax, %eax
	js	.L45
	movq	-64(%rbp), %r12
	cltq
	subq	$8, %r13
	addq	%rax, %r12
	cmpq	%r13, %r14
	jne	.L50
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L59:
	cltq
	addq	%rax, %r12
	cmpq	%r13, %r14
	je	.L49
.L50:
	movq	-56(%rbp), %rsi
	movq	%r12, %rax
	movq	-8(%r13), %rcx
	movq	%r12, %rdi
	subq	%r15, %rax
	leaq	.LC3(%rip), %rdx
	subq	$8, %r13
	subq	%rax, %rsi
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	testl	%eax, %eax
	jns	.L59
	.p2align 4,,10
	.p2align 3
.L45:
	movq	%r14, %rdi
	movl	$118, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%rbx, %rdi
	call	BN_free@PLT
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L51:
	movq	%r15, -64(%rbp)
	movq	$0, -72(%rbp)
	jmp	.L44
	.cfi_endproc
.LFE253:
	.size	BN_bn2dec, .-BN_bn2dec
	.p2align 4
	.globl	BN_hex2bn
	.type	BN_hex2bn, @function
BN_hex2bn:
.LFB254:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -96(%rbp)
	movq	%rsi, -56(%rbp)
	testq	%rsi, %rsi
	je	.L92
	movzbl	(%rsi), %eax
	testb	%al, %al
	je	.L92
	movl	$0, -88(%rbp)
	cmpb	$45, %al
	je	.L93
.L64:
	xorl	%r14d, %r14d
.L65:
	movq	-56(%rbp), %rax
	movl	$16, %esi
	movl	%r14d, %ebx
	movsbl	(%rax,%r14), %edi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	jne	.L67
	testl	%r14d, %r14d
	je	.L76
	movl	-88(%rbp), %eax
	addl	%r14d, %eax
	movl	%eax, -84(%rbp)
	movq	-96(%rbp), %rax
	testq	%rax, %rax
	je	.L60
	movq	(%rax), %rax
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L94
	movq	%rax, %rdi
	xorl	%esi, %esi
	call	BN_set_word@PLT
.L69:
	leal	0(,%rbx,4), %eax
	cmpl	$2147483584, %eax
	jg	.L66
	leal	63(%rax), %esi
	movq	-80(%rbp), %rax
	sarl	$6, %esi
	cmpl	12(%rax), %esi
	jg	.L70
.L72:
	movslq	%ebx, %rax
	movl	%r14d, -100(%rbp)
	movq	%rax, -72(%rbp)
	movq	$0, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L71:
	cmpl	$16, %r14d
	movl	$16, %eax
	movq	-72(%rbp), %rsi
	movq	-56(%rbp), %rdi
	cmovle	%r14d, %eax
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	movq	%rsi, %rdx
	leaq	1(%rdi,%rsi), %r12
	movslq	%eax, %rcx
	subl	$1, %eax
	subq	%rcx, %rdx
	subq	%rcx, %r12
	leaq	(%rdi,%rdx), %r15
	addq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L73:
	movzbl	(%r15), %edi
	salq	$4, %rbx
	call	OPENSSL_hexchar2int@PLT
	testl	%eax, %eax
	cmovs	%r13d, %eax
	addq	$1, %r15
	cltq
	orq	%rax, %rbx
	cmpq	%r15, %r12
	jne	.L73
	movq	-80(%rbp), %rax
	movq	-64(%rbp), %rcx
	subl	$16, %r14d
	subq	$16, -72(%rbp)
	movq	(%rax), %rax
	movq	%rbx, (%rax,%rcx)
	addq	$8, %rcx
	movq	%rcx, -64(%rbp)
	testl	%r14d, %r14d
	jg	.L71
	movl	-100(%rbp), %eax
	movq	-80(%rbp), %rbx
	subl	$1, %eax
	movq	%rbx, %rdi
	shrl	$4, %eax
	addl	$1, %eax
	movl	%eax, 8(%rbx)
	call	bn_correct_top@PLT
	movq	-96(%rbp), %rax
	movl	8(%rbx), %edx
	movq	%rbx, (%rax)
	testl	%edx, %edx
	je	.L60
	movl	-88(%rbp), %esi
	movl	%esi, 16(%rbx)
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L67:
	addq	$1, %r14
	cmpq	$536870912, %r14
	jne	.L65
	.p2align 4,,10
	.p2align 3
.L76:
	movq	$0, -80(%rbp)
.L66:
	movq	-96(%rbp), %rax
	cmpq	$0, (%rax)
	jne	.L92
	movq	-80(%rbp), %rdi
	call	BN_free@PLT
	.p2align 4,,10
	.p2align 3
.L92:
	movl	$0, -84(%rbp)
.L60:
	movl	-84(%rbp), %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	addq	$1, -56(%rbp)
	movl	$1, -88(%rbp)
	jmp	.L64
.L70:
	movq	%rax, %rdi
	call	bn_expand2@PLT
	testq	%rax, %rax
	jne	.L72
	jmp	.L66
.L94:
	call	BN_new@PLT
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	jne	.L69
	jmp	.L92
	.cfi_endproc
.LFE254:
	.size	BN_hex2bn, .-BN_hex2bn
	.p2align 4
	.globl	BN_dec2bn
	.type	BN_dec2bn, @function
BN_dec2bn:
.LFB255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -56(%rbp)
	testq	%rsi, %rsi
	je	.L139
	movzbl	(%rsi), %eax
	movq	%rsi, %r14
	testb	%al, %al
	je	.L139
	movl	$0, -60(%rbp)
	cmpb	$45, %al
	je	.L140
.L99:
	xorl	%r15d, %r15d
.L100:
	movsbl	(%r14,%r15), %edi
	movl	$4, %esi
	movl	%r15d, %r13d
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	jne	.L102
	testl	%r15d, %r15d
	je	.L114
	movl	-60(%rbp), %eax
	leal	(%rax,%r15), %r12d
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L95
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L141
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	BN_set_word@PLT
.L104:
	leal	0(,%r13,4), %eax
	cmpl	$2147483584, %eax
	jg	.L101
	leal	63(%rax), %esi
	sarl	$6, %esi
	cmpl	12(%rbx), %esi
	jg	.L142
.L105:
	movslq	%r13d, %rax
	movl	%r13d, %ecx
	imulq	$1808407283, %rax, %rax
	sarl	$31, %ecx
	sarq	$35, %rax
	subl	%ecx, %eax
	leal	(%rax,%rax,8), %ecx
	leal	(%rax,%rcx,2), %eax
	subl	%r13d, %eax
	leal	19(%rax), %edx
	movl	$0, %eax
	cmove	%eax, %edx
	leal	-1(%r15), %eax
	xorl	%r15d, %r15d
	leaq	1(%r14,%rax), %r13
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L110:
	cmpq	%r13, %r14
	je	.L143
.L111:
	movsbl	(%r14), %eax
	leaq	(%r15,%r15,4), %rcx
	addl	$1, %edx
	addq	$1, %r14
	subl	$48, %eax
	cltq
	leaq	(%rax,%rcx,2), %r15
	cmpl	$19, %edx
	jne	.L110
	movabsq	$-8446744073709551616, %rsi
	movq	%rbx, %rdi
	call	BN_mul_word@PLT
	testl	%eax, %eax
	je	.L101
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	BN_add_word@PLT
	testl	%eax, %eax
	je	.L101
	xorl	%edx, %edx
	xorl	%r15d, %r15d
	cmpq	%r13, %r14
	jne	.L111
	.p2align 4,,10
	.p2align 3
.L143:
	movq	%rbx, %rdi
	call	bn_correct_top@PLT
	movq	-56(%rbp), %rax
	movq	%rbx, (%rax)
	movl	8(%rbx), %eax
	testl	%eax, %eax
	je	.L95
	movl	-60(%rbp), %eax
	movl	%eax, 16(%rbx)
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L102:
	addq	$1, %r15
	cmpq	$536870912, %r15
	jne	.L100
	.p2align 4,,10
	.p2align 3
.L114:
	xorl	%ebx, %ebx
.L101:
	movq	-56(%rbp), %rax
	cmpq	$0, (%rax)
	jne	.L139
	movq	%rbx, %rdi
	call	BN_free@PLT
	.p2align 4,,10
	.p2align 3
.L139:
	xorl	%r12d, %r12d
.L95:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	movl	$1, -60(%rbp)
	addq	$1, %r14
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L142:
	movq	%rbx, %rdi
	call	bn_expand2@PLT
	testq	%rax, %rax
	jne	.L105
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L141:
	call	BN_new@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	jne	.L104
	jmp	.L139
	.cfi_endproc
.LFE255:
	.size	BN_dec2bn, .-BN_dec2bn
	.p2align 4
	.globl	BN_asc2bn
	.type	BN_asc2bn, @function
BN_asc2bn:
.LFB256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movzbl	(%rsi), %eax
	movq	%rsi, %rbx
	cmpb	$45, %al
	jne	.L145
	movzbl	1(%rbx), %eax
	leaq	1(%rsi), %rsi
.L145:
	cmpb	$48, %al
	jne	.L146
	movzbl	1(%rsi), %eax
	andl	$-33, %eax
	cmpb	$88, %al
	je	.L161
.L146:
	movq	%r12, %rdi
	call	BN_dec2bn
	testl	%eax, %eax
	je	.L147
.L150:
	cmpb	$45, (%rbx)
	movl	$1, %eax
	je	.L162
.L144:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore_state
	movq	(%r12), %rdx
	movl	$1, %eax
	movl	8(%rdx), %ecx
	testl	%ecx, %ecx
	je	.L144
	movl	$1, 16(%rdx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	addq	$2, %rsi
	movq	%r12, %rdi
	call	BN_hex2bn
	testl	%eax, %eax
	jne	.L150
.L147:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE256:
	.size	BN_asc2bn, .-BN_asc2bn
	.section	.rodata.str1.1
.LC4:
	.string	"-"
	.text
	.p2align 4
	.globl	BN_print
	.type	BN_print, @function
BN_print:
.LFB258:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	16(%rsi), %eax
	testl	%eax, %eax
	je	.L167
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	call	BIO_write@PLT
	cmpl	$1, %eax
	jne	.L170
.L167:
	movq	%r15, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L242
.L166:
	movslq	8(%r15), %rax
	movl	%eax, %edx
	subl	$1, %edx
	js	.L241
	movslq	%edx, %rbx
	movl	%edx, %edx
	leaq	Hex(%rip), %r13
	subq	%rdx, %rax
	salq	$3, %rbx
	leaq	-16(,%rax,8), %r14
	xorl	%eax, %eax
.L202:
	movq	(%r15), %rdx
	movq	(%rdx,%rbx), %rdx
	movq	%rdx, %rsi
	shrq	$60, %rsi
	orl	%esi, %eax
	jne	.L243
	movq	%rdx, %rax
	shrq	$56, %rax
	andl	$15, %eax
	jne	.L217
	movq	%rdx, %rax
	shrq	$52, %rax
	andl	$15, %eax
	jne	.L216
	movq	%rdx, %rax
	shrq	$48, %rax
	andl	$15, %eax
	jne	.L215
	movq	%rdx, %rax
	shrq	$44, %rax
	andl	$15, %eax
	jne	.L214
	movq	%rdx, %rax
	shrq	$40, %rax
	andl	$15, %eax
	jne	.L213
	movq	%rdx, %rax
	shrq	$36, %rax
	andl	$15, %eax
	jne	.L212
	movq	%rdx, %rax
	shrq	$32, %rax
	andl	$15, %eax
	jne	.L211
	movl	%edx, %eax
	shrl	$28, %eax
	jne	.L210
	movq	%rdx, %rax
	shrq	$24, %rax
	andl	$15, %eax
	jne	.L209
	movq	%rdx, %rax
	shrq	$20, %rax
	andl	$15, %eax
	jne	.L208
	movq	%rdx, %rax
	shrq	$16, %rax
	andl	$15, %eax
	jne	.L207
	movq	%rdx, %rax
	shrq	$12, %rax
	andl	$15, %eax
	jne	.L206
	movq	%rdx, %rax
	shrq	$8, %rax
	andl	$15, %eax
	jne	.L205
	movq	%rdx, %rax
	shrq	$4, %rax
	andl	$15, %eax
	jne	.L204
	movl	%edx, %eax
	andl	$15, %eax
	jne	.L203
.L201:
	subq	$8, %rbx
	cmpq	%r14, %rbx
	jne	.L202
.L241:
.L168:
	endbr64
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L242:
	.cfi_restore_state
	movl	$1, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	cmpl	$1, %eax
	je	.L166
.L170:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L243:
	.cfi_restore_state
	addq	%r13, %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	BIO_write@PLT
	cmpl	$1, %eax
	jne	.L170
	movq	(%r15), %rax
	movq	(%rax,%rbx), %rax
	shrq	$56, %rax
	andl	$15, %eax
.L217:
	cltq
	movl	$1, %edx
	movq	%r12, %rdi
	leaq	0(%r13,%rax), %rsi
	call	BIO_write@PLT
	cmpl	$1, %eax
	jne	.L170
	movq	(%r15), %rax
	movq	(%rax,%rbx), %rax
	shrq	$52, %rax
	andl	$15, %eax
.L216:
	cltq
	movl	$1, %edx
	movq	%r12, %rdi
	leaq	0(%r13,%rax), %rsi
	call	BIO_write@PLT
	cmpl	$1, %eax
	jne	.L170
	movq	(%r15), %rax
	movq	(%rax,%rbx), %rax
	shrq	$48, %rax
	andl	$15, %eax
.L215:
	cltq
	movl	$1, %edx
	movq	%r12, %rdi
	leaq	0(%r13,%rax), %rsi
	call	BIO_write@PLT
	cmpl	$1, %eax
	jne	.L170
	movq	(%r15), %rax
	movq	(%rax,%rbx), %rax
	shrq	$44, %rax
	andl	$15, %eax
.L214:
	cltq
	movl	$1, %edx
	movq	%r12, %rdi
	leaq	0(%r13,%rax), %rsi
	call	BIO_write@PLT
	cmpl	$1, %eax
	jne	.L170
	movq	(%r15), %rax
	movq	(%rax,%rbx), %rax
	shrq	$40, %rax
	andl	$15, %eax
.L213:
	cltq
	movl	$1, %edx
	movq	%r12, %rdi
	leaq	0(%r13,%rax), %rsi
	call	BIO_write@PLT
	cmpl	$1, %eax
	jne	.L170
	movq	(%r15), %rax
	movq	(%rax,%rbx), %rax
	shrq	$36, %rax
	andl	$15, %eax
.L212:
	cltq
	movl	$1, %edx
	movq	%r12, %rdi
	leaq	0(%r13,%rax), %rsi
	call	BIO_write@PLT
	cmpl	$1, %eax
	jne	.L170
	movq	(%r15), %rax
	movl	4(%rax,%rbx), %eax
	andl	$15, %eax
.L211:
	cltq
	movl	$1, %edx
	movq	%r12, %rdi
	leaq	0(%r13,%rax), %rsi
	call	BIO_write@PLT
	cmpl	$1, %eax
	jne	.L170
	movq	(%r15), %rax
	movq	(%rax,%rbx), %rax
	shrl	$28, %eax
.L210:
	cltq
	movl	$1, %edx
	movq	%r12, %rdi
	leaq	0(%r13,%rax), %rsi
	call	BIO_write@PLT
	cmpl	$1, %eax
	jne	.L170
	movq	(%r15), %rax
	movq	(%rax,%rbx), %rax
	shrq	$24, %rax
	andl	$15, %eax
.L209:
	cltq
	movl	$1, %edx
	movq	%r12, %rdi
	leaq	0(%r13,%rax), %rsi
	call	BIO_write@PLT
	cmpl	$1, %eax
	jne	.L170
	movq	(%r15), %rax
	movq	(%rax,%rbx), %rax
	shrq	$20, %rax
	andl	$15, %eax
.L208:
	cltq
	movl	$1, %edx
	movq	%r12, %rdi
	leaq	0(%r13,%rax), %rsi
	call	BIO_write@PLT
	cmpl	$1, %eax
	jne	.L170
	movq	(%r15), %rax
	movq	(%rax,%rbx), %rax
	shrq	$16, %rax
	andl	$15, %eax
.L207:
	cltq
	movl	$1, %edx
	movq	%r12, %rdi
	leaq	0(%r13,%rax), %rsi
	call	BIO_write@PLT
	cmpl	$1, %eax
	jne	.L170
	movq	(%r15), %rax
	movq	(%rax,%rbx), %rax
	shrq	$12, %rax
	andl	$15, %eax
.L206:
	cltq
	movl	$1, %edx
	movq	%r12, %rdi
	leaq	0(%r13,%rax), %rsi
	call	BIO_write@PLT
	cmpl	$1, %eax
	jne	.L170
	movq	(%r15), %rax
	movq	(%rax,%rbx), %rax
	shrq	$8, %rax
	andl	$15, %eax
.L205:
	cltq
	movl	$1, %edx
	movq	%r12, %rdi
	leaq	0(%r13,%rax), %rsi
	call	BIO_write@PLT
	cmpl	$1, %eax
	jne	.L170
	movq	(%r15), %rax
	movq	(%rax,%rbx), %rax
	shrq	$4, %rax
	andl	$15, %eax
.L204:
	cltq
	movl	$1, %edx
	movq	%r12, %rdi
	leaq	0(%r13,%rax), %rsi
	call	BIO_write@PLT
	cmpl	$1, %eax
	jne	.L170
	movq	(%r15), %rax
	movq	(%rax,%rbx), %rax
	andl	$15, %eax
.L203:
	cltq
	movl	$1, %edx
	movq	%r12, %rdi
	leaq	0(%r13,%rax), %rsi
	call	BIO_write@PLT
	cmpl	$1, %eax
	je	.L201
	jmp	.L170
	.cfi_endproc
.LFE258:
	.size	BN_print, .-BN_print
	.p2align 4
	.globl	BN_print_fp
	.type	BN_print_fp, @function
BN_print_fp:
.LFB257:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	testq	%rax, %rax
	je	.L244
	movq	%rax, %r12
	movq	%r15, %rcx
	xorl	%edx, %edx
	movl	$106, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	BN_print
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	BIO_free@PLT
.L244:
	popq	%r12
	movl	%r13d, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE257:
	.size	BN_print_fp, .-BN_print_fp
	.section	.rodata.str1.1
.LC5:
	.string	"bn(%zu,%zu)"
	.text
	.p2align 4
	.globl	BN_options
	.type	BN_options, @function
BN_options:
.LFB259:
	.cfi_startproc
	endbr64
	movl	init.7233(%rip), %eax
	testl	%eax, %eax
	je	.L256
	leaq	data.7234(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L256:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$64, %r8d
	movl	$64, %ecx
	xorl	%eax, %eax
	leaq	.LC5(%rip), %rdx
	movl	$16, %esi
	leaq	data.7234(%rip), %rdi
	movl	$1, init.7233(%rip)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	BIO_snprintf@PLT
	leaq	data.7234(%rip), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE259:
	.size	BN_options, .-BN_options
	.local	data.7234
	.comm	data.7234,16,16
	.local	init.7233
	.comm	init.7233,4,4
	.section	.rodata
	.align 16
	.type	Hex, @object
	.size	Hex, 17
Hex:
	.string	"0123456789ABCDEF"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
