	.file	"v3_utl.c"
	.text
	.p2align 4
	.type	equal_nocase, @function
equal_nocase:
.LFB1333:
	.cfi_startproc
	endbr64
	testl	$32768, %r8d
	je	.L2
	cmpq	%rsi, %rcx
	jnb	.L3
	movq	%rsi, %r9
	shrl	$4, %r8d
	addq	%rdi, %rsi
	subq	%rcx, %r9
	andl	$1, %r8d
	addq	%rdi, %r9
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L8:
	testb	%r8b, %r8b
	je	.L21
	cmpb	$46, %al
	je	.L26
.L21:
	addq	$1, %rdi
	movq	%rsi, %rax
	subq	%rdi, %rax
	cmpq	%r9, %rdi
	je	.L7
.L4:
	movzbl	(%rdi), %eax
	testb	%al, %al
	jne	.L8
.L26:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpq	%rsi, %rcx
	jne	.L26
.L9:
	testq	%rsi, %rsi
	je	.L17
	xorl	%eax, %eax
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L28:
	addl	$32, %ecx
	cmpb	$25, %r10b
	jbe	.L14
.L12:
	cmpb	%cl, %r8b
	jne	.L26
.L10:
	addq	$1, %rax
	cmpq	%rsi, %rax
	je	.L17
.L13:
	movzbl	(%rdi,%rax), %ecx
	movzbl	(%rdx,%rax), %r8d
	testb	%cl, %cl
	je	.L26
	cmpb	%r8b, %cl
	je	.L10
	leal	-65(%rcx), %r9d
	leal	-65(%r8), %r10d
	cmpb	$25, %r9b
	jbe	.L28
	cmpb	$25, %r10b
	ja	.L26
.L14:
	addl	$32, %r8d
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%rcx, %rsi
	cmpq	%rcx, %rax
	je	.L9
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L17:
	movl	$1, %eax
	ret
.L3:
	je	.L9
	jmp	.L26
	.cfi_endproc
.LFE1333:
	.size	equal_nocase, .-equal_nocase
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/x509v3/v3_utl.c"
	.text
	.p2align 4
	.type	str_free, @function
str_free:
.LFB1329:
	.cfi_startproc
	endbr64
	movl	$496, %edx
	leaq	.LC0(%rip), %rsi
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE1329:
	.size	str_free, .-str_free
	.p2align 4
	.type	sk_strcmp, @function
sk_strcmp:
.LFB1324:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rsi
	movq	(%rdi), %rdi
	jmp	strcmp@PLT
	.cfi_endproc
.LFE1324:
	.size	sk_strcmp, .-sk_strcmp
	.p2align 4
	.type	strip_spaces, @function
strip_spaces:
.LFB1322:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movsbl	(%rdi), %edi
	testb	%dil, %dil
	jne	.L32
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L34:
	movsbl	1(%r12), %edi
	addq	$1, %r12
	testb	%dil, %dil
	je	.L35
.L32:
	movl	$8, %esi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	jne	.L34
	cmpb	$0, (%r12)
	je	.L35
	movq	%r12, %rdi
	call	strlen@PLT
	leaq	-1(%r12,%rax), %rbx
	cmpq	%r12, %rbx
	jne	.L36
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L39:
	subq	$1, %rbx
	cmpq	%r12, %rbx
	je	.L38
.L36:
	movsbl	(%rbx), %edi
	movl	$8, %esi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	jne	.L39
	cmpq	%r12, %rbx
	je	.L38
	movb	$0, 1(%rbx)
.L38:
	cmpb	$0, (%r12)
	je	.L35
.L37:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1322:
	.size	strip_spaces, .-strip_spaces
	.p2align 4
	.type	equal_case, @function
equal_case:
.LFB1334:
	.cfi_startproc
	endbr64
	movq	%rsi, %r9
	movq	%rdx, %rsi
	testl	$32768, %r8d
	je	.L47
	cmpq	%r9, %rcx
	jnb	.L48
	movq	%r9, %rdx
	shrl	$4, %r8d
	addq	%rdi, %r9
	subq	%rcx, %rdx
	andl	$1, %r8d
	addq	%rdi, %rdx
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L53:
	testb	%r8b, %r8b
	je	.L58
	cmpb	$46, %al
	je	.L65
.L58:
	addq	$1, %rdi
	movq	%r9, %rax
	subq	%rdi, %rax
	cmpq	%rdx, %rdi
	je	.L52
.L49:
	movzbl	(%rdi), %eax
	testb	%al, %al
	jne	.L53
.L65:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	cmpq	%r9, %rcx
	jne	.L65
.L54:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r9, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	memcmp@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore 6
	movq	%rcx, %r9
	cmpq	%rax, %rcx
	je	.L54
	jmp	.L65
.L48:
	je	.L54
	jmp	.L65
	.cfi_endproc
.LFE1334:
	.size	equal_case, .-equal_case
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"xn--"
	.text
	.p2align 4
	.type	equal_wildcard, @function
equal_wildcard:
.LFB1338:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -56(%rbp)
	cmpq	$1, %rcx
	jbe	.L67
	cmpb	$46, (%rdx)
	je	.L68
.L67:
	testq	%r12, %r12
	je	.L68
	movl	%r8d, %eax
	xorl	%ecx, %ecx
	xorl	%r11d, %r11d
	movq	%r9, -88(%rbp)
	andl	$4, %eax
	movq	%r15, -96(%rbp)
	movq	%r9, %r13
	xorl	%r14d, %r14d
	movl	%eax, -68(%rbp)
	leaq	-1(%r12), %rax
	movq	%r12, %r9
	movl	$1, %ebx
	movq	%rax, -80(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%r11, %r12
	movl	%ecx, %r15d
	movl	%r8d, -72(%rbp)
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L69:
	movl	%eax, %edx
	andl	$-33, %edx
	subl	$65, %edx
	cmpb	$25, %dl
	jbe	.L73
	leal	-48(%rax), %edx
	cmpb	$9, %dl
	jbe	.L73
	cmpb	$46, %al
	je	.L137
	cmpb	$45, %al
	jne	.L133
	testb	$1, %bl
	jne	.L133
	orl	$4, %ebx
	.p2align 4,,10
	.p2align 3
.L72:
	addq	$1, %r12
	addq	$1, %r13
	cmpq	%r12, %r9
	je	.L138
.L77:
	movzbl	0(%r13), %eax
	cmpb	$42, %al
	jne	.L69
	movl	%ebx, %edx
	movl	$1, %eax
	andl	$1, %edx
	cmpq	%r12, -80(%rbp)
	je	.L70
	xorl	%eax, %eax
	cmpb	$46, 1(%r13)
	sete	%al
.L70:
	testq	%r14, %r14
	jne	.L133
	movl	%ebx, %edi
	andl	$8, %edi
	orl	%edi, %r15d
	jne	.L133
	movl	-68(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L71
	movl	%edx, %r8d
	movl	%eax, %edi
	xorl	$1, %r8d
	xorl	$1, %edi
	orl	%edi, %r8d
	jne	.L133
.L71:
	orl	%eax, %edx
	je	.L133
	addq	$1, %r12
	movq	%r13, %r14
	andl	$-2, %ebx
	addq	$1, %r13
	cmpq	%r12, %r9
	jne	.L77
.L138:
	movl	%r15d, %ecx
	testq	%r14, %r14
	movq	%r9, %r12
	movq	-96(%rbp), %r15
	sete	%dl
	cmpl	$1, %ecx
	movq	-88(%rbp), %r9
	movl	-72(%rbp), %r8d
	setle	%al
	orb	%al, %dl
	jne	.L68
	andl	$5, %ebx
	jne	.L68
	addq	%r9, %r12
	movq	%r14, %r11
	movq	-56(%rbp), %rbx
	xorl	%eax, %eax
	subq	%r14, %r12
	subq	%r9, %r11
	subq	$1, %r12
	leaq	(%r12,%r11), %rdx
	cmpq	%rdx, %rbx
	jb	.L66
	movq	%r11, %rcx
	movq	%r15, %rdx
	movq	%r11, %rsi
	movq	%r9, %rdi
	movl	%r8d, -64(%rbp)
	call	equal_nocase
	testl	%eax, %eax
	je	.L66
	subq	%r12, %rbx
	movl	-64(%rbp), %r8d
	leaq	1(%r14), %rdx
	movq	%r12, %rcx
	addq	%r15, %rbx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	equal_nocase
	testl	%eax, %eax
	je	.L66
	leaq	(%r15,%r11), %r12
	testq	%r11, %r11
	jne	.L83
	cmpb	$46, 1(%r14)
	jne	.L83
	cmpq	%rbx, %r12
	je	.L94
	movl	-64(%rbp), %r8d
	addq	$1, %r15
	shrl	$3, %r8d
	andl	$1, %r8d
	cmpq	%r15, %rbx
	je	.L139
.L85:
	xorl	$1, %r8d
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L88:
	addq	$1, %r12
	cmpq	%r12, %rbx
	je	.L136
.L89:
	movzbl	(%r12), %eax
	movl	%eax, %edx
	andl	$-33, %edx
	subl	$65, %edx
	cmpb	$25, %dl
	jbe	.L88
	leal	-48(%rax), %edx
	cmpb	$9, %dl
	jbe	.L88
	cmpb	$45, %al
	je	.L88
	testb	%r8b, %r8b
	jne	.L94
	cmpb	$46, %al
	je	.L88
.L94:
	xorl	%eax, %eax
.L66:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
	andl	$5, %ebx
	jne	.L133
	addl	$1, %r15d
	movl	$1, %ebx
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L73:
	testb	$1, %bl
	je	.L75
	movq	%r9, %rax
	subq	%r12, %rax
	cmpq	$3, %rax
	jbe	.L75
	movl	$4, %edx
	movq	%r13, %rdi
	movq	%r9, -64(%rbp)
	call	strncasecmp@PLT
	movq	-64(%rbp), %r9
	leaq	.LC1(%rip), %rsi
	movl	%eax, %r10d
	movl	%ebx, %eax
	orl	$8, %eax
	testl	%r10d, %r10d
	cmove	%eax, %ebx
.L75:
	andl	$-6, %ebx
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L133:
	movq	%r9, %r12
	movq	-96(%rbp), %r15
	movq	-88(%rbp), %r9
	movl	-72(%rbp), %r8d
.L68:
	movq	-56(%rbp), %rcx
	addq	$56, %rsp
	movq	%r15, %rdx
	movq	%r12, %rsi
	popq	%rbx
	movq	%r9, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	equal_nocase
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	cmpq	$3, -56(%rbp)
	jbe	.L86
	movl	$4, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	strncasecmp@PLT
	testl	%eax, %eax
	je	.L66
.L86:
	leaq	1(%r12), %rax
	cmpq	%rax, %rbx
	je	.L140
.L87:
	xorl	%r8d, %r8d
	cmpq	%rbx, %r12
	jne	.L85
.L136:
	addq	$56, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	cmpb	$42, (%r12)
	movl	$1, %eax
	jne	.L87
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L139:
	cmpb	$42, (%r12)
	movl	$1, %eax
	jne	.L85
	jmp	.L66
	.cfi_endproc
.LFE1338:
	.size	equal_wildcard, .-equal_wildcard
	.section	.rodata.str1.1
.LC2:
	.string	"%d.%d.%d.%d"
	.text
	.p2align 4
	.type	ipv6_cb, @function
ipv6_cb:
.LFB1350:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movslq	16(%rdx), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$16, %r15d
	je	.L150
	movq	%rdx, %rbx
	testl	%esi, %esi
	jne	.L144
	movl	20(%rdx), %eax
	cmpl	$-1, %eax
	je	.L155
	cmpl	%eax, %r15d
	je	.L146
.L150:
	xorl	%eax, %eax
.L141:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L156
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	movq	%rdi, %r12
	cmpl	$4, %esi
	jle	.L148
	cmpl	$12, %r15d
	jg	.L150
	movslq	%esi, %rsi
	cmpb	$0, (%rdi,%rsi)
	jne	.L150
	xorl	%eax, %eax
	leaq	-68(%rbp), %rcx
	leaq	-72(%rbp), %rdx
	leaq	-60(%rbp), %r9
	leaq	-64(%rbp), %r8
	leaq	.LC2(%rip), %rsi
	call	__isoc99_sscanf@PLT
	cmpl	$4, %eax
	jne	.L150
	movl	-72(%rbp), %eax
	cmpl	$255, %eax
	ja	.L150
	movl	-68(%rbp), %edx
	cmpl	$255, %edx
	ja	.L150
	movl	-64(%rbp), %ecx
	cmpl	$255, %ecx
	ja	.L150
	movl	-60(%rbp), %esi
	cmpl	$255, %esi
	ja	.L150
	addq	%rbx, %r15
	movb	%al, (%r15)
	movl	$1, %eax
	movb	%dl, 1(%r15)
	movb	%cl, 2(%r15)
	movb	%sil, 3(%r15)
	addl	$4, 16(%rbx)
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L155:
	movl	%r15d, 20(%rdx)
.L146:
	addl	$1, 24(%rbx)
	movl	$1, %eax
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L148:
	leal	-1(%rsi), %eax
	leaq	1(%rdi,%rax), %r14
	xorl	%eax, %eax
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L158:
	movsbl	%al, %eax
	orl	%r13d, %eax
	cmpq	%r14, %r12
	je	.L157
.L151:
	movzbl	(%r12), %edi
	sall	$4, %eax
	addq	$1, %r12
	movl	%eax, %r13d
	call	OPENSSL_hexchar2int@PLT
	testl	%eax, %eax
	jns	.L158
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L157:
	rolw	$8, %ax
	movw	%ax, (%rbx,%r15)
	movl	$1, %eax
	addl	$2, 16(%rbx)
	jmp	.L141
.L156:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1350:
	.size	ipv6_cb, .-ipv6_cb
	.p2align 4
	.globl	X509V3_conf_free
	.type	X509V3_conf_free, @function
X509V3_conf_free:
.LFB1311:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L159
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$82, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	call	CRYPTO_free@PLT
	movq	16(%r12), %rdi
	movl	$83, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	(%r12), %rdi
	movl	$84, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$85, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L159:
	ret
	.cfi_endproc
.LFE1311:
	.size	X509V3_conf_free, .-X509V3_conf_free
	.section	.rodata.str1.1
.LC3:
	.string	"-0x"
.LC4:
	.string	"0x"
	.text
	.p2align 4
	.type	bignum_to_string.part.0, @function
bignum_to_string.part.0:
.LFB1354:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	call	BN_bn2hex@PLT
	testq	%rax, %rax
	je	.L169
	movq	%rax, %rdi
	movq	%rax, %r12
	call	strlen@PLT
	movl	$122, %edx
	leaq	.LC0(%rip), %rsi
	leaq	3(%rax), %r14
	movq	%r14, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L171
	cmpb	$45, (%r12)
	movq	%r14, %rdx
	je	.L172
	leaq	.LC4(%rip), %rsi
	movq	%rax, %rdi
	call	OPENSSL_strlcpy@PLT
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	OPENSSL_strlcat@PLT
.L168:
	movl	$137, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
.L164:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	.cfi_restore_state
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	call	OPENSSL_strlcpy@PLT
	leaq	1(%r12), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	OPENSSL_strlcat@PLT
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L169:
	addq	$8, %rsp
	xorl	%r13d, %r13d
	popq	%r12
	movq	%r13, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	movl	$124, %r8d
	movl	$65, %edx
	movl	$167, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$125, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	jmp	.L164
	.cfi_endproc
.LFE1354:
	.size	bignum_to_string.part.0, .-bignum_to_string.part.0
	.p2align 4
	.type	append_ia5, @function
append_ia5:
.LFB1330:
	.cfi_startproc
	cmpl	$22, 4(%rsi)
	je	.L174
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L174:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	8(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L176
	movl	(%rbx), %eax
	testl	%eax, %eax
	je	.L176
	movq	%rdi, %r12
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L190
.L177:
	call	OPENSSL_sk_find@PLT
	cmpl	$-1, %eax
	je	.L191
.L176:
	movl	$1, %eax
.L173:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L191:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movl	$514, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L179
	movq	(%r12), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L176
.L179:
	movl	$516, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	movq	(%r12), %rdi
	leaq	str_free(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	$0, (%r12)
	xorl	%eax, %eax
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L190:
	leaq	sk_strcmp(%rip), %rdi
	call	OPENSSL_sk_new@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L192
	xorl	%eax, %eax
	jmp	.L173
.L192:
	movq	8(%rbx), %rsi
	jmp	.L177
	.cfi_endproc
.LFE1330:
	.size	append_ia5, .-append_ia5
	.p2align 4
	.type	do_x509_check, @function
do_x509_check:
.LFB1340:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$88, %rsp
	movq	%rdi, -112(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%r9, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%ecx, %eax
	andb	$127, %ah
	movl	%eax, -76(%rbp)
	cmpl	$1, %r8d
	je	.L222
	leaq	equal_case(%rip), %rax
	movl	$4, -80(%rbp)
	xorl	%r14d, %r14d
	movq	%rax, -104(%rbp)
	cmpl	$2, %r8d
	je	.L258
.L194:
	cmpq	$0, -72(%rbp)
	jne	.L197
	movq	-88(%rbp), %rdi
	call	strlen@PLT
	movq	%rax, -72(%rbp)
.L197:
	movq	-112(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$85, %esi
	call	X509_get_ext_d2i@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L198
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	cmpl	$22, -80(%rbp)
	je	.L199
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r15d
	jge	.L200
	.p2align 4,,10
	.p2align 3
.L260:
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	cmpl	(%rax), %ebx
	jne	.L201
	movq	8(%rax), %rax
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L227
	movl	(%rax), %r10d
	testl	%r10d, %r10d
	je	.L227
	movl	-80(%rbp), %ecx
	movl	$1, %r13d
	cmpl	4(%rax), %ecx
	jne	.L201
	cmpl	-72(%rbp), %r10d
	je	.L259
	.p2align 4,,10
	.p2align 3
.L201:
	addl	$1, %r15d
.L264:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r15d
	jl	.L260
.L200:
	movq	%r12, %rdi
	call	GENERAL_NAMES_free@PLT
	testl	%r13d, %r13d
	je	.L198
	testb	$1, -76(%rbp)
	je	.L204
.L198:
	testl	%r14d, %r14d
	je	.L204
	testb	$32, -76(%rbp)
	jne	.L204
	movq	-112(%rbp), %rdi
	movl	$-1, %r15d
	leaq	-64(%rbp), %r13
	call	X509_get_subject_name@PLT
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L214:
	movl	%r15d, %edx
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	X509_NAME_get_index_by_NID@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	js	.L204
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	X509_NAME_get_entry@PLT
	movq	%rax, %rdi
	call	X509_NAME_ENTRY_get_data@PLT
	cmpq	$0, 8(%rax)
	movq	%rax, %rsi
	je	.L214
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L214
	movq	%r13, %rdi
	call	ASN1_STRING_to_UTF8@PLT
	testl	%eax, %eax
	js	.L261
	movslq	%eax, %r12
	movl	-76(%rbp), %r8d
	movq	-72(%rbp), %rcx
	movq	-88(%rbp), %rdx
	movq	-64(%rbp), %rdi
	movq	%r12, %rsi
	movq	-104(%rbp), %rax
	call	*%rax
	testl	%eax, %eax
	jle	.L218
	cmpq	$0, -96(%rbp)
	je	.L218
	movq	-64(%rbp), %rdi
	movl	$812, %ecx
	movq	%r12, %rsi
	movl	%eax, -72(%rbp)
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_strndup@PLT
	movq	-96(%rbp), %rdx
	movq	-64(%rbp), %rdi
	leaq	.LC0(%rip), %rsi
	movq	%rax, (%rdx)
	movl	$813, %edx
	call	CRYPTO_free@PLT
	movl	-72(%rbp), %r8d
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L263:
	movl	-76(%rbp), %r8d
	movslq	%eax, %rsi
	movq	-72(%rbp), %rcx
	movq	-88(%rbp), %rdx
	movq	-104(%rbp), %rax
	call	*%rax
	movl	%eax, %r8d
	testl	%eax, %eax
	jle	.L209
	cmpq	$0, -96(%rbp)
	jne	.L262
.L209:
	testl	%r8d, %r8d
	jne	.L211
.L208:
	movl	$1, %r13d
.L206:
	addl	$1, %r15d
.L199:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r15d
	jge	.L200
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	cmpl	%ebx, (%rax)
	jne	.L206
	movq	8(%rax), %r13
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L208
	movl	0(%r13), %eax
	testl	%eax, %eax
	je	.L208
	cmpl	$22, 4(%r13)
	jne	.L208
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L227:
	movl	$1, %r13d
	addl	$1, %r15d
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L204:
	xorl	%r8d, %r8d
.L193:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L265
	addq	$88, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L258:
	.cfi_restore_state
	cmpq	$1, %rdx
	jbe	.L195
	movl	-76(%rbp), %ecx
	leaq	equal_wildcard(%rip), %rdx
	movl	$22, -80(%rbp)
	movl	$13, %r14d
	movl	%ecx, %eax
	orb	$-128, %ah
	cmpb	$46, (%rsi)
	cmovne	%ecx, %eax
	testb	$2, %al
	movl	%eax, -76(%rbp)
	leaq	equal_nocase(%rip), %rax
	cmove	%rdx, %rax
	movq	%rax, -104(%rbp)
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L218:
	movq	-64(%rbp), %rdi
	movl	$813, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, -80(%rbp)
	call	CRYPTO_free@PLT
	movl	-80(%rbp), %eax
	testl	%eax, %eax
	je	.L214
	movl	%eax, %r8d
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L222:
	leaq	equal_email(%rip), %rax
	movl	$22, -80(%rbp)
	movl	$48, %r14d
	movq	%rax, -104(%rbp)
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L259:
	movq	-72(%rbp), %rdx
	movq	-88(%rbp), %rsi
	movl	%r10d, -124(%rbp)
	movq	%rdi, -120(%rbp)
	call	memcmp@PLT
	movq	-120(%rbp), %rdi
	movl	-124(%rbp), %r10d
	testl	%eax, %eax
	jne	.L201
	cmpq	$0, -96(%rbp)
	movl	$1, %r8d
	jne	.L210
	.p2align 4,,10
	.p2align 3
.L211:
	movq	%r12, %rdi
	movl	%r8d, -72(%rbp)
	call	GENERAL_NAMES_free@PLT
	movl	-72(%rbp), %r8d
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L195:
	andl	$2, %ecx
	leaq	equal_wildcard(%rip), %rdx
	leaq	equal_nocase(%rip), %rax
	movl	$22, -80(%rbp)
	cmove	%rdx, %rax
	movl	$13, %r14d
	movq	%rax, -104(%rbp)
	jmp	.L194
.L262:
	movl	0(%r13), %r10d
	movq	8(%r13), %rdi
.L210:
	movl	$798, %ecx
	movslq	%r10d, %rsi
	leaq	.LC0(%rip), %rdx
	movl	%r8d, -72(%rbp)
	call	CRYPTO_strndup@PLT
	movq	-96(%rbp), %rcx
	movl	-72(%rbp), %r8d
	movq	%rax, (%rcx)
	jmp	.L211
.L261:
	movl	$-1, %r8d
	jmp	.L193
.L265:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1340:
	.size	do_x509_check, .-do_x509_check
	.p2align 4
	.type	get_email, @function
get_email:
.LFB1328:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	.LC0(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	$-1, %r12d
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L267:
	movl	%r12d, %edx
	movl	$48, %esi
	movq	%r13, %rdi
	call	X509_NAME_get_index_by_NID@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	js	.L291
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	X509_NAME_get_entry@PLT
	movq	%rax, %rdi
	call	X509_NAME_ENTRY_get_data@PLT
	cmpl	$22, 4(%rax)
	movq	%rax, %rbx
	jne	.L267
	movq	8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L267
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L267
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L292
.L269:
	call	OPENSSL_sk_find@PLT
	cmpl	$-1, %eax
	jne	.L267
	movq	8(%rbx), %rdi
	movl	$514, %edx
	movq	%r15, %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L272
	movq	-64(%rbp), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L267
.L272:
	leaq	.LC0(%rip), %rsi
	movq	%rbx, %rdi
	movl	$516, %edx
	call	CRYPTO_free@PLT
	movq	-64(%rbp), %rdi
	leaq	str_free(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
.L290:
	xorl	%eax, %eax
.L266:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L293
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L292:
	.cfi_restore_state
	leaq	sk_strcmp(%rip), %rdi
	call	OPENSSL_sk_new@PLT
	movq	%rax, -64(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L290
	movq	8(%rbx), %rsi
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L291:
	xorl	%ebx, %ebx
	leaq	-64(%rbp), %r12
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L277:
	addl	$1, %ebx
.L274:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L294
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	cmpl	$1, (%rax)
	jne	.L277
	movq	8(%rax), %rsi
	movq	%r12, %rdi
	call	append_ia5
	testl	%eax, %eax
	jne	.L277
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L294:
	movq	-64(%rbp), %rax
	jmp	.L266
.L293:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1328:
	.size	get_email, .-get_email
	.p2align 4
	.type	equal_email, @function
equal_email:
.LFB1335:
	.cfi_startproc
	endbr64
	movq	%rsi, %rax
	xorl	%r8d, %r8d
	cmpq	%rcx, %rsi
	jne	.L316
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rsi
	movq	%rax, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L304:
	subq	$1, %rdx
	cmpb	$64, (%rdi,%rdx)
	leaq	(%rdi,%rdx), %r10
	leaq	(%rsi,%rdx), %r11
	je	.L298
	cmpb	$64, (%rsi,%rdx)
	je	.L298
.L297:
	testq	%rdx, %rdx
	jne	.L304
	movq	%rax, %rdx
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L316:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L298:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	subq	%rdx, %rax
	movq	%rax, %r8
	je	.L299
	xorl	%eax, %eax
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L320:
	addl	$32, %ecx
	cmpb	$25, %bl
	jbe	.L306
.L302:
	cmpb	%cl, %r9b
	jne	.L319
.L300:
	addq	$1, %rax
	cmpq	%r8, %rax
	je	.L299
.L303:
	movzbl	(%r10,%rax), %ecx
	movzbl	(%r11,%rax), %r9d
	testb	%cl, %cl
	je	.L319
	cmpb	%r9b, %cl
	je	.L300
	leal	-65(%rcx), %r12d
	leal	-65(%r9), %ebx
	cmpb	$25, %r12b
	jbe	.L320
	cmpb	$25, %bl
	ja	.L319
.L306:
	addl	$32, %r9d
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L299:
	testq	%rdx, %rdx
	cmove	%r8, %rdx
.L305:
	call	memcmp@PLT
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	testl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	sete	%r8b
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L319:
	.cfi_restore_state
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movl	%r8d, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1335:
	.size	equal_email, .-equal_email
	.p2align 4
	.globl	X509V3_add_value
	.type	X509V3_add_value, @function
X509V3_add_value:
.LFB1309:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$8, %rsp
	movq	(%rdx), %r15
	testq	%rdi, %rdi
	je	.L322
	movl	$44, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L334
.L322:
	testq	%r12, %r12
	je	.L324
	movq	%r12, %rdi
	movl	$46, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L323
.L324:
	movl	$48, %edx
	leaq	.LC0(%rip), %rsi
	movl	$24, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L323
	testq	%r15, %r15
	je	.L350
	movq	$0, (%rax)
	movq	(%rbx), %rdi
	movq	%rax, %rsi
	movq	%r13, 8(%rax)
	movq	%r12, 16(%rax)
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L351
.L333:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L334:
	.cfi_restore_state
	xorl	%r12d, %r12d
.L323:
	movl	$59, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r14d, %r14d
	movl	$105, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	testq	%r15, %r15
	je	.L329
.L328:
	movq	%r14, %rdi
	movl	$64, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r13, %rdi
	movl	$65, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	movl	$66, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L350:
	.cfi_restore_state
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L330
	movq	$0, (%r14)
	movq	%r14, %rsi
	movq	%r13, 8(%r14)
	movq	%r12, 16(%r14)
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L333
.L330:
	movl	$59, %r8d
	movl	$65, %edx
	movl	$105, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L329:
	movq	(%rbx), %rdi
	call	OPENSSL_sk_free@PLT
	movq	$0, (%rbx)
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L351:
	movl	$59, %r8d
	movl	$65, %edx
	movl	$105, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L328
	.cfi_endproc
.LFE1309:
	.size	X509V3_add_value, .-X509V3_add_value
	.p2align 4
	.globl	X509V3_add_value_uchar
	.type	X509V3_add_value_uchar, @function
X509V3_add_value_uchar:
.LFB1310:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$8, %rsp
	movq	(%rdx), %r15
	testq	%rdi, %rdi
	je	.L353
	movl	$44, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L365
.L353:
	testq	%r12, %r12
	je	.L355
	movq	%r12, %rdi
	movl	$46, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L354
.L355:
	movl	$48, %edx
	leaq	.LC0(%rip), %rsi
	movl	$24, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L354
	testq	%r15, %r15
	je	.L381
	movq	$0, (%rax)
	movq	(%rbx), %rdi
	movq	%rax, %rsi
	movq	%r13, 8(%rax)
	movq	%r12, 16(%rax)
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L382
.L364:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L365:
	.cfi_restore_state
	xorl	%r12d, %r12d
.L354:
	movl	$59, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r14d, %r14d
	movl	$105, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	testq	%r15, %r15
	je	.L360
.L359:
	movq	%r14, %rdi
	movl	$64, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r13, %rdi
	movl	$65, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	movl	$66, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L381:
	.cfi_restore_state
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L361
	movq	$0, (%r14)
	movq	%r14, %rsi
	movq	%r13, 8(%r14)
	movq	%r12, 16(%r14)
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L364
.L361:
	movl	$59, %r8d
	movl	$65, %edx
	movl	$105, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L360:
	movq	(%rbx), %rdi
	call	OPENSSL_sk_free@PLT
	movq	$0, (%rbx)
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L382:
	movl	$59, %r8d
	movl	$65, %edx
	movl	$105, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L359
	.cfi_endproc
.LFE1310:
	.size	X509V3_add_value_uchar, .-X509V3_add_value_uchar
	.section	.rodata.str1.1
.LC5:
	.string	"TRUE"
.LC6:
	.string	"FALSE"
	.text
	.p2align 4
	.globl	X509V3_add_value_bool
	.type	X509V3_add_value_bool, @function
X509V3_add_value_bool:
.LFB1312:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	testl	%esi, %esi
	jne	.L409
	movq	(%rdx), %rbx
	testq	%rdi, %rdi
	je	.L385
	movl	$44, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L396
.L385:
	movl	$46, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	call	CRYPTO_strdup@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L386
	movl	$48, %edx
	leaq	.LC0(%rip), %rsi
	movl	$24, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L386
	testq	%rbx, %rbx
	je	.L410
	movq	$0, (%rax)
	movq	0(%r13), %rdi
	movq	%rax, %rsi
	movq	%r12, 8(%rax)
	movq	%r15, 16(%rax)
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L411
.L395:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L396:
	.cfi_restore_state
	xorl	%r15d, %r15d
.L386:
	movl	$59, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r14d, %r14d
	movl	$105, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	testq	%rbx, %rbx
	je	.L391
.L390:
	movq	%r14, %rdi
	movl	$64, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	movl	$65, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r15, %rdi
	movl	$66, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L409:
	.cfi_restore_state
	addq	$8, %rsp
	leaq	.LC5(%rip), %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	X509V3_add_value
	.p2align 4,,10
	.p2align 3
.L411:
	.cfi_restore_state
	movl	$59, %r8d
	movl	$65, %edx
	movl	$105, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L410:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 0(%r13)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L392
	movq	$0, (%r14)
	movq	%r14, %rsi
	movq	%r12, 8(%r14)
	movq	%r15, 16(%r14)
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L395
.L392:
	movl	$59, %r8d
	movl	$65, %edx
	movl	$105, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L391:
	movq	0(%r13), %rdi
	call	OPENSSL_sk_free@PLT
	movq	$0, 0(%r13)
	jmp	.L390
	.cfi_endproc
.LFE1312:
	.size	X509V3_add_value_bool, .-X509V3_add_value_bool
	.p2align 4
	.globl	X509V3_add_value_bool_nf
	.type	X509V3_add_value_bool_nf, @function
X509V3_add_value_bool_nf:
.LFB1313:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	jne	.L413
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L413:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$8, %rsp
	movq	(%rdx), %r15
	testq	%rdi, %rdi
	je	.L415
	movl	$44, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L425
.L415:
	movl	$46, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	call	CRYPTO_strdup@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L416
	movl	$48, %edx
	leaq	.LC0(%rip), %rsi
	movl	$24, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L416
	testq	%r15, %r15
	je	.L440
	movq	$0, (%rax)
	movq	(%rbx), %rdi
	movq	%rax, %rsi
	movq	%r12, 8(%rax)
	movq	%r14, 16(%rax)
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L441
.L423:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L425:
	.cfi_restore_state
	xorl	%r14d, %r14d
.L416:
	movl	$59, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$105, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	testq	%r15, %r15
	je	.L421
.L420:
	movq	%r13, %rdi
	movl	$64, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	movl	$65, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r14, %rdi
	movl	$66, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L440:
	.cfi_restore_state
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L422
	movq	$0, 0(%r13)
	movq	%r13, %rsi
	movq	%r12, 8(%r13)
	movq	%r14, 16(%r13)
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L423
.L422:
	movl	$59, %r8d
	movl	$65, %edx
	movl	$105, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L421:
	movq	(%rbx), %rdi
	call	OPENSSL_sk_free@PLT
	movq	$0, (%rbx)
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L441:
	movl	$59, %r8d
	movl	$65, %edx
	movl	$105, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L420
	.cfi_endproc
.LFE1313:
	.size	X509V3_add_value_bool_nf, .-X509V3_add_value_bool_nf
	.p2align 4
	.globl	i2s_ASN1_ENUMERATED
	.type	i2s_ASN1_ENUMERATED, @function
i2s_ASN1_ENUMERATED:
.LFB1315:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	testq	%rsi, %rsi
	je	.L449
	movq	%rsi, %rdi
	xorl	%esi, %esi
	call	ASN1_ENUMERATED_to_BN@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L448
	movq	%rax, %rdi
	call	BN_num_bits@PLT
	movq	%r12, %rdi
	cmpl	$127, %eax
	jle	.L454
	call	bignum_to_string.part.0
	movq	%rax, %r13
	testq	%r13, %r13
	je	.L448
.L445:
	movq	%r12, %rdi
	call	BN_free@PLT
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L454:
	.cfi_restore_state
	call	BN_bn2dec@PLT
	movq	%rax, %r13
	testq	%r13, %r13
	jne	.L445
	.p2align 4,,10
	.p2align 3
.L448:
	movl	$150, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$121, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L449:
	xorl	%r13d, %r13d
	popq	%r12
	movq	%r13, %rax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1315:
	.size	i2s_ASN1_ENUMERATED, .-i2s_ASN1_ENUMERATED
	.p2align 4
	.globl	i2s_ASN1_INTEGER
	.type	i2s_ASN1_INTEGER, @function
i2s_ASN1_INTEGER:
.LFB1316:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	testq	%rsi, %rsi
	je	.L462
	movq	%rsi, %rdi
	xorl	%esi, %esi
	call	ASN1_INTEGER_to_BN@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L461
	movq	%rax, %rdi
	call	BN_num_bits@PLT
	movq	%r12, %rdi
	cmpl	$127, %eax
	jle	.L467
	call	bignum_to_string.part.0
	movq	%rax, %r13
	testq	%r13, %r13
	je	.L461
.L458:
	movq	%r12, %rdi
	call	BN_free@PLT
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L467:
	.cfi_restore_state
	call	BN_bn2dec@PLT
	movq	%rax, %r13
	testq	%r13, %r13
	jne	.L458
	.p2align 4,,10
	.p2align 3
.L461:
	movl	$164, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$120, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L462:
	xorl	%r13d, %r13d
	popq	%r12
	movq	%r13, %rax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1316:
	.size	i2s_ASN1_INTEGER, .-i2s_ASN1_INTEGER
	.p2align 4
	.globl	s2i_ASN1_INTEGER
	.type	s2i_ASN1_INTEGER, @function
s2i_ASN1_INTEGER:
.LFB1317:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$0, -32(%rbp)
	testq	%rsi, %rsi
	je	.L497
	movq	%rsi, %rbx
	call	BN_new@PLT
	movq	%rax, -32(%rbp)
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L498
	movzbl	(%rbx), %eax
	xorl	%r12d, %r12d
	cmpb	$45, %al
	jne	.L472
	movzbl	1(%rbx), %eax
	movl	$1, %r12d
	addq	$1, %rbx
.L472:
	cmpb	$48, %al
	jne	.L473
	movzbl	1(%rbx), %eax
	andl	$-33, %eax
	cmpb	$88, %al
	je	.L499
.L473:
	leaq	-32(%rbp), %rdi
	movq	%rbx, %rsi
	call	BN_dec2bn@PLT
.L474:
	movq	-32(%rbp), %rdi
	testl	%eax, %eax
	je	.L475
	cltq
	cmpb	$0, (%rbx,%rax)
	jne	.L475
	testl	%r12d, %r12d
	jne	.L500
.L477:
	xorl	%esi, %esi
	call	BN_to_ASN1_INTEGER@PLT
	movq	-32(%rbp), %rdi
	movq	%rax, %r12
	call	BN_free@PLT
	testq	%r12, %r12
	je	.L479
.L468:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L501
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L479:
	.cfi_restore_state
	movl	$213, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$101, %edx
	xorl	%r12d, %r12d
	movl	$108, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L500:
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L502
	movq	-32(%rbp), %rdi
	xorl	%esi, %esi
	call	BN_to_ASN1_INTEGER@PLT
	movq	-32(%rbp), %rdi
	movq	%rax, %r12
	call	BN_free@PLT
	testq	%r12, %r12
	je	.L479
	orl	$256, 4(%r12)
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L475:
	call	BN_free@PLT
	movl	$203, %r8d
	movl	$100, %edx
	xorl	%r12d, %r12d
	leaq	.LC0(%rip), %rcx
	movl	$108, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L499:
	addq	$2, %rbx
	leaq	-32(%rbp), %rdi
	movq	%rbx, %rsi
	call	BN_hex2bn@PLT
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L502:
	movq	-32(%rbp), %rdi
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L497:
	movl	$176, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$109, %edx
	xorl	%r12d, %r12d
	movl	$108, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L498:
	movl	$181, %r8d
	movl	$65, %edx
	movl	$108, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L468
.L501:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1317:
	.size	s2i_ASN1_INTEGER, .-s2i_ASN1_INTEGER
	.p2align 4
	.globl	X509V3_add_value_int
	.type	X509V3_add_value_int, @function
X509V3_add_value_int:
.LFB1318:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	$1, %r12d
	testq	%rsi, %rsi
	je	.L503
	movq	%rdi, %r13
	movq	%rsi, %rdi
	xorl	%esi, %esi
	movq	%rdx, %r14
	call	ASN1_INTEGER_to_BN@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L508
	movq	%rax, %rdi
	call	BN_num_bits@PLT
	movq	%r12, %rdi
	cmpl	$127, %eax
	jle	.L516
	call	bignum_to_string.part.0
	movq	%rax, %r15
.L507:
	testq	%r15, %r15
	je	.L508
	movq	%r12, %rdi
	call	BN_free@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	X509V3_add_value
	movl	$233, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	movl	%eax, %r12d
	call	CRYPTO_free@PLT
.L503:
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L508:
	.cfi_restore_state
	movl	$164, %r8d
	movl	$65, %edx
	movl	$120, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	BN_free@PLT
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L516:
	.cfi_restore_state
	call	BN_bn2dec@PLT
	movq	%rax, %r15
	jmp	.L507
	.cfi_endproc
.LFE1318:
	.size	X509V3_add_value_int, .-X509V3_add_value_int
	.section	.rodata.str1.1
.LC7:
	.string	"true"
.LC8:
	.string	"YES"
.LC9:
	.string	"yes"
.LC10:
	.string	"false"
.LC11:
	.string	",value:"
.LC12:
	.string	",name:"
.LC13:
	.string	"section:"
	.text
	.p2align 4
	.globl	X509V3_get_value_bool
	.type	X509V3_get_value_bool, @function
X509V3_get_value_bool:
.LFB1319:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L518
	movq	%rsi, %r8
	movl	$5, %ecx
	leaq	.LC5(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L519
	movl	$5, %ecx
	leaq	.LC7(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L519
	movzbl	(%rax), %edx
	cmpl	$89, %edx
	je	.L568
.L529:
	cmpl	$121, %edx
	je	.L569
.L530:
	movl	$4, %ecx
	leaq	.LC8(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L519
	movl	$4, %ecx
	leaq	.LC9(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L519
	movl	$6, %ecx
	leaq	.LC6(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L524
	movl	$6, %ecx
	leaq	.LC10(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L524
	cmpl	$78, %edx
	jne	.L531
	cmpb	$0, 1(%rax)
	je	.L524
.L531:
	cmpl	$110, %edx
	jne	.L532
	cmpb	$0, 1(%rax)
	je	.L524
.L532:
	movzbl	(%rax), %edx
	cmpl	$78, %edx
	jne	.L533
	cmpb	$79, 1(%rax)
	jne	.L533
	cmpb	$0, 2(%rax)
	je	.L524
.L533:
	cmpl	$110, %edx
	jne	.L518
	cmpb	$111, 1(%rax)
	jne	.L518
	cmpb	$0, 2(%rax)
	jne	.L518
	.p2align 4,,10
	.p2align 3
.L524:
	movl	$0, (%r8)
	movl	$1, %eax
	jmp	.L517
	.p2align 4,,10
	.p2align 3
.L569:
	cmpb	$0, 1(%rax)
	jne	.L530
	.p2align 4,,10
	.p2align 3
.L519:
	movl	$255, (%r8)
	movl	$1, %eax
.L517:
	movq	-8(%rbp), %rbx
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L568:
	.cfi_restore_state
	cmpb	$0, 1(%rax)
	je	.L519
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L518:
	movl	$262, %r8d
	movl	$104, %edx
	movl	$110, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	subq	$8, %rsp
	pushq	16(%rbx)
	movq	(%rbx), %rdx
	movq	8(%rbx), %r8
	xorl	%eax, %eax
	leaq	.LC11(%rip), %r9
	leaq	.LC12(%rip), %rcx
	leaq	.LC13(%rip), %rsi
	movl	$6, %edi
	call	ERR_add_error_data@PLT
	popq	%rax
	movq	-8(%rbp), %rbx
	xorl	%eax, %eax
	popq	%rdx
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1319:
	.size	X509V3_get_value_bool, .-X509V3_get_value_bool
	.p2align 4
	.globl	X509V3_get_value_int
	.type	X509V3_get_value_int, @function
X509V3_get_value_int:
.LFB1320:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	16(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	testq	%rbx, %rbx
	je	.L598
	movq	%rsi, %r13
	call	BN_new@PLT
	movq	%rax, -48(%rbp)
	testq	%rax, %rax
	je	.L599
	movzbl	(%rbx), %eax
	xorl	%r14d, %r14d
	cmpb	$45, %al
	jne	.L574
	movzbl	1(%rbx), %eax
	movl	$1, %r14d
	addq	$1, %rbx
.L574:
	cmpb	$48, %al
	jne	.L575
	movzbl	1(%rbx), %eax
	andl	$-33, %eax
	cmpb	$88, %al
	je	.L600
.L575:
	leaq	-48(%rbp), %rdi
	movq	%rbx, %rsi
	call	BN_dec2bn@PLT
.L576:
	movq	-48(%rbp), %rdi
	testl	%eax, %eax
	je	.L577
	cltq
	cmpb	$0, (%rbx,%rax)
	jne	.L577
	testl	%r14d, %r14d
	jne	.L601
.L579:
	xorl	%esi, %esi
	call	BN_to_ASN1_INTEGER@PLT
	movq	-48(%rbp), %rdi
	movq	%rax, %rbx
	call	BN_free@PLT
	testq	%rbx, %rbx
	je	.L583
.L581:
	movq	%rbx, 0(%r13)
	movl	$1, %eax
.L570:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L602
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L601:
	.cfi_restore_state
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L603
	movq	-48(%rbp), %rdi
	xorl	%esi, %esi
	call	BN_to_ASN1_INTEGER@PLT
	movq	-48(%rbp), %rdi
	movq	%rax, %rbx
	call	BN_free@PLT
	testq	%rbx, %rbx
	je	.L583
	orl	$256, 4(%rbx)
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L577:
	call	BN_free@PLT
	movl	$203, %r8d
	movl	$100, %edx
	leaq	.LC0(%rip), %rcx
	movl	$108, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
.L572:
	subq	$8, %rsp
	pushq	16(%r12)
	movq	(%r12), %rdx
	xorl	%eax, %eax
	movq	8(%r12), %r8
	leaq	.LC11(%rip), %r9
	leaq	.LC12(%rip), %rcx
	movl	$6, %edi
	leaq	.LC13(%rip), %rsi
	call	ERR_add_error_data@PLT
	popq	%rax
	xorl	%eax, %eax
	popq	%rdx
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L600:
	addq	$2, %rbx
	leaq	-48(%rbp), %rdi
	movq	%rbx, %rsi
	call	BN_hex2bn@PLT
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L603:
	movq	-48(%rbp), %rdi
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L598:
	movl	$176, %r8d
	movl	$109, %edx
	movl	$108, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L572
	.p2align 4,,10
	.p2align 3
.L599:
	movl	$181, %r8d
	movl	$65, %edx
	movl	$108, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L572
	.p2align 4,,10
	.p2align 3
.L583:
	movl	$213, %r8d
	movl	$101, %edx
	movl	$108, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L572
.L602:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1320:
	.size	X509V3_get_value_int, .-X509V3_get_value_int
	.p2align 4
	.globl	X509V3_parse_list
	.type	X509V3_parse_list, @function
X509V3_parse_list:
.LFB1321:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$295, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	call	CRYPTO_strdup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L605
	movzbl	(%rax), %eax
	movq	%r12, %rdi
	testb	%al, %al
	je	.L607
	movq	%r12, %rbx
	movl	$1, %edx
	leaq	-64(%rbp), %r13
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L606:
	cmpb	$13, %al
	je	.L619
	cmpb	$10, %al
	je	.L619
	cmpl	$2, %edx
	jne	.L617
	cmpb	$44, %al
	je	.L615
.L648:
	movq	%rbx, %rax
	movl	$2, %edx
	addq	$1, %rbx
.L613:
	movzbl	1(%rax), %eax
	testb	%al, %al
	jne	.L606
.L619:
	cmpl	$2, %edx
	je	.L612
.L607:
	call	strip_spaces
	movl	$360, %r8d
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L642
	leaq	-64(%rbp), %rdx
	xorl	%esi, %esi
	call	X509V3_add_value
.L623:
	movl	$365, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	movq	-64(%rbp), %rax
.L604:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L644
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L631:
	.cfi_restore_state
	movq	%rdi, %rbx
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L617:
	cmpb	$58, %al
	je	.L645
	leaq	1(%rbx), %r15
	cmpb	$44, %al
	je	.L646
	movq	%rbx, %rax
	movl	$1, %edx
	movq	%r15, %rbx
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L645:
	movb	$0, (%rbx)
	call	strip_spaces
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L647
	movzbl	1(%rbx), %eax
	leaq	1(%rbx), %rdi
	cmpb	$13, %al
	ja	.L629
	movl	$9217, %edx
	btq	%rax, %rdx
	jc	.L612
.L629:
	movq	%rdi, %rbx
	cmpb	$44, %al
	jne	.L648
	.p2align 4,,10
	.p2align 3
.L615:
	movb	$0, (%rbx)
	call	strip_spaces
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L649
	movq	%r14, %rdi
	movq	%r13, %rdx
	call	X509V3_add_value
	movzbl	1(%rbx), %eax
	leaq	1(%rbx), %rdi
	cmpb	$13, %al
	ja	.L631
	movl	$9217, %edx
	btq	%rax, %rdx
	jc	.L607
	addq	$2, %rbx
	movq	%rdi, %rax
	movl	$1, %edx
	xorl	%r14d, %r14d
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L646:
	movb	$0, (%rbx)
	call	strip_spaces
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L650
	movq	%r13, %rdx
	movq	%rax, %rdi
	xorl	%esi, %esi
	call	X509V3_add_value
	movq	%rbx, %rax
	movq	%r15, %rdi
	movq	%r15, %rbx
	movl	$1, %edx
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L612:
	call	strip_spaces
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L651
	leaq	-64(%rbp), %rdx
	movq	%r14, %rdi
	call	X509V3_add_value
	jmp	.L623
.L650:
	movl	$323, %r8d
.L642:
	movl	$108, %edx
	movl	$109, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L608:
	movl	$369, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	movq	-64(%rbp), %rdi
	leaq	X509V3_conf_free(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	xorl	%eax, %eax
	jmp	.L604
.L605:
	movl	$297, %r8d
	movl	$65, %edx
	movl	$109, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L608
.L647:
	movl	$313, %r8d
	jmp	.L642
.L649:
	movl	$337, %r8d
.L643:
	movl	$109, %edx
	movl	$109, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L608
.L651:
	movl	$352, %r8d
	jmp	.L643
.L644:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1321:
	.size	X509V3_parse_list, .-X509V3_parse_list
	.p2align 4
	.globl	name_cmp
	.type	name_cmp, @function
name_cmp:
.LFB1323:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	call	strlen@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movslq	%eax, %r13
	movq	%r13, %rdx
	call	strncmp@PLT
	testl	%eax, %eax
	jne	.L652
	movzbl	(%rbx,%r13), %edx
	testb	%dl, %dl
	sete	%al
	cmpb	$46, %dl
	sete	%dl
	orl	%edx, %eax
	xorl	$1, %eax
	movzbl	%al, %eax
.L652:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1323:
	.size	name_cmp, .-name_cmp
	.p2align 4
	.globl	X509_get1_email
	.type	X509_get1_email, @function
X509_get1_email:
.LFB1325:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$85, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	call	X509_get_ext_d2i@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	X509_get_subject_name@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	get_email
	movq	GENERAL_NAME_free@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	OPENSSL_sk_pop_free@PLT
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1325:
	.size	X509_get1_email, .-X509_get1_email
	.p2align 4
	.globl	X509_get1_ocsp
	.type	X509_get1_ocsp, @function
X509_get1_ocsp:
.LFB1326:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$177, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	call	X509_get_ext_d2i@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L657
	xorl	%r12d, %r12d
	leaq	-48(%rbp), %r14
	jmp	.L659
	.p2align 4,,10
	.p2align 3
.L661:
	addl	$1, %r12d
.L659:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L663
	movq	%r13, %rdi
	movl	%r12d, %esi
	call	OPENSSL_sk_value@PLT
	movq	(%rax), %rdi
	movq	%rax, %rbx
	call	OBJ_obj2nid@PLT
	cmpl	$178, %eax
	jne	.L661
	movq	8(%rbx), %rax
	cmpl	$6, (%rax)
	jne	.L661
	movq	8(%rax), %rsi
	movq	%r14, %rdi
	call	append_ia5
	testl	%eax, %eax
	jne	.L661
	.p2align 4,,10
	.p2align 3
.L663:
	movq	%r13, %rdi
	call	AUTHORITY_INFO_ACCESS_free@PLT
	movq	-48(%rbp), %r13
.L657:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L669
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L669:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1326:
	.size	X509_get1_ocsp, .-X509_get1_ocsp
	.p2align 4
	.globl	X509_REQ_get1_email
	.type	X509_REQ_get1_email, @function
X509_REQ_get1_email:
.LFB1327:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	call	X509_REQ_get_extensions@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$85, %esi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	X509V3_get_d2i@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	X509_REQ_get_subject_name@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	get_email
	movq	GENERAL_NAME_free@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	OPENSSL_sk_pop_free@PLT
	movq	X509_EXTENSION_free@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	call	OPENSSL_sk_pop_free@PLT
	addq	$8, %rsp
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1327:
	.size	X509_REQ_get1_email, .-X509_REQ_get1_email
	.p2align 4
	.globl	X509_email_free
	.type	X509_email_free, @function
X509_email_free:
.LFB1331:
	.cfi_startproc
	endbr64
	leaq	str_free(%rip), %rsi
	jmp	OPENSSL_sk_pop_free@PLT
	.cfi_endproc
.LFE1331:
	.size	X509_email_free, .-X509_email_free
	.p2align 4
	.globl	X509_check_host
	.type	X509_check_host, @function
X509_check_host:
.LFB1341:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L684
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$24, %rsp
	movl	%ecx, -52(%rbp)
	testq	%rdx, %rdx
	jne	.L675
	movq	%rsi, %rdi
	call	strlen@PLT
	movl	-52(%rbp), %ecx
	cmpq	$1, %rax
	movq	%rax, %r12
	leaq	-1(%rax), %r15
	jbe	.L676
.L677:
	cmpb	$0, 0(%r13,%r15)
	cmove	%r15, %r12
.L676:
	addq	$24, %rsp
	movq	%rbx, %r9
	movq	%r12, %rdx
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	movq	%r13, %rsi
	popq	%r12
	.cfi_restore 12
	movq	%r14, %rdi
	popq	%r13
	.cfi_restore 13
	movl	$2, %r8d
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	do_x509_check
	.p2align 4,,10
	.p2align 3
.L675:
	.cfi_restore_state
	cmpq	$1, %rdx
	jbe	.L678
	leaq	-1(%rdx), %r15
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%r15, %rdx
	call	memchr@PLT
	movl	-52(%rbp), %ecx
	testq	%rax, %rax
	je	.L677
.L673:
	addq	$24, %rsp
	movl	$-2, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L678:
	.cfi_restore_state
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%r13, %rdi
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L673
	movl	-52(%rbp), %ecx
	movl	$1, %r12d
	jmp	.L676
.L684:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	$-2, %eax
	ret
	.cfi_endproc
.LFE1341:
	.size	X509_check_host, .-X509_check_host
	.p2align 4
	.globl	X509_check_email
	.type	X509_check_email, @function
X509_check_email:
.LFB1342:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L698
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	jne	.L689
	movq	%rsi, %rdi
	call	strlen@PLT
	movq	%rax, %r12
	leaq	-1(%rax), %rbx
	cmpq	$1, %rax
	jbe	.L690
.L691:
	cmpb	$0, 0(%r13,%rbx)
	cmove	%rbx, %r12
.L690:
	addq	$8, %rsp
	movl	%r15d, %ecx
	movq	%r12, %rdx
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	movq	%r13, %rsi
	popq	%r12
	.cfi_restore 12
	movq	%r14, %rdi
	popq	%r13
	.cfi_restore 13
	xorl	%r9d, %r9d
	popq	%r14
	.cfi_restore 14
	movl	$1, %r8d
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	do_x509_check
	.p2align 4,,10
	.p2align 3
.L689:
	.cfi_restore_state
	cmpq	$1, %rdx
	jbe	.L692
	leaq	-1(%rdx), %rbx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rbx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	je	.L691
.L687:
	addq	$8, %rsp
	movl	$-2, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L692:
	.cfi_restore_state
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%r13, %rdi
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L687
	movl	$1, %r12d
	jmp	.L690
.L698:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	$-2, %eax
	ret
	.cfi_endproc
.LFE1342:
	.size	X509_check_email, .-X509_check_email
	.p2align 4
	.globl	X509_check_ip
	.type	X509_check_ip, @function
X509_check_ip:
.LFB1343:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L702
	xorl	%r9d, %r9d
	movl	$7, %r8d
	jmp	do_x509_check
.L702:
	movl	$-2, %eax
	ret
	.cfi_endproc
.LFE1343:
	.size	X509_check_ip, .-X509_check_ip
	.p2align 4
	.globl	X509_check_ip_asc
	.type	X509_check_ip_asc, @function
X509_check_ip_asc:
.LFB1344:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L721
	movq	%rsi, %r12
	movq	%rdi, %r14
	movl	$58, %esi
	movl	%edx, %r13d
	movq	%r12, %rdi
	call	strchr@PLT
	testq	%rax, %rax
	je	.L705
	leaq	-112(%rbp), %rbx
	xorl	%edx, %edx
	leaq	ipv6_cb(%rip), %rcx
	movq	%r12, %rdi
	movq	%rbx, %r8
	movl	$58, %esi
	movabsq	$-4294967296, %rax
	movl	$0, -88(%rbp)
	movq	%rax, -96(%rbp)
	call	CONF_parse_list@PLT
	testl	%eax, %eax
	je	.L721
	movl	-92(%rbp), %r8d
	movl	-96(%rbp), %r12d
	cmpl	$-1, %r8d
	je	.L733
	cmpl	$16, %r12d
	je	.L721
	movl	-88(%rbp), %eax
	cmpl	$3, %eax
	jg	.L721
	je	.L734
	cmpl	$2, %eax
	je	.L735
	testl	%r8d, %r8d
	je	.L721
	cmpl	%r12d, %r8d
	je	.L721
	testl	%r8d, %r8d
	js	.L709
	movslq	%r8d, %r9
	leaq	-80(%rbp), %r15
	movl	$16, %ecx
	movq	%rbx, %rsi
	movq	%r9, %rdx
	movq	%r15, %rdi
	movl	%r8d, -140(%rbp)
	movq	%r9, -136(%rbp)
	call	__memcpy_chk@PLT
	movq	-136(%rbp), %r9
	movl	$16, %edx
	xorl	%esi, %esi
	subl	%r12d, %edx
	leaq	(%r15,%r9), %rdi
	movslq	%edx, %rdx
	call	memset@PLT
	movq	-136(%rbp), %r9
	movl	-140(%rbp), %r8d
.L718:
	movslq	%r12d, %rax
	movq	%r9, %rcx
	leaq	(%rbx,%r9), %rsi
	subl	%r8d, %r12d
	subq	%rax, %rcx
	movslq	%r12d, %rdx
	leaq	16(%r15,%rcx), %rdi
	call	memcpy@PLT
	movl	$16, %edx
	jmp	.L716
	.p2align 4,,10
	.p2align 3
.L705:
	xorl	%eax, %eax
	leaq	-124(%rbp), %rcx
	leaq	-128(%rbp), %rdx
	movq	%r12, %rdi
	leaq	-116(%rbp), %r9
	leaq	-120(%rbp), %r8
	leaq	.LC2(%rip), %rsi
	call	__isoc99_sscanf@PLT
	cmpl	$4, %eax
	je	.L736
	.p2align 4,,10
	.p2align 3
.L721:
	movl	$-2, %eax
.L703:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L737
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L736:
	.cfi_restore_state
	movl	-128(%rbp), %eax
	cmpl	$255, %eax
	ja	.L721
	movl	-124(%rbp), %edx
	cmpl	$255, %edx
	ja	.L721
	movl	-120(%rbp), %ecx
	cmpl	$255, %ecx
	ja	.L721
	movl	-116(%rbp), %esi
	cmpl	$255, %esi
	ja	.L721
	movb	%dl, -79(%rbp)
	leaq	-80(%rbp), %r15
	movl	$4, %edx
	movb	%al, -80(%rbp)
	movb	%cl, -78(%rbp)
	movb	%sil, -77(%rbp)
	jmp	.L716
	.p2align 4,,10
	.p2align 3
.L733:
	cmpl	$16, %r12d
	jne	.L721
.L709:
	movdqa	-112(%rbp), %xmm0
	movl	$16, %edx
	leaq	-80(%rbp), %r15
	movaps	%xmm0, -80(%rbp)
.L716:
	xorl	%r9d, %r9d
	movl	$7, %r8d
	movl	%r13d, %ecx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	do_x509_check
	jmp	.L703
	.p2align 4,,10
	.p2align 3
.L734:
	testl	%r12d, %r12d
	jg	.L721
.L711:
	testl	%r8d, %r8d
	js	.L709
	movslq	%r8d, %r9
	leaq	-80(%rbp), %r15
	movl	$16, %ecx
	movq	%rbx, %rsi
	movq	%r9, %rdx
	movq	%r15, %rdi
	movl	%r8d, -140(%rbp)
	movq	%r9, -136(%rbp)
	call	__memcpy_chk@PLT
	movq	-136(%rbp), %r9
	movl	$16, %edx
	xorl	%esi, %esi
	subl	%r12d, %edx
	movslq	%edx, %rdx
	leaq	(%r15,%r9), %rdi
	call	memset@PLT
	movl	-140(%rbp), %r8d
	movl	$16, %edx
	movq	-136(%rbp), %r9
	cmpl	%r12d, %r8d
	je	.L716
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L735:
	cmpl	%r12d, %r8d
	je	.L711
	testl	%r8d, %r8d
	je	.L711
	jmp	.L721
.L737:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1344:
	.size	X509_check_ip_asc, .-X509_check_ip_asc
	.p2align 4
	.globl	a2i_IPADDRESS
	.type	a2i_IPADDRESS, @function
a2i_IPADDRESS:
.LFB1345:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$58, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	testq	%rax, %rax
	je	.L739
	leaq	-112(%rbp), %r12
	xorl	%edx, %edx
	leaq	ipv6_cb(%rip), %rcx
	movq	%r13, %rdi
	movq	%r12, %r8
	movl	$58, %esi
	movabsq	$-4294967296, %rax
	movl	$0, -88(%rbp)
	movq	%rax, -96(%rbp)
	call	CONF_parse_list@PLT
	testl	%eax, %eax
	je	.L770
	movl	-92(%rbp), %r14d
	movl	-96(%rbp), %ebx
	cmpl	$-1, %r14d
	je	.L771
	cmpl	$16, %ebx
	je	.L770
	movl	-88(%rbp), %eax
	cmpl	$3, %eax
	jg	.L770
	je	.L772
	cmpl	$2, %eax
	je	.L773
	testl	%r14d, %r14d
	je	.L770
	cmpl	%ebx, %r14d
	je	.L770
	testl	%r14d, %r14d
	js	.L744
	leaq	-80(%rbp), %r8
	movslq	%r14d, %r15
	movl	$16, %ecx
	movq	%r12, %rsi
	movq	%r8, %rdi
	movq	%r15, %rdx
	call	__memcpy_chk@PLT
	movl	$16, %edx
	xorl	%esi, %esi
	subl	%ebx, %edx
	leaq	(%rax,%r15), %rdi
	movq	%rax, -136(%rbp)
	movslq	%edx, %rdx
	call	memset@PLT
	movq	-136(%rbp), %r8
.L753:
	movslq	%ebx, %rax
	movq	%r15, %rcx
	subl	%r14d, %ebx
	movl	$16, %r13d
	subq	%rax, %rcx
	movslq	%ebx, %rdx
	leaq	(%r12,%r15), %rsi
	leaq	16(%r8,%rcx), %rdi
	call	memcpy@PLT
	jmp	.L751
	.p2align 4,,10
	.p2align 3
.L771:
	cmpl	$16, %ebx
	jne	.L770
.L744:
	movdqa	-112(%rbp), %xmm0
	movl	$16, %r13d
	movaps	%xmm0, -80(%rbp)
.L751:
	call	ASN1_OCTET_STRING_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L738
	leaq	-80(%rbp), %rsi
	movl	%r13d, %edx
	movq	%rax, %rdi
	call	ASN1_OCTET_STRING_set@PLT
	testl	%eax, %eax
	jne	.L738
	movq	%r12, %rdi
	call	ASN1_OCTET_STRING_free@PLT
	.p2align 4,,10
	.p2align 3
.L770:
	xorl	%r12d, %r12d
.L738:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L774
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L739:
	.cfi_restore_state
	movq	%r13, %rdi
	leaq	-124(%rbp), %rcx
	leaq	-128(%rbp), %rdx
	movq	%rax, %r12
	leaq	-116(%rbp), %r9
	leaq	-120(%rbp), %r8
	xorl	%eax, %eax
	leaq	.LC2(%rip), %rsi
	call	__isoc99_sscanf@PLT
	movl	%eax, %r13d
	cmpl	$4, %eax
	jne	.L738
	movl	-128(%rbp), %eax
	cmpl	$255, %eax
	ja	.L738
	movl	-124(%rbp), %edx
	cmpl	$255, %edx
	ja	.L738
	movl	-120(%rbp), %ecx
	cmpl	$255, %ecx
	ja	.L738
	movl	-116(%rbp), %esi
	cmpl	$255, %esi
	ja	.L738
	movb	%al, -80(%rbp)
	movb	%dl, -79(%rbp)
	movb	%cl, -78(%rbp)
	movb	%sil, -77(%rbp)
	jmp	.L751
	.p2align 4,,10
	.p2align 3
.L772:
	testl	%ebx, %ebx
	jg	.L770
.L746:
	testl	%r14d, %r14d
	js	.L744
	leaq	-80(%rbp), %r8
	movslq	%r14d, %r15
	movl	$16, %ecx
	movq	%r12, %rsi
	movq	%r8, %rdi
	movq	%r15, %rdx
	movl	$16, %r13d
	call	__memcpy_chk@PLT
	movl	$16, %edx
	xorl	%esi, %esi
	subl	%ebx, %edx
	leaq	(%rax,%r15), %rdi
	movq	%rax, -136(%rbp)
	movslq	%edx, %rdx
	call	memset@PLT
	cmpl	%ebx, %r14d
	movq	-136(%rbp), %r8
	je	.L751
	jmp	.L753
	.p2align 4,,10
	.p2align 3
.L773:
	testl	%r14d, %r14d
	je	.L746
	cmpl	%ebx, %r14d
	je	.L746
	jmp	.L770
.L774:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1345:
	.size	a2i_IPADDRESS, .-a2i_IPADDRESS
	.p2align 4
	.globl	a2i_ipadd
	.type	a2i_ipadd, @function
a2i_ipadd:
.LFB1347:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$58, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	testq	%rax, %rax
	je	.L776
	leaq	-96(%rbp), %r13
	xorl	%edx, %edx
	leaq	ipv6_cb(%rip), %rcx
	movq	%r12, %rdi
	movq	%r13, %r8
	movl	$58, %esi
	movabsq	$-4294967296, %rax
	movl	$0, -72(%rbp)
	movq	%rax, -80(%rbp)
	call	CONF_parse_list@PLT
	testl	%eax, %eax
	je	.L788
	movl	-76(%rbp), %r14d
	movl	-80(%rbp), %r12d
	cmpl	$-1, %r14d
	je	.L803
	cmpl	$16, %r12d
	je	.L788
	movl	-72(%rbp), %eax
	cmpl	$3, %eax
	jg	.L788
	je	.L804
	cmpl	$2, %eax
	je	.L805
	testl	%r14d, %r14d
	je	.L788
	cmpl	%r12d, %r14d
	je	.L788
	testl	%r14d, %r14d
	js	.L781
	movslq	%r14d, %r15
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%r15, %rdx
	call	memcpy@PLT
	movl	$16, %edx
	leaq	(%rbx,%r15), %rdi
	xorl	%esi, %esi
	subl	%r12d, %edx
	movslq	%edx, %rdx
	call	memset@PLT
.L789:
	movslq	%r12d, %rax
	movq	%r15, %rcx
	leaq	0(%r13,%r15), %rsi
	subl	%r14d, %r12d
	subq	%rax, %rcx
	movslq	%r12d, %rdx
	leaq	16(%rbx,%rcx), %rdi
	call	memcpy@PLT
	movl	$16, %eax
	jmp	.L775
	.p2align 4,,10
	.p2align 3
.L776:
	xorl	%eax, %eax
	leaq	-108(%rbp), %rcx
	leaq	-112(%rbp), %rdx
	movq	%r12, %rdi
	leaq	-100(%rbp), %r9
	leaq	-104(%rbp), %r8
	leaq	.LC2(%rip), %rsi
	call	__isoc99_sscanf@PLT
	cmpl	$4, %eax
	je	.L806
	.p2align 4,,10
	.p2align 3
.L788:
	xorl	%eax, %eax
.L775:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L807
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L806:
	.cfi_restore_state
	movl	-112(%rbp), %edx
	cmpl	$255, %edx
	ja	.L788
	movl	-108(%rbp), %ecx
	cmpl	$255, %ecx
	ja	.L788
	movl	-104(%rbp), %esi
	cmpl	$255, %esi
	ja	.L788
	movl	-100(%rbp), %edi
	cmpl	$255, %edi
	ja	.L788
	movb	%dl, (%rbx)
	movb	%cl, 1(%rbx)
	movb	%sil, 2(%rbx)
	movb	%dil, 3(%rbx)
	jmp	.L775
	.p2align 4,,10
	.p2align 3
.L803:
	cmpl	$16, %r12d
	jne	.L788
.L781:
	movdqa	-96(%rbp), %xmm0
	movl	$16, %eax
	movups	%xmm0, (%rbx)
	jmp	.L775
	.p2align 4,,10
	.p2align 3
.L804:
	testl	%r12d, %r12d
	jg	.L788
.L783:
	testl	%r14d, %r14d
	js	.L781
	movslq	%r14d, %r15
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%r15, %rdx
	call	memcpy@PLT
	movl	$16, %edx
	leaq	(%rbx,%r15), %rdi
	xorl	%esi, %esi
	subl	%r12d, %edx
	movslq	%edx, %rdx
	call	memset@PLT
	movl	$16, %eax
	cmpl	%r12d, %r14d
	je	.L775
	jmp	.L789
	.p2align 4,,10
	.p2align 3
.L805:
	testl	%r14d, %r14d
	je	.L783
	cmpl	%r12d, %r14d
	je	.L783
	jmp	.L788
.L807:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1347:
	.size	a2i_ipadd, .-a2i_ipadd
	.p2align 4
	.globl	a2i_IPADDRESS_NC
	.type	a2i_IPADDRESS_NC, @function
a2i_IPADDRESS_NC:
.LFB1346:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$47, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	testq	%rax, %rax
	je	.L825
	movl	$995, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	CRYPTO_strdup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L825
	subq	%r13, %rbx
	leaq	-96(%rbp), %r14
	movq	%rax, %rsi
	addq	%rax, %rbx
	movq	%r14, %rdi
	movb	$0, (%rbx)
	call	a2i_ipadd
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L817
	movslq	%eax, %rdi
	leaq	1(%rbx), %rsi
	addq	%r14, %rdi
	call	a2i_ipadd
	movl	$1008, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	CRYPTO_free@PLT
	testl	%ebx, %ebx
	je	.L815
	cmpl	%ebx, %r13d
	jne	.L815
	call	ASN1_OCTET_STRING_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L815
	leal	0(%r13,%rbx), %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	xorl	%r12d, %r12d
	call	ASN1_OCTET_STRING_set@PLT
	testl	%eax, %eax
	jne	.L808
	jmp	.L812
	.p2align 4,,10
	.p2align 3
.L815:
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
.L812:
	movq	%r12, %rdi
	movl	$1023, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r15, %rdi
	call	ASN1_OCTET_STRING_free@PLT
.L825:
	xorl	%r15d, %r15d
.L808:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L826
	addq	$56, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L817:
	.cfi_restore_state
	xorl	%r15d, %r15d
	jmp	.L812
.L826:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1346:
	.size	a2i_IPADDRESS_NC, .-a2i_IPADDRESS_NC
	.p2align 4
	.globl	X509V3_NAME_from_section
	.type	X509V3_NAME_from_section, @function
X509V3_NAME_from_section:
.LFB1352:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L849
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%r12, %rdi
	xorl	%ebx, %ebx
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L850
	.p2align 4,,10
	.p2align 3
.L835:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	movq	8(%rax), %r10
	movzbl	(%r10), %r8d
	testb	%r8b, %r8b
	je	.L837
	movq	%r10, %rcx
	movl	%r8d, %edx
	jmp	.L832
	.p2align 4,,10
	.p2align 3
.L851:
	testb	%dl, %dl
	je	.L833
.L832:
	movl	%edx, %esi
	andl	$-3, %esi
	cmpb	$44, %sil
	sete	%dil
	cmpb	$58, %dl
	movzbl	1(%rcx), %edx
	sete	%sil
	addq	$1, %rcx
	orb	%sil, %dil
	je	.L851
	testb	%dl, %dl
	cmovne	%edx, %r8d
	cmovne	%rcx, %r10
.L833:
	xorl	%edx, %edx
	cmpb	$43, %r8b
	jne	.L830
	addq	$1, %r10
	movl	$-1, %edx
.L830:
	subq	$8, %rsp
	movq	16(%rax), %rcx
	movq	%r10, %rsi
	movq	%r13, %rdi
	pushq	%rdx
	movl	$-1, %r9d
	movl	%r14d, %edx
	movl	$-1, %r8d
	call	X509_NAME_add_entry_by_txt@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	je	.L834
	movq	%r12, %rdi
	addl	$1, %ebx
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L835
.L850:
	leaq	-32(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L834:
	.cfi_restore_state
	leaq	-32(%rbp), %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L837:
	.cfi_restore_state
	xorl	%edx, %edx
	jmp	.L830
.L849:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1352:
	.size	X509V3_NAME_from_section, .-X509V3_NAME_from_section
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
