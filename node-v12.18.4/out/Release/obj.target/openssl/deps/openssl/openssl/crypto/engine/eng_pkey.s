	.file	"eng_pkey.c"
	.text
	.p2align 4
	.globl	ENGINE_set_load_privkey_function
	.type	ENGINE_set_load_privkey_function, @function
ENGINE_set_load_privkey_function:
.LFB866:
	.cfi_startproc
	endbr64
	movq	%rsi, 120(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE866:
	.size	ENGINE_set_load_privkey_function, .-ENGINE_set_load_privkey_function
	.p2align 4
	.globl	ENGINE_set_load_pubkey_function
	.type	ENGINE_set_load_pubkey_function, @function
ENGINE_set_load_pubkey_function:
.LFB867:
	.cfi_startproc
	endbr64
	movq	%rsi, 128(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE867:
	.size	ENGINE_set_load_pubkey_function, .-ENGINE_set_load_pubkey_function
	.p2align 4
	.globl	ENGINE_set_load_ssl_client_cert_function
	.type	ENGINE_set_load_ssl_client_cert_function, @function
ENGINE_set_load_ssl_client_cert_function:
.LFB868:
	.cfi_startproc
	endbr64
	movq	%rsi, 136(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE868:
	.size	ENGINE_set_load_ssl_client_cert_function, .-ENGINE_set_load_ssl_client_cert_function
	.p2align 4
	.globl	ENGINE_get_load_privkey_function
	.type	ENGINE_get_load_privkey_function, @function
ENGINE_get_load_privkey_function:
.LFB869:
	.cfi_startproc
	endbr64
	movq	120(%rdi), %rax
	ret
	.cfi_endproc
.LFE869:
	.size	ENGINE_get_load_privkey_function, .-ENGINE_get_load_privkey_function
	.p2align 4
	.globl	ENGINE_get_load_pubkey_function
	.type	ENGINE_get_load_pubkey_function, @function
ENGINE_get_load_pubkey_function:
.LFB870:
	.cfi_startproc
	endbr64
	movq	128(%rdi), %rax
	ret
	.cfi_endproc
.LFE870:
	.size	ENGINE_get_load_pubkey_function, .-ENGINE_get_load_pubkey_function
	.p2align 4
	.globl	ENGINE_get_ssl_client_cert_function
	.type	ENGINE_get_ssl_client_cert_function, @function
ENGINE_get_ssl_client_cert_function:
.LFB871:
	.cfi_startproc
	endbr64
	movq	136(%rdi), %rax
	ret
	.cfi_endproc
.LFE871:
	.size	ENGINE_get_ssl_client_cert_function, .-ENGINE_get_ssl_client_cert_function
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/engine/eng_pkey.c"
	.text
	.p2align 4
	.globl	ENGINE_load_private_key
	.type	ENGINE_load_private_key, @function
ENGINE_load_private_key:
.LFB872:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	testq	%rdi, %rdi
	je	.L14
	movq	%rdi, %r12
	movq	global_engine_lock(%rip), %rdi
	movq	%rsi, %r13
	movq	%rdx, %r14
	movq	%rcx, %r15
	call	CRYPTO_THREAD_write_lock@PLT
	movl	160(%r12), %eax
	movq	global_engine_lock(%rip), %rdi
	testl	%eax, %eax
	je	.L15
	call	CRYPTO_THREAD_unlock@PLT
	movq	120(%r12), %rax
	testq	%rax, %rax
	je	.L16
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	testq	%rax, %rax
	je	.L17
.L8:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	call	CRYPTO_THREAD_unlock@PLT
	movl	$66, %r8d
	movl	$117, %edx
	leaq	.LC0(%rip), %rcx
	movl	$150, %esi
	movl	$38, %edi
	call	ERR_put_error@PLT
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	movl	$59, %r8d
	movl	$67, %edx
	movl	$150, %esi
	movl	$38, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L16:
	movl	$71, %r8d
	movl	$125, %edx
	movl	$150, %esi
	movl	$38, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L17:
	movl	$77, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$128, %edx
	movl	$150, %esi
	movl	$38, %edi
	movq	%rax, -40(%rbp)
	call	ERR_put_error@PLT
	movq	-40(%rbp), %rax
	jmp	.L8
	.cfi_endproc
.LFE872:
	.size	ENGINE_load_private_key, .-ENGINE_load_private_key
	.p2align 4
	.globl	ENGINE_load_public_key
	.type	ENGINE_load_public_key, @function
ENGINE_load_public_key:
.LFB873:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	testq	%rdi, %rdi
	je	.L24
	movq	%rdi, %r12
	movq	global_engine_lock(%rip), %rdi
	movq	%rsi, %r13
	movq	%rdx, %r14
	movq	%rcx, %r15
	call	CRYPTO_THREAD_write_lock@PLT
	movl	160(%r12), %eax
	movq	global_engine_lock(%rip), %rdi
	testl	%eax, %eax
	je	.L25
	call	CRYPTO_THREAD_unlock@PLT
	movq	128(%r12), %rax
	testq	%rax, %rax
	je	.L26
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	testq	%rax, %rax
	je	.L27
.L18:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	call	CRYPTO_THREAD_unlock@PLT
	movl	$97, %r8d
	movl	$117, %edx
	leaq	.LC0(%rip), %rcx
	movl	$151, %esi
	movl	$38, %edi
	call	ERR_put_error@PLT
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movl	$90, %r8d
	movl	$67, %edx
	movl	$151, %esi
	movl	$38, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L26:
	movl	$102, %r8d
	movl	$125, %edx
	movl	$151, %esi
	movl	$38, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L27:
	movl	$107, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$129, %edx
	movl	$151, %esi
	movl	$38, %edi
	movq	%rax, -40(%rbp)
	call	ERR_put_error@PLT
	movq	-40(%rbp), %rax
	jmp	.L18
	.cfi_endproc
.LFE873:
	.size	ENGINE_load_public_key, .-ENGINE_load_public_key
	.p2align 4
	.globl	ENGINE_load_ssl_client_cert
	.type	ENGINE_load_ssl_client_cert, @function
ENGINE_load_ssl_client_cert:
.LFB874:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	%rsi, -56(%rbp)
	movq	%rax, -64(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -72(%rbp)
	testq	%rdi, %rdi
	je	.L34
	movq	%rdi, %r12
	movq	global_engine_lock(%rip), %rdi
	movq	%rdx, %r13
	movq	%rcx, %r14
	movq	%r8, %rbx
	movq	%r9, %r15
	call	CRYPTO_THREAD_write_lock@PLT
	movl	160(%r12), %eax
	movq	global_engine_lock(%rip), %rdi
	testl	%eax, %eax
	je	.L35
	call	CRYPTO_THREAD_unlock@PLT
	movq	136(%r12), %rax
	testq	%rax, %rax
	je	.L36
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rcx
	movq	%r15, %r9
	movq	%rbx, %r8
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rdx, 24(%rbp)
	movq	%r13, %rdx
	movq	%rcx, 16(%rbp)
	addq	$40, %rsp
	movq	%r14, %rcx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	call	CRYPTO_THREAD_unlock@PLT
	movl	$128, %r8d
	movl	$117, %edx
	leaq	.LC0(%rip), %rcx
	movl	$194, %esi
	movl	$38, %edi
	call	ERR_put_error@PLT
.L28:
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	movl	$134, %r8d
	movl	$125, %edx
	movl	$194, %esi
	movl	$38, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L34:
	movl	$121, %r8d
	movl	$67, %edx
	movl	$194, %esi
	movl	$38, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L28
	.cfi_endproc
.LFE874:
	.size	ENGINE_load_ssl_client_cert, .-ENGINE_load_ssl_client_cert
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
