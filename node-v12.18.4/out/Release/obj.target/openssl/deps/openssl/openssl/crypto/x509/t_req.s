	.file	"t_req.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"critical"
.LC1:
	.string	""
.LC2:
	.string	"Certificate Request:\n"
.LC3:
	.string	"    Data:\n"
.LC4:
	.string	"%8sVersion: %ld (0x%lx)\n"
.LC5:
	.string	"%8sVersion: Unknown (%ld)\n"
.LC6:
	.string	"        Subject:%c"
.LC7:
	.string	"\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"        Subject Public Key Info:\n"
	.section	.rodata.str1.1
.LC9:
	.string	"%12sPublic Key Algorithm: "
	.section	.rodata.str1.8
	.align 8
.LC10:
	.string	"%12sUnable to load Public Key\n"
	.section	.rodata.str1.1
.LC11:
	.string	"%8sAttributes:\n"
.LC12:
	.string	"%12sa0:00\n"
.LC13:
	.string	"%12s"
	.section	.rodata.str1.8
	.align 8
.LC14:
	.string	"../deps/openssl/openssl/crypto/x509/t_req.c"
	.section	.rodata.str1.1
.LC15:
	.string	" "
.LC16:
	.string	":"
.LC17:
	.string	"unable to print attribute\n"
.LC18:
	.string	"%8sRequested Extensions:\n"
.LC19:
	.string	": %s\n"
.LC20:
	.string	"%16s"
	.text
	.p2align 4
	.globl	X509_REQ_print_ex
	.type	X509_REQ_print_ex, @function
X509_REQ_print_ex:
.LFB1297:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, %rax
	andl	$983040, %eax
	cmpq	$262144, %rax
	je	.L41
	cmpq	$1, %rdx
	movl	$32, -84(%rbp)
	sbbl	%r15d, %r15d
	andl	$16, %r15d
	testb	$1, %bl
	je	.L3
.L7:
	testb	$2, %bl
	je	.L59
.L5:
	testb	$64, %bl
	je	.L60
.L9:
	testb	$-128, %bl
	je	.L61
.L12:
	testb	$8, %bh
	je	.L17
.L21:
	testb	$1, %bh
	je	.L62
.L32:
	movl	$1, %r15d
	andb	$2, %bh
	je	.L63
	.p2align 4,,10
	.p2align 3
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L64
	addq	$72, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	movl	-84(%rbp), %edx
	xorl	%eax, %eax
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L16
	movq	%r14, %rdi
	call	X509_REQ_get_subject_name@PLT
	movq	%r13, %rcx
	movl	%r15d, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	X509_NAME_print_ex@PLT
	testl	%eax, %eax
	js	.L16
	movl	$1, %edx
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jg	.L9
	.p2align 4,,10
	.p2align 3
.L16:
	movl	$207, %r8d
	leaq	.LC14(%rip), %rcx
	movl	$7, %edx
	xorl	%r15d, %r15d
	movl	$121, %esi
	movl	$11, %edi
	call	ERR_put_error@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L41:
	movl	$10, -84(%rbp)
	movl	$12, %r15d
	testb	$1, %bl
	jne	.L7
.L3:
	movl	$21, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jle	.L16
	movl	$10, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jle	.L16
	testb	$2, %bl
	jne	.L5
	.p2align 4,,10
	.p2align 3
.L59:
	movq	%r14, %rdi
	call	X509_REQ_get_version@PLT
	movq	%rax, %r8
	cmpq	$2, %rax
	ja	.L10
	leaq	1(%rax), %rcx
	leaq	.LC1(%rip), %rdx
	xorl	%eax, %eax
	movq	%r12, %rdi
	leaq	.LC4(%rip), %rsi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jg	.L5
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L61:
	movl	$33, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jle	.L16
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rdx
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L16
	movq	%r14, %rdi
	call	X509_REQ_get_X509_PUBKEY@PLT
	xorl	%esi, %esi
	leaq	-64(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %r8
	call	X509_PUBKEY_get0_param@PLT
	movq	-64(%rbp), %rsi
	movq	%r12, %rdi
	call	i2a_ASN1_OBJECT@PLT
	testl	%eax, %eax
	jle	.L16
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L16
	movq	%r14, %rdi
	call	X509_REQ_get0_pubkey@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L65
	xorl	%ecx, %ecx
	movl	$16, %edx
	movq	%r12, %rdi
	call	EVP_PKEY_print_public@PLT
	testl	%eax, %eax
	jg	.L12
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L10:
	movq	%rax, %rcx
	leaq	.LC1(%rip), %rdx
	xorl	%eax, %eax
	movq	%r12, %rdi
	leaq	.LC5(%rip), %rsi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jg	.L5
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L62:
	movq	%r14, %rdi
	call	X509_REQ_get_extensions@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L32
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rdx
	leaq	.LC18(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L16
	movl	$0, -84(%rbp)
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L38:
	movl	-84(%rbp), %esi
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	leaq	.LC1(%rip), %rdx
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L16
	movq	%r13, %rdi
	call	X509_EXTENSION_get_object@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	i2a_ASN1_OBJECT@PLT
	testl	%eax, %eax
	jle	.L16
	movq	%r13, %rdi
	call	X509_EXTENSION_get_critical@PLT
	leaq	.LC1(%rip), %rdx
	leaq	.LC19(%rip), %rsi
	movq	%r12, %rdi
	testl	%eax, %eax
	leaq	.LC0(%rip), %rax
	cmovne	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L16
	movl	$16, %ecx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	X509V3_EXT_print@PLT
	testl	%eax, %eax
	jne	.L37
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rdx
	leaq	.LC20(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L16
	movq	%r13, %rdi
	call	X509_EXTENSION_get_data@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	ASN1_STRING_print@PLT
	testl	%eax, %eax
	jle	.L16
.L37:
	movl	$1, %edx
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jle	.L16
	addl	$1, -84(%rbp)
.L33:
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, -84(%rbp)
	jl	.L38
	movq	X509_EXTENSION_free@GOTPCREL(%rip), %rsi
	movq	%r15, %rdi
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L63:
	leaq	-72(%rbp), %rdx
	leaq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	X509_REQ_get0_signature@PLT
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rsi
	movq	%r12, %rdi
	call	X509_signature_print@PLT
	testl	%eax, %eax
	jne	.L1
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L17:
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rdx
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L16
	movq	%r14, %rdi
	call	X509_REQ_get_attr_count@PLT
	movl	$0, -88(%rbp)
	testl	%eax, %eax
	je	.L66
.L20:
	movq	%r14, %rdi
	call	X509_REQ_get_attr_count@PLT
	cmpl	-88(%rbp), %eax
	jle	.L21
	movl	-88(%rbp), %esi
	movq	%r14, %rdi
	call	X509_REQ_get_attr@PLT
	movq	%rax, %rdi
	movq	%rax, -96(%rbp)
	call	X509_ATTRIBUTE_get0_object@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	call	X509_REQ_extension_nid@PLT
	testl	%eax, %eax
	jne	.L29
	movl	%eax, -84(%rbp)
	leaq	.LC1(%rip), %rdx
	xorl	%eax, %eax
	movq	%r12, %rdi
	leaq	.LC13(%rip), %rsi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L16
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	i2a_ASN1_OBJECT@PLT
	movl	-84(%rbp), %ecx
	testl	%eax, %eax
	movl	%eax, %r13d
	jg	.L67
	movl	$0, -108(%rbp)
	movl	$1, %r15d
	movq	$0, -104(%rbp)
.L23:
	movl	$25, %eax
	subl	%r13d, %eax
	movl	%eax, %r13d
	testl	%eax, %eax
	jg	.L27
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L68:
	subl	$1, %r13d
	movl	-84(%rbp), %ecx
	je	.L26
.L27:
	movl	$1, %edx
	leaq	.LC15(%rip), %rsi
	movq	%r12, %rdi
	movl	%ecx, -84(%rbp)
	call	BIO_write@PLT
	cmpl	$1, %eax
	je	.L68
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L26:
	leaq	.LC16(%rip), %rsi
	movq	%r12, %rdi
	movl	%ecx, -84(%rbp)
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L16
	movl	-84(%rbp), %ecx
	cmpl	$22, %ecx
	ja	.L28
	movl	$1, %eax
	salq	%cl, %rax
	testl	$6033408, %eax
	jne	.L69
.L28:
	leaq	.LC17(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L16
.L30:
	movl	-108(%rbp), %ecx
	addl	$1, %ecx
	cmpl	%ecx, %r15d
	jg	.L24
.L29:
	addl	$1, -88(%rbp)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L67:
	movq	-96(%rbp), %rdi
	call	X509_ATTRIBUTE_count@PLT
	movl	-84(%rbp), %ecx
	testl	%eax, %eax
	movl	%eax, %r15d
	je	.L70
.L24:
	movq	-96(%rbp), %rdi
	movl	%ecx, %esi
	movl	%ecx, -84(%rbp)
	call	X509_ATTRIBUTE_get0_type@PLT
	movl	-84(%rbp), %ecx
	movq	8(%rax), %rdx
	movl	%ecx, -108(%rbp)
	movl	(%rax), %ecx
	movq	%rdx, -104(%rbp)
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L66:
	leaq	.LC1(%rip), %rdx
	leaq	.LC12(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jg	.L21
	jmp	.L16
.L69:
	movq	-104(%rbp), %rax
	movq	%r12, %rdi
	movl	(%rax), %edx
	movq	8(%rax), %rsi
	call	BIO_write@PLT
	movq	-104(%rbp), %rdx
	cmpl	(%rdx), %eax
	jne	.L16
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jg	.L30
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L65:
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rdx
	leaq	.LC10(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L16
	movq	%r12, %rdi
	call	ERR_print_errors@PLT
	jmp	.L12
.L70:
	movl	$131, %r8d
	movl	$138, %edx
	movl	$121, %esi
	movl	$11, %edi
	leaq	.LC14(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1
.L64:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1297:
	.size	X509_REQ_print_ex, .-X509_REQ_print_ex
	.p2align 4
	.globl	X509_REQ_print_fp
	.type	X509_REQ_print_fp, @function
X509_REQ_print_fp:
.LFB1296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	testq	%rax, %rax
	je	.L75
	movq	%rax, %r12
	movq	%r14, %rcx
	xorl	%edx, %edx
	movl	$106, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	X509_REQ_print_ex
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	BIO_free@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	movl	$27, %r8d
	leaq	.LC14(%rip), %rcx
	movl	$7, %edx
	xorl	%r13d, %r13d
	movl	$122, %esi
	movl	$11, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1296:
	.size	X509_REQ_print_fp, .-X509_REQ_print_fp
	.p2align 4
	.globl	X509_REQ_print
	.type	X509_REQ_print, @function
X509_REQ_print:
.LFB1298:
	.cfi_startproc
	endbr64
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	jmp	X509_REQ_print_ex
	.cfi_endproc
.LFE1298:
	.size	X509_REQ_print, .-X509_REQ_print
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
