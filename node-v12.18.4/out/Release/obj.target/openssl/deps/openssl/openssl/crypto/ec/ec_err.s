	.file	"ec_err.c"
	.text
	.p2align 4
	.globl	ERR_load_EC_strings
	.type	ERR_load_EC_strings, @function
ERR_load_EC_strings:
.LFB203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$269352960, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_func_error_string@PLT
	testq	%rax, %rax
	je	.L5
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	leaq	EC_str_functs(%rip), %rdi
	call	ERR_load_strings_const@PLT
	leaq	EC_str_reasons(%rip), %rdi
	call	ERR_load_strings_const@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE203:
	.size	ERR_load_EC_strings, .-ERR_load_EC_strings
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"asn1 error"
.LC1:
	.string	"bad signature"
.LC2:
	.string	"bignum out of range"
.LC3:
	.string	"buffer too small"
.LC4:
	.string	"cannot invert"
.LC5:
	.string	"coordinates out of range"
.LC6:
	.string	"curve does not support ecdh"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"curve does not support signing"
	.section	.rodata.str1.1
.LC8:
	.string	"d2i ecpkparameters failure"
.LC9:
	.string	"decode error"
.LC10:
	.string	"discriminant is zero"
.LC11:
	.string	"ec group new by name failure"
.LC12:
	.string	"field too large"
.LC13:
	.string	"gf2m not supported"
.LC14:
	.string	"group2pkparameters failure"
.LC15:
	.string	"i2d ecpkparameters failure"
.LC16:
	.string	"incompatible objects"
.LC17:
	.string	"invalid argument"
.LC18:
	.string	"invalid compressed point"
.LC19:
	.string	"invalid compression bit"
.LC20:
	.string	"invalid curve"
.LC21:
	.string	"invalid digest"
.LC22:
	.string	"invalid digest type"
.LC23:
	.string	"invalid encoding"
.LC24:
	.string	"invalid field"
.LC25:
	.string	"invalid form"
.LC26:
	.string	"invalid group order"
.LC27:
	.string	"invalid key"
.LC28:
	.string	"invalid output length"
.LC29:
	.string	"invalid peer key"
.LC30:
	.string	"invalid pentanomial basis"
.LC31:
	.string	"invalid private key"
.LC32:
	.string	"invalid trinomial basis"
.LC33:
	.string	"kdf parameter error"
.LC34:
	.string	"keys not set"
.LC35:
	.string	"ladder post failure"
.LC36:
	.string	"ladder pre failure"
.LC37:
	.string	"ladder step failure"
.LC38:
	.string	"missing parameters"
.LC39:
	.string	"missing private key"
.LC40:
	.string	"need new setup values"
.LC41:
	.string	"not a NIST prime"
.LC42:
	.string	"not implemented"
.LC43:
	.string	"not initialized"
.LC44:
	.string	"no parameters set"
.LC45:
	.string	"no private value"
.LC46:
	.string	"operation not supported"
.LC47:
	.string	"passed null parameter"
.LC48:
	.string	"peer key error"
.LC49:
	.string	"pkparameters2group failure"
.LC50:
	.string	"point arithmetic failure"
.LC51:
	.string	"point at infinity"
	.section	.rodata.str1.8
	.align 8
.LC52:
	.string	"point coordinates blind failure"
	.section	.rodata.str1.1
.LC53:
	.string	"point is not on curve"
	.section	.rodata.str1.8
	.align 8
.LC54:
	.string	"random number generation failed"
	.section	.rodata.str1.1
.LC55:
	.string	"shared info error"
.LC56:
	.string	"slot full"
.LC57:
	.string	"undefined generator"
.LC58:
	.string	"undefined order"
.LC59:
	.string	"unknown cofactor"
.LC60:
	.string	"unknown group"
.LC61:
	.string	"unknown order"
.LC62:
	.string	"unsupported field"
.LC63:
	.string	"wrong curve parameters"
.LC64:
	.string	"wrong order"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	EC_str_reasons, @object
	.size	EC_str_reasons, 1056
EC_str_reasons:
	.quad	268435571
	.quad	.LC0
	.quad	268435612
	.quad	.LC1
	.quad	268435600
	.quad	.LC2
	.quad	268435556
	.quad	.LC3
	.quad	268435621
	.quad	.LC4
	.quad	268435602
	.quad	.LC5
	.quad	268435616
	.quad	.LC6
	.quad	268435615
	.quad	.LC7
	.quad	268435573
	.quad	.LC8
	.quad	268435598
	.quad	.LC9
	.quad	268435574
	.quad	.LC10
	.quad	268435575
	.quad	.LC11
	.quad	268435599
	.quad	.LC12
	.quad	268435603
	.quad	.LC13
	.quad	268435576
	.quad	.LC14
	.quad	268435577
	.quad	.LC15
	.quad	268435557
	.quad	.LC16
	.quad	268435568
	.quad	.LC17
	.quad	268435566
	.quad	.LC18
	.quad	268435565
	.quad	.LC19
	.quad	268435597
	.quad	.LC20
	.quad	268435607
	.quad	.LC21
	.quad	268435594
	.quad	.LC22
	.quad	268435558
	.quad	.LC23
	.quad	268435559
	.quad	.LC24
	.quad	268435560
	.quad	.LC25
	.quad	268435578
	.quad	.LC26
	.quad	268435572
	.quad	.LC27
	.quad	268435617
	.quad	.LC28
	.quad	268435589
	.quad	.LC29
	.quad	268435588
	.quad	.LC30
	.quad	268435579
	.quad	.LC31
	.quad	268435593
	.quad	.LC32
	.quad	268435604
	.quad	.LC33
	.quad	268435596
	.quad	.LC34
	.quad	268435592
	.quad	.LC35
	.quad	268435609
	.quad	.LC36
	.quad	268435618
	.quad	.LC37
	.quad	268435580
	.quad	.LC38
	.quad	268435581
	.quad	.LC39
	.quad	268435613
	.quad	.LC40
	.quad	268435591
	.quad	.LC41
	.quad	268435582
	.quad	.LC42
	.quad	268435567
	.quad	.LC43
	.quad	268435595
	.quad	.LC44
	.quad	268435610
	.quad	.LC45
	.quad	268435608
	.quad	.LC46
	.quad	268435590
	.quad	.LC47
	.quad	268435605
	.quad	.LC48
	.quad	268435583
	.quad	.LC49
	.quad	268435611
	.quad	.LC50
	.quad	268435562
	.quad	.LC51
	.quad	268435619
	.quad	.LC52
	.quad	268435563
	.quad	.LC53
	.quad	268435614
	.quad	.LC54
	.quad	268435606
	.quad	.LC55
	.quad	268435564
	.quad	.LC56
	.quad	268435569
	.quad	.LC57
	.quad	268435584
	.quad	.LC58
	.quad	268435620
	.quad	.LC59
	.quad	268435585
	.quad	.LC60
	.quad	268435570
	.quad	.LC61
	.quad	268435587
	.quad	.LC62
	.quad	268435601
	.quad	.LC63
	.quad	268435586
	.quad	.LC64
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC65:
	.string	"BN_to_felem"
.LC66:
	.string	"d2i_ECParameters"
.LC67:
	.string	"d2i_ECPKParameters"
.LC68:
	.string	"d2i_ECPrivateKey"
.LC69:
	.string	"do_EC_KEY_print"
.LC70:
	.string	"ecdh_cms_decrypt"
.LC71:
	.string	"ecdh_cms_set_shared_info"
.LC72:
	.string	"ECDH_compute_key"
.LC73:
	.string	"ecdh_simple_compute_key"
.LC74:
	.string	"ECDSA_do_sign_ex"
.LC75:
	.string	"ECDSA_do_verify"
.LC76:
	.string	"ECDSA_sign_ex"
.LC77:
	.string	"ECDSA_sign_setup"
.LC78:
	.string	"ECDSA_SIG_new"
.LC79:
	.string	"ECDSA_verify"
.LC80:
	.string	"ecd_item_verify"
.LC81:
	.string	"eckey_param2type"
.LC82:
	.string	"eckey_param_decode"
.LC83:
	.string	"eckey_priv_decode"
.LC84:
	.string	"eckey_priv_encode"
.LC85:
	.string	"eckey_pub_decode"
.LC86:
	.string	"eckey_pub_encode"
.LC87:
	.string	"eckey_type2param"
.LC88:
	.string	"ECParameters_print"
.LC89:
	.string	"ECParameters_print_fp"
.LC90:
	.string	"ECPKParameters_print"
.LC91:
	.string	"ECPKParameters_print_fp"
.LC92:
	.string	"ecp_nistz256_get_affine"
.LC93:
	.string	"ecp_nistz256_inv_mod_ord"
.LC94:
	.string	"ecp_nistz256_mult_precompute"
.LC95:
	.string	"ecp_nistz256_points_mul"
.LC96:
	.string	"ecp_nistz256_pre_comp_new"
.LC97:
	.string	"ecp_nistz256_windowed_mul"
.LC98:
	.string	"ecx_key_op"
.LC99:
	.string	"ecx_priv_encode"
.LC100:
	.string	"ecx_pub_encode"
.LC101:
	.string	"ec_asn1_group2curve"
.LC102:
	.string	"ec_asn1_group2fieldid"
	.section	.rodata.str1.8
	.align 8
.LC103:
	.string	"ec_GF2m_montgomery_point_multiply"
	.section	.rodata.str1.1
.LC104:
	.string	"ec_GF2m_simple_field_inv"
	.section	.rodata.str1.8
	.align 8
.LC105:
	.string	"ec_GF2m_simple_group_check_discriminant"
	.align 8
.LC106:
	.string	"ec_GF2m_simple_group_set_curve"
	.section	.rodata.str1.1
.LC107:
	.string	"ec_GF2m_simple_ladder_post"
.LC108:
	.string	"ec_GF2m_simple_ladder_pre"
.LC109:
	.string	"ec_GF2m_simple_oct2point"
.LC110:
	.string	"ec_GF2m_simple_point2oct"
.LC111:
	.string	"ec_GF2m_simple_points_mul"
	.section	.rodata.str1.8
	.align 8
.LC112:
	.string	"ec_GF2m_simple_point_get_affine_coordinates"
	.align 8
.LC113:
	.string	"ec_GF2m_simple_point_set_affine_coordinates"
	.align 8
.LC114:
	.string	"ec_GF2m_simple_set_compressed_coordinates"
	.section	.rodata.str1.1
.LC115:
	.string	"ec_GFp_mont_field_decode"
.LC116:
	.string	"ec_GFp_mont_field_encode"
.LC117:
	.string	"ec_GFp_mont_field_inv"
.LC118:
	.string	"ec_GFp_mont_field_mul"
.LC119:
	.string	"ec_GFp_mont_field_set_to_one"
.LC120:
	.string	"ec_GFp_mont_field_sqr"
.LC121:
	.string	"ec_GFp_mont_group_set_curve"
	.section	.rodata.str1.8
	.align 8
.LC122:
	.string	"ec_GFp_nistp224_group_set_curve"
	.section	.rodata.str1.1
.LC123:
	.string	"ec_GFp_nistp224_points_mul"
	.section	.rodata.str1.8
	.align 8
.LC124:
	.string	"ec_GFp_nistp224_point_get_affine_coordinates"
	.align 8
.LC125:
	.string	"ec_GFp_nistp256_group_set_curve"
	.section	.rodata.str1.1
.LC126:
	.string	"ec_GFp_nistp256_points_mul"
	.section	.rodata.str1.8
	.align 8
.LC127:
	.string	"ec_GFp_nistp256_point_get_affine_coordinates"
	.align 8
.LC128:
	.string	"ec_GFp_nistp521_group_set_curve"
	.section	.rodata.str1.1
.LC129:
	.string	"ec_GFp_nistp521_points_mul"
	.section	.rodata.str1.8
	.align 8
.LC130:
	.string	"ec_GFp_nistp521_point_get_affine_coordinates"
	.section	.rodata.str1.1
.LC131:
	.string	"ec_GFp_nist_field_mul"
.LC132:
	.string	"ec_GFp_nist_field_sqr"
.LC133:
	.string	"ec_GFp_nist_group_set_curve"
	.section	.rodata.str1.8
	.align 8
.LC134:
	.string	"ec_GFp_simple_blind_coordinates"
	.section	.rodata.str1.1
.LC135:
	.string	"ec_GFp_simple_field_inv"
	.section	.rodata.str1.8
	.align 8
.LC136:
	.string	"ec_GFp_simple_group_check_discriminant"
	.section	.rodata.str1.1
.LC137:
	.string	"ec_GFp_simple_group_set_curve"
.LC138:
	.string	"ec_GFp_simple_make_affine"
.LC139:
	.string	"ec_GFp_simple_oct2point"
.LC140:
	.string	"ec_GFp_simple_point2oct"
	.section	.rodata.str1.8
	.align 8
.LC141:
	.string	"ec_GFp_simple_points_make_affine"
	.align 8
.LC142:
	.string	"ec_GFp_simple_point_get_affine_coordinates"
	.align 8
.LC143:
	.string	"ec_GFp_simple_point_set_affine_coordinates"
	.align 8
.LC144:
	.string	"ec_GFp_simple_set_compressed_coordinates"
	.section	.rodata.str1.1
.LC145:
	.string	"EC_GROUP_check"
.LC146:
	.string	"EC_GROUP_check_discriminant"
.LC147:
	.string	"EC_GROUP_copy"
.LC148:
	.string	"EC_GROUP_get_curve"
.LC149:
	.string	"EC_GROUP_get_curve_GF2m"
.LC150:
	.string	"EC_GROUP_get_curve_GFp"
.LC151:
	.string	"EC_GROUP_get_degree"
.LC152:
	.string	"EC_GROUP_get_ecparameters"
.LC153:
	.string	"EC_GROUP_get_ecpkparameters"
	.section	.rodata.str1.8
	.align 8
.LC154:
	.string	"EC_GROUP_get_pentanomial_basis"
	.section	.rodata.str1.1
.LC155:
	.string	"EC_GROUP_get_trinomial_basis"
.LC156:
	.string	"EC_GROUP_new"
.LC157:
	.string	"EC_GROUP_new_by_curve_name"
.LC158:
	.string	"ec_group_new_from_data"
	.section	.rodata.str1.8
	.align 8
.LC159:
	.string	"EC_GROUP_new_from_ecparameters"
	.align 8
.LC160:
	.string	"EC_GROUP_new_from_ecpkparameters"
	.section	.rodata.str1.1
.LC161:
	.string	"EC_GROUP_set_curve"
.LC162:
	.string	"EC_GROUP_set_curve_GF2m"
.LC163:
	.string	"EC_GROUP_set_curve_GFp"
.LC164:
	.string	"EC_GROUP_set_generator"
.LC165:
	.string	"EC_GROUP_set_seed"
.LC166:
	.string	"EC_KEY_check_key"
.LC167:
	.string	"EC_KEY_copy"
.LC168:
	.string	"EC_KEY_generate_key"
.LC169:
	.string	"EC_KEY_new"
.LC170:
	.string	"EC_KEY_new_method"
.LC171:
	.string	"EC_KEY_oct2priv"
.LC172:
	.string	"EC_KEY_print"
.LC173:
	.string	"EC_KEY_print_fp"
.LC174:
	.string	"EC_KEY_priv2buf"
.LC175:
	.string	"EC_KEY_priv2oct"
	.section	.rodata.str1.8
	.align 8
.LC176:
	.string	"EC_KEY_set_public_key_affine_coordinates"
	.section	.rodata.str1.1
.LC177:
	.string	"ec_key_simple_check_key"
.LC178:
	.string	"ec_key_simple_oct2priv"
.LC179:
	.string	"ec_key_simple_priv2oct"
.LC180:
	.string	"ec_pkey_check"
.LC181:
	.string	"ec_pkey_param_check"
.LC182:
	.string	"EC_POINTs_make_affine"
.LC183:
	.string	"EC_POINTs_mul"
.LC184:
	.string	"EC_POINT_add"
.LC185:
	.string	"EC_POINT_bn2point"
.LC186:
	.string	"EC_POINT_cmp"
.LC187:
	.string	"EC_POINT_copy"
.LC188:
	.string	"EC_POINT_dbl"
	.section	.rodata.str1.8
	.align 8
.LC189:
	.string	"EC_POINT_get_affine_coordinates"
	.align 8
.LC190:
	.string	"EC_POINT_get_affine_coordinates_GF2m"
	.align 8
.LC191:
	.string	"EC_POINT_get_affine_coordinates_GFp"
	.align 8
.LC192:
	.string	"EC_POINT_get_Jprojective_coordinates_GFp"
	.section	.rodata.str1.1
.LC193:
	.string	"EC_POINT_invert"
.LC194:
	.string	"EC_POINT_is_at_infinity"
.LC195:
	.string	"EC_POINT_is_on_curve"
.LC196:
	.string	"EC_POINT_make_affine"
.LC197:
	.string	"EC_POINT_new"
.LC198:
	.string	"EC_POINT_oct2point"
.LC199:
	.string	"EC_POINT_point2buf"
.LC200:
	.string	"EC_POINT_point2oct"
	.section	.rodata.str1.8
	.align 8
.LC201:
	.string	"EC_POINT_set_affine_coordinates"
	.align 8
.LC202:
	.string	"EC_POINT_set_affine_coordinates_GF2m"
	.align 8
.LC203:
	.string	"EC_POINT_set_affine_coordinates_GFp"
	.align 8
.LC204:
	.string	"EC_POINT_set_compressed_coordinates"
	.align 8
.LC205:
	.string	"EC_POINT_set_compressed_coordinates_GF2m"
	.align 8
.LC206:
	.string	"EC_POINT_set_compressed_coordinates_GFp"
	.align 8
.LC207:
	.string	"EC_POINT_set_Jprojective_coordinates_GFp"
	.section	.rodata.str1.1
.LC208:
	.string	"EC_POINT_set_to_infinity"
.LC209:
	.string	"ec_pre_comp_new"
.LC210:
	.string	"ec_scalar_mul_ladder"
.LC211:
	.string	"ec_wNAF_mul"
.LC212:
	.string	"ec_wNAF_precompute_mult"
.LC213:
	.string	"i2d_ECParameters"
.LC214:
	.string	"i2d_ECPKParameters"
.LC215:
	.string	"i2d_ECPrivateKey"
.LC216:
	.string	"i2o_ECPublicKey"
.LC217:
	.string	"nistp224_pre_comp_new"
.LC218:
	.string	"nistp256_pre_comp_new"
.LC219:
	.string	"nistp521_pre_comp_new"
.LC220:
	.string	"o2i_ECPublicKey"
.LC221:
	.string	"old_ec_priv_decode"
.LC222:
	.string	"ossl_ecdh_compute_key"
.LC223:
	.string	"ossl_ecdsa_sign_sig"
.LC224:
	.string	"ossl_ecdsa_verify_sig"
.LC225:
	.string	"pkey_ecd_ctrl"
.LC226:
	.string	"pkey_ecd_digestsign"
.LC227:
	.string	"pkey_ecd_digestsign25519"
.LC228:
	.string	"pkey_ecd_digestsign448"
.LC229:
	.string	"pkey_ecx_derive"
.LC230:
	.string	"pkey_ec_ctrl"
.LC231:
	.string	"pkey_ec_ctrl_str"
.LC232:
	.string	"pkey_ec_derive"
.LC233:
	.string	"pkey_ec_init"
.LC234:
	.string	"pkey_ec_kdf_derive"
.LC235:
	.string	"pkey_ec_keygen"
.LC236:
	.string	"pkey_ec_paramgen"
.LC237:
	.string	"pkey_ec_sign"
.LC238:
	.string	"validate_ecx_derive"
	.section	.data.rel.ro.local
	.align 32
	.type	EC_str_functs, @object
	.size	EC_str_functs, 2800
EC_str_functs:
	.quad	269352960
	.quad	.LC65
	.quad	269025280
	.quad	.LC66
	.quad	269029376
	.quad	.LC67
	.quad	269033472
	.quad	.LC68
	.quad	269340672
	.quad	.LC69
	.quad	269410304
	.quad	.LC70
	.quad	269414400
	.quad	.LC71
	.quad	269443072
	.quad	.LC72
	.quad	269488128
	.quad	.LC73
	.quad	269463552
	.quad	.LC74
	.quad	269467648
	.quad	.LC75
	.quad	269475840
	.quad	.LC76
	.quad	269451264
	.quad	.LC77
	.quad	269520896
	.quad	.LC78
	.quad	269471744
	.quad	.LC79
	.quad	269541376
	.quad	.LC80
	.quad	269348864
	.quad	.LC81
	.quad	269303808
	.quad	.LC82
	.quad	269307904
	.quad	.LC83
	.quad	269312000
	.quad	.LC84
	.quad	269316096
	.quad	.LC85
	.quad	269320192
	.quad	.LC86
	.quad	269336576
	.quad	.LC87
	.quad	269037568
	.quad	.LC88
	.quad	269041664
	.quad	.LC89
	.quad	269045760
	.quad	.LC90
	.quad	269049856
	.quad	.LC91
	.quad	269418496
	.quad	.LC92
	.quad	269561856
	.quad	.LC93
	.quad	269430784
	.quad	.LC94
	.quad	269422592
	.quad	.LC95
	.quad	269434880
	.quad	.LC96
	.quad	269426688
	.quad	.LC97
	.quad	269524992
	.quad	.LC98
	.quad	269529088
	.quad	.LC99
	.quad	269533184
	.quad	.LC100
	.quad	269062144
	.quad	.LC101
	.quad	269066240
	.quad	.LC102
	.quad	269287424
	.quad	.LC103
	.quad	269647872
	.quad	.LC104
	.quad	269086720
	.quad	.LC105
	.quad	269234176
	.quad	.LC106
	.quad	269602816
	.quad	.LC107
	.quad	269615104
	.quad	.LC108
	.quad	269090816
	.quad	.LC109
	.quad	269094912
	.quad	.LC110
	.quad	269619200
	.quad	.LC111
	.quad	269099008
	.quad	.LC112
	.quad	269103104
	.quad	.LC113
	.quad	269107200
	.quad	.LC114
	.quad	268980224
	.quad	.LC115
	.quad	268984320
	.quad	.LC116
	.quad	269651968
	.quad	.LC117
	.quad	268972032
	.quad	.LC118
	.quad	269291520
	.quad	.LC119
	.quad	268976128
	.quad	.LC120
	.quad	269209600
	.quad	.LC121
	.quad	269357056
	.quad	.LC122
	.quad	269369344
	.quad	.LC123
	.quad	269361152
	.quad	.LC124
	.quad	269377536
	.quad	.LC125
	.quad	269381632
	.quad	.LC126
	.quad	269385728
	.quad	.LC127
	.quad	269389824
	.quad	.LC128
	.quad	269393920
	.quad	.LC129
	.quad	269398016
	.quad	.LC130
	.quad	269254656
	.quad	.LC131
	.quad	269258752
	.quad	.LC132
	.quad	269262848
	.quad	.LC133
	.quad	269611008
	.quad	.LC134
	.quad	269656064
	.quad	.LC135
	.quad	269111296
	.quad	.LC136
	.quad	269115392
	.quad	.LC137
	.quad	268853248
	.quad	.LC138
	.quad	268857344
	.quad	.LC139
	.quad	268861440
	.quad	.LC140
	.quad	268996608
	.quad	.LC141
	.quad	269119488
	.quad	.LC142
	.quad	269123584
	.quad	.LC143
	.quad	269127680
	.quad	.LC144
	.quad	269131776
	.quad	.LC145
	.quad	269135872
	.quad	.LC146
	.quad	268869632
	.quad	.LC147
	.quad	269627392
	.quad	.LC148
	.quad	269139968
	.quad	.LC149
	.quad	268967936
	.quad	.LC150
	.quad	269144064
	.quad	.LC151
	.quad	269504512
	.quad	.LC152
	.quad	269508608
	.quad	.LC153
	.quad	269225984
	.quad	.LC154
	.quad	269230080
	.quad	.LC155
	.quad	268877824
	.quad	.LC156
	.quad	269148160
	.quad	.LC157
	.quad	269152256
	.quad	.LC158
	.quad	269512704
	.quad	.LC159
	.quad	269516800
	.quad	.LC160
	.quad	269631488
	.quad	.LC161
	.quad	269156352
	.quad	.LC162
	.quad	268881920
	.quad	.LC163
	.quad	268890112
	.quad	.LC164
	.quad	269606912
	.quad	.LC165
	.quad	269160448
	.quad	.LC166
	.quad	269164544
	.quad	.LC167
	.quad	269168640
	.quad	.LC168
	.quad	269180928
	.quad	.LC169
	.quad	269438976
	.quad	.LC170
	.quad	269479936
	.quad	.LC171
	.quad	269172736
	.quad	.LC172
	.quad	269176832
	.quad	.LC173
	.quad	269578240
	.quad	.LC174
	.quad	269484032
	.quad	.LC175
	.quad	269373440
	.quad	.LC176
	.quad	269492224
	.quad	.LC177
	.quad	269496320
	.quad	.LC178
	.quad	269500416
	.quad	.LC179
	.quad	269553664
	.quad	.LC180
	.quad	269557760
	.quad	.LC181
	.quad	268992512
	.quad	.LC182
	.quad	269623296
	.quad	.LC183
	.quad	268894208
	.quad	.LC184
	.quad	269582336
	.quad	.LC185
	.quad	268898304
	.quad	.LC186
	.quad	268902400
	.quad	.LC187
	.quad	268906496
	.quad	.LC188
	.quad	269635584
	.quad	.LC189
	.quad	269185024
	.quad	.LC190
	.quad	268910592
	.quad	.LC191
	.quad	268914688
	.quad	.LC192
	.quad	269295616
	.quad	.LC193
	.quad	268918784
	.quad	.LC194
	.quad	268922880
	.quad	.LC195
	.quad	268926976
	.quad	.LC196
	.quad	268931072
	.quad	.LC197
	.quad	268935168
	.quad	.LC198
	.quad	269586432
	.quad	.LC199
	.quad	268939264
	.quad	.LC200
	.quad	269639680
	.quad	.LC201
	.quad	269193216
	.quad	.LC202
	.quad	268943360
	.quad	.LC203
	.quad	269643776
	.quad	.LC204
	.quad	269197312
	.quad	.LC205
	.quad	268947456
	.quad	.LC206
	.quad	268951552
	.quad	.LC207
	.quad	268955648
	.quad	.LC208
	.quad	269238272
	.quad	.LC209
	.quad	269598720
	.quad	.LC210
	.quad	269201408
	.quad	.LC211
	.quad	269205504
	.quad	.LC212
	.quad	269213696
	.quad	.LC213
	.quad	269217792
	.quad	.LC214
	.quad	269221888
	.quad	.LC215
	.quad	269053952
	.quad	.LC216
	.quad	269365248
	.quad	.LC217
	.quad	269402112
	.quad	.LC218
	.quad	269406208
	.quad	.LC219
	.quad	269058048
	.quad	.LC220
	.quad	269344768
	.quad	.LC221
	.quad	269447168
	.quad	.LC222
	.quad	269455360
	.quad	.LC223
	.quad	269459456
	.quad	.LC224
	.quad	269545472
	.quad	.LC225
	.quad	269549568
	.quad	.LC226
	.quad	269565952
	.quad	.LC227
	.quad	269570048
	.quad	.LC228
	.quad	269537280
	.quad	.LC229
	.quad	269242368
	.quad	.LC230
	.quad	269246464
	.quad	.LC231
	.quad	269324288
	.quad	.LC232
	.quad	269590528
	.quad	.LC233
	.quad	269594624
	.quad	.LC234
	.quad	269250560
	.quad	.LC235
	.quad	269332480
	.quad	.LC236
	.quad	269328384
	.quad	.LC237
	.quad	269574144
	.quad	.LC238
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
