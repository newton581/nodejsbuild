	.file	"cms_att.c"
	.text
	.p2align 4
	.globl	CMS_signed_get_attr_count
	.type	CMS_signed_get_attr_count, @function
CMS_signed_get_attr_count:
.LFB1392:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdi
	jmp	X509at_get_attr_count@PLT
	.cfi_endproc
.LFE1392:
	.size	CMS_signed_get_attr_count, .-CMS_signed_get_attr_count
	.p2align 4
	.globl	CMS_signed_get_attr_by_NID
	.type	CMS_signed_get_attr_by_NID, @function
CMS_signed_get_attr_by_NID:
.LFB1393:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdi
	jmp	X509at_get_attr_by_NID@PLT
	.cfi_endproc
.LFE1393:
	.size	CMS_signed_get_attr_by_NID, .-CMS_signed_get_attr_by_NID
	.p2align 4
	.globl	CMS_signed_get_attr_by_OBJ
	.type	CMS_signed_get_attr_by_OBJ, @function
CMS_signed_get_attr_by_OBJ:
.LFB1394:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdi
	jmp	X509at_get_attr_by_OBJ@PLT
	.cfi_endproc
.LFE1394:
	.size	CMS_signed_get_attr_by_OBJ, .-CMS_signed_get_attr_by_OBJ
	.p2align 4
	.globl	CMS_signed_get_attr
	.type	CMS_signed_get_attr, @function
CMS_signed_get_attr:
.LFB1395:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdi
	jmp	X509at_get_attr@PLT
	.cfi_endproc
.LFE1395:
	.size	CMS_signed_get_attr, .-CMS_signed_get_attr
	.p2align 4
	.globl	CMS_signed_delete_attr
	.type	CMS_signed_delete_attr, @function
CMS_signed_delete_attr:
.LFB1396:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdi
	jmp	X509at_delete_attr@PLT
	.cfi_endproc
.LFE1396:
	.size	CMS_signed_delete_attr, .-CMS_signed_delete_attr
	.p2align 4
	.globl	CMS_signed_add1_attr
	.type	CMS_signed_add1_attr, @function
CMS_signed_add1_attr:
.LFB1397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$24, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509at_add1_attr@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1397:
	.size	CMS_signed_add1_attr, .-CMS_signed_add1_attr
	.p2align 4
	.globl	CMS_signed_add1_attr_by_OBJ
	.type	CMS_signed_add1_attr_by_OBJ, @function
CMS_signed_add1_attr_by_OBJ:
.LFB1398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$24, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509at_add1_attr_by_OBJ@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1398:
	.size	CMS_signed_add1_attr_by_OBJ, .-CMS_signed_add1_attr_by_OBJ
	.p2align 4
	.globl	CMS_signed_add1_attr_by_NID
	.type	CMS_signed_add1_attr_by_NID, @function
CMS_signed_add1_attr_by_NID:
.LFB1399:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$24, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509at_add1_attr_by_NID@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1399:
	.size	CMS_signed_add1_attr_by_NID, .-CMS_signed_add1_attr_by_NID
	.p2align 4
	.globl	CMS_signed_add1_attr_by_txt
	.type	CMS_signed_add1_attr_by_txt, @function
CMS_signed_add1_attr_by_txt:
.LFB1400:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$24, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509at_add1_attr_by_txt@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1400:
	.size	CMS_signed_add1_attr_by_txt, .-CMS_signed_add1_attr_by_txt
	.p2align 4
	.globl	CMS_signed_get0_data_by_OBJ
	.type	CMS_signed_get0_data_by_OBJ, @function
CMS_signed_get0_data_by_OBJ:
.LFB1401:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdi
	jmp	X509at_get0_data_by_OBJ@PLT
	.cfi_endproc
.LFE1401:
	.size	CMS_signed_get0_data_by_OBJ, .-CMS_signed_get0_data_by_OBJ
	.p2align 4
	.globl	CMS_unsigned_get_attr_count
	.type	CMS_unsigned_get_attr_count, @function
CMS_unsigned_get_attr_count:
.LFB1402:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rdi
	jmp	X509at_get_attr_count@PLT
	.cfi_endproc
.LFE1402:
	.size	CMS_unsigned_get_attr_count, .-CMS_unsigned_get_attr_count
	.p2align 4
	.globl	CMS_unsigned_get_attr_by_NID
	.type	CMS_unsigned_get_attr_by_NID, @function
CMS_unsigned_get_attr_by_NID:
.LFB1403:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rdi
	jmp	X509at_get_attr_by_NID@PLT
	.cfi_endproc
.LFE1403:
	.size	CMS_unsigned_get_attr_by_NID, .-CMS_unsigned_get_attr_by_NID
	.p2align 4
	.globl	CMS_unsigned_get_attr_by_OBJ
	.type	CMS_unsigned_get_attr_by_OBJ, @function
CMS_unsigned_get_attr_by_OBJ:
.LFB1404:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rdi
	jmp	X509at_get_attr_by_OBJ@PLT
	.cfi_endproc
.LFE1404:
	.size	CMS_unsigned_get_attr_by_OBJ, .-CMS_unsigned_get_attr_by_OBJ
	.p2align 4
	.globl	CMS_unsigned_get_attr
	.type	CMS_unsigned_get_attr, @function
CMS_unsigned_get_attr:
.LFB1405:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rdi
	jmp	X509at_get_attr@PLT
	.cfi_endproc
.LFE1405:
	.size	CMS_unsigned_get_attr, .-CMS_unsigned_get_attr
	.p2align 4
	.globl	CMS_unsigned_delete_attr
	.type	CMS_unsigned_delete_attr, @function
CMS_unsigned_delete_attr:
.LFB1406:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rdi
	jmp	X509at_delete_attr@PLT
	.cfi_endproc
.LFE1406:
	.size	CMS_unsigned_delete_attr, .-CMS_unsigned_delete_attr
	.p2align 4
	.globl	CMS_unsigned_add1_attr
	.type	CMS_unsigned_add1_attr, @function
CMS_unsigned_add1_attr:
.LFB1407:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$48, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509at_add1_attr@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1407:
	.size	CMS_unsigned_add1_attr, .-CMS_unsigned_add1_attr
	.p2align 4
	.globl	CMS_unsigned_add1_attr_by_OBJ
	.type	CMS_unsigned_add1_attr_by_OBJ, @function
CMS_unsigned_add1_attr_by_OBJ:
.LFB1408:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$48, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509at_add1_attr_by_OBJ@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1408:
	.size	CMS_unsigned_add1_attr_by_OBJ, .-CMS_unsigned_add1_attr_by_OBJ
	.p2align 4
	.globl	CMS_unsigned_add1_attr_by_NID
	.type	CMS_unsigned_add1_attr_by_NID, @function
CMS_unsigned_add1_attr_by_NID:
.LFB1409:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$48, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509at_add1_attr_by_NID@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1409:
	.size	CMS_unsigned_add1_attr_by_NID, .-CMS_unsigned_add1_attr_by_NID
	.p2align 4
	.globl	CMS_unsigned_add1_attr_by_txt
	.type	CMS_unsigned_add1_attr_by_txt, @function
CMS_unsigned_add1_attr_by_txt:
.LFB1410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$48, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509at_add1_attr_by_txt@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1410:
	.size	CMS_unsigned_add1_attr_by_txt, .-CMS_unsigned_add1_attr_by_txt
	.p2align 4
	.globl	CMS_unsigned_get0_data_by_OBJ
	.type	CMS_unsigned_get0_data_by_OBJ, @function
CMS_unsigned_get0_data_by_OBJ:
.LFB1411:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rdi
	jmp	X509at_get0_data_by_OBJ@PLT
	.cfi_endproc
.LFE1411:
	.size	CMS_unsigned_get0_data_by_OBJ, .-CMS_unsigned_get0_data_by_OBJ
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/cms/cms_att.c"
	.text
	.p2align 4
	.globl	CMS_si_check_attributes
	.type	CMS_si_check_attributes, @function
CMS_si_check_attributes:
.LFB1414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	cms_attribute_properties(%rip), %rbx
	subq	$24, %rsp
	movq	24(%rdi), %rdi
	call	X509at_get_attr_count@PLT
	movq	48(%r14), %rdi
	movl	%eax, -56(%rbp)
	call	X509at_get_attr_count@PLT
	movl	%eax, -60(%rbp)
.L47:
	movl	(%rbx), %r13d
	movq	24(%r14), %r15
	movl	$-1, %edx
	movl	4(%rbx), %r12d
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	X509at_get_attr_by_NID@PLT
	testl	%eax, %eax
	js	.L31
	movq	%r15, %rdi
	movl	%eax, %esi
	movl	%eax, -52(%rbp)
	call	X509at_get_attr@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L31
	call	X509_ATTRIBUTE_count@PLT
	testb	$1, %r12b
	movl	-52(%rbp), %edx
	movl	%eax, %ecx
	je	.L38
	testb	$32, %r12b
	jne	.L34
.L37:
	testb	$64, %r12b
	je	.L74
	cmpl	$1, %ecx
	jne	.L38
	.p2align 4,,10
	.p2align 3
.L41:
	movq	48(%r14), %r15
	movl	$-1, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	X509at_get_attr_by_NID@PLT
	testl	%eax, %eax
	js	.L39
	movq	%r15, %rdi
	movl	%eax, %esi
	movl	%eax, -52(%rbp)
	call	X509at_get_attr@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L39
	call	X509_ATTRIBUTE_count@PLT
	movl	%eax, %ecx
	testb	$2, %r12b
	je	.L38
	testb	$32, %r12b
	movl	-52(%rbp), %edx
	jne	.L42
.L45:
	andl	$64, %r12d
	je	.L75
	cmpl	$1, %ecx
	je	.L46
.L38:
	movl	$279, %r8d
	movl	$161, %edx
	movl	$183, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jle	.L46
	andl	$18, %r12d
	cmpl	$18, %r12d
	je	.L38
.L46:
	addq	$8, %rbx
	leaq	56+cms_attribute_properties(%rip), %rax
	cmpq	%rbx, %rax
	jne	.L47
.L76:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	movl	-56(%rbp), %edx
	testl	%edx, %edx
	jle	.L41
	movl	%r12d, %eax
	andl	$17, %eax
	cmpl	$17, %eax
	jne	.L41
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L74:
	testl	%ecx, %ecx
	je	.L38
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L75:
	testl	%ecx, %ecx
	je	.L38
	addq	$8, %rbx
	leaq	56+cms_attribute_properties(%rip), %rax
	cmpq	%rbx, %rax
	jne	.L47
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L34:
	movl	%r13d, %esi
	movq	%r15, %rdi
	movl	%eax, -52(%rbp)
	call	X509at_get_attr_by_NID@PLT
	movl	-52(%rbp), %ecx
	testl	%eax, %eax
	movl	%eax, %esi
	js	.L37
	movq	%r15, %rdi
	call	X509at_get_attr@PLT
	movl	-52(%rbp), %ecx
	testq	%rax, %rax
	jne	.L38
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L42:
	movl	%r13d, %esi
	movq	%r15, %rdi
	movl	%eax, -52(%rbp)
	call	X509at_get_attr_by_NID@PLT
	movl	-52(%rbp), %ecx
	testl	%eax, %eax
	movl	%eax, %esi
	js	.L45
	movq	%r15, %rdi
	call	X509at_get_attr@PLT
	movl	-52(%rbp), %ecx
	testq	%rax, %rax
	jne	.L38
	jmp	.L45
	.cfi_endproc
.LFE1414:
	.size	CMS_si_check_attributes, .-CMS_si_check_attributes
	.section	.rodata
	.align 32
	.type	cms_attribute_properties, @object
	.size	cms_attribute_properties, 56
cms_attribute_properties:
	.long	50
	.long	113
	.long	51
	.long	113
	.long	52
	.long	97
	.long	53
	.long	2
	.long	223
	.long	97
	.long	1086
	.long	97
	.long	212
	.long	97
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
