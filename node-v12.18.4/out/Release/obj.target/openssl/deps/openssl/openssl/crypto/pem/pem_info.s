	.file	"pem_info.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/pem/pem_info.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"CERTIFICATE"
.LC2:
	.string	"X509 CERTIFICATE"
.LC3:
	.string	"TRUSTED CERTIFICATE"
.LC4:
	.string	"X509 CRL"
.LC5:
	.string	"RSA PRIVATE KEY"
.LC6:
	.string	"DSA PRIVATE KEY"
.LC7:
	.string	"EC PRIVATE KEY"
	.text
	.p2align 4
	.globl	PEM_X509_INFO_read_bio
	.type	PEM_X509_INFO_read_bio, @function
PEM_X509_INFO_read_bio:
.LFB780:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -136(%rbp)
	movq	%rsi, -176(%rbp)
	movq	%rdx, -184(%rbp)
	movq	%rcx, -192(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	testq	%rsi, %rsi
	je	.L122
.L2:
	call	X509_INFO_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L4
	leaq	-88(%rbp), %rax
	leaq	.LC1(%rip), %rbx
	movq	%rax, -144(%rbp)
	leaq	-104(%rbp), %rax
	leaq	.LC2(%rip), %r14
	movq	%rax, -168(%rbp)
	leaq	-112(%rbp), %rax
	leaq	.LC3(%rip), %r15
	movq	%rax, -160(%rbp)
	leaq	-120(%rbp), %rax
	movq	%rax, -152(%rbp)
.L31:
	movq	-144(%rbp), %r8
	movq	-168(%rbp), %rcx
	movq	-160(%rbp), %rdx
	movq	-152(%rbp), %rsi
	movq	-136(%rbp), %rdi
	call	PEM_read_bio@PLT
	testl	%eax, %eax
	jne	.L5
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L125:
	movl	$17, %ecx
	movq	%r8, %rsi
	movq	%r14, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L9
	movl	$20, %ecx
	movq	%r8, %rsi
	movq	%r15, %rdi
	repz cmpsb
	seta	%r10b
	sbbb	$0, %r10b
	movsbl	%r10b, %r10d
	testl	%r10d, %r10d
	jne	.L15
	cmpq	$0, (%r12)
	je	.L16
	.p2align 4,,10
	.p2align 3
.L120:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L12
	call	X509_INFO_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L124
.L5:
	movq	-120(%rbp), %r8
	movl	$12, %ecx
	movq	%rbx, %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L125
.L9:
	cmpq	$0, (%r12)
	jne	.L120
	movq	d2i_X509@GOTPCREL(%rip), %rax
	movq	-112(%rbp), %rdi
	movq	%r12, %r9
	xorl	%r10d, %r10d
	movl	$0, -200(%rbp)
	movq	%rax, -208(%rbp)
.L14:
	leaq	-80(%rbp), %r11
	movq	%r9, -232(%rbp)
	movq	%r11, %rsi
	movl	%r10d, -220(%rbp)
	movq	%r11, -216(%rbp)
	call	PEM_get_EVP_CIPHER_INFO@PLT
	testl	%eax, %eax
	je	.L12
	movq	-216(%rbp), %r11
	movq	-192(%rbp), %r8
	movq	-184(%rbp), %rcx
	movq	-144(%rbp), %rdx
	movq	-104(%rbp), %rsi
	movq	%r11, %rdi
	call	PEM_do_header@PLT
	testl	%eax, %eax
	je	.L12
	movq	-104(%rbp), %rax
	movq	-88(%rbp), %rdx
	movl	-220(%rbp), %r10d
	movq	-232(%rbp), %r9
	movq	%rax, -96(%rbp)
	movl	-200(%rbp), %eax
	testl	%eax, %eax
	jne	.L126
	movq	-208(%rbp), %rax
	leaq	-96(%rbp), %rsi
	movq	%r9, %rdi
	call	*%rax
	testq	%rax, %rax
	je	.L127
.L119:
	movq	-120(%rbp), %r8
.L25:
	movq	%r8, %rdi
	movl	$209, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-112(%rbp), %rdi
	movl	$211, %edx
	leaq	.LC0(%rip), %rsi
	movq	$0, -120(%rbp)
	call	CRYPTO_free@PLT
	movq	-104(%rbp), %rdi
	movl	$213, %edx
	leaq	.LC0(%rip), %rsi
	movq	$0, -112(%rbp)
	call	CRYPTO_free@PLT
	movq	$0, -104(%rbp)
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L15:
	movl	$9, %ecx
	movq	%r8, %rsi
	leaq	.LC4(%rip), %rdi
	repz cmpsb
	seta	%r10b
	sbbb	$0, %r10b
	movsbl	%r10b, %r10d
	testl	%r10d, %r10d
	jne	.L18
	cmpq	$0, 8(%r12)
	jne	.L120
	movq	d2i_X509_CRL@GOTPCREL(%rip), %rax
	movq	-112(%rbp), %rdi
	leaq	8(%r12), %r9
	movl	$0, -200(%rbp)
	movq	%rax, -208(%rbp)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$16, %ecx
	movq	%r8, %rsi
	leaq	.LC5(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L20
	cmpq	$0, 16(%r12)
	jne	.L120
	movq	$0, 56(%r12)
	movl	$0, 48(%r12)
	call	X509_PKEY_new@PLT
	testq	%rax, %rax
	movq	%rax, 16(%r12)
	movq	%rax, -208(%rbp)
	je	.L12
	movq	-112(%rbp), %rdi
	movq	%rdi, -200(%rbp)
	call	strlen@PLT
	movq	-200(%rbp), %rdi
	cmpl	$10, %eax
	jg	.L22
	movq	-208(%rbp), %rdx
	movq	d2i_RSAPrivateKey@GOTPCREL(%rip), %rax
	movl	$6, -200(%rbp)
	movl	$6, %r10d
	movq	%rax, -208(%rbp)
	leaq	24(%rdx), %r9
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$16, %ecx
	leaq	.LC6(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L128
	movl	$15, %ecx
	leaq	.LC7(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L25
	cmpq	$0, 16(%r12)
	jne	.L120
	movq	$0, 56(%r12)
	movl	$0, 48(%r12)
	call	X509_PKEY_new@PLT
	testq	%rax, %rax
	movq	%rax, 16(%r12)
	movq	%rax, -208(%rbp)
	je	.L12
	movq	-112(%rbp), %rdi
	movq	%rdi, -200(%rbp)
	call	strlen@PLT
	movq	-200(%rbp), %rdi
	cmpl	$10, %eax
	jg	.L22
	movq	-208(%rbp), %rdx
	movq	d2i_ECPrivateKey@GOTPCREL(%rip), %rax
	movl	$408, -200(%rbp)
	movl	$408, %r10d
	movq	%rax, -208(%rbp)
	leaq	24(%rdx), %r9
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L128:
	cmpq	$0, 16(%r12)
	jne	.L120
	movq	$0, 56(%r12)
	movl	$0, 48(%r12)
	call	X509_PKEY_new@PLT
	testq	%rax, %rax
	movq	%rax, 16(%r12)
	movq	%rax, -208(%rbp)
	je	.L12
	movq	-112(%rbp), %rdi
	movq	%rdi, -200(%rbp)
	call	strlen@PLT
	movq	-200(%rbp), %rdi
	cmpl	$10, %eax
	jg	.L22
	movq	-208(%rbp), %rdx
	movq	d2i_DSAPrivateKey@GOTPCREL(%rip), %rax
	movl	$116, -200(%rbp)
	movl	$116, %r10d
	movq	%rax, -208(%rbp)
	leaq	24(%rdx), %r9
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L126:
	leaq	-96(%rbp), %r8
	movq	%rdx, %rcx
	movq	%r9, %rsi
	movl	%r10d, %edi
	movq	%r8, %rdx
	call	d2i_PrivateKey@PLT
	testq	%rax, %rax
	jne	.L119
	movl	$192, %r8d
.L118:
	movl	$13, %edx
	movl	$116, %esi
	movl	$9, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L12:
	movq	%r12, %rdi
	call	X509_INFO_free@PLT
	.p2align 4,,10
	.p2align 3
.L3:
	xorl	%ebx, %ebx
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L35:
	movl	%ebx, %esi
	movq	%r13, %rdi
	addl	$1, %ebx
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	call	X509_INFO_free@PLT
.L34:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L35
	cmpq	%r13, -176(%rbp)
	je	.L38
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	OPENSSL_sk_free@PLT
.L32:
	movq	-120(%rbp), %rdi
	movl	$240, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-112(%rbp), %rdi
	movl	$241, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-104(%rbp), %rdi
	movl	$242, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L129
	addq	$200, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	xorl	%r12d, %r12d
	movq	%r12, %rdi
	call	X509_INFO_free@PLT
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L122:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L2
	movl	$54, %r8d
	movl	$65, %edx
	movl	$116, %esi
	movl	$9, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%edi, %edi
	call	X509_INFO_free@PLT
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L38:
	xorl	%r13d, %r13d
	jmp	.L32
.L4:
	xorl	%edi, %edi
	call	X509_INFO_free@PLT
	jmp	.L3
.L22:
	leaq	24(%r12), %rsi
	call	PEM_get_EVP_CIPHER_INFO@PLT
	testl	%eax, %eax
	je	.L12
	movq	-104(%rbp), %rax
	movq	$0, -104(%rbp)
	movq	%rax, 56(%r12)
	movq	-88(%rbp), %rax
	movl	%eax, 48(%r12)
	jmp	.L119
.L16:
	movq	d2i_X509_AUX@GOTPCREL(%rip), %rax
	movq	-112(%rbp), %rdi
	movq	%r12, %r9
	movl	$0, -200(%rbp)
	movq	%rax, -208(%rbp)
	jmp	.L14
.L123:
	call	ERR_peek_last_error@PLT
	andl	$4095, %eax
	cmpl	$108, %eax
	jne	.L12
	call	ERR_clear_error@PLT
	cmpq	$0, (%r12)
	je	.L130
.L7:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L12
	xorl	%edi, %edi
	call	X509_INFO_free@PLT
	jmp	.L32
.L130:
	cmpq	$0, 8(%r12)
	jne	.L7
	cmpq	$0, 16(%r12)
	jne	.L7
	cmpq	$0, 56(%r12)
	jne	.L7
	movq	%r12, %rdi
	call	X509_INFO_free@PLT
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L127:
	movl	$196, %r8d
	jmp	.L118
.L129:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE780:
	.size	PEM_X509_INFO_read_bio, .-PEM_X509_INFO_read_bio
	.p2align 4
	.globl	PEM_X509_INFO_read
	.type	PEM_X509_INFO_read, @function
PEM_X509_INFO_read:
.LFB779:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	testq	%rax, %rax
	je	.L135
	movq	%rax, %r12
	movq	%rbx, %rcx
	xorl	%edx, %edx
	movl	$106, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r15, %rcx
	movq	%r14, %rdx
	call	PEM_X509_INFO_read_bio
	movq	%r12, %rdi
	movq	%rax, %r13
	call	BIO_free@PLT
.L131:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	.cfi_restore_state
	movl	$28, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$7, %edx
	xorl	%r13d, %r13d
	movl	$115, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
	jmp	.L131
	.cfi_endproc
.LFE779:
	.size	PEM_X509_INFO_read, .-PEM_X509_INFO_read
	.p2align 4
	.globl	PEM_X509_INFO_write_bio
	.type	PEM_X509_INFO_write_bio, @function
PEM_X509_INFO_write_bio:
.LFB781:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$1080, %rsp
	movq	16(%rbp), %rax
	movl	%r8d, -1096(%rbp)
	movq	%rax, -1104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L137
	movq	%rdx, %rdi
	call	EVP_CIPHER_nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L140
	call	strlen@PLT
	movq	%r13, %rdi
	movq	%rax, -1112(%rbp)
	call	EVP_CIPHER_iv_length@PLT
	movq	-1112(%rbp), %rcx
	addl	%eax, %eax
	cltq
	leaq	36(%rcx,%rax), %rax
	cmpq	$1024, %rax
	ja	.L140
.L137:
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.L157
	movq	56(%rbx), %r9
	testq	%r9, %r9
	je	.L143
	movl	48(%rbx), %r8d
	testl	%r8d, %r8d
	jle	.L143
	testq	%r13, %r13
	je	.L160
	movq	24(%rbx), %rdi
	leaq	32(%rbx), %rax
	movl	%r8d, -1104(%rbp)
	movq	%r9, -1096(%rbp)
	movq	%rax, -1112(%rbp)
	call	EVP_CIPHER_nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movq	-1096(%rbp), %r9
	movl	-1104(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L161
	leaq	-1088(%rbp), %r14
	movl	$10, %esi
	movl	%r8d, -1104(%rbp)
	movq	%r14, %rdi
	movq	%r9, -1096(%rbp)
	movb	$0, -1088(%rbp)
	call	PEM_proc_type@PLT
	movq	%r13, %rdi
	call	EVP_CIPHER_iv_length@PLT
	leaq	32(%rbx), %rcx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	%eax, %edx
	call	PEM_dek_info@PLT
	movq	-1096(%rbp), %r9
	movq	%r14, %rdx
	movslq	-1104(%rbp), %r8
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	movq	%r9, %rcx
	call	PEM_write_bio@PLT
	testl	%eax, %eax
	jle	.L147
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.L142
.L163:
	movl	$1, %r12d
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L140:
	movl	$266, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$113, %edx
.L158:
	movl	$117, %esi
	movl	$9, %edi
	xorl	%r12d, %r12d
	call	ERR_put_error@PLT
	leaq	-1088(%rbp), %r14
.L139:
	movl	$1024, %esi
	movq	%r14, %rdi
	call	OPENSSL_cleanse@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L162
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	movq	24(%rax), %rdi
	call	EVP_PKEY_get0_RSA@PLT
	subq	$8, %rsp
	movq	%r15, %rcx
	movq	%r13, %rdx
	pushq	-1104(%rbp)
	movq	%rax, %rsi
	movq	%r14, %r9
	movq	%r12, %rdi
	movl	-1096(%rbp), %r8d
	call	PEM_write_bio_RSAPrivateKey@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	jle	.L151
.L157:
	movq	(%rbx), %rsi
	leaq	-1088(%rbp), %r14
	testq	%rsi, %rsi
	je	.L163
.L142:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	PEM_write_bio_X509@PLT
	testl	%eax, %eax
	setg	%r12b
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L151:
	leaq	-1088(%rbp), %r14
.L147:
	xorl	%r12d, %r12d
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L161:
	movl	$295, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$113, %edx
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L160:
	movl	$279, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$127, %edx
	jmp	.L158
.L162:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE781:
	.size	PEM_X509_INFO_write_bio, .-PEM_X509_INFO_write_bio
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
