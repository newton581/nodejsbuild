	.file	"x_crl.c"
	.text
	.p2align 4
	.type	X509_REVOKED_cmp, @function
X509_REVOKED_cmp:
.LFB1412:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rsi
	movq	(%rdi), %rdi
	jmp	ASN1_STRING_cmp@PLT
	.cfi_endproc
.LFE1412:
	.size	X509_REVOKED_cmp, .-X509_REVOKED_cmp
	.p2align 4
	.type	crl_inf_cb, @function
crl_inf_cb:
.LFB1394:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	testq	%rax, %rax
	je	.L15
	movq	48(%rax), %r8
	cmpl	$5, %edi
	jne	.L15
	testq	%r8, %r8
	jne	.L18
.L15:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	X509_REVOKED_cmp(%rip), %rsi
	movq	%r8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	OPENSSL_sk_set_cmp_func@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1394:
	.size	crl_inf_cb, .-crl_inf_cb
	.p2align 4
	.type	def_crl_verify, @function
def_crl_verify:
.LFB1417:
	.cfi_startproc
	endbr64
	movq	%rdi, %rcx
	movq	%rsi, %r8
	leaq	104(%rdi), %rdx
	leaq	88(%rdi), %rsi
	leaq	X509_CRL_INFO_it(%rip), %rdi
	jmp	ASN1_item_verify@PLT
	.cfi_endproc
.LFE1417:
	.size	def_crl_verify, .-def_crl_verify
	.p2align 4
	.type	crl_cb, @function
crl_cb:
.LFB1396:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$4, %edi
	je	.L21
	jg	.L22
	cmpl	$1, %edi
	je	.L23
	cmpl	$3, %edi
	jne	.L119
	movq	208(%rbx), %rax
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L74
	movq	%rbx, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L60
.L74:
	movq	136(%rbx), %rdi
	call	AUTHORITY_KEYID_free@PLT
	movq	144(%rbx), %rdi
	call	ISSUING_DIST_POINT_free@PLT
	movq	160(%rbx), %rdi
	call	ASN1_INTEGER_free@PLT
	movq	168(%rbx), %rdi
	call	ASN1_INTEGER_free@PLT
	movq	176(%rbx), %rdi
	movq	GENERAL_NAMES_free@GOTPCREL(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
.L119:
	movl	$1, %eax
.L20:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L120
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	cmpl	$5, %edi
	jne	.L119
	call	EVP_sha1@PLT
	xorl	%ecx, %ecx
	leaq	184(%rbx), %rdx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	X509_CRL_digest@PLT
	testl	%eax, %eax
	jne	.L29
	orl	$128, 132(%rbx)
.L29:
	leaq	-64(%rbp), %r13
	xorl	%ecx, %ecx
	movl	$770, %esi
	movq	%rbx, %rdi
	movq	%r13, %rdx
	call	X509_CRL_get_ext_d2i@PLT
	movq	%rax, 144(%rbx)
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L30
	movl	152(%rbx), %edx
	movl	8(%r12), %edi
	movl	12(%r12), %ecx
	movl	28(%r12), %esi
	movl	%edx, %eax
	orl	$1, %eax
	movl	%eax, 152(%rbx)
	testl	%edi, %edi
	jle	.L121
	movl	%edx, %eax
	orl	$5, %eax
	movl	%eax, 152(%rbx)
	testl	%ecx, %ecx
	jle	.L116
	movl	%edx, %eax
	orl	$13, %eax
	movl	%eax, 152(%rbx)
	testl	%esi, %esi
	jle	.L76
.L36:
	orl	$16, %eax
.L76:
	orl	$2, %eax
	movl	%eax, 152(%rbx)
.L34:
	movl	24(%r12), %edx
	testl	%edx, %edx
	jle	.L37
	orl	$32, %eax
	movl	%eax, 152(%rbx)
.L37:
	movq	16(%r12), %rdx
	testq	%rdx, %rdx
	je	.L38
	movl	(%rdx), %ecx
	orl	$64, %eax
	movl	%eax, 152(%rbx)
	testl	%ecx, %ecx
	jle	.L122
	movq	8(%rdx), %rdx
	movzbl	(%rdx), %eax
	movl	%eax, 156(%rbx)
	cmpl	$1, %ecx
	je	.L40
	movzbl	1(%rdx), %edx
	sall	$8, %edx
	orl	%edx, %eax
.L40:
	andl	$32895, %eax
	movl	%eax, 156(%rbx)
.L38:
	movq	%rbx, %rdi
	call	X509_CRL_get_issuer@PLT
	movq	(%r12), %rdi
	movq	%rax, %rsi
	call	DIST_POINT_set_dpname@PLT
	testl	%eax, %eax
	jne	.L42
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L21:
	movq	208(%rbx), %rax
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L28
	movq	%rbx, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L60
.L28:
	movq	136(%rbx), %rdi
	call	AUTHORITY_KEYID_free@PLT
	movq	144(%rbx), %rdi
	call	ISSUING_DIST_POINT_free@PLT
	movq	160(%rbx), %rdi
	call	ASN1_INTEGER_free@PLT
	movq	168(%rbx), %rdi
	call	ASN1_INTEGER_free@PLT
	movq	176(%rbx), %rdi
	movq	GENERAL_NAMES_free@GOTPCREL(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
.L23:
	movq	default_crl_method(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 216(%rbx)
	movl	$0, 132(%rbx)
	movq	%rax, 208(%rbx)
	movabsq	$141282949201920, %rax
	movq	%rax, 152(%rbx)
	movl	$1, %eax
	movq	$0, 176(%rbx)
	movups	%xmm0, 136(%rbx)
	movups	%xmm0, 160(%rbx)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L30:
	cmpl	$-1, -64(%rbp)
	je	.L42
.L117:
	orl	$128, 132(%rbx)
.L42:
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$90, %esi
	movq	%rbx, %rdi
	call	X509_CRL_get_ext_d2i@PLT
	movq	%rax, 136(%rbx)
	testq	%rax, %rax
	je	.L123
.L44:
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$88, %esi
	movq	%rbx, %rdi
	call	X509_CRL_get_ext_d2i@PLT
	movq	%rax, 160(%rbx)
	testq	%rax, %rax
	je	.L124
.L45:
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$140, %esi
	movq	%rbx, %rdi
	call	X509_CRL_get_ext_d2i@PLT
	movq	%rax, 168(%rbx)
	testq	%rax, %rax
	je	.L125
	cmpq	$0, 160(%rbx)
	je	.L75
.L47:
	movq	56(%rbx), %r14
	xorl	%r13d, %r13d
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jge	.L51
.L54:
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	X509_EXTENSION_get_object@PLT
	movq	%rax, %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %r15d
	cmpl	$857, %eax
	je	.L126
	movq	%r12, %rdi
	call	X509_EXTENSION_get_critical@PLT
	testl	%eax, %eax
	je	.L50
	cmpl	$770, %r15d
	sete	%dl
	cmpl	$90, %r15d
	sete	%al
	orb	%al, %dl
	jne	.L50
	cmpl	$140, %r15d
	jne	.L52
	.p2align 4,,10
	.p2align 3
.L50:
	movq	%r14, %rdi
	addl	$1, %r13d
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jl	.L54
	.p2align 4,,10
	.p2align 3
.L51:
	movq	%rbx, %rdi
	xorl	%r13d, %r13d
	leaq	-60(%rbp), %r15
	call	X509_CRL_get_REVOKED@PLT
	movq	$0, -80(%rbp)
	movq	%rax, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L55:
	movq	-72(%rbp), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jge	.L58
	movq	-72(%rbp), %rdi
	movl	%r13d, %esi
	call	OPENSSL_sk_value@PLT
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movl	$771, %esi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	X509_REVOKED_get_ext_d2i@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L127
	movq	176(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L128
.L59:
	movq	%r14, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L60
	movq	%r14, -80(%rbp)
.L57:
	movq	-80(%rbp), %rax
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movl	$141, %esi
	movq	%rax, 40(%r12)
	call	X509_REVOKED_get_ext_d2i@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L129
	movq	%rax, -88(%rbp)
	call	ASN1_ENUMERATED_get@PLT
	movq	-88(%rbp), %rdi
	movl	%eax, 48(%r12)
	call	ASN1_ENUMERATED_free@PLT
.L63:
	movq	32(%r12), %r12
	movl	$0, -60(%rbp)
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	movl	-60(%rbp), %esi
	cmpl	%eax, %esi
	jge	.L66
.L67:
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	movq	%rax, %r14
	call	X509_EXTENSION_get_critical@PLT
	testl	%eax, %eax
	jne	.L130
.L65:
	movq	%r12, %rdi
	addl	$1, -60(%rbp)
	call	OPENSSL_sk_num@PLT
	movl	-60(%rbp), %esi
	cmpl	%eax, %esi
	jl	.L67
.L66:
	addl	$1, %r13d
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L121:
	testl	%ecx, %ecx
	jle	.L131
	movl	%edx, %eax
	orl	$9, %eax
	movl	%eax, 152(%rbx)
.L116:
	testl	%esi, %esi
	jg	.L36
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L127:
	cmpl	$-1, -60(%rbp)
	je	.L57
.L118:
	orl	$128, 132(%rbx)
.L58:
	movq	208(%rbx), %rax
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L72
	movq	%rbx, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L60
.L72:
	orl	$256, 132(%rbx)
	movl	$1, %eax
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L130:
	movq	%r14, %rdi
	call	X509_EXTENSION_get_object@PLT
	movq	%rax, %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$771, %eax
	je	.L65
	orl	$512, 132(%rbx)
	addl	$1, %r13d
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L129:
	cmpl	$-1, -60(%rbp)
	jne	.L118
	movl	$-1, 48(%r12)
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L128:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 176(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L59
	.p2align 4,,10
	.p2align 3
.L60:
	xorl	%eax, %eax
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L126:
	orl	$4096, 132(%rbx)
	movq	%r12, %rdi
	call	X509_EXTENSION_get_critical@PLT
	testl	%eax, %eax
	je	.L50
.L52:
	orl	$512, 132(%rbx)
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L125:
	cmpl	$-1, -64(%rbp)
	je	.L47
.L75:
	orl	$128, 132(%rbx)
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L124:
	cmpl	$-1, -64(%rbp)
	je	.L45
	orl	$128, 132(%rbx)
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L123:
	cmpl	$-1, -64(%rbp)
	je	.L44
	orl	$128, 132(%rbx)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L122:
	movl	156(%rbx), %eax
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L131:
	testl	%esi, %esi
	jle	.L34
	movl	%edx, %eax
	orl	$17, %eax
	movl	%eax, 152(%rbx)
	jmp	.L34
.L120:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1396:
	.size	crl_cb, .-crl_cb
	.p2align 4
	.type	def_crl_lookup, @function
def_crl_lookup:
.LFB1419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -144(%rbp)
	movq	48(%rdi), %rdi
	movq	%rcx, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L132
	movq	%rdx, %r15
	call	OPENSSL_sk_is_sorted@PLT
	testl	%eax, %eax
	je	.L167
.L134:
	movdqu	(%r15), %xmm0
	movq	16(%r15), %rax
	leaq	-112(%rbp), %rsi
	movq	48(%r14), %rdi
	movq	%rax, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	OPENSSL_sk_find@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	js	.L137
	movq	48(%r14), %rdi
	call	OPENSSL_sk_num@PLT
	movl	%eax, -132(%rbp)
	cmpl	%eax, %r13d
	jge	.L137
	.p2align 4,,10
	.p2align 3
.L147:
	movq	48(%r14), %rdi
	movl	%r13d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	ASN1_INTEGER_cmp@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	jne	.L137
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L168
	movq	-128(%rbp), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	jne	.L142
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L144:
	movq	40(%r12), %rdi
	addl	$1, %ebx
.L142:
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L140
	movq	40(%r12), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	cmpl	$4, (%rax)
	jne	.L144
	movq	8(%rax), %rsi
	movq	-120(%rbp), %rdi
	call	X509_NAME_cmp@PLT
	testl	%eax, %eax
	jne	.L144
.L139:
	movq	-144(%rbp), %rax
	testq	%rax, %rax
	je	.L146
	movq	%r12, (%rax)
	.p2align 4,,10
	.p2align 3
.L146:
	xorl	%eax, %eax
	cmpl	$8, 48(%r12)
	sete	%al
	addl	$1, %eax
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L168:
	movq	-128(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L139
	movq	%r14, %rdi
	call	X509_CRL_get_issuer@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	X509_NAME_cmp@PLT
	testl	%eax, %eax
	je	.L139
	.p2align 4,,10
	.p2align 3
.L140:
	addl	$1, %r13d
	cmpl	%r13d, -132(%rbp)
	jne	.L147
.L137:
	xorl	%eax, %eax
.L132:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L170
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L167:
	.cfi_restore_state
	movq	224(%r14), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	48(%r14), %rdi
	call	OPENSSL_sk_sort@PLT
	movq	224(%r14), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L169:
	movq	%r14, %rdi
	call	X509_CRL_get_issuer@PLT
	movq	40(%r12), %rdi
	movq	%rax, -120(%rbp)
	jmp	.L142
.L170:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1419:
	.size	def_crl_lookup, .-def_crl_lookup
	.p2align 4
	.globl	d2i_X509_REVOKED
	.type	d2i_X509_REVOKED, @function
d2i_X509_REVOKED:
.LFB1398:
	.cfi_startproc
	endbr64
	leaq	X509_REVOKED_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1398:
	.size	d2i_X509_REVOKED, .-d2i_X509_REVOKED
	.p2align 4
	.globl	i2d_X509_REVOKED
	.type	i2d_X509_REVOKED, @function
i2d_X509_REVOKED:
.LFB1399:
	.cfi_startproc
	endbr64
	leaq	X509_REVOKED_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1399:
	.size	i2d_X509_REVOKED, .-i2d_X509_REVOKED
	.p2align 4
	.globl	X509_REVOKED_new
	.type	X509_REVOKED_new, @function
X509_REVOKED_new:
.LFB1400:
	.cfi_startproc
	endbr64
	leaq	X509_REVOKED_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1400:
	.size	X509_REVOKED_new, .-X509_REVOKED_new
	.p2align 4
	.globl	X509_REVOKED_free
	.type	X509_REVOKED_free, @function
X509_REVOKED_free:
.LFB1401:
	.cfi_startproc
	endbr64
	leaq	X509_REVOKED_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1401:
	.size	X509_REVOKED_free, .-X509_REVOKED_free
	.p2align 4
	.globl	X509_REVOKED_dup
	.type	X509_REVOKED_dup, @function
X509_REVOKED_dup:
.LFB1402:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	leaq	X509_REVOKED_it(%rip), %rdi
	jmp	ASN1_item_dup@PLT
	.cfi_endproc
.LFE1402:
	.size	X509_REVOKED_dup, .-X509_REVOKED_dup
	.p2align 4
	.globl	d2i_X509_CRL_INFO
	.type	d2i_X509_CRL_INFO, @function
d2i_X509_CRL_INFO:
.LFB1403:
	.cfi_startproc
	endbr64
	leaq	X509_CRL_INFO_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1403:
	.size	d2i_X509_CRL_INFO, .-d2i_X509_CRL_INFO
	.p2align 4
	.globl	i2d_X509_CRL_INFO
	.type	i2d_X509_CRL_INFO, @function
i2d_X509_CRL_INFO:
.LFB1404:
	.cfi_startproc
	endbr64
	leaq	X509_CRL_INFO_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1404:
	.size	i2d_X509_CRL_INFO, .-i2d_X509_CRL_INFO
	.p2align 4
	.globl	X509_CRL_INFO_new
	.type	X509_CRL_INFO_new, @function
X509_CRL_INFO_new:
.LFB1405:
	.cfi_startproc
	endbr64
	leaq	X509_CRL_INFO_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1405:
	.size	X509_CRL_INFO_new, .-X509_CRL_INFO_new
	.p2align 4
	.globl	X509_CRL_INFO_free
	.type	X509_CRL_INFO_free, @function
X509_CRL_INFO_free:
.LFB1406:
	.cfi_startproc
	endbr64
	leaq	X509_CRL_INFO_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1406:
	.size	X509_CRL_INFO_free, .-X509_CRL_INFO_free
	.p2align 4
	.globl	d2i_X509_CRL
	.type	d2i_X509_CRL, @function
d2i_X509_CRL:
.LFB1407:
	.cfi_startproc
	endbr64
	leaq	X509_CRL_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1407:
	.size	d2i_X509_CRL, .-d2i_X509_CRL
	.p2align 4
	.globl	i2d_X509_CRL
	.type	i2d_X509_CRL, @function
i2d_X509_CRL:
.LFB1408:
	.cfi_startproc
	endbr64
	leaq	X509_CRL_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1408:
	.size	i2d_X509_CRL, .-i2d_X509_CRL
	.p2align 4
	.globl	X509_CRL_new
	.type	X509_CRL_new, @function
X509_CRL_new:
.LFB1409:
	.cfi_startproc
	endbr64
	leaq	X509_CRL_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1409:
	.size	X509_CRL_new, .-X509_CRL_new
	.p2align 4
	.globl	X509_CRL_free
	.type	X509_CRL_free, @function
X509_CRL_free:
.LFB1410:
	.cfi_startproc
	endbr64
	leaq	X509_CRL_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1410:
	.size	X509_CRL_free, .-X509_CRL_free
	.p2align 4
	.globl	X509_CRL_dup
	.type	X509_CRL_dup, @function
X509_CRL_dup:
.LFB1411:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	leaq	X509_CRL_it(%rip), %rdi
	jmp	ASN1_item_dup@PLT
	.cfi_endproc
.LFE1411:
	.size	X509_CRL_dup, .-X509_CRL_dup
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/x509/x_crl.c"
	.text
	.p2align 4
	.globl	X509_CRL_add0_revoked
	.type	X509_CRL_add0_revoked, @function
X509_CRL_add0_revoked:
.LFB1413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	48(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L193
.L186:
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L188
	movl	$1, 80(%rbx)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L193:
	.cfi_restore_state
	leaq	X509_REVOKED_cmp(%rip), %rdi
	movq	%rsi, -24(%rbp)
	call	OPENSSL_sk_new@PLT
	movq	-24(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, 48(%rbx)
	movq	%rax, %rdi
	jne	.L186
.L188:
	movl	$342, %r8d
	movl	$65, %edx
	movl	$169, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1413:
	.size	X509_CRL_add0_revoked, .-X509_CRL_add0_revoked
	.p2align 4
	.globl	X509_CRL_verify
	.type	X509_CRL_verify, @function
X509_CRL_verify:
.LFB1414:
	.cfi_startproc
	endbr64
	movq	208(%rdi), %rax
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.L195
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L195:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1414:
	.size	X509_CRL_verify, .-X509_CRL_verify
	.p2align 4
	.globl	X509_CRL_get0_by_serial
	.type	X509_CRL_get0_by_serial, @function
X509_CRL_get0_by_serial:
.LFB1415:
	.cfi_startproc
	endbr64
	movq	208(%rdi), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L197
	xorl	%ecx, %ecx
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L197:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1415:
	.size	X509_CRL_get0_by_serial, .-X509_CRL_get0_by_serial
	.p2align 4
	.globl	X509_CRL_get0_by_cert
	.type	X509_CRL_get0_by_cert, @function
X509_CRL_get0_by_cert:
.LFB1416:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	208(%rdi), %rax
	movq	24(%rax), %rbx
	testq	%rbx, %rbx
	jne	.L201
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L201:
	.cfi_restore_state
	movq	%rdx, %r13
	movq	%rdi, %r12
	movq	%rdx, %rdi
	movq	%rsi, %r14
	call	X509_get_issuer_name@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	X509_get_serialNumber@PLT
	addq	$8, %rsp
	movq	%r15, %rcx
	movq	%r14, %rsi
	movq	%rax, %rdx
	movq	%r12, %rdi
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE1416:
	.size	X509_CRL_get0_by_cert, .-X509_CRL_get0_by_cert
	.p2align 4
	.globl	X509_CRL_set_default_method
	.type	X509_CRL_set_default_method, @function
X509_CRL_set_default_method:
.LFB1420:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	leaq	int_crl_meth(%rip), %rax
	cmove	%rax, %rdi
	movq	%rdi, default_crl_method(%rip)
	ret
	.cfi_endproc
.LFE1420:
	.size	X509_CRL_set_default_method, .-X509_CRL_set_default_method
	.p2align 4
	.globl	X509_CRL_METHOD_new
	.type	X509_CRL_METHOD_new, @function
X509_CRL_METHOD_new:
.LFB1421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movl	$40, %edi
	movq	%rsi, -16(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rdx, -24(%rbp)
	movl	$462, %edx
	movq	%rcx, -32(%rbp)
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L208
	movq	-8(%rbp), %xmm0
	movl	$1, (%rax)
	movhps	-16(%rbp), %xmm0
	movups	%xmm0, 8(%rax)
	movq	-24(%rbp), %xmm0
	movhps	-32(%rbp), %xmm0
	movups	%xmm0, 24(%rax)
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	.cfi_restore_state
	movl	$465, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$154, %esi
	movl	$11, %edi
	movq	%rax, -8(%rbp)
	call	ERR_put_error@PLT
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1421:
	.size	X509_CRL_METHOD_new, .-X509_CRL_METHOD_new
	.p2align 4
	.globl	X509_CRL_METHOD_free
	.type	X509_CRL_METHOD_free, @function
X509_CRL_METHOD_free:
.LFB1422:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L209
	testb	$1, (%rdi)
	jne	.L217
.L209:
	ret
	.p2align 4,,10
	.p2align 3
.L217:
	movl	$480, %edx
	leaq	.LC0(%rip), %rsi
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE1422:
	.size	X509_CRL_METHOD_free, .-X509_CRL_METHOD_free
	.p2align 4
	.globl	X509_CRL_set_meth_data
	.type	X509_CRL_set_meth_data, @function
X509_CRL_set_meth_data:
.LFB1423:
	.cfi_startproc
	endbr64
	movq	%rsi, 216(%rdi)
	ret
	.cfi_endproc
.LFE1423:
	.size	X509_CRL_set_meth_data, .-X509_CRL_set_meth_data
	.p2align 4
	.globl	X509_CRL_get_meth_data
	.type	X509_CRL_get_meth_data, @function
X509_CRL_get_meth_data:
.LFB1424:
	.cfi_startproc
	endbr64
	movq	216(%rdi), %rax
	ret
	.cfi_endproc
.LFE1424:
	.size	X509_CRL_get_meth_data, .-X509_CRL_get_meth_data
	.globl	X509_CRL_it
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"X509_CRL"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	X509_CRL_it, @object
	.size	X509_CRL_it, 56
X509_CRL_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	X509_CRL_seq_tt
	.quad	3
	.quad	X509_CRL_aux
	.quad	232
	.quad	.LC1
	.section	.rodata.str1.1
.LC2:
	.string	"crl"
.LC3:
	.string	"sig_alg"
.LC4:
	.string	"signature"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	X509_CRL_seq_tt, @object
	.size	X509_CRL_seq_tt, 120
X509_CRL_seq_tt:
	.quad	4096
	.quad	0
	.quad	0
	.quad	.LC2
	.quad	X509_CRL_INFO_it
	.quad	4096
	.quad	0
	.quad	88
	.quad	.LC3
	.quad	X509_ALGOR_it
	.quad	4096
	.quad	0
	.quad	104
	.quad	.LC4
	.quad	ASN1_BIT_STRING_it
	.section	.data.rel.ro.local
	.align 32
	.type	X509_CRL_aux, @object
	.size	X509_CRL_aux, 40
X509_CRL_aux:
	.quad	0
	.long	1
	.long	128
	.long	224
	.zero	4
	.quad	crl_cb
	.long	0
	.zero	4
	.globl	X509_CRL_INFO_it
	.section	.rodata.str1.1
.LC5:
	.string	"X509_CRL_INFO"
	.section	.data.rel.ro.local
	.align 32
	.type	X509_CRL_INFO_it, @object
	.size	X509_CRL_INFO_it, 56
X509_CRL_INFO_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	X509_CRL_INFO_seq_tt
	.quad	7
	.quad	X509_CRL_INFO_aux
	.quad	88
	.quad	.LC5
	.section	.rodata.str1.1
.LC6:
	.string	"version"
.LC7:
	.string	"issuer"
.LC8:
	.string	"lastUpdate"
.LC9:
	.string	"nextUpdate"
.LC10:
	.string	"revoked"
.LC11:
	.string	"extensions"
	.section	.data.rel.ro
	.align 32
	.type	X509_CRL_INFO_seq_tt, @object
	.size	X509_CRL_INFO_seq_tt, 280
X509_CRL_INFO_seq_tt:
	.quad	1
	.quad	0
	.quad	0
	.quad	.LC6
	.quad	ASN1_INTEGER_it
	.quad	4096
	.quad	0
	.quad	8
	.quad	.LC3
	.quad	X509_ALGOR_it
	.quad	0
	.quad	0
	.quad	24
	.quad	.LC7
	.quad	X509_NAME_it
	.quad	0
	.quad	0
	.quad	32
	.quad	.LC8
	.quad	ASN1_TIME_it
	.quad	1
	.quad	0
	.quad	40
	.quad	.LC9
	.quad	ASN1_TIME_it
	.quad	5
	.quad	0
	.quad	48
	.quad	.LC10
	.quad	X509_REVOKED_it
	.quad	149
	.quad	0
	.quad	56
	.quad	.LC11
	.quad	X509_EXTENSION_it
	.section	.data.rel.ro.local
	.align 32
	.type	X509_CRL_INFO_aux, @object
	.size	X509_CRL_INFO_aux, 40
X509_CRL_INFO_aux:
	.quad	0
	.long	2
	.long	0
	.long	0
	.zero	4
	.quad	crl_inf_cb
	.long	64
	.zero	4
	.section	.data.rel.local,"aw"
	.align 8
	.type	default_crl_method, @object
	.size	default_crl_method, 8
default_crl_method:
	.quad	int_crl_meth
	.align 32
	.type	int_crl_meth, @object
	.size	int_crl_meth, 40
int_crl_meth:
	.long	0
	.zero	4
	.quad	0
	.quad	0
	.quad	def_crl_lookup
	.quad	def_crl_verify
	.globl	X509_REVOKED_it
	.section	.rodata.str1.1
.LC12:
	.string	"X509_REVOKED"
	.section	.data.rel.ro.local
	.align 32
	.type	X509_REVOKED_it, @object
	.size	X509_REVOKED_it, 56
X509_REVOKED_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	X509_REVOKED_seq_tt
	.quad	3
	.quad	0
	.quad	56
	.quad	.LC12
	.section	.rodata.str1.1
.LC13:
	.string	"serialNumber"
.LC14:
	.string	"revocationDate"
	.section	.data.rel.ro
	.align 32
	.type	X509_REVOKED_seq_tt, @object
	.size	X509_REVOKED_seq_tt, 120
X509_REVOKED_seq_tt:
	.quad	4096
	.quad	0
	.quad	0
	.quad	.LC13
	.quad	ASN1_INTEGER_it
	.quad	0
	.quad	0
	.quad	24
	.quad	.LC14
	.quad	ASN1_TIME_it
	.quad	5
	.quad	0
	.quad	32
	.quad	.LC11
	.quad	X509_EXTENSION_it
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
