	.file	"ocsp_srv.c"
	.text
	.p2align 4
	.globl	OCSP_request_onereq_count
	.type	OCSP_request_onereq_count, @function
OCSP_request_onereq_count:
.LFB1392:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	OPENSSL_sk_num@PLT
	.cfi_endproc
.LFE1392:
	.size	OCSP_request_onereq_count, .-OCSP_request_onereq_count
	.p2align 4
	.globl	OCSP_request_onereq_get0
	.type	OCSP_request_onereq_get0, @function
OCSP_request_onereq_get0:
.LFB1393:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	OPENSSL_sk_value@PLT
	.cfi_endproc
.LFE1393:
	.size	OCSP_request_onereq_get0, .-OCSP_request_onereq_get0
	.p2align 4
	.globl	OCSP_onereq_get0_id
	.type	OCSP_onereq_get0_id, @function
OCSP_onereq_get0_id:
.LFB1394:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE1394:
	.size	OCSP_onereq_get0_id, .-OCSP_onereq_get0_id
	.p2align 4
	.globl	OCSP_id_get0_info
	.type	OCSP_id_get0_info, @function
OCSP_id_get0_info:
.LFB1395:
	.cfi_startproc
	endbr64
	testq	%r8, %r8
	je	.L10
	testq	%rsi, %rsi
	je	.L7
	movq	(%r8), %rax
	movq	%rax, (%rsi)
.L7:
	testq	%rdi, %rdi
	je	.L8
	leaq	16(%r8), %rax
	movq	%rax, (%rdi)
.L8:
	testq	%rdx, %rdx
	je	.L9
	leaq	40(%r8), %rax
	movq	%rax, (%rdx)
.L9:
	movl	$1, %eax
	testq	%rcx, %rcx
	je	.L5
	addq	$64, %r8
	movq	%r8, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%eax, %eax
.L5:
	ret
	.cfi_endproc
.LFE1395:
	.size	OCSP_id_get0_info, .-OCSP_id_get0_info
	.p2align 4
	.globl	OCSP_request_is_signed
	.type	OCSP_request_is_signed, @function
OCSP_request_is_signed:
.LFB1396:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	$0, 32(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE1396:
	.size	OCSP_request_is_signed, .-OCSP_request_is_signed
	.p2align 4
	.globl	OCSP_response_create
	.type	OCSP_response_create, @function
OCSP_response_create:
.LFB1397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	%edi, %ebx
	subq	$8, %rsp
	call	OCSP_RESPONSE_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L26
	movq	(%rax), %rdi
	movslq	%ebx, %rsi
	call	ASN1_ENUMERATED_set@PLT
	testl	%eax, %eax
	je	.L26
	testq	%r13, %r13
	je	.L23
	call	OCSP_RESPBYTES_new@PLT
	movq	%rax, 8(%r12)
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L26
	movl	$365, %edi
	call	OBJ_nid2obj@PLT
	leaq	OCSP_BASICRESP_it(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, (%rbx)
	movq	8(%r12), %rax
	leaq	8(%rax), %rdx
	call	ASN1_item_pack@PLT
	testq	%rax, %rax
	je	.L26
.L23:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	OCSP_RESPONSE_free@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1397:
	.size	OCSP_response_create, .-OCSP_response_create
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ocsp/ocsp_srv.c"
	.text
	.p2align 4
	.globl	OCSP_basic_add1_status
	.type	OCSP_basic_add1_status, @function
OCSP_basic_add1_status:
.LFB1398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	cmpq	$0, 32(%rdi)
	movl	%ecx, -60(%rbp)
	movq	%r8, -56(%rbp)
	je	.L41
.L44:
	call	OCSP_SINGLERESP_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L42
	leaq	16(%rax), %rsi
	movq	%r14, %rdi
	call	ASN1_TIME_to_generalizedtime@PLT
	testq	%rax, %rax
	je	.L45
	cmpq	$0, 16(%rbp)
	je	.L48
	movq	16(%rbp), %rdi
	leaq	24(%r15), %rsi
	call	ASN1_TIME_to_generalizedtime@PLT
	testq	%rax, %rax
	je	.L45
.L48:
	movq	(%r15), %rdi
	call	OCSP_CERTID_free@PLT
	movq	%r13, %rdi
	call	OCSP_CERTID_dup@PLT
	movq	%rax, (%r15)
	testq	%rax, %rax
	je	.L45
	movq	8(%r15), %r13
	movl	%ebx, 0(%r13)
	cmpl	$1, %ebx
	je	.L49
	cmpl	$2, %ebx
	je	.L50
	testl	%ebx, %ebx
	je	.L50
.L45:
	movq	%r15, %rdi
	xorl	%r15d, %r15d
	call	OCSP_SINGLERESP_free@PLT
.L40:
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	call	ASN1_NULL_new@PLT
	movq	%rax, 8(%r13)
	testq	%rax, %rax
	je	.L45
.L55:
	movq	32(%r12), %rdi
	movq	%r15, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L40
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L41:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 32(%r12)
	testq	%rax, %rax
	jne	.L44
	.p2align 4,,10
	.p2align 3
.L42:
	xorl	%r15d, %r15d
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L49:
	cmpq	$0, -56(%rbp)
	je	.L90
	call	OCSP_REVOKEDINFO_new@PLT
	movq	%rax, 8(%r13)
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L45
	movq	-56(%rbp), %rdi
	movq	%rax, %rsi
	call	ASN1_TIME_to_generalizedtime@PLT
	testq	%rax, %rax
	je	.L45
	cmpl	$-1, -60(%rbp)
	je	.L55
	call	ASN1_ENUMERATED_new@PLT
	movq	%rax, 8(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L45
	movslq	-60(%rbp), %rsi
	call	ASN1_ENUMERATED_set@PLT
	testl	%eax, %eax
	jne	.L55
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L90:
	movl	$120, %r8d
	movl	$109, %edx
	movl	$103, %esi
	movl	$39, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L45
	.cfi_endproc
.LFE1398:
	.size	OCSP_basic_add1_status, .-OCSP_basic_add1_status
	.p2align 4
	.globl	OCSP_basic_add1_cert
	.type	OCSP_basic_add1_cert, @function
OCSP_basic_add1_cert:
.LFB1399:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	72(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L99
.L92:
	movq	%r12, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L100
.L94:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	movq	%r12, %rdi
	call	X509_up_ref@PLT
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore_state
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 72(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L92
	jmp	.L94
	.cfi_endproc
.LFE1399:
	.size	OCSP_basic_add1_cert, .-OCSP_basic_add1_cert
	.p2align 4
	.globl	OCSP_basic_sign_ctx
	.type	OCSP_basic_sign_ctx, @function
OCSP_basic_sign_ctx:
.LFB1400:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L104
	movq	%rdi, %r12
	movq	%rdx, %rdi
	movq	%rsi, %r14
	movq	%rdx, %rbx
	movq	%rcx, %r13
	call	EVP_MD_CTX_pkey_ctx@PLT
	testq	%rax, %rax
	je	.L104
	movq	%rbx, %rdi
	call	EVP_MD_CTX_pkey_ctx@PLT
	movq	%rax, %rdi
	call	EVP_PKEY_CTX_get0_pkey@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L106
	movq	%r14, %rdi
	call	X509_check_private_key@PLT
	testl	%eax, %eax
	je	.L106
	testb	$1, -88(%rbp)
	je	.L107
.L115:
	testq	$1024, -88(%rbp)
	jne	.L108
	movq	%r14, %rdi
	call	X509_get_subject_name@PLT
	leaq	16(%r12), %rdi
	movq	%rax, %rsi
	call	X509_NAME_set@PLT
	testl	%eax, %eax
	je	.L116
	movl	$0, 8(%r12)
.L118:
	testq	$2048, -88(%rbp)
	jne	.L120
	movq	24(%r12), %rdi
	xorl	%esi, %esi
	call	X509_gmtime_adj@PLT
	testq	%rax, %rax
	je	.L116
.L120:
	movq	64(%r12), %rcx
	xorl	%edx, %edx
	leaq	48(%r12), %rsi
	movq	%rbx, %r9
	movq	%r12, %r8
	leaq	OCSP_RESPDATA_it(%rip), %rdi
	call	ASN1_item_sign_ctx@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L106:
	movl	$192, %r8d
	movl	$110, %edx
	movl	$119, %esi
	movl	$39, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L101:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L152
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	movl	$186, %r8d
	movl	$130, %edx
	movl	$119, %esi
	movl	$39, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L108:
	call	EVP_sha1@PLT
	leaq	-80(%rbp), %r15
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	%r15, %rdx
	call	X509_pubkey_digest@PLT
	testl	%eax, %eax
	je	.L116
	call	ASN1_OCTET_STRING_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L116
	movl	$20, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	ASN1_OCTET_STRING_set@PLT
	testl	%eax, %eax
	je	.L153
	movl	$1, 8(%r12)
	movq	%r13, 16(%r12)
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L107:
	movq	72(%r12), %rdi
	testq	%rdi, %rdi
	je	.L154
.L110:
	movq	%r14, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L116
	movq	%r14, %rdi
	xorl	%r15d, %r15d
	call	X509_up_ref@PLT
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L156:
	movq	-96(%rbp), %r8
	addl	$1, %r15d
	movq	%r8, %rdi
	call	X509_up_ref@PLT
.L112:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r15d
	jge	.L115
	movq	%r13, %rdi
	movl	%r15d, %esi
	call	OPENSSL_sk_value@PLT
	movq	72(%r12), %rdi
	movq	%rax, %r8
	testq	%rdi, %rdi
	je	.L155
.L113:
	movq	%r8, %rsi
	movq	%r8, -96(%rbp)
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L156
.L116:
	xorl	%eax, %eax
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L155:
	movq	%rax, -96(%rbp)
	call	OPENSSL_sk_new_null@PLT
	movq	-96(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, 72(%r12)
	movq	%rax, %rdi
	jne	.L113
	xorl	%eax, %eax
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L154:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 72(%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L110
	xorl	%eax, %eax
	jmp	.L101
.L153:
	movq	%r13, %rdi
	movl	%eax, -88(%rbp)
	call	ASN1_OCTET_STRING_free@PLT
	movl	-88(%rbp), %eax
	jmp	.L101
.L152:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1400:
	.size	OCSP_basic_sign_ctx, .-OCSP_basic_sign_ctx
	.p2align 4
	.globl	OCSP_basic_sign
	.type	OCSP_basic_sign, @function
OCSP_basic_sign:
.LFB1401:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	movq	%rdi, -80(%rbp)
	movq	%rcx, -72(%rbp)
	movq	%r9, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_MD_CTX_new@PLT
	movq	$0, -64(%rbp)
	testq	%rax, %rax
	je	.L157
	movq	-72(%rbp), %rdx
	leaq	-64(%rbp), %rsi
	movq	%rbx, %r8
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	movq	%rax, %r13
	call	EVP_DigestSignInit@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L164
	movq	-88(%rbp), %r8
	movq	-80(%rbp), %rdi
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	OCSP_basic_sign_ctx
	movl	%eax, %r12d
.L164:
	movq	%r13, %rdi
	call	EVP_MD_CTX_free@PLT
.L157:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L165
	addq	$56, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L165:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1401:
	.size	OCSP_basic_sign, .-OCSP_basic_sign
	.p2align 4
	.globl	OCSP_RESPID_set_by_name
	.type	OCSP_RESPID_set_by_name, @function
OCSP_RESPID_set_by_name:
.LFB1402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	call	X509_get_subject_name@PLT
	leaq	8(%rbx), %rdi
	movq	%rax, %rsi
	call	X509_NAME_set@PLT
	testl	%eax, %eax
	je	.L166
	movl	$0, (%rbx)
	movl	$1, %eax
.L166:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1402:
	.size	OCSP_RESPID_set_by_name, .-OCSP_RESPID_set_by_name
	.p2align 4
	.globl	OCSP_RESPID_set_by_key
	.type	OCSP_RESPID_set_by_key, @function
OCSP_RESPID_set_by_key:
.LFB1403:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	EVP_sha1@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	X509_pubkey_digest@PLT
	testl	%eax, %eax
	jne	.L173
.L175:
	xorl	%eax, %eax
.L172:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L182
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L173:
	.cfi_restore_state
	call	ASN1_OCTET_STRING_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L175
	movl	$20, %edx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	ASN1_OCTET_STRING_set@PLT
	testl	%eax, %eax
	je	.L183
	movl	$1, (%rbx)
	movl	$1, %eax
	movq	%r12, 8(%rbx)
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L183:
	movq	%r12, %rdi
	movl	%eax, -68(%rbp)
	call	ASN1_OCTET_STRING_free@PLT
	movl	-68(%rbp), %eax
	jmp	.L172
.L182:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1403:
	.size	OCSP_RESPID_set_by_key, .-OCSP_RESPID_set_by_key
	.p2align 4
	.globl	OCSP_RESPID_match
	.type	OCSP_RESPID_match, @function
OCSP_RESPID_match:
.LFB1404:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	(%rdi), %eax
	cmpl	$1, %eax
	je	.L198
	testl	%eax, %eax
	je	.L199
.L193:
	xorl	%eax, %eax
.L184:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L200
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L199:
	.cfi_restore_state
	cmpq	$0, 8(%rdi)
	je	.L184
	movq	%rsi, %rdi
	call	X509_get_subject_name@PLT
	movq	8(%rbx), %rdi
	movq	%rax, %rsi
	call	X509_NAME_cmp@PLT
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L198:
	cmpq	$0, 8(%rdi)
	je	.L193
	call	EVP_sha1@PLT
	leaq	-64(%rbp), %r13
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r13, %rdx
	call	X509_pubkey_digest@PLT
	testl	%eax, %eax
	je	.L193
	movq	8(%rbx), %rdi
	call	ASN1_STRING_length@PLT
	cmpl	$20, %eax
	jne	.L193
	movq	8(%rbx), %rdi
	call	ASN1_STRING_get0_data@PLT
	movq	(%rax), %rdx
	movq	8(%rax), %rcx
	xorq	-64(%rbp), %rdx
	xorq	-56(%rbp), %rcx
	orq	%rdx, %rcx
	jne	.L189
	movl	-48(%rbp), %ecx
	cmpl	%ecx, 16(%rax)
	je	.L201
.L189:
	movl	$1, %eax
.L190:
	xorl	$1, %eax
	jmp	.L184
.L201:
	xorl	%eax, %eax
	jmp	.L190
.L200:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1404:
	.size	OCSP_RESPID_match, .-OCSP_RESPID_match
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
