	.file	"pk7_attr.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/pkcs7/pk7_attr.c"
	.text
	.p2align 4
	.globl	PKCS7_add_attrib_smimecap
	.type	PKCS7_add_attrib_smimecap, @function
PKCS7_add_attrib_smimecap:
.LFB755:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	call	ASN1_STRING_new@PLT
	testq	%rax, %rax
	je	.L5
	movq	%r13, %rdi
	leaq	8(%rax), %rsi
	leaq	X509_ALGORS_it(%rip), %rdx
	movq	%rax, %r12
	call	ASN1_item_i2d@PLT
	movq	%r12, %rcx
	movq	%r14, %rdi
	movl	$16, %edx
	movl	%eax, (%r12)
	addq	$8, %rsp
	movl	$167, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	PKCS7_add_signed_attribute@PLT
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	movl	$26, %r8d
	movl	$65, %edx
	movl	$118, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE755:
	.size	PKCS7_add_attrib_smimecap, .-PKCS7_add_attrib_smimecap
	.p2align 4
	.globl	PKCS7_get_smimecap
	.type	PKCS7_get_smimecap, @function
PKCS7_get_smimecap:
.LFB756:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$167, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	call	PKCS7_get_signed_attribute@PLT
	testq	%rax, %rax
	je	.L8
	cmpl	$16, (%rax)
	jne	.L11
	movq	8(%rax), %rax
	leaq	-16(%rbp), %rsi
	leaq	X509_ALGORS_it(%rip), %rcx
	xorl	%edi, %edi
	movq	8(%rax), %rdx
	movq	%rdx, -16(%rbp)
	movslq	(%rax), %rdx
	call	ASN1_item_d2i@PLT
.L8:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L16
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L8
.L16:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE756:
	.size	PKCS7_get_smimecap, .-PKCS7_get_smimecap
	.p2align 4
	.globl	PKCS7_simple_smimecap
	.type	PKCS7_simple_smimecap, @function
PKCS7_simple_smimecap:
.LFB757:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	%edx, %ebx
	call	X509_ALGOR_new@PLT
	testq	%rax, %rax
	je	.L34
	movq	(%rax), %rdi
	movq	%rax, %r12
	call	ASN1_OBJECT_free@PLT
	movl	%r13d, %edi
	call	OBJ_nid2obj@PLT
	movq	%rax, (%r12)
	testl	%ebx, %ebx
	jle	.L20
	call	ASN1_TYPE_new@PLT
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L23
	call	ASN1_INTEGER_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L23
	movslq	%ebx, %rsi
	movq	%rax, %rdi
	call	ASN1_INTEGER_set@PLT
	testl	%eax, %eax
	je	.L22
	movq	8(%r12), %rax
	movq	%r13, 8(%rax)
	movl	$2, (%rax)
.L20:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L23
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	xorl	%r13d, %r13d
.L22:
	movl	$80, %r8d
	movl	$65, %edx
	movl	$119, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r13, %rdi
	call	ASN1_INTEGER_free@PLT
	movq	%r12, %rdi
	call	X509_ALGOR_free@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	movl	$56, %r8d
	movl	$65, %edx
	movl	$119, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE757:
	.size	PKCS7_simple_smimecap, .-PKCS7_simple_smimecap
	.p2align 4
	.globl	PKCS7_add_attrib_content_type
	.type	PKCS7_add_attrib_content_type, @function
PKCS7_add_attrib_content_type:
.LFB758:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movl	$50, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	PKCS7_get_signed_attribute@PLT
	testq	%rax, %rax
	je	.L39
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L40
.L37:
	movq	%r13, %rcx
	movq	%r12, %rdi
	movl	$6, %edx
	popq	%r12
	movl	$50, %esi
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	PKCS7_add_signed_attribute@PLT
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	movl	$21, %edi
	call	OBJ_nid2obj@PLT
	movq	%rax, %r13
	jmp	.L37
	.cfi_endproc
.LFE758:
	.size	PKCS7_add_attrib_content_type, .-PKCS7_add_attrib_content_type
	.p2align 4
	.globl	PKCS7_add0_attrib_signing_time
	.type	PKCS7_add0_attrib_signing_time, @function
PKCS7_add0_attrib_signing_time:
.LFB759:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L48
.L42:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$23, %edx
	movl	$52, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	PKCS7_add_signed_attribute@PLT
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	X509_gmtime_adj@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	jne	.L42
	movl	$99, %r8d
	movl	$65, %edx
	movl	$135, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE759:
	.size	PKCS7_add0_attrib_signing_time, .-PKCS7_add0_attrib_signing_time
	.p2align 4
	.globl	PKCS7_add1_attrib_digest
	.type	PKCS7_add1_attrib_digest, @function
PKCS7_add1_attrib_digest:
.LFB760:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	call	ASN1_OCTET_STRING_new@PLT
	movq	%rax, %r12
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L49
	movl	%r15d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	ASN1_STRING_set@PLT
	testl	%eax, %eax
	je	.L52
	movq	%r12, %rcx
	movl	$4, %edx
	movl	$51, %esi
	movq	%r14, %rdi
	call	PKCS7_add_signed_attribute@PLT
	testl	%eax, %eax
	je	.L52
	movl	$1, %eax
.L49:
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	movq	%r12, %rdi
	call	ASN1_OCTET_STRING_free@PLT
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE760:
	.size	PKCS7_add1_attrib_digest, .-PKCS7_add1_attrib_digest
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
