	.file	"x_all.c"
	.text
	.p2align 4
	.globl	X509_verify
	.type	X509_verify, @function
X509_verify:
.LFB1394:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	136(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	leaq	32(%rdi), %rsi
	movq	%r13, %rdi
	subq	$8, %rsp
	call	X509_ALGOR_cmp@PLT
	testl	%eax, %eax
	je	.L5
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%rbx, %r8
	movq	%r12, %rcx
	movq	%r13, %rsi
	popq	%rbx
	leaq	152(%r12), %rdx
	leaq	X509_CINF_it(%rip), %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	ASN1_item_verify@PLT
	.cfi_endproc
.LFE1394:
	.size	X509_verify, .-X509_verify
	.p2align 4
	.globl	X509_REQ_verify
	.type	X509_REQ_verify, @function
X509_REQ_verify:
.LFB1395:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rdx
	movq	%rdi, %rcx
	movq	%rsi, %r8
	leaq	56(%rdi), %rsi
	leaq	X509_REQ_INFO_it(%rip), %rdi
	jmp	ASN1_item_verify@PLT
	.cfi_endproc
.LFE1395:
	.size	X509_REQ_verify, .-X509_REQ_verify
	.p2align 4
	.globl	NETSCAPE_SPKI_verify
	.type	NETSCAPE_SPKI_verify, @function
NETSCAPE_SPKI_verify:
.LFB1396:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdx
	movq	(%rdi), %rcx
	movq	%rsi, %r8
	leaq	8(%rdi), %rsi
	leaq	NETSCAPE_SPKAC_it(%rip), %rdi
	jmp	ASN1_item_verify@PLT
	.cfi_endproc
.LFE1396:
	.size	NETSCAPE_SPKI_verify, .-NETSCAPE_SPKI_verify
	.p2align 4
	.globl	X509_sign
	.type	X509_sign, @function
X509_sign:
.LFB1397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r10
	movq	%rdi, %r8
	movq	%rsi, %r9
	movl	$1, 128(%rdi)
	leaq	152(%rdi), %rcx
	leaq	136(%rdi), %rdx
	leaq	32(%rdi), %rsi
	leaq	X509_CINF_it(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	%r10
	call	ASN1_item_sign@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1397:
	.size	X509_sign, .-X509_sign
	.p2align 4
	.globl	X509_sign_ctx
	.type	X509_sign_ctx, @function
X509_sign_ctx:
.LFB1398:
	.cfi_startproc
	endbr64
	movl	$1, 128(%rdi)
	movq	%rdi, %r8
	movq	%rsi, %r9
	leaq	152(%rdi), %rcx
	leaq	136(%rdi), %rdx
	leaq	32(%rdi), %rsi
	leaq	X509_CINF_it(%rip), %rdi
	jmp	ASN1_item_sign_ctx@PLT
	.cfi_endproc
.LFE1398:
	.size	X509_sign_ctx, .-X509_sign_ctx
	.p2align 4
	.globl	X509_http_nbio
	.type	X509_http_nbio, @function
X509_http_nbio:
.LFB1399:
	.cfi_startproc
	endbr64
	leaq	X509_it(%rip), %rdx
	jmp	OCSP_REQ_CTX_nbio_d2i@PLT
	.cfi_endproc
.LFE1399:
	.size	X509_http_nbio, .-X509_http_nbio
	.p2align 4
	.globl	X509_REQ_sign
	.type	X509_REQ_sign, @function
X509_REQ_sign:
.LFB1400:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	72(%rdi), %rcx
	movq	%rdi, %r8
	movq	%rsi, %r9
	leaq	56(%rdi), %rsi
	leaq	X509_REQ_INFO_it(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	%rdx
	xorl	%edx, %edx
	call	ASN1_item_sign@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1400:
	.size	X509_REQ_sign, .-X509_REQ_sign
	.p2align 4
	.globl	X509_REQ_sign_ctx
	.type	X509_REQ_sign_ctx, @function
X509_REQ_sign_ctx:
.LFB1401:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rcx
	movq	%rdi, %r8
	movq	%rsi, %r9
	xorl	%edx, %edx
	leaq	56(%rdi), %rsi
	leaq	X509_REQ_INFO_it(%rip), %rdi
	jmp	ASN1_item_sign_ctx@PLT
	.cfi_endproc
.LFE1401:
	.size	X509_REQ_sign_ctx, .-X509_REQ_sign_ctx
	.p2align 4
	.globl	X509_CRL_sign
	.type	X509_CRL_sign, @function
X509_CRL_sign:
.LFB1402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r10
	movq	%rdi, %r8
	movq	%rsi, %r9
	movl	$1, 80(%rdi)
	leaq	104(%rdi), %rcx
	leaq	88(%rdi), %rdx
	leaq	8(%rdi), %rsi
	leaq	X509_CRL_INFO_it(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	%r10
	call	ASN1_item_sign@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1402:
	.size	X509_CRL_sign, .-X509_CRL_sign
	.p2align 4
	.globl	X509_CRL_sign_ctx
	.type	X509_CRL_sign_ctx, @function
X509_CRL_sign_ctx:
.LFB1403:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	leaq	104(%rdi), %rcx
	leaq	88(%rdi), %rdx
	movq	%rsi, %r9
	movl	$1, 80(%rdi)
	leaq	8(%rdi), %rsi
	leaq	X509_CRL_INFO_it(%rip), %rdi
	jmp	ASN1_item_sign_ctx@PLT
	.cfi_endproc
.LFE1403:
	.size	X509_CRL_sign_ctx, .-X509_CRL_sign_ctx
	.p2align 4
	.globl	X509_CRL_http_nbio
	.type	X509_CRL_http_nbio, @function
X509_CRL_http_nbio:
.LFB1404:
	.cfi_startproc
	endbr64
	leaq	X509_CRL_it(%rip), %rdx
	jmp	OCSP_REQ_CTX_nbio_d2i@PLT
	.cfi_endproc
.LFE1404:
	.size	X509_CRL_http_nbio, .-X509_CRL_http_nbio
	.p2align 4
	.globl	NETSCAPE_SPKI_sign
	.type	NETSCAPE_SPKI_sign, @function
NETSCAPE_SPKI_sign:
.LFB1405:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	24(%rdi), %rcx
	movq	%rsi, %r9
	leaq	8(%rdi), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	%rdx
	movq	(%rdi), %r8
	xorl	%edx, %edx
	leaq	NETSCAPE_SPKAC_it(%rip), %rdi
	call	ASN1_item_sign@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1405:
	.size	NETSCAPE_SPKI_sign, .-NETSCAPE_SPKI_sign
	.p2align 4
	.globl	d2i_X509_fp
	.type	d2i_X509_fp, @function
d2i_X509_fp:
.LFB1406:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	X509_it(%rip), %rdi
	jmp	ASN1_item_d2i_fp@PLT
	.cfi_endproc
.LFE1406:
	.size	d2i_X509_fp, .-d2i_X509_fp
	.p2align 4
	.globl	i2d_X509_fp
	.type	i2d_X509_fp, @function
i2d_X509_fp:
.LFB1407:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	X509_it(%rip), %rdi
	jmp	ASN1_item_i2d_fp@PLT
	.cfi_endproc
.LFE1407:
	.size	i2d_X509_fp, .-i2d_X509_fp
	.p2align 4
	.globl	d2i_X509_bio
	.type	d2i_X509_bio, @function
d2i_X509_bio:
.LFB1408:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	X509_it(%rip), %rdi
	jmp	ASN1_item_d2i_bio@PLT
	.cfi_endproc
.LFE1408:
	.size	d2i_X509_bio, .-d2i_X509_bio
	.p2align 4
	.globl	i2d_X509_bio
	.type	i2d_X509_bio, @function
i2d_X509_bio:
.LFB1409:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	X509_it(%rip), %rdi
	jmp	ASN1_item_i2d_bio@PLT
	.cfi_endproc
.LFE1409:
	.size	i2d_X509_bio, .-i2d_X509_bio
	.p2align 4
	.globl	d2i_X509_CRL_fp
	.type	d2i_X509_CRL_fp, @function
d2i_X509_CRL_fp:
.LFB1410:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	X509_CRL_it(%rip), %rdi
	jmp	ASN1_item_d2i_fp@PLT
	.cfi_endproc
.LFE1410:
	.size	d2i_X509_CRL_fp, .-d2i_X509_CRL_fp
	.p2align 4
	.globl	i2d_X509_CRL_fp
	.type	i2d_X509_CRL_fp, @function
i2d_X509_CRL_fp:
.LFB1411:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	X509_CRL_it(%rip), %rdi
	jmp	ASN1_item_i2d_fp@PLT
	.cfi_endproc
.LFE1411:
	.size	i2d_X509_CRL_fp, .-i2d_X509_CRL_fp
	.p2align 4
	.globl	d2i_X509_CRL_bio
	.type	d2i_X509_CRL_bio, @function
d2i_X509_CRL_bio:
.LFB1412:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	X509_CRL_it(%rip), %rdi
	jmp	ASN1_item_d2i_bio@PLT
	.cfi_endproc
.LFE1412:
	.size	d2i_X509_CRL_bio, .-d2i_X509_CRL_bio
	.p2align 4
	.globl	i2d_X509_CRL_bio
	.type	i2d_X509_CRL_bio, @function
i2d_X509_CRL_bio:
.LFB1413:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	X509_CRL_it(%rip), %rdi
	jmp	ASN1_item_i2d_bio@PLT
	.cfi_endproc
.LFE1413:
	.size	i2d_X509_CRL_bio, .-i2d_X509_CRL_bio
	.p2align 4
	.globl	d2i_PKCS7_fp
	.type	d2i_PKCS7_fp, @function
d2i_PKCS7_fp:
.LFB1414:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	PKCS7_it(%rip), %rdi
	jmp	ASN1_item_d2i_fp@PLT
	.cfi_endproc
.LFE1414:
	.size	d2i_PKCS7_fp, .-d2i_PKCS7_fp
	.p2align 4
	.globl	i2d_PKCS7_fp
	.type	i2d_PKCS7_fp, @function
i2d_PKCS7_fp:
.LFB1415:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	PKCS7_it(%rip), %rdi
	jmp	ASN1_item_i2d_fp@PLT
	.cfi_endproc
.LFE1415:
	.size	i2d_PKCS7_fp, .-i2d_PKCS7_fp
	.p2align 4
	.globl	d2i_PKCS7_bio
	.type	d2i_PKCS7_bio, @function
d2i_PKCS7_bio:
.LFB1416:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	PKCS7_it(%rip), %rdi
	jmp	ASN1_item_d2i_bio@PLT
	.cfi_endproc
.LFE1416:
	.size	d2i_PKCS7_bio, .-d2i_PKCS7_bio
	.p2align 4
	.globl	i2d_PKCS7_bio
	.type	i2d_PKCS7_bio, @function
i2d_PKCS7_bio:
.LFB1417:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	PKCS7_it(%rip), %rdi
	jmp	ASN1_item_i2d_bio@PLT
	.cfi_endproc
.LFE1417:
	.size	i2d_PKCS7_bio, .-i2d_PKCS7_bio
	.p2align 4
	.globl	d2i_X509_REQ_fp
	.type	d2i_X509_REQ_fp, @function
d2i_X509_REQ_fp:
.LFB1418:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	X509_REQ_it(%rip), %rdi
	jmp	ASN1_item_d2i_fp@PLT
	.cfi_endproc
.LFE1418:
	.size	d2i_X509_REQ_fp, .-d2i_X509_REQ_fp
	.p2align 4
	.globl	i2d_X509_REQ_fp
	.type	i2d_X509_REQ_fp, @function
i2d_X509_REQ_fp:
.LFB1419:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	X509_REQ_it(%rip), %rdi
	jmp	ASN1_item_i2d_fp@PLT
	.cfi_endproc
.LFE1419:
	.size	i2d_X509_REQ_fp, .-i2d_X509_REQ_fp
	.p2align 4
	.globl	d2i_X509_REQ_bio
	.type	d2i_X509_REQ_bio, @function
d2i_X509_REQ_bio:
.LFB1420:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	X509_REQ_it(%rip), %rdi
	jmp	ASN1_item_d2i_bio@PLT
	.cfi_endproc
.LFE1420:
	.size	d2i_X509_REQ_bio, .-d2i_X509_REQ_bio
	.p2align 4
	.globl	i2d_X509_REQ_bio
	.type	i2d_X509_REQ_bio, @function
i2d_X509_REQ_bio:
.LFB1421:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	X509_REQ_it(%rip), %rdi
	jmp	ASN1_item_i2d_bio@PLT
	.cfi_endproc
.LFE1421:
	.size	i2d_X509_REQ_bio, .-i2d_X509_REQ_bio
	.p2align 4
	.globl	d2i_RSAPrivateKey_fp
	.type	d2i_RSAPrivateKey_fp, @function
d2i_RSAPrivateKey_fp:
.LFB1422:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	RSAPrivateKey_it(%rip), %rdi
	jmp	ASN1_item_d2i_fp@PLT
	.cfi_endproc
.LFE1422:
	.size	d2i_RSAPrivateKey_fp, .-d2i_RSAPrivateKey_fp
	.p2align 4
	.globl	i2d_RSAPrivateKey_fp
	.type	i2d_RSAPrivateKey_fp, @function
i2d_RSAPrivateKey_fp:
.LFB1423:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	RSAPrivateKey_it(%rip), %rdi
	jmp	ASN1_item_i2d_fp@PLT
	.cfi_endproc
.LFE1423:
	.size	i2d_RSAPrivateKey_fp, .-i2d_RSAPrivateKey_fp
	.p2align 4
	.globl	d2i_RSAPublicKey_fp
	.type	d2i_RSAPublicKey_fp, @function
d2i_RSAPublicKey_fp:
.LFB1424:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	RSAPublicKey_it(%rip), %rdi
	jmp	ASN1_item_d2i_fp@PLT
	.cfi_endproc
.LFE1424:
	.size	d2i_RSAPublicKey_fp, .-d2i_RSAPublicKey_fp
	.p2align 4
	.globl	d2i_RSA_PUBKEY_fp
	.type	d2i_RSA_PUBKEY_fp, @function
d2i_RSA_PUBKEY_fp:
.LFB1425:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	movq	%rsi, %rcx
	movq	RSA_new@GOTPCREL(%rip), %rdi
	movq	d2i_RSA_PUBKEY@GOTPCREL(%rip), %rsi
	jmp	ASN1_d2i_fp@PLT
	.cfi_endproc
.LFE1425:
	.size	d2i_RSA_PUBKEY_fp, .-d2i_RSA_PUBKEY_fp
	.p2align 4
	.globl	i2d_RSAPublicKey_fp
	.type	i2d_RSAPublicKey_fp, @function
i2d_RSAPublicKey_fp:
.LFB1426:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	RSAPublicKey_it(%rip), %rdi
	jmp	ASN1_item_i2d_fp@PLT
	.cfi_endproc
.LFE1426:
	.size	i2d_RSAPublicKey_fp, .-i2d_RSAPublicKey_fp
	.p2align 4
	.globl	i2d_RSA_PUBKEY_fp
	.type	i2d_RSA_PUBKEY_fp, @function
i2d_RSA_PUBKEY_fp:
.LFB1427:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	i2d_RSA_PUBKEY@GOTPCREL(%rip), %rdi
	jmp	ASN1_i2d_fp@PLT
	.cfi_endproc
.LFE1427:
	.size	i2d_RSA_PUBKEY_fp, .-i2d_RSA_PUBKEY_fp
	.p2align 4
	.globl	d2i_RSAPrivateKey_bio
	.type	d2i_RSAPrivateKey_bio, @function
d2i_RSAPrivateKey_bio:
.LFB1428:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	RSAPrivateKey_it(%rip), %rdi
	jmp	ASN1_item_d2i_bio@PLT
	.cfi_endproc
.LFE1428:
	.size	d2i_RSAPrivateKey_bio, .-d2i_RSAPrivateKey_bio
	.p2align 4
	.globl	i2d_RSAPrivateKey_bio
	.type	i2d_RSAPrivateKey_bio, @function
i2d_RSAPrivateKey_bio:
.LFB1429:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	RSAPrivateKey_it(%rip), %rdi
	jmp	ASN1_item_i2d_bio@PLT
	.cfi_endproc
.LFE1429:
	.size	i2d_RSAPrivateKey_bio, .-i2d_RSAPrivateKey_bio
	.p2align 4
	.globl	d2i_RSAPublicKey_bio
	.type	d2i_RSAPublicKey_bio, @function
d2i_RSAPublicKey_bio:
.LFB1430:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	RSAPublicKey_it(%rip), %rdi
	jmp	ASN1_item_d2i_bio@PLT
	.cfi_endproc
.LFE1430:
	.size	d2i_RSAPublicKey_bio, .-d2i_RSAPublicKey_bio
	.p2align 4
	.globl	d2i_RSA_PUBKEY_bio
	.type	d2i_RSA_PUBKEY_bio, @function
d2i_RSA_PUBKEY_bio:
.LFB1431:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	movq	%rsi, %rcx
	movq	RSA_new@GOTPCREL(%rip), %rdi
	movq	d2i_RSA_PUBKEY@GOTPCREL(%rip), %rsi
	jmp	ASN1_d2i_bio@PLT
	.cfi_endproc
.LFE1431:
	.size	d2i_RSA_PUBKEY_bio, .-d2i_RSA_PUBKEY_bio
	.p2align 4
	.globl	i2d_RSAPublicKey_bio
	.type	i2d_RSAPublicKey_bio, @function
i2d_RSAPublicKey_bio:
.LFB1432:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	RSAPublicKey_it(%rip), %rdi
	jmp	ASN1_item_i2d_bio@PLT
	.cfi_endproc
.LFE1432:
	.size	i2d_RSAPublicKey_bio, .-i2d_RSAPublicKey_bio
	.p2align 4
	.globl	i2d_RSA_PUBKEY_bio
	.type	i2d_RSA_PUBKEY_bio, @function
i2d_RSA_PUBKEY_bio:
.LFB1433:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	i2d_RSA_PUBKEY@GOTPCREL(%rip), %rdi
	jmp	ASN1_i2d_bio@PLT
	.cfi_endproc
.LFE1433:
	.size	i2d_RSA_PUBKEY_bio, .-i2d_RSA_PUBKEY_bio
	.p2align 4
	.globl	d2i_DSAPrivateKey_fp
	.type	d2i_DSAPrivateKey_fp, @function
d2i_DSAPrivateKey_fp:
.LFB1434:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	movq	%rsi, %rcx
	movq	DSA_new@GOTPCREL(%rip), %rdi
	movq	d2i_DSAPrivateKey@GOTPCREL(%rip), %rsi
	jmp	ASN1_d2i_fp@PLT
	.cfi_endproc
.LFE1434:
	.size	d2i_DSAPrivateKey_fp, .-d2i_DSAPrivateKey_fp
	.p2align 4
	.globl	i2d_DSAPrivateKey_fp
	.type	i2d_DSAPrivateKey_fp, @function
i2d_DSAPrivateKey_fp:
.LFB1435:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	i2d_DSAPrivateKey@GOTPCREL(%rip), %rdi
	jmp	ASN1_i2d_fp@PLT
	.cfi_endproc
.LFE1435:
	.size	i2d_DSAPrivateKey_fp, .-i2d_DSAPrivateKey_fp
	.p2align 4
	.globl	d2i_DSA_PUBKEY_fp
	.type	d2i_DSA_PUBKEY_fp, @function
d2i_DSA_PUBKEY_fp:
.LFB1436:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	movq	%rsi, %rcx
	movq	DSA_new@GOTPCREL(%rip), %rdi
	movq	d2i_DSA_PUBKEY@GOTPCREL(%rip), %rsi
	jmp	ASN1_d2i_fp@PLT
	.cfi_endproc
.LFE1436:
	.size	d2i_DSA_PUBKEY_fp, .-d2i_DSA_PUBKEY_fp
	.p2align 4
	.globl	i2d_DSA_PUBKEY_fp
	.type	i2d_DSA_PUBKEY_fp, @function
i2d_DSA_PUBKEY_fp:
.LFB1437:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	i2d_DSA_PUBKEY@GOTPCREL(%rip), %rdi
	jmp	ASN1_i2d_fp@PLT
	.cfi_endproc
.LFE1437:
	.size	i2d_DSA_PUBKEY_fp, .-i2d_DSA_PUBKEY_fp
	.p2align 4
	.globl	d2i_DSAPrivateKey_bio
	.type	d2i_DSAPrivateKey_bio, @function
d2i_DSAPrivateKey_bio:
.LFB1438:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	movq	%rsi, %rcx
	movq	DSA_new@GOTPCREL(%rip), %rdi
	movq	d2i_DSAPrivateKey@GOTPCREL(%rip), %rsi
	jmp	ASN1_d2i_bio@PLT
	.cfi_endproc
.LFE1438:
	.size	d2i_DSAPrivateKey_bio, .-d2i_DSAPrivateKey_bio
	.p2align 4
	.globl	i2d_DSAPrivateKey_bio
	.type	i2d_DSAPrivateKey_bio, @function
i2d_DSAPrivateKey_bio:
.LFB1439:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	i2d_DSAPrivateKey@GOTPCREL(%rip), %rdi
	jmp	ASN1_i2d_bio@PLT
	.cfi_endproc
.LFE1439:
	.size	i2d_DSAPrivateKey_bio, .-i2d_DSAPrivateKey_bio
	.p2align 4
	.globl	d2i_DSA_PUBKEY_bio
	.type	d2i_DSA_PUBKEY_bio, @function
d2i_DSA_PUBKEY_bio:
.LFB1440:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	movq	%rsi, %rcx
	movq	DSA_new@GOTPCREL(%rip), %rdi
	movq	d2i_DSA_PUBKEY@GOTPCREL(%rip), %rsi
	jmp	ASN1_d2i_bio@PLT
	.cfi_endproc
.LFE1440:
	.size	d2i_DSA_PUBKEY_bio, .-d2i_DSA_PUBKEY_bio
	.p2align 4
	.globl	i2d_DSA_PUBKEY_bio
	.type	i2d_DSA_PUBKEY_bio, @function
i2d_DSA_PUBKEY_bio:
.LFB1441:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	i2d_DSA_PUBKEY@GOTPCREL(%rip), %rdi
	jmp	ASN1_i2d_bio@PLT
	.cfi_endproc
.LFE1441:
	.size	i2d_DSA_PUBKEY_bio, .-i2d_DSA_PUBKEY_bio
	.p2align 4
	.globl	d2i_EC_PUBKEY_fp
	.type	d2i_EC_PUBKEY_fp, @function
d2i_EC_PUBKEY_fp:
.LFB1442:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	movq	%rsi, %rcx
	movq	EC_KEY_new@GOTPCREL(%rip), %rdi
	movq	d2i_EC_PUBKEY@GOTPCREL(%rip), %rsi
	jmp	ASN1_d2i_fp@PLT
	.cfi_endproc
.LFE1442:
	.size	d2i_EC_PUBKEY_fp, .-d2i_EC_PUBKEY_fp
	.p2align 4
	.globl	i2d_EC_PUBKEY_fp
	.type	i2d_EC_PUBKEY_fp, @function
i2d_EC_PUBKEY_fp:
.LFB1443:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	i2d_EC_PUBKEY@GOTPCREL(%rip), %rdi
	jmp	ASN1_i2d_fp@PLT
	.cfi_endproc
.LFE1443:
	.size	i2d_EC_PUBKEY_fp, .-i2d_EC_PUBKEY_fp
	.p2align 4
	.globl	d2i_ECPrivateKey_fp
	.type	d2i_ECPrivateKey_fp, @function
d2i_ECPrivateKey_fp:
.LFB1444:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	movq	%rsi, %rcx
	movq	EC_KEY_new@GOTPCREL(%rip), %rdi
	movq	d2i_ECPrivateKey@GOTPCREL(%rip), %rsi
	jmp	ASN1_d2i_fp@PLT
	.cfi_endproc
.LFE1444:
	.size	d2i_ECPrivateKey_fp, .-d2i_ECPrivateKey_fp
	.p2align 4
	.globl	i2d_ECPrivateKey_fp
	.type	i2d_ECPrivateKey_fp, @function
i2d_ECPrivateKey_fp:
.LFB1445:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	i2d_ECPrivateKey@GOTPCREL(%rip), %rdi
	jmp	ASN1_i2d_fp@PLT
	.cfi_endproc
.LFE1445:
	.size	i2d_ECPrivateKey_fp, .-i2d_ECPrivateKey_fp
	.p2align 4
	.globl	d2i_EC_PUBKEY_bio
	.type	d2i_EC_PUBKEY_bio, @function
d2i_EC_PUBKEY_bio:
.LFB1446:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	movq	%rsi, %rcx
	movq	EC_KEY_new@GOTPCREL(%rip), %rdi
	movq	d2i_EC_PUBKEY@GOTPCREL(%rip), %rsi
	jmp	ASN1_d2i_bio@PLT
	.cfi_endproc
.LFE1446:
	.size	d2i_EC_PUBKEY_bio, .-d2i_EC_PUBKEY_bio
	.p2align 4
	.globl	i2d_EC_PUBKEY_bio
	.type	i2d_EC_PUBKEY_bio, @function
i2d_EC_PUBKEY_bio:
.LFB1447:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	i2d_EC_PUBKEY@GOTPCREL(%rip), %rdi
	jmp	ASN1_i2d_bio@PLT
	.cfi_endproc
.LFE1447:
	.size	i2d_EC_PUBKEY_bio, .-i2d_EC_PUBKEY_bio
	.p2align 4
	.globl	d2i_ECPrivateKey_bio
	.type	d2i_ECPrivateKey_bio, @function
d2i_ECPrivateKey_bio:
.LFB1448:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	movq	%rsi, %rcx
	movq	EC_KEY_new@GOTPCREL(%rip), %rdi
	movq	d2i_ECPrivateKey@GOTPCREL(%rip), %rsi
	jmp	ASN1_d2i_bio@PLT
	.cfi_endproc
.LFE1448:
	.size	d2i_ECPrivateKey_bio, .-d2i_ECPrivateKey_bio
	.p2align 4
	.globl	i2d_ECPrivateKey_bio
	.type	i2d_ECPrivateKey_bio, @function
i2d_ECPrivateKey_bio:
.LFB1449:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	i2d_ECPrivateKey@GOTPCREL(%rip), %rdi
	jmp	ASN1_i2d_bio@PLT
	.cfi_endproc
.LFE1449:
	.size	i2d_ECPrivateKey_bio, .-i2d_ECPrivateKey_bio
	.p2align 4
	.globl	X509_pubkey_digest
	.type	X509_pubkey_digest, @function
X509_pubkey_digest:
.LFB1450:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	X509_get0_pubkey_bitstr@PLT
	testq	%rax, %rax
	je	.L66
	movslq	(%rax), %rsi
	movq	8(%rax), %rdi
	addq	$8, %rsp
	movq	%rbx, %r8
	movq	%r12, %rcx
	popq	%rbx
	movq	%r13, %rdx
	popq	%r12
	xorl	%r9d, %r9d
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	EVP_Digest@PLT
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1450:
	.size	X509_pubkey_digest, .-X509_pubkey_digest
	.p2align 4
	.globl	X509_digest
	.type	X509_digest, @function
X509_digest:
.LFB1451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rcx, %rbx
	call	EVP_sha1@PLT
	cmpq	%rax, %r12
	je	.L77
.L69:
	movq	%rbx, %r8
	movq	%r14, %rcx
	popq	%rbx
	movq	%r13, %rdx
	movq	%r12, %rsi
	leaq	X509_it(%rip), %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ASN1_item_digest@PLT
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	movl	224(%r13), %eax
	andl	$384, %eax
	cmpl	$256, %eax
	jne	.L69
	testq	%rbx, %rbx
	je	.L70
	movl	$20, (%rbx)
.L70:
	movdqu	304(%r13), %xmm0
	popq	%rbx
	popq	%r12
	movups	%xmm0, (%r14)
	movl	320(%r13), %eax
	popq	%r13
	movl	%eax, 16(%r14)
	movl	$1, %eax
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1451:
	.size	X509_digest, .-X509_digest
	.p2align 4
	.globl	X509_CRL_digest
	.type	X509_CRL_digest, @function
X509_CRL_digest:
.LFB1452:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rcx, %rbx
	call	EVP_sha1@PLT
	cmpq	%rax, %r12
	je	.L87
.L79:
	movq	%rbx, %r8
	movq	%r14, %rcx
	popq	%rbx
	movq	%r13, %rdx
	movq	%r12, %rsi
	leaq	X509_CRL_it(%rip), %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ASN1_item_digest@PLT
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	movl	132(%r13), %eax
	andl	$384, %eax
	cmpl	$256, %eax
	jne	.L79
	testq	%rbx, %rbx
	je	.L80
	movl	$20, (%rbx)
.L80:
	movdqu	184(%r13), %xmm0
	popq	%rbx
	popq	%r12
	movups	%xmm0, (%r14)
	movl	200(%r13), %eax
	popq	%r13
	movl	%eax, 16(%r14)
	movl	$1, %eax
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1452:
	.size	X509_CRL_digest, .-X509_CRL_digest
	.p2align 4
	.globl	X509_REQ_digest
	.type	X509_REQ_digest, @function
X509_REQ_digest:
.LFB1453:
	.cfi_startproc
	endbr64
	movq	%rcx, %r8
	movq	%rdx, %rcx
	movq	%rdi, %rdx
	leaq	X509_REQ_it(%rip), %rdi
	jmp	ASN1_item_digest@PLT
	.cfi_endproc
.LFE1453:
	.size	X509_REQ_digest, .-X509_REQ_digest
	.p2align 4
	.globl	X509_NAME_digest
	.type	X509_NAME_digest, @function
X509_NAME_digest:
.LFB1454:
	.cfi_startproc
	endbr64
	movq	%rcx, %r8
	movq	%rdx, %rcx
	movq	%rdi, %rdx
	leaq	X509_NAME_it(%rip), %rdi
	jmp	ASN1_item_digest@PLT
	.cfi_endproc
.LFE1454:
	.size	X509_NAME_digest, .-X509_NAME_digest
	.p2align 4
	.globl	PKCS7_ISSUER_AND_SERIAL_digest
	.type	PKCS7_ISSUER_AND_SERIAL_digest, @function
PKCS7_ISSUER_AND_SERIAL_digest:
.LFB1455:
	.cfi_startproc
	endbr64
	movq	%rcx, %r8
	movq	%rdx, %rcx
	movq	%rdi, %rdx
	leaq	PKCS7_ISSUER_AND_SERIAL_it(%rip), %rdi
	jmp	ASN1_item_digest@PLT
	.cfi_endproc
.LFE1455:
	.size	PKCS7_ISSUER_AND_SERIAL_digest, .-PKCS7_ISSUER_AND_SERIAL_digest
	.p2align 4
	.globl	d2i_PKCS8_fp
	.type	d2i_PKCS8_fp, @function
d2i_PKCS8_fp:
.LFB1456:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	movq	%rsi, %rcx
	movq	X509_SIG_new@GOTPCREL(%rip), %rdi
	movq	d2i_X509_SIG@GOTPCREL(%rip), %rsi
	jmp	ASN1_d2i_fp@PLT
	.cfi_endproc
.LFE1456:
	.size	d2i_PKCS8_fp, .-d2i_PKCS8_fp
	.p2align 4
	.globl	i2d_PKCS8_fp
	.type	i2d_PKCS8_fp, @function
i2d_PKCS8_fp:
.LFB1457:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	i2d_X509_SIG@GOTPCREL(%rip), %rdi
	jmp	ASN1_i2d_fp@PLT
	.cfi_endproc
.LFE1457:
	.size	i2d_PKCS8_fp, .-i2d_PKCS8_fp
	.p2align 4
	.globl	d2i_PKCS8_bio
	.type	d2i_PKCS8_bio, @function
d2i_PKCS8_bio:
.LFB1458:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	movq	%rsi, %rcx
	movq	X509_SIG_new@GOTPCREL(%rip), %rdi
	movq	d2i_X509_SIG@GOTPCREL(%rip), %rsi
	jmp	ASN1_d2i_bio@PLT
	.cfi_endproc
.LFE1458:
	.size	d2i_PKCS8_bio, .-d2i_PKCS8_bio
	.p2align 4
	.globl	i2d_PKCS8_bio
	.type	i2d_PKCS8_bio, @function
i2d_PKCS8_bio:
.LFB1459:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	i2d_X509_SIG@GOTPCREL(%rip), %rdi
	jmp	ASN1_i2d_bio@PLT
	.cfi_endproc
.LFE1459:
	.size	i2d_PKCS8_bio, .-i2d_PKCS8_bio
	.p2align 4
	.globl	d2i_PKCS8_PRIV_KEY_INFO_fp
	.type	d2i_PKCS8_PRIV_KEY_INFO_fp, @function
d2i_PKCS8_PRIV_KEY_INFO_fp:
.LFB1460:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	movq	%rsi, %rcx
	movq	PKCS8_PRIV_KEY_INFO_new@GOTPCREL(%rip), %rdi
	movq	d2i_PKCS8_PRIV_KEY_INFO@GOTPCREL(%rip), %rsi
	jmp	ASN1_d2i_fp@PLT
	.cfi_endproc
.LFE1460:
	.size	d2i_PKCS8_PRIV_KEY_INFO_fp, .-d2i_PKCS8_PRIV_KEY_INFO_fp
	.p2align 4
	.globl	i2d_PKCS8_PRIV_KEY_INFO_fp
	.type	i2d_PKCS8_PRIV_KEY_INFO_fp, @function
i2d_PKCS8_PRIV_KEY_INFO_fp:
.LFB1461:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	i2d_PKCS8_PRIV_KEY_INFO@GOTPCREL(%rip), %rdi
	jmp	ASN1_i2d_fp@PLT
	.cfi_endproc
.LFE1461:
	.size	i2d_PKCS8_PRIV_KEY_INFO_fp, .-i2d_PKCS8_PRIV_KEY_INFO_fp
	.p2align 4
	.globl	i2d_PKCS8PrivateKeyInfo_fp
	.type	i2d_PKCS8PrivateKeyInfo_fp, @function
i2d_PKCS8PrivateKeyInfo_fp:
.LFB1462:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	movq	%rsi, %rdi
	pushq	%r13
	.cfi_offset 13, -32
	xorl	%r13d, %r13d
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	call	EVP_PKEY2PKCS8@PLT
	testq	%rax, %rax
	je	.L97
	movq	i2d_PKCS8_PRIV_KEY_INFO@GOTPCREL(%rip), %rdi
	movq	%rax, %r12
	movq	%rax, %rdx
	movq	%r14, %rsi
	call	ASN1_i2d_fp@PLT
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	PKCS8_PRIV_KEY_INFO_free@PLT
.L97:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1462:
	.size	i2d_PKCS8PrivateKeyInfo_fp, .-i2d_PKCS8PrivateKeyInfo_fp
	.p2align 4
	.globl	i2d_PrivateKey_fp
	.type	i2d_PrivateKey_fp, @function
i2d_PrivateKey_fp:
.LFB1463:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	i2d_PrivateKey@GOTPCREL(%rip), %rdi
	jmp	ASN1_i2d_fp@PLT
	.cfi_endproc
.LFE1463:
	.size	i2d_PrivateKey_fp, .-i2d_PrivateKey_fp
	.p2align 4
	.globl	d2i_PrivateKey_fp
	.type	d2i_PrivateKey_fp, @function
d2i_PrivateKey_fp:
.LFB1464:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	movq	%rsi, %rcx
	movq	EVP_PKEY_new@GOTPCREL(%rip), %rdi
	movq	d2i_AutoPrivateKey@GOTPCREL(%rip), %rsi
	jmp	ASN1_d2i_fp@PLT
	.cfi_endproc
.LFE1464:
	.size	d2i_PrivateKey_fp, .-d2i_PrivateKey_fp
	.p2align 4
	.globl	i2d_PUBKEY_fp
	.type	i2d_PUBKEY_fp, @function
i2d_PUBKEY_fp:
.LFB1465:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	i2d_PUBKEY@GOTPCREL(%rip), %rdi
	jmp	ASN1_i2d_fp@PLT
	.cfi_endproc
.LFE1465:
	.size	i2d_PUBKEY_fp, .-i2d_PUBKEY_fp
	.p2align 4
	.globl	d2i_PUBKEY_fp
	.type	d2i_PUBKEY_fp, @function
d2i_PUBKEY_fp:
.LFB1466:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	movq	%rsi, %rcx
	movq	EVP_PKEY_new@GOTPCREL(%rip), %rdi
	movq	d2i_PUBKEY@GOTPCREL(%rip), %rsi
	jmp	ASN1_d2i_fp@PLT
	.cfi_endproc
.LFE1466:
	.size	d2i_PUBKEY_fp, .-d2i_PUBKEY_fp
	.p2align 4
	.globl	d2i_PKCS8_PRIV_KEY_INFO_bio
	.type	d2i_PKCS8_PRIV_KEY_INFO_bio, @function
d2i_PKCS8_PRIV_KEY_INFO_bio:
.LFB1467:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	movq	%rsi, %rcx
	movq	PKCS8_PRIV_KEY_INFO_new@GOTPCREL(%rip), %rdi
	movq	d2i_PKCS8_PRIV_KEY_INFO@GOTPCREL(%rip), %rsi
	jmp	ASN1_d2i_bio@PLT
	.cfi_endproc
.LFE1467:
	.size	d2i_PKCS8_PRIV_KEY_INFO_bio, .-d2i_PKCS8_PRIV_KEY_INFO_bio
	.p2align 4
	.globl	i2d_PKCS8_PRIV_KEY_INFO_bio
	.type	i2d_PKCS8_PRIV_KEY_INFO_bio, @function
i2d_PKCS8_PRIV_KEY_INFO_bio:
.LFB1468:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	i2d_PKCS8_PRIV_KEY_INFO@GOTPCREL(%rip), %rdi
	jmp	ASN1_i2d_bio@PLT
	.cfi_endproc
.LFE1468:
	.size	i2d_PKCS8_PRIV_KEY_INFO_bio, .-i2d_PKCS8_PRIV_KEY_INFO_bio
	.p2align 4
	.globl	i2d_PKCS8PrivateKeyInfo_bio
	.type	i2d_PKCS8PrivateKeyInfo_bio, @function
i2d_PKCS8PrivateKeyInfo_bio:
.LFB1469:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	movq	%rsi, %rdi
	pushq	%r13
	.cfi_offset 13, -32
	xorl	%r13d, %r13d
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	call	EVP_PKEY2PKCS8@PLT
	testq	%rax, %rax
	je	.L109
	movq	i2d_PKCS8_PRIV_KEY_INFO@GOTPCREL(%rip), %rdi
	movq	%rax, %r12
	movq	%rax, %rdx
	movq	%r14, %rsi
	call	ASN1_i2d_bio@PLT
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	PKCS8_PRIV_KEY_INFO_free@PLT
.L109:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1469:
	.size	i2d_PKCS8PrivateKeyInfo_bio, .-i2d_PKCS8PrivateKeyInfo_bio
	.p2align 4
	.globl	i2d_PrivateKey_bio
	.type	i2d_PrivateKey_bio, @function
i2d_PrivateKey_bio:
.LFB1470:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	i2d_PrivateKey@GOTPCREL(%rip), %rdi
	jmp	ASN1_i2d_bio@PLT
	.cfi_endproc
.LFE1470:
	.size	i2d_PrivateKey_bio, .-i2d_PrivateKey_bio
	.p2align 4
	.globl	d2i_PrivateKey_bio
	.type	d2i_PrivateKey_bio, @function
d2i_PrivateKey_bio:
.LFB1471:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	movq	%rsi, %rcx
	movq	EVP_PKEY_new@GOTPCREL(%rip), %rdi
	movq	d2i_AutoPrivateKey@GOTPCREL(%rip), %rsi
	jmp	ASN1_d2i_bio@PLT
	.cfi_endproc
.LFE1471:
	.size	d2i_PrivateKey_bio, .-d2i_PrivateKey_bio
	.p2align 4
	.globl	i2d_PUBKEY_bio
	.type	i2d_PUBKEY_bio, @function
i2d_PUBKEY_bio:
.LFB1472:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	i2d_PUBKEY@GOTPCREL(%rip), %rdi
	jmp	ASN1_i2d_bio@PLT
	.cfi_endproc
.LFE1472:
	.size	i2d_PUBKEY_bio, .-i2d_PUBKEY_bio
	.p2align 4
	.globl	d2i_PUBKEY_bio
	.type	d2i_PUBKEY_bio, @function
d2i_PUBKEY_bio:
.LFB1473:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	movq	%rsi, %rcx
	movq	EVP_PKEY_new@GOTPCREL(%rip), %rdi
	movq	d2i_PUBKEY@GOTPCREL(%rip), %rsi
	jmp	ASN1_d2i_bio@PLT
	.cfi_endproc
.LFE1473:
	.size	d2i_PUBKEY_bio, .-d2i_PUBKEY_bio
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
