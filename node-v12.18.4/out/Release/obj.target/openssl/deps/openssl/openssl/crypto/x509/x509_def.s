	.file	"x509_def.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/etc/ssl/private"
	.text
	.p2align 4
	.globl	X509_get_default_private_dir
	.type	X509_get_default_private_dir, @function
X509_get_default_private_dir:
.LFB779:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE779:
	.size	X509_get_default_private_dir, .-X509_get_default_private_dir
	.section	.rodata.str1.1
.LC1:
	.string	"/etc/ssl"
	.text
	.p2align 4
	.globl	X509_get_default_cert_area
	.type	X509_get_default_cert_area, @function
X509_get_default_cert_area:
.LFB780:
	.cfi_startproc
	endbr64
	leaq	.LC1(%rip), %rax
	ret
	.cfi_endproc
.LFE780:
	.size	X509_get_default_cert_area, .-X509_get_default_cert_area
	.section	.rodata.str1.1
.LC2:
	.string	"/etc/ssl/certs"
	.text
	.p2align 4
	.globl	X509_get_default_cert_dir
	.type	X509_get_default_cert_dir, @function
X509_get_default_cert_dir:
.LFB781:
	.cfi_startproc
	endbr64
	leaq	.LC2(%rip), %rax
	ret
	.cfi_endproc
.LFE781:
	.size	X509_get_default_cert_dir, .-X509_get_default_cert_dir
	.section	.rodata.str1.1
.LC3:
	.string	"/etc/ssl/cert.pem"
	.text
	.p2align 4
	.globl	X509_get_default_cert_file
	.type	X509_get_default_cert_file, @function
X509_get_default_cert_file:
.LFB782:
	.cfi_startproc
	endbr64
	leaq	.LC3(%rip), %rax
	ret
	.cfi_endproc
.LFE782:
	.size	X509_get_default_cert_file, .-X509_get_default_cert_file
	.section	.rodata.str1.1
.LC4:
	.string	"SSL_CERT_DIR"
	.text
	.p2align 4
	.globl	X509_get_default_cert_dir_env
	.type	X509_get_default_cert_dir_env, @function
X509_get_default_cert_dir_env:
.LFB783:
	.cfi_startproc
	endbr64
	leaq	.LC4(%rip), %rax
	ret
	.cfi_endproc
.LFE783:
	.size	X509_get_default_cert_dir_env, .-X509_get_default_cert_dir_env
	.section	.rodata.str1.1
.LC5:
	.string	"SSL_CERT_FILE"
	.text
	.p2align 4
	.globl	X509_get_default_cert_file_env
	.type	X509_get_default_cert_file_env, @function
X509_get_default_cert_file_env:
.LFB784:
	.cfi_startproc
	endbr64
	leaq	.LC5(%rip), %rax
	ret
	.cfi_endproc
.LFE784:
	.size	X509_get_default_cert_file_env, .-X509_get_default_cert_file_env
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
