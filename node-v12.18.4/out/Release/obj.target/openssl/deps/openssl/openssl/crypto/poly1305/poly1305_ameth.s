	.file	"poly1305_ameth.c"
	.text
	.p2align 4
	.type	poly1305_size, @function
poly1305_size:
.LFB469:
	.cfi_startproc
	endbr64
	movl	$16, %eax
	ret
	.cfi_endproc
.LFE469:
	.size	poly1305_size, .-poly1305_size
	.p2align 4
	.type	poly1305_pkey_ctrl, @function
poly1305_pkey_ctrl:
.LFB471:
	.cfi_startproc
	endbr64
	movl	$-2, %eax
	ret
	.cfi_endproc
.LFE471:
	.size	poly1305_pkey_ctrl, .-poly1305_pkey_ctrl
	.p2align 4
	.type	poly1305_get_priv_key, @function
poly1305_get_priv_key:
.LFB474:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	testq	%rsi, %rsi
	je	.L10
	movq	40(%rdi), %r13
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L4
	cmpq	$31, (%rdx)
	ja	.L11
.L4:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movq	%r13, %rdi
	movq	%rsi, %r12
	call	ASN1_STRING_length@PLT
	movq	%r13, %rdi
	movl	%eax, %r14d
	call	ASN1_STRING_get0_data@PLT
	movslq	%r14d, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	memcpy@PLT
.L10:
	movq	$32, (%rbx)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE474:
	.size	poly1305_get_priv_key, .-poly1305_get_priv_key
	.p2align 4
	.type	poly1305_key_free, @function
poly1305_key_free:
.LFB470:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	EVP_PKEY_get0@PLT
	testq	%rax, %rax
	je	.L12
	movq	8(%rax), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L14
	movslq	(%rax), %rsi
	call	OPENSSL_cleanse@PLT
.L14:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ASN1_OCTET_STRING_free@PLT
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE470:
	.size	poly1305_key_free, .-poly1305_key_free
	.p2align 4
	.type	poly1305_pkey_public_cmp, @function
poly1305_pkey_public_cmp:
.LFB472:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -32
	call	EVP_PKEY_get0@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	EVP_PKEY_get0@PLT
	movq	%r12, %rsi
	popq	%r12
	popq	%r13
	movq	%rax, %rdi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	ASN1_OCTET_STRING_cmp@PLT
	.cfi_endproc
.LFE472:
	.size	poly1305_pkey_public_cmp, .-poly1305_pkey_public_cmp
	.p2align 4
	.type	poly1305_set_priv_key, @function
poly1305_set_priv_key:
.LFB473:
	.cfi_startproc
	endbr64
	cmpq	$0, 40(%rdi)
	jne	.L31
	cmpq	$32, %rdx
	je	.L22
.L31:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%rsi, -24(%rbp)
	call	ASN1_OCTET_STRING_new@PLT
	movq	%rax, %r12
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L21
	movq	-24(%rbp), %rsi
	movl	$32, %edx
	movq	%r12, %rdi
	call	ASN1_OCTET_STRING_set@PLT
	testl	%eax, %eax
	je	.L33
	movq	%r12, 40(%rbx)
	movl	$1, %eax
.L21:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	%eax, -24(%rbp)
	call	ASN1_OCTET_STRING_free@PLT
	movl	-24(%rbp), %eax
	jmp	.L21
	.cfi_endproc
.LFE473:
	.size	poly1305_set_priv_key, .-poly1305_set_priv_key
	.globl	poly1305_asn1_meth
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"POLY1305"
.LC1:
	.string	"OpenSSL POLY1305 method"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	poly1305_asn1_meth, @object
	.size	poly1305_asn1_meth, 280
poly1305_asn1_meth:
	.long	1061
	.long	1061
	.quad	0
	.quad	.LC0
	.quad	.LC1
	.quad	0
	.quad	0
	.quad	poly1305_pkey_public_cmp
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	poly1305_size
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	poly1305_key_free
	.quad	poly1305_pkey_ctrl
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	poly1305_set_priv_key
	.quad	0
	.quad	poly1305_get_priv_key
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
