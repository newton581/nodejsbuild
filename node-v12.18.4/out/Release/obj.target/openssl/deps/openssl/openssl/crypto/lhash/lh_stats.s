	.file	"lh_stats.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"node %6u -> %3u\n"
	.text
	.p2align 4
	.globl	OPENSSL_LH_node_stats
	.type	OPENSSL_LH_node_stats, @function
OPENSSL_LH_node_stats:
.LFB252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	testq	%rax, %rax
	je	.L1
	movq	%r13, %rcx
	xorl	%edx, %edx
	movl	$106, %esi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	BIO_ctrl@PLT
	movl	24(%rbx), %eax
	testl	%eax, %eax
	je	.L3
	xorl	%r13d, %r13d
	leaq	.LC0(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L6:
	movq	(%rbx), %rdx
	movl	%r13d, %eax
	xorl	%ecx, %ecx
	movq	(%rdx,%rax,8), %rax
	testq	%rax, %rax
	je	.L4
	.p2align 4,,10
	.p2align 3
.L5:
	movq	8(%rax), %rax
	addl	$1, %ecx
	testq	%rax, %rax
	jne	.L5
.L4:
	movl	%r13d, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	addl	$1, %r13d
	cmpl	24(%rbx), %r13d
	jb	.L6
.L3:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_free@PLT
	.p2align 4,,10
	.p2align 3
.L1:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE252:
	.size	OPENSSL_LH_node_stats, .-OPENSSL_LH_node_stats
	.section	.rodata.str1.1
.LC1:
	.string	"num_items             = %lu\n"
.LC2:
	.string	"num_nodes             = %u\n"
.LC3:
	.string	"num_alloc_nodes       = %u\n"
.LC4:
	.string	"num_expands           = %lu\n"
.LC5:
	.string	"num_expand_reallocs   = %lu\n"
.LC6:
	.string	"num_contracts         = %lu\n"
.LC7:
	.string	"num_contract_reallocs = %lu\n"
.LC8:
	.string	"num_hash_calls        = %lu\n"
.LC9:
	.string	"num_comp_calls        = %lu\n"
.LC10:
	.string	"num_insert            = %lu\n"
.LC11:
	.string	"num_replace           = %lu\n"
.LC12:
	.string	"num_delete            = %lu\n"
.LC13:
	.string	"num_no_delete         = %lu\n"
.LC14:
	.string	"num_retrieve          = %lu\n"
.LC15:
	.string	"num_retrieve_miss     = %lu\n"
.LC16:
	.string	"num_hash_comps        = %lu\n"
	.text
	.p2align 4
	.globl	OPENSSL_LH_stats_bio
	.type	OPENSSL_LH_stats_bio, @function
OPENSSL_LH_stats_bio:
.LFB254:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	leaq	.LC1(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	56(%rdi), %rdx
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	BIO_printf@PLT
	movl	24(%rbx), %edx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC2(%rip), %rsi
	call	BIO_printf@PLT
	movl	28(%rbx), %edx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC3(%rip), %rsi
	call	BIO_printf@PLT
	movq	64(%rbx), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC4(%rip), %rsi
	call	BIO_printf@PLT
	movq	72(%rbx), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC5(%rip), %rsi
	call	BIO_printf@PLT
	movq	80(%rbx), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC6(%rip), %rsi
	call	BIO_printf@PLT
	movq	88(%rbx), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC7(%rip), %rsi
	call	BIO_printf@PLT
	movq	%r12, %rdi
	leaq	.LC8(%rip), %rsi
	xorl	%eax, %eax
	movq	96(%rbx), %rdx
	call	BIO_printf@PLT
	movq	%r12, %rdi
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	movq	104(%rbx), %rdx
	call	BIO_printf@PLT
	movq	112(%rbx), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC10(%rip), %rsi
	call	BIO_printf@PLT
	movq	120(%rbx), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC11(%rip), %rsi
	call	BIO_printf@PLT
	movq	128(%rbx), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC12(%rip), %rsi
	call	BIO_printf@PLT
	movq	136(%rbx), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC13(%rip), %rsi
	call	BIO_printf@PLT
	movq	%r12, %rdi
	leaq	.LC14(%rip), %rsi
	xorl	%eax, %eax
	movq	144(%rbx), %rdx
	call	BIO_printf@PLT
	movq	%r12, %rdi
	leaq	.LC15(%rip), %rsi
	xorl	%eax, %eax
	movq	152(%rbx), %rdx
	call	BIO_printf@PLT
	movq	%r12, %rdi
	leaq	.LC16(%rip), %rsi
	xorl	%eax, %eax
	movq	160(%rbx), %rdx
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	BIO_printf@PLT
	.cfi_endproc
.LFE254:
	.size	OPENSSL_LH_stats_bio, .-OPENSSL_LH_stats_bio
	.p2align 4
	.globl	OPENSSL_LH_stats
	.type	OPENSSL_LH_stats, @function
OPENSSL_LH_stats:
.LFB251:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	testq	%rax, %rax
	je	.L14
	movq	%rax, %r12
	movq	%r14, %rcx
	xorl	%edx, %edx
	movl	$106, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	OPENSSL_LH_stats_bio
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_free@PLT
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE251:
	.size	OPENSSL_LH_stats, .-OPENSSL_LH_stats
	.p2align 4
	.globl	OPENSSL_LH_node_stats_bio
	.type	OPENSSL_LH_node_stats_bio, @function
OPENSSL_LH_node_stats_bio:
.LFB255:
	.cfi_startproc
	endbr64
	movl	24(%rdi), %eax
	testl	%eax, %eax
	je	.L26
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	.LC0(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L21:
	movq	(%r14), %rdx
	movl	%ebx, %eax
	xorl	%ecx, %ecx
	movq	(%rdx,%rax,8), %rax
	testq	%rax, %rax
	je	.L19
	.p2align 4,,10
	.p2align 3
.L20:
	movq	8(%rax), %rax
	addl	$1, %ecx
	testq	%rax, %rax
	jne	.L20
.L19:
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	addl	$1, %ebx
	cmpl	%ebx, 24(%r14)
	ja	.L21
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
.L26:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.cfi_endproc
.LFE255:
	.size	OPENSSL_LH_node_stats_bio, .-OPENSSL_LH_node_stats_bio
	.section	.rodata.str1.1
.LC17:
	.string	"%lu nodes used out of %u\n"
.LC18:
	.string	"%lu items\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC19:
	.string	"load %d.%02d  actual load %d.%02d\n"
	.text
	.p2align 4
	.globl	OPENSSL_LH_node_usage_stats_bio
	.type	OPENSSL_LH_node_usage_stats_bio, @function
OPENSSL_LH_node_usage_stats_bio:
.LFB256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	24(%rdi), %r8d
	testl	%r8d, %r8d
	je	.L30
	movq	(%rdi), %rcx
	movq	%rdi, %r14
	leal	-1(%r8), %eax
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	leaq	8(%rcx), %rdi
	leaq	(%rdi,%rax,8), %rax
	.p2align 4,,10
	.p2align 3
.L33:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L31
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L32:
	movq	8(%rcx), %rcx
	addq	$1, %rsi
	testq	%rcx, %rcx
	jne	.L32
	testq	%rsi, %rsi
	je	.L31
	addq	$1, %rbx
	addq	%rsi, %r12
.L31:
	movq	%rdi, %rcx
	cmpq	%rdi, %rax
	je	.L43
	addq	$8, %rdi
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L43:
	movq	%rbx, %rdx
	leaq	.LC17(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	movl	%r8d, %ecx
	call	BIO_printf@PLT
	xorl	%eax, %eax
	movq	%r12, %rdx
	movq	%r13, %rdi
	leaq	.LC18(%rip), %rsi
	call	BIO_printf@PLT
	testq	%rbx, %rbx
	je	.L29
	movq	%r12, %rax
	xorl	%edx, %edx
	movl	24(%r14), %esi
	divq	%rbx
	movq	%rdx, %rdi
	movq	%rax, %r8
	xorl	%edx, %edx
	movq	%r12, %rax
	divq	%rsi
	movq	%rax, %r12
	leaq	(%rdi,%rdi,4), %rax
	movq	%rdx, %rcx
	xorl	%edx, %edx
	leaq	(%rax,%rax,4), %rax
	movq	%r13, %rdi
	salq	$2, %rax
	divq	%rbx
	xorl	%edx, %edx
	popq	%rbx
	movq	%rax, %r9
	leaq	(%rcx,%rcx,4), %rax
	leaq	(%rax,%rax,4), %rax
	salq	$2, %rax
	divq	%rsi
	movl	%r12d, %edx
	leaq	.LC19(%rip), %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	%eax, %ecx
	xorl	%eax, %eax
	jmp	BIO_printf@PLT
.L29:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L30:
	.cfi_restore_state
	movq	%r13, %rdi
	xorl	%edx, %edx
	leaq	.LC17(%rip), %rsi
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	xorl	%edx, %edx
	popq	%r13
	leaq	.LC18(%rip), %rsi
	popq	%r14
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	BIO_printf@PLT
	.cfi_endproc
.LFE256:
	.size	OPENSSL_LH_node_usage_stats_bio, .-OPENSSL_LH_node_usage_stats_bio
	.p2align 4
	.globl	OPENSSL_LH_node_usage_stats
	.type	OPENSSL_LH_node_usage_stats, @function
OPENSSL_LH_node_usage_stats:
.LFB253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	testq	%rax, %rax
	je	.L45
	movq	%rax, %r12
	movq	%r14, %rcx
	xorl	%edx, %edx
	movl	$106, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	OPENSSL_LH_node_usage_stats_bio
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_free@PLT
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE253:
	.size	OPENSSL_LH_node_usage_stats, .-OPENSSL_LH_node_usage_stats
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
