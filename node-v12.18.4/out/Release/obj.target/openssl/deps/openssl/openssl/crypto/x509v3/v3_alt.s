	.file	"v3_alt.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/x509v3/v3_alt.c"
	.text
	.p2align 4
	.type	copy_email, @function
copy_email:
.LFB1302:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdi, %rdi
	je	.L2
	cmpl	$1, (%rdi)
	je	.L19
	movq	16(%rdi), %r8
	movq	%rsi, %r13
	movl	%edx, %r12d
	testq	%r8, %r8
	je	.L36
	movq	%r8, %rdi
	call	X509_get_subject_name@PLT
	movq	%rax, %rbx
.L8:
	movl	$-1, %r14d
	testl	%r12d, %r12d
	je	.L17
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L37:
	movl	%eax, %esi
	movq	%rbx, %rdi
	call	X509_NAME_get_entry@PLT
	movq	%rax, %rdi
	call	X509_NAME_ENTRY_get_data@PLT
	movq	%rax, %rdi
	call	ASN1_STRING_dup@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L14
	call	GENERAL_NAME_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L10
	movq	%r15, 8(%rax)
	movq	%rax, %rsi
	movq	%r13, %rdi
	movl	$1, (%rax)
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L16
.L17:
	movl	%r14d, %edx
	movl	$48, %esi
	movq	%rbx, %rdi
	call	X509_NAME_get_index_by_NID@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	jns	.L37
.L19:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	X509_NAME_get_entry@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	X509_NAME_ENTRY_get_data@PLT
	movq	%rax, %rdi
	call	ASN1_STRING_dup@PLT
	movl	%r14d, %esi
	movq	%rbx, %rdi
	subl	$1, %r14d
	movq	%rax, %r15
	call	X509_NAME_delete_entry@PLT
	movq	%r12, %rdi
	call	X509_NAME_ENTRY_free@PLT
	testq	%r15, %r15
	je	.L14
	call	GENERAL_NAME_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L10
	movq	%r15, 8(%rax)
	movq	%rax, %rsi
	movq	%r13, %rdi
	movl	$1, (%rax)
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L16
.L12:
	movl	%r14d, %edx
	movl	$48, %esi
	movq	%rbx, %rdi
	call	X509_NAME_get_index_by_NID@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	jns	.L18
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L14:
	xorl	%r15d, %r15d
.L10:
	movl	$369, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$122, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
.L7:
	movq	%r12, %rdi
	call	GENERAL_NAME_free@PLT
	movq	%r15, %rdi
	call	ASN1_IA5STRING_free@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2
	call	X509_REQ_get_subject_name@PLT
	movq	%rax, %rbx
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L2:
	movl	$125, %edx
	movl	$122, %esi
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
	movl	$349, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$34, %edi
	call	ERR_put_error@PLT
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L16:
	movl	$376, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r15d, %r15d
	movl	$122, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	jmp	.L7
	.cfi_endproc
.LFE1302:
	.size	copy_email, .-copy_email
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"email"
.LC2:
	.string	"URI"
.LC3:
	.string	"DNS"
.LC4:
	.string	"RID"
.LC5:
	.string	"IP"
.LC6:
	.string	"dirName"
.LC7:
	.string	"otherName"
.LC8:
	.string	"name="
.LC9:
	.string	"value="
.LC10:
	.string	"section="
	.text
	.p2align 4
	.type	v2i_GENERAL_NAME_ex.constprop.0, @function
v2i_GENERAL_NAME_ex.constprop.0:
.LFB1311:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdx), %r14
	movq	8(%rdx), %r12
	testq	%r14, %r14
	je	.L103
	movq	%rsi, %r13
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	name_cmp@PLT
	testl	%eax, %eax
	je	.L41
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	name_cmp@PLT
	testl	%eax, %eax
	jne	.L104
	call	GENERAL_NAME_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L49
	movl	$6, %ebx
.L48:
	call	ASN1_IA5STRING_new@PLT
	movq	%rax, 8(%r12)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L64
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	ASN1_STRING_set@PLT
	testl	%eax, %eax
	je	.L64
	.p2align 4,,10
	.p2align 3
.L56:
	movl	%ebx, (%r12)
.L38:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	call	GENERAL_NAME_new@PLT
	movl	$1, %ebx
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L48
.L49:
	movl	$443, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$164, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L64:
	movl	$501, %r8d
	movl	$65, %edx
	movl	$164, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L55:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	GENERAL_NAME_free@PLT
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L104:
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	name_cmp@PLT
	testl	%eax, %eax
	jne	.L105
	call	GENERAL_NAME_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L49
	movl	$2, %ebx
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L103:
	movl	$528, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$124, %edx
	xorl	%r12d, %r12d
	movl	$117, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L105:
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	name_cmp@PLT
	testl	%eax, %eax
	jne	.L106
	call	GENERAL_NAME_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L49
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	OBJ_txt2obj@PLT
	movl	$459, %r8d
	movl	$119, %edx
	leaq	.LC0(%rip), %rcx
	testq	%rax, %rax
	je	.L102
	movq	%rax, 8(%r12)
	movl	$8, %ebx
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L106:
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	name_cmp@PLT
	testl	%eax, %eax
	jne	.L107
	call	GENERAL_NAME_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L49
	movq	%r14, %rdi
	movl	$7, %ebx
	call	a2i_IPADDRESS@PLT
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	jne	.L56
	movl	$473, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$118, %edx
.L102:
	movl	$164, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	movq	%r14, %rdx
	movl	$2, %edi
	xorl	%eax, %eax
	leaq	.LC9(%rip), %rsi
	call	ERR_add_error_data@PLT
	jmp	.L55
.L107:
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	name_cmp@PLT
	testl	%eax, %eax
	jne	.L108
	call	GENERAL_NAME_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L49
	call	X509_NAME_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L109
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	X509V3_get_section@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L110
	movl	$4097, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	X509V3_NAME_from_section@PLT
	testl	%eax, %eax
	je	.L60
	movq	%r15, 8(%r12)
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movl	$4, %ebx
	call	X509V3_section_free@PLT
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L109:
	xorl	%edi, %edi
	call	X509_NAME_free@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	X509V3_section_free@PLT
.L58:
	movl	$481, %r8d
	movl	$149, %edx
	movl	$164, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L60:
	movq	%r15, %rdi
	call	X509_NAME_free@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	X509V3_section_free@PLT
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L110:
	movl	$593, %r8d
	movl	$150, %edx
	movl	$144, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r14, %rdx
	xorl	%eax, %eax
	movl	$2, %edi
	leaq	.LC10(%rip), %rsi
	call	ERR_add_error_data@PLT
	movq	%r15, %rdi
	call	X509_NAME_free@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	X509V3_section_free@PLT
	jmp	.L58
.L108:
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	name_cmp@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	jne	.L111
	call	GENERAL_NAME_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L49
	movl	$59, %esi
	movq	%r14, %rdi
	call	strchr@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L62
	call	OTHERNAME_new@PLT
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L62
	movq	8(%rax), %rdi
	call	ASN1_TYPE_free@PLT
	movq	8(%r12), %rdx
	leaq	1(%r15), %rdi
	movq	%r13, %rsi
	movq	%rdx, -56(%rbp)
	call	ASN1_generate_v3@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, 8(%rdx)
	testq	%rax, %rax
	je	.L62
	subq	%r14, %r15
	movl	$573, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r14, %rdi
	movslq	%r15d, %rsi
	call	CRYPTO_strndup@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L62
	xorl	%esi, %esi
	movq	8(%r12), %r14
	movq	%rax, %rdi
	call	OBJ_txt2obj@PLT
	movl	$577, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, (%r14)
	call	CRYPTO_free@PLT
	movq	8(%r12), %rax
	cmpq	$0, (%rax)
	jne	.L56
	.p2align 4,,10
	.p2align 3
.L62:
	movl	$488, %r8d
	movl	$147, %edx
	movl	$164, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L55
.L111:
	movl	$547, %r8d
	movl	$117, %edx
	movl	$117, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdx
	movl	$2, %edi
	xorl	%eax, %eax
	leaq	.LC8(%rip), %rsi
	xorl	%r12d, %r12d
	call	ERR_add_error_data@PLT
	jmp	.L38
	.cfi_endproc
.LFE1311:
	.size	v2i_GENERAL_NAME_ex.constprop.0, .-v2i_GENERAL_NAME_ex.constprop.0
	.section	.rodata.str1.1
.LC11:
	.string	"issuer"
.LC12:
	.string	"copy"
	.text
	.p2align 4
	.type	v2i_issuer_alt, @function
v2i_issuer_alt:
.LFB1299:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -64(%rbp)
	movq	%rdx, %rdi
	call	OPENSSL_sk_num@PLT
	xorl	%edi, %edi
	movl	%eax, %esi
	movl	%eax, -52(%rbp)
	movl	%eax, %r15d
	call	OPENSSL_sk_new_reserve@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L113
	xorl	%ebx, %ebx
	testl	%r15d, %r15d
	jg	.L114
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L116:
	movq	-64(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	call	v2i_GENERAL_NAME_ex.constprop.0
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L121
	movq	%r13, %rdi
	call	OPENSSL_sk_push@PLT
.L119:
	addl	$1, %ebx
	cmpl	-52(%rbp), %ebx
	je	.L112
.L114:
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	leaq	.LC11(%rip), %rsi
	movq	8(%rax), %rdi
	movq	%rax, %r15
	call	name_cmp@PLT
	testl	%eax, %eax
	jne	.L116
	movq	16(%r15), %rsi
	testq	%rsi, %rsi
	je	.L116
	movl	$5, %ecx
	leaq	.LC12(%rip), %rdi
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	movsbl	%cl, %ecx
	testl	%ecx, %ecx
	jne	.L116
	testq	%r12, %r12
	je	.L117
	cmpl	$1, (%r12)
	je	.L119
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L117
	movl	$85, %esi
	movl	$-1, %edx
	movl	%ecx, -56(%rbp)
	call	X509_get_ext_by_NID@PLT
	movl	%eax, %esi
	testl	%eax, %eax
	js	.L119
	movq	8(%r12), %rdi
	call	X509_get_ext@PLT
	movl	-56(%rbp), %ecx
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L124
	movl	%ecx, -56(%rbp)
	call	X509V3_EXT_d2i@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L124
	movq	%rax, %rdi
	call	OPENSSL_sk_num@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	movl	%eax, -68(%rbp)
	call	OPENSSL_sk_reserve@PLT
	testl	%eax, %eax
	je	.L125
	movl	-68(%rbp), %eax
	movl	-56(%rbp), %ecx
	testl	%eax, %eax
	jle	.L127
	.p2align 4,,10
	.p2align 3
.L126:
	movl	%ecx, %esi
	movq	%r15, %rdi
	movl	%ecx, -56(%rbp)
	call	OPENSSL_sk_value@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	movl	-56(%rbp), %ecx
	addl	$1, %ecx
	cmpl	%ecx, -68(%rbp)
	jne	.L126
.L127:
	movq	%r15, %rdi
	addl	$1, %ebx
	call	OPENSSL_sk_free@PLT
	cmpl	-52(%rbp), %ebx
	jne	.L114
.L112:
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	.cfi_restore_state
	movl	$263, %r8d
	movl	$127, %edx
	movl	$123, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L121:
	movq	GENERAL_NAME_free@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L124:
	movl	$271, %r8d
	movl	$126, %edx
	movl	$123, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L121
.L125:
	movl	$277, %r8d
	movl	$65, %edx
	movl	$123, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L121
.L113:
	movl	$226, %r8d
	movl	$65, %edx
	movl	$153, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%edi, %edi
	call	OPENSSL_sk_free@PLT
	jmp	.L112
	.cfi_endproc
.LFE1299:
	.size	v2i_issuer_alt, .-v2i_issuer_alt
	.section	.rodata.str1.1
.LC13:
	.string	"move"
	.text
	.p2align 4
	.type	v2i_subject_alt, @function
v2i_subject_alt:
.LFB1301:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -64(%rbp)
	movq	%rdx, %rdi
	call	OPENSSL_sk_num@PLT
	xorl	%edi, %edi
	movl	%eax, %esi
	movl	%eax, -52(%rbp)
	movl	%eax, %r15d
	call	OPENSSL_sk_new_reserve@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L147
	xorl	%ebx, %ebx
	testl	%r15d, %r15d
	jg	.L148
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L150:
	movq	8(%r15), %rdi
	leaq	.LC1(%rip), %rsi
	call	name_cmp@PLT
	testl	%eax, %eax
	jne	.L153
	movq	16(%r15), %rsi
	testq	%rsi, %rsi
	je	.L153
	movl	$5, %ecx
	leaq	.LC13(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L176
.L153:
	movq	-64(%rbp), %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	call	v2i_GENERAL_NAME_ex.constprop.0
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L151
	movq	%r12, %rdi
	call	OPENSSL_sk_push@PLT
.L152:
	addl	$1, %ebx
	cmpl	-52(%rbp), %ebx
	je	.L146
.L148:
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	leaq	.LC1(%rip), %rsi
	movq	8(%rax), %rdi
	movq	%rax, %r15
	call	name_cmp@PLT
	testl	%eax, %eax
	jne	.L150
	movq	16(%r15), %rsi
	testq	%rsi, %rsi
	je	.L150
	movl	$5, %ecx
	leaq	.LC12(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	xorl	%edx, %edx
	testb	%al, %al
	jne	.L150
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	copy_email
	testl	%eax, %eax
	jne	.L152
	.p2align 4,,10
	.p2align 3
.L151:
	movq	GENERAL_NAME_free@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	OPENSSL_sk_pop_free@PLT
.L146:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_restore_state
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	copy_email
	testl	%eax, %eax
	jne	.L152
	jmp	.L151
.L147:
	movl	$305, %r8d
	movl	$65, %edx
	movl	$154, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%edi, %edi
	call	OPENSSL_sk_free@PLT
	jmp	.L146
	.cfi_endproc
.LFE1301:
	.size	v2i_subject_alt, .-v2i_subject_alt
	.section	.rodata.str1.1
.LC14:
	.string	"<unsupported>"
.LC15:
	.string	"othername"
.LC16:
	.string	"X400Name"
.LC17:
	.string	"EdiPartyName"
.LC18:
	.string	"DirName"
.LC19:
	.string	"%d.%d.%d.%d"
.LC20:
	.string	"%X"
.LC21:
	.string	":"
.LC22:
	.string	"<invalid>"
.LC23:
	.string	"IP Address"
.LC24:
	.string	"Registered ID"
	.text
	.p2align 4
	.globl	i2v_GENERAL_NAME
	.type	i2v_GENERAL_NAME, @function
i2v_GENERAL_NAME:
.LFB1297:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$312, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -344(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$8, (%rsi)
	ja	.L178
	movl	(%rsi), %eax
	leaq	.L180(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L180:
	.long	.L188-.L180
	.long	.L187-.L180
	.long	.L186-.L180
	.long	.L185-.L180
	.long	.L184-.L180
	.long	.L183-.L180
	.long	.L182-.L180
	.long	.L181-.L180
	.long	.L179-.L180
	.text
	.p2align 4,,10
	.p2align 3
.L184:
	movq	8(%rsi), %rdi
	leaq	-320(%rbp), %r12
	movl	$256, %edx
	movq	%r12, %rsi
	call	X509_NAME_oneline@PLT
	testq	%rax, %rax
	je	.L189
	leaq	-344(%rbp), %rdx
	movq	%r12, %rsi
	leaq	.LC18(%rip), %rdi
	call	X509V3_add_value@PLT
	testl	%eax, %eax
	je	.L189
	.p2align 4,,10
	.p2align 3
.L178:
	movq	-344(%rbp), %rax
.L177:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L233
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	.cfi_restore_state
	movq	8(%rsi), %rax
	movq	8(%rax), %r13
	movl	(%rax), %eax
	cmpl	$4, %eax
	je	.L234
	cmpl	$16, %eax
	jne	.L194
	movb	$0, -320(%rbp)
	movzwl	0(%r13), %ecx
	xorl	%eax, %eax
	leaq	2(%r13), %rbx
	leaq	-325(%rbp), %r12
	movl	$5, %esi
	addq	$16, %r13
	rolw	$8, %cx
	leaq	.LC20(%rip), %rdx
	movq	%r12, %rdi
	movzwl	%cx, %ecx
	leaq	-320(%rbp), %r15
	leaq	.LC21(%rip), %r14
	call	BIO_snprintf@PLT
	movl	$256, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	__strcat_chk@PLT
	.p2align 4,,10
	.p2align 3
.L195:
	movl	$256, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	addq	$2, %rbx
	call	__strcat_chk@PLT
	movzwl	-2(%rbx), %ecx
	movl	$5, %esi
	xorl	%eax, %eax
	leaq	.LC20(%rip), %rdx
	movq	%r12, %rdi
	rolw	$8, %cx
	movzwl	%cx, %ecx
	call	BIO_snprintf@PLT
	movl	$256, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	__strcat_chk@PLT
	cmpq	%r13, %rbx
	jne	.L195
.L193:
	leaq	-344(%rbp), %rdx
	movq	%r15, %rsi
	leaq	.LC23(%rip), %rdi
	call	X509V3_add_value@PLT
	testl	%eax, %eax
	jne	.L178
.L189:
	xorl	%eax, %eax
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L179:
	movq	8(%rsi), %rdx
	leaq	-320(%rbp), %r12
	movl	$256, %esi
	movq	%r12, %rdi
	call	i2t_ASN1_OBJECT@PLT
	leaq	-344(%rbp), %rdx
	movq	%r12, %rsi
	leaq	.LC24(%rip), %rdi
	call	X509V3_add_value@PLT
	testl	%eax, %eax
	jne	.L178
	xorl	%eax, %eax
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L188:
	leaq	-344(%rbp), %rdx
	leaq	.LC14(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	call	X509V3_add_value@PLT
	testl	%eax, %eax
	jne	.L178
	xorl	%eax, %eax
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L185:
	leaq	-344(%rbp), %rdx
	leaq	.LC14(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	call	X509V3_add_value@PLT
	testl	%eax, %eax
	jne	.L178
	xorl	%eax, %eax
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L183:
	leaq	-344(%rbp), %rdx
	leaq	.LC14(%rip), %rsi
	leaq	.LC17(%rip), %rdi
	call	X509V3_add_value@PLT
	testl	%eax, %eax
	jne	.L178
	xorl	%eax, %eax
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L182:
	movq	8(%rsi), %rax
	leaq	-344(%rbp), %rdx
	leaq	.LC2(%rip), %rdi
	movq	8(%rax), %rsi
	call	X509V3_add_value_uchar@PLT
	testl	%eax, %eax
	jne	.L178
	xorl	%eax, %eax
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L187:
	movq	8(%rsi), %rax
	leaq	-344(%rbp), %rdx
	leaq	.LC1(%rip), %rdi
	movq	8(%rax), %rsi
	call	X509V3_add_value_uchar@PLT
	testl	%eax, %eax
	jne	.L178
	xorl	%eax, %eax
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L186:
	movq	8(%rsi), %rax
	leaq	-344(%rbp), %rdx
	leaq	.LC3(%rip), %rdi
	movq	8(%rax), %rsi
	call	X509V3_add_value_uchar@PLT
	testl	%eax, %eax
	jne	.L178
	xorl	%eax, %eax
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L194:
	leaq	-344(%rbp), %rdx
	leaq	.LC22(%rip), %rsi
	leaq	.LC23(%rip), %rdi
	call	X509V3_add_value@PLT
	testl	%eax, %eax
	jne	.L178
	xorl	%eax, %eax
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L234:
	movzbl	3(%r13), %eax
	subq	$8, %rsp
	movzbl	2(%r13), %r9d
	leaq	-320(%rbp), %r15
	movzbl	1(%r13), %r8d
	movzbl	0(%r13), %ecx
	movl	$256, %esi
	movq	%r15, %rdi
	pushq	%rax
	leaq	.LC19(%rip), %rdx
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	popq	%rax
	popq	%rdx
	jmp	.L193
.L233:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1297:
	.size	i2v_GENERAL_NAME, .-i2v_GENERAL_NAME
	.p2align 4
	.globl	i2v_GENERAL_NAMES
	.type	i2v_GENERAL_NAMES, @function
i2v_GENERAL_NAMES:
.LFB1296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L239:
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	i2v_GENERAL_NAME
	testq	%rax, %rax
	je	.L242
	addl	$1, %r15d
	movq	%rax, %r12
.L236:
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r15d
	jl	.L239
	testq	%r12, %r12
	je	.L243
.L235:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L242:
	.cfi_restore_state
	testq	%r14, %r14
	je	.L244
	xorl	%r12d, %r12d
	jmp	.L235
.L243:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	OPENSSL_sk_new_null@PLT
.L244:
	.cfi_restore_state
	movq	X509V3_conf_free@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L235
	.cfi_endproc
.LFE1296:
	.size	i2v_GENERAL_NAMES, .-i2v_GENERAL_NAMES
	.section	.rodata.str1.1
.LC25:
	.string	"othername:<unsupported>"
.LC26:
	.string	"X400Name:<unsupported>"
.LC27:
	.string	"EdiPartyName:<unsupported>"
.LC28:
	.string	"email:"
.LC29:
	.string	"DNS:"
.LC30:
	.string	"URI:"
.LC31:
	.string	"DirName:"
.LC32:
	.string	"IP Address:%d.%d.%d.%d"
.LC33:
	.string	":%X"
.LC34:
	.string	"\n"
.LC35:
	.string	"IP Address:<invalid>"
.LC36:
	.string	"Registered ID:"
	.text
	.p2align 4
	.globl	GENERAL_NAME_print
	.type	GENERAL_NAME_print, @function
GENERAL_NAME_print:
.LFB1298:
	.cfi_startproc
	endbr64
	cmpl	$8, (%rsi)
	ja	.L262
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.L248(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	(%rsi), %eax
	movq	%rsi, %rbx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L248:
	.long	.L256-.L248
	.long	.L255-.L248
	.long	.L254-.L248
	.long	.L253-.L248
	.long	.L252-.L248
	.long	.L251-.L248
	.long	.L250-.L248
	.long	.L249-.L248
	.long	.L247-.L248
	.text
	.p2align 4,,10
	.p2align 3
.L249:
	movq	8(%rsi), %rax
	movq	8(%rax), %r14
	movl	(%rax), %eax
	cmpl	$4, %eax
	je	.L266
	cmpl	$16, %eax
	je	.L267
	leaq	.LC35(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L246:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L247:
	.cfi_restore_state
	leaq	.LC36(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	8(%rbx), %rsi
	movq	%r12, %rdi
	call	i2a_ASN1_OBJECT@PLT
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L256:
	.cfi_restore_state
	leaq	.LC25(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	.cfi_restore_state
	leaq	.LC28(%rip), %rsi
.L265:
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	8(%rbx), %rsi
	movq	%r12, %rdi
	call	ASN1_STRING_print@PLT
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L254:
	.cfi_restore_state
	leaq	.LC29(%rip), %rsi
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L253:
	leaq	.LC26(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L252:
	.cfi_restore_state
	leaq	.LC31(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	8(%rbx), %rsi
	movq	%r12, %rdi
	xorl	%edx, %edx
	movl	$8520479, %ecx
	call	X509_NAME_print_ex@PLT
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L251:
	.cfi_restore_state
	leaq	.LC27(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L250:
	.cfi_restore_state
	leaq	.LC30(%rip), %rsi
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L267:
	leaq	.LC23(%rip), %rsi
	xorl	%eax, %eax
	leaq	16(%r14), %r13
	call	BIO_printf@PLT
	leaq	.LC33(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L259:
	movzwl	(%r14), %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	addq	$2, %r14
	rolw	$8, %dx
	movzwl	%dx, %edx
	call	BIO_printf@PLT
	cmpq	%r13, %r14
	jne	.L259
	leaq	.LC34(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L266:
	movzbl	1(%r14), %ecx
	movzbl	(%r14), %edx
	xorl	%eax, %eax
	leaq	.LC32(%rip), %rsi
	movzbl	3(%r14), %r9d
	movzbl	2(%r14), %r8d
	call	BIO_printf@PLT
	jmp	.L246
.L262:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1298:
	.size	GENERAL_NAME_print, .-GENERAL_NAME_print
	.p2align 4
	.globl	v2i_GENERAL_NAMES
	.type	v2i_GENERAL_NAMES, @function
v2i_GENERAL_NAMES:
.LFB1303:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -64(%rbp)
	call	OPENSSL_sk_num@PLT
	xorl	%edi, %edi
	movl	%eax, %esi
	movl	%eax, -56(%rbp)
	call	OPENSSL_sk_new_reserve@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L339
	movl	-56(%rbp), %eax
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jg	.L297
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L342:
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	name_cmp@PLT
	testl	%eax, %eax
	jne	.L340
	call	GENERAL_NAME_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L281
	movl	$6, %r8d
.L280:
	movl	%r8d, -72(%rbp)
	call	ASN1_IA5STRING_new@PLT
	movl	-72(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, 8(%r14)
	je	.L296
	movq	%r12, %rdi
	movl	%r8d, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	strlen@PLT
	movq	-72(%rbp), %r10
	movq	%r12, %rsi
	movq	%rax, %rdx
	movq	%r10, %rdi
	call	ASN1_STRING_set@PLT
	movl	-80(%rbp), %r8d
	testl	%eax, %eax
	je	.L296
	.p2align 4,,10
	.p2align 3
.L288:
	movl	%r8d, (%r14)
	movq	%r14, %rsi
	movq	%r13, %rdi
	addl	$1, %ebx
	call	OPENSSL_sk_push@PLT
	cmpl	-56(%rbp), %ebx
	je	.L268
.L297:
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	movq	16(%rax), %r12
	movq	8(%rax), %r14
	testq	%r12, %r12
	je	.L341
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	call	name_cmp@PLT
	testl	%eax, %eax
	jne	.L342
	call	GENERAL_NAME_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L281
	movl	$1, %r8d
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L340:
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	call	name_cmp@PLT
	testl	%eax, %eax
	jne	.L343
	call	GENERAL_NAME_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L281
	movl	$2, %r8d
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L341:
	movl	$528, %r8d
	movl	$124, %edx
	movl	$117, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L272:
	movq	GENERAL_NAME_free@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	OPENSSL_sk_pop_free@PLT
.L268:
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L296:
	.cfi_restore_state
	movl	$501, %r8d
	movl	$65, %edx
	movl	$164, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L287:
	movq	%r14, %rdi
	call	GENERAL_NAME_free@PLT
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L343:
	leaq	.LC4(%rip), %rsi
	movq	%r14, %rdi
	call	name_cmp@PLT
	testl	%eax, %eax
	jne	.L344
	call	GENERAL_NAME_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L281
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	OBJ_txt2obj@PLT
	testq	%rax, %rax
	je	.L345
	movq	%rax, 8(%r14)
	movl	$8, %r8d
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L281:
	movl	$443, %r8d
	movl	$65, %edx
	movl	$164, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L344:
	leaq	.LC5(%rip), %rsi
	movq	%r14, %rdi
	call	name_cmp@PLT
	testl	%eax, %eax
	jne	.L346
	call	GENERAL_NAME_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L281
	movq	%r12, %rdi
	call	a2i_IPADDRESS@PLT
	movq	%rax, 8(%r14)
	testq	%rax, %rax
	je	.L347
	movl	$7, %r8d
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L345:
	movl	$459, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$119, %edx
.L338:
	movl	$164, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	movq	%r12, %rdx
	movl	$2, %edi
	xorl	%eax, %eax
	leaq	.LC9(%rip), %rsi
	call	ERR_add_error_data@PLT
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L347:
	movl	$473, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$118, %edx
	jmp	.L338
.L346:
	leaq	.LC6(%rip), %rsi
	movq	%r14, %rdi
	call	name_cmp@PLT
	testl	%eax, %eax
	jne	.L348
	call	GENERAL_NAME_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L281
	call	X509_NAME_new@PLT
	testq	%rax, %rax
	je	.L349
	movq	-64(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, -72(%rbp)
	call	X509V3_get_section@PLT
	movq	-72(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %rsi
	je	.L350
	movq	%r10, %rdi
	movl	$4097, %edx
	movq	%r10, -72(%rbp)
	movq	%rax, -80(%rbp)
	call	X509V3_NAME_from_section@PLT
	movq	-72(%rbp), %r10
	movq	-80(%rbp), %rsi
	testl	%eax, %eax
	je	.L292
	movq	%r10, 8(%r14)
	movq	-64(%rbp), %rdi
	call	X509V3_section_free@PLT
	movl	$4, %r8d
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L349:
	xorl	%edi, %edi
	call	X509_NAME_free@PLT
	movq	-64(%rbp), %rdi
	xorl	%esi, %esi
	call	X509V3_section_free@PLT
.L290:
	movl	$481, %r8d
	movl	$149, %edx
	movl	$164, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L292:
	movq	%r10, %rdi
	movq	%rsi, -56(%rbp)
	call	X509_NAME_free@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rdi
	call	X509V3_section_free@PLT
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L350:
	movl	$593, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$150, %edx
	movl	$144, %esi
	movl	$34, %edi
	movq	%r10, -56(%rbp)
	call	ERR_put_error@PLT
	movq	%r12, %rdx
	xorl	%eax, %eax
	movl	$2, %edi
	leaq	.LC10(%rip), %rsi
	call	ERR_add_error_data@PLT
	movq	-56(%rbp), %r10
	movq	%r10, %rdi
	call	X509_NAME_free@PLT
	movq	-64(%rbp), %rdi
	xorl	%esi, %esi
	call	X509V3_section_free@PLT
	jmp	.L290
.L339:
	movl	$402, %r8d
	movl	$65, %edx
	movl	$118, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%edi, %edi
	call	OPENSSL_sk_free@PLT
	jmp	.L268
.L348:
	leaq	.LC7(%rip), %rsi
	movq	%r14, %rdi
	call	name_cmp@PLT
	testl	%eax, %eax
	jne	.L351
	movl	%eax, -72(%rbp)
	call	GENERAL_NAME_new@PLT
	movl	-72(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L281
	movl	$59, %esi
	movq	%r12, %rdi
	movl	%r8d, -72(%rbp)
	call	strchr@PLT
	movl	-72(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, -80(%rbp)
	je	.L294
	movl	%r8d, -84(%rbp)
	call	OTHERNAME_new@PLT
	movq	%rax, 8(%r14)
	testq	%rax, %rax
	je	.L294
	movq	8(%rax), %rdi
	call	ASN1_TYPE_free@PLT
	movq	-80(%rbp), %rax
	movq	8(%r14), %rdx
	movq	-64(%rbp), %rsi
	leaq	1(%rax), %rdi
	movq	%rdx, -72(%rbp)
	call	ASN1_generate_v3@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, 8(%rdx)
	testq	%rax, %rax
	je	.L294
	movq	-80(%rbp), %rax
	movq	%r12, %rdi
	movl	$573, %ecx
	leaq	.LC0(%rip), %rdx
	subq	%r12, %rax
	movslq	%eax, %rsi
	call	CRYPTO_strndup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L294
	movq	8(%r14), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rdx, -72(%rbp)
	call	OBJ_txt2obj@PLT
	movq	-72(%rbp), %rdx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, (%rdx)
	movl	$577, %edx
	call	CRYPTO_free@PLT
	movq	8(%r14), %rax
	movl	-84(%rbp), %r8d
	cmpq	$0, (%rax)
	jne	.L288
	.p2align 4,,10
	.p2align 3
.L294:
	movl	$488, %r8d
	movl	$147, %edx
	movl	$164, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L287
.L351:
	movl	$547, %r8d
	movl	$117, %edx
	movl	$117, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r14, %rdx
	movl	$2, %edi
	xorl	%eax, %eax
	leaq	.LC8(%rip), %rsi
	call	ERR_add_error_data@PLT
	jmp	.L272
	.cfi_endproc
.LFE1303:
	.size	v2i_GENERAL_NAMES, .-v2i_GENERAL_NAMES
	.p2align 4
	.globl	v2i_GENERAL_NAME
	.type	v2i_GENERAL_NAME, @function
v2i_GENERAL_NAME:
.LFB1304:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdx), %r14
	movq	8(%rdx), %r12
	testq	%r14, %r14
	je	.L417
	movq	%rsi, %r13
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	name_cmp@PLT
	testl	%eax, %eax
	je	.L355
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	name_cmp@PLT
	testl	%eax, %eax
	jne	.L418
	call	GENERAL_NAME_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L363
	movl	$6, %ebx
.L362:
	call	ASN1_IA5STRING_new@PLT
	movq	%rax, 8(%r12)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L378
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	ASN1_STRING_set@PLT
	testl	%eax, %eax
	je	.L378
	.p2align 4,,10
	.p2align 3
.L370:
	movl	%ebx, (%r12)
.L352:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L355:
	.cfi_restore_state
	call	GENERAL_NAME_new@PLT
	movl	$1, %ebx
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L362
.L363:
	movl	$443, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$164, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L378:
	movl	$501, %r8d
	movl	$65, %edx
	movl	$164, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L369:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	GENERAL_NAME_free@PLT
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L418:
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	name_cmp@PLT
	testl	%eax, %eax
	jne	.L419
	call	GENERAL_NAME_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L363
	movl	$2, %ebx
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L417:
	movl	$528, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$124, %edx
	xorl	%r12d, %r12d
	movl	$117, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L419:
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	name_cmp@PLT
	testl	%eax, %eax
	jne	.L420
	call	GENERAL_NAME_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L363
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	OBJ_txt2obj@PLT
	movl	$459, %r8d
	movl	$119, %edx
	leaq	.LC0(%rip), %rcx
	testq	%rax, %rax
	je	.L416
	movq	%rax, 8(%r12)
	movl	$8, %ebx
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L420:
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	name_cmp@PLT
	testl	%eax, %eax
	jne	.L421
	call	GENERAL_NAME_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L363
	movq	%r14, %rdi
	movl	$7, %ebx
	call	a2i_IPADDRESS@PLT
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	jne	.L370
	movl	$473, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$118, %edx
.L416:
	movl	$164, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	movq	%r14, %rdx
	movl	$2, %edi
	xorl	%eax, %eax
	leaq	.LC9(%rip), %rsi
	call	ERR_add_error_data@PLT
	jmp	.L369
.L421:
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	name_cmp@PLT
	testl	%eax, %eax
	jne	.L422
	call	GENERAL_NAME_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L363
	call	X509_NAME_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L423
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	X509V3_get_section@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L424
	movl	$4097, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	X509V3_NAME_from_section@PLT
	testl	%eax, %eax
	je	.L374
	movq	%r15, 8(%r12)
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movl	$4, %ebx
	call	X509V3_section_free@PLT
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L423:
	xorl	%edi, %edi
	call	X509_NAME_free@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	X509V3_section_free@PLT
.L372:
	movl	$481, %r8d
	movl	$149, %edx
	movl	$164, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L374:
	movq	%r15, %rdi
	call	X509_NAME_free@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	X509V3_section_free@PLT
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L424:
	movl	$593, %r8d
	movl	$150, %edx
	movl	$144, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r14, %rdx
	xorl	%eax, %eax
	movl	$2, %edi
	leaq	.LC10(%rip), %rsi
	call	ERR_add_error_data@PLT
	movq	%r15, %rdi
	call	X509_NAME_free@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	X509V3_section_free@PLT
	jmp	.L372
.L422:
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	name_cmp@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	jne	.L425
	call	GENERAL_NAME_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L363
	movl	$59, %esi
	movq	%r14, %rdi
	call	strchr@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L376
	call	OTHERNAME_new@PLT
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L376
	movq	8(%rax), %rdi
	call	ASN1_TYPE_free@PLT
	movq	8(%r12), %rdx
	leaq	1(%r15), %rdi
	movq	%r13, %rsi
	movq	%rdx, -56(%rbp)
	call	ASN1_generate_v3@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, 8(%rdx)
	testq	%rax, %rax
	je	.L376
	subq	%r14, %r15
	movl	$573, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r14, %rdi
	movslq	%r15d, %rsi
	call	CRYPTO_strndup@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L376
	xorl	%esi, %esi
	movq	8(%r12), %r14
	movq	%rax, %rdi
	call	OBJ_txt2obj@PLT
	movl	$577, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, (%r14)
	call	CRYPTO_free@PLT
	movq	8(%r12), %rax
	cmpq	$0, (%rax)
	jne	.L370
	.p2align 4,,10
	.p2align 3
.L376:
	movl	$488, %r8d
	movl	$147, %edx
	movl	$164, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L369
.L425:
	movl	$547, %r8d
	movl	$117, %edx
	movl	$117, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdx
	movl	$2, %edi
	xorl	%eax, %eax
	leaq	.LC8(%rip), %rsi
	xorl	%r12d, %r12d
	call	ERR_add_error_data@PLT
	jmp	.L352
	.cfi_endproc
.LFE1304:
	.size	v2i_GENERAL_NAME, .-v2i_GENERAL_NAME
	.p2align 4
	.globl	a2i_GENERAL_NAME
	.type	a2i_GENERAL_NAME, @function
a2i_GENERAL_NAME:
.LFB1305:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%r8, %r8
	je	.L467
	movq	%rdi, %r13
	movq	%rdx, %r14
	movl	%ecx, %ebx
	movq	%r8, %r15
	movq	%rdi, %r12
	testq	%rdi, %rdi
	je	.L468
.L429:
	cmpl	$8, %ebx
	ja	.L430
	leaq	.L432(%rip), %rcx
	movl	%ebx, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L432:
	.long	.L436-.L432
	.long	.L434-.L432
	.long	.L434-.L432
	.long	.L430-.L432
	.long	.L435-.L432
	.long	.L430-.L432
	.long	.L434-.L432
	.long	.L433-.L432
	.long	.L431-.L432
	.text
	.p2align 4,,10
	.p2align 3
.L434:
	call	ASN1_IA5STRING_new@PLT
	movq	%rax, 8(%r12)
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L450
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	ASN1_STRING_set@PLT
	testl	%eax, %eax
	je	.L450
	.p2align 4,,10
	.p2align 3
.L439:
	movl	%ebx, (%r12)
.L426:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L435:
	.cfi_restore_state
	call	X509_NAME_new@PLT
	testq	%rax, %rax
	je	.L469
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, -56(%rbp)
	call	X509V3_get_section@PLT
	movq	-56(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rsi
	je	.L470
	movq	%r9, %rdi
	movl	$4097, %edx
	movq	%r9, -56(%rbp)
	movq	%rax, -64(%rbp)
	call	X509V3_NAME_from_section@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rsi
	testl	%eax, %eax
	je	.L446
	movq	%r9, 8(%r12)
	movq	%r14, %rdi
	call	X509V3_section_free@PLT
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L433:
	movq	%r15, %rdi
	testl	%r9d, %r9d
	je	.L440
	call	a2i_IPADDRESS_NC@PLT
	movq	%rax, 8(%r12)
.L441:
	testq	%rax, %rax
	jne	.L439
	movl	$473, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$118, %edx
.L466:
	movl	$164, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	movq	%r15, %rdx
	movl	$2, %edi
	xorl	%eax, %eax
	leaq	.LC9(%rip), %rsi
	call	ERR_add_error_data@PLT
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L431:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	OBJ_txt2obj@PLT
	movl	$459, %r8d
	movl	$119, %edx
	leaq	.LC0(%rip), %rcx
	testq	%rax, %rax
	je	.L466
	movq	%rax, 8(%r12)
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L430:
	movl	$493, %r8d
	movl	$167, %edx
	movl	$164, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L438:
	testq	%r13, %r13
	je	.L471
	xorl	%r12d, %r12d
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L436:
	movl	$59, %esi
	movq	%r15, %rdi
	call	strchr@PLT
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L448
	call	OTHERNAME_new@PLT
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L448
	movq	8(%rax), %rdi
	call	ASN1_TYPE_free@PLT
	movq	-56(%rbp), %rax
	movq	8(%r12), %rdx
	movq	%r14, %rsi
	leaq	1(%rax), %rdi
	movq	%rdx, -64(%rbp)
	call	ASN1_generate_v3@PLT
	movq	-64(%rbp), %rdx
	movq	%rax, 8(%rdx)
	testq	%rax, %rax
	je	.L448
	movq	-56(%rbp), %rax
	movl	$573, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r15, %rdi
	subq	%r15, %rax
	movslq	%eax, %rsi
	call	CRYPTO_strndup@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L448
	xorl	%esi, %esi
	movq	8(%r12), %r15
	movq	%rax, %rdi
	call	OBJ_txt2obj@PLT
	movl	$577, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, (%r15)
	call	CRYPTO_free@PLT
	movq	8(%r12), %rax
	cmpq	$0, (%rax)
	jne	.L439
	.p2align 4,,10
	.p2align 3
.L448:
	movl	$488, %r8d
	movl	$147, %edx
	movl	$164, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L468:
	movl	%r9d, -56(%rbp)
	call	GENERAL_NAME_new@PLT
	movl	-56(%rbp), %r9d
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.L429
	movl	$443, %r8d
	movl	$65, %edx
	movl	$164, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L440:
	call	a2i_IPADDRESS@PLT
	movq	%rax, 8(%r12)
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L450:
	movl	$501, %r8d
	movl	$65, %edx
	movl	$164, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L471:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	GENERAL_NAME_free@PLT
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L469:
	xorl	%edi, %edi
	call	X509_NAME_free@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	X509V3_section_free@PLT
.L444:
	movl	$481, %r8d
	movl	$149, %edx
	movl	$164, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L446:
	movq	%r9, %rdi
	movq	%rsi, -56(%rbp)
	call	X509_NAME_free@PLT
	movq	-56(%rbp), %rsi
	movq	%r14, %rdi
	call	X509V3_section_free@PLT
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L467:
	movl	$434, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$124, %edx
	xorl	%r12d, %r12d
	movl	$164, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L470:
	movl	$593, %r8d
	movl	$150, %edx
	movl	$144, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r15, %rdx
	xorl	%eax, %eax
	movl	$2, %edi
	leaq	.LC10(%rip), %rsi
	call	ERR_add_error_data@PLT
	movq	-56(%rbp), %r9
	movq	%r9, %rdi
	call	X509_NAME_free@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	X509V3_section_free@PLT
	jmp	.L444
	.cfi_endproc
.LFE1305:
	.size	a2i_GENERAL_NAME, .-a2i_GENERAL_NAME
	.p2align 4
	.globl	v2i_GENERAL_NAME_ex
	.type	v2i_GENERAL_NAME_ex, @function
v2i_GENERAL_NAME_ex:
.LFB1306:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rcx), %r14
	movq	8(%rcx), %r15
	testq	%r14, %r14
	je	.L551
	movq	%rdi, %rbx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	movq	%rdx, %r12
	movl	%r8d, %r13d
	call	name_cmp@PLT
	testl	%eax, %eax
	je	.L475
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	name_cmp@PLT
	testl	%eax, %eax
	jne	.L552
	testq	%rbx, %rbx
	jne	.L512
	call	GENERAL_NAME_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L482
	.p2align 4,,10
	.p2align 3
.L549:
	movl	$6, %r13d
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L475:
	testq	%rbx, %rbx
	jne	.L511
	call	GENERAL_NAME_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L482
	movl	$1, %r13d
.L503:
	call	ASN1_IA5STRING_new@PLT
	movq	%rax, 8(%r15)
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L499
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	ASN1_STRING_set@PLT
	testl	%eax, %eax
	je	.L499
	.p2align 4,,10
	.p2align 3
.L489:
	movl	%r13d, (%r15)
.L472:
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L552:
	.cfi_restore_state
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	call	name_cmp@PLT
	testl	%eax, %eax
	jne	.L553
	testq	%rbx, %rbx
	jne	.L510
	call	GENERAL_NAME_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L482
	movl	$2, %r13d
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L551:
	movl	$528, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$124, %edx
	xorl	%r15d, %r15d
	movl	$117, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L511:
	movq	%rbx, %r15
	movl	$1, %r13d
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L499:
	movl	$501, %r8d
	movl	$65, %edx
	movl	$164, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L488:
	testq	%rbx, %rbx
	je	.L554
	xorl	%r15d, %r15d
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L554:
	movq	%r15, %rdi
	xorl	%r15d, %r15d
	call	GENERAL_NAME_free@PLT
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L512:
	movq	%rbx, %r15
	jmp	.L549
.L558:
	call	GENERAL_NAME_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L484
.L482:
	movl	$443, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r15d, %r15d
	movl	$164, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L553:
	leaq	.LC4(%rip), %rsi
	movq	%r15, %rdi
	call	name_cmp@PLT
	testl	%eax, %eax
	jne	.L555
	movq	%rbx, %r15
	testq	%rbx, %rbx
	je	.L556
.L485:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	OBJ_txt2obj@PLT
	movl	$459, %r8d
	movl	$119, %edx
	leaq	.LC0(%rip), %rcx
	testq	%rax, %rax
	je	.L550
	movq	%rax, 8(%r15)
	movl	$8, %r13d
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L555:
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	call	name_cmp@PLT
	testl	%eax, %eax
	jne	.L557
	testq	%rbx, %rbx
	je	.L558
	movq	%rbx, %r15
.L484:
	movq	%r14, %rdi
	testl	%r13d, %r13d
	je	.L490
	call	a2i_IPADDRESS_NC@PLT
	movq	%rax, 8(%r15)
.L491:
	movl	$7, %r13d
	testq	%rax, %rax
	jne	.L489
	movl	$473, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$118, %edx
.L550:
	movl	$164, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	movq	%r14, %rdx
	movl	$2, %edi
	xorl	%eax, %eax
	leaq	.LC9(%rip), %rsi
	call	ERR_add_error_data@PLT
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L510:
	movq	%rbx, %r15
	movl	$2, %r13d
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L490:
	call	a2i_IPADDRESS@PLT
	movq	%rax, 8(%r15)
	jmp	.L491
.L557:
	leaq	.LC6(%rip), %rsi
	movq	%r15, %rdi
	call	name_cmp@PLT
	testl	%eax, %eax
	jne	.L559
	movq	%rbx, %r15
	testq	%rbx, %rbx
	je	.L560
.L483:
	call	X509_NAME_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L561
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	X509V3_get_section@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L562
	movl	$4097, %edx
	movq	%r13, %rdi
	movq	%rax, -56(%rbp)
	call	X509V3_NAME_from_section@PLT
	movq	-56(%rbp), %rsi
	testl	%eax, %eax
	je	.L495
	movq	%r13, 8(%r15)
	movq	%r12, %rdi
	movl	$4, %r13d
	call	X509V3_section_free@PLT
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L561:
	xorl	%edi, %edi
	call	X509_NAME_free@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	X509V3_section_free@PLT
.L493:
	movl	$481, %r8d
	movl	$149, %edx
	movl	$164, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L495:
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	X509_NAME_free@PLT
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	call	X509V3_section_free@PLT
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L562:
	movl	$593, %r8d
	movl	$150, %edx
	movl	$144, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r14, %rdx
	xorl	%eax, %eax
	movl	$2, %edi
	leaq	.LC10(%rip), %rsi
	call	ERR_add_error_data@PLT
	movq	%r13, %rdi
	call	X509_NAME_free@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	X509V3_section_free@PLT
	jmp	.L493
.L559:
	leaq	.LC7(%rip), %rsi
	movq	%r15, %rdi
	call	name_cmp@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.L563
	testq	%rbx, %rbx
	je	.L564
	movq	%rbx, %r15
.L486:
	movl	$59, %esi
	movq	%r14, %rdi
	call	strchr@PLT
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L497
	call	OTHERNAME_new@PLT
	movq	%rax, 8(%r15)
	testq	%rax, %rax
	je	.L497
	movq	8(%rax), %rdi
	call	ASN1_TYPE_free@PLT
	movq	-56(%rbp), %rax
	movq	8(%r15), %rdx
	movq	%r12, %rsi
	leaq	1(%rax), %rdi
	movq	%rdx, -64(%rbp)
	call	ASN1_generate_v3@PLT
	movq	-64(%rbp), %rdx
	movq	%rax, 8(%rdx)
	testq	%rax, %rax
	je	.L497
	movq	-56(%rbp), %rax
	movl	$573, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r14, %rdi
	subq	%r14, %rax
	movslq	%eax, %rsi
	call	CRYPTO_strndup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L497
	xorl	%esi, %esi
	movq	8(%r15), %r14
	movq	%rax, %rdi
	call	OBJ_txt2obj@PLT
	movl	$577, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, (%r14)
	call	CRYPTO_free@PLT
	movq	8(%r15), %rax
	cmpq	$0, (%rax)
	jne	.L489
	.p2align 4,,10
	.p2align 3
.L497:
	movl	$488, %r8d
	movl	$147, %edx
	movl	$164, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L488
.L560:
	call	GENERAL_NAME_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L483
	jmp	.L482
.L563:
	movl	$547, %r8d
	movl	$117, %edx
	movl	$117, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r15, %rdx
	movl	$2, %edi
	xorl	%eax, %eax
	leaq	.LC8(%rip), %rsi
	xorl	%r15d, %r15d
	call	ERR_add_error_data@PLT
	jmp	.L472
.L564:
	call	GENERAL_NAME_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L486
	jmp	.L482
.L556:
	call	GENERAL_NAME_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L485
	jmp	.L482
	.cfi_endproc
.LFE1306:
	.size	v2i_GENERAL_NAME_ex, .-v2i_GENERAL_NAME_ex
	.globl	v3_alt
	.section	.data.rel.ro,"aw"
	.align 32
	.type	v3_alt, @object
	.size	v3_alt, 312
v3_alt:
	.long	85
	.long	0
	.quad	GENERAL_NAMES_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	i2v_GENERAL_NAMES
	.quad	v2i_subject_alt
	.quad	0
	.quad	0
	.quad	0
	.long	86
	.long	0
	.quad	GENERAL_NAMES_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	i2v_GENERAL_NAMES
	.quad	v2i_issuer_alt
	.quad	0
	.quad	0
	.quad	0
	.long	771
	.long	0
	.quad	GENERAL_NAMES_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	i2v_GENERAL_NAMES
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
