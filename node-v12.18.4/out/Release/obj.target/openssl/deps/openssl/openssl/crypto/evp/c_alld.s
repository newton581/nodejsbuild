	.file	"c_alld.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"MD5"
.LC1:
	.string	"ssl3-md5"
.LC2:
	.string	"SHA1"
.LC3:
	.string	"ssl3-sha1"
.LC4:
	.string	"RSA-SHA1"
.LC5:
	.string	"RSA-SHA1-2"
.LC6:
	.string	"RIPEMD160"
.LC7:
	.string	"ripemd"
.LC8:
	.string	"rmd160"
	.text
	.p2align 4
	.globl	openssl_add_all_digests_int
	.type	openssl_add_all_digests_int, @function
openssl_add_all_digests_int:
.LFB829:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	EVP_md4@PLT
	movq	%rax, %rdi
	call	EVP_add_digest@PLT
	call	EVP_md5@PLT
	movq	%rax, %rdi
	call	EVP_add_digest@PLT
	leaq	.LC0(%rip), %rdx
	movl	$32769, %esi
	leaq	.LC1(%rip), %rdi
	call	OBJ_NAME_add@PLT
	call	EVP_md5_sha1@PLT
	movq	%rax, %rdi
	call	EVP_add_digest@PLT
	call	EVP_sha1@PLT
	movq	%rax, %rdi
	call	EVP_add_digest@PLT
	leaq	.LC2(%rip), %rdx
	movl	$32769, %esi
	leaq	.LC3(%rip), %rdi
	call	OBJ_NAME_add@PLT
	leaq	.LC4(%rip), %rdx
	movl	$32769, %esi
	leaq	.LC5(%rip), %rdi
	call	OBJ_NAME_add@PLT
	call	EVP_mdc2@PLT
	movq	%rax, %rdi
	call	EVP_add_digest@PLT
	call	EVP_ripemd160@PLT
	movq	%rax, %rdi
	call	EVP_add_digest@PLT
	leaq	.LC6(%rip), %rdx
	movl	$32769, %esi
	leaq	.LC7(%rip), %rdi
	call	OBJ_NAME_add@PLT
	leaq	.LC6(%rip), %rdx
	movl	$32769, %esi
	leaq	.LC8(%rip), %rdi
	call	OBJ_NAME_add@PLT
	call	EVP_sha224@PLT
	movq	%rax, %rdi
	call	EVP_add_digest@PLT
	call	EVP_sha256@PLT
	movq	%rax, %rdi
	call	EVP_add_digest@PLT
	call	EVP_sha384@PLT
	movq	%rax, %rdi
	call	EVP_add_digest@PLT
	call	EVP_sha512@PLT
	movq	%rax, %rdi
	call	EVP_add_digest@PLT
	call	EVP_sha512_224@PLT
	movq	%rax, %rdi
	call	EVP_add_digest@PLT
	call	EVP_sha512_256@PLT
	movq	%rax, %rdi
	call	EVP_add_digest@PLT
	call	EVP_whirlpool@PLT
	movq	%rax, %rdi
	call	EVP_add_digest@PLT
	call	EVP_sm3@PLT
	movq	%rax, %rdi
	call	EVP_add_digest@PLT
	call	EVP_blake2b512@PLT
	movq	%rax, %rdi
	call	EVP_add_digest@PLT
	call	EVP_blake2s256@PLT
	movq	%rax, %rdi
	call	EVP_add_digest@PLT
	call	EVP_sha3_224@PLT
	movq	%rax, %rdi
	call	EVP_add_digest@PLT
	call	EVP_sha3_256@PLT
	movq	%rax, %rdi
	call	EVP_add_digest@PLT
	call	EVP_sha3_384@PLT
	movq	%rax, %rdi
	call	EVP_add_digest@PLT
	call	EVP_sha3_512@PLT
	movq	%rax, %rdi
	call	EVP_add_digest@PLT
	call	EVP_shake128@PLT
	movq	%rax, %rdi
	call	EVP_add_digest@PLT
	call	EVP_shake256@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	%rax, %rdi
	jmp	EVP_add_digest@PLT
	.cfi_endproc
.LFE829:
	.size	openssl_add_all_digests_int, .-openssl_add_all_digests_int
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
