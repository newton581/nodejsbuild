	.file	"cms_sd.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/cms/cms_sd.c"
	.text
	.p2align 4
	.globl	CMS_SignedData_init
	.type	CMS_SignedData_init, @function
CMS_SignedData_init:
.LFB1492:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpq	$0, 8(%rdi)
	movq	%rdi, %rbx
	je	.L9
	movq	(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$22, %eax
	jne	.L10
	movq	8(%rbx), %rax
.L5:
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
.L1:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	leaq	CMS_SignedData_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	je	.L11
	movl	$1, (%rax)
	movq	16(%rax), %r12
	movl	$21, %edi
	call	OBJ_nid2obj@PLT
	movq	(%rbx), %rdi
	movq	%rax, (%r12)
	movq	8(%rbx), %rax
	movq	16(%rax), %rax
	movl	$1, 16(%rax)
	call	ASN1_OBJECT_free@PLT
	movl	$22, %edi
	call	OBJ_nid2obj@PLT
	movq	%rax, (%rbx)
	movq	8(%rbx), %rax
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L10:
	movl	$26, %r8d
	movl	$108, %edx
	movl	$133, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movl	$37, %r8d
	movl	$65, %edx
	movl	$149, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L1
	.cfi_endproc
.LFE1492:
	.size	CMS_SignedData_init, .-CMS_SignedData_init
	.p2align 4
	.globl	cms_set1_SignerIdentifier
	.type	cms_set1_SignerIdentifier, @function
cms_set1_SignerIdentifier:
.LFB1496:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%edx, %ebx
	testl	%edx, %edx
	je	.L13
	cmpl	$1, %edx
	je	.L14
	movl	$186, %r8d
	movl	$150, %edx
	movl	$146, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L24:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	leaq	8(%rdi), %rdi
	call	cms_set1_ias@PLT
	testl	%eax, %eax
	je	.L24
.L16:
	movl	%ebx, (%r12)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	leaq	8(%rdi), %rdi
	call	cms_set1_keyid@PLT
	testl	%eax, %eax
	jne	.L16
	jmp	.L24
	.cfi_endproc
.LFE1496:
	.size	cms_set1_SignerIdentifier, .-cms_set1_SignerIdentifier
	.p2align 4
	.globl	cms_SignerIdentifier_get0_signer_id
	.type	cms_SignerIdentifier_get0_signer_id, @function
cms_SignerIdentifier_get0_signer_id:
.LFB1497:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	testl	%eax, %eax
	jne	.L26
	testq	%rdx, %rdx
	je	.L27
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L27:
	testq	%rcx, %rcx
	je	.L38
	movq	8(%rdi), %rax
	movq	8(%rax), %rax
	movq	%rax, (%rcx)
.L38:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	cmpl	$1, %eax
	jne	.L31
	testq	%rsi, %rsi
	je	.L38
	movq	8(%rdi), %rdx
	movq	%rdx, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1497:
	.size	cms_SignerIdentifier_get0_signer_id, .-cms_SignerIdentifier_get0_signer_id
	.p2align 4
	.globl	cms_SignerIdentifier_cert_cmp
	.type	cms_SignerIdentifier_cert_cmp, @function
cms_SignerIdentifier_cert_cmp:
.LFB1498:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	testl	%eax, %eax
	je	.L42
	cmpl	$1, %eax
	jne	.L41
	movq	8(%rdi), %rdi
	jmp	cms_keyid_cert_cmp@PLT
	.p2align 4,,10
	.p2align 3
.L42:
	movq	8(%rdi), %rdi
	jmp	cms_ias_cert_cmp@PLT
	.p2align 4,,10
	.p2align 3
.L41:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE1498:
	.size	cms_SignerIdentifier_cert_cmp, .-cms_SignerIdentifier_cert_cmp
	.p2align 4
	.globl	CMS_SignerInfo_get0_pkey_ctx
	.type	CMS_SignerInfo_get0_pkey_ctx, @function
CMS_SignerInfo_get0_pkey_ctx:
.LFB1502:
	.cfi_startproc
	endbr64
	movq	80(%rdi), %rax
	ret
	.cfi_endproc
.LFE1502:
	.size	CMS_SignerInfo_get0_pkey_ctx, .-CMS_SignerInfo_get0_pkey_ctx
	.p2align 4
	.globl	CMS_SignerInfo_get0_md_ctx
	.type	CMS_SignerInfo_get0_md_ctx, @function
CMS_SignerInfo_get0_md_ctx:
.LFB1503:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rax
	ret
	.cfi_endproc
.LFE1503:
	.size	CMS_SignerInfo_get0_md_ctx, .-CMS_SignerInfo_get0_md_ctx
	.p2align 4
	.globl	CMS_get0_SignerInfos
	.type	CMS_get0_SignerInfos, @function
CMS_get0_SignerInfos:
.LFB1504:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$22, %eax
	jne	.L52
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L45
	movq	40(%rax), %rax
.L45:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	movl	$26, %r8d
	movl	$108, %edx
	movl	$133, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1504:
	.size	CMS_get0_SignerInfos, .-CMS_get0_SignerInfos
	.p2align 4
	.globl	CMS_get0_signers
	.type	CMS_get0_signers, @function
CMS_get0_signers:
.LFB1505:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$22, %eax
	jne	.L71
	movq	8(%rbx), %r14
	testq	%r14, %r14
	je	.L55
	movq	40(%r14), %r14
.L55:
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L58:
	movq	%r13, %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L72
.L57:
	addl	$1, %r12d
.L56:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L53
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movq	56(%rax), %rsi
	movq	%rax, %rbx
	testq	%rsi, %rsi
	je	.L57
	testq	%r13, %r13
	jne	.L58
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L53
	movq	56(%rbx), %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L57
.L72:
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	OPENSSL_sk_free@PLT
.L53:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	movl	$26, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$108, %edx
	xorl	%r14d, %r14d
	movl	$133, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L55
	.cfi_endproc
.LFE1505:
	.size	CMS_get0_signers, .-CMS_get0_signers
	.p2align 4
	.globl	CMS_SignerInfo_set1_signer_cert
	.type	CMS_SignerInfo_set1_signer_cert, @function
CMS_SignerInfo_set1_signer_cert:
.LFB1506:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	je	.L74
	movq	%rsi, %rdi
	call	X509_up_ref@PLT
	movq	64(%rbx), %rdi
	call	EVP_PKEY_free@PLT
	movq	%r12, %rdi
	call	X509_get_pubkey@PLT
	movq	%rax, 64(%rbx)
.L74:
	movq	56(%rbx), %rdi
	call	X509_free@PLT
	movq	%r12, 56(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1506:
	.size	CMS_SignerInfo_set1_signer_cert, .-CMS_SignerInfo_set1_signer_cert
	.p2align 4
	.globl	CMS_SignerInfo_get0_signer_id
	.type	CMS_SignerInfo_get0_signer_id, @function
CMS_SignerInfo_get0_signer_id:
.LFB1507:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movl	(%rdi), %eax
	testl	%eax, %eax
	jne	.L80
	testq	%rdx, %rdx
	je	.L81
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L81:
	testq	%rcx, %rcx
	je	.L92
	movq	8(%rdi), %rax
	movq	8(%rax), %rax
	movq	%rax, (%rcx)
.L92:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	cmpl	$1, %eax
	jne	.L85
	testq	%rsi, %rsi
	je	.L92
	movq	8(%rdi), %rdx
	movq	%rdx, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1507:
	.size	CMS_SignerInfo_get0_signer_id, .-CMS_SignerInfo_get0_signer_id
	.p2align 4
	.globl	CMS_SignerInfo_cert_cmp
	.type	CMS_SignerInfo_cert_cmp, @function
CMS_SignerInfo_cert_cmp:
.LFB1508:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	je	.L96
	cmpl	$1, %edx
	jne	.L95
	movq	8(%rax), %rdi
	jmp	cms_keyid_cert_cmp@PLT
	.p2align 4,,10
	.p2align 3
.L96:
	movq	8(%rax), %rdi
	jmp	cms_ias_cert_cmp@PLT
	.p2align 4,,10
	.p2align 3
.L95:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE1508:
	.size	CMS_SignerInfo_cert_cmp, .-CMS_SignerInfo_cert_cmp
	.p2align 4
	.globl	CMS_set1_signers_certs
	.type	CMS_set1_signers_certs, @function
CMS_set1_signers_certs:
.LFB1509:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$22, %eax
	jne	.L125
	movq	8(%rbx), %r15
	testq	%r15, %r15
	je	.L116
	movq	24(%r15), %rax
	andl	$16, %r12d
	movl	$0, -56(%rbp)
	movl	%r12d, -76(%rbp)
	movq	%rax, -64(%rbp)
	movl	$0, -52(%rbp)
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L101:
	addl	$1, -52(%rbp)
.L100:
	movq	40(%r15), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, -52(%rbp)
	jge	.L97
	movq	40(%r15), %rdi
	movl	-52(%rbp), %esi
	xorl	%ebx, %ebx
	call	OPENSSL_sk_value@PLT
	cmpq	$0, 56(%rax)
	movq	%rax, %r12
	jne	.L101
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L108
	.p2align 4,,10
	.p2align 3
.L127:
	movq	56(%r12), %r14
.L107:
	testq	%r14, %r14
	jne	.L101
	movl	-76(%rbp), %ecx
	xorl	%ebx, %ebx
	testl	%ecx, %ecx
	jne	.L101
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L103:
	cmpl	$1, %ecx
	jne	.L105
	movq	8(%rax), %rdi
	movq	%r14, %rsi
	call	cms_keyid_cert_cmp@PLT
	testl	%eax, %eax
	je	.L126
.L105:
	movq	%r13, %rdi
	addl	$1, %ebx
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L127
.L108:
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r14
	movq	8(%r12), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jne	.L103
	movq	8(%rax), %rdi
	movq	%r14, %rsi
	call	cms_ias_cert_cmp@PLT
	testl	%eax, %eax
	jne	.L105
.L126:
	testq	%r14, %r14
	je	.L106
	movq	%r14, %rdi
	call	X509_up_ref@PLT
	movq	64(%r12), %rdi
	call	EVP_PKEY_free@PLT
	movq	%r14, %rdi
	call	X509_get_pubkey@PLT
	movq	%rax, 64(%r12)
.L106:
	movq	56(%r12), %rdi
	call	X509_free@PLT
	addl	$1, -56(%rbp)
	movq	%r14, 56(%r12)
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L111:
	cmpl	$1, %edx
	jne	.L110
	movq	8(%rax), %rdi
	movq	%rcx, %rsi
	movq	%rcx, -72(%rbp)
	call	cms_keyid_cert_cmp@PLT
	movq	-72(%rbp), %rcx
	testl	%eax, %eax
	je	.L128
.L110:
	addl	$1, %ebx
.L109:
	movq	-64(%rbp), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L101
	movq	-64(%rbp), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movl	(%rax), %edx
	testl	%edx, %edx
	jne	.L110
	movq	8(%rax), %rcx
	movq	8(%r12), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jne	.L111
	movq	8(%rax), %rdi
	movq	%rcx, %rsi
	movq	%rcx, -72(%rbp)
	call	cms_ias_cert_cmp@PLT
	movq	-72(%rbp), %rcx
	testl	%eax, %eax
	jne	.L110
.L128:
	testq	%rcx, %rcx
	je	.L113
	movq	%rcx, %rdi
	movq	%rcx, -72(%rbp)
	call	X509_up_ref@PLT
	movq	64(%r12), %rdi
	call	EVP_PKEY_free@PLT
	movq	-72(%rbp), %rcx
	movq	%rcx, %rdi
	call	X509_get_pubkey@PLT
	movq	-72(%rbp), %rcx
	movq	%rax, 64(%r12)
.L113:
	movq	56(%r12), %rdi
	movq	%rcx, -72(%rbp)
	call	X509_free@PLT
	movq	-72(%rbp), %rcx
	addl	$1, -56(%rbp)
	movq	%rcx, 56(%r12)
	jmp	.L101
.L116:
	movl	$-1, -56(%rbp)
.L97:
	movl	-56(%rbp), %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L125:
	.cfi_restore_state
	movl	$26, %r8d
	movl	$108, %edx
	movl	$133, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, -56(%rbp)
	jmp	.L97
	.cfi_endproc
.LFE1509:
	.size	CMS_set1_signers_certs, .-CMS_set1_signers_certs
	.p2align 4
	.globl	CMS_SignerInfo_get0_algs
	.type	CMS_SignerInfo_get0_algs, @function
CMS_SignerInfo_get0_algs:
.LFB1510:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L130
	movq	64(%rdi), %rax
	movq	%rax, (%rsi)
.L130:
	testq	%rdx, %rdx
	je	.L131
	movq	56(%rdi), %rax
	movq	%rax, (%rdx)
.L131:
	testq	%rcx, %rcx
	je	.L132
	movq	16(%rdi), %rax
	movq	%rax, (%rcx)
.L132:
	testq	%r8, %r8
	je	.L129
	movq	32(%rdi), %rax
	movq	%rax, (%r8)
.L129:
	ret
	.cfi_endproc
.LFE1510:
	.size	CMS_SignerInfo_get0_algs, .-CMS_SignerInfo_get0_algs
	.p2align 4
	.globl	CMS_SignerInfo_get0_signature
	.type	CMS_SignerInfo_get0_signature, @function
CMS_SignerInfo_get0_signature:
.LFB1511:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE1511:
	.size	CMS_SignerInfo_get0_signature, .-CMS_SignerInfo_get0_signature
	.p2align 4
	.globl	CMS_SignerInfo_sign
	.type	CMS_SignerInfo_sign, @function
CMS_SignerInfo_sign:
.LFB1514:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	72(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	movq	(%rax), %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	movq	%rax, %r12
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L147
	movl	$-1, %edx
	movl	$52, %esi
	movq	%rbx, %rdi
	call	CMS_signed_get_attr_by_NID@PLT
	testl	%eax, %eax
	js	.L174
	movq	%rbx, %rdi
	call	CMS_si_check_attributes@PLT
	testl	%eax, %eax
	je	.L154
.L179:
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L155
	movq	%rdi, -64(%rbp)
.L156:
	xorl	%r8d, %r8d
	movq	%rbx, %r9
	movl	$11, %ecx
	movl	$8, %edx
	movl	$-1, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	movl	$688, %r8d
	testl	%eax, %eax
	jle	.L173
	movq	24(%rbx), %rdi
	leaq	-56(%rbp), %rsi
	leaq	CMS_Attributes_Sign_it(%rip), %rdx
	call	ASN1_item_i2d@PLT
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L153
	movq	%rdi, %rsi
	movslq	%eax, %rdx
	movq	%r13, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jle	.L154
	leaq	-48(%rbp), %r12
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%r12, %rdx
	call	EVP_DigestSignFinal@PLT
	testl	%eax, %eax
	jle	.L154
	movq	-56(%rbp), %rdi
	movl	$700, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-48(%rbp), %rdi
	movl	$701, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, -56(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L153
	movq	%r12, %rdx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	EVP_DigestSignFinal@PLT
	testl	%eax, %eax
	jle	.L154
	movq	-64(%rbp), %rdi
	movq	%rbx, %r9
	movl	$1, %r8d
	movl	$11, %ecx
	movl	$8, %edx
	movl	$-1, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L175
	movq	%r13, %rdi
	call	EVP_MD_CTX_reset@PLT
	movq	40(%rbx), %rdi
	movl	-48(%rbp), %edx
	movq	-56(%rbp), %rsi
	call	ASN1_STRING_set0@PLT
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L147:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L176
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L174:
	.cfi_restore_state
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	X509_gmtime_adj@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L177
	movl	4(%rax), %edx
	movq	%rbx, %rdi
	movl	$-1, %r8d
	movq	%rax, %rcx
	movl	$52, %esi
	call	CMS_signed_add1_attr_by_NID@PLT
	movq	%r14, %rdi
	testl	%eax, %eax
	jle	.L178
	call	ASN1_TIME_free@PLT
	movq	%rbx, %rdi
	call	CMS_si_check_attributes@PLT
	testl	%eax, %eax
	jne	.L179
	.p2align 4,,10
	.p2align 3
.L154:
	movq	-56(%rbp), %rdi
.L153:
	movl	$720, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r13, %rdi
	call	EVP_MD_CTX_reset@PLT
	xorl	%eax, %eax
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L177:
	xorl	%edi, %edi
	call	ASN1_TIME_free@PLT
.L151:
	movl	$46, %edi
	movl	$419, %r8d
	movl	$65, %edx
	movl	$103, %esi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-56(%rbp), %rdi
	jmp	.L153
.L175:
	movl	$709, %r8d
	.p2align 4,,10
	.p2align 3
.L173:
	movl	$46, %edi
	movl	$110, %edx
	movl	$151, %esi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-56(%rbp), %rdi
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L155:
	movq	%r13, %rdi
	call	EVP_MD_CTX_reset@PLT
	movq	64(%rbx), %r8
	xorl	%ecx, %ecx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	EVP_DigestSignInit@PLT
	testl	%eax, %eax
	jle	.L154
	movq	-64(%rbp), %rdi
	movq	%rdi, 80(%rbx)
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L178:
	call	ASN1_TIME_free@PLT
	jmp	.L151
.L176:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1514:
	.size	CMS_SignerInfo_sign, .-CMS_SignerInfo_sign
	.p2align 4
	.globl	cms_SignedData_final
	.type	cms_SignedData_final, @function
cms_SignedData_final:
.LFB1513:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -168(%rbp)
	movq	(%rdi), %rdi
	movq	%rsi, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	OBJ_obj2nid@PLT
	cmpl	$22, %eax
	jne	.L226
	movq	-168(%rbp), %rax
	movq	8(%rax), %r12
	testq	%r12, %r12
	je	.L182
	movq	40(%r12), %r12
.L182:
	leaq	-136(%rbp), %rax
	movq	%r12, %rdi
	xorl	%ebx, %ebx
	movq	%rax, -160(%rbp)
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L227
	.p2align 4,,10
	.p2align 3
.L201:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r13
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L228
	cmpq	$0, 64(%r13)
	je	.L229
	movq	16(%r13), %rdx
	movq	-152(%rbp), %rsi
	movq	%rax, %rdi
	call	cms_DigestAlgorithm_find_ctx@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L187
	cmpq	$0, 80(%r13)
	je	.L188
	movq	64(%r13), %rdi
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L188
	movq	176(%rax), %rax
	testq	%rax, %rax
	je	.L188
	xorl	%edx, %edx
	movq	%r13, %rcx
	movl	$5, %esi
	call	*%rax
	cmpl	$-2, %eax
	je	.L230
	testl	%eax, %eax
	jle	.L231
.L188:
	movq	%r13, %rdi
	call	CMS_signed_get_attr_count@PLT
	testl	%eax, %eax
	jns	.L232
	movq	80(%r13), %r14
	testq	%r14, %r14
	je	.L194
	leaq	-128(%rbp), %rcx
	leaq	-140(%rbp), %rdx
	movq	%r15, %rdi
	movq	%rcx, %rsi
	movq	%rcx, -176(%rbp)
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	je	.L195
	movq	64(%r13), %rdi
	call	EVP_PKEY_size@PLT
	movl	$606, %edx
	leaq	.LC0(%rip), %rsi
	movslq	%eax, %rdi
	movq	%rdi, -136(%rbp)
	call	CRYPTO_malloc@PLT
	movq	-176(%rbp), %rcx
	testq	%rax, %rax
	je	.L233
	movl	-140(%rbp), %r8d
	movq	-160(%rbp), %rdx
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	%rax, -176(%rbp)
	call	EVP_PKEY_sign@PLT
	movq	-176(%rbp), %r11
	testl	%eax, %eax
	jle	.L234
	movq	40(%r13), %rdi
	movl	-136(%rbp), %edx
	movq	%r11, %rsi
	call	ASN1_STRING_set0@PLT
	movq	%r15, %rdi
	call	EVP_MD_CTX_free@PLT
	movq	%r14, %rdi
	call	EVP_PKEY_CTX_free@PLT
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L187:
	movq	%r15, %rdi
	call	EVP_MD_CTX_free@PLT
	xorl	%edi, %edi
	call	EVP_PKEY_CTX_free@PLT
.L180:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L235
	addq	$136, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L229:
	.cfi_restore_state
	movl	$569, %r8d
	movl	$133, %edx
	movl	$150, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L190:
	movq	%r15, %rdi
	xorl	%r14d, %r14d
	call	EVP_MD_CTX_free@PLT
	xorl	%edi, %edi
	call	EVP_PKEY_CTX_free@PLT
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L232:
	leaq	-128(%rbp), %r14
	movq	-160(%rbp), %rdx
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	je	.L190
	movq	%r14, %rcx
	movl	$4, %edx
	movl	$51, %esi
	movq	%r13, %rdi
	movl	-136(%rbp), %r8d
	call	CMS_signed_add1_attr_by_NID@PLT
	testl	%eax, %eax
	je	.L190
	movq	-168(%rbp), %rax
	movl	$-1, %r8d
	movl	$6, %edx
	movq	%r13, %rdi
	movl	$50, %esi
	movq	8(%rax), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rcx
	call	CMS_signed_add1_attr_by_NID@PLT
	testl	%eax, %eax
	jle	.L190
	movq	%r13, %rdi
	call	CMS_SignerInfo_sign
	testl	%eax, %eax
	jne	.L225
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L233:
	movl	$608, %r8d
	movl	$65, %edx
	movl	$150, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L195:
	movq	%r15, %rdi
	call	EVP_MD_CTX_free@PLT
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	EVP_PKEY_CTX_free@PLT
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L228:
	movl	$564, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r14d, %r14d
	movl	$150, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L194:
	movq	64(%r13), %rdi
	call	EVP_PKEY_size@PLT
	movl	$619, %edx
	leaq	.LC0(%rip), %rsi
	movslq	%eax, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L236
	movq	64(%r13), %rcx
	movq	-160(%rbp), %rdx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	EVP_SignFinal@PLT
	testl	%eax, %eax
	je	.L237
	movq	40(%r13), %rdi
	movl	-136(%rbp), %edx
	movq	%r14, %rsi
	call	ASN1_STRING_set0@PLT
.L225:
	movq	%r15, %rdi
	call	EVP_MD_CTX_free@PLT
	xorl	%edi, %edi
	call	EVP_PKEY_CTX_free@PLT
.L193:
	movq	%r12, %rdi
	addl	$1, %ebx
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L201
.L227:
	movq	-168(%rbp), %rax
	movl	$1, %r14d
	movq	8(%rax), %rax
	movq	16(%rax), %rax
	movl	$0, 16(%rax)
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L231:
	movl	$235, %r8d
	movl	$111, %edx
	movl	$170, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L230:
	movl	$231, %r8d
	movl	$125, %edx
	movl	$170, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L234:
	movl	$612, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r11, %rdi
	call	CRYPTO_free@PLT
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L236:
	movl	$621, %r8d
	movl	$65, %edx
	movl	$150, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L226:
	movl	$26, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$108, %edx
	xorl	%r12d, %r12d
	movl	$133, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L237:
	movl	$625, %r8d
	movl	$139, %edx
	movl	$150, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$626, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	jmp	.L190
.L235:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1513:
	.size	cms_SignedData_final, .-cms_SignedData_final
	.p2align 4
	.globl	CMS_SignerInfo_verify
	.type	CMS_SignerInfo_verify, @function
CMS_SignerInfo_verify:
.LFB1515:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 64(%rdi)
	movq	$0, -48(%rbp)
	je	.L265
	movq	%rdi, %rbx
	call	CMS_si_check_attributes@PLT
	testl	%eax, %eax
	je	.L264
	movq	16(%rbx), %rax
	movq	(%rax), %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L264
	movq	72(%rbx), %r12
	testq	%r12, %r12
	je	.L266
.L243:
	movq	64(%rbx), %r8
	xorl	%ecx, %ecx
	leaq	80(%rbx), %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	EVP_DigestVerifyInit@PLT
	testl	%eax, %eax
	jle	.L263
	movq	64(%rbx), %rdi
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L246
	movq	176(%rax), %rax
	testq	%rax, %rax
	je	.L246
	movq	%rbx, %rcx
	movl	$1, %edx
	movl	$5, %esi
	call	*%rax
	cmpl	$-2, %eax
	je	.L267
	testl	%eax, %eax
	jle	.L268
.L246:
	movq	24(%rbx), %rdi
	leaq	-48(%rbp), %rsi
	leaq	CMS_Attributes_Verify_it(%rip), %rdx
	call	ASN1_item_i2d@PLT
	movq	-48(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L263
	movslq	%eax, %rdx
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	movq	-48(%rbp), %rdi
	movl	$759, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, %r13d
	call	CRYPTO_free@PLT
	testl	%r13d, %r13d
	jle	.L263
	movq	40(%rbx), %rax
	movq	%r12, %rdi
	movslq	(%rax), %rdx
	movq	8(%rax), %rsi
	call	EVP_DigestVerifyFinal@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jg	.L245
	movl	$767, %r8d
	movl	$158, %edx
	movl	$152, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L267:
	movl	$231, %r8d
	movl	$125, %edx
	movl	$170, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L263:
	movl	$-1, %r13d
.L245:
	movq	%r12, %rdi
	call	EVP_MD_CTX_reset@PLT
.L238:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L269
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L266:
	.cfi_restore_state
	call	EVP_MD_CTX_new@PLT
	movq	%rax, 72(%rbx)
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L243
	movl	$744, %r8d
	movl	$65, %edx
	movl	$152, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L264:
	movl	$-1, %r13d
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L268:
	movl	$235, %r8d
	movl	$111, %edx
	movl	$170, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r13d
	call	ERR_put_error@PLT
	jmp	.L245
.L265:
	movl	$733, %r8d
	movl	$134, %edx
	movl	$152, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r13d
	call	ERR_put_error@PLT
	jmp	.L238
.L269:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1515:
	.size	CMS_SignerInfo_verify, .-CMS_SignerInfo_verify
	.p2align 4
	.globl	cms_SignedData_init_bio
	.type	cms_SignedData_init_bio, @function
cms_SignedData_init_bio:
.LFB1516:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$22, %eax
	jne	.L302
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L295
	movq	16(%rbx), %rax
	movl	16(%rax), %ecx
	testl	%ecx, %ecx
	jne	.L303
.L274:
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L305:
	movq	%r13, %rdi
	call	BIO_push@PLT
.L293:
	addl	$1, %r12d
.L291:
	movq	8(%rbx), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L270
	movq	8(%rbx), %rdi
	movl	%r12d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	call	cms_DigestAlgorithm_init_bio@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L304
	testq	%r13, %r13
	jne	.L305
	movq	%rax, %r13
	jmp	.L293
.L295:
	xorl	%r13d, %r13d
.L270:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L304:
	.cfi_restore_state
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	BIO_free_all@PLT
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L303:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L275:
	cmpl	$3, %eax
	je	.L306
	cmpl	$2, %eax
	je	.L307
.L276:
	addl	$1, %r12d
.L273:
	movq	24(%rbx), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L308
	movq	24(%rbx), %rdi
	movl	%r12d, %esi
	call	OPENSSL_sk_value@PLT
	movl	(%rax), %eax
	cmpl	$4, %eax
	jne	.L275
	cmpl	$4, (%rbx)
	jg	.L276
	movl	$5, (%rbx)
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L306:
	cmpl	$3, (%rbx)
	jg	.L276
	movl	$4, (%rbx)
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L307:
	cmpl	$2, (%rbx)
	jg	.L276
	movl	$3, (%rbx)
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L308:
	xorl	%r12d, %r12d
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L280:
	addl	$1, %r12d
.L279:
	movq	32(%rbx), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L309
	movq	32(%rbx), %rdi
	movl	%r12d, %esi
	call	OPENSSL_sk_value@PLT
	cmpl	$1, (%rax)
	jne	.L280
	cmpl	$4, (%rbx)
	jg	.L280
	movl	$5, (%rbx)
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L309:
	movq	16(%rbx), %rax
	movq	(%rax), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$21, %eax
	je	.L282
	cmpl	$2, (%rbx)
	jg	.L282
	movl	$3, (%rbx)
.L282:
	xorl	%r12d, %r12d
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L284:
	movl	(%rax), %edx
	testl	%edx, %edx
	jg	.L287
	movl	$1, (%rax)
.L287:
	addl	$1, %r12d
.L283:
	movq	40(%rbx), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L310
	movq	40(%rbx), %rdi
	movl	%r12d, %esi
	call	OPENSSL_sk_value@PLT
	movq	8(%rax), %rdx
	cmpl	$1, (%rdx)
	jne	.L284
	cmpl	$2, (%rax)
	jg	.L285
	movl	$3, (%rax)
.L285:
	cmpl	$2, (%rbx)
	jg	.L287
	movl	$3, (%rbx)
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L310:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L274
	movl	$1, (%rbx)
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L302:
	movl	$26, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$108, %edx
	xorl	%r13d, %r13d
	movl	$133, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1516:
	.size	cms_SignedData_init_bio, .-cms_SignedData_init_bio
	.p2align 4
	.globl	CMS_SignerInfo_verify_content
	.type	CMS_SignerInfo_verify_content, @function
CMS_SignerInfo_verify_content:
.LFB1517:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L341
	movq	%rbx, %rdi
	call	CMS_signed_get_attr_count@PLT
	testl	%eax, %eax
	js	.L323
	movl	$51, %edi
	call	OBJ_nid2obj@PLT
	movl	$4, %ecx
	movl	$-3, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	CMS_signed_get0_data_by_OBJ@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L342
.L314:
	movq	16(%rbx), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	cms_DigestAlgorithm_find_ctx@PLT
	testl	%eax, %eax
	je	.L338
	leaq	-128(%rbp), %r14
	leaq	-132(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	jle	.L343
	testq	%r12, %r12
	je	.L317
	movl	-132(%rbp), %edx
	cmpl	%edx, (%r12)
	jne	.L344
	movq	8(%r12), %rsi
	movq	%r14, %rdi
	xorl	%r12d, %r12d
	movl	$1, %r14d
	call	memcmp@PLT
	movl	$847, %r8d
	testl	%eax, %eax
	jne	.L340
.L313:
	movq	%r12, %rdi
	call	EVP_PKEY_CTX_free@PLT
	movq	%r13, %rdi
	call	EVP_MD_CTX_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L345
	addq	$104, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L323:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L317:
	movq	%r13, %rdi
	call	EVP_MD_CTX_md@PLT
	movq	64(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rax, %r15
	call	EVP_PKEY_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L338
	movq	%rax, %rdi
	call	EVP_PKEY_verify_init@PLT
	testl	%eax, %eax
	jle	.L326
	xorl	%r8d, %r8d
	movq	%r15, %r9
	movl	$1, %ecx
	movl	$248, %edx
	movl	$-1, %esi
	movq	%r12, %rdi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L326
	movq	64(%rbx), %rdi
	movq	%r12, 80(%rbx)
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L320
	movq	176(%rax), %rax
	testq	%rax, %rax
	je	.L320
	movq	%rbx, %rcx
	movl	$1, %edx
	movl	$5, %esi
	call	*%rax
	cmpl	$-2, %eax
	je	.L346
	testl	%eax, %eax
	jle	.L347
.L320:
	movq	40(%rbx), %rax
	movl	-132(%rbp), %r8d
	movq	%r14, %rcx
	movq	%r12, %rdi
	movslq	(%rax), %rdx
	movq	8(%rax), %rsi
	call	EVP_PKEY_verify@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	jg	.L313
	movl	$867, %r8d
	.p2align 4,,10
	.p2align 3
.L340:
	leaq	.LC0(%rip), %rcx
	movl	$158, %edx
	movl	$154, %esi
	xorl	%r14d, %r14d
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L313
.L344:
	movl	$841, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$120, %edx
.L339:
	movl	$154, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
.L338:
	movl	$-1, %r14d
	xorl	%r12d, %r12d
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L347:
	movl	$235, %r8d
	movl	$111, %edx
	movl	$170, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r14d
	call	ERR_put_error@PLT
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L346:
	movl	$231, %r8d
	movl	$125, %edx
	movl	$170, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r14d
	call	ERR_put_error@PLT
	jmp	.L313
.L326:
	movl	$-1, %r14d
	jmp	.L313
.L341:
	movl	$813, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$154, %esi
	movl	$46, %edi
	movl	$-1, %r14d
	call	ERR_put_error@PLT
	jmp	.L313
.L343:
	movl	$832, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$147, %edx
	jmp	.L339
.L342:
	movl	$822, %r8d
	movl	$114, %edx
	movl	$154, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r14d
	call	ERR_put_error@PLT
	jmp	.L313
.L345:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1517:
	.size	CMS_SignerInfo_verify_content, .-CMS_SignerInfo_verify_content
	.p2align 4
	.globl	CMS_add_smimecap
	.type	CMS_add_smimecap, @function
CMS_add_smimecap:
.LFB1518:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rsi, %rdi
	leaq	-32(%rbp), %rsi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$0, -32(%rbp)
	call	i2d_X509_ALGORS@PLT
	testl	%eax, %eax
	jle	.L348
	movq	-32(%rbp), %rcx
	movl	$16, %edx
	movq	%r12, %rdi
	movl	%eax, %r8d
	movl	$167, %esi
	call	CMS_signed_add1_attr_by_NID@PLT
	movq	-32(%rbp), %rdi
	movl	$889, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, %r13d
	call	CRYPTO_free@PLT
.L348:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L354
	addq	$16, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L354:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1518:
	.size	CMS_add_smimecap, .-CMS_add_smimecap
	.p2align 4
	.globl	CMS_add_simple_smimecap
	.type	CMS_add_simple_smimecap, @function
CMS_add_simple_smimecap:
.LFB1519:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	testl	%edx, %edx
	jle	.L376
	movl	%edx, %ebx
	call	ASN1_INTEGER_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L375
	movslq	%ebx, %rsi
	movq	%rax, %rdi
	call	ASN1_INTEGER_set@PLT
	testl	%eax, %eax
	je	.L375
	call	X509_ALGOR_new@PLT
	movl	$2, %ebx
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L377
.L358:
	movl	%r13d, %edi
	call	OBJ_nid2obj@PLT
	movq	%r14, %rdi
	movq	%r15, %rcx
	movl	%ebx, %edx
	movq	%rax, %rsi
	call	X509_ALGOR_set0@PLT
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L378
.L362:
	movq	%r14, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L363
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L377:
	.cfi_restore_state
	movq	%r15, %r14
.L357:
	movq	%r14, %rdi
	call	ASN1_INTEGER_free@PLT
.L375:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L376:
	.cfi_restore_state
	call	X509_ALGOR_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L357
	xorl	%r15d, %r15d
	movl	$-1, %ebx
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L378:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L362
.L363:
	movq	%r14, %rdi
	call	X509_ALGOR_free@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1519:
	.size	CMS_add_simple_smimecap, .-CMS_add_simple_smimecap
	.p2align 4
	.globl	CMS_add_standard_smimecap
	.type	CMS_add_standard_smimecap, @function
CMS_add_standard_smimecap:
.LFB1522:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	$427, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_cipherbyname@PLT
	testq	%rax, %rax
	jne	.L493
.L380:
	movl	$982, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	testq	%rax, %rax
	jne	.L494
.L385:
	movl	$983, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	testq	%rax, %rax
	jne	.L495
.L389:
	movl	$809, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	testq	%rax, %rax
	jne	.L496
.L392:
	movl	$813, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_cipherbyname@PLT
	testq	%rax, %rax
	jne	.L497
.L395:
	movl	$423, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_cipherbyname@PLT
	testq	%rax, %rax
	jne	.L498
.L398:
	movl	$419, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_cipherbyname@PLT
	testq	%rax, %rax
	jne	.L499
.L401:
	movl	$44, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_cipherbyname@PLT
	testq	%rax, %rax
	jne	.L500
.L404:
	movl	$37, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_cipherbyname@PLT
	testq	%rax, %rax
	jne	.L407
.L410:
	movl	$37, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_cipherbyname@PLT
	testq	%rax, %rax
	jne	.L501
.L409:
	movl	$31, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_cipherbyname@PLT
	testq	%rax, %rax
	jne	.L502
.L412:
	movl	$37, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_cipherbyname@PLT
	testq	%rax, %rax
	jne	.L414
	movl	$1, %eax
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L493:
	call	X509_ALGOR_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L386
	movl	$427, %edi
	call	OBJ_nid2obj@PLT
	movq	%r13, %rdi
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%rax, %rsi
	call	X509_ALGOR_set0@PLT
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L503
.L383:
	movq	%r13, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L380
.L406:
	movq	%r13, %rdi
	call	X509_ALGOR_free@PLT
	xorl	%eax, %eax
.L379:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L495:
	.cfi_restore_state
	call	X509_ALGOR_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L386
	movl	$983, %edi
	call	OBJ_nid2obj@PLT
	movq	%r13, %rdi
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%rax, %rsi
	call	X509_ALGOR_set0@PLT
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L504
.L390:
	movq	%r13, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L389
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L494:
	call	X509_ALGOR_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L386
	movl	$982, %edi
	call	OBJ_nid2obj@PLT
	movq	%r13, %rdi
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%rax, %rsi
	call	X509_ALGOR_set0@PLT
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L505
.L387:
	movq	%r13, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L385
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L386:
	xorl	%edi, %edi
	call	ASN1_INTEGER_free@PLT
	xorl	%eax, %eax
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L503:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L406
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L498:
	call	X509_ALGOR_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L386
	movl	$423, %edi
	call	OBJ_nid2obj@PLT
	movq	%r13, %rdi
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%rax, %rsi
	call	X509_ALGOR_set0@PLT
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L506
.L399:
	movq	%r13, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L398
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L496:
	call	X509_ALGOR_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L386
	movl	$809, %edi
	call	OBJ_nid2obj@PLT
	movq	%r13, %rdi
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%rax, %rsi
	call	X509_ALGOR_set0@PLT
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L507
.L393:
	movq	%r13, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L392
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L505:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L406
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L497:
	call	X509_ALGOR_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L386
	movl	$813, %edi
	call	OBJ_nid2obj@PLT
	movq	%r13, %rdi
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%rax, %rsi
	call	X509_ALGOR_set0@PLT
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L508
.L396:
	movq	%r13, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L395
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L504:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L406
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L507:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L406
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L499:
	call	X509_ALGOR_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L386
	movl	$419, %edi
	call	OBJ_nid2obj@PLT
	movq	%r13, %rdi
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%rax, %rsi
	call	X509_ALGOR_set0@PLT
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L509
.L402:
	movq	%r13, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L401
	jmp	.L406
.L508:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L406
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L500:
	call	X509_ALGOR_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L386
	movl	$44, %edi
	call	OBJ_nid2obj@PLT
	movq	%r13, %rdi
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%rax, %rsi
	call	X509_ALGOR_set0@PLT
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L510
.L405:
	movq	%r13, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L404
	jmp	.L406
.L506:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L406
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L407:
	movl	$128, %edx
	movl	$37, %esi
	movq	%r12, %rdi
	call	CMS_add_simple_smimecap
	testl	%eax, %eax
	jne	.L410
	xorl	%eax, %eax
	jmp	.L379
.L509:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L406
	jmp	.L402
.L501:
	movl	$64, %edx
	movl	$37, %esi
	movq	%r12, %rdi
	call	CMS_add_simple_smimecap
	testl	%eax, %eax
	jne	.L409
	xorl	%eax, %eax
	jmp	.L379
.L510:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L406
	jmp	.L405
.L502:
	orl	$-1, %edx
	movl	$31, %esi
	movq	%r12, %rdi
	call	CMS_add_simple_smimecap
	testl	%eax, %eax
	jne	.L412
	xorl	%eax, %eax
	jmp	.L379
.L414:
	movl	$40, %edx
	movl	$37, %esi
	movq	%r12, %rdi
	call	CMS_add_simple_smimecap
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L379
	.cfi_endproc
.LFE1522:
	.size	CMS_add_standard_smimecap, .-CMS_add_standard_smimecap
	.p2align 4
	.globl	CMS_add1_signer
	.type	CMS_add1_signer, @function
CMS_add1_signer:
.LFB1500:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -96(%rbp)
	movq	%rsi, %rdi
	movq	%rsi, -120(%rbp)
	movq	%rdx, %rsi
	movq	%rdx, -112(%rbp)
	movl	%r8d, -100(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	X509_check_private_key@PLT
	testl	%eax, %eax
	je	.L603
	movq	-96(%rbp), %rax
	cmpq	$0, 8(%rax)
	je	.L604
	movq	(%rax), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$22, %eax
	jne	.L605
	movq	-96(%rbp), %rax
	movq	8(%rax), %r14
.L517:
	testq	%r14, %r14
	je	.L561
	leaq	CMS_SignerInfo_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L520
	movq	-120(%rbp), %rbx
	movl	$-1, %edx
	movl	$-1, %esi
	movq	%rbx, %rdi
	call	X509_check_purpose@PLT
	movq	%rbx, %rdi
	call	X509_up_ref@PLT
	movq	-112(%rbp), %rdi
	call	EVP_PKEY_up_ref@PLT
	movq	%rbx, %xmm0
	movhps	-112(%rbp), %xmm0
	movups	%xmm0, 56(%r13)
	call	EVP_MD_CTX_new@PLT
	movq	$0, 80(%r13)
	movl	$272, %r8d
	movq	%rax, 72(%r13)
	testq	%rax, %rax
	je	.L602
	movq	8(%r13), %rbx
	leaq	8(%rbx), %rdi
	testl	$65536, -100(%rbp)
	je	.L522
	cmpl	$2, (%r14)
	movl	$3, 0(%r13)
	jle	.L606
.L523:
	movq	-120(%rbp), %rsi
	call	cms_set1_keyid@PLT
	testl	%eax, %eax
	je	.L516
	movl	$1, %eax
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L522:
	movl	$1, 0(%r13)
	movq	-120(%rbp), %rsi
	call	cms_set1_ias@PLT
	testl	%eax, %eax
	je	.L516
	xorl	%eax, %eax
.L524:
	movl	%eax, (%rbx)
	testq	%r15, %r15
	je	.L607
.L525:
	movq	16(%r13), %rdi
	movq	%r15, %rsi
	xorl	%ebx, %ebx
	call	X509_ALGOR_set_md@PLT
	leaq	-64(%rbp), %rax
	movq	%rax, -88(%rbp)
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L531:
	movq	8(%r14), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	-88(%rbp), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %rcx
	call	X509_ALGOR_get0@PLT
	movq	-64(%rbp), %rdi
	call	OBJ_obj2nid@PLT
	movq	%r15, %rdi
	movl	%eax, %r12d
	call	EVP_MD_type@PLT
	cmpl	%eax, %r12d
	je	.L530
	addl	$1, %ebx
.L529:
	movq	8(%r14), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L531
.L530:
	movq	8(%r14), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	je	.L608
.L532:
	movl	-100(%rbp), %eax
	andl	$262144, %eax
	movl	%eax, -88(%rbp)
	je	.L609
.L534:
	movl	-100(%rbp), %eax
	andl	$256, %eax
	movl	%eax, -104(%rbp)
	jne	.L547
	cmpq	$0, 24(%r13)
	je	.L539
.L542:
	testl	$512, -100(%rbp)
	je	.L610
.L541:
	testl	$32768, -100(%rbp)
	jne	.L611
.L547:
	testb	$2, -100(%rbp)
	je	.L612
.L538:
	movl	-88(%rbp), %edx
	testl	%edx, %edx
	je	.L556
	movl	-104(%rbp), %eax
	testl	%eax, %eax
	je	.L559
	movq	64(%r13), %rdi
	xorl	%esi, %esi
	call	EVP_PKEY_CTX_new@PLT
	movq	%rax, 80(%r13)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L516
	call	EVP_PKEY_sign_init@PLT
	testl	%eax, %eax
	jle	.L516
	movq	80(%r13), %rdi
	xorl	%r8d, %r8d
	movq	%r15, %r9
	movl	$1, %ecx
	movl	$248, %edx
	movl	$-1, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L516
	.p2align 4,,10
	.p2align 3
.L556:
	movq	40(%r14), %rdi
	testq	%rdi, %rdi
	je	.L613
.L558:
	movq	%r13, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L511
	.p2align 4,,10
	.p2align 3
.L520:
	movl	$388, %r8d
.L602:
	movl	$65, %edx
	movl	$102, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L516:
	movq	%r13, %rdi
	leaq	CMS_SignerInfo_it(%rip), %rsi
	xorl	%r13d, %r13d
	call	ASN1_item_free@PLT
.L511:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L614
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L604:
	.cfi_restore_state
	leaq	CMS_SignedData_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, %r13
	movq	-96(%rbp), %rax
	movq	%r13, 8(%rax)
	testq	%r13, %r13
	je	.L615
	movl	$1, 0(%r13)
	movq	16(%r13), %rbx
	movl	$21, %edi
	call	OBJ_nid2obj@PLT
	movq	%rax, (%rbx)
	movq	-96(%rbp), %rbx
	movq	8(%rbx), %rax
	movq	(%rbx), %rdi
	movq	16(%rax), %rax
	movl	$1, 16(%rax)
	call	ASN1_OBJECT_free@PLT
	movl	$22, %edi
	call	OBJ_nid2obj@PLT
	movq	8(%rbx), %r14
	movq	%rax, (%rbx)
	jmp	.L517
	.p2align 4,,10
	.p2align 3
.L606:
	movl	$3, (%r14)
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L605:
	movl	$26, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$108, %edx
	xorl	%r13d, %r13d
	movl	$133, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L603:
	movl	$250, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$136, %edx
	xorl	%r13d, %r13d
	movl	$102, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L609:
	movq	64(%r13), %rdi
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L534
	movq	176(%rax), %rax
	testq	%rax, %rax
	je	.L534
	xorl	%edx, %edx
	movq	%r13, %rcx
	movl	$5, %esi
	call	*%rax
	cmpl	$-2, %eax
	je	.L616
	testl	%eax, %eax
	jg	.L534
	movl	$235, %r8d
	movl	$111, %edx
	movl	$170, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L612:
	movq	-120(%rbp), %rsi
	movq	-96(%rbp), %rdi
	call	CMS_add1_cert@PLT
	testl	%eax, %eax
	jne	.L538
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L608:
	call	X509_ALGOR_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L520
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	X509_ALGOR_set_md@PLT
	movq	8(%r14), %rdi
	movq	%r12, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L532
	movq	%r12, %rdi
	call	X509_ALGOR_free@PLT
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L607:
	movq	-112(%rbp), %rdi
	leaq	-64(%rbp), %rsi
	call	EVP_PKEY_get_default_digest_nid@PLT
	testl	%eax, %eax
	jle	.L516
	movl	-64(%rbp), %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L525
	movl	$295, %r8d
	movl	$128, %edx
	movl	$102, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L559:
	movq	72(%r13), %rdi
	movq	-112(%rbp), %r8
	xorl	%ecx, %ecx
	leaq	80(%r13), %rsi
	movq	%r15, %rdx
	call	EVP_DigestSignInit@PLT
	testl	%eax, %eax
	jg	.L556
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L610:
	leaq	-72(%rbp), %rdi
	movq	$0, -72(%rbp)
	call	CMS_add_standard_smimecap
	testl	%eax, %eax
	jne	.L617
.L543:
	movq	X509_ALGOR_free@GOTPCREL(%rip), %rsi
	movq	-72(%rbp), %rdi
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L611:
	movq	-96(%rbp), %rax
	movq	(%rax), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$22, %eax
	jne	.L618
	movq	-96(%rbp), %rax
	movq	8(%rax), %rbx
	testq	%rbx, %rbx
	je	.L549
	movq	40(%rbx), %rbx
.L549:
	xorl	%r12d, %r12d
.L550:
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L619
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	cmpq	%rax, %r13
	je	.L551
	movq	%rax, %rdi
	movq	%rax, -128(%rbp)
	call	CMS_signed_get_attr_count@PLT
	testl	%eax, %eax
	js	.L551
	movq	-128(%rbp), %r8
	movq	16(%r13), %rax
	movq	16(%r8), %rdx
	movq	(%rax), %rdi
	movq	(%rdx), %rsi
	call	OBJ_cmp@PLT
	testl	%eax, %eax
	jne	.L551
	movl	$51, %edi
	call	OBJ_nid2obj@PLT
	movq	-128(%rbp), %r8
	movl	$4, %ecx
	movl	$-3, %edx
	movq	%rax, %rsi
	movq	%r8, %rdi
	call	CMS_signed_get0_data_by_OBJ@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L620
	movl	$-1, %r8d
	movl	$4, %edx
	movl	$51, %esi
	movq	%r13, %rdi
	call	CMS_signed_add1_attr_by_NID@PLT
	testl	%eax, %eax
	je	.L516
	movq	-96(%rbp), %rax
	movl	$-1, %r8d
	movl	$6, %edx
	movq	%r13, %rdi
	movl	$50, %esi
	movq	8(%rax), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rcx
	call	CMS_signed_add1_attr_by_NID@PLT
	testl	%eax, %eax
	jle	.L516
	testl	$278528, -100(%rbp)
	jne	.L547
	movq	%r13, %rdi
	call	CMS_SignerInfo_sign
	testl	%eax, %eax
	jne	.L547
	jmp	.L516
.L617:
	movq	-72(%rbp), %rdi
	leaq	-64(%rbp), %rsi
	movq	$0, -64(%rbp)
	call	i2d_X509_ALGORS@PLT
	movl	%eax, %r8d
	testl	%eax, %eax
	jle	.L543
	movq	-64(%rbp), %rcx
	movl	$16, %edx
	movl	$167, %esi
	movq	%r13, %rdi
	call	CMS_signed_add1_attr_by_NID@PLT
	movq	-64(%rbp), %rdi
	movl	$889, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, %ebx
	call	CRYPTO_free@PLT
	movq	X509_ALGOR_free@GOTPCREL(%rip), %rsi
	movq	-72(%rbp), %rdi
	call	OPENSSL_sk_pop_free@PLT
	testl	%ebx, %ebx
	jne	.L541
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L618:
	movl	$26, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$108, %edx
	xorl	%ebx, %ebx
	movl	$133, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L551:
	addl	$1, %r12d
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L613:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 40(%r14)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L520
	jmp	.L558
.L615:
	movl	$37, %r8d
	movl	$65, %edx
	movl	$149, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L516
.L539:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 24(%r13)
	testq	%rax, %rax
	jne	.L542
	jmp	.L520
.L561:
	xorl	%r13d, %r13d
	jmp	.L516
.L619:
	movl	$168, %r8d
	movl	$131, %edx
	movl	$108, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L516
.L616:
	movl	$231, %r8d
	movl	$125, %edx
	movl	$170, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L516
.L620:
	movl	$156, %r8d
	movl	$114, %edx
	movl	$108, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L516
.L614:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1500:
	.size	CMS_add1_signer, .-CMS_add1_signer
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
