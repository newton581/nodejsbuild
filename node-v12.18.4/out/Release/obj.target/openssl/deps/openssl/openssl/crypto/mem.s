	.file	"mem.c"
	.text
	.p2align 4
	.globl	CRYPTO_free
	.type	CRYPTO_free, @function
CRYPTO_free:
.LFB269:
	.cfi_startproc
	endbr64
	movq	free_impl(%rip), %rax
	testq	%rax, %rax
	je	.L2
	leaq	CRYPTO_free(%rip), %rcx
	cmpq	%rcx, %rax
	je	.L2
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L2:
	jmp	free@PLT
	.cfi_endproc
.LFE269:
	.size	CRYPTO_free, .-CRYPTO_free
	.p2align 4
	.globl	CRYPTO_malloc
	.type	CRYPTO_malloc, @function
CRYPTO_malloc:
.LFB265:
	.cfi_startproc
	endbr64
	movq	malloc_impl(%rip), %rax
	testq	%rax, %rax
	je	.L11
	leaq	CRYPTO_malloc(%rip), %rcx
	cmpq	%rcx, %rax
	je	.L11
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L11:
	testq	%rdi, %rdi
	je	.L12
	movl	allow_customize(%rip), %eax
	testl	%eax, %eax
	je	.L13
	movl	$0, allow_customize(%rip)
.L13:
	jmp	malloc@PLT
	.p2align 4,,10
	.p2align 3
.L12:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE265:
	.size	CRYPTO_malloc, .-CRYPTO_malloc
	.p2align 4
	.globl	CRYPTO_realloc
	.type	CRYPTO_realloc, @function
CRYPTO_realloc:
.LFB267:
	.cfi_startproc
	endbr64
	movq	realloc_impl(%rip), %rax
	movq	%rsi, %r8
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L21
	leaq	CRYPTO_realloc(%rip), %rdx
	cmpq	%rdx, %rax
	je	.L21
	movq	%r9, %rdx
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L21:
	testq	%rdi, %rdi
	je	.L52
	testq	%r8, %r8
	je	.L53
	movq	%r8, %rsi
	jmp	realloc@PLT
	.p2align 4,,10
	.p2align 3
.L53:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	free_impl(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rax, %rax
	je	.L27
	leaq	CRYPTO_free(%rip), %rdx
	cmpq	%rdx, %rax
	je	.L27
	movl	%ecx, %edx
	movq	%r9, %rsi
	call	*%rax
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L27:
	call	free@PLT
.L20:
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore 6
	movq	malloc_impl(%rip), %rax
	testq	%rax, %rax
	je	.L23
	leaq	CRYPTO_malloc(%rip), %rdx
	cmpq	%rdx, %rax
	je	.L23
	movl	%ecx, %edx
	movq	%r9, %rsi
	movq	%r8, %rdi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L23:
	testq	%r8, %r8
	je	.L49
	movl	allow_customize(%rip), %eax
	testl	%eax, %eax
	je	.L25
	movl	$0, allow_customize(%rip)
.L25:
	movq	%r8, %rdi
	jmp	malloc@PLT
.L49:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE267:
	.size	CRYPTO_realloc, .-CRYPTO_realloc
	.p2align 4
	.globl	CRYPTO_set_mem_functions
	.type	CRYPTO_set_mem_functions, @function
CRYPTO_set_mem_functions:
.LFB262:
	.cfi_startproc
	endbr64
	movl	allow_customize(%rip), %eax
	testl	%eax, %eax
	je	.L54
	testq	%rdi, %rdi
	je	.L56
	movq	%rdi, malloc_impl(%rip)
.L56:
	testq	%rsi, %rsi
	je	.L57
	movq	%rsi, realloc_impl(%rip)
.L57:
	movl	$1, %eax
	testq	%rdx, %rdx
	je	.L54
	movq	%rdx, free_impl(%rip)
.L54:
	ret
	.cfi_endproc
.LFE262:
	.size	CRYPTO_set_mem_functions, .-CRYPTO_set_mem_functions
	.p2align 4
	.globl	CRYPTO_set_mem_debug
	.type	CRYPTO_set_mem_debug, @function
CRYPTO_set_mem_debug:
.LFB263:
	.cfi_startproc
	endbr64
	movl	allow_customize(%rip), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	setne	%al
	ret
	.cfi_endproc
.LFE263:
	.size	CRYPTO_set_mem_debug, .-CRYPTO_set_mem_debug
	.p2align 4
	.globl	CRYPTO_get_mem_functions
	.type	CRYPTO_get_mem_functions, @function
CRYPTO_get_mem_functions:
.LFB264:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L71
	movq	malloc_impl(%rip), %rax
	movq	%rax, (%rdi)
.L71:
	testq	%rsi, %rsi
	je	.L72
	movq	realloc_impl(%rip), %rax
	movq	%rax, (%rsi)
.L72:
	testq	%rdx, %rdx
	je	.L70
	movq	free_impl(%rip), %rax
	movq	%rax, (%rdx)
.L70:
	ret
	.cfi_endproc
.LFE264:
	.size	CRYPTO_get_mem_functions, .-CRYPTO_get_mem_functions
	.p2align 4
	.globl	CRYPTO_zalloc
	.type	CRYPTO_zalloc, @function
CRYPTO_zalloc:
.LFB266:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	CRYPTO_malloc(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	malloc_impl(%rip), %rax
	cmpq	%rcx, %rax
	je	.L84
	testq	%rax, %rax
	je	.L84
	call	*%rax
	movq	%rax, %r8
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L84:
	testq	%r12, %r12
	je	.L88
	movl	allow_customize(%rip), %eax
	testl	%eax, %eax
	jne	.L99
.L87:
	movq	%r12, %rdi
	call	malloc@PLT
	movq	%rax, %r8
.L85:
	testq	%r8, %r8
	je	.L83
	movq	%r8, %rdi
	movq	%r12, %rdx
	xorl	%esi, %esi
	call	memset@PLT
	movq	%rax, %r8
.L83:
	addq	$8, %rsp
	movq	%r8, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore_state
	movl	$0, allow_customize(%rip)
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L88:
	addq	$8, %rsp
	xorl	%r8d, %r8d
	movq	%r8, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE266:
	.size	CRYPTO_zalloc, .-CRYPTO_zalloc
	.p2align 4
	.globl	CRYPTO_clear_realloc
	.type	CRYPTO_clear_realloc, @function
CRYPTO_clear_realloc:
.LFB268:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rdx, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%r13, %r13
	je	.L151
	movq	%rsi, %r12
	testq	%rdx, %rdx
	je	.L152
	cmpq	%rsi, %rdx
	jb	.L153
	movq	malloc_impl(%rip), %rax
	leaq	CRYPTO_malloc(%rip), %rdx
	cmpq	%rdx, %rax
	je	.L110
	testq	%rax, %rax
	je	.L110
	movl	%r8d, %edx
	movq	%rcx, %rsi
	call	*%rax
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.L150
.L155:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	memcpy@PLT
	testq	%r12, %r12
	jne	.L154
.L114:
	movq	free_impl(%rip), %rax
	testq	%rax, %rax
	je	.L115
	leaq	CRYPTO_free(%rip), %rdx
	cmpq	%rdx, %rax
	je	.L115
	movl	%r15d, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	*%rax
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L110:
	movl	allow_customize(%rip), %eax
	testl	%eax, %eax
	je	.L112
	movl	$0, allow_customize(%rip)
.L112:
	call	malloc@PLT
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.L155
.L150:
	xorl	%ebx, %ebx
.L100:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L153:
	.cfi_restore_state
	subq	%rdx, %rsi
	addq	%r13, %rdi
	movq	%r13, %rbx
	call	OPENSSL_cleanse@PLT
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L154:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	OPENSSL_cleanse@PLT
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L115:
	movq	%r13, %rdi
	call	free@PLT
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L151:
	movq	malloc_impl(%rip), %rax
	testq	%rax, %rax
	je	.L102
	leaq	CRYPTO_malloc(%rip), %rdx
	cmpq	%rdx, %rax
	je	.L102
	addq	$8, %rsp
	movl	%r8d, %edx
	movq	%rcx, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	testq	%rdi, %rdi
	je	.L150
	movl	allow_customize(%rip), %edx
	testl	%edx, %edx
	je	.L105
	movl	$0, allow_customize(%rip)
.L105:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	malloc@PLT
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore_state
	testq	%rsi, %rsi
	jne	.L156
.L107:
	movq	free_impl(%rip), %rax
	testq	%rax, %rax
	je	.L108
	leaq	CRYPTO_free(%rip), %rdx
	cmpq	%rdx, %rax
	je	.L108
	movl	%r15d, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	*%rax
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L156:
	movq	%r13, %rdi
	call	OPENSSL_cleanse@PLT
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L108:
	movq	%r13, %rdi
	xorl	%ebx, %ebx
	call	free@PLT
	jmp	.L100
	.cfi_endproc
.LFE268:
	.size	CRYPTO_clear_realloc, .-CRYPTO_clear_realloc
	.p2align 4
	.globl	CRYPTO_clear_free
	.type	CRYPTO_clear_free, @function
CRYPTO_clear_free:
.LFB270:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L157
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	subq	$16, %rsp
	testq	%rsi, %rsi
	jne	.L173
.L159:
	movq	free_impl(%rip), %rax
	testq	%rax, %rax
	je	.L160
	leaq	CRYPTO_free(%rip), %rdx
	cmpq	%rdx, %rax
	je	.L160
	addq	$16, %rsp
	movl	%r14d, %edx
	movq	%r13, %rsi
	popq	%r13
	.cfi_remember_state
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L160:
	.cfi_restore_state
	addq	$16, %rsp
	popq	%r13
	.cfi_remember_state
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L173:
	.cfi_restore_state
	movq	%rdi, -24(%rbp)
	call	OPENSSL_cleanse@PLT
	movq	-24(%rbp), %rdi
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L157:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 13
	.cfi_restore 14
	ret
	.cfi_endproc
.LFE270:
	.size	CRYPTO_clear_free, .-CRYPTO_clear_free
	.section	.data.rel.local,"aw"
	.align 8
	.type	free_impl, @object
	.size	free_impl, 8
free_impl:
	.quad	CRYPTO_free
	.align 8
	.type	realloc_impl, @object
	.size	realloc_impl, 8
realloc_impl:
	.quad	CRYPTO_realloc
	.align 8
	.type	malloc_impl, @object
	.size	malloc_impl, 8
malloc_impl:
	.quad	CRYPTO_malloc
	.data
	.align 4
	.type	allow_customize, @object
	.size	allow_customize, 4
allow_customize:
	.long	1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
