	.file	"o_time.c"
	.text
	.p2align 4
	.globl	OPENSSL_gmtime
	.type	OPENSSL_gmtime, @function
OPENSSL_gmtime:
.LFB151:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	call	gmtime_r@PLT
	testq	%rax, %rax
	movl	$0, %eax
	cmove	%rax, %r12
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE151:
	.size	OPENSSL_gmtime, .-OPENSSL_gmtime
	.p2align 4
	.globl	OPENSSL_gmtime_adj
	.type	OPENSSL_gmtime_adj, @function
OPENSSL_gmtime_adj:
.LFB152:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	movabsq	$1749024623285053783, %rdx
	movq	%r8, %rax
	imulq	%rdx
	movq	%r8, %rax
	sarq	$63, %rax
	sarq	$13, %rdx
	subq	%rax, %rdx
	imull	$86400, %edx, %eax
	leal	(%rsi,%rdx), %ecx
	imull	$3600, 8(%rdi), %edx
	subl	%eax, %r8d
	imull	$60, 4(%rdi), %eax
	addl	%eax, %edx
	addl	(%rdi), %edx
	addl	%edx, %r8d
	cmpl	$86399, %r8d
	jle	.L6
	addl	$1, %ecx
	subl	$86400, %r8d
.L7:
	movl	16(%rdi), %r10d
	movl	20(%rdi), %esi
	movslq	%ecx, %rcx
	leal	-13(%r10), %edx
	movslq	%edx, %rax
	sarl	$31, %edx
	imulq	$715827883, %rax, %rax
	sarq	$33, %rax
	movl	%eax, %r9d
	subl	%edx, %r9d
	subl	%eax, %edx
	leal	(%rdx,%rdx,2), %eax
	leal	-1(%r10,%rax,4), %edx
	imull	$367, %edx, %edx
	movslq	%edx, %rax
	sarl	$31, %edx
	imulq	$715827883, %rax, %rax
	sarq	$33, %rax
	subl	%edx, %eax
	movl	%eax, %edx
	leal	6700(%rsi,%r9), %eax
	leal	6800(%rsi,%r9), %esi
	imull	$1461, %eax, %r10d
	testl	%r10d, %r10d
	leal	3(%r10), %eax
	cmovns	%r10d, %eax
	sarl	$2, %eax
	addl	%edx, %eax
	movslq	%esi, %rdx
	sarl	$31, %esi
	imulq	$1374389535, %rdx, %rdx
	sarq	$37, %rdx
	subl	%esi, %edx
	leal	(%rdx,%rdx,2), %esi
	testl	%esi, %esi
	leal	3(%rsi), %edx
	cmovns	%esi, %edx
	xorl	%r9d, %r9d
	sarl	$2, %edx
	subl	%edx, %eax
	addl	12(%rdi), %eax
	subl	$32075, %eax
	cltq
	addq	%rax, %rcx
	js	.L12
	addq	$68569, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movabsq	$4137408090565272301, %rsi
	leaq	0(,%rcx,4), %rdx
	movq	%rdx, %rax
	mulq	%rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdx, %rsi
	shrq	$15, %rsi
	imulq	$146097, %rsi, %rdx
	leaq	6(%rdx), %rax
	addq	$3, %rdx
	cmovns	%rdx, %rax
	movabsq	$1654928120671552141, %rdx
	sarq	$2, %rax
	subq	%rax, %rcx
	leaq	1(%rcx), %r10
	imulq	$4000, %r10, %r10
	movq	%r10, %rax
	sarq	$63, %r10
	imulq	%rdx
	sarq	$17, %rdx
	subq	%r10, %rdx
	movq	%rdx, %r11
	imulq	$1461, %rdx, %rdx
	testq	%rdx, %rdx
	leaq	3(%rdx), %rax
	cmovns	%rdx, %rax
	subq	$49, %rsi
	movabsq	$1403534266930087369, %rdx
	imull	$100, %esi, %esi
	sarq	$2, %rax
	subq	%rax, %rcx
	addq	$31, %rcx
	addl	%r11d, %esi
	leaq	(%rcx,%rcx,4), %rbx
	salq	$4, %rbx
	movq	%rbx, %rax
	movq	%rbx, %r12
	imulq	%rdx
	sarq	$63, %r12
	sarq	$11, %rdx
	subq	%r12, %rdx
	leal	-1900(%rsi,%rdx), %r11d
	movq	%rdx, %r10
	cmpl	$8099, %r11d
	ja	.L5
	movq	%rbx, %rax
	imull	$-12, %r10d, %r10d
	movl	%r11d, 20(%rdi)
	movabsq	$-3007867137478590557, %rdx
	imulq	%rdx
	movl	$1, %r9d
	leaq	(%rdx,%rbx), %rsi
	movabsq	$7378697629483820647, %rdx
	sarq	$11, %rsi
	subq	%r12, %rsi
	leal	1(%r10,%rsi), %eax
	imulq	$2447, %rsi, %rsi
	movl	%eax, 16(%rdi)
	movq	%rsi, %rax
	sarq	$63, %rsi
	imulq	%rdx
	movslq	%r8d, %rax
	sarq	$5, %rdx
	subq	%rdx, %rsi
	imulq	$-1851608123, %rax, %rdx
	imulq	$-2004318071, %rax, %rax
	addl	%esi, %ecx
	movl	%ecx, 12(%rdi)
	movl	%r8d, %ecx
	shrq	$32, %rdx
	sarl	$31, %ecx
	shrq	$32, %rax
	addl	%r8d, %edx
	addl	%r8d, %eax
	sarl	$11, %edx
	sarl	$5, %eax
	subl	%ecx, %edx
	subl	%ecx, %eax
	movl	%edx, 8(%rdi)
	movslq	%eax, %rdx
	movl	%eax, %ecx
	movl	%eax, %ebx
	imulq	$-2004318071, %rdx, %rdx
	sarl	$31, %ecx
	shrq	$32, %rdx
	addl	%eax, %edx
	imull	$60, %eax, %eax
	sarl	$5, %edx
	subl	%ecx, %edx
	imull	$60, %edx, %edx
	subl	%eax, %r8d
	movl	%r8d, (%rdi)
	subl	%edx, %ebx
	movl	%ebx, 4(%rdi)
.L5:
	popq	%rbx
	movl	%r9d, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	testl	%r8d, %r8d
	jns	.L7
	subl	$1, %ecx
	addl	$86400, %r8d
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L12:
	movl	%r9d, %eax
	ret
	.cfi_endproc
.LFE152:
	.size	OPENSSL_gmtime_adj, .-OPENSSL_gmtime_adj
	.p2align 4
	.globl	OPENSSL_gmtime_diff
	.type	OPENSSL_gmtime_diff, @function
OPENSSL_gmtime_diff:
.LFB153:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	imull	$3600, 8(%rdx), %r9d
	imull	$60, 4(%rdx), %eax
	addl	%eax, %r9d
	addl	(%rdx), %r9d
	cmpl	$86399, %r9d
	jle	.L16
	subl	$86400, %r9d
	movl	$1, %r10d
.L17:
	movl	16(%rdx), %r12d
	movl	20(%rdx), %r11d
	leal	-13(%r12), %eax
	movslq	%eax, %r8
	sarl	$31, %eax
	imulq	$715827883, %r8, %r8
	sarq	$33, %r8
	movl	%r8d, %ebx
	subl	%eax, %ebx
	subl	%r8d, %eax
	leal	(%rax,%rax,2), %eax
	leal	-1(%r12,%rax,4), %r8d
	imull	$367, %r8d, %r8d
	movslq	%r8d, %rax
	sarl	$31, %r8d
	imulq	$715827883, %rax, %rax
	sarq	$33, %rax
	subl	%r8d, %eax
	movl	%eax, %r8d
	leal	6700(%r11,%rbx), %eax
	leal	6800(%r11,%rbx), %r11d
	imull	$1461, %eax, %r12d
	testl	%r12d, %r12d
	leal	3(%r12), %eax
	cmovns	%r12d, %eax
	sarl	$2, %eax
	addl	%r8d, %eax
	movslq	%r11d, %r8
	sarl	$31, %r11d
	imulq	$1374389535, %r8, %r8
	sarq	$37, %r8
	subl	%r11d, %r8d
	leal	(%r8,%r8,2), %r11d
	testl	%r11d, %r11d
	leal	3(%r11), %r8d
	cmovns	%r11d, %r8d
	xorl	%r12d, %r12d
	sarl	$2, %r8d
	subl	%r8d, %eax
	addl	12(%rdx), %eax
	subl	$32075, %eax
	cltq
	addq	%r10, %rax
	js	.L15
	imull	$3600, 8(%rcx), %r8d
	imull	$60, 4(%rcx), %edx
	addl	%edx, %r8d
	addl	(%rcx), %r8d
	cmpl	$86399, %r8d
	jle	.L19
	subl	$86400, %r8d
	movl	$1, %r11d
.L20:
	movl	16(%rcx), %ebx
	movl	20(%rcx), %r12d
	leal	-13(%rbx), %edx
	movslq	%edx, %r10
	sarl	$31, %edx
	imulq	$715827883, %r10, %r10
	sarq	$33, %r10
	movl	%r10d, %r13d
	subl	%edx, %r13d
	subl	%r10d, %edx
	leal	(%rdx,%rdx,2), %edx
	leal	-1(%rbx,%rdx,4), %ebx
	imull	$367, %ebx, %ebx
	movslq	%ebx, %rdx
	sarl	$31, %ebx
	imulq	$715827883, %rdx, %rdx
	sarq	$33, %rdx
	subl	%ebx, %edx
	movl	%edx, %r10d
	leal	6700(%r12,%r13), %edx
	imull	$1461, %edx, %ebx
	testl	%ebx, %ebx
	leal	3(%rbx), %edx
	cmovns	%ebx, %edx
	leal	6800(%r12,%r13), %ebx
	sarl	$2, %edx
	addl	%r10d, %edx
	movslq	%ebx, %r10
	sarl	$31, %ebx
	imulq	$1374389535, %r10, %r10
	sarq	$37, %r10
	subl	%ebx, %r10d
	leal	(%r10,%r10,2), %ebx
	testl	%ebx, %ebx
	leal	3(%rbx), %r10d
	cmovns	%ebx, %r10d
	xorl	%r12d, %r12d
	sarl	$2, %r10d
	subl	%r10d, %edx
	addl	12(%rcx), %edx
	subl	$32075, %edx
	movslq	%edx, %rdx
	addq	%r11, %rdx
	js	.L15
	subq	%rax, %rdx
	subl	%r9d, %r8d
	movq	%rdx, %rax
	testq	%rdx, %rdx
	jle	.L21
	testl	%r8d, %r8d
	jns	.L21
	subq	$1, %rax
	addl	$86400, %r8d
.L22:
	testq	%rdi, %rdi
	je	.L23
	movl	%eax, (%rdi)
.L23:
	movl	$1, %r12d
	testq	%rsi, %rsi
	je	.L15
	movl	%r8d, (%rsi)
.L15:
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	xorl	%r11d, %r11d
	testl	%r8d, %r8d
	jns	.L20
	addl	$86400, %r8d
	movq	$-1, %r11
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L16:
	xorl	%r10d, %r10d
	testl	%r9d, %r9d
	jns	.L17
	addl	$86400, %r9d
	movq	$-1, %r10
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L21:
	testq	%rax, %rax
	jns	.L22
	testl	%r8d, %r8d
	jle	.L22
	addq	$1, %rax
	subl	$86400, %r8d
	jmp	.L22
	.cfi_endproc
.LFE153:
	.size	OPENSSL_gmtime_diff, .-OPENSSL_gmtime_diff
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
