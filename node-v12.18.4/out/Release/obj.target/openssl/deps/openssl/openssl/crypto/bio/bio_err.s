	.file	"bio_err.c"
	.text
	.p2align 4
	.globl	ERR_load_BIO_strings
	.type	ERR_load_BIO_strings, @function
ERR_load_BIO_strings:
.LFB203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$537280512, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_func_error_string@PLT
	testq	%rax, %rax
	je	.L5
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	leaq	BIO_str_functs(%rip), %rdi
	call	ERR_load_strings_const@PLT
	leaq	BIO_str_reasons(%rip), %rdi
	call	ERR_load_strings_const@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE203:
	.size	ERR_load_BIO_strings, .-ERR_load_BIO_strings
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"accept error"
.LC1:
	.string	"addrinfo addr is not af inet"
.LC2:
	.string	"ambiguous host or service"
.LC3:
	.string	"bad fopen mode"
.LC4:
	.string	"broken pipe"
.LC5:
	.string	"connect error"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"gethostbyname addr is not af inet"
	.section	.rodata.str1.1
.LC7:
	.string	"getsockname error"
.LC8:
	.string	"getsockname truncated address"
.LC9:
	.string	"getting socktype"
.LC10:
	.string	"invalid argument"
.LC11:
	.string	"invalid socket"
.LC12:
	.string	"in use"
.LC13:
	.string	"length too long"
.LC14:
	.string	"listen v6 only"
.LC15:
	.string	"lookup returned nothing"
.LC16:
	.string	"malformed host or service"
.LC17:
	.string	"nbio connect error"
	.section	.rodata.str1.8
	.align 8
.LC18:
	.string	"no accept addr or service specified"
	.align 8
.LC19:
	.string	"no hostname or service specified"
	.section	.rodata.str1.1
.LC20:
	.string	"no port defined"
.LC21:
	.string	"no such file"
.LC22:
	.string	"null parameter"
.LC23:
	.string	"unable to bind socket"
.LC24:
	.string	"unable to create socket"
.LC25:
	.string	"unable to keepalive"
.LC26:
	.string	"unable to listen socket"
.LC27:
	.string	"unable to nodelay"
.LC28:
	.string	"unable to reuseaddr"
.LC29:
	.string	"unavailable ip family"
.LC30:
	.string	"uninitialized"
.LC31:
	.string	"unknown info type"
.LC32:
	.string	"unsupported ip family"
.LC33:
	.string	"unsupported method"
.LC34:
	.string	"unsupported protocol family"
.LC35:
	.string	"write to read only BIO"
.LC36:
	.string	"WSAStartup"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	BIO_str_reasons, @object
	.size	BIO_str_reasons, 608
BIO_str_reasons:
	.quad	536871012
	.quad	.LC0
	.quad	536871053
	.quad	.LC1
	.quad	536871041
	.quad	.LC2
	.quad	536871013
	.quad	.LC3
	.quad	536871036
	.quad	.LC4
	.quad	536871015
	.quad	.LC5
	.quad	536871019
	.quad	.LC6
	.quad	536871044
	.quad	.LC7
	.quad	536871045
	.quad	.LC8
	.quad	536871046
	.quad	.LC9
	.quad	536871037
	.quad	.LC10
	.quad	536871047
	.quad	.LC11
	.quad	536871035
	.quad	.LC12
	.quad	536871014
	.quad	.LC13
	.quad	536871048
	.quad	.LC14
	.quad	536871054
	.quad	.LC15
	.quad	536871042
	.quad	.LC16
	.quad	536871022
	.quad	.LC17
	.quad	536871055
	.quad	.LC18
	.quad	536871056
	.quad	.LC19
	.quad	536871025
	.quad	.LC20
	.quad	536871040
	.quad	.LC21
	.quad	536871027
	.quad	.LC22
	.quad	536871029
	.quad	.LC23
	.quad	536871030
	.quad	.LC24
	.quad	536871049
	.quad	.LC25
	.quad	536871031
	.quad	.LC26
	.quad	536871050
	.quad	.LC27
	.quad	536871051
	.quad	.LC28
	.quad	536871057
	.quad	.LC29
	.quad	536871032
	.quad	.LC30
	.quad	536871052
	.quad	.LC31
	.quad	536871058
	.quad	.LC32
	.quad	536871033
	.quad	.LC33
	.quad	536871043
	.quad	.LC34
	.quad	536871038
	.quad	.LC35
	.quad	536871034
	.quad	.LC36
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC37:
	.string	"acpt_state"
.LC38:
	.string	"addrinfo_wrap"
.LC39:
	.string	"addr_strings"
.LC40:
	.string	"BIO_accept"
.LC41:
	.string	"BIO_accept_ex"
.LC42:
	.string	"BIO_ACCEPT_new"
.LC43:
	.string	"BIO_ADDR_new"
.LC44:
	.string	"BIO_bind"
.LC45:
	.string	"BIO_callback_ctrl"
.LC46:
	.string	"BIO_connect"
.LC47:
	.string	"BIO_CONNECT_new"
.LC48:
	.string	"BIO_ctrl"
.LC49:
	.string	"BIO_gets"
.LC50:
	.string	"BIO_get_host_ip"
.LC51:
	.string	"BIO_get_new_index"
.LC52:
	.string	"BIO_get_port"
.LC53:
	.string	"BIO_listen"
.LC54:
	.string	"BIO_lookup"
.LC55:
	.string	"BIO_lookup_ex"
.LC56:
	.string	"bio_make_pair"
.LC57:
	.string	"BIO_meth_new"
.LC58:
	.string	"BIO_new"
.LC59:
	.string	"BIO_new_dgram_sctp"
.LC60:
	.string	"BIO_new_file"
.LC61:
	.string	"BIO_new_mem_buf"
.LC62:
	.string	"BIO_nread"
.LC63:
	.string	"BIO_nread0"
.LC64:
	.string	"BIO_nwrite"
.LC65:
	.string	"BIO_nwrite0"
.LC66:
	.string	"BIO_parse_hostserv"
.LC67:
	.string	"BIO_puts"
.LC68:
	.string	"BIO_read"
.LC69:
	.string	"BIO_read_ex"
.LC70:
	.string	"bio_read_intern"
.LC71:
	.string	"BIO_socket"
.LC72:
	.string	"BIO_socket_nbio"
.LC73:
	.string	"BIO_sock_info"
.LC74:
	.string	"BIO_sock_init"
.LC75:
	.string	"BIO_write"
.LC76:
	.string	"BIO_write_ex"
.LC77:
	.string	"bio_write_intern"
.LC78:
	.string	"buffer_ctrl"
.LC79:
	.string	"conn_ctrl"
.LC80:
	.string	"conn_state"
.LC81:
	.string	"dgram_sctp_new"
.LC82:
	.string	"dgram_sctp_read"
.LC83:
	.string	"dgram_sctp_write"
.LC84:
	.string	"doapr_outch"
.LC85:
	.string	"file_ctrl"
.LC86:
	.string	"file_read"
.LC87:
	.string	"linebuffer_ctrl"
.LC88:
	.string	"linebuffer_new"
.LC89:
	.string	"mem_write"
.LC90:
	.string	"nbiof_new"
.LC91:
	.string	"slg_write"
.LC92:
	.string	"SSL_new"
	.section	.data.rel.ro.local
	.align 32
	.type	BIO_str_functs, @object
	.size	BIO_str_functs, 912
BIO_str_functs:
	.quad	537280512
	.quad	.LC37
	.quad	537477120
	.quad	.LC38
	.quad	537419776
	.quad	.LC39
	.quad	537284608
	.quad	.LC40
	.quad	537432064
	.quad	.LC41
	.quad	537493504
	.quad	.LC42
	.quad	537460736
	.quad	.LC43
	.quad	537473024
	.quad	.LC44
	.quad	537407488
	.quad	.LC45
	.quad	537436160
	.quad	.LC46
	.quad	537497600
	.quad	.LC47
	.quad	537292800
	.quad	.LC48
	.quad	537296896
	.quad	.LC49
	.quad	537305088
	.quad	.LC50
	.quad	537288704
	.quad	.LC51
	.quad	537309184
	.quad	.LC52
	.quad	537440256
	.quad	.LC53
	.quad	537423872
	.quad	.LC54
	.quad	537456640
	.quad	.LC55
	.quad	537366528
	.quad	.LC56
	.quad	537468928
	.quad	.LC57
	.quad	537313280
	.quad	.LC58
	.quad	537464832
	.quad	.LC59
	.quad	537317376
	.quad	.LC60
	.quad	537387008
	.quad	.LC61
	.quad	537374720
	.quad	.LC62
	.quad	537378816
	.quad	.LC63
	.quad	537382912
	.quad	.LC64
	.quad	537370624
	.quad	.LC65
	.quad	537427968
	.quad	.LC66
	.quad	537321472
	.quad	.LC67
	.quad	537325568
	.quad	.LC68
	.quad	537300992
	.quad	.LC69
	.quad	537362432
	.quad	.LC70
	.quad	537444352
	.quad	.LC71
	.quad	537452544
	.quad	.LC72
	.quad	537448448
	.quad	.LC73
	.quad	537329664
	.quad	.LC74
	.quad	537333760
	.quad	.LC75
	.quad	537358336
	.quad	.LC76
	.quad	537395200
	.quad	.LC77
	.quad	537337856
	.quad	.LC78
	.quad	537391104
	.quad	.LC79
	.quad	537341952
	.quad	.LC80
	.quad	537481216
	.quad	.LC81
	.quad	537411584
	.quad	.LC82
	.quad	537415680
	.quad	.LC83
	.quad	537485312
	.quad	.LC84
	.quad	537346048
	.quad	.LC85
	.quad	537403392
	.quad	.LC86
	.quad	537399296
	.quad	.LC87
	.quad	537489408
	.quad	.LC88
	.quad	537350144
	.quad	.LC89
	.quad	537501696
	.quad	.LC90
	.quad	537505792
	.quad	.LC91
	.quad	537354240
	.quad	.LC92
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
