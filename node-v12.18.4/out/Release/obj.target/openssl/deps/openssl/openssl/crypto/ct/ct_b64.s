	.file	"ct_b64.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ct/ct_b64.c"
	.text
	.p2align 4
	.type	ct_base64_decode, @function
ct_base64_decode:
.LFB1307:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	call	strlen@PLT
	testq	%rax, %rax
	jne	.L2
	movq	$0, (%r12)
	xorl	%eax, %eax
.L1:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movq	%rax, %rbx
	shrq	$2, %rax
	movl	$36, %edx
	leaq	.LC0(%rip), %rsi
	leal	(%rax,%rax,2), %edi
	movslq	%edi, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L11
	movl	%ebx, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	EVP_DecodeBlock@PLT
	testl	%eax, %eax
	js	.L6
	cmpb	$61, -1(%r14,%rbx)
	jne	.L7
	cmpb	$61, -2(%r14,%rbx)
	leal	-1(%rax), %edx
	jne	.L8
	subl	$2, %eax
	cmpb	$61, -3(%r14,%rbx)
	jne	.L7
.L5:
	movq	%r13, %rdi
	movl	$59, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	popq	%rbx
	movl	$-1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movl	%edx, %eax
.L7:
	movq	%r13, (%r12)
	jmp	.L1
.L11:
	movl	$38, %r8d
	movl	$65, %edx
	movl	$124, %esi
	movl	$50, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L5
.L6:
	movl	$44, %r8d
	movl	$108, %edx
	movl	$124, %esi
	movl	$50, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L5
	.cfi_endproc
.LFE1307:
	.size	ct_base64_decode, .-ct_base64_decode
	.p2align 4
	.globl	SCT_new_from_base64
	.type	SCT_new_from_base64, @function
SCT_new_from_base64:
.LFB1308:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edi, %ebx
	subq	$56, %rsp
	movl	%edx, -84(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	SCT_new@PLT
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L28
	movzbl	%bl, %esi
	movq	%rax, %rdi
	call	SCT_set_version@PLT
	testl	%eax, %eax
	je	.L29
	leaq	-72(%rbp), %rbx
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	ct_base64_decode
	movl	$89, %r8d
	testl	%eax, %eax
	js	.L27
	movq	-72(%rbp), %rsi
	movslq	%eax, %rdx
	movq	%r12, %rdi
	call	SCT_set0_log_id@PLT
	testl	%eax, %eax
	je	.L16
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	$0, -72(%rbp)
	call	ct_base64_decode
	testl	%eax, %eax
	js	.L30
	movq	-72(%rbp), %rsi
	movslq	%eax, %rdx
	movq	%r12, %rdi
	call	SCT_set0_extensions@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	$0, -72(%rbp)
	call	ct_base64_decode
	movl	$106, %r8d
	testl	%eax, %eax
	js	.L27
	movq	-72(%rbp), %rdx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rdx, -64(%rbp)
	movslq	%eax, %rdx
	call	o2i_SCT_signature@PLT
	testl	%eax, %eax
	jle	.L16
	movq	-72(%rbp), %rdi
	movl	$113, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-96(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -72(%rbp)
	call	SCT_set_timestamp@PLT
	movl	-84(%rbp), %esi
	movq	%r12, %rdi
	call	SCT_set_log_entry_type@PLT
	testl	%eax, %eax
	jne	.L12
	.p2align 4,,10
	.p2align 3
.L16:
	movq	-72(%rbp), %rdi
	movl	$124, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	SCT_free@PLT
.L12:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L31
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movl	$98, %r8d
.L27:
	movl	$118, %edx
	movl	$127, %esi
	movl	$50, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L29:
	movl	$83, %r8d
	movl	$115, %edx
	movl	$127, %esi
	movl	$50, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L28:
	movl	$74, %r8d
	movl	$65, %edx
	movl	$127, %esi
	movl	$50, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L12
.L31:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1308:
	.size	SCT_new_from_base64, .-SCT_new_from_base64
	.p2align 4
	.globl	CTLOG_new_from_base64
	.type	CTLOG_new_from_base64, @function
CTLOG_new_from_base64:
.LFB1309:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -56(%rbp)
	testq	%rdi, %rdi
	je	.L41
	movq	%rsi, %rdi
	leaq	-56(%rbp), %rsi
	movq	%rdx, %r12
	call	ct_base64_decode
	movl	$149, %r8d
	testl	%eax, %eax
	js	.L40
	movq	-56(%rbp), %rdx
	xorl	%edi, %edi
	leaq	-48(%rbp), %rsi
	movq	%rdx, -48(%rbp)
	movslq	%eax, %rdx
	call	d2i_PUBKEY@PLT
	movq	-56(%rbp), %rdi
	movl	$155, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rax, %r13
	call	CRYPTO_free@PLT
	testq	%r13, %r13
	je	.L42
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	CTLOG_new@PLT
	movl	$1, %r8d
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L43
.L32:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L44
	addq	$40, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	movl	$157, %r8d
.L40:
	movl	$110, %edx
	movl	$118, %esi
	movl	$50, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r8d, %r8d
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L41:
	movl	$143, %r8d
	movl	$7, %edx
	movl	$118, %esi
	movl	$50, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r8d, %r8d
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L43:
	movq	%r13, %rdi
	call	EVP_PKEY_free@PLT
	xorl	%r8d, %r8d
	jmp	.L32
.L44:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1309:
	.size	CTLOG_new_from_base64, .-CTLOG_new_from_base64
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
