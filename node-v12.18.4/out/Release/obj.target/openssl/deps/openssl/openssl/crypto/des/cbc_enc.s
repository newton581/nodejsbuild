	.file	"cbc_enc.c"
	.text
	.p2align 4
	.globl	DES_cbc_encrypt
	.type	DES_cbc_encrypt, @function
DES_cbc_encrypt:
.LFB54:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movl	(%r8), %r15d
	movl	4(%r8), %r14d
	movq	%rdi, -112(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-8(%rdx), %rax
	movq	%rax, -120(%rbp)
	testl	%r9d, %r9d
	je	.L2
	testq	%rax, %rax
	js	.L28
	shrq	$3, %rax
	movl	%r14d, %edx
	leaq	-64(%rbp), %rbx
	movq	%rdi, %r14
	movq	%rax, -80(%rbp)
	leaq	8(,%rax,8), %rax
	leaq	(%rsi,%rax), %rsi
	movq	%rax, -72(%rbp)
	movl	%r15d, %eax
	movq	%rsi, %r15
	.p2align 4,,10
	.p2align 3
.L4:
	movl	4(%r14), %ecx
	movl	(%r14), %esi
	movq	%rbx, %rdi
	addq	$8, %r12
	addq	$8, %r14
	xorl	%ecx, %edx
	xorl	%esi, %eax
	movq	%r13, %rsi
	movl	%edx, -60(%rbp)
	movl	$1, %edx
	movl	%eax, -64(%rbp)
	call	DES_encrypt1@PLT
	movl	-64(%rbp), %eax
	movl	%eax, %edx
	movzbl	%ah, %ecx
	movb	%al, -8(%r12)
	shrl	$16, %edx
	movb	%cl, -7(%r12)
	movb	%dl, -6(%r12)
	movl	%eax, %edx
	shrl	$24, %edx
	movb	%dl, -5(%r12)
	movl	-60(%rbp), %edx
	movzbl	%dh, %ecx
	movb	%dl, -4(%r12)
	movb	%cl, -3(%r12)
	movl	%edx, %ecx
	shrl	$16, %ecx
	movb	%cl, -2(%r12)
	movl	%edx, %ecx
	shrl	$24, %ecx
	movb	%cl, -1(%r12)
	cmpq	%r15, %r12
	jne	.L4
	movl	%eax, %r15d
	movq	-80(%rbp), %rax
	movq	-104(%rbp), %rbx
	movl	%edx, %r14d
	movq	-72(%rbp), %rdi
	addq	%rdi, -112(%rbp)
	negq	%rax
	salq	$3, %rax
	leaq	-16(%rbx,%rax), %rdx
	addq	-120(%rbp), %rax
	movq	%rax, -104(%rbp)
.L3:
	cmpq	$-8, %rdx
	je	.L1
	movq	-112(%rbp), %rax
	leaq	8(%rax,%rdx), %rdx
	movq	-104(%rbp), %rax
	cmpq	$7, %rax
	ja	.L7
	leaq	.L9(%rip), %r9
	movslq	(%r9,%rax,4), %rdi
	addq	%r9, %rdi
	notrack jmp	*%rdi
	.section	.rodata
	.align 4
	.align 4
.L9:
	.long	.L7-.L9
	.long	.L29-.L9
	.long	.L30-.L9
	.long	.L31-.L9
	.long	.L32-.L9
	.long	.L33-.L9
	.long	.L34-.L9
	.long	.L8-.L9
	.text
	.p2align 4,,10
	.p2align 3
.L2:
	testq	%rax, %rax
	js	.L35
	shrq	$3, %rax
	movq	%rcx, -80(%rbp)
	leaq	-64(%rbp), %rbx
	movl	%r14d, %r13d
	movq	%rax, -136(%rbp)
	leaq	8(,%rax,8), %rax
	movl	%r15d, %r14d
	movq	%rax, -128(%rbp)
	addq	%rsi, %rax
	movq	%rax, -88(%rbp)
	movq	%rbx, -96(%rbp)
	movq	%rdi, %rbx
	.p2align 4,,10
	.p2align 3
.L17:
	movl	%r14d, -72(%rbp)
	movq	-80(%rbp), %rsi
	movl	%r13d, %r15d
	xorl	%edx, %edx
	movl	(%rbx), %r14d
	movl	4(%rbx), %r13d
	addq	$8, %r12
	addq	$8, %rbx
	movq	-96(%rbp), %rdi
	movl	%r14d, -64(%rbp)
	movl	%r13d, -60(%rbp)
	call	DES_encrypt1@PLT
	movl	-72(%rbp), %eax
	xorl	-64(%rbp), %eax
	movl	%eax, %edx
	movl	-60(%rbp), %eax
	movzbl	%dh, %ecx
	movb	%dl, -8(%r12)
	movl	%edx, %esi
	shrl	$24, %edx
	xorl	%r15d, %eax
	movb	%cl, -7(%r12)
	shrl	$16, %esi
	movb	%dl, -5(%r12)
	movl	%eax, %edx
	movzbl	%ah, %ecx
	movb	%al, -4(%r12)
	shrl	$16, %edx
	shrl	$24, %eax
	movb	%sil, -6(%r12)
	movb	%cl, -3(%r12)
	movb	%dl, -2(%r12)
	movb	%al, -1(%r12)
	cmpq	%r12, -88(%rbp)
	jne	.L17
	movq	-136(%rbp), %rax
	movq	-104(%rbp), %rdi
	movl	%r14d, %r15d
	movl	%r13d, %r14d
	movq	-128(%rbp), %rbx
	addq	%rbx, -112(%rbp)
	negq	%rax
	movq	-80(%rbp), %r13
	salq	$3, %rax
	leaq	-16(%rdi,%rax), %rbx
	addq	-120(%rbp), %rax
	movq	%rax, -104(%rbp)
.L16:
	cmpq	$-8, %rbx
	je	.L1
	movq	-112(%rbp), %rax
	leaq	-64(%rbp), %rdi
	movq	%r13, %rsi
	movq	(%rax), %rdx
	movq	%rdx, -64(%rbp)
	xorl	%edx, %edx
	call	DES_encrypt1@PLT
	movl	-64(%rbp), %ecx
	movl	-60(%rbp), %r8d
	leaq	8(%r12,%rbx), %rax
	movq	-104(%rbp), %rbx
	xorl	%r15d, %ecx
	xorl	%r14d, %r8d
	cmpq	$7, %rbx
	ja	.L1
	leaq	.L20(%rip), %rsi
	movslq	(%rsi,%rbx,4), %rdx
	addq	%rsi, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L20:
	.long	.L1-.L20
	.long	.L26-.L20
	.long	.L36-.L20
	.long	.L24-.L20
	.long	.L37-.L20
	.long	.L22-.L20
	.long	.L21-.L20
	.long	.L19-.L20
	.text
.L19:
	movl	%r8d, %edx
	subq	$1, %rax
	shrl	$16, %edx
	movb	%dl, (%rax)
.L21:
	movl	%r8d, %ebx
	subq	$1, %rax
	movb	%bh, (%rax)
.L22:
	movb	%r8b, -1(%rax)
	leaq	-1(%rax), %rdx
.L23:
	movl	%ecx, %esi
	leaq	-1(%rdx), %rax
	shrl	$24, %esi
	movb	%sil, -1(%rdx)
.L24:
	movl	%ecx, %esi
	leaq	-1(%rax), %rdx
	shrl	$16, %esi
	movb	%sil, -1(%rax)
.L25:
	movb	%ch, -1(%rdx)
	leaq	-1(%rdx), %rax
.L26:
	movb	%cl, -1(%rax)
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L45
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	xorl	%r9d, %r9d
.L10:
	movzbl	-1(%rdx), %edi
	subq	$1, %rdx
	sall	$8, %edi
	orl	%r9d, %edi
.L11:
	leaq	-1(%rdx), %r9
	movzbl	-1(%rdx), %edx
	orl	%edx, %edi
	xorl	%edi, %r14d
.L12:
	leaq	-1(%r9), %rdx
	movzbl	-1(%r9), %r9d
	sall	$24, %r9d
.L13:
	movzbl	-1(%rdx), %edi
	leaq	-1(%rdx), %r10
	sall	$16, %edi
	orl	%edi, %r9d
.L14:
	movzbl	-1(%r10), %edi
	leaq	-1(%r10), %rdx
	sall	$8, %edi
	orl	%r9d, %edi
.L15:
	movzbl	-1(%rdx), %edx
	orl	%edx, %edi
	xorl	%edi, %r15d
.L7:
	movl	$1, %edx
	leaq	-64(%rbp), %rdi
	movq	%r13, %rsi
	movl	%r15d, -64(%rbp)
	movl	%r14d, -60(%rbp)
	call	DES_encrypt1@PLT
	movq	-64(%rbp), %rdx
	movq	%rdx, (%r12)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L28:
	movq	%rax, %rdx
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L35:
	movq	%rax, %rbx
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L29:
	xorl	%edi, %edi
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L30:
	movq	%rdx, %r10
	xorl	%r9d, %r9d
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L31:
	xorl	%r9d, %r9d
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L32:
	movq	%rdx, %r9
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L33:
	xorl	%edi, %edi
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L8:
	movzbl	-1(%rdx), %edi
	subq	$1, %rdx
	sall	$16, %edi
	movl	%edi, %r9d
	jmp	.L10
.L36:
	movq	%rax, %rdx
	jmp	.L25
.L37:
	movq	%rax, %rdx
	jmp	.L23
.L45:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE54:
	.size	DES_cbc_encrypt, .-DES_cbc_encrypt
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
