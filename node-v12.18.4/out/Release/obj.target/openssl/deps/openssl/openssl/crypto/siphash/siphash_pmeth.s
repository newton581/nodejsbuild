	.file	"siphash_pmeth.c"
	.text
	.p2align 4
	.type	pkey_siphash_ctrl, @function
pkey_siphash_ctrl:
.LFB1329:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movslq	%edx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_PKEY_CTX_get_data@PLT
	movq	%rax, %r15
	cmpl	$7, %ebx
	jg	.L2
	cmpl	$5, %ebx
	jg	.L3
	xorl	%eax, %eax
	cmpl	$1, %ebx
	sete	%al
	leal	-2(%rax,%rax,2), %eax
.L1:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L17
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movl	$-2, %eax
	cmpl	$14, %ebx
	jne	.L1
	movslq	%r12d, %rsi
	leaq	24(%r15), %rdi
	call	SipHash_set_hash_size@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L3:
	cmpl	$6, %ebx
	jne	.L5
	movq	%r12, -64(%rbp)
.L6:
	testq	%r14, %r14
	je	.L8
	cmpq	$16, -64(%rbp)
	je	.L18
.L8:
	xorl	%eax, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	movq	%r13, %rdi
	call	EVP_PKEY_CTX_get0_pkey@PLT
	leaq	-64(%rbp), %rsi
	movq	%rax, %rdi
	call	EVP_PKEY_get0_siphash@PLT
	movq	%rax, %r14
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$16, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	ASN1_OCTET_STRING_set@PLT
	testl	%eax, %eax
	je	.L8
	movq	%r15, %rdi
	call	ASN1_STRING_get0_data@PLT
	leaq	24(%r15), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	SipHash_Init@PLT
	jmp	.L1
.L17:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1329:
	.size	pkey_siphash_ctrl, .-pkey_siphash_ctrl
	.p2align 4
	.type	siphash_signctx, @function
siphash_signctx:
.LFB1328:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	movq	40(%rdi), %r12
	addq	$24, %r12
	movq	%r12, %rdi
	call	SipHash_hash_size@PLT
	movq	%rax, (%rbx)
	testq	%r13, %r13
	jne	.L22
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	SipHash_Final@PLT
	.cfi_endproc
.LFE1328:
	.size	siphash_signctx, .-siphash_signctx
	.p2align 4
	.type	siphash_signctx_init, @function
siphash_signctx_init:
.LFB1327:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	EVP_PKEY_CTX_get_data@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	EVP_PKEY_CTX_get0_pkey@PLT
	leaq	-48(%rbp), %rsi
	movq	%rax, %rdi
	call	EVP_PKEY_get0_siphash@PLT
	testq	%rax, %rax
	je	.L26
	movq	%rax, %r12
	xorl	%eax, %eax
	cmpq	$16, -48(%rbp)
	je	.L29
.L23:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L30
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movq	%r13, %rdi
	movl	$256, %esi
	call	EVP_MD_CTX_set_flags@PLT
	movq	%r13, %rdi
	leaq	int_update(%rip), %rsi
	call	EVP_MD_CTX_set_update_fn@PLT
	leaq	24(%rbx), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	SipHash_Init@PLT
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L26:
	xorl	%eax, %eax
	jmp	.L23
.L30:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1327:
	.size	siphash_signctx_init, .-siphash_signctx_init
	.p2align 4
	.type	int_update, @function
int_update:
.LFB1326:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	call	EVP_MD_CTX_pkey_ctx@PLT
	movq	%rax, %rdi
	call	EVP_PKEY_CTX_get_data@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	leaq	24(%rax), %rdi
	call	SipHash_Update@PLT
	popq	%r12
	movl	$1, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1326:
	.size	int_update, .-int_update
	.p2align 4
	.type	pkey_siphash_keygen, @function
pkey_siphash_keygen:
.LFB1325:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	call	EVP_PKEY_CTX_get_data@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	ASN1_STRING_get0_data@PLT
	testq	%rax, %rax
	je	.L35
	movq	%r12, %rdi
	call	ASN1_OCTET_STRING_dup@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L35
	popq	%r12
	movq	%r13, %rdi
	movl	$1062, %esi
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	EVP_PKEY_assign@PLT
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1325:
	.size	pkey_siphash_keygen, .-pkey_siphash_keygen
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/siphash/siphash_pmeth.c"
	.text
	.p2align 4
	.type	pkey_siphash_cleanup, @function
pkey_siphash_cleanup:
.LFB1323:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	call	EVP_PKEY_CTX_get_data@PLT
	testq	%rax, %rax
	je	.L41
	movslq	(%rax), %rsi
	movq	8(%rax), %rdi
	movq	%rax, %r12
	movl	$47, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movq	%r12, %rdi
	movl	$88, %esi
	movl	$48, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	popq	%r12
	movq	%r13, %rdi
	xorl	%esi, %esi
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	EVP_PKEY_CTX_set_data@PLT
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1323:
	.size	pkey_siphash_cleanup, .-pkey_siphash_cleanup
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"digestsize"
.LC2:
	.string	"key"
.LC3:
	.string	"hexkey"
	.text
	.p2align 4
	.type	pkey_siphash_ctrl_str, @function
pkey_siphash_ctrl_str:
.LFB1330:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L48
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$11, %ecx
	movq	%rsi, %rax
	movq	%rdx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	.LC1(%rip), %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L54
	movl	$4, %ecx
	leaq	.LC2(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L55
	movq	%rax, %rsi
	movl	$7, %ecx
	leaq	.LC3(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L49
	popq	%rbx
	.cfi_restore 3
	movq	%r12, %rdi
	movq	%r8, %rdx
	popq	%r12
	.cfi_restore 12
	movl	$6, %esi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	EVP_PKEY_CTX_hex2ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L48:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	xorl	%esi, %esi
	movq	%r8, %rdi
	movl	$10, %edx
	call	strtol@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	EVP_PKEY_CTX_get_data@PLT
	movslq	%ebx, %rsi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	leaq	24(%rax), %rdi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	SipHash_set_hash_size@PLT
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	movq	%r12, %rdi
	movq	%r8, %rdx
	popq	%r12
	.cfi_restore 12
	movl	$6, %esi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	EVP_PKEY_CTX_str2ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	popq	%rbx
	movl	$-2, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1330:
	.size	pkey_siphash_ctrl_str, .-pkey_siphash_ctrl_str
	.p2align 4
	.type	pkey_siphash_init, @function
pkey_siphash_init:
.LFB1322:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$31, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$88, %edi
	subq	$8, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L60
	movl	$4, 4(%rax)
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	EVP_PKEY_CTX_set_data@PLT
	movq	%r12, %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	EVP_PKEY_CTX_set0_keygen_info@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	movl	$32, %r8d
	movl	$65, %edx
	movl	$125, %esi
	movl	$15, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1322:
	.size	pkey_siphash_init, .-pkey_siphash_init
	.p2align 4
	.type	pkey_siphash_copy, @function
pkey_siphash_copy:
.LFB1324:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$31, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	leaq	.LC0(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movl	$88, %edi
	pushq	%rbx
	.cfi_offset 3, -48
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L72
	movl	$4, 4(%rax)
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	EVP_PKEY_CTX_set_data@PLT
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	EVP_PKEY_CTX_set0_keygen_info@PLT
	movq	%r13, %rdi
	call	EVP_PKEY_CTX_get_data@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	EVP_PKEY_CTX_get_data@PLT
	movq	%rbx, %rdi
	movq	%rax, %r13
	call	ASN1_STRING_get0_data@PLT
	testq	%rax, %rax
	je	.L64
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	ASN1_STRING_copy@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L73
.L64:
	movdqu	24(%rbx), %xmm0
	movl	$1, %r14d
	movups	%xmm0, 24(%r13)
	movdqu	40(%rbx), %xmm1
	movups	%xmm1, 40(%r13)
	movdqu	56(%rbx), %xmm2
	movups	%xmm2, 56(%r13)
	movdqu	72(%rbx), %xmm3
	movups	%xmm3, 72(%r13)
.L61:
	popq	%rbx
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	movq	%r12, %rdi
	call	EVP_PKEY_CTX_get_data@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L61
	movslq	(%rax), %rsi
	movq	8(%rax), %rdi
	movl	$47, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movq	%r13, %rdi
	movl	$48, %ecx
	leaq	.LC0(%rip), %rdx
	movl	$88, %esi
	call	CRYPTO_clear_free@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	EVP_PKEY_CTX_set_data@PLT
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L72:
	xorl	%r14d, %r14d
	movl	$32, %r8d
	movl	$65, %edx
	movl	$125, %esi
	leaq	.LC0(%rip), %rcx
	movl	$15, %edi
	call	ERR_put_error@PLT
	popq	%rbx
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1324:
	.size	pkey_siphash_copy, .-pkey_siphash_copy
	.globl	siphash_pkey_meth
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	siphash_pkey_meth, @object
	.size	siphash_pkey_meth, 256
siphash_pkey_meth:
	.long	1062
	.long	4
	.quad	pkey_siphash_init
	.quad	pkey_siphash_copy
	.quad	pkey_siphash_cleanup
	.quad	0
	.quad	0
	.quad	0
	.quad	pkey_siphash_keygen
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	siphash_signctx_init
	.quad	siphash_signctx
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	pkey_siphash_ctrl
	.quad	pkey_siphash_ctrl_str
	.zero	48
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
