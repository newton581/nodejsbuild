	.file	"x_x509a.c"
	.text
	.p2align 4
	.globl	d2i_X509_CERT_AUX
	.type	d2i_X509_CERT_AUX, @function
d2i_X509_CERT_AUX:
.LFB805:
	.cfi_startproc
	endbr64
	leaq	X509_CERT_AUX_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE805:
	.size	d2i_X509_CERT_AUX, .-d2i_X509_CERT_AUX
	.p2align 4
	.globl	i2d_X509_CERT_AUX
	.type	i2d_X509_CERT_AUX, @function
i2d_X509_CERT_AUX:
.LFB806:
	.cfi_startproc
	endbr64
	leaq	X509_CERT_AUX_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE806:
	.size	i2d_X509_CERT_AUX, .-i2d_X509_CERT_AUX
	.p2align 4
	.globl	X509_CERT_AUX_new
	.type	X509_CERT_AUX_new, @function
X509_CERT_AUX_new:
.LFB807:
	.cfi_startproc
	endbr64
	leaq	X509_CERT_AUX_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE807:
	.size	X509_CERT_AUX_new, .-X509_CERT_AUX_new
	.p2align 4
	.globl	X509_CERT_AUX_free
	.type	X509_CERT_AUX_free, @function
X509_CERT_AUX_free:
.LFB808:
	.cfi_startproc
	endbr64
	leaq	X509_CERT_AUX_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE808:
	.size	X509_CERT_AUX_free, .-X509_CERT_AUX_free
	.p2align 4
	.globl	X509_trusted
	.type	X509_trusted, @function
X509_trusted:
.LFB809:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	$0, 328(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE809:
	.size	X509_trusted, .-X509_trusted
	.p2align 4
	.globl	X509_alias_set1
	.type	X509_alias_set1, @function
X509_alias_set1:
.LFB811:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	testq	%rsi, %rsi
	je	.L26
	testq	%rdi, %rdi
	je	.L12
	movq	328(%rdi), %r13
	testq	%r13, %r13
	je	.L27
.L11:
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.L28
.L14:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ASN1_STRING_set@PLT
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	testq	%rdi, %rdi
	je	.L16
	movq	328(%rdi), %rax
	testq	%rax, %rax
	je	.L16
	movq	16(%rax), %rdi
	movl	$1, %r12d
	testq	%rdi, %rdi
	je	.L7
	call	ASN1_UTF8STRING_free@PLT
	movq	328(%rbx), %rax
	movq	$0, 16(%rax)
.L7:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movl	%edx, -44(%rbp)
	movq	%rsi, -40(%rbp)
	call	ASN1_UTF8STRING_new@PLT
	movq	-40(%rbp), %rsi
	movl	-44(%rbp), %edx
	testq	%rax, %rax
	movq	%rax, 16(%r13)
	movq	%rax, %rdi
	jne	.L14
.L12:
	xorl	%r12d, %r12d
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L16:
	addq	$24, %rsp
	movl	$1, %r12d
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	leaq	X509_CERT_AUX_it(%rip), %rdi
	movl	%edx, -44(%rbp)
	movq	%rsi, -40(%rbp)
	call	ASN1_item_new@PLT
	movq	-40(%rbp), %rsi
	movl	-44(%rbp), %edx
	testq	%rax, %rax
	movq	%rax, 328(%rbx)
	movq	%rax, %r13
	jne	.L11
	jmp	.L12
	.cfi_endproc
.LFE811:
	.size	X509_alias_set1, .-X509_alias_set1
	.p2align 4
	.globl	X509_keyid_set1
	.type	X509_keyid_set1, @function
X509_keyid_set1:
.LFB812:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	testq	%rsi, %rsi
	je	.L48
	testq	%rdi, %rdi
	je	.L34
	movq	328(%rdi), %r13
	testq	%r13, %r13
	je	.L49
.L33:
	movq	24(%r13), %rdi
	testq	%rdi, %rdi
	je	.L50
.L36:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ASN1_STRING_set@PLT
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	testq	%rdi, %rdi
	je	.L38
	movq	328(%rdi), %rax
	testq	%rax, %rax
	je	.L38
	movq	24(%rax), %rdi
	movl	$1, %r12d
	testq	%rdi, %rdi
	je	.L29
	call	ASN1_OCTET_STRING_free@PLT
	movq	328(%rbx), %rax
	movq	$0, 24(%rax)
.L29:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	movl	%edx, -44(%rbp)
	movq	%rsi, -40(%rbp)
	call	ASN1_OCTET_STRING_new@PLT
	movq	-40(%rbp), %rsi
	movl	-44(%rbp), %edx
	testq	%rax, %rax
	movq	%rax, 24(%r13)
	movq	%rax, %rdi
	jne	.L36
.L34:
	xorl	%r12d, %r12d
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L38:
	addq	$24, %rsp
	movl	$1, %r12d
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	leaq	X509_CERT_AUX_it(%rip), %rdi
	movl	%edx, -44(%rbp)
	movq	%rsi, -40(%rbp)
	call	ASN1_item_new@PLT
	movq	-40(%rbp), %rsi
	movl	-44(%rbp), %edx
	testq	%rax, %rax
	movq	%rax, 328(%rbx)
	movq	%rax, %r13
	jne	.L33
	jmp	.L34
	.cfi_endproc
.LFE812:
	.size	X509_keyid_set1, .-X509_keyid_set1
	.p2align 4
	.globl	X509_alias_get0
	.type	X509_alias_get0, @function
X509_alias_get0:
.LFB813:
	.cfi_startproc
	endbr64
	movq	328(%rdi), %rax
	testq	%rax, %rax
	je	.L51
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L51
	testq	%rsi, %rsi
	je	.L53
	movl	(%rax), %edx
	movl	%edx, (%rsi)
.L53:
	movq	8(%rax), %rax
.L51:
	ret
	.cfi_endproc
.LFE813:
	.size	X509_alias_get0, .-X509_alias_get0
	.p2align 4
	.globl	X509_keyid_get0
	.type	X509_keyid_get0, @function
X509_keyid_get0:
.LFB814:
	.cfi_startproc
	endbr64
	movq	328(%rdi), %rax
	testq	%rax, %rax
	je	.L63
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L63
	testq	%rsi, %rsi
	je	.L65
	movl	(%rax), %edx
	movl	%edx, (%rsi)
.L65:
	movq	8(%rax), %rax
.L63:
	ret
	.cfi_endproc
.LFE814:
	.size	X509_keyid_get0, .-X509_keyid_get0
	.p2align 4
	.globl	X509_add1_trust_object
	.type	X509_add1_trust_object, @function
X509_add1_trust_object:
.LFB815:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L76
	movq	%rsi, %rdi
	call	OBJ_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L85
.L76:
	testq	%rbx, %rbx
	je	.L80
	movq	328(%rbx), %r13
	testq	%r13, %r13
	je	.L97
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L98
.L84:
	testq	%r12, %r12
	je	.L82
	movq	%r12, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L82
.L80:
	movq	%r12, %rdi
	call	ASN1_OBJECT_free@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	.cfi_restore_state
	leaq	X509_CERT_AUX_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, 328(%rbx)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L80
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	jne	.L84
	.p2align 4,,10
	.p2align 3
.L98:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 0(%r13)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L84
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L85:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE815:
	.size	X509_add1_trust_object, .-X509_add1_trust_object
	.p2align 4
	.globl	X509_add1_reject_object
	.type	X509_add1_reject_object, @function
X509_add1_reject_object:
.LFB816:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	call	OBJ_dup@PLT
	testq	%rax, %rax
	je	.L99
	movq	%rax, %r12
	testq	%rbx, %rbx
	je	.L101
	movq	328(%rbx), %r13
	testq	%r13, %r13
	je	.L117
.L102:
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L118
.L104:
	addq	$8, %rsp
	movq	%r12, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	OPENSSL_sk_push@PLT
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 8(%r13)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L104
.L101:
	movq	%r12, %rdi
	call	ASN1_OBJECT_free@PLT
.L99:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	.cfi_restore_state
	leaq	X509_CERT_AUX_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, 328(%rbx)
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L102
	jmp	.L101
	.cfi_endproc
.LFE816:
	.size	X509_add1_reject_object, .-X509_add1_reject_object
	.p2align 4
	.globl	X509_trust_clear
	.type	X509_trust_clear, @function
X509_trust_clear:
.LFB817:
	.cfi_startproc
	endbr64
	movq	328(%rdi), %rax
	testq	%rax, %rax
	je	.L125
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rax), %rdi
	movq	ASN1_OBJECT_free@GOTPCREL(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	328(%rbx), %rax
	movq	$0, (%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE817:
	.size	X509_trust_clear, .-X509_trust_clear
	.p2align 4
	.globl	X509_reject_clear
	.type	X509_reject_clear, @function
X509_reject_clear:
.LFB818:
	.cfi_startproc
	endbr64
	movq	328(%rdi), %rax
	testq	%rax, %rax
	je	.L134
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rax), %rdi
	movq	ASN1_OBJECT_free@GOTPCREL(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	328(%rbx), %rax
	movq	$0, 8(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE818:
	.size	X509_reject_clear, .-X509_reject_clear
	.p2align 4
	.globl	X509_get0_trust_objects
	.type	X509_get0_trust_objects, @function
X509_get0_trust_objects:
.LFB819:
	.cfi_startproc
	endbr64
	movq	328(%rdi), %rax
	testq	%rax, %rax
	je	.L137
	movq	(%rax), %rax
.L137:
	ret
	.cfi_endproc
.LFE819:
	.size	X509_get0_trust_objects, .-X509_get0_trust_objects
	.p2align 4
	.globl	X509_get0_reject_objects
	.type	X509_get0_reject_objects, @function
X509_get0_reject_objects:
.LFB820:
	.cfi_startproc
	endbr64
	movq	328(%rdi), %rax
	testq	%rax, %rax
	je	.L142
	movq	8(%rax), %rax
.L142:
	ret
	.cfi_endproc
.LFE820:
	.size	X509_get0_reject_objects, .-X509_get0_reject_objects
	.globl	X509_CERT_AUX_it
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"X509_CERT_AUX"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	X509_CERT_AUX_it, @object
	.size	X509_CERT_AUX_it, 56
X509_CERT_AUX_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	X509_CERT_AUX_seq_tt
	.quad	5
	.quad	0
	.quad	40
	.quad	.LC0
	.section	.rodata.str1.1
.LC1:
	.string	"trust"
.LC2:
	.string	"reject"
.LC3:
	.string	"alias"
.LC4:
	.string	"keyid"
.LC5:
	.string	"other"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	X509_CERT_AUX_seq_tt, @object
	.size	X509_CERT_AUX_seq_tt, 200
X509_CERT_AUX_seq_tt:
	.quad	5
	.quad	0
	.quad	0
	.quad	.LC1
	.quad	ASN1_OBJECT_it
	.quad	141
	.quad	0
	.quad	8
	.quad	.LC2
	.quad	ASN1_OBJECT_it
	.quad	1
	.quad	0
	.quad	16
	.quad	.LC3
	.quad	ASN1_UTF8STRING_it
	.quad	1
	.quad	0
	.quad	24
	.quad	.LC4
	.quad	ASN1_OCTET_STRING_it
	.quad	141
	.quad	1
	.quad	32
	.quad	.LC5
	.quad	X509_ALGOR_it
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
