	.file	"v3_bitst.c"
	.text
	.p2align 4
	.globl	i2v_ASN1_BIT_STRING
	.type	i2v_ASN1_BIT_STRING, @function
i2v_ASN1_BIT_STRING:
.LFB1296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	96(%rdi), %rbx
	movq	%rdx, -40(%rbp)
	cmpq	$0, 8(%rbx)
	je	.L2
	movq	%rsi, %r12
	leaq	-40(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L5:
	movl	(%rbx), %esi
	movq	%r12, %rdi
	call	ASN1_BIT_STRING_get_bit@PLT
	testl	%eax, %eax
	jne	.L8
	addq	$24, %rbx
	cmpq	$0, 8(%rbx)
	jne	.L5
.L2:
	movq	-40(%rbp), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r13, %rdx
	addq	$24, %rbx
	call	X509V3_add_value@PLT
	cmpq	$0, 8(%rbx)
	jne	.L5
	movq	-40(%rbp), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1296:
	.size	i2v_ASN1_BIT_STRING, .-i2v_ASN1_BIT_STRING
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/x509v3/v3_bitst.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	",value:"
.LC2:
	.string	",name:"
.LC3:
	.string	"section:"
	.text
	.p2align 4
	.globl	v2i_ASN1_BIT_STRING
	.type	v2i_ASN1_BIT_STRING, @function
v2i_ASN1_BIT_STRING:
.LFB1297:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -72(%rbp)
	call	ASN1_BIT_STRING_new@PLT
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L27
	.p2align 4,,10
	.p2align 3
.L10:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jge	.L9
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, -56(%rbp)
	movq	-72(%rbp), %rax
	movq	96(%rax), %r15
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L17
	movq	-56(%rbp), %rax
	movq	8(%rax), %rbx
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L28:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L13
	movq	32(%r15), %r12
	addq	$24, %r15
	testq	%r12, %r12
	je	.L17
.L16:
	movq	16(%r15), %rdi
	movq	%rbx, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L28
.L13:
	movl	(%r15), %esi
	movq	-64(%rbp), %rdi
	movl	$1, %edx
	call	ASN1_BIT_STRING_set_bit@PLT
	testl	%eax, %eax
	je	.L29
	cmpq	$0, 8(%r15)
	je	.L17
	addl	$1, %r13d
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L17:
	movl	$85, %r8d
	movl	$111, %edx
	movl	$101, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-56(%rbp), %rax
	subq	$8, %rsp
	leaq	.LC1(%rip), %r9
	leaq	.LC2(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	movl	$6, %edi
	movq	8(%rax), %r8
	pushq	16(%rax)
	movq	(%rax), %rdx
	xorl	%eax, %eax
	call	ERR_add_error_data@PLT
	movq	-64(%rbp), %rdi
	call	ASN1_BIT_STRING_free@PLT
	movq	$0, -64(%rbp)
	popq	%rax
	popq	%rdx
.L9:
	movq	-64(%rbp), %rax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L29:
	.cfi_restore_state
	movl	$76, %r8d
	movl	$65, %edx
	movl	$101, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-64(%rbp), %rdi
	call	ASN1_BIT_STRING_free@PLT
	movq	$0, -64(%rbp)
	jmp	.L9
.L27:
	movl	$67, %r8d
	movl	$65, %edx
	movl	$101, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L9
	.cfi_endproc
.LFE1297:
	.size	v2i_ASN1_BIT_STRING, .-v2i_ASN1_BIT_STRING
	.globl	v3_key_usage
	.section	.data.rel.ro,"aw"
	.align 32
	.type	v3_key_usage, @object
	.size	v3_key_usage, 104
v3_key_usage:
	.long	83
	.long	0
	.quad	ASN1_BIT_STRING_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	i2v_ASN1_BIT_STRING
	.quad	v2i_ASN1_BIT_STRING
	.quad	0
	.quad	0
	.quad	key_usage_type_table
	.globl	v3_nscert
	.align 32
	.type	v3_nscert, @object
	.size	v3_nscert, 104
v3_nscert:
	.long	71
	.long	0
	.quad	ASN1_BIT_STRING_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	i2v_ASN1_BIT_STRING
	.quad	v2i_ASN1_BIT_STRING
	.quad	0
	.quad	0
	.quad	ns_cert_type_table
	.section	.rodata.str1.1
.LC4:
	.string	"Digital Signature"
.LC5:
	.string	"digitalSignature"
.LC6:
	.string	"Non Repudiation"
.LC7:
	.string	"nonRepudiation"
.LC8:
	.string	"Key Encipherment"
.LC9:
	.string	"keyEncipherment"
.LC10:
	.string	"Data Encipherment"
.LC11:
	.string	"dataEncipherment"
.LC12:
	.string	"Key Agreement"
.LC13:
	.string	"keyAgreement"
.LC14:
	.string	"Certificate Sign"
.LC15:
	.string	"keyCertSign"
.LC16:
	.string	"CRL Sign"
.LC17:
	.string	"cRLSign"
.LC18:
	.string	"Encipher Only"
.LC19:
	.string	"encipherOnly"
.LC20:
	.string	"Decipher Only"
.LC21:
	.string	"decipherOnly"
	.section	.data.rel.local,"aw"
	.align 32
	.type	key_usage_type_table, @object
	.size	key_usage_type_table, 240
key_usage_type_table:
	.long	0
	.zero	4
	.quad	.LC4
	.quad	.LC5
	.long	1
	.zero	4
	.quad	.LC6
	.quad	.LC7
	.long	2
	.zero	4
	.quad	.LC8
	.quad	.LC9
	.long	3
	.zero	4
	.quad	.LC10
	.quad	.LC11
	.long	4
	.zero	4
	.quad	.LC12
	.quad	.LC13
	.long	5
	.zero	4
	.quad	.LC14
	.quad	.LC15
	.long	6
	.zero	4
	.quad	.LC16
	.quad	.LC17
	.long	7
	.zero	4
	.quad	.LC18
	.quad	.LC19
	.long	8
	.zero	4
	.quad	.LC20
	.quad	.LC21
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC22:
	.string	"SSL Client"
.LC23:
	.string	"client"
.LC24:
	.string	"SSL Server"
.LC25:
	.string	"server"
.LC26:
	.string	"S/MIME"
.LC27:
	.string	"email"
.LC28:
	.string	"Object Signing"
.LC29:
	.string	"objsign"
.LC30:
	.string	"Unused"
.LC31:
	.string	"reserved"
.LC32:
	.string	"SSL CA"
.LC33:
	.string	"sslCA"
.LC34:
	.string	"S/MIME CA"
.LC35:
	.string	"emailCA"
.LC36:
	.string	"Object Signing CA"
.LC37:
	.string	"objCA"
	.section	.data.rel.local
	.align 32
	.type	ns_cert_type_table, @object
	.size	ns_cert_type_table, 216
ns_cert_type_table:
	.long	0
	.zero	4
	.quad	.LC22
	.quad	.LC23
	.long	1
	.zero	4
	.quad	.LC24
	.quad	.LC25
	.long	2
	.zero	4
	.quad	.LC26
	.quad	.LC27
	.long	3
	.zero	4
	.quad	.LC28
	.quad	.LC29
	.long	4
	.zero	4
	.quad	.LC30
	.quad	.LC31
	.long	5
	.zero	4
	.quad	.LC32
	.quad	.LC33
	.long	6
	.zero	4
	.quad	.LC34
	.quad	.LC35
	.long	7
	.zero	4
	.quad	.LC36
	.quad	.LC37
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
