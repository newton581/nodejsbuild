	.file	"p12_add.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/pkcs12/p12_add.c"
	.text
	.p2align 4
	.globl	PKCS12_item_pack_safebag
	.type	PKCS12_item_pack_safebag, @function
PKCS12_item_pack_safebag:
.LFB803:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$8, %rsp
	call	PKCS12_BAGS_new@PLT
	testq	%rax, %rax
	je	.L10
	movl	%ebx, %edi
	movq	%rax, %r12
	call	OBJ_nid2obj@PLT
	leaq	8(%r12), %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, (%r12)
	call	ASN1_item_pack@PLT
	movl	$29, %r8d
	testq	%rax, %rax
	je	.L8
	call	PKCS12_SAFEBAG_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L11
	movq	%r12, 8(%rax)
	movl	%r14d, %edi
	call	OBJ_nid2obj@PLT
	movq	%rax, 0(%r13)
.L1:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movl	$33, %r8d
.L8:
.L5:
	endbr64
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$117, %esi
	xorl	%r13d, %r13d
	movl	$35, %edi
	call	ERR_put_error@PLT
	movq	%r12, %rdi
	call	PKCS12_BAGS_free@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L10:
	movl	$24, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$117, %esi
	movl	$35, %edi
	call	ERR_put_error@PLT
	jmp	.L1
	.cfi_endproc
.LFE803:
	.size	PKCS12_item_pack_safebag, .-PKCS12_item_pack_safebag
	.p2align 4
	.globl	PKCS12_pack_p7data
	.type	PKCS12_pack_p7data, @function
PKCS12_pack_p7data:
.LFB804:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	call	PKCS7_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L18
	movl	$21, %edi
	call	OBJ_nid2obj@PLT
	movq	%rax, 24(%r12)
	call	ASN1_OCTET_STRING_new@PLT
	movq	%rax, 32(%r12)
	testq	%rax, %rax
	je	.L19
	leaq	32(%r12), %rdx
	leaq	PKCS12_SAFEBAGS_it(%rip), %rsi
	movq	%r13, %rdi
	call	ASN1_item_pack@PLT
	testq	%rax, %rax
	je	.L20
.L12:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	movl	$51, %r8d
	movl	$65, %edx
	movl	$114, %esi
	movl	$35, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L19:
	movl	$56, %r8d
	movl	$65, %edx
	movl	$114, %esi
	movl	$35, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L16:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	PKCS7_free@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movl	$61, %r8d
	movl	$100, %edx
	movl	$114, %esi
	movl	$35, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L16
	.cfi_endproc
.LFE804:
	.size	PKCS12_pack_p7data, .-PKCS12_pack_p7data
	.p2align 4
	.globl	PKCS12_unpack_p7data
	.type	PKCS12_unpack_p7data, @function
PKCS12_unpack_p7data:
.LFB805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$21, %eax
	jne	.L25
	movq	32(%rbx), %rdi
	addq	$8, %rsp
	leaq	PKCS12_SAFEBAGS_it(%rip), %rsi
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ASN1_item_unpack@PLT
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	movl	$75, %r8d
	movl	$121, %edx
	movl	$131, %esi
	movl	$35, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE805:
	.size	PKCS12_unpack_p7data, .-PKCS12_unpack_p7data
	.p2align 4
	.globl	PKCS12_pack_p7encdata
	.type	PKCS12_pack_p7encdata, @function
PKCS12_pack_p7encdata:
.LFB806:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%r9d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	movq	%rsi, -56(%rbp)
	movl	%edx, -60(%rbp)
	call	PKCS7_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L36
	movl	$26, %esi
	movq	%rax, %rdi
	call	PKCS7_set_type@PLT
	testl	%eax, %eax
	je	.L37
	movl	%r12d, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_cipherbyname@PLT
	movl	%r15d, %ecx
	movq	%rbx, %rdx
	movl	%r13d, %esi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L32
	call	PKCS5_pbe2_set@PLT
	movq	%rax, %r12
.L33:
	testq	%r12, %r12
	je	.L38
	movq	32(%r14), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rdi
	call	X509_ALGOR_free@PLT
	movq	32(%r14), %rax
	movq	8(%rax), %rax
	movq	%r12, 8(%rax)
	movq	16(%rax), %rdi
	call	ASN1_OCTET_STRING_free@PLT
	movq	32(%r14), %rax
	movq	16(%rbp), %r8
	movq	%r12, %rdi
	movl	-60(%rbp), %ecx
	movq	-56(%rbp), %rdx
	movl	$1, %r9d
	leaq	PKCS12_SAFEBAGS_it(%rip), %rsi
	movq	8(%rax), %r13
	call	PKCS12_item_i2d_encrypt@PLT
	movq	%rax, 16(%r13)
	testq	%rax, %rax
	je	.L39
.L27:
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movl	%r12d, %edi
	call	PKCS5_pbe_set@PLT
	movq	%rax, %r12
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L37:
	movl	$97, %r8d
	movl	$120, %edx
	movl	$115, %esi
	movl	$35, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L31:
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	PKCS7_free@PLT
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$93, %r8d
	movl	$65, %edx
	movl	$115, %esi
	movl	$35, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L38:
	movl	$110, %r8d
	movl	$65, %edx
	movl	$115, %esi
	movl	$35, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L39:
	movl	$119, %r8d
	movl	$103, %edx
	movl	$115, %esi
	movl	$35, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L31
	.cfi_endproc
.LFE806:
	.size	PKCS12_pack_p7encdata, .-PKCS12_pack_p7encdata
	.p2align 4
	.globl	PKCS12_unpack_p7encdata
	.type	PKCS12_unpack_p7encdata, @function
PKCS12_unpack_p7encdata:
.LFB807:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$26, %eax
	jne	.L41
	movq	32(%rbx), %rax
	movl	%r13d, %ecx
	movq	%r12, %rdx
	movl	$1, %r9d
	leaq	PKCS12_SAFEBAGS_it(%rip), %rsi
	movq	8(%rax), %rax
	movq	8(%rax), %rdi
	movq	16(%rax), %r8
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	PKCS12_item_decrypt_d2i@PLT
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE807:
	.size	PKCS12_unpack_p7encdata, .-PKCS12_unpack_p7encdata
	.p2align 4
	.globl	PKCS12_decrypt_skey
	.type	PKCS12_decrypt_skey, @function
PKCS12_decrypt_skey:
.LFB808:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	jmp	PKCS8_decrypt@PLT
	.cfi_endproc
.LFE808:
	.size	PKCS12_decrypt_skey, .-PKCS12_decrypt_skey
	.p2align 4
	.globl	PKCS12_pack_authsafes
	.type	PKCS12_pack_authsafes, @function
PKCS12_pack_authsafes:
.LFB809:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsi, %rdi
	leaq	PKCS12_AUTHSAFES_it(%rip), %rsi
	movq	16(%r8), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	addq	$32, %rdx
	call	ASN1_item_pack@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE809:
	.size	PKCS12_pack_authsafes, .-PKCS12_pack_authsafes
	.p2align 4
	.globl	PKCS12_unpack_authsafes
	.type	PKCS12_unpack_authsafes, @function
PKCS12_unpack_authsafes:
.LFB810:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	24(%rax), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$21, %eax
	jne	.L50
	movq	16(%rbx), %rax
	leaq	PKCS12_AUTHSAFES_it(%rip), %rsi
	movq	32(%rax), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ASN1_item_unpack@PLT
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	movl	$158, %r8d
	movl	$121, %edx
	movl	$130, %esi
	movl	$35, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE810:
	.size	PKCS12_unpack_authsafes, .-PKCS12_unpack_authsafes
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
