	.file	"bio_b64.c"
	.text
	.p2align 4
	.type	b64_callback_ctrl, @function
b64_callback_ctrl:
.LFB425:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	call	BIO_next@PLT
	testq	%rax, %rax
	je	.L2
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	%rax, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_callback_ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE425:
	.size	b64_callback_ctrl, .-b64_callback_ctrl
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/evp/bio_b64.c"
	.align 8
.LC1:
	.string	"assertion failed: ctx->buf_off < (int)sizeof(ctx->buf)"
	.align 8
.LC2:
	.string	"assertion failed: ctx->buf_len <= (int)sizeof(ctx->buf)"
	.align 8
.LC3:
	.string	"assertion failed: ctx->buf_len >= ctx->buf_off"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC4:
	.string	"assertion failed: i <= n"
	.section	.rodata.str1.8
	.align 8
.LC5:
	.string	"assertion failed: ctx->buf_off <= (int)sizeof(ctx->buf)"
	.align 8
.LC6:
	.string	"assertion failed: ctx->tmp_len <= 3"
	.text
	.p2align 4
	.type	b64_write, @function
b64_write:
.LFB423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movl	%edx, -52(%rbp)
	call	BIO_get_data@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	BIO_next@PLT
	testq	%r14, %r14
	je	.L14
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L14
	movl	$15, %esi
	movq	%r12, %rdi
	call	BIO_clear_flags@PLT
	cmpl	$1, 16(%r14)
	je	.L9
	movl	$1, 16(%r14)
	movq	32(%r14), %rdi
	movq	$0, (%r14)
	movl	$0, 8(%r14)
	call	EVP_EncodeInit@PLT
.L9:
	movl	4(%r14), %edx
	cmpl	$1501, %edx
	jg	.L62
	movl	(%r14), %r15d
	cmpl	$1502, %r15d
	jg	.L63
	cmpl	%r15d, %edx
	jg	.L64
	subl	%edx, %r15d
	testl	%r15d, %r15d
	jg	.L13
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L16:
	cmpl	%r15d, %eax
	jg	.L65
	movl	4(%r14), %edx
	addl	%eax, %edx
	movl	%edx, 4(%r14)
	cmpl	$1502, %edx
	jg	.L66
	cmpl	(%r14), %edx
	jg	.L67
	subl	%eax, %r15d
	testl	%r15d, %r15d
	jle	.L20
.L13:
	movslq	%edx, %rdx
	movq	%rbx, %rdi
	leaq	40(%r14,%rdx), %rsi
	movl	%r15d, %edx
	call	BIO_write@PLT
	testl	%eax, %eax
	jg	.L16
	movq	%r12, %rdi
	movl	%eax, -52(%rbp)
	call	BIO_copy_next_retry@PLT
	movl	-52(%rbp), %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movq	$0, (%r14)
	testq	%r13, %r13
	jne	.L68
	.p2align 4,,10
	.p2align 3
.L14:
	xorl	%eax, %eax
.L5:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	movl	-52(%rbp), %eax
	testl	%eax, %eax
	jle	.L14
	movl	$0, -60(%rbp)
.L15:
	movl	-52(%rbp), %eax
	movl	$1024, %r15d
	movl	$-1, %esi
	movq	%r12, %rdi
	cmpl	$1024, %eax
	cmovle	%eax, %r15d
	movl	%r15d, -56(%rbp)
	call	BIO_test_flags@PLT
	testb	$1, %ah
	je	.L21
	movl	8(%r14), %eax
	testl	%eax, %eax
	jle	.L22
	cmpl	$3, %eax
	jg	.L69
	movl	$3, %r15d
	subl	%eax, %r15d
	movslq	%r15d, %rcx
	movl	%ecx, -56(%rbp)
	cmpl	-52(%rbp), %ecx
	jle	.L24
	movl	$1, -56(%rbp)
	movl	$1, %ecx
.L24:
	movslq	%eax, %rdx
	movl	%ecx, %r8d
	leaq	1542(%r14,%rdx), %rdi
	testl	%ecx, %ecx
	je	.L26
	xorl	%eax, %eax
.L25:
	movl	%eax, %edx
	addl	$1, %eax
	movzbl	0(%r13,%rdx), %esi
	movb	%sil, (%rdi,%rdx)
	cmpl	%r8d, %eax
	jb	.L25
	movl	8(%r14), %eax
.L26:
	movq	%rcx, -72(%rbp)
	movl	-56(%rbp), %ecx
	addl	%ecx, -60(%rbp)
	leal	(%rcx,%rax), %edx
	movl	%edx, 8(%r14)
	cmpl	$2, %edx
	jle	.L46
	leaq	1542(%r14), %rsi
	leaq	40(%r14), %rdi
	call	EVP_EncodeBlock@PLT
	movq	-72(%rbp), %rcx
	cmpl	$1502, %eax
	movl	%eax, (%r14)
	movl	%eax, %r15d
	jg	.L70
	cmpl	4(%r14), %eax
	jl	.L71
	movl	$0, 8(%r14)
.L29:
	movl	$0, 4(%r14)
	addq	%rcx, %r13
	xorl	%eax, %eax
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L44:
	cltq
	movl	%r15d, %edx
	movq	%rbx, %rdi
	leaq	40(%r14,%rax), %rsi
	call	BIO_write@PLT
	testl	%eax, %eax
	jle	.L72
	cmpl	%eax, %r15d
	jl	.L73
	subl	%eax, %r15d
	addl	4(%r14), %eax
	movl	%eax, 4(%r14)
	cmpl	$1502, %eax
	jg	.L74
	cmpl	(%r14), %eax
	jg	.L75
.L40:
	testl	%r15d, %r15d
	jg	.L44
	movl	-56(%rbp), %ecx
	subl	%ecx, -52(%rbp)
	movl	-52(%rbp), %eax
	movq	$0, (%r14)
	testl	%eax, %eax
	jg	.L15
.L46:
	movl	-60(%rbp), %eax
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L72:
	movq	%r12, %rdi
	movl	%eax, -52(%rbp)
	call	BIO_copy_next_retry@PLT
	movl	-60(%rbp), %ecx
	movl	-52(%rbp), %eax
	testl	%ecx, %ecx
	cmovne	%ecx, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L21:
	.cfi_restore_state
	movq	32(%r14), %rdi
	movl	-56(%rbp), %r8d
	leaq	40(%r14), %rsi
	movq	%r13, %rcx
	movq	%r14, %rdx
	call	EVP_EncodeUpdate@PLT
	testl	%eax, %eax
	je	.L76
	movl	(%r14), %r15d
	cmpl	$1502, %r15d
	jg	.L77
	cmpl	4(%r14), %r15d
	jl	.L78
.L39:
	movslq	-56(%rbp), %rcx
	addl	%ecx, -60(%rbp)
	jmp	.L29
.L22:
	cmpl	$2, -52(%rbp)
	jle	.L79
	movl	-56(%rbp), %eax
	movl	$2863311531, %ecx
	leaq	40(%r14), %rdi
	movq	%r13, %rsi
	imulq	%rcx, %rax
	shrq	$33, %rax
	leal	(%rax,%rax,2), %eax
	movl	%eax, %edx
	movl	%eax, -56(%rbp)
	call	EVP_EncodeBlock@PLT
	movl	%eax, (%r14)
	movl	%eax, %r15d
	cmpl	$1502, %eax
	jg	.L80
	cmpl	4(%r14), %eax
	jge	.L39
	movl	$413, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	OPENSSL_die@PLT
	.p2align 4,,10
	.p2align 3
.L65:
	movl	$361, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	call	OPENSSL_die@PLT
.L66:
	movl	$363, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	call	OPENSSL_die@PLT
.L67:
	movl	$364, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	OPENSSL_die@PLT
.L76:
	movl	-60(%rbp), %ecx
	movl	$-1, %eax
	testl	%ecx, %ecx
	cmovne	%ecx, %eax
	jmp	.L5
.L75:
	movl	$440, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	OPENSSL_die@PLT
.L74:
	movl	$439, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	call	OPENSSL_die@PLT
.L73:
	movl	$436, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	call	OPENSSL_die@PLT
.L79:
	movl	-56(%rbp), %ecx
	leaq	1542(%r14), %rax
	cmpl	$8, %ecx
	jnb	.L31
	andb	$4, %cl
	jne	.L81
	movl	-56(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L32
	movzbl	0(%r13), %edx
	andb	$2, %cl
	movb	%dl, 1542(%r14)
	jne	.L82
.L32:
	movl	-56(%rbp), %eax
	movl	%eax, 8(%r14)
	addl	-60(%rbp), %eax
	jmp	.L5
.L31:
	movq	0(%r13), %rdx
	leaq	1550(%r14), %rdi
	movq	%r13, %rsi
	andq	$-8, %rdi
	movq	%rdx, 1542(%r14)
	movl	-56(%rbp), %edx
	movq	-8(%r13,%rdx), %rcx
	movq	%rcx, -8(%rax,%rdx)
	subq	%rdi, %rax
	subq	%rax, %rsi
	addl	%edx, %eax
	shrl	$3, %eax
	movl	%eax, %ecx
	rep movsq
	jmp	.L32
.L81:
	movl	0(%r13), %edx
	movl	%edx, 1542(%r14)
	movl	-56(%rbp), %edx
	movl	-4(%r13,%rdx), %ecx
	movl	%ecx, -4(%rax,%rdx)
	jmp	.L32
.L82:
	movl	-56(%rbp), %edx
	movzwl	-2(%r13,%rdx), %ecx
	movw	%cx, -2(%rax,%rdx)
	jmp	.L32
.L71:
	movl	$395, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	OPENSSL_die@PLT
.L70:
	movl	$394, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	OPENSSL_die@PLT
.L80:
	movl	$412, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	OPENSSL_die@PLT
.L78:
	movl	$422, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	OPENSSL_die@PLT
.L77:
	movl	$421, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	OPENSSL_die@PLT
.L69:
	movl	$379, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	call	OPENSSL_die@PLT
.L62:
	movl	$351, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	OPENSSL_die@PLT
.L63:
	movl	$352, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	OPENSSL_die@PLT
.L64:
	movl	$353, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE423:
	.size	b64_write, .-b64_write
	.p2align 4
	.type	b64_ctrl, @function
b64_ctrl:
.LFB424:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	call	BIO_get_data@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	BIO_next@PLT
	testq	%r14, %r14
	je	.L107
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L107
	cmpl	$13, %ebx
	jg	.L85
	testl	%ebx, %ebx
	jle	.L86
	cmpl	$13, %ebx
	ja	.L86
	leaq	.L88(%rip), %rsi
	movl	%ebx, %edx
	movslq	(%rsi,%rdx,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L88:
	.long	.L86-.L88
	.long	.L92-.L88
	.long	.L91-.L88
	.long	.L86-.L88
	.long	.L86-.L88
	.long	.L86-.L88
	.long	.L86-.L88
	.long	.L86-.L88
	.long	.L86-.L88
	.long	.L86-.L88
	.long	.L90-.L88
	.long	.L89-.L88
	.long	.L108-.L88
	.long	.L87-.L88
	.text
.L91:
	movl	24(%r14), %ecx
	testl	%ecx, %ecx
	jg	.L95
.L108:
	movl	$1, %eax
.L83:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	cmpl	$101, %ebx
	jne	.L86
	movl	$15, %esi
	movq	%r12, %rdi
	call	BIO_clear_flags@PLT
	movq	-56(%rbp), %rdx
	movq	%r15, %rcx
	movq	%r13, %rdi
	movl	$101, %esi
	call	BIO_ctrl@PLT
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	BIO_copy_next_retry@PLT
	movq	-56(%rbp), %rax
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L107:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L87:
	.cfi_restore_state
	movl	(%r14), %eax
	movl	4(%r14), %edx
	cmpl	%edx, %eax
	jl	.L119
	subl	%edx, %eax
	jne	.L117
	movl	16(%r14), %edx
	testl	%edx, %edx
	jne	.L120
.L98:
	movq	-56(%rbp), %rdx
	movq	%r15, %rcx
	movl	$13, %esi
	jmp	.L118
.L86:
	movq	-56(%rbp), %rdx
	movq	%r15, %rcx
	movl	%ebx, %esi
.L118:
	addq	$24, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_ctrl@PLT
.L92:
	.cfi_restore_state
	movl	$1, 24(%r14)
	movq	-56(%rbp), %rdx
	movq	%r15, %rcx
	movl	$1, %esi
	movabsq	$4294967296, %rax
	movq	%rax, 16(%r14)
	jmp	.L118
.L90:
	movl	(%r14), %eax
	movl	4(%r14), %edx
	cmpl	%edx, %eax
	jl	.L121
	subl	%edx, %eax
	je	.L101
.L117:
	addq	$24, %rsp
	cltq
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L89:
	.cfi_restore_state
	movl	(%r14), %eax
	movl	4(%r14), %edx
	leaq	40(%r14), %rbx
	.p2align 4,,10
	.p2align 3
.L94:
	cmpl	%edx, %eax
	je	.L122
.L103:
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	b64_write
	testl	%eax, %eax
	js	.L117
	movl	(%r14), %eax
	movl	4(%r14), %edx
	cmpl	%edx, %eax
	jne	.L103
.L122:
	movl	$-1, %esi
	movq	%r12, %rdi
	call	BIO_test_flags@PLT
	testb	$1, %ah
	je	.L104
	movl	8(%r14), %edx
	testl	%edx, %edx
	je	.L105
	leaq	1542(%r14), %rsi
	movq	%rbx, %rdi
	call	EVP_EncodeBlock@PLT
	movq	$0, 4(%r14)
	xorl	%edx, %edx
	movl	%eax, (%r14)
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L104:
	movl	16(%r14), %eax
	testl	%eax, %eax
	je	.L105
	movq	32(%r14), %rdi
	call	EVP_ENCODE_CTX_num@PLT
	testl	%eax, %eax
	je	.L105
	movl	$0, 4(%r14)
	movq	32(%r14), %rdi
	movq	%r14, %rdx
	movq	%rbx, %rsi
	call	EVP_EncodeFinal@PLT
	movl	(%r14), %eax
	movl	4(%r14), %edx
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L95:
	movq	-56(%rbp), %rdx
	movq	%r15, %rcx
	movl	$2, %esi
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L101:
	movq	-56(%rbp), %rdx
	movq	%r15, %rcx
	movl	$10, %esi
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L105:
	movq	-56(%rbp), %rdx
	movq	%r15, %rcx
	movl	$11, %esi
	jmp	.L118
.L120:
	movq	32(%r14), %rdi
	call	EVP_ENCODE_CTX_num@PLT
	testl	%eax, %eax
	jne	.L108
	jmp	.L98
.L119:
	movl	$474, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	OPENSSL_die@PLT
.L121:
	movl	$483, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE424:
	.size	b64_ctrl, .-b64_ctrl
	.p2align 4
	.type	b64_puts, @function
b64_puts:
.LFB426:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	call	strlen@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	b64_write
	.cfi_endproc
.LFE426:
	.size	b64_puts, .-b64_puts
	.p2align 4
	.type	b64_free, @function
b64_free:
.LFB421:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L135
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	BIO_get_data@PLT
	movq	%rax, %r13
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L125
	movq	32(%r13), %rdi
	call	EVP_ENCODE_CTX_free@PLT
	movl	$103, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	BIO_set_data@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	BIO_set_init@PLT
	movl	$1, %eax
.L125:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE421:
	.size	b64_free, .-b64_free
	.p2align 4
	.type	b64_new, @function
b64_new:
.LFB420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$73, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$2568, %edi
	pushq	%r12
	.cfi_offset 12, -32
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L141
	movq	%rax, %r12
	movabsq	$4294967297, %rax
	movq	%rax, 20(%r12)
	call	EVP_ENCODE_CTX_new@PLT
	movq	%rax, 32(%r12)
	testq	%rax, %rax
	je	.L142
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	BIO_set_data@PLT
	movq	%r13, %rdi
	movl	$1, %esi
	call	BIO_set_init@PLT
	popq	%r12
	movl	$1, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	.cfi_restore_state
	movl	$74, %r8d
	movl	$65, %edx
	movl	$198, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$82, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE420:
	.size	b64_new, .-b64_new
	.section	.rodata.str1.8
	.align 8
.LC7:
	.string	"assertion failed: ctx->buf_off + i < (int)sizeof(ctx->buf)"
	.text
	.p2align 4
	.type	b64_read, @function
b64_read:
.LFB422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -104(%rbp)
	movq	%rsi, -88(%rbp)
	movl	%edx, -68(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L234
	movq	%rdi, %r15
	call	BIO_get_data@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	BIO_next@PLT
	movq	%rax, -112(%rbp)
	testq	%rbx, %rbx
	je	.L234
	testq	%rax, %rax
	je	.L234
	movl	$15, %esi
	movq	%r15, %rdi
	call	BIO_clear_flags@PLT
	cmpl	$2, 16(%rbx)
	jne	.L235
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L147
.L242:
	movslq	4(%rbx), %rdx
	cmpl	%edx, %eax
	jl	.L236
	movl	-68(%rbp), %ecx
	subl	%edx, %eax
	cmpl	%eax, %ecx
	cmovle	%ecx, %eax
	movl	%eax, -72(%rbp)
	addl	%edx, %eax
	cmpl	$1501, %eax
	jg	.L237
	movslq	-72(%rbp), %r12
	movq	-88(%rbp), %r14
	leaq	40(%rbx,%rdx), %rsi
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r12, %r15
	call	memcpy@PLT
	addl	4(%rbx), %r15d
	subl	%r12d, -68(%rbp)
	addq	%r12, %rax
	movl	%r15d, 4(%rbx)
	movq	%rax, -88(%rbp)
	cmpl	(%rbx), %r15d
	jne	.L150
	movq	$0, (%rbx)
.L150:
	movl	-68(%rbp), %r14d
	testl	%r14d, %r14d
	jle	.L200
.L193:
	leaq	-60(%rbp), %rax
	movl	$0, -124(%rbp)
	movq	%rax, -120(%rbp)
	.p2align 4,,10
	.p2align 3
.L192:
	movl	24(%rbx), %r13d
	testl	%r13d, %r13d
	jle	.L151
	movslq	8(%rbx), %rax
	movq	-112(%rbp), %rdi
	movl	$1024, %edx
	subl	%eax, %edx
	leaq	1542(%rbx,%rax), %rsi
	call	BIO_read@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jle	.L152
	movl	8(%rbx), %r14d
	addl	%eax, %r14d
.L153:
	movl	20(%rbx), %r12d
	movl	%r14d, 8(%rbx)
	testl	%r12d, %r12d
	jne	.L238
.L154:
	cmpl	$1023, %r14d
	jle	.L181
	movq	-104(%rbp), %rdi
	leaq	1542(%rbx), %rax
	movl	$-1, %esi
	leaq	40(%rbx), %r13
	movq	%rax, -96(%rbp)
	call	BIO_test_flags@PLT
	testb	$1, %ah
	je	.L183
	movl	%r14d, %r15d
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	andl	$-4, %r15d
	movl	%r15d, %edx
	call	EVP_DecodeBlock@PLT
	movl	%eax, %r12d
	leal	-1(%r15), %eax
	cltq
	cmpb	$61, 1542(%rbx,%rax)
	je	.L239
.L184:
	cmpl	%r14d, %r15d
	je	.L186
	subl	%r15d, %r14d
	movq	-96(%rbp), %rdi
	movslq	%r15d, %r15
	movslq	%r14d, %rdx
	leaq	1542(%rbx,%r15), %rsi
	call	memmove@PLT
	movl	%r14d, 8(%rbx)
.L186:
	testl	%r12d, %r12d
	jle	.L240
	movl	%r12d, (%rbx)
	movl	%r12d, 24(%rbx)
.L189:
	movl	-68(%rbp), %eax
	movq	-88(%rbp), %rdi
	movq	%r13, %rsi
	cmpl	%r12d, %eax
	cmovle	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdx
	call	memcpy@PLT
	addl	%r12d, -72(%rbp)
	movl	%r12d, 4(%rbx)
	cmpl	(%rbx), %r12d
	jne	.L191
	movq	$0, (%rbx)
.L191:
	subl	%r12d, -68(%rbp)
	movl	-68(%rbp), %eax
	addq	%r14, -88(%rbp)
	testl	%eax, %eax
	jg	.L192
.L151:
	movq	-104(%rbp), %rdi
	call	BIO_copy_next_retry@PLT
	movl	-72(%rbp), %eax
	testl	%eax, %eax
	cmove	-124(%rbp), %eax
	movl	%eax, -124(%rbp)
	jmp	.L143
.L147:
	movl	-68(%rbp), %eax
	movl	$0, -72(%rbp)
	testl	%eax, %eax
	jg	.L193
	movq	-104(%rbp), %rdi
	call	BIO_copy_next_retry@PLT
	.p2align 4,,10
	.p2align 3
.L234:
	movl	$0, -124(%rbp)
.L143:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L241
	movl	-124(%rbp), %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L235:
	.cfi_restore_state
	movl	$2, 16(%rbx)
	movq	32(%rbx), %rdi
	movq	$0, (%rbx)
	movl	$0, 8(%rbx)
	call	EVP_DecodeInit@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L242
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L183:
	movq	32(%rbx), %rdi
	movq	-96(%rbp), %rcx
	movl	%r14d, %r8d
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	EVP_DecodeUpdate@PLT
	movl	$0, 8(%rbx)
	movl	%eax, %r12d
.L188:
	movl	%r12d, 24(%rbx)
	movl	$0, 4(%rbx)
	testl	%r12d, %r12d
	js	.L243
	movl	(%rbx), %r12d
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L238:
	movq	-104(%rbp), %rdi
	movl	$-1, %esi
	call	BIO_test_flags@PLT
	andl	$256, %eax
	movl	%eax, %r9d
	je	.L155
	leaq	1542(%rbx), %rax
	movl	$0, 8(%rbx)
	leaq	40(%rbx), %r13
	movq	%rax, -96(%rbp)
.L156:
	movq	-104(%rbp), %rdi
	movl	$-1, %esi
	call	BIO_test_flags@PLT
	testb	$1, %ah
	je	.L183
	movl	%r14d, %r15d
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	andl	$-4, %r15d
	movl	%r15d, %edx
	call	EVP_DecodeBlock@PLT
	movl	%eax, %r12d
	cmpl	$2, %r15d
	jle	.L184
	leal	-1(%r15), %eax
	cltq
	cmpb	$61, 1542(%rbx,%rax)
	jne	.L184
.L239:
	leal	-2(%r15), %eax
	cltq
	cmpb	$61, 1542(%rbx,%rax)
	je	.L185
	subl	$1, %r12d
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L181:
	movl	24(%rbx), %edx
	testl	%edx, %edx
	jg	.L192
	leaq	1542(%rbx), %rax
	leaq	40(%rbx), %r13
	movq	%rax, -96(%rbp)
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L152:
	movq	-112(%rbp), %rdi
	movl	$8, %esi
	call	BIO_test_flags@PLT
	testl	%eax, %eax
	jne	.L201
	movl	8(%rbx), %r14d
	movl	%r12d, 24(%rbx)
	movl	%r12d, -124(%rbp)
	testl	%r14d, %r14d
	jne	.L153
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L155:
	movl	20(%rbx), %r11d
	testl	%r11d, %r11d
	je	.L154
	leaq	1542(%rbx), %rax
	movl	$0, -60(%rbp)
	movq	%rax, -136(%rbp)
	movq	%rax, -96(%rbp)
	testl	%r14d, %r14d
	jle	.L157
	movq	%rax, %r15
	movq	%rax, %r12
	movl	%r9d, %r13d
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L244:
	movl	$0, 12(%rbx)
	movq	%r12, %r15
.L158:
	addl	$1, %r13d
	cmpl	%r14d, %r13d
	je	.L168
.L170:
	addq	$1, %r12
	cmpb	$10, -1(%r12)
	jne	.L158
	movl	12(%rbx), %r10d
	testl	%r10d, %r10d
	jne	.L244
	movq	32(%rbx), %rdi
	movq	-120(%rbp), %rdx
	movl	%r12d, %r8d
	leaq	40(%rbx), %rsi
	subl	%r15d, %r8d
	movq	%r15, %rcx
	movq	%rsi, -80(%rbp)
	call	EVP_DecodeUpdate@PLT
	movq	-80(%rbp), %rsi
	testl	%eax, %eax
	jg	.L160
	movl	-60(%rbp), %r9d
	movq	32(%rbx), %rdi
	testl	%r9d, %r9d
	jne	.L232
	movl	20(%rbx), %r8d
	testl	%r8d, %r8d
	je	.L232
	call	EVP_DecodeInit@PLT
	addl	$1, %r13d
	movq	%r12, %r15
	cmpl	%r14d, %r13d
	jne	.L170
.L168:
	movl	-60(%rbp), %ecx
	leaq	40(%rbx), %r13
	testl	%ecx, %ecx
	je	.L245
.L169:
	movl	$0, 8(%rbx)
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L240:
	movl	$0, (%rbx)
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L232:
	movl	%r13d, %r9d
	movq	%rsi, %r13
.L161:
	movq	-136(%rbp), %rcx
	cmpq	%r15, %rcx
	jne	.L246
.L162:
	movl	%r9d, -80(%rbp)
	call	EVP_DecodeInit@PLT
	movl	-80(%rbp), %r9d
	movl	$0, 20(%rbx)
	cmpl	%r9d, %r14d
	jne	.L169
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L246:
	movq	%r15, %rax
	movl	%r14d, %esi
	subq	%rcx, %rax
	subl	%eax, %esi
	movl	%esi, %r14d
	testl	%esi, %esi
	jle	.L162
	leaq	16(%r15), %rax
	leal	-1(%rsi), %edx
	cmpq	%rax, %rcx
	leaq	1558(%rbx), %rax
	setnb	%r8b
	cmpq	%rax, %r15
	setnb	%al
	orb	%al, %r8b
	je	.L163
	cmpl	$14, %edx
	jbe	.L163
	movl	%esi, %edx
	xorl	%eax, %eax
	shrl	$4, %edx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L164:
	movdqu	(%r15,%rax), %xmm0
	movups	%xmm0, 1542(%rbx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L164
	movl	%esi, %eax
	andl	$-16, %eax
	testb	$15, %sil
	je	.L162
	movl	%eax, %r8d
	movslq	%eax, %rdx
	movzbl	(%r15,%r8), %r8d
	movb	%r8b, 1542(%rbx,%rdx)
	leal	1(%rax), %edx
	cmpl	%edx, %esi
	jle	.L162
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %r8d
	movb	%r8b, 1542(%rbx,%rdx)
	leal	2(%rax), %edx
	cmpl	%edx, %esi
	jle	.L162
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %r8d
	movb	%r8b, 1542(%rbx,%rdx)
	leal	3(%rax), %edx
	cmpl	%edx, %esi
	jle	.L162
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %r8d
	movb	%r8b, 1542(%rbx,%rdx)
	leal	4(%rax), %edx
	cmpl	%edx, %esi
	jle	.L162
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %r8d
	movb	%r8b, 1542(%rbx,%rdx)
	leal	5(%rax), %edx
	cmpl	%edx, %esi
	jle	.L162
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %r8d
	movb	%r8b, 1542(%rbx,%rdx)
	leal	6(%rax), %edx
	cmpl	%edx, %esi
	jle	.L162
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %r8d
	movb	%r8b, 1542(%rbx,%rdx)
	leal	7(%rax), %edx
	cmpl	%edx, %esi
	jle	.L162
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %r8d
	movb	%r8b, 1542(%rbx,%rdx)
	leal	8(%rax), %edx
	cmpl	%edx, %esi
	jle	.L162
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %r8d
	movb	%r8b, 1542(%rbx,%rdx)
	leal	9(%rax), %edx
	cmpl	%edx, %esi
	jle	.L162
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %r8d
	movb	%r8b, 1542(%rbx,%rdx)
	leal	10(%rax), %edx
	cmpl	%edx, %esi
	jle	.L162
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %r8d
	movb	%r8b, 1542(%rbx,%rdx)
	leal	11(%rax), %edx
	cmpl	%edx, %esi
	jle	.L162
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %r8d
	movb	%r8b, 1542(%rbx,%rdx)
	leal	12(%rax), %edx
	cmpl	%edx, %esi
	jle	.L162
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %r8d
	movb	%r8b, 1542(%rbx,%rdx)
	leal	13(%rax), %edx
	cmpl	%edx, %esi
	jle	.L162
	movslq	%edx, %rdx
	addl	$14, %eax
	movzbl	(%r15,%rdx), %r8d
	movb	%r8b, 1542(%rbx,%rdx)
	cmpl	%eax, %esi
	jle	.L162
	cltq
	movzbl	(%r15,%rax), %edx
	movb	%dl, 1542(%rbx,%rax)
	jmp	.L162
.L160:
	movl	%r13d, %r9d
	movq	32(%rbx), %rdi
	movq	%rsi, %r13
	jmp	.L161
.L185:
	subl	$2, %r12d
	jmp	.L184
.L245:
	cmpq	%r15, -136(%rbp)
	je	.L247
	cmpq	%r15, %r12
	je	.L192
	subq	%r15, %r12
	testl	%r12d, %r12d
	jle	.L177
	leaq	16(%r15), %rax
	cmpq	%rax, -136(%rbp)
	leal	-1(%r12), %edx
	leaq	1558(%rbx), %rax
	setnb	%sil
	cmpq	%rax, %r15
	setnb	%al
	orb	%al, %sil
	je	.L174
	cmpl	$14, %edx
	jbe	.L174
	movl	%r12d, %edx
	xorl	%eax, %eax
	shrl	$4, %edx
	salq	$4, %rdx
.L175:
	movdqu	(%r15,%rax), %xmm1
	movups	%xmm1, 1542(%rbx,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L175
	movl	%r12d, %eax
	andl	$-16, %eax
	testb	$15, %r12b
	je	.L177
	movslq	%eax, %rdx
	movzbl	(%r15,%rdx), %esi
	movb	%sil, 1542(%rbx,%rdx)
	leal	1(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L177
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %esi
	movb	%sil, 1542(%rbx,%rdx)
	leal	2(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L177
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %esi
	movb	%sil, 1542(%rbx,%rdx)
	leal	3(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L177
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %esi
	movb	%sil, 1542(%rbx,%rdx)
	leal	4(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L177
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %esi
	movb	%sil, 1542(%rbx,%rdx)
	leal	5(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L177
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %esi
	movb	%sil, 1542(%rbx,%rdx)
	leal	6(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L177
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %esi
	movb	%sil, 1542(%rbx,%rdx)
	leal	7(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L177
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %esi
	movb	%sil, 1542(%rbx,%rdx)
	leal	8(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L177
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %esi
	movb	%sil, 1542(%rbx,%rdx)
	leal	9(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L177
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %esi
	movb	%sil, 1542(%rbx,%rdx)
	leal	10(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L177
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %esi
	movb	%sil, 1542(%rbx,%rdx)
	leal	11(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L177
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %esi
	movb	%sil, 1542(%rbx,%rdx)
	leal	12(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L177
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %esi
	movb	%sil, 1542(%rbx,%rdx)
	leal	13(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L177
	movslq	%edx, %rdx
	addl	$14, %eax
	movzbl	(%r15,%rdx), %esi
	movb	%sil, 1542(%rbx,%rdx)
	cmpl	%eax, %r12d
	jle	.L177
	cltq
	movzbl	(%r15,%rax), %edx
	movb	%dl, 1542(%rbx,%rax)
.L177:
	movl	%r12d, 8(%rbx)
	jmp	.L192
.L243:
	movl	$0, (%rbx)
	movl	$0, -124(%rbp)
	jmp	.L151
.L163:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L166:
	movzbl	(%r15,%rax), %ecx
	movb	%cl, 1542(%rbx,%rax)
	movq	%rax, %rcx
	addq	$1, %rax
	cmpq	%rdx, %rcx
	jne	.L166
	jmp	.L162
.L157:
	leaq	40(%rbx), %r13
	jne	.L169
	jmp	.L192
.L201:
	movl	%r12d, -124(%rbp)
	jmp	.L151
.L247:
	cmpl	$1024, %r14d
	jne	.L192
	movabsq	$4294967296, %rax
	movq	%rax, 8(%rbx)
	jmp	.L192
.L174:
	xorl	%eax, %eax
.L179:
	movzbl	(%r15,%rax), %ecx
	movb	%cl, 1542(%rbx,%rax)
	movq	%rax, %rcx
	addq	$1, %rax
	cmpq	%rdx, %rcx
	jne	.L179
	jmp	.L177
.L200:
	movl	$0, -124(%rbp)
	jmp	.L151
.L237:
	movl	$141, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	call	OPENSSL_die@PLT
.L236:
	movl	$137, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	OPENSSL_die@PLT
.L241:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE422:
	.size	b64_read, .-b64_read
	.p2align 4
	.globl	BIO_f_base64
	.type	BIO_f_base64, @function
BIO_f_base64:
.LFB419:
	.cfi_startproc
	endbr64
	leaq	methods_b64(%rip), %rax
	ret
	.cfi_endproc
.LFE419:
	.size	BIO_f_base64, .-BIO_f_base64
	.section	.rodata.str1.1
.LC8:
	.string	"base64 encoding"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	methods_b64, @object
	.size	methods_b64, 96
methods_b64:
	.long	523
	.zero	4
	.quad	.LC8
	.quad	bwrite_conv
	.quad	b64_write
	.quad	bread_conv
	.quad	b64_read
	.quad	b64_puts
	.quad	0
	.quad	b64_ctrl
	.quad	b64_new
	.quad	b64_free
	.quad	b64_callback_ctrl
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
