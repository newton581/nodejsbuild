	.file	"bio_ok.c"
	.text
	.p2align 4
	.type	ok_callback_ctrl, @function
ok_callback_ctrl:
.LFB451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	call	BIO_next@PLT
	testq	%rax, %rax
	je	.L2
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	%rax, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_callback_ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE451:
	.size	ok_callback_ctrl, .-ok_callback_ctrl
	.p2align 4
	.type	block_out, @function
block_out:
.LFB455:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	call	BIO_get_data@PLT
	movq	40(%rax), %r14
	movq	%rax, %rbx
	movq	%r14, %rdi
	call	EVP_MD_CTX_md@PLT
	movq	%rax, %rdi
	call	EVP_MD_size@PLT
	leaq	60(%rbx), %rsi
	movq	%r14, %rdi
	movslq	%eax, %r13
	movq	(%rbx), %rax
	leaq	-4(%rax), %rdx
	movl	%edx, %eax
	bswap	%eax
	movl	%eax, 56(%rbx)
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jne	.L15
.L6:
	movq	%r12, %rdi
	movl	$15, %esi
	call	BIO_clear_flags@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	movq	(%rbx), %rax
	xorl	%edx, %edx
	movq	%r14, %rdi
	leaq	56(%rbx,%rax), %rsi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	je	.L6
	addq	%r13, (%rbx)
	movl	$1, %eax
	movl	$1, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE455:
	.size	block_out, .-block_out
	.p2align 4
	.type	ok_ctrl, @function
ok_ctrl:
.LFB450:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	call	BIO_get_data@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	BIO_next@PLT
	movq	%rax, %r8
	cmpl	$13, %r12d
	jg	.L17
	testl	%r12d, %r12d
	jle	.L18
	cmpl	$13, %r12d
	ja	.L18
	leaq	.L20(%rip), %rcx
	movl	%r12d, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L20:
	.long	.L18-.L20
	.long	.L24-.L20
	.long	.L23-.L20
	.long	.L22-.L20
	.long	.L18-.L20
	.long	.L18-.L20
	.long	.L18-.L20
	.long	.L18-.L20
	.long	.L18-.L20
	.long	.L18-.L20
	.long	.L19-.L20
	.long	.L21-.L20
	.long	.L18-.L20
	.long	.L19-.L20
	.text
	.p2align 4,,10
	.p2align 3
.L17:
	cmpl	$111, %r12d
	je	.L25
	cmpl	$112, %r12d
	jne	.L47
	movq	%r13, %rdi
	call	BIO_get_init@PLT
	testl	%eax, %eax
	jne	.L48
.L35:
	xorl	%eax, %eax
.L16:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L47:
	.cfi_restore_state
	cmpl	$101, %r12d
	jne	.L18
	movq	%r13, %rdi
	movl	$15, %esi
	movq	%rax, -56(%rbp)
	call	BIO_clear_flags@PLT
	movq	-56(%rbp), %r8
	movq	%r14, %rcx
	movq	%r15, %rdx
	movl	$101, %esi
	movq	%r8, %rdi
	call	BIO_ctrl@PLT
	movq	%r13, %rdi
	movq	%rax, -56(%rbp)
	call	BIO_copy_next_retry@PLT
	movq	-56(%rbp), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L19:
	.cfi_restore_state
	movl	48(%rbx), %ecx
	testl	%ecx, %ecx
	je	.L18
	movq	(%rbx), %rax
	subq	8(%rbx), %rax
	testq	%rax, %rax
	jg	.L16
.L18:
	movq	%r14, %rcx
	movq	%r15, %rdx
	movl	%r12d, %esi
.L46:
	addq	$24, %rsp
	movq	%r8, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_ctrl@PLT
.L24:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	movq	%r14, %rcx
	movq	%r15, %rdx
	movl	$1, %esi
	movabsq	$4294967296, %rax
	movq	$1, 32(%rbx)
	movq	%rax, 48(%rbx)
	movups	%xmm0, (%rbx)
	movups	%xmm0, 16(%rbx)
	jmp	.L46
.L23:
	movl	32(%rbx), %esi
	movl	$1, %eax
	testl	%esi, %esi
	jle	.L16
	movq	%r14, %rcx
	movq	%r15, %rdx
	movl	$2, %esi
	jmp	.L46
.L22:
	movslq	32(%rbx), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L21:
	.cfi_restore_state
	movl	48(%rbx), %edx
	testl	%edx, %edx
	je	.L49
.L31:
.L44:
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L25:
	movq	40(%rbx), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	je	.L35
	movl	$1, %esi
	movq	%r13, %rdi
	call	BIO_set_init@PLT
	movl	$1, %eax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L48:
	movq	40(%rbx), %rdi
	call	EVP_MD_CTX_md@PLT
	movq	%rax, (%r14)
	movl	$1, %eax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L49:
	movq	%r13, %rdi
	movq	%r8, -56(%rbp)
	call	block_out
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	je	.L35
	movl	48(%rbx), %eax
	testl	%eax, %eax
	jne	.L31
	pxor	%xmm0, %xmm0
	movq	%r14, %rcx
	movq	%r15, %rdx
	movl	$11, %esi
	movabsq	$4294967297, %rax
	movups	%xmm0, (%rbx)
	movq	%rax, 32(%rbx)
	jmp	.L46
	.cfi_endproc
.LFE450:
	.size	ok_ctrl, .-ok_ctrl
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/evp/bio_ok.c"
	.text
	.p2align 4
	.type	ok_free, @function
ok_free:
.LFB447:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L54
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	BIO_get_data@PLT
	movq	40(%rax), %rdi
	movq	%rax, %r13
	call	EVP_MD_CTX_free@PLT
	movl	$164, %ecx
	movq	%r13, %rdi
	leaq	.LC0(%rip), %rdx
	movl	$4352, %esi
	call	CRYPTO_clear_free@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	BIO_set_data@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	BIO_set_init@PLT
	popq	%r12
	movl	$1, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE447:
	.size	ok_free, .-ok_free
	.p2align 4
	.type	ok_new, @function
ok_new:
.LFB446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$136, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$4352, %edi
	pushq	%r12
	.cfi_offset 12, -32
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L64
	movl	$1, 32(%rax)
	movq	%rax, %r12
	movl	$1, 52(%rax)
	call	EVP_MD_CTX_new@PLT
	movq	%rax, 40(%r12)
	testq	%rax, %rax
	je	.L65
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	BIO_set_init@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	BIO_set_data@PLT
	popq	%r12
	movl	$1, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
	movl	$137, %r8d
	movl	$65, %edx
	movl	$200, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$145, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE446:
	.size	ok_new, .-ok_new
	.section	.rodata.str1.8
	.align 8
.LC1:
	.string	"The quick brown fox jumped over the lazy dog's back."
	.text
	.p2align 4
	.type	ok_read, @function
ok_read:
.LFB448:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L116
	movq	%rdi, %r12
	movl	%edx, %ebx
	call	BIO_get_data@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	BIO_next@PLT
	movq	%rax, %r15
	testq	%r13, %r13
	je	.L116
	testq	%rax, %rax
	je	.L116
	movq	%r12, %rdi
	call	BIO_get_init@PLT
	testl	%eax, %eax
	je	.L116
	leaq	56(%r13), %rax
	movq	%r12, -144(%rbp)
	movq	%r13, %r12
	movl	$0, -156(%rbp)
	movq	%rax, -192(%rbp)
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L71:
	movl	$4292, %edx
	movq	%r15, %rdi
	subl	%esi, %edx
	leaq	56(%r12,%rax), %rsi
	call	BIO_read@PLT
	testl	%eax, %eax
	jle	.L92
	movl	52(%r12), %r14d
	cltq
	addq	%rax, (%r12)
	cmpl	$1, %r14d
	je	.L117
.L76:
	testl	%r14d, %r14d
	je	.L118
.L83:
	movl	32(%r12), %eax
	testl	%eax, %eax
	jle	.L92
.L70:
	testl	%ebx, %ebx
	jle	.L92
	movq	(%r12), %rax
	movl	48(%r12), %edx
	movl	%eax, %esi
	testl	%edx, %edx
	je	.L71
	movq	8(%r12), %rdx
	movq	-136(%rbp), %rdi
	subl	%edx, %eax
	leaq	56(%r12,%rdx), %rsi
	cmpl	%ebx, %eax
	cmovg	%ebx, %eax
	movslq	%eax, %r14
	movq	%r14, %rdx
	subl	%r14d, %ebx
	call	memcpy@PLT
	addl	%r14d, -156(%rbp)
	addq	%r14, -136(%rbp)
	addq	8(%r12), %r14
	movq	%r14, 8(%r12)
	cmpq	(%r12), %r14
	je	.L119
.L72:
	testl	%ebx, %ebx
	je	.L92
	movq	(%r12), %rax
	movl	%eax, %esi
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L117:
	movq	-144(%rbp), %rdi
	call	BIO_get_data@PLT
	movq	40(%rax), %r13
	movq	%rax, -168(%rbp)
	movq	%r13, %rdi
	call	EVP_MD_CTX_md@PLT
	movq	%rax, %rdi
	movq	%rax, -152(%rbp)
	call	EVP_MD_size@PLT
	movq	%r13, %rdi
	movslq	%eax, %r14
	call	EVP_MD_CTX_md_data@PLT
	movq	-168(%rbp), %r8
	leal	(%r14,%r14), %edx
	movq	-152(%rbp), %rsi
	movq	%rax, %r9
	movq	(%r8), %rax
	subl	8(%r8), %eax
	cmpl	%edx, %eax
	jl	.L115
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%r9, -168(%rbp)
	movq	%r8, -152(%rbp)
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	jne	.L120
.L78:
	movq	-144(%rbp), %r12
	movl	$15, %esi
	movq	%r12, %rdi
	call	BIO_clear_flags@PLT
	movl	$15, %esi
	movq	%r12, %rdi
	call	BIO_clear_flags@PLT
.L116:
	xorl	%r14d, %r14d
.L66:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L121
	addq	$168, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	movq	-144(%rbp), %rdi
	call	BIO_get_data@PLT
	movq	40(%rax), %r13
	movq	%rax, -152(%rbp)
	movq	%r13, %rdi
	call	EVP_MD_CTX_md@PLT
	movq	%rax, %rdi
	call	EVP_MD_size@PLT
	movq	-152(%rbp), %r8
	movslq	%eax, %r10
	movl	56(%r8), %r9d
	movq	%r10, -152(%rbp)
	bswap	%r9d
	movl	%r9d, %r9d
	leaq	4(%r9,%r10), %r11
	cmpq	%r11, (%r8)
	movq	%r11, -168(%rbp)
	jb	.L83
	leaq	60(%r8), %rsi
	movq	%r9, %rdx
	movq	%r13, %rdi
	movq	%r8, -184(%rbp)
	movq	%r9, -176(%rbp)
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jne	.L122
.L85:
	movq	-144(%rbp), %r12
	movl	$15, %esi
	movq	%r12, %rdi
	call	BIO_clear_flags@PLT
	movl	$15, %esi
	movq	%r12, %rdi
	call	BIO_clear_flags@PLT
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L119:
	movq	16(%r12), %rdx
	movq	24(%r12), %rax
	movq	$0, 8(%r12)
	cmpq	%rax, %rdx
	jne	.L123
	movq	$0, (%r12)
.L74:
	movl	$0, 48(%r12)
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L120:
	movq	-152(%rbp), %r8
	movq	-168(%rbp), %r9
	movq	%r14, %rdx
	movq	8(%r8), %rax
	movq	%r9, %rdi
	leaq	56(%r8,%rax), %rsi
	call	memcpy@PLT
	testq	%r14, %r14
	movq	-152(%rbp), %r8
	movq	%rax, %r9
	je	.L79
	movzbl	(%rax), %edi
	movzbl	3(%rax), %r10d
	movzbl	1(%rax), %edx
	movzbl	2(%rax), %esi
	xorl	%eax, %eax
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L91:
	movl	%edx, %ecx
	movl	%esi, %edx
	movl	%ecx, %esi
	movl	%edi, %ecx
	movl	%r10d, %edi
	movl	%ecx, %r10d
.L80:
	addq	$4, %rax
	cmpq	%rax, %r14
	ja	.L91
	movb	%r10b, (%r9)
	movb	%dil, 3(%r9)
	movb	%sil, 1(%r9)
	movb	%dl, 2(%r9)
.L79:
	addq	%r14, 8(%r8)
	movl	$52, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	movq	%r8, -152(%rbp)
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L78
	leaq	-128(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rsi, -168(%rbp)
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	je	.L78
	movq	-152(%rbp), %r8
	movq	-168(%rbp), %rsi
	movq	%r14, %rdx
	movq	8(%r8), %r13
	leaq	56(%r8,%r13), %rdi
	addq	%r13, %r14
	call	memcmp@PLT
	movq	-152(%rbp), %r8
	movq	%r14, 8(%r8)
	testl	%eax, %eax
	jne	.L81
	movq	(%r8), %rax
	movl	$0, 52(%r8)
	movq	%rax, %r13
	subq	%r14, %r13
	cmpq	%rax, %r14
	je	.L82
	leaq	56(%r8,%r14), %rsi
	leaq	56(%r8), %rdi
	movq	%r13, %rdx
	call	memmove@PLT
	movq	-152(%rbp), %r8
.L82:
	movq	%r13, (%r8)
	movq	$0, 8(%r8)
	.p2align 4,,10
	.p2align 3
.L115:
	movl	52(%r12), %r14d
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L122:
	leaq	-128(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rsi, -200(%rbp)
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	je	.L85
	movq	-176(%rbp), %r9
	movq	-184(%rbp), %r8
	movq	-152(%rbp), %r10
	movq	-200(%rbp), %rsi
	leaq	60(%r8,%r9), %rdi
	movq	%r8, -176(%rbp)
	leaq	4(%r9), %r13
	movq	%r10, %rdx
	call	memcmp@PLT
	movq	-176(%rbp), %r8
	testl	%eax, %eax
	jne	.L86
	movq	(%r8), %rax
	movq	-168(%rbp), %r11
	movq	%r13, (%r8)
	movq	$4, 8(%r8)
	movq	%r11, 24(%r8)
	movq	%rax, 16(%r8)
	movl	$1, 48(%r8)
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L123:
	subq	%rax, %rdx
	movq	-192(%rbp), %rdi
	leaq	56(%r12,%rax), %rsi
	movq	%rdx, (%r12)
	call	memmove@PLT
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L92:
	movq	-144(%rbp), %r12
	movl	$15, %esi
	movl	-156(%rbp), %r14d
	movq	%r12, %rdi
	call	BIO_clear_flags@PLT
	movq	%r12, %rdi
	call	BIO_copy_next_retry@PLT
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L86:
	movl	$0, 32(%r8)
	jmp	.L83
.L81:
	movl	$0, 32(%r8)
	movl	52(%r12), %r14d
	jmp	.L76
.L121:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE448:
	.size	ok_read, .-ok_read
	.p2align 4
	.type	ok_write, @function
ok_write:
.LFB449:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -56(%rbp)
	testl	%edx, %edx
	jle	.L124
	movq	%rdi, %r12
	movl	%edx, %r13d
	call	BIO_get_data@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	BIO_next@PLT
	movq	%rax, %rbx
	testq	%r15, %r15
	je	.L180
	testq	%rax, %rax
	je	.L180
	movq	%r12, %rdi
	call	BIO_get_init@PLT
	testl	%eax, %eax
	je	.L180
	movl	52(%r15), %esi
	testl	%esi, %esi
	jne	.L181
.L133:
	movl	%r13d, -64(%rbp)
	movl	%r13d, %r14d
.L141:
	movl	$15, %esi
	movq	%r12, %rdi
	call	BIO_clear_flags@PLT
	movq	(%r15), %rax
	movq	8(%r15), %rdx
	movl	48(%r15), %ecx
	movl	%eax, %r13d
	subl	%edx, %r13d
	testl	%ecx, %ecx
	je	.L134
	testl	%r13d, %r13d
	jg	.L136
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L135:
	movslq	%eax, %rdx
	subl	%eax, %r13d
	movl	48(%r15), %eax
	addq	8(%r15), %rdx
	movq	%rdx, 8(%r15)
	testl	%eax, %eax
	je	.L146
	testl	%r13d, %r13d
	jle	.L146
.L136:
	leaq	56(%r15,%rdx), %rsi
	movq	%rbx, %rdi
	movl	%r13d, %edx
	call	BIO_write@PLT
	testl	%eax, %eax
	jg	.L135
	movq	%r12, %rdi
	movl	%eax, -56(%rbp)
	call	BIO_copy_next_retry@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	BIO_test_flags@PLT
	movl	-56(%rbp), %r8d
	testl	%eax, %eax
	jne	.L124
	movl	$0, 32(%r15)
.L124:
	addq	$56, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L182:
	.cfi_restore_state
	movq	%r12, %rdi
	call	block_out
	testl	%eax, %eax
	jne	.L140
.L143:
	movl	$15, %esi
	movq	%r12, %rdi
	call	BIO_clear_flags@PLT
	.p2align 4,,10
	.p2align 3
.L180:
	xorl	%r8d, %r8d
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L146:
	movq	(%r15), %rax
.L134:
	movl	$0, 48(%r15)
	cmpq	%rdx, %rax
	jne	.L138
	movdqa	.LC2(%rip), %xmm0
	movl	$4, %eax
	movups	%xmm0, (%r15)
.L138:
	movq	-56(%rbp), %r13
	testq	%r13, %r13
	je	.L180
	movslq	%r14d, %rdx
	leaq	56(%r15,%rax), %rdi
	leaq	(%rdx,%rax), %rsi
	cmpq	$4100, %rsi
	jbe	.L139
	movl	$4100, %r8d
	movq	%r13, %rsi
	subl	%eax, %r8d
	movslq	%r8d, %rdx
	movl	%r8d, -72(%rbp)
	movq	%rdx, -56(%rbp)
	call	memcpy@PLT
	movq	-56(%rbp), %rdx
	movq	(%r15), %rax
	movl	-72(%rbp), %r8d
	addq	%rdx, %rax
	addq	%rdx, %r13
	movq	%rax, (%r15)
	subl	%r8d, %r14d
	movq	%r13, -56(%rbp)
	cmpq	$4099, %rax
	ja	.L182
.L140:
	testl	%r14d, %r14d
	jg	.L141
	movl	-64(%rbp), %r13d
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L139:
	movq	%r13, %rsi
	movl	-64(%rbp), %r13d
	movq	%rdx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rdx
	addq	(%r15), %rdx
	movq	%rdx, (%r15)
	cmpq	$4099, %rdx
	jbe	.L142
	movq	%r12, %rdi
	call	block_out
	testl	%eax, %eax
	je	.L143
.L142:
	movl	$15, %esi
	movq	%r12, %rdi
	call	BIO_clear_flags@PLT
	movq	%r12, %rdi
	call	BIO_copy_next_retry@PLT
	movl	%r13d, %r8d
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L181:
	movq	%r12, %rdi
	call	BIO_get_data@PLT
	movq	%rax, %r8
	movq	40(%rax), %rax
	movq	%r8, -72(%rbp)
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	EVP_MD_CTX_md@PLT
	movq	%rax, %rdi
	movq	%rax, -80(%rbp)
	call	EVP_MD_size@PLT
	movq	-64(%rbp), %rdi
	movslq	%eax, %r14
	call	EVP_MD_CTX_md_data@PLT
	movq	-72(%rbp), %r8
	movq	%rax, -88(%rbp)
	leal	(%r14,%r14), %eax
	cltq
	addq	(%r8), %rax
	cmpq	$4096, %rax
	ja	.L133
	movq	-80(%rbp), %rsi
	movq	-64(%rbp), %rdi
	xorl	%edx, %edx
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	je	.L143
	movq	-88(%rbp), %r9
	movl	%r14d, %esi
	movq	%r9, %rdi
	movq	%r9, -80(%rbp)
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jle	.L143
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %r9
	movq	%r14, %rdx
	movq	(%r8), %rax
	movq	%r9, %rsi
	leaq	56(%r8,%rax), %rdi
	call	memcpy@PLT
	movq	-72(%rbp), %r8
	movq	(%r8), %rax
	movq	%rax, %r10
	leaq	56(%r8,%rax), %r11
	testq	%r14, %r14
	je	.L131
	movzbl	56(%r8,%rax), %edi
	movzbl	3(%r11), %r9d
	xorl	%eax, %eax
	movzbl	1(%r11), %edx
	movzbl	2(%r11), %esi
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L145:
	movl	%edx, %ecx
	movl	%esi, %edx
	movl	%ecx, %esi
	movl	%edi, %ecx
	movl	%r9d, %edi
	movl	%ecx, %r9d
.L132:
	addq	$4, %rax
	cmpq	%rax, %r14
	ja	.L145
	movb	%r9b, 56(%r8,%r10)
	movb	%dil, 3(%r11)
	movb	%sil, 1(%r11)
	movb	%dl, 2(%r11)
.L131:
	addq	%r14, (%r8)
	movq	-64(%rbp), %rdi
	movl	$52, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r8, -72(%rbp)
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L143
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %rdi
	xorl	%edx, %edx
	movq	(%r8), %rax
	leaq	56(%r8,%rax), %rsi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	je	.L143
	movq	-72(%rbp), %r8
	addq	%r14, (%r8)
	movq	$1, 48(%r8)
	jmp	.L133
	.cfi_endproc
.LFE449:
	.size	ok_write, .-ok_write
	.p2align 4
	.globl	BIO_f_reliable
	.type	BIO_f_reliable, @function
BIO_f_reliable:
.LFB445:
	.cfi_startproc
	endbr64
	leaq	methods_ok(%rip), %rax
	ret
	.cfi_endproc
.LFE445:
	.size	BIO_f_reliable, .-BIO_f_reliable
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC3:
	.string	"reliable"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	methods_ok, @object
	.size	methods_ok, 96
methods_ok:
	.long	522
	.zero	4
	.quad	.LC3
	.quad	bwrite_conv
	.quad	ok_write
	.quad	bread_conv
	.quad	ok_read
	.quad	0
	.quad	0
	.quad	ok_ctrl
	.quad	ok_new
	.quad	ok_free
	.quad	ok_callback_ctrl
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.quad	4
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
