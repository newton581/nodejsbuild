	.file	"bio_md.c"
	.text
	.p2align 4
	.type	md_callback_ctrl, @function
md_callback_ctrl:
.LFB475:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	call	BIO_next@PLT
	testq	%rax, %rax
	je	.L2
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	%rax, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_callback_ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE475:
	.size	md_callback_ctrl, .-md_callback_ctrl
	.p2align 4
	.type	md_new, @function
md_new:
.LFB470:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r12
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L5
	movl	$1, %esi
	movq	%r13, %rdi
	call	BIO_set_init@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	BIO_set_data@PLT
	movl	$1, %eax
.L5:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE470:
	.size	md_new, .-md_new
	.p2align 4
	.type	md_ctrl, @function
md_ctrl:
.LFB474:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	call	BIO_get_data@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	BIO_next@PLT
	cmpl	$111, %r12d
	je	.L12
	movq	%rax, %r8
	jg	.L13
	cmpl	$12, %r12d
	je	.L14
	cmpl	$101, %r12d
	jne	.L35
	movl	$15, %esi
	movq	%r13, %rdi
	movq	%rax, -56(%rbp)
	call	BIO_clear_flags@PLT
	movq	-56(%rbp), %r8
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movl	$101, %esi
	movq	%r8, %rdi
	call	BIO_ctrl@PLT
	movq	%r13, %rdi
	movq	%rax, -56(%rbp)
	call	BIO_copy_next_retry@PLT
	movq	-56(%rbp), %rax
.L11:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	cmpl	$1, %r12d
	jne	.L17
	movq	%r13, %rdi
	movq	%rax, -56(%rbp)
	call	BIO_get_init@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	jne	.L21
.L23:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	cmpl	$120, %r12d
	je	.L18
	cmpl	$148, %r12d
	jne	.L36
	movq	%r13, %rdi
	call	BIO_get_init@PLT
	testl	%eax, %eax
	je	.L23
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	BIO_set_data@PLT
	movl	$1, %eax
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L36:
	cmpl	$112, %r12d
	jne	.L17
	movq	%r13, %rdi
	call	BIO_get_init@PLT
	testl	%eax, %eax
	je	.L23
	movq	(%r15), %rax
	movq	%rax, (%r14)
	movl	$1, %eax
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L14:
	movq	%r14, %rdi
	call	BIO_get_data@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	EVP_MD_CTX_copy_ex@PLT
	testl	%eax, %eax
	je	.L23
	movl	$1, %esi
	movq	%r13, %rdi
	call	BIO_set_init@PLT
	movl	$1, %eax
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L17:
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movl	%r12d, %esi
.L34:
	addq	$24, %rsp
	movq	%r8, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	movq	%r15, (%r14)
	movq	%r13, %rdi
	movl	$1, %esi
	call	BIO_set_init@PLT
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	EVP_DigestInit_ex@PLT
	cltq
	testq	%rax, %rax
	jle	.L11
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%rax, -56(%rbp)
	call	BIO_set_init@PLT
	movq	-56(%rbp), %rax
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L21:
	movq	(%r15), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%r8, -56(%rbp)
	call	EVP_DigestInit_ex@PLT
	cltq
	testq	%rax, %rax
	jle	.L11
	movq	-56(%rbp), %r8
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movl	$1, %esi
	jmp	.L34
	.cfi_endproc
.LFE474:
	.size	md_ctrl, .-md_ctrl
	.p2align 4
	.type	md_gets, @function
md_gets:
.LFB476:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%edx, %ebx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	BIO_get_data@PLT
	movq	(%rax), %rdx
	movq	%rax, %rdi
	xorl	%eax, %eax
	cmpl	%ebx, 8(%rdx)
	jle	.L43
.L37:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L44
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	leaq	-28(%rbp), %rdx
	movq	%r12, %rsi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	jle	.L41
	movl	-28(%rbp), %eax
	jmp	.L37
.L41:
	movl	$-1, %eax
	jmp	.L37
.L44:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE476:
	.size	md_gets, .-md_gets
	.p2align 4
	.type	md_free, @function
md_free:
.LFB471:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L49
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	BIO_get_data@PLT
	movq	%rax, %rdi
	call	EVP_MD_CTX_free@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	BIO_set_data@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	BIO_set_init@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE471:
	.size	md_free, .-md_free
	.p2align 4
	.type	md_read, @function
md_read:
.LFB472:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	testq	%rsi, %rsi
	je	.L57
	movq	%rdi, %r12
	movq	%rsi, %r13
	movl	%edx, %r14d
	call	BIO_get_data@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	BIO_next@PLT
	movq	%rax, %rdi
	testq	%r15, %r15
	je	.L57
	testq	%rax, %rax
	je	.L57
	movl	%r14d, %edx
	movq	%r13, %rsi
	call	BIO_read@PLT
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	BIO_get_init@PLT
	testl	%eax, %eax
	je	.L60
	testl	%r14d, %r14d
	jg	.L58
.L60:
	movq	%r12, %rdi
	movl	$15, %esi
	call	BIO_clear_flags@PLT
	movq	%r12, %rdi
	call	BIO_copy_next_retry@PLT
	popq	%r12
	movl	%r14d, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	xorl	%r14d, %r14d
.L54:
	popq	%r12
	movl	%r14d, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	movslq	%r14d, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jg	.L60
	orl	$-1, %r14d
	jmp	.L54
	.cfi_endproc
.LFE472:
	.size	md_read, .-md_read
	.p2align 4
	.type	md_write, @function
md_write:
.LFB473:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rsi, %rsi
	je	.L85
	movl	%edx, %r14d
	testl	%edx, %edx
	jle	.L85
	movq	%rdi, %r12
	movq	%rsi, %r13
	call	BIO_get_data@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	BIO_next@PLT
	movq	%rax, %rbx
	testq	%r15, %r15
	je	.L74
	testq	%rax, %rax
	jne	.L69
.L74:
	movq	%r12, %rdi
	call	BIO_get_init@PLT
	testq	%rbx, %rbx
	jne	.L86
.L85:
	xorl	%r14d, %r14d
.L65:
	addq	$8, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	movl	%r14d, %edx
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	BIO_write@PLT
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	BIO_get_init@PLT
	testl	%eax, %eax
	je	.L71
	testl	%r14d, %r14d
	jle	.L71
	movslq	%r14d, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L87
.L71:
	movq	%r12, %rdi
	movl	$15, %esi
	call	BIO_clear_flags@PLT
	movq	%r12, %rdi
	call	BIO_copy_next_retry@PLT
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L86:
	xorl	%r14d, %r14d
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L87:
	movl	$15, %esi
	movq	%r12, %rdi
	call	BIO_clear_flags@PLT
	jmp	.L85
	.cfi_endproc
.LFE473:
	.size	md_write, .-md_write
	.p2align 4
	.globl	BIO_f_md
	.type	BIO_f_md, @function
BIO_f_md:
.LFB469:
	.cfi_startproc
	endbr64
	leaq	methods_md(%rip), %rax
	ret
	.cfi_endproc
.LFE469:
	.size	BIO_f_md, .-BIO_f_md
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"message digest"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	methods_md, @object
	.size	methods_md, 96
methods_md:
	.long	520
	.zero	4
	.quad	.LC0
	.quad	bwrite_conv
	.quad	md_write
	.quad	bread_conv
	.quad	md_read
	.quad	0
	.quad	md_gets
	.quad	md_ctrl
	.quad	md_new
	.quad	md_free
	.quad	md_callback_ctrl
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
