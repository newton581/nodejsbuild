	.file	"ssl3_buffer.c"
	.text
	.p2align 4
	.globl	SSL3_BUFFER_set_data
	.type	SSL3_BUFFER_set_data, @function
SSL3_BUFFER_set_data:
.LFB964:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	je	.L2
	movq	(%rdi), %rdi
	call	memcpy@PLT
.L2:
	movq	%r12, 32(%rbx)
	movq	$0, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE964:
	.size	SSL3_BUFFER_set_data, .-SSL3_BUFFER_set_data
	.p2align 4
	.globl	SSL3_BUFFER_clear
	.type	SSL3_BUFFER_clear, @function
SSL3_BUFFER_clear:
.LFB965:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movups	%xmm0, 24(%rdi)
	ret
	.cfi_endproc
.LFE965:
	.size	SSL3_BUFFER_clear, .-SSL3_BUFFER_clear
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/ssl/record/ssl3_buffer.c"
	.text
	.p2align 4
	.globl	SSL3_BUFFER_release
	.type	SSL3_BUFFER_release, @function
SSL3_BUFFER_release:
.LFB966:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$33, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	CRYPTO_free@PLT
	movq	$0, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE966:
	.size	SSL3_BUFFER_release, .-SSL3_BUFFER_release
	.p2align 4
	.globl	ssl3_setup_read_buffer
	.type	ssl3_setup_read_buffer, @function
ssl3_setup_read_buffer:
.LFB967:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	8(%rdi), %rax
	movq	192(%rax), %rax
	movl	96(%rax), %eax
	andl	$8, %eax
	cmpl	$1, %eax
	movq	2144(%rdi), %rax
	sbbq	%rdx, %rdx
	andq	$-8, %rdx
	addq	$16720, %rdx
	testq	%rax, %rax
	je	.L18
.L13:
	movq	%rax, 6024(%r12)
	movl	$1, %eax
.L11:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	cmpq	%rdx, 2152(%rdi)
	movq	%rdx, %rbx
	cmovnb	2152(%rdi), %rbx
	movl	$63, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rbx, %rdi
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L19
	movq	%rax, 2144(%r12)
	movq	%rbx, 2160(%r12)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L19:
	movl	$69, %r9d
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
	movq	%r12, %rdi
	movl	$156, %edx
	movl	$-1, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L11
	.cfi_endproc
.LFE967:
	.size	ssl3_setup_read_buffer, .-ssl3_setup_read_buffer
	.p2align 4
	.globl	ssl3_setup_write_buffer
	.type	ssl3_setup_write_buffer, @function
ssl3_setup_write_buffer:
.LFB968:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -56(%rbp)
	movq	%rsi, 2136(%rdi)
	testq	%rdx, %rdx
	jne	.L21
	movq	8(%rdi), %rax
	movq	%rdi, %r14
	movq	192(%rax), %rax
	movl	96(%rax), %eax
	andl	$8, %eax
	cmpl	$1, %eax
	sbbq	%rbx, %rbx
	andq	$-9, %rbx
	addq	$97, %rbx
	cmpl	$1, %eax
	sbbq	%r15, %r15
	call	ssl_get_max_send_fragment@PLT
	andq	$-9, %r15
	addq	$17, %r15
	leal	80(%rax), %ecx
	addq	%rcx, %r15
	addq	%r15, %rbx
	testb	$8, 1493(%r14)
	cmove	%rbx, %r15
.L21:
	movq	-56(%rbp), %rax
	xorl	%r12d, %r12d
	leaq	.LC0(%rip), %r14
	leaq	2184(%rax), %rbx
	testq	%r13, %r13
	je	.L28
	.p2align 4,,10
	.p2align 3
.L27:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L25
	cmpq	%r15, 16(%rbx)
	je	.L26
	movl	$115, %edx
	movq	%r14, %rsi
	call	CRYPTO_free@PLT
	movq	$0, (%rbx)
.L25:
	movl	$120, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L40
	pxor	%xmm0, %xmm0
	movq	%rax, (%rbx)
	movups	%xmm0, 8(%rbx)
	movq	%r15, 16(%rbx)
	movups	%xmm0, 24(%rbx)
.L26:
	addq	$1, %r12
	addq	$40, %rbx
	cmpq	%r12, %r13
	jne	.L27
.L28:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L40:
	.cfi_restore_state
	movq	-56(%rbp), %rax
	movl	$128, %r9d
	movl	$65, %ecx
	leaq	.LC0(%rip), %r8
	movl	$291, %edx
	movl	$-1, %esi
	movq	%r12, 2136(%rax)
	movq	%rax, %rdi
	call	ossl_statem_fatal@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE968:
	.size	ssl3_setup_write_buffer, .-ssl3_setup_write_buffer
	.p2align 4
	.globl	ssl3_setup_buffers
	.type	ssl3_setup_buffers, @function
ssl3_setup_buffers:
.LFB969:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	8(%rdi), %rax
	movq	192(%rax), %rax
	testb	$8, 96(%rax)
	je	.L66
	movq	2144(%rdi), %rax
	testq	%rax, %rax
	je	.L67
	movq	%rax, 6024(%r12)
	movl	$97, %r13d
	movl	$17, %r14d
	movq	$1, 2136(%r12)
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L66:
	movq	2144(%rdi), %rax
	testq	%rax, %rax
	je	.L68
	movq	%rax, 6024(%r12)
	movl	$88, %r13d
	movl	$8, %r14d
	movq	$1, 2136(%r12)
.L46:
	movq	%r12, %rdi
	call	ssl_get_max_send_fragment@PLT
	movq	2184(%r12), %rdi
	leal	80(%rax), %ebx
	addq	%r14, %rbx
	addq	%rbx, %r13
	testb	$8, 1493(%r12)
	cmove	%r13, %rbx
	leaq	2184(%r12), %r13
	testq	%rdi, %rdi
	je	.L48
	movl	$1, %eax
	cmpq	%rbx, 2200(%r12)
	jne	.L69
.L41:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	movl	$115, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 2184(%r12)
.L48:
	movl	$120, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rbx, %rdi
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L70
	pxor	%xmm0, %xmm0
	movups	%xmm0, 2184(%r12)
	movq	$0, 32(%r13)
	movups	%xmm0, 16(%r13)
	movq	%rax, 2184(%r12)
	movl	$1, %eax
	movq	%rbx, 2200(%r12)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	movl	$16712, %eax
.L51:
	cmpq	%rax, 2152(%r12)
	movl	$63, %edx
	cmovnb	2152(%r12), %rax
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L71
	movq	8(%r12), %rdx
	movq	%rax, 2144(%r12)
	movq	%rbx, 2160(%r12)
	movq	192(%rdx), %rdx
	movl	96(%rdx), %edx
	movq	%rax, 6024(%r12)
	movq	$1, 2136(%r12)
	andl	$8, %edx
	cmpl	$1, %edx
	sbbq	%r13, %r13
	andq	$-9, %r13
	addq	$97, %r13
	cmpl	$1, %edx
	sbbq	%r14, %r14
	andq	$-9, %r14
	addq	$17, %r14
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L67:
	movl	$16720, %eax
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L70:
	movl	$128, %r9d
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
	movq	%r12, %rdi
	movl	$291, %edx
	movl	$-1, %esi
	movq	$0, 2136(%r12)
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L71:
	movl	$69, %r9d
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
	movq	%r12, %rdi
	movl	$156, %edx
	movl	$-1, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L41
	.cfi_endproc
.LFE969:
	.size	ssl3_setup_buffers, .-ssl3_setup_buffers
	.p2align 4
	.globl	ssl3_release_write_buffer
	.type	ssl3_release_write_buffer, @function
ssl3_release_write_buffer:
.LFB970:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	2136(%rdi), %rax
	testq	%rax, %rax
	je	.L73
	leaq	(%rax,%rax,4), %rax
	leaq	2144(%rdi), %r13
	leaq	2144(%rdi,%rax,8), %rbx
	leaq	.LC0(%rip), %r12
	.p2align 4,,10
	.p2align 3
.L74:
	movq	(%rbx), %rdi
	movl	$163, %edx
	movq	%r12, %rsi
	subq	$40, %rbx
	call	CRYPTO_free@PLT
	movq	$0, 40(%rbx)
	cmpq	%r13, %rbx
	jne	.L74
.L73:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	movq	$0, 2136(%r14)
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE970:
	.size	ssl3_release_write_buffer, .-ssl3_release_write_buffer
	.p2align 4
	.globl	ssl3_release_read_buffer
	.type	ssl3_release_read_buffer, @function
ssl3_release_read_buffer:
.LFB971:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$176, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	2144(%rdi), %rdi
	call	CRYPTO_free@PLT
	movl	$1, %eax
	movq	$0, 2144(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE971:
	.size	ssl3_release_read_buffer, .-ssl3_release_read_buffer
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
