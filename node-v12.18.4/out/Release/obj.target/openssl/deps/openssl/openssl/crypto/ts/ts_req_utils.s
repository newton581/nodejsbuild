	.file	"ts_req_utils.c"
	.text
	.p2align 4
	.globl	TS_REQ_set_version
	.type	TS_REQ_set_version, @function
TS_REQ_set_version:
.LFB1368:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	jmp	ASN1_INTEGER_set@PLT
	.cfi_endproc
.LFE1368:
	.size	TS_REQ_set_version, .-TS_REQ_set_version
	.p2align 4
	.globl	TS_REQ_get_version
	.type	TS_REQ_get_version, @function
TS_REQ_get_version:
.LFB1369:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	jmp	ASN1_INTEGER_get@PLT
	.cfi_endproc
.LFE1369:
	.size	TS_REQ_get_version, .-TS_REQ_get_version
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ts/ts_req_utils.c"
	.text
	.p2align 4
	.globl	TS_REQ_set_msg_imprint
	.type	TS_REQ_set_msg_imprint, @function
TS_REQ_set_msg_imprint:
.LFB1370:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	$1, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	%rsi, 8(%rdi)
	je	.L4
	movq	%rsi, %rdi
	call	TS_MSG_IMPRINT_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L9
	movq	8(%rbx), %rdi
	call	TS_MSG_IMPRINT_free@PLT
	movq	%r12, 8(%rbx)
.L4:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	movl	$35, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$119, %esi
	movl	$47, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1370:
	.size	TS_REQ_set_msg_imprint, .-TS_REQ_set_msg_imprint
	.p2align 4
	.globl	TS_REQ_get_msg_imprint
	.type	TS_REQ_get_msg_imprint, @function
TS_REQ_get_msg_imprint:
.LFB1371:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE1371:
	.size	TS_REQ_get_msg_imprint, .-TS_REQ_get_msg_imprint
	.p2align 4
	.globl	TS_MSG_IMPRINT_set_algo
	.type	TS_MSG_IMPRINT_set_algo, @function
TS_MSG_IMPRINT_set_algo:
.LFB1372:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	$1, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	%rsi, (%rdi)
	je	.L11
	movq	%rsi, %rdi
	call	X509_ALGOR_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L16
	movq	(%rbx), %rdi
	call	X509_ALGOR_free@PLT
	movq	%r12, (%rbx)
.L11:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movl	$56, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$118, %esi
	movl	$47, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1372:
	.size	TS_MSG_IMPRINT_set_algo, .-TS_MSG_IMPRINT_set_algo
	.p2align 4
	.globl	TS_MSG_IMPRINT_get_algo
	.type	TS_MSG_IMPRINT_get_algo, @function
TS_MSG_IMPRINT_get_algo:
.LFB1373:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE1373:
	.size	TS_MSG_IMPRINT_get_algo, .-TS_MSG_IMPRINT_get_algo
	.p2align 4
	.globl	TS_MSG_IMPRINT_set_msg
	.type	TS_MSG_IMPRINT_set_msg, @function
TS_MSG_IMPRINT_set_msg:
.LFB1374:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	jmp	ASN1_OCTET_STRING_set@PLT
	.cfi_endproc
.LFE1374:
	.size	TS_MSG_IMPRINT_set_msg, .-TS_MSG_IMPRINT_set_msg
	.p2align 4
	.globl	TS_MSG_IMPRINT_get_msg
	.type	TS_MSG_IMPRINT_get_msg, @function
TS_MSG_IMPRINT_get_msg:
.LFB1375:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE1375:
	.size	TS_MSG_IMPRINT_get_msg, .-TS_MSG_IMPRINT_get_msg
	.p2align 4
	.globl	TS_REQ_set_policy_id
	.type	TS_REQ_set_policy_id, @function
TS_REQ_set_policy_id:
.LFB1376:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	$1, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	%rsi, 16(%rdi)
	je	.L20
	movq	%rsi, %rdi
	call	OBJ_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L25
	movq	16(%rbx), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	%r12, 16(%rbx)
.L20:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	movl	$87, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$121, %esi
	movl	$47, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1376:
	.size	TS_REQ_set_policy_id, .-TS_REQ_set_policy_id
	.p2align 4
	.globl	TS_REQ_get_policy_id
	.type	TS_REQ_get_policy_id, @function
TS_REQ_get_policy_id:
.LFB1377:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE1377:
	.size	TS_REQ_get_policy_id, .-TS_REQ_get_policy_id
	.p2align 4
	.globl	TS_REQ_set_nonce
	.type	TS_REQ_set_nonce, @function
TS_REQ_set_nonce:
.LFB1378:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	$1, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	%rsi, 24(%rdi)
	je	.L27
	movq	%rsi, %rdi
	call	ASN1_INTEGER_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L32
	movq	24(%rbx), %rdi
	call	ASN1_INTEGER_free@PLT
	movq	%r12, 24(%rbx)
.L27:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movl	$108, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$120, %esi
	movl	$47, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1378:
	.size	TS_REQ_set_nonce, .-TS_REQ_set_nonce
	.p2align 4
	.globl	TS_REQ_get_nonce
	.type	TS_REQ_get_nonce, @function
TS_REQ_get_nonce:
.LFB1379:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE1379:
	.size	TS_REQ_get_nonce, .-TS_REQ_get_nonce
	.p2align 4
	.globl	TS_REQ_set_cert_req
	.type	TS_REQ_set_cert_req, @function
TS_REQ_set_cert_req:
.LFB1380:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	movl	$255, %eax
	cmovne	%eax, %esi
	movl	$1, %eax
	movl	%esi, 32(%rdi)
	ret
	.cfi_endproc
.LFE1380:
	.size	TS_REQ_set_cert_req, .-TS_REQ_set_cert_req
	.p2align 4
	.globl	TS_REQ_get_cert_req
	.type	TS_REQ_get_cert_req, @function
TS_REQ_get_cert_req:
.LFB1381:
	.cfi_startproc
	endbr64
	movl	32(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	setne	%al
	ret
	.cfi_endproc
.LFE1381:
	.size	TS_REQ_get_cert_req, .-TS_REQ_get_cert_req
	.p2align 4
	.globl	TS_REQ_get_exts
	.type	TS_REQ_get_exts, @function
TS_REQ_get_exts:
.LFB1382:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE1382:
	.size	TS_REQ_get_exts, .-TS_REQ_get_exts
	.p2align 4
	.globl	TS_REQ_ext_free
	.type	TS_REQ_ext_free, @function
TS_REQ_ext_free:
.LFB1383:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L45
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	40(%rdi), %rdi
	movq	X509_EXTENSION_free@GOTPCREL(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	$0, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE1383:
	.size	TS_REQ_ext_free, .-TS_REQ_ext_free
	.p2align 4
	.globl	TS_REQ_get_ext_count
	.type	TS_REQ_get_ext_count, @function
TS_REQ_get_ext_count:
.LFB1384:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	jmp	X509v3_get_ext_count@PLT
	.cfi_endproc
.LFE1384:
	.size	TS_REQ_get_ext_count, .-TS_REQ_get_ext_count
	.p2align 4
	.globl	TS_REQ_get_ext_by_NID
	.type	TS_REQ_get_ext_by_NID, @function
TS_REQ_get_ext_by_NID:
.LFB1385:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	jmp	X509v3_get_ext_by_NID@PLT
	.cfi_endproc
.LFE1385:
	.size	TS_REQ_get_ext_by_NID, .-TS_REQ_get_ext_by_NID
	.p2align 4
	.globl	TS_REQ_get_ext_by_OBJ
	.type	TS_REQ_get_ext_by_OBJ, @function
TS_REQ_get_ext_by_OBJ:
.LFB1386:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	jmp	X509v3_get_ext_by_OBJ@PLT
	.cfi_endproc
.LFE1386:
	.size	TS_REQ_get_ext_by_OBJ, .-TS_REQ_get_ext_by_OBJ
	.p2align 4
	.globl	TS_REQ_get_ext_by_critical
	.type	TS_REQ_get_ext_by_critical, @function
TS_REQ_get_ext_by_critical:
.LFB1387:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	jmp	X509v3_get_ext_by_critical@PLT
	.cfi_endproc
.LFE1387:
	.size	TS_REQ_get_ext_by_critical, .-TS_REQ_get_ext_by_critical
	.p2align 4
	.globl	TS_REQ_get_ext
	.type	TS_REQ_get_ext, @function
TS_REQ_get_ext:
.LFB1388:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	jmp	X509v3_get_ext@PLT
	.cfi_endproc
.LFE1388:
	.size	TS_REQ_get_ext, .-TS_REQ_get_ext
	.p2align 4
	.globl	TS_REQ_delete_ext
	.type	TS_REQ_delete_ext, @function
TS_REQ_delete_ext:
.LFB1389:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	jmp	X509v3_delete_ext@PLT
	.cfi_endproc
.LFE1389:
	.size	TS_REQ_delete_ext, .-TS_REQ_delete_ext
	.p2align 4
	.globl	TS_REQ_add_ext
	.type	TS_REQ_add_ext, @function
TS_REQ_add_ext:
.LFB1390:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$40, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509v3_add_ext@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1390:
	.size	TS_REQ_add_ext, .-TS_REQ_add_ext
	.p2align 4
	.globl	TS_REQ_get_ext_d2i
	.type	TS_REQ_get_ext_d2i, @function
TS_REQ_get_ext_d2i:
.LFB1391:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	jmp	X509V3_get_d2i@PLT
	.cfi_endproc
.LFE1391:
	.size	TS_REQ_get_ext_d2i, .-TS_REQ_get_ext_d2i
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
