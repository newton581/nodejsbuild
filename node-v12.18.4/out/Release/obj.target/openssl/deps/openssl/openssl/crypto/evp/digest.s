	.file	"digest.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/evp/digest.c"
	.text
	.p2align 4
	.globl	EVP_MD_CTX_reset
	.type	EVP_MD_CTX_reset, @function
EVP_MD_CTX_reset:
.LFB853:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L16
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L4
	cmpq	$0, 56(%rax)
	je	.L5
	movl	$2, %esi
	call	EVP_MD_CTX_test_flags@PLT
	testl	%eax, %eax
	jne	.L6
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*56(%rax)
.L6:
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L4
.L5:
	movl	68(%rax), %eax
	testl	%eax, %eax
	jne	.L20
.L4:
	movl	$1024, %esi
	movq	%r12, %rdi
	call	EVP_MD_CTX_test_flags@PLT
	testl	%eax, %eax
	je	.L21
.L8:
	movq	8(%r12), %rdi
	call	ENGINE_finish@PLT
	movq	%r12, %rdi
	movl	$48, %esi
	call	OPENSSL_cleanse@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	cmpq	$0, 24(%r12)
	je	.L4
	movl	$4, %esi
	movq	%r12, %rdi
	call	EVP_MD_CTX_test_flags@PLT
	testl	%eax, %eax
	jne	.L4
	movq	(%r12), %rax
	movq	24(%r12), %rdi
	movl	$33, %ecx
	leaq	.LC0(%rip), %rdx
	movslq	68(%rax), %rsi
	call	CRYPTO_clear_free@PLT
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L21:
	movq	32(%r12), %rdi
	call	EVP_PKEY_CTX_free@PLT
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE853:
	.size	EVP_MD_CTX_reset, .-EVP_MD_CTX_reset
	.p2align 4
	.globl	EVP_MD_CTX_new
	.type	EVP_MD_CTX_new, @function
EVP_MD_CTX_new:
.LFB854:
	.cfi_startproc
	endbr64
	movl	$51, %edx
	leaq	.LC0(%rip), %rsi
	movl	$48, %edi
	jmp	CRYPTO_zalloc@PLT
	.cfi_endproc
.LFE854:
	.size	EVP_MD_CTX_new, .-EVP_MD_CTX_new
	.p2align 4
	.globl	EVP_MD_CTX_free
	.type	EVP_MD_CTX_free, @function
EVP_MD_CTX_free:
.LFB855:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	testq	%rdi, %rdi
	je	.L24
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L26
	cmpq	$0, 56(%rax)
	je	.L27
	movl	$2, %esi
	call	EVP_MD_CTX_test_flags@PLT
	testl	%eax, %eax
	jne	.L28
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*56(%rax)
.L28:
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L26
.L27:
	movl	68(%rax), %eax
	testl	%eax, %eax
	jne	.L38
.L26:
	movl	$1024, %esi
	movq	%r12, %rdi
	call	EVP_MD_CTX_test_flags@PLT
	testl	%eax, %eax
	je	.L39
.L30:
	movq	8(%r12), %rdi
	call	ENGINE_finish@PLT
	movl	$48, %esi
	movq	%r12, %rdi
	call	OPENSSL_cleanse@PLT
.L24:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$57, %edx
	popq	%r12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	cmpq	$0, 24(%r12)
	je	.L26
	movl	$4, %esi
	movq	%r12, %rdi
	call	EVP_MD_CTX_test_flags@PLT
	testl	%eax, %eax
	jne	.L26
	movq	(%r12), %rax
	movq	24(%r12), %rdi
	movl	$33, %ecx
	leaq	.LC0(%rip), %rdx
	movslq	68(%rax), %rsi
	call	CRYPTO_clear_free@PLT
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L39:
	movq	32(%r12), %rdi
	call	EVP_PKEY_CTX_free@PLT
	jmp	.L30
	.cfi_endproc
.LFE855:
	.size	EVP_MD_CTX_free, .-EVP_MD_CTX_free
	.p2align 4
	.globl	EVP_DigestInit_ex
	.type	EVP_DigestInit_ex, @function
EVP_DigestInit_ex:
.LFB857:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	movl	$2, %esi
	subq	$24, %rsp
	call	EVP_MD_CTX_clear_flags@PLT
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L41
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L42
	testq	%rbx, %rbx
	je	.L43
	movl	(%rax), %eax
	cmpl	%eax, (%rbx)
	je	.L43
.L44:
	call	ENGINE_finish@PLT
	testq	%r13, %r13
	je	.L46
	movq	%r13, %rdi
	call	ENGINE_init@PLT
	testl	%eax, %eax
	je	.L77
.L47:
	movl	(%rbx), %esi
	movq	%r13, %rdi
	call	ENGINE_get_digest@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L78
	movq	%r13, 8(%r12)
.L51:
	movq	(%r12), %rax
	cmpq	%rbx, %rax
	je	.L43
	testq	%rax, %rax
	je	.L52
	movslq	68(%rax), %rsi
	testl	%esi, %esi
	jne	.L79
.L52:
	movq	%rbx, (%r12)
	testb	$1, 17(%r12)
	jne	.L53
	movslq	68(%rbx), %rdi
	testl	%edi, %edi
	jne	.L80
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L55
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L42:
	testq	%rbx, %rbx
	jne	.L44
.L61:
	movl	$115, %r8d
	movl	$139, %edx
	movl	$128, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L40:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	testq	%rbx, %rbx
	jne	.L44
	cmpq	$0, (%r12)
	je	.L61
.L43:
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L59
.L55:
	movl	$248, %edx
	movq	%r12, %r9
	xorl	%r8d, %r8d
	movl	$7, %ecx
	movl	$-1, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	movl	%eax, %edx
	testl	%eax, %eax
	jg	.L59
	xorl	%eax, %eax
	cmpl	$-2, %edx
	jne	.L40
.L59:
	testb	$1, 17(%r12)
	jne	.L56
	movq	(%r12), %rbx
.L60:
	movq	24(%rbx), %rax
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L55
.L56:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	movl	(%rbx), %edi
	call	ENGINE_get_digest_engine@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L47
	movq	$0, 8(%r12)
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L79:
	movq	24(%r12), %rdi
	movl	$123, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movq	$0, 24(%r12)
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L80:
	movq	32(%rbx), %rax
	movl	$129, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rax, 40(%r12)
	call	CRYPTO_zalloc@PLT
	movq	%rax, 24(%r12)
	testq	%rax, %rax
	jne	.L43
	movl	$131, %r8d
	movl	$65, %edx
	movl	$128, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L77:
	movl	$88, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$134, %edx
	movl	%eax, -36(%rbp)
	movl	$128, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	movl	-36(%rbp), %eax
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L78:
	movl	$100, %r8d
	movl	$134, %edx
	movl	$128, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r13, %rdi
	call	ENGINE_finish@PLT
	xorl	%eax, %eax
	jmp	.L40
	.cfi_endproc
.LFE857:
	.size	EVP_DigestInit_ex, .-EVP_DigestInit_ex
	.p2align 4
	.globl	EVP_DigestInit
	.type	EVP_DigestInit, @function
EVP_DigestInit:
.LFB856:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	testq	%rdi, %rdi
	je	.L82
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L84
	cmpq	$0, 56(%rax)
	je	.L85
	movl	$2, %esi
	call	EVP_MD_CTX_test_flags@PLT
	testl	%eax, %eax
	jne	.L86
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*56(%rax)
.L86:
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L84
.L85:
	movl	68(%rax), %eax
	testl	%eax, %eax
	jne	.L96
.L84:
	movl	$1024, %esi
	movq	%r12, %rdi
	call	EVP_MD_CTX_test_flags@PLT
	testl	%eax, %eax
	je	.L97
.L88:
	movq	8(%r12), %rdi
	call	ENGINE_finish@PLT
	movl	$48, %esi
	movq	%r12, %rdi
	call	OPENSSL_cleanse@PLT
.L82:
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%edx, %edx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	EVP_DigestInit_ex
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	cmpq	$0, 24(%r12)
	je	.L84
	movl	$4, %esi
	movq	%r12, %rdi
	call	EVP_MD_CTX_test_flags@PLT
	testl	%eax, %eax
	jne	.L84
	movq	(%r12), %rax
	movq	24(%r12), %rdi
	movl	$33, %ecx
	leaq	.LC0(%rip), %rdx
	movslq	68(%rax), %rsi
	call	CRYPTO_clear_free@PLT
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L97:
	movq	32(%r12), %rdi
	call	EVP_PKEY_CTX_free@PLT
	jmp	.L88
	.cfi_endproc
.LFE856:
	.size	EVP_DigestInit, .-EVP_DigestInit
	.p2align 4
	.globl	EVP_DigestUpdate
	.type	EVP_DigestUpdate, @function
EVP_DigestUpdate:
.LFB858:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L99
	jmp	*40(%rdi)
	.p2align 4,,10
	.p2align 3
.L99:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE858:
	.size	EVP_DigestUpdate, .-EVP_DigestUpdate
	.section	.rodata.str1.8
	.align 8
.LC1:
	.string	"assertion failed: ctx->digest->md_size <= EVP_MAX_MD_SIZE"
	.text
	.p2align 4
	.globl	EVP_DigestFinal
	.type	EVP_DigestFinal, @function
EVP_DigestFinal:
.LFB859:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %rax
	cmpl	$64, 8(%rax)
	jg	.L109
	movq	%rdi, %r12
	movq	%rdx, %rbx
	call	*40(%rax)
	movl	%eax, %r13d
	movq	(%r12), %rax
	testq	%rbx, %rbx
	je	.L103
	movl	8(%rax), %edx
	movl	%edx, (%rbx)
.L103:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L104
	movq	%r12, %rdi
	call	*%rdx
	movl	$2, %esi
	movq	%r12, %rdi
	call	EVP_MD_CTX_set_flags@PLT
	movq	(%r12), %rax
.L104:
	movq	24(%r12), %rdi
	movslq	68(%rax), %rsi
	call	OPENSSL_cleanse@PLT
	movq	%r12, %rdi
	call	EVP_MD_CTX_reset
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L109:
	.cfi_restore_state
	movl	$173, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE859:
	.size	EVP_DigestFinal, .-EVP_DigestFinal
	.p2align 4
	.globl	EVP_DigestFinal_ex
	.type	EVP_DigestFinal_ex, @function
EVP_DigestFinal_ex:
.LFB860:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %rax
	cmpl	$64, 8(%rax)
	jg	.L119
	movq	%rdi, %rbx
	movq	%rdx, %r12
	call	*40(%rax)
	movl	%eax, %r13d
	movq	(%rbx), %rax
	testq	%r12, %r12
	je	.L113
	movl	8(%rax), %edx
	movl	%edx, (%r12)
.L113:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L114
	movq	%rbx, %rdi
	call	*%rdx
	movl	$2, %esi
	movq	%rbx, %rdi
	call	EVP_MD_CTX_set_flags@PLT
	movq	(%rbx), %rax
.L114:
	movslq	68(%rax), %rsi
	movq	24(%rbx), %rdi
	call	OPENSSL_cleanse@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L119:
	.cfi_restore_state
	movl	$173, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE860:
	.size	EVP_DigestFinal_ex, .-EVP_DigestFinal_ex
	.p2align 4
	.globl	EVP_DigestFinalXOF
	.type	EVP_DigestFinalXOF, @function
EVP_DigestFinalXOF:
.LFB861:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	testb	$2, 16(%rax)
	je	.L121
	cmpq	$2147483647, %rdx
	ja	.L121
	movq	%rsi, %r12
	xorl	%ecx, %ecx
	movq	%rdi, %rbx
	movl	$3, %esi
	call	*72(%rax)
	testl	%eax, %eax
	je	.L121
	movq	(%rbx), %rax
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	*40(%rax)
	movl	%eax, %r12d
	movq	(%rbx), %rax
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L122
	movq	%rbx, %rdi
	call	*%rdx
	movl	$2, %esi
	movq	%rbx, %rdi
	call	EVP_MD_CTX_set_flags@PLT
	movq	(%rbx), %rax
.L122:
	movslq	68(%rax), %rsi
	movq	24(%rbx), %rdi
	call	OPENSSL_cleanse@PLT
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	xorl	%r12d, %r12d
	movl	$200, %r8d
	movl	$178, %edx
	movl	$174, %esi
	leaq	.LC0(%rip), %rcx
	movl	$6, %edi
	call	ERR_put_error@PLT
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE861:
	.size	EVP_DigestFinalXOF, .-EVP_DigestFinalXOF
	.p2align 4
	.globl	EVP_MD_CTX_copy
	.type	EVP_MD_CTX_copy, @function
EVP_MD_CTX_copy:
.LFB862:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$24, %rsp
	call	EVP_MD_CTX_reset
	testq	%r13, %r13
	je	.L138
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L138
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L141
	call	ENGINE_init@PLT
	testl	%eax, %eax
	je	.L142
	movq	0(%r13), %rax
.L141:
	xorl	%r14d, %r14d
	cmpq	%rax, (%r12)
	je	.L164
.L143:
	movq	%r12, %rdi
	call	EVP_MD_CTX_reset
	movdqu	0(%r13), %xmm1
	movl	$1024, %esi
	movq	%r12, %rdi
	movups	%xmm1, (%r12)
	movdqu	16(%r13), %xmm2
	movups	%xmm2, 16(%r12)
	movdqu	32(%r13), %xmm3
	movups	%xmm3, 32(%r12)
	call	EVP_MD_CTX_clear_flags@PLT
	pxor	%xmm0, %xmm0
	movups	%xmm0, 24(%r12)
	cmpq	$0, 24(%r13)
	je	.L144
	movq	(%r12), %rax
	movl	68(%rax), %eax
	testl	%eax, %eax
	je	.L144
	movslq	%eax, %r8
	testq	%r14, %r14
	je	.L145
	movq	%r14, 24(%r12)
.L146:
	movq	24(%r13), %rsi
	movq	%r8, %rdx
	movq	%r14, %rdi
	call	memcpy@PLT
.L144:
	movq	40(%r13), %rax
	movq	32(%r13), %rdi
	movq	%rax, 40(%r12)
	testq	%rdi, %rdi
	je	.L148
	call	EVP_PKEY_CTX_dup@PLT
	movq	%rax, 32(%r12)
	testq	%rax, %rax
	je	.L165
.L148:
	movq	(%r12), %rax
	movq	48(%rax), %rax
	testq	%rax, %rax
	je	.L150
	addq	$24, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L150:
	.cfi_restore_state
	movl	$1, %eax
.L137:
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	movl	$216, %r8d
	movl	$111, %edx
	movl	$110, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	movl	$4, %esi
	movq	%r12, %rdi
	movq	24(%r12), %r14
	call	EVP_MD_CTX_set_flags@PLT
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L142:
	movl	$222, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$38, %edx
	movl	%eax, -36(%rbp)
	movl	$110, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	movl	-36(%rbp), %eax
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L145:
	movl	$249, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r8, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, 24(%r12)
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L166
	movq	(%r12), %rax
	movslq	68(%rax), %r8
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L165:
	movq	%r12, %rdi
	call	EVP_MD_CTX_reset
	xorl	%eax, %eax
	jmp	.L137
.L166:
	movl	$251, %r8d
	movl	$65, %edx
	movl	$110, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L137
	.cfi_endproc
.LFE862:
	.size	EVP_MD_CTX_copy, .-EVP_MD_CTX_copy
	.p2align 4
	.globl	EVP_MD_CTX_copy_ex
	.type	EVP_MD_CTX_copy_ex, @function
EVP_MD_CTX_copy_ex:
.LFB863:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	testq	%rsi, %rsi
	je	.L168
	movq	(%rsi), %rax
	movq	%rsi, %r13
	testq	%rax, %rax
	je	.L168
	movq	%rdi, %r12
	movq	8(%rsi), %rdi
	testq	%rdi, %rdi
	je	.L171
	call	ENGINE_init@PLT
	testl	%eax, %eax
	je	.L172
	movq	0(%r13), %rax
.L171:
	xorl	%r14d, %r14d
	cmpq	%rax, (%r12)
	je	.L194
.L173:
	movq	%r12, %rdi
	call	EVP_MD_CTX_reset
	movdqu	0(%r13), %xmm1
	movl	$1024, %esi
	movq	%r12, %rdi
	movups	%xmm1, (%r12)
	movdqu	16(%r13), %xmm2
	movups	%xmm2, 16(%r12)
	movdqu	32(%r13), %xmm3
	movups	%xmm3, 32(%r12)
	call	EVP_MD_CTX_clear_flags@PLT
	pxor	%xmm0, %xmm0
	movups	%xmm0, 24(%r12)
	cmpq	$0, 24(%r13)
	je	.L174
	movq	(%r12), %rax
	movl	68(%rax), %eax
	testl	%eax, %eax
	je	.L174
	movslq	%eax, %r8
	testq	%r14, %r14
	je	.L175
	movq	%r14, 24(%r12)
.L176:
	movq	24(%r13), %rsi
	movq	%r8, %rdx
	movq	%r14, %rdi
	call	memcpy@PLT
.L174:
	movq	40(%r13), %rax
	movq	32(%r13), %rdi
	movq	%rax, 40(%r12)
	testq	%rdi, %rdi
	je	.L178
	call	EVP_PKEY_CTX_dup@PLT
	movq	%rax, 32(%r12)
	testq	%rax, %rax
	je	.L195
.L178:
	movq	(%r12), %rax
	movq	48(%rax), %rax
	testq	%rax, %rax
	je	.L180
	addq	$24, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L180:
	.cfi_restore_state
	movl	$1, %eax
.L167:
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L168:
	.cfi_restore_state
	movl	$216, %r8d
	movl	$111, %edx
	movl	$110, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L194:
	.cfi_restore_state
	movl	$4, %esi
	movq	%r12, %rdi
	movq	24(%r12), %r14
	call	EVP_MD_CTX_set_flags@PLT
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L172:
	movl	$222, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$38, %edx
	movl	%eax, -36(%rbp)
	movl	$110, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	movl	-36(%rbp), %eax
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L175:
	movl	$249, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r8, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, 24(%r12)
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L196
	movq	(%r12), %rax
	movslq	68(%rax), %r8
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L195:
	movq	%r12, %rdi
	call	EVP_MD_CTX_reset
	xorl	%eax, %eax
	jmp	.L167
.L196:
	movl	$251, %r8d
	movl	$65, %edx
	movl	$110, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L167
	.cfi_endproc
.LFE863:
	.size	EVP_MD_CTX_copy_ex, .-EVP_MD_CTX_copy_ex
	.p2align 4
	.globl	EVP_Digest
	.type	EVP_Digest, @function
EVP_Digest:
.LFB864:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	leaq	.LC0(%rip), %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	movq	%rdi, -64(%rbp)
	movl	$48, %edi
	movq	%rdx, -56(%rbp)
	movl	$51, %edx
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L209
	movq	%rax, %r15
	movl	$1, %esi
	movq	%rax, %rdi
	call	EVP_MD_CTX_set_flags@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	EVP_DigestInit_ex
	testl	%eax, %eax
	jne	.L199
.L205:
	xorl	%r12d, %r12d
.L200:
	movq	%r15, %rdi
	call	EVP_MD_CTX_reset
	movl	$57, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	call	CRYPTO_free@PLT
.L197:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L199:
	.cfi_restore_state
	testq	%r14, %r14
	je	.L204
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	*40(%r15)
	testl	%eax, %eax
	je	.L205
.L204:
	movq	(%r15), %rax
	cmpl	$64, 8(%rax)
	jg	.L215
	movq	-56(%rbp), %rsi
	movq	%r15, %rdi
	call	*40(%rax)
	movl	%eax, %r12d
	movq	(%r15), %rax
	testq	%rbx, %rbx
	je	.L207
	movl	8(%rax), %edx
	movl	%edx, (%rbx)
.L207:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L208
	movq	%r15, %rdi
	call	*%rdx
	movl	$2, %esi
	movq	%r15, %rdi
	call	EVP_MD_CTX_set_flags@PLT
	movq	(%r15), %rax
.L208:
	movslq	68(%rax), %rsi
	movq	24(%r15), %rdi
	call	OPENSSL_cleanse@PLT
	testl	%r12d, %r12d
	setne	%r12b
	movzbl	%r12b, %r12d
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L209:
	xorl	%r12d, %r12d
	jmp	.L197
.L215:
	movl	$173, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE864:
	.size	EVP_Digest, .-EVP_Digest
	.p2align 4
	.globl	EVP_MD_CTX_ctrl
	.type	EVP_MD_CTX_ctrl, @function
EVP_MD_CTX_ctrl:
.LFB865:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	xorl	%r8d, %r8d
	testq	%rax, %rax
	je	.L223
	movq	72(%rax), %rax
	testq	%rax, %rax
	je	.L223
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	*%rax
	xorl	%r8d, %r8d
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	setg	%r8b
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	.cfi_restore 6
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE865:
	.size	EVP_MD_CTX_ctrl, .-EVP_MD_CTX_ctrl
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
