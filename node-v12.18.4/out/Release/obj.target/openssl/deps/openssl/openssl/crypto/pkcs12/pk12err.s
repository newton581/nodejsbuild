	.file	"pk12err.c"
	.text
	.p2align 4
	.globl	ERR_load_PKCS12_strings
	.type	ERR_load_PKCS12_strings, @function
ERR_load_PKCS12_strings:
.LFB203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$587698176, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_func_error_string@PLT
	testq	%rax, %rax
	je	.L5
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	leaq	PKCS12_str_functs(%rip), %rdi
	call	ERR_load_strings_const@PLT
	leaq	PKCS12_str_reasons(%rip), %rdi
	call	ERR_load_strings_const@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE203:
	.size	ERR_load_PKCS12_strings, .-ERR_load_PKCS12_strings
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"cant pack structure"
.LC1:
	.string	"content type not data"
.LC2:
	.string	"decode error"
.LC3:
	.string	"encode error"
.LC4:
	.string	"encrypt error"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"error setting encrypted data type"
	.section	.rodata.str1.1
.LC6:
	.string	"invalid null argument"
.LC7:
	.string	"invalid null pkcs12 pointer"
.LC8:
	.string	"iv gen error"
.LC9:
	.string	"key gen error"
.LC10:
	.string	"mac absent"
.LC11:
	.string	"mac generation error"
.LC12:
	.string	"mac setup error"
.LC13:
	.string	"mac string set error"
.LC14:
	.string	"mac verify failure"
.LC15:
	.string	"parse error"
.LC16:
	.string	"pkcs12 algor cipherinit error"
.LC17:
	.string	"pkcs12 cipherfinal error"
.LC18:
	.string	"pkcs12 pbe crypt error"
.LC19:
	.string	"unknown digest algorithm"
.LC20:
	.string	"unsupported pkcs12 mode"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	PKCS12_str_reasons, @object
	.size	PKCS12_str_reasons, 352
PKCS12_str_reasons:
	.quad	587202660
	.quad	.LC0
	.quad	587202681
	.quad	.LC1
	.quad	587202661
	.quad	.LC2
	.quad	587202662
	.quad	.LC3
	.quad	587202663
	.quad	.LC4
	.quad	587202680
	.quad	.LC5
	.quad	587202664
	.quad	.LC6
	.quad	587202665
	.quad	.LC7
	.quad	587202666
	.quad	.LC8
	.quad	587202667
	.quad	.LC9
	.quad	587202668
	.quad	.LC10
	.quad	587202669
	.quad	.LC11
	.quad	587202670
	.quad	.LC12
	.quad	587202671
	.quad	.LC13
	.quad	587202673
	.quad	.LC14
	.quad	587202674
	.quad	.LC15
	.quad	587202675
	.quad	.LC16
	.quad	587202676
	.quad	.LC17
	.quad	587202677
	.quad	.LC18
	.quad	587202678
	.quad	.LC19
	.quad	587202679
	.quad	.LC20
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC21:
	.string	"OPENSSL_asc2uni"
.LC22:
	.string	"OPENSSL_uni2asc"
.LC23:
	.string	"OPENSSL_uni2utf8"
.LC24:
	.string	"OPENSSL_utf82uni"
.LC25:
	.string	"PKCS12_create"
.LC26:
	.string	"PKCS12_gen_mac"
.LC27:
	.string	"PKCS12_init"
.LC28:
	.string	"PKCS12_item_decrypt_d2i"
.LC29:
	.string	"PKCS12_item_i2d_encrypt"
.LC30:
	.string	"PKCS12_item_pack_safebag"
.LC31:
	.string	"PKCS12_key_gen_asc"
.LC32:
	.string	"PKCS12_key_gen_uni"
.LC33:
	.string	"PKCS12_key_gen_utf8"
.LC34:
	.string	"PKCS12_newpass"
.LC35:
	.string	"PKCS12_pack_p7data"
.LC36:
	.string	"PKCS12_pack_p7encdata"
.LC37:
	.string	"PKCS12_parse"
.LC38:
	.string	"PKCS12_pbe_crypt"
.LC39:
	.string	"PKCS12_PBE_keyivgen"
.LC40:
	.string	"PKCS12_SAFEBAG_create0_p8inf"
.LC41:
	.string	"PKCS12_SAFEBAG_create0_pkcs8"
	.section	.rodata.str1.8
	.align 8
.LC42:
	.string	"PKCS12_SAFEBAG_create_pkcs8_encrypt"
	.section	.rodata.str1.1
.LC43:
	.string	"PKCS12_setup_mac"
.LC44:
	.string	"PKCS12_set_mac"
.LC45:
	.string	"PKCS12_unpack_authsafes"
.LC46:
	.string	"PKCS12_unpack_p7data"
.LC47:
	.string	"PKCS12_verify_mac"
.LC48:
	.string	"PKCS8_encrypt"
.LC49:
	.string	"PKCS8_set0_pbe"
	.section	.data.rel.ro.local
	.align 32
	.type	PKCS12_str_functs, @object
	.size	PKCS12_str_functs, 480
PKCS12_str_functs:
	.quad	587698176
	.quad	.LC21
	.quad	587710464
	.quad	.LC22
	.quad	587722752
	.quad	.LC23
	.quad	587730944
	.quad	.LC24
	.quad	587632640
	.quad	.LC25
	.quad	587640832
	.quad	.LC26
	.quad	587649024
	.quad	.LC27
	.quad	587636736
	.quad	.LC28
	.quad	587644928
	.quad	.LC29
	.quad	587681792
	.quad	.LC30
	.quad	587653120
	.quad	.LC31
	.quad	587657216
	.quad	.LC32
	.quad	587677696
	.quad	.LC33
	.quad	587726848
	.quad	.LC34
	.quad	587669504
	.quad	.LC35
	.quad	587673600
	.quad	.LC36
	.quad	587685888
	.quad	.LC37
	.quad	587689984
	.quad	.LC38
	.quad	587694080
	.quad	.LC39
	.quad	587661312
	.quad	.LC40
	.quad	587665408
	.quad	.LC41
	.quad	587747328
	.quad	.LC42
	.quad	587702272
	.quad	.LC43
	.quad	587706368
	.quad	.LC44
	.quad	587735040
	.quad	.LC45
	.quad	587739136
	.quad	.LC46
	.quad	587718656
	.quad	.LC47
	.quad	587714560
	.quad	.LC48
	.quad	587743232
	.quad	.LC49
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
