	.file	"bn_x931p.c"
	.text
	.p2align 4
	.type	bn_x931_derive_pi, @function
bn_x931_derive_pi:
.LFB141:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L6
	movq	%rbx, %rdi
	call	BN_is_odd@PLT
	testl	%eax, %eax
	je	.L4
.L7:
	xorl	%r12d, %r12d
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L17:
	jne	.L8
	movl	$2, %esi
	movq	%rbx, %rdi
	call	BN_add_word@PLT
	testl	%eax, %eax
	je	.L6
.L5:
	addl	$1, %r12d
	xorl	%esi, %esi
	movq	%r13, %rdi
	movl	%r12d, %edx
	call	BN_GENCB_call@PLT
	movq	%r13, %r8
	movl	$1, %ecx
	movq	%r14, %rdx
	movl	$27, %esi
	movq	%rbx, %rdi
	call	BN_is_prime_fasttest_ex@PLT
	testl	%eax, %eax
	jns	.L17
.L6:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	movl	$1, %esi
	movq	%rbx, %rdi
	call	BN_add_word@PLT
	testl	%eax, %eax
	jne	.L7
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L8:
	movl	%r12d, %edx
	movq	%r13, %rdi
	movl	$2, %esi
	call	BN_GENCB_call@PLT
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE141:
	.size	bn_x931_derive_pi, .-bn_x931_derive_pi
	.p2align 4
	.type	BN_X931_derive_prime_ex.part.0, @function
BN_X931_derive_prime_ex.part.0:
.LFB146:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rbp), %r12
	movq	%rdi, -72(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%r12, %rdi
	movq	%r9, -56(%rbp)
	call	BN_CTX_start@PLT
	testq	%r13, %r13
	je	.L81
	testq	%r14, %r14
	je	.L82
.L20:
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	BN_CTX_get@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L23
	movq	32(%rbp), %rcx
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	bn_x931_derive_pi
	testl	%eax, %eax
	jne	.L83
.L23:
	xorl	%r13d, %r13d
.L22:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	addq	$56, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	movq	32(%rbp), %rcx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	bn_x931_derive_pi
	testl	%eax, %eax
	je	.L23
	movq	-64(%rbp), %rdi
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	BN_mul@PLT
	testl	%eax, %eax
	je	.L23
	movq	-72(%rbp), %r15
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	BN_mod_inverse@PLT
	testq	%rax, %rax
	je	.L23
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	BN_mul@PLT
	testl	%eax, %eax
	je	.L23
	movq	%r14, %rdx
	movq	-80(%rbp), %r14
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	BN_mod_inverse@PLT
	testq	%rax, %rax
	je	.L23
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	BN_mul@PLT
	testl	%eax, %eax
	je	.L23
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	BN_sub@PLT
	testl	%eax, %eax
	je	.L23
	movl	16(%r15), %eax
	testl	%eax, %eax
	je	.L26
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rdx
	movq	%rsi, %rdi
	call	BN_add@PLT
	testl	%eax, %eax
	je	.L23
.L26:
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rcx
	movq	%r12, %r8
	movq	-88(%rbp), %rdx
	movq	%rsi, %rdi
	call	BN_mod_sub@PLT
	testl	%eax, %eax
	je	.L23
	movq	-88(%rbp), %rdx
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L28:
	movq	32(%rbp), %rdi
	xorl	%esi, %esi
	movl	$1, %edx
	call	BN_GENCB_call@PLT
	movq	-72(%rbp), %rsi
	movq	%rbx, %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L23
	movl	$1, %esi
	movq	%rbx, %rdi
	call	BN_sub_word@PLT
	testl	%eax, %eax
	je	.L23
	movq	-80(%rbp), %r15
	movq	16(%rbp), %rdx
	movq	%r12, %rcx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	BN_gcd@PLT
	testl	%eax, %eax
	je	.L23
	movq	%r15, %rdi
	call	BN_is_one@PLT
	testl	%eax, %eax
	je	.L29
	movq	32(%rbp), %r8
	movq	-72(%rbp), %rdi
	movl	$1, %ecx
	movq	%r12, %rdx
	movl	$50, %esi
	call	BN_is_prime_fasttest_ex@PLT
	testl	%eax, %eax
	js	.L23
	jne	.L84
.L29:
	movq	-64(%rbp), %rdx
.L80:
	movq	-72(%rbp), %rsi
	movq	%rsi, %rdi
	call	BN_add@PLT
	testl	%eax, %eax
	jne	.L28
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L81:
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, %r13
	testq	%r14, %r14
	jne	.L20
	.p2align 4,,10
	.p2align 3
.L82:
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, %r14
	jmp	.L20
.L84:
	movq	32(%rbp), %rdi
	xorl	%edx, %edx
	movl	$3, %esi
	movl	$1, %r13d
	call	BN_GENCB_call@PLT
	jmp	.L22
	.cfi_endproc
.LFE146:
	.size	BN_X931_derive_prime_ex.part.0, .-BN_X931_derive_prime_ex.part.0
	.p2align 4
	.globl	BN_X931_derive_prime_ex
	.type	BN_X931_derive_prime_ex, @function
BN_X931_derive_prime_ex:
.LFB142:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %rbx
	movq	24(%rbp), %rax
	movq	%rcx, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	32(%rbp), %r15
	movq	%rbx, %rdi
	movq	%r9, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	BN_is_odd@PLT
	testl	%eax, %eax
	jne	.L88
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	movq	-56(%rbp), %rax
	movq	%r15, 32(%rbp)
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	movq	%rbx, 16(%rbp)
	movq	%r12, %rdi
	movq	%rax, 24(%rbp)
	movq	-80(%rbp), %rcx
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	BN_X931_derive_prime_ex.part.0
	.cfi_endproc
.LFE142:
	.size	BN_X931_derive_prime_ex, .-BN_X931_derive_prime_ex
	.p2align 4
	.globl	BN_X931_generate_Xpq
	.type	BN_X931_generate_Xpq, @function
BN_X931_generate_Xpq:
.LFB143:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -64(%rbp)
	cmpl	$1023, %edx
	jle	.L89
	movl	%edx, %r15d
	movl	%edx, %ebx
	andl	$255, %r15d
	je	.L112
	xorl	%r15d, %r15d
.L89:
	addq	$24, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	sarl	%ebx
	movq	%rsi, %r12
	movq	%rcx, %r14
	movl	$1, %edx
	xorl	%ecx, %ecx
	movl	%ebx, %esi
	movq	%rdi, %r13
	call	BN_priv_rand@PLT
	testl	%eax, %eax
	jne	.L113
.L110:
	movq	-64(%rbp), %rdi
	call	BN_CTX_end@PLT
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L113:
	movq	%r14, %rdi
	call	BN_CTX_start@PLT
	movq	%r14, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L110
	leal	-99(%rbx), %eax
	movl	$1000, -56(%rbp)
	movl	%eax, -52(%rbp)
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L114:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	BN_sub@PLT
	testl	%eax, %eax
	je	.L110
	movq	%r14, %rdi
	call	BN_num_bits@PLT
	cmpl	%eax, -52(%rbp)
	jle	.L92
	subl	$1, -56(%rbp)
	je	.L110
.L93:
	xorl	%ecx, %ecx
	movl	$1, %edx
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	BN_priv_rand@PLT
	testl	%eax, %eax
	jne	.L114
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L92:
	movq	-64(%rbp), %rdi
	movl	$1, %r15d
	call	BN_CTX_end@PLT
	jmp	.L89
	.cfi_endproc
.LFE143:
	.size	BN_X931_generate_Xpq, .-BN_X931_generate_Xpq
	.p2align 4
	.globl	BN_X931_generate_prime_ex
	.type	BN_X931_generate_prime_ex, @function
BN_X931_generate_prime_ex:
.LFB144:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	movq	24(%rbp), %r13
	movq	%rdi, -56(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%r13, %rdi
	call	BN_CTX_start@PLT
	testq	%rbx, %rbx
	je	.L140
	testq	%r12, %r12
	je	.L122
.L118:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$101, %esi
	movq	%rbx, %rdi
	call	BN_priv_rand@PLT
	testl	%eax, %eax
	je	.L121
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$101, %esi
	movq	%r12, %rdi
	call	BN_priv_rand@PLT
	testl	%eax, %eax
	jne	.L141
	.p2align 4,,10
	.p2align 3
.L121:
	xorl	%r12d, %r12d
.L120:
	movq	%r13, %rdi
	call	BN_CTX_end@PLT
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	movq	%r13, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, %rbx
	testq	%r12, %r12
	jne	.L117
	.p2align 4,,10
	.p2align 3
.L122:
	movq	%r13, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, %r12
.L117:
	testq	%rbx, %rbx
	je	.L121
	testq	%r12, %r12
	je	.L121
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L141:
	movq	16(%rbp), %rdi
	call	BN_is_odd@PLT
	testl	%eax, %eax
	je	.L121
	subq	$8, %rsp
	pushq	32(%rbp)
	movq	-64(%rbp), %rsi
	movq	%r12, %r9
	pushq	%r13
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	%r14, %rdx
	pushq	16(%rbp)
	movq	-56(%rbp), %rdi
	xorl	%r12d, %r12d
	call	BN_X931_derive_prime_ex.part.0
	addq	$32, %rsp
	testl	%eax, %eax
	setne	%r12b
	jmp	.L120
	.cfi_endproc
.LFE144:
	.size	BN_X931_generate_prime_ex, .-BN_X931_generate_prime_ex
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
