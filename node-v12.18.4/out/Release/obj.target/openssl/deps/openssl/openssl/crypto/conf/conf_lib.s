	.file	"conf_lib.c"
	.text
	.p2align 4
	.type	default_to_int, @function
default_to_int:
.LFB330:
	.cfi_startproc
	endbr64
	movsbl	%sil, %eax
	subl	$48, %eax
	ret
	.cfi_endproc
.LFE330:
	.size	default_to_int, .-default_to_int
	.p2align 4
	.type	default_is_number, @function
default_is_number:
.LFB329:
	.cfi_startproc
	endbr64
	movsbl	%sil, %edi
	movl	$4, %esi
	jmp	ossl_ctype_check@PLT
	.cfi_endproc
.LFE329:
	.size	default_is_number, .-default_is_number
	.p2align 4
	.globl	CONF_set_nconf
	.type	CONF_set_nconf, @function
CONF_set_nconf:
.LFB310:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	default_CONF_method(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%rax, %rax
	je	.L7
	movq	%rbx, %rdi
	call	*16(%rax)
	movq	%r12, 16(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	call	NCONF_default@PLT
	movq	%rbx, %rdi
	movq	%rax, default_CONF_method(%rip)
	call	*16(%rax)
	movq	%r12, 16(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE310:
	.size	CONF_set_nconf, .-CONF_set_nconf
	.p2align 4
	.globl	CONF_set_default_method
	.type	CONF_set_default_method, @function
CONF_set_default_method:
.LFB311:
	.cfi_startproc
	endbr64
	movq	%rdi, default_CONF_method(%rip)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE311:
	.size	CONF_set_default_method, .-CONF_set_default_method
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"rb"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../deps/openssl/openssl/crypto/conf/conf_lib.c"
	.text
	.p2align 4
	.globl	CONF_load
	.type	CONF_load, @function
CONF_load:
.LFB312:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	leaq	.LC0(%rip), %rsi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	BIO_new_file@PLT
	testq	%rax, %rax
	je	.L17
	movq	%rax, %r12
	movq	default_CONF_method(%rip), %rax
	testq	%rax, %rax
	je	.L18
.L12:
	leaq	-64(%rbp), %r14
	movq	%r14, %rdi
	call	*16(%rax)
	movq	-64(%rbp), %rax
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rbx, -48(%rbp)
	movq	%r12, %rsi
	movl	$0, %r13d
	call	*40(%rax)
	movq	%r12, %rdi
	testl	%eax, %eax
	cmovne	-48(%rbp), %r13
	call	BIO_free@PLT
.L9:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L19
	addq	$32, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	call	NCONF_default@PLT
	movq	%rax, default_CONF_method(%rip)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L17:
	movl	$57, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$2, %edx
	xorl	%r13d, %r13d
	movl	$100, %esi
	movl	$14, %edi
	call	ERR_put_error@PLT
	jmp	.L9
.L19:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE312:
	.size	CONF_load, .-CONF_load
	.p2align 4
	.globl	CONF_load_fp
	.type	CONF_load_fp, @function
CONF_load_fp:
.LFB313:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	xorl	%esi, %esi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	BIO_new_fp@PLT
	testq	%rax, %rax
	je	.L28
	movq	%rax, %r12
	movq	default_CONF_method(%rip), %rax
	testq	%rax, %rax
	je	.L29
.L23:
	leaq	-64(%rbp), %r14
	movq	%r14, %rdi
	call	*16(%rax)
	movq	-64(%rbp), %rax
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rbx, -48(%rbp)
	movq	%r12, %rsi
	movl	$0, %r13d
	call	*40(%rax)
	movq	%r12, %rdi
	testl	%eax, %eax
	cmovne	-48(%rbp), %r13
	call	BIO_free@PLT
.L20:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L30
	addq	$32, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	call	NCONF_default@PLT
	movq	%rax, default_CONF_method(%rip)
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L28:
	movl	$74, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$7, %edx
	xorl	%r13d, %r13d
	movl	$103, %esi
	movl	$14, %edi
	call	ERR_put_error@PLT
	jmp	.L20
.L30:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE313:
	.size	CONF_load_fp, .-CONF_load_fp
	.p2align 4
	.globl	CONF_load_bio
	.type	CONF_load_bio, @function
CONF_load_bio:
.LFB314:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	default_CONF_method(%rip), %rax
	testq	%rax, %rax
	je	.L37
.L32:
	leaq	-64(%rbp), %r14
	movq	%r14, %rdi
	call	*16(%rax)
	movq	-64(%rbp), %rax
	movq	%rbx, -48(%rbp)
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	*40(%rax)
	testl	%eax, %eax
	movl	$0, %eax
	cmovne	-48(%rbp), %rax
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L38
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	call	NCONF_default@PLT
	movq	%rax, default_CONF_method(%rip)
	jmp	.L32
.L38:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE314:
	.size	CONF_load_bio, .-CONF_load_bio
	.p2align 4
	.globl	CONF_get_section
	.type	CONF_get_section, @function
CONF_get_section:
.LFB315:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L39
	movq	default_CONF_method(%rip), %rax
	movq	%rdi, %rbx
	movq	%rsi, %r12
	testq	%rax, %rax
	je	.L48
.L41:
	leaq	-64(%rbp), %r13
	movq	%r13, %rdi
	call	*16(%rax)
	movq	%rbx, -48(%rbp)
	testq	%r12, %r12
	je	.L49
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_CONF_get_section_values@PLT
.L39:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L50
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	call	NCONF_default@PLT
	movq	%rax, default_CONF_method(%rip)
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L49:
	movl	$250, %r8d
	movl	$107, %edx
	movl	$108, %esi
	movl	$14, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L39
.L50:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE315:
	.size	CONF_get_section, .-CONF_get_section
	.section	.rodata.str1.1
.LC2:
	.string	" name="
.LC3:
	.string	"group="
	.text
	.p2align 4
	.globl	CONF_get_string
	.type	CONF_get_string, @function
CONF_get_string:
.LFB316:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	subq	$32, %rsp
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L58
	movq	default_CONF_method(%rip), %rax
	movq	%rdi, %r12
	testq	%rax, %rax
	je	.L59
.L54:
	leaq	-64(%rbp), %r15
	movq	%r15, %rdi
	call	*16(%rax)
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%r12, -48(%rbp)
	call	_CONF_get_string@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L60
.L51:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L61
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	movl	$273, %r8d
	movl	$108, %edx
	movl	$109, %esi
	movl	$14, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r14, %r8
	movq	%r13, %rdx
	movl	$4, %edi
	leaq	.LC2(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	xorl	%eax, %eax
	call	ERR_add_error_data@PLT
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L58:
	xorl	%edi, %edi
	call	_CONF_get_string@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L51
	movl	$269, %r8d
	movl	$106, %edx
	movl	$109, %esi
	movl	$14, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L59:
	call	NCONF_default@PLT
	movq	%rax, default_CONF_method(%rip)
	jmp	.L54
.L61:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE316:
	.size	CONF_get_string, .-CONF_get_string
	.p2align 4
	.globl	CONF_free
	.type	CONF_free, @function
CONF_free:
.LFB318:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	default_CONF_method(%rip), %rax
	testq	%rax, %rax
	je	.L66
.L63:
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	call	*16(%rax)
	movq	-48(%rbp), %rax
	movq	%rbx, -32(%rbp)
	movq	%r12, %rdi
	call	*32(%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L67
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	call	NCONF_default@PLT
	movq	%rax, default_CONF_method(%rip)
	jmp	.L63
.L67:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE318:
	.size	CONF_free, .-CONF_free
	.p2align 4
	.globl	CONF_dump_fp
	.type	CONF_dump_fp, @function
CONF_dump_fp:
.LFB319:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	xorl	%esi, %esi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	BIO_new_fp@PLT
	testq	%rax, %rax
	je	.L74
	movq	%rax, %r12
	movq	default_CONF_method(%rip), %rax
	testq	%rax, %rax
	je	.L75
.L71:
	leaq	-64(%rbp), %r13
	movq	%r13, %rdi
	call	*16(%rax)
	movq	-64(%rbp), %rax
	movq	%r13, %rdi
	movq	%rbx, -48(%rbp)
	movq	%r12, %rsi
	call	*48(%rax)
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	BIO_free@PLT
.L68:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L76
	addq	$40, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	movl	$153, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$7, %edx
	xorl	%r13d, %r13d
	movl	$104, %esi
	movl	$14, %edi
	call	ERR_put_error@PLT
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L75:
	call	NCONF_default@PLT
	movq	%rax, default_CONF_method(%rip)
	jmp	.L71
.L76:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE319:
	.size	CONF_dump_fp, .-CONF_dump_fp
	.p2align 4
	.globl	CONF_dump_bio
	.type	CONF_dump_bio, @function
CONF_dump_bio:
.LFB320:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	default_CONF_method(%rip), %rax
	testq	%rax, %rax
	je	.L81
.L78:
	leaq	-64(%rbp), %r13
	movq	%r13, %rdi
	call	*16(%rax)
	movq	-64(%rbp), %rax
	movq	%rbx, -48(%rbp)
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	*48(%rax)
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L82
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	call	NCONF_default@PLT
	movq	%rax, default_CONF_method(%rip)
	jmp	.L78
.L82:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE320:
	.size	CONF_dump_bio, .-CONF_dump_bio
	.p2align 4
	.globl	NCONF_new
	.type	NCONF_new, @function
NCONF_new:
.LFB321:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	testq	%rdi, %rdi
	je	.L87
.L84:
	call	*8(%rdi)
	testq	%rax, %rax
	je	.L88
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	call	NCONF_default@PLT
	movq	%rax, %rdi
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L88:
	movl	$186, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$65, %edx
	movl	$111, %esi
	movl	$14, %edi
	movq	%rax, -8(%rbp)
	call	ERR_put_error@PLT
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE321:
	.size	NCONF_new, .-NCONF_new
	.p2align 4
	.globl	NCONF_free
	.type	NCONF_free, @function
NCONF_free:
.LFB322:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L89
	movq	(%rdi), %rax
	jmp	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L89:
	ret
	.cfi_endproc
.LFE322:
	.size	NCONF_free, .-NCONF_free
	.p2align 4
	.globl	NCONF_free_data
	.type	NCONF_free_data, @function
NCONF_free_data:
.LFB323:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L91
	movq	(%rdi), %rax
	jmp	*32(%rax)
	.p2align 4,,10
	.p2align 3
.L91:
	ret
	.cfi_endproc
.LFE323:
	.size	NCONF_free_data, .-NCONF_free_data
	.p2align 4
	.globl	NCONF_load
	.type	NCONF_load, @function
NCONF_load:
.LFB324:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L97
	movq	(%rdi), %rax
	jmp	*72(%rax)
	.p2align 4,,10
	.p2align 3
.L97:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$105, %edx
	movl	$113, %esi
	movl	$14, %edi
	movl	$210, %r8d
	leaq	.LC1(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE324:
	.size	NCONF_load, .-NCONF_load
	.p2align 4
	.globl	NCONF_load_fp
	.type	NCONF_load_fp, @function
NCONF_load_fp:
.LFB325:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movq	%rsi, %rdi
	xorl	%esi, %esi
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	call	BIO_new_fp@PLT
	testq	%rax, %rax
	je	.L106
	movq	%rax, %r12
	testq	%r13, %r13
	je	.L107
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	*40(%rax)
	movl	%eax, %r13d
.L104:
	movq	%r12, %rdi
	call	BIO_free@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	movl	$223, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$7, %edx
	xorl	%r13d, %r13d
	movl	$114, %esi
	movl	$14, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	movl	$235, %r8d
	movl	$105, %edx
	movl	$110, %esi
	movl	$14, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L104
	.cfi_endproc
.LFE325:
	.size	NCONF_load_fp, .-NCONF_load_fp
	.p2align 4
	.globl	NCONF_load_bio
	.type	NCONF_load_bio, @function
NCONF_load_bio:
.LFB326:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L112
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.p2align 4,,10
	.p2align 3
.L112:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$105, %edx
	movl	$110, %esi
	movl	$14, %edi
	movl	$235, %r8d
	leaq	.LC1(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE326:
	.size	NCONF_load_bio, .-NCONF_load_bio
	.p2align 4
	.globl	NCONF_get_section
	.type	NCONF_get_section, @function
NCONF_get_section:
.LFB327:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rdi, %rdi
	je	.L120
	testq	%rsi, %rsi
	je	.L121
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_CONF_get_section_values@PLT
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	movl	$250, %r8d
	movl	$107, %edx
	movl	$108, %esi
	movl	$14, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
.L115:
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore_state
	movl	$245, %r8d
	movl	$105, %edx
	movl	$108, %esi
	movl	$14, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L115
	.cfi_endproc
.LFE327:
	.size	NCONF_get_section, .-NCONF_get_section
	.p2align 4
	.globl	NCONF_get_string
	.type	NCONF_get_string, @function
NCONF_get_string:
.LFB328:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	call	_CONF_get_string@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L126
.L122:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L126:
	.cfi_restore_state
	testq	%rbx, %rbx
	je	.L127
	movl	$273, %r8d
	movl	$108, %edx
	movl	$109, %esi
	movl	$14, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r14, %r8
	movq	%r13, %rdx
	xorl	%eax, %eax
	leaq	.LC2(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	movl	$4, %edi
	call	ERR_add_error_data@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	movl	$269, %r8d
	movl	$106, %edx
	movl	$109, %esi
	movl	$14, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L122
	.cfi_endproc
.LFE328:
	.size	NCONF_get_string, .-NCONF_get_string
	.p2align 4
	.globl	NCONF_get_number_e
	.type	NCONF_get_number_e, @function
NCONF_get_number_e:
.LFB331:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -56(%rbp)
	testq	%rcx, %rcx
	je	.L143
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rdx, %r14
	call	_CONF_get_string@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L144
	leaq	default_to_int(%rip), %r15
	leaq	default_is_number(%rip), %r14
	testq	%r13, %r13
	je	.L133
	movq	0(%r13), %rax
	leaq	default_is_number(%rip), %rdx
	movq	56(%rax), %r14
	movq	64(%rax), %r15
	leaq	default_to_int(%rip), %rax
	testq	%r14, %r14
	cmove	%rdx, %r14
	testq	%r15, %r15
	cmove	%rax, %r15
.L133:
	xorl	%r12d, %r12d
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L137:
	movsbl	(%rbx), %esi
	movq	%r13, %rdi
	call	*%r15
	movabsq	$9223372036854775807, %rdx
	movslq	%eax, %rcx
	movabsq	$-3689348814741910323, %rax
	subq	%rcx, %rdx
	mulq	%rdx
	shrq	$3, %rdx
	cmpq	%r12, %rdx
	jl	.L145
	leaq	(%r12,%r12,4), %rax
	addq	$1, %rbx
	leaq	(%rcx,%rax,2), %r12
.L135:
	movsbl	(%rbx), %esi
	movq	%r13, %rdi
	call	*%r14
	testl	%eax, %eax
	jne	.L137
	movq	-56(%rbp), %rax
	movq	%r12, (%rax)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L146
	movl	$273, %r8d
	movl	$108, %edx
	movl	$109, %esi
	movl	$14, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	movq	%r14, %r8
	movq	%r12, %rdx
	leaq	.LC2(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	movl	$4, %edi
	call	ERR_add_error_data@PLT
	xorl	%eax, %eax
.L128:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	movl	$316, %r8d
	movl	$121, %edx
	movl	$112, %esi
	movl	$14, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	movl	$297, %r8d
	movl	$67, %edx
	movl	$112, %esi
	movl	$14, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_restore_state
	movl	$269, %r8d
	movl	$106, %edx
	movl	$109, %esi
	movl	$14, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L128
	.cfi_endproc
.LFE331:
	.size	NCONF_get_number_e, .-NCONF_get_number_e
	.p2align 4
	.globl	CONF_get_number
	.type	CONF_get_number, @function
CONF_get_number:
.LFB317:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -72(%rbp)
	call	ERR_set_mark@PLT
	testq	%rbx, %rbx
	je	.L155
	movq	default_CONF_method(%rip), %rax
	testq	%rax, %rax
	je	.L156
.L150:
	leaq	-64(%rbp), %r14
	movq	%r14, %rdi
	call	*16(%rax)
	leaq	-72(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rbx, -48(%rbp)
	call	NCONF_get_number_e
	movl	%eax, %ebx
.L149:
	call	ERR_pop_to_mark@PLT
	testl	%ebx, %ebx
	movl	$0, %eax
	cmovne	-72(%rbp), %rax
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L157
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L155:
	.cfi_restore_state
	leaq	-72(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	xorl	%edi, %edi
	call	NCONF_get_number_e
	movl	%eax, %ebx
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L156:
	call	NCONF_default@PLT
	movq	%rax, default_CONF_method(%rip)
	jmp	.L150
.L157:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE317:
	.size	CONF_get_number, .-CONF_get_number
	.p2align 4
	.globl	NCONF_dump_fp
	.type	NCONF_dump_fp, @function
NCONF_dump_fp:
.LFB332:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -32
	xorl	%esi, %esi
	call	BIO_new_fp@PLT
	testq	%rax, %rax
	je	.L164
	movq	%rax, %r12
	testq	%r13, %r13
	je	.L165
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	*48(%rax)
	movl	%eax, %r13d
.L162:
	movq	%r12, %rdi
	call	BIO_free@PLT
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	xorl	%r13d, %r13d
	movl	$332, %r8d
	movl	$7, %edx
	movl	$106, %esi
	leaq	.LC1(%rip), %rcx
	movl	$14, %edi
	call	ERR_put_error@PLT
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	.cfi_restore_state
	movl	$344, %r8d
	movl	$105, %edx
	movl	$105, %esi
	movl	$14, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L162
	.cfi_endproc
.LFE332:
	.size	NCONF_dump_fp, .-NCONF_dump_fp
	.p2align 4
	.globl	NCONF_dump_bio
	.type	NCONF_dump_bio, @function
NCONF_dump_bio:
.LFB333:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L170
	movq	(%rdi), %rax
	jmp	*48(%rax)
	.p2align 4,,10
	.p2align 3
.L170:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$105, %edx
	movl	$105, %esi
	movl	$14, %edi
	movl	$344, %r8d
	leaq	.LC1(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE333:
	.size	NCONF_dump_bio, .-NCONF_dump_bio
	.p2align 4
	.globl	OPENSSL_INIT_new
	.type	OPENSSL_INIT_new, @function
OPENSSL_INIT_new:
.LFB334:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	calloc@PLT
	testq	%rax, %rax
	je	.L173
	movq	$50, 16(%rax)
.L173:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE334:
	.size	OPENSSL_INIT_new, .-OPENSSL_INIT_new
	.p2align 4
	.globl	OPENSSL_INIT_set_config_filename
	.type	OPENSSL_INIT_set_config_filename, @function
OPENSSL_INIT_set_config_filename:
.LFB335:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	je	.L180
	movq	%rsi, %rdi
	call	strdup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L182
.L180:
	movq	(%rbx), %rdi
	call	free@PLT
	movq	%r12, (%rbx)
	movl	$1, %eax
.L179:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L182:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L179
	.cfi_endproc
.LFE335:
	.size	OPENSSL_INIT_set_config_filename, .-OPENSSL_INIT_set_config_filename
	.p2align 4
	.globl	OPENSSL_INIT_set_config_file_flags
	.type	OPENSSL_INIT_set_config_file_flags, @function
OPENSSL_INIT_set_config_file_flags:
.LFB336:
	.cfi_startproc
	endbr64
	movq	%rsi, 16(%rdi)
	ret
	.cfi_endproc
.LFE336:
	.size	OPENSSL_INIT_set_config_file_flags, .-OPENSSL_INIT_set_config_file_flags
	.p2align 4
	.globl	OPENSSL_INIT_set_config_appname
	.type	OPENSSL_INIT_set_config_appname, @function
OPENSSL_INIT_set_config_appname:
.LFB337:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	je	.L189
	movq	%rsi, %rdi
	call	strdup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L191
.L189:
	movq	8(%rbx), %rdi
	call	free@PLT
	movq	%r12, 8(%rbx)
	movl	$1, %eax
.L188:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L191:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L188
	.cfi_endproc
.LFE337:
	.size	OPENSSL_INIT_set_config_appname, .-OPENSSL_INIT_set_config_appname
	.p2align 4
	.globl	OPENSSL_INIT_free
	.type	OPENSSL_INIT_free, @function
OPENSSL_INIT_free:
.LFB338:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	free@PLT
	movq	8(%r12), %rdi
	call	free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	free@PLT
	.cfi_endproc
.LFE338:
	.size	OPENSSL_INIT_free, .-OPENSSL_INIT_free
	.local	default_CONF_method
	.comm	default_CONF_method,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
