	.file	"rsa_crpt.c"
	.text
	.p2align 4
	.globl	RSA_bits
	.type	RSA_bits, @function
RSA_bits:
.LFB445:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdi
	jmp	BN_num_bits@PLT
	.cfi_endproc
.LFE445:
	.size	RSA_bits, .-RSA_bits
	.p2align 4
	.globl	RSA_size
	.type	RSA_size, @function
RSA_size:
.LFB446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	24(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	BN_num_bits@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	leal	14(%rax), %edx
	addl	$7, %eax
	cmovs	%edx, %eax
	sarl	$3, %eax
	ret
	.cfi_endproc
.LFE446:
	.size	RSA_size, .-RSA_size
	.p2align 4
	.globl	RSA_public_encrypt
	.type	RSA_public_encrypt, @function
RSA_public_encrypt:
.LFB447:
	.cfi_startproc
	endbr64
	movq	8(%rcx), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE447:
	.size	RSA_public_encrypt, .-RSA_public_encrypt
	.p2align 4
	.globl	RSA_private_encrypt
	.type	RSA_private_encrypt, @function
RSA_private_encrypt:
.LFB448:
	.cfi_startproc
	endbr64
	movq	8(%rcx), %rax
	jmp	*24(%rax)
	.cfi_endproc
.LFE448:
	.size	RSA_private_encrypt, .-RSA_private_encrypt
	.p2align 4
	.globl	RSA_private_decrypt
	.type	RSA_private_decrypt, @function
RSA_private_decrypt:
.LFB449:
	.cfi_startproc
	endbr64
	movq	8(%rcx), %rax
	jmp	*32(%rax)
	.cfi_endproc
.LFE449:
	.size	RSA_private_decrypt, .-RSA_private_decrypt
	.p2align 4
	.globl	RSA_public_decrypt
	.type	RSA_public_decrypt, @function
RSA_public_decrypt:
.LFB450:
	.cfi_startproc
	endbr64
	movq	8(%rcx), %rax
	jmp	*16(%rax)
	.cfi_endproc
.LFE450:
	.size	RSA_public_decrypt, .-RSA_public_decrypt
	.p2align 4
	.globl	RSA_flags
	.type	RSA_flags, @function
RSA_flags:
.LFB451:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L9
	movq	8(%rdi), %rax
	movl	72(%rax), %eax
.L9:
	ret
	.cfi_endproc
.LFE451:
	.size	RSA_flags, .-RSA_flags
	.p2align 4
	.globl	RSA_blinding_off
	.type	RSA_blinding_off, @function
RSA_blinding_off:
.LFB452:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	152(%rdi), %rdi
	call	BN_BLINDING_free@PLT
	movl	116(%rbx), %eax
	movq	$0, 152(%rbx)
	andl	$-9, %eax
	orb	$-128, %al
	movl	%eax, 116(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE452:
	.size	RSA_blinding_off, .-RSA_blinding_off
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/rsa/rsa_crpt.c"
	.text
	.p2align 4
	.globl	RSA_setup_blinding
	.type	RSA_setup_blinding, @function
RSA_setup_blinding:
.LFB455:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	testq	%rsi, %rsi
	je	.L41
.L16:
	movq	%r12, %rdi
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movl	$126, %r8d
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L40
	movq	32(%rbx), %r13
	testq	%r13, %r13
	je	.L42
.L20:
	call	BN_new@PLT
	testq	%rax, %rax
	je	.L43
	movq	24(%rbx), %rsi
	movq	%rax, %rdi
	movl	$4, %edx
	movq	%rax, -56(%rbp)
	call	BN_with_flags@PLT
	movq	8(%rbx), %rax
	movq	-56(%rbp), %r10
	xorl	%edi, %edi
	movq	120(%rbx), %r9
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	48(%rax), %r8
	movq	%r10, %rdx
	call	BN_BLINDING_create_param@PLT
	movq	-56(%rbp), %r10
	movq	%rax, %r15
	movq	%r10, %rdi
	call	BN_free@PLT
	testq	%r15, %r15
	je	.L44
	movq	%r15, %rdi
	call	BN_BLINDING_set_current_thread@PLT
.L19:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	cmpq	%r14, %r12
	je	.L31
	movq	%r12, %rdi
	call	BN_CTX_free@PLT
.L31:
	cmpq	%r13, 32(%rbx)
	je	.L15
	movq	%r13, %rdi
	call	BN_free@PLT
.L15:
	addq	$56, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	call	BN_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L16
	xorl	%r15d, %r15d
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L42:
	movq	40(%rbx), %rax
	movq	48(%rbx), %rsi
	movq	56(%rbx), %r15
	testq	%rax, %rax
	movq	%rax, -72(%rbp)
	sete	%dl
	testq	%rsi, %rsi
	movq	%rsi, -56(%rbp)
	sete	%al
	orb	%al, %dl
	jne	.L23
	testq	%r15, %r15
	je	.L23
	movq	%r12, %rdi
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -88(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	BN_CTX_get@PLT
	movq	-56(%rbp), %rsi
	testq	%rax, %rax
	je	.L39
	movq	%rsi, -56(%rbp)
	movq	%rax, -80(%rbp)
	call	BN_value_one@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movq	%rax, %rdx
	call	BN_sub@PLT
	movq	-80(%rbp), %r8
	testl	%eax, %eax
	je	.L39
	movq	%r8, -56(%rbp)
	call	BN_value_one@PLT
	movq	-56(%rbp), %r8
	movq	%r15, %rsi
	movq	%rax, %rdx
	movq	%r8, %rdi
	call	BN_sub@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	jne	.L26
	.p2align 4,,10
	.p2align 3
.L39:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
.L23:
	movl	$133, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$140, %edx
	xorl	%r15d, %r15d
	movl	$136, %esi
	movl	$4, %edi
	call	ERR_put_error@PLT
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L43:
	movl	$144, %r8d
.L40:
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$136, %esi
	xorl	%r15d, %r15d
	movl	$4, %edi
	call	ERR_put_error@PLT
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L44:
	movl	$155, %r8d
	movl	$3, %edx
	movl	$136, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L26:
	movq	-64(%rbp), %rsi
	movq	-88(%rbp), %rdi
	movq	%r12, %rcx
	movq	%r8, %rdx
	call	BN_mul@PLT
	testl	%eax, %eax
	je	.L39
	movq	-88(%rbp), %rdx
	movq	-72(%rbp), %rsi
	xorl	%edi, %edi
	movq	%r12, %rcx
	call	BN_mod_inverse@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	BN_CTX_end@PLT
	testq	%r15, %r15
	je	.L23
	movq	%r15, %r13
	jmp	.L20
	.cfi_endproc
.LFE455:
	.size	RSA_setup_blinding, .-RSA_setup_blinding
	.p2align 4
	.globl	RSA_blinding_on
	.type	RSA_blinding_on, @function
RSA_blinding_on:
.LFB453:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	152(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L46
	call	BN_BLINDING_free@PLT
	movl	116(%rbx), %eax
	movq	$0, 152(%rbx)
	andl	$-9, %eax
	orb	$-128, %al
	movl	%eax, 116(%rbx)
.L46:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	RSA_setup_blinding
	movq	%rax, 152(%rbx)
	testq	%rax, %rax
	je	.L48
	movl	116(%rbx), %eax
	andb	$127, %al
	orl	$8, %eax
	movl	%eax, 116(%rbx)
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
.L47:
	endbr64
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE453:
	.size	RSA_blinding_on, .-RSA_blinding_on
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
