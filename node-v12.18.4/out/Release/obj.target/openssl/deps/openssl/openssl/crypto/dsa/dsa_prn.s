	.file	"dsa_prn.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/dsa/dsa_prn.c"
	.text
	.p2align 4
	.globl	DSA_print_fp
	.type	DSA_print_fp, @function
DSA_print_fp:
.LFB419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	testq	%rax, %rax
	je	.L12
	movq	%r13, %rcx
	xorl	%edx, %edx
	movl	$106, %esi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	BIO_ctrl@PLT
	call	EVP_PKEY_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L6
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	EVP_PKEY_set1_DSA@PLT
	testl	%eax, %eax
	jne	.L13
.L6:
	xorl	%r14d, %r14d
.L5:
	movq	%r12, %rdi
	call	BIO_free@PLT
	popq	%r12
	movl	%r14d, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movl	%r15d, %edx
	movq	%r13, %rsi
	call	EVP_PKEY_print_private@PLT
	movq	%r13, %rdi
	movl	%eax, %r14d
	call	EVP_PKEY_free@PLT
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L12:
	xorl	%r14d, %r14d
	movl	$22, %r8d
	movl	$7, %edx
	movl	$105, %esi
	leaq	.LC0(%rip), %rcx
	movl	$10, %edi
	call	ERR_put_error@PLT
	popq	%r12
	movl	%r14d, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE419:
	.size	DSA_print_fp, .-DSA_print_fp
	.p2align 4
	.globl	DSAparams_print_fp
	.type	DSAparams_print_fp, @function
DSAparams_print_fp:
.LFB420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	testq	%rax, %rax
	je	.L24
	movq	%r13, %rcx
	xorl	%edx, %edx
	movl	$106, %esi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	BIO_ctrl@PLT
	call	EVP_PKEY_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L19
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	EVP_PKEY_set1_DSA@PLT
	testl	%eax, %eax
	jne	.L25
.L19:
	xorl	%r14d, %r14d
.L18:
	movq	%r12, %rdi
	call	BIO_free@PLT
	addq	$8, %rsp
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movl	$4, %edx
	movq	%r13, %rsi
	call	EVP_PKEY_print_params@PLT
	movq	%r13, %rdi
	movl	%eax, %r14d
	call	EVP_PKEY_free@PLT
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L24:
	movl	$37, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$7, %edx
	xorl	%r14d, %r14d
	movl	$101, %esi
	movl	$10, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE420:
	.size	DSAparams_print_fp, .-DSAparams_print_fp
	.p2align 4
	.globl	DSA_print
	.type	DSA_print, @function
DSA_print:
.LFB421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	call	EVP_PKEY_new@PLT
	testq	%rax, %rax
	je	.L29
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	EVP_PKEY_set1_DSA@PLT
	testl	%eax, %eax
	jne	.L34
.L29:
	xorl	%r13d, %r13d
	popq	%r12
	movl	%r13d, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	movl	%r15d, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	xorl	%ecx, %ecx
	call	EVP_PKEY_print_private@PLT
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	EVP_PKEY_free@PLT
	popq	%r12
	movl	%r13d, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE421:
	.size	DSA_print, .-DSA_print
	.p2align 4
	.globl	DSAparams_print
	.type	DSAparams_print, @function
DSAparams_print:
.LFB422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	call	EVP_PKEY_new@PLT
	testq	%rax, %rax
	je	.L38
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	EVP_PKEY_set1_DSA@PLT
	testl	%eax, %eax
	jne	.L43
.L38:
	addq	$8, %rsp
	xorl	%r13d, %r13d
	popq	%r12
	movl	%r13d, %eax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r14, %rdi
	xorl	%ecx, %ecx
	movl	$4, %edx
	call	EVP_PKEY_print_params@PLT
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	EVP_PKEY_free@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE422:
	.size	DSAparams_print, .-DSAparams_print
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
