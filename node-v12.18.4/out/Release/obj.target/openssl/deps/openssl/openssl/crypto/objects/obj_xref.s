	.file	"obj_xref.c"
	.text
	.p2align 4
	.type	sig_cmp_BSEARCH_CMP_FN, @function
sig_cmp_BSEARCH_CMP_FN:
.LFB396:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	subl	(%rsi), %eax
	ret
	.cfi_endproc
.LFE396:
	.size	sig_cmp_BSEARCH_CMP_FN, .-sig_cmp_BSEARCH_CMP_FN
	.p2align 4
	.type	sig_sk_cmp, @function
sig_sk_cmp:
.LFB398:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rsi), %rdx
	movl	(%rax), %eax
	subl	(%rdx), %eax
	ret
	.cfi_endproc
.LFE398:
	.size	sig_sk_cmp, .-sig_sk_cmp
	.p2align 4
	.type	sigx_cmp, @function
sigx_cmp:
.LFB399:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rcx
	movq	(%rsi), %rdx
	movl	4(%rcx), %eax
	subl	4(%rdx), %eax
	jne	.L4
	movl	8(%rcx), %eax
	subl	8(%rdx), %eax
.L4:
	ret
	.cfi_endproc
.LFE399:
	.size	sigx_cmp, .-sigx_cmp
	.p2align 4
	.type	sigx_cmp_BSEARCH_CMP_FN, @function
sigx_cmp_BSEARCH_CMP_FN:
.LFB400:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rcx
	movq	(%rsi), %rdx
	movl	4(%rcx), %eax
	subl	4(%rdx), %eax
	jne	.L6
	movl	8(%rcx), %eax
	subl	8(%rdx), %eax
.L6:
	ret
	.cfi_endproc
.LFE400:
	.size	sigx_cmp_BSEARCH_CMP_FN, .-sigx_cmp_BSEARCH_CMP_FN
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/objects/obj_xref.c"
	.text
	.p2align 4
	.type	sid_free, @function
sid_free:
.LFB405:
	.cfi_startproc
	endbr64
	movl	$130, %edx
	leaq	.LC0(%rip), %rsi
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE405:
	.size	sid_free, .-sid_free
	.p2align 4
	.globl	OBJ_find_sigid_algs
	.type	OBJ_find_sigid_algs, @function
OBJ_find_sigid_algs:
.LFB402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-52(%rbp), %r13
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	%edi, -52(%rbp)
	movq	sig_app(%rip), %rdi
	testq	%rdi, %rdi
	je	.L13
	movq	%r13, %rsi
	call	OPENSSL_sk_find@PLT
	movq	sig_app(%rip), %rdi
	movl	%eax, %esi
	call	OPENSSL_sk_value@PLT
	testq	%rax, %rax
	je	.L13
.L12:
	testq	%r12, %r12
	je	.L14
	movl	4(%rax), %edx
	movl	%edx, (%r12)
.L14:
	movl	$1, %r8d
	testq	%rbx, %rbx
	je	.L9
	movl	8(%rax), %eax
	movl	%eax, (%rbx)
.L9:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L27
	addq	$40, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	leaq	sig_cmp_BSEARCH_CMP_FN(%rip), %r8
	movl	$12, %ecx
	movl	$48, %edx
	movq	%r13, %rdi
	leaq	sigoid_srt(%rip), %rsi
	call	OBJ_bsearch_@PLT
	xorl	%r8d, %r8d
	testq	%rax, %rax
	jne	.L12
	jmp	.L9
.L27:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE402:
	.size	OBJ_find_sigid_algs, .-OBJ_find_sigid_algs
	.p2align 4
	.globl	OBJ_find_sigid_by_algs
	.type	OBJ_find_sigid_by_algs, @function
OBJ_find_sigid_by_algs:
.LFB403:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	-36(%rbp), %rsi
	subq	$40, %rsp
	movq	sigx_app(%rip), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -48(%rbp)
	movl	%r8d, -32(%rbp)
	movl	%edx, -28(%rbp)
	testq	%rdi, %rdi
	je	.L29
	call	OPENSSL_sk_find@PLT
	movl	%eax, %esi
	testl	%eax, %eax
	jns	.L41
.L29:
	leaq	sigx_cmp_BSEARCH_CMP_FN(%rip), %r8
	leaq	-48(%rbp), %rdi
	movl	$8, %ecx
	movl	$43, %edx
	leaq	sigoid_srt_xref(%rip), %rsi
	call	OBJ_bsearch_@PLT
	xorl	%r8d, %r8d
	testq	%rax, %rax
	je	.L28
.L30:
	movl	$1, %r8d
	testq	%rbx, %rbx
	je	.L28
	movq	(%rax), %rax
	movl	(%rax), %eax
	movl	%eax, (%rbx)
.L28:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L42
	addq	$40, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movq	sigx_app(%rip), %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, -48(%rbp)
	leaq	-48(%rbp), %rax
	jmp	.L30
.L42:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE403:
	.size	OBJ_find_sigid_by_algs, .-OBJ_find_sigid_by_algs
	.p2align 4
	.globl	OBJ_add_sigid
	.type	OBJ_add_sigid, @function
OBJ_add_sigid:
.LFB404:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	%esi, %ebx
	subq	$16, %rsp
	cmpq	$0, sig_app(%rip)
	je	.L44
.L47:
	cmpq	$0, sigx_app(%rip)
	je	.L62
.L46:
	movl	$106, %edx
	leaq	.LC0(%rip), %rsi
	movl	$12, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L63
	movl	%r13d, (%rax)
	movq	sig_app(%rip), %rdi
	movq	%rax, %rsi
	movl	%ebx, 4(%rax)
	movl	%r14d, 8(%rax)
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L64
	movq	sigx_app(%rip), %rdi
	movq	%r12, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L65
.L61:
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	leaq	sigx_cmp(%rip), %rdi
	call	OPENSSL_sk_new@PLT
	movq	%rax, sigx_app(%rip)
	testq	%rax, %rax
	je	.L61
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L64:
	movq	%r12, %rdi
	movl	$115, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, -36(%rbp)
	call	CRYPTO_free@PLT
	movl	-36(%rbp), %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	movq	sig_app(%rip), %rdi
	call	OPENSSL_sk_sort@PLT
	movq	sigx_app(%rip), %rdi
	call	OPENSSL_sk_sort@PLT
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	leaq	sig_sk_cmp(%rip), %rdi
	call	OPENSSL_sk_new@PLT
	movq	%rax, sig_app(%rip)
	testq	%rax, %rax
	je	.L61
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L63:
	movl	$107, %r8d
	movl	$65, %edx
	movl	$107, %esi
	movl	$8, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L61
	.cfi_endproc
.LFE404:
	.size	OBJ_add_sigid, .-OBJ_add_sigid
	.p2align 4
	.globl	OBJ_sigid_free
	.type	OBJ_sigid_free, @function
OBJ_sigid_free:
.LFB406:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	sig_app(%rip), %rdi
	leaq	sid_free(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	OPENSSL_sk_pop_free@PLT
	movq	sigx_app(%rip), %rdi
	movq	$0, sig_app(%rip)
	call	OPENSSL_sk_free@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	$0, sigx_app(%rip)
	ret
	.cfi_endproc
.LFE406:
	.size	OBJ_sigid_free, .-OBJ_sigid_free
	.local	sigx_app
	.comm	sigx_app,8,8
	.local	sig_app
	.comm	sig_app,8,8
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	sigoid_srt_xref, @object
	.size	sigoid_srt_xref, 344
sigoid_srt_xref:
	.quad	sigoid_srt
	.quad	sigoid_srt+12
	.quad	sigoid_srt+84
	.quad	sigoid_srt+24
	.quad	sigoid_srt+48
	.quad	sigoid_srt+36
	.quad	sigoid_srt+108
	.quad	sigoid_srt+60
	.quad	sigoid_srt+96
	.quad	sigoid_srt+144
	.quad	sigoid_srt+360
	.quad	sigoid_srt+420
	.quad	sigoid_srt+72
	.quad	sigoid_srt+120
	.quad	sigoid_srt+132
	.quad	sigoid_srt+156
	.quad	sigoid_srt+288
	.quad	sigoid_srt+240
	.quad	sigoid_srt+384
	.quad	sigoid_srt+444
	.quad	sigoid_srt+168
	.quad	sigoid_srt+252
	.quad	sigoid_srt+396
	.quad	sigoid_srt+456
	.quad	sigoid_srt+180
	.quad	sigoid_srt+264
	.quad	sigoid_srt+408
	.quad	sigoid_srt+468
	.quad	sigoid_srt+192
	.quad	sigoid_srt+276
	.quad	sigoid_srt+228
	.quad	sigoid_srt+372
	.quad	sigoid_srt+432
	.quad	sigoid_srt+300
	.quad	sigoid_srt+312
	.quad	sigoid_srt+324
	.quad	sigoid_srt+336
	.quad	sigoid_srt+480
	.quad	sigoid_srt+492
	.quad	sigoid_srt+528
	.quad	sigoid_srt+540
	.quad	sigoid_srt+552
	.quad	sigoid_srt+564
	.section	.rodata
	.align 32
	.type	sigoid_srt, @object
	.size	sigoid_srt, 576
sigoid_srt:
	.long	7
	.long	3
	.long	6
	.long	8
	.long	4
	.long	6
	.long	42
	.long	41
	.long	6
	.long	65
	.long	64
	.long	6
	.long	66
	.long	41
	.long	116
	.long	70
	.long	64
	.long	67
	.long	96
	.long	95
	.long	6
	.long	104
	.long	4
	.long	19
	.long	113
	.long	64
	.long	116
	.long	115
	.long	64
	.long	19
	.long	119
	.long	117
	.long	6
	.long	396
	.long	257
	.long	6
	.long	416
	.long	64
	.long	408
	.long	668
	.long	672
	.long	6
	.long	669
	.long	673
	.long	6
	.long	670
	.long	674
	.long	6
	.long	671
	.long	675
	.long	6
	.long	791
	.long	0
	.long	408
	.long	792
	.long	0
	.long	408
	.long	793
	.long	675
	.long	408
	.long	794
	.long	672
	.long	408
	.long	795
	.long	673
	.long	408
	.long	796
	.long	674
	.long	408
	.long	802
	.long	675
	.long	116
	.long	803
	.long	672
	.long	116
	.long	807
	.long	809
	.long	811
	.long	808
	.long	809
	.long	812
	.long	852
	.long	809
	.long	850
	.long	853
	.long	809
	.long	851
	.long	912
	.long	0
	.long	6
	.long	936
	.long	64
	.long	946
	.long	937
	.long	675
	.long	946
	.long	938
	.long	672
	.long	946
	.long	939
	.long	673
	.long	946
	.long	940
	.long	674
	.long	946
	.long	941
	.long	64
	.long	947
	.long	942
	.long	675
	.long	947
	.long	943
	.long	672
	.long	947
	.long	944
	.long	673
	.long	947
	.long	945
	.long	674
	.long	947
	.long	985
	.long	982
	.long	979
	.long	986
	.long	983
	.long	980
	.long	1087
	.long	0
	.long	1087
	.long	1088
	.long	0
	.long	1088
	.long	1116
	.long	1096
	.long	6
	.long	1117
	.long	1097
	.long	6
	.long	1118
	.long	1098
	.long	6
	.long	1119
	.long	1099
	.long	6
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
