	.file	"p5_pbe.c"
	.text
	.p2align 4
	.globl	d2i_PBEPARAM
	.type	d2i_PBEPARAM, @function
d2i_PBEPARAM:
.LFB803:
	.cfi_startproc
	endbr64
	leaq	PBEPARAM_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE803:
	.size	d2i_PBEPARAM, .-d2i_PBEPARAM
	.p2align 4
	.globl	i2d_PBEPARAM
	.type	i2d_PBEPARAM, @function
i2d_PBEPARAM:
.LFB804:
	.cfi_startproc
	endbr64
	leaq	PBEPARAM_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE804:
	.size	i2d_PBEPARAM, .-i2d_PBEPARAM
	.p2align 4
	.globl	PBEPARAM_new
	.type	PBEPARAM_new, @function
PBEPARAM_new:
.LFB805:
	.cfi_startproc
	endbr64
	leaq	PBEPARAM_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE805:
	.size	PBEPARAM_new, .-PBEPARAM_new
	.p2align 4
	.globl	PBEPARAM_free
	.type	PBEPARAM_free, @function
PBEPARAM_free:
.LFB806:
	.cfi_startproc
	endbr64
	leaq	PBEPARAM_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE806:
	.size	PBEPARAM_free, .-PBEPARAM_free
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/asn1/p5_pbe.c"
	.text
	.p2align 4
	.globl	PKCS5_pbe_set0_algor
	.type	PKCS5_pbe_set0_algor, @function
PKCS5_pbe_set0_algor:
.LFB807:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	leaq	PBEPARAM_it(%rip), %rdi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$40, %rsp
	movl	%esi, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	call	ASN1_item_new@PLT
	movl	$36, %r8d
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L21
	testl	%ebx, %ebx
	movl	$2048, %eax
	movq	8(%r12), %rdi
	cmovle	%eax, %ebx
	movslq	%ebx, %rsi
	call	ASN1_INTEGER_set@PLT
	testl	%eax, %eax
	je	.L23
	movslq	%r13d, %r8
	testl	%r13d, %r13d
	je	.L24
.L11:
	movq	%r8, %rdi
	movl	$48, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r8, -80(%rbp)
	call	CRYPTO_malloc@PLT
	movq	-80(%rbp), %r8
	testq	%rax, %rax
	je	.L25
	testq	%r14, %r14
	je	.L13
	movq	%r8, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r9
.L14:
	movq	(%r12), %rdi
	movl	%r13d, %edx
	movq	%r9, %rsi
	call	ASN1_STRING_set0@PLT
	leaq	-64(%rbp), %rdx
	leaq	PBEPARAM_it(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_item_pack@PLT
	testq	%rax, %rax
	je	.L26
	leaq	PBEPARAM_it(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_item_free@PLT
	movl	-72(%rbp), %edi
	movq	-64(%rbp), %r12
	call	OBJ_nid2obj@PLT
	movq	%r12, %rcx
	movl	$16, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	X509_ALGOR_set0@PLT
	movl	%eax, %r8d
	movl	$1, %eax
	testl	%r8d, %r8d
	jne	.L6
	xorl	%r9d, %r9d
	xorl	%r12d, %r12d
.L8:
	movq	%r9, %rdi
	movl	$73, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	leaq	PBEPARAM_it(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_item_free@PLT
	movq	-64(%rbp), %rdi
	call	ASN1_STRING_free@PLT
	xorl	%eax, %eax
.L6:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L27
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movl	$8, %r8d
	movl	$8, %r13d
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L23:
	movl	$42, %r8d
.L21:
	movl	$65, %edx
	movl	$215, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r9d, %r9d
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L13:
	movl	%r13d, %esi
	movq	%rax, %rdi
	movq	%rax, -80(%rbp)
	call	RAND_bytes@PLT
	movq	-80(%rbp), %r9
	testl	%eax, %eax
	jg	.L14
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L26:
	movq	%rax, -72(%rbp)
	movl	$62, %r8d
.L22:
	movl	$65, %edx
	movl	$215, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-72(%rbp), %r9
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L25:
	movq	%rax, -72(%rbp)
	movl	$50, %r8d
	jmp	.L22
.L27:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE807:
	.size	PKCS5_pbe_set0_algor, .-PKCS5_pbe_set0_algor
	.p2align 4
	.globl	PKCS5_pbe_set
	.type	PKCS5_pbe_set, @function
PKCS5_pbe_set:
.LFB808:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$8, %rsp
	call	X509_ALGOR_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L32
	movl	%ebx, %r8d
	movq	%r15, %rcx
	movl	%r14d, %edx
	movl	%r13d, %esi
	movq	%rax, %rdi
	call	PKCS5_pbe_set0_algor
	testl	%eax, %eax
	je	.L33
.L28:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	X509_ALGOR_free@PLT
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L32:
	movl	$87, %r8d
	movl	$65, %edx
	movl	$202, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L28
	.cfi_endproc
.LFE808:
	.size	PKCS5_pbe_set, .-PKCS5_pbe_set
	.globl	PBEPARAM_it
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"PBEPARAM"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	PBEPARAM_it, @object
	.size	PBEPARAM_it, 56
PBEPARAM_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	PBEPARAM_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC1
	.section	.rodata.str1.1
.LC2:
	.string	"salt"
.LC3:
	.string	"iter"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	PBEPARAM_seq_tt, @object
	.size	PBEPARAM_seq_tt, 80
PBEPARAM_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC2
	.quad	ASN1_OCTET_STRING_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC3
	.quad	ASN1_INTEGER_it
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
