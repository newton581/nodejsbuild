	.file	"bn_mpi.c"
	.text
	.p2align 4
	.globl	BN_bn2mpi
	.type	BN_bn2mpi, @function
BN_bn2mpi:
.LFB252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	BN_num_bits@PLT
	movl	%eax, %ecx
	leal	14(%rax), %edx
	addl	$7, %ecx
	cmovns	%ecx, %edx
	xorl	%r13d, %r13d
	sarl	$3, %edx
	testl	%eax, %eax
	jle	.L2
	xorl	%r13d, %r13d
	testb	$7, %al
	sete	%r13b
.L2:
	leal	4(%r13,%rdx), %eax
	testq	%rbx, %rbx
	je	.L1
	addl	%r13d, %edx
	bswap	%edx
	movl	%edx, (%rbx)
	testl	%r13d, %r13d
	je	.L5
	movb	$0, 4(%rbx)
.L5:
	leal	4(%r13), %esi
	movq	%r12, %rdi
	movslq	%esi, %rsi
	addq	%rbx, %rsi
	call	BN_bn2bin@PLT
	movl	16(%r12), %edx
	testl	%edx, %edx
	jne	.L14
.L6:
	leal	4(%r13,%rax), %eax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	orb	$-128, 4(%rbx)
	jmp	.L6
	.cfi_endproc
.LFE252:
	.size	BN_bn2mpi, .-BN_bn2mpi
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/bn/bn_mpi.c"
	.text
	.p2align 4
	.globl	BN_mpi2bn
	.type	BN_mpi2bn, @function
BN_mpi2bn:
.LFB253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpl	$3, %esi
	jle	.L39
	movl	(%rdi), %r12d
	movslq	%esi, %rsi
	movq	%rdi, %rbx
	bswap	%r12d
	movl	%r12d, %eax
	addq	$4, %rax
	cmpq	%rsi, %rax
	jne	.L40
	movq	%rdx, %r13
	movq	%rdx, %r14
	testq	%rdx, %rdx
	je	.L41
.L19:
	testl	%r12d, %r12d
	je	.L42
	cmpb	$0, 4(%rbx)
	leaq	4(%rbx), %rdi
	js	.L43
	movq	%r14, %rdx
	movl	%r12d, %esi
	call	BN_bin2bn@PLT
	testq	%rax, %rax
	je	.L24
	movl	$0, 16(%r14)
.L15:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	movl	$0, 16(%r14)
	movq	%r14, %rax
	movl	$0, 8(%r14)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	movq	%r14, %rdx
	movl	%r12d, %esi
	call	BN_bin2bn@PLT
	testq	%rax, %rax
	je	.L24
	movl	$1, 16(%r14)
	movq	%r14, %rdi
	call	BN_num_bits@PLT
	movq	%r14, %rdi
	leal	-1(%rax), %esi
	call	BN_clear_bit@PLT
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	xorl	%r14d, %r14d
	movl	$55, %r8d
	movl	$104, %edx
	movl	$112, %esi
	leaq	.LC0(%rip), %rcx
	movl	$3, %edi
	call	ERR_put_error@PLT
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	call	BN_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L19
.L38:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	movl	$49, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$106, %edx
	xorl	%r14d, %r14d
	movl	$112, %esi
	movl	$3, %edi
	call	ERR_put_error@PLT
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L24:
	testq	%r13, %r13
	jne	.L38
	movq	%r14, %rdi
	call	BN_free@PLT
	jmp	.L38
	.cfi_endproc
.LFE253:
	.size	BN_mpi2bn, .-BN_mpi2bn
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
