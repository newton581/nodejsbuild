	.file	"pcy_node.c"
	.text
	.p2align 4
	.type	node_cmp, @function
node_cmp:
.LFB1272:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rsi
	movq	(%rdi), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	jmp	OBJ_cmp@PLT
	.cfi_endproc
.LFE1272:
	.size	node_cmp, .-node_cmp
	.p2align 4
	.globl	policy_node_cmp_new
	.type	policy_node_cmp_new, @function
policy_node_cmp_new:
.LFB1273:
	.cfi_startproc
	endbr64
	leaq	node_cmp(%rip), %rdi
	jmp	OPENSSL_sk_new@PLT
	.cfi_endproc
.LFE1273:
	.size	policy_node_cmp_new, .-policy_node_cmp_new
	.p2align 4
	.globl	tree_find_sk
	.type	tree_find_sk, @function
tree_find_sk:
.LFB1274:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -56(%rbp)
	leaq	-64(%rbp), %rax
	leaq	-96(%rbp), %rsi
	movq	%rax, -96(%rbp)
	call	OPENSSL_sk_find@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	OPENSSL_sk_value@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L7
	addq	$88, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L7:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1274:
	.size	tree_find_sk, .-tree_find_sk
	.p2align 4
	.globl	level_find_node
	.type	level_find_node, @function
level_find_node:
.LFB1275:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$8, %rsp
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L12:
	addl	$1, %ebx
.L9:
	movq	8(%r13), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L18
	movq	8(%r13), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r12
	cmpq	%r14, 8(%rax)
	jne	.L12
	movq	(%rax), %rax
	movq	%r15, %rsi
	movq	8(%rax), %rdi
	call	OBJ_cmp@PLT
	testl	%eax, %eax
	jne	.L12
.L8:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L8
	.cfi_endproc
.LFE1275:
	.size	level_find_node, .-level_find_node
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/x509v3/pcy_node.c"
	.text
	.p2align 4
	.globl	level_add_node
	.type	level_add_node, @function
level_add_node:
.LFB1276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	$24, %edi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	leaq	.LC0(%rip), %rsi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	movl	$66, %edx
	subq	$8, %rsp
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L40
	movq	%r13, %xmm0
	movq	%rbx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rax)
	testq	%r14, %r14
	je	.L25
	movq	8(%r13), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$746, %eax
	je	.L41
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.L42
.L26:
	movq	%r12, %rsi
	call	OPENSSL_sk_push@PLT
	movl	$87, %r8d
	testl	%eax, %eax
	je	.L39
.L25:
	testq	%r15, %r15
	je	.L27
	movq	16(%r15), %rdi
	testq	%rdi, %rdi
	je	.L43
.L28:
	movq	%r13, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L44
.L27:
	testq	%rbx, %rbx
	je	.L19
	addl	$1, 16(%rbx)
.L19:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	cmpq	$0, 16(%r14)
	jne	.L24
	movq	%r12, 16(%r14)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L43:
	call	OPENSSL_sk_new_null@PLT
	movl	$97, %r8d
	movq	%rax, 16(%r15)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L28
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L44:
	movl	$101, %r8d
.L39:
	movl	$65, %edx
	movl	$168, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L24:
	movq	%r12, %rdi
	movl	$118, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%r12d, %r12d
	call	CRYPTO_free@PLT
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L42:
	leaq	node_cmp(%rip), %rdi
	call	OPENSSL_sk_new@PLT
	movl	$83, %r8d
	movq	%rax, 8(%r14)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L26
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L40:
	movl	$68, %r8d
	movl	$65, %edx
	movl	$168, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L19
	.cfi_endproc
.LFE1276:
	.size	level_add_node, .-level_add_node
	.p2align 4
	.globl	policy_node_free
	.type	policy_node_free, @function
policy_node_free:
.LFB1277:
	.cfi_startproc
	endbr64
	movl	$118, %edx
	leaq	.LC0(%rip), %rsi
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE1277:
	.size	policy_node_free, .-policy_node_free
	.p2align 4
	.globl	policy_node_match
	.type	policy_node_match, @function
policy_node_match:
.LFB1278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rsi), %r12
	testb	$4, 25(%rdi)
	jne	.L47
	testb	$3, (%r12)
	je	.L47
	xorl	%ebx, %ebx
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L50:
	movq	24(%r12), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	OBJ_cmp@PLT
	testl	%eax, %eax
	je	.L52
	addl	$1, %ebx
.L48:
	movq	24(%r12), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L50
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	movq	8(%r12), %rdi
	movq	%r13, %rsi
	call	OBJ_cmp@PLT
	testl	%eax, %eax
	sete	%al
	addq	$8, %rsp
	popq	%rbx
	movzbl	%al, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1278:
	.size	policy_node_match, .-policy_node_match
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
