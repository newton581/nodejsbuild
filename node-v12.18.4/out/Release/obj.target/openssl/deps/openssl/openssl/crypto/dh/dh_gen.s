	.file	"dh_gen.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/dh/dh_gen.c"
	.text
	.p2align 4
	.globl	DH_generate_parameters_ex
	.type	DH_generate_parameters_ex, @function
DH_generate_parameters_ex:
.LFB421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	movq	120(%rdi), %rax
	movq	64(%rax), %rax
	testq	%rax, %rax
	je	.L2
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	call	BN_CTX_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L4
	movq	%rax, %rdi
	call	BN_CTX_start@PLT
	movq	%r15, %rdi
	call	BN_CTX_get@PLT
	movq	%r15, %rdi
	movq	%rax, -56(%rbp)
	call	BN_CTX_get@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L4
	cmpq	$0, 8(%r12)
	je	.L6
.L9:
	cmpq	$0, 16(%r12)
	je	.L57
.L8:
	cmpl	$1, %r13d
	jle	.L58
	movq	%r8, -64(%rbp)
	cmpl	$2, %r13d
	je	.L59
	cmpl	$5, %r13d
	je	.L60
	movq	-56(%rbp), %rdi
	movl	$12, %esi
	call	BN_set_word@PLT
	testl	%eax, %eax
	jne	.L54
	.p2align 4,,10
	.p2align 3
.L4:
	movl	$121, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$3, %edx
	xorl	%r12d, %r12d
	movl	$106, %esi
	movl	$5, %edi
	call	ERR_put_error@PLT
.L15:
	movq	%r15, %rdi
	call	BN_CTX_end@PLT
	movq	%r15, %rdi
	call	BN_CTX_free@PLT
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	movq	-56(%rbp), %rdi
	movl	$24, %esi
	call	BN_set_word@PLT
	testl	%eax, %eax
	je	.L4
.L54:
	movl	$11, %esi
.L55:
	movq	-64(%rbp), %r8
	movq	%r8, %rdi
	call	BN_set_word@PLT
	movq	-64(%rbp), %r8
	testl	%eax, %eax
	je	.L4
	movq	8(%r12), %rdi
	movq	-56(%rbp), %rcx
	movq	%rbx, %r9
	movl	%r14d, %esi
	movl	$1, %edx
	call	BN_generate_prime_ex@PLT
	testl	%eax, %eax
	je	.L4
	xorl	%edx, %edx
	movl	$3, %esi
	movq	%rbx, %rdi
	call	BN_GENCB_call@PLT
	testl	%eax, %eax
	je	.L4
	movq	16(%r12), %rdi
	movslq	%r13d, %rsi
	movl	$1, %r12d
	call	BN_set_word@PLT
	testl	%eax, %eax
	jne	.L15
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L60:
	movq	-56(%rbp), %rdi
	movl	$60, %esi
	call	BN_set_word@PLT
	movl	$23, %esi
	testl	%eax, %eax
	je	.L4
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L6:
	movq	%rax, -64(%rbp)
	call	BN_new@PLT
	movq	-64(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, 8(%r12)
	jne	.L9
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L57:
	movq	%r8, -64(%rbp)
	call	BN_new@PLT
	movq	-64(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, 16(%r12)
	jne	.L8
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L58:
	movl	$84, %r8d
	movl	$101, %edx
	movl	$106, %esi
	movl	$5, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L4
	.cfi_endproc
.LFE421:
	.size	DH_generate_parameters_ex, .-DH_generate_parameters_ex
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
