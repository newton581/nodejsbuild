	.file	"p5_scrypt.c"
	.text
	.p2align 4
	.globl	d2i_SCRYPT_PARAMS
	.type	d2i_SCRYPT_PARAMS, @function
d2i_SCRYPT_PARAMS:
.LFB803:
	.cfi_startproc
	endbr64
	leaq	SCRYPT_PARAMS_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE803:
	.size	d2i_SCRYPT_PARAMS, .-d2i_SCRYPT_PARAMS
	.p2align 4
	.globl	i2d_SCRYPT_PARAMS
	.type	i2d_SCRYPT_PARAMS, @function
i2d_SCRYPT_PARAMS:
.LFB804:
	.cfi_startproc
	endbr64
	leaq	SCRYPT_PARAMS_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE804:
	.size	i2d_SCRYPT_PARAMS, .-i2d_SCRYPT_PARAMS
	.p2align 4
	.globl	SCRYPT_PARAMS_new
	.type	SCRYPT_PARAMS_new, @function
SCRYPT_PARAMS_new:
.LFB805:
	.cfi_startproc
	endbr64
	leaq	SCRYPT_PARAMS_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE805:
	.size	SCRYPT_PARAMS_new, .-SCRYPT_PARAMS_new
	.p2align 4
	.globl	SCRYPT_PARAMS_free
	.type	SCRYPT_PARAMS_free, @function
SCRYPT_PARAMS_free:
.LFB806:
	.cfi_startproc
	endbr64
	leaq	SCRYPT_PARAMS_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE806:
	.size	SCRYPT_PARAMS_free, .-SCRYPT_PARAMS_free
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/asn1/p5_scrypt.c"
	.text
	.p2align 4
	.globl	PKCS5_pbe2_set_scrypt
	.type	PKCS5_pbe2_set_scrypt, @function
PKCS5_pbe2_set_scrypt:
.LFB807:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -104(%rbp)
	movl	%edx, -108(%rbp)
	movq	%r9, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L80
	pushq	$0
	xorl	%edx, %edx
	movq	%rcx, %r13
	xorl	%esi, %esi
	pushq	$0
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	movq	%r8, %r15
	pushq	$0
	pushq	16(%rbp)
	call	EVP_PBE_scrypt@PLT
	addq	$32, %rsp
	movl	$57, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$227, %edx
	testl	%eax, %eax
	je	.L79
	movq	%r12, %rdi
	call	EVP_CIPHER_type@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	je	.L81
	call	PBE2PARAM_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L11
	movq	8(%rax), %rax
	movl	%ebx, %edi
	movq	%rax, -96(%rbp)
	call	OBJ_nid2obj@PLT
	movq	-96(%rbp), %rcx
	movq	%rax, (%rcx)
	call	ASN1_TYPE_new@PLT
	movq	-96(%rbp), %rcx
	movq	%rax, 8(%rcx)
	testq	%rax, %rax
	je	.L11
	movq	%r12, %rdi
	call	EVP_CIPHER_iv_length@PLT
	testl	%eax, %eax
	je	.L13
	movq	%r12, %rdi
	testq	%r13, %r13
	je	.L14
	call	EVP_CIPHER_iv_length@PLT
	leaq	-80(%rbp), %rdi
	movl	$16, %ecx
	movq	%r13, %rsi
	movslq	%eax, %rdx
	call	__memcpy_chk@PLT
.L13:
	call	EVP_CIPHER_CTX_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L11
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	leaq	-80(%rbp), %r8
	movq	%rax, %rdi
	call	EVP_CipherInit_ex@PLT
	testl	%eax, %eax
	je	.L33
	movq	-96(%rbp), %rax
	movq	%r13, %rdi
	movq	8(%rax), %rsi
	call	EVP_CIPHER_param_to_asn1@PLT
	testl	%eax, %eax
	jle	.L82
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	EVP_CIPHER_CTX_free@PLT
	cmpl	$37, %ebx
	je	.L83
.L17:
	movq	(%r14), %rdi
	call	X509_ALGOR_free@PLT
	leaq	SCRYPT_PARAMS_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L18
	movl	-108(%rbp), %ebx
	movl	$8, %eax
	movq	(%r12), %rdi
	testl	%ebx, %ebx
	cmovne	%ebx, %eax
	movq	-104(%rbp), %rbx
	movl	%eax, %edx
	movq	%rbx, %rsi
	movl	%eax, -108(%rbp)
	call	ASN1_STRING_set@PLT
	testl	%eax, %eax
	je	.L18
	testq	%rbx, %rbx
	je	.L20
.L23:
	movq	8(%r12), %rdi
	movq	%r15, %rsi
	call	ASN1_INTEGER_set_uint64@PLT
	testl	%eax, %eax
	je	.L18
	movq	16(%r12), %rdi
	movq	-88(%rbp), %rsi
	call	ASN1_INTEGER_set_uint64@PLT
	testl	%eax, %eax
	je	.L18
	movq	24(%r12), %rdi
	movq	16(%rbp), %rsi
	call	ASN1_INTEGER_set_uint64@PLT
	testl	%eax, %eax
	je	.L18
	testq	%r13, %r13
	je	.L26
	call	ASN1_INTEGER_new@PLT
	movq	%rax, 32(%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L18
	movq	%r13, %rsi
	call	ASN1_INTEGER_set_int64@PLT
	testl	%eax, %eax
	je	.L18
.L26:
	call	X509_ALGOR_new@PLT
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L18
	movl	$973, %edi
	call	OBJ_nid2obj@PLT
	movq	-104(%rbp), %rbx
	movq	%r12, %rsi
	leaq	SCRYPT_PARAMS_it(%rip), %rdi
	movq	%rax, (%rbx)
	leaq	8(%rbx), %rdx
	call	ASN1_TYPE_pack_sequence@PLT
	testq	%rax, %rax
	je	.L27
	movq	%r12, %rdi
	leaq	SCRYPT_PARAMS_it(%rip), %rsi
	call	ASN1_item_free@PLT
	movq	%rbx, (%r14)
	call	X509_ALGOR_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L11
	movl	$161, %edi
	call	OBJ_nid2obj@PLT
	leaq	8(%r12), %rdx
	movq	%r14, %rsi
	leaq	PBE2PARAM_it(%rip), %rdi
	movq	%rax, (%r12)
	call	ASN1_TYPE_pack_sequence@PLT
	testq	%rax, %rax
	je	.L29
	movq	%r14, %rdi
	call	PBE2PARAM_free@PLT
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L11:
	xorl	%r12d, %r12d
.L29:
	movl	$138, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$231, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
.L8:
	movq	%r14, %rdi
	call	PBE2PARAM_free@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	X509_ALGOR_free@PLT
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_free@PLT
.L6:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L84
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	movl	$64, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$108, %edx
.L79:
	movl	$231, %esi
	movl	$13, %edi
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	call	ERR_put_error@PLT
	xorl	%r12d, %r12d
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L18:
	movq	$0, -104(%rbp)
.L27:
	movl	$205, %r8d
	movl	$65, %edx
	movl	$232, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L22:
	movq	%r12, %rdi
	leaq	SCRYPT_PARAMS_it(%rip), %rsi
	xorl	%r12d, %r12d
	call	ASN1_item_free@PLT
	movq	-104(%rbp), %rdi
	call	X509_ALGOR_free@PLT
	movq	$0, (%r14)
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L14:
	call	EVP_CIPHER_iv_length@PLT
	leaq	-80(%rbp), %rdi
	movl	%eax, %esi
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jg	.L13
	.p2align 4,,10
	.p2align 3
.L33:
	xorl	%r12d, %r12d
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L80:
	movl	$67, %edx
	movl	$231, %esi
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	movl	$52, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$13, %edi
	call	ERR_put_error@PLT
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L20:
	movq	(%r12), %rax
	movl	-108(%rbp), %esi
	movq	8(%rax), %rdi
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jg	.L23
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L83:
	movq	%r12, %rdi
	call	EVP_CIPHER_key_length@PLT
	movslq	%eax, %r13
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L82:
	movl	$97, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$114, %edx
	xorl	%r12d, %r12d
	movl	$231, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
	jmp	.L8
.L84:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE807:
	.size	PKCS5_pbe2_set_scrypt, .-PKCS5_pbe2_set_scrypt
	.p2align 4
	.globl	PKCS5_v2_scrypt_keyivgen
	.type	PKCS5_v2_scrypt_keyivgen, @function
PKCS5_v2_scrypt_keyivgen:
.LFB809:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_CIPHER_CTX_cipher@PLT
	testq	%rax, %rax
	je	.L118
	movq	%r15, %rsi
	leaq	SCRYPT_PARAMS_it(%rip), %rdi
	call	ASN1_TYPE_unpack_sequence@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L119
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	32(%r12), %rsi
	leaq	-136(%rbp), %rdi
	movslq	%eax, %r15
	testq	%rsi, %rsi
	je	.L89
	movq	%rdi, -168(%rbp)
	call	ASN1_INTEGER_get_uint64@PLT
	testl	%eax, %eax
	je	.L90
	cmpq	%r15, -136(%rbp)
	movq	-168(%rbp), %rdi
	je	.L89
.L90:
	movl	$245, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$123, %edx
	xorl	%r14d, %r14d
	movl	$180, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
.L92:
	testq	%r15, %r15
	je	.L87
	leaq	-128(%rbp), %rdi
	movq	%r15, %rsi
	call	OPENSSL_cleanse@PLT
.L87:
	leaq	SCRYPT_PARAMS_it(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_item_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L120
	leaq	-40(%rbp), %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	movq	8(%r12), %rsi
	call	ASN1_INTEGER_get_uint64@PLT
	testl	%eax, %eax
	je	.L94
	movq	16(%r12), %rsi
	leaq	-144(%rbp), %rdi
	call	ASN1_INTEGER_get_uint64@PLT
	testl	%eax, %eax
	jne	.L121
.L94:
	movl	$255, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$171, %edx
	xorl	%r14d, %r14d
	movl	$180, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L121:
	movq	24(%r12), %rsi
	leaq	-152(%rbp), %rdi
	call	ASN1_INTEGER_get_uint64@PLT
	testl	%eax, %eax
	je	.L94
	pushq	$0
	movq	-144(%rbp), %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	$0
	movq	-136(%rbp), %r8
	xorl	%esi, %esi
	xorl	%edi, %edi
	pushq	$0
	pushq	-152(%rbp)
	call	EVP_PBE_scrypt@PLT
	addq	$32, %rsp
	testl	%eax, %eax
	je	.L94
	movq	(%r12), %rax
	movslq	%ebx, %rsi
	leaq	-128(%rbp), %rbx
	movq	%r14, %rdi
	movq	-144(%rbp), %r9
	movq	-136(%rbp), %r8
	movslq	(%rax), %rcx
	movq	8(%rax), %rdx
	pushq	%r15
	pushq	%rbx
	pushq	$0
	pushq	-152(%rbp)
	call	EVP_PBE_scrypt@PLT
	addq	$32, %rsp
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L92
	movl	16(%rbp), %r9d
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	EVP_CipherInit_ex@PLT
	movl	%eax, %r14d
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L118:
	movl	$131, %edx
	movl	$180, %esi
	movq	%rax, %r12
	xorl	%r14d, %r14d
	movl	$224, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$6, %edi
	call	ERR_put_error@PLT
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L119:
	movl	$233, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$114, %edx
	xorl	%r14d, %r14d
	movl	$180, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	jmp	.L87
.L120:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE809:
	.size	PKCS5_v2_scrypt_keyivgen, .-PKCS5_v2_scrypt_keyivgen
	.globl	SCRYPT_PARAMS_it
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"SCRYPT_PARAMS"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	SCRYPT_PARAMS_it, @object
	.size	SCRYPT_PARAMS_it, 56
SCRYPT_PARAMS_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	SCRYPT_PARAMS_seq_tt
	.quad	5
	.quad	0
	.quad	40
	.quad	.LC1
	.section	.rodata.str1.1
.LC2:
	.string	"salt"
.LC3:
	.string	"costParameter"
.LC4:
	.string	"blockSize"
.LC5:
	.string	"parallelizationParameter"
.LC6:
	.string	"keyLength"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	SCRYPT_PARAMS_seq_tt, @object
	.size	SCRYPT_PARAMS_seq_tt, 200
SCRYPT_PARAMS_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC2
	.quad	ASN1_OCTET_STRING_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC3
	.quad	ASN1_INTEGER_it
	.quad	0
	.quad	0
	.quad	16
	.quad	.LC4
	.quad	ASN1_INTEGER_it
	.quad	0
	.quad	0
	.quad	24
	.quad	.LC5
	.quad	ASN1_INTEGER_it
	.quad	1
	.quad	0
	.quad	32
	.quad	.LC6
	.quad	ASN1_INTEGER_it
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
