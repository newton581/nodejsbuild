	.file	"a_object.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/asn1/a_object.c"
	.text
	.p2align 4
	.globl	i2d_ASN1_OBJECT
	.type	i2d_ASN1_OBJECT, @function
i2d_ASN1_OBJECT:
.LFB491:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L7
	xorl	%r13d, %r13d
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L1
	movq	%rsi, %r12
	movl	20(%rdi), %esi
	movl	$6, %edx
	xorl	%edi, %edi
	call	ASN1_object_size@PLT
	movl	%eax, %r13d
	testq	%r12, %r12
	je	.L1
	cmpl	$-1, %eax
	je	.L1
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L11
	movl	20(%rbx), %edx
	leaq	-48(%rbp), %rdi
	xorl	%r8d, %r8d
	movl	$6, %ecx
	xorl	%esi, %esi
	movq	%rax, -48(%rbp)
	call	ASN1_put_object@PLT
	movslq	20(%rbx), %rdx
	movq	24(%rbx), %rsi
	movq	-48(%rbp), %rdi
	call	memcpy@PLT
	movslq	20(%rbx), %r14
	addq	-48(%rbp), %r14
.L5:
	movq	%r14, (%r12)
.L1:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L12
	addq	$16, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	xorl	%r13d, %r13d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L11:
	movslq	%r13d, %rdi
	movl	$34, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, -48(%rbp)
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L13
	movl	20(%rbx), %edx
	leaq	-48(%rbp), %rdi
	xorl	%esi, %esi
	xorl	%r8d, %r8d
	movl	$6, %ecx
	call	ASN1_put_object@PLT
	movslq	20(%rbx), %rdx
	movq	24(%rbx), %rsi
	movq	-48(%rbp), %rdi
	call	memcpy@PLT
	jmp	.L5
.L13:
	movl	$35, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$143, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
	jmp	.L1
.L12:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE491:
	.size	i2d_ASN1_OBJECT, .-i2d_ASN1_OBJECT
	.p2align 4
	.globl	a2d_ASN1_OBJECT
	.type	a2d_ASN1_OBJECT, @function
a2d_ASN1_OBJECT:
.LFB492:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -104(%rbp)
	movl	%esi, -116(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	je	.L14
	movq	%rdx, %rbx
	cmpl	$-1, %ecx
	je	.L125
	movsbl	(%rbx), %eax
	subl	$48, %eax
	movl	%eax, -108(%rbp)
	cmpl	$2, %eax
	ja	.L17
.L134:
	cmpl	$1, %ecx
	jle	.L126
	subl	$2, %ecx
	movsbl	1(%rbx), %eax
	leaq	2(%rbx), %r14
	movl	%ecx, %r12d
	je	.L58
	cmpl	$32, %eax
	je	.L63
	cmpl	$46, %eax
	jne	.L59
.L63:
	movslq	-108(%rbp), %rax
	movl	$24, -120(%rbp)
	xorl	%r13d, %r13d
	movl	$0, -112(%rbp)
	movq	%rax, %rcx
	leaq	(%rax,%rax,4), %rax
	salq	$3, %rax
	movq	%rax, -136(%rbp)
	leal	(%rcx,%rcx,4), %eax
	sall	$3, %eax
	cltq
	movq	%rax, -144(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	%rax, -96(%rbp)
.L24:
	xorl	%r15d, %r15d
	xorl	%edx, %edx
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L129:
	testq	%r13, %r13
	je	.L127
.L32:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	BN_set_word@PLT
	testl	%eax, %eax
	je	.L28
.L33:
	movl	$10, %esi
	movq	%r13, %rdi
	call	BN_mul_word@PLT
	testl	%eax, %eax
	je	.L28
	leal	-48(%rbx), %esi
	movq	%r13, %rdi
	movslq	%esi, %rsi
	call	BN_add_word@PLT
	testl	%eax, %eax
	je	.L28
	movl	$1, %edx
	testl	%r12d, %r12d
	je	.L29
.L27:
	movsbl	(%r14), %ebx
	addq	$1, %r14
	subl	$1, %r12d
	cmpl	$32, %ebx
	je	.L29
	cmpl	$46, %ebx
	je	.L29
	movl	$4, %esi
	movl	%ebx, %edi
	movl	%edx, -88(%rbp)
	call	ossl_ctype_check@PLT
	movl	-88(%rbp), %edx
	testl	%eax, %eax
	je	.L128
	testl	%edx, %edx
	jne	.L33
	movabsq	$1844674407370955152, %rax
	cmpq	%rax, %r15
	ja	.L129
	testl	%edx, %edx
	jne	.L33
	leal	-48(%rbx), %eax
	leaq	(%r15,%r15,4), %rcx
	cltq
	leaq	(%rax,%rcx,2), %r15
	testl	%r12d, %r12d
	jne	.L27
	.p2align 4,,10
	.p2align 3
.L29:
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	jne	.L36
	cmpl	$1, -108(%rbp)
	jg	.L37
	cmpq	$39, %r15
	ja	.L130
.L37:
	addq	-136(%rbp), %r15
	testl	%edx, %edx
	jne	.L131
.L41:
	movq	-96(%rbp), %rdx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L52:
	movl	%r15d, %ecx
	addq	$1, %rdx
	movl	%eax, %r8d
	andl	$127, %ecx
	leal	1(%rax), %eax
	movb	%cl, -1(%rdx)
	shrq	$7, %r15
	jne	.L52
	addl	-112(%rbp), %eax
.L53:
	cmpq	$0, -104(%rbp)
	jne	.L132
	movl	%eax, -112(%rbp)
.L49:
	testl	%r12d, %r12d
	je	.L25
	cmpl	$46, %ebx
	je	.L24
	cmpl	$32, %ebx
	je	.L24
.L22:
	movl	$87, %r8d
	movl	$131, %edx
	movl	$100, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L28:
	movq	-128(%rbp), %rcx
	cmpq	%rcx, -96(%rbp)
	je	.L20
.L44:
	movq	-96(%rbp), %rdi
	movl	$175, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
.L20:
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	call	BN_free@PLT
.L14:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L133
	addq	$120, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_restore_state
	movq	%rdx, %rdi
	call	strlen@PLT
	movl	%eax, %ecx
	movsbl	(%rbx), %eax
	subl	$48, %eax
	movl	%eax, -108(%rbp)
	cmpl	$2, %eax
	jbe	.L134
.L17:
	movl	$73, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$122, %edx
	xorl	%r13d, %r13d
	movl	$100, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L127:
	call	BN_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L32
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L36:
	testl	%edx, %edx
	je	.L41
.L42:
	movq	%r13, %rdi
	call	BN_num_bits@PLT
	addl	$6, %eax
	movslq	%eax, %r15
	imulq	$-1840700269, %r15, %r15
	shrq	$32, %r15
	addl	%eax, %r15d
	sarl	$31, %eax
	sarl	$2, %r15d
	subl	%eax, %r15d
	cmpl	-120(%rbp), %r15d
	jle	.L135
	movq	-96(%rbp), %rax
	movq	-128(%rbp), %rcx
	cmpq	%rcx, %rax
	je	.L43
	movl	$136, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	call	CRYPTO_free@PLT
.L43:
	leal	32(%r15), %eax
	movl	$138, %edx
	leaq	.LC0(%rip), %rsi
	movslq	%eax, %rdi
	movl	%eax, -120(%rbp)
	call	CRYPTO_malloc@PLT
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L44
	leal	-1(%r15), %eax
.L48:
	xorl	%r15d, %r15d
	movq	%r14, -88(%rbp)
	movq	-96(%rbp), %r14
	movl	%r12d, -152(%rbp)
	movq	%r15, %r12
	movq	%rax, %r15
	movl	%ebx, -148(%rbp)
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L137:
	movb	%al, (%r14,%r12)
	leal	1(%r12), %ecx
	leaq	1(%r12), %rax
	cmpq	%r12, %r15
	je	.L136
	movq	%rax, %r12
.L46:
	movl	$128, %esi
	movq	%r13, %rdi
	call	BN_div_word@PLT
	cmpq	$-1, %rax
	jne	.L137
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L126:
	movl	$78, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$138, %edx
	xorl	%r13d, %r13d
	movl	$100, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L128:
	movl	$100, %r8d
	movl	$130, %edx
	movl	$100, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L132:
	cmpl	%eax, -116(%rbp)
	jl	.L50
	movl	-112(%rbp), %eax
	testl	%r8d, %r8d
	je	.L51
	movslq	-112(%rbp), %rdx
	movq	-104(%rbp), %rdi
	movslq	%r8d, %rcx
	addq	-96(%rbp), %rcx
	leaq	1(%rdi,%rdx), %rsi
	leaq	(%rdi,%rdx), %rax
	leal	-1(%r8), %edx
	movq	%rdx, %r8
	addq	%rdx, %rsi
	.p2align 4,,10
	.p2align 3
.L55:
	movzbl	(%rcx), %edx
	addq	$1, %rax
	subq	$1, %rcx
	orl	$-128, %edx
	movb	%dl, -1(%rax)
	cmpq	%rax, %rsi
	jne	.L55
	movl	-112(%rbp), %eax
	leal	1(%rax,%r8), %eax
.L51:
	movq	-96(%rbp), %rcx
	leal	1(%rax), %edi
	cltq
	movl	%edi, -112(%rbp)
	movq	-104(%rbp), %rdi
	movzbl	(%rcx), %edx
	movb	%dl, (%rdi,%rax)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L135:
	leal	-1(%r15), %eax
	testl	%r15d, %r15d
	jne	.L48
	cmpq	$0, -104(%rbp)
	je	.L49
	movl	-112(%rbp), %eax
	movl	-116(%rbp), %ecx
	cmpl	%ecx, %eax
	jle	.L51
.L50:
	movl	$160, %r8d
	movl	$107, %edx
	movl	$100, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L136:
	movl	-112(%rbp), %eax
	movl	%r12d, %r8d
	movq	-88(%rbp), %r14
	movl	-148(%rbp), %ebx
	movl	-152(%rbp), %r12d
	addl	%ecx, %eax
	jmp	.L53
.L131:
	movq	-144(%rbp), %rsi
	movq	%r13, %rdi
	call	BN_add_word@PLT
	testl	%eax, %eax
	jne	.L42
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L58:
	xorl	%r13d, %r13d
.L21:
	movq	%r13, %rdi
	call	BN_free@PLT
	jmp	.L14
.L59:
	leaq	-80(%rbp), %rax
	xorl	%r13d, %r13d
	movq	%rax, -128(%rbp)
	movq	%rax, -96(%rbp)
	jmp	.L22
.L25:
	movq	-96(%rbp), %rax
	movq	-128(%rbp), %rdi
	cmpq	%rdi, %rax
	je	.L62
	movl	$170, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	call	CRYPTO_free@PLT
	movl	-112(%rbp), %r12d
	jmp	.L21
.L130:
	movl	$119, %r8d
	movl	$147, %edx
	movl	$100, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L28
.L133:
	call	__stack_chk_fail@PLT
.L62:
	movl	-112(%rbp), %r12d
	jmp	.L21
	.cfi_endproc
.LFE492:
	.size	a2d_ASN1_OBJECT, .-a2d_ASN1_OBJECT
	.p2align 4
	.globl	i2t_ASN1_OBJECT
	.type	i2t_ASN1_OBJECT, @function
i2t_ASN1_OBJECT:
.LFB493:
	.cfi_startproc
	endbr64
	xorl	%ecx, %ecx
	jmp	OBJ_obj2txt@PLT
	.cfi_endproc
.LFE493:
	.size	i2t_ASN1_OBJECT, .-i2t_ASN1_OBJECT
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"NULL"
.LC2:
	.string	"<INVALID>"
	.text
	.p2align 4
	.globl	i2a_ASN1_OBJECT
	.type	i2a_ASN1_OBJECT, @function
i2a_ASN1_OBJECT:
.LFB494:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L140
	cmpq	$0, 24(%rsi)
	movq	%rsi, %r13
	je	.L140
	leaq	-144(%rbp), %rbx
	movq	%rsi, %rdx
	xorl	%ecx, %ecx
	movl	$80, %esi
	movq	%rbx, %rdi
	call	OBJ_obj2txt@PLT
	movl	%eax, %r12d
	cmpl	$79, %eax
	jg	.L154
	testl	%eax, %eax
	jg	.L152
	movl	$9, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	BIO_write@PLT
	movl	20(%r13), %edx
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	movl	%eax, %r12d
	call	BIO_dump@PLT
	addl	%eax, %r12d
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L154:
	leal	1(%rax), %r8d
	movl	$194, %edx
	leaq	.LC0(%rip), %rsi
	movslq	%r8d, %rdi
	movl	%r8d, -148(%rbp)
	call	CRYPTO_malloc@PLT
	movl	-148(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L155
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	%r8d, %esi
	movq	%rax, %rdi
	call	OBJ_obj2txt@PLT
	movl	%r12d, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	BIO_write@PLT
	cmpq	%rbx, %r15
	je	.L139
	movl	$207, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	call	CRYPTO_free@PLT
.L139:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L156
	addq	$120, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	movl	$4, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	call	BIO_write@PLT
	movl	%eax, %r12d
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L152:
	movl	%r12d, %edx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	BIO_write@PLT
	jmp	.L139
.L155:
	movl	$195, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$126, %esi
	movl	$13, %edi
	orl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L139
.L156:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE494:
	.size	i2a_ASN1_OBJECT, .-i2a_ASN1_OBJECT
	.p2align 4
	.globl	ASN1_OBJECT_new
	.type	ASN1_OBJECT_new, @function
ASN1_OBJECT_new:
.LFB497:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$339, %edx
	movl	$40, %edi
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L161
	movl	$1, 32(%rax)
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	movl	$341, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$123, %esi
	movl	$13, %edi
	movq	%rax, -8(%rbp)
	call	ERR_put_error@PLT
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE497:
	.size	ASN1_OBJECT_new, .-ASN1_OBJECT_new
	.p2align 4
	.globl	ASN1_OBJECT_free
	.type	ASN1_OBJECT_free, @function
ASN1_OBJECT_free:
.LFB498:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L178
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movl	32(%rdi), %eax
	testb	$4, %al
	jne	.L181
	testb	$8, %al
	jne	.L182
.L166:
	testb	$1, %al
	jne	.L183
.L162:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	.cfi_restore_state
	movq	(%rdi), %rdi
	movl	$356, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	8(%r12), %rdi
	movl	$357, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	32(%r12), %eax
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%r12)
	testb	$8, %al
	je	.L166
.L182:
	movq	24(%r12), %rdi
	movl	$362, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	32(%r12), %eax
	movq	$0, 24(%r12)
	movl	$0, 20(%r12)
	testb	$1, %al
	je	.L162
.L183:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$367, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L178:
	ret
	.cfi_endproc
.LFE498:
	.size	ASN1_OBJECT_free, .-ASN1_OBJECT_free
	.p2align 4
	.globl	c2i_ASN1_OBJECT
	.type	c2i_ASN1_OBJECT, @function
c2i_ASN1_OBJECT:
.LFB496:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L185
	leaq	-1(%rdx), %rax
	cmpq	$2147483646, %rax
	ja	.L185
	movq	(%rsi), %r13
	movq	%rsi, %r15
	testq	%r13, %r13
	je	.L185
	cmpb	$0, -1(%r13,%rdx)
	movq	%rdx, %rbx
	js	.L185
	movq	%rdi, %r12
	leaq	-96(%rbp), %rdi
	movq	%r13, -72(%rbp)
	movl	%edx, %r14d
	movl	$0, -80(%rbp)
	movl	%edx, -76(%rbp)
	movl	$0, -64(%rbp)
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	testl	%eax, %eax
	je	.L203
	call	OBJ_nid2obj@PLT
	movq	%rax, %r13
	testq	%r12, %r12
	je	.L189
	movq	(%r12), %rdi
	call	ASN1_OBJECT_free
	movq	%r13, (%r12)
.L189:
	addq	%rbx, (%r15)
.L184:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L238
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	.cfi_restore_state
	movl	$254, %r8d
.L236:
	movl	$216, %edx
	movl	$196, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L237:
	xorl	%r13d, %r13d
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L203:
	xorl	%eax, %eax
.L188:
	cmpb	$-128, 0(%r13,%rax)
	je	.L239
.L190:
	addq	$1, %rax
	cmpl	%eax, %r14d
	jg	.L188
	testq	%r12, %r12
	je	.L192
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L192
	testb	$1, 32(%r13)
	jne	.L193
.L192:
	movl	$339, %edx
	leaq	.LC0(%rip), %rsi
	movl	$40, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L240
	movl	$1, 32(%r13)
.L193:
	movq	24(%r13), %rdi
	movq	(%r15), %rcx
	movslq	%ebx, %r14
	movq	$0, 24(%r13)
	testq	%rdi, %rdi
	je	.L194
	cmpl	%ebx, 20(%r13)
	jge	.L195
.L194:
	movl	$307, %edx
	leaq	.LC0(%rip), %rsi
	movl	$0, 20(%r13)
	movq	%rcx, -104(%rbp)
	call	CRYPTO_free@PLT
	movq	%r14, %rdi
	movl	$308, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	-104(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L241
	orl	$8, 32(%r13)
.L195:
	movq	%r14, %rdx
	movq	%rcx, %rsi
	movq	%rcx, -104(%rbp)
	call	memcpy@PLT
	movq	-104(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movl	%ebx, 20(%r13)
	movq	%rax, 24(%r13)
	movups	%xmm0, 0(%r13)
	addq	%rcx, %r14
	testq	%r12, %r12
	je	.L199
	movq	%r13, (%r12)
.L199:
	movq	%r14, (%r15)
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L239:
	testq	%rax, %rax
	jne	.L242
.L191:
	movl	$284, %r8d
	jmp	.L236
.L241:
	movl	$329, %r8d
	movl	$65, %edx
	movl	$196, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	testq	%r12, %r12
	je	.L197
	cmpq	%r13, (%r12)
	je	.L237
.L197:
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	ASN1_OBJECT_free
	jmp	.L184
.L240:
	movl	$341, %r8d
	movl	$65, %edx
	movl	$123, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L184
.L238:
	call	__stack_chk_fail@PLT
.L242:
	cmpb	$0, -1(%r13,%rax)
	js	.L190
	jmp	.L191
	.cfi_endproc
.LFE496:
	.size	c2i_ASN1_OBJECT, .-c2i_ASN1_OBJECT
	.p2align 4
	.globl	d2i_ASN1_OBJECT
	.type	d2i_ASN1_OBJECT, @function
d2i_ASN1_OBJECT:
.LFB495:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-116(%rbp), %rcx
	leaq	-120(%rbp), %rdx
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-112(%rbp), %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	leaq	-104(%rbp), %rsi
	movq	%rax, -112(%rbp)
	call	ASN1_get_object@PLT
	testb	$-128, %al
	jne	.L265
	cmpl	$6, -120(%rbp)
	jne	.L266
	movq	-104(%rbp), %r14
	leaq	-1(%r14), %rax
	cmpq	$2147483646, %rax
	ja	.L245
	movq	-112(%rbp), %r13
	testq	%r13, %r13
	je	.L245
	cmpb	$0, -1(%r13,%r14)
	js	.L245
	leaq	-96(%rbp), %rdi
	movl	$0, -80(%rbp)
	movl	%r14d, %r15d
	movq	%r13, -72(%rbp)
	movl	%r14d, -76(%rbp)
	movl	$0, -64(%rbp)
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	testl	%eax, %eax
	je	.L267
	call	OBJ_nid2obj@PLT
	movq	%rax, %r13
	testq	%r12, %r12
	je	.L249
	movq	(%r12), %rdi
	call	ASN1_OBJECT_free
	movq	%r13, (%r12)
.L249:
	addq	-112(%rbp), %r14
	testq	%r13, %r13
	je	.L243
.L250:
	movq	%r14, (%rbx)
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L266:
	movl	$116, %edx
.L244:
	movl	$235, %r8d
	movl	$147, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L299:
	xorl	%r13d, %r13d
.L243:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L301
	addq	$104, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L267:
	.cfi_restore_state
	xorl	%eax, %eax
.L248:
	cmpb	$-128, 0(%r13,%rax)
	je	.L302
.L251:
	addq	$1, %rax
	cmpl	%eax, %r15d
	jg	.L248
	testq	%r12, %r12
	je	.L253
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L253
	testb	$1, 32(%r13)
	jne	.L254
.L253:
	movl	$339, %edx
	leaq	.LC0(%rip), %rsi
	movl	$40, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L303
	movl	$1, 32(%r13)
.L254:
	movq	24(%r13), %rdi
	movq	-112(%rbp), %rcx
	movslq	%r14d, %r15
	movq	$0, 24(%r13)
	testq	%rdi, %rdi
	je	.L255
	cmpl	20(%r13), %r14d
	jle	.L256
.L255:
	movl	$0, 20(%r13)
	movl	$307, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rcx, -136(%rbp)
	call	CRYPTO_free@PLT
	movq	%r15, %rdi
	movl	$308, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	-136(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L304
	orl	$8, 32(%r13)
.L256:
	movq	%rcx, %rsi
	movq	%r15, %rdx
	movq	%rcx, -136(%rbp)
	call	memcpy@PLT
	movq	-136(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movl	%r14d, 20(%r13)
	movq	%rax, 24(%r13)
	movups	%xmm0, 0(%r13)
	leaq	(%rcx,%r15), %r14
	testq	%r12, %r12
	je	.L250
	movq	%r13, (%r12)
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L265:
	movl	$102, %edx
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L245:
	movl	$254, %r8d
.L300:
	leaq	.LC0(%rip), %rcx
	movl	$216, %edx
	movl	$196, %esi
	xorl	%r13d, %r13d
	movl	$13, %edi
	call	ERR_put_error@PLT
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L302:
	testq	%rax, %rax
	jne	.L305
.L252:
	movl	$284, %r8d
	jmp	.L300
.L304:
	movl	$329, %r8d
	movl	$65, %edx
	movl	$196, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	testq	%r12, %r12
	je	.L258
	cmpq	(%r12), %r13
	je	.L299
.L258:
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	ASN1_OBJECT_free
	jmp	.L243
.L301:
	call	__stack_chk_fail@PLT
.L303:
	movl	$341, %r8d
	movl	$65, %edx
	movl	$123, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L243
.L305:
	cmpb	$0, -1(%r13,%rax)
	js	.L251
	jmp	.L252
	.cfi_endproc
.LFE495:
	.size	d2i_ASN1_OBJECT, .-d2i_ASN1_OBJECT
	.p2align 4
	.globl	ASN1_OBJECT_create
	.type	ASN1_OBJECT_create, @function
ASN1_OBJECT_create:
.LFB499:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$64, %rsp
	movq	%r8, -56(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movhps	-56(%rbp), %xmm0
	movl	%edi, -32(%rbp)
	leaq	-48(%rbp), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	movl	$13, -16(%rbp)
	movaps	%xmm0, -48(%rbp)
	call	OBJ_dup@PLT
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L309
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L309:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE499:
	.size	ASN1_OBJECT_create, .-ASN1_OBJECT_create
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
