	.file	"dtls1_bitmap.c"
	.text
	.p2align 4
	.globl	dtls1_record_replay_check
	.type	dtls1_record_replay_check, @function
dtls1_record_replay_check:
.LFB965:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	6104(%rdi), %rcx
	leaq	6104(%rdi), %r9
	movq	8(%rsi), %rdx
	bswap	%rcx
	movq	%rcx, %rax
	bswap	%rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	%rdx, %rax
	cmpq	%rdx, %rcx
	jbe	.L11
	testq	%rax, %rax
	js	.L20
.L11:
	cmpq	%rdx, %rcx
	jnb	.L12
	testq	%rax, %rax
	jg	.L8
.L12:
	cmpq	$128, %rax
	jg	.L20
	cmpq	$-128, %rax
	jl	.L8
	testl	%eax, %eax
	jg	.L20
	xorl	%r8d, %r8d
	cmpl	$-63, %eax
	jl	.L1
	movq	(%rsi), %rdx
	negl	%eax
	btq	%rax, %rdx
	jnc	.L20
.L1:
	movl	%r8d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	addq	$3464, %rdi
	movq	%r9, %rsi
	call	SSL3_RECORD_set_seq_num@PLT
	movl	$1, %r8d
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	xorl	%r8d, %r8d
	jmp	.L1
	.cfi_endproc
.LFE965:
	.size	dtls1_record_replay_check, .-dtls1_record_replay_check
	.p2align 4
	.globl	dtls1_record_bitmap_update
	.type	dtls1_record_bitmap_update, @function
dtls1_record_bitmap_update:
.LFB966:
	.cfi_startproc
	endbr64
	movq	6104(%rdi), %rdx
	movq	8(%rsi), %rax
	bswap	%rdx
	movq	%rdx, %rcx
	bswap	%rax
	subq	%rax, %rcx
	cmpq	%rax, %rdx
	jbe	.L31
	testq	%rcx, %rcx
	js	.L30
.L31:
	cmpq	%rax, %rdx
	jnb	.L32
	testq	%rcx, %rcx
	jg	.L22
.L32:
	cmpq	$128, %rcx
	jg	.L30
	cmpq	$-128, %rcx
	jl	.L22
	testl	%ecx, %ecx
	jg	.L39
	cmpl	$-63, %ecx
	jl	.L22
	negl	%ecx
	movl	$1, %eax
	salq	%cl, %rax
	orq	%rax, (%rsi)
.L22:
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	movl	$1, %eax
	movq	%rax, (%rsi)
	movq	6104(%rdi), %rax
	movq	%rax, 8(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	cmpl	$63, %ecx
	jg	.L30
	movq	(%rsi), %rax
	salq	%cl, %rax
	orq	$1, %rax
	movq	%rax, (%rsi)
	movq	6104(%rdi), %rax
	movq	%rax, 8(%rsi)
	ret
	.cfi_endproc
.LFE966:
	.size	dtls1_record_bitmap_update, .-dtls1_record_bitmap_update
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
