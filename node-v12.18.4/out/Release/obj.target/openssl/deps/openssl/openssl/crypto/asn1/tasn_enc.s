	.file	"tasn_enc.c"
	.text
	.p2align 4
	.type	der_cmp, @function
der_cmp:
.LFB520:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movslq	8(%rsi), %r12
	movl	8(%rdi), %ebx
	movq	(%rsi), %rsi
	movq	(%rdi), %rdi
	cmpl	%ebx, %r12d
	movslq	%ebx, %rdx
	cmovle	%r12, %rdx
	subl	%r12d, %ebx
	call	memcmp@PLT
	testl	%eax, %eax
	cmove	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE520:
	.size	der_cmp, .-der_cmp
	.p2align 4
	.type	asn1_i2d_ex_primitive, @function
asn1_i2d_ex_primitive:
.LFB522:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	8(%rdx), %rdx
	movq	32(%r12), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%edx, -68(%rbp)
	movl	%edx, %eax
	testq	%rcx, %rcx
	je	.L6
	movq	48(%rcx), %r10
	testq	%r10, %r10
	je	.L6
	movl	%r8d, -96(%rbp)
	leaq	-68(%rbp), %rdx
	movq	%r12, %rcx
	xorl	%esi, %esi
	movq	%rdi, -88(%rbp)
	call	*%r10
.L121:
	movl	%eax, %r13d
	movl	-68(%rbp), %eax
	movq	-88(%rbp), %r9
	movl	-96(%rbp), %r8d
	leal	-16(%rax), %edx
	cmpl	$1, %edx
	setbe	%bl
	cmpl	$-3, %eax
	sete	%dl
	orl	%edx, %ebx
.L7:
	xorl	$1, %ebx
	movzbl	%bl, %ebx
	cmpl	$-1, %r13d
	je	.L22
.L131:
	xorl	%r10d, %r10d
	cmpl	$-2, %r13d
	je	.L122
.L17:
	cmpl	$-1, %r14d
	cmove	%eax, %r14d
	testq	%r15, %r15
	je	.L24
	testl	%ebx, %ebx
	jne	.L124
.L25:
	movq	(%r15), %r8
	movq	32(%r12), %rax
	movq	%r8, -64(%rbp)
	testq	%rax, %rax
	je	.L26
	movq	48(%rax), %rax
	testq	%rax, %rax
	je	.L26
	movl	%r10d, -88(%rbp)
	leaq	-68(%rbp), %rdx
	movq	%r12, %rcx
	movq	%r8, %rsi
	movq	%r9, %rdi
	call	*%rax
	movl	-88(%rbp), %r10d
.L27:
	testl	%r10d, %r10d
	jne	.L125
	movslq	%r13d, %rax
	addq	%rax, (%r15)
.L24:
	testl	%ebx, %ebx
	jne	.L126
.L5:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L127
	addq	$56, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movzbl	(%r12), %ecx
	testb	%cl, %cl
	jne	.L59
	cmpq	$1, %rdx
	je	.L51
.L59:
	movq	(%r9), %rsi
	testq	%rsi, %rsi
	je	.L22
	cmpb	$5, %cl
	je	.L128
	movq	%r9, %rdi
	cmpq	$-4, %rdx
	jne	.L12
	movl	(%rsi), %eax
	leaq	8(%rsi), %rdi
	movl	%eax, -68(%rbp)
	cmpl	$10, %eax
	jbe	.L129
	.p2align 4,,10
	.p2align 3
.L13:
	leal	-16(%rax), %ecx
	movq	(%rdi), %rdx
	cmpl	$1, %ecx
	setbe	%bl
	cmpl	$-3, %eax
	sete	%cl
	orl	%ecx, %ebx
	cmpq	$2048, 40(%r12)
	je	.L130
.L20:
	movl	(%rdx), %r13d
	xorl	$1, %ebx
	movzbl	%bl, %ebx
	cmpl	$-1, %r13d
	jne	.L131
	.p2align 4,,10
	.p2align 3
.L22:
	xorl	%r13d, %r13d
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L128:
	movl	4(%rsi), %eax
	movq	%r9, %rdi
	movl	%eax, -68(%rbp)
.L12:
	cmpl	$10, %eax
	ja	.L13
.L129:
	leaq	.L15(%rip), %r10
	movl	%eax, %esi
	movslq	(%r10,%rsi,4), %rcx
	addq	%r10, %rcx
	notrack jmp	*%rcx
	.section	.rodata
	.align 4
	.align 4
.L15:
	.long	.L13-.L15
	.long	.L8-.L15
	.long	.L14-.L15
	.long	.L18-.L15
	.long	.L13-.L15
	.long	.L53-.L15
	.long	.L16-.L15
	.long	.L13-.L15
	.long	.L13-.L15
	.long	.L13-.L15
	.long	.L14-.L15
	.text
	.p2align 4,,10
	.p2align 3
.L26:
	movzbl	(%r12), %eax
	testb	%al, %al
	jne	.L28
	movq	8(%r12), %rax
	cmpq	$1, %rax
	je	.L30
	movq	(%r9), %rdx
	testq	%rdx, %rdx
	je	.L27
.L49:
	cmpq	$-4, %rax
	je	.L132
.L30:
	movl	-68(%rbp), %eax
.L33:
	cmpl	$10, %eax
	ja	.L34
	leaq	.L36(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L36:
	.long	.L34-.L36
	.long	.L39-.L36
	.long	.L35-.L36
	.long	.L38-.L36
	.long	.L34-.L36
	.long	.L27-.L36
	.long	.L37-.L36
	.long	.L34-.L36
	.long	.L34-.L36
	.long	.L34-.L36
	.long	.L35-.L36
	.text
	.p2align 4,,10
	.p2align 3
.L130:
	testb	$16, 16(%rdx)
	je	.L20
	xorl	$1, %ebx
	movzbl	%bl, %ebx
	.p2align 4,,10
	.p2align 3
.L122:
	movl	$2, %r10d
	xorl	%r13d, %r13d
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L28:
	movq	(%r9), %rdx
	testq	%rdx, %rdx
	je	.L27
	cmpb	$5, %al
	jne	.L32
	movl	4(%rdx), %eax
	movl	%eax, -68(%rbp)
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L14:
	movq	(%rdi), %rdi
	xorl	%esi, %esi
	movl	%r8d, -96(%rbp)
	movq	%r9, -88(%rbp)
	call	i2c_ASN1_INTEGER@PLT
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L51:
	movq	%r9, %rdi
.L8:
	movl	(%rdi), %r10d
	cmpl	$-1, %r10d
	je	.L22
	cmpq	$-4, %rdx
	je	.L54
	movq	40(%r12), %rax
	testl	%r10d, %r10d
	jne	.L133
	testq	%rax, %rax
	je	.L22
	movl	$1, %eax
	movl	$1, %ebx
	movl	$1, %r13d
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L35:
	leaq	-64(%rbp), %rax
	testq	%r8, %r8
	movq	(%r9), %rdi
	movl	%r10d, -88(%rbp)
	cmovne	%rax, %r8
	movq	%r8, %rsi
	call	i2c_ASN1_INTEGER@PLT
	movl	-88(%rbp), %r10d
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L34:
	cmpq	$2048, 40(%r12)
	movq	(%r9), %rdx
	je	.L134
.L46:
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L27
	testq	%r8, %r8
	je	.L27
	movq	8(%rdx), %rsi
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L18:
	movq	(%rdi), %rdi
	xorl	%esi, %esi
	movl	%r8d, -96(%rbp)
	movq	%r9, -88(%rbp)
	call	i2c_ASN1_BIT_STRING@PLT
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L16:
	movq	(%rdi), %rdx
	movl	20(%rdx), %r13d
	cmpq	$0, 24(%rdx)
	sete	%bl
	testl	%r13d, %r13d
	sete	%dl
	orb	%dl, %bl
	je	.L7
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L53:
	movl	$1, %ebx
	xorl	%r10d, %r10d
	xorl	%r13d, %r13d
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L37:
	movq	(%r9), %rax
	movq	24(%rax), %rsi
	movl	20(%rax), %eax
	testq	%rsi, %rsi
	sete	%cl
	testl	%eax, %eax
	sete	%dl
	orb	%dl, %cl
	jne	.L27
	testq	%r8, %r8
	je	.L27
.L123:
	movslq	%eax, %rdx
	.p2align 4,,10
	.p2align 3
.L41:
	movq	%r8, %rdi
	movl	%r10d, -88(%rbp)
	call	memcpy@PLT
	movl	-88(%rbp), %r10d
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L39:
	movl	(%r9), %eax
	cmpl	$-1, %eax
	je	.L27
	cmpq	$-4, 8(%r12)
	je	.L42
	movq	40(%r12), %rdx
	testl	%eax, %eax
	je	.L43
	testq	%rdx, %rdx
	jg	.L27
.L42:
	movb	%al, -69(%rbp)
	movl	$1, %edx
	leaq	-69(%rbp), %rsi
	testq	%r8, %r8
	jne	.L41
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L38:
	leaq	-64(%rbp), %rax
	testq	%r8, %r8
	movq	(%r9), %rdi
	movl	%r10d, -88(%rbp)
	cmovne	%rax, %r8
	movq	%r8, %rsi
	call	i2c_ASN1_BIT_STRING@PLT
	movl	-88(%rbp), %r10d
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L125:
	movq	%r15, %rdi
	movl	%r10d, -88(%rbp)
	call	ASN1_put_eoc@PLT
	movl	-88(%rbp), %r10d
	testl	%ebx, %ebx
	je	.L5
.L126:
	movl	%r13d, %esi
	movl	%r14d, %edx
	movl	%r10d, %edi
	call	ASN1_object_size@PLT
	movl	%eax, %r13d
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L124:
	movl	%r10d, %esi
	movl	%r14d, %ecx
	movl	%r13d, %edx
	movq	%r15, %rdi
	movq	%r9, -96(%rbp)
	movl	%r10d, -88(%rbp)
	call	ASN1_put_object@PLT
	movq	-96(%rbp), %r9
	movl	-88(%rbp), %r10d
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L132:
	movl	(%rdx), %eax
	leaq	8(%rdx), %r9
	movl	%eax, -68(%rbp)
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L133:
	testq	%rax, %rax
	jg	.L22
.L54:
	movl	$1, %eax
	movl	$1, %ebx
	xorl	%r10d, %r10d
	movl	$1, %r13d
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L32:
	movq	8(%r12), %rax
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L134:
	testb	$16, 16(%rdx)
	je	.L46
	testq	%r8, %r8
	je	.L27
	movq	%r8, 8(%rdx)
	movl	$0, (%rdx)
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L43:
	testq	%rdx, %rdx
	jne	.L42
	jmp	.L27
.L127:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE522:
	.size	asn1_i2d_ex_primitive, .-asn1_i2d_ex_primitive
	.p2align 4
	.globl	ASN1_item_ex_i2d
	.type	ASN1_item_ex_i2d, @function
ASN1_item_ex_i2d:
.LFB518:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$72, %rsp
	movq	%rsi, -80(%rbp)
	movq	32(%rdx), %r9
	movl	%ecx, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	(%rdx), %eax
	testb	%al, %al
	jne	.L215
.L136:
	movq	16(%r15), %rdx
	movl	-88(%rbp), %ecx
	movl	%ebx, %r8d
	testq	%rdx, %rdx
	je	.L214
	movq	-80(%rbp), %rsi
	movq	%r12, %rdi
	call	asn1_template_ex_i2d
	movl	%eax, %r9d
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L215:
	cmpq	$0, (%rdi)
	je	.L175
	testq	%r9, %r9
	je	.L139
	movq	24(%r9), %r13
	testq	%r13, %r13
	je	.L139
	cmpb	$6, %al
	ja	.L175
	leaq	.L166(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L166:
	.long	.L175-.L166
	.long	.L176-.L166
	.long	.L143-.L166
	.long	.L175-.L166
	.long	.L148-.L166
	.long	.L142-.L166
	.long	.L149-.L166
	.text
.L176:
	movl	$1, -68(%rbp)
.L150:
	movq	-80(%rbp), %rsi
	leaq	-60(%rbp), %rdi
	movq	%r15, %rcx
	movq	%r12, %rdx
	call	asn1_enc_restore@PLT
	testl	%eax, %eax
	js	.L175
	movl	-60(%rbp), %r9d
	jne	.L135
	cmpl	$-1, -88(%rbp)
	movl	$0, -60(%rbp)
	jne	.L152
	movl	$16, -88(%rbp)
	andb	$63, %bl
.L152:
	testq	%r13, %r13
	je	.L156
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r12, %rsi
	movl	$6, %edi
	call	*%r13
	testl	%eax, %eax
	je	.L175
.L156:
	xorl	%eax, %eax
	cmpq	$0, 24(%r15)
	movq	16(%r15), %r14
	jle	.L216
	movq	%r13, -104(%rbp)
	movq	%rax, %r13
	movq	%r15, -96(%rbp)
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L218:
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	asn1_get_field_ptr@PLT
	xorl	%esi, %esi
	movl	%ebx, %r8d
	movl	$-1, %ecx
	movq	%rax, %rdi
	movq	%r15, %rdx
	call	asn1_template_ex_i2d
	cmpl	$-1, %eax
	je	.L211
	movl	-60(%rbp), %edx
	movl	$2147483647, %ecx
	subl	%edx, %ecx
	cmpl	%eax, %ecx
	jl	.L170
	movq	-96(%rbp), %rsi
	addl	%edx, %eax
	addq	$40, %r14
	addq	$1, %r13
	movl	%eax, -60(%rbp)
	cmpq	%r13, 24(%rsi)
	jle	.L217
.L154:
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	asn1_do_adb@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L218
.L175:
	xorl	%r9d, %r9d
.L135:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L219
	addq	$72, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	cmpb	$6, %al
	ja	.L175
	leaq	.L165(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L165:
	.long	.L136-.L165
	.long	.L173-.L165
	.long	.L162-.L165
	.long	.L175-.L165
	.long	.L148-.L165
	.long	.L142-.L165
	.long	.L174-.L165
	.text
.L174:
	xorl	%r13d, %r13d
.L149:
	xorl	%eax, %eax
	testb	$8, %bh
	setne	%al
	addl	$1, %eax
	movl	%eax, -68(%rbp)
	jmp	.L150
.L173:
	movl	$1, -68(%rbp)
	xorl	%r13d, %r13d
	jmp	.L150
.L142:
	movl	%ebx, %r8d
	movl	$-1, %ecx
.L214:
	movq	-80(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	asn1_i2d_ex_primitive
	movl	%eax, %r9d
	jmp	.L135
.L148:
	movl	-88(%rbp), %ecx
	movq	-80(%rbp), %rsi
	movl	%ebx, %r8d
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	*40(%r9)
	movl	%eax, %r9d
	jmp	.L135
.L143:
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r12, %rsi
	movl	$6, %edi
	call	*%r13
	testl	%eax, %eax
	je	.L175
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	asn1_get_choice_selector@PLT
	testl	%eax, %eax
	js	.L147
	cltq
	cmpq	24(%r15), %rax
	jl	.L146
.L147:
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r12, %rsi
	movl	$7, %edi
	call	*%r13
	xorl	%r9d, %r9d
	jmp	.L135
.L162:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	asn1_get_choice_selector@PLT
	testl	%eax, %eax
	js	.L175
	cltq
	cmpq	24(%r15), %rax
	jge	.L175
.L146:
	leaq	(%rax,%rax,4), %rdx
	movq	16(%r15), %rax
	movq	%r12, %rdi
	leaq	(%rax,%rdx,8), %r13
	movq	%r13, %rsi
	call	asn1_get_field_ptr@PLT
	movq	-80(%rbp), %rsi
	movl	%ebx, %r8d
	movq	%r13, %rdx
	movq	%rax, %rdi
	movl	$-1, %ecx
	call	asn1_template_ex_i2d
	movl	%eax, %r9d
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L211:
	movl	%eax, %r9d
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L170:
	movl	$-1, %r9d
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L217:
	movq	-104(%rbp), %r13
	movq	%rsi, %r15
	movl	%eax, %esi
.L157:
	movl	-68(%rbp), %r14d
	movl	-88(%rbp), %edx
	movl	%r14d, %edi
	call	ASN1_object_size@PLT
	movl	%eax, %r9d
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L135
	cmpl	$-1, %r9d
	je	.L135
	movl	-88(%rbp), %ecx
	movl	-60(%rbp), %edx
	movl	%r14d, %esi
	movq	%rax, %rdi
	movl	%ebx, %r8d
	movl	%r9d, -96(%rbp)
	call	ASN1_put_object@PLT
	xorl	%eax, %eax
	cmpq	$0, 24(%r15)
	movq	16(%r15), %r14
	movl	-96(%rbp), %r9d
	jle	.L161
	movq	%r13, -104(%rbp)
	movq	%rax, %r13
	movq	%r15, -96(%rbp)
	movq	-80(%rbp), %r15
	movl	%r9d, -72(%rbp)
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L221:
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	%rax, -88(%rbp)
	addq	$40, %r14
	call	asn1_get_field_ptr@PLT
	movq	-88(%rbp), %rdx
	movl	%ebx, %r8d
	movq	%r15, %rsi
	movq	%rax, %rdi
	movl	$-1, %ecx
	addq	$1, %r13
	call	asn1_template_ex_i2d
	movq	-96(%rbp), %rax
	cmpq	%r13, 24(%rax)
	jle	.L220
.L158:
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	asn1_do_adb@PLT
	testq	%rax, %rax
	jne	.L221
	jmp	.L175
.L220:
	movq	-104(%rbp), %r13
	movl	-72(%rbp), %r9d
	movq	%rax, %r15
.L161:
	cmpl	$2, -68(%rbp)
	je	.L222
.L160:
	testq	%r13, %r13
	je	.L135
	movl	%r9d, -68(%rbp)
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r12, %rsi
	movl	$7, %edi
	call	*%r13
	movl	-68(%rbp), %r9d
	testl	%eax, %eax
	jne	.L135
	jmp	.L175
.L216:
	movl	-60(%rbp), %esi
	jmp	.L157
.L222:
	movq	-80(%rbp), %rdi
	movl	%r9d, -68(%rbp)
	call	ASN1_put_eoc@PLT
	movl	-68(%rbp), %r9d
	jmp	.L160
.L219:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE518:
	.size	ASN1_item_ex_i2d, .-ASN1_item_ex_i2d
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/asn1/tasn_enc.c"
	.text
	.p2align 4
	.type	asn1_template_ex_i2d, @function
asn1_template_ex_i2d:
.LFB519:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -144(%rbp)
	movl	%ecx, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	%rax, -104(%rbp)
	testb	$16, %ah
	je	.L224
	movq	%rdi, -88(%rbp)
	leaq	-88(%rbp), %r12
.L224:
	movq	-104(%rbp), %rcx
	movl	%ecx, %eax
	andl	$24, %eax
	movl	%eax, -128(%rbp)
	je	.L225
	cmpl	$-1, -120(%rbp)
	jne	.L258
	movl	8(%r13), %eax
	movl	%eax, -120(%rbp)
	movl	%ecx, %eax
	andl	$192, %eax
	movl	%eax, -128(%rbp)
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L225:
	movl	%r8d, %eax
	andl	$192, %eax
	cmpl	$-1, -120(%rbp)
	cmove	-128(%rbp), %eax
	movl	%eax, -128(%rbp)
.L227:
	movl	$1, -132(%rbp)
	movl	%r8d, %r14d
	andb	$63, %r14b
	testq	$2048, -104(%rbp)
	je	.L228
	xorl	%eax, %eax
	andl	$2048, %r8d
	setne	%al
	addl	$1, %eax
	movl	%eax, -132(%rbp)
.L228:
	movq	-104(%rbp), %rax
	testb	$6, %al
	je	.L229
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.L261
	movl	%eax, %ecx
	andl	$2, %ecx
	movl	%ecx, -136(%rbp)
	je	.L231
	movl	%eax, %ebx
	movl	-120(%rbp), %ecx
	andl	$4, %ebx
	movl	%ebx, -136(%rbp)
	je	.L300
	movl	$2, -152(%rbp)
	cmpl	$-1, %ecx
	je	.L269
	movl	-104(%rbp), %eax
	movl	$0, %edx
	andl	$16, %eax
	movl	$17, %eax
	cmove	-128(%rbp), %edx
	cmove	%ecx, %eax
	movl	%edx, -136(%rbp)
	movl	%eax, -148(%rbp)
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L229:
	movq	32(%r13), %rdx
	testb	$16, -104(%rbp)
	je	.L253
	movl	%r14d, %r8d
	movl	$-1, %ecx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	ASN1_item_ex_i2d
	movl	%eax, %r15d
	testl	%eax, %eax
	jne	.L301
	.p2align 4,,10
	.p2align 3
.L223:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L302
	addq	$120, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L258:
	.cfi_restore_state
	movl	$-1, %r15d
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L253:
	movl	-128(%rbp), %r8d
	movl	-120(%rbp), %ecx
	movq	%r12, %rdi
	movq	-144(%rbp), %rsi
	orl	%r14d, %r8d
	call	ASN1_item_ex_i2d
.L299:
	movl	%eax, %r15d
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L231:
	cmpl	$-1, -120(%rbp)
	je	.L297
	movl	-104(%rbp), %eax
	andl	$16, %eax
	movl	%eax, -152(%rbp)
	jne	.L297
	movl	-128(%rbp), %eax
	movl	%eax, -136(%rbp)
	movl	-120(%rbp), %eax
	movl	%eax, -148(%rbp)
	.p2align 4,,10
	.p2align 3
.L233:
	leaq	-80(%rbp), %rax
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	movq	%rax, -112(%rbp)
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L237:
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	movq	32(%r13), %rdx
	movq	-112(%rbp), %rdi
	xorl	%esi, %esi
	movl	%r14d, %r8d
	movl	$-1, %ecx
	movq	%rax, -80(%rbp)
	call	ASN1_item_ex_i2d
	cmpl	$-1, %eax
	je	.L258
	movl	$2147483647, %edx
	subl	%eax, %edx
	cmpl	%ebx, %edx
	jl	.L258
	addl	%eax, %ebx
	addl	$1, %r15d
.L234:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r15d
	jl	.L237
	movl	-148(%rbp), %edx
	movl	-132(%rbp), %edi
	movl	%ebx, %esi
	call	ASN1_object_size@PLT
	movl	%eax, %r15d
	cmpl	$-1, %eax
	je	.L258
	movl	-104(%rbp), %eax
	andl	$16, %eax
	movl	%eax, -156(%rbp)
	jne	.L303
	cmpq	$0, -144(%rbp)
	je	.L223
.L255:
	movl	-136(%rbp), %r8d
	movl	-148(%rbp), %ecx
	movl	%ebx, %edx
	movl	-132(%rbp), %esi
	movq	-144(%rbp), %rdi
	call	ASN1_put_object@PLT
	movq	32(%r13), %rax
	movl	-152(%rbp), %edx
	movq	$0, -64(%rbp)
	movq	%rax, -112(%rbp)
	testl	%edx, %edx
	jne	.L239
.L241:
	xorl	%r13d, %r13d
	leaq	-72(%rbp), %rbx
	movl	%r15d, -104(%rbp)
	movl	%r13d, %r15d
	movq	%rbx, %r13
	movq	-112(%rbp), %rbx
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L245:
	movl	%r15d, %esi
	movq	%r12, %rdi
	addl	$1, %r15d
	call	OPENSSL_sk_value@PLT
	movl	%r14d, %r8d
	movl	$-1, %ecx
	movq	%rbx, %rdx
	movq	-144(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, -72(%rbp)
	call	ASN1_item_ex_i2d
.L240:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r15d
	jl	.L245
	movl	-104(%rbp), %r15d
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L300:
	movl	$1, -152(%rbp)
	cmpl	$-1, %ecx
	je	.L296
	andl	$16, %eax
	movl	$17, %edx
	cmove	-128(%rbp), %ebx
	cmove	%ecx, %edx
	movl	%ebx, -136(%rbp)
	movl	%edx, -148(%rbp)
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L301:
	movl	-120(%rbp), %edx
	movl	-132(%rbp), %edi
	movl	%eax, %esi
	call	ASN1_object_size@PLT
	movl	%eax, %ebx
	movq	-144(%rbp), %rax
	testq	%rax, %rax
	je	.L267
	cmpl	$-1, %ebx
	jne	.L304
.L267:
	movl	%ebx, %r15d
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L297:
	movl	$0, -152(%rbp)
	movl	$16, -148(%rbp)
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L261:
	xorl	%r15d, %r15d
	jmp	.L223
.L269:
	movl	$0, -136(%rbp)
.L296:
	movl	$17, -148(%rbp)
	jmp	.L233
.L304:
	movl	-128(%rbp), %r8d
	movl	-120(%rbp), %ecx
	movl	%r15d, %edx
	movq	%rax, %rdi
	movl	-132(%rbp), %esi
	movq	%rax, %r15
	call	ASN1_put_object@PLT
	movq	32(%r13), %rdx
	movq	%r15, %rsi
	movl	%r14d, %r8d
	movl	$-1, %ecx
	movq	%r12, %rdi
	movl	%ebx, %r15d
	call	ASN1_item_ex_i2d
	cmpl	$2, -132(%rbp)
	jne	.L223
.L298:
	movq	-144(%rbp), %rdi
	call	ASN1_put_eoc@PLT
	jmp	.L223
.L239:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	$1, %eax
	jle	.L241
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	movl	$383, %edx
	leaq	.LC0(%rip), %rsi
	cltq
	leaq	(%rax,%rax,2), %rdi
	salq	$3, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L243
	movslq	%ebx, %rdi
	movl	$387, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, -128(%rbp)
	testq	%rax, %rax
	je	.L305
	movq	%rax, -64(%rbp)
	leaq	-64(%rbp), %rax
	movq	-120(%rbp), %rbx
	xorl	%r13d, %r13d
	movl	%r15d, -136(%rbp)
	movq	-112(%rbp), %r15
	movq	%rax, -104(%rbp)
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L247:
	movl	%r13d, %esi
	movq	%r12, %rdi
	addl	$1, %r13d
	addq	$24, %rbx
	call	OPENSSL_sk_value@PLT
	movq	-104(%rbp), %rsi
	leaq	-72(%rbp), %rdi
	movl	%r14d, %r8d
	movq	%rax, -72(%rbp)
	movq	-64(%rbp), %rax
	movl	$-1, %ecx
	movq	%r15, %rdx
	movq	%rax, -24(%rbx)
	call	ASN1_item_ex_i2d
	movl	%eax, -16(%rbx)
	movq	-72(%rbp), %rax
	movq	%rax, -8(%rbx)
.L254:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jl	.L247
	movq	%r12, %rdi
	movl	-136(%rbp), %r15d
	xorl	%ebx, %ebx
	call	OPENSSL_sk_num@PLT
	movq	-120(%rbp), %r13
	movl	$24, %edx
	leaq	der_cmp(%rip), %rcx
	movslq	%eax, %rsi
	movq	%r13, %rdi
	call	qsort@PLT
	movq	-144(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L249:
	movslq	8(%r13), %rdx
	movq	0(%r13), %rsi
	addl	$1, %ebx
	addq	$24, %r13
	movq	-64(%rbp), %rdi
	call	memcpy@PLT
	movslq	-16(%r13), %rax
	addq	%rax, -64(%rbp)
.L248:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L249
	movq	-64(%rbp), %rax
	movq	-144(%rbp), %rsi
	cmpl	$2, -152(%rbp)
	movq	%rax, (%rsi)
	je	.L266
.L252:
	movq	-120(%rbp), %rdi
	movl	$426, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-128(%rbp), %rdi
	movl	$427, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
.L243:
	cmpl	$2, -132(%rbp)
	jne	.L223
	movq	-144(%rbp), %rdi
	call	ASN1_put_eoc@PLT
	movl	-156(%rbp), %eax
	testl	%eax, %eax
	je	.L223
	jmp	.L298
.L303:
	movl	-120(%rbp), %edx
	movl	-132(%rbp), %edi
	movl	%r15d, %esi
	call	ASN1_object_size@PLT
	movq	-144(%rbp), %r9
	testq	%r9, %r9
	je	.L299
	cmpl	$-1, %eax
	je	.L299
	movl	-128(%rbp), %r8d
	movl	-120(%rbp), %ecx
	movl	%r15d, %edx
	movq	%r9, %rdi
	movl	-132(%rbp), %esi
	movl	%eax, -104(%rbp)
	call	ASN1_put_object@PLT
	movl	-104(%rbp), %eax
	movl	%eax, %r15d
	jmp	.L255
.L266:
	movq	-120(%rbp), %rbx
	xorl	%r13d, %r13d
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L251:
	movq	16(%rbx), %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	addl	$1, %r13d
	addq	$24, %rbx
	call	OPENSSL_sk_set@PLT
.L250:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jl	.L251
	jmp	.L252
.L302:
	call	__stack_chk_fail@PLT
.L305:
	movq	-120(%rbp), %rdi
	movl	$389, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L243
	.cfi_endproc
.LFE519:
	.size	asn1_template_ex_i2d, .-asn1_template_ex_i2d
	.p2align 4
	.globl	ASN1_item_ndef_i2d
	.type	ASN1_item_ndef_i2d, @function
ASN1_item_ndef_i2d:
.LFB515:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -72(%rbp)
	testq	%rsi, %rsi
	je	.L307
	cmpq	$0, (%rsi)
	je	.L392
.L307:
	movzbl	(%r14), %eax
	movq	32(%r14), %r10
	testb	%al, %al
	je	.L311
	testq	%rdi, %rdi
	je	.L348
.L311:
	testq	%r10, %r10
	je	.L313
	movq	24(%r10), %rcx
	movq	%rcx, -88(%rbp)
	testq	%rcx, %rcx
	je	.L313
	cmpb	$6, %al
	ja	.L348
	leaq	.L341(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L341:
	.long	.L315-.L341
	.long	.L349-.L341
	.long	.L318-.L341
	.long	.L348-.L341
	.long	.L323-.L341
	.long	.L317-.L341
	.long	.L340-.L341
	.text
.L340:
	movl	$2, %r15d
.L325:
	leaq	-72(%rbp), %rbx
	leaq	-64(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%rbx, %rdx
	call	asn1_enc_restore@PLT
	testl	%eax, %eax
	js	.L348
	movl	-64(%rbp), %r12d
	jne	.L306
	cmpq	$0, -88(%rbp)
	movl	$0, -64(%rbp)
	je	.L330
	xorl	%ecx, %ecx
	movq	-88(%rbp), %rax
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movl	$6, %edi
	call	*%rax
	testl	%eax, %eax
	je	.L348
.L330:
	movq	16(%r14), %rax
	cmpq	$0, 24(%r14)
	movq	$0, -96(%rbp)
	movq	%rax, -104(%rbp)
	jg	.L328
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L394:
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	asn1_get_field_ptr@PLT
	movq	%r12, %rdx
	movl	$2048, %r8d
	xorl	%esi, %esi
	movq	%rax, %rdi
	movl	$-1, %ecx
	call	asn1_template_ex_i2d
	movl	%eax, %r12d
	cmpl	$-1, %eax
	je	.L306
	movl	-64(%rbp), %esi
	movl	$2147483647, %eax
	subl	%esi, %eax
	cmpl	%eax, %r12d
	jg	.L344
	addl	%r12d, %esi
	addq	$1, -96(%rbp)
	movq	-96(%rbp), %rax
	addq	$40, -104(%rbp)
	movl	%esi, -64(%rbp)
	cmpq	%rax, 24(%r14)
	jle	.L331
.L328:
	movq	-104(%rbp), %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	call	asn1_do_adb@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L394
.L348:
	xorl	%r12d, %r12d
.L306:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L395
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L349:
	.cfi_restore_state
	movl	$1, %r15d
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L313:
	cmpb	$6, %al
	ja	.L348
	leaq	.L339(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L339:
	.long	.L315-.L339
	.long	.L347-.L339
	.long	.L336-.L339
	.long	.L348-.L339
	.long	.L323-.L339
	.long	.L317-.L339
	.long	.L324-.L339
	.text
.L324:
	movq	$0, -88(%rbp)
	movl	$2, %r15d
	jmp	.L325
.L347:
	movq	$0, -88(%rbp)
	movl	$1, %r15d
	jmp	.L325
.L317:
	leaq	-72(%rbp), %rdi
	movl	$2048, %r8d
	movl	$-1, %ecx
.L391:
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	asn1_i2d_ex_primitive
	movl	%eax, %r12d
	jmp	.L306
.L315:
	movq	16(%r14), %rdx
	leaq	-72(%rbp), %rdi
	movl	$2048, %r8d
	movl	$-1, %ecx
	testq	%rdx, %rdx
	je	.L391
.L390:
	movq	%r13, %rsi
	call	asn1_template_ex_i2d
	movl	%eax, %r12d
	jmp	.L306
.L323:
	leaq	-72(%rbp), %rdi
	movl	$2048, %r8d
	movq	%r14, %rdx
	movq	%r13, %rsi
	movl	$-1, %ecx
	call	*40(%r10)
	movl	%eax, %r12d
	jmp	.L306
.L318:
	leaq	-72(%rbp), %rbx
	xorl	%ecx, %ecx
	movq	-88(%rbp), %rax
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movl	$6, %edi
	call	*%rax
	testl	%eax, %eax
	je	.L348
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	asn1_get_choice_selector@PLT
	testl	%eax, %eax
	js	.L322
	cltq
	cmpq	24(%r14), %rax
	jl	.L321
.L322:
	movq	-88(%rbp), %rax
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movl	$7, %edi
	xorl	%r12d, %r12d
	call	*%rax
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L392:
	leaq	-72(%rbp), %r15
	movl	$2048, %r8d
	movl	$-1, %ecx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	ASN1_item_ex_i2d
	movl	%eax, %r12d
	testl	%eax, %eax
	jle	.L306
	movslq	%eax, %rdi
	movl	$64, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L396
	leaq	-64(%rbp), %rsi
	movl	$2048, %r8d
	movq	%r14, %rdx
	movq	%r15, %rdi
	movl	$-1, %ecx
	movq	%rax, -64(%rbp)
	call	ASN1_item_ex_i2d
	movq	%rbx, 0(%r13)
	jmp	.L306
.L336:
	leaq	-72(%rbp), %rbx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	asn1_get_choice_selector@PLT
	testl	%eax, %eax
	js	.L348
	cltq
	cmpq	24(%r14), %rax
	jge	.L348
.L321:
	leaq	(%rax,%rax,4), %rdx
	movq	16(%r14), %rax
	movq	%rbx, %rdi
	leaq	(%rax,%rdx,8), %r12
	movq	%r12, %rsi
	call	asn1_get_field_ptr@PLT
	movl	$2048, %r8d
	movl	$-1, %ecx
	movq	%r12, %rdx
	movq	%rax, %rdi
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L396:
	movl	$65, %r8d
	movl	$65, %edx
	movl	$118, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L306
.L393:
	movl	-64(%rbp), %esi
	.p2align 4,,10
	.p2align 3
.L331:
	movl	$16, %edx
	movl	%r15d, %edi
	call	ASN1_object_size@PLT
	movl	%eax, %r12d
	testq	%r13, %r13
	je	.L306
	cmpl	$-1, %eax
	je	.L306
	movl	-64(%rbp), %edx
	movl	$2048, %r8d
	movl	%r15d, %esi
	movq	%r13, %rdi
	movl	$16, %ecx
	call	ASN1_put_object@PLT
	movq	16(%r14), %rax
	cmpq	$0, 24(%r14)
	movq	$0, -96(%rbp)
	movq	%rax, -104(%rbp)
	jg	.L332
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L397:
	movq	%rax, %rsi
	movq	%rbx, %rdi
	movq	%rax, -112(%rbp)
	call	asn1_get_field_ptr@PLT
	movq	-112(%rbp), %rdx
	movl	$-1, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movl	$2048, %r8d
	call	asn1_template_ex_i2d
	addq	$1, -96(%rbp)
	movq	-96(%rbp), %rax
	addq	$40, -104(%rbp)
	cmpq	%rax, 24(%r14)
	jle	.L335
.L332:
	movq	-104(%rbp), %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	call	asn1_do_adb@PLT
	testq	%rax, %rax
	jne	.L397
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L344:
	movl	$-1, %r12d
	jmp	.L306
.L335:
	cmpl	$2, %r15d
	je	.L398
.L334:
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	je	.L306
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movl	$7, %edi
	call	*%rax
	testl	%eax, %eax
	jne	.L306
	jmp	.L348
.L398:
	movq	%r13, %rdi
	call	ASN1_put_eoc@PLT
	jmp	.L334
.L395:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE515:
	.size	ASN1_item_ndef_i2d, .-ASN1_item_ndef_i2d
	.p2align 4
	.globl	ASN1_item_i2d
	.type	ASN1_item_i2d, @function
ASN1_item_i2d:
.LFB516:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -72(%rbp)
	testq	%rsi, %rsi
	je	.L400
	cmpq	$0, (%rsi)
	je	.L478
.L400:
	movzbl	(%r15), %eax
	movq	32(%r15), %r10
	testb	%al, %al
	je	.L404
	testq	%rdi, %rdi
	je	.L438
.L404:
	testq	%r10, %r10
	je	.L406
	movq	24(%r10), %rbx
	testq	%rbx, %rbx
	je	.L406
	cmpb	$6, %al
	ja	.L438
	leaq	.L431(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L431:
	.long	.L408-.L431
	.long	.L417-.L431
	.long	.L411-.L431
	.long	.L438-.L431
	.long	.L416-.L431
	.long	.L410-.L431
	.long	.L417-.L431
	.text
.L437:
	xorl	%ebx, %ebx
.L417:
	leaq	-72(%rbp), %r14
	leaq	-64(%rbp), %rdi
	movq	%r15, %rcx
	movq	%r12, %rsi
	movq	%r14, %rdx
	call	asn1_enc_restore@PLT
	testl	%eax, %eax
	js	.L438
	movl	-64(%rbp), %r10d
	jne	.L399
	movl	$0, -64(%rbp)
	testq	%rbx, %rbx
	je	.L422
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movl	$6, %edi
	call	*%rbx
	testl	%eax, %eax
	jne	.L422
.L438:
	xorl	%r10d, %r10d
.L399:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L479
	addq	$72, %rsp
	movl	%r10d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L406:
	.cfi_restore_state
	cmpb	$6, %al
	ja	.L438
	leaq	.L430(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L430:
	.long	.L408-.L430
	.long	.L437-.L430
	.long	.L428-.L430
	.long	.L438-.L430
	.long	.L416-.L430
	.long	.L410-.L430
	.long	.L437-.L430
	.text
.L408:
	movq	16(%r15), %rdx
	leaq	-72(%rbp), %rdi
	xorl	%r8d, %r8d
	movl	$-1, %ecx
	testq	%rdx, %rdx
	je	.L477
.L476:
	movq	%r12, %rsi
	call	asn1_template_ex_i2d
	movl	%eax, %r10d
	jmp	.L399
.L416:
	leaq	-72(%rbp), %rdi
	xorl	%r8d, %r8d
	movl	$-1, %ecx
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	*40(%r10)
	movl	%eax, %r10d
	jmp	.L399
.L410:
	leaq	-72(%rbp), %rdi
	xorl	%r8d, %r8d
	movl	$-1, %ecx
.L477:
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	asn1_i2d_ex_primitive
	movl	%eax, %r10d
	jmp	.L399
.L411:
	leaq	-72(%rbp), %r14
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movl	$6, %edi
	movq	%r14, %rsi
	call	*%rbx
	testl	%eax, %eax
	je	.L438
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	asn1_get_choice_selector@PLT
	testl	%eax, %eax
	js	.L415
	cltq
	cmpq	24(%r15), %rax
	jl	.L414
.L415:
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movl	$7, %edi
	call	*%rbx
	xorl	%r10d, %r10d
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L422:
	cmpq	$0, 24(%r15)
	movq	16(%r15), %r13
	movq	$0, -88(%rbp)
	jg	.L420
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L481:
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	%rax, -96(%rbp)
	call	asn1_get_field_ptr@PLT
	movq	-96(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%rax, %rdi
	movl	$-1, %ecx
	call	asn1_template_ex_i2d
	movl	%eax, %r10d
	cmpl	$-1, %eax
	je	.L399
	movl	-64(%rbp), %esi
	movl	$2147483647, %eax
	subl	%esi, %eax
	cmpl	%eax, %r10d
	jg	.L434
	addl	%r10d, %esi
	addq	$1, -88(%rbp)
	addq	$40, %r13
	movq	-88(%rbp), %rax
	movl	%esi, -64(%rbp)
	cmpq	%rax, 24(%r15)
	jle	.L423
.L420:
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	asn1_do_adb@PLT
	testq	%rax, %rax
	jne	.L481
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L478:
	leaq	-72(%rbp), %r13
	xorl	%r8d, %r8d
	movl	$-1, %ecx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	ASN1_item_ex_i2d
	movl	%eax, %r10d
	testl	%eax, %eax
	jle	.L399
	movslq	%eax, %rdi
	movl	$64, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, -88(%rbp)
	call	CRYPTO_malloc@PLT
	movl	-88(%rbp), %r10d
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L482
	leaq	-64(%rbp), %rsi
	xorl	%r8d, %r8d
	movl	$-1, %ecx
	movq	%r15, %rdx
	movq	%r13, %rdi
	movl	%r10d, -88(%rbp)
	movq	%rax, -64(%rbp)
	call	ASN1_item_ex_i2d
	movq	%rbx, (%r12)
	movl	-88(%rbp), %r10d
	jmp	.L399
.L428:
	leaq	-72(%rbp), %r14
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	asn1_get_choice_selector@PLT
	testl	%eax, %eax
	js	.L438
	cltq
	cmpq	24(%r15), %rax
	jge	.L438
.L414:
	leaq	(%rax,%rax,4), %rdx
	movq	16(%r15), %rax
	movq	%r14, %rdi
	leaq	(%rax,%rdx,8), %r13
	movq	%r13, %rsi
	call	asn1_get_field_ptr@PLT
	xorl	%r8d, %r8d
	movl	$-1, %ecx
	movq	%r13, %rdx
	movq	%rax, %rdi
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L482:
	movl	$65, %r8d
	movl	$65, %edx
	movl	$118, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %r10d
	jmp	.L399
.L480:
	movl	-64(%rbp), %esi
	.p2align 4,,10
	.p2align 3
.L423:
	movl	$16, %edx
	movl	$1, %edi
	call	ASN1_object_size@PLT
	movl	%eax, %r10d
	testq	%r12, %r12
	je	.L399
	cmpl	$-1, %eax
	je	.L399
	movl	-64(%rbp), %edx
	xorl	%r8d, %r8d
	movl	$16, %ecx
	movl	$1, %esi
	movq	%r12, %rdi
	movl	%eax, -96(%rbp)
	call	ASN1_put_object@PLT
	cmpq	$0, 24(%r15)
	movq	16(%r15), %r13
	movq	$0, -88(%rbp)
	movl	-96(%rbp), %r10d
	jg	.L424
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L483:
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	%rax, -104(%rbp)
	addq	$40, %r13
	call	asn1_get_field_ptr@PLT
	movq	-104(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%rax, %rdi
	movl	$-1, %ecx
	call	asn1_template_ex_i2d
	addq	$1, -88(%rbp)
	movq	-88(%rbp), %rax
	cmpq	%rax, 24(%r15)
	movl	-96(%rbp), %r10d
	jle	.L426
.L424:
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movl	%r10d, -96(%rbp)
	call	asn1_do_adb@PLT
	testq	%rax, %rax
	jne	.L483
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L434:
	movl	$-1, %r10d
	jmp	.L399
.L426:
	testq	%rbx, %rbx
	je	.L399
	movl	%r10d, -88(%rbp)
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movl	$7, %edi
	call	*%rbx
	movl	-88(%rbp), %r10d
	testl	%eax, %eax
	jne	.L399
	jmp	.L438
.L479:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE516:
	.size	ASN1_item_i2d, .-ASN1_item_i2d
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
