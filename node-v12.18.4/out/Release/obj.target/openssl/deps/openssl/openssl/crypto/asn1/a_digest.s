	.file	"a_digest.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/asn1/a_digest.c"
	.text
	.p2align 4
	.globl	ASN1_digest
	.type	ASN1_digest, @function
ASN1_digest:
.LFB779:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	xorl	%esi, %esi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	subq	$40, %rsp
	movq	%rcx, -72(%rbp)
	movq	%r8, -80(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	*%rbx
	testl	%eax, %eax
	jle	.L9
	movslq	%eax, %r15
	movl	$34, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L10
	leaq	-64(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	call	*%rbx
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %rdx
	xorl	%r9d, %r9d
	movq	%r14, %r8
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	EVP_Digest@PLT
	testl	%eax, %eax
	je	.L11
	movl	$45, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	movl	$1, %eax
.L1:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L12
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movl	$42, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, -72(%rbp)
	call	CRYPTO_free@PLT
	movl	-72(%rbp), %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$31, %r8d
	movl	$68, %edx
	movl	$184, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L10:
	movl	$35, %r8d
	movl	$65, %edx
	movl	$184, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L1
.L12:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE779:
	.size	ASN1_digest, .-ASN1_digest
	.p2align 4
	.globl	ASN1_item_digest
	.type	ASN1_item_digest, @function
ASN1_item_digest:
.LFB780:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r9
	movq	%rdx, %rdi
	movq	%r9, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	leaq	-48(%rbp), %rsi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	call	ASN1_item_i2d@PLT
	movq	-48(%rbp), %rdi
	movslq	%eax, %rsi
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L13
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	%r12, %rdx
	call	EVP_Digest@PLT
	testl	%eax, %eax
	je	.L20
	movq	-48(%rbp), %rdi
	movl	$65, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$1, %eax
.L13:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L21
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movq	-48(%rbp), %rdi
	movl	$62, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, -52(%rbp)
	call	CRYPTO_free@PLT
	movl	-52(%rbp), %eax
	jmp	.L13
.L21:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE780:
	.size	ASN1_item_digest, .-ASN1_item_digest
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
