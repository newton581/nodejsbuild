	.file	"pem_xaux.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"TRUSTED CERTIFICATE"
	.text
	.p2align 4
	.globl	PEM_read_bio_X509_AUX
	.type	PEM_read_bio_X509_AUX, @function
PEM_read_bio_X509_AUX:
.LFB779:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	movq	%rdi, %rdx
	movq	d2i_X509_AUX@GOTPCREL(%rip), %rdi
	movq	%rcx, %r9
	movq	%rsi, %rcx
	leaq	.LC0(%rip), %rsi
	jmp	PEM_ASN1_read_bio@PLT
	.cfi_endproc
.LFE779:
	.size	PEM_read_bio_X509_AUX, .-PEM_read_bio_X509_AUX
	.p2align 4
	.globl	PEM_read_X509_AUX
	.type	PEM_read_X509_AUX, @function
PEM_read_X509_AUX:
.LFB780:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	movq	%rdi, %rdx
	movq	d2i_X509_AUX@GOTPCREL(%rip), %rdi
	movq	%rcx, %r9
	movq	%rsi, %rcx
	leaq	.LC0(%rip), %rsi
	jmp	PEM_ASN1_read@PLT
	.cfi_endproc
.LFE780:
	.size	PEM_read_X509_AUX, .-PEM_read_X509_AUX
	.p2align 4
	.globl	PEM_write_bio_X509_AUX
	.type	PEM_write_bio_X509_AUX, @function
PEM_write_bio_X509_AUX:
.LFB781:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	i2d_X509_AUX@GOTPCREL(%rip), %rdi
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write_bio@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE781:
	.size	PEM_write_bio_X509_AUX, .-PEM_write_bio_X509_AUX
	.p2align 4
	.globl	PEM_write_X509_AUX
	.type	PEM_write_X509_AUX, @function
PEM_write_X509_AUX:
.LFB782:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	i2d_X509_AUX@GOTPCREL(%rip), %rdi
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE782:
	.size	PEM_write_X509_AUX, .-PEM_write_X509_AUX
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
