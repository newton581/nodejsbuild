	.file	"cms_lib.c"
	.text
	.p2align 4
	.globl	d2i_CMS_ContentInfo
	.type	d2i_CMS_ContentInfo, @function
d2i_CMS_ContentInfo:
.LFB1392:
	.cfi_startproc
	endbr64
	leaq	CMS_ContentInfo_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1392:
	.size	d2i_CMS_ContentInfo, .-d2i_CMS_ContentInfo
	.p2align 4
	.globl	i2d_CMS_ContentInfo
	.type	i2d_CMS_ContentInfo, @function
i2d_CMS_ContentInfo:
.LFB1393:
	.cfi_startproc
	endbr64
	leaq	CMS_ContentInfo_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1393:
	.size	i2d_CMS_ContentInfo, .-i2d_CMS_ContentInfo
	.p2align 4
	.globl	CMS_ContentInfo_new
	.type	CMS_ContentInfo_new, @function
CMS_ContentInfo_new:
.LFB1394:
	.cfi_startproc
	endbr64
	leaq	CMS_ContentInfo_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1394:
	.size	CMS_ContentInfo_new, .-CMS_ContentInfo_new
	.p2align 4
	.globl	CMS_ContentInfo_free
	.type	CMS_ContentInfo_free, @function
CMS_ContentInfo_free:
.LFB1395:
	.cfi_startproc
	endbr64
	leaq	CMS_ContentInfo_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1395:
	.size	CMS_ContentInfo_free, .-CMS_ContentInfo_free
	.p2align 4
	.globl	CMS_ContentInfo_print_ctx
	.type	CMS_ContentInfo_print_ctx, @function
CMS_ContentInfo_print_ctx:
.LFB1396:
	.cfi_startproc
	endbr64
	movq	%rcx, %r8
	leaq	CMS_ContentInfo_it(%rip), %rcx
	jmp	ASN1_item_print@PLT
	.cfi_endproc
.LFE1396:
	.size	CMS_ContentInfo_print_ctx, .-CMS_ContentInfo_print_ctx
	.p2align 4
	.globl	CMS_get0_type
	.type	CMS_get0_type, @function
CMS_get0_type:
.LFB1397:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE1397:
	.size	CMS_get0_type, .-CMS_get0_type
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/cms/cms_lib.c"
	.text
	.p2align 4
	.globl	cms_Data_create
	.type	cms_Data_create, @function
cms_Data_create:
.LFB1398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	CMS_ContentInfo_it(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	call	ASN1_item_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L8
	movl	$21, %edi
	call	OBJ_nid2obj@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$26, %eax
	jg	.L10
	cmpl	$20, %eax
	jle	.L11
	subl	$21, %eax
	cmpl	$5, %eax
	ja	.L11
	leaq	.L13(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L13:
	.long	.L17-.L13
	.long	.L14-.L13
	.long	.L15-.L13
	.long	.L11-.L13
	.long	.L14-.L13
	.long	.L12-.L13
	.text
	.p2align 4,,10
	.p2align 3
.L10:
	cmpl	$205, %eax
	je	.L18
	cmpl	$786, %eax
	jne	.L11
	movq	8(%r12), %rax
	movq	24(%rax), %rbx
	addq	$8, %rbx
	.p2align 4,,10
	.p2align 3
.L20:
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L29
.L24:
	orq	$32, 16(%rax)
.L8:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L14:
	.cfi_restore_state
	movq	8(%r12), %rax
	movq	16(%rax), %rbx
	addq	$8, %rbx
	jmp	.L20
.L11:
	movq	8(%r12), %rbx
	cmpl	$4, (%rbx)
	jne	.L21
	addq	$8, %rbx
	jmp	.L20
.L17:
	leaq	8(%r12), %rbx
	jmp	.L20
.L12:
	movq	8(%r12), %rax
	movq	8(%rax), %rbx
	addq	$16, %rbx
	jmp	.L20
.L15:
	movq	8(%r12), %rax
	movq	24(%rax), %rbx
	addq	$16, %rbx
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L18:
	movq	8(%r12), %rax
	movq	40(%rax), %rbx
	addq	$8, %rbx
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L29:
	call	ASN1_OCTET_STRING_new@PLT
	movq	%rax, (%rbx)
	testq	%rax, %rax
	jne	.L24
	movl	$280, %r8d
	movl	$65, %edx
	movl	$147, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	movl	$184, %r8d
	movl	$152, %edx
	movl	$129, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L8
	.cfi_endproc
.LFE1398:
	.size	cms_Data_create, .-cms_Data_create
	.p2align 4
	.globl	cms_content_bio
	.type	cms_content_bio, @function
cms_content_bio:
.LFB1399:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$26, %eax
	jg	.L31
	cmpl	$20, %eax
	jle	.L32
	subl	$21, %eax
	cmpl	$5, %eax
	ja	.L32
	leaq	.L34(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L34:
	.long	.L50-.L34
	.long	.L35-.L34
	.long	.L36-.L34
	.long	.L32-.L34
	.long	.L35-.L34
	.long	.L33-.L34
	.text
	.p2align 4,,10
	.p2align 3
.L31:
	cmpl	$205, %eax
	je	.L39
	cmpl	$786, %eax
	jne	.L32
	movq	8(%rbx), %rax
	movq	24(%rax), %rbx
.L50:
	addq	$8, %rbx
.L41:
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L52
	cmpq	$32, 16(%rax)
	je	.L53
	movq	8(%rax), %rdi
	movl	(%rax), %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_new_mem_buf@PLT
.L35:
	.cfi_restore_state
	movq	8(%rbx), %rax
	movq	16(%rax), %rbx
	addq	$8, %rbx
	jmp	.L41
.L32:
	movq	8(%rbx), %rbx
	cmpl	$4, (%rbx)
	je	.L50
	movl	$184, %r8d
	movl	$152, %edx
	movl	$129, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L33:
	.cfi_restore_state
	movq	8(%rbx), %rax
	movq	8(%rax), %rbx
	addq	$16, %rbx
	jmp	.L41
.L36:
	movq	8(%rbx), %rax
	movq	24(%rax), %rbx
	addq	$16, %rbx
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L53:
	call	BIO_s_mem@PLT
.L51:
	addq	$8, %rsp
	movq	%rax, %rdi
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_new@PLT
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	movq	8(%rbx), %rax
	movq	40(%rax), %rbx
	addq	$8, %rbx
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L52:
	call	BIO_s_null@PLT
	jmp	.L51
	.cfi_endproc
.LFE1399:
	.size	cms_content_bio, .-cms_content_bio
	.p2align 4
	.globl	CMS_dataFinal
	.type	CMS_dataFinal, @function
CMS_dataFinal:
.LFB1401:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	OBJ_obj2nid@PLT
	cmpl	$26, %eax
	jg	.L55
	cmpl	$20, %eax
	jle	.L56
	subl	$21, %eax
	cmpl	$5, %eax
	ja	.L56
	leaq	.L58(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L58:
	.long	.L62-.L58
	.long	.L59-.L58
	.long	.L60-.L58
	.long	.L56-.L58
	.long	.L59-.L58
	.long	.L57-.L58
	.text
	.p2align 4,,10
	.p2align 3
.L55:
	cmpl	$205, %eax
	je	.L63
	cmpl	$786, %eax
	jne	.L56
	movq	8(%r13), %rax
	movq	24(%rax), %rbx
	addq	$8, %rbx
	.p2align 4,,10
	.p2align 3
.L65:
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L68
	testb	$32, 16(%rax)
	jne	.L84
.L68:
	movq	0(%r13), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$26, %eax
	jg	.L70
	cmpl	$20, %eax
	jle	.L71
	subl	$21, %eax
	cmpl	$5, %eax
	ja	.L71
	leaq	.L72(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L72:
	.long	.L77-.L72
	.long	.L74-.L72
	.long	.L77-.L72
	.long	.L71-.L72
	.long	.L73-.L72
	.long	.L77-.L72
	.text
.L77:
	movl	$1, %r8d
.L54:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L85
	addq	$24, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	movl	$1, %r8d
	cmpl	$786, %eax
	je	.L54
.L71:
	movl	$146, %r8d
	movl	$156, %edx
	movl	$110, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r8d, %r8d
	jmp	.L54
.L59:
	movq	8(%r13), %rax
	movq	16(%rax), %rbx
	addq	$8, %rbx
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L84:
	movl	$1025, %esi
	movq	%r12, %rdi
	call	BIO_find_type@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L86
	leaq	-64(%rbp), %rcx
	xorl	%edx, %edx
	movl	$3, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	movl	$512, %esi
	movq	%r14, %rdi
	movq	%rax, %r15
	call	BIO_set_flags@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$130, %esi
	movq	%r14, %rdi
	call	BIO_ctrl@PLT
	movq	-64(%rbp), %rsi
	movq	(%rbx), %rdi
	movl	%r15d, %edx
	call	ASN1_STRING_set0@PLT
	movq	(%rbx), %rax
	andq	$-33, 16(%rax)
	jmp	.L68
.L73:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	cms_DigestedData_do_final@PLT
	movl	%eax, %r8d
	jmp	.L54
.L74:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	cms_SignedData_final@PLT
	movl	%eax, %r8d
	jmp	.L54
.L60:
	movq	8(%r13), %rax
	movq	24(%rax), %rbx
	addq	$16, %rbx
	jmp	.L65
.L57:
	movq	8(%r13), %rax
	movq	8(%rax), %rbx
	addq	$16, %rbx
	jmp	.L65
.L56:
	movq	8(%r13), %rbx
	cmpl	$4, (%rbx)
	jne	.L66
	addq	$8, %rbx
	jmp	.L65
.L62:
	leaq	8(%r13), %rbx
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L63:
	movq	8(%r13), %rax
	movq	40(%rax), %rbx
	addq	$8, %rbx
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L66:
	movl	$184, %r8d
	movl	$152, %edx
	movl	$129, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r8d, %r8d
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L86:
	movl	$119, %r8d
	movl	$105, %edx
	movl	$110, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r8d, %r8d
	jmp	.L54
.L85:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1401:
	.size	CMS_dataFinal, .-CMS_dataFinal
	.p2align 4
	.globl	CMS_get0_content
	.type	CMS_get0_content, @function
CMS_get0_content:
.LFB1402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$26, %eax
	jg	.L88
	cmpl	$20, %eax
	jle	.L89
	subl	$21, %eax
	cmpl	$5, %eax
	ja	.L89
	leaq	.L91(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L91:
	.long	.L95-.L91
	.long	.L92-.L91
	.long	.L93-.L91
	.long	.L89-.L91
	.long	.L92-.L91
	.long	.L90-.L91
	.text
	.p2align 4,,10
	.p2align 3
.L88:
	cmpl	$205, %eax
	je	.L96
	cmpl	$786, %eax
	jne	.L89
	movq	8(%rbx), %rax
	movq	24(%rax), %rax
	addq	$8, %rax
.L87:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L92:
	.cfi_restore_state
	movq	8(%rbx), %rax
	movq	16(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	addq	$8, %rax
	ret
.L93:
	.cfi_restore_state
	movq	8(%rbx), %rax
	movq	24(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	addq	$16, %rax
	ret
.L89:
	.cfi_restore_state
	movq	8(%rbx), %rax
	cmpl	$4, (%rax)
	jne	.L99
	addq	$8, %rax
	jmp	.L87
.L95:
	addq	$8, %rsp
	leaq	8(%rbx), %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L90:
	.cfi_restore_state
	movq	8(%rbx), %rax
	movq	8(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	addq	$16, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	movq	8(%rbx), %rax
	movq	40(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	addq	$8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore_state
	movl	$184, %r8d
	movl	$152, %edx
	movl	$129, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L87
	.cfi_endproc
.LFE1402:
	.size	CMS_get0_content, .-CMS_get0_content
	.p2align 4
	.globl	CMS_dataInit
	.type	CMS_dataInit, @function
CMS_dataInit:
.LFB1400:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L124
.L102:
	movq	0(%r13), %rdi
	call	OBJ_obj2nid@PLT
	subl	$21, %eax
	cmpl	$5, %eax
	ja	.L109
	leaq	.L111(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L111:
	.long	.L118-.L111
	.long	.L114-.L111
	.long	.L113-.L111
	.long	.L109-.L111
	.long	.L112-.L111
	.long	.L110-.L111
	.text
	.p2align 4,,10
	.p2align 3
.L118:
	movq	%r12, %r13
.L101:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore_state
	movq	%r13, %rdi
	call	cms_SignedData_init_bio@PLT
	movq	%rax, %r13
.L115:
	testq	%r13, %r13
	jne	.L125
.L116:
	testq	%rbx, %rbx
	jne	.L101
	movq	%r12, %rdi
	call	BIO_free@PLT
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L113:
	movq	%r13, %rdi
	call	cms_EnvelopedData_init_bio@PLT
	movq	%rax, %r13
	testq	%r13, %r13
	je	.L116
.L125:
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_push@PLT
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	movq	%r13, %rdi
	call	cms_DigestedData_init_bio@PLT
	movq	%rax, %r13
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L110:
	movq	%r13, %rdi
	call	cms_EncryptedData_init_bio@PLT
	movq	%rax, %r13
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L109:
	movl	$94, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$156, %edx
	xorl	%r13d, %r13d
	movl	$111, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	call	CMS_get0_content
	testq	%rax, %rax
	je	.L108
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L126
	cmpq	$32, 16(%rax)
	je	.L127
	movq	8(%rax), %rdi
	movl	(%rax), %esi
	call	BIO_new_mem_buf@PLT
	movq	%rax, %r12
.L106:
	testq	%r12, %r12
	jne	.L102
.L108:
	movl	$64, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$127, %edx
	xorl	%r13d, %r13d
	movl	$111, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L127:
	call	BIO_s_mem@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, %r12
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L126:
	call	BIO_s_null@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, %r12
	jmp	.L106
	.cfi_endproc
.LFE1400:
	.size	CMS_dataInit, .-CMS_dataInit
	.p2align 4
	.globl	CMS_get0_eContentType
	.type	CMS_get0_eContentType, @function
CMS_get0_eContentType:
.LFB1404:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$26, %eax
	je	.L129
	jg	.L130
	cmpl	$23, %eax
	je	.L136
	cmpl	$25, %eax
	jne	.L143
.L132:
	movq	8(%rbx), %rax
	movq	16(%rax), %rax
.L137:
	testq	%rax, %rax
	je	.L128
	movq	(%rax), %rax
.L128:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	cmpl	$22, %eax
	je	.L132
.L134:
	movl	$218, %r8d
	movl	$152, %edx
	movl	$130, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	cmpl	$205, %eax
	je	.L135
	cmpl	$786, %eax
	jne	.L134
.L136:
	movq	8(%rbx), %rax
	movq	24(%rax), %rax
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L135:
	movq	8(%rbx), %rax
	movq	40(%rax), %rax
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L129:
	movq	8(%rbx), %rax
	movq	8(%rax), %rax
	jmp	.L137
	.cfi_endproc
.LFE1404:
	.size	CMS_get0_eContentType, .-CMS_get0_eContentType
	.p2align 4
	.globl	CMS_set1_eContentType
	.type	CMS_set1_eContentType, @function
CMS_set1_eContentType:
.LFB1405:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$26, %eax
	je	.L145
	jg	.L146
	cmpl	$23, %eax
	je	.L152
	cmpl	$25, %eax
	jne	.L163
.L148:
	movq	8(%rbx), %rax
	movq	16(%rax), %r13
.L153:
	testq	%r13, %r13
	je	.L156
	movl	$1, %r14d
	testq	%r12, %r12
	je	.L144
	movq	%r12, %rdi
	call	OBJ_dup@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L156
	movq	0(%r13), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	%rbx, 0(%r13)
.L144:
	popq	%rbx
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L163:
	.cfi_restore_state
	cmpl	$22, %eax
	je	.L148
.L150:
	xorl	%r14d, %r14d
	movl	$218, %r8d
	movl	$152, %edx
	movl	$130, %esi
	leaq	.LC0(%rip), %rcx
	movl	$46, %edi
	call	ERR_put_error@PLT
	popq	%rbx
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_restore_state
	cmpl	$205, %eax
	je	.L151
	cmpl	$786, %eax
	jne	.L150
.L152:
	movq	8(%rbx), %rax
	movq	24(%rax), %r13
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L156:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movl	%r14d, %eax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	movq	8(%rbx), %rax
	movq	8(%rax), %r13
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L151:
	movq	8(%rbx), %rax
	movq	40(%rax), %r13
	jmp	.L153
	.cfi_endproc
.LFE1405:
	.size	CMS_set1_eContentType, .-CMS_set1_eContentType
	.p2align 4
	.globl	CMS_is_detached
	.type	CMS_is_detached, @function
CMS_is_detached:
.LFB1406:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$26, %eax
	jg	.L165
	cmpl	$20, %eax
	jle	.L166
	subl	$21, %eax
	cmpl	$5, %eax
	ja	.L166
	leaq	.L168(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L168:
	.long	.L179-.L168
	.long	.L169-.L168
	.long	.L170-.L168
	.long	.L166-.L168
	.long	.L169-.L168
	.long	.L167-.L168
	.text
	.p2align 4,,10
	.p2align 3
.L165:
	cmpl	$205, %eax
	je	.L173
	cmpl	$786, %eax
	jne	.L166
	movq	8(%rbx), %rax
	movq	24(%rax), %rbx
.L179:
	addq	$8, %rbx
.L175:
	xorl	%eax, %eax
	cmpq	$0, (%rbx)
	sete	%al
.L164:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L169:
	.cfi_restore_state
	movq	8(%rbx), %rax
	movq	16(%rax), %rbx
	addq	$8, %rbx
	jmp	.L175
.L166:
	movq	8(%rbx), %rbx
	cmpl	$4, (%rbx)
	je	.L179
	movl	$184, %r8d
	movl	$152, %edx
	movl	$129, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L164
.L167:
	movq	8(%rbx), %rax
	movq	8(%rax), %rbx
	addq	$16, %rbx
	jmp	.L175
.L170:
	movq	8(%rbx), %rax
	movq	24(%rax), %rbx
	addq	$16, %rbx
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L173:
	movq	8(%rbx), %rax
	movq	40(%rax), %rbx
	addq	$8, %rbx
	jmp	.L175
	.cfi_endproc
.LFE1406:
	.size	CMS_is_detached, .-CMS_is_detached
	.p2align 4
	.globl	CMS_set_detached
	.type	CMS_set_detached, @function
CMS_set_detached:
.LFB1407:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rdi
	movl	%esi, %ebx
	call	OBJ_obj2nid@PLT
	cmpl	$26, %eax
	jg	.L181
	cmpl	$20, %eax
	jle	.L182
	subl	$21, %eax
	cmpl	$5, %eax
	ja	.L182
	leaq	.L184(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L184:
	.long	.L200-.L184
	.long	.L185-.L184
	.long	.L186-.L184
	.long	.L182-.L184
	.long	.L185-.L184
	.long	.L183-.L184
	.text
	.p2align 4,,10
	.p2align 3
.L181:
	cmpl	$205, %eax
	je	.L189
	cmpl	$786, %eax
	jne	.L182
	movq	8(%r12), %rax
	movq	24(%rax), %r12
.L200:
	addq	$8, %r12
.L191:
	movq	(%r12), %rdi
	testl	%ebx, %ebx
	jne	.L201
.L198:
	testq	%rdi, %rdi
	je	.L202
.L194:
	orq	$32, 16(%rdi)
	movl	$1, %eax
.L180:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L185:
	.cfi_restore_state
	movq	8(%r12), %rax
	movq	16(%rax), %r12
	addq	$8, %r12
	movq	(%r12), %rdi
	testl	%ebx, %ebx
	je	.L198
.L201:
	call	ASN1_OCTET_STRING_free@PLT
	movq	$0, (%r12)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L182:
	.cfi_restore_state
	movq	8(%r12), %r12
	cmpl	$4, (%r12)
	je	.L200
	movl	$184, %r8d
	movl	$152, %edx
	movl	$129, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L180
.L183:
	movq	8(%r12), %rax
	movq	8(%rax), %r12
	addq	$16, %r12
	jmp	.L191
.L186:
	movq	8(%r12), %rax
	movq	24(%rax), %r12
	addq	$16, %r12
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L202:
	call	ASN1_OCTET_STRING_new@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L194
	movl	$280, %r8d
	movl	$65, %edx
	movl	$147, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore_state
	movq	8(%r12), %rax
	movq	40(%rax), %r12
	addq	$8, %r12
	jmp	.L191
	.cfi_endproc
.LFE1407:
	.size	CMS_set_detached, .-CMS_set_detached
	.p2align 4
	.globl	cms_DigestAlgorithm_init_bio
	.type	cms_DigestAlgorithm_init_bio, @function
cms_DigestAlgorithm_init_bio:
.LFB1408:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-32(%rbp), %rdi
	subq	$16, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	X509_ALGOR_get0@PLT
	movq	-32(%rbp), %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	testq	%rax, %rax
	je	.L214
	movq	%rax, %r13
	call	BIO_f_md@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L207
	xorl	%edx, %edx
	movq	%r13, %rcx
	movl	$111, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	testq	%rax, %rax
	je	.L207
.L203:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L215
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L207:
	.cfi_restore_state
	movl	$300, %r8d
	movl	$119, %edx
	movl	$116, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L205:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	BIO_free@PLT
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L214:
	movl	$294, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$149, %edx
	xorl	%r12d, %r12d
	movl	$116, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L205
.L215:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1408:
	.size	cms_DigestAlgorithm_init_bio, .-cms_DigestAlgorithm_init_bio
	.p2align 4
	.globl	cms_DigestAlgorithm_find_ctx
	.type	cms_DigestAlgorithm_find_ctx, @function
cms_DigestAlgorithm_find_ctx:
.LFB1409:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rcx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	leaq	-56(%rbp), %rdi
	.cfi_offset 13, -32
	leaq	-48(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	X509_ALGOR_get0@PLT
	movq	-56(%rbp), %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %ebx
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L217:
	xorl	%edx, %edx
	movq	%r13, %rcx
	movl	$120, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	movq	-48(%rbp), %rdi
	call	EVP_MD_CTX_md@PLT
	movq	%rax, %rdi
	call	EVP_MD_type@PLT
	cmpl	%ebx, %eax
	je	.L220
	movq	-48(%rbp), %rdi
	call	EVP_MD_CTX_md@PLT
	movq	%rax, %rdi
	call	EVP_MD_pkey_type@PLT
	cmpl	%ebx, %eax
	je	.L220
	movq	%r12, %rdi
	call	BIO_next@PLT
	movq	%rax, %r12
.L221:
	movq	%r12, %rdi
	movl	$520, %esi
	call	BIO_find_type@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L217
	movl	$323, %r8d
	movl	$131, %edx
	movl	$115, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L220:
	movq	-48(%rbp), %rsi
	movq	%r14, %rdi
	call	EVP_MD_CTX_copy_ex@PLT
.L216:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L224
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L224:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1409:
	.size	cms_DigestAlgorithm_find_ctx, .-cms_DigestAlgorithm_find_ctx
	.p2align 4
	.globl	CMS_add0_CertificateChoices
	.type	CMS_add0_CertificateChoices, @function
CMS_add0_CertificateChoices:
.LFB1411:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$22, %eax
	je	.L226
	cmpl	$23, %eax
	je	.L227
	movl	$353, %r8d
	movl	$152, %edx
	movl	$128, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L243:
	xorl	%r12d, %r12d
.L225:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L226:
	.cfi_restore_state
	movq	8(%r12), %rbx
	addq	$24, %rbx
	cmpq	$0, (%rbx)
	je	.L245
.L234:
	leaq	CMS_CertificateChoices_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L243
	movq	(%rbx), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L225
	movq	%r12, %rdi
	leaq	CMS_CertificateChoices_it(%rip), %rsi
	xorl	%r12d, %r12d
	call	ASN1_item_free@PLT
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L227:
	movq	8(%r12), %rax
	movq	8(%rax), %rbx
	testq	%rbx, %rbx
	je	.L243
	cmpq	$0, (%rbx)
	jne	.L234
.L245:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L243
	jmp	.L234
	.cfi_endproc
.LFE1411:
	.size	CMS_add0_CertificateChoices, .-CMS_add0_CertificateChoices
	.p2align 4
	.globl	CMS_add0_cert
	.type	CMS_add0_cert, @function
CMS_add0_cert:
.LFB1412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$22, %eax
	je	.L247
	cmpl	$23, %eax
	je	.L248
	movl	$353, %r8d
	movl	$152, %edx
	movl	$128, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L262:
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L247:
	.cfi_restore_state
	movq	8(%r14), %rax
	leaq	24(%rax), %r12
.L250:
	xorl	%ebx, %ebx
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L253:
	addl	$1, %ebx
.L256:
	movq	(%r12), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L263
	movq	(%r12), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movl	(%rax), %edx
	testl	%edx, %edx
	jne	.L253
	movq	8(%rax), %rdi
	movq	%r13, %rsi
	call	X509_cmp@PLT
	testl	%eax, %eax
	jne	.L253
	movl	$393, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$175, %edx
	movl	%eax, -36(%rbp)
	movl	$164, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	movl	-36(%rbp), %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L263:
	.cfi_restore_state
	movq	%r14, %rdi
	call	CMS_add0_CertificateChoices
	testq	%rax, %rax
	je	.L262
	movl	$0, (%rax)
	movq	%r13, 8(%rax)
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L248:
	.cfi_restore_state
	movq	8(%r14), %rax
	movq	8(%rax), %r12
	testq	%r12, %r12
	jne	.L250
	jmp	.L262
	.cfi_endproc
.LFE1412:
	.size	CMS_add0_cert, .-CMS_add0_cert
	.p2align 4
	.globl	CMS_add1_cert
	.type	CMS_add1_cert, @function
CMS_add1_cert:
.LFB1413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	call	CMS_add0_cert
	movl	%eax, %r12d
	testl	%eax, %eax
	jle	.L264
	movq	%r13, %rdi
	call	X509_up_ref@PLT
.L264:
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1413:
	.size	CMS_add1_cert, .-CMS_add1_cert
	.p2align 4
	.globl	CMS_add0_RevocationInfoChoice
	.type	CMS_add0_RevocationInfoChoice, @function
CMS_add0_RevocationInfoChoice:
.LFB1415:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$22, %eax
	je	.L268
	cmpl	$23, %eax
	je	.L269
	movl	$430, %r8d
	movl	$152, %edx
	movl	$132, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L285:
	xorl	%r12d, %r12d
.L267:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L268:
	.cfi_restore_state
	movq	8(%r12), %rbx
	addq	$32, %rbx
	cmpq	$0, (%rbx)
	je	.L287
.L276:
	leaq	CMS_RevocationInfoChoice_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L285
	movq	(%rbx), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L267
	movq	%r12, %rdi
	leaq	CMS_RevocationInfoChoice_it(%rip), %rsi
	xorl	%r12d, %r12d
	call	ASN1_item_free@PLT
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L269:
	movq	8(%r12), %rax
	movq	8(%rax), %rax
	leaq	8(%rax), %rbx
	testq	%rax, %rax
	je	.L285
	cmpq	$0, (%rbx)
	jne	.L276
.L287:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L285
	jmp	.L276
	.cfi_endproc
.LFE1415:
	.size	CMS_add0_RevocationInfoChoice, .-CMS_add0_RevocationInfoChoice
	.p2align 4
	.globl	CMS_add0_crl
	.type	CMS_add0_crl, @function
CMS_add0_crl:
.LFB1416:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$22, %eax
	je	.L289
	cmpl	$23, %eax
	je	.L290
	movl	$430, %r8d
	movl	$152, %edx
	movl	$132, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L307:
	xorl	%eax, %eax
.L288:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L289:
	.cfi_restore_state
	movq	8(%r12), %r13
	addq	$32, %r13
	cmpq	$0, 0(%r13)
	je	.L309
.L298:
	leaq	CMS_RevocationInfoChoice_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L307
	movq	0(%r13), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L310
	movl	$0, (%r12)
	movl	$1, %eax
	movq	%rbx, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L290:
	.cfi_restore_state
	movq	8(%r12), %rax
	movq	8(%rax), %rax
	leaq	8(%rax), %r13
	testq	%rax, %rax
	je	.L307
	cmpq	$0, 0(%r13)
	jne	.L298
.L309:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	je	.L307
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L310:
	leaq	CMS_RevocationInfoChoice_it(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, -36(%rbp)
	call	ASN1_item_free@PLT
	movl	-36(%rbp), %eax
	jmp	.L288
	.cfi_endproc
.LFE1416:
	.size	CMS_add0_crl, .-CMS_add0_crl
	.p2align 4
	.globl	CMS_add1_crl
	.type	CMS_add1_crl, @function
CMS_add1_crl:
.LFB1417:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$22, %eax
	je	.L312
	cmpl	$23, %eax
	je	.L313
	movl	$430, %r8d
	movl	$152, %edx
	movl	$132, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L330:
	xorl	%eax, %eax
.L311:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L312:
	.cfi_restore_state
	movq	8(%r12), %rbx
	addq	$32, %rbx
	cmpq	$0, (%rbx)
	je	.L332
.L321:
	leaq	CMS_RevocationInfoChoice_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L330
	movq	(%rbx), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L333
	movq	%r13, 8(%r12)
	movq	%r13, %rdi
	movl	$0, (%r12)
	call	X509_CRL_up_ref@PLT
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L313:
	.cfi_restore_state
	movq	8(%r12), %rax
	movq	8(%rax), %rax
	leaq	8(%rax), %rbx
	testq	%rax, %rax
	je	.L330
	cmpq	$0, (%rbx)
	jne	.L321
.L332:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L330
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L333:
	leaq	CMS_RevocationInfoChoice_it(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, -36(%rbp)
	call	ASN1_item_free@PLT
	movl	-36(%rbp), %eax
	jmp	.L311
	.cfi_endproc
.LFE1417:
	.size	CMS_add1_crl, .-CMS_add1_crl
	.p2align 4
	.globl	CMS_get1_certs
	.type	CMS_get1_certs, @function
CMS_get1_certs:
.LFB1418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$22, %eax
	je	.L335
	cmpl	$23, %eax
	je	.L336
	movl	$353, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$152, %edx
	xorl	%r14d, %r14d
	movl	$128, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
.L334:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L335:
	.cfi_restore_state
	movq	8(%rbx), %r13
	addq	$24, %r13
.L338:
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L342:
	movq	8(%rbx), %rsi
	movq	%r14, %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L353
	movq	8(%rbx), %rdi
	call	X509_up_ref@PLT
.L341:
	addl	$1, %r12d
.L346:
	movq	0(%r13), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L334
	movq	0(%r13), %rdi
	movl	%r12d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rbx
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L341
	testq	%r14, %r14
	jne	.L342
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L342
.L352:
	xorl	%r14d, %r14d
.L354:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L336:
	.cfi_restore_state
	movq	8(%rbx), %rax
	movq	8(%rax), %r13
	testq	%r13, %r13
	jne	.L338
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L353:
	movq	X509_free@GOTPCREL(%rip), %rsi
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L354
	.cfi_endproc
.LFE1418:
	.size	CMS_get1_certs, .-CMS_get1_certs
	.p2align 4
	.globl	CMS_get1_crls
	.type	CMS_get1_crls, @function
CMS_get1_crls:
.LFB1419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$22, %eax
	je	.L356
	cmpl	$23, %eax
	je	.L357
	movl	$430, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$152, %edx
	xorl	%r14d, %r14d
	movl	$132, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
.L355:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L356:
	.cfi_restore_state
	movq	8(%rbx), %r13
	addq	$32, %r13
.L359:
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L363:
	movq	8(%rbx), %rsi
	movq	%r14, %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L374
	movq	8(%rbx), %rdi
	call	X509_CRL_up_ref@PLT
.L362:
	addl	$1, %r12d
.L367:
	movq	0(%r13), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L355
	movq	0(%r13), %rdi
	movl	%r12d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rbx
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L362
	testq	%r14, %r14
	jne	.L363
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L363
.L373:
	xorl	%r14d, %r14d
.L375:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L357:
	.cfi_restore_state
	movq	8(%rbx), %rax
	movq	8(%rax), %rax
	leaq	8(%rax), %r13
	testq	%rax, %rax
	jne	.L359
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L374:
	movq	X509_CRL_free@GOTPCREL(%rip), %rsi
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L375
	.cfi_endproc
.LFE1419:
	.size	CMS_get1_crls, .-CMS_get1_crls
	.p2align 4
	.globl	cms_ias_cert_cmp
	.type	cms_ias_cert_cmp, @function
cms_ias_cert_cmp:
.LFB1420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	call	X509_get_issuer_name@PLT
	movq	(%rbx), %rdi
	movq	%rax, %rsi
	call	X509_NAME_cmp@PLT
	testl	%eax, %eax
	je	.L379
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L379:
	.cfi_restore_state
	movq	%r12, %rdi
	call	X509_get_serialNumber@PLT
	movq	8(%rbx), %rdi
	popq	%rbx
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	ASN1_INTEGER_cmp@PLT
	.cfi_endproc
.LFE1420:
	.size	cms_ias_cert_cmp, .-cms_ias_cert_cmp
	.p2align 4
	.globl	cms_keyid_cert_cmp
	.type	cms_keyid_cert_cmp, @function
cms_keyid_cert_cmp:
.LFB1421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	subq	$8, %rsp
	call	X509_get0_subject_key_id@PLT
	testq	%rax, %rax
	je	.L381
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ASN1_OCTET_STRING_cmp@PLT
.L381:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$-1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1421:
	.size	cms_keyid_cert_cmp, .-cms_keyid_cert_cmp
	.p2align 4
	.globl	cms_set1_ias
	.type	cms_set1_ias, @function
cms_set1_ias:
.LFB1422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	CMS_IssuerAndSerialNumber_it(%rip), %rdi
	subq	$8, %rsp
	call	ASN1_item_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L384
	movq	%r13, %rdi
	call	X509_get_issuer_name@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	X509_NAME_set@PLT
	testl	%eax, %eax
	jne	.L396
.L384:
	movq	%r12, %rdi
	leaq	CMS_IssuerAndSerialNumber_it(%rip), %rsi
	call	ASN1_item_free@PLT
	movl	$566, %r8d
	movl	$65, %edx
	leaq	.LC0(%rip), %rcx
	movl	$176, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L396:
	.cfi_restore_state
	movq	%r13, %rdi
	call	X509_get_serialNumber@PLT
	movq	8(%r12), %rdi
	movq	%rax, %rsi
	call	ASN1_STRING_copy@PLT
	testl	%eax, %eax
	je	.L384
	movq	(%rbx), %rdi
	leaq	CMS_IssuerAndSerialNumber_it(%rip), %rsi
	call	ASN1_item_free@PLT
	movq	%r12, (%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1422:
	.size	cms_set1_ias, .-cms_set1_ias
	.p2align 4
	.globl	cms_set1_keyid
	.type	cms_set1_keyid, @function
cms_set1_keyid:
.LFB1423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	call	X509_get0_subject_key_id@PLT
	testq	%rax, %rax
	je	.L402
	movq	%rax, %rdi
	call	ASN1_STRING_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L403
	movq	(%rbx), %rdi
	call	ASN1_OCTET_STRING_free@PLT
	movq	%r12, (%rbx)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L402:
	.cfi_restore_state
	movl	$576, %r8d
	movl	$160, %edx
	movl	$177, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L403:
	.cfi_restore_state
	movl	$581, %r8d
	movl	$65, %edx
	movl	$177, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1423:
	.size	cms_set1_keyid, .-cms_set1_keyid
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
