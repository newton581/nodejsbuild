	.file	"ssl_txt.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unknown"
.LC1:
	.string	"None"
.LC2:
	.string	"yes"
.LC3:
	.string	"no"
.LC4:
	.string	"SSL-Session:\n"
.LC5:
	.string	"    Protocol  : %s\n"
.LC6:
	.string	"    Cipher    : %06lX\n"
.LC7:
	.string	"    Session-ID: "
.LC8:
	.string	"    Cipher    : %04lX\n"
.LC9:
	.string	"    Cipher    : %s\n"
.LC10:
	.string	"%02X"
.LC11:
	.string	"\n    Session-ID-ctx: "
.LC12:
	.string	"\n    Resumption PSK: "
.LC13:
	.string	"\n    Master-Key: "
.LC14:
	.string	"\n    PSK identity: "
.LC15:
	.string	"%s"
.LC16:
	.string	"\n    PSK identity hint: "
.LC17:
	.string	"\n    SRP username: "
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC18:
	.string	"\n    TLS session ticket lifetime hint: %ld (seconds)"
	.section	.rodata.str1.1
.LC19:
	.string	"\n    TLS session ticket:\n"
.LC20:
	.string	"\n    Start Time: %ld"
.LC21:
	.string	"\n"
.LC22:
	.string	"\n    Timeout   : %ld (sec)"
.LC23:
	.string	"    Verify return code: "
.LC24:
	.string	"%ld (%s)\n"
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"    Extended master secret: %s\n"
	.section	.rodata.str1.1
.LC26:
	.string	"    Max Early Data: %u\n"
	.text
	.p2align 4
	.type	SSL_SESSION_print.part.0, @function
SSL_SESSION_print.part.0:
.LFB967:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	movl	(%rsi), %r15d
	leaq	.LC4(%rip), %rsi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L4
	movl	(%rbx), %edi
	call	ssl_protocol_to_string@PLT
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L4
	movq	504(%rbx), %rax
	testq	%rax, %rax
	je	.L51
	movq	8(%rax), %rdx
	leaq	.LC0(%rip), %rax
	leaq	.LC9(%rip), %rsi
	testq	%rdx, %rdx
	cmove	%rax, %rdx
.L49:
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L4
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L4
	cmpq	$0, 336(%rbx)
	je	.L10
	xorl	%r13d, %r13d
	leaq	.LC10(%rip), %r14
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L52:
	addq	$1, %r13
	cmpq	336(%rbx), %r13
	jnb	.L10
.L11:
	movzbl	344(%rbx,%r13), %edx
	xorl	%eax, %eax
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jg	.L52
	.p2align 4,,10
	.p2align 3
.L4:
	xorl	%eax, %eax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	movq	512(%rbx), %rdx
	movq	%rdx, %rax
	andl	$4278190080, %eax
	cmpq	$33554432, %rax
	je	.L53
	movzwl	%dx, %edx
	leaq	.LC8(%rip), %rsi
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L10:
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L4
	cmpq	$0, 376(%rbx)
	je	.L12
	xorl	%r13d, %r13d
	leaq	.LC10(%rip), %r14
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L54:
	addq	$1, %r13
	cmpq	376(%rbx), %r13
	jnb	.L12
.L13:
	movzbl	384(%rbx,%r13), %edx
	xorl	%eax, %eax
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jg	.L54
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L12:
	cmpl	$772, %r15d
	je	.L55
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L4
.L15:
	xorl	%r13d, %r13d
	cmpq	$0, 8(%rbx)
	leaq	.LC10(%rip), %r14
	jne	.L16
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L56:
	addq	$1, %r13
	cmpq	8(%rbx), %r13
	jnb	.L18
.L16:
	movzbl	80(%rbx,%r13), %edx
	xorl	%eax, %eax
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jg	.L56
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L53:
	andl	$16777215, %edx
	leaq	.LC6(%rip), %rsi
	jmp	.L49
.L18:
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L4
	movq	424(%rbx), %rdx
	leaq	.LC1(%rip), %r13
	leaq	.LC15(%rip), %rsi
	movq	%r12, %rdi
	testq	%rdx, %rdx
	cmove	%r13, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L4
	leaq	.LC16(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L4
	movq	416(%rbx), %rdx
	leaq	.LC15(%rip), %rsi
	movq	%r12, %rdi
	testq	%rdx, %rdx
	cmove	%r13, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L4
	leaq	.LC17(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L4
	movq	608(%rbx), %rdx
	leaq	.LC15(%rip), %rsi
	movq	%r12, %rdi
	testq	%rdx, %rdx
	cmove	%r13, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L4
	movq	568(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.L22
.L25:
	cmpq	$0, 552(%rbx)
	je	.L24
	leaq	.LC19(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L4
	movq	552(%rbx), %rsi
	movl	560(%rbx), %edx
	movl	$4, %ecx
	movq	%r12, %rdi
	call	BIO_dump_indent@PLT
	testl	%eax, %eax
	jle	.L4
.L24:
	movq	488(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.L57
.L27:
	movq	480(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L29
	xorl	%eax, %eax
	leaq	.LC22(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L4
.L29:
	leaq	.LC21(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L4
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L4
	movq	464(%rbx), %rdi
	call	X509_verify_cert_error_string@PLT
	movq	464(%rbx), %rdx
	leaq	.LC24(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L4
	testb	$1, 632(%rbx)
	leaq	.LC3(%rip), %rax
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rdx
	leaq	.LC25(%rip), %rsi
	cmove	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L4
	movl	$1, %eax
	cmpl	$772, %r15d
	jne	.L1
	movl	580(%rbx), %edx
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L55:
	leaq	.LC12(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jg	.L15
	jmp	.L4
.L22:
	xorl	%eax, %eax
	leaq	.LC18(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jg	.L25
	jmp	.L4
.L57:
	xorl	%eax, %eax
	leaq	.LC20(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jg	.L27
	jmp	.L4
	.cfi_endproc
.LFE967:
	.size	SSL_SESSION_print.part.0, .-SSL_SESSION_print.part.0
	.section	.rodata.str1.8
	.align 8
.LC27:
	.string	"../deps/openssl/openssl/ssl/ssl_txt.c"
	.text
	.p2align 4
	.globl	SSL_SESSION_print_fp
	.type	SSL_SESSION_print_fp, @function
SSL_SESSION_print_fp:
.LFB964:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	testq	%rax, %rax
	je	.L65
	movq	%r14, %rcx
	xorl	%edx, %edx
	movl	$106, %esi
	movq	%rax, %rdi
	movq	%rax, %r12
	xorl	%r14d, %r14d
	call	BIO_ctrl@PLT
	testq	%r13, %r13
	je	.L61
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	SSL_SESSION_print.part.0
	movl	%eax, %r14d
.L61:
	movq	%r12, %rdi
	call	BIO_free@PLT
	addq	$8, %rsp
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	movl	$22, %r8d
	leaq	.LC27(%rip), %rcx
	movl	$7, %edx
	xorl	%r14d, %r14d
	movl	$190, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE964:
	.size	SSL_SESSION_print_fp, .-SSL_SESSION_print_fp
	.p2align 4
	.globl	SSL_SESSION_print
	.type	SSL_SESSION_print, @function
SSL_SESSION_print:
.LFB965:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L67
	jmp	SSL_SESSION_print.part.0
	.p2align 4,,10
	.p2align 3
.L67:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE965:
	.size	SSL_SESSION_print, .-SSL_SESSION_print
	.section	.rodata.str1.1
.LC28:
	.string	"RSA "
.LC29:
	.string	"Session-ID:"
.LC30:
	.string	" Master-Key:"
	.text
	.p2align 4
	.globl	SSL_SESSION_print_keylog
	.type	SSL_SESSION_print_keylog, @function
SSL_SESSION_print_keylog:
.LFB966:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L81
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpq	$0, 336(%rsi)
	movq	%rsi, %rbx
	je	.L71
	cmpq	$0, 8(%rsi)
	jne	.L82
.L71:
	xorl	%eax, %eax
.L68:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	leaq	.LC28(%rip), %rsi
	movq	%rdi, %r12
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L71
	leaq	.LC29(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L71
	cmpq	$0, 336(%rbx)
	je	.L72
	xorl	%r13d, %r13d
	leaq	.LC10(%rip), %r14
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L83:
	addq	$1, %r13
	cmpq	%r13, 336(%rbx)
	jbe	.L72
.L73:
	movzbl	344(%rbx,%r13), %edx
	xorl	%eax, %eax
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jg	.L83
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	leaq	.LC30(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L71
	cmpq	$0, 8(%rbx)
	je	.L74
	xorl	%r13d, %r13d
	leaq	.LC10(%rip), %r14
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L84:
	addq	$1, %r13
	cmpq	%r13, 8(%rbx)
	jbe	.L74
.L75:
	movzbl	80(%rbx,%r13), %edx
	xorl	%eax, %eax
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jg	.L84
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L74:
	leaq	.LC21(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
	jmp	.L68
	.cfi_endproc
.LFE966:
	.size	SSL_SESSION_print_keylog, .-SSL_SESSION_print_keylog
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
