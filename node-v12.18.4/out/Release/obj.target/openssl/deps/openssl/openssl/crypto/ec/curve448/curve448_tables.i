# 1 "../deps/openssl/openssl/crypto/ec/curve448/curve448_tables.c"
# 1 "<built-in>"
# 1 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 1 "<command-line>" 2
# 1 "../deps/openssl/openssl/crypto/ec/curve448/curve448_tables.c"
# 12 "../deps/openssl/openssl/crypto/ec/curve448/curve448_tables.c"
# 1 "../deps/openssl/openssl/crypto/ec/curve448/field.h" 1
# 16 "../deps/openssl/openssl/crypto/ec/curve448/field.h"
# 1 "../deps/openssl/openssl/include/internal/constant_time.h" 1
# 13 "../deps/openssl/openssl/include/internal/constant_time.h"
# 1 "/usr/include/stdlib.h" 1 3 4
# 25 "/usr/include/stdlib.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/libc-header-start.h" 1 3 4
# 33 "/usr/include/x86_64-linux-gnu/bits/libc-header-start.h" 3 4
# 1 "/usr/include/features.h" 1 3 4
# 461 "/usr/include/features.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/sys/cdefs.h" 1 3 4
# 452 "/usr/include/x86_64-linux-gnu/sys/cdefs.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/wordsize.h" 1 3 4
# 453 "/usr/include/x86_64-linux-gnu/sys/cdefs.h" 2 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/long-double.h" 1 3 4
# 454 "/usr/include/x86_64-linux-gnu/sys/cdefs.h" 2 3 4
# 462 "/usr/include/features.h" 2 3 4
# 485 "/usr/include/features.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/gnu/stubs.h" 1 3 4
# 10 "/usr/include/x86_64-linux-gnu/gnu/stubs.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/gnu/stubs-64.h" 1 3 4
# 11 "/usr/include/x86_64-linux-gnu/gnu/stubs.h" 2 3 4
# 486 "/usr/include/features.h" 2 3 4
# 34 "/usr/include/x86_64-linux-gnu/bits/libc-header-start.h" 2 3 4
# 26 "/usr/include/stdlib.h" 2 3 4





# 1 "/usr/lib/gcc/x86_64-linux-gnu/9/include/stddef.h" 1 3 4
# 209 "/usr/lib/gcc/x86_64-linux-gnu/9/include/stddef.h" 3 4

# 209 "/usr/lib/gcc/x86_64-linux-gnu/9/include/stddef.h" 3 4
typedef long unsigned int size_t;
# 321 "/usr/lib/gcc/x86_64-linux-gnu/9/include/stddef.h" 3 4
typedef int wchar_t;
# 32 "/usr/include/stdlib.h" 2 3 4







# 1 "/usr/include/x86_64-linux-gnu/bits/waitflags.h" 1 3 4
# 52 "/usr/include/x86_64-linux-gnu/bits/waitflags.h" 3 4
typedef enum
{
  P_ALL,
  P_PID,
  P_PGID
} idtype_t;
# 40 "/usr/include/stdlib.h" 2 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/waitstatus.h" 1 3 4
# 41 "/usr/include/stdlib.h" 2 3 4
# 55 "/usr/include/stdlib.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/floatn.h" 1 3 4
# 120 "/usr/include/x86_64-linux-gnu/bits/floatn.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/floatn-common.h" 1 3 4
# 24 "/usr/include/x86_64-linux-gnu/bits/floatn-common.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/long-double.h" 1 3 4
# 25 "/usr/include/x86_64-linux-gnu/bits/floatn-common.h" 2 3 4
# 121 "/usr/include/x86_64-linux-gnu/bits/floatn.h" 2 3 4
# 56 "/usr/include/stdlib.h" 2 3 4


typedef struct
  {
    int quot;
    int rem;
  } div_t;



typedef struct
  {
    long int quot;
    long int rem;
  } ldiv_t;





__extension__ typedef struct
  {
    long long int quot;
    long long int rem;
  } lldiv_t;
# 97 "/usr/include/stdlib.h" 3 4
extern size_t __ctype_get_mb_cur_max (void) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__warn_unused_result__));



extern double atof (const char *__nptr)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1))) __attribute__ ((__warn_unused_result__));

extern int atoi (const char *__nptr)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1))) __attribute__ ((__warn_unused_result__));

extern long int atol (const char *__nptr)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1))) __attribute__ ((__warn_unused_result__));



__extension__ extern long long int atoll (const char *__nptr)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1))) __attribute__ ((__warn_unused_result__));



extern double strtod (const char *__restrict __nptr,
        char **__restrict __endptr)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));



extern float strtof (const char *__restrict __nptr,
       char **__restrict __endptr) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));

extern long double strtold (const char *__restrict __nptr,
       char **__restrict __endptr)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
# 176 "/usr/include/stdlib.h" 3 4
extern long int strtol (const char *__restrict __nptr,
   char **__restrict __endptr, int __base)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));

extern unsigned long int strtoul (const char *__restrict __nptr,
      char **__restrict __endptr, int __base)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));



__extension__
extern long long int strtoq (const char *__restrict __nptr,
        char **__restrict __endptr, int __base)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));

__extension__
extern unsigned long long int strtouq (const char *__restrict __nptr,
           char **__restrict __endptr, int __base)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));




__extension__
extern long long int strtoll (const char *__restrict __nptr,
         char **__restrict __endptr, int __base)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));

__extension__
extern unsigned long long int strtoull (const char *__restrict __nptr,
     char **__restrict __endptr, int __base)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
# 360 "/usr/include/stdlib.h" 3 4
extern __inline __attribute__ ((__gnu_inline__)) int
__attribute__ ((__nothrow__ , __leaf__)) atoi (const char *__nptr)
{
  return (int) strtol (__nptr, (char **) ((void *)0), 10);
}
extern __inline __attribute__ ((__gnu_inline__)) long int
__attribute__ ((__nothrow__ , __leaf__)) atol (const char *__nptr)
{
  return strtol (__nptr, (char **) ((void *)0), 10);
}


__extension__ extern __inline __attribute__ ((__gnu_inline__)) long long int
__attribute__ ((__nothrow__ , __leaf__)) atoll (const char *__nptr)
{
  return strtoll (__nptr, (char **) ((void *)0), 10);
}
# 385 "/usr/include/stdlib.h" 3 4
extern char *l64a (long int __n) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__warn_unused_result__));


extern long int a64l (const char *__s)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1))) __attribute__ ((__warn_unused_result__));




# 1 "/usr/include/x86_64-linux-gnu/sys/types.h" 1 3 4
# 27 "/usr/include/x86_64-linux-gnu/sys/types.h" 3 4


# 1 "/usr/include/x86_64-linux-gnu/bits/types.h" 1 3 4
# 27 "/usr/include/x86_64-linux-gnu/bits/types.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/wordsize.h" 1 3 4
# 28 "/usr/include/x86_64-linux-gnu/bits/types.h" 2 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/timesize.h" 1 3 4
# 29 "/usr/include/x86_64-linux-gnu/bits/types.h" 2 3 4


typedef unsigned char __u_char;
typedef unsigned short int __u_short;
typedef unsigned int __u_int;
typedef unsigned long int __u_long;


typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef signed short int __int16_t;
typedef unsigned short int __uint16_t;
typedef signed int __int32_t;
typedef unsigned int __uint32_t;

typedef signed long int __int64_t;
typedef unsigned long int __uint64_t;






typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;



typedef long int __quad_t;
typedef unsigned long int __u_quad_t;







typedef long int __intmax_t;
typedef unsigned long int __uintmax_t;
# 141 "/usr/include/x86_64-linux-gnu/bits/types.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/typesizes.h" 1 3 4
# 142 "/usr/include/x86_64-linux-gnu/bits/types.h" 2 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/time64.h" 1 3 4
# 143 "/usr/include/x86_64-linux-gnu/bits/types.h" 2 3 4


typedef unsigned long int __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long int __ino_t;
typedef unsigned long int __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long int __nlink_t;
typedef long int __off_t;
typedef long int __off64_t;
typedef int __pid_t;
typedef struct { int __val[2]; } __fsid_t;
typedef long int __clock_t;
typedef unsigned long int __rlim_t;
typedef unsigned long int __rlim64_t;
typedef unsigned int __id_t;
typedef long int __time_t;
typedef unsigned int __useconds_t;
typedef long int __suseconds_t;

typedef int __daddr_t;
typedef int __key_t;


typedef int __clockid_t;


typedef void * __timer_t;


typedef long int __blksize_t;




typedef long int __blkcnt_t;
typedef long int __blkcnt64_t;


typedef unsigned long int __fsblkcnt_t;
typedef unsigned long int __fsblkcnt64_t;


typedef unsigned long int __fsfilcnt_t;
typedef unsigned long int __fsfilcnt64_t;


typedef long int __fsword_t;

typedef long int __ssize_t;


typedef long int __syscall_slong_t;

typedef unsigned long int __syscall_ulong_t;



typedef __off64_t __loff_t;
typedef char *__caddr_t;


typedef long int __intptr_t;


typedef unsigned int __socklen_t;




typedef int __sig_atomic_t;
# 30 "/usr/include/x86_64-linux-gnu/sys/types.h" 2 3 4



typedef __u_char u_char;
typedef __u_short u_short;
typedef __u_int u_int;
typedef __u_long u_long;
typedef __quad_t quad_t;
typedef __u_quad_t u_quad_t;
typedef __fsid_t fsid_t;


typedef __loff_t loff_t;




typedef __ino_t ino_t;
# 59 "/usr/include/x86_64-linux-gnu/sys/types.h" 3 4
typedef __dev_t dev_t;




typedef __gid_t gid_t;




typedef __mode_t mode_t;




typedef __nlink_t nlink_t;




typedef __uid_t uid_t;





typedef __off_t off_t;
# 97 "/usr/include/x86_64-linux-gnu/sys/types.h" 3 4
typedef __pid_t pid_t;





typedef __id_t id_t;




typedef __ssize_t ssize_t;





typedef __daddr_t daddr_t;
typedef __caddr_t caddr_t;





typedef __key_t key_t;




# 1 "/usr/include/x86_64-linux-gnu/bits/types/clock_t.h" 1 3 4






typedef __clock_t clock_t;
# 127 "/usr/include/x86_64-linux-gnu/sys/types.h" 2 3 4

# 1 "/usr/include/x86_64-linux-gnu/bits/types/clockid_t.h" 1 3 4






typedef __clockid_t clockid_t;
# 129 "/usr/include/x86_64-linux-gnu/sys/types.h" 2 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/types/time_t.h" 1 3 4






typedef __time_t time_t;
# 130 "/usr/include/x86_64-linux-gnu/sys/types.h" 2 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/types/timer_t.h" 1 3 4






typedef __timer_t timer_t;
# 131 "/usr/include/x86_64-linux-gnu/sys/types.h" 2 3 4
# 144 "/usr/include/x86_64-linux-gnu/sys/types.h" 3 4
# 1 "/usr/lib/gcc/x86_64-linux-gnu/9/include/stddef.h" 1 3 4
# 145 "/usr/include/x86_64-linux-gnu/sys/types.h" 2 3 4



typedef unsigned long int ulong;
typedef unsigned short int ushort;
typedef unsigned int uint;




# 1 "/usr/include/x86_64-linux-gnu/bits/stdint-intn.h" 1 3 4
# 24 "/usr/include/x86_64-linux-gnu/bits/stdint-intn.h" 3 4
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
# 156 "/usr/include/x86_64-linux-gnu/sys/types.h" 2 3 4


typedef __uint8_t u_int8_t;
typedef __uint16_t u_int16_t;
typedef __uint32_t u_int32_t;
typedef __uint64_t u_int64_t;


typedef int register_t __attribute__ ((__mode__ (__word__)));
# 176 "/usr/include/x86_64-linux-gnu/sys/types.h" 3 4
# 1 "/usr/include/endian.h" 1 3 4
# 24 "/usr/include/endian.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/endian.h" 1 3 4
# 35 "/usr/include/x86_64-linux-gnu/bits/endian.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/endianness.h" 1 3 4
# 36 "/usr/include/x86_64-linux-gnu/bits/endian.h" 2 3 4
# 25 "/usr/include/endian.h" 2 3 4
# 35 "/usr/include/endian.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/byteswap.h" 1 3 4
# 33 "/usr/include/x86_64-linux-gnu/bits/byteswap.h" 3 4
static __inline __uint16_t
__bswap_16 (__uint16_t __bsx)
{

  return __builtin_bswap16 (__bsx);



}






static __inline __uint32_t
__bswap_32 (__uint32_t __bsx)
{

  return __builtin_bswap32 (__bsx);



}
# 69 "/usr/include/x86_64-linux-gnu/bits/byteswap.h" 3 4
__extension__ static __inline __uint64_t
__bswap_64 (__uint64_t __bsx)
{

  return __builtin_bswap64 (__bsx);



}
# 36 "/usr/include/endian.h" 2 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/uintn-identity.h" 1 3 4
# 32 "/usr/include/x86_64-linux-gnu/bits/uintn-identity.h" 3 4
static __inline __uint16_t
__uint16_identity (__uint16_t __x)
{
  return __x;
}

static __inline __uint32_t
__uint32_identity (__uint32_t __x)
{
  return __x;
}

static __inline __uint64_t
__uint64_identity (__uint64_t __x)
{
  return __x;
}
# 37 "/usr/include/endian.h" 2 3 4
# 177 "/usr/include/x86_64-linux-gnu/sys/types.h" 2 3 4


# 1 "/usr/include/x86_64-linux-gnu/sys/select.h" 1 3 4
# 30 "/usr/include/x86_64-linux-gnu/sys/select.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/select.h" 1 3 4
# 22 "/usr/include/x86_64-linux-gnu/bits/select.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/wordsize.h" 1 3 4
# 23 "/usr/include/x86_64-linux-gnu/bits/select.h" 2 3 4
# 31 "/usr/include/x86_64-linux-gnu/sys/select.h" 2 3 4


# 1 "/usr/include/x86_64-linux-gnu/bits/types/sigset_t.h" 1 3 4



# 1 "/usr/include/x86_64-linux-gnu/bits/types/__sigset_t.h" 1 3 4




typedef struct
{
  unsigned long int __val[(1024 / (8 * sizeof (unsigned long int)))];
} __sigset_t;
# 5 "/usr/include/x86_64-linux-gnu/bits/types/sigset_t.h" 2 3 4


typedef __sigset_t sigset_t;
# 34 "/usr/include/x86_64-linux-gnu/sys/select.h" 2 3 4



# 1 "/usr/include/x86_64-linux-gnu/bits/types/struct_timeval.h" 1 3 4







struct timeval
{
  __time_t tv_sec;
  __suseconds_t tv_usec;
};
# 38 "/usr/include/x86_64-linux-gnu/sys/select.h" 2 3 4

# 1 "/usr/include/x86_64-linux-gnu/bits/types/struct_timespec.h" 1 3 4
# 10 "/usr/include/x86_64-linux-gnu/bits/types/struct_timespec.h" 3 4
struct timespec
{
  __time_t tv_sec;



  __syscall_slong_t tv_nsec;
# 26 "/usr/include/x86_64-linux-gnu/bits/types/struct_timespec.h" 3 4
};
# 40 "/usr/include/x86_64-linux-gnu/sys/select.h" 2 3 4



typedef __suseconds_t suseconds_t;





typedef long int __fd_mask;
# 59 "/usr/include/x86_64-linux-gnu/sys/select.h" 3 4
typedef struct
  {






    __fd_mask __fds_bits[1024 / (8 * (int) sizeof (__fd_mask))];


  } fd_set;






typedef __fd_mask fd_mask;
# 91 "/usr/include/x86_64-linux-gnu/sys/select.h" 3 4

# 101 "/usr/include/x86_64-linux-gnu/sys/select.h" 3 4
extern int select (int __nfds, fd_set *__restrict __readfds,
     fd_set *__restrict __writefds,
     fd_set *__restrict __exceptfds,
     struct timeval *__restrict __timeout);
# 113 "/usr/include/x86_64-linux-gnu/sys/select.h" 3 4
extern int pselect (int __nfds, fd_set *__restrict __readfds,
      fd_set *__restrict __writefds,
      fd_set *__restrict __exceptfds,
      const struct timespec *__restrict __timeout,
      const __sigset_t *__restrict __sigmask);





# 1 "/usr/include/x86_64-linux-gnu/bits/select2.h" 1 3 4
# 24 "/usr/include/x86_64-linux-gnu/bits/select2.h" 3 4
extern long int __fdelt_chk (long int __d);
extern long int __fdelt_warn (long int __d)
  __attribute__((__warning__ ("bit outside of fd_set selected")));
# 124 "/usr/include/x86_64-linux-gnu/sys/select.h" 2 3 4



# 180 "/usr/include/x86_64-linux-gnu/sys/types.h" 2 3 4





typedef __blksize_t blksize_t;






typedef __blkcnt_t blkcnt_t;



typedef __fsblkcnt_t fsblkcnt_t;



typedef __fsfilcnt_t fsfilcnt_t;
# 227 "/usr/include/x86_64-linux-gnu/sys/types.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/pthreadtypes.h" 1 3 4
# 23 "/usr/include/x86_64-linux-gnu/bits/pthreadtypes.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/thread-shared-types.h" 1 3 4
# 44 "/usr/include/x86_64-linux-gnu/bits/thread-shared-types.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/pthreadtypes-arch.h" 1 3 4
# 21 "/usr/include/x86_64-linux-gnu/bits/pthreadtypes-arch.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/wordsize.h" 1 3 4
# 22 "/usr/include/x86_64-linux-gnu/bits/pthreadtypes-arch.h" 2 3 4
# 45 "/usr/include/x86_64-linux-gnu/bits/thread-shared-types.h" 2 3 4




typedef struct __pthread_internal_list
{
  struct __pthread_internal_list *__prev;
  struct __pthread_internal_list *__next;
} __pthread_list_t;

typedef struct __pthread_internal_slist
{
  struct __pthread_internal_slist *__next;
} __pthread_slist_t;
# 74 "/usr/include/x86_64-linux-gnu/bits/thread-shared-types.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/struct_mutex.h" 1 3 4
# 22 "/usr/include/x86_64-linux-gnu/bits/struct_mutex.h" 3 4
struct __pthread_mutex_s
{
  int __lock;
  unsigned int __count;
  int __owner;

  unsigned int __nusers;



  int __kind;

  short __spins;
  short __elision;
  __pthread_list_t __list;
# 53 "/usr/include/x86_64-linux-gnu/bits/struct_mutex.h" 3 4
};
# 75 "/usr/include/x86_64-linux-gnu/bits/thread-shared-types.h" 2 3 4
# 87 "/usr/include/x86_64-linux-gnu/bits/thread-shared-types.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/struct_rwlock.h" 1 3 4
# 23 "/usr/include/x86_64-linux-gnu/bits/struct_rwlock.h" 3 4
struct __pthread_rwlock_arch_t
{
  unsigned int __readers;
  unsigned int __writers;
  unsigned int __wrphase_futex;
  unsigned int __writers_futex;
  unsigned int __pad3;
  unsigned int __pad4;

  int __cur_writer;
  int __shared;
  signed char __rwelision;




  unsigned char __pad1[7];


  unsigned long int __pad2;


  unsigned int __flags;
# 55 "/usr/include/x86_64-linux-gnu/bits/struct_rwlock.h" 3 4
};
# 88 "/usr/include/x86_64-linux-gnu/bits/thread-shared-types.h" 2 3 4




struct __pthread_cond_s
{
  __extension__ union
  {
    __extension__ unsigned long long int __wseq;
    struct
    {
      unsigned int __low;
      unsigned int __high;
    } __wseq32;
  };
  __extension__ union
  {
    __extension__ unsigned long long int __g1_start;
    struct
    {
      unsigned int __low;
      unsigned int __high;
    } __g1_start32;
  };
  unsigned int __g_refs[2] ;
  unsigned int __g_size[2];
  unsigned int __g1_orig_size;
  unsigned int __wrefs;
  unsigned int __g_signals[2];
};
# 24 "/usr/include/x86_64-linux-gnu/bits/pthreadtypes.h" 2 3 4



typedef unsigned long int pthread_t;




typedef union
{
  char __size[4];
  int __align;
} pthread_mutexattr_t;




typedef union
{
  char __size[4];
  int __align;
} pthread_condattr_t;



typedef unsigned int pthread_key_t;



typedef int pthread_once_t;


union pthread_attr_t
{
  char __size[56];
  long int __align;
};

typedef union pthread_attr_t pthread_attr_t;




typedef union
{
  struct __pthread_mutex_s __data;
  char __size[40];
  long int __align;
} pthread_mutex_t;


typedef union
{
  struct __pthread_cond_s __data;
  char __size[48];
  __extension__ long long int __align;
} pthread_cond_t;





typedef union
{
  struct __pthread_rwlock_arch_t __data;
  char __size[56];
  long int __align;
} pthread_rwlock_t;

typedef union
{
  char __size[8];
  long int __align;
} pthread_rwlockattr_t;





typedef volatile int pthread_spinlock_t;




typedef union
{
  char __size[32];
  long int __align;
} pthread_barrier_t;

typedef union
{
  char __size[4];
  int __align;
} pthread_barrierattr_t;
# 228 "/usr/include/x86_64-linux-gnu/sys/types.h" 2 3 4



# 395 "/usr/include/stdlib.h" 2 3 4






extern long int random (void) __attribute__ ((__nothrow__ , __leaf__));


extern void srandom (unsigned int __seed) __attribute__ ((__nothrow__ , __leaf__));





extern char *initstate (unsigned int __seed, char *__statebuf,
   size_t __statelen) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2)));



extern char *setstate (char *__statebuf) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));







struct random_data
  {
    int32_t *fptr;
    int32_t *rptr;
    int32_t *state;
    int rand_type;
    int rand_deg;
    int rand_sep;
    int32_t *end_ptr;
  };

extern int random_r (struct random_data *__restrict __buf,
       int32_t *__restrict __result) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));

extern int srandom_r (unsigned int __seed, struct random_data *__buf)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2)));

extern int initstate_r (unsigned int __seed, char *__restrict __statebuf,
   size_t __statelen,
   struct random_data *__restrict __buf)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2, 4)));

extern int setstate_r (char *__restrict __statebuf,
         struct random_data *__restrict __buf)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));





extern int rand (void) __attribute__ ((__nothrow__ , __leaf__));

extern void srand (unsigned int __seed) __attribute__ ((__nothrow__ , __leaf__));



extern int rand_r (unsigned int *__seed) __attribute__ ((__nothrow__ , __leaf__));







extern double drand48 (void) __attribute__ ((__nothrow__ , __leaf__));
extern double erand48 (unsigned short int __xsubi[3]) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));


extern long int lrand48 (void) __attribute__ ((__nothrow__ , __leaf__));
extern long int nrand48 (unsigned short int __xsubi[3])
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));


extern long int mrand48 (void) __attribute__ ((__nothrow__ , __leaf__));
extern long int jrand48 (unsigned short int __xsubi[3])
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));


extern void srand48 (long int __seedval) __attribute__ ((__nothrow__ , __leaf__));
extern unsigned short int *seed48 (unsigned short int __seed16v[3])
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
extern void lcong48 (unsigned short int __param[7]) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));





struct drand48_data
  {
    unsigned short int __x[3];
    unsigned short int __old_x[3];
    unsigned short int __c;
    unsigned short int __init;
    __extension__ unsigned long long int __a;

  };


extern int drand48_r (struct drand48_data *__restrict __buffer,
        double *__restrict __result) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));
extern int erand48_r (unsigned short int __xsubi[3],
        struct drand48_data *__restrict __buffer,
        double *__restrict __result) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));


extern int lrand48_r (struct drand48_data *__restrict __buffer,
        long int *__restrict __result)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));
extern int nrand48_r (unsigned short int __xsubi[3],
        struct drand48_data *__restrict __buffer,
        long int *__restrict __result)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));


extern int mrand48_r (struct drand48_data *__restrict __buffer,
        long int *__restrict __result)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));
extern int jrand48_r (unsigned short int __xsubi[3],
        struct drand48_data *__restrict __buffer,
        long int *__restrict __result)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));


extern int srand48_r (long int __seedval, struct drand48_data *__buffer)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2)));

extern int seed48_r (unsigned short int __seed16v[3],
       struct drand48_data *__buffer) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));

extern int lcong48_r (unsigned short int __param[7],
        struct drand48_data *__buffer)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));




extern void *malloc (size_t __size) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__malloc__))
     __attribute__ ((__alloc_size__ (1))) __attribute__ ((__warn_unused_result__));

extern void *calloc (size_t __nmemb, size_t __size)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__malloc__)) __attribute__ ((__alloc_size__ (1, 2))) __attribute__ ((__warn_unused_result__));






extern void *realloc (void *__ptr, size_t __size)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__warn_unused_result__)) __attribute__ ((__alloc_size__ (2)));







extern void *reallocarray (void *__ptr, size_t __nmemb, size_t __size)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__warn_unused_result__))
     __attribute__ ((__alloc_size__ (2, 3)));



extern void free (void *__ptr) __attribute__ ((__nothrow__ , __leaf__));


# 1 "/usr/include/alloca.h" 1 3 4
# 24 "/usr/include/alloca.h" 3 4
# 1 "/usr/lib/gcc/x86_64-linux-gnu/9/include/stddef.h" 1 3 4
# 25 "/usr/include/alloca.h" 2 3 4







extern void *alloca (size_t __size) __attribute__ ((__nothrow__ , __leaf__));






# 569 "/usr/include/stdlib.h" 2 3 4





extern void *valloc (size_t __size) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__malloc__))
     __attribute__ ((__alloc_size__ (1))) __attribute__ ((__warn_unused_result__));




extern int posix_memalign (void **__memptr, size_t __alignment, size_t __size)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1))) __attribute__ ((__warn_unused_result__));




extern void *aligned_alloc (size_t __alignment, size_t __size)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__malloc__)) __attribute__ ((__alloc_size__ (2))) __attribute__ ((__warn_unused_result__));



extern void abort (void) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));



extern int atexit (void (*__func) (void)) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));







extern int at_quick_exit (void (*__func) (void)) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));






extern int on_exit (void (*__func) (int __status, void *__arg), void *__arg)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));





extern void exit (int __status) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));





extern void quick_exit (int __status) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));





extern void _Exit (int __status) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));




extern char *getenv (const char *__name) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1))) __attribute__ ((__warn_unused_result__));
# 647 "/usr/include/stdlib.h" 3 4
extern int putenv (char *__string) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));





extern int setenv (const char *__name, const char *__value, int __replace)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2)));


extern int unsetenv (const char *__name) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));






extern int clearenv (void) __attribute__ ((__nothrow__ , __leaf__));
# 675 "/usr/include/stdlib.h" 3 4
extern char *mktemp (char *__template) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
# 688 "/usr/include/stdlib.h" 3 4
extern int mkstemp (char *__template) __attribute__ ((__nonnull__ (1))) __attribute__ ((__warn_unused_result__));
# 710 "/usr/include/stdlib.h" 3 4
extern int mkstemps (char *__template, int __suffixlen) __attribute__ ((__nonnull__ (1))) __attribute__ ((__warn_unused_result__));
# 731 "/usr/include/stdlib.h" 3 4
extern char *mkdtemp (char *__template) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1))) __attribute__ ((__warn_unused_result__));
# 784 "/usr/include/stdlib.h" 3 4
extern int system (const char *__command) __attribute__ ((__warn_unused_result__));
# 800 "/usr/include/stdlib.h" 3 4
extern char *realpath (const char *__restrict __name,
         char *__restrict __resolved) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__warn_unused_result__));






typedef int (*__compar_fn_t) (const void *, const void *);
# 820 "/usr/include/stdlib.h" 3 4
extern void *bsearch (const void *__key, const void *__base,
        size_t __nmemb, size_t __size, __compar_fn_t __compar)
     __attribute__ ((__nonnull__ (1, 2, 5))) __attribute__ ((__warn_unused_result__));


# 1 "/usr/include/x86_64-linux-gnu/bits/stdlib-bsearch.h" 1 3 4
# 19 "/usr/include/x86_64-linux-gnu/bits/stdlib-bsearch.h" 3 4
extern __inline __attribute__ ((__gnu_inline__)) void *
bsearch (const void *__key, const void *__base, size_t __nmemb, size_t __size,
  __compar_fn_t __compar)
{
  size_t __l, __u, __idx;
  const void *__p;
  int __comparison;

  __l = 0;
  __u = __nmemb;
  while (__l < __u)
    {
      __idx = (__l + __u) / 2;
      __p = (void *) (((const char *) __base) + (__idx * __size));
      __comparison = (*__compar) (__key, __p);
      if (__comparison < 0)
 __u = __idx;
      else if (__comparison > 0)
 __l = __idx + 1;
      else
 return (void *) __p;
    }

  return ((void *)0);
}
# 826 "/usr/include/stdlib.h" 2 3 4




extern void qsort (void *__base, size_t __nmemb, size_t __size,
     __compar_fn_t __compar) __attribute__ ((__nonnull__ (1, 4)));
# 840 "/usr/include/stdlib.h" 3 4
extern int abs (int __x) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__const__)) __attribute__ ((__warn_unused_result__));
extern long int labs (long int __x) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__const__)) __attribute__ ((__warn_unused_result__));


__extension__ extern long long int llabs (long long int __x)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__const__)) __attribute__ ((__warn_unused_result__));






extern div_t div (int __numer, int __denom)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__const__)) __attribute__ ((__warn_unused_result__));
extern ldiv_t ldiv (long int __numer, long int __denom)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__const__)) __attribute__ ((__warn_unused_result__));


__extension__ extern lldiv_t lldiv (long long int __numer,
        long long int __denom)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__const__)) __attribute__ ((__warn_unused_result__));
# 872 "/usr/include/stdlib.h" 3 4
extern char *ecvt (double __value, int __ndigit, int *__restrict __decpt,
     int *__restrict __sign) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (3, 4))) __attribute__ ((__warn_unused_result__));




extern char *fcvt (double __value, int __ndigit, int *__restrict __decpt,
     int *__restrict __sign) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (3, 4))) __attribute__ ((__warn_unused_result__));




extern char *gcvt (double __value, int __ndigit, char *__buf)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (3))) __attribute__ ((__warn_unused_result__));




extern char *qecvt (long double __value, int __ndigit,
      int *__restrict __decpt, int *__restrict __sign)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (3, 4))) __attribute__ ((__warn_unused_result__));
extern char *qfcvt (long double __value, int __ndigit,
      int *__restrict __decpt, int *__restrict __sign)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (3, 4))) __attribute__ ((__warn_unused_result__));
extern char *qgcvt (long double __value, int __ndigit, char *__buf)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (3))) __attribute__ ((__warn_unused_result__));




extern int ecvt_r (double __value, int __ndigit, int *__restrict __decpt,
     int *__restrict __sign, char *__restrict __buf,
     size_t __len) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (3, 4, 5)));
extern int fcvt_r (double __value, int __ndigit, int *__restrict __decpt,
     int *__restrict __sign, char *__restrict __buf,
     size_t __len) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (3, 4, 5)));

extern int qecvt_r (long double __value, int __ndigit,
      int *__restrict __decpt, int *__restrict __sign,
      char *__restrict __buf, size_t __len)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (3, 4, 5)));
extern int qfcvt_r (long double __value, int __ndigit,
      int *__restrict __decpt, int *__restrict __sign,
      char *__restrict __buf, size_t __len)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (3, 4, 5)));





extern int mblen (const char *__s, size_t __n) __attribute__ ((__nothrow__ , __leaf__));


extern int mbtowc (wchar_t *__restrict __pwc,
     const char *__restrict __s, size_t __n) __attribute__ ((__nothrow__ , __leaf__));


extern int wctomb (char *__s, wchar_t __wchar) __attribute__ ((__nothrow__ , __leaf__));



extern size_t mbstowcs (wchar_t *__restrict __pwcs,
   const char *__restrict __s, size_t __n) __attribute__ ((__nothrow__ , __leaf__));

extern size_t wcstombs (char *__restrict __s,
   const wchar_t *__restrict __pwcs, size_t __n)
     __attribute__ ((__nothrow__ , __leaf__));







extern int rpmatch (const char *__response) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1))) __attribute__ ((__warn_unused_result__));
# 957 "/usr/include/stdlib.h" 3 4
extern int getsubopt (char **__restrict __optionp,
        char *const *__restrict __tokens,
        char **__restrict __valuep)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2, 3))) __attribute__ ((__warn_unused_result__));
# 1003 "/usr/include/stdlib.h" 3 4
extern int getloadavg (double __loadavg[], int __nelem)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
# 1013 "/usr/include/stdlib.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/stdlib-float.h" 1 3 4
# 24 "/usr/include/x86_64-linux-gnu/bits/stdlib-float.h" 3 4
extern __inline __attribute__ ((__gnu_inline__)) double
__attribute__ ((__nothrow__ , __leaf__)) atof (const char *__nptr)
{
  return strtod (__nptr, (char **) ((void *)0));
}
# 1014 "/usr/include/stdlib.h" 2 3 4



# 1 "/usr/include/x86_64-linux-gnu/bits/stdlib.h" 1 3 4
# 23 "/usr/include/x86_64-linux-gnu/bits/stdlib.h" 3 4
extern char *__realpath_chk (const char *__restrict __name,
        char *__restrict __resolved,
        size_t __resolvedlen) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__warn_unused_result__));
extern char *__realpath_alias (const char *__restrict __name, char *__restrict __resolved) __asm__ ("" "realpath") __attribute__ ((__nothrow__ , __leaf__))

                                                 __attribute__ ((__warn_unused_result__));
extern char *__realpath_chk_warn (const char *__restrict __name, char *__restrict __resolved, size_t __resolvedlen) __asm__ ("" "__realpath_chk") __attribute__ ((__nothrow__ , __leaf__))


                                                __attribute__ ((__warn_unused_result__))
     __attribute__((__warning__ ("second argument of realpath must be either NULL or at " "least PATH_MAX bytes long buffer")))
                                      ;

extern __inline __attribute__ ((__always_inline__)) __attribute__ ((__gnu_inline__)) __attribute__ ((__artificial__)) __attribute__ ((__warn_unused_result__)) char *
__attribute__ ((__nothrow__ , __leaf__)) realpath (const char *__restrict __name, char *__restrict __resolved)
{
  if (__builtin_object_size (__resolved, 2 > 1) != (size_t) -1)
    {




      return __realpath_chk (__name, __resolved, __builtin_object_size (__resolved, 2 > 1));
    }

  return __realpath_alias (__name, __resolved);
}


extern int __ptsname_r_chk (int __fd, char *__buf, size_t __buflen,
       size_t __nreal) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2)));
extern int __ptsname_r_alias (int __fd, char *__buf, size_t __buflen) __asm__ ("" "ptsname_r") __attribute__ ((__nothrow__ , __leaf__))

     __attribute__ ((__nonnull__ (2)));
extern int __ptsname_r_chk_warn (int __fd, char *__buf, size_t __buflen, size_t __nreal) __asm__ ("" "__ptsname_r_chk") __attribute__ ((__nothrow__ , __leaf__))


     __attribute__ ((__nonnull__ (2))) __attribute__((__warning__ ("ptsname_r called with buflen bigger than " "size of buf")))
                   ;

extern __inline __attribute__ ((__always_inline__)) __attribute__ ((__gnu_inline__)) __attribute__ ((__artificial__)) int
__attribute__ ((__nothrow__ , __leaf__)) ptsname_r (int __fd, char *__buf, size_t __buflen)
{
  if (__builtin_object_size (__buf, 2 > 1) != (size_t) -1)
    {
      if (!__builtin_constant_p (__buflen))
 return __ptsname_r_chk (__fd, __buf, __buflen, __builtin_object_size (__buf, 2 > 1));
      if (__buflen > __builtin_object_size (__buf, 2 > 1))
 return __ptsname_r_chk_warn (__fd, __buf, __buflen, __builtin_object_size (__buf, 2 > 1));
    }
  return __ptsname_r_alias (__fd, __buf, __buflen);
}


extern int __wctomb_chk (char *__s, wchar_t __wchar, size_t __buflen)
  __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__warn_unused_result__));
extern int __wctomb_alias (char *__s, wchar_t __wchar) __asm__ ("" "wctomb") __attribute__ ((__nothrow__ , __leaf__))
              __attribute__ ((__warn_unused_result__));

extern __inline __attribute__ ((__always_inline__)) __attribute__ ((__gnu_inline__)) __attribute__ ((__artificial__)) __attribute__ ((__warn_unused_result__)) int
__attribute__ ((__nothrow__ , __leaf__)) wctomb (char *__s, wchar_t __wchar)
{







  if (__builtin_object_size (__s, 2 > 1) != (size_t) -1 && 16 > __builtin_object_size (__s, 2 > 1))
    return __wctomb_chk (__s, __wchar, __builtin_object_size (__s, 2 > 1));
  return __wctomb_alias (__s, __wchar);
}


extern size_t __mbstowcs_chk (wchar_t *__restrict __dst,
         const char *__restrict __src,
         size_t __len, size_t __dstlen) __attribute__ ((__nothrow__ , __leaf__));
extern size_t __mbstowcs_alias (wchar_t *__restrict __dst, const char *__restrict __src, size_t __len) __asm__ ("" "mbstowcs") __attribute__ ((__nothrow__ , __leaf__))


                                  ;
extern size_t __mbstowcs_chk_warn (wchar_t *__restrict __dst, const char *__restrict __src, size_t __len, size_t __dstlen) __asm__ ("" "__mbstowcs_chk") __attribute__ ((__nothrow__ , __leaf__))



     __attribute__((__warning__ ("mbstowcs called with dst buffer smaller than len " "* sizeof (wchar_t)")))
                        ;

extern __inline __attribute__ ((__always_inline__)) __attribute__ ((__gnu_inline__)) __attribute__ ((__artificial__)) size_t
__attribute__ ((__nothrow__ , __leaf__)) mbstowcs (wchar_t *__restrict __dst, const char *__restrict __src, size_t __len)

{
  if (__builtin_object_size (__dst, 2 > 1) != (size_t) -1)
    {
      if (!__builtin_constant_p (__len))
 return __mbstowcs_chk (__dst, __src, __len,
          __builtin_object_size (__dst, 2 > 1) / sizeof (wchar_t));

      if (__len > __builtin_object_size (__dst, 2 > 1) / sizeof (wchar_t))
 return __mbstowcs_chk_warn (__dst, __src, __len,
         __builtin_object_size (__dst, 2 > 1) / sizeof (wchar_t));
    }
  return __mbstowcs_alias (__dst, __src, __len);
}


extern size_t __wcstombs_chk (char *__restrict __dst,
         const wchar_t *__restrict __src,
         size_t __len, size_t __dstlen) __attribute__ ((__nothrow__ , __leaf__));
extern size_t __wcstombs_alias (char *__restrict __dst, const wchar_t *__restrict __src, size_t __len) __asm__ ("" "wcstombs") __attribute__ ((__nothrow__ , __leaf__))


                                  ;
extern size_t __wcstombs_chk_warn (char *__restrict __dst, const wchar_t *__restrict __src, size_t __len, size_t __dstlen) __asm__ ("" "__wcstombs_chk") __attribute__ ((__nothrow__ , __leaf__))



     __attribute__((__warning__ ("wcstombs called with dst buffer smaller than len")));

extern __inline __attribute__ ((__always_inline__)) __attribute__ ((__gnu_inline__)) __attribute__ ((__artificial__)) size_t
__attribute__ ((__nothrow__ , __leaf__)) wcstombs (char *__restrict __dst, const wchar_t *__restrict __src, size_t __len)

{
  if (__builtin_object_size (__dst, 2 > 1) != (size_t) -1)
    {
      if (!__builtin_constant_p (__len))
 return __wcstombs_chk (__dst, __src, __len, __builtin_object_size (__dst, 2 > 1));
      if (__len > __builtin_object_size (__dst, 2 > 1))
 return __wcstombs_chk_warn (__dst, __src, __len, __builtin_object_size (__dst, 2 > 1));
    }
  return __wcstombs_alias (__dst, __src, __len);
}
# 1018 "/usr/include/stdlib.h" 2 3 4






# 14 "../deps/openssl/openssl/include/internal/constant_time.h" 2
# 1 "/usr/include/string.h" 1 3 4
# 26 "/usr/include/string.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/libc-header-start.h" 1 3 4
# 27 "/usr/include/string.h" 2 3 4






# 1 "/usr/lib/gcc/x86_64-linux-gnu/9/include/stddef.h" 1 3 4
# 34 "/usr/include/string.h" 2 3 4
# 43 "/usr/include/string.h" 3 4
extern void *memcpy (void *__restrict __dest, const void *__restrict __src,
       size_t __n) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));


extern void *memmove (void *__dest, const void *__src, size_t __n)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));





extern void *memccpy (void *__restrict __dest, const void *__restrict __src,
        int __c, size_t __n)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));




extern void *memset (void *__s, int __c, size_t __n) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));


extern int memcmp (const void *__s1, const void *__s2, size_t __n)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1, 2)));
# 91 "/usr/include/string.h" 3 4
extern void *memchr (const void *__s, int __c, size_t __n)
      __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1)));
# 122 "/usr/include/string.h" 3 4
extern char *strcpy (char *__restrict __dest, const char *__restrict __src)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));

extern char *strncpy (char *__restrict __dest,
        const char *__restrict __src, size_t __n)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));


extern char *strcat (char *__restrict __dest, const char *__restrict __src)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));

extern char *strncat (char *__restrict __dest, const char *__restrict __src,
        size_t __n) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));


extern int strcmp (const char *__s1, const char *__s2)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1, 2)));

extern int strncmp (const char *__s1, const char *__s2, size_t __n)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1, 2)));


extern int strcoll (const char *__s1, const char *__s2)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1, 2)));

extern size_t strxfrm (char *__restrict __dest,
         const char *__restrict __src, size_t __n)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2)));



# 1 "/usr/include/x86_64-linux-gnu/bits/types/locale_t.h" 1 3 4
# 22 "/usr/include/x86_64-linux-gnu/bits/types/locale_t.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/types/__locale_t.h" 1 3 4
# 28 "/usr/include/x86_64-linux-gnu/bits/types/__locale_t.h" 3 4
struct __locale_struct
{

  struct __locale_data *__locales[13];


  const unsigned short int *__ctype_b;
  const int *__ctype_tolower;
  const int *__ctype_toupper;


  const char *__names[13];
};

typedef struct __locale_struct *__locale_t;
# 23 "/usr/include/x86_64-linux-gnu/bits/types/locale_t.h" 2 3 4

typedef __locale_t locale_t;
# 154 "/usr/include/string.h" 2 3 4


extern int strcoll_l (const char *__s1, const char *__s2, locale_t __l)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1, 2, 3)));


extern size_t strxfrm_l (char *__dest, const char *__src, size_t __n,
    locale_t __l) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2, 4)));





extern char *strdup (const char *__s)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__malloc__)) __attribute__ ((__nonnull__ (1)));






extern char *strndup (const char *__string, size_t __n)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__malloc__)) __attribute__ ((__nonnull__ (1)));
# 226 "/usr/include/string.h" 3 4
extern char *strchr (const char *__s, int __c)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1)));
# 253 "/usr/include/string.h" 3 4
extern char *strrchr (const char *__s, int __c)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1)));
# 273 "/usr/include/string.h" 3 4
extern size_t strcspn (const char *__s, const char *__reject)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1, 2)));


extern size_t strspn (const char *__s, const char *__accept)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1, 2)));
# 303 "/usr/include/string.h" 3 4
extern char *strpbrk (const char *__s, const char *__accept)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1, 2)));
# 330 "/usr/include/string.h" 3 4
extern char *strstr (const char *__haystack, const char *__needle)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1, 2)));




extern char *strtok (char *__restrict __s, const char *__restrict __delim)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2)));



extern char *__strtok_r (char *__restrict __s,
    const char *__restrict __delim,
    char **__restrict __save_ptr)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2, 3)));

extern char *strtok_r (char *__restrict __s, const char *__restrict __delim,
         char **__restrict __save_ptr)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2, 3)));
# 385 "/usr/include/string.h" 3 4
extern size_t strlen (const char *__s)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1)));




extern size_t strnlen (const char *__string, size_t __maxlen)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1)));




extern char *strerror (int __errnum) __attribute__ ((__nothrow__ , __leaf__));
# 410 "/usr/include/string.h" 3 4
extern int strerror_r (int __errnum, char *__buf, size_t __buflen) __asm__ ("" "__xpg_strerror_r") __attribute__ ((__nothrow__ , __leaf__))

                        __attribute__ ((__nonnull__ (2)));
# 428 "/usr/include/string.h" 3 4
extern char *strerror_l (int __errnum, locale_t __l) __attribute__ ((__nothrow__ , __leaf__));



# 1 "/usr/include/strings.h" 1 3 4
# 23 "/usr/include/strings.h" 3 4
# 1 "/usr/lib/gcc/x86_64-linux-gnu/9/include/stddef.h" 1 3 4
# 24 "/usr/include/strings.h" 2 3 4










extern int bcmp (const void *__s1, const void *__s2, size_t __n)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1, 2)));


extern void bcopy (const void *__src, void *__dest, size_t __n)
  __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));


extern void bzero (void *__s, size_t __n) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
# 68 "/usr/include/strings.h" 3 4
extern char *index (const char *__s, int __c)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1)));
# 96 "/usr/include/strings.h" 3 4
extern char *rindex (const char *__s, int __c)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1)));






extern int ffs (int __i) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__const__));





extern int ffsl (long int __l) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__const__));
__extension__ extern int ffsll (long long int __ll)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__const__));



extern int strcasecmp (const char *__s1, const char *__s2)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1, 2)));


extern int strncasecmp (const char *__s1, const char *__s2, size_t __n)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1, 2)));






extern int strcasecmp_l (const char *__s1, const char *__s2, locale_t __loc)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1, 2, 3)));



extern int strncasecmp_l (const char *__s1, const char *__s2,
     size_t __n, locale_t __loc)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1, 2, 4)));








# 1 "/usr/include/x86_64-linux-gnu/bits/strings_fortified.h" 1 3 4
# 22 "/usr/include/x86_64-linux-gnu/bits/strings_fortified.h" 3 4
extern __inline __attribute__ ((__always_inline__)) __attribute__ ((__gnu_inline__)) __attribute__ ((__artificial__)) void
__attribute__ ((__nothrow__ , __leaf__)) bcopy (const void *__src, void *__dest, size_t __len)
{
  (void) __builtin___memmove_chk (__dest, __src, __len, __builtin_object_size (__dest, 0));
}

extern __inline __attribute__ ((__always_inline__)) __attribute__ ((__gnu_inline__)) __attribute__ ((__artificial__)) void
__attribute__ ((__nothrow__ , __leaf__)) bzero (void *__dest, size_t __len)
{
  (void) __builtin___memset_chk (__dest, '\0', __len, __builtin_object_size (__dest, 0));
}
# 145 "/usr/include/strings.h" 2 3 4
# 433 "/usr/include/string.h" 2 3 4



extern void explicit_bzero (void *__s, size_t __n) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));



extern char *strsep (char **__restrict __stringp,
       const char *__restrict __delim)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));




extern char *strsignal (int __sig) __attribute__ ((__nothrow__ , __leaf__));


extern char *__stpcpy (char *__restrict __dest, const char *__restrict __src)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));
extern char *stpcpy (char *__restrict __dest, const char *__restrict __src)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));



extern char *__stpncpy (char *__restrict __dest,
   const char *__restrict __src, size_t __n)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));
extern char *stpncpy (char *__restrict __dest,
        const char *__restrict __src, size_t __n)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));
# 495 "/usr/include/string.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/string_fortified.h" 1 3 4
# 30 "/usr/include/x86_64-linux-gnu/bits/string_fortified.h" 3 4
extern __inline __attribute__ ((__always_inline__)) __attribute__ ((__gnu_inline__)) __attribute__ ((__artificial__)) void *
__attribute__ ((__nothrow__ , __leaf__)) memcpy (void *__restrict __dest, const void *__restrict __src, size_t __len)

{
  return __builtin___memcpy_chk (__dest, __src, __len, __builtin_object_size (__dest, 0));
}

extern __inline __attribute__ ((__always_inline__)) __attribute__ ((__gnu_inline__)) __attribute__ ((__artificial__)) void *
__attribute__ ((__nothrow__ , __leaf__)) memmove (void *__dest, const void *__src, size_t __len)
{
  return __builtin___memmove_chk (__dest, __src, __len, __builtin_object_size (__dest, 0));
}
# 58 "/usr/include/x86_64-linux-gnu/bits/string_fortified.h" 3 4
extern __inline __attribute__ ((__always_inline__)) __attribute__ ((__gnu_inline__)) __attribute__ ((__artificial__)) void *
__attribute__ ((__nothrow__ , __leaf__)) memset (void *__dest, int __ch, size_t __len)
{
# 71 "/usr/include/x86_64-linux-gnu/bits/string_fortified.h" 3 4
  return __builtin___memset_chk (__dest, __ch, __len, __builtin_object_size (__dest, 0));
}




void __explicit_bzero_chk (void *__dest, size_t __len, size_t __destlen)
  __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));

extern __inline __attribute__ ((__always_inline__)) __attribute__ ((__gnu_inline__)) __attribute__ ((__artificial__)) void
__attribute__ ((__nothrow__ , __leaf__)) explicit_bzero (void *__dest, size_t __len)
{
  __explicit_bzero_chk (__dest, __len, __builtin_object_size (__dest, 0));
}


extern __inline __attribute__ ((__always_inline__)) __attribute__ ((__gnu_inline__)) __attribute__ ((__artificial__)) char *
__attribute__ ((__nothrow__ , __leaf__)) strcpy (char *__restrict __dest, const char *__restrict __src)
{
  return __builtin___strcpy_chk (__dest, __src, __builtin_object_size (__dest, 2 > 1));
}
# 102 "/usr/include/x86_64-linux-gnu/bits/string_fortified.h" 3 4
extern __inline __attribute__ ((__always_inline__)) __attribute__ ((__gnu_inline__)) __attribute__ ((__artificial__)) char *
__attribute__ ((__nothrow__ , __leaf__)) strncpy (char *__restrict __dest, const char *__restrict __src, size_t __len)

{
  return __builtin___strncpy_chk (__dest, __src, __len, __builtin_object_size (__dest, 2 > 1));
}


extern char *__stpncpy_chk (char *__dest, const char *__src, size_t __n,
       size_t __destlen) __attribute__ ((__nothrow__ , __leaf__));
extern char *__stpncpy_alias (char *__dest, const char *__src, size_t __n) __asm__ ("" "stpncpy") __attribute__ ((__nothrow__ , __leaf__))
                                 ;

extern __inline __attribute__ ((__always_inline__)) __attribute__ ((__gnu_inline__)) __attribute__ ((__artificial__)) char *
__attribute__ ((__nothrow__ , __leaf__)) stpncpy (char *__dest, const char *__src, size_t __n)
{
  if (__builtin_object_size (__dest, 2 > 1) != (size_t) -1
      && (!__builtin_constant_p (__n) || __n > __builtin_object_size (__dest, 2 > 1)))
    return __stpncpy_chk (__dest, __src, __n, __builtin_object_size (__dest, 2 > 1));
  return __stpncpy_alias (__dest, __src, __n);
}


extern __inline __attribute__ ((__always_inline__)) __attribute__ ((__gnu_inline__)) __attribute__ ((__artificial__)) char *
__attribute__ ((__nothrow__ , __leaf__)) strcat (char *__restrict __dest, const char *__restrict __src)
{
  return __builtin___strcat_chk (__dest, __src, __builtin_object_size (__dest, 2 > 1));
}


extern __inline __attribute__ ((__always_inline__)) __attribute__ ((__gnu_inline__)) __attribute__ ((__artificial__)) char *
__attribute__ ((__nothrow__ , __leaf__)) strncat (char *__restrict __dest, const char *__restrict __src, size_t __len)

{
  return __builtin___strncat_chk (__dest, __src, __len, __builtin_object_size (__dest, 2 > 1));
}
# 496 "/usr/include/string.h" 2 3 4




# 15 "../deps/openssl/openssl/include/internal/constant_time.h" 2
# 1 "../deps/openssl/openssl/include/openssl/e_os2.h" 1
# 13 "../deps/openssl/openssl/include/openssl/e_os2.h"
# 1 "../deps/openssl/openssl/include/openssl/opensslconf.h" 1
# 1 "../deps/openssl/openssl/include/../../config/opensslconf.h" 1
# 9 "../deps/openssl/openssl/include/../../config/opensslconf.h"
# 1 "../deps/openssl/openssl/include/../../config/./opensslconf_asm.h" 1
# 98 "../deps/openssl/openssl/include/../../config/./opensslconf_asm.h"
# 1 "../deps/openssl/openssl/include/../../config/././archs/linux-x86_64/asm/include/openssl/opensslconf.h" 1
# 13 "../deps/openssl/openssl/include/../../config/././archs/linux-x86_64/asm/include/openssl/opensslconf.h"
# 1 "../deps/openssl/openssl/include/openssl/opensslv.h" 1
# 14 "../deps/openssl/openssl/include/../../config/././archs/linux-x86_64/asm/include/openssl/opensslconf.h" 2
# 99 "../deps/openssl/openssl/include/../../config/./opensslconf_asm.h" 2
# 10 "../deps/openssl/openssl/include/../../config/opensslconf.h" 2
# 1 "../deps/openssl/openssl/include/openssl/opensslconf.h" 2
# 14 "../deps/openssl/openssl/include/openssl/e_os2.h" 2
# 243 "../deps/openssl/openssl/include/openssl/e_os2.h"
# 1 "/usr/include/inttypes.h" 1 3 4
# 27 "/usr/include/inttypes.h" 3 4
# 1 "/usr/lib/gcc/x86_64-linux-gnu/9/include/stdint.h" 1 3 4
# 9 "/usr/lib/gcc/x86_64-linux-gnu/9/include/stdint.h" 3 4
# 1 "/usr/include/stdint.h" 1 3 4
# 26 "/usr/include/stdint.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/libc-header-start.h" 1 3 4
# 27 "/usr/include/stdint.h" 2 3 4

# 1 "/usr/include/x86_64-linux-gnu/bits/wchar.h" 1 3 4
# 29 "/usr/include/stdint.h" 2 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/wordsize.h" 1 3 4
# 30 "/usr/include/stdint.h" 2 3 4







# 1 "/usr/include/x86_64-linux-gnu/bits/stdint-uintn.h" 1 3 4
# 24 "/usr/include/x86_64-linux-gnu/bits/stdint-uintn.h" 3 4
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
# 38 "/usr/include/stdint.h" 2 3 4





typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;


typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;





typedef signed char int_fast8_t;

typedef long int int_fast16_t;
typedef long int int_fast32_t;
typedef long int int_fast64_t;
# 71 "/usr/include/stdint.h" 3 4
typedef unsigned char uint_fast8_t;

typedef unsigned long int uint_fast16_t;
typedef unsigned long int uint_fast32_t;
typedef unsigned long int uint_fast64_t;
# 87 "/usr/include/stdint.h" 3 4
typedef long int intptr_t;


typedef unsigned long int uintptr_t;
# 101 "/usr/include/stdint.h" 3 4
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
# 10 "/usr/lib/gcc/x86_64-linux-gnu/9/include/stdint.h" 2 3 4
# 28 "/usr/include/inttypes.h" 2 3 4






typedef int __gwchar_t;
# 266 "/usr/include/inttypes.h" 3 4





typedef struct
  {
    long int quot;
    long int rem;
  } imaxdiv_t;
# 290 "/usr/include/inttypes.h" 3 4
extern intmax_t imaxabs (intmax_t __n) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__const__));


extern imaxdiv_t imaxdiv (intmax_t __numer, intmax_t __denom)
      __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__const__));


extern intmax_t strtoimax (const char *__restrict __nptr,
      char **__restrict __endptr, int __base) __attribute__ ((__nothrow__ , __leaf__));


extern uintmax_t strtoumax (const char *__restrict __nptr,
       char ** __restrict __endptr, int __base) __attribute__ ((__nothrow__ , __leaf__));


extern intmax_t wcstoimax (const __gwchar_t *__restrict __nptr,
      __gwchar_t **__restrict __endptr, int __base)
     __attribute__ ((__nothrow__ , __leaf__));


extern uintmax_t wcstoumax (const __gwchar_t *__restrict __nptr,
       __gwchar_t ** __restrict __endptr, int __base)
     __attribute__ ((__nothrow__ , __leaf__));





extern long int __strtol_internal (const char *__restrict __nptr,
       char **__restrict __endptr,
       int __base, int __group)
  __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1))) __attribute__ ((__warn_unused_result__));

extern __inline __attribute__ ((__gnu_inline__)) intmax_t
__attribute__ ((__nothrow__ , __leaf__)) strtoimax (const char *__restrict nptr, char **__restrict endptr, int base)

{
  return __strtol_internal (nptr, endptr, base, 0);
}

extern unsigned long int __strtoul_internal (const char *__restrict __nptr,
          char ** __restrict __endptr,
          int __base, int __group)
  __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1))) __attribute__ ((__warn_unused_result__));

extern __inline __attribute__ ((__gnu_inline__)) uintmax_t
__attribute__ ((__nothrow__ , __leaf__)) strtoumax (const char *__restrict nptr, char **__restrict endptr, int base)

{
  return __strtoul_internal (nptr, endptr, base, 0);
}

extern long int __wcstol_internal (const __gwchar_t * __restrict __nptr,
       __gwchar_t **__restrict __endptr,
       int __base, int __group)
  __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1))) __attribute__ ((__warn_unused_result__));

extern __inline __attribute__ ((__gnu_inline__)) intmax_t
__attribute__ ((__nothrow__ , __leaf__)) wcstoimax (const __gwchar_t *__restrict nptr, __gwchar_t **__restrict endptr, int base)

{
  return __wcstol_internal (nptr, endptr, base, 0);
}

extern unsigned long int __wcstoul_internal (const __gwchar_t *
          __restrict __nptr,
          __gwchar_t **
          __restrict __endptr,
          int __base, int __group)
  __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1))) __attribute__ ((__warn_unused_result__));

extern __inline __attribute__ ((__gnu_inline__)) uintmax_t
__attribute__ ((__nothrow__ , __leaf__)) wcstoumax (const __gwchar_t *__restrict nptr, __gwchar_t **__restrict endptr, int base)

{
  return __wcstoul_internal (nptr, endptr, base, 0);
}
# 432 "/usr/include/inttypes.h" 3 4

# 244 "../deps/openssl/openssl/include/openssl/e_os2.h" 2
# 16 "../deps/openssl/openssl/include/internal/constant_time.h" 2
# 32 "../deps/openssl/openssl/include/internal/constant_time.h"

# 32 "../deps/openssl/openssl/include/internal/constant_time.h"
static inline unsigned int constant_time_msb(unsigned int a);

static inline uint32_t constant_time_msb_32(uint32_t a);

static inline uint64_t constant_time_msb_64(uint64_t a);


static inline unsigned int constant_time_lt(unsigned int a,
                                                 unsigned int b);

static inline unsigned char constant_time_lt_8(unsigned int a,
                                                    unsigned int b);

static inline uint64_t constant_time_lt_64(uint64_t a, uint64_t b);


static inline unsigned int constant_time_ge(unsigned int a,
                                                 unsigned int b);

static inline unsigned char constant_time_ge_8(unsigned int a,
                                                    unsigned int b);


static inline unsigned int constant_time_is_zero(unsigned int a);

static inline unsigned char constant_time_is_zero_8(unsigned int a);

static inline uint32_t constant_time_is_zero_32(uint32_t a);


static inline unsigned int constant_time_eq(unsigned int a,
                                                 unsigned int b);

static inline unsigned char constant_time_eq_8(unsigned int a,
                                                    unsigned int b);

static inline unsigned int constant_time_eq_int(int a, int b);

static inline unsigned char constant_time_eq_int_8(int a, int b);
# 79 "../deps/openssl/openssl/include/internal/constant_time.h"
static inline unsigned int constant_time_select(unsigned int mask,
                                                     unsigned int a,
                                                     unsigned int b);

static inline unsigned char constant_time_select_8(unsigned char mask,
                                                        unsigned char a,
                                                        unsigned char b);


static inline uint32_t constant_time_select_32(uint32_t mask, uint32_t a,
                                                    uint32_t b);


static inline uint64_t constant_time_select_64(uint64_t mask, uint64_t a,
                                                    uint64_t b);

static inline int constant_time_select_int(unsigned int mask, int a,
                                                int b);


static inline unsigned int constant_time_msb(unsigned int a)
{
    return 0 - (a >> (sizeof(a) * 8 - 1));
}


static inline uint32_t constant_time_msb_32(uint32_t a)
{
    return 0 - (a >> 31);
}

static inline uint64_t constant_time_msb_64(uint64_t a)
{
    return 0 - (a >> 63);
}

static inline size_t constant_time_msb_s(size_t a)
{
    return 0 - (a >> (sizeof(a) * 8 - 1));
}

static inline unsigned int constant_time_lt(unsigned int a,
                                                 unsigned int b)
{
    return constant_time_msb(a ^ ((a ^ b) | ((a - b) ^ b)));
}

static inline size_t constant_time_lt_s(size_t a, size_t b)
{
    return constant_time_msb_s(a ^ ((a ^ b) | ((a - b) ^ b)));
}

static inline unsigned char constant_time_lt_8(unsigned int a,
                                                    unsigned int b)
{
    return (unsigned char)constant_time_lt(a, b);
}

static inline uint64_t constant_time_lt_64(uint64_t a, uint64_t b)
{
    return constant_time_msb_64(a ^ ((a ^ b) | ((a - b) ^ b)));
}

static inline unsigned int constant_time_ge(unsigned int a,
                                                 unsigned int b)
{
    return ~constant_time_lt(a, b);
}

static inline size_t constant_time_ge_s(size_t a, size_t b)
{
    return ~constant_time_lt_s(a, b);
}

static inline unsigned char constant_time_ge_8(unsigned int a,
                                                    unsigned int b)
{
    return (unsigned char)constant_time_ge(a, b);
}

static inline unsigned char constant_time_ge_8_s(size_t a, size_t b)
{
    return (unsigned char)constant_time_ge_s(a, b);
}

static inline unsigned int constant_time_is_zero(unsigned int a)
{
    return constant_time_msb(~a & (a - 1));
}

static inline size_t constant_time_is_zero_s(size_t a)
{
    return constant_time_msb_s(~a & (a - 1));
}

static inline unsigned char constant_time_is_zero_8(unsigned int a)
{
    return (unsigned char)constant_time_is_zero(a);
}

static inline uint32_t constant_time_is_zero_32(uint32_t a)
{
    return constant_time_msb_32(~a & (a - 1));
}

static inline unsigned int constant_time_eq(unsigned int a,
                                                 unsigned int b)
{
    return constant_time_is_zero(a ^ b);
}

static inline size_t constant_time_eq_s(size_t a, size_t b)
{
    return constant_time_is_zero_s(a ^ b);
}

static inline unsigned char constant_time_eq_8(unsigned int a,
                                                    unsigned int b)
{
    return (unsigned char)constant_time_eq(a, b);
}

static inline unsigned char constant_time_eq_8_s(size_t a, size_t b)
{
    return (unsigned char)constant_time_eq_s(a, b);
}

static inline unsigned int constant_time_eq_int(int a, int b)
{
    return constant_time_eq((unsigned)(a), (unsigned)(b));
}

static inline unsigned char constant_time_eq_int_8(int a, int b)
{
    return constant_time_eq_8((unsigned)(a), (unsigned)(b));
}
# 223 "../deps/openssl/openssl/include/internal/constant_time.h"
static inline unsigned int value_barrier(unsigned int a)
{

    unsigned int r;
    __asm__("" : "=r"(r) : "0"(a));



    return r;
}


static inline uint32_t value_barrier_32(uint32_t a)
{

    uint32_t r;
    __asm__("" : "=r"(r) : "0"(a));



    return r;
}


static inline uint64_t value_barrier_64(uint64_t a)
{

    uint64_t r;
    __asm__("" : "=r"(r) : "0"(a));



    return r;
}


static inline size_t value_barrier_s(size_t a)
{

    size_t r;
    __asm__("" : "=r"(r) : "0"(a));



    return r;
}

static inline unsigned int constant_time_select(unsigned int mask,
                                                     unsigned int a,
                                                     unsigned int b)
{
    return (value_barrier(mask) & a) | (value_barrier(~mask) & b);
}

static inline size_t constant_time_select_s(size_t mask,
                                                 size_t a,
                                                 size_t b)
{
    return (value_barrier_s(mask) & a) | (value_barrier_s(~mask) & b);
}

static inline unsigned char constant_time_select_8(unsigned char mask,
                                                        unsigned char a,
                                                        unsigned char b)
{
    return (unsigned char)constant_time_select(mask, a, b);
}

static inline int constant_time_select_int(unsigned int mask, int a,
                                                int b)
{
    return (int)constant_time_select(mask, (unsigned)(a), (unsigned)(b));
}

static inline int constant_time_select_int_s(size_t mask, int a, int b)
{
    return (int)constant_time_select((unsigned)mask, (unsigned)(a),
                                      (unsigned)(b));
}

static inline uint32_t constant_time_select_32(uint32_t mask, uint32_t a,
                                                    uint32_t b)
{
    return (value_barrier_32(mask) & a) | (value_barrier_32(~mask) & b);
}

static inline uint64_t constant_time_select_64(uint64_t mask, uint64_t a,
                                                    uint64_t b)
{
    return (value_barrier_64(mask) & a) | (value_barrier_64(~mask) & b);
}
# 325 "../deps/openssl/openssl/include/internal/constant_time.h"
static inline void constant_time_cond_swap_32(uint32_t mask, uint32_t *a,
                                                   uint32_t *b)
{
    uint32_t xor = *a ^ *b;

    xor &= mask;
    *a ^= xor;
    *b ^= xor;
}
# 345 "../deps/openssl/openssl/include/internal/constant_time.h"
static inline void constant_time_cond_swap_64(uint64_t mask, uint64_t *a,
                                                   uint64_t *b)
{
    uint64_t xor = *a ^ *b;

    xor &= mask;
    *a ^= xor;
    *b ^= xor;
}






static inline void constant_time_lookup(void *out,
                                             const void *table,
                                             size_t rowsize,
                                             size_t numrows,
                                             size_t idx)
{
    size_t i, j;
    const unsigned char *tablec = (const unsigned char *)table;
    unsigned char *outc = (unsigned char *)out;
    unsigned char mask;

    memset(out, 0, rowsize);


    for (i = 0; i < numrows; i++, idx--) {
        mask = (unsigned char)constant_time_is_zero_s(idx);
        for (j = 0; j < rowsize; j++)
            *(outc + j) |= constant_time_select_8(mask, *(tablec++), 0);
    }
}





void err_clear_last_constant_time(int clear);
# 17 "../deps/openssl/openssl/crypto/ec/curve448/field.h" 2

# 1 "/usr/include/assert.h" 1 3 4
# 19 "../deps/openssl/openssl/crypto/ec/curve448/field.h" 2
# 1 "../deps/openssl/openssl/crypto/ec/curve448/word.h" 1
# 17 "../deps/openssl/openssl/crypto/ec/curve448/word.h"
# 1 "/usr/include/assert.h" 1 3 4
# 18 "../deps/openssl/openssl/crypto/ec/curve448/word.h" 2


# 1 "../deps/openssl/openssl/crypto/ec/curve448/arch_32/arch_intrinsics.h" 1
# 22 "../deps/openssl/openssl/crypto/ec/curve448/arch_32/arch_intrinsics.h"
static inline uint64_t widemul(uint32_t a, uint32_t b)
{
    return ((uint64_t)a) * b;
}
# 21 "../deps/openssl/openssl/crypto/ec/curve448/word.h" 2
# 1 "../deps/openssl/openssl/crypto/ec/curve448/curve448utils.h" 1
# 38 "../deps/openssl/openssl/crypto/ec/curve448/curve448utils.h"
typedef uint64_t c448_word_t;

typedef int64_t c448_sword_t;

typedef uint64_t c448_bool_t;

typedef __uint128_t c448_dword_t;

typedef __int128_t c448_dsword_t;
# 69 "../deps/openssl/openssl/crypto/ec/curve448/curve448utils.h"
typedef enum {
    C448_SUCCESS = -1,
    C448_FAILURE = 0
} c448_error_t;


static inline c448_error_t c448_succeed_if(c448_bool_t x)
{
    return (c448_error_t) x;
}
# 22 "../deps/openssl/openssl/crypto/ec/curve448/word.h" 2
# 30 "../deps/openssl/openssl/crypto/ec/curve448/word.h"
typedef uint32_t word_t, mask_t;
typedef uint64_t dword_t;
typedef int16_t hsword_t;
typedef int32_t sword_t;
typedef int64_t dsword_t;
# 61 "../deps/openssl/openssl/crypto/ec/curve448/word.h"
static inline c448_bool_t mask_to_bool(mask_t m)
{
    return (c448_sword_t)(sword_t)m;
}

static inline mask_t bool_to_mask(c448_bool_t m)
{

    mask_t ret = 0;
    unsigned int i;
    unsigned int limit = sizeof(c448_bool_t) / sizeof(mask_t);

    if (limit < 1)
        limit = 1;
    for (i = 0; i < limit; i++)
        ret |= ~constant_time_is_zero_32(m >> (i * 8 * sizeof(word_t)));

    return ret;
}
# 20 "../deps/openssl/openssl/crypto/ec/curve448/field.h" 2
# 35 "../deps/openssl/openssl/crypto/ec/curve448/field.h"
typedef struct gf_s {
    word_t limb[(64/sizeof(word_t))];
} __attribute__((__aligned__(16))) gf_s, gf[1];






static __inline__ __attribute__((__unused__,__always_inline__)) void gf_copy(gf out, const gf a)
{
    *out = *a;
}

static __inline__ __attribute__((__unused__,__always_inline__)) void gf_add_RAW(gf out, const gf a, const gf b);
static __inline__ __attribute__((__unused__,__always_inline__)) void gf_sub_RAW(gf out, const gf a, const gf b);
static __inline__ __attribute__((__unused__,__always_inline__)) void gf_bias(gf inout, int amount);
static __inline__ __attribute__((__unused__,__always_inline__)) void gf_weak_reduce(gf inout);

void gf_strong_reduce(gf inout);
void gf_add(gf out, const gf a, const gf b);
void gf_sub(gf out, const gf a, const gf b);
void gf_mul(gf_s * __restrict__ out, const gf a, const gf b);
void gf_mulw_unsigned(gf_s * __restrict__ out, const gf a, uint32_t b);
void gf_sqr(gf_s * __restrict__ out, const gf a);
mask_t gf_isr(gf a, const gf x);
mask_t gf_eq(const gf x, const gf y);
mask_t gf_lobit(const gf x);
mask_t gf_hibit(const gf x);

void gf_serialize(uint8_t *serial, const gf x, int with_highbit);
mask_t gf_deserialize(gf x, const uint8_t serial[56], int with_hibit,
                      uint8_t hi_nmask);

# 1 "../deps/openssl/openssl/crypto/ec/curve448/arch_32/f_impl.h" 1
# 23 "../deps/openssl/openssl/crypto/ec/curve448/arch_32/f_impl.h"
void gf_add_RAW(gf out, const gf a, const gf b)
{
    unsigned int i;

    for (i = 0; i < (64/sizeof(word_t)); i++)
        out->limb[i] = a->limb[i] + b->limb[i];
}

void gf_sub_RAW(gf out, const gf a, const gf b)
{
    unsigned int i;

    for (i = 0; i < (64/sizeof(word_t)); i++)
        out->limb[i] = a->limb[i] - b->limb[i];
}

void gf_bias(gf a, int amt)
{
    unsigned int i;
    uint32_t co1 = ((1 << 28) - 1) * amt, co2 = co1 - amt;

    for (i = 0; i < (64/sizeof(word_t)); i++)
        a->limb[i] += (i == (64/sizeof(word_t)) / 2) ? co2 : co1;
}

void gf_weak_reduce(gf a)
{
    uint32_t mask = (1 << 28) - 1;
    uint32_t tmp = a->limb[(64/sizeof(word_t)) - 1] >> 28;
    unsigned int i;

    a->limb[(64/sizeof(word_t)) / 2] += tmp;
    for (i = (64/sizeof(word_t)) - 1; i > 0; i--)
        a->limb[i] = (a->limb[i] & mask) + (a->limb[i - 1] >> 28);
    a->limb[0] = (a->limb[0] & mask) + tmp;
}
# 70 "../deps/openssl/openssl/crypto/ec/curve448/field.h" 2




static const gf ZERO = {{{0}}}, ONE = {{{1}}};


static inline void gf_sqrn(gf_s * __restrict__ y, const gf x, int n)
{
    gf tmp;

    
# 81 "../deps/openssl/openssl/crypto/ec/curve448/field.h" 3 4
   ((void) (0))
# 81 "../deps/openssl/openssl/crypto/ec/curve448/field.h"
                ;
    if (n & 1) {
        gf_sqr(y, x);
        n--;
    } else {
        gf_sqr(tmp, x);
        gf_sqr(y, tmp);
        n -= 2;
    }
    for (; n; n -= 2) {
        gf_sqr(tmp, y);
        gf_sqr(y, tmp);
    }
}




static inline void gf_sub_nr(gf c, const gf a, const gf b)
{
    gf_sub_RAW(c, a, b);
    gf_bias(c, 2);
    if (2 < 3)
        gf_weak_reduce(c);
}


static inline void gf_subx_nr(gf c, const gf a, const gf b, int amt)
{
    gf_sub_RAW(c, a, b);
    gf_bias(c, amt);
    if (2 < amt + 1)
        gf_weak_reduce(c);
}


static inline void gf_mulw(gf c, const gf a, int32_t w)
{
    if (w > 0) {
        gf_mulw_unsigned(c, a, w);
    } else {
        gf_mulw_unsigned(c, a, -w);
        gf_sub(c, ZERO, c);
    }
}


static inline void gf_cond_sel(gf x, const gf y, const gf z, mask_t is_z)
{
    size_t i;

    for (i = 0; i < (64/sizeof(word_t)); i++) {

        x[0].limb[i] = constant_time_select_32(is_z, z[0].limb[i],
                                               y[0].limb[i]);





    }
}


static inline void gf_cond_neg(gf x, mask_t neg)
{
    gf y;

    gf_sub(y, ZERO, x);
    gf_cond_sel(x, x, y, neg);
}


static inline void gf_cond_swap(gf x, gf_s * __restrict__ y, mask_t swap)
{
    size_t i;

    for (i = 0; i < (64/sizeof(word_t)); i++) {

        constant_time_cond_swap_32(swap, &(x[0].limb[i]), &(y->limb[i]));




    }
}
# 13 "../deps/openssl/openssl/crypto/ec/curve448/curve448_tables.c" 2

# 1 "../deps/openssl/openssl/crypto/ec/curve448/point_448.h" 1
# 25 "../deps/openssl/openssl/crypto/ec/curve448/point_448.h"
typedef struct {
    gf a, b, c;
} niels_s, niels_t[1];
typedef struct {
    niels_t n;
    gf z;
} pniels_t[1];


struct curve448_precomputed_s {
    niels_t table[5 << (5 - 1)];
};
# 56 "../deps/openssl/openssl/crypto/ec/curve448/point_448.h"
typedef struct curve448_point_s {
    gf x, y, z, t;
} curve448_point_t[1];


struct curve448_precomputed_s;


typedef struct curve448_precomputed_s curve448_precomputed_s;


typedef struct curve448_scalar_s {
    c448_word_t limb[((446-1)/64 +1)];
} curve448_scalar_t[1];


extern const curve448_scalar_t curve448_scalar_one;


extern const curve448_scalar_t curve448_scalar_zero;


extern const curve448_point_t curve448_point_identity;


extern const struct curve448_precomputed_s *curve448_precomputed_base;
extern const niels_t *curve448_wnaf_base;
# 95 "../deps/openssl/openssl/crypto/ec/curve448/point_448.h"
c448_error_t curve448_scalar_decode(curve448_scalar_t out,
                                    const unsigned char ser[56]);
# 105 "../deps/openssl/openssl/crypto/ec/curve448/point_448.h"
void curve448_scalar_decode_long(curve448_scalar_t out,
                                 const unsigned char *ser, size_t ser_len);







void curve448_scalar_encode(unsigned char ser[56],
                            const curve448_scalar_t s);
# 124 "../deps/openssl/openssl/crypto/ec/curve448/point_448.h"
void curve448_scalar_add(curve448_scalar_t out,
                         const curve448_scalar_t a, const curve448_scalar_t b);







void curve448_scalar_sub(curve448_scalar_t out,
                         const curve448_scalar_t a, const curve448_scalar_t b);
# 143 "../deps/openssl/openssl/crypto/ec/curve448/point_448.h"
void curve448_scalar_mul(curve448_scalar_t out,
                         const curve448_scalar_t a, const curve448_scalar_t b);







void curve448_scalar_halve(curve448_scalar_t out, const curve448_scalar_t a);
# 161 "../deps/openssl/openssl/crypto/ec/curve448/point_448.h"
static inline void curve448_scalar_copy(curve448_scalar_t out,
                                             const curve448_scalar_t a)
{
    *out = *a;
}
# 174 "../deps/openssl/openssl/crypto/ec/curve448/point_448.h"
static inline void curve448_point_copy(curve448_point_t a,
                                            const curve448_point_t b)
{
    *a = *b;
}
# 191 "../deps/openssl/openssl/crypto/ec/curve448/point_448.h"
 c448_bool_t curve448_point_eq(const curve448_point_t a,
                                     const curve448_point_t b);
# 201 "../deps/openssl/openssl/crypto/ec/curve448/point_448.h"
void curve448_point_double(curve448_point_t two_a, const curve448_point_t a);
# 216 "../deps/openssl/openssl/crypto/ec/curve448/point_448.h"
 c448_error_t x448_int(uint8_t out[56],
                             const uint8_t base[56],
                             const uint8_t scalar[56]);
# 239 "../deps/openssl/openssl/crypto/ec/curve448/point_448.h"
void curve448_point_mul_by_ratio_and_encode_like_x448(
                                        uint8_t out[56],
                                        const curve448_point_t p);
# 250 "../deps/openssl/openssl/crypto/ec/curve448/point_448.h"
void x448_derive_public_key(uint8_t out[56],
                            const uint8_t scalar[56]);
# 260 "../deps/openssl/openssl/crypto/ec/curve448/point_448.h"
void curve448_precomputed_scalarmul(curve448_point_t scaled,
                                    const curve448_precomputed_s * base,
                                    const curve448_scalar_t scalar);
# 279 "../deps/openssl/openssl/crypto/ec/curve448/point_448.h"
void curve448_base_double_scalarmul_non_secret(curve448_point_t combo,
                                               const curve448_scalar_t scalar1,
                                               const curve448_point_t base2,
                                               const curve448_scalar_t scalar2);
# 293 "../deps/openssl/openssl/crypto/ec/curve448/point_448.h"
 c448_bool_t curve448_point_valid(const curve448_point_t to_test);


void curve448_scalar_destroy(curve448_scalar_t scalar);


void curve448_point_destroy(curve448_point_t point);
# 15 "../deps/openssl/openssl/crypto/ec/curve448/curve448_tables.c" 2

static const curve448_precomputed_s curve448_precomputed_base_table = {
    {
        {{
            {{{((0x00cc3b062366f4ccULL) & ((1 << 28) - 1)), ((0x00cc3b062366f4ccULL) >> 28), ((0x003d6e34e314aa3cULL) & ((1 << 28) - 1)), ((0x003d6e34e314aa3cULL) >> 28), ((0x00d51c0a7521774dULL) & ((1 << 28) - 1)), ((0x00d51c0a7521774dULL) >> 28), ((0x0094e060eec6ab8bULL) & ((1 << 28) - 1)), ((0x0094e060eec6ab8bULL) >> 28), ((0x00d21291b4d80082ULL) & ((1 << 28) - 1)), ((0x00d21291b4d80082ULL) >> 28), ((0x00befed12b55ef1eULL) & ((1 << 28) - 1)), ((0x00befed12b55ef1eULL) >> 28), ((0x00c3dd2df5c94518ULL) & ((1 << 28) - 1)), ((0x00c3dd2df5c94518ULL) >> 28), ((0x00e0a7b112b8d4e6ULL) & ((1 << 28) - 1)), ((0x00e0a7b112b8d4e6ULL) >> 28)}}


                                                                        },
            {{{((0x0019eb5608d8723aULL) & ((1 << 28) - 1)), ((0x0019eb5608d8723aULL) >> 28), ((0x00d1bab52fb3aedbULL) & ((1 << 28) - 1)), ((0x00d1bab52fb3aedbULL) >> 28), ((0x00270a7311ebc90cULL) & ((1 << 28) - 1)), ((0x00270a7311ebc90cULL) >> 28), ((0x0037c12b91be7f13ULL) & ((1 << 28) - 1)), ((0x0037c12b91be7f13ULL) >> 28), ((0x005be16cd8b5c704ULL) & ((1 << 28) - 1)), ((0x005be16cd8b5c704ULL) >> 28), ((0x003e181acda888e1ULL) & ((1 << 28) - 1)), ((0x003e181acda888e1ULL) >> 28), ((0x00bc1f00fc3fc6d0ULL) & ((1 << 28) - 1)), ((0x00bc1f00fc3fc6d0ULL) >> 28), ((0x00d3839bfa319e20ULL) & ((1 << 28) - 1)), ((0x00d3839bfa319e20ULL) >> 28)}}


                                                                        },
            {{{((0x003caeb88611909fULL) & ((1 << 28) - 1)), ((0x003caeb88611909fULL) >> 28), ((0x00ea8b378c4df3d4ULL) & ((1 << 28) - 1)), ((0x00ea8b378c4df3d4ULL) >> 28), ((0x00b3295b95a5a19aULL) & ((1 << 28) - 1)), ((0x00b3295b95a5a19aULL) >> 28), ((0x00a65f97514bdfb5ULL) & ((1 << 28) - 1)), ((0x00a65f97514bdfb5ULL) >> 28), ((0x00b39efba743cab1ULL) & ((1 << 28) - 1)), ((0x00b39efba743cab1ULL) >> 28), ((0x0016ba98b862fd2dULL) & ((1 << 28) - 1)), ((0x0016ba98b862fd2dULL) >> 28), ((0x0001508812ee71d7ULL) & ((1 << 28) - 1)), ((0x0001508812ee71d7ULL) >> 28), ((0x000a75740eea114aULL) & ((1 << 28) - 1)), ((0x000a75740eea114aULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x00ebcf0eb649f823ULL) & ((1 << 28) - 1)), ((0x00ebcf0eb649f823ULL) >> 28), ((0x00166d332e98ea03ULL) & ((1 << 28) - 1)), ((0x00166d332e98ea03ULL) >> 28), ((0x0059ddf64f5cd5f6ULL) & ((1 << 28) - 1)), ((0x0059ddf64f5cd5f6ULL) >> 28), ((0x0047763123d9471bULL) & ((1 << 28) - 1)), ((0x0047763123d9471bULL) >> 28), ((0x00a64065c53ef62fULL) & ((1 << 28) - 1)), ((0x00a64065c53ef62fULL) >> 28), ((0x00978e44c480153dULL) & ((1 << 28) - 1)), ((0x00978e44c480153dULL) >> 28), ((0x000b5b2a0265f194ULL) & ((1 << 28) - 1)), ((0x000b5b2a0265f194ULL) >> 28), ((0x0046a24b9f32965aULL) & ((1 << 28) - 1)), ((0x0046a24b9f32965aULL) >> 28)}}


                                                                        },
            {{{((0x00b9eef787034df0ULL) & ((1 << 28) - 1)), ((0x00b9eef787034df0ULL) >> 28), ((0x0020bc24de3390cdULL) & ((1 << 28) - 1)), ((0x0020bc24de3390cdULL) >> 28), ((0x000022160bae99bbULL) & ((1 << 28) - 1)), ((0x000022160bae99bbULL) >> 28), ((0x00ae66e886e97946ULL) & ((1 << 28) - 1)), ((0x00ae66e886e97946ULL) >> 28), ((0x0048d4bbe02cbb8bULL) & ((1 << 28) - 1)), ((0x0048d4bbe02cbb8bULL) >> 28), ((0x0072ba97b34e38d4ULL) & ((1 << 28) - 1)), ((0x0072ba97b34e38d4ULL) >> 28), ((0x00eae7ec8f03e85aULL) & ((1 << 28) - 1)), ((0x00eae7ec8f03e85aULL) >> 28), ((0x005ba92ecf808b2cULL) & ((1 << 28) - 1)), ((0x005ba92ecf808b2cULL) >> 28)}}


                                                                        },
            {{{((0x00c9cfbbe74258fdULL) & ((1 << 28) - 1)), ((0x00c9cfbbe74258fdULL) >> 28), ((0x00843a979ea9eaa7ULL) & ((1 << 28) - 1)), ((0x00843a979ea9eaa7ULL) >> 28), ((0x000cbb4371cfbe90ULL) & ((1 << 28) - 1)), ((0x000cbb4371cfbe90ULL) >> 28), ((0x0059bac8f7f0a628ULL) & ((1 << 28) - 1)), ((0x0059bac8f7f0a628ULL) >> 28), ((0x004b3dff882ff530ULL) & ((1 << 28) - 1)), ((0x004b3dff882ff530ULL) >> 28), ((0x0011869df4d90733ULL) & ((1 << 28) - 1)), ((0x0011869df4d90733ULL) >> 28), ((0x00595aa71f4abfc2ULL) & ((1 << 28) - 1)), ((0x00595aa71f4abfc2ULL) >> 28), ((0x0070e2d38990c2e6ULL) & ((1 << 28) - 1)), ((0x0070e2d38990c2e6ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x00de2010c0a01733ULL) & ((1 << 28) - 1)), ((0x00de2010c0a01733ULL) >> 28), ((0x00c739a612e24297ULL) & ((1 << 28) - 1)), ((0x00c739a612e24297ULL) >> 28), ((0x00a7212643141d7cULL) & ((1 << 28) - 1)), ((0x00a7212643141d7cULL) >> 28), ((0x00f88444f6b67c11ULL) & ((1 << 28) - 1)), ((0x00f88444f6b67c11ULL) >> 28), ((0x00484b7b16ec28f2ULL) & ((1 << 28) - 1)), ((0x00484b7b16ec28f2ULL) >> 28), ((0x009c1b8856af9c68ULL) & ((1 << 28) - 1)), ((0x009c1b8856af9c68ULL) >> 28), ((0x00ff4669591fe9d6ULL) & ((1 << 28) - 1)), ((0x00ff4669591fe9d6ULL) >> 28), ((0x0054974be08a32c8ULL) & ((1 << 28) - 1)), ((0x0054974be08a32c8ULL) >> 28)}}


                                                                        },
            {{{((0x0010de3fd682ceedULL) & ((1 << 28) - 1)), ((0x0010de3fd682ceedULL) >> 28), ((0x008c07642d83ca4eULL) & ((1 << 28) - 1)), ((0x008c07642d83ca4eULL) >> 28), ((0x0013bb064e00a1ccULL) & ((1 << 28) - 1)), ((0x0013bb064e00a1ccULL) >> 28), ((0x009411ae27870e11ULL) & ((1 << 28) - 1)), ((0x009411ae27870e11ULL) >> 28), ((0x00ea8e5b4d531223ULL) & ((1 << 28) - 1)), ((0x00ea8e5b4d531223ULL) >> 28), ((0x0032fe7d2aaece2eULL) & ((1 << 28) - 1)), ((0x0032fe7d2aaece2eULL) >> 28), ((0x00d989e243e7bb41ULL) & ((1 << 28) - 1)), ((0x00d989e243e7bb41ULL) >> 28), ((0x000fe79a508e9b8bULL) & ((1 << 28) - 1)), ((0x000fe79a508e9b8bULL) >> 28)}}


                                                                        },
            {{{((0x005e0426b9bfc5b1ULL) & ((1 << 28) - 1)), ((0x005e0426b9bfc5b1ULL) >> 28), ((0x0041a5b1d29ee4faULL) & ((1 << 28) - 1)), ((0x0041a5b1d29ee4faULL) >> 28), ((0x0015b0def7774391ULL) & ((1 << 28) - 1)), ((0x0015b0def7774391ULL) >> 28), ((0x00bc164f1f51af01ULL) & ((1 << 28) - 1)), ((0x00bc164f1f51af01ULL) >> 28), ((0x00d543b0942797b9ULL) & ((1 << 28) - 1)), ((0x00d543b0942797b9ULL) >> 28), ((0x003c129b6398099cULL) & ((1 << 28) - 1)), ((0x003c129b6398099cULL) >> 28), ((0x002b114c6e5adf18ULL) & ((1 << 28) - 1)), ((0x002b114c6e5adf18ULL) >> 28), ((0x00b4e630e4018a7bULL) & ((1 << 28) - 1)), ((0x00b4e630e4018a7bULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x00d490afc95f8420ULL) & ((1 << 28) - 1)), ((0x00d490afc95f8420ULL) >> 28), ((0x00b096bf50c1d9b9ULL) & ((1 << 28) - 1)), ((0x00b096bf50c1d9b9ULL) >> 28), ((0x00799fd707679866ULL) & ((1 << 28) - 1)), ((0x00799fd707679866ULL) >> 28), ((0x007c74d9334afbeaULL) & ((1 << 28) - 1)), ((0x007c74d9334afbeaULL) >> 28), ((0x00efaa8be80ff4edULL) & ((1 << 28) - 1)), ((0x00efaa8be80ff4edULL) >> 28), ((0x0075c4943bb81694ULL) & ((1 << 28) - 1)), ((0x0075c4943bb81694ULL) >> 28), ((0x00c21c2fca161f36ULL) & ((1 << 28) - 1)), ((0x00c21c2fca161f36ULL) >> 28), ((0x00e77035d492bfeeULL) & ((1 << 28) - 1)), ((0x00e77035d492bfeeULL) >> 28)}}


                                                                        },
            {{{((0x006658a190dd6661ULL) & ((1 << 28) - 1)), ((0x006658a190dd6661ULL) >> 28), ((0x00e0e9bab38609a6ULL) & ((1 << 28) - 1)), ((0x00e0e9bab38609a6ULL) >> 28), ((0x0028895c802237edULL) & ((1 << 28) - 1)), ((0x0028895c802237edULL) >> 28), ((0x006a0229c494f587ULL) & ((1 << 28) - 1)), ((0x006a0229c494f587ULL) >> 28), ((0x002dcde96c9916b7ULL) & ((1 << 28) - 1)), ((0x002dcde96c9916b7ULL) >> 28), ((0x00d158822de16218ULL) & ((1 << 28) - 1)), ((0x00d158822de16218ULL) >> 28), ((0x00173b917a06856fULL) & ((1 << 28) - 1)), ((0x00173b917a06856fULL) >> 28), ((0x00ca78a79ae07326ULL) & ((1 << 28) - 1)), ((0x00ca78a79ae07326ULL) >> 28)}}


                                                                        },
            {{{((0x00e35bfc79caced4ULL) & ((1 << 28) - 1)), ((0x00e35bfc79caced4ULL) >> 28), ((0x0087238a3e1fe3bbULL) & ((1 << 28) - 1)), ((0x0087238a3e1fe3bbULL) >> 28), ((0x00bcbf0ff4ceff5bULL) & ((1 << 28) - 1)), ((0x00bcbf0ff4ceff5bULL) >> 28), ((0x00a19c1c94099b91ULL) & ((1 << 28) - 1)), ((0x00a19c1c94099b91ULL) >> 28), ((0x0071e102b49db976ULL) & ((1 << 28) - 1)), ((0x0071e102b49db976ULL) >> 28), ((0x0059e3d004eada1eULL) & ((1 << 28) - 1)), ((0x0059e3d004eada1eULL) >> 28), ((0x008da78afa58a47eULL) & ((1 << 28) - 1)), ((0x008da78afa58a47eULL) >> 28), ((0x00579c8ebf269187ULL) & ((1 << 28) - 1)), ((0x00579c8ebf269187ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x00a16c2905eee75fULL) & ((1 << 28) - 1)), ((0x00a16c2905eee75fULL) >> 28), ((0x009d4bcaea2c7e1dULL) & ((1 << 28) - 1)), ((0x009d4bcaea2c7e1dULL) >> 28), ((0x00d3bd79bfad19dfULL) & ((1 << 28) - 1)), ((0x00d3bd79bfad19dfULL) >> 28), ((0x0050da745193342cULL) & ((1 << 28) - 1)), ((0x0050da745193342cULL) >> 28), ((0x006abdb8f6b29ab1ULL) & ((1 << 28) - 1)), ((0x006abdb8f6b29ab1ULL) >> 28), ((0x00a24fe0a4fef7efULL) & ((1 << 28) - 1)), ((0x00a24fe0a4fef7efULL) >> 28), ((0x0063730da1057dfbULL) & ((1 << 28) - 1)), ((0x0063730da1057dfbULL) >> 28), ((0x00a08c312c8eb108ULL) & ((1 << 28) - 1)), ((0x00a08c312c8eb108ULL) >> 28)}}


                                                                        },
            {{{((0x00b583be005375beULL) & ((1 << 28) - 1)), ((0x00b583be005375beULL) >> 28), ((0x00a40c8f8a4e3df4ULL) & ((1 << 28) - 1)), ((0x00a40c8f8a4e3df4ULL) >> 28), ((0x003fac4a8f5bdbf7ULL) & ((1 << 28) - 1)), ((0x003fac4a8f5bdbf7ULL) >> 28), ((0x00d4481d872cd718ULL) & ((1 << 28) - 1)), ((0x00d4481d872cd718ULL) >> 28), ((0x004dc8749cdbaefeULL) & ((1 << 28) - 1)), ((0x004dc8749cdbaefeULL) >> 28), ((0x00cce740d5e5c975ULL) & ((1 << 28) - 1)), ((0x00cce740d5e5c975ULL) >> 28), ((0x000b1c1f4241fd21ULL) & ((1 << 28) - 1)), ((0x000b1c1f4241fd21ULL) >> 28), ((0x00a76de1b4e1cd07ULL) & ((1 << 28) - 1)), ((0x00a76de1b4e1cd07ULL) >> 28)}}


                                                                        },
            {{{((0x007a076500d30b62ULL) & ((1 << 28) - 1)), ((0x007a076500d30b62ULL) >> 28), ((0x000a6e117b7f090fULL) & ((1 << 28) - 1)), ((0x000a6e117b7f090fULL) >> 28), ((0x00c8712ae7eebd9aULL) & ((1 << 28) - 1)), ((0x00c8712ae7eebd9aULL) >> 28), ((0x000fbd6c1d5f6ff7ULL) & ((1 << 28) - 1)), ((0x000fbd6c1d5f6ff7ULL) >> 28), ((0x003a7977246ebf11ULL) & ((1 << 28) - 1)), ((0x003a7977246ebf11ULL) >> 28), ((0x00166ed969c6600eULL) & ((1 << 28) - 1)), ((0x00166ed969c6600eULL) >> 28), ((0x00aa42e469c98becULL) & ((1 << 28) - 1)), ((0x00aa42e469c98becULL) >> 28), ((0x00dc58f307cf0666ULL) & ((1 << 28) - 1)), ((0x00dc58f307cf0666ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x004b491f65a9a28bULL) & ((1 << 28) - 1)), ((0x004b491f65a9a28bULL) >> 28), ((0x006a10309e8a55b7ULL) & ((1 << 28) - 1)), ((0x006a10309e8a55b7ULL) >> 28), ((0x00b67210185187efULL) & ((1 << 28) - 1)), ((0x00b67210185187efULL) >> 28), ((0x00cf6497b12d9b8fULL) & ((1 << 28) - 1)), ((0x00cf6497b12d9b8fULL) >> 28), ((0x0085778c56e2b1baULL) & ((1 << 28) - 1)), ((0x0085778c56e2b1baULL) >> 28), ((0x0015b4c07a814d85ULL) & ((1 << 28) - 1)), ((0x0015b4c07a814d85ULL) >> 28), ((0x00686479e62da561ULL) & ((1 << 28) - 1)), ((0x00686479e62da561ULL) >> 28), ((0x008de5d88f114916ULL) & ((1 << 28) - 1)), ((0x008de5d88f114916ULL) >> 28)}}


                                                                        },
            {{{((0x00e37c88d6bba7b1ULL) & ((1 << 28) - 1)), ((0x00e37c88d6bba7b1ULL) >> 28), ((0x003e4577e1b8d433ULL) & ((1 << 28) - 1)), ((0x003e4577e1b8d433ULL) >> 28), ((0x0050d8ea5f510ec0ULL) & ((1 << 28) - 1)), ((0x0050d8ea5f510ec0ULL) >> 28), ((0x0042fc9f2da9ef59ULL) & ((1 << 28) - 1)), ((0x0042fc9f2da9ef59ULL) >> 28), ((0x003bd074c1141420ULL) & ((1 << 28) - 1)), ((0x003bd074c1141420ULL) >> 28), ((0x00561b8b7b68774eULL) & ((1 << 28) - 1)), ((0x00561b8b7b68774eULL) >> 28), ((0x00232e5e5d1013a3ULL) & ((1 << 28) - 1)), ((0x00232e5e5d1013a3ULL) >> 28), ((0x006b7f2cb3d7e73fULL) & ((1 << 28) - 1)), ((0x006b7f2cb3d7e73fULL) >> 28)}}


                                                                        },
            {{{((0x004bdd0f0b41e6a0ULL) & ((1 << 28) - 1)), ((0x004bdd0f0b41e6a0ULL) >> 28), ((0x001773057c405d24ULL) & ((1 << 28) - 1)), ((0x001773057c405d24ULL) >> 28), ((0x006029f99915bd97ULL) & ((1 << 28) - 1)), ((0x006029f99915bd97ULL) >> 28), ((0x006a5ba70a17fe2fULL) & ((1 << 28) - 1)), ((0x006a5ba70a17fe2fULL) >> 28), ((0x0046111977df7e08ULL) & ((1 << 28) - 1)), ((0x0046111977df7e08ULL) >> 28), ((0x004d8124c89fb6b7ULL) & ((1 << 28) - 1)), ((0x004d8124c89fb6b7ULL) >> 28), ((0x00580983b2bb2724ULL) & ((1 << 28) - 1)), ((0x00580983b2bb2724ULL) >> 28), ((0x00207bf330d6f3feULL) & ((1 << 28) - 1)), ((0x00207bf330d6f3feULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x007efdc93972a48bULL) & ((1 << 28) - 1)), ((0x007efdc93972a48bULL) >> 28), ((0x002f5e50e78d5feeULL) & ((1 << 28) - 1)), ((0x002f5e50e78d5feeULL) >> 28), ((0x0080dc11d61c7fe5ULL) & ((1 << 28) - 1)), ((0x0080dc11d61c7fe5ULL) >> 28), ((0x0065aa598707245bULL) & ((1 << 28) - 1)), ((0x0065aa598707245bULL) >> 28), ((0x009abba2300641beULL) & ((1 << 28) - 1)), ((0x009abba2300641beULL) >> 28), ((0x000c68787656543aULL) & ((1 << 28) - 1)), ((0x000c68787656543aULL) >> 28), ((0x00ffe0fef2dc0a17ULL) & ((1 << 28) - 1)), ((0x00ffe0fef2dc0a17ULL) >> 28), ((0x00007ffbd6cb4f3aULL) & ((1 << 28) - 1)), ((0x00007ffbd6cb4f3aULL) >> 28)}}


                                                                        },
            {{{((0x0036012f2b836efcULL) & ((1 << 28) - 1)), ((0x0036012f2b836efcULL) >> 28), ((0x00458c126d6b5fbcULL) & ((1 << 28) - 1)), ((0x00458c126d6b5fbcULL) >> 28), ((0x00a34436d719ad1eULL) & ((1 << 28) - 1)), ((0x00a34436d719ad1eULL) >> 28), ((0x0097be6167117deaULL) & ((1 << 28) - 1)), ((0x0097be6167117deaULL) >> 28), ((0x0009c219c879cff3ULL) & ((1 << 28) - 1)), ((0x0009c219c879cff3ULL) >> 28), ((0x0065564493e60755ULL) & ((1 << 28) - 1)), ((0x0065564493e60755ULL) >> 28), ((0x00993ac94a8cdec0ULL) & ((1 << 28) - 1)), ((0x00993ac94a8cdec0ULL) >> 28), ((0x002d4885a4d0dbafULL) & ((1 << 28) - 1)), ((0x002d4885a4d0dbafULL) >> 28)}}


                                                                        },
            {{{((0x00598b60b4c068baULL) & ((1 << 28) - 1)), ((0x00598b60b4c068baULL) >> 28), ((0x00c547a0be7f1afdULL) & ((1 << 28) - 1)), ((0x00c547a0be7f1afdULL) >> 28), ((0x009582164acf12afULL) & ((1 << 28) - 1)), ((0x009582164acf12afULL) >> 28), ((0x00af4acac4fbbe40ULL) & ((1 << 28) - 1)), ((0x00af4acac4fbbe40ULL) >> 28), ((0x005f6ca7c539121aULL) & ((1 << 28) - 1)), ((0x005f6ca7c539121aULL) >> 28), ((0x003b6e752ebf9d66ULL) & ((1 << 28) - 1)), ((0x003b6e752ebf9d66ULL) >> 28), ((0x00f08a30d5cac5d4ULL) & ((1 << 28) - 1)), ((0x00f08a30d5cac5d4ULL) >> 28), ((0x00e399bb5f97c5a9ULL) & ((1 << 28) - 1)), ((0x00e399bb5f97c5a9ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x007445a0409c0a66ULL) & ((1 << 28) - 1)), ((0x007445a0409c0a66ULL) >> 28), ((0x00a65c369f3829c0ULL) & ((1 << 28) - 1)), ((0x00a65c369f3829c0ULL) >> 28), ((0x0031d248a4f74826ULL) & ((1 << 28) - 1)), ((0x0031d248a4f74826ULL) >> 28), ((0x006817f34defbe8eULL) & ((1 << 28) - 1)), ((0x006817f34defbe8eULL) >> 28), ((0x00649741d95ebf2eULL) & ((1 << 28) - 1)), ((0x00649741d95ebf2eULL) >> 28), ((0x00d46466ab16b397ULL) & ((1 << 28) - 1)), ((0x00d46466ab16b397ULL) >> 28), ((0x00fdc35703bee414ULL) & ((1 << 28) - 1)), ((0x00fdc35703bee414ULL) >> 28), ((0x00343b43334525f8ULL) & ((1 << 28) - 1)), ((0x00343b43334525f8ULL) >> 28)}}


                                                                        },
            {{{((0x001796bea93f6401ULL) & ((1 << 28) - 1)), ((0x001796bea93f6401ULL) >> 28), ((0x00090c5a42e85269ULL) & ((1 << 28) - 1)), ((0x00090c5a42e85269ULL) >> 28), ((0x00672412ba1252edULL) & ((1 << 28) - 1)), ((0x00672412ba1252edULL) >> 28), ((0x001201d47b6de7deULL) & ((1 << 28) - 1)), ((0x001201d47b6de7deULL) >> 28), ((0x006877bccfe66497ULL) & ((1 << 28) - 1)), ((0x006877bccfe66497ULL) >> 28), ((0x00b554fd97a4c161ULL) & ((1 << 28) - 1)), ((0x00b554fd97a4c161ULL) >> 28), ((0x009753f42dbac3cfULL) & ((1 << 28) - 1)), ((0x009753f42dbac3cfULL) >> 28), ((0x00e983e3e378270aULL) & ((1 << 28) - 1)), ((0x00e983e3e378270aULL) >> 28)}}


                                                                        },
            {{{((0x00ac3eff18849872ULL) & ((1 << 28) - 1)), ((0x00ac3eff18849872ULL) >> 28), ((0x00f0eea3bff05690ULL) & ((1 << 28) - 1)), ((0x00f0eea3bff05690ULL) >> 28), ((0x00a6d72c21dd505dULL) & ((1 << 28) - 1)), ((0x00a6d72c21dd505dULL) >> 28), ((0x001b832642424169ULL) & ((1 << 28) - 1)), ((0x001b832642424169ULL) >> 28), ((0x00a6813017b540e5ULL) & ((1 << 28) - 1)), ((0x00a6813017b540e5ULL) >> 28), ((0x00a744bd71b385cdULL) & ((1 << 28) - 1)), ((0x00a744bd71b385cdULL) >> 28), ((0x0022a7d089130a7bULL) & ((1 << 28) - 1)), ((0x0022a7d089130a7bULL) >> 28), ((0x004edeec9a133486ULL) & ((1 << 28) - 1)), ((0x004edeec9a133486ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x00b2d6729196e8a9ULL) & ((1 << 28) - 1)), ((0x00b2d6729196e8a9ULL) >> 28), ((0x0088a9bb2031cef4ULL) & ((1 << 28) - 1)), ((0x0088a9bb2031cef4ULL) >> 28), ((0x00579e7787dc1567ULL) & ((1 << 28) - 1)), ((0x00579e7787dc1567ULL) >> 28), ((0x0030f49feb059190ULL) & ((1 << 28) - 1)), ((0x0030f49feb059190ULL) >> 28), ((0x00a0b1d69c7f7d8fULL) & ((1 << 28) - 1)), ((0x00a0b1d69c7f7d8fULL) >> 28), ((0x0040bdcc6d9d806fULL) & ((1 << 28) - 1)), ((0x0040bdcc6d9d806fULL) >> 28), ((0x00d76c4037edd095ULL) & ((1 << 28) - 1)), ((0x00d76c4037edd095ULL) >> 28), ((0x00bbf24376415dd7ULL) & ((1 << 28) - 1)), ((0x00bbf24376415dd7ULL) >> 28)}}


                                                                        },
            {{{((0x00240465ff5a7197ULL) & ((1 << 28) - 1)), ((0x00240465ff5a7197ULL) >> 28), ((0x00bb97e76caf27d0ULL) & ((1 << 28) - 1)), ((0x00bb97e76caf27d0ULL) >> 28), ((0x004b4edbf8116d39ULL) & ((1 << 28) - 1)), ((0x004b4edbf8116d39ULL) >> 28), ((0x001d8586f708cbaaULL) & ((1 << 28) - 1)), ((0x001d8586f708cbaaULL) >> 28), ((0x000f8ee8ff8e4a50ULL) & ((1 << 28) - 1)), ((0x000f8ee8ff8e4a50ULL) >> 28), ((0x00dde5a1945dd622ULL) & ((1 << 28) - 1)), ((0x00dde5a1945dd622ULL) >> 28), ((0x00e6fc1c0957e07cULL) & ((1 << 28) - 1)), ((0x00e6fc1c0957e07cULL) >> 28), ((0x0041c9cdabfd88a0ULL) & ((1 << 28) - 1)), ((0x0041c9cdabfd88a0ULL) >> 28)}}


                                                                        },
            {{{((0x005344b0bf5b548cULL) & ((1 << 28) - 1)), ((0x005344b0bf5b548cULL) >> 28), ((0x002957d0b705cc99ULL) & ((1 << 28) - 1)), ((0x002957d0b705cc99ULL) >> 28), ((0x00f586a70390553dULL) & ((1 << 28) - 1)), ((0x00f586a70390553dULL) >> 28), ((0x0075b3229f583cc3ULL) & ((1 << 28) - 1)), ((0x0075b3229f583cc3ULL) >> 28), ((0x00a1aa78227490e4ULL) & ((1 << 28) - 1)), ((0x00a1aa78227490e4ULL) >> 28), ((0x001bf09cf7957717ULL) & ((1 << 28) - 1)), ((0x001bf09cf7957717ULL) >> 28), ((0x00cf6bf344325f52ULL) & ((1 << 28) - 1)), ((0x00cf6bf344325f52ULL) >> 28), ((0x0065bd1c23ca3ecfULL) & ((1 << 28) - 1)), ((0x0065bd1c23ca3ecfULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x009bff3b3239363cULL) & ((1 << 28) - 1)), ((0x009bff3b3239363cULL) >> 28), ((0x00e17368796ef7c0ULL) & ((1 << 28) - 1)), ((0x00e17368796ef7c0ULL) >> 28), ((0x00528b0fe0971f3aULL) & ((1 << 28) - 1)), ((0x00528b0fe0971f3aULL) >> 28), ((0x0008014fc8d4a095ULL) & ((1 << 28) - 1)), ((0x0008014fc8d4a095ULL) >> 28), ((0x00d09f2e8a521ec4ULL) & ((1 << 28) - 1)), ((0x00d09f2e8a521ec4ULL) >> 28), ((0x006713ab5dde5987ULL) & ((1 << 28) - 1)), ((0x006713ab5dde5987ULL) >> 28), ((0x0003015758e0dbb1ULL) & ((1 << 28) - 1)), ((0x0003015758e0dbb1ULL) >> 28), ((0x00215999f1ba212dULL) & ((1 << 28) - 1)), ((0x00215999f1ba212dULL) >> 28)}}


                                                                        },
            {{{((0x002c88e93527da0eULL) & ((1 << 28) - 1)), ((0x002c88e93527da0eULL) >> 28), ((0x0077c78f3456aad5ULL) & ((1 << 28) - 1)), ((0x0077c78f3456aad5ULL) >> 28), ((0x0071087a0a389d1cULL) & ((1 << 28) - 1)), ((0x0071087a0a389d1cULL) >> 28), ((0x00934dac1fb96dbdULL) & ((1 << 28) - 1)), ((0x00934dac1fb96dbdULL) >> 28), ((0x008470e801162697ULL) & ((1 << 28) - 1)), ((0x008470e801162697ULL) >> 28), ((0x005bc2196cd4ad49ULL) & ((1 << 28) - 1)), ((0x005bc2196cd4ad49ULL) >> 28), ((0x00e535601d5087c3ULL) & ((1 << 28) - 1)), ((0x00e535601d5087c3ULL) >> 28), ((0x00769888700f497fULL) & ((1 << 28) - 1)), ((0x00769888700f497fULL) >> 28)}}


                                                                        },
            {{{((0x00da7a4b557298adULL) & ((1 << 28) - 1)), ((0x00da7a4b557298adULL) >> 28), ((0x0019d2589ea5df76ULL) & ((1 << 28) - 1)), ((0x0019d2589ea5df76ULL) >> 28), ((0x00ef3e38be0c6497ULL) & ((1 << 28) - 1)), ((0x00ef3e38be0c6497ULL) >> 28), ((0x00a9644e1312609aULL) & ((1 << 28) - 1)), ((0x00a9644e1312609aULL) >> 28), ((0x004592f61b2558daULL) & ((1 << 28) - 1)), ((0x004592f61b2558daULL) >> 28), ((0x0082c1df510d7e46ULL) & ((1 << 28) - 1)), ((0x0082c1df510d7e46ULL) >> 28), ((0x0042809a535c0023ULL) & ((1 << 28) - 1)), ((0x0042809a535c0023ULL) >> 28), ((0x00215bcb5afd7757ULL) & ((1 << 28) - 1)), ((0x00215bcb5afd7757ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x002b9df55a1a4213ULL) & ((1 << 28) - 1)), ((0x002b9df55a1a4213ULL) >> 28), ((0x00dcfc3b464a26beULL) & ((1 << 28) - 1)), ((0x00dcfc3b464a26beULL) >> 28), ((0x00c4f9e07a8144d5ULL) & ((1 << 28) - 1)), ((0x00c4f9e07a8144d5ULL) >> 28), ((0x00c8e0617a92b602ULL) & ((1 << 28) - 1)), ((0x00c8e0617a92b602ULL) >> 28), ((0x008e3c93accafae0ULL) & ((1 << 28) - 1)), ((0x008e3c93accafae0ULL) >> 28), ((0x00bf1bcb95b2ca60ULL) & ((1 << 28) - 1)), ((0x00bf1bcb95b2ca60ULL) >> 28), ((0x004ce2426a613bf3ULL) & ((1 << 28) - 1)), ((0x004ce2426a613bf3ULL) >> 28), ((0x00266cac58e40921ULL) & ((1 << 28) - 1)), ((0x00266cac58e40921ULL) >> 28)}}


                                                                        },
            {{{((0x008456d5db76e8f0ULL) & ((1 << 28) - 1)), ((0x008456d5db76e8f0ULL) >> 28), ((0x0032ca9cab2ce163ULL) & ((1 << 28) - 1)), ((0x0032ca9cab2ce163ULL) >> 28), ((0x0059f2b8bf91abcfULL) & ((1 << 28) - 1)), ((0x0059f2b8bf91abcfULL) >> 28), ((0x0063c2a021712788ULL) & ((1 << 28) - 1)), ((0x0063c2a021712788ULL) >> 28), ((0x00f86155af22f72dULL) & ((1 << 28) - 1)), ((0x00f86155af22f72dULL) >> 28), ((0x00db98b2a6c005a0ULL) & ((1 << 28) - 1)), ((0x00db98b2a6c005a0ULL) >> 28), ((0x00ac6e416a693ac4ULL) & ((1 << 28) - 1)), ((0x00ac6e416a693ac4ULL) >> 28), ((0x007a93572af53226ULL) & ((1 << 28) - 1)), ((0x007a93572af53226ULL) >> 28)}}


                                                                        },
            {{{((0x0087767520f0de22ULL) & ((1 << 28) - 1)), ((0x0087767520f0de22ULL) >> 28), ((0x0091f64012279fb5ULL) & ((1 << 28) - 1)), ((0x0091f64012279fb5ULL) >> 28), ((0x001050f1f0644999ULL) & ((1 << 28) - 1)), ((0x001050f1f0644999ULL) >> 28), ((0x004f097a2477ad3cULL) & ((1 << 28) - 1)), ((0x004f097a2477ad3cULL) >> 28), ((0x006b37913a9947bdULL) & ((1 << 28) - 1)), ((0x006b37913a9947bdULL) >> 28), ((0x001a3d78645af241ULL) & ((1 << 28) - 1)), ((0x001a3d78645af241ULL) >> 28), ((0x0057832bbb3008a7ULL) & ((1 << 28) - 1)), ((0x0057832bbb3008a7ULL) >> 28), ((0x002c1d902b80dc20ULL) & ((1 << 28) - 1)), ((0x002c1d902b80dc20ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x001a6002bf178877ULL) & ((1 << 28) - 1)), ((0x001a6002bf178877ULL) >> 28), ((0x009bce168aa5af50ULL) & ((1 << 28) - 1)), ((0x009bce168aa5af50ULL) >> 28), ((0x005fc318ff04a7f5ULL) & ((1 << 28) - 1)), ((0x005fc318ff04a7f5ULL) >> 28), ((0x0052818f55c36461ULL) & ((1 << 28) - 1)), ((0x0052818f55c36461ULL) >> 28), ((0x008768f5d4b24afbULL) & ((1 << 28) - 1)), ((0x008768f5d4b24afbULL) >> 28), ((0x0037ffbae7b69c85ULL) & ((1 << 28) - 1)), ((0x0037ffbae7b69c85ULL) >> 28), ((0x0018195a4b61edc0ULL) & ((1 << 28) - 1)), ((0x0018195a4b61edc0ULL) >> 28), ((0x001e12ea088434b2ULL) & ((1 << 28) - 1)), ((0x001e12ea088434b2ULL) >> 28)}}


                                                                        },
            {{{((0x0047d3f804e7ab07ULL) & ((1 << 28) - 1)), ((0x0047d3f804e7ab07ULL) >> 28), ((0x00a809ab5f905260ULL) & ((1 << 28) - 1)), ((0x00a809ab5f905260ULL) >> 28), ((0x00b3ffc7cdaf306dULL) & ((1 << 28) - 1)), ((0x00b3ffc7cdaf306dULL) >> 28), ((0x00746e8ec2d6e509ULL) & ((1 << 28) - 1)), ((0x00746e8ec2d6e509ULL) >> 28), ((0x00d0dade8887a645ULL) & ((1 << 28) - 1)), ((0x00d0dade8887a645ULL) >> 28), ((0x00acceeebde0dd37ULL) & ((1 << 28) - 1)), ((0x00acceeebde0dd37ULL) >> 28), ((0x009bc2579054686bULL) & ((1 << 28) - 1)), ((0x009bc2579054686bULL) >> 28), ((0x0023804f97f1c2bfULL) & ((1 << 28) - 1)), ((0x0023804f97f1c2bfULL) >> 28)}}


                                                                        },
            {{{((0x0043e2e2e50b80d7ULL) & ((1 << 28) - 1)), ((0x0043e2e2e50b80d7ULL) >> 28), ((0x00143aafe4427e0fULL) & ((1 << 28) - 1)), ((0x00143aafe4427e0fULL) >> 28), ((0x005594aaecab855bULL) & ((1 << 28) - 1)), ((0x005594aaecab855bULL) >> 28), ((0x008b12ccaaecbc01ULL) & ((1 << 28) - 1)), ((0x008b12ccaaecbc01ULL) >> 28), ((0x002deeb091082bc3ULL) & ((1 << 28) - 1)), ((0x002deeb091082bc3ULL) >> 28), ((0x009cca4be2ae7514ULL) & ((1 << 28) - 1)), ((0x009cca4be2ae7514ULL) >> 28), ((0x00142b96e696d047ULL) & ((1 << 28) - 1)), ((0x00142b96e696d047ULL) >> 28), ((0x00ad2a2b1c05256aULL) & ((1 << 28) - 1)), ((0x00ad2a2b1c05256aULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x003914f2f144b78bULL) & ((1 << 28) - 1)), ((0x003914f2f144b78bULL) >> 28), ((0x007a95dd8bee6f68ULL) & ((1 << 28) - 1)), ((0x007a95dd8bee6f68ULL) >> 28), ((0x00c7f4384d61c8e6ULL) & ((1 << 28) - 1)), ((0x00c7f4384d61c8e6ULL) >> 28), ((0x004e51eb60f1bdb2ULL) & ((1 << 28) - 1)), ((0x004e51eb60f1bdb2ULL) >> 28), ((0x00f64be7aa4621d8ULL) & ((1 << 28) - 1)), ((0x00f64be7aa4621d8ULL) >> 28), ((0x006797bfec2f0ac0ULL) & ((1 << 28) - 1)), ((0x006797bfec2f0ac0ULL) >> 28), ((0x007d17aab3c75900ULL) & ((1 << 28) - 1)), ((0x007d17aab3c75900ULL) >> 28), ((0x001893e73cac8bc5ULL) & ((1 << 28) - 1)), ((0x001893e73cac8bc5ULL) >> 28)}}


                                                                        },
            {{{((0x00140360b768665bULL) & ((1 << 28) - 1)), ((0x00140360b768665bULL) >> 28), ((0x00b68aca4967f977ULL) & ((1 << 28) - 1)), ((0x00b68aca4967f977ULL) >> 28), ((0x0001089b66195ae4ULL) & ((1 << 28) - 1)), ((0x0001089b66195ae4ULL) >> 28), ((0x00fe71122185e725ULL) & ((1 << 28) - 1)), ((0x00fe71122185e725ULL) >> 28), ((0x000bca2618d49637ULL) & ((1 << 28) - 1)), ((0x000bca2618d49637ULL) >> 28), ((0x00a54f0557d7e98aULL) & ((1 << 28) - 1)), ((0x00a54f0557d7e98aULL) >> 28), ((0x00cdcd2f91d6f417ULL) & ((1 << 28) - 1)), ((0x00cdcd2f91d6f417ULL) >> 28), ((0x00ab8c13741fd793ULL) & ((1 << 28) - 1)), ((0x00ab8c13741fd793ULL) >> 28)}}


                                                                        },
            {{{((0x00725ee6b1e549e0ULL) & ((1 << 28) - 1)), ((0x00725ee6b1e549e0ULL) >> 28), ((0x007124a0769777faULL) & ((1 << 28) - 1)), ((0x007124a0769777faULL) >> 28), ((0x000b68fdad07ae42ULL) & ((1 << 28) - 1)), ((0x000b68fdad07ae42ULL) >> 28), ((0x0085b909cd4952dfULL) & ((1 << 28) - 1)), ((0x0085b909cd4952dfULL) >> 28), ((0x0092d2e3c81606f4ULL) & ((1 << 28) - 1)), ((0x0092d2e3c81606f4ULL) >> 28), ((0x009f22f6cac099a0ULL) & ((1 << 28) - 1)), ((0x009f22f6cac099a0ULL) >> 28), ((0x00f59da57f2799a8ULL) & ((1 << 28) - 1)), ((0x00f59da57f2799a8ULL) >> 28), ((0x00f06c090122f777ULL) & ((1 << 28) - 1)), ((0x00f06c090122f777ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x00ce0bed0a3532bcULL) & ((1 << 28) - 1)), ((0x00ce0bed0a3532bcULL) >> 28), ((0x001a5048a22df16bULL) & ((1 << 28) - 1)), ((0x001a5048a22df16bULL) >> 28), ((0x00e31db4cbad8bf1ULL) & ((1 << 28) - 1)), ((0x00e31db4cbad8bf1ULL) >> 28), ((0x00e89292120cf00eULL) & ((1 << 28) - 1)), ((0x00e89292120cf00eULL) >> 28), ((0x007d1dd1a9b00034ULL) & ((1 << 28) - 1)), ((0x007d1dd1a9b00034ULL) >> 28), ((0x00e2a9041ff8f680ULL) & ((1 << 28) - 1)), ((0x00e2a9041ff8f680ULL) >> 28), ((0x006a4c837ae596e7ULL) & ((1 << 28) - 1)), ((0x006a4c837ae596e7ULL) >> 28), ((0x00713af1068070b3ULL) & ((1 << 28) - 1)), ((0x00713af1068070b3ULL) >> 28)}}


                                                                        },
            {{{((0x00c4fe64ce66d04bULL) & ((1 << 28) - 1)), ((0x00c4fe64ce66d04bULL) >> 28), ((0x00b095d52e09b3d7ULL) & ((1 << 28) - 1)), ((0x00b095d52e09b3d7ULL) >> 28), ((0x00758bbecb1a3a8eULL) & ((1 << 28) - 1)), ((0x00758bbecb1a3a8eULL) >> 28), ((0x00f35cce8d0650c0ULL) & ((1 << 28) - 1)), ((0x00f35cce8d0650c0ULL) >> 28), ((0x002b878aa5984473ULL) & ((1 << 28) - 1)), ((0x002b878aa5984473ULL) >> 28), ((0x0062e0a3b7544ddcULL) & ((1 << 28) - 1)), ((0x0062e0a3b7544ddcULL) >> 28), ((0x00b25b290ed116feULL) & ((1 << 28) - 1)), ((0x00b25b290ed116feULL) >> 28), ((0x007b0f6abe0bebf2ULL) & ((1 << 28) - 1)), ((0x007b0f6abe0bebf2ULL) >> 28)}}


                                                                        },
            {{{((0x0081d4e3addae0a8ULL) & ((1 << 28) - 1)), ((0x0081d4e3addae0a8ULL) >> 28), ((0x003410c836c7ffccULL) & ((1 << 28) - 1)), ((0x003410c836c7ffccULL) >> 28), ((0x00c8129ad89e4314ULL) & ((1 << 28) - 1)), ((0x00c8129ad89e4314ULL) >> 28), ((0x000e3d5a23922dcdULL) & ((1 << 28) - 1)), ((0x000e3d5a23922dcdULL) >> 28), ((0x00d91e46f29c31f3ULL) & ((1 << 28) - 1)), ((0x00d91e46f29c31f3ULL) >> 28), ((0x006c728cde8c5947ULL) & ((1 << 28) - 1)), ((0x006c728cde8c5947ULL) >> 28), ((0x002bc655ba2566c0ULL) & ((1 << 28) - 1)), ((0x002bc655ba2566c0ULL) >> 28), ((0x002ca94721533108ULL) & ((1 << 28) - 1)), ((0x002ca94721533108ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x0051e4b3f764d8a9ULL) & ((1 << 28) - 1)), ((0x0051e4b3f764d8a9ULL) >> 28), ((0x0019792d46e904a0ULL) & ((1 << 28) - 1)), ((0x0019792d46e904a0ULL) >> 28), ((0x00853bc13dbc8227ULL) & ((1 << 28) - 1)), ((0x00853bc13dbc8227ULL) >> 28), ((0x000840208179f12dULL) & ((1 << 28) - 1)), ((0x000840208179f12dULL) >> 28), ((0x0068243474879235ULL) & ((1 << 28) - 1)), ((0x0068243474879235ULL) >> 28), ((0x0013856fbfe374d0ULL) & ((1 << 28) - 1)), ((0x0013856fbfe374d0ULL) >> 28), ((0x00bda12fe8676424ULL) & ((1 << 28) - 1)), ((0x00bda12fe8676424ULL) >> 28), ((0x00bbb43635926eb2ULL) & ((1 << 28) - 1)), ((0x00bbb43635926eb2ULL) >> 28)}}


                                                                        },
            {{{((0x0012cdc880a93982ULL) & ((1 << 28) - 1)), ((0x0012cdc880a93982ULL) >> 28), ((0x003c495b21cd1b58ULL) & ((1 << 28) - 1)), ((0x003c495b21cd1b58ULL) >> 28), ((0x00b7e5c93f22a26eULL) & ((1 << 28) - 1)), ((0x00b7e5c93f22a26eULL) >> 28), ((0x0044aa82dfb99458ULL) & ((1 << 28) - 1)), ((0x0044aa82dfb99458ULL) >> 28), ((0x009ba092cdffe9c0ULL) & ((1 << 28) - 1)), ((0x009ba092cdffe9c0ULL) >> 28), ((0x00a14b3ab2083b73ULL) & ((1 << 28) - 1)), ((0x00a14b3ab2083b73ULL) >> 28), ((0x000271c2f70e1c4bULL) & ((1 << 28) - 1)), ((0x000271c2f70e1c4bULL) >> 28), ((0x00eea9cac0f66eb8ULL) & ((1 << 28) - 1)), ((0x00eea9cac0f66eb8ULL) >> 28)}}


                                                                        },
            {{{((0x001a1847c4ac5480ULL) & ((1 << 28) - 1)), ((0x001a1847c4ac5480ULL) >> 28), ((0x00b1b412935bb03aULL) & ((1 << 28) - 1)), ((0x00b1b412935bb03aULL) >> 28), ((0x00f74285983bf2b2ULL) & ((1 << 28) - 1)), ((0x00f74285983bf2b2ULL) >> 28), ((0x00624138b5b5d0f1ULL) & ((1 << 28) - 1)), ((0x00624138b5b5d0f1ULL) >> 28), ((0x008820c0b03d38bfULL) & ((1 << 28) - 1)), ((0x008820c0b03d38bfULL) >> 28), ((0x00b94e50a18c1572ULL) & ((1 << 28) - 1)), ((0x00b94e50a18c1572ULL) >> 28), ((0x0060f6934841798fULL) & ((1 << 28) - 1)), ((0x0060f6934841798fULL) >> 28), ((0x00c52f5d66d6ebe2ULL) & ((1 << 28) - 1)), ((0x00c52f5d66d6ebe2ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x00da23d59f9bcea6ULL) & ((1 << 28) - 1)), ((0x00da23d59f9bcea6ULL) >> 28), ((0x00e0f27007a06a4bULL) & ((1 << 28) - 1)), ((0x00e0f27007a06a4bULL) >> 28), ((0x00128b5b43a6758cULL) & ((1 << 28) - 1)), ((0x00128b5b43a6758cULL) >> 28), ((0x000cf50190fa8b56ULL) & ((1 << 28) - 1)), ((0x000cf50190fa8b56ULL) >> 28), ((0x00fc877aba2b2d72ULL) & ((1 << 28) - 1)), ((0x00fc877aba2b2d72ULL) >> 28), ((0x00623bef52edf53fULL) & ((1 << 28) - 1)), ((0x00623bef52edf53fULL) >> 28), ((0x00e6af6b819669e2ULL) & ((1 << 28) - 1)), ((0x00e6af6b819669e2ULL) >> 28), ((0x00e314dc34fcaa4fULL) & ((1 << 28) - 1)), ((0x00e314dc34fcaa4fULL) >> 28)}}


                                                                        },
            {{{((0x0066e5eddd164d1eULL) & ((1 << 28) - 1)), ((0x0066e5eddd164d1eULL) >> 28), ((0x00418a7c6fe28238ULL) & ((1 << 28) - 1)), ((0x00418a7c6fe28238ULL) >> 28), ((0x0002e2f37e962c25ULL) & ((1 << 28) - 1)), ((0x0002e2f37e962c25ULL) >> 28), ((0x00f01f56b5975306ULL) & ((1 << 28) - 1)), ((0x00f01f56b5975306ULL) >> 28), ((0x0048842fa503875cULL) & ((1 << 28) - 1)), ((0x0048842fa503875cULL) >> 28), ((0x0057b0e968078143ULL) & ((1 << 28) - 1)), ((0x0057b0e968078143ULL) >> 28), ((0x00ff683024f3d134ULL) & ((1 << 28) - 1)), ((0x00ff683024f3d134ULL) >> 28), ((0x0082ae28fcad12e4ULL) & ((1 << 28) - 1)), ((0x0082ae28fcad12e4ULL) >> 28)}}


                                                                        },
            {{{((0x0011ddfd21260e42ULL) & ((1 << 28) - 1)), ((0x0011ddfd21260e42ULL) >> 28), ((0x00d05b0319a76892ULL) & ((1 << 28) - 1)), ((0x00d05b0319a76892ULL) >> 28), ((0x00183ea4368e9b8fULL) & ((1 << 28) - 1)), ((0x00183ea4368e9b8fULL) >> 28), ((0x00b0815662affc96ULL) & ((1 << 28) - 1)), ((0x00b0815662affc96ULL) >> 28), ((0x00b466a5e7ce7c88ULL) & ((1 << 28) - 1)), ((0x00b466a5e7ce7c88ULL) >> 28), ((0x00db93b07506e6eeULL) & ((1 << 28) - 1)), ((0x00db93b07506e6eeULL) >> 28), ((0x0033885f82f62401ULL) & ((1 << 28) - 1)), ((0x0033885f82f62401ULL) >> 28), ((0x0086f9090ec9b419ULL) & ((1 << 28) - 1)), ((0x0086f9090ec9b419ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x00d95d1c5fcb435aULL) & ((1 << 28) - 1)), ((0x00d95d1c5fcb435aULL) >> 28), ((0x0016d1ed6b5086f9ULL) & ((1 << 28) - 1)), ((0x0016d1ed6b5086f9ULL) >> 28), ((0x00792aa0b7e54d71ULL) & ((1 << 28) - 1)), ((0x00792aa0b7e54d71ULL) >> 28), ((0x0067b65715f1925dULL) & ((1 << 28) - 1)), ((0x0067b65715f1925dULL) >> 28), ((0x00a219755ec6176bULL) & ((1 << 28) - 1)), ((0x00a219755ec6176bULL) >> 28), ((0x00bc3f026b12c28fULL) & ((1 << 28) - 1)), ((0x00bc3f026b12c28fULL) >> 28), ((0x00700c897ffeb93eULL) & ((1 << 28) - 1)), ((0x00700c897ffeb93eULL) >> 28), ((0x0089b83f6ec50b46ULL) & ((1 << 28) - 1)), ((0x0089b83f6ec50b46ULL) >> 28)}}


                                                                        },
            {{{((0x003c97e6384da36eULL) & ((1 << 28) - 1)), ((0x003c97e6384da36eULL) >> 28), ((0x00423d53eac81a09ULL) & ((1 << 28) - 1)), ((0x00423d53eac81a09ULL) >> 28), ((0x00b70d68f3cdce35ULL) & ((1 << 28) - 1)), ((0x00b70d68f3cdce35ULL) >> 28), ((0x00ee7959b354b92cULL) & ((1 << 28) - 1)), ((0x00ee7959b354b92cULL) >> 28), ((0x00f4e9718819c8caULL) & ((1 << 28) - 1)), ((0x00f4e9718819c8caULL) >> 28), ((0x009349f12acbffe9ULL) & ((1 << 28) - 1)), ((0x009349f12acbffe9ULL) >> 28), ((0x005aee7b62cb7da6ULL) & ((1 << 28) - 1)), ((0x005aee7b62cb7da6ULL) >> 28), ((0x00d97764154ffc86ULL) & ((1 << 28) - 1)), ((0x00d97764154ffc86ULL) >> 28)}}


                                                                        },
            {{{((0x00526324babb46dcULL) & ((1 << 28) - 1)), ((0x00526324babb46dcULL) >> 28), ((0x002ee99b38d7bf9eULL) & ((1 << 28) - 1)), ((0x002ee99b38d7bf9eULL) >> 28), ((0x007ea51794706ef4ULL) & ((1 << 28) - 1)), ((0x007ea51794706ef4ULL) >> 28), ((0x00abeb04da6e3c39ULL) & ((1 << 28) - 1)), ((0x00abeb04da6e3c39ULL) >> 28), ((0x006b457c1d281060ULL) & ((1 << 28) - 1)), ((0x006b457c1d281060ULL) >> 28), ((0x00fe243e9a66c793ULL) & ((1 << 28) - 1)), ((0x00fe243e9a66c793ULL) >> 28), ((0x00378de0fb6c6ee4ULL) & ((1 << 28) - 1)), ((0x00378de0fb6c6ee4ULL) >> 28), ((0x003e4194b9c3cb93ULL) & ((1 << 28) - 1)), ((0x003e4194b9c3cb93ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x00fed3cd80ca2292ULL) & ((1 << 28) - 1)), ((0x00fed3cd80ca2292ULL) >> 28), ((0x0015b043a73ca613ULL) & ((1 << 28) - 1)), ((0x0015b043a73ca613ULL) >> 28), ((0x000a9fd7bf9be227ULL) & ((1 << 28) - 1)), ((0x000a9fd7bf9be227ULL) >> 28), ((0x003b5e03de2db983ULL) & ((1 << 28) - 1)), ((0x003b5e03de2db983ULL) >> 28), ((0x005af72d46904ef7ULL) & ((1 << 28) - 1)), ((0x005af72d46904ef7ULL) >> 28), ((0x00c0f1b5c49faa99ULL) & ((1 << 28) - 1)), ((0x00c0f1b5c49faa99ULL) >> 28), ((0x00dc86fc3bd305e1ULL) & ((1 << 28) - 1)), ((0x00dc86fc3bd305e1ULL) >> 28), ((0x00c92f08c1cb1797ULL) & ((1 << 28) - 1)), ((0x00c92f08c1cb1797ULL) >> 28)}}


                                                                        },
            {{{((0x0079680ce111ed3bULL) & ((1 << 28) - 1)), ((0x0079680ce111ed3bULL) >> 28), ((0x001a1ed82806122cULL) & ((1 << 28) - 1)), ((0x001a1ed82806122cULL) >> 28), ((0x000c2e7466d15df3ULL) & ((1 << 28) - 1)), ((0x000c2e7466d15df3ULL) >> 28), ((0x002c407f6f7150fdULL) & ((1 << 28) - 1)), ((0x002c407f6f7150fdULL) >> 28), ((0x00c5e7c96b1b0ce3ULL) & ((1 << 28) - 1)), ((0x00c5e7c96b1b0ce3ULL) >> 28), ((0x009aa44626863ff9ULL) & ((1 << 28) - 1)), ((0x009aa44626863ff9ULL) >> 28), ((0x00887b8b5b80be42ULL) & ((1 << 28) - 1)), ((0x00887b8b5b80be42ULL) >> 28), ((0x00b6023cec964825ULL) & ((1 << 28) - 1)), ((0x00b6023cec964825ULL) >> 28)}}


                                                                        },
            {{{((0x00e4a8e1048970c8ULL) & ((1 << 28) - 1)), ((0x00e4a8e1048970c8ULL) >> 28), ((0x0062887b7830a302ULL) & ((1 << 28) - 1)), ((0x0062887b7830a302ULL) >> 28), ((0x00bcf1c8cd81402bULL) & ((1 << 28) - 1)), ((0x00bcf1c8cd81402bULL) >> 28), ((0x0056dbb81a68f5beULL) & ((1 << 28) - 1)), ((0x0056dbb81a68f5beULL) >> 28), ((0x0014eced83f12452ULL) & ((1 << 28) - 1)), ((0x0014eced83f12452ULL) >> 28), ((0x00139e1a510150dfULL) & ((1 << 28) - 1)), ((0x00139e1a510150dfULL) >> 28), ((0x00bb81140a82d1a3ULL) & ((1 << 28) - 1)), ((0x00bb81140a82d1a3ULL) >> 28), ((0x000febcc1aaf1aa7ULL) & ((1 << 28) - 1)), ((0x000febcc1aaf1aa7ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x00a7527958238159ULL) & ((1 << 28) - 1)), ((0x00a7527958238159ULL) >> 28), ((0x0013ec9537a84cd6ULL) & ((1 << 28) - 1)), ((0x0013ec9537a84cd6ULL) >> 28), ((0x001d7fee7d562525ULL) & ((1 << 28) - 1)), ((0x001d7fee7d562525ULL) >> 28), ((0x00b9eefa6191d5e5ULL) & ((1 << 28) - 1)), ((0x00b9eefa6191d5e5ULL) >> 28), ((0x00dbc97db70bcb8aULL) & ((1 << 28) - 1)), ((0x00dbc97db70bcb8aULL) >> 28), ((0x00481affc7a4d395ULL) & ((1 << 28) - 1)), ((0x00481affc7a4d395ULL) >> 28), ((0x006f73d3e70c31bbULL) & ((1 << 28) - 1)), ((0x006f73d3e70c31bbULL) >> 28), ((0x00183f324ed96a61ULL) & ((1 << 28) - 1)), ((0x00183f324ed96a61ULL) >> 28)}}


                                                                        },
            {{{((0x0039dd7ce7fc6860ULL) & ((1 << 28) - 1)), ((0x0039dd7ce7fc6860ULL) >> 28), ((0x00d64f6425653da1ULL) & ((1 << 28) - 1)), ((0x00d64f6425653da1ULL) >> 28), ((0x003e037c7f57d0afULL) & ((1 << 28) - 1)), ((0x003e037c7f57d0afULL) >> 28), ((0x0063477a06e2bcf2ULL) & ((1 << 28) - 1)), ((0x0063477a06e2bcf2ULL) >> 28), ((0x001727dbb7ac67e6ULL) & ((1 << 28) - 1)), ((0x001727dbb7ac67e6ULL) >> 28), ((0x0049589f5efafe2eULL) & ((1 << 28) - 1)), ((0x0049589f5efafe2eULL) >> 28), ((0x00fc0fef2e813d54ULL) & ((1 << 28) - 1)), ((0x00fc0fef2e813d54ULL) >> 28), ((0x008baa5d087fb50dULL) & ((1 << 28) - 1)), ((0x008baa5d087fb50dULL) >> 28)}}


                                                                        },
            {{{((0x0024fb59d9b457c7ULL) & ((1 << 28) - 1)), ((0x0024fb59d9b457c7ULL) >> 28), ((0x00a7d4e060223e4cULL) & ((1 << 28) - 1)), ((0x00a7d4e060223e4cULL) >> 28), ((0x00c118d1b555fd80ULL) & ((1 << 28) - 1)), ((0x00c118d1b555fd80ULL) >> 28), ((0x0082e216c732f22aULL) & ((1 << 28) - 1)), ((0x0082e216c732f22aULL) >> 28), ((0x00cd2a2993089504ULL) & ((1 << 28) - 1)), ((0x00cd2a2993089504ULL) >> 28), ((0x003638e836a3e13dULL) & ((1 << 28) - 1)), ((0x003638e836a3e13dULL) >> 28), ((0x000d855ee89b4729ULL) & ((1 << 28) - 1)), ((0x000d855ee89b4729ULL) >> 28), ((0x008ec5b7d4810c91ULL) & ((1 << 28) - 1)), ((0x008ec5b7d4810c91ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x001bf51f7d65cdfdULL) & ((1 << 28) - 1)), ((0x001bf51f7d65cdfdULL) >> 28), ((0x00d14cdafa16a97dULL) & ((1 << 28) - 1)), ((0x00d14cdafa16a97dULL) >> 28), ((0x002c38e60fcd10e7ULL) & ((1 << 28) - 1)), ((0x002c38e60fcd10e7ULL) >> 28), ((0x00a27446e393efbdULL) & ((1 << 28) - 1)), ((0x00a27446e393efbdULL) >> 28), ((0x000b5d8946a71fddULL) & ((1 << 28) - 1)), ((0x000b5d8946a71fddULL) >> 28), ((0x0063df2cde128f2fULL) & ((1 << 28) - 1)), ((0x0063df2cde128f2fULL) >> 28), ((0x006c8679569b1888ULL) & ((1 << 28) - 1)), ((0x006c8679569b1888ULL) >> 28), ((0x0059ffc4925d732dULL) & ((1 << 28) - 1)), ((0x0059ffc4925d732dULL) >> 28)}}


                                                                        },
            {{{((0x00ece96f95f2b66fULL) & ((1 << 28) - 1)), ((0x00ece96f95f2b66fULL) >> 28), ((0x00ece7952813a27bULL) & ((1 << 28) - 1)), ((0x00ece7952813a27bULL) >> 28), ((0x0026fc36592e489eULL) & ((1 << 28) - 1)), ((0x0026fc36592e489eULL) >> 28), ((0x007157d1a2de0f66ULL) & ((1 << 28) - 1)), ((0x007157d1a2de0f66ULL) >> 28), ((0x00759dc111d86ddfULL) & ((1 << 28) - 1)), ((0x00759dc111d86ddfULL) >> 28), ((0x0012881e5780bb0fULL) & ((1 << 28) - 1)), ((0x0012881e5780bb0fULL) >> 28), ((0x00c8ccc83ad29496ULL) & ((1 << 28) - 1)), ((0x00c8ccc83ad29496ULL) >> 28), ((0x0012b9bd1929eb71ULL) & ((1 << 28) - 1)), ((0x0012b9bd1929eb71ULL) >> 28)}}


                                                                        },
            {{{((0x000fa15a20da5df0ULL) & ((1 << 28) - 1)), ((0x000fa15a20da5df0ULL) >> 28), ((0x00349ddb1a46cd31ULL) & ((1 << 28) - 1)), ((0x00349ddb1a46cd31ULL) >> 28), ((0x002c512ad1d8e726ULL) & ((1 << 28) - 1)), ((0x002c512ad1d8e726ULL) >> 28), ((0x00047611f669318dULL) & ((1 << 28) - 1)), ((0x00047611f669318dULL) >> 28), ((0x009e68fba591e17eULL) & ((1 << 28) - 1)), ((0x009e68fba591e17eULL) >> 28), ((0x004320dffa803906ULL) & ((1 << 28) - 1)), ((0x004320dffa803906ULL) >> 28), ((0x00a640874951a3d3ULL) & ((1 << 28) - 1)), ((0x00a640874951a3d3ULL) >> 28), ((0x00b6353478baa24fULL) & ((1 << 28) - 1)), ((0x00b6353478baa24fULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x009696510000d333ULL) & ((1 << 28) - 1)), ((0x009696510000d333ULL) >> 28), ((0x00ec2f788bc04826ULL) & ((1 << 28) - 1)), ((0x00ec2f788bc04826ULL) >> 28), ((0x000e4d02b1f67ba5ULL) & ((1 << 28) - 1)), ((0x000e4d02b1f67ba5ULL) >> 28), ((0x00659aa8dace08b6ULL) & ((1 << 28) - 1)), ((0x00659aa8dace08b6ULL) >> 28), ((0x00d7a38a3a3ae533ULL) & ((1 << 28) - 1)), ((0x00d7a38a3a3ae533ULL) >> 28), ((0x008856defa8c746bULL) & ((1 << 28) - 1)), ((0x008856defa8c746bULL) >> 28), ((0x004d7a4402d3da1aULL) & ((1 << 28) - 1)), ((0x004d7a4402d3da1aULL) >> 28), ((0x00ea82e06229260fULL) & ((1 << 28) - 1)), ((0x00ea82e06229260fULL) >> 28)}}


                                                                        },
            {{{((0x006a15bb20f75c0cULL) & ((1 << 28) - 1)), ((0x006a15bb20f75c0cULL) >> 28), ((0x0079a144027a5d0cULL) & ((1 << 28) - 1)), ((0x0079a144027a5d0cULL) >> 28), ((0x00d19116ce0b4d70ULL) & ((1 << 28) - 1)), ((0x00d19116ce0b4d70ULL) >> 28), ((0x0059b83bcb0b268eULL) & ((1 << 28) - 1)), ((0x0059b83bcb0b268eULL) >> 28), ((0x005f58f63f16c127ULL) & ((1 << 28) - 1)), ((0x005f58f63f16c127ULL) >> 28), ((0x0079958318ee2c37ULL) & ((1 << 28) - 1)), ((0x0079958318ee2c37ULL) >> 28), ((0x00defbb063d07f82ULL) & ((1 << 28) - 1)), ((0x00defbb063d07f82ULL) >> 28), ((0x00f1f0b931d2d446ULL) & ((1 << 28) - 1)), ((0x00f1f0b931d2d446ULL) >> 28)}}


                                                                        },
            {{{((0x00cb5e4c3c35d422ULL) & ((1 << 28) - 1)), ((0x00cb5e4c3c35d422ULL) >> 28), ((0x008df885ca43577fULL) & ((1 << 28) - 1)), ((0x008df885ca43577fULL) >> 28), ((0x00fa50b16ca3e471ULL) & ((1 << 28) - 1)), ((0x00fa50b16ca3e471ULL) >> 28), ((0x005a0e58e17488c8ULL) & ((1 << 28) - 1)), ((0x005a0e58e17488c8ULL) >> 28), ((0x00b2ceccd6d34d19ULL) & ((1 << 28) - 1)), ((0x00b2ceccd6d34d19ULL) >> 28), ((0x00f01d5d235e36e9ULL) & ((1 << 28) - 1)), ((0x00f01d5d235e36e9ULL) >> 28), ((0x00db2e7e4be6ca44ULL) & ((1 << 28) - 1)), ((0x00db2e7e4be6ca44ULL) >> 28), ((0x00260ab77f35fccdULL) & ((1 << 28) - 1)), ((0x00260ab77f35fccdULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x006f6fd9baac61d5ULL) & ((1 << 28) - 1)), ((0x006f6fd9baac61d5ULL) >> 28), ((0x002a7710a020a895ULL) & ((1 << 28) - 1)), ((0x002a7710a020a895ULL) >> 28), ((0x009de0db7fc03d4dULL) & ((1 << 28) - 1)), ((0x009de0db7fc03d4dULL) >> 28), ((0x00cdedcb1875f40bULL) & ((1 << 28) - 1)), ((0x00cdedcb1875f40bULL) >> 28), ((0x00050caf9b6b1e22ULL) & ((1 << 28) - 1)), ((0x00050caf9b6b1e22ULL) >> 28), ((0x005e3a6654456ab0ULL) & ((1 << 28) - 1)), ((0x005e3a6654456ab0ULL) >> 28), ((0x00775fdf8c4423d4ULL) & ((1 << 28) - 1)), ((0x00775fdf8c4423d4ULL) >> 28), ((0x0028701ea5738b5dULL) & ((1 << 28) - 1)), ((0x0028701ea5738b5dULL) >> 28)}}


                                                                        },
            {{{((0x009ffd90abfeae96ULL) & ((1 << 28) - 1)), ((0x009ffd90abfeae96ULL) >> 28), ((0x00cba3c2b624a516ULL) & ((1 << 28) - 1)), ((0x00cba3c2b624a516ULL) >> 28), ((0x005ef08bcee46c91ULL) & ((1 << 28) - 1)), ((0x005ef08bcee46c91ULL) >> 28), ((0x00e6fde30afb6185ULL) & ((1 << 28) - 1)), ((0x00e6fde30afb6185ULL) >> 28), ((0x00f0b4db4f818ce4ULL) & ((1 << 28) - 1)), ((0x00f0b4db4f818ce4ULL) >> 28), ((0x006c54f45d2127f5ULL) & ((1 << 28) - 1)), ((0x006c54f45d2127f5ULL) >> 28), ((0x00040125035854c7ULL) & ((1 << 28) - 1)), ((0x00040125035854c7ULL) >> 28), ((0x00372658a3287e13ULL) & ((1 << 28) - 1)), ((0x00372658a3287e13ULL) >> 28)}}


                                                                        },
            {{{((0x00d7070fb1beb2abULL) & ((1 << 28) - 1)), ((0x00d7070fb1beb2abULL) >> 28), ((0x0078fc845a93896bULL) & ((1 << 28) - 1)), ((0x0078fc845a93896bULL) >> 28), ((0x006894a4b2f224a6ULL) & ((1 << 28) - 1)), ((0x006894a4b2f224a6ULL) >> 28), ((0x005bdd8192b9dbdeULL) & ((1 << 28) - 1)), ((0x005bdd8192b9dbdeULL) >> 28), ((0x00b38839874b3a9eULL) & ((1 << 28) - 1)), ((0x00b38839874b3a9eULL) >> 28), ((0x00f93618b04b7a57ULL) & ((1 << 28) - 1)), ((0x00f93618b04b7a57ULL) >> 28), ((0x003e3ec75fd2c67eULL) & ((1 << 28) - 1)), ((0x003e3ec75fd2c67eULL) >> 28), ((0x00bf5e6bfc29494aULL) & ((1 << 28) - 1)), ((0x00bf5e6bfc29494aULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x00f19224ebba2aa5ULL) & ((1 << 28) - 1)), ((0x00f19224ebba2aa5ULL) >> 28), ((0x0074f89d358e694dULL) & ((1 << 28) - 1)), ((0x0074f89d358e694dULL) >> 28), ((0x00eea486597135adULL) & ((1 << 28) - 1)), ((0x00eea486597135adULL) >> 28), ((0x0081579a4555c7e1ULL) & ((1 << 28) - 1)), ((0x0081579a4555c7e1ULL) >> 28), ((0x0010b9b872930a9dULL) & ((1 << 28) - 1)), ((0x0010b9b872930a9dULL) >> 28), ((0x00f002e87a30ecc0ULL) & ((1 << 28) - 1)), ((0x00f002e87a30ecc0ULL) >> 28), ((0x009b9d66b6de56e2ULL) & ((1 << 28) - 1)), ((0x009b9d66b6de56e2ULL) >> 28), ((0x00a3c4f45e8004ebULL) & ((1 << 28) - 1)), ((0x00a3c4f45e8004ebULL) >> 28)}}


                                                                        },
            {{{((0x0045e8dda9400888ULL) & ((1 << 28) - 1)), ((0x0045e8dda9400888ULL) >> 28), ((0x002ff12e5fc05db7ULL) & ((1 << 28) - 1)), ((0x002ff12e5fc05db7ULL) >> 28), ((0x00a7098d54afe69cULL) & ((1 << 28) - 1)), ((0x00a7098d54afe69cULL) >> 28), ((0x00cdbe846a500585ULL) & ((1 << 28) - 1)), ((0x00cdbe846a500585ULL) >> 28), ((0x00879c1593ca1882ULL) & ((1 << 28) - 1)), ((0x00879c1593ca1882ULL) >> 28), ((0x003f7a7fea76c8b0ULL) & ((1 << 28) - 1)), ((0x003f7a7fea76c8b0ULL) >> 28), ((0x002cd73dd0c8e0a1ULL) & ((1 << 28) - 1)), ((0x002cd73dd0c8e0a1ULL) >> 28), ((0x00645d6ce96f51feULL) & ((1 << 28) - 1)), ((0x00645d6ce96f51feULL) >> 28)}}


                                                                        },
            {{{((0x002b7e83e123d6d6ULL) & ((1 << 28) - 1)), ((0x002b7e83e123d6d6ULL) >> 28), ((0x00398346f7419c80ULL) & ((1 << 28) - 1)), ((0x00398346f7419c80ULL) >> 28), ((0x0042922e55940163ULL) & ((1 << 28) - 1)), ((0x0042922e55940163ULL) >> 28), ((0x005e7fc5601886a3ULL) & ((1 << 28) - 1)), ((0x005e7fc5601886a3ULL) >> 28), ((0x00e88f2cee1d3103ULL) & ((1 << 28) - 1)), ((0x00e88f2cee1d3103ULL) >> 28), ((0x00e7fab135f2e377ULL) & ((1 << 28) - 1)), ((0x00e7fab135f2e377ULL) >> 28), ((0x00b059984dbf0dedULL) & ((1 << 28) - 1)), ((0x00b059984dbf0dedULL) >> 28), ((0x0009ce080faa5bb8ULL) & ((1 << 28) - 1)), ((0x0009ce080faa5bb8ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x0085e78af7758979ULL) & ((1 << 28) - 1)), ((0x0085e78af7758979ULL) >> 28), ((0x00275a4ee1631a3aULL) & ((1 << 28) - 1)), ((0x00275a4ee1631a3aULL) >> 28), ((0x00d26bc0ed78b683ULL) & ((1 << 28) - 1)), ((0x00d26bc0ed78b683ULL) >> 28), ((0x004f8355ea21064fULL) & ((1 << 28) - 1)), ((0x004f8355ea21064fULL) >> 28), ((0x00d618e1a32696e5ULL) & ((1 << 28) - 1)), ((0x00d618e1a32696e5ULL) >> 28), ((0x008d8d7b150e5680ULL) & ((1 << 28) - 1)), ((0x008d8d7b150e5680ULL) >> 28), ((0x00a74cd854b278d2ULL) & ((1 << 28) - 1)), ((0x00a74cd854b278d2ULL) >> 28), ((0x001dd62702203ea0ULL) & ((1 << 28) - 1)), ((0x001dd62702203ea0ULL) >> 28)}}


                                                                        },
            {{{((0x00f89335c2a59286ULL) & ((1 << 28) - 1)), ((0x00f89335c2a59286ULL) >> 28), ((0x00a0f5c905d55141ULL) & ((1 << 28) - 1)), ((0x00a0f5c905d55141ULL) >> 28), ((0x00b41fb836ee9382ULL) & ((1 << 28) - 1)), ((0x00b41fb836ee9382ULL) >> 28), ((0x00e235d51730ca43ULL) & ((1 << 28) - 1)), ((0x00e235d51730ca43ULL) >> 28), ((0x00a5cb37b5c0a69aULL) & ((1 << 28) - 1)), ((0x00a5cb37b5c0a69aULL) >> 28), ((0x009b966ffe136c45ULL) & ((1 << 28) - 1)), ((0x009b966ffe136c45ULL) >> 28), ((0x00cb2ea10bf80ed1ULL) & ((1 << 28) - 1)), ((0x00cb2ea10bf80ed1ULL) >> 28), ((0x00fb2b370b40dc35ULL) & ((1 << 28) - 1)), ((0x00fb2b370b40dc35ULL) >> 28)}}


                                                                        },
            {{{((0x00d687d16d4ee8baULL) & ((1 << 28) - 1)), ((0x00d687d16d4ee8baULL) >> 28), ((0x0071520bdd069dffULL) & ((1 << 28) - 1)), ((0x0071520bdd069dffULL) >> 28), ((0x00de85c60d32355dULL) & ((1 << 28) - 1)), ((0x00de85c60d32355dULL) >> 28), ((0x0087d2e3565102f4ULL) & ((1 << 28) - 1)), ((0x0087d2e3565102f4ULL) >> 28), ((0x00cde391b8dfc9aaULL) & ((1 << 28) - 1)), ((0x00cde391b8dfc9aaULL) >> 28), ((0x00e18d69efdfefe5ULL) & ((1 << 28) - 1)), ((0x00e18d69efdfefe5ULL) >> 28), ((0x004a9d0591954e91ULL) & ((1 << 28) - 1)), ((0x004a9d0591954e91ULL) >> 28), ((0x00fa36dd8b50eee5ULL) & ((1 << 28) - 1)), ((0x00fa36dd8b50eee5ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x002e788749a865f7ULL) & ((1 << 28) - 1)), ((0x002e788749a865f7ULL) >> 28), ((0x006e4dc3116861eaULL) & ((1 << 28) - 1)), ((0x006e4dc3116861eaULL) >> 28), ((0x009f1428c37276e6ULL) & ((1 << 28) - 1)), ((0x009f1428c37276e6ULL) >> 28), ((0x00e7d2e0fc1e1226ULL) & ((1 << 28) - 1)), ((0x00e7d2e0fc1e1226ULL) >> 28), ((0x003aeebc6b6c45f6ULL) & ((1 << 28) - 1)), ((0x003aeebc6b6c45f6ULL) >> 28), ((0x0071a8073bf500c9ULL) & ((1 << 28) - 1)), ((0x0071a8073bf500c9ULL) >> 28), ((0x004b22ad986b530cULL) & ((1 << 28) - 1)), ((0x004b22ad986b530cULL) >> 28), ((0x00f439e63c0d79d4ULL) & ((1 << 28) - 1)), ((0x00f439e63c0d79d4ULL) >> 28)}}


                                                                        },
            {{{((0x006bc3d53011f470ULL) & ((1 << 28) - 1)), ((0x006bc3d53011f470ULL) >> 28), ((0x00032d6e692b83e8ULL) & ((1 << 28) - 1)), ((0x00032d6e692b83e8ULL) >> 28), ((0x00059722f497cd0bULL) & ((1 << 28) - 1)), ((0x00059722f497cd0bULL) >> 28), ((0x0009b4e6f0c497ccULL) & ((1 << 28) - 1)), ((0x0009b4e6f0c497ccULL) >> 28), ((0x0058a804b7cce6c0ULL) & ((1 << 28) - 1)), ((0x0058a804b7cce6c0ULL) >> 28), ((0x002b71d3302bbd5dULL) & ((1 << 28) - 1)), ((0x002b71d3302bbd5dULL) >> 28), ((0x00e2f82a36765fceULL) & ((1 << 28) - 1)), ((0x00e2f82a36765fceULL) >> 28), ((0x008dded99524c703ULL) & ((1 << 28) - 1)), ((0x008dded99524c703ULL) >> 28)}}


                                                                        },
            {{{((0x004d058953747d64ULL) & ((1 << 28) - 1)), ((0x004d058953747d64ULL) >> 28), ((0x00701940fe79aa6fULL) & ((1 << 28) - 1)), ((0x00701940fe79aa6fULL) >> 28), ((0x00a620ac71c760bfULL) & ((1 << 28) - 1)), ((0x00a620ac71c760bfULL) >> 28), ((0x009532b611158b75ULL) & ((1 << 28) - 1)), ((0x009532b611158b75ULL) >> 28), ((0x00547ed7f466f300ULL) & ((1 << 28) - 1)), ((0x00547ed7f466f300ULL) >> 28), ((0x003cb5ab53a8401aULL) & ((1 << 28) - 1)), ((0x003cb5ab53a8401aULL) >> 28), ((0x00c7763168ce3120ULL) & ((1 << 28) - 1)), ((0x00c7763168ce3120ULL) >> 28), ((0x007e48e33e4b9ab2ULL) & ((1 << 28) - 1)), ((0x007e48e33e4b9ab2ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x001b2fc57bf3c738ULL) & ((1 << 28) - 1)), ((0x001b2fc57bf3c738ULL) >> 28), ((0x006a3f918993fb80ULL) & ((1 << 28) - 1)), ((0x006a3f918993fb80ULL) >> 28), ((0x0026f7a14fdec288ULL) & ((1 << 28) - 1)), ((0x0026f7a14fdec288ULL) >> 28), ((0x0075a2cdccef08dbULL) & ((1 << 28) - 1)), ((0x0075a2cdccef08dbULL) >> 28), ((0x00d3ecbc9eecdbf1ULL) & ((1 << 28) - 1)), ((0x00d3ecbc9eecdbf1ULL) >> 28), ((0x0048c40f06e5bf7fULL) & ((1 << 28) - 1)), ((0x0048c40f06e5bf7fULL) >> 28), ((0x00d63e423009896bULL) & ((1 << 28) - 1)), ((0x00d63e423009896bULL) >> 28), ((0x000598bc99c056a8ULL) & ((1 << 28) - 1)), ((0x000598bc99c056a8ULL) >> 28)}}


                                                                        },
            {{{((0x002f194eaafa46dcULL) & ((1 << 28) - 1)), ((0x002f194eaafa46dcULL) >> 28), ((0x008e38f57fe87613ULL) & ((1 << 28) - 1)), ((0x008e38f57fe87613ULL) >> 28), ((0x00dc8e5ae25f4ab2ULL) & ((1 << 28) - 1)), ((0x00dc8e5ae25f4ab2ULL) >> 28), ((0x000a17809575e6bdULL) & ((1 << 28) - 1)), ((0x000a17809575e6bdULL) >> 28), ((0x00d3ec7923ba366aULL) & ((1 << 28) - 1)), ((0x00d3ec7923ba366aULL) >> 28), ((0x003a7e72e0ad75e3ULL) & ((1 << 28) - 1)), ((0x003a7e72e0ad75e3ULL) >> 28), ((0x0010024b88436e0aULL) & ((1 << 28) - 1)), ((0x0010024b88436e0aULL) >> 28), ((0x00ed3c5444b64051ULL) & ((1 << 28) - 1)), ((0x00ed3c5444b64051ULL) >> 28)}}


                                                                        },
            {{{((0x00831fc1340af342ULL) & ((1 << 28) - 1)), ((0x00831fc1340af342ULL) >> 28), ((0x00c9645669466d35ULL) & ((1 << 28) - 1)), ((0x00c9645669466d35ULL) >> 28), ((0x007692b4cc5a080fULL) & ((1 << 28) - 1)), ((0x007692b4cc5a080fULL) >> 28), ((0x009fd4a47ac9259fULL) & ((1 << 28) - 1)), ((0x009fd4a47ac9259fULL) >> 28), ((0x001eeddf7d45928bULL) & ((1 << 28) - 1)), ((0x001eeddf7d45928bULL) >> 28), ((0x003c0446fc45f28bULL) & ((1 << 28) - 1)), ((0x003c0446fc45f28bULL) >> 28), ((0x002c0713aa3e2507ULL) & ((1 << 28) - 1)), ((0x002c0713aa3e2507ULL) >> 28), ((0x0095706935f0f41eULL) & ((1 << 28) - 1)), ((0x0095706935f0f41eULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x00766ae4190ec6d8ULL) & ((1 << 28) - 1)), ((0x00766ae4190ec6d8ULL) >> 28), ((0x0065768cabc71380ULL) & ((1 << 28) - 1)), ((0x0065768cabc71380ULL) >> 28), ((0x00b902598416cdc2ULL) & ((1 << 28) - 1)), ((0x00b902598416cdc2ULL) >> 28), ((0x00380021ad38df52ULL) & ((1 << 28) - 1)), ((0x00380021ad38df52ULL) >> 28), ((0x008f0b89d6551134ULL) & ((1 << 28) - 1)), ((0x008f0b89d6551134ULL) >> 28), ((0x004254d4cc62c5a5ULL) & ((1 << 28) - 1)), ((0x004254d4cc62c5a5ULL) >> 28), ((0x000d79f4484b9b94ULL) & ((1 << 28) - 1)), ((0x000d79f4484b9b94ULL) >> 28), ((0x00b516732ae3c50eULL) & ((1 << 28) - 1)), ((0x00b516732ae3c50eULL) >> 28)}}


                                                                        },
            {{{((0x001fb73475c45509ULL) & ((1 << 28) - 1)), ((0x001fb73475c45509ULL) >> 28), ((0x00d2b2e5ea43345aULL) & ((1 << 28) - 1)), ((0x00d2b2e5ea43345aULL) >> 28), ((0x00cb3c3842077bd1ULL) & ((1 << 28) - 1)), ((0x00cb3c3842077bd1ULL) >> 28), ((0x0029f90ad820946eULL) & ((1 << 28) - 1)), ((0x0029f90ad820946eULL) >> 28), ((0x007c11b2380778aaULL) & ((1 << 28) - 1)), ((0x007c11b2380778aaULL) >> 28), ((0x009e54ece62c1704ULL) & ((1 << 28) - 1)), ((0x009e54ece62c1704ULL) >> 28), ((0x004bc60c41ca01c3ULL) & ((1 << 28) - 1)), ((0x004bc60c41ca01c3ULL) >> 28), ((0x004525679a5a0b03ULL) & ((1 << 28) - 1)), ((0x004525679a5a0b03ULL) >> 28)}}


                                                                        },
            {{{((0x00c64fbddbed87b3ULL) & ((1 << 28) - 1)), ((0x00c64fbddbed87b3ULL) >> 28), ((0x0040601d11731faaULL) & ((1 << 28) - 1)), ((0x0040601d11731faaULL) >> 28), ((0x009c22475b6f9d67ULL) & ((1 << 28) - 1)), ((0x009c22475b6f9d67ULL) >> 28), ((0x0024b79dae875f15ULL) & ((1 << 28) - 1)), ((0x0024b79dae875f15ULL) >> 28), ((0x00616fed3f02c3b0ULL) & ((1 << 28) - 1)), ((0x00616fed3f02c3b0ULL) >> 28), ((0x0000cf39f6af2d3bULL) & ((1 << 28) - 1)), ((0x0000cf39f6af2d3bULL) >> 28), ((0x00c46bac0aa9a688ULL) & ((1 << 28) - 1)), ((0x00c46bac0aa9a688ULL) >> 28), ((0x00ab23e2800da204ULL) & ((1 << 28) - 1)), ((0x00ab23e2800da204ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x000b3a37617632b0ULL) & ((1 << 28) - 1)), ((0x000b3a37617632b0ULL) >> 28), ((0x00597199fe1cfb6cULL) & ((1 << 28) - 1)), ((0x00597199fe1cfb6cULL) >> 28), ((0x0042a7ccdfeafdd6ULL) & ((1 << 28) - 1)), ((0x0042a7ccdfeafdd6ULL) >> 28), ((0x004cc9f15ebcea17ULL) & ((1 << 28) - 1)), ((0x004cc9f15ebcea17ULL) >> 28), ((0x00f436e596a6b4a4ULL) & ((1 << 28) - 1)), ((0x00f436e596a6b4a4ULL) >> 28), ((0x00168861142df0d8ULL) & ((1 << 28) - 1)), ((0x00168861142df0d8ULL) >> 28), ((0x000753edfec26af5ULL) & ((1 << 28) - 1)), ((0x000753edfec26af5ULL) >> 28), ((0x000c495d7e388116ULL) & ((1 << 28) - 1)), ((0x000c495d7e388116ULL) >> 28)}}


                                                                        },
            {{{((0x0017085f4a346148ULL) & ((1 << 28) - 1)), ((0x0017085f4a346148ULL) >> 28), ((0x00c7cf7a37f62272ULL) & ((1 << 28) - 1)), ((0x00c7cf7a37f62272ULL) >> 28), ((0x001776e129bc5c30ULL) & ((1 << 28) - 1)), ((0x001776e129bc5c30ULL) >> 28), ((0x009955134c9eef2aULL) & ((1 << 28) - 1)), ((0x009955134c9eef2aULL) >> 28), ((0x001ba5bdf1df07beULL) & ((1 << 28) - 1)), ((0x001ba5bdf1df07beULL) >> 28), ((0x00ec39497103a55cULL) & ((1 << 28) - 1)), ((0x00ec39497103a55cULL) >> 28), ((0x006578354fda6cfbULL) & ((1 << 28) - 1)), ((0x006578354fda6cfbULL) >> 28), ((0x005f02719d4f15eeULL) & ((1 << 28) - 1)), ((0x005f02719d4f15eeULL) >> 28)}}


                                                                        },
            {{{((0x0052b9d9b5d9655dULL) & ((1 << 28) - 1)), ((0x0052b9d9b5d9655dULL) >> 28), ((0x00d4ec7ba1b461c3ULL) & ((1 << 28) - 1)), ((0x00d4ec7ba1b461c3ULL) >> 28), ((0x00f95df4974f280bULL) & ((1 << 28) - 1)), ((0x00f95df4974f280bULL) >> 28), ((0x003d8e5ca11aeb51ULL) & ((1 << 28) - 1)), ((0x003d8e5ca11aeb51ULL) >> 28), ((0x00d4981eb5a70b26ULL) & ((1 << 28) - 1)), ((0x00d4981eb5a70b26ULL) >> 28), ((0x000af9a4f6659f29ULL) & ((1 << 28) - 1)), ((0x000af9a4f6659f29ULL) >> 28), ((0x004598c846faeb43ULL) & ((1 << 28) - 1)), ((0x004598c846faeb43ULL) >> 28), ((0x0049d9a183a47670ULL) & ((1 << 28) - 1)), ((0x0049d9a183a47670ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x000a72d23dcb3f1fULL) & ((1 << 28) - 1)), ((0x000a72d23dcb3f1fULL) >> 28), ((0x00a3737f84011727ULL) & ((1 << 28) - 1)), ((0x00a3737f84011727ULL) >> 28), ((0x00f870c0fbbf4a47ULL) & ((1 << 28) - 1)), ((0x00f870c0fbbf4a47ULL) >> 28), ((0x00a7aadd04b5c9caULL) & ((1 << 28) - 1)), ((0x00a7aadd04b5c9caULL) >> 28), ((0x000c7715c67bd072ULL) & ((1 << 28) - 1)), ((0x000c7715c67bd072ULL) >> 28), ((0x00015a136afcd74eULL) & ((1 << 28) - 1)), ((0x00015a136afcd74eULL) >> 28), ((0x0080d5caea499634ULL) & ((1 << 28) - 1)), ((0x0080d5caea499634ULL) >> 28), ((0x0026b448ec7514b7ULL) & ((1 << 28) - 1)), ((0x0026b448ec7514b7ULL) >> 28)}}


                                                                        },
            {{{((0x00b60167d9e7d065ULL) & ((1 << 28) - 1)), ((0x00b60167d9e7d065ULL) >> 28), ((0x00e60ba0d07381e8ULL) & ((1 << 28) - 1)), ((0x00e60ba0d07381e8ULL) >> 28), ((0x003a4f17b725c2d4ULL) & ((1 << 28) - 1)), ((0x003a4f17b725c2d4ULL) >> 28), ((0x006c19fe176b64faULL) & ((1 << 28) - 1)), ((0x006c19fe176b64faULL) >> 28), ((0x003b57b31af86ccbULL) & ((1 << 28) - 1)), ((0x003b57b31af86ccbULL) >> 28), ((0x0021047c286180fdULL) & ((1 << 28) - 1)), ((0x0021047c286180fdULL) >> 28), ((0x00bdc8fb00c6dbb6ULL) & ((1 << 28) - 1)), ((0x00bdc8fb00c6dbb6ULL) >> 28), ((0x00fe4a9f4bab4f3fULL) & ((1 << 28) - 1)), ((0x00fe4a9f4bab4f3fULL) >> 28)}}


                                                                        },
            {{{((0x0088ffc3a16111f7ULL) & ((1 << 28) - 1)), ((0x0088ffc3a16111f7ULL) >> 28), ((0x009155e4245d0bc8ULL) & ((1 << 28) - 1)), ((0x009155e4245d0bc8ULL) >> 28), ((0x00851d68220572d5ULL) & ((1 << 28) - 1)), ((0x00851d68220572d5ULL) >> 28), ((0x00557ace1e514d29ULL) & ((1 << 28) - 1)), ((0x00557ace1e514d29ULL) >> 28), ((0x0031d7c339d91022ULL) & ((1 << 28) - 1)), ((0x0031d7c339d91022ULL) >> 28), ((0x00101d0ae2eaceeaULL) & ((1 << 28) - 1)), ((0x00101d0ae2eaceeaULL) >> 28), ((0x00246ab3f837b66aULL) & ((1 << 28) - 1)), ((0x00246ab3f837b66aULL) >> 28), ((0x00d5216d381ff530ULL) & ((1 << 28) - 1)), ((0x00d5216d381ff530ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x0057e7ea35f36daeULL) & ((1 << 28) - 1)), ((0x0057e7ea35f36daeULL) >> 28), ((0x00f47d7ad15de22eULL) & ((1 << 28) - 1)), ((0x00f47d7ad15de22eULL) >> 28), ((0x00d757ea4b105115ULL) & ((1 << 28) - 1)), ((0x00d757ea4b105115ULL) >> 28), ((0x008311457d579d7eULL) & ((1 << 28) - 1)), ((0x008311457d579d7eULL) >> 28), ((0x00b49b75b1edd4ebULL) & ((1 << 28) - 1)), ((0x00b49b75b1edd4ebULL) >> 28), ((0x0081c7ff742fd63aULL) & ((1 << 28) - 1)), ((0x0081c7ff742fd63aULL) >> 28), ((0x00ddda3187433df6ULL) & ((1 << 28) - 1)), ((0x00ddda3187433df6ULL) >> 28), ((0x00475727d55f9c66ULL) & ((1 << 28) - 1)), ((0x00475727d55f9c66ULL) >> 28)}}


                                                                        },
            {{{((0x00a6295218dc136aULL) & ((1 << 28) - 1)), ((0x00a6295218dc136aULL) >> 28), ((0x00563b3af0e9c012ULL) & ((1 << 28) - 1)), ((0x00563b3af0e9c012ULL) >> 28), ((0x00d3753b0145db1bULL) & ((1 << 28) - 1)), ((0x00d3753b0145db1bULL) >> 28), ((0x004550389c043dc1ULL) & ((1 << 28) - 1)), ((0x004550389c043dc1ULL) >> 28), ((0x00ea94ae27401bdfULL) & ((1 << 28) - 1)), ((0x00ea94ae27401bdfULL) >> 28), ((0x002b0b949f2b7956ULL) & ((1 << 28) - 1)), ((0x002b0b949f2b7956ULL) >> 28), ((0x00c63f780ad8e23cULL) & ((1 << 28) - 1)), ((0x00c63f780ad8e23cULL) >> 28), ((0x00e591c47d6bab15ULL) & ((1 << 28) - 1)), ((0x00e591c47d6bab15ULL) >> 28)}}


                                                                        },
            {{{((0x00416c582b058eb6ULL) & ((1 << 28) - 1)), ((0x00416c582b058eb6ULL) >> 28), ((0x004107da5b2cc695ULL) & ((1 << 28) - 1)), ((0x004107da5b2cc695ULL) >> 28), ((0x00b3cd2556aeec64ULL) & ((1 << 28) - 1)), ((0x00b3cd2556aeec64ULL) >> 28), ((0x00c0b418267e57a1ULL) & ((1 << 28) - 1)), ((0x00c0b418267e57a1ULL) >> 28), ((0x001799293579bd2eULL) & ((1 << 28) - 1)), ((0x001799293579bd2eULL) >> 28), ((0x0046ed44590e4d07ULL) & ((1 << 28) - 1)), ((0x0046ed44590e4d07ULL) >> 28), ((0x001d7459b3630a1eULL) & ((1 << 28) - 1)), ((0x001d7459b3630a1eULL) >> 28), ((0x00c6afba8b6696aaULL) & ((1 << 28) - 1)), ((0x00c6afba8b6696aaULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x008d6009b26da3f8ULL) & ((1 << 28) - 1)), ((0x008d6009b26da3f8ULL) >> 28), ((0x00898e88ca06b1caULL) & ((1 << 28) - 1)), ((0x00898e88ca06b1caULL) >> 28), ((0x00edb22b2ed7fe62ULL) & ((1 << 28) - 1)), ((0x00edb22b2ed7fe62ULL) >> 28), ((0x00fbc93516aabe80ULL) & ((1 << 28) - 1)), ((0x00fbc93516aabe80ULL) >> 28), ((0x008b4b470c42ce0dULL) & ((1 << 28) - 1)), ((0x008b4b470c42ce0dULL) >> 28), ((0x00e0032ba7d0dcbbULL) & ((1 << 28) - 1)), ((0x00e0032ba7d0dcbbULL) >> 28), ((0x00d76da3a956ecc8ULL) & ((1 << 28) - 1)), ((0x00d76da3a956ecc8ULL) >> 28), ((0x007f20fe74e3852aULL) & ((1 << 28) - 1)), ((0x007f20fe74e3852aULL) >> 28)}}


                                                                        },
            {{{((0x002419222c607674ULL) & ((1 << 28) - 1)), ((0x002419222c607674ULL) >> 28), ((0x00a7f23af89188b3ULL) & ((1 << 28) - 1)), ((0x00a7f23af89188b3ULL) >> 28), ((0x00ad127284e73d1cULL) & ((1 << 28) - 1)), ((0x00ad127284e73d1cULL) >> 28), ((0x008bba582fae1c51ULL) & ((1 << 28) - 1)), ((0x008bba582fae1c51ULL) >> 28), ((0x00fc6aa7ca9ecab1ULL) & ((1 << 28) - 1)), ((0x00fc6aa7ca9ecab1ULL) >> 28), ((0x003df5319eb6c2baULL) & ((1 << 28) - 1)), ((0x003df5319eb6c2baULL) >> 28), ((0x002a05af8a8b199aULL) & ((1 << 28) - 1)), ((0x002a05af8a8b199aULL) >> 28), ((0x004bf8354558407cULL) & ((1 << 28) - 1)), ((0x004bf8354558407cULL) >> 28)}}


                                                                        },
            {{{((0x00ce7d4a30f0fcbfULL) & ((1 << 28) - 1)), ((0x00ce7d4a30f0fcbfULL) >> 28), ((0x00d02c272629f03dULL) & ((1 << 28) - 1)), ((0x00d02c272629f03dULL) >> 28), ((0x0048c001f7400bc2ULL) & ((1 << 28) - 1)), ((0x0048c001f7400bc2ULL) >> 28), ((0x002c21368011958dULL) & ((1 << 28) - 1)), ((0x002c21368011958dULL) >> 28), ((0x0098a550391e96b5ULL) & ((1 << 28) - 1)), ((0x0098a550391e96b5ULL) >> 28), ((0x002d80b66390f379ULL) & ((1 << 28) - 1)), ((0x002d80b66390f379ULL) >> 28), ((0x001fa878760cc785ULL) & ((1 << 28) - 1)), ((0x001fa878760cc785ULL) >> 28), ((0x001adfce54b613d5ULL) & ((1 << 28) - 1)), ((0x001adfce54b613d5ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x001ed4dc71fa2523ULL) & ((1 << 28) - 1)), ((0x001ed4dc71fa2523ULL) >> 28), ((0x005d0bff19bf9b5cULL) & ((1 << 28) - 1)), ((0x005d0bff19bf9b5cULL) >> 28), ((0x00c3801cee065a64ULL) & ((1 << 28) - 1)), ((0x00c3801cee065a64ULL) >> 28), ((0x001ed0b504323fbfULL) & ((1 << 28) - 1)), ((0x001ed0b504323fbfULL) >> 28), ((0x0003ab9fdcbbc593ULL) & ((1 << 28) - 1)), ((0x0003ab9fdcbbc593ULL) >> 28), ((0x00df82070178b8d2ULL) & ((1 << 28) - 1)), ((0x00df82070178b8d2ULL) >> 28), ((0x00a2bcaa9c251f85ULL) & ((1 << 28) - 1)), ((0x00a2bcaa9c251f85ULL) >> 28), ((0x00c628a3674bd02eULL) & ((1 << 28) - 1)), ((0x00c628a3674bd02eULL) >> 28)}}


                                                                        },
            {{{((0x006b7a0674f9f8deULL) & ((1 << 28) - 1)), ((0x006b7a0674f9f8deULL) >> 28), ((0x00a742414e5c7cffULL) & ((1 << 28) - 1)), ((0x00a742414e5c7cffULL) >> 28), ((0x0041cbf3c6e13221ULL) & ((1 << 28) - 1)), ((0x0041cbf3c6e13221ULL) >> 28), ((0x00e3a64fd207af24ULL) & ((1 << 28) - 1)), ((0x00e3a64fd207af24ULL) >> 28), ((0x0087c05f15fbe8d1ULL) & ((1 << 28) - 1)), ((0x0087c05f15fbe8d1ULL) >> 28), ((0x004c50936d9e8a33ULL) & ((1 << 28) - 1)), ((0x004c50936d9e8a33ULL) >> 28), ((0x001306ec21042b6dULL) & ((1 << 28) - 1)), ((0x001306ec21042b6dULL) >> 28), ((0x00a4f4137d1141c2ULL) & ((1 << 28) - 1)), ((0x00a4f4137d1141c2ULL) >> 28)}}


                                                                        },
            {{{((0x0009e6fb921568b0ULL) & ((1 << 28) - 1)), ((0x0009e6fb921568b0ULL) >> 28), ((0x00b3c60120219118ULL) & ((1 << 28) - 1)), ((0x00b3c60120219118ULL) >> 28), ((0x002a6c3460dd503aULL) & ((1 << 28) - 1)), ((0x002a6c3460dd503aULL) >> 28), ((0x009db1ef11654b54ULL) & ((1 << 28) - 1)), ((0x009db1ef11654b54ULL) >> 28), ((0x0063e4bf0be79601ULL) & ((1 << 28) - 1)), ((0x0063e4bf0be79601ULL) >> 28), ((0x00670d34bb2592b9ULL) & ((1 << 28) - 1)), ((0x00670d34bb2592b9ULL) >> 28), ((0x00dcee2f6c4130ceULL) & ((1 << 28) - 1)), ((0x00dcee2f6c4130ceULL) >> 28), ((0x00b2682e88e77f54ULL) & ((1 << 28) - 1)), ((0x00b2682e88e77f54ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x000d5b4b3da135abULL) & ((1 << 28) - 1)), ((0x000d5b4b3da135abULL) >> 28), ((0x00838f3e5064d81dULL) & ((1 << 28) - 1)), ((0x00838f3e5064d81dULL) >> 28), ((0x00d44eb50f6d94edULL) & ((1 << 28) - 1)), ((0x00d44eb50f6d94edULL) >> 28), ((0x0008931ab502ac6dULL) & ((1 << 28) - 1)), ((0x0008931ab502ac6dULL) >> 28), ((0x00debe01ca3d3586ULL) & ((1 << 28) - 1)), ((0x00debe01ca3d3586ULL) >> 28), ((0x0025c206775f0641ULL) & ((1 << 28) - 1)), ((0x0025c206775f0641ULL) >> 28), ((0x005ad4b6ae912763ULL) & ((1 << 28) - 1)), ((0x005ad4b6ae912763ULL) >> 28), ((0x007e2c318ad8f247ULL) & ((1 << 28) - 1)), ((0x007e2c318ad8f247ULL) >> 28)}}


                                                                        },
            {{{((0x00ddbe0750dd1addULL) & ((1 << 28) - 1)), ((0x00ddbe0750dd1addULL) >> 28), ((0x004b3c7b885844b8ULL) & ((1 << 28) - 1)), ((0x004b3c7b885844b8ULL) >> 28), ((0x00363e7ecf12f1aeULL) & ((1 << 28) - 1)), ((0x00363e7ecf12f1aeULL) >> 28), ((0x0062e953e6438f9dULL) & ((1 << 28) - 1)), ((0x0062e953e6438f9dULL) >> 28), ((0x0023cc73b076afe9ULL) & ((1 << 28) - 1)), ((0x0023cc73b076afe9ULL) >> 28), ((0x00b09fa083b4da32ULL) & ((1 << 28) - 1)), ((0x00b09fa083b4da32ULL) >> 28), ((0x00c7c3d2456c541dULL) & ((1 << 28) - 1)), ((0x00c7c3d2456c541dULL) >> 28), ((0x005b591ec6b694d4ULL) & ((1 << 28) - 1)), ((0x005b591ec6b694d4ULL) >> 28)}}


                                                                        },
            {{{((0x0028656e19d62fcfULL) & ((1 << 28) - 1)), ((0x0028656e19d62fcfULL) >> 28), ((0x0052a4af03df148dULL) & ((1 << 28) - 1)), ((0x0052a4af03df148dULL) >> 28), ((0x00122765ddd14e42ULL) & ((1 << 28) - 1)), ((0x00122765ddd14e42ULL) >> 28), ((0x00f2252904f67157ULL) & ((1 << 28) - 1)), ((0x00f2252904f67157ULL) >> 28), ((0x004741965b636f3aULL) & ((1 << 28) - 1)), ((0x004741965b636f3aULL) >> 28), ((0x006441d296132cb9ULL) & ((1 << 28) - 1)), ((0x006441d296132cb9ULL) >> 28), ((0x005e2106f956a5b7ULL) & ((1 << 28) - 1)), ((0x005e2106f956a5b7ULL) >> 28), ((0x00247029592d335cULL) & ((1 << 28) - 1)), ((0x00247029592d335cULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x003fe038eb92f894ULL) & ((1 << 28) - 1)), ((0x003fe038eb92f894ULL) >> 28), ((0x000e6da1b72e8e32ULL) & ((1 << 28) - 1)), ((0x000e6da1b72e8e32ULL) >> 28), ((0x003a1411bfcbe0faULL) & ((1 << 28) - 1)), ((0x003a1411bfcbe0faULL) >> 28), ((0x00b55d473164a9e4ULL) & ((1 << 28) - 1)), ((0x00b55d473164a9e4ULL) >> 28), ((0x00b9a775ac2df48dULL) & ((1 << 28) - 1)), ((0x00b9a775ac2df48dULL) >> 28), ((0x0002ddf350659e21ULL) & ((1 << 28) - 1)), ((0x0002ddf350659e21ULL) >> 28), ((0x00a279a69eb19cb3ULL) & ((1 << 28) - 1)), ((0x00a279a69eb19cb3ULL) >> 28), ((0x00f844eab25cba44ULL) & ((1 << 28) - 1)), ((0x00f844eab25cba44ULL) >> 28)}}


                                                                        },
            {{{((0x00c41d1f9c1f1ac1ULL) & ((1 << 28) - 1)), ((0x00c41d1f9c1f1ac1ULL) >> 28), ((0x007b2df4e9f19146ULL) & ((1 << 28) - 1)), ((0x007b2df4e9f19146ULL) >> 28), ((0x00b469355fd5ba7aULL) & ((1 << 28) - 1)), ((0x00b469355fd5ba7aULL) >> 28), ((0x00b5e1965afc852aULL) & ((1 << 28) - 1)), ((0x00b5e1965afc852aULL) >> 28), ((0x00388d5f1e2d8217ULL) & ((1 << 28) - 1)), ((0x00388d5f1e2d8217ULL) >> 28), ((0x0022079e4c09ae93ULL) & ((1 << 28) - 1)), ((0x0022079e4c09ae93ULL) >> 28), ((0x0014268acd4ef518ULL) & ((1 << 28) - 1)), ((0x0014268acd4ef518ULL) >> 28), ((0x00c1dd8d9640464cULL) & ((1 << 28) - 1)), ((0x00c1dd8d9640464cULL) >> 28)}}


                                                                        },
            {{{((0x0038526adeed0c55ULL) & ((1 << 28) - 1)), ((0x0038526adeed0c55ULL) >> 28), ((0x00dd68c607e3fe85ULL) & ((1 << 28) - 1)), ((0x00dd68c607e3fe85ULL) >> 28), ((0x00f746ddd48a5d57ULL) & ((1 << 28) - 1)), ((0x00f746ddd48a5d57ULL) >> 28), ((0x0042f2952b963b7cULL) & ((1 << 28) - 1)), ((0x0042f2952b963b7cULL) >> 28), ((0x001cbbd6876d5ec2ULL) & ((1 << 28) - 1)), ((0x001cbbd6876d5ec2ULL) >> 28), ((0x005e341470bca5c2ULL) & ((1 << 28) - 1)), ((0x005e341470bca5c2ULL) >> 28), ((0x00871d41e085f413ULL) & ((1 << 28) - 1)), ((0x00871d41e085f413ULL) >> 28), ((0x00e53ab098f45732ULL) & ((1 << 28) - 1)), ((0x00e53ab098f45732ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x004d51124797c831ULL) & ((1 << 28) - 1)), ((0x004d51124797c831ULL) >> 28), ((0x008f5ae3750347adULL) & ((1 << 28) - 1)), ((0x008f5ae3750347adULL) >> 28), ((0x0070ced94c1a0c8eULL) & ((1 << 28) - 1)), ((0x0070ced94c1a0c8eULL) >> 28), ((0x00f6db2043898e64ULL) & ((1 << 28) - 1)), ((0x00f6db2043898e64ULL) >> 28), ((0x000d00c9a5750cd0ULL) & ((1 << 28) - 1)), ((0x000d00c9a5750cd0ULL) >> 28), ((0x000741ec59bad712ULL) & ((1 << 28) - 1)), ((0x000741ec59bad712ULL) >> 28), ((0x003c9d11aab37b7fULL) & ((1 << 28) - 1)), ((0x003c9d11aab37b7fULL) >> 28), ((0x00a67ba169807714ULL) & ((1 << 28) - 1)), ((0x00a67ba169807714ULL) >> 28)}}


                                                                        },
            {{{((0x00adb2c1566e8b8fULL) & ((1 << 28) - 1)), ((0x00adb2c1566e8b8fULL) >> 28), ((0x0096c68a35771a9aULL) & ((1 << 28) - 1)), ((0x0096c68a35771a9aULL) >> 28), ((0x00869933356f334aULL) & ((1 << 28) - 1)), ((0x00869933356f334aULL) >> 28), ((0x00ba9c93459f5962ULL) & ((1 << 28) - 1)), ((0x00ba9c93459f5962ULL) >> 28), ((0x009ec73fb6e8ca4bULL) & ((1 << 28) - 1)), ((0x009ec73fb6e8ca4bULL) >> 28), ((0x003c3802c27202e1ULL) & ((1 << 28) - 1)), ((0x003c3802c27202e1ULL) >> 28), ((0x0031f5b733e0c008ULL) & ((1 << 28) - 1)), ((0x0031f5b733e0c008ULL) >> 28), ((0x00f9058c19611fa9ULL) & ((1 << 28) - 1)), ((0x00f9058c19611fa9ULL) >> 28)}}


                                                                        },
            {{{((0x00238f01814a3421ULL) & ((1 << 28) - 1)), ((0x00238f01814a3421ULL) >> 28), ((0x00c325a44b6cce28ULL) & ((1 << 28) - 1)), ((0x00c325a44b6cce28ULL) >> 28), ((0x002136f97aeb0e73ULL) & ((1 << 28) - 1)), ((0x002136f97aeb0e73ULL) >> 28), ((0x000cac8268a4afe2ULL) & ((1 << 28) - 1)), ((0x000cac8268a4afe2ULL) >> 28), ((0x0022fd218da471b3ULL) & ((1 << 28) - 1)), ((0x0022fd218da471b3ULL) >> 28), ((0x009dcd8dfff8def9ULL) & ((1 << 28) - 1)), ((0x009dcd8dfff8def9ULL) >> 28), ((0x00cb9f8181d999bbULL) & ((1 << 28) - 1)), ((0x00cb9f8181d999bbULL) >> 28), ((0x00143ae56edea349ULL) & ((1 << 28) - 1)), ((0x00143ae56edea349ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x0000623bf87622c5ULL) & ((1 << 28) - 1)), ((0x0000623bf87622c5ULL) >> 28), ((0x00a1966fdd069496ULL) & ((1 << 28) - 1)), ((0x00a1966fdd069496ULL) >> 28), ((0x00c315b7b812f9fcULL) & ((1 << 28) - 1)), ((0x00c315b7b812f9fcULL) >> 28), ((0x00bdf5efcd128b97ULL) & ((1 << 28) - 1)), ((0x00bdf5efcd128b97ULL) >> 28), ((0x001d464f532e3e16ULL) & ((1 << 28) - 1)), ((0x001d464f532e3e16ULL) >> 28), ((0x003cd94f081bfd7eULL) & ((1 << 28) - 1)), ((0x003cd94f081bfd7eULL) >> 28), ((0x00ed9dae12ce4009ULL) & ((1 << 28) - 1)), ((0x00ed9dae12ce4009ULL) >> 28), ((0x002756f5736eee70ULL) & ((1 << 28) - 1)), ((0x002756f5736eee70ULL) >> 28)}}


                                                                        },
            {{{((0x00a5187e6ee7341bULL) & ((1 << 28) - 1)), ((0x00a5187e6ee7341bULL) >> 28), ((0x00e6d52e82d83b6eULL) & ((1 << 28) - 1)), ((0x00e6d52e82d83b6eULL) >> 28), ((0x00df3c41323094a7ULL) & ((1 << 28) - 1)), ((0x00df3c41323094a7ULL) >> 28), ((0x00b3324f444e9de9ULL) & ((1 << 28) - 1)), ((0x00b3324f444e9de9ULL) >> 28), ((0x00689eb21a35bfe5ULL) & ((1 << 28) - 1)), ((0x00689eb21a35bfe5ULL) >> 28), ((0x00f16363becd548dULL) & ((1 << 28) - 1)), ((0x00f16363becd548dULL) >> 28), ((0x00e187cc98e7f60fULL) & ((1 << 28) - 1)), ((0x00e187cc98e7f60fULL) >> 28), ((0x00127d9062f0ccabULL) & ((1 << 28) - 1)), ((0x00127d9062f0ccabULL) >> 28)}}


                                                                        },
            {{{((0x004ad71b31c29e40ULL) & ((1 << 28) - 1)), ((0x004ad71b31c29e40ULL) >> 28), ((0x00a5fcace12fae29ULL) & ((1 << 28) - 1)), ((0x00a5fcace12fae29ULL) >> 28), ((0x004425b5597280edULL) & ((1 << 28) - 1)), ((0x004425b5597280edULL) >> 28), ((0x00e7ef5d716c3346ULL) & ((1 << 28) - 1)), ((0x00e7ef5d716c3346ULL) >> 28), ((0x0010b53ada410ac8ULL) & ((1 << 28) - 1)), ((0x0010b53ada410ac8ULL) >> 28), ((0x0092310226060c9bULL) & ((1 << 28) - 1)), ((0x0092310226060c9bULL) >> 28), ((0x0091c26128729c7eULL) & ((1 << 28) - 1)), ((0x0091c26128729c7eULL) >> 28), ((0x0088b42900f8ec3bULL) & ((1 << 28) - 1)), ((0x0088b42900f8ec3bULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x00f1e26e9762d4a8ULL) & ((1 << 28) - 1)), ((0x00f1e26e9762d4a8ULL) >> 28), ((0x00d9d74082183414ULL) & ((1 << 28) - 1)), ((0x00d9d74082183414ULL) >> 28), ((0x00ffec9bd57a0282ULL) & ((1 << 28) - 1)), ((0x00ffec9bd57a0282ULL) >> 28), ((0x000919e128fd497aULL) & ((1 << 28) - 1)), ((0x000919e128fd497aULL) >> 28), ((0x00ab7ae7d00fe5f8ULL) & ((1 << 28) - 1)), ((0x00ab7ae7d00fe5f8ULL) >> 28), ((0x0054dc442851ff68ULL) & ((1 << 28) - 1)), ((0x0054dc442851ff68ULL) >> 28), ((0x00c9ebeb3b861687ULL) & ((1 << 28) - 1)), ((0x00c9ebeb3b861687ULL) >> 28), ((0x00507f7cab8b698fULL) & ((1 << 28) - 1)), ((0x00507f7cab8b698fULL) >> 28)}}


                                                                        },
            {{{((0x00c13c5aae3ae341ULL) & ((1 << 28) - 1)), ((0x00c13c5aae3ae341ULL) >> 28), ((0x009c6c9ed98373e7ULL) & ((1 << 28) - 1)), ((0x009c6c9ed98373e7ULL) >> 28), ((0x00098f26864577a8ULL) & ((1 << 28) - 1)), ((0x00098f26864577a8ULL) >> 28), ((0x0015b886e9488b45ULL) & ((1 << 28) - 1)), ((0x0015b886e9488b45ULL) >> 28), ((0x0037692c42aadba5ULL) & ((1 << 28) - 1)), ((0x0037692c42aadba5ULL) >> 28), ((0x00b83170b8e7791cULL) & ((1 << 28) - 1)), ((0x00b83170b8e7791cULL) >> 28), ((0x001670952ece1b44ULL) & ((1 << 28) - 1)), ((0x001670952ece1b44ULL) >> 28), ((0x00fd932a39276da2ULL) & ((1 << 28) - 1)), ((0x00fd932a39276da2ULL) >> 28)}}


                                                                        },
            {{{((0x0081a3259bef3398ULL) & ((1 << 28) - 1)), ((0x0081a3259bef3398ULL) >> 28), ((0x005480fff416107bULL) & ((1 << 28) - 1)), ((0x005480fff416107bULL) >> 28), ((0x00ce4f607d21be98ULL) & ((1 << 28) - 1)), ((0x00ce4f607d21be98ULL) >> 28), ((0x003ffc084b41df9bULL) & ((1 << 28) - 1)), ((0x003ffc084b41df9bULL) >> 28), ((0x0043d0bb100502d1ULL) & ((1 << 28) - 1)), ((0x0043d0bb100502d1ULL) >> 28), ((0x00ec35f575ba3261ULL) & ((1 << 28) - 1)), ((0x00ec35f575ba3261ULL) >> 28), ((0x00ca18f677300ef3ULL) & ((1 << 28) - 1)), ((0x00ca18f677300ef3ULL) >> 28), ((0x00e8bb0a827d8548ULL) & ((1 << 28) - 1)), ((0x00e8bb0a827d8548ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x00df76b3328ada72ULL) & ((1 << 28) - 1)), ((0x00df76b3328ada72ULL) >> 28), ((0x002e20621604a7c2ULL) & ((1 << 28) - 1)), ((0x002e20621604a7c2ULL) >> 28), ((0x00f910638a105b09ULL) & ((1 << 28) - 1)), ((0x00f910638a105b09ULL) >> 28), ((0x00ef4724d96ef2cdULL) & ((1 << 28) - 1)), ((0x00ef4724d96ef2cdULL) >> 28), ((0x00377d83d6b8a2f7ULL) & ((1 << 28) - 1)), ((0x00377d83d6b8a2f7ULL) >> 28), ((0x00b4f48805ade324ULL) & ((1 << 28) - 1)), ((0x00b4f48805ade324ULL) >> 28), ((0x001cd5da8b152018ULL) & ((1 << 28) - 1)), ((0x001cd5da8b152018ULL) >> 28), ((0x0045af671a20ca7fULL) & ((1 << 28) - 1)), ((0x0045af671a20ca7fULL) >> 28)}}


                                                                        },
            {{{((0x009ae3b93a56c404ULL) & ((1 << 28) - 1)), ((0x009ae3b93a56c404ULL) >> 28), ((0x004a410b7a456699ULL) & ((1 << 28) - 1)), ((0x004a410b7a456699ULL) >> 28), ((0x00023a619355e6b2ULL) & ((1 << 28) - 1)), ((0x00023a619355e6b2ULL) >> 28), ((0x009cdc7297387257ULL) & ((1 << 28) - 1)), ((0x009cdc7297387257ULL) >> 28), ((0x0055b94d4ae70d04ULL) & ((1 << 28) - 1)), ((0x0055b94d4ae70d04ULL) >> 28), ((0x002cbd607f65b005ULL) & ((1 << 28) - 1)), ((0x002cbd607f65b005ULL) >> 28), ((0x003208b489697166ULL) & ((1 << 28) - 1)), ((0x003208b489697166ULL) >> 28), ((0x00ea2aa058867370ULL) & ((1 << 28) - 1)), ((0x00ea2aa058867370ULL) >> 28)}}


                                                                        },
            {{{((0x00f29d2598ee3f32ULL) & ((1 << 28) - 1)), ((0x00f29d2598ee3f32ULL) >> 28), ((0x00b4ac5385d82adcULL) & ((1 << 28) - 1)), ((0x00b4ac5385d82adcULL) >> 28), ((0x007633eaf04df19bULL) & ((1 << 28) - 1)), ((0x007633eaf04df19bULL) >> 28), ((0x00aa2d3d77ceab01ULL) & ((1 << 28) - 1)), ((0x00aa2d3d77ceab01ULL) >> 28), ((0x004a2302fcbb778aULL) & ((1 << 28) - 1)), ((0x004a2302fcbb778aULL) >> 28), ((0x00927f225d5afa34ULL) & ((1 << 28) - 1)), ((0x00927f225d5afa34ULL) >> 28), ((0x004a8e9d5047f237ULL) & ((1 << 28) - 1)), ((0x004a8e9d5047f237ULL) >> 28), ((0x008224ae9dbce530ULL) & ((1 << 28) - 1)), ((0x008224ae9dbce530ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x001cf640859b02f8ULL) & ((1 << 28) - 1)), ((0x001cf640859b02f8ULL) >> 28), ((0x00758d1d5d5ce427ULL) & ((1 << 28) - 1)), ((0x00758d1d5d5ce427ULL) >> 28), ((0x00763c784ef4604cULL) & ((1 << 28) - 1)), ((0x00763c784ef4604cULL) >> 28), ((0x005fa81aee205270ULL) & ((1 << 28) - 1)), ((0x005fa81aee205270ULL) >> 28), ((0x00ac537bfdfc44cbULL) & ((1 << 28) - 1)), ((0x00ac537bfdfc44cbULL) >> 28), ((0x004b919bd342d670ULL) & ((1 << 28) - 1)), ((0x004b919bd342d670ULL) >> 28), ((0x00238508d9bf4b7aULL) & ((1 << 28) - 1)), ((0x00238508d9bf4b7aULL) >> 28), ((0x00154888795644f3ULL) & ((1 << 28) - 1)), ((0x00154888795644f3ULL) >> 28)}}


                                                                        },
            {{{((0x00c845923c084294ULL) & ((1 << 28) - 1)), ((0x00c845923c084294ULL) >> 28), ((0x00072419a201bc25ULL) & ((1 << 28) - 1)), ((0x00072419a201bc25ULL) >> 28), ((0x0045f408b5f8e669ULL) & ((1 << 28) - 1)), ((0x0045f408b5f8e669ULL) >> 28), ((0x00e9d6a186b74dfeULL) & ((1 << 28) - 1)), ((0x00e9d6a186b74dfeULL) >> 28), ((0x00e19108c68fa075ULL) & ((1 << 28) - 1)), ((0x00e19108c68fa075ULL) >> 28), ((0x0017b91d874177b7ULL) & ((1 << 28) - 1)), ((0x0017b91d874177b7ULL) >> 28), ((0x002f0ca2c7912c5aULL) & ((1 << 28) - 1)), ((0x002f0ca2c7912c5aULL) >> 28), ((0x009400aa385a90a2ULL) & ((1 << 28) - 1)), ((0x009400aa385a90a2ULL) >> 28)}}


                                                                        },
            {{{((0x0071110b01482184ULL) & ((1 << 28) - 1)), ((0x0071110b01482184ULL) >> 28), ((0x00cfed0044f2bef8ULL) & ((1 << 28) - 1)), ((0x00cfed0044f2bef8ULL) >> 28), ((0x0034f2901cf4662eULL) & ((1 << 28) - 1)), ((0x0034f2901cf4662eULL) >> 28), ((0x003b4ae2a67f9834ULL) & ((1 << 28) - 1)), ((0x003b4ae2a67f9834ULL) >> 28), ((0x00cca9b96fe94810ULL) & ((1 << 28) - 1)), ((0x00cca9b96fe94810ULL) >> 28), ((0x00522507ae77abd0ULL) & ((1 << 28) - 1)), ((0x00522507ae77abd0ULL) >> 28), ((0x00bac7422721e73eULL) & ((1 << 28) - 1)), ((0x00bac7422721e73eULL) >> 28), ((0x0066622b0f3a62b0ULL) & ((1 << 28) - 1)), ((0x0066622b0f3a62b0ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x00f8ac5cf4705b6aULL) & ((1 << 28) - 1)), ((0x00f8ac5cf4705b6aULL) >> 28), ((0x00867d82dcb457e3ULL) & ((1 << 28) - 1)), ((0x00867d82dcb457e3ULL) >> 28), ((0x007e13ab2ccc2ce9ULL) & ((1 << 28) - 1)), ((0x007e13ab2ccc2ce9ULL) >> 28), ((0x009ee9a018d3930eULL) & ((1 << 28) - 1)), ((0x009ee9a018d3930eULL) >> 28), ((0x008370f8ecb42df8ULL) & ((1 << 28) - 1)), ((0x008370f8ecb42df8ULL) >> 28), ((0x002d9f019add263eULL) & ((1 << 28) - 1)), ((0x002d9f019add263eULL) >> 28), ((0x003302385b92d196ULL) & ((1 << 28) - 1)), ((0x003302385b92d196ULL) >> 28), ((0x00a15654536e2c0cULL) & ((1 << 28) - 1)), ((0x00a15654536e2c0cULL) >> 28)}}


                                                                        },
            {{{((0x0026ef1614e160afULL) & ((1 << 28) - 1)), ((0x0026ef1614e160afULL) >> 28), ((0x00c023f9edfc9c76ULL) & ((1 << 28) - 1)), ((0x00c023f9edfc9c76ULL) >> 28), ((0x00cff090da5f57baULL) & ((1 << 28) - 1)), ((0x00cff090da5f57baULL) >> 28), ((0x0076db7a66643ae9ULL) & ((1 << 28) - 1)), ((0x0076db7a66643ae9ULL) >> 28), ((0x0019462f8c646999ULL) & ((1 << 28) - 1)), ((0x0019462f8c646999ULL) >> 28), ((0x008fec00b3854b22ULL) & ((1 << 28) - 1)), ((0x008fec00b3854b22ULL) >> 28), ((0x00d55041692a0a1cULL) & ((1 << 28) - 1)), ((0x00d55041692a0a1cULL) >> 28), ((0x0065db894215ca00ULL) & ((1 << 28) - 1)), ((0x0065db894215ca00ULL) >> 28)}}


                                                                        },
            {{{((0x00a925036e0a451cULL) & ((1 << 28) - 1)), ((0x00a925036e0a451cULL) >> 28), ((0x002a0390c36b6cc1ULL) & ((1 << 28) - 1)), ((0x002a0390c36b6cc1ULL) >> 28), ((0x00f27020d90894f4ULL) & ((1 << 28) - 1)), ((0x00f27020d90894f4ULL) >> 28), ((0x008d90d52cbd3d7fULL) & ((1 << 28) - 1)), ((0x008d90d52cbd3d7fULL) >> 28), ((0x00e1d0137392f3b8ULL) & ((1 << 28) - 1)), ((0x00e1d0137392f3b8ULL) >> 28), ((0x00f017c158b51a8fULL) & ((1 << 28) - 1)), ((0x00f017c158b51a8fULL) >> 28), ((0x00cac313d3ed7dbcULL) & ((1 << 28) - 1)), ((0x00cac313d3ed7dbcULL) >> 28), ((0x00b99a81e3eb42d3ULL) & ((1 << 28) - 1)), ((0x00b99a81e3eb42d3ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x00b54850275fe626ULL) & ((1 << 28) - 1)), ((0x00b54850275fe626ULL) >> 28), ((0x0053a3fd1ec71140ULL) & ((1 << 28) - 1)), ((0x0053a3fd1ec71140ULL) >> 28), ((0x00e3d2d7dbe096faULL) & ((1 << 28) - 1)), ((0x00e3d2d7dbe096faULL) >> 28), ((0x00e4ac7b595cce4cULL) & ((1 << 28) - 1)), ((0x00e4ac7b595cce4cULL) >> 28), ((0x0077bad449c0a494ULL) & ((1 << 28) - 1)), ((0x0077bad449c0a494ULL) >> 28), ((0x00b7c98814afd5b3ULL) & ((1 << 28) - 1)), ((0x00b7c98814afd5b3ULL) >> 28), ((0x0057226f58486cf9ULL) & ((1 << 28) - 1)), ((0x0057226f58486cf9ULL) >> 28), ((0x00b1557154f0cc57ULL) & ((1 << 28) - 1)), ((0x00b1557154f0cc57ULL) >> 28)}}


                                                                        },
            {{{((0x008cc9cd236315c0ULL) & ((1 << 28) - 1)), ((0x008cc9cd236315c0ULL) >> 28), ((0x0031d9c5b39fda54ULL) & ((1 << 28) - 1)), ((0x0031d9c5b39fda54ULL) >> 28), ((0x00a5713ef37e1171ULL) & ((1 << 28) - 1)), ((0x00a5713ef37e1171ULL) >> 28), ((0x00293d5ae2886325ULL) & ((1 << 28) - 1)), ((0x00293d5ae2886325ULL) >> 28), ((0x00c4aba3e05015e1ULL) & ((1 << 28) - 1)), ((0x00c4aba3e05015e1ULL) >> 28), ((0x0003f35ef78e4fc6ULL) & ((1 << 28) - 1)), ((0x0003f35ef78e4fc6ULL) >> 28), ((0x0039d6bd3ac1527bULL) & ((1 << 28) - 1)), ((0x0039d6bd3ac1527bULL) >> 28), ((0x0019d7c3afb77106ULL) & ((1 << 28) - 1)), ((0x0019d7c3afb77106ULL) >> 28)}}


                                                                        },
            {{{((0x007b162931a985afULL) & ((1 << 28) - 1)), ((0x007b162931a985afULL) >> 28), ((0x00ad40a2e0daa713ULL) & ((1 << 28) - 1)), ((0x00ad40a2e0daa713ULL) >> 28), ((0x006df27c4009f118ULL) & ((1 << 28) - 1)), ((0x006df27c4009f118ULL) >> 28), ((0x00503e9f4e2e8becULL) & ((1 << 28) - 1)), ((0x00503e9f4e2e8becULL) >> 28), ((0x00751a77c82c182dULL) & ((1 << 28) - 1)), ((0x00751a77c82c182dULL) >> 28), ((0x000298937769245bULL) & ((1 << 28) - 1)), ((0x000298937769245bULL) >> 28), ((0x00ffb1e8fabf9ee5ULL) & ((1 << 28) - 1)), ((0x00ffb1e8fabf9ee5ULL) >> 28), ((0x0008334706e09abeULL) & ((1 << 28) - 1)), ((0x0008334706e09abeULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x00dbca4e98a7dcd9ULL) & ((1 << 28) - 1)), ((0x00dbca4e98a7dcd9ULL) >> 28), ((0x00ee29cfc78bde99ULL) & ((1 << 28) - 1)), ((0x00ee29cfc78bde99ULL) >> 28), ((0x00e4a3b6995f52e9ULL) & ((1 << 28) - 1)), ((0x00e4a3b6995f52e9ULL) >> 28), ((0x0045d70189ae8096ULL) & ((1 << 28) - 1)), ((0x0045d70189ae8096ULL) >> 28), ((0x00fd2a8a3b9b0d1bULL) & ((1 << 28) - 1)), ((0x00fd2a8a3b9b0d1bULL) >> 28), ((0x00af1793b107d8e1ULL) & ((1 << 28) - 1)), ((0x00af1793b107d8e1ULL) >> 28), ((0x00dbf92cbe4afa20ULL) & ((1 << 28) - 1)), ((0x00dbf92cbe4afa20ULL) >> 28), ((0x00da60f798e3681dULL) & ((1 << 28) - 1)), ((0x00da60f798e3681dULL) >> 28)}}


                                                                        },
            {{{((0x004246bfcecc627aULL) & ((1 << 28) - 1)), ((0x004246bfcecc627aULL) >> 28), ((0x004ba431246c03a4ULL) & ((1 << 28) - 1)), ((0x004ba431246c03a4ULL) >> 28), ((0x00bd1d101872d497ULL) & ((1 << 28) - 1)), ((0x00bd1d101872d497ULL) >> 28), ((0x003b73d3f185ee16ULL) & ((1 << 28) - 1)), ((0x003b73d3f185ee16ULL) >> 28), ((0x001feb2e2678c0e3ULL) & ((1 << 28) - 1)), ((0x001feb2e2678c0e3ULL) >> 28), ((0x00ff13c5a89dec76ULL) & ((1 << 28) - 1)), ((0x00ff13c5a89dec76ULL) >> 28), ((0x00ed06042e771d8fULL) & ((1 << 28) - 1)), ((0x00ed06042e771d8fULL) >> 28), ((0x00a4fd2a897a83ddULL) & ((1 << 28) - 1)), ((0x00a4fd2a897a83ddULL) >> 28)}}


                                                                        },
            {{{((0x009a4a3be50d6597ULL) & ((1 << 28) - 1)), ((0x009a4a3be50d6597ULL) >> 28), ((0x00de3165fc5a1096ULL) & ((1 << 28) - 1)), ((0x00de3165fc5a1096ULL) >> 28), ((0x004f3f56e345b0c7ULL) & ((1 << 28) - 1)), ((0x004f3f56e345b0c7ULL) >> 28), ((0x00f7bf721d5ab8bcULL) & ((1 << 28) - 1)), ((0x00f7bf721d5ab8bcULL) >> 28), ((0x004313e47b098c50ULL) & ((1 << 28) - 1)), ((0x004313e47b098c50ULL) >> 28), ((0x00e4c7d5c0e1adbbULL) & ((1 << 28) - 1)), ((0x00e4c7d5c0e1adbbULL) >> 28), ((0x002e3e3db365051eULL) & ((1 << 28) - 1)), ((0x002e3e3db365051eULL) >> 28), ((0x00a480c2cd6a96fbULL) & ((1 << 28) - 1)), ((0x00a480c2cd6a96fbULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x00417fa30a7119edULL) & ((1 << 28) - 1)), ((0x00417fa30a7119edULL) >> 28), ((0x00af257758419751ULL) & ((1 << 28) - 1)), ((0x00af257758419751ULL) >> 28), ((0x00d358a487b463d4ULL) & ((1 << 28) - 1)), ((0x00d358a487b463d4ULL) >> 28), ((0x0089703cc720b00dULL) & ((1 << 28) - 1)), ((0x0089703cc720b00dULL) >> 28), ((0x00ce56314ff7f271ULL) & ((1 << 28) - 1)), ((0x00ce56314ff7f271ULL) >> 28), ((0x0064db171ade62c1ULL) & ((1 << 28) - 1)), ((0x0064db171ade62c1ULL) >> 28), ((0x00640b36d4a22fedULL) & ((1 << 28) - 1)), ((0x00640b36d4a22fedULL) >> 28), ((0x00424eb88696d23fULL) & ((1 << 28) - 1)), ((0x00424eb88696d23fULL) >> 28)}}


                                                                        },
            {{{((0x004ede34af2813f3ULL) & ((1 << 28) - 1)), ((0x004ede34af2813f3ULL) >> 28), ((0x00d4a8e11c9e8216ULL) & ((1 << 28) - 1)), ((0x00d4a8e11c9e8216ULL) >> 28), ((0x004796d5041de8a5ULL) & ((1 << 28) - 1)), ((0x004796d5041de8a5ULL) >> 28), ((0x00c4c6b4d21cc987ULL) & ((1 << 28) - 1)), ((0x00c4c6b4d21cc987ULL) >> 28), ((0x00e8a433ee07fa1eULL) & ((1 << 28) - 1)), ((0x00e8a433ee07fa1eULL) >> 28), ((0x0055720b5abcc5a1ULL) & ((1 << 28) - 1)), ((0x0055720b5abcc5a1ULL) >> 28), ((0x008873ea9c74b080ULL) & ((1 << 28) - 1)), ((0x008873ea9c74b080ULL) >> 28), ((0x005b3fec1ab65d48ULL) & ((1 << 28) - 1)), ((0x005b3fec1ab65d48ULL) >> 28)}}


                                                                        },
            {{{((0x0047e5277db70ec5ULL) & ((1 << 28) - 1)), ((0x0047e5277db70ec5ULL) >> 28), ((0x000a096c66db7d6bULL) & ((1 << 28) - 1)), ((0x000a096c66db7d6bULL) >> 28), ((0x00b4164cc1730159ULL) & ((1 << 28) - 1)), ((0x00b4164cc1730159ULL) >> 28), ((0x004a9f783fe720feULL) & ((1 << 28) - 1)), ((0x004a9f783fe720feULL) >> 28), ((0x00a8177b94449dbcULL) & ((1 << 28) - 1)), ((0x00a8177b94449dbcULL) >> 28), ((0x0095a24ff49a599fULL) & ((1 << 28) - 1)), ((0x0095a24ff49a599fULL) >> 28), ((0x0069c1c578250cbcULL) & ((1 << 28) - 1)), ((0x0069c1c578250cbcULL) >> 28), ((0x00452019213debf4ULL) & ((1 << 28) - 1)), ((0x00452019213debf4ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x0021ce99e09ebda3ULL) & ((1 << 28) - 1)), ((0x0021ce99e09ebda3ULL) >> 28), ((0x00fcbd9f91875ad0ULL) & ((1 << 28) - 1)), ((0x00fcbd9f91875ad0ULL) >> 28), ((0x009bbf6b7b7a0b5fULL) & ((1 << 28) - 1)), ((0x009bbf6b7b7a0b5fULL) >> 28), ((0x00388886a69b1940ULL) & ((1 << 28) - 1)), ((0x00388886a69b1940ULL) >> 28), ((0x00926a56d0f81f12ULL) & ((1 << 28) - 1)), ((0x00926a56d0f81f12ULL) >> 28), ((0x00e12903c3358d46ULL) & ((1 << 28) - 1)), ((0x00e12903c3358d46ULL) >> 28), ((0x005dfce4e8e1ce9dULL) & ((1 << 28) - 1)), ((0x005dfce4e8e1ce9dULL) >> 28), ((0x0044cfa94e2f7e23ULL) & ((1 << 28) - 1)), ((0x0044cfa94e2f7e23ULL) >> 28)}}


                                                                        },
            {{{((0x001bd59c09e982eaULL) & ((1 << 28) - 1)), ((0x001bd59c09e982eaULL) >> 28), ((0x00f72daeb937b289ULL) & ((1 << 28) - 1)), ((0x00f72daeb937b289ULL) >> 28), ((0x0018b76dca908e0eULL) & ((1 << 28) - 1)), ((0x0018b76dca908e0eULL) >> 28), ((0x00edb498512384adULL) & ((1 << 28) - 1)), ((0x00edb498512384adULL) >> 28), ((0x00ce0243b6cc9538ULL) & ((1 << 28) - 1)), ((0x00ce0243b6cc9538ULL) >> 28), ((0x00f96ff690cb4e70ULL) & ((1 << 28) - 1)), ((0x00f96ff690cb4e70ULL) >> 28), ((0x007c77bf9f673c8dULL) & ((1 << 28) - 1)), ((0x007c77bf9f673c8dULL) >> 28), ((0x005bf704c088a528ULL) & ((1 << 28) - 1)), ((0x005bf704c088a528ULL) >> 28)}}


                                                                        },
            {{{((0x0093d4628dcb33beULL) & ((1 << 28) - 1)), ((0x0093d4628dcb33beULL) >> 28), ((0x0095263d51d42582ULL) & ((1 << 28) - 1)), ((0x0095263d51d42582ULL) >> 28), ((0x0049b3222458fe06ULL) & ((1 << 28) - 1)), ((0x0049b3222458fe06ULL) >> 28), ((0x00e7fce73b653a7fULL) & ((1 << 28) - 1)), ((0x00e7fce73b653a7fULL) >> 28), ((0x003ca2ebce60b369ULL) & ((1 << 28) - 1)), ((0x003ca2ebce60b369ULL) >> 28), ((0x00c5de239a32bea4ULL) & ((1 << 28) - 1)), ((0x00c5de239a32bea4ULL) >> 28), ((0x0063b8b3d71fb6bfULL) & ((1 << 28) - 1)), ((0x0063b8b3d71fb6bfULL) >> 28), ((0x0039aeeb78a1a839ULL) & ((1 << 28) - 1)), ((0x0039aeeb78a1a839ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x007dc52da400336cULL) & ((1 << 28) - 1)), ((0x007dc52da400336cULL) >> 28), ((0x001fded1e15b9457ULL) & ((1 << 28) - 1)), ((0x001fded1e15b9457ULL) >> 28), ((0x00902e00f5568e3aULL) & ((1 << 28) - 1)), ((0x00902e00f5568e3aULL) >> 28), ((0x00219bef40456d2dULL) & ((1 << 28) - 1)), ((0x00219bef40456d2dULL) >> 28), ((0x005684161fb3dbc9ULL) & ((1 << 28) - 1)), ((0x005684161fb3dbc9ULL) >> 28), ((0x004a4e9be49a76eaULL) & ((1 << 28) - 1)), ((0x004a4e9be49a76eaULL) >> 28), ((0x006e685ae88b78ffULL) & ((1 << 28) - 1)), ((0x006e685ae88b78ffULL) >> 28), ((0x0021c42f13042d3cULL) & ((1 << 28) - 1)), ((0x0021c42f13042d3cULL) >> 28)}}


                                                                        },
            {{{((0x00fb22bb5fd3ce50ULL) & ((1 << 28) - 1)), ((0x00fb22bb5fd3ce50ULL) >> 28), ((0x0017b48aada7ae54ULL) & ((1 << 28) - 1)), ((0x0017b48aada7ae54ULL) >> 28), ((0x00fd5c44ad19a536ULL) & ((1 << 28) - 1)), ((0x00fd5c44ad19a536ULL) >> 28), ((0x000ccc4e4e55e45cULL) & ((1 << 28) - 1)), ((0x000ccc4e4e55e45cULL) >> 28), ((0x00fd637d45b4c3f5ULL) & ((1 << 28) - 1)), ((0x00fd637d45b4c3f5ULL) >> 28), ((0x0038914e023c37cfULL) & ((1 << 28) - 1)), ((0x0038914e023c37cfULL) >> 28), ((0x00ac1881d6a8d898ULL) & ((1 << 28) - 1)), ((0x00ac1881d6a8d898ULL) >> 28), ((0x00611ed8d3d943a8ULL) & ((1 << 28) - 1)), ((0x00611ed8d3d943a8ULL) >> 28)}}


                                                                        },
            {{{((0x0056e2259d113d2bULL) & ((1 << 28) - 1)), ((0x0056e2259d113d2bULL) >> 28), ((0x00594819b284ec16ULL) & ((1 << 28) - 1)), ((0x00594819b284ec16ULL) >> 28), ((0x00c7bf794bb36696ULL) & ((1 << 28) - 1)), ((0x00c7bf794bb36696ULL) >> 28), ((0x00721ee75097cdc6ULL) & ((1 << 28) - 1)), ((0x00721ee75097cdc6ULL) >> 28), ((0x00f71be9047a2892ULL) & ((1 << 28) - 1)), ((0x00f71be9047a2892ULL) >> 28), ((0x00df6ba142564edfULL) & ((1 << 28) - 1)), ((0x00df6ba142564edfULL) >> 28), ((0x0069580b7a184e8dULL) & ((1 << 28) - 1)), ((0x0069580b7a184e8dULL) >> 28), ((0x00f056e38fca0feeULL) & ((1 << 28) - 1)), ((0x00f056e38fca0feeULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x009df98566a18c6dULL) & ((1 << 28) - 1)), ((0x009df98566a18c6dULL) >> 28), ((0x00cf3a200968f219ULL) & ((1 << 28) - 1)), ((0x00cf3a200968f219ULL) >> 28), ((0x0044ba60da6d9086ULL) & ((1 << 28) - 1)), ((0x0044ba60da6d9086ULL) >> 28), ((0x00dbc9c0e344da03ULL) & ((1 << 28) - 1)), ((0x00dbc9c0e344da03ULL) >> 28), ((0x000f9401c4466855ULL) & ((1 << 28) - 1)), ((0x000f9401c4466855ULL) >> 28), ((0x00d46a57c5b0a8d1ULL) & ((1 << 28) - 1)), ((0x00d46a57c5b0a8d1ULL) >> 28), ((0x00875a635d7ac7c6ULL) & ((1 << 28) - 1)), ((0x00875a635d7ac7c6ULL) >> 28), ((0x00ef4a933b7e0ae6ULL) & ((1 << 28) - 1)), ((0x00ef4a933b7e0ae6ULL) >> 28)}}


                                                                        },
            {{{((0x005e8694077a1535ULL) & ((1 << 28) - 1)), ((0x005e8694077a1535ULL) >> 28), ((0x008bef75f71c8f1dULL) & ((1 << 28) - 1)), ((0x008bef75f71c8f1dULL) >> 28), ((0x000a7c1316423511ULL) & ((1 << 28) - 1)), ((0x000a7c1316423511ULL) >> 28), ((0x00906e1d70604320ULL) & ((1 << 28) - 1)), ((0x00906e1d70604320ULL) >> 28), ((0x003fc46c1a2ffbd6ULL) & ((1 << 28) - 1)), ((0x003fc46c1a2ffbd6ULL) >> 28), ((0x00d1d5022e68f360ULL) & ((1 << 28) - 1)), ((0x00d1d5022e68f360ULL) >> 28), ((0x002515fba37bbf46ULL) & ((1 << 28) - 1)), ((0x002515fba37bbf46ULL) >> 28), ((0x00ca16234e023b44ULL) & ((1 << 28) - 1)), ((0x00ca16234e023b44ULL) >> 28)}}


                                                                        },
            {{{((0x00787c99561f4690ULL) & ((1 << 28) - 1)), ((0x00787c99561f4690ULL) >> 28), ((0x00a857a8c1561f27ULL) & ((1 << 28) - 1)), ((0x00a857a8c1561f27ULL) >> 28), ((0x00a10df9223c09feULL) & ((1 << 28) - 1)), ((0x00a10df9223c09feULL) >> 28), ((0x00b98a9562e3b154ULL) & ((1 << 28) - 1)), ((0x00b98a9562e3b154ULL) >> 28), ((0x004330b8744c3ed2ULL) & ((1 << 28) - 1)), ((0x004330b8744c3ed2ULL) >> 28), ((0x00e06812807ec5c4ULL) & ((1 << 28) - 1)), ((0x00e06812807ec5c4ULL) >> 28), ((0x00e4cf6a7db9f1e3ULL) & ((1 << 28) - 1)), ((0x00e4cf6a7db9f1e3ULL) >> 28), ((0x00d95b089f132a34ULL) & ((1 << 28) - 1)), ((0x00d95b089f132a34ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x002922b39ca33eecULL) & ((1 << 28) - 1)), ((0x002922b39ca33eecULL) >> 28), ((0x0090d12a5f3ab194ULL) & ((1 << 28) - 1)), ((0x0090d12a5f3ab194ULL) >> 28), ((0x00ab60c02fb5f8edULL) & ((1 << 28) - 1)), ((0x00ab60c02fb5f8edULL) >> 28), ((0x00188d292abba1cfULL) & ((1 << 28) - 1)), ((0x00188d292abba1cfULL) >> 28), ((0x00e10edec9698f6eULL) & ((1 << 28) - 1)), ((0x00e10edec9698f6eULL) >> 28), ((0x0069a4d9934133c8ULL) & ((1 << 28) - 1)), ((0x0069a4d9934133c8ULL) >> 28), ((0x0024aac40e6d3d06ULL) & ((1 << 28) - 1)), ((0x0024aac40e6d3d06ULL) >> 28), ((0x001702c2177661b0ULL) & ((1 << 28) - 1)), ((0x001702c2177661b0ULL) >> 28)}}


                                                                        },
            {{{((0x00139078397030bdULL) & ((1 << 28) - 1)), ((0x00139078397030bdULL) >> 28), ((0x000e3c447e859a00ULL) & ((1 << 28) - 1)), ((0x000e3c447e859a00ULL) >> 28), ((0x0064a5b334c82393ULL) & ((1 << 28) - 1)), ((0x0064a5b334c82393ULL) >> 28), ((0x00b8aabeb7358093ULL) & ((1 << 28) - 1)), ((0x00b8aabeb7358093ULL) >> 28), ((0x00020778bb9ae73bULL) & ((1 << 28) - 1)), ((0x00020778bb9ae73bULL) >> 28), ((0x0032ee94c7892a18ULL) & ((1 << 28) - 1)), ((0x0032ee94c7892a18ULL) >> 28), ((0x008215253cb41bdaULL) & ((1 << 28) - 1)), ((0x008215253cb41bdaULL) >> 28), ((0x005e2797593517aeULL) & ((1 << 28) - 1)), ((0x005e2797593517aeULL) >> 28)}}


                                                                        },
            {{{((0x0083765a5f855d4aULL) & ((1 << 28) - 1)), ((0x0083765a5f855d4aULL) >> 28), ((0x0051b6d1351b8ee2ULL) & ((1 << 28) - 1)), ((0x0051b6d1351b8ee2ULL) >> 28), ((0x00116de548b0f7bbULL) & ((1 << 28) - 1)), ((0x00116de548b0f7bbULL) >> 28), ((0x0087bd88703affa0ULL) & ((1 << 28) - 1)), ((0x0087bd88703affa0ULL) >> 28), ((0x0095b2cc34d7fdd2ULL) & ((1 << 28) - 1)), ((0x0095b2cc34d7fdd2ULL) >> 28), ((0x0084cd81b53f0bc8ULL) & ((1 << 28) - 1)), ((0x0084cd81b53f0bc8ULL) >> 28), ((0x008562fc995350edULL) & ((1 << 28) - 1)), ((0x008562fc995350edULL) >> 28), ((0x00a39abb193651e3ULL) & ((1 << 28) - 1)), ((0x00a39abb193651e3ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x0019e23f0474b114ULL) & ((1 << 28) - 1)), ((0x0019e23f0474b114ULL) >> 28), ((0x00eb94c2ad3b437eULL) & ((1 << 28) - 1)), ((0x00eb94c2ad3b437eULL) >> 28), ((0x006ddb34683b75acULL) & ((1 << 28) - 1)), ((0x006ddb34683b75acULL) >> 28), ((0x00391f9209b564c6ULL) & ((1 << 28) - 1)), ((0x00391f9209b564c6ULL) >> 28), ((0x00083b3bb3bff7aaULL) & ((1 << 28) - 1)), ((0x00083b3bb3bff7aaULL) >> 28), ((0x00eedcd0f6dceefcULL) & ((1 << 28) - 1)), ((0x00eedcd0f6dceefcULL) >> 28), ((0x00b50817f794fe01ULL) & ((1 << 28) - 1)), ((0x00b50817f794fe01ULL) >> 28), ((0x0036474deaaa75c9ULL) & ((1 << 28) - 1)), ((0x0036474deaaa75c9ULL) >> 28)}}


                                                                        },
            {{{((0x0091868594265aa2ULL) & ((1 << 28) - 1)), ((0x0091868594265aa2ULL) >> 28), ((0x00797accae98ca6dULL) & ((1 << 28) - 1)), ((0x00797accae98ca6dULL) >> 28), ((0x0008d8c5f0f8a184ULL) & ((1 << 28) - 1)), ((0x0008d8c5f0f8a184ULL) >> 28), ((0x00d1f4f1c2b2fe6eULL) & ((1 << 28) - 1)), ((0x00d1f4f1c2b2fe6eULL) >> 28), ((0x0036783dfb48a006ULL) & ((1 << 28) - 1)), ((0x0036783dfb48a006ULL) >> 28), ((0x008c165120503527ULL) & ((1 << 28) - 1)), ((0x008c165120503527ULL) >> 28), ((0x0025fd780058ce9bULL) & ((1 << 28) - 1)), ((0x0025fd780058ce9bULL) >> 28), ((0x0068beb007be7d27ULL) & ((1 << 28) - 1)), ((0x0068beb007be7d27ULL) >> 28)}}


                                                                        },
            {{{((0x00d0ff88aa7c90c2ULL) & ((1 << 28) - 1)), ((0x00d0ff88aa7c90c2ULL) >> 28), ((0x00b2c60dacf53394ULL) & ((1 << 28) - 1)), ((0x00b2c60dacf53394ULL) >> 28), ((0x0094a7284d9666d6ULL) & ((1 << 28) - 1)), ((0x0094a7284d9666d6ULL) >> 28), ((0x00bed9022ce7a19dULL) & ((1 << 28) - 1)), ((0x00bed9022ce7a19dULL) >> 28), ((0x00c51553f0cd7682ULL) & ((1 << 28) - 1)), ((0x00c51553f0cd7682ULL) >> 28), ((0x00c3fb870b124992ULL) & ((1 << 28) - 1)), ((0x00c3fb870b124992ULL) >> 28), ((0x008d0bc539956c9bULL) & ((1 << 28) - 1)), ((0x008d0bc539956c9bULL) >> 28), ((0x00fc8cf258bb8885ULL) & ((1 << 28) - 1)), ((0x00fc8cf258bb8885ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x003667bf998406f8ULL) & ((1 << 28) - 1)), ((0x003667bf998406f8ULL) >> 28), ((0x0000115c43a12975ULL) & ((1 << 28) - 1)), ((0x0000115c43a12975ULL) >> 28), ((0x001e662f3b20e8fdULL) & ((1 << 28) - 1)), ((0x001e662f3b20e8fdULL) >> 28), ((0x0019ffa534cb24ebULL) & ((1 << 28) - 1)), ((0x0019ffa534cb24ebULL) >> 28), ((0x00016be0dc8efb45ULL) & ((1 << 28) - 1)), ((0x00016be0dc8efb45ULL) >> 28), ((0x00ff76a8b26243f5ULL) & ((1 << 28) - 1)), ((0x00ff76a8b26243f5ULL) >> 28), ((0x00ae20d241a541e3ULL) & ((1 << 28) - 1)), ((0x00ae20d241a541e3ULL) >> 28), ((0x0069bd6af13cd430ULL) & ((1 << 28) - 1)), ((0x0069bd6af13cd430ULL) >> 28)}}


                                                                        },
            {{{((0x0045fdc16487cda3ULL) & ((1 << 28) - 1)), ((0x0045fdc16487cda3ULL) >> 28), ((0x00b2d8e844cf2ed7ULL) & ((1 << 28) - 1)), ((0x00b2d8e844cf2ed7ULL) >> 28), ((0x00612c50e88c1607ULL) & ((1 << 28) - 1)), ((0x00612c50e88c1607ULL) >> 28), ((0x00a08aabc66c1672ULL) & ((1 << 28) - 1)), ((0x00a08aabc66c1672ULL) >> 28), ((0x006031fdcbb24d97ULL) & ((1 << 28) - 1)), ((0x006031fdcbb24d97ULL) >> 28), ((0x001b639525744b93ULL) & ((1 << 28) - 1)), ((0x001b639525744b93ULL) >> 28), ((0x004409d62639ab17ULL) & ((1 << 28) - 1)), ((0x004409d62639ab17ULL) >> 28), ((0x00a1853d0347ab1dULL) & ((1 << 28) - 1)), ((0x00a1853d0347ab1dULL) >> 28)}}


                                                                        },
            {{{((0x0075a1a56ebf5c21ULL) & ((1 << 28) - 1)), ((0x0075a1a56ebf5c21ULL) >> 28), ((0x00a3e72be9ac53edULL) & ((1 << 28) - 1)), ((0x00a3e72be9ac53edULL) >> 28), ((0x00efcde1629170c2ULL) & ((1 << 28) - 1)), ((0x00efcde1629170c2ULL) >> 28), ((0x0004225fe91ef535ULL) & ((1 << 28) - 1)), ((0x0004225fe91ef535ULL) >> 28), ((0x0088049fc73dfda7ULL) & ((1 << 28) - 1)), ((0x0088049fc73dfda7ULL) >> 28), ((0x004abc74857e1288ULL) & ((1 << 28) - 1)), ((0x004abc74857e1288ULL) >> 28), ((0x0024e2434657317cULL) & ((1 << 28) - 1)), ((0x0024e2434657317cULL) >> 28), ((0x00d98cb3d3e5543cULL) & ((1 << 28) - 1)), ((0x00d98cb3d3e5543cULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x00b4b53eab6bdb19ULL) & ((1 << 28) - 1)), ((0x00b4b53eab6bdb19ULL) >> 28), ((0x009b22d8b43711d0ULL) & ((1 << 28) - 1)), ((0x009b22d8b43711d0ULL) >> 28), ((0x00d948b9d961785dULL) & ((1 << 28) - 1)), ((0x00d948b9d961785dULL) >> 28), ((0x00cb167b6f279eadULL) & ((1 << 28) - 1)), ((0x00cb167b6f279eadULL) >> 28), ((0x00191de3a678e1c9ULL) & ((1 << 28) - 1)), ((0x00191de3a678e1c9ULL) >> 28), ((0x00d9dd9511095c2eULL) & ((1 << 28) - 1)), ((0x00d9dd9511095c2eULL) >> 28), ((0x00f284324cd43067ULL) & ((1 << 28) - 1)), ((0x00f284324cd43067ULL) >> 28), ((0x00ed74fa535151ddULL) & ((1 << 28) - 1)), ((0x00ed74fa535151ddULL) >> 28)}}


                                                                        },
            {{{((0x007e32c049b5c477ULL) & ((1 << 28) - 1)), ((0x007e32c049b5c477ULL) >> 28), ((0x009d2bfdbd9bcfd8ULL) & ((1 << 28) - 1)), ((0x009d2bfdbd9bcfd8ULL) >> 28), ((0x00636e93045938c6ULL) & ((1 << 28) - 1)), ((0x00636e93045938c6ULL) >> 28), ((0x007fde4af7687298ULL) & ((1 << 28) - 1)), ((0x007fde4af7687298ULL) >> 28), ((0x0046a5184fafa5d3ULL) & ((1 << 28) - 1)), ((0x0046a5184fafa5d3ULL) >> 28), ((0x0079b1e7f13a359bULL) & ((1 << 28) - 1)), ((0x0079b1e7f13a359bULL) >> 28), ((0x00875adf1fb927d6ULL) & ((1 << 28) - 1)), ((0x00875adf1fb927d6ULL) >> 28), ((0x00333e21c61bcad2ULL) & ((1 << 28) - 1)), ((0x00333e21c61bcad2ULL) >> 28)}}


                                                                        },
            {{{((0x00048014f73d8b8dULL) & ((1 << 28) - 1)), ((0x00048014f73d8b8dULL) >> 28), ((0x0075684aa0966388ULL) & ((1 << 28) - 1)), ((0x0075684aa0966388ULL) >> 28), ((0x0092be7df06dc47cULL) & ((1 << 28) - 1)), ((0x0092be7df06dc47cULL) >> 28), ((0x0097cebcd0f5568aULL) & ((1 << 28) - 1)), ((0x0097cebcd0f5568aULL) >> 28), ((0x005a7004d9c4c6a9ULL) & ((1 << 28) - 1)), ((0x005a7004d9c4c6a9ULL) >> 28), ((0x00b0ecbb659924c7ULL) & ((1 << 28) - 1)), ((0x00b0ecbb659924c7ULL) >> 28), ((0x00d90332dd492a7cULL) & ((1 << 28) - 1)), ((0x00d90332dd492a7cULL) >> 28), ((0x0057fc14df11493dULL) & ((1 << 28) - 1)), ((0x0057fc14df11493dULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x0008ed8ea0ad95beULL) & ((1 << 28) - 1)), ((0x0008ed8ea0ad95beULL) >> 28), ((0x0041d324b9709645ULL) & ((1 << 28) - 1)), ((0x0041d324b9709645ULL) >> 28), ((0x00e25412257a19b4ULL) & ((1 << 28) - 1)), ((0x00e25412257a19b4ULL) >> 28), ((0x0058df9f3423d8d2ULL) & ((1 << 28) - 1)), ((0x0058df9f3423d8d2ULL) >> 28), ((0x00a9ab20def71304ULL) & ((1 << 28) - 1)), ((0x00a9ab20def71304ULL) >> 28), ((0x009ae0dbf8ac4a81ULL) & ((1 << 28) - 1)), ((0x009ae0dbf8ac4a81ULL) >> 28), ((0x00c9565977e4392aULL) & ((1 << 28) - 1)), ((0x00c9565977e4392aULL) >> 28), ((0x003c9269444baf55ULL) & ((1 << 28) - 1)), ((0x003c9269444baf55ULL) >> 28)}}


                                                                        },
            {{{((0x007df6cbb926830bULL) & ((1 << 28) - 1)), ((0x007df6cbb926830bULL) >> 28), ((0x00d336058ae37865ULL) & ((1 << 28) - 1)), ((0x00d336058ae37865ULL) >> 28), ((0x007af47dac696423ULL) & ((1 << 28) - 1)), ((0x007af47dac696423ULL) >> 28), ((0x0048d3011ec64ac8ULL) & ((1 << 28) - 1)), ((0x0048d3011ec64ac8ULL) >> 28), ((0x006b87666e40049fULL) & ((1 << 28) - 1)), ((0x006b87666e40049fULL) >> 28), ((0x0036a2e0e51303d7ULL) & ((1 << 28) - 1)), ((0x0036a2e0e51303d7ULL) >> 28), ((0x00ba319bd79dbc55ULL) & ((1 << 28) - 1)), ((0x00ba319bd79dbc55ULL) >> 28), ((0x003e2737ecc94f53ULL) & ((1 << 28) - 1)), ((0x003e2737ecc94f53ULL) >> 28)}}


                                                                        },
            {{{((0x00d296ff726272d9ULL) & ((1 << 28) - 1)), ((0x00d296ff726272d9ULL) >> 28), ((0x00f6d097928fcf57ULL) & ((1 << 28) - 1)), ((0x00f6d097928fcf57ULL) >> 28), ((0x00e0e616a55d7013ULL) & ((1 << 28) - 1)), ((0x00e0e616a55d7013ULL) >> 28), ((0x00deaf454ed9eac7ULL) & ((1 << 28) - 1)), ((0x00deaf454ed9eac7ULL) >> 28), ((0x0073a56bedef4d92ULL) & ((1 << 28) - 1)), ((0x0073a56bedef4d92ULL) >> 28), ((0x006ccfdf6fc92e19ULL) & ((1 << 28) - 1)), ((0x006ccfdf6fc92e19ULL) >> 28), ((0x009d1ee1371a7218ULL) & ((1 << 28) - 1)), ((0x009d1ee1371a7218ULL) >> 28), ((0x00ee3c2ee4462d80ULL) & ((1 << 28) - 1)), ((0x00ee3c2ee4462d80ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x00437bce9bccdf9dULL) & ((1 << 28) - 1)), ((0x00437bce9bccdf9dULL) >> 28), ((0x00e0c8e2f85dc0a3ULL) & ((1 << 28) - 1)), ((0x00e0c8e2f85dc0a3ULL) >> 28), ((0x00c91a7073995a19ULL) & ((1 << 28) - 1)), ((0x00c91a7073995a19ULL) >> 28), ((0x00856ec9fe294559ULL) & ((1 << 28) - 1)), ((0x00856ec9fe294559ULL) >> 28), ((0x009e4b33394b156eULL) & ((1 << 28) - 1)), ((0x009e4b33394b156eULL) >> 28), ((0x00e245b0dc497e5cULL) & ((1 << 28) - 1)), ((0x00e245b0dc497e5cULL) >> 28), ((0x006a54e687eeaeffULL) & ((1 << 28) - 1)), ((0x006a54e687eeaeffULL) >> 28), ((0x00f1cd1cd00fdb7cULL) & ((1 << 28) - 1)), ((0x00f1cd1cd00fdb7cULL) >> 28)}}


                                                                        },
            {{{((0x008132ae5c5d8cd1ULL) & ((1 << 28) - 1)), ((0x008132ae5c5d8cd1ULL) >> 28), ((0x00121d68324a1d9fULL) & ((1 << 28) - 1)), ((0x00121d68324a1d9fULL) >> 28), ((0x00d6be9dafcb8c76ULL) & ((1 << 28) - 1)), ((0x00d6be9dafcb8c76ULL) >> 28), ((0x00684d9070edf745ULL) & ((1 << 28) - 1)), ((0x00684d9070edf745ULL) >> 28), ((0x00519fbc96d7448eULL) & ((1 << 28) - 1)), ((0x00519fbc96d7448eULL) >> 28), ((0x00388182fdc1f27eULL) & ((1 << 28) - 1)), ((0x00388182fdc1f27eULL) >> 28), ((0x000235baed41f158ULL) & ((1 << 28) - 1)), ((0x000235baed41f158ULL) >> 28), ((0x00bf6cf6f1a1796aULL) & ((1 << 28) - 1)), ((0x00bf6cf6f1a1796aULL) >> 28)}}


                                                                        },
            {{{((0x002adc4b4d148219ULL) & ((1 << 28) - 1)), ((0x002adc4b4d148219ULL) >> 28), ((0x003084ada0d3a90aULL) & ((1 << 28) - 1)), ((0x003084ada0d3a90aULL) >> 28), ((0x0046de8aab0f2e4eULL) & ((1 << 28) - 1)), ((0x0046de8aab0f2e4eULL) >> 28), ((0x00452d342a67b5fdULL) & ((1 << 28) - 1)), ((0x00452d342a67b5fdULL) >> 28), ((0x00d4b50f01d4de21ULL) & ((1 << 28) - 1)), ((0x00d4b50f01d4de21ULL) >> 28), ((0x00db6d9fc0cefb79ULL) & ((1 << 28) - 1)), ((0x00db6d9fc0cefb79ULL) >> 28), ((0x008c184c86a462cdULL) & ((1 << 28) - 1)), ((0x008c184c86a462cdULL) >> 28), ((0x00e17c83764d42daULL) & ((1 << 28) - 1)), ((0x00e17c83764d42daULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x007b2743b9a1e01aULL) & ((1 << 28) - 1)), ((0x007b2743b9a1e01aULL) >> 28), ((0x007847ffd42688c4ULL) & ((1 << 28) - 1)), ((0x007847ffd42688c4ULL) >> 28), ((0x006c7844d610a316ULL) & ((1 << 28) - 1)), ((0x006c7844d610a316ULL) >> 28), ((0x00f0cb8b250aa4b0ULL) & ((1 << 28) - 1)), ((0x00f0cb8b250aa4b0ULL) >> 28), ((0x00a19060143b3ae6ULL) & ((1 << 28) - 1)), ((0x00a19060143b3ae6ULL) >> 28), ((0x0014eb10b77cfd80ULL) & ((1 << 28) - 1)), ((0x0014eb10b77cfd80ULL) >> 28), ((0x000170905729dd06ULL) & ((1 << 28) - 1)), ((0x000170905729dd06ULL) >> 28), ((0x00063b5b9cd72477ULL) & ((1 << 28) - 1)), ((0x00063b5b9cd72477ULL) >> 28)}}


                                                                        },
            {{{((0x00ce382dc7993d92ULL) & ((1 << 28) - 1)), ((0x00ce382dc7993d92ULL) >> 28), ((0x00021153e938b4c8ULL) & ((1 << 28) - 1)), ((0x00021153e938b4c8ULL) >> 28), ((0x00096f7567f48f51ULL) & ((1 << 28) - 1)), ((0x00096f7567f48f51ULL) >> 28), ((0x0058f81ddfe4b0d5ULL) & ((1 << 28) - 1)), ((0x0058f81ddfe4b0d5ULL) >> 28), ((0x00cc379a56b355c7ULL) & ((1 << 28) - 1)), ((0x00cc379a56b355c7ULL) >> 28), ((0x002c760770d3e819ULL) & ((1 << 28) - 1)), ((0x002c760770d3e819ULL) >> 28), ((0x00ee22d1d26e5a40ULL) & ((1 << 28) - 1)), ((0x00ee22d1d26e5a40ULL) >> 28), ((0x00de6d93d5b082d7ULL) & ((1 << 28) - 1)), ((0x00de6d93d5b082d7ULL) >> 28)}}


                                                                        },
            {{{((0x000a91a42c52e056ULL) & ((1 << 28) - 1)), ((0x000a91a42c52e056ULL) >> 28), ((0x00185f6b77fce7eaULL) & ((1 << 28) - 1)), ((0x00185f6b77fce7eaULL) >> 28), ((0x000803c51962f6b5ULL) & ((1 << 28) - 1)), ((0x000803c51962f6b5ULL) >> 28), ((0x0022528582ba563dULL) & ((1 << 28) - 1)), ((0x0022528582ba563dULL) >> 28), ((0x0043f8040e9856d6ULL) & ((1 << 28) - 1)), ((0x0043f8040e9856d6ULL) >> 28), ((0x0085a29ec81fb860ULL) & ((1 << 28) - 1)), ((0x0085a29ec81fb860ULL) >> 28), ((0x005f9a611549f5ffULL) & ((1 << 28) - 1)), ((0x005f9a611549f5ffULL) >> 28), ((0x00c1f974ecbd4b06ULL) & ((1 << 28) - 1)), ((0x00c1f974ecbd4b06ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x005b64c6fd65ec97ULL) & ((1 << 28) - 1)), ((0x005b64c6fd65ec97ULL) >> 28), ((0x00c1fdd7f877bc7fULL) & ((1 << 28) - 1)), ((0x00c1fdd7f877bc7fULL) >> 28), ((0x000d9cc6c89f841cULL) & ((1 << 28) - 1)), ((0x000d9cc6c89f841cULL) >> 28), ((0x005c97b7f1aff9adULL) & ((1 << 28) - 1)), ((0x005c97b7f1aff9adULL) >> 28), ((0x0075e3c61475d47eULL) & ((1 << 28) - 1)), ((0x0075e3c61475d47eULL) >> 28), ((0x001ecb1ba8153011ULL) & ((1 << 28) - 1)), ((0x001ecb1ba8153011ULL) >> 28), ((0x00fe7f1c8d71d40dULL) & ((1 << 28) - 1)), ((0x00fe7f1c8d71d40dULL) >> 28), ((0x003fa9757a229832ULL) & ((1 << 28) - 1)), ((0x003fa9757a229832ULL) >> 28)}}


                                                                        },
            {{{((0x00ffc5c89d2b0cbaULL) & ((1 << 28) - 1)), ((0x00ffc5c89d2b0cbaULL) >> 28), ((0x00d363d42e3e6fc3ULL) & ((1 << 28) - 1)), ((0x00d363d42e3e6fc3ULL) >> 28), ((0x0019a1a0118e2e8aULL) & ((1 << 28) - 1)), ((0x0019a1a0118e2e8aULL) >> 28), ((0x00f7baeff48882e1ULL) & ((1 << 28) - 1)), ((0x00f7baeff48882e1ULL) >> 28), ((0x001bd5af28c6b514ULL) & ((1 << 28) - 1)), ((0x001bd5af28c6b514ULL) >> 28), ((0x0055476ca2253cb2ULL) & ((1 << 28) - 1)), ((0x0055476ca2253cb2ULL) >> 28), ((0x00d8eb1977e2ddf3ULL) & ((1 << 28) - 1)), ((0x00d8eb1977e2ddf3ULL) >> 28), ((0x00b173b1adb228a1ULL) & ((1 << 28) - 1)), ((0x00b173b1adb228a1ULL) >> 28)}}


                                                                        },
            {{{((0x00f2cb99dd0ad707ULL) & ((1 << 28) - 1)), ((0x00f2cb99dd0ad707ULL) >> 28), ((0x00e1e08b6859ddd8ULL) & ((1 << 28) - 1)), ((0x00e1e08b6859ddd8ULL) >> 28), ((0x000008f2d0650bccULL) & ((1 << 28) - 1)), ((0x000008f2d0650bccULL) >> 28), ((0x00d7ed392f8615c3ULL) & ((1 << 28) - 1)), ((0x00d7ed392f8615c3ULL) >> 28), ((0x00976750a94da27fULL) & ((1 << 28) - 1)), ((0x00976750a94da27fULL) >> 28), ((0x003e83bb0ecb69baULL) & ((1 << 28) - 1)), ((0x003e83bb0ecb69baULL) >> 28), ((0x00df8e8d15c14ac6ULL) & ((1 << 28) - 1)), ((0x00df8e8d15c14ac6ULL) >> 28), ((0x00f9f7174295d9c2ULL) & ((1 << 28) - 1)), ((0x00f9f7174295d9c2ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x00f11cc8e0e70bcbULL) & ((1 << 28) - 1)), ((0x00f11cc8e0e70bcbULL) >> 28), ((0x00e5dc689974e7ddULL) & ((1 << 28) - 1)), ((0x00e5dc689974e7ddULL) >> 28), ((0x0014e409f9ee5870ULL) & ((1 << 28) - 1)), ((0x0014e409f9ee5870ULL) >> 28), ((0x00826e6689acbd63ULL) & ((1 << 28) - 1)), ((0x00826e6689acbd63ULL) >> 28), ((0x008a6f4e3d895d88ULL) & ((1 << 28) - 1)), ((0x008a6f4e3d895d88ULL) >> 28), ((0x00b26a8da41fd4adULL) & ((1 << 28) - 1)), ((0x00b26a8da41fd4adULL) >> 28), ((0x000fb7723f83efd7ULL) & ((1 << 28) - 1)), ((0x000fb7723f83efd7ULL) >> 28), ((0x009c749db0a5f6c3ULL) & ((1 << 28) - 1)), ((0x009c749db0a5f6c3ULL) >> 28)}}


                                                                        },
            {{{((0x002389319450f9baULL) & ((1 << 28) - 1)), ((0x002389319450f9baULL) >> 28), ((0x003677f31aa1250aULL) & ((1 << 28) - 1)), ((0x003677f31aa1250aULL) >> 28), ((0x0092c3db642f38cbULL) & ((1 << 28) - 1)), ((0x0092c3db642f38cbULL) >> 28), ((0x00f8b64c0dfc9773ULL) & ((1 << 28) - 1)), ((0x00f8b64c0dfc9773ULL) >> 28), ((0x00cd49fe3505b795ULL) & ((1 << 28) - 1)), ((0x00cd49fe3505b795ULL) >> 28), ((0x0068105a4090a510ULL) & ((1 << 28) - 1)), ((0x0068105a4090a510ULL) >> 28), ((0x00df0ba2072a8bb6ULL) & ((1 << 28) - 1)), ((0x00df0ba2072a8bb6ULL) >> 28), ((0x00eb396143afd8beULL) & ((1 << 28) - 1)), ((0x00eb396143afd8beULL) >> 28)}}


                                                                        },
            {{{((0x00a0d4ecfb24cdffULL) & ((1 << 28) - 1)), ((0x00a0d4ecfb24cdffULL) >> 28), ((0x00ddaf8008ba6479ULL) & ((1 << 28) - 1)), ((0x00ddaf8008ba6479ULL) >> 28), ((0x00f0b3e36d4b0f44ULL) & ((1 << 28) - 1)), ((0x00f0b3e36d4b0f44ULL) >> 28), ((0x003734bd3af1f146ULL) & ((1 << 28) - 1)), ((0x003734bd3af1f146ULL) >> 28), ((0x00b87e2efc75527eULL) & ((1 << 28) - 1)), ((0x00b87e2efc75527eULL) >> 28), ((0x00d230df55ddab50ULL) & ((1 << 28) - 1)), ((0x00d230df55ddab50ULL) >> 28), ((0x002613257ae56c1dULL) & ((1 << 28) - 1)), ((0x002613257ae56c1dULL) >> 28), ((0x00bc0946d135934dULL) & ((1 << 28) - 1)), ((0x00bc0946d135934dULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x00468711bd994651ULL) & ((1 << 28) - 1)), ((0x00468711bd994651ULL) >> 28), ((0x0033108fa67561bfULL) & ((1 << 28) - 1)), ((0x0033108fa67561bfULL) >> 28), ((0x0089d760192a54b4ULL) & ((1 << 28) - 1)), ((0x0089d760192a54b4ULL) >> 28), ((0x00adc433de9f1871ULL) & ((1 << 28) - 1)), ((0x00adc433de9f1871ULL) >> 28), ((0x000467d05f36e050ULL) & ((1 << 28) - 1)), ((0x000467d05f36e050ULL) >> 28), ((0x007847e0f0579f7fULL) & ((1 << 28) - 1)), ((0x007847e0f0579f7fULL) >> 28), ((0x00a2314ad320052dULL) & ((1 << 28) - 1)), ((0x00a2314ad320052dULL) >> 28), ((0x00b3a93649f0b243ULL) & ((1 << 28) - 1)), ((0x00b3a93649f0b243ULL) >> 28)}}


                                                                        },
            {{{((0x0067f8f0c4fe26c9ULL) & ((1 << 28) - 1)), ((0x0067f8f0c4fe26c9ULL) >> 28), ((0x0079c4a3cc8f67b9ULL) & ((1 << 28) - 1)), ((0x0079c4a3cc8f67b9ULL) >> 28), ((0x0082b1e62f23550dULL) & ((1 << 28) - 1)), ((0x0082b1e62f23550dULL) >> 28), ((0x00f2d409caefd7f5ULL) & ((1 << 28) - 1)), ((0x00f2d409caefd7f5ULL) >> 28), ((0x0080e67dcdb26e81ULL) & ((1 << 28) - 1)), ((0x0080e67dcdb26e81ULL) >> 28), ((0x0087ae993ea1f98aULL) & ((1 << 28) - 1)), ((0x0087ae993ea1f98aULL) >> 28), ((0x00aa108becf61d03ULL) & ((1 << 28) - 1)), ((0x00aa108becf61d03ULL) >> 28), ((0x001acf11efb608a3ULL) & ((1 << 28) - 1)), ((0x001acf11efb608a3ULL) >> 28)}}


                                                                        },
            {{{((0x008225febbab50d9ULL) & ((1 << 28) - 1)), ((0x008225febbab50d9ULL) >> 28), ((0x00f3b605e4dd2083ULL) & ((1 << 28) - 1)), ((0x00f3b605e4dd2083ULL) >> 28), ((0x00a32b28189e23d2ULL) & ((1 << 28) - 1)), ((0x00a32b28189e23d2ULL) >> 28), ((0x00d507e5e5eb4c97ULL) & ((1 << 28) - 1)), ((0x00d507e5e5eb4c97ULL) >> 28), ((0x005a1a84e302821fULL) & ((1 << 28) - 1)), ((0x005a1a84e302821fULL) >> 28), ((0x0006f54c1c5f08c7ULL) & ((1 << 28) - 1)), ((0x0006f54c1c5f08c7ULL) >> 28), ((0x00a347c8cb2843f0ULL) & ((1 << 28) - 1)), ((0x00a347c8cb2843f0ULL) >> 28), ((0x0009f73e9544bfa5ULL) & ((1 << 28) - 1)), ((0x0009f73e9544bfa5ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x006c59c9ae744185ULL) & ((1 << 28) - 1)), ((0x006c59c9ae744185ULL) >> 28), ((0x009fc32f1b4282cdULL) & ((1 << 28) - 1)), ((0x009fc32f1b4282cdULL) >> 28), ((0x004d6348ca59b1acULL) & ((1 << 28) - 1)), ((0x004d6348ca59b1acULL) >> 28), ((0x00105376881be067ULL) & ((1 << 28) - 1)), ((0x00105376881be067ULL) >> 28), ((0x00af4096013147dcULL) & ((1 << 28) - 1)), ((0x00af4096013147dcULL) >> 28), ((0x004abfb5a5cb3124ULL) & ((1 << 28) - 1)), ((0x004abfb5a5cb3124ULL) >> 28), ((0x000d2a7f8626c354ULL) & ((1 << 28) - 1)), ((0x000d2a7f8626c354ULL) >> 28), ((0x009c6ed568e07431ULL) & ((1 << 28) - 1)), ((0x009c6ed568e07431ULL) >> 28)}}


                                                                        },
            {{{((0x00e828333c297f8bULL) & ((1 << 28) - 1)), ((0x00e828333c297f8bULL) >> 28), ((0x009ef3cf8c3f7e1fULL) & ((1 << 28) - 1)), ((0x009ef3cf8c3f7e1fULL) >> 28), ((0x00ab45f8fff31cb9ULL) & ((1 << 28) - 1)), ((0x00ab45f8fff31cb9ULL) >> 28), ((0x00c8b4178cb0b013ULL) & ((1 << 28) - 1)), ((0x00c8b4178cb0b013ULL) >> 28), ((0x00d0c50dd3260a3fULL) & ((1 << 28) - 1)), ((0x00d0c50dd3260a3fULL) >> 28), ((0x0097126ac257f5bcULL) & ((1 << 28) - 1)), ((0x0097126ac257f5bcULL) >> 28), ((0x0042376cc90c705aULL) & ((1 << 28) - 1)), ((0x0042376cc90c705aULL) >> 28), ((0x001d96fdb4a1071eULL) & ((1 << 28) - 1)), ((0x001d96fdb4a1071eULL) >> 28)}}


                                                                        },
            {{{((0x00542d44d89ee1a8ULL) & ((1 << 28) - 1)), ((0x00542d44d89ee1a8ULL) >> 28), ((0x00306642e0442d98ULL) & ((1 << 28) - 1)), ((0x00306642e0442d98ULL) >> 28), ((0x0090853872b87338ULL) & ((1 << 28) - 1)), ((0x0090853872b87338ULL) >> 28), ((0x002362cbf22dc044ULL) & ((1 << 28) - 1)), ((0x002362cbf22dc044ULL) >> 28), ((0x002c222adff663b8ULL) & ((1 << 28) - 1)), ((0x002c222adff663b8ULL) >> 28), ((0x0067c924495fcb79ULL) & ((1 << 28) - 1)), ((0x0067c924495fcb79ULL) >> 28), ((0x000e621d983c977cULL) & ((1 << 28) - 1)), ((0x000e621d983c977cULL) >> 28), ((0x00df77a9eccb66fbULL) & ((1 << 28) - 1)), ((0x00df77a9eccb66fbULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x002809e4bbf1814aULL) & ((1 << 28) - 1)), ((0x002809e4bbf1814aULL) >> 28), ((0x00b9e854f9fafb32ULL) & ((1 << 28) - 1)), ((0x00b9e854f9fafb32ULL) >> 28), ((0x00d35e67c10f7a67ULL) & ((1 << 28) - 1)), ((0x00d35e67c10f7a67ULL) >> 28), ((0x008f1bcb76e748cfULL) & ((1 << 28) - 1)), ((0x008f1bcb76e748cfULL) >> 28), ((0x004224d9515687d2ULL) & ((1 << 28) - 1)), ((0x004224d9515687d2ULL) >> 28), ((0x005ba0b774e620c4ULL) & ((1 << 28) - 1)), ((0x005ba0b774e620c4ULL) >> 28), ((0x00b5e57db5d54119ULL) & ((1 << 28) - 1)), ((0x00b5e57db5d54119ULL) >> 28), ((0x00e15babe5683282ULL) & ((1 << 28) - 1)), ((0x00e15babe5683282ULL) >> 28)}}


                                                                        },
            {{{((0x00832d02369b482cULL) & ((1 << 28) - 1)), ((0x00832d02369b482cULL) >> 28), ((0x00cba52ff0d93450ULL) & ((1 << 28) - 1)), ((0x00cba52ff0d93450ULL) >> 28), ((0x003fa9c908d554dbULL) & ((1 << 28) - 1)), ((0x003fa9c908d554dbULL) >> 28), ((0x008d1e357b54122fULL) & ((1 << 28) - 1)), ((0x008d1e357b54122fULL) >> 28), ((0x00abd91c2dc950c6ULL) & ((1 << 28) - 1)), ((0x00abd91c2dc950c6ULL) >> 28), ((0x007eff1df4c0ec69ULL) & ((1 << 28) - 1)), ((0x007eff1df4c0ec69ULL) >> 28), ((0x003f6aeb13fb2d31ULL) & ((1 << 28) - 1)), ((0x003f6aeb13fb2d31ULL) >> 28), ((0x00002d6179fc5b2cULL) & ((1 << 28) - 1)), ((0x00002d6179fc5b2cULL) >> 28)}}


                                                                        },
            {{{((0x0046c9eda81c9c89ULL) & ((1 << 28) - 1)), ((0x0046c9eda81c9c89ULL) >> 28), ((0x00b60cb71c8f62fcULL) & ((1 << 28) - 1)), ((0x00b60cb71c8f62fcULL) >> 28), ((0x0022f5a683baa558ULL) & ((1 << 28) - 1)), ((0x0022f5a683baa558ULL) >> 28), ((0x00f87319fccdf997ULL) & ((1 << 28) - 1)), ((0x00f87319fccdf997ULL) >> 28), ((0x009ca09b51ce6a22ULL) & ((1 << 28) - 1)), ((0x009ca09b51ce6a22ULL) >> 28), ((0x005b12baf4af7d77ULL) & ((1 << 28) - 1)), ((0x005b12baf4af7d77ULL) >> 28), ((0x008a46524a1e33e2ULL) & ((1 << 28) - 1)), ((0x008a46524a1e33e2ULL) >> 28), ((0x00035a77e988be0dULL) & ((1 << 28) - 1)), ((0x00035a77e988be0dULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x00a7efe46a7dbe2fULL) & ((1 << 28) - 1)), ((0x00a7efe46a7dbe2fULL) >> 28), ((0x002f66fd55014fe7ULL) & ((1 << 28) - 1)), ((0x002f66fd55014fe7ULL) >> 28), ((0x006a428afa1ff026ULL) & ((1 << 28) - 1)), ((0x006a428afa1ff026ULL) >> 28), ((0x0056caaa9604ab72ULL) & ((1 << 28) - 1)), ((0x0056caaa9604ab72ULL) >> 28), ((0x0033f3bcd7fac8aeULL) & ((1 << 28) - 1)), ((0x0033f3bcd7fac8aeULL) >> 28), ((0x00ccb1aa01c86764ULL) & ((1 << 28) - 1)), ((0x00ccb1aa01c86764ULL) >> 28), ((0x00158d1edf13bf40ULL) & ((1 << 28) - 1)), ((0x00158d1edf13bf40ULL) >> 28), ((0x009848ee76fcf3b4ULL) & ((1 << 28) - 1)), ((0x009848ee76fcf3b4ULL) >> 28)}}


                                                                        },
            {{{((0x00a9e7730a819691ULL) & ((1 << 28) - 1)), ((0x00a9e7730a819691ULL) >> 28), ((0x00d9cc73c4992b70ULL) & ((1 << 28) - 1)), ((0x00d9cc73c4992b70ULL) >> 28), ((0x00e299bde067de5aULL) & ((1 << 28) - 1)), ((0x00e299bde067de5aULL) >> 28), ((0x008c314eb705192aULL) & ((1 << 28) - 1)), ((0x008c314eb705192aULL) >> 28), ((0x00e7226f17e8a3ccULL) & ((1 << 28) - 1)), ((0x00e7226f17e8a3ccULL) >> 28), ((0x0029dfd956e65a47ULL) & ((1 << 28) - 1)), ((0x0029dfd956e65a47ULL) >> 28), ((0x0053a8e839073b12ULL) & ((1 << 28) - 1)), ((0x0053a8e839073b12ULL) >> 28), ((0x006f942b2ab1597eULL) & ((1 << 28) - 1)), ((0x006f942b2ab1597eULL) >> 28)}}


                                                                        },
            {{{((0x001c3d780ecd5e39ULL) & ((1 << 28) - 1)), ((0x001c3d780ecd5e39ULL) >> 28), ((0x0094f247fbdcc5feULL) & ((1 << 28) - 1)), ((0x0094f247fbdcc5feULL) >> 28), ((0x00d5c786fd527764ULL) & ((1 << 28) - 1)), ((0x00d5c786fd527764ULL) >> 28), ((0x00b6f4da74f0db2aULL) & ((1 << 28) - 1)), ((0x00b6f4da74f0db2aULL) >> 28), ((0x0080f1f8badcd5fcULL) & ((1 << 28) - 1)), ((0x0080f1f8badcd5fcULL) >> 28), ((0x00f36a373ad2e23bULL) & ((1 << 28) - 1)), ((0x00f36a373ad2e23bULL) >> 28), ((0x00f804f9f4343bf2ULL) & ((1 << 28) - 1)), ((0x00f804f9f4343bf2ULL) >> 28), ((0x00d1af40ec623982ULL) & ((1 << 28) - 1)), ((0x00d1af40ec623982ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x0082aeace5f1b144ULL) & ((1 << 28) - 1)), ((0x0082aeace5f1b144ULL) >> 28), ((0x00f68b3108cf4dd3ULL) & ((1 << 28) - 1)), ((0x00f68b3108cf4dd3ULL) >> 28), ((0x00634af01dde3020ULL) & ((1 << 28) - 1)), ((0x00634af01dde3020ULL) >> 28), ((0x000beab5df5c2355ULL) & ((1 << 28) - 1)), ((0x000beab5df5c2355ULL) >> 28), ((0x00e8b790d1b49b0bULL) & ((1 << 28) - 1)), ((0x00e8b790d1b49b0bULL) >> 28), ((0x00e48d15854e36f4ULL) & ((1 << 28) - 1)), ((0x00e48d15854e36f4ULL) >> 28), ((0x0040ab2d95f3db9fULL) & ((1 << 28) - 1)), ((0x0040ab2d95f3db9fULL) >> 28), ((0x002711c4ed9e899aULL) & ((1 << 28) - 1)), ((0x002711c4ed9e899aULL) >> 28)}}


                                                                        },
            {{{((0x0039343746531ebeULL) & ((1 << 28) - 1)), ((0x0039343746531ebeULL) >> 28), ((0x00c8509d835d429dULL) & ((1 << 28) - 1)), ((0x00c8509d835d429dULL) >> 28), ((0x00e79eceff6b0018ULL) & ((1 << 28) - 1)), ((0x00e79eceff6b0018ULL) >> 28), ((0x004abfd31e8efce5ULL) & ((1 << 28) - 1)), ((0x004abfd31e8efce5ULL) >> 28), ((0x007bbfaaa1e20210ULL) & ((1 << 28) - 1)), ((0x007bbfaaa1e20210ULL) >> 28), ((0x00e3be89c193e179ULL) & ((1 << 28) - 1)), ((0x00e3be89c193e179ULL) >> 28), ((0x001c420f4c31d585ULL) & ((1 << 28) - 1)), ((0x001c420f4c31d585ULL) >> 28), ((0x00f414a315bef5aeULL) & ((1 << 28) - 1)), ((0x00f414a315bef5aeULL) >> 28)}}


                                                                        },
            {{{((0x007c296a24990df8ULL) & ((1 << 28) - 1)), ((0x007c296a24990df8ULL) >> 28), ((0x00d5d07525a75588ULL) & ((1 << 28) - 1)), ((0x00d5d07525a75588ULL) >> 28), ((0x00dd8e113e94b7e7ULL) & ((1 << 28) - 1)), ((0x00dd8e113e94b7e7ULL) >> 28), ((0x007bbc58febe0cc8ULL) & ((1 << 28) - 1)), ((0x007bbc58febe0cc8ULL) >> 28), ((0x0029f51af9bfcad3ULL) & ((1 << 28) - 1)), ((0x0029f51af9bfcad3ULL) >> 28), ((0x007e9311ec7ab6f3ULL) & ((1 << 28) - 1)), ((0x007e9311ec7ab6f3ULL) >> 28), ((0x009a884de1676343ULL) & ((1 << 28) - 1)), ((0x009a884de1676343ULL) >> 28), ((0x0050d5f2dce84be9ULL) & ((1 << 28) - 1)), ((0x0050d5f2dce84be9ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x005fa020cca2450aULL) & ((1 << 28) - 1)), ((0x005fa020cca2450aULL) >> 28), ((0x00491c29db6416d8ULL) & ((1 << 28) - 1)), ((0x00491c29db6416d8ULL) >> 28), ((0x0037cefe3f9f9a85ULL) & ((1 << 28) - 1)), ((0x0037cefe3f9f9a85ULL) >> 28), ((0x003d405230647066ULL) & ((1 << 28) - 1)), ((0x003d405230647066ULL) >> 28), ((0x0049e835f0fdbe89ULL) & ((1 << 28) - 1)), ((0x0049e835f0fdbe89ULL) >> 28), ((0x00feb78ac1a0815cULL) & ((1 << 28) - 1)), ((0x00feb78ac1a0815cULL) >> 28), ((0x00828e4b32dc9724ULL) & ((1 << 28) - 1)), ((0x00828e4b32dc9724ULL) >> 28), ((0x00db84f2dc8d6fd4ULL) & ((1 << 28) - 1)), ((0x00db84f2dc8d6fd4ULL) >> 28)}}


                                                                        },
            {{{((0x0098cddc8b39549aULL) & ((1 << 28) - 1)), ((0x0098cddc8b39549aULL) >> 28), ((0x006da37e3b05d22cULL) & ((1 << 28) - 1)), ((0x006da37e3b05d22cULL) >> 28), ((0x00ce633cfd4eb3cbULL) & ((1 << 28) - 1)), ((0x00ce633cfd4eb3cbULL) >> 28), ((0x00fda288ef526acdULL) & ((1 << 28) - 1)), ((0x00fda288ef526acdULL) >> 28), ((0x0025338878c5d30aULL) & ((1 << 28) - 1)), ((0x0025338878c5d30aULL) >> 28), ((0x00f34438c4e5a1b4ULL) & ((1 << 28) - 1)), ((0x00f34438c4e5a1b4ULL) >> 28), ((0x00584efea7c310f1ULL) & ((1 << 28) - 1)), ((0x00584efea7c310f1ULL) >> 28), ((0x0041a551f1b660adULL) & ((1 << 28) - 1)), ((0x0041a551f1b660adULL) >> 28)}}


                                                                        },
            {{{((0x00d7f7a8fbd6437aULL) & ((1 << 28) - 1)), ((0x00d7f7a8fbd6437aULL) >> 28), ((0x0062872413bf3753ULL) & ((1 << 28) - 1)), ((0x0062872413bf3753ULL) >> 28), ((0x00ad4bbcb43c584bULL) & ((1 << 28) - 1)), ((0x00ad4bbcb43c584bULL) >> 28), ((0x007fe49be601d7e3ULL) & ((1 << 28) - 1)), ((0x007fe49be601d7e3ULL) >> 28), ((0x0077c659789babf4ULL) & ((1 << 28) - 1)), ((0x0077c659789babf4ULL) >> 28), ((0x00eb45fcb06a741bULL) & ((1 << 28) - 1)), ((0x00eb45fcb06a741bULL) >> 28), ((0x005ce244913f9708ULL) & ((1 << 28) - 1)), ((0x005ce244913f9708ULL) >> 28), ((0x0088426401736326ULL) & ((1 << 28) - 1)), ((0x0088426401736326ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x007bf562ca768d7cULL) & ((1 << 28) - 1)), ((0x007bf562ca768d7cULL) >> 28), ((0x006c1f3a174e387cULL) & ((1 << 28) - 1)), ((0x006c1f3a174e387cULL) >> 28), ((0x00f024b447fee939ULL) & ((1 << 28) - 1)), ((0x00f024b447fee939ULL) >> 28), ((0x007e7af75f01143fULL) & ((1 << 28) - 1)), ((0x007e7af75f01143fULL) >> 28), ((0x003adb70b4eed89dULL) & ((1 << 28) - 1)), ((0x003adb70b4eed89dULL) >> 28), ((0x00e43544021ad79aULL) & ((1 << 28) - 1)), ((0x00e43544021ad79aULL) >> 28), ((0x0091f7f7042011f6ULL) & ((1 << 28) - 1)), ((0x0091f7f7042011f6ULL) >> 28), ((0x0093c1a1ee3a0ddcULL) & ((1 << 28) - 1)), ((0x0093c1a1ee3a0ddcULL) >> 28)}}


                                                                        },
            {{{((0x00a0b68ec1eb72d2ULL) & ((1 << 28) - 1)), ((0x00a0b68ec1eb72d2ULL) >> 28), ((0x002c03235c0d45a0ULL) & ((1 << 28) - 1)), ((0x002c03235c0d45a0ULL) >> 28), ((0x00553627323fe8c5ULL) & ((1 << 28) - 1)), ((0x00553627323fe8c5ULL) >> 28), ((0x006186e94b17af94ULL) & ((1 << 28) - 1)), ((0x006186e94b17af94ULL) >> 28), ((0x00a9906196e29f14ULL) & ((1 << 28) - 1)), ((0x00a9906196e29f14ULL) >> 28), ((0x0025b3aee6567733ULL) & ((1 << 28) - 1)), ((0x0025b3aee6567733ULL) >> 28), ((0x007e0dd840080517ULL) & ((1 << 28) - 1)), ((0x007e0dd840080517ULL) >> 28), ((0x0018eb5801a4ba93ULL) & ((1 << 28) - 1)), ((0x0018eb5801a4ba93ULL) >> 28)}}


                                                                        },
            {{{((0x00d7fe7017bf6a40ULL) & ((1 << 28) - 1)), ((0x00d7fe7017bf6a40ULL) >> 28), ((0x006e3f0624be0c42ULL) & ((1 << 28) - 1)), ((0x006e3f0624be0c42ULL) >> 28), ((0x00ffbba205358245ULL) & ((1 << 28) - 1)), ((0x00ffbba205358245ULL) >> 28), ((0x00f9fc2cf8194239ULL) & ((1 << 28) - 1)), ((0x00f9fc2cf8194239ULL) >> 28), ((0x008d93b37bf15b4eULL) & ((1 << 28) - 1)), ((0x008d93b37bf15b4eULL) >> 28), ((0x006ddf2e38be8e95ULL) & ((1 << 28) - 1)), ((0x006ddf2e38be8e95ULL) >> 28), ((0x002b6e79bf5fcff9ULL) & ((1 << 28) - 1)), ((0x002b6e79bf5fcff9ULL) >> 28), ((0x00ab355da425e2deULL) & ((1 << 28) - 1)), ((0x00ab355da425e2deULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x00938f97e20be973ULL) & ((1 << 28) - 1)), ((0x00938f97e20be973ULL) >> 28), ((0x0099141a36aaf306ULL) & ((1 << 28) - 1)), ((0x0099141a36aaf306ULL) >> 28), ((0x0057b0ca29e545a1ULL) & ((1 << 28) - 1)), ((0x0057b0ca29e545a1ULL) >> 28), ((0x0085db571f9fbc13ULL) & ((1 << 28) - 1)), ((0x0085db571f9fbc13ULL) >> 28), ((0x008b333c554b4693ULL) & ((1 << 28) - 1)), ((0x008b333c554b4693ULL) >> 28), ((0x0043ab6ef3e241cbULL) & ((1 << 28) - 1)), ((0x0043ab6ef3e241cbULL) >> 28), ((0x0054fb20aa1e5c70ULL) & ((1 << 28) - 1)), ((0x0054fb20aa1e5c70ULL) >> 28), ((0x00be0ff852760adfULL) & ((1 << 28) - 1)), ((0x00be0ff852760adfULL) >> 28)}}


                                                                        },
            {{{((0x003973d8938971d6ULL) & ((1 << 28) - 1)), ((0x003973d8938971d6ULL) >> 28), ((0x002aca26fa80c1f5ULL) & ((1 << 28) - 1)), ((0x002aca26fa80c1f5ULL) >> 28), ((0x00108af1faa6b513ULL) & ((1 << 28) - 1)), ((0x00108af1faa6b513ULL) >> 28), ((0x00daae275d7924e6ULL) & ((1 << 28) - 1)), ((0x00daae275d7924e6ULL) >> 28), ((0x0053634ced721308ULL) & ((1 << 28) - 1)), ((0x0053634ced721308ULL) >> 28), ((0x00d2355fe0bbd443ULL) & ((1 << 28) - 1)), ((0x00d2355fe0bbd443ULL) >> 28), ((0x00357612b2d22095ULL) & ((1 << 28) - 1)), ((0x00357612b2d22095ULL) >> 28), ((0x00f9bb9dd4136cf3ULL) & ((1 << 28) - 1)), ((0x00f9bb9dd4136cf3ULL) >> 28)}}


                                                                        },
            {{{((0x002bff12cf5e03a5ULL) & ((1 << 28) - 1)), ((0x002bff12cf5e03a5ULL) >> 28), ((0x001bdb1fa8a19cf8ULL) & ((1 << 28) - 1)), ((0x001bdb1fa8a19cf8ULL) >> 28), ((0x00c91c6793f84d39ULL) & ((1 << 28) - 1)), ((0x00c91c6793f84d39ULL) >> 28), ((0x00f869f1b2eba9afULL) & ((1 << 28) - 1)), ((0x00f869f1b2eba9afULL) >> 28), ((0x0059bc547dc3236bULL) & ((1 << 28) - 1)), ((0x0059bc547dc3236bULL) >> 28), ((0x00d91611d6d38689ULL) & ((1 << 28) - 1)), ((0x00d91611d6d38689ULL) >> 28), ((0x00e062daaa2c0214ULL) & ((1 << 28) - 1)), ((0x00e062daaa2c0214ULL) >> 28), ((0x00ed3c047cc2bc82ULL) & ((1 << 28) - 1)), ((0x00ed3c047cc2bc82ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x000050d70c32b31aULL) & ((1 << 28) - 1)), ((0x000050d70c32b31aULL) >> 28), ((0x001939d576d437b3ULL) & ((1 << 28) - 1)), ((0x001939d576d437b3ULL) >> 28), ((0x00d709e598bf9fe6ULL) & ((1 << 28) - 1)), ((0x00d709e598bf9fe6ULL) >> 28), ((0x00a885b34bd2ee9eULL) & ((1 << 28) - 1)), ((0x00a885b34bd2ee9eULL) >> 28), ((0x00dd4b5c08ab1a50ULL) & ((1 << 28) - 1)), ((0x00dd4b5c08ab1a50ULL) >> 28), ((0x0091bebd50b55639ULL) & ((1 << 28) - 1)), ((0x0091bebd50b55639ULL) >> 28), ((0x00cf79ff64acdbc6ULL) & ((1 << 28) - 1)), ((0x00cf79ff64acdbc6ULL) >> 28), ((0x006067a39d826336ULL) & ((1 << 28) - 1)), ((0x006067a39d826336ULL) >> 28)}}


                                                                        },
            {{{((0x0062dd0fb31be374ULL) & ((1 << 28) - 1)), ((0x0062dd0fb31be374ULL) >> 28), ((0x00fcc96b84c8e727ULL) & ((1 << 28) - 1)), ((0x00fcc96b84c8e727ULL) >> 28), ((0x003f64f1375e6ae3ULL) & ((1 << 28) - 1)), ((0x003f64f1375e6ae3ULL) >> 28), ((0x0057d9b6dd1af004ULL) & ((1 << 28) - 1)), ((0x0057d9b6dd1af004ULL) >> 28), ((0x00d6a167b1103c7bULL) & ((1 << 28) - 1)), ((0x00d6a167b1103c7bULL) >> 28), ((0x00dd28f3180fb537ULL) & ((1 << 28) - 1)), ((0x00dd28f3180fb537ULL) >> 28), ((0x004ff27ad7167128ULL) & ((1 << 28) - 1)), ((0x004ff27ad7167128ULL) >> 28), ((0x008934c33461f2acULL) & ((1 << 28) - 1)), ((0x008934c33461f2acULL) >> 28)}}


                                                                        },
            {{{((0x0065b472b7900043ULL) & ((1 << 28) - 1)), ((0x0065b472b7900043ULL) >> 28), ((0x00ba7efd2ff1064bULL) & ((1 << 28) - 1)), ((0x00ba7efd2ff1064bULL) >> 28), ((0x000b67d6c4c3020fULL) & ((1 << 28) - 1)), ((0x000b67d6c4c3020fULL) >> 28), ((0x0012d28469f4e46dULL) & ((1 << 28) - 1)), ((0x0012d28469f4e46dULL) >> 28), ((0x0031c32939703ec7ULL) & ((1 << 28) - 1)), ((0x0031c32939703ec7ULL) >> 28), ((0x00b49f0bce133066ULL) & ((1 << 28) - 1)), ((0x00b49f0bce133066ULL) >> 28), ((0x00f7e10416181d47ULL) & ((1 << 28) - 1)), ((0x00f7e10416181d47ULL) >> 28), ((0x005c90f51867eeccULL) & ((1 << 28) - 1)), ((0x005c90f51867eeccULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x0051207abd179101ULL) & ((1 << 28) - 1)), ((0x0051207abd179101ULL) >> 28), ((0x00fc2a5c20d9c5daULL) & ((1 << 28) - 1)), ((0x00fc2a5c20d9c5daULL) >> 28), ((0x00fb9d5f2701b6dfULL) & ((1 << 28) - 1)), ((0x00fb9d5f2701b6dfULL) >> 28), ((0x002dd040fdea82b8ULL) & ((1 << 28) - 1)), ((0x002dd040fdea82b8ULL) >> 28), ((0x00f163b0738442ffULL) & ((1 << 28) - 1)), ((0x00f163b0738442ffULL) >> 28), ((0x00d9736bd68855b8ULL) & ((1 << 28) - 1)), ((0x00d9736bd68855b8ULL) >> 28), ((0x00e0d8e93005e61cULL) & ((1 << 28) - 1)), ((0x00e0d8e93005e61cULL) >> 28), ((0x00df5a40b3988570ULL) & ((1 << 28) - 1)), ((0x00df5a40b3988570ULL) >> 28)}}


                                                                        },
            {{{((0x0006918f5dfce6dcULL) & ((1 << 28) - 1)), ((0x0006918f5dfce6dcULL) >> 28), ((0x00d4bf1c793c57fbULL) & ((1 << 28) - 1)), ((0x00d4bf1c793c57fbULL) >> 28), ((0x0069a3f649435364ULL) & ((1 << 28) - 1)), ((0x0069a3f649435364ULL) >> 28), ((0x00e89a50e5b0cd6eULL) & ((1 << 28) - 1)), ((0x00e89a50e5b0cd6eULL) >> 28), ((0x00b9f6a237e973afULL) & ((1 << 28) - 1)), ((0x00b9f6a237e973afULL) >> 28), ((0x006d4ed8b104e41dULL) & ((1 << 28) - 1)), ((0x006d4ed8b104e41dULL) >> 28), ((0x00498946a3924cd2ULL) & ((1 << 28) - 1)), ((0x00498946a3924cd2ULL) >> 28), ((0x00c136ec5ac9d4f7ULL) & ((1 << 28) - 1)), ((0x00c136ec5ac9d4f7ULL) >> 28)}}


                                                                        },
            {{{((0x0011a9c290ac5336ULL) & ((1 << 28) - 1)), ((0x0011a9c290ac5336ULL) >> 28), ((0x002b9a2d4a6a6533ULL) & ((1 << 28) - 1)), ((0x002b9a2d4a6a6533ULL) >> 28), ((0x009a8a68c445d937ULL) & ((1 << 28) - 1)), ((0x009a8a68c445d937ULL) >> 28), ((0x00361b27b07e5e5cULL) & ((1 << 28) - 1)), ((0x00361b27b07e5e5cULL) >> 28), ((0x003c043b1755b974ULL) & ((1 << 28) - 1)), ((0x003c043b1755b974ULL) >> 28), ((0x00b7eb66cf1155eeULL) & ((1 << 28) - 1)), ((0x00b7eb66cf1155eeULL) >> 28), ((0x0077af5909eefff2ULL) & ((1 << 28) - 1)), ((0x0077af5909eefff2ULL) >> 28), ((0x0098f609877cc806ULL) & ((1 << 28) - 1)), ((0x0098f609877cc806ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x00ab13af436bf8f4ULL) & ((1 << 28) - 1)), ((0x00ab13af436bf8f4ULL) >> 28), ((0x000bcf0a0dac8574ULL) & ((1 << 28) - 1)), ((0x000bcf0a0dac8574ULL) >> 28), ((0x00d50c864f705045ULL) & ((1 << 28) - 1)), ((0x00d50c864f705045ULL) >> 28), ((0x00c40e611debc842ULL) & ((1 << 28) - 1)), ((0x00c40e611debc842ULL) >> 28), ((0x0085010489bd5caaULL) & ((1 << 28) - 1)), ((0x0085010489bd5caaULL) >> 28), ((0x007c5050acec026fULL) & ((1 << 28) - 1)), ((0x007c5050acec026fULL) >> 28), ((0x00f67d943c8da6d1ULL) & ((1 << 28) - 1)), ((0x00f67d943c8da6d1ULL) >> 28), ((0x00de1da0278074c6ULL) & ((1 << 28) - 1)), ((0x00de1da0278074c6ULL) >> 28)}}


                                                                        },
            {{{((0x00b373076597455fULL) & ((1 << 28) - 1)), ((0x00b373076597455fULL) >> 28), ((0x00e83f1af53ac0f5ULL) & ((1 << 28) - 1)), ((0x00e83f1af53ac0f5ULL) >> 28), ((0x0041f63c01dc6840ULL) & ((1 << 28) - 1)), ((0x0041f63c01dc6840ULL) >> 28), ((0x0097dea19b0c6f4bULL) & ((1 << 28) - 1)), ((0x0097dea19b0c6f4bULL) >> 28), ((0x007f9d63b4c1572cULL) & ((1 << 28) - 1)), ((0x007f9d63b4c1572cULL) >> 28), ((0x00e692d492d0f5f0ULL) & ((1 << 28) - 1)), ((0x00e692d492d0f5f0ULL) >> 28), ((0x00cbcb392e83b4adULL) & ((1 << 28) - 1)), ((0x00cbcb392e83b4adULL) >> 28), ((0x0069c0f39ed9b1a8ULL) & ((1 << 28) - 1)), ((0x0069c0f39ed9b1a8ULL) >> 28)}}


                                                                        },
            {{{((0x00861030012707c9ULL) & ((1 << 28) - 1)), ((0x00861030012707c9ULL) >> 28), ((0x009fbbdc7fd4aafbULL) & ((1 << 28) - 1)), ((0x009fbbdc7fd4aafbULL) >> 28), ((0x008f591d6b554822ULL) & ((1 << 28) - 1)), ((0x008f591d6b554822ULL) >> 28), ((0x00df08a41ea18adeULL) & ((1 << 28) - 1)), ((0x00df08a41ea18adeULL) >> 28), ((0x009d7d83e642abeaULL) & ((1 << 28) - 1)), ((0x009d7d83e642abeaULL) >> 28), ((0x0098c71bda3b78ffULL) & ((1 << 28) - 1)), ((0x0098c71bda3b78ffULL) >> 28), ((0x0022c89e7021f005ULL) & ((1 << 28) - 1)), ((0x0022c89e7021f005ULL) >> 28), ((0x0044d29a3fe1e3c4ULL) & ((1 << 28) - 1)), ((0x0044d29a3fe1e3c4ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x00e748cd7b5c52f2ULL) & ((1 << 28) - 1)), ((0x00e748cd7b5c52f2ULL) >> 28), ((0x00ea9df883f89cc3ULL) & ((1 << 28) - 1)), ((0x00ea9df883f89cc3ULL) >> 28), ((0x0018970df156b6c7ULL) & ((1 << 28) - 1)), ((0x0018970df156b6c7ULL) >> 28), ((0x00c5a46c2a33a847ULL) & ((1 << 28) - 1)), ((0x00c5a46c2a33a847ULL) >> 28), ((0x00cbde395e32aa09ULL) & ((1 << 28) - 1)), ((0x00cbde395e32aa09ULL) >> 28), ((0x0072474ebb423140ULL) & ((1 << 28) - 1)), ((0x0072474ebb423140ULL) >> 28), ((0x00fb00053086a23dULL) & ((1 << 28) - 1)), ((0x00fb00053086a23dULL) >> 28), ((0x001dafcfe22d4e1fULL) & ((1 << 28) - 1)), ((0x001dafcfe22d4e1fULL) >> 28)}}


                                                                        },
            {{{((0x00c903ee6d825540ULL) & ((1 << 28) - 1)), ((0x00c903ee6d825540ULL) >> 28), ((0x00add6c4cf98473eULL) & ((1 << 28) - 1)), ((0x00add6c4cf98473eULL) >> 28), ((0x007636efed4227f1ULL) & ((1 << 28) - 1)), ((0x007636efed4227f1ULL) >> 28), ((0x00905124ae55e772ULL) & ((1 << 28) - 1)), ((0x00905124ae55e772ULL) >> 28), ((0x00e6b38fab12ed53ULL) & ((1 << 28) - 1)), ((0x00e6b38fab12ed53ULL) >> 28), ((0x0045e132b863fe55ULL) & ((1 << 28) - 1)), ((0x0045e132b863fe55ULL) >> 28), ((0x003974662edb366aULL) & ((1 << 28) - 1)), ((0x003974662edb366aULL) >> 28), ((0x00b1787052be8208ULL) & ((1 << 28) - 1)), ((0x00b1787052be8208ULL) >> 28)}}


                                                                        },
            {{{((0x00a614b00d775c7cULL) & ((1 << 28) - 1)), ((0x00a614b00d775c7cULL) >> 28), ((0x00d7c78941cc7754ULL) & ((1 << 28) - 1)), ((0x00d7c78941cc7754ULL) >> 28), ((0x00422dd68b5dabc4ULL) & ((1 << 28) - 1)), ((0x00422dd68b5dabc4ULL) >> 28), ((0x00a6110f0167d28bULL) & ((1 << 28) - 1)), ((0x00a6110f0167d28bULL) >> 28), ((0x00685a309c252886ULL) & ((1 << 28) - 1)), ((0x00685a309c252886ULL) >> 28), ((0x00b439ffd5143660ULL) & ((1 << 28) - 1)), ((0x00b439ffd5143660ULL) >> 28), ((0x003656e29ee7396fULL) & ((1 << 28) - 1)), ((0x003656e29ee7396fULL) >> 28), ((0x00c7c9b9ed5ad854ULL) & ((1 << 28) - 1)), ((0x00c7c9b9ed5ad854ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x0040f7e7c5b37bf2ULL) & ((1 << 28) - 1)), ((0x0040f7e7c5b37bf2ULL) >> 28), ((0x0064e4dc81181bbaULL) & ((1 << 28) - 1)), ((0x0064e4dc81181bbaULL) >> 28), ((0x00a8767ae2a366b6ULL) & ((1 << 28) - 1)), ((0x00a8767ae2a366b6ULL) >> 28), ((0x001496b4f90546f2ULL) & ((1 << 28) - 1)), ((0x001496b4f90546f2ULL) >> 28), ((0x002a28493f860441ULL) & ((1 << 28) - 1)), ((0x002a28493f860441ULL) >> 28), ((0x0021f59513049a3aULL) & ((1 << 28) - 1)), ((0x0021f59513049a3aULL) >> 28), ((0x00852d369a8b7ee3ULL) & ((1 << 28) - 1)), ((0x00852d369a8b7ee3ULL) >> 28), ((0x00dd2e7d8b7d30a9ULL) & ((1 << 28) - 1)), ((0x00dd2e7d8b7d30a9ULL) >> 28)}}


                                                                        },
            {{{((0x00006e34a35d9fbcULL) & ((1 << 28) - 1)), ((0x00006e34a35d9fbcULL) >> 28), ((0x00eee4e48b2f019aULL) & ((1 << 28) - 1)), ((0x00eee4e48b2f019aULL) >> 28), ((0x006b344743003a5fULL) & ((1 << 28) - 1)), ((0x006b344743003a5fULL) >> 28), ((0x00541d514f04a7e3ULL) & ((1 << 28) - 1)), ((0x00541d514f04a7e3ULL) >> 28), ((0x00e81f9ee7647455ULL) & ((1 << 28) - 1)), ((0x00e81f9ee7647455ULL) >> 28), ((0x005e2b916c438f81ULL) & ((1 << 28) - 1)), ((0x005e2b916c438f81ULL) >> 28), ((0x00116f8137b7eff0ULL) & ((1 << 28) - 1)), ((0x00116f8137b7eff0ULL) >> 28), ((0x009bd3decc7039d1ULL) & ((1 << 28) - 1)), ((0x009bd3decc7039d1ULL) >> 28)}}


                                                                        },
            {{{((0x0005d226f434110dULL) & ((1 << 28) - 1)), ((0x0005d226f434110dULL) >> 28), ((0x00af8288b8ef21d5ULL) & ((1 << 28) - 1)), ((0x00af8288b8ef21d5ULL) >> 28), ((0x004a7a52ef181c8cULL) & ((1 << 28) - 1)), ((0x004a7a52ef181c8cULL) >> 28), ((0x00be0b781b4b06deULL) & ((1 << 28) - 1)), ((0x00be0b781b4b06deULL) >> 28), ((0x00e6e3627ded07e1ULL) & ((1 << 28) - 1)), ((0x00e6e3627ded07e1ULL) >> 28), ((0x00e43aa342272b8bULL) & ((1 << 28) - 1)), ((0x00e43aa342272b8bULL) >> 28), ((0x00e86ab424577d84ULL) & ((1 << 28) - 1)), ((0x00e86ab424577d84ULL) >> 28), ((0x00fb292c566e35bbULL) & ((1 << 28) - 1)), ((0x00fb292c566e35bbULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x00334f5303ea1222ULL) & ((1 << 28) - 1)), ((0x00334f5303ea1222ULL) >> 28), ((0x00dfb3dbeb0a5d3eULL) & ((1 << 28) - 1)), ((0x00dfb3dbeb0a5d3eULL) >> 28), ((0x002940d9592335c1ULL) & ((1 << 28) - 1)), ((0x002940d9592335c1ULL) >> 28), ((0x00706a7a63e8938aULL) & ((1 << 28) - 1)), ((0x00706a7a63e8938aULL) >> 28), ((0x005a533558bc4cafULL) & ((1 << 28) - 1)), ((0x005a533558bc4cafULL) >> 28), ((0x00558e33192022a9ULL) & ((1 << 28) - 1)), ((0x00558e33192022a9ULL) >> 28), ((0x00970d9faf74c133ULL) & ((1 << 28) - 1)), ((0x00970d9faf74c133ULL) >> 28), ((0x002979fcb63493caULL) & ((1 << 28) - 1)), ((0x002979fcb63493caULL) >> 28)}}


                                                                        },
            {{{((0x00e38abece3c82abULL) & ((1 << 28) - 1)), ((0x00e38abece3c82abULL) >> 28), ((0x005a51f18a2c7a86ULL) & ((1 << 28) - 1)), ((0x005a51f18a2c7a86ULL) >> 28), ((0x009dafa2e86d592eULL) & ((1 << 28) - 1)), ((0x009dafa2e86d592eULL) >> 28), ((0x00495a62eb688678ULL) & ((1 << 28) - 1)), ((0x00495a62eb688678ULL) >> 28), ((0x00b79df74c0eb212ULL) & ((1 << 28) - 1)), ((0x00b79df74c0eb212ULL) >> 28), ((0x0023e8cc78b75982ULL) & ((1 << 28) - 1)), ((0x0023e8cc78b75982ULL) >> 28), ((0x005998cb91075e13ULL) & ((1 << 28) - 1)), ((0x005998cb91075e13ULL) >> 28), ((0x00735aa9ba61bc76ULL) & ((1 << 28) - 1)), ((0x00735aa9ba61bc76ULL) >> 28)}}


                                                                        },
            {{{((0x00d9f7a82ddbe628ULL) & ((1 << 28) - 1)), ((0x00d9f7a82ddbe628ULL) >> 28), ((0x00a1fc782889ae0fULL) & ((1 << 28) - 1)), ((0x00a1fc782889ae0fULL) >> 28), ((0x0071ffda12d14b66ULL) & ((1 << 28) - 1)), ((0x0071ffda12d14b66ULL) >> 28), ((0x0037cf4eca7fb3d5ULL) & ((1 << 28) - 1)), ((0x0037cf4eca7fb3d5ULL) >> 28), ((0x00c80bc242c58808ULL) & ((1 << 28) - 1)), ((0x00c80bc242c58808ULL) >> 28), ((0x0075bf8c2d08c863ULL) & ((1 << 28) - 1)), ((0x0075bf8c2d08c863ULL) >> 28), ((0x008d41f31afc52a7ULL) & ((1 << 28) - 1)), ((0x008d41f31afc52a7ULL) >> 28), ((0x00197962ecf38741ULL) & ((1 << 28) - 1)), ((0x00197962ecf38741ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x006e9f475cccf2eeULL) & ((1 << 28) - 1)), ((0x006e9f475cccf2eeULL) >> 28), ((0x00454b9cd506430cULL) & ((1 << 28) - 1)), ((0x00454b9cd506430cULL) >> 28), ((0x00224a4fb79ee479ULL) & ((1 << 28) - 1)), ((0x00224a4fb79ee479ULL) >> 28), ((0x0062e3347ef0b5e2ULL) & ((1 << 28) - 1)), ((0x0062e3347ef0b5e2ULL) >> 28), ((0x0034fd2a3512232aULL) & ((1 << 28) - 1)), ((0x0034fd2a3512232aULL) >> 28), ((0x00b8b3cb0f457046ULL) & ((1 << 28) - 1)), ((0x00b8b3cb0f457046ULL) >> 28), ((0x00eb20165daa38ecULL) & ((1 << 28) - 1)), ((0x00eb20165daa38ecULL) >> 28), ((0x00128eebc2d9c0f7ULL) & ((1 << 28) - 1)), ((0x00128eebc2d9c0f7ULL) >> 28)}}


                                                                        },
            {{{((0x00bfc5fa1e4ea21fULL) & ((1 << 28) - 1)), ((0x00bfc5fa1e4ea21fULL) >> 28), ((0x00c21d7b6bb892e6ULL) & ((1 << 28) - 1)), ((0x00c21d7b6bb892e6ULL) >> 28), ((0x00cf043f3acf0291ULL) & ((1 << 28) - 1)), ((0x00cf043f3acf0291ULL) >> 28), ((0x00c13f2f849b3c90ULL) & ((1 << 28) - 1)), ((0x00c13f2f849b3c90ULL) >> 28), ((0x00d1a97ebef10891ULL) & ((1 << 28) - 1)), ((0x00d1a97ebef10891ULL) >> 28), ((0x0061e130a445e7feULL) & ((1 << 28) - 1)), ((0x0061e130a445e7feULL) >> 28), ((0x0019513fdedbf22bULL) & ((1 << 28) - 1)), ((0x0019513fdedbf22bULL) >> 28), ((0x001d60c813bff841ULL) & ((1 << 28) - 1)), ((0x001d60c813bff841ULL) >> 28)}}


                                                                        },
            {{{((0x0019561c7fcf0213ULL) & ((1 << 28) - 1)), ((0x0019561c7fcf0213ULL) >> 28), ((0x00e3dca6843ebd77ULL) & ((1 << 28) - 1)), ((0x00e3dca6843ebd77ULL) >> 28), ((0x0068ea95b9ca920eULL) & ((1 << 28) - 1)), ((0x0068ea95b9ca920eULL) >> 28), ((0x009bdfb70f253595ULL) & ((1 << 28) - 1)), ((0x009bdfb70f253595ULL) >> 28), ((0x00c68f59186aa02aULL) & ((1 << 28) - 1)), ((0x00c68f59186aa02aULL) >> 28), ((0x005aee1cca1c3039ULL) & ((1 << 28) - 1)), ((0x005aee1cca1c3039ULL) >> 28), ((0x00ab79a8a937a1ceULL) & ((1 << 28) - 1)), ((0x00ab79a8a937a1ceULL) >> 28), ((0x00b9a0e549959e6fULL) & ((1 << 28) - 1)), ((0x00b9a0e549959e6fULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x00c79e0b6d97dfbdULL) & ((1 << 28) - 1)), ((0x00c79e0b6d97dfbdULL) >> 28), ((0x00917c71fd2bc6e8ULL) & ((1 << 28) - 1)), ((0x00917c71fd2bc6e8ULL) >> 28), ((0x00db7529ccfb63d8ULL) & ((1 << 28) - 1)), ((0x00db7529ccfb63d8ULL) >> 28), ((0x00be5be957f17866ULL) & ((1 << 28) - 1)), ((0x00be5be957f17866ULL) >> 28), ((0x00a9e11fdc2cdac1ULL) & ((1 << 28) - 1)), ((0x00a9e11fdc2cdac1ULL) >> 28), ((0x007b91a8e1f44443ULL) & ((1 << 28) - 1)), ((0x007b91a8e1f44443ULL) >> 28), ((0x00a3065e4057d80fULL) & ((1 << 28) - 1)), ((0x00a3065e4057d80fULL) >> 28), ((0x004825f5b8d5f6d4ULL) & ((1 << 28) - 1)), ((0x004825f5b8d5f6d4ULL) >> 28)}}


                                                                        },
            {{{((0x003e4964fa8a8fc8ULL) & ((1 << 28) - 1)), ((0x003e4964fa8a8fc8ULL) >> 28), ((0x00f6a1cdbcf41689ULL) & ((1 << 28) - 1)), ((0x00f6a1cdbcf41689ULL) >> 28), ((0x00943cb18fe7fda7ULL) & ((1 << 28) - 1)), ((0x00943cb18fe7fda7ULL) >> 28), ((0x00606dafbf34440aULL) & ((1 << 28) - 1)), ((0x00606dafbf34440aULL) >> 28), ((0x005d37a86399c789ULL) & ((1 << 28) - 1)), ((0x005d37a86399c789ULL) >> 28), ((0x00e79a2a69417403ULL) & ((1 << 28) - 1)), ((0x00e79a2a69417403ULL) >> 28), ((0x00fe34f7e68b8866ULL) & ((1 << 28) - 1)), ((0x00fe34f7e68b8866ULL) >> 28), ((0x0011f448ed2df10eULL) & ((1 << 28) - 1)), ((0x0011f448ed2df10eULL) >> 28)}}


                                                                        },
            {{{((0x00f1f57efcc1fcc4ULL) & ((1 << 28) - 1)), ((0x00f1f57efcc1fcc4ULL) >> 28), ((0x00513679117de154ULL) & ((1 << 28) - 1)), ((0x00513679117de154ULL) >> 28), ((0x002e5b5b7c86d8c3ULL) & ((1 << 28) - 1)), ((0x002e5b5b7c86d8c3ULL) >> 28), ((0x009f6486561f9cfbULL) & ((1 << 28) - 1)), ((0x009f6486561f9cfbULL) >> 28), ((0x00169e74b0170cf7ULL) & ((1 << 28) - 1)), ((0x00169e74b0170cf7ULL) >> 28), ((0x00900205af4af696ULL) & ((1 << 28) - 1)), ((0x00900205af4af696ULL) >> 28), ((0x006acfddb77853f3ULL) & ((1 << 28) - 1)), ((0x006acfddb77853f3ULL) >> 28), ((0x00df184c90f31068ULL) & ((1 << 28) - 1)), ((0x00df184c90f31068ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x00b37396c3320791ULL) & ((1 << 28) - 1)), ((0x00b37396c3320791ULL) >> 28), ((0x00fc7b67175c5783ULL) & ((1 << 28) - 1)), ((0x00fc7b67175c5783ULL) >> 28), ((0x00c36d2cd73ecc38ULL) & ((1 << 28) - 1)), ((0x00c36d2cd73ecc38ULL) >> 28), ((0x0080ebcc0b328fc5ULL) & ((1 << 28) - 1)), ((0x0080ebcc0b328fc5ULL) >> 28), ((0x0043a5b22b35d35dULL) & ((1 << 28) - 1)), ((0x0043a5b22b35d35dULL) >> 28), ((0x00466c9f1713c9daULL) & ((1 << 28) - 1)), ((0x00466c9f1713c9daULL) >> 28), ((0x0026ad346dcaa8daULL) & ((1 << 28) - 1)), ((0x0026ad346dcaa8daULL) >> 28), ((0x007c684e701183a6ULL) & ((1 << 28) - 1)), ((0x007c684e701183a6ULL) >> 28)}}


                                                                        },
            {{{((0x00fd579ffb691713ULL) & ((1 << 28) - 1)), ((0x00fd579ffb691713ULL) >> 28), ((0x00b76af4f81c412dULL) & ((1 << 28) - 1)), ((0x00b76af4f81c412dULL) >> 28), ((0x00f239de96110f82ULL) & ((1 << 28) - 1)), ((0x00f239de96110f82ULL) >> 28), ((0x00e965fb437f0306ULL) & ((1 << 28) - 1)), ((0x00e965fb437f0306ULL) >> 28), ((0x00ca7e9436900921ULL) & ((1 << 28) - 1)), ((0x00ca7e9436900921ULL) >> 28), ((0x00e487f1325fa24aULL) & ((1 << 28) - 1)), ((0x00e487f1325fa24aULL) >> 28), ((0x00633907de476380ULL) & ((1 << 28) - 1)), ((0x00633907de476380ULL) >> 28), ((0x00721c62ac5b8ea0ULL) & ((1 << 28) - 1)), ((0x00721c62ac5b8ea0ULL) >> 28)}}


                                                                        },
            {{{((0x00c0d54e542eb4f9ULL) & ((1 << 28) - 1)), ((0x00c0d54e542eb4f9ULL) >> 28), ((0x004ed657171c8dcfULL) & ((1 << 28) - 1)), ((0x004ed657171c8dcfULL) >> 28), ((0x00b743a4f7c2a39bULL) & ((1 << 28) - 1)), ((0x00b743a4f7c2a39bULL) >> 28), ((0x00fd9f93ed6cc567ULL) & ((1 << 28) - 1)), ((0x00fd9f93ed6cc567ULL) >> 28), ((0x00307fae3113e58bULL) & ((1 << 28) - 1)), ((0x00307fae3113e58bULL) >> 28), ((0x0058aa577c93c319ULL) & ((1 << 28) - 1)), ((0x0058aa577c93c319ULL) >> 28), ((0x00d254556f35b346ULL) & ((1 << 28) - 1)), ((0x00d254556f35b346ULL) >> 28), ((0x00491aada2203f0dULL) & ((1 << 28) - 1)), ((0x00491aada2203f0dULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x00dff3103786ff34ULL) & ((1 << 28) - 1)), ((0x00dff3103786ff34ULL) >> 28), ((0x000144553b1f20c3ULL) & ((1 << 28) - 1)), ((0x000144553b1f20c3ULL) >> 28), ((0x0095613baeb930e4ULL) & ((1 << 28) - 1)), ((0x0095613baeb930e4ULL) >> 28), ((0x00098058275ea5d4ULL) & ((1 << 28) - 1)), ((0x00098058275ea5d4ULL) >> 28), ((0x007cd1402b046756ULL) & ((1 << 28) - 1)), ((0x007cd1402b046756ULL) >> 28), ((0x0074d74e4d58aee3ULL) & ((1 << 28) - 1)), ((0x0074d74e4d58aee3ULL) >> 28), ((0x005f93fc343ff69bULL) & ((1 << 28) - 1)), ((0x005f93fc343ff69bULL) >> 28), ((0x00873df17296b3b0ULL) & ((1 << 28) - 1)), ((0x00873df17296b3b0ULL) >> 28)}}


                                                                        },
            {{{((0x00c4a1fb48635413ULL) & ((1 << 28) - 1)), ((0x00c4a1fb48635413ULL) >> 28), ((0x00b5dd54423ad59fULL) & ((1 << 28) - 1)), ((0x00b5dd54423ad59fULL) >> 28), ((0x009ff5d53fd24a88ULL) & ((1 << 28) - 1)), ((0x009ff5d53fd24a88ULL) >> 28), ((0x003c98d267fc06a7ULL) & ((1 << 28) - 1)), ((0x003c98d267fc06a7ULL) >> 28), ((0x002db7cb20013641ULL) & ((1 << 28) - 1)), ((0x002db7cb20013641ULL) >> 28), ((0x00bd1d6716e191f2ULL) & ((1 << 28) - 1)), ((0x00bd1d6716e191f2ULL) >> 28), ((0x006dbc8b29094241ULL) & ((1 << 28) - 1)), ((0x006dbc8b29094241ULL) >> 28), ((0x0044bbf233dafa2cULL) & ((1 << 28) - 1)), ((0x0044bbf233dafa2cULL) >> 28)}}


                                                                        },
            {{{((0x0055838d41f531e6ULL) & ((1 << 28) - 1)), ((0x0055838d41f531e6ULL) >> 28), ((0x00bf6a2dd03c81b2ULL) & ((1 << 28) - 1)), ((0x00bf6a2dd03c81b2ULL) >> 28), ((0x005827a061c4839eULL) & ((1 << 28) - 1)), ((0x005827a061c4839eULL) >> 28), ((0x0000de2cbb36aac3ULL) & ((1 << 28) - 1)), ((0x0000de2cbb36aac3ULL) >> 28), ((0x002efa29d9717478ULL) & ((1 << 28) - 1)), ((0x002efa29d9717478ULL) >> 28), ((0x00f9e928cc8a77baULL) & ((1 << 28) - 1)), ((0x00f9e928cc8a77baULL) >> 28), ((0x00c134b458def9efULL) & ((1 << 28) - 1)), ((0x00c134b458def9efULL) >> 28), ((0x00958a182223fc48ULL) & ((1 << 28) - 1)), ((0x00958a182223fc48ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x000a9ee23c06881fULL) & ((1 << 28) - 1)), ((0x000a9ee23c06881fULL) >> 28), ((0x002c727d3d871945ULL) & ((1 << 28) - 1)), ((0x002c727d3d871945ULL) >> 28), ((0x00f47d971512d24aULL) & ((1 << 28) - 1)), ((0x00f47d971512d24aULL) >> 28), ((0x00671e816f9ef31aULL) & ((1 << 28) - 1)), ((0x00671e816f9ef31aULL) >> 28), ((0x00883af2cfaad673ULL) & ((1 << 28) - 1)), ((0x00883af2cfaad673ULL) >> 28), ((0x00601f98583d6c9aULL) & ((1 << 28) - 1)), ((0x00601f98583d6c9aULL) >> 28), ((0x00b435f5adc79655ULL) & ((1 << 28) - 1)), ((0x00b435f5adc79655ULL) >> 28), ((0x00ad87b71c04bff2ULL) & ((1 << 28) - 1)), ((0x00ad87b71c04bff2ULL) >> 28)}}


                                                                        },
            {{{((0x007860d99db787cfULL) & ((1 << 28) - 1)), ((0x007860d99db787cfULL) >> 28), ((0x00fda8983018f4a8ULL) & ((1 << 28) - 1)), ((0x00fda8983018f4a8ULL) >> 28), ((0x008c8866bac4743cULL) & ((1 << 28) - 1)), ((0x008c8866bac4743cULL) >> 28), ((0x00ef471f84c82a3fULL) & ((1 << 28) - 1)), ((0x00ef471f84c82a3fULL) >> 28), ((0x00abea5976d3b8e7ULL) & ((1 << 28) - 1)), ((0x00abea5976d3b8e7ULL) >> 28), ((0x00714882896cd015ULL) & ((1 << 28) - 1)), ((0x00714882896cd015ULL) >> 28), ((0x00b49fae584ddac5ULL) & ((1 << 28) - 1)), ((0x00b49fae584ddac5ULL) >> 28), ((0x008e33a1a0b69c81ULL) & ((1 << 28) - 1)), ((0x008e33a1a0b69c81ULL) >> 28)}}


                                                                        },
            {{{((0x007b6ee2c9e8a9ecULL) & ((1 << 28) - 1)), ((0x007b6ee2c9e8a9ecULL) >> 28), ((0x002455dbbd89d622ULL) & ((1 << 28) - 1)), ((0x002455dbbd89d622ULL) >> 28), ((0x006490cf4eaab038ULL) & ((1 << 28) - 1)), ((0x006490cf4eaab038ULL) >> 28), ((0x00d925f6c3081561ULL) & ((1 << 28) - 1)), ((0x00d925f6c3081561ULL) >> 28), ((0x00153b3047de7382ULL) & ((1 << 28) - 1)), ((0x00153b3047de7382ULL) >> 28), ((0x003b421f8bdceb6fULL) & ((1 << 28) - 1)), ((0x003b421f8bdceb6fULL) >> 28), ((0x00761a4a5049da78ULL) & ((1 << 28) - 1)), ((0x00761a4a5049da78ULL) >> 28), ((0x00980348c5202433ULL) & ((1 << 28) - 1)), ((0x00980348c5202433ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x007f8a43da97dd5cULL) & ((1 << 28) - 1)), ((0x007f8a43da97dd5cULL) >> 28), ((0x00058539c800fc7bULL) & ((1 << 28) - 1)), ((0x00058539c800fc7bULL) >> 28), ((0x0040f3cf5a28414aULL) & ((1 << 28) - 1)), ((0x0040f3cf5a28414aULL) >> 28), ((0x00d68dd0d95283d6ULL) & ((1 << 28) - 1)), ((0x00d68dd0d95283d6ULL) >> 28), ((0x004adce9da90146eULL) & ((1 << 28) - 1)), ((0x004adce9da90146eULL) >> 28), ((0x00befa41c7d4f908ULL) & ((1 << 28) - 1)), ((0x00befa41c7d4f908ULL) >> 28), ((0x007603bc2e3c3060ULL) & ((1 << 28) - 1)), ((0x007603bc2e3c3060ULL) >> 28), ((0x00bdf360ab3545dbULL) & ((1 << 28) - 1)), ((0x00bdf360ab3545dbULL) >> 28)}}


                                                                        },
            {{{((0x00eebfd4e2312cc3ULL) & ((1 << 28) - 1)), ((0x00eebfd4e2312cc3ULL) >> 28), ((0x00474b2564e4fc8cULL) & ((1 << 28) - 1)), ((0x00474b2564e4fc8cULL) >> 28), ((0x003303ef14b1da9bULL) & ((1 << 28) - 1)), ((0x003303ef14b1da9bULL) >> 28), ((0x003c93e0e66beb1dULL) & ((1 << 28) - 1)), ((0x003c93e0e66beb1dULL) >> 28), ((0x0013619b0566925aULL) & ((1 << 28) - 1)), ((0x0013619b0566925aULL) >> 28), ((0x008817c24d901bf3ULL) & ((1 << 28) - 1)), ((0x008817c24d901bf3ULL) >> 28), ((0x00b62bd8898d218bULL) & ((1 << 28) - 1)), ((0x00b62bd8898d218bULL) >> 28), ((0x0075a7716f1e88a2ULL) & ((1 << 28) - 1)), ((0x0075a7716f1e88a2ULL) >> 28)}}


                                                                        },
            {{{((0x0009218da1e6890fULL) & ((1 << 28) - 1)), ((0x0009218da1e6890fULL) >> 28), ((0x0026907f5fd02575ULL) & ((1 << 28) - 1)), ((0x0026907f5fd02575ULL) >> 28), ((0x004dabed5f19d605ULL) & ((1 << 28) - 1)), ((0x004dabed5f19d605ULL) >> 28), ((0x003abf181870249dULL) & ((1 << 28) - 1)), ((0x003abf181870249dULL) >> 28), ((0x00b52fd048cc92c4ULL) & ((1 << 28) - 1)), ((0x00b52fd048cc92c4ULL) >> 28), ((0x00b6dd51e415a5c5ULL) & ((1 << 28) - 1)), ((0x00b6dd51e415a5c5ULL) >> 28), ((0x00d9eb82bd2b4014ULL) & ((1 << 28) - 1)), ((0x00d9eb82bd2b4014ULL) >> 28), ((0x002c865a43b46b43ULL) & ((1 << 28) - 1)), ((0x002c865a43b46b43ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x0070047189452f4cULL) & ((1 << 28) - 1)), ((0x0070047189452f4cULL) >> 28), ((0x00f7ad12e1ce78d5ULL) & ((1 << 28) - 1)), ((0x00f7ad12e1ce78d5ULL) >> 28), ((0x00af1ba51ec44a8bULL) & ((1 << 28) - 1)), ((0x00af1ba51ec44a8bULL) >> 28), ((0x005f39f63e667cd6ULL) & ((1 << 28) - 1)), ((0x005f39f63e667cd6ULL) >> 28), ((0x00058eac4648425eULL) & ((1 << 28) - 1)), ((0x00058eac4648425eULL) >> 28), ((0x00d7fdab42bea03bULL) & ((1 << 28) - 1)), ((0x00d7fdab42bea03bULL) >> 28), ((0x0028576a5688de15ULL) & ((1 << 28) - 1)), ((0x0028576a5688de15ULL) >> 28), ((0x00af973209e77c10ULL) & ((1 << 28) - 1)), ((0x00af973209e77c10ULL) >> 28)}}


                                                                        },
            {{{((0x00c338b915d8fef0ULL) & ((1 << 28) - 1)), ((0x00c338b915d8fef0ULL) >> 28), ((0x00a893292045c39aULL) & ((1 << 28) - 1)), ((0x00a893292045c39aULL) >> 28), ((0x0028ab4f2eba6887ULL) & ((1 << 28) - 1)), ((0x0028ab4f2eba6887ULL) >> 28), ((0x0060743cb519fd61ULL) & ((1 << 28) - 1)), ((0x0060743cb519fd61ULL) >> 28), ((0x0006213964093ac0ULL) & ((1 << 28) - 1)), ((0x0006213964093ac0ULL) >> 28), ((0x007c0b7a43f6266dULL) & ((1 << 28) - 1)), ((0x007c0b7a43f6266dULL) >> 28), ((0x008e3557c4fa5bdaULL) & ((1 << 28) - 1)), ((0x008e3557c4fa5bdaULL) >> 28), ((0x002da976de7b8d9dULL) & ((1 << 28) - 1)), ((0x002da976de7b8d9dULL) >> 28)}}


                                                                        },
            {{{((0x0048729f8a8b6dcdULL) & ((1 << 28) - 1)), ((0x0048729f8a8b6dcdULL) >> 28), ((0x00fe23b85cc4d323ULL) & ((1 << 28) - 1)), ((0x00fe23b85cc4d323ULL) >> 28), ((0x00e7384d16e4db0eULL) & ((1 << 28) - 1)), ((0x00e7384d16e4db0eULL) >> 28), ((0x004a423970678942ULL) & ((1 << 28) - 1)), ((0x004a423970678942ULL) >> 28), ((0x00ec0b763345d4baULL) & ((1 << 28) - 1)), ((0x00ec0b763345d4baULL) >> 28), ((0x00c477b9f99ed721ULL) & ((1 << 28) - 1)), ((0x00c477b9f99ed721ULL) >> 28), ((0x00c29dad3777b230ULL) & ((1 << 28) - 1)), ((0x00c29dad3777b230ULL) >> 28), ((0x001c517b466f7df6ULL) & ((1 << 28) - 1)), ((0x001c517b466f7df6ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x006366c380f7b574ULL) & ((1 << 28) - 1)), ((0x006366c380f7b574ULL) >> 28), ((0x001c7d1f09ff0438ULL) & ((1 << 28) - 1)), ((0x001c7d1f09ff0438ULL) >> 28), ((0x003e20a7301f5b22ULL) & ((1 << 28) - 1)), ((0x003e20a7301f5b22ULL) >> 28), ((0x00d3efb1916d28f6ULL) & ((1 << 28) - 1)), ((0x00d3efb1916d28f6ULL) >> 28), ((0x0049f4f81060ce83ULL) & ((1 << 28) - 1)), ((0x0049f4f81060ce83ULL) >> 28), ((0x00c69d91ea43ced1ULL) & ((1 << 28) - 1)), ((0x00c69d91ea43ced1ULL) >> 28), ((0x002b6f3e5cd269edULL) & ((1 << 28) - 1)), ((0x002b6f3e5cd269edULL) >> 28), ((0x005b0fb22ce9ec65ULL) & ((1 << 28) - 1)), ((0x005b0fb22ce9ec65ULL) >> 28)}}


                                                                        },
            {{{((0x00aa2261022d883fULL) & ((1 << 28) - 1)), ((0x00aa2261022d883fULL) >> 28), ((0x00ebcca4548010acULL) & ((1 << 28) - 1)), ((0x00ebcca4548010acULL) >> 28), ((0x002528512e28a437ULL) & ((1 << 28) - 1)), ((0x002528512e28a437ULL) >> 28), ((0x0070ca7676b66082ULL) & ((1 << 28) - 1)), ((0x0070ca7676b66082ULL) >> 28), ((0x0084bda170f7c6d3ULL) & ((1 << 28) - 1)), ((0x0084bda170f7c6d3ULL) >> 28), ((0x00581b4747c9b8bbULL) & ((1 << 28) - 1)), ((0x00581b4747c9b8bbULL) >> 28), ((0x005c96a01061c7e2ULL) & ((1 << 28) - 1)), ((0x005c96a01061c7e2ULL) >> 28), ((0x00fb7c4a362b5273ULL) & ((1 << 28) - 1)), ((0x00fb7c4a362b5273ULL) >> 28)}}


                                                                        },
            {{{((0x00c30020eb512d02ULL) & ((1 << 28) - 1)), ((0x00c30020eb512d02ULL) >> 28), ((0x0060f288283a4d26ULL) & ((1 << 28) - 1)), ((0x0060f288283a4d26ULL) >> 28), ((0x00b7ed13becde260ULL) & ((1 << 28) - 1)), ((0x00b7ed13becde260ULL) >> 28), ((0x0075ebb74220f6e9ULL) & ((1 << 28) - 1)), ((0x0075ebb74220f6e9ULL) >> 28), ((0x00701079fcfe8a1fULL) & ((1 << 28) - 1)), ((0x00701079fcfe8a1fULL) >> 28), ((0x001c28fcdff58938ULL) & ((1 << 28) - 1)), ((0x001c28fcdff58938ULL) >> 28), ((0x002e4544b8f4df6bULL) & ((1 << 28) - 1)), ((0x002e4544b8f4df6bULL) >> 28), ((0x0060c5bc4f1a7d73ULL) & ((1 << 28) - 1)), ((0x0060c5bc4f1a7d73ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x00ae307cf069f701ULL) & ((1 << 28) - 1)), ((0x00ae307cf069f701ULL) >> 28), ((0x005859f222dd618bULL) & ((1 << 28) - 1)), ((0x005859f222dd618bULL) >> 28), ((0x00212d6c46ec0b0dULL) & ((1 << 28) - 1)), ((0x00212d6c46ec0b0dULL) >> 28), ((0x00a0fe4642afb62dULL) & ((1 << 28) - 1)), ((0x00a0fe4642afb62dULL) >> 28), ((0x00420d8e4a0a8903ULL) & ((1 << 28) - 1)), ((0x00420d8e4a0a8903ULL) >> 28), ((0x00a80ff639bdf7b0ULL) & ((1 << 28) - 1)), ((0x00a80ff639bdf7b0ULL) >> 28), ((0x0019bee1490b5d8eULL) & ((1 << 28) - 1)), ((0x0019bee1490b5d8eULL) >> 28), ((0x007439e4b9c27a86ULL) & ((1 << 28) - 1)), ((0x007439e4b9c27a86ULL) >> 28)}}


                                                                        },
            {{{((0x00a94700032a093fULL) & ((1 << 28) - 1)), ((0x00a94700032a093fULL) >> 28), ((0x0076e96c225216e7ULL) & ((1 << 28) - 1)), ((0x0076e96c225216e7ULL) >> 28), ((0x00a63a4316e45f91ULL) & ((1 << 28) - 1)), ((0x00a63a4316e45f91ULL) >> 28), ((0x007d8bbb4645d3b2ULL) & ((1 << 28) - 1)), ((0x007d8bbb4645d3b2ULL) >> 28), ((0x00340a6ff22793ebULL) & ((1 << 28) - 1)), ((0x00340a6ff22793ebULL) >> 28), ((0x006f935d4572aeb7ULL) & ((1 << 28) - 1)), ((0x006f935d4572aeb7ULL) >> 28), ((0x00b1fb69f00afa28ULL) & ((1 << 28) - 1)), ((0x00b1fb69f00afa28ULL) >> 28), ((0x009e8f3423161ed3ULL) & ((1 << 28) - 1)), ((0x009e8f3423161ed3ULL) >> 28)}}


                                                                        },
            {{{((0x009ef49c6b5ced17ULL) & ((1 << 28) - 1)), ((0x009ef49c6b5ced17ULL) >> 28), ((0x00a555e6269e9f0aULL) & ((1 << 28) - 1)), ((0x00a555e6269e9f0aULL) >> 28), ((0x007e6f1d79ec73b5ULL) & ((1 << 28) - 1)), ((0x007e6f1d79ec73b5ULL) >> 28), ((0x009ac78695a32ac4ULL) & ((1 << 28) - 1)), ((0x009ac78695a32ac4ULL) >> 28), ((0x0001d77fbbcd5682ULL) & ((1 << 28) - 1)), ((0x0001d77fbbcd5682ULL) >> 28), ((0x008cea1fee0aaeedULL) & ((1 << 28) - 1)), ((0x008cea1fee0aaeedULL) >> 28), ((0x00f42bea82a53462ULL) & ((1 << 28) - 1)), ((0x00f42bea82a53462ULL) >> 28), ((0x002e46ab96cafcc9ULL) & ((1 << 28) - 1)), ((0x002e46ab96cafcc9ULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x0051cfcc5885377aULL) & ((1 << 28) - 1)), ((0x0051cfcc5885377aULL) >> 28), ((0x00dce566cb1803caULL) & ((1 << 28) - 1)), ((0x00dce566cb1803caULL) >> 28), ((0x00430c7643f2c7d4ULL) & ((1 << 28) - 1)), ((0x00430c7643f2c7d4ULL) >> 28), ((0x00dce1a1337bdcc0ULL) & ((1 << 28) - 1)), ((0x00dce1a1337bdcc0ULL) >> 28), ((0x0010d5bd7283c128ULL) & ((1 << 28) - 1)), ((0x0010d5bd7283c128ULL) >> 28), ((0x003b1b547f9b46feULL) & ((1 << 28) - 1)), ((0x003b1b547f9b46feULL) >> 28), ((0x000f245e37e770abULL) & ((1 << 28) - 1)), ((0x000f245e37e770abULL) >> 28), ((0x007b72511f022b37ULL) & ((1 << 28) - 1)), ((0x007b72511f022b37ULL) >> 28)}}


                                                                        },
            {{{((0x0060db815bc4786cULL) & ((1 << 28) - 1)), ((0x0060db815bc4786cULL) >> 28), ((0x006fab25beedc434ULL) & ((1 << 28) - 1)), ((0x006fab25beedc434ULL) >> 28), ((0x00c610d06084797cULL) & ((1 << 28) - 1)), ((0x00c610d06084797cULL) >> 28), ((0x000c48f08537bec0ULL) & ((1 << 28) - 1)), ((0x000c48f08537bec0ULL) >> 28), ((0x0031aba51c5b93daULL) & ((1 << 28) - 1)), ((0x0031aba51c5b93daULL) >> 28), ((0x007968fa6e01f347ULL) & ((1 << 28) - 1)), ((0x007968fa6e01f347ULL) >> 28), ((0x0030070da52840c6ULL) & ((1 << 28) - 1)), ((0x0030070da52840c6ULL) >> 28), ((0x00c043c225a4837fULL) & ((1 << 28) - 1)), ((0x00c043c225a4837fULL) >> 28)}}


                                                                        },
            {{{((0x001bcfd00649ee93ULL) & ((1 << 28) - 1)), ((0x001bcfd00649ee93ULL) >> 28), ((0x006dceb47e2a0fd5ULL) & ((1 << 28) - 1)), ((0x006dceb47e2a0fd5ULL) >> 28), ((0x00f2cebda0cf8fd0ULL) & ((1 << 28) - 1)), ((0x00f2cebda0cf8fd0ULL) >> 28), ((0x00b6b9d9d1fbdec3ULL) & ((1 << 28) - 1)), ((0x00b6b9d9d1fbdec3ULL) >> 28), ((0x00815262e6490611ULL) & ((1 << 28) - 1)), ((0x00815262e6490611ULL) >> 28), ((0x00ef7f5ce3176760ULL) & ((1 << 28) - 1)), ((0x00ef7f5ce3176760ULL) >> 28), ((0x00e49cd0c998d58bULL) & ((1 << 28) - 1)), ((0x00e49cd0c998d58bULL) >> 28), ((0x005fc6cc269ba57cULL) & ((1 << 28) - 1)), ((0x005fc6cc269ba57cULL) >> 28)}}


                                                                        },
        }}, {{
            {{{((0x008940211aa0d633ULL) & ((1 << 28) - 1)), ((0x008940211aa0d633ULL) >> 28), ((0x00addae28136571dULL) & ((1 << 28) - 1)), ((0x00addae28136571dULL) >> 28), ((0x00d68fdbba20d673ULL) & ((1 << 28) - 1)), ((0x00d68fdbba20d673ULL) >> 28), ((0x003bc6129bc9e21aULL) & ((1 << 28) - 1)), ((0x003bc6129bc9e21aULL) >> 28), ((0x000346cf184ebe9aULL) & ((1 << 28) - 1)), ((0x000346cf184ebe9aULL) >> 28), ((0x0068774d741ebc7fULL) & ((1 << 28) - 1)), ((0x0068774d741ebc7fULL) >> 28), ((0x0019d5e9e6966557ULL) & ((1 << 28) - 1)), ((0x0019d5e9e6966557ULL) >> 28), ((0x0003cbd7f981b651ULL) & ((1 << 28) - 1)), ((0x0003cbd7f981b651ULL) >> 28)}}


                                                                        },
            {{{((0x004a2902926f8d3fULL) & ((1 << 28) - 1)), ((0x004a2902926f8d3fULL) >> 28), ((0x00ad79b42637ab75ULL) & ((1 << 28) - 1)), ((0x00ad79b42637ab75ULL) >> 28), ((0x0088f60b90f2d4e8ULL) & ((1 << 28) - 1)), ((0x0088f60b90f2d4e8ULL) >> 28), ((0x0030f54ef0e398c4ULL) & ((1 << 28) - 1)), ((0x0030f54ef0e398c4ULL) >> 28), ((0x00021dc9bf99681eULL) & ((1 << 28) - 1)), ((0x00021dc9bf99681eULL) >> 28), ((0x007ebf66fde74ee3ULL) & ((1 << 28) - 1)), ((0x007ebf66fde74ee3ULL) >> 28), ((0x004ade654386e9a4ULL) & ((1 << 28) - 1)), ((0x004ade654386e9a4ULL) >> 28), ((0x00e7485066be4c27ULL) & ((1 << 28) - 1)), ((0x00e7485066be4c27ULL) >> 28)}}


                                                                        },
            {{{((0x00445f1263983be0ULL) & ((1 << 28) - 1)), ((0x00445f1263983be0ULL) >> 28), ((0x004cf371dda45e6aULL) & ((1 << 28) - 1)), ((0x004cf371dda45e6aULL) >> 28), ((0x00744a89d5a310e7ULL) & ((1 << 28) - 1)), ((0x00744a89d5a310e7ULL) >> 28), ((0x001f20ce4f904833ULL) & ((1 << 28) - 1)), ((0x001f20ce4f904833ULL) >> 28), ((0x00e746edebe66e29ULL) & ((1 << 28) - 1)), ((0x00e746edebe66e29ULL) >> 28), ((0x000912ab1f6c153dULL) & ((1 << 28) - 1)), ((0x000912ab1f6c153dULL) >> 28), ((0x00f61d77d9b2444cULL) & ((1 << 28) - 1)), ((0x00f61d77d9b2444cULL) >> 28), ((0x0001499cd6647610ULL) & ((1 << 28) - 1)), ((0x0001499cd6647610ULL) >> 28)}}


                                                                        },
        }}
    }
};
const struct curve448_precomputed_s *curve448_precomputed_base
    = &curve448_precomputed_base_table;

static const niels_t curve448_wnaf_base_table[32] = {
    {{
        {{{((0x00303cda6feea532ULL) & ((1 << 28) - 1)), ((0x00303cda6feea532ULL) >> 28), ((0x00860f1d5a3850e4ULL) & ((1 << 28) - 1)), ((0x00860f1d5a3850e4ULL) >> 28), ((0x00226b9fa4728ccdULL) & ((1 << 28) - 1)), ((0x00226b9fa4728ccdULL) >> 28), ((0x00e822938a0a0c0cULL) & ((1 << 28) - 1)), ((0x00e822938a0a0c0cULL) >> 28), ((0x00263a61c9ea9216ULL) & ((1 << 28) - 1)), ((0x00263a61c9ea9216ULL) >> 28), ((0x001204029321b828ULL) & ((1 << 28) - 1)), ((0x001204029321b828ULL) >> 28), ((0x006a468360983c65ULL) & ((1 << 28) - 1)), ((0x006a468360983c65ULL) >> 28), ((0x0002846f0a782143ULL) & ((1 << 28) - 1)), ((0x0002846f0a782143ULL) >> 28)}}


                                                                    },
        {{{((0x00303cda6feea532ULL) & ((1 << 28) - 1)), ((0x00303cda6feea532ULL) >> 28), ((0x00860f1d5a3850e4ULL) & ((1 << 28) - 1)), ((0x00860f1d5a3850e4ULL) >> 28), ((0x00226b9fa4728ccdULL) & ((1 << 28) - 1)), ((0x00226b9fa4728ccdULL) >> 28), ((0x006822938a0a0c0cULL) & ((1 << 28) - 1)), ((0x006822938a0a0c0cULL) >> 28), ((0x00263a61c9ea9215ULL) & ((1 << 28) - 1)), ((0x00263a61c9ea9215ULL) >> 28), ((0x001204029321b828ULL) & ((1 << 28) - 1)), ((0x001204029321b828ULL) >> 28), ((0x006a468360983c65ULL) & ((1 << 28) - 1)), ((0x006a468360983c65ULL) >> 28), ((0x0082846f0a782143ULL) & ((1 << 28) - 1)), ((0x0082846f0a782143ULL) >> 28)}}


                                                                    },
        {{{((0x00ef8e22b275198dULL) & ((1 << 28) - 1)), ((0x00ef8e22b275198dULL) >> 28), ((0x00b0eb141a0b0e8bULL) & ((1 << 28) - 1)), ((0x00b0eb141a0b0e8bULL) >> 28), ((0x001f6789da3cb38cULL) & ((1 << 28) - 1)), ((0x001f6789da3cb38cULL) >> 28), ((0x006d2ff8ed39073eULL) & ((1 << 28) - 1)), ((0x006d2ff8ed39073eULL) >> 28), ((0x00610bdb69a167f3ULL) & ((1 << 28) - 1)), ((0x00610bdb69a167f3ULL) >> 28), ((0x00571f306c9689b4ULL) & ((1 << 28) - 1)), ((0x00571f306c9689b4ULL) >> 28), ((0x00f557e6f84b2df8ULL) & ((1 << 28) - 1)), ((0x00f557e6f84b2df8ULL) >> 28), ((0x002affd38b2c86dbULL) & ((1 << 28) - 1)), ((0x002affd38b2c86dbULL) >> 28)}}


                                                                    },
    }}, {{
        {{{((0x00cea0fc8d2e88b5ULL) & ((1 << 28) - 1)), ((0x00cea0fc8d2e88b5ULL) >> 28), ((0x00821612d69f1862ULL) & ((1 << 28) - 1)), ((0x00821612d69f1862ULL) >> 28), ((0x0074c283b3e67522ULL) & ((1 << 28) - 1)), ((0x0074c283b3e67522ULL) >> 28), ((0x005a195ba05a876dULL) & ((1 << 28) - 1)), ((0x005a195ba05a876dULL) >> 28), ((0x000cddfe557feea4ULL) & ((1 << 28) - 1)), ((0x000cddfe557feea4ULL) >> 28), ((0x008046c795bcc5e5ULL) & ((1 << 28) - 1)), ((0x008046c795bcc5e5ULL) >> 28), ((0x00540969f4d6e119ULL) & ((1 << 28) - 1)), ((0x00540969f4d6e119ULL) >> 28), ((0x00d27f96d6b143d5ULL) & ((1 << 28) - 1)), ((0x00d27f96d6b143d5ULL) >> 28)}}


                                                                    },
        {{{((0x000c3b1019d474e8ULL) & ((1 << 28) - 1)), ((0x000c3b1019d474e8ULL) >> 28), ((0x00e19533e4952284ULL) & ((1 << 28) - 1)), ((0x00e19533e4952284ULL) >> 28), ((0x00cc9810ba7c920aULL) & ((1 << 28) - 1)), ((0x00cc9810ba7c920aULL) >> 28), ((0x00f103d2785945acULL) & ((1 << 28) - 1)), ((0x00f103d2785945acULL) >> 28), ((0x00bfa5696cc69b34ULL) & ((1 << 28) - 1)), ((0x00bfa5696cc69b34ULL) >> 28), ((0x00a8d3d51e9ca839ULL) & ((1 << 28) - 1)), ((0x00a8d3d51e9ca839ULL) >> 28), ((0x005623cb459586b9ULL) & ((1 << 28) - 1)), ((0x005623cb459586b9ULL) >> 28), ((0x00eae7ce1cd52e9eULL) & ((1 << 28) - 1)), ((0x00eae7ce1cd52e9eULL) >> 28)}}


                                                                    },
        {{{((0x0005a178751dd7d8ULL) & ((1 << 28) - 1)), ((0x0005a178751dd7d8ULL) >> 28), ((0x002cc3844c69c42fULL) & ((1 << 28) - 1)), ((0x002cc3844c69c42fULL) >> 28), ((0x00acbfe5efe10539ULL) & ((1 << 28) - 1)), ((0x00acbfe5efe10539ULL) >> 28), ((0x009c20f43431a65aULL) & ((1 << 28) - 1)), ((0x009c20f43431a65aULL) >> 28), ((0x008435d96374a7b3ULL) & ((1 << 28) - 1)), ((0x008435d96374a7b3ULL) >> 28), ((0x009ee57566877bd3ULL) & ((1 << 28) - 1)), ((0x009ee57566877bd3ULL) >> 28), ((0x0044691725ed4757ULL) & ((1 << 28) - 1)), ((0x0044691725ed4757ULL) >> 28), ((0x001e87bb2fe2c6b2ULL) & ((1 << 28) - 1)), ((0x001e87bb2fe2c6b2ULL) >> 28)}}


                                                                    },
    }}, {{
        {{{((0x000cedc4debf7a04ULL) & ((1 << 28) - 1)), ((0x000cedc4debf7a04ULL) >> 28), ((0x002ffa45000470acULL) & ((1 << 28) - 1)), ((0x002ffa45000470acULL) >> 28), ((0x002e9f9678201915ULL) & ((1 << 28) - 1)), ((0x002e9f9678201915ULL) >> 28), ((0x0017da1208c4fe72ULL) & ((1 << 28) - 1)), ((0x0017da1208c4fe72ULL) >> 28), ((0x007d558cc7d656cbULL) & ((1 << 28) - 1)), ((0x007d558cc7d656cbULL) >> 28), ((0x0037a827287cf289ULL) & ((1 << 28) - 1)), ((0x0037a827287cf289ULL) >> 28), ((0x00142472d3441819ULL) & ((1 << 28) - 1)), ((0x00142472d3441819ULL) >> 28), ((0x009c21f166cf8dd1ULL) & ((1 << 28) - 1)), ((0x009c21f166cf8dd1ULL) >> 28)}}


                                                                    },
        {{{((0x003ef83af164b2f2ULL) & ((1 << 28) - 1)), ((0x003ef83af164b2f2ULL) >> 28), ((0x000949a5a0525d0dULL) & ((1 << 28) - 1)), ((0x000949a5a0525d0dULL) >> 28), ((0x00f4498186cac051ULL) & ((1 << 28) - 1)), ((0x00f4498186cac051ULL) >> 28), ((0x00e77ac09ef126d2ULL) & ((1 << 28) - 1)), ((0x00e77ac09ef126d2ULL) >> 28), ((0x0073ae0b2c9296e9ULL) & ((1 << 28) - 1)), ((0x0073ae0b2c9296e9ULL) >> 28), ((0x001c163f6922e3edULL) & ((1 << 28) - 1)), ((0x001c163f6922e3edULL) >> 28), ((0x0062946159321beaULL) & ((1 << 28) - 1)), ((0x0062946159321beaULL) >> 28), ((0x00cfb79b22990b39ULL) & ((1 << 28) - 1)), ((0x00cfb79b22990b39ULL) >> 28)}}


                                                                    },
        {{{((0x00b001431ca9e654ULL) & ((1 << 28) - 1)), ((0x00b001431ca9e654ULL) >> 28), ((0x002d7e5eabcc9a3aULL) & ((1 << 28) - 1)), ((0x002d7e5eabcc9a3aULL) >> 28), ((0x0052e8114c2f6747ULL) & ((1 << 28) - 1)), ((0x0052e8114c2f6747ULL) >> 28), ((0x0079ac4f94487f92ULL) & ((1 << 28) - 1)), ((0x0079ac4f94487f92ULL) >> 28), ((0x00bffd919b5d749cULL) & ((1 << 28) - 1)), ((0x00bffd919b5d749cULL) >> 28), ((0x00261f92ad15e620ULL) & ((1 << 28) - 1)), ((0x00261f92ad15e620ULL) >> 28), ((0x00718397b7a97895ULL) & ((1 << 28) - 1)), ((0x00718397b7a97895ULL) >> 28), ((0x00c1443e6ebbc0c4ULL) & ((1 << 28) - 1)), ((0x00c1443e6ebbc0c4ULL) >> 28)}}


                                                                    },
    }}, {{
        {{{((0x00eacd90c1e0a049ULL) & ((1 << 28) - 1)), ((0x00eacd90c1e0a049ULL) >> 28), ((0x008977935b149fbeULL) & ((1 << 28) - 1)), ((0x008977935b149fbeULL) >> 28), ((0x0004cb9ba11c93dcULL) & ((1 << 28) - 1)), ((0x0004cb9ba11c93dcULL) >> 28), ((0x009fbd5b3470844dULL) & ((1 << 28) - 1)), ((0x009fbd5b3470844dULL) >> 28), ((0x004bc18c9bfc22cfULL) & ((1 << 28) - 1)), ((0x004bc18c9bfc22cfULL) >> 28), ((0x0057679a991839f3ULL) & ((1 << 28) - 1)), ((0x0057679a991839f3ULL) >> 28), ((0x00ef15b76fb4092eULL) & ((1 << 28) - 1)), ((0x00ef15b76fb4092eULL) >> 28), ((0x0074a5173a225041ULL) & ((1 << 28) - 1)), ((0x0074a5173a225041ULL) >> 28)}}


                                                                    },
        {{{((0x003f5f9d7ec4777bULL) & ((1 << 28) - 1)), ((0x003f5f9d7ec4777bULL) >> 28), ((0x00ab2e733c919c94ULL) & ((1 << 28) - 1)), ((0x00ab2e733c919c94ULL) >> 28), ((0x001bb6c035245ae5ULL) & ((1 << 28) - 1)), ((0x001bb6c035245ae5ULL) >> 28), ((0x00a325a49a883630ULL) & ((1 << 28) - 1)), ((0x00a325a49a883630ULL) >> 28), ((0x0033e9a9ea3cea2fULL) & ((1 << 28) - 1)), ((0x0033e9a9ea3cea2fULL) >> 28), ((0x00e442a1eaa0e844ULL) & ((1 << 28) - 1)), ((0x00e442a1eaa0e844ULL) >> 28), ((0x00b2116d5b0e71b8ULL) & ((1 << 28) - 1)), ((0x00b2116d5b0e71b8ULL) >> 28), ((0x00c16abed6d64047ULL) & ((1 << 28) - 1)), ((0x00c16abed6d64047ULL) >> 28)}}


                                                                    },
        {{{((0x00c560b5ed051165ULL) & ((1 << 28) - 1)), ((0x00c560b5ed051165ULL) >> 28), ((0x001945adc5d65094ULL) & ((1 << 28) - 1)), ((0x001945adc5d65094ULL) >> 28), ((0x00e221865710f910ULL) & ((1 << 28) - 1)), ((0x00e221865710f910ULL) >> 28), ((0x00cc12bc9e9b8cebULL) & ((1 << 28) - 1)), ((0x00cc12bc9e9b8cebULL) >> 28), ((0x004faa9518914e35ULL) & ((1 << 28) - 1)), ((0x004faa9518914e35ULL) >> 28), ((0x0017476d89d42f6dULL) & ((1 << 28) - 1)), ((0x0017476d89d42f6dULL) >> 28), ((0x00b8f637c8fa1c8bULL) & ((1 << 28) - 1)), ((0x00b8f637c8fa1c8bULL) >> 28), ((0x0088c7d2790864b8ULL) & ((1 << 28) - 1)), ((0x0088c7d2790864b8ULL) >> 28)}}


                                                                    },
    }}, {{
        {{{((0x00ef7eafc1c69be6ULL) & ((1 << 28) - 1)), ((0x00ef7eafc1c69be6ULL) >> 28), ((0x0085d3855778fbeaULL) & ((1 << 28) - 1)), ((0x0085d3855778fbeaULL) >> 28), ((0x002c8d5b450cb6f5ULL) & ((1 << 28) - 1)), ((0x002c8d5b450cb6f5ULL) >> 28), ((0x004e77de5e1e7fecULL) & ((1 << 28) - 1)), ((0x004e77de5e1e7fecULL) >> 28), ((0x0047c057893abdedULL) & ((1 << 28) - 1)), ((0x0047c057893abdedULL) >> 28), ((0x001b430b85d51e16ULL) & ((1 << 28) - 1)), ((0x001b430b85d51e16ULL) >> 28), ((0x00965c7b45640c3cULL) & ((1 << 28) - 1)), ((0x00965c7b45640c3cULL) >> 28), ((0x00487b2bb1162b97ULL) & ((1 << 28) - 1)), ((0x00487b2bb1162b97ULL) >> 28)}}


                                                                    },
        {{{((0x0099c73a311beec2ULL) & ((1 << 28) - 1)), ((0x0099c73a311beec2ULL) >> 28), ((0x00a3eff38d8912adULL) & ((1 << 28) - 1)), ((0x00a3eff38d8912adULL) >> 28), ((0x002efa9d1d7e8972ULL) & ((1 << 28) - 1)), ((0x002efa9d1d7e8972ULL) >> 28), ((0x00f717ae1e14d126ULL) & ((1 << 28) - 1)), ((0x00f717ae1e14d126ULL) >> 28), ((0x002833f795850c8bULL) & ((1 << 28) - 1)), ((0x002833f795850c8bULL) >> 28), ((0x0066c12ad71486bdULL) & ((1 << 28) - 1)), ((0x0066c12ad71486bdULL) >> 28), ((0x00ae9889da4820ebULL) & ((1 << 28) - 1)), ((0x00ae9889da4820ebULL) >> 28), ((0x00d6044309555c08ULL) & ((1 << 28) - 1)), ((0x00d6044309555c08ULL) >> 28)}}


                                                                    },
        {{{((0x004b1c5283d15e41ULL) & ((1 << 28) - 1)), ((0x004b1c5283d15e41ULL) >> 28), ((0x00669d8ea308ff75ULL) & ((1 << 28) - 1)), ((0x00669d8ea308ff75ULL) >> 28), ((0x0004390233f762a1ULL) & ((1 << 28) - 1)), ((0x0004390233f762a1ULL) >> 28), ((0x00e1d67b83cb6cecULL) & ((1 << 28) - 1)), ((0x00e1d67b83cb6cecULL) >> 28), ((0x003eebaa964c78b1ULL) & ((1 << 28) - 1)), ((0x003eebaa964c78b1ULL) >> 28), ((0x006b0aff965eb664ULL) & ((1 << 28) - 1)), ((0x006b0aff965eb664ULL) >> 28), ((0x00b313d4470bdc37ULL) & ((1 << 28) - 1)), ((0x00b313d4470bdc37ULL) >> 28), ((0x008814ffcb3cb9d8ULL) & ((1 << 28) - 1)), ((0x008814ffcb3cb9d8ULL) >> 28)}}


                                                                    },
    }}, {{
        {{{((0x009724b8ce68db70ULL) & ((1 << 28) - 1)), ((0x009724b8ce68db70ULL) >> 28), ((0x007678b5ed006f3dULL) & ((1 << 28) - 1)), ((0x007678b5ed006f3dULL) >> 28), ((0x00bdf4b89c0abd73ULL) & ((1 << 28) - 1)), ((0x00bdf4b89c0abd73ULL) >> 28), ((0x00299748e04c7c6dULL) & ((1 << 28) - 1)), ((0x00299748e04c7c6dULL) >> 28), ((0x00ddd86492c3c977ULL) & ((1 << 28) - 1)), ((0x00ddd86492c3c977ULL) >> 28), ((0x00c5a7febfa30a99ULL) & ((1 << 28) - 1)), ((0x00c5a7febfa30a99ULL) >> 28), ((0x00ed84715b4b02bbULL) & ((1 << 28) - 1)), ((0x00ed84715b4b02bbULL) >> 28), ((0x00319568adf70486ULL) & ((1 << 28) - 1)), ((0x00319568adf70486ULL) >> 28)}}


                                                                    },
        {{{((0x0070ff2d864de5bbULL) & ((1 << 28) - 1)), ((0x0070ff2d864de5bbULL) >> 28), ((0x005a37eeb637ee95ULL) & ((1 << 28) - 1)), ((0x005a37eeb637ee95ULL) >> 28), ((0x0033741c258de160ULL) & ((1 << 28) - 1)), ((0x0033741c258de160ULL) >> 28), ((0x00e6ca5cb1988f46ULL) & ((1 << 28) - 1)), ((0x00e6ca5cb1988f46ULL) >> 28), ((0x001ceabd92a24661ULL) & ((1 << 28) - 1)), ((0x001ceabd92a24661ULL) >> 28), ((0x0030957bd500fe40ULL) & ((1 << 28) - 1)), ((0x0030957bd500fe40ULL) >> 28), ((0x001c3362afe912c5ULL) & ((1 << 28) - 1)), ((0x001c3362afe912c5ULL) >> 28), ((0x005187889f678bd2ULL) & ((1 << 28) - 1)), ((0x005187889f678bd2ULL) >> 28)}}


                                                                    },
        {{{((0x0086835fc62bbdc7ULL) & ((1 << 28) - 1)), ((0x0086835fc62bbdc7ULL) >> 28), ((0x009c3516ca4910a1ULL) & ((1 << 28) - 1)), ((0x009c3516ca4910a1ULL) >> 28), ((0x00956c71f8d00783ULL) & ((1 << 28) - 1)), ((0x00956c71f8d00783ULL) >> 28), ((0x0095c78fcf63235fULL) & ((1 << 28) - 1)), ((0x0095c78fcf63235fULL) >> 28), ((0x00fc7ff6ba05c222ULL) & ((1 << 28) - 1)), ((0x00fc7ff6ba05c222ULL) >> 28), ((0x00cdd8b3f8d74a52ULL) & ((1 << 28) - 1)), ((0x00cdd8b3f8d74a52ULL) >> 28), ((0x00ac5ae16de8256eULL) & ((1 << 28) - 1)), ((0x00ac5ae16de8256eULL) >> 28), ((0x00e9d4be8ed48624ULL) & ((1 << 28) - 1)), ((0x00e9d4be8ed48624ULL) >> 28)}}


                                                                    },
    }}, {{
        {{{((0x00c0ce11405df2d8ULL) & ((1 << 28) - 1)), ((0x00c0ce11405df2d8ULL) >> 28), ((0x004e3f37b293d7b6ULL) & ((1 << 28) - 1)), ((0x004e3f37b293d7b6ULL) >> 28), ((0x002410172e1ac6dbULL) & ((1 << 28) - 1)), ((0x002410172e1ac6dbULL) >> 28), ((0x00b8dbff4bf8143dULL) & ((1 << 28) - 1)), ((0x00b8dbff4bf8143dULL) >> 28), ((0x003a7b409d56eb66ULL) & ((1 << 28) - 1)), ((0x003a7b409d56eb66ULL) >> 28), ((0x003e0f6a0dfef9afULL) & ((1 << 28) - 1)), ((0x003e0f6a0dfef9afULL) >> 28), ((0x0081c4e4d3645be1ULL) & ((1 << 28) - 1)), ((0x0081c4e4d3645be1ULL) >> 28), ((0x00ce76076b127623ULL) & ((1 << 28) - 1)), ((0x00ce76076b127623ULL) >> 28)}}


                                                                    },
        {{{((0x00f6ee0f98974239ULL) & ((1 << 28) - 1)), ((0x00f6ee0f98974239ULL) >> 28), ((0x0042d89af07d3a4fULL) & ((1 << 28) - 1)), ((0x0042d89af07d3a4fULL) >> 28), ((0x00846b7fe84346b5ULL) & ((1 << 28) - 1)), ((0x00846b7fe84346b5ULL) >> 28), ((0x006a21fc6a8d39a1ULL) & ((1 << 28) - 1)), ((0x006a21fc6a8d39a1ULL) >> 28), ((0x00ac8bc2541ff2d9ULL) & ((1 << 28) - 1)), ((0x00ac8bc2541ff2d9ULL) >> 28), ((0x006d4e2a77732732ULL) & ((1 << 28) - 1)), ((0x006d4e2a77732732ULL) >> 28), ((0x009a39b694cc3f2fULL) & ((1 << 28) - 1)), ((0x009a39b694cc3f2fULL) >> 28), ((0x0085c0aa2a404c8fULL) & ((1 << 28) - 1)), ((0x0085c0aa2a404c8fULL) >> 28)}}


                                                                    },
        {{{((0x00b261101a218548ULL) & ((1 << 28) - 1)), ((0x00b261101a218548ULL) >> 28), ((0x00c1cae96424277bULL) & ((1 << 28) - 1)), ((0x00c1cae96424277bULL) >> 28), ((0x00869da0a77dd268ULL) & ((1 << 28) - 1)), ((0x00869da0a77dd268ULL) >> 28), ((0x00bc0b09f8ec83eaULL) & ((1 << 28) - 1)), ((0x00bc0b09f8ec83eaULL) >> 28), ((0x00d61027f8e82ba9ULL) & ((1 << 28) - 1)), ((0x00d61027f8e82ba9ULL) >> 28), ((0x00aa4c85999dce67ULL) & ((1 << 28) - 1)), ((0x00aa4c85999dce67ULL) >> 28), ((0x00eac3132b9f3fe1ULL) & ((1 << 28) - 1)), ((0x00eac3132b9f3fe1ULL) >> 28), ((0x00fb9b0cf1c695d2ULL) & ((1 << 28) - 1)), ((0x00fb9b0cf1c695d2ULL) >> 28)}}


                                                                    },
    }}, {{
        {{{((0x0043079295512f0dULL) & ((1 << 28) - 1)), ((0x0043079295512f0dULL) >> 28), ((0x0046a009861758e0ULL) & ((1 << 28) - 1)), ((0x0046a009861758e0ULL) >> 28), ((0x003ee2842a807378ULL) & ((1 << 28) - 1)), ((0x003ee2842a807378ULL) >> 28), ((0x0034cc9d1298e4faULL) & ((1 << 28) - 1)), ((0x0034cc9d1298e4faULL) >> 28), ((0x009744eb4d31b3eeULL) & ((1 << 28) - 1)), ((0x009744eb4d31b3eeULL) >> 28), ((0x00afacec96650cd0ULL) & ((1 << 28) - 1)), ((0x00afacec96650cd0ULL) >> 28), ((0x00ac891b313761aeULL) & ((1 << 28) - 1)), ((0x00ac891b313761aeULL) >> 28), ((0x00e864d6d26e708aULL) & ((1 << 28) - 1)), ((0x00e864d6d26e708aULL) >> 28)}}


                                                                    },
        {{{((0x00a84d7c8a23b491ULL) & ((1 << 28) - 1)), ((0x00a84d7c8a23b491ULL) >> 28), ((0x0088e19aa868b27fULL) & ((1 << 28) - 1)), ((0x0088e19aa868b27fULL) >> 28), ((0x0005986d43e78ce9ULL) & ((1 << 28) - 1)), ((0x0005986d43e78ce9ULL) >> 28), ((0x00f28012f0606d28ULL) & ((1 << 28) - 1)), ((0x00f28012f0606d28ULL) >> 28), ((0x0017ded7e10249b3ULL) & ((1 << 28) - 1)), ((0x0017ded7e10249b3ULL) >> 28), ((0x005ed4084b23af9bULL) & ((1 << 28) - 1)), ((0x005ed4084b23af9bULL) >> 28), ((0x00b9b0a940564472ULL) & ((1 << 28) - 1)), ((0x00b9b0a940564472ULL) >> 28), ((0x00ad9056cceeb1f4ULL) & ((1 << 28) - 1)), ((0x00ad9056cceeb1f4ULL) >> 28)}}


                                                                    },
        {{{((0x00db91b357fe755eULL) & ((1 << 28) - 1)), ((0x00db91b357fe755eULL) >> 28), ((0x00a1aa544b15359cULL) & ((1 << 28) - 1)), ((0x00a1aa544b15359cULL) >> 28), ((0x00af4931a0195574ULL) & ((1 << 28) - 1)), ((0x00af4931a0195574ULL) >> 28), ((0x007686124fe11aefULL) & ((1 << 28) - 1)), ((0x007686124fe11aefULL) >> 28), ((0x00d1ead3c7b9ef7eULL) & ((1 << 28) - 1)), ((0x00d1ead3c7b9ef7eULL) >> 28), ((0x00aaf5fc580f8c15ULL) & ((1 << 28) - 1)), ((0x00aaf5fc580f8c15ULL) >> 28), ((0x00e727be147ee1ecULL) & ((1 << 28) - 1)), ((0x00e727be147ee1ecULL) >> 28), ((0x003c61c1e1577b86ULL) & ((1 << 28) - 1)), ((0x003c61c1e1577b86ULL) >> 28)}}


                                                                    },
    }}, {{
        {{{((0x009d3fca983220cfULL) & ((1 << 28) - 1)), ((0x009d3fca983220cfULL) >> 28), ((0x00cd11acbc853dc4ULL) & ((1 << 28) - 1)), ((0x00cd11acbc853dc4ULL) >> 28), ((0x0017590409d27f1dULL) & ((1 << 28) - 1)), ((0x0017590409d27f1dULL) >> 28), ((0x00d2176698082802ULL) & ((1 << 28) - 1)), ((0x00d2176698082802ULL) >> 28), ((0x00fa01251b2838c8ULL) & ((1 << 28) - 1)), ((0x00fa01251b2838c8ULL) >> 28), ((0x00dd297a0d9b51c6ULL) & ((1 << 28) - 1)), ((0x00dd297a0d9b51c6ULL) >> 28), ((0x00d76c92c045820aULL) & ((1 << 28) - 1)), ((0x00d76c92c045820aULL) >> 28), ((0x00534bc7c46c9033ULL) & ((1 << 28) - 1)), ((0x00534bc7c46c9033ULL) >> 28)}}


                                                                    },
        {{{((0x0080ed9bc9b07338ULL) & ((1 << 28) - 1)), ((0x0080ed9bc9b07338ULL) >> 28), ((0x00fceac7745d2652ULL) & ((1 << 28) - 1)), ((0x00fceac7745d2652ULL) >> 28), ((0x008a9d55f5f2cc69ULL) & ((1 << 28) - 1)), ((0x008a9d55f5f2cc69ULL) >> 28), ((0x0096ce72df301ac5ULL) & ((1 << 28) - 1)), ((0x0096ce72df301ac5ULL) >> 28), ((0x00f53232e7974d87ULL) & ((1 << 28) - 1)), ((0x00f53232e7974d87ULL) >> 28), ((0x0071728c7ae73947ULL) & ((1 << 28) - 1)), ((0x0071728c7ae73947ULL) >> 28), ((0x0090507602570778ULL) & ((1 << 28) - 1)), ((0x0090507602570778ULL) >> 28), ((0x00cb81cfd883b1b2ULL) & ((1 << 28) - 1)), ((0x00cb81cfd883b1b2ULL) >> 28)}}


                                                                    },
        {{{((0x005011aadea373daULL) & ((1 << 28) - 1)), ((0x005011aadea373daULL) >> 28), ((0x003a8578ec896034ULL) & ((1 << 28) - 1)), ((0x003a8578ec896034ULL) >> 28), ((0x00f20a6535fa6d71ULL) & ((1 << 28) - 1)), ((0x00f20a6535fa6d71ULL) >> 28), ((0x005152d31e5a87cfULL) & ((1 << 28) - 1)), ((0x005152d31e5a87cfULL) >> 28), ((0x002bac1c8e68ca31ULL) & ((1 << 28) - 1)), ((0x002bac1c8e68ca31ULL) >> 28), ((0x00b0e323db4c1381ULL) & ((1 << 28) - 1)), ((0x00b0e323db4c1381ULL) >> 28), ((0x00f1d596b7d5ae25ULL) & ((1 << 28) - 1)), ((0x00f1d596b7d5ae25ULL) >> 28), ((0x00eae458097cb4e0ULL) & ((1 << 28) - 1)), ((0x00eae458097cb4e0ULL) >> 28)}}


                                                                    },
    }}, {{
        {{{((0x00920ac80f9b0d21ULL) & ((1 << 28) - 1)), ((0x00920ac80f9b0d21ULL) >> 28), ((0x00f80f7f73401246ULL) & ((1 << 28) - 1)), ((0x00f80f7f73401246ULL) >> 28), ((0x0086d37849b557d6ULL) & ((1 << 28) - 1)), ((0x0086d37849b557d6ULL) >> 28), ((0x0002bd4b317b752eULL) & ((1 << 28) - 1)), ((0x0002bd4b317b752eULL) >> 28), ((0x00b26463993a42bbULL) & ((1 << 28) - 1)), ((0x00b26463993a42bbULL) >> 28), ((0x002070422a73b129ULL) & ((1 << 28) - 1)), ((0x002070422a73b129ULL) >> 28), ((0x00341acaa0380cb3ULL) & ((1 << 28) - 1)), ((0x00341acaa0380cb3ULL) >> 28), ((0x00541914dd66a1b2ULL) & ((1 << 28) - 1)), ((0x00541914dd66a1b2ULL) >> 28)}}


                                                                    },
        {{{((0x00c1513cd66abe8cULL) & ((1 << 28) - 1)), ((0x00c1513cd66abe8cULL) >> 28), ((0x000139e01118944dULL) & ((1 << 28) - 1)), ((0x000139e01118944dULL) >> 28), ((0x0064abbcb8080bbbULL) & ((1 << 28) - 1)), ((0x0064abbcb8080bbbULL) >> 28), ((0x00b3b08202473142ULL) & ((1 << 28) - 1)), ((0x00b3b08202473142ULL) >> 28), ((0x00c629ef25da2403ULL) & ((1 << 28) - 1)), ((0x00c629ef25da2403ULL) >> 28), ((0x00f0aec3310d9b7fULL) & ((1 << 28) - 1)), ((0x00f0aec3310d9b7fULL) >> 28), ((0x0050b2227472d8cdULL) & ((1 << 28) - 1)), ((0x0050b2227472d8cdULL) >> 28), ((0x00f6c8a922d41fb4ULL) & ((1 << 28) - 1)), ((0x00f6c8a922d41fb4ULL) >> 28)}}


                                                                    },
        {{{((0x001075ccf26b7b1fULL) & ((1 << 28) - 1)), ((0x001075ccf26b7b1fULL) >> 28), ((0x00bb6bb213170433ULL) & ((1 << 28) - 1)), ((0x00bb6bb213170433ULL) >> 28), ((0x00e9491ad262da79ULL) & ((1 << 28) - 1)), ((0x00e9491ad262da79ULL) >> 28), ((0x009ef4f48d2d384cULL) & ((1 << 28) - 1)), ((0x009ef4f48d2d384cULL) >> 28), ((0x008992770766f09dULL) & ((1 << 28) - 1)), ((0x008992770766f09dULL) >> 28), ((0x001584396b6b1101ULL) & ((1 << 28) - 1)), ((0x001584396b6b1101ULL) >> 28), ((0x00af3f8676c9feefULL) & ((1 << 28) - 1)), ((0x00af3f8676c9feefULL) >> 28), ((0x0024603c40269118ULL) & ((1 << 28) - 1)), ((0x0024603c40269118ULL) >> 28)}}


                                                                    },
    }}, {{
        {{{((0x009dd7b31319527cULL) & ((1 << 28) - 1)), ((0x009dd7b31319527cULL) >> 28), ((0x001e7ac948d873a9ULL) & ((1 << 28) - 1)), ((0x001e7ac948d873a9ULL) >> 28), ((0x00fa54b46ef9673aULL) & ((1 << 28) - 1)), ((0x00fa54b46ef9673aULL) >> 28), ((0x0066efb8d5b02fe6ULL) & ((1 << 28) - 1)), ((0x0066efb8d5b02fe6ULL) >> 28), ((0x00754b1d3928aeaeULL) & ((1 << 28) - 1)), ((0x00754b1d3928aeaeULL) >> 28), ((0x0004262ac72a6f6bULL) & ((1 << 28) - 1)), ((0x0004262ac72a6f6bULL) >> 28), ((0x0079b7d49a6eb026ULL) & ((1 << 28) - 1)), ((0x0079b7d49a6eb026ULL) >> 28), ((0x003126a753540102ULL) & ((1 << 28) - 1)), ((0x003126a753540102ULL) >> 28)}}


                                                                    },
        {{{((0x009666e24f693947ULL) & ((1 << 28) - 1)), ((0x009666e24f693947ULL) >> 28), ((0x00f714311269d45fULL) & ((1 << 28) - 1)), ((0x00f714311269d45fULL) >> 28), ((0x0010ffac1d0c851cULL) & ((1 << 28) - 1)), ((0x0010ffac1d0c851cULL) >> 28), ((0x0066e80c37363497ULL) & ((1 << 28) - 1)), ((0x0066e80c37363497ULL) >> 28), ((0x00f1f4ad010c60b0ULL) & ((1 << 28) - 1)), ((0x00f1f4ad010c60b0ULL) >> 28), ((0x0015c87408470ff7ULL) & ((1 << 28) - 1)), ((0x0015c87408470ff7ULL) >> 28), ((0x00651d5e9c7766a4ULL) & ((1 << 28) - 1)), ((0x00651d5e9c7766a4ULL) >> 28), ((0x008138819d7116deULL) & ((1 << 28) - 1)), ((0x008138819d7116deULL) >> 28)}}


                                                                    },
        {{{((0x003934b11c57253bULL) & ((1 << 28) - 1)), ((0x003934b11c57253bULL) >> 28), ((0x00ef308edf21f46eULL) & ((1 << 28) - 1)), ((0x00ef308edf21f46eULL) >> 28), ((0x00e54e99c7a16198ULL) & ((1 << 28) - 1)), ((0x00e54e99c7a16198ULL) >> 28), ((0x0080d57135764e63ULL) & ((1 << 28) - 1)), ((0x0080d57135764e63ULL) >> 28), ((0x00751c27b946bc24ULL) & ((1 << 28) - 1)), ((0x00751c27b946bc24ULL) >> 28), ((0x00dd389ce4e9e129ULL) & ((1 << 28) - 1)), ((0x00dd389ce4e9e129ULL) >> 28), ((0x00a1a2bfd1cd84dcULL) & ((1 << 28) - 1)), ((0x00a1a2bfd1cd84dcULL) >> 28), ((0x002fae73e5149b32ULL) & ((1 << 28) - 1)), ((0x002fae73e5149b32ULL) >> 28)}}


                                                                    },
    }}, {{
        {{{((0x00911657dffb4cddULL) & ((1 << 28) - 1)), ((0x00911657dffb4cddULL) >> 28), ((0x00c100b7cc553d06ULL) & ((1 << 28) - 1)), ((0x00c100b7cc553d06ULL) >> 28), ((0x00449d075ec467ccULL) & ((1 << 28) - 1)), ((0x00449d075ec467ccULL) >> 28), ((0x007062100bc64e70ULL) & ((1 << 28) - 1)), ((0x007062100bc64e70ULL) >> 28), ((0x0043cf86f7bd21e7ULL) & ((1 << 28) - 1)), ((0x0043cf86f7bd21e7ULL) >> 28), ((0x00f401dc4b797deaULL) & ((1 << 28) - 1)), ((0x00f401dc4b797deaULL) >> 28), ((0x005224afb2f62e65ULL) & ((1 << 28) - 1)), ((0x005224afb2f62e65ULL) >> 28), ((0x00d1ede3fb5a42beULL) & ((1 << 28) - 1)), ((0x00d1ede3fb5a42beULL) >> 28)}}


                                                                    },
        {{{((0x00f2ba36a41aa144ULL) & ((1 << 28) - 1)), ((0x00f2ba36a41aa144ULL) >> 28), ((0x00a0c22d946ee18fULL) & ((1 << 28) - 1)), ((0x00a0c22d946ee18fULL) >> 28), ((0x008aae8ef9a14f99ULL) & ((1 << 28) - 1)), ((0x008aae8ef9a14f99ULL) >> 28), ((0x00eef4d79b19bb36ULL) & ((1 << 28) - 1)), ((0x00eef4d79b19bb36ULL) >> 28), ((0x008e75ce3d27b1fcULL) & ((1 << 28) - 1)), ((0x008e75ce3d27b1fcULL) >> 28), ((0x00a65daa03b29a27ULL) & ((1 << 28) - 1)), ((0x00a65daa03b29a27ULL) >> 28), ((0x00d9cc83684eb145ULL) & ((1 << 28) - 1)), ((0x00d9cc83684eb145ULL) >> 28), ((0x009e1ed80cc2ed74ULL) & ((1 << 28) - 1)), ((0x009e1ed80cc2ed74ULL) >> 28)}}


                                                                    },
        {{{((0x00bed953d1997988ULL) & ((1 << 28) - 1)), ((0x00bed953d1997988ULL) >> 28), ((0x00b93ed175a24128ULL) & ((1 << 28) - 1)), ((0x00b93ed175a24128ULL) >> 28), ((0x00871c5963fb6365ULL) & ((1 << 28) - 1)), ((0x00871c5963fb6365ULL) >> 28), ((0x00ca2df20014a787ULL) & ((1 << 28) - 1)), ((0x00ca2df20014a787ULL) >> 28), ((0x00f5d9c1d0b34322ULL) & ((1 << 28) - 1)), ((0x00f5d9c1d0b34322ULL) >> 28), ((0x00f6f5942818db0aULL) & ((1 << 28) - 1)), ((0x00f6f5942818db0aULL) >> 28), ((0x004cc091f49c9906ULL) & ((1 << 28) - 1)), ((0x004cc091f49c9906ULL) >> 28), ((0x00e8a188a60bff9fULL) & ((1 << 28) - 1)), ((0x00e8a188a60bff9fULL) >> 28)}}


                                                                    },
    }}, {{
        {{{((0x0032c7762032fae8ULL) & ((1 << 28) - 1)), ((0x0032c7762032fae8ULL) >> 28), ((0x00e4087232e0bc21ULL) & ((1 << 28) - 1)), ((0x00e4087232e0bc21ULL) >> 28), ((0x00f767344b6e8d85ULL) & ((1 << 28) - 1)), ((0x00f767344b6e8d85ULL) >> 28), ((0x00bbf369b76c2aa2ULL) & ((1 << 28) - 1)), ((0x00bbf369b76c2aa2ULL) >> 28), ((0x008a1f46c6e1570cULL) & ((1 << 28) - 1)), ((0x008a1f46c6e1570cULL) >> 28), ((0x001368cd9780369fULL) & ((1 << 28) - 1)), ((0x001368cd9780369fULL) >> 28), ((0x007359a39d079430ULL) & ((1 << 28) - 1)), ((0x007359a39d079430ULL) >> 28), ((0x0003646512921434ULL) & ((1 << 28) - 1)), ((0x0003646512921434ULL) >> 28)}}


                                                                    },
        {{{((0x007c4b47ca7c73e7ULL) & ((1 << 28) - 1)), ((0x007c4b47ca7c73e7ULL) >> 28), ((0x005396221039734bULL) & ((1 << 28) - 1)), ((0x005396221039734bULL) >> 28), ((0x008b64ddf0e45d7eULL) & ((1 << 28) - 1)), ((0x008b64ddf0e45d7eULL) >> 28), ((0x00bfad5af285e6c2ULL) & ((1 << 28) - 1)), ((0x00bfad5af285e6c2ULL) >> 28), ((0x008ec711c5b1a1a8ULL) & ((1 << 28) - 1)), ((0x008ec711c5b1a1a8ULL) >> 28), ((0x00cf663301237f98ULL) & ((1 << 28) - 1)), ((0x00cf663301237f98ULL) >> 28), ((0x00917ee3f1655126ULL) & ((1 << 28) - 1)), ((0x00917ee3f1655126ULL) >> 28), ((0x004152f337efedd8ULL) & ((1 << 28) - 1)), ((0x004152f337efedd8ULL) >> 28)}}


                                                                    },
        {{{((0x0007c7edc9305daaULL) & ((1 << 28) - 1)), ((0x0007c7edc9305daaULL) >> 28), ((0x000a6664f273701cULL) & ((1 << 28) - 1)), ((0x000a6664f273701cULL) >> 28), ((0x00f6e78795e200b1ULL) & ((1 << 28) - 1)), ((0x00f6e78795e200b1ULL) >> 28), ((0x005d05b9ecd2473eULL) & ((1 << 28) - 1)), ((0x005d05b9ecd2473eULL) >> 28), ((0x0014f5f17c865786ULL) & ((1 << 28) - 1)), ((0x0014f5f17c865786ULL) >> 28), ((0x00c7fd2d166fa995ULL) & ((1 << 28) - 1)), ((0x00c7fd2d166fa995ULL) >> 28), ((0x004939a2d8eb80e0ULL) & ((1 << 28) - 1)), ((0x004939a2d8eb80e0ULL) >> 28), ((0x002244ba0942c199ULL) & ((1 << 28) - 1)), ((0x002244ba0942c199ULL) >> 28)}}


                                                                    },
    }}, {{
        {{{((0x00321e767f0262cfULL) & ((1 << 28) - 1)), ((0x00321e767f0262cfULL) >> 28), ((0x002e57d776caf68eULL) & ((1 << 28) - 1)), ((0x002e57d776caf68eULL) >> 28), ((0x00bf2c94814f0437ULL) & ((1 << 28) - 1)), ((0x00bf2c94814f0437ULL) >> 28), ((0x00c339196acd622fULL) & ((1 << 28) - 1)), ((0x00c339196acd622fULL) >> 28), ((0x001db4cce71e2770ULL) & ((1 << 28) - 1)), ((0x001db4cce71e2770ULL) >> 28), ((0x001ded5ddba6eee2ULL) & ((1 << 28) - 1)), ((0x001ded5ddba6eee2ULL) >> 28), ((0x0078608ab1554c8dULL) & ((1 << 28) - 1)), ((0x0078608ab1554c8dULL) >> 28), ((0x00067fe0ab76365bULL) & ((1 << 28) - 1)), ((0x00067fe0ab76365bULL) >> 28)}}


                                                                    },
        {{{((0x00f09758e11e3985ULL) & ((1 << 28) - 1)), ((0x00f09758e11e3985ULL) >> 28), ((0x00169efdbd64fad3ULL) & ((1 << 28) - 1)), ((0x00169efdbd64fad3ULL) >> 28), ((0x00e8889b7d6dacd6ULL) & ((1 << 28) - 1)), ((0x00e8889b7d6dacd6ULL) >> 28), ((0x0035cdd58ea88209ULL) & ((1 << 28) - 1)), ((0x0035cdd58ea88209ULL) >> 28), ((0x00bcda47586d7f49ULL) & ((1 << 28) - 1)), ((0x00bcda47586d7f49ULL) >> 28), ((0x003cdddcb2879088ULL) & ((1 << 28) - 1)), ((0x003cdddcb2879088ULL) >> 28), ((0x0016da70187e954bULL) & ((1 << 28) - 1)), ((0x0016da70187e954bULL) >> 28), ((0x009556ea2e92aacdULL) & ((1 << 28) - 1)), ((0x009556ea2e92aacdULL) >> 28)}}


                                                                    },
        {{{((0x008cab16bd1ff897ULL) & ((1 << 28) - 1)), ((0x008cab16bd1ff897ULL) >> 28), ((0x00b389972cdf753fULL) & ((1 << 28) - 1)), ((0x00b389972cdf753fULL) >> 28), ((0x00ea8ed1e46dfdc0ULL) & ((1 << 28) - 1)), ((0x00ea8ed1e46dfdc0ULL) >> 28), ((0x004fe7ef94c589f4ULL) & ((1 << 28) - 1)), ((0x004fe7ef94c589f4ULL) >> 28), ((0x002b8ae9b805ecf3ULL) & ((1 << 28) - 1)), ((0x002b8ae9b805ecf3ULL) >> 28), ((0x0025c08d892874a5ULL) & ((1 << 28) - 1)), ((0x0025c08d892874a5ULL) >> 28), ((0x0023938e98d44c4cULL) & ((1 << 28) - 1)), ((0x0023938e98d44c4cULL) >> 28), ((0x00f759134cabf69cULL) & ((1 << 28) - 1)), ((0x00f759134cabf69cULL) >> 28)}}


                                                                    },
    }}, {{
        {{{((0x006c2a84678e4b3bULL) & ((1 << 28) - 1)), ((0x006c2a84678e4b3bULL) >> 28), ((0x007a194aacd1868fULL) & ((1 << 28) - 1)), ((0x007a194aacd1868fULL) >> 28), ((0x00ed0225af424761ULL) & ((1 << 28) - 1)), ((0x00ed0225af424761ULL) >> 28), ((0x00da0a6f293c64b8ULL) & ((1 << 28) - 1)), ((0x00da0a6f293c64b8ULL) >> 28), ((0x001062ac5c6a7a18ULL) & ((1 << 28) - 1)), ((0x001062ac5c6a7a18ULL) >> 28), ((0x0030f5775a8aeef4ULL) & ((1 << 28) - 1)), ((0x0030f5775a8aeef4ULL) >> 28), ((0x0002acaad76b7af0ULL) & ((1 << 28) - 1)), ((0x0002acaad76b7af0ULL) >> 28), ((0x00410b8fd63a579fULL) & ((1 << 28) - 1)), ((0x00410b8fd63a579fULL) >> 28)}}


                                                                    },
        {{{((0x001ec59db3d9590eULL) & ((1 << 28) - 1)), ((0x001ec59db3d9590eULL) >> 28), ((0x001e9e3f1c3f182dULL) & ((1 << 28) - 1)), ((0x001e9e3f1c3f182dULL) >> 28), ((0x0045a9c3ec2cab14ULL) & ((1 << 28) - 1)), ((0x0045a9c3ec2cab14ULL) >> 28), ((0x0008198572aeb673ULL) & ((1 << 28) - 1)), ((0x0008198572aeb673ULL) >> 28), ((0x00773b74068bd167ULL) & ((1 << 28) - 1)), ((0x00773b74068bd167ULL) >> 28), ((0x0012535eaa395434ULL) & ((1 << 28) - 1)), ((0x0012535eaa395434ULL) >> 28), ((0x0044dba9e3bbb74aULL) & ((1 << 28) - 1)), ((0x0044dba9e3bbb74aULL) >> 28), ((0x002fba4d3c74bd0eULL) & ((1 << 28) - 1)), ((0x002fba4d3c74bd0eULL) >> 28)}}


                                                                    },
        {{{((0x0042bf08fe66922cULL) & ((1 << 28) - 1)), ((0x0042bf08fe66922cULL) >> 28), ((0x003318b8fbb49e8cULL) & ((1 << 28) - 1)), ((0x003318b8fbb49e8cULL) >> 28), ((0x00d75946004aa14cULL) & ((1 << 28) - 1)), ((0x00d75946004aa14cULL) >> 28), ((0x00f601586b42bf1cULL) & ((1 << 28) - 1)), ((0x00f601586b42bf1cULL) >> 28), ((0x00c74cf1d912fe66ULL) & ((1 << 28) - 1)), ((0x00c74cf1d912fe66ULL) >> 28), ((0x00abcb36974b30adULL) & ((1 << 28) - 1)), ((0x00abcb36974b30adULL) >> 28), ((0x007eb78720c9d2b8ULL) & ((1 << 28) - 1)), ((0x007eb78720c9d2b8ULL) >> 28), ((0x009f54ab7bd4df85ULL) & ((1 << 28) - 1)), ((0x009f54ab7bd4df85ULL) >> 28)}}


                                                                    },
    }}, {{
        {{{((0x00db9fc948f73826ULL) & ((1 << 28) - 1)), ((0x00db9fc948f73826ULL) >> 28), ((0x00fa8b3746ed8ee9ULL) & ((1 << 28) - 1)), ((0x00fa8b3746ed8ee9ULL) >> 28), ((0x00132cb65aafbeb2ULL) & ((1 << 28) - 1)), ((0x00132cb65aafbeb2ULL) >> 28), ((0x00c36ff3fe7925b8ULL) & ((1 << 28) - 1)), ((0x00c36ff3fe7925b8ULL) >> 28), ((0x00837daed353d2feULL) & ((1 << 28) - 1)), ((0x00837daed353d2feULL) >> 28), ((0x00ec661be0667cf4ULL) & ((1 << 28) - 1)), ((0x00ec661be0667cf4ULL) >> 28), ((0x005beb8ed2e90204ULL) & ((1 << 28) - 1)), ((0x005beb8ed2e90204ULL) >> 28), ((0x00d77dd69e564967ULL) & ((1 << 28) - 1)), ((0x00d77dd69e564967ULL) >> 28)}}


                                                                    },
        {{{((0x0042e6268b861751ULL) & ((1 << 28) - 1)), ((0x0042e6268b861751ULL) >> 28), ((0x0008dd0469500c16ULL) & ((1 << 28) - 1)), ((0x0008dd0469500c16ULL) >> 28), ((0x00b51b57c338a3fdULL) & ((1 << 28) - 1)), ((0x00b51b57c338a3fdULL) >> 28), ((0x00cc4497d85cff6bULL) & ((1 << 28) - 1)), ((0x00cc4497d85cff6bULL) >> 28), ((0x002f13d6b57c34a4ULL) & ((1 << 28) - 1)), ((0x002f13d6b57c34a4ULL) >> 28), ((0x0083652eaf301105ULL) & ((1 << 28) - 1)), ((0x0083652eaf301105ULL) >> 28), ((0x00cc344294cc93a8ULL) & ((1 << 28) - 1)), ((0x00cc344294cc93a8ULL) >> 28), ((0x0060f4d02810e270ULL) & ((1 << 28) - 1)), ((0x0060f4d02810e270ULL) >> 28)}}


                                                                    },
        {{{((0x00a8954363cd518bULL) & ((1 << 28) - 1)), ((0x00a8954363cd518bULL) >> 28), ((0x00ad171124bccb7bULL) & ((1 << 28) - 1)), ((0x00ad171124bccb7bULL) >> 28), ((0x0065f46a4adaae00ULL) & ((1 << 28) - 1)), ((0x0065f46a4adaae00ULL) >> 28), ((0x001b1a5b2a96e500ULL) & ((1 << 28) - 1)), ((0x001b1a5b2a96e500ULL) >> 28), ((0x0043fe24f8233285ULL) & ((1 << 28) - 1)), ((0x0043fe24f8233285ULL) >> 28), ((0x0066996d8ae1f2c3ULL) & ((1 << 28) - 1)), ((0x0066996d8ae1f2c3ULL) >> 28), ((0x00c530f3264169f9ULL) & ((1 << 28) - 1)), ((0x00c530f3264169f9ULL) >> 28), ((0x00c0f92d07cf6a57ULL) & ((1 << 28) - 1)), ((0x00c0f92d07cf6a57ULL) >> 28)}}


                                                                    },
    }}, {{
        {{{((0x0036a55c6815d943ULL) & ((1 << 28) - 1)), ((0x0036a55c6815d943ULL) >> 28), ((0x008c8d1def993db3ULL) & ((1 << 28) - 1)), ((0x008c8d1def993db3ULL) >> 28), ((0x002e0e1e8ff7318fULL) & ((1 << 28) - 1)), ((0x002e0e1e8ff7318fULL) >> 28), ((0x00d883a4b92db00aULL) & ((1 << 28) - 1)), ((0x00d883a4b92db00aULL) >> 28), ((0x002f5e781ae33906ULL) & ((1 << 28) - 1)), ((0x002f5e781ae33906ULL) >> 28), ((0x001a72adb235c06dULL) & ((1 << 28) - 1)), ((0x001a72adb235c06dULL) >> 28), ((0x00f2e59e736e9caaULL) & ((1 << 28) - 1)), ((0x00f2e59e736e9caaULL) >> 28), ((0x001a4b58e3031914ULL) & ((1 << 28) - 1)), ((0x001a4b58e3031914ULL) >> 28)}}


                                                                    },
        {{{((0x00d73bfae5e00844ULL) & ((1 << 28) - 1)), ((0x00d73bfae5e00844ULL) >> 28), ((0x00bf459766fb5f52ULL) & ((1 << 28) - 1)), ((0x00bf459766fb5f52ULL) >> 28), ((0x0061b4f5a5313cdeULL) & ((1 << 28) - 1)), ((0x0061b4f5a5313cdeULL) >> 28), ((0x004392d4c3b95514ULL) & ((1 << 28) - 1)), ((0x004392d4c3b95514ULL) >> 28), ((0x000d3551b1077523ULL) & ((1 << 28) - 1)), ((0x000d3551b1077523ULL) >> 28), ((0x0000998840ee5d71ULL) & ((1 << 28) - 1)), ((0x0000998840ee5d71ULL) >> 28), ((0x006de6e340448b7bULL) & ((1 << 28) - 1)), ((0x006de6e340448b7bULL) >> 28), ((0x00251aa504875d6eULL) & ((1 << 28) - 1)), ((0x00251aa504875d6eULL) >> 28)}}


                                                                    },
        {{{((0x003bf343427ac342ULL) & ((1 << 28) - 1)), ((0x003bf343427ac342ULL) >> 28), ((0x00adc0a78642b8c5ULL) & ((1 << 28) - 1)), ((0x00adc0a78642b8c5ULL) >> 28), ((0x0003b893175a8314ULL) & ((1 << 28) - 1)), ((0x0003b893175a8314ULL) >> 28), ((0x0061a34ade5703bcULL) & ((1 << 28) - 1)), ((0x0061a34ade5703bcULL) >> 28), ((0x00ea3ea8bb71d632ULL) & ((1 << 28) - 1)), ((0x00ea3ea8bb71d632ULL) >> 28), ((0x00be0df9a1f198c2ULL) & ((1 << 28) - 1)), ((0x00be0df9a1f198c2ULL) >> 28), ((0x0046dd8e7c1635fbULL) & ((1 << 28) - 1)), ((0x0046dd8e7c1635fbULL) >> 28), ((0x00f1523fdd25d5e5ULL) & ((1 << 28) - 1)), ((0x00f1523fdd25d5e5ULL) >> 28)}}


                                                                    },
    }}, {{
        {{{((0x00633f63fc9dd406ULL) & ((1 << 28) - 1)), ((0x00633f63fc9dd406ULL) >> 28), ((0x00e713ff80e04a43ULL) & ((1 << 28) - 1)), ((0x00e713ff80e04a43ULL) >> 28), ((0x0060c6e970f2d621ULL) & ((1 << 28) - 1)), ((0x0060c6e970f2d621ULL) >> 28), ((0x00a57cd7f0df1891ULL) & ((1 << 28) - 1)), ((0x00a57cd7f0df1891ULL) >> 28), ((0x00f2406a550650bbULL) & ((1 << 28) - 1)), ((0x00f2406a550650bbULL) >> 28), ((0x00b064290efdc684ULL) & ((1 << 28) - 1)), ((0x00b064290efdc684ULL) >> 28), ((0x001eab0144d17916ULL) & ((1 << 28) - 1)), ((0x001eab0144d17916ULL) >> 28), ((0x00cd15f863c293abULL) & ((1 << 28) - 1)), ((0x00cd15f863c293abULL) >> 28)}}


                                                                    },
        {{{((0x0029cec55273f70dULL) & ((1 << 28) - 1)), ((0x0029cec55273f70dULL) >> 28), ((0x007044ee275c6340ULL) & ((1 << 28) - 1)), ((0x007044ee275c6340ULL) >> 28), ((0x0040f637a93015e2ULL) & ((1 << 28) - 1)), ((0x0040f637a93015e2ULL) >> 28), ((0x00338bb78db5aae9ULL) & ((1 << 28) - 1)), ((0x00338bb78db5aae9ULL) >> 28), ((0x001491b2a6132147ULL) & ((1 << 28) - 1)), ((0x001491b2a6132147ULL) >> 28), ((0x00a125d6cfe6bde3ULL) & ((1 << 28) - 1)), ((0x00a125d6cfe6bde3ULL) >> 28), ((0x005f7ac561ba8669ULL) & ((1 << 28) - 1)), ((0x005f7ac561ba8669ULL) >> 28), ((0x001d5eaea3fbaacfULL) & ((1 << 28) - 1)), ((0x001d5eaea3fbaacfULL) >> 28)}}


                                                                    },
        {{{((0x00054e9635e3be31ULL) & ((1 << 28) - 1)), ((0x00054e9635e3be31ULL) >> 28), ((0x000e43f31e2872beULL) & ((1 << 28) - 1)), ((0x000e43f31e2872beULL) >> 28), ((0x00d05b1c9e339841ULL) & ((1 << 28) - 1)), ((0x00d05b1c9e339841ULL) >> 28), ((0x006fac50bd81fd98ULL) & ((1 << 28) - 1)), ((0x006fac50bd81fd98ULL) >> 28), ((0x00cdc7852eaebb09ULL) & ((1 << 28) - 1)), ((0x00cdc7852eaebb09ULL) >> 28), ((0x004ff519b061991bULL) & ((1 << 28) - 1)), ((0x004ff519b061991bULL) >> 28), ((0x009099e8107d4c85ULL) & ((1 << 28) - 1)), ((0x009099e8107d4c85ULL) >> 28), ((0x00273e24c36a4a61ULL) & ((1 << 28) - 1)), ((0x00273e24c36a4a61ULL) >> 28)}}


                                                                    },
    }}, {{
        {{{((0x00070b4441ef2c46ULL) & ((1 << 28) - 1)), ((0x00070b4441ef2c46ULL) >> 28), ((0x00efa5b02801a109ULL) & ((1 << 28) - 1)), ((0x00efa5b02801a109ULL) >> 28), ((0x00bf0b8c3ee64adfULL) & ((1 << 28) - 1)), ((0x00bf0b8c3ee64adfULL) >> 28), ((0x008a67e0b3452e98ULL) & ((1 << 28) - 1)), ((0x008a67e0b3452e98ULL) >> 28), ((0x001916b1f2fa7a74ULL) & ((1 << 28) - 1)), ((0x001916b1f2fa7a74ULL) >> 28), ((0x00d781a78ff6cdc3ULL) & ((1 << 28) - 1)), ((0x00d781a78ff6cdc3ULL) >> 28), ((0x008682ce57e5c919ULL) & ((1 << 28) - 1)), ((0x008682ce57e5c919ULL) >> 28), ((0x00cc1109dd210da3ULL) & ((1 << 28) - 1)), ((0x00cc1109dd210da3ULL) >> 28)}}


                                                                    },
        {{{((0x00cae8aaff388663ULL) & ((1 << 28) - 1)), ((0x00cae8aaff388663ULL) >> 28), ((0x005e983a35dda1c7ULL) & ((1 << 28) - 1)), ((0x005e983a35dda1c7ULL) >> 28), ((0x007ab1030d8e37f4ULL) & ((1 << 28) - 1)), ((0x007ab1030d8e37f4ULL) >> 28), ((0x00e48940f5d032feULL) & ((1 << 28) - 1)), ((0x00e48940f5d032feULL) >> 28), ((0x006a36f9ef30b331ULL) & ((1 << 28) - 1)), ((0x006a36f9ef30b331ULL) >> 28), ((0x009be6f03958c757ULL) & ((1 << 28) - 1)), ((0x009be6f03958c757ULL) >> 28), ((0x0086231ceba91400ULL) & ((1 << 28) - 1)), ((0x0086231ceba91400ULL) >> 28), ((0x008bd0f7b823e7aaULL) & ((1 << 28) - 1)), ((0x008bd0f7b823e7aaULL) >> 28)}}


                                                                    },
        {{{((0x00cf881ebef5a45aULL) & ((1 << 28) - 1)), ((0x00cf881ebef5a45aULL) >> 28), ((0x004ebea78e7c6f2cULL) & ((1 << 28) - 1)), ((0x004ebea78e7c6f2cULL) >> 28), ((0x0090da9209cf26a0ULL) & ((1 << 28) - 1)), ((0x0090da9209cf26a0ULL) >> 28), ((0x00de2b2e4c775b84ULL) & ((1 << 28) - 1)), ((0x00de2b2e4c775b84ULL) >> 28), ((0x0071d6031c3c15aeULL) & ((1 << 28) - 1)), ((0x0071d6031c3c15aeULL) >> 28), ((0x00d9e927ef177d70ULL) & ((1 << 28) - 1)), ((0x00d9e927ef177d70ULL) >> 28), ((0x00894ee8c23896fdULL) & ((1 << 28) - 1)), ((0x00894ee8c23896fdULL) >> 28), ((0x00e3b3b401e41aadULL) & ((1 << 28) - 1)), ((0x00e3b3b401e41aadULL) >> 28)}}


                                                                    },
    }}, {{
        {{{((0x00204fef26864170ULL) & ((1 << 28) - 1)), ((0x00204fef26864170ULL) >> 28), ((0x00819269c5dee0f8ULL) & ((1 << 28) - 1)), ((0x00819269c5dee0f8ULL) >> 28), ((0x00bfb4713ec97966ULL) & ((1 << 28) - 1)), ((0x00bfb4713ec97966ULL) >> 28), ((0x0026339a6f34df78ULL) & ((1 << 28) - 1)), ((0x0026339a6f34df78ULL) >> 28), ((0x001f26e64c761dc2ULL) & ((1 << 28) - 1)), ((0x001f26e64c761dc2ULL) >> 28), ((0x00effe3af313cb60ULL) & ((1 << 28) - 1)), ((0x00effe3af313cb60ULL) >> 28), ((0x00e17b70138f601bULL) & ((1 << 28) - 1)), ((0x00e17b70138f601bULL) >> 28), ((0x00f16e1ccd9ede5eULL) & ((1 << 28) - 1)), ((0x00f16e1ccd9ede5eULL) >> 28)}}


                                                                    },
        {{{((0x005d9a8353fdb2dbULL) & ((1 << 28) - 1)), ((0x005d9a8353fdb2dbULL) >> 28), ((0x0055cc2048c698f0ULL) & ((1 << 28) - 1)), ((0x0055cc2048c698f0ULL) >> 28), ((0x00f6c4ac89657218ULL) & ((1 << 28) - 1)), ((0x00f6c4ac89657218ULL) >> 28), ((0x00525034d73faeb2ULL) & ((1 << 28) - 1)), ((0x00525034d73faeb2ULL) >> 28), ((0x00435776fbda3c7dULL) & ((1 << 28) - 1)), ((0x00435776fbda3c7dULL) >> 28), ((0x0070ea5312323cbcULL) & ((1 << 28) - 1)), ((0x0070ea5312323cbcULL) >> 28), ((0x007a105d44d069fbULL) & ((1 << 28) - 1)), ((0x007a105d44d069fbULL) >> 28), ((0x006dbc8d6dc786aaULL) & ((1 << 28) - 1)), ((0x006dbc8d6dc786aaULL) >> 28)}}


                                                                    },
        {{{((0x0017cff19cd394ecULL) & ((1 << 28) - 1)), ((0x0017cff19cd394ecULL) >> 28), ((0x00fef7b810922587ULL) & ((1 << 28) - 1)), ((0x00fef7b810922587ULL) >> 28), ((0x00e6483970dff548ULL) & ((1 << 28) - 1)), ((0x00e6483970dff548ULL) >> 28), ((0x00ddf36ad6874264ULL) & ((1 << 28) - 1)), ((0x00ddf36ad6874264ULL) >> 28), ((0x00e61778523fcce2ULL) & ((1 << 28) - 1)), ((0x00e61778523fcce2ULL) >> 28), ((0x0093a66c0c93b24aULL) & ((1 << 28) - 1)), ((0x0093a66c0c93b24aULL) >> 28), ((0x00fd367114db7f86ULL) & ((1 << 28) - 1)), ((0x00fd367114db7f86ULL) >> 28), ((0x007652d7ddce26ddULL) & ((1 << 28) - 1)), ((0x007652d7ddce26ddULL) >> 28)}}


                                                                    },
    }}, {{
        {{{((0x00d92ced7ba12843ULL) & ((1 << 28) - 1)), ((0x00d92ced7ba12843ULL) >> 28), ((0x00aea9c7771e86e7ULL) & ((1 << 28) - 1)), ((0x00aea9c7771e86e7ULL) >> 28), ((0x0046639693354f7bULL) & ((1 << 28) - 1)), ((0x0046639693354f7bULL) >> 28), ((0x00a628dbb6a80c47ULL) & ((1 << 28) - 1)), ((0x00a628dbb6a80c47ULL) >> 28), ((0x003a0b0507372953ULL) & ((1 << 28) - 1)), ((0x003a0b0507372953ULL) >> 28), ((0x00421113ab45c0d9ULL) & ((1 << 28) - 1)), ((0x00421113ab45c0d9ULL) >> 28), ((0x00e545f08362ab7aULL) & ((1 << 28) - 1)), ((0x00e545f08362ab7aULL) >> 28), ((0x0028ce087b4d6d96ULL) & ((1 << 28) - 1)), ((0x0028ce087b4d6d96ULL) >> 28)}}


                                                                    },
        {{{((0x00a67ee7cf9f99ebULL) & ((1 << 28) - 1)), ((0x00a67ee7cf9f99ebULL) >> 28), ((0x005713b275f2ff68ULL) & ((1 << 28) - 1)), ((0x005713b275f2ff68ULL) >> 28), ((0x00f1d536a841513dULL) & ((1 << 28) - 1)), ((0x00f1d536a841513dULL) >> 28), ((0x00823b59b024712eULL) & ((1 << 28) - 1)), ((0x00823b59b024712eULL) >> 28), ((0x009c46b9d0d38cecULL) & ((1 << 28) - 1)), ((0x009c46b9d0d38cecULL) >> 28), ((0x00cdb1595aa2d7d4ULL) & ((1 << 28) - 1)), ((0x00cdb1595aa2d7d4ULL) >> 28), ((0x008375b3423d9af8ULL) & ((1 << 28) - 1)), ((0x008375b3423d9af8ULL) >> 28), ((0x000ab0b516d978f7ULL) & ((1 << 28) - 1)), ((0x000ab0b516d978f7ULL) >> 28)}}


                                                                    },
        {{{((0x00428dcb3c510b0fULL) & ((1 << 28) - 1)), ((0x00428dcb3c510b0fULL) >> 28), ((0x00585607ea24bb4eULL) & ((1 << 28) - 1)), ((0x00585607ea24bb4eULL) >> 28), ((0x003736bf1603687aULL) & ((1 << 28) - 1)), ((0x003736bf1603687aULL) >> 28), ((0x00c47e568c4fe3c7ULL) & ((1 << 28) - 1)), ((0x00c47e568c4fe3c7ULL) >> 28), ((0x003cd00282848605ULL) & ((1 << 28) - 1)), ((0x003cd00282848605ULL) >> 28), ((0x0043a487c3b91939ULL) & ((1 << 28) - 1)), ((0x0043a487c3b91939ULL) >> 28), ((0x004ffc04e1095a06ULL) & ((1 << 28) - 1)), ((0x004ffc04e1095a06ULL) >> 28), ((0x00a4c989a3d4b918ULL) & ((1 << 28) - 1)), ((0x00a4c989a3d4b918ULL) >> 28)}}


                                                                    },
    }}, {{
        {{{((0x00a8778d0e429f7aULL) & ((1 << 28) - 1)), ((0x00a8778d0e429f7aULL) >> 28), ((0x004c02b059105a68ULL) & ((1 << 28) - 1)), ((0x004c02b059105a68ULL) >> 28), ((0x0016653b609da3ffULL) & ((1 << 28) - 1)), ((0x0016653b609da3ffULL) >> 28), ((0x00d5107bd1a12d27ULL) & ((1 << 28) - 1)), ((0x00d5107bd1a12d27ULL) >> 28), ((0x00b4708f9a771cabULL) & ((1 << 28) - 1)), ((0x00b4708f9a771cabULL) >> 28), ((0x00bb63b662033f69ULL) & ((1 << 28) - 1)), ((0x00bb63b662033f69ULL) >> 28), ((0x0072f322240e7215ULL) & ((1 << 28) - 1)), ((0x0072f322240e7215ULL) >> 28), ((0x0019445b59c69222ULL) & ((1 << 28) - 1)), ((0x0019445b59c69222ULL) >> 28)}}


                                                                    },
        {{{((0x00cf4f6069a658e6ULL) & ((1 << 28) - 1)), ((0x00cf4f6069a658e6ULL) >> 28), ((0x0053ca52859436a6ULL) & ((1 << 28) - 1)), ((0x0053ca52859436a6ULL) >> 28), ((0x0064b994d7e3e117ULL) & ((1 << 28) - 1)), ((0x0064b994d7e3e117ULL) >> 28), ((0x00cb469b9a07f534ULL) & ((1 << 28) - 1)), ((0x00cb469b9a07f534ULL) >> 28), ((0x00cfb68f399e9d47ULL) & ((1 << 28) - 1)), ((0x00cfb68f399e9d47ULL) >> 28), ((0x00f0dcb8dac1c6e7ULL) & ((1 << 28) - 1)), ((0x00f0dcb8dac1c6e7ULL) >> 28), ((0x00f2ab67f538b3a5ULL) & ((1 << 28) - 1)), ((0x00f2ab67f538b3a5ULL) >> 28), ((0x0055544f178ab975ULL) & ((1 << 28) - 1)), ((0x0055544f178ab975ULL) >> 28)}}


                                                                    },
        {{{((0x0099b7a2685d538cULL) & ((1 << 28) - 1)), ((0x0099b7a2685d538cULL) >> 28), ((0x00e2f1897b7c0018ULL) & ((1 << 28) - 1)), ((0x00e2f1897b7c0018ULL) >> 28), ((0x003adac8ce48dae3ULL) & ((1 << 28) - 1)), ((0x003adac8ce48dae3ULL) >> 28), ((0x00089276d5c50c0cULL) & ((1 << 28) - 1)), ((0x00089276d5c50c0cULL) >> 28), ((0x00172fca07ad6717ULL) & ((1 << 28) - 1)), ((0x00172fca07ad6717ULL) >> 28), ((0x00cb1a72f54069e5ULL) & ((1 << 28) - 1)), ((0x00cb1a72f54069e5ULL) >> 28), ((0x004ee42f133545b3ULL) & ((1 << 28) - 1)), ((0x004ee42f133545b3ULL) >> 28), ((0x00785f8651362f16ULL) & ((1 << 28) - 1)), ((0x00785f8651362f16ULL) >> 28)}}


                                                                    },
    }}, {{
        {{{((0x0049cbac38509e11ULL) & ((1 << 28) - 1)), ((0x0049cbac38509e11ULL) >> 28), ((0x0015234505d42cdfULL) & ((1 << 28) - 1)), ((0x0015234505d42cdfULL) >> 28), ((0x00794fb0b5840f1cULL) & ((1 << 28) - 1)), ((0x00794fb0b5840f1cULL) >> 28), ((0x00496437344045a5ULL) & ((1 << 28) - 1)), ((0x00496437344045a5ULL) >> 28), ((0x0031b6d944e4f9b0ULL) & ((1 << 28) - 1)), ((0x0031b6d944e4f9b0ULL) >> 28), ((0x00b207318ac1f5d8ULL) & ((1 << 28) - 1)), ((0x00b207318ac1f5d8ULL) >> 28), ((0x0000c840da7f5c5dULL) & ((1 << 28) - 1)), ((0x0000c840da7f5c5dULL) >> 28), ((0x00526f373a5c8814ULL) & ((1 << 28) - 1)), ((0x00526f373a5c8814ULL) >> 28)}}


                                                                    },
        {{{((0x002c7b7742d1dfd9ULL) & ((1 << 28) - 1)), ((0x002c7b7742d1dfd9ULL) >> 28), ((0x002cabeb18623c01ULL) & ((1 << 28) - 1)), ((0x002cabeb18623c01ULL) >> 28), ((0x00055f5e3e044446ULL) & ((1 << 28) - 1)), ((0x00055f5e3e044446ULL) >> 28), ((0x006c20f3b4ef54baULL) & ((1 << 28) - 1)), ((0x006c20f3b4ef54baULL) >> 28), ((0x00c600141ec6b35fULL) & ((1 << 28) - 1)), ((0x00c600141ec6b35fULL) >> 28), ((0x00354f437f1a32a3ULL) & ((1 << 28) - 1)), ((0x00354f437f1a32a3ULL) >> 28), ((0x00bac4624a3520f9ULL) & ((1 << 28) - 1)), ((0x00bac4624a3520f9ULL) >> 28), ((0x00c483f734a90691ULL) & ((1 << 28) - 1)), ((0x00c483f734a90691ULL) >> 28)}}


                                                                    },
        {{{((0x0053a737d422918dULL) & ((1 << 28) - 1)), ((0x0053a737d422918dULL) >> 28), ((0x00f7fca1d8758625ULL) & ((1 << 28) - 1)), ((0x00f7fca1d8758625ULL) >> 28), ((0x00c360336dadb04cULL) & ((1 << 28) - 1)), ((0x00c360336dadb04cULL) >> 28), ((0x00f38e3d9158a1b8ULL) & ((1 << 28) - 1)), ((0x00f38e3d9158a1b8ULL) >> 28), ((0x0069ce3b418e84c6ULL) & ((1 << 28) - 1)), ((0x0069ce3b418e84c6ULL) >> 28), ((0x005d1697eca16eadULL) & ((1 << 28) - 1)), ((0x005d1697eca16eadULL) >> 28), ((0x00f8bd6a35ece13dULL) & ((1 << 28) - 1)), ((0x00f8bd6a35ece13dULL) >> 28), ((0x007885dfc2b5afeaULL) & ((1 << 28) - 1)), ((0x007885dfc2b5afeaULL) >> 28)}}


                                                                    },
    }}, {{
        {{{((0x00c3617ae260776cULL) & ((1 << 28) - 1)), ((0x00c3617ae260776cULL) >> 28), ((0x00b20dc3e96922d7ULL) & ((1 << 28) - 1)), ((0x00b20dc3e96922d7ULL) >> 28), ((0x00a1a7802246706aULL) & ((1 << 28) - 1)), ((0x00a1a7802246706aULL) >> 28), ((0x00ca6505a5240244ULL) & ((1 << 28) - 1)), ((0x00ca6505a5240244ULL) >> 28), ((0x002246b62d919782ULL) & ((1 << 28) - 1)), ((0x002246b62d919782ULL) >> 28), ((0x001439102d7aa9b3ULL) & ((1 << 28) - 1)), ((0x001439102d7aa9b3ULL) >> 28), ((0x00e8af1139e6422cULL) & ((1 << 28) - 1)), ((0x00e8af1139e6422cULL) >> 28), ((0x00c888d1b52f2b05ULL) & ((1 << 28) - 1)), ((0x00c888d1b52f2b05ULL) >> 28)}}


                                                                    },
        {{{((0x005b67690ffd41d9ULL) & ((1 << 28) - 1)), ((0x005b67690ffd41d9ULL) >> 28), ((0x005294f28df516f9ULL) & ((1 << 28) - 1)), ((0x005294f28df516f9ULL) >> 28), ((0x00a879272412fcb9ULL) & ((1 << 28) - 1)), ((0x00a879272412fcb9ULL) >> 28), ((0x00098b629a6d1c8dULL) & ((1 << 28) - 1)), ((0x00098b629a6d1c8dULL) >> 28), ((0x00fabd3c8050865aULL) & ((1 << 28) - 1)), ((0x00fabd3c8050865aULL) >> 28), ((0x00cd7e5b0a3879c5ULL) & ((1 << 28) - 1)), ((0x00cd7e5b0a3879c5ULL) >> 28), ((0x00153238210f3423ULL) & ((1 << 28) - 1)), ((0x00153238210f3423ULL) >> 28), ((0x00357cac101e9f42ULL) & ((1 << 28) - 1)), ((0x00357cac101e9f42ULL) >> 28)}}


                                                                    },
        {{{((0x008917b454444fb7ULL) & ((1 << 28) - 1)), ((0x008917b454444fb7ULL) >> 28), ((0x00f59247c97e441bULL) & ((1 << 28) - 1)), ((0x00f59247c97e441bULL) >> 28), ((0x00a6200a6815152dULL) & ((1 << 28) - 1)), ((0x00a6200a6815152dULL) >> 28), ((0x0009a4228601d254ULL) & ((1 << 28) - 1)), ((0x0009a4228601d254ULL) >> 28), ((0x001c0360559bd374ULL) & ((1 << 28) - 1)), ((0x001c0360559bd374ULL) >> 28), ((0x007563362039cb36ULL) & ((1 << 28) - 1)), ((0x007563362039cb36ULL) >> 28), ((0x00bd75b48d74e32bULL) & ((1 << 28) - 1)), ((0x00bd75b48d74e32bULL) >> 28), ((0x0017f515ac3499e8ULL) & ((1 << 28) - 1)), ((0x0017f515ac3499e8ULL) >> 28)}}


                                                                    },
    }}, {{
        {{{((0x001532a7ffe41c5aULL) & ((1 << 28) - 1)), ((0x001532a7ffe41c5aULL) >> 28), ((0x00eb1edce358d6bfULL) & ((1 << 28) - 1)), ((0x00eb1edce358d6bfULL) >> 28), ((0x00ddbacc7b678a7bULL) & ((1 << 28) - 1)), ((0x00ddbacc7b678a7bULL) >> 28), ((0x008a7b70f3c841a3ULL) & ((1 << 28) - 1)), ((0x008a7b70f3c841a3ULL) >> 28), ((0x00f1923bf27d3f4cULL) & ((1 << 28) - 1)), ((0x00f1923bf27d3f4cULL) >> 28), ((0x000b2713ed8f7873ULL) & ((1 << 28) - 1)), ((0x000b2713ed8f7873ULL) >> 28), ((0x00aaf67e29047902ULL) & ((1 << 28) - 1)), ((0x00aaf67e29047902ULL) >> 28), ((0x0044994a70b3976dULL) & ((1 << 28) - 1)), ((0x0044994a70b3976dULL) >> 28)}}


                                                                    },
        {{{((0x00d54e802082d42cULL) & ((1 << 28) - 1)), ((0x00d54e802082d42cULL) >> 28), ((0x00a55aa0dce7cc6cULL) & ((1 << 28) - 1)), ((0x00a55aa0dce7cc6cULL) >> 28), ((0x006477b96073f146ULL) & ((1 << 28) - 1)), ((0x006477b96073f146ULL) >> 28), ((0x0082efe4ceb43594ULL) & ((1 << 28) - 1)), ((0x0082efe4ceb43594ULL) >> 28), ((0x00a922bcba026845ULL) & ((1 << 28) - 1)), ((0x00a922bcba026845ULL) >> 28), ((0x0077f19d1ab75182ULL) & ((1 << 28) - 1)), ((0x0077f19d1ab75182ULL) >> 28), ((0x00c2bb2737846e59ULL) & ((1 << 28) - 1)), ((0x00c2bb2737846e59ULL) >> 28), ((0x0004d7eec791dd33ULL) & ((1 << 28) - 1)), ((0x0004d7eec791dd33ULL) >> 28)}}


                                                                    },
        {{{((0x0044588d1a81d680ULL) & ((1 << 28) - 1)), ((0x0044588d1a81d680ULL) >> 28), ((0x00b0a9097208e4f8ULL) & ((1 << 28) - 1)), ((0x00b0a9097208e4f8ULL) >> 28), ((0x00212605350dc57eULL) & ((1 << 28) - 1)), ((0x00212605350dc57eULL) >> 28), ((0x0028717cd2871123ULL) & ((1 << 28) - 1)), ((0x0028717cd2871123ULL) >> 28), ((0x00fb083c100fd979ULL) & ((1 << 28) - 1)), ((0x00fb083c100fd979ULL) >> 28), ((0x0045a056ce063fdfULL) & ((1 << 28) - 1)), ((0x0045a056ce063fdfULL) >> 28), ((0x00a5d604b4dd6a41ULL) & ((1 << 28) - 1)), ((0x00a5d604b4dd6a41ULL) >> 28), ((0x001dabc08ba4e236ULL) & ((1 << 28) - 1)), ((0x001dabc08ba4e236ULL) >> 28)}}


                                                                    },
    }}, {{
        {{{((0x00c4887198d7a7faULL) & ((1 << 28) - 1)), ((0x00c4887198d7a7faULL) >> 28), ((0x00244f98fb45784aULL) & ((1 << 28) - 1)), ((0x00244f98fb45784aULL) >> 28), ((0x0045911e15a15d01ULL) & ((1 << 28) - 1)), ((0x0045911e15a15d01ULL) >> 28), ((0x001d323d374c0966ULL) & ((1 << 28) - 1)), ((0x001d323d374c0966ULL) >> 28), ((0x00967c3915196562ULL) & ((1 << 28) - 1)), ((0x00967c3915196562ULL) >> 28), ((0x0039373abd2f3c67ULL) & ((1 << 28) - 1)), ((0x0039373abd2f3c67ULL) >> 28), ((0x000d2c5614312423ULL) & ((1 << 28) - 1)), ((0x000d2c5614312423ULL) >> 28), ((0x0041cf2215442ce3ULL) & ((1 << 28) - 1)), ((0x0041cf2215442ce3ULL) >> 28)}}


                                                                    },
        {{{((0x008ede889ada7f06ULL) & ((1 << 28) - 1)), ((0x008ede889ada7f06ULL) >> 28), ((0x001611e91de2e135ULL) & ((1 << 28) - 1)), ((0x001611e91de2e135ULL) >> 28), ((0x00fdb9a458a471b9ULL) & ((1 << 28) - 1)), ((0x00fdb9a458a471b9ULL) >> 28), ((0x00563484e03710d1ULL) & ((1 << 28) - 1)), ((0x00563484e03710d1ULL) >> 28), ((0x0031cc81925e3070ULL) & ((1 << 28) - 1)), ((0x0031cc81925e3070ULL) >> 28), ((0x0062c97b3af80005ULL) & ((1 << 28) - 1)), ((0x0062c97b3af80005ULL) >> 28), ((0x00fa733eea28edebULL) & ((1 << 28) - 1)), ((0x00fa733eea28edebULL) >> 28), ((0x00e82457e1ebbc88ULL) & ((1 << 28) - 1)), ((0x00e82457e1ebbc88ULL) >> 28)}}


                                                                    },
        {{{((0x006a0df5fe9b6f59ULL) & ((1 << 28) - 1)), ((0x006a0df5fe9b6f59ULL) >> 28), ((0x00a0d4ff46040d92ULL) & ((1 << 28) - 1)), ((0x00a0d4ff46040d92ULL) >> 28), ((0x004a7cedb6f93250ULL) & ((1 << 28) - 1)), ((0x004a7cedb6f93250ULL) >> 28), ((0x00d1df8855b8c357ULL) & ((1 << 28) - 1)), ((0x00d1df8855b8c357ULL) >> 28), ((0x00e73a46086fd058ULL) & ((1 << 28) - 1)), ((0x00e73a46086fd058ULL) >> 28), ((0x0048fb0add6dfe59ULL) & ((1 << 28) - 1)), ((0x0048fb0add6dfe59ULL) >> 28), ((0x001e03a28f1b4e3dULL) & ((1 << 28) - 1)), ((0x001e03a28f1b4e3dULL) >> 28), ((0x00a871c993308d76ULL) & ((1 << 28) - 1)), ((0x00a871c993308d76ULL) >> 28)}}


                                                                    },
    }}, {{
        {{{((0x0030dbb2d1766ec8ULL) & ((1 << 28) - 1)), ((0x0030dbb2d1766ec8ULL) >> 28), ((0x00586c0ad138555eULL) & ((1 << 28) - 1)), ((0x00586c0ad138555eULL) >> 28), ((0x00d1a34f9e91c77cULL) & ((1 << 28) - 1)), ((0x00d1a34f9e91c77cULL) >> 28), ((0x0063408ad0e89014ULL) & ((1 << 28) - 1)), ((0x0063408ad0e89014ULL) >> 28), ((0x00d61231b05f6f5bULL) & ((1 << 28) - 1)), ((0x00d61231b05f6f5bULL) >> 28), ((0x0009abf569f5fd8aULL) & ((1 << 28) - 1)), ((0x0009abf569f5fd8aULL) >> 28), ((0x00aec67a110f1c43ULL) & ((1 << 28) - 1)), ((0x00aec67a110f1c43ULL) >> 28), ((0x0031d1a790938dd7ULL) & ((1 << 28) - 1)), ((0x0031d1a790938dd7ULL) >> 28)}}


                                                                    },
        {{{((0x006cded841e2a862ULL) & ((1 << 28) - 1)), ((0x006cded841e2a862ULL) >> 28), ((0x00198d60af0ab6fbULL) & ((1 << 28) - 1)), ((0x00198d60af0ab6fbULL) >> 28), ((0x0018f09db809e750ULL) & ((1 << 28) - 1)), ((0x0018f09db809e750ULL) >> 28), ((0x004e6ac676016263ULL) & ((1 << 28) - 1)), ((0x004e6ac676016263ULL) >> 28), ((0x00eafcd1620969cbULL) & ((1 << 28) - 1)), ((0x00eafcd1620969cbULL) >> 28), ((0x002c9784ca34917dULL) & ((1 << 28) - 1)), ((0x002c9784ca34917dULL) >> 28), ((0x0054f00079796de7ULL) & ((1 << 28) - 1)), ((0x0054f00079796de7ULL) >> 28), ((0x00d9fab5c5972204ULL) & ((1 << 28) - 1)), ((0x00d9fab5c5972204ULL) >> 28)}}


                                                                    },
        {{{((0x004bd0fee2438a83ULL) & ((1 << 28) - 1)), ((0x004bd0fee2438a83ULL) >> 28), ((0x00b571e62b0f83bdULL) & ((1 << 28) - 1)), ((0x00b571e62b0f83bdULL) >> 28), ((0x0059287d7ce74800ULL) & ((1 << 28) - 1)), ((0x0059287d7ce74800ULL) >> 28), ((0x00fb3631b645c3f0ULL) & ((1 << 28) - 1)), ((0x00fb3631b645c3f0ULL) >> 28), ((0x00a018e977f78494ULL) & ((1 << 28) - 1)), ((0x00a018e977f78494ULL) >> 28), ((0x0091e27065c27b12ULL) & ((1 << 28) - 1)), ((0x0091e27065c27b12ULL) >> 28), ((0x007696c1817165e0ULL) & ((1 << 28) - 1)), ((0x007696c1817165e0ULL) >> 28), ((0x008c40be7c45ba3aULL) & ((1 << 28) - 1)), ((0x008c40be7c45ba3aULL) >> 28)}}


                                                                    },
    }}, {{
        {{{((0x00a0f326327cb684ULL) & ((1 << 28) - 1)), ((0x00a0f326327cb684ULL) >> 28), ((0x001c7d0f672680ffULL) & ((1 << 28) - 1)), ((0x001c7d0f672680ffULL) >> 28), ((0x008c1c81ffb112d1ULL) & ((1 << 28) - 1)), ((0x008c1c81ffb112d1ULL) >> 28), ((0x00f8f801674eddc8ULL) & ((1 << 28) - 1)), ((0x00f8f801674eddc8ULL) >> 28), ((0x00e926d5d48c2a9dULL) & ((1 << 28) - 1)), ((0x00e926d5d48c2a9dULL) >> 28), ((0x005bd6d954c6fe9aULL) & ((1 << 28) - 1)), ((0x005bd6d954c6fe9aULL) >> 28), ((0x004c6b24b4e33703ULL) & ((1 << 28) - 1)), ((0x004c6b24b4e33703ULL) >> 28), ((0x00d05eb5c09105ccULL) & ((1 << 28) - 1)), ((0x00d05eb5c09105ccULL) >> 28)}}


                                                                    },
        {{{((0x00d61731caacf2cfULL) & ((1 << 28) - 1)), ((0x00d61731caacf2cfULL) >> 28), ((0x002df0c7609e01c5ULL) & ((1 << 28) - 1)), ((0x002df0c7609e01c5ULL) >> 28), ((0x00306172208b1e2bULL) & ((1 << 28) - 1)), ((0x00306172208b1e2bULL) >> 28), ((0x00b413fe4fb2b686ULL) & ((1 << 28) - 1)), ((0x00b413fe4fb2b686ULL) >> 28), ((0x00826d360902a221ULL) & ((1 << 28) - 1)), ((0x00826d360902a221ULL) >> 28), ((0x003f8d056e67e7f7ULL) & ((1 << 28) - 1)), ((0x003f8d056e67e7f7ULL) >> 28), ((0x0065025b0175e989ULL) & ((1 << 28) - 1)), ((0x0065025b0175e989ULL) >> 28), ((0x00369add117865ebULL) & ((1 << 28) - 1)), ((0x00369add117865ebULL) >> 28)}}


                                                                    },
        {{{((0x00aaf895aec2fa11ULL) & ((1 << 28) - 1)), ((0x00aaf895aec2fa11ULL) >> 28), ((0x000f892bc313eb52ULL) & ((1 << 28) - 1)), ((0x000f892bc313eb52ULL) >> 28), ((0x005b1c794dad050bULL) & ((1 << 28) - 1)), ((0x005b1c794dad050bULL) >> 28), ((0x003f8ec4864cec14ULL) & ((1 << 28) - 1)), ((0x003f8ec4864cec14ULL) >> 28), ((0x00af81058d0b90e5ULL) & ((1 << 28) - 1)), ((0x00af81058d0b90e5ULL) >> 28), ((0x00ebe43e183997bbULL) & ((1 << 28) - 1)), ((0x00ebe43e183997bbULL) >> 28), ((0x00a9d610f9f3e615ULL) & ((1 << 28) - 1)), ((0x00a9d610f9f3e615ULL) >> 28), ((0x007acd8eec2e88d3ULL) & ((1 << 28) - 1)), ((0x007acd8eec2e88d3ULL) >> 28)}}


                                                                    },
    }}, {{
        {{{((0x0049b2fab13812a3ULL) & ((1 << 28) - 1)), ((0x0049b2fab13812a3ULL) >> 28), ((0x00846db32cd60431ULL) & ((1 << 28) - 1)), ((0x00846db32cd60431ULL) >> 28), ((0x000177fa578c8d6cULL) & ((1 << 28) - 1)), ((0x000177fa578c8d6cULL) >> 28), ((0x00047d0e2ad4bc51ULL) & ((1 << 28) - 1)), ((0x00047d0e2ad4bc51ULL) >> 28), ((0x00b158ba38d1e588ULL) & ((1 << 28) - 1)), ((0x00b158ba38d1e588ULL) >> 28), ((0x006a45daad79e3f3ULL) & ((1 << 28) - 1)), ((0x006a45daad79e3f3ULL) >> 28), ((0x000997b93cab887bULL) & ((1 << 28) - 1)), ((0x000997b93cab887bULL) >> 28), ((0x00c47ea42fa23dc3ULL) & ((1 << 28) - 1)), ((0x00c47ea42fa23dc3ULL) >> 28)}}


                                                                    },
        {{{((0x0012b6fef7aeb1caULL) & ((1 << 28) - 1)), ((0x0012b6fef7aeb1caULL) >> 28), ((0x009412768194b6a7ULL) & ((1 << 28) - 1)), ((0x009412768194b6a7ULL) >> 28), ((0x00ff0d351f23ab93ULL) & ((1 << 28) - 1)), ((0x00ff0d351f23ab93ULL) >> 28), ((0x007e8a14c1aff71bULL) & ((1 << 28) - 1)), ((0x007e8a14c1aff71bULL) >> 28), ((0x006c1c0170c512bcULL) & ((1 << 28) - 1)), ((0x006c1c0170c512bcULL) >> 28), ((0x0016243ea02ab2e5ULL) & ((1 << 28) - 1)), ((0x0016243ea02ab2e5ULL) >> 28), ((0x007bb6865b303f3eULL) & ((1 << 28) - 1)), ((0x007bb6865b303f3eULL) >> 28), ((0x0015ce6b29b159f4ULL) & ((1 << 28) - 1)), ((0x0015ce6b29b159f4ULL) >> 28)}}


                                                                    },
        {{{((0x009961cd02e68108ULL) & ((1 << 28) - 1)), ((0x009961cd02e68108ULL) >> 28), ((0x00e2035d3a1d0836ULL) & ((1 << 28) - 1)), ((0x00e2035d3a1d0836ULL) >> 28), ((0x005d51f69b5e1a1dULL) & ((1 << 28) - 1)), ((0x005d51f69b5e1a1dULL) >> 28), ((0x004bccb4ea36edcdULL) & ((1 << 28) - 1)), ((0x004bccb4ea36edcdULL) >> 28), ((0x0069be6a7aeef268ULL) & ((1 << 28) - 1)), ((0x0069be6a7aeef268ULL) >> 28), ((0x0063f4dd9de8d5a7ULL) & ((1 << 28) - 1)), ((0x0063f4dd9de8d5a7ULL) >> 28), ((0x006283783092ca35ULL) & ((1 << 28) - 1)), ((0x006283783092ca35ULL) >> 28), ((0x0075a31af2c35409ULL) & ((1 << 28) - 1)), ((0x0075a31af2c35409ULL) >> 28)}}


                                                                    },
    }}, {{
        {{{((0x00c412365162e8cfULL) & ((1 << 28) - 1)), ((0x00c412365162e8cfULL) >> 28), ((0x00012283fb34388aULL) & ((1 << 28) - 1)), ((0x00012283fb34388aULL) >> 28), ((0x003e6543babf39e2ULL) & ((1 << 28) - 1)), ((0x003e6543babf39e2ULL) >> 28), ((0x00eead6b3a804978ULL) & ((1 << 28) - 1)), ((0x00eead6b3a804978ULL) >> 28), ((0x0099c0314e8b326fULL) & ((1 << 28) - 1)), ((0x0099c0314e8b326fULL) >> 28), ((0x00e98e0a8d477a4fULL) & ((1 << 28) - 1)), ((0x00e98e0a8d477a4fULL) >> 28), ((0x00d2eb96b127a687ULL) & ((1 << 28) - 1)), ((0x00d2eb96b127a687ULL) >> 28), ((0x00ed8d7df87571bbULL) & ((1 << 28) - 1)), ((0x00ed8d7df87571bbULL) >> 28)}}


                                                                    },
        {{{((0x00777463e308cacfULL) & ((1 << 28) - 1)), ((0x00777463e308cacfULL) >> 28), ((0x00c8acb93950132dULL) & ((1 << 28) - 1)), ((0x00c8acb93950132dULL) >> 28), ((0x00ebddbf4ca48b2cULL) & ((1 << 28) - 1)), ((0x00ebddbf4ca48b2cULL) >> 28), ((0x0026ad7ca0795a0aULL) & ((1 << 28) - 1)), ((0x0026ad7ca0795a0aULL) >> 28), ((0x00f99a3d9a715064ULL) & ((1 << 28) - 1)), ((0x00f99a3d9a715064ULL) >> 28), ((0x000d60bcf9d4dfccULL) & ((1 << 28) - 1)), ((0x000d60bcf9d4dfccULL) >> 28), ((0x005e65a73a437a06ULL) & ((1 << 28) - 1)), ((0x005e65a73a437a06ULL) >> 28), ((0x0019d536a8db56c8ULL) & ((1 << 28) - 1)), ((0x0019d536a8db56c8ULL) >> 28)}}


                                                                    },
        {{{((0x00192d7dd558d135ULL) & ((1 << 28) - 1)), ((0x00192d7dd558d135ULL) >> 28), ((0x0027cd6a8323ffa7ULL) & ((1 << 28) - 1)), ((0x0027cd6a8323ffa7ULL) >> 28), ((0x00239f1a412dc1e7ULL) & ((1 << 28) - 1)), ((0x00239f1a412dc1e7ULL) >> 28), ((0x0046b4b3be74fc5cULL) & ((1 << 28) - 1)), ((0x0046b4b3be74fc5cULL) >> 28), ((0x0020c47a2bef5bceULL) & ((1 << 28) - 1)), ((0x0020c47a2bef5bceULL) >> 28), ((0x00aa17e48f43862bULL) & ((1 << 28) - 1)), ((0x00aa17e48f43862bULL) >> 28), ((0x00f7e26c96342e5fULL) & ((1 << 28) - 1)), ((0x00f7e26c96342e5fULL) >> 28), ((0x0008011c530f39a9ULL) & ((1 << 28) - 1)), ((0x0008011c530f39a9ULL) >> 28)}}


                                                                    },
    }}, {{
        {{{((0x00aad4ac569bf0f1ULL) & ((1 << 28) - 1)), ((0x00aad4ac569bf0f1ULL) >> 28), ((0x00a67adc90b27740ULL) & ((1 << 28) - 1)), ((0x00a67adc90b27740ULL) >> 28), ((0x0048551369a5751aULL) & ((1 << 28) - 1)), ((0x0048551369a5751aULL) >> 28), ((0x0031252584a3306aULL) & ((1 << 28) - 1)), ((0x0031252584a3306aULL) >> 28), ((0x0084e15df770e6fcULL) & ((1 << 28) - 1)), ((0x0084e15df770e6fcULL) >> 28), ((0x00d7bba1c74b5805ULL) & ((1 << 28) - 1)), ((0x00d7bba1c74b5805ULL) >> 28), ((0x00a80ef223af1012ULL) & ((1 << 28) - 1)), ((0x00a80ef223af1012ULL) >> 28), ((0x0089c85ceb843a34ULL) & ((1 << 28) - 1)), ((0x0089c85ceb843a34ULL) >> 28)}}


                                                                    },
        {{{((0x00c4545be4a54004ULL) & ((1 << 28) - 1)), ((0x00c4545be4a54004ULL) >> 28), ((0x0099e11f60357e6cULL) & ((1 << 28) - 1)), ((0x0099e11f60357e6cULL) >> 28), ((0x001f3936d19515a6ULL) & ((1 << 28) - 1)), ((0x001f3936d19515a6ULL) >> 28), ((0x007793df84341a6eULL) & ((1 << 28) - 1)), ((0x007793df84341a6eULL) >> 28), ((0x0051061886717ffaULL) & ((1 << 28) - 1)), ((0x0051061886717ffaULL) >> 28), ((0x00e9b0a660b28f85ULL) & ((1 << 28) - 1)), ((0x00e9b0a660b28f85ULL) >> 28), ((0x0044ea685892de0dULL) & ((1 << 28) - 1)), ((0x0044ea685892de0dULL) >> 28), ((0x000257d2a1fda9d9ULL) & ((1 << 28) - 1)), ((0x000257d2a1fda9d9ULL) >> 28)}}


                                                                    },
        {{{((0x007e8b01b24ac8a8ULL) & ((1 << 28) - 1)), ((0x007e8b01b24ac8a8ULL) >> 28), ((0x006cf3b0b5ca1337ULL) & ((1 << 28) - 1)), ((0x006cf3b0b5ca1337ULL) >> 28), ((0x00f1607d3e36a570ULL) & ((1 << 28) - 1)), ((0x00f1607d3e36a570ULL) >> 28), ((0x0039b7fab82991a1ULL) & ((1 << 28) - 1)), ((0x0039b7fab82991a1ULL) >> 28), ((0x00231777065840c5ULL) & ((1 << 28) - 1)), ((0x00231777065840c5ULL) >> 28), ((0x00998e5afdd346f9ULL) & ((1 << 28) - 1)), ((0x00998e5afdd346f9ULL) >> 28), ((0x00b7dc3e64acc85fULL) & ((1 << 28) - 1)), ((0x00b7dc3e64acc85fULL) >> 28), ((0x00baacc748013ad6ULL) & ((1 << 28) - 1)), ((0x00baacc748013ad6ULL) >> 28)}}


                                                                    },
    }}, {{
        {{{((0x008ea6a4177580bfULL) & ((1 << 28) - 1)), ((0x008ea6a4177580bfULL) >> 28), ((0x005fa1953e3f0378ULL) & ((1 << 28) - 1)), ((0x005fa1953e3f0378ULL) >> 28), ((0x005fe409ac74d614ULL) & ((1 << 28) - 1)), ((0x005fe409ac74d614ULL) >> 28), ((0x00452327f477e047ULL) & ((1 << 28) - 1)), ((0x00452327f477e047ULL) >> 28), ((0x00a4018507fb6073ULL) & ((1 << 28) - 1)), ((0x00a4018507fb6073ULL) >> 28), ((0x007b6e71951caac8ULL) & ((1 << 28) - 1)), ((0x007b6e71951caac8ULL) >> 28), ((0x0012b42ab8a6ce91ULL) & ((1 << 28) - 1)), ((0x0012b42ab8a6ce91ULL) >> 28), ((0x0080eca677294ab7ULL) & ((1 << 28) - 1)), ((0x0080eca677294ab7ULL) >> 28)}}


                                                                    },
        {{{((0x00a53edc023ba69bULL) & ((1 << 28) - 1)), ((0x00a53edc023ba69bULL) >> 28), ((0x00c6afa83ddde2e8ULL) & ((1 << 28) - 1)), ((0x00c6afa83ddde2e8ULL) >> 28), ((0x00c3f638b307b14eULL) & ((1 << 28) - 1)), ((0x00c3f638b307b14eULL) >> 28), ((0x004a357a64414062ULL) & ((1 << 28) - 1)), ((0x004a357a64414062ULL) >> 28), ((0x00e4d94d8b582dc9ULL) & ((1 << 28) - 1)), ((0x00e4d94d8b582dc9ULL) >> 28), ((0x001739caf71695b7ULL) & ((1 << 28) - 1)), ((0x001739caf71695b7ULL) >> 28), ((0x0012431b2ae28de1ULL) & ((1 << 28) - 1)), ((0x0012431b2ae28de1ULL) >> 28), ((0x003b6bc98682907cULL) & ((1 << 28) - 1)), ((0x003b6bc98682907cULL) >> 28)}}


                                                                    },
        {{{((0x008a9a93be1f99d6ULL) & ((1 << 28) - 1)), ((0x008a9a93be1f99d6ULL) >> 28), ((0x0079fa627cc699c8ULL) & ((1 << 28) - 1)), ((0x0079fa627cc699c8ULL) >> 28), ((0x00b0cfb134ba84c8ULL) & ((1 << 28) - 1)), ((0x00b0cfb134ba84c8ULL) >> 28), ((0x001c4b778249419aULL) & ((1 << 28) - 1)), ((0x001c4b778249419aULL) >> 28), ((0x00df4ab3d9c44f40ULL) & ((1 << 28) - 1)), ((0x00df4ab3d9c44f40ULL) >> 28), ((0x009f596e6c1a9e3cULL) & ((1 << 28) - 1)), ((0x009f596e6c1a9e3cULL) >> 28), ((0x001979c0df237316ULL) & ((1 << 28) - 1)), ((0x001979c0df237316ULL) >> 28), ((0x00501e953a919b87ULL) & ((1 << 28) - 1)), ((0x00501e953a919b87ULL) >> 28)}}


                                                                    },
    }}
};
const niels_t *curve448_wnaf_base = curve448_wnaf_base_table;
