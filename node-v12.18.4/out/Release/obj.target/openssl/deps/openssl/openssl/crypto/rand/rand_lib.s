	.file	"rand_lib.c"
	.text
	.p2align 4
	.type	do_rand_init_ossl_, @function
do_rand_init_ossl_:
.LFB820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	CRYPTO_THREAD_lock_new@PLT
	xorl	%edx, %edx
	movq	%rax, rand_engine_lock(%rip)
	testq	%rax, %rax
	je	.L2
	call	CRYPTO_THREAD_lock_new@PLT
	movq	%rax, rand_meth_lock(%rip)
	testq	%rax, %rax
	je	.L3
	call	CRYPTO_THREAD_lock_new@PLT
	movq	%rax, rand_nonce_lock(%rip)
	testq	%rax, %rax
	je	.L4
	call	rand_pool_init@PLT
	testl	%eax, %eax
	je	.L16
	movl	$1, rand_inited(%rip)
	movl	$1, %edx
.L2:
	movl	%edx, do_rand_init_ossl_ret_(%rip)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movq	rand_nonce_lock(%rip), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	movq	$0, rand_nonce_lock(%rip)
.L4:
	movq	rand_meth_lock(%rip), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	movq	$0, rand_meth_lock(%rip)
.L3:
	movq	rand_engine_lock(%rip), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	xorl	%edx, %edx
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	$0, rand_engine_lock(%rip)
	movl	%edx, do_rand_init_ossl_ret_(%rip)
	ret
	.cfi_endproc
.LFE820:
	.size	do_rand_init_ossl_, .-do_rand_init_ossl_
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/rand/rand_lib.c"
	.text
	.p2align 4
	.type	rand_pool_grow, @function
rand_pool_grow:
.LFB835:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	$1, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	40(%rdi), %rax
	movq	8(%rdi), %rdx
	movq	%rax, %rcx
	subq	%rdx, %rcx
	cmpq	%rsi, %rcx
	jnb	.L17
	movl	16(%rdi), %r12d
	movq	%rdi, %rbx
	testl	%r12d, %r12d
	jne	.L19
	movq	32(%rdi), %r13
	movq	%r13, %rcx
	movq	%r13, %rdi
	subq	%rdx, %rcx
	shrq	%rdi
	cmpq	%rsi, %rcx
	jb	.L19
	.p2align 4,,10
	.p2align 3
.L22:
	cmpq	%rdi, %rax
	jb	.L32
.L21:
	movl	20(%rbx), %edx
	testl	%edx, %edx
	jne	.L33
	movl	$637, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r14
.L24:
	testq	%r14, %r14
	je	.L34
	movq	(%rbx), %rsi
	movq	8(%rbx), %rdx
	movq	%r14, %rdi
	call	memcpy@PLT
	movl	20(%rbx), %eax
	movq	40(%rbx), %rsi
	testl	%eax, %eax
	je	.L26
	movq	(%rbx), %rdi
	movl	$644, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_secure_clear_free@PLT
.L27:
	movq	%r14, (%rbx)
	movl	$1, %r12d
	movq	%r13, 40(%rbx)
.L17:
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	xorl	%r12d, %r12d
	movl	$626, %r8d
	movl	$68, %edx
	movl	$125, %esi
	leaq	.LC0(%rip), %rcx
	movl	$36, %edi
	call	ERR_put_error@PLT
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	addq	%rax, %rax
	movq	%rax, %rcx
	subq	%rdx, %rcx
	cmpq	%rsi, %rcx
	jb	.L22
	movq	%rax, %r13
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L33:
	movl	$635, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_secure_zalloc@PLT
	movq	%rax, %r14
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L26:
	movq	(%rbx), %rdi
	movl	$646, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L34:
	movl	$639, %r8d
	movl	$65, %edx
	movl	$125, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L17
	.cfi_endproc
.LFE835:
	.size	rand_pool_grow, .-rand_pool_grow
	.p2align 4
	.globl	rand_drbg_cleanup_entropy
	.type	rand_drbg_cleanup_entropy, @function
rand_drbg_cleanup_entropy:
.LFB815:
	.cfi_startproc
	endbr64
	cmpq	$0, 32(%rdi)
	movq	%rsi, %r8
	movq	%rdx, %rsi
	je	.L38
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	movl	16(%rdi), %eax
	testl	%eax, %eax
	je	.L37
	movl	$221, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r8, %rdi
	jmp	CRYPTO_secure_clear_free@PLT
	.p2align 4,,10
	.p2align 3
.L37:
	movl	$223, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r8, %rdi
	jmp	CRYPTO_clear_free@PLT
	.cfi_endproc
.LFE815:
	.size	rand_drbg_cleanup_entropy, .-rand_drbg_cleanup_entropy
	.p2align 4
	.globl	rand_drbg_cleanup_nonce
	.type	rand_drbg_cleanup_nonce, @function
rand_drbg_cleanup_nonce:
.LFB817:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdi
	movl	$274, %ecx
	movq	%rdx, %rsi
	leaq	.LC0(%rip), %rdx
	jmp	CRYPTO_clear_free@PLT
	.cfi_endproc
.LFE817:
	.size	rand_drbg_cleanup_nonce, .-rand_drbg_cleanup_nonce
	.p2align 4
	.globl	rand_drbg_get_additional_data
	.type	rand_drbg_get_additional_data, @function
rand_drbg_get_additional_data:
.LFB818:
	.cfi_startproc
	endbr64
.L41:
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	rand_pool_add_additional_data@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	je	.L40
	movq	(%rbx), %rdx
	movq	8(%rbx), %rax
	movq	$0, (%rbx)
	movq	$0, 48(%rbx)
	movq	%rdx, (%r12)
.L40:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE818:
	.size	rand_drbg_get_additional_data, .-rand_drbg_get_additional_data
	.p2align 4
	.globl	rand_drbg_cleanup_additional_data
	.type	rand_drbg_cleanup_additional_data, @function
rand_drbg_cleanup_additional_data:
.LFB819:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	movq	%rsi, (%rbx)
	movq	8(%rbx), %rsi
	call	OPENSSL_cleanse@PLT
	movq	$0, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE819:
	.size	rand_drbg_cleanup_additional_data, .-rand_drbg_cleanup_additional_data
	.p2align 4
	.globl	rand_cleanup_int
	.type	rand_cleanup_int, @function
rand_cleanup_int:
.LFB822:
	.cfi_startproc
	endbr64
	movl	rand_inited(%rip), %edx
	testl	%edx, %edx
	je	.L62
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	default_RAND_meth(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rax, %rax
	je	.L50
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L50
	call	*%rax
.L50:
	leaq	do_rand_init_ossl_(%rip), %rsi
	leaq	rand_init(%rip), %rdi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L51
	movl	do_rand_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	je	.L51
	movq	rand_meth_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	funct_ref(%rip), %rdi
	call	ENGINE_finish@PLT
	movq	rand_meth_lock(%rip), %rdi
	movq	$0, funct_ref(%rip)
	movq	$0, default_RAND_meth(%rip)
	call	CRYPTO_THREAD_unlock@PLT
.L51:
	call	rand_pool_cleanup@PLT
	movq	rand_engine_lock(%rip), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	movq	rand_meth_lock(%rip), %rdi
	movq	$0, rand_engine_lock(%rip)
	call	CRYPTO_THREAD_lock_free@PLT
	movq	rand_nonce_lock(%rip), %rdi
	movq	$0, rand_meth_lock(%rip)
	call	CRYPTO_THREAD_lock_free@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	$0, rand_nonce_lock(%rip)
	movl	$0, rand_inited(%rip)
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE822:
	.size	rand_cleanup_int, .-rand_cleanup_int
	.p2align 4
	.globl	RAND_keep_random_devices_open
	.type	RAND_keep_random_devices_open, @function
RAND_keep_random_devices_open:
.LFB823:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	do_rand_init_ossl_(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%edi, %r12d
	leaq	rand_init(%rip), %rdi
	subq	$8, %rsp
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L65
	movl	do_rand_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	jne	.L71
.L65:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	addq	$8, %rsp
	movl	%r12d, %edi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	rand_pool_keep_random_devices_open@PLT
	.cfi_endproc
.LFE823:
	.size	RAND_keep_random_devices_open, .-RAND_keep_random_devices_open
	.p2align 4
	.globl	rand_pool_new
	.type	rand_pool_new, @function
rand_pool_new:
.LFB825:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movl	$437, %edx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movslq	%edi, %r13
	movl	$64, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	leaq	.LC0(%rip), %rsi
	subq	$8, %rsp
	call	CRYPTO_zalloc@PLT
	cmpl	$1, %ebx
	sbbq	%rdx, %rdx
	movq	%rax, %r12
	andl	$32, %edx
	addq	$16, %rdx
	testq	%rax, %rax
	je	.L83
	cmpq	$12288, %r14
	movl	$12288, %edi
	movq	%r15, 24(%rax)
	cmovbe	%r14, %rdi
	cmpq	%r15, %rdx
	cmovb	%r15, %rdx
	movq	%rdi, 32(%rax)
	cmpq	%rdx, %rdi
	jb	.L76
	movq	%rdx, 40(%rax)
	movq	%rdx, %rdi
	testl	%ebx, %ebx
	je	.L78
.L85:
	movl	$453, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_secure_zalloc@PLT
	movq	%rax, (%r12)
.L79:
	testq	%rax, %rax
	je	.L84
	movq	%r13, 56(%r12)
	movl	%ebx, 20(%r12)
.L72:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	movq	%rdi, 40(%rax)
	testl	%ebx, %ebx
	jne	.L85
.L78:
	movl	$455, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_zalloc@PLT
	movq	%rax, (%r12)
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L83:
	movl	$441, %r8d
	movl	$65, %edx
	movl	$116, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L84:
	movl	$458, %r8d
	movl	$65, %edx
	movl	$116, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movl	$468, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L72
	.cfi_endproc
.LFE825:
	.size	rand_pool_new, .-rand_pool_new
	.p2align 4
	.globl	rand_drbg_get_entropy
	.type	rand_drbg_get_entropy, @function
rand_drbg_get_entropy:
.LFB814:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %r10
	movq	%rcx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%r9d, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rax
	movq	%rdi, -56(%rbp)
	testq	%rax, %rax
	je	.L87
	movl	48(%rax), %ecx
	cmpl	%ecx, 48(%rdi)
	jg	.L134
.L87:
	movq	32(%rdi), %r12
	testq	%r12, %r12
	je	.L89
	movq	%r10, 56(%r12)
.L90:
	testq	%rax, %rax
	je	.L91
	movq	48(%r12), %rcx
	movq	56(%r12), %rax
	xorl	%r14d, %r14d
	movq	8(%r12), %rdx
	cmpq	%rax, %rcx
	jb	.L135
.L92:
	movq	24(%r12), %rax
	cmpq	%rax, %rdx
	jb	.L136
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	rand_pool_grow
	testl	%eax, %eax
	je	.L96
	testq	%r14, %r14
	je	.L132
.L95:
	movq	32(%r12), %rax
	subq	8(%r12), %rax
	movl	$779, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$125, %edx
	cmpq	%r14, %rax
	jb	.L133
	cmpq	$0, (%r12)
	je	.L137
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	rand_pool_grow
	testl	%eax, %eax
	je	.L132
	movq	8(%r12), %rsi
	addq	(%r12), %rsi
	movq	-56(%rbp), %rdi
	movq	%rsi, %r15
	je	.L97
	movq	8(%rdi), %rdi
	call	rand_drbg_lock@PLT
	movq	-56(%rbp), %rax
	movq	%r14, %rdx
	leaq	-56(%rbp), %r8
	movl	$8, %r9d
	movl	%r13d, %ecx
	movq	%r15, %rsi
	movq	8(%rax), %rdi
	call	RAND_DRBG_generate@PLT
	testl	%eax, %eax
	movq	-56(%rbp), %rax
	movq	8(%rax), %rdi
	movl	136(%rdi), %edx
	movl	%edx, 140(%rax)
	je	.L100
	call	rand_drbg_unlock@PLT
	movq	8(%r12), %rdx
	movq	40(%r12), %rcx
	leaq	0(,%r14,8), %rax
	subq	%rdx, %rcx
	cmpq	%r14, %rcx
	jb	.L138
	addq	%rdx, %r14
	addq	48(%r12), %rax
	movq	%r14, 8(%r12)
	movq	%rax, 48(%r12)
.L102:
	cmpq	%rax, 56(%r12)
	ja	.L132
	movq	24(%r12), %rsi
	cmpq	%rsi, 8(%r12)
	jnb	.L103
	.p2align 4,,10
	.p2align 3
.L132:
	movq	-56(%rbp), %rdi
.L97:
	xorl	%eax, %eax
.L93:
	cmpq	$0, 32(%rdi)
	je	.L139
.L86:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	testl	%r13d, %r13d
	jne	.L140
	movq	%r12, %rdi
	call	rand_pool_acquire_entropy@PLT
.L103:
	testq	%rax, %rax
	je	.L132
	movq	-56(%rbp), %rdi
	movq	(%r12), %rdx
	movq	$0, 48(%r12)
	movq	$0, (%r12)
	movq	8(%r12), %rax
	cmpq	$0, 32(%rdi)
	movq	%rdx, (%rbx)
	jne	.L86
.L139:
	movl	16(%r12), %ecx
	testl	%ecx, %ecx
	jne	.L105
	movl	20(%r12), %edx
	movq	40(%r12), %rsi
	movq	%rax, -64(%rbp)
	movq	(%r12), %rdi
	testl	%edx, %edx
	je	.L106
	movl	$520, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_secure_clear_free@PLT
	movq	-64(%rbp), %rax
.L105:
	movq	%r12, %rdi
	movl	$525, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	CRYPTO_free@PLT
	movq	-64(%rbp), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	.cfi_restore_state
	subq	%rdx, %rax
	movq	%r12, %rdi
	cmpq	%rax, %r14
	cmovb	%rax, %r14
	movq	%r14, %rsi
	call	rand_pool_grow
	testl	%eax, %eax
	jne	.L95
.L96:
	movq	$0, 8(%r12)
	movq	-56(%rbp), %rdi
	xorl	%eax, %eax
	movq	$0, 32(%r12)
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L135:
	leaq	7(%rax), %r14
	movq	32(%r12), %rax
	subq	%rcx, %r14
	shrq	$3, %r14
	subq	%rdx, %rax
	cmpq	%rax, %r14
	jbe	.L92
	movl	$36, %edi
	movl	$673, %r8d
	movl	$125, %edx
	movl	$115, %esi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-56(%rbp), %rdi
	xorl	%eax, %eax
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L89:
	movl	16(%rdi), %esi
	movq	%r8, %rcx
	movl	%r10d, %edi
	call	rand_pool_new
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L108
	movq	-56(%rbp), %rax
	movq	8(%rax), %rax
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L134:
	movl	$143, %r8d
	movl	$131, %edx
	movl	$120, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	movl	$36, %edi
	movl	$192, %r8d
	movl	$133, %edx
	movl	$120, %esi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-56(%rbp), %rdi
	xorl	%eax, %eax
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L100:
	call	rand_drbg_unlock@PLT
	movq	48(%r12), %rax
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L137:
	movl	$784, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$68, %edx
.L133:
	movl	$36, %edi
	movl	$113, %esi
	call	ERR_put_error@PLT
	movq	-56(%rbp), %rdi
	xorl	%eax, %eax
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L106:
	movl	$522, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movq	-64(%rbp), %rax
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L138:
	movl	$815, %r8d
	movl	$125, %edx
	movl	$114, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	48(%r12), %rax
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L108:
	xorl	%eax, %eax
	jmp	.L86
	.cfi_endproc
.LFE814:
	.size	rand_drbg_get_entropy, .-rand_drbg_get_entropy
	.p2align 4
	.globl	rand_pool_attach
	.type	rand_pool_attach, @function
rand_pool_attach:
.LFB826:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$64, %edi
	subq	$24, %rsp
	movq	%rsi, -24(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rdx, -32(%rbp)
	movl	$481, %edx
	call	CRYPTO_zalloc@PLT
	movq	-24(%rbp), %xmm0
	testq	%rax, %rax
	je	.L145
	movdqa	%xmm0, %xmm1
	movq	%rbx, (%rax)
	movl	$1, 16(%rax)
	punpcklqdq	%xmm1, %xmm1
	movq	%xmm0, 8(%rax)
	movhps	-32(%rbp), %xmm0
	movups	%xmm1, 24(%rax)
	movups	%xmm0, 40(%rax)
.L141:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	movl	$484, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$124, %esi
	movl	$36, %edi
	movq	%rax, -24(%rbp)
	call	ERR_put_error@PLT
	movq	-24(%rbp), %rax
	jmp	.L141
	.cfi_endproc
.LFE826:
	.size	rand_pool_attach, .-rand_pool_attach
	.p2align 4
	.globl	rand_pool_free
	.type	rand_pool_free, @function
rand_pool_free:
.LFB827:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L146
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movl	16(%rdi), %edx
	testl	%edx, %edx
	jne	.L148
	movl	20(%r12), %eax
	movq	40(%rdi), %rsi
	movq	(%rdi), %rdi
	testl	%eax, %eax
	je	.L149
	movl	$520, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_secure_clear_free@PLT
.L148:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$525, %edx
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L149:
	.cfi_restore_state
	movl	$522, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE827:
	.size	rand_pool_free, .-rand_pool_free
	.p2align 4
	.globl	rand_pool_buffer
	.type	rand_pool_buffer, @function
rand_pool_buffer:
.LFB828:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE828:
	.size	rand_pool_buffer, .-rand_pool_buffer
	.p2align 4
	.globl	rand_pool_entropy
	.type	rand_pool_entropy, @function
rand_pool_entropy:
.LFB829:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	ret
	.cfi_endproc
.LFE829:
	.size	rand_pool_entropy, .-rand_pool_entropy
	.p2align 4
	.globl	rand_pool_length
	.type	rand_pool_length, @function
rand_pool_length:
.LFB830:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE830:
	.size	rand_pool_length, .-rand_pool_length
	.p2align 4
	.globl	rand_pool_detach
	.type	rand_pool_detach, @function
rand_pool_detach:
.LFB831:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	$0, 48(%rdi)
	movq	$0, (%rdi)
	ret
	.cfi_endproc
.LFE831:
	.size	rand_pool_detach, .-rand_pool_detach
	.p2align 4
	.globl	rand_pool_reattach
	.type	rand_pool_reattach, @function
rand_pool_reattach:
.LFB857:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	movq	%rsi, (%rbx)
	movq	8(%rbx), %rsi
	call	OPENSSL_cleanse@PLT
	movq	$0, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE857:
	.size	rand_pool_reattach, .-rand_pool_reattach
	.p2align 4
	.globl	rand_pool_entropy_available
	.type	rand_pool_entropy_available, @function
rand_pool_entropy_available:
.LFB833:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	cmpq	56(%rdi), %rax
	jb	.L161
	movq	24(%rdi), %rcx
	movl	$0, %edx
	cmpq	%rcx, 8(%rdi)
	cmovb	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE833:
	.size	rand_pool_entropy_available, .-rand_pool_entropy_available
	.p2align 4
	.globl	rand_pool_entropy_needed
	.type	rand_pool_entropy_needed, @function
rand_pool_entropy_needed:
.LFB834:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rdx
	movq	48(%rdi), %rcx
	movq	%rdx, %rax
	subq	%rcx, %rax
	cmpq	%rdx, %rcx
	movl	$0, %edx
	cmovnb	%rdx, %rax
	ret
	.cfi_endproc
.LFE834:
	.size	rand_pool_entropy_needed, .-rand_pool_entropy_needed
	.p2align 4
	.globl	rand_pool_bytes_needed
	.type	rand_pool_bytes_needed, @function
rand_pool_bytes_needed:
.LFB836:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	48(%rdi), %rax
	movq	%rdi, %rbx
	movq	56(%rdi), %r12
	cmpq	%r12, %rax
	jb	.L195
	testl	%esi, %esi
	je	.L183
	movq	8(%rbx), %rdx
	movq	24(%rbx), %rax
	xorl	%r12d, %r12d
	movq	32(%rbx), %r13
	cmpq	%rdx, %rax
	jbe	.L166
	movq	%r13, %rcx
	subq	%rdx, %rcx
.L182:
	subq	%rdx, %rax
	cmpq	%rax, %r12
	cmovb	%rax, %r12
.L171:
	movq	40(%rbx), %rax
	movq	%rax, %rsi
	subq	%rdx, %rsi
	cmpq	%rsi, %r12
	jbe	.L166
	movl	16(%rbx), %esi
	testl	%esi, %esi
	jne	.L186
	cmpq	%rcx, %r12
	movq	%r13, %rsi
	seta	%cl
	shrq	%rsi
	testb	%cl, %cl
	jne	.L186
	.p2align 4,,10
	.p2align 3
.L176:
	cmpq	%rsi, %rax
	jb	.L196
.L175:
	movl	20(%rbx), %edx
	testl	%edx, %edx
	jne	.L197
	movl	$637, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r14
.L178:
	testq	%r14, %r14
	je	.L198
	movq	(%rbx), %rsi
	movq	8(%rbx), %rdx
	movq	%r14, %rdi
	call	memcpy@PLT
	movl	20(%rbx), %eax
	movq	40(%rbx), %rsi
	testl	%eax, %eax
	je	.L180
	movq	(%rbx), %rdi
	movl	$644, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_secure_clear_free@PLT
.L181:
	movq	%r14, (%rbx)
	movq	%r13, 40(%rbx)
.L166:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L196:
	.cfi_restore_state
	addq	%rax, %rax
	movq	%rax, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %r12
	ja	.L176
	movq	%rax, %r13
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L195:
	subq	%rax, %r12
	movq	%r12, %rax
	testl	%esi, %esi
	je	.L183
	movl	%esi, %r12d
	movq	32(%rdi), %r13
	movq	8(%rdi), %rdx
	imulq	%rax, %r12
	movq	%r13, %rcx
	subq	%rdx, %rcx
	addq	$7, %r12
	shrq	$3, %r12
	cmpq	%r12, %rcx
	jnb	.L170
	xorl	%r12d, %r12d
	movl	$673, %r8d
	movl	$125, %edx
	movl	$115, %esi
	leaq	.LC0(%rip), %rcx
	movl	$36, %edi
	call	ERR_put_error@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L183:
	.cfi_restore_state
	xorl	%r12d, %r12d
	movl	$665, %r8d
	movl	$105, %edx
	movl	$115, %esi
	leaq	.LC0(%rip), %rcx
	movl	$36, %edi
	call	ERR_put_error@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore_state
	movq	24(%rdi), %rax
	cmpq	%rax, %rdx
	jb	.L182
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L197:
	movl	$635, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_secure_zalloc@PLT
	movq	%rax, %r14
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L186:
	movl	$626, %r8d
	movl	$68, %edx
	movl	$125, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L174:
	xorl	%r12d, %r12d
	movq	$0, 8(%rbx)
	movq	%r12, %rax
	movq	$0, 32(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	.cfi_restore_state
	movq	(%rbx), %rdi
	movl	$646, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L198:
	movl	$639, %r8d
	movl	$65, %edx
	movl	$125, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L174
	.cfi_endproc
.LFE836:
	.size	rand_pool_bytes_needed, .-rand_pool_bytes_needed
	.p2align 4
	.globl	rand_pool_bytes_remaining
	.type	rand_pool_bytes_remaining, @function
rand_pool_bytes_remaining:
.LFB837:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	subq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE837:
	.size	rand_pool_bytes_remaining, .-rand_pool_bytes_remaining
	.p2align 4
	.globl	rand_pool_add
	.type	rand_pool_add, @function
rand_pool_add:
.LFB838:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	8(%rdi), %rdx
	movq	32(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%r12, %rax
	jb	.L212
	movq	%rsi, %r13
	movq	(%rdi), %rsi
	movq	%rdi, %rbx
	movl	$727, %r8d
	testq	%rsi, %rsi
	je	.L211
	movl	$1, %eax
	testq	%r12, %r12
	je	.L200
	movq	%rcx, %r14
	cmpq	40(%rdi), %rdx
	jnb	.L204
	addq	%rsi, %rdx
	cmpq	%rdx, %r13
	je	.L213
.L204:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	rand_pool_grow
	testl	%eax, %eax
	jne	.L214
.L200:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L212:
	.cfi_restore_state
	movl	$722, %r8d
	movl	$106, %edx
	movl	$103, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L214:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	%r12, %rdx
	addq	(%rbx), %rdi
	movq	%r13, %rsi
	call	memcpy@PLT
	addq	%r12, 8(%rbx)
	movl	$1, %eax
	addq	%r14, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L213:
	.cfi_restore_state
	movl	$741, %r8d
.L211:
	movl	$68, %edx
	movl	$103, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE838:
	.size	rand_pool_add, .-rand_pool_add
	.p2align 4
	.globl	rand_drbg_get_nonce
	.type	rand_drbg_get_nonce, @function
rand_drbg_get_nonce:
.LFB816:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movl	$437, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	leaq	.LC0(%rip), %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$64, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L235
	cmpq	$12288, %rbx
	movl	$12288, %edi
	movq	%r15, 24(%rax)
	movq	%rax, %r12
	cmovbe	%rbx, %rdi
	movl	$48, %ecx
	cmpq	$48, %r15
	cmovb	%rcx, %r15
	movq	%rdi, 32(%rax)
	cmpq	%r15, %rdi
	jb	.L236
	movq	%r15, 40(%rax)
	movq	%r15, %rdi
.L219:
	movl	$455, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_zalloc@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L237
	movq	$0, 56(%r12)
	movq	%r12, %rdi
	movl	$0, 20(%r12)
	call	rand_pool_add_nonce_data@PLT
	testl	%eax, %eax
	je	.L224
	movq	rand_nonce_lock(%rip), %rcx
	movq	%r13, -80(%rbp)
	leaq	-72(%rbp), %rdx
	movl	$1, %esi
	leaq	rand_nonce_count(%rip), %rdi
	leaq	-80(%rbp), %r13
	call	CRYPTO_atomic_add@PLT
	xorl	%ecx, %ecx
	movl	$16, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	rand_pool_add
	testl	%eax, %eax
	je	.L224
	movq	(%r12), %rax
	movq	8(%r12), %r13
	movq	$0, (%r12)
	movq	$0, 48(%r12)
	movq	%rax, (%r14)
.L221:
	movl	16(%r12), %edx
	testl	%edx, %edx
	jne	.L222
	movl	20(%r12), %eax
	movq	40(%r12), %rsi
	movq	(%r12), %rdi
	testl	%eax, %eax
	je	.L223
	movl	$520, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_secure_clear_free@PLT
.L222:
	movl	$525, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
.L215:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L238
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L236:
	.cfi_restore_state
	movq	%rdi, 40(%rax)
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L224:
	xorl	%r13d, %r13d
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L223:
	movl	$522, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L235:
	movl	$441, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$116, %esi
	movl	$36, %edi
	call	ERR_put_error@PLT
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L237:
	movl	$458, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$116, %esi
	movl	$36, %edi
	call	ERR_put_error@PLT
	movl	$468, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	jmp	.L215
.L238:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE816:
	.size	rand_drbg_get_nonce, .-rand_drbg_get_nonce
	.p2align 4
	.globl	rand_pool_add_begin
	.type	rand_pool_add_begin, @function
rand_pool_add_begin:
.LFB839:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testq	%rsi, %rsi
	je	.L251
	movq	32(%rdi), %r13
	movq	8(%rdi), %rdx
	movq	%rdi, %rbx
	movq	%r13, %rax
	subq	%rdx, %rax
	cmpq	%rsi, %rax
	jb	.L255
	movq	(%rdi), %r12
	testq	%r12, %r12
	je	.L256
	movq	40(%rdi), %rax
	movq	%rax, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %rsi
	jbe	.L243
	movl	16(%rbx), %ecx
	movq	%r13, %rdi
	shrq	%rdi
	testl	%ecx, %ecx
	jne	.L257
	.p2align 4,,10
	.p2align 3
.L244:
	cmpq	%rax, %rdi
	ja	.L258
.L245:
	movl	20(%rbx), %edx
	testl	%edx, %edx
	jne	.L259
	movl	$637, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
.L247:
	testq	%r12, %r12
	je	.L260
	movq	(%rbx), %rsi
	movq	8(%rbx), %rdx
	movq	%r12, %rdi
	call	memcpy@PLT
	movl	20(%rbx), %eax
	movq	40(%rbx), %rsi
	testl	%eax, %eax
	je	.L249
	movq	(%rbx), %rdi
	movl	$644, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_secure_clear_free@PLT
.L250:
	movq	%r12, (%rbx)
	movq	8(%rbx), %rdx
	movq	%r13, 40(%rbx)
.L243:
	addq	%rdx, %r12
.L239:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L258:
	.cfi_restore_state
	addq	%rax, %rax
	movq	%rax, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %rsi
	ja	.L244
	movq	%rax, %r13
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L259:
	movl	$635, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_secure_zalloc@PLT
	movq	%rax, %r12
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L251:
	addq	$8, %rsp
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L257:
	.cfi_restore_state
	movl	$626, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$68, %edx
	xorl	%r12d, %r12d
	movl	$125, %esi
	movl	$36, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	.cfi_restore_state
	movl	$779, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$125, %edx
	xorl	%r12d, %r12d
	movl	$113, %esi
	movl	$36, %edi
	call	ERR_put_error@PLT
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L249:
	movq	(%rbx), %rdi
	movl	$646, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L256:
	movl	$784, %r8d
	movl	$68, %edx
	movl	$113, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L260:
	movl	$639, %r8d
	movl	$65, %edx
	movl	$125, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L239
	.cfi_endproc
.LFE839:
	.size	rand_pool_add_begin, .-rand_pool_add_begin
	.p2align 4
	.globl	rand_pool_add_end
	.type	rand_pool_add_end, @function
rand_pool_add_end:
.LFB840:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	movq	40(%rdi), %rax
	subq	%rcx, %rax
	cmpq	%rsi, %rax
	jb	.L270
	movl	$1, %eax
	testq	%rsi, %rsi
	je	.L267
	addq	%rcx, %rsi
	addq	%rdx, 48(%rdi)
	movq	%rsi, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L267:
	ret
	.p2align 4,,10
	.p2align 3
.L270:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$125, %edx
	movl	$114, %esi
	movl	$36, %edi
	movl	$815, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE840:
	.size	rand_pool_add_end, .-rand_pool_add_end
	.p2align 4
	.globl	RAND_set_rand_method
	.type	RAND_set_rand_method, @function
RAND_set_rand_method:
.LFB841:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	do_rand_init_ossl_(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	rand_init(%rip), %rdi
	subq	$8, %rsp
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L271
	movl	do_rand_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	jne	.L280
.L271:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L280:
	.cfi_restore_state
	movq	rand_meth_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	funct_ref(%rip), %rdi
	call	ENGINE_finish@PLT
	movq	rand_meth_lock(%rip), %rdi
	movq	%rbx, default_RAND_meth(%rip)
	movq	$0, funct_ref(%rip)
	call	CRYPTO_THREAD_unlock@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE841:
	.size	RAND_set_rand_method, .-RAND_set_rand_method
	.p2align 4
	.globl	RAND_get_rand_method
	.type	RAND_get_rand_method, @function
RAND_get_rand_method:
.LFB842:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	do_rand_init_ossl_(%rip), %rsi
	leaq	rand_init(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	xorl	%r12d, %r12d
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L281
	movl	do_rand_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	je	.L281
	movq	rand_meth_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	default_RAND_meth(%rip), %r12
	testq	%r12, %r12
	je	.L294
.L283:
	movq	rand_meth_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
.L281:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L294:
	.cfi_restore_state
	call	ENGINE_get_default_RAND@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L284
	movq	%rax, %rdi
	call	ENGINE_get_RAND@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L284
	movq	%r13, funct_ref(%rip)
	movq	%rax, default_RAND_meth(%rip)
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L284:
	movq	%r13, %rdi
	leaq	rand_meth(%rip), %r12
	call	ENGINE_finish@PLT
	movq	%r12, default_RAND_meth(%rip)
	jmp	.L283
	.cfi_endproc
.LFE842:
	.size	RAND_get_rand_method, .-RAND_get_rand_method
	.p2align 4
	.globl	RAND_poll
	.type	RAND_poll, @function
RAND_poll:
.LFB824:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	call	RAND_get_rand_method
	testq	%rax, %rax
	je	.L314
	movq	%rax, %rbx
	call	RAND_OpenSSL@PLT
	cmpq	%rax, %rbx
	je	.L315
	movl	$437, %edx
	leaq	.LC0(%rip), %rsi
	movl	$64, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L316
	movdqa	.LC1(%rip), %xmm0
	movq	$32, 40(%rax)
	movl	$453, %edx
	leaq	.LC0(%rip), %rsi
	movl	$32, %edi
	movups	%xmm0, 24(%rax)
	call	CRYPTO_secure_zalloc@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L317
	movq	$256, 56(%r12)
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	movl	$1, 20(%r12)
	call	rand_pool_acquire_entropy@PLT
	testq	%rax, %rax
	je	.L302
	movq	24(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L302
	movq	48(%r12), %rax
	testq	%rax, %rax
	js	.L303
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L304:
	mulsd	.LC2(%rip), %xmm0
	movl	8(%r12), %esi
	xorl	%r13d, %r13d
	movq	(%r12), %rdi
	call	*%rdx
	testl	%eax, %eax
	setne	%r13b
.L302:
	movl	16(%r12), %edx
	testl	%edx, %edx
	jne	.L305
	movl	20(%r12), %eax
	movq	40(%r12), %rsi
	movq	(%r12), %rdi
	testl	%eax, %eax
	je	.L306
	movl	$520, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_secure_clear_free@PLT
.L305:
	movq	%r12, %rdi
	movl	$525, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L315:
	.cfi_restore_state
	call	RAND_DRBG_get0_master@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L314
	movq	%rax, %rdi
	call	rand_drbg_lock@PLT
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	rand_drbg_restart@PLT
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	rand_drbg_unlock@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L316:
	.cfi_restore_state
	movl	$441, %r8d
	movl	$65, %edx
	movl	$116, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L314:
	xorl	%r13d, %r13d
.L295:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L303:
	.cfi_restore_state
	movq	%rax, %rcx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rax, %rcx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L306:
	movl	$522, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L317:
	movl	$458, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$116, %esi
	movl	$36, %edi
	call	ERR_put_error@PLT
	movl	$468, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	jmp	.L295
	.cfi_endproc
.LFE824:
	.size	RAND_poll, .-RAND_poll
	.p2align 4
	.globl	RAND_set_rand_engine
	.type	RAND_set_rand_engine, @function
RAND_set_rand_engine:
.LFB843:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	do_rand_init_ossl_(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	rand_init(%rip), %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L332
	movl	do_rand_init_ossl_ret_(%rip), %edx
	testl	%edx, %edx
	je	.L332
	testq	%r12, %r12
	je	.L324
	movq	%r12, %rdi
	call	ENGINE_init@PLT
	testl	%eax, %eax
	jne	.L333
.L332:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L333:
	.cfi_restore_state
	movq	%r12, %rdi
	call	ENGINE_get_RAND@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L334
.L322:
	movq	rand_engine_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	leaq	do_rand_init_ossl_(%rip), %rsi
	leaq	rand_init(%rip), %rdi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L323
	movl	do_rand_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	je	.L323
	movq	rand_meth_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	funct_ref(%rip), %rdi
	call	ENGINE_finish@PLT
	movq	rand_meth_lock(%rip), %rdi
	movq	$0, funct_ref(%rip)
	movq	%rbx, default_RAND_meth(%rip)
	call	CRYPTO_THREAD_unlock@PLT
.L323:
	movq	rand_engine_lock(%rip), %rdi
	movq	%r12, funct_ref(%rip)
	call	CRYPTO_THREAD_unlock@PLT
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L324:
	.cfi_restore_state
	xorl	%ebx, %ebx
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L334:
	movq	%r12, %rdi
	call	ENGINE_finish@PLT
	jmp	.L332
	.cfi_endproc
.LFE843:
	.size	RAND_set_rand_engine, .-RAND_set_rand_engine
	.p2align 4
	.globl	RAND_seed
	.type	RAND_seed, @function
RAND_seed:
.LFB844:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	leaq	do_rand_init_ossl_(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	leaq	rand_init(%rip), %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L335
	movl	do_rand_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	je	.L335
	movq	rand_meth_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	default_RAND_meth(%rip), %rbx
	testq	%rbx, %rbx
	je	.L349
.L339:
	movq	rand_meth_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L335
	popq	%rbx
	movl	%r13d, %esi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L335:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L349:
	.cfi_restore_state
	call	ENGINE_get_default_RAND@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L340
	movq	%rax, %rdi
	call	ENGINE_get_RAND@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L340
	movq	%r14, funct_ref(%rip)
	movq	%rax, default_RAND_meth(%rip)
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L340:
	movq	%r14, %rdi
	leaq	rand_meth(%rip), %rbx
	call	ENGINE_finish@PLT
	movq	%rbx, default_RAND_meth(%rip)
	jmp	.L339
	.cfi_endproc
.LFE844:
	.size	RAND_seed, .-RAND_seed
	.p2align 4
	.globl	RAND_add
	.type	RAND_add, @function
RAND_add:
.LFB845:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	leaq	do_rand_init_ossl_(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	leaq	rand_init(%rip), %rdi
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movsd	%xmm0, -40(%rbp)
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L350
	movl	do_rand_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	je	.L350
	movq	rand_meth_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	default_RAND_meth(%rip), %rbx
	testq	%rbx, %rbx
	je	.L364
.L354:
	movq	rand_meth_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L350
	movsd	-40(%rbp), %xmm0
	addq	$16, %rsp
	movl	%r13d, %esi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L350:
	.cfi_restore_state
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L364:
	.cfi_restore_state
	call	ENGINE_get_default_RAND@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L355
	movq	%rax, %rdi
	call	ENGINE_get_RAND@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L355
	movq	%r14, funct_ref(%rip)
	movq	%rax, default_RAND_meth(%rip)
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L355:
	movq	%r14, %rdi
	leaq	rand_meth(%rip), %rbx
	call	ENGINE_finish@PLT
	movq	%rbx, default_RAND_meth(%rip)
	jmp	.L354
	.cfi_endproc
.LFE845:
	.size	RAND_add, .-RAND_add
	.p2align 4
	.globl	RAND_priv_bytes
	.type	RAND_priv_bytes, @function
RAND_priv_bytes:
.LFB846:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	leaq	rand_init(%rip), %rdi
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	leaq	do_rand_init_ossl_(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -48
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L366
	movl	do_rand_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	je	.L366
	movq	rand_meth_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	default_RAND_meth(%rip), %rbx
	testq	%rbx, %rbx
	je	.L390
.L367:
	movq	rand_meth_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	call	RAND_OpenSSL@PLT
	cmpq	%rax, %rbx
	jne	.L391
.L366:
	call	RAND_DRBG_get0_private@PLT
	movq	%rax, %rdi
	xorl	%eax, %eax
	testq	%rdi, %rdi
	jne	.L392
.L365:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L391:
	.cfi_restore_state
	call	RAND_get_rand_method
	testq	%rax, %rax
	je	.L369
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L369
	popq	%rbx
	movl	%r12d, %esi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L392:
	.cfi_restore_state
	popq	%rbx
	movslq	%r12d, %rdx
	movq	%r13, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	RAND_DRBG_bytes@PLT
	.p2align 4,,10
	.p2align 3
.L390:
	.cfi_restore_state
	call	ENGINE_get_default_RAND@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L368
	movq	%rax, %rdi
	call	ENGINE_get_RAND@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L368
	movq	%r14, funct_ref(%rip)
	movq	%rax, default_RAND_meth(%rip)
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L368:
	movq	%r14, %rdi
	leaq	rand_meth(%rip), %rbx
	call	ENGINE_finish@PLT
	movq	%rbx, default_RAND_meth(%rip)
	jmp	.L367
.L369:
	movl	$940, %r8d
	movl	$101, %edx
	movl	$100, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L365
	.cfi_endproc
.LFE846:
	.size	RAND_priv_bytes, .-RAND_priv_bytes
	.p2align 4
	.globl	RAND_bytes
	.type	RAND_bytes, @function
RAND_bytes:
.LFB847:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	leaq	do_rand_init_ossl_(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	leaq	rand_init(%rip), %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L394
	movl	do_rand_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	je	.L394
	movq	rand_meth_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	default_RAND_meth(%rip), %rbx
	testq	%rbx, %rbx
	je	.L409
.L395:
	movq	rand_meth_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L394
	popq	%rbx
	movl	%r13d, %esi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L394:
	.cfi_restore_state
	movl	$940, %r8d
	movl	$101, %edx
	movl	$100, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	movl	$-1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L409:
	.cfi_restore_state
	call	ENGINE_get_default_RAND@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L396
	movq	%rax, %rdi
	call	ENGINE_get_RAND@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L396
	movq	%r14, funct_ref(%rip)
	movq	%rax, default_RAND_meth(%rip)
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L396:
	movq	%r14, %rdi
	leaq	rand_meth(%rip), %rbx
	call	ENGINE_finish@PLT
	movq	%rbx, default_RAND_meth(%rip)
	jmp	.L395
	.cfi_endproc
.LFE847:
	.size	RAND_bytes, .-RAND_bytes
	.p2align 4
	.globl	RAND_pseudo_bytes
	.type	RAND_pseudo_bytes, @function
RAND_pseudo_bytes:
.LFB848:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	leaq	do_rand_init_ossl_(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	leaq	rand_init(%rip), %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L411
	movl	do_rand_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	je	.L411
	movq	rand_meth_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	default_RAND_meth(%rip), %rbx
	testq	%rbx, %rbx
	je	.L426
.L412:
	movq	rand_meth_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.L411
	popq	%rbx
	movl	%r13d, %esi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L411:
	.cfi_restore_state
	movl	$951, %r8d
	movl	$101, %edx
	movl	$126, %esi
	movl	$36, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	movl	$-1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L426:
	.cfi_restore_state
	call	ENGINE_get_default_RAND@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L413
	movq	%rax, %rdi
	call	ENGINE_get_RAND@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L413
	movq	%r14, funct_ref(%rip)
	movq	%rax, default_RAND_meth(%rip)
	jmp	.L412
	.p2align 4,,10
	.p2align 3
.L413:
	movq	%r14, %rdi
	leaq	rand_meth(%rip), %rbx
	call	ENGINE_finish@PLT
	movq	%rbx, default_RAND_meth(%rip)
	jmp	.L412
	.cfi_endproc
.LFE848:
	.size	RAND_pseudo_bytes, .-RAND_pseudo_bytes
	.p2align 4
	.globl	RAND_status
	.type	RAND_status, @function
RAND_status:
.LFB849:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	do_rand_init_ossl_(%rip), %rsi
	leaq	rand_init(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L436
	movl	do_rand_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	je	.L436
	movq	rand_meth_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	default_RAND_meth(%rip), %rbx
	testq	%rbx, %rbx
	je	.L442
.L431:
	movq	rand_meth_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movq	40(%rbx), %rax
	testq	%rax, %rax
	je	.L436
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L436:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L442:
	.cfi_restore_state
	call	ENGINE_get_default_RAND@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L432
	movq	%rax, %rdi
	call	ENGINE_get_RAND@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L432
	movq	%r12, funct_ref(%rip)
	movq	%rax, default_RAND_meth(%rip)
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L432:
	movq	%r12, %rdi
	leaq	rand_meth(%rip), %rbx
	call	ENGINE_finish@PLT
	movq	%rbx, default_RAND_meth(%rip)
	jmp	.L431
	.cfi_endproc
.LFE849:
	.size	RAND_status, .-RAND_status
	.local	do_rand_init_ossl_ret_
	.comm	do_rand_init_ossl_ret_,4,4
	.local	rand_inited
	.comm	rand_inited,4,4
	.local	rand_nonce_count
	.comm	rand_nonce_count,4,4
	.local	rand_nonce_lock
	.comm	rand_nonce_lock,8,8
	.local	rand_init
	.comm	rand_init,4,4
	.local	default_RAND_meth
	.comm	default_RAND_meth,8,8
	.local	rand_meth_lock
	.comm	rand_meth_lock,8,8
	.local	rand_engine_lock
	.comm	rand_engine_lock,8,8
	.local	funct_ref
	.comm	funct_ref,8,8
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.quad	32
	.quad	12288
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC2:
	.long	0
	.long	1069547520
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
