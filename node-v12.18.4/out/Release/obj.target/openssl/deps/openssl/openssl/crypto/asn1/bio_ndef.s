	.file	"bio_ndef.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/asn1/bio_ndef.c"
	.text
	.p2align 4
	.type	ndef_suffix, @function
ndef_suffix:
.LFB399:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L11
	movq	(%rcx), %rbx
	movq	%rdx, %r13
	movq	%rsi, %r12
	movl	$11, %edi
	movq	8(%rbx), %rdx
	movdqu	8(%rbx), %xmm1
	movq	%rbx, %rsi
	movq	32(%rbx), %rcx
	movdqu	24(%rbx), %xmm0
	movq	32(%rdx), %rax
	movq	%rcx, -48(%rbp)
	shufpd	$2, %xmm1, %xmm0
	leaq	-64(%rbp), %rcx
	movaps	%xmm0, -64(%rbp)
	call	*24(%rax)
	testl	%eax, %eax
	jle	.L11
	movq	8(%rbx), %rdx
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	call	ASN1_item_ndef_i2d@PLT
	movl	$186, %edx
	leaq	.LC0(%rip), %rsi
	movslq	%eax, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	je	.L13
	movq	8(%rbx), %rdx
	movq	(%rbx), %rdi
	movq	%rax, 40(%rbx)
	leaq	-72(%rbp), %rsi
	movq	%rax, (%r12)
	call	ASN1_item_ndef_i2d@PLT
	movq	32(%rbx), %rdx
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L11
	movq	%rdx, (%r12)
	subq	40(%rbx), %rdx
	subl	%edx, %eax
	movl	%eax, 0(%r13)
	movl	$1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$187, %r8d
	movl	$65, %edx
	movl	$136, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L11:
	xorl	%eax, %eax
.L1:
	movq	-40(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L14
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L14:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE399:
	.size	ndef_suffix, .-ndef_suffix
	.p2align 4
	.type	ndef_suffix_free, @function
ndef_suffix_free:
.LFB398:
	.cfi_startproc
	endbr64
	testq	%rcx, %rcx
	je	.L19
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	(%rcx), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	leaq	.LC0(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	movl	$143, %edx
	pushq	%rbx
	.cfi_offset 3, -48
	movq	40(%r14), %rdi
	movq	%rcx, %rbx
	call	CRYPTO_free@PLT
	movq	$0, 40(%r14)
	movq	(%rbx), %rdi
	movl	$157, %edx
	movq	$0, 0(%r13)
	leaq	.LC0(%rip), %rsi
	movl	$0, (%r12)
	call	CRYPTO_free@PLT
	movq	$0, (%rbx)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE398:
	.size	ndef_suffix_free, .-ndef_suffix_free
	.p2align 4
	.type	ndef_prefix, @function
ndef_prefix:
.LFB396:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L34
	movq	(%rcx), %r13
	movq	%rdx, %r12
	movq	%rsi, %rbx
	xorl	%esi, %esi
	movq	8(%r13), %rdx
	movq	0(%r13), %rdi
	call	ASN1_item_ndef_i2d@PLT
	movl	$116, %edx
	leaq	.LC0(%rip), %rsi
	movslq	%eax, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, -48(%rbp)
	testq	%rax, %rax
	je	.L35
	movq	8(%r13), %rdx
	movq	0(%r13), %rdi
	movq	%rax, 40(%r13)
	leaq	-48(%rbp), %rsi
	movq	%rax, (%rbx)
	call	ASN1_item_ndef_i2d@PLT
	movq	32(%r13), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L34
	subq	(%rbx), %rax
	movl	%eax, (%r12)
	movl	$1, %eax
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L35:
	movl	$117, %r8d
	movl	$65, %edx
	movl	$127, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L34:
	xorl	%eax, %eax
.L24:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L36
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L36:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE396:
	.size	ndef_prefix, .-ndef_prefix
	.p2align 4
	.type	ndef_prefix_free, @function
ndef_prefix_free:
.LFB397:
	.cfi_startproc
	endbr64
	testq	%rcx, %rcx
	je	.L41
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	leaq	.LC0(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	movl	$143, %edx
	subq	$8, %rsp
	movq	(%rcx), %r13
	movq	40(%r13), %rdi
	call	CRYPTO_free@PLT
	movq	$0, 40(%r13)
	movl	$1, %eax
	movq	$0, (%r12)
	movl	$0, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE397:
	.size	ndef_prefix_free, .-ndef_prefix_free
	.p2align 4
	.globl	BIO_new_NDEF
	.type	BIO_new_NDEF, @function
BIO_new_NDEF:
.LFB395:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -88(%rbp)
	movq	32(%rdx), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r15, %r15
	je	.L47
	cmpq	$0, 24(%r15)
	je	.L47
	leaq	.LC0(%rip), %rsi
	movq	%rdi, %r14
	movq	%rdx, %rbx
	movl	$48, %edi
	movl	$63, %edx
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	call	BIO_f_asn1@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, %r13
	testq	%r12, %r12
	je	.L50
	testq	%rax, %rax
	je	.L50
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	BIO_push@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L50
	leaq	ndef_prefix_free(%rip), %rdx
	leaq	ndef_prefix(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_asn1_set_prefix@PLT
	leaq	ndef_suffix_free(%rip), %rdx
	leaq	ndef_suffix(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_asn1_set_suffix@PLT
	movq	%r14, -80(%rbp)
	leaq	-80(%rbp), %rcx
	movq	%rbx, %rdx
	movq	$0, -72(%rbp)
	leaq	-88(%rbp), %rsi
	movl	$10, %edi
	movq	$0, -64(%rbp)
	call	*24(%r15)
	testl	%eax, %eax
	jle	.L50
	movq	-88(%rbp), %rax
	movq	%rbx, 8(%r12)
	movq	%r12, %rcx
	xorl	%edx, %edx
	movq	%r14, 24(%r12)
	movl	$153, %esi
	movq	%r13, %rdi
	movq	%rax, (%r12)
	movq	-72(%rbp), %rax
	movq	%rax, 16(%r12)
	movq	-64(%rbp), %rax
	movq	%rax, 32(%r12)
	call	BIO_ctrl@PLT
	movq	-72(%rbp), %rax
.L46:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L59
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	movq	%r13, %rdi
	call	BIO_free@PLT
	movl	$100, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	xorl	%eax, %eax
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L47:
	movl	$60, %r8d
	movl	$202, %edx
	movl	$208, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L46
.L59:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE395:
	.size	BIO_new_NDEF, .-BIO_new_NDEF
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
