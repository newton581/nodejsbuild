	.file	"ocsp_prn.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	"%*sCertificate ID:\n"
.LC2:
	.string	"%*sHash Algorithm: "
.LC3:
	.string	"\n%*sIssuer Name Hash: "
.LC4:
	.string	"\n%*sIssuer Key Hash: "
.LC5:
	.string	"\n%*sSerial Number: "
.LC6:
	.string	"\n"
	.text
	.p2align 4
	.type	ocsp_certid_print, @function
ocsp_certid_print:
.LFB1392:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rcx
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	addl	$2, %r13d
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	leaq	.LC1(%rip), %rsi
	subq	$8, %rsp
	call	BIO_printf@PLT
	movl	%r13d, %edx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rcx
	leaq	.LC2(%rip), %rsi
	call	BIO_printf@PLT
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	i2a_ASN1_OBJECT@PLT
	movl	%r13d, %edx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	call	BIO_printf@PLT
	leaq	16(%rbx), %rsi
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	i2a_ASN1_STRING@PLT
	movl	%r13d, %edx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rcx
	leaq	.LC4(%rip), %rsi
	call	BIO_printf@PLT
	leaq	40(%rbx), %rsi
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	i2a_ASN1_STRING@PLT
	movl	%r13d, %edx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	call	BIO_printf@PLT
	leaq	64(%rbx), %rsi
	movq	%r12, %rdi
	call	i2a_ASN1_INTEGER@PLT
	movq	%r12, %rdi
	leaq	.LC6(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1392:
	.size	ocsp_certid_print, .-ocsp_certid_print
	.section	.rodata.str1.1
.LC7:
	.string	"(UNKNOWN)"
	.text
	.p2align 4
	.globl	OCSP_response_status_str
	.type	OCSP_response_status_str, @function
OCSP_response_status_str:
.LFB1394:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L7
	cmpq	$1, %rdi
	je	.L8
	cmpq	$2, %rdi
	je	.L9
	cmpq	$3, %rdi
	je	.L10
	cmpq	$5, %rdi
	je	.L11
	leaq	.LC7(%rip), %rax
	cmpq	$6, %rdi
	je	.L13
	ret
.L7:
	leaq	rstat_tbl.21816(%rip), %rax
.L5:
	movq	8(%rax), %rax
	ret
.L8:
	leaq	16+rstat_tbl.21816(%rip), %rax
	jmp	.L5
.L13:
	leaq	80+rstat_tbl.21816(%rip), %rax
	jmp	.L5
.L9:
	leaq	32+rstat_tbl.21816(%rip), %rax
	jmp	.L5
.L10:
	leaq	48+rstat_tbl.21816(%rip), %rax
	jmp	.L5
.L11:
	leaq	64+rstat_tbl.21816(%rip), %rax
	jmp	.L5
	.cfi_endproc
.LFE1394:
	.size	OCSP_response_status_str, .-OCSP_response_status_str
	.p2align 4
	.globl	OCSP_cert_status_str
	.type	OCSP_cert_status_str, @function
OCSP_cert_status_str:
.LFB1395:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L17
	cmpq	$1, %rdi
	je	.L18
	leaq	.LC7(%rip), %rax
	cmpq	$2, %rdi
	je	.L20
	ret
.L17:
	leaq	cstat_tbl.21820(%rip), %rax
.L15:
	movq	8(%rax), %rax
	ret
.L18:
	leaq	16+cstat_tbl.21820(%rip), %rax
	jmp	.L15
.L20:
	leaq	32+cstat_tbl.21820(%rip), %rax
	jmp	.L15
	.cfi_endproc
.LFE1395:
	.size	OCSP_cert_status_str, .-OCSP_cert_status_str
	.p2align 4
	.globl	OCSP_crl_reason_str
	.type	OCSP_crl_reason_str, @function
OCSP_crl_reason_str:
.LFB1396:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L24
	cmpq	$1, %rdi
	je	.L25
	cmpq	$2, %rdi
	je	.L26
	cmpq	$3, %rdi
	je	.L27
	cmpq	$4, %rdi
	je	.L28
	cmpq	$5, %rdi
	je	.L29
	cmpq	$6, %rdi
	je	.L30
	leaq	.LC7(%rip), %rax
	cmpq	$8, %rdi
	je	.L32
	ret
.L24:
	leaq	reason_tbl.21824(%rip), %rax
.L22:
	movq	8(%rax), %rax
	ret
.L25:
	leaq	16+reason_tbl.21824(%rip), %rax
	jmp	.L22
.L32:
	leaq	112+reason_tbl.21824(%rip), %rax
	jmp	.L22
.L26:
	leaq	32+reason_tbl.21824(%rip), %rax
	jmp	.L22
.L27:
	leaq	48+reason_tbl.21824(%rip), %rax
	jmp	.L22
.L28:
	leaq	64+reason_tbl.21824(%rip), %rax
	jmp	.L22
.L29:
	leaq	80+reason_tbl.21824(%rip), %rax
	jmp	.L22
.L30:
	leaq	96+reason_tbl.21824(%rip), %rax
	jmp	.L22
	.cfi_endproc
.LFE1396:
	.size	OCSP_crl_reason_str, .-OCSP_crl_reason_str
	.section	.rodata.str1.1
.LC8:
	.string	"OCSP Request Data:\n"
.LC9:
	.string	"    Version: %lu (0x%lx)"
.LC10:
	.string	"\n    Requestor Name: "
.LC11:
	.string	"\n    Requestor List:\n"
.LC12:
	.string	"Request Single Extensions"
.LC13:
	.string	"Request Extensions"
	.text
	.p2align 4
	.globl	OCSP_REQUEST_print
	.type	OCSP_REQUEST_print, @function
OCSP_REQUEST_print:
.LFB1397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movl	$19, %edx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	32(%rsi), %rax
	leaq	.LC8(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	BIO_write@PLT
	testl	%eax, %eax
	jle	.L36
	movq	(%r12), %rdi
	call	ASN1_INTEGER_get@PLT
	leaq	.LC9(%rip), %rsi
	movq	%rbx, %rdi
	movq	%rax, %rcx
	leaq	1(%rax), %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L36
	cmpq	$0, 8(%r12)
	je	.L37
	movl	$21, %edx
	leaq	.LC10(%rip), %rsi
	movq	%rbx, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jle	.L36
	movq	8(%r12), %rsi
	movq	%rbx, %rdi
	call	GENERAL_NAME_print@PLT
.L37:
	movl	$21, %edx
	leaq	.LC11(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%r15d, %r15d
	call	BIO_write@PLT
	testl	%eax, %eax
	jg	.L38
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L39:
	movq	16(%r12), %rdi
	movl	%r15d, %esi
	call	OPENSSL_sk_value@PLT
	movl	$8, %edx
	movq	%rbx, %rdi
	movq	(%rax), %rsi
	movq	%rax, %r14
	call	ocsp_certid_print
	movq	8(%r14), %rdx
	movq	%r13, %rcx
	movq	%rbx, %rdi
	movl	$8, %r8d
	leaq	.LC12(%rip), %rsi
	call	X509V3_extensions_print@PLT
	testl	%eax, %eax
	je	.L36
	addl	$1, %r15d
.L38:
	movq	16(%r12), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r15d
	jl	.L39
	movq	24(%r12), %rdx
	movl	$4, %r8d
	movq	%r13, %rcx
	movq	%rbx, %rdi
	leaq	.LC13(%rip), %rsi
	call	X509V3_extensions_print@PLT
	testl	%eax, %eax
	je	.L36
	cmpq	$0, -56(%rbp)
	je	.L43
	movq	-56(%rbp), %rsi
	movq	%rbx, %rdi
	xorl	%r12d, %r12d
	movq	16(%rsi), %rdx
	call	X509_signature_print@PLT
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L42:
	movq	-56(%rbp), %r14
	movl	%r12d, %esi
	movq	24(%r14), %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	X509_print@PLT
	movq	24(%r14), %rdi
	movl	%r12d, %esi
	addl	$1, %r12d
	call	OPENSSL_sk_value@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	PEM_write_bio_X509@PLT
.L41:
	movq	-56(%rbp), %rax
	movq	24(%rax), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jl	.L42
.L43:
	movl	$1, %eax
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L36:
	xorl	%eax, %eax
.L33:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1397:
	.size	OCSP_REQUEST_print, .-OCSP_REQUEST_print
	.section	.rodata.str1.1
.LC14:
	.string	"OCSP Response Data:\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC15:
	.string	"    OCSP Response Status: %s (0x%lx)\n"
	.section	.rodata.str1.1
.LC16:
	.string	"    Response Type: "
.LC17:
	.string	" (unknown response type)\n"
.LC18:
	.string	"\n    Version: %lu (0x%lx)\n"
.LC19:
	.string	"    Responder Id: "
.LC20:
	.string	"\n    Produced At: "
.LC21:
	.string	"\n    Responses:\n"
.LC22:
	.string	"    Cert Status: %s"
.LC23:
	.string	"\n    This Update: "
.LC24:
	.string	"\n    Revocation Time: "
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"\n    Revocation Reason: %s (0x%lx)"
	.section	.rodata.str1.1
.LC26:
	.string	"\n    Next Update: "
.LC27:
	.string	"Response Single Extensions"
.LC28:
	.string	"Response Extensions"
	.text
	.p2align 4
	.globl	OCSP_RESPONSE_print
	.type	OCSP_RESPONSE_print, @function
OCSP_RESPONSE_print:
.LFB1398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	8(%rsi), %rbx
	leaq	.LC14(%rip), %rsi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L56
	movq	(%r14), %rdi
	call	ASN1_ENUMERATED_get@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L78
	cmpq	$1, %rax
	je	.L79
	cmpq	$2, %rax
	je	.L80
	cmpq	$3, %rax
	je	.L81
	cmpq	$5, %rax
	je	.L82
	leaq	.LC7(%rip), %rdx
	leaq	80+rstat_tbl.21816(%rip), %rax
	cmpq	$6, %rcx
	je	.L54
.L55:
	xorl	%eax, %eax
	leaq	.LC15(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L56
	movl	$1, %r15d
	testq	%rbx, %rbx
	je	.L51
	leaq	.LC16(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L56
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	i2a_ASN1_OBJECT@PLT
	testl	%eax, %eax
	jle	.L56
	movq	(%rbx), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$365, %eax
	jne	.L137
	movq	%r14, %rdi
	call	OCSP_response_get1_basic@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L56
	movq	(%rax), %rdi
	xorl	%r15d, %r15d
	call	ASN1_INTEGER_get@PLT
	leaq	.LC18(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	leaq	1(%rax), %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L53
	leaq	.LC19(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L53
	movl	8(%r14), %eax
	testl	%eax, %eax
	je	.L59
	cmpl	$1, %eax
	jne	.L61
	movq	16(%r14), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	i2a_ASN1_STRING@PLT
.L61:
	leaq	.LC20(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	xorl	%r15d, %r15d
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L53
	movq	24(%r14), %rsi
	movq	%r12, %rdi
	call	ASN1_GENERALIZEDTIME_print@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L53
	leaq	.LC21(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	xorl	%r15d, %r15d
	call	BIO_printf@PLT
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jg	.L62
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L74:
	addl	$1, %ebx
.L62:
	movq	32(%r14), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L138
	movq	32(%r14), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	testq	%rax, %rax
	je	.L74
	movq	32(%r14), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movl	$4, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	movq	%rax, -56(%rbp)
	movq	%rax, %r15
	call	ocsp_certid_print
	testl	%eax, %eax
	jle	.L108
	movq	8(%r15), %r15
	movslq	(%r15), %rax
	testq	%rax, %rax
	je	.L90
	cmpq	$1, %rax
	je	.L91
	leaq	.LC7(%rip), %rdx
	cmpq	$2, %rax
	je	.L139
.L65:
	xorl	%eax, %eax
	leaq	.LC22(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L108
	cmpl	$1, (%r15)
	je	.L66
.L68:
	xorl	%eax, %eax
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L108
	movq	-56(%rbp), %rax
	movq	%r12, %rdi
	movq	16(%rax), %rsi
	call	ASN1_GENERALIZEDTIME_print@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L53
	movq	-56(%rbp), %rax
	cmpq	$0, 24(%rax)
	je	.L73
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L108
	movq	-56(%rbp), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rsi
	call	ASN1_GENERALIZEDTIME_print@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L53
.L73:
	movl	$1, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jle	.L108
	movq	-56(%rbp), %rax
	movl	$8, %r8d
	movq	%r13, %rcx
	movq	%r12, %rdi
	leaq	.LC27(%rip), %rsi
	movq	32(%rax), %rdx
	call	X509V3_extensions_print@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L53
	movl	$1, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jg	.L74
.L108:
	xorl	%r15d, %r15d
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L56:
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
.L53:
	movq	%r14, %rdi
	call	OCSP_BASICRESP_free@PLT
.L51:
	addq	$24, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L79:
	.cfi_restore_state
	leaq	16+rstat_tbl.21816(%rip), %rax
	.p2align 4,,10
	.p2align 3
.L54:
	movq	8(%rax), %rdx
	jmp	.L55
.L137:
	leaq	.LC17(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	jmp	.L51
.L80:
	leaq	32+rstat_tbl.21816(%rip), %rax
	jmp	.L54
.L78:
	leaq	rstat_tbl.21816(%rip), %rax
	jmp	.L54
.L81:
	leaq	48+rstat_tbl.21816(%rip), %rax
	jmp	.L54
.L82:
	leaq	64+rstat_tbl.21816(%rip), %rax
	jmp	.L54
.L59:
	movq	16(%r14), %rsi
	movl	$8520479, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	X509_NAME_print_ex@PLT
	jmp	.L61
.L66:
	movq	8(%r15), %rax
	leaq	.LC24(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L108
	movq	(%r15), %rsi
	movq	%r12, %rdi
	movq	%r15, -64(%rbp)
	call	ASN1_GENERALIZEDTIME_print@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L53
	movq	-64(%rbp), %rax
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L68
	call	ASN1_ENUMERATED_get@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L96
	cmpq	$1, %rax
	je	.L97
	cmpq	$2, %rax
	je	.L98
	cmpq	$3, %rax
	je	.L99
	cmpq	$4, %rax
	je	.L100
	cmpq	$5, %rax
	je	.L101
	cmpq	$6, %rax
	je	.L102
	leaq	.LC7(%rip), %rdx
	leaq	112+reason_tbl.21824(%rip), %rax
	cmpq	$8, %rcx
	je	.L69
.L70:
	xorl	%eax, %eax
	leaq	.LC25(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jg	.L68
	jmp	.L108
.L138:
	movq	40(%r14), %rdx
	movl	$4, %r8d
	movq	%r13, %rcx
	movq	%r12, %rdi
	leaq	.LC28(%rip), %rsi
	call	X509V3_extensions_print@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L53
	movq	64(%r14), %rdx
	leaq	48(%r14), %rsi
	movq	%r12, %rdi
	call	X509_signature_print@PLT
	testl	%eax, %eax
	jle	.L108
	xorl	%ebx, %ebx
	jmp	.L76
.L77:
	movq	72(%r14), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	X509_print@PLT
	movq	72(%r14), %rdi
	movl	%ebx, %esi
	addl	$1, %ebx
	call	OPENSSL_sk_value@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	PEM_write_bio_X509@PLT
.L76:
	movq	72(%r14), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L77
	movl	$1, %r15d
	jmp	.L53
.L90:
	leaq	cstat_tbl.21820(%rip), %rax
.L64:
	movq	8(%rax), %rdx
	jmp	.L65
.L139:
	leaq	32+cstat_tbl.21820(%rip), %rax
	jmp	.L64
.L91:
	leaq	16+cstat_tbl.21820(%rip), %rax
	jmp	.L64
.L97:
	leaq	16+reason_tbl.21824(%rip), %rax
.L69:
	movq	8(%rax), %rdx
	jmp	.L70
.L102:
	leaq	96+reason_tbl.21824(%rip), %rax
	jmp	.L69
.L101:
	leaq	80+reason_tbl.21824(%rip), %rax
	jmp	.L69
.L100:
	leaq	64+reason_tbl.21824(%rip), %rax
	jmp	.L69
.L99:
	leaq	48+reason_tbl.21824(%rip), %rax
	jmp	.L69
.L98:
	leaq	32+reason_tbl.21824(%rip), %rax
	jmp	.L69
.L96:
	leaq	reason_tbl.21824(%rip), %rax
	jmp	.L69
	.cfi_endproc
.LFE1398:
	.size	OCSP_RESPONSE_print, .-OCSP_RESPONSE_print
	.section	.rodata.str1.1
.LC29:
	.string	"unspecified"
.LC30:
	.string	"keyCompromise"
.LC31:
	.string	"cACompromise"
.LC32:
	.string	"affiliationChanged"
.LC33:
	.string	"superseded"
.LC34:
	.string	"cessationOfOperation"
.LC35:
	.string	"certificateHold"
.LC36:
	.string	"removeFromCRL"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	reason_tbl.21824, @object
	.size	reason_tbl.21824, 128
reason_tbl.21824:
	.quad	0
	.quad	.LC29
	.quad	1
	.quad	.LC30
	.quad	2
	.quad	.LC31
	.quad	3
	.quad	.LC32
	.quad	4
	.quad	.LC33
	.quad	5
	.quad	.LC34
	.quad	6
	.quad	.LC35
	.quad	8
	.quad	.LC36
	.section	.rodata.str1.1
.LC37:
	.string	"good"
.LC38:
	.string	"revoked"
.LC39:
	.string	"unknown"
	.section	.data.rel.ro.local
	.align 32
	.type	cstat_tbl.21820, @object
	.size	cstat_tbl.21820, 48
cstat_tbl.21820:
	.quad	0
	.quad	.LC37
	.quad	1
	.quad	.LC38
	.quad	2
	.quad	.LC39
	.section	.rodata.str1.1
.LC40:
	.string	"successful"
.LC41:
	.string	"malformedrequest"
.LC42:
	.string	"internalerror"
.LC43:
	.string	"trylater"
.LC44:
	.string	"sigrequired"
.LC45:
	.string	"unauthorized"
	.section	.data.rel.ro.local
	.align 32
	.type	rstat_tbl.21816, @object
	.size	rstat_tbl.21816, 96
rstat_tbl.21816:
	.quad	0
	.quad	.LC40
	.quad	1
	.quad	.LC41
	.quad	2
	.quad	.LC42
	.quad	3
	.quad	.LC43
	.quad	5
	.quad	.LC44
	.quad	6
	.quad	.LC45
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
