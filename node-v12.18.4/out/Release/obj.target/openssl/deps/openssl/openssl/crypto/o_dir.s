	.file	"o_dir.c"
	.text
	.p2align 4
	.globl	OPENSSL_DIR_read
	.type	OPENSSL_DIR_read, @function
OPENSSL_DIR_read:
.LFB162:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	testq	%rdi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	sete	%r12b
	testq	%rsi, %rsi
	pushq	%rbx
	.cfi_offset 3, -48
	sete	%al
	movq	%rdi, %rbx
	orl	%eax, %r12d
	call	__errno_location@PLT
	movq	%rax, %r14
	testb	%r12b, %r12b
	jne	.L12
	movl	$0, (%rax)
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L13
	movq	(%rax), %r12
.L6:
	movq	%r12, %rdi
	call	readdir@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1
	leaq	19(%rax), %rsi
	movq	(%rbx), %rax
	movl	$4097, %edx
	leaq	8(%rax), %rdi
	call	OPENSSL_strlcpy@PLT
	movq	(%rbx), %r12
	addq	$8, %r12
.L1:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	movl	$1, %esi
	movl	$4112, %edi
	call	calloc@PLT
	movq	%rax, (%rbx)
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L14
	movq	%r13, %rdi
	call	opendir@PLT
	movq	(%rbx), %rdi
	movq	%rax, (%r12)
	movq	(%rdi), %r12
	testq	%r12, %r12
	jne	.L6
	movl	(%r14), %r13d
	call	free@PLT
	movq	$0, (%rbx)
	movl	%r13d, (%r14)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L12:
	xorl	%r12d, %r12d
	movl	$22, (%rax)
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L14:
	.cfi_restore_state
	movl	$12, (%r14)
	jmp	.L1
	.cfi_endproc
.LFE162:
	.size	OPENSSL_DIR_read, .-OPENSSL_DIR_read
	.p2align 4
	.globl	OPENSSL_DIR_end
	.type	OPENSSL_DIR_end, @function
OPENSSL_DIR_end:
.LFB163:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	testq	%rdi, %rdi
	je	.L16
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	testq	%rax, %rax
	je	.L16
	movq	(%rax), %rdi
	call	closedir@PLT
	movq	(%rbx), %rdi
	movl	%eax, %r12d
	call	free@PLT
	xorl	%eax, %eax
	cmpl	$-1, %r12d
	je	.L15
	movl	$1, %eax
	testl	%r12d, %r12d
	je	.L15
.L16:
	call	__errno_location@PLT
	movl	$22, (%rax)
	xorl	%eax, %eax
.L15:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE163:
	.size	OPENSSL_DIR_end, .-OPENSSL_DIR_end
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
