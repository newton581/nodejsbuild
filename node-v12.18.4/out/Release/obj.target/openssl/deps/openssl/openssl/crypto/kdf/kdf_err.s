	.file	"kdf_err.c"
	.text
	.p2align 4
	.globl	ERR_load_KDF_strings
	.type	ERR_load_KDF_strings, @function
ERR_load_KDF_strings:
.LFB203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$872837120, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_func_error_string@PLT
	testq	%rax, %rax
	je	.L5
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	leaq	KDF_str_functs(%rip), %rdi
	call	ERR_load_strings_const@PLT
	leaq	KDF_str_reasons(%rip), %rdi
	call	ERR_load_strings_const@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE203:
	.size	ERR_load_KDF_strings, .-ERR_load_KDF_strings
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"invalid digest"
.LC1:
	.string	"missing iteration count"
.LC2:
	.string	"missing key"
.LC3:
	.string	"missing message digest"
.LC4:
	.string	"missing parameter"
.LC5:
	.string	"missing pass"
.LC6:
	.string	"missing salt"
.LC7:
	.string	"missing secret"
.LC8:
	.string	"missing seed"
.LC9:
	.string	"unknown parameter type"
.LC10:
	.string	"value error"
.LC11:
	.string	"value missing"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	KDF_str_reasons, @object
	.size	KDF_str_reasons, 208
KDF_str_reasons:
	.quad	872415332
	.quad	.LC0
	.quad	872415341
	.quad	.LC1
	.quad	872415336
	.quad	.LC2
	.quad	872415337
	.quad	.LC3
	.quad	872415333
	.quad	.LC4
	.quad	872415342
	.quad	.LC5
	.quad	872415343
	.quad	.LC6
	.quad	872415339
	.quad	.LC7
	.quad	872415338
	.quad	.LC8
	.quad	872415335
	.quad	.LC9
	.quad	872415340
	.quad	.LC10
	.quad	872415334
	.quad	.LC11
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC12:
	.string	"pkey_hkdf_ctrl_str"
.LC13:
	.string	"pkey_hkdf_derive"
.LC14:
	.string	"pkey_hkdf_init"
.LC15:
	.string	"pkey_scrypt_ctrl_str"
.LC16:
	.string	"pkey_scrypt_ctrl_uint64"
.LC17:
	.string	"pkey_scrypt_derive"
.LC18:
	.string	"pkey_scrypt_init"
.LC19:
	.string	"pkey_scrypt_set_membuf"
.LC20:
	.string	"pkey_tls1_prf_ctrl_str"
.LC21:
	.string	"pkey_tls1_prf_derive"
.LC22:
	.string	"pkey_tls1_prf_init"
.LC23:
	.string	"tls1_prf_alg"
	.section	.data.rel.ro.local
	.align 32
	.type	KDF_str_functs, @object
	.size	KDF_str_functs, 208
KDF_str_functs:
	.quad	872837120
	.quad	.LC12
	.quad	872833024
	.quad	.LC13
	.quad	872857600
	.quad	.LC14
	.quad	872841216
	.quad	.LC15
	.quad	872845312
	.quad	.LC16
	.quad	872861696
	.quad	.LC17
	.quad	872849408
	.quad	.LC18
	.quad	872853504
	.quad	.LC19
	.quad	872824832
	.quad	.LC20
	.quad	872828928
	.quad	.LC21
	.quad	872865792
	.quad	.LC22
	.quad	872869888
	.quad	.LC23
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
