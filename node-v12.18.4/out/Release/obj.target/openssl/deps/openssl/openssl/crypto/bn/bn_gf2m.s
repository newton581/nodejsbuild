	.file	"bn_gf2m.c"
	.text
	.p2align 4
	.globl	BN_GF2m_add
	.type	BN_GF2m_add, @function
BN_GF2m_add:
.LFB252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movl	8(%rsi), %esi
	movl	8(%rdx), %eax
	cmpl	%eax, %esi
	jge	.L2
	movl	%eax, %esi
	movq	%rbx, %rax
	movq	%rdx, %rbx
	movq	%rax, %r13
.L2:
	movq	%r12, %rdi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L17
	movl	8(%r13), %eax
	testl	%eax, %eax
	jle	.L18
	movq	(%rbx), %rcx
	movq	(%r12), %rsi
	leal	-1(%rax), %edx
	movq	0(%r13), %rdi
	leaq	15(%rcx), %r8
	subq	%rsi, %r8
	cmpq	$30, %r8
	leaq	15(%rdi), %r8
	seta	%r9b
	subq	%rsi, %r8
	cmpq	$30, %r8
	seta	%r8b
	testb	%r8b, %r9b
	je	.L5
	cmpl	$2, %edx
	jbe	.L5
	movl	%eax, %r8d
	xorl	%edx, %edx
	shrl	%r8d
	salq	$4, %r8
	.p2align 4,,10
	.p2align 3
.L6:
	movdqu	(%rcx,%rdx), %xmm0
	movdqu	(%rdi,%rdx), %xmm1
	pxor	%xmm1, %xmm0
	movups	%xmm0, (%rsi,%rdx)
	addq	$16, %rdx
	cmpq	%r8, %rdx
	jne	.L6
	movl	%eax, %edx
	andl	$-2, %edx
	testb	$1, %al
	je	.L4
	movq	(%rcx,%rdx,8), %rcx
	xorq	(%rdi,%rdx,8), %rcx
	movq	%rcx, (%rsi,%rdx,8)
.L4:
	movl	8(%rbx), %r9d
	cmpl	%r9d, %eax
	jge	.L14
	movq	(%rbx), %rdi
	movq	(%r12), %rsi
	movslq	%eax, %rdx
	leaq	16(,%rdx,8), %r8
	leaq	0(,%rdx,8), %rcx
	leaq	(%rdi,%r8), %r11
	leaq	(%rsi,%rcx), %r10
	addq	%rdi, %rcx
	cmpq	%r11, %r10
	setnb	%r11b
	addq	%rsi, %r8
	cmpq	%r8, %rcx
	setnb	%r8b
	orb	%r8b, %r11b
	je	.L25
	leal	-1(%r9), %r8d
	subl	%eax, %r8d
	cmpl	$3, %r8d
	jbe	.L25
	movl	%r9d, %r11d
	xorl	%edx, %edx
	subl	%eax, %r11d
	movl	%r11d, %r8d
	shrl	%r8d
	salq	$4, %r8
	.p2align 4,,10
	.p2align 3
.L12:
	movdqu	(%rcx,%rdx), %xmm2
	movups	%xmm2, (%r10,%rdx)
	addq	$16, %rdx
	cmpq	%r8, %rdx
	jne	.L12
	movl	%r11d, %edx
	andl	$-2, %edx
	addl	%edx, %eax
	cmpl	%edx, %r11d
	je	.L14
	cltq
	movq	(%rdi,%rax,8), %rdx
	movq	%rdx, (%rsi,%rax,8)
.L14:
	movl	%r9d, 8(%r12)
	movq	%r12, %rdi
	call	bn_correct_top@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	movq	(%rdi,%rdx,8), %rax
	movq	%rax, (%rsi,%rdx,8)
	addq	$1, %rdx
	cmpl	%edx, %r9d
	jg	.L25
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L17:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	movl	%edx, %r9d
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L8:
	movq	(%rcx,%rdx,8), %r8
	xorq	(%rdi,%rdx,8), %r8
	movq	%r8, (%rsi,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%r9, %r8
	jne	.L8
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L18:
	xorl	%eax, %eax
	jmp	.L4
	.cfi_endproc
.LFE252:
	.size	BN_GF2m_add, .-BN_GF2m_add
	.p2align 4
	.globl	BN_GF2m_mod_arr
	.type	BN_GF2m_mod_arr, @function
BN_GF2m_mod_arr:
.LFB253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %r10d
	movq	%rdi, -56(%rbp)
	testl	%r10d, %r10d
	je	.L97
	movq	%rsi, %r12
	movq	%rdx, %rbx
	cmpq	%rdi, %rsi
	je	.L98
	movl	8(%rsi), %esi
	movq	%rdi, %r14
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L68
	movl	8(%r12), %ecx
	movq	(%r14), %rdi
	testl	%ecx, %ecx
	jle	.L99
	movq	(%r12), %rdx
	leal	-1(%rcx), %r9d
	leaq	15(%rdx), %rax
	subq	%rdi, %rax
	cmpq	$30, %rax
	jbe	.L42
	cmpl	$3, %r9d
	jbe	.L42
	movl	%ecx, %esi
	xorl	%eax, %eax
	shrl	%esi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L43:
	movdqu	(%rdx,%rax), %xmm0
	movups	%xmm0, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rax, %rsi
	jne	.L43
	movl	%ecx, %eax
	andl	$-2, %eax
	testb	$1, %cl
	je	.L45
	movq	(%rdx,%rax,8), %rdx
	movq	%rdx, (%rdi,%rax,8)
.L45:
	movq	-56(%rbp), %rax
	movl	%ecx, 8(%rax)
	movl	(%rbx), %r10d
	jmp	.L40
.L98:
	movl	8(%rdi), %ecx
	movq	(%rdi), %rdi
	leal	-1(%rcx), %r9d
.L40:
	testl	%r10d, %r10d
	leal	63(%r10), %r11d
	cmovns	%r10d, %r11d
	sarl	$6, %r11d
	cmpl	%r9d, %r11d
	jge	.L49
	movl	%r10d, %eax
	movl	%r10d, %r14d
	movl	$64, %r12d
	sarl	$31, %eax
	andl	$63, %r14d
	shrl	$26, %eax
	leal	(%r10,%rax), %r13d
	andl	$63, %r13d
	subl	%eax, %r13d
	movl	$64, %eax
	subl	%r13d, %eax
	movl	%eax, -60(%rbp)
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L100:
	subl	$1, %r9d
.L51:
	cmpl	%r11d, %r9d
	jle	.L49
.L58:
	movslq	%r9d, %rax
	leaq	(%rdi,%rax,8), %rax
	movq	(%rax), %r8
	testq	%r8, %r8
	je	.L100
	movq	$0, (%rax)
	movl	4(%rbx), %eax
	leaq	8(%rbx), %rsi
	testl	%eax, %eax
	je	.L56
	.p2align 4,,10
	.p2align 3
.L57:
	movl	%r10d, %edx
	movl	%r9d, %r15d
	subl	%eax, %edx
	movl	%edx, %eax
	sarl	$31, %eax
	shrl	$26, %eax
	leal	(%rdx,%rax), %ecx
	andl	$63, %ecx
	subl	%eax, %ecx
	testl	%edx, %edx
	leal	63(%rdx), %eax
	cmovns	%edx, %eax
	sarl	$6, %eax
	subl	%eax, %r15d
	movslq	%r15d, %rax
	movq	%r8, %r15
	salq	$3, %rax
	shrq	%cl, %r15
	xorq	%r15, (%rdi,%rax)
	andl	$63, %edx
	je	.L54
	movl	%r12d, %edx
	addq	$4, %rsi
	subl	%ecx, %edx
	movl	%edx, %ecx
	movq	%r8, %rdx
	salq	%cl, %rdx
	xorq	%rdx, -8(%rdi,%rax)
	movl	-4(%rsi), %eax
	testl	%eax, %eax
	jne	.L57
.L56:
	movl	%r9d, %eax
	movq	%r8, %rdx
	movl	%r13d, %ecx
	subl	%r11d, %eax
	shrq	%cl, %rdx
	cltq
	salq	$3, %rax
	xorq	%rdx, (%rdi,%rax)
	testl	%r14d, %r14d
	je	.L51
	movzbl	-60(%rbp), %ecx
	salq	%cl, %r8
	xorq	%r8, -8(%rdi,%rax)
	cmpl	%r11d, %r9d
	jg	.L58
	.p2align 4,,10
	.p2align 3
.L49:
	cmpl	%r9d, %r11d
	jne	.L59
	movl	%r10d, %eax
	movslq	%r11d, %r11
	movl	$64, %edx
	sarl	$31, %eax
	leaq	(%rdi,%r11,8), %r12
	shrl	$26, %eax
	leal	(%r10,%rax), %r14d
	andl	$63, %r10d
	andl	$63, %r14d
	subl	%eax, %r14d
	movq	(%r12), %rax
	movl	%r14d, %ecx
	subl	%r14d, %edx
	movq	%rax, %r9
	shrq	%cl, %r9
	testq	%r9, %r9
	je	.L59
	movl	4(%rbx), %r13d
	movl	$64, %r11d
	.p2align 4,,10
	.p2align 3
.L64:
	movl	%edx, %ecx
	movl	$0, %esi
	leaq	8(%rbx), %r8
	salq	%cl, %rax
	shrq	%cl, %rax
	testl	%r10d, %r10d
	cmove	%rsi, %rax
	movl	%r13d, %esi
	movq	%rax, (%r12)
	xorq	%r9, (%rdi)
	testl	%r13d, %r13d
	je	.L67
	.p2align 4,,10
	.p2align 3
.L66:
	movl	%esi, %eax
	movq	%r9, %r15
	sarl	$31, %eax
	shrl	$26, %eax
	leal	(%rsi,%rax), %ecx
	andl	$63, %ecx
	subl	%eax, %ecx
	testl	%esi, %esi
	leal	63(%rsi), %eax
	cmovns	%esi, %eax
	salq	%cl, %r15
	sarl	$6, %eax
	cltq
	salq	$3, %rax
	xorq	%r15, (%rdi,%rax)
	andl	$63, %esi
	je	.L65
	movl	%r11d, %esi
	subl	%ecx, %esi
	movl	%esi, %ecx
	movq	%r9, %rsi
	shrq	%cl, %rsi
	testq	%rsi, %rsi
	je	.L65
	xorq	%rsi, 8(%rdi,%rax)
.L65:
	movl	(%r8), %esi
	addq	$4, %r8
	testl	%esi, %esi
	jne	.L66
.L67:
	movq	(%r12), %rax
	movl	%r14d, %ecx
	movq	%rax, %r9
	shrq	%cl, %r9
	testq	%r9, %r9
	jne	.L64
.L59:
	movq	-56(%rbp), %rdi
	call	bn_correct_top@PLT
	movl	$1, %eax
.L36:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	movl	(%rsi), %eax
	addq	$4, %rsi
	testl	%eax, %eax
	jne	.L57
	jmp	.L56
.L97:
	xorl	%esi, %esi
	call	BN_set_word@PLT
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L68:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L36
.L42:
	movl	%r9d, %r8d
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L47:
	movq	(%rdx,%rax,8), %rsi
	movq	%rsi, (%rdi,%rax,8)
	movq	%rax, %rsi
	addq	$1, %rax
	cmpq	%rsi, %r8
	jne	.L47
	jmp	.L45
.L99:
	leal	-1(%rcx), %r9d
	jmp	.L45
	.cfi_endproc
.LFE253:
	.size	BN_GF2m_mod_arr, .-BN_GF2m_mod_arr
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/bn/bn_gf2m.c"
	.text
	.p2align 4
	.globl	BN_GF2m_mod
	.type	BN_GF2m_mod, @function
BN_GF2m_mod:
.LFB254:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L112
	movl	8(%rbx), %edx
	subl	$1, %edx
	js	.L114
	movslq	%edx, %rsi
	sall	$6, %edx
	movq	(%rbx), %r10
	subl	$1, %edx
	jmp	.L109
.L105:
	subq	$1, %rsi
	subl	$64, %edx
	testl	%esi, %esi
	js	.L124
	.p2align 4,,10
	.p2align 3
.L109:
	movq	(%r10,%rsi,8), %r8
	testq	%r8, %r8
	je	.L105
	movabsq	$-9223372036854775808, %rdi
	leal	64(%rdx), %ecx
	.p2align 4,,10
	.p2align 3
.L108:
	testq	%rdi, %r8
	je	.L106
	cmpl	$5, %eax
	jg	.L107
	movslq	%eax, %r9
	movl	%ecx, -64(%rbp,%r9,4)
.L107:
	addl	$1, %eax
.L106:
	subl	$1, %ecx
	shrq	%rdi
	cmpl	%ecx, %edx
	jne	.L108
	subq	$1, %rsi
	subl	$64, %edx
	testl	%esi, %esi
	jns	.L109
.L124:
	cmpl	$5, %eax
	jle	.L125
	cmpl	$6, %eax
	jg	.L112
	leaq	-64(%rbp), %rdx
.L111:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	BN_GF2m_mod_arr
.L101:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L126
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_restore_state
	leaq	-64(%rbp), %rdx
	cltq
	leaq	(%rdx,%rax,4), %rax
.L104:
	movl	$-1, (%rax)
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L112:
	movl	$398, %r8d
	movl	$106, %edx
	movl	$131, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L114:
	leaq	-64(%rbp), %rdx
	movq	%rdx, %rax
	jmp	.L104
.L126:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE254:
	.size	BN_GF2m_mod, .-BN_GF2m_mod
	.p2align 4
	.globl	BN_GF2m_mod_sqr_arr
	.type	BN_GF2m_mod_sqr_arr, @function
BN_GF2m_mod_sqr_arr:
.LFB257:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%rdi, -80(%rbp)
	movq	%rcx, %rdi
	movq	%rdx, -88(%rbp)
	movq	%rcx, -72(%rbp)
	call	BN_CTX_start@PLT
	movq	%r15, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L130
	movq	%rax, %r15
	movl	8(%rbx), %eax
	movq	%r15, %rdi
	leal	(%rax,%rax), %esi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L130
	movl	8(%rbx), %edx
	movl	%edx, %eax
	movl	%edx, -52(%rbp)
	subl	$1, %eax
	js	.L131
	leal	(%rdx,%rdx), %r11d
	movl	%edx, %r10d
	movq	(%r15), %rcx
	movq	(%rbx), %rsi
	movslq	%r11d, %r8
	subl	$2, %r11d
	negq	%r10
	movslq	%eax, %rdi
	salq	$3, %r8
	movslq	%r11d, %r11
	movq	%r10, %r15
	leaq	-24(%r8), %r9
	salq	$4, %r15
	leaq	-16(%r8), %rdx
	leaq	8(,%r11,8), %r11
	leaq	0(,%rdi,8), %r13
	cmpq	%r11, %r9
	leaq	-32(%r8), %r9
	leaq	8(%r13,%r10,8), %r10
	setge	%bl
	leaq	8(%r8,%r15), %r12
	leaq	8(%rsi,%r13), %r14
	cmpq	%r8, %r9
	setge	%r9b
	orl	%ebx, %r9d
	cmpl	$1, %eax
	seta	%bl
	addq	%rsi, %r10
	andl	%ebx, %r9d
	leaq	(%rcx,%r8), %rbx
	cmpq	%rbx, %r10
	setnb	%bl
	addq	%rcx, %r12
	cmpq	%r12, %r14
	setbe	%r12b
	orl	%r12d, %ebx
	testb	%bl, %r9b
	je	.L132
	addq	%rcx, %r11
	cmpq	%r11, %r10
	leaq	16(%rdx,%r15), %r10
	setnb	%r9b
	addq	%rcx, %r10
	cmpq	%r10, %r14
	setbe	%r10b
	orb	%r10b, %r9b
	je	.L132
	movl	-52(%rbp), %r10d
	movdqa	.LC1(%rip), %xmm5
	leaq	-8(%rsi,%r13), %r9
	leaq	-8(%rcx,%r8), %r8
	movdqa	.LC2(%rip), %xmm4
	movdqa	.LC3(%rip), %xmm3
	addq	%rcx, %rdx
	xorl	%edi, %edi
	shrl	%r10d
	movdqa	.LC4(%rip), %xmm2
	negq	%r10
	salq	$4, %r10
	.p2align 4,,10
	.p2align 3
.L133:
	movdqu	(%r9,%rdi), %xmm1
	shufpd	$1, %xmm1, %xmm1
	movdqa	%xmm1, %xmm13
	movdqa	%xmm1, %xmm15
	movdqa	%xmm1, %xmm11
	psrlq	$60, %xmm13
	psrlq	$56, %xmm15
	movdqa	%xmm1, %xmm6
	movdqa	%xmm13, %xmm10
	movdqa	%xmm13, %xmm0
	movdqa	%xmm15, %xmm12
	psllq	$3, %xmm10
	psllq	$2, %xmm0
	movdqa	%xmm1, %xmm7
	pand	%xmm4, %xmm0
	pand	%xmm5, %xmm10
	movdqa	%xmm1, %xmm9
	por	%xmm0, %xmm10
	movdqa	%xmm13, %xmm0
	pand	%xmm2, %xmm13
	psllq	$1, %xmm0
	psllq	$3, %xmm12
	movdqa	%xmm1, %xmm8
	pand	%xmm3, %xmm0
	psrlq	$52, %xmm11
	pand	%xmm5, %xmm12
	por	%xmm0, %xmm13
	movdqa	%xmm15, %xmm0
	movdqa	%xmm12, %xmm14
	psllq	$2, %xmm0
	movdqa	%xmm15, %xmm12
	pand	%xmm2, %xmm15
	pand	%xmm4, %xmm0
	psllq	$1, %xmm12
	por	%xmm13, %xmm10
	psllq	$56, %xmm10
	psrlq	$32, %xmm1
	por	%xmm0, %xmm14
	psrlq	$48, %xmm6
	psrlq	$44, %xmm7
	movdqa	%xmm12, %xmm0
	pand	%xmm3, %xmm0
	movdqa	%xmm15, %xmm12
	por	%xmm0, %xmm12
	movdqa	%xmm11, %xmm0
	psllq	$3, %xmm0
	por	%xmm14, %xmm12
	psllq	$48, %xmm12
	psrlq	$40, %xmm9
	pand	%xmm5, %xmm0
	por	%xmm12, %xmm10
	psrlq	$36, %xmm8
	movdqa	%xmm11, %xmm12
	psllq	$2, %xmm12
	pand	%xmm4, %xmm12
	por	%xmm12, %xmm0
	movdqa	%xmm11, %xmm12
	pand	%xmm2, %xmm11
	psllq	$1, %xmm12
	pand	%xmm3, %xmm12
	por	%xmm12, %xmm11
	por	%xmm11, %xmm0
	movdqa	%xmm1, %xmm11
	psllq	$40, %xmm0
	psllq	$2, %xmm11
	por	%xmm10, %xmm0
	movdqa	%xmm1, %xmm10
	pand	%xmm4, %xmm11
	psllq	$3, %xmm10
	pand	%xmm5, %xmm10
	por	%xmm11, %xmm10
	movdqa	%xmm1, %xmm11
	pand	%xmm2, %xmm1
	psllq	$1, %xmm11
	pand	%xmm3, %xmm11
	por	%xmm11, %xmm1
	por	%xmm10, %xmm1
	movdqa	%xmm6, %xmm10
	por	%xmm0, %xmm1
	psllq	$2, %xmm10
	movdqa	%xmm6, %xmm0
	psllq	$3, %xmm0
	pand	%xmm4, %xmm10
	pand	%xmm5, %xmm0
	por	%xmm10, %xmm0
	movdqa	%xmm6, %xmm10
	pand	%xmm2, %xmm6
	psllq	$1, %xmm10
	pand	%xmm3, %xmm10
	por	%xmm10, %xmm6
	por	%xmm6, %xmm0
	movdqa	%xmm7, %xmm6
	psllq	$32, %xmm0
	psllq	$3, %xmm6
	movdqa	%xmm0, %xmm10
	movdqa	%xmm7, %xmm0
	pand	%xmm5, %xmm6
	psllq	$2, %xmm0
	pand	%xmm4, %xmm0
	por	%xmm0, %xmm6
	movdqa	%xmm7, %xmm0
	pand	%xmm2, %xmm7
	psllq	$1, %xmm0
	pand	%xmm3, %xmm0
	por	%xmm7, %xmm0
	movdqa	%xmm9, %xmm7
	por	%xmm6, %xmm0
	psllq	$3, %xmm7
	movdqa	%xmm9, %xmm6
	psllq	$2, %xmm6
	psllq	$24, %xmm0
	pand	%xmm5, %xmm7
	pand	%xmm4, %xmm6
	por	%xmm10, %xmm0
	por	%xmm6, %xmm7
	movdqa	%xmm9, %xmm6
	pand	%xmm2, %xmm9
	psllq	$1, %xmm6
	pand	%xmm3, %xmm6
	por	%xmm6, %xmm9
	movdqa	%xmm8, %xmm6
	por	%xmm9, %xmm7
	psllq	$3, %xmm6
	movdqa	%xmm8, %xmm9
	psllq	$16, %xmm7
	psllq	$1, %xmm9
	pand	%xmm5, %xmm6
	por	%xmm7, %xmm0
	pand	%xmm3, %xmm9
	por	%xmm1, %xmm0
	movdqa	%xmm8, %xmm1
	pand	%xmm2, %xmm8
	psllq	$2, %xmm1
	por	%xmm9, %xmm8
	pand	%xmm4, %xmm1
	por	%xmm1, %xmm6
	por	%xmm8, %xmm6
	psllq	$8, %xmm6
	por	%xmm0, %xmm6
	movq	%xmm6, (%r8,%rdi,2)
	movhps	%xmm6, -16(%r8,%rdi,2)
	movdqu	(%r9,%rdi), %xmm11
	shufpd	$1, %xmm11, %xmm11
	movdqa	%xmm11, %xmm0
	movdqa	%xmm11, %xmm13
	movdqa	%xmm11, %xmm12
	psrlq	$28, %xmm0
	psrlq	$24, %xmm13
	movdqa	%xmm11, %xmm10
	movdqa	%xmm0, %xmm6
	movdqa	%xmm0, %xmm1
	movdqa	%xmm0, %xmm14
	psllq	$3, %xmm6
	psllq	$2, %xmm1
	pand	%xmm2, %xmm0
	psllq	$1, %xmm14
	pand	%xmm4, %xmm1
	pand	%xmm5, %xmm6
	pand	%xmm3, %xmm14
	por	%xmm1, %xmm6
	movdqa	%xmm13, %xmm1
	por	%xmm14, %xmm0
	movdqa	%xmm13, %xmm14
	movdqa	%xmm11, %xmm7
	por	%xmm0, %xmm6
	movdqa	%xmm13, %xmm0
	pand	%xmm2, %xmm13
	psllq	$3, %xmm14
	psllq	$2, %xmm0
	movdqa	%xmm11, %xmm9
	psllq	$1, %xmm1
	psrlq	$20, %xmm12
	pand	%xmm4, %xmm0
	psllq	$56, %xmm6
	pand	%xmm3, %xmm1
	pand	%xmm5, %xmm14
	por	%xmm0, %xmm14
	por	%xmm1, %xmm13
	movdqa	%xmm12, %xmm0
	movdqa	%xmm6, %xmm1
	movdqa	%xmm12, %xmm6
	por	%xmm14, %xmm13
	psllq	$3, %xmm6
	psllq	$2, %xmm0
	movdqa	%xmm11, %xmm8
	pand	%xmm4, %xmm0
	psllq	$48, %xmm13
	pand	%xmm5, %xmm6
	por	%xmm0, %xmm6
	movdqa	%xmm12, %xmm0
	pand	%xmm2, %xmm12
	psllq	$1, %xmm0
	psrlq	$16, %xmm10
	por	%xmm13, %xmm1
	psrlq	$12, %xmm7
	psrlq	$8, %xmm9
	pand	%xmm3, %xmm0
	por	%xmm0, %xmm12
	psrlq	$4, %xmm8
	por	%xmm12, %xmm6
	movdqa	%xmm11, %xmm12
	psllq	$40, %xmm6
	psllq	$3, %xmm12
	por	%xmm1, %xmm6
	movdqa	%xmm11, %xmm1
	pand	%xmm5, %xmm12
	psllq	$2, %xmm1
	pand	%xmm4, %xmm1
	por	%xmm1, %xmm12
	movdqa	%xmm11, %xmm1
	psllq	$1, %xmm1
	pand	%xmm3, %xmm1
	movdqa	%xmm1, %xmm0
	movdqa	%xmm11, %xmm1
	pand	%xmm2, %xmm1
	por	%xmm0, %xmm1
	movdqa	%xmm10, %xmm0
	por	%xmm12, %xmm1
	psllq	$3, %xmm0
	por	%xmm6, %xmm1
	movdqa	%xmm10, %xmm6
	pand	%xmm5, %xmm0
	psllq	$2, %xmm6
	pand	%xmm4, %xmm6
	por	%xmm6, %xmm0
	movdqa	%xmm10, %xmm6
	pand	%xmm2, %xmm10
	psllq	$1, %xmm6
	pand	%xmm3, %xmm6
	por	%xmm6, %xmm10
	movdqa	%xmm7, %xmm6
	por	%xmm10, %xmm0
	psllq	$3, %xmm6
	psllq	$32, %xmm0
	pand	%xmm5, %xmm6
	movdqa	%xmm0, %xmm10
	movdqa	%xmm7, %xmm0
	psllq	$2, %xmm0
	pand	%xmm4, %xmm0
	por	%xmm0, %xmm6
	movdqa	%xmm7, %xmm0
	pand	%xmm2, %xmm7
	psllq	$1, %xmm0
	pand	%xmm3, %xmm0
	por	%xmm7, %xmm0
	movdqa	%xmm9, %xmm7
	por	%xmm6, %xmm0
	psllq	$3, %xmm7
	movdqa	%xmm9, %xmm6
	psllq	$2, %xmm6
	psllq	$24, %xmm0
	pand	%xmm5, %xmm7
	pand	%xmm4, %xmm6
	por	%xmm10, %xmm0
	por	%xmm6, %xmm7
	movdqa	%xmm9, %xmm6
	pand	%xmm2, %xmm9
	psllq	$1, %xmm6
	pand	%xmm3, %xmm6
	por	%xmm6, %xmm9
	movdqa	%xmm0, %xmm6
	movdqa	%xmm8, %xmm0
	por	%xmm9, %xmm7
	psllq	$2, %xmm0
	movdqa	%xmm8, %xmm9
	psllq	$16, %xmm7
	psllq	$1, %xmm9
	pand	%xmm4, %xmm0
	por	%xmm7, %xmm6
	pand	%xmm3, %xmm9
	por	%xmm6, %xmm1
	movdqa	%xmm8, %xmm6
	pand	%xmm2, %xmm8
	psllq	$3, %xmm6
	por	%xmm9, %xmm8
	pand	%xmm5, %xmm6
	por	%xmm0, %xmm6
	por	%xmm8, %xmm6
	psllq	$8, %xmm6
	por	%xmm1, %xmm6
	movq	%xmm6, (%rdx,%rdi,2)
	movhps	%xmm6, -16(%rdx,%rdi,2)
	subq	$16, %rdi
	cmpq	%r10, %rdi
	jne	.L133
	movl	-52(%rbp), %edi
	movl	%edi, %edx
	andl	$-2, %edx
	subl	%edx, %eax
	cmpl	%edx, %edi
	je	.L131
	movslq	%eax, %rdx
	addl	%eax, %eax
	leaq	(%rsi,%rdx,8), %r13
	cltq
	movq	0(%r13), %rdx
	movq	%rdx, %rbx
	movq	%rdx, %r11
	movq	%rdx, %r10
	movq	%rdx, %r9
	shrq	$60, %rbx
	movq	%rdx, %r8
	movq	%rdx, %rdi
	movq	%rdx, %rsi
	leaq	0(,%rbx,8), %r14
	shrq	$32, %rdx
	shrq	$56, %r11
	shrq	$52, %r10
	andl	$64, %r14d
	shrq	$48, %r9
	shrq	$44, %r8
	movq	%r14, %r12
	leaq	0(,%rbx,4), %r14
	shrq	$40, %rdi
	andl	$16, %r14d
	shrq	$36, %rsi
	orq	%r12, %r14
	movq	%rbx, %r12
	addq	%rbx, %rbx
	andl	$1, %r12d
	andl	$4, %ebx
	orq	%r14, %r12
	leaq	0(,%rdx,8), %r14
	andl	$64, %r14d
	orq	%r12, %rbx
	movq	%r14, %r12
	leaq	0(,%rdx,4), %r14
	salq	$56, %rbx
	andl	$16, %r14d
	orq	%r12, %r14
	movq	%rdx, %r12
	andl	$1, %r12d
	orq	%r14, %r12
	leaq	(%rdx,%rdx), %r14
	leaq	0(,%r11,8), %rdx
	andl	$64, %edx
	andl	$4, %r14d
	movq	%rdx, %r15
	leaq	0(,%r11,4), %rdx
	orq	%r14, %r12
	andl	$16, %edx
	orq	%r12, %rbx
	leaq	0(,%r8,8), %r12
	orq	%r15, %rdx
	movq	%r11, %r15
	andl	$1, %r15d
	orq	%rdx, %r15
	leaq	(%r11,%r11), %rdx
	andl	$4, %edx
	orq	%r15, %rdx
	andl	$64, %r12d
	salq	$48, %rdx
	orq	%rdx, %rbx
	leaq	0(,%r10,8), %rdx
	andl	$64, %edx
	movq	%rdx, %r11
	leaq	0(,%r10,4), %rdx
	andl	$16, %edx
	orq	%r11, %rdx
	movq	%r10, %r11
	andl	$1, %r11d
	orq	%rdx, %r11
	leaq	(%r10,%r10), %rdx
	andl	$4, %edx
	orq	%r11, %rdx
	salq	$40, %rdx
	orq	%rdx, %rbx
	leaq	0(,%r9,8), %rdx
	andl	$64, %edx
	movq	%rdx, %r10
	leaq	0(,%r9,4), %rdx
	andl	$16, %edx
	orq	%r10, %rdx
	movq	%r9, %r10
	addq	%r9, %r9
	andl	$1, %r10d
	andl	$4, %r9d
	orq	%rdx, %r10
	leaq	0(,%r8,4), %rdx
	andl	$16, %edx
	orq	%r9, %r10
	leaq	0(,%rdi,8), %r9
	orq	%rdx, %r12
	movq	%r8, %rdx
	addq	%r8, %r8
	salq	$32, %r10
	andl	$1, %edx
	andl	$4, %r8d
	orq	%r10, %rbx
	orq	%rdx, %r12
	leaq	0(,%rdi,4), %rdx
	orq	%r8, %r12
	salq	$24, %r12
	orq	%r12, %rbx
	andl	$64, %r9d
	andl	$16, %edx
	orq	%rdx, %r9
	movq	%rdi, %rdx
	addq	%rdi, %rdi
	andl	$1, %edx
	andl	$4, %edi
	orq	%rdx, %r9
	leaq	0(,%rsi,8), %rdx
	orq	%r9, %rdi
	andl	$64, %edx
	salq	$16, %rdi
	orq	%rdi, %rbx
	leaq	0(,%rsi,4), %rdi
	andl	$16, %edi
	orq	%rdi, %rdx
	movq	%rsi, %rdi
	addq	%rsi, %rsi
	andl	$1, %edi
	andl	$4, %esi
	orq	%rdi, %rdx
	orq	%rdx, %rsi
	salq	$8, %rsi
	orq	%rsi, %rbx
	movq	%rbx, 8(%rcx,%rax,8)
	movq	0(%r13), %rdx
	movq	%rdx, %rbx
	movq	%rdx, %r11
	movq	%rdx, %r10
	movq	%rdx, %r9
	shrq	$28, %rbx
	movq	%rdx, %r8
	movq	%rdx, %rdi
	movq	%rdx, %rsi
	leaq	0(,%rbx,8), %r13
	shrq	$24, %r11
	shrq	$20, %r10
	shrq	$16, %r9
	andl	$64, %r13d
	shrq	$12, %r8
	shrq	$8, %rdi
	movq	%r13, %r12
	leaq	0(,%rbx,4), %r13
	shrq	$4, %rsi
	andl	$16, %r13d
	orq	%r12, %r13
	movq	%rbx, %r12
	andl	$1, %r12d
	orq	%r13, %r12
	addq	%rbx, %rbx
	leaq	0(,%rdx,8), %r13
	andl	$64, %r13d
	andl	$4, %ebx
	orq	%r12, %rbx
	movq	%r13, %r12
	leaq	0(,%rdx,4), %r13
	andl	$16, %r13d
	salq	$56, %rbx
	orq	%r12, %r13
	movq	%rdx, %r12
	andl	$1, %r12d
	orq	%r13, %r12
	leaq	(%rdx,%rdx), %r13
	leaq	0(,%r11,8), %rdx
	andl	$64, %edx
	andl	$4, %r13d
	movq	%rdx, %r14
	leaq	0(,%r11,4), %rdx
	orq	%r13, %r12
	andl	$16, %edx
	orq	%r12, %rbx
	orq	%r14, %rdx
	movq	%r11, %r14
	andl	$1, %r14d
	orq	%rdx, %r14
	leaq	(%r11,%r11), %rdx
	leaq	0(,%r9,8), %r11
	andl	$4, %edx
	andl	$64, %r11d
	orq	%r14, %rdx
	salq	$48, %rdx
	orq	%rdx, %rbx
	leaq	0(,%r10,8), %rdx
	andl	$64, %edx
	movq	%rdx, %r15
	leaq	0(,%r10,4), %rdx
	andl	$16, %edx
	orq	%r15, %rdx
	movq	%r10, %r15
	addq	%r10, %r10
	andl	$1, %r15d
	andl	$4, %r10d
	orq	%rdx, %r15
	leaq	0(,%r9,4), %rdx
	orq	%r10, %r15
	leaq	0(,%r8,8), %r10
	salq	$40, %r15
	orq	%r15, %rbx
	andl	$16, %edx
	andl	$64, %r10d
	orq	%rdx, %r11
	movq	%r9, %rdx
	addq	%r9, %r9
	andl	$1, %edx
	andl	$4, %r9d
	orq	%rdx, %r11
	leaq	0(,%r8,4), %rdx
	andl	$16, %edx
	orq	%r9, %r11
	leaq	0(,%rdi,8), %r9
	orq	%rdx, %r10
	movq	%r8, %rdx
	andl	$64, %r9d
	addq	%r8, %r8
	andl	$1, %edx
	andl	$4, %r8d
	salq	$32, %r11
	orq	%rdx, %r10
	leaq	0(,%rdi,4), %rdx
	orq	%r11, %rbx
	andl	$16, %edx
	orq	%r10, %r8
	orq	%rdx, %r9
	movq	%rdi, %rdx
	addq	%rdi, %rdi
	salq	$24, %r8
	andl	$1, %edx
	andl	$4, %edi
	orq	%r8, %rbx
	orq	%rdx, %r9
	leaq	0(,%rsi,8), %rdx
	orq	%r9, %rdi
	andl	$64, %edx
	salq	$16, %rdi
	orq	%rdi, %rbx
	leaq	0(,%rsi,4), %rdi
	andl	$16, %edi
	orq	%rdi, %rdx
	movq	%rsi, %rdi
	andl	$1, %edi
	addq	%rsi, %rsi
	orq	%rdi, %rdx
	andl	$4, %esi
	orq	%rdx, %rsi
	salq	$8, %rsi
	orq	%rsi, %rbx
	movq	%rbx, (%rcx,%rax,8)
.L131:
	movl	-52(%rbp), %eax
	movq	-64(%rbp), %rbx
	xorl	%r12d, %r12d
	addl	%eax, %eax
	movq	%rbx, %rdi
	movl	%eax, 8(%rbx)
	call	bn_correct_top@PLT
	movq	-88(%rbp), %rdx
	movq	-80(%rbp), %rdi
	movq	%rbx, %rsi
	call	BN_GF2m_mod_arr
	testl	%eax, %eax
	setne	%r12b
.L129:
	movq	-72(%rbp), %rdi
	call	BN_CTX_end@PLT
	addq	$56, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L132:
	addq	%rcx, %rdx
	.p2align 4,,10
	.p2align 3
.L135:
	movq	(%rsi,%rdi,8), %rcx
	movq	%rcx, %r13
	movq	%rcx, %r12
	movq	%rcx, %rbx
	movq	%rcx, %r11
	shrq	$60, %r13
	movq	%rcx, %r10
	movq	%rcx, %r9
	movq	%rcx, %r8
	leaq	0(,%r13,4), %rax
	leaq	0(,%r13,8), %r14
	shrq	$32, %rcx
	shrq	$56, %r12
	shrq	$52, %rbx
	andl	$64, %r14d
	andl	$16, %eax
	shrq	$48, %r11
	shrq	$44, %r10
	orq	%r14, %rax
	movq	%r13, %r14
	addq	%r13, %r13
	shrq	$40, %r9
	andl	$1, %r14d
	andl	$4, %r13d
	shrq	$36, %r8
	orq	%r14, %rax
	leaq	0(,%rcx,8), %r14
	orq	%r13, %rax
	leaq	0(,%rcx,4), %r13
	andl	$64, %r14d
	andl	$16, %r13d
	salq	$56, %rax
	orq	%r14, %r13
	movq	%rcx, %r14
	addq	%rcx, %rcx
	andl	$1, %r14d
	andl	$4, %ecx
	orq	%r14, %r13
	orq	%r13, %rcx
	leaq	0(,%r12,8), %r13
	orq	%rax, %rcx
	leaq	0(,%r12,4), %rax
	andl	$64, %r13d
	andl	$16, %eax
	orq	%r13, %rax
	movq	%r12, %r13
	addq	%r12, %r12
	andl	$1, %r13d
	orq	%r13, %rax
	andl	$4, %r12d
	orq	%r12, %rax
	leaq	0(,%rbx,8), %r12
	salq	$48, %rax
	andl	$64, %r12d
	orq	%rax, %rcx
	leaq	0(,%rbx,4), %rax
	andl	$16, %eax
	orq	%r12, %rax
	movq	%rbx, %r12
	addq	%rbx, %rbx
	andl	$1, %r12d
	andl	$4, %ebx
	orq	%r12, %rax
	orq	%rbx, %rax
	leaq	0(,%r11,8), %rbx
	salq	$40, %rax
	andl	$64, %ebx
	orq	%rcx, %rax
	leaq	0(,%r11,4), %rcx
	andl	$16, %ecx
	orq	%rbx, %rcx
	movq	%r11, %rbx
	addq	%r11, %r11
	andl	$1, %ebx
	andl	$4, %r11d
	orq	%rbx, %rcx
	orq	%r11, %rcx
	leaq	0(,%r10,8), %r11
	salq	$32, %rcx
	andl	$64, %r11d
	orq	%rax, %rcx
	leaq	0(,%r10,4), %rax
	andl	$16, %eax
	orq	%r11, %rax
	movq	%r10, %r11
	addq	%r10, %r10
	andl	$1, %r11d
	andl	$4, %r10d
	orq	%r11, %rax
	orq	%r10, %rax
	leaq	0(,%r9,8), %r10
	salq	$24, %rax
	andl	$64, %r10d
	orq	%rcx, %rax
	leaq	0(,%r9,4), %rcx
	andl	$16, %ecx
	orq	%r10, %rcx
	movq	%r9, %r10
	addq	%r9, %r9
	andl	$1, %r10d
	andl	$4, %r9d
	orq	%r10, %rcx
	orq	%r9, %rcx
	leaq	0(,%r8,8), %r9
	salq	$16, %rcx
	andl	$64, %r9d
	orq	%rcx, %rax
	leaq	0(,%r8,4), %rcx
	andl	$16, %ecx
	orq	%r9, %rcx
	movq	%r8, %r9
	addq	%r8, %r8
	andl	$1, %r9d
	andl	$4, %r8d
	orq	%r9, %rcx
	orq	%r8, %rcx
	salq	$8, %rcx
	orq	%rax, %rcx
	movq	%rcx, 8(%rdx)
	movq	(%rsi,%rdi,8), %r13
	movq	%r13, %r14
	movq	%r13, %r12
	movq	%r13, %rbx
	movq	%r13, %r11
	shrq	$28, %r14
	movq	%r13, %r10
	movq	%r13, %r9
	movq	%r13, %r8
	leaq	0(,%r14,4), %rax
	leaq	0(,%r14,8), %rcx
	shrq	$24, %r12
	shrq	$20, %rbx
	shrq	$16, %r11
	andl	$64, %ecx
	andl	$16, %eax
	shrq	$12, %r10
	shrq	$8, %r9
	orq	%rcx, %rax
	movq	%r14, %rcx
	shrq	$4, %r8
	andl	$1, %ecx
	orq	%rcx, %rax
	leaq	(%r14,%r14), %rcx
	leaq	0(,%r13,4), %r14
	andl	$4, %ecx
	andl	$16, %r14d
	orq	%rcx, %rax
	salq	$56, %rax
	movq	%rax, %rcx
	leaq	0(,%r13,8), %rax
	andl	$64, %eax
	orq	%r14, %rax
	movq	%r13, %r14
	andl	$1, %r14d
	orq	%rax, %r14
	leaq	(%r13,%r13), %rax
	leaq	0(,%r12,8), %r13
	andl	$4, %eax
	andl	$64, %r13d
	orq	%r14, %rax
	orq	%rcx, %rax
	leaq	0(,%r12,4), %rcx
	andl	$16, %ecx
	orq	%r13, %rcx
	movq	%r12, %r13
	addq	%r12, %r12
	andl	$1, %r13d
	andl	$4, %r12d
	orq	%r13, %rcx
	orq	%r12, %rcx
	leaq	0(,%rbx,4), %r12
	salq	$48, %rcx
	andl	$16, %r12d
	orq	%rcx, %rax
	leaq	0(,%rbx,8), %rcx
	andl	$64, %ecx
	orq	%r12, %rcx
	movq	%rbx, %r12
	addq	%rbx, %rbx
	andl	$1, %r12d
	andl	$4, %ebx
	orq	%r12, %rcx
	orq	%rbx, %rcx
	leaq	0(,%r11,4), %rbx
	salq	$40, %rcx
	orq	%rcx, %rax
	leaq	0(,%r11,8), %rcx
	andl	$16, %ebx
	andl	$64, %ecx
	orq	%rbx, %rcx
	movq	%r11, %rbx
	addq	%r11, %r11
	andl	$1, %ebx
	andl	$4, %r11d
	orq	%rbx, %rcx
	orq	%r11, %rcx
	leaq	0(,%r10,4), %r11
	salq	$32, %rcx
	andl	$16, %r11d
	orq	%rax, %rcx
	leaq	0(,%r10,8), %rax
	andl	$64, %eax
	orq	%r11, %rax
	movq	%r10, %r11
	addq	%r10, %r10
	andl	$1, %r11d
	andl	$4, %r10d
	orq	%r11, %rax
	orq	%r10, %rax
	leaq	0(,%r9,4), %r10
	salq	$24, %rax
	andl	$16, %r10d
	orq	%rcx, %rax
	leaq	0(,%r9,8), %rcx
	andl	$64, %ecx
	orq	%r10, %rcx
	movq	%r9, %r10
	addq	%r9, %r9
	andl	$1, %r10d
	andl	$4, %r9d
	orq	%r10, %rcx
	orq	%r9, %rcx
	leaq	0(,%r8,4), %r9
	salq	$16, %rcx
	orq	%rax, %rcx
	leaq	0(,%r8,8), %rax
	andl	$64, %eax
	andl	$16, %r9d
	subq	$1, %rdi
	subq	$16, %rdx
	orq	%r9, %rax
	movq	%r8, %r9
	addq	%r8, %r8
	andl	$1, %r9d
	andl	$4, %r8d
	orq	%r9, %rax
	orq	%r8, %rax
	salq	$8, %rax
	orq	%rcx, %rax
	movq	%rax, 16(%rdx)
	testl	%edi, %edi
	jns	.L135
	jmp	.L131
	.cfi_endproc
.LFE257:
	.size	BN_GF2m_mod_sqr_arr, .-BN_GF2m_mod_sqr_arr
	.p2align 4
	.globl	BN_GF2m_mod_mul_arr
	.type	BN_GF2m_mod_mul_arr, @function
BN_GF2m_mod_mul_arr:
.LFB255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -160(%rbp)
	movq	%rdx, -144(%rbp)
	movq	%rcx, -168(%rbp)
	movq	%r8, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%rdx, %rsi
	je	.L173
	movq	%r8, %rbx
	movq	%r8, %rdi
	call	BN_CTX_start@PLT
	movq	%rbx, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L154
	movq	%rax, %r15
	movq	-144(%rbp), %rax
	movq	%r15, %rdi
	movl	8(%rax), %ebx
	addl	8(%r12), %ebx
	leal	4(%rbx), %r14d
	movl	%r14d, %esi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L154
	movl	%r14d, 8(%r15)
	testl	%r14d, %r14d
	jle	.L158
	leal	3(%rbx), %eax
	xorl	%esi, %esi
	leaq	8(,%rax,8), %rdx
	movq	-120(%rbp), %rax
	movq	(%rax), %rdi
	call	memset@PLT
.L158:
	movq	-144(%rbp), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jle	.L157
	movq	$0, -136(%rbp)
	movl	8(%r12), %edx
	movl	$0, -124(%rbp)
	.p2align 4,,10
	.p2align 3
.L163:
	movq	-144(%rbp), %rdi
	movl	-124(%rbp), %esi
	xorl	%r13d, %r13d
	movq	(%rdi), %rcx
	movq	-136(%rbp), %rdi
	addl	$1, %esi
	movq	(%rcx,%rdi), %r8
	cmpl	%esi, %eax
	je	.L159
	movq	8(%rcx,%rdi), %r13
.L159:
	testl	%edx, %edx
	jle	.L160
	xorl	%r14d, %r14d
	leaq	-96(%rbp), %rax
	xorl	%r15d, %r15d
	movq	-136(%rbp), %rbx
	movq	%rax, -112(%rbp)
	movq	%r14, %rax
	movl	%r15d, %r14d
	movq	%rax, %r15
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L167:
	movq	%rcx, %rbx
.L162:
	movq	(%r12), %rax
	leal	1(%r14), %ecx
	xorl	%esi, %esi
	movq	(%rax,%r15), %r9
	cmpl	%ecx, %edx
	je	.L161
	movq	8(%rax,%r15), %rsi
.L161:
	movq	-112(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r9, %rdx
	movq	%r8, -104(%rbp)
	addl	$2, %r14d
	addq	$16, %r15
	call	bn_GF2m_mul_2x2@PLT
	movq	-120(%rbp), %rax
	leaq	16(%rbx), %rcx
	movq	-96(%rbp), %rdx
	movq	-104(%rbp), %r8
	movq	(%rax), %rax
	xorq	%rdx, (%rax,%rbx)
	movq	-88(%rbp), %rdx
	xorq	%rdx, 8(%rax,%rbx)
	movq	-80(%rbp), %rdx
	xorq	%rdx, (%rax,%rcx)
	movq	-72(%rbp), %rdx
	xorq	%rdx, 24(%rax,%rbx)
	movl	8(%r12), %edx
	cmpl	%r14d, %edx
	jg	.L167
	movq	-144(%rbp), %rax
	movl	8(%rax), %eax
.L160:
	addl	$2, -124(%rbp)
	movl	-124(%rbp), %edi
	addq	$16, -136(%rbp)
	cmpl	%edi, %eax
	jg	.L163
.L157:
	movq	-120(%rbp), %rbx
	xorl	%r12d, %r12d
	movq	%rbx, %rdi
	call	bn_correct_top@PLT
	movq	-168(%rbp), %rdx
	movq	-160(%rbp), %rdi
	movq	%rbx, %rsi
	call	BN_GF2m_mod_arr
	testl	%eax, %eax
	setne	%r12b
.L153:
	movq	-152(%rbp), %rdi
	call	BN_CTX_end@PLT
.L149:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L174
	addq	$136, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L154:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L153
.L173:
	movq	%rcx, %rdx
	movq	%r8, %rcx
	call	BN_GF2m_mod_sqr_arr
	movl	%eax, %r12d
	jmp	.L149
.L174:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE255:
	.size	BN_GF2m_mod_mul_arr, .-BN_GF2m_mod_mul_arr
	.p2align 4
	.type	BN_GF2m_mod_solve_quad_arr.part.0, @function
BN_GF2m_mod_solve_quad_arr.part.0:
.LFB274:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	%rdi, -96(%rbp)
	movq	%rcx, %rdi
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	BN_CTX_get@PLT
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L258
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	BN_GF2m_mod_arr
	testl	%eax, %eax
	jne	.L259
.L258:
	movl	$0, -56(%rbp)
.L177:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	movl	-56(%rbp), %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L259:
	.cfi_restore_state
	movq	%r15, %rdi
	call	BN_is_zero@PLT
	movl	%eax, -56(%rbp)
	testl	%eax, %eax
	jne	.L260
	movl	0(%r13), %ebx
	andl	$1, %ebx
	jne	.L261
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	BN_CTX_get@PLT
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L258
	movl	$0, -100(%rbp)
	movq	%r15, -88(%rbp)
	movq	-80(%rbp), %r15
.L185:
	movl	0(%r13), %esi
	movq	-72(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	BN_priv_rand@PLT
	testl	%eax, %eax
	je	.L258
	movq	-72(%rbp), %rsi
	movq	%r13, %rdx
	movq	%rsi, %rdi
	call	BN_GF2m_mod_arr
	testl	%eax, %eax
	je	.L258
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	BN_set_word@PLT
	movq	-72(%rbp), %rsi
	movq	%r15, %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L258
	cmpl	$1, 0(%r13)
	jle	.L183
	movl	$1, -52(%rbp)
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L262:
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	BN_GF2m_mod_sqr_arr
	testl	%eax, %eax
	je	.L258
	movq	-88(%rbp), %rdx
	movq	-64(%rbp), %rdi
	movq	%r12, %r8
	movq	%r13, %rcx
	movq	%rbx, %rsi
	call	BN_GF2m_mod_mul_arr
	testl	%eax, %eax
	je	.L258
	movq	-64(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	BN_GF2m_add
	testl	%eax, %eax
	je	.L258
	movq	-72(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	BN_GF2m_add
	testl	%eax, %eax
	je	.L258
	addl	$1, -52(%rbp)
	movl	-52(%rbp), %eax
	cmpl	0(%r13), %eax
	jge	.L183
.L184:
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	BN_GF2m_mod_sqr_arr
	testl	%eax, %eax
	jne	.L262
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L261:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L258
	cmpl	$2, 0(%r13)
	jg	.L182
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L263:
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	BN_GF2m_mod_sqr_arr
	testl	%eax, %eax
	je	.L258
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	BN_GF2m_add
	testl	%eax, %eax
	je	.L258
	movl	0(%r13), %eax
	addl	$1, %ebx
	leal	-1(%rax), %edx
	movl	%edx, %eax
	shrl	$31, %eax
	addl	%edx, %eax
	sarl	%eax
	cmpl	%eax, %ebx
	jg	.L181
.L182:
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	BN_GF2m_mod_sqr_arr
	testl	%eax, %eax
	jne	.L263
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L260:
	movq	-96(%rbp), %rdi
	xorl	%esi, %esi
	call	BN_set_word@PLT
	movl	$1, -56(%rbp)
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L181:
	movq	-80(%rbp), %rbx
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	BN_GF2m_mod_sqr_arr
	testl	%eax, %eax
	je	.L258
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	BN_GF2m_add
	testl	%eax, %eax
	je	.L258
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	jne	.L264
	movq	-96(%rbp), %rdi
	movq	%r14, %rsi
	call	BN_copy@PLT
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	movl	%eax, -56(%rbp)
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L183:
	movq	%r15, %rdi
	addl	$1, -100(%rbp)
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L188
	cmpl	$50, -100(%rbp)
	jne	.L185
.L188:
	movq	-80(%rbp), %rdi
	movq	-88(%rbp), %r15
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L181
	movl	$1056, %r8d
	movl	$113, %edx
	movl	$135, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L264:
	movl	$1066, %r8d
	movl	$116, %edx
	movl	$135, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L177
	.cfi_endproc
.LFE274:
	.size	BN_GF2m_mod_solve_quad_arr.part.0, .-BN_GF2m_mod_solve_quad_arr.part.0
	.p2align 4
	.globl	BN_GF2m_mod_exp_arr
	.type	BN_GF2m_mod_exp_arr, @function
BN_GF2m_mod_exp_arr:
.LFB264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -64(%rbp)
	movq	%rdx, %rdi
	movq	%rsi, -56(%rbp)
	call	BN_is_zero@PLT
	movl	$1, %esi
	testl	%eax, %eax
	jne	.L287
	movq	%r13, %rdi
	call	BN_abs_is_word@PLT
	movl	%eax, -68(%rbp)
	testl	%eax, %eax
	jne	.L288
	movq	%r12, %rdi
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L270
	movq	-56(%rbp), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	BN_GF2m_mod_arr
	testl	%eax, %eax
	jne	.L289
.L270:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L288:
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	movl	%eax, -68(%rbp)
.L265:
	movl	-68(%rbp), %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L287:
	.cfi_restore_state
	movq	-64(%rbp), %rdi
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BN_set_word@PLT
	.p2align 4,,10
	.p2align 3
.L289:
	.cfi_restore_state
	movq	%r13, %rdi
	call	BN_num_bits@PLT
	subl	$2, %eax
	movl	%eax, %ebx
	jns	.L274
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L275:
	subl	$1, %ebx
	cmpl	$-1, %ebx
	je	.L272
.L274:
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	BN_GF2m_mod_sqr_arr
	testl	%eax, %eax
	je	.L270
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	BN_is_bit_set@PLT
	testl	%eax, %eax
	je	.L275
	movq	-56(%rbp), %rdx
	movq	%r12, %r8
	movq	%r14, %rcx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	BN_GF2m_mod_mul_arr
	testl	%eax, %eax
	jne	.L275
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L272:
	movq	-64(%rbp), %rdi
	movq	%r15, %rsi
	call	BN_copy@PLT
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	movl	%eax, -68(%rbp)
	jmp	.L270
	.cfi_endproc
.LFE264:
	.size	BN_GF2m_mod_exp_arr, .-BN_GF2m_mod_exp_arr
	.p2align 4
	.globl	BN_GF2m_mod_sqrt_arr
	.type	BN_GF2m_mod_sqrt_arr, @function
BN_GF2m_mod_sqrt_arr:
.LFB266:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L300
	movq	%rcx, %r12
	movq	%rcx, %rdi
	movq	%rsi, %r15
	movq	%rdx, %r13
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L295
	movl	0(%r13), %eax
	movq	%rbx, %rdi
	leal	-1(%rax), %esi
	call	BN_set_bit@PLT
	testl	%eax, %eax
	je	.L295
	movq	%r13, %rcx
	movq	%r12, %r8
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	BN_GF2m_mod_exp_arr
	movl	%eax, %r13d
.L294:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
.L290:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L295:
	.cfi_restore_state
	xorl	%r13d, %r13d
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L300:
	xorl	%esi, %esi
	movl	$1, %r13d
	call	BN_set_word@PLT
	jmp	.L290
	.cfi_endproc
.LFE266:
	.size	BN_GF2m_mod_sqrt_arr, .-BN_GF2m_mod_sqrt_arr
	.p2align 4
	.globl	BN_GF2m_mod_solve_quad_arr
	.type	BN_GF2m_mod_solve_quad_arr, @function
BN_GF2m_mod_solve_quad_arr:
.LFB268:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L305
	jmp	BN_GF2m_mod_solve_quad_arr.part.0
	.p2align 4,,10
	.p2align 3
.L305:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	BN_set_word@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE268:
	.size	BN_GF2m_mod_solve_quad_arr, .-BN_GF2m_mod_solve_quad_arr
	.p2align 4
	.globl	BN_GF2m_poly2arr
	.type	BN_GF2m_poly2arr, @function
BN_GF2m_poly2arr:
.LFB270:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	subq	$8, %rsp
	call	BN_is_zero@PLT
	xorl	%r8d, %r8d
	testl	%eax, %eax
	jne	.L308
	movl	8(%r13), %r10d
	movl	%eax, %r8d
	subl	$1, %r10d
	js	.L310
	movq	0(%r13), %r11
	movslq	%r10d, %r10
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L311:
	subq	$1, %r10
	testl	%r10d, %r10d
	js	.L310
.L315:
	movq	(%r11,%r10,8), %rcx
	testq	%rcx, %rcx
	je	.L311
	movl	%r10d, %r9d
	movl	$64, %eax
	movabsq	$-9223372036854775808, %rdx
	sall	$6, %r9d
	subl	$1, %r9d
	.p2align 4,,10
	.p2align 3
.L314:
	testq	%rdx, %rcx
	je	.L312
	cmpl	%r8d, %ebx
	jle	.L313
	movslq	%r8d, %rsi
	leal	(%r9,%rax), %edi
	movl	%edi, (%r12,%rsi,4)
.L313:
	addl	$1, %r8d
.L312:
	shrq	%rdx
	subl	$1, %eax
	jne	.L314
	subq	$1, %r10
	testl	%r10d, %r10d
	jns	.L315
.L310:
	cmpl	%r8d, %ebx
	jle	.L308
	movslq	%r8d, %rax
	addl	$1, %r8d
	movl	$-1, (%r12,%rax,4)
.L308:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE270:
	.size	BN_GF2m_poly2arr, .-BN_GF2m_poly2arr
	.p2align 4
	.globl	BN_GF2m_mod_mul
	.type	BN_GF2m_mod_mul, @function
BN_GF2m_mod_mul:
.LFB256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -56(%rbp)
	movq	%rcx, %rdi
	movq	%rsi, -64(%rbp)
	call	BN_num_bits@PLT
	movl	$474, %edx
	leaq	.LC0(%rip), %rsi
	leal	1(%rax), %ebx
	movslq	%ebx, %rdi
	salq	$2, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L327
	movl	%ebx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	BN_GF2m_poly2arr
	testl	%eax, %eax
	je	.L331
	cmpl	%eax, %ebx
	jl	.L331
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	movq	%r15, %r8
	movq	%r12, %rcx
	movq	%r14, %rdx
	call	BN_GF2m_mod_mul_arr
.L327:
	movq	%r12, %rdi
	movl	$484, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, -56(%rbp)
	call	CRYPTO_free@PLT
	movl	-56(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L331:
	.cfi_restore_state
	movl	$478, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$106, %edx
	movl	%eax, -56(%rbp)
	movl	$133, %esi
	movl	$3, %edi
	call	ERR_put_error@PLT
	movl	-56(%rbp), %eax
	jmp	.L327
	.cfi_endproc
.LFE256:
	.size	BN_GF2m_mod_mul, .-BN_GF2m_mod_mul
	.p2align 4
	.globl	BN_GF2m_mod_inv
	.type	BN_GF2m_mod_inv, @function
BN_GF2m_mod_inv:
.LFB260:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$152, %rsp
	movq	%rdi, -56(%rbp)
	movq	%rcx, %rdi
	call	BN_CTX_start@PLT
	movq	%r15, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L335
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L425:
	movq	%r13, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L424
.L335:
	movq	%r14, %rdi
	call	BN_num_bits@PLT
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%r13, %rdi
	leal	-1(%rax), %esi
	call	BN_priv_rand@PLT
	testl	%eax, %eax
	jne	.L425
.L423:
	xorl	%r12d, %r12d
.L336:
	movq	%r15, %rdi
	call	BN_CTX_end@PLT
	addq	$152, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L424:
	.cfi_restore_state
	movq	-56(%rbp), %rdi
	movq	%r15, %r8
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movl	%eax, %r12d
	call	BN_GF2m_mod_mul
	testl	%eax, %eax
	je	.L423
	movq	%r15, %rdi
	call	BN_CTX_start@PLT
	movq	%r15, %rdi
	call	BN_CTX_get@PLT
	movq	%r15, %rdi
	movq	%rax, -88(%rbp)
	call	BN_CTX_get@PLT
	movq	%r15, %rdi
	movq	%rax, -96(%rbp)
	call	BN_CTX_get@PLT
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	call	BN_CTX_get@PLT
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L339
	movq	-64(%rbp), %rbx
	movq	-56(%rbp), %rsi
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	BN_GF2m_mod
	testl	%eax, %eax
	je	.L339
	movq	%rbx, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L426
	.p2align 4,,10
	.p2align 3
.L339:
	movq	%r15, %rdi
	call	BN_CTX_end@PLT
	jmp	.L336
.L426:
	movq	-80(%rbp), %rdi
	movq	%r14, %rsi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L339
	movq	%rbx, %rdi
	call	BN_num_bits@PLT
	movq	-80(%rbp), %rdi
	movl	%eax, -120(%rbp)
	call	BN_num_bits@PLT
	movq	%rbx, %rdi
	movl	%eax, -68(%rbp)
	movl	8(%r14), %eax
	movl	%eax, %esi
	movl	%eax, -72(%rbp)
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L339
	movq	%rbx, %rax
	movq	(%rbx), %rbx
	movslq	8(%rax), %rax
	cmpl	%eax, -72(%rbp)
	jle	.L342
	movl	-72(%rbp), %edx
	leaq	(%rbx,%rax,8), %rdi
	xorl	%esi, %esi
	leal	-1(%rdx), %ecx
	movl	%ecx, %edx
	subl	%eax, %edx
	leaq	8(,%rdx,8), %rdx
	call	memset@PLT
.L342:
	movl	-72(%rbp), %eax
	movq	-64(%rbp), %rdx
	movq	-88(%rbp), %rdi
	movl	%eax, 8(%rdx)
	movl	%eax, %esi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L339
	movq	-88(%rbp), %rdx
	movl	-72(%rbp), %eax
	movq	(%rdx), %rcx
	movq	$1, (%rcx)
	cmpl	$1, %eax
	jle	.L343
	subl	$2, %eax
	leaq	8(%rcx), %rdi
	xorl	%esi, %esi
	movq	%rcx, -104(%rbp)
	leaq	8(,%rax,8), %rdx
	call	memset@PLT
	movl	-72(%rbp), %eax
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rdi
	movl	%eax, 8(%rdx)
	movl	%eax, %esi
	call	bn_wexpand@PLT
	movq	-104(%rbp), %rcx
	testq	%rax, %rax
	je	.L339
	movq	-96(%rbp), %rax
	movq	(%rax), %r10
.L371:
	movq	%rcx, -112(%rbp)
	movl	-72(%rbp), %ecx
	movq	%r10, %rdi
	leal	-1(%rcx), %eax
	testl	%ecx, %ecx
	movl	%eax, -104(%rbp)
	leaq	8(,%rax,8), %rdx
	movl	$8, %eax
	cmovle	%rax, %rdx
	xorl	%esi, %esi
	call	memset@PLT
	movq	-112(%rbp), %rcx
	movq	%rax, %r10
.L346:
	movl	-72(%rbp), %esi
	movq	-96(%rbp), %rax
	movq	%r13, -184(%rbp)
	movl	%r12d, -188(%rbp)
	movl	%esi, 8(%rax)
	movq	-80(%rbp), %rax
	movq	%r15, -176(%rbp)
	movl	-120(%rbp), %r15d
	movq	(%rax), %r9
	movl	%esi, %eax
	movq	%r14, -152(%rbp)
	shrl	%eax
	salq	$4, %rax
	movq	%rax, %rdi
	movl	%esi, %eax
	andl	$-2, %eax
	movq	%rdi, %r14
	movslq	%eax, %rdx
	movl	%eax, -112(%rbp)
	movslq	-104(%rbp), %rax
	salq	$3, %rdx
	salq	$3, %rax
	movq	%rax, -160(%rbp)
	leal	-2(%rsi), %eax
	addq	$2, %rax
	movq	%rax, -168(%rbp)
	.p2align 4,,10
	.p2align 3
.L347:
	movq	(%rbx), %rdi
	testl	%r15d, %r15d
	je	.L350
	testb	$1, %dil
	je	.L351
	cmpl	$64, %r15d
	jle	.L352
.L353:
	movl	-68(%rbp), %esi
	cmpl	%esi, %r15d
	jge	.L356
	movl	%r15d, %eax
	movl	%esi, %r15d
	movq	-80(%rbp), %rsi
	movq	-88(%rbp), %rdi
	movq	-64(%rbp), %r11
	movl	%eax, -68(%rbp)
	movq	%r10, %rcx
	movq	%r9, %rbx
	movq	%rsi, -64(%rbp)
	movq	-96(%rbp), %rsi
	movq	(%rdi), %r10
	movq	(%r11), %r9
	movq	%r11, -80(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rdi, -96(%rbp)
.L356:
	movl	-72(%rbp), %eax
	testl	%eax, %eax
	jle	.L363
	leaq	15(%rbx), %rax
	movq	%rax, %rsi
	movq	%rax, %rdi
	subq	%r9, %rsi
	cmpq	$30, %rsi
	seta	%sil
	subq	%rcx, %rdi
	cmpq	$30, %rdi
	seta	%dil
	andl	%esi, %edi
	cmpl	$1, -104(%rbp)
	seta	%sil
	subq	%r10, %rax
	andl	%edi, %esi
	cmpq	$30, %rax
	leaq	15(%r9), %rax
	seta	%dil
	subq	%rcx, %rax
	andl	%edi, %esi
	cmpq	$30, %rax
	seta	%al
	testb	%al, %sil
	je	.L360
	leaq	15(%rcx), %rax
	subq	%r10, %rax
	cmpq	$30, %rax
	jbe	.L360
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L361:
	movdqu	(%rbx,%rax), %xmm0
	movdqu	(%r9,%rax), %xmm1
	pxor	%xmm1, %xmm0
	movups	%xmm0, (%rbx,%rax)
	movdqu	(%rcx,%rax), %xmm0
	movdqu	(%r10,%rax), %xmm2
	pxor	%xmm2, %xmm0
	movups	%xmm0, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%r14, %rax
	jne	.L361
	movl	-112(%rbp), %esi
	cmpl	%esi, -72(%rbp)
	je	.L363
	movq	(%r9,%rdx), %rax
	xorq	%rax, (%rbx,%rdx)
	movq	(%r10,%rdx), %rax
	xorq	%rax, (%rcx,%rdx)
.L363:
	cmpl	-68(%rbp), %r15d
	jne	.L347
	movl	-68(%rbp), %esi
	leal	62(%rsi), %eax
	subl	$1, %esi
	cmovns	%esi, %eax
	sarl	$6, %eax
	movslq	%eax, %rsi
	movl	%eax, %r12d
	movq	(%rbx,%rsi,8), %rdi
	testq	%rdi, %rdi
	jne	.L367
	testl	%eax, %eax
	je	.L368
	leal	-1(%rax), %edi
	subq	$2, %rsi
	movslq	%edi, %rax
	movl	%edi, %edi
	subq	%rdi, %rsi
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L369:
	subq	$1, %rax
	cmpq	%rsi, %rax
	je	.L427
.L370:
	movq	(%rbx,%rax,8), %rdi
	movl	%eax, %r12d
	testq	%rdi, %rdi
	je	.L369
.L367:
	sall	$6, %r12d
.L368:
	movq	%rdx, -144(%rbp)
	movq	%r10, -136(%rbp)
	movq	%rcx, -128(%rbp)
	movq	%r9, -120(%rbp)
	call	BN_num_bits_word@PLT
	movq	-120(%rbp), %r9
	movq	-128(%rbp), %rcx
	movq	-136(%rbp), %r10
	movq	-144(%rbp), %rdx
	leal	(%rax,%r12), %r15d
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L352:
	cmpq	$1, %rdi
	jne	.L353
	movq	-88(%rbp), %rbx
	movq	-184(%rbp), %r13
	movl	-188(%rbp), %r12d
	movq	-152(%rbp), %r14
	movq	%rbx, %rdi
	movq	-176(%rbp), %r15
	call	bn_correct_top@PLT
	movq	%rbx, %rsi
	movq	-56(%rbp), %rbx
	movq	%rbx, %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L339
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	BN_CTX_end@PLT
	movq	%r15, %r8
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	BN_GF2m_mod_mul
	testl	%eax, %eax
	setne	%r12b
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L350:
	testq	%rdi, %rdi
	jne	.L352
.L354:
	endbr64
	movq	-176(%rbp), %r15
	movq	%r15, %rdi
	call	BN_CTX_end@PLT
	jmp	.L423
.L360:
	movl	-104(%rbp), %edi
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L365:
	movq	(%r9,%rax,8), %rsi
	xorq	%rsi, (%rbx,%rax,8)
	movq	(%r10,%rax,8), %rsi
	xorq	%rsi, (%rcx,%rax,8)
	movq	%rax, %rsi
	addq	$1, %rax
	cmpq	%rdi, %rsi
	jne	.L365
	jmp	.L363
.L351:
	movq	-152(%rbp), %rsi
	movq	(%rcx), %rax
	movq	(%rsi), %r12
	movq	%rax, %r11
	movl	-104(%rbp), %esi
	andl	$1, %r11d
	movq	(%r12), %r8
	negq	%r11
	andq	%r11, %r8
	xorq	%rax, %r8
	testl	%esi, %esi
	jle	.L372
	movq	%r9, -120(%rbp)
	movq	-168(%rbp), %r9
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L349:
	movq	%rdi, %rsi
	movq	(%rbx,%rax,8), %rdi
	shrq	%rsi
	movq	%rdi, %r13
	salq	$63, %r13
	orq	%r13, %rsi
	movq	%rsi, -8(%rbx,%rax,8)
	movq	%r8, %rsi
	movq	(%r12,%rax,8), %r8
	shrq	%rsi
	andq	%r11, %r8
	xorq	(%rcx,%rax,8), %r8
	movq	%r8, %r13
	salq	$63, %r13
	orq	%r13, %rsi
	movq	%rsi, -8(%rcx,%rax,8)
	addq	$1, %rax
	cmpq	%r9, %rax
	jne	.L349
	movq	-160(%rbp), %rax
	movq	-120(%rbp), %r9
	leaq	(%rbx,%rax), %rsi
	addq	%rcx, %rax
.L348:
	shrq	%rdi
	shrq	%r8
	subl	$1, %r15d
	movq	%rdi, (%rsi)
	movq	%r8, (%rax)
	jmp	.L347
.L427:
	xorl	%r12d, %r12d
	jmp	.L368
.L372:
	movq	%rcx, %rax
	movq	%rbx, %rsi
	jmp	.L348
.L343:
	movl	%eax, 8(%rdx)
	movq	-96(%rbp), %rdi
	movl	%eax, %esi
	movq	%rcx, -104(%rbp)
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L339
	movl	-72(%rbp), %eax
	movq	-96(%rbp), %rdx
	movq	-104(%rbp), %rcx
	cmpl	$1, %eax
	movq	(%rdx), %r10
	je	.L371
	subl	$1, %eax
	movl	%eax, -104(%rbp)
	jmp	.L346
	.cfi_endproc
.LFE260:
	.size	BN_GF2m_mod_inv, .-BN_GF2m_mod_inv
	.p2align 4
	.globl	BN_GF2m_mod_inv_arr
	.type	BN_GF2m_mod_inv_arr, @function
BN_GF2m_mod_inv_arr:
.LFB261:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rcx, %rdi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	testq	%rax, %rax
	je	.L429
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	BN_set_word@PLT
	movl	(%rbx), %esi
	cmpl	$-1, %esi
	je	.L430
	addq	$4, %rbx
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L445:
	movl	(%rbx), %esi
	addq	$4, %rbx
	cmpl	$-1, %esi
	je	.L430
.L431:
	movq	%r13, %rdi
	call	BN_set_bit@PLT
	testl	%eax, %eax
	jne	.L445
.L429:
	xorl	%r13d, %r13d
.L432:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L430:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r12, %rcx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	BN_GF2m_mod_inv
	movl	%eax, %r13d
	jmp	.L432
	.cfi_endproc
.LFE261:
	.size	BN_GF2m_mod_inv_arr, .-BN_GF2m_mod_inv_arr
	.p2align 4
	.globl	BN_GF2m_mod_div
	.type	BN_GF2m_mod_div, @function
BN_GF2m_mod_div:
.LFB262:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%rdi, -56(%rbp)
	movq	%r8, %rdi
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	testq	%rax, %rax
	je	.L449
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	BN_GF2m_mod_inv
	testl	%eax, %eax
	jne	.L454
.L449:
	xorl	%r13d, %r13d
.L448:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L454:
	.cfi_restore_state
	movq	-56(%rbp), %rdi
	movq	%r13, %rdx
	movq	%r12, %r8
	movq	%r14, %rcx
	movq	%rbx, %rsi
	xorl	%r13d, %r13d
	call	BN_GF2m_mod_mul
	testl	%eax, %eax
	setne	%r13b
	jmp	.L448
	.cfi_endproc
.LFE262:
	.size	BN_GF2m_mod_div, .-BN_GF2m_mod_div
	.p2align 4
	.globl	BN_GF2m_mod_div_arr
	.type	BN_GF2m_mod_div_arr, @function
BN_GF2m_mod_div_arr:
.LFB263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	movq	%rdi, -56(%rbp)
	movq	%r8, %rdi
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	testq	%rax, %rax
	je	.L459
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	BN_set_word@PLT
	movl	(%rbx), %esi
	cmpl	$-1, %esi
	je	.L458
	addq	$4, %rbx
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L478:
	movl	(%rbx), %esi
	addq	$4, %rbx
	cmpl	$-1, %esi
	je	.L458
.L460:
	movq	%r14, %rdi
	call	BN_set_bit@PLT
	testl	%eax, %eax
	jne	.L478
.L459:
	xorl	%r13d, %r13d
.L457:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L458:
	.cfi_restore_state
	movq	%r12, %rdi
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L463
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	BN_GF2m_mod_inv
	testl	%eax, %eax
	jne	.L461
.L463:
	xorl	%r13d, %r13d
.L462:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L461:
	movq	-56(%rbp), %rdi
	movq	%r12, %r8
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r15, %rsi
	xorl	%r13d, %r13d
	call	BN_GF2m_mod_mul
	testl	%eax, %eax
	setne	%r13b
	jmp	.L462
	.cfi_endproc
.LFE263:
	.size	BN_GF2m_mod_div_arr, .-BN_GF2m_mod_div_arr
	.p2align 4
	.globl	BN_GF2m_mod_sqr
	.type	BN_GF2m_mod_sqr, @function
BN_GF2m_mod_sqr:
.LFB258:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -56(%rbp)
	movq	%rdx, %rdi
	movq	%rsi, -64(%rbp)
	call	BN_num_bits@PLT
	movl	$532, %edx
	leaq	.LC0(%rip), %rsi
	leal	1(%rax), %ebx
	movslq	%ebx, %rdi
	salq	$2, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L480
	movl	%ebx, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	BN_GF2m_poly2arr
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L484
	cmpl	%eax, %ebx
	jl	.L484
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	movq	%r15, %rcx
	movq	%r13, %rdx
	call	BN_GF2m_mod_sqr_arr
	movl	%eax, %r12d
.L480:
	movq	%r13, %rdi
	movl	$542, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L484:
	.cfi_restore_state
	movl	$536, %r8d
	movl	$106, %edx
	movl	$136, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L480
	.cfi_endproc
.LFE258:
	.size	BN_GF2m_mod_sqr, .-BN_GF2m_mod_sqr
	.p2align 4
	.globl	BN_GF2m_mod_exp
	.type	BN_GF2m_mod_exp, @function
BN_GF2m_mod_exp:
.LFB265:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -56(%rbp)
	movq	%rcx, %rdi
	movq	%rsi, -64(%rbp)
	call	BN_num_bits@PLT
	movl	$906, %edx
	leaq	.LC0(%rip), %rsi
	leal	1(%rax), %ebx
	movslq	%ebx, %rdi
	salq	$2, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L488
	movl	%ebx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	BN_GF2m_poly2arr
	testl	%eax, %eax
	je	.L492
	cmpl	%eax, %ebx
	jl	.L492
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	movq	%r15, %r8
	movq	%r12, %rcx
	movq	%r14, %rdx
	call	BN_GF2m_mod_exp_arr
.L488:
	movq	%r12, %rdi
	movl	$916, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, -56(%rbp)
	call	CRYPTO_free@PLT
	movl	-56(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L492:
	.cfi_restore_state
	movl	$910, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$106, %edx
	movl	%eax, -56(%rbp)
	movl	$132, %esi
	movl	$3, %edi
	call	ERR_put_error@PLT
	movl	-56(%rbp), %eax
	jmp	.L488
	.cfi_endproc
.LFE265:
	.size	BN_GF2m_mod_exp, .-BN_GF2m_mod_exp
	.p2align 4
	.globl	BN_GF2m_mod_sqrt
	.type	BN_GF2m_mod_sqrt, @function
BN_GF2m_mod_sqrt:
.LFB267:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -56(%rbp)
	movq	%rdx, %rdi
	movq	%rsi, -64(%rbp)
	call	BN_num_bits@PLT
	movl	$965, %edx
	leaq	.LC0(%rip), %rsi
	leal	1(%rax), %ebx
	movslq	%ebx, %rdi
	salq	$2, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L496
	movl	%ebx, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	BN_GF2m_poly2arr
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L504
	cmpl	%eax, %ebx
	jl	.L504
	movl	(%r12), %eax
	testl	%eax, %eax
	je	.L510
	movq	%r13, %rdi
	call	BN_CTX_start@PLT
	movq	%r13, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L502
	movl	(%r12), %eax
	movq	%r15, %rdi
	leal	-1(%rax), %esi
	call	BN_set_bit@PLT
	testl	%eax, %eax
	jne	.L511
.L502:
	xorl	%r15d, %r15d
.L501:
	movq	%r13, %rdi
	call	BN_CTX_end@PLT
.L496:
	movq	%r12, %rdi
	movl	$975, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$24, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L504:
	.cfi_restore_state
	movl	$969, %r8d
	movl	$106, %edx
	movl	$137, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L496
	.p2align 4,,10
	.p2align 3
.L511:
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	movq	%r15, %rdx
	movq	%r13, %r8
	movq	%r12, %rcx
	call	BN_GF2m_mod_exp_arr
	movl	%eax, %r15d
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L510:
	movq	-56(%rbp), %rdi
	xorl	%esi, %esi
	movl	$1, %r15d
	call	BN_set_word@PLT
	jmp	.L496
	.cfi_endproc
.LFE267:
	.size	BN_GF2m_mod_sqrt, .-BN_GF2m_mod_sqrt
	.p2align 4
	.globl	BN_GF2m_mod_solve_quad
	.type	BN_GF2m_mod_solve_quad, @function
BN_GF2m_mod_solve_quad:
.LFB269:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rdx, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -56(%rbp)
	movq	%rcx, -64(%rbp)
	call	BN_num_bits@PLT
	movl	$1095, %edx
	leaq	.LC0(%rip), %rsi
	leal	1(%rax), %ebx
	movslq	%ebx, %rdi
	salq	$2, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L513
	movl	%ebx, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	BN_GF2m_poly2arr
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L518
	cmpl	%eax, %ebx
	jl	.L518
	movl	(%r12), %eax
	testl	%eax, %eax
	je	.L521
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	BN_GF2m_mod_solve_quad_arr.part.0
	movl	%eax, %r14d
.L513:
	movq	%r12, %rdi
	movl	$1105, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$24, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L518:
	.cfi_restore_state
	movl	$1099, %r8d
	movl	$106, %edx
	movl	$134, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L521:
	xorl	%esi, %esi
	movq	%r13, %rdi
	movl	$1, %r14d
	call	BN_set_word@PLT
	jmp	.L513
	.cfi_endproc
.LFE269:
	.size	BN_GF2m_mod_solve_quad, .-BN_GF2m_mod_solve_quad
	.p2align 4
	.globl	BN_GF2m_arr2poly
	.type	BN_GF2m_arr2poly, @function
BN_GF2m_arr2poly:
.LFB271:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	BN_set_word@PLT
	movl	(%rbx), %esi
	cmpl	$-1, %esi
	je	.L523
	addq	$4, %rbx
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L534:
	movl	(%rbx), %esi
	addq	$4, %rbx
	cmpl	$-1, %esi
	je	.L523
.L525:
	movq	%r12, %rdi
	call	BN_set_bit@PLT
	testl	%eax, %eax
	jne	.L534
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L523:
	.cfi_restore_state
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE271:
	.size	BN_GF2m_arr2poly, .-BN_GF2m_arr2poly
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.quad	64
	.quad	64
	.align 16
.LC2:
	.quad	16
	.quad	16
	.align 16
.LC3:
	.quad	4
	.quad	4
	.align 16
.LC4:
	.quad	1
	.quad	1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
