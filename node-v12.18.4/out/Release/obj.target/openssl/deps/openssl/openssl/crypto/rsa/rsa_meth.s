	.file	"rsa_meth.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/rsa/rsa_meth.c"
	.text
	.p2align 4
	.globl	RSA_meth_new
	.type	RSA_meth_new, @function
RSA_meth_new:
.LFB408:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$120, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	%esi, %ebx
	leaq	.LC0(%rip), %rsi
	subq	$8, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L2
	movl	%ebx, 72(%rax)
	movl	$21, %edx
	movq	%r13, %rdi
	movq	%rax, %r12
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L9
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	movl	$25, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
.L2:
	movl	$28, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$162, %esi
	movl	$4, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE408:
	.size	RSA_meth_new, .-RSA_meth_new
	.p2align 4
	.globl	RSA_meth_free
	.type	RSA_meth_free, @function
RSA_meth_free:
.LFB409:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L10
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$35, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$36, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	ret
	.cfi_endproc
.LFE409:
	.size	RSA_meth_free, .-RSA_meth_free
	.p2align 4
	.globl	RSA_meth_dup
	.type	RSA_meth_dup, @function
RSA_meth_dup:
.LFB410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$42, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$120, %edi
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L16
	movdqu	(%rbx), %xmm0
	movq	%rax, %r12
	movl	$47, %edx
	leaq	.LC0(%rip), %rsi
	movups	%xmm0, (%rax)
	movdqu	16(%rbx), %xmm1
	movups	%xmm1, 16(%rax)
	movdqu	32(%rbx), %xmm2
	movups	%xmm2, 32(%rax)
	movdqu	48(%rbx), %xmm3
	movups	%xmm3, 48(%rax)
	movdqu	64(%rbx), %xmm4
	movups	%xmm4, 64(%rax)
	movdqu	80(%rbx), %xmm5
	movups	%xmm5, 80(%rax)
	movdqu	96(%rbx), %xmm6
	movups	%xmm6, 96(%rax)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movq	(%rbx), %rdi
	call	CRYPTO_strdup@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L22
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	movl	$51, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
.L16:
	xorl	%r12d, %r12d
	movl	$54, %r8d
	movl	$65, %edx
	movl	$161, %esi
	leaq	.LC0(%rip), %rcx
	movl	$4, %edi
	call	ERR_put_error@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE410:
	.size	RSA_meth_dup, .-RSA_meth_dup
	.p2align 4
	.globl	RSA_meth_get0_name
	.type	RSA_meth_get0_name, @function
RSA_meth_get0_name:
.LFB411:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE411:
	.size	RSA_meth_get0_name, .-RSA_meth_get0_name
	.p2align 4
	.globl	RSA_meth_set1_name
	.type	RSA_meth_set1_name, @function
RSA_meth_set1_name:
.LFB412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$65, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	testq	%rax, %rax
	je	.L28
	movq	(%r12), %rdi
	movq	%rax, %rbx
	movl	$72, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%rbx, (%r12)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movl	$68, %r8d
	movl	$65, %edx
	movl	$163, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE412:
	.size	RSA_meth_set1_name, .-RSA_meth_set1_name
	.p2align 4
	.globl	RSA_meth_get_flags
	.type	RSA_meth_get_flags, @function
RSA_meth_get_flags:
.LFB413:
	.cfi_startproc
	endbr64
	movl	72(%rdi), %eax
	ret
	.cfi_endproc
.LFE413:
	.size	RSA_meth_get_flags, .-RSA_meth_get_flags
	.p2align 4
	.globl	RSA_meth_set_flags
	.type	RSA_meth_set_flags, @function
RSA_meth_set_flags:
.LFB414:
	.cfi_startproc
	endbr64
	movl	%esi, 72(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE414:
	.size	RSA_meth_set_flags, .-RSA_meth_set_flags
	.p2align 4
	.globl	RSA_meth_get0_app_data
	.type	RSA_meth_get0_app_data, @function
RSA_meth_get0_app_data:
.LFB415:
	.cfi_startproc
	endbr64
	movq	80(%rdi), %rax
	ret
	.cfi_endproc
.LFE415:
	.size	RSA_meth_get0_app_data, .-RSA_meth_get0_app_data
	.p2align 4
	.globl	RSA_meth_set0_app_data
	.type	RSA_meth_set0_app_data, @function
RSA_meth_set0_app_data:
.LFB416:
	.cfi_startproc
	endbr64
	movq	%rsi, 80(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE416:
	.size	RSA_meth_set0_app_data, .-RSA_meth_set0_app_data
	.p2align 4
	.globl	RSA_meth_get_pub_enc
	.type	RSA_meth_get_pub_enc, @function
RSA_meth_get_pub_enc:
.LFB417:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE417:
	.size	RSA_meth_get_pub_enc, .-RSA_meth_get_pub_enc
	.p2align 4
	.globl	RSA_meth_set_pub_enc
	.type	RSA_meth_set_pub_enc, @function
RSA_meth_set_pub_enc:
.LFB418:
	.cfi_startproc
	endbr64
	movq	%rsi, 8(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE418:
	.size	RSA_meth_set_pub_enc, .-RSA_meth_set_pub_enc
	.p2align 4
	.globl	RSA_meth_get_pub_dec
	.type	RSA_meth_get_pub_dec, @function
RSA_meth_get_pub_dec:
.LFB419:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE419:
	.size	RSA_meth_get_pub_dec, .-RSA_meth_get_pub_dec
	.p2align 4
	.globl	RSA_meth_set_pub_dec
	.type	RSA_meth_set_pub_dec, @function
RSA_meth_set_pub_dec:
.LFB420:
	.cfi_startproc
	endbr64
	movq	%rsi, 16(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE420:
	.size	RSA_meth_set_pub_dec, .-RSA_meth_set_pub_dec
	.p2align 4
	.globl	RSA_meth_get_priv_enc
	.type	RSA_meth_get_priv_enc, @function
RSA_meth_get_priv_enc:
.LFB421:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE421:
	.size	RSA_meth_get_priv_enc, .-RSA_meth_get_priv_enc
	.p2align 4
	.globl	RSA_meth_set_priv_enc
	.type	RSA_meth_set_priv_enc, @function
RSA_meth_set_priv_enc:
.LFB422:
	.cfi_startproc
	endbr64
	movq	%rsi, 24(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE422:
	.size	RSA_meth_set_priv_enc, .-RSA_meth_set_priv_enc
	.p2align 4
	.globl	RSA_meth_get_priv_dec
	.type	RSA_meth_get_priv_dec, @function
RSA_meth_get_priv_dec:
.LFB423:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE423:
	.size	RSA_meth_get_priv_dec, .-RSA_meth_get_priv_dec
	.p2align 4
	.globl	RSA_meth_set_priv_dec
	.type	RSA_meth_set_priv_dec, @function
RSA_meth_set_priv_dec:
.LFB424:
	.cfi_startproc
	endbr64
	movq	%rsi, 32(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE424:
	.size	RSA_meth_set_priv_dec, .-RSA_meth_set_priv_dec
	.p2align 4
	.globl	RSA_meth_get_mod_exp
	.type	RSA_meth_get_mod_exp, @function
RSA_meth_get_mod_exp:
.LFB425:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE425:
	.size	RSA_meth_get_mod_exp, .-RSA_meth_get_mod_exp
	.p2align 4
	.globl	RSA_meth_set_mod_exp
	.type	RSA_meth_set_mod_exp, @function
RSA_meth_set_mod_exp:
.LFB426:
	.cfi_startproc
	endbr64
	movq	%rsi, 40(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE426:
	.size	RSA_meth_set_mod_exp, .-RSA_meth_set_mod_exp
	.p2align 4
	.globl	RSA_meth_get_bn_mod_exp
	.type	RSA_meth_get_bn_mod_exp, @function
RSA_meth_get_bn_mod_exp:
.LFB427:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	ret
	.cfi_endproc
.LFE427:
	.size	RSA_meth_get_bn_mod_exp, .-RSA_meth_get_bn_mod_exp
	.p2align 4
	.globl	RSA_meth_set_bn_mod_exp
	.type	RSA_meth_set_bn_mod_exp, @function
RSA_meth_set_bn_mod_exp:
.LFB428:
	.cfi_startproc
	endbr64
	movq	%rsi, 48(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE428:
	.size	RSA_meth_set_bn_mod_exp, .-RSA_meth_set_bn_mod_exp
	.p2align 4
	.globl	RSA_meth_get_init
	.type	RSA_meth_get_init, @function
RSA_meth_get_init:
.LFB429:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rax
	ret
	.cfi_endproc
.LFE429:
	.size	RSA_meth_get_init, .-RSA_meth_get_init
	.p2align 4
	.globl	RSA_meth_set_init
	.type	RSA_meth_set_init, @function
RSA_meth_set_init:
.LFB430:
	.cfi_startproc
	endbr64
	movq	%rsi, 56(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE430:
	.size	RSA_meth_set_init, .-RSA_meth_set_init
	.p2align 4
	.globl	RSA_meth_get_finish
	.type	RSA_meth_get_finish, @function
RSA_meth_get_finish:
.LFB431:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	ret
	.cfi_endproc
.LFE431:
	.size	RSA_meth_get_finish, .-RSA_meth_get_finish
	.p2align 4
	.globl	RSA_meth_set_finish
	.type	RSA_meth_set_finish, @function
RSA_meth_set_finish:
.LFB432:
	.cfi_startproc
	endbr64
	movq	%rsi, 64(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE432:
	.size	RSA_meth_set_finish, .-RSA_meth_set_finish
	.p2align 4
	.globl	RSA_meth_get_sign
	.type	RSA_meth_get_sign, @function
RSA_meth_get_sign:
.LFB433:
	.cfi_startproc
	endbr64
	movq	88(%rdi), %rax
	ret
	.cfi_endproc
.LFE433:
	.size	RSA_meth_get_sign, .-RSA_meth_get_sign
	.p2align 4
	.globl	RSA_meth_set_sign
	.type	RSA_meth_set_sign, @function
RSA_meth_set_sign:
.LFB434:
	.cfi_startproc
	endbr64
	movq	%rsi, 88(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE434:
	.size	RSA_meth_set_sign, .-RSA_meth_set_sign
	.p2align 4
	.globl	RSA_meth_get_verify
	.type	RSA_meth_get_verify, @function
RSA_meth_get_verify:
.LFB435:
	.cfi_startproc
	endbr64
	movq	96(%rdi), %rax
	ret
	.cfi_endproc
.LFE435:
	.size	RSA_meth_get_verify, .-RSA_meth_get_verify
	.p2align 4
	.globl	RSA_meth_set_verify
	.type	RSA_meth_set_verify, @function
RSA_meth_set_verify:
.LFB436:
	.cfi_startproc
	endbr64
	movq	%rsi, 96(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE436:
	.size	RSA_meth_set_verify, .-RSA_meth_set_verify
	.p2align 4
	.globl	RSA_meth_get_keygen
	.type	RSA_meth_get_keygen, @function
RSA_meth_get_keygen:
.LFB437:
	.cfi_startproc
	endbr64
	movq	104(%rdi), %rax
	ret
	.cfi_endproc
.LFE437:
	.size	RSA_meth_get_keygen, .-RSA_meth_get_keygen
	.p2align 4
	.globl	RSA_meth_set_keygen
	.type	RSA_meth_set_keygen, @function
RSA_meth_set_keygen:
.LFB438:
	.cfi_startproc
	endbr64
	movq	%rsi, 104(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE438:
	.size	RSA_meth_set_keygen, .-RSA_meth_set_keygen
	.p2align 4
	.globl	RSA_meth_get_multi_prime_keygen
	.type	RSA_meth_get_multi_prime_keygen, @function
RSA_meth_get_multi_prime_keygen:
.LFB439:
	.cfi_startproc
	endbr64
	movq	112(%rdi), %rax
	ret
	.cfi_endproc
.LFE439:
	.size	RSA_meth_get_multi_prime_keygen, .-RSA_meth_get_multi_prime_keygen
	.p2align 4
	.globl	RSA_meth_set_multi_prime_keygen
	.type	RSA_meth_set_multi_prime_keygen, @function
RSA_meth_set_multi_prime_keygen:
.LFB440:
	.cfi_startproc
	endbr64
	movq	%rsi, 112(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE440:
	.size	RSA_meth_set_multi_prime_keygen, .-RSA_meth_set_multi_prime_keygen
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
