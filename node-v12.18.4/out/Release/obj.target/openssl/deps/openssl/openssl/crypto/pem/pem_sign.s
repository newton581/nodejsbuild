	.file	"pem_sign.c"
	.text
	.p2align 4
	.globl	PEM_SignInit
	.type	PEM_SignInit, @function
PEM_SignInit:
.LFB779:
	.cfi_startproc
	endbr64
	xorl	%edx, %edx
	jmp	EVP_DigestInit_ex@PLT
	.cfi_endproc
.LFE779:
	.size	PEM_SignInit, .-PEM_SignInit
	.p2align 4
	.globl	PEM_SignUpdate
	.type	PEM_SignUpdate, @function
PEM_SignUpdate:
.LFB780:
	.cfi_startproc
	endbr64
	movl	%edx, %edx
	jmp	EVP_DigestUpdate@PLT
	.cfi_endproc
.LFE780:
	.size	PEM_SignUpdate, .-PEM_SignUpdate
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/pem/pem_sign.c"
	.text
	.p2align 4
	.globl	PEM_SignFinal
	.type	PEM_SignFinal, @function
PEM_SignFinal:
.LFB781:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%rcx, %rdi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_PKEY_size@PLT
	movl	$34, %edx
	leaq	.LC0(%rip), %rsi
	movslq	%eax, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L10
	movq	%r13, %rcx
	leaq	-60(%rbp), %rdx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	EVP_SignFinal@PLT
	xorl	%r13d, %r13d
	testl	%eax, %eax
	jle	.L6
	movl	-60(%rbp), %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	$1, %r13d
	call	EVP_EncodeBlock@PLT
	movl	%eax, (%rbx)
.L6:
	movl	$48, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L11
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movl	$36, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$112, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
	jmp	.L6
.L11:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE781:
	.size	PEM_SignFinal, .-PEM_SignFinal
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
