	.file	"bf_ecb.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"blowfish(ptr)"
	.text
	.p2align 4
	.globl	BF_options
	.type	BF_options, @function
BF_options:
.LFB4:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE4:
	.size	BF_options, .-BF_options
	.p2align 4
	.globl	BF_ecb_encrypt
	.type	BF_ecb_encrypt, @function
BF_ecb_encrypt:
.LFB5:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	(%rdi), %eax
	bswap	%eax
	movl	%eax, -32(%rbp)
	movl	4(%rdi), %eax
	leaq	-32(%rbp), %rdi
	bswap	%eax
	movl	%eax, -28(%rbp)
	testl	%ecx, %ecx
	je	.L4
	call	BF_encrypt@PLT
.L5:
	movl	-32(%rbp), %eax
	bswap	%eax
	movl	%eax, (%rbx)
	movl	-28(%rbp), %eax
	bswap	%eax
	movl	%eax, 4(%rbx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L8
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	call	BF_decrypt@PLT
	jmp	.L5
.L8:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5:
	.size	BF_ecb_encrypt, .-BF_ecb_encrypt
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
