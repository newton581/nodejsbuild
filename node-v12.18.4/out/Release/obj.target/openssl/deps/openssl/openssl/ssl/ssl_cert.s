	.file	"ssl_cert.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"SSL for verify callback"
	.text
	.p2align 4
	.type	ssl_x509_store_ctx_init_ossl_, @function
ssl_x509_store_ctx_init_ossl_:
.LFB1481:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	.LC0(%rip), %rdx
	xorl	%esi, %esi
	movl	$5, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	CRYPTO_get_ex_new_index@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%eax, ssl_x509_store_ctx_idx(%rip)
	movl	ssl_x509_store_ctx_idx(%rip), %eax
	notl	%eax
	shrl	$31, %eax
	movl	%eax, ssl_x509_store_ctx_init_ossl_ret_(%rip)
	ret
	.cfi_endproc
.LFE1481:
	.size	ssl_x509_store_ctx_init_ossl_, .-ssl_x509_store_ctx_init_ossl_
	.p2align 4
	.type	ssl_security_default_callback, @function
ssl_security_default_callback:
.LFB1520:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L5
	movq	%rsi, %rdi
	call	SSL_CTX_get_security_level@PLT
.L6:
	testl	%eax, %eax
	jle	.L40
	cmpl	$5, %eax
	movl	$5, %edx
	leaq	minbits_table.26886(%rip), %rcx
	cmovle	%eax, %edx
	subl	$1, %edx
	movslq	%edx, %rdx
	movl	(%rcx,%rdx,4), %edx
	cmpl	$15, %r15d
	je	.L9
	jg	.L10
	cmpl	$9, %r15d
	je	.L11
	cmpl	$10, %r15d
	jne	.L13
	cmpl	$2, %eax
	setle	%al
	movzbl	%al, %eax
.L4:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	subl	$65537, %r15d
	cmpl	$2, %r15d
	ja	.L13
	cmpl	%ebx, %edx
	jg	.L26
	testb	$4, 32(%r14)
	jne	.L26
	movl	40(%r14), %ecx
	testb	$1, %cl
	jne	.L26
	cmpl	$160, %edx
	jle	.L14
	andl	$2, %ecx
	jne	.L26
.L14:
	cmpl	$1, %eax
	je	.L4
	cmpl	$4, 36(%r14)
	je	.L26
	cmpl	$2, %eax
	movl	$1, %eax
	je	.L4
	cmpl	$772, 44(%r14)
	je	.L4
	xorl	%eax, %eax
	testb	$6, 28(%r14)
	setne	%al
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L40:
	cmpl	$262151, %r15d
	sete	%al
	cmpl	$79, %ebx
	setle	%dl
	addq	$8, %rsp
	andl	%edx, %eax
	popq	%rbx
	popq	%r12
	xorl	$1, %eax
	popq	%r13
	popq	%r14
	movzbl	%al, %eax
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	call	SSL_get_security_level@PLT
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L11:
	movq	8(%r12), %rdx
	movq	192(%rdx), %rdx
	testb	$8, 96(%rdx)
	jne	.L15
	cmpl	$768, %r13d
	jg	.L27
	cmpl	$1, %eax
	jg	.L26
.L27:
	cmpl	$769, %r13d
	jg	.L28
	cmpl	$2, %eax
	jg	.L26
.L28:
	cmpl	$770, %r13d
	setle	%dl
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L13:
	xorl	%eax, %eax
	cmpl	%ebx, %edx
	setle	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	cmpl	$1, %eax
	sete	%al
	addq	$8, %rsp
	popq	%rbx
	movzbl	%al, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	cmpl	$256, %r13d
	sete	%dl
	cmpl	$65277, %r13d
	setg	%cl
	orl	%ecx, %edx
.L39:
	cmpl	$3, %eax
	setg	%al
	andl	%eax, %edx
	xorl	$1, %edx
	movzbl	%dl, %eax
	jmp	.L4
	.cfi_endproc
.LFE1520:
	.size	ssl_security_default_callback, .-ssl_security_default_callback
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../deps/openssl/openssl/ssl/ssl_cert.c"
	.text
	.p2align 4
	.type	xname_cmp, @function
xname_cmp:
.LFB1512:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	leaq	-56(%rbp), %rsi
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	call	i2d_X509_NAME@PLT
	leaq	-48(%rbp), %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	i2d_X509_NAME@PLT
	movq	-56(%rbp), %r13
	testl	%ebx, %ebx
	js	.L47
	testl	%eax, %eax
	js	.L47
	cmpl	%eax, %ebx
	je	.L45
	subl	%eax, %ebx
	movl	%ebx, %r12d
.L44:
	movl	$588, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	movq	-48(%rbp), %rdi
	movl	$589, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L49
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	movq	-48(%rbp), %rsi
	movslq	%ebx, %rdx
	movq	%r13, %rdi
	call	memcmp@PLT
	movl	%eax, %r12d
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L47:
	movl	$-2, %r12d
	jmp	.L44
.L49:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1512:
	.size	xname_cmp, .-xname_cmp
	.p2align 4
	.type	xname_hash, @function
xname_hash:
.LFB1514:
	.cfi_startproc
	endbr64
	jmp	X509_NAME_hash@PLT
	.cfi_endproc
.LFE1514:
	.size	xname_hash, .-xname_hash
	.p2align 4
	.type	xname_sk_cmp, @function
xname_sk_cmp:
.LFB1513:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %rdi
	movq	(%rsi), %r12
	leaq	-56(%rbp), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	call	i2d_X509_NAME@PLT
	leaq	-48(%rbp), %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	i2d_X509_NAME@PLT
	movq	-56(%rbp), %r13
	testl	%ebx, %ebx
	js	.L57
	testl	%eax, %eax
	js	.L57
	cmpl	%eax, %ebx
	je	.L55
	subl	%eax, %ebx
	movl	%ebx, %r12d
.L54:
	movl	$588, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	movq	-48(%rbp), %rdi
	movl	$589, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L59
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	movq	-48(%rbp), %rsi
	movslq	%ebx, %rdx
	movq	%r13, %rdi
	call	memcmp@PLT
	movl	%eax, %r12d
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L57:
	movl	$-2, %r12d
	jmp	.L54
.L59:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1513:
	.size	xname_sk_cmp, .-xname_sk_cmp
	.p2align 4
	.globl	SSL_get_ex_data_X509_STORE_CTX_idx
	.type	SSL_get_ex_data_X509_STORE_CTX_idx, @function
SSL_get_ex_data_X509_STORE_CTX_idx:
.LFB1483:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	ssl_x509_store_ctx_init_ossl_(%rip), %rsi
	leaq	ssl_x509_store_ctx_once(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L63
	movl	ssl_x509_store_ctx_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	je	.L63
	movl	ssl_x509_store_ctx_idx(%rip), %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	movl	$-1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1483:
	.size	SSL_get_ex_data_X509_STORE_CTX_idx, .-SSL_get_ex_data_X509_STORE_CTX_idx
	.p2align 4
	.globl	ssl_cert_new
	.type	ssl_cert_new, @function
ssl_cert_new:
.LFB1484:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$52, %edx
	movl	$536, %edi
	leaq	.LC1(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L69
	leaq	32(%rax), %rax
	movq	%rax, (%r12)
	leaq	ssl_security_default_callback(%rip), %rax
	movl	$1, 520(%r12)
	mfence
	movq	%rax, 488(%r12)
	movl	$1, 496(%r12)
	movq	$0, 504(%r12)
	call	CRYPTO_THREAD_lock_new@PLT
	movq	%rax, 528(%r12)
	testq	%rax, %rax
	je	.L70
.L65:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	movl	$55, %r8d
	movl	$65, %edx
	movl	$162, %esi
	movl	$20, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L70:
	movl	$66, %r8d
	movl	$65, %edx
	movl	$162, %esi
	movl	$20, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movl	$67, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L65
	.cfi_endproc
.LFE1484:
	.size	ssl_cert_new, .-ssl_cert_new
	.p2align 4
	.globl	ssl_cert_clear_certs
	.type	ssl_cert_clear_certs, @function
ssl_cert_clear_certs:
.LFB1486:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L77
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	X509_free@GOTPCREL(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	.LC1(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	392(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	32(%rdi), %rbx
	.p2align 4,,10
	.p2align 3
.L73:
	movq	(%rbx), %rdi
	addq	$40, %rbx
	call	X509_free@PLT
	movq	$0, -40(%rbx)
	movq	-32(%rbx), %rdi
	call	EVP_PKEY_free@PLT
	movq	$0, -32(%rbx)
	movq	-24(%rbx), %rdi
	movq	%r14, %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	$0, -24(%rbx)
	movq	-16(%rbx), %rdi
	movq	%r13, %rsi
	movl	$216, %edx
	call	CRYPTO_free@PLT
	movq	$0, -16(%rbx)
	movq	$0, -8(%rbx)
	cmpq	%r12, %rbx
	jne	.L73
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.cfi_endproc
.LFE1486:
	.size	ssl_cert_clear_certs, .-ssl_cert_clear_certs
	.p2align 4
	.globl	ssl_cert_free
	.type	ssl_cert_free, @function
ssl_cert_free:
.LFB1487:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L89
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	lock xaddl	%eax, 520(%rdi)
	subl	$1, %eax
	testl	%eax, %eax
	je	.L92
	jle	.L84
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
.L84:
	movq	8(%r12), %rdi
	leaq	32(%r12), %rbx
	leaq	392(%r12), %r15
	leaq	.LC1(%rip), %r13
	call	EVP_PKEY_free@PLT
	movq	X509_free@GOTPCREL(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L86:
	movq	(%rbx), %rdi
	addq	$40, %rbx
	call	X509_free@PLT
	movq	$0, -40(%rbx)
	movq	-32(%rbx), %rdi
	call	EVP_PKEY_free@PLT
	movq	$0, -32(%rbx)
	movq	-24(%rbx), %rdi
	movq	%r14, %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	$0, -24(%rbx)
	movq	-16(%rbx), %rdi
	movq	%r13, %rsi
	movl	$216, %edx
	call	CRYPTO_free@PLT
	movq	$0, -16(%rbx)
	movq	$0, -8(%rbx)
	cmpq	%r15, %rbx
	jne	.L86
	movq	408(%r12), %rdi
	movl	$239, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	424(%r12), %rdi
	movl	$240, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	392(%r12), %rdi
	movl	$241, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	464(%r12), %rdi
	call	X509_STORE_free@PLT
	movq	456(%r12), %rdi
	call	X509_STORE_free@PLT
	leaq	472(%r12), %rdi
	call	custom_exts_free@PLT
	movq	512(%r12), %rdi
	movl	$246, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	528(%r12), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$249, %edx
	popq	%rbx
	.cfi_restore 3
	leaq	.LC1(%rip), %rsi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L89:
	ret
	.cfi_endproc
.LFE1487:
	.size	ssl_cert_free, .-ssl_cert_free
	.p2align 4
	.globl	ssl_cert_dup
	.type	ssl_cert_dup, @function
ssl_cert_dup:
.LFB1485:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$76, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movl	$536, %edi
	pushq	%rbx
	.cfi_offset 3, -48
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L153
	movl	$1, 520(%rax)
	mfence
	movq	(%r12), %rax
	addq	%r13, %rax
	subq	%r12, %rax
	movq	%rax, 0(%r13)
	call	CRYPTO_THREAD_lock_new@PLT
	movq	%rax, 528(%r13)
	testq	%rax, %rax
	je	.L154
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L97
	movq	%rdi, 8(%r13)
	call	EVP_PKEY_up_ref@PLT
.L97:
	movq	16(%r12), %rax
	movl	$32, %ebx
	leaq	.LC1(%rip), %r14
	movq	%rax, 16(%r13)
	movl	24(%r12), %eax
	movl	%eax, 24(%r13)
	.p2align 4,,10
	.p2align 3
.L104:
	movq	(%r12,%rbx), %rdi
	testq	%rdi, %rdi
	je	.L98
	movq	%rdi, 0(%r13,%rbx)
	call	X509_up_ref@PLT
.L98:
	movq	8(%r12,%rbx), %rax
	testq	%rax, %rax
	je	.L99
	movq	%rax, 8(%r13,%rbx)
	movq	8(%r12,%rbx), %rdi
	call	EVP_PKEY_up_ref@PLT
.L99:
	movq	16(%r12,%rbx), %rdi
	testq	%rdi, %rdi
	je	.L100
	call	X509_chain_up_ref@PLT
	movq	%rax, 16(%r13,%rbx)
	testq	%rax, %rax
	je	.L155
.L100:
	cmpq	$0, 24(%r12,%rbx)
	je	.L102
	movq	32(%r12,%rbx), %rdi
	movl	$124, %edx
	movq	%r14, %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, 24(%r13,%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L156
	movq	32(%r12,%rbx), %rax
	movq	24(%r12,%rbx), %rsi
	movq	%rax, 32(%r13,%rbx)
	movq	32(%r12,%rbx), %rdx
	call	memcpy@PLT
.L102:
	addq	$40, %rbx
	cmpq	$392, %rbx
	jne	.L104
	cmpq	$0, 408(%r12)
	je	.L105
	movq	416(%r12), %rax
	movl	$137, %edx
	leaq	.LC1(%rip), %rsi
	leaq	(%rax,%rax), %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, 408(%r13)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L101
	movq	416(%r12), %rax
	movq	408(%r12), %rsi
	leaq	(%rax,%rax), %rdx
	call	memcpy@PLT
	movq	416(%r12), %rax
	cmpq	$0, 424(%r12)
	movq	%rax, 416(%r13)
	je	.L108
.L157:
	movq	432(%r12), %rax
	movl	$148, %edx
	leaq	.LC1(%rip), %rsi
	leaq	(%rax,%rax), %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, 424(%r13)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L101
	movq	432(%r12), %rax
	movq	424(%r12), %rsi
	leaq	(%rax,%rax), %rdx
	call	memcpy@PLT
	movq	432(%r12), %rax
	movq	%rax, 432(%r13)
.L110:
	movq	392(%r12), %rdi
	testq	%rdi, %rdi
	je	.L111
	movq	400(%r12), %rsi
	movl	$159, %ecx
	leaq	.LC1(%rip), %rdx
	call	CRYPTO_memdup@PLT
	movq	%rax, 392(%r13)
	testq	%rax, %rax
	je	.L101
	movq	400(%r12), %rax
	movq	%rax, 400(%r13)
.L111:
	movl	28(%r12), %eax
	movq	464(%r12), %rdi
	movl	%eax, 28(%r13)
	movq	440(%r12), %rax
	movq	%rax, 440(%r13)
	movq	448(%r12), %rax
	movq	%rax, 448(%r13)
	testq	%rdi, %rdi
	je	.L112
	call	X509_STORE_up_ref@PLT
	movq	464(%r12), %rax
	movq	%rax, 464(%r13)
.L112:
	movq	456(%r12), %rdi
	testq	%rdi, %rdi
	je	.L113
	call	X509_STORE_up_ref@PLT
	movq	456(%r12), %rax
	movq	%rax, 456(%r13)
.L113:
	movq	488(%r12), %rax
	leaq	472(%r12), %rsi
	leaq	472(%r13), %rdi
	movq	%rax, 488(%r13)
	movl	496(%r12), %eax
	movl	%eax, 496(%r13)
	movq	504(%r12), %rax
	movq	%rax, 504(%r13)
	call	custom_exts_copy@PLT
	testl	%eax, %eax
	je	.L101
	movq	512(%r12), %rdi
	testq	%rdi, %rdi
	je	.L93
	movl	$188, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, 512(%r13)
	testq	%rax, %rax
	je	.L101
.L93:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore_state
	cmpq	$0, 424(%r12)
	movq	$0, 408(%r13)
	jne	.L157
.L108:
	movq	$0, 424(%r13)
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L155:
	movl	$117, %r8d
.L152:
	movl	$65, %edx
	movl	$221, %esi
	movl	$20, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
.L101:
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	ssl_cert_free
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L156:
	.cfi_restore_state
	movl	$126, %r8d
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L153:
	movl	$80, %r8d
	movl	$65, %edx
	movl	$221, %esi
	movl	$20, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L93
.L154:
	movl	$88, %r8d
	movl	$65, %edx
	movl	$221, %esi
	movl	$20, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	movl	$89, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L93
	.cfi_endproc
.LFE1485:
	.size	ssl_cert_dup, .-ssl_cert_dup
	.p2align 4
	.globl	ssl_cert_set0_chain
	.type	ssl_cert_set0_chain, @function
ssl_cert_set0_chain:
.LFB1488:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	testq	%rdi, %rdi
	je	.L159
	movq	1168(%rdi), %rax
	movq	(%rax), %r15
	testq	%r15, %r15
	je	.L165
.L168:
	xorl	%ebx, %ebx
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L164:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%rax, %rdx
	movq	%r13, %rdi
	call	ssl_security_cert@PLT
	cmpl	$1, %eax
	jne	.L167
	addl	$1, %ebx
.L162:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L164
	movq	16(%r15), %rdi
	movq	X509_free@GOTPCREL(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	%r12, 16(%r15)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L167:
	.cfi_restore_state
	movl	%eax, %edx
	movl	$261, %r8d
	movl	$340, %esi
	movl	$20, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore_state
	movq	320(%rsi), %rax
	movq	(%rax), %r15
	testq	%r15, %r15
	jne	.L168
.L165:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1488:
	.size	ssl_cert_set0_chain, .-ssl_cert_set0_chain
	.p2align 4
	.globl	ssl_cert_set1_chain
	.type	ssl_cert_set1_chain, @function
ssl_cert_set1_chain:
.LFB1489:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -40
	testq	%rdx, %rdx
	je	.L176
	movq	%rdx, %rdi
	call	X509_chain_up_ref@PLT
	movq	%rax, %r12
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L169
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	ssl_cert_set0_chain
	testl	%eax, %eax
	je	.L177
	movl	$1, %eax
.L169:
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	movq	X509_free@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, -36(%rbp)
	call	OPENSSL_sk_pop_free@PLT
	movl	-36(%rbp), %eax
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_restore_state
	addq	$24, %rsp
	xorl	%edx, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	ssl_cert_set0_chain
	.cfi_endproc
.LFE1489:
	.size	ssl_cert_set1_chain, .-ssl_cert_set1_chain
	.p2align 4
	.globl	ssl_cert_add0_chain_cert
	.type	ssl_cert_add0_chain_cert, @function
ssl_cert_add0_chain_cert:
.LFB1490:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testq	%rdi, %rdi
	je	.L179
	movq	1168(%rdi), %rax
	movq	(%rax), %rbx
.L180:
	testq	%rbx, %rbx
	je	.L190
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	call	ssl_security_cert@PLT
	cmpl	$1, %eax
	jne	.L191
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L192
.L184:
	movq	%r12, %rsi
	call	OPENSSL_sk_push@PLT
	popq	%rbx
	popq	%r12
	testl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	setne	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L191:
	.cfi_restore_state
	movl	$293, %r8d
	leaq	.LC1(%rip), %rcx
	movl	%eax, %edx
	movl	$346, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
.L190:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	.cfi_restore_state
	movq	320(%rsi), %rax
	movq	(%rax), %rbx
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L192:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L190
	jmp	.L184
	.cfi_endproc
.LFE1490:
	.size	ssl_cert_add0_chain_cert, .-ssl_cert_add0_chain_cert
	.p2align 4
	.globl	ssl_cert_add1_chain_cert
	.type	ssl_cert_add1_chain_cert, @function
ssl_cert_add1_chain_cert:
.LFB1491:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testq	%rdi, %rdi
	je	.L194
	movq	1168(%rdi), %rax
	movq	(%rax), %rbx
.L195:
	testq	%rbx, %rbx
	je	.L208
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	call	ssl_security_cert@PLT
	movl	%eax, %r12d
	cmpl	$1, %eax
	jne	.L209
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L210
.L199:
	movq	%r13, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L208
	movq	%r13, %rdi
	call	X509_up_ref@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	.cfi_restore_state
	movl	$293, %r8d
	leaq	.LC1(%rip), %rcx
	movl	%eax, %edx
	movl	$346, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
.L208:
	addq	$8, %rsp
	xorl	%r12d, %r12d
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L194:
	.cfi_restore_state
	movq	320(%rsi), %rax
	movq	(%rax), %rbx
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L210:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L208
	jmp	.L199
	.cfi_endproc
.LFE1491:
	.size	ssl_cert_add1_chain_cert, .-ssl_cert_add1_chain_cert
	.p2align 4
	.globl	ssl_cert_select_current
	.type	ssl_cert_select_current, @function
ssl_cert_select_current:
.LFB1492:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L234
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	32(%rdi), %rbx
	movq	%rbx, %rax
	cmpq	32(%rdi), %rsi
	je	.L235
.L214:
	leaq	72(%r12), %rax
	cmpq	72(%r12), %r13
	je	.L236
	leaq	112(%r12), %rax
	cmpq	112(%r12), %r13
	je	.L237
.L217:
	leaq	152(%r12), %rax
	cmpq	152(%r12), %r13
	je	.L238
.L218:
	leaq	192(%r12), %rax
	cmpq	192(%r12), %r13
	je	.L239
.L219:
	leaq	232(%r12), %rax
	cmpq	232(%r12), %r13
	je	.L240
.L220:
	leaq	272(%r12), %rax
	cmpq	272(%r12), %r13
	je	.L241
.L221:
	leaq	312(%r12), %rax
	cmpq	312(%r12), %r13
	je	.L242
.L222:
	leaq	352(%r12), %rax
	cmpq	%r13, 352(%r12)
	je	.L243
.L223:
	leaq	392(%r12), %r14
	.p2align 4,,10
	.p2align 3
.L225:
	cmpq	$0, 8(%rbx)
	je	.L224
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L224
	movq	%r13, %rsi
	call	X509_cmp@PLT
	testl	%eax, %eax
	je	.L244
.L224:
	addq	$40, %rbx
	cmpq	%r14, %rbx
	jne	.L225
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L235:
	.cfi_restore_state
	cmpq	$0, 40(%rdi)
	je	.L214
.L215:
	movq	%rax, (%r12)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L236:
	.cfi_restore_state
	cmpq	$0, 80(%r12)
	jne	.L215
	leaq	112(%r12), %rax
	cmpq	112(%r12), %r13
	jne	.L217
.L237:
	cmpq	$0, 120(%r12)
	je	.L217
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L238:
	cmpq	$0, 160(%r12)
	je	.L218
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L239:
	cmpq	$0, 200(%r12)
	je	.L219
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L240:
	cmpq	$0, 240(%r12)
	je	.L220
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L241:
	cmpq	$0, 280(%r12)
	je	.L221
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L242:
	cmpq	$0, 320(%r12)
	je	.L222
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L243:
	cmpq	$0, 360(%r12)
	je	.L223
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L234:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L244:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	%rbx, (%r12)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1492:
	.size	ssl_cert_select_current, .-ssl_cert_select_current
	.p2align 4
	.globl	ssl_cert_set_current
	.type	ssl_cert_set_current, @function
ssl_cert_set_current:
.LFB1493:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L251
	cmpq	$1, %rsi
	je	.L255
	xorl	%eax, %eax
	cmpq	$2, %rsi
	jne	.L245
	movq	(%rdi), %rdx
	leaq	32(%rdi), %rsi
	subq	%rsi, %rdx
	sarq	$3, %rdx
	imull	$-858993459, %edx, %edx
	leal	1(%rdx), %r8d
	cmpl	$8, %r8d
	jg	.L245
.L248:
	movl	$8, %edx
	movslq	%r8d, %rcx
	subl	%r8d, %edx
	leaq	(%rcx,%rcx,4), %rax
	addq	%rcx, %rdx
	leaq	(%rsi,%rax,8), %rax
	leaq	(%rdx,%rdx,4), %rdx
	leaq	40(%rsi,%rdx,8), %rdx
	.p2align 4,,10
	.p2align 3
.L250:
	cmpq	$0, (%rax)
	je	.L249
	cmpq	$0, 8(%rax)
	jne	.L256
.L249:
	addq	$40, %rax
	cmpq	%rdx, %rax
	jne	.L250
.L251:
	xorl	%eax, %eax
.L245:
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	leaq	32(%rdi), %rsi
	xorl	%r8d, %r8d
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L256:
	movq	%rax, (%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1493:
	.size	ssl_cert_set_current, .-ssl_cert_set_current
	.p2align 4
	.globl	ssl_cert_set_cert_cb
	.type	ssl_cert_set_cert_cb, @function
ssl_cert_set_cert_cb:
.LFB1494:
	.cfi_startproc
	endbr64
	movq	%rsi, 440(%rdi)
	movq	%rdx, 448(%rdi)
	ret
	.cfi_endproc
.LFE1494:
	.size	ssl_cert_set_cert_cb, .-ssl_cert_set_cert_cb
	.section	.rodata.str1.1
.LC2:
	.string	"ssl_client"
.LC3:
	.string	"ssl_server"
	.text
	.p2align 4
	.globl	ssl_verify_cert_chain
	.type	ssl_verify_cert_chain, @function
ssl_verify_cert_chain:
.LFB1495:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testq	%rsi, %rsi
	je	.L289
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movq	%rsi, %r12
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	je	.L289
	movq	1168(%rbx), %rax
	movq	464(%rax), %r14
	testq	%r14, %r14
	je	.L290
.L262:
	call	X509_STORE_CTX_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L291
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	OPENSSL_sk_value@PLT
	movq	%r12, %rcx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	X509_STORE_CTX_init@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L292
	movq	%r13, %rdi
	call	X509_STORE_CTX_get0_param@PLT
	movq	%rbx, %rdi
	movq	%rax, %r14
	call	SSL_get_security_level@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	X509_VERIFY_PARAM_set_auth_level@PLT
	movq	1168(%rbx), %rax
	movq	%r13, %rdi
	movl	28(%rax), %esi
	andl	$196608, %esi
	call	X509_STORE_CTX_set_flags@PLT
	leaq	ssl_x509_store_ctx_init_ossl_(%rip), %rsi
	leaq	ssl_x509_store_ctx_once(%rip), %rdi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L274
	movl	ssl_x509_store_ctx_init_ossl_ret_(%rip), %edx
	testl	%edx, %edx
	je	.L274
	movl	ssl_x509_store_ctx_idx(%rip), %esi
.L266:
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	X509_STORE_CTX_set_ex_data@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L293
.L265:
	endbr64
	movq	%r13, %rdi
	call	X509_STORE_CTX_free@PLT
.L295:
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L291:
	.cfi_restore_state
	movl	$381, %r8d
	movl	$65, %edx
	movl	$207, %esi
	movl	$20, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
.L289:
	xorl	%r12d, %r12d
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L293:
	.cfi_restore_state
	movq	224(%rbx), %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jle	.L267
	leaq	216(%rbx), %rsi
	movq	%r13, %rdi
	call	X509_STORE_CTX_set0_dane@PLT
.L267:
	movl	56(%rbx), %eax
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	testl	%eax, %eax
	leaq	.LC3(%rip), %rax
	cmove	%rax, %rsi
	call	X509_STORE_CTX_set_default@PLT
	movq	208(%rbx), %rsi
	movq	%r14, %rdi
	call	X509_VERIFY_PARAM_set1@PLT
	movq	1384(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L269
	movq	%r13, %rdi
	call	X509_STORE_CTX_set_verify_cb@PLT
.L269:
	movq	1440(%rbx), %rdx
	movq	160(%rdx), %rax
	testq	%rax, %rax
	je	.L270
	movq	168(%rdx), %rsi
	movq	%r13, %rdi
	call	*%rax
	movl	%eax, %r12d
.L271:
	movq	%r13, %rdi
	call	X509_STORE_CTX_get_error@PLT
	movq	1448(%rbx), %rdi
	movq	X509_free@GOTPCREL(%rip), %rsi
	cltq
	movq	%rax, 1456(%rbx)
	call	OPENSSL_sk_pop_free@PLT
	movq	%r13, %rdi
	movq	$0, 1448(%rbx)
	call	X509_STORE_CTX_get0_chain@PLT
	testq	%rax, %rax
	je	.L272
	movq	%r13, %rdi
	call	X509_STORE_CTX_get1_chain@PLT
	movq	%rax, 1448(%rbx)
	testq	%rax, %rax
	je	.L294
.L272:
	movq	208(%rbx), %rdi
	movq	%r14, %rsi
	call	X509_VERIFY_PARAM_move_peername@PLT
	movq	%r13, %rdi
	call	X509_STORE_CTX_free@PLT
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L290:
	movq	1440(%rbx), %rax
	movq	32(%rax), %r14
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L292:
	movl	$387, %r8d
	movl	$11, %edx
	movl	$207, %esi
	movl	$20, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r13, %rdi
	call	X509_STORE_CTX_free@PLT
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L270:
	movq	%r13, %rdi
	call	X509_verify_cert@PLT
	movl	%eax, %r12d
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L294:
	movl	$435, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$207, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L272
.L274:
	movl	$-1, %esi
	jmp	.L266
	.cfi_endproc
.LFE1495:
	.size	ssl_verify_cert_chain, .-ssl_verify_cert_chain
	.p2align 4
	.globl	SSL_dup_CA_list
	.type	SSL_dup_CA_list, @function
SSL_dup_CA_list:
.LFB1497:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	call	OPENSSL_sk_num@PLT
	xorl	%edi, %edi
	movl	%eax, %esi
	movl	%eax, %ebx
	call	OPENSSL_sk_new_reserve@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L297
	xorl	%r14d, %r14d
	testl	%ebx, %ebx
	jg	.L298
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L300:
	movq	%r12, %rdi
	addl	$1, %r14d
	call	OPENSSL_sk_push@PLT
	cmpl	%ebx, %r14d
	je	.L296
.L298:
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	call	X509_NAME_dup@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L300
	movl	$470, %r8d
	movl	$65, %edx
	movl	$408, %esi
	movl	$20, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	movq	X509_NAME_free@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	OPENSSL_sk_pop_free@PLT
.L296:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L297:
	.cfi_restore_state
	movl	$464, %r8d
	movl	$65, %edx
	movl	$408, %esi
	movl	$20, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L296
	.cfi_endproc
.LFE1497:
	.size	SSL_dup_CA_list, .-SSL_dup_CA_list
	.p2align 4
	.globl	SSL_set0_CA_list
	.type	SSL_set0_CA_list, @function
SSL_set0_CA_list:
.LFB1498:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	movq	X509_NAME_free@GOTPCREL(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	1472(%rdi), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	%r12, 1472(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1498:
	.size	SSL_set0_CA_list, .-SSL_set0_CA_list
	.p2align 4
	.globl	SSL_CTX_set0_CA_list
	.type	SSL_CTX_set0_CA_list, @function
SSL_CTX_set0_CA_list:
.LFB1499:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	movq	X509_NAME_free@GOTPCREL(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	280(%rdi), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	%r12, 280(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1499:
	.size	SSL_CTX_set0_CA_list, .-SSL_CTX_set0_CA_list
	.p2align 4
	.globl	SSL_CTX_get0_CA_list
	.type	SSL_CTX_get0_CA_list, @function
SSL_CTX_get0_CA_list:
.LFB1500:
	.cfi_startproc
	endbr64
	movq	280(%rdi), %rax
	ret
	.cfi_endproc
.LFE1500:
	.size	SSL_CTX_get0_CA_list, .-SSL_CTX_get0_CA_list
	.p2align 4
	.globl	SSL_get0_CA_list
	.type	SSL_get0_CA_list, @function
SSL_get0_CA_list:
.LFB1501:
	.cfi_startproc
	endbr64
	movq	1472(%rdi), %rax
	testq	%rax, %rax
	je	.L312
	ret
	.p2align 4,,10
	.p2align 3
.L312:
	movq	1440(%rdi), %rax
	movq	280(%rax), %rax
	ret
	.cfi_endproc
.LFE1501:
	.size	SSL_get0_CA_list, .-SSL_get0_CA_list
	.p2align 4
	.globl	SSL_CTX_set_client_CA_list
	.type	SSL_CTX_set_client_CA_list, @function
SSL_CTX_set_client_CA_list:
.LFB1502:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	movq	X509_NAME_free@GOTPCREL(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	288(%rdi), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	%r12, 288(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1502:
	.size	SSL_CTX_set_client_CA_list, .-SSL_CTX_set_client_CA_list
	.p2align 4
	.globl	SSL_CTX_get_client_CA_list
	.type	SSL_CTX_get_client_CA_list, @function
SSL_CTX_get_client_CA_list:
.LFB1503:
	.cfi_startproc
	endbr64
	movq	288(%rdi), %rax
	ret
	.cfi_endproc
.LFE1503:
	.size	SSL_CTX_get_client_CA_list, .-SSL_CTX_get_client_CA_list
	.p2align 4
	.globl	SSL_set_client_CA_list
	.type	SSL_set_client_CA_list, @function
SSL_set_client_CA_list:
.LFB1504:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	movq	X509_NAME_free@GOTPCREL(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	1480(%rdi), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	%r12, 1480(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1504:
	.size	SSL_set_client_CA_list, .-SSL_set_client_CA_list
	.p2align 4
	.globl	SSL_get0_peer_CA_list
	.type	SSL_get0_peer_CA_list, @function
SSL_get0_peer_CA_list:
.LFB1505:
	.cfi_startproc
	endbr64
	movq	168(%rdi), %rax
	testq	%rax, %rax
	je	.L318
	movq	608(%rax), %rax
.L318:
	ret
	.cfi_endproc
.LFE1505:
	.size	SSL_get0_peer_CA_list, .-SSL_get0_peer_CA_list
	.p2align 4
	.globl	SSL_get_client_CA_list
	.type	SSL_get_client_CA_list, @function
SSL_get_client_CA_list:
.LFB1506:
	.cfi_startproc
	endbr64
	movl	56(%rdi), %eax
	testl	%eax, %eax
	je	.L329
	movq	1480(%rdi), %rax
	testq	%rax, %rax
	je	.L330
.L323:
	ret
	.p2align 4,,10
	.p2align 3
.L329:
	movq	168(%rdi), %rax
	testq	%rax, %rax
	je	.L323
	movq	608(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L330:
	movq	1440(%rdi), %rax
	movq	288(%rax), %rax
	ret
	.cfi_endproc
.LFE1506:
	.size	SSL_get_client_CA_list, .-SSL_get_client_CA_list
	.p2align 4
	.globl	SSL_add1_to_CA_list
	.type	SSL_add1_to_CA_list, @function
SSL_add1_to_CA_list:
.LFB1508:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L349
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	cmpq	$0, 1472(%rdi)
	je	.L334
.L337:
	movq	%r12, %rdi
	call	X509_get_subject_name@PLT
	movq	%rax, %rdi
	call	X509_NAME_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L335
	movq	1472(%rbx), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L350
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L334:
	.cfi_restore_state
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 1472(%rbx)
	testq	%rax, %rax
	jne	.L337
.L335:
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L350:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	%eax, -20(%rbp)
	call	X509_NAME_free@PLT
	movl	-20(%rbp), %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L349:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1508:
	.size	SSL_add1_to_CA_list, .-SSL_add1_to_CA_list
	.p2align 4
	.globl	SSL_CTX_add1_to_CA_list
	.type	SSL_CTX_add1_to_CA_list, @function
SSL_CTX_add1_to_CA_list:
.LFB1509:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L369
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	cmpq	$0, 280(%rdi)
	je	.L354
.L357:
	movq	%r12, %rdi
	call	X509_get_subject_name@PLT
	movq	%rax, %rdi
	call	X509_NAME_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L355
	movq	280(%rbx), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L370
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L354:
	.cfi_restore_state
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 280(%rbx)
	testq	%rax, %rax
	jne	.L357
.L355:
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L370:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	%eax, -20(%rbp)
	call	X509_NAME_free@PLT
	movl	-20(%rbp), %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L369:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1509:
	.size	SSL_CTX_add1_to_CA_list, .-SSL_CTX_add1_to_CA_list
	.p2align 4
	.globl	SSL_add_client_CA
	.type	SSL_add_client_CA, @function
SSL_add_client_CA:
.LFB1510:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L389
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	cmpq	$0, 1480(%rdi)
	je	.L374
.L377:
	movq	%r12, %rdi
	call	X509_get_subject_name@PLT
	movq	%rax, %rdi
	call	X509_NAME_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L375
	movq	1480(%rbx), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L390
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L374:
	.cfi_restore_state
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 1480(%rbx)
	testq	%rax, %rax
	jne	.L377
.L375:
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L390:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	%eax, -20(%rbp)
	call	X509_NAME_free@PLT
	movl	-20(%rbp), %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L389:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1510:
	.size	SSL_add_client_CA, .-SSL_add_client_CA
	.p2align 4
	.globl	SSL_CTX_add_client_CA
	.type	SSL_CTX_add_client_CA, @function
SSL_CTX_add_client_CA:
.LFB1511:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L409
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	cmpq	$0, 288(%rdi)
	je	.L394
.L397:
	movq	%r12, %rdi
	call	X509_get_subject_name@PLT
	movq	%rax, %rdi
	call	X509_NAME_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L395
	movq	288(%rbx), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L410
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L394:
	.cfi_restore_state
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 288(%rbx)
	testq	%rax, %rax
	jne	.L397
.L395:
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L410:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	%eax, -20(%rbp)
	call	X509_NAME_free@PLT
	movl	-20(%rbp), %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L409:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1511:
	.size	SSL_CTX_add_client_CA, .-SSL_CTX_add_client_CA
	.p2align 4
	.globl	SSL_load_client_CA_file
	.type	SSL_load_client_CA_file, @function
SSL_load_client_CA_file:
.LFB1515:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	leaq	xname_cmp(%rip), %rsi
	leaq	xname_hash(%rip), %rdi
	movq	$0, -64(%rbp)
	movq	%rax, %r15
	call	OPENSSL_LH_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L423
	testq	%r15, %r15
	je	.L423
	movq	%r12, %rcx
	movl	$3, %edx
	movl	$108, %esi
	movq	%r15, %rdi
	call	BIO_ctrl@PLT
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	testl	%eax, %eax
	je	.L414
	leaq	-64(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L415:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	PEM_read_bio_X509@PLT
	testq	%rax, %rax
	je	.L416
	testq	%r13, %r13
	je	.L434
.L417:
	movq	-64(%rbp), %rdi
	call	X509_get_subject_name@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L414
	movq	%rax, %rdi
	call	X509_NAME_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L414
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	OPENSSL_LH_retrieve@PLT
	testq	%rax, %rax
	je	.L418
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	X509_NAME_free@PLT
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L434:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L417
	movl	$626, %r8d
	movl	$65, %edx
	movl	$185, %esi
	movl	$20, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L414:
	movq	%r12, %rdi
	call	X509_NAME_free@PLT
	movq	X509_NAME_free@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	OPENSSL_sk_pop_free@PLT
	movq	%r15, %rdi
	call	BIO_free@PLT
	movq	-64(%rbp), %rdi
	call	X509_free@PLT
	movq	%r14, %rdi
	call	OPENSSL_LH_free@PLT
.L411:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L435
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L418:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	OPENSSL_LH_insert@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L415
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L416:
	movq	%r15, %rdi
	call	BIO_free@PLT
	movq	-64(%rbp), %rdi
	call	X509_free@PLT
	movq	%r14, %rdi
	call	OPENSSL_LH_free@PLT
	testq	%r13, %r13
	je	.L411
	call	ERR_clear_error@PLT
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L423:
	movl	$65, %edx
	movl	$185, %esi
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	movl	$613, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L414
.L435:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1515:
	.size	SSL_load_client_CA_file, .-SSL_load_client_CA_file
	.p2align 4
	.globl	SSL_add_file_cert_subjects_to_stack
	.type	SSL_add_file_cert_subjects_to_stack, @function
SSL_add_file_cert_subjects_to_stack:
.LFB1516:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	leaq	xname_sk_cmp(%rip), %rsi
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	call	OPENSSL_sk_set_cmp_func@PLT
	movq	%rax, %r15
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L454
	movq	%r12, %rcx
	movl	$3, %edx
	movl	$108, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	leaq	-64(%rbp), %rbx
	testl	%eax, %eax
	je	.L441
	.p2align 4,,10
	.p2align 3
.L439:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	PEM_read_bio_X509@PLT
	testq	%rax, %rax
	je	.L440
	movq	-64(%rbp), %rdi
	call	X509_get_subject_name@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L441
	call	X509_NAME_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L441
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_find@PLT
	testl	%eax, %eax
	js	.L442
	movq	%r12, %rdi
	call	X509_NAME_free@PLT
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L442:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L439
	movq	%r12, %rdi
	movl	%eax, -68(%rbp)
	call	X509_NAME_free@PLT
	movl	-68(%rbp), %r8d
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L441:
	xorl	%r8d, %r8d
.L438:
	movq	%r14, %rdi
	movl	%r8d, -68(%rbp)
	call	BIO_free@PLT
	movq	-64(%rbp), %rdi
	call	X509_free@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_set_cmp_func@PLT
	movl	-68(%rbp), %r8d
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L455
	addq	$40, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L454:
	.cfi_restore_state
	movl	$675, %r8d
	movl	$65, %edx
	movl	$216, %esi
	movl	$20, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r8d, %r8d
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L440:
	call	ERR_clear_error@PLT
	movl	$1, %r8d
	jmp	.L438
.L455:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1516:
	.size	SSL_add_file_cert_subjects_to_stack, .-SSL_add_file_cert_subjects_to_stack
	.section	.rodata.str1.1
.LC4:
	.string	"%s/%s"
.LC5:
	.string	"')"
.LC6:
	.string	"OPENSSL_DIR_read(&ctx, '"
	.text
	.p2align 4
	.globl	SSL_add_dir_cert_subjects_to_stack
	.type	SSL_add_dir_cert_subjects_to_stack, @function
SSL_add_dir_cert_subjects_to_stack:
.LFB1517:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1096(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1088(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$1080, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -1112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -1096(%rbp)
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L462:
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%rbx, %rdi
	movq	%rax, %r14
	call	strlen@PLT
	leaq	2(%r14,%rax), %rax
	cmpq	$1024, %rax
	ja	.L472
	movq	%rbx, %r8
	movq	%r12, %rcx
	leaq	.LC4(%rip), %rdx
	movq	%r13, %rdi
	movl	$1024, %esi
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	subl	$1, %eax
	cmpl	$1022, %eax
	ja	.L471
	movq	-1112(%rbp), %rdi
	movq	%r13, %rsi
	call	SSL_add_file_cert_subjects_to_stack
	testl	%eax, %eax
	je	.L471
.L457:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	OPENSSL_DIR_read@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	jne	.L462
	call	__errno_location@PLT
	movl	$1, %r13d
	movl	(%rax), %edx
	testl	%edx, %edx
	je	.L461
	movl	$741, %r8d
	movl	$10, %esi
	movl	$2, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdx
	movl	$3, %edi
	xorl	%eax, %eax
	leaq	.LC5(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	call	ERR_add_error_data@PLT
	movl	$743, %r8d
	movl	$2, %edx
	leaq	.LC1(%rip), %rcx
	movl	$215, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L471:
	xorl	%r13d, %r13d
.L461:
	cmpq	$0, -1096(%rbp)
	je	.L456
	movq	%r15, %rdi
	call	OPENSSL_DIR_end@PLT
.L456:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L473
	addq	$1080, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L472:
	.cfi_restore_state
	movl	$725, %r8d
	movl	$270, %edx
	movl	$215, %esi
	movl	$20, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L471
.L473:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1517:
	.size	SSL_add_dir_cert_subjects_to_stack, .-SSL_add_dir_cert_subjects_to_stack
	.section	.rodata.str1.1
.LC7:
	.string	"Verify error:"
	.text
	.p2align 4
	.globl	ssl_build_cert_chain
	.type	ssl_build_cert_chain, @function
ssl_build_cert_chain:
.LFB1518:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -64(%rbp)
	movq	%rsi, -56(%rbp)
	testq	%rdi, %rdi
	je	.L475
	movq	1168(%rdi), %r15
.L476:
	movq	(%r15), %rbx
	movl	%r14d, %eax
	andl	$4, %eax
	movq	(%rbx), %r13
	movl	%eax, -76(%rbp)
	testq	%r13, %r13
	je	.L519
	movl	-76(%rbp), %edx
	testl	%edx, %edx
	jne	.L520
	movq	456(%r15), %r13
	testq	%r13, %r13
	je	.L521
.L484:
	xorl	%r12d, %r12d
	testb	$1, %r14b
	jne	.L522
.L483:
	call	X509_STORE_CTX_new@PLT
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	je	.L523
.L486:
	movq	(%rbx), %rdx
	movq	%r12, %rcx
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	X509_STORE_CTX_init@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L524
	movl	28(%r15), %esi
	movq	-72(%rbp), %r15
	andl	$196608, %esi
	movq	%r15, %rdi
	call	X509_STORE_CTX_set_flags@PLT
	movq	%r15, %rdi
	call	X509_verify_cert@PLT
	testl	%eax, %eax
	jle	.L525
	movq	-72(%rbp), %rdi
	xorl	%r12d, %r12d
	call	X509_STORE_CTX_get1_chain@PLT
	movq	%rax, %r15
.L491:
	movq	%r15, %rdi
	call	OPENSSL_sk_shift@PLT
	movq	%rax, %rdi
	call	X509_free@PLT
	andl	$2, %r14d
	jne	.L526
.L493:
	xorl	%r14d, %r14d
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L497:
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rdi
	xorl	%r8d, %r8d
	movq	%rax, %rdx
	xorl	%ecx, %ecx
	call	ssl_security_cert@PLT
	movl	%eax, %r12d
	cmpl	$1, %eax
	jne	.L527
	addl	$1, %r14d
.L495:
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r14d
	jl	.L497
	movq	16(%rbx), %rdi
	movq	X509_free@GOTPCREL(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	testl	%r12d, %r12d
	movl	$1, %eax
	movq	%r15, 16(%rbx)
	cmove	%eax, %r12d
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L522:
	movq	16(%rbx), %r12
	call	X509_STORE_CTX_new@PLT
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	jne	.L486
.L523:
	movl	$798, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$332, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L475:
	movq	320(%rsi), %r15
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L520:
	call	X509_STORE_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L480
	xorl	%r12d, %r12d
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L482:
	movq	16(%rbx), %rdi
	movl	%r12d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	X509_STORE_add_cert@PLT
	testl	%eax, %eax
	je	.L480
	addl	$1, %r12d
.L481:
	movq	16(%rbx), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jl	.L482
	movq	(%rbx), %rsi
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	call	X509_STORE_add_cert@PLT
	testl	%eax, %eax
	jne	.L483
	.p2align 4,,10
	.p2align 3
.L480:
	movq	$0, -72(%rbp)
	xorl	%r12d, %r12d
.L499:
	movq	%r13, %rdi
	call	X509_STORE_free@PLT
.L498:
	movq	-72(%rbp), %rdi
	call	X509_STORE_CTX_free@PLT
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L524:
	.cfi_restore_state
	movl	$802, %r8d
	movl	$11, %edx
	movl	$332, %esi
	movl	$20, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
.L478:
	movl	-76(%rbp), %eax
	testl	%eax, %eax
	je	.L498
	jmp	.L499
	.p2align 4,,10
	.p2align 3
.L521:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L485
	movq	1440(%rax), %rax
	xorl	%r12d, %r12d
	movq	32(%rax), %r13
	testb	$1, %r14b
	je	.L483
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L526:
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jle	.L493
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	movq	%r15, %rdi
	leal	-1(%rax), %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	call	X509_get_extension_flags@PLT
	testb	$32, %ah
	je	.L493
	movq	%r15, %rdi
	call	OPENSSL_sk_pop@PLT
	movq	%rax, %rdi
	call	X509_free@PLT
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L525:
	movl	%r14d, %r12d
	andl	$8, %r12d
	je	.L489
	testb	$16, %r14b
	jne	.L490
	movq	%r15, %rdi
	movl	$2, %r12d
	call	X509_STORE_CTX_get1_chain@PLT
	movq	%rax, %r15
	jmp	.L491
	.p2align 4,,10
	.p2align 3
.L527:
	movl	$847, %r8d
	movl	%eax, %edx
	movl	$332, %esi
	xorl	%r12d, %r12d
	leaq	.LC1(%rip), %rcx
	movl	$20, %edi
	call	ERR_put_error@PLT
	movq	X509_free@GOTPCREL(%rip), %rsi
	movq	%r15, %rdi
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L489:
	movl	$819, %r8d
	movl	$134, %edx
	movl	$332, %esi
	movl	$20, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-72(%rbp), %rdi
	call	X509_STORE_CTX_get_error@PLT
	movslq	%eax, %rdi
	call	X509_verify_cert_error_string@PLT
	leaq	.LC7(%rip), %rsi
	movl	$2, %edi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	ERR_add_error_data@PLT
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L519:
	movl	$768, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$179, %edx
	xorl	%r12d, %r12d
	movl	$332, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	movq	$0, -72(%rbp)
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L485:
	movq	-56(%rbp), %rax
	movq	32(%rax), %r13
	jmp	.L484
.L490:
	call	ERR_clear_error@PLT
	movq	-72(%rbp), %rdi
	movl	$2, %r12d
	call	X509_STORE_CTX_get1_chain@PLT
	movq	%rax, %r15
	jmp	.L491
	.cfi_endproc
.LFE1518:
	.size	ssl_build_cert_chain, .-ssl_build_cert_chain
	.p2align 4
	.globl	ssl_cert_set_cert_store
	.type	ssl_cert_set_cert_store, @function
ssl_cert_set_cert_store:
.LFB1519:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%ecx, %ebx
	subq	$8, %rsp
	testl	%edx, %edx
	je	.L529
	leaq	456(%rdi), %r13
	movq	456(%rdi), %rdi
.L530:
	call	X509_STORE_free@PLT
	movq	%r12, 0(%r13)
	testl	%ebx, %ebx
	je	.L531
	testq	%r12, %r12
	jne	.L539
.L531:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L529:
	.cfi_restore_state
	leaq	464(%rdi), %r13
	movq	464(%rdi), %rdi
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L539:
	movq	%r12, %rdi
	call	X509_STORE_up_ref@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1519:
	.size	ssl_cert_set_cert_store, .-ssl_cert_set_cert_store
	.p2align 4
	.globl	ssl_security
	.type	ssl_security, @function
ssl_security:
.LFB1521:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	1168(%rdi), %rax
	movq	%r8, %r9
	movl	%ecx, %r8d
	movl	%edx, %ecx
	movl	%esi, %edx
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	504(%rax)
	call	*488(%rax)
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1521:
	.size	ssl_security, .-ssl_security
	.p2align 4
	.globl	ssl_ctx_security
	.type	ssl_ctx_security, @function
ssl_ctx_security:
.LFB1522:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %r9
	movl	%ecx, %r8d
	movl	%edx, %ecx
	movl	%esi, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -24
	movq	320(%rdi), %rax
	xorl	%edi, %edi
	pushq	504(%rax)
	call	*488(%rax)
	movq	-8(%rbp), %r12
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1522:
	.size	ssl_ctx_security, .-ssl_ctx_security
	.p2align 4
	.globl	ssl_cert_lookup_by_nid
	.type	ssl_cert_lookup_by_nid, @function
ssl_cert_lookup_by_nid:
.LFB1523:
	.cfi_startproc
	endbr64
	cmpl	$6, %edi
	je	.L547
	cmpl	$912, %edi
	je	.L548
	cmpl	$116, %edi
	je	.L549
	cmpl	$408, %edi
	je	.L550
	cmpl	$811, %edi
	je	.L551
	cmpl	$979, %edi
	je	.L552
	cmpl	$980, %edi
	je	.L553
	cmpl	$1087, %edi
	je	.L554
	xorl	%eax, %eax
	cmpl	$1088, %edi
	je	.L556
	ret
.L547:
	xorl	%eax, %eax
.L545:
	movq	%rax, (%rsi)
	movl	$1, %eax
	ret
.L548:
	movl	$1, %eax
	jmp	.L545
.L556:
	movl	$8, %eax
	jmp	.L545
.L549:
	movl	$2, %eax
	jmp	.L545
.L550:
	movl	$3, %eax
	jmp	.L545
.L551:
	movl	$4, %eax
	jmp	.L545
.L552:
	movl	$5, %eax
	jmp	.L545
.L553:
	movl	$6, %eax
	jmp	.L545
.L554:
	movl	$7, %eax
	jmp	.L545
	.cfi_endproc
.LFE1523:
	.size	ssl_cert_lookup_by_nid, .-ssl_cert_lookup_by_nid
	.p2align 4
	.globl	ssl_cert_lookup_by_pkey
	.type	ssl_cert_lookup_by_pkey, @function
ssl_cert_lookup_by_pkey:
.LFB1524:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	EVP_PKEY_id@PLT
	testl	%eax, %eax
	je	.L561
	cmpl	$6, %eax
	je	.L562
	cmpl	$912, %eax
	je	.L563
	cmpl	$116, %eax
	je	.L564
	cmpl	$408, %eax
	je	.L565
	cmpl	$811, %eax
	je	.L566
	cmpl	$979, %eax
	je	.L567
	cmpl	$980, %eax
	je	.L568
	cmpl	$1087, %eax
	je	.L569
	xorl	%r8d, %r8d
	cmpl	$1088, %eax
	je	.L575
	addq	$8, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L561:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%r8d, %r8d
	movq	%r8, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L575:
	.cfi_restore_state
	movl	$8, %eax
	.p2align 4,,10
	.p2align 3
.L559:
	testq	%rbx, %rbx
	je	.L560
	movq	%rax, (%rbx)
.L560:
	leaq	ssl_cert_info(%rip), %rdx
	addq	$8, %rsp
	leaq	(%rdx,%rax,8), %r8
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	%r8, %rax
	ret
.L562:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L559
.L563:
	movl	$1, %eax
	jmp	.L559
.L564:
	movl	$2, %eax
	jmp	.L559
.L565:
	movl	$3, %eax
	jmp	.L559
.L566:
	movl	$4, %eax
	jmp	.L559
.L567:
	movl	$5, %eax
	jmp	.L559
.L568:
	movl	$6, %eax
	jmp	.L559
.L569:
	movl	$7, %eax
	jmp	.L559
	.cfi_endproc
.LFE1524:
	.size	ssl_cert_lookup_by_pkey, .-ssl_cert_lookup_by_pkey
	.p2align 4
	.globl	ssl_cert_lookup_by_idx
	.type	ssl_cert_lookup_by_idx, @function
ssl_cert_lookup_by_idx:
.LFB1525:
	.cfi_startproc
	endbr64
	cmpq	$8, %rdi
	ja	.L578
	leaq	ssl_cert_info(%rip), %rax
	leaq	(%rax,%rdi,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L578:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1525:
	.size	ssl_cert_lookup_by_idx, .-ssl_cert_lookup_by_idx
	.section	.rodata
	.align 16
	.type	minbits_table.26886, @object
	.size	minbits_table.26886, 20
minbits_table.26886:
	.long	80
	.long	112
	.long	128
	.long	192
	.long	256
	.local	ssl_x509_store_ctx_init_ossl_ret_
	.comm	ssl_x509_store_ctx_init_ossl_ret_,4,4
	.data
	.align 4
	.type	ssl_x509_store_ctx_idx, @object
	.size	ssl_x509_store_ctx_idx, 4
ssl_x509_store_ctx_idx:
	.long	-1
	.local	ssl_x509_store_ctx_once
	.comm	ssl_x509_store_ctx_once,4,4
	.section	.rodata
	.align 32
	.type	ssl_cert_info, @object
	.size	ssl_cert_info, 72
ssl_cert_info:
	.long	6
	.long	1
	.long	912
	.long	1
	.long	116
	.long	2
	.long	408
	.long	8
	.long	811
	.long	32
	.long	979
	.long	128
	.long	980
	.long	128
	.long	1087
	.long	8
	.long	1088
	.long	8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
