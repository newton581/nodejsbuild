	.file	"p5_pbev2.c"
	.text
	.p2align 4
	.globl	d2i_PBE2PARAM
	.type	d2i_PBE2PARAM, @function
d2i_PBE2PARAM:
.LFB803:
	.cfi_startproc
	endbr64
	leaq	PBE2PARAM_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE803:
	.size	d2i_PBE2PARAM, .-d2i_PBE2PARAM
	.p2align 4
	.globl	i2d_PBE2PARAM
	.type	i2d_PBE2PARAM, @function
i2d_PBE2PARAM:
.LFB804:
	.cfi_startproc
	endbr64
	leaq	PBE2PARAM_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE804:
	.size	i2d_PBE2PARAM, .-i2d_PBE2PARAM
	.p2align 4
	.globl	PBE2PARAM_new
	.type	PBE2PARAM_new, @function
PBE2PARAM_new:
.LFB805:
	.cfi_startproc
	endbr64
	leaq	PBE2PARAM_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE805:
	.size	PBE2PARAM_new, .-PBE2PARAM_new
	.p2align 4
	.globl	PBE2PARAM_free
	.type	PBE2PARAM_free, @function
PBE2PARAM_free:
.LFB806:
	.cfi_startproc
	endbr64
	leaq	PBE2PARAM_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE806:
	.size	PBE2PARAM_free, .-PBE2PARAM_free
	.p2align 4
	.globl	d2i_PBKDF2PARAM
	.type	d2i_PBKDF2PARAM, @function
d2i_PBKDF2PARAM:
.LFB807:
	.cfi_startproc
	endbr64
	leaq	PBKDF2PARAM_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE807:
	.size	d2i_PBKDF2PARAM, .-d2i_PBKDF2PARAM
	.p2align 4
	.globl	i2d_PBKDF2PARAM
	.type	i2d_PBKDF2PARAM, @function
i2d_PBKDF2PARAM:
.LFB808:
	.cfi_startproc
	endbr64
	leaq	PBKDF2PARAM_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE808:
	.size	i2d_PBKDF2PARAM, .-i2d_PBKDF2PARAM
	.p2align 4
	.globl	PBKDF2PARAM_new
	.type	PBKDF2PARAM_new, @function
PBKDF2PARAM_new:
.LFB809:
	.cfi_startproc
	endbr64
	leaq	PBKDF2PARAM_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE809:
	.size	PBKDF2PARAM_new, .-PBKDF2PARAM_new
	.p2align 4
	.globl	PBKDF2PARAM_free
	.type	PBKDF2PARAM_free, @function
PBKDF2PARAM_free:
.LFB810:
	.cfi_startproc
	endbr64
	leaq	PBKDF2PARAM_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE810:
	.size	PBKDF2PARAM_free, .-PBKDF2PARAM_free
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/asn1/p5_pbev2.c"
	.text
	.p2align 4
	.globl	PKCS5_pbkdf2_set
	.type	PKCS5_pbkdf2_set, @function
PKCS5_pbkdf2_set:
.LFB813:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edi, %ebx
	leaq	PBKDF2PARAM_it(%rip), %rdi
	subq	$40, %rsp
	movl	%r8d, -52(%rbp)
	call	ASN1_item_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L11
	call	ASN1_OCTET_STRING_new@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L11
	movq	(%r12), %rax
	movslq	%r13d, %r8
	movq	%rcx, 8(%rax)
	movl	$4, (%rax)
	testl	%r13d, %r13d
	je	.L55
.L12:
	movq	%r8, %rdi
	movl	$166, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rcx, -64(%rbp)
	movq	%r8, -72(%rbp)
	call	CRYPTO_malloc@PLT
	movq	-64(%rbp), %rcx
	movq	%rax, %rdi
	movq	%rax, 8(%rcx)
	testq	%rax, %rax
	je	.L11
	movl	%r13d, (%rcx)
	testq	%r14, %r14
	je	.L13
	movq	-72(%rbp), %r8
	movq	%r14, %rsi
	movq	%r8, %rdx
	call	memcpy@PLT
.L14:
	testl	%ebx, %ebx
	movl	$2048, %eax
	movq	8(%r12), %rdi
	cmovle	%eax, %ebx
	movslq	%ebx, %rsi
	call	ASN1_INTEGER_set@PLT
	testl	%eax, %eax
	je	.L11
	movl	-52(%rbp), %eax
	testl	%eax, %eax
	jg	.L16
.L20:
	testl	%r15d, %r15d
	jle	.L19
	cmpl	$163, %r15d
	je	.L19
	call	X509_ALGOR_new@PLT
	movq	%rax, 24(%r12)
	testq	%rax, %rax
	je	.L11
	movl	%r15d, %edi
	call	OBJ_nid2obj@PLT
	movq	24(%r12), %rdi
	xorl	%ecx, %ecx
	movl	$5, %edx
	movq	%rax, %rsi
	call	X509_ALGOR_set0@PLT
.L19:
	call	X509_ALGOR_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L11
	movl	$69, %edi
	call	OBJ_nid2obj@PLT
	leaq	8(%r13), %rdx
	movq	%r12, %rsi
	leaq	PBKDF2PARAM_it(%rip), %rdi
	movq	%rax, 0(%r13)
	call	ASN1_TYPE_pack_sequence@PLT
	testq	%rax, %rax
	je	.L21
	leaq	PBKDF2PARAM_it(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_item_free@PLT
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L16:
	call	ASN1_INTEGER_new@PLT
	movq	%rax, 16(%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L11
	movslq	-52(%rbp), %rsi
	call	ASN1_INTEGER_set@PLT
	testl	%eax, %eax
	jne	.L20
	.p2align 4,,10
	.p2align 3
.L11:
	xorl	%r13d, %r13d
.L21:
	movl	$217, %r8d
	movl	$65, %edx
	movl	$219, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	leaq	PBKDF2PARAM_it(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_item_free@PLT
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	X509_ALGOR_free@PLT
.L10:
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	movl	$8, %r8d
	movl	$8, %r13d
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L13:
	movl	%r13d, %esi
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jg	.L14
	jmp	.L11
	.cfi_endproc
.LFE813:
	.size	PKCS5_pbkdf2_set, .-PKCS5_pbkdf2_set
	.p2align 4
	.globl	PKCS5_pbe2_set_iv
	.type	PKCS5_pbe2_set_iv, @function
PKCS5_pbe2_set_iv:
.LFB811:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movl	%esi, -88(%rbp)
	movq	%rdx, -96(%rbp)
	movl	%ecx, -100(%rbp)
	movl	%r9d, -84(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_CIPHER_type@PLT
	testl	%eax, %eax
	je	.L94
	leaq	PBE2PARAM_it(%rip), %rdi
	movl	%eax, %ebx
	call	ASN1_item_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L59
	movq	8(%rax), %r15
	movl	%ebx, %edi
	call	OBJ_nid2obj@PLT
	movq	%rax, (%r15)
	call	ASN1_TYPE_new@PLT
	movq	%rax, 8(%r15)
	testq	%rax, %rax
	je	.L59
	movq	%r14, %rdi
	call	EVP_CIPHER_iv_length@PLT
	testl	%eax, %eax
	je	.L61
	movq	%r14, %rdi
	testq	%r12, %r12
	je	.L62
	call	EVP_CIPHER_iv_length@PLT
	leaq	-80(%rbp), %rdi
	movl	$16, %ecx
	movq	%r12, %rsi
	movslq	%eax, %rdx
	call	__memcpy_chk@PLT
.L61:
	call	EVP_CIPHER_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L59
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	leaq	-80(%rbp), %r8
	movq	%rax, %rdi
	call	EVP_CipherInit_ex@PLT
	testl	%eax, %eax
	je	.L73
	movq	8(%r15), %rsi
	movq	%r12, %rdi
	call	EVP_CIPHER_param_to_asn1@PLT
	testl	%eax, %eax
	jle	.L95
	cmpl	$-1, -84(%rbp)
	je	.L96
.L66:
	movq	%r12, %rdi
	movl	$-1, %r12d
	call	EVP_CIPHER_CTX_free@PLT
	cmpl	$37, %ebx
	je	.L97
.L68:
	movq	0(%r13), %rdi
	call	X509_ALGOR_free@PLT
	movl	-84(%rbp), %ecx
	movl	-100(%rbp), %edx
	movl	%r12d, %r8d
	movq	-96(%rbp), %rsi
	movl	-88(%rbp), %edi
	call	PKCS5_pbkdf2_set
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	je	.L59
	call	X509_ALGOR_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L59
	movl	$161, %edi
	call	OBJ_nid2obj@PLT
	leaq	8(%r14), %rdx
	movq	%r13, %rsi
	leaq	PBE2PARAM_it(%rip), %rdi
	movq	%rax, (%r14)
	call	ASN1_TYPE_pack_sequence@PLT
	testq	%rax, %rax
	je	.L69
	leaq	PBE2PARAM_it(%rip), %rsi
	movq	%r13, %rdi
	call	ASN1_item_free@PLT
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L59:
	xorl	%r14d, %r14d
.L69:
	movl	$132, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$167, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
.L58:
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_free@PLT
	leaq	PBE2PARAM_it(%rip), %rsi
	movq	%r13, %rdi
	call	ASN1_item_free@PLT
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	X509_ALGOR_free@PLT
.L56:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L98
	addq	$72, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	movl	$108, %edx
	movl	$167, %esi
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	movl	$52, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$13, %edi
	xorl	%r14d, %r14d
	call	ERR_put_error@PLT
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L62:
	call	EVP_CIPHER_iv_length@PLT
	leaq	-80(%rbp), %rdi
	movl	%eax, %esi
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jg	.L61
	.p2align 4,,10
	.p2align 3
.L73:
	xorl	%r14d, %r14d
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L97:
	movq	%r14, %rdi
	call	EVP_CIPHER_key_length@PLT
	movl	%eax, %r12d
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L96:
	xorl	%edx, %edx
	leaq	-84(%rbp), %rcx
	movl	$7, %esi
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_ctrl@PLT
	testl	%eax, %eax
	jg	.L66
	call	ERR_clear_error@PLT
	movl	$799, -84(%rbp)
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L95:
	movl	$82, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$114, %edx
	xorl	%r14d, %r14d
	movl	$167, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
	jmp	.L58
.L98:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE811:
	.size	PKCS5_pbe2_set_iv, .-PKCS5_pbe2_set_iv
	.p2align 4
	.globl	PKCS5_pbe2_set
	.type	PKCS5_pbe2_set, @function
PKCS5_pbe2_set:
.LFB812:
	.cfi_startproc
	endbr64
	movl	$-1, %r9d
	xorl	%r8d, %r8d
	jmp	PKCS5_pbe2_set_iv
	.cfi_endproc
.LFE812:
	.size	PKCS5_pbe2_set, .-PKCS5_pbe2_set
	.globl	PBKDF2PARAM_it
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"PBKDF2PARAM"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	PBKDF2PARAM_it, @object
	.size	PBKDF2PARAM_it, 56
PBKDF2PARAM_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	PBKDF2PARAM_seq_tt
	.quad	4
	.quad	0
	.quad	32
	.quad	.LC1
	.section	.rodata.str1.1
.LC2:
	.string	"salt"
.LC3:
	.string	"iter"
.LC4:
	.string	"keylength"
.LC5:
	.string	"prf"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	PBKDF2PARAM_seq_tt, @object
	.size	PBKDF2PARAM_seq_tt, 160
PBKDF2PARAM_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC2
	.quad	ASN1_ANY_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC3
	.quad	ASN1_INTEGER_it
	.quad	1
	.quad	0
	.quad	16
	.quad	.LC4
	.quad	ASN1_INTEGER_it
	.quad	1
	.quad	0
	.quad	24
	.quad	.LC5
	.quad	X509_ALGOR_it
	.globl	PBE2PARAM_it
	.section	.rodata.str1.1
.LC6:
	.string	"PBE2PARAM"
	.section	.data.rel.ro.local
	.align 32
	.type	PBE2PARAM_it, @object
	.size	PBE2PARAM_it, 56
PBE2PARAM_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	PBE2PARAM_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC6
	.section	.rodata.str1.1
.LC7:
	.string	"keyfunc"
.LC8:
	.string	"encryption"
	.section	.data.rel.ro
	.align 32
	.type	PBE2PARAM_seq_tt, @object
	.size	PBE2PARAM_seq_tt, 80
PBE2PARAM_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC7
	.quad	X509_ALGOR_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC8
	.quad	X509_ALGOR_it
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
