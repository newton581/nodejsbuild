	.file	"sha256.c"
	.text
	.p2align 4
	.type	SHA256_Update.part.0, @function
SHA256_Update.part.0:
.LFB160:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	0(,%rdx,8), %eax
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	addl	32(%rdi), %eax
	movl	36(%rdi), %edx
	setc	%cl
	movl	104(%rdi), %r14d
	movl	%eax, 32(%rdi)
	cmpl	$1, %ecx
	movq	%r12, %rcx
	sbbl	$-1, %edx
	shrq	$29, %rcx
	addl	%ecx, %edx
	movl	%edx, 36(%rdi)
	testq	%r14, %r14
	je	.L5
	leaq	40(%rdi), %rcx
	leaq	(%rcx,%r14), %rdi
	cmpq	$63, %r12
	ja	.L6
	leaq	(%r12,%r14), %rax
	cmpq	$63, %rax
	jbe	.L7
.L6:
	movl	$64, %r15d
	subq	%r14, %r15
	cmpq	$8, %r15
	jnb	.L8
	testb	$4, %r15b
	jne	.L33
	testq	%r15, %r15
	je	.L9
	movzbl	0(%r13), %eax
	movb	%al, (%rdi)
	testb	$2, %r15b
	jne	.L34
.L9:
	movq	%rcx, %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%rcx, -56(%rbp)
	call	sha256_block_data_order@PLT
	movq	-56(%rbp), %rcx
	pxor	%xmm0, %xmm0
	addq	%r15, %r13
	movl	$0, 104(%rbx)
	leaq	-64(%r14,%r12), %r12
	movups	%xmm0, 40(%rbx)
	movups	%xmm0, 16(%rcx)
	movups	%xmm0, 32(%rcx)
	movups	%xmm0, 48(%rcx)
.L5:
	cmpq	$63, %r12
	ja	.L35
.L15:
	testq	%r12, %r12
	jne	.L36
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	movl	%r12d, 104(%rbx)
	addq	$24, %rsp
	leaq	40(%rbx), %rdi
	movq	%r12, %rdx
	popq	%rbx
	movq	%r13, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	memcpy@PLT
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	shrq	$6, %rdx
	call	sha256_block_data_order@PLT
	movq	%r12, %rax
	andq	$-64, %rax
	addq	%rax, %r13
	subq	%rax, %r12
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L8:
	movq	0(%r13), %rax
	leaq	8(%rdi), %rdx
	movq	%r13, %r8
	andq	$-8, %rdx
	movq	%rax, (%rdi)
	movq	-8(%r13,%r15), %rax
	movq	%rax, -8(%rdi,%r15)
	subq	%rdx, %rdi
	subq	%rdi, %r8
	addq	%r15, %rdi
	andq	$-8, %rdi
	cmpq	$8, %rdi
	jb	.L9
	andq	$-8, %rdi
	xorl	%eax, %eax
.L12:
	movq	(%r8,%rax), %rsi
	movq	%rsi, (%rdx,%rax)
	addq	$8, %rax
	cmpq	%rdi, %rax
	jb	.L12
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%r12, %rdx
	call	memcpy@PLT
	addl	%r12d, 104(%rbx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L33:
	movl	0(%r13), %eax
	movl	%eax, (%rdi)
	movl	-4(%r13,%r15), %eax
	movl	%eax, -4(%rdi,%r15)
	jmp	.L9
.L34:
	movzwl	-2(%r13,%r15), %eax
	movw	%ax, -2(%rdi,%r15)
	jmp	.L9
	.cfi_endproc
.LFE160:
	.size	SHA256_Update.part.0, .-SHA256_Update.part.0
	.p2align 4
	.globl	SHA224_Init
	.type	SHA224_Init, @function
SHA224_Init:
.LFB151:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1, %eax
	movups	%xmm0, 96(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm0, 64(%rdi)
	movups	%xmm0, 80(%rdi)
	movdqa	.LC0(%rip), %xmm0
	movl	$28, 108(%rdi)
	movups	%xmm0, (%rdi)
	movdqa	.LC1(%rip), %xmm0
	movups	%xmm0, 16(%rdi)
	ret
	.cfi_endproc
.LFE151:
	.size	SHA224_Init, .-SHA224_Init
	.p2align 4
	.globl	SHA256_Init
	.type	SHA256_Init, @function
SHA256_Init:
.LFB152:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1, %eax
	movups	%xmm0, 96(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm0, 64(%rdi)
	movups	%xmm0, 80(%rdi)
	movdqa	.LC2(%rip), %xmm0
	movl	$32, 108(%rdi)
	movups	%xmm0, (%rdi)
	movdqa	.LC3(%rip), %xmm0
	movups	%xmm0, 16(%rdi)
	ret
	.cfi_endproc
.LFE152:
	.size	SHA256_Init, .-SHA256_Init
	.p2align 4
	.globl	SHA224_Update
	.type	SHA224_Update, @function
SHA224_Update:
.LFB155:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L73
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	0(,%rdx,8), %eax
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	addl	32(%rdi), %eax
	movl	36(%rdi), %edx
	setc	%cl
	movl	104(%rdi), %r14d
	movl	%eax, 32(%rdi)
	cmpl	$1, %ecx
	movq	%r12, %rcx
	sbbl	$-1, %edx
	shrq	$29, %rcx
	addl	%ecx, %edx
	movl	%edx, 36(%rdi)
	testq	%r14, %r14
	jne	.L76
	cmpq	$63, %r12
	ja	.L54
.L57:
	movl	%r15d, 104(%rbx)
	leaq	40(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
.L63:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	leaq	40(%rdi), %rcx
	leaq	(%rcx,%r14), %rdi
	cmpq	$63, %r12
	ja	.L46
	leaq	(%r12,%r14), %rax
	cmpq	$63, %rax
	jbe	.L47
.L46:
	movl	$64, %r15d
	subq	%r14, %r15
	cmpq	$8, %r15
	jnb	.L48
	testb	$4, %r15b
	jne	.L77
	testq	%r15, %r15
	je	.L49
	movzbl	0(%r13), %eax
	movb	%al, (%rdi)
	testb	$2, %r15b
	jne	.L78
.L49:
	movq	%rcx, %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%rcx, -56(%rbp)
	call	sha256_block_data_order@PLT
	movq	-56(%rbp), %rcx
	pxor	%xmm0, %xmm0
	addq	%r15, %r13
	leaq	-64(%r14,%r12), %r12
	movl	$0, 104(%rbx)
	movups	%xmm0, 40(%rbx)
	movups	%xmm0, 16(%rcx)
	movups	%xmm0, 32(%rcx)
	movups	%xmm0, 48(%rcx)
	cmpq	$63, %r12
	jbe	.L55
.L54:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	shrq	$6, %rdx
	call	sha256_block_data_order@PLT
	movq	%r12, %rax
	andq	$-64, %rax
	addq	%rax, %r13
	subq	%rax, %r12
.L55:
	testq	%r12, %r12
	je	.L63
	movl	%r12d, %r15d
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	0(%r13), %rax
	leaq	8(%rdi), %rdx
	movq	%r13, %r8
	andq	$-8, %rdx
	movq	%rax, (%rdi)
	movq	-8(%r13,%r15), %rax
	movq	%rax, -8(%rdi,%r15)
	subq	%rdx, %rdi
	subq	%rdi, %r8
	addq	%r15, %rdi
	andq	$-8, %rdi
	cmpq	$8, %rdi
	jb	.L49
	andq	$-8, %rdi
	xorl	%eax, %eax
.L52:
	movq	(%r8,%rax), %rsi
	movq	%rsi, (%rdx,%rax)
	addq	$8, %rax
	cmpq	%rdi, %rax
	jb	.L52
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L47:
	movq	%r12, %rdx
	call	memcpy@PLT
	addl	%r12d, 104(%rbx)
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L77:
	movl	0(%r13), %eax
	movl	%eax, (%rdi)
	movl	-4(%r13,%r15), %eax
	movl	%eax, -4(%rdi,%r15)
	jmp	.L49
.L78:
	movzwl	-2(%r13,%r15), %eax
	movw	%ax, -2(%rdi,%r15)
	jmp	.L49
	.cfi_endproc
.LFE155:
	.size	SHA224_Update, .-SHA224_Update
	.p2align 4
	.globl	SHA256_Update
	.type	SHA256_Update, @function
SHA256_Update:
.LFB157:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L113
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	0(,%rdx,8), %eax
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	addl	32(%rdi), %eax
	movl	36(%rdi), %edx
	setc	%cl
	movl	%eax, 32(%rdi)
	movl	104(%rdi), %eax
	cmpl	$1, %ecx
	movq	%r12, %rcx
	sbbl	$-1, %edx
	shrq	$29, %rcx
	addl	%ecx, %edx
	movl	%edx, 36(%rdi)
	testq	%rax, %rax
	jne	.L116
	cmpq	$63, %r12
	ja	.L94
.L97:
	movl	%r14d, 104(%rbx)
	leaq	40(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
.L103:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
	leaq	40(%rdi), %rcx
	leaq	(%r12,%rax), %r15
	leaq	(%rcx,%rax), %rdi
	cmpq	$63, %r12
	ja	.L86
	cmpq	$63, %r15
	jbe	.L87
.L86:
	movl	$64, %r12d
	subq	%rax, %r12
	cmpq	$8, %r12
	jnb	.L88
	testb	$4, %r12b
	jne	.L117
	testq	%r12, %r12
	je	.L89
	movzbl	0(%r13), %eax
	movb	%al, (%rdi)
	testb	$2, %r12b
	jne	.L118
.L89:
	movq	%rcx, %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%rcx, -56(%rbp)
	call	sha256_block_data_order@PLT
	movq	-56(%rbp), %rcx
	pxor	%xmm0, %xmm0
	addq	%r12, %r13
	leaq	-64(%r15), %r12
	movl	$0, 104(%rbx)
	movups	%xmm0, 40(%rbx)
	movups	%xmm0, 16(%rcx)
	movups	%xmm0, 32(%rcx)
	movups	%xmm0, 48(%rcx)
	cmpq	$63, %r12
	jbe	.L95
.L94:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	shrq	$6, %rdx
	call	sha256_block_data_order@PLT
	movq	%r12, %rax
	andq	$-64, %rax
	addq	%rax, %r13
	subq	%rax, %r12
.L95:
	testq	%r12, %r12
	je	.L103
	movl	%r12d, %r14d
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	0(%r13), %rax
	leaq	8(%rdi), %rdx
	movq	%r13, %r8
	andq	$-8, %rdx
	movq	%rax, (%rdi)
	movq	-8(%r13,%r12), %rax
	movq	%rax, -8(%rdi,%r12)
	subq	%rdx, %rdi
	subq	%rdi, %r8
	addq	%r12, %rdi
	andq	$-8, %rdi
	cmpq	$8, %rdi
	jb	.L89
	andq	$-8, %rdi
	xorl	%eax, %eax
.L92:
	movq	(%r8,%rax), %rsi
	movq	%rsi, (%rdx,%rax)
	addq	$8, %rax
	cmpq	%rdi, %rax
	jb	.L92
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L87:
	movq	%r12, %rdx
	call	memcpy@PLT
	addl	%r12d, 104(%rbx)
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L117:
	movl	0(%r13), %eax
	movl	%eax, (%rdi)
	movl	-4(%r13,%r12), %eax
	movl	%eax, -4(%rdi,%r12)
	jmp	.L89
.L118:
	movzwl	-2(%r13,%r12), %eax
	movw	%ax, -2(%rdi,%r12)
	jmp	.L89
	.cfi_endproc
.LFE157:
	.size	SHA256_Update, .-SHA256_Update
	.p2align 4
	.globl	SHA256_Transform
	.type	SHA256_Transform, @function
SHA256_Transform:
.LFB158:
	.cfi_startproc
	endbr64
	movl	$1, %edx
	jmp	sha256_block_data_order@PLT
	.cfi_endproc
.LFE158:
	.size	SHA256_Transform, .-SHA256_Transform
	.p2align 4
	.globl	SHA256_Final
	.type	SHA256_Final, @function
SHA256_Final:
.LFB159:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	40(%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movl	104(%rsi), %eax
	movb	$-128, 40(%rsi,%rax)
	addq	$1, %rax
	leaq	0(%r13,%rax), %rcx
	cmpq	$56, %rax
	ja	.L121
	movl	$56, %edx
	subq	%rax, %rdx
.L122:
	movl	%edx, %eax
	xorl	%edi, %edi
	cmpl	$8, %edx
	jnb	.L125
	andl	$4, %edx
	jne	.L153
	testl	%eax, %eax
	je	.L126
	movb	$0, (%rcx)
	testb	$2, %al
	jne	.L154
.L126:
	movq	32(%rbx), %rax
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	bswap	%rax
	movq	%rax, 96(%rbx)
	call	sha256_block_data_order@PLT
	movl	$0, 104(%rbx)
	movl	$64, %esi
	movq	%r13, %rdi
	call	OPENSSL_cleanse@PLT
	movl	108(%rbx), %eax
	cmpl	$28, %eax
	je	.L131
	cmpl	$32, %eax
	je	.L132
	movl	$0, %r8d
	ja	.L120
	shrl	$2, %eax
	je	.L135
	movl	(%rbx), %eax
	bswap	%eax
	movl	%eax, (%r12)
	cmpl	$7, 108(%rbx)
	jbe	.L135
	movl	4(%rbx), %eax
	bswap	%eax
	movl	%eax, 4(%r12)
	cmpl	$11, 108(%rbx)
	jbe	.L135
	movl	8(%rbx), %eax
	bswap	%eax
	movl	%eax, 8(%r12)
	cmpl	$15, 108(%rbx)
	jbe	.L135
	movl	12(%rbx), %eax
	bswap	%eax
	movl	%eax, 12(%r12)
	cmpl	$19, 108(%rbx)
	jbe	.L135
	movl	16(%rbx), %eax
	bswap	%eax
	movl	%eax, 16(%r12)
	cmpl	$23, 108(%rbx)
	jbe	.L135
	movl	20(%rbx), %eax
	bswap	%eax
	movl	%eax, 20(%r12)
	cmpl	$27, 108(%rbx)
	jbe	.L135
	movl	24(%rbx), %eax
	bswap	%eax
	movl	%eax, 24(%r12)
	cmpl	$31, 108(%rbx)
	jbe	.L135
	movl	28(%rbx), %eax
	bswap	%eax
	movl	%eax, 28(%r12)
.L135:
	movl	$1, %r8d
.L120:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_restore_state
	leaq	8(%rcx), %rsi
	movl	%edx, %eax
	movq	$0, (%rcx)
	movq	$0, -8(%rcx,%rax)
	andq	$-8, %rsi
	subq	%rsi, %rcx
	leal	(%rdx,%rcx), %eax
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L126
	andl	$-8, %eax
	xorl	%edx, %edx
.L129:
	movl	%edx, %ecx
	addl	$8, %edx
	movq	%rdi, (%rsi,%rcx)
	cmpl	%eax, %edx
	jb	.L129
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L131:
	movl	(%rbx), %eax
	movl	$1, %r8d
	bswap	%eax
	movl	%eax, (%r12)
	movl	4(%rbx), %eax
	bswap	%eax
	movl	%eax, 4(%r12)
	movl	8(%rbx), %eax
	bswap	%eax
	movl	%eax, 8(%r12)
	movl	12(%rbx), %eax
	bswap	%eax
	movl	%eax, 12(%r12)
	movl	16(%rbx), %eax
	bswap	%eax
	movl	%eax, 16(%r12)
	movl	20(%rbx), %eax
	bswap	%eax
	movl	%eax, 20(%r12)
	movl	24(%rbx), %eax
	bswap	%eax
	movl	%eax, 24(%r12)
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L132:
	movl	(%rbx), %eax
	movl	$1, %r8d
	bswap	%eax
	movl	%eax, (%r12)
	movl	4(%rbx), %eax
	bswap	%eax
	movl	%eax, 4(%r12)
	movl	8(%rbx), %eax
	bswap	%eax
	movl	%eax, 8(%r12)
	movl	12(%rbx), %eax
	bswap	%eax
	movl	%eax, 12(%r12)
	movl	16(%rbx), %eax
	bswap	%eax
	movl	%eax, 16(%r12)
	movl	20(%rbx), %eax
	bswap	%eax
	movl	%eax, 20(%r12)
	movl	24(%rbx), %eax
	bswap	%eax
	movl	%eax, 24(%r12)
	movl	28(%rbx), %eax
	bswap	%eax
	movl	%eax, 28(%r12)
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L121:
	movl	$64, %edx
	subq	%rax, %rdx
	je	.L124
	xorl	%eax, %eax
.L123:
	movb	$0, (%rcx,%rax)
	addq	$1, %rax
	cmpq	%rdx, %rax
	jb	.L123
.L124:
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	sha256_block_data_order@PLT
	movq	%r13, %rcx
	movl	$56, %edx
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L153:
	movl	%eax, %edx
	movl	$0, (%rcx)
	movl	$0, -4(%rcx,%rdx)
	jmp	.L126
.L154:
	movl	%eax, %edx
	xorl	%eax, %eax
	movw	%ax, -2(%rcx,%rdx)
	jmp	.L126
	.cfi_endproc
.LFE159:
	.size	SHA256_Final, .-SHA256_Final
	.p2align 4
	.globl	SHA224
	.type	SHA224, @function
SHA224:
.LFB153:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r9
	movl	$9, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	leaq	-144(%rbp), %r13
	addq	$-128, %rsp
	movdqa	.LC0(%rip), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	leaq	m.5024(%rip), %rax
	leaq	-112(%rbp), %rdx
	cmove	%rax, %r12
	movq	%rdx, %rdi
	xorl	%eax, %eax
	movaps	%xmm0, -144(%rbp)
	rep stosq
	movdqa	.LC1(%rip), %xmm0
	movaps	%xmm0, -128(%rbp)
	movl	$0, (%rdi)
	movl	$28, -36(%rbp)
	testq	%rsi, %rsi
	je	.L157
	movq	%rsi, %rdx
	movq	%r13, %rdi
	movq	%r9, %rsi
	call	SHA256_Update.part.0
.L157:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	SHA256_Final
	movl	$112, %esi
	movq	%r13, %rdi
	call	OPENSSL_cleanse@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L163
	subq	$-128, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L163:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE153:
	.size	SHA224, .-SHA224
	.p2align 4
	.globl	SHA256
	.type	SHA256, @function
SHA256:
.LFB154:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r9
	movl	$9, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	leaq	-144(%rbp), %r13
	addq	$-128, %rsp
	movdqa	.LC2(%rip), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	leaq	m.5031(%rip), %rax
	leaq	-112(%rbp), %rdx
	cmove	%rax, %r12
	movq	%rdx, %rdi
	xorl	%eax, %eax
	movaps	%xmm0, -144(%rbp)
	rep stosq
	movdqa	.LC3(%rip), %xmm0
	movaps	%xmm0, -128(%rbp)
	movl	$0, (%rdi)
	movl	$32, -36(%rbp)
	testq	%rsi, %rsi
	je	.L166
	movq	%rsi, %rdx
	movq	%r13, %rdi
	movq	%r9, %rsi
	call	SHA256_Update.part.0
.L166:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	SHA256_Final
	movl	$112, %esi
	movq	%r13, %rdi
	call	OPENSSL_cleanse@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L172
	subq	$-128, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L172:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE154:
	.size	SHA256, .-SHA256
	.p2align 4
	.globl	SHA224_Final
	.type	SHA224_Final, @function
SHA224_Final:
.LFB156:
	.cfi_startproc
	endbr64
	jmp	SHA256_Final
	.cfi_endproc
.LFE156:
	.size	SHA224_Final, .-SHA224_Final
	.local	m.5031
	.comm	m.5031,32,32
	.local	m.5024
	.comm	m.5024,28,16
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	-1056596264
	.long	914150663
	.long	812702999
	.long	-150054599
	.align 16
.LC1:
	.long	-4191439
	.long	1750603025
	.long	1694076839
	.long	-1090891868
	.align 16
.LC2:
	.long	1779033703
	.long	-1150833019
	.long	1013904242
	.long	-1521486534
	.align 16
.LC3:
	.long	1359893119
	.long	-1694144372
	.long	528734635
	.long	1541459225
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
