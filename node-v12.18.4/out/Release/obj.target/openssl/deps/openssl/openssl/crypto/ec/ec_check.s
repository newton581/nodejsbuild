	.file	"ec_check.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ec/ec_check.c"
	.text
	.p2align 4
	.globl	EC_GROUP_check
	.type	EC_GROUP_check, @function
EC_GROUP_check:
.LFB377:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	$1, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	movl	(%rax), %eax
	andl	$2, %eax
	jne	.L1
	movq	%rdi, %r12
	movq	%rsi, %rbx
	movl	%eax, %r14d
	xorl	%r15d, %r15d
	testq	%rsi, %rsi
	je	.L24
.L3:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	EC_GROUP_check_discriminant@PLT
	testl	%eax, %eax
	je	.L25
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L26
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	EC_POINT_is_on_curve@PLT
	testl	%eax, %eax
	jle	.L27
	movq	%r12, %rdi
	call	EC_POINT_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L4
	movq	%r12, %rdi
	call	EC_GROUP_get0_order@PLT
	testq	%rax, %rax
	je	.L4
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	BN_is_zero@PLT
	movq	-56(%rbp), %rdx
	testl	%eax, %eax
	jne	.L28
	movq	%rbx, %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	EC_POINT_mul@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	jne	.L29
	.p2align 4,,10
	.p2align 3
.L4:
	movq	%r15, %rdi
	call	BN_CTX_free@PLT
	movq	%r13, %rdi
	call	EC_POINT_free@PLT
.L1:
	addq	$24, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	call	BN_CTX_new@PLT
	movq	%rax, %r15
	movq	%rax, %rbx
	testq	%rax, %rax
	jne	.L3
	movl	$27, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$170, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L26:
	movl	$40, %r8d
	movl	$113, %edx
	movl	$170, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L25:
	movl	$118, %edx
	movl	$170, %esi
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	movl	$34, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$16, %edi
	call	ERR_put_error@PLT
	movq	%r15, %rdi
	call	BN_CTX_free@PLT
	movq	%r13, %rdi
	call	EC_POINT_free@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L27:
	movl	$44, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$107, %edx
	xorl	%r13d, %r13d
	movl	$170, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	movq	%r15, %rdi
	call	BN_CTX_free@PLT
	movq	%r13, %rdi
	call	EC_POINT_free@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L29:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	EC_POINT_is_at_infinity@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L30
	movl	$1, %r14d
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L28:
	movl	$55, %r8d
	movl	$128, %edx
	movl	$170, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L4
.L30:
	movl	$62, %r8d
	movl	$122, %edx
	movl	$170, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L4
	.cfi_endproc
.LFE377:
	.size	EC_GROUP_check, .-EC_GROUP_check
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
