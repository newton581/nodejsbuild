	.file	"ui_err.c"
	.text
	.p2align 4
	.globl	ERR_load_UI_strings
	.type	ERR_load_UI_strings, @function
ERR_load_UI_strings:
.LFB203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$671559680, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_func_error_string@PLT
	testq	%rax, %rax
	je	.L5
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	leaq	UI_str_functs(%rip), %rdi
	call	ERR_load_strings_const@PLT
	leaq	UI_str_reasons(%rip), %rdi
	call	ERR_load_strings_const@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE203:
	.size	ERR_load_UI_strings, .-ERR_load_UI_strings
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"common ok and cancel characters"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"index too large"
.LC2:
	.string	"index too small"
.LC3:
	.string	"no result buffer"
.LC4:
	.string	"processing error"
.LC5:
	.string	"result too large"
.LC6:
	.string	"result too small"
.LC7:
	.string	"sys$assign error"
.LC8:
	.string	"sys$dassgn error"
.LC9:
	.string	"sys$qiow error"
.LC10:
	.string	"unknown control command"
.LC11:
	.string	"unknown ttyget errno value"
	.section	.rodata.str1.8
	.align 8
.LC12:
	.string	"user data duplication unsupported"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	UI_str_reasons, @object
	.size	UI_str_reasons, 224
UI_str_reasons:
	.quad	671088744
	.quad	.LC0
	.quad	671088742
	.quad	.LC1
	.quad	671088743
	.quad	.LC2
	.quad	671088745
	.quad	.LC3
	.quad	671088747
	.quad	.LC4
	.quad	671088740
	.quad	.LC5
	.quad	671088741
	.quad	.LC6
	.quad	671088749
	.quad	.LC7
	.quad	671088750
	.quad	.LC8
	.quad	671088751
	.quad	.LC9
	.quad	671088746
	.quad	.LC10
	.quad	671088748
	.quad	.LC11
	.quad	671088752
	.quad	.LC12
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC13:
	.string	"close_console"
.LC14:
	.string	"echo_console"
.LC15:
	.string	"general_allocate_boolean"
.LC16:
	.string	"general_allocate_prompt"
.LC17:
	.string	"noecho_console"
.LC18:
	.string	"open_console"
.LC19:
	.string	"UI_construct_prompt"
.LC20:
	.string	"UI_create_method"
.LC21:
	.string	"UI_ctrl"
.LC22:
	.string	"UI_dup_error_string"
.LC23:
	.string	"UI_dup_info_string"
.LC24:
	.string	"UI_dup_input_boolean"
.LC25:
	.string	"UI_dup_input_string"
.LC26:
	.string	"UI_dup_user_data"
.LC27:
	.string	"UI_dup_verify_string"
.LC28:
	.string	"UI_get0_result"
.LC29:
	.string	"UI_get_result_length"
.LC30:
	.string	"UI_new_method"
.LC31:
	.string	"UI_process"
.LC32:
	.string	"UI_set_result"
.LC33:
	.string	"UI_set_result_ex"
	.section	.data.rel.ro.local
	.align 32
	.type	UI_str_functs, @object
	.size	UI_str_functs, 352
UI_str_functs:
	.quad	671559680
	.quad	.LC13
	.quad	671563776
	.quad	.LC14
	.quad	671531008
	.quad	.LC15
	.quad	671535104
	.quad	.LC16
	.quad	671567872
	.quad	.LC17
	.quad	671555584
	.quad	.LC18
	.quad	671584256
	.quad	.LC19
	.quad	671547392
	.quad	.LC20
	.quad	671543296
	.quad	.LC21
	.quad	671502336
	.quad	.LC22
	.quad	671506432
	.quad	.LC23
	.quad	671539200
	.quad	.LC24
	.quad	671510528
	.quad	.LC25
	.quad	671571968
	.quad	.LC26
	.quad	671522816
	.quad	.LC27
	.quad	671526912
	.quad	.LC28
	.quad	671576064
	.quad	.LC29
	.quad	671514624
	.quad	.LC30
	.quad	671551488
	.quad	.LC31
	.quad	671518720
	.quad	.LC32
	.quad	671580160
	.quad	.LC33
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
