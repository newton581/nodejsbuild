	.file	"ec_kmeth.c"
	.text
	.p2align 4
	.globl	EC_KEY_OpenSSL
	.type	EC_KEY_OpenSSL, @function
EC_KEY_OpenSSL:
.LFB772:
	.cfi_startproc
	endbr64
	leaq	openssl_ec_key_method(%rip), %rax
	ret
	.cfi_endproc
.LFE772:
	.size	EC_KEY_OpenSSL, .-EC_KEY_OpenSSL
	.p2align 4
	.globl	EC_KEY_get_default_method
	.type	EC_KEY_get_default_method, @function
EC_KEY_get_default_method:
.LFB773:
	.cfi_startproc
	endbr64
	movq	default_ec_key_meth(%rip), %rax
	ret
	.cfi_endproc
.LFE773:
	.size	EC_KEY_get_default_method, .-EC_KEY_get_default_method
	.p2align 4
	.globl	EC_KEY_set_default_method
	.type	EC_KEY_set_default_method, @function
EC_KEY_set_default_method:
.LFB774:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	leaq	openssl_ec_key_method(%rip), %rax
	cmove	%rax, %rdi
	movq	%rdi, default_ec_key_meth(%rip)
	ret
	.cfi_endproc
.LFE774:
	.size	EC_KEY_set_default_method, .-EC_KEY_set_default_method
	.p2align 4
	.globl	EC_KEY_get_method
	.type	EC_KEY_get_method, @function
EC_KEY_get_method:
.LFB775:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE775:
	.size	EC_KEY_get_method, .-EC_KEY_get_method
	.p2align 4
	.globl	EC_KEY_set_method
	.type	EC_KEY_set_method, @function
EC_KEY_set_method:
.LFB776:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rsi, %rbx
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L8
	call	*%rax
.L8:
	movq	8(%r12), %rdi
	call	ENGINE_finish@PLT
	movq	16(%rbx), %rax
	movq	%rbx, (%r12)
	movq	$0, 8(%r12)
	testq	%rax, %rax
	je	.L9
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE776:
	.size	EC_KEY_set_method, .-EC_KEY_set_method
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ec/ec_kmeth.c"
	.text
	.p2align 4
	.globl	EC_KEY_new_method
	.type	EC_KEY_new_method, @function
EC_KEY_new_method:
.LFB777:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$75, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$80, %edi
	pushq	%r12
	.cfi_offset 12, -32
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L34
	movl	$1, 56(%rax)
	mfence
	call	CRYPTO_THREAD_lock_new@PLT
	movq	%rax, 72(%r12)
	testq	%rax, %rax
	je	.L35
	movq	default_ec_key_meth(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L18
	movq	%r13, %rdi
	call	ENGINE_init@PLT
	movl	$94, %r8d
	testl	%eax, %eax
	je	.L33
	movq	%r13, 8(%r12)
.L21:
	movq	%r13, %rdi
	call	ENGINE_get_EC@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L36
.L22:
	leaq	64(%r12), %rdx
	movq	%r12, %rsi
	movl	$8, %edi
	movl	$1, 16(%r12)
	movl	$4, 52(%r12)
	call	CRYPTO_new_ex_data@PLT
	testl	%eax, %eax
	je	.L20
	movq	(%r12), %rax
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L14
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L37
.L14:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	movl	$103, %r8d
.L33:
	movl	$38, %edx
	movl	$245, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L20:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	EC_KEY_free@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	call	ENGINE_get_default_EC@PLT
	movq	%rax, 8(%r12)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L22
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L34:
	movl	$78, %r8d
	movl	$65, %edx
	movl	$245, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L37:
	movl	$117, %r8d
	movl	$70, %edx
	movl	$245, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L35:
	movl	$85, %r8d
	movl	$65, %edx
	movl	$245, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movl	$86, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L14
	.cfi_endproc
.LFE777:
	.size	EC_KEY_new_method, .-EC_KEY_new_method
	.p2align 4
	.globl	ECDH_compute_key
	.type	ECDH_compute_key, @function
ECDH_compute_key:
.LFB778:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rcx), %rax
	movq	$0, -56(%rbp)
	movq	72(%rax), %rax
	testq	%rax, %rax
	je	.L50
	cmpq	$2147483647, -72(%rbp)
	ja	.L51
	movq	%rdi, %r12
	leaq	-48(%rbp), %rsi
	leaq	-56(%rbp), %rdi
	movq	%r8, %rbx
	call	*%rax
	testl	%eax, %eax
	je	.L38
	movq	-48(%rbp), %r13
	movq	-56(%rbp), %r14
	testq	%rbx, %rbx
	je	.L42
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	-72(%rbp), %rcx
	movq	%r12, %rdx
	call	*%rbx
	movq	-48(%rbp), %r13
	movq	-56(%rbp), %r14
.L43:
	movl	$151, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	CRYPTO_clear_free@PLT
	movl	-72(%rbp), %eax
.L38:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L52
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	movl	$139, %r8d
	movl	$161, %edx
	movl	$246, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L42:
	movq	-72(%rbp), %rdx
	cmpq	%r13, %rdx
	jbe	.L44
	movq	%r13, -72(%rbp)
	movq	%r13, %rdx
.L44:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	memcpy@PLT
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L50:
	movl	$135, %r8d
	movl	$152, %edx
	movl	$246, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L38
.L52:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE778:
	.size	ECDH_compute_key, .-ECDH_compute_key
	.p2align 4
	.globl	EC_KEY_METHOD_new
	.type	EC_KEY_METHOD_new, @function
EC_KEY_METHOD_new:
.LFB779:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$157, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$120, %edi
	subq	$8, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L53
	testq	%rbx, %rbx
	je	.L55
	movdqu	(%rbx), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%rbx), %xmm1
	movups	%xmm1, 16(%rax)
	movdqu	32(%rbx), %xmm2
	movups	%xmm2, 32(%rax)
	movdqu	48(%rbx), %xmm3
	movups	%xmm3, 48(%rax)
	movdqu	64(%rbx), %xmm4
	movups	%xmm4, 64(%rax)
	movdqu	80(%rbx), %xmm5
	movups	%xmm5, 80(%rax)
	movdqu	96(%rbx), %xmm6
	movups	%xmm6, 96(%rax)
	movq	112(%rbx), %rdx
	movq	%rdx, 112(%rax)
.L55:
	orl	$1, 8(%rax)
.L53:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE779:
	.size	EC_KEY_METHOD_new, .-EC_KEY_METHOD_new
	.p2align 4
	.globl	EC_KEY_METHOD_free
	.type	EC_KEY_METHOD_free, @function
EC_KEY_METHOD_free:
.LFB780:
	.cfi_startproc
	endbr64
	testb	$1, 8(%rdi)
	jne	.L65
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	movl	$170, %edx
	leaq	.LC0(%rip), %rsi
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE780:
	.size	EC_KEY_METHOD_free, .-EC_KEY_METHOD_free
	.p2align 4
	.globl	EC_KEY_METHOD_set_init
	.type	EC_KEY_METHOD_set_init, @function
EC_KEY_METHOD_set_init:
.LFB781:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movups	%xmm0, 16(%rdi)
	movq	%rcx, %xmm0
	movq	%r8, -8(%rbp)
	movhps	-8(%rbp), %xmm0
	movups	%xmm0, 32(%rdi)
	movq	%r9, %xmm0
	movhps	16(%rbp), %xmm0
	movups	%xmm0, 48(%rdi)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE781:
	.size	EC_KEY_METHOD_set_init, .-EC_KEY_METHOD_set_init
	.p2align 4
	.globl	EC_KEY_METHOD_set_keygen
	.type	EC_KEY_METHOD_set_keygen, @function
EC_KEY_METHOD_set_keygen:
.LFB782:
	.cfi_startproc
	endbr64
	movq	%rsi, 64(%rdi)
	ret
	.cfi_endproc
.LFE782:
	.size	EC_KEY_METHOD_set_keygen, .-EC_KEY_METHOD_set_keygen
	.p2align 4
	.globl	EC_KEY_METHOD_set_compute_key
	.type	EC_KEY_METHOD_set_compute_key, @function
EC_KEY_METHOD_set_compute_key:
.LFB783:
	.cfi_startproc
	endbr64
	movq	%rsi, 72(%rdi)
	ret
	.cfi_endproc
.LFE783:
	.size	EC_KEY_METHOD_set_compute_key, .-EC_KEY_METHOD_set_compute_key
	.p2align 4
	.globl	EC_KEY_METHOD_set_sign
	.type	EC_KEY_METHOD_set_sign, @function
EC_KEY_METHOD_set_sign:
.LFB784:
	.cfi_startproc
	endbr64
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	movq	%rcx, 96(%rdi)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 80(%rdi)
	ret
	.cfi_endproc
.LFE784:
	.size	EC_KEY_METHOD_set_sign, .-EC_KEY_METHOD_set_sign
	.p2align 4
	.globl	EC_KEY_METHOD_set_verify
	.type	EC_KEY_METHOD_set_verify, @function
EC_KEY_METHOD_set_verify:
.LFB785:
	.cfi_startproc
	endbr64
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 104(%rdi)
	ret
	.cfi_endproc
.LFE785:
	.size	EC_KEY_METHOD_set_verify, .-EC_KEY_METHOD_set_verify
	.p2align 4
	.globl	EC_KEY_METHOD_get_init
	.type	EC_KEY_METHOD_get_init, @function
EC_KEY_METHOD_get_init:
.LFB786:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	16(%rbp), %rax
	testq	%rsi, %rsi
	je	.L73
	movq	16(%rdi), %r10
	movq	%r10, (%rsi)
.L73:
	testq	%rdx, %rdx
	je	.L74
	movq	24(%rdi), %rsi
	movq	%rsi, (%rdx)
.L74:
	testq	%rcx, %rcx
	je	.L75
	movq	32(%rdi), %rdx
	movq	%rdx, (%rcx)
.L75:
	testq	%r8, %r8
	je	.L76
	movq	40(%rdi), %rdx
	movq	%rdx, (%r8)
.L76:
	testq	%r9, %r9
	je	.L77
	movq	48(%rdi), %rdx
	movq	%rdx, (%r9)
.L77:
	testq	%rax, %rax
	je	.L72
	movq	56(%rdi), %rdx
	movq	%rdx, (%rax)
.L72:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE786:
	.size	EC_KEY_METHOD_get_init, .-EC_KEY_METHOD_get_init
	.p2align 4
	.globl	EC_KEY_METHOD_get_keygen
	.type	EC_KEY_METHOD_get_keygen, @function
EC_KEY_METHOD_get_keygen:
.LFB787:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L98
	movq	64(%rdi), %rax
	movq	%rax, (%rsi)
.L98:
	ret
	.cfi_endproc
.LFE787:
	.size	EC_KEY_METHOD_get_keygen, .-EC_KEY_METHOD_get_keygen
	.p2align 4
	.globl	EC_KEY_METHOD_get_compute_key
	.type	EC_KEY_METHOD_get_compute_key, @function
EC_KEY_METHOD_get_compute_key:
.LFB788:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L103
	movq	72(%rdi), %rax
	movq	%rax, (%rsi)
.L103:
	ret
	.cfi_endproc
.LFE788:
	.size	EC_KEY_METHOD_get_compute_key, .-EC_KEY_METHOD_get_compute_key
	.p2align 4
	.globl	EC_KEY_METHOD_get_sign
	.type	EC_KEY_METHOD_get_sign, @function
EC_KEY_METHOD_get_sign:
.LFB789:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L109
	movq	80(%rdi), %rax
	movq	%rax, (%rsi)
.L109:
	testq	%rdx, %rdx
	je	.L110
	movq	88(%rdi), %rax
	movq	%rax, (%rdx)
.L110:
	testq	%rcx, %rcx
	je	.L108
	movq	96(%rdi), %rax
	movq	%rax, (%rcx)
.L108:
	ret
	.cfi_endproc
.LFE789:
	.size	EC_KEY_METHOD_get_sign, .-EC_KEY_METHOD_get_sign
	.p2align 4
	.globl	EC_KEY_METHOD_get_verify
	.type	EC_KEY_METHOD_get_verify, @function
EC_KEY_METHOD_get_verify:
.LFB790:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L122
	movq	104(%rdi), %rax
	movq	%rax, (%rsi)
.L122:
	testq	%rdx, %rdx
	je	.L121
	movq	112(%rdi), %rax
	movq	%rax, (%rdx)
.L121:
	ret
	.cfi_endproc
.LFE790:
	.size	EC_KEY_METHOD_get_verify, .-EC_KEY_METHOD_get_verify
	.section	.data.rel.local,"aw"
	.align 8
	.type	default_ec_key_meth, @object
	.size	default_ec_key_meth, 8
default_ec_key_meth:
	.quad	openssl_ec_key_method
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"OpenSSL EC_KEY method"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	openssl_ec_key_method, @object
	.size	openssl_ec_key_method, 120
openssl_ec_key_method:
	.quad	.LC1
	.long	0
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	ossl_ec_key_gen
	.quad	ossl_ecdh_compute_key
	.quad	ossl_ecdsa_sign
	.quad	ossl_ecdsa_sign_setup
	.quad	ossl_ecdsa_sign_sig
	.quad	ossl_ecdsa_verify
	.quad	ossl_ecdsa_verify_sig
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
