	.file	"v3_admis.c"
	.text
	.p2align 4
	.globl	PROFESSION_INFO_free
	.type	PROFESSION_INFO_free, @function
PROFESSION_INFO_free:
.LFB1327:
	.cfi_startproc
	endbr64
	leaq	PROFESSION_INFO_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1327:
	.size	PROFESSION_INFO_free, .-PROFESSION_INFO_free
	.p2align 4
	.globl	ADMISSIONS_free
	.type	ADMISSIONS_free, @function
ADMISSIONS_free:
.LFB1331:
	.cfi_startproc
	endbr64
	leaq	ADMISSIONS_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1331:
	.size	ADMISSIONS_free, .-ADMISSIONS_free
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	")"
.LC2:
	.string	" ("
.LC3:
	.string	"%*snamingAuthority: "
.LC4:
	.string	"%*s  admissionAuthorityId: "
.LC5:
	.string	"%s%s%s%s\n"
.LC6:
	.string	"%*s  namingAuthorityText: "
.LC7:
	.string	"\n"
.LC8:
	.string	"%*s  namingAuthorityUrl: "
	.text
	.p2align 4
	.type	i2r_NAMING_AUTHORITY.isra.0, @function
i2r_NAMING_AUTHORITY.isra.0:
.LFB1364:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L10
	cmpq	$0, (%rdi)
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movl	%edx, %r13d
	je	.L21
.L7:
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rcx
	movl	%r13d, %edx
	movq	%r12, %rdi
	leaq	.LC3(%rip), %rsi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L10
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L9
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2ln@PLT
	leaq	.LC0(%rip), %rcx
	movl	%r13d, %edx
	movq	%r12, %rdi
	movq	%rax, %r14
	leaq	.LC4(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L10
	movq	(%rbx), %rdx
	movl	$1, %ecx
	movl	$128, %esi
	leaq	-192(%rbp), %r15
	movq	%r15, %rdi
	call	OBJ_obj2txt@PLT
	leaq	.LC1(%rip), %r9
	leaq	.LC2(%rip), %rcx
	testq	%r14, %r14
	je	.L22
.L11:
	xorl	%eax, %eax
	movq	%r15, %r8
	movq	%r14, %rdx
	movq	%r12, %rdi
	leaq	.LC5(%rip), %rsi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L10
.L9:
	cmpq	$0, 16(%rbx)
	je	.L14
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rcx
	movl	%r13d, %edx
	movq	%r12, %rdi
	leaq	.LC6(%rip), %rsi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L10
	movq	16(%rbx), %rsi
	movq	%r12, %rdi
	call	ASN1_STRING_print@PLT
	testl	%eax, %eax
	jle	.L10
	xorl	%eax, %eax
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L10
.L14:
	cmpq	$0, 8(%rbx)
	movl	$1, %eax
	je	.L4
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rcx
	movl	%r13d, %edx
	movq	%r12, %rdi
	leaq	.LC8(%rip), %rsi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L10
	movq	8(%rbx), %rsi
	movq	%r12, %rdi
	call	ASN1_STRING_print@PLT
	testl	%eax, %eax
	jle	.L10
	xorl	%eax, %eax
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L21:
	cmpq	$0, 16(%rdi)
	jne	.L7
	cmpq	$0, 8(%rdi)
	jne	.L7
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%eax, %eax
.L4:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L23
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	leaq	.LC0(%rip), %r9
	movq	%r9, %rcx
	movq	%r9, %r14
	jmp	.L11
.L23:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1364:
	.size	i2r_NAMING_AUTHORITY.isra.0, .-i2r_NAMING_AUTHORITY.isra.0
	.section	.rodata.str1.1
.LC9:
	.string	"%*sadmissionAuthority:\n"
.LC10:
	.string	"%*s  "
.LC11:
	.string	"%*sEntry %0d:\n"
.LC12:
	.string	"%*s  admissionAuthority:\n"
.LC13:
	.string	"%*s    "
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC14:
	.string	"%*s  Profession Info Entry %0d:\n"
	.section	.rodata.str1.1
.LC15:
	.string	"%*s    registrationNumber: "
.LC16:
	.string	"%*s    Info Entries:\n"
.LC17:
	.string	"%*s      "
.LC18:
	.string	"%*s    Profession OIDs:\n"
.LC19:
	.string	"%*s      %s%s%s%s\n"
	.text
	.p2align 4
	.type	i2r_ADMISSION_SYNTAX, @function
i2r_ADMISSION_SYNTAX:
.LFB1337:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -240(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, (%rsi)
	je	.L25
	leaq	.LC0(%rip), %rcx
	movl	%r13d, %edx
	leaq	.LC9(%rip), %rsi
	movq	%r14, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L46
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rcx
	movl	%r13d, %edx
	movq	%r14, %rdi
	leaq	.LC10(%rip), %rsi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L46
	movq	-240(%rbp), %rax
	movq	%r14, %rdi
	movq	(%rax), %rsi
	call	GENERAL_NAME_print@PLT
	testl	%eax, %eax
	jle	.L46
	xorl	%eax, %eax
	leaq	.LC7(%rip), %rsi
	movq	%r14, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L46
	.p2align 4,,10
	.p2align 3
.L25:
	leaq	-192(%rbp), %rax
	movl	$0, -216(%rbp)
	movq	%rax, -256(%rbp)
.L50:
	movq	-240(%rbp), %rax
	movq	8(%rax), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, -216(%rbp)
	jge	.L56
	movq	-240(%rbp), %rax
	movl	-216(%rbp), %ebx
	movq	8(%rax), %rdi
	movl	%ebx, %esi
	addl	$1, %ebx
	call	OPENSSL_sk_value@PLT
	movl	%ebx, %r8d
	movl	%r13d, %edx
	movq	%r14, %rdi
	movq	%rax, %r15
	movq	%rax, -224(%rbp)
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rcx
	leaq	.LC11(%rip), %rsi
	movl	%ebx, -216(%rbp)
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L46
	cmpq	$0, (%r15)
	je	.L32
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rcx
	movl	%r13d, %edx
	movq	%r14, %rdi
	leaq	.LC12(%rip), %rsi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L46
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rcx
	movl	%r13d, %edx
	movq	%r14, %rdi
	leaq	.LC13(%rip), %rsi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L46
	movq	-224(%rbp), %rax
	movq	%r14, %rdi
	movq	(%rax), %rsi
	call	GENERAL_NAME_print@PLT
	testl	%eax, %eax
	jle	.L46
	xorl	%eax, %eax
	leaq	.LC7(%rip), %rsi
	movq	%r14, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L46
.L32:
	movq	-224(%rbp), %rax
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L31
	movl	%r13d, %edx
	movq	%r14, %rsi
	call	i2r_NAMING_AUTHORITY.isra.0
	testl	%eax, %eax
	jle	.L46
.L31:
	movl	$0, -212(%rbp)
	leal	2(%r13), %eax
	movl	%eax, -244(%rbp)
.L33:
	movq	-224(%rbp), %rax
	movq	16(%rax), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, -212(%rbp)
	jge	.L50
	movq	-224(%rbp), %rax
	movl	-212(%rbp), %r15d
	movq	16(%rax), %rdi
	movl	%r15d, %esi
	addl	$1, %r15d
	call	OPENSSL_sk_value@PLT
	movl	%r15d, %r8d
	movl	%r13d, %edx
	movq	%r14, %rdi
	movq	%rax, %rbx
	leaq	.LC0(%rip), %rcx
	xorl	%eax, %eax
	movl	%r15d, -212(%rbp)
	leaq	.LC14(%rip), %rsi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L46
	cmpq	$0, 24(%rbx)
	je	.L37
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rcx
	movl	%r13d, %edx
	movq	%r14, %rdi
	leaq	.LC15(%rip), %rsi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L46
	movq	24(%rbx), %rsi
	movq	%r14, %rdi
	call	ASN1_STRING_print@PLT
	testl	%eax, %eax
	jle	.L46
	xorl	%eax, %eax
	leaq	.LC7(%rip), %rsi
	movq	%r14, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L46
.L37:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L36
	movl	-244(%rbp), %edx
	movq	%r14, %rsi
	call	i2r_NAMING_AUTHORITY.isra.0
	testl	%eax, %eax
	jle	.L46
.L36:
	cmpq	$0, 8(%rbx)
	jne	.L57
.L39:
	cmpq	$0, 16(%rbx)
	je	.L33
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rcx
	movl	%r13d, %edx
	movq	%r14, %rdi
	leaq	.LC18(%rip), %rsi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L46
	movq	%r14, -232(%rbp)
	xorl	%r12d, %r12d
	movq	-256(%rbp), %r14
	leaq	.LC0(%rip), %r15
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L59:
	leaq	.LC1(%rip), %rax
	leaq	.LC2(%rip), %r9
	movq	%r15, %rcx
	pushq	%rax
	pushq	%r14
.L55:
	movq	-232(%rbp), %rdi
	movl	%r13d, %edx
	leaq	.LC19(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	jle	.L46
	addl	$1, %r12d
.L44:
	movq	16(%rbx), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L58
	movq	16(%rbx), %rdi
	movl	%r12d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	movq	%rax, -208(%rbp)
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2ln@PLT
	movq	-208(%rbp), %rdx
	movl	$1, %ecx
	movq	%r14, %rdi
	movl	$128, %esi
	movq	%rax, -200(%rbp)
	call	OBJ_obj2txt@PLT
	movq	-200(%rbp), %r8
	testq	%r8, %r8
	jne	.L59
	leaq	.LC0(%rip), %r9
	pushq	%r15
	movq	%r9, %r8
	pushq	%r14
	movq	%r9, %rcx
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L57:
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rcx
	movl	%r13d, %edx
	movq	%r14, %rdi
	leaq	.LC16(%rip), %rsi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L46
	xorl	%r12d, %r12d
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L60:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	ASN1_STRING_print@PLT
	testl	%eax, %eax
	jle	.L46
	xorl	%eax, %eax
	leaq	.LC7(%rip), %rsi
	movq	%r14, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L46
	addl	$1, %r12d
.L42:
	movq	8(%rbx), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L39
	movq	8(%rbx), %rdi
	movl	%r12d, %esi
	call	OPENSSL_sk_value@PLT
	leaq	.LC0(%rip), %rcx
	movl	%r13d, %edx
	movq	%r14, %rdi
	movq	%rax, %r15
	leaq	.LC17(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jg	.L60
	.p2align 4,,10
	.p2align 3
.L46:
	movl	$-1, %eax
.L24:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L61
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	movq	-232(%rbp), %r14
	jmp	.L33
.L56:
	movl	$1, %eax
	jmp	.L24
.L61:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1337:
	.size	i2r_ADMISSION_SYNTAX, .-i2r_ADMISSION_SYNTAX
	.p2align 4
	.globl	d2i_NAMING_AUTHORITY
	.type	d2i_NAMING_AUTHORITY, @function
d2i_NAMING_AUTHORITY:
.LFB1320:
	.cfi_startproc
	endbr64
	leaq	NAMING_AUTHORITY_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1320:
	.size	d2i_NAMING_AUTHORITY, .-d2i_NAMING_AUTHORITY
	.p2align 4
	.globl	i2d_NAMING_AUTHORITY
	.type	i2d_NAMING_AUTHORITY, @function
i2d_NAMING_AUTHORITY:
.LFB1321:
	.cfi_startproc
	endbr64
	leaq	NAMING_AUTHORITY_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1321:
	.size	i2d_NAMING_AUTHORITY, .-i2d_NAMING_AUTHORITY
	.p2align 4
	.globl	NAMING_AUTHORITY_new
	.type	NAMING_AUTHORITY_new, @function
NAMING_AUTHORITY_new:
.LFB1322:
	.cfi_startproc
	endbr64
	leaq	NAMING_AUTHORITY_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1322:
	.size	NAMING_AUTHORITY_new, .-NAMING_AUTHORITY_new
	.p2align 4
	.globl	NAMING_AUTHORITY_free
	.type	NAMING_AUTHORITY_free, @function
NAMING_AUTHORITY_free:
.LFB1323:
	.cfi_startproc
	endbr64
	leaq	NAMING_AUTHORITY_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1323:
	.size	NAMING_AUTHORITY_free, .-NAMING_AUTHORITY_free
	.p2align 4
	.globl	d2i_PROFESSION_INFO
	.type	d2i_PROFESSION_INFO, @function
d2i_PROFESSION_INFO:
.LFB1324:
	.cfi_startproc
	endbr64
	leaq	PROFESSION_INFO_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1324:
	.size	d2i_PROFESSION_INFO, .-d2i_PROFESSION_INFO
	.p2align 4
	.globl	i2d_PROFESSION_INFO
	.type	i2d_PROFESSION_INFO, @function
i2d_PROFESSION_INFO:
.LFB1325:
	.cfi_startproc
	endbr64
	leaq	PROFESSION_INFO_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1325:
	.size	i2d_PROFESSION_INFO, .-i2d_PROFESSION_INFO
	.p2align 4
	.globl	PROFESSION_INFO_new
	.type	PROFESSION_INFO_new, @function
PROFESSION_INFO_new:
.LFB1326:
	.cfi_startproc
	endbr64
	leaq	PROFESSION_INFO_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1326:
	.size	PROFESSION_INFO_new, .-PROFESSION_INFO_new
	.p2align 4
	.globl	d2i_ADMISSIONS
	.type	d2i_ADMISSIONS, @function
d2i_ADMISSIONS:
.LFB1328:
	.cfi_startproc
	endbr64
	leaq	ADMISSIONS_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1328:
	.size	d2i_ADMISSIONS, .-d2i_ADMISSIONS
	.p2align 4
	.globl	i2d_ADMISSIONS
	.type	i2d_ADMISSIONS, @function
i2d_ADMISSIONS:
.LFB1329:
	.cfi_startproc
	endbr64
	leaq	ADMISSIONS_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1329:
	.size	i2d_ADMISSIONS, .-i2d_ADMISSIONS
	.p2align 4
	.globl	ADMISSIONS_new
	.type	ADMISSIONS_new, @function
ADMISSIONS_new:
.LFB1330:
	.cfi_startproc
	endbr64
	leaq	ADMISSIONS_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1330:
	.size	ADMISSIONS_new, .-ADMISSIONS_new
	.p2align 4
	.globl	d2i_ADMISSION_SYNTAX
	.type	d2i_ADMISSION_SYNTAX, @function
d2i_ADMISSION_SYNTAX:
.LFB1332:
	.cfi_startproc
	endbr64
	leaq	ADMISSION_SYNTAX_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1332:
	.size	d2i_ADMISSION_SYNTAX, .-d2i_ADMISSION_SYNTAX
	.p2align 4
	.globl	i2d_ADMISSION_SYNTAX
	.type	i2d_ADMISSION_SYNTAX, @function
i2d_ADMISSION_SYNTAX:
.LFB1333:
	.cfi_startproc
	endbr64
	leaq	ADMISSION_SYNTAX_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1333:
	.size	i2d_ADMISSION_SYNTAX, .-i2d_ADMISSION_SYNTAX
	.p2align 4
	.globl	ADMISSION_SYNTAX_new
	.type	ADMISSION_SYNTAX_new, @function
ADMISSION_SYNTAX_new:
.LFB1334:
	.cfi_startproc
	endbr64
	leaq	ADMISSION_SYNTAX_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1334:
	.size	ADMISSION_SYNTAX_new, .-ADMISSION_SYNTAX_new
	.p2align 4
	.globl	ADMISSION_SYNTAX_free
	.type	ADMISSION_SYNTAX_free, @function
ADMISSION_SYNTAX_free:
.LFB1335:
	.cfi_startproc
	endbr64
	leaq	ADMISSION_SYNTAX_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1335:
	.size	ADMISSION_SYNTAX_free, .-ADMISSION_SYNTAX_free
	.p2align 4
	.globl	NAMING_AUTHORITY_get0_authorityId
	.type	NAMING_AUTHORITY_get0_authorityId, @function
NAMING_AUTHORITY_get0_authorityId:
.LFB1338:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE1338:
	.size	NAMING_AUTHORITY_get0_authorityId, .-NAMING_AUTHORITY_get0_authorityId
	.p2align 4
	.globl	NAMING_AUTHORITY_set0_authorityId
	.type	NAMING_AUTHORITY_set0_authorityId, @function
NAMING_AUTHORITY_set0_authorityId:
.LFB1339:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	%r12, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1339:
	.size	NAMING_AUTHORITY_set0_authorityId, .-NAMING_AUTHORITY_set0_authorityId
	.p2align 4
	.globl	NAMING_AUTHORITY_get0_authorityURL
	.type	NAMING_AUTHORITY_get0_authorityURL, @function
NAMING_AUTHORITY_get0_authorityURL:
.LFB1340:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE1340:
	.size	NAMING_AUTHORITY_get0_authorityURL, .-NAMING_AUTHORITY_get0_authorityURL
	.p2align 4
	.globl	NAMING_AUTHORITY_set0_authorityURL
	.type	NAMING_AUTHORITY_set0_authorityURL, @function
NAMING_AUTHORITY_set0_authorityURL:
.LFB1341:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	call	ASN1_IA5STRING_free@PLT
	movq	%r12, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1341:
	.size	NAMING_AUTHORITY_set0_authorityURL, .-NAMING_AUTHORITY_set0_authorityURL
	.p2align 4
	.globl	NAMING_AUTHORITY_get0_authorityText
	.type	NAMING_AUTHORITY_get0_authorityText, @function
NAMING_AUTHORITY_get0_authorityText:
.LFB1342:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE1342:
	.size	NAMING_AUTHORITY_get0_authorityText, .-NAMING_AUTHORITY_get0_authorityText
	.p2align 4
	.globl	NAMING_AUTHORITY_set0_authorityText
	.type	NAMING_AUTHORITY_set0_authorityText, @function
NAMING_AUTHORITY_set0_authorityText:
.LFB1343:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	16(%rdi), %rdi
	call	ASN1_IA5STRING_free@PLT
	movq	%r12, 16(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1343:
	.size	NAMING_AUTHORITY_set0_authorityText, .-NAMING_AUTHORITY_set0_authorityText
	.p2align 4
	.globl	ADMISSION_SYNTAX_get0_admissionAuthority
	.type	ADMISSION_SYNTAX_get0_admissionAuthority, @function
ADMISSION_SYNTAX_get0_admissionAuthority:
.LFB1344:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE1344:
	.size	ADMISSION_SYNTAX_get0_admissionAuthority, .-ADMISSION_SYNTAX_get0_admissionAuthority
	.p2align 4
	.globl	ADMISSION_SYNTAX_set0_admissionAuthority
	.type	ADMISSION_SYNTAX_set0_admissionAuthority, @function
ADMISSION_SYNTAX_set0_admissionAuthority:
.LFB1345:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	call	GENERAL_NAME_free@PLT
	movq	%r12, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1345:
	.size	ADMISSION_SYNTAX_set0_admissionAuthority, .-ADMISSION_SYNTAX_set0_admissionAuthority
	.p2align 4
	.globl	ADMISSION_SYNTAX_get0_contentsOfAdmissions
	.type	ADMISSION_SYNTAX_get0_contentsOfAdmissions, @function
ADMISSION_SYNTAX_get0_contentsOfAdmissions:
.LFB1346:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE1346:
	.size	ADMISSION_SYNTAX_get0_contentsOfAdmissions, .-ADMISSION_SYNTAX_get0_contentsOfAdmissions
	.p2align 4
	.globl	ADMISSION_SYNTAX_set0_contentsOfAdmissions
	.type	ADMISSION_SYNTAX_set0_contentsOfAdmissions, @function
ADMISSION_SYNTAX_set0_contentsOfAdmissions:
.LFB1347:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	leaq	ADMISSIONS_free(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	%r12, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1347:
	.size	ADMISSION_SYNTAX_set0_contentsOfAdmissions, .-ADMISSION_SYNTAX_set0_contentsOfAdmissions
	.p2align 4
	.globl	ADMISSIONS_get0_admissionAuthority
	.type	ADMISSIONS_get0_admissionAuthority, @function
ADMISSIONS_get0_admissionAuthority:
.LFB1348:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE1348:
	.size	ADMISSIONS_get0_admissionAuthority, .-ADMISSIONS_get0_admissionAuthority
	.p2align 4
	.globl	ADMISSIONS_set0_admissionAuthority
	.type	ADMISSIONS_set0_admissionAuthority, @function
ADMISSIONS_set0_admissionAuthority:
.LFB1349:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	call	GENERAL_NAME_free@PLT
	movq	%r12, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1349:
	.size	ADMISSIONS_set0_admissionAuthority, .-ADMISSIONS_set0_admissionAuthority
	.p2align 4
	.globl	ADMISSIONS_get0_namingAuthority
	.type	ADMISSIONS_get0_namingAuthority, @function
ADMISSIONS_get0_namingAuthority:
.LFB1350:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE1350:
	.size	ADMISSIONS_get0_namingAuthority, .-ADMISSIONS_get0_namingAuthority
	.p2align 4
	.globl	ADMISSIONS_set0_namingAuthority
	.type	ADMISSIONS_set0_namingAuthority, @function
ADMISSIONS_set0_namingAuthority:
.LFB1351:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	leaq	NAMING_AUTHORITY_it(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	call	ASN1_item_free@PLT
	movq	%r12, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1351:
	.size	ADMISSIONS_set0_namingAuthority, .-ADMISSIONS_set0_namingAuthority
	.p2align 4
	.globl	ADMISSIONS_get0_professionInfos
	.type	ADMISSIONS_get0_professionInfos, @function
ADMISSIONS_get0_professionInfos:
.LFB1352:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE1352:
	.size	ADMISSIONS_get0_professionInfos, .-ADMISSIONS_get0_professionInfos
	.p2align 4
	.globl	ADMISSIONS_set0_professionInfos
	.type	ADMISSIONS_set0_professionInfos, @function
ADMISSIONS_set0_professionInfos:
.LFB1353:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	leaq	PROFESSION_INFO_free(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	16(%rdi), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	%r12, 16(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1353:
	.size	ADMISSIONS_set0_professionInfos, .-ADMISSIONS_set0_professionInfos
	.p2align 4
	.globl	PROFESSION_INFO_get0_addProfessionInfo
	.type	PROFESSION_INFO_get0_addProfessionInfo, @function
PROFESSION_INFO_get0_addProfessionInfo:
.LFB1354:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE1354:
	.size	PROFESSION_INFO_get0_addProfessionInfo, .-PROFESSION_INFO_get0_addProfessionInfo
	.p2align 4
	.globl	PROFESSION_INFO_set0_addProfessionInfo
	.type	PROFESSION_INFO_set0_addProfessionInfo, @function
PROFESSION_INFO_set0_addProfessionInfo:
.LFB1355:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	32(%rdi), %rdi
	call	ASN1_OCTET_STRING_free@PLT
	movq	%r12, 32(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1355:
	.size	PROFESSION_INFO_set0_addProfessionInfo, .-PROFESSION_INFO_set0_addProfessionInfo
	.p2align 4
	.globl	PROFESSION_INFO_get0_namingAuthority
	.type	PROFESSION_INFO_get0_namingAuthority, @function
PROFESSION_INFO_get0_namingAuthority:
.LFB1356:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE1356:
	.size	PROFESSION_INFO_get0_namingAuthority, .-PROFESSION_INFO_get0_namingAuthority
	.p2align 4
	.globl	PROFESSION_INFO_set0_namingAuthority
	.type	PROFESSION_INFO_set0_namingAuthority, @function
PROFESSION_INFO_set0_namingAuthority:
.LFB1357:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	leaq	NAMING_AUTHORITY_it(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	call	ASN1_item_free@PLT
	movq	%r12, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1357:
	.size	PROFESSION_INFO_set0_namingAuthority, .-PROFESSION_INFO_set0_namingAuthority
	.p2align 4
	.globl	PROFESSION_INFO_get0_professionItems
	.type	PROFESSION_INFO_get0_professionItems, @function
PROFESSION_INFO_get0_professionItems:
.LFB1358:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE1358:
	.size	PROFESSION_INFO_get0_professionItems, .-PROFESSION_INFO_get0_professionItems
	.p2align 4
	.globl	PROFESSION_INFO_set0_professionItems
	.type	PROFESSION_INFO_set0_professionItems, @function
PROFESSION_INFO_set0_professionItems:
.LFB1359:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	movq	ASN1_STRING_free@GOTPCREL(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	%r12, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1359:
	.size	PROFESSION_INFO_set0_professionItems, .-PROFESSION_INFO_set0_professionItems
	.p2align 4
	.globl	PROFESSION_INFO_get0_professionOIDs
	.type	PROFESSION_INFO_get0_professionOIDs, @function
PROFESSION_INFO_get0_professionOIDs:
.LFB1360:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE1360:
	.size	PROFESSION_INFO_get0_professionOIDs, .-PROFESSION_INFO_get0_professionOIDs
	.p2align 4
	.globl	PROFESSION_INFO_set0_professionOIDs
	.type	PROFESSION_INFO_set0_professionOIDs, @function
PROFESSION_INFO_set0_professionOIDs:
.LFB1361:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	movq	ASN1_OBJECT_free@GOTPCREL(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	16(%rdi), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	%r12, 16(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1361:
	.size	PROFESSION_INFO_set0_professionOIDs, .-PROFESSION_INFO_set0_professionOIDs
	.p2align 4
	.globl	PROFESSION_INFO_get0_registrationNumber
	.type	PROFESSION_INFO_get0_registrationNumber, @function
PROFESSION_INFO_get0_registrationNumber:
.LFB1362:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE1362:
	.size	PROFESSION_INFO_get0_registrationNumber, .-PROFESSION_INFO_get0_registrationNumber
	.p2align 4
	.globl	PROFESSION_INFO_set0_registrationNumber
	.type	PROFESSION_INFO_set0_registrationNumber, @function
PROFESSION_INFO_set0_registrationNumber:
.LFB1363:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	24(%rdi), %rdi
	call	ASN1_PRINTABLESTRING_free@PLT
	movq	%r12, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1363:
	.size	PROFESSION_INFO_set0_registrationNumber, .-PROFESSION_INFO_set0_registrationNumber
	.globl	v3_ext_admission
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	v3_ext_admission, @object
	.size	v3_ext_admission, 104
v3_ext_admission:
	.long	1093
	.long	0
	.quad	ADMISSION_SYNTAX_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	i2r_ADMISSION_SYNTAX
	.quad	0
	.quad	0
	.globl	ADMISSION_SYNTAX_it
	.section	.rodata.str1.1
.LC20:
	.string	"ADMISSION_SYNTAX"
	.section	.data.rel.ro.local
	.align 32
	.type	ADMISSION_SYNTAX_it, @object
	.size	ADMISSION_SYNTAX_it, 56
ADMISSION_SYNTAX_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	ADMISSION_SYNTAX_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC20
	.section	.rodata.str1.1
.LC21:
	.string	"admissionAuthority"
.LC22:
	.string	"contentsOfAdmissions"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	ADMISSION_SYNTAX_seq_tt, @object
	.size	ADMISSION_SYNTAX_seq_tt, 80
ADMISSION_SYNTAX_seq_tt:
	.quad	1
	.quad	0
	.quad	0
	.quad	.LC21
	.quad	GENERAL_NAME_it
	.quad	4
	.quad	0
	.quad	8
	.quad	.LC22
	.quad	ADMISSIONS_it
	.globl	ADMISSIONS_it
	.section	.rodata.str1.1
.LC23:
	.string	"ADMISSIONS"
	.section	.data.rel.ro.local
	.align 32
	.type	ADMISSIONS_it, @object
	.size	ADMISSIONS_it, 56
ADMISSIONS_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	ADMISSIONS_seq_tt
	.quad	3
	.quad	0
	.quad	24
	.quad	.LC23
	.section	.rodata.str1.1
.LC24:
	.string	"namingAuthority"
.LC25:
	.string	"professionInfos"
	.section	.data.rel.ro
	.align 32
	.type	ADMISSIONS_seq_tt, @object
	.size	ADMISSIONS_seq_tt, 120
ADMISSIONS_seq_tt:
	.quad	145
	.quad	0
	.quad	0
	.quad	.LC21
	.quad	GENERAL_NAME_it
	.quad	145
	.quad	1
	.quad	8
	.quad	.LC24
	.quad	NAMING_AUTHORITY_it
	.quad	4
	.quad	0
	.quad	16
	.quad	.LC25
	.quad	PROFESSION_INFO_it
	.globl	PROFESSION_INFO_it
	.section	.rodata.str1.1
.LC26:
	.string	"PROFESSION_INFO"
	.section	.data.rel.ro.local
	.align 32
	.type	PROFESSION_INFO_it, @object
	.size	PROFESSION_INFO_it, 56
PROFESSION_INFO_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	PROFESSION_INFO_seq_tt
	.quad	5
	.quad	0
	.quad	40
	.quad	.LC26
	.section	.rodata.str1.1
.LC27:
	.string	"professionItems"
.LC28:
	.string	"professionOIDs"
.LC29:
	.string	"registrationNumber"
.LC30:
	.string	"addProfessionInfo"
	.section	.data.rel.ro
	.align 32
	.type	PROFESSION_INFO_seq_tt, @object
	.size	PROFESSION_INFO_seq_tt, 200
PROFESSION_INFO_seq_tt:
	.quad	145
	.quad	0
	.quad	0
	.quad	.LC24
	.quad	NAMING_AUTHORITY_it
	.quad	4
	.quad	0
	.quad	8
	.quad	.LC27
	.quad	DIRECTORYSTRING_it
	.quad	5
	.quad	0
	.quad	16
	.quad	.LC28
	.quad	ASN1_OBJECT_it
	.quad	1
	.quad	0
	.quad	24
	.quad	.LC29
	.quad	ASN1_PRINTABLESTRING_it
	.quad	1
	.quad	0
	.quad	32
	.quad	.LC30
	.quad	ASN1_OCTET_STRING_it
	.globl	NAMING_AUTHORITY_it
	.section	.rodata.str1.1
.LC31:
	.string	"NAMING_AUTHORITY"
	.section	.data.rel.ro.local
	.align 32
	.type	NAMING_AUTHORITY_it, @object
	.size	NAMING_AUTHORITY_it, 56
NAMING_AUTHORITY_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	NAMING_AUTHORITY_seq_tt
	.quad	3
	.quad	0
	.quad	24
	.quad	.LC31
	.section	.rodata.str1.1
.LC32:
	.string	"namingAuthorityId"
.LC33:
	.string	"namingAuthorityUrl"
.LC34:
	.string	"namingAuthorityText"
	.section	.data.rel.ro
	.align 32
	.type	NAMING_AUTHORITY_seq_tt, @object
	.size	NAMING_AUTHORITY_seq_tt, 120
NAMING_AUTHORITY_seq_tt:
	.quad	1
	.quad	0
	.quad	0
	.quad	.LC32
	.quad	ASN1_OBJECT_it
	.quad	1
	.quad	0
	.quad	8
	.quad	.LC33
	.quad	ASN1_IA5STRING_it
	.quad	1
	.quad	0
	.quad	16
	.quad	.LC34
	.quad	DIRECTORYSTRING_it
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
