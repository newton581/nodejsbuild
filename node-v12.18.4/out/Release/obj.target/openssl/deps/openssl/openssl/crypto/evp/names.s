	.file	"names.c"
	.text
	.p2align 4
	.type	do_all_cipher_fn, @function
do_all_cipher_fn:
.LFB810:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %edx
	movq	8(%rsi), %rax
	movq	(%rsi), %rcx
	movq	16(%rdi), %r8
	movq	8(%rdi), %rsi
	testl	%edx, %edx
	je	.L2
	movq	%r8, %rdx
	xorl	%edi, %edi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L2:
	xorl	%edx, %edx
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE810:
	.size	do_all_cipher_fn, .-do_all_cipher_fn
	.p2align 4
	.type	do_all_md_fn, @function
do_all_md_fn:
.LFB813:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %edx
	movq	8(%rsi), %rax
	movq	(%rsi), %rcx
	movq	16(%rdi), %r8
	movq	8(%rdi), %rsi
	testl	%edx, %edx
	je	.L5
	movq	%r8, %rdx
	xorl	%edi, %edi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L5:
	xorl	%edx, %edx
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE813:
	.size	do_all_md_fn, .-do_all_md_fn
	.p2align 4
	.globl	EVP_add_cipher
	.type	EVP_add_cipher, @function
EVP_add_cipher:
.LFB805:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L14
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movl	(%rdi), %edi
	call	OBJ_nid2sn@PLT
	movq	%r12, %rdx
	movl	$2, %esi
	movq	%rax, %rdi
	call	OBJ_NAME_add@PLT
	testl	%eax, %eax
	jne	.L17
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movl	(%r12), %edi
	call	OBJ_nid2ln@PLT
	addq	$8, %rsp
	movq	%r12, %rdx
	movl	$2, %esi
	popq	%r12
	.cfi_restore 12
	movq	%rax, %rdi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	OBJ_NAME_add@PLT
	.p2align 4,,10
	.p2align 3
.L14:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE805:
	.size	EVP_add_cipher, .-EVP_add_cipher
	.p2align 4
	.globl	EVP_add_digest
	.type	EVP_add_digest, @function
EVP_add_digest:
.LFB806:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	(%rdi), %edi
	call	OBJ_nid2sn@PLT
	movq	%rbx, %rdx
	movl	$1, %esi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	OBJ_NAME_add@PLT
	testl	%eax, %eax
	jne	.L19
.L21:
	xorl	%eax, %eax
.L18:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movl	(%rbx), %edi
	call	OBJ_nid2ln@PLT
	movq	%rbx, %rdx
	movl	$1, %esi
	movq	%rax, %rdi
	call	OBJ_NAME_add@PLT
	testl	%eax, %eax
	je	.L21
	movl	4(%rbx), %edi
	testl	%edi, %edi
	je	.L18
	cmpl	(%rbx), %edi
	je	.L18
	call	OBJ_nid2sn@PLT
	movq	%r12, %rdx
	movl	$32769, %esi
	movq	%rax, %rdi
	call	OBJ_NAME_add@PLT
	testl	%eax, %eax
	je	.L21
	movl	4(%rbx), %edi
	call	OBJ_nid2ln@PLT
	popq	%rbx
	movq	%r12, %rdx
	movl	$32769, %esi
	popq	%r12
	movq	%rax, %rdi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	OBJ_NAME_add@PLT
	.cfi_endproc
.LFE806:
	.size	EVP_add_digest, .-EVP_add_digest
	.p2align 4
	.globl	EVP_get_cipherbyname
	.type	EVP_get_cipherbyname, @function
EVP_get_cipherbyname:
.LFB807:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$4, %edi
	subq	$8, %rsp
	call	OPENSSL_init_crypto@PLT
	testl	%eax, %eax
	je	.L33
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$2, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	OBJ_NAME_get@PLT
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE807:
	.size	EVP_get_cipherbyname, .-EVP_get_cipherbyname
	.p2align 4
	.globl	EVP_get_digestbyname
	.type	EVP_get_digestbyname, @function
EVP_get_digestbyname:
.LFB808:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$8, %edi
	subq	$8, %rsp
	call	OPENSSL_init_crypto@PLT
	testl	%eax, %eax
	je	.L36
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$1, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	OBJ_NAME_get@PLT
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE808:
	.size	EVP_get_digestbyname, .-EVP_get_digestbyname
	.p2align 4
	.globl	evp_cleanup_int
	.type	evp_cleanup_int, @function
evp_cleanup_int:
.LFB809:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	OBJ_NAME_cleanup@PLT
	movl	$1, %edi
	call	OBJ_NAME_cleanup@PLT
	movl	$-1, %edi
	call	OBJ_NAME_cleanup@PLT
	call	EVP_PBE_cleanup@PLT
	call	OBJ_sigid_free@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	evp_app_cleanup_int@PLT
	.cfi_endproc
.LFE809:
	.size	evp_cleanup_int, .-evp_cleanup_int
	.p2align 4
	.globl	EVP_CIPHER_do_all
	.type	EVP_CIPHER_do_all, @function
EVP_CIPHER_do_all:
.LFB811:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$4, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	xorl	%esi, %esi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	OPENSSL_init_crypto@PLT
	leaq	-48(%rbp), %rdx
	leaq	do_all_cipher_fn(%rip), %rsi
	movl	$2, %edi
	movq	%r12, -40(%rbp)
	movq	%rbx, -48(%rbp)
	call	OBJ_NAME_do_all@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L43
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L43:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE811:
	.size	EVP_CIPHER_do_all, .-EVP_CIPHER_do_all
	.p2align 4
	.globl	EVP_CIPHER_do_all_sorted
	.type	EVP_CIPHER_do_all_sorted, @function
EVP_CIPHER_do_all_sorted:
.LFB812:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$4, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	xorl	%esi, %esi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	OPENSSL_init_crypto@PLT
	leaq	-48(%rbp), %rdx
	leaq	do_all_cipher_fn(%rip), %rsi
	movl	$2, %edi
	movq	%r12, -40(%rbp)
	movq	%rbx, -48(%rbp)
	call	OBJ_NAME_do_all_sorted@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L47
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L47:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE812:
	.size	EVP_CIPHER_do_all_sorted, .-EVP_CIPHER_do_all_sorted
	.p2align 4
	.globl	EVP_MD_do_all
	.type	EVP_MD_do_all, @function
EVP_MD_do_all:
.LFB814:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$8, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	xorl	%esi, %esi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	OPENSSL_init_crypto@PLT
	leaq	-48(%rbp), %rdx
	leaq	do_all_md_fn(%rip), %rsi
	movl	$1, %edi
	movq	%r12, -40(%rbp)
	movq	%rbx, -48(%rbp)
	call	OBJ_NAME_do_all@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L51
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L51:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE814:
	.size	EVP_MD_do_all, .-EVP_MD_do_all
	.p2align 4
	.globl	EVP_MD_do_all_sorted
	.type	EVP_MD_do_all_sorted, @function
EVP_MD_do_all_sorted:
.LFB815:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$8, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	xorl	%esi, %esi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	OPENSSL_init_crypto@PLT
	leaq	-48(%rbp), %rdx
	leaq	do_all_md_fn(%rip), %rsi
	movl	$1, %edi
	movq	%r12, -40(%rbp)
	movq	%rbx, -48(%rbp)
	call	OBJ_NAME_do_all_sorted@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L55
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L55:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE815:
	.size	EVP_MD_do_all_sorted, .-EVP_MD_do_all_sorted
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
