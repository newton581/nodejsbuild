	.file	"dh_asn1.c"
	.text
	.p2align 4
	.type	dh_cb, @function
dh_cb:
.LFB445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	testl	%edi, %edi
	je	.L8
	movl	$1, %eax
	cmpl	$2, %edi
	je	.L9
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	call	DH_new@PLT
	testq	%rax, %rax
	movq	%rax, (%rbx)
	setne	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	addl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	movq	(%rsi), %rdi
	call	DH_free@PLT
	movq	$0, (%rbx)
	movl	$2, %eax
	jmp	.L1
	.cfi_endproc
.LFE445:
	.size	dh_cb, .-dh_cb
	.p2align 4
	.globl	d2i_DHparams
	.type	d2i_DHparams, @function
d2i_DHparams:
.LFB446:
	.cfi_startproc
	endbr64
	leaq	DHparams_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE446:
	.size	d2i_DHparams, .-d2i_DHparams
	.p2align 4
	.globl	i2d_DHparams
	.type	i2d_DHparams, @function
i2d_DHparams:
.LFB447:
	.cfi_startproc
	endbr64
	leaq	DHparams_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE447:
	.size	i2d_DHparams, .-i2d_DHparams
	.p2align 4
	.globl	d2i_int_dhx
	.type	d2i_int_dhx, @function
d2i_int_dhx:
.LFB448:
	.cfi_startproc
	endbr64
	leaq	DHxparams_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE448:
	.size	d2i_int_dhx, .-d2i_int_dhx
	.p2align 4
	.globl	i2d_int_dhx
	.type	i2d_int_dhx, @function
i2d_int_dhx:
.LFB449:
	.cfi_startproc
	endbr64
	leaq	DHxparams_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE449:
	.size	i2d_int_dhx, .-i2d_int_dhx
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/dh/dh_asn1.c"
	.text
	.p2align 4
	.globl	d2i_DHxparams
	.type	d2i_DHxparams, @function
d2i_DHxparams:
.LFB450:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	call	DH_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L14
	movq	%r13, %rsi
	leaq	DHxparams_it(%rip), %rcx
	movq	%r14, %rdx
	xorl	%edi, %edi
	call	ASN1_item_d2i@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L29
	testq	%rbx, %rbx
	je	.L17
	movq	(%rbx), %rdi
	call	DH_free@PLT
	movq	%r12, (%rbx)
.L17:
	movq	0(%r13), %rax
	movq	%rax, 8(%r12)
	movq	8(%r13), %rax
	movq	%rax, 64(%r12)
	movq	16(%r13), %rax
	movq	%rax, 16(%r12)
	movq	24(%r13), %rax
	movq	%rax, 72(%r12)
	movq	32(%r13), %rax
	testq	%rax, %rax
	je	.L18
	movq	(%rax), %rdi
	movq	8(%rax), %rax
	movq	8(%rdi), %rdx
	movq	%rdx, 80(%r12)
	movl	(%rdi), %edx
	movq	%rax, 96(%r12)
	movl	%edx, 88(%r12)
	movq	$0, 8(%rdi)
	call	ASN1_BIT_STRING_free@PLT
	movq	32(%r13), %rdi
	movl	$110, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 32(%r13)
.L18:
	movl	$114, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
.L14:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	DH_free@PLT
	jmp	.L14
	.cfi_endproc
.LFE450:
	.size	d2i_DHxparams, .-d2i_DHxparams
	.p2align 4
	.globl	i2d_DHxparams
	.type	i2d_DHxparams, @function
i2d_DHxparams:
.LFB451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$96, %rsp
	movdqu	8(%rdi), %xmm0
	movdqu	72(%rdi), %xmm2
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movdqu	56(%rdi), %xmm3
	movq	96(%rdi), %rax
	movdqa	%xmm0, %xmm1
	shufpd	$1, %xmm2, %xmm1
	shufpd	$2, %xmm3, %xmm0
	movaps	%xmm1, -32(%rbp)
	movaps	%xmm0, -48(%rbp)
	testq	%rax, %rax
	je	.L31
	movq	80(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L34
	movl	88(%rdi), %ecx
	testl	%ecx, %ecx
	jle	.L34
	movq	%rdx, -72(%rbp)
	leaq	-80(%rbp), %rdx
	movq	$8, -64(%rbp)
	movl	%ecx, -80(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%rax, -88(%rbp)
	leaq	-96(%rbp), %rax
.L31:
	leaq	-48(%rbp), %rdi
	leaq	DHxparams_it(%rip), %rdx
	movq	%rax, -16(%rbp)
	call	ASN1_item_i2d@PLT
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L39
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L31
.L39:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE451:
	.size	i2d_DHxparams, .-i2d_DHxparams
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"int_dhx942_dh"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	DHxparams_it, @object
	.size	DHxparams_it, 56
DHxparams_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	DHxparams_seq_tt
	.quad	5
	.quad	0
	.quad	40
	.quad	.LC1
	.section	.rodata.str1.1
.LC2:
	.string	"p"
.LC3:
	.string	"g"
.LC4:
	.string	"q"
.LC5:
	.string	"j"
.LC6:
	.string	"vparams"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	DHxparams_seq_tt, @object
	.size	DHxparams_seq_tt, 200
DHxparams_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC2
	.quad	BIGNUM_it
	.quad	0
	.quad	0
	.quad	16
	.quad	.LC3
	.quad	BIGNUM_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC4
	.quad	BIGNUM_it
	.quad	1
	.quad	0
	.quad	24
	.quad	.LC5
	.quad	BIGNUM_it
	.quad	1
	.quad	0
	.quad	32
	.quad	.LC6
	.quad	DHvparams_it
	.section	.rodata.str1.1
.LC7:
	.string	"int_dhvparams"
	.section	.data.rel.ro.local
	.align 32
	.type	DHvparams_it, @object
	.size	DHvparams_it, 56
DHvparams_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	DHvparams_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC7
	.section	.rodata.str1.1
.LC8:
	.string	"seed"
.LC9:
	.string	"counter"
	.section	.data.rel.ro
	.align 32
	.type	DHvparams_seq_tt, @object
	.size	DHvparams_seq_tt, 80
DHvparams_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC8
	.quad	ASN1_BIT_STRING_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC9
	.quad	BIGNUM_it
	.globl	DHparams_it
	.section	.rodata.str1.1
.LC10:
	.string	"DHparams"
	.section	.data.rel.ro.local
	.align 32
	.type	DHparams_it, @object
	.size	DHparams_it, 56
DHparams_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	DHparams_seq_tt
	.quad	3
	.quad	DHparams_aux
	.quad	144
	.quad	.LC10
	.section	.rodata.str1.1
.LC11:
	.string	"length"
	.section	.data.rel.ro
	.align 32
	.type	DHparams_seq_tt, @object
	.size	DHparams_seq_tt, 120
DHparams_seq_tt:
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC2
	.quad	BIGNUM_it
	.quad	0
	.quad	0
	.quad	16
	.quad	.LC3
	.quad	BIGNUM_it
	.quad	4097
	.quad	0
	.quad	24
	.quad	.LC11
	.quad	ZINT32_it
	.section	.data.rel.ro.local
	.align 32
	.type	DHparams_aux, @object
	.size	DHparams_aux, 40
DHparams_aux:
	.quad	0
	.long	0
	.long	0
	.long	0
	.zero	4
	.quad	dh_cb
	.long	0
	.zero	4
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
