	.file	"s3_enc.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/ssl/s3_enc.c"
	.text
	.p2align 4
	.globl	ssl3_change_cipher_state
	.type	ssl3_change_cipher_state, @function
ssl3_change_cipher_state:
.LFB1002:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$108, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	168(%rdi), %rax
	movq	640(%rax), %r12
	movq	632(%rax), %r13
	testq	%r12, %r12
	je	.L25
	movl	%esi, %r15d
	movl	%esi, %ebx
	andl	$1, %r15d
	jne	.L29
	movq	1136(%rdi), %rax
	movl	$1, 124(%rdi)
	movl	$1, -64(%rbp)
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L30
.L9:
	leaq	1160(%r14), %rdi
	movq	%r12, %rsi
	call	ssl_replace_hash@PLT
	testq	%rax, %rax
	je	.L31
	leaq	2112(%r14), %rdi
	call	RECORD_LAYER_reset_write_sequence@PLT
	movq	168(%r14), %rax
	movl	-64(%rbp), %r15d
	leaq	88(%rax), %rcx
	movq	%rcx, -72(%rbp)
	testl	%r15d, %r15d
	jne	.L32
.L12:
	movq	624(%rax), %rsi
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	EVP_MD_size@PLT
	movq	-64(%rbp), %rsi
	movl	$199, %r9d
	testl	%eax, %eax
	js	.L25
	movslq	%eax, %rdx
	movq	%r13, %rdi
	movq	%rsi, -80(%rbp)
	movq	%rdx, -64(%rbp)
	call	EVP_CIPHER_key_length@PLT
	movq	%r13, %rdi
	movslq	%eax, %r15
	call	EVP_CIPHER_iv_length@PLT
	movq	-64(%rbp), %rdx
	cmpl	$18, %ebx
	movq	-80(%rbp), %rsi
	cltq
	leaq	(%rdx,%rdx), %rdi
	je	.L20
	cmpl	$33, %ebx
	je	.L20
	addq	%r15, %rdi
	addq	%rax, %r15
	leaq	(%rsi,%rdi), %rcx
	addq	%r15, %rdi
	leaq	(%rsi,%rdi), %r15
	addq	%rdi, %rax
	addq	%rdx, %rsi
.L16:
	movq	168(%r14), %rdi
	cmpq	%rax, 616(%rdi)
	jb	.L33
	movq	-72(%rbp), %rdi
	movq	%rcx, -64(%rbp)
	andl	$2, %ebx
	call	memcpy@PLT
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %rdi
	movq	%r15, %r8
	movl	%ebx, %r9d
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	EVP_CipherInit_ex@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L34
	movl	$0, 124(%r14)
	movl	$1, %r15d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L33:
	movl	$226, %r9d
.L25:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
.L26:
	movl	$129, %edx
	movl	$80, %esi
	movq	%r14, %rdi
	xorl	%r15d, %r15d
	call	ossl_statem_fatal@PLT
.L1:
	addq	$40, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movq	1088(%rdi), %rax
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L35
.L5:
	leaq	1112(%r14), %rdi
	movq	%r12, %rsi
	call	ssl_replace_hash@PLT
	movl	$135, %r9d
	testq	%rax, %rax
	je	.L25
	leaq	2112(%r14), %rdi
	call	RECORD_LAYER_reset_read_sequence@PLT
	movq	168(%r14), %rax
	leaq	16(%rax), %rcx
	movq	%rcx, -72(%rbp)
	testl	%r15d, %r15d
	je	.L12
.L32:
	movq	-56(%rbp), %rdi
	call	EVP_CIPHER_CTX_reset@PLT
	movq	168(%r14), %rax
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L20:
	leaq	(%rsi,%rdi), %rcx
	leaq	(%rdi,%r15,2), %rdi
	leaq	(%rsi,%rdi), %r15
	leaq	(%rdi,%rax,2), %rax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L30:
	call	EVP_CIPHER_CTX_new@PLT
	movl	$160, %r9d
	movq	%rax, 1136(%r14)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L27
	call	EVP_CIPHER_CTX_reset@PLT
	movq	1136(%r14), %rax
	movl	$0, -64(%rbp)
	movq	%rax, -56(%rbp)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L35:
	call	EVP_CIPHER_CTX_new@PLT
	movl	$123, %r9d
	movl	$65, %ecx
	leaq	.LC0(%rip), %r8
	movq	%rax, 1088(%r14)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L26
	call	EVP_CIPHER_CTX_reset@PLT
	movq	1088(%r14), %rax
	xorl	%r15d, %r15d
	movq	%rax, -56(%rbp)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L31:
	movl	$171, %r9d
.L27:
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
	movl	$129, %edx
	movq	%r14, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L34:
	movl	$234, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r14, %rdi
	movl	$129, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L1
	.cfi_endproc
.LFE1002:
	.size	ssl3_change_cipher_state, .-ssl3_change_cipher_state
	.p2align 4
	.globl	ssl3_setup_key_block
	.type	ssl3_setup_key_block, @function
ssl3_setup_key_block:
.LFB1003:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	$1, %ebx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	cmpq	$0, 616(%rax)
	je	.L100
.L36:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L101
	leaq	-40(%rbp), %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	subq	$8, %rsp
	movq	%rdi, %r12
	movq	1296(%rdi), %rdi
	xorl	%ecx, %ecx
	pushq	$0
	leaq	-112(%rbp), %rdx
	leaq	-120(%rbp), %rsi
	xorl	%r8d, %r8d
	leaq	-104(%rbp), %r9
	call	ssl_cipher_get_evp@PLT
	popq	%rdx
	popq	%rcx
	movl	%eax, %ebx
	testl	%eax, %eax
	je	.L102
	movq	168(%r12), %rax
	movq	-120(%rbp), %xmm0
	xorl	%ebx, %ebx
	movq	-112(%rbp), %rdi
	movq	$0, 664(%rax)
	movhps	-112(%rbp), %xmm0
	movups	%xmm0, 632(%rax)
	call	EVP_MD_size@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	js	.L36
	movq	-120(%rbp), %rdi
	call	EVP_CIPHER_key_length@PLT
	movq	-120(%rbp), %rdi
	addl	%eax, %r13d
	call	EVP_CIPHER_iv_length@PLT
	movl	$315, %ecx
	leaq	.LC0(%rip), %rdx
	addl	%eax, %r13d
	movq	168(%r12), %rax
	leal	(%r13,%r13), %r14d
	movq	616(%rax), %rsi
	movq	624(%rax), %rdi
	movl	%r14d, -140(%rbp)
	movslq	%r14d, %r13
	call	CRYPTO_clear_free@PLT
	movq	168(%r12), %rax
	movl	$280, %edx
	movq	%r13, %rdi
	leaq	.LC0(%rip), %rsi
	movq	$0, 624(%rax)
	movq	$0, 616(%rax)
	call	CRYPTO_malloc@PLT
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L103
	movq	%rax, %rcx
	movq	168(%r12), %rax
	movq	%r13, 616(%rax)
	movq	%rcx, 624(%rax)
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r14
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r13
	testq	%r14, %r14
	je	.L61
	testq	%rax, %rax
	je	.L61
	movl	$8, %esi
	movq	%r14, %rdi
	call	EVP_MD_CTX_set_flags@PLT
	movl	-140(%rbp), %eax
	testl	%eax, %eax
	jle	.L104
	movl	-140(%rbp), %eax
	movl	$1, %r15d
	subl	$1, %eax
	shrl	$4, %eax
	addq	$1, %rax
	movq	%rax, -168(%rbp)
	movq	-160(%rbp), %rax
	movq	%rax, -152(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, -136(%rbp)
.L44:
	movq	-136(%rbp), %r9
	movl	%r15d, -144(%rbp)
	leal	64(%r15), %edi
	movl	%r15d, %ecx
	movq	%r9, %rdx
	cmpl	$8, %r15d
	jnb	.L105
.L45:
	andl	$7, %ecx
	je	.L49
	xorl	%eax, %eax
.L48:
	movl	%eax, %esi
	addl	$1, %eax
	movb	%dil, (%rdx,%rsi)
	cmpl	%ecx, %eax
	jb	.L48
.L49:
	call	EVP_sha1@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	je	.L51
	movq	-136(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L51
	movq	1296(%r12), %rsi
	movq	%r13, %rdi
	movq	8(%rsi), %rdx
	addq	$80, %rsi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L51
	movq	168(%r12), %rax
	movl	$32, %edx
	movq	%r13, %rdi
	leaq	152(%rax), %rsi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L51
	movq	168(%r12), %rax
	movl	$32, %edx
	movq	%r13, %rdi
	leaq	184(%rax), %rsi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L51
	leaq	-80(%rbp), %rbx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	je	.L51
	call	EVP_md5@PLT
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	je	.L51
	movq	1296(%r12), %rsi
	movq	%r14, %rdi
	movq	8(%rsi), %rdx
	addq	$80, %rsi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L51
	movl	$20, %edx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L51
	movl	-144(%rbp), %eax
	xorl	%edx, %edx
	sall	$4, %eax
	cmpl	%eax, -140(%rbp)
	jge	.L52
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	je	.L106
	movq	-152(%rbp), %rdi
	movl	-140(%rbp), %edx
	movq	%rbx, %rsi
	addl	-160(%rbp), %edx
	subl	%edi, %edx
	call	memcpy@PLT
.L54:
	addq	$16, -152(%rbp)
	cmpq	-168(%rbp), %r15
	je	.L107
	addq	$1, %r15
	cmpq	$17, %r15
	jne	.L44
	movl	$42, %r9d
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L102:
	movl	$258, %r9d
	leaq	.LC0(%rip), %r8
	movl	$138, %ecx
	movq	%r12, %rdi
	movl	$157, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L52:
	movq	-152(%rbp), %rsi
	movq	%r14, %rdi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	jne	.L54
	movl	%eax, %ebx
	movl	$74, %r9d
.L98:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	$238, %edx
	movq	%r12, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L105:
	movabsq	$72340172838076673, %rax
	movzbl	%dil, %edx
	movl	%r15d, %r8d
	imulq	%rax, %rdx
	andl	$-8, %r8d
	xorl	%eax, %eax
.L46:
	movl	%eax, %esi
	addl	$8, %eax
	movq	%rdx, (%r9,%rsi)
	cmpl	%r8d, %eax
	jb	.L46
	movq	-136(%rbp), %rbx
	leaq	(%rbx,%rax), %rdx
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L51:
	movl	$61, %r9d
.L97:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
.L96:
	movl	$238, %edx
	movl	$80, %esi
	movq	%r12, %rdi
	xorl	%ebx, %ebx
	call	ossl_statem_fatal@PLT
.L42:
	movq	%r14, %rdi
	call	EVP_MD_CTX_free@PLT
	movq	%r13, %rdi
	call	EVP_MD_CTX_free@PLT
	testb	$8, 1493(%r12)
	jne	.L36
	movq	1296(%r12), %rax
	movq	168(%r12), %rdx
	movq	504(%rax), %rax
	movl	$1, 216(%rdx)
	testq	%rax, %rax
	je	.L36
	movl	36(%rax), %eax
	cmpl	$32, %eax
	je	.L99
	cmpl	$4, %eax
	jne	.L36
.L99:
	movl	$0, 216(%rdx)
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L61:
	movl	$33, %r9d
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L103:
	movl	$281, %r9d
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
	movq	%r12, %rdi
	movl	$157, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L36
.L107:
	movq	%rbx, %rdi
.L56:
	movl	$20, %esi
	movl	$1, %ebx
	call	OPENSSL_cleanse@PLT
	jmp	.L42
.L106:
	movl	%eax, %ebx
	movl	$67, %r9d
	jmp	.L98
.L104:
	leaq	-80(%rbp), %rax
	movq	%rax, %rdi
	jmp	.L56
.L101:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1003:
	.size	ssl3_setup_key_block, .-ssl3_setup_key_block
	.p2align 4
	.globl	ssl3_cleanup_key_block
	.type	ssl3_cleanup_key_block, @function
ssl3_cleanup_key_block:
.LFB1004:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$315, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	168(%rdi), %rax
	movq	616(%rax), %rsi
	movq	624(%rax), %rdi
	call	CRYPTO_clear_free@PLT
	movq	168(%rbx), %rax
	movq	$0, 624(%rax)
	movq	$0, 616(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1004:
	.size	ssl3_cleanup_key_block, .-ssl3_cleanup_key_block
	.p2align 4
	.globl	ssl3_init_finished_mac
	.type	ssl3_init_finished_mac, @function
ssl3_init_finished_mac:
.LFB1005:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	call	BIO_s_mem@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	testq	%rax, %rax
	je	.L114
	movq	%rax, %r12
	movq	168(%r13), %rax
	movq	224(%rax), %rdi
	call	BIO_free@PLT
	movq	168(%r13), %rax
	movq	$0, 224(%rax)
	movq	232(%rax), %rdi
	call	EVP_MD_CTX_free@PLT
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	168(%r13), %rax
	movl	$9, %esi
	movq	%r12, 224(%rax)
	movq	$0, 232(%rax)
	call	BIO_ctrl@PLT
	popq	%r12
	movl	$1, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore_state
	movq	%r13, %rdi
	movl	$325, %r9d
	movl	$65, %ecx
	movl	$397, %edx
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1005:
	.size	ssl3_init_finished_mac, .-ssl3_init_finished_mac
	.p2align 4
	.globl	ssl3_free_digest_list
	.type	ssl3_free_digest_list, @function
ssl3_free_digest_list:
.LFB1006:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	168(%rdi), %rax
	movq	224(%rax), %rdi
	call	BIO_free@PLT
	movq	168(%rbx), %rax
	movq	$0, 224(%rax)
	movq	232(%rax), %rdi
	call	EVP_MD_CTX_free@PLT
	movq	168(%rbx), %rax
	movq	$0, 232(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1006:
	.size	ssl3_free_digest_list, .-ssl3_free_digest_list
	.p2align 4
	.globl	ssl3_finish_mac
	.type	ssl3_finish_mac, @function
ssl3_finish_mac:
.LFB1007:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	subq	$16, %rsp
	movq	168(%rdi), %rax
	movq	232(%rax), %rdi
	testq	%rdi, %rdi
	je	.L125
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L126
.L122:
	addq	$16, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_restore_state
	movl	$355, %r9d
	leaq	.LC0(%rip), %r8
	movl	$237, %ecx
	cmpq	$2147483647, %rdx
	ja	.L124
	movq	224(%rax), %rdi
	call	BIO_write@PLT
	cmpl	%r12d, %eax
	jne	.L121
	testl	%eax, %eax
	jg	.L122
.L121:
	movl	$361, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
.L124:
	movq	%r13, %rdi
	movl	$587, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L126:
	.cfi_restore_state
	movq	%r13, %rdi
	movl	$68, %ecx
	movl	$587, %edx
	movl	%eax, -20(%rbp)
	movl	$368, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movl	-20(%rbp), %eax
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1007:
	.size	ssl3_finish_mac, .-ssl3_finish_mac
	.p2align 4
	.globl	ssl3_digest_cached_records
	.type	ssl3_digest_cached_records, @function
ssl3_digest_cached_records:
.LFB1008:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	cmpq	$0, 232(%rax)
	je	.L144
.L128:
	movl	$1, %r13d
	testl	%ebx, %ebx
	je	.L145
.L127:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L146
	addq	$16, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	movq	168(%r12), %rax
	movq	224(%rax), %rdi
	call	BIO_free@PLT
	movq	168(%r12), %rax
	movq	$0, 224(%rax)
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L144:
	movq	224(%rax), %rdi
	leaq	-48(%rbp), %rcx
	xorl	%edx, %edx
	movl	$3, %esi
	call	BIO_ctrl@PLT
	movl	$385, %r9d
	movl	$332, %ecx
	leaq	.LC0(%rip), %r8
	movq	%rax, %r13
	testq	%rax, %rax
	jle	.L143
	movq	168(%r12), %r14
	call	EVP_MD_CTX_new@PLT
	movq	%rax, 232(%r14)
	movq	168(%r12), %rax
	cmpq	$0, 232(%rax)
	je	.L147
	movq	%r12, %rdi
	call	ssl_handshake_md@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L133
	movq	168(%r12), %rax
	xorl	%edx, %edx
	movq	232(%rax), %rdi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	jne	.L148
.L133:
	movl	$400, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
.L143:
	movl	$293, %edx
	movl	$80, %esi
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	ossl_statem_fatal@PLT
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L148:
	movq	168(%r12), %rax
	movq	-48(%rbp), %rsi
	movq	%r13, %rdx
	movq	232(%rax), %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jne	.L128
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L147:
	movl	$392, %r9d
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
	jmp	.L143
.L146:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1008:
	.size	ssl3_digest_cached_records, .-ssl3_digest_cached_records
	.p2align 4
	.globl	ssl3_final_finish_mac
	.type	ssl3_final_finish_mac, @function
ssl3_final_finish_mac:
.LFB1009:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	cmpq	$0, 232(%rax)
	movq	224(%rax), %rdi
	je	.L179
.L150:
	call	BIO_free@PLT
	movq	168(%r12), %rax
	movq	$0, 224(%rax)
	movq	232(%rax), %rdi
	call	EVP_MD_CTX_md@PLT
	movq	%rax, %rdi
	call	EVP_MD_type@PLT
	cmpl	$114, %eax
	je	.L155
	movl	$425, %r9d
	leaq	.LC0(%rip), %r8
	movl	$324, %ecx
.L177:
	movl	$285, %edx
	movl	$80, %esi
	movq	%r12, %rdi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L149:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L180
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L155:
	.cfi_restore_state
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L181
	movq	168(%r12), %rax
	movq	%rbx, %rdi
	movq	232(%rax), %rsi
	call	EVP_MD_CTX_copy_ex@PLT
	movl	$437, %r9d
	testl	%eax, %eax
	je	.L176
	movq	%rbx, %rdi
	call	EVP_MD_CTX_md@PLT
	movq	%rax, %rdi
	call	EVP_MD_size@PLT
	movl	$445, %r9d
	movl	%eax, -72(%rbp)
	testl	%eax, %eax
	js	.L176
	testq	%r13, %r13
	je	.L163
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jle	.L161
.L163:
	movq	1296(%r12), %rax
	movl	$29, %esi
	movq	%rbx, %rdi
	movq	8(%rax), %rdx
	leaq	80(%rax), %rcx
	call	EVP_MD_CTX_ctrl@PLT
	testl	%eax, %eax
	jg	.L182
.L161:
	movl	$456, %r9d
.L176:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	$285, %edx
	movq	%r12, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L158:
	movq	%rbx, %rdi
	movq	%rax, -72(%rbp)
	call	EVP_MD_CTX_free@PLT
	movq	-72(%rbp), %rax
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L179:
	leaq	-64(%rbp), %rcx
	xorl	%edx, %edx
	movl	$3, %esi
	call	BIO_ctrl@PLT
	movl	$385, %r9d
	movl	$332, %ecx
	leaq	.LC0(%rip), %r8
	movq	%rax, %rbx
	testq	%rax, %rax
	jle	.L178
	movq	168(%r12), %rdx
	movq	%rdx, -72(%rbp)
	call	EVP_MD_CTX_new@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, 232(%rdx)
	movq	168(%r12), %rax
	cmpq	$0, 232(%rax)
	je	.L183
	movq	%r12, %rdi
	call	ssl_handshake_md@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L154
	movq	168(%r12), %rax
	xorl	%edx, %edx
	movq	232(%rax), %rdi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	jne	.L184
.L154:
	movl	$400, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
.L178:
	movl	$293, %edx
	movl	$80, %esi
	movq	%r12, %rdi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L184:
	movq	168(%r12), %rax
	movq	-64(%rbp), %rsi
	movq	%rbx, %rdx
	movq	232(%rax), %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L154
	movq	168(%r12), %rax
	movq	224(%rax), %rdi
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L182:
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	jle	.L161
	movslq	-72(%rbp), %rax
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L181:
	movl	$432, %r9d
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L183:
	movl	$392, %r9d
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
	jmp	.L178
.L180:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1009:
	.size	ssl3_final_finish_mac, .-ssl3_final_finish_mac
	.p2align 4
	.globl	ssl3_generate_master_secret
	.type	ssl3_generate_master_secret, @function
ssl3_generate_master_secret:
.LFB1010:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	leaq	-128(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$152, %rsp
	movq	%rsi, -168(%rbp)
	movq	%r8, -192(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_MD_CTX_new@PLT
	movq	$0, -184(%rbp)
	movq	%rax, %r12
	leaq	salt.23404(%rip), %rax
	movq	%rax, -152(%rbp)
	leaq	-132(%rbp), %rax
	movq	%rax, -160(%rbp)
	testq	%r12, %r12
	je	.L196
.L190:
	movq	1440(%r13), %rax
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	248(%rax), %rsi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	jle	.L189
	movq	-152(%rbp), %rax
	movq	(%rax), %rsi
	movq	%rsi, %rdi
	movq	%rsi, -176(%rbp)
	call	strlen@PLT
	movq	-176(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jle	.L189
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jle	.L189
	movq	168(%r13), %rcx
	movl	$32, %edx
	movq	%r12, %rdi
	leaq	184(%rcx), %rsi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jle	.L189
	movq	168(%r13), %rcx
	movl	$32, %edx
	movq	%r12, %rdi
	leaq	152(%rcx), %rsi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jle	.L189
	movq	-160(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	jle	.L189
	movq	1440(%r13), %rax
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	240(%rax), %rsi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	jle	.L189
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jle	.L189
	movl	-132(%rbp), %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jle	.L189
	movq	-160(%rbp), %rdx
	movq	-168(%rbp), %rsi
	movq	%r12, %rdi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	jle	.L189
	movl	-132(%rbp), %eax
	addq	$8, -152(%rbp)
	leaq	24+salt.23404(%rip), %rdi
	addq	%rax, -168(%rbp)
	addq	%rax, -184(%rbp)
	movq	-152(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L190
	movq	%r12, %rdi
	call	EVP_MD_CTX_free@PLT
	movl	$64, %esi
	movq	%r15, %rdi
	call	OPENSSL_cleanse@PLT
	movq	-192(%rbp), %rax
	movq	-184(%rbp), %rcx
	movq	%rcx, (%rax)
	movl	$1, %eax
.L185:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L197
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore_state
	movl	$507, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r13, %rdi
	movl	$388, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movq	%r12, %rdi
	call	EVP_MD_CTX_free@PLT
	movl	$64, %esi
	movq	%r15, %rdi
	call	OPENSSL_cleanse@PLT
	xorl	%eax, %eax
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L196:
	movl	$488, %r9d
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
	movq	%r13, %rdi
	movl	$388, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L185
.L197:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1010:
	.size	ssl3_generate_master_secret, .-ssl3_generate_master_secret
	.p2align 4
	.globl	ssl3_alert_code
	.type	ssl3_alert_code, @function
ssl3_alert_code:
.LFB1011:
	.cfi_startproc
	endbr64
	movl	$-1, %eax
	cmpl	$120, %edi
	ja	.L198
	movl	%edi, %edi
	leaq	CSWTCH.25(%rip), %rax
	movsbl	(%rax,%rdi), %eax
.L198:
	ret
	.cfi_endproc
.LFE1011:
	.size	ssl3_alert_code, .-ssl3_alert_code
	.section	.rodata
	.align 32
	.type	CSWTCH.25, @object
	.size	CSWTCH.25, 121
CSWTCH.25:
	.byte	0
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	10
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	20
	.byte	20
	.byte	20
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	30
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	40
	.byte	41
	.byte	42
	.byte	43
	.byte	44
	.byte	45
	.byte	46
	.byte	47
	.byte	42
	.byte	40
	.byte	40
	.byte	40
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	40
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	40
	.byte	40
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	40
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	86
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	40
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	115
	.byte	40
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	120
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"A"
.LC2:
	.string	"BB"
.LC3:
	.string	"CCC"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	salt.23404, @object
	.size	salt.23404, 24
salt.23404:
	.quad	.LC1
	.quad	.LC2
	.quad	.LC3
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
