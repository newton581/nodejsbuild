	.file	"cms_err.c"
	.text
	.p2align 4
	.globl	ERR_load_CMS_strings
	.type	ERR_load_CMS_strings, @function
ERR_load_CMS_strings:
.LFB203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$772157440, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_func_error_string@PLT
	testq	%rax, %rax
	je	.L5
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	leaq	CMS_str_functs(%rip), %rdi
	call	ERR_load_strings_const@PLT
	leaq	CMS_str_reasons(%rip), %rdi
	call	ERR_load_strings_const@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE203:
	.size	ERR_load_CMS_strings, .-ERR_load_CMS_strings
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"add signer error"
.LC1:
	.string	"attribute error"
.LC2:
	.string	"certificate already present"
.LC3:
	.string	"certificate has no keyid"
.LC4:
	.string	"certificate verify error"
.LC5:
	.string	"cipher initialisation error"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"cipher parameter initialisation error"
	.section	.rodata.str1.1
.LC7:
	.string	"cms datafinal error"
.LC8:
	.string	"cms lib"
.LC9:
	.string	"contentidentifier mismatch"
.LC10:
	.string	"content not found"
.LC11:
	.string	"content type mismatch"
	.section	.rodata.str1.8
	.align 8
.LC12:
	.string	"content type not compressed data"
	.align 8
.LC13:
	.string	"content type not enveloped data"
	.section	.rodata.str1.1
.LC14:
	.string	"content type not signed data"
.LC15:
	.string	"content verify error"
.LC16:
	.string	"ctrl error"
.LC17:
	.string	"ctrl failure"
.LC18:
	.string	"decrypt error"
.LC19:
	.string	"error getting public key"
	.section	.rodata.str1.8
	.align 8
.LC20:
	.string	"error reading messagedigest attribute"
	.section	.rodata.str1.1
.LC21:
	.string	"error setting key"
.LC22:
	.string	"error setting recipientinfo"
.LC23:
	.string	"invalid encrypted key length"
	.section	.rodata.str1.8
	.align 8
.LC24:
	.string	"invalid key encryption parameter"
	.section	.rodata.str1.1
.LC25:
	.string	"invalid key length"
.LC26:
	.string	"md bio init error"
	.section	.rodata.str1.8
	.align 8
.LC27:
	.string	"messagedigest attribute wrong length"
	.section	.rodata.str1.1
.LC28:
	.string	"messagedigest wrong length"
.LC29:
	.string	"msgsigdigest error"
	.section	.rodata.str1.8
	.align 8
.LC30:
	.string	"msgsigdigest verification failure"
	.section	.rodata.str1.1
.LC31:
	.string	"msgsigdigest wrong length"
.LC32:
	.string	"need one signer"
.LC33:
	.string	"not a signed receipt"
.LC34:
	.string	"not encrypted data"
.LC35:
	.string	"not kek"
.LC36:
	.string	"not key agreement"
.LC37:
	.string	"not key transport"
.LC38:
	.string	"not pwri"
	.section	.rodata.str1.8
	.align 8
.LC39:
	.string	"not supported for this key type"
	.section	.rodata.str1.1
.LC40:
	.string	"no cipher"
.LC41:
	.string	"no content"
.LC42:
	.string	"no content type"
.LC43:
	.string	"no default digest"
.LC44:
	.string	"no digest set"
.LC45:
	.string	"no key"
.LC46:
	.string	"no key or cert"
.LC47:
	.string	"no matching digest"
.LC48:
	.string	"no matching recipient"
.LC49:
	.string	"no matching signature"
.LC50:
	.string	"no msgsigdigest"
.LC51:
	.string	"no password"
.LC52:
	.string	"no private key"
.LC53:
	.string	"no public key"
.LC54:
	.string	"no receipt request"
.LC55:
	.string	"no signers"
	.section	.rodata.str1.8
	.align 8
.LC56:
	.string	"private key does not match certificate"
	.section	.rodata.str1.1
.LC57:
	.string	"receipt decode error"
.LC58:
	.string	"recipient error"
.LC59:
	.string	"signer certificate not found"
.LC60:
	.string	"signfinal error"
.LC61:
	.string	"smime text error"
.LC62:
	.string	"store init error"
.LC63:
	.string	"type not compressed data"
.LC64:
	.string	"type not data"
.LC65:
	.string	"type not digested data"
.LC66:
	.string	"type not encrypted data"
.LC67:
	.string	"type not enveloped data"
.LC68:
	.string	"unable to finalize context"
.LC69:
	.string	"unknown cipher"
.LC70:
	.string	"unknown digest algorithm"
.LC71:
	.string	"unknown id"
	.section	.rodata.str1.8
	.align 8
.LC72:
	.string	"unsupported compression algorithm"
	.section	.rodata.str1.1
.LC73:
	.string	"unsupported content type"
.LC74:
	.string	"unsupported kek algorithm"
	.section	.rodata.str1.8
	.align 8
.LC75:
	.string	"unsupported key encryption algorithm"
	.align 8
.LC76:
	.string	"unsupported recipientinfo type"
	.section	.rodata.str1.1
.LC77:
	.string	"unsupported recipient type"
.LC78:
	.string	"unsupported type"
.LC79:
	.string	"unwrap error"
.LC80:
	.string	"unwrap failure"
.LC81:
	.string	"verification failure"
.LC82:
	.string	"wrap error"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	CMS_str_reasons, @object
	.size	CMS_str_reasons, 1344
CMS_str_reasons:
	.quad	771752035
	.quad	.LC0
	.quad	771752097
	.quad	.LC1
	.quad	771752111
	.quad	.LC2
	.quad	771752096
	.quad	.LC3
	.quad	771752036
	.quad	.LC4
	.quad	771752037
	.quad	.LC5
	.quad	771752038
	.quad	.LC6
	.quad	771752039
	.quad	.LC7
	.quad	771752040
	.quad	.LC8
	.quad	771752106
	.quad	.LC9
	.quad	771752041
	.quad	.LC10
	.quad	771752107
	.quad	.LC11
	.quad	771752042
	.quad	.LC12
	.quad	771752043
	.quad	.LC13
	.quad	771752044
	.quad	.LC14
	.quad	771752045
	.quad	.LC15
	.quad	771752046
	.quad	.LC16
	.quad	771752047
	.quad	.LC17
	.quad	771752048
	.quad	.LC18
	.quad	771752049
	.quad	.LC19
	.quad	771752050
	.quad	.LC20
	.quad	771752051
	.quad	.LC21
	.quad	771752052
	.quad	.LC22
	.quad	771752053
	.quad	.LC23
	.quad	771752112
	.quad	.LC24
	.quad	771752054
	.quad	.LC25
	.quad	771752055
	.quad	.LC26
	.quad	771752056
	.quad	.LC27
	.quad	771752057
	.quad	.LC28
	.quad	771752108
	.quad	.LC29
	.quad	771752098
	.quad	.LC30
	.quad	771752099
	.quad	.LC31
	.quad	771752100
	.quad	.LC32
	.quad	771752101
	.quad	.LC33
	.quad	771752058
	.quad	.LC34
	.quad	771752059
	.quad	.LC35
	.quad	771752117
	.quad	.LC36
	.quad	771752060
	.quad	.LC37
	.quad	771752113
	.quad	.LC38
	.quad	771752061
	.quad	.LC39
	.quad	771752062
	.quad	.LC40
	.quad	771752063
	.quad	.LC41
	.quad	771752109
	.quad	.LC42
	.quad	771752064
	.quad	.LC43
	.quad	771752065
	.quad	.LC44
	.quad	771752066
	.quad	.LC45
	.quad	771752110
	.quad	.LC46
	.quad	771752067
	.quad	.LC47
	.quad	771752068
	.quad	.LC48
	.quad	771752102
	.quad	.LC49
	.quad	771752103
	.quad	.LC50
	.quad	771752114
	.quad	.LC51
	.quad	771752069
	.quad	.LC52
	.quad	771752070
	.quad	.LC53
	.quad	771752104
	.quad	.LC54
	.quad	771752071
	.quad	.LC55
	.quad	771752072
	.quad	.LC56
	.quad	771752105
	.quad	.LC57
	.quad	771752073
	.quad	.LC58
	.quad	771752074
	.quad	.LC59
	.quad	771752075
	.quad	.LC60
	.quad	771752076
	.quad	.LC61
	.quad	771752077
	.quad	.LC62
	.quad	771752078
	.quad	.LC63
	.quad	771752079
	.quad	.LC64
	.quad	771752080
	.quad	.LC65
	.quad	771752081
	.quad	.LC66
	.quad	771752082
	.quad	.LC67
	.quad	771752083
	.quad	.LC68
	.quad	771752084
	.quad	.LC69
	.quad	771752085
	.quad	.LC70
	.quad	771752086
	.quad	.LC71
	.quad	771752087
	.quad	.LC72
	.quad	771752088
	.quad	.LC73
	.quad	771752089
	.quad	.LC74
	.quad	771752115
	.quad	.LC75
	.quad	771752091
	.quad	.LC76
	.quad	771752090
	.quad	.LC77
	.quad	771752092
	.quad	.LC78
	.quad	771752093
	.quad	.LC79
	.quad	771752116
	.quad	.LC80
	.quad	771752094
	.quad	.LC81
	.quad	771752095
	.quad	.LC82
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC83:
	.string	"check_content"
.LC84:
	.string	"CMS_add0_cert"
.LC85:
	.string	"CMS_add0_recipient_key"
.LC86:
	.string	"CMS_add0_recipient_password"
.LC87:
	.string	"CMS_add1_ReceiptRequest"
.LC88:
	.string	"CMS_add1_recipient_cert"
.LC89:
	.string	"CMS_add1_signer"
.LC90:
	.string	"cms_add1_signingTime"
.LC91:
	.string	"CMS_compress"
.LC92:
	.string	"cms_CompressedData_create"
.LC93:
	.string	"cms_CompressedData_init_bio"
.LC94:
	.string	"cms_copy_content"
.LC95:
	.string	"cms_copy_messageDigest"
.LC96:
	.string	"CMS_data"
.LC97:
	.string	"CMS_dataFinal"
.LC98:
	.string	"CMS_dataInit"
.LC99:
	.string	"CMS_decrypt"
.LC100:
	.string	"CMS_decrypt_set1_key"
.LC101:
	.string	"CMS_decrypt_set1_password"
.LC102:
	.string	"CMS_decrypt_set1_pkey"
.LC103:
	.string	"cms_DigestAlgorithm_find_ctx"
.LC104:
	.string	"cms_DigestAlgorithm_init_bio"
.LC105:
	.string	"cms_DigestedData_do_final"
.LC106:
	.string	"CMS_digest_verify"
.LC107:
	.string	"cms_encode_Receipt"
.LC108:
	.string	"CMS_encrypt"
.LC109:
	.string	"cms_EncryptedContent_init"
.LC110:
	.string	"cms_EncryptedContent_init_bio"
.LC111:
	.string	"CMS_EncryptedData_decrypt"
.LC112:
	.string	"CMS_EncryptedData_encrypt"
.LC113:
	.string	"CMS_EncryptedData_set1_key"
.LC114:
	.string	"CMS_EnvelopedData_create"
.LC115:
	.string	"cms_EnvelopedData_init_bio"
.LC116:
	.string	"cms_enveloped_data_init"
.LC117:
	.string	"cms_env_asn1_ctrl"
.LC118:
	.string	"CMS_final"
.LC119:
	.string	"cms_get0_certificate_choices"
.LC120:
	.string	"CMS_get0_content"
.LC121:
	.string	"cms_get0_econtent_type"
.LC122:
	.string	"cms_get0_enveloped"
.LC123:
	.string	"cms_get0_revocation_choices"
.LC124:
	.string	"cms_get0_signed"
.LC125:
	.string	"cms_msgSigDigest_add1"
.LC126:
	.string	"CMS_ReceiptRequest_create0"
.LC127:
	.string	"cms_Receipt_verify"
.LC128:
	.string	"CMS_RecipientInfo_decrypt"
.LC129:
	.string	"CMS_RecipientInfo_encrypt"
	.section	.rodata.str1.8
	.align 8
.LC130:
	.string	"cms_RecipientInfo_kari_encrypt"
	.align 8
.LC131:
	.string	"CMS_RecipientInfo_kari_get0_alg"
	.align 8
.LC132:
	.string	"CMS_RecipientInfo_kari_get0_orig_id"
	.align 8
.LC133:
	.string	"CMS_RecipientInfo_kari_get0_reks"
	.align 8
.LC134:
	.string	"CMS_RecipientInfo_kari_orig_id_cmp"
	.align 8
.LC135:
	.string	"cms_RecipientInfo_kekri_decrypt"
	.align 8
.LC136:
	.string	"cms_RecipientInfo_kekri_encrypt"
	.align 8
.LC137:
	.string	"CMS_RecipientInfo_kekri_get0_id"
	.align 8
.LC138:
	.string	"CMS_RecipientInfo_kekri_id_cmp"
	.align 8
.LC139:
	.string	"CMS_RecipientInfo_ktri_cert_cmp"
	.align 8
.LC140:
	.string	"cms_RecipientInfo_ktri_decrypt"
	.align 8
.LC141:
	.string	"cms_RecipientInfo_ktri_encrypt"
	.align 8
.LC142:
	.string	"CMS_RecipientInfo_ktri_get0_algs"
	.align 8
.LC143:
	.string	"CMS_RecipientInfo_ktri_get0_signer_id"
	.section	.rodata.str1.1
.LC144:
	.string	"cms_RecipientInfo_pwri_crypt"
.LC145:
	.string	"CMS_RecipientInfo_set0_key"
	.section	.rodata.str1.8
	.align 8
.LC146:
	.string	"CMS_RecipientInfo_set0_password"
	.section	.rodata.str1.1
.LC147:
	.string	"CMS_RecipientInfo_set0_pkey"
.LC148:
	.string	"cms_sd_asn1_ctrl"
.LC149:
	.string	"cms_set1_ias"
.LC150:
	.string	"cms_set1_keyid"
.LC151:
	.string	"cms_set1_SignerIdentifier"
.LC152:
	.string	"CMS_set_detached"
.LC153:
	.string	"CMS_sign"
.LC154:
	.string	"cms_signed_data_init"
.LC155:
	.string	"cms_SignerInfo_content_sign"
.LC156:
	.string	"CMS_SignerInfo_sign"
.LC157:
	.string	"CMS_SignerInfo_verify"
.LC158:
	.string	"cms_signerinfo_verify_cert"
.LC159:
	.string	"CMS_SignerInfo_verify_content"
.LC160:
	.string	"CMS_sign_receipt"
.LC161:
	.string	"CMS_si_check_attributes"
.LC162:
	.string	"CMS_stream"
.LC163:
	.string	"CMS_uncompress"
.LC164:
	.string	"CMS_verify"
.LC165:
	.string	"kek_unwrap_key"
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_str_functs, @object
	.size	CMS_str_functs, 1344
CMS_str_functs:
	.quad	772157440
	.quad	.LC83
	.quad	772423680
	.quad	.LC84
	.quad	772161536
	.quad	.LC85
	.quad	772427776
	.quad	.LC86
	.quad	772399104
	.quad	.LC87
	.quad	772165632
	.quad	.LC88
	.quad	772169728
	.quad	.LC89
	.quad	772173824
	.quad	.LC90
	.quad	772177920
	.quad	.LC91
	.quad	772182016
	.quad	.LC92
	.quad	772186112
	.quad	.LC93
	.quad	772190208
	.quad	.LC94
	.quad	772194304
	.quad	.LC95
	.quad	772198400
	.quad	.LC96
	.quad	772202496
	.quad	.LC97
	.quad	772206592
	.quad	.LC98
	.quad	772210688
	.quad	.LC99
	.quad	772214784
	.quad	.LC100
	.quad	772431872
	.quad	.LC101
	.quad	772218880
	.quad	.LC102
	.quad	772222976
	.quad	.LC103
	.quad	772227072
	.quad	.LC104
	.quad	772231168
	.quad	.LC105
	.quad	772235264
	.quad	.LC106
	.quad	772411392
	.quad	.LC107
	.quad	772239360
	.quad	.LC108
	.quad	772485120
	.quad	.LC109
	.quad	772243456
	.quad	.LC110
	.quad	772247552
	.quad	.LC111
	.quad	772251648
	.quad	.LC112
	.quad	772255744
	.quad	.LC113
	.quad	772259840
	.quad	.LC114
	.quad	772263936
	.quad	.LC115
	.quad	772268032
	.quad	.LC116
	.quad	772452352
	.quad	.LC117
	.quad	772272128
	.quad	.LC118
	.quad	772276224
	.quad	.LC119
	.quad	772280320
	.quad	.LC120
	.quad	772284416
	.quad	.LC121
	.quad	772288512
	.quad	.LC122
	.quad	772292608
	.quad	.LC123
	.quad	772296704
	.quad	.LC124
	.quad	772415488
	.quad	.LC125
	.quad	772403200
	.quad	.LC126
	.quad	772407296
	.quad	.LC127
	.quad	772300800
	.quad	.LC128
	.quad	772444160
	.quad	.LC129
	.quad	772481024
	.quad	.LC130
	.quad	772468736
	.quad	.LC131
	.quad	772460544
	.quad	.LC132
	.quad	772456448
	.quad	.LC133
	.quad	772464640
	.quad	.LC134
	.quad	772304896
	.quad	.LC135
	.quad	772308992
	.quad	.LC136
	.quad	772313088
	.quad	.LC137
	.quad	772317184
	.quad	.LC138
	.quad	772321280
	.quad	.LC139
	.quad	772325376
	.quad	.LC140
	.quad	772329472
	.quad	.LC141
	.quad	772333568
	.quad	.LC142
	.quad	772337664
	.quad	.LC143
	.quad	772435968
	.quad	.LC144
	.quad	772341760
	.quad	.LC145
	.quad	772440064
	.quad	.LC146
	.quad	772345856
	.quad	.LC147
	.quad	772448256
	.quad	.LC148
	.quad	772472832
	.quad	.LC149
	.quad	772476928
	.quad	.LC150
	.quad	772349952
	.quad	.LC151
	.quad	772354048
	.quad	.LC152
	.quad	772358144
	.quad	.LC153
	.quad	772362240
	.quad	.LC154
	.quad	772366336
	.quad	.LC155
	.quad	772370432
	.quad	.LC156
	.quad	772374528
	.quad	.LC157
	.quad	772378624
	.quad	.LC158
	.quad	772382720
	.quad	.LC159
	.quad	772419584
	.quad	.LC160
	.quad	772501504
	.quad	.LC161
	.quad	772386816
	.quad	.LC162
	.quad	772390912
	.quad	.LC163
	.quad	772395008
	.quad	.LC164
	.quad	772489216
	.quad	.LC165
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
