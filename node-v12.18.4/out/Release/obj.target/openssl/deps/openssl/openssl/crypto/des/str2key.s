	.file	"str2key.c"
	.text
	.p2align 4
	.globl	DES_string_to_key
	.type	DES_string_to_key, @function
DES_string_to_key:
.LFB151:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$144, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, (%rsi)
	call	strlen@PLT
	xorl	%esi, %esi
	movq	%rax, %rbx
	leal	-1(%rax), %edx
	testl	%eax, %eax
	jg	.L5
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L14:
	movq	%rsi, %rax
	addl	%ecx, %ecx
	andl	$7, %eax
	xorb	%cl, 0(%r13,%rax)
	leaq	1(%rsi), %rax
	cmpq	%rdx, %rsi
	je	.L6
.L8:
	movq	%rax, %rsi
.L5:
	movzbl	(%r12,%rsi), %ecx
	testb	$8, %sil
	je	.L14
	rolb	$4, %cl
	leal	0(,%rcx,4), %eax
	shrb	$2, %cl
	andl	$-52, %eax
	andl	$51, %ecx
	orl	%eax, %ecx
	leal	(%rcx,%rcx), %eax
	shrb	%cl
	andl	$-86, %eax
	andl	$85, %ecx
	orl	%eax, %ecx
	movl	%esi, %eax
	notl	%eax
	andl	$7, %eax
	xorb	%cl, 0(%r13,%rax)
	leaq	1(%rsi), %rax
	cmpq	%rdx, %rsi
	jne	.L8
.L6:
	leaq	-176(%rbp), %r14
	movq	%r13, %rdi
	call	DES_set_odd_parity@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	DES_set_key_unchecked@PLT
	movslq	%ebx, %rdx
	movq	%r13, %r8
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	DES_cbc_cksum@PLT
	movl	$128, %esi
	movq	%r14, %rdi
	call	OPENSSL_cleanse@PLT
	movq	%r13, %rdi
	call	DES_set_odd_parity@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L15
	addq	$144, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L15:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE151:
	.size	DES_string_to_key, .-DES_string_to_key
	.p2align 4
	.globl	DES_string_to_2keys
	.type	DES_string_to_2keys, @function
DES_string_to_2keys:
.LFB152:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, (%rsi)
	movq	$0, (%rdx)
	call	strlen@PLT
	xorl	%ecx, %ecx
	movq	%rax, %rbx
	leal	-1(%rax), %edx
	testl	%eax, %eax
	jg	.L24
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L33:
	movl	%ecx, %esi
	addl	%edi, %edi
	andl	$7, %esi
	movslq	%esi, %rsi
	testl	%eax, %eax
	jne	.L21
	xorb	%dil, 0(%r13,%rsi)
.L22:
	leaq	1(%rcx), %rax
	cmpq	%rdx, %rcx
	je	.L32
.L27:
	movq	%rax, %rcx
.L24:
	movzbl	(%r14,%rcx), %edi
	movl	%ecx, %eax
	andl	$8, %eax
	movl	%edi, %esi
	testb	$16, %cl
	je	.L33
	sall	$4, %edi
	shrb	$4, %sil
	orl	%edi, %esi
	leal	0(,%rsi,4), %edi
	shrb	$2, %sil
	andl	$-52, %edi
	andl	$51, %esi
	orl	%edi, %esi
	leal	(%rsi,%rsi), %edi
	shrb	%sil
	andl	$-86, %edi
	andl	$85, %esi
	orl	%edi, %esi
	movl	%ecx, %edi
	notl	%edi
	andl	$7, %edi
	movslq	%edi, %rdi
	testl	%eax, %eax
	jne	.L23
	xorb	%sil, 0(%r13,%rdi)
	leaq	1(%rcx), %rax
	cmpq	%rdx, %rcx
	jne	.L27
.L32:
	cmpl	$8, %ebx
	jle	.L18
.L19:
	movq	%r13, %rdi
	leaq	-192(%rbp), %r15
	movslq	%ebx, %rbx
	call	DES_set_odd_parity@PLT
	movq	%r12, %rdi
	call	DES_set_odd_parity@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	DES_set_key_unchecked@PLT
	movq	%r13, %r8
	movq	%r15, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	DES_cbc_cksum@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	DES_set_key_unchecked@PLT
	movq	%r12, %r8
	movq	%r15, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	DES_cbc_cksum@PLT
	movl	$128, %esi
	movq	%r15, %rdi
	call	OPENSSL_cleanse@PLT
	movq	%r13, %rdi
	call	DES_set_odd_parity@PLT
	movq	%r12, %rdi
	call	DES_set_odd_parity@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L34
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	xorb	%dil, (%r12,%rsi)
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L23:
	xorb	%sil, (%r12,%rdi)
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L18:
	movq	0(%r13), %rax
	movq	%rax, (%r12)
	jmp	.L19
.L34:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE152:
	.size	DES_string_to_2keys, .-DES_string_to_2keys
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
