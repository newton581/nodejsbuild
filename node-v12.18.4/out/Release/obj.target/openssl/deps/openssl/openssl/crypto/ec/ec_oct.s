	.file	"ec_oct.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ec/ec_oct.c"
	.text
	.p2align 4
	.globl	EC_POINT_set_compressed_coordinates
	.type	EC_POINT_set_compressed_coordinates, @function
EC_POINT_set_compressed_coordinates:
.LFB388:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	152(%rax), %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%r9, %r9
	je	.L19
.L2:
	cmpq	(%rsi), %rax
	je	.L20
.L4:
	movl	$28, %r8d
	movl	$101, %edx
	movl	$295, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L1:
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movl	32(%rdi), %r10d
	testl	%r10d, %r10d
	je	.L5
	movl	8(%rsi), %r11d
	testl	%r11d, %r11d
	je	.L5
	cmpl	%r11d, %r10d
	jne	.L4
.L5:
	testb	$1, (%rax)
	je	.L6
	cmpl	$406, 4(%rax)
	je	.L21
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ec_GF2m_simple_set_compressed_coordinates@PLT
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	testb	$1, (%rax)
	jne	.L2
	movl	$23, %r8d
	movl	$66, %edx
	movl	$295, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L6:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%r9
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	ec_GFp_simple_set_compressed_coordinates@PLT
	.cfi_endproc
.LFE388:
	.size	EC_POINT_set_compressed_coordinates, .-EC_POINT_set_compressed_coordinates
	.p2align 4
	.globl	EC_POINT_set_compressed_coordinates_GFp
	.type	EC_POINT_set_compressed_coordinates_GFp, @function
EC_POINT_set_compressed_coordinates_GFp:
.LFB389:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	152(%rax), %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%r9, %r9
	je	.L39
.L23:
	cmpq	(%rsi), %rax
	je	.L40
.L25:
	movl	$28, %r8d
	movl	$101, %edx
	movl	$295, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L22:
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	movl	32(%rdi), %r10d
	testl	%r10d, %r10d
	je	.L26
	movl	8(%rsi), %r11d
	testl	%r11d, %r11d
	je	.L26
	cmpl	%r11d, %r10d
	jne	.L25
.L26:
	testb	$1, (%rax)
	je	.L27
	cmpl	$406, 4(%rax)
	je	.L41
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ec_GF2m_simple_set_compressed_coordinates@PLT
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	testb	$1, (%rax)
	jne	.L23
	movl	$23, %r8d
	movl	$66, %edx
	movl	$295, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L27:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%r9
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	ec_GFp_simple_set_compressed_coordinates@PLT
	.cfi_endproc
.LFE389:
	.size	EC_POINT_set_compressed_coordinates_GFp, .-EC_POINT_set_compressed_coordinates_GFp
	.p2align 4
	.globl	EC_POINT_set_compressed_coordinates_GF2m
	.type	EC_POINT_set_compressed_coordinates_GF2m, @function
EC_POINT_set_compressed_coordinates_GF2m:
.LFB396:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	152(%rax), %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%r9, %r9
	je	.L59
.L43:
	cmpq	(%rsi), %rax
	je	.L60
.L45:
	movl	$28, %r8d
	movl	$101, %edx
	movl	$295, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L42:
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	movl	32(%rdi), %r10d
	testl	%r10d, %r10d
	je	.L46
	movl	8(%rsi), %r11d
	testl	%r11d, %r11d
	je	.L46
	cmpl	%r11d, %r10d
	jne	.L45
.L46:
	testb	$1, (%rax)
	je	.L47
	cmpl	$406, 4(%rax)
	je	.L61
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ec_GF2m_simple_set_compressed_coordinates@PLT
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	testb	$1, (%rax)
	jne	.L43
	movl	$23, %r8d
	movl	$66, %edx
	movl	$295, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L47:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%r9
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	ec_GFp_simple_set_compressed_coordinates@PLT
	.cfi_endproc
.LFE396:
	.size	EC_POINT_set_compressed_coordinates_GF2m, .-EC_POINT_set_compressed_coordinates_GF2m
	.p2align 4
	.globl	EC_POINT_point2oct
	.type	EC_POINT_point2oct, @function
EC_POINT_point2oct:
.LFB391:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	160(%rax), %r10
	testq	%r10, %r10
	je	.L79
.L63:
	cmpq	(%rsi), %rax
	je	.L80
.L65:
	movl	$80, %r8d
	movl	$101, %edx
	movl	$123, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L62:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	movl	32(%rdi), %r11d
	testl	%r11d, %r11d
	je	.L66
	movl	8(%rsi), %ebx
	testl	%ebx, %ebx
	je	.L66
	cmpl	%ebx, %r11d
	jne	.L65
.L66:
	testb	$1, (%rax)
	je	.L67
	cmpl	$406, 4(%rax)
	je	.L81
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ec_GF2m_simple_point2oct@PLT
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	testb	$1, (%rax)
	jne	.L63
	movl	$76, %r8d
	movl	$66, %edx
	movl	$123, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L67:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%r10
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	ec_GFp_simple_point2oct@PLT
	.cfi_endproc
.LFE391:
	.size	EC_POINT_point2oct, .-EC_POINT_point2oct
	.p2align 4
	.globl	EC_POINT_oct2point
	.type	EC_POINT_oct2point, @function
EC_POINT_oct2point:
.LFB392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	168(%rax), %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%r9, %r9
	je	.L99
.L83:
	cmpq	(%rsi), %rax
	je	.L100
.L85:
	movl	$110, %r8d
	movl	$101, %edx
	movl	$122, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L82:
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	movl	32(%rdi), %r10d
	testl	%r10d, %r10d
	je	.L86
	movl	8(%rsi), %r11d
	testl	%r11d, %r11d
	je	.L86
	cmpl	%r11d, %r10d
	jne	.L85
.L86:
	testb	$1, (%rax)
	je	.L87
	cmpl	$406, 4(%rax)
	je	.L101
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ec_GF2m_simple_oct2point@PLT
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore_state
	testb	$1, (%rax)
	jne	.L83
	movl	$106, %r8d
	movl	$66, %edx
	movl	$122, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L87:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%r9
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	ec_GFp_simple_oct2point@PLT
	.cfi_endproc
.LFE392:
	.size	EC_POINT_oct2point, .-EC_POINT_oct2point
	.p2align 4
	.globl	EC_POINT_point2buf
	.type	EC_POINT_point2buf, @function
EC_POINT_point2buf:
.LFB393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	movq	(%rdi), %rax
	movq	%rcx, -56(%rbp)
	movq	160(%rax), %r11
	testq	%r11, %r11
	je	.L125
.L103:
	cmpq	(%r15), %rax
	je	.L126
.L105:
	movl	$80, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$101, %edx
	xorl	%r12d, %r12d
	movl	$123, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
.L102:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L126:
	.cfi_restore_state
	movl	32(%r13), %edx
	testl	%edx, %edx
	je	.L106
	movl	8(%r15), %ecx
	testl	%ecx, %ecx
	je	.L106
	cmpl	%ecx, %edx
	jne	.L105
.L106:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	%ebx, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	testb	$1, (%rax)
	jne	.L127
	call	*%r11
	movq	%rax, %r12
.L109:
	testq	%r12, %r12
	je	.L102
	movl	$139, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L128
	movq	%r12, %r8
	movq	%r14, %r9
	movq	%rax, %rcx
	movl	%ebx, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	call	EC_POINT_point2oct
	movq	-64(%rbp), %r11
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L129
	movq	-56(%rbp), %rax
	movq	%r11, (%rax)
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L125:
	testb	$1, (%rax)
	jne	.L103
	movl	$76, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$66, %edx
	xorl	%r12d, %r12d
	movl	$123, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L127:
	cmpl	$406, 4(%rax)
	je	.L130
	call	ec_GF2m_simple_point2oct@PLT
	movq	%rax, %r12
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L128:
	movl	$140, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$281, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L129:
	movl	$145, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r11, %rdi
	call	CRYPTO_free@PLT
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L130:
	call	ec_GFp_simple_point2oct@PLT
	movq	%rax, %r12
	jmp	.L109
	.cfi_endproc
.LFE393:
	.size	EC_POINT_point2buf, .-EC_POINT_point2buf
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
