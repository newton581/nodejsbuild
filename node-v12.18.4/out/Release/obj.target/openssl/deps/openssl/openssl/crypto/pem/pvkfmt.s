	.file	"pvkfmt.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/pem/pvkfmt.c"
	.text
	.p2align 4
	.type	b2i_dss, @function
b2i_dss:
.LFB785:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -80(%rbp)
	movq	(%rdi), %rbx
	movl	%edx, -72(%rbp)
	call	DSA_new@PLT
	movq	%rax, %r14
	call	EVP_PKEY_new@PLT
	movq	%rax, %r13
	testq	%r14, %r14
	je	.L2
	testq	%rax, %rax
	je	.L2
	addl	$7, %r12d
	xorl	%edx, %edx
	movq	%rbx, %rdi
	shrl	$3, %r12d
	movl	%r12d, %esi
	call	BN_lebin2bn@PLT
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L2
	movl	%r12d, %r15d
	xorl	%edx, %edx
	movl	$20, %esi
	addq	%r15, %rbx
	movq	%rbx, %rdi
	call	BN_lebin2bn@PLT
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L7
	leaq	20(%rbx), %rcx
	xorl	%edx, %edx
	movl	%r12d, %esi
	movq	%rcx, %rdi
	movq	%rcx, -88(%rbp)
	call	BN_lebin2bn@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L8
	movq	-88(%rbp), %rcx
	movl	-72(%rbp), %eax
	addq	%r15, %rcx
	movq	%rcx, -88(%rbp)
	testl	%eax, %eax
	jne	.L29
	movq	%rcx, %rdi
	xorl	%edx, %edx
	movl	$20, %esi
	call	BN_lebin2bn@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L10
	movl	$4, %esi
	movq	%rax, %rdi
	call	BN_set_flags@PLT
	call	BN_new@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L27
	movq	%rax, -72(%rbp)
	call	BN_CTX_new@PLT
	movq	-72(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L3
	movq	-56(%rbp), %rcx
	movq	%r9, %rdi
	movq	%rax, %r8
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	BN_mod_exp@PLT
	movq	-72(%rbp), %r9
	testl	%eax, %eax
	je	.L3
	movq	-88(%rbp), %rax
	movq	%r15, %rdi
	addq	$20, %rax
	movq	%rax, -88(%rbp)
	call	BN_CTX_free@PLT
	movq	-72(%rbp), %r9
.L5:
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%rbx, %rcx
	movq	%r14, %rdi
	movq	%r9, -72(%rbp)
	call	DSA_set0_pqg@PLT
	movq	-72(%rbp), %r9
	testl	%eax, %eax
	je	.L27
	movq	%r9, %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r9, -56(%rbp)
	call	DSA_set0_key@PLT
	movq	-56(%rbp), %r9
	testl	%eax, %eax
	je	.L13
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	EVP_PKEY_set1_DSA@PLT
	testl	%eax, %eax
	je	.L2
	movq	%r14, %rdi
	call	DSA_free@PLT
	movq	-80(%rbp), %rax
	movq	-88(%rbp), %rcx
	movq	%rcx, (%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L2:
	movq	$0, -64(%rbp)
	xorl	%r9d, %r9d
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	movq	$0, -56(%rbp)
	xorl	%r15d, %r15d
.L3:
	movl	$307, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$127, %esi
	movl	$9, %edi
	movq	%r9, -72(%rbp)
	call	ERR_put_error@PLT
	movq	%r14, %rdi
	call	DSA_free@PLT
	movq	-56(%rbp), %rdi
	call	BN_free@PLT
	movq	-64(%rbp), %rdi
	call	BN_free@PLT
	movq	%rbx, %rdi
	call	BN_free@PLT
	movq	-72(%rbp), %r9
	movq	%r9, %rdi
	call	BN_free@PLT
	movq	%r12, %rdi
	call	BN_free@PLT
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	EVP_PKEY_free@PLT
	movq	%r15, %rdi
	call	BN_CTX_free@PLT
.L1:
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movl	%r12d, %esi
	xorl	%edx, %edx
	movq	%rcx, %rdi
	movq	%rcx, %r12
	call	BN_lebin2bn@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L9
	addq	%r12, %r15
	xorl	%r12d, %r12d
	movq	%r15, -88(%rbp)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L7:
	xorl	%r9d, %r9d
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L13:
	movq	$0, -64(%rbp)
	xorl	%ebx, %ebx
	movq	$0, -56(%rbp)
.L27:
	xorl	%r15d, %r15d
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L8:
	xorl	%r9d, %r9d
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%r9d, %r9d
	xorl	%r15d, %r15d
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L9:
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
	jmp	.L3
	.cfi_endproc
.LFE785:
	.size	b2i_dss, .-b2i_dss
	.p2align 4
	.type	b2i_rsa, @function
b2i_rsa:
.LFB786:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -72(%rbp)
	movq	(%rdi), %rbx
	movl	%edx, -64(%rbp)
	call	RSA_new@PLT
	movq	%rax, %r15
	call	EVP_PKEY_new@PLT
	movq	%rax, %r12
	testq	%r15, %r15
	je	.L45
	testq	%rax, %rax
	je	.L45
	call	BN_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L45
	movl	(%rbx), %esi
	movq	%rax, %rdi
	call	BN_set_word@PLT
	testl	%eax, %eax
	je	.L35
	leal	7(%r14), %eax
	addq	$4, %rbx
	xorl	%edx, %edx
	shrl	$3, %eax
	movq	%rbx, %rdi
	movl	%eax, %esi
	movl	%eax, -80(%rbp)
	call	BN_lebin2bn@PLT
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L36
	movl	-80(%rbp), %edi
	movl	-64(%rbp), %eax
	movq	%rdi, -96(%rbp)
	addq	%rdi, %rbx
	testl	%eax, %eax
	jne	.L37
	leal	15(%r14), %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	%esi, %r14d
	shrl	$4, %r14d
	movl	%r14d, %esi
	call	BN_lebin2bn@PLT
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L38
	movl	%r14d, %edx
	movl	%r14d, %esi
	addq	%rdx, %rbx
	movq	%rdx, -88(%rbp)
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	BN_lebin2bn@PLT
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L39
	addq	-88(%rbp), %rbx
	xorl	%edx, %edx
	movl	%r14d, %esi
	movq	%rax, -104(%rbp)
	movq	%rbx, %rdi
	call	BN_lebin2bn@PLT
	movq	-104(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %r11
	je	.L40
	movq	-88(%rbp), %rdx
	movl	%r14d, %esi
	movq	%rax, -112(%rbp)
	leaq	(%rbx,%rdx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rdi
	movq	%rcx, -120(%rbp)
	call	BN_lebin2bn@PLT
	movq	-104(%rbp), %r10
	movq	-112(%rbp), %r11
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L41
	movq	-120(%rbp), %rcx
	addq	-88(%rbp), %rcx
	xorl	%edx, %edx
	movl	%r14d, %esi
	movq	%rcx, %rdi
	movq	%rcx, -120(%rbp)
	call	BN_lebin2bn@PLT
	movq	-104(%rbp), %r10
	movq	-112(%rbp), %r11
	testq	%rax, %rax
	movq	%rax, %r9
	je	.L42
	movq	-120(%rbp), %rcx
	movq	-88(%rbp), %r8
	xorl	%edx, %edx
	movq	%rax, -128(%rbp)
	movl	-80(%rbp), %esi
	addq	%rcx, %r8
	movq	%r8, %rdi
	movq	%r8, -120(%rbp)
	call	BN_lebin2bn@PLT
	movq	-104(%rbp), %r10
	movq	-112(%rbp), %r11
	testq	%rax, %rax
	movq	-128(%rbp), %r9
	movq	%rax, %r14
	je	.L32
	movq	-64(%rbp), %rsi
	movq	%r10, %rdx
	movq	%r15, %rdi
	movq	%r9, -104(%rbp)
	movq	%r11, -88(%rbp)
	movq	%r10, -80(%rbp)
	call	RSA_set0_factors@PLT
	movq	-80(%rbp), %r10
	movq	-88(%rbp), %r11
	testl	%eax, %eax
	movq	-104(%rbp), %r9
	je	.L32
	movq	%r9, %rcx
	movq	%r11, %rsi
	movq	%rbx, %rdx
	movq	%r15, %rdi
	movq	%r9, -80(%rbp)
	movq	%r11, -64(%rbp)
	call	RSA_set0_crt_params@PLT
	movq	-64(%rbp), %r11
	movq	-80(%rbp), %r9
	testl	%eax, %eax
	je	.L56
	movq	-120(%rbp), %rbx
	addq	-96(%rbp), %rbx
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L45:
	movq	$0, -64(%rbp)
	xorl	%r9d, %r9d
	xorl	%ebx, %ebx
	xorl	%r11d, %r11d
	movq	$0, -56(%rbp)
	xorl	%r10d, %r10d
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
.L32:
	movl	$371, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$129, %esi
	movl	$9, %edi
	movq	%r9, -88(%rbp)
	movq	%r11, -80(%rbp)
	movq	%r10, -72(%rbp)
	call	ERR_put_error@PLT
	movq	%r13, %rdi
	call	BN_free@PLT
	movq	-56(%rbp), %rdi
	call	BN_free@PLT
	movq	-64(%rbp), %rdi
	call	BN_free@PLT
	movq	-72(%rbp), %r10
	movq	%r10, %rdi
	call	BN_free@PLT
	movq	-80(%rbp), %r11
	movq	%r11, %rdi
	call	BN_free@PLT
	movq	%rbx, %rdi
	call	BN_free@PLT
	movq	-88(%rbp), %r9
	movq	%r9, %rdi
	call	BN_free@PLT
	movq	%r14, %rdi
	call	BN_free@PLT
	movq	%r15, %rdi
	call	RSA_free@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	EVP_PKEY_free@PLT
.L30:
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movq	$0, -64(%rbp)
	xorl	%r9d, %r9d
	xorl	%ebx, %ebx
	xorl	%r11d, %r11d
	movq	$0, -56(%rbp)
	xorl	%r10d, %r10d
	xorl	%r14d, %r14d
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L37:
	xorl	%r14d, %r14d
.L33:
	movq	-56(%rbp), %rsi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	RSA_set0_key@PLT
	testl	%eax, %eax
	je	.L44
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	EVP_PKEY_set1_RSA@PLT
	testl	%eax, %eax
	je	.L45
	movq	%r15, %rdi
	call	RSA_free@PLT
	movq	-72(%rbp), %rax
	movq	%rbx, (%rax)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L36:
	movq	$0, -64(%rbp)
	xorl	%r9d, %r9d
	xorl	%ebx, %ebx
	xorl	%r11d, %r11d
	xorl	%r10d, %r10d
	xorl	%r14d, %r14d
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L44:
	xorl	%r9d, %r9d
	xorl	%ebx, %ebx
	xorl	%r11d, %r11d
.L56:
	movq	$0, -64(%rbp)
	xorl	%r10d, %r10d
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L38:
	xorl	%r9d, %r9d
	xorl	%ebx, %ebx
	xorl	%r11d, %r11d
	xorl	%r10d, %r10d
	xorl	%r14d, %r14d
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L39:
	xorl	%r9d, %r9d
	xorl	%ebx, %ebx
	xorl	%r11d, %r11d
	xorl	%r14d, %r14d
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L40:
	xorl	%r9d, %r9d
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L41:
	xorl	%r9d, %r9d
	xorl	%r14d, %r14d
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L42:
	xorl	%r14d, %r14d
	jmp	.L32
	.cfi_endproc
.LFE786:
	.size	b2i_rsa, .-b2i_rsa
	.p2align 4
	.type	do_b2i_bio, @function
do_b2i_bio:
.LFB784:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-80(%rbp), %rbx
	movq	%rbx, %rsi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	BIO_read@PLT
	cmpl	$16, %eax
	jne	.L103
	movzbl	-80(%rbp), %eax
	movq	%rbx, -88(%rbp)
	cmpb	$6, %al
	je	.L104
	xorl	%r14d, %r14d
	cmpb	$7, %al
	jne	.L57
	cmpl	$1, %r13d
	je	.L105
	cmpb	$2, -79(%rbp)
	jne	.L63
	movl	-72(%rbp), %eax
	movl	-68(%rbp), %r14d
	cmpl	$843141970, %eax
	je	.L106
	ja	.L68
	cmpl	$826364754, %eax
	je	.L69
	cmpl	$827544388, %eax
	jne	.L74
.L69:
	movl	$126, %r8d
.L102:
	leaq	.LC0(%rip), %rcx
	movl	$119, %edx
	movl	$134, %esi
	xorl	%r14d, %r14d
	movl	$9, %edi
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L57:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L107
	addq	$72, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	movl	$96, %r8d
	testl	%r13d, %r13d
	je	.L102
	cmpb	$2, -79(%rbp)
	jne	.L63
	movl	-72(%rbp), %eax
	movl	-68(%rbp), %r14d
	cmpl	$843141970, %eax
	je	.L64
	ja	.L71
	cmpl	$826364754, %eax
	je	.L108
	cmpl	$827544388, %eax
	jne	.L74
	leaq	-64(%rbp), %rax
	movl	$1, -100(%rbp)
	movl	$1, %r15d
	movq	%rax, -88(%rbp)
	leal	7(%r14), %eax
	shrl	$3, %eax
	leal	44(%rax,%rax,2), %ebx
	cmpl	$102400, %ebx
	jbe	.L77
.L109:
	movl	$220, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$128, %edx
	xorl	%r14d, %r14d
	movl	$133, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L103:
	movl	$211, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$123, %edx
	xorl	%r14d, %r14d
	movl	$133, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L63:
	movl	$111, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$117, %edx
	xorl	%r14d, %r14d
	movl	$134, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L108:
	leaq	-64(%rbp), %rax
	leal	7(%r14), %ecx
	movl	$0, -100(%rbp)
	movl	$1, %r15d
	movq	%rax, -88(%rbp)
	shrl	$3, %ecx
	leal	4(%rcx), %ebx
.L76:
	cmpl	$102400, %ebx
	ja	.L109
.L77:
	movl	%ebx, %edi
	movl	$223, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L110
	movl	%ebx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	%rax, -88(%rbp)
	call	BIO_read@PLT
	cmpl	%eax, %ebx
	jne	.L111
	movl	-100(%rbp), %eax
	leaq	-88(%rbp), %rdi
	movl	%r15d, %edx
	movl	%r14d, %esi
	testl	%eax, %eax
	je	.L81
	call	b2i_dss
	movq	%rax, %r14
.L79:
	movl	$240, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L106:
	leaq	-64(%rbp), %rax
	leal	7(%r14), %edx
	movl	$0, -100(%rbp)
	xorl	%r15d, %r15d
	movq	%rax, -88(%rbp)
	leal	15(%r14), %eax
	shrl	$3, %edx
	shrl	$4, %eax
	leal	(%rax,%rax,4), %eax
	leal	4(%rax,%rdx,2), %ebx
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L81:
	call	b2i_rsa
	movq	%rax, %r14
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L111:
	movl	$230, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$123, %edx
	xorl	%r14d, %r14d
	movl	$133, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L110:
	movl	$225, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r14d, %r14d
	movl	$133, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L74:
	movl	$142, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$116, %edx
	xorl	%r14d, %r14d
	movl	$134, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L68:
	cmpl	$844321604, %eax
	jne	.L74
	leaq	-64(%rbp), %rax
	movl	$1, -100(%rbp)
	xorl	%r15d, %r15d
	movq	%rax, -88(%rbp)
	leal	7(%r14), %eax
	shrl	$3, %eax
	leal	64(%rax,%rax), %ebx
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L71:
	cmpl	$844321604, %eax
	jne	.L74
.L64:
	movl	$136, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$120, %edx
	xorl	%r14d, %r14d
	movl	$134, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L105:
	movl	$102, %r8d
	movl	$120, %edx
	movl	$134, %esi
	movl	$9, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L57
.L107:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE784:
	.size	do_b2i_bio, .-do_b2i_bio
	.p2align 4
	.type	do_i2b, @function
do_i2b:
.LFB793:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rsi, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_PKEY_id@PLT
	cmpl	$116, %eax
	je	.L162
	cmpl	$6, %eax
	jne	.L161
	movq	%r13, %rdi
	call	EVP_PKEY_get0_RSA@PLT
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	leaq	-112(%rbp), %rdx
	movq	%rax, %rdi
	movq	%rax, %r12
	call	RSA_get0_key@PLT
	movq	-112(%rbp), %rdi
	call	BN_num_bits@PLT
	cmpl	$32, %eax
	jle	.L163
.L121:
	movl	$554, %r8d
	movl	$126, %edx
	movl	$131, %esi
	movl	$9, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
.L112:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L164
	addq	$120, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L163:
	.cfi_restore_state
	movq	%r12, %rdi
	call	RSA_bits@PLT
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	RSA_size@PLT
	movl	%eax, -136(%rbp)
	testl	%r15d, %r15d
	je	.L165
	movl	%ebx, %ecx
	testl	%ebx, %ebx
	je	.L161
	leal	7(%rcx), %r12d
	movl	$-92, %r11d
	movl	$41984, -136(%rbp)
	movl	$49, %r10d
	shrl	$3, %r12d
	movl	$65, %r9d
	movl	$82, %r8d
	addl	$4, %r12d
.L133:
	addl	$16, %r12d
	testq	%r14, %r14
	je	.L112
	movl	$0, -140(%rbp)
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	je	.L166
.L126:
	movl	$7, %eax
	xorl	%edx, %edx
	movb	%r8b, 8(%rbx)
	leaq	16(%rbx), %r8
	subl	%r15d, %eax
	movl	$2, 1(%rbx)
	cmpl	$8704, -136(%rbp)
	movb	%al, (%rbx)
	movb	%r11b, 5(%rbx)
	movw	%dx, 6(%rbx)
	movb	$83, 9(%rbx)
	movb	%r9b, 10(%rbx)
	movb	%r10b, 11(%rbx)
	movl	%ecx, 12(%rbx)
	je	.L167
	movq	%r13, %rdi
	movq	%r8, -152(%rbp)
	addq	$20, %rbx
	call	EVP_PKEY_get0_RSA@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	RSA_size@PLT
	movq	%r13, %rdi
	movl	%eax, -136(%rbp)
	call	RSA_bits@PLT
	leaq	-112(%rbp), %rcx
	leaq	-104(%rbp), %rdx
	movq	%r13, %rdi
	leaq	-120(%rbp), %rsi
	movl	%eax, -144(%rbp)
	call	RSA_get0_key@PLT
	movq	-152(%rbp), %r8
	movq	-104(%rbp), %rdi
	movl	$4, %edx
	movq	%r8, %rsi
	call	BN_bn2lebinpad@PLT
	movl	-136(%rbp), %edx
	movq	-120(%rbp), %rdi
	movq	%rbx, %rsi
	call	BN_bn2lebinpad@PLT
	testl	%r15d, %r15d
	jne	.L131
	movl	-144(%rbp), %r8d
	movq	%r13, %rdi
	leaq	-88(%rbp), %rdx
	leaq	-96(%rbp), %rsi
	movslq	-136(%rbp), %rax
	addl	$15, %r8d
	movl	%r8d, %r15d
	addq	%rax, %rbx
	call	RSA_get0_factors@PLT
	leaq	-80(%rbp), %rcx
	movq	%r13, %rdi
	leaq	-64(%rbp), %rdx
	leaq	-72(%rbp), %rsi
	sarl	$4, %r15d
	call	RSA_get0_crt_params@PLT
	movq	-96(%rbp), %rdi
	movslq	%r15d, %r13
	movq	%rbx, %rsi
	movl	%r15d, %edx
	addq	%r13, %rbx
	call	BN_bn2lebinpad@PLT
	movq	-88(%rbp), %rdi
	movq	%rbx, %rsi
	movl	%r15d, %edx
	addq	%r13, %rbx
	call	BN_bn2lebinpad@PLT
	movq	-72(%rbp), %rdi
	movq	%rbx, %rsi
	movl	%r15d, %edx
	addq	%r13, %rbx
	call	BN_bn2lebinpad@PLT
	movq	-64(%rbp), %rdi
	movq	%rbx, %rsi
	movl	%r15d, %edx
	addq	%r13, %rbx
	call	BN_bn2lebinpad@PLT
	movq	-80(%rbp), %rdi
	movl	%r15d, %edx
	movq	%rbx, %rsi
	call	BN_bn2lebinpad@PLT
	movl	-136(%rbp), %edx
	movq	-112(%rbp), %rdi
	leaq	(%rbx,%r13), %rsi
	call	BN_bn2lebinpad@PLT
.L131:
	movl	-140(%rbp), %eax
	testl	%eax, %eax
	jne	.L112
	movslq	%r12d, %rax
	addq	%rax, (%r14)
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L162:
	movq	%r13, %rdi
	call	EVP_PKEY_get0_DSA@PLT
	leaq	-80(%rbp), %rcx
	leaq	-88(%rbp), %rdx
	movq	$0, -96(%rbp)
	movq	%rax, %r12
	movq	%rax, %rdi
	leaq	-96(%rbp), %rsi
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	call	DSA_get0_pqg@PLT
	leaq	-64(%rbp), %rdx
	leaq	-72(%rbp), %rsi
	movq	%r12, %rdi
	call	DSA_get0_key@PLT
	movq	-96(%rbp), %rdi
	call	BN_num_bits@PLT
	movl	%eax, %ebx
	testb	$7, %al
	je	.L114
.L116:
	movl	$514, %r8d
	movl	$126, %edx
	movl	$130, %esi
	movl	$9, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L165:
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	-104(%rbp), %rcx
	movq	%r12, %rdi
	call	RSA_get0_key@PLT
	movq	-104(%rbp), %rdi
	call	BN_num_bits@PLT
	movl	-136(%rbp), %r8d
	leal	14(%rax), %edx
	addl	$7, %eax
	cmovs	%edx, %eax
	sarl	$3, %eax
	cmpl	%eax, %r8d
	jl	.L121
	leal	15(%rbx), %r8d
	movq	%r12, %rdi
	leaq	-88(%rbp), %rdx
	movl	%r8d, %eax
	leaq	-96(%rbp), %rsi
	movl	%r8d, -140(%rbp)
	sarl	$4, %eax
	movl	%eax, -136(%rbp)
	call	RSA_get0_factors@PLT
	leaq	-64(%rbp), %rdx
	movq	%r12, %rdi
	leaq	-80(%rbp), %rcx
	leaq	-72(%rbp), %rsi
	call	RSA_get0_crt_params@PLT
	movq	-80(%rbp), %rdi
	call	BN_num_bits@PLT
	movl	-136(%rbp), %r12d
	leal	14(%rax), %edx
	addl	$7, %eax
	cmovs	%edx, %eax
	sarl	$3, %eax
	cmpl	%eax, %r12d
	jl	.L121
	movq	-96(%rbp), %rdi
	call	BN_num_bits@PLT
	leal	14(%rax), %edx
	addl	$7, %eax
	cmovs	%edx, %eax
	sarl	$3, %eax
	cmpl	%eax, %r12d
	jl	.L121
	movq	-88(%rbp), %rdi
	call	BN_num_bits@PLT
	leal	14(%rax), %edx
	addl	$7, %eax
	cmovs	%edx, %eax
	sarl	$3, %eax
	cmpl	%eax, %r12d
	jl	.L121
	movq	-72(%rbp), %rdi
	call	BN_num_bits@PLT
	leal	14(%rax), %edx
	addl	$7, %eax
	cmovs	%edx, %eax
	sarl	$3, %eax
	cmpl	%eax, %r12d
	jl	.L121
	movq	-64(%rbp), %rdi
	call	BN_num_bits@PLT
	leal	14(%rax), %edx
	addl	$7, %eax
	cmovs	%edx, %eax
	sarl	$3, %eax
	cmpl	%eax, %r12d
	jl	.L121
	testl	%ebx, %ebx
	movl	-140(%rbp), %r8d
	movl	%ebx, %ecx
	je	.L161
	movl	%r8d, %eax
	leal	7(%rbx), %edx
	movl	$41984, -136(%rbp)
	movl	$-92, %r11d
	shrl	$4, %eax
	shrl	$3, %edx
	movl	$50, %r10d
	movl	$65, %r9d
	leal	(%rax,%rax,4), %eax
	movl	$82, %r8d
	leal	4(%rax,%rdx,2), %r12d
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L114:
	movq	-88(%rbp), %rdi
	call	BN_num_bits@PLT
	cmpl	$160, %eax
	jne	.L116
	movq	-80(%rbp), %rdi
	call	BN_num_bits@PLT
	cmpl	%eax, %ebx
	jl	.L116
	testl	%r15d, %r15d
	je	.L117
	movq	-72(%rbp), %rdi
	call	BN_num_bits@PLT
	cmpl	%eax, %ebx
	jl	.L116
	movl	%ebx, %ecx
	testl	%ebx, %ebx
	je	.L161
	leal	7(%rcx), %eax
	movl	$34, %r11d
	movl	$8704, -136(%rbp)
	movl	$49, %r10d
	shrl	$3, %eax
	movl	$83, %r9d
	movl	$68, %r8d
	leal	44(%rax,%rax,2), %r12d
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L167:
	movq	%r13, %rdi
	movq	%r8, -136(%rbp)
	call	EVP_PKEY_get0_DSA@PLT
	leaq	-80(%rbp), %rcx
	leaq	-88(%rbp), %rdx
	movq	$0, -96(%rbp)
	movq	%rax, %r13
	movq	%rax, %rdi
	leaq	-96(%rbp), %rsi
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	call	DSA_get0_pqg@PLT
	leaq	-64(%rbp), %rdx
	leaq	-72(%rbp), %rsi
	movq	%r13, %rdi
	call	DSA_get0_key@PLT
	movq	-96(%rbp), %rdi
	call	BN_num_bits@PLT
	movq	-136(%rbp), %r8
	movq	-96(%rbp), %rdi
	leal	14(%rax), %r13d
	addl	$7, %eax
	cmovns	%eax, %r13d
	movq	%r8, %rsi
	sarl	$3, %r13d
	movl	%r13d, %edx
	call	BN_bn2lebinpad@PLT
	movq	-136(%rbp), %r8
	movslq	%r13d, %rcx
	movq	-88(%rbp), %rdi
	movl	$20, %edx
	movq	%rcx, -136(%rbp)
	leaq	(%r8,%rcx), %rbx
	movq	%rbx, %rsi
	addq	$20, %rbx
	call	BN_bn2lebinpad@PLT
	movq	-80(%rbp), %rdi
	movq	%rbx, %rsi
	movl	%r13d, %edx
	call	BN_bn2lebinpad@PLT
	movq	-136(%rbp), %rcx
	addq	%rcx, %rbx
	testl	%r15d, %r15d
	je	.L129
	movq	-72(%rbp), %rdi
	movq	%rbx, %rsi
	movl	%r13d, %edx
	call	BN_bn2lebinpad@PLT
	movq	-136(%rbp), %rcx
	addq	%rcx, %rbx
.L130:
	pcmpeqd	%xmm0, %xmm0
	movq	$-1, 16(%rbx)
	movups	%xmm0, (%rbx)
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L166:
	movslq	%r12d, %rdi
	movl	$450, %edx
	leaq	.LC0(%rip), %rsi
	movb	%r10b, -154(%rbp)
	movb	%r9b, -153(%rbp)
	movb	%r8b, -144(%rbp)
	movl	%ecx, -152(%rbp)
	movb	%r11b, -140(%rbp)
	call	CRYPTO_malloc@PLT
	movzbl	-140(%rbp), %r11d
	movl	-152(%rbp), %ecx
	testq	%rax, %rax
	movzbl	-144(%rbp), %r8d
	movq	%rax, %rbx
	movzbl	-153(%rbp), %r9d
	movzbl	-154(%rbp), %r10d
	je	.L168
	movq	%rax, (%r14)
	movl	$1, -140(%rbp)
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L129:
	movq	-64(%rbp), %rdi
	movq	%rbx, %rsi
	movl	$20, %edx
	addq	$20, %rbx
	call	BN_bn2lebinpad@PLT
	jmp	.L130
.L168:
	movl	$451, %r8d
	movl	$65, %edx
	movl	$146, %esi
	movl	$9, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L161:
	movl	$-1, %r12d
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L117:
	movq	-64(%rbp), %rdi
	call	BN_num_bits@PLT
	cmpl	$160, %eax
	jg	.L116
	movl	%ebx, %ecx
	testl	%ebx, %ebx
	je	.L161
	leal	7(%rcx), %eax
	movl	$34, %r11d
	movl	$8704, -136(%rbp)
	movl	$50, %r10d
	shrl	$3, %eax
	movl	$83, %r9d
	movl	$68, %r8d
	leal	64(%rax,%rax), %r12d
	jmp	.L133
.L164:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE793:
	.size	do_i2b, .-do_i2b
	.p2align 4
	.type	i2b_PVK.constprop.0, @function
i2b_PVK.constprop.0:
.LFB810:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$1144, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -1144(%rbp)
	movq	%rcx, -1152(%rbp)
	movq	%r8, -1160(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$1, %edx
	movq	$0, -1128(%rbp)
	sbbl	%r12d, %r12d
	xorl	%edx, %edx
	xorl	%edi, %edi
	andl	$-16, %r12d
	addl	$40, %r12d
	call	do_i2b
	testl	%eax, %eax
	js	.L216
	movl	%eax, %ebx
	addl	%eax, %r12d
	movq	-1144(%rbp), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L217
	movq	%rax, -1128(%rbp)
	movq	$0, -1168(%rbp)
.L172:
	call	EVP_CIPHER_CTX_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L192
	movq	-1128(%rbp), %rax
	movl	$2964713758, %ecx
	movq	%r14, %rdi
	movq	%rcx, (%rax)
	addq	$8, %rax
	movq	%rax, -1128(%rbp)
	call	EVP_PKEY_id@PLT
	movq	-1128(%rbp), %rdx
	cmpl	$116, %eax
	leaq	4(%rdx), %rax
	je	.L218
	movl	$1, (%rdx)
.L176:
	testl	%r13d, %r13d
	leaq	12(%rax), %rdi
	movb	$0, 3(%rax)
	setne	%dl
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movb	$0, 7(%rax)
	movb	%dl, (%rax)
	sall	$4, %edx
	movw	%cx, 1(%rax)
	movb	%dl, 4(%rax)
	movw	%si, 5(%rax)
	movl	%ebx, 8(%rax)
	movq	%rdi, -1128(%rbp)
	testl	%r13d, %r13d
	jne	.L219
	leaq	-1128(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	do_i2b
.L191:
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_free@PLT
	movq	-1144(%rbp), %rax
	cmpq	$0, (%rax)
	jne	.L169
	movq	-1168(%rbp), %rbx
	movq	%rbx, (%rax)
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L221:
	movl	$835, %r8d
	movl	$104, %edx
	movl	$137, %esi
	movl	$9, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L192:
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_free@PLT
	movq	-1144(%rbp), %rax
	cmpq	$0, (%rax)
	jne	.L216
	movq	-1168(%rbp), %rdi
	movl	$863, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L216:
	movl	$-1, %r12d
.L169:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L220
	addq	$1144, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L219:
	.cfi_restore_state
	movl	$16, %esi
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jle	.L192
	movq	-1128(%rbp), %rax
	movq	%r14, %rsi
	leaq	-1128(%rbp), %rdi
	xorl	%edx, %edx
	leaq	-1088(%rbp), %r14
	movq	%rax, -1176(%rbp)
	addq	$16, %rax
	movq	%rax, -1128(%rbp)
	call	do_i2b
	movq	-1152(%rbp), %rax
	movl	$1, %edx
	movq	%r14, %rdi
	movq	-1160(%rbp), %rcx
	movl	$1024, %esi
	testq	%rax, %rax
	je	.L179
	call	*%rax
	movl	%eax, -1152(%rbp)
.L180:
	movl	-1152(%rbp), %edx
	testl	%edx, %edx
	jle	.L221
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L184
	movq	%rax, -1160(%rbp)
	call	EVP_sha1@PLT
	movq	-1160(%rbp), %rdi
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	EVP_DigestInit_ex@PLT
	movq	-1160(%rbp), %rdi
	testl	%eax, %eax
	jne	.L222
.L184:
	call	EVP_MD_CTX_free@PLT
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L218:
	movl	$2, (%rdx)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L217:
	movslq	%r12d, %rdi
	movl	$799, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, -1168(%rbp)
	movq	%rax, -1128(%rbp)
	testq	%rax, %rax
	jne	.L172
	movl	$801, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$137, %esi
	movl	$9, %edi
	orl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L179:
	call	PEM_def_callback@PLT
	movl	%eax, -1152(%rbp)
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L222:
	movq	-1176(%rbp), %rsi
	movl	$16, %edx
	call	EVP_DigestUpdate@PLT
	movq	-1160(%rbp), %rdi
	testl	%eax, %eax
	je	.L184
	movslq	-1152(%rbp), %rdx
	movq	%r14, %rsi
	movq	%rdi, -1152(%rbp)
	call	EVP_DigestUpdate@PLT
	movq	-1152(%rbp), %rdi
	testl	%eax, %eax
	je	.L184
	leaq	-1120(%rbp), %r14
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	EVP_DigestFinal_ex@PLT
	movq	-1152(%rbp), %rdi
	testl	%eax, %eax
	je	.L184
	call	EVP_MD_CTX_free@PLT
	cmpl	$1, %r13d
	je	.L223
.L189:
	movq	-1176(%rbp), %rax
	addq	$24, %rax
	movq	%rax, -1128(%rbp)
	call	EVP_rc4@PLT
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r14, %rcx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	EVP_EncryptInit_ex@PLT
	testl	%eax, %eax
	je	.L192
	movl	$20, %esi
	movq	%r14, %rdi
	leaq	-1132(%rbp), %r13
	call	OPENSSL_cleanse@PLT
	leal	-8(%rbx), %r8d
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	-1128(%rbp), %rsi
	movq	%rsi, %rcx
	call	EVP_EncryptUpdate@PLT
	testl	%eax, %eax
	je	.L192
	movslq	-1132(%rbp), %rsi
	movq	%r13, %rdx
	addq	-1128(%rbp), %rsi
	movq	%r15, %rdi
	call	EVP_EncryptFinal_ex@PLT
	testl	%eax, %eax
	jne	.L191
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L223:
	xorl	%eax, %eax
	movb	$0, -1105(%rbp)
	movq	$0, -1115(%rbp)
	movw	%ax, -1107(%rbp)
	jmp	.L189
.L220:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE810:
	.size	i2b_PVK.constprop.0, .-i2b_PVK.constprop.0
	.p2align 4
	.type	do_PVK_body.isra.0, @function
do_PVK_body.isra.0:
.LFB808:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1128, %rsp
	movq	%r8, -1144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_CIPHER_CTX_new@PLT
	movq	%rax, %r13
	testl	%r15d, %r15d
	je	.L255
	leaq	-1088(%rbp), %rax
	testq	%r14, %r14
	movl	$0, %edx
	movq	-1144(%rbp), %rcx
	movq	%rax, -1152(%rbp)
	movl	$1024, %esi
	movq	%rax, %rdi
	je	.L226
	call	*%r14
	movl	%eax, -1144(%rbp)
.L227:
	movl	-1144(%rbp), %edx
	testl	%edx, %edx
	js	.L301
	leal	8(%r12), %edi
	movl	$696, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L302
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L233
	movq	%rax, -1160(%rbp)
	call	EVP_sha1@PLT
	movq	-1160(%rbp), %rdi
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	EVP_DigestInit_ex@PLT
	movq	-1160(%rbp), %rdi
	testl	%eax, %eax
	jne	.L303
.L233:
	call	EVP_MD_CTX_free@PLT
	leaq	-1120(%rbp), %r15
.L232:
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	call	EVP_CIPHER_CTX_free@PLT
.L241:
	movl	$20, %esi
	movq	%r15, %rdi
	call	OPENSSL_cleanse@PLT
	movl	$744, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
.L224:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L304
	addq	$1128, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	.cfi_restore_state
	xorl	%r14d, %r14d
.L225:
	movq	%rbx, -1128(%rbp)
	cmpl	$15, %r12d
	jbe	.L242
	movzbl	(%rbx), %eax
	movl	$96, %r8d
	cmpb	$6, %al
	je	.L299
	cmpb	$7, %al
	je	.L305
.L242:
	movl	$189, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$122, %edx
	xorl	%r12d, %r12d
	movl	$132, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L252:
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_free@PLT
	testq	%r14, %r14
	je	.L224
	leaq	-1120(%rbp), %r15
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L305:
	cmpb	$2, 1(%rbx)
	jne	.L306
	movl	8(%rbx), %eax
	movl	12(%rbx), %esi
	addq	$16, %rbx
	cmpl	$843141970, %eax
	je	.L245
	ja	.L246
	cmpl	$826364754, %eax
	je	.L247
	cmpl	$827544388, %eax
	jne	.L248
.L247:
	movl	$126, %r8d
.L299:
	movl	$119, %edx
	movl	$134, %esi
	movl	$9, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L303:
	movl	%r15d, %eax
	movq	%rbx, %rsi
	movq	%rdi, -1168(%rbp)
	movq	%rax, %rdx
	movq	%rax, -1160(%rbp)
	call	EVP_DigestUpdate@PLT
	movq	-1168(%rbp), %rdi
	testl	%eax, %eax
	je	.L233
	movslq	-1144(%rbp), %rdx
	movq	-1152(%rbp), %rsi
	movq	%rdi, -1144(%rbp)
	call	EVP_DigestUpdate@PLT
	movq	-1144(%rbp), %rdi
	testl	%eax, %eax
	je	.L233
	leaq	-1120(%rbp), %r15
	xorl	%edx, %edx
	movq	%r15, %rsi
	call	EVP_DigestFinal_ex@PLT
	movq	-1144(%rbp), %rdi
	testl	%eax, %eax
	jne	.L236
	call	EVP_MD_CTX_free@PLT
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L245:
	leal	15(%rsi), %eax
	leal	7(%rsi), %ecx
	subl	$16, %r12d
	movq	%rbx, -1128(%rbp)
	shrl	$4, %eax
	shrl	$3, %ecx
	leal	(%rax,%rax,4), %eax
	leal	4(%rax,%rcx,2), %eax
	cmpl	%eax, %r12d
	jb	.L250
	leaq	-1128(%rbp), %rdi
	xorl	%edx, %edx
	call	b2i_rsa
	movq	%rax, %r12
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L226:
	call	PEM_def_callback@PLT
	movl	%eax, -1144(%rbp)
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L246:
	cmpl	$844321604, %eax
	jne	.L248
	leal	7(%rsi), %eax
	subl	$16, %r12d
	movq	%rbx, -1128(%rbp)
	shrl	$3, %eax
	leal	64(%rax,%rax), %eax
	cmpl	%r12d, %eax
	ja	.L250
	leaq	-1128(%rbp), %rdi
	xorl	%edx, %edx
	call	b2i_dss
	movq	%rax, %r12
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L301:
	movl	$693, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$104, %edx
.L300:
	movl	$135, %esi
	movl	$9, %edi
	xorl	%r12d, %r12d
	call	ERR_put_error@PLT
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_free@PLT
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L306:
	movl	$111, %r8d
	movl	$117, %edx
	movl	$134, %esi
	movl	$9, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L242
.L236:
	call	EVP_MD_CTX_free@PLT
	addq	-1160(%rbp), %rbx
	movq	(%rbx), %rax
	movq	%rax, (%r14)
	cmpl	$7, %r12d
	jbe	.L307
	call	EVP_rc4@PLT
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r15, %rcx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	EVP_DecryptInit_ex@PLT
	testl	%eax, %eax
	je	.L232
	leaq	8(%rbx), %rax
	leal	-8(%r12), %ebx
	movq	%r13, %rdi
	movl	%ebx, %edx
	leaq	8(%r14), %rbx
	leaq	-1128(%rbp), %r9
	movq	%rax, %rcx
	movl	%edx, -1160(%rbp)
	movl	%edx, %r8d
	movq	%rbx, %rsi
	movq	%r9, %rdx
	movq	%rax, -1152(%rbp)
	movq	%r9, -1144(%rbp)
	call	EVP_DecryptUpdate@PLT
	testl	%eax, %eax
	je	.L232
	movslq	-1128(%rbp), %rsi
	movq	-1144(%rbp), %r9
	movq	%r13, %rdi
	addq	%rbx, %rsi
	movq	%r9, %rdx
	call	EVP_DecryptFinal_ex@PLT
	testl	%eax, %eax
	je	.L232
	movl	8(%r14), %eax
	cmpl	$843141970, %eax
	je	.L240
	cmpl	$844321604, %eax
	je	.L240
	xorl	%eax, %eax
	movb	$0, -1105(%rbp)
	movq	$0, -1115(%rbp)
	movw	%ax, -1107(%rbp)
	call	EVP_rc4@PLT
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r15, %rcx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	EVP_DecryptInit_ex@PLT
	testl	%eax, %eax
	je	.L232
	movq	-1144(%rbp), %r9
	movl	-1160(%rbp), %r8d
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	-1152(%rbp), %rcx
	movq	%r9, %rdx
	call	EVP_DecryptUpdate@PLT
	testl	%eax, %eax
	je	.L232
	movslq	-1128(%rbp), %rsi
	movq	-1144(%rbp), %r9
	movq	%r13, %rdi
	addq	%rbx, %rsi
	movq	%r9, %rdx
	call	EVP_DecryptFinal_ex@PLT
	testl	%eax, %eax
	je	.L232
	movl	8(%r14), %eax
	cmpl	$843141970, %eax
	je	.L240
	cmpl	$844321604, %eax
	jne	.L308
	.p2align 4,,10
	.p2align 3
.L240:
	movq	%r14, %rbx
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L307:
	movl	$709, %r8d
	movl	$125, %edx
	movl	$135, %esi
	movl	$9, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L302:
	movl	$698, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	jmp	.L300
.L250:
	movl	$194, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$123, %edx
	xorl	%r12d, %r12d
	movl	$132, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L248:
	movl	$142, %r8d
	movl	$116, %edx
	movl	$134, %esi
	movl	$9, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L242
.L308:
	movl	$732, %r8d
	movl	$101, %edx
	movl	$135, %esi
	movl	$9, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L232
.L304:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE808:
	.size	do_PVK_body.isra.0, .-do_PVK_body.isra.0
	.p2align 4
	.globl	b2i_PrivateKey
	.type	b2i_PrivateKey, @function
b2i_PrivateKey:
.LFB787:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, -16(%rbp)
	cmpl	$15, %esi
	jbe	.L310
	movzbl	(%rax), %edx
	movl	$96, %r8d
	cmpb	$6, %dl
	je	.L332
	cmpb	$7, %dl
	je	.L333
.L310:
	movl	$189, %r8d
	movl	$122, %edx
	movl	$132, %esi
	movl	$9, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L309:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L334
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L333:
	.cfi_restore_state
	cmpb	$2, 1(%rax)
	jne	.L335
	movl	8(%rax), %edx
	movl	12(%rax), %r8d
	addq	$16, %rax
	cmpl	$843141970, %edx
	je	.L313
	ja	.L314
	cmpl	$826364754, %edx
	je	.L315
	cmpl	$827544388, %edx
	jne	.L316
.L315:
	movl	$126, %r8d
.L332:
	movl	$119, %edx
	movl	$134, %esi
	movl	$9, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L314:
	cmpl	$844321604, %edx
	jne	.L316
	movq	%rax, -16(%rbp)
	leal	7(%r8), %eax
	subl	$16, %esi
	shrl	$3, %eax
	leal	64(%rax,%rax), %eax
	cmpl	%esi, %eax
	ja	.L318
	leaq	-16(%rbp), %rdi
	xorl	%edx, %edx
	movl	%r8d, %esi
	call	b2i_dss
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L313:
	movq	%rax, -16(%rbp)
	leal	15(%r8), %eax
	leal	7(%r8), %edx
	subl	$16, %esi
	shrl	$4, %eax
	shrl	$3, %edx
	leal	(%rax,%rax,4), %eax
	leal	4(%rax,%rdx,2), %eax
	cmpl	%eax, %esi
	jb	.L318
	leaq	-16(%rbp), %rdi
	xorl	%edx, %edx
	movl	%r8d, %esi
	call	b2i_rsa
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L335:
	movl	$111, %r8d
	movl	$117, %edx
	movl	$134, %esi
	movl	$9, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L310
.L318:
	movl	$194, %r8d
	movl	$123, %edx
	movl	$132, %esi
	movl	$9, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L316:
	movl	$142, %r8d
	movl	$116, %edx
	movl	$134, %esi
	movl	$9, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L310
.L334:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE787:
	.size	b2i_PrivateKey, .-b2i_PrivateKey
	.p2align 4
	.globl	b2i_PublicKey
	.type	b2i_PublicKey, @function
b2i_PublicKey:
.LFB788:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, -16(%rbp)
	cmpl	$15, %esi
	jbe	.L337
	movzbl	(%rax), %edx
	cmpb	$6, %dl
	je	.L338
	movl	$102, %r8d
	cmpb	$7, %dl
	je	.L359
.L337:
	movl	$189, %r8d
	movl	$122, %edx
	movl	$132, %esi
	movl	$9, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L338:
	cmpb	$2, 1(%rax)
	jne	.L360
	movl	8(%rax), %edx
	movl	12(%rax), %r8d
	addq	$16, %rax
	cmpl	$843141970, %edx
	je	.L340
	ja	.L341
	cmpl	$826364754, %edx
	je	.L342
	cmpl	$827544388, %edx
	jne	.L344
	movq	%rax, -16(%rbp)
	leal	7(%r8), %eax
	subl	$16, %esi
	shrl	$3, %eax
	leal	44(%rax,%rax,2), %eax
	cmpl	%esi, %eax
	ja	.L345
	leaq	-16(%rbp), %rdi
	movl	$1, %edx
	movl	%r8d, %esi
	call	b2i_dss
.L336:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L361
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L342:
	.cfi_restore_state
	movq	%rax, -16(%rbp)
	leal	7(%r8), %eax
	subl	$16, %esi
	shrl	$3, %eax
	addl	$4, %eax
	cmpl	%eax, %esi
	jb	.L345
	leaq	-16(%rbp), %rdi
	movl	$1, %edx
	movl	%r8d, %esi
	call	b2i_rsa
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L341:
	cmpl	$844321604, %edx
	jne	.L344
.L340:
	movl	$136, %r8d
.L359:
	movl	$120, %edx
	movl	$134, %esi
	movl	$9, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L360:
	movl	$111, %r8d
	movl	$117, %edx
	movl	$134, %esi
	movl	$9, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L337
.L345:
	movl	$194, %r8d
	movl	$123, %edx
	movl	$132, %esi
	movl	$9, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L344:
	movl	$142, %r8d
	movl	$116, %edx
	movl	$134, %esi
	movl	$9, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L337
.L361:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE788:
	.size	b2i_PublicKey, .-b2i_PublicKey
	.p2align 4
	.globl	b2i_PrivateKey_bio
	.type	b2i_PrivateKey_bio, @function
b2i_PrivateKey_bio:
.LFB789:
	.cfi_startproc
	endbr64
	xorl	%esi, %esi
	jmp	do_b2i_bio
	.cfi_endproc
.LFE789:
	.size	b2i_PrivateKey_bio, .-b2i_PrivateKey_bio
	.p2align 4
	.globl	b2i_PublicKey_bio
	.type	b2i_PublicKey_bio, @function
b2i_PublicKey_bio:
.LFB790:
	.cfi_startproc
	endbr64
	movl	$1, %esi
	jmp	do_b2i_bio
	.cfi_endproc
.LFE790:
	.size	b2i_PublicKey_bio, .-b2i_PublicKey_bio
	.p2align 4
	.globl	i2b_PrivateKey_bio
	.type	i2b_PrivateKey_bio, @function
i2b_PrivateKey_bio:
.LFB799:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	leaq	-32(%rbp), %rdi
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$0, -32(%rbp)
	call	do_i2b
	testl	%eax, %eax
	js	.L365
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movl	%eax, %edx
	movl	%eax, %ebx
	call	BIO_write@PLT
	movq	-32(%rbp), %rdi
	movl	$484, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, %r12d
	call	CRYPTO_free@PLT
	cmpl	%r12d, %ebx
	jne	.L365
.L364:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L369
	addq	$16, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L365:
	.cfi_restore_state
	movl	$-1, %r12d
	jmp	.L364
.L369:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE799:
	.size	i2b_PrivateKey_bio, .-i2b_PrivateKey_bio
	.p2align 4
	.globl	i2b_PublicKey_bio
	.type	i2b_PublicKey_bio, @function
i2b_PublicKey_bio:
.LFB800:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	leaq	-32(%rbp), %rdi
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$0, -32(%rbp)
	call	do_i2b
	testl	%eax, %eax
	js	.L371
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movl	%eax, %edx
	movl	%eax, %ebx
	call	BIO_write@PLT
	movq	-32(%rbp), %rdi
	movl	$484, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, %r12d
	call	CRYPTO_free@PLT
	cmpl	%r12d, %ebx
	jne	.L371
.L370:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L375
	addq	$16, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L371:
	.cfi_restore_state
	movl	$-1, %r12d
	jmp	.L370
.L375:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE800:
	.size	i2b_PublicKey_bio, .-i2b_PublicKey_bio
	.p2align 4
	.globl	b2i_PVK_bio
	.type	b2i_PVK_bio, @function
b2i_PVK_bio:
.LFB804:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -88(%rbp)
	leaq	-80(%rbp), %rsi
	movq	%rdx, -96(%rbp)
	movl	$24, %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	BIO_read@PLT
	cmpl	$24, %eax
	jne	.L393
	cmpl	$-1330253538, -80(%rbp)
	jne	.L394
	movl	-60(%rbp), %eax
	movl	-64(%rbp), %r13d
	movl	%eax, -100(%rbp)
	cmpl	$10240, %r13d
	ja	.L385
	cmpl	$102400, %eax
	ja	.L385
	movl	-68(%rbp), %eax
	testl	%eax, %eax
	je	.L380
	testl	%r13d, %r13d
	je	.L395
.L380:
	movl	-100(%rbp), %eax
	movl	$765, %edx
	leaq	.LC0(%rip), %rsi
	leal	0(%r13,%rax), %r15d
	movq	%r15, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L396
	movl	%r15d, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	BIO_read@PLT
	cmpl	%r15d, %eax
	je	.L382
	movl	$772, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$124, %edx
	xorl	%r12d, %r12d
	movl	$128, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
.L383:
	movl	$778, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	CRYPTO_clear_free@PLT
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L394:
	movl	$632, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$116, %edx
	xorl	%r12d, %r12d
	movl	$136, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L376:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L397
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L393:
	.cfi_restore_state
	movl	$757, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$124, %edx
	xorl	%r12d, %r12d
	movl	$128, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L385:
	xorl	%r12d, %r12d
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L382:
	movq	-96(%rbp), %r8
	movq	-88(%rbp), %rcx
	movl	%r13d, %esi
	movq	%r14, %rdi
	movl	-100(%rbp), %edx
	call	do_PVK_body.isra.0
	movq	%rax, %r12
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L395:
	movl	$649, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$121, %edx
	xorl	%r12d, %r12d
	movl	$136, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L396:
	movl	$767, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$128, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
	jmp	.L376
.L397:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE804:
	.size	b2i_PVK_bio, .-b2i_PVK_bio
	.p2align 4
	.globl	i2b_PVK_bio
	.type	i2b_PVK_bio, @function
i2b_PVK_bio:
.LFB806:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	leaq	-32(%rbp), %rdi
	subq	$16, %rsp
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$0, -32(%rbp)
	call	i2b_PVK.constprop.0
	testl	%eax, %eax
	js	.L401
	movq	-32(%rbp), %rsi
	movq	%r13, %rdi
	movl	%eax, %edx
	movl	%eax, %r12d
	call	BIO_write@PLT
	movq	-32(%rbp), %rdi
	movl	$876, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, %r13d
	call	CRYPTO_free@PLT
	cmpl	%r13d, %r12d
	jne	.L401
	movl	$878, %r8d
	movl	$118, %edx
	movl	$138, %esi
	movl	$9, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L398:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L404
	addq	$16, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L401:
	.cfi_restore_state
	movl	$-1, %r12d
	jmp	.L398
.L404:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE806:
	.size	i2b_PVK_bio, .-i2b_PVK_bio
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
