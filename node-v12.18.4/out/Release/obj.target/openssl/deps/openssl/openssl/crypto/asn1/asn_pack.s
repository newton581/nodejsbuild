	.file	"asn_pack.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/asn1/asn_pack.c"
	.text
	.p2align 4
	.globl	ASN1_item_pack
	.type	ASN1_item_pack, @function
ASN1_item_pack:
.LFB419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	testq	%rdx, %rdx
	je	.L2
	movq	(%rdx), %r12
	testq	%r12, %r12
	je	.L2
.L3:
	movq	8(%r12), %rdi
	movl	$29, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	leaq	8(%r12), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	$0, 8(%r12)
	call	ASN1_item_i2d@PLT
	movl	%eax, (%r12)
	testl	%eax, %eax
	je	.L21
	cmpq	$0, 8(%r12)
	je	.L22
	testq	%rbx, %rbx
	je	.L1
	cmpq	$0, (%rbx)
	je	.L23
.L1:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	call	ASN1_STRING_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L3
	movl	$22, %r8d
	movl	$65, %edx
	movl	$198, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L23:
	movq	%r12, (%rbx)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	movl	$33, %r8d
	movl	$112, %edx
	movl	$198, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L6:
	testq	%rbx, %rbx
	je	.L8
	cmpq	$0, (%rbx)
	je	.L8
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	ASN1_STRING_free@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L22:
	movl	$37, %r8d
	movl	$65, %edx
	movl	$198, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L6
	.cfi_endproc
.LFE419:
	.size	ASN1_item_pack, .-ASN1_item_pack
	.p2align 4
	.globl	ASN1_item_unpack
	.type	ASN1_item_unpack, @function
ASN1_item_unpack:
.LFB420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movslq	(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	leaq	-16(%rbp), %rsi
	xorl	%edi, %edi
	movq	%rax, -16(%rbp)
	call	ASN1_item_d2i@PLT
	testq	%rax, %rax
	je	.L28
.L24:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L29
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movl	$60, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$110, %edx
	movl	$199, %esi
	movl	$13, %edi
	movq	%rax, -24(%rbp)
	call	ERR_put_error@PLT
	movq	-24(%rbp), %rax
	jmp	.L24
.L29:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE420:
	.size	ASN1_item_unpack, .-ASN1_item_unpack
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
