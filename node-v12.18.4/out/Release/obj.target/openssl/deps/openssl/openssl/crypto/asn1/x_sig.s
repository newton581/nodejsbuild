	.file	"x_sig.c"
	.text
	.p2align 4
	.globl	d2i_X509_SIG
	.type	d2i_X509_SIG, @function
d2i_X509_SIG:
.LFB805:
	.cfi_startproc
	endbr64
	leaq	X509_SIG_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE805:
	.size	d2i_X509_SIG, .-d2i_X509_SIG
	.p2align 4
	.globl	i2d_X509_SIG
	.type	i2d_X509_SIG, @function
i2d_X509_SIG:
.LFB806:
	.cfi_startproc
	endbr64
	leaq	X509_SIG_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE806:
	.size	i2d_X509_SIG, .-i2d_X509_SIG
	.p2align 4
	.globl	X509_SIG_new
	.type	X509_SIG_new, @function
X509_SIG_new:
.LFB807:
	.cfi_startproc
	endbr64
	leaq	X509_SIG_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE807:
	.size	X509_SIG_new, .-X509_SIG_new
	.p2align 4
	.globl	X509_SIG_free
	.type	X509_SIG_free, @function
X509_SIG_free:
.LFB808:
	.cfi_startproc
	endbr64
	leaq	X509_SIG_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE808:
	.size	X509_SIG_free, .-X509_SIG_free
	.p2align 4
	.globl	X509_SIG_get0
	.type	X509_SIG_get0, @function
X509_SIG_get0:
.LFB809:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L7
	movq	(%rdi), %rax
	movq	%rax, (%rsi)
.L7:
	testq	%rdx, %rdx
	je	.L6
	movq	8(%rdi), %rax
	movq	%rax, (%rdx)
.L6:
	ret
	.cfi_endproc
.LFE809:
	.size	X509_SIG_get0, .-X509_SIG_get0
	.p2align 4
	.globl	X509_SIG_getm
	.type	X509_SIG_getm, @function
X509_SIG_getm:
.LFB812:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L16
	movq	(%rdi), %rax
	movq	%rax, (%rsi)
.L16:
	testq	%rdx, %rdx
	je	.L15
	movq	8(%rdi), %rax
	movq	%rax, (%rdx)
.L15:
	ret
	.cfi_endproc
.LFE812:
	.size	X509_SIG_getm, .-X509_SIG_getm
	.globl	X509_SIG_it
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"X509_SIG"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	X509_SIG_it, @object
	.size	X509_SIG_it, 56
X509_SIG_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	X509_SIG_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC0
	.section	.rodata.str1.1
.LC1:
	.string	"algor"
.LC2:
	.string	"digest"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	X509_SIG_seq_tt, @object
	.size	X509_SIG_seq_tt, 80
X509_SIG_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC1
	.quad	X509_ALGOR_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC2
	.quad	ASN1_OCTET_STRING_it
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
