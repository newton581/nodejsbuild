	.file	"hmac.c"
	.text
	.p2align 4
	.globl	HMAC_Init_ex
	.type	HMAC_Init_ex, @function
HMAC_Init_ex:
.LFB419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$344, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	testq	%rcx, %rcx
	je	.L2
	movq	%rcx, %r12
	cmpq	%rcx, %rax
	je	.L3
	testq	%rsi, %rsi
	je	.L7
	testl	%edx, %edx
	js	.L7
.L3:
	movq	%r12, (%rbx)
.L6:
	movq	%r12, %rdi
	call	EVP_MD_meth_get_flags@PLT
	testb	$2, %al
	jne	.L7
	testq	%r14, %r14
	je	.L8
	movq	%r12, %rdi
	call	EVP_MD_block_size@PLT
	cmpl	$144, %eax
	jg	.L7
	cmpl	%eax, %r13d
	jg	.L44
	cmpl	$144, %r13d
	ja	.L7
	movl	%r13d, -372(%rbp)
	movslq	%r13d, %rdx
	leaq	-208(%rbp), %r13
	movq	%r14, %rsi
	movl	$144, %ecx
	movq	%r13, %rdi
	call	__memcpy_chk@PLT
	movl	-372(%rbp), %r8d
	movl	%r8d, -356(%rbp)
.L10:
	cmpl	$144, %r8d
	jne	.L45
.L11:
	movdqa	.LC0(%rip), %xmm0
	movq	16(%rbx), %rdi
	movq	%r15, %rdx
	movq	%r12, %rsi
	movdqa	-208(%rbp), %xmm1
	pxor	%xmm0, %xmm1
	movaps	%xmm1, -352(%rbp)
	movdqa	-192(%rbp), %xmm1
	pxor	%xmm0, %xmm1
	movaps	%xmm1, -336(%rbp)
	movdqa	-176(%rbp), %xmm1
	pxor	%xmm0, %xmm1
	movaps	%xmm1, -320(%rbp)
	movdqa	-160(%rbp), %xmm1
	pxor	%xmm0, %xmm1
	movaps	%xmm1, -304(%rbp)
	movdqa	-144(%rbp), %xmm1
	pxor	%xmm0, %xmm1
	movaps	%xmm1, -288(%rbp)
	movdqa	-128(%rbp), %xmm1
	pxor	%xmm0, %xmm1
	movaps	%xmm1, -272(%rbp)
	movdqa	-112(%rbp), %xmm1
	pxor	%xmm0, %xmm1
	movaps	%xmm1, -256(%rbp)
	movdqa	-96(%rbp), %xmm1
	pxor	%xmm0, %xmm1
	pxor	-80(%rbp), %xmm0
	movaps	%xmm1, -240(%rbp)
	movaps	%xmm0, -224(%rbp)
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	jne	.L12
	leaq	-352(%rbp), %r14
.L14:
	xorl	%eax, %eax
.L13:
	movl	$144, %esi
	movq	%r13, %rdi
	movl	%eax, -372(%rbp)
	call	OPENSSL_cleanse@PLT
	movl	$144, %esi
	movq	%r14, %rdi
	call	OPENSSL_cleanse@PLT
	movl	-372(%rbp), %eax
	jmp	.L1
.L44:
	movq	8(%rbx), %rdi
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	jne	.L46
	.p2align 4,,10
	.p2align 3
.L7:
	xorl	%eax, %eax
.L1:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L47
	addq	$344, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L7
	movq	%rax, %r12
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L8:
	movq	16(%rbx), %rsi
	movq	8(%rbx), %rdi
	call	EVP_MD_CTX_copy_ex@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L1
.L45:
	movl	%r8d, %edi
	movl	$144, %edx
	xorl	%esi, %esi
	subl	%r8d, %edx
	addq	%r13, %rdi
	call	memset@PLT
	jmp	.L11
.L12:
	movq	%r12, %rdi
	leaq	-352(%rbp), %r14
	call	EVP_MD_block_size@PLT
	movq	16(%rbx), %rdi
	movq	%r14, %rsi
	movslq	%eax, %rdx
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L14
	movdqa	.LC1(%rip), %xmm0
	movq	24(%rbx), %rdi
	movq	%r15, %rdx
	movq	%r12, %rsi
	movdqa	-208(%rbp), %xmm1
	pxor	%xmm0, %xmm1
	movaps	%xmm1, -352(%rbp)
	movdqa	-192(%rbp), %xmm1
	pxor	%xmm0, %xmm1
	movaps	%xmm1, -336(%rbp)
	movdqa	-176(%rbp), %xmm1
	pxor	%xmm0, %xmm1
	movaps	%xmm1, -320(%rbp)
	movdqa	-160(%rbp), %xmm1
	pxor	%xmm0, %xmm1
	movaps	%xmm1, -304(%rbp)
	movdqa	-144(%rbp), %xmm1
	pxor	%xmm0, %xmm1
	movaps	%xmm1, -288(%rbp)
	movdqa	-128(%rbp), %xmm1
	pxor	%xmm0, %xmm1
	movaps	%xmm1, -272(%rbp)
	movdqa	-112(%rbp), %xmm1
	pxor	%xmm0, %xmm1
	movaps	%xmm1, -256(%rbp)
	movdqa	-96(%rbp), %xmm1
	pxor	%xmm0, %xmm1
	pxor	-80(%rbp), %xmm0
	movaps	%xmm1, -240(%rbp)
	movaps	%xmm0, -224(%rbp)
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	je	.L14
	movq	%r12, %rdi
	call	EVP_MD_block_size@PLT
	movq	24(%rbx), %rdi
	movq	%r14, %rsi
	movslq	%eax, %rdx
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L14
	movq	16(%rbx), %rsi
	movq	8(%rbx), %rdi
	call	EVP_MD_CTX_copy_ex@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L13
.L46:
	movq	8(%rbx), %rdi
	movslq	%r13d, %rdx
	movq	%r14, %rsi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L7
	leaq	-208(%rbp), %r13
	movq	8(%rbx), %rdi
	leaq	-356(%rbp), %rdx
	movq	%r13, %rsi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	je	.L7
	movl	-356(%rbp), %r8d
	jmp	.L10
.L47:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE419:
	.size	HMAC_Init_ex, .-HMAC_Init_ex
	.p2align 4
	.globl	HMAC_Update
	.type	HMAC_Update, @function
HMAC_Update:
.LFB421:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L49
	movq	8(%rdi), %rdi
	jmp	EVP_DigestUpdate@PLT
	.p2align 4,,10
	.p2align 3
.L49:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE421:
	.size	HMAC_Update, .-HMAC_Update
	.p2align 4
	.globl	HMAC_Final
	.type	HMAC_Final, @function
HMAC_Final:
.LFB422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$96, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, (%rdi)
	je	.L53
	movq	%rdi, %rbx
	leaq	-112(%rbp), %r14
	movq	8(%rdi), %rdi
	movq	%rsi, %r12
	movq	%rdx, %r13
	movq	%r14, %rsi
	leaq	-116(%rbp), %rdx
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	jne	.L65
.L53:
	xorl	%eax, %eax
.L50:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L66
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	movq	24(%rbx), %rsi
	movq	8(%rbx), %rdi
	call	EVP_MD_CTX_copy_ex@PLT
	testl	%eax, %eax
	je	.L53
	movl	-116(%rbp), %edx
	movq	8(%rbx), %rdi
	movq	%r14, %rsi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L53
	movq	8(%rbx), %rdi
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L50
.L66:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE422:
	.size	HMAC_Final, .-HMAC_Final
	.p2align 4
	.globl	HMAC_size
	.type	HMAC_size, @function
HMAC_size:
.LFB423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	EVP_MD_size@PLT
	movl	$0, %edx
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	cmovs	%edx, %eax
	cltq
	ret
	.cfi_endproc
.LFE423:
	.size	HMAC_size, .-HMAC_size
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"../deps/openssl/openssl/crypto/hmac/hmac.c"
	.text
	.p2align 4
	.globl	HMAC_CTX_free
	.type	HMAC_CTX_free, @function
HMAC_CTX_free:
.LFB426:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L69
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	call	EVP_MD_CTX_reset@PLT
	movq	24(%r12), %rdi
	call	EVP_MD_CTX_reset@PLT
	movq	8(%r12), %rdi
	call	EVP_MD_CTX_reset@PLT
	movq	$0, (%r12)
	movq	16(%r12), %rdi
	call	EVP_MD_CTX_free@PLT
	movq	24(%r12), %rdi
	call	EVP_MD_CTX_free@PLT
	movq	8(%r12), %rdi
	call	EVP_MD_CTX_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$163, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC2(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L69:
	ret
	.cfi_endproc
.LFE426:
	.size	HMAC_CTX_free, .-HMAC_CTX_free
	.p2align 4
	.globl	HMAC_CTX_reset
	.type	HMAC_CTX_reset, @function
HMAC_CTX_reset:
.LFB428:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	call	EVP_MD_CTX_reset@PLT
	movq	24(%rbx), %rdi
	call	EVP_MD_CTX_reset@PLT
	movq	8(%rbx), %rdi
	call	EVP_MD_CTX_reset@PLT
	cmpq	$0, 16(%rbx)
	movq	$0, (%rbx)
	je	.L75
.L79:
	cmpq	$0, 24(%rbx)
	je	.L88
.L77:
	cmpq	$0, 8(%rbx)
	je	.L80
.L82:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	call	EVP_MD_CTX_new@PLT
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L79
.L78:
	call	EVP_MD_CTX_reset@PLT
	movq	24(%rbx), %rdi
	call	EVP_MD_CTX_reset@PLT
	movq	8(%rbx), %rdi
	call	EVP_MD_CTX_reset@PLT
	movq	$0, (%rbx)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	call	EVP_MD_CTX_new@PLT
	movq	%rax, 24(%rbx)
	testq	%rax, %rax
	jne	.L77
.L87:
	movq	16(%rbx), %rdi
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L80:
	call	EVP_MD_CTX_new@PLT
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	je	.L87
	jmp	.L82
	.cfi_endproc
.LFE428:
	.size	HMAC_CTX_reset, .-HMAC_CTX_reset
	.p2align 4
	.globl	HMAC_Init
	.type	HMAC_Init, @function
HMAC_Init:
.LFB420:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L98
	testq	%rcx, %rcx
	jne	.L101
.L98:
	xorl	%r8d, %r8d
	jmp	HMAC_Init_ex
	.p2align 4,,10
	.p2align 3
.L101:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rcx, -32(%rbp)
	movl	%edx, -20(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdi, -8(%rbp)
	call	HMAC_CTX_reset
	movq	-32(%rbp), %rcx
	movl	-20(%rbp), %edx
	xorl	%r8d, %r8d
	movq	-16(%rbp), %rsi
	movq	-8(%rbp), %rdi
	leave
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	HMAC_Init_ex
	.cfi_endproc
.LFE420:
	.size	HMAC_Init, .-HMAC_Init
	.p2align 4
	.globl	HMAC_CTX_new
	.type	HMAC_CTX_new, @function
HMAC_CTX_new:
.LFB424:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$137, %edx
	movl	$32, %edi
	leaq	.LC2(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L102
	movq	%rax, %rdi
	call	HMAC_CTX_reset
	testl	%eax, %eax
	je	.L108
.L102:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	movq	16(%r12), %rdi
	call	EVP_MD_CTX_reset@PLT
	movq	24(%r12), %rdi
	call	EVP_MD_CTX_reset@PLT
	movq	8(%r12), %rdi
	call	EVP_MD_CTX_reset@PLT
	movq	$0, (%r12)
	movq	16(%r12), %rdi
	call	EVP_MD_CTX_free@PLT
	movq	24(%r12), %rdi
	call	EVP_MD_CTX_free@PLT
	movq	8(%r12), %rdi
	call	EVP_MD_CTX_free@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movl	$163, %edx
	leaq	.LC2(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L102
	.cfi_endproc
.LFE424:
	.size	HMAC_CTX_new, .-HMAC_CTX_new
	.p2align 4
	.globl	HMAC_CTX_copy
	.type	HMAC_CTX_copy, @function
HMAC_CTX_copy:
.LFB429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpq	$0, 16(%rdi)
	movq	%rdi, %rbx
	je	.L110
.L114:
	cmpq	$0, 24(%rbx)
	je	.L125
.L112:
	cmpq	$0, 8(%rbx)
	je	.L126
.L115:
	movq	16(%r12), %rsi
	movq	16(%rbx), %rdi
	call	EVP_MD_CTX_copy_ex@PLT
	testl	%eax, %eax
	jne	.L116
.L124:
	movq	16(%rbx), %rdi
.L113:
	call	EVP_MD_CTX_reset@PLT
	movq	24(%rbx), %rdi
	call	EVP_MD_CTX_reset@PLT
	movq	8(%rbx), %rdi
	call	EVP_MD_CTX_reset@PLT
	movq	$0, (%rbx)
	xorl	%eax, %eax
.L109:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
	movq	24(%r12), %rsi
	movq	24(%rbx), %rdi
	call	EVP_MD_CTX_copy_ex@PLT
	testl	%eax, %eax
	je	.L124
	movq	8(%r12), %rsi
	movq	8(%rbx), %rdi
	call	EVP_MD_CTX_copy_ex@PLT
	testl	%eax, %eax
	je	.L124
	movq	(%r12), %rax
	movq	%rax, (%rbx)
	movl	$1, %eax
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L110:
	call	EVP_MD_CTX_new@PLT
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L113
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L125:
	call	EVP_MD_CTX_new@PLT
	movq	%rax, 24(%rbx)
	testq	%rax, %rax
	je	.L124
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L126:
	call	EVP_MD_CTX_new@PLT
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	je	.L124
	jmp	.L115
	.cfi_endproc
.LFE429:
	.size	HMAC_CTX_copy, .-HMAC_CTX_copy
	.p2align 4
	.globl	HMAC
	.type	HMAC, @function
HMAC:
.LFB430:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	leaq	.LC2(%rip), %rsi
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$32, %edi
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	movl	$137, %edx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$136, %rsp
	movq	16(%rbp), %rax
	movq	%rcx, -152(%rbp)
	movq	%r8, -160(%rbp)
	movq	%rax, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r9, %r9
	leaq	m.10529(%rip), %rax
	cmove	%rax, %rbx
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L127
	movq	%rax, %rdi
	call	HMAC_CTX_reset
	testl	%eax, %eax
	je	.L133
	testq	%r15, %r15
	jne	.L134
	testl	%r12d, %r12d
	leaq	dummy_key.10530(%rip), %rax
	cmove	%rax, %r15
.L134:
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	%r12d, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	HMAC_Init_ex
	testl	%eax, %eax
	je	.L133
	cmpq	$0, (%r14)
	je	.L133
	movq	8(%r14), %rdi
	movq	-160(%rbp), %rdx
	movq	-152(%rbp), %rsi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jne	.L163
.L133:
	movq	16(%r14), %rdi
	call	EVP_MD_CTX_reset@PLT
	movq	24(%r14), %rdi
	call	EVP_MD_CTX_reset@PLT
	movq	8(%r14), %rdi
	call	EVP_MD_CTX_reset@PLT
	movq	$0, (%r14)
	movq	16(%r14), %rdi
	call	EVP_MD_CTX_free@PLT
	movq	24(%r14), %rdi
	call	EVP_MD_CTX_free@PLT
	movq	8(%r14), %rdi
	call	EVP_MD_CTX_free@PLT
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	movl	$163, %edx
	leaq	.LC2(%rip), %rsi
	call	CRYPTO_free@PLT
.L127:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L164
	addq	$136, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L163:
	.cfi_restore_state
	cmpq	$0, (%r14)
	je	.L133
	leaq	-128(%rbp), %r12
	movq	8(%r14), %rdi
	leaq	-132(%rbp), %rdx
	movq	%r12, %rsi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	je	.L133
	movq	24(%r14), %rsi
	movq	8(%r14), %rdi
	call	EVP_MD_CTX_copy_ex@PLT
	testl	%eax, %eax
	je	.L133
	movl	-132(%rbp), %edx
	movq	8(%r14), %rdi
	movq	%r12, %rsi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L133
	movq	8(%r14), %rdi
	movq	-168(%rbp), %rdx
	movq	%rbx, %rsi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	je	.L133
	movq	16(%r14), %rdi
	call	EVP_MD_CTX_reset@PLT
	movq	24(%r14), %rdi
	call	EVP_MD_CTX_reset@PLT
	movq	8(%r14), %rdi
	call	EVP_MD_CTX_reset@PLT
	movq	$0, (%r14)
	movq	16(%r14), %rdi
	call	EVP_MD_CTX_free@PLT
	movq	24(%r14), %rdi
	call	EVP_MD_CTX_free@PLT
	movq	8(%r14), %rdi
	call	EVP_MD_CTX_free@PLT
	movq	%r14, %rdi
	movq	%rbx, %r14
	movl	$163, %edx
	leaq	.LC2(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L127
.L164:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE430:
	.size	HMAC, .-HMAC
	.p2align 4
	.globl	HMAC_CTX_set_flags
	.type	HMAC_CTX_set_flags, @function
HMAC_CTX_set_flags:
.LFB431:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	16(%rdi), %rdi
	movq	%rsi, %rbx
	call	EVP_MD_CTX_set_flags@PLT
	movq	24(%r12), %rdi
	movl	%ebx, %esi
	call	EVP_MD_CTX_set_flags@PLT
	movq	8(%r12), %rdi
	movl	%ebx, %esi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	EVP_MD_CTX_set_flags@PLT
	.cfi_endproc
.LFE431:
	.size	HMAC_CTX_set_flags, .-HMAC_CTX_set_flags
	.p2align 4
	.globl	HMAC_CTX_get_md
	.type	HMAC_CTX_get_md, @function
HMAC_CTX_get_md:
.LFB432:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE432:
	.size	HMAC_CTX_get_md, .-HMAC_CTX_get_md
	.section	.rodata
	.type	dummy_key.10530, @object
	.size	dummy_key.10530, 1
dummy_key.10530:
	.zero	1
	.local	m.10529
	.comm	m.10529,64,32
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.align 16
.LC1:
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
