	.file	"c_skey.c"
	.text
	.p2align 4
	.globl	CAST_set_key
	.type	CAST_set_key, @function
CAST_set_key:
.LFB0:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$632, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -672(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$16, %esi
	movl	$16, %eax
	movaps	%xmm0, -256(%rbp)
	cmovle	%esi, %eax
	movaps	%xmm0, -240(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm0, -208(%rbp)
	testl	%esi, %esi
	jle	.L5
	cmpl	$15, %esi
	jle	.L8
	movdqu	(%rdx), %xmm0
	pxor	%xmm1, %xmm1
	movl	%eax, %ecx
	andl	$-16, %ecx
	movdqa	%xmm0, %xmm2
	punpckhbw	%xmm1, %xmm0
	punpcklbw	%xmm1, %xmm2
	pxor	%xmm1, %xmm1
	movdqa	%xmm2, %xmm3
	punpckhwd	%xmm1, %xmm2
	movaps	%xmm2, -240(%rbp)
	movdqa	%xmm0, %xmm2
	punpcklwd	%xmm1, %xmm3
	punpckhwd	%xmm1, %xmm0
	punpcklwd	%xmm1, %xmm2
	movaps	%xmm3, -256(%rbp)
	movaps	%xmm2, -224(%rbp)
	movaps	%xmm0, -208(%rbp)
	testb	$15, %al
	je	.L5
.L4:
	movslq	%ecx, %rdi
	movzbl	(%rdx,%rdi), %r8d
	movl	%r8d, -256(%rbp,%rdi,4)
	leal	1(%rcx), %edi
	cmpl	%edi, %eax
	jle	.L5
	movslq	%edi, %rdi
	movzbl	(%rdx,%rdi), %r8d
	movl	%r8d, -256(%rbp,%rdi,4)
	leal	2(%rcx), %edi
	cmpl	%edi, %eax
	jle	.L5
	movslq	%edi, %rdi
	movzbl	(%rdx,%rdi), %r8d
	movl	%r8d, -256(%rbp,%rdi,4)
	leal	3(%rcx), %edi
	cmpl	%edi, %eax
	jle	.L5
	movslq	%edi, %rdi
	movzbl	(%rdx,%rdi), %r8d
	movl	%r8d, -256(%rbp,%rdi,4)
	leal	4(%rcx), %edi
	cmpl	%edi, %eax
	jle	.L5
	movslq	%edi, %rdi
	movzbl	(%rdx,%rdi), %r8d
	movl	%r8d, -256(%rbp,%rdi,4)
	leal	5(%rcx), %edi
	cmpl	%edi, %eax
	jle	.L5
	movslq	%edi, %rdi
	movzbl	(%rdx,%rdi), %r8d
	movl	%r8d, -256(%rbp,%rdi,4)
	leal	6(%rcx), %edi
	cmpl	%edi, %eax
	jle	.L5
	movslq	%edi, %rdi
	movzbl	(%rdx,%rdi), %r8d
	movl	%r8d, -256(%rbp,%rdi,4)
	leal	7(%rcx), %edi
	cmpl	%edi, %eax
	jle	.L5
	movslq	%edi, %rdi
	movzbl	(%rdx,%rdi), %r8d
	movl	%r8d, -256(%rbp,%rdi,4)
	leal	8(%rcx), %edi
	cmpl	%edi, %eax
	jle	.L5
	movslq	%edi, %rdi
	movzbl	(%rdx,%rdi), %r8d
	movl	%r8d, -256(%rbp,%rdi,4)
	leal	9(%rcx), %edi
	cmpl	%edi, %eax
	jle	.L5
	movslq	%edi, %rdi
	movzbl	(%rdx,%rdi), %r8d
	movl	%r8d, -256(%rbp,%rdi,4)
	leal	10(%rcx), %edi
	cmpl	%edi, %eax
	jle	.L5
	movslq	%edi, %rdi
	movzbl	(%rdx,%rdi), %r8d
	movl	%r8d, -256(%rbp,%rdi,4)
	leal	11(%rcx), %edi
	cmpl	%edi, %eax
	jle	.L5
	movslq	%edi, %rdi
	movzbl	(%rdx,%rdi), %r8d
	movl	%r8d, -256(%rbp,%rdi,4)
	leal	12(%rcx), %edi
	cmpl	%edi, %eax
	jle	.L5
	movslq	%edi, %rdi
	movzbl	(%rdx,%rdi), %r8d
	movl	%r8d, -256(%rbp,%rdi,4)
	leal	13(%rcx), %edi
	cmpl	%edi, %eax
	jle	.L5
	movslq	%edi, %rdi
	addl	$14, %ecx
	movzbl	(%rdx,%rdi), %r8d
	movl	%r8d, -256(%rbp,%rdi,4)
	cmpl	%ecx, %eax
	jle	.L5
	movslq	%ecx, %rcx
	movzbl	(%rdx,%rcx), %eax
	movl	%eax, -256(%rbp,%rcx,4)
	.p2align 4,,10
	.p2align 3
.L5:
	movq	-672(%rbp), %rbx
	xorl	%eax, %eax
	cmpl	$10, %esi
	movl	-236(%rbp), %r14d
	setle	%al
	movl	-220(%rbp), %edi
	movl	-224(%rbp), %r9d
	movl	$2, -656(%rbp)
	movl	%eax, 128(%rbx)
	movl	-240(%rbp), %eax
	sall	$16, %r14d
	leaq	CAST_S_table4(%rip), %r12
	movl	-216(%rbp), %esi
	movl	-256(%rbp), %r15d
	movl	%r9d, %r10d
	movq	%rdi, -400(%rbp)
	sall	$24, %eax
	movl	-252(%rbp), %r13d
	sall	$24, %r10d
	movl	-212(%rbp), %r8d
	orl	%r14d, %eax
	movl	-232(%rbp), %r14d
	orl	-228(%rbp), %eax
	sall	$24, %r15d
	sall	$16, %r13d
	movl	-208(%rbp), %edx
	movq	%rsi, -392(%rbp)
	sall	$8, %r14d
	orl	%r13d, %r15d
	movl	-248(%rbp), %r13d
	orl	-244(%rbp), %r15d
	orl	%r14d, %eax
	movl	-204(%rbp), %r14d
	movl	%r15d, %ecx
	movl	%edx, %ebx
	movl	%eax, -372(%rbp)
	movl	%edi, %eax
	sall	$8, %r13d
	leaq	CAST_S_table5(%rip), %r15
	sall	$16, %eax
	orl	%r13d, %ecx
	movl	%r14d, %r11d
	movl	-200(%rbp), %r13d
	orl	%eax, %r10d
	movl	%esi, %eax
	sall	$16, %r11d
	leaq	-128(%rbp), %rdi
	sall	$8, %eax
	orl	%r8d, %r10d
	sall	$24, %ebx
	movq	%rdi, -664(%rbp)
	orl	%eax, %r10d
	orl	%r11d, %ebx
	movl	%r13d, %r11d
	movl	%ecx, -260(%rbp)
	movl	%r10d, -268(%rbp)
	movl	-196(%rbp), %r10d
	sall	$8, %r11d
	movl	(%r15,%r10,4), %eax
	orl	%r10d, %ebx
	movl	%edx, %r10d
	movl	%r13d, %edx
	orl	%r11d, %ebx
	movl	%eax, -376(%rbp)
	leaq	CAST_S_table6(%rip), %rax
	movl	%ebx, %r13d
	movq	%rdx, %rbx
	movl	(%rax,%r9,4), %r11d
	movl	%r11d, -272(%rbp)
	movl	(%r15,%r8,4), %r11d
	movl	%r14d, %r15d
	movq	%r15, %rdi
	movl	%r11d, -264(%rbp)
	leaq	-192(%rbp), %r11
	movq	%r11, -384(%rbp)
	leaq	CAST_S_table7(%rip), %r11
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$1, -656(%rbp)
.L3:
	movl	(%r12,%rdi,4), %r8d
	xorl	(%rax,%r10,4), %r8d
	xorl	(%r11,%rbx,4), %r8d
	movl	-272(%rbp), %ebx
	xorl	-260(%rbp), %r8d
	xorl	-376(%rbp), %r8d
	xorl	%r8d, %ebx
	movl	%ebx, %r9d
	movl	%ebx, %edx
	movzbl	%bl, %ecx
	shrl	$24, %edx
	shrl	$16, %r9d
	movq	%rcx, -296(%rbp)
	movl	%edx, %r14d
	movzbl	%r9b, %r9d
	movq	-392(%rbp), %rdx
	movl	(%rax,%r9,4), %edi
	movl	(%r12,%r14,4), %esi
	movq	%r14, -288(%rbp)
	leaq	CAST_S_table5(%rip), %r14
	xorl	(%r11,%rcx,4), %esi
	movzbl	%bh, %ecx
	xorl	(%r11,%rdx,4), %esi
	xorl	(%r14,%rcx,4), %esi
	movl	%edi, -404(%rbp)
	xorl	%edi, %esi
	xorl	-268(%rbp), %esi
	movq	%rcx, -304(%rbp)
	movl	%esi, %r8d
	movl	%esi, %r10d
	movl	%esi, %ecx
	shrl	$16, %r8d
	shrl	$24, %r10d
	movzbl	%ch, %edx
	movzbl	%r8b, %r8d
	movl	(%r14,%rdx,4), %r15d
	movzbl	%sil, %r14d
	movl	(%rax,%r8,4), %edi
	movq	%r14, -312(%rbp)
	movl	%r15d, -408(%rbp)
	movl	%edi, %ecx
	movl	(%r11,%r10,4), %edi
	movl	%ecx, -412(%rbp)
	movl	%edi, -416(%rbp)
	xorl	(%r12,%r14,4), %edi
	movq	-400(%rbp), %r14
	xorl	(%r12,%r14,4), %edi
	xorl	%r15d, %edi
	xorl	%ecx, %edi
	xorl	%r13d, %edi
	movl	%edi, %r13d
	movl	%edi, %ecx
	shrl	$16, %r13d
	movzbl	%ch, %ecx
	movl	%r13d, %r14d
	movl	(%r12,%rcx,4), %r15d
	movl	%edi, %r13d
	movzbl	%r14b, %ecx
	leaq	CAST_S_table5(%rip), %r14
	shrl	$24, %r13d
	movl	(%r14,%rcx,4), %r14d
	movq	%rcx, -504(%rbp)
	movzbl	%dil, %ecx
	movq	%rcx, -512(%rbp)
	movq	%r13, -320(%rbp)
	movl	(%rax,%rcx,4), %r13d
	movq	-320(%rbp), %rcx
	movl	%r14d, -424(%rbp)
	movl	%r15d, -420(%rbp)
	xorl	(%r11,%rcx,4), %r13d
	xorl	%r15d, %r13d
	xorl	%r14d, %r13d
	xorl	-372(%rbp), %r13d
	xorl	-264(%rbp), %r13d
	movl	%r13d, %r14d
	movl	%r13d, %r15d
	movl	(%r11,%rdx,4), %r13d
	movq	-312(%rbp), %rdx
	shrl	$24, %r14d
	movl	%r14d, -428(%rbp)
	leaq	CAST_S_table5(%rip), %r14
	movl	%r13d, %ecx
	movq	-304(%rbp), %r13
	movl	(%r14,%rdx,4), %edx
	xorl	(%rax,%r10,4), %edx
	movl	%ecx, -432(%rbp)
	movq	-288(%rbp), %r10
	movl	(%r11,%r13,4), %r13d
	xorl	(%rax,%r10,4), %edx
	movl	%r13d, -436(%rbp)
	xorl	%ecx, %edx
	xorl	(%r12,%r8,4), %edx
	xorl	%edx, %edi
	movl	%edi, %r8d
	movl	%edi, %ecx
	shrl	$16, %r8d
	movzbl	%ch, %ecx
	xorl	%r13d, %ebx
	xorl	(%r12,%r9,4), %esi
	movl	%r8d, %edx
	movl	%edi, %r8d
	movl	(%r14,%rcx,4), %r10d
	shrl	$24, %r8d
	movzbl	%dl, %edx
	xorl	(%r12,%r8,4), %ebx
	movq	%r8, -520(%rbp)
	movzbl	%dil, %r8d
	xorl	(%r11,%r8,4), %ebx
	movq	%rdx, -536(%rbp)
	xorl	%r10d, %ebx
	xorl	(%rax,%rdx,4), %ebx
	movq	%r8, -528(%rbp)
	movl	%ebx, %r13d
	movzbl	%bh, %ecx
	movl	%r10d, -440(%rbp)
	shrl	$16, %r13d
	movq	%rcx, %r10
	leaq	CAST_S_table5(%rip), %rcx
	movl	%r13d, %edx
	movl	%ebx, %r13d
	movl	(%rcx,%r10,4), %r10d
	shrl	$24, %r13d
	movl	%r13d, %r8d
	movzbl	%bl, %r13d
	movq	%r13, -544(%rbp)
	movl	(%r12,%r13,4), %r13d
	movl	%r8d, %r9d
	xorl	%r13d, %esi
	movl	%r13d, -444(%rbp)
	movq	%r9, -552(%rbp)
	xorl	(%r11,%r9,4), %esi
	xorl	%r10d, %esi
	movl	%r10d, -448(%rbp)
	movzbl	%dl, %r10d
	movq	-296(%rbp), %r9
	xorl	(%rax,%r10,4), %esi
	movq	%r10, -560(%rbp)
	movl	%r15d, %r10d
	movl	%esi, %r8d
	xorl	(%rcx,%r9,4), %r10d
	movl	%r15d, -496(%rbp)
	movq	%rcx, %r15
	shrl	$16, %r8d
	movzbl	%sil, %ecx
	movl	%r10d, %r14d
	movl	%r8d, %edx
	movl	%esi, %r8d
	xorl	(%rax,%rcx,4), %r14d
	movq	%rcx, -328(%rbp)
	shrl	$24, %r8d
	movl	%esi, %ecx
	movzbl	%dl, %edx
	movl	%r8d, %r10d
	movzbl	%ch, %ecx
	movq	%rdx, -336(%rbp)
	xorl	(%r11,%r10,4), %r14d
	xorl	(%r12,%rcx,4), %r14d
	movq	%rcx, %r13
	movq	%r15, %rcx
	xorl	(%r15,%rdx,4), %r14d
	movl	(%rax,%r10,4), %r9d
	movq	%r10, -568(%rbp)
	movl	%r14d, %r15d
	xorl	(%r11,%r13,4), %esi
	movq	%r13, -576(%rbp)
	shrl	$16, %r15d
	xorl	%r9d, %edi
	movl	%r9d, -456(%rbp)
	movl	%r15d, %r8d
	movl	%r14d, %r15d
	movl	%edi, %r10d
	movzbl	%r14b, %edi
	shrl	$24, %r15d
	movzbl	%r8b, %r9d
	movq	%rdi, -584(%rbp)
	movl	%r15d, %edx
	movq	%r9, -592(%rbp)
	movl	(%rax,%rdx,4), %r15d
	xorl	%r15d, %r10d
	movl	%r15d, -452(%rbp)
	movl	%r10d, %edx
	movq	%rcx, %r10
	xorl	(%rcx,%rdi,4), %edx
	movl	%r14d, %ecx
	movzbl	%ch, %edi
	xorl	(%r12,%r9,4), %edx
	xorl	(%r11,%rdi,4), %edx
	movq	%rdi, -600(%rbp)
	movl	%edx, %r15d
	movzbl	%dh, %ecx
	shrl	$16, %r15d
	movl	(%r10,%rcx,4), %r10d
	movq	%rcx, -344(%rbp)
	movl	%r15d, %r9d
	movl	%edx, %r15d
	shrl	$24, %r15d
	movzbl	%r9b, %r9d
	movl	%r15d, %r13d
	movzbl	%dl, %r15d
	xorl	(%r12,%r13,4), %esi
	xorl	(%r11,%r15,4), %esi
	movq	%r13, -352(%rbp)
	xorl	%r10d, %esi
	movq	%r15, -360(%rbp)
	movl	%r10d, -460(%rbp)
	xorl	(%rax,%r9,4), %esi
	leaq	CAST_S_table5(%rip), %r10
	movzbl	%sil, %edi
	movl	%esi, %ecx
	movl	%esi, %r15d
	movl	%esi, %r8d
	movl	(%r12,%rdi,4), %edi
	shrl	$24, %r15d
	shrl	$16, %r8d
	movzbl	%r8b, %r8d
	movl	%edi, -276(%rbp)
	movzbl	%ch, %edi
	movq	%rdi, %r13
	movl	(%r10,%rdi,4), %edi
	movl	%edi, %ecx
	movq	-336(%rbp), %rdi
	movl	%ecx, -464(%rbp)
	xorl	(%r12,%rdi,4), %r14d
	movl	-276(%rbp), %edi
	xorl	%r14d, %edi
	movl	%r15d, %r14d
	xorl	(%r11,%r14,4), %edi
	movq	%r14, -368(%rbp)
	xorl	%ecx, %edi
	movq	-328(%rbp), %rcx
	xorl	(%rax,%r8,4), %edi
	movl	%edi, %r14d
	movl	%edi, %r15d
	shrl	$24, %r15d
	xorl	(%r10,%rcx,4), %ebx
	movzbl	%dil, %ecx
	shrl	$16, %r14d
	xorl	(%rax,%rcx,4), %ebx
	movq	%rcx, -608(%rbp)
	movl	%edi, %ecx
	xorl	(%r11,%r15,4), %ebx
	movzbl	%ch, %ecx
	movq	%r15, -616(%rbp)
	movzbl	%r14b, %r15d
	xorl	(%r12,%rcx,4), %ebx
	xorl	(%r10,%r15,4), %ebx
	movq	%rcx, -624(%rbp)
	movl	%ebx, %r14d
	shrl	$24, %ebx
	movq	%r15, -632(%rbp)
	movq	%r10, %r15
	movl	%ebx, -468(%rbp)
	movl	(%r12,%r9,4), %ebx
	movl	%ebx, -280(%rbp)
	movl	(%r12,%r8,4), %ebx
	movl	%ebx, %r9d
	movl	(%r11,%r13,4), %ebx
	movl	%r9d, -472(%rbp)
	movl	%ebx, %r13d
	movzbl	%sil, %ebx
	movl	(%r10,%rbx,4), %r8d
	movq	-368(%rbp), %rbx
	xorl	(%rax,%rbx,4), %r8d
	movq	-352(%rbp), %rbx
	xorl	(%rax,%rbx,4), %r8d
	movl	%r13d, -476(%rbp)
	xorl	%r9d, %r8d
	xorl	%r13d, %r8d
	movl	%r8d, %ebx
	xorl	%edi, %ebx
	movl	%ebx, -260(%rbp)
	shrl	$24, %ebx
	movl	-260(%rbp), %ecx
	movl	%ebx, %edi
	movl	-260(%rbp), %ebx
	movzbl	-260(%rbp), %r9d
	shrl	$16, %ebx
	movzbl	%bl, %r8d
	movl	(%rax,%r8,4), %ebx
	movl	%ebx, %r13d
	movq	-344(%rbp), %rbx
	movl	%r13d, -480(%rbp)
	xorl	(%r11,%rbx,4), %edx
	movl	%edi, %ebx
	xorl	(%r12,%rbx,4), %edx
	xorl	(%r11,%r9,4), %edx
	movq	%rbx, -640(%rbp)
	movzbl	%ch, %ebx
	xorl	(%r10,%rbx,4), %edx
	xorl	%r13d, %edx
	movl	%edx, %r8d
	movl	%edx, -372(%rbp)
	shrl	$16, %edx
	movzbl	%dl, %edx
	movzbl	%r8b, %r10d
	movl	%r8d, %ecx
	movl	(%rax,%rdx,4), %edi
	movl	%r8d, %edx
	shrl	$24, %edx
	xorl	-280(%rbp), %esi
	xorl	(%r12,%r10,4), %esi
	movl	(%r11,%rdx,4), %edx
	movl	%edi, %r13d
	movzbl	%ch, %edi
	movl	%r13d, -484(%rbp)
	xorl	%edx, %esi
	xorl	(%r15,%rdi,4), %esi
	movq	%rdi, -648(%rbp)
	xorl	%r13d, %esi
	movl	%esi, %r13d
	shrl	$16, %esi
	movzbl	%sil, %esi
	movl	%r13d, %ecx
	movl	%r13d, -268(%rbp)
	movl	%r13d, %r8d
	movq	%rsi, -400(%rbp)
	movl	(%r15,%rsi,4), %esi
	movzbl	%ch, %edi
	shrl	$24, %r8d
	movl	(%r12,%rdi,4), %r13d
	movq	%rdi, -392(%rbp)
	movl	%esi, %ecx
	movq	-360(%rbp), %rsi
	movl	%r13d, -488(%rbp)
	movl	(%r15,%rsi,4), %edi
	movzbl	-268(%rbp), %esi
	movl	%ecx, -492(%rbp)
	xorl	%r14d, %edi
	xorl	(%rax,%rsi,4), %edi
	xorl	(%r11,%r8,4), %edi
	xorl	%r13d, %edi
	movl	%edi, %r13d
	xorl	%ecx, %r13d
	movl	%r13d, %ecx
	movl	%r13d, %edi
	shrl	$24, %ecx
	shrl	$16, %edi
	movl	%ecx, -652(%rbp)
	movl	(%r15,%rsi,4), %esi
	movzbl	%dil, %edi
	movq	-648(%rbp), %rcx
	movl	%esi, -264(%rbp)
	movl	(%rax,%r8,4), %esi
	movl	%esi, -272(%rbp)
	movzbl	%r13b, %esi
	movl	(%r15,%rsi,4), %esi
	movl	%esi, %r15d
	movl	%esi, -376(%rbp)
	movl	(%rax,%r10,4), %esi
	xorl	(%r12,%r9,4), %esi
	xorl	(%r11,%rcx,4), %esi
	leaq	CAST_S_table5(%rip), %rcx
	xorl	(%r12,%r8,4), %esi
	movl	(%rax,%r9,4), %r8d
	movq	-640(%rbp), %r9
	xorl	(%rcx,%r10,4), %edx
	xorl	(%r11,%rbx,4), %r8d
	movl	-652(%rbp), %r10d
	movl	-480(%rbp), %ebx
	xorl	(%r11,%r9,4), %ebx
	xorl	-492(%rbp), %esi
	movl	%ebx, %r9d
	xorl	-272(%rbp), %r8d
	xorl	%r15d, %r9d
	xorl	(%r12,%r10,4), %r8d
	movd	%esi, %xmm0
	movq	-632(%rbp), %r15
	xorl	(%rcx,%rdi,4), %r8d
	movq	-360(%rbp), %rsi
	movl	%r13d, %ecx
	xorl	-484(%rbp), %edx
	movzbl	%ch, %ebx
	movd	%r8d, %xmm1
	xorl	-264(%rbp), %edx
	xorl	-488(%rbp), %edx
	xorl	(%r12,%rbx,4), %r9d
	leaq	CAST_S_table5(%rip), %rcx
	xorl	(%r11,%rdi,4), %r9d
	movd	%edx, %xmm6
	movl	-460(%rbp), %edx
	xorl	(%r12,%rsi,4), %edx
	xorl	(%r12,%r15,4), %edx
	movd	%r9d, %xmm5
	movl	%r14d, %esi
	movl	-468(%rbp), %r8d
	punpckldq	%xmm5, %xmm1
	punpckldq	%xmm6, %xmm0
	movzbl	%r14b, %r9d
	punpcklqdq	%xmm1, %xmm0
	xorl	(%rax,%r8,4), %edx
	shrl	$16, %esi
	movzbl	%sil, %esi
	xorl	(%r11,%rsi,4), %edx
	movl	-280(%rbp), %esi
	movd	%edx, %xmm1
	movq	-352(%rbp), %rdx
	xorl	(%rcx,%rdx,4), %esi
	xorl	(%r11,%r9,4), %esi
	xorl	(%rcx,%r8,4), %esi
	movl	%r14d, %ecx
	movq	-616(%rbp), %r14
	movzbl	%ch, %edx
	movl	-276(%rbp), %r8d
	leaq	CAST_S_table5(%rip), %rcx
	xorl	(%rax,%rdx,4), %esi
	movq	-344(%rbp), %rdx
	movd	%esi, %xmm5
	xorl	(%rax,%rdx,4), %r8d
	movl	-472(%rbp), %edx
	punpckldq	%xmm5, %xmm1
	xorl	-476(%rbp), %edx
	xorl	-464(%rbp), %r8d
	movl	%edx, %r9d
	xorl	(%rax,%r14,4), %r8d
	movq	-368(%rbp), %rdx
	movq	-608(%rbp), %r14
	xorl	(%r11,%r15,4), %r8d
	xorl	(%rcx,%rdx,4), %r9d
	movd	%r8d, %xmm2
	movq	-528(%rbp), %r8
	xorl	(%r11,%r14,4), %r9d
	movq	-624(%rbp), %r14
	xorl	(%rax,%r14,4), %r9d
	movq	%rcx, %r14
	movd	%r9d, %xmm7
	movl	-440(%rbp), %r9d
	xorl	(%r12,%r8,4), %r9d
	movq	%r14, %r15
	movl	%r9d, %edx
	movq	-568(%rbp), %r9
	punpckldq	%xmm7, %xmm2
	punpcklqdq	%xmm2, %xmm1
	xorl	(%r12,%r9,4), %edx
	movq	-592(%rbp), %r9
	xorl	-452(%rbp), %edx
	movl	(%r11,%r9,4), %esi
	xorl	%edx, %esi
	movq	-536(%rbp), %rdx
	movd	%esi, %xmm2
	movl	(%r12,%rdx,4), %esi
	movq	-520(%rbp), %rdx
	xorl	(%rcx,%rdx,4), %esi
	movq	-584(%rbp), %rdx
	xorl	(%r11,%rdx,4), %esi
	movq	-600(%rbp), %rdx
	xorl	(%rax,%rdx,4), %esi
	xorl	(%rcx,%r9,4), %esi
	movq	%r8, %r9
	movl	-444(%rbp), %r8d
	xorl	(%rax,%r9,4), %r8d
	movd	%esi, %xmm7
	movq	-336(%rbp), %r9
	movq	-544(%rbp), %rdx
	punpckldq	%xmm7, %xmm2
	xorl	-448(%rbp), %r8d
	xorl	-456(%rbp), %r8d
	xorl	(%r11,%r9,4), %r8d
	movq	-552(%rbp), %r9
	movq	-304(%rbp), %rsi
	movd	%r8d, %xmm3
	movl	(%rcx,%r9,4), %r9d
	movq	-328(%rbp), %rcx
	xorl	(%r11,%rdx,4), %r9d
	movq	-560(%rbp), %rdx
	xorl	(%r12,%rdx,4), %r9d
	xorl	(%r11,%rcx,4), %r9d
	movq	-576(%rbp), %rcx
	movq	-312(%rbp), %rdx
	xorl	(%rax,%rcx,4), %r9d
	movl	(%rax,%rdx,4), %edx
	movd	%r9d, %xmm6
	movq	-320(%rbp), %r9
	xorl	(%r12,%rsi,4), %edx
	punpckldq	%xmm6, %xmm3
	xorl	-432(%rbp), %edx
	xorl	(%r12,%r9,4), %edx
	xorl	-424(%rbp), %edx
	punpcklqdq	%xmm3, %xmm2
	movd	%edx, %xmm3
	movl	-416(%rbp), %edx
	movl	-408(%rbp), %esi
	xorl	-412(%rbp), %esi
	movq	-296(%rbp), %r8
	xorl	%esi, %edx
	movq	-512(%rbp), %rsi
	movl	-496(%rbp), %ecx
	xorl	(%r14,%rsi,4), %edx
	movl	-420(%rbp), %esi
	xorl	%edx, %esi
	movl	%esi, %r9d
	movl	-436(%rbp), %esi
	xorl	(%rax,%r8,4), %esi
	movq	-504(%rbp), %r8
	movd	%r9d, %xmm6
	punpckldq	%xmm6, %xmm3
	xorl	(%rax,%r8,4), %esi
	movl	%ecx, %r8d
	movl	%esi, %edx
	shrl	$16, %r8d
	movl	-428(%rbp), %esi
	movzbl	%r8b, %r8d
	xorl	(%r12,%rsi,4), %edx
	xorl	(%r14,%r8,4), %edx
	movzbl	%cl, %r8d
	movq	-288(%rbp), %r14
	movd	%edx, %xmm4
	movl	-404(%rbp), %edx
	xorl	(%r11,%r14,4), %edx
	xorl	(%r15,%r8,4), %edx
	movl	(%r11,%rsi,4), %r8d
	xorl	%edx, %r8d
	movzbl	%ch, %edx
	movq	-384(%rbp), %rcx
	movl	%r8d, %esi
	xorl	(%r12,%rdx,4), %esi
	cmpl	$1, -656(%rbp)
	movd	%esi, %xmm5
	movaps	%xmm2, 16(%rcx)
	punpckldq	%xmm5, %xmm4
	movaps	%xmm1, 32(%rcx)
	punpcklqdq	%xmm4, %xmm3
	movaps	%xmm0, 48(%rcx)
	movaps	%xmm3, (%rcx)
	movq	-664(%rbp), %rcx
	movq	%rcx, -384(%rbp)
	jne	.L9
	movdqa	.LC0(%rip), %xmm0
	movdqa	-128(%rbp), %xmm3
	movdqa	.LC1(%rip), %xmm2
	movdqa	-192(%rbp), %xmm1
	paddd	%xmm0, %xmm3
	movq	-672(%rbp), %rax
	pand	%xmm2, %xmm3
	movdqa	%xmm1, %xmm4
	punpckldq	%xmm3, %xmm4
	punpckhdq	%xmm3, %xmm1
	movdqa	-112(%rbp), %xmm3
	movups	%xmm1, 16(%rax)
	movdqa	-176(%rbp), %xmm1
	paddd	%xmm0, %xmm3
	movups	%xmm4, (%rax)
	pand	%xmm2, %xmm3
	movdqa	%xmm1, %xmm4
	punpckldq	%xmm3, %xmm4
	punpckhdq	%xmm3, %xmm1
	movdqa	-96(%rbp), %xmm3
	movups	%xmm1, 48(%rax)
	movdqa	-160(%rbp), %xmm1
	paddd	%xmm0, %xmm3
	movups	%xmm4, 32(%rax)
	paddd	-80(%rbp), %xmm0
	pand	%xmm2, %xmm3
	movdqa	%xmm1, %xmm4
	punpckhdq	%xmm3, %xmm1
	pand	%xmm2, %xmm0
	punpckldq	%xmm3, %xmm4
	movups	%xmm1, 80(%rax)
	movdqa	-144(%rbp), %xmm1
	movups	%xmm4, 64(%rax)
	movdqa	%xmm1, %xmm2
	punpckhdq	%xmm0, %xmm1
	punpckldq	%xmm0, %xmm2
	movups	%xmm1, 112(%rax)
	movups	%xmm2, 96(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L12
	addq	$632, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L8:
	.cfi_restore_state
	xorl	%ecx, %ecx
	jmp	.L4
.L12:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE0:
	.size	CAST_set_key, .-CAST_set_key
	.globl	CAST_S_table7
	.section	.rodata
	.align 32
	.type	CAST_S_table7, @object
	.size	CAST_S_table7, 1024
CAST_S_table7:
	.long	-501862387
	.long	-1143078916
	.long	-1477715267
	.long	895778965
	.long	2005530807
	.long	-423554533
	.long	237245952
	.long	86829237
	.long	296341424
	.long	-443207919
	.long	-320366326
	.long	-1819881100
	.long	709006108
	.long	1994621201
	.long	-1322389702
	.long	937287164
	.long	-560275791
	.long	168608556
	.long	-1105629143
	.long	-2069886656
	.long	-1155253745
	.long	-1261357105
	.long	-1269925392
	.long	77524477
	.long	185966941
	.long	1208824168
	.long	-1950622118
	.long	1721625922
	.long	-940775375
	.long	1066374631
	.long	1927223579
	.long	1971335949
	.long	-1811463599
	.long	1551748602
	.long	-1413583517
	.long	-1438637724
	.long	-1291725814
	.long	48746954
	.long	1398218158
	.long	2050065058
	.long	313056748
	.long	-39177379
	.long	393167848
	.long	1912293076
	.long	940740642
	.long	-829121836
	.long	-1203279443
	.long	-1772365726
	.long	-2097950635
	.long	1727764327
	.long	364383054
	.long	492521376
	.long	1291706479
	.long	-1030830920
	.long	1474851438
	.long	1685747964
	.long	-1719247548
	.long	1619776915
	.long	1814040067
	.long	970743798
	.long	1561002147
	.long	-1369198606
	.long	2123093554
	.long	1880132620
	.long	-1143779255
	.long	697884420
	.long	-1743981526
	.long	-1687292783
	.long	-1635852973
	.long	110200136
	.long	1489731079
	.long	997519150
	.long	1378877361
	.long	-767096628
	.long	478029773
	.long	-1528094373
	.long	1022481122
	.long	431258168
	.long	1112503832
	.long	897933369
	.long	-1659379993
	.long	669726182
	.long	-911214981
	.long	918222264
	.long	163866573
	.long	-1047981903
	.long	-518144133
	.long	114105080
	.long	1903216136
	.long	761148244
	.long	-723629734
	.long	1690750982
	.long	-1128217044
	.long	1037045171
	.long	1888456500
	.long	2010454850
	.long	642736655
	.long	616092351
	.long	365016990
	.long	1185228132
	.long	-120068786
	.long	1043824992
	.long	2023083429
	.long	-2053368411
	.long	-431646840
	.long	-1015298209
	.long	-620250612
	.long	108438443
	.long	2132974366
	.long	830746235
	.long	606445527
	.long	-121703310
	.long	-2090861384
	.long	1844756978
	.long	-1762283115
	.long	-49614596
	.long	-1325526196
	.long	-498045635
	.long	1335562986
	.long	-233442779
	.long	-1574734993
	.long	-1615543256
	.long	634407289
	.long	885462008
	.long	-1000242809
	.long	-361075048
	.long	2094100220
	.long	339117932
	.long	-246136569
	.long	-1092686316
	.long	1458155303
	.long	-1605721023
	.long	1022871705
	.long	-1829979418
	.long	-580451987
	.long	353796843
	.long	-1472008481
	.long	-38117196
	.long	-242189451
	.long	551748367
	.long	618185374
	.long	-516331717
	.long	-274317384
	.long	1904685140
	.long	-1225601221
	.long	-1624087486
	.long	-887774004
	.long	-1340455676
	.long	-236683891
	.long	-2075517979
	.long	-1159208996
	.long	1120655984
	.long	-847401462
	.long	1474845562
	.long	-717268234
	.long	550456716
	.long	-828058584
	.long	2043752612
	.long	881257467
	.long	869518812
	.long	2005220179
	.long	938474677
	.long	-989427848
	.long	-444550170
	.long	1315485940
	.long	-976702594
	.long	226533026
	.long	965733244
	.long	321539988
	.long	1136104718
	.long	804158748
	.long	573969341
	.long	-586757470
	.long	937399083
	.long	-1004240247
	.long	-1393300541
	.long	1461057207
	.long	-281773859
	.long	-228105873
	.long	-1052193820
	.long	-1873641122
	.long	1581322155
	.long	-1266015131
	.long	786071460
	.long	-394575644
	.long	-376528764
	.long	1485433313
	.long	-271347460
	.long	-586689701
	.long	-616016236
	.long	953673138
	.long	1467089153
	.long	1930354364
	.long	1533292819
	.long	-1802404273
	.long	1346121658
	.long	1685000834
	.long	1965281866
	.long	-529033579
	.long	-104760689
	.long	2052792609
	.long	-779634538
	.long	690371149
	.long	-1169093409
	.long	-2114683745
	.long	-1391369235
	.long	-361014939
	.long	436236910
	.long	289419410
	.long	14314871
	.long	1242357089
	.long	-1390459389
	.long	1616633776
	.long	-1628585116
	.long	585885352
	.long	-823668086
	.long	-1595459936
	.long	1432659641
	.long	277164553
	.long	-940863689
	.long	770115018
	.long	-1991158001
	.long	-553024981
	.long	-1117185428
	.long	-1441602318
	.long	-2025513969
	.long	-520707462
	.long	987383833
	.long	1290892879
	.long	225909803
	.long	1741533526
	.long	890078084
	.long	1496906255
	.long	1111072499
	.long	916028167
	.long	243534141
	.long	1252605537
	.long	-2090805125
	.long	531204876
	.long	290011180
	.long	-378133083
	.long	102027703
	.long	237315147
	.long	209093447
	.long	1486785922
	.long	220223953
	.long	-1536771298
	.long	-119928190
	.long	82940208
	.long	-1167176000
	.long	-1725542044
	.long	518464269
	.long	1353887104
	.long	-353474559
	.long	-1917672829
	.long	-359926370
	.globl	CAST_S_table6
	.align 32
	.type	CAST_S_table6, @object
	.size	CAST_S_table6, 1024
CAST_S_table6:
	.long	-2048901095
	.long	858518887
	.long	1714274303
	.long	-809085293
	.long	713916271
	.long	-1415853806
	.long	-564131679
	.long	539548191
	.long	36158695
	.long	1298409750
	.long	419087104
	.long	1358007170
	.long	749914897
	.long	-1305286820
	.long	1261868530
	.long	-1299773474
	.long	-1604338442
	.long	-851344919
	.long	-514842356
	.long	-498142787
	.long	-1318534271
	.long	-35330167
	.long	1551479000
	.long	512490819
	.long	1296650241
	.long	951993153
	.long	-1858277859
	.long	-1834509249
	.long	144139966
	.long	-1158763020
	.long	310820559
	.long	-1226126567
	.long	643875328
	.long	1969602020
	.long	1680088954
	.long	-2109154135
	.long	-1011634842
	.long	672358534
	.long	198762408
	.long	896343282
	.long	276269502
	.long	-1280120370
	.long	84060815
	.long	197145886
	.long	376173866
	.long	-351076478
	.long	-481793775
	.long	-749898474
	.long	1316698879
	.long	1598252827
	.long	-1661542345
	.long	1233235075
	.long	859989710
	.long	-1936506441
	.long	-791128896
	.long	-885363576
	.long	1203513385
	.long	1193654839
	.long	-1502948821
	.long	2060853022
	.long	207403770
	.long	1144516871
	.long	-1226335902
	.long	1121114134
	.long	177607304
	.long	-509230994
	.long	326409831
	.long	1929119770
	.long	-1311688201
	.long	-111659195
	.long	-820388008
	.long	-1094453418
	.long	-1066485200
	.long	119610148
	.long	1170376745
	.long	-916573825
	.long	-1131494127
	.long	951863017
	.long	-957941228
	.long	-1159178166
	.long	-1387348922
	.long	1183797387
	.long	2015970143
	.long	-249292741
	.long	-2111980897
	.long	-1342828556
	.long	-366195091
	.long	384012900
	.long	-1839969653
	.long	10178499
	.long	-1415148307
	.long	-1698074760
	.long	111523738
	.long	-1299878290
	.long	451689641
	.long	-1098676600
	.long	235406569
	.long	1441906262
	.long	-404408773
	.long	-1281232291
	.long	-136397947
	.long	1644036924
	.long	376726067
	.long	1006849064
	.long	-630387596
	.long	2041234796
	.long	1021632941
	.long	1374734338
	.long	-1728515238
	.long	371631263
	.long	-287823063
	.long	490221539
	.long	206551450
	.long	-1154328712
	.long	1053219195
	.long	1853335209
	.long	-882537636
	.long	-732811065
	.long	735133835
	.long	1623211703
	.long	-1190752904
	.long	-1556654860
	.long	-198129539
	.long	-928574718
	.long	-1184003022
	.long	-338368578
	.long	-1098146515
	.long	2038037254
	.long	-417180920
	.long	-1955213449
	.long	300912036
	.long	-528234408
	.long	-1922336657
	.long	1516443558
	.long	-94570592
	.long	1574567987
	.long	-225525840
	.long	-172375280
	.long	-1595227520
	.long	146372218
	.long	-1546005840
	.long	2043888151
	.long	35287437
	.long	-1698286742
	.long	655490400
	.long	1132482787
	.long	110692520
	.long	1031794116
	.long	-2106774545
	.long	1324057718
	.long	1217253157
	.long	919197030
	.long	686247489
	.long	-1033827638
	.long	1028237775
	.long	-1159480865
	.long	-1235251738
	.long	-1834045596
	.long	986174950
	.long	-1633155831
	.long	-232062595
	.long	-1541980304
	.long	-585230653
	.long	367056889
	.long	1353824391
	.long	731860949
	.long	1650113154
	.long	1778481506
	.long	784341916
	.long	357075625
	.long	-686364864
	.long	1074092588
	.long	-1814914526
	.long	-483541094
	.long	92751289
	.long	877911070
	.long	-694605458
	.long	1231880047
	.long	480201094
	.long	-538776313
	.long	-1200471343
	.long	434011822
	.long	87971354
	.long	363687820
	.long	1717726236
	.long	1901380172
	.long	-368563414
	.long	-1813305031
	.long	400339184
	.long	1490350766
	.long	-1633512197
	.long	1389319756
	.long	-1736180122
	.long	784598401
	.long	1983468483
	.long	30828846
	.long	-744439544
	.long	-1578691058
	.long	-453845082
	.long	1765724805
	.long	1955612312
	.long	1277890269
	.long	1333098070
	.long	1564029816
	.long	-1590549681
	.long	1026694237
	.long	-1007296108
	.long	1260819201
	.long	-945880529
	.long	1016692350
	.long	1582273796
	.long	1073413053
	.long	1995943182
	.long	694588404
	.long	1025494639
	.long	-971094594
	.long	-743068876
	.long	-148112969
	.long	453260480
	.long	1316140391
	.long	1435673405
	.long	-1256025343
	.long	-808277889
	.long	1622062951
	.long	403978347
	.long	817677117
	.long	950059133
	.long	-48888078
	.long	-1016901221
	.long	1486738320
	.long	1417279718
	.long	481875527
	.long	-1745002071
	.long	-361276940
	.long	760697757
	.long	1452955855
	.long	-397515859
	.long	1177426808
	.long	1702951038
	.long	-209618668
	.long	-1847962124
	.long	1084371187
	.long	-778531019
	.long	-1226630958
	.long	1073369276
	.long	1027665953
	.long	-1010778706
	.long	1230553676
	.long	1368340146
	.long	-2068720784
	.long	267243139
	.long	-2020746534
	.long	-224233017
	.long	-1797252120
	.long	-1871614133
	.long	-1790211421
	.globl	CAST_S_table5
	.align 32
	.type	CAST_S_table5, @object
	.size	CAST_S_table5, 1024
CAST_S_table5:
	.long	-151351395
	.long	749497569
	.long	1285769319
	.long	-499941508
	.long	-1780807449
	.long	23610292
	.long	-319988548
	.long	844452780
	.long	-1080096416
	.long	-543038739
	.long	-2081400931
	.long	1676510905
	.long	448177848
	.long	-564216263
	.long	-208668878
	.long	-1987464904
	.long	871450977
	.long	-1072089155
	.long	-184105254
	.long	-463315330
	.long	-1559696743
	.long	1310974780
	.long	2043402188
	.long	1218528103
	.long	-1558931943
	.long	-20362283
	.long	-1592518838
	.long	-358606746
	.long	-1601905875
	.long	162023535
	.long	-1467457206
	.long	687910808
	.long	23484817
	.long	-510056349
	.long	-923595680
	.long	779677500
	.long	-791340750
	.long	-821040108
	.long	-137754670
	.long	-794288014
	.long	-46065282
	.long	-1828346192
	.long	-395582502
	.long	1958663117
	.long	925738300
	.long	1283408968
	.long	-625617856
	.long	1840910019
	.long	137959847
	.long	-1615139111
	.long	1239142320
	.long	1315376211
	.long	1547541505
	.long	1690155329
	.long	739140458
	.long	-1166157363
	.long	-361794680
	.long	-418658462
	.long	905091803
	.long	1548541325
	.long	-254505588
	.long	-1199483934
	.long	144808038
	.long	451078856
	.long	676114313
	.long	-1433239005
	.long	-1825259949
	.long	993665471
	.long	373509091
	.long	-1695926010
	.long	-269958290
	.long	-124727847
	.long	-2145227346
	.long	-1019173725
	.long	-545350647
	.long	-1500207097
	.long	1534877388
	.long	572371878
	.long	-1704353745
	.long	1753320020
	.long	-827184785
	.long	1405125690
	.long	-24562091
	.long	633333386
	.long	-1268610372
	.long	-819843393
	.long	632057672
	.long	-1448504441
	.long	1404951397
	.long	-412091417
	.long	-379060872
	.long	195638627
	.long	-1909183551
	.long	-392094743
	.long	1233155085
	.long	-938967556
	.long	-1914388583
	.long	-1592720992
	.long	2144565621
	.long	-631626048
	.long	-400582321
	.long	-1792488055
	.long	-46948371
	.long	-1200081729
	.long	1594115437
	.long	572884632
	.long	-909850565
	.long	767645374
	.long	1331858858
	.long	1475698373
	.long	-501085506
	.long	-762220865
	.long	1321687957
	.long	619889600
	.long	1121017241
	.long	-854753376
	.long	2070816767
	.long	-1461941520
	.long	1933951238
	.long	-199351505
	.long	890643334
	.long	-420837082
	.long	859025556
	.long	360630002
	.long	925594799
	.long	1764062180
	.long	-374745016
	.long	-216661367
	.long	979562269
	.long	-1484266952
	.long	-207227274
	.long	1949714515
	.long	546639971
	.long	1165388173
	.long	-1225075705
	.long	1495988560
	.long	922170659
	.long	1291546247
	.long	2107952832
	.long	1813327274
	.long	-888957272
	.long	-988938659
	.long	-53016661
	.long	153207855
	.long	-1981812549
	.long	1608695416
	.long	1150242611
	.long	1967526857
	.long	721801357
	.long	1220138373
	.long	-603679679
	.long	-938897509
	.long	2112743302
	.long	-1013304461
	.long	1111556101
	.long	1778980689
	.long	250857638
	.long	-1996459306
	.long	673216130
	.long	-1448478786
	.long	-1087215715
	.long	-732210315
	.long	-1286341376
	.long	-877599912
	.long	-2096160246
	.long	529510932
	.long	-747450616
	.long	-868464109
	.long	-1930022554
	.long	102533054
	.long	-2000056440
	.long	1617093527
	.long	1204784762
	.long	-1228385661
	.long	1019391227
	.long	1069574518
	.long	1317995090
	.long	1691889997
	.long	-633835293
	.long	510022745
	.long	-1056372496
	.long	1362108837
	.long	1817929911
	.long	-2110813536
	.long	805817662
	.long	1953603311
	.long	-595122559
	.long	120799444
	.long	2118332377
	.long	207536705
	.long	-2012665748
	.long	-174925679
	.long	145305846
	.long	-1786842363
	.long	-1208221763
	.long	-1033442961
	.long	1877257368
	.long	-1317802816
	.long	-1134513110
	.long	-1791715110
	.long	-73290222
	.long	759945014
	.long	254147243
	.long	-1527513877
	.long	-493448925
	.long	629083197
	.long	-1823953079
	.long	907280572
	.long	-394170550
	.long	940896768
	.long	-1543946173
	.long	-1669704510
	.long	-1133490345
	.long	-633214983
	.long	-1034235078
	.long	1425318020
	.long	-1317055227
	.long	1496677566
	.long	-306375224
	.long	2140652971
	.long	-1168455755
	.long	-1225335121
	.long	977771578
	.long	1392695845
	.long	1698528874
	.long	1411812681
	.long	1369733098
	.long	1343739227
	.long	-674079352
	.long	1142123638
	.long	67414216
	.long	-1192910559
	.long	-1206218102
	.long	1626167401
	.long	-1748673642
	.long	-353593061
	.long	697522451
	.long	33404913
	.long	143560186
	.long	-1699285259
	.long	994885535
	.long	1247667115
	.long	-435872459
	.long	-1595811755
	.long	-747942671
	.long	-180032021
	.long	-1326893788
	.long	-1095004227
	.long	-1562942769
	.long	1237921620
	.long	951448369
	.long	1898488916
	.long	1211705605
	.long	-1503978056
	.long	-2061723715
	.long	-696922321
	.globl	CAST_S_table4
	.align 32
	.type	CAST_S_table4, @object
	.size	CAST_S_table4, 1024
CAST_S_table4:
	.long	2127105028
	.long	745436345
	.long	-1693554977
	.long	-1506576111
	.long	-1200979969
	.long	500390133
	.long	1155374404
	.long	389092991
	.long	150729210
	.long	-403369524
	.long	-771417344
	.long	1935325696
	.long	716645080
	.long	946045387
	.long	-1393155014
	.long	1774124410
	.long	-425531521
	.long	-255385395
	.long	-1001830378
	.long	-856309376
	.long	948246080
	.long	363898952
	.long	-427091765
	.long	1286266623
	.long	1598556673
	.long	68334250
	.long	630723836
	.long	1104211938
	.long	1312863373
	.long	613332731
	.long	-1917182722
	.long	1101634306
	.long	441780740
	.long	-1165007413
	.long	1917973735
	.long	-1784342747
	.long	-1056510761
	.long	-1750755318
	.long	-986072662
	.long	1299840618
	.long	-218892445
	.long	1756332096
	.long	-317940138
	.long	297047435
	.long	-504669560
	.long	-2029394256
	.long	-673156778
	.long	1311375015
	.long	1667687725
	.long	47300608
	.long	-995324411
	.long	-1820854927
	.long	201668394
	.long	1468347890
	.long	576830978
	.long	-700276535
	.long	-552361344
	.long	1958042578
	.long	1747032512
	.long	-735975956
	.long	1408974056
	.long	-928125517
	.long	682131401
	.long	1033214337
	.long	1545599232
	.long	-29830247
	.long	206503691
	.long	103024618
	.long	-1439739983
	.long	1337551222
	.long	-1865968379
	.long	-1331124364
	.long	-279600641
	.long	-442719550
	.long	-1498010329
	.long	-429243805
	.long	-547028961
	.long	247794022
	.long	-539142724
	.long	702416469
	.long	-1860275302
	.long	397379957
	.long	851939612
	.long	-1980197784
	.long	218229120
	.long	1380406772
	.long	62274761
	.long	214451378
	.long	-1124863830
	.long	-2018756887
	.long	-449154010
	.long	28563499
	.long	446592073
	.long	1693330814
	.long	-841240102
	.long	29968656
	.long	-1201094784
	.long	220656637
	.long	-1824330265
	.long	77972100
	.long	1667708854
	.long	1358280214
	.long	-230201629
	.long	-1899350335
	.long	325977563
	.long	-17726575
	.long	-74941897
	.long	-689440812
	.long	-939819575
	.long	811859167
	.long	-1225422370
	.long	-332840486
	.long	652502677
	.long	-1219075047
	.long	-162205755
	.long	-796043081
	.long	1217549313
	.long	-1044722817
	.long	-436251377
	.long	-1240977335
	.long	1538642152
	.long	-2015941030
	.long	-1419088159
	.long	574252750
	.long	-970198067
	.long	-1643608583
	.long	1758150215
	.long	141295887
	.long	-1575098336
	.long	-779392546
	.long	-201959561
	.long	-100482058
	.long	1082055363
	.long	-877406896
	.long	395511885
	.long	-1328083270
	.long	179534037
	.long	-648938740
	.long	-556279210
	.long	1092926436
	.long	-1798698154
	.long	257381841
	.long	-522066578
	.long	1636087230
	.long	1477059743
	.long	-1795732544
	.long	-483948402
	.long	-1619307167
	.long	-1008991616
	.long	90732309
	.long	1684827095
	.long	1150307763
	.long	1723134115
	.long	-1057921910
	.long	1769919919
	.long	1240018934
	.long	815675215
	.long	750138730
	.long	-2055174797
	.long	1234303040
	.long	1995484674
	.long	138143821
	.long	675421338
	.long	1145607174
	.long	1936608440
	.long	-1056364272
	.long	-1949737018
	.long	2105974004
	.long	323969391
	.long	779555213
	.long	-1290064927
	.long	-1433357198
	.long	1017501463
	.long	2098600890
	.long	-1666346992
	.long	-1354355806
	.long	-1612424750
	.long	1171473753
	.long	-638395885
	.long	-607759225
	.long	-203097778
	.long	393037935
	.long	159126506
	.long	1662887367
	.long	1147106178
	.long	391545844
	.long	-842634601
	.long	1891500680
	.long	-1278357646
	.long	1851642611
	.long	546529401
	.long	1167818917
	.long	-1100946725
	.long	-1446891263
	.long	-341495460
	.long	575554290
	.long	475796850
	.long	-160294100
	.long	450035699
	.long	-1943715762
	.long	844027695
	.long	1080539133
	.long	86184846
	.long	1554234488
	.long	-602941842
	.long	1972511363
	.long	2018339607
	.long	1491841390
	.long	1141460869
	.long	1061690759
	.long	-50418053
	.long	2008416118
	.long	-1943862593
	.long	-1426819754
	.long	1598468138
	.long	722020353
	.long	1027143159
	.long	212344630
	.long	1387219594
	.long	1725294528
	.long	-549779340
	.long	-1794813680
	.long	458938280
	.long	-165751379
	.long	1828119673
	.long	544571780
	.long	-791741851
	.long	-1997029800
	.long	1241802790
	.long	267843827
	.long	-1600356496
	.long	1397140384
	.long	1558801448
	.long	-512299613
	.long	1806446719
	.long	929573330
	.long	-2060054615
	.long	400817706
	.long	616011623
	.long	-173446368
	.long	-691198571
	.long	1761550015
	.long	1968522284
	.long	-241236290
	.long	-102734438
	.long	-289847011
	.long	872482584
	.long	-1154430280
	.long	-400359915
	.long	-2007561853
	.long	1963876937
	.long	-631079339
	.long	1584857000
	.long	-1319942842
	.long	1833426440
	.long	-269883436
	.globl	CAST_S_table3
	.align 32
	.type	CAST_S_table3, @object
	.size	CAST_S_table3, 1024
CAST_S_table3:
	.long	-1649212384
	.long	532081118
	.long	-1480688657
	.long	-764173672
	.long	1246723035
	.long	1689095255
	.long	-2058288061
	.long	-100528431
	.long	2116582143
	.long	-435177885
	.long	157234593
	.long	2045505824
	.long	-49963709
	.long	1687664561
	.long	-211542173
	.long	605965023
	.long	672431967
	.long	1336064205
	.long	-918355904
	.long	214114848
	.long	-36500688
	.long	-1062914225
	.long	489488601
	.long	605322005
	.long	-296939238
	.long	264917351
	.long	1912574028
	.long	756637694
	.long	436560991
	.long	202637054
	.long	135989450
	.long	85393697
	.long	-2142043904
	.long	-398565634
	.long	-1399130888
	.long	2145855233
	.long	-759632289
	.long	115294817
	.long	-1147233398
	.long	1922296357
	.long	-830144545
	.long	-177108991
	.long	1037454084
	.long	-1569774021
	.long	2127856640
	.long	1417604070
	.long	1148013728
	.long	1827919605
	.long	642362335
	.long	-1365194763
	.long	909348033
	.long	1346338451
	.long	-747167647
	.long	297154785
	.long	1917849091
	.long	-133254469
	.long	-1411362770
	.long	-326273058
	.long	1469521537
	.long	-514889914
	.long	-919383040
	.long	1763717519
	.long	136166297
	.long	-3996507
	.long	1295325189
	.long	2134727907
	.long	-1496815930
	.long	1566297257
	.long	-622039062
	.long	-1617793135
	.long	-1622793681
	.long	965822077
	.long	-1514181234
	.long	289653839
	.long	1133871874
	.long	-803123477
	.long	35685304
	.long	1068898316
	.long	418943774
	.long	672553190
	.long	642281022
	.long	-1948808592
	.long	1954014401
	.long	-1257840516
	.long	-215152091
	.long	2030668546
	.long	-454378623
	.long	672283427
	.long	1776201016
	.long	359975446
	.long	-544793758
	.long	555499703
	.long	-1524982023
	.long	1324923
	.long	69110472
	.long	152125443
	.long	-1118182190
	.long	-472820011
	.long	1340634837
	.long	798073664
	.long	1434183902
	.long	15393959
	.long	216384236
	.long	1303690150
	.long	-413745665
	.long	-583833172
	.long	-333991883
	.long	106373927
	.long	-1716533072
	.long	1455997841
	.long	1801814300
	.long	1578393881
	.long	1854262133
	.long	-1106788350
	.long	-1036888713
	.long	-1992297236
	.long	1539295533
	.long	-789824731
	.long	-1216341321
	.long	-1922221276
	.long	549938159
	.long	-1016683012
	.long	-1674041216
	.long	181285381
	.long	-1429646198
	.long	-324937785
	.long	68876850
	.long	488006234
	.long	1728155692
	.long	-1686799788
	.long	836007927
	.long	-1859735503
	.long	919367643
	.long	-955544762
	.long	-639210936
	.long	1457871481
	.long	40520939
	.long	1380155135
	.long	797931188
	.long	234455205
	.long	-2039165469
	.long	-304478997
	.long	397000196
	.long	739833055
	.long	-1217101923
	.long	-1423247436
	.long	-272413408
	.long	772369276
	.long	390177364
	.long	-441016267
	.long	557662966
	.long	740064294
	.long	1640166671
	.long	1699928825
	.long	-759025160
	.long	622006121
	.long	-669614174
	.long	68743880
	.long	1742502
	.long	219489963
	.long	1664179233
	.long	1577743084
	.long	1236991741
	.long	410585305
	.long	-1928479354
	.long	823226535
	.long	1050371084
	.long	-868347689
	.long	-708127818
	.long	212779912
	.long	-147848735
	.long	1819446015
	.long	1911218849
	.long	530248558
	.long	-808726225
	.long	-1042381801
	.long	-1408778645
	.long	-884694568
	.long	-1952772266
	.long	20547779
	.long	-1312477238
	.long	-1262603827
	.long	-663214074
	.long	312714466
	.long	1870521650
	.long	1493008054
	.long	-803280640
	.long	615382978
	.long	-191295547
	.long	-1760449851
	.long	1932181
	.long	-2098862126
	.long	278426614
	.long	6369430
	.long	-1020422879
	.long	-1381948929
	.long	697336853
	.long	2143000447
	.long	-1348553765
	.long	701099306
	.long	1558357093
	.long	-1489964244
	.long	-794148888
	.long	-1973632879
	.long	-727831321
	.long	216290473
	.long	-703935098
	.long	23009561
	.long	1996984579
	.long	-559924490
	.long	2024298078
	.long	-555526433
	.long	569400510
	.long	-1955208313
	.long	-1278933423
	.long	-1197095953
	.long	-655444270
	.long	-450642313
	.long	-1038793431
	.long	795471839
	.long	-1343849733
	.long	-193936206
	.long	-203363493
	.long	-691234698
	.long	971261452
	.long	534414648
	.long	428311343
	.long	-905940121
	.long	-1450097416
	.long	694888862
	.long	1227866773
	.long	-1838760277
	.long	-1251512727
	.long	-1680613926
	.long	-545389265
	.long	-618303460
	.long	459166190
	.long	-162323226
	.long	1794958188
	.long	51825668
	.long	-2042355394
	.long	-1210295856
	.long	2036672799
	.long	-858325693
	.long	1099053433
	.long	-1825845770
	.long	-1235762355
	.long	1323291266
	.long	2061838604
	.long	1018778475
	.long	-2061623042
	.long	-1741466242
	.long	334295216
	.long	-738217102
	.long	1065731521
	.long	183467730
	.globl	CAST_S_table2
	.align 32
	.type	CAST_S_table2, @object
	.size	CAST_S_table2, 1024
CAST_S_table2:
	.long	-1913667008
	.long	637164959
	.long	-342868545
	.long	-401553145
	.long	1197506559
	.long	916448331
	.long	-1944074684
	.long	-1362179440
	.long	-1095632449
	.long	-285488406
	.long	-389080752
	.long	1373570990
	.long	-1844541434
	.long	-257096376
	.long	-516125309
	.long	-1838149419
	.long	286293407
	.long	124026297
	.long	-1293687596
	.long	1028597854
	.long	-1179670496
	.long	-86080800
	.long	-1603852661
	.long	-2106427090
	.long	1430237888
	.long	1218109995
	.long	-722495596
	.long	308166588
	.long	570424558
	.long	-2107958275
	.long	-1839872531
	.long	307733056
	.long	1310360322
	.long	-1159692289
	.long	1384269543
	.long	-1906895858
	.long	863238079
	.long	-1935703672
	.long	-1493414168
	.long	-914180699
	.long	-1463804489
	.long	1470087780
	.long	1728663345
	.long	-222478497
	.long	1090516929
	.long	532123132
	.long	-1905536319
	.long	1132193179
	.long	-1716503105
	.long	-1243888053
	.long	1670234342
	.long	1434557849
	.long	-1583888356
	.long	1241591150
	.long	-980923864
	.long	-859607183
	.long	-1203518957
	.long	1812415473
	.long	-2096527044
	.long	267246943
	.long	796911696
	.long	-675250306
	.long	38830015
	.long	1526438404
	.long	-1488465200
	.long	374413614
	.long	-1351565506
	.long	1489179520
	.long	1603809326
	.long	1920779204
	.long	168801282
	.long	260042626
	.long	-1936261715
	.long	1563175598
	.long	-1897293239
	.long	1356499128
	.long	-2077756256
	.long	514611088
	.long	2037363785
	.long	-2108498923
	.long	-272794213
	.long	-1502455427
	.long	-1381482280
	.long	1173701892
	.long	-94538749
	.long	-398540027
	.long	1334932762
	.long	-1839830590
	.long	602925377
	.long	-1459359442
	.long	1613172210
	.long	41346230
	.long	-1795332748
	.long	-1837529678
	.long	-2106139701
	.long	41386358
	.long	-122711667
	.long	1313404830
	.long	-1889440289
	.long	-492993522
	.long	-2077262461
	.long	873260488
	.long	-1766082942
	.long	-1816874680
	.long	-282051413
	.long	-1739608280
	.long	2006953883
	.long	-1831053811
	.long	575479328
	.long	-2076726648
	.long	2099895446
	.long	660001756
	.long	-1953465106
	.long	-1256205760
	.long	-406815517
	.long	-446253919
	.long	-1008115362
	.long	1022894237
	.long	1620365795
	.long	-845372607
	.long	1551255054
	.long	15374395
	.long	-724141951
	.long	-45656276
	.long	-143856167
	.long	-1113054564
	.long	310226346
	.long	1133119310
	.long	530038928
	.long	136043402
	.long	-1818198338
	.long	-1187460587
	.long	-1750057729
	.long	1036173560
	.long	-1927630100
	.long	1681395281
	.long	1758231547
	.long	-653318264
	.long	306774401
	.long	1575354324
	.long	-578881430
	.long	1990386196
	.long	-1180433560
	.long	-1839360625
	.long	1262092282
	.long	-1170624791
	.long	-1526738165
	.long	-84438213
	.long	1833535011
	.long	423410938
	.long	660763973
	.long	-2107837318
	.long	1639812000
	.long	-786545967
	.long	-827521804
	.long	310289298
	.long	272797111
	.long	-2106414734
	.long	-1838103384
	.long	310240523
	.long	677093832
	.long	1013118031
	.long	901835429
	.long	-402271695
	.long	1116285435
	.long	-1258496126
	.long	1337354835
	.long	243122523
	.long	520626091
	.long	277223598
	.long	-50526099
	.long	-100718455
	.long	1766575121
	.long	594173102
	.long	316590669
	.long	742362309
	.long	-758108674
	.long	-118531946
	.long	-456174886
	.long	-1793762457
	.long	1229605004
	.long	-1179211764
	.long	1552908988
	.long	-1982633147
	.long	979407927
	.long	-335492695
	.long	1148277331
	.long	176638793
	.long	-680281024
	.long	2083809052
	.long	40992502
	.long	1340822838
	.long	-1563414529
	.long	-759209788
	.long	-734067776
	.long	1354035053
	.long	122129617
	.long	7215240
	.long	-1562034347
	.long	-1176054596
	.long	-1576763370
	.long	-1755891661
	.long	-685736601
	.long	-569405635
	.long	1928887091
	.long	-1412673741
	.long	1988674909
	.long	2063640240
	.long	-1803878399
	.long	1459647954
	.long	-105150216
	.long	-1992162914
	.long	1113892351
	.long	-2057108768
	.long	1927010603
	.long	-292086935
	.long	1856122846
	.long	1594404395
	.long	-1350934163
	.long	-439777433
	.long	-819991598
	.long	1643104450
	.long	-240376463
	.long	-863880766
	.long	1730235576
	.long	-1310358575
	.long	-1210302878
	.long	2131803598
	.long	-116761544
	.long	267404349
	.long	1617849798
	.long	1616132681
	.long	1462223176
	.long	736725533
	.long	-1967909064
	.long	551665188
	.long	-1349068273
	.long	1749386277
	.long	-1719452699
	.long	1611482493
	.long	674206544
	.long	-2093698206
	.long	-652406496
	.long	728599968
	.long	1680547377
	.long	-1674552832
	.long	1388111496
	.long	453204106
	.long	-138743851
	.long	1094905244
	.long	-1540269039
	.long	-2093859131
	.long	-537967050
	.long	-1590442751
	.long	-372026596
	.long	-298502269
	.globl	CAST_S_table1
	.align 32
	.type	CAST_S_table1, @object
	.size	CAST_S_table1, 1024
CAST_S_table1:
	.long	522195092
	.long	-284448933
	.long	1776537470
	.long	960447360
	.long	-27144326
	.long	-289070982
	.long	1435016340
	.long	1929119313
	.long	-1381503111
	.long	1310552629
	.long	-715496498
	.long	-570149190
	.long	-1715195665
	.long	1594623892
	.long	417127293
	.long	-1579749389
	.long	-1598738565
	.long	1508390405
	.long	-300568428
	.long	-369108727
	.long	-599523194
	.long	-275495847
	.long	-1165767501
	.long	-524038661
	.long	-774225535
	.long	990456497
	.long	-107482687
	.long	-1511600261
	.long	21106139
	.long	-454561957
	.long	631373633
	.long	-511641594
	.long	532942976
	.long	396095098
	.long	-746928471
	.long	-27774812
	.long	-1730245761
	.long	2011709262
	.long	2039648873
	.long	620404603
	.long	-518797221
	.long	-1396440957
	.long	-682609371
	.long	-135634593
	.long	1645490516
	.long	223693667
	.long	1567101217
	.long	-932789415
	.long	1029951347
	.long	-824036160
	.long	-724009337
	.long	1550265121
	.long	119497089
	.long	972513919
	.long	907948164
	.long	-454338757
	.long	1613718692
	.long	-700789348
	.long	465323573
	.long	-1635712211
	.long	654439692
	.long	-1719371084
	.long	-1595678855
	.long	-1167264884
	.long	277098644
	.long	624404830
	.long	-194023426
	.long	-1577108705
	.long	546110314
	.long	-1891267468
	.long	-639589849
	.long	1321679412
	.long	-58175639
	.long	1045293279
	.long	-284295032
	.long	895050893
	.long	-1975175028
	.long	494945126
	.long	1914543101
	.long	-1517910853
	.long	-400202957
	.long	-2075229678
	.long	311263384
	.long	-19710028
	.long	-836236575
	.long	669096869
	.long	-710491566
	.long	-459844419
	.long	-975809059
	.long	-345608092
	.long	2005142349
	.long	-1581864959
	.long	-2066012503
	.long	-524982508
	.long	569394103
	.long	-439330720
	.long	1425027204
	.long	108000370
	.long	-1558535853
	.long	-623098027
	.long	-1251844673
	.long	1750473702
	.long	-2083886188
	.long	762237499
	.long	-321977893
	.long	-1496067910
	.long	-1233109668
	.long	-1351112951
	.long	867476300
	.long	964413654
	.long	1591880597
	.long	1594774276
	.long	-2115145887
	.long	552026980
	.long	-1268903048
	.long	-568826981
	.long	-2011389662
	.long	-1184422191
	.long	-2142656536
	.long	582474363
	.long	1582640421
	.long	1383256631
	.long	2043843868
	.long	-972191412
	.long	1217180674
	.long	463797851
	.long	-1531928725
	.long	480777679
	.long	-1576259579
	.long	-2005803165
	.long	-1176621109
	.long	214354409
	.long	200212307
	.long	-484358889
	.long	-1269553099
	.long	-1620891332
	.long	-297670871
	.long	1847405948
	.long	1342460550
	.long	510035443
	.long	-214695482
	.long	815934613
	.long	833030224
	.long	1620250387
	.long	1945732119
	.long	-1591306151
	.long	-328967100
	.long	1388869545
	.long	-838913114
	.long	-1607788735
	.long	2092620194
	.long	562037615
	.long	1356438536
	.long	-885045151
	.long	-1033119899
	.long	1688467115
	.long	-2144065930
	.long	631725691
	.long	-454635012
	.long	549916902
	.long	-839862656
	.long	394546491
	.long	837744717
	.long	2114462948
	.long	751520235
	.long	-2073412690
	.long	-1879607160
	.long	-295870218
	.long	2063029875
	.long	803036379
	.long	-1592380991
	.long	821456707
	.long	-1275401132
	.long	360699898
	.long	-276465204
	.long	-783098280
	.long	-617611938
	.long	-1892495847
	.long	812317050
	.long	49299192
	.long	-1724802347
	.long	-1035798001
	.long	-1478235216
	.long	-963753722
	.long	-1193663732
	.long	-2138951640
	.long	-589368376
	.long	-748703375
	.long	143268808
	.long	-1094662816
	.long	1638124008
	.long	-1129777843
	.long	-953159686
	.long	578956953
	.long	-2100989772
	.long	-656847223
	.long	-1961085764
	.long	807278310
	.long	658237817
	.long	-1325405530
	.long	1641658566
	.long	11683945
	.long	-1207972289
	.long	148645947
	.long	1138423386
	.long	-136210536
	.long	1981396783
	.long	-1893950556
	.long	-595183712
	.long	380097457
	.long	-1614572617
	.long	-1491898645
	.long	-960707010
	.long	441530178
	.long	-278386500
	.long	1375954390
	.long	761952171
	.long	891809099
	.long	-2111843818
	.long	157052462
	.long	-611126533
	.long	1592404427
	.long	341349109
	.long	-1856483457
	.long	1417898363
	.long	644327628
	.long	-2061934520
	.long	-1941197590
	.long	-2093457196
	.long	220455161
	.long	1815641738
	.long	182899273
	.long	-1299947508
	.long	-667585763
	.long	-592329145
	.long	-1404283158
	.long	1052606899
	.long	588164016
	.long	1681439879
	.long	-256527878
	.long	-1889623373
	.long	-65518014
	.long	167996282
	.long	1336969661
	.long	1688053129
	.long	-1555742370
	.long	1543734051
	.long	1046297529
	.long	1138201970
	.long	2121126012
	.long	115334942
	.long	1819067631
	.long	1902159161
	.long	1941945968
	.long	-2088274427
	.long	1159982321
	.globl	CAST_S_table0
	.align 32
	.type	CAST_S_table0, @object
	.size	CAST_S_table0, 1024
CAST_S_table0:
	.long	821772500
	.long	-1616838901
	.long	1810681135
	.long	1059425402
	.long	505495343
	.long	-1677701677
	.long	1610868032
	.long	-811611831
	.long	-1076580569
	.long	-2000962123
	.long	-503103344
	.long	-1731160459
	.long	1852023008
	.long	365126098
	.long	-1025022435
	.long	584384398
	.long	677919599
	.long	-1065365415
	.long	-14452280
	.long	2002735330
	.long	1136869587
	.long	-550533546
	.long	-2005097446
	.long	-1563247315
	.long	-1580605226
	.long	879511577
	.long	1639411079
	.long	575934255
	.long	717107937
	.long	-1437329813
	.long	576097850
	.long	-1563213360
	.long	1725645000
	.long	-1484506833
	.long	5111599
	.long	767152862
	.long	-1751892052
	.long	1251459544
	.long	1383482551
	.long	-1242286169
	.long	-1205028113
	.long	-682503847
	.long	1878520045
	.long	1510570527
	.long	-2105841456
	.long	-1863518930
	.long	582008916
	.long	-1131521739
	.long	1265446783
	.long	1354458274
	.long	-765048560
	.long	-1092255443
	.long	-1221385584
	.long	-382003809
	.long	-1265703919
	.long	1275016285
	.long	-45759936
	.long	-1389258945
	.long	-990457810
	.long	1442611557
	.long	-709768531
	.long	-1582551634
	.long	-1563117715
	.long	-1046803376
	.long	-2011021070
	.long	208555832
	.long	-1528512553
	.long	1331405426
	.long	1447828783
	.long	-979610855
	.long	-1186340012
	.long	-1337562626
	.long	-1313428598
	.long	-955033379
	.long	1669711173
	.long	286233437
	.long	1465092821
	.long	1782121619
	.long	-432195616
	.long	710211251
	.long	980974943
	.long	1651941557
	.long	430374111
	.long	2051154026
	.long	704238805
	.long	-165996399
	.long	-1150146722
	.long	-1437564569
	.long	948965521
	.long	-961214997
	.long	-2067281012
	.long	718756367
	.long	-2025188313
	.long	-1563323541
	.long	718440111
	.long	-1437150575
	.long	-678870176
	.long	1113355533
	.long	-1816945114
	.long	410092745
	.long	1811985197
	.long	1944238868
	.long	-1598112708
	.long	1415722873
	.long	1682284203
	.long	1060277122
	.long	1998114690
	.long	1503841958
	.long	82706478
	.long	-1979811610
	.long	1068173648
	.long	845149890
	.long	-2127020283
	.long	1768146376
	.long	1993038550
	.long	-728140599
	.long	-904393265
	.long	940016341
	.long	-939893514
	.long	-1966926575
	.long	904371731
	.long	1205506512
	.long	-200306554
	.long	-1478344290
	.long	825647681
	.long	85914773
	.long	-1437123836
	.long	1249926541
	.long	1417871568
	.long	3287612
	.long	-1083912737
	.long	-1168660850
	.long	1975924523
	.long	1353700161
	.long	-1480510859
	.long	-1856369675
	.long	1800716203
	.long	722146342
	.long	-1421030953
	.long	1151126914
	.long	-134483355
	.long	-1417296397
	.long	458611604
	.long	-1428888796
	.long	-811287233
	.long	770352098
	.long	-1642050302
	.long	-927128148
	.long	-354462285
	.long	-708993384
	.long	-485346894
	.long	718646636
	.long	-1790760482
	.long	-1380039384
	.long	-663679127
	.long	-1437480689
	.long	-1434948618
	.long	575749918
	.long	-1437489253
	.long	718488780
	.long	2069512688
	.long	-746783827
	.long	453416197
	.long	1106044049
	.long	-1262275866
	.long	52586708
	.long	-916452660
	.long	-835158419
	.long	-1083461268
	.long	1785789304
	.long	218356169
	.long	-723568162
	.long	-535796774
	.long	1194783844
	.long	1523787992
	.long	-1287140202
	.long	1975193539
	.long	-1739514885
	.long	1341901877
	.long	-1249128598
	.long	-518059332
	.long	-1077543350
	.long	-1492456432
	.long	-1405528310
	.long	1057244207
	.long	1636348243
	.long	-533104082
	.long	1462225785
	.long	-1662303857
	.long	481089165
	.long	718503062
	.long	24497053
	.long	-962724087
	.long	-950311440
	.long	-639942440
	.long	-334596231
	.long	1195698900
	.long	-1323552140
	.long	-584791138
	.long	2115785917
	.long	-267303687
	.long	-769388879
	.long	-1770671107
	.long	-1548994731
	.long	-730060881
	.long	1372086093
	.long	1452307862
	.long	-1514465818
	.long	1476592880
	.long	-905696015
	.long	18495466
	.long	-1916818725
	.long	901398090
	.long	891748256
	.long	-1015329527
	.long	-1137676583
	.long	-1734007194
	.long	1447622437
	.long	-10594659
	.long	216884176
	.long	2086908623
	.long	1879786977
	.long	-706064143
	.long	-2052511630
	.long	-1356874329
	.long	-735885200
	.long	-1484321805
	.long	758861177
	.long	1121993112
	.long	215018983
	.long	642190776
	.long	-125730484
	.long	1196255959
	.long	2081185372
	.long	-786228903
	.long	941322904
	.long	-170724133
	.long	-1417443757
	.long	1848581667
	.long	-2089706338
	.long	-1114513338
	.long	-1705622162
	.long	-600236020
	.long	550028657
	.long	-1775511012
	.long	-504981761
	.long	-1321096440
	.long	2093648313
	.long	443148163
	.long	46942275
	.long	-1560820359
	.long	1117713533
	.long	1115362972
	.long	1523183689
	.long	-577827072
	.long	1551984063
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	16
	.long	16
	.long	16
	.long	16
	.align 16
.LC1:
	.long	31
	.long	31
	.long	31
	.long	31
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
