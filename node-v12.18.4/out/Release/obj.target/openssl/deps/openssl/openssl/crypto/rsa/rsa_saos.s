	.file	"rsa_saos.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/rsa/rsa_saos.c"
	.text
	.p2align 4
	.globl	RSA_sign_ASN1_OCTET_STRING
	.type	RSA_sign_ASN1_OCTET_STRING, @function
RSA_sign_ASN1_OCTET_STRING:
.LFB779:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	movq	%r14, %rdi
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -104(%rbp)
	movq	%r8, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -72(%rbp)
	xorl	%esi, %esi
	movl	%edx, -80(%rbp)
	movl	$4, -76(%rbp)
	call	i2d_ASN1_OCTET_STRING@PLT
	movq	%r13, %rdi
	movl	%eax, %r12d
	call	RSA_size@PLT
	leal	-10(%rax), %edx
	cmpl	%r12d, %edx
	jle	.L10
	leal	1(%rax), %ebx
	movl	$37, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rbx, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L11
	leaq	-88(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, -88(%rbp)
	call	i2d_ASN1_OCTET_STRING@PLT
	movq	-104(%rbp), %rdx
	movl	%r12d, %edi
	movq	%r13, %rcx
	movl	$1, %r8d
	movq	%r15, %rsi
	xorl	%r12d, %r12d
	call	RSA_private_encrypt@PLT
	testl	%eax, %eax
	jle	.L5
	movq	-112(%rbp), %rcx
	movl	$1, %r12d
	movl	%eax, (%rcx)
.L5:
	movl	$50, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	CRYPTO_clear_free@PLT
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L12
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movl	$33, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$112, %edx
	xorl	%r12d, %r12d
	movl	$118, %esi
	movl	$4, %edi
	call	ERR_put_error@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L11:
	movl	$39, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$118, %esi
	movl	$4, %edi
	call	ERR_put_error@PLT
	jmp	.L1
.L12:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE779:
	.size	RSA_sign_ASN1_OCTET_STRING, .-RSA_sign_ASN1_OCTET_STRING
	.p2align 4
	.globl	RSA_verify_ASN1_OCTET_STRING
	.type	RSA_verify_ASN1_OCTET_STRING, @function
RSA_verify_ASN1_OCTET_STRING:
.LFB780:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r9, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$40, %rsp
	movq	%rsi, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	RSA_size@PLT
	cmpl	%r13d, %eax
	je	.L14
	movl	$65, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$119, %edx
	xorl	%r12d, %r12d
	movl	$120, %esi
	movl	$4, %edi
	call	ERR_put_error@PLT
.L13:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L28
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	movl	%eax, %r15d
	movl	$70, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, -80(%rbp)
	movq	%r15, %rdi
	call	CRYPTO_malloc@PLT
	movl	-80(%rbp), %r10d
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L29
	movl	$1, %r8d
	movq	%r14, %rcx
	movq	%rax, %rdx
	movq	%r12, %rsi
	movl	%r10d, %edi
	call	RSA_public_decrypt@PLT
	testl	%eax, %eax
	jle	.L19
	movslq	%eax, %rdx
	leaq	-64(%rbp), %rsi
	xorl	%edi, %edi
	movq	%r13, -64(%rbp)
	call	d2i_ASN1_OCTET_STRING@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L19
	cmpl	%ebx, (%rax)
	jne	.L20
	movq	8(%rax), %rsi
	movq	-72(%rbp), %rdi
	movl	%ebx, %edx
	movq	%rax, -80(%rbp)
	movl	$1, %r12d
	call	memcmp@PLT
	movq	-80(%rbp), %r9
	testl	%eax, %eax
	je	.L17
.L20:
	movl	$87, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$104, %edx
	movl	$120, %esi
	movl	$4, %edi
	movq	%r9, -72(%rbp)
	xorl	%r12d, %r12d
	call	ERR_put_error@PLT
	movq	-72(%rbp), %r9
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L19:
	xorl	%r9d, %r9d
	xorl	%r12d, %r12d
.L17:
	movq	%r9, %rdi
	call	ASN1_OCTET_STRING_free@PLT
	movl	$93, %ecx
	movq	%r15, %rsi
	movq	%r13, %rdi
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L29:
	movl	$72, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$120, %esi
	movl	$4, %edi
	call	ERR_put_error@PLT
	xorl	%r9d, %r9d
	jmp	.L17
.L28:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE780:
	.size	RSA_verify_ASN1_OCTET_STRING, .-RSA_verify_ASN1_OCTET_STRING
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
