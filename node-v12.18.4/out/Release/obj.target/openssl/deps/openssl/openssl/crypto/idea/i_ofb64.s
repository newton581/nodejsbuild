	.file	"i_ofb64.c"
	.text
	.p2align 4
	.globl	IDEA_ofb64_encrypt
	.type	IDEA_ofb64_encrypt, @function
IDEA_ofb64_encrypt:
.LFB0:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movl	4(%r8), %esi
	movl	(%r9), %eax
	movq	%r9, -104(%rbp)
	bswap	%esi
	movd	%esi, %xmm1
	bswap	%esi
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	movl	(%r8), %edi
	movl	%esi, -60(%rbp)
	bswap	%edi
	movd	%edi, %xmm0
	bswap	%edi
	movl	%edi, -64(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	testq	%rdx, %rdx
	je	.L2
	leaq	-80(%rbp), %rdi
	movq	%r8, %r13
	leaq	(%rbx,%rdx), %r15
	xorl	%r14d, %r14d
	movq	%rdi, -96(%rbp)
	.p2align 4,,10
	.p2align 3
.L5:
	testl	%eax, %eax
	jne	.L3
	movq	-96(%rbp), %rdi
	movq	%rcx, %rsi
	movq	%rcx, -88(%rbp)
	addq	$1, %rbx
	addl	$1, %r14d
	addq	$1, %r12
	call	IDEA_encrypt@PLT
	movl	-80(%rbp), %esi
	movl	-72(%rbp), %edx
	movl	%esi, %eax
	movl	%edx, %edi
	bswap	%eax
	movl	%eax, -64(%rbp)
	xorb	-1(%rbx), %al
	bswap	%edi
	movb	%al, -1(%r12)
	movl	$1, %eax
	movl	%edi, -60(%rbp)
	cmpq	%r15, %rbx
	je	.L4
	movq	-88(%rbp), %rcx
.L3:
	movslq	%eax, %rsi
	movzbl	(%rbx), %edx
	addq	$1, %rbx
	addq	$1, %r12
	xorb	-64(%rbp,%rsi), %dl
	addl	$1, %eax
	movb	%dl, -1(%r12)
	andl	$7, %eax
	cmpq	%r15, %rbx
	jne	.L5
	testl	%r14d, %r14d
	jne	.L17
.L2:
	movq	-104(%rbp), %rcx
	movl	%eax, (%rcx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L18
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movl	-80(%rbp), %esi
	movl	-72(%rbp), %edx
.L4:
	bswap	%esi
	bswap	%edx
	movl	%esi, 0(%r13)
	movl	%edx, 4(%r13)
	jmp	.L2
.L18:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE0:
	.size	IDEA_ofb64_encrypt, .-IDEA_ofb64_encrypt
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
