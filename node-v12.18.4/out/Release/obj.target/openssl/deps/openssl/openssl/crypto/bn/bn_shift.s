	.file	"bn_shift.c"
	.text
	.p2align 4
	.globl	BN_lshift1
	.type	BN_lshift1, @function
BN_lshift1:
.LFB252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	8(%rsi), %eax
	movq	%rdi, %rbx
	leal	1(%rax), %esi
	cmpq	%r12, %rdi
	je	.L2
	movl	16(%r12), %eax
	movl	%eax, 16(%rdi)
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L6
	movl	8(%r12), %r10d
	movl	%r10d, 8(%rbx)
.L5:
	movl	8(%r12), %eax
	movq	(%r12), %r9
	movq	(%rbx), %r8
	testl	%eax, %eax
	jle	.L9
	leal	-1(%rax), %edi
	xorl	%esi, %esi
	xorl	%eax, %eax
	leaq	1(%rdi), %r11
	.p2align 4,,10
	.p2align 3
.L8:
	movq	(%r9,%rax,8), %rcx
	leaq	(%rcx,%rcx), %rdx
	shrq	$63, %rcx
	orq	%rsi, %rdx
	movq	%rcx, %rsi
	movq	%rdx, (%r8,%rax,8)
	movq	%rax, %rdx
	addq	$1, %rax
	cmpq	%rdx, %rdi
	jne	.L8
	leaq	(%r8,%r11,8), %r8
	movl	%ecx, %eax
.L7:
	addl	%eax, %r10d
	movq	%rsi, (%r8)
	movl	$1, %eax
	movl	%r10d, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L6
	movl	8(%rbx), %r10d
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L6:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	xorl	%eax, %eax
	xorl	%esi, %esi
	jmp	.L7
	.cfi_endproc
.LFE252:
	.size	BN_lshift1, .-BN_lshift1
	.p2align 4
	.globl	BN_rshift1
	.type	BN_rshift1, @function
BN_rshift1:
.LFB253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L28
	movl	8(%r12), %r15d
	movq	(%r12), %rbx
	cmpq	%r13, %r12
	je	.L19
	movl	%r15d, %esi
	movq	%r13, %rdi
	movl	%eax, %r14d
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L16
	movl	16(%r12), %eax
	movl	%eax, 16(%r13)
.L19:
	leal	-1(%r15), %ecx
	movq	0(%r13), %rdi
	movslq	%ecx, %rax
	movq	(%rbx,%rax,8), %rdx
	movq	%rdx, %rsi
	shrq	%rsi
	movq	%rsi, (%rdi,%rax,8)
	movq	%rdx, %rsi
	salq	$63, %rsi
	cmpq	$1, %rdx
	sete	%dl
	movzbl	%dl, %edx
	subl	%edx, %r15d
	movl	%r15d, 8(%r13)
	testl	%ecx, %ecx
	jle	.L20
	.p2align 4,,10
	.p2align 3
.L21:
	movq	-8(%rbx,%rax,8), %rcx
	movq	%rcx, %rdx
	shrq	%rdx
	orq	%rsi, %rdx
	movq	%rcx, %rsi
	movq	%rdx, -8(%rdi,%rax,8)
	subq	$1, %rax
	salq	$63, %rsi
	testl	%eax, %eax
	jg	.L21
.L20:
	movl	$1, %r14d
	testl	%r15d, %r15d
	jne	.L16
	movl	$0, 16(%r13)
.L16:
	addq	$8, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r13, %rdi
	movl	$1, %r14d
	call	BN_set_word@PLT
	jmp	.L16
	.cfi_endproc
.LFE253:
	.size	BN_rshift1, .-BN_rshift1
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/bn/bn_shift.c"
	.text
	.p2align 4
	.globl	BN_lshift
	.type	BN_lshift, @function
BN_lshift:
.LFB254:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testl	%edx, %edx
	js	.L44
	movq	%rsi, %rbx
	movl	%edx, %r13d
	movl	8(%rsi), %esi
	movq	%rdi, %r12
	sarl	$6, %r13d
	movl	%edx, %r14d
	addl	%r13d, %esi
	addl	$1, %esi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L38
	movq	(%r12), %r11
	movslq	%r13d, %r8
	movl	8(%rbx), %edi
	salq	$3, %r8
	leaq	(%r11,%r8), %r9
	testl	%edi, %edi
	jne	.L45
	movq	$0, (%r9)
	testl	%r13d, %r13d
	jne	.L46
.L37:
	movl	16(%rbx), %eax
	movl	%eax, 16(%r12)
	leal	1(%r13,%rdi), %eax
	movl	$1, %r13d
	movl	%eax, 8(%r12)
.L32:
	movq	%r12, %rdi
	call	bn_correct_top@PLT
.L29:
	addq	$40, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	movl	%r14d, %edx
	andl	$63, %edx
	movl	%edx, %r10d
	negl	%r10d
	andl	$63, %r10d
	movl	%r10d, %eax
	negq	%rax
	movq	%rax, %rcx
	shrq	$8, %rcx
	orq	%rcx, %rax
	movq	(%rbx), %rcx
	movq	%rax, %r14
	movq	%rax, -56(%rbp)
	movslq	%edi, %rax
	movq	-8(%rcx,%rax,8), %rsi
	movq	%rcx, -64(%rbp)
	movl	%r10d, %ecx
	movq	%rsi, %r15
	shrq	%cl, %r15
	movq	%r15, %rcx
	andq	%r14, %rcx
	movq	%rcx, (%r9,%rax,8)
	leal	-1(%rdi), %eax
	testl	%eax, %eax
	jle	.L34
	movl	%edi, -68(%rbp)
	movq	-64(%rbp), %r15
	cltq
	.p2align 4,,10
	.p2align 3
.L35:
	movl	%edx, %ecx
	salq	%cl, %rsi
	movl	%r10d, %ecx
	movq	%rsi, %r14
	movq	-8(%r15,%rax,8), %rsi
	movq	%rsi, %rdi
	shrq	%cl, %rdi
	movq	-56(%rbp), %rcx
	andq	%rdi, %rcx
	orq	%rcx, %r14
	movq	%r14, (%r9,%rax,8)
	subq	$1, %rax
	testl	%eax, %eax
	jg	.L35
	movl	-68(%rbp), %edi
.L34:
	movl	%edx, %ecx
	salq	%cl, %rsi
	movq	%rsi, (%r9)
	testl	%r13d, %r13d
	je	.L37
.L46:
	movq	%r11, %rdi
	movq	%r8, %rdx
	xorl	%esi, %esi
	call	memset@PLT
	movl	8(%rbx), %edi
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L44:
	movl	$86, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$119, %edx
	xorl	%r13d, %r13d
	movl	$145, %esi
	movl	$3, %edi
	call	ERR_put_error@PLT
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L38:
	xorl	%r13d, %r13d
	jmp	.L32
	.cfi_endproc
.LFE254:
	.size	BN_lshift, .-BN_lshift
	.p2align 4
	.globl	bn_lshift_fixed_top
	.type	bn_lshift_fixed_top, @function
bn_lshift_fixed_top:
.LFB255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leal	63(%rdx), %ebx
	subq	$24, %rsp
	testl	%edx, %edx
	movl	8(%rsi), %esi
	cmovns	%edx, %ebx
	sarl	$6, %ebx
	addl	%ebx, %esi
	addl	$1, %esi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L54
	movq	(%r12), %r11
	movslq	%ebx, %rdx
	movl	8(%r13), %r15d
	leaq	0(,%rdx,8), %rax
	movq	%rax, -56(%rbp)
	leaq	(%r11,%rax), %r8
	testl	%r15d, %r15d
	jne	.L60
	movq	$0, (%r8)
	testl	%ebx, %ebx
	jne	.L61
.L53:
	movl	16(%r13), %eax
	movl	%eax, 16(%r12)
	leal	1(%r15,%rbx), %eax
	movl	%eax, 8(%r12)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	movl	%r14d, %edx
	movq	0(%r13), %r14
	andl	$63, %edx
	movl	%edx, %r9d
	negl	%r9d
	andl	$63, %r9d
	movl	%r9d, %r10d
	movl	%r9d, %ecx
	negq	%r10
	movq	%r10, %rax
	shrq	$8, %rax
	orq	%rax, %r10
	movslq	%r15d, %rax
	movq	-8(%r14,%rax,8), %rsi
	movq	%rsi, %rdi
	shrq	%cl, %rdi
	movq	%rdi, %rcx
	andq	%r10, %rcx
	movq	%rcx, (%r8,%rax,8)
	leal	-1(%r15), %eax
	testl	%eax, %eax
	jle	.L50
	movq	%r11, -64(%rbp)
	cltq
	.p2align 4,,10
	.p2align 3
.L51:
	movq	%rsi, %rdi
	movq	-8(%r14,%rax,8), %rsi
	movl	%edx, %ecx
	salq	%cl, %rdi
	movl	%r9d, %ecx
	movq	%rsi, %r11
	shrq	%cl, %r11
	movq	%r11, %rcx
	andq	%r10, %rcx
	orq	%rdi, %rcx
	movq	%rcx, (%r8,%rax,8)
	subq	$1, %rax
	testl	%eax, %eax
	jg	.L51
	movq	-64(%rbp), %r11
.L50:
	movl	%edx, %ecx
	salq	%cl, %rsi
	movq	%rsi, (%r8)
	testl	%ebx, %ebx
	je	.L53
.L61:
	movq	-56(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r11, %rdi
	call	memset@PLT
	movl	8(%r13), %r15d
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L54:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE255:
	.size	bn_lshift_fixed_top, .-bn_lshift_fixed_top
	.p2align 4
	.globl	bn_rshift_fixed_top
	.type	bn_rshift_fixed_top, @function
bn_rshift_fixed_top:
.LFB257:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	63(%rdx), %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testl	%edx, %edx
	movl	8(%rsi), %r9d
	cmovns	%edx, %ecx
	sarl	$6, %ecx
	cmpl	%ecx, %r9d
	jle	.L73
	movl	%edx, %ebx
	movq	%rsi, %r14
	subl	%ecx, %r9d
	andl	$63, %ebx
	movl	%ebx, %r12d
	negl	%r12d
	andl	$63, %r12d
	movl	%r12d, %r13d
	negq	%r13
	movq	%r13, %rax
	shrq	$8, %rax
	orq	%rax, %r13
	cmpq	%rdi, %rsi
	je	.L68
	movl	%r9d, %esi
	movl	%ecx, -56(%rbp)
	movl	%r9d, -52(%rbp)
	call	bn_wexpand@PLT
	movl	-52(%rbp), %r9d
	movl	-56(%rbp), %ecx
	testq	%rax, %rax
	je	.L74
.L68:
	movq	(%r14), %rax
	movslq	%ecx, %rcx
	movq	(%r15), %rdx
	leaq	(%rax,%rcx,8), %r10
	movq	(%r10), %r8
	cmpl	$1, %r9d
	jle	.L67
	leal	-2(%r9), %edi
	movl	$1, %esi
	addq	$2, %rdi
	.p2align 4,,10
	.p2align 3
.L69:
	movq	%r8, %r11
	movq	(%r10,%rsi,8), %r8
	movl	%r12d, %ecx
	movq	%r8, %rax
	salq	%cl, %rax
	movl	%ebx, %ecx
	andq	%r13, %rax
	shrq	%cl, %r11
	orq	%r11, %rax
	movq	%rax, -8(%rdx,%rsi,8)
	addq	$1, %rsi
	cmpq	%rsi, %rdi
	jne	.L69
	leal	-1(%r9), %eax
	cltq
	leaq	(%rdx,%rax,8), %rdx
.L67:
	movl	%ebx, %ecx
	movl	16(%r14), %eax
	shrq	%cl, %r8
	movq	%r8, (%rdx)
	movl	%r9d, 8(%r15)
	movl	%eax, 16(%r15)
	movl	$1, %eax
.L62:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	xorl	%esi, %esi
	call	BN_set_word@PLT
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L62
	.cfi_endproc
.LFE257:
	.size	bn_rshift_fixed_top, .-bn_rshift_fixed_top
	.p2align 4
	.globl	BN_rshift
	.type	BN_rshift, @function
BN_rshift:
.LFB256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	testl	%edx, %edx
	js	.L79
	movq	%rdi, %r12
	call	bn_rshift_fixed_top
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	bn_correct_top@PLT
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	xorl	%r13d, %r13d
	movl	$155, %r8d
	movl	$119, %edx
	movl	$146, %esi
	leaq	.LC0(%rip), %rcx
	movl	$3, %edi
	call	ERR_put_error@PLT
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE256:
	.size	BN_rshift, .-BN_rshift
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
