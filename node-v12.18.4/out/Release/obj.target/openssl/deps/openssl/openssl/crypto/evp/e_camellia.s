	.file	"e_camellia.c"
	.text
	.p2align 4
	.type	camellia_cbc_cipher, @function
camellia_cbc_cipher:
.LFB409:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	288(%rax), %r10
	movq	%rax, %r13
	testq	%r10, %r10
	movq	%r10, -56(%rbp)
	je	.L2
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r12, %rdi
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movl	-60(%rbp), %r9d
	movq	-56(%rbp), %r10
	movq	%r13, %rcx
	movq	%rax, %r8
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	*%r10
.L3:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	280(%r13), %r9
	movq	%r12, %rdi
	movq	%r9, -56(%rbp)
	testl	%eax, %eax
	je	.L4
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	-56(%rbp), %r9
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%rax, %r8
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	CRYPTO_cbc128_encrypt@PLT
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L4:
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	-56(%rbp), %r9
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%rax, %r8
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	CRYPTO_cbc128_decrypt@PLT
	jmp	.L3
	.cfi_endproc
.LFE409:
	.size	camellia_cbc_cipher, .-camellia_cbc_cipher
	.p2align 4
	.type	camellia_ecb_cipher, @function
camellia_ecb_cipher:
.LFB410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -56(%rbp)
	call	EVP_CIPHER_CTX_block_size@PLT
	movq	%r15, %rdi
	movslq	%eax, %r12
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %r15
	movq	%rcx, %rax
	subq	%r12, %rax
	movq	%rax, -56(%rbp)
	cmpq	%rcx, %r12
	ja	.L13
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L9:
	leaq	(%r14,%rbx), %rsi
	leaq	0(%r13,%rbx), %rdi
	movq	%r15, %rdx
	addq	%r12, %rbx
	call	*280(%r15)
	cmpq	%rbx, -56(%rbp)
	jnb	.L9
.L13:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE410:
	.size	camellia_ecb_cipher, .-camellia_ecb_cipher
	.p2align 4
	.type	camellia_ofb_cipher, @function
camellia_ofb_cipher:
.LFB411:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	EVP_CIPHER_CTX_num@PLT
	movq	280(%r13), %rdx
	movq	%r12, %rdi
	movl	%eax, -60(%rbp)
	movq	%rdx, -72(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	-72(%rbp), %rdx
	subq	$8, %rsp
	movq	%r13, %rcx
	movq	%rax, %r8
	leaq	-60(%rbp), %r9
	movq	%r15, %rsi
	movq	%r14, %rdi
	pushq	%rdx
	movq	%rbx, %rdx
	call	CRYPTO_ofb128_encrypt@PLT
	movl	-60(%rbp), %esi
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L18
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L18:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE411:
	.size	camellia_ofb_cipher, .-camellia_ofb_cipher
	.p2align 4
	.type	camellia_cfb_cipher, @function
camellia_cfb_cipher:
.LFB412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	EVP_CIPHER_CTX_num@PLT
	movq	280(%r13), %rcx
	movq	%r12, %rdi
	movl	%eax, -60(%rbp)
	movq	%rcx, -80(%rbp)
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r12, %rdi
	movl	%eax, -68(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	-80(%rbp), %rcx
	movl	-68(%rbp), %edx
	leaq	-60(%rbp), %r9
	movq	%rax, %r8
	movq	%r15, %rsi
	movq	%r14, %rdi
	pushq	%rcx
	movq	%r13, %rcx
	pushq	%rdx
	movq	%rbx, %rdx
	call	CRYPTO_cfb128_encrypt@PLT
	movl	-60(%rbp), %esi
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L22
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L22:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE412:
	.size	camellia_cfb_cipher, .-camellia_cfb_cipher
	.p2align 4
	.type	camellia_cfb1_cipher, @function
camellia_cfb1_cipher:
.LFB414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$72, %rsp
	movq	%rdx, -104(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movl	$8192, %esi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	EVP_CIPHER_CTX_test_flags@PLT
	testl	%eax, %eax
	jne	.L24
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rbx
	jbe	.L36
	movabsq	$-1152921504606846976, %rax
	addq	-96(%rbp), %rax
	movq	-104(%rbp), %rbx
	movq	%r15, %r13
	shrq	$60, %rax
	addq	$1, %rax
	salq	$60, %rax
	movq	%rax, -112(%rbp)
	addq	%r15, %rax
	movq	%rax, -80(%rbp)
	leaq	-60(%rbp), %rax
	movq	%rax, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L28:
	movq	%r14, %rdi
	call	EVP_CIPHER_CTX_num@PLT
	movq	280(%r12), %rcx
	movq	%r14, %rdi
	movl	%eax, -60(%rbp)
	movq	%rcx, -72(%rbp)
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r14, %rdi
	movl	%eax, %r15d
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	-72(%rbp), %rcx
	movq	-88(%rbp), %r9
	movq	%r13, %rsi
	movabsq	$-9223372036854775808, %rdx
	movq	%rax, %r8
	movq	%rbx, %rdi
	pushq	%rcx
	movq	%r12, %rcx
	pushq	%r15
	call	CRYPTO_cfb128_1_encrypt@PLT
	movl	-60(%rbp), %esi
	movq	%r14, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	popq	%rcx
	popq	%rsi
	movabsq	$1152921504606846976, %rax
	addq	%rax, %r13
	addq	%rax, %rbx
	cmpq	-80(%rbp), %r13
	jne	.L28
	movq	-112(%rbp), %rdx
	subq	$1, %rax
	addq	%rdx, -104(%rbp)
	andq	%rax, -96(%rbp)
.L26:
	cmpq	$0, -96(%rbp)
	jne	.L37
.L27:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L38
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	movq	%r14, %rdi
	call	EVP_CIPHER_CTX_num@PLT
	movq	%r14, %rdi
	movq	280(%r12), %r13
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r14, %rdi
	movl	%eax, %ebx
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	pushq	%r13
	movq	-96(%rbp), %rdx
	leaq	-60(%rbp), %r9
	pushq	%rbx
	movq	-80(%rbp), %rsi
	movq	%rax, %r8
	movq	%r12, %rcx
	salq	$3, %rdx
.L35:
	movq	-104(%rbp), %rdi
	call	CRYPTO_cfb128_1_encrypt@PLT
	movl	-60(%rbp), %esi
	movq	%r14, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	popq	%rax
	popq	%rdx
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L24:
	movq	%r14, %rdi
	call	EVP_CIPHER_CTX_num@PLT
	movq	%r14, %rdi
	movq	280(%r12), %r13
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r14, %rdi
	movl	%eax, %ebx
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	pushq	%r13
	movq	-96(%rbp), %rdx
	leaq	-60(%rbp), %r9
	pushq	%rbx
	movq	%rax, %r8
	movq	%r12, %rcx
	movq	%r15, %rsi
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L36:
	movq	%r15, -80(%rbp)
	jmp	.L26
.L38:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE414:
	.size	camellia_cfb1_cipher, .-camellia_cfb1_cipher
	.p2align 4
	.type	camellia_cfb8_cipher, @function
camellia_cfb8_cipher:
.LFB413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	EVP_CIPHER_CTX_num@PLT
	movq	280(%r13), %rcx
	movq	%r12, %rdi
	movl	%eax, -60(%rbp)
	movq	%rcx, -80(%rbp)
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r12, %rdi
	movl	%eax, -68(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	-80(%rbp), %rcx
	movl	-68(%rbp), %edx
	leaq	-60(%rbp), %r9
	movq	%rax, %r8
	movq	%r15, %rsi
	movq	%r14, %rdi
	pushq	%rcx
	movq	%r13, %rcx
	pushq	%rdx
	movq	%rbx, %rdx
	call	CRYPTO_cfb128_8_encrypt@PLT
	movl	-60(%rbp), %esi
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L42
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L42:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE413:
	.size	camellia_cfb8_cipher, .-camellia_cfb8_cipher
	.p2align 4
	.type	camellia_ctr_cipher, @function
camellia_ctr_cipher:
.LFB415:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_CIPHER_CTX_num@PLT
	movq	%r12, %rdi
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	288(%rax), %rdx
	movq	%rax, %r13
	testq	%rdx, %rdx
	movq	%rdx, -72(%rbp)
	je	.L44
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_buf_noconst@PLT
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %r9
	movq	%r13, %rcx
	movq	%rax, %r8
	leaq	-60(%rbp), %rax
	movq	%r15, %rsi
	movq	%r14, %rdi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	%rax
	call	CRYPTO_ctr128_encrypt_ctr32@PLT
	popq	%rcx
	popq	%rsi
.L45:
	movl	-60(%rbp), %esi
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L48
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	movq	280(%rax), %rdx
	movq	%r12, %rdi
	movq	%rdx, -80(%rbp)
	call	EVP_CIPHER_CTX_buf_noconst@PLT
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	-80(%rbp), %rdx
	movq	-72(%rbp), %r9
	movq	%r13, %rcx
	movq	%rax, %r8
	leaq	-60(%rbp), %rax
	movq	%r15, %rsi
	movq	%r14, %rdi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	%rax
	call	CRYPTO_ctr128_encrypt@PLT
	popq	%rax
	popq	%rdx
	jmp	.L45
.L48:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE415:
	.size	camellia_ctr_cipher, .-camellia_ctr_cipher
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/evp/e_camellia.c"
	.text
	.p2align 4
	.type	camellia_init_key, @function
camellia_init_key:
.LFB408:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%rbx, %rdx
	movq	%r13, %rdi
	leal	0(,%rax,8), %esi
	call	Camellia_set_key@PLT
	testl	%eax, %eax
	js	.L65
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	leal	-1(%rax), %edx
	cmpl	$1, %edx
	ja	.L52
	testl	%r14d, %r14d
	je	.L66
.L52:
	movq	Camellia_encrypt@GOTPCREL(%rip), %rdx
	movq	%rdx, 280(%rbx)
	cmpl	$2, %eax
	je	.L56
.L64:
	xorl	%eax, %eax
.L54:
	movq	%rax, 288(%rbx)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movq	Camellia_decrypt@GOTPCREL(%rip), %rdx
	movq	%rdx, 280(%rbx)
	cmpl	$2, %eax
	jne	.L64
.L56:
	movq	Camellia_cbc_encrypt@GOTPCREL(%rip), %rax
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L65:
	movl	$221, %r8d
	movl	$157, %edx
	movl	$159, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE408:
	.size	camellia_init_key, .-camellia_init_key
	.p2align 4
	.globl	EVP_camellia_128_cbc
	.type	EVP_camellia_128_cbc, @function
EVP_camellia_128_cbc:
.LFB416:
	.cfi_startproc
	endbr64
	leaq	camellia_128_cbc(%rip), %rax
	ret
	.cfi_endproc
.LFE416:
	.size	EVP_camellia_128_cbc, .-EVP_camellia_128_cbc
	.p2align 4
	.globl	EVP_camellia_128_ecb
	.type	EVP_camellia_128_ecb, @function
EVP_camellia_128_ecb:
.LFB417:
	.cfi_startproc
	endbr64
	leaq	camellia_128_ecb(%rip), %rax
	ret
	.cfi_endproc
.LFE417:
	.size	EVP_camellia_128_ecb, .-EVP_camellia_128_ecb
	.p2align 4
	.globl	EVP_camellia_128_ofb
	.type	EVP_camellia_128_ofb, @function
EVP_camellia_128_ofb:
.LFB418:
	.cfi_startproc
	endbr64
	leaq	camellia_128_ofb(%rip), %rax
	ret
	.cfi_endproc
.LFE418:
	.size	EVP_camellia_128_ofb, .-EVP_camellia_128_ofb
	.p2align 4
	.globl	EVP_camellia_128_cfb128
	.type	EVP_camellia_128_cfb128, @function
EVP_camellia_128_cfb128:
.LFB419:
	.cfi_startproc
	endbr64
	leaq	camellia_128_cfb(%rip), %rax
	ret
	.cfi_endproc
.LFE419:
	.size	EVP_camellia_128_cfb128, .-EVP_camellia_128_cfb128
	.p2align 4
	.globl	EVP_camellia_128_cfb1
	.type	EVP_camellia_128_cfb1, @function
EVP_camellia_128_cfb1:
.LFB420:
	.cfi_startproc
	endbr64
	leaq	camellia_128_cfb1(%rip), %rax
	ret
	.cfi_endproc
.LFE420:
	.size	EVP_camellia_128_cfb1, .-EVP_camellia_128_cfb1
	.p2align 4
	.globl	EVP_camellia_128_cfb8
	.type	EVP_camellia_128_cfb8, @function
EVP_camellia_128_cfb8:
.LFB421:
	.cfi_startproc
	endbr64
	leaq	camellia_128_cfb8(%rip), %rax
	ret
	.cfi_endproc
.LFE421:
	.size	EVP_camellia_128_cfb8, .-EVP_camellia_128_cfb8
	.p2align 4
	.globl	EVP_camellia_128_ctr
	.type	EVP_camellia_128_ctr, @function
EVP_camellia_128_ctr:
.LFB422:
	.cfi_startproc
	endbr64
	leaq	camellia_128_ctr(%rip), %rax
	ret
	.cfi_endproc
.LFE422:
	.size	EVP_camellia_128_ctr, .-EVP_camellia_128_ctr
	.p2align 4
	.globl	EVP_camellia_192_cbc
	.type	EVP_camellia_192_cbc, @function
EVP_camellia_192_cbc:
.LFB423:
	.cfi_startproc
	endbr64
	leaq	camellia_192_cbc(%rip), %rax
	ret
	.cfi_endproc
.LFE423:
	.size	EVP_camellia_192_cbc, .-EVP_camellia_192_cbc
	.p2align 4
	.globl	EVP_camellia_192_ecb
	.type	EVP_camellia_192_ecb, @function
EVP_camellia_192_ecb:
.LFB424:
	.cfi_startproc
	endbr64
	leaq	camellia_192_ecb(%rip), %rax
	ret
	.cfi_endproc
.LFE424:
	.size	EVP_camellia_192_ecb, .-EVP_camellia_192_ecb
	.p2align 4
	.globl	EVP_camellia_192_ofb
	.type	EVP_camellia_192_ofb, @function
EVP_camellia_192_ofb:
.LFB425:
	.cfi_startproc
	endbr64
	leaq	camellia_192_ofb(%rip), %rax
	ret
	.cfi_endproc
.LFE425:
	.size	EVP_camellia_192_ofb, .-EVP_camellia_192_ofb
	.p2align 4
	.globl	EVP_camellia_192_cfb128
	.type	EVP_camellia_192_cfb128, @function
EVP_camellia_192_cfb128:
.LFB426:
	.cfi_startproc
	endbr64
	leaq	camellia_192_cfb(%rip), %rax
	ret
	.cfi_endproc
.LFE426:
	.size	EVP_camellia_192_cfb128, .-EVP_camellia_192_cfb128
	.p2align 4
	.globl	EVP_camellia_192_cfb1
	.type	EVP_camellia_192_cfb1, @function
EVP_camellia_192_cfb1:
.LFB427:
	.cfi_startproc
	endbr64
	leaq	camellia_192_cfb1(%rip), %rax
	ret
	.cfi_endproc
.LFE427:
	.size	EVP_camellia_192_cfb1, .-EVP_camellia_192_cfb1
	.p2align 4
	.globl	EVP_camellia_192_cfb8
	.type	EVP_camellia_192_cfb8, @function
EVP_camellia_192_cfb8:
.LFB428:
	.cfi_startproc
	endbr64
	leaq	camellia_192_cfb8(%rip), %rax
	ret
	.cfi_endproc
.LFE428:
	.size	EVP_camellia_192_cfb8, .-EVP_camellia_192_cfb8
	.p2align 4
	.globl	EVP_camellia_192_ctr
	.type	EVP_camellia_192_ctr, @function
EVP_camellia_192_ctr:
.LFB429:
	.cfi_startproc
	endbr64
	leaq	camellia_192_ctr(%rip), %rax
	ret
	.cfi_endproc
.LFE429:
	.size	EVP_camellia_192_ctr, .-EVP_camellia_192_ctr
	.p2align 4
	.globl	EVP_camellia_256_cbc
	.type	EVP_camellia_256_cbc, @function
EVP_camellia_256_cbc:
.LFB430:
	.cfi_startproc
	endbr64
	leaq	camellia_256_cbc(%rip), %rax
	ret
	.cfi_endproc
.LFE430:
	.size	EVP_camellia_256_cbc, .-EVP_camellia_256_cbc
	.p2align 4
	.globl	EVP_camellia_256_ecb
	.type	EVP_camellia_256_ecb, @function
EVP_camellia_256_ecb:
.LFB431:
	.cfi_startproc
	endbr64
	leaq	camellia_256_ecb(%rip), %rax
	ret
	.cfi_endproc
.LFE431:
	.size	EVP_camellia_256_ecb, .-EVP_camellia_256_ecb
	.p2align 4
	.globl	EVP_camellia_256_ofb
	.type	EVP_camellia_256_ofb, @function
EVP_camellia_256_ofb:
.LFB432:
	.cfi_startproc
	endbr64
	leaq	camellia_256_ofb(%rip), %rax
	ret
	.cfi_endproc
.LFE432:
	.size	EVP_camellia_256_ofb, .-EVP_camellia_256_ofb
	.p2align 4
	.globl	EVP_camellia_256_cfb128
	.type	EVP_camellia_256_cfb128, @function
EVP_camellia_256_cfb128:
.LFB433:
	.cfi_startproc
	endbr64
	leaq	camellia_256_cfb(%rip), %rax
	ret
	.cfi_endproc
.LFE433:
	.size	EVP_camellia_256_cfb128, .-EVP_camellia_256_cfb128
	.p2align 4
	.globl	EVP_camellia_256_cfb1
	.type	EVP_camellia_256_cfb1, @function
EVP_camellia_256_cfb1:
.LFB434:
	.cfi_startproc
	endbr64
	leaq	camellia_256_cfb1(%rip), %rax
	ret
	.cfi_endproc
.LFE434:
	.size	EVP_camellia_256_cfb1, .-EVP_camellia_256_cfb1
	.p2align 4
	.globl	EVP_camellia_256_cfb8
	.type	EVP_camellia_256_cfb8, @function
EVP_camellia_256_cfb8:
.LFB435:
	.cfi_startproc
	endbr64
	leaq	camellia_256_cfb8(%rip), %rax
	ret
	.cfi_endproc
.LFE435:
	.size	EVP_camellia_256_cfb8, .-EVP_camellia_256_cfb8
	.p2align 4
	.globl	EVP_camellia_256_ctr
	.type	EVP_camellia_256_ctr, @function
EVP_camellia_256_ctr:
.LFB436:
	.cfi_startproc
	endbr64
	leaq	camellia_256_ctr(%rip), %rax
	ret
	.cfi_endproc
.LFE436:
	.size	EVP_camellia_256_ctr, .-EVP_camellia_256_ctr
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	camellia_256_ctr, @object
	.size	camellia_256_ctr, 88
camellia_256_ctr:
	.long	971
	.long	1
	.long	32
	.long	16
	.quad	5
	.quad	camellia_init_key
	.quad	camellia_ctr_cipher
	.quad	0
	.long	296
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	camellia_256_cfb8, @object
	.size	camellia_256_cfb8, 88
camellia_256_cfb8:
	.long	765
	.long	1
	.long	32
	.long	16
	.quad	3
	.quad	camellia_init_key
	.quad	camellia_cfb8_cipher
	.quad	0
	.long	296
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	camellia_256_cfb1, @object
	.size	camellia_256_cfb1, 88
camellia_256_cfb1:
	.long	762
	.long	1
	.long	32
	.long	16
	.quad	3
	.quad	camellia_init_key
	.quad	camellia_cfb1_cipher
	.quad	0
	.long	296
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	camellia_256_cfb, @object
	.size	camellia_256_cfb, 88
camellia_256_cfb:
	.long	759
	.long	1
	.long	32
	.long	16
	.quad	4099
	.quad	camellia_init_key
	.quad	camellia_cfb_cipher
	.quad	0
	.long	296
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	camellia_256_ofb, @object
	.size	camellia_256_ofb, 88
camellia_256_ofb:
	.long	768
	.long	1
	.long	32
	.long	16
	.quad	4100
	.quad	camellia_init_key
	.quad	camellia_ofb_cipher
	.quad	0
	.long	296
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	camellia_256_ecb, @object
	.size	camellia_256_ecb, 88
camellia_256_ecb:
	.long	756
	.long	16
	.long	32
	.long	0
	.quad	4097
	.quad	camellia_init_key
	.quad	camellia_ecb_cipher
	.quad	0
	.long	296
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	camellia_256_cbc, @object
	.size	camellia_256_cbc, 88
camellia_256_cbc:
	.long	753
	.long	16
	.long	32
	.long	16
	.quad	4098
	.quad	camellia_init_key
	.quad	camellia_cbc_cipher
	.quad	0
	.long	296
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	camellia_192_ctr, @object
	.size	camellia_192_ctr, 88
camellia_192_ctr:
	.long	967
	.long	1
	.long	24
	.long	16
	.quad	5
	.quad	camellia_init_key
	.quad	camellia_ctr_cipher
	.quad	0
	.long	296
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	camellia_192_cfb8, @object
	.size	camellia_192_cfb8, 88
camellia_192_cfb8:
	.long	764
	.long	1
	.long	24
	.long	16
	.quad	3
	.quad	camellia_init_key
	.quad	camellia_cfb8_cipher
	.quad	0
	.long	296
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	camellia_192_cfb1, @object
	.size	camellia_192_cfb1, 88
camellia_192_cfb1:
	.long	761
	.long	1
	.long	24
	.long	16
	.quad	3
	.quad	camellia_init_key
	.quad	camellia_cfb1_cipher
	.quad	0
	.long	296
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	camellia_192_cfb, @object
	.size	camellia_192_cfb, 88
camellia_192_cfb:
	.long	758
	.long	1
	.long	24
	.long	16
	.quad	4099
	.quad	camellia_init_key
	.quad	camellia_cfb_cipher
	.quad	0
	.long	296
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	camellia_192_ofb, @object
	.size	camellia_192_ofb, 88
camellia_192_ofb:
	.long	767
	.long	1
	.long	24
	.long	16
	.quad	4100
	.quad	camellia_init_key
	.quad	camellia_ofb_cipher
	.quad	0
	.long	296
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	camellia_192_ecb, @object
	.size	camellia_192_ecb, 88
camellia_192_ecb:
	.long	755
	.long	16
	.long	24
	.long	0
	.quad	4097
	.quad	camellia_init_key
	.quad	camellia_ecb_cipher
	.quad	0
	.long	296
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	camellia_192_cbc, @object
	.size	camellia_192_cbc, 88
camellia_192_cbc:
	.long	752
	.long	16
	.long	24
	.long	16
	.quad	4098
	.quad	camellia_init_key
	.quad	camellia_cbc_cipher
	.quad	0
	.long	296
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	camellia_128_ctr, @object
	.size	camellia_128_ctr, 88
camellia_128_ctr:
	.long	963
	.long	1
	.long	16
	.long	16
	.quad	5
	.quad	camellia_init_key
	.quad	camellia_ctr_cipher
	.quad	0
	.long	296
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	camellia_128_cfb8, @object
	.size	camellia_128_cfb8, 88
camellia_128_cfb8:
	.long	763
	.long	1
	.long	16
	.long	16
	.quad	3
	.quad	camellia_init_key
	.quad	camellia_cfb8_cipher
	.quad	0
	.long	296
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	camellia_128_cfb1, @object
	.size	camellia_128_cfb1, 88
camellia_128_cfb1:
	.long	760
	.long	1
	.long	16
	.long	16
	.quad	3
	.quad	camellia_init_key
	.quad	camellia_cfb1_cipher
	.quad	0
	.long	296
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	camellia_128_cfb, @object
	.size	camellia_128_cfb, 88
camellia_128_cfb:
	.long	757
	.long	1
	.long	16
	.long	16
	.quad	4099
	.quad	camellia_init_key
	.quad	camellia_cfb_cipher
	.quad	0
	.long	296
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	camellia_128_ofb, @object
	.size	camellia_128_ofb, 88
camellia_128_ofb:
	.long	766
	.long	1
	.long	16
	.long	16
	.quad	4100
	.quad	camellia_init_key
	.quad	camellia_ofb_cipher
	.quad	0
	.long	296
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	camellia_128_ecb, @object
	.size	camellia_128_ecb, 88
camellia_128_ecb:
	.long	754
	.long	16
	.long	16
	.long	0
	.quad	4097
	.quad	camellia_init_key
	.quad	camellia_ecb_cipher
	.quad	0
	.long	296
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	camellia_128_cbc, @object
	.size	camellia_128_cbc, 88
camellia_128_cbc:
	.long	751
	.long	16
	.long	16
	.long	16
	.quad	4098
	.quad	camellia_init_key
	.quad	camellia_cbc_cipher
	.quad	0
	.long	296
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
