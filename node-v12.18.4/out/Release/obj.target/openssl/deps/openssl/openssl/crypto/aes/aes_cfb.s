	.file	"aes_cfb.c"
	.text
	.p2align 4
	.globl	AES_cfb128_encrypt
	.type	AES_cfb128_encrypt, @function
AES_cfb128_encrypt:
.LFB0:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	AES_encrypt@GOTPCREL(%rip)
	movl	16(%rbp), %eax
	pushq	%rax
	call	CRYPTO_cfb128_encrypt@PLT
	popq	%rax
	popq	%rdx
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	AES_cfb128_encrypt, .-AES_cfb128_encrypt
	.p2align 4
	.globl	AES_cfb1_encrypt
	.type	AES_cfb1_encrypt, @function
AES_cfb1_encrypt:
.LFB1:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	AES_encrypt@GOTPCREL(%rip)
	movl	16(%rbp), %eax
	pushq	%rax
	call	CRYPTO_cfb128_1_encrypt@PLT
	popq	%rax
	popq	%rdx
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1:
	.size	AES_cfb1_encrypt, .-AES_cfb1_encrypt
	.p2align 4
	.globl	AES_cfb8_encrypt
	.type	AES_cfb8_encrypt, @function
AES_cfb8_encrypt:
.LFB2:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	AES_encrypt@GOTPCREL(%rip)
	movl	16(%rbp), %eax
	pushq	%rax
	call	CRYPTO_cfb128_8_encrypt@PLT
	popq	%rax
	popq	%rdx
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2:
	.size	AES_cfb8_encrypt, .-AES_cfb8_encrypt
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
