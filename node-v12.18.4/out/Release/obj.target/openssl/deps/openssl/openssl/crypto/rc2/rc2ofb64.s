	.file	"rc2ofb64.c"
	.text
	.p2align 4
	.globl	RC2_ofb64_encrypt
	.type	RC2_ofb64_encrypt, @function
RC2_ofb64_encrypt:
.LFB0:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movd	(%r8), %xmm0
	movq	(%r8), %rsi
	movq	%r9, -104(%rbp)
	movd	4(%r8), %xmm1
	movl	(%r9), %eax
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	punpcklqdq	%xmm1, %xmm0
	movq	%rsi, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	testq	%rdx, %rdx
	je	.L2
	leaq	-80(%rbp), %rdi
	movq	%r8, %rbx
	leaq	0(%r13,%rdx), %r15
	xorl	%r12d, %r12d
	movq	%rdi, -96(%rbp)
	.p2align 4,,10
	.p2align 3
.L5:
	testl	%eax, %eax
	jne	.L3
	movq	-96(%rbp), %rdi
	movq	%rcx, %rsi
	movq	%rcx, -88(%rbp)
	addq	$1, %r13
	addl	$1, %r12d
	addq	$1, %r14
	call	RC2_encrypt@PLT
	movzbl	-1(%r13), %eax
	movl	-80(%rbp), %edx
	movl	-72(%rbp), %esi
	xorl	%edx, %eax
	movl	%edx, -64(%rbp)
	movb	%al, -1(%r14)
	movl	$1, %eax
	movl	%esi, -60(%rbp)
	cmpq	%r15, %r13
	je	.L4
	movq	-88(%rbp), %rcx
.L3:
	movslq	%eax, %rsi
	movzbl	0(%r13), %edx
	addq	$1, %r13
	addq	$1, %r14
	xorb	-64(%rbp,%rsi), %dl
	addl	$1, %eax
	movb	%dl, -1(%r14)
	andl	$7, %eax
	cmpq	%r15, %r13
	jne	.L5
	testl	%r12d, %r12d
	jne	.L17
.L2:
	movq	-104(%rbp), %rbx
	movl	%eax, (%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L18
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movl	-80(%rbp), %edx
	movl	-72(%rbp), %esi
.L4:
	movl	%edx, (%rbx)
	movl	%esi, 4(%rbx)
	jmp	.L2
.L18:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE0:
	.size	RC2_ofb64_encrypt, .-RC2_ofb64_encrypt
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
