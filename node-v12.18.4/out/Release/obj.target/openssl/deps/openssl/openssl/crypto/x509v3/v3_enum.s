	.file	"v3_enum.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/x509v3/v3_enum.c"
	.text
	.p2align 4
	.globl	i2s_ASN1_ENUMERATED_TABLE
	.type	i2s_ASN1_ENUMERATED_TABLE, @function
i2s_ASN1_ENUMERATED_TABLE:
.LFB1296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rsi, %rdi
	call	ASN1_ENUMERATED_get@PLT
	movq	96(%r12), %rdx
	movq	8(%rdx), %rdi
	testq	%rdi, %rdi
	jne	.L4
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L3:
	movq	32(%rdx), %rdi
	addq	$24, %rdx
	testq	%rdi, %rdi
	je	.L2
.L4:
	movslq	(%rdx), %rcx
	cmpq	%rax, %rcx
	jne	.L3
	popq	%r12
	movl	$50, %edx
	leaq	.LC0(%rip), %rsi
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_strdup@PLT
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	i2s_ASN1_ENUMERATED@PLT
	.cfi_endproc
.LFE1296:
	.size	i2s_ASN1_ENUMERATED_TABLE, .-i2s_ASN1_ENUMERATED_TABLE
	.globl	v3_crl_reason
	.section	.data.rel.ro,"aw"
	.align 32
	.type	v3_crl_reason, @object
	.size	v3_crl_reason, 104
v3_crl_reason:
	.long	141
	.long	0
	.quad	ASN1_ENUMERATED_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	i2s_ASN1_ENUMERATED_TABLE
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	crl_reasons
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Unspecified"
.LC2:
	.string	"unspecified"
.LC3:
	.string	"Key Compromise"
.LC4:
	.string	"keyCompromise"
.LC5:
	.string	"CA Compromise"
.LC6:
	.string	"CACompromise"
.LC7:
	.string	"Affiliation Changed"
.LC8:
	.string	"affiliationChanged"
.LC9:
	.string	"Superseded"
.LC10:
	.string	"superseded"
.LC11:
	.string	"Cessation Of Operation"
.LC12:
	.string	"cessationOfOperation"
.LC13:
	.string	"Certificate Hold"
.LC14:
	.string	"certificateHold"
.LC15:
	.string	"Remove From CRL"
.LC16:
	.string	"removeFromCRL"
.LC17:
	.string	"Privilege Withdrawn"
.LC18:
	.string	"privilegeWithdrawn"
.LC19:
	.string	"AA Compromise"
.LC20:
	.string	"AACompromise"
	.section	.data.rel.local,"aw"
	.align 32
	.type	crl_reasons, @object
	.size	crl_reasons, 264
crl_reasons:
	.long	0
	.zero	4
	.quad	.LC1
	.quad	.LC2
	.long	1
	.zero	4
	.quad	.LC3
	.quad	.LC4
	.long	2
	.zero	4
	.quad	.LC5
	.quad	.LC6
	.long	3
	.zero	4
	.quad	.LC7
	.quad	.LC8
	.long	4
	.zero	4
	.quad	.LC9
	.quad	.LC10
	.long	5
	.zero	4
	.quad	.LC11
	.quad	.LC12
	.long	6
	.zero	4
	.quad	.LC13
	.quad	.LC14
	.long	8
	.zero	4
	.quad	.LC15
	.quad	.LC16
	.long	9
	.zero	4
	.quad	.LC17
	.quad	.LC18
	.long	10
	.zero	4
	.quad	.LC19
	.quad	.LC20
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
