	.file	"ecx_meth.c"
	.text
	.p2align 4
	.type	ecx_size, @function
ecx_size:
.LFB840:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movl	(%rax), %eax
	cmpl	$1034, %eax
	je	.L3
	cmpl	$1087, %eax
	je	.L3
	cmpl	$1035, %eax
	setne	%al
	movzbl	%al, %eax
	addl	$56, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$32, %eax
	ret
	.cfi_endproc
.LFE840:
	.size	ecx_size, .-ecx_size
	.p2align 4
	.type	ecx_bits, @function
ecx_bits:
.LFB841:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movl	(%rax), %eax
	cmpl	$1034, %eax
	je	.L14
	cmpl	$1087, %eax
	je	.L14
	cmpl	$1035, %eax
	setne	%al
	movzbl	%al, %eax
	leal	448(,%rax,8), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movl	$253, %eax
	ret
	.cfi_endproc
.LFE841:
	.size	ecx_bits, .-ecx_bits
	.p2align 4
	.type	ecx_security_bits, @function
ecx_security_bits:
.LFB842:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movl	(%rax), %eax
	cmpl	$1034, %eax
	je	.L18
	cmpl	$1087, %eax
	je	.L18
	movl	$224, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$128, %eax
	ret
	.cfi_endproc
.LFE842:
	.size	ecx_security_bits, .-ecx_security_bits
	.p2align 4
	.type	ecx_cmp_parameters, @function
ecx_cmp_parameters:
.LFB844:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE844:
	.size	ecx_cmp_parameters, .-ecx_cmp_parameters
	.p2align 4
	.type	ecd_ctrl, @function
ecd_ctrl:
.LFB849:
	.cfi_startproc
	endbr64
	cmpl	$3, %esi
	jne	.L22
	movl	$0, (%rcx)
	movl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	movl	$-2, %eax
	ret
	.cfi_endproc
.LFE849:
	.size	ecd_ctrl, .-ecd_ctrl
	.p2align 4
	.type	ecd_size25519, @function
ecd_size25519:
.LFB854:
	.cfi_startproc
	endbr64
	movl	$64, %eax
	ret
	.cfi_endproc
.LFE854:
	.size	ecd_size25519, .-ecd_size25519
	.p2align 4
	.type	ecd_size448, @function
ecd_size448:
.LFB855:
	.cfi_startproc
	endbr64
	movl	$114, %eax
	ret
	.cfi_endproc
.LFE855:
	.size	ecd_size448, .-ecd_size448
	.p2align 4
	.type	pkey_ecx_ctrl, @function
pkey_ecx_ctrl:
.LFB865:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$2, %esi
	sete	%al
	leal	-2(%rax,%rax,2), %eax
	ret
	.cfi_endproc
.LFE865:
	.size	pkey_ecx_ctrl, .-pkey_ecx_ctrl
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ec/ecx_meth.c"
	.text
	.p2align 4
	.type	ecx_free, @function
ecx_free:
.LFB843:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L29
	movq	16(%rbx), %rax
	movl	(%rax), %eax
	cmpl	$1034, %eax
	je	.L31
	cmpl	$1087, %eax
	je	.L31
	xorl	%esi, %esi
	cmpl	$1035, %eax
	setne	%sil
	addq	$56, %rsi
.L30:
	movq	64(%rdi), %rdi
	movl	$258, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_secure_clear_free@PLT
	movq	40(%rbx), %rdi
.L29:
	addq	$8, %rsp
	movl	$259, %edx
	leaq	.LC0(%rip), %rsi
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	movl	$32, %esi
	jmp	.L30
	.cfi_endproc
.LFE843:
	.size	ecx_free, .-ecx_free
	.p2align 4
	.type	ecx_priv_encode, @function
ecx_priv_encode:
.LFB839:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$64, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	40(%rsi), %rax
	movq	$0, -72(%rbp)
	testq	%rax, %rax
	je	.L44
	movq	64(%rax), %rax
	testq	%rax, %rax
	je	.L44
	movq	%rax, -56(%rbp)
	movq	16(%rsi), %rax
	movq	%rdi, %r13
	movq	%rsi, %rbx
	movl	(%rax), %eax
	cmpl	$1034, %eax
	je	.L50
	cmpl	$1087, %eax
	je	.L50
	cmpl	$1035, %eax
	setne	%al
	movzbl	%al, %eax
	addl	$56, %eax
.L47:
	leaq	-72(%rbp), %rsi
	leaq	-64(%rbp), %rdi
	movl	%eax, -64(%rbp)
	movq	$0, -48(%rbp)
	call	i2d_ASN1_OCTET_STRING@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	js	.L63
	movq	16(%rbx), %rax
	movq	-72(%rbp), %r14
	movl	(%rax), %edi
	call	OBJ_nid2obj@PLT
	subq	$8, %rsp
	movl	$-1, %ecx
	xorl	%edx, %edx
	pushq	%r12
	movq	%rax, %rsi
	movq	%r14, %r9
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	call	PKCS8_pkey_set0@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	je	.L64
	movl	$1, %eax
.L43:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L65
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	movl	$32, %eax
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L44:
	movl	$206, %r8d
	movl	$123, %edx
	movl	$267, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L63:
	movl	$216, %r8d
	movl	$65, %edx
	movl	$267, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L64:
	movq	-72(%rbp), %rdi
	movslq	%r12d, %rsi
	movl	$222, %ecx
	movl	%eax, -84(%rbp)
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movl	$223, %r8d
	movl	$65, %edx
	leaq	.LC0(%rip), %rcx
	movl	$267, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	movl	-84(%rbp), %eax
	jmp	.L43
.L65:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE839:
	.size	ecx_priv_encode, .-ecx_priv_encode
	.p2align 4
	.type	ecx_pub_cmp, @function
ecx_pub_cmp:
.LFB837:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %r8
	movq	40(%rsi), %rsi
	testq	%r8, %r8
	je	.L69
	testq	%rsi, %rsi
	je	.L69
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	16(%rdi), %rax
	movl	(%rax), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$1034, %eax
	je	.L70
	cmpl	$1087, %eax
	je	.L70
	xorl	%edx, %edx
	cmpl	$1035, %eax
	setne	%dl
	addq	$56, %rdx
.L68:
	movq	%r8, %rdi
	call	CRYPTO_memcmp@PLT
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	movl	$32, %edx
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movl	$-2, %eax
	ret
	.cfi_endproc
.LFE837:
	.size	ecx_pub_cmp, .-ecx_pub_cmp
	.p2align 4
	.type	ecx_pub_encode, @function
ecx_pub_encode:
.LFB835:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	40(%rsi), %rdi
	testq	%rdi, %rdi
	je	.L106
	movq	16(%rsi), %rax
	movq	%rsi, %rbx
	movl	(%rax), %eax
	cmpl	$1034, %eax
	je	.L88
	cmpl	$1087, %eax
	je	.L88
	xorl	%esi, %esi
	cmpl	$1035, %eax
	setne	%sil
	addq	$56, %rsi
.L85:
	movl	$135, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_memdup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L107
	movq	16(%rbx), %rax
	movl	(%rax), %edi
	cmpl	$1034, %edi
	je	.L90
	cmpl	$1087, %edi
	je	.L90
	xorl	%ebx, %ebx
	cmpl	$1035, %edi
	setne	%bl
	addl	$56, %ebx
.L87:
	call	OBJ_nid2obj@PLT
	xorl	%ecx, %ecx
	movl	%ebx, %r9d
	movq	%r12, %r8
	movq	%rax, %rsi
	movl	$-1, %edx
	movq	%r13, %rdi
	call	X509_PUBKEY_set0_param@PLT
	testl	%eax, %eax
	je	.L108
	movl	$1, %eax
.L82:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	movl	$32, %esi
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L90:
	movl	$32, %ebx
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L108:
	movq	%r12, %rdi
	movl	$143, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, -36(%rbp)
	call	CRYPTO_free@PLT
	movl	$144, %r8d
	movl	$65, %edx
	leaq	.LC0(%rip), %rcx
	movl	$268, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	movl	-36(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	movl	$131, %r8d
	movl	$116, %edx
	movl	$268, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L107:
	movl	$137, %r8d
	movl	$65, %edx
	movl	$268, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L82
	.cfi_endproc
.LFE835:
	.size	ecx_pub_encode, .-ecx_pub_encode
	.p2align 4
	.type	ecd_sig_info_set25519, @function
ecd_sig_info_set25519:
.LFB858:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %r8d
	movl	$128, %ecx
	xorl	%esi, %esi
	movl	$1087, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509_SIG_INFO_set@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE858:
	.size	ecd_sig_info_set25519, .-ecd_sig_info_set25519
	.p2align 4
	.type	ecd_sig_info_set448, @function
ecd_sig_info_set448:
.LFB860:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %r8d
	movl	$224, %ecx
	xorl	%esi, %esi
	movl	$1088, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509_SIG_INFO_set@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE860:
	.size	ecd_sig_info_set448, .-ecd_sig_info_set448
	.p2align 4
	.type	pkey_ecd_digestverify25519, @function
pkey_ecd_digestverify25519:
.LFB868:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	call	EVP_MD_CTX_pkey_ctx@PLT
	cmpq	$64, %rbx
	je	.L116
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
	movq	16(%rax), %rax
	popq	%rbx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	40(%rax), %rcx
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	ED25519_verify@PLT
	.cfi_endproc
.LFE868:
	.size	pkey_ecd_digestverify25519, .-pkey_ecd_digestverify25519
	.p2align 4
	.type	pkey_ecd_digestverify448, @function
pkey_ecd_digestverify448:
.LFB869:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	call	EVP_MD_CTX_pkey_ctx@PLT
	cmpq	$114, %rbx
	je	.L120
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore_state
	movq	16(%rax), %rax
	popq	%rbx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%r9d, %r9d
	popq	%r12
	xorl	%r8d, %r8d
	movq	40(%rax), %rcx
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	ED448_verify@PLT
	.cfi_endproc
.LFE869:
	.size	pkey_ecd_digestverify448, .-pkey_ecd_digestverify448
	.p2align 4
	.type	ecx_get_priv_key, @function
ecx_get_priv_key:
.LFB852:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L145
	movq	40(%rdi), %rax
	testq	%rax, %rax
	je	.L129
	movq	64(%rax), %r8
	testq	%r8, %r8
	je	.L129
	movq	16(%rdi), %rax
	movq	(%rdx), %rcx
	movl	(%rax), %eax
	cmpl	$1034, %eax
	je	.L130
	cmpl	$1087, %eax
	je	.L130
	cmpl	$1035, %eax
	setne	%al
	movzbl	%al, %eax
	addq	$56, %rax
.L125:
	xorl	%r9d, %r9d
	cmpq	%rax, %rcx
	jb	.L121
	movq	%rax, (%rdx)
	movq	(%r8), %rdx
	leaq	8(%rsi), %rdi
	movq	%rsi, %rcx
	andq	$-8, %rdi
	movl	$1, %r9d
	movq	%rdx, (%rsi)
	movq	-8(%r8,%rax), %rdx
	subq	%rdi, %rcx
	movq	%rdx, -8(%rsi,%rax)
	movq	%r8, %rsi
	subq	%rcx, %rsi
	addl	%eax, %ecx
	movl	%ecx, %eax
	shrl	$3, %eax
	movl	%eax, %ecx
	rep movsq
.L121:
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	movl	$32, %eax
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L129:
	xorl	%r9d, %r9d
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	movq	16(%rdi), %rax
	movl	(%rax), %eax
	cmpl	$1034, %eax
	je	.L126
	cmpl	$1087, %eax
	je	.L126
	cmpl	$1035, %eax
	setne	%al
	movzbl	%al, %eax
	addq	$56, %rax
.L123:
	movl	$1, %r9d
	movq	%rax, (%rdx)
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L126:
	movl	$32, %eax
	jmp	.L123
	.cfi_endproc
.LFE852:
	.size	ecx_get_priv_key, .-ecx_get_priv_key
	.p2align 4
	.type	pkey_ecx_derive25519, @function
pkey_ecx_derive25519:
.LFB863:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L147
	movq	%rdx, %rbx
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L147
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L150
	movq	%rsi, %r8
	movq	64(%rax), %rsi
	testq	%rsi, %rsi
	je	.L150
	movq	40(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L162
	testq	%r8, %r8
	je	.L154
	movq	%r8, %rdi
	call	X25519@PLT
	testl	%eax, %eax
	je	.L146
.L154:
	movq	$32, (%rbx)
	movl	$1, %eax
.L146:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	.cfi_restore_state
	movl	$665, %r8d
	movl	$140, %edx
	movl	$278, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore_state
	movl	$675, %r8d
	movl	$133, %edx
	movl	$278, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L150:
	movl	$671, %r8d
	movl	$123, %edx
	movl	$278, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE863:
	.size	pkey_ecx_derive25519, .-pkey_ecx_derive25519
	.p2align 4
	.type	ecd_item_sign25519, @function
ecd_item_sign25519:
.LFB857:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1087, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%r8, %r12
	call	OBJ_nid2obj@PLT
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	X509_ALGOR_set0@PLT
	testq	%r12, %r12
	je	.L164
	movl	$1087, %edi
	call	OBJ_nid2obj@PLT
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	X509_ALGOR_set0@PLT
.L164:
	popq	%r12
	movl	$3, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE857:
	.size	ecd_item_sign25519, .-ecd_item_sign25519
	.p2align 4
	.type	ecd_item_sign448, @function
ecd_item_sign448:
.LFB859:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1088, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%r8, %r12
	call	OBJ_nid2obj@PLT
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	X509_ALGOR_set0@PLT
	testq	%r12, %r12
	je	.L170
	movl	$1088, %edi
	call	OBJ_nid2obj@PLT
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	X509_ALGOR_set0@PLT
.L170:
	popq	%r12
	movl	$3, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE859:
	.size	ecd_item_sign448, .-ecd_item_sign448
	.p2align 4
	.type	ecd_item_verify, @function
ecd_item_verify:
.LFB856:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	leaq	-36(%rbp), %rsi
	leaq	-32(%rbp), %rdi
	.cfi_offset 3, -32
	movq	%r9, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	X509_ALGOR_get0@PLT
	movq	-32(%rbp), %rdi
	call	OBJ_obj2nid@PLT
	subl	$1087, %eax
	cmpl	$1, %eax
	ja	.L176
	cmpl	$-1, -36(%rbp)
	je	.L177
.L176:
	movl	$517, %r8d
	movl	$102, %edx
	movl	$270, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L175:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L182
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%rbx, %r8
	movq	%r12, %rdi
	call	EVP_DigestVerifyInit@PLT
	movl	$2, %edx
	testl	%eax, %eax
	cmovne	%edx, %eax
	jmp	.L175
.L182:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE856:
	.size	ecd_item_verify, .-ecd_item_verify
	.p2align 4
	.type	pkey_ecd_digestsign25519, @function
pkey_ecd_digestsign25519:
.LFB866:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	call	EVP_MD_CTX_pkey_ctx@PLT
	testq	%r12, %r12
	je	.L187
	cmpq	$63, (%rbx)
	jbe	.L189
	movq	16(%rax), %rax
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	40(%rax), %rcx
	movq	64(%rcx), %r8
	call	ED25519_sign@PLT
	testl	%eax, %eax
	je	.L183
.L187:
	movq	$64, (%rbx)
	movl	$1, %eax
.L183:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore_state
	movl	$749, %r8d
	movl	$100, %edx
	movl	$276, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE866:
	.size	pkey_ecd_digestsign25519, .-pkey_ecd_digestsign25519
	.p2align 4
	.type	pkey_ecd_ctrl, @function
pkey_ecd_ctrl:
.LFB870:
	.cfi_startproc
	endbr64
	cmpl	$1, %esi
	je	.L191
	xorl	%eax, %eax
	cmpl	$7, %esi
	sete	%al
	leal	-2(%rax,%rax,2), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L191:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rcx, %rbx
	subq	$8, %rsp
	testq	%rcx, %rcx
	je	.L194
	call	EVP_md_null@PLT
	cmpq	%rax, %rbx
	je	.L194
	movl	$812, %r8d
	movl	$138, %edx
	movl	$271, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L194:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE870:
	.size	pkey_ecd_ctrl, .-pkey_ecd_ctrl
	.p2align 4
	.type	pkey_ecd_digestsign448, @function
pkey_ecd_digestsign448:
.LFB867:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	call	EVP_MD_CTX_pkey_ctx@PLT
	testq	%r12, %r12
	je	.L203
	cmpq	$113, (%rbx)
	jbe	.L205
	movq	16(%rax), %rax
	subq	$8, %rsp
	movq	%r14, %rdx
	xorl	%r9d, %r9d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	40(%rax), %rcx
	movq	64(%rcx), %r8
	pushq	$0
	call	ED448_sign@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	je	.L199
.L203:
	movq	$114, (%rbx)
	movl	$1, %eax
.L199:
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	.cfi_restore_state
	movl	$770, %r8d
	movl	$100, %edx
	movl	$277, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	leaq	-32(%rbp), %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE867:
	.size	pkey_ecd_digestsign448, .-pkey_ecd_digestsign448
	.p2align 4
	.type	ecx_get_pub_key, @function
ecx_get_pub_key:
.LFB853:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L229
	movq	40(%rdi), %r8
	testq	%r8, %r8
	je	.L213
	movq	16(%rdi), %rax
	movq	(%rdx), %rcx
	movl	(%rax), %eax
	cmpl	$1034, %eax
	je	.L214
	cmpl	$1087, %eax
	je	.L214
	cmpl	$1035, %eax
	setne	%al
	movzbl	%al, %eax
	addq	$56, %rax
.L210:
	xorl	%r9d, %r9d
	cmpq	%rax, %rcx
	jb	.L206
	movq	%rax, (%rdx)
	movq	(%r8), %rdx
	leaq	8(%rsi), %rdi
	movq	%rsi, %rcx
	andq	$-8, %rdi
	movl	$1, %r9d
	movq	%rdx, (%rsi)
	movq	-8(%r8,%rax), %rdx
	subq	%rdi, %rcx
	movq	%rdx, -8(%rsi,%rax)
	movq	%r8, %rsi
	subq	%rcx, %rsi
	addl	%eax, %ecx
	shrl	$3, %ecx
	rep movsq
.L206:
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L214:
	movl	$32, %eax
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L229:
	movq	16(%rdi), %rax
	movl	(%rax), %eax
	cmpl	$1034, %eax
	je	.L211
	cmpl	$1087, %eax
	je	.L211
	cmpl	$1035, %eax
	setne	%al
	movzbl	%al, %eax
	addq	$56, %rax
.L208:
	movl	$1, %r9d
	movq	%rax, (%rdx)
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L213:
	xorl	%r9d, %r9d
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L211:
	movl	$32, %eax
	jmp	.L208
	.cfi_endproc
.LFE853:
	.size	ecx_get_pub_key, .-ecx_get_pub_key
	.p2align 4
	.type	pkey_ecx_derive448, @function
pkey_ecx_derive448:
.LFB864:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L231
	movq	%rdx, %rbx
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L231
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L234
	movq	%rsi, %r8
	movq	64(%rax), %rsi
	testq	%rsi, %rsi
	je	.L234
	movq	40(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L246
	testq	%r8, %r8
	je	.L238
	movq	%r8, %rdi
	call	X448@PLT
	testl	%eax, %eax
	je	.L230
.L238:
	movq	$56, (%rbx)
	movl	$1, %eax
.L230:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L231:
	.cfi_restore_state
	movl	$665, %r8d
	movl	$140, %edx
	movl	$278, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L246:
	.cfi_restore_state
	movl	$675, %r8d
	movl	$133, %edx
	movl	$278, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L234:
	movl	$671, %r8d
	movl	$123, %edx
	movl	$278, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE864:
	.size	pkey_ecx_derive448, .-pkey_ecx_derive448
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	""
.LC2:
	.string	"%*s<INVALID PUBLIC KEY>\n"
.LC3:
	.string	"%*s%s Public-Key:\n"
.LC4:
	.string	"%*spub:\n"
	.text
	.p2align 4
	.type	ecx_pub_print, @function
ecx_pub_print:
.LFB847:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	16(%rsi), %rax
	movq	%rsi, %rbx
	movq	40(%rsi), %r14
	movl	(%rax), %edi
	call	OBJ_nid2ln@PLT
	testq	%r14, %r14
	je	.L262
	movq	%rax, %r8
	leaq	.LC1(%rip), %rcx
	xorl	%eax, %eax
	movl	%r12d, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L251
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rcx
	movl	%r12d, %edx
	movq	%r13, %rdi
	leaq	.LC4(%rip), %rsi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L251
	movq	16(%rbx), %rax
	leal	4(%r12), %ecx
	movl	(%rax), %eax
	cmpl	$1034, %eax
	je	.L253
	cmpl	$1087, %eax
	je	.L253
	xorl	%edx, %edx
	cmpl	$1035, %eax
	setne	%dl
	addq	$56, %rdx
.L252:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	ASN1_buf_print@PLT
	popq	%rbx
	popq	%r12
	testl	%eax, %eax
	popq	%r13
	popq	%r14
	setne	%al
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L251:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L262:
	.cfi_restore_state
	movl	%r12d, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rcx
	xorl	%eax, %eax
	leaq	.LC2(%rip), %rsi
	call	BIO_printf@PLT
	popq	%rbx
	popq	%r12
	testl	%eax, %eax
	popq	%r13
	popq	%r14
	setg	%al
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L253:
	.cfi_restore_state
	movl	$32, %edx
	jmp	.L252
	.cfi_endproc
.LFE847:
	.size	ecx_pub_print, .-ecx_pub_print
	.section	.rodata.str1.1
.LC5:
	.string	"%*s<INVALID PRIVATE KEY>\n"
.LC6:
	.string	"%*s%s Private-Key:\n"
.LC7:
	.string	"%*spriv:\n"
	.text
	.p2align 4
	.type	ecx_priv_print, @function
ecx_priv_print:
.LFB846:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	16(%rsi), %rax
	movq	40(%rsi), %r14
	movl	(%rax), %edi
	call	OBJ_nid2ln@PLT
	testq	%r14, %r14
	je	.L264
	cmpq	$0, 64(%r14)
	je	.L264
	movq	%rax, %r8
	leaq	.LC1(%rip), %rcx
	xorl	%eax, %eax
	movl	%r13d, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L268
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rcx
	movl	%r13d, %edx
	movq	%r12, %rdi
	leaq	.LC7(%rip), %rsi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L268
	movq	16(%rbx), %rax
	leal	4(%r13), %r15d
	movl	(%rax), %eax
	cmpl	$1034, %eax
	je	.L271
	cmpl	$1087, %eax
	je	.L271
	xorl	%edx, %edx
	cmpl	$1035, %eax
	setne	%dl
	addq	$56, %rdx
.L269:
	movq	64(%r14), %rsi
	movl	%r15d, %ecx
	movq	%r12, %rdi
	call	ASN1_buf_print@PLT
	testl	%eax, %eax
	je	.L268
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rcx
	movl	%r13d, %edx
	movq	%r12, %rdi
	leaq	.LC4(%rip), %rsi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L268
	movq	16(%rbx), %rax
	movl	(%rax), %eax
	cmpl	$1034, %eax
	je	.L273
	cmpl	$1087, %eax
	je	.L273
	xorl	%edx, %edx
	cmpl	$1035, %eax
	setne	%dl
	addq	$56, %rdx
.L270:
	movl	%r15d, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	ASN1_buf_print@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L264:
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rcx
	movl	%r13d, %edx
	movq	%r12, %rdi
	leaq	.LC5(%rip), %rsi
	call	BIO_printf@PLT
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
.L263:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L268:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L271:
	.cfi_restore_state
	movl	$32, %edx
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L273:
	movl	$32, %edx
	jmp	.L270
	.cfi_endproc
.LFE846:
	.size	ecx_priv_print, .-ecx_priv_print
	.p2align 4
	.type	ecx_priv_decode, @function
ecx_priv_decode:
.LFB838:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-72(%rbp), %r14
	leaq	-64(%rbp), %rcx
	pushq	%r13
	leaq	-80(%rbp), %rdx
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r14, %rsi
	pushq	%r12
	xorl	%edi, %edi
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	PKCS8_pkey_get0@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L347
.L294:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L348
	addq	$56, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L347:
	.cfi_restore_state
	movslq	-80(%rbp), %rdx
	movq	%r14, %rsi
	xorl	%edi, %edi
	call	d2i_ASN1_OCTET_STRING@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L349
	movq	%rax, %rdi
	call	ASN1_STRING_get0_data@PLT
	movq	%r14, %rdi
	movq	%rax, -72(%rbp)
	call	ASN1_STRING_length@PLT
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %r8
	movl	%eax, -80(%rbp)
	movl	%eax, %ebx
	movq	16(%r13), %rax
	movl	(%rax), %r12d
	testq	%rcx, %rcx
	jne	.L297
.L299:
	testq	%r8, %r8
	je	.L301
	cmpl	$1034, %r12d
	movl	$32, %edx
	setne	%al
	cmpl	$1087, %r12d
	setne	%r15b
	andb	%al, %r15b
	je	.L302
	xorl	%edx, %edx
	cmpl	$1035, %r12d
	setne	%dl
	addl	$56, %edx
.L302:
	movq	%r8, -88(%rbp)
	cmpl	%edx, %ebx
	jne	.L301
	movl	$70, %edx
	leaq	.LC0(%rip), %rsi
	movl	$72, %edi
	call	CRYPTO_zalloc@PLT
	movq	-88(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L350
	testb	%r15b, %r15b
	je	.L304
	cmpl	$1035, %r12d
	je	.L351
	leaq	.LC0(%rip), %rsi
	movl	$80, %edx
	movl	$57, %edi
	movq	%r8, -88(%rbp)
	call	CRYPTO_secure_malloc@PLT
	movq	-88(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, 64(%rbx)
	movq	%rax, %rsi
	je	.L314
	movdqu	(%r8), %xmm2
	movups	%xmm2, (%rsi)
	movdqu	16(%r8), %xmm3
	movups	%xmm3, 16(%rsi)
	movdqu	32(%r8), %xmm4
	movups	%xmm4, 32(%rsi)
	movq	48(%r8), %rax
	movq	%rax, 48(%rsi)
	movzbl	56(%r8), %eax
	movb	%al, 56(%rsi)
.L316:
	cmpl	$1087, %r12d
	jg	.L309
	cmpl	$1034, %r12d
	jne	.L352
	movq	%rbx, %rdi
	call	X25519_public_from_private@PLT
	.p2align 4,,10
	.p2align 3
.L312:
	movl	%r12d, %esi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movl	$1, %r12d
	call	EVP_PKEY_assign@PLT
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L349:
	movq	16(%r13), %rax
	movq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	xorl	%ebx, %ebx
	movq	$0, -72(%rbp)
	movl	$0, -80(%rbp)
	movl	(%rax), %r12d
	testq	%rcx, %rcx
	jne	.L297
	.p2align 4,,10
	.p2align 3
.L301:
	movl	$65, %r8d
.L346:
	leaq	.LC0(%rip), %rcx
	movl	$102, %edx
	movl	$266, %esi
	xorl	%r12d, %r12d
	movl	$16, %edi
	call	ERR_put_error@PLT
.L298:
	movq	%r14, %rdi
	call	ASN1_STRING_clear_free@PLT
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L297:
	xorl	%edx, %edx
	xorl	%edi, %edi
	leaq	-76(%rbp), %rsi
	movq	%r8, -88(%rbp)
	call	X509_ALGOR_get0@PLT
	cmpl	$-1, -76(%rbp)
	movq	-88(%rbp), %r8
	je	.L299
	movl	$59, %r8d
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L304:
	leaq	.LC0(%rip), %rsi
	movl	$80, %edx
	movl	$32, %edi
	movq	%r8, -88(%rbp)
	call	CRYPTO_secure_malloc@PLT
	movq	-88(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, 64(%rbx)
	movq	%rax, %rsi
	je	.L314
	movdqu	(%r8), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%r8), %xmm1
	movups	%xmm1, 16(%rax)
	cmpl	$1087, %r12d
	jne	.L316
	movq	%rbx, %rdi
	call	ED25519_public_from_private@PLT
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L309:
	cmpl	$1088, %r12d
	jne	.L312
	movq	%rbx, %rdi
	call	ED448_public_from_private@PLT
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L352:
	cmpl	$1035, %r12d
	jne	.L312
.L311:
	movq	%rbx, %rdi
	call	X448_public_from_private@PLT
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L314:
	movl	$82, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$266, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	movl	$121, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rbx, %rdi
	call	CRYPTO_free@PLT
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L351:
	leaq	.LC0(%rip), %rsi
	movl	$80, %edx
	movl	$56, %edi
	movq	%r8, -88(%rbp)
	call	CRYPTO_secure_malloc@PLT
	movq	-88(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, 64(%rbx)
	movq	%rax, %rsi
	je	.L314
	movdqu	(%r8), %xmm5
	movups	%xmm5, (%rsi)
	movdqu	16(%r8), %xmm6
	movups	%xmm6, 16(%rsi)
	movdqu	32(%r8), %xmm7
	movups	%xmm7, 32(%rsi)
	movq	48(%r8), %rax
	movq	%rax, 48(%rsi)
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L350:
	movl	$72, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$266, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L298
.L348:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE838:
	.size	ecx_priv_decode, .-ecx_priv_decode
	.p2align 4
	.type	ecx_set_pub_key, @function
ecx_set_pub_key:
.LFB851:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	16(%rdi), %rax
	movl	(%rax), %r13d
	testq	%rsi, %rsi
	je	.L354
	movq	%rdi, %r12
	movq	%rsi, %r14
	movq	%rdx, %rbx
	cmpl	$1034, %r13d
	je	.L359
	cmpl	$1087, %r13d
	je	.L359
	xorl	%eax, %eax
	cmpl	$1035, %r13d
	setne	%al
	addl	$56, %eax
.L355:
	cmpl	%eax, %ebx
	jne	.L354
	movl	$70, %edx
	leaq	.LC0(%rip), %rsi
	movl	$72, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L371
	movq	(%r14), %rax
	leaq	8(%rdx), %rdi
	movq	%r14, %rsi
	andq	$-8, %rdi
	movq	%rax, (%rdx)
	movl	%ebx, %eax
	movq	-8(%r14,%rax), %rcx
	movq	%rcx, -8(%rdx,%rax)
	movq	%rdx, %rcx
	subq	%rdi, %rcx
	subq	%rcx, %rsi
	addl	%ebx, %ecx
	movl	%ecx, %ebx
	shrl	$3, %ebx
	movl	%ebx, %ecx
	rep movsq
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	EVP_PKEY_assign@PLT
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L354:
	.cfi_restore_state
	movl	$65, %r8d
	movl	$102, %edx
	movl	$266, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L353:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L359:
	.cfi_restore_state
	movl	$32, %eax
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L371:
	movl	$72, %r8d
	movl	$65, %edx
	movl	$266, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L353
	.cfi_endproc
.LFE851:
	.size	ecx_set_pub_key, .-ecx_set_pub_key
	.p2align 4
	.type	ecx_ctrl, @function
ecx_ctrl:
.LFB848:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpl	$9, %esi
	je	.L373
	movl	$-2, %eax
	cmpl	$10, %esi
	jne	.L372
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L382
	movq	16(%r12), %rax
	movl	(%rax), %eax
	cmpl	$1034, %eax
	je	.L385
	cmpl	$1087, %eax
	je	.L385
	xorl	%esi, %esi
	cmpl	$1035, %eax
	setne	%sil
	addq	$56, %rsi
.L381:
	movl	$329, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_memdup@PLT
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	je	.L382
	movq	16(%r12), %rax
	movl	(%rax), %eax
	cmpl	$1034, %eax
	je	.L387
	cmpl	$1087, %eax
	je	.L387
	cmpl	$1035, %eax
	setne	%al
	movzbl	%al, %eax
	addl	$56, %eax
.L372:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L382:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L373:
	.cfi_restore_state
	movq	16(%rdi), %rax
	movl	(%rax), %r14d
	testq	%rcx, %rcx
	je	.L376
	movq	%rdx, %rbx
	cmpl	$1034, %r14d
	je	.L383
	cmpl	$1087, %r14d
	je	.L383
	xorl	%eax, %eax
	cmpl	$1035, %r14d
	setne	%al
	addl	$56, %eax
	cmpl	%eax, %ebx
	jne	.L376
.L378:
	movl	$70, %edx
	leaq	.LC0(%rip), %rsi
	movl	$72, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L414
	movq	0(%r13), %rax
	leaq	8(%rdx), %rdi
	movq	%r13, %rsi
	andq	$-8, %rdi
	movq	%rax, (%rdx)
	movl	%ebx, %eax
	movq	-8(%r13,%rax), %rcx
	movq	%rcx, -8(%rdx,%rax)
	movq	%rdx, %rcx
	subq	%rdi, %rcx
	subq	%rcx, %rsi
	addl	%ebx, %ecx
	shrl	$3, %ecx
	rep movsq
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	EVP_PKEY_assign@PLT
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L383:
	.cfi_restore_state
	movl	$32, %eax
	cmpl	%eax, %ebx
	je	.L378
.L376:
	movl	$65, %r8d
	movl	$102, %edx
	movl	$266, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L385:
	.cfi_restore_state
	movl	$32, %esi
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L387:
	movl	$32, %eax
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L414:
	movl	$72, %r8d
	movl	$65, %edx
	movl	$266, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L372
	.cfi_endproc
.LFE848:
	.size	ecx_ctrl, .-ecx_ctrl
	.p2align 4
	.type	ecx_set_priv_key, @function
ecx_set_priv_key:
.LFB850:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rax
	movl	(%rax), %r12d
	testq	%rsi, %rsi
	je	.L416
	cmpl	$1034, %r12d
	movq	%rdi, %r13
	movq	%rsi, %rbx
	movl	$32, %ecx
	setne	%al
	cmpl	$1087, %r12d
	setne	%r14b
	andb	%al, %r14b
	je	.L417
	xorl	%ecx, %ecx
	cmpl	$1035, %r12d
	setne	%cl
	addl	$56, %ecx
.L417:
	cmpl	%ecx, %edx
	jne	.L416
	movl	$70, %edx
	leaq	.LC0(%rip), %rsi
	movl	$72, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L454
	testb	%r14b, %r14b
	je	.L421
	cmpl	$1035, %r12d
	je	.L455
	leaq	.LC0(%rip), %rsi
	movl	$80, %edx
	movl	$57, %edi
	call	CRYPTO_secure_malloc@PLT
	movq	%rax, 64(%r15)
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L431
	movdqu	(%rbx), %xmm2
	movups	%xmm2, (%rsi)
	movdqu	16(%rbx), %xmm3
	movups	%xmm3, 16(%rsi)
	movdqu	32(%rbx), %xmm4
	movups	%xmm4, 32(%rsi)
	movq	48(%rbx), %rax
	movq	%rax, 48(%rsi)
	movzbl	56(%rbx), %eax
	movb	%al, 56(%rsi)
	cmpl	$1087, %r12d
	jg	.L426
.L433:
	cmpl	$1035, %r12d
	jne	.L429
.L428:
	movq	%r15, %rdi
	call	X448_public_from_private@PLT
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L416:
	movl	$65, %r8d
	movl	$102, %edx
	movl	$266, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L415:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L421:
	.cfi_restore_state
	leaq	.LC0(%rip), %rsi
	movl	$80, %edx
	movl	$32, %edi
	call	CRYPTO_secure_malloc@PLT
	movq	%rax, 64(%r15)
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L431
	movdqu	(%rbx), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%rbx), %xmm1
	movups	%xmm1, 16(%rax)
	cmpl	$1087, %r12d
	je	.L425
	jg	.L426
	cmpl	$1034, %r12d
	jne	.L433
	movq	%r15, %rdi
	call	X25519_public_from_private@PLT
.L429:
	movq	%r15, %rdx
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	EVP_PKEY_assign@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L455:
	.cfi_restore_state
	leaq	.LC0(%rip), %rsi
	movl	$80, %edx
	movl	$56, %edi
	call	CRYPTO_secure_malloc@PLT
	movq	%rax, 64(%r15)
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L431
	movdqu	(%rbx), %xmm5
	movups	%xmm5, (%rsi)
	movdqu	16(%rbx), %xmm6
	movups	%xmm6, 16(%rsi)
	movdqu	32(%rbx), %xmm7
	movups	%xmm7, 32(%rsi)
	movq	48(%rbx), %rax
	movq	%rax, 48(%rsi)
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L426:
	cmpl	$1088, %r12d
	jne	.L429
	movq	%r15, %rdi
	call	ED448_public_from_private@PLT
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L425:
	movq	%r15, %rdi
	call	ED25519_public_from_private@PLT
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L431:
	movl	$82, %r8d
	movl	$65, %edx
	movl	$266, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$121, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	call	CRYPTO_free@PLT
	xorl	%eax, %eax
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L454:
	movl	$72, %r8d
	movl	$65, %edx
	movl	$266, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L415
	.cfi_endproc
.LFE850:
	.size	ecx_set_priv_key, .-ecx_set_priv_key
	.p2align 4
	.type	ecx_pub_decode, @function
ecx_pub_decode:
.LFB836:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	leaq	-48(%rbp), %rcx
	leaq	-64(%rbp), %rdx
	pushq	%r12
	leaq	-56(%rbp), %rsi
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	xorl	%edi, %edi
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	X509_PUBKEY_get0_param@PLT
	testl	%eax, %eax
	jne	.L484
.L456:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L485
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L484:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	-48(%rbp), %rcx
	movl	-64(%rbp), %ebx
	movq	-56(%rbp), %r13
	movl	(%rax), %r14d
	testq	%rcx, %rcx
	je	.L458
	xorl	%edx, %edx
	xorl	%edi, %edi
	leaq	-60(%rbp), %rsi
	call	X509_ALGOR_get0@PLT
	cmpl	$-1, -60(%rbp)
	movl	$59, %r8d
	jne	.L483
.L458:
	testq	%r13, %r13
	je	.L460
	cmpl	$1034, %r14d
	je	.L465
	cmpl	$1087, %r14d
	je	.L465
	xorl	%eax, %eax
	cmpl	$1035, %r14d
	setne	%al
	addl	$56, %eax
.L461:
	cmpl	%eax, %ebx
	jne	.L460
	movl	$70, %edx
	leaq	.LC0(%rip), %rsi
	movl	$72, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L486
	movq	0(%r13), %rax
	leaq	8(%rdx), %rdi
	movq	%r13, %rsi
	andq	$-8, %rdi
	movq	%rax, (%rdx)
	movl	%ebx, %eax
	movq	-8(%r13,%rax), %rcx
	movq	%rcx, -8(%rdx,%rax)
	movq	%rdx, %rcx
	subq	%rdi, %rcx
	subq	%rcx, %rsi
	addl	%ebx, %ecx
	movl	%ecx, %ebx
	shrl	$3, %ebx
	movl	%ebx, %ecx
	rep movsq
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	EVP_PKEY_assign@PLT
	movl	$1, %eax
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L460:
	movl	$65, %r8d
.L483:
	movl	$102, %edx
	movl	$266, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L465:
	movl	$32, %eax
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L486:
	movl	$72, %r8d
	movl	$65, %edx
	movl	$266, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L456
.L485:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE836:
	.size	ecx_pub_decode, .-ecx_pub_decode
	.p2align 4
	.type	pkey_ecx_keygen, @function
pkey_ecx_keygen:
.LFB861:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$70, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	leaq	.LC0(%rip), %rsi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	(%rdi), %rax
	movl	$72, %edi
	movl	(%rax), %r13d
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L534
	movq	%rax, %r15
	cmpl	$1034, %r13d
	je	.L490
	cmpl	$1087, %r13d
	je	.L490
	cmpl	$1035, %r13d
	je	.L535
	movl	$80, %edx
	leaq	.LC0(%rip), %rsi
	movl	$57, %edi
	call	CRYPTO_secure_malloc@PLT
	movq	%rax, 64(%r15)
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L502
	movl	$57, %esi
	movq	%r12, %rdi
	call	RAND_priv_bytes@PLT
	testl	%eax, %eax
	jle	.L504
.L497:
	cmpl	$1088, %r13d
	je	.L499
	jg	.L500
	cmpl	$1087, %r13d
	jne	.L536
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	ED25519_public_from_private@PLT
	.p2align 4,,10
	.p2align 3
.L500:
	movq	%r15, %rdx
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	EVP_PKEY_assign@PLT
	movl	$1, %eax
.L487:
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L490:
	.cfi_restore_state
	movl	$80, %edx
	leaq	.LC0(%rip), %rsi
	movl	$32, %edi
	call	CRYPTO_secure_malloc@PLT
	movq	%rax, 64(%r15)
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L502
	movl	$32, %esi
	movq	%r12, %rdi
	call	RAND_priv_bytes@PLT
	testl	%eax, %eax
	jle	.L504
	cmpl	$1034, %r13d
	jne	.L495
	movzbl	31(%r12), %eax
	andb	$-8, (%r12)
	andl	$127, %eax
	orl	$64, %eax
	movb	%al, 31(%r12)
.L496:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	X25519_public_from_private@PLT
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L536:
	cmpl	$1034, %r13d
	je	.L496
	cmpl	$1035, %r13d
	jne	.L500
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L535:
	movl	$80, %edx
	leaq	.LC0(%rip), %rsi
	movl	$56, %edi
	call	CRYPTO_secure_malloc@PLT
	movq	%rax, 64(%r15)
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L502
	movl	$56, %esi
	movq	%r12, %rdi
	call	RAND_priv_bytes@PLT
	testl	%eax, %eax
	jle	.L504
.L505:
	andb	$-4, (%r12)
	orb	$-128, 55(%r12)
.L498:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	X448_public_from_private@PLT
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L499:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	ED448_public_from_private@PLT
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L504:
	movl	$87, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_secure_free@PLT
	movq	$0, 64(%r15)
.L493:
	movq	%r15, %rdi
	movl	$121, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L495:
	.cfi_restore_state
	cmpl	$1035, %r13d
	jne	.L497
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L534:
	movl	$72, %r8d
	movl	$65, %edx
	movl	$266, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L502:
	movl	$82, %r8d
	movl	$65, %edx
	movl	$266, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L493
	.cfi_endproc
.LFE861:
	.size	pkey_ecx_keygen, .-pkey_ecx_keygen
	.globl	ed448_pkey_meth
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	ed448_pkey_meth, @object
	.size	ed448_pkey_meth, 256
ed448_pkey_meth:
	.long	1088
	.long	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	pkey_ecx_keygen
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	pkey_ecd_ctrl
	.quad	0
	.quad	pkey_ecd_digestsign448
	.quad	pkey_ecd_digestverify448
	.zero	32
	.globl	ed25519_pkey_meth
	.align 32
	.type	ed25519_pkey_meth, @object
	.size	ed25519_pkey_meth, 256
ed25519_pkey_meth:
	.long	1087
	.long	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	pkey_ecx_keygen
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	pkey_ecd_ctrl
	.quad	0
	.quad	pkey_ecd_digestsign25519
	.quad	pkey_ecd_digestverify25519
	.zero	32
	.globl	ecx448_pkey_meth
	.align 32
	.type	ecx448_pkey_meth, @object
	.size	ecx448_pkey_meth, 256
ecx448_pkey_meth:
	.long	1035
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	pkey_ecx_keygen
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	pkey_ecx_derive448
	.quad	pkey_ecx_ctrl
	.quad	0
	.zero	48
	.globl	ecx25519_pkey_meth
	.align 32
	.type	ecx25519_pkey_meth, @object
	.size	ecx25519_pkey_meth, 256
ecx25519_pkey_meth:
	.long	1034
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	pkey_ecx_keygen
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	pkey_ecx_derive25519
	.quad	pkey_ecx_ctrl
	.quad	0
	.zero	48
	.globl	ed448_asn1_meth
	.section	.rodata.str1.1
.LC8:
	.string	"ED448"
.LC9:
	.string	"OpenSSL ED448 algorithm"
	.section	.data.rel.ro.local
	.align 32
	.type	ed448_asn1_meth, @object
	.size	ed448_asn1_meth, 280
ed448_asn1_meth:
	.long	1088
	.long	1088
	.quad	0
	.quad	.LC8
	.quad	.LC9
	.quad	ecx_pub_decode
	.quad	ecx_pub_encode
	.quad	ecx_pub_cmp
	.quad	ecx_pub_print
	.quad	ecx_priv_decode
	.quad	ecx_priv_encode
	.quad	ecx_priv_print
	.quad	ecd_size448
	.quad	ecx_bits
	.quad	ecx_security_bits
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	ecx_cmp_parameters
	.quad	0
	.quad	0
	.quad	ecx_free
	.quad	ecd_ctrl
	.quad	0
	.quad	0
	.quad	ecd_item_verify
	.quad	ecd_item_sign448
	.quad	ecd_sig_info_set448
	.quad	0
	.quad	0
	.quad	0
	.quad	ecx_set_priv_key
	.quad	ecx_set_pub_key
	.quad	ecx_get_priv_key
	.quad	ecx_get_pub_key
	.globl	ed25519_asn1_meth
	.section	.rodata.str1.1
.LC10:
	.string	"ED25519"
.LC11:
	.string	"OpenSSL ED25519 algorithm"
	.section	.data.rel.ro.local
	.align 32
	.type	ed25519_asn1_meth, @object
	.size	ed25519_asn1_meth, 280
ed25519_asn1_meth:
	.long	1087
	.long	1087
	.quad	0
	.quad	.LC10
	.quad	.LC11
	.quad	ecx_pub_decode
	.quad	ecx_pub_encode
	.quad	ecx_pub_cmp
	.quad	ecx_pub_print
	.quad	ecx_priv_decode
	.quad	ecx_priv_encode
	.quad	ecx_priv_print
	.quad	ecd_size25519
	.quad	ecx_bits
	.quad	ecx_security_bits
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	ecx_cmp_parameters
	.quad	0
	.quad	0
	.quad	ecx_free
	.quad	ecd_ctrl
	.quad	0
	.quad	0
	.quad	ecd_item_verify
	.quad	ecd_item_sign25519
	.quad	ecd_sig_info_set25519
	.quad	0
	.quad	0
	.quad	0
	.quad	ecx_set_priv_key
	.quad	ecx_set_pub_key
	.quad	ecx_get_priv_key
	.quad	ecx_get_pub_key
	.globl	ecx448_asn1_meth
	.section	.rodata.str1.1
.LC12:
	.string	"X448"
.LC13:
	.string	"OpenSSL X448 algorithm"
	.section	.data.rel.ro.local
	.align 32
	.type	ecx448_asn1_meth, @object
	.size	ecx448_asn1_meth, 280
ecx448_asn1_meth:
	.long	1035
	.long	1035
	.quad	0
	.quad	.LC12
	.quad	.LC13
	.quad	ecx_pub_decode
	.quad	ecx_pub_encode
	.quad	ecx_pub_cmp
	.quad	ecx_pub_print
	.quad	ecx_priv_decode
	.quad	ecx_priv_encode
	.quad	ecx_priv_print
	.quad	ecx_size
	.quad	ecx_bits
	.quad	ecx_security_bits
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	ecx_cmp_parameters
	.quad	0
	.quad	0
	.quad	ecx_free
	.quad	ecx_ctrl
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	ecx_set_priv_key
	.quad	ecx_set_pub_key
	.quad	ecx_get_priv_key
	.quad	ecx_get_pub_key
	.globl	ecx25519_asn1_meth
	.section	.rodata.str1.1
.LC14:
	.string	"X25519"
.LC15:
	.string	"OpenSSL X25519 algorithm"
	.section	.data.rel.ro.local
	.align 32
	.type	ecx25519_asn1_meth, @object
	.size	ecx25519_asn1_meth, 280
ecx25519_asn1_meth:
	.long	1034
	.long	1034
	.quad	0
	.quad	.LC14
	.quad	.LC15
	.quad	ecx_pub_decode
	.quad	ecx_pub_encode
	.quad	ecx_pub_cmp
	.quad	ecx_pub_print
	.quad	ecx_priv_decode
	.quad	ecx_priv_encode
	.quad	ecx_priv_print
	.quad	ecx_size
	.quad	ecx_bits
	.quad	ecx_security_bits
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	ecx_cmp_parameters
	.quad	0
	.quad	0
	.quad	ecx_free
	.quad	ecx_ctrl
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	ecx_set_priv_key
	.quad	ecx_set_pub_key
	.quad	ecx_get_priv_key
	.quad	ecx_get_pub_key
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
