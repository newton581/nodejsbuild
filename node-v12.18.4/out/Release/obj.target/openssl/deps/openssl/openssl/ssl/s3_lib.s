	.file	"s3_lib.c"
	.text
	.p2align 4
	.type	cipher_compare, @function
cipher_compare:
.LFB1001:
	.cfi_startproc
	endbr64
	movl	24(%rsi), %edx
	xorl	%eax, %eax
	cmpl	%edx, 24(%rdi)
	je	.L1
	sbbl	%eax, %eax
	orl	$1, %eax
.L1:
	ret
	.cfi_endproc
.LFE1001:
	.size	cipher_compare, .-cipher_compare
	.p2align 4
	.type	ssl_undefined_function_1, @function
ssl_undefined_function_1:
.LFB1003:
	.cfi_startproc
	endbr64
	jmp	ssl_undefined_function@PLT
	.cfi_endproc
.LFE1003:
	.size	ssl_undefined_function_1, .-ssl_undefined_function_1
	.p2align 4
	.globl	ssl3_handshake_write
	.type	ssl3_handshake_write, @function
ssl3_handshake_write:
.LFB1008:
	.cfi_startproc
	endbr64
	movl	$22, %esi
	jmp	ssl3_do_write@PLT
	.cfi_endproc
.LFE1008:
	.size	ssl3_handshake_write, .-ssl3_handshake_write
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/ssl/s3_lib.c"
	.text
	.p2align 4
	.type	srp_password_from_info_cb, @function
srp_password_from_info_cb:
.LFB1012:
	.cfi_startproc
	endbr64
	movq	2080(%rdi), %rdi
	movl	$3384, %edx
	leaq	.LC0(%rip), %rsi
	jmp	CRYPTO_strdup@PLT
	.cfi_endproc
.LFE1012:
	.size	srp_password_from_info_cb, .-srp_password_from_info_cb
	.p2align 4
	.globl	ssl3_set_handshake_header
	.type	ssl3_set_handshake_header, @function
ssl3_set_handshake_header:
.LFB1007:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	cmpl	$257, %edx
	je	.L17
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	movl	%edx, %esi
	movq	%r12, %rdi
	movl	$1, %edx
	subq	$8, %rsp
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	jne	.L20
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$3, %esi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	setne	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE1007:
	.size	ssl3_set_handshake_header, .-ssl3_set_handshake_header
	.p2align 4
	.globl	ssl_sort_cipher_list
	.type	ssl_sort_cipher_list, @function
ssl_sort_cipher_list:
.LFB1002:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$80, %edx
	leaq	cipher_compare(%rip), %rcx
	movl	$5, %esi
	leaq	tls13_ciphers(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	qsort@PLT
	movl	$80, %edx
	leaq	cipher_compare(%rip), %rcx
	movl	$160, %esi
	leaq	ssl3_ciphers(%rip), %rdi
	call	qsort@PLT
	movl	$80, %edx
	movl	$2, %esi
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	cipher_compare(%rip), %rcx
	leaq	ssl3_scsvs(%rip), %rdi
	jmp	qsort@PLT
	.cfi_endproc
.LFE1002:
	.size	ssl_sort_cipher_list, .-ssl_sort_cipher_list
	.p2align 4
	.globl	ssl3_default_timeout
	.type	ssl3_default_timeout, @function
ssl3_default_timeout:
.LFB1004:
	.cfi_startproc
	endbr64
	movl	$7200, %eax
	ret
	.cfi_endproc
.LFE1004:
	.size	ssl3_default_timeout, .-ssl3_default_timeout
	.p2align 4
	.globl	ssl3_num_ciphers
	.type	ssl3_num_ciphers, @function
ssl3_num_ciphers:
.LFB1005:
	.cfi_startproc
	endbr64
	movl	$160, %eax
	ret
	.cfi_endproc
.LFE1005:
	.size	ssl3_num_ciphers, .-ssl3_num_ciphers
	.p2align 4
	.globl	ssl3_get_cipher
	.type	ssl3_get_cipher, @function
ssl3_get_cipher:
.LFB1006:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$159, %edi
	ja	.L25
	movl	%edi, %edi
	movl	$159, %eax
	leaq	ssl3_ciphers(%rip), %rdx
	subq	%rdi, %rax
	leaq	(%rax,%rax,4), %rax
	salq	$4, %rax
	addq	%rdx, %rax
.L25:
	ret
	.cfi_endproc
.LFE1006:
	.size	ssl3_get_cipher, .-ssl3_get_cipher
	.p2align 4
	.globl	ssl3_new
	.type	ssl3_new, @function
ssl3_new:
.LFB1009:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3296, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$1040, %edi
	subq	$8, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L31
	movq	%rax, 168(%r12)
	movq	%r12, %rdi
	call	SSL_SRP_CTX_init@PLT
	testl	%eax, %eax
	je	.L31
	movq	8(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	testl	%eax, %eax
	setne	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1009:
	.size	ssl3_new, .-ssl3_new
	.p2align 4
	.globl	ssl3_free
	.type	ssl3_free, @function
ssl3_free:
.LFB1010:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L42
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	$0, 168(%rdi)
	je	.L36
	call	ssl3_cleanup_key_block@PLT
	movq	168(%rbx), %rax
	movq	1032(%rax), %rdi
	call	EVP_PKEY_free@PLT
	movq	168(%rbx), %rax
	movq	$0, 1032(%rax)
	movq	576(%rax), %rdi
	call	EVP_PKEY_free@PLT
	movq	168(%rbx), %rax
	movl	$3327, %edx
	leaq	.LC0(%rip), %rsi
	movq	$0, 576(%rax)
	movq	592(%rax), %rdi
	call	CRYPTO_free@PLT
	movq	168(%rbx), %rax
	movq	X509_NAME_free@GOTPCREL(%rip), %rsi
	movq	608(%rax), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	168(%rbx), %rax
	movl	$3329, %edx
	leaq	.LC0(%rip), %rsi
	movq	680(%rax), %rdi
	call	CRYPTO_free@PLT
	movq	168(%rbx), %rax
	movl	$3330, %ecx
	leaq	.LC0(%rip), %rdx
	movq	704(%rax), %rsi
	movq	696(%rax), %rdi
	call	CRYPTO_clear_free@PLT
	movq	168(%rbx), %rax
	movl	$3331, %edx
	leaq	.LC0(%rip), %rsi
	movq	744(%rax), %rdi
	call	CRYPTO_free@PLT
	movq	168(%rbx), %rax
	movl	$3332, %edx
	leaq	.LC0(%rip), %rsi
	movq	752(%rax), %rdi
	call	CRYPTO_free@PLT
	movq	%rbx, %rdi
	call	ssl3_free_digest_list@PLT
	movq	168(%rbx), %rax
	movl	$3334, %edx
	leaq	.LC0(%rip), %rsi
	movq	992(%rax), %rdi
	call	CRYPTO_free@PLT
	movq	168(%rbx), %rax
	movl	$3335, %edx
	leaq	.LC0(%rip), %rsi
	movq	1008(%rax), %rdi
	call	CRYPTO_free@PLT
	movq	%rbx, %rdi
	call	SSL_SRP_CTX_free@PLT
	movl	$3340, %ecx
	movl	$1040, %esi
	movq	168(%rbx), %rdi
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movq	$0, 168(%rbx)
.L36:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE1010:
	.size	ssl3_free, .-ssl3_free
	.p2align 4
	.globl	ssl3_clear
	.type	ssl3_clear, @function
ssl3_clear:
.LFB1011:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	ssl3_cleanup_key_block@PLT
	movq	168(%rbx), %rax
	movl	$3347, %edx
	leaq	.LC0(%rip), %rsi
	movq	592(%rax), %rdi
	call	CRYPTO_free@PLT
	movq	168(%rbx), %rax
	movq	X509_NAME_free@GOTPCREL(%rip), %rsi
	movq	608(%rax), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	168(%rbx), %rax
	movl	$3349, %edx
	leaq	.LC0(%rip), %rsi
	movq	680(%rax), %rdi
	call	CRYPTO_free@PLT
	movq	168(%rbx), %rax
	movl	$3350, %ecx
	leaq	.LC0(%rip), %rdx
	movq	704(%rax), %rsi
	movq	696(%rax), %rdi
	call	CRYPTO_clear_free@PLT
	movq	168(%rbx), %rax
	movl	$3351, %edx
	leaq	.LC0(%rip), %rsi
	movq	744(%rax), %rdi
	call	CRYPTO_free@PLT
	movq	168(%rbx), %rax
	movl	$3352, %edx
	leaq	.LC0(%rip), %rsi
	movq	752(%rax), %rdi
	call	CRYPTO_free@PLT
	movq	168(%rbx), %rax
	movq	576(%rax), %rdi
	call	EVP_PKEY_free@PLT
	movq	168(%rbx), %rax
	movq	1032(%rax), %rdi
	call	EVP_PKEY_free@PLT
	movq	%rbx, %rdi
	call	ssl3_free_digest_list@PLT
	movq	168(%rbx), %rax
	movl	$3361, %edx
	leaq	.LC0(%rip), %rsi
	movq	992(%rax), %rdi
	call	CRYPTO_free@PLT
	movq	168(%rbx), %rax
	movl	$3362, %edx
	leaq	.LC0(%rip), %rsi
	movq	1008(%rax), %rdi
	call	CRYPTO_free@PLT
	movq	168(%rbx), %rcx
	xorl	%eax, %eax
	leaq	8(%rcx), %rdi
	movq	$0, (%rcx)
	movq	$0, 1032(%rcx)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$1040, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	%rbx, %rdi
	call	ssl_free_wbio_buffer@PLT
	testl	%eax, %eax
	je	.L45
	movl	$768, (%rbx)
	movq	1792(%rbx), %rdi
	movl	$3373, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$1, %eax
	movq	$0, 1792(%rbx)
	movq	$0, 1800(%rbx)
.L45:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1011:
	.size	ssl3_clear, .-ssl3_clear
	.p2align 4
	.globl	ssl3_ctrl
	.type	ssl3_ctrl, @function
ssl3_ctrl:
.LFB1013:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$130, %esi
	ja	.L124
	movq	%rdx, %r13
	leaq	.L54(%rip), %rdx
	movq	%rdi, %r12
	movq	%rcx, %r14
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L54:
	.long	.L94-.L54
	.long	.L93-.L54
	.long	.L124-.L54
	.long	.L92-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L91-.L54
	.long	.L90-.L54
	.long	.L89-.L54
	.long	.L88-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L87-.L54
	.long	.L124-.L54
	.long	.L86-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L85-.L54
	.long	.L84-.L54
	.long	.L83-.L54
	.long	.L82-.L54
	.long	.L81-.L54
	.long	.L80-.L54
	.long	.L79-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L78-.L54
	.long	.L77-.L54
	.long	.L76-.L54
	.long	.L75-.L54
	.long	.L74-.L54
	.long	.L73-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L72-.L54
	.long	.L71-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L70-.L54
	.long	.L69-.L54
	.long	.L68-.L54
	.long	.L67-.L54
	.long	.L66-.L54
	.long	.L65-.L54
	.long	.L64-.L54
	.long	.L63-.L54
	.long	.L62-.L54
	.long	.L124-.L54
	.long	.L61-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L60-.L54
	.long	.L59-.L54
	.long	.L58-.L54
	.long	.L57-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L56-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L124-.L54
	.long	.L55-.L54
	.long	.L53-.L54
	.text
.L63:
	movq	168(%rdi), %rax
	movq	776(%rax), %rax
	testq	%rax, %rax
	jne	.L168
	.p2align 4,,10
	.p2align 3
.L124:
	xorl	%eax, %eax
.L51:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L169
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L53:
	.cfi_restore_state
	cmpq	$0, 1296(%rdi)
	je	.L124
	movq	168(%rdi), %rax
	movq	576(%rax), %rdi
	testq	%rdi, %rdi
	je	.L124
	call	EVP_PKEY_up_ref@PLT
	movq	168(%r12), %rax
	movq	576(%rax), %rax
	movq	%rax, (%r14)
.L167:
	movl	$1, %eax
	jmp	.L51
.L55:
	movq	168(%rdi), %rax
	movq	728(%rax), %rax
	testq	%rax, %rax
	je	.L124
.L168:
	movl	12(%rax), %eax
	movl	%eax, (%r14)
	movl	$1, %eax
	jmp	.L51
.L56:
	movslq	1608(%rdi), %rax
	jmp	.L51
.L57:
	movq	1168(%rdi), %rax
	movl	%r13d, 24(%rax)
	movl	$1, %eax
	jmp	.L51
.L58:
	cmpq	$3, %r13
	jne	.L110
	movl	56(%rdi), %esi
	testl	%esi, %esi
	je	.L124
	movq	168(%rdi), %rcx
	movq	568(%rcx), %rdx
	testq	%rdx, %rdx
	je	.L124
	movl	$2, %eax
	testb	$68, 32(%rdx)
	jne	.L51
	movq	736(%rcx), %rax
	testq	%rax, %rax
	je	.L124
	movq	1168(%rdi), %rdx
	movq	%rax, (%rdx)
	movl	$1, %eax
	jmp	.L51
.L59:
	movq	1168(%rdi), %rdi
	movq	%rcx, %rsi
	call	ssl_cert_select_current@PLT
	cltq
	jmp	.L51
.L61:
	movq	1696(%rdi), %rax
	testq	%rax, %rax
	je	.L124
	movq	%rax, (%rcx)
	movslq	1688(%rdi), %rax
	jmp	.L51
.L62:
	cmpq	$0, 1296(%rdi)
	je	.L124
	movq	168(%rdi), %rax
	movq	1032(%rax), %rdi
	testq	%rdi, %rdi
	je	.L124
	call	EVP_PKEY_up_ref@PLT
	movq	168(%r12), %rax
	movq	1032(%rax), %rax
	movq	%rax, (%r14)
	movl	$1, %eax
	jmp	.L51
.L60:
	movq	1168(%rdi), %rax
	movq	(%rax), %rax
	movq	16(%rax), %rax
	movq	%rax, (%rcx)
	movl	$1, %eax
	jmp	.L51
.L64:
	movq	1168(%rdi), %rdi
	movl	%r13d, %ecx
	movl	$1, %edx
	movq	%r14, %rsi
	call	ssl_cert_set_cert_store@PLT
	cltq
	jmp	.L51
.L65:
	movq	1168(%rdi), %rdi
	movl	%r13d, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	ssl_cert_set_cert_store@PLT
	cltq
	jmp	.L51
.L66:
	movl	%r13d, %edx
	xorl	%esi, %esi
	call	ssl_build_cert_chain@PLT
	cltq
	jmp	.L51
.L67:
	movl	56(%rdi), %eax
	testl	%eax, %eax
	je	.L124
	movq	1168(%rdi), %rbx
	movl	$4389, %edx
	leaq	.LC0(%rip), %rsi
	movq	392(%rbx), %rdi
	call	CRYPTO_free@PLT
	movq	$0, 392(%rbx)
	movq	$0, 400(%rbx)
	testq	%r14, %r14
	je	.L167
	testq	%r13, %r13
	je	.L167
	cmpq	$255, %r13
	ja	.L124
	movl	$4396, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	CRYPTO_memdup@PLT
	movq	%rax, 392(%rbx)
	testq	%rax, %rax
	je	.L124
	movq	%r13, 400(%rbx)
	movl	$1, %eax
	jmp	.L51
.L68:
	movl	56(%rdi), %ecx
	testl	%ecx, %ecx
	jne	.L124
	movq	168(%rdi), %rax
	movl	584(%rax), %edx
	testl	%edx, %edx
	je	.L124
	testq	%r14, %r14
	je	.L119
	movq	592(%rax), %rdx
	movq	%rdx, (%r14)
.L119:
	movq	600(%rax), %rax
	jmp	.L51
.L69:
	movq	1168(%rdi), %rdi
	movl	$1, %edx
	movq	%rcx, %rsi
	call	tls1_set_sigalgs_list@PLT
	cltq
	jmp	.L51
.L70:
	movq	1168(%rdi), %rdi
	movl	$1, %ecx
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	tls1_set_sigalgs@PLT
	cltq
	jmp	.L51
.L71:
	movq	1168(%rdi), %rdi
	xorl	%edx, %edx
	movq	%rcx, %rsi
	call	tls1_set_sigalgs_list@PLT
	cltq
	jmp	.L51
.L72:
	movq	1168(%rdi), %rdi
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	tls1_set_sigalgs@PLT
	cltq
	jmp	.L51
.L73:
	movl	%r13d, %esi
	call	tls1_shared_group@PLT
	movl	%eax, %edx
	movzwl	%ax, %eax
	cmpq	$-1, %r13
	je	.L51
	movzwl	%dx, %edi
	call	tls1_group_id_lookup@PLT
	testq	%rax, %rax
	je	.L124
	movslq	(%rax), %rax
	jmp	.L51
.L74:
	leaq	1704(%rdi), %rsi
	movq	%rcx, %rdx
	leaq	1712(%rdi), %rdi
	call	tls1_set_groups_list@PLT
	cltq
	jmp	.L51
.L75:
	leaq	1704(%rdi), %rsi
	movq	%r13, %rcx
	leaq	1712(%rdi), %rdi
	movq	%r14, %rdx
	call	tls1_set_groups@PLT
	cltq
	jmp	.L51
.L76:
	cmpq	$0, 1296(%rdi)
	je	.L124
	movq	1720(%rdi), %rbx
	testq	%rcx, %rcx
	je	.L114
	testq	%rbx, %rbx
	je	.L114
	movq	1728(%rdi), %r13
	xorl	%r12d, %r12d
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L170:
	movl	(%rax), %eax
	movl	%eax, (%r14,%r12,4)
.L116:
	addq	$1, %r12
	cmpq	%r12, %rbx
	je	.L114
.L117:
	movzwl	0(%r13,%r12,2), %edi
	call	tls1_group_id_lookup@PLT
	testq	%rax, %rax
	jne	.L170
	movzwl	0(%r13,%r12,2), %eax
	orl	$16777216, %eax
	movl	%eax, (%r14,%r12,4)
	jmp	.L116
.L77:
	movq	%rcx, %rdx
	xorl	%esi, %esi
	testq	%r13, %r13
	je	.L109
	call	ssl_cert_add1_chain_cert@PLT
	cltq
	jmp	.L51
.L78:
	movq	%rcx, %rdx
	xorl	%esi, %esi
	testq	%r13, %r13
	je	.L108
	call	ssl_cert_set1_chain@PLT
	cltq
	jmp	.L51
.L79:
	movq	1648(%rdi), %rdi
	movl	$3543, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r14, 1648(%r12)
	movl	$1, %eax
	movq	%r13, 1656(%r12)
	jmp	.L51
.L87:
	testq	%r13, %r13
	jne	.L104
	movq	1600(%rdi), %rdi
	movl	$3481, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 1600(%r12)
	testq	%r14, %r14
	je	.L167
	movq	%r14, %rdi
	call	strlen@PLT
	subq	$1, %rax
	cmpq	$254, %rax
	ja	.L171
	movl	$3492, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_strdup@PLT
	movq	%rax, 1600(%r12)
	testq	%rax, %rax
	jne	.L167
	movl	$3493, %r8d
	movl	$68, %edx
	movl	$213, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L88:
	movq	168(%rdi), %rax
	movslq	(%rax), %rax
	jmp	.L51
.L89:
	movq	168(%rdi), %rax
	movslq	264(%rax), %rax
	jmp	.L51
.L90:
	movq	168(%rdi), %rdx
	movslq	268(%rdx), %rax
	movl	$0, 268(%rdx)
	jmp	.L51
.L91:
	movq	168(%rdi), %rax
	movslq	268(%rax), %rax
	jmp	.L51
.L92:
	movl	$3437, %r8d
	movl	$66, %edx
	movl	$213, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L51
.L93:
	testq	%rcx, %rcx
	je	.L172
	movq	%rcx, %rdi
	call	EC_KEY_get0_group@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L173
	call	EC_GROUP_get_curve_name@PLT
	movl	%eax, %edx
	movl	%eax, -44(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L51
	leaq	-44(%rbp), %rdx
	leaq	1704(%r12), %rsi
	movl	$1, %ecx
	leaq	1712(%r12), %rdi
	call	tls1_set_groups@PLT
	cltq
	jmp	.L51
.L94:
	movl	$3416, %r8d
	testq	%rcx, %rcx
	je	.L165
	call	EVP_PKEY_new@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	EVP_PKEY_set1_DH@PLT
	testl	%eax, %eax
	jle	.L174
	testq	%r13, %r13
	je	.L98
	movq	%r13, %rdi
	call	EVP_PKEY_security_bits@PLT
	xorl	%ecx, %ecx
	movq	%r13, %r8
	movl	$262151, %esi
	movl	%eax, %edx
	movq	%r12, %rdi
	call	ssl_security@PLT
	testl	%eax, %eax
	je	.L175
	movq	1168(%r12), %rax
	movq	8(%rax), %rdi
	call	EVP_PKEY_free@PLT
	movq	1168(%r12), %rax
	movq	%r13, 8(%rax)
	movl	$1, %eax
	jmp	.L51
.L83:
	movq	%rcx, 1640(%r12)
	movl	$1, %eax
	jmp	.L51
.L84:
	movq	1640(%rdi), %rax
	movq	%rax, (%rcx)
	movl	$1, %eax
	jmp	.L51
.L85:
	movl	%r13d, 1608(%rdi)
	movl	$1, %eax
	jmp	.L51
.L86:
	movq	%rcx, 1592(%r12)
	jmp	.L167
.L81:
	movq	%rcx, 1632(%r12)
	movl	$1, %eax
	jmp	.L51
.L82:
	movq	1632(%rdi), %rax
	movq	%rax, (%rcx)
	movl	$1, %eax
	jmp	.L51
.L80:
	movq	1648(%rdi), %rax
	movq	%rax, (%rcx)
	movq	1656(%rdi), %rax
	testq	%rax, %rax
	jg	.L51
	orq	$-1, %rax
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L114:
	movslq	%ebx, %rax
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L108:
	call	ssl_cert_set0_chain@PLT
	cltq
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L109:
	call	ssl_cert_add0_chain_cert@PLT
	cltq
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L110:
	movq	1168(%rdi), %rdi
	movq	%r13, %rsi
	call	ssl_cert_set_current@PLT
	cltq
	jmp	.L51
.L172:
	movl	$3451, %r8d
.L165:
	movl	$67, %edx
	movl	$213, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L51
.L104:
	movl	$3497, %r8d
	movl	$320, %edx
	movl	$213, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L51
.L175:
	movl	$3426, %r8d
	movl	$394, %edx
	movl	$213, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r13, %rdi
	call	EVP_PKEY_free@PLT
	xorl	%eax, %eax
	jmp	.L51
.L171:
	movl	$3489, %r8d
	movl	$319, %edx
	movl	$213, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L51
.L173:
	movl	$3456, %r8d
	movl	$124, %edx
	movl	$213, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L51
.L174:
	movq	%r13, %rdi
	call	EVP_PKEY_free@PLT
.L98:
	movl	$3421, %r8d
	movl	$65, %edx
	movl	$213, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L51
.L169:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1013:
	.size	ssl3_ctrl, .-ssl3_ctrl
	.p2align 4
	.globl	ssl3_callback_ctrl
	.type	ssl3_callback_ctrl, @function
ssl3_callback_ctrl:
.LFB1014:
	.cfi_startproc
	endbr64
	cmpl	$56, %esi
	je	.L177
	cmpl	$79, %esi
	je	.L178
	cmpl	$6, %esi
	je	.L180
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	movq	%rdx, 1584(%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	movq	1168(%rdi), %rax
	movq	%rdx, 16(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	movq	%rdx, 2104(%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1014:
	.size	ssl3_callback_ctrl, .-ssl3_callback_ctrl
	.p2align 4
	.globl	ssl3_ctx_ctrl
	.type	ssl3_ctx_ctrl, @function
ssl3_ctx_ctrl:
.LFB1015:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leal	-3(%rsi), %eax
	cmpl	$126, %eax
	ja	.L253
	movq	%rdx, %r14
	leaq	.L184(%rip), %rdx
	movq	%rdi, %r12
	movq	%rcx, %r13
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L184:
	.long	.L216-.L184
	.long	.L215-.L184
	.long	.L253-.L184
	.long	.L214-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L213-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L212-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L211-.L184
	.long	.L211-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L210-.L184
	.long	.L209-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L208-.L184
	.long	.L207-.L184
	.long	.L206-.L184
	.long	.L205-.L184
	.long	.L204-.L184
	.long	.L203-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L202-.L184
	.long	.L201-.L184
	.long	.L253-.L184
	.long	.L200-.L184
	.long	.L199-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L198-.L184
	.long	.L197-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L196-.L184
	.long	.L195-.L184
	.long	.L253-.L184
	.long	.L194-.L184
	.long	.L193-.L184
	.long	.L192-.L184
	.long	.L191-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L190-.L184
	.long	.L189-.L184
	.long	.L188-.L184
	.long	.L187-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L253-.L184
	.long	.L186-.L184
	.long	.L185-.L184
	.long	.L183-.L184
	.text
.L262:
	movl	$3806, %r8d
.L252:
	movl	$67, %edx
	movl	$133, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L253:
	xorl	%eax, %eax
.L181:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L258
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L211:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L242
	cmpq	$80, %r14
	jne	.L259
	leaq	16(%rcx), %rdx
	leaq	48(%rcx), %rax
	cmpl	$59, %esi
	je	.L260
	movdqu	528(%rdi), %xmm0
	movups	%xmm0, (%rcx)
	movq	544(%rdi), %rcx
	movdqu	(%rcx), %xmm1
	movups	%xmm1, 16(%r13)
	movdqu	16(%rcx), %xmm2
	movups	%xmm2, 16(%rdx)
	movq	544(%rdi), %rdx
	movdqu	32(%rdx), %xmm3
	movups	%xmm3, 48(%r13)
	movdqu	48(%rdx), %xmm4
	movups	%xmm4, 16(%rax)
	movl	$1, %eax
	jmp	.L181
.L204:
	movq	256(%rdi), %rax
	orq	%rax, %r14
	jne	.L257
.L190:
	movq	320(%r12), %rax
	movq	(%rax), %rax
	movq	16(%rax), %rax
.L257:
	movq	%rax, 0(%r13)
.L255:
	movl	$1, %eax
	jmp	.L181
.L189:
	movq	320(%rdi), %rdi
	movq	%rcx, %rsi
	call	ssl_cert_select_current@PLT
	cltq
	jmp	.L181
.L187:
	movq	320(%rdi), %rax
	movl	%r14d, 24(%rax)
	movl	$1, %eax
	jmp	.L181
.L195:
	movq	320(%rdi), %rdi
	movl	$1, %edx
	movq	%rcx, %rsi
	call	tls1_set_sigalgs_list@PLT
	cltq
	jmp	.L181
.L196:
	movq	320(%rdi), %rdi
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	tls1_set_sigalgs@PLT
	cltq
	jmp	.L181
.L197:
	movq	320(%rdi), %rdi
	xorl	%edx, %edx
	movq	%rcx, %rsi
	call	tls1_set_sigalgs_list@PLT
	cltq
	jmp	.L181
.L198:
	movq	320(%rdi), %rdi
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	tls1_set_sigalgs@PLT
	cltq
	jmp	.L181
.L199:
	leaq	600(%rdi), %rsi
	movq	%rcx, %rdx
	leaq	608(%rdi), %rdi
	call	tls1_set_groups_list@PLT
	cltq
	jmp	.L181
.L200:
	leaq	600(%rdi), %rsi
	movq	%r14, %rcx
	leaq	608(%rdi), %rdi
	movq	%r13, %rdx
	call	tls1_set_groups@PLT
	cltq
	jmp	.L181
.L201:
	movq	%rdi, %rsi
	movq	%rcx, %rdx
	xorl	%edi, %edi
	testq	%r14, %r14
	je	.L238
	call	ssl_cert_add1_chain_cert@PLT
	cltq
	jmp	.L181
.L202:
	movq	%rdi, %rsi
	movq	%rcx, %rdx
	xorl	%edi, %edi
	testq	%r14, %r14
	je	.L237
	call	ssl_cert_set1_chain@PLT
	cltq
	jmp	.L181
.L203:
	movq	256(%rdi), %rdi
	movq	X509_free@GOTPCREL(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movl	$1, %eax
	movq	$0, 256(%r12)
	jmp	.L181
.L207:
	orq	$32, 864(%rdi)
	movq	776(%rdi), %rdi
	movl	$3884, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 776(%r12)
	testq	%r13, %r13
	je	.L255
	movq	%r13, %rdi
	call	strlen@PLT
	cmpq	$255, %rax
	ja	.L228
	cmpb	$0, 0(%r13)
	je	.L228
	movl	$3892, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_strdup@PLT
	movl	$3893, %r8d
	movq	%rax, 776(%r12)
	testq	%rax, %rax
	jne	.L255
.L256:
	movl	$68, %edx
	movl	$133, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L181
.L208:
	orq	$32, 864(%rdi)
	movl	$1, %eax
	movq	%rcx, 744(%r12)
	jmp	.L181
.L209:
	movl	%r14d, 576(%rdi)
	movl	$1, %eax
	jmp	.L181
.L210:
	movq	%rcx, 568(%r12)
	movl	$1, %eax
	jmp	.L181
.L205:
	leaq	srp_password_from_info_cb(%rip), %rax
	movq	%rax, 768(%rdi)
	movq	848(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L231
	movl	$3901, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
.L231:
	movl	$3902, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_strdup@PLT
	movq	%rax, 848(%r12)
	testq	%rax, %rax
	jne	.L255
	movl	$3903, %r8d
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L206:
	movl	%r14d, 856(%rdi)
	movl	$1, %eax
	jmp	.L181
.L194:
	movq	320(%rdi), %r12
	movl	$4389, %edx
	leaq	.LC0(%rip), %rsi
	movq	392(%r12), %rdi
	call	CRYPTO_free@PLT
	movq	$0, 392(%r12)
	movq	$0, 400(%r12)
	testq	%r13, %r13
	je	.L255
	testq	%r14, %r14
	je	.L255
	cmpq	$255, %r14
	ja	.L253
	movl	$4396, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	CRYPTO_memdup@PLT
	movq	%rax, 392(%r12)
	testq	%rax, %rax
	je	.L253
	movq	%r14, 400(%r12)
	movl	$1, %eax
	jmp	.L181
.L213:
	movq	256(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L261
.L235:
	movq	%r13, %rsi
	call	OPENSSL_sk_push@PLT
	movl	$3961, %r8d
	testl	%eax, %eax
	jne	.L255
.L254:
	movl	$65, %edx
	movl	$133, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L181
.L214:
	movl	$3792, %r8d
	movl	$66, %edx
	movl	$133, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L181
.L215:
	testq	%rcx, %rcx
	je	.L262
	movq	%rcx, %rdi
	call	EC_KEY_get0_group@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L263
	call	EC_GROUP_get_curve_name@PLT
	movl	%eax, %edx
	movl	%eax, -44(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L181
	leaq	-44(%rbp), %rdx
	leaq	600(%r12), %rsi
	movl	$1, %ecx
	leaq	608(%r12), %rdi
	call	tls1_set_groups@PLT
	cltq
	jmp	.L181
.L216:
	movl	$3772, %r8d
	testq	%rcx, %rcx
	je	.L252
	call	EVP_PKEY_new@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	EVP_PKEY_set1_DH@PLT
	testl	%eax, %eax
	jle	.L264
	testq	%r14, %r14
	je	.L219
	movq	%r14, %rdi
	call	EVP_PKEY_security_bits@PLT
	xorl	%ecx, %ecx
	movq	%r14, %r8
	movl	$262151, %esi
	movl	%eax, %edx
	movq	%r12, %rdi
	call	ssl_ctx_security@PLT
	testl	%eax, %eax
	je	.L265
	movq	320(%r12), %rax
	movq	8(%rax), %rdi
	call	EVP_PKEY_free@PLT
	movq	320(%r12), %rax
	movq	%r14, 8(%rax)
	movl	$1, %eax
	jmp	.L181
.L183:
	movq	568(%rdi), %rax
	movq	%rax, (%rcx)
	movl	$1, %eax
	jmp	.L181
.L188:
	movq	320(%rdi), %rdi
	movq	%r14, %rsi
	call	ssl_cert_set_current@PLT
	cltq
	jmp	.L181
.L191:
	movq	320(%rdi), %rdi
	movl	%r14d, %ecx
	movl	$1, %edx
	movq	%r13, %rsi
	call	ssl_cert_set_cert_store@PLT
	cltq
	jmp	.L181
.L192:
	movq	320(%rdi), %rdi
	movl	%r14d, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	ssl_cert_set_cert_store@PLT
	cltq
	jmp	.L181
.L193:
	movq	%rdi, %rsi
	movl	%r14d, %edx
	xorl	%edi, %edi
	call	ssl_build_cert_chain@PLT
	cltq
	jmp	.L181
.L212:
	movq	%rcx, 520(%r12)
	movl	$1, %eax
	jmp	.L181
.L185:
	movq	560(%rdi), %rax
	movq	%rax, (%rcx)
	movl	$1, %eax
	jmp	.L181
.L186:
	movslq	576(%rdi), %rax
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L237:
	call	ssl_cert_set0_chain@PLT
	cltq
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L238:
	call	ssl_cert_add0_chain_cert@PLT
	cltq
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L228:
	movl	$3889, %r8d
	movl	$357, %edx
	movl	$133, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L181
.L261:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 256(%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L235
	movl	$3956, %r8d
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L259:
	movl	$3835, %r8d
	movl	$325, %edx
	movl	$133, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L181
.L265:
	movl	$3782, %r8d
	movl	$394, %edx
	movl	$133, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r14, %rdi
	call	EVP_PKEY_free@PLT
	xorl	%eax, %eax
	jmp	.L181
.L260:
	movdqu	(%rcx), %xmm5
	movq	544(%rdi), %rcx
	movups	%xmm5, 528(%rdi)
	movdqu	16(%r13), %xmm6
	movups	%xmm6, (%rcx)
	movdqu	16(%rdx), %xmm7
	movups	%xmm7, 16(%rcx)
	movq	544(%rdi), %rdx
	movdqu	48(%r13), %xmm5
	movups	%xmm5, 32(%rdx)
	movdqu	16(%rax), %xmm6
	movl	$1, %eax
	movups	%xmm6, 48(%rdx)
	jmp	.L181
.L263:
	movl	$3811, %r8d
	movl	$124, %edx
	movl	$133, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L181
.L264:
	movq	%r14, %rdi
	call	EVP_PKEY_free@PLT
.L219:
	movl	$3777, %r8d
	jmp	.L254
.L242:
	movl	$80, %eax
	jmp	.L181
.L258:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1015:
	.size	ssl3_ctx_ctrl, .-ssl3_ctx_ctrl
	.p2align 4
	.globl	ssl3_ctx_callback_ctrl
	.type	ssl3_ctx_callback_ctrl, @function
ssl3_ctx_callback_ctrl:
.LFB1016:
	.cfi_startproc
	endbr64
	cmpl	$6, %esi
	je	.L267
	subl	$53, %esi
	cmpl	$26, %esi
	ja	.L278
	leaq	.L270(%rip), %rcx
	movslq	(%rcx,%rsi,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L270:
	.long	.L276-.L270
	.long	.L278-.L270
	.long	.L278-.L270
	.long	.L278-.L270
	.long	.L278-.L270
	.long	.L278-.L270
	.long	.L278-.L270
	.long	.L278-.L270
	.long	.L278-.L270
	.long	.L278-.L270
	.long	.L275-.L270
	.long	.L278-.L270
	.long	.L278-.L270
	.long	.L278-.L270
	.long	.L278-.L270
	.long	.L278-.L270
	.long	.L278-.L270
	.long	.L278-.L270
	.long	.L278-.L270
	.long	.L274-.L270
	.long	.L278-.L270
	.long	.L278-.L270
	.long	.L273-.L270
	.long	.L272-.L270
	.long	.L271-.L270
	.long	.L278-.L270
	.long	.L269-.L270
	.text
.L278:
	xorl	%eax, %eax
	ret
.L269:
	movq	%rdx, 912(%rdi)
	movl	$1, %eax
	ret
.L276:
	movq	%rdx, 512(%rdi)
	movl	$1, %eax
	ret
.L275:
	movq	%rdx, 560(%rdi)
	movl	$1, %eax
	ret
.L274:
	movq	%rdx, 552(%rdi)
	movl	$1, %eax
	ret
.L273:
	orq	$32, 864(%rdi)
	movl	$1, %eax
	movq	%rdx, 752(%rdi)
	ret
.L272:
	orq	$32, 864(%rdi)
	movl	$1, %eax
	movq	%rdx, 760(%rdi)
	ret
.L271:
	orq	$32, 864(%rdi)
	movl	$1, %eax
	movq	%rdx, 768(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L267:
	movq	320(%rdi), %rax
	movq	%rdx, 16(%rax)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1016:
	.size	ssl3_ctx_callback_ctrl, .-ssl3_ctx_callback_ctrl
	.p2align 4
	.globl	ssl3_get_cipher_by_id
	.type	ssl3_get_cipher_by_id, @function
ssl3_get_cipher_by_id:
.LFB1017:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$5, %edx
	leaq	tls13_ciphers(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-112(%rbp), %r12
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	%edi, -88(%rbp)
	movq	%r12, %rdi
	call	OBJ_bsearch_ssl_cipher_id@PLT
	testq	%rax, %rax
	je	.L283
.L279:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L284
	addq	$104, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L283:
	.cfi_restore_state
	movl	$160, %edx
	leaq	ssl3_ciphers(%rip), %rsi
	movq	%r12, %rdi
	call	OBJ_bsearch_ssl_cipher_id@PLT
	testq	%rax, %rax
	jne	.L279
	movl	$2, %edx
	leaq	ssl3_scsvs(%rip), %rsi
	movq	%r12, %rdi
	call	OBJ_bsearch_ssl_cipher_id@PLT
	jmp	.L279
.L284:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1017:
	.size	ssl3_get_cipher_by_id, .-ssl3_get_cipher_by_id
	.p2align 4
	.globl	ssl3_get_cipher_by_std_name
	.type	ssl3_get_cipher_by_std_name, @function
ssl3_get_cipher_by_std_name:
.LFB1018:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	tls13_ciphers(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	ssl3_ciphers(%rip), %rax
	movq	$0, -104(%rbp)
	movq	%rax, %xmm1
	leaq	-96(%rbp), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -112(%rbp)
	leaq	-80(%rbp), %rax
	movaps	%xmm0, -96(%rbp)
	movdqa	.LC1(%rip), %xmm0
	movq	%rax, -120(%rbp)
	movaps	%xmm0, -80(%rbp)
.L289:
	movq	-112(%rbp), %rax
	movq	(%rax,%r12,8), %r15
	movq	-120(%rbp), %rax
	movq	(%rax,%r12,8), %rbx
	testq	%rbx, %rbx
	je	.L286
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L288:
	movq	16(%r15), %rsi
	testq	%rsi, %rsi
	je	.L287
	movq	%r14, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L292
.L287:
	addq	$1, %r13
	addq	$80, %r15
	cmpq	%r13, %rbx
	jne	.L288
.L286:
	cmpq	$1, %r12
	jne	.L293
	cmpq	$0, -104(%rbp)
	je	.L304
.L285:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L305
	movq	-104(%rbp), %rax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L293:
	.cfi_restore_state
	movl	$1, %r12d
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L292:
	movq	%r15, -104(%rbp)
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L304:
	movq	16+ssl3_scsvs(%rip), %rsi
	movq	%r14, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L294
	movq	96+ssl3_scsvs(%rip), %rsi
	movq	%r14, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	leaq	80+ssl3_scsvs(%rip), %rax
	cmovne	-104(%rbp), %rax
	movq	%rax, -104(%rbp)
	jmp	.L285
.L294:
	leaq	ssl3_scsvs(%rip), %rax
	movq	%rax, -104(%rbp)
	jmp	.L285
.L305:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1018:
	.size	ssl3_get_cipher_by_std_name, .-ssl3_get_cipher_by_std_name
	.p2align 4
	.globl	ssl3_get_cipher_by_char
	.type	ssl3_get_cipher_by_char, @function
ssl3_get_cipher_by_char:
.LFB1019:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$5, %edx
	leaq	tls13_ciphers(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-112(%rbp), %r12
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzwl	(%rdi), %eax
	movq	%r12, %rdi
	rolw	$8, %ax
	movzwl	%ax, %eax
	orl	$50331648, %eax
	movl	%eax, -88(%rbp)
	call	OBJ_bsearch_ssl_cipher_id@PLT
	testq	%rax, %rax
	je	.L310
.L306:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L311
	addq	$104, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L310:
	.cfi_restore_state
	movl	$160, %edx
	leaq	ssl3_ciphers(%rip), %rsi
	movq	%r12, %rdi
	call	OBJ_bsearch_ssl_cipher_id@PLT
	testq	%rax, %rax
	jne	.L306
	movl	$2, %edx
	leaq	ssl3_scsvs(%rip), %rsi
	movq	%r12, %rdi
	call	OBJ_bsearch_ssl_cipher_id@PLT
	jmp	.L306
.L311:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1019:
	.size	ssl3_get_cipher_by_char, .-ssl3_get_cipher_by_char
	.p2align 4
	.globl	ssl3_put_cipher_by_char
	.type	ssl3_put_cipher_by_char, @function
ssl3_put_cipher_by_char:
.LFB1020:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdx, %rbx
	subq	$8, %rsp
	movl	24(%r8), %esi
	movl	%esi, %eax
	andl	$-16777216, %eax
	cmpl	$50331648, %eax
	je	.L313
	movq	$0, (%rdx)
	movl	$1, %eax
.L312:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L313:
	.cfi_restore_state
	movzwl	%si, %esi
	movl	$2, %edx
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L312
	movq	$2, (%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1020:
	.size	ssl3_put_cipher_by_char, .-ssl3_put_cipher_by_char
	.p2align 4
	.globl	ssl3_choose_cipher
	.type	ssl3_choose_cipher, @function
ssl3_choose_cipher:
.LFB1021:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	call	EVP_sha256@PLT
	movq	%rax, -104(%rbp)
	movq	1168(%r12), %rax
	testl	$196608, 28(%rax)
	jne	.L420
	movl	1492(%r12), %eax
	testl	$4194304, %eax
	je	.L359
	testl	$2097152, %eax
	jne	.L421
	.p2align 4,,10
	.p2align 3
.L420:
	movq	$0, -96(%rbp)
.L321:
	movq	8(%r12), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L330
	movl	(%rax), %eax
	cmpl	$65536, %eax
	je	.L330
	cmpl	$771, %eax
	jle	.L330
	cmpq	$0, 1416(%r12)
	movl	$0, -84(%rbp)
	je	.L331
	movq	1168(%r12), %rax
	cmpq	$0, 32(%rax)
	je	.L332
	cmpq	$0, 40(%rax)
	jne	.L331
.L332:
	cmpq	$0, 72(%rax)
	je	.L333
	cmpq	$0, 80(%rax)
	movl	$0, -84(%rbp)
	jne	.L331
.L333:
	cmpq	$0, 112(%rax)
	je	.L334
	cmpq	$0, 120(%rax)
	movl	$0, -84(%rbp)
	jne	.L331
.L334:
	cmpq	$0, 152(%rax)
	je	.L335
	cmpq	$0, 160(%rax)
	movl	$0, -84(%rbp)
	jne	.L331
.L335:
	cmpq	$0, 192(%rax)
	je	.L336
	cmpq	$0, 200(%rax)
	movl	$0, -84(%rbp)
	jne	.L331
.L336:
	cmpq	$0, 232(%rax)
	je	.L337
	cmpq	$0, 240(%rax)
	movl	$0, -84(%rbp)
	jne	.L331
.L337:
	cmpq	$0, 272(%rax)
	je	.L338
	cmpq	$0, 280(%rax)
	movl	$0, -84(%rbp)
	jne	.L331
.L338:
	cmpq	$0, 312(%rax)
	je	.L339
	cmpq	$0, 320(%rax)
	movl	$0, -84(%rbp)
	jne	.L331
.L339:
	cmpq	$0, 352(%rax)
	je	.L369
	cmpq	$0, 360(%rax)
	sete	%al
	movzbl	%al, %eax
	movl	%eax, -84(%rbp)
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L330:
	movq	%r12, %rdi
	call	tls1_set_cert_validity@PLT
	movq	%r12, %rdi
	call	ssl_set_masks@PLT
	movl	$0, -84(%rbp)
.L331:
	movq	$0, -56(%rbp)
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	movq	$0, -80(%rbp)
	movq	%r15, -64(%rbp)
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L341:
	movl	52(%r15), %eax
	cmpl	$256, %edx
	je	.L344
	cmpl	$256, %eax
	movl	$65280, %ecx
	cmove	%ecx, %eax
.L345:
	cmpl	%edx, %eax
	jl	.L342
	movl	56(%r15), %eax
	movl	$65280, %ecx
	cmpl	$256, %eax
	cmove	%ecx, %eax
.L346:
	cmpl	%edx, %eax
	jg	.L342
.L347:
	movq	168(%r12), %rdx
	movl	820(%rdx), %eax
	movl	824(%rdx), %edx
	testb	$32, 2096(%r12)
	je	.L351
	orl	$32, %eax
	orl	$64, %edx
.L351:
	movl	28(%r15), %r14d
	movl	32(%r15), %esi
	movq	%r14, %rcx
	movq	%rsi, -56(%rbp)
	testl	$456, %r14d
	je	.L352
	cmpq	$0, 1416(%r12)
	je	.L342
.L352:
	testq	%r14, %rax
	je	.L342
	testq	%rdx, -56(%rbp)
	je	.L342
	andl	$4, %ecx
	je	.L348
	movl	24(%r15), %esi
	movq	%r12, %rdi
	call	tls1_check_ec_tmp_key@PLT
	testl	%eax, %eax
	je	.L342
.L348:
	movq	-64(%rbp), %rdi
	movq	%r15, %rsi
	call	OPENSSL_sk_find@PLT
	testl	%eax, %eax
	movl	%eax, -72(%rbp)
	jns	.L422
	.p2align 4,,10
	.p2align 3
.L342:
	addl	$1, %ebx
.L340:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L355
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r15
	movq	8(%r12), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	movl	(%r12), %edx
	jne	.L341
	cmpl	44(%r15), %edx
	jl	.L342
	cmpl	48(%r15), %edx
	jg	.L342
	movl	(%rax), %eax
	cmpl	$65536, %eax
	je	.L347
	cmpl	$771, %eax
	jle	.L347
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L344:
	movl	$65280, %edx
	cmpl	$256, %eax
	jne	.L345
	movl	56(%r15), %eax
	cmpl	$256, %eax
	jne	.L346
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L422:
	movl	68(%r15), %edx
	xorl	%ecx, %ecx
	movq	%r15, %r8
	movl	$65538, %esi
	movq	%r12, %rdi
	call	ssl_security@PLT
	testl	%eax, %eax
	je	.L342
	testb	$4, %r14b
	movl	-72(%rbp), %r9d
	je	.L353
	testb	$8, -56(%rbp)
	jne	.L423
.L353:
	movl	-84(%rbp), %eax
	testl	%eax, %eax
	je	.L354
	movq	-64(%rbp), %rdi
	movl	%r9d, %esi
	call	OPENSSL_sk_value@PLT
	movl	64(%rax), %edi
	movq	%rax, -72(%rbp)
	call	ssl_md@PLT
	cmpq	%rax, -104(%rbp)
	movq	-72(%rbp), %rdx
	je	.L373
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	cmovne	%rax, %rdx
	movq	%rdx, -80(%rbp)
	jmp	.L342
.L354:
	movq	-64(%rbp), %r15
	movl	%r9d, %esi
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, -80(%rbp)
.L355:
	movq	-96(%rbp), %rdi
	call	OPENSSL_sk_free@PLT
	movq	-80(%rbp), %rax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L423:
	.cfi_restore_state
	movq	168(%r12), %rax
	cmpb	$0, 1028(%rax)
	je	.L353
	cmpq	$0, -80(%rbp)
	jne	.L342
	movq	-64(%rbp), %rdi
	movl	%r9d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, -80(%rbp)
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L359:
	movq	%r13, %rax
	movq	%r15, %r13
	movq	%rax, %r15
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L373:
	movq	%rdx, -80(%rbp)
	jmp	.L355
.L421:
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jle	.L420
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	cmpl	$524288, 36(%rax)
	jne	.L420
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	jle	.L420
	xorl	%r14d, %r14d
	jmp	.L324
.L424:
	addl	$1, %r14d
	cmpl	%ebx, %r14d
	je	.L420
.L324:
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	cmpl	$524288, 36(%rax)
	jne	.L424
	xorl	%edi, %edi
	movl	%ebx, %esi
	movq	%rax, -56(%rbp)
	call	OPENSSL_sk_new_reserve@PLT
	movq	-56(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, -96(%rbp)
	je	.L420
	movq	%rax, %rdi
	movq	%r8, %rsi
	addl	$1, %r14d
	call	OPENSSL_sk_push@PLT
	cmpl	%ebx, %r14d
	jl	.L325
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L327:
	addl	$1, %r14d
	cmpl	%ebx, %r14d
	je	.L328
.L325:
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	cmpl	$524288, 36(%rax)
	movq	%rax, %rsi
	jne	.L327
	movq	-96(%rbp), %rdi
	call	OPENSSL_sk_push@PLT
	jmp	.L327
.L328:
	xorl	%r14d, %r14d
.L326:
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	cmpl	$524288, 36(%rax)
	movq	%rax, %rsi
	je	.L329
	movq	-96(%rbp), %rdi
	call	OPENSSL_sk_push@PLT
.L329:
	addl	$1, %r14d
	cmpl	%ebx, %r14d
	jne	.L326
	movq	-96(%rbp), %r13
	jmp	.L321
.L369:
	movl	$1, -84(%rbp)
	jmp	.L331
	.cfi_endproc
.LFE1021:
	.size	ssl3_choose_cipher, .-ssl3_choose_cipher
	.p2align 4
	.globl	ssl3_get_req_cert_type
	.type	ssl3_get_req_cert_type, @function
ssl3_get_req_cert_type:
.LFB1022:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	1168(%rdi), %rax
	movl	$0, -28(%rbp)
	movq	392(%rax), %rsi
	testq	%rsi, %rsi
	je	.L426
	movq	400(%rax), %rdx
	movq	%r12, %rdi
	call	WPACKET_memcpy@PLT
.L425:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L452
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L426:
	.cfi_restore_state
	movq	%rdi, %rbx
	movl	$327694, %edx
	leaq	-28(%rbp), %rdi
	movq	%rbx, %rsi
	call	ssl_set_sig_mask@PLT
	cmpl	$768, (%rbx)
	je	.L428
.L431:
	movl	-28(%rbp), %eax
	testb	$1, %al
	je	.L453
.L430:
	testb	$2, %al
	je	.L434
.L436:
	cmpl	$768, (%rbx)
	movl	$1, %eax
	jle	.L425
	testb	$8, -28(%rbp)
	jne	.L425
	movl	$1, %edx
	movl	$64, %esi
	movq	%r12, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L428:
	movq	168(%rbx), %rax
	movq	568(%rax), %rax
	testb	$2, 28(%rax)
	je	.L431
	movl	$1, %edx
	movl	$5, %esi
	movq	%r12, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	jne	.L432
.L433:
	xorl	%eax, %eax
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L453:
	movl	$1, %edx
	movl	$1, %esi
	movq	%r12, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L433
	movl	-28(%rbp), %eax
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L434:
	movl	$1, %edx
	movl	$2, %esi
	movq	%r12, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	jne	.L436
	xorl	%eax, %eax
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L432:
	movl	$1, %edx
	movl	$6, %esi
	movq	%r12, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	jne	.L431
	xorl	%eax, %eax
	jmp	.L425
.L452:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1022:
	.size	ssl3_get_req_cert_type, .-ssl3_get_req_cert_type
	.p2align 4
	.globl	ssl3_shutdown
	.type	ssl3_shutdown, @function
ssl3_shutdown:
.LFB1024:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movl	64(%rdi), %r8d
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	je	.L455
.L457:
	movl	$3, 68(%rbx)
	movl	$1, %r12d
.L454:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L477
	leaq	-16(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L455:
	.cfi_restore_state
	call	SSL_in_before@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L457
	movl	68(%rbx), %eax
	testb	$1, %al
	je	.L478
	movq	168(%rbx), %rdx
	movl	252(%rdx), %esi
	testl	%esi, %esi
	je	.L460
	movq	8(%rbx), %rax
	movq	%rbx, %rdi
	call	*120(%rax)
	cmpl	$-1, %eax
	je	.L476
	movl	68(%rbx), %eax
.L462:
	cmpl	$3, %eax
	jne	.L454
	movq	168(%rbx), %rax
	movl	252(%rax), %eax
	testl	%eax, %eax
	jne	.L454
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L460:
	testb	$2, %al
	jne	.L463
	movq	8(%rbx), %rax
	subq	$8, %rsp
	leaq	-32(%rbp), %rdx
	xorl	%ecx, %ecx
	pushq	%rdx
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	*104(%rax)
	movl	68(%rbx), %eax
	popq	%rdx
	popq	%rcx
	testb	$2, %al
	jne	.L462
.L476:
	movl	$-1, %r12d
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L478:
	orl	$1, %eax
	movq	%rbx, %rdi
	xorl	%edx, %edx
	movl	$1, %esi
	movl	%eax, 68(%rbx)
	call	ssl3_send_alert@PLT
	movq	168(%rbx), %rax
	movl	252(%rax), %edi
	testl	%edi, %edi
	jne	.L476
	movl	68(%rbx), %eax
.L463:
	cmpl	$3, %eax
	jne	.L454
.L467:
	movl	$1, %r12d
	jmp	.L454
.L477:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1024:
	.size	ssl3_shutdown, .-ssl3_shutdown
	.p2align 4
	.globl	ssl3_write
	.type	ssl3_write, @function
ssl3_write:
.LFB1025:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$8, %rsp
	call	__errno_location@PLT
	movl	$0, (%rax)
	movq	168(%r12), %rax
	movl	260(%rax), %eax
	testl	%eax, %eax
	jne	.L484
.L481:
	movq	8(%r12), %rax
	movq	%rbx, %r8
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$23, %esi
	movq	112(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L484:
	.cfi_restore_state
	leaq	2112(%r12), %r15
	movq	%r15, %rdi
	call	RECORD_LAYER_read_pending@PLT
	testl	%eax, %eax
	jne	.L481
	movq	%r15, %rdi
	call	RECORD_LAYER_write_pending@PLT
	testl	%eax, %eax
	jne	.L481
	movq	%r12, %rdi
	call	SSL_in_init@PLT
	testl	%eax, %eax
	jne	.L481
	movq	%r12, %rdi
	call	ossl_statem_set_renegotiate@PLT
	movq	168(%r12), %rax
	addl	$1, 268(%rax)
	addl	$1, 264(%rax)
	movl	$0, 260(%rax)
	jmp	.L481
	.cfi_endproc
.LFE1025:
	.size	ssl3_write, .-ssl3_write
	.p2align 4
	.globl	ssl3_read
	.type	ssl3_read, @function
ssl3_read:
.LFB1027:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	call	__errno_location@PLT
	movl	$0, (%rax)
	movq	168(%r12), %rax
	movl	260(%rax), %edi
	testl	%edi, %edi
	jne	.L495
.L486:
	subq	$8, %rsp
	movq	%r13, %rcx
	xorl	%edx, %edx
	movl	$23, %esi
	movl	$1, 272(%rax)
	movq	8(%r12), %rax
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	pushq	%r14
	movq	%r12, %rdi
	call	*104(%rax)
	popq	%rcx
	movq	168(%r12), %rdx
	popq	%rsi
	cmpl	$-1, %eax
	je	.L496
.L491:
	movl	$0, 272(%rdx)
.L485:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L496:
	.cfi_restore_state
	cmpl	$2, 272(%rdx)
	jne	.L491
	movl	$1, %esi
	movq	%r12, %rdi
	call	ossl_statem_set_in_handshake@PLT
	subq	$8, %rsp
	movq	8(%r12), %rax
	xorl	%edx, %edx
	pushq	%r14
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	movq	%r13, %rcx
	movl	$23, %esi
	movq	%r12, %rdi
	call	*104(%rax)
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	%eax, -52(%rbp)
	call	ossl_statem_set_in_handshake@PLT
	popq	%rax
	movl	-52(%rbp), %eax
	popq	%rdx
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L495:
	leaq	2112(%r12), %r15
	movq	%r15, %rdi
	call	RECORD_LAYER_read_pending@PLT
	testl	%eax, %eax
	je	.L487
.L494:
	movq	168(%r12), %rax
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L487:
	movq	%r15, %rdi
	call	RECORD_LAYER_write_pending@PLT
	testl	%eax, %eax
	jne	.L494
	movq	%r12, %rdi
	call	SSL_in_init@PLT
	testl	%eax, %eax
	jne	.L494
	movq	%r12, %rdi
	call	ossl_statem_set_renegotiate@PLT
	movq	168(%r12), %rax
	addl	$1, 268(%rax)
	addl	$1, 264(%rax)
	movl	$0, 260(%rax)
	jmp	.L486
	.cfi_endproc
.LFE1027:
	.size	ssl3_read, .-ssl3_read
	.p2align 4
	.globl	ssl3_peek
	.type	ssl3_peek, @function
ssl3_peek:
.LFB1028:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	call	__errno_location@PLT
	movl	$0, (%rax)
	movq	168(%r12), %rax
	movl	260(%rax), %edi
	testl	%edi, %edi
	jne	.L507
.L498:
	subq	$8, %rsp
	movq	%r13, %rcx
	xorl	%edx, %edx
	movl	$23, %esi
	movl	$1, 272(%rax)
	movq	8(%r12), %rax
	movq	%rbx, %r8
	movq	%r12, %rdi
	pushq	%r14
	movl	$1, %r9d
	call	*104(%rax)
	popq	%rcx
	movq	168(%r12), %rdx
	popq	%rsi
	cmpl	$-1, %eax
	je	.L508
.L503:
	movl	$0, 272(%rdx)
.L497:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L508:
	.cfi_restore_state
	cmpl	$2, 272(%rdx)
	jne	.L503
	movl	$1, %esi
	movq	%r12, %rdi
	call	ossl_statem_set_in_handshake@PLT
	subq	$8, %rsp
	movq	8(%r12), %rax
	xorl	%edx, %edx
	pushq	%r14
	movl	$1, %r9d
	movq	%rbx, %r8
	movq	%r13, %rcx
	movl	$23, %esi
	movq	%r12, %rdi
	call	*104(%rax)
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	%eax, -52(%rbp)
	call	ossl_statem_set_in_handshake@PLT
	popq	%rax
	movl	-52(%rbp), %eax
	popq	%rdx
	jmp	.L497
	.p2align 4,,10
	.p2align 3
.L507:
	leaq	2112(%r12), %r15
	movq	%r15, %rdi
	call	RECORD_LAYER_read_pending@PLT
	testl	%eax, %eax
	je	.L499
.L506:
	movq	168(%r12), %rax
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L499:
	movq	%r15, %rdi
	call	RECORD_LAYER_write_pending@PLT
	testl	%eax, %eax
	jne	.L506
	movq	%r12, %rdi
	call	SSL_in_init@PLT
	testl	%eax, %eax
	jne	.L506
	movq	%r12, %rdi
	call	ossl_statem_set_renegotiate@PLT
	movq	168(%r12), %rax
	addl	$1, 268(%rax)
	addl	$1, 264(%rax)
	movl	$0, 260(%rax)
	jmp	.L498
	.cfi_endproc
.LFE1028:
	.size	ssl3_peek, .-ssl3_peek
	.p2align 4
	.globl	ssl3_renegotiate
	.type	ssl3_renegotiate, @function
ssl3_renegotiate:
.LFB1029:
	.cfi_startproc
	endbr64
	cmpq	$0, 48(%rdi)
	je	.L510
	movq	168(%rdi), %rax
	movl	$1, 260(%rax)
.L510:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1029:
	.size	ssl3_renegotiate, .-ssl3_renegotiate
	.p2align 4
	.globl	ssl3_renegotiate_check
	.type	ssl3_renegotiate_check, @function
ssl3_renegotiate_check:
.LFB1030:
	.cfi_startproc
	endbr64
	movq	168(%rdi), %rax
	movl	260(%rax), %eax
	testl	%eax, %eax
	jne	.L512
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L512:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	2112(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$8, %rsp
	call	RECORD_LAYER_read_pending@PLT
	testl	%eax, %eax
	je	.L523
.L514:
	xorl	%eax, %eax
.L511:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L523:
	.cfi_restore_state
	movq	%r13, %rdi
	call	RECORD_LAYER_write_pending@PLT
	testl	%eax, %eax
	jne	.L514
	testl	%r12d, %r12d
	je	.L515
.L516:
	movq	%rbx, %rdi
	call	ossl_statem_set_renegotiate@PLT
	movq	168(%rbx), %rax
	addl	$1, 268(%rax)
	addl	$1, 264(%rax)
	movl	$0, 260(%rax)
	movl	$1, %eax
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L515:
	movq	%rbx, %rdi
	call	SSL_in_init@PLT
	testl	%eax, %eax
	je	.L516
	jmp	.L514
	.cfi_endproc
.LFE1030:
	.size	ssl3_renegotiate_check, .-ssl3_renegotiate_check
	.p2align 4
	.globl	ssl_get_algorithm2
	.type	ssl_get_algorithm2, @function
ssl_get_algorithm2:
.LFB1031:
	.cfi_startproc
	endbr64
	movq	168(%rdi), %rax
	testq	%rax, %rax
	je	.L529
	movq	568(%rax), %rdx
	testq	%rdx, %rdx
	je	.L529
	movq	8(%rdi), %rcx
	movl	64(%rdx), %eax
	movq	192(%rcx), %rcx
	testb	$4, 96(%rcx)
	jne	.L534
	testl	$456, 28(%rdx)
	je	.L524
	cmpq	$1285, %rax
	movl	$2313, %edx
	cmove	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L534:
	cmpq	$2313, %rax
	movl	$1028, %edx
	cmove	%rdx, %rax
	ret
.L529:
	movq	$-1, %rax
.L524:
	ret
	.cfi_endproc
.LFE1031:
	.size	ssl_get_algorithm2, .-ssl_get_algorithm2
	.p2align 4
	.globl	ssl_fill_hello_random
	.type	ssl_fill_hello_random, @function
ssl_fill_hello_random:
.LFB1032:
	.cfi_startproc
	endbr64
	cmpq	$3, %rcx
	jbe	.L547
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rcx, %rbx
	subq	$8, %rsp
	movl	1496(%rdi), %eax
	testl	%esi, %esi
	jne	.L548
	shrl	$5, %eax
	andl	$1, %eax
	testl	%eax, %eax
	je	.L540
.L551:
	xorl	%edi, %edi
	call	time@PLT
	leal	-4(%rbx), %esi
	leaq	4(%r12), %rdi
	bswap	%eax
	movl	%eax, (%r12)
	call	RAND_bytes@PLT
.L541:
	testl	%eax, %eax
	jle	.L535
	cmpq	$8, %rbx
	jbe	.L542
	cmpl	$1, %r13d
	je	.L549
	cmpl	$2, %r13d
	je	.L550
.L535:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L548:
	.cfi_restore_state
	shrl	$6, %eax
	andl	$1, %eax
	testl	%eax, %eax
	jne	.L551
.L540:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	RAND_bytes@PLT
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L542:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L547:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L549:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movabsq	$91288358664752964, %rdx
	movq	%rdx, -8(%r12,%rbx)
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L550:
	movabsq	$19230764626825028, %rcx
	movq	%rcx, -8(%r12,%rbx)
	jmp	.L535
	.cfi_endproc
.LFE1032:
	.size	ssl_fill_hello_random, .-ssl_fill_hello_random
	.p2align 4
	.globl	ssl_generate_master_secret
	.type	ssl_generate_master_secret, @function
ssl_generate_master_secret:
.LFB1033:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	168(%rdi), %rax
	movl	%ecx, -52(%rbp)
	movq	568(%rax), %rdx
	movl	28(%rdx), %edx
	testl	$456, %edx
	je	.L553
	movzwl	720(%rax), %r11d
	movq	720(%rax), %r8
	rolw	$8, %r11w
	andl	$8, %edx
	jne	.L554
	leaq	4(%r8,%r12), %r15
	movl	$4626, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r8, -64(%rbp)
	movq	%r15, %rdi
	call	CRYPTO_malloc@PLT
	movq	-64(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L573
	movl	%r12d, %eax
	leaq	2(%r14), %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	rolw	$8, %ax
	movq	%r8, -64(%rbp)
	movw	%ax, (%r14)
	call	memcpy@PLT
	movq	-64(%rbp), %r8
	movq	%rax, %rdi
	movq	%r8, %rax
	movl	%r8d, %ecx
	movzbl	%ah, %eax
	movq	%rax, %r10
.L561:
	addq	%r12, %rdi
	movq	%r8, %rdx
	movq	%r8, -64(%rbp)
	movb	%r10b, (%rdi)
	addq	$2, %rdi
	movb	%cl, -1(%rdi)
	movq	168(%rbx), %rax
	movq	712(%rax), %rsi
	call	memcpy@PLT
	movq	168(%rbx), %rax
	movq	-64(%rbp), %r8
	movl	$4639, %ecx
	leaq	.LC0(%rip), %rdx
	movq	712(%rax), %rdi
	movq	%r8, %rsi
	call	CRYPTO_clear_free@PLT
	movq	8(%rbx), %rdx
	movq	%r15, %rcx
	movq	%rbx, %rdi
	movq	168(%rbx), %rax
	movq	192(%rdx), %r10
	movq	%r14, %rdx
	movq	$0, 712(%rax)
	movq	1296(%rbx), %rax
	leaq	80(%rax), %rsi
	leaq	8(%rax), %r8
	call	*24(%r10)
	testl	%eax, %eax
	je	.L574
	movl	$4648, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	CRYPTO_clear_free@PLT
	movl	$1, %eax
.L557:
	testq	%r13, %r13
	je	.L558
	movl	-52(%rbp), %ecx
	movl	%eax, -52(%rbp)
	testl	%ecx, %ecx
	je	.L559
	movl	$4666, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	CRYPTO_clear_free@PLT
	movl	-52(%rbp), %eax
.L558:
	movl	56(%rbx), %edx
	testl	%edx, %edx
	jne	.L552
	movq	168(%rbx), %rdx
	movq	$0, 696(%rdx)
.L552:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L554:
	.cfi_restore_state
	leaq	4(%r8,%r8), %r15
	movl	$4626, %edx
	leaq	.LC0(%rip), %rsi
	movl	%r11d, -56(%rbp)
	movq	%r15, %rdi
	movq	%r8, -64(%rbp)
	call	CRYPTO_malloc@PLT
	movq	-64(%rbp), %r8
	movl	-56(%rbp), %r11d
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L575
	movw	%r11w, (%r14)
	movq	%r8, %rax
	leaq	2(%r14), %rdi
	movq	%r8, %rdx
	movzbl	%ah, %eax
	xorl	%esi, %esi
	movb	%r8b, -56(%rbp)
	movq	%r8, -64(%rbp)
	movb	%al, -65(%rbp)
	call	memset@PLT
	movq	-64(%rbp), %r8
	movzbl	-56(%rbp), %ecx
	movzbl	-65(%rbp), %r10d
	movq	%rax, %rdi
	movq	%r8, %r12
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L553:
	movq	8(%rdi), %rdx
	movq	1296(%rdi), %rax
	movq	%r12, %rcx
	movq	192(%rdx), %r9
	leaq	80(%rax), %rsi
	leaq	8(%rax), %r8
	movq	%r13, %rdx
	call	*24(%r9)
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L559:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	OPENSSL_cleanse@PLT
	movl	-52(%rbp), %eax
	jmp	.L558
	.p2align 4,,10
	.p2align 3
.L574:
	movl	$4644, %ecx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	%eax, -64(%rbp)
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movl	-64(%rbp), %eax
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L573:
	xorl	%eax, %eax
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L575:
	movq	%r8, %r12
	xorl	%eax, %eax
	jmp	.L557
	.cfi_endproc
.LFE1033:
	.size	ssl_generate_master_secret, .-ssl_generate_master_secret
	.p2align 4
	.globl	ssl_generate_pkey
	.type	ssl_generate_pkey, @function
ssl_generate_pkey:
.LFB1034:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$0, -32(%rbp)
	testq	%rdi, %rdi
	je	.L582
	xorl	%esi, %esi
	call	EVP_PKEY_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L579
	movq	%rax, %rdi
	call	EVP_PKEY_keygen_init@PLT
	testl	%eax, %eax
	jle	.L579
	leaq	-32(%rbp), %rsi
	movq	%r12, %rdi
	call	EVP_PKEY_keygen@PLT
	testl	%eax, %eax
	jle	.L584
.L579:
	movq	%r12, %rdi
	call	EVP_PKEY_CTX_free@PLT
	movq	-32(%rbp), %rax
.L576:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L585
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L584:
	.cfi_restore_state
	movq	-32(%rbp), %rdi
	call	EVP_PKEY_free@PLT
	movq	$0, -32(%rbp)
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L582:
	xorl	%eax, %eax
	jmp	.L576
.L585:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1034:
	.size	ssl_generate_pkey, .-ssl_generate_pkey
	.p2align 4
	.globl	ssl_generate_pkey_group
	.type	ssl_generate_pkey_group, @function
ssl_generate_pkey_group:
.LFB1035:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	movzwl	%si, %edi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	call	tls1_group_id_lookup@PLT
	testq	%rax, %rax
	je	.L600
	movzwl	8(%rax), %r13d
	movq	%rax, %rbx
	xorl	%esi, %esi
	andl	$3, %r13d
	cmpw	$2, %r13w
	je	.L601
	movl	$408, %edi
	call	EVP_PKEY_CTX_new_id@PLT
	movq	%rax, %r12
	testq	%r12, %r12
	je	.L602
.L591:
	movq	%r12, %rdi
	call	EVP_PKEY_keygen_init@PLT
	movl	$4722, %r9d
	testl	%eax, %eax
	jle	.L599
	cmpw	$2, %r13w
	je	.L593
	movl	(%rbx), %r8d
	xorl	%r9d, %r9d
	movl	$4097, %ecx
	movl	$6, %edx
	movl	$408, %esi
	movq	%r12, %rdi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L603
.L593:
	leaq	-48(%rbp), %rsi
	movq	%r12, %rdi
	call	EVP_PKEY_keygen@PLT
	testl	%eax, %eax
	jle	.L604
.L588:
	movq	%r12, %rdi
	call	EVP_PKEY_CTX_free@PLT
	movq	-48(%rbp), %rax
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L605
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L601:
	.cfi_restore_state
	movl	(%rax), %edi
	call	EVP_PKEY_CTX_new_id@PLT
	movq	%rax, %r12
	testq	%r12, %r12
	jne	.L591
.L602:
	movl	$4717, %r9d
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
	movq	%r14, %rdi
	movl	$559, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L604:
	movl	$4733, %r9d
	leaq	.LC0(%rip), %r8
	movl	$6, %ecx
	movq	%r14, %rdi
	movl	$559, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movq	-48(%rbp), %rdi
	call	EVP_PKEY_free@PLT
	movq	$0, -48(%rbp)
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L603:
	movl	$4728, %r9d
.L599:
	leaq	.LC0(%rip), %r8
	movl	$6, %ecx
	movl	$559, %edx
	movq	%r14, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L600:
	movl	$68, %ecx
	movl	$559, %edx
	movq	%r14, %rdi
	xorl	%r12d, %r12d
	movl	$4707, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L588
.L605:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1035:
	.size	ssl_generate_pkey_group, .-ssl_generate_pkey_group
	.p2align 4
	.globl	ssl_generate_param_group
	.type	ssl_generate_param_group, @function
ssl_generate_param_group:
.LFB1036:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzwl	%di, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$0, -32(%rbp)
	call	tls1_group_id_lookup@PLT
	testq	%rax, %rax
	je	.L613
	movq	%rax, %rbx
	movzwl	8(%rax), %eax
	andl	$3, %eax
	cmpw	$2, %ax
	je	.L622
	xorl	%esi, %esi
	movl	$408, %edi
	call	EVP_PKEY_CTX_new_id@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L613
	movq	%rax, %rdi
	call	EVP_PKEY_paramgen_init@PLT
	testl	%eax, %eax
	jle	.L608
	movl	(%rbx), %r8d
	xorl	%r9d, %r9d
	movl	$4097, %ecx
	movl	$6, %edx
	movl	$408, %esi
	movq	%r12, %rdi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L608
	leaq	-32(%rbp), %rsi
	movq	%r12, %rdi
	call	EVP_PKEY_paramgen@PLT
	testl	%eax, %eax
	jg	.L608
	movq	-32(%rbp), %rdi
	call	EVP_PKEY_free@PLT
	movq	$0, -32(%rbp)
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L613:
	xorl	%r12d, %r12d
.L608:
	movq	%r12, %rdi
	call	EVP_PKEY_CTX_free@PLT
	movq	-32(%rbp), %rax
.L606:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L623
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L622:
	.cfi_restore_state
	call	EVP_PKEY_new@PLT
	movq	%rax, -32(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L610
	movl	(%rbx), %esi
	call	EVP_PKEY_set_type@PLT
	movl	%eax, %r8d
	movq	-32(%rbp), %rax
	testl	%r8d, %r8d
	jne	.L606
	movq	%rax, %rdi
.L610:
	call	EVP_PKEY_free@PLT
	xorl	%eax, %eax
	jmp	.L606
.L623:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1036:
	.size	ssl_generate_param_group, .-ssl_generate_param_group
	.p2align 4
	.globl	ssl_derive
	.type	ssl_derive, @function
ssl_derive:
.LFB1037:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	testq	%rsi, %rsi
	je	.L637
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L637
	movq	%rsi, %rdi
	xorl	%esi, %esi
	movl	%ecx, %ebx
	call	EVP_PKEY_CTX_new@PLT
	movq	%rax, %rdi
	movq	%rax, %r14
	call	EVP_PKEY_derive_init@PLT
	testl	%eax, %eax
	jle	.L630
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	EVP_PKEY_derive_set_peer@PLT
	testl	%eax, %eax
	jle	.L630
	leaq	-64(%rbp), %r15
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%r15, %rdx
	call	EVP_PKEY_derive@PLT
	testl	%eax, %eax
	jle	.L630
	movq	-64(%rbp), %rdi
	movl	$4806, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movl	$4808, %r9d
	movl	$65, %ecx
	leaq	.LC0(%rip), %r8
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L645
	movq	%r15, %rdx
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	EVP_PKEY_derive@PLT
	testl	%eax, %eax
	jle	.L647
	testl	%ebx, %ebx
	je	.L633
	movq	8(%r13), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L634
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L634
	cmpl	$65536, %eax
	je	.L634
	movl	200(%r13), %eax
	testl	%eax, %eax
	jne	.L635
	movq	%r13, %rdi
	call	ssl_handshake_md@PLT
	leaq	316(%r13), %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	call	tls13_generate_secret@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L646
.L635:
	movq	-64(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	xorl	%r15d, %r15d
	call	tls13_generate_handshake_secret@PLT
	movq	-64(%rbp), %rsi
	testl	%eax, %eax
	setne	%r15b
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L630:
	movl	$80, %esi
	movl	$68, %ecx
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	movl	$4801, %r9d
	leaq	.LC0(%rip), %r8
	movl	$590, %edx
	xorl	%r15d, %r15d
	call	ossl_statem_fatal@PLT
	movq	-64(%rbp), %rsi
.L629:
	movq	%r12, %rdi
	movl	$4846, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movq	%r14, %rdi
	call	EVP_PKEY_CTX_free@PLT
.L624:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L648
	addq	$24, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L637:
	.cfi_restore_state
	movl	$68, %ecx
	movl	$590, %edx
	movq	%r13, %rdi
	xorl	%r15d, %r15d
	movl	$4791, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L624
	.p2align 4,,10
	.p2align 3
.L633:
	movq	168(%r13), %rax
	movq	-64(%rbp), %rsi
	movl	$1, %r15d
	movq	%r12, 696(%rax)
	xorl	%r12d, %r12d
	movq	%rsi, 704(%rax)
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L634:
	movq	-64(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	ssl_generate_master_secret
	movl	%eax, %r15d
.L646:
	movq	-64(%rbp), %rsi
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L647:
	movl	$4814, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
.L645:
	movl	$80, %esi
	movl	$590, %edx
	movq	%r13, %rdi
	xorl	%r15d, %r15d
	call	ossl_statem_fatal@PLT
	movq	-64(%rbp), %rsi
	jmp	.L629
.L648:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1037:
	.size	ssl_derive, .-ssl_derive
	.p2align 4
	.globl	ssl_dh_to_pkey
	.type	ssl_dh_to_pkey, @function
ssl_dh_to_pkey:
.LFB1038:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	testq	%rdi, %rdi
	je	.L651
	movq	%rdi, %r12
	call	EVP_PKEY_new@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	EVP_PKEY_set1_DH@PLT
	testl	%eax, %eax
	jle	.L653
.L649:
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L651:
	.cfi_restore_state
	xorl	%r13d, %r13d
	popq	%r12
	movq	%r13, %rax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L653:
	.cfi_restore_state
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	EVP_PKEY_free@PLT
	jmp	.L649
	.cfi_endproc
.LFE1038:
	.size	ssl_dh_to_pkey, .-ssl_dh_to_pkey
	.globl	SSLv3_enc_data
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	"CLNT"
.LC3:
	.string	"SRVR"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	SSLv3_enc_data, @object
	.size	SSLv3_enc_data, 128
SSLv3_enc_data:
	.quad	ssl3_enc
	.quad	n_ssl3_mac
	.quad	ssl3_setup_key_block
	.quad	ssl3_generate_master_secret
	.quad	ssl3_change_cipher_state
	.quad	ssl3_final_finish_mac
	.quad	.LC2
	.quad	4
	.quad	.LC3
	.quad	4
	.quad	ssl3_alert_code
	.quad	ssl_undefined_function_1
	.long	0
	.zero	4
	.quad	ssl3_set_handshake_header
	.quad	tls_close_construct_packet
	.quad	ssl3_handshake_write
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"TLS_EMPTY_RENEGOTIATION_INFO_SCSV"
	.section	.rodata.str1.1
.LC5:
	.string	"TLS_FALLBACK_SCSV"
	.section	.data.rel.local,"aw"
	.align 32
	.type	ssl3_scsvs, @object
	.size	ssl3_scsvs, 160
ssl3_scsvs:
	.long	0
	.zero	4
	.quad	.LC4
	.quad	.LC4
	.long	50331903
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.zero	4
	.long	0
	.zero	4
	.quad	.LC5
	.quad	.LC5
	.long	50353664
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.zero	4
	.section	.rodata.str1.1
.LC6:
	.string	"NULL-MD5"
.LC7:
	.string	"TLS_RSA_WITH_NULL_MD5"
.LC8:
	.string	"NULL-SHA"
.LC9:
	.string	"TLS_RSA_WITH_NULL_SHA"
.LC10:
	.string	"AES128-SHA"
.LC11:
	.string	"TLS_RSA_WITH_AES_128_CBC_SHA"
.LC12:
	.string	"DHE-DSS-AES128-SHA"
	.section	.rodata.str1.8
	.align 8
.LC13:
	.string	"TLS_DHE_DSS_WITH_AES_128_CBC_SHA"
	.section	.rodata.str1.1
.LC14:
	.string	"DHE-RSA-AES128-SHA"
	.section	.rodata.str1.8
	.align 8
.LC15:
	.string	"TLS_DHE_RSA_WITH_AES_128_CBC_SHA"
	.section	.rodata.str1.1
.LC16:
	.string	"ADH-AES128-SHA"
	.section	.rodata.str1.8
	.align 8
.LC17:
	.string	"TLS_DH_anon_WITH_AES_128_CBC_SHA"
	.section	.rodata.str1.1
.LC18:
	.string	"AES256-SHA"
.LC19:
	.string	"TLS_RSA_WITH_AES_256_CBC_SHA"
.LC20:
	.string	"DHE-DSS-AES256-SHA"
	.section	.rodata.str1.8
	.align 8
.LC21:
	.string	"TLS_DHE_DSS_WITH_AES_256_CBC_SHA"
	.section	.rodata.str1.1
.LC22:
	.string	"DHE-RSA-AES256-SHA"
	.section	.rodata.str1.8
	.align 8
.LC23:
	.string	"TLS_DHE_RSA_WITH_AES_256_CBC_SHA"
	.section	.rodata.str1.1
.LC24:
	.string	"ADH-AES256-SHA"
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"TLS_DH_anon_WITH_AES_256_CBC_SHA"
	.section	.rodata.str1.1
.LC26:
	.string	"NULL-SHA256"
.LC27:
	.string	"TLS_RSA_WITH_NULL_SHA256"
.LC28:
	.string	"AES128-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC29:
	.string	"TLS_RSA_WITH_AES_128_CBC_SHA256"
	.section	.rodata.str1.1
.LC30:
	.string	"AES256-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC31:
	.string	"TLS_RSA_WITH_AES_256_CBC_SHA256"
	.section	.rodata.str1.1
.LC32:
	.string	"DHE-DSS-AES128-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC33:
	.string	"TLS_DHE_DSS_WITH_AES_128_CBC_SHA256"
	.section	.rodata.str1.1
.LC34:
	.string	"DHE-RSA-AES128-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC35:
	.string	"TLS_DHE_RSA_WITH_AES_128_CBC_SHA256"
	.section	.rodata.str1.1
.LC36:
	.string	"DHE-DSS-AES256-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC37:
	.string	"TLS_DHE_DSS_WITH_AES_256_CBC_SHA256"
	.section	.rodata.str1.1
.LC38:
	.string	"DHE-RSA-AES256-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC39:
	.string	"TLS_DHE_RSA_WITH_AES_256_CBC_SHA256"
	.section	.rodata.str1.1
.LC40:
	.string	"ADH-AES128-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC41:
	.string	"TLS_DH_anon_WITH_AES_128_CBC_SHA256"
	.section	.rodata.str1.1
.LC42:
	.string	"ADH-AES256-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC43:
	.string	"TLS_DH_anon_WITH_AES_256_CBC_SHA256"
	.section	.rodata.str1.1
.LC44:
	.string	"AES128-GCM-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC45:
	.string	"TLS_RSA_WITH_AES_128_GCM_SHA256"
	.section	.rodata.str1.1
.LC46:
	.string	"AES256-GCM-SHA384"
	.section	.rodata.str1.8
	.align 8
.LC47:
	.string	"TLS_RSA_WITH_AES_256_GCM_SHA384"
	.section	.rodata.str1.1
.LC48:
	.string	"DHE-RSA-AES128-GCM-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC49:
	.string	"TLS_DHE_RSA_WITH_AES_128_GCM_SHA256"
	.section	.rodata.str1.1
.LC50:
	.string	"DHE-RSA-AES256-GCM-SHA384"
	.section	.rodata.str1.8
	.align 8
.LC51:
	.string	"TLS_DHE_RSA_WITH_AES_256_GCM_SHA384"
	.section	.rodata.str1.1
.LC52:
	.string	"DHE-DSS-AES128-GCM-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC53:
	.string	"TLS_DHE_DSS_WITH_AES_128_GCM_SHA256"
	.section	.rodata.str1.1
.LC54:
	.string	"DHE-DSS-AES256-GCM-SHA384"
	.section	.rodata.str1.8
	.align 8
.LC55:
	.string	"TLS_DHE_DSS_WITH_AES_256_GCM_SHA384"
	.section	.rodata.str1.1
.LC56:
	.string	"ADH-AES128-GCM-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC57:
	.string	"TLS_DH_anon_WITH_AES_128_GCM_SHA256"
	.section	.rodata.str1.1
.LC58:
	.string	"ADH-AES256-GCM-SHA384"
	.section	.rodata.str1.8
	.align 8
.LC59:
	.string	"TLS_DH_anon_WITH_AES_256_GCM_SHA384"
	.section	.rodata.str1.1
.LC60:
	.string	"AES128-CCM"
.LC61:
	.string	"TLS_RSA_WITH_AES_128_CCM"
.LC62:
	.string	"AES256-CCM"
.LC63:
	.string	"TLS_RSA_WITH_AES_256_CCM"
.LC64:
	.string	"DHE-RSA-AES128-CCM"
.LC65:
	.string	"TLS_DHE_RSA_WITH_AES_128_CCM"
.LC66:
	.string	"DHE-RSA-AES256-CCM"
.LC67:
	.string	"TLS_DHE_RSA_WITH_AES_256_CCM"
.LC68:
	.string	"AES128-CCM8"
.LC69:
	.string	"TLS_RSA_WITH_AES_128_CCM_8"
.LC70:
	.string	"AES256-CCM8"
.LC71:
	.string	"TLS_RSA_WITH_AES_256_CCM_8"
.LC72:
	.string	"DHE-RSA-AES128-CCM8"
	.section	.rodata.str1.8
	.align 8
.LC73:
	.string	"TLS_DHE_RSA_WITH_AES_128_CCM_8"
	.section	.rodata.str1.1
.LC74:
	.string	"DHE-RSA-AES256-CCM8"
	.section	.rodata.str1.8
	.align 8
.LC75:
	.string	"TLS_DHE_RSA_WITH_AES_256_CCM_8"
	.section	.rodata.str1.1
.LC76:
	.string	"PSK-AES128-CCM"
.LC77:
	.string	"TLS_PSK_WITH_AES_128_CCM"
.LC78:
	.string	"PSK-AES256-CCM"
.LC79:
	.string	"TLS_PSK_WITH_AES_256_CCM"
.LC80:
	.string	"DHE-PSK-AES128-CCM"
.LC81:
	.string	"TLS_DHE_PSK_WITH_AES_128_CCM"
.LC82:
	.string	"DHE-PSK-AES256-CCM"
.LC83:
	.string	"TLS_DHE_PSK_WITH_AES_256_CCM"
.LC84:
	.string	"PSK-AES128-CCM8"
.LC85:
	.string	"TLS_PSK_WITH_AES_128_CCM_8"
.LC86:
	.string	"PSK-AES256-CCM8"
.LC87:
	.string	"TLS_PSK_WITH_AES_256_CCM_8"
.LC88:
	.string	"DHE-PSK-AES128-CCM8"
	.section	.rodata.str1.8
	.align 8
.LC89:
	.string	"TLS_PSK_DHE_WITH_AES_128_CCM_8"
	.section	.rodata.str1.1
.LC90:
	.string	"DHE-PSK-AES256-CCM8"
	.section	.rodata.str1.8
	.align 8
.LC91:
	.string	"TLS_PSK_DHE_WITH_AES_256_CCM_8"
	.section	.rodata.str1.1
.LC92:
	.string	"ECDHE-ECDSA-AES128-CCM"
	.section	.rodata.str1.8
	.align 8
.LC93:
	.string	"TLS_ECDHE_ECDSA_WITH_AES_128_CCM"
	.section	.rodata.str1.1
.LC94:
	.string	"ECDHE-ECDSA-AES256-CCM"
	.section	.rodata.str1.8
	.align 8
.LC95:
	.string	"TLS_ECDHE_ECDSA_WITH_AES_256_CCM"
	.section	.rodata.str1.1
.LC96:
	.string	"ECDHE-ECDSA-AES128-CCM8"
	.section	.rodata.str1.8
	.align 8
.LC97:
	.string	"TLS_ECDHE_ECDSA_WITH_AES_128_CCM_8"
	.section	.rodata.str1.1
.LC98:
	.string	"ECDHE-ECDSA-AES256-CCM8"
	.section	.rodata.str1.8
	.align 8
.LC99:
	.string	"TLS_ECDHE_ECDSA_WITH_AES_256_CCM_8"
	.section	.rodata.str1.1
.LC100:
	.string	"ECDHE-ECDSA-NULL-SHA"
.LC101:
	.string	"TLS_ECDHE_ECDSA_WITH_NULL_SHA"
.LC102:
	.string	"ECDHE-ECDSA-AES128-SHA"
	.section	.rodata.str1.8
	.align 8
.LC103:
	.string	"TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA"
	.section	.rodata.str1.1
.LC104:
	.string	"ECDHE-ECDSA-AES256-SHA"
	.section	.rodata.str1.8
	.align 8
.LC105:
	.string	"TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA"
	.section	.rodata.str1.1
.LC106:
	.string	"ECDHE-RSA-NULL-SHA"
.LC107:
	.string	"TLS_ECDHE_RSA_WITH_NULL_SHA"
.LC108:
	.string	"ECDHE-RSA-AES128-SHA"
	.section	.rodata.str1.8
	.align 8
.LC109:
	.string	"TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA"
	.section	.rodata.str1.1
.LC110:
	.string	"ECDHE-RSA-AES256-SHA"
	.section	.rodata.str1.8
	.align 8
.LC111:
	.string	"TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA"
	.section	.rodata.str1.1
.LC112:
	.string	"AECDH-NULL-SHA"
.LC113:
	.string	"TLS_ECDH_anon_WITH_NULL_SHA"
.LC114:
	.string	"AECDH-AES128-SHA"
	.section	.rodata.str1.8
	.align 8
.LC115:
	.string	"TLS_ECDH_anon_WITH_AES_128_CBC_SHA"
	.section	.rodata.str1.1
.LC116:
	.string	"AECDH-AES256-SHA"
	.section	.rodata.str1.8
	.align 8
.LC117:
	.string	"TLS_ECDH_anon_WITH_AES_256_CBC_SHA"
	.section	.rodata.str1.1
.LC118:
	.string	"ECDHE-ECDSA-AES128-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC119:
	.string	"TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256"
	.section	.rodata.str1.1
.LC120:
	.string	"ECDHE-ECDSA-AES256-SHA384"
	.section	.rodata.str1.8
	.align 8
.LC121:
	.string	"TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384"
	.section	.rodata.str1.1
.LC122:
	.string	"ECDHE-RSA-AES128-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC123:
	.string	"TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256"
	.section	.rodata.str1.1
.LC124:
	.string	"ECDHE-RSA-AES256-SHA384"
	.section	.rodata.str1.8
	.align 8
.LC125:
	.string	"TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384"
	.section	.rodata.str1.1
.LC126:
	.string	"ECDHE-ECDSA-AES128-GCM-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC127:
	.string	"TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256"
	.section	.rodata.str1.1
.LC128:
	.string	"ECDHE-ECDSA-AES256-GCM-SHA384"
	.section	.rodata.str1.8
	.align 8
.LC129:
	.string	"TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384"
	.section	.rodata.str1.1
.LC130:
	.string	"ECDHE-RSA-AES128-GCM-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC131:
	.string	"TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256"
	.section	.rodata.str1.1
.LC132:
	.string	"ECDHE-RSA-AES256-GCM-SHA384"
	.section	.rodata.str1.8
	.align 8
.LC133:
	.string	"TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384"
	.section	.rodata.str1.1
.LC134:
	.string	"PSK-NULL-SHA"
.LC135:
	.string	"TLS_PSK_WITH_NULL_SHA"
.LC136:
	.string	"DHE-PSK-NULL-SHA"
.LC137:
	.string	"TLS_DHE_PSK_WITH_NULL_SHA"
.LC138:
	.string	"RSA-PSK-NULL-SHA"
.LC139:
	.string	"TLS_RSA_PSK_WITH_NULL_SHA"
.LC140:
	.string	"PSK-AES128-CBC-SHA"
.LC141:
	.string	"TLS_PSK_WITH_AES_128_CBC_SHA"
.LC142:
	.string	"PSK-AES256-CBC-SHA"
.LC143:
	.string	"TLS_PSK_WITH_AES_256_CBC_SHA"
.LC144:
	.string	"DHE-PSK-AES128-CBC-SHA"
	.section	.rodata.str1.8
	.align 8
.LC145:
	.string	"TLS_DHE_PSK_WITH_AES_128_CBC_SHA"
	.section	.rodata.str1.1
.LC146:
	.string	"DHE-PSK-AES256-CBC-SHA"
	.section	.rodata.str1.8
	.align 8
.LC147:
	.string	"TLS_DHE_PSK_WITH_AES_256_CBC_SHA"
	.section	.rodata.str1.1
.LC148:
	.string	"RSA-PSK-AES128-CBC-SHA"
	.section	.rodata.str1.8
	.align 8
.LC149:
	.string	"TLS_RSA_PSK_WITH_AES_128_CBC_SHA"
	.section	.rodata.str1.1
.LC150:
	.string	"RSA-PSK-AES256-CBC-SHA"
	.section	.rodata.str1.8
	.align 8
.LC151:
	.string	"TLS_RSA_PSK_WITH_AES_256_CBC_SHA"
	.section	.rodata.str1.1
.LC152:
	.string	"PSK-AES128-GCM-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC153:
	.string	"TLS_PSK_WITH_AES_128_GCM_SHA256"
	.section	.rodata.str1.1
.LC154:
	.string	"PSK-AES256-GCM-SHA384"
	.section	.rodata.str1.8
	.align 8
.LC155:
	.string	"TLS_PSK_WITH_AES_256_GCM_SHA384"
	.section	.rodata.str1.1
.LC156:
	.string	"DHE-PSK-AES128-GCM-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC157:
	.string	"TLS_DHE_PSK_WITH_AES_128_GCM_SHA256"
	.section	.rodata.str1.1
.LC158:
	.string	"DHE-PSK-AES256-GCM-SHA384"
	.section	.rodata.str1.8
	.align 8
.LC159:
	.string	"TLS_DHE_PSK_WITH_AES_256_GCM_SHA384"
	.section	.rodata.str1.1
.LC160:
	.string	"RSA-PSK-AES128-GCM-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC161:
	.string	"TLS_RSA_PSK_WITH_AES_128_GCM_SHA256"
	.section	.rodata.str1.1
.LC162:
	.string	"RSA-PSK-AES256-GCM-SHA384"
	.section	.rodata.str1.8
	.align 8
.LC163:
	.string	"TLS_RSA_PSK_WITH_AES_256_GCM_SHA384"
	.section	.rodata.str1.1
.LC164:
	.string	"PSK-AES128-CBC-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC165:
	.string	"TLS_PSK_WITH_AES_128_CBC_SHA256"
	.section	.rodata.str1.1
.LC166:
	.string	"PSK-AES256-CBC-SHA384"
	.section	.rodata.str1.8
	.align 8
.LC167:
	.string	"TLS_PSK_WITH_AES_256_CBC_SHA384"
	.section	.rodata.str1.1
.LC168:
	.string	"PSK-NULL-SHA256"
.LC169:
	.string	"TLS_PSK_WITH_NULL_SHA256"
.LC170:
	.string	"PSK-NULL-SHA384"
.LC171:
	.string	"TLS_PSK_WITH_NULL_SHA384"
.LC172:
	.string	"DHE-PSK-AES128-CBC-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC173:
	.string	"TLS_DHE_PSK_WITH_AES_128_CBC_SHA256"
	.section	.rodata.str1.1
.LC174:
	.string	"DHE-PSK-AES256-CBC-SHA384"
	.section	.rodata.str1.8
	.align 8
.LC175:
	.string	"TLS_DHE_PSK_WITH_AES_256_CBC_SHA384"
	.section	.rodata.str1.1
.LC176:
	.string	"DHE-PSK-NULL-SHA256"
.LC177:
	.string	"TLS_DHE_PSK_WITH_NULL_SHA256"
.LC178:
	.string	"DHE-PSK-NULL-SHA384"
.LC179:
	.string	"TLS_DHE_PSK_WITH_NULL_SHA384"
.LC180:
	.string	"RSA-PSK-AES128-CBC-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC181:
	.string	"TLS_RSA_PSK_WITH_AES_128_CBC_SHA256"
	.section	.rodata.str1.1
.LC182:
	.string	"RSA-PSK-AES256-CBC-SHA384"
	.section	.rodata.str1.8
	.align 8
.LC183:
	.string	"TLS_RSA_PSK_WITH_AES_256_CBC_SHA384"
	.section	.rodata.str1.1
.LC184:
	.string	"RSA-PSK-NULL-SHA256"
.LC185:
	.string	"TLS_RSA_PSK_WITH_NULL_SHA256"
.LC186:
	.string	"RSA-PSK-NULL-SHA384"
.LC187:
	.string	"TLS_RSA_PSK_WITH_NULL_SHA384"
.LC188:
	.string	"ECDHE-PSK-AES128-CBC-SHA"
	.section	.rodata.str1.8
	.align 8
.LC189:
	.string	"TLS_ECDHE_PSK_WITH_AES_128_CBC_SHA"
	.section	.rodata.str1.1
.LC190:
	.string	"ECDHE-PSK-AES256-CBC-SHA"
	.section	.rodata.str1.8
	.align 8
.LC191:
	.string	"TLS_ECDHE_PSK_WITH_AES_256_CBC_SHA"
	.section	.rodata.str1.1
.LC192:
	.string	"ECDHE-PSK-AES128-CBC-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC193:
	.string	"TLS_ECDHE_PSK_WITH_AES_128_CBC_SHA256"
	.section	.rodata.str1.1
.LC194:
	.string	"ECDHE-PSK-AES256-CBC-SHA384"
	.section	.rodata.str1.8
	.align 8
.LC195:
	.string	"TLS_ECDHE_PSK_WITH_AES_256_CBC_SHA384"
	.section	.rodata.str1.1
.LC196:
	.string	"ECDHE-PSK-NULL-SHA"
.LC197:
	.string	"TLS_ECDHE_PSK_WITH_NULL_SHA"
.LC198:
	.string	"ECDHE-PSK-NULL-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC199:
	.string	"TLS_ECDHE_PSK_WITH_NULL_SHA256"
	.section	.rodata.str1.1
.LC200:
	.string	"ECDHE-PSK-NULL-SHA384"
	.section	.rodata.str1.8
	.align 8
.LC201:
	.string	"TLS_ECDHE_PSK_WITH_NULL_SHA384"
	.section	.rodata.str1.1
.LC202:
	.string	"SRP-AES-128-CBC-SHA"
	.section	.rodata.str1.8
	.align 8
.LC203:
	.string	"TLS_SRP_SHA_WITH_AES_128_CBC_SHA"
	.section	.rodata.str1.1
.LC204:
	.string	"SRP-RSA-AES-128-CBC-SHA"
	.section	.rodata.str1.8
	.align 8
.LC205:
	.string	"TLS_SRP_SHA_RSA_WITH_AES_128_CBC_SHA"
	.section	.rodata.str1.1
.LC206:
	.string	"SRP-DSS-AES-128-CBC-SHA"
	.section	.rodata.str1.8
	.align 8
.LC207:
	.string	"TLS_SRP_SHA_DSS_WITH_AES_128_CBC_SHA"
	.section	.rodata.str1.1
.LC208:
	.string	"SRP-AES-256-CBC-SHA"
	.section	.rodata.str1.8
	.align 8
.LC209:
	.string	"TLS_SRP_SHA_WITH_AES_256_CBC_SHA"
	.section	.rodata.str1.1
.LC210:
	.string	"SRP-RSA-AES-256-CBC-SHA"
	.section	.rodata.str1.8
	.align 8
.LC211:
	.string	"TLS_SRP_SHA_RSA_WITH_AES_256_CBC_SHA"
	.section	.rodata.str1.1
.LC212:
	.string	"SRP-DSS-AES-256-CBC-SHA"
	.section	.rodata.str1.8
	.align 8
.LC213:
	.string	"TLS_SRP_SHA_DSS_WITH_AES_256_CBC_SHA"
	.section	.rodata.str1.1
.LC214:
	.string	"DHE-RSA-CHACHA20-POLY1305"
	.section	.rodata.str1.8
	.align 8
.LC215:
	.string	"TLS_DHE_RSA_WITH_CHACHA20_POLY1305_SHA256"
	.section	.rodata.str1.1
.LC216:
	.string	"ECDHE-RSA-CHACHA20-POLY1305"
	.section	.rodata.str1.8
	.align 8
.LC217:
	.string	"TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256"
	.section	.rodata.str1.1
.LC218:
	.string	"ECDHE-ECDSA-CHACHA20-POLY1305"
	.section	.rodata.str1.8
	.align 8
.LC219:
	.string	"TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256"
	.section	.rodata.str1.1
.LC220:
	.string	"PSK-CHACHA20-POLY1305"
	.section	.rodata.str1.8
	.align 8
.LC221:
	.string	"TLS_PSK_WITH_CHACHA20_POLY1305_SHA256"
	.section	.rodata.str1.1
.LC222:
	.string	"ECDHE-PSK-CHACHA20-POLY1305"
	.section	.rodata.str1.8
	.align 8
.LC223:
	.string	"TLS_ECDHE_PSK_WITH_CHACHA20_POLY1305_SHA256"
	.section	.rodata.str1.1
.LC224:
	.string	"DHE-PSK-CHACHA20-POLY1305"
	.section	.rodata.str1.8
	.align 8
.LC225:
	.string	"TLS_DHE_PSK_WITH_CHACHA20_POLY1305_SHA256"
	.section	.rodata.str1.1
.LC226:
	.string	"RSA-PSK-CHACHA20-POLY1305"
	.section	.rodata.str1.8
	.align 8
.LC227:
	.string	"TLS_RSA_PSK_WITH_CHACHA20_POLY1305_SHA256"
	.section	.rodata.str1.1
.LC228:
	.string	"CAMELLIA128-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC229:
	.string	"TLS_RSA_WITH_CAMELLIA_128_CBC_SHA256"
	.section	.rodata.str1.1
.LC230:
	.string	"DHE-DSS-CAMELLIA128-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC231:
	.string	"TLS_DHE_DSS_WITH_CAMELLIA_128_CBC_SHA256"
	.section	.rodata.str1.1
.LC232:
	.string	"DHE-RSA-CAMELLIA128-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC233:
	.string	"TLS_DHE_RSA_WITH_CAMELLIA_128_CBC_SHA256"
	.section	.rodata.str1.1
.LC234:
	.string	"ADH-CAMELLIA128-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC235:
	.string	"TLS_DH_anon_WITH_CAMELLIA_128_CBC_SHA256"
	.section	.rodata.str1.1
.LC236:
	.string	"CAMELLIA256-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC237:
	.string	"TLS_RSA_WITH_CAMELLIA_256_CBC_SHA256"
	.section	.rodata.str1.1
.LC238:
	.string	"DHE-DSS-CAMELLIA256-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC239:
	.string	"TLS_DHE_DSS_WITH_CAMELLIA_256_CBC_SHA256"
	.section	.rodata.str1.1
.LC240:
	.string	"DHE-RSA-CAMELLIA256-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC241:
	.string	"TLS_DHE_RSA_WITH_CAMELLIA_256_CBC_SHA256"
	.section	.rodata.str1.1
.LC242:
	.string	"ADH-CAMELLIA256-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC243:
	.string	"TLS_DH_anon_WITH_CAMELLIA_256_CBC_SHA256"
	.section	.rodata.str1.1
.LC244:
	.string	"CAMELLIA256-SHA"
	.section	.rodata.str1.8
	.align 8
.LC245:
	.string	"TLS_RSA_WITH_CAMELLIA_256_CBC_SHA"
	.section	.rodata.str1.1
.LC246:
	.string	"DHE-DSS-CAMELLIA256-SHA"
	.section	.rodata.str1.8
	.align 8
.LC247:
	.string	"TLS_DHE_DSS_WITH_CAMELLIA_256_CBC_SHA"
	.section	.rodata.str1.1
.LC248:
	.string	"DHE-RSA-CAMELLIA256-SHA"
	.section	.rodata.str1.8
	.align 8
.LC249:
	.string	"TLS_DHE_RSA_WITH_CAMELLIA_256_CBC_SHA"
	.section	.rodata.str1.1
.LC250:
	.string	"ADH-CAMELLIA256-SHA"
	.section	.rodata.str1.8
	.align 8
.LC251:
	.string	"TLS_DH_anon_WITH_CAMELLIA_256_CBC_SHA"
	.section	.rodata.str1.1
.LC252:
	.string	"CAMELLIA128-SHA"
	.section	.rodata.str1.8
	.align 8
.LC253:
	.string	"TLS_RSA_WITH_CAMELLIA_128_CBC_SHA"
	.section	.rodata.str1.1
.LC254:
	.string	"DHE-DSS-CAMELLIA128-SHA"
	.section	.rodata.str1.8
	.align 8
.LC255:
	.string	"TLS_DHE_DSS_WITH_CAMELLIA_128_CBC_SHA"
	.section	.rodata.str1.1
.LC256:
	.string	"DHE-RSA-CAMELLIA128-SHA"
	.section	.rodata.str1.8
	.align 8
.LC257:
	.string	"TLS_DHE_RSA_WITH_CAMELLIA_128_CBC_SHA"
	.section	.rodata.str1.1
.LC258:
	.string	"ADH-CAMELLIA128-SHA"
	.section	.rodata.str1.8
	.align 8
.LC259:
	.string	"TLS_DH_anon_WITH_CAMELLIA_128_CBC_SHA"
	.align 8
.LC260:
	.string	"ECDHE-ECDSA-CAMELLIA128-SHA256"
	.align 8
.LC261:
	.string	"TLS_ECDHE_ECDSA_WITH_CAMELLIA_128_CBC_SHA256"
	.align 8
.LC262:
	.string	"ECDHE-ECDSA-CAMELLIA256-SHA384"
	.align 8
.LC263:
	.string	"TLS_ECDHE_ECDSA_WITH_CAMELLIA_256_CBC_SHA384"
	.section	.rodata.str1.1
.LC264:
	.string	"ECDHE-RSA-CAMELLIA128-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC265:
	.string	"TLS_ECDHE_RSA_WITH_CAMELLIA_128_CBC_SHA256"
	.section	.rodata.str1.1
.LC266:
	.string	"ECDHE-RSA-CAMELLIA256-SHA384"
	.section	.rodata.str1.8
	.align 8
.LC267:
	.string	"TLS_ECDHE_RSA_WITH_CAMELLIA_256_CBC_SHA384"
	.section	.rodata.str1.1
.LC268:
	.string	"PSK-CAMELLIA128-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC269:
	.string	"TLS_PSK_WITH_CAMELLIA_128_CBC_SHA256"
	.section	.rodata.str1.1
.LC270:
	.string	"PSK-CAMELLIA256-SHA384"
	.section	.rodata.str1.8
	.align 8
.LC271:
	.string	"TLS_PSK_WITH_CAMELLIA_256_CBC_SHA384"
	.section	.rodata.str1.1
.LC272:
	.string	"DHE-PSK-CAMELLIA128-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC273:
	.string	"TLS_DHE_PSK_WITH_CAMELLIA_128_CBC_SHA256"
	.section	.rodata.str1.1
.LC274:
	.string	"DHE-PSK-CAMELLIA256-SHA384"
	.section	.rodata.str1.8
	.align 8
.LC275:
	.string	"TLS_DHE_PSK_WITH_CAMELLIA_256_CBC_SHA384"
	.section	.rodata.str1.1
.LC276:
	.string	"RSA-PSK-CAMELLIA128-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC277:
	.string	"TLS_RSA_PSK_WITH_CAMELLIA_128_CBC_SHA256"
	.section	.rodata.str1.1
.LC278:
	.string	"RSA-PSK-CAMELLIA256-SHA384"
	.section	.rodata.str1.8
	.align 8
.LC279:
	.string	"TLS_RSA_PSK_WITH_CAMELLIA_256_CBC_SHA384"
	.section	.rodata.str1.1
.LC280:
	.string	"ECDHE-PSK-CAMELLIA128-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC281:
	.string	"TLS_ECDHE_PSK_WITH_CAMELLIA_128_CBC_SHA256"
	.section	.rodata.str1.1
.LC282:
	.string	"ECDHE-PSK-CAMELLIA256-SHA384"
	.section	.rodata.str1.8
	.align 8
.LC283:
	.string	"TLS_ECDHE_PSK_WITH_CAMELLIA_256_CBC_SHA384"
	.section	.rodata.str1.1
.LC284:
	.string	"IDEA-CBC-SHA"
.LC285:
	.string	"TLS_RSA_WITH_IDEA_CBC_SHA"
.LC286:
	.string	"SEED-SHA"
.LC287:
	.string	"TLS_RSA_WITH_SEED_CBC_SHA"
.LC288:
	.string	"DHE-DSS-SEED-SHA"
.LC289:
	.string	"TLS_DHE_DSS_WITH_SEED_CBC_SHA"
.LC290:
	.string	"DHE-RSA-SEED-SHA"
.LC291:
	.string	"TLS_DHE_RSA_WITH_SEED_CBC_SHA"
.LC292:
	.string	"ADH-SEED-SHA"
.LC293:
	.string	"TLS_DH_anon_WITH_SEED_CBC_SHA"
.LC294:
	.string	"ARIA128-GCM-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC295:
	.string	"TLS_RSA_WITH_ARIA_128_GCM_SHA256"
	.section	.rodata.str1.1
.LC296:
	.string	"ARIA256-GCM-SHA384"
	.section	.rodata.str1.8
	.align 8
.LC297:
	.string	"TLS_RSA_WITH_ARIA_256_GCM_SHA384"
	.section	.rodata.str1.1
.LC298:
	.string	"DHE-RSA-ARIA128-GCM-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC299:
	.string	"TLS_DHE_RSA_WITH_ARIA_128_GCM_SHA256"
	.section	.rodata.str1.1
.LC300:
	.string	"DHE-RSA-ARIA256-GCM-SHA384"
	.section	.rodata.str1.8
	.align 8
.LC301:
	.string	"TLS_DHE_RSA_WITH_ARIA_256_GCM_SHA384"
	.section	.rodata.str1.1
.LC302:
	.string	"DHE-DSS-ARIA128-GCM-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC303:
	.string	"TLS_DHE_DSS_WITH_ARIA_128_GCM_SHA256"
	.section	.rodata.str1.1
.LC304:
	.string	"DHE-DSS-ARIA256-GCM-SHA384"
	.section	.rodata.str1.8
	.align 8
.LC305:
	.string	"TLS_DHE_DSS_WITH_ARIA_256_GCM_SHA384"
	.align 8
.LC306:
	.string	"ECDHE-ECDSA-ARIA128-GCM-SHA256"
	.align 8
.LC307:
	.string	"TLS_ECDHE_ECDSA_WITH_ARIA_128_GCM_SHA256"
	.align 8
.LC308:
	.string	"ECDHE-ECDSA-ARIA256-GCM-SHA384"
	.align 8
.LC309:
	.string	"TLS_ECDHE_ECDSA_WITH_ARIA_256_GCM_SHA384"
	.section	.rodata.str1.1
.LC310:
	.string	"ECDHE-ARIA128-GCM-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC311:
	.string	"TLS_ECDHE_RSA_WITH_ARIA_128_GCM_SHA256"
	.section	.rodata.str1.1
.LC312:
	.string	"ECDHE-ARIA256-GCM-SHA384"
	.section	.rodata.str1.8
	.align 8
.LC313:
	.string	"TLS_ECDHE_RSA_WITH_ARIA_256_GCM_SHA384"
	.section	.rodata.str1.1
.LC314:
	.string	"PSK-ARIA128-GCM-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC315:
	.string	"TLS_PSK_WITH_ARIA_128_GCM_SHA256"
	.section	.rodata.str1.1
.LC316:
	.string	"PSK-ARIA256-GCM-SHA384"
	.section	.rodata.str1.8
	.align 8
.LC317:
	.string	"TLS_PSK_WITH_ARIA_256_GCM_SHA384"
	.section	.rodata.str1.1
.LC318:
	.string	"DHE-PSK-ARIA128-GCM-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC319:
	.string	"TLS_DHE_PSK_WITH_ARIA_128_GCM_SHA256"
	.section	.rodata.str1.1
.LC320:
	.string	"DHE-PSK-ARIA256-GCM-SHA384"
	.section	.rodata.str1.8
	.align 8
.LC321:
	.string	"TLS_DHE_PSK_WITH_ARIA_256_GCM_SHA384"
	.section	.rodata.str1.1
.LC322:
	.string	"RSA-PSK-ARIA128-GCM-SHA256"
	.section	.rodata.str1.8
	.align 8
.LC323:
	.string	"TLS_RSA_PSK_WITH_ARIA_128_GCM_SHA256"
	.section	.rodata.str1.1
.LC324:
	.string	"RSA-PSK-ARIA256-GCM-SHA384"
	.section	.rodata.str1.8
	.align 8
.LC325:
	.string	"TLS_RSA_PSK_WITH_ARIA_256_GCM_SHA384"
	.section	.data.rel.local
	.align 32
	.type	ssl3_ciphers, @object
	.size	ssl3_ciphers, 12800
ssl3_ciphers:
	.long	1
	.zero	4
	.quad	.LC6
	.quad	.LC7
	.long	50331649
	.long	1
	.long	1
	.long	32
	.long	1
	.long	768
	.long	771
	.long	256
	.long	65277
	.long	1
	.long	2313
	.long	0
	.long	0
	.zero	4
	.long	1
	.zero	4
	.quad	.LC8
	.quad	.LC9
	.long	50331650
	.long	1
	.long	1
	.long	32
	.long	2
	.long	768
	.long	771
	.long	256
	.long	65277
	.long	17
	.long	2313
	.long	0
	.long	0
	.zero	4
	.long	1
	.zero	4
	.quad	.LC10
	.quad	.LC11
	.long	50331695
	.long	1
	.long	1
	.long	64
	.long	2
	.long	768
	.long	771
	.long	256
	.long	65277
	.long	24
	.long	2313
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC12
	.quad	.LC13
	.long	50331698
	.long	2
	.long	2
	.long	64
	.long	2
	.long	768
	.long	771
	.long	256
	.long	65277
	.long	56
	.long	2313
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC14
	.quad	.LC15
	.long	50331699
	.long	2
	.long	1
	.long	64
	.long	2
	.long	768
	.long	771
	.long	256
	.long	65277
	.long	24
	.long	2313
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC16
	.quad	.LC17
	.long	50331700
	.long	2
	.long	4
	.long	64
	.long	2
	.long	768
	.long	771
	.long	256
	.long	65277
	.long	56
	.long	2313
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC18
	.quad	.LC19
	.long	50331701
	.long	1
	.long	1
	.long	128
	.long	2
	.long	768
	.long	771
	.long	256
	.long	65277
	.long	24
	.long	2313
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC20
	.quad	.LC21
	.long	50331704
	.long	2
	.long	2
	.long	128
	.long	2
	.long	768
	.long	771
	.long	256
	.long	65277
	.long	56
	.long	2313
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC22
	.quad	.LC23
	.long	50331705
	.long	2
	.long	1
	.long	128
	.long	2
	.long	768
	.long	771
	.long	256
	.long	65277
	.long	24
	.long	2313
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC24
	.quad	.LC25
	.long	50331706
	.long	2
	.long	4
	.long	128
	.long	2
	.long	768
	.long	771
	.long	256
	.long	65277
	.long	56
	.long	2313
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC26
	.quad	.LC27
	.long	50331707
	.long	1
	.long	1
	.long	32
	.long	16
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	17
	.long	2313
	.long	0
	.long	0
	.zero	4
	.long	1
	.zero	4
	.quad	.LC28
	.quad	.LC29
	.long	50331708
	.long	1
	.long	1
	.long	64
	.long	16
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	24
	.long	2313
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC30
	.quad	.LC31
	.long	50331709
	.long	1
	.long	1
	.long	128
	.long	16
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	24
	.long	2313
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC32
	.quad	.LC33
	.long	50331712
	.long	2
	.long	2
	.long	64
	.long	16
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	56
	.long	2313
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC34
	.quad	.LC35
	.long	50331751
	.long	2
	.long	1
	.long	64
	.long	16
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	24
	.long	2313
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC36
	.quad	.LC37
	.long	50331754
	.long	2
	.long	2
	.long	128
	.long	16
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	56
	.long	2313
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC38
	.quad	.LC39
	.long	50331755
	.long	2
	.long	1
	.long	128
	.long	16
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	24
	.long	2313
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC40
	.quad	.LC41
	.long	50331756
	.long	2
	.long	4
	.long	64
	.long	16
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	56
	.long	2313
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC42
	.quad	.LC43
	.long	50331757
	.long	2
	.long	4
	.long	128
	.long	16
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	56
	.long	2313
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC44
	.quad	.LC45
	.long	50331804
	.long	1
	.long	1
	.long	4096
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	24
	.long	1028
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC46
	.quad	.LC47
	.long	50331805
	.long	1
	.long	1
	.long	8192
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	24
	.long	1285
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC48
	.quad	.LC49
	.long	50331806
	.long	2
	.long	1
	.long	4096
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	24
	.long	1028
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC50
	.quad	.LC51
	.long	50331807
	.long	2
	.long	1
	.long	8192
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	24
	.long	1285
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC52
	.quad	.LC53
	.long	50331810
	.long	2
	.long	2
	.long	4096
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	56
	.long	1028
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC54
	.quad	.LC55
	.long	50331811
	.long	2
	.long	2
	.long	8192
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	56
	.long	1285
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC56
	.quad	.LC57
	.long	50331814
	.long	2
	.long	4
	.long	4096
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	56
	.long	1028
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC58
	.quad	.LC59
	.long	50331815
	.long	2
	.long	4
	.long	8192
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	56
	.long	1285
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC60
	.quad	.LC61
	.long	50380956
	.long	1
	.long	1
	.long	16384
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1028
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC62
	.quad	.LC63
	.long	50380957
	.long	1
	.long	1
	.long	32768
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1028
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC64
	.quad	.LC65
	.long	50380958
	.long	2
	.long	1
	.long	16384
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1028
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC66
	.quad	.LC67
	.long	50380959
	.long	2
	.long	1
	.long	32768
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1028
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC68
	.quad	.LC69
	.long	50380960
	.long	1
	.long	1
	.long	65536
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1028
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC70
	.quad	.LC71
	.long	50380961
	.long	1
	.long	1
	.long	131072
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1028
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC72
	.quad	.LC73
	.long	50380962
	.long	2
	.long	1
	.long	65536
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1028
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC74
	.quad	.LC75
	.long	50380963
	.long	2
	.long	1
	.long	131072
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1028
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC76
	.quad	.LC77
	.long	50380964
	.long	8
	.long	16
	.long	16384
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1028
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC78
	.quad	.LC79
	.long	50380965
	.long	8
	.long	16
	.long	32768
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1028
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC80
	.quad	.LC81
	.long	50380966
	.long	256
	.long	16
	.long	16384
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1028
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC82
	.quad	.LC83
	.long	50380967
	.long	256
	.long	16
	.long	32768
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1028
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC84
	.quad	.LC85
	.long	50380968
	.long	8
	.long	16
	.long	65536
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1028
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC86
	.quad	.LC87
	.long	50380969
	.long	8
	.long	16
	.long	131072
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1028
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC88
	.quad	.LC89
	.long	50380970
	.long	256
	.long	16
	.long	65536
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1028
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC90
	.quad	.LC91
	.long	50380971
	.long	256
	.long	16
	.long	131072
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1028
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC92
	.quad	.LC93
	.long	50380972
	.long	4
	.long	8
	.long	16384
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1028
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC94
	.quad	.LC95
	.long	50380973
	.long	4
	.long	8
	.long	32768
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1028
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC96
	.quad	.LC97
	.long	50380974
	.long	4
	.long	8
	.long	65536
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1028
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC98
	.quad	.LC99
	.long	50380975
	.long	4
	.long	8
	.long	131072
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1028
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC100
	.quad	.LC101
	.long	50380806
	.long	4
	.long	8
	.long	32
	.long	2
	.long	769
	.long	771
	.long	256
	.long	65277
	.long	17
	.long	2313
	.long	0
	.long	0
	.zero	4
	.long	1
	.zero	4
	.quad	.LC102
	.quad	.LC103
	.long	50380809
	.long	4
	.long	8
	.long	64
	.long	2
	.long	769
	.long	771
	.long	256
	.long	65277
	.long	24
	.long	2313
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC104
	.quad	.LC105
	.long	50380810
	.long	4
	.long	8
	.long	128
	.long	2
	.long	769
	.long	771
	.long	256
	.long	65277
	.long	24
	.long	2313
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC106
	.quad	.LC107
	.long	50380816
	.long	4
	.long	1
	.long	32
	.long	2
	.long	769
	.long	771
	.long	256
	.long	65277
	.long	17
	.long	2313
	.long	0
	.long	0
	.zero	4
	.long	1
	.zero	4
	.quad	.LC108
	.quad	.LC109
	.long	50380819
	.long	4
	.long	1
	.long	64
	.long	2
	.long	769
	.long	771
	.long	256
	.long	65277
	.long	24
	.long	2313
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC110
	.quad	.LC111
	.long	50380820
	.long	4
	.long	1
	.long	128
	.long	2
	.long	769
	.long	771
	.long	256
	.long	65277
	.long	24
	.long	2313
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC112
	.quad	.LC113
	.long	50380821
	.long	4
	.long	4
	.long	32
	.long	2
	.long	769
	.long	771
	.long	256
	.long	65277
	.long	17
	.long	2313
	.long	0
	.long	0
	.zero	4
	.long	1
	.zero	4
	.quad	.LC114
	.quad	.LC115
	.long	50380824
	.long	4
	.long	4
	.long	64
	.long	2
	.long	769
	.long	771
	.long	256
	.long	65277
	.long	56
	.long	2313
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC116
	.quad	.LC117
	.long	50380825
	.long	4
	.long	4
	.long	128
	.long	2
	.long	769
	.long	771
	.long	256
	.long	65277
	.long	56
	.long	2313
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC118
	.quad	.LC119
	.long	50380835
	.long	4
	.long	8
	.long	64
	.long	16
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	24
	.long	1028
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC120
	.quad	.LC121
	.long	50380836
	.long	4
	.long	8
	.long	128
	.long	32
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	24
	.long	1285
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC122
	.quad	.LC123
	.long	50380839
	.long	4
	.long	1
	.long	64
	.long	16
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	24
	.long	1028
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC124
	.quad	.LC125
	.long	50380840
	.long	4
	.long	1
	.long	128
	.long	32
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	24
	.long	1285
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC126
	.quad	.LC127
	.long	50380843
	.long	4
	.long	8
	.long	4096
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	24
	.long	1028
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC128
	.quad	.LC129
	.long	50380844
	.long	4
	.long	8
	.long	8192
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	24
	.long	1285
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC130
	.quad	.LC131
	.long	50380847
	.long	4
	.long	1
	.long	4096
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	24
	.long	1028
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC132
	.quad	.LC133
	.long	50380848
	.long	4
	.long	1
	.long	8192
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	24
	.long	1285
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC134
	.quad	.LC135
	.long	50331692
	.long	8
	.long	16
	.long	32
	.long	2
	.long	768
	.long	771
	.long	256
	.long	65277
	.long	17
	.long	2313
	.long	0
	.long	0
	.zero	4
	.long	1
	.zero	4
	.quad	.LC136
	.quad	.LC137
	.long	50331693
	.long	256
	.long	16
	.long	32
	.long	2
	.long	768
	.long	771
	.long	256
	.long	65277
	.long	17
	.long	2313
	.long	0
	.long	0
	.zero	4
	.long	1
	.zero	4
	.quad	.LC138
	.quad	.LC139
	.long	50331694
	.long	64
	.long	1
	.long	32
	.long	2
	.long	768
	.long	771
	.long	256
	.long	65277
	.long	17
	.long	2313
	.long	0
	.long	0
	.zero	4
	.long	1
	.zero	4
	.quad	.LC140
	.quad	.LC141
	.long	50331788
	.long	8
	.long	16
	.long	64
	.long	2
	.long	768
	.long	771
	.long	256
	.long	65277
	.long	24
	.long	2313
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC142
	.quad	.LC143
	.long	50331789
	.long	8
	.long	16
	.long	128
	.long	2
	.long	768
	.long	771
	.long	256
	.long	65277
	.long	24
	.long	2313
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC144
	.quad	.LC145
	.long	50331792
	.long	256
	.long	16
	.long	64
	.long	2
	.long	768
	.long	771
	.long	256
	.long	65277
	.long	24
	.long	2313
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC146
	.quad	.LC147
	.long	50331793
	.long	256
	.long	16
	.long	128
	.long	2
	.long	768
	.long	771
	.long	256
	.long	65277
	.long	24
	.long	2313
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC148
	.quad	.LC149
	.long	50331796
	.long	64
	.long	1
	.long	64
	.long	2
	.long	768
	.long	771
	.long	256
	.long	65277
	.long	24
	.long	2313
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC150
	.quad	.LC151
	.long	50331797
	.long	64
	.long	1
	.long	128
	.long	2
	.long	768
	.long	771
	.long	256
	.long	65277
	.long	24
	.long	2313
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC152
	.quad	.LC153
	.long	50331816
	.long	8
	.long	16
	.long	4096
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	24
	.long	1028
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC154
	.quad	.LC155
	.long	50331817
	.long	8
	.long	16
	.long	8192
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	24
	.long	1285
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC156
	.quad	.LC157
	.long	50331818
	.long	256
	.long	16
	.long	4096
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	24
	.long	1028
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC158
	.quad	.LC159
	.long	50331819
	.long	256
	.long	16
	.long	8192
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	24
	.long	1285
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC160
	.quad	.LC161
	.long	50331820
	.long	64
	.long	1
	.long	4096
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	24
	.long	1028
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC162
	.quad	.LC163
	.long	50331821
	.long	64
	.long	1
	.long	8192
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	24
	.long	1285
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC164
	.quad	.LC165
	.long	50331822
	.long	8
	.long	16
	.long	64
	.long	16
	.long	769
	.long	771
	.long	256
	.long	65277
	.long	24
	.long	2313
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC166
	.quad	.LC167
	.long	50331823
	.long	8
	.long	16
	.long	128
	.long	32
	.long	769
	.long	771
	.long	256
	.long	65277
	.long	24
	.long	1285
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC168
	.quad	.LC169
	.long	50331824
	.long	8
	.long	16
	.long	32
	.long	16
	.long	769
	.long	771
	.long	256
	.long	65277
	.long	17
	.long	2313
	.long	0
	.long	0
	.zero	4
	.long	1
	.zero	4
	.quad	.LC170
	.quad	.LC171
	.long	50331825
	.long	8
	.long	16
	.long	32
	.long	32
	.long	769
	.long	771
	.long	256
	.long	65277
	.long	17
	.long	1285
	.long	0
	.long	0
	.zero	4
	.long	1
	.zero	4
	.quad	.LC172
	.quad	.LC173
	.long	50331826
	.long	256
	.long	16
	.long	64
	.long	16
	.long	769
	.long	771
	.long	256
	.long	65277
	.long	24
	.long	2313
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC174
	.quad	.LC175
	.long	50331827
	.long	256
	.long	16
	.long	128
	.long	32
	.long	769
	.long	771
	.long	256
	.long	65277
	.long	24
	.long	1285
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC176
	.quad	.LC177
	.long	50331828
	.long	256
	.long	16
	.long	32
	.long	16
	.long	769
	.long	771
	.long	256
	.long	65277
	.long	17
	.long	2313
	.long	0
	.long	0
	.zero	4
	.long	1
	.zero	4
	.quad	.LC178
	.quad	.LC179
	.long	50331829
	.long	256
	.long	16
	.long	32
	.long	32
	.long	769
	.long	771
	.long	256
	.long	65277
	.long	17
	.long	1285
	.long	0
	.long	0
	.zero	4
	.long	1
	.zero	4
	.quad	.LC180
	.quad	.LC181
	.long	50331830
	.long	64
	.long	1
	.long	64
	.long	16
	.long	769
	.long	771
	.long	256
	.long	65277
	.long	24
	.long	2313
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC182
	.quad	.LC183
	.long	50331831
	.long	64
	.long	1
	.long	128
	.long	32
	.long	769
	.long	771
	.long	256
	.long	65277
	.long	24
	.long	1285
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC184
	.quad	.LC185
	.long	50331832
	.long	64
	.long	1
	.long	32
	.long	16
	.long	769
	.long	771
	.long	256
	.long	65277
	.long	17
	.long	2313
	.long	0
	.long	0
	.zero	4
	.long	1
	.zero	4
	.quad	.LC186
	.quad	.LC187
	.long	50331833
	.long	64
	.long	1
	.long	32
	.long	32
	.long	769
	.long	771
	.long	256
	.long	65277
	.long	17
	.long	1285
	.long	0
	.long	0
	.zero	4
	.long	1
	.zero	4
	.quad	.LC188
	.quad	.LC189
	.long	50380853
	.long	128
	.long	16
	.long	64
	.long	2
	.long	769
	.long	771
	.long	256
	.long	65277
	.long	24
	.long	2313
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC190
	.quad	.LC191
	.long	50380854
	.long	128
	.long	16
	.long	128
	.long	2
	.long	769
	.long	771
	.long	256
	.long	65277
	.long	24
	.long	2313
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC192
	.quad	.LC193
	.long	50380855
	.long	128
	.long	16
	.long	64
	.long	16
	.long	769
	.long	771
	.long	256
	.long	65277
	.long	24
	.long	2313
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC194
	.quad	.LC195
	.long	50380856
	.long	128
	.long	16
	.long	128
	.long	32
	.long	769
	.long	771
	.long	256
	.long	65277
	.long	24
	.long	1285
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC196
	.quad	.LC197
	.long	50380857
	.long	128
	.long	16
	.long	32
	.long	2
	.long	769
	.long	771
	.long	256
	.long	65277
	.long	17
	.long	2313
	.long	0
	.long	0
	.zero	4
	.long	1
	.zero	4
	.quad	.LC198
	.quad	.LC199
	.long	50380858
	.long	128
	.long	16
	.long	32
	.long	16
	.long	769
	.long	771
	.long	256
	.long	65277
	.long	17
	.long	2313
	.long	0
	.long	0
	.zero	4
	.long	1
	.zero	4
	.quad	.LC200
	.quad	.LC201
	.long	50380859
	.long	128
	.long	16
	.long	32
	.long	32
	.long	769
	.long	771
	.long	256
	.long	65277
	.long	17
	.long	1285
	.long	0
	.long	0
	.zero	4
	.long	1
	.zero	4
	.quad	.LC202
	.quad	.LC203
	.long	50380829
	.long	32
	.long	64
	.long	64
	.long	2
	.long	768
	.long	771
	.long	256
	.long	65277
	.long	8
	.long	2313
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC204
	.quad	.LC205
	.long	50380830
	.long	32
	.long	1
	.long	64
	.long	2
	.long	768
	.long	771
	.long	256
	.long	65277
	.long	8
	.long	2313
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC206
	.quad	.LC207
	.long	50380831
	.long	32
	.long	2
	.long	64
	.long	2
	.long	768
	.long	771
	.long	256
	.long	65277
	.long	40
	.long	2313
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC208
	.quad	.LC209
	.long	50380832
	.long	32
	.long	64
	.long	128
	.long	2
	.long	768
	.long	771
	.long	256
	.long	65277
	.long	8
	.long	2313
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC210
	.quad	.LC211
	.long	50380833
	.long	32
	.long	1
	.long	128
	.long	2
	.long	768
	.long	771
	.long	256
	.long	65277
	.long	8
	.long	2313
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC212
	.quad	.LC213
	.long	50380834
	.long	32
	.long	2
	.long	128
	.long	2
	.long	768
	.long	771
	.long	256
	.long	65277
	.long	40
	.long	2313
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC214
	.quad	.LC215
	.long	50384042
	.long	2
	.long	1
	.long	524288
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	8
	.long	1028
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC216
	.quad	.LC217
	.long	50384040
	.long	4
	.long	1
	.long	524288
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	8
	.long	1028
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC218
	.quad	.LC219
	.long	50384041
	.long	4
	.long	8
	.long	524288
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	8
	.long	1028
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC220
	.quad	.LC221
	.long	50384043
	.long	8
	.long	16
	.long	524288
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	8
	.long	1028
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC222
	.quad	.LC223
	.long	50384044
	.long	128
	.long	16
	.long	524288
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	8
	.long	1028
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC224
	.quad	.LC225
	.long	50384045
	.long	256
	.long	16
	.long	524288
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	8
	.long	1028
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC226
	.quad	.LC227
	.long	50384046
	.long	64
	.long	1
	.long	524288
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	8
	.long	1028
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC228
	.quad	.LC229
	.long	50331834
	.long	1
	.long	1
	.long	256
	.long	16
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1028
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC230
	.quad	.LC231
	.long	50331837
	.long	2
	.long	2
	.long	256
	.long	16
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1028
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC232
	.quad	.LC233
	.long	50331838
	.long	2
	.long	1
	.long	256
	.long	16
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1028
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC234
	.quad	.LC235
	.long	50331839
	.long	2
	.long	4
	.long	256
	.long	16
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1028
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC236
	.quad	.LC237
	.long	50331840
	.long	1
	.long	1
	.long	512
	.long	16
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1028
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC238
	.quad	.LC239
	.long	50331843
	.long	2
	.long	2
	.long	512
	.long	16
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1028
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC240
	.quad	.LC241
	.long	50331844
	.long	2
	.long	1
	.long	512
	.long	16
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1028
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC242
	.quad	.LC243
	.long	50331845
	.long	2
	.long	4
	.long	512
	.long	16
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1028
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC244
	.quad	.LC245
	.long	50331780
	.long	1
	.long	1
	.long	512
	.long	2
	.long	768
	.long	771
	.long	256
	.long	65277
	.long	40
	.long	2313
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC246
	.quad	.LC247
	.long	50331783
	.long	2
	.long	2
	.long	512
	.long	2
	.long	768
	.long	771
	.long	256
	.long	65277
	.long	40
	.long	2313
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC248
	.quad	.LC249
	.long	50331784
	.long	2
	.long	1
	.long	512
	.long	2
	.long	768
	.long	771
	.long	256
	.long	65277
	.long	40
	.long	2313
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC250
	.quad	.LC251
	.long	50331785
	.long	2
	.long	4
	.long	512
	.long	2
	.long	768
	.long	771
	.long	256
	.long	65277
	.long	40
	.long	2313
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC252
	.quad	.LC253
	.long	50331713
	.long	1
	.long	1
	.long	256
	.long	2
	.long	768
	.long	771
	.long	256
	.long	65277
	.long	40
	.long	2313
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC254
	.quad	.LC255
	.long	50331716
	.long	2
	.long	2
	.long	256
	.long	2
	.long	768
	.long	771
	.long	256
	.long	65277
	.long	40
	.long	2313
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC256
	.quad	.LC257
	.long	50331717
	.long	2
	.long	1
	.long	256
	.long	2
	.long	768
	.long	771
	.long	256
	.long	65277
	.long	40
	.long	2313
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC258
	.quad	.LC259
	.long	50331718
	.long	2
	.long	4
	.long	256
	.long	2
	.long	768
	.long	771
	.long	256
	.long	65277
	.long	40
	.long	2313
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC260
	.quad	.LC261
	.long	50380914
	.long	4
	.long	8
	.long	256
	.long	16
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1028
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC262
	.quad	.LC263
	.long	50380915
	.long	4
	.long	8
	.long	512
	.long	32
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1285
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC264
	.quad	.LC265
	.long	50380918
	.long	4
	.long	1
	.long	256
	.long	16
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1028
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC266
	.quad	.LC267
	.long	50380919
	.long	4
	.long	1
	.long	512
	.long	32
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1285
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC268
	.quad	.LC269
	.long	50380948
	.long	8
	.long	16
	.long	256
	.long	16
	.long	769
	.long	771
	.long	256
	.long	65277
	.long	40
	.long	2313
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC270
	.quad	.LC271
	.long	50380949
	.long	8
	.long	16
	.long	512
	.long	32
	.long	769
	.long	771
	.long	256
	.long	65277
	.long	40
	.long	1285
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC272
	.quad	.LC273
	.long	50380950
	.long	256
	.long	16
	.long	256
	.long	16
	.long	769
	.long	771
	.long	256
	.long	65277
	.long	40
	.long	2313
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC274
	.quad	.LC275
	.long	50380951
	.long	256
	.long	16
	.long	512
	.long	32
	.long	769
	.long	771
	.long	256
	.long	65277
	.long	40
	.long	1285
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC276
	.quad	.LC277
	.long	50380952
	.long	64
	.long	1
	.long	256
	.long	16
	.long	769
	.long	771
	.long	256
	.long	65277
	.long	40
	.long	2313
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC278
	.quad	.LC279
	.long	50380953
	.long	64
	.long	1
	.long	512
	.long	32
	.long	769
	.long	771
	.long	256
	.long	65277
	.long	40
	.long	1285
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC280
	.quad	.LC281
	.long	50380954
	.long	128
	.long	16
	.long	256
	.long	16
	.long	769
	.long	771
	.long	256
	.long	65277
	.long	40
	.long	2313
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC282
	.quad	.LC283
	.long	50380955
	.long	128
	.long	16
	.long	512
	.long	32
	.long	769
	.long	771
	.long	256
	.long	65277
	.long	40
	.long	1285
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC284
	.quad	.LC285
	.long	50331655
	.long	1
	.long	1
	.long	16
	.long	2
	.long	768
	.long	770
	.long	256
	.long	65279
	.long	36
	.long	2313
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC286
	.quad	.LC287
	.long	50331798
	.long	1
	.long	1
	.long	2048
	.long	2
	.long	768
	.long	771
	.long	256
	.long	65277
	.long	36
	.long	2313
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC288
	.quad	.LC289
	.long	50331801
	.long	2
	.long	2
	.long	2048
	.long	2
	.long	768
	.long	771
	.long	256
	.long	65277
	.long	36
	.long	2313
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC290
	.quad	.LC291
	.long	50331802
	.long	2
	.long	1
	.long	2048
	.long	2
	.long	768
	.long	771
	.long	256
	.long	65277
	.long	36
	.long	2313
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC292
	.quad	.LC293
	.long	50331803
	.long	2
	.long	4
	.long	2048
	.long	2
	.long	768
	.long	771
	.long	256
	.long	65277
	.long	36
	.long	2313
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC294
	.quad	.LC295
	.long	50380880
	.long	1
	.long	1
	.long	1048576
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1028
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC296
	.quad	.LC297
	.long	50380881
	.long	1
	.long	1
	.long	2097152
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1285
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC298
	.quad	.LC299
	.long	50380882
	.long	2
	.long	1
	.long	1048576
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1028
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC300
	.quad	.LC301
	.long	50380883
	.long	2
	.long	1
	.long	2097152
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1285
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC302
	.quad	.LC303
	.long	50380886
	.long	2
	.long	2
	.long	1048576
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1028
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC304
	.quad	.LC305
	.long	50380887
	.long	2
	.long	2
	.long	2097152
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1285
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC306
	.quad	.LC307
	.long	50380892
	.long	4
	.long	8
	.long	1048576
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1028
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC308
	.quad	.LC309
	.long	50380893
	.long	4
	.long	8
	.long	2097152
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1285
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC310
	.quad	.LC311
	.long	50380896
	.long	4
	.long	1
	.long	1048576
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1028
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC312
	.quad	.LC313
	.long	50380897
	.long	4
	.long	1
	.long	2097152
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1285
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC314
	.quad	.LC315
	.long	50380906
	.long	8
	.long	16
	.long	1048576
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1028
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC316
	.quad	.LC317
	.long	50380907
	.long	8
	.long	16
	.long	2097152
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1285
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC318
	.quad	.LC319
	.long	50380908
	.long	256
	.long	16
	.long	1048576
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1028
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC320
	.quad	.LC321
	.long	50380909
	.long	256
	.long	16
	.long	2097152
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1285
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC322
	.quad	.LC323
	.long	50380910
	.long	64
	.long	1
	.long	1048576
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1028
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC324
	.quad	.LC325
	.long	50380911
	.long	64
	.long	1
	.long	2097152
	.long	64
	.long	771
	.long	771
	.long	65277
	.long	65277
	.long	40
	.long	1285
	.long	256
	.long	256
	.zero	4
	.section	.rodata.str1.1
.LC326:
	.string	"TLS_AES_128_GCM_SHA256"
.LC327:
	.string	"TLS_AES_256_GCM_SHA384"
.LC328:
	.string	"TLS_CHACHA20_POLY1305_SHA256"
.LC329:
	.string	"TLS_AES_128_CCM_SHA256"
.LC330:
	.string	"TLS_AES_128_CCM_8_SHA256"
	.section	.data.rel.local
	.align 32
	.type	tls13_ciphers, @object
	.size	tls13_ciphers, 400
tls13_ciphers:
	.long	1
	.zero	4
	.quad	.LC326
	.quad	.LC326
	.long	50336513
	.long	0
	.long	0
	.long	4096
	.long	64
	.long	772
	.long	772
	.long	0
	.long	0
	.long	8
	.long	4
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC327
	.quad	.LC327
	.long	50336514
	.long	0
	.long	0
	.long	8192
	.long	64
	.long	772
	.long	772
	.long	0
	.long	0
	.long	8
	.long	5
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC328
	.quad	.LC328
	.long	50336515
	.long	0
	.long	0
	.long	524288
	.long	64
	.long	772
	.long	772
	.long	0
	.long	0
	.long	8
	.long	4
	.long	256
	.long	256
	.zero	4
	.long	1
	.zero	4
	.quad	.LC329
	.quad	.LC329
	.long	50336516
	.long	0
	.long	0
	.long	16384
	.long	64
	.long	772
	.long	772
	.long	0
	.long	0
	.long	40
	.long	4
	.long	128
	.long	128
	.zero	4
	.long	1
	.zero	4
	.quad	.LC330
	.quad	.LC330
	.long	50336517
	.long	0
	.long	0
	.long	65536
	.long	64
	.long	772
	.long	772
	.long	0
	.long	0
	.long	40
	.long	4
	.long	128
	.long	128
	.zero	4
	.globl	tls12downgrade
	.section	.rodata
	.align 8
	.type	tls12downgrade, @object
	.size	tls12downgrade, 8
tls12downgrade:
	.ascii	"DOWNGRD\001"
	.globl	tls11downgrade
	.align 8
	.type	tls11downgrade, @object
	.size	tls11downgrade, 8
tls11downgrade:
	.string	"DOWNGRD"
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.quad	5
	.quad	160
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
