	.file	"cms_asn1.c"
	.text
	.p2align 4
	.type	cms_rek_cb, @function
cms_rek_cb:
.LFB1380:
	.cfi_startproc
	endbr64
	cmpl	$3, %edi
	je	.L8
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rsi), %rax
	movq	16(%rax), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	EVP_PKEY_free@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1380:
	.size	cms_rek_cb, .-cms_rek_cb
	.p2align 4
	.type	cms_kari_cb, @function
cms_kari_cb:
.LFB1381:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	(%rsi), %rbx
	cmpl	$1, %edi
	je	.L16
	movl	$1, %eax
	cmpl	$3, %edi
	je	.L17
.L9:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	call	EVP_CIPHER_CTX_new@PLT
	movq	%rax, %rdi
	movq	%rax, 48(%rbx)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L9
	movl	$1, %esi
	call	EVP_CIPHER_CTX_set_flags@PLT
	movq	$0, 40(%rbx)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movq	40(%rbx), %rdi
	movl	%eax, -20(%rbp)
	call	EVP_PKEY_CTX_free@PLT
	movq	48(%rbx), %rdi
	call	EVP_CIPHER_CTX_free@PLT
	movl	-20(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1381:
	.size	cms_kari_cb, .-cms_kari_cb
	.p2align 4
	.type	cms_cb, @function
cms_cb:
.LFB1383:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L24
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rcx, %rbx
	movq	(%rsi), %r12
	cmpl	$12, %edi
	je	.L20
	jg	.L21
	cmpl	$10, %edi
	je	.L22
	cmpl	$11, %edi
	jne	.L39
.L23:
	movq	8(%rbx), %rsi
	movq	%r12, %rdi
	call	CMS_dataFinal@PLT
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
.L18:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	cmpl	$13, %edi
	je	.L23
.L39:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	leaq	16(%rcx), %rdi
	movq	%r12, %rsi
	call	CMS_stream@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L18
.L20:
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	CMS_dataInit@PLT
	testq	%rax, %rax
	movq	%rax, 8(%rbx)
	popq	%rbx
	setne	%al
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1383:
	.size	cms_cb, .-cms_cb
	.p2align 4
	.type	cms_si_cb, @function
cms_si_cb:
.LFB1379:
	.cfi_startproc
	endbr64
	cmpl	$3, %edi
	je	.L46
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rsi), %rbx
	movq	64(%rbx), %rdi
	call	EVP_PKEY_free@PLT
	movq	56(%rbx), %rdi
	call	X509_free@PLT
	movq	72(%rbx), %rdi
	call	EVP_MD_CTX_free@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1379:
	.size	cms_si_cb, .-cms_si_cb
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/cms/cms_asn1.c"
	.text
	.p2align 4
	.type	cms_ri_cb, @function
cms_ri_cb:
.LFB1382:
	.cfi_startproc
	endbr64
	cmpl	$2, %edi
	je	.L55
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rsi), %rdx
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L56
	cmpl	$2, %eax
	je	.L57
	cmpl	$3, %eax
	je	.L58
.L48:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	movq	8(%rdx), %rbx
	movq	40(%rbx), %rdi
	call	EVP_PKEY_free@PLT
	movq	32(%rbx), %rdi
	call	X509_free@PLT
	movq	48(%rbx), %rdi
	call	EVP_PKEY_CTX_free@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	movq	8(%rdx), %rax
	movl	$210, %ecx
	leaq	.LC0(%rip), %rdx
	movq	40(%rax), %rsi
	movq	32(%rax), %rdi
	call	CRYPTO_clear_free@PLT
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L58:
	movq	8(%rdx), %rax
	movl	$213, %ecx
	leaq	.LC0(%rip), %rdx
	movq	40(%rax), %rsi
	movq	32(%rax), %rdi
	call	CRYPTO_clear_free@PLT
	jmp	.L48
	.cfi_endproc
.LFE1382:
	.size	cms_ri_cb, .-cms_ri_cb
	.p2align 4
	.globl	CMS_SharedInfo_encode
	.type	CMS_SharedInfo_encode, @function
CMS_SharedInfo_encode:
.LFB1384:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	sall	$3, %ecx
	punpcklqdq	%xmm1, %xmm0
	leaq	CMS_SharedInfo_it(%rip), %rdx
	movq	%r8, %rsi
	bswap	%ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-12(%rbp), %rax
	leaq	-48(%rbp), %rdi
	movl	%ecx, -12(%rbp)
	movq	%rax, -72(%rbp)
	movabsq	$17179869188, %rax
	movq	%rax, -80(%rbp)
	leaq	-80(%rbp), %rax
	movq	$0, -64(%rbp)
	movq	%rax, -32(%rbp)
	movaps	%xmm0, -48(%rbp)
	call	ASN1_item_i2d@PLT
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L62
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L62:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1384:
	.size	CMS_SharedInfo_encode, .-CMS_SharedInfo_encode
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"CMS_SharedInfo"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	CMS_SharedInfo_it, @object
	.size	CMS_SharedInfo_it, 56
CMS_SharedInfo_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	CMS_SharedInfo_seq_tt
	.quad	3
	.quad	0
	.quad	24
	.quad	.LC1
	.section	.rodata.str1.1
.LC2:
	.string	"keyInfo"
.LC3:
	.string	"entityUInfo"
.LC4:
	.string	"suppPubInfo"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	CMS_SharedInfo_seq_tt, @object
	.size	CMS_SharedInfo_seq_tt, 120
CMS_SharedInfo_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC2
	.quad	X509_ALGOR_it
	.quad	145
	.quad	0
	.quad	8
	.quad	.LC3
	.quad	ASN1_OCTET_STRING_it
	.quad	145
	.quad	2
	.quad	16
	.quad	.LC4
	.quad	ASN1_OCTET_STRING_it
	.globl	CMS_Receipt_it
	.section	.rodata.str1.1
.LC5:
	.string	"CMS_Receipt"
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_Receipt_it, @object
	.size	CMS_Receipt_it, 56
CMS_Receipt_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	CMS_Receipt_seq_tt
	.quad	4
	.quad	0
	.quad	32
	.quad	.LC5
	.section	.rodata.str1.1
.LC6:
	.string	"version"
.LC7:
	.string	"contentType"
.LC8:
	.string	"signedContentIdentifier"
.LC9:
	.string	"originatorSignatureValue"
	.section	.data.rel.ro
	.align 32
	.type	CMS_Receipt_seq_tt, @object
	.size	CMS_Receipt_seq_tt, 160
CMS_Receipt_seq_tt:
	.quad	4096
	.quad	0
	.quad	0
	.quad	.LC6
	.quad	INT32_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC7
	.quad	ASN1_OBJECT_it
	.quad	0
	.quad	0
	.quad	16
	.quad	.LC8
	.quad	ASN1_OCTET_STRING_it
	.quad	0
	.quad	0
	.quad	24
	.quad	.LC9
	.quad	ASN1_OCTET_STRING_it
	.globl	CMS_ReceiptRequest_it
	.section	.rodata.str1.1
.LC10:
	.string	"CMS_ReceiptRequest"
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_ReceiptRequest_it, @object
	.size	CMS_ReceiptRequest_it, 56
CMS_ReceiptRequest_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	CMS_ReceiptRequest_seq_tt
	.quad	3
	.quad	0
	.quad	24
	.quad	.LC10
	.section	.rodata.str1.1
.LC11:
	.string	"receiptsFrom"
.LC12:
	.string	"receiptsTo"
	.section	.data.rel.ro
	.align 32
	.type	CMS_ReceiptRequest_seq_tt, @object
	.size	CMS_ReceiptRequest_seq_tt, 120
CMS_ReceiptRequest_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC8
	.quad	ASN1_OCTET_STRING_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC11
	.quad	CMS_ReceiptsFrom_it
	.quad	4
	.quad	0
	.quad	16
	.quad	.LC12
	.quad	GENERAL_NAMES_it
	.section	.rodata.str1.1
.LC13:
	.string	"CMS_ReceiptsFrom"
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_ReceiptsFrom_it, @object
	.size	CMS_ReceiptsFrom_it, 56
CMS_ReceiptsFrom_it:
	.byte	2
	.zero	7
	.quad	0
	.quad	CMS_ReceiptsFrom_ch_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC13
	.section	.rodata.str1.1
.LC14:
	.string	"d.allOrFirstTier"
.LC15:
	.string	"d.receiptList"
	.section	.data.rel.ro
	.align 32
	.type	CMS_ReceiptsFrom_ch_tt, @object
	.size	CMS_ReceiptsFrom_ch_tt, 80
CMS_ReceiptsFrom_ch_tt:
	.quad	4232
	.quad	0
	.quad	8
	.quad	.LC14
	.quad	INT32_it
	.quad	140
	.quad	1
	.quad	8
	.quad	.LC15
	.quad	GENERAL_NAMES_it
	.globl	CMS_Attributes_Verify_it
	.section	.rodata.str1.1
.LC16:
	.string	"CMS_Attributes_Verify"
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_Attributes_Verify_it, @object
	.size	CMS_Attributes_Verify_it, 56
CMS_Attributes_Verify_it:
	.byte	0
	.zero	7
	.quad	-1
	.quad	CMS_Attributes_Verify_item_tt
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC16
	.section	.rodata.str1.1
.LC17:
	.string	"CMS_ATTRIBUTES"
	.section	.data.rel.ro
	.align 32
	.type	CMS_Attributes_Verify_item_tt, @object
	.size	CMS_Attributes_Verify_item_tt, 40
CMS_Attributes_Verify_item_tt:
	.quad	12
	.quad	17
	.quad	0
	.quad	.LC17
	.quad	X509_ATTRIBUTE_it
	.globl	CMS_Attributes_Sign_it
	.section	.rodata.str1.1
.LC18:
	.string	"CMS_Attributes_Sign"
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_Attributes_Sign_it, @object
	.size	CMS_Attributes_Sign_it, 56
CMS_Attributes_Sign_it:
	.byte	0
	.zero	7
	.quad	-1
	.quad	CMS_Attributes_Sign_item_tt
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC18
	.section	.data.rel.ro
	.align 32
	.type	CMS_Attributes_Sign_item_tt, @object
	.size	CMS_Attributes_Sign_item_tt, 40
CMS_Attributes_Sign_item_tt:
	.quad	6
	.quad	0
	.quad	0
	.quad	.LC17
	.quad	X509_ATTRIBUTE_it
	.globl	CMS_ContentInfo_it
	.section	.rodata.str1.1
.LC19:
	.string	"CMS_ContentInfo"
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_ContentInfo_it, @object
	.size	CMS_ContentInfo_it, 56
CMS_ContentInfo_it:
	.byte	6
	.zero	7
	.quad	16
	.quad	CMS_ContentInfo_seq_tt
	.quad	2
	.quad	CMS_ContentInfo_aux
	.quad	16
	.quad	.LC19
	.section	.data.rel.ro
	.align 32
	.type	CMS_ContentInfo_seq_tt, @object
	.size	CMS_ContentInfo_seq_tt, 80
CMS_ContentInfo_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC7
	.quad	ASN1_OBJECT_it
	.quad	256
	.quad	-1
	.quad	0
	.quad	.LC19
	.quad	CMS_ContentInfo_adb
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_ContentInfo_aux, @object
	.size	CMS_ContentInfo_aux, 40
CMS_ContentInfo_aux:
	.quad	0
	.long	0
	.long	0
	.long	0
	.zero	4
	.quad	cms_cb
	.long	0
	.zero	4
	.align 32
	.type	CMS_ContentInfo_adb, @object
	.size	CMS_ContentInfo_adb, 56
CMS_ContentInfo_adb:
	.quad	0
	.quad	0
	.quad	0
	.quad	CMS_ContentInfo_adbtbl
	.quad	7
	.quad	cms_default_tt
	.quad	0
	.section	.rodata.str1.1
.LC20:
	.string	"d.data"
.LC21:
	.string	"d.signedData"
.LC22:
	.string	"d.envelopedData"
.LC23:
	.string	"d.digestedData"
.LC24:
	.string	"d.encryptedData"
.LC25:
	.string	"d.authenticatedData"
.LC26:
	.string	"d.compressedData"
	.section	.data.rel.ro
	.align 32
	.type	CMS_ContentInfo_adbtbl, @object
	.size	CMS_ContentInfo_adbtbl, 336
CMS_ContentInfo_adbtbl:
	.quad	21
	.quad	2192
	.quad	0
	.quad	8
	.quad	.LC20
	.quad	ASN1_OCTET_STRING_NDEF_it
	.quad	22
	.quad	2192
	.quad	0
	.quad	8
	.quad	.LC21
	.quad	CMS_SignedData_it
	.quad	23
	.quad	2192
	.quad	0
	.quad	8
	.quad	.LC22
	.quad	CMS_EnvelopedData_it
	.quad	25
	.quad	2192
	.quad	0
	.quad	8
	.quad	.LC23
	.quad	CMS_DigestedData_it
	.quad	26
	.quad	2192
	.quad	0
	.quad	8
	.quad	.LC24
	.quad	CMS_EncryptedData_it
	.quad	205
	.quad	2192
	.quad	0
	.quad	8
	.quad	.LC25
	.quad	CMS_AuthenticatedData_it
	.quad	786
	.quad	2192
	.quad	0
	.quad	8
	.quad	.LC26
	.quad	CMS_CompressedData_it
	.section	.rodata.str1.1
.LC27:
	.string	"d.other"
	.section	.data.rel.ro
	.align 32
	.type	cms_default_tt, @object
	.size	cms_default_tt, 40
cms_default_tt:
	.quad	144
	.quad	0
	.quad	8
	.quad	.LC27
	.quad	ASN1_ANY_it
	.globl	CMS_CompressedData_it
	.section	.rodata.str1.1
.LC28:
	.string	"CMS_CompressedData"
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_CompressedData_it, @object
	.size	CMS_CompressedData_it, 56
CMS_CompressedData_it:
	.byte	6
	.zero	7
	.quad	16
	.quad	CMS_CompressedData_seq_tt
	.quad	3
	.quad	0
	.quad	32
	.quad	.LC28
	.section	.rodata.str1.1
.LC29:
	.string	"compressionAlgorithm"
.LC30:
	.string	"encapContentInfo"
	.section	.data.rel.ro
	.align 32
	.type	CMS_CompressedData_seq_tt, @object
	.size	CMS_CompressedData_seq_tt, 120
CMS_CompressedData_seq_tt:
	.quad	4096
	.quad	0
	.quad	0
	.quad	.LC6
	.quad	INT32_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC29
	.quad	X509_ALGOR_it
	.quad	0
	.quad	0
	.quad	24
	.quad	.LC30
	.quad	CMS_EncapsulatedContentInfo_it
	.section	.rodata.str1.1
.LC31:
	.string	"CMS_AuthenticatedData"
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_AuthenticatedData_it, @object
	.size	CMS_AuthenticatedData_it, 56
CMS_AuthenticatedData_it:
	.byte	6
	.zero	7
	.quad	16
	.quad	CMS_AuthenticatedData_seq_tt
	.quad	9
	.quad	0
	.quad	72
	.quad	.LC31
	.section	.rodata.str1.1
.LC32:
	.string	"originatorInfo"
.LC33:
	.string	"recipientInfos"
.LC34:
	.string	"macAlgorithm"
.LC35:
	.string	"digestAlgorithm"
.LC36:
	.string	"authAttrs"
.LC37:
	.string	"mac"
.LC38:
	.string	"unauthAttrs"
	.section	.data.rel.ro
	.align 32
	.type	CMS_AuthenticatedData_seq_tt, @object
	.size	CMS_AuthenticatedData_seq_tt, 360
CMS_AuthenticatedData_seq_tt:
	.quad	4096
	.quad	0
	.quad	0
	.quad	.LC6
	.quad	INT32_it
	.quad	137
	.quad	0
	.quad	8
	.quad	.LC32
	.quad	CMS_OriginatorInfo_it
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC33
	.quad	CMS_RecipientInfo_it
	.quad	0
	.quad	0
	.quad	24
	.quad	.LC34
	.quad	X509_ALGOR_it
	.quad	136
	.quad	1
	.quad	32
	.quad	.LC35
	.quad	X509_ALGOR_it
	.quad	0
	.quad	0
	.quad	40
	.quad	.LC30
	.quad	CMS_EncapsulatedContentInfo_it
	.quad	139
	.quad	2
	.quad	48
	.quad	.LC36
	.quad	X509_ALGOR_it
	.quad	0
	.quad	0
	.quad	56
	.quad	.LC37
	.quad	ASN1_OCTET_STRING_it
	.quad	139
	.quad	3
	.quad	64
	.quad	.LC38
	.quad	X509_ALGOR_it
	.globl	CMS_EncryptedData_it
	.section	.rodata.str1.1
.LC39:
	.string	"CMS_EncryptedData"
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_EncryptedData_it, @object
	.size	CMS_EncryptedData_it, 56
CMS_EncryptedData_it:
	.byte	6
	.zero	7
	.quad	16
	.quad	CMS_EncryptedData_seq_tt
	.quad	3
	.quad	0
	.quad	24
	.quad	.LC39
	.section	.rodata.str1.1
.LC40:
	.string	"encryptedContentInfo"
.LC41:
	.string	"unprotectedAttrs"
	.section	.data.rel.ro
	.align 32
	.type	CMS_EncryptedData_seq_tt, @object
	.size	CMS_EncryptedData_seq_tt, 120
CMS_EncryptedData_seq_tt:
	.quad	4096
	.quad	0
	.quad	0
	.quad	.LC6
	.quad	INT32_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC40
	.quad	CMS_EncryptedContentInfo_it
	.quad	139
	.quad	1
	.quad	16
	.quad	.LC41
	.quad	X509_ATTRIBUTE_it
	.globl	CMS_DigestedData_it
	.section	.rodata.str1.1
.LC42:
	.string	"CMS_DigestedData"
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_DigestedData_it, @object
	.size	CMS_DigestedData_it, 56
CMS_DigestedData_it:
	.byte	6
	.zero	7
	.quad	16
	.quad	CMS_DigestedData_seq_tt
	.quad	4
	.quad	0
	.quad	32
	.quad	.LC42
	.section	.rodata.str1.1
.LC43:
	.string	"digest"
	.section	.data.rel.ro
	.align 32
	.type	CMS_DigestedData_seq_tt, @object
	.size	CMS_DigestedData_seq_tt, 160
CMS_DigestedData_seq_tt:
	.quad	4096
	.quad	0
	.quad	0
	.quad	.LC6
	.quad	INT32_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC35
	.quad	X509_ALGOR_it
	.quad	0
	.quad	0
	.quad	16
	.quad	.LC30
	.quad	CMS_EncapsulatedContentInfo_it
	.quad	0
	.quad	0
	.quad	24
	.quad	.LC43
	.quad	ASN1_OCTET_STRING_it
	.globl	CMS_EnvelopedData_it
	.section	.rodata.str1.1
.LC44:
	.string	"CMS_EnvelopedData"
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_EnvelopedData_it, @object
	.size	CMS_EnvelopedData_it, 56
CMS_EnvelopedData_it:
	.byte	6
	.zero	7
	.quad	16
	.quad	CMS_EnvelopedData_seq_tt
	.quad	5
	.quad	0
	.quad	40
	.quad	.LC44
	.section	.data.rel.ro
	.align 32
	.type	CMS_EnvelopedData_seq_tt, @object
	.size	CMS_EnvelopedData_seq_tt, 200
CMS_EnvelopedData_seq_tt:
	.quad	4096
	.quad	0
	.quad	0
	.quad	.LC6
	.quad	INT32_it
	.quad	137
	.quad	0
	.quad	8
	.quad	.LC32
	.quad	CMS_OriginatorInfo_it
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC33
	.quad	CMS_RecipientInfo_it
	.quad	0
	.quad	0
	.quad	24
	.quad	.LC40
	.quad	CMS_EncryptedContentInfo_it
	.quad	139
	.quad	1
	.quad	32
	.quad	.LC41
	.quad	X509_ATTRIBUTE_it
	.globl	CMS_RecipientInfo_it
	.section	.rodata.str1.1
.LC45:
	.string	"CMS_RecipientInfo"
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_RecipientInfo_it, @object
	.size	CMS_RecipientInfo_it, 56
CMS_RecipientInfo_it:
	.byte	2
	.zero	7
	.quad	0
	.quad	CMS_RecipientInfo_ch_tt
	.quad	5
	.quad	CMS_RecipientInfo_aux
	.quad	16
	.quad	.LC45
	.section	.rodata.str1.1
.LC46:
	.string	"d.ktri"
.LC47:
	.string	"d.kari"
.LC48:
	.string	"d.kekri"
.LC49:
	.string	"d.pwri"
.LC50:
	.string	"d.ori"
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_RecipientInfo_ch_tt, @object
	.size	CMS_RecipientInfo_ch_tt, 200
CMS_RecipientInfo_ch_tt:
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC46
	.quad	CMS_KeyTransRecipientInfo_it
	.quad	136
	.quad	1
	.quad	8
	.quad	.LC47
	.quad	CMS_KeyAgreeRecipientInfo_it
	.quad	136
	.quad	2
	.quad	8
	.quad	.LC48
	.quad	CMS_KEKRecipientInfo_it
	.quad	136
	.quad	3
	.quad	8
	.quad	.LC49
	.quad	CMS_PasswordRecipientInfo_it
	.quad	136
	.quad	4
	.quad	8
	.quad	.LC50
	.quad	CMS_OtherRecipientInfo_it
	.align 32
	.type	CMS_RecipientInfo_aux, @object
	.size	CMS_RecipientInfo_aux, 40
CMS_RecipientInfo_aux:
	.quad	0
	.long	0
	.long	0
	.long	0
	.zero	4
	.quad	cms_ri_cb
	.long	0
	.zero	4
	.section	.rodata.str1.1
.LC51:
	.string	"CMS_OtherRecipientInfo"
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_OtherRecipientInfo_it, @object
	.size	CMS_OtherRecipientInfo_it, 56
CMS_OtherRecipientInfo_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	CMS_OtherRecipientInfo_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC51
	.section	.rodata.str1.1
.LC52:
	.string	"oriType"
.LC53:
	.string	"oriValue"
	.section	.data.rel.ro
	.align 32
	.type	CMS_OtherRecipientInfo_seq_tt, @object
	.size	CMS_OtherRecipientInfo_seq_tt, 80
CMS_OtherRecipientInfo_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC52
	.quad	ASN1_OBJECT_it
	.quad	1
	.quad	0
	.quad	8
	.quad	.LC53
	.quad	ASN1_ANY_it
	.globl	CMS_PasswordRecipientInfo_it
	.section	.rodata.str1.1
.LC54:
	.string	"CMS_PasswordRecipientInfo"
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_PasswordRecipientInfo_it, @object
	.size	CMS_PasswordRecipientInfo_it, 56
CMS_PasswordRecipientInfo_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	CMS_PasswordRecipientInfo_seq_tt
	.quad	4
	.quad	0
	.quad	48
	.quad	.LC54
	.section	.rodata.str1.1
.LC55:
	.string	"keyDerivationAlgorithm"
.LC56:
	.string	"keyEncryptionAlgorithm"
.LC57:
	.string	"encryptedKey"
	.section	.data.rel.ro
	.align 32
	.type	CMS_PasswordRecipientInfo_seq_tt, @object
	.size	CMS_PasswordRecipientInfo_seq_tt, 160
CMS_PasswordRecipientInfo_seq_tt:
	.quad	4096
	.quad	0
	.quad	0
	.quad	.LC6
	.quad	INT32_it
	.quad	137
	.quad	0
	.quad	8
	.quad	.LC55
	.quad	X509_ALGOR_it
	.quad	0
	.quad	0
	.quad	16
	.quad	.LC56
	.quad	X509_ALGOR_it
	.quad	0
	.quad	0
	.quad	24
	.quad	.LC57
	.quad	ASN1_OCTET_STRING_it
	.globl	CMS_KEKRecipientInfo_it
	.section	.rodata.str1.1
.LC58:
	.string	"CMS_KEKRecipientInfo"
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_KEKRecipientInfo_it, @object
	.size	CMS_KEKRecipientInfo_it, 56
CMS_KEKRecipientInfo_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	CMS_KEKRecipientInfo_seq_tt
	.quad	4
	.quad	0
	.quad	48
	.quad	.LC58
	.section	.rodata.str1.1
.LC59:
	.string	"kekid"
	.section	.data.rel.ro
	.align 32
	.type	CMS_KEKRecipientInfo_seq_tt, @object
	.size	CMS_KEKRecipientInfo_seq_tt, 160
CMS_KEKRecipientInfo_seq_tt:
	.quad	4096
	.quad	0
	.quad	0
	.quad	.LC6
	.quad	INT32_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC59
	.quad	CMS_KEKIdentifier_it
	.quad	0
	.quad	0
	.quad	16
	.quad	.LC56
	.quad	X509_ALGOR_it
	.quad	0
	.quad	0
	.quad	24
	.quad	.LC57
	.quad	ASN1_OCTET_STRING_it
	.section	.rodata.str1.1
.LC60:
	.string	"CMS_KEKIdentifier"
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_KEKIdentifier_it, @object
	.size	CMS_KEKIdentifier_it, 56
CMS_KEKIdentifier_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	CMS_KEKIdentifier_seq_tt
	.quad	3
	.quad	0
	.quad	24
	.quad	.LC60
	.section	.rodata.str1.1
.LC61:
	.string	"keyIdentifier"
.LC62:
	.string	"date"
.LC63:
	.string	"other"
	.section	.data.rel.ro
	.align 32
	.type	CMS_KEKIdentifier_seq_tt, @object
	.size	CMS_KEKIdentifier_seq_tt, 120
CMS_KEKIdentifier_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC61
	.quad	ASN1_OCTET_STRING_it
	.quad	1
	.quad	0
	.quad	8
	.quad	.LC62
	.quad	ASN1_GENERALIZEDTIME_it
	.quad	1
	.quad	0
	.quad	16
	.quad	.LC63
	.quad	CMS_OtherKeyAttribute_it
	.globl	CMS_KeyAgreeRecipientInfo_it
	.section	.rodata.str1.1
.LC64:
	.string	"CMS_KeyAgreeRecipientInfo"
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_KeyAgreeRecipientInfo_it, @object
	.size	CMS_KeyAgreeRecipientInfo_it, 56
CMS_KeyAgreeRecipientInfo_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	CMS_KeyAgreeRecipientInfo_seq_tt
	.quad	5
	.quad	CMS_KeyAgreeRecipientInfo_aux
	.quad	56
	.quad	.LC64
	.section	.rodata.str1.1
.LC65:
	.string	"originator"
.LC66:
	.string	"ukm"
.LC67:
	.string	"recipientEncryptedKeys"
	.section	.data.rel.ro
	.align 32
	.type	CMS_KeyAgreeRecipientInfo_seq_tt, @object
	.size	CMS_KeyAgreeRecipientInfo_seq_tt, 200
CMS_KeyAgreeRecipientInfo_seq_tt:
	.quad	4096
	.quad	0
	.quad	0
	.quad	.LC6
	.quad	INT32_it
	.quad	144
	.quad	0
	.quad	8
	.quad	.LC65
	.quad	CMS_OriginatorIdentifierOrKey_it
	.quad	145
	.quad	1
	.quad	16
	.quad	.LC66
	.quad	ASN1_OCTET_STRING_it
	.quad	0
	.quad	0
	.quad	24
	.quad	.LC56
	.quad	X509_ALGOR_it
	.quad	4
	.quad	0
	.quad	32
	.quad	.LC67
	.quad	CMS_RecipientEncryptedKey_it
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_KeyAgreeRecipientInfo_aux, @object
	.size	CMS_KeyAgreeRecipientInfo_aux, 40
CMS_KeyAgreeRecipientInfo_aux:
	.quad	0
	.long	0
	.long	0
	.long	0
	.zero	4
	.quad	cms_kari_cb
	.long	0
	.zero	4
	.section	.rodata.str1.1
.LC68:
	.string	"CMS_OriginatorIdentifierOrKey"
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_OriginatorIdentifierOrKey_it, @object
	.size	CMS_OriginatorIdentifierOrKey_it, 56
CMS_OriginatorIdentifierOrKey_it:
	.byte	2
	.zero	7
	.quad	0
	.quad	CMS_OriginatorIdentifierOrKey_ch_tt
	.quad	3
	.quad	0
	.quad	16
	.quad	.LC68
	.section	.rodata.str1.1
.LC69:
	.string	"d.issuerAndSerialNumber"
.LC70:
	.string	"d.subjectKeyIdentifier"
.LC71:
	.string	"d.originatorKey"
	.section	.data.rel.ro
	.align 32
	.type	CMS_OriginatorIdentifierOrKey_ch_tt, @object
	.size	CMS_OriginatorIdentifierOrKey_ch_tt, 120
CMS_OriginatorIdentifierOrKey_ch_tt:
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC69
	.quad	CMS_IssuerAndSerialNumber_it
	.quad	136
	.quad	0
	.quad	8
	.quad	.LC70
	.quad	ASN1_OCTET_STRING_it
	.quad	136
	.quad	1
	.quad	8
	.quad	.LC71
	.quad	CMS_OriginatorPublicKey_it
	.globl	CMS_OriginatorPublicKey_it
	.section	.rodata.str1.1
.LC72:
	.string	"CMS_OriginatorPublicKey"
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_OriginatorPublicKey_it, @object
	.size	CMS_OriginatorPublicKey_it, 56
CMS_OriginatorPublicKey_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	CMS_OriginatorPublicKey_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC72
	.section	.rodata.str1.1
.LC73:
	.string	"algorithm"
.LC74:
	.string	"publicKey"
	.section	.data.rel.ro
	.align 32
	.type	CMS_OriginatorPublicKey_seq_tt, @object
	.size	CMS_OriginatorPublicKey_seq_tt, 80
CMS_OriginatorPublicKey_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC73
	.quad	X509_ALGOR_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC74
	.quad	ASN1_BIT_STRING_it
	.globl	CMS_RecipientEncryptedKey_it
	.section	.rodata.str1.1
.LC75:
	.string	"CMS_RecipientEncryptedKey"
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_RecipientEncryptedKey_it, @object
	.size	CMS_RecipientEncryptedKey_it, 56
CMS_RecipientEncryptedKey_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	CMS_RecipientEncryptedKey_seq_tt
	.quad	2
	.quad	CMS_RecipientEncryptedKey_aux
	.quad	24
	.quad	.LC75
	.section	.rodata.str1.1
.LC76:
	.string	"rid"
	.section	.data.rel.ro
	.align 32
	.type	CMS_RecipientEncryptedKey_seq_tt, @object
	.size	CMS_RecipientEncryptedKey_seq_tt, 80
CMS_RecipientEncryptedKey_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC76
	.quad	CMS_KeyAgreeRecipientIdentifier_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC57
	.quad	ASN1_OCTET_STRING_it
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_RecipientEncryptedKey_aux, @object
	.size	CMS_RecipientEncryptedKey_aux, 40
CMS_RecipientEncryptedKey_aux:
	.quad	0
	.long	0
	.long	0
	.long	0
	.zero	4
	.quad	cms_rek_cb
	.long	0
	.zero	4
	.section	.rodata.str1.8
	.align 8
.LC77:
	.string	"CMS_KeyAgreeRecipientIdentifier"
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_KeyAgreeRecipientIdentifier_it, @object
	.size	CMS_KeyAgreeRecipientIdentifier_it, 56
CMS_KeyAgreeRecipientIdentifier_it:
	.byte	2
	.zero	7
	.quad	0
	.quad	CMS_KeyAgreeRecipientIdentifier_ch_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC77
	.section	.rodata.str1.1
.LC78:
	.string	"d.rKeyId"
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_KeyAgreeRecipientIdentifier_ch_tt, @object
	.size	CMS_KeyAgreeRecipientIdentifier_ch_tt, 80
CMS_KeyAgreeRecipientIdentifier_ch_tt:
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC69
	.quad	CMS_IssuerAndSerialNumber_it
	.quad	136
	.quad	0
	.quad	8
	.quad	.LC78
	.quad	CMS_RecipientKeyIdentifier_it
	.globl	CMS_RecipientKeyIdentifier_it
	.section	.rodata.str1.1
.LC79:
	.string	"CMS_RecipientKeyIdentifier"
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_RecipientKeyIdentifier_it, @object
	.size	CMS_RecipientKeyIdentifier_it, 56
CMS_RecipientKeyIdentifier_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	CMS_RecipientKeyIdentifier_seq_tt
	.quad	3
	.quad	0
	.quad	24
	.quad	.LC79
	.section	.rodata.str1.1
.LC80:
	.string	"subjectKeyIdentifier"
	.section	.data.rel.ro
	.align 32
	.type	CMS_RecipientKeyIdentifier_seq_tt, @object
	.size	CMS_RecipientKeyIdentifier_seq_tt, 120
CMS_RecipientKeyIdentifier_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC80
	.quad	ASN1_OCTET_STRING_it
	.quad	1
	.quad	0
	.quad	8
	.quad	.LC62
	.quad	ASN1_GENERALIZEDTIME_it
	.quad	1
	.quad	0
	.quad	16
	.quad	.LC63
	.quad	CMS_OtherKeyAttribute_it
	.globl	CMS_OtherKeyAttribute_it
	.section	.rodata.str1.1
.LC81:
	.string	"CMS_OtherKeyAttribute"
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_OtherKeyAttribute_it, @object
	.size	CMS_OtherKeyAttribute_it, 56
CMS_OtherKeyAttribute_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	CMS_OtherKeyAttribute_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC81
	.section	.rodata.str1.1
.LC82:
	.string	"keyAttrId"
.LC83:
	.string	"keyAttr"
	.section	.data.rel.ro
	.align 32
	.type	CMS_OtherKeyAttribute_seq_tt, @object
	.size	CMS_OtherKeyAttribute_seq_tt, 80
CMS_OtherKeyAttribute_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC82
	.quad	ASN1_OBJECT_it
	.quad	1
	.quad	0
	.quad	8
	.quad	.LC83
	.quad	ASN1_ANY_it
	.globl	CMS_KeyTransRecipientInfo_it
	.section	.rodata.str1.1
.LC84:
	.string	"CMS_KeyTransRecipientInfo"
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_KeyTransRecipientInfo_it, @object
	.size	CMS_KeyTransRecipientInfo_it, 56
CMS_KeyTransRecipientInfo_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	CMS_KeyTransRecipientInfo_seq_tt
	.quad	4
	.quad	0
	.quad	56
	.quad	.LC84
	.section	.data.rel.ro
	.align 32
	.type	CMS_KeyTransRecipientInfo_seq_tt, @object
	.size	CMS_KeyTransRecipientInfo_seq_tt, 160
CMS_KeyTransRecipientInfo_seq_tt:
	.quad	4096
	.quad	0
	.quad	0
	.quad	.LC6
	.quad	INT32_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC76
	.quad	CMS_SignerIdentifier_it
	.quad	0
	.quad	0
	.quad	16
	.quad	.LC56
	.quad	X509_ALGOR_it
	.quad	0
	.quad	0
	.quad	24
	.quad	.LC57
	.quad	ASN1_OCTET_STRING_it
	.section	.rodata.str1.1
.LC85:
	.string	"CMS_EncryptedContentInfo"
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_EncryptedContentInfo_it, @object
	.size	CMS_EncryptedContentInfo_it, 56
CMS_EncryptedContentInfo_it:
	.byte	6
	.zero	7
	.quad	16
	.quad	CMS_EncryptedContentInfo_seq_tt
	.quad	3
	.quad	0
	.quad	56
	.quad	.LC85
	.section	.rodata.str1.1
.LC86:
	.string	"contentEncryptionAlgorithm"
.LC87:
	.string	"encryptedContent"
	.section	.data.rel.ro
	.align 32
	.type	CMS_EncryptedContentInfo_seq_tt, @object
	.size	CMS_EncryptedContentInfo_seq_tt, 120
CMS_EncryptedContentInfo_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC7
	.quad	ASN1_OBJECT_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC86
	.quad	X509_ALGOR_it
	.quad	137
	.quad	0
	.quad	16
	.quad	.LC87
	.quad	ASN1_OCTET_STRING_NDEF_it
	.section	.rodata.str1.1
.LC88:
	.string	"CMS_OriginatorInfo"
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_OriginatorInfo_it, @object
	.size	CMS_OriginatorInfo_it, 56
CMS_OriginatorInfo_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	CMS_OriginatorInfo_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC88
	.section	.rodata.str1.1
.LC89:
	.string	"certificates"
.LC90:
	.string	"crls"
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_OriginatorInfo_seq_tt, @object
	.size	CMS_OriginatorInfo_seq_tt, 80
CMS_OriginatorInfo_seq_tt:
	.quad	139
	.quad	0
	.quad	0
	.quad	.LC89
	.quad	CMS_CertificateChoices_it
	.quad	139
	.quad	1
	.quad	8
	.quad	.LC90
	.quad	CMS_RevocationInfoChoice_it
	.globl	CMS_SignedData_it
	.section	.rodata.str1.1
.LC91:
	.string	"CMS_SignedData"
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_SignedData_it, @object
	.size	CMS_SignedData_it, 56
CMS_SignedData_it:
	.byte	6
	.zero	7
	.quad	16
	.quad	CMS_SignedData_seq_tt
	.quad	6
	.quad	0
	.quad	48
	.quad	.LC91
	.section	.rodata.str1.1
.LC92:
	.string	"digestAlgorithms"
.LC93:
	.string	"signerInfos"
	.section	.data.rel.ro
	.align 32
	.type	CMS_SignedData_seq_tt, @object
	.size	CMS_SignedData_seq_tt, 240
CMS_SignedData_seq_tt:
	.quad	4096
	.quad	0
	.quad	0
	.quad	.LC6
	.quad	INT32_it
	.quad	2
	.quad	0
	.quad	8
	.quad	.LC92
	.quad	X509_ALGOR_it
	.quad	0
	.quad	0
	.quad	16
	.quad	.LC30
	.quad	CMS_EncapsulatedContentInfo_it
	.quad	139
	.quad	0
	.quad	24
	.quad	.LC89
	.quad	CMS_CertificateChoices_it
	.quad	139
	.quad	1
	.quad	32
	.quad	.LC90
	.quad	CMS_RevocationInfoChoice_it
	.quad	2
	.quad	0
	.quad	40
	.quad	.LC93
	.quad	CMS_SignerInfo_it
	.globl	CMS_RevocationInfoChoice_it
	.section	.rodata.str1.1
.LC94:
	.string	"CMS_RevocationInfoChoice"
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_RevocationInfoChoice_it, @object
	.size	CMS_RevocationInfoChoice_it, 56
CMS_RevocationInfoChoice_it:
	.byte	2
	.zero	7
	.quad	0
	.quad	CMS_RevocationInfoChoice_ch_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC94
	.section	.rodata.str1.1
.LC95:
	.string	"d.crl"
	.section	.data.rel.ro
	.align 32
	.type	CMS_RevocationInfoChoice_ch_tt, @object
	.size	CMS_RevocationInfoChoice_ch_tt, 80
CMS_RevocationInfoChoice_ch_tt:
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC95
	.quad	X509_CRL_it
	.quad	136
	.quad	1
	.quad	8
	.quad	.LC27
	.quad	CMS_OtherRevocationInfoFormat_it
	.section	.rodata.str1.1
.LC96:
	.string	"CMS_OtherRevocationInfoFormat"
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_OtherRevocationInfoFormat_it, @object
	.size	CMS_OtherRevocationInfoFormat_it, 56
CMS_OtherRevocationInfoFormat_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	CMS_OtherRevocationInfoFormat_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC96
	.section	.rodata.str1.1
.LC97:
	.string	"otherRevInfoFormat"
.LC98:
	.string	"otherRevInfo"
	.section	.data.rel.ro
	.align 32
	.type	CMS_OtherRevocationInfoFormat_seq_tt, @object
	.size	CMS_OtherRevocationInfoFormat_seq_tt, 80
CMS_OtherRevocationInfoFormat_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC97
	.quad	ASN1_OBJECT_it
	.quad	1
	.quad	0
	.quad	8
	.quad	.LC98
	.quad	ASN1_ANY_it
	.globl	CMS_SignerInfo_it
	.section	.rodata.str1.1
.LC99:
	.string	"CMS_SignerInfo"
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_SignerInfo_it, @object
	.size	CMS_SignerInfo_it, 56
CMS_SignerInfo_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	CMS_SignerInfo_seq_tt
	.quad	7
	.quad	CMS_SignerInfo_aux
	.quad	88
	.quad	.LC99
	.section	.rodata.str1.1
.LC100:
	.string	"sid"
.LC101:
	.string	"signedAttrs"
.LC102:
	.string	"signatureAlgorithm"
.LC103:
	.string	"signature"
.LC104:
	.string	"unsignedAttrs"
	.section	.data.rel.ro
	.align 32
	.type	CMS_SignerInfo_seq_tt, @object
	.size	CMS_SignerInfo_seq_tt, 280
CMS_SignerInfo_seq_tt:
	.quad	4096
	.quad	0
	.quad	0
	.quad	.LC6
	.quad	INT32_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC100
	.quad	CMS_SignerIdentifier_it
	.quad	0
	.quad	0
	.quad	16
	.quad	.LC35
	.quad	X509_ALGOR_it
	.quad	139
	.quad	0
	.quad	24
	.quad	.LC101
	.quad	X509_ATTRIBUTE_it
	.quad	0
	.quad	0
	.quad	32
	.quad	.LC102
	.quad	X509_ALGOR_it
	.quad	0
	.quad	0
	.quad	40
	.quad	.LC103
	.quad	ASN1_OCTET_STRING_it
	.quad	139
	.quad	1
	.quad	48
	.quad	.LC104
	.quad	X509_ATTRIBUTE_it
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_SignerInfo_aux, @object
	.size	CMS_SignerInfo_aux, 40
CMS_SignerInfo_aux:
	.quad	0
	.long	0
	.long	0
	.long	0
	.zero	4
	.quad	cms_si_cb
	.long	0
	.zero	4
	.section	.rodata.str1.1
.LC105:
	.string	"CMS_EncapsulatedContentInfo"
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_EncapsulatedContentInfo_it, @object
	.size	CMS_EncapsulatedContentInfo_it, 56
CMS_EncapsulatedContentInfo_it:
	.byte	6
	.zero	7
	.quad	16
	.quad	CMS_EncapsulatedContentInfo_seq_tt
	.quad	2
	.quad	0
	.quad	24
	.quad	.LC105
	.section	.rodata.str1.1
.LC106:
	.string	"eContentType"
.LC107:
	.string	"eContent"
	.section	.data.rel.ro
	.align 32
	.type	CMS_EncapsulatedContentInfo_seq_tt, @object
	.size	CMS_EncapsulatedContentInfo_seq_tt, 80
CMS_EncapsulatedContentInfo_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC106
	.quad	ASN1_OBJECT_it
	.quad	2193
	.quad	0
	.quad	8
	.quad	.LC107
	.quad	ASN1_OCTET_STRING_NDEF_it
	.section	.rodata.str1.1
.LC108:
	.string	"CMS_SignerIdentifier"
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_SignerIdentifier_it, @object
	.size	CMS_SignerIdentifier_it, 56
CMS_SignerIdentifier_it:
	.byte	2
	.zero	7
	.quad	0
	.quad	CMS_SignerIdentifier_ch_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC108
	.section	.data.rel.ro
	.align 32
	.type	CMS_SignerIdentifier_ch_tt, @object
	.size	CMS_SignerIdentifier_ch_tt, 80
CMS_SignerIdentifier_ch_tt:
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC69
	.quad	CMS_IssuerAndSerialNumber_it
	.quad	136
	.quad	0
	.quad	8
	.quad	.LC70
	.quad	ASN1_OCTET_STRING_it
	.globl	CMS_CertificateChoices_it
	.section	.rodata.str1.1
.LC109:
	.string	"CMS_CertificateChoices"
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_CertificateChoices_it, @object
	.size	CMS_CertificateChoices_it, 56
CMS_CertificateChoices_it:
	.byte	2
	.zero	7
	.quad	0
	.quad	CMS_CertificateChoices_ch_tt
	.quad	5
	.quad	0
	.quad	16
	.quad	.LC109
	.section	.rodata.str1.1
.LC110:
	.string	"d.certificate"
.LC111:
	.string	"d.extendedCertificate"
.LC112:
	.string	"d.v1AttrCert"
.LC113:
	.string	"d.v2AttrCert"
	.section	.data.rel.ro
	.align 32
	.type	CMS_CertificateChoices_ch_tt, @object
	.size	CMS_CertificateChoices_ch_tt, 200
CMS_CertificateChoices_ch_tt:
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC110
	.quad	X509_it
	.quad	136
	.quad	0
	.quad	8
	.quad	.LC111
	.quad	ASN1_SEQUENCE_it
	.quad	136
	.quad	1
	.quad	8
	.quad	.LC112
	.quad	ASN1_SEQUENCE_it
	.quad	136
	.quad	2
	.quad	8
	.quad	.LC113
	.quad	ASN1_SEQUENCE_it
	.quad	136
	.quad	3
	.quad	8
	.quad	.LC27
	.quad	CMS_OtherCertificateFormat_it
	.section	.rodata.str1.1
.LC114:
	.string	"CMS_OtherCertificateFormat"
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_OtherCertificateFormat_it, @object
	.size	CMS_OtherCertificateFormat_it, 56
CMS_OtherCertificateFormat_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	CMS_OtherCertificateFormat_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC114
	.section	.rodata.str1.1
.LC115:
	.string	"otherCertFormat"
.LC116:
	.string	"otherCert"
	.section	.data.rel.ro
	.align 32
	.type	CMS_OtherCertificateFormat_seq_tt, @object
	.size	CMS_OtherCertificateFormat_seq_tt, 80
CMS_OtherCertificateFormat_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC115
	.quad	ASN1_OBJECT_it
	.quad	1
	.quad	0
	.quad	8
	.quad	.LC116
	.quad	ASN1_ANY_it
	.globl	CMS_IssuerAndSerialNumber_it
	.section	.rodata.str1.1
.LC117:
	.string	"CMS_IssuerAndSerialNumber"
	.section	.data.rel.ro.local
	.align 32
	.type	CMS_IssuerAndSerialNumber_it, @object
	.size	CMS_IssuerAndSerialNumber_it, 56
CMS_IssuerAndSerialNumber_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	CMS_IssuerAndSerialNumber_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC117
	.section	.rodata.str1.1
.LC118:
	.string	"issuer"
.LC119:
	.string	"serialNumber"
	.section	.data.rel.ro
	.align 32
	.type	CMS_IssuerAndSerialNumber_seq_tt, @object
	.size	CMS_IssuerAndSerialNumber_seq_tt, 80
CMS_IssuerAndSerialNumber_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC118
	.quad	X509_NAME_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC119
	.quad	ASN1_INTEGER_it
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
