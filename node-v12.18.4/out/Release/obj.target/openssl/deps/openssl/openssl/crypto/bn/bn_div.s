	.file	"bn_div.c"
	.text
	.p2align 4
	.globl	bn_div_fixed_top
	.type	bn_div_fixed_top, @function
bn_div_fixed_top:
.LFB254:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r8, %rdi
	subq	$120, %rsp
	movq	%rsi, -136(%rbp)
	movq	%rdx, -104(%rbp)
	call	BN_CTX_start@PLT
	testq	%rbx, %rbx
	je	.L85
.L2:
	movq	%r13, %rdi
	call	BN_CTX_get@PLT
	movq	%r13, %rdi
	movq	%rax, -56(%rbp)
	call	BN_CTX_get@PLT
	movq	%r13, %rdi
	movq	%rax, -96(%rbp)
	call	BN_CTX_get@PLT
	movq	%rax, -64(%rbp)
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L3
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L3
	movq	%r15, %rax
	movq	(%r15), %r15
	movslq	8(%rax), %rax
	movq	-8(%r15,%rax,8), %rdi
	movq	%rax, %r14
	call	BN_num_bits_word@PLT
	movl	$64, %edx
	subl	%eax, %edx
	movl	%edx, -116(%rbp)
	cltd
	shrl	$26, %edx
	leal	(%rax,%rdx), %r8d
	andl	$63, %r8d
	subl	%edx, %r8d
	movslq	%r8d, %r9
	negq	%r9
	movq	%r9, %rax
	shrq	$8, %rax
	orq	%rax, %r9
	testl	%r14d, %r14d
	jle	.L7
	leal	-1(%r14), %eax
	movl	-116(%rbp), %r11d
	movq	%r15, %rsi
	leaq	8(%r15,%rax,8), %r10
	xorl	%eax, %eax
	movq	%rax, %rdi
	.p2align 4,,10
	.p2align 3
.L6:
	movq	(%rsi), %rdx
	movl	%r11d, %ecx
	addq	$8, %rsi
	movq	%rdx, %rax
	salq	%cl, %rax
	movl	%r8d, %ecx
	orq	%rdi, %rax
	shrq	%cl, %rdx
	movq	%rdx, %rdi
	movq	%rax, -8(%rsi)
	andq	%r9, %rdi
	cmpq	%r10, %rsi
	jne	.L6
.L7:
	movq	-64(%rbp), %rax
	movl	-116(%rbp), %edx
	movq	-104(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movl	$0, 16(%rax)
	call	bn_lshift_fixed_top@PLT
	testl	%eax, %eax
	je	.L3
	movq	-64(%rbp), %rax
	movl	8(%rax), %r8d
	movq	-96(%rbp), %rax
	movl	8(%rax), %eax
	movl	%eax, -72(%rbp)
	cmpl	%eax, %r8d
	jge	.L8
	subl	%r8d, %eax
	movl	%eax, %edx
	cltq
	salq	$3, %rax
	movq	%rax, -128(%rbp)
.L9:
	movq	-96(%rbp), %rax
	movslq	%r8d, %rcx
	xorl	%r14d, %r14d
	leaq	0(,%rcx,8), %rdi
	movq	(%rax), %rax
	movq	%rdi, -112(%rbp)
	movq	%rax, %r15
	movq	-64(%rbp), %rax
	movq	(%rax), %rax
	movq	-8(%rax,%rdi), %rcx
	movq	%rcx, -152(%rbp)
	cmpl	$1, %r8d
	je	.L10
	movq	-16(%rax,%rdi), %r14
.L10:
	movl	%edx, %esi
	movq	%rbx, %rdi
	movl	%r8d, -88(%rbp)
	movl	%edx, -76(%rbp)
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L3
	movq	-104(%rbp), %rax
	movl	-76(%rbp), %edx
	movl	-88(%rbp), %r8d
	movq	-56(%rbp), %rdi
	movl	16(%rax), %eax
	xorl	16(%r12), %eax
	movl	%edx, 8(%rbx)
	movl	%eax, 16(%rbx)
	leal	1(%r8), %eax
	movq	(%rbx), %rbx
	movl	%eax, %esi
	movl	%edx, -120(%rbp)
	movl	%r8d, -76(%rbp)
	movl	%eax, -80(%rbp)
	call	bn_wexpand@PLT
	movl	-76(%rbp), %r8d
	movl	-120(%rbp), %edx
	testq	%rax, %rax
	je	.L3
	movslq	-72(%rbp), %rax
	movq	-128(%rbp), %rcx
	movq	%r15, %rdi
	leaq	-8(%rdi,%rax,8), %r9
	leal	-1(%rdx), %eax
	addq	%rcx, %rbx
	addq	%rcx, %r15
	notq	%rax
	movq	%rbx, -72(%rbp)
	leaq	(%rbx,%rax,8), %rax
	movq	-152(%rbp), %rbx
	movq	%r13, -152(%rbp)
	movq	%rax, -128(%rbp)
	movl	%r8d, %eax
	shrl	%eax
	salq	$4, %rax
	movq	%rax, -144(%rbp)
	movl	%r8d, %eax
	andl	$-2, %eax
	movl	%eax, -120(%rbp)
	movslq	%eax, %r12
	salq	$3, %r12
	.p2align 4,,10
	.p2align 3
.L33:
	movq	(%r9), %rdx
	movq	$-1, %r13
	cmpq	%rdx, %rbx
	jne	.L86
.L14:
	movq	-64(%rbp), %rax
	movl	%r8d, %edx
	movq	%r13, %rcx
	movq	%r9, -88(%rbp)
	movl	%r8d, -76(%rbp)
	subq	$8, %r15
	movq	(%rax), %rsi
	movq	-56(%rbp), %rax
	movq	(%rax), %rdi
	call	bn_mul_words@PLT
	movq	-56(%rbp), %rdi
	movq	-112(%rbp), %rsi
	movl	-80(%rbp), %ecx
	movq	(%rdi), %rdx
	movq	%r15, %rdi
	movq	%rax, (%rdx,%rsi)
	movq	%r15, %rsi
	call	bn_sub_words@PLT
	movl	-76(%rbp), %r8d
	movq	-88(%rbp), %r9
	subq	%rax, %r13
	negq	%rax
	testl	%r8d, %r8d
	jle	.L25
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rdi
	movq	(%rdx), %rcx
	movq	(%rdi), %rdx
	leal	-1(%r8), %edi
	leaq	15(%rcx), %rsi
	subq	%rdx, %rsi
	cmpq	$30, %rsi
	jbe	.L26
	cmpl	$3, %edi
	jbe	.L26
	movq	%rax, %xmm1
	movq	-144(%rbp), %rdi
	xorl	%esi, %esi
	punpcklqdq	%xmm1, %xmm1
	.p2align 4,,10
	.p2align 3
.L27:
	movdqu	(%rcx,%rsi), %xmm0
	pand	%xmm1, %xmm0
	movups	%xmm0, (%rdx,%rsi)
	addq	$16, %rsi
	cmpq	%rdi, %rsi
	jne	.L27
	cmpl	%r8d, -120(%rbp)
	je	.L29
	andq	(%rcx,%r12), %rax
	movq	%rax, (%rdx,%r12)
.L29:
	movl	%r8d, %ecx
	movq	%r15, %rsi
	movq	%r15, %rdi
	movq	%r9, -88(%rbp)
	movl	%r8d, -76(%rbp)
	call	bn_add_words@PLT
	movq	-88(%rbp), %r9
	subq	$8, -72(%rbp)
	movl	-76(%rbp), %r8d
	addq	%rax, (%r9)
	movq	-72(%rbp), %rax
	subq	$8, %r9
	cmpq	%rax, -128(%rbp)
	movq	%r13, (%rax)
	jne	.L33
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rbx
	cmpq	$0, -136(%rbp)
	movq	-152(%rbp), %r13
	movl	16(%rax), %eax
	movl	%r8d, 8(%rbx)
	movl	%eax, 16(%rbx)
	je	.L13
	movl	-116(%rbp), %edx
	movq	-136(%rbp), %rdi
	movq	%rbx, %rsi
	call	bn_rshift_fixed_top@PLT
.L13:
	movq	%r13, %rdi
	call	BN_CTX_end@PLT
	addq	$120, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	movq	%r13, %rdi
	call	BN_CTX_end@PLT
	addq	$120, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	movq	-8(%r9), %rax
	xorl	%edi, %edi
	cmpq	%r9, %r15
	je	.L15
	movq	-16(%r9), %rdi
.L15:
#APP
# 385 "../deps/openssl/openssl/crypto/bn/bn_div.c" 1
	divq   %rbx
# 0 "" 2
#NO_APP
	movq	%rax, %rsi
	movq	%rax, %r13
	movq	%r14, %rax
	movq	%rdx, %rcx
	mulq	%rsi
	movq	%rdx, %r10
	cmpq	%rdx, %rcx
	ja	.L14
	cmpq	%rax, %rdi
	jb	.L39
	cmpq	%rdx, %rcx
	je	.L14
.L39:
	leaq	-1(%rsi), %r13
	addq	%rbx, %rcx
	jnc	.L19
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L24:
	jne	.L40
	cmpq	%rax, %rdi
	jnb	.L14
.L40:
	subq	$1, %r13
	addq	%rbx, %rcx
	jc	.L14
.L19:
	cmpq	%r14, %rax
	sbbq	$0, %r10
	subq	%r14, %rax
	cmpq	%rcx, %r10
	jnb	.L24
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L26:
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L31:
	movq	(%rcx,%rsi,8), %r10
	andq	%rax, %r10
	movq	%r10, (%rdx,%rsi,8)
	movq	%rsi, %r10
	addq	$1, %rsi
	cmpq	%r10, %rdi
	jne	.L31
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L85:
	movq	%r13, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, %rbx
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L25:
	movq	-56(%rbp), %rax
	movq	(%rax), %rdx
	jmp	.L29
.L8:
	movq	-96(%rbp), %r15
	leal	1(%r8), %r14d
	movl	%r8d, -76(%rbp)
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L3
	movl	-76(%rbp), %r8d
	movslq	-72(%rbp), %rcx
	xorl	%esi, %esi
	movq	(%r15), %rax
	movl	%r8d, %edx
	subl	%ecx, %edx
	leaq	(%rax,%rcx,8), %rdi
	addl	$1, %edx
	movslq	%edx, %rdx
	salq	$3, %rdx
	call	memset@PLT
	movl	%r14d, 8(%r15)
	movl	-76(%rbp), %r8d
	movl	$1, %edx
	movl	%r14d, -72(%rbp)
	movq	$8, -128(%rbp)
	jmp	.L9
	.cfi_endproc
.LFE254:
	.size	bn_div_fixed_top, .-bn_div_fixed_top
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/bn/bn_div.c"
	.text
	.p2align 4
	.globl	BN_div
	.type	BN_div, @function
BN_div:
.LFB253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rcx, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$24, %rsp
	movq	%rcx, -56(%rbp)
	call	BN_is_zero@PLT
	movq	-56(%rbp), %rcx
	testl	%eax, %eax
	jne	.L102
	movslq	8(%rcx), %rdx
	movl	%eax, %r12d
	movq	(%rcx), %rax
	cmpq	$0, -8(%rax,%rdx,8)
	je	.L103
	movq	%rbx, %r8
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	bn_div_fixed_top
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L87
	testq	%r14, %r14
	je	.L91
	movq	%r14, %rdi
	call	bn_correct_top@PLT
.L91:
	testq	%r13, %r13
	je	.L87
	movq	%r13, %rdi
	call	bn_correct_top@PLT
.L87:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	movl	$225, %r8d
	movl	$107, %edx
	movl	$107, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L102:
	movl	$215, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$103, %edx
	xorl	%r12d, %r12d
	movl	$107, %esi
	movl	$3, %edi
	call	ERR_put_error@PLT
	jmp	.L87
	.cfi_endproc
.LFE253:
	.size	BN_div, .-BN_div
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
