	.file	"v3_skey.c"
	.text
	.p2align 4
	.globl	i2s_ASN1_OCTET_STRING
	.type	i2s_ASN1_OCTET_STRING, @function
i2s_ASN1_OCTET_STRING:
.LFB1298:
	.cfi_startproc
	endbr64
	movslq	(%rsi), %r8
	movq	8(%rsi), %rdi
	movq	%r8, %rsi
	jmp	OPENSSL_buf2hexstr@PLT
	.cfi_endproc
.LFE1298:
	.size	i2s_ASN1_OCTET_STRING, .-i2s_ASN1_OCTET_STRING
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"hash"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../deps/openssl/openssl/crypto/x509v3/v3_skey.c"
	.text
	.p2align 4
	.type	s2i_skey_id, @function
s2i_skey_id:
.LFB1300:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$5, %ecx
	leaq	.LC0(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L26
	call	ASN1_OCTET_STRING_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L27
	testq	%rbx, %rbx
	je	.L11
	cmpl	$1, (%rbx)
	je	.L3
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L28
	movq	40(%rax), %r8
.L15:
	testq	%r8, %r8
	je	.L29
	xorl	%ecx, %ecx
	leaq	-128(%rbp), %rdx
	leaq	-120(%rbp), %rsi
	xorl	%edi, %edi
	call	X509_PUBKEY_get0_param@PLT
	leaq	-112(%rbp), %r13
	call	EVP_sha1@PLT
	movslq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdi
	xorl	%r9d, %r9d
	movq	%rax, %r8
	leaq	-124(%rbp), %rcx
	movq	%r13, %rdx
	call	EVP_Digest@PLT
	testl	%eax, %eax
	je	.L14
	movl	-124(%rbp), %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	ASN1_OCTET_STRING_set@PLT
	testl	%eax, %eax
	je	.L30
.L3:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L31
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movq	%rdx, %r13
	call	ASN1_OCTET_STRING_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L32
	leaq	-120(%rbp), %rsi
	movq	%r13, %rdi
	call	OPENSSL_hexstr2buf@PLT
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L14
	movq	-120(%rbp), %rax
	movl	%eax, (%r12)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L11:
	movl	$77, %r8d
.L25:
	movl	$114, %edx
	movl	$115, %esi
	movl	$34, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
.L14:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	ASN1_OCTET_STRING_free@PLT
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L28:
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.L11
	movq	80(%rax), %r8
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L29:
	movl	$87, %r8d
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L30:
	movl	$97, %r8d
	movl	$65, %edx
	movl	$115, %esi
	movl	$34, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L32:
	movl	$40, %r8d
	movl	$65, %edx
	movl	$112, %esi
	movl	$34, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L27:
	movl	$69, %r8d
	movl	$65, %edx
	movl	$115, %esi
	movl	$34, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L3
.L31:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1300:
	.size	s2i_skey_id, .-s2i_skey_id
	.p2align 4
	.globl	s2i_ASN1_OCTET_STRING
	.type	s2i_ASN1_OCTET_STRING, @function
s2i_ASN1_OCTET_STRING:
.LFB1299:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	ASN1_OCTET_STRING_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L39
	leaq	-32(%rbp), %rsi
	movq	%r13, %rdi
	call	OPENSSL_hexstr2buf@PLT
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L40
	movq	-32(%rbp), %rax
	movl	%eax, (%r12)
.L33:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L41
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	movl	$40, %r8d
	movl	$65, %edx
	movl	$112, %esi
	movl	$34, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L40:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	ASN1_OCTET_STRING_free@PLT
	jmp	.L33
.L41:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1299:
	.size	s2i_ASN1_OCTET_STRING, .-s2i_ASN1_OCTET_STRING
	.globl	v3_skey_id
	.section	.data.rel.ro,"aw"
	.align 32
	.type	v3_skey_id, @object
	.size	v3_skey_id, 104
v3_skey_id:
	.long	82
	.long	0
	.quad	ASN1_OCTET_STRING_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	i2s_ASN1_OCTET_STRING
	.quad	s2i_skey_id
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
