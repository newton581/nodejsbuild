	.file	"ts_conf.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"r"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../deps/openssl/openssl/crypto/ts/ts_conf.c"
	.text
	.p2align 4
	.globl	TS_CONF_load_cert
	.type	TS_CONF_load_cert, @function
TS_CONF_load_cert:
.LFB1392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	call	BIO_new_file@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L4
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	PEM_read_bio_X509_AUX@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L4
.L3:
	movq	%r12, %rdi
	call	BIO_free@PLT
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	movl	$54, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$137, %edx
	xorl	%r13d, %r13d
	movl	$153, %esi
	movl	$47, %edi
	call	ERR_put_error@PLT
	jmp	.L3
	.cfi_endproc
.LFE1392:
	.size	TS_CONF_load_cert, .-TS_CONF_load_cert
	.p2align 4
	.globl	TS_CONF_load_certs
	.type	TS_CONF_load_certs, @function
TS_CONF_load_certs:
.LFB1393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	call	BIO_new_file@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L11
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	PEM_X509_INFO_read_bio@PLT
	xorl	%r12d, %r12d
	movq	%rax, %r13
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L14:
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movq	(%rax), %rsi
	movq	%rax, %rbx
	testq	%rsi, %rsi
	je	.L13
	movq	%r14, %rdi
	call	OPENSSL_sk_push@PLT
	movq	$0, (%rbx)
.L13:
	addl	$1, %r12d
.L12:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jl	.L14
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L11:
	movl	$137, %edx
	movl	$154, %esi
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	movl	$81, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$47, %edi
	call	ERR_put_error@PLT
.L15:
	movq	X509_INFO_free@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	%r15, %rdi
	call	BIO_free@PLT
	addq	$8, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1393:
	.size	TS_CONF_load_certs, .-TS_CONF_load_certs
	.p2align 4
	.globl	TS_CONF_load_key
	.type	TS_CONF_load_key, @function
TS_CONF_load_key:
.LFB1394:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	leaq	.LC0(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -32
	call	BIO_new_file@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L29
	movq	%r13, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	PEM_read_bio_PrivateKey@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L29
.L28:
	movq	%r12, %rdi
	call	BIO_free@PLT
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movl	$97, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$138, %edx
	xorl	%r13d, %r13d
	movl	$155, %esi
	movl	$47, %edi
	call	ERR_put_error@PLT
	jmp	.L28
	.cfi_endproc
.LFE1394:
	.size	TS_CONF_load_key, .-TS_CONF_load_key
	.section	.rodata.str1.1
.LC2:
	.string	"default_tsa"
.LC3:
	.string	"tsa"
.LC4:
	.string	"::"
	.text
	.p2align 4
	.globl	TS_CONF_get_tsa_section
	.type	TS_CONF_get_tsa_section, @function
TS_CONF_get_tsa_section:
.LFB1397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L37
.L35:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	leaq	.LC2(%rip), %rdx
	leaq	.LC3(%rip), %rsi
	call	NCONF_get_string@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L35
	movl	$106, %r8d
	movl	$136, %edx
	movl	$152, %esi
	movl	$47, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	leaq	.LC2(%rip), %rcx
	movl	$3, %edi
	xorl	%eax, %eax
	leaq	.LC4(%rip), %rdx
	leaq	.LC3(%rip), %rsi
	call	ERR_add_error_data@PLT
	jmp	.L35
	.cfi_endproc
.LFE1397:
	.size	TS_CONF_get_tsa_section, .-TS_CONF_get_tsa_section
	.section	.rodata.str1.1
.LC5:
	.string	"serial"
	.text
	.p2align 4
	.globl	TS_CONF_set_serial
	.type	TS_CONF_set_serial, @function
TS_CONF_set_serial:
.LFB1398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	leaq	.LC5(%rip), %rdx
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$8, %rsp
	call	NCONF_get_string@PLT
	testq	%rax, %rax
	je	.L42
.L40:
	endbr64
	movq	%rax, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	TS_RESP_CTX_set_serial_cb@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	movl	$106, %r8d
	movl	$136, %edx
	movl	$152, %esi
	movl	$47, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rsi
	xorl	%eax, %eax
	movl	$3, %edi
	leaq	.LC5(%rip), %rcx
	leaq	.LC4(%rip), %rdx
	call	ERR_add_error_data@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1398:
	.size	TS_CONF_set_serial, .-TS_CONF_set_serial
	.section	.rodata.str1.1
.LC6:
	.string	"crypto_device"
.LC7:
	.string	"builtin"
.LC8:
	.string	"engine:"
.LC9:
	.string	"chil"
	.text
	.p2align 4
	.globl	TS_CONF_set_crypto_device
	.type	TS_CONF_set_crypto_device, @function
TS_CONF_set_crypto_device:
.LFB1399:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	subq	$8, %rsp
	testq	%rdx, %rdx
	je	.L59
.L44:
	movl	$8, %ecx
	leaq	.LC7(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L60
.L58:
.L45:
	endbr64
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	movq	%r12, %rdi
	call	ENGINE_by_id@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L49
	movl	$5, %ecx
	leaq	.LC9(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L61
.L48:
	movl	$65535, %esi
	movq	%r14, %rdi
	call	ENGINE_set_default@PLT
	testl	%eax, %eax
	je	.L49
	movq	%r14, %rdi
	call	ENGINE_free@PLT
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L59:
	leaq	.LC6(%rip), %rdx
	call	NCONF_get_string@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L58
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L49:
	movl	$179, %r8d
	movl	$127, %edx
	movl	$146, %esi
	movl	$47, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdx
	movl	$2, %edi
	xorl	%eax, %eax
	leaq	.LC8(%rip), %rsi
	call	ERR_add_error_data@PLT
	movq	%r14, %rdi
	call	ENGINE_free@PLT
	movl	$112, %r8d
	movl	$135, %edx
	leaq	.LC1(%rip), %rcx
	movl	$151, %esi
	movl	$47, %edi
	call	ERR_put_error@PLT
	movq	%r13, %rsi
	xorl	%eax, %eax
	movl	$3, %edi
	leaq	.LC6(%rip), %rcx
	leaq	.LC4(%rip), %rdx
	call	ERR_add_error_data@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %edx
	movl	$100, %esi
	movq	%r14, %rdi
	call	ENGINE_ctrl@PLT
	jmp	.L48
	.cfi_endproc
.LFE1399:
	.size	TS_CONF_set_crypto_device, .-TS_CONF_set_crypto_device
	.p2align 4
	.globl	TS_CONF_set_default_engine
	.type	TS_CONF_set_default_engine, @function
TS_CONF_set_default_engine:
.LFB1400:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	$1, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	leaq	.LC7(%rip), %rdi
	movq	%r12, %rsi
	subq	$8, %rsp
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L75
	addq	$8, %rsp
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	movq	%r12, %rdi
	call	ENGINE_by_id@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L67
	movl	$5, %ecx
	leaq	.LC9(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L76
.L66:
	movl	$65535, %esi
	movq	%r13, %rdi
	call	ENGINE_set_default@PLT
	testl	%eax, %eax
	je	.L67
	movl	$1, %r14d
.L65:
	movq	%r13, %rdi
	call	ENGINE_free@PLT
	addq	$8, %rsp
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	movl	$179, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$127, %edx
	xorl	%r14d, %r14d
	movl	$146, %esi
	movl	$47, %edi
	call	ERR_put_error@PLT
	movq	%r12, %rdx
	movl	$2, %edi
	xorl	%eax, %eax
	leaq	.LC8(%rip), %rsi
	call	ERR_add_error_data@PLT
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L76:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %edx
	movl	$100, %esi
	movq	%r13, %rdi
	call	ENGINE_ctrl@PLT
	jmp	.L66
	.cfi_endproc
.LFE1400:
	.size	TS_CONF_set_default_engine, .-TS_CONF_set_default_engine
	.section	.rodata.str1.1
.LC10:
	.string	"signer_cert"
	.text
	.p2align 4
	.globl	TS_CONF_set_signer_cert
	.type	TS_CONF_set_signer_cert, @function
TS_CONF_set_signer_cert:
.LFB1401:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	subq	$8, %rsp
	testq	%rdx, %rdx
	je	.L90
.L78:
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_new_file@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L81
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	PEM_read_bio_X509_AUX@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L81
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	BIO_free@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	TS_RESP_CTX_set_signer_cert@PLT
	testl	%eax, %eax
	setne	%r13b
.L79:
	movq	%r12, %rdi
	call	X509_free@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	movl	$54, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$137, %edx
	xorl	%r12d, %r12d
	movl	$153, %esi
	movl	$47, %edi
	call	ERR_put_error@PLT
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	BIO_free@PLT
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L90:
	leaq	.LC10(%rip), %rdx
	movq	%rsi, %r13
	call	NCONF_get_string@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L78
	movl	$106, %r8d
	movl	$136, %edx
	movl	$152, %esi
	movl	$47, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r13, %rsi
	movl	$3, %edi
	xorl	%eax, %eax
	leaq	.LC10(%rip), %rcx
	leaq	.LC4(%rip), %rdx
	xorl	%r13d, %r13d
	call	ERR_add_error_data@PLT
	jmp	.L79
	.cfi_endproc
.LFE1401:
	.size	TS_CONF_set_signer_cert, .-TS_CONF_set_signer_cert
	.section	.rodata.str1.1
.LC11:
	.string	"certs"
	.text
	.p2align 4
	.globl	TS_CONF_set_certs
	.type	TS_CONF_set_certs, @function
TS_CONF_set_certs:
.LFB1402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	subq	$8, %rsp
	testq	%rdx, %rdx
	je	.L99
.L92:
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	TS_CONF_load_certs
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L93
	movq	%rax, %rsi
	movq	%r13, %rdi
	xorl	%r14d, %r14d
	call	TS_RESP_CTX_set_certs@PLT
	testl	%eax, %eax
	setne	%r14b
.L93:
	movq	X509_free@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	call	OPENSSL_sk_pop_free@PLT
	addq	$8, %rsp
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore_state
	leaq	.LC11(%rip), %rdx
	call	NCONF_get_string@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L92
	movl	$1, %r14d
	jmp	.L93
	.cfi_endproc
.LFE1402:
	.size	TS_CONF_set_certs, .-TS_CONF_set_certs
	.section	.rodata.str1.1
.LC12:
	.string	"signer_key"
	.text
	.p2align 4
	.globl	TS_CONF_set_signer_key
	.type	TS_CONF_set_signer_key, @function
TS_CONF_set_signer_key:
.LFB1403:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L113
.L101:
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_new_file@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L104
	movq	%r15, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	PEM_read_bio_PrivateKey@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L104
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	BIO_free@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	TS_RESP_CTX_set_signer_key@PLT
	testl	%eax, %eax
	setne	%r13b
.L102:
	movq	%r12, %rdi
	call	EVP_PKEY_free@PLT
	popq	%r12
	movl	%r13d, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	movl	$97, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$138, %edx
	xorl	%r12d, %r12d
	movl	$155, %esi
	movl	$47, %edi
	call	ERR_put_error@PLT
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	BIO_free@PLT
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L113:
	leaq	.LC12(%rip), %rdx
	movq	%rsi, %r13
	call	NCONF_get_string@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L101
	movl	$106, %r8d
	movl	$136, %edx
	movl	$152, %esi
	movl	$47, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r13, %rsi
	movl	$3, %edi
	xorl	%eax, %eax
	leaq	.LC12(%rip), %rcx
	leaq	.LC4(%rip), %rdx
	xorl	%r13d, %r13d
	call	ERR_add_error_data@PLT
	jmp	.L102
	.cfi_endproc
.LFE1403:
	.size	TS_CONF_set_signer_key, .-TS_CONF_set_signer_key
	.section	.rodata.str1.1
.LC13:
	.string	"signer_digest"
	.text
	.p2align 4
	.globl	TS_CONF_set_signer_digest
	.type	TS_CONF_set_signer_digest, @function
TS_CONF_set_signer_digest:
.LFB1404:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	testq	%rdx, %rdx
	je	.L121
	movq	%rdx, %rdi
.L115:
	call	EVP_get_digestbyname@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L122
.L116:
	endbr64
	movq	%r13, %rdi
	call	TS_RESP_CTX_set_signer_digest@PLT
	popq	%r12
	popq	%r13
	testl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	setne	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	leaq	.LC13(%rip), %rdx
	call	NCONF_get_string@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L115
	movl	$106, %r8d
	movl	$136, %edx
	movl	$152, %esi
	leaq	.LC1(%rip), %rcx
.L120:
	movl	$47, %edi
	call	ERR_put_error@PLT
	movq	%r12, %rsi
	xorl	%eax, %eax
	movl	$3, %edi
	leaq	.LC13(%rip), %rcx
	leaq	.LC4(%rip), %rdx
	call	ERR_add_error_data@PLT
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_restore_state
	movl	$112, %r8d
	movl	$135, %edx
	movl	$151, %esi
	leaq	.LC1(%rip), %rcx
	jmp	.L120
	.cfi_endproc
.LFE1404:
	.size	TS_CONF_set_signer_digest, .-TS_CONF_set_signer_digest
	.section	.rodata.str1.1
.LC14:
	.string	"default_policy"
	.text
	.p2align 4
	.globl	TS_CONF_set_def_policy
	.type	TS_CONF_set_def_policy, @function
TS_CONF_set_def_policy:
.LFB1405:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	subq	$8, %rsp
	testq	%rdx, %rdx
	je	.L130
.L124:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	OBJ_txt2obj@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L131
	movq	%rax, %rsi
	movq	%r14, %rdi
	xorl	%r13d, %r13d
	call	TS_RESP_CTX_set_def_policy@PLT
	testl	%eax, %eax
	setne	%r13b
.L125:
	movq	%r12, %rdi
	call	ASN1_OBJECT_free@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	leaq	.LC14(%rip), %rdx
	call	NCONF_get_string@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L124
	movl	$106, %r8d
	movl	$136, %edx
	movl	$152, %esi
	leaq	.LC1(%rip), %rcx
.L129:
	movl	$47, %edi
	call	ERR_put_error@PLT
	movq	%r13, %rsi
	movl	$3, %edi
	xorl	%eax, %eax
	leaq	.LC14(%rip), %rcx
	leaq	.LC4(%rip), %rdx
	xorl	%r13d, %r13d
	call	ERR_add_error_data@PLT
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L131:
	movl	$112, %r8d
	movl	$135, %edx
	movl	$151, %esi
	leaq	.LC1(%rip), %rcx
	jmp	.L129
	.cfi_endproc
.LFE1405:
	.size	TS_CONF_set_def_policy, .-TS_CONF_set_def_policy
	.section	.rodata.str1.1
.LC15:
	.string	"other_policies"
	.text
	.p2align 4
	.globl	TS_CONF_set_policies
	.type	TS_CONF_set_policies, @function
TS_CONF_set_policies:
.LFB1406:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	leaq	.LC15(%rip), %rdx
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	call	NCONF_get_string@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L133
	movq	%rax, %rdi
	call	X509V3_parse_list@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L146
.L133:
	xorl	%ebx, %ebx
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L136:
	xorl	%esi, %esi
	call	OBJ_txt2obj@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L146
.L137:
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	TS_RESP_CTX_add_policy@PLT
	testl	%eax, %eax
	je	.L134
	movq	%r12, %rdi
	addl	$1, %ebx
	call	ASN1_OBJECT_free@PLT
.L135:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L147
	movq	%r13, %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L136
	movq	8(%rax), %rdi
	xorl	%esi, %esi
	call	OBJ_txt2obj@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L137
.L146:
	movl	$112, %r8d
	movl	$135, %edx
	movl	$151, %esi
	movl	$47, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	movq	%r15, %rsi
	movl	$3, %edi
	leaq	.LC15(%rip), %rcx
	leaq	.LC4(%rip), %rdx
	call	ERR_add_error_data@PLT
	xorl	%eax, %eax
.L134:
	movq	X509V3_conf_free@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	movl	%eax, -52(%rbp)
	call	OPENSSL_sk_pop_free@PLT
	movl	-52(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	.cfi_restore_state
	movl	$1, %eax
	jmp	.L134
	.cfi_endproc
.LFE1406:
	.size	TS_CONF_set_policies, .-TS_CONF_set_policies
	.section	.rodata.str1.1
.LC16:
	.string	"digests"
	.text
	.p2align 4
	.globl	TS_CONF_set_digests
	.type	TS_CONF_set_digests, @function
TS_CONF_set_digests:
.LFB1407:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	leaq	.LC16(%rip), %rdx
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	call	NCONF_get_string@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L164
	movq	%rax, %rdi
	call	X509V3_parse_list@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L163
	movq	%rax, %rdi
	xorl	%ebx, %ebx
	call	OPENSSL_sk_num@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	jne	.L152
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L153:
	call	EVP_get_digestbyname@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L163
.L154:
	movq	%r13, %rdi
	call	TS_RESP_CTX_add_md@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L150
	addl	$1, %ebx
.L152:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L166
	movq	%r12, %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L153
	movq	8(%rax), %rdi
	call	EVP_get_digestbyname@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L154
	.p2align 4,,10
	.p2align 3
.L163:
	movl	$112, %r8d
	movl	$135, %edx
	movl	$151, %esi
	leaq	.LC1(%rip), %rcx
.L162:
	movl	$47, %edi
	xorl	%r15d, %r15d
	call	ERR_put_error@PLT
	movq	%r14, %rsi
	movl	$3, %edi
	xorl	%eax, %eax
	leaq	.LC16(%rip), %rcx
	leaq	.LC4(%rip), %rdx
	call	ERR_add_error_data@PLT
.L150:
	movq	X509V3_conf_free@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	call	OPENSSL_sk_pop_free@PLT
	addq	$8, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L166:
	.cfi_restore_state
	movl	$1, %r15d
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L164:
	movl	$106, %r8d
	movl	$136, %edx
	movl	$152, %esi
	leaq	.LC1(%rip), %rcx
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L165:
	movl	$112, %r8d
	movl	$135, %edx
	movl	$151, %esi
	movl	$47, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r14, %rsi
	movl	$3, %edi
	xorl	%eax, %eax
	leaq	.LC16(%rip), %rcx
	leaq	.LC4(%rip), %rdx
	call	ERR_add_error_data@PLT
	jmp	.L150
	.cfi_endproc
.LFE1407:
	.size	TS_CONF_set_digests, .-TS_CONF_set_digests
	.section	.rodata.str1.1
.LC17:
	.string	"accuracy"
.LC18:
	.string	"secs"
.LC19:
	.string	"millisecs"
.LC20:
	.string	"microsecs"
	.text
	.p2align 4
	.globl	TS_CONF_set_accuracy
	.type	TS_CONF_set_accuracy, @function
TS_CONF_set_accuracy:
.LFB1408:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -64(%rbp)
	leaq	.LC17(%rip), %rdx
	movq	%rsi, -72(%rbp)
	call	NCONF_get_string@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L168
	movq	%rax, %rdi
	call	X509V3_parse_list@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L174
.L168:
	movl	$0, -56(%rbp)
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	leaq	.LC18(%rip), %r13
	movl	$0, -52(%rbp)
	leaq	.LC19(%rip), %r15
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L190:
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.L172
	movl	$10, %edx
	xorl	%esi, %esi
	call	strtol@PLT
	movl	%eax, %r14d
.L172:
	addl	$1, %ebx
.L170:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L189
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	movl	$5, %ecx
	movq	%r13, %rdi
	movq	8(%rax), %r8
	movq	%r8, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L190
	movl	$10, %ecx
	movq	%r8, %rsi
	movq	%r15, %rdi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	jne	.L173
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.L172
	movl	$10, %edx
	xorl	%esi, %esi
	call	strtol@PLT
	movl	%eax, -52(%rbp)
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L173:
	movl	$10, %ecx
	movq	%r8, %rsi
	leaq	.LC20(%rip), %rdi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	jne	.L174
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.L172
	movl	$10, %edx
	xorl	%esi, %esi
	call	strtol@PLT
	movl	%eax, -56(%rbp)
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L189:
	movl	-56(%rbp), %ecx
	movl	-52(%rbp), %edx
	movl	%r14d, %esi
	xorl	%r13d, %r13d
	movq	-64(%rbp), %rdi
	call	TS_RESP_CTX_set_accuracy@PLT
	testl	%eax, %eax
	setne	%r13b
.L169:
	movq	X509V3_conf_free@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	call	OPENSSL_sk_pop_free@PLT
	addq	$40, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L174:
	.cfi_restore_state
	movl	$112, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$135, %edx
	xorl	%r13d, %r13d
	movl	$151, %esi
	movl	$47, %edi
	call	ERR_put_error@PLT
	movq	-72(%rbp), %rsi
	movl	$3, %edi
	xorl	%eax, %eax
	leaq	.LC17(%rip), %rcx
	leaq	.LC4(%rip), %rdx
	call	ERR_add_error_data@PLT
	jmp	.L169
	.cfi_endproc
.LFE1408:
	.size	TS_CONF_set_accuracy, .-TS_CONF_set_accuracy
	.section	.rodata.str1.1
.LC21:
	.string	"clock_precision_digits"
	.text
	.p2align 4
	.globl	TS_CONF_set_clock_precision_digits
	.type	TS_CONF_set_clock_precision_digits, @function
TS_CONF_set_clock_precision_digits:
.LFB1409:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	leaq	.LC21(%rip), %rdx
	pushq	%r12
	leaq	-32(%rbp), %rcx
	.cfi_offset 12, -32
	movq	%rsi, %r12
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$0, -32(%rbp)
	call	NCONF_get_number_e@PLT
	testl	%eax, %eax
	jne	.L192
	movq	$0, -32(%rbp)
	xorl	%esi, %esi
.L193:
	movq	%r13, %rdi
	call	TS_RESP_CTX_set_clock_precision_digits@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
.L191:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L197
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L192:
	.cfi_restore_state
	movq	-32(%rbp), %rsi
	cmpq	$6, %rsi
	jbe	.L193
	movl	$112, %r8d
	movl	$135, %edx
	movl	$151, %esi
	movl	$47, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	movq	%r12, %rsi
	movl	$3, %edi
	leaq	.LC21(%rip), %rcx
	leaq	.LC4(%rip), %rdx
	call	ERR_add_error_data@PLT
	xorl	%eax, %eax
	jmp	.L191
.L197:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1409:
	.size	TS_CONF_set_clock_precision_digits, .-TS_CONF_set_clock_precision_digits
	.section	.rodata.str1.1
.LC22:
	.string	"ordering"
.LC23:
	.string	"yes"
	.text
	.p2align 4
	.globl	TS_CONF_set_ordering
	.type	TS_CONF_set_ordering, @function
TS_CONF_set_ordering:
.LFB1411:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	$1, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	leaq	.LC22(%rip), %rdx
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$8, %rsp
	call	NCONF_get_string@PLT
	testq	%rax, %rax
	je	.L198
	movl	$4, %ecx
	leaq	.LC23(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L208
	cmpb	$110, (%rax)
	je	.L209
.L204:
	movl	$112, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$135, %edx
	xorl	%r14d, %r14d
	movl	$151, %esi
	movl	$47, %edi
	call	ERR_put_error@PLT
	movq	%r12, %rsi
	movl	$3, %edi
	xorl	%eax, %eax
	leaq	.LC22(%rip), %rcx
	leaq	.LC4(%rip), %rdx
	call	ERR_add_error_data@PLT
.L198:
	addq	$8, %rsp
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	.cfi_restore_state
	movq	%r13, %rdi
	movl	$2, %esi
	call	TS_RESP_CTX_add_flags@PLT
	addq	$8, %rsp
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	.cfi_restore_state
	cmpb	$111, 1(%rax)
	jne	.L204
	cmpb	$0, 2(%rax)
	jne	.L204
	addq	$8, %rsp
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1411:
	.size	TS_CONF_set_ordering, .-TS_CONF_set_ordering
	.section	.rodata.str1.1
.LC24:
	.string	"tsa_name"
	.text
	.p2align 4
	.globl	TS_CONF_set_tsa_name
	.type	TS_CONF_set_tsa_name, @function
TS_CONF_set_tsa_name:
.LFB1412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	$1, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	leaq	.LC24(%rip), %rdx
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$8, %rsp
	call	NCONF_get_string@PLT
	testq	%rax, %rax
	je	.L210
	movl	$4, %ecx
	leaq	.LC23(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L220
	cmpb	$110, (%rax)
	je	.L221
.L216:
	movl	$112, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$135, %edx
	xorl	%r14d, %r14d
	movl	$151, %esi
	movl	$47, %edi
	call	ERR_put_error@PLT
	movq	%r12, %rsi
	movl	$3, %edi
	xorl	%eax, %eax
	leaq	.LC24(%rip), %rcx
	leaq	.LC4(%rip), %rdx
	call	ERR_add_error_data@PLT
.L210:
	addq	$8, %rsp
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L220:
	.cfi_restore_state
	movq	%r13, %rdi
	movl	$1, %esi
	call	TS_RESP_CTX_add_flags@PLT
	addq	$8, %rsp
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L221:
	.cfi_restore_state
	cmpb	$111, 1(%rax)
	jne	.L216
	cmpb	$0, 2(%rax)
	jne	.L216
	addq	$8, %rsp
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1412:
	.size	TS_CONF_set_tsa_name, .-TS_CONF_set_tsa_name
	.section	.rodata.str1.1
.LC25:
	.string	"ess_cert_id_chain"
	.text
	.p2align 4
	.globl	TS_CONF_set_ess_cert_id_chain
	.type	TS_CONF_set_ess_cert_id_chain, @function
TS_CONF_set_ess_cert_id_chain:
.LFB1413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	$1, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	leaq	.LC25(%rip), %rdx
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$8, %rsp
	call	NCONF_get_string@PLT
	testq	%rax, %rax
	je	.L222
	movl	$4, %ecx
	leaq	.LC23(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L232
	cmpb	$110, (%rax)
	je	.L233
.L228:
	movl	$112, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$135, %edx
	xorl	%r14d, %r14d
	movl	$151, %esi
	movl	$47, %edi
	call	ERR_put_error@PLT
	movq	%r12, %rsi
	movl	$3, %edi
	xorl	%eax, %eax
	leaq	.LC25(%rip), %rcx
	leaq	.LC4(%rip), %rdx
	call	ERR_add_error_data@PLT
.L222:
	addq	$8, %rsp
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L232:
	.cfi_restore_state
	movq	%r13, %rdi
	movl	$4, %esi
	call	TS_RESP_CTX_add_flags@PLT
	addq	$8, %rsp
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L233:
	.cfi_restore_state
	cmpb	$111, 1(%rax)
	jne	.L228
	cmpb	$0, 2(%rax)
	jne	.L228
	addq	$8, %rsp
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1413:
	.size	TS_CONF_set_ess_cert_id_chain, .-TS_CONF_set_ess_cert_id_chain
	.section	.rodata.str1.1
.LC26:
	.string	"sha1"
.LC27:
	.string	"ess_cert_id_alg"
	.text
	.p2align 4
	.globl	TS_CONF_set_ess_cert_id_digest
	.type	TS_CONF_set_ess_cert_id_digest, @function
TS_CONF_set_ess_cert_id_digest:
.LFB1414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	leaq	.LC27(%rip), %rdx
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	call	NCONF_get_string@PLT
	testq	%rax, %rax
	movq	%rax, %rdi
	leaq	.LC26(%rip), %rax
	cmove	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	testq	%rax, %rax
	je	.L239
.L237:
	endbr64
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	TS_RESP_CTX_set_ess_cert_id_digest@PLT
	popq	%r12
	popq	%r13
	testl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	setne	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L239:
	.cfi_restore_state
	movl	$112, %r8d
	movl	$135, %edx
	movl	$151, %esi
	movl	$47, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rsi
	xorl	%eax, %eax
	movl	$3, %edi
	leaq	.LC27(%rip), %rcx
	leaq	.LC4(%rip), %rdx
	call	ERR_add_error_data@PLT
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1414:
	.size	TS_CONF_set_ess_cert_id_digest, .-TS_CONF_set_ess_cert_id_digest
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
