	.file	"ec2_smpl.c"
	.text
	.p2align 4
	.globl	ec_GF2m_simple_points_make_affine
	.type	ec_GF2m_simple_points_make_affine, @function
ec_GF2m_simple_points_make_affine:
.LFB399:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L13
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$8, %rsp
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L19:
	addq	$1, %rbx
	cmpq	%rbx, %r12
	je	.L18
.L4:
	movq	(%r14), %rax
	movq	(%r15,%rbx,8), %rsi
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	*224(%rax)
	testl	%eax, %eax
	jne	.L19
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
.L13:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE399:
	.size	ec_GF2m_simple_points_make_affine, .-ec_GF2m_simple_points_make_affine
	.p2align 4
	.globl	ec_GF2m_simple_group_init
	.type	ec_GF2m_simple_group_init, @function
ec_GF2m_simple_group_init:
.LFB377:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	BN_new@PLT
	movq	%rax, 64(%rbx)
	call	BN_new@PLT
	movq	%rax, 96(%rbx)
	call	BN_new@PLT
	movq	64(%rbx), %rdi
	movq	%rax, 104(%rbx)
	testq	%rdi, %rdi
	je	.L21
	cmpq	$0, 96(%rbx)
	je	.L21
	movl	$1, %r8d
	testq	%rax, %rax
	je	.L21
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	call	BN_free@PLT
	movq	96(%rbx), %rdi
	call	BN_free@PLT
	movq	104(%rbx), %rdi
	call	BN_free@PLT
	addq	$8, %rsp
	xorl	%r8d, %r8d
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE377:
	.size	ec_GF2m_simple_group_init, .-ec_GF2m_simple_group_init
	.p2align 4
	.globl	ec_GF2m_simple_group_finish
	.type	ec_GF2m_simple_group_finish, @function
ec_GF2m_simple_group_finish:
.LFB378:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	64(%rdi), %rdi
	call	BN_free@PLT
	movq	96(%rbx), %rdi
	call	BN_free@PLT
	movq	104(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	BN_free@PLT
	.cfi_endproc
.LFE378:
	.size	ec_GF2m_simple_group_finish, .-ec_GF2m_simple_group_finish
	.p2align 4
	.globl	ec_GF2m_simple_point_init
	.type	ec_GF2m_simple_point_init, @function
ec_GF2m_simple_point_init:
.LFB385:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	BN_new@PLT
	movq	%rax, 16(%rbx)
	call	BN_new@PLT
	movq	%rax, 24(%rbx)
	call	BN_new@PLT
	movq	16(%rbx), %rdi
	movq	%rax, 32(%rbx)
	testq	%rdi, %rdi
	je	.L33
	cmpq	$0, 24(%rbx)
	je	.L33
	movl	$1, %r8d
	testq	%rax, %rax
	je	.L33
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	call	BN_free@PLT
	movq	24(%rbx), %rdi
	call	BN_free@PLT
	movq	32(%rbx), %rdi
	call	BN_free@PLT
	addq	$8, %rsp
	xorl	%r8d, %r8d
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE385:
	.size	ec_GF2m_simple_point_init, .-ec_GF2m_simple_point_init
	.p2align 4
	.globl	ec_GF2m_simple_point_finish
	.type	ec_GF2m_simple_point_finish, @function
ec_GF2m_simple_point_finish:
.LFB386:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	call	BN_free@PLT
	movq	24(%rbx), %rdi
	call	BN_free@PLT
	movq	32(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	BN_free@PLT
	.cfi_endproc
.LFE386:
	.size	ec_GF2m_simple_point_finish, .-ec_GF2m_simple_point_finish
	.p2align 4
	.globl	ec_GF2m_simple_group_clear_finish
	.type	ec_GF2m_simple_group_clear_finish, @function
ec_GF2m_simple_group_clear_finish:
.LFB379:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	64(%rdi), %rdi
	call	BN_clear_free@PLT
	movq	96(%rbx), %rdi
	call	BN_clear_free@PLT
	movq	104(%rbx), %rdi
	call	BN_clear_free@PLT
	pxor	%xmm0, %xmm0
	movabsq	$-4294967296, %rax
	movq	%rax, 88(%rbx)
	movups	%xmm0, 72(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE379:
	.size	ec_GF2m_simple_group_clear_finish, .-ec_GF2m_simple_group_clear_finish
	.p2align 4
	.globl	ec_GF2m_simple_point_clear_finish
	.type	ec_GF2m_simple_point_clear_finish, @function
ec_GF2m_simple_point_clear_finish:
.LFB387:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	call	BN_clear_free@PLT
	movq	24(%rbx), %rdi
	call	BN_clear_free@PLT
	movq	32(%rbx), %rdi
	call	BN_clear_free@PLT
	movl	$0, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE387:
	.size	ec_GF2m_simple_point_clear_finish, .-ec_GF2m_simple_point_clear_finish
	.p2align 4
	.globl	ec_GF2m_simple_group_get_degree
	.type	ec_GF2m_simple_group_get_degree, @function
ec_GF2m_simple_group_get_degree:
.LFB383:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	64(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	BN_num_bits@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	subl	$1, %eax
	ret
	.cfi_endproc
.LFE383:
	.size	ec_GF2m_simple_group_get_degree, .-ec_GF2m_simple_group_get_degree
	.p2align 4
	.globl	ec_GF2m_simple_is_at_infinity
	.type	ec_GF2m_simple_is_at_infinity, @function
ec_GF2m_simple_is_at_infinity:
.LFB395:
	.cfi_startproc
	endbr64
	movq	32(%rsi), %rdi
	jmp	BN_is_zero@PLT
	.cfi_endproc
.LFE395:
	.size	ec_GF2m_simple_is_at_infinity, .-ec_GF2m_simple_is_at_infinity
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ec/ec2_smpl.c"
	.text
	.p2align 4
	.globl	ec_GF2m_simple_group_check_discriminant
	.type	ec_GF2m_simple_group_check_discriminant, @function
ec_GF2m_simple_group_check_discriminant:
.LFB384:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L63
.L52:
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L53
	movq	104(%rbx), %rsi
	leaq	72(%rbx), %rdx
	movq	%rax, %rdi
	call	BN_GF2m_mod_arr@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.L64
.L53:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	movq	%r15, %rdi
	call	BN_CTX_free@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
	movq	%r14, %rdi
	xorl	%r13d, %r13d
	call	BN_is_zero@PLT
	testl	%eax, %eax
	sete	%r13b
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L63:
	call	BN_CTX_new@PLT
	movq	%rax, %r12
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L52
	movl	$184, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$159, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L53
	.cfi_endproc
.LFE384:
	.size	ec_GF2m_simple_group_check_discriminant, .-ec_GF2m_simple_group_check_discriminant
	.p2align 4
	.globl	ec_GF2m_simple_point_set_to_infinity
	.type	ec_GF2m_simple_point_set_to_infinity, @function
ec_GF2m_simple_point_set_to_infinity:
.LFB389:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	32(%rsi), %rdi
	movl	$0, 40(%rsi)
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	BN_set_word@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE389:
	.size	ec_GF2m_simple_point_set_to_infinity, .-ec_GF2m_simple_point_set_to_infinity
	.p2align 4
	.type	ec_GF2m_simple_ladder_step, @function
ec_GF2m_simple_ladder_step:
.LFB404:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	16(%rdx), %rcx
	movq	32(%rsi), %rdx
	movq	24(%rsi), %rsi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L70
	movq	(%r12), %rax
	movq	32(%r13), %rcx
	movq	%r14, %r8
	movq	%r12, %rdi
	movq	16(%rbx), %rdx
	movq	16(%r13), %rsi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L70
	movq	(%r12), %rax
	movq	32(%rbx), %rdx
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	24(%r13), %rsi
	call	*272(%rax)
	testl	%eax, %eax
	je	.L70
	movq	(%r12), %rax
	movq	16(%rbx), %rdx
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	32(%rbx), %rsi
	call	*272(%rax)
	testl	%eax, %eax
	jne	.L108
	.p2align 4,,10
	.p2align 3
.L70:
	xorl	%eax, %eax
.L67:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	movq	16(%r13), %rdx
	movq	24(%rbx), %rsi
	movq	32(%r13), %rdi
	call	BN_GF2m_add@PLT
	testl	%eax, %eax
	je	.L70
	movq	32(%r13), %rsi
	movq	(%r12), %rax
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	%rsi, %rdx
	call	*272(%rax)
	testl	%eax, %eax
	je	.L70
	movq	16(%r13), %rsi
	movq	(%r12), %rax
	movq	%r14, %r8
	movq	%r12, %rdi
	movq	24(%rbx), %rdx
	movq	%rsi, %rcx
	call	*264(%rax)
	testl	%eax, %eax
	je	.L70
	movq	(%r12), %rax
	movq	16(%r15), %rcx
	movq	%r14, %r8
	movq	%r12, %rdi
	movq	32(%r13), %rdx
	movq	24(%rbx), %rsi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L70
	movq	16(%r13), %rdi
	movq	24(%rbx), %rdx
	movq	%rdi, %rsi
	call	BN_GF2m_add@PLT
	testl	%eax, %eax
	je	.L70
	movq	(%r12), %rax
	movq	32(%rbx), %rdx
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	24(%rbx), %rsi
	call	*272(%rax)
	testl	%eax, %eax
	je	.L70
	movq	32(%rbx), %rsi
	movq	(%r12), %rax
	movq	%r14, %r8
	movq	%r12, %rdi
	movq	24(%r13), %rcx
	movq	%rsi, %rdx
	call	*264(%rax)
	testl	%eax, %eax
	je	.L70
	movq	24(%r13), %rsi
	movq	(%r12), %rax
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	%rsi, %rdx
	call	*272(%rax)
	testl	%eax, %eax
	je	.L70
	movq	24(%r13), %rsi
	movq	(%r12), %rax
	movq	%r14, %r8
	movq	%r12, %rdi
	movq	104(%r12), %rcx
	movq	%rsi, %rdx
	call	*264(%rax)
	testl	%eax, %eax
	je	.L70
	movq	24(%r13), %rdx
	movq	24(%rbx), %rsi
	movq	16(%rbx), %rdi
	call	BN_GF2m_add@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L67
	.cfi_endproc
.LFE404:
	.size	ec_GF2m_simple_ladder_step, .-ec_GF2m_simple_ladder_step
	.p2align 4
	.globl	ec_GF2m_simple_field_mul
	.type	ec_GF2m_simple_field_mul, @function
ec_GF2m_simple_field_mul:
.LFB400:
	.cfi_startproc
	endbr64
	movq	%rdi, %r9
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	leaq	72(%r9), %rcx
	jmp	BN_GF2m_mod_mul_arr@PLT
	.cfi_endproc
.LFE400:
	.size	ec_GF2m_simple_field_mul, .-ec_GF2m_simple_field_mul
	.p2align 4
	.globl	ec_GF2m_simple_field_sqr
	.type	ec_GF2m_simple_field_sqr, @function
ec_GF2m_simple_field_sqr:
.LFB401:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	leaq	72(%r8), %rdx
	jmp	BN_GF2m_mod_sqr_arr@PLT
	.cfi_endproc
.LFE401:
	.size	ec_GF2m_simple_field_sqr, .-ec_GF2m_simple_field_sqr
	.p2align 4
	.globl	ec_GF2m_simple_field_div
	.type	ec_GF2m_simple_field_div, @function
ec_GF2m_simple_field_div:
.LFB402:
	.cfi_startproc
	endbr64
	movq	%rdi, %r9
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	movq	64(%r9), %rcx
	jmp	BN_GF2m_mod_div@PLT
	.cfi_endproc
.LFE402:
	.size	ec_GF2m_simple_field_div, .-ec_GF2m_simple_field_div
	.p2align 4
	.globl	ec_GF2m_simple_group_get_curve
	.type	ec_GF2m_simple_group_get_curve, @function
ec_GF2m_simple_group_get_curve:
.LFB382:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L116
	movq	%rsi, %rdi
	movq	64(%rbx), %rsi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L119
.L116:
	testq	%r12, %r12
	je	.L115
	movq	96(%rbx), %rsi
	movq	%r12, %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L119
.L115:
	movl	$1, %eax
	testq	%r13, %r13
	je	.L112
	movq	104(%rbx), %rsi
	movq	%r13, %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
.L112:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE382:
	.size	ec_GF2m_simple_group_get_curve, .-ec_GF2m_simple_group_get_curve
	.p2align 4
	.globl	ec_GF2m_simple_point_copy
	.type	ec_GF2m_simple_point_copy, @function
ec_GF2m_simple_point_copy:
.LFB388:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	16(%rsi), %rsi
	movq	16(%rdi), %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L127
	movq	24(%r12), %rsi
	movq	24(%rbx), %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L127
	movq	32(%r12), %rsi
	movq	32(%rbx), %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L127
	movl	40(%r12), %eax
	movl	%eax, 40(%rbx)
	movl	8(%r12), %eax
	movl	%eax, 8(%rbx)
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE388:
	.size	ec_GF2m_simple_point_copy, .-ec_GF2m_simple_point_copy
	.p2align 4
	.globl	ec_GF2m_simple_group_copy
	.type	ec_GF2m_simple_group_copy, @function
ec_GF2m_simple_group_copy:
.LFB380:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	64(%rsi), %rsi
	movq	64(%rdi), %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L138
	movq	96(%r12), %rsi
	movq	96(%rbx), %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L138
	movq	104(%r12), %rsi
	movq	104(%rbx), %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L138
	movdqu	72(%r12), %xmm0
	movl	72(%r12), %eax
	movq	96(%rbx), %rdi
	movups	%xmm0, 72(%rbx)
	movl	88(%r12), %edx
	leal	126(%rax), %esi
	addl	$63, %eax
	cmovns	%eax, %esi
	movl	%edx, 88(%rbx)
	movl	92(%r12), %edx
	sarl	$6, %esi
	movl	%edx, 92(%rbx)
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L138
	movl	72(%rbx), %eax
	movq	104(%rbx), %rdi
	leal	126(%rax), %esi
	addl	$63, %eax
	cmovns	%eax, %esi
	sarl	$6, %esi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L138
	movq	96(%rbx), %rdi
	call	bn_set_all_zero@PLT
	movq	104(%rbx), %rdi
	call	bn_set_all_zero@PLT
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE380:
	.size	ec_GF2m_simple_group_copy, .-ec_GF2m_simple_group_copy
	.p2align 4
	.globl	ec_GF2m_simple_group_set_curve
	.type	ec_GF2m_simple_group_set_curve, @function
ec_GF2m_simple_group_set_curve:
.LFB381:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	64(%rdi), %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L170
	movq	64(%rbx), %rdi
	leaq	72(%rbx), %r14
	movl	$6, %edx
	movq	%r14, %rsi
	call	BN_GF2m_poly2arr@PLT
	andl	$-3, %eax
	cmpl	$4, %eax
	jne	.L171
	movq	96(%rbx), %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	BN_GF2m_mod_arr@PLT
	testl	%eax, %eax
	je	.L170
	movl	72(%rbx), %eax
	movq	96(%rbx), %rdi
	leal	126(%rax), %esi
	addl	$63, %eax
	cmovns	%eax, %esi
	sarl	$6, %esi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L170
	movq	96(%rbx), %rdi
	call	bn_set_all_zero@PLT
	movq	104(%rbx), %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	BN_GF2m_mod_arr@PLT
	testl	%eax, %eax
	je	.L170
	movl	72(%rbx), %eax
	movq	104(%rbx), %rdi
	leal	126(%rax), %esi
	addl	$63, %eax
	cmovns	%eax, %esi
	sarl	$6, %esi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L170
.L154:
	endbr64
	movq	104(%rbx), %rdi
	call	bn_set_all_zero@PLT
	movl	$1, %eax
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L171:
	movl	$106, %r8d
	movl	$131, %edx
	movl	$195, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L170:
	xorl	%eax, %eax
.L152:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE381:
	.size	ec_GF2m_simple_group_set_curve, .-ec_GF2m_simple_group_set_curve
	.p2align 4
	.globl	ec_GF2m_simple_point_set_affine_coordinates
	.type	ec_GF2m_simple_point_set_affine_coordinates, @function
ec_GF2m_simple_point_set_affine_coordinates:
.LFB390:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	testq	%rdx, %rdx
	je	.L178
	movq	%rcx, %r12
	testq	%rcx, %rcx
	je	.L178
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	movq	16(%rbx), %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L177
	movq	16(%rbx), %rdi
	xorl	%esi, %esi
	call	BN_set_negative@PLT
	movq	24(%rbx), %rdi
	movq	%r12, %rsi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L177
	movq	24(%rbx), %rdi
	xorl	%esi, %esi
	call	BN_set_negative@PLT
	call	BN_value_one@PLT
	movq	32(%rbx), %rdi
	movq	%rax, %rsi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L177
	movq	32(%rbx), %rdi
	xorl	%esi, %esi
	call	BN_set_negative@PLT
	movl	$1, 40(%rbx)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	movl	$286, %r8d
	movl	$67, %edx
	movl	$163, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE390:
	.size	ec_GF2m_simple_point_set_affine_coordinates, .-ec_GF2m_simple_point_set_affine_coordinates
	.p2align 4
	.globl	ec_GF2m_simple_point_get_affine_coordinates
	.type	ec_GF2m_simple_point_get_affine_coordinates, @function
ec_GF2m_simple_point_get_affine_coordinates:
.LFB391:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	call	EC_POINT_is_at_infinity@PLT
	testl	%eax, %eax
	jne	.L201
	call	BN_value_one@PLT
	movq	32(%rbx), %rdi
	movq	%rax, %rsi
	call	BN_cmp@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L202
	testq	%r13, %r13
	je	.L190
	movq	16(%rbx), %rsi
	movq	%r13, %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L186
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	BN_set_negative@PLT
.L190:
	testq	%r14, %r14
	je	.L193
	movq	24(%rbx), %rsi
	movq	%r14, %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L186
	xorl	%esi, %esi
	movq	%r14, %rdi
	movl	$1, %r12d
	call	BN_set_negative@PLT
.L186:
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L201:
	.cfi_restore_state
	xorl	%r12d, %r12d
	movl	$319, %r8d
	movl	$106, %edx
	movl	$162, %esi
	leaq	.LC0(%rip), %rcx
	movl	$16, %edi
	call	ERR_put_error@PLT
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L193:
	.cfi_restore_state
	movl	$1, %r12d
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L202:
	.cfi_restore_state
	xorl	%r12d, %r12d
	movl	$325, %r8d
	movl	$66, %edx
	movl	$162, %esi
	leaq	.LC0(%rip), %rcx
	movl	$16, %edi
	call	ERR_put_error@PLT
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE391:
	.size	ec_GF2m_simple_point_get_affine_coordinates, .-ec_GF2m_simple_point_get_affine_coordinates
	.p2align 4
	.globl	ec_GF2m_simple_cmp
	.type	ec_GF2m_simple_cmp, @function
ec_GF2m_simple_cmp:
.LFB397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	call	EC_POINT_is_at_infinity@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	testl	%eax, %eax
	jne	.L223
	call	EC_POINT_is_at_infinity@PLT
	testl	%eax, %eax
	jne	.L208
	movl	40(%r13), %edx
	testl	%edx, %edx
	je	.L207
	movl	40(%r14), %eax
	testl	%eax, %eax
	jne	.L224
.L207:
	xorl	%ebx, %ebx
	testq	%r12, %r12
	je	.L225
.L209:
	movq	%r12, %rdi
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	BN_CTX_get@PLT
	testq	%rax, %rax
	je	.L212
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %rdx
	movq	%r12, %r8
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, -80(%rbp)
	call	EC_POINT_get_affine_coordinates@PLT
	testl	%eax, %eax
	jne	.L226
.L212:
	movl	$-1, %r13d
.L211:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	movq	%rbx, %rdi
	call	BN_CTX_free@PLT
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L208:
	movl	$1, %r13d
.L203:
	addq	$40, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	.cfi_restore_state
	call	EC_POINT_is_at_infinity@PLT
	xorl	%r13d, %r13d
	testl	%eax, %eax
	sete	%r13b
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L226:
	movq	-80(%rbp), %r10
	movq	-72(%rbp), %r13
	movq	%r12, %r8
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%r10, %rcx
	movq	%r13, %rdx
	call	EC_POINT_get_affine_coordinates@PLT
	testl	%eax, %eax
	je	.L212
	movq	-56(%rbp), %rdi
	movq	%r13, %rsi
	movl	$1, %r13d
	call	BN_cmp@PLT
	movq	-80(%rbp), %r10
	testl	%eax, %eax
	jne	.L211
	movq	-64(%rbp), %rdi
	movq	%r10, %rsi
	xorl	%r13d, %r13d
	call	BN_cmp@PLT
	testl	%eax, %eax
	setne	%r13b
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L225:
	call	BN_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L214
	movq	%rax, %rbx
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L224:
	movq	16(%r14), %rsi
	movq	16(%r13), %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	jne	.L208
	movq	24(%r13), %rdi
	movq	24(%r14), %rsi
	xorl	%r13d, %r13d
	call	BN_cmp@PLT
	testl	%eax, %eax
	setne	%r13b
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L214:
	movl	$-1, %r13d
	jmp	.L203
	.cfi_endproc
.LFE397:
	.size	ec_GF2m_simple_cmp, .-ec_GF2m_simple_cmp
	.p2align 4
	.globl	ec_GF2m_simple_make_affine
	.type	ec_GF2m_simple_make_affine, @function
ec_GF2m_simple_make_affine:
.LFB398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	40(%rsi), %eax
	testl	%eax, %eax
	je	.L228
.L230:
	movl	$1, %r13d
.L227:
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L228:
	.cfi_restore_state
	movq	%rdi, %r14
	movq	%rsi, %rbx
	movq	%rdx, %r12
	call	EC_POINT_is_at_infinity@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.L230
	xorl	%r15d, %r15d
	testq	%r12, %r12
	je	.L252
.L231:
	movq	%r12, %rdi
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	BN_CTX_get@PLT
	testq	%rax, %rax
	je	.L233
	movq	-56(%rbp), %rdx
	movq	%r12, %r8
	movq	%rax, %rcx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	call	EC_POINT_get_affine_coordinates@PLT
	testl	%eax, %eax
	jne	.L253
.L233:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	movq	%r15, %rdi
	call	BN_CTX_free@PLT
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L253:
	movq	16(%rbx), %rdi
	movq	-56(%rbp), %rsi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L233
	movq	-64(%rbp), %r10
	movq	24(%rbx), %rdi
	movq	%r10, %rsi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L233
	movq	32(%rbx), %rdi
	movl	$1, %esi
	call	BN_set_word@PLT
	testl	%eax, %eax
	je	.L233
	movl	$1, 40(%rbx)
	movl	$1, %r13d
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L252:
	call	BN_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L227
	movq	%rax, %r15
	jmp	.L231
	.cfi_endproc
.LFE398:
	.size	ec_GF2m_simple_make_affine, .-ec_GF2m_simple_make_affine
	.p2align 4
	.globl	ec_GF2m_simple_is_on_curve
	.type	ec_GF2m_simple_is_on_curve, @function
ec_GF2m_simple_is_on_curve:
.LFB396:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	$1, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	call	EC_POINT_is_at_infinity@PLT
	testl	%eax, %eax
	jne	.L254
	movq	0(%r13), %rax
	movq	264(%rax), %rcx
	movq	272(%rax), %rax
	movq	%rax, -64(%rbp)
	movl	40(%rbx), %eax
	movq	%rcx, -56(%rbp)
	testl	%eax, %eax
	je	.L258
	xorl	%r14d, %r14d
	testq	%r12, %r12
	je	.L289
.L257:
	movq	%r12, %rdi
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	BN_CTX_get@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L261
	movq	96(%r13), %rdx
	movq	16(%rbx), %rsi
	movq	%rax, %rdi
	call	BN_GF2m_add@PLT
	testl	%eax, %eax
	je	.L261
	movq	16(%rbx), %rcx
	movq	-56(%rbp), %rax
	movq	%r12, %r8
	movq	%r15, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L290
.L261:
	movl	$-1, %r15d
.L260:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	movq	%r14, %rdi
	call	BN_CTX_free@PLT
.L254:
	addq	$40, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L289:
	.cfi_restore_state
	call	BN_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L258
	movq	%rax, %r14
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L290:
	movq	24(%rbx), %rdx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	BN_GF2m_add@PLT
	testl	%eax, %eax
	je	.L261
	movq	16(%rbx), %rcx
	movq	-56(%rbp), %rax
	movq	%r12, %r8
	movq	%r15, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L261
	movq	104(%r13), %rdx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	BN_GF2m_add@PLT
	testl	%eax, %eax
	je	.L261
	movq	24(%rbx), %rdx
	movq	-72(%rbp), %rbx
	movq	%r12, %rcx
	movq	%r13, %rdi
	movq	-64(%rbp), %rax
	movq	%rbx, %rsi
	call	*%rax
	testl	%eax, %eax
	je	.L261
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	BN_GF2m_add@PLT
	testl	%eax, %eax
	je	.L261
	movq	%r15, %rdi
	call	BN_is_zero@PLT
	movl	%eax, %r15d
	jmp	.L260
.L258:
	movl	$-1, %r15d
	jmp	.L254
	.cfi_endproc
.LFE396:
	.size	ec_GF2m_simple_is_on_curve, .-ec_GF2m_simple_is_on_curve
	.p2align 4
	.type	ec_GF2m_simple_add.part.0, @function
ec_GF2m_simple_add.part.0:
.LFB418:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$88, %rsp
	movq	%rsi, -88(%rbp)
	movq	%rcx, -64(%rbp)
	movq	$0, -56(%rbp)
	testq	%r8, %r8
	je	.L382
.L292:
	movq	%r12, %rdi
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -120(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	BN_CTX_get@PLT
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L297
	movl	40(%rbx), %ecx
	testl	%ecx, %ecx
	jne	.L383
	movq	-72(%rbp), %rcx
	movq	%r12, %r8
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	EC_POINT_get_affine_coordinates@PLT
	testl	%eax, %eax
	je	.L297
.L300:
	movq	-64(%rbp), %rax
	movl	40(%rax), %edx
	testl	%edx, %edx
	je	.L384
	movq	16(%rax), %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L297
	movq	24(%rbx), %rsi
	movq	-80(%rbp), %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L297
.L303:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	je	.L385
	movq	-96(%rbp), %rbx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	BN_GF2m_add@PLT
	testl	%eax, %eax
	je	.L297
	movq	-104(%rbp), %r14
	movq	-80(%rbp), %rdx
	movq	-72(%rbp), %rsi
	movq	%r14, %rdi
	call	BN_GF2m_add@PLT
	testl	%eax, %eax
	je	.L297
	movq	(%r15), %rax
	movq	%r12, %r8
	movq	%rbx, %rcx
	movq	%r14, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	*280(%rax)
	testl	%eax, %eax
	je	.L297
	movq	(%r15), %rax
	movq	-112(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	*272(%rax)
	testl	%eax, %eax
	je	.L297
	movq	-112(%rbp), %rsi
	movq	96(%r15), %rdx
	movq	%rsi, %rdi
	call	BN_GF2m_add@PLT
	testl	%eax, %eax
	je	.L297
	movq	%r14, %rdx
	movq	-112(%rbp), %r14
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	BN_GF2m_add@PLT
	movq	%rbx, %rdx
	testl	%eax, %eax
	je	.L297
.L381:
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	BN_GF2m_add@PLT
	testl	%eax, %eax
	je	.L297
	movq	-112(%rbp), %rdx
	movq	-120(%rbp), %rdi
	movq	%r13, %rsi
	call	BN_GF2m_add@PLT
	testl	%eax, %eax
	je	.L297
	movq	-120(%rbp), %rbx
	movq	(%r15), %rax
	movq	%r12, %r8
	movq	%r15, %rdi
	movq	-104(%rbp), %rcx
	movq	%rbx, %rdx
	movq	%rbx, %rsi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L297
	movq	-112(%rbp), %r14
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	call	BN_GF2m_add@PLT
	testl	%eax, %eax
	je	.L297
	movq	-80(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	BN_GF2m_add@PLT
	testl	%eax, %eax
	je	.L297
	movq	-88(%rbp), %rsi
	movq	%r12, %r8
	movq	%rbx, %rcx
	movq	%r14, %rdx
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	EC_POINT_set_affine_coordinates@PLT
	testl	%eax, %eax
	setne	%r13b
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L384:
	movq	-80(%rbp), %rcx
	movq	%rax, %rsi
	movq	%r12, %r8
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	EC_POINT_get_affine_coordinates@PLT
	testl	%eax, %eax
	jne	.L303
	.p2align 4,,10
	.p2align 3
.L297:
	xorl	%r13d, %r13d
.L295:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	movq	-56(%rbp), %rdi
	call	BN_CTX_free@PLT
.L291:
	addq	$88, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L383:
	.cfi_restore_state
	movq	16(%rbx), %rsi
	movq	%r14, %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L297
	movq	24(%rbx), %rsi
	movq	-72(%rbp), %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	jne	.L300
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L382:
	call	BN_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L309
	movq	%rax, -56(%rbp)
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L385:
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	jne	.L306
	movq	%r13, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L306
	movq	-104(%rbp), %rbx
	movq	(%r15), %rax
	movq	%r12, %r8
	movq	%r13, %rcx
	movq	-80(%rbp), %rdx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	call	*280(%rax)
	testl	%eax, %eax
	je	.L297
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	BN_GF2m_add@PLT
	testl	%eax, %eax
	je	.L297
	movq	-112(%rbp), %r14
	movq	(%r15), %rax
	movq	%r12, %rcx
	movq	%rbx, %rdx
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	*272(%rax)
	testl	%eax, %eax
	je	.L297
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	BN_GF2m_add@PLT
	testl	%eax, %eax
	je	.L297
	movq	96(%r15), %rdx
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L306:
	movq	-88(%rbp), %rsi
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	EC_POINT_set_to_infinity@PLT
	testl	%eax, %eax
	setne	%r13b
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L309:
	xorl	%r13d, %r13d
	jmp	.L291
	.cfi_endproc
.LFE418:
	.size	ec_GF2m_simple_add.part.0, .-ec_GF2m_simple_add.part.0
	.p2align 4
	.globl	ec_GF2m_simple_add
	.type	ec_GF2m_simple_add, @function
ec_GF2m_simple_add:
.LFB392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movq	%rdx, %rsi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$8, %rsp
	call	EC_POINT_is_at_infinity@PLT
	movq	%r14, %rsi
	testl	%eax, %eax
	jne	.L391
	movq	%r12, %rdi
	call	EC_POINT_is_at_infinity@PLT
	testl	%eax, %eax
	jne	.L392
	addq	$8, %rsp
	movq	%rbx, %r8
	movq	%r14, %rcx
	movq	%r13, %rdx
	popq	%rbx
	movq	%r15, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ec_GF2m_simple_add.part.0
	.p2align 4,,10
	.p2align 3
.L392:
	.cfi_restore_state
	movq	%r13, %rsi
.L391:
	movq	%r15, %rdi
	call	EC_POINT_copy@PLT
	testl	%eax, %eax
	setne	%al
	addq	$8, %rsp
	popq	%rbx
	movzbl	%al, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE392:
	.size	ec_GF2m_simple_add, .-ec_GF2m_simple_add
	.p2align 4
	.globl	ec_GF2m_simple_invert
	.type	ec_GF2m_simple_invert, @function
ec_GF2m_simple_invert:
.LFB394:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	EC_POINT_is_at_infinity@PLT
	testl	%eax, %eax
	je	.L394
.L396:
	movl	$1, %eax
.L393:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L394:
	.cfi_restore_state
	movq	24(%rbx), %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L396
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	EC_POINT_make_affine@PLT
	testl	%eax, %eax
	je	.L393
	movq	24(%rbx), %rdi
	movq	16(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	movq	%rdi, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	BN_GF2m_add@PLT
	.cfi_endproc
.LFE394:
	.size	ec_GF2m_simple_invert, .-ec_GF2m_simple_invert
	.p2align 4
	.type	ec_GF2m_simple_ladder_post, @function
ec_GF2m_simple_ladder_post:
.LFB405:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	32(%rsi), %rdi
	movq	%rcx, -56(%rbp)
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L469
	movq	32(%rbx), %rdi
	call	BN_is_zero@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L403
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	call	EC_POINT_copy@PLT
	testl	%eax, %eax
	je	.L406
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	$1, %r15d
	call	EC_POINT_invert@PLT
	testl	%eax, %eax
	je	.L406
.L401:
	addq	$40, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L403:
	.cfi_restore_state
	movq	%r13, %rdi
	call	BN_CTX_start@PLT
	movq	%r13, %rdi
	call	BN_CTX_get@PLT
	movq	%r13, %rdi
	movq	%rax, -72(%rbp)
	call	BN_CTX_get@PLT
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	call	BN_CTX_get@PLT
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L470
	movq	(%r14), %rax
	movq	32(%rbx), %rcx
	movq	%r13, %r8
	movq	%r14, %rdi
	movq	32(%r12), %rdx
	movq	-72(%rbp), %rsi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L408
	movq	-56(%rbp), %rax
	movq	32(%r12), %rcx
	movq	%r13, %r8
	movq	%r14, %rdi
	movq	-64(%rbp), %rsi
	movq	16(%rax), %rdx
	movq	(%r14), %rax
	call	*264(%rax)
	testl	%eax, %eax
	je	.L408
	movq	-64(%rbp), %rdx
	movq	16(%r12), %rsi
	movq	%rdx, %rdi
	call	BN_GF2m_add@PLT
	testl	%eax, %eax
	je	.L408
	movq	-56(%rbp), %rax
	movq	32(%rbx), %rcx
	movq	%r13, %r8
	movq	%r14, %rdi
	movq	-80(%rbp), %rsi
	movq	16(%rax), %rdx
	movq	(%r14), %rax
	call	*264(%rax)
	testl	%eax, %eax
	je	.L408
	movq	(%r14), %rax
	movq	16(%r12), %rdx
	movq	%r13, %r8
	movq	%r14, %rdi
	movq	32(%r12), %rsi
	movq	-80(%rbp), %rcx
	call	*264(%rax)
	testl	%eax, %eax
	je	.L408
	movq	16(%rbx), %rdx
	movq	-80(%rbp), %rbx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	BN_GF2m_add@PLT
	testl	%eax, %eax
	je	.L408
	movq	%rbx, -80(%rbp)
	movq	%rbx, %rcx
	movq	(%r14), %rax
	movq	%r13, %r8
	movq	-64(%rbp), %rbx
	movq	%r14, %rdi
	movq	%rbx, %rdx
	movq	%rbx, %rsi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L408
	movq	-56(%rbp), %rax
	movq	-80(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r14, %rdi
	movq	16(%rax), %rdx
	movq	(%r14), %rax
	call	*272(%rax)
	testl	%eax, %eax
	je	.L408
	movq	-56(%rbp), %rax
	movq	-80(%rbp), %rdx
	movq	24(%rax), %rsi
	movq	%rdx, %rdi
	call	BN_GF2m_add@PLT
	testl	%eax, %eax
	je	.L408
	movq	-80(%rbp), %rdx
	movq	(%r14), %rax
	movq	%r13, %r8
	movq	%r14, %rdi
	movq	-72(%rbp), %rcx
	movq	%rdx, %rsi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L408
	movq	-80(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%rbx, %rdi
	call	BN_GF2m_add@PLT
	testl	%eax, %eax
	je	.L408
	movq	-56(%rbp), %rax
	movq	-72(%rbp), %rcx
	movq	%r13, %r8
	movq	%r14, %rdi
	movq	-80(%rbp), %rsi
	movq	16(%rax), %rdx
	movq	(%r14), %rax
	call	*264(%rax)
	testl	%eax, %eax
	je	.L408
	movq	-80(%rbp), %rdx
	movq	(%r14), %rax
	movq	%r13, %rcx
	movq	%r14, %rdi
	movq	%rdx, %rsi
	call	*288(%rax)
	testl	%eax, %eax
	je	.L408
	movq	(%r14), %rax
	movq	-80(%rbp), %rcx
	movq	%r13, %r8
	movq	%rbx, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L408
	movq	(%r14), %rax
	movq	32(%r12), %rdx
	movq	%r13, %r8
	movq	%r14, %rdi
	movq	16(%r12), %rsi
	movq	-80(%rbp), %rcx
	call	*264(%rax)
	testl	%eax, %eax
	je	.L408
	movq	-56(%rbp), %rax
	movq	16(%r12), %rdx
	movq	-80(%rbp), %rdi
	movq	16(%rax), %rsi
	call	BN_GF2m_add@PLT
	testl	%eax, %eax
	je	.L408
	movq	%rbx, %rcx
	movq	(%r14), %rax
	movq	-80(%rbp), %rbx
	movq	%r13, %r8
	movq	%r14, %rdi
	movq	%rbx, %rdx
	movq	%rbx, %rsi
	call	*264(%rax)
	testl	%eax, %eax
	je	.L408
	movq	-56(%rbp), %rax
	movq	24(%r12), %rdi
	movq	%rbx, %rdx
	movq	24(%rax), %rsi
	call	BN_GF2m_add@PLT
	testl	%eax, %eax
	je	.L408
	movq	32(%r12), %rdi
	movl	$1, %esi
	call	BN_set_word@PLT
	testl	%eax, %eax
	je	.L408
	movl	$1, 40(%r12)
	movq	16(%r12), %rdi
	xorl	%esi, %esi
	movl	$1, %r15d
	call	BN_set_negative@PLT
	movq	24(%r12), %rdi
	xorl	%esi, %esi
	call	BN_set_negative@PLT
	.p2align 4,,10
	.p2align 3
.L408:
	movq	%r13, %rdi
	call	BN_CTX_end@PLT
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L406:
	movl	$785, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$16, %edx
	xorl	%r15d, %r15d
	movl	$285, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L469:
	addq	$40, %rsp
	movq	%r12, %rsi
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	EC_POINT_set_to_infinity@PLT
	.p2align 4,,10
	.p2align 3
.L470:
	.cfi_restore_state
	movl	$796, %r8d
	movl	$65, %edx
	movl	$285, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L408
	.cfi_endproc
.LFE405:
	.size	ec_GF2m_simple_ladder_post, .-ec_GF2m_simple_ladder_post
	.p2align 4
	.type	ec_GF2m_simple_ladder_pre, @function
ec_GF2m_simple_ladder_pre:
.LFB403:
	.cfi_startproc
	endbr64
	movl	40(%rcx), %eax
	testl	%eax, %eax
	je	.L518
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L474:
	movq	32(%rbx), %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L519
.L472:
	movq	64(%r12), %rdi
	call	BN_num_bits@PLT
	movq	32(%rbx), %rdi
	xorl	%ecx, %ecx
	movl	$-1, %edx
	leal	-1(%rax), %esi
	call	BN_priv_rand@PLT
	testl	%eax, %eax
	jne	.L474
	movl	%eax, -52(%rbp)
	movl	$701, %r8d
.L517:
	movl	$3, %edx
	movl	$288, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	-52(%rbp), %eax
.L471:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L519:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	32(%rbx), %r9
	movq	296(%rax), %r8
	testq	%r8, %r8
	je	.L475
	movq	%r15, %rcx
	movq	%r9, %rdx
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	*%r8
	testl	%eax, %eax
	je	.L476
	movq	(%r12), %rax
	movq	32(%rbx), %r9
.L475:
	movq	16(%r14), %rdx
	movq	16(%rbx), %rsi
	movq	%r15, %r8
	movq	%r9, %rcx
	movq	%r12, %rdi
	call	*264(%rax)
	testl	%eax, %eax
	jne	.L478
.L476:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L477:
	.cfi_restore_state
	movq	24(%r13), %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L520
.L478:
	movq	64(%r12), %rdi
	call	BN_num_bits@PLT
	movq	24(%r13), %rdi
	xorl	%ecx, %ecx
	movl	$-1, %edx
	leal	-1(%rax), %esi
	call	BN_priv_rand@PLT
	testl	%eax, %eax
	jne	.L477
	movl	%eax, -52(%rbp)
	movl	$716, %r8d
	jmp	.L517
	.p2align 4,,10
	.p2align 3
.L518:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L520:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	(%r12), %rax
	movq	296(%rax), %r8
	testq	%r8, %r8
	je	.L479
	movq	24(%r13), %rsi
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rsi, %rdx
	call	*%r8
	testl	%eax, %eax
	je	.L476
	movq	(%r12), %rax
.L479:
	movq	16(%r14), %rdx
	movq	32(%r13), %rsi
	movq	%r15, %rcx
	movq	%r12, %rdi
	call	*272(%rax)
	testl	%eax, %eax
	je	.L476
	movq	(%r12), %rax
	movq	32(%r13), %rdx
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	16(%r13), %rsi
	call	*272(%rax)
	testl	%eax, %eax
	je	.L476
	movq	16(%r13), %rdi
	movq	104(%r12), %rdx
	movq	%rdi, %rsi
	call	BN_GF2m_add@PLT
	testl	%eax, %eax
	je	.L476
	movq	32(%r13), %rsi
	movq	(%r12), %rax
	movq	%r15, %r8
	movq	%r12, %rdi
	movq	24(%r13), %rcx
	movq	%rsi, %rdx
	call	*264(%rax)
	testl	%eax, %eax
	je	.L476
	movq	16(%r13), %rsi
	movq	(%r12), %rax
	movq	%r15, %r8
	movq	%r12, %rdi
	movq	24(%r13), %rcx
	movq	%rsi, %rdx
	call	*264(%rax)
	testl	%eax, %eax
	je	.L476
	movl	$0, 40(%rbx)
	movl	$1, %eax
	movl	$0, 40(%r13)
	jmp	.L471
	.cfi_endproc
.LFE403:
	.size	ec_GF2m_simple_ladder_pre, .-ec_GF2m_simple_ladder_pre
	.p2align 4
	.type	ec_GF2m_simple_field_inv, @function
ec_GF2m_simple_field_inv:
.LFB407:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	64(%r8), %rdx
	call	BN_GF2m_mod_inv@PLT
	testl	%eax, %eax
	je	.L524
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L524:
	.cfi_restore_state
	movl	$902, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$165, %edx
	movl	%eax, -4(%rbp)
	movl	$296, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	movl	-4(%rbp), %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE407:
	.size	ec_GF2m_simple_field_inv, .-ec_GF2m_simple_field_inv
	.p2align 4
	.type	ec_GF2m_simple_points_mul, @function
ec_GF2m_simple_points_mul:
.LFB406:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %rbx
	cmpq	$1, %rcx
	ja	.L527
	movq	16(%rdi), %rdi
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	BN_is_zero@PLT
	movq	-56(%rbp), %r10
	movq	-64(%rbp), %r9
	testl	%eax, %eax
	je	.L553
.L527:
	movq	%rbx, 16(%rbp)
	addq	$40, %rsp
	movq	%r12, %rcx
	movq	%r14, %rdx
	popq	%rbx
	movq	%r15, %rsi
	popq	%r12
	movq	%r13, %rdi
	movq	%r10, %r8
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ec_wNAF_mul@PLT
	.p2align 4,,10
	.p2align 3
.L553:
	.cfi_restore_state
	movq	24(%r13), %rdi
	call	BN_is_zero@PLT
	movq	-56(%rbp), %r10
	movq	-64(%rbp), %r9
	testl	%eax, %eax
	movl	%eax, %r11d
	jne	.L527
	testq	%r14, %r14
	je	.L550
	testb	$1, %r12b
	je	.L554
	testq	%r14, %r14
	je	.L550
.L529:
	movq	%r13, %rdi
	movq	%r9, -72(%rbp)
	movq	%r10, -64(%rbp)
	movl	%r11d, -56(%rbp)
	call	EC_POINT_new@PLT
	movl	-56(%rbp), %r11d
	movq	-64(%rbp), %r10
	testq	%rax, %rax
	movq	-72(%rbp), %r9
	movq	%rax, %r12
	je	.L555
	xorl	%ecx, %ecx
	movq	%rbx, %r8
	movq	%r14, %rdx
	movq	%rax, %rsi
	movq	%r13, %rdi
	movl	%r11d, -56(%rbp)
	movq	%r9, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	ec_scalar_mul_ladder@PLT
	movl	-56(%rbp), %r11d
	testl	%eax, %eax
	jne	.L556
.L533:
	movq	%r12, %rdi
	movl	%r11d, -56(%rbp)
	call	EC_POINT_free@PLT
	movl	-56(%rbp), %r11d
.L525:
	addq	$40, %rsp
	movl	%r11d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L550:
	.cfi_restore_state
	andl	$1, %r12d
	je	.L529
	movq	(%r10), %rcx
	movq	(%r9), %rdx
	movq	%rbx, %r8
.L552:
	addq	$40, %rsp
	movq	%r15, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ec_scalar_mul_ladder@PLT
	.p2align 4,,10
	.p2align 3
.L556:
	.cfi_restore_state
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r9
	movq	%rbx, %r8
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	(%r10), %rcx
	movq	(%r9), %rdx
	call	ec_scalar_mul_ladder@PLT
	movl	-56(%rbp), %r11d
	testl	%eax, %eax
	je	.L533
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	EC_POINT_add@PLT
	xorl	%r11d, %r11d
	testl	%eax, %eax
	setne	%r11b
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L554:
	movq	%rbx, %r8
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	jmp	.L552
	.p2align 4,,10
	.p2align 3
.L555:
	movl	$875, %r8d
	movl	$65, %edx
	movl	$289, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	-56(%rbp), %r11d
	jmp	.L525
	.cfi_endproc
.LFE406:
	.size	ec_GF2m_simple_points_mul, .-ec_GF2m_simple_points_mul
	.p2align 4
	.globl	ec_GF2m_simple_dbl
	.type	ec_GF2m_simple_dbl, @function
ec_GF2m_simple_dbl:
.LFB393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	movq	%rdx, %rsi
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rcx, %rbx
	call	EC_POINT_is_at_infinity@PLT
	movq	%r12, %rsi
	testl	%eax, %eax
	jne	.L562
	movq	%r13, %rdi
	call	EC_POINT_is_at_infinity@PLT
	testl	%eax, %eax
	jne	.L563
	movq	%rbx, %r8
	movq	%r12, %rcx
	popq	%rbx
	movq	%r12, %rdx
	movq	%r14, %rsi
	popq	%r12
	movq	%r13, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ec_GF2m_simple_add.part.0
	.p2align 4,,10
	.p2align 3
.L563:
	.cfi_restore_state
	movq	%r12, %rsi
.L562:
	movq	%r14, %rdi
	call	EC_POINT_copy@PLT
	popq	%rbx
	popq	%r12
	testl	%eax, %eax
	popq	%r13
	popq	%r14
	setne	%al
	popq	%rbp
	.cfi_def_cfa 7, 8
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE393:
	.size	ec_GF2m_simple_dbl, .-ec_GF2m_simple_dbl
	.p2align 4
	.globl	EC_GF2m_simple_method
	.type	EC_GF2m_simple_method, @function
EC_GF2m_simple_method:
.LFB408:
	.cfi_startproc
	endbr64
	leaq	ret.10005(%rip), %rax
	ret
	.cfi_endproc
.LFE408:
	.size	EC_GF2m_simple_method, .-EC_GF2m_simple_method
	.section	.data.rel.ro,"aw"
	.align 32
	.type	ret.10005, @object
	.size	ret.10005, 432
ret.10005:
	.long	1
	.long	407
	.quad	ec_GF2m_simple_group_init
	.quad	ec_GF2m_simple_group_finish
	.quad	ec_GF2m_simple_group_clear_finish
	.quad	ec_GF2m_simple_group_copy
	.quad	ec_GF2m_simple_group_set_curve
	.quad	ec_GF2m_simple_group_get_curve
	.quad	ec_GF2m_simple_group_get_degree
	.quad	ec_group_simple_order_bits
	.quad	ec_GF2m_simple_group_check_discriminant
	.quad	ec_GF2m_simple_point_init
	.quad	ec_GF2m_simple_point_finish
	.quad	ec_GF2m_simple_point_clear_finish
	.quad	ec_GF2m_simple_point_copy
	.quad	ec_GF2m_simple_point_set_to_infinity
	.quad	0
	.quad	0
	.quad	ec_GF2m_simple_point_set_affine_coordinates
	.quad	ec_GF2m_simple_point_get_affine_coordinates
	.quad	0
	.quad	0
	.quad	0
	.quad	ec_GF2m_simple_add
	.quad	ec_GF2m_simple_dbl
	.quad	ec_GF2m_simple_invert
	.quad	ec_GF2m_simple_is_at_infinity
	.quad	ec_GF2m_simple_is_on_curve
	.quad	ec_GF2m_simple_cmp
	.quad	ec_GF2m_simple_make_affine
	.quad	ec_GF2m_simple_points_make_affine
	.quad	ec_GF2m_simple_points_mul
	.quad	0
	.quad	0
	.quad	ec_GF2m_simple_field_mul
	.quad	ec_GF2m_simple_field_sqr
	.quad	ec_GF2m_simple_field_div
	.quad	ec_GF2m_simple_field_inv
	.quad	0
	.quad	0
	.quad	0
	.quad	ec_key_simple_priv2oct
	.quad	ec_key_simple_oct2priv
	.quad	0
	.quad	ec_key_simple_generate_key
	.quad	ec_key_simple_check_key
	.quad	ec_key_simple_generate_public_key
	.quad	0
	.quad	0
	.quad	ecdh_simple_compute_key
	.quad	0
	.quad	0
	.quad	ec_GF2m_simple_ladder_pre
	.quad	ec_GF2m_simple_ladder_step
	.quad	ec_GF2m_simple_ladder_post
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
