	.file	"p12_crpt.c"
	.text
	.p2align 4
	.globl	PKCS12_PBE_add
	.type	PKCS12_PBE_add, @function
PKCS12_PBE_add:
.LFB803:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE803:
	.size	PKCS12_PBE_add, .-PKCS12_PBE_add
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/pkcs12/p12_crpt.c"
	.text
	.p2align 4
	.globl	PKCS12_PBE_keyivgen
	.type	PKCS12_PBE_keyivgen, @function
PKCS12_PBE_keyivgen:
.LFB804:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -168(%rbp)
	movq	%rsi, -152(%rbp)
	movl	%edx, -156(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L3
	movq	%rcx, %rsi
	leaq	PBEPARAM_it(%rip), %rdi
	movq	%r8, %r12
	movq	%r9, %r15
	call	ASN1_TYPE_unpack_sequence@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L16
	movq	8(%rax), %rdi
	movl	$1, %ebx
	testq	%rdi, %rdi
	je	.L6
	call	ASN1_INTEGER_get@PLT
	movl	%eax, %ebx
.L6:
	movq	(%r14), %rax
	movq	%r12, %rdi
	movq	8(%rax), %r13
	movl	(%rax), %eax
	movq	%r13, -176(%rbp)
	movl	%eax, -160(%rbp)
	call	EVP_CIPHER_key_length@PLT
	subq	$8, %rsp
	leaq	-128(%rbp), %rdx
	movl	%ebx, %r9d
	pushq	%r15
	movl	-160(%rbp), %ecx
	movl	$1, %r8d
	movl	-156(%rbp), %esi
	movq	-152(%rbp), %rdi
	pushq	%rdx
	pushq	%rax
	movq	%rdx, -184(%rbp)
	movq	%r13, %rdx
	call	PKCS12_key_gen_utf8@PLT
	addq	$32, %rsp
	movl	$55, %r8d
	leaq	.LC0(%rip), %rcx
	movl	%eax, %r13d
	movl	$107, %edx
	testl	%eax, %eax
	je	.L15
	movq	%r12, %rdi
	call	EVP_CIPHER_iv_length@PLT
	subq	$8, %rsp
	movl	%ebx, %r9d
	movl	-160(%rbp), %ecx
	pushq	%r15
	leaq	-144(%rbp), %r15
	movq	-176(%rbp), %rdx
	movl	$2, %r8d
	movl	-156(%rbp), %esi
	movq	-152(%rbp), %rdi
	pushq	%r15
	pushq	%rax
	call	PKCS12_key_gen_utf8@PLT
	addq	$32, %rsp
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L17
	movq	%r14, %rdi
	call	PBEPARAM_free@PLT
	movl	16(%rbp), %r9d
	movq	%r15, %r8
	xorl	%edx, %edx
	movq	-184(%rbp), %rbx
	movq	-168(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rbx, %rcx
	call	EVP_CipherInit_ex@PLT
	movl	$64, %esi
	movq	%rbx, %rdi
	movl	%eax, %r13d
	call	OPENSSL_cleanse@PLT
	movl	$16, %esi
	movq	%r15, %rdi
	call	OPENSSL_cleanse@PLT
.L3:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L18
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movl	$61, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$106, %edx
.L15:
	movl	$120, %esi
	movl	$35, %edi
	call	ERR_put_error@PLT
	movq	%r14, %rdi
	call	PBEPARAM_free@PLT
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L16:
	movl	$43, %r8d
	movl	$101, %edx
	movl	$120, %esi
	movl	$35, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L3
.L18:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE804:
	.size	PKCS12_PBE_keyivgen, .-PKCS12_PBE_keyivgen
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
