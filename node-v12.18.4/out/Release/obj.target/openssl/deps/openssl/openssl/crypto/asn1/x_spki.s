	.file	"x_spki.c"
	.text
	.p2align 4
	.globl	d2i_NETSCAPE_SPKAC
	.type	d2i_NETSCAPE_SPKAC, @function
d2i_NETSCAPE_SPKAC:
.LFB803:
	.cfi_startproc
	endbr64
	leaq	NETSCAPE_SPKAC_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE803:
	.size	d2i_NETSCAPE_SPKAC, .-d2i_NETSCAPE_SPKAC
	.p2align 4
	.globl	i2d_NETSCAPE_SPKAC
	.type	i2d_NETSCAPE_SPKAC, @function
i2d_NETSCAPE_SPKAC:
.LFB804:
	.cfi_startproc
	endbr64
	leaq	NETSCAPE_SPKAC_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE804:
	.size	i2d_NETSCAPE_SPKAC, .-i2d_NETSCAPE_SPKAC
	.p2align 4
	.globl	NETSCAPE_SPKAC_new
	.type	NETSCAPE_SPKAC_new, @function
NETSCAPE_SPKAC_new:
.LFB805:
	.cfi_startproc
	endbr64
	leaq	NETSCAPE_SPKAC_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE805:
	.size	NETSCAPE_SPKAC_new, .-NETSCAPE_SPKAC_new
	.p2align 4
	.globl	NETSCAPE_SPKAC_free
	.type	NETSCAPE_SPKAC_free, @function
NETSCAPE_SPKAC_free:
.LFB806:
	.cfi_startproc
	endbr64
	leaq	NETSCAPE_SPKAC_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE806:
	.size	NETSCAPE_SPKAC_free, .-NETSCAPE_SPKAC_free
	.p2align 4
	.globl	d2i_NETSCAPE_SPKI
	.type	d2i_NETSCAPE_SPKI, @function
d2i_NETSCAPE_SPKI:
.LFB807:
	.cfi_startproc
	endbr64
	leaq	NETSCAPE_SPKI_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE807:
	.size	d2i_NETSCAPE_SPKI, .-d2i_NETSCAPE_SPKI
	.p2align 4
	.globl	i2d_NETSCAPE_SPKI
	.type	i2d_NETSCAPE_SPKI, @function
i2d_NETSCAPE_SPKI:
.LFB808:
	.cfi_startproc
	endbr64
	leaq	NETSCAPE_SPKI_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE808:
	.size	i2d_NETSCAPE_SPKI, .-i2d_NETSCAPE_SPKI
	.p2align 4
	.globl	NETSCAPE_SPKI_new
	.type	NETSCAPE_SPKI_new, @function
NETSCAPE_SPKI_new:
.LFB809:
	.cfi_startproc
	endbr64
	leaq	NETSCAPE_SPKI_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE809:
	.size	NETSCAPE_SPKI_new, .-NETSCAPE_SPKI_new
	.p2align 4
	.globl	NETSCAPE_SPKI_free
	.type	NETSCAPE_SPKI_free, @function
NETSCAPE_SPKI_free:
.LFB810:
	.cfi_startproc
	endbr64
	leaq	NETSCAPE_SPKI_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE810:
	.size	NETSCAPE_SPKI_free, .-NETSCAPE_SPKI_free
	.globl	NETSCAPE_SPKI_it
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"NETSCAPE_SPKI"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	NETSCAPE_SPKI_it, @object
	.size	NETSCAPE_SPKI_it, 56
NETSCAPE_SPKI_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	NETSCAPE_SPKI_seq_tt
	.quad	3
	.quad	0
	.quad	32
	.quad	.LC0
	.section	.rodata.str1.1
.LC1:
	.string	"spkac"
.LC2:
	.string	"sig_algor"
.LC3:
	.string	"signature"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	NETSCAPE_SPKI_seq_tt, @object
	.size	NETSCAPE_SPKI_seq_tt, 120
NETSCAPE_SPKI_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC1
	.quad	NETSCAPE_SPKAC_it
	.quad	4096
	.quad	0
	.quad	8
	.quad	.LC2
	.quad	X509_ALGOR_it
	.quad	0
	.quad	0
	.quad	24
	.quad	.LC3
	.quad	ASN1_BIT_STRING_it
	.globl	NETSCAPE_SPKAC_it
	.section	.rodata.str1.1
.LC4:
	.string	"NETSCAPE_SPKAC"
	.section	.data.rel.ro.local
	.align 32
	.type	NETSCAPE_SPKAC_it, @object
	.size	NETSCAPE_SPKAC_it, 56
NETSCAPE_SPKAC_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	NETSCAPE_SPKAC_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC4
	.section	.rodata.str1.1
.LC5:
	.string	"pubkey"
.LC6:
	.string	"challenge"
	.section	.data.rel.ro
	.align 32
	.type	NETSCAPE_SPKAC_seq_tt, @object
	.size	NETSCAPE_SPKAC_seq_tt, 80
NETSCAPE_SPKAC_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC5
	.quad	X509_PUBKEY_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC6
	.quad	ASN1_IA5STRING_it
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
