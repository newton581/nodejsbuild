	.file	"poly1305.c"
	.text
	.p2align 4
	.globl	Poly1305_ctx_size
	.type	Poly1305_ctx_size, @function
Poly1305_ctx_size:
.LFB151:
	.cfi_startproc
	endbr64
	movl	$248, %eax
	ret
	.cfi_endproc
.LFE151:
	.size	Poly1305_ctx_size, .-Poly1305_ctx_size
	.p2align 4
	.globl	Poly1305_Init
	.type	Poly1305_Init, @function
Poly1305_Init:
.LFB153:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	232(%rdi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	16(%rsi), %eax
	movl	%eax, 192(%rdi)
	movl	20(%rsi), %eax
	movl	%eax, 196(%rdi)
	movl	24(%rsi), %eax
	movl	%eax, 200(%rdi)
	movl	28(%rsi), %eax
	movl	%eax, 204(%rdi)
	call	poly1305_init@PLT
	testl	%eax, %eax
	jne	.L4
	movq	poly1305_blocks@GOTPCREL(%rip), %xmm0
	movhps	poly1305_emit@GOTPCREL(%rip), %xmm0
	movups	%xmm0, 232(%rbx)
.L4:
	movq	$0, 224(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE153:
	.size	Poly1305_Init, .-Poly1305_Init
	.p2align 4
	.globl	Poly1305_Update
	.type	Poly1305_Update, @function
Poly1305_Update:
.LFB154:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	224(%rdi), %r14
	movq	232(%rdi), %r8
	testq	%r14, %r14
	je	.L7
	movl	$16, %r15d
	leaq	208(%rdi), %r9
	movq	%r8, -64(%rbp)
	subq	%r14, %r15
	movq	%r9, -56(%rbp)
	leaq	(%r9,%r14), %rdi
	cmpq	%r15, %rdx
	jb	.L8
	movq	%r15, %rdx
	addq	%r15, %r13
	leaq	-16(%r14,%rbx), %rbx
	call	memcpy@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %r8
	movq	%r12, %rdi
	movl	$1, %ecx
	movl	$16, %edx
	movq	%r8, -56(%rbp)
	movq	%r9, %rsi
	call	*%r8
	movq	-56(%rbp), %r8
.L7:
	movq	%rbx, %r14
	andq	$-16, %rbx
	andl	$15, %r14d
	cmpq	$15, %rbx
	ja	.L29
	testq	%r14, %r14
	jne	.L30
.L11:
	movq	%r14, 224(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movq	%r13, %rsi
	movl	$1, %ecx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	*%r8
	addq	%rbx, %r13
	testq	%r14, %r14
	je	.L11
.L30:
	leaq	208(%r12), %rax
	cmpl	$8, %r14d
	jb	.L31
	movq	0(%r13), %rdx
	leaq	216(%r12), %rsi
	andq	$-8, %rsi
	movq	%rdx, 208(%r12)
	movq	-8(%r13,%r14), %rdx
	movq	%rdx, -8(%rax,%r14)
	subq	%rsi, %rax
	subq	%rax, %r13
	addl	%r14d, %eax
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L11
	andl	$-8, %eax
	xorl	%edx, %edx
.L16:
	movl	%edx, %ecx
	addl	$8, %edx
	movq	0(%r13,%rcx), %rdi
	movq	%rdi, (%rsi,%rcx)
	cmpl	%eax, %edx
	jb	.L16
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L8:
	call	memcpy@PLT
	addq	%r14, %rbx
	movq	%rbx, 224(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	testb	$4, %r14b
	jne	.L32
	testl	%r14d, %r14d
	je	.L11
	movzbl	0(%r13), %edx
	movb	%dl, 208(%r12)
	testb	$2, %r14b
	je	.L11
	movzwl	-2(%r13,%r14), %edx
	movw	%dx, -2(%rax,%r14)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L32:
	movl	0(%r13), %edx
	movl	%edx, 208(%r12)
	movl	-4(%r13,%r14), %edx
	movl	%edx, -4(%rax,%r14)
	jmp	.L11
	.cfi_endproc
.LFE154:
	.size	Poly1305_Update, .-Poly1305_Update
	.p2align 4
	.globl	Poly1305_Final
	.type	Poly1305_Final, @function
Poly1305_Final:
.LFB155:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	224(%rdi), %rdx
	movq	240(%rdi), %rbx
	testq	%rdx, %rdx
	je	.L34
	leaq	1(%rdx), %rax
	movq	232(%rdi), %r8
	movb	$1, 208(%rdi,%rdx)
	cmpq	$15, %rax
	ja	.L42
	movl	$15, %eax
	leaq	209(%rdi,%rdx), %rcx
	xorl	%edi, %edi
	subq	%rdx, %rax
	cmpl	$8, %eax
	jnb	.L36
	testb	$4, %al
	jne	.L51
	testl	%eax, %eax
	jne	.L52
.L42:
	leaq	208(%r12), %rsi
	xorl	%ecx, %ecx
	movl	$16, %edx
	movq	%r12, %rdi
	call	*%r8
.L34:
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	192(%r12), %rdx
	call	*%rbx
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$248, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	OPENSSL_cleanse@PLT
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	leaq	8(%rcx), %rsi
	movl	%eax, %edx
	movq	$0, (%rcx)
	movq	$0, -8(%rcx,%rdx)
	andq	$-8, %rsi
	subq	%rsi, %rcx
	addl	%ecx, %eax
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L42
	andl	$-8, %eax
	xorl	%edx, %edx
.L40:
	movl	%edx, %ecx
	addl	$8, %edx
	movq	%rdi, (%rsi,%rcx)
	cmpl	%eax, %edx
	jb	.L40
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L52:
	movb	$0, (%rcx)
	testb	$2, %al
	je	.L42
	movl	%eax, %edx
	xorl	%eax, %eax
	movw	%ax, -2(%rcx,%rdx)
	jmp	.L42
.L51:
	movl	%eax, %edx
	movl	$0, (%rcx)
	movl	$0, -4(%rcx,%rdx)
	jmp	.L42
	.cfi_endproc
.LFE155:
	.size	Poly1305_Final, .-Poly1305_Final
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
