	.file	"bss_fd.c"
	.text
	.p2align 4
	.type	fd_new, @function
fd_new:
.LFB269:
	.cfi_startproc
	endbr64
	movl	$0, 32(%rdi)
	movl	$1, %eax
	movl	$-1, 48(%rdi)
	movq	$0, 56(%rdi)
	movl	$0, 40(%rdi)
	ret
	.cfi_endproc
.LFE269:
	.size	fd_new, .-fd_new
	.p2align 4
	.type	fd_ctrl, @function
fd_ctrl:
.LFB273:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	cmpl	$12, %esi
	jg	.L4
	testl	%esi, %esi
	jle	.L20
	cmpl	$12, %esi
	ja	.L20
	leaq	.L7(%rip), %rdx
	movl	%esi, %esi
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L7:
	.long	.L20-.L7
	.long	.L21-.L7
	.long	.L11-.L7
	.long	.L10-.L7
	.long	.L20-.L7
	.long	.L20-.L7
	.long	.L20-.L7
	.long	.L20-.L7
	.long	.L9-.L7
	.long	.L8-.L7
	.long	.L20-.L7
	.long	.L36-.L7
	.long	.L36-.L7
	.text
.L39:
	cmpl	$104, %esi
	jne	.L20
	testq	%rdi, %rdi
	je	.L16
	movl	36(%rdi), %esi
	testl	%esi, %esi
	je	.L16
	movl	32(%rdi), %edx
	testl	%edx, %edx
	jne	.L38
.L17:
	movl	$0, 32(%rbx)
	movl	$0, 40(%rbx)
.L16:
	movl	(%rcx), %eax
	movl	%r12d, 36(%rbx)
	movl	$1, 32(%rbx)
	movl	%eax, 48(%rbx)
.L36:
	movl	$1, %eax
.L3:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L21:
	.cfi_restore_state
	xorl	%r12d, %r12d
.L12:
	movl	48(%rbx), %edi
	addq	$16, %rsp
	movq	%r12, %rsi
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	lseek@PLT
.L20:
	.cfi_restore_state
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	cmpl	$105, %esi
	je	.L13
	jle	.L39
	cmpl	$128, %esi
	je	.L12
	cmpl	$133, %esi
	jne	.L20
.L10:
	movl	48(%rbx), %edi
	addq	$16, %rsp
	movl	$1, %edx
	xorl	%esi, %esi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	lseek@PLT
.L11:
	.cfi_restore_state
	movl	40(%rdi), %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	sarl	$11, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	andl	$1, %eax
	ret
.L9:
	.cfi_restore_state
	movslq	36(%rdi), %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L8:
	.cfi_restore_state
	movl	%r12d, 36(%rdi)
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	movl	32(%rdi), %eax
	testl	%eax, %eax
	je	.L22
	movslq	48(%rdi), %rax
	testq	%rcx, %rcx
	je	.L3
	movl	%eax, (%rcx)
	movslq	48(%rdi), %rax
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L38:
	movl	48(%rdi), %edi
	movq	%rcx, -24(%rbp)
	call	close@PLT
	movq	-24(%rbp), %rcx
	jmp	.L17
.L22:
	movq	$-1, %rax
	jmp	.L3
	.cfi_endproc
.LFE273:
	.size	fd_ctrl, .-fd_ctrl
	.p2align 4
	.type	fd_read, @function
fd_read:
.LFB271:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rsi, %rsi
	je	.L40
	movq	%rdi, %r13
	movl	%edx, %ebx
	movq	%rsi, %r12
	call	__errno_location@PLT
	movslq	%ebx, %rdx
	movq	%r12, %rsi
	movl	$0, (%rax)
	movl	48(%r13), %edi
	movq	%rax, %r14
	call	read@PLT
	movl	$15, %esi
	movq	%r13, %rdi
	movq	%rax, %rbx
	movl	%eax, %r15d
	call	BIO_clear_flags@PLT
	testl	%ebx, %ebx
	jle	.L57
.L40:
	addq	$8, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	leal	1(%rbx), %eax
	cmpl	$1, %eax
	jbe	.L58
.L42:
	testl	%ebx, %ebx
	jne	.L40
	orl	$2048, 40(%r13)
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L58:
	movl	(%r14), %eax
	cmpl	$115, %eax
	jg	.L42
	cmpl	$70, %eax
	jg	.L43
	cmpl	$4, %eax
	je	.L44
	cmpl	$11, %eax
	jne	.L42
.L44:
	movl	$9, %esi
	movq	%r13, %rdi
	call	BIO_set_flags@PLT
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L43:
	movabsq	$26456998543361, %rdx
	subl	$71, %eax
	btq	%rax, %rdx
	jnc	.L42
	movl	$9, %esi
	movq	%r13, %rdi
	call	BIO_set_flags@PLT
	jmp	.L40
	.cfi_endproc
.LFE271:
	.size	fd_read, .-fd_read
	.p2align 4
	.type	fd_free, @function
fd_free:
.LFB270:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L62
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	36(%rdi), %edx
	testl	%edx, %edx
	je	.L59
	movl	32(%rdi), %eax
	testl	%eax, %eax
	jne	.L68
.L61:
	movl	$0, 32(%rbx)
	movl	$1, %eax
	movl	$0, 40(%rbx)
.L59:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	48(%rdi), %edi
	call	close@PLT
	jmp	.L61
	.cfi_endproc
.LFE270:
	.size	fd_free, .-fd_free
	.section	.text.unlikely,"ax",@progbits
.LCOLDB0:
	.text
.LHOTB0:
	.p2align 4
	.type	fd_gets, @function
fd_gets:
.LFB275:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-1(%rsi,%rdx), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%rsi, -64(%rbp)
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L72:
	addq	$1, %rbx
	cmpb	$10, -1(%rbx)
	je	.L70
.L77:
	cmpq	%r13, %rbx
	jnb	.L70
	testq	%rbx, %rbx
	je	.L71
	call	__errno_location@PLT
	movq	%rbx, %rsi
	movl	$1, %edx
	movl	$0, (%rax)
	movl	48(%r12), %edi
	movq	%rax, %r15
	call	read@PLT
	movl	$15, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	movl	%eax, -52(%rbp)
	call	BIO_clear_flags@PLT
	testl	%r14d, %r14d
	jg	.L72
	leal	1(%r14), %eax
	cmpl	$1, %eax
	jbe	.L89
.L73:
	movl	-52(%rbp), %eax
	testl	%eax, %eax
	jne	.L70
	orl	$2048, 40(%r12)
.L70:
	movq	-64(%rbp), %rdi
	movb	$0, (%rbx)
	xorl	%eax, %eax
	cmpb	$0, (%rdi)
	je	.L69
	call	strlen@PLT
.L69:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	movl	(%r15), %eax
	cmpl	$115, %eax
	jg	.L73
	cmpl	$70, %eax
	jg	.L74
	cmpl	$4, %eax
	je	.L75
	cmpl	$11, %eax
	jne	.L73
.L75:
	movl	$9, %esi
	movq	%r12, %rdi
	call	BIO_set_flags@PLT
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L74:
	movabsq	$26456998543361, %rdx
	subl	$71, %eax
	btq	%rax, %rdx
	jnc	.L73
	movl	$9, %esi
	movq	%r12, %rdi
	call	BIO_set_flags@PLT
	jmp	.L70
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	fd_gets.cold, @function
fd_gets.cold:
.LFSB275:
.L71:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movb	$0, 0
	ud2
	.cfi_endproc
.LFE275:
	.text
	.size	fd_gets, .-fd_gets
	.section	.text.unlikely
	.size	fd_gets.cold, .-fd_gets.cold
.LCOLDE0:
	.text
.LHOTE0:
	.p2align 4
	.type	fd_puts, @function
fd_puts:
.LFB274:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	call	strlen@PLT
	movq	%rax, %rbx
	call	__errno_location@PLT
	movslq	%ebx, %rdx
	movq	%r13, %rsi
	movl	$0, (%rax)
	movl	48(%r12), %edi
	movq	%rax, %r14
	call	write@PLT
	movl	$15, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	BIO_clear_flags@PLT
	testl	%ebx, %ebx
	jle	.L105
.L90:
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore_state
	leal	1(%rbx), %eax
	cmpl	$1, %eax
	ja	.L90
	movl	(%r14), %eax
	cmpl	$115, %eax
	jg	.L90
	cmpl	$70, %eax
	jg	.L92
	cmpl	$4, %eax
	je	.L93
	cmpl	$11, %eax
	jne	.L90
.L93:
	movl	$10, %esi
	movq	%r12, %rdi
	call	BIO_set_flags@PLT
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L92:
	movabsq	$26456998543361, %rdx
	subl	$71, %eax
	btq	%rax, %rdx
	jc	.L93
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE274:
	.size	fd_puts, .-fd_puts
	.p2align 4
	.type	fd_write, @function
fd_write:
.LFB272:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	call	__errno_location@PLT
	movslq	%ebx, %rdx
	movq	%r14, %rsi
	movl	$0, (%rax)
	movl	48(%r12), %edi
	movq	%rax, %r13
	call	write@PLT
	movl	$15, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	BIO_clear_flags@PLT
	testl	%ebx, %ebx
	jle	.L121
.L106:
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	leal	1(%rbx), %eax
	cmpl	$1, %eax
	ja	.L106
	movl	0(%r13), %eax
	cmpl	$115, %eax
	jg	.L106
	cmpl	$70, %eax
	jg	.L108
	cmpl	$4, %eax
	je	.L109
	cmpl	$11, %eax
	jne	.L106
.L109:
	movl	$10, %esi
	movq	%r12, %rdi
	call	BIO_set_flags@PLT
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L108:
	movabsq	$26456998543361, %rdx
	subl	$71, %eax
	btq	%rax, %rdx
	jc	.L109
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE272:
	.size	fd_write, .-fd_write
	.p2align 4
	.globl	BIO_s_fd
	.type	BIO_s_fd, @function
BIO_s_fd:
.LFB267:
	.cfi_startproc
	endbr64
	leaq	methods_fdp(%rip), %rax
	ret
	.cfi_endproc
.LFE267:
	.size	BIO_s_fd, .-BIO_s_fd
	.p2align 4
	.globl	BIO_new_fd
	.type	BIO_new_fd, @function
BIO_new_fd:
.LFB268:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edi, %r13d
	leaq	methods_fdp(%rip), %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$8, %rsp
	call	BIO_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L123
	movslq	%ebx, %rdx
	movl	%r13d, %ecx
	movl	$104, %esi
	movq	%rax, %rdi
	call	BIO_int_ctrl@PLT
.L123:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE268:
	.size	BIO_new_fd, .-BIO_new_fd
	.p2align 4
	.globl	BIO_fd_should_retry
	.type	BIO_fd_should_retry, @function
BIO_fd_should_retry:
.LFB276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addl	$1, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	xorl	%r12d, %r12d
	subq	$8, %rsp
	cmpl	$1, %edi
	jbe	.L137
.L129:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
	call	__errno_location@PLT
	movl	(%rax), %ecx
	cmpl	$115, %ecx
	jg	.L129
	cmpl	$70, %ecx
	jg	.L131
	movl	$1, %r12d
	cmpl	$4, %ecx
	je	.L129
	xorl	%r12d, %r12d
	cmpl	$11, %ecx
	sete	%r12b
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L131:
	.cfi_restore_state
	subl	$71, %ecx
	movl	$1, %eax
	xorl	%r12d, %r12d
	movabsq	$26456998543361, %rdx
	salq	%cl, %rax
	testq	%rdx, %rax
	setne	%r12b
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE276:
	.size	BIO_fd_should_retry, .-BIO_fd_should_retry
	.p2align 4
	.globl	BIO_fd_non_fatal_error
	.type	BIO_fd_non_fatal_error, @function
BIO_fd_non_fatal_error:
.LFB277:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$115, %edi
	jg	.L138
	cmpl	$70, %edi
	jg	.L140
	movl	$1, %eax
	cmpl	$4, %edi
	je	.L138
	xorl	%eax, %eax
	cmpl	$11, %edi
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	leal	-71(%rdi), %ecx
	movl	$1, %eax
	movabsq	$26456998543361, %rdx
	salq	%cl, %rax
	testq	%rdx, %rax
	setne	%al
	movzbl	%al, %eax
.L138:
	ret
	.cfi_endproc
.LFE277:
	.size	BIO_fd_non_fatal_error, .-BIO_fd_non_fatal_error
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"file descriptor"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	methods_fdp, @object
	.size	methods_fdp, 96
methods_fdp:
	.long	1284
	.zero	4
	.quad	.LC1
	.quad	bwrite_conv
	.quad	fd_write
	.quad	bread_conv
	.quad	fd_read
	.quad	fd_puts
	.quad	fd_gets
	.quad	fd_ctrl
	.quad	fd_new
	.quad	fd_free
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
