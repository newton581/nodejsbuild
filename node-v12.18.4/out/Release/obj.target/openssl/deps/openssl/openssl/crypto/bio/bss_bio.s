	.file	"bss_bio.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/bio/bss_bio.c"
	.text
	.p2align 4
	.type	bio_free, @function
bio_free:
.LFB269:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L4
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	56(%rdi), %r12
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L3
	movq	56(%rax), %rdx
	pxor	%xmm0, %xmm0
	movq	$0, (%rdx)
	movl	$0, 32(%rax)
	movups	%xmm0, 16(%rdx)
	movq	$0, (%r12)
	movl	$0, 32(%rdi)
	movups	%xmm0, 16(%r12)
.L3:
	movq	40(%r12), %rdi
	movl	$105, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	movl	$106, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE269:
	.size	bio_free, .-bio_free
	.p2align 4
	.type	bio_new, @function
bio_new:
.LFB268:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$80, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$56, %edi
	subq	$8, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L15
	movq	$17408, 32(%rax)
	movq	%rax, 56(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE268:
	.size	bio_new, .-bio_new
	.p2align 4
	.type	bio_read, @function
bio_read:
.LFB270:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$15, %esi
	pushq	%r14
	.cfi_offset 14, -32
	movslq	%edx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	call	BIO_clear_flags@PLT
	movl	32(%r13), %edx
	testl	%edx, %edx
	je	.L20
	movq	56(%r13), %rax
	movq	(%rax), %rax
	movq	56(%rax), %rbx
	movq	$0, 48(%rbx)
	testq	%r15, %r15
	je	.L20
	testq	%r14, %r14
	je	.L20
	movq	16(%rbx), %r12
	testq	%r12, %r12
	jne	.L21
	movl	8(%rbx), %eax
	testl	%eax, %eax
	je	.L31
.L20:
	xorl	%eax, %eax
.L17:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	cmpq	%r14, %r12
	cmova	%r14, %r12
	movq	%r12, %r13
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L33:
	movq	%rsi, %rcx
	movq	%rax, %r14
	subq	%rax, %rcx
	subq	%rsi, %r14
	addq	%rcx, %r13
.L23:
	addq	40(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	memcpy@PLT
	subq	%r14, 16(%rbx)
	je	.L24
	movq	24(%rbx), %rax
	addq	%r14, %rax
	movq	%rax, 24(%rbx)
	cmpq	32(%rbx), %rax
	jne	.L25
	movq	$0, 24(%rbx)
.L25:
	addq	%r14, %r15
	testq	%r13, %r13
	je	.L32
.L27:
	movq	24(%rbx), %rsi
	movq	32(%rbx), %rax
	leaq	(%rsi,%r13), %rdx
	cmpq	%rax, %rdx
	ja	.L33
	movq	%r13, %r14
	xorl	%r13d, %r13d
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L24:
	movq	$0, 24(%rbx)
	testq	%r13, %r13
	jne	.L27
.L32:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L31:
	.cfi_restore_state
	movl	$9, %esi
	movq	%r13, %rdi
	call	BIO_set_flags@PLT
	movq	32(%rbx), %rax
	cmpq	%r14, %rax
	jb	.L22
	movq	%r14, 48(%rbx)
	movl	$-1, %eax
	jmp	.L17
.L22:
	movq	%rax, 48(%rbx)
	movl	$-1, %eax
	jmp	.L17
	.cfi_endproc
.LFE270:
	.size	bio_read, .-bio_read
	.p2align 4
	.type	bio_write, @function
bio_write:
.LFB273:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	$15, %esi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	call	BIO_clear_flags@PLT
	movl	32(%r15), %eax
	testl	%eax, %eax
	je	.L34
	movslq	%ebx, %rdx
	testq	%r14, %r14
	je	.L42
	testq	%rdx, %rdx
	je	.L42
	movq	56(%r15), %r13
	movl	8(%r13), %eax
	movq	$0, 48(%r13)
	testl	%eax, %eax
	jne	.L50
	movq	16(%r13), %rcx
	movq	32(%r13), %r12
	cmpq	%r12, %rcx
	je	.L51
	movq	%r12, %rax
	subq	%rcx, %rax
	cmpq	%rdx, %rax
	cmovbe	%rax, %rdx
	movq	%rdx, -64(%rbp)
	movq	%rdx, %r15
	.p2align 4,,10
	.p2align 3
.L41:
	addq	24(%r13), %rcx
	movq	40(%r13), %rdi
	movq	%rcx, %rax
	movq	%rcx, %rbx
	subq	%r12, %rax
	cmpq	%rcx, %r12
	cmovbe	%rax, %rbx
	leaq	(%r15,%rbx), %rax
	addq	%rbx, %rdi
	cmpq	%r12, %rax
	ja	.L52
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	addq	%r15, 16(%r13)
.L40:
	movl	-64(%rbp), %eax
.L34:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r14, %rsi
	subq	%rbx, %rdx
	subq	%r12, %rbx
	movq	%rdx, -56(%rbp)
	call	memcpy@PLT
	movq	-56(%rbp), %rdx
	movq	16(%r13), %rcx
	addq	%rdx, %rcx
	addq	%rdx, %r14
	movq	%rcx, 16(%r13)
	addq	%rbx, %r15
	je	.L40
	movq	32(%r13), %r12
	jmp	.L41
.L50:
	movl	$289, %r8d
	movl	$124, %edx
	movl	$113, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L34
.L51:
	movl	$10, %esi
	movq	%r15, %rdi
	call	BIO_set_flags@PLT
	movl	$-1, %eax
	jmp	.L34
	.cfi_endproc
.LFE273:
	.size	bio_write, .-bio_write
	.p2align 4
	.type	bio_puts, @function
bio_puts:
.LFB277:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	call	strlen@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	bio_write
	.cfi_endproc
.LFE277:
	.size	bio_puts, .-bio_puts
	.p2align 4
	.type	bio_nwrite0, @function
bio_nwrite0:
.LFB274:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movl	$15, %esi
	call	BIO_clear_flags@PLT
	movl	32(%r12), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L55
	movq	56(%r12), %rsi
	movl	8(%rsi), %eax
	movq	$0, 48(%rsi)
	testl	%eax, %eax
	jne	.L68
	movq	16(%rsi), %rdx
	movq	32(%rsi), %rcx
	cmpq	%rcx, %rdx
	je	.L69
	movq	24(%rsi), %rdi
	movq	%rcx, %rax
	subq	%rdx, %rax
	addq	%rdi, %rdx
	cmpq	%rdx, %rcx
	jbe	.L59
	addq	%rcx, %rdi
.L60:
	movq	%rcx, %r8
	subq	%rdx, %r8
	cmpq	%rdi, %rcx
	cmovb	%r8, %rax
	testq	%rbx, %rbx
	je	.L55
	addq	40(%rsi), %rdx
	movq	%rdx, (%rbx)
.L55:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	subq	%rcx, %rdx
	jmp	.L60
.L68:
	movl	$365, %r8d
	movl	$124, %edx
	movl	$122, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	$-1, %rax
	jmp	.L55
.L69:
	movl	$10, %esi
	movq	%r12, %rdi
	call	BIO_set_flags@PLT
	movq	$-1, %rax
	jmp	.L55
	.cfi_endproc
.LFE274:
	.size	bio_nwrite0, .-bio_nwrite0
	.p2align 4
	.type	bio_ctrl, @function
bio_ctrl:
.LFB276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	56(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$147, %esi
	ja	.L132
	movq	%rdx, %rbx
	movl	%esi, %esi
	leaq	.L73(%rip), %rdx
	movq	%rdi, %r12
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L73:
	.long	.L132-.L73
	.long	.L92-.L73
	.long	.L91-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L90-.L73
	.long	.L89-.L73
	.long	.L88-.L73
	.long	.L111-.L73
	.long	.L86-.L73
	.long	.L85-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L132-.L73
	.long	.L84-.L73
	.long	.L83-.L73
	.long	.L82-.L73
	.long	.L81-.L73
	.long	.L80-.L73
	.long	.L79-.L73
	.long	.L78-.L73
	.long	.L77-.L73
	.long	.L76-.L73
	.long	.L75-.L73
	.long	.L74-.L73
	.long	.L72-.L73
	.text
.L84:
	cmpq	$0, 0(%r13)
	je	.L93
	movl	$430, %r8d
	movl	$123, %edx
	movl	$103, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L132:
	xorl	%eax, %eax
.L70:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L134
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L72:
	.cfi_restore_state
	movq	$0, 48(%r13)
	movl	$1, %eax
	jmp	.L70
.L74:
	movabsq	$9223372036854775807, %rax
	testq	%rbx, %rbx
	movq	%rcx, %rsi
	cmovs	%rax, %rbx
	call	bio_nwrite0
	cmpq	%rbx, %rax
	cmovg	%rbx, %rax
	testq	%rax, %rax
	jle	.L70
	movq	56(%r12), %rdx
	addq	%rax, 16(%rdx)
	jmp	.L70
.L75:
	movq	%rcx, %rsi
	call	bio_nwrite0
	jmp	.L70
.L76:
	testq	%rbx, %rbx
	movl	$15, %esi
	movq	%rcx, -56(%rbp)
	movabsq	$9223372036854775807, %rax
	cmovs	%rax, %rbx
	call	BIO_clear_flags@PLT
	movl	32(%r12), %ecx
	testl	%ecx, %ecx
	je	.L132
	movq	56(%r12), %rax
	movq	-56(%rbp), %rcx
	movq	(%rax), %rax
	movq	56(%rax), %rdx
	movq	16(%rdx), %rdi
	movq	$0, 48(%rdx)
	testq	%rdi, %rdi
	je	.L135
	movq	32(%rdx), %r8
	movq	24(%rdx), %rsi
	movq	%r8, %rax
	leaq	(%rdi,%rsi), %r9
	subq	%rsi, %rax
	cmpq	%r9, %r8
	cmovnb	%rdi, %rax
	testq	%rcx, %rcx
	je	.L106
	addq	40(%rdx), %rsi
	movq	%rsi, (%rcx)
.L106:
	cmpq	%rax, %rbx
	cmovle	%rbx, %rax
	testq	%rax, %rax
	jle	.L70
	movq	56(%r12), %rdx
	movq	(%rdx), %rdx
	movq	56(%rdx), %rdx
	subq	%rax, 16(%rdx)
	je	.L108
	movq	24(%rdx), %rcx
	addq	%rax, %rcx
	movq	%rcx, 24(%rdx)
	cmpq	32(%rdx), %rcx
	jne	.L70
.L108:
	movq	$0, 24(%rdx)
	jmp	.L70
.L77:
	movl	$15, %esi
	movq	%rcx, -56(%rbp)
	call	BIO_clear_flags@PLT
	movl	32(%r12), %esi
	testl	%esi, %esi
	je	.L132
	movq	56(%r12), %rax
	movq	-56(%rbp), %rcx
	movq	(%rax), %rax
	movq	56(%rax), %rdx
	movq	16(%rdx), %rax
	movq	$0, 48(%rdx)
	testq	%rax, %rax
	je	.L136
	movq	32(%rdx), %rdi
	movq	24(%rdx), %rsi
	movq	%rdi, %r8
	leaq	(%rax,%rsi), %r9
	subq	%rsi, %r8
	cmpq	%r9, %rdi
	cmovb	%r8, %rax
	testq	%rcx, %rcx
	je	.L70
	addq	40(%rdx), %rsi
	movq	%rsi, (%rcx)
	jmp	.L70
.L78:
	movl	$1, 8(%r13)
	movl	$1, %eax
	jmp	.L70
.L85:
	cmpq	$0, 40(%r13)
	je	.L132
	movq	16(%r13), %rax
	jmp	.L70
.L80:
	cmpq	$0, 0(%r13)
	je	.L132
	movl	8(%r13), %edi
	testl	%edi, %edi
	jne	.L132
	movq	32(%r13), %rax
	subq	16(%r13), %rax
	jmp	.L70
.L81:
	movl	$1, %eax
	testq	%r13, %r13
	je	.L70
	movq	0(%r13), %rdx
	testq	%rdx, %rdx
	je	.L70
	movq	56(%rdx), %rcx
	pxor	%xmm0, %xmm0
	movq	$0, (%rcx)
	movl	$0, 32(%rdx)
	movups	%xmm0, 16(%rcx)
	movq	$0, 0(%r13)
	movl	$0, 32(%rdi)
	movups	%xmm0, 16(%r13)
	jmp	.L70
.L82:
	cmpq	$0, 0(%r13)
	jne	.L95
	movq	56(%rcx), %rbx
	cmpq	$0, (%rbx)
	je	.L96
.L95:
	movl	$619, %r8d
	movl	$123, %edx
	movl	$121, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L70
.L83:
	movq	32(%r13), %rax
	jmp	.L70
.L79:
	movq	48(%r13), %rax
	jmp	.L70
.L89:
	movl	%ebx, 36(%rdi)
	movl	$1, %eax
	jmp	.L70
.L90:
	movslq	36(%rdi), %rax
	jmp	.L70
.L91:
	movq	0(%r13), %rdx
	movl	$1, %eax
	testq	%rdx, %rdx
	je	.L70
	movq	56(%rdx), %rdx
	cmpq	$0, 16(%rdx)
	jne	.L132
	movl	8(%rdx), %edx
	testl	%edx, %edx
	jne	.L70
	jmp	.L132
.L92:
	cmpq	$0, 40(%r13)
	je	.L132
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movups	%xmm0, 16(%r13)
	jmp	.L70
.L86:
	movq	56(%rcx), %rax
	movq	32(%r13), %rdx
	movq	%rdx, 32(%rax)
	movl	$1, %eax
	jmp	.L70
.L88:
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L132
	movq	56(%rax), %rax
	movq	16(%rax), %rax
	jmp	.L70
.L111:
	movl	$1, %eax
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L93:
	testq	%rbx, %rbx
	je	.L137
	movl	$1, %eax
	cmpq	%rbx, 32(%r13)
	je	.L70
	movq	40(%r13), %rdi
	movl	$439, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	CRYPTO_free@PLT
	movq	%rbx, 32(%r13)
	movq	-56(%rbp), %rax
	movq	$0, 40(%r13)
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L96:
	cmpq	$0, 40(%r13)
	je	.L138
.L97:
	cmpq	$0, 40(%rbx)
	je	.L139
.L99:
	movq	%rcx, 0(%r13)
	movl	$1, %eax
	movl	$0, 8(%r13)
	movq	$0, 48(%r13)
	movq	%r12, (%rbx)
	movl	$0, 8(%rbx)
	movq	$0, 48(%rbx)
	movl	$1, 32(%r12)
	movl	$1, 32(%rcx)
	jmp	.L70
.L137:
	movl	$433, %r8d
	movl	$125, %edx
	movl	$103, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L70
.L136:
	leaq	-41(%rbp), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	bio_read
	cltq
	jmp	.L70
.L135:
	leaq	-41(%rbp), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	bio_read
	cltq
	jmp	.L106
.L138:
	movq	32(%r13), %rdi
	movl	$624, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rcx, -56(%rbp)
	call	CRYPTO_malloc@PLT
	movq	-56(%rbp), %rcx
	movl	$626, %r8d
	testq	%rax, %rax
	movq	%rax, 40(%r13)
	je	.L133
	pxor	%xmm0, %xmm0
	movups	%xmm0, 16(%r13)
	jmp	.L97
.L139:
	movq	32(%rbx), %rdi
	movl	$634, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rcx, -56(%rbp)
	call	CRYPTO_malloc@PLT
	movq	-56(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, 40(%rbx)
	je	.L140
	pxor	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
	jmp	.L99
.L140:
	movl	$636, %r8d
.L133:
	movl	$65, %edx
	movl	$121, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L70
.L134:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE276:
	.size	bio_ctrl, .-bio_ctrl
	.p2align 4
	.globl	BIO_s_bio
	.type	BIO_s_bio, @function
BIO_s_bio:
.LFB267:
	.cfi_startproc
	endbr64
	leaq	methods_biop(%rip), %rax
	ret
	.cfi_endproc
.LFE267:
	.size	BIO_s_bio, .-BIO_s_bio
	.p2align 4
	.globl	BIO_new_bio_pair
	.type	BIO_new_bio_pair, @function
BIO_new_bio_pair:
.LFB280:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	leaq	methods_biop(%rip), %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	%rsi, -56(%rbp)
	call	BIO_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L145
	leaq	methods_biop(%rip), %rdi
	call	BIO_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L145
	cmpq	$0, -56(%rbp)
	jne	.L146
	testq	%r14, %r14
	jne	.L147
.L148:
	xorl	%edx, %edx
	movq	%r15, %rcx
	movl	$138, %esi
	movq	%r12, %rdi
	call	BIO_ctrl@PLT
	testl	%eax, %eax
	je	.L144
	movl	$1, %eax
.L150:
	movq	%r12, 0(%r13)
	movq	%r15, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	.cfi_restore_state
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movl	$136, %esi
	movq	%r15, %rdi
	call	BIO_ctrl@PLT
	testl	%eax, %eax
	jne	.L148
	.p2align 4,,10
	.p2align 3
.L144:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	BIO_free@PLT
	movq	%r15, %rdi
	xorl	%r15d, %r15d
	call	BIO_free@PLT
	xorl	%eax, %eax
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L145:
	xorl	%r15d, %r15d
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L146:
	movq	-56(%rbp), %rdx
	xorl	%ecx, %ecx
	movl	$136, %esi
	movq	%r12, %rdi
	call	BIO_ctrl@PLT
	testl	%eax, %eax
	je	.L144
	testq	%r14, %r14
	jne	.L147
	jmp	.L148
	.cfi_endproc
.LFE280:
	.size	BIO_new_bio_pair, .-BIO_new_bio_pair
	.p2align 4
	.globl	BIO_ctrl_get_write_guarantee
	.type	BIO_ctrl_get_write_guarantee, @function
BIO_ctrl_get_write_guarantee:
.LFB281:
	.cfi_startproc
	endbr64
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$140, %esi
	jmp	BIO_ctrl@PLT
	.cfi_endproc
.LFE281:
	.size	BIO_ctrl_get_write_guarantee, .-BIO_ctrl_get_write_guarantee
	.p2align 4
	.globl	BIO_ctrl_get_read_request
	.type	BIO_ctrl_get_read_request, @function
BIO_ctrl_get_read_request:
.LFB282:
	.cfi_startproc
	endbr64
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$141, %esi
	jmp	BIO_ctrl@PLT
	.cfi_endproc
.LFE282:
	.size	BIO_ctrl_get_read_request, .-BIO_ctrl_get_read_request
	.p2align 4
	.globl	BIO_ctrl_reset_read_request
	.type	BIO_ctrl_reset_read_request, @function
BIO_ctrl_reset_read_request:
.LFB283:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$147, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	BIO_ctrl@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE283:
	.size	BIO_ctrl_reset_read_request, .-BIO_ctrl_reset_read_request
	.p2align 4
	.globl	BIO_nread0
	.type	BIO_nread0, @function
BIO_nread0:
.LFB284:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	32(%rdi), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%eax, %eax
	je	.L165
	xorl	%edx, %edx
	movq	%rsi, %rcx
	movl	$143, %esi
	call	BIO_ctrl@PLT
	movl	$2147483647, %edx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	cmpq	$2147483647, %rax
	cmovg	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	.cfi_restore_state
	movl	$753, %r8d
	movl	$120, %edx
	movl	$124, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE284:
	.size	BIO_nread0, .-BIO_nread0
	.p2align 4
	.globl	BIO_nread
	.type	BIO_nread, @function
BIO_nread:
.LFB285:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	32(%rdi), %eax
	testl	%eax, %eax
	je	.L170
	movq	%rsi, %rcx
	movslq	%edx, %rdx
	movl	$144, %esi
	movq	%rdi, %rbx
	call	BIO_ctrl@PLT
	movl	%eax, %r8d
	testl	%eax, %eax
	jle	.L166
	cltq
	addq	%rax, 88(%rbx)
.L166:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore_state
	movl	$769, %r8d
	movl	$120, %edx
	movl	$123, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %r8d
	jmp	.L166
	.cfi_endproc
.LFE285:
	.size	BIO_nread, .-BIO_nread
	.p2align 4
	.globl	BIO_nwrite0
	.type	BIO_nwrite0, @function
BIO_nwrite0:
.LFB286:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	32(%rdi), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%eax, %eax
	je	.L176
	xorl	%edx, %edx
	movq	%rsi, %rcx
	movl	$145, %esi
	call	BIO_ctrl@PLT
	movl	$2147483647, %edx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	cmpq	$2147483647, %rax
	cmovg	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_restore_state
	movl	$784, %r8d
	movl	$120, %edx
	movl	$122, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE286:
	.size	BIO_nwrite0, .-BIO_nwrite0
	.p2align 4
	.globl	BIO_nwrite
	.type	BIO_nwrite, @function
BIO_nwrite:
.LFB287:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	32(%rdi), %eax
	testl	%eax, %eax
	je	.L181
	movq	%rsi, %rcx
	movslq	%edx, %rdx
	movl	$146, %esi
	movq	%rdi, %rbx
	call	BIO_ctrl@PLT
	movl	%eax, %r8d
	testl	%eax, %eax
	jle	.L177
	cltq
	addq	%rax, 96(%rbx)
.L177:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	.cfi_restore_state
	movl	$800, %r8d
	movl	$120, %edx
	movl	$125, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %r8d
	jmp	.L177
	.cfi_endproc
.LFE287:
	.size	BIO_nwrite, .-BIO_nwrite
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"BIO pair"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	methods_biop, @object
	.size	methods_biop, 96
methods_biop:
	.long	1043
	.zero	4
	.quad	.LC1
	.quad	bwrite_conv
	.quad	bio_write
	.quad	bread_conv
	.quad	bio_read
	.quad	bio_puts
	.quad	0
	.quad	bio_ctrl
	.quad	bio_new
	.quad	bio_free
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
