	.file	"ssl_lib.c"
	.text
	.p2align 4
	.type	ssl_io_intern, @function
ssl_io_intern:
.LFB1727:
	.cfi_startproc
	endbr64
	movl	24(%rdi), %eax
	movq	(%rdi), %r8
	movq	8(%rdi), %rsi
	movq	16(%rdi), %rdx
	cmpl	$1, %eax
	je	.L2
	cmpl	$2, %eax
	je	.L3
	testl	%eax, %eax
	je	.L2
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	32(%rdi), %rax
	leaq	6168(%r8), %rcx
	movq	%r8, %rdi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L3:
	movq	32(%rdi), %rax
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE1727:
	.size	ssl_io_intern, .-ssl_io_intern
	.p2align 4
	.type	ssl_do_handshake_intern, @function
ssl_do_handshake_intern:
.LFB1801:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	jmp	*48(%rdi)
	.cfi_endproc
.LFE1801:
	.size	ssl_do_handshake_intern, .-ssl_do_handshake_intern
	.p2align 4
	.type	ssl_cipher_id_cmp_BSEARCH_CMP_FN, @function
ssl_cipher_id_cmp_BSEARCH_CMP_FN:
.LFB1909:
	.cfi_startproc
	endbr64
	movl	24(%rsi), %edx
	movl	$1, %eax
	cmpl	%edx, 24(%rdi)
	ja	.L7
	sbbl	%eax, %eax
.L7:
	ret
	.cfi_endproc
.LFE1909:
	.size	ssl_cipher_id_cmp_BSEARCH_CMP_FN, .-ssl_cipher_id_cmp_BSEARCH_CMP_FN
	.p2align 4
	.type	ct_permissive, @function
ct_permissive:
.LFB1916:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1916:
	.size	ct_permissive, .-ct_permissive
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/ssl/ssl_lib.c"
	.text
	.p2align 4
	.globl	ssl_undefined_function
	.type	ssl_undefined_function, @function
ssl_undefined_function:
.LFB1805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$66, %edx
	movl	$197, %esi
	movl	$20, %edi
	movl	$3687, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1805:
	.size	ssl_undefined_function, .-ssl_undefined_function
	.p2align 4
	.type	ssl_undefined_function_7, @function
ssl_undefined_function_7:
.LFB1644:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$66, %edx
	movl	$197, %esi
	movl	$20, %edi
	movl	$3687, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1644:
	.size	ssl_undefined_function_7, .-ssl_undefined_function_7
	.p2align 4
	.type	ssl_undefined_function_6, @function
ssl_undefined_function_6:
.LFB1643:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$66, %edx
	movl	$197, %esi
	movl	$20, %edi
	movl	$3687, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1643:
	.size	ssl_undefined_function_6, .-ssl_undefined_function_6
	.p2align 4
	.type	ssl_undefined_function_5, @function
ssl_undefined_function_5:
.LFB1642:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$66, %edx
	movl	$197, %esi
	movl	$20, %edi
	movl	$3687, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1642:
	.size	ssl_undefined_function_5, .-ssl_undefined_function_5
	.p2align 4
	.type	ssl_undefined_function_4, @function
ssl_undefined_function_4:
.LFB1641:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$66, %edx
	movl	$197, %esi
	movl	$20, %edi
	movl	$3687, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1641:
	.size	ssl_undefined_function_4, .-ssl_undefined_function_4
	.p2align 4
	.type	ssl_undefined_function_3, @function
ssl_undefined_function_3:
.LFB1640:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$66, %edx
	movl	$197, %esi
	movl	$20, %edi
	movl	$3687, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1640:
	.size	ssl_undefined_function_3, .-ssl_undefined_function_3
	.p2align 4
	.type	ssl_undefined_function_2, @function
ssl_undefined_function_2:
.LFB1639:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$66, %edx
	movl	$197, %esi
	movl	$20, %edi
	movl	$3687, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1639:
	.size	ssl_undefined_function_2, .-ssl_undefined_function_2
	.p2align 4
	.type	ssl_undefined_function_1, @function
ssl_undefined_function_1:
.LFB1638:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$66, %edx
	movl	$197, %esi
	movl	$20, %edi
	movl	$3687, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1638:
	.size	ssl_undefined_function_1, .-ssl_undefined_function_1
	.p2align 4
	.type	ssl_start_async_job, @function
ssl_start_async_job:
.LFB1726:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	6160(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L39
.L28:
	leaq	-28(%rbp), %rdx
	leaq	6152(%rbx), %rdi
	movl	$40, %r9d
	movq	%r12, %rcx
	call	ASYNC_start_job@PLT
	cmpl	$2, %eax
	je	.L30
	jg	.L31
	testl	%eax, %eax
	je	.L32
	cmpl	$1, %eax
	jne	.L34
	movl	$6, 40(%rbx)
	movl	$-1, %eax
	.p2align 4,,10
	.p2align 3
.L27:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L40
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	cmpl	$3, %eax
	jne	.L34
	movq	$0, 6152(%rbx)
	movl	-28(%rbp), %eax
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L39:
	movq	%r8, -40(%rbp)
	call	ASYNC_WAIT_CTX_new@PLT
	movq	-40(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, 6160(%rbx)
	movq	%rax, %rsi
	jne	.L28
	movl	$-1, %eax
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L30:
	movl	$5, 40(%rbx)
	movl	$-1, %eax
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L32:
	movl	$1683, %r8d
	movl	$405, %edx
	movl	$389, %esi
	movl	$1, 40(%rbx)
	leaq	.LC0(%rip), %rcx
	movl	$20, %edi
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L34:
	movl	$1696, %r8d
	movl	$68, %edx
	movl	$389, %esi
	movl	$1, 40(%rbx)
	leaq	.LC0(%rip), %rcx
	movl	$20, %edi
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L27
.L40:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1726:
	.size	ssl_start_async_job, .-ssl_start_async_job
	.p2align 4
	.type	ssl_check_allowed_versions, @function
ssl_check_allowed_versions:
.LFB1653:
	.cfi_startproc
	cmpl	$256, %edi
	je	.L69
	movl	%edi, %eax
	sarl	$8, %eax
	cmpl	$256, %esi
	je	.L45
	movl	%esi, %edx
	sarl	$8, %edx
	cmpl	$254, %edx
	je	.L45
	cmpl	$254, %eax
	jne	.L70
.L44:
	xorl	%eax, %eax
	testl	%esi, %esi
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	cmpl	$254, %eax
	je	.L68
	xorl	%eax, %eax
	testl	%edi, %edi
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	movl	$1, %eax
	cmpl	$256, %esi
	je	.L41
	movl	%esi, %edx
	sarl	$8, %edx
	cmpl	$254, %edx
	jne	.L44
	ret
.L70:
	testl	%edi, %edi
	jne	.L71
.L68:
	movl	$1, %eax
.L41:
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	movl	$772, %eax
	testl	%esi, %esi
	cmove	%eax, %esi
	movl	$1, %eax
	cmpl	$768, %edi
	je	.L41
	setle	%al
	cmpl	$767, %esi
	setg	%dl
	andl	%edx, %eax
	xorl	$1, %eax
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1653:
	.size	ssl_check_allowed_versions, .-ssl_check_allowed_versions
	.p2align 4
	.type	ssl_session_hash, @function
ssl_session_hash:
.LFB1775:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	344(%rdi), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	336(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	cmpq	$3, %rdx
	jbe	.L76
.L73:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	movl	(%rsi), %eax
	jne	.L77
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	leaq	-12(%rbp), %rdi
	movl	$4, %ecx
	movl	$0, -12(%rbp)
	call	__memcpy_chk@PLT
	movq	%rax, %rsi
	jmp	.L73
.L77:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1775:
	.size	ssl_session_hash, .-ssl_session_hash
	.p2align 4
	.type	tlsa_free.part.0, @function
tlsa_free.part.0:
.LFB1974:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$186, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	call	CRYPTO_free@PLT
	movq	24(%r12), %rdi
	call	EVP_PKEY_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$188, %edx
	popq	%r12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE1974:
	.size	tlsa_free.part.0, .-tlsa_free.part.0
	.p2align 4
	.type	ssl_session_cmp, @function
ssl_session_cmp:
.LFB1776:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	cmpl	%eax, (%rdi)
	jne	.L80
	movq	336(%rdi), %rdx
	cmpq	336(%rsi), %rdx
	je	.L84
.L80:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	addq	$344, %rsi
	addq	$344, %rdi
	jmp	memcmp@PLT
	.cfi_endproc
.LFE1776:
	.size	ssl_session_cmp, .-ssl_session_cmp
	.p2align 4
	.type	dup_ca_names.part.0, @function
dup_ca_names.part.0:
.LFB1979:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	call	OPENSSL_sk_new_null@PLT
	testq	%rax, %rax
	je	.L91
	movq	%rax, %r14
	xorl	%ebx, %ebx
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L90:
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	call	X509_NAME_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L93
	movl	%ebx, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	OPENSSL_sk_insert@PLT
	testl	%eax, %eax
	je	.L94
	addl	$1, %ebx
.L87:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L90
	movq	%r14, (%r15)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	movq	X509_NAME_free@GOTPCREL(%rip), %rsi
	movq	%r14, %rdi
	call	OPENSSL_sk_pop_free@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	%eax, -52(%rbp)
	call	X509_NAME_free@PLT
	movq	X509_NAME_free@GOTPCREL(%rip), %rsi
	movq	%r14, %rdi
	call	OPENSSL_sk_pop_free@PLT
	movl	-52(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1979:
	.size	dup_ca_names.part.0, .-dup_ca_names.part.0
	.p2align 4
	.type	ct_move_scts, @function
ct_move_scts:
.LFB1911:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	$0, (%rdi)
	je	.L96
.L98:
	xorl	%r13d, %r13d
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L101:
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	SCT_set_source@PLT
	cmpl	$1, %eax
	jne	.L100
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jle	.L100
	addl	$1, %r13d
.L97:
	movq	%r14, %rdi
	call	OPENSSL_sk_pop@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L101
.L95:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	$-1, %r13d
	call	OPENSSL_sk_push@PLT
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L96:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, (%rbx)
	testq	%rax, %rax
	jne	.L98
	movl	$4694, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$345, %esi
	movl	$20, %edi
	orl	$-1, %r13d
	call	ERR_put_error@PLT
	jmp	.L95
	.cfi_endproc
.LFE1911:
	.size	ct_move_scts, .-ct_move_scts
	.p2align 4
	.type	ct_strict, @function
ct_strict:
.LFB1917:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testq	%rsi, %rsi
	je	.L104
	movq	%rsi, %rdi
	movq	%rsi, %r12
	call	OPENSSL_sk_num@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jle	.L104
	xorl	%ebx, %ebx
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L113:
	addl	$1, %ebx
	cmpl	%r13d, %ebx
	je	.L104
.L106:
	movq	%r12, %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	call	SCT_get_validation_status@PLT
	cmpl	$2, %eax
	jne	.L113
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	movl	$4851, %r8d
	movl	$216, %edx
	movl	$349, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1917:
	.size	ct_strict, .-ct_strict
	.p2align 4
	.type	SSL_CTX_free.part.0, @function
SSL_CTX_free.part.0:
.LFB1990:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	416(%rdi), %rdi
	call	X509_VERIFY_PARAM_free@PLT
	movq	872(%r12), %rdi
	movl	$174, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	880(%r12), %rdi
	movq	$0, 872(%r12)
	movl	$177, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	cmpq	$0, 40(%r12)
	movq	$0, 880(%r12)
	movb	$0, 888(%r12)
	je	.L115
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	SSL_CTX_flush_sessions@PLT
.L115:
	leaq	232(%r12), %rdx
	movq	%r12, %rsi
	movl	$1, %edi
	call	CRYPTO_free_ex_data@PLT
	movq	40(%r12), %rdi
	call	OPENSSL_LH_free@PLT
	movq	32(%r12), %rdi
	call	X509_STORE_free@PLT
	movq	432(%r12), %rdi
	call	CTLOG_STORE_free@PLT
	movq	8(%r12), %rdi
	call	OPENSSL_sk_free@PLT
	movq	16(%r12), %rdi
	call	OPENSSL_sk_free@PLT
	movq	24(%r12), %rdi
	call	OPENSSL_sk_free@PLT
	movq	320(%r12), %rdi
	call	ssl_cert_free@PLT
	movq	X509_NAME_free@GOTPCREL(%rip), %r13
	movq	280(%r12), %rdi
	movq	%r13, %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	288(%r12), %rdi
	movq	%r13, %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	X509_free@GOTPCREL(%rip), %rsi
	movq	256(%r12), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	904(%r12), %rdi
	movq	$0, 264(%r12)
	call	OPENSSL_sk_free@PLT
	movq	%r12, %rdi
	call	SSL_CTX_SRP_CTX_free@PLT
	movq	488(%r12), %rdi
	call	ENGINE_finish@PLT
	movq	592(%r12), %rdi
	movl	$3215, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	608(%r12), %rdi
	movl	$3216, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	632(%r12), %rdi
	movl	$3218, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	544(%r12), %rdi
	movl	$3219, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_secure_free@PLT
	movq	920(%r12), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movl	$3223, %edx
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE1990:
	.size	SSL_CTX_free.part.0, .-SSL_CTX_free.part.0
	.p2align 4
	.type	tlsa_free, @function
tlsa_free:
.LFB1647:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L117
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$186, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	call	CRYPTO_free@PLT
	movq	24(%r12), %rdi
	call	EVP_PKEY_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$188, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L117:
	ret
	.cfi_endproc
.LFE1647:
	.size	tlsa_free, .-tlsa_free
	.p2align 4
	.type	SSL_free.part.0, @function
SSL_free.part.0:
.LFB1992:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	208(%rdi), %rdi
	call	X509_VERIFY_PARAM_free@PLT
	movq	224(%r12), %rdi
	leaq	tlsa_free(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	X509_free@GOTPCREL(%rip), %r13
	movq	$0, 224(%r12)
	movq	232(%r12), %rdi
	movq	%r13, %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	248(%r12), %rdi
	movq	$0, 232(%r12)
	call	X509_free@PLT
	pxor	%xmm0, %xmm0
	xorl	%edi, %edi
	movq	%r12, %rsi
	movq	$-1, 260(%r12)
	leaq	1464(%r12), %rdx
	movups	%xmm0, 240(%r12)
	call	CRYPTO_free_ex_data@PLT
	cmpq	$0, 32(%r12)
	je	.L123
	movq	24(%r12), %rdi
	call	BIO_pop@PLT
	movq	32(%r12), %rdi
	movq	%rax, 24(%r12)
	call	BIO_free@PLT
	movq	$0, 32(%r12)
.L123:
	movq	24(%r12), %rdi
	call	BIO_free_all@PLT
	movq	16(%r12), %rdi
	call	BIO_free_all@PLT
	movq	136(%r12), %rdi
	call	BUF_MEM_free@PLT
	movq	288(%r12), %rdi
	call	OPENSSL_sk_free@PLT
	movq	296(%r12), %rdi
	call	OPENSSL_sk_free@PLT
	movq	304(%r12), %rdi
	call	OPENSSL_sk_free@PLT
	movq	280(%r12), %rdi
	call	OPENSSL_sk_free@PLT
	cmpq	$0, 1296(%r12)
	je	.L124
	movq	%r12, %rdi
	call	ssl_clear_bad_session@PLT
	movq	1296(%r12), %rdi
	call	SSL_SESSION_free@PLT
.L124:
	movq	1304(%r12), %rdi
	call	SSL_SESSION_free@PLT
	movq	1312(%r12), %rdi
	movl	$1176, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	1088(%r12), %rdi
	testq	%rdi, %rdi
	je	.L125
	call	EVP_CIPHER_CTX_free@PLT
	movq	$0, 1088(%r12)
.L125:
	movq	1136(%r12), %rdi
	testq	%rdi, %rdi
	je	.L126
	call	EVP_CIPHER_CTX_free@PLT
	movq	$0, 1136(%r12)
.L126:
	movq	1112(%r12), %rdi
	call	EVP_MD_CTX_free@PLT
	movq	1160(%r12), %rdi
	movq	$0, 1112(%r12)
	call	EVP_MD_CTX_free@PLT
	movq	1168(%r12), %rdi
	movq	$0, 1160(%r12)
	call	ssl_cert_free@PLT
	movq	6264(%r12), %rdi
	movl	$1181, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	1600(%r12), %rdi
	movl	$1184, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	1904(%r12), %rdi
	testq	%rdi, %rdi
	je	.L128
	movl	$-1, %eax
	lock xaddl	%eax, 156(%rdi)
	subl	$1, %eax
	testl	%eax, %eax
	je	.L148
	jle	.L130
.L128:
	movq	1680(%r12), %rdi
	movl	$1187, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	1696(%r12), %rdi
	movl	$1188, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	1712(%r12), %rdi
	movl	$1189, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	1728(%r12), %rdi
	movl	$1190, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	1640(%r12), %rdi
	movq	X509_EXTENSION_free@GOTPCREL(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	OCSP_RESPID_free@GOTPCREL(%rip), %rsi
	movq	1632(%r12), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	1888(%r12), %rdi
	call	SCT_LIST_free@PLT
	movq	1616(%r12), %rdi
	movl	$1198, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	1648(%r12), %rdi
	movl	$1200, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	1776(%r12), %rdi
	movl	$1201, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	1824(%r12), %rdi
	movl	$1202, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	1856(%r12), %rdi
	movl	$1203, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	1944(%r12), %rdi
	movl	$1204, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	1968(%r12), %rdi
	call	EVP_MD_CTX_free@PLT
	movq	X509_NAME_free@GOTPCREL(%rip), %r14
	movq	1472(%r12), %rdi
	movq	%r14, %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	1480(%r12), %rdi
	movq	%r14, %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	1448(%r12), %rdi
	movq	%r13, %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L132
	movq	%r12, %rdi
	call	*32(%rax)
.L132:
	leaq	2112(%r12), %rdi
	call	RECORD_LAYER_release@PLT
	movq	1440(%r12), %rdi
	testq	%rdi, %rdi
	je	.L134
	movl	$-1, %eax
	lock xaddl	%eax, 156(%rdi)
	subl	$1, %eax
	testl	%eax, %eax
	je	.L149
	jle	.L136
.L134:
	movq	6160(%r12), %rdi
	call	ASYNC_WAIT_CTX_free@PLT
	movq	1792(%r12), %rdi
	movl	$1222, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	1912(%r12), %rdi
	call	OPENSSL_sk_free@PLT
	movq	6216(%r12), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$1231, %edx
	popq	%r12
	leaq	.LC0(%rip), %rsi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L149:
	.cfi_restore_state
.L136:
	call	SSL_CTX_free.part.0
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L148:
.L130:
	call	SSL_CTX_free.part.0
	jmp	.L128
	.cfi_endproc
.LFE1992:
	.size	SSL_free.part.0, .-SSL_free.part.0
	.p2align 4
	.globl	SSL_clear
	.type	SSL_clear, @function
SSL_clear:
.LFB1655:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpq	$0, 8(%rdi)
	je	.L176
	movq	%rdi, %rbx
	call	ssl_clear_bad_session@PLT
	testl	%eax, %eax
	jne	.L177
.L153:
	movq	1304(%rbx), %rdi
	call	SSL_SESSION_free@PLT
	movq	1312(%rbx), %rdi
	movq	$0, 1304(%rbx)
	movl	$590, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	1928(%rbx), %r12d
	movq	$0, 1312(%rbx)
	movq	$0, 1320(%rbx)
	movl	$0, 1248(%rbx)
	movq	$0, 6232(%rbx)
	movl	$0, 1400(%rbx)
	movl	$0, 200(%rbx)
	movl	$0, 68(%rbx)
	testl	%r12d, %r12d
	jne	.L178
	movq	%rbx, %rdi
	call	ossl_statem_clear@PLT
	movq	8(%rbx), %rax
	movq	136(%rbx), %rdi
	movl	(%rax), %eax
	movl	$1, 40(%rbx)
	movl	%eax, (%rbx)
	movl	%eax, 1524(%rbx)
	call	BUF_MEM_free@PLT
	movq	1088(%rbx), %rdi
	movq	$0, 136(%rbx)
	testq	%rdi, %rdi
	je	.L155
	call	EVP_CIPHER_CTX_free@PLT
	movq	$0, 1088(%rbx)
.L155:
	movq	1136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L156
	call	EVP_CIPHER_CTX_free@PLT
	movq	$0, 1136(%rbx)
.L156:
	movq	1112(%rbx), %rdi
	call	EVP_MD_CTX_free@PLT
	movq	1160(%rbx), %rdi
	movq	$0, 1112(%rbx)
	call	EVP_MD_CTX_free@PLT
	movq	1968(%rbx), %rdi
	movq	$0, 1160(%rbx)
	movl	$0, 1520(%rbx)
	movl	$-1, 1932(%rbx)
	call	EVP_MD_CTX_free@PLT
	movq	248(%rbx), %rdi
	movq	$0, 1968(%rbx)
	movq	$-1, 260(%rbx)
	call	X509_free@PLT
	pxor	%xmm0, %xmm0
	movq	208(%rbx), %rdi
	xorl	%esi, %esi
	movups	%xmm0, 240(%rbx)
	call	X509_VERIFY_PARAM_move_peername@PLT
	movq	6264(%rbx), %rdi
	movl	$632, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	1440(%rbx), %rdx
	movq	8(%rbx), %rax
	movq	%rbx, %rdi
	movq	$0, 6264(%rbx)
	movq	$0, 6272(%rbx)
	cmpq	(%rdx), %rax
	je	.L157
	call	*32(%rax)
	movq	1440(%rbx), %rax
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 8(%rbx)
	call	*16(%rax)
	testl	%eax, %eax
	je	.L150
.L159:
	leaq	2112(%rbx), %rdi
	movl	$1, %r12d
	call	RECORD_LAYER_clear@PLT
.L150:
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	xorl	%r12d, %r12d
	movl	$601, %r8d
	movl	$68, %edx
	movl	$164, %esi
	leaq	.LC0(%rip), %rcx
	movl	$20, %edi
	call	ERR_put_error@PLT
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	movq	1296(%rbx), %rdi
	call	SSL_SESSION_free@PLT
	movq	$0, 1296(%rbx)
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L157:
	call	*24(%rax)
	testl	%eax, %eax
	jne	.L159
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L176:
	xorl	%r12d, %r12d
	movl	$580, %r8d
	movl	$188, %edx
	movl	$164, %esi
	leaq	.LC0(%rip), %rcx
	movl	$20, %edi
	call	ERR_put_error@PLT
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1655:
	.size	SSL_clear, .-SSL_clear
	.section	.rodata.str1.8
	.align 8
.LC1:
	.string	"TLS_AES_256_GCM_SHA384:TLS_CHACHA20_POLY1305_SHA256:TLS_AES_128_GCM_SHA256"
	.align 8
.LC2:
	.string	"ALL:!COMPLEMENTOFDEFAULT:!eNULL"
	.text
	.p2align 4
	.globl	SSL_CTX_set_ssl_version
	.type	SSL_CTX_set_ssl_version, @function
SSL_CTX_set_ssl_version:
.LFB1656:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%rsi, (%rdi)
	leaq	.LC1(%rip), %rsi
	call	SSL_CTX_set_ciphersuites@PLT
	testl	%eax, %eax
	je	.L185
	movq	(%rbx), %rdi
	movq	24(%rbx), %rsi
	leaq	16(%rbx), %rcx
	leaq	8(%rbx), %rdx
	movq	320(%rbx), %r9
	leaq	.LC2(%rip), %r8
	call	ssl_create_cipher_list@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L183
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jle	.L183
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L183:
	.cfi_restore_state
	movl	$672, %r8d
	movl	$230, %edx
	movl	$170, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	.cfi_restore_state
	movl	$663, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$230, %edx
	movl	%eax, -20(%rbp)
	movl	$170, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	movl	-20(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1656:
	.size	SSL_CTX_set_ssl_version, .-SSL_CTX_set_ssl_version
	.p2align 4
	.globl	SSL_is_dtls
	.type	SSL_is_dtls, @function
SSL_is_dtls:
.LFB1658:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	192(%rax), %rax
	movl	96(%rax), %eax
	shrl	$3, %eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE1658:
	.size	SSL_is_dtls, .-SSL_is_dtls
	.p2align 4
	.globl	SSL_up_ref
	.type	SSL_up_ref, @function
SSL_up_ref:
.LFB1659:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	lock xaddl	%eax, 1488(%rdi)
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1659:
	.size	SSL_up_ref, .-SSL_up_ref
	.p2align 4
	.globl	SSL_CTX_set_session_id_context
	.type	SSL_CTX_set_session_id_context, @function
SSL_CTX_set_session_id_context:
.LFB1660:
	.cfi_startproc
	endbr64
	cmpl	$32, %edx
	ja	.L208
	movl	%edx, %ecx
	leaq	368(%rdi), %rax
	movq	%rcx, 360(%rdi)
	cmpl	$8, %edx
	jb	.L209
	movq	(%rsi), %r8
	addq	$376, %rdi
	movq	%r8, -8(%rdi)
	movq	-8(%rsi,%rcx), %r8
	andq	$-8, %rdi
	movq	%r8, -8(%rax,%rcx)
	subq	%rdi, %rax
	addl	%eax, %edx
	subq	%rax, %rsi
	andl	$-8, %edx
	cmpl	$8, %edx
	jb	.L192
	andl	$-8, %edx
	xorl	%eax, %eax
.L195:
	movl	%eax, %ecx
	addl	$8, %eax
	movq	(%rsi,%rcx), %r8
	movq	%r8, (%rdi,%rcx)
	cmpl	%edx, %eax
	jb	.L195
.L192:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	testb	$4, %dl
	jne	.L210
	testl	%edx, %edx
	je	.L192
	movzbl	(%rsi), %r8d
	andl	$2, %edx
	movb	%r8b, 368(%rdi)
	je	.L192
	movzwl	-2(%rsi,%rcx), %edx
	movw	%dx, -2(%rax,%rcx)
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L208:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$273, %edx
	movl	$219, %esi
	movl	$20, %edi
	movl	$876, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L210:
	.cfi_restore 6
	movl	(%rsi), %edx
	movl	%edx, 368(%rdi)
	movl	-4(%rsi,%rcx), %edx
	movl	%edx, -4(%rax,%rcx)
	jmp	.L192
	.cfi_endproc
.LFE1660:
	.size	SSL_CTX_set_session_id_context, .-SSL_CTX_set_session_id_context
	.p2align 4
	.globl	SSL_set_session_id_context
	.type	SSL_set_session_id_context, @function
SSL_set_session_id_context:
.LFB1661:
	.cfi_startproc
	endbr64
	cmpl	$32, %edx
	ja	.L231
	movl	%edx, %ecx
	leaq	1264(%rdi), %rax
	movq	%rcx, 1256(%rdi)
	cmpl	$8, %edx
	jb	.L232
	movq	(%rsi), %r8
	addq	$1272, %rdi
	movq	%r8, -8(%rdi)
	movq	-8(%rsi,%rcx), %r8
	andq	$-8, %rdi
	movq	%r8, -8(%rax,%rcx)
	subq	%rdi, %rax
	addl	%eax, %edx
	subq	%rax, %rsi
	andl	$-8, %edx
	cmpl	$8, %edx
	jb	.L215
	andl	$-8, %edx
	xorl	%eax, %eax
.L218:
	movl	%eax, %ecx
	addl	$8, %eax
	movq	(%rsi,%rcx), %r8
	movq	%r8, (%rdi,%rcx)
	cmpl	%edx, %eax
	jb	.L218
.L215:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L232:
	testb	$4, %dl
	jne	.L233
	testl	%edx, %edx
	je	.L215
	movzbl	(%rsi), %r8d
	andl	$2, %edx
	movb	%r8b, 1264(%rdi)
	je	.L215
	movzwl	-2(%rsi,%rcx), %edx
	movw	%dx, -2(%rax,%rcx)
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L231:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$273, %edx
	movl	$218, %esi
	movl	$20, %edi
	movl	$890, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L233:
	.cfi_restore 6
	movl	(%rsi), %edx
	movl	%edx, 1264(%rdi)
	movl	-4(%rsi,%rcx), %edx
	movl	%edx, -4(%rax,%rcx)
	jmp	.L215
	.cfi_endproc
.LFE1661:
	.size	SSL_set_session_id_context, .-SSL_set_session_id_context
	.p2align 4
	.globl	SSL_CTX_set_generate_session_id
	.type	SSL_CTX_set_generate_session_id, @function
SSL_CTX_set_generate_session_id:
.LFB1662:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	920(%rdi), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	%r12, 408(%rbx)
	movq	920(%rbx), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1662:
	.size	SSL_CTX_set_generate_session_id, .-SSL_CTX_set_generate_session_id
	.p2align 4
	.globl	SSL_set_generate_session_id
	.type	SSL_set_generate_session_id, @function
SSL_set_generate_session_id:
.LFB1663:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	6216(%rdi), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	%r12, 1328(%rbx)
	movq	6216(%rbx), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1663:
	.size	SSL_set_generate_session_id, .-SSL_set_generate_session_id
	.p2align 4
	.globl	SSL_has_matching_session_id
	.type	SSL_has_matching_session_id, @function
SSL_has_matching_session_id:
.LFB1664:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$656, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$32, %edx
	ja	.L238
	movl	(%rdi), %eax
	movl	%edx, %edx
	movq	%rdi, %rbx
	movl	$304, %ecx
	leaq	-328(%rbp), %rdi
	movq	%rdx, -336(%rbp)
	leaq	-672(%rbp), %r12
	movl	%eax, -672(%rbp)
	call	__memcpy_chk@PLT
	movq	1904(%rbx), %rax
	movq	920(%rax), %rdi
	call	CRYPTO_THREAD_read_lock@PLT
	movq	1904(%rbx), %rax
	movq	%r12, %rsi
	movq	40(%rax), %rdi
	call	OPENSSL_LH_retrieve@PLT
	movq	%rax, %r12
	movq	1904(%rbx), %rax
	movq	920(%rax), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	xorl	%eax, %eax
	testq	%r12, %r12
	setne	%al
.L238:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L243
	addq	$656, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L243:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1664:
	.size	SSL_has_matching_session_id, .-SSL_has_matching_session_id
	.p2align 4
	.globl	SSL_CTX_set_purpose
	.type	SSL_CTX_set_purpose, @function
SSL_CTX_set_purpose:
.LFB1665:
	.cfi_startproc
	endbr64
	movq	416(%rdi), %rdi
	jmp	X509_VERIFY_PARAM_set_purpose@PLT
	.cfi_endproc
.LFE1665:
	.size	SSL_CTX_set_purpose, .-SSL_CTX_set_purpose
	.p2align 4
	.globl	SSL_set_purpose
	.type	SSL_set_purpose, @function
SSL_set_purpose:
.LFB1666:
	.cfi_startproc
	endbr64
	movq	208(%rdi), %rdi
	jmp	X509_VERIFY_PARAM_set_purpose@PLT
	.cfi_endproc
.LFE1666:
	.size	SSL_set_purpose, .-SSL_set_purpose
	.p2align 4
	.globl	SSL_CTX_set_trust
	.type	SSL_CTX_set_trust, @function
SSL_CTX_set_trust:
.LFB1667:
	.cfi_startproc
	endbr64
	movq	416(%rdi), %rdi
	jmp	X509_VERIFY_PARAM_set_trust@PLT
	.cfi_endproc
.LFE1667:
	.size	SSL_CTX_set_trust, .-SSL_CTX_set_trust
	.p2align 4
	.globl	SSL_set_trust
	.type	SSL_set_trust, @function
SSL_set_trust:
.LFB1668:
	.cfi_startproc
	endbr64
	movq	208(%rdi), %rdi
	jmp	X509_VERIFY_PARAM_set_trust@PLT
	.cfi_endproc
.LFE1668:
	.size	SSL_set_trust, .-SSL_set_trust
	.p2align 4
	.globl	SSL_set1_host
	.type	SSL_set1_host, @function
SSL_set1_host:
.LFB1669:
	.cfi_startproc
	endbr64
	movq	208(%rdi), %rdi
	xorl	%edx, %edx
	jmp	X509_VERIFY_PARAM_set1_host@PLT
	.cfi_endproc
.LFE1669:
	.size	SSL_set1_host, .-SSL_set1_host
	.p2align 4
	.globl	SSL_add1_host
	.type	SSL_add1_host, @function
SSL_add1_host:
.LFB1670:
	.cfi_startproc
	endbr64
	movq	208(%rdi), %rdi
	xorl	%edx, %edx
	jmp	X509_VERIFY_PARAM_add1_host@PLT
	.cfi_endproc
.LFE1670:
	.size	SSL_add1_host, .-SSL_add1_host
	.p2align 4
	.globl	SSL_set_hostflags
	.type	SSL_set_hostflags, @function
SSL_set_hostflags:
.LFB1671:
	.cfi_startproc
	endbr64
	movq	208(%rdi), %rdi
	jmp	X509_VERIFY_PARAM_set_hostflags@PLT
	.cfi_endproc
.LFE1671:
	.size	SSL_set_hostflags, .-SSL_set_hostflags
	.p2align 4
	.globl	SSL_get0_peername
	.type	SSL_get0_peername, @function
SSL_get0_peername:
.LFB1672:
	.cfi_startproc
	endbr64
	movq	208(%rdi), %rdi
	jmp	X509_VERIFY_PARAM_get0_peername@PLT
	.cfi_endproc
.LFE1672:
	.size	SSL_get0_peername, .-SSL_get0_peername
	.p2align 4
	.globl	SSL_CTX_dane_enable
	.type	SSL_CTX_dane_enable, @function
SSL_CTX_dane_enable:
.LFB1673:
	.cfi_startproc
	endbr64
	cmpq	$0, 872(%rdi)
	movl	$1, %eax
	je	.L270
	ret
	.p2align 4,,10
	.p2align 3
.L270:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$144, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	CRYPTO_zalloc@PLT
	movl	$145, %edx
	movl	$3, %edi
	leaq	.LC0(%rip), %rsi
	movq	%rax, %r12
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L259
	testq	%r12, %r12
	je	.L259
	movl	$672, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	testq	%rax, %rax
	je	.L256
	movq	%rax, 8(%r12)
	movb	$1, 1(%r13)
.L256:
	movl	$674, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	testq	%rax, %rax
	je	.L257
	movq	%rax, 16(%r12)
	movb	$2, 2(%r13)
.L257:
	movq	%r12, %xmm0
	movq	%r13, %xmm1
	movl	$1, %eax
	movb	$2, 888(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 872(%rbx)
.L252:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L259:
	.cfi_restore_state
	movl	$148, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	movl	$149, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	movl	$150, %r8d
	movl	$65, %edx
	leaq	.LC0(%rip), %rcx
	movl	$347, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L252
	.cfi_endproc
.LFE1673:
	.size	SSL_CTX_dane_enable, .-SSL_CTX_dane_enable
	.p2align 4
	.globl	SSL_CTX_dane_set_flags
	.type	SSL_CTX_dane_set_flags, @function
SSL_CTX_dane_set_flags:
.LFB1674:
	.cfi_startproc
	endbr64
	movq	896(%rdi), %rax
	orq	%rax, %rsi
	movq	%rsi, 896(%rdi)
	ret
	.cfi_endproc
.LFE1674:
	.size	SSL_CTX_dane_set_flags, .-SSL_CTX_dane_set_flags
	.p2align 4
	.globl	SSL_CTX_dane_clear_flags
	.type	SSL_CTX_dane_clear_flags, @function
SSL_CTX_dane_clear_flags:
.LFB1675:
	.cfi_startproc
	endbr64
	movq	896(%rdi), %rax
	notq	%rsi
	andq	%rax, %rsi
	movq	%rsi, 896(%rdi)
	ret
	.cfi_endproc
.LFE1675:
	.size	SSL_CTX_dane_clear_flags, .-SSL_CTX_dane_clear_flags
	.p2align 4
	.globl	SSL_dane_set_flags
	.type	SSL_dane_set_flags, @function
SSL_dane_set_flags:
.LFB1677:
	.cfi_startproc
	endbr64
	movq	272(%rdi), %rax
	orq	%rax, %rsi
	movq	%rsi, 272(%rdi)
	ret
	.cfi_endproc
.LFE1677:
	.size	SSL_dane_set_flags, .-SSL_dane_set_flags
	.p2align 4
	.globl	SSL_dane_clear_flags
	.type	SSL_dane_clear_flags, @function
SSL_dane_clear_flags:
.LFB1678:
	.cfi_startproc
	endbr64
	movq	272(%rdi), %rax
	notq	%rsi
	andq	%rax, %rsi
	movq	%rsi, 272(%rdi)
	ret
	.cfi_endproc
.LFE1678:
	.size	SSL_dane_clear_flags, .-SSL_dane_clear_flags
	.p2align 4
	.globl	SSL_get0_dane_authority
	.type	SSL_get0_dane_authority, @function
SSL_get0_dane_authority:
.LFB1679:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	224(%rdi), %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jle	.L283
	cmpq	$0, 1456(%rbx)
	jne	.L283
	movq	240(%rbx), %rax
	testq	%rax, %rax
	je	.L278
	testq	%r13, %r13
	je	.L279
	movq	248(%rbx), %rdx
	movq	%rdx, 0(%r13)
.L279:
	testq	%r12, %r12
	je	.L278
	xorl	%edx, %edx
	cmpq	$0, 248(%rbx)
	je	.L292
.L281:
	movq	%rdx, (%r12)
.L278:
	movl	260(%rbx), %eax
.L275:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L292:
	.cfi_restore_state
	movq	24(%rax), %rdx
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L283:
	movl	$-1, %eax
	jmp	.L275
	.cfi_endproc
.LFE1679:
	.size	SSL_get0_dane_authority, .-SSL_get0_dane_authority
	.p2align 4
	.globl	SSL_get0_dane_tlsa
	.type	SSL_get0_dane_tlsa, @function
SSL_get0_dane_tlsa:
.LFB1680:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	224(%rdi), %rdi
	movq	%rcx, -56(%rbp)
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jle	.L303
	cmpq	$0, 1456(%rbx)
	jne	.L303
	movq	240(%rbx), %rax
	testq	%rax, %rax
	je	.L296
	testq	%r14, %r14
	movq	-56(%rbp), %rcx
	je	.L297
	movzbl	(%rax), %eax
	movb	%al, (%r14)
.L297:
	testq	%r15, %r15
	je	.L298
	movq	240(%rbx), %rax
	movzbl	1(%rax), %eax
	movb	%al, (%r15)
.L298:
	testq	%rcx, %rcx
	je	.L299
	movq	240(%rbx), %rax
	movzbl	2(%rax), %eax
	movb	%al, (%rcx)
.L299:
	testq	%r13, %r13
	je	.L300
	movq	240(%rbx), %rax
	movq	8(%rax), %rax
	movq	%rax, 0(%r13)
.L300:
	testq	%r12, %r12
	je	.L296
	movq	240(%rbx), %rax
	movq	16(%rax), %rax
	movq	%rax, (%r12)
.L296:
	movl	260(%rbx), %eax
.L293:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L303:
	.cfi_restore_state
	movl	$-1, %eax
	jmp	.L293
	.cfi_endproc
.LFE1680:
	.size	SSL_get0_dane_tlsa, .-SSL_get0_dane_tlsa
	.p2align 4
	.globl	SSL_get0_dane
	.type	SSL_get0_dane, @function
SSL_get0_dane:
.LFB1681:
	.cfi_startproc
	endbr64
	leaq	216(%rdi), %rax
	ret
	.cfi_endproc
.LFE1681:
	.size	SSL_get0_dane, .-SSL_get0_dane
	.p2align 4
	.globl	SSL_dane_tlsa_add
	.type	SSL_dane_tlsa_add, @function
SSL_dane_tlsa_add:
.LFB1682:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movb	%sil, -82(%rbp)
	movb	%dl, -81(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 224(%rdi)
	je	.L369
	movq	%r9, %rbx
	testl	%r9d, %r9d
	js	.L324
	movslq	%r9d, %r9
	cmpq	%r9, %rbx
	je	.L325
.L324:
	movl	$307, %r8d
	movl	$189, %edx
	movl	$394, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L321:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L370
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L325:
	.cfi_restore_state
	movl	%esi, %r12d
	cmpb	$3, %sil
	ja	.L371
	movl	%edx, %r10d
	cmpb	$1, %dl
	ja	.L372
	movq	%rdi, %r15
	movl	%ecx, %r13d
	testb	%cl, %cl
	jne	.L373
.L328:
	testq	%r8, %r8
	je	.L374
	movl	$338, %edx
	leaq	.LC0(%rip), %rsi
	movl	$32, %edi
	movb	%cl, -104(%rbp)
	movq	%r9, -120(%rbp)
	movq	%r8, -112(%rbp)
	movl	%r10d, -96(%rbp)
	call	CRYPTO_zalloc@PLT
	movl	-96(%rbp), %r10d
	movzbl	-104(%rbp), %ecx
	testq	%rax, %rax
	movq	-112(%rbp), %r8
	movq	-120(%rbp), %r9
	movq	%rax, %r14
	je	.L375
	movb	%r10b, 1(%rax)
	movq	%rbx, %rdi
	movl	$346, %edx
	leaq	.LC0(%rip), %rsi
	movb	%r12b, (%rax)
	movb	%r13b, 2(%rax)
	movq	%r9, -120(%rbp)
	movq	%r8, -112(%rbp)
	movb	%cl, -104(%rbp)
	movl	%r10d, -96(%rbp)
	call	CRYPTO_malloc@PLT
	movl	-96(%rbp), %r10d
	movzbl	-104(%rbp), %ecx
	testq	%rax, %rax
	movq	%rax, 8(%r14)
	movq	-112(%rbp), %r8
	movq	%rax, %rdi
	movq	-120(%rbp), %r9
	je	.L376
	movzbl	%r12b, %eax
	movq	%rbx, %rdx
	movq	%r8, %rsi
	movb	%cl, -96(%rbp)
	movq	%r9, -120(%rbp)
	movl	%r10d, -112(%rbp)
	movl	%eax, -88(%rbp)
	movq	%r8, -104(%rbp)
	call	memcpy@PLT
	testb	%r13b, %r13b
	movq	%rbx, 16(%r14)
	movzbl	-96(%rbp), %ecx
	jne	.L334
	movl	-112(%rbp), %r10d
	movq	-104(%rbp), %r8
	leaq	-80(%rbp), %rsi
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movq	-120(%rbp), %r9
	cmpb	$1, %r10b
	movq	%r8, -80(%rbp)
	movq	%r8, -96(%rbp)
	movb	%cl, -104(%rbp)
	je	.L335
	leaq	-72(%rbp), %rdi
	movq	%r9, %rdx
	call	d2i_X509@PLT
	testq	%rax, %rax
	je	.L336
	movq	-80(%rbp), %rax
	movq	-96(%rbp), %r8
	cmpq	%rax, %r8
	ja	.L336
	subq	%r8, %rax
	movzbl	-104(%rbp), %ecx
	cmpq	%rbx, %rax
	je	.L337
.L336:
	movq	8(%r14), %rdi
	movl	$186, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	24(%r14), %rdi
	call	EVP_PKEY_free@PLT
	movl	$188, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	movl	$366, %r8d
.L368:
	movl	$180, %edx
	movl	$394, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L373:
	movq	216(%rdi), %rdx
	cmpb	16(%rdx), %cl
	ja	.L329
	movq	(%rdx), %rdx
	movzbl	%cl, %eax
	movq	(%rdx,%rax,8), %rdi
	testq	%rdi, %rdi
	je	.L329
	movq	%r9, -120(%rbp)
	movq	%r8, -112(%rbp)
	movb	%cl, -104(%rbp)
	movl	%r10d, -96(%rbp)
	call	EVP_MD_size@PLT
	movl	-96(%rbp), %r10d
	movzbl	-104(%rbp), %ecx
	cltq
	movq	-112(%rbp), %r8
	movq	-120(%rbp), %r9
	cmpq	%rax, %rbx
	je	.L328
	movl	$330, %r8d
	movl	$192, %edx
	movl	$394, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L369:
	movl	$302, %r8d
	movl	$175, %edx
	movl	$394, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L372:
	movl	$317, %r8d
	movl	$202, %edx
	movl	$394, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L371:
	movl	$312, %r8d
	movl	$184, %edx
	movl	$394, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L335:
	leaq	-64(%rbp), %rdi
	movq	%r9, %rdx
	call	d2i_PUBKEY@PLT
	testq	%rax, %rax
	je	.L344
	movq	-80(%rbp), %rax
	movq	-96(%rbp), %r8
	cmpq	%rax, %r8
	ja	.L344
	subq	%r8, %rax
	movzbl	-104(%rbp), %ecx
	cmpq	%rbx, %rax
	jne	.L344
	movq	-64(%rbp), %rdi
	cmpb	$2, %r12b
	jne	.L346
	movq	%rdi, 24(%r14)
	.p2align 4,,10
	.p2align 3
.L334:
	movq	224(%r15), %rdi
	movb	%cl, -96(%rbp)
	xorl	%r13d, %r13d
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jle	.L347
	movzbl	-96(%rbp), %r12d
	movzbl	-82(%rbp), %ebx
	movq	%r14, -96(%rbp)
	movq	%r12, %r14
	movl	%eax, %r12d
.L349:
	movq	224(%r15), %rdi
	movl	%r13d, %esi
	call	OPENSSL_sk_value@PLT
	cmpb	(%rax), %bl
	jb	.L348
	ja	.L365
	movzbl	-81(%rbp), %esi
	cmpb	1(%rax), %sil
	jb	.L348
	ja	.L365
	movq	216(%r15), %rsi
	movzbl	2(%rax), %eax
	movq	8(%rsi), %rsi
	movzbl	(%rsi,%r14), %edx
	cmpb	%dl, (%rsi,%rax)
	ja	.L348
.L365:
	movq	-96(%rbp), %r14
.L347:
	movq	224(%r15), %rdi
	movl	%r13d, %edx
	movq	%r14, %rsi
	call	OPENSSL_sk_insert@PLT
	testl	%eax, %eax
	je	.L377
	movzbl	-88(%rbp), %ecx
	movl	$1, %eax
	sall	%cl, %eax
	orl	%eax, 256(%r15)
	movl	$1, %eax
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L348:
	addl	$1, %r13d
	cmpl	%r13d, %r12d
	jne	.L349
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L329:
	movl	$324, %r8d
	movl	$200, %edx
	movl	$394, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L374:
	movl	$334, %r8d
	movl	$203, %edx
	movl	$394, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L375:
	movl	$339, %r8d
.L367:
	movl	$65, %edx
	movl	$394, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L376:
	movl	$186, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	24(%r14), %rdi
	call	EVP_PKEY_free@PLT
	movl	$188, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	movl	$349, %r8d
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L344:
	movq	8(%r14), %rdi
	movl	$186, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	24(%r14), %rdi
	call	EVP_PKEY_free@PLT
	movl	$188, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	movl	$401, %r8d
	movl	$201, %edx
	leaq	.LC0(%rip), %rcx
	movl	$394, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L321
.L377:
	movq	8(%r14), %rdi
	movl	$186, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	24(%r14), %rdi
	call	EVP_PKEY_free@PLT
	movl	$188, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	movl	$451, %r8d
	jmp	.L367
.L337:
	movq	-72(%rbp), %rdi
	movb	%cl, -96(%rbp)
	call	X509_get0_pubkey@PLT
	movzbl	-96(%rbp), %ecx
	testq	%rax, %rax
	je	.L378
	movl	-88(%rbp), %ebx
	movl	$5, %eax
	btl	%ebx, %eax
	jnc	.L379
	movq	232(%r15), %rdi
	testq	%rdi, %rdi
	je	.L380
.L342:
	movq	-72(%rbp), %rsi
	movb	%cl, -96(%rbp)
	call	OPENSSL_sk_push@PLT
	movzbl	-96(%rbp), %ecx
	testl	%eax, %eax
	jne	.L334
.L343:
	movl	$390, %r8d
	movl	$65, %edx
	movl	$394, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-72(%rbp), %rdi
	call	X509_free@PLT
	movq	8(%r14), %rdi
	movl	$186, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	24(%r14), %rdi
	call	EVP_PKEY_free@PLT
	movl	$188, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	movl	$-1, %eax
	jmp	.L321
.L346:
	movb	%cl, -96(%rbp)
	call	EVP_PKEY_free@PLT
	movzbl	-96(%rbp), %ecx
	jmp	.L334
.L379:
	movq	-72(%rbp), %rdi
	movb	%cl, -96(%rbp)
	call	X509_free@PLT
	movzbl	-96(%rbp), %ecx
	jmp	.L334
.L380:
	movb	%cl, -96(%rbp)
	call	OPENSSL_sk_new_null@PLT
	movzbl	-96(%rbp), %ecx
	testq	%rax, %rax
	movq	%rax, 232(%r15)
	movq	%rax, %rdi
	jne	.L342
	jmp	.L343
.L378:
	movq	%r14, %rdi
	call	tlsa_free.part.0
	movl	$371, %r8d
	jmp	.L368
.L370:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1682:
	.size	SSL_dane_tlsa_add, .-SSL_dane_tlsa_add
	.p2align 4
	.globl	SSL_CTX_dane_mtype_set
	.type	SSL_CTX_dane_mtype_set, @function
SSL_CTX_dane_mtype_set:
.LFB1683:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movzbl	%dl, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	testb	%dl, %dl
	jne	.L382
	testq	%rsi, %rsi
	jne	.L407
.L382:
	movq	872(%rbx), %rdi
	cmpb	888(%rbx), %r12b
	ja	.L384
	movq	880(%rbx), %rcx
.L385:
	movl	$0, %eax
	testq	%r14, %r14
	movq	%r14, (%rdi,%r15,8)
	cmove	%eax, %r13d
	movl	$1, %eax
	movb	%r13b, (%rcx,%r15)
.L381:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L407:
	.cfi_restore_state
	movl	$244, %r8d
	movl	$173, %edx
	movl	$393, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L384:
	.cfi_restore_state
	movzbl	%r12b, %eax
	movl	$253, %ecx
	leaq	.LC0(%rip), %rdx
	leal	1(%rax), %r8d
	movl	%eax, -64(%rbp)
	movslq	%r8d, %r8
	leaq	0(,%r8,8), %rsi
	movq	%r8, -56(%rbp)
	call	CRYPTO_realloc@PLT
	movq	-56(%rbp), %r8
	testq	%rax, %rax
	je	.L408
	movq	%rax, 872(%rbx)
	movl	$260, %ecx
	movq	%r8, %rsi
	movq	880(%rbx), %rdi
	leaq	.LC0(%rip), %rdx
	movq	%rax, -56(%rbp)
	call	CRYPTO_realloc@PLT
	movq	-56(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rcx
	je	.L409
	movq	%rax, 880(%rbx)
	movzbl	888(%rbx), %eax
	movl	-64(%rbp), %esi
	leal	1(%rax), %r8d
	movq	%rax, %rdx
	cmpl	%r8d, %esi
	jle	.L388
	subl	%eax, %esi
	movslq	%r8d, %r8
	leaq	1(%rcx,%rdx), %r10
	movl	%esi, %eax
	leaq	1(%r8), %rsi
	leaq	(%r9,%r8,8), %rdi
	subl	$2, %eax
	leaq	(%rsi,%rax), %r11
	leaq	(%r9,%r11,8), %r11
	cmpq	%r11, %r10
	jnb	.L393
	leaq	2(%rdx,%rax), %rdx
	addq	%rcx, %rdx
	cmpq	%rdx, %rdi
	jb	.L403
.L393:
	leaq	1(%rax), %r9
	xorl	%esi, %esi
	movq	%rcx, -72(%rbp)
	leaq	0(,%r9,8), %rdx
	movq	%r8, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	memset@PLT
	movq	-64(%rbp), %r8
	movq	-72(%rbp), %rcx
	xorl	%esi, %esi
	movq	-56(%rbp), %r9
	leaq	(%rcx,%r8), %rdi
	movq	%r9, %rdx
	call	memset@PLT
.L391:
	movq	880(%rbx), %rcx
.L388:
	movb	%r12b, 888(%rbx)
	movq	872(%rbx), %rdi
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L409:
	movl	$262, %r8d
.L406:
	movl	$65, %edx
	movl	$393, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L408:
	movl	$255, %r8d
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L403:
	movl	-64(%rbp), %eax
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L410:
	addq	$1, %rsi
.L389:
	movq	$0, (%r9,%r8,8)
	movb	$0, (%rcx,%r8)
	movq	%rsi, %r8
	cmpl	%esi, %eax
	jg	.L410
	jmp	.L391
	.cfi_endproc
.LFE1683:
	.size	SSL_CTX_dane_mtype_set, .-SSL_CTX_dane_mtype_set
	.p2align 4
	.globl	SSL_CTX_set1_param
	.type	SSL_CTX_set1_param, @function
SSL_CTX_set1_param:
.LFB1684:
	.cfi_startproc
	endbr64
	movq	416(%rdi), %rdi
	jmp	X509_VERIFY_PARAM_set1@PLT
	.cfi_endproc
.LFE1684:
	.size	SSL_CTX_set1_param, .-SSL_CTX_set1_param
	.p2align 4
	.globl	SSL_set1_param
	.type	SSL_set1_param, @function
SSL_set1_param:
.LFB1685:
	.cfi_startproc
	endbr64
	movq	208(%rdi), %rdi
	jmp	X509_VERIFY_PARAM_set1@PLT
	.cfi_endproc
.LFE1685:
	.size	SSL_set1_param, .-SSL_set1_param
	.p2align 4
	.globl	SSL_CTX_get0_param
	.type	SSL_CTX_get0_param, @function
SSL_CTX_get0_param:
.LFB1686:
	.cfi_startproc
	endbr64
	movq	416(%rdi), %rax
	ret
	.cfi_endproc
.LFE1686:
	.size	SSL_CTX_get0_param, .-SSL_CTX_get0_param
	.p2align 4
	.globl	SSL_get0_param
	.type	SSL_get0_param, @function
SSL_get0_param:
.LFB1687:
	.cfi_startproc
	endbr64
	movq	208(%rdi), %rax
	ret
	.cfi_endproc
.LFE1687:
	.size	SSL_get0_param, .-SSL_get0_param
	.p2align 4
	.globl	SSL_certs_clear
	.type	SSL_certs_clear, @function
SSL_certs_clear:
.LFB1688:
	.cfi_startproc
	endbr64
	movq	1168(%rdi), %rdi
	jmp	ssl_cert_clear_certs@PLT
	.cfi_endproc
.LFE1688:
	.size	SSL_certs_clear, .-SSL_certs_clear
	.p2align 4
	.globl	SSL_free
	.type	SSL_free, @function
SSL_free:
.LFB1689:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L416
	movl	$-1, %eax
	lock xaddl	%eax, 1488(%rdi)
	subl	$1, %eax
	testl	%eax, %eax
	je	.L422
	jle	.L420
.L416:
	ret
	.p2align 4,,10
	.p2align 3
.L422:
.L420:
	jmp	SSL_free.part.0
	.cfi_endproc
.LFE1689:
	.size	SSL_free, .-SSL_free
	.p2align 4
	.globl	SSL_set0_rbio
	.type	SSL_set0_rbio, @function
SSL_set0_rbio:
.LFB1690:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	16(%rdi), %rdi
	call	BIO_free_all@PLT
	movq	%r12, 16(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1690:
	.size	SSL_set0_rbio, .-SSL_set0_rbio
	.p2align 4
	.globl	SSL_set0_wbio
	.type	SSL_set0_wbio, @function
SSL_set0_wbio:
.LFB1691:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	24(%rdi), %rdi
	cmpq	$0, 32(%rbx)
	je	.L426
	call	BIO_pop@PLT
	movq	%rax, 24(%rbx)
	movq	%rax, %rdi
.L426:
	call	BIO_free_all@PLT
	movq	32(%rbx), %rdi
	movq	%r12, 24(%rbx)
	testq	%rdi, %rdi
	je	.L425
	movq	%r12, %rsi
	call	BIO_push@PLT
	movq	%rax, 24(%rbx)
.L425:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1691:
	.size	SSL_set0_wbio, .-SSL_set0_wbio
	.p2align 4
	.globl	SSL_set_bio
	.type	SSL_set_bio, @function
SSL_set_bio:
.LFB1692:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %r14
	cmpq	%r14, %rsi
	je	.L476
	testq	%rsi, %rsi
	je	.L453
	cmpq	%rdx, %rsi
	jne	.L453
.L451:
	movq	%r12, %rdi
	call	BIO_up_ref@PLT
.L438:
	movq	16(%rbx), %r14
	movq	32(%rbx), %rdi
	cmpq	%r14, %r12
	je	.L477
	testq	%rdi, %rdi
	jne	.L478
.L442:
	movq	24(%rbx), %rax
	cmpq	%rax, %r13
	je	.L445
.L446:
	movq	%r14, %rdi
	call	BIO_free_all@PLT
	cmpq	$0, 32(%rbx)
	movq	%r12, 16(%rbx)
	movq	24(%rbx), %rdi
	je	.L450
	call	BIO_pop@PLT
	movq	%rax, 24(%rbx)
	movq	%rax, %rdi
.L450:
	call	BIO_free_all@PLT
	movq	32(%rbx), %rdi
	movq	%r13, 24(%rbx)
	testq	%rdi, %rdi
	je	.L432
	movq	%r13, %rsi
	call	BIO_push@PLT
	movq	%rax, 24(%rbx)
.L432:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L453:
	.cfi_restore_state
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L442
.L478:
	call	BIO_next@PLT
	cmpq	%rax, %r13
	je	.L479
	movq	16(%rbx), %r14
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L476:
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.L480
	movq	24(%rbx), %rax
.L435:
	cmpq	%rax, %r13
	je	.L432
	testq	%r14, %r14
	je	.L438
	cmpq	%r14, %r13
	jne	.L438
	jmp	.L451
.L447:
	movq	24(%rbx), %rax
	movq	%r15, %r14
	.p2align 4,,10
	.p2align 3
.L445:
	movq	%r14, %r15
.L448:
	cmpq	%r15, %rax
	je	.L446
	movq	%r14, %rdi
	call	BIO_free_all@PLT
	movq	%r12, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L479:
	.cfi_restore_state
	movq	32(%rbx), %rdi
	movq	16(%rbx), %r15
	testq	%rdi, %rdi
	je	.L447
	call	BIO_next@PLT
	movq	16(%rbx), %r14
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L480:
	call	BIO_next@PLT
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L477:
	movq	24(%rbx), %r8
	testq	%rdi, %rdi
	je	.L440
	movq	%r8, %rdi
	call	BIO_pop@PLT
	movq	%rax, 24(%rbx)
	movq	%rax, %r8
.L440:
	movq	%r8, %rdi
	jmp	.L450
	.cfi_endproc
.LFE1692:
	.size	SSL_set_bio, .-SSL_set_bio
	.p2align 4
	.globl	SSL_get_rbio
	.type	SSL_get_rbio, @function
SSL_get_rbio:
.LFB1693:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE1693:
	.size	SSL_get_rbio, .-SSL_get_rbio
	.p2align 4
	.globl	SSL_get_wbio
	.type	SSL_get_wbio, @function
SSL_get_wbio:
.LFB1694:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %r8
	testq	%r8, %r8
	jne	.L484
	movq	24(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L484:
	movq	%r8, %rdi
	jmp	BIO_next@PLT
	.cfi_endproc
.LFE1694:
	.size	SSL_get_wbio, .-SSL_get_wbio
	.p2align 4
	.globl	SSL_get_fd
	.type	SSL_get_fd, @function
SSL_get_fd:
.LFB1695:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$256, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	$-1, -12(%rbp)
	call	BIO_find_type@PLT
	testq	%rax, %rax
	je	.L486
	movq	%rax, %rdi
	leaq	-12(%rbp), %rcx
	xorl	%edx, %edx
	movl	$105, %esi
	call	BIO_ctrl@PLT
.L486:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	movl	-12(%rbp), %eax
	jne	.L492
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L492:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1695:
	.size	SSL_get_fd, .-SSL_get_fd
	.p2align 4
	.globl	SSL_get_rfd
	.type	SSL_get_rfd, @function
SSL_get_rfd:
.LFB1696:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$256, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	$-1, -12(%rbp)
	call	BIO_find_type@PLT
	testq	%rax, %rax
	je	.L494
	movq	%rax, %rdi
	leaq	-12(%rbp), %rcx
	xorl	%edx, %edx
	movl	$105, %esi
	call	BIO_ctrl@PLT
.L494:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	movl	-12(%rbp), %eax
	jne	.L500
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L500:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1696:
	.size	SSL_get_rfd, .-SSL_get_rfd
	.p2align 4
	.globl	SSL_get_wfd
	.type	SSL_get_wfd, @function
SSL_get_wfd:
.LFB1697:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	32(%rdi), %r8
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	$-1, -12(%rbp)
	testq	%r8, %r8
	jne	.L510
	movq	24(%rdi), %rdi
.L503:
	movl	$256, %esi
	call	BIO_find_type@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L504
	leaq	-12(%rbp), %rcx
	xorl	%edx, %edx
	movl	$105, %esi
	call	BIO_ctrl@PLT
.L504:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	movl	-12(%rbp), %eax
	jne	.L511
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L510:
	.cfi_restore_state
	movq	%r8, %rdi
	call	BIO_next@PLT
	movq	%rax, %rdi
	jmp	.L503
.L511:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1697:
	.size	SSL_get_wfd, .-SSL_get_wfd
	.p2align 4
	.globl	SSL_set_fd
	.type	SSL_set_fd, @function
SSL_set_fd:
.LFB1698:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	call	BIO_s_socket@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	testq	%rax, %rax
	je	.L516
.L514:
	endbr64
	movq	%rax, %r12
	movl	%r14d, %ecx
	xorl	%edx, %edx
	movl	$104, %esi
	movq	%rax, %rdi
	call	BIO_int_ctrl@PLT
	movq	%r12, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	SSL_set_bio
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L516:
	.cfi_restore_state
	movl	$1351, %r8d
	movl	$7, %edx
	movl	$192, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1698:
	.size	SSL_set_fd, .-SSL_set_fd
	.p2align 4
	.globl	SSL_set_wfd
	.type	SSL_set_wfd, @function
SSL_set_wfd:
.LFB1699:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %r12
	testq	%r12, %r12
	je	.L521
	movq	%r12, %rdi
	call	BIO_method_type@PLT
	cmpl	$1285, %eax
	je	.L538
.L521:
	call	BIO_s_socket@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L539
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	%r13d, %ecx
	movl	$104, %esi
	call	BIO_int_ctrl@PLT
	cmpq	$0, 32(%rbx)
	movq	24(%rbx), %rdi
	je	.L528
.L527:
	call	BIO_pop@PLT
	movq	%rax, 24(%rbx)
	movq	%rax, %rdi
.L528:
	call	BIO_free_all@PLT
	movq	32(%rbx), %rdi
	movq	%r12, 24(%rbx)
	testq	%rdi, %rdi
	je	.L536
	movq	%r12, %rsi
	call	BIO_push@PLT
	movq	%rax, 24(%rbx)
.L536:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L539:
	.cfi_restore_state
	movl	$1370, %r8d
	movl	$7, %edx
	movl	$196, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L538:
	.cfi_restore_state
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$105, %esi
	movq	%r12, %rdi
	call	BIO_ctrl@PLT
	cmpl	%eax, %r13d
	jne	.L521
	movq	%r12, %rdi
	call	BIO_up_ref@PLT
	cmpq	$0, 32(%rbx)
	movq	24(%rbx), %rdi
	jne	.L527
	jmp	.L528
	.cfi_endproc
.LFE1699:
	.size	SSL_set_wfd, .-SSL_set_wfd
	.p2align 4
	.globl	SSL_set_rfd
	.type	SSL_set_rfd, @function
SSL_set_rfd:
.LFB1700:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.L552
	movq	24(%rbx), %r12
.L542:
	testq	%r12, %r12
	je	.L546
	movq	%r12, %rdi
	call	BIO_method_type@PLT
	cmpl	$1285, %eax
	je	.L553
.L546:
	call	BIO_s_socket@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L554
	movl	%r13d, %ecx
	xorl	%edx, %edx
	movl	$104, %esi
	movq	%rax, %rdi
	call	BIO_int_ctrl@PLT
	movq	16(%rbx), %rdi
	call	BIO_free_all@PLT
	movq	%r12, 16(%rbx)
	movl	$1, %eax
.L540:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L552:
	.cfi_restore_state
	call	BIO_next@PLT
	movq	%rax, %r12
	jmp	.L542
	.p2align 4,,10
	.p2align 3
.L554:
	movl	$1391, %r8d
	movl	$7, %edx
	movl	$194, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L553:
	.cfi_restore_state
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$105, %esi
	movq	%r12, %rdi
	call	BIO_ctrl@PLT
	cmpl	%eax, %r13d
	jne	.L546
	movq	%r12, %rdi
	call	BIO_up_ref@PLT
	movq	16(%rbx), %rdi
	call	BIO_free_all@PLT
	movq	%r12, 16(%rbx)
	movl	$1, %eax
	jmp	.L540
	.cfi_endproc
.LFE1700:
	.size	SSL_set_rfd, .-SSL_set_rfd
	.p2align 4
	.globl	SSL_get_finished
	.type	SSL_get_finished, @function
SSL_get_finished:
.LFB1701:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	xorl	%r12d, %r12d
	subq	$8, %rsp
	movq	168(%r8), %rsi
	testq	%rsi, %rsi
	je	.L555
	movq	408(%rsi), %r12
	cmpq	%rdx, %r12
	cmovbe	%r12, %rdx
	addq	$280, %rsi
	call	memcpy@PLT
.L555:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1701:
	.size	SSL_get_finished, .-SSL_get_finished
	.p2align 4
	.globl	SSL_get_peer_finished
	.type	SSL_get_peer_finished, @function
SSL_get_peer_finished:
.LFB1702:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	xorl	%r12d, %r12d
	subq	$8, %rsp
	movq	168(%r8), %rsi
	testq	%rsi, %rsi
	je	.L561
	movq	544(%rsi), %r12
	cmpq	%rdx, %r12
	cmovbe	%r12, %rdx
	addq	$416, %rsi
	call	memcpy@PLT
.L561:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1702:
	.size	SSL_get_peer_finished, .-SSL_get_peer_finished
	.p2align 4
	.globl	SSL_get_verify_mode
	.type	SSL_get_verify_mode, @function
SSL_get_verify_mode:
.LFB1703:
	.cfi_startproc
	endbr64
	movl	1376(%rdi), %eax
	ret
	.cfi_endproc
.LFE1703:
	.size	SSL_get_verify_mode, .-SSL_get_verify_mode
	.p2align 4
	.globl	SSL_get_verify_depth
	.type	SSL_get_verify_depth, @function
SSL_get_verify_depth:
.LFB1704:
	.cfi_startproc
	endbr64
	movq	208(%rdi), %rdi
	jmp	X509_VERIFY_PARAM_get_depth@PLT
	.cfi_endproc
.LFE1704:
	.size	SSL_get_verify_depth, .-SSL_get_verify_depth
	.p2align 4
	.globl	SSL_get_verify_callback
	.type	SSL_get_verify_callback, @function
SSL_get_verify_callback:
.LFB1705:
	.cfi_startproc
	endbr64
	movq	1384(%rdi), %rax
	ret
	.cfi_endproc
.LFE1705:
	.size	SSL_get_verify_callback, .-SSL_get_verify_callback
	.p2align 4
	.globl	SSL_CTX_get_verify_mode
	.type	SSL_CTX_get_verify_mode, @function
SSL_CTX_get_verify_mode:
.LFB1706:
	.cfi_startproc
	endbr64
	movl	352(%rdi), %eax
	ret
	.cfi_endproc
.LFE1706:
	.size	SSL_CTX_get_verify_mode, .-SSL_CTX_get_verify_mode
	.p2align 4
	.globl	SSL_CTX_get_verify_depth
	.type	SSL_CTX_get_verify_depth, @function
SSL_CTX_get_verify_depth:
.LFB1707:
	.cfi_startproc
	endbr64
	movq	416(%rdi), %rdi
	jmp	X509_VERIFY_PARAM_get_depth@PLT
	.cfi_endproc
.LFE1707:
	.size	SSL_CTX_get_verify_depth, .-SSL_CTX_get_verify_depth
	.p2align 4
	.globl	SSL_CTX_get_verify_callback
	.type	SSL_CTX_get_verify_callback, @function
SSL_CTX_get_verify_callback:
.LFB1708:
	.cfi_startproc
	endbr64
	movq	400(%rdi), %rax
	ret
	.cfi_endproc
.LFE1708:
	.size	SSL_CTX_get_verify_callback, .-SSL_CTX_get_verify_callback
	.p2align 4
	.globl	SSL_set_verify
	.type	SSL_set_verify, @function
SSL_set_verify:
.LFB1709:
	.cfi_startproc
	endbr64
	movl	%esi, 1376(%rdi)
	testq	%rdx, %rdx
	je	.L573
	movq	%rdx, 1384(%rdi)
.L573:
	ret
	.cfi_endproc
.LFE1709:
	.size	SSL_set_verify, .-SSL_set_verify
	.p2align 4
	.globl	SSL_set_verify_depth
	.type	SSL_set_verify_depth, @function
SSL_set_verify_depth:
.LFB1710:
	.cfi_startproc
	endbr64
	movq	208(%rdi), %rdi
	jmp	X509_VERIFY_PARAM_set_depth@PLT
	.cfi_endproc
.LFE1710:
	.size	SSL_set_verify_depth, .-SSL_set_verify_depth
	.p2align 4
	.globl	SSL_set_read_ahead
	.type	SSL_set_read_ahead, @function
SSL_set_read_ahead:
.LFB1711:
	.cfi_startproc
	endbr64
	movl	%esi, 2120(%rdi)
	ret
	.cfi_endproc
.LFE1711:
	.size	SSL_set_read_ahead, .-SSL_set_read_ahead
	.p2align 4
	.globl	SSL_get_read_ahead
	.type	SSL_get_read_ahead, @function
SSL_get_read_ahead:
.LFB1712:
	.cfi_startproc
	endbr64
	movl	2120(%rdi), %eax
	ret
	.cfi_endproc
.LFE1712:
	.size	SSL_get_read_ahead, .-SSL_get_read_ahead
	.p2align 4
	.globl	SSL_pending
	.type	SSL_pending, @function
SSL_pending:
.LFB1713:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	8(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	*160(%rax)
	movl	$2147483647, %edx
	popq	%rbp
	.cfi_def_cfa 7, 8
	cmpq	$2147483647, %rax
	cmovnb	%rdx, %rax
	ret
	.cfi_endproc
.LFE1713:
	.size	SSL_pending, .-SSL_pending
	.p2align 4
	.globl	SSL_has_pending
	.type	SSL_has_pending, @function
SSL_has_pending:
.LFB1714:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	2112(%rdi), %r12
	movq	%r12, %rdi
	subq	$8, %rsp
	call	RECORD_LAYER_processed_read_pending@PLT
	testl	%eax, %eax
	je	.L587
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L587:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	RECORD_LAYER_read_pending@PLT
	.cfi_endproc
.LFE1714:
	.size	SSL_has_pending, .-SSL_has_pending
	.p2align 4
	.globl	SSL_get_peer_certificate
	.type	SSL_get_peer_certificate, @function
SSL_get_peer_certificate:
.LFB1715:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	testq	%rdi, %rdi
	je	.L590
	movq	1296(%rdi), %r12
	testq	%r12, %r12
	je	.L588
	movq	440(%r12), %r12
	testq	%r12, %r12
	je	.L588
	movq	%r12, %rdi
	call	X509_up_ref@PLT
.L588:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L590:
	.cfi_restore_state
	xorl	%r12d, %r12d
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1715:
	.size	SSL_get_peer_certificate, .-SSL_get_peer_certificate
	.p2align 4
	.globl	SSL_get_peer_cert_chain
	.type	SSL_get_peer_cert_chain, @function
SSL_get_peer_cert_chain:
.LFB1716:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L600
	movq	1296(%rdi), %rax
	testq	%rax, %rax
	je	.L598
	movq	456(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L600:
	xorl	%eax, %eax
.L598:
	ret
	.cfi_endproc
.LFE1716:
	.size	SSL_get_peer_cert_chain, .-SSL_get_peer_cert_chain
	.p2align 4
	.globl	SSL_copy_session_id
	.type	SSL_copy_session_id, @function
SSL_copy_session_id:
.LFB1717:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	call	SSL_get_session@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	SSL_set_session@PLT
	testl	%eax, %eax
	je	.L627
	movq	8(%rbx), %rax
	cmpq	8(%r12), %rax
	je	.L611
	movq	%rbx, %rdi
	call	*32(%rax)
	movq	8(%r12), %rax
	movq	%rbx, %rdi
	movq	%rax, 8(%rbx)
	call	*16(%rax)
	testl	%eax, %eax
	je	.L627
.L611:
	movq	1168(%r12), %rax
	lock addl	$1, 520(%rax)
	movq	1168(%rbx), %rdi
	call	ssl_cert_free@PLT
	movq	1168(%r12), %rax
	movq	%rax, 1168(%rbx)
	movq	1256(%r12), %rax
	cmpl	$32, %eax
	ja	.L628
	movl	%eax, %eax
	leaq	1264(%rbx), %rcx
	leaq	1264(%r12), %rdx
	movq	%rax, 1256(%rbx)
	cmpl	$8, %eax
	jnb	.L612
	testb	$4, %al
	jne	.L629
	testl	%eax, %eax
	je	.L613
	movzbl	1264(%r12), %esi
	movb	%sil, 1264(%rbx)
	testb	$2, %al
	jne	.L630
.L613:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L628:
	.cfi_restore_state
	movl	$890, %r8d
	movl	$273, %edx
	movl	$218, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L627:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L612:
	.cfi_restore_state
	movq	1264(%r12), %rsi
	addq	$1272, %rbx
	movq	%rsi, -8(%rbx)
	movq	-8(%rdx,%rax), %rsi
	andq	$-8, %rbx
	movq	%rsi, -8(%rcx,%rax)
	subq	%rbx, %rcx
	movq	%rcx, %rsi
	movq	%rdx, %rcx
	addl	%esi, %eax
	subq	%rsi, %rcx
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L613
	andl	$-8, %eax
	xorl	%edx, %edx
.L616:
	movl	%edx, %esi
	addl	$8, %edx
	movq	(%rcx,%rsi), %rdi
	movq	%rdi, (%rbx,%rsi)
	cmpl	%eax, %edx
	jb	.L616
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L629:
	movl	1264(%r12), %esi
	movl	%esi, 1264(%rbx)
	movl	-4(%rdx,%rax), %edx
	movl	%edx, -4(%rcx,%rax)
	jmp	.L613
.L630:
	movzwl	-2(%rdx,%rax), %edx
	movw	%dx, -2(%rcx,%rax)
	jmp	.L613
	.cfi_endproc
.LFE1717:
	.size	SSL_copy_session_id, .-SSL_copy_session_id
	.p2align 4
	.globl	SSL_CTX_check_private_key
	.type	SSL_CTX_check_private_key, @function
SSL_CTX_check_private_key:
.LFB1718:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rdi, %rdi
	je	.L632
	movq	320(%rdi), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L632
	movq	8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L640
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	X509_check_private_key@PLT
	.p2align 4,,10
	.p2align 3
.L632:
	.cfi_restore_state
	movl	$1587, %r8d
	movl	$177, %edx
	movl	$168, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L640:
	.cfi_restore_state
	movl	$1591, %r8d
	movl	$190, %edx
	movl	$168, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1718:
	.size	SSL_CTX_check_private_key, .-SSL_CTX_check_private_key
	.p2align 4
	.globl	SSL_check_private_key
	.type	SSL_check_private_key, @function
SSL_check_private_key:
.LFB1719:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rdi, %rdi
	je	.L647
	movq	1168(%rdi), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L648
	movq	8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L649
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	X509_check_private_key@PLT
	.p2align 4,,10
	.p2align 3
.L649:
	.cfi_restore_state
	movl	$1610, %r8d
	movl	$190, %edx
	movl	$163, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L641:
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L647:
	.cfi_restore_state
	movl	$1602, %r8d
	movl	$67, %edx
	movl	$163, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L641
	.p2align 4,,10
	.p2align 3
.L648:
	movl	$1606, %r8d
	movl	$177, %edx
	movl	$163, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L641
	.cfi_endproc
.LFE1719:
	.size	SSL_check_private_key, .-SSL_check_private_key
	.p2align 4
	.globl	SSL_waiting_for_async
	.type	SSL_waiting_for_async, @function
SSL_waiting_for_async:
.LFB1720:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	$0, 6152(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE1720:
	.size	SSL_waiting_for_async, .-SSL_waiting_for_async
	.p2align 4
	.globl	SSL_get_all_async_fds
	.type	SSL_get_all_async_fds, @function
SSL_get_all_async_fds:
.LFB1721:
	.cfi_startproc
	endbr64
	movq	6160(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L652
	jmp	ASYNC_WAIT_CTX_get_all_fds@PLT
	.p2align 4,,10
	.p2align 3
.L652:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1721:
	.size	SSL_get_all_async_fds, .-SSL_get_all_async_fds
	.p2align 4
	.globl	SSL_get_changed_async_fds
	.type	SSL_get_changed_async_fds, @function
SSL_get_changed_async_fds:
.LFB1722:
	.cfi_startproc
	endbr64
	movq	6160(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L654
	jmp	ASYNC_WAIT_CTX_get_changed_fds@PLT
	.p2align 4,,10
	.p2align 3
.L654:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1722:
	.size	SSL_get_changed_async_fds, .-SSL_get_changed_async_fds
	.p2align 4
	.globl	SSL_accept
	.type	SSL_accept, @function
SSL_accept:
.LFB1723:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 48(%rdi)
	je	.L672
.L656:
	movl	$-1, %esi
	movq	%r12, %rdi
	call	ossl_statem_check_finish_init@PLT
	movq	8(%r12), %rax
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	*96(%rax)
	movq	%r12, %rdi
	call	SSL_in_init@PLT
	testl	%eax, %eax
	je	.L660
.L663:
	testb	$1, 1497(%r12)
	jne	.L673
.L662:
	movq	%r12, %rdi
	call	*48(%r12)
.L655:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L674
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L660:
	.cfi_restore_state
	movq	%r12, %rdi
	call	SSL_in_before@PLT
	testl	%eax, %eax
	jne	.L663
	movl	$1, %eax
	jmp	.L655
	.p2align 4,,10
	.p2align 3
.L673:
	call	ASYNC_get_current_job@PLT
	testq	%rax, %rax
	jne	.L662
	leaq	-64(%rbp), %rsi
	leaq	ssl_do_handshake_intern(%rip), %rdx
	movq	%r12, %rdi
	movq	%r12, -64(%rbp)
	call	ssl_start_async_job
	jmp	.L655
	.p2align 4,,10
	.p2align 3
.L672:
	movl	$1, 56(%rdi)
	movl	$0, 68(%rdi)
	call	ossl_statem_clear@PLT
	movq	8(%r12), %rax
	movq	1088(%r12), %rdi
	movq	40(%rax), %rax
	movq	%rax, 48(%r12)
	testq	%rdi, %rdi
	je	.L657
	call	EVP_CIPHER_CTX_free@PLT
	movq	$0, 1088(%r12)
.L657:
	movq	1136(%r12), %rdi
	testq	%rdi, %rdi
	je	.L658
	call	EVP_CIPHER_CTX_free@PLT
	movq	$0, 1136(%r12)
.L658:
	movq	1112(%r12), %rdi
	call	EVP_MD_CTX_free@PLT
	movq	1160(%r12), %rdi
	movq	$0, 1112(%r12)
	call	EVP_MD_CTX_free@PLT
	cmpq	$0, 48(%r12)
	movq	$0, 1160(%r12)
	jne	.L656
	movl	$3645, %r8d
	movl	$144, %edx
	movl	$180, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L655
.L674:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1723:
	.size	SSL_accept, .-SSL_accept
	.p2align 4
	.globl	SSL_connect
	.type	SSL_connect, @function
SSL_connect:
.LFB1724:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 48(%rdi)
	je	.L692
.L676:
	movl	$-1, %esi
	movq	%r12, %rdi
	call	ossl_statem_check_finish_init@PLT
	movq	8(%r12), %rax
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	*96(%rax)
	movq	%r12, %rdi
	call	SSL_in_init@PLT
	testl	%eax, %eax
	je	.L680
.L683:
	testb	$1, 1497(%r12)
	jne	.L693
.L682:
	movq	%r12, %rdi
	call	*48(%r12)
.L675:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L694
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L680:
	.cfi_restore_state
	movq	%r12, %rdi
	call	SSL_in_before@PLT
	testl	%eax, %eax
	jne	.L683
	movl	$1, %eax
	jmp	.L675
	.p2align 4,,10
	.p2align 3
.L693:
	call	ASYNC_get_current_job@PLT
	testq	%rax, %rax
	jne	.L682
	leaq	-64(%rbp), %rsi
	leaq	ssl_do_handshake_intern(%rip), %rdx
	movq	%r12, %rdi
	movq	%r12, -64(%rbp)
	call	ssl_start_async_job
	jmp	.L675
	.p2align 4,,10
	.p2align 3
.L692:
	movl	$0, 56(%rdi)
	movl	$0, 68(%rdi)
	call	ossl_statem_clear@PLT
	movq	8(%r12), %rax
	movq	1088(%r12), %rdi
	movq	48(%rax), %rax
	movq	%rax, 48(%r12)
	testq	%rdi, %rdi
	je	.L677
	call	EVP_CIPHER_CTX_free@PLT
	movq	$0, 1088(%r12)
.L677:
	movq	1136(%r12), %rdi
	testq	%rdi, %rdi
	je	.L678
	call	EVP_CIPHER_CTX_free@PLT
	movq	$0, 1136(%r12)
.L678:
	movq	1112(%r12), %rdi
	call	EVP_MD_CTX_free@PLT
	movq	1160(%r12), %rdi
	movq	$0, 1112(%r12)
	call	EVP_MD_CTX_free@PLT
	cmpq	$0, 48(%r12)
	movq	$0, 1160(%r12)
	jne	.L676
	movl	$3645, %r8d
	movl	$144, %edx
	movl	$180, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L675
.L694:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1724:
	.size	SSL_connect, .-SSL_connect
	.p2align 4
	.globl	SSL_get_default_timeout
	.type	SSL_get_default_timeout, @function
SSL_get_default_timeout:
.LFB1725:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	jmp	*184(%rax)
	.cfi_endproc
.LFE1725:
	.size	SSL_get_default_timeout, .-SSL_get_default_timeout
	.p2align 4
	.globl	ssl_read_internal
	.type	ssl_read_internal, @function
ssl_read_internal:
.LFB1728:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$64, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 48(%rdi)
	je	.L709
	movl	68(%rdi), %eax
	movq	%rdi, %r12
	andl	$2, %eax
	jne	.L710
	movq	%rdx, %r13
	movl	132(%rdi), %edx
	cmpl	$1, %edx
	je	.L704
	cmpl	$8, %edx
	je	.L704
	movq	%rsi, %r15
	xorl	%esi, %esi
	movq	%rcx, %r14
	call	ossl_statem_check_finish_init@PLT
	testb	$1, 1497(%r12)
	je	.L702
	call	ASYNC_get_current_job@PLT
	testq	%rax, %rax
	je	.L711
.L702:
	movq	8(%r12), %rax
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	*56(%rax)
.L696:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L712
	addq	$64, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L704:
	.cfi_restore_state
	movl	$1738, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$66, %edx
	movl	%eax, -84(%rbp)
	movl	$523, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	movl	-84(%rbp), %eax
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L710:
	movl	$1, 40(%rdi)
	xorl	%eax, %eax
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L711:
	movq	8(%r12), %rax
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, -64(%rbp)
	movq	%r15, %xmm1
	movq	%r12, %xmm0
	leaq	ssl_io_intern(%rip), %rdx
	movl	$0, -56(%rbp)
	movq	56(%rax), %rax
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -48(%rbp)
	call	ssl_start_async_job
	movq	6168(%r12), %rdx
	movq	%rdx, (%r14)
	jmp	.L696
.L709:
	movl	$1727, %r8d
	movl	$276, %edx
	movl	$523, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L696
.L712:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1728:
	.size	ssl_read_internal, .-ssl_read_internal
	.p2align 4
	.globl	SSL_read
	.type	SSL_read, @function
SSL_read:
.LFB1729:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$88, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	js	.L728
	cmpq	$0, 48(%rdi)
	movq	%rdi, %r12
	je	.L729
	movl	68(%rdi), %eax
	andl	$2, %eax
	jne	.L730
	movl	132(%rdi), %ecx
	cmpl	$1, %ecx
	je	.L723
	cmpl	$8, %ecx
	je	.L723
	movq	%rsi, %r14
	xorl	%esi, %esi
	movslq	%edx, %r13
	call	ossl_statem_check_finish_init@PLT
	testb	$1, 1497(%r12)
	je	.L720
	call	ASYNC_get_current_job@PLT
	testq	%rax, %rax
	je	.L731
.L720:
	movq	8(%r12), %rax
	leaq	-88(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*56(%rax)
.L721:
	testl	%eax, %eax
	cmovg	-88(%rbp), %eax
.L713:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L732
	addq	$88, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L723:
	.cfi_restore_state
	movl	$1738, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$66, %edx
	movl	%eax, -100(%rbp)
	movl	$523, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	movl	-100(%rbp), %eax
	jmp	.L713
	.p2align 4,,10
	.p2align 3
.L730:
	movl	$1, 40(%rdi)
	xorl	%eax, %eax
	jmp	.L713
	.p2align 4,,10
	.p2align 3
.L731:
	movq	8(%r12), %rax
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, -64(%rbp)
	movq	%r14, %xmm1
	movq	%r12, %xmm0
	leaq	ssl_io_intern(%rip), %rdx
	movl	$0, -56(%rbp)
	movq	56(%rax), %rax
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -48(%rbp)
	call	ssl_start_async_job
	movq	6168(%r12), %rdx
	movq	%rdx, -88(%rbp)
	jmp	.L721
.L728:
	movl	$1771, %r8d
	movl	$271, %edx
	movl	$223, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L713
.L729:
	movl	$1727, %r8d
	movl	$276, %edx
	movl	$523, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L713
.L732:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1729:
	.size	SSL_read, .-SSL_read
	.p2align 4
	.globl	SSL_read_ex
	.type	SSL_read_ex, @function
SSL_read_ex:
.LFB1730:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 48(%rdi)
	je	.L746
	movl	68(%rdi), %r13d
	movq	%rdi, %r12
	andl	$2, %r13d
	jne	.L747
	movl	132(%rdi), %eax
	cmpl	$1, %eax
	je	.L741
	cmpl	$8, %eax
	je	.L741
	movq	%rsi, %rbx
	xorl	%esi, %esi
	movq	%rdx, %r14
	movq	%rcx, %r15
	call	ossl_statem_check_finish_init@PLT
	testb	$1, 1497(%r12)
	je	.L739
	call	ASYNC_get_current_job@PLT
	testq	%rax, %rax
	je	.L748
.L739:
	movq	8(%r12), %rax
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	$0, %r13d
	call	*56(%rax)
	testl	%eax, %eax
	cmovns	%eax, %r13d
.L733:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L749
	addq	$56, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L741:
	.cfi_restore_state
	movl	$1738, %r8d
	movl	$66, %edx
	movl	$523, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L733
	.p2align 4,,10
	.p2align 3
.L747:
	movl	$1, 40(%rdi)
	xorl	%r13d, %r13d
	jmp	.L733
	.p2align 4,,10
	.p2align 3
.L748:
	movq	8(%r12), %rax
	leaq	-96(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r14, -80(%rbp)
	movq	%rbx, %xmm1
	movq	%r12, %xmm0
	leaq	ssl_io_intern(%rip), %rdx
	movl	$0, -72(%rbp)
	movq	56(%rax), %rax
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -64(%rbp)
	call	ssl_start_async_job
	movq	6168(%r12), %rdx
	testl	%eax, %eax
	movq	%rdx, (%r15)
	cmovns	%eax, %r13d
	jmp	.L733
.L746:
	movl	$1727, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$276, %edx
	xorl	%r13d, %r13d
	movl	$523, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L733
.L749:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1730:
	.size	SSL_read_ex, .-SSL_read_ex
	.p2align 4
	.globl	SSL_get_early_data_status
	.type	SSL_get_early_data_status, @function
SSL_get_early_data_status:
.LFB1732:
	.cfi_startproc
	endbr64
	movl	1816(%rdi), %eax
	ret
	.cfi_endproc
.LFE1732:
	.size	SSL_get_early_data_status, .-SSL_get_early_data_status
	.p2align 4
	.globl	SSL_peek
	.type	SSL_peek, @function
SSL_peek:
.LFB1734:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$72, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	js	.L763
	cmpq	$0, 48(%rdi)
	movq	%rdi, %r12
	je	.L764
	xorl	%eax, %eax
	testb	$2, 68(%rdi)
	jne	.L751
	movq	%rsi, %r14
	movslq	%edx, %r13
	testb	$1, 1497(%rdi)
	jne	.L765
.L755:
	movq	8(%r12), %rax
	leaq	-88(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*64(%rax)
.L756:
	testl	%eax, %eax
	cmovg	-88(%rbp), %eax
.L751:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L766
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L765:
	.cfi_restore_state
	call	ASYNC_get_current_job@PLT
	testq	%rax, %rax
	jne	.L755
	movq	8(%r12), %rax
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, -64(%rbp)
	movq	%r14, %xmm1
	movq	%r12, %xmm0
	leaq	ssl_io_intern(%rip), %rdx
	movl	$0, -56(%rbp)
	movq	64(%rax), %rax
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -48(%rbp)
	call	ssl_start_async_job
	movq	6168(%r12), %rdx
	movq	%rdx, -88(%rbp)
	jmp	.L756
.L763:
	movl	$1890, %r8d
	movl	$271, %edx
	movl	$270, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L751
.L764:
	movl	$1859, %r8d
	movl	$276, %edx
	movl	$522, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L751
.L766:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1734:
	.size	SSL_peek, .-SSL_peek
	.p2align 4
	.globl	SSL_peek_ex
	.type	SSL_peek_ex, @function
SSL_peek_ex:
.LFB1735:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$64, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 48(%rdi)
	je	.L777
	movq	%rdi, %r12
	xorl	%r13d, %r13d
	testb	$2, 68(%rdi)
	jne	.L767
	movq	%rsi, %r14
	movq	%rdx, %r15
	testb	$1, 1497(%rdi)
	jne	.L778
.L770:
	movq	8(%r12), %rax
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$0, %r13d
	call	*64(%rax)
	testl	%eax, %eax
	cmovns	%eax, %r13d
.L767:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L779
	addq	$64, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L778:
	.cfi_restore_state
	movq	%rcx, -88(%rbp)
	call	ASYNC_get_current_job@PLT
	movq	-88(%rbp), %rcx
	testq	%rax, %rax
	jne	.L770
	movq	8(%r12), %rax
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r15, -64(%rbp)
	movq	%r14, %xmm1
	movq	%r12, %xmm0
	leaq	ssl_io_intern(%rip), %rdx
	movl	$0, -56(%rbp)
	movq	64(%rax), %rax
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -48(%rbp)
	call	ssl_start_async_job
	movq	6168(%r12), %rdx
	movq	-88(%rbp), %rcx
	testl	%eax, %eax
	movq	%rdx, (%rcx)
	cmovns	%eax, %r13d
	jmp	.L767
.L777:
	movl	$1859, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$276, %edx
	xorl	%r13d, %r13d
	movl	$522, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L767
.L779:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1735:
	.size	SSL_peek_ex, .-SSL_peek_ex
	.p2align 4
	.globl	ssl_write_internal
	.type	ssl_write_internal, @function
ssl_write_internal:
.LFB1736:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 48(%rdi)
	je	.L793
	movl	68(%rdi), %r13d
	movq	%rdi, %r12
	andl	$1, %r13d
	jne	.L794
	movl	132(%rdi), %eax
	movq	%rdx, %r14
	movl	%eax, %edx
	andl	$-3, %edx
	cmpl	$8, %edx
	je	.L788
	cmpl	$1, %eax
	je	.L788
	movq	%rsi, %rbx
	movl	$1, %esi
	movq	%rcx, %r15
	call	ossl_statem_check_finish_init@PLT
	testb	$1, 1497(%r12)
	jne	.L795
.L786:
	movq	8(%r12), %rax
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	*72(%rax)
	movl	%eax, %r13d
.L780:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L796
	addq	$56, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L795:
	.cfi_restore_state
	call	ASYNC_get_current_job@PLT
	testq	%rax, %rax
	jne	.L786
	movq	8(%r12), %rax
	leaq	-96(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r14, -80(%rbp)
	movq	%rbx, %xmm1
	movq	%r12, %xmm0
	leaq	ssl_io_intern(%rip), %rdx
	movl	$1, -72(%rbp)
	movq	72(%rax), %rax
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -64(%rbp)
	call	ssl_start_async_job
	movl	%eax, %r13d
	movq	6168(%r12), %rax
	movq	%rax, (%r15)
	jmp	.L780
	.p2align 4,,10
	.p2align 3
.L788:
	movl	$1932, %r8d
	movl	$66, %edx
	movl	$524, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L780
.L794:
	movl	$1925, %r8d
	movl	$207, %edx
	movl	$524, %esi
	movl	$1, 40(%rdi)
	leaq	.LC0(%rip), %rcx
	movl	$20, %edi
	movl	$-1, %r13d
	call	ERR_put_error@PLT
	jmp	.L780
.L793:
	movl	$1919, %r8d
	movl	$276, %edx
	movl	$524, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r13d
	call	ERR_put_error@PLT
	jmp	.L780
.L796:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1736:
	.size	ssl_write_internal, .-ssl_write_internal
	.p2align 4
	.globl	SSL_write
	.type	SSL_write, @function
SSL_write:
.LFB1737:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$72, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	js	.L812
	cmpq	$0, 48(%rdi)
	movq	%rdi, %r13
	je	.L813
	movl	68(%rdi), %r12d
	andl	$1, %r12d
	jne	.L814
	movl	132(%rdi), %eax
	movl	%eax, %ecx
	andl	$-3, %ecx
	cmpl	$8, %ecx
	je	.L807
	cmpl	$1, %eax
	je	.L807
	movq	%rsi, %r14
	movl	$1, %esi
	movslq	%edx, %r12
	call	ossl_statem_check_finish_init@PLT
	testb	$1, 1497(%r13)
	jne	.L815
.L804:
	movq	8(%r13), %rax
	movq	%r12, %rdx
	leaq	-88(%rbp), %rcx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	*72(%rax)
	movl	%eax, %r12d
.L805:
	testl	%r12d, %r12d
	movl	%r12d, %eax
	cmovg	-88(%rbp), %eax
	movl	%eax, %r12d
.L797:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L816
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L815:
	.cfi_restore_state
	call	ASYNC_get_current_job@PLT
	testq	%rax, %rax
	jne	.L804
	movq	8(%r13), %rax
	movq	%r14, %xmm1
	leaq	-80(%rbp), %rsi
	movq	%r13, %rdi
	movq	%r13, %xmm0
	leaq	ssl_io_intern(%rip), %rdx
	movq	%r12, -64(%rbp)
	movq	72(%rax), %rax
	punpcklqdq	%xmm1, %xmm0
	movl	$1, -56(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -48(%rbp)
	call	ssl_start_async_job
	movl	%eax, %r12d
	movq	6168(%r13), %rax
	movq	%rax, -88(%rbp)
	jmp	.L805
	.p2align 4,,10
	.p2align 3
.L807:
	movl	$1932, %r8d
	movl	$66, %edx
	movl	$524, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L797
.L812:
	movl	$1962, %r8d
	movl	$271, %edx
	movl	$208, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L797
.L814:
	movl	$1925, %r8d
	movl	$207, %edx
	movl	$524, %esi
	movl	$1, 40(%rdi)
	leaq	.LC0(%rip), %rcx
	movl	$20, %edi
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L797
.L813:
	movl	$1919, %r8d
	movl	$276, %edx
	movl	$524, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L797
.L816:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1737:
	.size	SSL_write, .-SSL_write
	.p2align 4
	.globl	SSL_write_ex
	.type	SSL_write_ex, @function
SSL_write_ex:
.LFB1738:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 48(%rdi)
	je	.L830
	movl	68(%rdi), %r13d
	movq	%rdi, %r12
	andl	$1, %r13d
	jne	.L831
	movl	132(%rdi), %eax
	movq	%rdx, %r14
	movl	%eax, %edx
	andl	$-3, %edx
	cmpl	$8, %edx
	je	.L825
	cmpl	$1, %eax
	je	.L825
	movq	%rsi, %rbx
	movl	$1, %esi
	movq	%rcx, %r15
	call	ossl_statem_check_finish_init@PLT
	testb	$1, 1497(%r12)
	jne	.L832
.L823:
	movq	8(%r12), %rax
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	$0, %r13d
	call	*72(%rax)
	testl	%eax, %eax
	cmovns	%eax, %r13d
.L817:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L833
	addq	$56, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L832:
	.cfi_restore_state
	call	ASYNC_get_current_job@PLT
	testq	%rax, %rax
	jne	.L823
	movq	8(%r12), %rax
	leaq	-96(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r14, -80(%rbp)
	movq	%rbx, %xmm1
	movq	%r12, %xmm0
	leaq	ssl_io_intern(%rip), %rdx
	movl	$1, -72(%rbp)
	movq	72(%rax), %rax
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -64(%rbp)
	call	ssl_start_async_job
	movq	6168(%r12), %rdx
	testl	%eax, %eax
	movq	%rdx, (%r15)
	cmovns	%eax, %r13d
	jmp	.L817
	.p2align 4,,10
	.p2align 3
.L825:
	movl	$1932, %r8d
	movl	$66, %edx
	movl	$524, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L817
.L831:
	movl	$1, 40(%rdi)
	movl	$1925, %r8d
	movl	$207, %edx
	xorl	%r13d, %r13d
	leaq	.LC0(%rip), %rcx
	movl	$524, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L817
.L830:
	movl	$1919, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$276, %edx
	xorl	%r13d, %r13d
	movl	$524, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L817
.L833:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1738:
	.size	SSL_write_ex, .-SSL_write_ex
	.p2align 4
	.globl	SSL_shutdown
	.type	SSL_shutdown, @function
SSL_shutdown:
.LFB1740:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 48(%rdi)
	je	.L844
	movq	%rdi, %r12
	call	SSL_in_init@PLT
	testl	%eax, %eax
	jne	.L837
	testb	$1, 1497(%r12)
	jne	.L845
.L838:
	movq	8(%r12), %rax
	movq	%r12, %rdi
	call	*80(%rax)
.L834:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L846
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L845:
	.cfi_restore_state
	call	ASYNC_get_current_job@PLT
	testq	%rax, %rax
	jne	.L838
	movq	8(%r12), %rax
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r12, -64(%rbp)
	leaq	ssl_io_intern(%rip), %rdx
	movl	$2, -40(%rbp)
	movq	80(%rax), %rax
	movq	%rax, -32(%rbp)
	call	ssl_start_async_job
	jmp	.L834
.L837:
	movl	$2086, %r8d
	movl	$407, %edx
	movl	$224, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L834
.L844:
	movl	$2069, %r8d
	movl	$276, %edx
	movl	$224, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L834
.L846:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1740:
	.size	SSL_shutdown, .-SSL_shutdown
	.p2align 4
	.globl	SSL_key_update
	.type	SSL_key_update, @function
SSL_key_update:
.LFB1741:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	8(%rdi), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L848
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L848
	cmpl	$65536, %eax
	je	.L848
	movl	%esi, %r12d
	cmpl	$1, %esi
	ja	.L855
	movq	%rdi, %rbx
	call	SSL_is_init_finished@PLT
	testl	%eax, %eax
	je	.L856
	movq	%rbx, %rdi
	movl	$1, %esi
	call	ossl_statem_set_in_init@PLT
	movl	%r12d, 1932(%rbx)
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L848:
	.cfi_restore_state
	movl	$2099, %r8d
	movl	$266, %edx
	movl	$515, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L847:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L856:
	.cfi_restore_state
	movl	$2110, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$121, %edx
	movl	%eax, -20(%rbp)
	movl	$515, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	movl	-20(%rbp), %eax
	jmp	.L847
	.p2align 4,,10
	.p2align 3
.L855:
	movl	$2105, %r8d
	movl	$120, %edx
	movl	$515, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1741:
	.size	SSL_key_update, .-SSL_key_update
	.p2align 4
	.globl	SSL_get_key_update_type
	.type	SSL_get_key_update_type, @function
SSL_get_key_update_type:
.LFB1742:
	.cfi_startproc
	endbr64
	movl	1932(%rdi), %eax
	ret
	.cfi_endproc
.LFE1742:
	.size	SSL_get_key_update_type, .-SSL_get_key_update_type
	.p2align 4
	.globl	SSL_renegotiate
	.type	SSL_renegotiate, @function
SSL_renegotiate:
.LFB1743:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	8(%rdi), %rax
	movq	192(%rax), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testb	$8, 96(%rdx)
	jne	.L859
	movl	(%rax), %edx
	cmpl	$771, %edx
	jle	.L859
	cmpl	$65536, %edx
	jne	.L869
.L859:
	testb	$64, 1495(%rdi)
	jne	.L870
	movl	$1, 1928(%rdi)
	movq	88(%rax), %rax
	movl	$1, 60(%rdi)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L869:
	.cfi_restore_state
	movl	$2127, %r8d
	movl	$266, %edx
	movl	$516, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L870:
	.cfi_restore_state
	movl	$2132, %r8d
	movl	$339, %edx
	movl	$516, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1743:
	.size	SSL_renegotiate, .-SSL_renegotiate
	.p2align 4
	.globl	SSL_renegotiate_abbreviated
	.type	SSL_renegotiate_abbreviated, @function
SSL_renegotiate_abbreviated:
.LFB1744:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	8(%rdi), %rax
	movq	192(%rax), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testb	$8, 96(%rdx)
	jne	.L872
	movl	(%rax), %edx
	cmpl	$771, %edx
	jle	.L872
	cmpl	$65536, %edx
	jne	.L882
.L872:
	testb	$64, 1495(%rdi)
	jne	.L883
	movl	$1, 1928(%rdi)
	movq	88(%rax), %rax
	movl	$0, 60(%rdi)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L882:
	.cfi_restore_state
	movl	$2145, %r8d
	movl	$266, %edx
	movl	$546, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L883:
	.cfi_restore_state
	movl	$2150, %r8d
	movl	$339, %edx
	movl	$546, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1744:
	.size	SSL_renegotiate_abbreviated, .-SSL_renegotiate_abbreviated
	.p2align 4
	.globl	SSL_renegotiate_pending
	.type	SSL_renegotiate_pending, @function
SSL_renegotiate_pending:
.LFB1745:
	.cfi_startproc
	endbr64
	movl	1928(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	setne	%al
	ret
	.cfi_endproc
.LFE1745:
	.size	SSL_renegotiate_pending, .-SSL_renegotiate_pending
	.p2align 4
	.globl	SSL_ctrl
	.type	SSL_ctrl, @function
SSL_ctrl:
.LFB1746:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	-16(%rsi), %eax
	movq	%rdx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpl	$115, %eax
	ja	.L886
	leaq	.L888(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L888:
	.long	.L906-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L905-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L904-.L888
	.long	.L903-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L902-.L888
	.long	.L901-.L888
	.long	.L900-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L899-.L888
	.long	.L886-.L888
	.long	.L898-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L897-.L888
	.long	.L896-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L895-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L894-.L888
	.long	.L893-.L888
	.long	.L892-.L888
	.long	.L891-.L888
	.long	.L890-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L886-.L888
	.long	.L889-.L888
	.long	.L887-.L888
	.text
	.p2align 4,,10
	.p2align 3
.L886:
	movq	8(%r12), %rax
	movq	%r12, %rdi
	movq	%r8, %rdx
	movq	128(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
.L906:
	.cfi_restore_state
	movq	%rcx, 192(%rdi)
	movl	$1, %eax
.L885:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L905:
	.cfi_restore_state
	orl	1496(%rdi), %r8d
	movl	%r8d, 1496(%rdi)
	movl	%r8d, %eax
	jmp	.L885
.L904:
	movslq	2120(%rdi), %rax
	jmp	.L885
.L903:
	movslq	2120(%rdi), %rax
	movl	%r8d, 2120(%rdi)
	jmp	.L885
.L902:
	movq	1512(%rdi), %rax
	jmp	.L885
.L901:
	xorl	%eax, %eax
	testq	%r8, %r8
	js	.L885
	movq	1512(%rdi), %rax
	movq	%r8, 1512(%rdi)
	jmp	.L885
.L900:
	leaq	-512(%r8), %rdx
	xorl	%eax, %eax
	cmpq	$15872, %rdx
	ja	.L885
	movq	%r8, 1536(%rdi)
	cmpq	1528(%rdi), %r8
	jb	.L908
	jmp	.L931
.L899:
	movq	168(%rdi), %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L885
	movslq	984(%rdx), %rax
	jmp	.L885
.L898:
	notl	%r8d
	andl	1496(%rdi), %r8d
	movl	%r8d, 1496(%rdi)
	movl	%r8d, %eax
	jmp	.L885
.L897:
	movq	1168(%rdi), %rax
	orl	28(%rax), %r8d
	movl	%r8d, 28(%rax)
	movl	%r8d, %eax
	jmp	.L885
.L896:
	movq	1168(%rdi), %rax
	notl	%r8d
	andl	28(%rax), %r8d
	movl	%r8d, 28(%rax)
	movl	%r8d, %eax
	jmp	.L885
.L895:
	testq	%rcx, %rcx
	je	.L919
	movq	168(%rdi), %rdx
	xorl	%eax, %eax
	movq	680(%rdx), %rsi
	testq	%rsi, %rsi
	je	.L885
	movq	%rsi, (%rcx)
	movslq	688(%rdx), %rax
	jmp	.L885
.L894:
	cmpq	$0, 1296(%rdi)
	je	.L911
	call	SSL_in_init@PLT
	testl	%eax, %eax
	jne	.L911
	movq	%r12, %rdi
	call	ossl_statem_get_in_handshake@PLT
	testl	%eax, %eax
	jne	.L911
	movq	1296(%r12), %rax
	movl	632(%rax), %eax
	andl	$1, %eax
	jmp	.L885
.L893:
	movl	1504(%rdi), %esi
	movl	%r8d, %edi
	call	ssl_check_allowed_versions
	leaq	1500(%r12), %rdx
	movl	%eax, %r9d
	xorl	%eax, %eax
	testl	%r9d, %r9d
	je	.L885
.L932:
	movq	1440(%r12), %rax
	movl	%r8d, %esi
	movq	(%rax), %rax
	movl	(%rax), %edi
	call	ssl_set_version_bound@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L885
.L892:
	movl	1500(%rdi), %edi
	movl	%r8d, %esi
	call	ssl_check_allowed_versions
	movl	%eax, %r9d
	xorl	%eax, %eax
	testl	%r9d, %r9d
	je	.L885
	leaq	1504(%r12), %rdx
	jmp	.L932
.L891:
	cmpq	%r8, 1536(%rdi)
	jb	.L916
	testq	%r8, %r8
	je	.L916
.L908:
	movq	%r8, 1528(%r12)
.L931:
	movl	$1, %eax
	jmp	.L885
.L890:
	leaq	-1(%r8), %rdx
	xorl	%eax, %eax
	cmpq	$31, %rdx
	ja	.L885
	movq	%r8, 1544(%rdi)
	cmpq	$1, %r8
	je	.L931
	movl	$1, 2120(%rdi)
	movl	$1, %eax
	jmp	.L885
.L889:
	movslq	1500(%rdi), %rax
	jmp	.L885
.L887:
	movslq	1504(%rdi), %rax
	jmp	.L885
	.p2align 4,,10
	.p2align 3
.L916:
	xorl	%eax, %eax
	jmp	.L885
.L919:
	movl	$2, %eax
	jmp	.L885
.L911:
	movq	$-1, %rax
	jmp	.L885
	.cfi_endproc
.LFE1746:
	.size	SSL_ctrl, .-SSL_ctrl
	.p2align 4
	.globl	SSL_dane_enable
	.type	SSL_dane_enable, @function
SSL_dane_enable:
.LFB1676:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	1440(%rdi), %rax
	cmpb	$0, 888(%rax)
	je	.L942
	cmpq	$0, 224(%rdi)
	movq	%rdi, %rbx
	jne	.L943
	cmpq	$0, 1600(%rdi)
	movq	%rsi, %r12
	je	.L944
.L937:
	movq	208(%rbx), %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	X509_VERIFY_PARAM_set1_host@PLT
	testl	%eax, %eax
	je	.L945
	movq	1440(%rbx), %rax
	movq	$-1, 260(%rbx)
	addq	$872, %rax
	movq	%rax, 216(%rbx)
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %rdx
	movq	%rax, 224(%rbx)
	movl	$1, %eax
	testq	%rdx, %rdx
	je	.L946
.L933:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L943:
	.cfi_restore_state
	movl	$1011, %r8d
	movl	$172, %edx
	movl	$395, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L942:
	.cfi_restore_state
	movl	$1007, %r8d
	movl	$167, %edx
	movl	$395, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L944:
	.cfi_restore_state
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movl	$55, %esi
	call	SSL_ctrl
	movl	$1022, %r8d
	testq	%rax, %rax
	jne	.L937
.L941:
	movl	$204, %edx
	movl	$395, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	movl	$-1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L945:
	.cfi_restore_state
	movl	$1029, %r8d
	jmp	.L941
	.p2align 4,,10
	.p2align 3
.L946:
	movl	$1039, %r8d
	movl	$65, %edx
	movl	$395, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L933
	.cfi_endproc
.LFE1676:
	.size	SSL_dane_enable, .-SSL_dane_enable
	.p2align 4
	.globl	SSL_new
	.type	SSL_new, @function
SSL_new:
.LFB1657:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testq	%rdi, %rdi
	je	.L1011
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	testq	%r12, %r12
	je	.L1012
	movl	$691, %edx
	leaq	.LC0(%rip), %rsi
	movl	$6280, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L952
	leaq	1488(%rax), %r13
	movl	$1, 1488(%rax)
	mfence
	call	CRYPTO_THREAD_lock_new@PLT
	movq	%rax, 6216(%r12)
	testq	%rax, %rax
	je	.L1013
	leaq	2112(%r12), %rdi
	movq	%r12, %rsi
	call	RECORD_LAYER_init@PLT
	movl	296(%rbx), %eax
	movq	24(%rbx), %rdi
	movl	%eax, 1492(%r12)
	movq	896(%rbx), %rax
	movq	%rax, 272(%r12)
	movl	304(%rbx), %eax
	movl	%eax, 1500(%r12)
	movl	308(%rbx), %eax
	movl	%eax, 1504(%r12)
	movl	300(%rbx), %eax
	movl	%eax, 1496(%r12)
	movq	312(%rbx), %rax
	movq	%rax, 1512(%r12)
	movl	936(%rbx), %eax
	movl	%eax, 6176(%r12)
	movl	940(%rbx), %eax
	movl	%eax, 6180(%r12)
	movq	992(%rbx), %rax
	movq	%rax, 6224(%r12)
	movl	1016(%rbx), %eax
	movl	%eax, 1940(%r12)
	call	OPENSSL_sk_dup@PLT
	movq	%rax, 304(%r12)
	testq	%rax, %rax
	je	.L962
	movq	320(%rbx), %rdi
	call	ssl_cert_dup@PLT
	movq	%rax, 1168(%r12)
	testq	%rax, %rax
	je	.L962
	movl	328(%rbx), %eax
	movl	%eax, 2120(%r12)
	movq	336(%rbx), %rax
	movq	%rax, 184(%r12)
	movq	344(%rbx), %rax
	movq	%rax, 192(%r12)
	movl	352(%rbx), %eax
	movl	%eax, 1376(%r12)
	movq	912(%rbx), %rax
	movq	%rax, 2104(%r12)
	movq	944(%rbx), %rax
	movq	%rax, 6192(%r12)
	movq	952(%rbx), %rax
	movq	%rax, 6200(%r12)
	movq	960(%rbx), %rax
	movq	%rax, 6208(%r12)
	movq	360(%rbx), %rax
	movq	%rax, 1256(%r12)
	cmpq	$32, %rax
	ja	.L962
	movdqu	368(%rbx), %xmm1
	movups	%xmm1, 1264(%r12)
	movdqu	384(%rbx), %xmm2
	movups	%xmm2, 1280(%r12)
	movq	400(%rbx), %rax
	movq	%rax, 1384(%r12)
	movq	408(%rbx), %rax
	movq	%rax, 1328(%r12)
	call	X509_VERIFY_PARAM_new@PLT
	movq	%rax, 208(%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L962
	movq	416(%rbx), %rsi
	call	X509_VERIFY_PARAM_inherit@PLT
	movl	424(%rbx), %eax
	movl	%eax, 64(%r12)
	movzbl	580(%rbx), %eax
	movb	%al, 1844(%r12)
	movdqu	456(%rbx), %xmm3
	movups	%xmm3, 1528(%r12)
	movq	472(%rbx), %rax
	movq	%rax, 1544(%r12)
	cmpq	$1, %rax
	jbe	.L955
	movl	$1, 2120(%r12)
.L955:
	movq	480(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.L1014
.L956:
	leaq	156(%rbx), %rax
	lock addl	$1, (%rax)
	pxor	%xmm0, %xmm0
	movl	576(%rbx), %edx
	movq	%rbx, 1440(%r12)
	movq	$0, 1584(%r12)
	movq	$0, 1592(%r12)
	movl	$0, 1664(%r12)
	movl	%edx, 1608(%r12)
	movl	$0, 1628(%r12)
	movq	$0, 1648(%r12)
	movq	$0, 1656(%r12)
	movups	%xmm0, 1632(%r12)
	lock addl	$1, (%rax)
	movq	592(%rbx), %rdi
	movq	%rbx, 1904(%r12)
	testq	%rdi, %rdi
	je	.L957
	movq	584(%rbx), %rsi
	movl	$780, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_memdup@PLT
	movq	%rax, 1680(%r12)
	testq	%rax, %rax
	je	.L962
	movq	584(%rbx), %rax
	movq	%rax, 1672(%r12)
.L957:
	movq	608(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L958
	movq	600(%rbx), %rax
	movl	$789, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	(%rax,%rax), %rsi
	call	CRYPTO_memdup@PLT
	movq	%rax, 1712(%r12)
	testq	%rax, %rax
	je	.L962
	movq	600(%rbx), %rax
	movq	%rax, 1704(%r12)
.L958:
	movq	1440(%r12), %rax
	movq	$0, 1792(%r12)
	cmpq	$0, 632(%rax)
	je	.L959
	movq	640(%rax), %rdi
	movl	$802, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, 1776(%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L962
	movq	1440(%r12), %rax
	movq	632(%rax), %rsi
	movq	640(%rax), %rdx
	call	memcpy@PLT
	movq	1440(%r12), %rax
	movq	640(%rax), %rax
	movq	%rax, 1784(%r12)
.L959:
	movq	176(%rbx), %rax
	movq	%r12, %rdi
	movq	1000(%rbx), %rdx
	movq	$0, 1448(%r12)
	movq	$0, 1456(%r12)
	movq	%rax, 6136(%r12)
	movq	184(%rbx), %rax
	movq	%rax, 6144(%r12)
	movq	(%rbx), %rax
	movq	%rdx, 6248(%r12)
	movq	1008(%rbx), %rdx
	movq	%rax, 8(%r12)
	movl	$-1, 1932(%r12)
	movq	%rdx, 6256(%r12)
	call	*16(%rax)
	testl	%eax, %eax
	je	.L962
	movq	(%rbx), %rdx
	leaq	ssl_undefined_function(%rip), %rax
	movq	%r12, %rdi
	cmpq	%rax, 40(%rdx)
	setne	%al
	movzbl	%al, %eax
	movl	%eax, 56(%r12)
	call	SSL_clear
	testl	%eax, %eax
	je	.L962
	xorl	%edi, %edi
	leaq	1464(%r12), %rdx
	movq	%r12, %rsi
	call	CRYPTO_new_ex_data@PLT
	testl	%eax, %eax
	je	.L962
	movdqu	728(%rbx), %xmm0
	movdqu	712(%rbx), %xmm4
	movq	$0, 6152(%r12)
	movups	%xmm4, 1408(%r12)
	movups	%xmm0, 1424(%r12)
	movq	448(%rbx), %r14
	movq	440(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L960
	movq	1440(%r12), %rdi
	movl	$18, %esi
	call	SSL_CTX_has_client_custom_ext@PLT
	testl	%eax, %eax
	jne	.L1015
	xorl	%ecx, %ecx
	movl	$1, %edx
	movl	$65, %esi
	movq	%r12, %rdi
	call	SSL_ctrl
	testq	%rax, %rax
	je	.L962
.L960:
	movq	%rbx, 1872(%r12)
	movq	%r14, 1880(%r12)
	jmp	.L947
.L1015:
	movl	$4865, %r8d
	movl	$206, %edx
	movl	$399, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L962:
	movl	$-1, %eax
	lock xaddl	%eax, 0(%r13)
	subl	$1, %eax
	testl	%eax, %eax
	je	.L1016
	jle	.L963
.L952:
	movl	$851, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$186, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
.L947:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1016:
	.cfi_restore_state
.L963:
	movq	%r12, %rdi
	call	SSL_free.part.0
	jmp	.L952
	.p2align 4,,10
	.p2align 3
.L1014:
	movq	%r12, %rdi
	call	SSL_set_default_read_buffer_len@PLT
	jmp	.L956
	.p2align 4,,10
	.p2align 3
.L1012:
	movl	$687, %r8d
	movl	$228, %edx
	movl	$186, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L947
	.p2align 4,,10
	.p2align 3
.L1013:
	movl	$698, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	jmp	.L952
	.p2align 4,,10
	.p2align 3
.L1011:
	movl	$683, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$195, %edx
	xorl	%r12d, %r12d
	movl	$186, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L947
	.cfi_endproc
.LFE1657:
	.size	SSL_new, .-SSL_new
	.p2align 4
	.globl	SSL_callback_ctrl
	.type	SSL_callback_ctrl, @function
SSL_callback_ctrl:
.LFB1747:
	.cfi_startproc
	endbr64
	cmpl	$15, %esi
	je	.L1021
	movq	8(%rdi), %rax
	jmp	*208(%rax)
	.p2align 4,,10
	.p2align 3
.L1021:
	movq	%rdx, 184(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1747:
	.size	SSL_callback_ctrl, .-SSL_callback_ctrl
	.p2align 4
	.globl	SSL_CTX_sessions
	.type	SSL_CTX_sessions, @function
SSL_CTX_sessions:
.LFB1748:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE1748:
	.size	SSL_CTX_sessions, .-SSL_CTX_sessions
	.p2align 4
	.globl	SSL_CTX_ctrl
	.type	SSL_CTX_ctrl, @function
SSL_CTX_ctrl:
.LFB1749:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rdi, %rdi
	je	.L1076
	leal	-16(%rsi), %eax
	movq	%rdi, %r8
	movq	%rdx, %r9
	cmpl	$115, %eax
	ja	.L1027
	leaq	.L1029(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1029:
	.long	.L1060-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1059-.L1029
	.long	.L1058-.L1029
	.long	.L1057-.L1029
	.long	.L1056-.L1029
	.long	.L1055-.L1029
	.long	.L1054-.L1029
	.long	.L1053-.L1029
	.long	.L1052-.L1029
	.long	.L1051-.L1029
	.long	.L1050-.L1029
	.long	.L1049-.L1029
	.long	.L1048-.L1029
	.long	.L1027-.L1029
	.long	.L1047-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1046-.L1029
	.long	.L1045-.L1029
	.long	.L1044-.L1029
	.long	.L1043-.L1029
	.long	.L1042-.L1029
	.long	.L1041-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1040-.L1029
	.long	.L1039-.L1029
	.long	.L1038-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1037-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1036-.L1029
	.long	.L1035-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1034-.L1029
	.long	.L1033-.L1029
	.long	.L1032-.L1029
	.long	.L1031-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1027-.L1029
	.long	.L1030-.L1029
	.long	.L1028-.L1029
	.text
	.p2align 4,,10
	.p2align 3
.L1076:
	movl	%esi, %eax
	andl	$-5, %eax
	cmpl	$98, %eax
	je	.L1025
	xorl	%eax, %eax
	cmpl	$92, %esi
	je	.L1077
.L1023:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1077:
	.cfi_restore_state
	movq	%rcx, %rdx
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	tls1_set_groups_list@PLT
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	cltq
	ret
	.p2align 4,,10
	.p2align 3
.L1025:
	.cfi_restore_state
	xorl	%edx, %edx
	movq	%rcx, %rsi
	xorl	%edi, %edi
	call	tls1_set_sigalgs_list@PLT
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	cltq
	ret
.L1028:
	.cfi_restore_state
	movslq	308(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1030:
	.cfi_restore_state
	movslq	304(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1031:
	.cfi_restore_state
	leaq	-1(%r9), %rdx
	xorl	%eax, %eax
	cmpq	$31, %rdx
	ja	.L1023
	movq	%r9, 472(%rdi)
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1032:
	.cfi_restore_state
	cmpq	%r9, 464(%rdi)
	jb	.L1068
	testq	%r9, %r9
	je	.L1068
	movq	%r9, 456(%rdi)
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1033:
	.cfi_restore_state
	movl	304(%rdi), %edi
	movl	%r9d, %esi
	call	ssl_check_allowed_versions
	movl	%eax, %r10d
	xorl	%eax, %eax
	testl	%r10d, %r10d
	je	.L1023
	leaq	308(%r8), %rdx
.L1075:
	movq	(%r8), %rax
	movl	%r9d, %esi
	movl	(%rax), %edi
	call	ssl_set_version_bound@PLT
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	ret
.L1034:
	.cfi_restore_state
	movl	308(%rdi), %esi
	movl	%r9d, %edi
	call	ssl_check_allowed_versions
	movl	%eax, %r10d
	leaq	304(%r8), %rdx
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jne	.L1075
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1035:
	.cfi_restore_state
	movq	320(%rdi), %rax
	notl	%r9d
	andl	28(%rax), %r9d
	movl	%r9d, 28(%rax)
	movl	%r9d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1036:
	.cfi_restore_state
	movq	320(%rdi), %rax
	orl	28(%rax), %r9d
	movl	%r9d, 28(%rax)
	movl	%r9d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1037:
	.cfi_restore_state
	notl	%r9d
	andl	300(%rdi), %r9d
	movl	%r9d, 300(%rdi)
	movl	%r9d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1038:
	.cfi_restore_state
	leaq	-512(%r9), %rdx
	xorl	%eax, %eax
	cmpq	$15872, %rdx
	ja	.L1023
	movq	%r9, 464(%rdi)
	movl	$1, %eax
	cmpq	456(%rdi), %r9
	jnb	.L1023
	movq	%r9, 456(%rdi)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1039:
	.cfi_restore_state
	xorl	%eax, %eax
	testq	%r9, %r9
	js	.L1023
	movq	312(%rdi), %rax
	movq	%r9, 312(%rdi)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1040:
	.cfi_restore_state
	movq	312(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1041:
	.cfi_restore_state
	movl	72(%rdi), %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1042:
	.cfi_restore_state
	movl	72(%rdi), %eax
	movl	%r9d, 72(%rdi)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1043:
	.cfi_restore_state
	movq	48(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1044:
	.cfi_restore_state
	xorl	%eax, %eax
	testq	%r9, %r9
	js	.L1023
	movq	48(%rdi), %rax
	movq	%r9, 48(%rdi)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1045:
	.cfi_restore_state
	movslq	328(%rdi), %rax
	movl	%r9d, 328(%rdi)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1046:
	.cfi_restore_state
	movslq	328(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1047:
	.cfi_restore_state
	orl	300(%rdi), %r9d
	movl	%r9d, 300(%rdi)
	movl	%r9d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1048:
	.cfi_restore_state
	movslq	144(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1049:
	.cfi_restore_state
	movslq	140(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1050:
	.cfi_restore_state
	movslq	136(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1051:
	.cfi_restore_state
	movslq	152(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1052:
	.cfi_restore_state
	movslq	148(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1053:
	.cfi_restore_state
	movslq	128(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1054:
	.cfi_restore_state
	movslq	132(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1055:
	.cfi_restore_state
	movslq	124(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1056:
	.cfi_restore_state
	movslq	116(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1057:
	.cfi_restore_state
	movslq	120(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1058:
	.cfi_restore_state
	movslq	112(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1059:
	.cfi_restore_state
	movq	40(%rdi), %rdi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	OPENSSL_LH_num_items@PLT
.L1060:
	.cfi_restore_state
	movq	%rcx, 344(%rdi)
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1027:
	.cfi_restore_state
	movq	(%r8), %rax
	movq	%r9, %rdx
	movq	%r8, %rdi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	136(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1068:
	.cfi_restore_state
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1749:
	.size	SSL_CTX_ctrl, .-SSL_CTX_ctrl
	.p2align 4
	.globl	SSL_CTX_callback_ctrl
	.type	SSL_CTX_callback_ctrl, @function
SSL_CTX_callback_ctrl:
.LFB1750:
	.cfi_startproc
	endbr64
	cmpl	$15, %esi
	je	.L1082
	movq	(%rdi), %rax
	jmp	*216(%rax)
	.p2align 4,,10
	.p2align 3
.L1082:
	movq	%rdx, 336(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1750:
	.size	SSL_CTX_callback_ctrl, .-SSL_CTX_callback_ctrl
	.p2align 4
	.globl	ssl_cipher_id_cmp
	.type	ssl_cipher_id_cmp, @function
ssl_cipher_id_cmp:
.LFB1751:
	.cfi_startproc
	endbr64
	movl	24(%rsi), %edx
	movl	$1, %eax
	cmpl	%edx, 24(%rdi)
	ja	.L1083
	sbbl	%eax, %eax
.L1083:
	ret
	.cfi_endproc
.LFE1751:
	.size	ssl_cipher_id_cmp, .-ssl_cipher_id_cmp
	.p2align 4
	.globl	ssl_cipher_ptr_id_cmp
	.type	ssl_cipher_ptr_id_cmp, @function
ssl_cipher_ptr_id_cmp:
.LFB1752:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rdx
	movq	(%rdi), %rcx
	movl	$1, %eax
	movl	24(%rdx), %edx
	cmpl	%edx, 24(%rcx)
	ja	.L1086
	sbbl	%eax, %eax
.L1086:
	ret
	.cfi_endproc
.LFE1752:
	.size	ssl_cipher_ptr_id_cmp, .-ssl_cipher_ptr_id_cmp
	.p2align 4
	.globl	SSL_get_ciphers
	.type	SSL_get_ciphers, @function
SSL_get_ciphers:
.LFB1753:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1091
	movq	288(%rdi), %rax
	testq	%rax, %rax
	je	.L1095
.L1089:
	ret
	.p2align 4,,10
	.p2align 3
.L1095:
	movq	1440(%rdi), %rax
	testq	%rax, %rax
	je	.L1089
	movq	8(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1091:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1753:
	.size	SSL_get_ciphers, .-SSL_get_ciphers
	.p2align 4
	.globl	SSL_get_client_ciphers
	.type	SSL_get_client_ciphers, @function
SSL_get_client_ciphers:
.LFB1754:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1099
	movl	56(%rdi), %eax
	testl	%eax, %eax
	je	.L1099
	movq	280(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1099:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1754:
	.size	SSL_get_client_ciphers, .-SSL_get_client_ciphers
	.p2align 4
	.globl	SSL_get1_supported_ciphers
	.type	SSL_get1_supported_ciphers, @function
SSL_get1_supported_ciphers:
.LFB1755:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdi, %rdi
	je	.L1119
	movq	288(%rdi), %r13
	movq	%rdi, %r14
	testq	%r13, %r13
	je	.L1120
.L1103:
	movq	%r14, %rdi
	call	ssl_set_client_disabled@PLT
	testl	%eax, %eax
	je	.L1119
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	jmp	.L1108
	.p2align 4,,10
	.p2align 3
.L1106:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L1121
.L1105:
	addl	$1, %ebx
.L1108:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L1100
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	xorl	%ecx, %ecx
	movl	$65537, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	%rax, %r12
	call	ssl_cipher_disabled@PLT
	testl	%eax, %eax
	jne	.L1105
	testq	%r15, %r15
	jne	.L1106
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L1106
.L1119:
	xorl	%r15d, %r15d
.L1100:
	addq	$8, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1120:
	.cfi_restore_state
	movq	1440(%rdi), %rax
	testq	%rax, %rax
	je	.L1119
	movq	8(%rax), %r13
	testq	%r13, %r13
	jne	.L1103
	jmp	.L1119
	.p2align 4,,10
	.p2align 3
.L1121:
	movq	%r15, %rdi
	call	OPENSSL_sk_free@PLT
	jmp	.L1119
	.cfi_endproc
.LFE1755:
	.size	SSL_get1_supported_ciphers, .-SSL_get1_supported_ciphers
	.p2align 4
	.globl	ssl_get_ciphers_by_id
	.type	ssl_get_ciphers_by_id, @function
ssl_get_ciphers_by_id:
.LFB1756:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1124
	movq	296(%rdi), %rax
	testq	%rax, %rax
	je	.L1128
.L1122:
	ret
	.p2align 4,,10
	.p2align 3
.L1128:
	movq	1440(%rdi), %rax
	testq	%rax, %rax
	je	.L1122
	movq	16(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1124:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1756:
	.size	ssl_get_ciphers_by_id, .-ssl_get_ciphers_by_id
	.p2align 4
	.globl	SSL_get_cipher_list
	.type	SSL_get_cipher_list, @function
SSL_get_cipher_list:
.LFB1757:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1145
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	288(%rdi), %r13
	movl	%esi, %r12d
	testq	%r13, %r13
	je	.L1146
.L1132:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L1133
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	testq	%rax, %rax
	je	.L1133
	popq	%r12
	movq	8(%rax), %rax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1146:
	.cfi_restore_state
	movq	1440(%rdi), %rax
	testq	%rax, %rax
	je	.L1133
	movq	8(%rax), %r13
	testq	%r13, %r13
	jne	.L1132
.L1133:
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1145:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1757:
	.size	SSL_get_cipher_list, .-SSL_get_cipher_list
	.p2align 4
	.globl	SSL_CTX_get_ciphers
	.type	SSL_CTX_get_ciphers, @function
SSL_CTX_get_ciphers:
.LFB1758:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1147
	movq	8(%rdi), %rax
.L1147:
	ret
	.cfi_endproc
.LFE1758:
	.size	SSL_CTX_get_ciphers, .-SSL_CTX_get_ciphers
	.p2align 4
	.globl	SSL_CTX_set_cipher_list
	.type	SSL_CTX_set_cipher_list, @function
SSL_CTX_set_cipher_list:
.LFB1760:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	leaq	16(%rdi), %rcx
	leaq	8(%rdi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	24(%rdi), %rsi
	movq	320(%rdi), %r9
	movq	(%rdi), %rdi
	call	ssl_create_cipher_list@PLT
	testq	%rax, %rax
	je	.L1151
	movq	%rax, %rbx
	xorl	%r13d, %r13d
	jmp	.L1153
	.p2align 4,,10
	.p2align 3
.L1155:
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	cmpl	$771, 44(%rax)
	setle	%al
	addl	$1, %r13d
	movzbl	%al, %eax
	addl	%eax, %r12d
.L1153:
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jl	.L1155
	testl	%r12d, %r12d
	je	.L1160
	movl	$1, %r12d
.L1151:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1160:
	.cfi_restore_state
	movl	$2558, %r8d
	movl	$185, %edx
	movl	$269, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1760:
	.size	SSL_CTX_set_cipher_list, .-SSL_CTX_set_cipher_list
	.p2align 4
	.globl	SSL_set_cipher_list
	.type	SSL_set_cipher_list, @function
SSL_set_cipher_list:
.LFB1761:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	leaq	296(%rdi), %rcx
	leaq	288(%rdi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	1440(%rdi), %rax
	movq	304(%rdi), %rsi
	movq	1168(%rdi), %r9
	movq	(%rax), %rdi
	call	ssl_create_cipher_list@PLT
	testq	%rax, %rax
	je	.L1161
	movq	%rax, %rbx
	xorl	%r13d, %r13d
	jmp	.L1163
	.p2align 4,,10
	.p2align 3
.L1165:
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	cmpl	$771, 44(%rax)
	setle	%al
	addl	$1, %r13d
	movzbl	%al, %eax
	addl	%eax, %r12d
.L1163:
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jl	.L1165
	testl	%r12d, %r12d
	je	.L1170
	movl	$1, %r12d
.L1161:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1170:
	.cfi_restore_state
	movl	$2576, %r8d
	movl	$185, %edx
	movl	$271, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1761:
	.size	SSL_set_cipher_list, .-SSL_set_cipher_list
	.p2align 4
	.globl	SSL_get_shared_ciphers
	.type	SSL_get_shared_ciphers, @function
SSL_get_shared_ciphers:
.LFB1762:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	56(%rdi), %eax
	movq	%rsi, -64(%rbp)
	testl	%eax, %eax
	je	.L1174
	movq	280(%rdi), %r13
	testq	%r13, %r13
	je	.L1174
	movl	%edx, %r12d
	cmpl	$1, %edx
	jle	.L1174
	movq	288(%rdi), %r15
	testq	%r15, %r15
	je	.L1194
.L1175:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	je	.L1174
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	je	.L1174
	movq	-64(%rbp), %r14
	xorl	%ebx, %ebx
	jmp	.L1176
	.p2align 4,,10
	.p2align 3
.L1180:
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, -56(%rbp)
	call	OPENSSL_sk_find@PLT
	testl	%eax, %eax
	js	.L1177
	movq	-56(%rbp), %rdx
	movq	8(%rdx), %rsi
	movq	%rsi, %rdi
	movq	%rsi, -56(%rbp)
	call	strlen@PLT
	movq	-56(%rbp), %rsi
	cmpl	%eax, %r12d
	jle	.L1195
	movq	%r14, %rdi
	leaq	1(%rax), %rdx
	movq	%rax, -56(%rbp)
	call	memcpy@PLT
	movq	-56(%rbp), %rcx
	movslq	%ecx, %rax
	addl	$1, %ecx
	addq	%r14, %rax
	subl	%ecx, %r12d
	movb	$58, (%rax)
	leaq	1(%rax), %r14
.L1177:
	addl	$1, %ebx
.L1176:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L1180
	movb	$0, -1(%r14)
	movq	-64(%rbp), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1194:
	.cfi_restore_state
	movq	1440(%rdi), %rax
	testq	%rax, %rax
	je	.L1174
	movq	8(%rax), %r15
	testq	%r15, %r15
	jne	.L1175
.L1174:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1195:
	.cfi_restore_state
	movq	-64(%rbp), %rbx
	leaq	-1(%r14), %rax
	cmpq	%rbx, %r14
	cmove	%rbx, %rax
	movb	$0, (%rax)
	addq	$24, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1762:
	.size	SSL_get_shared_ciphers, .-SSL_get_shared_ciphers
	.p2align 4
	.globl	SSL_get_servername
	.type	SSL_get_servername, @function
SSL_get_servername:
.LFB1763:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	$0, 48(%rdi)
	je	.L1220
	movl	56(%rdi), %eax
	testl	%esi, %esi
	jne	.L1209
	testl	%eax, %eax
	je	.L1199
	movl	200(%rdi), %edx
	testl	%edx, %edx
	jne	.L1200
.L1207:
	movq	1600(%rbx), %rax
.L1196:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1200:
	.cfi_restore_state
	movq	8(%rdi), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L1202
	movl	(%rax), %eax
	cmpl	$65536, %eax
	je	.L1202
	cmpl	$771, %eax
	jg	.L1207
.L1202:
	movq	1296(%rbx), %rax
	movq	544(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1220:
	.cfi_restore_state
	testl	%esi, %esi
	jne	.L1209
.L1199:
	movq	%rbx, %rdi
	call	SSL_in_before@PLT
	testl	%eax, %eax
	jne	.L1221
	movq	8(%rbx), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L1206
	movl	(%rax), %eax
	cmpl	$65536, %eax
	je	.L1206
	cmpl	$771, %eax
	jg	.L1207
.L1206:
	movl	200(%rbx), %eax
	testl	%eax, %eax
	je	.L1207
	movq	1296(%rbx), %rax
	movq	544(%rax), %rax
	testq	%rax, %rax
	jne	.L1196
	jmp	.L1207
	.p2align 4,,10
	.p2align 3
.L1221:
	movq	1600(%rbx), %rax
	testq	%rax, %rax
	jne	.L1196
	movq	1296(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L1196
	cmpl	$772, (%rdx)
	je	.L1196
	movq	544(%rdx), %rax
	jmp	.L1196
	.p2align 4,,10
	.p2align 3
.L1209:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1763:
	.size	SSL_get_servername, .-SSL_get_servername
	.p2align 4
	.globl	SSL_get_servername_type
	.type	SSL_get_servername_type, @function
SSL_get_servername_type:
.LFB1764:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	$0, 48(%rdi)
	je	.L1223
	movl	56(%rdi), %esi
	testl	%esi, %esi
	je	.L1223
	movl	200(%rdi), %ecx
	testl	%ecx, %ecx
	jne	.L1224
.L1232:
	movq	1600(%rbx), %rax
.L1229:
	testq	%rax, %rax
	sete	%al
	movzbl	%al, %eax
	negl	%eax
.L1222:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1224:
	.cfi_restore_state
	movq	8(%rdi), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L1226
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L1226
	cmpl	$65536, %eax
	jne	.L1232
.L1226:
	movq	1296(%rbx), %rax
	movq	544(%rax), %rax
	jmp	.L1229
	.p2align 4,,10
	.p2align 3
.L1223:
	movq	%rbx, %rdi
	call	SSL_in_before@PLT
	testl	%eax, %eax
	jne	.L1241
	movq	8(%rbx), %rdx
	movq	192(%rdx), %rcx
	testb	$8, 96(%rcx)
	jne	.L1231
	movl	(%rdx), %edx
	cmpl	$771, %edx
	jle	.L1231
	cmpl	$65536, %edx
	jne	.L1232
.L1231:
	movl	200(%rbx), %edx
	testl	%edx, %edx
	je	.L1232
	movq	1296(%rbx), %rdx
	cmpq	$0, 544(%rdx)
	jne	.L1222
	jmp	.L1232
	.p2align 4,,10
	.p2align 3
.L1241:
	movq	1600(%rbx), %rax
	testq	%rax, %rax
	jne	.L1229
	movq	1296(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L1229
	cmpl	$772, (%rdx)
	je	.L1229
	movq	544(%rdx), %rax
	jmp	.L1229
	.cfi_endproc
.LFE1764:
	.size	SSL_get_servername_type, .-SSL_get_servername_type
	.p2align 4
	.globl	SSL_select_next_proto
	.type	SSL_select_next_proto, @function
SSL_select_next_proto:
.LFB1765:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r9d, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -96(%rbp)
	movq	%rsi, -104(%rbp)
	movq	%rdx, -88(%rbp)
	movl	%ecx, -72(%rbp)
	movl	$0, -68(%rbp)
	testl	%ecx, %ecx
	je	.L1251
.L1243:
	movl	-68(%rbp), %eax
	movq	-88(%rbp), %rdi
	xorl	%r12d, %r12d
	movq	%rax, %rcx
	addq	%rdi, %rax
	movq	%rax, -80(%rbp)
	leal	1(%rcx), %ebx
	movzbl	(%rax), %eax
	addq	%rdi, %rbx
	movq	%rax, -56(%rbp)
	movq	%rax, %r13
	movq	%rbx, -64(%rbp)
	testl	%r14d, %r14d
	jne	.L1246
	jmp	.L1248
	.p2align 4,,10
	.p2align 3
.L1245:
	leal	1(%r12,%rbx), %r12d
	cmpl	%r12d, %r14d
	jbe	.L1248
.L1246:
	movl	%r12d, %eax
	movzbl	(%r15,%rax), %ebx
	cmpb	%r13b, %bl
	jne	.L1245
	leal	1(%r12), %esi
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rdi
	addq	%r15, %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L1245
	movl	$1, %eax
.L1244:
	movq	-80(%rbp), %rcx
	movq	-96(%rbp), %rdi
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rdi)
	movzbl	(%rcx), %edx
	movq	-104(%rbp), %rcx
	movb	%dl, (%rcx)
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1248:
	.cfi_restore_state
	movl	-68(%rbp), %eax
	movzbl	%r13b, %r10d
	leal	1(%rax,%r10), %eax
	movl	%eax, -68(%rbp)
	cmpl	%eax, -72(%rbp)
	ja	.L1243
.L1251:
	movq	%r15, -80(%rbp)
	movl	$2, %eax
	jmp	.L1244
	.cfi_endproc
.LFE1765:
	.size	SSL_select_next_proto, .-SSL_select_next_proto
	.p2align 4
	.globl	SSL_get0_next_proto_negotiated
	.type	SSL_get0_next_proto_negotiated, @function
SSL_get0_next_proto_negotiated:
.LFB1766:
	.cfi_startproc
	endbr64
	movq	1792(%rdi), %rax
	xorl	%ecx, %ecx
	movq	%rax, (%rsi)
	testq	%rax, %rax
	je	.L1260
	movl	1800(%rdi), %ecx
.L1260:
	movl	%ecx, (%rdx)
	ret
	.cfi_endproc
.LFE1766:
	.size	SSL_get0_next_proto_negotiated, .-SSL_get0_next_proto_negotiated
	.p2align 4
	.globl	SSL_CTX_set_next_protos_advertised_cb
	.type	SSL_CTX_set_next_protos_advertised_cb, @function
SSL_CTX_set_next_protos_advertised_cb:
.LFB1767:
	.cfi_startproc
	endbr64
	movq	%rsi, 648(%rdi)
	movq	%rdx, 656(%rdi)
	ret
	.cfi_endproc
.LFE1767:
	.size	SSL_CTX_set_next_protos_advertised_cb, .-SSL_CTX_set_next_protos_advertised_cb
	.p2align 4
	.globl	SSL_CTX_set_next_proto_select_cb
	.type	SSL_CTX_set_next_proto_select_cb, @function
SSL_CTX_set_next_proto_select_cb:
.LFB1768:
	.cfi_startproc
	endbr64
	movq	%rsi, 664(%rdi)
	movq	%rdx, 672(%rdi)
	ret
	.cfi_endproc
.LFE1768:
	.size	SSL_CTX_set_next_proto_select_cb, .-SSL_CTX_set_next_proto_select_cb
	.p2align 4
	.globl	SSL_CTX_set_alpn_protos
	.type	SSL_CTX_set_alpn_protos, @function
SSL_CTX_set_alpn_protos:
.LFB1769:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	leaq	.LC0(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	movl	$2832, %edx
	subq	$8, %rsp
	movq	632(%rdi), %rdi
	call	CRYPTO_free@PLT
	movl	$2833, %ecx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_memdup@PLT
	movq	%rax, 632(%r12)
	testq	%rax, %rax
	je	.L1269
	movq	%rbx, 640(%r12)
	xorl	%eax, %eax
.L1265:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1269:
	.cfi_restore_state
	movl	$2835, %r8d
	movl	$65, %edx
	movl	$343, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$1, %eax
	jmp	.L1265
	.cfi_endproc
.LFE1769:
	.size	SSL_CTX_set_alpn_protos, .-SSL_CTX_set_alpn_protos
	.p2align 4
	.globl	SSL_set_alpn_protos
	.type	SSL_set_alpn_protos, @function
SSL_set_alpn_protos:
.LFB1770:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	leaq	.LC0(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	movl	$2851, %edx
	subq	$8, %rsp
	movq	1776(%rdi), %rdi
	call	CRYPTO_free@PLT
	movl	$2852, %ecx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_memdup@PLT
	movq	%rax, 1776(%r12)
	testq	%rax, %rax
	je	.L1274
	movq	%rbx, 1784(%r12)
	xorl	%eax, %eax
.L1270:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1274:
	.cfi_restore_state
	movl	$2854, %r8d
	movl	$65, %edx
	movl	$344, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$1, %eax
	jmp	.L1270
	.cfi_endproc
.LFE1770:
	.size	SSL_set_alpn_protos, .-SSL_set_alpn_protos
	.p2align 4
	.globl	SSL_CTX_set_alpn_select_cb
	.type	SSL_CTX_set_alpn_select_cb, @function
SSL_CTX_set_alpn_select_cb:
.LFB1771:
	.cfi_startproc
	endbr64
	movq	%rsi, 616(%rdi)
	movq	%rdx, 624(%rdi)
	ret
	.cfi_endproc
.LFE1771:
	.size	SSL_CTX_set_alpn_select_cb, .-SSL_CTX_set_alpn_select_cb
	.p2align 4
	.globl	SSL_get0_alpn_selected
	.type	SSL_get0_alpn_selected, @function
SSL_get0_alpn_selected:
.LFB1772:
	.cfi_startproc
	endbr64
	movq	168(%rdi), %rax
	movq	$0, (%rsi)
	testq	%rax, %rax
	je	.L1279
	movq	992(%rax), %rcx
	movq	%rcx, (%rsi)
	testq	%rcx, %rcx
	je	.L1279
	movl	1000(%rax), %eax
	movl	%eax, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L1279:
	xorl	%eax, %eax
	movl	%eax, (%rdx)
	ret
	.cfi_endproc
.LFE1772:
	.size	SSL_get0_alpn_selected, .-SSL_get0_alpn_selected
	.p2align 4
	.globl	SSL_export_keying_material
	.type	SSL_export_keying_material, @function
SSL_export_keying_material:
.LFB1773:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	(%rdi), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	16(%rbp), %r10
	movl	24(%rbp), %r11d
	cmpl	$768, %eax
	jg	.L1286
	cmpl	$256, %eax
	jne	.L1284
.L1286:
	movq	8(%rdi), %rax
	movq	192(%rax), %rax
	movl	%r11d, 24(%rbp)
	movq	%r10, 16(%rbp)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	88(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1284:
	.cfi_restore_state
	movl	$-1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1773:
	.size	SSL_export_keying_material, .-SSL_export_keying_material
	.p2align 4
	.globl	SSL_export_keying_material_early
	.type	SSL_export_keying_material_early, @function
SSL_export_keying_material_early:
.LFB1774:
	.cfi_startproc
	endbr64
	cmpl	$772, (%rdi)
	je	.L1293
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1293:
	jmp	tls13_export_keying_material_early@PLT
	.cfi_endproc
.LFE1774:
	.size	SSL_export_keying_material_early, .-SSL_export_keying_material_early
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC3:
	.string	"ssl3-md5"
.LC4:
	.string	"ssl3-sha1"
	.text
	.p2align 4
	.globl	SSL_CTX_new
	.type	SSL_CTX_new, @function
SSL_CTX_new:
.LFB1777:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testq	%rdi, %rdi
	je	.L1351
	movq	%rdi, %rbx
	xorl	%esi, %esi
	movl	$2097152, %edi
	call	OPENSSL_init_ssl@PLT
	testl	%eax, %eax
	je	.L1350
	call	SSL_get_ex_data_X509_STORE_CTX_idx@PLT
	testl	%eax, %eax
	js	.L1352
	movl	$2977, %edx
	leaq	.LC0(%rip), %rsi
	movl	$1024, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1299
	movq	%rbx, (%rax)
	leaq	156(%r12), %r13
	movl	$4, 300(%rax)
	movq	$0, 304(%rax)
	movl	$2, 72(%rax)
	movq	$20480, 48(%rax)
	call	*184(%rbx)
	movq	%rax, 80(%r12)
	movl	$1, 156(%r12)
	mfence
	call	CRYPTO_THREAD_lock_new@PLT
	movq	%rax, 920(%r12)
	testq	%rax, %rax
	je	.L1353
	movq	$102400, 312(%r12)
	movl	$0, 352(%r12)
	call	ssl_cert_new@PLT
	movq	%rax, 320(%r12)
	testq	%rax, %rax
	je	.L1301
	leaq	ssl_session_cmp(%rip), %rsi
	leaq	ssl_session_hash(%rip), %rdi
	call	OPENSSL_LH_new@PLT
	movq	%rax, 40(%r12)
	testq	%rax, %rax
	je	.L1301
	call	X509_STORE_new@PLT
	movq	%rax, 32(%r12)
	testq	%rax, %rax
	je	.L1301
	call	CTLOG_STORE_new@PLT
	movq	%rax, 432(%r12)
	testq	%rax, %rax
	je	.L1301
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	SSL_CTX_set_ciphersuites@PLT
	testl	%eax, %eax
	je	.L1301
	movq	24(%r12), %rsi
	movq	(%r12), %rdi
	leaq	16(%r12), %rcx
	leaq	8(%r12), %rdx
	movq	320(%r12), %r9
	leaq	.LC2(%rip), %r8
	call	ssl_create_cipher_list@PLT
	testq	%rax, %rax
	je	.L1304
	movq	8(%r12), %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jle	.L1304
	call	X509_VERIFY_PARAM_new@PLT
	movq	%rax, 416(%r12)
	testq	%rax, %rax
	je	.L1301
	leaq	.LC3(%rip), %rdi
	call	EVP_get_digestbyname@PLT
	movq	%rax, 240(%r12)
	testq	%rax, %rax
	je	.L1354
	leaq	.LC4(%rip), %rdi
	call	EVP_get_digestbyname@PLT
	movq	%rax, 248(%r12)
	testq	%rax, %rax
	je	.L1355
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 280(%r12)
	testq	%rax, %rax
	je	.L1301
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 288(%r12)
	testq	%rax, %rax
	je	.L1301
	leaq	232(%r12), %rdx
	movq	%r12, %rsi
	movl	$1, %edi
	call	CRYPTO_new_ex_data@PLT
	testl	%eax, %eax
	je	.L1301
	movl	$3047, %edx
	leaq	.LC0(%rip), %rsi
	movl	$64, %edi
	call	CRYPTO_secure_zalloc@PLT
	movq	%rax, 544(%r12)
	testq	%rax, %rax
	je	.L1301
	movq	192(%rbx), %rax
	testb	$8, 96(%rax)
	je	.L1356
.L1307:
	movdqa	.LC5(%rip), %xmm0
	leaq	528(%r12), %rdi
	movl	$16, %esi
	movups	%xmm0, 456(%r12)
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jg	.L1308
.L1310:
	orl	$16384, 296(%r12)
.L1309:
	leaq	680(%r12), %rdi
	movl	$32, %esi
	call	RAND_priv_bytes@PLT
	testl	%eax, %eax
	jle	.L1301
	movq	%r12, %rdi
	call	SSL_CTX_SRP_CTX_init@PLT
	testl	%eax, %eax
	je	.L1301
	movabsq	$70368744177664, %rax
	movq	%r12, %rdi
	orl	$1179652, 296(%r12)
	movl	$-1, 576(%r12)
	movq	%rax, 936(%r12)
	movq	$2, 992(%r12)
	call	ssl_ctx_system_config@PLT
	jmp	.L1294
	.p2align 4,,10
	.p2align 3
.L1352:
	movl	$2974, %r8d
	movl	$269, %edx
	movl	$169, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$3144, %r8d
	movl	$65, %edx
	leaq	.LC0(%rip), %rcx
	movl	$169, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L1350:
	xorl	%r12d, %r12d
.L1294:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1301:
	.cfi_restore_state
	movl	$3144, %r8d
	movl	$65, %edx
	movl	$169, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L1303:
	movl	$-1, %eax
	lock xaddl	%eax, 0(%r13)
	subl	$1, %eax
	testl	%eax, %eax
	je	.L1357
	jg	.L1350
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	SSL_CTX_free.part.0
.L1358:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1357:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	SSL_CTX_free.part.0
	jmp	.L1358
	.p2align 4,,10
	.p2align 3
.L1351:
	movl	$2966, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$196, %edx
	xorl	%r12d, %r12d
	movl	$169, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L1294
	.p2align 4,,10
	.p2align 3
.L1353:
	movl	$2992, %r8d
	movl	$65, %edx
	movl	$169, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movl	$2993, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L1294
	.p2align 4,,10
	.p2align 3
.L1304:
	movl	$3021, %r8d
	movl	$161, %edx
	movl	$169, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1303
	.p2align 4,,10
	.p2align 3
.L1299:
	movl	$3144, %r8d
	movl	$65, %edx
	movl	$169, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1294
	.p2align 4,,10
	.p2align 3
.L1308:
	movq	544(%r12), %rdi
	movl	$32, %esi
	call	RAND_priv_bytes@PLT
	testl	%eax, %eax
	jle	.L1310
	movq	544(%r12), %rax
	movl	$32, %esi
	leaq	32(%rax), %rdi
	call	RAND_priv_bytes@PLT
	testl	%eax, %eax
	jg	.L1309
	jmp	.L1310
	.p2align 4,,10
	.p2align 3
.L1356:
	call	SSL_COMP_get_compression_methods@PLT
	movq	%rax, 264(%r12)
	jmp	.L1307
.L1354:
	movl	$3030, %r8d
	movl	$242, %edx
	movl	$169, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1303
.L1355:
	movl	$3034, %r8d
	movl	$243, %edx
	movl	$169, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1303
	.cfi_endproc
.LFE1777:
	.size	SSL_CTX_new, .-SSL_CTX_new
	.p2align 4
	.globl	SSL_CTX_up_ref
	.type	SSL_CTX_up_ref, @function
SSL_CTX_up_ref:
.LFB1778:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	lock xaddl	%eax, 156(%rdi)
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1778:
	.size	SSL_CTX_up_ref, .-SSL_CTX_up_ref
	.p2align 4
	.globl	SSL_CTX_free
	.type	SSL_CTX_free, @function
SSL_CTX_free:
.LFB1779:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1360
	movl	$-1, %eax
	lock xaddl	%eax, 156(%rdi)
	subl	$1, %eax
	testl	%eax, %eax
	je	.L1366
	jle	.L1364
.L1360:
	ret
	.p2align 4,,10
	.p2align 3
.L1366:
.L1364:
	jmp	SSL_CTX_free.part.0
	.cfi_endproc
.LFE1779:
	.size	SSL_CTX_free, .-SSL_CTX_free
	.p2align 4
	.globl	SSL_CTX_set_default_passwd_cb
	.type	SSL_CTX_set_default_passwd_cb, @function
SSL_CTX_set_default_passwd_cb:
.LFB1780:
	.cfi_startproc
	endbr64
	movq	%rsi, 176(%rdi)
	ret
	.cfi_endproc
.LFE1780:
	.size	SSL_CTX_set_default_passwd_cb, .-SSL_CTX_set_default_passwd_cb
	.p2align 4
	.globl	SSL_CTX_set_default_passwd_cb_userdata
	.type	SSL_CTX_set_default_passwd_cb_userdata, @function
SSL_CTX_set_default_passwd_cb_userdata:
.LFB1781:
	.cfi_startproc
	endbr64
	movq	%rsi, 184(%rdi)
	ret
	.cfi_endproc
.LFE1781:
	.size	SSL_CTX_set_default_passwd_cb_userdata, .-SSL_CTX_set_default_passwd_cb_userdata
	.p2align 4
	.globl	SSL_CTX_get_default_passwd_cb
	.type	SSL_CTX_get_default_passwd_cb, @function
SSL_CTX_get_default_passwd_cb:
.LFB1782:
	.cfi_startproc
	endbr64
	movq	176(%rdi), %rax
	ret
	.cfi_endproc
.LFE1782:
	.size	SSL_CTX_get_default_passwd_cb, .-SSL_CTX_get_default_passwd_cb
	.p2align 4
	.globl	SSL_CTX_get_default_passwd_cb_userdata
	.type	SSL_CTX_get_default_passwd_cb_userdata, @function
SSL_CTX_get_default_passwd_cb_userdata:
.LFB1783:
	.cfi_startproc
	endbr64
	movq	184(%rdi), %rax
	ret
	.cfi_endproc
.LFE1783:
	.size	SSL_CTX_get_default_passwd_cb_userdata, .-SSL_CTX_get_default_passwd_cb_userdata
	.p2align 4
	.globl	SSL_set_default_passwd_cb
	.type	SSL_set_default_passwd_cb, @function
SSL_set_default_passwd_cb:
.LFB1784:
	.cfi_startproc
	endbr64
	movq	%rsi, 6136(%rdi)
	ret
	.cfi_endproc
.LFE1784:
	.size	SSL_set_default_passwd_cb, .-SSL_set_default_passwd_cb
	.p2align 4
	.globl	SSL_set_default_passwd_cb_userdata
	.type	SSL_set_default_passwd_cb_userdata, @function
SSL_set_default_passwd_cb_userdata:
.LFB1785:
	.cfi_startproc
	endbr64
	movq	%rsi, 6144(%rdi)
	ret
	.cfi_endproc
.LFE1785:
	.size	SSL_set_default_passwd_cb_userdata, .-SSL_set_default_passwd_cb_userdata
	.p2align 4
	.globl	SSL_get_default_passwd_cb
	.type	SSL_get_default_passwd_cb, @function
SSL_get_default_passwd_cb:
.LFB1786:
	.cfi_startproc
	endbr64
	movq	6136(%rdi), %rax
	ret
	.cfi_endproc
.LFE1786:
	.size	SSL_get_default_passwd_cb, .-SSL_get_default_passwd_cb
	.p2align 4
	.globl	SSL_get_default_passwd_cb_userdata
	.type	SSL_get_default_passwd_cb_userdata, @function
SSL_get_default_passwd_cb_userdata:
.LFB1787:
	.cfi_startproc
	endbr64
	movq	6144(%rdi), %rax
	ret
	.cfi_endproc
.LFE1787:
	.size	SSL_get_default_passwd_cb_userdata, .-SSL_get_default_passwd_cb_userdata
	.p2align 4
	.globl	SSL_CTX_set_cert_verify_callback
	.type	SSL_CTX_set_cert_verify_callback, @function
SSL_CTX_set_cert_verify_callback:
.LFB1788:
	.cfi_startproc
	endbr64
	movq	%rsi, 160(%rdi)
	movq	%rdx, 168(%rdi)
	ret
	.cfi_endproc
.LFE1788:
	.size	SSL_CTX_set_cert_verify_callback, .-SSL_CTX_set_cert_verify_callback
	.p2align 4
	.globl	SSL_CTX_set_verify
	.type	SSL_CTX_set_verify, @function
SSL_CTX_set_verify:
.LFB1789:
	.cfi_startproc
	endbr64
	movl	%esi, 352(%rdi)
	movq	%rdx, 400(%rdi)
	ret
	.cfi_endproc
.LFE1789:
	.size	SSL_CTX_set_verify, .-SSL_CTX_set_verify
	.p2align 4
	.globl	SSL_CTX_set_verify_depth
	.type	SSL_CTX_set_verify_depth, @function
SSL_CTX_set_verify_depth:
.LFB1790:
	.cfi_startproc
	endbr64
	movq	416(%rdi), %rdi
	jmp	X509_VERIFY_PARAM_set_depth@PLT
	.cfi_endproc
.LFE1790:
	.size	SSL_CTX_set_verify_depth, .-SSL_CTX_set_verify_depth
	.p2align 4
	.globl	SSL_CTX_set_cert_cb
	.type	SSL_CTX_set_cert_cb, @function
SSL_CTX_set_cert_cb:
.LFB1791:
	.cfi_startproc
	endbr64
	movq	320(%rdi), %rdi
	jmp	ssl_cert_set_cert_cb@PLT
	.cfi_endproc
.LFE1791:
	.size	SSL_CTX_set_cert_cb, .-SSL_CTX_set_cert_cb
	.p2align 4
	.globl	SSL_set_cert_cb
	.type	SSL_set_cert_cb, @function
SSL_set_cert_cb:
.LFB1792:
	.cfi_startproc
	endbr64
	movq	1168(%rdi), %rdi
	jmp	ssl_cert_set_cert_cb@PLT
	.cfi_endproc
.LFE1792:
	.size	SSL_set_cert_cb, .-SSL_set_cert_cb
	.p2align 4
	.globl	ssl_set_masks
	.type	ssl_set_masks, @function
ssl_set_masks:
.LFB1793:
	.cfi_startproc
	endbr64
	movq	1168(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L1432
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	168(%rdi), %r14
	movl	784(%r14), %ecx
	movl	792(%r14), %edi
	movl	796(%r14), %esi
	andl	$1, %ecx
	andl	$1, %edi
	andl	$1, %esi
	cmpq	$0, 8(%rdx)
	je	.L1435
.L1382:
	movl	%ecx, %ebx
	movslq	%ecx, %r8
	orl	$2, %ebx
	movslq	%ebx, %rbx
.L1383:
	movl	$1, %r13d
	testl	%ecx, %ecx
	jne	.L1384
	xorl	%r13d, %r13d
	cmpq	$0, 72(%rdx)
	je	.L1384
	cmpq	$0, 80(%rdx)
	je	.L1384
	testb	$1, 789(%r14)
	jne	.L1436
	.p2align 4,,10
	.p2align 3
.L1384:
	movq	%r13, %rax
	movq	%r14, %rcx
	orq	$2, %rax
	testl	%edi, %edi
	cmovne	%rax, %r13
	movq	%r13, %r15
	orq	$4, %r15
	testl	%esi, %esi
	jne	.L1437
.L1386:
	cmpq	$0, 312(%rdx)
	je	.L1390
	cmpq	$0, 320(%rdx)
	je	.L1390
	testb	$1, 813(%r14)
	jne	.L1438
.L1390:
	cmpq	$0, 352(%rdx)
	je	.L1389
	cmpq	$0, 360(%rdx)
	je	.L1389
	testb	$1, 817(%r14)
	je	.L1389
	movl	(%r12), %eax
	movl	%eax, %edx
	sarl	$8, %edx
	cmpl	$3, %edx
	jne	.L1389
	cmpl	$771, %eax
	sete	%al
	orq	$12, %r13
	testb	%al, %al
	cmovne	%r13, %r15
.L1389:
	movq	%rbx, %rax
	orq	$16, %r15
	orq	$76, %rbx
	orq	$12, %rax
	testq	%r8, %r8
	movl	%r15d, 824(%rcx)
	cmovne	%rbx, %rax
	movq	%rax, %rdx
	orb	$1, %dh
	testb	$2, %al
	cmovne	%rdx, %rax
	orb	$-128, %al
	movl	%eax, 820(%rcx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1437:
	.cfi_restore_state
	movq	152(%rdx), %rdi
	movq	%r8, -56(%rbp)
	movq	%r14, -64(%rbp)
	call	X509_get_key_usage@PLT
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %r8
	testb	$2, 796(%rcx)
	je	.L1401
	testb	%al, %al
	jns	.L1401
	movq	%r13, %r15
	movq	168(%r12), %rcx
	orq	$12, %r15
	jmp	.L1389
	.p2align 4,,10
	.p2align 3
.L1401:
	movq	1168(%r12), %rdx
	movq	168(%r12), %rcx
	jmp	.L1386
	.p2align 4,,10
	.p2align 3
.L1435:
	cmpq	$0, 16(%rdx)
	jne	.L1382
	movl	24(%rdx), %eax
	testl	%eax, %eax
	jne	.L1382
	movslq	%ecx, %rbx
	movq	%rbx, %r8
	jmp	.L1383
	.p2align 4,,10
	.p2align 3
.L1438:
	movl	(%r12), %eax
	movl	%eax, %esi
	sarl	$8, %esi
	cmpl	$3, %esi
	jne	.L1390
	cmpl	$771, %eax
	jne	.L1390
	movq	%r13, %r15
	orq	$12, %r15
	jmp	.L1389
	.p2align 4,,10
	.p2align 3
.L1436:
	movl	(%r12), %eax
	movl	%eax, %ecx
	sarl	$8, %ecx
	cmpl	$3, %ecx
	sete	%cl
	xorl	%r13d, %r13d
	cmpl	$771, %eax
	sete	%r13b
	andq	%rcx, %r13
	jmp	.L1384
	.p2align 4,,10
	.p2align 3
.L1432:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE1793:
	.size	ssl_set_masks, .-ssl_set_masks
	.p2align 4
	.globl	ssl_check_srvr_ecc_cert_and_alg
	.type	ssl_check_srvr_ecc_cert_and_alg, @function
ssl_check_srvr_ecc_cert_and_alg:
.LFB1794:
	.cfi_startproc
	endbr64
	movq	168(%rsi), %rax
	movq	568(%rax), %rax
	testb	$8, 32(%rax)
	jne	.L1440
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1440:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509_get_key_usage@PLT
	testb	$-128, %al
	je	.L1446
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1446:
	.cfi_restore_state
	movl	$3418, %r8d
	movl	$318, %edx
	movl	$279, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1794:
	.size	ssl_check_srvr_ecc_cert_and_alg, .-ssl_check_srvr_ecc_cert_and_alg
	.p2align 4
	.globl	ssl_get_server_cert_serverinfo
	.type	ssl_get_server_cert_serverinfo, @function
ssl_get_server_cert_serverinfo:
.LFB1795:
	.cfi_startproc
	endbr64
	movq	168(%rdi), %rax
	movq	736(%rax), %rax
	movq	$0, (%rdx)
	testq	%rax, %rax
	je	.L1450
	movq	24(%rax), %rcx
	testq	%rcx, %rcx
	je	.L1450
	movq	32(%rax), %rax
	movq	%rcx, (%rsi)
	movq	%rax, (%rdx)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1450:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1795:
	.size	ssl_get_server_cert_serverinfo, .-ssl_get_server_cert_serverinfo
	.p2align 4
	.globl	ssl_update_cache
	.type	ssl_update_cache, @function
ssl_update_cache:
.LFB1796:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	1296(%rdi), %rsi
	cmpq	$0, 336(%rsi)
	je	.L1451
	movl	56(%rdi), %eax
	movq	%rdi, %rbx
	testl	%eax, %eax
	je	.L1454
	cmpq	$0, 376(%rsi)
	jne	.L1454
	testb	$1, 1376(%rdi)
	jne	.L1451
	.p2align 4,,10
	.p2align 3
.L1454:
	movq	1904(%rbx), %rdi
	movl	72(%rdi), %r13d
	movl	%r13d, %r14d
	andl	%r12d, %r14d
	je	.L1457
	movl	200(%rbx), %edx
	testl	%edx, %edx
	je	.L1458
	movq	8(%rbx), %rdx
	movq	192(%rdx), %rcx
	testb	$8, 96(%rcx)
	jne	.L1457
	movl	(%rdx), %edx
	cmpl	$65536, %edx
	je	.L1457
	cmpl	$771, %edx
	jg	.L1490
	.p2align 4,,10
	.p2align 3
.L1457:
	andl	$128, %r13d
	jne	.L1451
	cmpl	%r12d, %r14d
	je	.L1491
.L1451:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1458:
	.cfi_restore_state
	testl	$512, %r13d
	jne	.L1462
	movq	8(%rbx), %rdx
	movq	192(%rdx), %rcx
	testb	$8, 96(%rcx)
	jne	.L1463
	movl	(%rdx), %edx
.L1467:
	cmpl	$771, %edx
	setle	%cl
	cmpl	$65536, %edx
	sete	%dl
	orb	%dl, %cl
	jne	.L1463
	testl	%eax, %eax
	je	.L1463
	movl	6176(%rbx), %eax
	testl	%eax, %eax
	je	.L1464
	testb	$1, 1495(%rbx)
	je	.L1463
.L1464:
	cmpq	$0, 96(%rdi)
	jne	.L1463
	testb	$64, 1493(%rbx)
	je	.L1462
	.p2align 4,,10
	.p2align 3
.L1463:
	call	SSL_CTX_add_session@PLT
	movq	1904(%rbx), %rdi
.L1462:
	cmpq	$0, 88(%rdi)
	je	.L1457
	movq	1296(%rbx), %rdi
	call	SSL_SESSION_up_ref@PLT
	movq	1904(%rbx), %rax
	movq	1296(%rbx), %rsi
	movq	%rbx, %rdi
	call	*88(%rax)
	testl	%eax, %eax
	jne	.L1457
	movq	1296(%rbx), %rdi
	call	SSL_SESSION_free@PLT
	jmp	.L1457
	.p2align 4,,10
	.p2align 3
.L1491:
	movq	1904(%rbx), %rax
	leaq	120(%rax), %rdx
	addq	$132, %rax
	andl	$1, %r12d
	cmovne	%rdx, %rax
	movl	(%rax), %eax
	movzbl	%al, %eax
	cmpl	$255, %eax
	jne	.L1451
	xorl	%edi, %edi
	call	time@PLT
	movq	1904(%rbx), %rdi
	popq	%rbx
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	SSL_CTX_flush_sessions@PLT
	.p2align 4,,10
	.p2align 3
.L1490:
	.cfi_restore_state
	testl	$512, %r13d
	je	.L1467
	jmp	.L1462
	.cfi_endproc
.LFE1796:
	.size	ssl_update_cache, .-ssl_update_cache
	.p2align 4
	.globl	SSL_CTX_get_ssl_method
	.type	SSL_CTX_get_ssl_method, @function
SSL_CTX_get_ssl_method:
.LFB1797:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE1797:
	.size	SSL_CTX_get_ssl_method, .-SSL_CTX_get_ssl_method
	.p2align 4
	.globl	SSL_get_ssl_method
	.type	SSL_get_ssl_method, @function
SSL_get_ssl_method:
.LFB1798:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE1798:
	.size	SSL_get_ssl_method, .-SSL_get_ssl_method
	.p2align 4
	.globl	SSL_set_ssl_method
	.type	SSL_set_ssl_method, @function
SSL_set_ssl_method:
.LFB1799:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	8(%rdi), %r13
	cmpq	%rsi, %r13
	je	.L1494
	movq	48(%rdi), %r14
	movl	(%rsi), %ecx
	movq	%rdi, %rbx
	movq	%rsi, %r12
	cmpl	%ecx, 0(%r13)
	je	.L1501
	call	*32(%r13)
	movq	%r12, 8(%rbx)
	movq	%rbx, %rdi
	call	*16(%r12)
	cmpq	%r14, 48(%r13)
	je	.L1502
.L1498:
	cmpq	%r14, 40(%r13)
	je	.L1503
.L1494:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1501:
	.cfi_restore_state
	movq	%rsi, 8(%rdi)
	cmpq	%r14, 48(%r13)
	jne	.L1498
.L1502:
	movq	48(%r12), %rdx
	movq	%rdx, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1503:
	.cfi_restore_state
	movq	40(%r12), %rdx
	movq	%rdx, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1799:
	.size	SSL_set_ssl_method, .-SSL_set_ssl_method
	.p2align 4
	.globl	SSL_get_error
	.type	SSL_get_error, @function
SSL_get_error:
.LFB1800:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testl	%esi, %esi
	jle	.L1541
	ret
	.p2align 4,,10
	.p2align 3
.L1541:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	ERR_peek_error@PLT
	testq	%rax, %rax
	jne	.L1542
	movl	40(%rbx), %eax
	cmpl	$3, %eax
	je	.L1543
.L1507:
	cmpl	$2, %eax
	je	.L1544
.L1513:
	cmpl	$4, %eax
	je	.L1504
	cmpl	$5, %eax
	je	.L1521
	cmpl	$6, %eax
	je	.L1522
	cmpl	$7, %eax
	je	.L1523
	testb	$2, 68(%rbx)
	je	.L1512
	movq	168(%rbx), %rax
	movl	244(%rax), %eax
	testl	%eax, %eax
	je	.L1545
.L1512:
	popq	%rbx
	movl	$5, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1542:
	.cfi_restore_state
	movl	%eax, %edx
	movl	$1, %eax
	shrl	$24, %edx
	cmpl	$2, %edx
	je	.L1512
.L1504:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1545:
	.cfi_restore_state
	movl	$6, %eax
	jmp	.L1504
	.p2align 4,,10
	.p2align 3
.L1543:
	movq	16(%rbx), %r12
	movl	$1, %esi
	movq	%r12, %rdi
	call	BIO_test_flags@PLT
	testl	%eax, %eax
	jne	.L1515
	movl	$2, %esi
	movq	%r12, %rdi
	call	BIO_test_flags@PLT
	testl	%eax, %eax
	jne	.L1514
	movl	$4, %esi
	movq	%r12, %rdi
	call	BIO_test_flags@PLT
	testl	%eax, %eax
	jne	.L1510
	movl	40(%rbx), %eax
	jmp	.L1507
	.p2align 4,,10
	.p2align 3
.L1544:
	movq	24(%rbx), %r12
	movl	$2, %esi
	movq	%r12, %rdi
	call	BIO_test_flags@PLT
	testl	%eax, %eax
	jne	.L1514
	movl	$1, %esi
	movq	%r12, %rdi
	call	BIO_test_flags@PLT
	testl	%eax, %eax
	jne	.L1515
	movl	$4, %esi
	movq	%r12, %rdi
	call	BIO_test_flags@PLT
	testl	%eax, %eax
	jne	.L1510
	movl	40(%rbx), %eax
	jmp	.L1513
	.p2align 4,,10
	.p2align 3
.L1515:
	movl	$2, %eax
	jmp	.L1504
	.p2align 4,,10
	.p2align 3
.L1514:
	movl	$3, %eax
	jmp	.L1504
	.p2align 4,,10
	.p2align 3
.L1521:
	movl	$9, %eax
	jmp	.L1504
	.p2align 4,,10
	.p2align 3
.L1522:
	movl	$10, %eax
	jmp	.L1504
	.p2align 4,,10
	.p2align 3
.L1523:
	movl	$11, %eax
	jmp	.L1504
	.p2align 4,,10
	.p2align 3
.L1510:
	movq	%r12, %rdi
	call	BIO_get_retry_reason@PLT
	cmpl	$2, %eax
	je	.L1546
	cmpl	$3, %eax
	jne	.L1512
	movl	$8, %eax
	jmp	.L1504
.L1546:
	movl	$7, %eax
	jmp	.L1504
	.cfi_endproc
.LFE1800:
	.size	SSL_get_error, .-SSL_get_error
	.p2align 4
	.globl	SSL_do_handshake
	.type	SSL_do_handshake, @function
SSL_do_handshake:
.LFB1802:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 48(%rdi)
	je	.L1556
	movq	%rdi, %r12
	movl	$-1, %esi
	call	ossl_statem_check_finish_init@PLT
	movq	8(%r12), %rax
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	*96(%rax)
	movq	%r12, %rdi
	call	SSL_in_init@PLT
	testl	%eax, %eax
	je	.L1550
.L1553:
	testb	$1, 1497(%r12)
	jne	.L1557
.L1552:
	movq	%r12, %rdi
	call	*48(%r12)
.L1547:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1558
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1550:
	.cfi_restore_state
	movq	%r12, %rdi
	call	SSL_in_before@PLT
	testl	%eax, %eax
	jne	.L1553
	movl	$1, %eax
	jmp	.L1547
	.p2align 4,,10
	.p2align 3
.L1557:
	call	ASYNC_get_current_job@PLT
	testq	%rax, %rax
	jne	.L1552
	leaq	-64(%rbp), %rsi
	leaq	ssl_do_handshake_intern(%rip), %rdx
	movq	%r12, %rdi
	movq	%r12, -64(%rbp)
	call	ssl_start_async_job
	jmp	.L1547
.L1556:
	movl	$3645, %r8d
	movl	$144, %edx
	movl	$180, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L1547
.L1558:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1802:
	.size	SSL_do_handshake, .-SSL_do_handshake
	.p2align 4
	.globl	SSL_write_early_data
	.type	SSL_write_early_data, @function
SSL_write_early_data:
.LFB1739:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	132(%rdi), %r15d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$12, %r15d
	ja	.L1560
	movq	%rdx, %r13
	movq	%rcx, %r14
	movl	%r15d, %edx
	movq	%rdi, %rbx
	leaq	.L1562(%rip), %rcx
	movq	%rsi, %r12
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1562:
	.long	.L1566-.L1562
	.long	.L1565-.L1562
	.long	.L1560-.L1562
	.long	.L1564-.L1562
	.long	.L1560-.L1562
	.long	.L1563-.L1562
	.long	.L1560-.L1562
	.long	.L1560-.L1562
	.long	.L1560-.L1562
	.long	.L1560-.L1562
	.long	.L1561-.L1562
	.long	.L1560-.L1562
	.long	.L1561-.L1562
	.text
	.p2align 4,,10
	.p2align 3
.L1566:
	movl	56(%rdi), %edx
	testl	%edx, %edx
	jne	.L1569
	call	SSL_in_before@PLT
	testl	%eax, %eax
	je	.L1569
	movq	1296(%rbx), %rax
	testq	%rax, %rax
	je	.L1570
	movl	580(%rax), %eax
	testl	%eax, %eax
	jne	.L1565
.L1570:
	cmpq	$0, 1432(%rbx)
	je	.L1569
.L1565:
	movl	$2, 132(%rbx)
	cmpq	$0, 48(%rbx)
	jne	.L1571
	movl	$0, 56(%rbx)
	movq	%rbx, %rdi
	movl	$0, 68(%rbx)
	call	ossl_statem_clear@PLT
	movq	8(%rbx), %rax
	movq	1088(%rbx), %rdi
	movq	48(%rax), %rax
	movq	%rax, 48(%rbx)
	testq	%rdi, %rdi
	je	.L1572
	call	EVP_CIPHER_CTX_free@PLT
	movq	$0, 1088(%rbx)
.L1572:
	movq	1136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1573
	call	EVP_CIPHER_CTX_free@PLT
	movq	$0, 1136(%rbx)
.L1573:
	movq	1112(%rbx), %rdi
	call	EVP_MD_CTX_free@PLT
	movq	1160(%rbx), %rdi
	movq	$0, 1112(%rbx)
	call	EVP_MD_CTX_free@PLT
	movq	$0, 1160(%rbx)
	.p2align 4,,10
	.p2align 3
.L1571:
	movq	%rbx, %rdi
	call	SSL_do_handshake
	testl	%eax, %eax
	jle	.L1600
.L1564:
	movl	$4, 132(%rbx)
	movl	1496(%rbx), %eax
	movl	%eax, %r15d
	andl	$-2, %eax
	andl	$1, %r15d
	cmpq	$0, 48(%rbx)
	movl	%eax, 1496(%rbx)
	je	.L1601
	testb	$1, 68(%rbx)
	jne	.L1602
	movl	$1, %esi
	movq	%rbx, %rdi
	call	ossl_statem_check_finish_init@PLT
	testb	$1, 1497(%rbx)
	je	.L1577
	call	ASYNC_get_current_job@PLT
	testq	%rax, %rax
	je	.L1603
.L1577:
	movq	8(%rbx), %rax
	leaq	-104(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	*72(%rax)
.L1578:
	orl	%r15d, 1496(%rbx)
	testl	%eax, %eax
	jle	.L1575
	movl	$5, 132(%rbx)
.L1563:
	movq	%rbx, %rdi
	call	statem_flush@PLT
	movl	%eax, %r12d
	cmpl	$1, %eax
	jne	.L1582
	movq	%r13, (%r14)
	movl	$3, 132(%rbx)
.L1559:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1604
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1582:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L1559
	.p2align 4,,10
	.p2align 3
.L1560:
	movl	$2054, %r8d
.L1599:
	leaq	.LC0(%rip), %rcx
	movl	$66, %edx
	movl	$526, %esi
	xorl	%r12d, %r12d
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L1559
	.p2align 4,,10
	.p2align 3
.L1561:
	movl	$6, 132(%rdi)
	movq	%r14, %rcx
	movq	%r13, %rdx
	movl	$0, %r12d
	call	ssl_write_internal
	testl	%eax, %eax
	cmovns	%eax, %r12d
	jle	.L1580
	movq	24(%rbx), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
.L1580:
	movl	%r15d, 132(%rbx)
	jmp	.L1559
	.p2align 4,,10
	.p2align 3
.L1600:
	movl	$1, 132(%rbx)
	xorl	%r12d, %r12d
	jmp	.L1559
	.p2align 4,,10
	.p2align 3
.L1569:
	movl	$1999, %r8d
	jmp	.L1599
.L1601:
	movl	$1919, %r8d
	movl	$276, %edx
	movl	$524, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	orl	%r15d, 1496(%rbx)
	.p2align 4,,10
	.p2align 3
.L1575:
	movl	$3, 132(%rbx)
	xorl	%r12d, %r12d
	jmp	.L1559
	.p2align 4,,10
	.p2align 3
.L1603:
	movq	8(%rbx), %rax
	movq	%r12, %xmm1
	leaq	-96(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rbx, %xmm0
	leaq	ssl_io_intern(%rip), %rdx
	movq	%r13, -80(%rbp)
	movq	72(%rax), %rax
	punpcklqdq	%xmm1, %xmm0
	movl	$1, -72(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -64(%rbp)
	call	ssl_start_async_job
	movq	6168(%rbx), %rdx
	movq	%rdx, -104(%rbp)
	jmp	.L1578
.L1602:
	movl	$1925, %r8d
	movl	$207, %edx
	movl	$524, %esi
	movl	$1, 40(%rbx)
	leaq	.LC0(%rip), %rcx
	movl	$20, %edi
	call	ERR_put_error@PLT
	orl	%r15d, 1496(%rbx)
	jmp	.L1575
.L1604:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1739:
	.size	SSL_write_early_data, .-SSL_write_early_data
	.p2align 4
	.globl	SSL_read_early_data
	.type	SSL_read_early_data, @function
SSL_read_early_data:
.LFB1731:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	56(%rdi), %eax
	testl	%eax, %eax
	je	.L1631
	movl	132(%rdi), %eax
	movq	%rdi, %rbx
	movq	%rsi, %r13
	movq	%rdx, %r14
	movq	%rcx, %r12
	cmpl	$8, %eax
	je	.L1608
	cmpl	$10, %eax
	je	.L1609
	testl	%eax, %eax
	je	.L1632
	movl	$1846, %r8d
	movl	$66, %edx
	movl	$529, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L1605:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1632:
	.cfi_restore_state
	call	SSL_in_before@PLT
	testl	%eax, %eax
	je	.L1633
.L1608:
	movl	$9, 132(%rbx)
	cmpq	$0, 48(%rbx)
	je	.L1634
.L1611:
	movq	%rbx, %rdi
	call	SSL_do_handshake
	testl	%eax, %eax
	jle	.L1635
.L1609:
	cmpl	$2, 1816(%rbx)
	je	.L1636
	movl	$12, 132(%rbx)
.L1616:
	movq	$0, (%r12)
	addq	$16, %rsp
	movl	$2, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1631:
	.cfi_restore_state
	movl	%eax, -36(%rbp)
	movl	$1801, %r8d
.L1630:
	movl	$66, %edx
	movl	$529, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	-36(%rbp), %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1635:
	.cfi_restore_state
	movl	$8, 132(%rbx)
	xorl	%eax, %eax
	jmp	.L1605
	.p2align 4,,10
	.p2align 3
.L1636:
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movl	$11, 132(%rbx)
	call	ssl_read_internal
	testl	%eax, %eax
	jg	.L1615
	cmpl	$12, 132(%rbx)
	je	.L1616
.L1615:
	movl	$10, 132(%rbx)
	testl	%eax, %eax
	setg	%al
	addq	$16, %rsp
	popq	%rbx
	movzbl	%al, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1634:
	.cfi_restore_state
	movl	$1, 56(%rbx)
	movq	%rbx, %rdi
	movl	$0, 68(%rbx)
	call	ossl_statem_clear@PLT
	movq	8(%rbx), %rax
	movq	1088(%rbx), %rdi
	movq	40(%rax), %rax
	movq	%rax, 48(%rbx)
	testq	%rdi, %rdi
	je	.L1612
	call	EVP_CIPHER_CTX_free@PLT
	movq	$0, 1088(%rbx)
.L1612:
	movq	1136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1613
	call	EVP_CIPHER_CTX_free@PLT
	movq	$0, 1136(%rbx)
.L1613:
	movq	1112(%rbx), %rdi
	call	EVP_MD_CTX_free@PLT
	movq	1160(%rbx), %rdi
	movq	$0, 1112(%rbx)
	call	EVP_MD_CTX_free@PLT
	movq	$0, 1160(%rbx)
	jmp	.L1611
	.p2align 4,,10
	.p2align 3
.L1633:
	movl	%eax, -36(%rbp)
	movl	$1808, %r8d
	jmp	.L1630
	.cfi_endproc
.LFE1731:
	.size	SSL_read_early_data, .-SSL_read_early_data
	.p2align 4
	.globl	SSL_set_accept_state
	.type	SSL_set_accept_state, @function
SSL_set_accept_state:
.LFB1803:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	$1, 56(%rdi)
	movl	$0, 68(%rdi)
	call	ossl_statem_clear@PLT
	movq	8(%rbx), %rax
	movq	1088(%rbx), %rdi
	movq	40(%rax), %rax
	movq	%rax, 48(%rbx)
	testq	%rdi, %rdi
	je	.L1638
	call	EVP_CIPHER_CTX_free@PLT
	movq	$0, 1088(%rbx)
.L1638:
	movq	1136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1639
	call	EVP_CIPHER_CTX_free@PLT
	movq	$0, 1136(%rbx)
.L1639:
	movq	1112(%rbx), %rdi
	call	EVP_MD_CTX_free@PLT
	movq	1160(%rbx), %rdi
	movq	$0, 1112(%rbx)
	call	EVP_MD_CTX_free@PLT
	movq	$0, 1160(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1803:
	.size	SSL_set_accept_state, .-SSL_set_accept_state
	.p2align 4
	.globl	SSL_set_connect_state
	.type	SSL_set_connect_state, @function
SSL_set_connect_state:
.LFB1804:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	$0, 56(%rdi)
	movl	$0, 68(%rdi)
	call	ossl_statem_clear@PLT
	movq	8(%rbx), %rax
	movq	1088(%rbx), %rdi
	movq	48(%rax), %rax
	movq	%rax, 48(%rbx)
	testq	%rdi, %rdi
	je	.L1648
	call	EVP_CIPHER_CTX_free@PLT
	movq	$0, 1088(%rbx)
.L1648:
	movq	1136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1649
	call	EVP_CIPHER_CTX_free@PLT
	movq	$0, 1136(%rbx)
.L1649:
	movq	1112(%rbx), %rdi
	call	EVP_MD_CTX_free@PLT
	movq	1160(%rbx), %rdi
	movq	$0, 1112(%rbx)
	call	EVP_MD_CTX_free@PLT
	movq	$0, 1160(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1804:
	.size	SSL_set_connect_state, .-SSL_set_connect_state
	.p2align 4
	.globl	ssl_undefined_void_function
	.type	ssl_undefined_void_function, @function
ssl_undefined_void_function:
.LFB1806:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$66, %edx
	movl	$244, %esi
	movl	$20, %edi
	movl	$3693, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1806:
	.size	ssl_undefined_void_function, .-ssl_undefined_void_function
	.p2align 4
	.globl	ssl_undefined_const_function
	.type	ssl_undefined_const_function, @function
ssl_undefined_const_function:
.LFB1807:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1807:
	.size	ssl_undefined_const_function, .-ssl_undefined_const_function
	.p2align 4
	.globl	ssl_bad_method
	.type	ssl_bad_method, @function
ssl_bad_method:
.LFB1808:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$66, %edx
	movl	$160, %esi
	movl	$20, %edi
	movl	$3705, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1808:
	.size	ssl_bad_method, .-ssl_bad_method
	.section	.rodata.str1.1
.LC6:
	.string	"unknown"
.LC7:
	.string	"TLSv1.3"
.LC8:
	.string	"TLSv1.1"
.LC9:
	.string	"TLSv1"
.LC10:
	.string	"SSLv3"
.LC11:
	.string	"DTLSv0.9"
.LC12:
	.string	"DTLSv1.2"
.LC13:
	.string	"TLSv1.2"
.LC14:
	.string	"DTLSv1"
	.text
	.p2align 4
	.globl	ssl_protocol_to_string
	.type	ssl_protocol_to_string, @function
ssl_protocol_to_string:
.LFB1809:
	.cfi_startproc
	endbr64
	cmpl	$772, %edi
	jg	.L1663
	cmpl	$767, %edi
	jle	.L1676
	subl	$769, %edi
	leaq	.LC10(%rip), %rax
	cmpl	$3, %edi
	ja	.L1662
	leaq	.L1667(%rip), %rdx
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1667:
	.long	.L1670-.L1667
	.long	.L1669-.L1667
	.long	.L1668-.L1667
	.long	.L1672-.L1667
	.text
.L1668:
	leaq	.LC13(%rip), %rax
	ret
.L1672:
	leaq	.LC7(%rip), %rax
.L1662:
	ret
	.p2align 4,,10
	.p2align 3
.L1676:
	cmpl	$256, %edi
	leaq	.LC11(%rip), %rdx
	leaq	.LC6(%rip), %rax
	cmove	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1663:
	leaq	.LC12(%rip), %rax
	cmpl	$65277, %edi
	je	.L1662
	cmpl	$65279, %edi
	leaq	.LC14(%rip), %rdx
	leaq	.LC6(%rip), %rax
	cmove	%rdx, %rax
	ret
.L1669:
	leaq	.LC8(%rip), %rax
	ret
.L1670:
	leaq	.LC9(%rip), %rax
	ret
	.cfi_endproc
.LFE1809:
	.size	ssl_protocol_to_string, .-ssl_protocol_to_string
	.p2align 4
	.globl	SSL_get_version
	.type	SSL_get_version, @function
SSL_get_version:
.LFB1810:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	cmpl	$772, %eax
	jg	.L1678
	cmpl	$767, %eax
	jle	.L1691
	subl	$769, %eax
	leaq	.LC10(%rip), %r8
	cmpl	$3, %eax
	ja	.L1677
	leaq	.L1682(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1682:
	.long	.L1685-.L1682
	.long	.L1684-.L1682
	.long	.L1683-.L1682
	.long	.L1687-.L1682
	.text
.L1683:
	leaq	.LC13(%rip), %r8
.L1677:
	movq	%r8, %rax
	ret
.L1687:
	leaq	.LC7(%rip), %r8
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1691:
	cmpl	$256, %eax
	leaq	.LC6(%rip), %r8
	leaq	.LC11(%rip), %rax
	cmove	%rax, %r8
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1678:
	leaq	.LC12(%rip), %r8
	cmpl	$65277, %eax
	je	.L1677
	cmpl	$65279, %eax
	leaq	.LC6(%rip), %r8
	leaq	.LC14(%rip), %rax
	cmove	%rax, %r8
	movq	%r8, %rax
	ret
.L1684:
	leaq	.LC8(%rip), %r8
	movq	%r8, %rax
	ret
.L1685:
	leaq	.LC9(%rip), %r8
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE1810:
	.size	SSL_get_version, .-SSL_get_version
	.p2align 4
	.globl	SSL_dup
	.type	SSL_dup, @function
SSL_dup:
.LFB1812:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	SSL_in_init@PLT
	testl	%eax, %eax
	je	.L1695
	movq	%r15, %rdi
	call	SSL_in_before@PLT
	testl	%eax, %eax
	jne	.L1852
.L1695:
	lock addl	$1, 1488(%r15)
	movq	%r15, %r12
.L1692:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1853
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1852:
	.cfi_restore_state
	movq	1440(%r15), %rdi
	call	SSL_new
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1848
	cmpq	$0, 1296(%r15)
	je	.L1697
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	SSL_copy_session_id
	testl	%eax, %eax
	je	.L1699
.L1698:
	movq	224(%r15), %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jle	.L1723
	movq	224(%r15), %rdi
	call	OPENSSL_sk_num@PLT
	movq	224(%r12), %rdi
	leaq	tlsa_free(%rip), %rsi
	movl	%eax, %ebx
	call	OPENSSL_sk_pop_free@PLT
	movq	X509_free@GOTPCREL(%rip), %rsi
	movq	$0, 224(%r12)
	movq	232(%r12), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	248(%r12), %rdi
	movq	$0, 232(%r12)
	call	X509_free@PLT
	pxor	%xmm0, %xmm0
	movl	%ebx, %esi
	xorl	%edi, %edi
	movups	%xmm0, 240(%r12)
	movq	272(%r15), %rax
	movq	$-1, 260(%r12)
	movq	%rax, 272(%r12)
	movq	1440(%r12), %rax
	addq	$872, %rax
	movq	%rax, 216(%r12)
	call	OPENSSL_sk_new_reserve@PLT
	movq	%rax, 224(%r12)
	testq	%rax, %rax
	je	.L1721
	leaq	-80(%rbp), %rax
	xorl	%r13d, %r13d
	movq	%rax, -128(%rbp)
	testl	%ebx, %ebx
	jle	.L1723
	movl	%ebx, -84(%rbp)
	movq	%r12, %r14
	movl	%r13d, %r12d
	.p2align 4,,10
	.p2align 3
.L1722:
	movq	224(%r15), %rdi
	movl	%r12d, %esi
	call	OPENSSL_sk_value@PLT
	cmpq	$0, 224(%r14)
	movq	16(%rax), %r13
	je	.L1854
	testl	%r13d, %r13d
	js	.L1725
	movslq	%r13d, %r11
	cmpq	%r11, %r13
	jne	.L1725
	movzbl	(%rax), %ebx
	cmpb	$3, %bl
	ja	.L1855
	movzbl	1(%rax), %ecx
	movb	%cl, -85(%rbp)
	cmpb	$1, %cl
	ja	.L1856
	movq	8(%rax), %rdx
	movzbl	2(%rax), %eax
	movq	%rdx, -104(%rbp)
	movb	%al, -108(%rbp)
	testb	%al, %al
	je	.L1729
	movq	216(%r14), %rdx
	cmpb	16(%rdx), %al
	ja	.L1730
	movq	(%rdx), %rdx
	movq	(%rdx,%rax,8), %rdi
	testq	%rdi, %rdi
	je	.L1730
	movq	%r11, -96(%rbp)
	call	EVP_MD_size@PLT
	movq	-96(%rbp), %r11
	cltq
	cmpq	%rax, %r13
	jne	.L1857
.L1729:
	cmpq	$0, -104(%rbp)
	je	.L1858
	movl	$338, %edx
	leaq	.LC0(%rip), %rsi
	movl	$32, %edi
	movq	%r11, -120(%rbp)
	call	CRYPTO_zalloc@PLT
	movq	-120(%rbp), %r11
	testq	%rax, %rax
	movq	%rax, -96(%rbp)
	je	.L1859
	movzbl	-85(%rbp), %edx
	movb	%bl, (%rax)
	movq	%r13, %rdi
	leaq	.LC0(%rip), %rsi
	movq	%r11, -120(%rbp)
	movb	%dl, 1(%rax)
	movzbl	-108(%rbp), %edx
	movb	%dl, 2(%rax)
	movl	$346, %edx
	call	CRYPTO_malloc@PLT
	movq	-120(%rbp), %r11
	movq	%rax, %rdi
	movq	-96(%rbp), %rax
	testq	%rdi, %rdi
	movq	%rdi, 8(%rax)
	je	.L1860
	movq	-104(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r11, -120(%rbp)
	call	memcpy@PLT
	movq	-96(%rbp), %rdx
	cmpb	$0, -108(%rbp)
	movq	%r13, 16(%rdx)
	jne	.L1735
	movq	-104(%rbp), %rax
	cmpb	$1, -85(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movq	-120(%rbp), %r11
	movq	%rax, -80(%rbp)
	je	.L1736
	movq	-128(%rbp), %rsi
	leaq	-72(%rbp), %rdi
	movq	%r11, %rdx
	call	d2i_X509@PLT
	testq	%rax, %rax
	je	.L1737
	movq	-80(%rbp), %rax
	movq	-104(%rbp), %rdx
	cmpq	%rax, %rdx
	ja	.L1737
	subq	%rdx, %rax
	cmpq	%r13, %rax
	je	.L1738
.L1737:
	movq	%r14, %r12
	movq	-96(%rbp), %r14
	movl	$186, %edx
	leaq	.LC0(%rip), %rsi
	movq	8(%r14), %rdi
	call	CRYPTO_free@PLT
	movq	24(%r14), %rdi
	call	EVP_PKEY_free@PLT
	movl	$188, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	movl	$366, %r8d
.L1846:
	movl	$180, %edx
	movl	$394, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L1699:
	movl	$-1, %eax
	lock xaddl	%eax, 1488(%r12)
	subl	$1, %eax
	testl	%eax, %eax
	je	.L1861
	jle	.L1771
.L1848:
	xorl	%r12d, %r12d
	jmp	.L1692
	.p2align 4,,10
	.p2align 3
.L1736:
	movq	-128(%rbp), %rsi
	leaq	-64(%rbp), %rdi
	movq	%r11, %rdx
	call	d2i_PUBKEY@PLT
	testq	%rax, %rax
	je	.L1744
	movq	-80(%rbp), %rax
	movq	-104(%rbp), %rdx
	cmpq	%rax, %rdx
	ja	.L1744
	subq	%rdx, %rax
	cmpq	%r13, %rax
	jne	.L1744
	movq	-64(%rbp), %rdi
	cmpb	$2, %bl
	jne	.L1746
	movq	-96(%rbp), %rax
	movq	%rdi, 24(%rax)
	.p2align 4,,10
	.p2align 3
.L1735:
	movq	224(%r14), %rdi
	call	OPENSSL_sk_num@PLT
	xorl	%edx, %edx
	movl	%eax, -104(%rbp)
	testl	%eax, %eax
	jle	.L1747
	movzbl	-108(%rbp), %r13d
	movl	%r12d, -108(%rbp)
	movq	%r13, %r12
	movl	%edx, %r13d
.L1749:
	movq	224(%r14), %rdi
	movl	%r13d, %esi
	call	OPENSSL_sk_value@PLT
	cmpb	(%rax), %bl
	jb	.L1748
	ja	.L1843
	movzbl	-85(%rbp), %edx
	cmpb	1(%rax), %dl
	jb	.L1748
	ja	.L1843
	movq	216(%r14), %rsi
	movzbl	2(%rax), %eax
	movq	8(%rsi), %rsi
	movzbl	(%rsi,%r12), %edx
	cmpb	%dl, (%rsi,%rax)
	ja	.L1748
.L1843:
	movl	-108(%rbp), %r12d
	movl	%r13d, %edx
.L1747:
	movq	224(%r14), %rdi
	movq	-96(%rbp), %rsi
	call	OPENSSL_sk_insert@PLT
	testl	%eax, %eax
	je	.L1862
	movl	$1, %eax
	movl	%ebx, %ecx
	addl	$1, %r12d
	sall	%cl, %eax
	orl	%eax, 256(%r14)
	cmpl	%r12d, -84(%rbp)
	jne	.L1722
	movq	%r14, %r12
.L1723:
	movl	(%r15), %eax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$50, %esi
	movq	%r15, %rdi
	movl	%eax, (%r12)
	movl	1492(%r15), %eax
	movl	%eax, 1492(%r12)
	movl	1496(%r15), %eax
	movl	%eax, 1496(%r12)
	call	SSL_ctrl
	xorl	%ecx, %ecx
	movl	$51, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	SSL_ctrl
	movl	2120(%r15), %eax
	movl	1376(%r15), %edx
	movl	%eax, 2120(%r12)
	movq	184(%r15), %rax
	movq	%rax, 184(%r12)
	movq	192(%r15), %rax
	movq	%rax, 192(%r12)
	movq	1384(%r15), %rax
	movl	%edx, 1376(%r12)
	testq	%rax, %rax
	je	.L1720
	movq	%rax, 1384(%r12)
.L1720:
	movq	208(%r15), %rdi
	call	X509_VERIFY_PARAM_get_depth@PLT
	movq	208(%r12), %rdi
	movl	%eax, %esi
	call	X509_VERIFY_PARAM_set_depth@PLT
	movq	1328(%r15), %rax
	xorl	%edi, %edi
	leaq	1464(%r15), %rdx
	leaq	1464(%r12), %rsi
	movq	%rax, 1328(%r12)
	movq	1392(%r15), %rax
	movq	%rax, 1392(%r12)
	call	CRYPTO_dup_ex_data@PLT
	testl	%eax, %eax
	je	.L1699
	movq	16(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1751
	xorl	%edx, %edx
	leaq	16(%r12), %rcx
	movl	$12, %esi
	call	BIO_ctrl@PLT
	testq	%rax, %rax
	je	.L1699
	movq	24(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1753
	cmpq	16(%r15), %rdi
	jne	.L1773
	movq	16(%r12), %rdi
	call	BIO_up_ref@PLT
	movq	16(%r12), %rax
	movq	%rax, 24(%r12)
.L1753:
	movl	56(%r15), %eax
	cmpq	$0, 48(%r15)
	movl	%eax, 56(%r12)
	je	.L1756
	testl	%eax, %eax
	jne	.L1863
	movl	$0, 68(%r12)
	movq	%r12, %rdi
	call	ossl_statem_clear@PLT
	movq	8(%r12), %rax
	movq	1088(%r12), %rdi
	movq	48(%rax), %rax
	movq	%rax, 48(%r12)
	testq	%rdi, %rdi
	je	.L1760
.L1851:
	call	EVP_CIPHER_CTX_free@PLT
	movq	$0, 1088(%r12)
.L1760:
	movq	1136(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1761
	call	EVP_CIPHER_CTX_free@PLT
	movq	$0, 1136(%r12)
.L1761:
	movq	1112(%r12), %rdi
	call	EVP_MD_CTX_free@PLT
	movq	1160(%r12), %rdi
	movq	$0, 1112(%r12)
	call	EVP_MD_CTX_free@PLT
	movq	$0, 1160(%r12)
.L1756:
	movl	68(%r15), %eax
	movq	208(%r12), %rdi
	movq	208(%r15), %rsi
	movl	%eax, 68(%r12)
	movl	200(%r15), %eax
	movl	%eax, 200(%r12)
	movq	6136(%r15), %rax
	movq	%rax, 6136(%r12)
	movq	6144(%r15), %rax
	movq	%rax, 6144(%r12)
	call	X509_VERIFY_PARAM_inherit@PLT
	movq	288(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1765
	call	OPENSSL_sk_dup@PLT
	movq	%rax, 288(%r12)
	testq	%rax, %rax
	je	.L1699
.L1765:
	movq	296(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1764
	call	OPENSSL_sk_dup@PLT
	movq	%rax, 296(%r12)
	testq	%rax, %rax
	je	.L1699
.L1764:
	movq	1472(%r15), %rsi
	testq	%rsi, %rsi
	je	.L1864
	leaq	1472(%r12), %rdi
	call	dup_ca_names.part.0
	testl	%eax, %eax
	je	.L1699
.L1768:
	movq	1480(%r15), %rsi
	testq	%rsi, %rsi
	je	.L1865
	leaq	1480(%r12), %rdi
	call	dup_ca_names.part.0
	testl	%eax, %eax
	jne	.L1692
	jmp	.L1699
	.p2align 4,,10
	.p2align 3
.L1725:
	movl	$307, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$189, %edx
	movq	%r14, %r12
	movl	$394, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L1699
	.p2align 4,,10
	.p2align 3
.L1854:
	movl	$302, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$175, %edx
	movq	%r14, %r12
	movl	$394, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L1699
	.p2align 4,,10
	.p2align 3
.L1697:
	movq	8(%r15), %r13
	movq	8(%rax), %rbx
	cmpq	%rbx, %r13
	je	.L1701
	movq	48(%rax), %r14
	movl	0(%r13), %eax
	cmpl	%eax, (%rbx)
	je	.L1866
	movq	%r12, %rdi
	call	*32(%rbx)
	movq	%r13, 8(%r12)
	movq	%r12, %rdi
	call	*16(%r13)
	cmpq	48(%rbx), %r14
	je	.L1867
	cmpq	40(%rbx), %r14
	je	.L1868
.L1705:
	testl	%eax, %eax
	je	.L1699
.L1701:
	cmpq	$0, 1168(%r15)
	je	.L1711
	movq	1168(%r12), %rdi
	call	ssl_cert_free@PLT
	movq	1168(%r15), %rdi
	call	ssl_cert_dup@PLT
	movq	%rax, 1168(%r12)
	testq	%rax, %rax
	je	.L1699
.L1711:
	movq	1256(%r15), %rax
	cmpl	$32, %eax
	ja	.L1869
	movl	%eax, %eax
	leaq	1264(%r12), %rdx
	leaq	1264(%r15), %rcx
	movq	%rax, 1256(%r12)
	cmpl	$8, %eax
	jnb	.L1712
	testb	$4, %al
	jne	.L1870
	testl	%eax, %eax
	je	.L1698
	movzbl	1264(%r15), %esi
	movb	%sil, (%rdx)
	testb	$2, %al
	je	.L1698
	movzwl	-2(%rcx,%rax), %ecx
	movw	%cx, -2(%rdx,%rax)
	jmp	.L1698
	.p2align 4,,10
	.p2align 3
.L1856:
	movl	$317, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$202, %edx
	movq	%r14, %r12
	movl	$394, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L1699
	.p2align 4,,10
	.p2align 3
.L1855:
	movl	$312, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$184, %edx
	movq	%r14, %r12
	movl	$394, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L1699
	.p2align 4,,10
	.p2align 3
.L1751:
	movq	24(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1753
.L1773:
	xorl	%edx, %edx
	leaq	24(%r12), %rcx
	movl	$12, %esi
	call	BIO_ctrl@PLT
	testq	%rax, %rax
	jne	.L1753
	jmp	.L1699
	.p2align 4,,10
	.p2align 3
.L1861:
.L1771:
	movq	%r12, %rdi
	call	SSL_free.part.0
	jmp	.L1848
	.p2align 4,,10
	.p2align 3
.L1730:
	movl	$324, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$200, %edx
	movq	%r14, %r12
	movl	$394, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L1699
	.p2align 4,,10
	.p2align 3
.L1863:
	movl	$1, 56(%r12)
	movq	%r12, %rdi
	movl	$0, 68(%r12)
	call	ossl_statem_clear@PLT
	movq	8(%r12), %rax
	movq	1088(%r12), %rdi
	movq	40(%rax), %rax
	movq	%rax, 48(%r12)
	testq	%rdi, %rdi
	jne	.L1851
	jmp	.L1760
	.p2align 4,,10
	.p2align 3
.L1869:
	movl	$890, %r8d
	movl	$273, %edx
	movl	$218, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1699
	.p2align 4,,10
	.p2align 3
.L1857:
	movl	$330, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$192, %edx
	movq	%r14, %r12
	movl	$394, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L1699
	.p2align 4,,10
	.p2align 3
.L1866:
	movq	%r13, 8(%r12)
	cmpq	48(%rbx), %r14
	je	.L1871
	cmpq	40(%rbx), %r14
	jne	.L1701
	movq	40(%r13), %rax
	movq	%rax, 48(%r12)
	jmp	.L1701
	.p2align 4,,10
	.p2align 3
.L1748:
	addl	$1, %r13d
	cmpl	%r13d, -104(%rbp)
	jne	.L1749
	jmp	.L1843
	.p2align 4,,10
	.p2align 3
.L1864:
	movq	$0, 1472(%r12)
	jmp	.L1768
	.p2align 4,,10
	.p2align 3
.L1858:
	movl	$334, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$203, %edx
	movq	%r14, %r12
	movl	$394, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L1699
	.p2align 4,,10
	.p2align 3
.L1865:
	movq	$0, 1480(%r12)
	jmp	.L1692
	.p2align 4,,10
	.p2align 3
.L1859:
	movq	%r14, %r12
	movl	$339, %r8d
.L1847:
	movl	$65, %edx
	movl	$394, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1699
	.p2align 4,,10
	.p2align 3
.L1712:
	movq	1264(%r15), %rsi
	leaq	1272(%r12), %rdi
	andq	$-8, %rdi
	movq	%rsi, 1264(%r12)
	movq	-8(%rcx,%rax), %rsi
	movq	%rsi, -8(%rdx,%rax)
	subq	%rdi, %rdx
	addl	%edx, %eax
	subq	%rdx, %rcx
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L1698
	andl	$-8, %eax
	xorl	%edx, %edx
.L1716:
	movl	%edx, %esi
	addl	$8, %edx
	movq	(%rcx,%rsi), %r8
	movq	%r8, (%rdi,%rsi)
	cmpl	%eax, %edx
	jb	.L1716
	jmp	.L1698
	.p2align 4,,10
	.p2align 3
.L1860:
	movl	$186, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %r12
	movq	%rax, %r14
	call	CRYPTO_free@PLT
	movq	24(%r14), %rdi
	call	EVP_PKEY_free@PLT
	movl	$188, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	movl	$349, %r8d
	jmp	.L1847
	.p2align 4,,10
	.p2align 3
.L1744:
	movq	%r14, %r12
	movq	-96(%rbp), %r14
	movl	$186, %edx
	leaq	.LC0(%rip), %rsi
	movq	8(%r14), %rdi
	call	CRYPTO_free@PLT
	movq	24(%r14), %rdi
	call	EVP_PKEY_free@PLT
	movq	%r14, %rdi
	movl	$188, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$401, %r8d
	movl	$201, %edx
	leaq	.LC0(%rip), %rcx
	movl	$394, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L1699
	.p2align 4,,10
	.p2align 3
.L1721:
	movl	$224, %r8d
	movl	$65, %edx
	movl	$403, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1699
	.p2align 4,,10
	.p2align 3
.L1867:
	movq	48(%r13), %rdx
	movq	%rdx, 48(%r12)
	jmp	.L1705
.L1868:
	movq	40(%r13), %rdx
	movq	%rdx, 48(%r12)
	jmp	.L1705
.L1862:
	movq	%r14, %r12
	movq	-96(%rbp), %r14
	movl	$186, %edx
	leaq	.LC0(%rip), %rsi
	movq	8(%r14), %rdi
	call	CRYPTO_free@PLT
	movq	24(%r14), %rdi
	call	EVP_PKEY_free@PLT
	movl	$188, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	movl	$451, %r8d
	jmp	.L1847
.L1738:
	movq	-72(%rbp), %rdi
	call	X509_get0_pubkey@PLT
	testq	%rax, %rax
	je	.L1872
	movl	$5, %eax
	btl	%ebx, %eax
	jnc	.L1873
	movq	232(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1874
.L1742:
	movq	-72(%rbp), %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L1735
.L1845:
	movl	$390, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movq	%r14, %r12
	movl	$394, %esi
	movl	$20, %edi
	movq	-96(%rbp), %r14
	call	ERR_put_error@PLT
	movq	-72(%rbp), %rdi
	call	X509_free@PLT
	movq	8(%r14), %rdi
	movl	$186, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	24(%r14), %rdi
	call	EVP_PKEY_free@PLT
	movl	$188, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	jmp	.L1699
.L1871:
	movq	48(%r13), %rax
	movq	%rax, 48(%r12)
	jmp	.L1701
.L1746:
	call	EVP_PKEY_free@PLT
	jmp	.L1735
.L1870:
	movl	1264(%r15), %esi
	movl	%esi, (%rdx)
	movl	-4(%rcx,%rax), %ecx
	movl	%ecx, -4(%rdx,%rax)
	jmp	.L1698
.L1873:
	movq	-72(%rbp), %rdi
	call	X509_free@PLT
	jmp	.L1735
.L1874:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 232(%r14)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L1742
	jmp	.L1845
.L1872:
	movq	%r14, %r12
	movq	-96(%rbp), %r14
	movq	%r14, %rdi
	call	tlsa_free.part.0
	movl	$371, %r8d
	jmp	.L1846
.L1853:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1812:
	.size	SSL_dup, .-SSL_dup
	.p2align 4
	.globl	ssl_clear_cipher_ctx
	.type	ssl_clear_cipher_ctx, @function
ssl_clear_cipher_ctx:
.LFB1813:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	1088(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1876
	call	EVP_CIPHER_CTX_free@PLT
	movq	$0, 1088(%rbx)
.L1876:
	movq	1136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1875
	call	EVP_CIPHER_CTX_free@PLT
	movq	$0, 1136(%rbx)
.L1875:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1813:
	.size	ssl_clear_cipher_ctx, .-ssl_clear_cipher_ctx
	.p2align 4
	.globl	SSL_get_certificate
	.type	SSL_get_certificate, @function
SSL_get_certificate:
.LFB1814:
	.cfi_startproc
	endbr64
	movq	1168(%rdi), %rax
	testq	%rax, %rax
	je	.L1885
	movq	(%rax), %rax
	movq	(%rax), %rax
.L1885:
	ret
	.cfi_endproc
.LFE1814:
	.size	SSL_get_certificate, .-SSL_get_certificate
	.p2align 4
	.globl	SSL_get_privatekey
	.type	SSL_get_privatekey, @function
SSL_get_privatekey:
.LFB1815:
	.cfi_startproc
	endbr64
	movq	1168(%rdi), %rax
	testq	%rax, %rax
	je	.L1890
	movq	(%rax), %rax
	movq	8(%rax), %rax
.L1890:
	ret
	.cfi_endproc
.LFE1815:
	.size	SSL_get_privatekey, .-SSL_get_privatekey
	.p2align 4
	.globl	SSL_CTX_get0_certificate
	.type	SSL_CTX_get0_certificate, @function
SSL_CTX_get0_certificate:
.LFB1816:
	.cfi_startproc
	endbr64
	movq	320(%rdi), %rax
	testq	%rax, %rax
	je	.L1895
	movq	(%rax), %rax
	movq	(%rax), %rax
.L1895:
	ret
	.cfi_endproc
.LFE1816:
	.size	SSL_CTX_get0_certificate, .-SSL_CTX_get0_certificate
	.p2align 4
	.globl	SSL_CTX_get0_privatekey
	.type	SSL_CTX_get0_privatekey, @function
SSL_CTX_get0_privatekey:
.LFB1817:
	.cfi_startproc
	endbr64
	movq	320(%rdi), %rax
	testq	%rax, %rax
	je	.L1900
	movq	(%rax), %rax
	movq	8(%rax), %rax
.L1900:
	ret
	.cfi_endproc
.LFE1817:
	.size	SSL_CTX_get0_privatekey, .-SSL_CTX_get0_privatekey
	.p2align 4
	.globl	SSL_get_current_cipher
	.type	SSL_get_current_cipher, @function
SSL_get_current_cipher:
.LFB1818:
	.cfi_startproc
	endbr64
	movq	1296(%rdi), %rax
	testq	%rax, %rax
	je	.L1905
	movq	504(%rax), %rax
.L1905:
	ret
	.cfi_endproc
.LFE1818:
	.size	SSL_get_current_cipher, .-SSL_get_current_cipher
	.p2align 4
	.globl	SSL_get_pending_cipher
	.type	SSL_get_pending_cipher, @function
SSL_get_pending_cipher:
.LFB1819:
	.cfi_startproc
	endbr64
	movq	168(%rdi), %rax
	movq	568(%rax), %rax
	ret
	.cfi_endproc
.LFE1819:
	.size	SSL_get_pending_cipher, .-SSL_get_pending_cipher
	.p2align 4
	.globl	SSL_get_current_compression
	.type	SSL_get_current_compression, @function
SSL_get_current_compression:
.LFB1820:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1820:
	.size	SSL_get_current_compression, .-SSL_get_current_compression
	.p2align 4
	.globl	SSL_get_current_expansion
	.type	SSL_get_current_expansion, @function
SSL_get_current_expansion:
.LFB1996:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1996:
	.size	SSL_get_current_expansion, .-SSL_get_current_expansion
	.p2align 4
	.globl	ssl_init_wbio_buffer
	.type	ssl_init_wbio_buffer, @function
ssl_init_wbio_buffer:
.LFB1822:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	$1, %r12d
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	cmpq	$0, 32(%rdi)
	je	.L1922
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1922:
	.cfi_restore_state
	movq	%rdi, %rbx
	call	BIO_f_buffer@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1916
	xorl	%ecx, %ecx
	movl	$1, %edx
	movl	$117, %esi
	movq	%rax, %rdi
	call	BIO_int_ctrl@PLT
	testq	%rax, %rax
	jne	.L1923
.L1916:
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	call	BIO_free@PLT
	movl	$3986, %r8d
	movl	$7, %edx
	leaq	.LC0(%rip), %rcx
	movl	$184, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1923:
	.cfi_restore_state
	movq	%r13, 32(%rbx)
	movq	24(%rbx), %rsi
	movq	%r13, %rdi
	call	BIO_push@PLT
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1822:
	.size	ssl_init_wbio_buffer, .-ssl_init_wbio_buffer
	.p2align 4
	.globl	ssl_free_wbio_buffer
	.type	ssl_free_wbio_buffer, @function
ssl_free_wbio_buffer:
.LFB1823:
	.cfi_startproc
	endbr64
	cmpq	$0, 32(%rdi)
	je	.L1927
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %rdi
	call	BIO_pop@PLT
	movq	32(%rbx), %rdi
	movq	%rax, 24(%rbx)
	call	BIO_free@PLT
	movq	$0, 32(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1927:
	.cfi_restore 3
	.cfi_restore 6
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1823:
	.size	ssl_free_wbio_buffer, .-ssl_free_wbio_buffer
	.p2align 4
	.globl	SSL_CTX_set_quiet_shutdown
	.type	SSL_CTX_set_quiet_shutdown, @function
SSL_CTX_set_quiet_shutdown:
.LFB1824:
	.cfi_startproc
	endbr64
	movl	%esi, 424(%rdi)
	ret
	.cfi_endproc
.LFE1824:
	.size	SSL_CTX_set_quiet_shutdown, .-SSL_CTX_set_quiet_shutdown
	.p2align 4
	.globl	SSL_CTX_get_quiet_shutdown
	.type	SSL_CTX_get_quiet_shutdown, @function
SSL_CTX_get_quiet_shutdown:
.LFB1825:
	.cfi_startproc
	endbr64
	movl	424(%rdi), %eax
	ret
	.cfi_endproc
.LFE1825:
	.size	SSL_CTX_get_quiet_shutdown, .-SSL_CTX_get_quiet_shutdown
	.p2align 4
	.globl	SSL_set_quiet_shutdown
	.type	SSL_set_quiet_shutdown, @function
SSL_set_quiet_shutdown:
.LFB1826:
	.cfi_startproc
	endbr64
	movl	%esi, 64(%rdi)
	ret
	.cfi_endproc
.LFE1826:
	.size	SSL_set_quiet_shutdown, .-SSL_set_quiet_shutdown
	.p2align 4
	.globl	SSL_get_quiet_shutdown
	.type	SSL_get_quiet_shutdown, @function
SSL_get_quiet_shutdown:
.LFB1827:
	.cfi_startproc
	endbr64
	movl	64(%rdi), %eax
	ret
	.cfi_endproc
.LFE1827:
	.size	SSL_get_quiet_shutdown, .-SSL_get_quiet_shutdown
	.p2align 4
	.globl	SSL_set_shutdown
	.type	SSL_set_shutdown, @function
SSL_set_shutdown:
.LFB1828:
	.cfi_startproc
	endbr64
	movl	%esi, 68(%rdi)
	ret
	.cfi_endproc
.LFE1828:
	.size	SSL_set_shutdown, .-SSL_set_shutdown
	.p2align 4
	.globl	SSL_get_shutdown
	.type	SSL_get_shutdown, @function
SSL_get_shutdown:
.LFB1829:
	.cfi_startproc
	endbr64
	movl	68(%rdi), %eax
	ret
	.cfi_endproc
.LFE1829:
	.size	SSL_get_shutdown, .-SSL_get_shutdown
	.p2align 4
	.globl	SSL_version
	.type	SSL_version, @function
SSL_version:
.LFB1830:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	ret
	.cfi_endproc
.LFE1830:
	.size	SSL_version, .-SSL_version
	.p2align 4
	.globl	SSL_client_version
	.type	SSL_client_version, @function
SSL_client_version:
.LFB1831:
	.cfi_startproc
	endbr64
	movl	1524(%rdi), %eax
	ret
	.cfi_endproc
.LFE1831:
	.size	SSL_client_version, .-SSL_client_version
	.p2align 4
	.globl	SSL_get_SSL_CTX
	.type	SSL_get_SSL_CTX, @function
SSL_get_SSL_CTX:
.LFB1832:
	.cfi_startproc
	endbr64
	movq	1440(%rdi), %rax
	ret
	.cfi_endproc
.LFE1832:
	.size	SSL_get_SSL_CTX, .-SSL_get_SSL_CTX
	.p2align 4
	.globl	SSL_set_SSL_CTX
	.type	SSL_set_SSL_CTX, @function
SSL_set_SSL_CTX:
.LFB1833:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	cmpq	%rsi, 1440(%rdi)
	je	.L1951
	movq	%rdi, %r13
	testq	%rsi, %rsi
	je	.L1957
.L1941:
	movq	320(%rbx), %rdi
	call	ssl_cert_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1956
	movq	1168(%r13), %rax
	leaq	472(%r12), %rdi
	leaq	472(%rax), %rsi
	call	custom_exts_copy_flags@PLT
	testl	%eax, %eax
	je	.L1958
	movq	1168(%r13), %rdi
	call	ssl_cert_free@PLT
	movq	1256(%r13), %rdx
	movq	%r12, 1168(%r13)
	cmpq	$32, %rdx
	ja	.L1956
	movq	1440(%r13), %rsi
	testq	%rsi, %rsi
	je	.L1945
	cmpq	360(%rsi), %rdx
	je	.L1959
.L1945:
	lock addl	$1, 156(%rbx)
	movq	1440(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1947
	movl	$-1, %eax
	lock xaddl	%eax, 156(%rdi)
	subl	$1, %eax
	testl	%eax, %eax
	je	.L1960
	jle	.L1949
.L1947:
	movq	%rbx, 1440(%r13)
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1958:
	.cfi_restore_state
	movq	%r12, %rdi
	call	ssl_cert_free@PLT
.L1956:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1957:
	.cfi_restore_state
	movq	1904(%rdi), %rbx
	jmp	.L1941
	.p2align 4,,10
	.p2align 3
.L1960:
.L1949:
	call	SSL_CTX_free.part.0
	jmp	.L1947
	.p2align 4,,10
	.p2align 3
.L1951:
	addq	$8, %rsp
	movq	%rsi, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1959:
	.cfi_restore_state
	leaq	1264(%r13), %r12
	addq	$368, %rsi
	movq	%r12, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L1945
	movq	360(%rbx), %rax
	movq	%rax, 1256(%r13)
	movdqu	368(%rbx), %xmm0
	movups	%xmm0, 1264(%r13)
	movdqu	384(%rbx), %xmm1
	movups	%xmm1, 1280(%r13)
	jmp	.L1945
	.cfi_endproc
.LFE1833:
	.size	SSL_set_SSL_CTX, .-SSL_set_SSL_CTX
	.p2align 4
	.globl	SSL_CTX_set_default_verify_paths
	.type	SSL_CTX_set_default_verify_paths, @function
SSL_CTX_set_default_verify_paths:
.LFB1834:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdi
	jmp	X509_STORE_set_default_paths@PLT
	.cfi_endproc
.LFE1834:
	.size	SSL_CTX_set_default_verify_paths, .-SSL_CTX_set_default_verify_paths
	.p2align 4
	.globl	SSL_CTX_set_default_verify_dir
	.type	SSL_CTX_set_default_verify_dir, @function
SSL_CTX_set_default_verify_dir:
.LFB1835:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	X509_LOOKUP_hash_dir@PLT
	movq	32(%rbx), %rdi
	movq	%rax, %rsi
	call	X509_STORE_add_lookup@PLT
	movq	%rax, %rdi
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1962
	xorl	%r8d, %r8d
	movl	$3, %ecx
	xorl	%edx, %edx
	movl	$2, %esi
	call	X509_LOOKUP_ctrl@PLT
	call	ERR_clear_error@PLT
	movl	$1, %eax
.L1962:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1835:
	.size	SSL_CTX_set_default_verify_dir, .-SSL_CTX_set_default_verify_dir
	.p2align 4
	.globl	SSL_CTX_set_default_verify_file
	.type	SSL_CTX_set_default_verify_file, @function
SSL_CTX_set_default_verify_file:
.LFB1836:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	X509_LOOKUP_file@PLT
	movq	32(%rbx), %rdi
	movq	%rax, %rsi
	call	X509_STORE_add_lookup@PLT
	movq	%rax, %rdi
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1968
	xorl	%r8d, %r8d
	movl	$3, %ecx
	xorl	%edx, %edx
	movl	$1, %esi
	call	X509_LOOKUP_ctrl@PLT
	call	ERR_clear_error@PLT
	movl	$1, %eax
.L1968:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1836:
	.size	SSL_CTX_set_default_verify_file, .-SSL_CTX_set_default_verify_file
	.p2align 4
	.globl	SSL_CTX_load_verify_locations
	.type	SSL_CTX_load_verify_locations, @function
SSL_CTX_load_verify_locations:
.LFB1837:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdi
	jmp	X509_STORE_load_locations@PLT
	.cfi_endproc
.LFE1837:
	.size	SSL_CTX_load_verify_locations, .-SSL_CTX_load_verify_locations
	.p2align 4
	.globl	SSL_set_info_callback
	.type	SSL_set_info_callback, @function
SSL_set_info_callback:
.LFB1838:
	.cfi_startproc
	endbr64
	movq	%rsi, 1392(%rdi)
	ret
	.cfi_endproc
.LFE1838:
	.size	SSL_set_info_callback, .-SSL_set_info_callback
	.p2align 4
	.globl	SSL_get_info_callback
	.type	SSL_get_info_callback, @function
SSL_get_info_callback:
.LFB1839:
	.cfi_startproc
	endbr64
	movq	1392(%rdi), %rax
	ret
	.cfi_endproc
.LFE1839:
	.size	SSL_get_info_callback, .-SSL_get_info_callback
	.p2align 4
	.globl	SSL_set_verify_result
	.type	SSL_set_verify_result, @function
SSL_set_verify_result:
.LFB1840:
	.cfi_startproc
	endbr64
	movq	%rsi, 1456(%rdi)
	ret
	.cfi_endproc
.LFE1840:
	.size	SSL_set_verify_result, .-SSL_set_verify_result
	.p2align 4
	.globl	SSL_get_verify_result
	.type	SSL_get_verify_result, @function
SSL_get_verify_result:
.LFB1841:
	.cfi_startproc
	endbr64
	movq	1456(%rdi), %rax
	ret
	.cfi_endproc
.LFE1841:
	.size	SSL_get_verify_result, .-SSL_get_verify_result
	.p2align 4
	.globl	SSL_get_client_random
	.type	SSL_get_client_random, @function
SSL_get_client_random:
.LFB1842:
	.cfi_startproc
	endbr64
	movl	$32, %eax
	testq	%rdx, %rdx
	je	.L1979
	cmpq	$32, %rdx
	movq	168(%rdi), %rdi
	cmovbe	%rdx, %rax
	leaq	184(%rdi), %rcx
	movl	%eax, %edx
	cmpl	$8, %eax
	jb	.L1996
	movq	184(%rdi), %rdx
	leaq	8(%rsi), %r8
	andq	$-8, %r8
	movq	%rdx, (%rsi)
	movl	%eax, %edx
	movq	-8(%rcx,%rdx), %rdi
	movq	%rdi, -8(%rsi,%rdx)
	subq	%r8, %rsi
	leal	(%rax,%rsi), %edx
	subq	%rsi, %rcx
	andl	$-8, %edx
	movq	%rcx, %rdi
	cmpl	$8, %edx
	jb	.L1979
	andl	$-8, %edx
	xorl	%ecx, %ecx
.L1985:
	movl	%ecx, %esi
	addl	$8, %ecx
	movq	(%rdi,%rsi), %r9
	movq	%r9, (%r8,%rsi)
	cmpl	%edx, %ecx
	jb	.L1985
.L1979:
	ret
	.p2align 4,,10
	.p2align 3
.L1996:
	testb	$4, %al
	jne	.L1997
	testl	%edx, %edx
	je	.L1979
	movzbl	184(%rdi), %edi
	movb	%dil, (%rsi)
	testb	$2, %dl
	je	.L1979
	movzwl	-2(%rcx,%rdx), %ecx
	movw	%cx, -2(%rsi,%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L1997:
	movl	184(%rdi), %edi
	movl	%edi, (%rsi)
	movl	-4(%rcx,%rdx), %ecx
	movl	%ecx, -4(%rsi,%rdx)
	ret
	.cfi_endproc
.LFE1842:
	.size	SSL_get_client_random, .-SSL_get_client_random
	.p2align 4
	.globl	SSL_get_server_random
	.type	SSL_get_server_random, @function
SSL_get_server_random:
.LFB1843:
	.cfi_startproc
	endbr64
	movl	$32, %eax
	testq	%rdx, %rdx
	je	.L1998
	cmpq	$32, %rdx
	movq	168(%rdi), %rdi
	cmovbe	%rdx, %rax
	leaq	152(%rdi), %rcx
	movl	%eax, %edx
	cmpl	$8, %eax
	jb	.L2015
	movq	152(%rdi), %rdx
	leaq	8(%rsi), %r8
	andq	$-8, %r8
	movq	%rdx, (%rsi)
	movl	%eax, %edx
	movq	-8(%rcx,%rdx), %rdi
	movq	%rdi, -8(%rsi,%rdx)
	subq	%r8, %rsi
	leal	(%rax,%rsi), %edx
	subq	%rsi, %rcx
	andl	$-8, %edx
	movq	%rcx, %rdi
	cmpl	$8, %edx
	jb	.L1998
	andl	$-8, %edx
	xorl	%ecx, %ecx
.L2004:
	movl	%ecx, %esi
	addl	$8, %ecx
	movq	(%rdi,%rsi), %r9
	movq	%r9, (%r8,%rsi)
	cmpl	%edx, %ecx
	jb	.L2004
.L1998:
	ret
	.p2align 4,,10
	.p2align 3
.L2015:
	testb	$4, %al
	jne	.L2016
	testl	%edx, %edx
	je	.L1998
	movzbl	152(%rdi), %edi
	movb	%dil, (%rsi)
	testb	$2, %dl
	je	.L1998
	movzwl	-2(%rcx,%rdx), %ecx
	movw	%cx, -2(%rsi,%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L2016:
	movl	152(%rdi), %edi
	movl	%edi, (%rsi)
	movl	-4(%rcx,%rdx), %ecx
	movl	%ecx, -4(%rsi,%rdx)
	ret
	.cfi_endproc
.LFE1843:
	.size	SSL_get_server_random, .-SSL_get_server_random
	.p2align 4
	.globl	SSL_SESSION_get_master_key
	.type	SSL_SESSION_get_master_key, @function
SSL_SESSION_get_master_key:
.LFB1844:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	testq	%rdx, %rdx
	je	.L2017
	cmpq	%rdx, %r12
	movq	%rsi, %r8
	leaq	80(%rdi), %rsi
	cmova	%rdx, %r12
	movq	%r8, %rdi
	movq	%r12, %rdx
	call	memcpy@PLT
.L2017:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1844:
	.size	SSL_SESSION_get_master_key, .-SSL_SESSION_get_master_key
	.p2align 4
	.globl	SSL_SESSION_set1_master_key
	.type	SSL_SESSION_set1_master_key, @function
SSL_SESSION_set1_master_key:
.LFB1845:
	.cfi_startproc
	endbr64
	cmpq	$256, %rdx
	jbe	.L2031
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2031:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	80(%rdi), %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	call	memcpy@PLT
	movq	%rbx, 8(%r12)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1845:
	.size	SSL_SESSION_set1_master_key, .-SSL_SESSION_set1_master_key
	.p2align 4
	.globl	SSL_set_ex_data
	.type	SSL_set_ex_data, @function
SSL_set_ex_data:
.LFB1846:
	.cfi_startproc
	endbr64
	addq	$1464, %rdi
	jmp	CRYPTO_set_ex_data@PLT
	.cfi_endproc
.LFE1846:
	.size	SSL_set_ex_data, .-SSL_set_ex_data
	.p2align 4
	.globl	SSL_get_ex_data
	.type	SSL_get_ex_data, @function
SSL_get_ex_data:
.LFB1847:
	.cfi_startproc
	endbr64
	addq	$1464, %rdi
	jmp	CRYPTO_get_ex_data@PLT
	.cfi_endproc
.LFE1847:
	.size	SSL_get_ex_data, .-SSL_get_ex_data
	.p2align 4
	.globl	SSL_CTX_set_ex_data
	.type	SSL_CTX_set_ex_data, @function
SSL_CTX_set_ex_data:
.LFB1848:
	.cfi_startproc
	endbr64
	addq	$232, %rdi
	jmp	CRYPTO_set_ex_data@PLT
	.cfi_endproc
.LFE1848:
	.size	SSL_CTX_set_ex_data, .-SSL_CTX_set_ex_data
	.p2align 4
	.globl	SSL_CTX_get_ex_data
	.type	SSL_CTX_get_ex_data, @function
SSL_CTX_get_ex_data:
.LFB1849:
	.cfi_startproc
	endbr64
	addq	$232, %rdi
	jmp	CRYPTO_get_ex_data@PLT
	.cfi_endproc
.LFE1849:
	.size	SSL_CTX_get_ex_data, .-SSL_CTX_get_ex_data
	.p2align 4
	.globl	SSL_CTX_get_cert_store
	.type	SSL_CTX_get_cert_store, @function
SSL_CTX_get_cert_store:
.LFB1850:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE1850:
	.size	SSL_CTX_get_cert_store, .-SSL_CTX_get_cert_store
	.p2align 4
	.globl	SSL_CTX_set_cert_store
	.type	SSL_CTX_set_cert_store, @function
SSL_CTX_set_cert_store:
.LFB1851:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	32(%rdi), %rdi
	call	X509_STORE_free@PLT
	movq	%r12, 32(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1851:
	.size	SSL_CTX_set_cert_store, .-SSL_CTX_set_cert_store
	.p2align 4
	.globl	SSL_CTX_set1_cert_store
	.type	SSL_CTX_set1_cert_store, @function
SSL_CTX_set1_cert_store:
.LFB1852:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	testq	%rsi, %rsi
	je	.L2040
	movq	%rsi, %rdi
	call	X509_STORE_up_ref@PLT
.L2040:
	movq	32(%r12), %rdi
	call	X509_STORE_free@PLT
	movq	%rbx, 32(%r12)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1852:
	.size	SSL_CTX_set1_cert_store, .-SSL_CTX_set1_cert_store
	.p2align 4
	.globl	SSL_want
	.type	SSL_want, @function
SSL_want:
.LFB1853:
	.cfi_startproc
	endbr64
	movl	40(%rdi), %eax
	ret
	.cfi_endproc
.LFE1853:
	.size	SSL_want, .-SSL_want
	.p2align 4
	.globl	SSL_CTX_set_tmp_dh_callback
	.type	SSL_CTX_set_tmp_dh_callback, @function
SSL_CTX_set_tmp_dh_callback:
.LFB1854:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	%rsi, %rdx
	movl	$6, %esi
	movq	216(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE1854:
	.size	SSL_CTX_set_tmp_dh_callback, .-SSL_CTX_set_tmp_dh_callback
	.p2align 4
	.globl	SSL_set_tmp_dh_callback
	.type	SSL_set_tmp_dh_callback, @function
SSL_set_tmp_dh_callback:
.LFB1855:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	%rsi, %rdx
	movl	$6, %esi
	movq	208(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE1855:
	.size	SSL_set_tmp_dh_callback, .-SSL_set_tmp_dh_callback
	.p2align 4
	.globl	SSL_CTX_use_psk_identity_hint
	.type	SSL_CTX_use_psk_identity_hint, @function
SSL_CTX_use_psk_identity_hint:
.LFB1856:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L2049
	movq	%rsi, %rdi
	movq	%rsi, %r12
	call	strlen@PLT
	cmpq	$128, %rax
	ja	.L2053
	movq	320(%rbx), %rax
	movl	$4282, %edx
	leaq	.LC0(%rip), %rsi
	movq	512(%rax), %rdi
	call	CRYPTO_free@PLT
	movq	320(%rbx), %r13
	movq	%r12, %rdi
	movl	$4284, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, 512(%r13)
	movq	320(%rbx), %rax
	cmpq	$0, 512(%rax)
	setne	%al
	addq	$8, %rsp
	popq	%rbx
	movzbl	%al, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2049:
	.cfi_restore_state
	movq	320(%rdi), %rax
	movl	$4282, %edx
	leaq	.LC0(%rip), %rsi
	movq	512(%rax), %rdi
	call	CRYPTO_free@PLT
	movq	320(%rbx), %rax
	movq	$0, 512(%rax)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2053:
	.cfi_restore_state
	movl	$4279, %r8d
	movl	$146, %edx
	movl	$272, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1856:
	.size	SSL_CTX_use_psk_identity_hint, .-SSL_CTX_use_psk_identity_hint
	.p2align 4
	.globl	SSL_use_psk_identity_hint
	.type	SSL_use_psk_identity_hint, @function
SSL_use_psk_identity_hint:
.LFB1857:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L2058
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L2056
	movq	%rsi, %rdi
	call	strlen@PLT
	cmpq	$128, %rax
	ja	.L2063
	movq	1168(%rbx), %rax
	movl	$4301, %edx
	leaq	.LC0(%rip), %rsi
	movq	512(%rax), %rdi
	call	CRYPTO_free@PLT
	movq	1168(%rbx), %r13
	movq	%r12, %rdi
	movl	$4303, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, 512(%r13)
	movq	1168(%rbx), %rax
	cmpq	$0, 512(%rax)
	setne	%al
	addq	$8, %rsp
	popq	%rbx
	movzbl	%al, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2056:
	.cfi_restore_state
	movq	1168(%rdi), %rax
	movl	$4301, %edx
	leaq	.LC0(%rip), %rsi
	movq	512(%rax), %rdi
	call	CRYPTO_free@PLT
	movq	1168(%rbx), %rax
	movq	$0, 512(%rax)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2058:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2063:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	$4298, %r8d
	movl	$146, %edx
	movl	$273, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1857:
	.size	SSL_use_psk_identity_hint, .-SSL_use_psk_identity_hint
	.p2align 4
	.globl	SSL_get_psk_identity_hint
	.type	SSL_get_psk_identity_hint, @function
SSL_get_psk_identity_hint:
.LFB1858:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L2066
	movq	1296(%rdi), %rax
	testq	%rax, %rax
	je	.L2064
	movq	416(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L2066:
	xorl	%eax, %eax
.L2064:
	ret
	.cfi_endproc
.LFE1858:
	.size	SSL_get_psk_identity_hint, .-SSL_get_psk_identity_hint
	.p2align 4
	.globl	SSL_get_psk_identity
	.type	SSL_get_psk_identity, @function
SSL_get_psk_identity:
.LFB1859:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L2072
	movq	1296(%rdi), %rax
	testq	%rax, %rax
	je	.L2070
	movq	424(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L2072:
	xorl	%eax, %eax
.L2070:
	ret
	.cfi_endproc
.LFE1859:
	.size	SSL_get_psk_identity, .-SSL_get_psk_identity
	.p2align 4
	.globl	SSL_set_psk_client_callback
	.type	SSL_set_psk_client_callback, @function
SSL_set_psk_client_callback:
.LFB1860:
	.cfi_startproc
	endbr64
	movq	%rsi, 1408(%rdi)
	ret
	.cfi_endproc
.LFE1860:
	.size	SSL_set_psk_client_callback, .-SSL_set_psk_client_callback
	.p2align 4
	.globl	SSL_CTX_set_psk_client_callback
	.type	SSL_CTX_set_psk_client_callback, @function
SSL_CTX_set_psk_client_callback:
.LFB1861:
	.cfi_startproc
	endbr64
	movq	%rsi, 712(%rdi)
	ret
	.cfi_endproc
.LFE1861:
	.size	SSL_CTX_set_psk_client_callback, .-SSL_CTX_set_psk_client_callback
	.p2align 4
	.globl	SSL_set_psk_server_callback
	.type	SSL_set_psk_server_callback, @function
SSL_set_psk_server_callback:
.LFB1862:
	.cfi_startproc
	endbr64
	movq	%rsi, 1416(%rdi)
	ret
	.cfi_endproc
.LFE1862:
	.size	SSL_set_psk_server_callback, .-SSL_set_psk_server_callback
	.p2align 4
	.globl	SSL_CTX_set_psk_server_callback
	.type	SSL_CTX_set_psk_server_callback, @function
SSL_CTX_set_psk_server_callback:
.LFB1863:
	.cfi_startproc
	endbr64
	movq	%rsi, 720(%rdi)
	ret
	.cfi_endproc
.LFE1863:
	.size	SSL_CTX_set_psk_server_callback, .-SSL_CTX_set_psk_server_callback
	.p2align 4
	.globl	SSL_set_psk_find_session_callback
	.type	SSL_set_psk_find_session_callback, @function
SSL_set_psk_find_session_callback:
.LFB1864:
	.cfi_startproc
	endbr64
	movq	%rsi, 1424(%rdi)
	ret
	.cfi_endproc
.LFE1864:
	.size	SSL_set_psk_find_session_callback, .-SSL_set_psk_find_session_callback
	.p2align 4
	.globl	SSL_CTX_set_psk_find_session_callback
	.type	SSL_CTX_set_psk_find_session_callback, @function
SSL_CTX_set_psk_find_session_callback:
.LFB1865:
	.cfi_startproc
	endbr64
	movq	%rsi, 728(%rdi)
	ret
	.cfi_endproc
.LFE1865:
	.size	SSL_CTX_set_psk_find_session_callback, .-SSL_CTX_set_psk_find_session_callback
	.p2align 4
	.globl	SSL_set_psk_use_session_callback
	.type	SSL_set_psk_use_session_callback, @function
SSL_set_psk_use_session_callback:
.LFB1866:
	.cfi_startproc
	endbr64
	movq	%rsi, 1432(%rdi)
	ret
	.cfi_endproc
.LFE1866:
	.size	SSL_set_psk_use_session_callback, .-SSL_set_psk_use_session_callback
	.p2align 4
	.globl	SSL_CTX_set_psk_use_session_callback
	.type	SSL_CTX_set_psk_use_session_callback, @function
SSL_CTX_set_psk_use_session_callback:
.LFB1867:
	.cfi_startproc
	endbr64
	movq	%rsi, 736(%rdi)
	ret
	.cfi_endproc
.LFE1867:
	.size	SSL_CTX_set_psk_use_session_callback, .-SSL_CTX_set_psk_use_session_callback
	.p2align 4
	.globl	SSL_CTX_set_msg_callback
	.type	SSL_CTX_set_msg_callback, @function
SSL_CTX_set_msg_callback:
.LFB1868:
	.cfi_startproc
	endbr64
	movq	%rsi, 336(%rdi)
	ret
	.cfi_endproc
.LFE1868:
	.size	SSL_CTX_set_msg_callback, .-SSL_CTX_set_msg_callback
	.p2align 4
	.globl	SSL_set_msg_callback
	.type	SSL_set_msg_callback, @function
SSL_set_msg_callback:
.LFB1869:
	.cfi_startproc
	endbr64
	movq	%rsi, 184(%rdi)
	ret
	.cfi_endproc
.LFE1869:
	.size	SSL_set_msg_callback, .-SSL_set_msg_callback
	.p2align 4
	.globl	SSL_CTX_set_not_resumable_session_callback
	.type	SSL_CTX_set_not_resumable_session_callback, @function
SSL_CTX_set_not_resumable_session_callback:
.LFB1870:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	%rsi, %rdx
	movl	$79, %esi
	movq	216(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE1870:
	.size	SSL_CTX_set_not_resumable_session_callback, .-SSL_CTX_set_not_resumable_session_callback
	.p2align 4
	.globl	SSL_set_not_resumable_session_callback
	.type	SSL_set_not_resumable_session_callback, @function
SSL_set_not_resumable_session_callback:
.LFB1871:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	%rsi, %rdx
	movl	$79, %esi
	movq	208(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE1871:
	.size	SSL_set_not_resumable_session_callback, .-SSL_set_not_resumable_session_callback
	.p2align 4
	.globl	SSL_CTX_set_record_padding_callback
	.type	SSL_CTX_set_record_padding_callback, @function
SSL_CTX_set_record_padding_callback:
.LFB1872:
	.cfi_startproc
	endbr64
	movq	%rsi, 944(%rdi)
	ret
	.cfi_endproc
.LFE1872:
	.size	SSL_CTX_set_record_padding_callback, .-SSL_CTX_set_record_padding_callback
	.p2align 4
	.globl	SSL_CTX_set_record_padding_callback_arg
	.type	SSL_CTX_set_record_padding_callback_arg, @function
SSL_CTX_set_record_padding_callback_arg:
.LFB1873:
	.cfi_startproc
	endbr64
	movq	%rsi, 952(%rdi)
	ret
	.cfi_endproc
.LFE1873:
	.size	SSL_CTX_set_record_padding_callback_arg, .-SSL_CTX_set_record_padding_callback_arg
	.p2align 4
	.globl	SSL_CTX_get_record_padding_callback_arg
	.type	SSL_CTX_get_record_padding_callback_arg, @function
SSL_CTX_get_record_padding_callback_arg:
.LFB1874:
	.cfi_startproc
	endbr64
	movq	952(%rdi), %rax
	ret
	.cfi_endproc
.LFE1874:
	.size	SSL_CTX_get_record_padding_callback_arg, .-SSL_CTX_get_record_padding_callback_arg
	.p2align 4
	.globl	SSL_CTX_set_block_padding
	.type	SSL_CTX_set_block_padding, @function
SSL_CTX_set_block_padding:
.LFB1875:
	.cfi_startproc
	endbr64
	cmpq	$1, %rsi
	je	.L2095
	xorl	%eax, %eax
	cmpq	$16384, %rsi
	ja	.L2091
	movq	%rsi, 960(%rdi)
	movl	$1, %eax
.L2091:
	ret
	.p2align 4,,10
	.p2align 3
.L2095:
	movq	$0, 960(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1875:
	.size	SSL_CTX_set_block_padding, .-SSL_CTX_set_block_padding
	.p2align 4
	.globl	SSL_set_record_padding_callback
	.type	SSL_set_record_padding_callback, @function
SSL_set_record_padding_callback:
.LFB1876:
	.cfi_startproc
	endbr64
	movq	%rsi, 6192(%rdi)
	ret
	.cfi_endproc
.LFE1876:
	.size	SSL_set_record_padding_callback, .-SSL_set_record_padding_callback
	.p2align 4
	.globl	SSL_set_record_padding_callback_arg
	.type	SSL_set_record_padding_callback_arg, @function
SSL_set_record_padding_callback_arg:
.LFB1877:
	.cfi_startproc
	endbr64
	movq	%rsi, 6200(%rdi)
	ret
	.cfi_endproc
.LFE1877:
	.size	SSL_set_record_padding_callback_arg, .-SSL_set_record_padding_callback_arg
	.p2align 4
	.globl	SSL_get_record_padding_callback_arg
	.type	SSL_get_record_padding_callback_arg, @function
SSL_get_record_padding_callback_arg:
.LFB1878:
	.cfi_startproc
	endbr64
	movq	6200(%rdi), %rax
	ret
	.cfi_endproc
.LFE1878:
	.size	SSL_get_record_padding_callback_arg, .-SSL_get_record_padding_callback_arg
	.p2align 4
	.globl	SSL_set_block_padding
	.type	SSL_set_block_padding, @function
SSL_set_block_padding:
.LFB1879:
	.cfi_startproc
	endbr64
	cmpq	$1, %rsi
	je	.L2103
	xorl	%eax, %eax
	cmpq	$16384, %rsi
	ja	.L2099
	movq	%rsi, 6208(%rdi)
	movl	$1, %eax
.L2099:
	ret
	.p2align 4,,10
	.p2align 3
.L2103:
	movq	$0, 6208(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1879:
	.size	SSL_set_block_padding, .-SSL_set_block_padding
	.p2align 4
	.globl	SSL_set_num_tickets
	.type	SSL_set_num_tickets, @function
SSL_set_num_tickets:
.LFB1880:
	.cfi_startproc
	endbr64
	movq	%rsi, 6224(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1880:
	.size	SSL_set_num_tickets, .-SSL_set_num_tickets
	.p2align 4
	.globl	SSL_get_num_tickets
	.type	SSL_get_num_tickets, @function
SSL_get_num_tickets:
.LFB1881:
	.cfi_startproc
	endbr64
	movq	6224(%rdi), %rax
	ret
	.cfi_endproc
.LFE1881:
	.size	SSL_get_num_tickets, .-SSL_get_num_tickets
	.p2align 4
	.globl	SSL_CTX_set_num_tickets
	.type	SSL_CTX_set_num_tickets, @function
SSL_CTX_set_num_tickets:
.LFB1882:
	.cfi_startproc
	endbr64
	movq	%rsi, 992(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1882:
	.size	SSL_CTX_set_num_tickets, .-SSL_CTX_set_num_tickets
	.p2align 4
	.globl	SSL_CTX_get_num_tickets
	.type	SSL_CTX_get_num_tickets, @function
SSL_CTX_get_num_tickets:
.LFB1883:
	.cfi_startproc
	endbr64
	movq	992(%rdi), %rax
	ret
	.cfi_endproc
.LFE1883:
	.size	SSL_CTX_get_num_tickets, .-SSL_CTX_get_num_tickets
	.p2align 4
	.globl	ssl_replace_hash
	.type	ssl_replace_hash, @function
ssl_replace_hash:
.LFB1884:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	call	EVP_MD_CTX_free@PLT
	movq	$0, (%rbx)
	call	EVP_MD_CTX_new@PLT
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L2109
	testq	%r12, %r12
	je	.L2108
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	movq	(%rbx), %rax
	jle	.L2109
.L2108:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2109:
	.cfi_restore_state
	movq	%rax, %rdi
	call	EVP_MD_CTX_free@PLT
	movq	$0, (%rbx)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1884:
	.size	ssl_replace_hash, .-ssl_replace_hash
	.p2align 4
	.globl	ssl_clear_hash_ctx
	.type	ssl_clear_hash_ctx, @function
ssl_clear_hash_ctx:
.LFB1885:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	EVP_MD_CTX_free@PLT
	movq	$0, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1885:
	.size	ssl_clear_hash_ctx, .-ssl_clear_hash_ctx
	.p2align 4
	.globl	ssl_handshake_hash
	.type	ssl_handshake_hash, @function
ssl_handshake_hash:
.LFB1886:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	168(%rdi), %rax
	movq	%rsi, -56(%rbp)
	movq	%rcx, -64(%rbp)
	movq	232(%rax), %r12
	movq	%r12, %rdi
	call	EVP_MD_CTX_md@PLT
	movq	%rax, %rdi
	call	EVP_MD_size@PLT
	testl	%eax, %eax
	js	.L2123
	movslq	%eax, %r14
	cmpq	%rbx, %r14
	jbe	.L2124
.L2123:
	movl	$4519, %r9d
	movl	$68, %ecx
	movq	%r15, %rdi
	xorl	%ebx, %ebx
	leaq	.LC0(%rip), %r8
	movl	$560, %edx
	movl	$80, %esi
	xorl	%r13d, %r13d
	call	ossl_statem_fatal@PLT
.L2125:
	movq	%rbx, %rdi
	call	EVP_MD_CTX_free@PLT
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2124:
	.cfi_restore_state
	call	EVP_MD_CTX_new@PLT
	xorl	%r13d, %r13d
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L2125
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	EVP_MD_CTX_copy_ex@PLT
	testl	%eax, %eax
	je	.L2127
	movq	-56(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	jle	.L2127
	movq	-64(%rbp), %rax
	movl	$1, %r13d
	movq	%r14, (%rax)
	jmp	.L2125
	.p2align 4,,10
	.p2align 3
.L2127:
	movl	$68, %ecx
	movl	$560, %edx
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	movl	$4530, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L2125
	.cfi_endproc
.LFE1886:
	.size	ssl_handshake_hash, .-ssl_handshake_hash
	.p2align 4
	.globl	SSL_session_reused
	.type	SSL_session_reused, @function
SSL_session_reused:
.LFB1887:
	.cfi_startproc
	endbr64
	movl	200(%rdi), %eax
	ret
	.cfi_endproc
.LFE1887:
	.size	SSL_session_reused, .-SSL_session_reused
	.p2align 4
	.globl	SSL_is_server
	.type	SSL_is_server, @function
SSL_is_server:
.LFB1888:
	.cfi_startproc
	endbr64
	movl	56(%rdi), %eax
	ret
	.cfi_endproc
.LFE1888:
	.size	SSL_is_server, .-SSL_is_server
	.p2align 4
	.globl	SSL_set_debug
	.type	SSL_set_debug, @function
SSL_set_debug:
.LFB1889:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE1889:
	.size	SSL_set_debug, .-SSL_set_debug
	.p2align 4
	.globl	SSL_set_security_level
	.type	SSL_set_security_level, @function
SSL_set_security_level:
.LFB1890:
	.cfi_startproc
	endbr64
	movq	1168(%rdi), %rax
	movl	%esi, 496(%rax)
	ret
	.cfi_endproc
.LFE1890:
	.size	SSL_set_security_level, .-SSL_set_security_level
	.p2align 4
	.globl	SSL_get_security_level
	.type	SSL_get_security_level, @function
SSL_get_security_level:
.LFB1891:
	.cfi_startproc
	endbr64
	movq	1168(%rdi), %rax
	movl	496(%rax), %eax
	ret
	.cfi_endproc
.LFE1891:
	.size	SSL_get_security_level, .-SSL_get_security_level
	.p2align 4
	.globl	SSL_set_security_callback
	.type	SSL_set_security_callback, @function
SSL_set_security_callback:
.LFB1892:
	.cfi_startproc
	endbr64
	movq	1168(%rdi), %rax
	movq	%rsi, 488(%rax)
	ret
	.cfi_endproc
.LFE1892:
	.size	SSL_set_security_callback, .-SSL_set_security_callback
	.p2align 4
	.globl	SSL_get_security_callback
	.type	SSL_get_security_callback, @function
SSL_get_security_callback:
.LFB1893:
	.cfi_startproc
	endbr64
	movq	1168(%rdi), %rax
	movq	488(%rax), %rax
	ret
	.cfi_endproc
.LFE1893:
	.size	SSL_get_security_callback, .-SSL_get_security_callback
	.p2align 4
	.globl	SSL_set0_security_ex_data
	.type	SSL_set0_security_ex_data, @function
SSL_set0_security_ex_data:
.LFB1894:
	.cfi_startproc
	endbr64
	movq	1168(%rdi), %rax
	movq	%rsi, 504(%rax)
	ret
	.cfi_endproc
.LFE1894:
	.size	SSL_set0_security_ex_data, .-SSL_set0_security_ex_data
	.p2align 4
	.globl	SSL_get0_security_ex_data
	.type	SSL_get0_security_ex_data, @function
SSL_get0_security_ex_data:
.LFB1895:
	.cfi_startproc
	endbr64
	movq	1168(%rdi), %rax
	movq	504(%rax), %rax
	ret
	.cfi_endproc
.LFE1895:
	.size	SSL_get0_security_ex_data, .-SSL_get0_security_ex_data
	.p2align 4
	.globl	SSL_CTX_set_security_level
	.type	SSL_CTX_set_security_level, @function
SSL_CTX_set_security_level:
.LFB1896:
	.cfi_startproc
	endbr64
	movq	320(%rdi), %rax
	movl	%esi, 496(%rax)
	ret
	.cfi_endproc
.LFE1896:
	.size	SSL_CTX_set_security_level, .-SSL_CTX_set_security_level
	.p2align 4
	.globl	SSL_CTX_get_security_level
	.type	SSL_CTX_get_security_level, @function
SSL_CTX_get_security_level:
.LFB1897:
	.cfi_startproc
	endbr64
	movq	320(%rdi), %rax
	movl	496(%rax), %eax
	ret
	.cfi_endproc
.LFE1897:
	.size	SSL_CTX_get_security_level, .-SSL_CTX_get_security_level
	.p2align 4
	.globl	SSL_CTX_set_security_callback
	.type	SSL_CTX_set_security_callback, @function
SSL_CTX_set_security_callback:
.LFB1898:
	.cfi_startproc
	endbr64
	movq	320(%rdi), %rax
	movq	%rsi, 488(%rax)
	ret
	.cfi_endproc
.LFE1898:
	.size	SSL_CTX_set_security_callback, .-SSL_CTX_set_security_callback
	.p2align 4
	.globl	SSL_CTX_get_security_callback
	.type	SSL_CTX_get_security_callback, @function
SSL_CTX_get_security_callback:
.LFB1899:
	.cfi_startproc
	endbr64
	movq	320(%rdi), %rax
	movq	488(%rax), %rax
	ret
	.cfi_endproc
.LFE1899:
	.size	SSL_CTX_get_security_callback, .-SSL_CTX_get_security_callback
	.p2align 4
	.globl	SSL_CTX_set0_security_ex_data
	.type	SSL_CTX_set0_security_ex_data, @function
SSL_CTX_set0_security_ex_data:
.LFB1900:
	.cfi_startproc
	endbr64
	movq	320(%rdi), %rax
	movq	%rsi, 504(%rax)
	ret
	.cfi_endproc
.LFE1900:
	.size	SSL_CTX_set0_security_ex_data, .-SSL_CTX_set0_security_ex_data
	.p2align 4
	.globl	SSL_CTX_get0_security_ex_data
	.type	SSL_CTX_get0_security_ex_data, @function
SSL_CTX_get0_security_ex_data:
.LFB1901:
	.cfi_startproc
	endbr64
	movq	320(%rdi), %rax
	movq	504(%rax), %rax
	ret
	.cfi_endproc
.LFE1901:
	.size	SSL_CTX_get0_security_ex_data, .-SSL_CTX_get0_security_ex_data
	.p2align 4
	.globl	SSL_CTX_get_options
	.type	SSL_CTX_get_options, @function
SSL_CTX_get_options:
.LFB1902:
	.cfi_startproc
	endbr64
	movl	296(%rdi), %eax
	ret
	.cfi_endproc
.LFE1902:
	.size	SSL_CTX_get_options, .-SSL_CTX_get_options
	.p2align 4
	.globl	SSL_get_options
	.type	SSL_get_options, @function
SSL_get_options:
.LFB1903:
	.cfi_startproc
	endbr64
	movl	1492(%rdi), %eax
	ret
	.cfi_endproc
.LFE1903:
	.size	SSL_get_options, .-SSL_get_options
	.p2align 4
	.globl	SSL_CTX_set_options
	.type	SSL_CTX_set_options, @function
SSL_CTX_set_options:
.LFB1904:
	.cfi_startproc
	endbr64
	orl	296(%rdi), %esi
	movl	%esi, 296(%rdi)
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE1904:
	.size	SSL_CTX_set_options, .-SSL_CTX_set_options
	.p2align 4
	.globl	SSL_set_options
	.type	SSL_set_options, @function
SSL_set_options:
.LFB1905:
	.cfi_startproc
	endbr64
	orl	1492(%rdi), %esi
	movl	%esi, 1492(%rdi)
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE1905:
	.size	SSL_set_options, .-SSL_set_options
	.p2align 4
	.globl	SSL_CTX_clear_options
	.type	SSL_CTX_clear_options, @function
SSL_CTX_clear_options:
.LFB1906:
	.cfi_startproc
	endbr64
	notl	%esi
	andl	296(%rdi), %esi
	movl	%esi, 296(%rdi)
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE1906:
	.size	SSL_CTX_clear_options, .-SSL_CTX_clear_options
	.p2align 4
	.globl	SSL_clear_options
	.type	SSL_clear_options, @function
SSL_clear_options:
.LFB1907:
	.cfi_startproc
	endbr64
	notl	%esi
	andl	1492(%rdi), %esi
	movl	%esi, 1492(%rdi)
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE1907:
	.size	SSL_clear_options, .-SSL_clear_options
	.p2align 4
	.globl	SSL_get0_verified_chain
	.type	SSL_get0_verified_chain, @function
SSL_get0_verified_chain:
.LFB1908:
	.cfi_startproc
	endbr64
	movq	1448(%rdi), %rax
	ret
	.cfi_endproc
.LFE1908:
	.size	SSL_get0_verified_chain, .-SSL_get0_verified_chain
	.p2align 4
	.globl	OBJ_bsearch_ssl_cipher_id
	.type	OBJ_bsearch_ssl_cipher_id, @function
OBJ_bsearch_ssl_cipher_id:
.LFB1910:
	.cfi_startproc
	endbr64
	leaq	ssl_cipher_id_cmp_BSEARCH_CMP_FN(%rip), %r8
	movl	$80, %ecx
	jmp	OBJ_bsearch_@PLT
	.cfi_endproc
.LFE1910:
	.size	OBJ_bsearch_ssl_cipher_id, .-OBJ_bsearch_ssl_cipher_id
	.p2align 4
	.globl	SSL_get0_peer_scts
	.type	SSL_get0_peer_scts, @function
SSL_get0_peer_scts:
.LFB1915:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movl	1896(%rdi), %r15d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r15d, %r15d
	je	.L2187
.L2155:
	movq	1888(%rbx), %rax
.L2154:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2188
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2187:
	.cfi_restore_state
	movq	1616(%rdi), %rax
	testq	%rax, %rax
	je	.L2159
	movzwl	1624(%rdi), %edx
	leaq	-64(%rbp), %rsi
	xorl	%edi, %edi
	movq	%rax, -64(%rbp)
	call	o2i_SCT_LIST@PLT
	leaq	1888(%rbx), %rdi
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%rax, %r12
	call	ct_move_scts
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	SCT_LIST_free@PLT
	testl	%r13d, %r13d
	jns	.L2159
.L2186:
	xorl	%eax, %eax
	jmp	.L2154
	.p2align 4,,10
	.p2align 3
.L2159:
	movq	1648(%rbx), %rax
	testq	%rax, %rax
	je	.L2157
	movq	1656(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.L2189
.L2157:
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
.L2161:
	movq	%r12, %rdi
	call	SCT_LIST_free@PLT
	movq	%r13, %rdi
	call	OCSP_BASICRESP_free@PLT
	movq	%r14, %rdi
	call	OCSP_RESPONSE_free@PLT
	movq	1296(%rbx), %rax
	testq	%rax, %rax
	je	.L2168
	movq	440(%rax), %rdi
	testq	%rdi, %rdi
	je	.L2168
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$951, %esi
	call	X509_get_ext_d2i@PLT
	leaq	1888(%rbx), %rdi
	movl	$2, %edx
	movq	%rax, %rsi
	movq	%rax, %r12
	call	ct_move_scts
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	SCT_LIST_free@PLT
	testl	%r13d, %r13d
	js	.L2186
.L2168:
	movl	$1, 1896(%rbx)
	jmp	.L2155
	.p2align 4,,10
	.p2align 3
.L2189:
	movslq	%edx, %rdx
	leaq	-64(%rbp), %rsi
	xorl	%edi, %edi
	movq	%rax, -64(%rbp)
	call	d2i_OCSP_RESPONSE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2157
	movq	%rax, %rdi
	xorl	%r12d, %r12d
	call	OCSP_response_get1_basic@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2161
	leaq	1888(%rbx), %rax
	movq	%rax, -72(%rbp)
	jmp	.L2162
	.p2align 4,,10
	.p2align 3
.L2163:
	addl	$1, %r15d
.L2162:
	movq	%r13, %rdi
	call	OCSP_resp_count@PLT
	cmpl	%eax, %r15d
	jge	.L2161
	movq	%r13, %rdi
	movl	%r15d, %esi
	call	OCSP_resp_get0@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2163
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	$954, %esi
	call	OCSP_SINGLERESP_get1_ext_d2i@PLT
	movq	-72(%rbp), %rdi
	movl	$3, %edx
	movq	%rax, %rsi
	movq	%rax, %r12
	call	ct_move_scts
	testl	%eax, %eax
	jns	.L2163
	movq	%r12, %rdi
	call	SCT_LIST_free@PLT
	movq	%r13, %rdi
	call	OCSP_BASICRESP_free@PLT
	movq	%r14, %rdi
	call	OCSP_RESPONSE_free@PLT
	xorl	%eax, %eax
	jmp	.L2154
.L2188:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1915:
	.size	SSL_get0_peer_scts, .-SSL_get0_peer_scts
	.p2align 4
	.globl	SSL_set_ct_validation_callback
	.type	SSL_set_ct_validation_callback, @function
SSL_set_ct_validation_callback:
.LFB1918:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	je	.L2191
	movq	1440(%rdi), %rdi
	movl	$18, %esi
	call	SSL_CTX_has_client_custom_ext@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.L2201
	movq	8(%rbx), %rax
	xorl	%ecx, %ecx
	movl	$1, %edx
	movl	$65, %esi
	movq	%rbx, %rdi
	call	*128(%rax)
	testq	%rax, %rax
	je	.L2190
.L2191:
	movq	%r12, 1872(%rbx)
	movl	$1, %r13d
	movq	%r14, 1880(%rbx)
.L2190:
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2201:
	.cfi_restore_state
	xorl	%r13d, %r13d
	movl	$4865, %r8d
	movl	$206, %edx
	movl	$399, %esi
	leaq	.LC0(%rip), %rcx
	movl	$20, %edi
	call	ERR_put_error@PLT
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1918:
	.size	SSL_set_ct_validation_callback, .-SSL_set_ct_validation_callback
	.p2align 4
	.globl	SSL_CTX_set_ct_validation_callback
	.type	SSL_CTX_set_ct_validation_callback, @function
SSL_CTX_set_ct_validation_callback:
.LFB1919:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L2203
	movl	$18, %esi
	call	SSL_CTX_has_client_custom_ext@PLT
	testl	%eax, %eax
	jne	.L2212
.L2203:
	movq	%r12, 440(%rbx)
	movl	$1, %eax
	movq	%r13, 448(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2212:
	.cfi_restore_state
	movl	$4894, %r8d
	movl	$206, %edx
	movl	$396, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1919:
	.size	SSL_CTX_set_ct_validation_callback, .-SSL_CTX_set_ct_validation_callback
	.p2align 4
	.globl	SSL_ct_is_enabled
	.type	SSL_ct_is_enabled, @function
SSL_ct_is_enabled:
.LFB1920:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	$0, 1872(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE1920:
	.size	SSL_ct_is_enabled, .-SSL_ct_is_enabled
	.p2align 4
	.globl	SSL_CTX_ct_is_enabled
	.type	SSL_CTX_ct_is_enabled, @function
SSL_CTX_ct_is_enabled:
.LFB1921:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	$0, 440(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE1921:
	.size	SSL_CTX_ct_is_enabled, .-SSL_CTX_ct_is_enabled
	.p2align 4
	.globl	ssl_validate_ct
	.type	ssl_validate_ct, @function
ssl_validate_ct:
.LFB1922:
	.cfi_startproc
	endbr64
	movq	1296(%rdi), %rax
	testq	%rax, %rax
	je	.L2234
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	cmpq	$0, 1872(%rdi)
	movq	440(%rax), %r13
	je	.L2218
	testq	%r13, %r13
	je	.L2218
	cmpq	$0, 1456(%rdi)
	jne	.L2218
	movq	1448(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2218
	call	OPENSSL_sk_num@PLT
	cmpl	$1, %eax
	jle	.L2218
	movq	224(%rbx), %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jle	.L2219
	movq	240(%rbx), %rax
	testq	%rax, %rax
	je	.L2219
	movzbl	(%rax), %eax
	subl	$2, %eax
	cmpl	$1, %eax
	jbe	.L2218
.L2219:
	call	CT_POLICY_EVAL_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L2235
	movq	1448(%rbx), %rdi
	movl	$1, %esi
	call	OPENSSL_sk_value@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	CT_POLICY_EVAL_CTX_set1_cert@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	CT_POLICY_EVAL_CTX_set1_issuer@PLT
	movq	1440(%rbx), %rax
	movq	%r12, %rdi
	movq	432(%rax), %rsi
	call	CT_POLICY_EVAL_CTX_set_shared_CTLOG_STORE@PLT
	movq	%rbx, %rdi
	call	SSL_get_session@PLT
	movq	%rax, %rdi
	call	SSL_SESSION_get_time@PLT
	movq	%r12, %rdi
	imulq	$1000, %rax, %rsi
	call	CT_POLICY_EVAL_CTX_set_time@PLT
	movq	%rbx, %rdi
	call	SSL_get0_peer_scts
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	SCT_LIST_validate@PLT
	movl	$4982, %r9d
	movl	$208, %ecx
	leaq	.LC0(%rip), %r8
	testl	%eax, %eax
	js	.L2233
	movq	1880(%rbx), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*1872(%rbx)
	testl	%eax, %eax
	jg	.L2223
	movl	$4991, %r9d
	leaq	.LC0(%rip), %r8
	movl	$234, %ecx
.L2233:
	movq	%rbx, %rdi
	movl	$400, %edx
	movl	$40, %esi
	call	ossl_statem_fatal@PLT
	movq	%r12, %rdi
	call	CT_POLICY_EVAL_CTX_free@PLT
.L2221:
	movq	$71, 1456(%rbx)
	xorl	%eax, %eax
	jmp	.L2215
	.p2align 4,,10
	.p2align 3
.L2218:
	movl	$1, %eax
.L2215:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2234:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2223:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	%r12, %rdi
	movl	%eax, -36(%rbp)
	call	CT_POLICY_EVAL_CTX_free@PLT
	movl	-36(%rbp), %eax
	jmp	.L2215
	.p2align 4,,10
	.p2align 3
.L2235:
	movq	%rbx, %rdi
	movl	$4953, %r9d
	movl	$65, %ecx
	movl	$400, %edx
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%edi, %edi
	call	CT_POLICY_EVAL_CTX_free@PLT
	jmp	.L2221
	.cfi_endproc
.LFE1922:
	.size	ssl_validate_ct, .-ssl_validate_ct
	.p2align 4
	.globl	SSL_CTX_enable_ct
	.type	SSL_CTX_enable_ct, @function
SSL_CTX_enable_ct:
.LFB1923:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	testl	%esi, %esi
	je	.L2237
	cmpl	$1, %esi
	je	.L2238
	movl	$5020, %r8d
	movl	$212, %edx
	movl	$398, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2237:
	.cfi_restore_state
	movl	$18, %esi
	call	SSL_CTX_has_client_custom_ext@PLT
	testl	%eax, %eax
	jne	.L2241
	leaq	ct_permissive(%rip), %rax
	movq	$0, 448(%rbx)
	movq	%rax, 440(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2238:
	.cfi_restore_state
	movl	$18, %esi
	call	SSL_CTX_has_client_custom_ext@PLT
	testl	%eax, %eax
	jne	.L2241
	leaq	ct_strict(%rip), %rax
	movq	$0, 448(%rbx)
	movq	%rax, 440(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2241:
	.cfi_restore_state
	movl	$4894, %r8d
	movl	$206, %edx
	movl	$396, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1923:
	.size	SSL_CTX_enable_ct, .-SSL_CTX_enable_ct
	.p2align 4
	.globl	SSL_enable_ct
	.type	SSL_enable_ct, @function
SSL_enable_ct:
.LFB1924:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	testl	%esi, %esi
	je	.L2244
	cmpl	$1, %esi
	je	.L2245
	movl	$5033, %r8d
	movl	$212, %edx
	movl	$402, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2244:
	.cfi_restore_state
	movq	1440(%rdi), %rdi
	movl	$18, %esi
	call	SSL_CTX_has_client_custom_ext@PLT
	testl	%eax, %eax
	jne	.L2249
	xorl	%ecx, %ecx
	movl	$1, %edx
	movl	$65, %esi
	movq	%rbx, %rdi
	call	SSL_ctrl
	testq	%rax, %rax
	je	.L2250
	leaq	ct_permissive(%rip), %rax
	movq	$0, 1880(%rbx)
	movq	%rax, 1872(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2245:
	.cfi_restore_state
	movq	1440(%rdi), %rdi
	movl	$18, %esi
	call	SSL_CTX_has_client_custom_ext@PLT
	testl	%eax, %eax
	jne	.L2249
	xorl	%ecx, %ecx
	movl	$1, %edx
	movl	$65, %esi
	movq	%rbx, %rdi
	call	SSL_ctrl
	testq	%rax, %rax
	je	.L2250
	leaq	ct_strict(%rip), %rax
	movq	$0, 1880(%rbx)
	movq	%rax, 1872(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2250:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2249:
	.cfi_restore_state
	movl	$4865, %r8d
	movl	$206, %edx
	movl	$399, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1924:
	.size	SSL_enable_ct, .-SSL_enable_ct
	.p2align 4
	.globl	SSL_CTX_set_default_ctlog_list_file
	.type	SSL_CTX_set_default_ctlog_list_file, @function
SSL_CTX_set_default_ctlog_list_file:
.LFB1925:
	.cfi_startproc
	endbr64
	movq	432(%rdi), %rdi
	jmp	CTLOG_STORE_load_default_file@PLT
	.cfi_endproc
.LFE1925:
	.size	SSL_CTX_set_default_ctlog_list_file, .-SSL_CTX_set_default_ctlog_list_file
	.p2align 4
	.globl	SSL_CTX_set_ctlog_list_file
	.type	SSL_CTX_set_ctlog_list_file, @function
SSL_CTX_set_ctlog_list_file:
.LFB1926:
	.cfi_startproc
	endbr64
	movq	432(%rdi), %rdi
	jmp	CTLOG_STORE_load_file@PLT
	.cfi_endproc
.LFE1926:
	.size	SSL_CTX_set_ctlog_list_file, .-SSL_CTX_set_ctlog_list_file
	.p2align 4
	.globl	SSL_CTX_set0_ctlog_store
	.type	SSL_CTX_set0_ctlog_store, @function
SSL_CTX_set0_ctlog_store:
.LFB1927:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	432(%rdi), %rdi
	call	CTLOG_STORE_free@PLT
	movq	%r12, 432(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1927:
	.size	SSL_CTX_set0_ctlog_store, .-SSL_CTX_set0_ctlog_store
	.p2align 4
	.globl	SSL_CTX_get0_ctlog_store
	.type	SSL_CTX_get0_ctlog_store, @function
SSL_CTX_get0_ctlog_store:
.LFB1928:
	.cfi_startproc
	endbr64
	movq	432(%rdi), %rax
	ret
	.cfi_endproc
.LFE1928:
	.size	SSL_CTX_get0_ctlog_store, .-SSL_CTX_get0_ctlog_store
	.p2align 4
	.globl	SSL_CTX_set_client_hello_cb
	.type	SSL_CTX_set_client_hello_cb, @function
SSL_CTX_set_client_hello_cb:
.LFB1929:
	.cfi_startproc
	endbr64
	movq	%rsi, 496(%rdi)
	movq	%rdx, 504(%rdi)
	ret
	.cfi_endproc
.LFE1929:
	.size	SSL_CTX_set_client_hello_cb, .-SSL_CTX_set_client_hello_cb
	.p2align 4
	.globl	SSL_client_hello_isv2
	.type	SSL_client_hello_isv2, @function
SSL_client_hello_isv2:
.LFB1930:
	.cfi_startproc
	endbr64
	movq	1856(%rdi), %rax
	testq	%rax, %rax
	je	.L2263
	movl	(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2263:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1930:
	.size	SSL_client_hello_isv2, .-SSL_client_hello_isv2
	.p2align 4
	.globl	SSL_client_hello_get0_legacy_version
	.type	SSL_client_hello_get0_legacy_version, @function
SSL_client_hello_get0_legacy_version:
.LFB1931:
	.cfi_startproc
	endbr64
	movq	1856(%rdi), %rax
	testq	%rax, %rax
	je	.L2266
	movl	4(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2266:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1931:
	.size	SSL_client_hello_get0_legacy_version, .-SSL_client_hello_get0_legacy_version
	.p2align 4
	.globl	SSL_client_hello_get0_random
	.type	SSL_client_hello_get0_random, @function
SSL_client_hello_get0_random:
.LFB1932:
	.cfi_startproc
	endbr64
	movq	1856(%rdi), %rax
	testq	%rax, %rax
	je	.L2269
	movl	$32, %r8d
	testq	%rsi, %rsi
	je	.L2267
	addq	$8, %rax
	movq	%rax, (%rsi)
.L2267:
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L2269:
	xorl	%r8d, %r8d
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE1932:
	.size	SSL_client_hello_get0_random, .-SSL_client_hello_get0_random
	.p2align 4
	.globl	SSL_client_hello_get0_session_id
	.type	SSL_client_hello_get0_session_id, @function
SSL_client_hello_get0_session_id:
.LFB1933:
	.cfi_startproc
	endbr64
	movq	1856(%rdi), %rax
	testq	%rax, %rax
	je	.L2275
	testq	%rsi, %rsi
	je	.L2274
	leaq	48(%rax), %rdx
	movq	%rdx, (%rsi)
.L2274:
	movq	40(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L2275:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1933:
	.size	SSL_client_hello_get0_session_id, .-SSL_client_hello_get0_session_id
	.p2align 4
	.globl	SSL_client_hello_get0_ciphers
	.type	SSL_client_hello_get0_ciphers, @function
SSL_client_hello_get0_ciphers:
.LFB1934:
	.cfi_startproc
	endbr64
	movq	1856(%rdi), %rax
	testq	%rax, %rax
	je	.L2282
	testq	%rsi, %rsi
	je	.L2281
	movq	344(%rax), %rdx
	movq	%rdx, (%rsi)
.L2281:
	movq	352(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L2282:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1934:
	.size	SSL_client_hello_get0_ciphers, .-SSL_client_hello_get0_ciphers
	.p2align 4
	.globl	SSL_client_hello_get0_compression_methods
	.type	SSL_client_hello_get0_compression_methods, @function
SSL_client_hello_get0_compression_methods:
.LFB1935:
	.cfi_startproc
	endbr64
	movq	1856(%rdi), %rax
	testq	%rax, %rax
	je	.L2289
	testq	%rsi, %rsi
	je	.L2288
	leaq	368(%rax), %rdx
	movq	%rdx, (%rsi)
.L2288:
	movq	360(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L2289:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1935:
	.size	SSL_client_hello_get0_compression_methods, .-SSL_client_hello_get0_compression_methods
	.p2align 4
	.globl	SSL_client_hello_get1_extensions_present
	.type	SSL_client_hello_get1_extensions_present, @function
SSL_client_hello_get1_extensions_present:
.LFB1936:
	.cfi_startproc
	endbr64
	movq	1856(%rdi), %rax
	testq	%rax, %rax
	je	.L2305
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	testq	%rsi, %rsi
	je	.L2306
	movq	%rdx, %r14
	testq	%rdx, %rdx
	je	.L2306
	movq	640(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2295
	movq	648(%rax), %rcx
	leaq	(%rdx,%rdx,4), %rdx
	movq	%rdi, %r13
	xorl	%ebx, %ebx
	leaq	16(%rcx), %rax
	leaq	16(%rcx,%rdx,8), %rdx
	.p2align 4,,10
	.p2align 3
.L2297:
	cmpl	$1, (%rax)
	sbbq	$-1, %rbx
	addq	$40, %rax
	cmpq	%rdx, %rax
	jne	.L2297
	testq	%rbx, %rbx
	je	.L2295
	leaq	0(,%rbx,4), %rdi
	movl	$5140, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L2299
	movq	1856(%r13), %rax
	movq	640(%rax), %rdi
	testq	%rdi, %rdi
	je	.L2301
	movq	648(%rax), %rdx
	xorl	%ecx, %ecx
	addq	$16, %rdx
	.p2align 4,,10
	.p2align 3
.L2304:
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L2302
	movq	16(%rdx), %rsi
	cmpq	%rbx, %rsi
	jnb	.L2316
	movl	8(%rdx), %r8d
	movl	%r8d, (%r9,%rsi,4)
.L2302:
	addq	$1, %rcx
	addq	$40, %rdx
	cmpq	%rdi, %rcx
	jne	.L2304
.L2301:
	movq	%r9, (%r12)
	movl	$1, %eax
	movq	%rbx, (%r14)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2295:
	.cfi_restore_state
	movq	$0, (%r12)
	movl	$1, %eax
	movq	$0, (%r14)
.L2293:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2306:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2305:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2316:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movl	$5157, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r9, %rdi
	call	CRYPTO_free@PLT
	xorl	%eax, %eax
	jmp	.L2293
.L2299:
	movl	$5141, %r8d
	movl	$65, %edx
	movl	$627, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L2293
	.cfi_endproc
.LFE1936:
	.size	SSL_client_hello_get1_extensions_present, .-SSL_client_hello_get1_extensions_present
	.p2align 4
	.globl	SSL_client_hello_get0_ext
	.type	SSL_client_hello_get0_ext, @function
SSL_client_hello_get0_ext:
.LFB1937:
	.cfi_startproc
	endbr64
	movq	1856(%rdi), %rax
	testq	%rax, %rax
	je	.L2323
	movq	640(%rax), %r8
	testq	%r8, %r8
	je	.L2323
	movq	648(%rax), %rax
	xorl	%edi, %edi
	.p2align 4,,10
	.p2align 3
.L2321:
	movl	16(%rax), %r9d
	testl	%r9d, %r9d
	je	.L2319
	cmpl	%esi, 24(%rax)
	je	.L2330
.L2319:
	addq	$1, %rdi
	addq	$40, %rax
	cmpq	%r8, %rdi
	jne	.L2321
.L2323:
	xorl	%r8d, %r8d
.L2317:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2330:
	testq	%rdx, %rdx
	je	.L2320
	movq	(%rax), %rsi
	movq	%rsi, (%rdx)
.L2320:
	movl	$1, %r8d
	testq	%rcx, %rcx
	je	.L2317
	movq	8(%rax), %rax
	movq	%rax, (%rcx)
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE1937:
	.size	SSL_client_hello_get0_ext, .-SSL_client_hello_get0_ext
	.p2align 4
	.globl	SSL_free_buffers
	.type	SSL_free_buffers, @function
SSL_free_buffers:
.LFB1938:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	2112(%rdi), %r12
	movq	%r12, %rdi
	subq	$8, %rsp
	call	RECORD_LAYER_read_pending@PLT
	testl	%eax, %eax
	je	.L2332
.L2334:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2332:
	.cfi_restore_state
	movq	%r12, %rdi
	call	RECORD_LAYER_write_pending@PLT
	testl	%eax, %eax
	jne	.L2334
	movq	%r12, %rdi
	call	RECORD_LAYER_release@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1938:
	.size	SSL_free_buffers, .-SSL_free_buffers
	.p2align 4
	.globl	SSL_alloc_buffers
	.type	SSL_alloc_buffers, @function
SSL_alloc_buffers:
.LFB1939:
	.cfi_startproc
	endbr64
	jmp	ssl3_setup_buffers@PLT
	.cfi_endproc
.LFE1939:
	.size	SSL_alloc_buffers, .-SSL_alloc_buffers
	.p2align 4
	.globl	SSL_CTX_set_keylog_callback
	.type	SSL_CTX_set_keylog_callback, @function
SSL_CTX_set_keylog_callback:
.LFB1940:
	.cfi_startproc
	endbr64
	movq	%rsi, 928(%rdi)
	ret
	.cfi_endproc
.LFE1940:
	.size	SSL_CTX_set_keylog_callback, .-SSL_CTX_set_keylog_callback
	.p2align 4
	.globl	SSL_CTX_get_keylog_callback
	.type	SSL_CTX_get_keylog_callback, @function
SSL_CTX_get_keylog_callback:
.LFB1941:
	.cfi_startproc
	endbr64
	movq	928(%rdi), %rax
	ret
	.cfi_endproc
.LFE1941:
	.size	SSL_CTX_get_keylog_callback, .-SSL_CTX_get_keylog_callback
	.section	.rodata.str1.1
.LC15:
	.string	"%02x"
	.text
	.p2align 4
	.globl	ssl_log_rsa_client_key_exchange
	.type	ssl_log_rsa_client_key_exchange, @function
ssl_log_rsa_client_key_exchange:
.LFB1943:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -64(%rbp)
	cmpq	$7, %rdx
	jbe	.L2353
	movq	1440(%rdi), %rdx
	movl	$1, %eax
	cmpq	$0, 928(%rdx)
	je	.L2339
	leaq	16(%r8,%r8), %rax
	movq	%rsi, %rbx
	movl	$5234, %edx
	movq	%r8, %r12
	movq	%rax, -80(%rbp)
	addq	$6, %rax
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -72(%rbp)
	call	CRYPTO_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2354
	leaq	4(%rax), %rdi
	leaq	20(%rax), %rax
	movl	$4281170, -20(%rax)
	movb	$32, -17(%rax)
	leaq	.LC15(%rip), %r15
	movq	%rax, -56(%rbp)
	movq	%r12, -88(%rbp)
	movq	%rdi, %r12
	.p2align 4,,10
	.p2align 3
.L2343:
	movzbl	(%rbx), %r8d
	movq	%r12, %rdi
	movq	%r15, %rcx
	movl	$1, %esi
	movq	$-1, %rdx
	xorl	%eax, %eax
	addq	$2, %r12
	addq	$1, %rbx
	call	__sprintf_chk@PLT
	cmpq	-56(%rbp), %r12
	jne	.L2343
	movq	-88(%rbp), %r12
	movb	$32, 20(%r14)
	leaq	21(%r14), %r15
	testq	%r12, %r12
	je	.L2344
	movq	-64(%rbp), %rbx
	movq	%r13, -64(%rbp)
	leaq	(%rbx,%r12), %rax
	movq	%rbx, %r13
	leaq	.LC15(%rip), %r12
	movq	%r15, %rbx
	movq	%rax, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L2345:
	movzbl	0(%r13), %r8d
	movq	%rbx, %rdi
	movq	%r12, %rcx
	movl	$1, %esi
	movq	$-1, %rdx
	xorl	%eax, %eax
	addq	$2, %rbx
	addq	$1, %r13
	call	__sprintf_chk@PLT
	cmpq	-56(%rbp), %r13
	jne	.L2345
	movq	-80(%rbp), %rax
	movq	-64(%rbp), %r13
	leaq	-16(%r15,%rax), %r15
.L2344:
	movb	$0, (%r15)
	movq	1440(%r13), %rax
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	*928(%rax)
	movq	-72(%rbp), %rsi
	movl	$5257, %ecx
	movq	%r14, %rdi
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movl	$1, %eax
.L2339:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2353:
	.cfi_restore_state
	movl	$5269, %r9d
	movl	$68, %ecx
	movl	$499, %edx
	movl	$80, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	addq	$56, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2354:
	.cfi_restore_state
	movl	$5235, %r9d
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
	movq	%r13, %rdi
	movl	$500, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L2339
	.cfi_endproc
.LFE1943:
	.size	ssl_log_rsa_client_key_exchange, .-ssl_log_rsa_client_key_exchange
	.p2align 4
	.globl	ssl_log_secret
	.type	ssl_log_secret, @function
ssl_log_secret:
.LFB1944:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -64(%rbp)
	movq	1440(%rdi), %rdx
	cmpq	$0, 928(%rdx)
	je	.L2355
	movq	168(%rdi), %rax
	movq	%rdi, %r13
	movq	%rsi, %rdi
	movq	%rcx, %r12
	movq	%rsi, %r15
	movq	%rax, -56(%rbp)
	call	strlen@PLT
	movl	$5234, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rbx
	leaq	64(%r12,%r12), %rax
	movq	%rax, -80(%rbp)
	leaq	3(%rax,%rbx), %rax
	movq	%rax, %rdi
	movq	%rax, -72(%rbp)
	call	CRYPTO_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2368
	addq	%r14, %rbx
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	strcpy@PLT
	leaq	65(%rbx), %rax
	movq	-56(%rbp), %r15
	movb	$32, (%rbx)
	movq	%rax, -56(%rbp)
	leaq	1(%rbx), %rdi
	leaq	.LC15(%rip), %rcx
	movq	%rbx, -88(%rbp)
	addq	$184, %r15
	movq	%rdi, %rbx
	.p2align 4,,10
	.p2align 3
.L2358:
	movzbl	(%r15), %r8d
	movq	%rbx, %rdi
	xorl	%eax, %eax
	movq	$-1, %rdx
	movl	$1, %esi
	addq	$2, %rbx
	addq	$1, %r15
	call	__sprintf_chk@PLT
	cmpq	-56(%rbp), %rbx
	leaq	.LC15(%rip), %rcx
	jne	.L2358
	movq	-88(%rbp), %rbx
	movb	$32, 65(%rbx)
	leaq	66(%rbx), %r15
	testq	%r12, %r12
	je	.L2359
	movq	-64(%rbp), %rbx
	movq	%r13, -64(%rbp)
	movq	%r15, %r13
	leaq	(%rbx,%r12), %rax
	movq	%rcx, %r12
	movq	%rax, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L2360:
	movzbl	(%rbx), %r8d
	movq	%r13, %rdi
	movq	%r12, %rcx
	movl	$1, %esi
	movq	$-1, %rdx
	xorl	%eax, %eax
	addq	$2, %r13
	addq	$1, %rbx
	call	__sprintf_chk@PLT
	cmpq	-56(%rbp), %rbx
	jne	.L2360
	movq	-80(%rbp), %rax
	movq	-64(%rbp), %r13
	leaq	-64(%r15,%rax), %r15
.L2359:
	movb	$0, (%r15)
	movq	1440(%r13), %rax
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	*928(%rax)
	movq	-72(%rbp), %rsi
	movl	$5257, %ecx
	movq	%r14, %rdi
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movl	$1, %eax
.L2355:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2368:
	.cfi_restore_state
	movq	%r13, %rdi
	movl	$5235, %r9d
	movl	$65, %ecx
	movl	$500, %edx
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	addq	$56, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1944:
	.size	ssl_log_secret, .-ssl_log_secret
	.section	.rodata.str1.8
	.align 8
.LC16:
	.string	"../deps/openssl/openssl/ssl/packet_local.h"
	.text
	.p2align 4
	.globl	ssl_cache_cipherlist
	.type	ssl_cache_cipherlist, @function
ssl_cache_cipherlist:
.LFB1945:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$8, %rsp
	testl	%edx, %edx
	movq	8(%rsi), %rax
	setne	%bl
	addq	$2, %rbx
	testq	%rax, %rax
	je	.L2396
	movl	%edx, %r14d
	xorl	%edx, %edx
	divq	%rbx
	testq	%rdx, %rdx
	jne	.L2397
	movq	168(%rdi), %rax
	movq	%rsi, %r13
	movl	$5316, %edx
	leaq	.LC0(%rip), %rsi
	movq	680(%rax), %rdi
	call	CRYPTO_free@PLT
	movq	168(%r12), %r15
	movq	$0, 680(%r15)
	movq	$0, 688(%r15)
	testl	%r14d, %r14d
	je	.L2374
	movq	8(%r13), %r14
	xorl	%edx, %edx
	leaq	.LC0(%rip), %rsi
	movq	0(%r13), %r13
	movq	%r14, %rax
	divq	%rbx
	movl	$5333, %edx
	leaq	(%rax,%rax), %rdi
	call	CRYPTO_malloc@PLT
	movq	168(%r12), %rdx
	movq	%rax, 680(%rdx)
	testq	%rax, %rax
	je	.L2398
	movq	$0, 688(%rdx)
	leaq	2(%r14), %rcx
	leaq	3(%r13), %rdx
	addq	%r13, %rcx
	addq	%r14, %r13
	movq	%r13, %r8
	testq	%r14, %r14
	je	.L2395
.L2382:
	movq	%rcx, %rsi
	subq	%rdx, %rsi
	cmpb	$0, -3(%rdx)
	je	.L2399
	cmpq	$1, %rsi
	jbe	.L2380
	movq	%r13, %rsi
	subq	%rdx, %rsi
	jmp	.L2381
	.p2align 4,,10
	.p2align 3
.L2374:
	movl	$420, %edx
	leaq	.LC16(%rip), %rsi
	xorl	%edi, %edi
	call	CRYPTO_free@PLT
	movq	$0, 688(%r15)
	movq	8(%r13), %rbx
	movq	$0, 680(%r15)
	testq	%rbx, %rbx
	jne	.L2400
.L2395:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2399:
	.cfi_restore_state
	cmpq	$1, %rsi
	ja	.L2401
.L2380:
	movl	$5349, %r9d
	leaq	.LC0(%rip), %r8
	movl	$240, %ecx
	movq	%r12, %rdi
	movl	$520, %edx
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	movq	168(%r12), %rax
	movl	$5351, %edx
	leaq	.LC0(%rip), %rsi
	movq	680(%rax), %rdi
	call	CRYPTO_free@PLT
	movq	168(%r12), %rax
	movq	$0, 680(%rax)
	movq	$0, 688(%rax)
	xorl	%eax, %eax
.L2402:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2397:
	.cfi_restore_state
	movl	$5311, %r9d
	movl	$151, %ecx
	movl	$520, %edx
	movl	$50, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2396:
	.cfi_restore_state
	movl	$5305, %r9d
	movl	$183, %ecx
	movl	$520, %edx
	movl	$47, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2401:
	.cfi_restore_state
	movzwl	-2(%rdx), %esi
	movw	%si, (%rax)
	movq	168(%r12), %rdi
	movq	%r8, %rsi
	subq	%rdx, %rsi
	addq	$2, 688(%rdi)
.L2381:
	addq	$2, %rax
	addq	$3, %rdx
	testq	%rsi, %rsi
	jne	.L2382
	jmp	.L2395
	.p2align 4,,10
	.p2align 3
.L2400:
	movq	0(%r13), %rdi
	movl	$429, %ecx
	leaq	.LC16(%rip), %rdx
	movq	%rbx, %rsi
	call	CRYPTO_memdup@PLT
	movq	%rax, 680(%r15)
	testq	%rax, %rax
	je	.L2384
	movq	%rbx, 688(%r15)
	jmp	.L2395
	.p2align 4,,10
	.p2align 3
.L2384:
	movl	$5361, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$520, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L2402
	.p2align 4,,10
	.p2align 3
.L2398:
	movl	$5336, %r9d
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
	movq	%r12, %rdi
	movl	$520, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L2402
	.cfi_endproc
.LFE1945:
	.size	ssl_cache_cipherlist, .-ssl_cache_cipherlist
	.p2align 4
	.globl	SSL_bytes_to_cipher_list
	.type	SSL_bytes_to_cipher_list, @function
SSL_bytes_to_cipher_list:
.LFB1946:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	js	.L2403
	xorl	%r12d, %r12d
	testl	%ecx, %ecx
	movq	%rdx, %rbx
	movl	%ecx, %r13d
	setne	%r12b
	addq	$2, %r12
	testq	%rdx, %rdx
	je	.L2455
	movq	%rdx, %rax
	xorl	%edx, %edx
	divq	%r12
	testq	%rdx, %rdx
	je	.L2408
	movl	$5407, %r8d
	movl	$151, %edx
	movl	$519, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L2403:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2456
	addq	$88, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2455:
	.cfi_restore_state
	movl	$5398, %r8d
	movl	$183, %edx
	movl	$519, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L2403
	.p2align 4,,10
	.p2align 3
.L2408:
	movq	%r9, -96(%rbp)
	movq	%r8, -88(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %r14
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %r15
	testq	%r14, %r14
	je	.L2409
	testq	%rax, %rax
	je	.L2409
	cmpq	%r12, %rbx
	movq	-72(%rbp), %r10
	movq	-80(%rbp), %rsi
	movq	-88(%rbp), %r8
	movq	-96(%rbp), %r9
	jnb	.L2410
.L2428:
	movl	$5451, %r8d
	movl	$271, %edx
	movl	$519, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L2411:
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	OPENSSL_sk_free@PLT
	movq	%r15, %rdi
	call	OPENSSL_sk_free@PLT
	jmp	.L2403
	.p2align 4,,10
	.p2align 3
.L2409:
	movl	$5419, %r8d
.L2454:
	movl	$65, %edx
	movl	$519, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L2411
	.p2align 4,,10
	.p2align 3
.L2410:
	testl	%r13d, %r13d
	leaq	-59(%rbp), %r11
	leaq	-58(%rbp), %rax
	movq	%rsi, %r13
	cmove	%r11, %rax
	movl	%r12d, %ecx
	movq	%rax, -72(%rbp)
	jne	.L2413
.L2420:
	testl	%r12d, %r12d
	je	.L2415
	xorl	%eax, %eax
.L2414:
	movl	%eax, %edx
	addl	$1, %eax
	movzbl	0(%r13,%rdx), %esi
	movb	%sil, (%r11,%rdx)
	cmpl	%ecx, %eax
	jb	.L2414
.L2415:
	movq	%r11, %rsi
	movq	%r10, %rdi
	movl	$1, %edx
	movl	%ecx, -104(%rbp)
	movq	%r9, -96(%rbp)
	subq	%r12, %rbx
	movq	%r8, -88(%rbp)
	movq	%r11, -80(%rbp)
	movq	%r10, -72(%rbp)
	call	ssl_get_cipher_by_char@PLT
	movq	-72(%rbp), %r10
	movq	-80(%rbp), %r11
	testq	%rax, %rax
	movq	-88(%rbp), %r8
	movq	-96(%rbp), %r9
	movq	%rax, %rsi
	movl	-104(%rbp), %ecx
	je	.L2416
	movl	(%rax), %edi
	testl	%edi, %edi
	jne	.L2457
.L2417:
	movq	%r15, %rdi
	movq	%r11, -104(%rbp)
	movl	%ecx, -96(%rbp)
	movq	%r9, -88(%rbp)
	movq	%r8, -80(%rbp)
	movq	%r10, -72(%rbp)
	call	OPENSSL_sk_push@PLT
	movq	-72(%rbp), %r10
	movq	-80(%rbp), %r8
	testl	%eax, %eax
	movq	-88(%rbp), %r9
	movl	-96(%rbp), %ecx
	movq	-104(%rbp), %r11
	je	.L2418
.L2416:
	addq	%r12, %r13
	cmpq	%rbx, %r12
	jbe	.L2420
.L2419:
	testq	%rbx, %rbx
	jne	.L2428
	testq	%r8, %r8
	je	.L2429
	movq	%r14, (%r8)
.L2430:
	testq	%r9, %r9
	je	.L2431
	movq	%r15, (%r9)
	movl	$1, %r14d
	jmp	.L2403
	.p2align 4,,10
	.p2align 3
.L2426:
	subq	%r12, %rbx
	cmpb	$0, -59(%rbp)
	jne	.L2421
	movq	-72(%rbp), %rsi
	movq	%r10, %rdi
	movl	$1, %edx
	movq	%r11, -112(%rbp)
	movl	%ecx, -104(%rbp)
	movq	%r9, -96(%rbp)
	movq	%r8, -88(%rbp)
	movq	%r10, -80(%rbp)
	call	ssl_get_cipher_by_char@PLT
	movq	-80(%rbp), %r10
	movq	-88(%rbp), %r8
	testq	%rax, %rax
	movq	-96(%rbp), %r9
	movl	-104(%rbp), %ecx
	movq	%rax, %rsi
	movq	-112(%rbp), %r11
	je	.L2421
	movl	(%rax), %edx
	testl	%edx, %edx
	jne	.L2422
.L2424:
	movq	%r15, %rdi
	movq	%r11, -112(%rbp)
	movl	%ecx, -104(%rbp)
	movq	%r9, -96(%rbp)
	movq	%r8, -88(%rbp)
	movq	%r10, -80(%rbp)
	call	OPENSSL_sk_push@PLT
	movq	-80(%rbp), %r10
	movq	-88(%rbp), %r8
	testl	%eax, %eax
	movq	-96(%rbp), %r9
	movl	-104(%rbp), %ecx
	movq	-112(%rbp), %r11
	je	.L2418
.L2421:
	addq	%r12, %r13
	cmpq	%r12, %rbx
	jb	.L2419
.L2413:
	testl	%r12d, %r12d
	je	.L2426
	xorl	%eax, %eax
.L2425:
	movl	%eax, %edx
	addl	$1, %eax
	movzbl	0(%r13,%rdx), %esi
	movb	%sil, (%r11,%rdx)
	cmpl	%ecx, %eax
	jb	.L2425
	jmp	.L2426
	.p2align 4,,10
	.p2align 3
.L2431:
	movq	%r15, %rdi
	movl	$1, %r14d
	call	OPENSSL_sk_free@PLT
	jmp	.L2403
	.p2align 4,,10
	.p2align 3
.L2429:
	movq	%r14, %rdi
	movq	%r9, -72(%rbp)
	call	OPENSSL_sk_free@PLT
	movq	-72(%rbp), %r9
	jmp	.L2430
	.p2align 4,,10
	.p2align 3
.L2422:
	movq	%r14, %rdi
	movq	%r11, -120(%rbp)
	movl	%ecx, -112(%rbp)
	movq	%r9, -104(%rbp)
	movq	%r8, -96(%rbp)
	movq	%r10, -88(%rbp)
	movq	%rax, -80(%rbp)
	call	OPENSSL_sk_push@PLT
	movq	-80(%rbp), %rsi
	movq	-88(%rbp), %r10
	testl	%eax, %eax
	movq	-96(%rbp), %r8
	movq	-104(%rbp), %r9
	movl	-112(%rbp), %ecx
	movq	-120(%rbp), %r11
	je	.L2418
	movl	(%rsi), %eax
	testl	%eax, %eax
	jne	.L2421
	jmp	.L2424
	.p2align 4,,10
	.p2align 3
.L2457:
	movq	%r14, %rdi
	movq	%r11, -112(%rbp)
	movq	%r10, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L2418
	movq	-72(%rbp), %rsi
	movq	-80(%rbp), %r10
	movq	-88(%rbp), %r8
	movq	-96(%rbp), %r9
	movl	(%rsi), %ecx
	movq	-112(%rbp), %r11
	testl	%ecx, %ecx
	movl	-104(%rbp), %ecx
	jne	.L2416
	jmp	.L2417
	.p2align 4,,10
	.p2align 3
.L2418:
	movl	$5441, %r8d
	jmp	.L2454
.L2456:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1946:
	.size	SSL_bytes_to_cipher_list, .-SSL_bytes_to_cipher_list
	.p2align 4
	.globl	bytes_to_cipher_list
	.type	bytes_to_cipher_list, @function
bytes_to_cipher_list:
.LFB1947:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%r9d, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	movq	8(%rsi), %rax
	setne	%bl
	addq	$2, %rbx
	testq	%rax, %rax
	je	.L2519
	movq	%rdx, %r11
	xorl	%edx, %edx
	divq	%rbx
	testq	%rdx, %rdx
	je	.L2463
	testl	%r9d, %r9d
	je	.L2464
	movl	$5404, %r9d
	movl	$151, %ecx
	movl	$519, %edx
	movl	$50, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L2458
	.p2align 4,,10
	.p2align 3
.L2463:
	movl	%r8d, -96(%rbp)
	movq	%rsi, %r12
	movq	%rcx, -88(%rbp)
	movq	%r11, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %r14
	call	OPENSSL_sk_new_null@PLT
	testq	%r14, %r14
	movq	-72(%rbp), %r10
	movq	%rax, %r15
	je	.L2465
	testq	%rax, %rax
	je	.L2465
	movq	8(%r12), %rax
	movq	-80(%rbp), %r11
	movq	-88(%rbp), %rcx
	cmpq	%rax, %rbx
	jbe	.L2520
.L2466:
	testq	%rax, %rax
	jne	.L2521
	testq	%r11, %r11
	je	.L2487
	movq	%r14, (%r11)
.L2488:
	testq	%rcx, %rcx
	je	.L2489
	movq	%r15, (%rcx)
	movl	$1, %eax
	jmp	.L2458
	.p2align 4,,10
	.p2align 3
.L2519:
	testl	%r9d, %r9d
	jne	.L2522
	movl	$5398, %r8d
	movl	$183, %edx
	movl	$519, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L2458:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2523
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2522:
	.cfi_restore_state
	movl	$5395, %r9d
	movl	$183, %ecx
	movl	$519, %edx
	movl	$47, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L2458
	.p2align 4,,10
	.p2align 3
.L2464:
	movl	$5407, %r8d
	movl	$151, %edx
	movl	$519, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L2458
	.p2align 4,,10
	.p2align 3
.L2520:
	movl	-96(%rbp), %r8d
	leaq	-59(%rbp), %r9
	leaq	-58(%rbp), %rdx
	testl	%r8d, %r8d
	movl	%ebx, %r8d
	cmove	%r9, %rdx
	movq	%rdx, -72(%rbp)
	je	.L2507
	movq	%r11, -104(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%r15, -80(%rbp)
	movq	%r9, %r15
	movq	%r14, -88(%rbp)
	movl	%r8d, %r14d
	movl	%r13d, -96(%rbp)
	movq	%r12, %r13
	movq	%rbx, %r12
	movq	%r10, %rbx
.L2467:
	movq	0(%r13), %rdi
	testl	%r12d, %r12d
	je	.L2483
	xorl	%edx, %edx
.L2482:
	movl	%edx, %ecx
	addl	$1, %edx
	movzbl	(%rdi,%rcx), %esi
	movb	%sil, (%r15,%rcx)
	cmpl	%r14d, %edx
	jb	.L2482
.L2483:
	addq	%r12, %rdi
	subq	%r12, %rax
	cmpb	$0, -59(%rbp)
	movq	%rdi, 0(%r13)
	movq	%rax, 8(%r13)
	jne	.L2479
	movq	-72(%rbp), %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	call	ssl_get_cipher_by_char@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2516
	movl	(%rax), %edx
	testl	%edx, %edx
	jne	.L2524
.L2480:
	movq	-80(%rbp), %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L2516
.L2513:
	movq	-80(%rbp), %r15
	movq	-88(%rbp), %r14
	movq	%rbx, %r10
	movl	-96(%rbp), %r13d
.L2472:
	testl	%r13d, %r13d
	je	.L2481
	movl	$5438, %r9d
	jmp	.L2517
	.p2align 4,,10
	.p2align 3
.L2521:
	testl	%r13d, %r13d
	je	.L2486
	movl	$5448, %r9d
	leaq	.LC0(%rip), %r8
	movl	$271, %ecx
	movq	%r10, %rdi
	movl	$519, %edx
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L2477
	.p2align 4,,10
	.p2align 3
.L2524:
	movq	-88(%rbp), %rdi
	movq	%rax, -120(%rbp)
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L2513
	movq	-120(%rbp), %rsi
	movl	(%rsi), %eax
	testl	%eax, %eax
	je	.L2480
.L2516:
	movq	8(%r13), %rax
.L2479:
	cmpq	%rax, %r12
	jbe	.L2467
	movq	-80(%rbp), %r15
	movq	-88(%rbp), %r14
	movq	%rbx, %r10
	movq	-104(%rbp), %r11
	movq	-112(%rbp), %rcx
	movl	-96(%rbp), %r13d
	jmp	.L2466
	.p2align 4,,10
	.p2align 3
.L2465:
	movl	$5416, %r9d
	movl	$5419, %r8d
	testl	%r13d, %r13d
	je	.L2518
.L2517:
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
	movl	$519, %edx
	movq	%r10, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
.L2477:
	movq	%r14, %rdi
	call	OPENSSL_sk_free@PLT
	movq	%r15, %rdi
	call	OPENSSL_sk_free@PLT
	xorl	%eax, %eax
	jmp	.L2458
.L2481:
	movl	$5441, %r8d
	.p2align 4,,10
	.p2align 3
.L2518:
	movl	$65, %edx
	movl	$519, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L2477
	.p2align 4,,10
	.p2align 3
.L2507:
	movq	%r11, -96(%rbp)
	movq	%rcx, -104(%rbp)
	movq	%r15, -72(%rbp)
	movq	%r9, %r15
	movq	%r14, -80(%rbp)
	movl	%ebx, %r14d
	movl	%r13d, -88(%rbp)
	movq	%r12, %r13
	movq	%r10, %r12
.L2475:
	movq	0(%r13), %rdi
	testl	%ebx, %ebx
	je	.L2469
	xorl	%edx, %edx
.L2468:
	movl	%edx, %ecx
	addl	$1, %edx
	movzbl	(%rdi,%rcx), %esi
	movb	%sil, (%r15,%rcx)
	cmpl	%r14d, %edx
	jb	.L2468
.L2469:
	addq	%rbx, %rdi
	subq	%rbx, %rax
	movq	%r15, %rsi
	movl	$1, %edx
	movq	%rdi, 0(%r13)
	movq	%r12, %rdi
	movq	%rax, 8(%r13)
	call	ssl_get_cipher_by_char@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2470
	movl	(%rax), %edi
	testl	%edi, %edi
	jne	.L2471
.L2474:
	movq	-72(%rbp), %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L2470
.L2510:
	movq	-72(%rbp), %r15
	movq	-80(%rbp), %r14
	movq	%r12, %r10
	movl	-88(%rbp), %r13d
	jmp	.L2472
	.p2align 4,,10
	.p2align 3
.L2471:
	movq	-80(%rbp), %rdi
	movq	%rax, -112(%rbp)
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L2510
	movq	-112(%rbp), %rsi
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	je	.L2474
.L2470:
	movq	8(%r13), %rax
	cmpq	%rax, %rbx
	jbe	.L2475
	movq	-72(%rbp), %r15
	movq	-80(%rbp), %r14
	movq	%r12, %r10
	movq	-96(%rbp), %r11
	movq	-104(%rbp), %rcx
	movl	-88(%rbp), %r13d
	jmp	.L2466
	.p2align 4,,10
	.p2align 3
.L2486:
	movl	$5451, %r8d
	movl	$271, %edx
	movl	$519, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L2477
	.p2align 4,,10
	.p2align 3
.L2489:
	movq	%r15, %rdi
	call	OPENSSL_sk_free@PLT
	movl	$1, %eax
	jmp	.L2458
	.p2align 4,,10
	.p2align 3
.L2487:
	movq	%r14, %rdi
	movq	%rcx, -72(%rbp)
	call	OPENSSL_sk_free@PLT
	movq	-72(%rbp), %rcx
	jmp	.L2488
.L2523:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1947:
	.size	bytes_to_cipher_list, .-bytes_to_cipher_list
	.p2align 4
	.globl	SSL_CTX_set_max_early_data
	.type	SSL_CTX_set_max_early_data, @function
SSL_CTX_set_max_early_data:
.LFB1948:
	.cfi_startproc
	endbr64
	movl	%esi, 936(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1948:
	.size	SSL_CTX_set_max_early_data, .-SSL_CTX_set_max_early_data
	.p2align 4
	.globl	SSL_CTX_get_max_early_data
	.type	SSL_CTX_get_max_early_data, @function
SSL_CTX_get_max_early_data:
.LFB1949:
	.cfi_startproc
	endbr64
	movl	936(%rdi), %eax
	ret
	.cfi_endproc
.LFE1949:
	.size	SSL_CTX_get_max_early_data, .-SSL_CTX_get_max_early_data
	.p2align 4
	.globl	SSL_set_max_early_data
	.type	SSL_set_max_early_data, @function
SSL_set_max_early_data:
.LFB1950:
	.cfi_startproc
	endbr64
	movl	%esi, 6176(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1950:
	.size	SSL_set_max_early_data, .-SSL_set_max_early_data
	.p2align 4
	.globl	SSL_get_max_early_data
	.type	SSL_get_max_early_data, @function
SSL_get_max_early_data:
.LFB1951:
	.cfi_startproc
	endbr64
	movl	6176(%rdi), %eax
	ret
	.cfi_endproc
.LFE1951:
	.size	SSL_get_max_early_data, .-SSL_get_max_early_data
	.p2align 4
	.globl	SSL_CTX_set_recv_max_early_data
	.type	SSL_CTX_set_recv_max_early_data, @function
SSL_CTX_set_recv_max_early_data:
.LFB1952:
	.cfi_startproc
	endbr64
	movl	%esi, 940(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1952:
	.size	SSL_CTX_set_recv_max_early_data, .-SSL_CTX_set_recv_max_early_data
	.p2align 4
	.globl	SSL_CTX_get_recv_max_early_data
	.type	SSL_CTX_get_recv_max_early_data, @function
SSL_CTX_get_recv_max_early_data:
.LFB1953:
	.cfi_startproc
	endbr64
	movl	940(%rdi), %eax
	ret
	.cfi_endproc
.LFE1953:
	.size	SSL_CTX_get_recv_max_early_data, .-SSL_CTX_get_recv_max_early_data
	.p2align 4
	.globl	SSL_set_recv_max_early_data
	.type	SSL_set_recv_max_early_data, @function
SSL_set_recv_max_early_data:
.LFB1954:
	.cfi_startproc
	endbr64
	movl	%esi, 6180(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1954:
	.size	SSL_set_recv_max_early_data, .-SSL_set_recv_max_early_data
	.p2align 4
	.globl	SSL_get_recv_max_early_data
	.type	SSL_get_recv_max_early_data, @function
SSL_get_recv_max_early_data:
.LFB1955:
	.cfi_startproc
	endbr64
	movl	6180(%rdi), %eax
	ret
	.cfi_endproc
.LFE1955:
	.size	SSL_get_recv_max_early_data, .-SSL_get_recv_max_early_data
	.p2align 4
	.globl	ssl_get_max_send_fragment
	.type	ssl_get_max_send_fragment, @function
ssl_get_max_send_fragment:
.LFB1956:
	.cfi_startproc
	endbr64
	movq	1296(%rdi), %rax
	testq	%rax, %rax
	je	.L2534
	movzbl	600(%rax), %ecx
	leal	-1(%rcx), %eax
	cmpb	$3, %al
	jbe	.L2539
.L2534:
	movl	1536(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2539:
	movl	%eax, %ecx
	movl	$512, %eax
	sall	%cl, %eax
	ret
	.cfi_endproc
.LFE1956:
	.size	ssl_get_max_send_fragment, .-ssl_get_max_send_fragment
	.p2align 4
	.globl	ssl_get_split_send_fragment
	.type	ssl_get_split_send_fragment, @function
ssl_get_split_send_fragment:
.LFB1957:
	.cfi_startproc
	endbr64
	movq	1296(%rdi), %rax
	movq	1528(%rdi), %rdx
	testq	%rax, %rax
	je	.L2541
	movzbl	600(%rax), %ecx
	leal	-1(%rcx), %eax
	cmpb	$3, %al
	ja	.L2541
	movl	%eax, %ecx
	movl	$512, %eax
	sall	%cl, %eax
	movl	%eax, %ecx
	cmpq	%rdx, %rcx
	jb	.L2540
.L2541:
	movq	1536(%rdi), %rax
	cmpq	%rdx, %rax
	cmovnb	%edx, %eax
.L2540:
	ret
	.cfi_endproc
.LFE1957:
	.size	ssl_get_split_send_fragment, .-ssl_get_split_send_fragment
	.p2align 4
	.globl	SSL_stateless
	.type	SSL_stateless, @function
SSL_stateless:
.LFB1958:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	SSL_clear
	testl	%eax, %eax
	jne	.L2565
.L2547:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2565:
	.cfi_restore_state
	call	ERR_clear_error@PLT
	movq	168(%r12), %rax
	orq	$2048, (%rax)
	cmpq	$0, 48(%r12)
	je	.L2566
.L2549:
	movq	%r12, %rdi
	call	SSL_do_handshake
	movl	%eax, %r8d
	movq	168(%r12), %rax
	andq	$-2049, (%rax)
	testl	%r8d, %r8d
	jle	.L2552
	movl	1840(%r12), %edx
	movl	$1, %eax
	testl	%edx, %edx
	jne	.L2547
.L2552:
	cmpl	$1, 1248(%r12)
	movl	$-1, %eax
	jne	.L2547
	movq	%r12, %rdi
	call	ossl_statem_in_error@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	negl	%eax
	jmp	.L2547
	.p2align 4,,10
	.p2align 3
.L2566:
	movl	$1, 56(%r12)
	movq	%r12, %rdi
	movl	$0, 68(%r12)
	call	ossl_statem_clear@PLT
	movq	8(%r12), %rax
	movq	1088(%r12), %rdi
	movq	40(%rax), %rax
	movq	%rax, 48(%r12)
	testq	%rdi, %rdi
	je	.L2550
	call	EVP_CIPHER_CTX_free@PLT
	movq	$0, 1088(%r12)
.L2550:
	movq	1136(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2551
	call	EVP_CIPHER_CTX_free@PLT
	movq	$0, 1136(%r12)
.L2551:
	movq	1112(%r12), %rdi
	call	EVP_MD_CTX_free@PLT
	movq	1160(%r12), %rdi
	movq	$0, 1112(%r12)
	call	EVP_MD_CTX_free@PLT
	movq	$0, 1160(%r12)
	jmp	.L2549
	.cfi_endproc
.LFE1958:
	.size	SSL_stateless, .-SSL_stateless
	.p2align 4
	.globl	SSL_CTX_set_post_handshake_auth
	.type	SSL_CTX_set_post_handshake_auth, @function
SSL_CTX_set_post_handshake_auth:
.LFB1959:
	.cfi_startproc
	endbr64
	movl	%esi, 1016(%rdi)
	ret
	.cfi_endproc
.LFE1959:
	.size	SSL_CTX_set_post_handshake_auth, .-SSL_CTX_set_post_handshake_auth
	.p2align 4
	.globl	SSL_set_post_handshake_auth
	.type	SSL_set_post_handshake_auth, @function
SSL_set_post_handshake_auth:
.LFB1960:
	.cfi_startproc
	endbr64
	movl	%esi, 1940(%rdi)
	ret
	.cfi_endproc
.LFE1960:
	.size	SSL_set_post_handshake_auth, .-SSL_set_post_handshake_auth
	.p2align 4
	.globl	SSL_verify_client_post_handshake
	.type	SSL_verify_client_post_handshake, @function
SSL_verify_client_post_handshake:
.LFB1961:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L2570
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L2570
	cmpl	$65536, %eax
	je	.L2570
	movl	56(%rdi), %eax
	movq	%rdi, %r12
	testl	%eax, %eax
	je	.L2584
	call	SSL_is_init_finished@PLT
	testl	%eax, %eax
	je	.L2585
	movl	1936(%r12), %eax
	cmpl	$3, %eax
	je	.L2576
	ja	.L2577
	testl	%eax, %eax
	je	.L2578
	cmpl	$2, %eax
	jne	.L2580
	movl	$3, 1936(%r12)
	movq	%r12, %rdi
	call	send_certificate_request@PLT
	testl	%eax, %eax
	je	.L2586
	movl	$1, %esi
	movq	%r12, %rdi
	call	ossl_statem_set_in_init@PLT
	movl	$1, %eax
	jmp	.L2569
	.p2align 4,,10
	.p2align 3
.L2570:
	movl	$5579, %r8d
	movl	$266, %edx
	movl	$616, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L2569:
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2577:
	.cfi_restore_state
	cmpl	$4, %eax
	jne	.L2580
	movl	$5606, %r8d
	movl	$286, %edx
	movl	$616, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L2569
	.p2align 4,,10
	.p2align 3
.L2584:
	movl	$5583, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$284, %edx
	movl	%eax, -20(%rbp)
	movl	$616, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	movl	-20(%rbp), %eax
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2585:
	.cfi_restore_state
	movl	$5588, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$121, %edx
	movl	%eax, -20(%rbp)
	movl	$616, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	movl	-20(%rbp), %eax
	jmp	.L2569
	.p2align 4,,10
	.p2align 3
.L2578:
	movl	$5594, %r8d
	movl	$279, %edx
	movl	$616, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L2569
	.p2align 4,,10
	.p2align 3
.L2580:
	movl	$5598, %r8d
	movl	$68, %edx
	movl	$616, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L2569
	.p2align 4,,10
	.p2align 3
.L2576:
	movl	$5603, %r8d
	movl	$285, %edx
	movl	$616, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L2569
	.p2align 4,,10
	.p2align 3
.L2586:
	movl	$5615, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$283, %edx
	movl	%eax, -20(%rbp)
	movl	$616, %esi
	movl	$20, %edi
	movl	$2, 1936(%r12)
	call	ERR_put_error@PLT
	movl	-20(%rbp), %eax
	jmp	.L2569
	.cfi_endproc
.LFE1961:
	.size	SSL_verify_client_post_handshake, .-SSL_verify_client_post_handshake
	.p2align 4
	.globl	SSL_CTX_set_session_ticket_cb
	.type	SSL_CTX_set_session_ticket_cb, @function
SSL_CTX_set_session_ticket_cb:
.LFB1962:
	.cfi_startproc
	endbr64
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	movl	$1, %eax
	movq	%rcx, 984(%rdi)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 968(%rdi)
	ret
	.cfi_endproc
.LFE1962:
	.size	SSL_CTX_set_session_ticket_cb, .-SSL_CTX_set_session_ticket_cb
	.p2align 4
	.globl	SSL_CTX_set_allow_early_data_cb
	.type	SSL_CTX_set_allow_early_data_cb, @function
SSL_CTX_set_allow_early_data_cb:
.LFB1963:
	.cfi_startproc
	endbr64
	movq	%rsi, 1000(%rdi)
	movq	%rdx, 1008(%rdi)
	ret
	.cfi_endproc
.LFE1963:
	.size	SSL_CTX_set_allow_early_data_cb, .-SSL_CTX_set_allow_early_data_cb
	.p2align 4
	.globl	SSL_set_allow_early_data_cb
	.type	SSL_set_allow_early_data_cb, @function
SSL_set_allow_early_data_cb:
.LFB1964:
	.cfi_startproc
	endbr64
	movq	%rsi, 6248(%rdi)
	movq	%rdx, 6256(%rdi)
	ret
	.cfi_endproc
.LFE1964:
	.size	SSL_set_allow_early_data_cb, .-SSL_set_allow_early_data_cb
	.globl	ssl3_undef_enc_method
	.section	.data.rel.local,"aw"
	.align 32
	.type	ssl3_undef_enc_method, @object
	.size	ssl3_undef_enc_method, 128
ssl3_undef_enc_method:
	.quad	ssl_undefined_function_1
	.quad	ssl_undefined_function_2
	.quad	ssl_undefined_function
	.quad	ssl_undefined_function_3
	.quad	ssl_undefined_function_4
	.quad	ssl_undefined_function_5
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	ssl_undefined_function_6
	.quad	ssl_undefined_function_7
	.zero	32
	.globl	SSL_version_str
	.section	.rodata
	.align 16
	.type	SSL_version_str, @object
	.size	SSL_version_str, 28
SSL_version_str:
	.string	"OpenSSL 1.1.1g  21 Apr 2020"
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC5:
	.quad	16384
	.quad	16384
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
