	.file	"x509_lu.c"
	.text
	.p2align 4
	.type	x509_object_cmp, @function
x509_object_cmp:
.LFB1384:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rcx
	movq	(%rsi), %rdx
	movl	(%rcx), %eax
	movl	%eax, %r8d
	subl	(%rdx), %r8d
	jne	.L1
	cmpl	$1, %eax
	je	.L3
	cmpl	$2, %eax
	je	.L6
.L1:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movq	8(%rdx), %rsi
	movq	8(%rcx), %rdi
	jmp	X509_CRL_cmp@PLT
	.p2align 4,,10
	.p2align 3
.L3:
	movq	8(%rdx), %rsi
	movq	8(%rcx), %rdi
	jmp	X509_subject_name_cmp@PLT
	.cfi_endproc
.LFE1384:
	.size	x509_object_cmp, .-x509_object_cmp
	.p2align 4
	.type	x509_object_idx_cnt, @function
x509_object_idx_cnt:
.LFB1403:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$624, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	%esi, -656(%rbp)
	cmpl	$1, %esi
	je	.L8
	cmpl	$2, %esi
	je	.L9
	movl	$-1, %r14d
	testl	%esi, %esi
	je	.L7
.L11:
	leaq	-656(%rbp), %rsi
	movq	%r12, %rdi
	call	OPENSSL_sk_find@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	js	.L7
	testq	%r13, %r13
	je	.L7
	movl	$1, 0(%r13)
	leal	1(%rax), %ebx
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L26:
	cmpl	$1, %edx
	je	.L13
	cmpl	$2, %edx
	je	.L25
.L15:
	addl	$1, 0(%r13)
	addl	$1, %ebx
.L12:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L7
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	movl	(%rax), %edx
	cmpl	-656(%rbp), %edx
	je	.L26
.L7:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L27
	addq	$624, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	movq	8(%rax), %rdi
	movq	-648(%rbp), %rsi
	call	X509_subject_name_cmp@PLT
.L16:
	testl	%eax, %eax
	je	.L15
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L25:
	movq	8(%rax), %rdi
	movq	-648(%rbp), %rsi
	call	X509_CRL_cmp@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L9:
	leaq	-640(%rbp), %rax
	movq	%rdx, -616(%rbp)
	movq	%rax, -648(%rbp)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L8:
	leaq	-400(%rbp), %rax
	movq	%rdx, -328(%rbp)
	movq	%rax, -648(%rbp)
	jmp	.L11
.L27:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1403:
	.size	x509_object_idx_cnt, .-x509_object_idx_cnt
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/x509/x509_lu.c"
	.text
	.p2align 4
	.globl	X509_OBJECT_free
	.type	X509_OBJECT_free, @function
X509_OBJECT_free:
.LFB1402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	testq	%rdi, %rdi
	je	.L29
	movl	(%rdi), %eax
	cmpl	$1, %eax
	je	.L30
	cmpl	$2, %eax
	je	.L31
.L29:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$471, %edx
	popq	%r12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	X509_CRL_free@PLT
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L30:
	movq	8(%rdi), %rdi
	call	X509_free@PLT
	jmp	.L29
	.cfi_endproc
.LFE1402:
	.size	X509_OBJECT_free, .-X509_OBJECT_free
	.p2align 4
	.globl	X509_LOOKUP_new
	.type	X509_LOOKUP_new, @function
X509_LOOKUP_new:
.LFB1370:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$20, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$32, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L43
	movq	%rbx, 8(%rax)
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L36
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L44
.L36:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$29, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%r12d, %r12d
	call	CRYPTO_free@PLT
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L43:
	movl	$23, %r8d
	movl	$65, %edx
	movl	$155, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L36
	.cfi_endproc
.LFE1370:
	.size	X509_LOOKUP_new, .-X509_LOOKUP_new
	.p2align 4
	.globl	X509_LOOKUP_free
	.type	X509_LOOKUP_free, @function
X509_LOOKUP_free:
.LFB1371:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L45
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L47
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L47
	call	*%rax
.L47:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$41, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L45:
	ret
	.cfi_endproc
.LFE1371:
	.size	X509_LOOKUP_free, .-X509_LOOKUP_free
	.p2align 4
	.globl	X509_STORE_lock
	.type	X509_STORE_lock, @function
X509_STORE_lock:
.LFB1372:
	.cfi_startproc
	endbr64
	movq	144(%rdi), %rdi
	jmp	CRYPTO_THREAD_write_lock@PLT
	.cfi_endproc
.LFE1372:
	.size	X509_STORE_lock, .-X509_STORE_lock
	.p2align 4
	.globl	X509_STORE_unlock
	.type	X509_STORE_unlock, @function
X509_STORE_unlock:
.LFB1373:
	.cfi_startproc
	endbr64
	movq	144(%rdi), %rdi
	jmp	CRYPTO_THREAD_unlock@PLT
	.cfi_endproc
.LFE1373:
	.size	X509_STORE_unlock, .-X509_STORE_unlock
	.p2align 4
	.globl	X509_LOOKUP_init
	.type	X509_LOOKUP_init, @function
X509_LOOKUP_init:
.LFB1374:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L61
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L62
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L62:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1374:
	.size	X509_LOOKUP_init, .-X509_LOOKUP_init
	.p2align 4
	.globl	X509_LOOKUP_shutdown
	.type	X509_LOOKUP_shutdown, @function
X509_LOOKUP_shutdown:
.LFB1375:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L65
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.L66
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L66:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1375:
	.size	X509_LOOKUP_shutdown, .-X509_LOOKUP_shutdown
	.p2align 4
	.globl	X509_LOOKUP_ctrl
	.type	X509_LOOKUP_ctrl, @function
X509_LOOKUP_ctrl:
.LFB1376:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L69
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L70
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L70:
	movl	$1, %eax
	ret
.L69:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE1376:
	.size	X509_LOOKUP_ctrl, .-X509_LOOKUP_ctrl
	.p2align 4
	.globl	X509_LOOKUP_by_subject
	.type	X509_LOOKUP_by_subject, @function
X509_LOOKUP_by_subject:
.LFB1377:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L71
	movq	48(%rax), %rax
	testq	%rax, %rax
	je	.L71
	movl	4(%rdi), %r8d
	testl	%r8d, %r8d
	jne	.L71
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L71:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1377:
	.size	X509_LOOKUP_by_subject, .-X509_LOOKUP_by_subject
	.p2align 4
	.globl	X509_LOOKUP_by_issuer_serial
	.type	X509_LOOKUP_by_issuer_serial, @function
X509_LOOKUP_by_issuer_serial:
.LFB1378:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L78
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L78
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L78:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1378:
	.size	X509_LOOKUP_by_issuer_serial, .-X509_LOOKUP_by_issuer_serial
	.p2align 4
	.globl	X509_LOOKUP_by_fingerprint
	.type	X509_LOOKUP_by_fingerprint, @function
X509_LOOKUP_by_fingerprint:
.LFB1379:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L84
	movq	64(%rax), %rax
	testq	%rax, %rax
	je	.L84
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L84:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1379:
	.size	X509_LOOKUP_by_fingerprint, .-X509_LOOKUP_by_fingerprint
	.p2align 4
	.globl	X509_LOOKUP_by_alias
	.type	X509_LOOKUP_by_alias, @function
X509_LOOKUP_by_alias:
.LFB1380:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L90
	movq	72(%rax), %rax
	testq	%rax, %rax
	je	.L90
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L90:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1380:
	.size	X509_LOOKUP_by_alias, .-X509_LOOKUP_by_alias
	.p2align 4
	.globl	X509_LOOKUP_set_method_data
	.type	X509_LOOKUP_set_method_data, @function
X509_LOOKUP_set_method_data:
.LFB1381:
	.cfi_startproc
	endbr64
	movq	%rsi, 16(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1381:
	.size	X509_LOOKUP_set_method_data, .-X509_LOOKUP_set_method_data
	.p2align 4
	.globl	X509_LOOKUP_get_method_data
	.type	X509_LOOKUP_get_method_data, @function
X509_LOOKUP_get_method_data:
.LFB1382:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE1382:
	.size	X509_LOOKUP_get_method_data, .-X509_LOOKUP_get_method_data
	.p2align 4
	.globl	X509_LOOKUP_get_store
	.type	X509_LOOKUP_get_store, @function
X509_LOOKUP_get_store:
.LFB1383:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE1383:
	.size	X509_LOOKUP_get_store, .-X509_LOOKUP_get_store
	.p2align 4
	.globl	X509_STORE_new
	.type	X509_STORE_new, @function
X509_STORE_new:
.LFB1385:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$162, %edx
	movl	$152, %edi
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L110
	leaq	x509_object_cmp(%rip), %rdi
	call	OPENSSL_sk_new@PLT
	movl	$169, %r8d
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L109
	movl	$1, (%r12)
	call	OPENSSL_sk_new_null@PLT
	movl	$174, %r8d
	movq	%rax, 16(%r12)
	testq	%rax, %rax
	je	.L109
	call	X509_VERIFY_PARAM_new@PLT
	movl	$179, %r8d
	movq	%rax, 24(%r12)
	testq	%rax, %rax
	je	.L109
	leaq	128(%r12), %rdx
	movq	%r12, %rsi
	movl	$4, %edi
	call	CRYPTO_new_ex_data@PLT
	movl	$183, %r8d
	testl	%eax, %eax
	je	.L109
	call	CRYPTO_THREAD_lock_new@PLT
	movq	%rax, 144(%r12)
	testq	%rax, %rax
	je	.L111
	movl	$1, 136(%r12)
	mfence
.L99:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	movl	$189, %r8d
.L109:
.L103:
	endbr64
	movl	$65, %edx
	movl	$158, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	24(%r12), %rdi
	call	X509_VERIFY_PARAM_free@PLT
	movq	8(%r12), %rdi
	call	OPENSSL_sk_free@PLT
	movq	16(%r12), %rdi
	call	OPENSSL_sk_free@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movl	$200, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	movl	$165, %r8d
	movl	$65, %edx
	movl	$158, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L99
	.cfi_endproc
.LFE1385:
	.size	X509_STORE_new, .-X509_STORE_new
	.p2align 4
	.globl	X509_STORE_free
	.type	X509_STORE_free, @function
X509_STORE_free:
.LFB1386:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L134
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	lock xaddl	%eax, 136(%rdi)
	subl	$1, %eax
	testl	%eax, %eax
	je	.L137
	jle	.L116
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
.L116:
	movq	16(%r13), %r14
	xorl	%ebx, %ebx
	leaq	.LC0(%rip), %r15
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L123:
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r12
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L120
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L121
	movq	%r12, %rdi
	call	*%rdx
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L120
.L121:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L120
	movq	%r12, %rdi
	call	*%rax
.L120:
	movl	$41, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	addl	$1, %ebx
	call	CRYPTO_free@PLT
.L118:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%ebx, %eax
	jg	.L123
	movq	%r14, %rdi
	call	OPENSSL_sk_free@PLT
	movq	8(%r13), %rdi
	leaq	X509_OBJECT_free(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	leaq	128(%r13), %rdx
	movq	%r13, %rsi
	movl	$4, %edi
	call	CRYPTO_free_ex_data@PLT
	movq	24(%r13), %rdi
	call	X509_VERIFY_PARAM_free@PLT
	movq	144(%r13), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$230, %edx
	popq	%rbx
	.cfi_restore 3
	leaq	.LC0(%rip), %rsi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L134:
	ret
	.cfi_endproc
.LFE1386:
	.size	X509_STORE_free, .-X509_STORE_free
	.p2align 4
	.globl	X509_STORE_up_ref
	.type	X509_STORE_up_ref, @function
X509_STORE_up_ref:
.LFB1387:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	lock xaddl	%eax, 136(%rdi)
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1387:
	.size	X509_STORE_up_ref, .-X509_STORE_up_ref
	.p2align 4
	.globl	X509_STORE_add_lookup
	.type	X509_STORE_add_lookup, @function
X509_STORE_add_lookup:
.LFB1388:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$8, %rsp
	movq	16(%rdi), %r13
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L142:
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r12
	cmpq	%r14, 8(%rax)
	je	.L139
	addl	$1, %ebx
.L140:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L142
	movl	$20, %edx
	leaq	.LC0(%rip), %rsi
	movl	$32, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L159
	movq	%r14, 8(%rax)
	movq	8(%r14), %rax
	testq	%rax, %rax
	je	.L145
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L160
.L145:
	movq	%r15, 24(%r12)
	movq	16(%r15), %rdi
	movq	%r12, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L157
.L139:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L157:
	.cfi_restore_state
	movl	$269, %r8d
	movl	$65, %edx
	movl	$157, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L146
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L146
	movq	%r12, %rdi
	call	*%rax
.L146:
	movq	%r12, %rdi
	movl	$41, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%r12d, %r12d
	call	CRYPTO_free@PLT
	jmp	.L139
.L159:
	movl	$23, %r8d
	movl	$65, %edx
	movl	$155, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L144:
	movl	$261, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$157, %esi
	movl	$11, %edi
	call	ERR_put_error@PLT
	jmp	.L139
.L160:
	movl	$29, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	jmp	.L144
	.cfi_endproc
.LFE1388:
	.size	X509_STORE_add_lookup, .-X509_STORE_add_lookup
	.p2align 4
	.globl	X509_STORE_CTX_get_by_subject
	.type	X509_STORE_CTX_get_by_subject, @function
X509_STORE_CTX_get_by_subject:
.LFB1390:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -96(%rbp)
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L171
	movq	144(%r12), %rdi
	movl	%esi, %r13d
	movq	%rdx, %r14
	movl	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	call	CRYPTO_THREAD_write_lock@PLT
	movq	8(%r12), %r15
	movl	%r13d, %esi
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	x509_object_idx_cnt
	movl	%eax, %esi
	cmpl	$-1, %eax
	je	.L164
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	movq	144(%r12), %rdi
	movq	%rax, %rbx
	movq	%rax, -88(%rbp)
	call	CRYPTO_THREAD_unlock@PLT
	testq	%rbx, %rbx
	je	.L165
	cmpl	$2, %r13d
	je	.L165
.L166:
	movq	-88(%rbp), %rdx
	movl	(%rdx), %eax
	movq	8(%rdx), %rdi
	cmpl	$1, %eax
	je	.L172
.L192:
	cmpl	$2, %eax
	jne	.L174
	call	X509_CRL_up_ref@PLT
	testl	%eax, %eax
	jne	.L189
.L171:
	xorl	%eax, %eax
.L161:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L190
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	movq	144(%r12), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movq	$0, -88(%rbp)
.L165:
	xorl	%ebx, %ebx
	leaq	-80(%rbp), %r15
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L170:
	movq	16(%r12), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L169
	movq	48(%rax), %rax
	testq	%rax, %rax
	je	.L169
	movl	4(%rdi), %edx
	testl	%edx, %edx
	jne	.L169
	movq	%r15, %rcx
	movq	%r14, %rdx
	movl	%r13d, %esi
	call	*%rax
	testl	%eax, %eax
	jne	.L191
.L169:
	addl	$1, %ebx
.L168:
	movq	16(%r12), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L170
	cmpq	$0, -88(%rbp)
	je	.L171
	movq	-88(%rbp), %rdx
	movl	(%rdx), %eax
	movq	8(%rdx), %rdi
	cmpl	$1, %eax
	jne	.L192
.L172:
	call	X509_up_ref@PLT
	testl	%eax, %eax
	je	.L171
.L189:
	movq	-88(%rbp), %rcx
	movl	(%rcx), %eax
	movq	8(%rcx), %rdi
.L174:
	movq	-96(%rbp), %rcx
	movl	%eax, (%rcx)
	movl	$1, %eax
	movq	%rdi, 8(%rcx)
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L191:
	movq	%r15, -88(%rbp)
	jmp	.L166
.L190:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1390:
	.size	X509_STORE_CTX_get_by_subject, .-X509_STORE_CTX_get_by_subject
	.p2align 4
	.globl	X509_STORE_CTX_get_obj_by_subject
	.type	X509_STORE_CTX_get_obj_by_subject, @function
X509_STORE_CTX_get_obj_by_subject:
.LFB1389:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movl	$420, %edx
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	leaq	.LC0(%rip), %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$16, %edi
	pushq	%r12
	.cfi_offset 12, -48
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L200
	movl	$0, (%rax)
	movq	%rax, %rcx
	movq	%r15, %rdx
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	X509_STORE_CTX_get_by_subject
	testl	%eax, %eax
	je	.L201
.L193:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L201:
	.cfi_restore_state
	movl	(%r12), %eax
	cmpl	$1, %eax
	je	.L196
	cmpl	$2, %eax
	je	.L197
.L198:
	movq	%r12, %rdi
	movl	$471, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%r12d, %r12d
	call	CRYPTO_free@PLT
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L197:
	movq	8(%r12), %rdi
	call	X509_CRL_free@PLT
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L196:
	movq	8(%r12), %rdi
	call	X509_free@PLT
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L200:
	movl	$423, %r8d
	movl	$65, %edx
	movl	$150, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L193
	.cfi_endproc
.LFE1389:
	.size	X509_STORE_CTX_get_obj_by_subject, .-X509_STORE_CTX_get_obj_by_subject
	.p2align 4
	.globl	X509_OBJECT_up_ref_count
	.type	X509_OBJECT_up_ref_count, @function
X509_OBJECT_up_ref_count:
.LFB1394:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	cmpl	$1, %eax
	je	.L203
	cmpl	$2, %eax
	je	.L204
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L203:
	movq	8(%rdi), %rdi
	jmp	X509_up_ref@PLT
	.p2align 4,,10
	.p2align 3
.L204:
	movq	8(%rdi), %rdi
	jmp	X509_CRL_up_ref@PLT
	.cfi_endproc
.LFE1394:
	.size	X509_OBJECT_up_ref_count, .-X509_OBJECT_up_ref_count
	.p2align 4
	.globl	X509_OBJECT_get0_X509
	.type	X509_OBJECT_get0_X509, @function
X509_OBJECT_get0_X509:
.LFB1395:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L207
	cmpl	$1, (%rdi)
	jne	.L207
	movq	8(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L207:
	ret
	.cfi_endproc
.LFE1395:
	.size	X509_OBJECT_get0_X509, .-X509_OBJECT_get0_X509
	.p2align 4
	.globl	X509_OBJECT_get0_X509_CRL
	.type	X509_OBJECT_get0_X509_CRL, @function
X509_OBJECT_get0_X509_CRL:
.LFB1396:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L211
	cmpl	$2, (%rdi)
	jne	.L211
	movq	8(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L211:
	ret
	.cfi_endproc
.LFE1396:
	.size	X509_OBJECT_get0_X509_CRL, .-X509_OBJECT_get0_X509_CRL
	.p2align 4
	.globl	X509_OBJECT_get_type
	.type	X509_OBJECT_get_type, @function
X509_OBJECT_get_type:
.LFB1397:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	ret
	.cfi_endproc
.LFE1397:
	.size	X509_OBJECT_get_type, .-X509_OBJECT_get_type
	.p2align 4
	.globl	X509_OBJECT_new
	.type	X509_OBJECT_new, @function
X509_OBJECT_new:
.LFB1398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$420, %edx
	movl	$16, %edi
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L220
	movl	$0, (%rax)
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L220:
	.cfi_restore_state
	movl	$423, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$150, %esi
	movl	$11, %edi
	movq	%rax, -8(%rbp)
	call	ERR_put_error@PLT
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1398:
	.size	X509_OBJECT_new, .-X509_OBJECT_new
	.p2align 4
	.globl	X509_OBJECT_set1_X509
	.type	X509_OBJECT_set1_X509, @function
X509_OBJECT_set1_X509:
.LFB1400:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L234
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	call	X509_up_ref@PLT
	testl	%eax, %eax
	jne	.L235
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L235:
	.cfi_restore_state
	movl	(%rbx), %eax
	cmpl	$1, %eax
	je	.L225
	cmpl	$2, %eax
	je	.L226
.L227:
	movl	$1, (%rbx)
	movl	$1, %eax
	movq	%r12, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L234:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L226:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	8(%rbx), %rdi
	call	X509_CRL_free@PLT
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L225:
	movq	8(%rbx), %rdi
	call	X509_free@PLT
	jmp	.L227
	.cfi_endproc
.LFE1400:
	.size	X509_OBJECT_set1_X509, .-X509_OBJECT_set1_X509
	.p2align 4
	.globl	X509_OBJECT_set1_X509_CRL
	.type	X509_OBJECT_set1_X509_CRL, @function
X509_OBJECT_set1_X509_CRL:
.LFB1401:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L249
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	call	X509_CRL_up_ref@PLT
	testl	%eax, %eax
	jne	.L250
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L250:
	.cfi_restore_state
	movl	(%rbx), %eax
	cmpl	$1, %eax
	je	.L240
	cmpl	$2, %eax
	je	.L241
.L242:
	movl	$2, (%rbx)
	movl	$1, %eax
	movq	%r12, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L249:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L241:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	8(%rbx), %rdi
	call	X509_CRL_free@PLT
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L240:
	movq	8(%rbx), %rdi
	call	X509_free@PLT
	jmp	.L242
	.cfi_endproc
.LFE1401:
	.size	X509_OBJECT_set1_X509_CRL, .-X509_OBJECT_set1_X509_CRL
	.p2align 4
	.globl	X509_OBJECT_idx_by_subject
	.type	X509_OBJECT_idx_by_subject, @function
X509_OBJECT_idx_by_subject:
.LFB1404:
	.cfi_startproc
	endbr64
	xorl	%ecx, %ecx
	jmp	x509_object_idx_cnt
	.cfi_endproc
.LFE1404:
	.size	X509_OBJECT_idx_by_subject, .-X509_OBJECT_idx_by_subject
	.p2align 4
	.globl	X509_OBJECT_retrieve_by_subject
	.type	X509_OBJECT_retrieve_by_subject, @function
X509_OBJECT_retrieve_by_subject:
.LFB1405:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	x509_object_idx_cnt
	cmpl	$-1, %eax
	je	.L253
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	OPENSSL_sk_value@PLT
	.p2align 4,,10
	.p2align 3
.L253:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1405:
	.size	X509_OBJECT_retrieve_by_subject, .-X509_OBJECT_retrieve_by_subject
	.p2align 4
	.globl	X509_STORE_get0_objects
	.type	X509_STORE_get0_objects, @function
X509_STORE_get0_objects:
.LFB1406:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE1406:
	.size	X509_STORE_get0_objects, .-X509_STORE_get0_objects
	.p2align 4
	.globl	X509_STORE_CTX_get1_certs
	.type	X509_STORE_CTX_get1_certs, @function
X509_STORE_CTX_get1_certs:
.LFB1407:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L256
	movq	%rdi, %r15
	movq	144(%r13), %rdi
	movq	%rsi, %r12
	leaq	-60(%rbp), %rbx
	call	CRYPTO_THREAD_write_lock@PLT
	movq	8(%r13), %rdi
	movq	%rbx, %rcx
	movq	%r12, %rdx
	movl	$1, %esi
	call	x509_object_idx_cnt
	movl	%eax, %r14d
	testl	%eax, %eax
	js	.L277
.L258:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %r15
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jle	.L267
	xorl	%ebx, %ebx
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L268:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L278
	addl	$1, %ebx
	cmpl	%ebx, -60(%rbp)
	jle	.L267
.L270:
	movq	8(%r13), %rdi
	leal	(%rbx,%r14), %esi
	call	OPENSSL_sk_value@PLT
	movq	8(%rax), %r12
	movq	%r12, %rdi
	call	X509_up_ref@PLT
	testl	%eax, %eax
	jne	.L268
	movq	144(%r13), %rdi
	xorl	%r13d, %r13d
	call	CRYPTO_THREAD_unlock@PLT
	movq	X509_free@GOTPCREL(%rip), %rsi
	movq	%r15, %rdi
	call	OPENSSL_sk_pop_free@PLT
.L256:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L279
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L277:
	.cfi_restore_state
	movl	$420, %edx
	leaq	.LC0(%rip), %rsi
	movl	$16, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L280
	movl	$0, (%rax)
	movq	144(%r13), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movq	%r14, %rcx
	movq	%r12, %rdx
	movl	$1, %esi
	movq	%r15, %rdi
	call	X509_STORE_CTX_get_by_subject
	testl	%eax, %eax
	movl	(%r14), %eax
	je	.L281
	cmpl	$1, %eax
	je	.L264
	cmpl	$2, %eax
	jne	.L266
	movq	8(%r14), %rdi
	call	X509_CRL_free@PLT
.L266:
	movl	$471, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	movq	144(%r13), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	8(%r13), %rdi
	movq	%rbx, %rcx
	movq	%r12, %rdx
	movl	$1, %esi
	call	x509_object_idx_cnt
	movl	%eax, %r14d
	testl	%eax, %eax
	jns	.L258
	movq	144(%r13), %rdi
	xorl	%r13d, %r13d
	call	CRYPTO_THREAD_unlock@PLT
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L267:
	movq	144(%r13), %rdi
	movq	%r15, %r13
	call	CRYPTO_THREAD_unlock@PLT
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L278:
	movq	144(%r13), %rdi
	xorl	%r13d, %r13d
	call	CRYPTO_THREAD_unlock@PLT
	movq	%r12, %rdi
	call	X509_free@PLT
	movq	X509_free@GOTPCREL(%rip), %rsi
	movq	%r15, %rdi
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L256
.L264:
	movq	8(%r14), %rdi
	call	X509_free@PLT
	jmp	.L266
.L281:
	cmpl	$1, %eax
	je	.L261
	cmpl	$2, %eax
	jne	.L263
	movq	8(%r14), %rdi
	call	X509_CRL_free@PLT
.L263:
	movl	$471, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	xorl	%r13d, %r13d
	call	CRYPTO_free@PLT
	jmp	.L256
.L280:
	movl	$423, %r8d
	movl	$65, %edx
	movl	$150, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	144(%r13), %rdi
	xorl	%r13d, %r13d
	call	CRYPTO_THREAD_unlock@PLT
	jmp	.L256
.L261:
	movq	8(%r14), %rdi
	call	X509_free@PLT
	jmp	.L263
.L279:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1407:
	.size	X509_STORE_CTX_get1_certs, .-X509_STORE_CTX_get1_certs
	.p2align 4
	.globl	X509_STORE_CTX_get1_crls
	.type	X509_STORE_CTX_get1_crls, @function
X509_STORE_CTX_get1_crls:
.LFB1408:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	OPENSSL_sk_new_null@PLT
	movl	$420, %edx
	movl	$16, %edi
	leaq	.LC0(%rip), %rsi
	movq	%rax, %r13
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L308
	movl	$0, (%rax)
	movq	(%r15), %r14
	testq	%r13, %r13
	je	.L284
	testq	%r14, %r14
	je	.L284
	movq	%rax, %rcx
	movq	%rbx, %rdx
	movl	$2, %esi
	movq	%r15, %rdi
	call	X509_STORE_CTX_get_by_subject
	testl	%eax, %eax
	movl	(%r12), %eax
	je	.L309
	cmpl	$1, %eax
	je	.L289
	cmpl	$2, %eax
	jne	.L291
	movq	8(%r12), %rdi
	call	X509_CRL_free@PLT
.L291:
	movl	$471, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	movq	144(%r14), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	8(%r14), %rdi
	leaq	-60(%rbp), %rcx
	movq	%rbx, %rdx
	movl	$2, %esi
	call	x509_object_idx_cnt
	movl	%eax, %r15d
	testl	%eax, %eax
	js	.L292
	movl	-60(%rbp), %eax
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jg	.L293
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L295:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L310
	addl	$1, %ebx
	cmpl	%ebx, -60(%rbp)
	jle	.L294
.L293:
	movq	8(%r14), %rdi
	leal	(%rbx,%r15), %esi
	call	OPENSSL_sk_value@PLT
	movq	8(%rax), %r12
	movq	%r12, %rdi
	call	X509_CRL_up_ref@PLT
	testl	%eax, %eax
	jne	.L295
	movq	144(%r14), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movq	X509_CRL_free@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L309:
	cmpl	$1, %eax
	je	.L286
	cmpl	$2, %eax
	jne	.L284
	movq	8(%r12), %rdi
	call	X509_CRL_free@PLT
.L284:
	movl	$471, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	OPENSSL_sk_free@PLT
.L282:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L311
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L289:
	.cfi_restore_state
	movq	8(%r12), %rdi
	call	X509_free@PLT
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L286:
	movq	8(%r12), %rdi
	call	X509_free@PLT
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L294:
	movq	144(%r14), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L310:
	movq	144(%r14), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movq	%r12, %rdi
	call	X509_CRL_free@PLT
	movq	X509_CRL_free@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L282
.L308:
	movl	$423, %r8d
	movl	$65, %edx
	movl	$150, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L284
.L292:
	movq	144(%r14), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	OPENSSL_sk_free@PLT
	jmp	.L282
.L311:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1408:
	.size	X509_STORE_CTX_get1_crls, .-X509_STORE_CTX_get1_crls
	.p2align 4
	.globl	X509_OBJECT_retrieve_match
	.type	X509_OBJECT_retrieve_match, @function
X509_OBJECT_retrieve_match:
.LFB1409:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	OPENSSL_sk_find@PLT
	testl	%eax, %eax
	js	.L316
	movl	%eax, %r15d
	movl	(%rbx), %eax
	subl	$1, %eax
	cmpl	$1, %eax
	ja	.L329
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	movl	%eax, %r14d
	cmpl	%eax, %r15d
	jl	.L322
	.p2align 4,,10
	.p2align 3
.L316:
	xorl	%r12d, %r12d
.L312:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L332:
	.cfi_restore_state
	cmpl	$1, %edx
	je	.L317
	cmpl	$2, %edx
	je	.L330
.L319:
	cmpl	$1, %eax
	je	.L331
	cmpl	$2, %eax
	jne	.L312
	movq	8(%rbx), %rsi
	movq	8(%r12), %rdi
	call	X509_CRL_match@PLT
	testl	%eax, %eax
	je	.L312
.L323:
	addl	$1, %r15d
	cmpl	%r14d, %r15d
	je	.L316
.L322:
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movl	(%rax), %edx
	movq	%rax, %r12
	movl	(%rbx), %eax
	cmpl	%eax, %edx
	je	.L332
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L331:
	movq	8(%rbx), %rsi
	movq	8(%r12), %rdi
	call	X509_cmp@PLT
	testl	%eax, %eax
	jne	.L323
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L330:
	movq	8(%rbx), %rsi
	movq	8(%r12), %rdi
	call	X509_CRL_cmp@PLT
.L320:
	testl	%eax, %eax
	jne	.L316
	movl	(%rbx), %eax
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L317:
	movq	8(%rbx), %rsi
	movq	8(%r12), %rdi
	call	X509_subject_name_cmp@PLT
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L329:
	addq	$8, %rsp
	movl	%r15d, %esi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	OPENSSL_sk_value@PLT
	.cfi_endproc
.LFE1409:
	.size	X509_OBJECT_retrieve_match, .-X509_OBJECT_retrieve_match
	.p2align 4
	.globl	X509_STORE_add_crl
	.type	X509_STORE_add_crl, @function
X509_STORE_add_crl:
.LFB1393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testq	%rsi, %rsi
	je	.L335
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movl	$420, %edx
	movl	$16, %edi
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L350
	movl	$2, (%rax)
	movq	%r12, %rdi
	movq	%r12, 8(%rax)
	call	X509_CRL_up_ref@PLT
	testl	%eax, %eax
	je	.L351
	movq	144(%rbx), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	8(%rbx), %rdi
	movq	%r13, %rsi
	call	X509_OBJECT_retrieve_match
	testq	%rax, %rax
	je	.L338
	movq	144(%rbx), %rdi
	movl	$1, %r12d
	call	CRYPTO_THREAD_unlock@PLT
.L339:
	movl	0(%r13), %eax
	cmpl	$1, %eax
	je	.L341
	cmpl	$2, %eax
	jne	.L343
	movq	8(%r13), %rdi
	call	X509_CRL_free@PLT
.L343:
	movl	$471, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
.L340:
	testl	%r12d, %r12d
	jne	.L333
	.p2align 4,,10
	.p2align 3
.L335:
	movl	$380, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$125, %esi
	movl	$11, %edi
	call	ERR_put_error@PLT
.L333:
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L351:
	.cfi_restore_state
	movl	$0, 0(%r13)
	movl	$471, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L338:
	movq	8(%rbx), %rdi
	movq	%r13, %rsi
	xorl	%r12d, %r12d
	call	OPENSSL_sk_push@PLT
	movq	144(%rbx), %rdi
	testl	%eax, %eax
	movl	%eax, %r14d
	setne	%r12b
	call	CRYPTO_THREAD_unlock@PLT
	testl	%r14d, %r14d
	jne	.L340
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L341:
	movq	8(%r13), %rdi
	call	X509_free@PLT
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L350:
	movl	$423, %r8d
	movl	$65, %edx
	movl	$150, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L335
	.cfi_endproc
.LFE1393:
	.size	X509_STORE_add_crl, .-X509_STORE_add_crl
	.p2align 4
	.globl	X509_STORE_add_cert
	.type	X509_STORE_add_cert, @function
X509_STORE_add_cert:
.LFB1392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testq	%rsi, %rsi
	je	.L354
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movl	$420, %edx
	movl	$16, %edi
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L369
	movl	$1, (%rax)
	movq	%r12, %rdi
	movq	%r12, 8(%rax)
	call	X509_up_ref@PLT
	testl	%eax, %eax
	je	.L370
	movq	144(%rbx), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	8(%rbx), %rdi
	movq	%r13, %rsi
	call	X509_OBJECT_retrieve_match
	testq	%rax, %rax
	je	.L357
	movq	144(%rbx), %rdi
	movl	$1, %r12d
	call	CRYPTO_THREAD_unlock@PLT
.L358:
	movl	0(%r13), %eax
	cmpl	$1, %eax
	je	.L360
	cmpl	$2, %eax
	jne	.L362
	movq	8(%r13), %rdi
	call	X509_CRL_free@PLT
.L362:
	movl	$471, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
.L359:
	testl	%r12d, %r12d
	jne	.L352
	.p2align 4,,10
	.p2align 3
.L354:
	movl	$371, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$124, %esi
	movl	$11, %edi
	call	ERR_put_error@PLT
.L352:
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L370:
	.cfi_restore_state
	movl	$0, 0(%r13)
	movl	$471, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L357:
	movq	8(%rbx), %rdi
	movq	%r13, %rsi
	xorl	%r12d, %r12d
	call	OPENSSL_sk_push@PLT
	movq	144(%rbx), %rdi
	testl	%eax, %eax
	movl	%eax, %r14d
	setne	%r12b
	call	CRYPTO_THREAD_unlock@PLT
	testl	%r14d, %r14d
	jne	.L359
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L360:
	movq	8(%r13), %rdi
	call	X509_free@PLT
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L369:
	movl	$423, %r8d
	movl	$65, %edx
	movl	$150, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L354
	.cfi_endproc
.LFE1392:
	.size	X509_STORE_add_cert, .-X509_STORE_add_cert
	.p2align 4
	.globl	X509_STORE_CTX_get1_issuer
	.type	X509_STORE_CTX_get1_issuer, @function
X509_STORE_CTX_get1_issuer:
.LFB1410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	movl	$420, %edx
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	leaq	.LC0(%rip), %rsi
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -64(%rbp)
	movl	$16, %edi
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L410
	movl	$0, (%rax)
	movq	%rax, %rbx
	movq	-64(%rbp), %rax
	movq	%r14, %rdi
	movq	(%r12), %r13
	movq	$0, (%rax)
	call	X509_get_issuer_name@PLT
	movq	%rbx, %rcx
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, -56(%rbp)
	call	X509_STORE_CTX_get_by_subject
	cmpl	$1, %eax
	jne	.L411
	movq	8(%rbx), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*72(%r12)
	testl	%eax, %eax
	je	.L378
	movq	8(%rbx), %rsi
	movl	$-1, %edx
	movq	%r12, %rdi
	call	x509_check_cert_time@PLT
	testl	%eax, %eax
	jne	.L412
.L378:
	movl	(%rbx), %eax
	cmpl	$1, %eax
	je	.L383
	cmpl	$2, %eax
	je	.L384
.L385:
	movl	$471, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rbx, %rdi
	call	CRYPTO_free@PLT
	movl	$0, -68(%rbp)
	testq	%r13, %r13
	je	.L371
	movq	144(%r13), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	8(%r13), %rdi
	movq	-56(%rbp), %rdx
	xorl	%ecx, %ecx
	movl	$1, %esi
	call	x509_object_idx_cnt
	movl	%eax, %ebx
	cmpl	$-1, %eax
	jne	.L386
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L388:
	addl	$1, %ebx
.L386:
	movq	8(%r13), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L387
	movq	8(%r13), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	cmpl	$1, (%rax)
	movq	%rax, %r15
	jne	.L387
	movq	8(%rax), %rdi
	call	X509_get_subject_name@PLT
	movq	-56(%rbp), %rdi
	movq	%rax, %rsi
	call	X509_NAME_cmp@PLT
	testl	%eax, %eax
	jne	.L387
	movq	8(%r15), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*72(%r12)
	testl	%eax, %eax
	je	.L388
	movq	8(%r15), %rsi
	movq	-64(%rbp), %rax
	movl	$-1, %edx
	movq	%r12, %rdi
	movq	%rsi, (%rax)
	call	x509_check_cert_time@PLT
	movl	$1, -68(%rbp)
	testl	%eax, %eax
	je	.L388
	.p2align 4,,10
	.p2align 3
.L387:
	movq	-64(%rbp), %rbx
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L390
	call	X509_up_ref@PLT
	testl	%eax, %eax
	jne	.L390
	movq	$0, (%rbx)
	movl	$-1, -68(%rbp)
.L390:
	movq	144(%r13), %rdi
	call	CRYPTO_THREAD_unlock@PLT
.L371:
	movl	-68(%rbp), %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L411:
	.cfi_restore_state
	movl	(%rbx), %eax
	cmpl	$1, %eax
	je	.L375
	cmpl	$2, %eax
	je	.L376
.L377:
	movl	$471, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rbx, %rdi
	call	CRYPTO_free@PLT
	movl	$0, -68(%rbp)
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L384:
	movq	8(%rbx), %rdi
	call	X509_CRL_free@PLT
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L383:
	movq	8(%rbx), %rdi
	call	X509_free@PLT
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L376:
	movq	8(%rbx), %rdi
	call	X509_CRL_free@PLT
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L375:
	movq	8(%rbx), %rdi
	call	X509_free@PLT
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L412:
	movq	8(%rbx), %rdi
	movq	-64(%rbp), %r14
	movq	%rdi, (%r14)
	call	X509_up_ref@PLT
	movl	$1, -68(%rbp)
	testl	%eax, %eax
	jne	.L379
	movq	$0, (%r14)
	movl	$-1, -68(%rbp)
.L379:
	movl	(%rbx), %eax
	cmpl	$1, %eax
	je	.L380
	cmpl	$2, %eax
	je	.L381
.L382:
	movl	$471, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rbx, %rdi
	call	CRYPTO_free@PLT
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L381:
	movq	8(%rbx), %rdi
	call	X509_CRL_free@PLT
	jmp	.L382
.L380:
	movq	8(%rbx), %rdi
	call	X509_free@PLT
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L410:
	movl	$423, %r8d
	movl	$65, %edx
	movl	$150, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, -68(%rbp)
	jmp	.L371
	.cfi_endproc
.LFE1410:
	.size	X509_STORE_CTX_get1_issuer, .-X509_STORE_CTX_get1_issuer
	.p2align 4
	.globl	X509_STORE_set_flags
	.type	X509_STORE_set_flags, @function
X509_STORE_set_flags:
.LFB1411:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdi
	jmp	X509_VERIFY_PARAM_set_flags@PLT
	.cfi_endproc
.LFE1411:
	.size	X509_STORE_set_flags, .-X509_STORE_set_flags
	.p2align 4
	.globl	X509_STORE_set_depth
	.type	X509_STORE_set_depth, @function
X509_STORE_set_depth:
.LFB1412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	24(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509_VERIFY_PARAM_set_depth@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1412:
	.size	X509_STORE_set_depth, .-X509_STORE_set_depth
	.p2align 4
	.globl	X509_STORE_set_purpose
	.type	X509_STORE_set_purpose, @function
X509_STORE_set_purpose:
.LFB1413:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdi
	jmp	X509_VERIFY_PARAM_set_purpose@PLT
	.cfi_endproc
.LFE1413:
	.size	X509_STORE_set_purpose, .-X509_STORE_set_purpose
	.p2align 4
	.globl	X509_STORE_set_trust
	.type	X509_STORE_set_trust, @function
X509_STORE_set_trust:
.LFB1414:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdi
	jmp	X509_VERIFY_PARAM_set_trust@PLT
	.cfi_endproc
.LFE1414:
	.size	X509_STORE_set_trust, .-X509_STORE_set_trust
	.p2align 4
	.globl	X509_STORE_set1_param
	.type	X509_STORE_set1_param, @function
X509_STORE_set1_param:
.LFB1415:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdi
	jmp	X509_VERIFY_PARAM_set1@PLT
	.cfi_endproc
.LFE1415:
	.size	X509_STORE_set1_param, .-X509_STORE_set1_param
	.p2align 4
	.globl	X509_STORE_get0_param
	.type	X509_STORE_get0_param, @function
X509_STORE_get0_param:
.LFB1416:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE1416:
	.size	X509_STORE_get0_param, .-X509_STORE_get0_param
	.p2align 4
	.globl	X509_STORE_set_verify
	.type	X509_STORE_set_verify, @function
X509_STORE_set_verify:
.LFB1417:
	.cfi_startproc
	endbr64
	movq	%rsi, 32(%rdi)
	ret
	.cfi_endproc
.LFE1417:
	.size	X509_STORE_set_verify, .-X509_STORE_set_verify
	.p2align 4
	.globl	X509_STORE_get_verify
	.type	X509_STORE_get_verify, @function
X509_STORE_get_verify:
.LFB1418:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE1418:
	.size	X509_STORE_get_verify, .-X509_STORE_get_verify
	.p2align 4
	.globl	X509_STORE_set_verify_cb
	.type	X509_STORE_set_verify_cb, @function
X509_STORE_set_verify_cb:
.LFB1419:
	.cfi_startproc
	endbr64
	movq	%rsi, 40(%rdi)
	ret
	.cfi_endproc
.LFE1419:
	.size	X509_STORE_set_verify_cb, .-X509_STORE_set_verify_cb
	.p2align 4
	.globl	X509_STORE_get_verify_cb
	.type	X509_STORE_get_verify_cb, @function
X509_STORE_get_verify_cb:
.LFB1420:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE1420:
	.size	X509_STORE_get_verify_cb, .-X509_STORE_get_verify_cb
	.p2align 4
	.globl	X509_STORE_set_get_issuer
	.type	X509_STORE_set_get_issuer, @function
X509_STORE_set_get_issuer:
.LFB1421:
	.cfi_startproc
	endbr64
	movq	%rsi, 48(%rdi)
	ret
	.cfi_endproc
.LFE1421:
	.size	X509_STORE_set_get_issuer, .-X509_STORE_set_get_issuer
	.p2align 4
	.globl	X509_STORE_get_get_issuer
	.type	X509_STORE_get_get_issuer, @function
X509_STORE_get_get_issuer:
.LFB1422:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	ret
	.cfi_endproc
.LFE1422:
	.size	X509_STORE_get_get_issuer, .-X509_STORE_get_get_issuer
	.p2align 4
	.globl	X509_STORE_set_check_issued
	.type	X509_STORE_set_check_issued, @function
X509_STORE_set_check_issued:
.LFB1423:
	.cfi_startproc
	endbr64
	movq	%rsi, 56(%rdi)
	ret
	.cfi_endproc
.LFE1423:
	.size	X509_STORE_set_check_issued, .-X509_STORE_set_check_issued
	.p2align 4
	.globl	X509_STORE_get_check_issued
	.type	X509_STORE_get_check_issued, @function
X509_STORE_get_check_issued:
.LFB1424:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rax
	ret
	.cfi_endproc
.LFE1424:
	.size	X509_STORE_get_check_issued, .-X509_STORE_get_check_issued
	.p2align 4
	.globl	X509_STORE_set_check_revocation
	.type	X509_STORE_set_check_revocation, @function
X509_STORE_set_check_revocation:
.LFB1425:
	.cfi_startproc
	endbr64
	movq	%rsi, 64(%rdi)
	ret
	.cfi_endproc
.LFE1425:
	.size	X509_STORE_set_check_revocation, .-X509_STORE_set_check_revocation
	.p2align 4
	.globl	X509_STORE_get_check_revocation
	.type	X509_STORE_get_check_revocation, @function
X509_STORE_get_check_revocation:
.LFB1426:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	ret
	.cfi_endproc
.LFE1426:
	.size	X509_STORE_get_check_revocation, .-X509_STORE_get_check_revocation
	.p2align 4
	.globl	X509_STORE_set_get_crl
	.type	X509_STORE_set_get_crl, @function
X509_STORE_set_get_crl:
.LFB1427:
	.cfi_startproc
	endbr64
	movq	%rsi, 72(%rdi)
	ret
	.cfi_endproc
.LFE1427:
	.size	X509_STORE_set_get_crl, .-X509_STORE_set_get_crl
	.p2align 4
	.globl	X509_STORE_get_get_crl
	.type	X509_STORE_get_get_crl, @function
X509_STORE_get_get_crl:
.LFB1428:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rax
	ret
	.cfi_endproc
.LFE1428:
	.size	X509_STORE_get_get_crl, .-X509_STORE_get_get_crl
	.p2align 4
	.globl	X509_STORE_set_check_crl
	.type	X509_STORE_set_check_crl, @function
X509_STORE_set_check_crl:
.LFB1429:
	.cfi_startproc
	endbr64
	movq	%rsi, 80(%rdi)
	ret
	.cfi_endproc
.LFE1429:
	.size	X509_STORE_set_check_crl, .-X509_STORE_set_check_crl
	.p2align 4
	.globl	X509_STORE_get_check_crl
	.type	X509_STORE_get_check_crl, @function
X509_STORE_get_check_crl:
.LFB1430:
	.cfi_startproc
	endbr64
	movq	80(%rdi), %rax
	ret
	.cfi_endproc
.LFE1430:
	.size	X509_STORE_get_check_crl, .-X509_STORE_get_check_crl
	.p2align 4
	.globl	X509_STORE_set_cert_crl
	.type	X509_STORE_set_cert_crl, @function
X509_STORE_set_cert_crl:
.LFB1431:
	.cfi_startproc
	endbr64
	movq	%rsi, 88(%rdi)
	ret
	.cfi_endproc
.LFE1431:
	.size	X509_STORE_set_cert_crl, .-X509_STORE_set_cert_crl
	.p2align 4
	.globl	X509_STORE_get_cert_crl
	.type	X509_STORE_get_cert_crl, @function
X509_STORE_get_cert_crl:
.LFB1432:
	.cfi_startproc
	endbr64
	movq	88(%rdi), %rax
	ret
	.cfi_endproc
.LFE1432:
	.size	X509_STORE_get_cert_crl, .-X509_STORE_get_cert_crl
	.p2align 4
	.globl	X509_STORE_set_check_policy
	.type	X509_STORE_set_check_policy, @function
X509_STORE_set_check_policy:
.LFB1433:
	.cfi_startproc
	endbr64
	movq	%rsi, 96(%rdi)
	ret
	.cfi_endproc
.LFE1433:
	.size	X509_STORE_set_check_policy, .-X509_STORE_set_check_policy
	.p2align 4
	.globl	X509_STORE_get_check_policy
	.type	X509_STORE_get_check_policy, @function
X509_STORE_get_check_policy:
.LFB1434:
	.cfi_startproc
	endbr64
	movq	96(%rdi), %rax
	ret
	.cfi_endproc
.LFE1434:
	.size	X509_STORE_get_check_policy, .-X509_STORE_get_check_policy
	.p2align 4
	.globl	X509_STORE_set_lookup_certs
	.type	X509_STORE_set_lookup_certs, @function
X509_STORE_set_lookup_certs:
.LFB1435:
	.cfi_startproc
	endbr64
	movq	%rsi, 104(%rdi)
	ret
	.cfi_endproc
.LFE1435:
	.size	X509_STORE_set_lookup_certs, .-X509_STORE_set_lookup_certs
	.p2align 4
	.globl	X509_STORE_get_lookup_certs
	.type	X509_STORE_get_lookup_certs, @function
X509_STORE_get_lookup_certs:
.LFB1436:
	.cfi_startproc
	endbr64
	movq	104(%rdi), %rax
	ret
	.cfi_endproc
.LFE1436:
	.size	X509_STORE_get_lookup_certs, .-X509_STORE_get_lookup_certs
	.p2align 4
	.globl	X509_STORE_set_lookup_crls
	.type	X509_STORE_set_lookup_crls, @function
X509_STORE_set_lookup_crls:
.LFB1437:
	.cfi_startproc
	endbr64
	movq	%rsi, 112(%rdi)
	ret
	.cfi_endproc
.LFE1437:
	.size	X509_STORE_set_lookup_crls, .-X509_STORE_set_lookup_crls
	.p2align 4
	.globl	X509_STORE_get_lookup_crls
	.type	X509_STORE_get_lookup_crls, @function
X509_STORE_get_lookup_crls:
.LFB1438:
	.cfi_startproc
	endbr64
	movq	112(%rdi), %rax
	ret
	.cfi_endproc
.LFE1438:
	.size	X509_STORE_get_lookup_crls, .-X509_STORE_get_lookup_crls
	.p2align 4
	.globl	X509_STORE_set_cleanup
	.type	X509_STORE_set_cleanup, @function
X509_STORE_set_cleanup:
.LFB1439:
	.cfi_startproc
	endbr64
	movq	%rsi, 120(%rdi)
	ret
	.cfi_endproc
.LFE1439:
	.size	X509_STORE_set_cleanup, .-X509_STORE_set_cleanup
	.p2align 4
	.globl	X509_STORE_get_cleanup
	.type	X509_STORE_get_cleanup, @function
X509_STORE_get_cleanup:
.LFB1440:
	.cfi_startproc
	endbr64
	movq	120(%rdi), %rax
	ret
	.cfi_endproc
.LFE1440:
	.size	X509_STORE_get_cleanup, .-X509_STORE_get_cleanup
	.p2align 4
	.globl	X509_STORE_set_ex_data
	.type	X509_STORE_set_ex_data, @function
X509_STORE_set_ex_data:
.LFB1441:
	.cfi_startproc
	endbr64
	subq	$-128, %rdi
	jmp	CRYPTO_set_ex_data@PLT
	.cfi_endproc
.LFE1441:
	.size	X509_STORE_set_ex_data, .-X509_STORE_set_ex_data
	.p2align 4
	.globl	X509_STORE_get_ex_data
	.type	X509_STORE_get_ex_data, @function
X509_STORE_get_ex_data:
.LFB1442:
	.cfi_startproc
	endbr64
	subq	$-128, %rdi
	jmp	CRYPTO_get_ex_data@PLT
	.cfi_endproc
.LFE1442:
	.size	X509_STORE_get_ex_data, .-X509_STORE_get_ex_data
	.p2align 4
	.globl	X509_STORE_CTX_get0_store
	.type	X509_STORE_CTX_get0_store, @function
X509_STORE_CTX_get0_store:
.LFB1443:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE1443:
	.size	X509_STORE_CTX_get0_store, .-X509_STORE_CTX_get0_store
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
