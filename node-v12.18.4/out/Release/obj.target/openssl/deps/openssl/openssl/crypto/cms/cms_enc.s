	.file	"cms_enc.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/cms/cms_enc.c"
	.text
	.p2align 4
	.globl	cms_EncryptedContent_init_bio
	.type	cms_EncryptedContent_init_bio, @function
cms_EncryptedContent_init_bio:
.LFB1440:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	24(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	%rax, -104(%rbp)
	call	BIO_f_cipher@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L46
	xorl	%edx, %edx
	leaq	-88(%rbp), %rcx
	movl	$129, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	testq	%r15, %r15
	je	.L4
	cmpq	$0, 32(%rbx)
	movq	24(%rbx), %rsi
	je	.L5
	movq	$0, 24(%rbx)
.L5:
	movq	-88(%rbp), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r9d
	call	EVP_CipherInit_ex@PLT
	testl	%eax, %eax
	jle	.L27
	movq	-88(%rbp), %rdi
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_type@PLT
	movl	%eax, %edi
	call	OBJ_nid2obj@PLT
	movq	-104(%rbp), %rdx
	movq	-88(%rbp), %rdi
	movq	%rax, (%rdx)
	call	EVP_CIPHER_CTX_iv_length@PLT
	movq	$0, -112(%rbp)
	movl	%eax, %esi
	testl	%eax, %eax
	jg	.L47
.L8:
	movq	-88(%rbp), %rdi
	call	EVP_CIPHER_CTX_key_length@PLT
	cmpq	$0, 32(%rbx)
	movslq	%eax, %r13
	je	.L26
.L42:
	xorl	%r14d, %r14d
.L11:
	movq	40(%rbx), %rsi
	movq	-88(%rbp), %rdi
	cmpq	%r13, %rsi
	je	.L48
	call	EVP_CIPHER_CTX_set_key_length@PLT
	testl	%eax, %eax
	jle	.L17
	movq	32(%rbx), %rcx
	movq	-88(%rbp), %rdi
.L16:
	xorl	%r9d, %r9d
	movq	-112(%rbp), %r8
	testq	%r15, %r15
	setne	%r9b
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	EVP_CipherInit_ex@PLT
	testl	%eax, %eax
	jle	.L13
	testq	%r15, %r15
	je	.L20
	xorl	%r15d, %r15d
.L14:
	call	ASN1_TYPE_new@PLT
	movq	%rax, %rsi
	movq	-104(%rbp), %rax
	movq	%rsi, 8(%rax)
	testq	%rsi, %rsi
	je	.L49
	movq	-88(%rbp), %rdi
	call	EVP_CIPHER_param_to_asn1@PLT
	testl	%eax, %eax
	jle	.L50
	movq	-104(%rbp), %rax
	movq	8(%rax), %rdi
	cmpl	$-1, (%rdi)
	je	.L51
.L24:
	cmpl	$1, %r15d
	jne	.L20
.L44:
	movl	$158, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	CRYPTO_clear_free@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L47:
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jle	.L32
	movq	%r13, -112(%rbp)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L4:
	movq	-104(%rbp), %rax
	movq	(%rax), %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_cipherbyname@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L52
	movq	-88(%rbp), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	EVP_CipherInit_ex@PLT
	testl	%eax, %eax
	jle	.L27
	movq	-104(%rbp), %rax
	movq	-88(%rbp), %rdi
	movq	8(%rax), %rsi
	call	EVP_CIPHER_asn1_to_param@PLT
	testl	%eax, %eax
	jle	.L53
	movq	-88(%rbp), %rdi
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	$0, -112(%rbp)
	movslq	%eax, %r13
.L26:
	movl	$87, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_malloc@PLT
	movl	$89, %r8d
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L43
	movq	-88(%rbp), %rdi
	movq	%rax, %rsi
	call	EVP_CIPHER_CTX_rand_key@PLT
	testl	%eax, %eax
	jle	.L7
	cmpq	$0, 32(%rbx)
	jne	.L11
	movq	%r14, 32(%rbx)
	movq	%r13, 40(%rbx)
	testq	%r15, %r15
	jne	.L54
	call	ERR_clear_error@PLT
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L17:
	testq	%r15, %r15
	jne	.L18
	movl	48(%rbx), %eax
	testl	%eax, %eax
	je	.L19
.L18:
	movl	$115, %r8d
	movl	$118, %edx
	movl	$120, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	movq	40(%rbx), %rsi
	movq	32(%rbx), %rdi
	movl	$155, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movl	$158, %ecx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	$0, 32(%rbx)
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	BIO_free@PLT
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L55
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	movl	$101, %edx
	movl	$120, %esi
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	movl	$64, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L32:
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L48:
	movq	32(%rbx), %rcx
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L49:
	movl	$137, %r8d
.L43:
	movl	$65, %edx
	movl	$120, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L46:
	movl	$41, %r8d
	movl	$65, %edx
	movl	$120, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L19:
	movq	40(%rbx), %rsi
	movq	32(%rbx), %rdi
	movl	$120, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movq	%r14, 32(%rbx)
	xorl	%r14d, %r14d
	movq	%r13, 40(%rbx)
	call	ERR_clear_error@PLT
	movq	32(%rbx), %rcx
	movq	-112(%rbp), %r8
	xorl	%r9d, %r9d
	movq	-88(%rbp), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	EVP_CipherInit_ex@PLT
	testl	%eax, %eax
	jg	.L20
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$130, %r8d
	movl	$101, %edx
	movl	$120, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L7
.L20:
	movq	40(%rbx), %rsi
	movq	32(%rbx), %rdi
	movl	$155, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movq	$0, 32(%rbx)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L54:
	movq	-112(%rbp), %r8
	movq	-88(%rbp), %rdi
	movq	%r14, %rcx
	xorl	%edx, %edx
	movl	$1, %r9d
	xorl	%esi, %esi
	xorl	%r14d, %r14d
	call	EVP_CipherInit_ex@PLT
	testl	%eax, %eax
	jle	.L13
	movl	$1, %r15d
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L53:
	movl	$102, %edx
	movl	$120, %esi
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	movl	$80, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L51:
	call	ASN1_TYPE_free@PLT
	movq	-104(%rbp), %rax
	movq	$0, 8(%rax)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L50:
	movl	$141, %r8d
	movl	$102, %edx
	movl	$120, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L52:
	movl	$58, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$148, %edx
	xorl	%r13d, %r13d
	movl	$120, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L7
.L55:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1440:
	.size	cms_EncryptedContent_init_bio, .-cms_EncryptedContent_init_bio
	.p2align 4
	.globl	cms_EncryptedContent_init
	.type	cms_EncryptedContent_init, @function
cms_EncryptedContent_init:
.LFB1441:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, 24(%rdi)
	testq	%rdx, %rdx
	je	.L57
	movq	%rcx, %rdi
	movq	%rdx, %r14
	leaq	.LC0(%rip), %rsi
	movl	$171, %edx
	call	CRYPTO_malloc@PLT
	movq	%rax, 32(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L66
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
.L57:
	movq	%r13, 40(%rbx)
	movl	$1, %r13d
	testq	%r12, %r12
	je	.L56
	movl	$21, %edi
	call	OBJ_nid2obj@PLT
	movq	%rax, (%rbx)
.L56:
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movl	$172, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$179, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L56
	.cfi_endproc
.LFE1441:
	.size	cms_EncryptedContent_init, .-cms_EncryptedContent_init
	.p2align 4
	.globl	CMS_EncryptedData_set1_key
	.type	CMS_EncryptedData_set1_key, @function
CMS_EncryptedData_set1_key:
.LFB1442:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testq	%rdx, %rdx
	je	.L77
	movq	%rcx, %rbx
	testq	%rcx, %rcx
	je	.L77
	movq	%rdi, %r12
	movq	%rsi, %r14
	movq	%rdx, %r13
	testq	%rsi, %rsi
	je	.L71
	leaq	CMS_EncryptedData_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L87
	movl	$26, %edi
	call	OBJ_nid2obj@PLT
	movq	%rbx, %rdi
	movl	$171, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rax, (%r12)
	movq	8(%r12), %rax
	movq	8(%rax), %r12
	movl	$0, (%rax)
	movq	%r14, 24(%r12)
	call	CRYPTO_malloc@PLT
	movq	%rax, 32(%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L73
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	%rbx, 40(%r12)
	movl	$21, %edi
	call	OBJ_nid2obj@PLT
	movq	%rax, (%r12)
	movl	$1, %eax
.L67:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$26, %eax
	jne	.L88
	movq	8(%r12), %rax
	movq	%rbx, %rdi
	movl	$171, %edx
	leaq	.LC0(%rip), %rsi
	movq	8(%rax), %r12
	movq	$0, 24(%r12)
	call	CRYPTO_malloc@PLT
	movq	%rax, 32(%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L73
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	%rbx, 40(%r12)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	movl	$188, %r8d
	movl	$130, %edx
	movl	$123, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	movl	$194, %r8d
	movl	$65, %edx
	movl	$123, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	movl	$200, %r8d
	movl	$122, %edx
	movl	$123, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L73:
	movl	$172, %r8d
	movl	$65, %edx
	movl	$179, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1442:
	.size	CMS_EncryptedData_set1_key, .-CMS_EncryptedData_set1_key
	.p2align 4
	.globl	cms_EncryptedData_init_bio
	.type	cms_EncryptedData_init_bio, @function
cms_EncryptedData_init_bio:
.LFB1443:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	8(%rax), %rdi
	cmpq	$0, 24(%rdi)
	je	.L90
	cmpq	$0, 16(%rax)
	je	.L90
	movl	$2, (%rax)
.L90:
	jmp	cms_EncryptedContent_init_bio
	.cfi_endproc
.LFE1443:
	.size	cms_EncryptedData_init_bio, .-cms_EncryptedData_init_bio
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
