	.file	"statem_lib.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/ssl/statem/statem_lib.c"
	.text
	.p2align 4
	.type	ssl_add_cert_to_wpacket, @function
ssl_add_cert_to_wpacket:
.LFB1014:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	xorl	%esi, %esi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rdx, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movl	%ecx, -68(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	i2d_X509@PLT
	movl	$889, %r9d
	movl	$7, %ecx
	leaq	.LC0(%rip), %r8
	testl	%eax, %eax
	js	.L16
	leaq	-64(%rbp), %r15
	movslq	%eax, %rsi
	movl	$3, %ecx
	movq	%r14, %rdi
	movq	%r15, %rdx
	movl	%eax, %ebx
	call	WPACKET_sub_allocate_bytes__@PLT
	testl	%eax, %eax
	je	.L5
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	i2d_X509@PLT
	cmpl	%ebx, %eax
	jne	.L5
	movq	8(%r13), %rdx
	movl	$1, %eax
	movq	192(%rdx), %rcx
	testb	$8, 96(%rcx)
	jne	.L1
	movl	(%rdx), %eax
	cmpl	$771, %eax
	jle	.L8
	cmpl	$65536, %eax
	jne	.L18
.L8:
	movl	$1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$895, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
.L16:
	movl	$493, %edx
	movl	$80, %esi
	movq	%r13, %rdi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L1:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L19
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	movslq	-68(%rbp), %r8
	movq	%r12, %rcx
	movl	$4096, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	tls_construct_extensions@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L1
.L19:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1014:
	.size	ssl_add_cert_to_wpacket, .-ssl_add_cert_to_wpacket
	.p2align 4
	.type	ca_dn_cmp, @function
ca_dn_cmp:
.LFB1035:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rsi
	movq	(%rdi), %rdi
	jmp	X509_NAME_cmp@PLT
	.cfi_endproc
.LFE1035:
	.size	ca_dn_cmp, .-ca_dn_cmp
	.p2align 4
	.type	get_cert_verify_tbs_data, @function
get_cert_verify_tbs_data:
.LFB1004:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L22
	movl	(%rax), %eax
	cmpl	$65536, %eax
	je	.L22
	cmpl	$771, %eax
	jg	.L40
.L22:
	movq	168(%r12), %rax
	xorl	%edx, %edx
	movq	%r14, %rcx
	movl	$3, %esi
	movq	224(%rax), %rdi
	call	BIO_ctrl@PLT
	testq	%rax, %rax
	jle	.L41
	movq	%rax, 0(%r13)
	movl	$1, %eax
.L21:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L42
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	movdqa	.LC1(%rip), %xmm0
	movq	%rsi, %rbx
	movaps	%xmm0, (%rsi)
	movaps	%xmm0, 16(%rsi)
	movaps	%xmm0, 32(%rsi)
	movaps	%xmm0, 48(%rsi)
	movl	92(%rdi), %eax
	subl	$39, %eax
	cmpl	$1, %eax
	jbe	.L43
	movzwl	32+clientcontext.24096(%rip), %eax
	movdqa	clientcontext.24096(%rip), %xmm1
	movdqa	16+clientcontext.24096(%rip), %xmm2
	movw	%ax, 96(%rsi)
	movaps	%xmm1, 64(%rsi)
	movaps	%xmm2, 80(%rsi)
.L24:
	movl	92(%r12), %eax
	leaq	98(%rbx), %rdi
	cmpl	$39, %eax
	je	.L32
	cmpl	$29, %eax
	je	.L32
	movl	$64, %edx
	movq	%rdi, %rsi
	leaq	-48(%rbp), %rcx
	movq	%r12, %rdi
	call	ssl_handshake_hash@PLT
	movq	-48(%rbp), %rdx
	testl	%eax, %eax
	je	.L21
.L27:
	addq	$98, %rdx
	movq	%rbx, (%r14)
	movl	$1, %eax
	movq	%rdx, 0(%r13)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L41:
	movl	$220, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$588, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L32:
	movq	1240(%r12), %rdx
	leaq	1176(%r12), %rsi
	call	memcpy@PLT
	movq	1240(%r12), %rdx
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L43:
	movdqa	servercontext.24095(%rip), %xmm3
	movdqa	16+servercontext.24095(%rip), %xmm4
	movzwl	32+servercontext.24095(%rip), %eax
	movaps	%xmm3, 64(%rsi)
	movw	%ax, 96(%rsi)
	movaps	%xmm4, 80(%rsi)
	jmp	.L24
.L42:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1004:
	.size	get_cert_verify_tbs_data, .-get_cert_verify_tbs_data
	.p2align 4
	.globl	ssl3_do_write
	.type	ssl3_do_write, @function
ssl3_do_write:
.LFB1001:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	leaq	-32(%rbp), %r8
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	152(%rdi), %rcx
	movq	160(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	136(%rdi), %rax
	movq	$0, -32(%rbp)
	addq	8(%rax), %rdx
	call	ssl3_write_bytes@PLT
	testl	%eax, %eax
	js	.L52
	cmpl	$22, %r12d
	je	.L47
.L51:
	movq	152(%rbx), %r8
	movq	-32(%rbp), %rax
	cmpq	%rax, %r8
	je	.L67
	subq	%rax, %r8
	addq	%rax, 160(%rbx)
	xorl	%eax, %eax
	movq	%r8, 152(%rbx)
.L44:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L68
	leaq	-16(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	movq	8(%rbx), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L50
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L50
	cmpl	$65536, %eax
	je	.L50
	movl	92(%rbx), %eax
	leal	-42(%rax), %edx
	cmpl	$1, %edx
	jbe	.L51
	cmpl	$33, %eax
	je	.L51
	.p2align 4,,10
	.p2align 3
.L50:
	movq	136(%rbx), %rax
	movq	-32(%rbp), %rdx
	movq	%rbx, %rdi
	movq	160(%rbx), %rsi
	addq	8(%rax), %rsi
	call	ssl3_finish_mac@PLT
	testl	%eax, %eax
	jne	.L51
.L52:
	movl	$-1, %eax
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L67:
	movq	184(%rbx), %r10
	movl	$1, %eax
	testq	%r10, %r10
	je	.L44
	movq	136(%rbx), %rdx
	subq	$8, %rsp
	movl	(%rbx), %esi
	movl	%eax, -36(%rbp)
	movq	%rbx, %r9
	addq	160(%rbx), %r8
	movl	$1, %edi
	movq	8(%rdx), %rcx
	pushq	192(%rbx)
	movl	%r12d, %edx
	call	*%r10
	popq	%rax
	movl	-36(%rbp), %eax
	popq	%rdx
	jmp	.L44
.L68:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1001:
	.size	ssl3_do_write, .-ssl3_do_write
	.p2align 4
	.globl	tls_close_construct_packet
	.type	tls_close_construct_packet, @function
tls_close_construct_packet:
.LFB1002:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$257, %edx
	je	.L73
	movq	%rsi, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L71
.L73:
	leaq	-32(%rbp), %rsi
	movq	%r12, %rdi
	call	WPACKET_get_length@PLT
	testl	%eax, %eax
	je	.L71
	movq	-32(%rbp), %rax
	cmpq	$2147483647, %rax
	ja	.L71
	movq	%rax, 152(%rbx)
	movl	$1, %eax
	movq	$0, 160(%rbx)
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L71:
	xorl	%eax, %eax
.L69:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L81
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L81:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1002:
	.size	tls_close_construct_packet, .-tls_close_construct_packet
	.p2align 4
	.globl	tls_construct_cert_verify
	.type	tls_construct_cert_verify, @function
tls_construct_cert_verify:
.LFB1005:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movq	$0, -264(%rbp)
	movq	$0, -256(%rbp)
	movq	728(%rax), %rbx
	movq	$0, -248(%rbp)
	movq	$0, -240(%rbp)
	testq	%rbx, %rbx
	je	.L83
	movq	736(%rax), %rax
	testq	%rax, %rax
	je	.L83
	movq	8(%rax), %r15
	testq	%r15, %r15
	je	.L87
	movq	%rsi, %r13
	movq	%rbx, %rdi
	leaq	-264(%rbp), %rsi
	call	tls1_lookup_md@PLT
	testl	%eax, %eax
	jne	.L122
.L87:
	movl	$250, %r9d
.L119:
	movl	$68, %ecx
	movl	$496, %edx
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%r10d, %r10d
.L85:
	movq	%r10, %rdi
	movl	$340, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r14, %rdi
	call	EVP_MD_CTX_free@PLT
	xorl	%eax, %eax
.L82:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L123
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_restore_state
	call	EVP_MD_CTX_new@PLT
	movl	$257, %r9d
	movl	$65, %ecx
	leaq	.LC0(%rip), %r8
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L120
	leaq	-248(%rbp), %rcx
	leaq	-232(%rbp), %rdx
	movq	%r12, %rdi
	leaq	-224(%rbp), %rsi
	call	get_cert_verify_tbs_data
	testl	%eax, %eax
	je	.L102
	movq	8(%r12), %rax
	movq	192(%rax), %rax
	testb	$2, 96(%rax)
	je	.L89
	movzwl	8(%rbx), %esi
	movl	$2, %edx
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L124
.L89:
	movq	%r15, %rdi
	call	EVP_PKEY_size@PLT
	movl	$274, %edx
	leaq	.LC0(%rip), %rsi
	movslq	%eax, %rdi
	movq	%rdi, -240(%rbp)
	call	CRYPTO_malloc@PLT
	movl	$276, %r9d
	movl	$65, %ecx
	leaq	.LC0(%rip), %r8
	movq	%rax, -280(%rbp)
	testq	%rax, %rax
	je	.L121
	movq	-264(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%r15, %r8
	movq	%r14, %rdi
	leaq	-256(%rbp), %rsi
	call	EVP_DigestSignInit@PLT
	movl	$282, %r9d
	movl	$6, %ecx
	movq	-280(%rbp), %r10
	testl	%eax, %eax
	leaq	.LC0(%rip), %r8
	jle	.L121
	cmpl	$912, 20(%rbx)
	je	.L125
.L92:
	cmpl	$768, (%r12)
	movq	-248(%rbp), %r8
	movq	-232(%rbp), %rcx
	je	.L126
	movq	%r10, %rsi
	leaq	-240(%rbp), %rdx
	movq	%r14, %rdi
	movq	%r10, -280(%rbp)
	call	EVP_DigestSign@PLT
	movl	$308, %r9d
	movl	$6, %ecx
	movq	-280(%rbp), %r10
	testl	%eax, %eax
	leaq	.LC0(%rip), %r8
	jle	.L121
.L98:
	movq	-240(%rbp), %rdx
	movq	%r10, %rsi
	movl	$2, %ecx
	movq	%r13, %rdi
	movq	%r10, -280(%rbp)
	call	WPACKET_sub_memcpy__@PLT
	movq	-280(%rbp), %r10
	testl	%eax, %eax
	je	.L127
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%r10, -280(%rbp)
	call	ssl3_digest_cached_records@PLT
	movq	-280(%rbp), %r10
	testl	%eax, %eax
	je	.L85
	movq	%r10, %rdi
	movl	$336, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r14, %rdi
	call	EVP_MD_CTX_free@PLT
	movl	$1, %eax
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L83:
	movl	$243, %r9d
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L102:
	xorl	%r10d, %r10d
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L126:
	movq	%r8, %rdx
	movq	%rcx, %rsi
	movq	%r14, %rdi
	movq	%r10, -280(%rbp)
	call	EVP_DigestUpdate@PLT
	movq	-280(%rbp), %r10
	testl	%eax, %eax
	jle	.L97
	movq	1296(%r12), %rax
	movl	$29, %esi
	movq	%r14, %rdi
	movq	%r10, -280(%rbp)
	movq	8(%rax), %rdx
	leaq	80(%rax), %rcx
	call	EVP_MD_CTX_ctrl@PLT
	movq	-280(%rbp), %r10
	testl	%eax, %eax
	je	.L97
	movq	%r10, %rsi
	leaq	-240(%rbp), %rdx
	movq	%r14, %rdi
	call	EVP_DigestSignFinal@PLT
	movq	-280(%rbp), %r10
	testl	%eax, %eax
	jg	.L98
	.p2align 4,,10
	.p2align 3
.L97:
	movq	%r10, -280(%rbp)
	movl	$303, %r9d
	movl	$6, %ecx
	leaq	.LC0(%rip), %r8
.L121:
	movl	$496, %edx
	movl	$80, %esi
	movq	%r12, %rdi
	call	ossl_statem_fatal@PLT
	movq	-280(%rbp), %r10
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L124:
	movl	$269, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
.L120:
	movl	$496, %edx
	movl	$80, %esi
	movq	%r12, %rdi
	call	ossl_statem_fatal@PLT
	xorl	%r10d, %r10d
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L127:
	movl	$325, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L125:
	movq	-256(%rbp), %rdi
	xorl	%r8d, %r8d
	movl	$6, %ecx
	movl	$4097, %edx
	movl	$-1, %esi
	movq	%r10, -280(%rbp)
	call	RSA_pkey_ctx_ctrl@PLT
	movq	-280(%rbp), %r10
	testl	%eax, %eax
	jle	.L94
	movq	-256(%rbp), %rdi
	xorl	%r8d, %r8d
	movl	$-1, %ecx
	movl	$4098, %edx
	movl	$24, %esi
	movq	%r10, -280(%rbp)
	call	RSA_pkey_ctx_ctrl@PLT
	movq	-280(%rbp), %r10
	testl	%eax, %eax
	jg	.L92
.L94:
	movq	%r10, -280(%rbp)
	movl	$291, %r9d
	movl	$6, %ecx
	leaq	.LC0(%rip), %r8
	jmp	.L121
.L123:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1005:
	.size	tls_construct_cert_verify, .-tls_construct_cert_verify
	.p2align 4
	.globl	tls_process_cert_verify
	.type	tls_process_cert_verify, @function
tls_process_cert_verify:
.LFB1006:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$232, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -256(%rbp)
	movq	$0, -248(%rbp)
	call	EVP_MD_CTX_new@PLT
	movl	$364, %r9d
	movl	$65, %ecx
	movq	$0, -232(%rbp)
	movq	%rax, %r13
	leaq	.LC0(%rip), %r8
	testq	%rax, %rax
	je	.L171
	movq	1296(%rbx), %rax
	movq	440(%rax), %rdi
	call	X509_get0_pubkey@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L177
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	ssl_cert_lookup_by_pkey@PLT
	testq	%rax, %rax
	je	.L178
	movq	8(%rbx), %rax
	movq	192(%rax), %rax
	movl	96(%rax), %r15d
	andl	$2, %r15d
	je	.L133
	movq	8(%r12), %rax
	movl	$387, %r9d
	movl	$240, %ecx
	leaq	.LC0(%rip), %r8
	cmpq	$1, %rax
	jbe	.L175
	movq	(%r12), %rdx
	subq	$2, %rax
	movq	%rbx, %rdi
	movzwl	(%rdx), %esi
	addq	$2, %rdx
	movq	%rax, 8(%r12)
	movq	%rdx, (%r12)
	movq	%r14, %rdx
	rolw	$8, %si
	movzwl	%si, %esi
	call	tls12_check_peer_sigalg@PLT
	testl	%eax, %eax
	jle	.L172
.L135:
	movq	168(%rbx), %rax
	leaq	-256(%rbp), %rsi
	movq	776(%rax), %rdi
	call	tls1_lookup_md@PLT
	movl	$402, %r9d
	testl	%eax, %eax
	je	.L173
	movq	8(%r12), %rax
	cmpq	$1, %rax
	jbe	.L137
	movq	(%r12), %rdx
	subq	$2, %rax
	movq	%r14, %rdi
	movzwl	(%rdx), %r15d
	addq	$2, %rdx
	movq	%rax, 8(%r12)
	movq	%rdx, (%r12)
	rolw	$8, %r15w
	call	EVP_PKEY_size@PLT
	movzwl	%r15w, %edx
	cmpl	%eax, %edx
	jg	.L138
	movq	8(%r12), %rdx
	cmpl	%edx, %eax
	jl	.L138
	testq	%rdx, %rdx
	je	.L138
	movzwl	%r15w, %r15d
	cmpq	%rdx, %r15
	ja	.L142
	movq	(%r12), %rax
	subq	%r15, %rdx
	movq	%rbx, %rdi
	leaq	-248(%rbp), %rcx
	movq	%rdx, 8(%r12)
	leaq	-224(%rbp), %rsi
	leaq	-240(%rbp), %rdx
	movq	%rax, -264(%rbp)
	addq	%r15, %rax
	movq	%rax, (%r12)
	call	get_cert_verify_tbs_data
	testl	%eax, %eax
	jne	.L143
.L172:
	movq	168(%rbx), %rax
	xorl	%r15d, %r15d
	jmp	.L130
.L180:
	movq	-232(%rbp), %rdi
	xorl	%r8d, %r8d
	movl	$6, %ecx
	movl	$4097, %edx
	movl	$-1, %esi
	call	RSA_pkey_ctx_ctrl@PLT
	testl	%eax, %eax
	jle	.L147
	movq	-232(%rbp), %rdi
	xorl	%r8d, %r8d
	movl	$-1, %ecx
	movl	$4098, %edx
	movl	$24, %esi
	call	RSA_pkey_ctx_ctrl@PLT
	testl	%eax, %eax
	jg	.L145
.L147:
	movl	$482, %r9d
	leaq	.LC0(%rip), %r8
	movl	$6, %ecx
	.p2align 4,,10
	.p2align 3
.L171:
	movl	$379, %edx
	movl	$80, %esi
	movq	%rbx, %rdi
	xorl	%r15d, %r15d
	call	ossl_statem_fatal@PLT
	movq	168(%rbx), %rax
.L130:
	movq	224(%rax), %rdi
	call	BIO_free@PLT
	movq	168(%rbx), %rax
	movq	%r13, %rdi
	movq	$0, 224(%rax)
	call	EVP_MD_CTX_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L179
	addq	$232, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
	movl	$429, %r9d
.L174:
	leaq	.LC0(%rip), %r8
	movl	$159, %ecx
.L175:
	movl	$379, %edx
	movl	$50, %esi
	movq	%rbx, %rdi
	xorl	%r15d, %r15d
	call	ossl_statem_fatal@PLT
	movq	168(%rbx), %rax
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L177:
	movl	$372, %r9d
.L173:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L133:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	tls1_set_peer_legacy_sigalg@PLT
	testl	%eax, %eax
	jne	.L135
	movl	$396, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%rbx, %rdi
	movl	$379, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movq	168(%rbx), %rax
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L138:
	movl	$437, %r9d
	leaq	.LC0(%rip), %r8
	movl	$265, %ecx
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L178:
	movl	$220, %ecx
	movl	$379, %edx
	movq	%rbx, %rdi
	xorl	%r15d, %r15d
	movl	$378, %r9d
	leaq	.LC0(%rip), %r8
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
	movq	168(%rbx), %rax
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L143:
	movq	-256(%rbp), %rdx
	movq	%r14, %r8
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	leaq	-232(%rbp), %rsi
	call	EVP_DigestVerifyInit@PLT
	movl	$457, %r9d
	movl	$6, %ecx
	leaq	.LC0(%rip), %r8
	testl	%eax, %eax
	jle	.L171
	movq	168(%rbx), %rax
	movq	776(%rax), %rax
	testq	%rax, %rax
	je	.L145
	cmpl	$912, 20(%rax)
	je	.L180
.L145:
	cmpl	$768, (%rbx)
	movq	-248(%rbp), %r8
	movq	-240(%rbp), %rcx
	je	.L181
	movq	-264(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	EVP_DigestVerify@PLT
	testl	%eax, %eax
	jle	.L182
.L151:
	movl	56(%rbx), %edx
	movq	168(%rbx), %rax
	movl	$3, %r15d
	testl	%edx, %edx
	jne	.L130
	movq	8(%rbx), %rdx
	movq	192(%rdx), %rcx
	testb	$8, 96(%rcx)
	jne	.L130
	movl	(%rdx), %edx
	cmpl	$771, %edx
	jle	.L155
	cmpl	$65536, %edx
	je	.L155
	xorl	%r15d, %r15d
	cmpl	$1, 584(%rax)
	setne	%r15b
	addl	$2, %r15d
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L142:
	movl	$442, %r9d
	jmp	.L174
.L181:
	movq	%r8, %rdx
	movq	%rcx, %rsi
	movq	%r13, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jle	.L150
	movq	1296(%rbx), %rax
	movl	$29, %esi
	movq	%r13, %rdi
	movq	8(%rax), %rdx
	leaq	80(%rax), %rcx
	call	EVP_MD_CTX_ctrl@PLT
	testl	%eax, %eax
	je	.L150
	movq	-264(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	EVP_DigestVerifyFinal@PLT
	movl	$497, %r9d
	testl	%eax, %eax
	jg	.L151
.L176:
	leaq	.LC0(%rip), %r8
	movl	$123, %ecx
	movl	$379, %edx
	movq	%rbx, %rdi
	movl	$51, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L172
.L150:
	movl	$492, %r9d
	leaq	.LC0(%rip), %r8
	movl	$6, %ecx
	jmp	.L171
.L155:
	movl	$3, %r15d
	jmp	.L130
.L182:
	movl	$504, %r9d
	jmp	.L176
.L179:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1006:
	.size	tls_process_cert_verify, .-tls_process_cert_verify
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	"CLIENT_RANDOM"
	.text
	.p2align 4
	.globl	tls_construct_finished
	.type	tls_construct_finished, @function
tls_construct_finished:
.LFB1007:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	8(%rdi), %rdx
	movq	168(%rdi), %rcx
	movl	56(%rdi), %edi
	movq	192(%rdx), %rax
	testl	%edi, %edi
	jne	.L184
	cmpl	$4, 1936(%r12)
	movl	96(%rax), %esi
	je	.L185
	movl	$1, 112(%r12)
.L185:
	andl	$8, %esi
	je	.L197
.L186:
	movq	48(%rax), %rsi
	movq	56(%rax), %rdx
.L189:
	addq	$280, %rcx
	movq	%r12, %rdi
	call	*40(%rax)
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L212
	movq	168(%r12), %rsi
	movq	%rax, %rdx
	movq	%r13, %rdi
	movq	%rax, 408(%rsi)
	addq	$280, %rsi
	call	WPACKET_memcpy@PLT
	testl	%eax, %eax
	je	.L213
	movq	8(%r12), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L192
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L192
	cmpl	$65536, %eax
	jne	.L195
.L192:
	movq	1296(%r12), %rdx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rdx), %rcx
	addq	$80, %rdx
	call	ssl_log_secret@PLT
	testl	%eax, %eax
	je	.L212
.L195:
	cmpq	$64, %rbx
	ja	.L214
	movq	168(%r12), %rdi
	movl	56(%r12), %eax
	leaq	280(%rdi), %rsi
	testl	%eax, %eax
	jne	.L196
	movq	%rbx, %rdx
	addq	$840, %rdi
	call	memcpy@PLT
	movq	168(%r12), %rax
	movq	%rbx, 904(%rax)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L197:
	.cfi_restore_state
	movl	(%rdx), %edx
	cmpl	$771, %edx
	jle	.L186
	cmpl	$65536, %edx
	je	.L186
	movl	584(%rcx), %esi
	testl	%esi, %esi
	jne	.L186
	movl	$146, %esi
	movq	%r12, %rdi
	call	*32(%rax)
	testl	%eax, %eax
	je	.L212
	movq	8(%r12), %rax
	movl	56(%r12), %edx
	movq	168(%r12), %rcx
	movq	192(%rax), %rax
	testl	%edx, %edx
	je	.L186
	.p2align 4,,10
	.p2align 3
.L184:
	movq	64(%rax), %rsi
	movq	72(%rax), %rdx
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L214:
	movl	$594, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$359, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	.p2align 4,,10
	.p2align 3
.L212:
	xorl	%eax, %eax
.L183:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L196:
	.cfi_restore_state
	movq	%rbx, %rdx
	addq	$912, %rdi
	call	memcpy@PLT
	movq	168(%r12), %rax
	movq	%rbx, 976(%rax)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L213:
	.cfi_restore_state
	movl	$68, %ecx
	movl	$359, %edx
	movq	%r12, %rdi
	movl	%eax, -36(%rbp)
	movl	$574, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movl	-36(%rbp), %eax
	jmp	.L183
	.cfi_endproc
.LFE1007:
	.size	tls_construct_finished, .-tls_construct_finished
	.p2align 4
	.globl	tls_construct_key_update
	.type	tls_construct_key_update, @function
tls_construct_key_update:
.LFB1008:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	subq	$24, %rsp
	movl	1932(%r12), %esi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L219
	movl	$-1, 1932(%r12)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L219:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$68, %ecx
	movl	$517, %edx
	movl	%eax, -20(%rbp)
	movl	$614, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movl	-20(%rbp), %eax
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1008:
	.size	tls_construct_key_update, .-tls_construct_key_update
	.p2align 4
	.globl	tls_process_key_update
	.type	tls_process_key_update, @function
tls_process_key_update:
.LFB1009:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	addq	$2112, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	RECORD_LAYER_processed_read_pending@PLT
	testl	%eax, %eax
	jne	.L228
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L224
	movq	(%rbx), %rdx
	movzbl	(%rdx), %ecx
	addq	$1, %rdx
	subq	$1, %rax
	movq	%rdx, (%rbx)
	movq	%rax, 8(%rbx)
	jne	.L224
	cmpl	$1, %ecx
	ja	.L229
	je	.L230
.L226:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	tls13_update_key@PLT
	popq	%rbx
	popq	%r12
	testl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	setne	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L224:
	.cfi_restore_state
	movl	$639, %r9d
	leaq	.LC0(%rip), %r8
	movl	$122, %ecx
	movq	%r12, %rdi
	movl	$518, %edx
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L220:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L228:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$632, %r9d
	movl	$182, %ecx
	movl	$518, %edx
	leaq	.LC0(%rip), %r8
	movl	$10, %esi
	call	ossl_statem_fatal@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L230:
	.cfi_restore_state
	movl	$0, 1932(%r12)
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L229:
	movl	$650, %r9d
	leaq	.LC0(%rip), %r8
	movl	$122, %ecx
	movq	%r12, %rdi
	movl	$518, %edx
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L220
	.cfi_endproc
.LFE1009:
	.size	tls_process_key_update, .-tls_process_key_update
	.p2align 4
	.globl	ssl3_take_mac
	.type	ssl3_take_mac, @function
ssl3_take_mac:
.LFB1010:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	8(%rdi), %rax
	movq	%rdi, %rbx
	movl	56(%rdi), %edx
	movq	192(%rax), %rax
	testl	%edx, %edx
	jne	.L232
	movq	64(%rax), %rsi
	movq	72(%rax), %rdx
.L233:
	movq	168(%rbx), %r12
	movq	%rbx, %rdi
	leaq	416(%r12), %rcx
	call	*40(%rax)
	movq	%rax, 544(%r12)
	movq	168(%rbx), %rax
	popq	%rbx
	popq	%r12
	cmpq	$0, 544(%rax)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	setne	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L232:
	.cfi_restore_state
	movq	48(%rax), %rsi
	movq	56(%rax), %rdx
	jmp	.L233
	.cfi_endproc
.LFE1010:
	.size	ssl3_take_mac, .-ssl3_take_mac
	.p2align 4
	.globl	tls_process_change_cipher_spec
	.type	tls_process_change_cipher_spec, @function
tls_process_change_cipher_spec:
.LFB1011:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	8(%rdi), %rax
	movq	8(%rsi), %rdx
	movq	192(%rax), %rax
	movl	96(%rax), %eax
	andl	$8, %eax
	je	.L236
	cmpl	$256, (%rdi)
	je	.L253
	testq	%rdx, %rdx
	jne	.L238
.L239:
	movq	168(%r12), %rax
	cmpq	$0, 568(%rax)
	je	.L254
	movl	$1, 240(%rax)
	movq	%r12, %rdi
	call	ssl3_do_change_cipher_spec@PLT
	testl	%eax, %eax
	je	.L255
	movq	8(%r12), %rax
	movq	192(%rax), %rax
	testb	$8, 96(%rax)
	je	.L252
	movl	$1, %esi
	movq	%r12, %rdi
	call	dtls1_reset_seq_numbers@PLT
	cmpl	$256, (%r12)
	je	.L256
.L252:
	addq	$24, %rsp
	movl	$3, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L236:
	.cfi_restore_state
	testq	%rdx, %rdx
	je	.L239
	movl	$722, %r9d
	leaq	.LC0(%rip), %r8
	movl	$103, %ecx
	movl	%eax, -20(%rbp)
	movl	$363, %edx
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	movl	-20(%rbp), %eax
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L253:
	.cfi_restore_state
	cmpq	$2, %rdx
	je	.L239
.L238:
	movq	%r12, %rdi
	movl	$715, %r9d
	movl	$103, %ecx
	movl	$363, %edx
	leaq	.LC0(%rip), %r8
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$738, %r9d
	movl	$68, %ecx
	movl	$363, %edx
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L256:
	.cfi_restore_state
	movq	176(%r12), %rax
	addw	$1, 272(%rax)
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L254:
	movq	%r12, %rdi
	movl	$731, %r9d
	movl	$133, %ecx
	movl	$363, %edx
	leaq	.LC0(%rip), %r8
	movl	$10, %esi
	call	ossl_statem_fatal@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1011:
	.size	tls_process_change_cipher_spec, .-tls_process_change_cipher_spec
	.p2align 4
	.globl	tls_construct_change_cipher_spec
	.type	tls_construct_change_cipher_spec, @function
tls_construct_change_cipher_spec:
.LFB1013:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	movl	$1, %esi
	subq	$24, %rsp
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L261
	addq	$24, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L261:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$68, %ecx
	movl	$427, %edx
	movl	%eax, -20(%rbp)
	movl	$873, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movl	-20(%rbp), %eax
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1013:
	.size	tls_construct_change_cipher_spec, .-tls_construct_change_cipher_spec
	.p2align 4
	.globl	ssl3_output_cert_chain
	.type	ssl3_output_cert_chain, @function
ssl3_output_cert_chain:
.LFB1016:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$3, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	call	WPACKET_start_sub_packet_len__@PLT
	movl	$1010, %r9d
	testl	%eax, %eax
	je	.L294
	testq	%rbx, %rbx
	je	.L266
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L266
	movq	16(%rbx), %r14
	testq	%r14, %r14
	je	.L297
.L268:
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	ssl_security_cert_chain@PLT
	movl	%eax, %ecx
	cmpl	$1, %eax
	jne	.L298
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	ssl_add_cert_to_wpacket
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jne	.L277
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L279:
	movl	%ebx, %esi
	movq	%r14, %rdi
	addl	$1, %ebx
	call	OPENSSL_sk_value@PLT
	movl	%ebx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	ssl_add_cert_to_wpacket
	testl	%eax, %eax
	je	.L295
.L277:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L279
.L266:
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	movl	%eax, %r8d
	movl	$1, %eax
	testl	%r8d, %r8d
	je	.L299
.L262:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L299:
	.cfi_restore_state
	movl	$1019, %r9d
	.p2align 4,,10
	.p2align 3
.L294:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	$147, %edx
	movq	%r12, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
.L295:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L298:
	.cfi_restore_state
	movl	$989, %r9d
	leaq	.LC0(%rip), %r8
.L296:
	movq	%r12, %rdi
	movl	$316, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L297:
	.cfi_restore_state
	movq	1440(%r12), %rax
	movq	256(%rax), %r14
	testb	$8, 1496(%r12)
	jne	.L268
	testq	%r14, %r14
	jne	.L268
	movq	1168(%r12), %rdx
	movq	456(%rdx), %r14
	testq	%r14, %r14
	je	.L300
.L269:
	call	X509_STORE_CTX_new@PLT
	movl	$943, %r9d
	movl	$65, %ecx
	leaq	.LC0(%rip), %r8
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L296
	movq	%rax, %rdi
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	X509_STORE_CTX_init@PLT
	movq	%rbx, %rdi
	testl	%eax, %eax
	je	.L301
	call	X509_verify_cert@PLT
	call	ERR_clear_error@PLT
	movq	%rbx, %rdi
	call	X509_STORE_CTX_get0_chain@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	ssl_security_cert_chain@PLT
	movl	%eax, %r15d
	cmpl	$1, %eax
	je	.L272
	movq	%rbx, %rdi
	call	X509_STORE_CTX_free@PLT
	movl	$972, %r9d
	leaq	.LC0(%rip), %r8
	movl	%r15d, %ecx
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L300:
	movq	32(%rax), %r14
	testq	%r14, %r14
	je	.L268
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L301:
	call	X509_STORE_CTX_free@PLT
	movl	$949, %r9d
	movl	$11, %ecx
	leaq	.LC0(%rip), %r8
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L272:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	movl	%eax, -52(%rbp)
	testl	%eax, %eax
	jle	.L273
	xorl	%r15d, %r15d
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L274:
	addl	$1, %r15d
	cmpl	%r15d, -52(%rbp)
	je	.L273
.L275:
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movl	%r15d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	ssl_add_cert_to_wpacket
	testl	%eax, %eax
	jne	.L274
	movq	%rbx, %rdi
	call	X509_STORE_CTX_free@PLT
	xorl	%eax, %eax
	jmp	.L262
.L273:
	movq	%rbx, %rdi
	call	X509_STORE_CTX_free@PLT
	jmp	.L266
	.cfi_endproc
.LFE1016:
	.size	ssl3_output_cert_chain, .-ssl3_output_cert_chain
	.p2align 4
	.globl	tls_finish_handshake
	.type	tls_finish_handshake, @function
tls_finish_handshake:
.LFB1017:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%ecx, %ebx
	subq	$8, %rsp
	movq	8(%rdi), %rdx
	movl	112(%rdi), %r13d
	movq	192(%rdx), %rax
	movl	96(%rax), %eax
	andl	$8, %eax
	testl	%r8d, %r8d
	je	.L303
	testl	%eax, %eax
	je	.L356
	movq	%r12, %rdi
	call	ssl_free_wbio_buffer@PLT
	testl	%eax, %eax
	je	.L357
.L305:
	movq	8(%r12), %rdx
	movq	$0, 152(%r12)
	movq	192(%rdx), %rax
	movl	96(%rax), %eax
	andl	$8, %eax
.L303:
	testl	%eax, %eax
	jne	.L307
	movl	(%rdx), %eax
	cmpl	$771, %eax
	jle	.L307
	cmpl	$65536, %eax
	jne	.L358
	.p2align 4,,10
	.p2align 3
.L307:
	testl	%r13d, %r13d
	jne	.L359
	movq	1392(%r12), %r13
	testq	%r13, %r13
	je	.L360
.L327:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	ossl_statem_set_in_init@PLT
	movq	8(%r12), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L319
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L319
	cmpl	$65536, %eax
	je	.L319
	movq	168(%r12), %rax
	cmpq	$0, 408(%rax)
	jne	.L361
	.p2align 4,,10
	.p2align 3
.L319:
	movl	$1, %edx
	movl	$32, %esi
	movq	%r12, %rdi
	call	*%r13
.L365:
	movl	$1, %eax
	testl	%ebx, %ebx
	jne	.L302
.L362:
	movq	%r12, %rdi
	movl	$1, %esi
	call	ossl_statem_set_in_init@PLT
	addq	$8, %rsp
	movl	$2, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L360:
	.cfi_restore_state
	movq	1440(%r12), %rax
	movq	272(%rax), %r13
	testq	%r13, %r13
	jne	.L327
.L326:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	ossl_statem_set_in_init@PLT
.L318:
	movl	$1, %eax
	testl	%ebx, %ebx
	je	.L362
.L302:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L359:
	.cfi_restore_state
	movl	$0, 1928(%r12)
	movq	%r12, %rdi
	movl	$0, 60(%r12)
	movl	$0, 112(%r12)
	movl	$0, 1664(%r12)
	call	ssl3_cleanup_key_block@PLT
	movl	56(%r12), %esi
	movq	8(%r12), %rax
	testl	%esi, %esi
	movq	192(%rax), %rdx
	je	.L309
	testb	$8, 96(%rdx)
	jne	.L310
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L310
	cmpl	$65536, %eax
	jne	.L311
.L310:
	movl	$2, %esi
	movq	%r12, %rdi
	call	ssl_update_cache@PLT
.L311:
	movq	1440(%r12), %rax
	lock addl	$1, 132(%rax)
	movq	ossl_statem_accept@GOTPCREL(%rip), %rax
	movq	%rax, 48(%r12)
.L313:
	movq	8(%r12), %rax
	movq	192(%rax), %rax
	testb	$8, 96(%rax)
	jne	.L363
.L317:
	movq	1392(%r12), %r13
	testq	%r13, %r13
	je	.L364
.L325:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	ossl_statem_set_in_init@PLT
	movl	$1, %edx
	movl	$32, %esi
	movq	%r12, %rdi
	call	*%r13
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L356:
	movq	136(%rdi), %rdi
	call	BUF_MEM_free@PLT
	movq	%r12, %rdi
	movq	$0, 136(%r12)
	call	ssl_free_wbio_buffer@PLT
	testl	%eax, %eax
	jne	.L305
.L357:
	movq	%r12, %rdi
	movl	$1058, %r9d
	movl	$68, %ecx
	movl	$597, %edx
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L358:
	.cfi_restore_state
	movl	56(%r12), %edi
	testl	%edi, %edi
	jne	.L307
	cmpl	$4, 1936(%r12)
	jne	.L307
	movl	$1, 1936(%r12)
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L309:
	testb	$8, 96(%rdx)
	jne	.L314
	movl	(%rax), %eax
	cmpl	$65536, %eax
	je	.L314
	cmpl	$771, %eax
	jg	.L366
.L314:
	movq	%r12, %rdi
	movl	$1, %esi
	call	ssl_update_cache@PLT
	movq	1904(%r12), %rdi
.L315:
	movl	200(%r12), %ecx
	testl	%ecx, %ecx
	jne	.L367
.L316:
	movq	ossl_statem_connect@GOTPCREL(%rip), %rax
	movq	%rax, 48(%r12)
	lock addl	$1, 120(%rdi)
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L363:
	movq	176(%r12), %rax
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$0, 268(%rax)
	movw	%dx, 272(%rax)
	call	dtls1_clear_received_buffer@PLT
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L366:
	movq	1904(%r12), %rdi
	testb	$1, 72(%rdi)
	je	.L315
	movq	1296(%r12), %rsi
	call	SSL_CTX_remove_session@PLT
	movq	1904(%r12), %rdi
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L361:
	cmpq	$0, 544(%rax)
	jne	.L318
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L367:
	lock addl	$1, 148(%rdi)
	movq	1904(%r12), %rdi
	jmp	.L316
.L364:
	movq	1440(%r12), %rax
	movq	272(%rax), %r13
	testq	%r13, %r13
	jne	.L325
	jmp	.L326
	.cfi_endproc
.LFE1017:
	.size	tls_finish_handshake, .-tls_finish_handshake
	.p2align 4
	.globl	tls_get_message_header
	.type	tls_get_message_header, @function
tls_get_message_header:
.LFB1018:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	136(%rdi), %rax
	movq	8(%rax), %rbx
	movq	152(%rdi), %rax
	.p2align 4,,10
	.p2align 3
.L369:
	cmpq	$3, %rax
	jbe	.L377
.L387:
	movl	56(%r12), %esi
	movzbl	(%rbx), %eax
	testl	%esi, %esi
	jne	.L378
	cmpl	$1, 92(%r12)
	je	.L378
	testb	%al, %al
	jne	.L378
	cmpb	$0, 1(%rbx)
	jne	.L378
	movzbl	2(%rbx), %eax
	testb	%al, %al
	jne	.L383
	cmpb	$0, 3(%rbx)
	jne	.L378
	movq	184(%r12), %rax
	movq	$0, 152(%r12)
	testq	%rax, %rax
	je	.L384
	subq	$8, %rsp
	movq	%rbx, %rcx
	movl	$22, %edx
	movl	(%r12), %esi
	pushq	192(%r12)
	movq	%r12, %r9
	movl	$4, %r8d
	xorl	%edi, %edi
	call	*%rax
	movq	152(%r12), %rax
	popq	%rdx
	popq	%rcx
	cmpq	$3, %rax
	ja	.L387
.L377:
	movq	8(%r12), %r10
	subq	$8, %rsp
	leaq	-48(%rbp), %rsi
	xorl	%r9d, %r9d
	movl	$4, %r8d
	pushq	%rsi
	leaq	(%rbx,%rax), %rcx
	leaq	-52(%rbp), %rdx
	subq	%rax, %r8
	movl	$22, %esi
	movq	%r12, %rdi
	call	*104(%r10)
	popq	%r8
	popq	%r9
	testl	%eax, %eax
	jle	.L388
	movl	-52(%rbp), %eax
	cmpl	$20, %eax
	je	.L389
	cmpl	$22, %eax
	jne	.L390
	movq	-48(%rbp), %rax
	addq	152(%r12), %rax
	movq	%rax, 152(%r12)
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L384:
	xorl	%eax, %eax
	jmp	.L377
.L383:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L378:
	movl	%eax, 0(%r13)
	movzbl	(%rbx), %edx
	leaq	2112(%r12), %r13
	movq	168(%r12), %rax
	movq	%r13, %rdi
	movl	%edx, 560(%rax)
	call	RECORD_LAYER_is_sslv2_record@PLT
	testl	%eax, %eax
	jne	.L391
	movzbl	1(%rbx), %eax
	movzbl	2(%rbx), %edx
	movq	168(%r12), %rcx
	salq	$8, %rdx
	salq	$16, %rax
	orq	%rdx, %rax
	movzbl	3(%rbx), %edx
	orq	%rdx, %rax
	movq	%rax, 552(%rcx)
	movq	136(%r12), %rax
	movq	8(%rax), %rax
	movq	$0, 152(%r12)
	addq	$4, %rax
	movq	%rax, 144(%r12)
	movl	$1, %eax
.L368:
	movq	-40(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L392
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L391:
	.cfi_restore_state
	movq	%r13, %rdi
	call	RECORD_LAYER_get_rrec_length@PLT
	movq	168(%r12), %rdx
	addq	$4, %rax
	movq	%rax, 552(%rdx)
	movq	136(%r12), %rax
	movq	8(%rax), %rax
	movq	$4, 152(%r12)
	movq	%rax, 144(%r12)
	movl	$1, %eax
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L388:
	movl	$3, 40(%r12)
	xorl	%eax, %eax
	jmp	.L368
.L389:
	cmpq	$0, 152(%r12)
	jne	.L373
	cmpq	$1, -48(%rbp)
	jne	.L373
	cmpb	$1, (%rbx)
	jne	.L373
	movl	92(%r12), %edi
	movq	168(%r12), %rdx
	testl	%edi, %edi
	jne	.L375
	xorl	%eax, %eax
	testq	$2048, (%rdx)
	jne	.L368
.L375:
	movq	136(%r12), %rax
	movl	$257, 0(%r13)
	movl	$257, 560(%rdx)
	movq	$0, 152(%r12)
	movq	8(%rax), %rax
	movq	%rax, 144(%r12)
	movl	$1, %eax
	movq	$1, 552(%rdx)
	jmp	.L368
.L373:
	movl	$1174, %r9d
	leaq	.LC0(%rip), %r8
	movl	$103, %ecx
.L386:
	movl	$387, %edx
	movl	$10, %esi
	movq	%r12, %rdi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L368
.L390:
	movl	$1196, %r9d
	leaq	.LC0(%rip), %r8
	movl	$133, %ecx
	jmp	.L386
.L392:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1018:
	.size	tls_get_message_header, .-tls_get_message_header
	.p2align 4
	.globl	tls_get_message_body
	.type	tls_get_message_body, @function
tls_get_message_body:
.LFB1019:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	152(%rdi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	cmpl	$257, 560(%rax)
	je	.L435
	movq	552(%rax), %r15
	movq	144(%rdi), %r13
	movq	%rdi, %rbx
	leaq	-64(%rbp), %r12
	subq	%rcx, %r15
	jne	.L396
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L399:
	movq	152(%rbx), %rcx
	movq	-64(%rbp), %rax
	addq	%rax, %rcx
	movq	%rcx, 152(%rbx)
	subq	%rax, %r15
	je	.L400
.L396:
	movq	8(%rbx), %rax
	subq	$8, %rsp
	xorl	%r9d, %r9d
	movq	%r15, %r8
	pushq	%r12
	addq	%r13, %rcx
	xorl	%edx, %edx
	movl	$22, %esi
	movq	%rbx, %rdi
	call	*104(%rax)
	popq	%r8
	popq	%r9
	testl	%eax, %eax
	jg	.L399
	movl	$3, 40(%rbx)
	xorl	%eax, %eax
	movq	$0, (%r14)
.L393:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L436
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L400:
	.cfi_restore_state
	movq	136(%rbx), %rax
	movq	8(%rax), %rax
	cmpb	$20, (%rax)
	je	.L437
.L398:
	leaq	2112(%rbx), %rdi
	call	RECORD_LAYER_is_sslv2_record@PLT
	testl	%eax, %eax
	jne	.L438
	movq	8(%rbx), %rax
	movq	168(%rbx), %rdx
	movq	152(%rbx), %r8
	movq	192(%rax), %rcx
	movl	560(%rdx), %edx
	testb	$8, 96(%rcx)
	jne	.L406
	movl	(%rax), %eax
	cmpl	$65536, %eax
	je	.L406
	cmpl	$771, %eax
	jg	.L439
	.p2align 4,,10
	.p2align 3
.L406:
	movq	136(%rbx), %rax
	movq	8(%rax), %rsi
	cmpl	$2, %edx
	jne	.L408
	cmpq	$37, %r8
	jbe	.L408
	movq	8+hrrrandom(%rip), %rdx
	movq	hrrrandom(%rip), %rax
	xorq	14(%rsi), %rdx
	xorq	6(%rsi), %rax
	orq	%rax, %rdx
	je	.L440
	.p2align 4,,10
	.p2align 3
.L408:
	leaq	4(%r8), %rdx
	movq	%rbx, %rdi
	call	ssl3_finish_mac@PLT
	testl	%eax, %eax
	je	.L404
	movq	152(%rbx), %r8
.L407:
	movq	184(%rbx), %rax
	testq	%rax, %rax
	je	.L405
	movq	136(%rbx), %rdx
	subq	$8, %rsp
	movl	(%rbx), %esi
	movq	%rbx, %r9
	addq	$4, %r8
	xorl	%edi, %edi
	movq	8(%rdx), %rcx
	pushq	192(%rbx)
	movl	$22, %edx
	call	*%rax
	movq	152(%rbx), %r8
	popq	%rax
	popq	%rdx
.L405:
	movq	%r8, (%r14)
	movl	$1, %eax
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L435:
	movq	%rcx, (%rsi)
	movl	$1, %eax
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L437:
	movq	8(%rbx), %rax
	movl	56(%rbx), %edi
	movq	192(%rax), %rax
	testl	%edi, %edi
	jne	.L401
	movq	64(%rax), %rsi
	movq	72(%rax), %rdx
.L402:
	movq	168(%rbx), %r12
	movq	%rbx, %rdi
	leaq	416(%r12), %rcx
	call	*40(%rax)
	movq	%rax, 544(%r12)
	movq	168(%rbx), %rax
	cmpq	$0, 544(%rax)
	jne	.L398
.L404:
	movq	$0, (%r14)
	xorl	%eax, %eax
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L438:
	movq	136(%rbx), %rax
	movq	152(%rbx), %rdx
	movq	%rbx, %rdi
	movq	8(%rax), %rsi
	call	ssl3_finish_mac@PLT
	testl	%eax, %eax
	je	.L404
	movq	184(%rbx), %rax
	movq	152(%rbx), %r8
	testq	%rax, %rax
	je	.L405
	movq	136(%rbx), %rdx
	subq	$8, %rsp
	movq	%rbx, %r9
	xorl	%edi, %edi
	movl	$2, %esi
	movq	8(%rdx), %rcx
	pushq	192(%rbx)
	xorl	%edx, %edx
	call	*%rax
	popq	%rcx
	movq	152(%rbx), %r8
	popq	%rsi
	jmp	.L405
.L401:
	movq	48(%rax), %rsi
	movq	56(%rax), %rdx
	jmp	.L402
.L439:
	cmpl	$4, %edx
	je	.L407
	cmpl	$24, %edx
	jne	.L406
	jmp	.L407
.L440:
	movq	24+hrrrandom(%rip), %rdx
	movq	16+hrrrandom(%rip), %rax
	xorq	30(%rsi), %rdx
	xorq	22(%rsi), %rax
	orq	%rax, %rdx
	je	.L407
	jmp	.L408
.L436:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1019:
	.size	tls_get_message_body, .-tls_get_message_body
	.p2align 4
	.globl	ssl_x509err2alert
	.type	ssl_x509err2alert, @function
ssl_x509err2alert:
.LFB1020:
	.cfi_startproc
	endbr64
	cmpl	$50, %edi
	je	.L445
	leaq	x509table(%rip), %rax
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L449:
	testl	%edx, %edx
	je	.L446
.L443:
	movq	%rax, %rcx
	movl	8(%rax), %edx
	addq	$8, %rax
	cmpl	%edi, %edx
	jne	.L449
.L446:
	movl	12(%rcx), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L445:
	movl	$40, %eax
	ret
	.cfi_endproc
.LFE1020:
	.size	ssl_x509err2alert, .-ssl_x509err2alert
	.p2align 4
	.globl	ssl_allow_compression
	.type	ssl_allow_compression, @function
ssl_allow_compression:
.LFB1021:
	.cfi_startproc
	endbr64
	testb	$2, 1494(%rdi)
	je	.L452
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L452:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$15, %esi
	jmp	ssl_security@PLT
	.cfi_endproc
.LFE1021:
	.size	ssl_allow_compression, .-ssl_allow_compression
	.p2align 4
	.globl	ssl_version_supported
	.type	ssl_version_supported, @function
ssl_version_supported:
.LFB1025:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -64(%rbp)
	movq	8(%rdi), %rdx
	movl	(%rdx), %eax
	movq	192(%rdx), %rdx
	movl	96(%rdx), %edx
	cmpl	$65536, %eax
	je	.L482
	cmpl	$131071, %eax
	je	.L483
	cmpl	%esi, (%rdi)
	sete	%r13b
	movzbl	%r13b, %r13d
.L453:
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L483:
	.cfi_restore_state
	leaq	dtls_version_table(%rip), %r15
	movl	$65277, %eax
.L454:
	cmpl	$256, %r13d
	movl	$65280, %ebx
	cmovne	%r13d, %ebx
	movl	%ebx, -52(%rbp)
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L475:
	cmpl	$256, %r13d
	je	.L477
	movl	-52(%rbp), %edx
	cmpl	$256, %eax
	jne	.L478
	movl	%r13d, %edx
	movl	$65280, %eax
.L478:
	cmpl	%eax, %edx
	jle	.L476
.L458:
	movl	24(%r15), %eax
	addq	$24, %r15
	testl	%eax, %eax
	je	.L476
	movq	8(%r14), %rdx
	movq	192(%rdx), %rdx
	movl	96(%rdx), %edx
.L472:
	andl	$8, %edx
	cmpl	%eax, %r13d
	je	.L514
	testl	%edx, %edx
	jne	.L475
	cmpl	%eax, %r13d
	jl	.L458
.L476:
	xorl	%r13d, %r13d
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L514:
	movq	8(%r15), %rax
	testq	%rax, %rax
	je	.L458
	call	*%rax
	movl	(%rax), %ebx
	movq	%rax, %r12
	movl	1500(%r14), %eax
	testl	%eax, %eax
	je	.L456
	movq	8(%r14), %rdx
	movq	192(%rdx), %rdx
	movl	96(%rdx), %edx
	andl	$8, %edx
	cmpl	%eax, %ebx
	je	.L456
	testl	%edx, %edx
	je	.L515
	cmpl	$256, %ebx
	je	.L459
	movl	%ebx, %edx
	cmpl	$256, %eax
	jne	.L460
	movl	$65280, %eax
.L460:
	cmpl	%edx, %eax
	jl	.L458
	.p2align 4,,10
	.p2align 3
.L456:
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movl	%ebx, %ecx
	movl	$9, %esi
	movq	%r14, %rdi
	call	ssl_security@PLT
	testl	%eax, %eax
	je	.L458
	movl	1504(%r14), %eax
	testl	%eax, %eax
	je	.L461
	movq	8(%r14), %rdx
	movq	192(%rdx), %rdx
	movl	96(%rdx), %edx
	andl	$8, %edx
	cmpl	%eax, %ebx
	je	.L461
	testl	%edx, %edx
	je	.L516
	cmpl	$256, %ebx
	je	.L463
	cmpl	$256, %eax
	movl	$65280, %ecx
	cmove	%ecx, %eax
.L464:
	cmpl	%ebx, %eax
	jge	.L458
	.p2align 4,,10
	.p2align 3
.L461:
	movl	1492(%r14), %eax
	testq	%rax, 8(%r12)
	jne	.L458
	testb	$2, 4(%r12)
	je	.L465
	movq	1168(%r14), %rax
	testl	$196608, 28(%rax)
	jne	.L458
.L465:
	movl	56(%r14), %eax
	testl	%eax, %eax
	je	.L466
	cmpl	$772, %r13d
	jne	.L466
	cmpq	$0, 1416(%r14)
	je	.L517
.L466:
	movq	-64(%rbp), %rbx
	movl	$1, %r13d
	testq	%rbx, %rbx
	je	.L453
	call	*8(%r15)
	movq	%rax, (%rbx)
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L477:
	cmpl	$256, %eax
	je	.L476
	movl	$65280, %edx
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L517:
	cmpq	$0, 1424(%r14)
	jne	.L466
	movq	1168(%r14), %rax
	cmpq	$0, 440(%rax)
	jne	.L466
	xorl	%ebx, %ebx
	cmpl	$2, %ebx
	je	.L467
	leal	-4(%rbx), %eax
	cmpl	$2, %eax
	ja	.L518
.L468:
	cmpl	$8, %ebx
	je	.L458
.L467:
	addl	$1, %ebx
	cmpl	$2, %ebx
	je	.L467
	leal	-4(%rbx), %eax
	cmpl	$2, %eax
	jbe	.L468
.L518:
	movq	1168(%r14), %rcx
	movslq	%ebx, %rax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rcx,%rax,8), %rax
	cmpq	$0, 32(%rax)
	je	.L468
	cmpq	$0, 40(%rax)
	je	.L468
	cmpl	$3, %ebx
	jne	.L466
	movq	160(%rcx), %rdi
	call	EVP_PKEY_get0_EC_KEY@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L467
	call	EC_KEY_get0_group@PLT
	movq	%rax, %rdi
	call	EC_GROUP_get_curve_name@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	tls_check_sigalg_curve@PLT
	testl	%eax, %eax
	je	.L467
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L515:
	cmpl	%eax, %ebx
	jl	.L458
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L463:
	movl	$65280, %ebx
	cmpl	$256, %eax
	jne	.L464
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L459:
	movl	$65280, %edx
	cmpl	$256, %eax
	jne	.L460
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L516:
	cmpl	%eax, %ebx
	jl	.L461
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L482:
	leaq	tls_version_table(%rip), %r15
	movl	$772, %eax
	jmp	.L454
	.cfi_endproc
.LFE1025:
	.size	ssl_version_supported, .-ssl_version_supported
	.p2align 4
	.globl	ssl_check_version_downgrade
	.type	ssl_check_version_downgrade, @function
ssl_check_version_downgrade:
.LFB1026:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	1440(%rdi), %rax
	movq	(%rax), %rax
	movl	(%rax), %r13d
	movl	$1, %eax
	cmpl	%r13d, (%rdi)
	je	.L519
	movq	%rdi, %rbx
	leaq	tls_version_table(%rip), %r12
	call	TLS_method@PLT
	cmpl	(%rax), %r13d
	jne	.L560
.L521:
	movl	$65280, %r15d
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L562:
	movq	8(%rbx), %rdx
	movq	192(%rdx), %rdx
	movl	96(%rdx), %edx
	andl	$8, %edx
	cmpl	%eax, %r14d
	je	.L523
	testl	%edx, %edx
	je	.L561
	cmpl	$256, %r14d
	je	.L525
	movl	%r14d, %edx
	cmpl	$256, %eax
	jne	.L526
	movl	$65280, %eax
.L526:
	cmpl	%edx, %eax
	jge	.L523
	.p2align 4,,10
	.p2align 3
.L522:
	movl	24(%r12), %eax
	addq	$24, %r12
	testl	%eax, %eax
	je	.L533
.L532:
	movq	16(%r12), %rax
	testq	%rax, %rax
	je	.L522
	call	*%rax
	movl	(%rax), %r14d
	movq	%rax, %r13
	movl	1500(%rbx), %eax
	testl	%eax, %eax
	jne	.L562
.L523:
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movl	%r14d, %ecx
	movl	$9, %esi
	movq	%rbx, %rdi
	call	ssl_security@PLT
	testl	%eax, %eax
	je	.L522
	movl	1504(%rbx), %eax
	testl	%eax, %eax
	je	.L527
	movq	8(%rbx), %rdx
	movq	192(%rdx), %rdx
	movl	96(%rdx), %edx
	andl	$8, %edx
	cmpl	%eax, %r14d
	je	.L527
	testl	%edx, %edx
	je	.L563
	cmpl	$256, %r14d
	je	.L529
	cmpl	$256, %eax
	cmove	%r15d, %eax
.L530:
	cmpl	%r14d, %eax
	jge	.L522
	.p2align 4,,10
	.p2align 3
.L527:
	movl	1492(%rbx), %eax
	testq	%rax, 8(%r13)
	jne	.L522
	testb	$2, 4(%r13)
	jne	.L564
.L531:
	movl	(%r12), %eax
	cmpl	%eax, (%rbx)
	sete	%al
	movzbl	%al, %eax
.L519:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L560:
	.cfi_restore_state
	movq	1440(%rbx), %rax
	movq	(%rax), %rax
	movl	(%rax), %r12d
	call	DTLS_method@PLT
	cmpl	(%rax), %r12d
	jne	.L533
	leaq	dtls_version_table(%rip), %r12
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L533:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L564:
	.cfi_restore_state
	movq	1168(%rbx), %rax
	testl	$196608, 28(%rax)
	je	.L531
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L561:
	cmpl	%eax, %r14d
	jl	.L522
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L525:
	movl	$65280, %edx
	cmpl	$256, %eax
	jne	.L526
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L563:
	cmpl	%eax, %r14d
	jl	.L527
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L529:
	movl	$65280, %r14d
	cmpl	$256, %eax
	jne	.L530
	jmp	.L522
	.cfi_endproc
.LFE1026:
	.size	ssl_check_version_downgrade, .-ssl_check_version_downgrade
	.p2align 4
	.globl	ssl_set_version_bound
	.type	ssl_set_version_bound, @function
ssl_set_version_bound:
.LFB1027:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	je	.L573
	cmpl	$65536, %edi
	je	.L568
	cmpl	$131071, %edi
	je	.L569
.L572:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L569:
	cmpl	$256, %esi
	setne	%cl
	cmpl	$65276, %esi
	setle	%al
	testb	%al, %cl
	jne	.L572
	cmpl	$65280, %esi
	jg	.L572
.L570:
	movl	%esi, (%rdx)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L568:
	leal	-768(%rsi), %ecx
	xorl	%eax, %eax
	cmpl	$4, %ecx
	jbe	.L570
	ret
	.p2align 4,,10
	.p2align 3
.L573:
	movl	$0, (%rdx)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1027:
	.size	ssl_set_version_bound, .-ssl_set_version_bound
	.p2align 4
	.globl	ssl_choose_server_version
	.type	ssl_choose_server_version, @function
ssl_choose_server_version:
.LFB1029:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	8(%rdi), %rcx
	movl	4(%rsi), %r12d
	movq	%rdx, -80(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rcx), %eax
	movl	%r12d, 1524(%rdi)
	cmpl	$65536, %eax
	je	.L630
	cmpl	$131071, %eax
	je	.L631
	movq	192(%rcx), %rdx
	movl	96(%rdx), %edx
	testb	$8, %dl
	jne	.L649
	cmpl	$771, %eax
	jg	.L576
.L649:
	movl	(%rbx), %ecx
	andl	$8, %edx
	cmpl	%r12d, %ecx
	je	.L578
	testl	%edx, %edx
	je	.L704
	cmpl	$256, %r12d
	je	.L581
	cmpl	$256, %ecx
	movl	$65280, %eax
	cmove	%eax, %ecx
.L582:
	movl	$266, %eax
	cmpl	%r12d, %ecx
	jl	.L574
.L578:
	movq	-80(%rbp), %rax
	movl	$0, (%rax)
	xorl	%eax, %eax
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L631:
	movl	$65277, %eax
	leaq	dtls_version_table(%rip), %r13
.L575:
	movq	648(%rsi), %rsi
	movl	696(%rsi), %edi
	testl	%edi, %edi
	jne	.L583
.L627:
	movl	1248(%rbx), %esi
	testl	%esi, %esi
	jne	.L596
	movq	192(%rcx), %rdx
	movl	96(%rdx), %edx
.L585:
	andl	$8, %edx
	cmpl	$772, %r12d
	je	.L639
	testl	%edx, %edx
	je	.L705
	cmpl	$256, %r12d
	je	.L601
	cmpl	$772, %r12d
	movl	$771, %edx
	cmovle	%edx, %r12d
.L601:
	cmpl	$256, %r12d
	movl	$65280, %r14d
	cmovne	%r12d, %r14d
	xorl	%esi, %esi
	movl	%r14d, -72(%rbp)
	jmp	.L622
	.p2align 4,,10
	.p2align 3
.L606:
	cmpl	$256, %r12d
	je	.L607
	movl	-72(%rbp), %edx
	cmpl	$256, %eax
	jne	.L608
	movl	%r12d, %edx
	movl	$65280, %eax
.L608:
	cmpl	%edx, %eax
	jge	.L605
.L604:
	movl	24(%r13), %eax
	addq	$24, %r13
	testl	%eax, %eax
	je	.L706
.L622:
	movq	16(%r13), %rcx
	testq	%rcx, %rcx
	je	.L604
	movq	8(%rbx), %rdx
	movq	192(%rdx), %rdx
	movl	96(%rdx), %edx
	andl	$8, %edx
	cmpl	%eax, %r12d
	je	.L605
	testl	%edx, %edx
	jne	.L606
	cmpl	%eax, %r12d
	jl	.L604
.L605:
	call	*%rcx
	movl	(%rax), %r14d
	movq	%rax, %r15
	movl	1500(%rbx), %eax
	testl	%eax, %eax
	je	.L609
	movq	8(%rbx), %rdx
	movq	192(%rdx), %rdx
	movl	96(%rdx), %edx
	andl	$8, %edx
	cmpl	%eax, %r14d
	je	.L609
	testl	%edx, %edx
	je	.L707
	cmpl	$256, %r14d
	je	.L611
	movl	%r14d, %edx
	cmpl	$256, %eax
	jne	.L612
	movl	$65280, %eax
.L612:
	cmpl	%edx, %eax
	jge	.L609
.L613:
	movl	24(%r13), %eax
	addq	$24, %r13
	movl	$1, %esi
	testl	%eax, %eax
	jne	.L622
.L706:
	movl	$396, %eax
	testl	%esi, %esi
	je	.L574
.L596:
	movl	$258, %eax
.L574:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L708
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L583:
	.cfi_restore_state
	movq	192(%rcx), %rdx
	movl	96(%rdx), %edx
	testb	$8, %dl
	jne	.L585
.L628:
	movq	688(%rsi), %rcx
	movq	$0, -64(%rbp)
	movl	$1, 700(%rsi)
	testq	%rcx, %rcx
	je	.L586
	movq	680(%rsi), %r8
	leaq	-1(%rcx), %rdi
	movzbl	(%r8), %eax
	cmpq	%rax, %rdi
	je	.L709
.L586:
	movl	$159, %eax
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L707:
	cmpl	%eax, %r14d
	jl	.L613
	.p2align 4,,10
	.p2align 3
.L609:
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movl	%r14d, %ecx
	movl	$9, %esi
	movq	%rbx, %rdi
	call	ssl_security@PLT
	testl	%eax, %eax
	je	.L613
	movl	1504(%rbx), %eax
	testl	%eax, %eax
	je	.L614
	movq	8(%rbx), %rdx
	movq	192(%rdx), %rdx
	movl	96(%rdx), %edx
	andl	$8, %edx
	cmpl	%eax, %r14d
	je	.L614
	testl	%edx, %edx
	je	.L710
	cmpl	$256, %r14d
	je	.L616
	cmpl	$256, %eax
	movl	$65280, %edi
	cmove	%edi, %eax
.L617:
	cmpl	%eax, %r14d
	jle	.L613
	.p2align 4,,10
	.p2align 3
.L614:
	movl	1492(%rbx), %eax
	testq	%rax, 8(%r15)
	jne	.L613
	testb	$2, 4(%r15)
	jne	.L711
.L618:
	movl	0(%r13), %eax
	cmpl	$771, %eax
	je	.L712
	movq	8(%rbx), %rdx
	movq	192(%rdx), %rdx
	testb	$8, 96(%rdx)
	jne	.L620
	cmpl	$770, %eax
	jle	.L713
.L620:
	movq	-80(%rbp), %rax
	movl	$0, (%rax)
.L621:
	movl	0(%r13), %eax
	movq	%r15, 8(%rbx)
	movl	%eax, (%rbx)
	xorl	%eax, %eax
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L607:
	movl	$65280, %edx
	cmpl	$256, %eax
	jne	.L608
	jmp	.L605
	.p2align 4,,10
	.p2align 3
.L711:
	movq	1168(%rbx), %rax
	testl	$196608, 28(%rax)
	je	.L618
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L611:
	movl	$65280, %edx
	cmpl	$256, %eax
	jne	.L612
	jmp	.L609
	.p2align 4,,10
	.p2align 3
.L630:
	movl	$772, %eax
	leaq	tls_version_table(%rip), %r13
	jmp	.L575
	.p2align 4,,10
	.p2align 3
.L710:
	cmpl	%eax, %r14d
	jl	.L614
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L576:
	movq	648(%rsi), %rsi
	movl	696(%rsi), %eax
	testl	%eax, %eax
	jne	.L628
	movl	$772, %eax
	leaq	tls_version_table(%rip), %r13
	jmp	.L627
	.p2align 4,,10
	.p2align 3
.L704:
	movl	$266, %eax
	cmpl	%r12d, %ecx
	jg	.L574
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L616:
	movl	$65280, %r14d
	cmpl	$256, %eax
	jne	.L617
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L705:
	cmpl	$771, %r12d
	movl	$771, %edx
	cmovg	%edx, %r12d
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L639:
	movl	$771, %r12d
	jmp	.L601
.L712:
	xorl	%edx, %edx
	movl	$772, %esi
	movq	%rbx, %rdi
	call	ssl_version_supported
	testl	%eax, %eax
	je	.L620
	movq	-80(%rbp), %rax
	movl	$1, (%rax)
	jmp	.L621
.L581:
	movl	$65280, %r12d
	cmpl	$256, %ecx
	jne	.L582
	jmp	.L578
.L709:
	movq	$0, 688(%rsi)
	leaq	(%r8,%rcx), %rax
	movq	%rax, 680(%rsi)
	movl	$292, %eax
	cmpl	$768, %r12d
	jle	.L574
	cmpq	$1, %rdi
	jbe	.L588
	leaq	-3(%rcx), %rax
	xorl	%r12d, %r12d
	movq	%rax, %rdi
	movq	%rax, -96(%rbp)
	andq	$-2, %rax
	shrq	%rdi
	leaq	(%r8,%rax), %r14
	movq	%rdi, -88(%rbp)
	jmp	.L589
	.p2align 4,,10
	.p2align 3
.L715:
	movq	8(%rbx), %rax
	addq	$2, %r8
	movq	192(%rax), %rax
	movl	96(%rax), %edx
.L589:
	movzwl	1(%r8), %r15d
	movl	%r12d, %r13d
	andl	$8, %edx
	rolw	$8, %r15w
	movzwl	%r15w, %r15d
	cmpl	%r15d, %r12d
	je	.L636
	testl	%edx, %edx
	je	.L714
	cmpl	$256, %r15d
	je	.L594
	movl	%r15d, %eax
	movl	$65280, %edx
	cmpl	$256, %r12d
	je	.L595
.L626:
	movl	%r12d, %edx
.L595:
	cmpl	%eax, %edx
	jge	.L593
.L590:
	cmpq	%r8, %r14
	jne	.L715
	movq	-88(%rbp), %r14
	addq	%r14, %r14
	cmpq	-96(%rbp), %r14
	jne	.L586
	testl	%r12d, %r12d
	je	.L596
	movl	1248(%rbx), %edx
	testl	%edx, %edx
	je	.L597
	xorl	%eax, %eax
	cmpl	$772, %r12d
	jne	.L596
	jmp	.L574
.L714:
	cmpl	%r15d, %r12d
	jg	.L590
.L593:
	leaq	-64(%rbp), %rdx
	movl	%r15d, %esi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	call	ssl_version_supported
	movq	-72(%rbp), %r8
	testl	%eax, %eax
	je	.L590
	movl	%r15d, %r13d
	movl	%r15d, %r12d
	jmp	.L590
.L713:
	xorl	%edx, %edx
	movl	$771, %esi
	movq	%rbx, %rdi
	call	ssl_version_supported
	testl	%eax, %eax
	je	.L620
	movq	-80(%rbp), %rax
	movl	$2, (%rax)
	jmp	.L621
.L636:
	movl	%r12d, %r13d
	jmp	.L590
.L594:
	movl	$65280, %eax
	cmpl	$256, %r12d
	jne	.L626
	jmp	.L593
.L588:
	testq	%rdi, %rdi
	je	.L596
	jmp	.L586
.L597:
	cmpl	$771, %r12d
	je	.L716
	movq	8(%rbx), %rax
	movq	192(%rax), %rax
	testb	$8, 96(%rax)
	jne	.L599
	cmpl	$770, %r12d
	jbe	.L717
.L599:
	movq	-80(%rbp), %rax
	movl	$0, (%rax)
.L600:
	movq	-64(%rbp), %rax
	movl	%r13d, (%rbx)
	movq	%rax, 8(%rbx)
	xorl	%eax, %eax
	jmp	.L574
.L716:
	xorl	%edx, %edx
	movl	$772, %esi
	movq	%rbx, %rdi
	call	ssl_version_supported
	testl	%eax, %eax
	je	.L599
	movq	-80(%rbp), %rax
	movl	$1, (%rax)
	jmp	.L600
.L708:
	call	__stack_chk_fail@PLT
.L717:
	xorl	%edx, %edx
	movl	$771, %esi
	movq	%rbx, %rdi
	call	ssl_version_supported
	testl	%eax, %eax
	je	.L599
	movq	-80(%rbp), %rax
	movl	$2, (%rax)
	jmp	.L600
	.cfi_endproc
.LFE1029:
	.size	ssl_choose_server_version, .-ssl_choose_server_version
	.p2align 4
	.globl	ssl_get_min_max_version
	.type	ssl_get_min_max_version, @function
ssl_get_min_max_version:
.LFB1031:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	%rsi, -64(%rbp)
	movq	%rdx, -80(%rbp)
	movl	(%rax), %eax
	movq	%rcx, -72(%rbp)
	cmpl	$65536, %eax
	je	.L719
	cmpl	$131071, %eax
	je	.L720
	movl	(%rdi), %eax
	movl	%eax, (%rdx)
	movl	%eax, (%rsi)
	movl	$68, %eax
	testq	%rcx, %rcx
	jne	.L718
.L737:
	xorl	%eax, %eax
.L718:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L719:
	.cfi_restore_state
	movq	-72(%rbp), %rax
	movl	$0, (%rsi)
	leaq	tls_version_table(%rip), %r15
	testq	%rax, %rax
	je	.L722
	movl	$0, (%rax)
.L722:
	movq	%r15, %r13
	movl	$0, -56(%rbp)
	xorl	%r12d, %r12d
	movq	%r14, %r15
	movl	$1, %ebx
	jmp	.L736
	.p2align 4,,10
	.p2align 3
.L780:
	movq	8(%r15), %rdx
	movq	192(%rdx), %rdx
	movl	96(%rdx), %edx
	andl	$8, %edx
	cmpl	%eax, %ecx
	je	.L725
	testl	%edx, %edx
	je	.L778
	cmpl	$256, %ecx
	je	.L727
	movl	%ecx, %edx
	cmpl	$256, %eax
	jne	.L728
	movl	$65280, %eax
.L728:
	cmpl	%edx, %eax
	jge	.L725
.L751:
	movl	$1, %ebx
.L723:
	movl	24(%r13), %eax
	addq	$24, %r13
	testl	%eax, %eax
	je	.L779
.L736:
	movq	8(%r13), %rax
	testq	%rax, %rax
	je	.L743
	call	*%rax
	movq	%rax, %r14
	testl	%r12d, %r12d
	jne	.L724
	testl	%ebx, %ebx
	je	.L724
	movl	0(%r13), %r12d
.L724:
	movl	1500(%r15), %eax
	movl	(%r14), %ecx
	testl	%eax, %eax
	jne	.L780
.L725:
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movl	$9, %esi
	movq	%r15, %rdi
	movl	%ecx, -52(%rbp)
	call	ssl_security@PLT
	testl	%eax, %eax
	je	.L751
	movl	1504(%r15), %eax
	testl	%eax, %eax
	je	.L729
	movq	8(%r15), %rdx
	movl	-52(%rbp), %ecx
	movq	192(%rdx), %rdx
	movl	96(%rdx), %edx
	andl	$8, %edx
	cmpl	%eax, %ecx
	je	.L729
	testl	%edx, %edx
	je	.L781
	cmpl	$256, %ecx
	je	.L731
	cmpl	$256, %eax
	movl	$65280, %edx
	cmove	%edx, %eax
.L732:
	cmpl	%eax, %ecx
	jle	.L751
	.p2align 4,,10
	.p2align 3
.L729:
	movl	1492(%r15), %eax
	testq	%rax, 8(%r14)
	jne	.L751
	testb	$2, 4(%r14)
	je	.L733
	movq	1168(%r15), %rax
	testl	$196608, 28(%rax)
	jne	.L751
.L733:
	testl	%ebx, %ebx
	jne	.L734
	movl	(%r14), %eax
	movq	-64(%rbp), %rdi
	addq	$24, %r13
	movl	%eax, (%rdi)
	movl	0(%r13), %eax
	testl	%eax, %eax
	jne	.L736
.L779:
	movq	-80(%rbp), %rsi
	movl	-56(%rbp), %eax
	movl	%eax, (%rsi)
	testl	%eax, %eax
	jne	.L737
	movl	$191, %eax
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L734:
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L735
	testl	%r12d, %r12d
	je	.L735
	movl	%r12d, (%rax)
.L735:
	movl	(%r14), %eax
	movq	-64(%rbp), %rdi
	xorl	%ebx, %ebx
	movl	%eax, -56(%rbp)
	movl	%eax, (%rdi)
	jmp	.L723
	.p2align 4,,10
	.p2align 3
.L743:
	movl	$1, %ebx
	xorl	%r12d, %r12d
	jmp	.L723
	.p2align 4,,10
	.p2align 3
.L778:
	cmpl	%eax, %ecx
	jl	.L751
	jmp	.L725
	.p2align 4,,10
	.p2align 3
.L727:
	movl	$65280, %edx
	cmpl	$256, %eax
	jne	.L728
	jmp	.L725
	.p2align 4,,10
	.p2align 3
.L781:
	cmpl	%eax, %ecx
	jl	.L729
	jmp	.L751
	.p2align 4,,10
	.p2align 3
.L720:
	cmpq	$0, -72(%rbp)
	movl	$0, (%rsi)
	leaq	dtls_version_table(%rip), %r15
	je	.L722
	movl	$0, (%rcx)
	leaq	dtls_version_table(%rip), %r15
	jmp	.L722
	.p2align 4,,10
	.p2align 3
.L731:
	movl	$65280, %ecx
	cmpl	$256, %eax
	jne	.L732
	jmp	.L751
	.cfi_endproc
.LFE1031:
	.size	ssl_get_min_max_version, .-ssl_get_min_max_version
	.section	.rodata.str1.8
	.align 8
.LC3:
	.string	"No ciphers enabled for max supported SSL/TLS version"
	.text
	.p2align 4
	.globl	tls_setup_handshake
	.type	tls_setup_handshake, @function
tls_setup_handshake:
.LFB1003:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	ssl3_init_finished_mac@PLT
	testl	%eax, %eax
	jne	.L818
.L782:
	movq	-40(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L819
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L818:
	.cfi_restore_state
	movl	56(%r12), %edx
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	$0, 1568(%r12)
	movw	%ax, 1576(%r12)
	movups	%xmm0, 1552(%r12)
	testl	%edx, %edx
	jne	.L820
	movq	168(%r12), %rdx
	movq	1904(%r12), %rax
	cmpq	$0, 408(%rdx)
	je	.L797
	cmpq	$0, 544(%rdx)
	jne	.L798
.L797:
	lock addl	$1, 112(%rax)
.L799:
	movq	168(%r12), %rax
	pxor	%xmm0, %xmm0
	movups	%xmm0, 184(%rax)
	movups	%xmm0, 200(%rax)
	movq	168(%r12), %rax
	movl	$0, 200(%r12)
	movl	$0, 584(%rax)
	movq	8(%r12), %rax
	movq	192(%rax), %rdx
	movl	$1, %eax
	testb	$8, 96(%rdx)
	je	.L782
	movl	$1, 120(%r12)
	jmp	.L782
	.p2align 4,,10
	.p2align 3
.L798:
	lock addl	$1, 116(%rax)
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L820:
	movq	%r12, %rdi
	movl	$65280, %r13d
	call	SSL_get_ciphers@PLT
	leaq	-44(%rbp), %rdx
	leaq	-48(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	ssl_get_min_max_version
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L785
	jmp	.L821
	.p2align 4,,10
	.p2align 3
.L824:
	movl	-44(%rbp), %ecx
	movl	52(%rax), %edx
	cmpl	$256, %ecx
	je	.L788
	cmpl	$256, %edx
	cmove	%r13d, %edx
.L789:
	cmpl	%ecx, %edx
	jge	.L822
.L790:
	addl	$1, %r14d
.L785:
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r14d
	jge	.L823
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	movq	8(%r12), %rdx
	movq	192(%rdx), %rdx
	testb	$8, 96(%rdx)
	jne	.L824
	movl	-44(%rbp), %edx
	cmpl	%edx, 44(%rax)
	jg	.L790
	cmpl	48(%rax), %edx
	jg	.L790
.L792:
	movq	168(%r12), %rax
	cmpq	$0, 408(%rax)
	je	.L800
	cmpq	$0, 544(%rax)
	jne	.L795
.L800:
	movq	1904(%r12), %rax
	lock addl	$1, 124(%rax)
.L796:
	movl	$1, %eax
	jmp	.L782
	.p2align 4,,10
	.p2align 3
.L822:
	movl	56(%rax), %eax
	cmpl	$256, %eax
	cmove	%r13d, %eax
.L791:
	cmpl	%ecx, %eax
	jg	.L790
	jmp	.L792
	.p2align 4,,10
	.p2align 3
.L788:
	movl	$65280, %ecx
	cmpl	$256, %edx
	jne	.L789
	movl	56(%rax), %eax
	cmpl	$256, %eax
	jne	.L791
	jmp	.L792
.L795:
	movq	1440(%r12), %rax
	lock addl	$1, 128(%rax)
	movq	168(%r12), %rax
	movl	$0, 672(%rax)
	jmp	.L796
	.p2align 4,,10
	.p2align 3
.L821:
	movl	$109, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$508, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L782
	.p2align 4,,10
	.p2align 3
.L823:
	movl	$127, %r9d
	leaq	.LC0(%rip), %r8
	movl	$181, %ecx
	movq	%r12, %rdi
	movl	$508, %edx
	movl	$40, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	movl	$1, %edi
	leaq	.LC3(%rip), %rsi
	call	ERR_add_error_data@PLT
	xorl	%eax, %eax
	jmp	.L782
.L819:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1003:
	.size	tls_setup_handshake, .-tls_setup_handshake
	.p2align 4
	.globl	ssl_choose_client_version
	.type	ssl_choose_client_version, @function
ssl_choose_client_version:
.LFB1030:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$768, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -40
	movl	(%rdi), %ebx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	%esi, (%rdi)
	movl	$17, %esi
	call	tls_parse_extension@PLT
	testl	%eax, %eax
	je	.L870
	movl	1248(%r12), %eax
	testl	%eax, %eax
	je	.L828
	cmpl	$772, (%r12)
	jne	.L871
.L828:
	movq	8(%r12), %rax
	movl	(%rax), %edx
	cmpl	$65536, %edx
	je	.L851
	cmpl	$131071, %edx
	je	.L852
	movl	$1, %eax
	cmpl	(%r12), %edx
	je	.L825
	movl	%ebx, (%r12)
	movl	$1910, %r9d
.L866:
	leaq	.LC0(%rip), %r8
	movl	$266, %ecx
.L867:
	movl	$607, %edx
	movl	$70, %esi
	movq	%r12, %rdi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L825
	.p2align 4,,10
	.p2align 3
.L852:
	leaq	dtls_version_table(%rip), %r13
.L829:
	leaq	-44(%rbp), %rcx
	leaq	-48(%rbp), %rdx
	movq	%r12, %rdi
	leaq	-52(%rbp), %rsi
	call	ssl_get_min_max_version
	testl	%eax, %eax
	jne	.L872
	movq	8(%r12), %rdx
	movl	-52(%rbp), %esi
	movq	192(%rdx), %rdx
	movl	96(%rdx), %ecx
	movl	(%r12), %edx
	andl	$8, %ecx
	jne	.L873
	cmpl	%esi, %edx
	jl	.L849
	movl	-48(%rbp), %esi
	cmpl	%esi, %edx
	setg	%dil
.L836:
	testb	%dil, %dil
	jne	.L874
	testb	$-128, 1496(%r12)
	jne	.L838
	movl	%esi, -44(%rbp)
.L838:
	cmpl	$771, %edx
	je	.L875
	cmpl	$770, %edx
	jg	.L865
	testl	%ecx, %ecx
	je	.L841
	.p2align 4,,10
	.p2align 3
.L865:
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	je	.L876
	movq	8(%r13), %rsi
	testq	%rsi, %rsi
	je	.L856
	cmpl	%ecx, %edx
	jne	.L856
	call	*%rsi
	movq	%rax, 8(%r12)
	movl	$1, %eax
	jmp	.L825
	.p2align 4,,10
	.p2align 3
.L870:
	movl	%ebx, (%r12)
.L825:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L877
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L851:
	.cfi_restore_state
	leaq	tls_version_table(%rip), %r13
	jmp	.L829
	.p2align 4,,10
	.p2align 3
.L873:
	cmpl	$256, %edx
	je	.L832
	cmpl	$256, %esi
	movl	$65280, %edi
	movl	%edx, %r8d
	cmove	%edi, %esi
	cmpl	%esi, %edx
	jg	.L849
.L848:
	movl	-48(%rbp), %esi
	movl	$65280, %edi
	cmpl	$256, %esi
	cmovne	%esi, %edi
	cmpl	%edi, %r8d
	setl	%dil
	jmp	.L836
	.p2align 4,,10
	.p2align 3
.L871:
	movl	%ebx, (%r12)
	movl	$1901, %r9d
	jmp	.L866
	.p2align 4,,10
	.p2align 3
.L856:
	addq	$24, %r13
	jmp	.L865
	.p2align 4,,10
	.p2align 3
.L872:
	movl	%ebx, (%r12)
	movl	$1934, %r9d
	leaq	.LC0(%rip), %r8
	movl	%eax, %ecx
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L875:
	cmpl	$771, -44(%rbp)
	jle	.L865
	movq	168(%r12), %rcx
	movq	tls12downgrade(%rip), %rdi
	cmpq	%rdi, 176(%rcx)
	jne	.L865
	movl	%eax, -68(%rbp)
	movl	$1962, %r9d
	movl	%ebx, (%r12)
	jmp	.L869
	.p2align 4,,10
	.p2align 3
.L849:
	movl	%eax, -68(%rbp)
	movl	$1941, %r9d
	movl	%ebx, (%r12)
.L868:
	leaq	.LC0(%rip), %r8
	movl	$258, %ecx
	movl	$607, %edx
	movq	%r12, %rdi
	movl	$70, %esi
	call	ossl_statem_fatal@PLT
	movl	-68(%rbp), %eax
	jmp	.L825
	.p2align 4,,10
	.p2align 3
.L841:
	cmpl	-44(%rbp), %edx
	jge	.L865
	movq	168(%r12), %rcx
	movq	tls11downgrade(%rip), %rdi
	cmpq	%rdi, 176(%rcx)
	jne	.L865
	movl	%eax, -68(%rbp)
	movl	$1975, %r9d
	movl	%ebx, (%r12)
.L869:
	leaq	.LC0(%rip), %r8
	movl	$373, %ecx
	movl	$607, %edx
	movq	%r12, %rdi
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
	movl	-68(%rbp), %eax
	jmp	.L825
	.p2align 4,,10
	.p2align 3
.L832:
	cmpl	$256, %esi
	je	.L864
	cmpl	$65279, %esi
	jle	.L849
.L864:
	movl	$65280, %r8d
	jmp	.L848
	.p2align 4,,10
	.p2align 3
.L874:
	movl	%eax, -68(%rbp)
	movl	$1947, %r9d
	movl	%ebx, (%r12)
	jmp	.L868
	.p2align 4,,10
	.p2align 3
.L876:
	movl	%eax, -68(%rbp)
	movl	$1991, %r9d
	movl	%ebx, (%r12)
	jmp	.L868
.L877:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1030:
	.size	ssl_choose_client_version, .-ssl_choose_client_version
	.p2align 4
	.globl	ssl_set_client_hello_version
	.type	ssl_set_client_hello_version, @function
ssl_set_client_hello_version:
.LFB1032:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	168(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 408(%rdx)
	je	.L879
	cmpq	$0, 544(%rdx)
	jne	.L878
.L879:
	xorl	%ecx, %ecx
	leaq	-28(%rbp), %rdx
	leaq	-32(%rbp), %rsi
	movq	%rbx, %rdi
	call	ssl_get_min_max_version
	testl	%eax, %eax
	jne	.L878
	movq	8(%rbx), %rcx
	movl	-28(%rbp), %edx
	movq	192(%rcx), %rcx
	movl	%edx, (%rbx)
	testb	$8, 96(%rcx)
	jne	.L881
	cmpl	$771, %edx
	movl	$771, %ecx
	cmovg	%ecx, %edx
.L881:
	movl	%edx, 1524(%rbx)
.L878:
	movq	-24(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L890
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L890:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1032:
	.size	ssl_set_client_hello_version, .-ssl_set_client_hello_version
	.p2align 4
	.globl	check_in_list
	.type	check_in_list, @function
check_in_list:
.LFB1033:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L902
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rcx, %rcx
	je	.L892
	movq	%rdx, %r12
	movl	%esi, %r13d
	testl	%r8d, %r8d
	je	.L898
	movzwl	%si, %eax
	movq	%rdi, %r14
	xorl	%ebx, %ebx
	movl	%eax, -52(%rbp)
	jmp	.L896
	.p2align 4,,10
	.p2align 3
.L894:
	addq	$1, %rbx
	cmpq	%rbx, %r15
	jbe	.L892
.L896:
	cmpw	%r13w, (%r12,%rbx,2)
	jne	.L894
	movl	-52(%rbp), %esi
	movl	$131078, %edx
	movq	%r14, %rdi
	call	tls_curve_allowed@PLT
	testl	%eax, %eax
	je	.L894
	movl	$1, %eax
.L908:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L892:
	.cfi_restore_state
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L902:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	xorl	%eax, %eax
	ret
.L898:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	xorl	%eax, %eax
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L907:
	addq	$1, %rax
	cmpq	%rax, %r15
	jbe	.L892
.L893:
	cmpw	%r13w, (%r12,%rax,2)
	jne	.L907
	movl	$1, %eax
	jmp	.L908
	.cfi_endproc
.LFE1033:
	.size	check_in_list, .-check_in_list
	.p2align 4
	.globl	create_synthetic_message_hash
	.type	create_synthetic_message_hash, @function
create_synthetic_message_hash:
.LFB1034:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$112, %rsp
	movq	%rdx, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -116(%rbp)
	testq	%rsi, %rsi
	je	.L934
.L910:
	movq	%r12, %rdi
	call	ssl3_init_finished_mac@PLT
	testl	%eax, %eax
	jne	.L935
.L913:
	xorl	%eax, %eax
.L909:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L936
	addq	$112, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L935:
	.cfi_restore_state
	leaq	-116(%rbp), %rsi
	movl	$4, %edx
	movq	%r12, %rdi
	movb	$-2, -116(%rbp)
	movq	-136(%rbp), %rax
	movb	%al, -113(%rbp)
	call	ssl3_finish_mac@PLT
	testl	%eax, %eax
	je	.L913
	movq	-136(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	ssl3_finish_mac@PLT
	testl	%eax, %eax
	je	.L913
	movl	$1, %eax
	testq	%r14, %r14
	je	.L909
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	ssl3_finish_mac@PLT
	testl	%eax, %eax
	je	.L913
	movq	168(%r12), %rax
	movq	%r12, %rdi
	movq	552(%rax), %rdx
	movq	136(%r12), %rax
	movq	8(%rax), %rsi
	addq	$4, %rdx
	call	ssl3_finish_mac@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L909
	.p2align 4,,10
	.p2align 3
.L934:
	movq	$0, -136(%rbp)
	xorl	%esi, %esi
	call	ssl3_digest_cached_records@PLT
	testl	%eax, %eax
	je	.L913
	leaq	-112(%rbp), %r13
	leaq	-136(%rbp), %rcx
	movl	$64, %edx
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	ssl_handshake_hash@PLT
	testl	%eax, %eax
	jne	.L910
	jmp	.L913
.L936:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1034:
	.size	create_synthetic_message_hash, .-create_synthetic_message_hash
	.p2align 4
	.globl	parse_ca_names
	.type	parse_ca_names, @function
parse_ca_names:
.LFB1036:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	ca_dn_cmp(%rip), %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	OPENSSL_sk_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L962
	movq	8(%r13), %rax
	cmpq	$1, %rax
	jbe	.L940
	movq	0(%r13), %r12
	subq	$2, %rax
	movzwl	(%r12), %ebx
	rolw	$8, %bx
	movzwl	%bx, %ebx
	cmpq	%rbx, %rax
	jb	.L940
	addq	$2, %r12
	subq	%rbx, %rax
	leaq	(%r12,%rbx), %rdx
	movq	%rax, 8(%r13)
	movq	%rdx, 0(%r13)
	testq	%rbx, %rbx
	je	.L942
	cmpq	$1, %rbx
	je	.L943
	leaq	-64(%rbp), %rax
	movq	%rax, -72(%rbp)
.L950:
	movzwl	(%r12), %edx
	subq	$2, %rbx
	rolw	$8, %dx
	movzwl	%dx, %edx
	cmpq	%rbx, %rdx
	ja	.L943
	movq	-72(%rbp), %rsi
	addq	$2, %r12
	xorl	%edi, %edi
	subq	%rdx, %rbx
	movq	%r12, -64(%rbp)
	addq	%rdx, %r12
	call	d2i_X509_NAME@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L963
	cmpq	%r12, -64(%rbp)
	jne	.L964
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L965
	testq	%rbx, %rbx
	je	.L942
	cmpq	$1, %rbx
	jne	.L950
.L943:
	movl	$2263, %r9d
	jmp	.L961
	.p2align 4,,10
	.p2align 3
.L940:
	movl	$2252, %r9d
.L961:
	movl	$159, %ecx
	movl	$541, %edx
	movq	%r14, %rdi
	xorl	%r13d, %r13d
	leaq	.LC0(%rip), %r8
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
.L939:
	movq	X509_NAME_free@GOTPCREL(%rip), %rsi
	movq	%r15, %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	%r13, %rdi
	call	X509_NAME_free@PLT
	xorl	%eax, %eax
.L937:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L966
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L962:
	.cfi_restore_state
	movl	$65, %ecx
	movl	$541, %edx
	movq	%r14, %rdi
	xorl	%r13d, %r13d
	movl	$2246, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L939
	.p2align 4,,10
	.p2align 3
.L942:
	movq	168(%r14), %rax
	movq	X509_NAME_free@GOTPCREL(%rip), %rsi
	movq	608(%rax), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	168(%r14), %rax
	movq	%r15, 608(%rax)
	movl	$1, %eax
	jmp	.L937
.L963:
	movl	$2270, %r9d
	leaq	.LC0(%rip), %r8
	movl	$13, %ecx
	movq	%r14, %rdi
	movl	$541, %edx
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L939
.L964:
	movl	$2275, %r9d
	leaq	.LC0(%rip), %r8
	movl	$131, %ecx
	movq	%r14, %rdi
	movl	$541, %edx
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L939
.L965:
	movl	$2281, %r9d
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
	movq	%r14, %rdi
	movl	$541, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L939
.L966:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1036:
	.size	parse_ca_names, .-parse_ca_names
	.p2align 4
	.globl	get_ca_names
	.type	get_ca_names, @function
get_ca_names:
.LFB1037:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	56(%rdi), %eax
	movq	%rdi, %r12
	testl	%eax, %eax
	je	.L969
	call	SSL_get_client_CA_list@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L969
	movq	%rax, %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jne	.L977
.L969:
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	SSL_get0_CA_list@PLT
	.p2align 4,,10
	.p2align 3
.L977:
	.cfi_restore_state
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1037:
	.size	get_ca_names, .-get_ca_names
	.p2align 4
	.globl	construct_ca_names
	.type	construct_ca_names, @function
construct_ca_names:
.LFB1038:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$2, %esi
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -80(%rbp)
	movq	%rdx, %rdi
	movq	%rdx, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L995
	xorl	%r12d, %r12d
	leaq	-64(%rbp), %r14
	testq	%r13, %r13
	jne	.L981
	jmp	.L986
	.p2align 4,,10
	.p2align 3
.L985:
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L984
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	i2d_X509_NAME@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	js	.L984
	movq	-72(%rbp), %rdi
	movslq	%eax, %rsi
	movl	$2, %ecx
	movq	%r14, %rdx
	call	WPACKET_sub_allocate_bytes__@PLT
	testl	%eax, %eax
	je	.L984
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	i2d_X509_NAME@PLT
	cmpl	%ebx, %eax
	jne	.L984
	addl	$1, %r12d
.L981:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jl	.L985
.L986:
	movq	-72(%rbp), %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L982
	movl	$1, %eax
	jmp	.L978
	.p2align 4,,10
	.p2align 3
.L984:
	movq	-80(%rbp), %rdi
	movl	$2337, %r9d
	movl	$68, %ecx
	leaq	.LC0(%rip), %r8
	movl	$552, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L978:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L996
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L995:
	.cfi_restore_state
	movl	%eax, -72(%rbp)
	movl	$2319, %r9d
.L994:
	movq	-80(%rbp), %rdi
	movl	$68, %ecx
	movl	$552, %edx
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movl	-72(%rbp), %eax
	jmp	.L978
.L982:
	movl	%eax, -72(%rbp)
	movl	$2345, %r9d
	jmp	.L994
.L996:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1038:
	.size	construct_ca_names, .-construct_ca_names
	.p2align 4
	.globl	construct_key_exchange_tbs
	.type	construct_key_exchange_tbs, @function
construct_key_exchange_tbs:
.LFB1039:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movl	$2358, %edx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	64(%rcx), %r13
	pushq	%r12
	movq	%r13, %rdi
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -56(%rbp)
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L1001
	movq	%rax, %rbx
	movq	168(%r14), %rax
	movq	%r12, %rdx
	movq	%r15, %rsi
	leaq	64(%rbx), %rdi
	movdqu	184(%rax), %xmm0
	movups	%xmm0, (%rbx)
	movdqu	200(%rax), %xmm1
	movups	%xmm1, 16(%rbx)
	movq	168(%r14), %rax
	movdqu	152(%rax), %xmm2
	movups	%xmm2, 32(%rbx)
	movdqu	168(%rax), %xmm3
	movups	%xmm3, 48(%rbx)
	call	memcpy@PLT
	movq	-56(%rbp), %rax
	movq	%rbx, (%rax)
.L997:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1001:
	.cfi_restore_state
	movl	$65, %ecx
	movl	$553, %edx
	movq	%r14, %rdi
	xorl	%r13d, %r13d
	movl	$2361, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L997
	.cfi_endproc
.LFE1039:
	.size	construct_key_exchange_tbs, .-construct_key_exchange_tbs
	.p2align 4
	.globl	tls13_save_handshake_digest_for_pha
	.type	tls13_save_handshake_digest_for_pha, @function
tls13_save_handshake_digest_for_pha:
.LFB1040:
	.cfi_startproc
	endbr64
	cmpq	$0, 1968(%rdi)
	je	.L1003
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1003:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	call	ssl3_digest_cached_records@PLT
	testl	%eax, %eax
	je	.L1002
	call	EVP_MD_CTX_new@PLT
	movq	%rax, 1968(%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1013
	movq	168(%r12), %rax
	movq	232(%rax), %rsi
	call	EVP_MD_CTX_copy_ex@PLT
	testl	%eax, %eax
	je	.L1014
	movl	$1, %eax
.L1002:
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1013:
	.cfi_restore_state
	movl	$2387, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$618, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L1002
	.p2align 4,,10
	.p2align 3
.L1014:
	movl	$68, %ecx
	movl	$618, %edx
	movq	%r12, %rdi
	movl	%eax, -20(%rbp)
	movl	$2394, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movl	-20(%rbp), %eax
	jmp	.L1002
	.cfi_endproc
.LFE1040:
	.size	tls13_save_handshake_digest_for_pha, .-tls13_save_handshake_digest_for_pha
	.p2align 4
	.globl	tls_process_finished
	.type	tls_process_finished, @function
tls_process_finished:
.LFB1012:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movl	56(%rdi), %esi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	jne	.L1016
.L1022:
	movq	8(%r12), %rax
	movq	192(%rax), %rdx
	movl	96(%rdx), %r13d
	andl	$8, %r13d
	jne	.L1018
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L1020
	cmpl	$65536, %eax
	je	.L1020
	leaq	2112(%r12), %rdi
	call	RECORD_LAYER_processed_read_pending@PLT
	testl	%eax, %eax
	jne	.L1072
	movq	8(%r12), %rax
	movq	168(%r12), %rsi
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L1024
	movl	(%rax), %eax
	jmp	.L1034
	.p2align 4,,10
	.p2align 3
.L1018:
	movq	168(%r12), %rsi
.L1024:
	movl	240(%rsi), %ecx
	testl	%ecx, %ecx
	je	.L1073
.L1025:
	movl	$0, 240(%rsi)
	movq	544(%rsi), %r13
	cmpq	8(%rbx), %r13
	je	.L1027
	movl	$111, %ecx
	movl	$364, %edx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	movl	$804, %r9d
	leaq	.LC0(%rip), %r8
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
.L1015:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1074
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1016:
	.cfi_restore_state
	cmpl	$4, 1936(%rdi)
	movl	$0, 128(%rdi)
	je	.L1019
	movl	$1, 112(%rdi)
.L1019:
	movq	8(%r12), %rax
	movq	192(%rax), %rdx
	movl	96(%rdx), %r13d
	andl	$8, %r13d
	jne	.L1018
	movl	(%rax), %eax
	cmpl	$771, %eax
	jg	.L1075
.L1020:
	movq	168(%r12), %rsi
.L1034:
	cmpl	$771, %eax
	jle	.L1024
	cmpl	$65536, %eax
	jne	.L1025
	movl	240(%rsi), %ecx
	testl	%ecx, %ecx
	jne	.L1025
.L1073:
	movl	$154, %ecx
	movl	$364, %edx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	movl	$795, %r9d
	leaq	.LC0(%rip), %r8
	movl	$10, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L1027:
	movq	(%rbx), %rdi
	addq	$416, %rsi
	movq	%r13, %rdx
	call	CRYPTO_memcmp@PLT
	testl	%eax, %eax
	jne	.L1076
	cmpq	$64, %r13
	ja	.L1077
	movq	168(%r12), %rdi
	movl	56(%r12), %edx
	leaq	416(%rdi), %rsi
	testl	%edx, %edx
	jne	.L1078
	addq	$912, %rdi
	movq	%r13, %rdx
	call	memcpy@PLT
	movq	168(%r12), %rax
	movq	%r13, 976(%rax)
.L1031:
	movq	8(%r12), %rax
	movq	192(%rax), %r9
	movl	96(%r9), %r13d
	andl	$8, %r13d
	jne	.L1037
	movl	(%rax), %eax
	cmpl	$65536, %eax
	je	.L1037
	cmpl	$771, %eax
	jg	.L1079
.L1037:
	movl	$1, %r13d
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L1075:
	cmpl	$65536, %eax
	je	.L1020
	movq	%r12, %rdi
	call	tls13_save_handshake_digest_for_pha
	testl	%eax, %eax
	jne	.L1022
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L1078:
	addq	$840, %rdi
	movq	%r13, %rdx
	call	memcpy@PLT
	movq	168(%r12), %rax
	movq	%r13, 904(%rax)
	jmp	.L1031
	.p2align 4,,10
	.p2align 3
.L1076:
	movl	$149, %ecx
	movl	$364, %edx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	movl	$811, %r9d
	leaq	.LC0(%rip), %r8
	movl	$51, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L1077:
	movl	$68, %ecx
	movl	$364, %edx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	movl	$820, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L1079:
	movl	56(%r12), %eax
	testl	%eax, %eax
	je	.L1032
	cmpl	$4, 1936(%r12)
	movl	$1, %r13d
	je	.L1015
	movl	$289, %esi
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	*32(%r9)
	testl	%eax, %eax
	setne	%r13b
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L1072:
	movl	$788, %r9d
	leaq	.LC0(%rip), %r8
	movl	$182, %ecx
	movq	%r12, %rdi
	movl	$364, %edx
	movl	$10, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L1032:
	xorl	%ecx, %ecx
	leaq	380(%r12), %rdx
	leaq	-48(%rbp), %r8
	movq	%r12, %rdi
	leaq	444(%r12), %rsi
	call	*24(%r9)
	testl	%eax, %eax
	je	.L1015
	movq	8(%r12), %rax
	movl	$273, %esi
	movq	%r12, %rdi
	movq	192(%rax), %rax
	call	*32(%rax)
	testl	%eax, %eax
	je	.L1015
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	tls_process_initial_server_flight@PLT
	testl	%eax, %eax
	setne	%r13b
	jmp	.L1015
.L1074:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1012:
	.size	tls_process_finished, .-tls_process_finished
	.p2align 4
	.globl	tls13_restore_handshake_digest_for_pha
	.type	tls13_restore_handshake_digest_for_pha, @function
tls13_restore_handshake_digest_for_pha:
.LFB1041:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	1968(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L1085
	movq	168(%rdi), %rax
	movq	232(%rax), %rdi
	call	EVP_MD_CTX_copy_ex@PLT
	testl	%eax, %eax
	je	.L1086
	addq	$24, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1085:
	.cfi_restore_state
	movl	$2410, %r9d
	movl	$68, %ecx
	movl	$617, %edx
	movl	$80, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1086:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$68, %ecx
	movl	$617, %edx
	movl	%eax, -20(%rbp)
	movl	$2417, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movl	-20(%rbp), %eax
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1041:
	.size	tls13_restore_handshake_digest_for_pha, .-tls13_restore_handshake_digest_for_pha
	.section	.rodata
	.align 32
	.type	clientcontext.24096, @object
	.size	clientcontext.24096, 34
clientcontext.24096:
	.string	"TLS 1.3, client CertificateVerify"
	.align 32
	.type	servercontext.24095, @object
	.size	servercontext.24095, 34
servercontext.24095:
	.string	"TLS 1.3, server CertificateVerify"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	dtls_version_table, @object
	.size	dtls_version_table, 96
dtls_version_table:
	.long	65277
	.zero	4
	.quad	dtlsv1_2_client_method
	.quad	dtlsv1_2_server_method
	.long	65279
	.zero	4
	.quad	dtlsv1_client_method
	.quad	dtlsv1_server_method
	.long	256
	.zero	4
	.quad	dtls_bad_ver_client_method
	.quad	0
	.long	0
	.zero	4
	.quad	0
	.quad	0
	.align 32
	.type	tls_version_table, @object
	.size	tls_version_table, 144
tls_version_table:
	.long	772
	.zero	4
	.quad	tlsv1_3_client_method
	.quad	tlsv1_3_server_method
	.long	771
	.zero	4
	.quad	tlsv1_2_client_method
	.quad	tlsv1_2_server_method
	.long	770
	.zero	4
	.quad	tlsv1_1_client_method
	.quad	tlsv1_1_server_method
	.long	769
	.zero	4
	.quad	tlsv1_client_method
	.quad	tlsv1_server_method
	.long	768
	.zero	4
	.quad	0
	.quad	0
	.long	0
	.zero	4
	.quad	0
	.quad	0
	.section	.rodata
	.align 32
	.type	x509table, @object
	.size	x509table, 320
x509table:
	.long	50
	.long	40
	.long	67
	.long	42
	.long	68
	.long	42
	.long	22
	.long	48
	.long	10
	.long	45
	.long	9
	.long	42
	.long	28
	.long	42
	.long	23
	.long	44
	.long	7
	.long	51
	.long	27
	.long	42
	.long	12
	.long	45
	.long	11
	.long	42
	.long	8
	.long	51
	.long	65
	.long	42
	.long	18
	.long	48
	.long	66
	.long	42
	.long	63
	.long	42
	.long	14
	.long	42
	.long	13
	.long	42
	.long	15
	.long	42
	.long	16
	.long	42
	.long	62
	.long	42
	.long	24
	.long	48
	.long	69
	.long	80
	.long	26
	.long	43
	.long	64
	.long	42
	.long	17
	.long	80
	.long	25
	.long	48
	.long	19
	.long	48
	.long	70
	.long	80
	.long	6
	.long	42
	.long	4
	.long	42
	.long	5
	.long	42
	.long	3
	.long	48
	.long	33
	.long	48
	.long	2
	.long	48
	.long	20
	.long	48
	.long	21
	.long	48
	.long	1
	.long	80
	.long	0
	.long	46
	.globl	hrrrandom
	.align 32
	.type	hrrrandom, @object
	.size	hrrrandom, 32
hrrrandom:
	.ascii	"\317!\255t\345\232a\021\276\035\214\002\036e\270\221\302\242"
	.ascii	"\021\026z\273\214^\007\236\t\342\310\2503\234"
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.quad	2314885530818453536
	.quad	2314885530818453536
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
