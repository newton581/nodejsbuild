	.file	"m_sha1.c"
	.text
	.p2align 4
	.type	update, @function
update:
.LFB446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	call	EVP_MD_CTX_md_data@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	popq	%r12
	movq	%rax, %rdi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	SHA1_Update@PLT
	.cfi_endproc
.LFE446:
	.size	update, .-update
	.p2align 4
	.type	final, @function
final:
.LFB447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	call	EVP_MD_CTX_md_data@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	SHA1_Final@PLT
	.cfi_endproc
.LFE447:
	.size	final, .-final
	.p2align 4
	.type	init, @function
init:
.LFB445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	EVP_MD_CTX_md_data@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	%rax, %rdi
	jmp	SHA1_Init@PLT
	.cfi_endproc
.LFE445:
	.size	init, .-init
	.p2align 4
	.type	final224, @function
final224:
.LFB452:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	call	EVP_MD_CTX_md_data@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	SHA224_Final@PLT
	.cfi_endproc
.LFE452:
	.size	final224, .-final224
	.p2align 4
	.type	update224, @function
update224:
.LFB451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	call	EVP_MD_CTX_md_data@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	popq	%r12
	movq	%rax, %rdi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	SHA224_Update@PLT
	.cfi_endproc
.LFE451:
	.size	update224, .-update224
	.p2align 4
	.type	init224, @function
init224:
.LFB450:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	EVP_MD_CTX_md_data@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	%rax, %rdi
	jmp	SHA224_Init@PLT
	.cfi_endproc
.LFE450:
	.size	init224, .-init224
	.p2align 4
	.type	final256, @function
final256:
.LFB455:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	call	EVP_MD_CTX_md_data@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	SHA256_Final@PLT
	.cfi_endproc
.LFE455:
	.size	final256, .-final256
	.p2align 4
	.type	update256, @function
update256:
.LFB454:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	call	EVP_MD_CTX_md_data@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	popq	%r12
	movq	%rax, %rdi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	SHA256_Update@PLT
	.cfi_endproc
.LFE454:
	.size	update256, .-update256
	.p2align 4
	.type	init256, @function
init256:
.LFB453:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	EVP_MD_CTX_md_data@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	%rax, %rdi
	jmp	SHA256_Init@PLT
	.cfi_endproc
.LFE453:
	.size	init256, .-init256
	.p2align 4
	.type	final512, @function
final512:
.LFB465:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	call	EVP_MD_CTX_md_data@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	SHA512_Final@PLT
	.cfi_endproc
.LFE465:
	.size	final512, .-final512
	.p2align 4
	.type	update512, @function
update512:
.LFB464:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	call	EVP_MD_CTX_md_data@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	popq	%r12
	movq	%rax, %rdi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	SHA512_Update@PLT
	.cfi_endproc
.LFE464:
	.size	update512, .-update512
	.p2align 4
	.type	init512_224, @function
init512_224:
.LFB458:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	EVP_MD_CTX_md_data@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	%rax, %rdi
	jmp	sha512_224_init@PLT
	.cfi_endproc
.LFE458:
	.size	init512_224, .-init512_224
	.p2align 4
	.type	init512_256, @function
init512_256:
.LFB459:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	EVP_MD_CTX_md_data@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	%rax, %rdi
	jmp	sha512_256_init@PLT
	.cfi_endproc
.LFE459:
	.size	init512_256, .-init512_256
	.p2align 4
	.type	final384, @function
final384:
.LFB462:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	call	EVP_MD_CTX_md_data@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	SHA384_Final@PLT
	.cfi_endproc
.LFE462:
	.size	final384, .-final384
	.p2align 4
	.type	update384, @function
update384:
.LFB461:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	call	EVP_MD_CTX_md_data@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	popq	%r12
	movq	%rax, %rdi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	SHA384_Update@PLT
	.cfi_endproc
.LFE461:
	.size	update384, .-update384
	.p2align 4
	.type	init384, @function
init384:
.LFB460:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	EVP_MD_CTX_md_data@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	%rax, %rdi
	jmp	SHA384_Init@PLT
	.cfi_endproc
.LFE460:
	.size	init384, .-init384
	.p2align 4
	.type	init512, @function
init512:
.LFB463:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	EVP_MD_CTX_md_data@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	%rax, %rdi
	jmp	SHA512_Init@PLT
	.cfi_endproc
.LFE463:
	.size	init512, .-init512
	.p2align 4
	.type	ctrl, @function
ctrl:
.LFB448:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$29, %esi
	jne	.L42
	testq	%rdi, %rdi
	je	.L36
	movl	%edx, %ebx
	movq	%rcx, %r12
	call	EVP_MD_CTX_md_data@PLT
	movq	%rax, %r13
	cmpl	$48, %ebx
	je	.L38
.L40:
	xorl	%eax, %eax
.L36:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L61
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	movl	$48, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	SHA1_Update@PLT
	testl	%eax, %eax
	jle	.L40
	movdqa	.LC0(%rip), %xmm0
	leaq	-96(%rbp), %r14
	movl	$40, %edx
	movq	%r13, %rdi
	movabsq	$3906369333256140342, %rax
	movq	%r14, %rsi
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	SHA1_Update@PLT
	testl	%eax, %eax
	je	.L40
	leaq	-128(%rbp), %r15
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	SHA1_Final@PLT
	testl	%eax, %eax
	je	.L40
	movq	%r13, %rdi
	call	SHA1_Init@PLT
	testl	%eax, %eax
	je	.L40
	movl	$48, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	SHA1_Update@PLT
	testl	%eax, %eax
	jle	.L40
	movdqa	.LC1(%rip), %xmm0
	movl	$40, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movabsq	$6655295901103053916, %rax
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	SHA1_Update@PLT
	testl	%eax, %eax
	je	.L40
	movl	$20, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	SHA1_Update@PLT
	testl	%eax, %eax
	je	.L40
	movl	$20, %esi
	movq	%r15, %rdi
	call	OPENSSL_cleanse@PLT
	movl	$1, %eax
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L42:
	movl	$-2, %eax
	jmp	.L36
.L61:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE448:
	.size	ctrl, .-ctrl
	.p2align 4
	.globl	EVP_sha1
	.type	EVP_sha1, @function
EVP_sha1:
.LFB449:
	.cfi_startproc
	endbr64
	leaq	sha1_md(%rip), %rax
	ret
	.cfi_endproc
.LFE449:
	.size	EVP_sha1, .-EVP_sha1
	.p2align 4
	.globl	EVP_sha224
	.type	EVP_sha224, @function
EVP_sha224:
.LFB456:
	.cfi_startproc
	endbr64
	leaq	sha224_md(%rip), %rax
	ret
	.cfi_endproc
.LFE456:
	.size	EVP_sha224, .-EVP_sha224
	.p2align 4
	.globl	EVP_sha256
	.type	EVP_sha256, @function
EVP_sha256:
.LFB457:
	.cfi_startproc
	endbr64
	leaq	sha256_md(%rip), %rax
	ret
	.cfi_endproc
.LFE457:
	.size	EVP_sha256, .-EVP_sha256
	.p2align 4
	.globl	EVP_sha512_224
	.type	EVP_sha512_224, @function
EVP_sha512_224:
.LFB466:
	.cfi_startproc
	endbr64
	leaq	sha512_224_md(%rip), %rax
	ret
	.cfi_endproc
.LFE466:
	.size	EVP_sha512_224, .-EVP_sha512_224
	.p2align 4
	.globl	EVP_sha512_256
	.type	EVP_sha512_256, @function
EVP_sha512_256:
.LFB467:
	.cfi_startproc
	endbr64
	leaq	sha512_256_md(%rip), %rax
	ret
	.cfi_endproc
.LFE467:
	.size	EVP_sha512_256, .-EVP_sha512_256
	.p2align 4
	.globl	EVP_sha384
	.type	EVP_sha384, @function
EVP_sha384:
.LFB468:
	.cfi_startproc
	endbr64
	leaq	sha384_md(%rip), %rax
	ret
	.cfi_endproc
.LFE468:
	.size	EVP_sha384, .-EVP_sha384
	.p2align 4
	.globl	EVP_sha512
	.type	EVP_sha512, @function
EVP_sha512:
.LFB469:
	.cfi_startproc
	endbr64
	leaq	sha512_md(%rip), %rax
	ret
	.cfi_endproc
.LFE469:
	.size	EVP_sha512, .-EVP_sha512
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	sha512_md, @object
	.size	sha512_md, 80
sha512_md:
	.long	674
	.long	670
	.long	64
	.zero	4
	.quad	8
	.quad	init512
	.quad	update512
	.quad	final512
	.quad	0
	.quad	0
	.long	128
	.long	224
	.zero	8
	.align 32
	.type	sha384_md, @object
	.size	sha384_md, 80
sha384_md:
	.long	673
	.long	669
	.long	48
	.zero	4
	.quad	8
	.quad	init384
	.quad	update384
	.quad	final384
	.quad	0
	.quad	0
	.long	128
	.long	224
	.zero	8
	.align 32
	.type	sha512_256_md, @object
	.size	sha512_256_md, 80
sha512_256_md:
	.long	1095
	.long	1146
	.long	32
	.zero	4
	.quad	8
	.quad	init512_256
	.quad	update512
	.quad	final512
	.quad	0
	.quad	0
	.long	128
	.long	224
	.zero	8
	.align 32
	.type	sha512_224_md, @object
	.size	sha512_224_md, 80
sha512_224_md:
	.long	1094
	.long	1145
	.long	28
	.zero	4
	.quad	8
	.quad	init512_224
	.quad	update512
	.quad	final512
	.quad	0
	.quad	0
	.long	128
	.long	224
	.zero	8
	.align 32
	.type	sha256_md, @object
	.size	sha256_md, 80
sha256_md:
	.long	672
	.long	668
	.long	32
	.zero	4
	.quad	8
	.quad	init256
	.quad	update256
	.quad	final256
	.quad	0
	.quad	0
	.long	64
	.long	120
	.zero	8
	.align 32
	.type	sha224_md, @object
	.size	sha224_md, 80
sha224_md:
	.long	675
	.long	671
	.long	28
	.zero	4
	.quad	8
	.quad	init224
	.quad	update224
	.quad	final224
	.quad	0
	.quad	0
	.long	64
	.long	120
	.zero	8
	.align 32
	.type	sha1_md, @object
	.size	sha1_md, 80
sha1_md:
	.long	64
	.long	65
	.long	20
	.zero	4
	.quad	8
	.quad	init
	.quad	update
	.quad	final
	.quad	0
	.quad	0
	.long	64
	.long	104
	.quad	ctrl
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.quad	3906369333256140342
	.quad	3906369333256140342
	.align 16
.LC1:
	.quad	6655295901103053916
	.quad	6655295901103053916
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
