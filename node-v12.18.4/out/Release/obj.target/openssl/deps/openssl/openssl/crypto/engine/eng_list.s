	.file	"eng_list.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/engine/eng_list.c"
	.text
	.p2align 4
	.globl	ENGINE_get_first
	.type	ENGINE_get_first, @function
ENGINE_get_first:
.LFB869:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	engine_lock_init(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	do_engine_lock_init_ossl_@GOTPCREL(%rip), %rsi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L2
	movl	do_engine_lock_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	je	.L2
	movq	global_engine_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	engine_list_head(%rip), %r12
	testq	%r12, %r12
	je	.L5
	lock addl	$1, 156(%r12)
.L5:
	movq	global_engine_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movl	$135, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$195, %esi
	movl	$38, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE869:
	.size	ENGINE_get_first, .-ENGINE_get_first
	.p2align 4
	.globl	ENGINE_get_last
	.type	ENGINE_get_last, @function
ENGINE_get_last:
.LFB870:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	engine_lock_init(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	do_engine_lock_init_ossl_@GOTPCREL(%rip), %rsi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L15
	movl	do_engine_lock_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	je	.L15
	movq	global_engine_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	engine_list_tail(%rip), %r12
	testq	%r12, %r12
	je	.L18
	lock addl	$1, 156(%r12)
.L18:
	movq	global_engine_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	movl	$154, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$196, %esi
	movl	$38, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE870:
	.size	ENGINE_get_last, .-ENGINE_get_last
	.p2align 4
	.globl	ENGINE_get_next
	.type	ENGINE_get_next, @function
ENGINE_get_next:
.LFB871:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	testq	%rdi, %rdi
	je	.L34
	movq	%rdi, %r12
	movq	global_engine_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	184(%r12), %r13
	testq	%r13, %r13
	je	.L29
	lock addl	$1, 156(%r13)
.L29:
	movq	global_engine_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movq	%r12, %rdi
	call	ENGINE_free@PLT
.L26:
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	movl	$173, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$67, %edx
	xorl	%r13d, %r13d
	movl	$115, %esi
	movl	$38, %edi
	call	ERR_put_error@PLT
	jmp	.L26
	.cfi_endproc
.LFE871:
	.size	ENGINE_get_next, .-ENGINE_get_next
	.p2align 4
	.globl	ENGINE_get_prev
	.type	ENGINE_get_prev, @function
ENGINE_get_prev:
.LFB872:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	testq	%rdi, %rdi
	je	.L43
	movq	%rdi, %r12
	movq	global_engine_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	176(%r12), %r13
	testq	%r13, %r13
	je	.L38
	lock addl	$1, 156(%r13)
.L38:
	movq	global_engine_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movq	%r12, %rdi
	call	ENGINE_free@PLT
.L35:
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	movl	$193, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$67, %edx
	xorl	%r13d, %r13d
	movl	$116, %esi
	movl	$38, %edi
	call	ERR_put_error@PLT
	jmp	.L35
	.cfi_endproc
.LFE872:
	.size	ENGINE_get_prev, .-ENGINE_get_prev
	.p2align 4
	.globl	ENGINE_add
	.type	ENGINE_add, @function
ENGINE_add:
.LFB873:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testq	%rdi, %rdi
	je	.L73
	cmpq	$0, (%rdi)
	movq	%rdi, %rbx
	je	.L47
	cmpq	$0, 8(%rdi)
	je	.L47
	movq	global_engine_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	engine_list_head(%rip), %r12
	testq	%r12, %r12
	je	.L49
	movq	(%rbx), %r13
	.p2align 4,,10
	.p2align 3
.L50:
	movq	(%r12), %rdi
	movq	%r13, %rsi
	call	strcmp@PLT
	movq	184(%r12), %r12
	testl	%eax, %eax
	je	.L68
	testq	%r12, %r12
	jne	.L50
	testl	%eax, %eax
	je	.L68
	movq	engine_list_tail(%rip), %rax
	testq	%rax, %rax
	je	.L58
	cmpq	$0, 184(%rax)
	je	.L59
.L58:
	movl	$81, %r8d
.L72:
	movl	$110, %edx
	movl	$120, %esi
	movl	$38, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L56:
	movl	$223, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$110, %edx
	xorl	%r12d, %r12d
	movl	$105, %esi
	movl	$38, %edi
	call	ERR_put_error@PLT
.L60:
	movq	global_engine_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	movl	$218, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$108, %edx
	xorl	%r12d, %r12d
	movl	$105, %esi
	movl	$38, %edi
	call	ERR_put_error@PLT
.L44:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	movq	%rbx, 184(%rax)
	movq	%rax, 176(%rbx)
.L57:
	lock addl	$1, 156(%rbx)
	movl	$1, %r12d
	movq	%rbx, engine_list_tail(%rip)
	movq	$0, 184(%rbx)
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L49:
	cmpq	$0, engine_list_tail(%rip)
	movl	$69, %r8d
	jne	.L72
	leaq	engine_list_cleanup(%rip), %rdi
	movq	%rbx, engine_list_head(%rip)
	movq	$0, 176(%rbx)
	call	engine_cleanup_add_last@PLT
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L68:
	movl	$63, %r8d
	movl	$103, %edx
	movl	$120, %esi
	movl	$38, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L73:
	movl	$214, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$67, %edx
	xorl	%r12d, %r12d
	movl	$105, %esi
	movl	$38, %edi
	call	ERR_put_error@PLT
	jmp	.L44
	.cfi_endproc
.LFE873:
	.size	ENGINE_add, .-ENGINE_add
	.p2align 4
	.globl	ENGINE_remove
	.type	ENGINE_remove, @function
ENGINE_remove:
.LFB874:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	testq	%rdi, %rdi
	je	.L105
	movq	global_engine_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	engine_list_head(%rip), %rdx
	movq	%rdx, %rax
	cmpq	%rdx, %r12
	je	.L77
	testq	%rdx, %rdx
	je	.L77
	.p2align 4,,10
	.p2align 3
.L78:
	movq	184(%rax), %rax
	testq	%rax, %rax
	je	.L103
	cmpq	%rax, %r12
	jne	.L78
.L77:
	testq	%rax, %rax
	je	.L103
	movq	184(%r12), %rcx
	movq	176(%r12), %rax
	testq	%rcx, %rcx
	je	.L82
	movq	%rax, 176(%rcx)
	movq	176(%r12), %rax
.L82:
	testq	%rax, %rax
	je	.L83
	movq	%rcx, 184(%rax)
.L83:
	cmpq	%rdx, %r12
	je	.L106
	cmpq	engine_list_tail(%rip), %r12
	je	.L107
.L85:
	movq	%r12, %rdi
	xorl	%esi, %esi
	movl	$1, %r12d
	call	engine_free_util@PLT
.L81:
	movq	global_engine_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	movl	$111, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$105, %edx
	xorl	%r12d, %r12d
	movl	$121, %esi
	movl	$38, %edi
	call	ERR_put_error@PLT
	movl	$240, %r8d
	movl	$110, %edx
	leaq	.LC0(%rip), %rcx
	movl	$123, %esi
	movl	$38, %edi
	call	ERR_put_error@PLT
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L106:
	movq	184(%r12), %rdx
	cmpq	engine_list_tail(%rip), %r12
	movq	%rdx, engine_list_head(%rip)
	jne	.L85
.L107:
	movq	%rax, engine_list_tail(%rip)
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L105:
	movl	$235, %r8d
	movl	$67, %edx
	movl	$123, %esi
	movl	$38, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE874:
	.size	ENGINE_remove, .-ENGINE_remove
	.p2align 4
	.type	engine_list_cleanup, @function
engine_list_cleanup:
.LFB866:
	.cfi_startproc
	endbr64
	movq	engine_list_head(%rip), %rdi
	testq	%rdi, %rdi
	je	.L116
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	.p2align 4,,10
	.p2align 3
.L110:
	call	ENGINE_remove
	movq	engine_list_head(%rip), %rdi
	testq	%rdi, %rdi
	jne	.L110
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE866:
	.size	engine_list_cleanup, .-engine_list_cleanup
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"/dev/null"
.LC2:
	.string	"dynamic"
.LC3:
	.string	"OPENSSL_ENGINES"
.LC4:
	.string	"ID"
.LC5:
	.string	"2"
.LC6:
	.string	"DIR_LOAD"
.LC7:
	.string	"DIR_ADD"
.LC8:
	.string	"1"
.LC9:
	.string	"LIST_ADD"
.LC10:
	.string	"LOAD"
.LC11:
	.string	"id="
	.text
	.p2align 4
	.globl	ENGINE_by_id
	.type	ENGINE_by_id, @function
ENGINE_by_id:
.LFB876:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	testq	%rdi, %rdi
	je	.L161
	movq	do_engine_lock_init_ossl_@GOTPCREL(%rip), %rsi
	movq	%rdi, %r13
	leaq	engine_lock_init(%rip), %rdi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L121
	movl	do_engine_lock_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	je	.L121
	movq	global_engine_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	engine_list_head(%rip), %r12
	testq	%r12, %r12
	jne	.L123
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L125:
	movq	184(%r12), %r12
	testq	%r12, %r12
	je	.L124
.L123:
	movq	(%r12), %rsi
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L125
	testb	$4, 152(%r12)
	je	.L162
	call	ENGINE_new@PLT
	testq	%rax, %rax
	je	.L124
	movdqu	16(%r12), %xmm1
	movdqu	32(%r12), %xmm0
	movdqu	(%r12), %xmm2
	movups	%xmm1, 16(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm0, 32(%rax)
	movq	48(%r12), %rdx
	movq	%rdx, 48(%rax)
	movdqu	56(%r12), %xmm3
	movups	%xmm3, 56(%rax)
	movq	72(%r12), %rdx
	movq	%rdx, 72(%rax)
	movdqu	104(%r12), %xmm1
	movdqu	120(%r12), %xmm0
	movdqu	88(%r12), %xmm4
	movups	%xmm1, 104(%rax)
	movups	%xmm4, 88(%rax)
	movups	%xmm0, 120(%rax)
	movq	144(%r12), %rdx
	movq	%rdx, 144(%rax)
	movl	152(%r12), %edx
	movq	%rax, %r12
	movl	%edx, 152(%rax)
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L124:
	movq	global_engine_lock(%rip), %rdi
	leaq	.LC2(%rip), %r12
	call	CRYPTO_THREAD_unlock@PLT
	movl	$8, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L133
	leaq	.LC3(%rip), %rdi
	call	ossl_safe_getenv@PLT
	movq	%r12, %rdi
	testq	%rax, %rax
	movq	%rax, %r14
	leaq	.LC1(%rip), %rax
	cmove	%rax, %r14
	call	ENGINE_by_id
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L133
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	leaq	.LC4(%rip), %rsi
	movq	%rax, %rdi
	call	ENGINE_ctrl_cmd_string@PLT
	testl	%eax, %eax
	je	.L131
	xorl	%ecx, %ecx
	leaq	.LC5(%rip), %rdx
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	ENGINE_ctrl_cmd_string@PLT
	testl	%eax, %eax
	je	.L131
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	ENGINE_ctrl_cmd_string@PLT
	testl	%eax, %eax
	je	.L131
	xorl	%ecx, %ecx
	leaq	.LC8(%rip), %rdx
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	call	ENGINE_ctrl_cmd_string@PLT
	testl	%eax, %eax
	je	.L131
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leaq	.LC10(%rip), %rsi
	movq	%r12, %rdi
	call	ENGINE_ctrl_cmd_string@PLT
	testl	%eax, %eax
	jne	.L118
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L121:
	movl	$286, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$106, %esi
	movl	$38, %edi
	call	ERR_put_error@PLT
.L118:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore_state
	lock addl	$1, 156(%r12)
.L129:
	movq	global_engine_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore_state
	xorl	%r12d, %r12d
.L131:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	ENGINE_free@PLT
	movl	$334, %r8d
	movl	$116, %edx
	leaq	.LC0(%rip), %rcx
	movl	$106, %esi
	movl	$38, %edi
	call	ERR_put_error@PLT
	movq	%r13, %rdx
	xorl	%eax, %eax
	movl	$2, %edi
	leaq	.LC11(%rip), %rsi
	call	ERR_add_error_data@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	movl	$282, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$67, %edx
	xorl	%r12d, %r12d
	movl	$106, %esi
	movl	$38, %edi
	call	ERR_put_error@PLT
	jmp	.L118
	.cfi_endproc
.LFE876:
	.size	ENGINE_by_id, .-ENGINE_by_id
	.p2align 4
	.globl	ENGINE_up_ref
	.type	ENGINE_up_ref, @function
ENGINE_up_ref:
.LFB877:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L170
	lock addl	$1, 156(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$67, %edx
	movl	$190, %esi
	movl	$38, %edi
	movl	$344, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE877:
	.size	ENGINE_up_ref, .-ENGINE_up_ref
	.local	engine_list_tail
	.comm	engine_list_tail,8,8
	.local	engine_list_head
	.comm	engine_list_head,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
