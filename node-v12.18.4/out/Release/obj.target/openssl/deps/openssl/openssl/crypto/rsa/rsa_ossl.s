	.file	"rsa_ossl.c"
	.text
	.p2align 4
	.type	rsa_ossl_init, @function
rsa_ossl_init:
.LFB493:
	.cfi_startproc
	endbr64
	orl	$6, 116(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE493:
	.size	rsa_ossl_init, .-rsa_ossl_init
	.p2align 4
	.type	rsa_ossl_finish, @function
rsa_ossl_finish:
.LFB494:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	120(%rdi), %rdi
	xorl	%ebx, %ebx
	call	BN_MONT_CTX_free@PLT
	movq	128(%r12), %rdi
	call	BN_MONT_CTX_free@PLT
	movq	136(%r12), %rdi
	call	BN_MONT_CTX_free@PLT
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L5:
	movq	88(%r12), %rdi
	movl	%ebx, %esi
	addl	$1, %ebx
	call	OPENSSL_sk_value@PLT
	movq	32(%rax), %rdi
	call	BN_MONT_CTX_free@PLT
.L4:
	movq	88(%r12), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L5
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE494:
	.size	rsa_ossl_finish, .-rsa_ossl_finish
	.p2align 4
	.type	rsa_ossl_mod_exp, @function
rsa_ossl_mod_exp:
.LFB492:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$136, %rsp
	movq	%rsi, -88(%rbp)
	movq	%rdi, -112(%rbp)
	movq	%rcx, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -128(%rbp)
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	BN_CTX_get@PLT
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L197
	xorl	%r15d, %r15d
	cmpl	$1, 4(%rbx)
	je	.L202
.L10:
	movl	116(%rbx), %eax
	testb	$4, %al
	jne	.L203
.L12:
	testb	$2, %al
	je	.L60
	movq	24(%rbx), %rdx
	movq	168(%rbx), %rsi
	leaq	120(%rbx), %rdi
	movq	%r12, %rcx
	call	BN_MONT_CTX_set_locked@PLT
	testq	%rax, %rax
	jne	.L60
	.p2align 4,,10
	.p2align 3
.L197:
	xorl	%eax, %eax
.L9:
	movq	%r12, %rdi
	movl	%eax, -88(%rbp)
	call	BN_CTX_end@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	movl	-88(%rbp), %eax
	jne	.L204
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	call	BN_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L197
	movq	-88(%rbp), %rsi
	movl	$4, %edx
	movq	%rax, %rdi
	call	BN_with_flags@PLT
	movq	56(%rbx), %rcx
	movq	-96(%rbp), %rsi
	xorl	%edi, %edi
	movq	%r12, %r8
	movq	%r14, %rdx
	call	BN_div@PLT
	testl	%eax, %eax
	je	.L200
	call	BN_new@PLT
	testq	%rax, %rax
	je	.L13
	movq	72(%rbx), %rsi
	movq	%rax, %rdi
	movl	$4, %edx
	movq	%rax, -104(%rbp)
	call	BN_with_flags@PLT
	movq	-104(%rbp), %r10
	movq	8(%rbx), %rax
	movq	%r12, %r8
	movq	56(%rbx), %rcx
	movq	136(%rbx), %r9
	movq	%r13, %rdi
	movq	%r10, %rdx
	movq	-96(%rbp), %rsi
	call	*48(%rax)
	movq	-104(%rbp), %r10
	testl	%eax, %eax
	je	.L205
	movq	%r10, %rdi
	call	BN_free@PLT
	movq	48(%rbx), %rcx
	movq	-96(%rbp), %rsi
	xorl	%edi, %edi
	movq	%r12, %r8
	movq	%r14, %rdx
	call	BN_div@PLT
	testl	%eax, %eax
	je	.L200
	movq	%r14, %rdi
	call	BN_free@PLT
	call	BN_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L197
	movq	64(%rbx), %rsi
	movq	%rax, %rdi
	movl	$4, %edx
	call	BN_with_flags@PLT
	movq	8(%rbx), %rax
	movq	48(%rbx), %rcx
	movq	%r12, %r8
	movq	128(%rbx), %r9
	movq	-96(%rbp), %rsi
	movq	%r14, %rdx
	movq	-112(%rbp), %rdi
	call	*48(%rax)
	testl	%eax, %eax
	je	.L200
	movq	%r14, %rdi
	call	BN_free@PLT
	testl	%r15d, %r15d
	je	.L26
	call	BN_new@PLT
	movq	%rax, -136(%rbp)
	movq	%rax, %r14
	call	BN_new@PLT
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L199
	leaq	-80(%rbp), %rax
	xorl	%ecx, %ecx
	movq	%rax, -152(%rbp)
	testq	%r14, %r14
	je	.L199
	movq	%r13, -168(%rbp)
	movq	%rcx, %r14
	movl	%r15d, -156(%rbp)
	movq	-96(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L27:
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, %r10
	movq	-152(%rbp), %rax
	movq	%r10, (%rax,%r14,8)
	testq	%r10, %r10
	je	.L199
	movq	88(%rbx), %rdi
	movl	%r14d, %esi
	movq	%r10, -144(%rbp)
	call	OPENSSL_sk_value@PLT
	movq	-88(%rbp), %rsi
	movq	-104(%rbp), %rdi
	movl	$4, %edx
	movq	%rax, %r13
	call	BN_with_flags@PLT
	movq	8(%r13), %rsi
	movl	$4, %edx
	movq	-136(%rbp), %rdi
	call	BN_with_flags@PLT
	movq	0(%r13), %rcx
	movq	-104(%rbp), %rdx
	xorl	%edi, %edi
	movq	%r12, %r8
	movq	%r15, %rsi
	call	BN_div@PLT
	movq	-144(%rbp), %r10
	testl	%eax, %eax
	je	.L198
	movq	8(%rbx), %rax
	movq	32(%r13), %r9
	movq	%r12, %r8
	movq	%r15, %rsi
	movq	0(%r13), %rcx
	movq	-136(%rbp), %rdx
	movq	%r10, %rdi
	call	*48(%rax)
	testl	%eax, %eax
	je	.L198
	addq	$1, %r14
	cmpl	%r14d, -156(%rbp)
	jg	.L27
	movq	-104(%rbp), %rdi
	movl	-156(%rbp), %r15d
	movq	-168(%rbp), %r13
	call	BN_free@PLT
	movq	-136(%rbp), %rdi
	call	BN_free@PLT
.L26:
	movq	-112(%rbp), %r14
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	BN_sub@PLT
	testl	%eax, %eax
	je	.L197
	movq	%r14, %rdi
	call	BN_is_negative@PLT
	testl	%eax, %eax
	je	.L34
	movq	-112(%rbp), %rsi
	movq	48(%rbx), %rdx
	movq	%rsi, %rdi
	call	BN_add@PLT
	testl	%eax, %eax
	je	.L197
.L34:
	movq	80(%rbx), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rcx
	movq	-96(%rbp), %rdi
	call	BN_mul@PLT
	testl	%eax, %eax
	je	.L197
	call	BN_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L197
	movq	-96(%rbp), %rsi
	movl	$4, %edx
	movq	%rax, %rdi
	call	BN_with_flags@PLT
	movq	48(%rbx), %rcx
	movq	-112(%rbp), %rsi
	xorl	%edi, %edi
	movq	%r12, %r8
	movq	%r14, %rdx
	call	BN_div@PLT
	testl	%eax, %eax
	je	.L200
	movq	%r14, %rdi
	call	BN_free@PLT
	movq	-112(%rbp), %rdi
	call	BN_is_negative@PLT
	testl	%eax, %eax
	je	.L38
	movq	-112(%rbp), %rsi
	movq	48(%rbx), %rdx
	movq	%rsi, %rdi
	call	BN_add@PLT
	testl	%eax, %eax
	je	.L197
.L38:
	movq	56(%rbx), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rcx
	movq	-96(%rbp), %rdi
	call	BN_mul@PLT
	testl	%eax, %eax
	je	.L197
	movq	-96(%rbp), %rsi
	movq	-112(%rbp), %rdi
	movq	%r13, %rdx
	call	BN_add@PLT
	testl	%eax, %eax
	je	.L197
	testl	%r15d, %r15d
	jne	.L206
.L20:
	movq	32(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L48
	movq	24(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L48
	movq	8(%rbx), %rax
	movq	BN_mod_exp_mont@GOTPCREL(%rip), %rsi
	cmpq	%rsi, 48(%rax)
	je	.L207
	movq	-112(%rbp), %r15
	movq	%r15, %rdi
	call	bn_correct_top@PLT
	movq	8(%rbx), %rax
	movq	24(%rbx), %rcx
	movq	%r12, %r8
	movq	32(%rbx), %rdx
	movq	120(%rbx), %r9
	movq	%r15, %rsi
	movq	-120(%rbp), %rdi
	call	*48(%rax)
	testl	%eax, %eax
	je	.L197
.L52:
	movq	-120(%rbp), %rsi
	movq	-88(%rbp), %rdx
	movq	%rsi, %rdi
	call	BN_sub@PLT
	testl	%eax, %eax
	je	.L197
	movq	-120(%rbp), %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L48
	movq	-120(%rbp), %r15
	movq	24(%rbx), %rcx
	xorl	%edi, %edi
	movq	%r12, %r8
	movq	%r15, %rdx
	movq	%r15, %rsi
	call	BN_div@PLT
	testl	%eax, %eax
	je	.L197
	movq	%r15, %rdi
	call	BN_is_negative@PLT
	testl	%eax, %eax
	je	.L56
	movq	-120(%rbp), %rsi
	movq	24(%rbx), %rdx
	movq	%rsi, %rdi
	call	BN_add@PLT
	testl	%eax, %eax
	je	.L197
.L56:
	movq	-120(%rbp), %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L208
.L48:
	movq	-112(%rbp), %rdi
	call	bn_correct_top@PLT
	movl	$1, %eax
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L203:
	call	BN_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L197
	movq	48(%rbx), %rsi
	movl	$4, %edx
	movq	%rax, %rdi
	call	BN_with_flags@PLT
	movq	168(%rbx), %rsi
	movq	%r12, %rcx
	movq	%r14, %rdx
	leaq	128(%rbx), %rdi
	call	BN_MONT_CTX_set_locked@PLT
	testq	%rax, %rax
	je	.L13
	movq	56(%rbx), %rsi
	movl	$4, %edx
	movq	%r14, %rdi
	call	BN_with_flags@PLT
	movq	168(%rbx), %rsi
	movq	%r12, %rcx
	movq	%r14, %rdx
	leaq	136(%rbx), %rdi
	call	BN_MONT_CTX_set_locked@PLT
	testq	%rax, %rax
	je	.L13
	xorl	%r8d, %r8d
	testl	%r15d, %r15d
	je	.L209
	movl	%r15d, -104(%rbp)
	movq	%rbx, %r15
	movq	%r13, -136(%rbp)
	movl	%r8d, %r13d
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L16:
	addl	$1, %r13d
	cmpl	%r13d, -104(%rbp)
	je	.L210
.L14:
	movq	88(%r15), %rdi
	movl	%r13d, %esi
	call	OPENSSL_sk_value@PLT
	movl	$4, %edx
	movq	%r14, %rdi
	movq	(%rax), %rsi
	movq	%rax, %rbx
	call	BN_with_flags@PLT
	leaq	32(%rbx), %rdi
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	168(%r15), %rsi
	call	BN_MONT_CTX_set_locked@PLT
	testq	%rax, %rax
	jne	.L16
.L13:
	movq	%r14, %rdi
	call	BN_free@PLT
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L200:
	movq	%r14, %rdi
	movl	%eax, -88(%rbp)
	call	BN_free@PLT
	movl	-88(%rbp), %eax
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L202:
	movq	88(%rbx), %rdi
	call	OPENSSL_sk_num@PLT
	movl	%eax, %r15d
	leal	-1(%rax), %eax
	cmpl	$2, %eax
	ja	.L197
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L210:
	movq	%r15, %rbx
	movq	%r14, %rdi
	movq	-136(%rbp), %r13
	movl	-104(%rbp), %r15d
	call	BN_free@PLT
	movl	116(%rbx), %eax
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L205:
	movq	%r14, %rdi
	movl	%eax, -96(%rbp)
	movq	%r10, -88(%rbp)
	call	BN_free@PLT
	movq	-88(%rbp), %r10
	movq	%r10, %rdi
	call	BN_free@PLT
	movl	-96(%rbp), %eax
	jmp	.L9
.L209:
	movq	%r14, %rdi
	call	BN_free@PLT
	movq	8(%rbx), %rax
	movq	BN_mod_exp_mont@GOTPCREL(%rip), %rcx
	cmpq	%rcx, 48(%rax)
	je	.L18
.L201:
	movl	116(%rbx), %eax
	jmp	.L12
.L18:
	movq	56(%rbx), %rdi
	call	BN_num_bits@PLT
	movq	48(%rbx), %rdi
	movl	%eax, %r14d
	call	BN_num_bits@PLT
	cmpl	%eax, %r14d
	jne	.L201
	testb	$2, 116(%rbx)
	jne	.L211
.L59:
	movq	-88(%rbp), %r15
	movq	136(%rbx), %rdx
	movq	%r12, %rcx
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	bn_from_mont_fixed_top@PLT
	testl	%eax, %eax
	je	.L197
	movq	136(%rbx), %rdx
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	bn_to_mont_fixed_top@PLT
	testl	%eax, %eax
	je	.L197
	movq	56(%rbx), %rcx
	movq	72(%rbx), %rdx
	movq	%r12, %r8
	movq	%r13, %rsi
	movq	136(%rbx), %r9
	movq	%r13, %rdi
	call	BN_mod_exp_mont_consttime@PLT
	testl	%eax, %eax
	je	.L197
	movq	%r15, %rsi
	movq	-96(%rbp), %r15
	movq	128(%rbx), %rdx
	movq	%r12, %rcx
	movq	%r15, %rdi
	call	bn_from_mont_fixed_top@PLT
	testl	%eax, %eax
	je	.L197
	movq	128(%rbx), %rdx
	movq	%r12, %rcx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	bn_to_mont_fixed_top@PLT
	testl	%eax, %eax
	je	.L197
	movq	48(%rbx), %rcx
	movq	64(%rbx), %rdx
	movq	%r12, %r8
	movq	%r15, %rsi
	movq	128(%rbx), %r9
	movq	%r15, %rdi
	call	BN_mod_exp_mont_consttime@PLT
	testl	%eax, %eax
	je	.L197
	movq	48(%rbx), %rcx
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	bn_mod_sub_fixed_top@PLT
	testl	%eax, %eax
	je	.L197
	movq	128(%rbx), %rdx
	movq	%r12, %rcx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	bn_to_mont_fixed_top@PLT
	testl	%eax, %eax
	je	.L197
	movq	128(%rbx), %rcx
	movq	%r12, %r8
	movq	%r15, %rsi
	movq	%r15, %rdi
	movq	80(%rbx), %rdx
	call	bn_mul_mont_fixed_top@PLT
	testl	%eax, %eax
	je	.L197
	movq	%r15, %rsi
	movq	-112(%rbp), %r15
	movq	56(%rbx), %rdx
	movq	%r12, %rcx
	movq	%r15, %rdi
	call	bn_mul_fixed_top@PLT
	testl	%eax, %eax
	je	.L197
	movq	24(%rbx), %rcx
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	bn_mod_add_fixed_top@PLT
	testl	%eax, %eax
	je	.L197
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L199:
	movq	-104(%rbp), %rdi
	call	BN_free@PLT
	movq	-136(%rbp), %rdi
	call	BN_free@PLT
	xorl	%eax, %eax
	jmp	.L9
.L198:
	movq	-104(%rbp), %rdi
	movl	%eax, -88(%rbp)
	call	BN_free@PLT
	movq	-136(%rbp), %rdi
	call	BN_free@PLT
	movl	-88(%rbp), %eax
	jmp	.L9
.L204:
	call	__stack_chk_fail@PLT
.L206:
	call	BN_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L197
	leaq	-80(%rbp), %rax
	movl	%r15d, -136(%rbp)
	xorl	%r14d, %r14d
	movq	%rax, -152(%rbp)
	movq	%rbx, -104(%rbp)
	movq	-96(%rbp), %rbx
.L46:
	movq	-104(%rbp), %rax
	movl	%r14d, %esi
	movq	88(%rax), %rdi
	call	OPENSSL_sk_value@PLT
	movq	-112(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%rax, %r15
	movq	-152(%rbp), %rax
	movq	(%rax,%r14,8), %rsi
	call	BN_sub@PLT
	testl	%eax, %eax
	je	.L196
	movq	16(%r15), %rdx
	movq	-128(%rbp), %rdi
	movq	%r12, %rcx
	movq	%rbx, %rsi
	call	BN_mul@PLT
	testl	%eax, %eax
	je	.L196
	movq	-128(%rbp), %rsi
	movl	$4, %edx
	movq	%r13, %rdi
	call	BN_with_flags@PLT
	movq	(%r15), %rcx
	xorl	%edi, %edi
	movq	%r12, %r8
	movq	%r13, %rdx
	movq	%rbx, %rsi
	call	BN_div@PLT
	testl	%eax, %eax
	je	.L196
	movq	%rbx, %rdi
	call	BN_is_negative@PLT
	testl	%eax, %eax
	je	.L43
	movq	(%r15), %rdx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	BN_add@PLT
	testl	%eax, %eax
	je	.L196
.L43:
	movq	24(%r15), %rdx
	movq	%r12, %rcx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	BN_mul@PLT
	testl	%eax, %eax
	je	.L196
	movq	-112(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%rsi, %rdi
	call	BN_add@PLT
	testl	%eax, %eax
	je	.L196
	addq	$1, %r14
	cmpl	%r14d, -136(%rbp)
	jg	.L46
	movq	%r13, %rdi
	movq	-104(%rbp), %rbx
	call	BN_free@PLT
	jmp	.L20
.L196:
	movq	%r13, %rdi
	movl	%eax, -88(%rbp)
	call	BN_free@PLT
	movl	-88(%rbp), %eax
	jmp	.L9
.L211:
	movq	24(%rbx), %rdx
	movq	168(%rbx), %rsi
	leaq	120(%rbx), %rdi
	movq	%r12, %rcx
	call	BN_MONT_CTX_set_locked@PLT
	testq	%rax, %rax
	jne	.L59
	jmp	.L197
.L207:
	movq	120(%rbx), %r9
	movq	-112(%rbp), %rsi
	movq	%r12, %r8
	movq	-120(%rbp), %rdi
	call	BN_mod_exp_mont@PLT
	testl	%eax, %eax
	jne	.L52
	jmp	.L197
.L208:
	call	BN_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L197
	movq	40(%rbx), %rsi
	movq	%rax, %rdi
	movl	$4, %edx
	call	BN_with_flags@PLT
	movq	8(%rbx), %rax
	movq	24(%rbx), %rcx
	movq	%r12, %r8
	movq	120(%rbx), %r9
	movq	-88(%rbp), %rsi
	movq	%r13, %rdx
	movq	-112(%rbp), %rdi
	call	*48(%rax)
	testl	%eax, %eax
	je	.L196
	movq	%r13, %rdi
	call	BN_free@PLT
	jmp	.L48
	.cfi_endproc
.LFE492:
	.size	rsa_ossl_mod_exp, .-rsa_ossl_mod_exp
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/rsa/rsa_ossl.c"
	.text
	.p2align 4
	.type	rsa_ossl_public_decrypt, @function
rsa_ossl_public_decrypt:
.LFB491:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$56, %rsp
	movq	24(%rcx), %rdi
	movq	%rsi, -72(%rbp)
	movq	%rdx, -88(%rbp)
	movl	%r8d, -80(%rbp)
	call	BN_num_bits@PLT
	cmpl	$16384, %eax
	jg	.L245
	movq	32(%rbx), %rsi
	movq	24(%rbx), %rdi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	jle	.L246
	movq	24(%rbx), %rdi
	call	BN_num_bits@PLT
	cmpl	$3072, %eax
	jg	.L247
.L216:
	call	BN_CTX_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L228
	movq	%rax, %rdi
	call	BN_CTX_start@PLT
	movq	%r14, %rdi
	call	BN_CTX_get@PLT
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	call	BN_CTX_get@PLT
	movq	24(%rbx), %rdi
	movq	%rax, -56(%rbp)
	call	BN_num_bits@PLT
	leaq	.LC0(%rip), %rsi
	leal	14(%rax), %edx
	addl	$7, %eax
	cmovs	%edx, %eax
	movl	$534, %edx
	sarl	$3, %eax
	movslq	%eax, %r15
	movq	%r15, %rdi
	movl	%r15d, -76(%rbp)
	call	CRYPTO_malloc@PLT
	cmpq	$0, -56(%rbp)
	movq	%rax, %r13
	je	.L233
	testq	%rax, %rax
	je	.L233
	cmpl	%r12d, -76(%rbp)
	jl	.L248
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rdi
	movl	%r12d, %esi
	movl	$-1, %r12d
	call	BN_bin2bn@PLT
	testq	%rax, %rax
	je	.L217
	movq	24(%rbx), %rsi
	movq	-64(%rbp), %rdi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	jns	.L249
	movq	24(%rbx), %r11
	testb	$2, 116(%rbx)
	jne	.L250
.L222:
	movq	8(%rbx), %rax
	movq	32(%rbx), %rdx
	movq	%r14, %r8
	movq	%r11, %rcx
	movq	120(%rbx), %r9
	movq	-64(%rbp), %rsi
	movl	$-1, %r12d
	movq	-56(%rbp), %rdi
	call	*48(%rax)
	testl	%eax, %eax
	je	.L217
	cmpl	$5, -80(%rbp)
	je	.L251
	movl	-76(%rbp), %ebx
	movq	-56(%rbp), %rdi
	movq	%r13, %rsi
	movl	%ebx, %edx
	call	BN_bn2binpad@PLT
	movl	%eax, %r12d
	movl	-80(%rbp), %eax
	cmpl	$3, %eax
	je	.L225
	cmpl	$1, %eax
	jne	.L226
	movq	-88(%rbp), %rdi
	movl	%r12d, %ecx
	movl	%ebx, %r8d
	movq	%r13, %rdx
	movl	%ebx, %esi
	call	RSA_padding_check_PKCS1_type_1@PLT
	movl	%eax, %r12d
.L227:
	testl	%r12d, %r12d
	jns	.L217
	movl	$588, %r8d
	movl	$114, %edx
	movl	$103, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L217:
	movq	%r14, %rdi
	call	BN_CTX_end@PLT
	movq	%r14, %rdi
	call	BN_CTX_free@PLT
	movl	$593, %ecx
	movq	%r15, %rsi
	movq	%r13, %rdi
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
.L212:
	addq	$56, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L247:
	.cfi_restore_state
	movq	32(%rbx), %rdi
	call	BN_num_bits@PLT
	cmpl	$64, %eax
	jle	.L216
	movl	$523, %r8d
.L244:
	movl	$101, %edx
	movl	$103, %esi
	movl	$4, %edi
	movl	$-1, %r12d
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L228:
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	movl	$-1, %r12d
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L248:
	movl	$545, %r8d
	movl	$108, %edx
	movl	$103, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L249:
	movl	$553, %r8d
	movl	$132, %edx
	movl	$103, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L233:
	movl	$536, %r8d
	movl	$65, %edx
	movl	$103, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L250:
	movq	168(%rbx), %rsi
	leaq	120(%rbx), %rdi
	movq	%r14, %rcx
	movq	%r11, %rdx
	call	BN_MONT_CTX_set_locked@PLT
	testq	%rax, %rax
	je	.L217
	movq	24(%rbx), %r11
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L226:
	movl	$584, %r8d
	movl	$118, %edx
	movl	$103, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L251:
	movq	-56(%rbp), %rdi
	call	bn_get_words@PLT
	movq	(%rax), %rax
	andl	$15, %eax
	cmpq	$12, %rax
	je	.L224
	movq	-56(%rbp), %rdx
	movq	24(%rbx), %rsi
	movq	%rdx, %rdi
	call	BN_sub@PLT
	testl	%eax, %eax
	je	.L217
.L224:
	movl	-76(%rbp), %ebx
	movq	-56(%rbp), %rdi
	movq	%r13, %rsi
	movl	%ebx, %edx
	call	BN_bn2binpad@PLT
	movq	-88(%rbp), %rdi
	movl	%ebx, %r8d
	movq	%r13, %rdx
	movl	%eax, %ecx
	movl	%ebx, %esi
	call	RSA_padding_check_X931@PLT
	movl	%eax, %r12d
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L225:
	movq	-88(%rbp), %rdi
	movslq	%r12d, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L245:
	movl	$511, %r8d
	movl	$105, %edx
	movl	$103, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L212
.L246:
	movl	$516, %r8d
	jmp	.L244
	.cfi_endproc
.LFE491:
	.size	rsa_ossl_public_decrypt, .-rsa_ossl_public_decrypt
	.p2align 4
	.type	rsa_ossl_public_encrypt, @function
rsa_ossl_public_encrypt:
.LFB485:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%r8d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$56, %rsp
	movl	%edi, -68(%rbp)
	movq	24(%rcx), %rdi
	movq	%rsi, -80(%rbp)
	movq	%rdx, -96(%rbp)
	call	BN_num_bits@PLT
	cmpl	$16384, %eax
	jg	.L282
	movq	32(%rbx), %rsi
	movq	24(%rbx), %rdi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	jle	.L283
	movq	24(%rbx), %rdi
	call	BN_num_bits@PLT
	cmpl	$3072, %eax
	jg	.L284
.L256:
	call	BN_CTX_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L269
	movq	%rax, %rdi
	call	BN_CTX_start@PLT
	movq	%r15, %rdi
	call	BN_CTX_get@PLT
	movq	%r15, %rdi
	movq	%rax, -88(%rbp)
	call	BN_CTX_get@PLT
	movq	24(%rbx), %rdi
	movq	%rax, -64(%rbp)
	call	BN_num_bits@PLT
	movl	$100, %edx
	leaq	.LC0(%rip), %rsi
	leal	14(%rax), %r13d
	addl	$7, %eax
	cmovns	%eax, %r13d
	sarl	$3, %r13d
	movslq	%r13d, %rcx
	movq	%rcx, %rdi
	movq	%rcx, -56(%rbp)
	call	CRYPTO_malloc@PLT
	cmpq	$0, -64(%rbp)
	movq	%rax, %r14
	je	.L274
	testq	%rax, %rax
	je	.L274
	cmpl	$3, %r12d
	je	.L260
	jg	.L261
	cmpl	$1, %r12d
	je	.L262
	cmpl	$2, %r12d
	jne	.L264
	movl	-68(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movl	%r13d, %esi
	movq	%rax, %rdi
	call	RSA_padding_add_SSLv23@PLT
.L266:
	movl	$-1, %r12d
	testl	%eax, %eax
	jle	.L257
	movq	-88(%rbp), %rdx
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	BN_bin2bn@PLT
	testq	%rax, %rax
	je	.L257
	movq	24(%rbx), %rsi
	movq	-88(%rbp), %rdi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	jns	.L285
	movq	24(%rbx), %r11
	testb	$2, 116(%rbx)
	jne	.L286
.L268:
	movq	32(%rbx), %rdx
	movq	8(%rbx), %rax
	movq	%r15, %r8
	movq	%r11, %rcx
	movq	120(%rbx), %r9
	movq	-64(%rbp), %rbx
	movl	$-1, %r12d
	movq	-88(%rbp), %rsi
	movq	%rbx, %rdi
	call	*48(%rax)
	testl	%eax, %eax
	je	.L257
	movq	-96(%rbp), %rsi
	movl	%r13d, %edx
	movq	%rbx, %rdi
	call	BN_bn2binpad@PLT
	movl	%eax, %r12d
	.p2align 4,,10
	.p2align 3
.L257:
	movq	%r15, %rdi
	call	BN_CTX_end@PLT
	movq	%r15, %rdi
	call	BN_CTX_free@PLT
	movq	-56(%rbp), %rsi
	movl	$153, %ecx
	movq	%r14, %rdi
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
.L252:
	addq	$56, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L261:
	.cfi_restore_state
	cmpl	$4, %r12d
	jne	.L264
	movl	-68(%rbp), %ecx
	movq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	%r13d, %esi
	movq	%rax, %rdi
	call	RSA_padding_add_PKCS1_OAEP@PLT
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L284:
	movq	32(%rbx), %rdi
	call	BN_num_bits@PLT
	cmpl	$64, %eax
	jle	.L256
	movl	$89, %r8d
.L281:
	movl	$101, %edx
	movl	$104, %esi
	movl	$4, %edi
	movl	$-1, %r12d
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L269:
	movq	$0, -56(%rbp)
	xorl	%r14d, %r14d
	movl	$-1, %r12d
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L262:
	movl	-68(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movl	%r13d, %esi
	movq	%rax, %rdi
	call	RSA_padding_add_PKCS1_type_2@PLT
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L264:
	movl	$120, %r8d
	movl	$118, %edx
	movl	$104, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L260:
	movl	-68(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movl	%r13d, %esi
	movq	%rax, %rdi
	call	RSA_padding_add_none@PLT
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L274:
	movl	$102, %r8d
	movl	$65, %edx
	movl	$104, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L285:
	movl	$131, %r8d
	movl	$132, %edx
	movl	$104, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L286:
	movq	168(%rbx), %rsi
	leaq	120(%rbx), %rdi
	movq	%r15, %rcx
	movq	%r11, %rdx
	call	BN_MONT_CTX_set_locked@PLT
	testq	%rax, %rax
	je	.L257
	movq	24(%rbx), %r11
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L282:
	movl	$77, %r8d
	movl	$105, %edx
	movl	$104, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L252
.L283:
	movl	$82, %r8d
	jmp	.L281
	.cfi_endproc
.LFE485:
	.size	rsa_ossl_public_encrypt, .-rsa_ossl_public_encrypt
	.p2align 4
	.type	rsa_ossl_private_encrypt, @function
rsa_ossl_private_encrypt:
.LFB489:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$72, %rsp
	movl	%edi, -88(%rbp)
	movq	%rdx, -96(%rbp)
	movl	%r8d, -68(%rbp)
	call	BN_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L315
	movq	%rax, %rdi
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	BN_CTX_get@PLT
	movq	24(%rbx), %rdi
	movq	%rax, %r13
	call	BN_num_bits@PLT
	leaq	.LC0(%rip), %rsi
	leal	14(%rax), %edx
	addl	$7, %eax
	cmovs	%edx, %eax
	movl	$255, %edx
	sarl	$3, %eax
	movl	%eax, -56(%rbp)
	cltq
	movq	%rax, %rdi
	movq	%rax, -80(%rbp)
	call	CRYPTO_malloc@PLT
	movq	%rax, %r15
	testq	%r13, %r13
	je	.L325
	testq	%rax, %rax
	je	.L325
	movl	-68(%rbp), %eax
	cmpl	$3, %eax
	je	.L291
	cmpl	$5, %eax
	je	.L292
	cmpl	$1, %eax
	je	.L336
	movl	$273, %r8d
	movl	$118, %edx
	movl	$102, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r13d
	call	ERR_put_error@PLT
.L288:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	movq	%r12, %rdi
	call	BN_CTX_free@PLT
	movq	-80(%rbp), %rsi
	movq	%r15, %rdi
	movl	$363, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	addq	$72, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L291:
	.cfi_restore_state
	movl	-88(%rbp), %ecx
	movl	-56(%rbp), %esi
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	RSA_padding_add_none@PLT
.L294:
	testl	%eax, %eax
	jle	.L335
	movq	-64(%rbp), %r14
	movl	-56(%rbp), %esi
	movq	%r15, %rdi
	movq	%r14, %rdx
	call	BN_bin2bn@PLT
	testq	%rax, %rax
	je	.L335
	movq	24(%rbx), %rsi
	movq	%r14, %rdi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	jns	.L337
	movl	116(%rbx), %eax
	testb	$2, %al
	jne	.L338
	movq	$0, -88(%rbp)
	xorl	%r14d, %r14d
	testb	$-128, %al
	je	.L339
.L297:
	testb	$32, %al
	jne	.L305
	cmpl	$1, 4(%rbx)
	je	.L305
	cmpq	$0, 48(%rbx)
	je	.L306
	cmpq	$0, 56(%rbx)
	je	.L306
	cmpq	$0, 64(%rbx)
	je	.L306
	cmpq	$0, 72(%rbx)
	je	.L306
	cmpq	$0, 80(%rbx)
	je	.L306
	.p2align 4,,10
	.p2align 3
.L305:
	movq	8(%rbx), %rax
	movq	-64(%rbp), %rsi
	movq	%r12, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	*40(%rax)
	testl	%eax, %eax
	je	.L335
.L307:
	testq	%r14, %r14
	je	.L314
	movq	-88(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	BN_BLINDING_invert_ex@PLT
	testl	%eax, %eax
	je	.L335
.L314:
	cmpl	$5, -68(%rbp)
	je	.L340
.L313:
	movl	-56(%rbp), %edx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	BN_bn2binpad@PLT
	movl	%eax, %r13d
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L336:
	movl	-88(%rbp), %ecx
	movl	-56(%rbp), %esi
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	RSA_padding_add_PKCS1_type_1@PLT
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L292:
	movl	-88(%rbp), %ecx
	movl	-56(%rbp), %esi
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	RSA_padding_add_X931@PLT
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L340:
	movq	24(%rbx), %rsi
	movq	-64(%rbp), %rbx
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	BN_sub@PLT
	testl	%eax, %eax
	je	.L335
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	BN_cmp@PLT
	testl	%eax, %eax
	cmovg	%rbx, %r13
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L338:
	movq	24(%rbx), %rdx
	movq	168(%rbx), %rsi
	leaq	120(%rbx), %rdi
	movq	%r12, %rcx
	call	BN_MONT_CTX_set_locked@PLT
	testq	%rax, %rax
	je	.L335
	movl	116(%rbx), %eax
	movq	$0, -88(%rbp)
	xorl	%r14d, %r14d
	testb	$-128, %al
	jne	.L297
.L339:
	movq	168(%rbx), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	152(%rbx), %r14
	testq	%r14, %r14
	je	.L341
.L298:
	movq	%r14, %rdi
	call	BN_BLINDING_is_current_thread@PLT
	testl	%eax, %eax
	jne	.L300
	movq	160(%rbx), %r14
	testq	%r14, %r14
	je	.L342
	movq	168(%rbx), %rdi
	call	CRYPTO_THREAD_unlock@PLT
.L302:
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L343
	movq	%r14, %rdi
	call	BN_BLINDING_lock@PLT
	movq	-88(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r12, %rcx
	call	BN_BLINDING_convert_ex@PLT
	movq	%r14, %rdi
	movl	%eax, -104(%rbp)
	call	BN_BLINDING_unlock@PLT
	movl	-104(%rbp), %edx
.L303:
	testl	%edx, %edx
	je	.L335
	movl	116(%rbx), %eax
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L306:
	call	BN_new@PLT
	testq	%rax, %rax
	je	.L344
	movq	40(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L345
	movq	%rax, %rdi
	movl	$4, %edx
	movq	%rax, -104(%rbp)
	call	BN_with_flags@PLT
	movq	-104(%rbp), %r10
	movq	8(%rbx), %rax
	movq	%r13, %rdi
	movq	24(%rbx), %rcx
	movq	120(%rbx), %r9
	movq	%r12, %r8
	movq	%r10, %rdx
	movq	-64(%rbp), %rsi
	call	*48(%rax)
	movq	-104(%rbp), %r10
	testl	%eax, %eax
	movq	%r10, %rdi
	je	.L346
	call	BN_free@PLT
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L300:
	movq	168(%rbx), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movq	-64(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r12, %rcx
	xorl	%esi, %esi
	call	BN_BLINDING_convert_ex@PLT
	movq	$0, -88(%rbp)
	movl	%eax, %edx
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L341:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	RSA_setup_blinding@PLT
	movq	%rax, 152(%rbx)
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L298
	movq	168(%rbx), %rdi
	call	CRYPTO_THREAD_unlock@PLT
.L299:
	movl	$297, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$68, %edx
	movl	$102, %esi
	movl	$4, %edi
	orl	$-1, %r13d
	call	ERR_put_error@PLT
	jmp	.L288
.L325:
	movl	$257, %r8d
.L334:
	movl	$65, %edx
	movl	$102, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L335:
	movl	$-1, %r13d
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L342:
	movq	%rbx, %rdi
	movq	%r12, %rsi
	call	RSA_setup_blinding@PLT
	movq	168(%rbx), %rdi
	movq	%rax, 160(%rbx)
	movq	%rax, %r14
	call	CRYPTO_THREAD_unlock@PLT
	testq	%r14, %r14
	jne	.L302
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L315:
	movq	$0, -80(%rbp)
	xorl	%r15d, %r15d
	movl	$-1, %r13d
	jmp	.L288
.L337:
	movl	$284, %r8d
	movl	$132, %edx
	movl	$102, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r13d
	call	ERR_put_error@PLT
	jmp	.L288
.L346:
	call	BN_free@PLT
	movl	$-1, %r13d
	jmp	.L288
.L345:
	movl	$325, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$179, %edx
	movl	$102, %esi
	movl	$4, %edi
	movq	%rax, -56(%rbp)
	orl	$-1, %r13d
	call	ERR_put_error@PLT
	movq	-56(%rbp), %r10
	movq	%r10, %rdi
	call	BN_free@PLT
	jmp	.L288
.L344:
	movl	$321, %r8d
	jmp	.L334
.L343:
	movl	$304, %r8d
	jmp	.L334
	.cfi_endproc
.LFE489:
	.size	rsa_ossl_private_encrypt, .-rsa_ossl_private_encrypt
	.p2align 4
	.type	rsa_ossl_private_decrypt, @function
rsa_ossl_private_decrypt:
.LFB490:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -72(%rbp)
	movq	%rdx, -88(%rbp)
	movl	%r8d, -64(%rbp)
	call	BN_CTX_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L377
	movq	%rax, %rdi
	call	BN_CTX_start@PLT
	movq	%r13, %rdi
	call	BN_CTX_get@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	BN_CTX_get@PLT
	movq	24(%r12), %rdi
	movq	%rax, -56(%rbp)
	call	BN_num_bits@PLT
	leaq	.LC0(%rip), %rsi
	leal	14(%rax), %edx
	addl	$7, %eax
	cmovs	%edx, %eax
	movl	$389, %edx
	sarl	$3, %eax
	movslq	%eax, %rcx
	movq	%rcx, %rdi
	movl	%ecx, -60(%rbp)
	movq	%rcx, -80(%rbp)
	call	CRYPTO_malloc@PLT
	cmpq	$0, -56(%rbp)
	movq	%rax, %rbx
	je	.L383
	testq	%rax, %rax
	je	.L383
	cmpl	%r15d, -60(%rbp)
	jl	.L394
	movq	-72(%rbp), %rdi
	movq	%r14, %rdx
	movl	%r15d, %esi
	call	BN_bin2bn@PLT
	testq	%rax, %rax
	je	.L393
	movq	24(%r12), %rsi
	movq	%r14, %rdi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	jns	.L395
	movl	116(%r12), %eax
	movq	$0, -72(%rbp)
	xorl	%r15d, %r15d
	testb	$-128, %al
	je	.L396
.L353:
	testb	$32, %al
	je	.L397
.L361:
	movq	8(%r12), %rax
	movq	-56(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	*40(%rax)
	testl	%eax, %eax
	je	.L393
.L363:
	testq	%r15, %r15
	je	.L375
	movq	-72(%rbp), %rsi
	movq	-56(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r15, %rdx
	call	BN_BLINDING_invert_ex@PLT
	testl	%eax, %eax
	je	.L393
.L375:
	movl	-60(%rbp), %edx
	movq	-56(%rbp), %rdi
	movq	%rbx, %rsi
	call	BN_bn2binpad@PLT
	movl	%eax, %r12d
	movl	-64(%rbp), %eax
	cmpl	$3, %eax
	je	.L369
	jg	.L370
	cmpl	$1, %eax
	je	.L371
	cmpl	$2, %eax
	jne	.L373
	movl	-60(%rbp), %esi
	movq	-88(%rbp), %rdi
	movl	%r12d, %ecx
	movq	%rbx, %rdx
	movl	%esi, %r8d
	call	RSA_padding_check_SSLv23@PLT
	movl	%eax, %r12d
.L376:
	movl	$4, %edi
	movl	$491, %r8d
	movl	$114, %edx
	movl	$101, %esi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	%r12d, %edi
	shrl	$31, %edi
	xorl	$1, %edi
	call	err_clear_last_constant_time@PLT
.L348:
	movq	%r13, %rdi
	call	BN_CTX_end@PLT
	movq	%r13, %rdi
	call	BN_CTX_free@PLT
	movq	-80(%rbp), %rsi
	movq	%rbx, %rdi
	movl	$497, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L370:
	.cfi_restore_state
	cmpl	$4, -64(%rbp)
	jne	.L373
	subq	$8, %rsp
	movl	-60(%rbp), %esi
	movq	-88(%rbp), %rdi
	movl	%r12d, %ecx
	pushq	$0
	movq	%rbx, %rdx
	xorl	%r9d, %r9d
	movl	%esi, %r8d
	call	RSA_padding_check_PKCS1_OAEP@PLT
	movl	%eax, %r12d
	popq	%rax
	popq	%rdx
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L397:
	cmpl	$1, 4(%r12)
	je	.L361
	cmpq	$0, 48(%r12)
	je	.L362
	cmpq	$0, 56(%r12)
	je	.L362
	cmpq	$0, 64(%r12)
	je	.L362
	cmpq	$0, 72(%r12)
	je	.L362
	cmpq	$0, 80(%r12)
	jne	.L361
	.p2align 4,,10
	.p2align 3
.L362:
	call	BN_new@PLT
	testq	%rax, %rax
	je	.L398
	movq	40(%r12), %rsi
	testq	%rsi, %rsi
	je	.L399
	movl	$4, %edx
	movq	%rax, %rdi
	movq	%rax, -96(%rbp)
	call	BN_with_flags@PLT
	testb	$2, 116(%r12)
	movq	-96(%rbp), %r10
	jne	.L400
.L366:
	movq	8(%r12), %rax
	movq	%r10, -96(%rbp)
	movq	%r10, %rdx
	movq	%r13, %r8
	movq	-56(%rbp), %rdi
	movq	24(%r12), %rcx
	movq	%r14, %rsi
	movq	120(%r12), %r9
	call	*48(%rax)
	movq	-96(%rbp), %r10
	testl	%eax, %eax
	movq	%r10, %rdi
	je	.L401
	call	BN_free@PLT
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L396:
	movq	168(%r12), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L402
.L354:
	movq	%r15, %rdi
	call	BN_BLINDING_is_current_thread@PLT
	testl	%eax, %eax
	jne	.L356
	movq	160(%r12), %r15
	testq	%r15, %r15
	je	.L403
	movq	168(%r12), %rdi
	call	CRYPTO_THREAD_unlock@PLT
.L358:
	movq	%r13, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	je	.L404
	movq	%r15, %rdi
	call	BN_BLINDING_lock@PLT
	movq	-72(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r13, %rcx
	movq	%r14, %rdi
	call	BN_BLINDING_convert_ex@PLT
	movq	%r15, %rdi
	movl	%eax, -96(%rbp)
	call	BN_BLINDING_unlock@PLT
	movl	-96(%rbp), %edx
.L359:
	testl	%edx, %edx
	je	.L393
	movl	116(%r12), %eax
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L356:
	movq	168(%r12), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movq	%r15, %rdx
	movq	%r13, %rcx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	BN_BLINDING_convert_ex@PLT
	movq	$0, -72(%rbp)
	movl	%eax, %edx
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L400:
	movq	24(%r12), %rdx
	movq	168(%r12), %rsi
	leaq	120(%r12), %rdi
	movq	%r13, %rcx
	call	BN_MONT_CTX_set_locked@PLT
	movq	-96(%rbp), %r10
	testq	%rax, %rax
	jne	.L366
	movq	%r10, %rdi
	orl	$-1, %r12d
	call	BN_free@PLT
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L369:
	movq	-88(%rbp), %rdi
	movslq	%r12d, %rdx
	movq	%rbx, %rsi
	call	memcpy@PLT
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L371:
	movl	-60(%rbp), %esi
	movq	-88(%rbp), %rdi
	movl	%r12d, %ecx
	movq	%rbx, %rdx
	movl	%esi, %r8d
	call	RSA_padding_check_PKCS1_type_2@PLT
	movl	%eax, %r12d
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L373:
	movl	$488, %r8d
	movl	$118, %edx
	movl	$101, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L402:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	RSA_setup_blinding@PLT
	movq	%rax, 152(%r12)
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L354
	movq	168(%r12), %rdi
	call	CRYPTO_THREAD_unlock@PLT
.L355:
	movl	$418, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$68, %edx
	movl	$101, %esi
	movl	$4, %edi
	orl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L348
.L383:
	movl	$391, %r8d
.L392:
	movl	$65, %edx
	movl	$101, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L393:
	movl	$-1, %r12d
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L403:
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	RSA_setup_blinding@PLT
	movq	168(%r12), %rdi
	movq	%rax, 160(%r12)
	movq	%rax, %r15
	call	CRYPTO_THREAD_unlock@PLT
	testq	%r15, %r15
	jne	.L358
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L394:
	movl	$400, %r8d
	movl	$108, %edx
	movl	$101, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L348
.L377:
	movq	$0, -80(%rbp)
	xorl	%ebx, %ebx
	movl	$-1, %r12d
	jmp	.L348
.L395:
	movl	$410, %r8d
	movl	$132, %edx
	movl	$101, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L348
.L401:
	call	BN_free@PLT
	movl	$-1, %r12d
	jmp	.L348
.L399:
	movl	$447, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$179, %edx
	movl	$101, %esi
	movl	$4, %edi
	movq	%rax, -56(%rbp)
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	movq	-56(%rbp), %r10
	movq	%r10, %rdi
	call	BN_free@PLT
	jmp	.L348
.L398:
	movl	$443, %r8d
	jmp	.L392
.L404:
	movl	$425, %r8d
	jmp	.L392
	.cfi_endproc
.LFE490:
	.size	rsa_ossl_private_decrypt, .-rsa_ossl_private_decrypt
	.p2align 4
	.globl	RSA_set_default_method
	.type	RSA_set_default_method, @function
RSA_set_default_method:
.LFB481:
	.cfi_startproc
	endbr64
	movq	%rdi, default_RSA_meth(%rip)
	ret
	.cfi_endproc
.LFE481:
	.size	RSA_set_default_method, .-RSA_set_default_method
	.p2align 4
	.globl	RSA_get_default_method
	.type	RSA_get_default_method, @function
RSA_get_default_method:
.LFB482:
	.cfi_startproc
	endbr64
	movq	default_RSA_meth(%rip), %rax
	ret
	.cfi_endproc
.LFE482:
	.size	RSA_get_default_method, .-RSA_get_default_method
	.p2align 4
	.globl	RSA_PKCS1_OpenSSL
	.type	RSA_PKCS1_OpenSSL, @function
RSA_PKCS1_OpenSSL:
.LFB483:
	.cfi_startproc
	endbr64
	leaq	rsa_pkcs1_ossl_meth(%rip), %rax
	ret
	.cfi_endproc
.LFE483:
	.size	RSA_PKCS1_OpenSSL, .-RSA_PKCS1_OpenSSL
	.p2align 4
	.globl	RSA_null_method
	.type	RSA_null_method, @function
RSA_null_method:
.LFB484:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE484:
	.size	RSA_null_method, .-RSA_null_method
	.section	.data.rel.local,"aw"
	.align 8
	.type	default_RSA_meth, @object
	.size	default_RSA_meth, 8
default_RSA_meth:
	.quad	rsa_pkcs1_ossl_meth
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"OpenSSL PKCS#1 RSA"
	.section	.data.rel,"aw"
	.align 32
	.type	rsa_pkcs1_ossl_meth, @object
	.size	rsa_pkcs1_ossl_meth, 120
rsa_pkcs1_ossl_meth:
	.quad	.LC1
	.quad	rsa_ossl_public_encrypt
	.quad	rsa_ossl_public_decrypt
	.quad	rsa_ossl_private_encrypt
	.quad	rsa_ossl_private_decrypt
	.quad	rsa_ossl_mod_exp
	.quad	BN_mod_exp_mont
	.quad	rsa_ossl_init
	.quad	rsa_ossl_finish
	.long	1024
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
