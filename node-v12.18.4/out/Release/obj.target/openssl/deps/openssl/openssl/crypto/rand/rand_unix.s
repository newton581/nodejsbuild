	.file	"rand_unix.c"
	.text
	.p2align 4
	.type	cleanup_shm, @function
cleanup_shm:
.LFB456:
	.cfi_startproc
	endbr64
	movq	shm_addr(%rip), %rdi
	jmp	shmdt@PLT
	.cfi_endproc
.LFE456:
	.size	cleanup_shm, .-cleanup_shm
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/dev/random"
	.text
	.p2align 4
	.type	wait_random_seeded, @function
wait_random_seeded:
.LFB457:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$544, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	seeded.13883(%rip), %r12d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%r12d, %r12d
	je	.L34
.L3:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L35
	addq	$544, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	xorl	%edx, %edx
	movl	$1, %esi
	movl	$114, %edi
	call	shmget@PLT
	cmpl	$-1, %eax
	je	.L36
.L5:
	movl	$4096, %edx
	xorl	%esi, %esi
	movl	%eax, %edi
	movl	$1, seeded.13883(%rip)
	call	shmat@PLT
	movq	%rax, shm_addr(%rip)
	cmpq	$-1, %rax
	je	.L19
	leaq	cleanup_shm(%rip), %rdi
	call	OPENSSL_atexit@PLT
	movl	seeded.13883(%rip), %r12d
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L38:
	cmpl	$4, %ebx
	jg	.L3
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%esi, %esi
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	open@PLT
	movl	%eax, %r13d
	cmpl	$-1, %eax
	je	.L33
	leaq	-561(%rbp), %rbx
	cmpl	$1023, %eax
	jg	.L14
	leaq	-560(%rbp), %rbx
	xorl	%eax, %eax
	movl	$16, %ecx
	movq	%rbx, %rdi
#APP
# 461 "../deps/openssl/openssl/crypto/rand/rand_unix.c" 1
	cld; rep; stosq
# 0 "" 2
#NO_APP
	movslq	%r13d, %rdi
	leal	1(%r13), %r14d
	call	__fdelt_chk@PLT
	movq	%rax, %r8
	movl	%r13d, %eax
	sarl	$31, %eax
	shrl	$26, %eax
	leal	0(%r13,%rax), %ecx
	andl	$63, %ecx
	subl	%eax, %ecx
	movl	$1, %eax
	salq	%cl, %rax
	orq	%rax, -560(%rbp,%r8,8)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L37:
	call	__errno_location@PLT
	cmpl	$4, (%rax)
	jne	.L17
.L16:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movl	%r14d, %edi
	call	select@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	js	.L37
.L15:
	movl	%r13d, %edi
	call	close@PLT
	cmpl	$1, %r12d
	jne	.L33
	movl	$804, %edx
	movl	$1, %esi
	movl	$114, %edi
	movl	$1, seeded.13883(%rip)
	call	shmget@PLT
	cmpl	$-1, %eax
	jne	.L5
.L19:
	movl	$1, %r12d
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L36:
	leaq	-432(%rbp), %rdi
	call	uname@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L10
	leaq	-302(%rbp), %r13
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	call	strtol@PLT
	movl	$46, %esi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	strchr@PLT
	testq	%rax, %rax
	je	.L38
	xorl	%esi, %esi
	leaq	1(%rax), %rdi
	movl	$10, %edx
	call	strtol@PLT
	cmpl	$4, %ebx
	jg	.L3
	cmpl	$7, %eax
	jle	.L10
	cmpl	$4, %ebx
	jne	.L10
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L39:
	call	__errno_location@PLT
	cmpl	$4, (%rax)
	jne	.L17
.L14:
	movl	$1, %edx
	movq	%rbx, %rsi
	movl	%r13d, %edi
	call	read@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	js	.L39
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L17:
	movl	%r13d, %edi
	call	close@PLT
.L33:
	movl	seeded.13883(%rip), %r12d
	jmp	.L3
.L35:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE457:
	.size	wait_random_seeded, .-wait_random_seeded
	.p2align 4
	.globl	rand_pool_init
	.type	rand_pool_init, @function
rand_pool_init:
.LFB461:
	.cfi_startproc
	endbr64
	movl	$-1, random_devices(%rip)
	movl	$1, %eax
	movl	$-1, 40+random_devices(%rip)
	movl	$-1, 80+random_devices(%rip)
	movl	$-1, 120+random_devices(%rip)
	ret
	.cfi_endproc
.LFE461:
	.size	rand_pool_init, .-rand_pool_init
	.p2align 4
	.globl	rand_pool_cleanup
	.type	rand_pool_cleanup, @function
rand_pool_cleanup:
.LFB462:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$160, %rsp
	movl	random_devices(%rip), %esi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	cmpl	$-1, %esi
	jne	.L74
.L43:
	movl	$-1, random_devices(%rip)
	movl	40+random_devices(%rip), %esi
	cmpl	$-1, %esi
	jne	.L75
.L45:
	movl	$-1, 40+random_devices(%rip)
	movl	80+random_devices(%rip), %esi
	cmpl	$-1, %esi
	jne	.L76
.L47:
	movl	$-1, 80+random_devices(%rip)
	movl	120+random_devices(%rip), %esi
	cmpl	$-1, %esi
	jne	.L77
.L49:
	movl	$-1, 120+random_devices(%rip)
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L78
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	leaq	-160(%rbp), %rdx
	movl	$1, %edi
	call	__fxstat@PLT
	cmpl	$-1, %eax
	je	.L43
	movq	8+random_devices(%rip), %rax
	cmpq	%rax, -160(%rbp)
	jne	.L43
	movq	16+random_devices(%rip), %rax
	cmpq	%rax, -152(%rbp)
	jne	.L43
	movl	24+random_devices(%rip), %eax
	xorl	-136(%rbp), %eax
	testl	$-512, %eax
	jne	.L43
	movq	-120(%rbp), %rax
	cmpq	%rax, 32+random_devices(%rip)
	jne	.L43
	movl	random_devices(%rip), %edi
	call	close@PLT
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L77:
	leaq	-160(%rbp), %rdx
	movl	$1, %edi
	call	__fxstat@PLT
	cmpl	$-1, %eax
	je	.L49
	movq	-160(%rbp), %rax
	cmpq	%rax, 128+random_devices(%rip)
	jne	.L49
	movq	-152(%rbp), %rax
	cmpq	%rax, 136+random_devices(%rip)
	jne	.L49
	movl	144+random_devices(%rip), %eax
	xorl	-136(%rbp), %eax
	testl	$-512, %eax
	jne	.L49
	movq	-120(%rbp), %rax
	cmpq	%rax, 152+random_devices(%rip)
	jne	.L49
	movl	120+random_devices(%rip), %edi
	call	close@PLT
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L76:
	leaq	-160(%rbp), %rdx
	movl	$1, %edi
	call	__fxstat@PLT
	cmpl	$-1, %eax
	je	.L47
	movq	-160(%rbp), %rax
	cmpq	%rax, 88+random_devices(%rip)
	jne	.L47
	movq	-152(%rbp), %rax
	cmpq	%rax, 96+random_devices(%rip)
	jne	.L47
	movl	104+random_devices(%rip), %eax
	xorl	-136(%rbp), %eax
	testl	$-512, %eax
	jne	.L47
	movq	-120(%rbp), %rax
	cmpq	%rax, 112+random_devices(%rip)
	jne	.L47
	movl	80+random_devices(%rip), %edi
	call	close@PLT
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L75:
	leaq	-160(%rbp), %rdx
	movl	$1, %edi
	call	__fxstat@PLT
	cmpl	$-1, %eax
	je	.L45
	movq	-160(%rbp), %rax
	cmpq	%rax, 48+random_devices(%rip)
	jne	.L45
	movq	-152(%rbp), %rax
	cmpq	%rax, 56+random_devices(%rip)
	jne	.L45
	movl	64+random_devices(%rip), %eax
	xorl	-136(%rbp), %eax
	testl	$-512, %eax
	jne	.L45
	movq	-120(%rbp), %rax
	cmpq	%rax, 72+random_devices(%rip)
	jne	.L45
	movl	40+random_devices(%rip), %edi
	call	close@PLT
	jmp	.L45
.L78:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE462:
	.size	rand_pool_cleanup, .-rand_pool_cleanup
	.p2align 4
	.globl	rand_pool_keep_random_devices_open
	.type	rand_pool_keep_random_devices_open, @function
rand_pool_keep_random_devices_open:
.LFB463:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%edi, %ebx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testl	%edi, %edi
	jne	.L80
	movl	random_devices(%rip), %esi
	cmpl	$-1, %esi
	jne	.L113
.L82:
	movl	$-1, random_devices(%rip)
	movl	40+random_devices(%rip), %esi
	cmpl	$-1, %esi
	jne	.L114
.L84:
	movl	$-1, 40+random_devices(%rip)
	movl	80+random_devices(%rip), %esi
	cmpl	$-1, %esi
	jne	.L115
.L86:
	movl	$-1, 80+random_devices(%rip)
	movl	120+random_devices(%rip), %esi
	cmpl	$-1, %esi
	jne	.L116
.L88:
	movl	$-1, 120+random_devices(%rip)
.L80:
	movl	%ebx, keep_random_devices_open(%rip)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L117
	addq	$168, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
	leaq	-176(%rbp), %rdx
	movl	$1, %edi
	call	__fxstat@PLT
	cmpl	$-1, %eax
	je	.L88
	movq	-176(%rbp), %rax
	cmpq	%rax, 128+random_devices(%rip)
	jne	.L88
	movq	-168(%rbp), %rax
	cmpq	%rax, 136+random_devices(%rip)
	jne	.L88
	movl	144+random_devices(%rip), %eax
	xorl	-152(%rbp), %eax
	testl	$-512, %eax
	jne	.L88
	movq	-136(%rbp), %rax
	cmpq	%rax, 152+random_devices(%rip)
	jne	.L88
	movl	120+random_devices(%rip), %edi
	call	close@PLT
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L115:
	leaq	-176(%rbp), %rdx
	movl	$1, %edi
	call	__fxstat@PLT
	cmpl	$-1, %eax
	je	.L86
	movq	-176(%rbp), %rax
	cmpq	%rax, 88+random_devices(%rip)
	jne	.L86
	movq	-168(%rbp), %rax
	cmpq	%rax, 96+random_devices(%rip)
	jne	.L86
	movl	104+random_devices(%rip), %eax
	xorl	-152(%rbp), %eax
	testl	$-512, %eax
	jne	.L86
	movq	-136(%rbp), %rax
	cmpq	%rax, 112+random_devices(%rip)
	jne	.L86
	movl	80+random_devices(%rip), %edi
	call	close@PLT
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L114:
	leaq	-176(%rbp), %rdx
	movl	$1, %edi
	call	__fxstat@PLT
	cmpl	$-1, %eax
	je	.L84
	movq	-176(%rbp), %rax
	cmpq	%rax, 48+random_devices(%rip)
	jne	.L84
	movq	-168(%rbp), %rax
	cmpq	%rax, 56+random_devices(%rip)
	jne	.L84
	movl	64+random_devices(%rip), %eax
	xorl	-152(%rbp), %eax
	testl	$-512, %eax
	jne	.L84
	movq	-136(%rbp), %rax
	cmpq	%rax, 72+random_devices(%rip)
	jne	.L84
	movl	40+random_devices(%rip), %edi
	call	close@PLT
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L113:
	leaq	-176(%rbp), %rdx
	movl	$1, %edi
	call	__fxstat@PLT
	cmpl	$-1, %eax
	je	.L82
	movq	8+random_devices(%rip), %rax
	cmpq	%rax, -176(%rbp)
	jne	.L82
	movq	16+random_devices(%rip), %rax
	cmpq	%rax, -168(%rbp)
	jne	.L82
	movl	-152(%rbp), %eax
	xorl	24+random_devices(%rip), %eax
	testl	$-512, %eax
	jne	.L82
	movq	-136(%rbp), %rax
	cmpq	%rax, 32+random_devices(%rip)
	jne	.L82
	movl	random_devices(%rip), %edi
	call	close@PLT
	jmp	.L82
.L117:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE463:
	.size	rand_pool_keep_random_devices_open, .-rand_pool_keep_random_devices_open
	.p2align 4
	.globl	rand_pool_acquire_entropy
	.type	rand_pool_acquire_entropy, @function
rand_pool_acquire_entropy:
.LFB464:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	rand_pool_bytes_needed@PLT
	testq	%rax, %rax
	je	.L127
	movq	%rax, %r13
	movq	getentropy@GOTPCREL(%rip), %r15
	movl	$2, %eax
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L129:
	call	__errno_location@PLT
	cmpl	$4, (%rax)
	jne	.L127
.L128:
	leal	-1(%r14), %eax
	testl	%r14d, %r14d
	jle	.L127
.L119:
	movl	%eax, %r14d
	testq	%r15, %r15
	je	.L124
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	rand_pool_add_begin@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	getentropy@PLT
	testl	%eax, %eax
	jne	.L129
	testq	%r13, %r13
	jle	.L129
	leaq	0(,%r13,8), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	rand_pool_add_end@PLT
.L127:
	movq	%r12, %rdi
	call	rand_pool_entropy_available@PLT
	movq	%rax, -216(%rbp)
	testq	%rax, %rax
	je	.L185
.L118:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L186
	movq	-216(%rbp), %rax
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	rand_pool_add_begin@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$318, %edi
	movq	%rax, %rsi
	xorl	%eax, %eax
	call	syscall@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	jle	.L123
	leaq	0(,%rax,8), %rdx
	movq	%rax, %rsi
	movq	%r12, %rdi
	movl	$2, %r14d
	call	rand_pool_add_end@PLT
	subq	%rbx, %r13
	jne	.L124
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L123:
	je	.L128
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L185:
	call	wait_random_seeded
	testl	%eax, %eax
	je	.L132
	movl	$1, %esi
	movq	%r12, %rdi
	call	rand_pool_bytes_needed@PLT
	movq	%rax, %rbx
	leaq	random_devices(%rip), %rax
	movq	%rax, -224(%rbp)
	testq	%rbx, %rbx
	je	.L152
	.p2align 4,,10
	.p2align 3
.L150:
	movq	-224(%rbp), %r14
	movl	(%r14), %esi
	cmpl	$-1, %esi
	je	.L133
	leaq	-208(%rbp), %rax
	movl	$1, %edi
	movq	%rax, %rdx
	movq	%rax, -232(%rbp)
	call	__fxstat@PLT
	cmpl	$-1, %eax
	je	.L133
	movq	-208(%rbp), %rax
	cmpq	%rax, 8(%r14)
	je	.L187
.L133:
	movq	-216(%rbp), %rcx
	leaq	random_device_paths(%rip), %rax
	xorl	%esi, %esi
	movq	(%rax,%rcx,8), %rdi
	xorl	%eax, %eax
	call	open@PLT
	movl	%eax, %esi
	movq	-224(%rbp), %rax
	movl	%esi, (%rax)
	cmpl	$-1, %esi
	je	.L138
	leaq	-208(%rbp), %rax
	movl	$1, %edi
	movq	%rax, %rdx
	movq	%rax, -232(%rbp)
	call	__fxstat@PLT
	cmpl	$-1, %eax
	je	.L139
	movq	-224(%rbp), %rcx
	movl	-184(%rbp), %eax
	movdqa	-208(%rbp), %xmm0
	movl	%eax, 24(%rcx)
	movq	-168(%rbp), %rax
	movl	(%rcx), %r14d
	movups	%xmm0, 8(%rcx)
	movq	%rax, 32(%rcx)
.L137:
	cmpl	$-1, %r14d
	je	.L138
	movl	$2, %eax
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L140:
	je	.L144
	call	__errno_location@PLT
	cmpl	$4, (%rax)
	jne	.L142
.L144:
	leal	-1(%r13), %eax
	testl	%r13d, %r13d
	jle	.L188
.L147:
	movl	%eax, %r13d
.L141:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	rand_pool_add_begin@PLT
	movq	%rbx, %rdx
	movl	%r14d, %edi
	movq	%rax, %rsi
	call	read@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jle	.L140
	leaq	0(,%rax,8), %rdx
	movq	%rax, %rsi
	movq	%r12, %rdi
	movl	$2, %r13d
	call	rand_pool_add_end@PLT
	subq	%r15, %rbx
	jne	.L141
.L148:
	movl	keep_random_devices_open(%rip), %eax
	testl	%eax, %eax
	jne	.L143
.L142:
	movq	-224(%rbp), %rax
	movl	(%rax), %esi
	cmpl	$-1, %esi
	je	.L149
	movq	-232(%rbp), %rdx
	movl	$1, %edi
	call	__fxstat@PLT
	cmpl	$-1, %eax
	je	.L149
	movq	-224(%rbp), %rax
	movq	-208(%rbp), %rcx
	cmpq	%rcx, 8(%rax)
	je	.L189
	.p2align 4,,10
	.p2align 3
.L149:
	movq	-224(%rbp), %rax
	movl	$-1, (%rax)
.L143:
	movl	$1, %esi
	movq	%r12, %rdi
	call	rand_pool_bytes_needed@PLT
	movq	%rax, %rbx
.L138:
	addq	$1, -216(%rbp)
	movq	-216(%rbp), %rax
	addq	$40, -224(%rbp)
	testq	%rbx, %rbx
	je	.L152
	cmpq	$3, %rax
	jbe	.L150
.L152:
	movq	%r12, %rdi
	call	rand_pool_entropy_available@PLT
	movq	%rax, -216(%rbp)
	testq	%rax, %rax
	jne	.L118
.L132:
	movq	%r12, %rdi
	call	rand_pool_entropy_available@PLT
	movq	%rax, -216(%rbp)
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L188:
	testq	%r15, %r15
	je	.L148
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L187:
	movq	-224(%rbp), %rax
	movq	-200(%rbp), %rcx
	cmpq	%rcx, 16(%rax)
	jne	.L133
	movq	%rax, %rcx
	movl	24(%rax), %eax
	xorl	-184(%rbp), %eax
	testl	$-512, %eax
	jne	.L133
	movq	-168(%rbp), %rax
	cmpq	%rax, 32(%rcx)
	jne	.L133
	movq	-224(%rbp), %rax
	movl	(%rax), %r14d
	jmp	.L137
.L189:
	movq	-200(%rbp), %rcx
	cmpq	%rcx, 16(%rax)
	jne	.L149
	movq	%rax, %rcx
	movl	24(%rax), %eax
	xorl	-184(%rbp), %eax
	testl	$-512, %eax
	jne	.L149
	movq	-168(%rbp), %rax
	cmpq	%rax, 32(%rcx)
	jne	.L149
	movl	(%rcx), %edi
	call	close@PLT
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L139:
	movq	-224(%rbp), %r14
	movl	(%r14), %edi
	call	close@PLT
	movl	$-1, (%r14)
	jmp	.L138
.L186:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE464:
	.size	rand_pool_acquire_entropy, .-rand_pool_acquire_entropy
	.p2align 4
	.globl	rand_pool_add_nonce_data
	.type	rand_pool_add_nonce_data, @function
rand_pool_add_nonce_data:
.LFB465:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -48(%rbp)
	movq	$0, -32(%rbp)
	call	getpid@PLT
	movl	%eax, -48(%rbp)
	call	CRYPTO_THREAD_get_current_id@PLT
	xorl	%edi, %edi
	movq	%r13, %rsi
	movq	%rax, -40(%rbp)
	call	clock_gettime@PLT
	testl	%eax, %eax
	jne	.L191
.L196:
	movq	-64(%rbp), %rax
	salq	$32, %rax
	addq	-56(%rbp), %rax
.L192:
	xorl	%ecx, %ecx
	movl	$24, %edx
	leaq	-48(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -32(%rbp)
	call	rand_pool_add@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L197
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L191:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	gettimeofday@PLT
	testl	%eax, %eax
	je	.L196
	xorl	%edi, %edi
	call	time@PLT
	jmp	.L192
.L197:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE465:
	.size	rand_pool_add_nonce_data, .-rand_pool_add_nonce_data
	.p2align 4
	.globl	rand_pool_add_additional_data
	.type	rand_pool_add_additional_data, @function
rand_pool_add_additional_data:
.LFB466:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -48(%rbp)
	movq	$0, -32(%rbp)
	call	openssl_get_fork_id@PLT
	movl	%eax, -48(%rbp)
	call	CRYPTO_THREAD_get_current_id@PLT
	movq	%rax, -40(%rbp)
	call	OPENSSL_rdtsc@PLT
	movl	%eax, %eax
	testq	%rax, %rax
	je	.L205
.L199:
	xorl	%ecx, %ecx
	movl	$24, %edx
	leaq	-48(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -32(%rbp)
	call	rand_pool_add@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L206
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	.cfi_restore_state
	leaq	-64(%rbp), %r13
	movl	$7, %edi
	movq	%r13, %rsi
	call	clock_gettime@PLT
	testl	%eax, %eax
	je	.L204
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	gettimeofday@PLT
	testl	%eax, %eax
	jne	.L201
.L204:
	movq	-64(%rbp), %rax
	salq	$32, %rax
	addq	-56(%rbp), %rax
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L201:
	xorl	%edi, %edi
	call	time@PLT
	jmp	.L199
.L206:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE466:
	.size	rand_pool_add_additional_data, .-rand_pool_add_additional_data
	.local	seeded.13883
	.comm	seeded.13883,4,4
	.local	shm_addr
	.comm	shm_addr,8,8
	.data
	.align 4
	.type	keep_random_devices_open, @object
	.size	keep_random_devices_open, 4
keep_random_devices_open:
	.long	1
	.local	random_devices
	.comm	random_devices,160,32
	.section	.rodata.str1.1
.LC1:
	.string	"/dev/urandom"
.LC2:
	.string	"/dev/hwrng"
.LC3:
	.string	"/dev/srandom"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	random_device_paths, @object
	.size	random_device_paths, 32
random_device_paths:
	.quad	.LC1
	.quad	.LC0
	.quad	.LC2
	.quad	.LC3
	.weak	getentropy
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
