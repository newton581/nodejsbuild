	.file	"conf_ssl.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/conf/conf_ssl.c"
	.text
	.p2align 4
	.type	ssl_module_free.part.0, @function
ssl_module_free.part.0:
.LFB305:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	$0, ssl_names_count(%rip)
	je	.L2
	movq	$0, -56(%rbp)
	xorl	%r15d, %r15d
	leaq	.LC0(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L5:
	movq	ssl_names(%rip), %r12
	movl	$49, %edx
	movq	%r13, %rsi
	addq	%r15, %r12
	movq	(%r12), %rdi
	call	CRYPTO_free@PLT
	cmpq	$0, 16(%r12)
	je	.L3
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L4:
	movq	8(%r12), %rdx
	movq	%rbx, %r14
	movq	%r13, %rsi
	addq	$1, %rbx
	salq	$4, %r14
	movq	(%rdx,%r14), %rdi
	movl	$51, %edx
	call	CRYPTO_free@PLT
	movq	8(%r12), %rdx
	movq	%r13, %rsi
	movq	8(%rdx,%r14), %rdi
	movl	$52, %edx
	call	CRYPTO_free@PLT
	cmpq	16(%r12), %rbx
	jb	.L4
.L3:
	movq	8(%r12), %rdi
	movl	$54, %edx
	movq	%r13, %rsi
	addq	$24, %r15
	call	CRYPTO_free@PLT
	addq	$1, -56(%rbp)
	movq	-56(%rbp), %rax
	cmpq	ssl_names_count(%rip), %rax
	jb	.L5
.L2:
	movq	ssl_names(%rip), %rdi
	movl	$56, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, ssl_names(%rip)
	movq	$0, ssl_names_count(%rip)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE305:
	.size	ssl_module_free.part.0, .-ssl_module_free.part.0
	.p2align 4
	.type	ssl_module_free, @function
ssl_module_free:
.LFB299:
	.cfi_startproc
	endbr64
	cmpq	$0, ssl_names(%rip)
	je	.L10
	jmp	ssl_module_free.part.0
	.p2align 4,,10
	.p2align 3
.L10:
	ret
	.cfi_endproc
.LFE299:
	.size	ssl_module_free, .-ssl_module_free
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"section="
.LC2:
	.string	", value="
.LC3:
	.string	"name="
	.text
	.p2align 4
	.type	ssl_module_init, @function
ssl_module_init:
.LFB300:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%rsi, -96(%rbp)
	call	CONF_imodule_get_value@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%rax, %r12
	call	NCONF_get_section@PLT
	movq	%rax, %rdi
	movq	%rax, -80(%rbp)
	movq	%rax, %rbx
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jle	.L45
	movq	-80(%rbp), %rdi
	call	OPENSSL_sk_num@PLT
	cmpq	$0, ssl_names(%rip)
	movslq	%eax, %rbx
	je	.L17
	call	ssl_module_free.part.0
.L17:
	leaq	(%rbx,%rbx,2), %rdi
	movl	$80, %edx
	leaq	.LC0(%rip), %rsi
	salq	$3, %rdi
	call	CRYPTO_zalloc@PLT
	movq	%rax, ssl_names(%rip)
	testq	%rax, %rax
	je	.L44
	movq	%rbx, ssl_names_count(%rip)
	testq	%rbx, %rbx
	je	.L27
	movq	$0, -88(%rbp)
	leaq	.LC0(%rip), %rbx
.L20:
	movq	-88(%rbp), %rsi
	movq	-80(%rbp), %rdi
	leaq	(%rsi,%rsi,2), %rdx
	leaq	(%rax,%rdx,8), %rax
	movq	%rax, -64(%rbp)
	call	OPENSSL_sk_value@PLT
	movq	-96(%rbp), %rdi
	movq	16(%rax), %rsi
	movq	%rax, %r12
	call	NCONF_get_section@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jle	.L46
	movq	8(%r12), %rdi
	movl	$99, %edx
	movq	%rbx, %rsi
	call	CRYPTO_strdup@PLT
	movq	-64(%rbp), %r15
	movq	%rax, (%r15)
	testq	%rax, %rax
	je	.L16
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	movl	$103, %edx
	movq	%rbx, %rsi
	movslq	%eax, %r14
	movq	%r14, %rdi
	movq	%r14, -72(%rbp)
	salq	$4, %rdi
	call	CRYPTO_zalloc@PLT
	movq	%rax, 8(%r15)
	testq	%rax, %rax
	je	.L16
	movq	%r14, 16(%r15)
	xorl	%r15d, %r15d
	testq	%r14, %r14
	jne	.L25
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L47:
	testq	%rax, %rax
	je	.L16
	addq	$1, %r15
	cmpq	%r15, -72(%rbp)
	je	.L29
.L25:
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r15, %rcx
	movl	$46, %esi
	movq	%rax, %r14
	movq	-64(%rbp), %rax
	salq	$4, %rcx
	movq	8(%r14), %rdi
	addq	8(%rax), %rcx
	movq	%rcx, %r12
	movq	%rdi, -56(%rbp)
	call	strchr@PLT
	movq	-56(%rbp), %rdi
	movq	%rbx, %rsi
	testq	%rax, %rax
	leaq	1(%rax), %rdx
	cmovne	%rdx, %rdi
	movl	$118, %edx
	call	CRYPTO_strdup@PLT
	movq	16(%r14), %rdi
	movl	$119, %edx
	movq	%rbx, %rsi
	movq	%rax, (%r12)
	call	CRYPTO_strdup@PLT
	cmpq	$0, (%r12)
	movq	%rax, 8(%r12)
	jne	.L47
.L16:
	cmpq	$0, ssl_names(%rip)
	je	.L44
	call	ssl_module_free.part.0
.L44:
	xorl	%eax, %eax
.L12:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	testq	%rbx, %rbx
	je	.L48
	movl	$74, %r8d
	movl	$119, %edx
	movl	$123, %esi
	movl	$14, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L15:
	movq	%r12, %rdx
	leaq	.LC1(%rip), %rsi
	movl	$2, %edi
	xorl	%eax, %eax
	call	ERR_add_error_data@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L29:
	addq	$1, -88(%rbp)
	movq	-88(%rbp), %rax
	cmpq	%rax, ssl_names_count(%rip)
	jbe	.L27
	movq	ssl_names(%rip), %rax
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L48:
	movl	$72, %r8d
	movl	$120, %edx
	movl	$123, %esi
	movl	$14, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L15
.L27:
	movl	$1, %eax
	jmp	.L12
.L46:
	testq	%r13, %r13
	je	.L49
	movl	$94, %r8d
	movl	$117, %edx
	movl	$123, %esi
	movl	$14, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L23:
	movq	8(%r12), %rdx
	movq	16(%r12), %r8
	xorl	%eax, %eax
	leaq	.LC2(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	movl	$4, %edi
	call	ERR_add_error_data@PLT
	jmp	.L16
.L49:
	movl	$91, %r8d
	movl	$118, %edx
	movl	$123, %esi
	movl	$14, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L23
	.cfi_endproc
.LFE300:
	.size	ssl_module_init, .-ssl_module_init
	.p2align 4
	.globl	conf_ssl_get
	.type	conf_ssl_get, @function
conf_ssl_get:
.LFB301:
	.cfi_startproc
	endbr64
	movq	ssl_names(%rip), %rax
	leaq	(%rdi,%rdi,2), %rcx
	leaq	(%rax,%rcx,8), %rax
	movq	(%rax), %rcx
	movq	%rcx, (%rsi)
	movq	16(%rax), %rcx
	movq	8(%rax), %rax
	movq	%rcx, (%rdx)
	ret
	.cfi_endproc
.LFE301:
	.size	conf_ssl_get, .-conf_ssl_get
	.p2align 4
	.globl	conf_ssl_name_find
	.type	conf_ssl_name_find, @function
conf_ssl_name_find:
.LFB302:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L55
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	ssl_names_count(%rip), %r14
	movq	ssl_names(%rip), %rbx
	testq	%r14, %r14
	je	.L56
	movq	%rdi, %r13
	movq	%rsi, %r15
	xorl	%r12d, %r12d
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L53:
	addq	$1, %r12
	addq	$24, %rbx
	cmpq	%r14, %r12
	je	.L56
.L54:
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L53
	movq	%r12, (%r15)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE302:
	.size	conf_ssl_name_find, .-conf_ssl_name_find
	.p2align 4
	.globl	conf_ssl_get_cmd
	.type	conf_ssl_get_cmd, @function
conf_ssl_get_cmd:
.LFB303:
	.cfi_startproc
	endbr64
	salq	$4, %rsi
	addq	%rsi, %rdi
	movq	(%rdi), %rax
	movq	%rax, (%rdx)
	movq	8(%rdi), %rax
	movq	%rax, (%rcx)
	ret
	.cfi_endproc
.LFE303:
	.size	conf_ssl_get_cmd, .-conf_ssl_get_cmd
	.section	.rodata.str1.1
.LC4:
	.string	"ssl_conf"
	.text
	.p2align 4
	.globl	conf_add_ssl_module
	.type	conf_add_ssl_module, @function
conf_add_ssl_module:
.LFB304:
	.cfi_startproc
	endbr64
	leaq	ssl_module_free(%rip), %rdx
	leaq	ssl_module_init(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	jmp	CONF_module_add@PLT
	.cfi_endproc
.LFE304:
	.size	conf_add_ssl_module, .-conf_add_ssl_module
	.local	ssl_names_count
	.comm	ssl_names_count,8,8
	.local	ssl_names
	.comm	ssl_names,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
