	.file	"x509rset.c"
	.text
	.p2align 4
	.globl	X509_REQ_set_version
	.type	X509_REQ_set_version, @function
X509_REQ_set_version:
.LFB781:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L2
	movl	$1, 16(%rdi)
	movq	24(%rdi), %rdi
	jmp	ASN1_INTEGER_set@PLT
	.p2align 4,,10
	.p2align 3
.L2:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE781:
	.size	X509_REQ_set_version, .-X509_REQ_set_version
	.p2align 4
	.globl	X509_REQ_set_subject_name
	.type	X509_REQ_set_subject_name, @function
X509_REQ_set_subject_name:
.LFB782:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L5
	movl	$1, 16(%rdi)
	addq	$32, %rdi
	jmp	X509_NAME_set@PLT
	.p2align 4,,10
	.p2align 3
.L5:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE782:
	.size	X509_REQ_set_subject_name, .-X509_REQ_set_subject_name
	.p2align 4
	.globl	X509_REQ_set_pubkey
	.type	X509_REQ_set_pubkey, @function
X509_REQ_set_pubkey:
.LFB783:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L7
	movl	$1, 16(%rdi)
	addq	$40, %rdi
	jmp	X509_PUBKEY_set@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE783:
	.size	X509_REQ_set_pubkey, .-X509_REQ_set_pubkey
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
