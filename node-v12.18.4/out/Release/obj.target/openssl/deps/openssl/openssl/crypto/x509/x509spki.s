	.file	"x509spki.c"
	.text
	.p2align 4
	.globl	NETSCAPE_SPKI_set_pubkey
	.type	NETSCAPE_SPKI_set_pubkey, @function
NETSCAPE_SPKI_set_pubkey:
.LFB779:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1
	jmp	X509_PUBKEY_set@PLT
	.p2align 4,,10
	.p2align 3
.L1:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE779:
	.size	NETSCAPE_SPKI_set_pubkey, .-NETSCAPE_SPKI_set_pubkey
	.p2align 4
	.globl	NETSCAPE_SPKI_get_pubkey
	.type	NETSCAPE_SPKI_get_pubkey, @function
NETSCAPE_SPKI_get_pubkey:
.LFB780:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L8
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L8
	movq	(%rax), %rdi
	jmp	X509_PUBKEY_get@PLT
	.p2align 4,,10
	.p2align 3
.L8:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE780:
	.size	NETSCAPE_SPKI_get_pubkey, .-NETSCAPE_SPKI_get_pubkey
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/x509/x509spki.c"
	.text
	.p2align 4
	.globl	NETSCAPE_SPKI_b64_decode
	.type	NETSCAPE_SPKI_b64_decode, @function
NETSCAPE_SPKI_b64_decode:
.LFB781:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	%esi, %r12d
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	jg	.L16
	call	strlen@PLT
	movl	%eax, %r12d
.L16:
	leal	1(%r12), %edi
	movl	$38, %edx
	leaq	.LC0(%rip), %rsi
	movslq	%edi, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L23
	movl	%r12d, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	EVP_DecodeBlock@PLT
	testl	%eax, %eax
	js	.L24
	movslq	%eax, %rdx
	leaq	-48(%rbp), %rsi
	xorl	%edi, %edi
	movq	%r13, -48(%rbp)
	call	d2i_NETSCAPE_SPKI@PLT
	movl	$50, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, %r12
	call	CRYPTO_free@PLT
.L15:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L25
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movl	$44, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$118, %edx
	xorl	%r12d, %r12d
	movl	$129, %esi
	movl	$11, %edi
	call	ERR_put_error@PLT
	movl	$45, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L23:
	movl	$39, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$129, %esi
	movl	$11, %edi
	call	ERR_put_error@PLT
	jmp	.L15
.L25:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE781:
	.size	NETSCAPE_SPKI_b64_decode, .-NETSCAPE_SPKI_b64_decode
	.p2align 4
	.globl	NETSCAPE_SPKI_b64_encode
	.type	NETSCAPE_SPKI_b64_encode, @function
NETSCAPE_SPKI_b64_encode:
.LFB782:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	i2d_NETSCAPE_SPKI@PLT
	movl	$62, %edx
	leaq	.LC0(%rip), %rsi
	movslq	%eax, %rdi
	movq	%rdi, %r14
	call	CRYPTO_malloc@PLT
	leal	(%r14,%r14), %edi
	movl	$63, %edx
	leaq	.LC0(%rip), %rsi
	movslq	%edi, %rdi
	movq	%rax, %r12
	call	CRYPTO_malloc@PLT
	movq	%rax, %r13
	testq	%r12, %r12
	je	.L31
	testq	%rax, %rax
	je	.L31
	leaq	-48(%rbp), %rsi
	movq	%r15, %rdi
	movq	%r12, -48(%rbp)
	call	i2d_NETSCAPE_SPKI@PLT
	movl	%r14d, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	EVP_EncodeBlock@PLT
	movl	$73, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
.L26:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L33
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	movl	$65, %r8d
	movl	$65, %edx
	movl	$130, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$66, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	movl	$67, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L26
.L33:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE782:
	.size	NETSCAPE_SPKI_b64_encode, .-NETSCAPE_SPKI_b64_encode
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
