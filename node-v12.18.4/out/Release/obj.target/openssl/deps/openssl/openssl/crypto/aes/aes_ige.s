	.file	"aes_ige.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/aes/aes_ige.c"
	.align 8
.LC1:
	.string	"assertion failed: in && out && key && ivec"
	.align 8
.LC2:
	.string	"assertion failed: (AES_ENCRYPT == enc) || (AES_DECRYPT == enc)"
	.align 8
.LC3:
	.string	"assertion failed: (length % AES_BLOCK_SIZE) == 0"
	.text
	.p2align 4
	.globl	AES_ige_encrypt
	.type	AES_ige_encrypt, @function
AES_ige_encrypt:
.LFB251:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L1
	movq	%rdi, %r14
	testq	%rdi, %rdi
	je	.L3
	movq	%rsi, %rbx
	testq	%rsi, %rsi
	je	.L3
	movq	%rcx, %r13
	testq	%rcx, %rcx
	je	.L3
	testq	%r8, %r8
	je	.L3
	cmpl	$1, %r9d
	ja	.L33
	movq	%rdx, %r12
	testb	$15, %dl
	jne	.L34
	shrq	$4, %r12
	movq	%r12, -128(%rbp)
	cmpl	$1, %r9d
	je	.L35
	cmpq	%rsi, %rdi
	je	.L15
	movq	%r8, %r9
	movq	%r12, %rdi
	leaq	16(%r8), %rax
	testq	%r12, %r12
	je	.L24
	salq	$4, %rdi
	movq	16(%r8), %rdx
	movq	24(%r8), %rcx
	movq	%r14, %r15
	leaq	(%r14,%rdi), %rax
	leaq	-80(%rbp), %r12
	movq	%rax, -112(%rbp)
	movq	%rsi, %rax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L25:
	movq	%rsi, %r15
.L17:
	movq	%rcx, %xmm1
	movdqu	(%r15), %xmm2
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	%rdx, %xmm0
	movq	%r13, %rdx
	movq	%r9, -104(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -96(%rbp)
	pxor	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	AES_decrypt@PLT
	movq	-96(%rbp), %rax
	movq	-104(%rbp), %r9
	leaq	16(%r15), %rsi
	movq	(%rax), %rdx
	xorq	(%r9), %rdx
	addq	$16, %rax
	movq	-8(%rax), %rcx
	movq	%rdx, -16(%rax)
	xorq	8(%r9), %rcx
	movq	%r15, %r9
	movq	%rcx, -8(%rax)
	cmpq	-112(%rbp), %rsi
	jne	.L25
	movq	-128(%rbp), %r12
	subq	$1, %r12
	salq	$4, %r12
	leaq	(%r14,%r12), %r10
	leaq	(%rbx,%r12), %rax
.L16:
	movdqu	(%r10), %xmm7
	movq	-120(%rbp), %rbx
	movups	%xmm7, (%rbx)
	movdqu	(%rax), %xmm6
	movaps	%xmm6, -96(%rbp)
	movups	%xmm6, 16(%rbx)
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L36
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	cmpq	%rsi, %rdi
	je	.L9
	movq	%r12, %rdi
	leaq	16(%r8), %r12
	testq	%rdi, %rdi
	je	.L22
	salq	$4, %rdi
	movq	(%r8), %rax
	movq	8(%r8), %rdx
	movq	%r14, %r15
	leaq	(%rsi,%rdi), %rcx
	movq	%rcx, -104(%rbp)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L23:
	movq	%rdi, %r15
.L11:
	movdqu	(%r15), %xmm4
	movq	%rax, %xmm0
	movq	%rdx, %xmm3
	movq	%rsi, %rdi
	punpcklqdq	%xmm3, %xmm0
	movq	%r13, %rdx
	movq	%rsi, -96(%rbp)
	pxor	%xmm4, %xmm0
	movups	%xmm0, (%rsi)
	call	AES_encrypt@PLT
	movq	-96(%rbp), %rsi
	leaq	16(%r15), %rdi
	movq	(%rsi), %rax
	xorq	(%r12), %rax
	addq	$16, %rsi
	movq	-8(%rsi), %rdx
	movq	%rax, -16(%rsi)
	xorq	8(%r12), %rdx
	movq	%r15, %r12
	movq	%rdx, -8(%rsi)
	cmpq	-104(%rbp), %rsi
	jne	.L23
	movq	-128(%rbp), %r12
	subq	$1, %r12
	salq	$4, %r12
	addq	%r12, %rbx
	addq	%r14, %r12
.L10:
	movdqu	(%rbx), %xmm6
	movq	-120(%rbp), %rax
	movups	%xmm6, (%rax)
	movdqu	(%r12), %xmm6
	movaps	%xmm6, -96(%rbp)
	movups	%xmm6, 16(%rax)
	jmp	.L1
.L9:
	cmpq	$0, -128(%rbp)
	movdqu	(%r8), %xmm0
	movq	16(%r8), %rdx
	movq	24(%r8), %rcx
	je	.L14
	salq	$4, %r12
	movq	%rcx, %r15
	movq	%rdx, %rax
	leaq	(%rdi,%r12), %rcx
	leaq	-80(%rbp), %r12
	movq	%rcx, -128(%rbp)
	.p2align 4,,10
	.p2align 3
.L13:
	movdqu	(%r14), %xmm6
	movq	%rax, -96(%rbp)
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	(%r14), %rax
	movq	%r12, %rdi
	movq	%r15, -104(%rbp)
	addq	$16, %r14
	pxor	%xmm6, %xmm0
	movq	-8(%r14), %r15
	addq	$16, %rbx
	movq	%rax, -112(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	AES_encrypt@PLT
	movq	-96(%rbp), %xmm0
	movq	-112(%rbp), %rax
	movq	%r15, %rcx
	movhps	-104(%rbp), %xmm0
	pxor	-80(%rbp), %xmm0
	movq	%rax, %rdx
	movups	%xmm0, -16(%rbx)
	movaps	%xmm0, -80(%rbp)
	cmpq	-128(%rbp), %r14
	jne	.L13
.L14:
	movq	-120(%rbp), %rax
	movq	%rdx, 16(%rax)
	movq	%rcx, 24(%rax)
	movups	%xmm0, (%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L15:
	cmpq	$0, -128(%rbp)
	movq	(%r8), %rdx
	movq	8(%r8), %rcx
	movdqu	16(%r8), %xmm0
	je	.L20
	salq	$4, %r12
	movq	%rcx, %r15
	movq	%rdx, %rax
	leaq	(%rdi,%r12), %rcx
	leaq	-80(%rbp), %r12
	movq	%rcx, -128(%rbp)
	.p2align 4,,10
	.p2align 3
.L19:
	movdqu	(%r14), %xmm5
	movq	(%r14), %rdx
	movq	%r12, %rsi
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	addq	$16, %r14
	addq	$16, %rbx
	movaps	%xmm5, -80(%rbp)
	pxor	-80(%rbp), %xmm0
	movq	%rdx, -112(%rbp)
	movq	%r13, %rdx
	movq	%r15, -104(%rbp)
	movq	-72(%rbp), %r15
	movaps	%xmm0, -80(%rbp)
	call	AES_decrypt@PLT
	movq	-96(%rbp), %xmm0
	movq	-112(%rbp), %rax
	movq	%r15, %rcx
	movhps	-104(%rbp), %xmm0
	pxor	-80(%rbp), %xmm0
	movq	%rax, %rdx
	movups	%xmm0, -16(%rbx)
	movaps	%xmm0, -80(%rbp)
	cmpq	%r14, -128(%rbp)
	jne	.L19
.L20:
	movq	-120(%rbp), %rax
	movq	%rdx, (%rax)
	movq	%rcx, 8(%rax)
	movups	%xmm0, 16(%rax)
	jmp	.L1
.L24:
	movq	%r8, %r10
	jmp	.L16
.L22:
	movq	%r8, %rbx
	jmp	.L10
.L36:
	call	__stack_chk_fail@PLT
.L3:
	movl	$47, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	OPENSSL_die@PLT
.L34:
	movl	$49, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	OPENSSL_die@PLT
.L33:
	movl	$48, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE251:
	.size	AES_ige_encrypt, .-AES_ige_encrypt
	.p2align 4
	.globl	AES_bi_ige_encrypt
	.type	AES_bi_ige_encrypt, @function
AES_bi_ige_encrypt:
.LFB252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -120(%rbp)
	movl	16(%rbp), %eax
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	testq	%r15, %r15
	je	.L38
	movq	%rsi, %r14
	testq	%rsi, %rsi
	je	.L38
	testq	%rcx, %rcx
	je	.L38
	movq	%r9, %r12
	testq	%r9, %r9
	je	.L38
	cmpl	$1, %eax
	ja	.L60
	movq	%rdx, %rbx
	testb	$15, %dl
	jne	.L61
	cmpl	$1, %eax
	je	.L62
	addq	%rdx, %r15
	leaq	(%rsi,%rdx), %rax
	leaq	32(%r9), %r13
	movq	%r15, -104(%rbp)
	movq	%rax, -160(%rbp)
	cmpq	$15, %rdx
	jbe	.L37
	movzbl	58(%r12), %esi
	movzbl	54(%r12), %edi
	subq	$16, %rbx
	notq	%rbx
	movzbl	50(%r9), %eax
	movzbl	48(%r9), %r11d
	movb	%sil, -139(%rbp)
	movzbl	59(%r12), %esi
	movq	%rbx, %r14
	movb	%dil, -128(%rbp)
	movzbl	55(%r12), %edi
	andq	$-16, %r14
	movb	%sil, -138(%rbp)
	movzbl	60(%r12), %esi
	addq	%r14, %r15
	movb	%dil, -140(%rbp)
	movzbl	57(%r12), %edi
	movq	%r14, -184(%rbp)
	leaq	-80(%rbp), %r14
	movzbl	49(%r9), %ecx
	movb	%sil, -137(%rbp)
	movzbl	61(%r12), %esi
	movq	%r14, -112(%rbp)
	leaq	-96(%rbp), %r14
	movzbl	51(%r9), %r10d
	movb	%dil, -176(%rbp)
	movzbl	52(%r9), %r9d
	movq	%r15, -168(%rbp)
	movzbl	-176(%rbp), %r15d
	movq	%r14, -152(%rbp)
	movzbl	53(%r12), %r8d
	movb	%al, -141(%rbp)
	movzbl	56(%r12), %edx
	movq	%r12, -176(%rbp)
	movzbl	62(%r12), %edi
	movb	%sil, -136(%rbp)
	movq	-160(%rbp), %rbx
	movzbl	63(%r12), %esi
	.p2align 4,,10
	.p2align 3
.L49:
	subq	$16, -104(%rbp)
	movq	-104(%rbp), %rax
	subq	$16, %rbx
	movq	(%rax), %r14
	movq	8(%rax), %r12
	movq	%r14, %rax
	xorl	%r12d, %edx
	xorl	%r14d, %r11d
	movzbl	%ah, %eax
	movb	%dl, -88(%rbp)
	movq	-120(%rbp), %rdx
	xorl	%eax, %ecx
	movzbl	-141(%rbp), %eax
	movb	%r11b, -96(%rbp)
	movb	%cl, -95(%rbp)
	movq	%r14, %rcx
	shrq	$16, %rcx
	xorl	%ecx, %eax
	movq	%r14, %rcx
	shrq	$24, %rcx
	movb	%al, -94(%rbp)
	xorl	%ecx, %r10d
	movq	%r14, %rcx
	shrq	$32, %rcx
	movb	%r10b, -93(%rbp)
	xorl	%ecx, %r9d
	movq	%r14, %rcx
	shrq	$40, %rcx
	movb	%r9b, -92(%rbp)
	xorl	%ecx, %r8d
	movq	%r14, %rcx
	shrq	$48, %rcx
	movb	%r8b, -91(%rbp)
	movq	%rcx, %rax
	movq	%r14, %rcx
	xorb	-128(%rbp), %al
	shrq	$56, %rcx
	movb	%al, -90(%rbp)
	movq	%rcx, %rax
	xorb	-140(%rbp), %al
	movq	%r12, %rcx
	movb	%al, -89(%rbp)
	movq	%r12, %rax
	shrq	$16, %rcx
	movzbl	%ah, %eax
	xorl	%eax, %r15d
	movq	%rcx, %rax
	movq	%r12, %rcx
	xorb	-139(%rbp), %al
	shrq	$24, %rcx
	movb	%al, -86(%rbp)
	movq	%rcx, %rax
	movq	%r12, %rcx
	xorb	-138(%rbp), %al
	movb	%r15b, -87(%rbp)
	shrq	$32, %rcx
	movb	%al, -85(%rbp)
	movq	%rcx, %rax
	movq	%r12, %rcx
	xorb	-137(%rbp), %al
	shrq	$40, %rcx
	movb	%al, -84(%rbp)
	movq	%rcx, %rax
	movq	%r12, %rcx
	xorb	-136(%rbp), %al
	shrq	$48, %rcx
	movb	%al, -83(%rbp)
	xorl	%ecx, %edi
	movb	%dil, -82(%rbp)
	movq	%r12, %rdi
	shrq	$56, %rdi
	xorl	%edi, %esi
	movq	-152(%rbp), %rdi
	movb	%sil, -81(%rbp)
	movq	%rbx, %rsi
	call	AES_decrypt@PLT
	movzbl	(%rbx), %r11d
	xorb	0(%r13), %r11b
	movzbl	1(%rbx), %ecx
	movzbl	2(%rbx), %eax
	movb	%r11b, (%rbx)
	xorb	1(%r13), %cl
	movzbl	3(%rbx), %r10d
	movzbl	4(%rbx), %r9d
	movzbl	5(%rbx), %r8d
	movb	%cl, 1(%rbx)
	xorb	2(%r13), %al
	movzbl	6(%rbx), %edx
	movb	%al, 2(%rbx)
	xorb	3(%r13), %r10b
	movb	%r10b, 3(%rbx)
	xorb	4(%r13), %r9b
	movb	%r9b, 4(%rbx)
	xorb	5(%r13), %r8b
	movb	%r8b, 5(%rbx)
	xorb	6(%r13), %dl
	movb	%dl, 6(%rbx)
	movzbl	9(%rbx), %r15d
	movb	%dl, -128(%rbp)
	movzbl	7(%rbx), %edx
	xorb	7(%r13), %dl
	movb	%al, -141(%rbp)
	movb	%dl, 7(%rbx)
	movb	%dl, -140(%rbp)
	movzbl	8(%rbx), %edx
	xorb	8(%r13), %dl
	movb	%dl, 8(%rbx)
	xorb	9(%r13), %r15b
	movb	%r15b, 9(%rbx)
	movzbl	10(%rbx), %esi
	xorb	10(%r13), %sil
	movzbl	14(%rbx), %edi
	movb	%sil, 10(%rbx)
	movq	-104(%rbp), %rax
	movb	%sil, -139(%rbp)
	movzbl	11(%rbx), %esi
	xorb	11(%r13), %sil
	movb	%sil, 11(%rbx)
	movb	%sil, -138(%rbp)
	movzbl	12(%rbx), %esi
	xorb	12(%r13), %sil
	movb	%sil, 12(%rbx)
	movb	%sil, -137(%rbp)
	movzbl	13(%rbx), %esi
	xorb	13(%r13), %sil
	movb	%sil, 13(%rbx)
	xorb	14(%r13), %dil
	movb	%sil, -136(%rbp)
	movzbl	15(%rbx), %esi
	movb	%dil, 14(%rbx)
	xorb	15(%r13), %sil
	movb	%sil, 15(%rbx)
	movq	-112(%rbp), %r13
	movq	%r14, -80(%rbp)
	movq	%r12, -72(%rbp)
	cmpq	-168(%rbp), %rax
	jne	.L49
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %rbx
	addq	-160(%rbp), %rbx
	movzbl	20(%r12), %edi
	movzbl	19(%r12), %eax
	movzbl	16(%r12), %r11d
	movzbl	17(%r12), %ecx
	movzbl	18(%r12), %edx
	movzbl	24(%r12), %r15d
	movb	%dil, -104(%rbp)
	movzbl	21(%r12), %edi
	movb	%al, -140(%rbp)
	movzbl	27(%r12), %r10d
	movzbl	28(%r12), %r9d
	movzbl	29(%r12), %r8d
	movzbl	31(%r12), %esi
	movb	%dil, -128(%rbp)
	movzbl	22(%r12), %edi
	movb	%dil, -136(%rbp)
	movzbl	23(%r12), %edi
	movb	%dil, -137(%rbp)
	movzbl	25(%r12), %edi
	movb	%dil, -138(%rbp)
	movzbl	26(%r12), %edi
	movb	%dil, -139(%rbp)
	movzbl	30(%r12), %edi
	.p2align 4,,10
	.p2align 3
.L51:
	movq	(%rbx), %r14
	movq	8(%rbx), %r13
	movq	%r14, %rax
	xorl	%r14d, %r11d
	xorl	%r13d, %r15d
	movzbl	%ah, %eax
	movb	%r11b, -96(%rbp)
	xorl	%eax, %ecx
	movzbl	-140(%rbp), %eax
	movb	%r15b, -88(%rbp)
	movb	%cl, -95(%rbp)
	movq	%r14, %rcx
	shrq	$16, %rcx
	xorl	%ecx, %edx
	movb	%dl, -94(%rbp)
	movq	%r14, %rdx
	shrq	$24, %rdx
	xorl	%edx, %eax
	movq	-120(%rbp), %rdx
	movb	%al, -93(%rbp)
	movq	%r14, %rax
	shrq	$32, %rax
	xorb	-104(%rbp), %al
	movb	%al, -92(%rbp)
	movq	%r14, %rax
	shrq	$40, %rax
	xorb	-128(%rbp), %al
	movb	%al, -91(%rbp)
	movq	%r14, %rax
	shrq	$48, %rax
	xorb	-136(%rbp), %al
	movb	%al, -90(%rbp)
	movq	%r14, %rax
	shrq	$56, %rax
	xorb	-137(%rbp), %al
	movb	%al, -89(%rbp)
	movq	%r13, %rax
	movzbl	%ah, %eax
	xorb	-138(%rbp), %al
	movb	%al, -87(%rbp)
	movq	%r13, %rax
	shrq	$16, %rax
	xorb	-139(%rbp), %al
	movb	%al, -86(%rbp)
	movq	%r13, %rax
	shrq	$24, %rax
	xorl	%eax, %r10d
	movq	%r13, %rax
	shrq	$32, %rax
	movb	%r10b, -85(%rbp)
	xorl	%eax, %r9d
	movq	%r13, %rax
	shrq	$40, %rax
	movb	%r9b, -84(%rbp)
	xorl	%eax, %r8d
	movq	%r13, %rax
	shrq	$48, %rax
	movb	%r8b, -83(%rbp)
	xorl	%eax, %edi
	movq	%r13, %rax
	shrq	$56, %rax
	movb	%dil, -82(%rbp)
	movq	-152(%rbp), %rdi
	xorl	%eax, %esi
	movb	%sil, -81(%rbp)
	movq	%rbx, %rsi
	call	AES_decrypt@PLT
	movzbl	(%rbx), %r11d
	xorb	(%r12), %r11b
	movzbl	1(%rbx), %ecx
	movzbl	2(%rbx), %edx
	movb	%r11b, (%rbx)
	xorb	1(%r12), %cl
	movzbl	3(%rbx), %eax
	movzbl	4(%rbx), %esi
	movzbl	8(%rbx), %r15d
	movb	%cl, 1(%rbx)
	xorb	2(%r12), %dl
	movb	%dl, 2(%rbx)
	xorb	3(%r12), %al
	addq	$16, %rbx
	movb	%al, -13(%rbx)
	xorb	4(%r12), %sil
	movb	%sil, -12(%rbx)
	movb	%sil, -104(%rbp)
	movzbl	-11(%rbx), %esi
	xorb	5(%r12), %sil
	movb	%al, -140(%rbp)
	movb	%sil, -11(%rbx)
	movb	%sil, -128(%rbp)
	movzbl	-10(%rbx), %esi
	xorb	6(%r12), %sil
	movb	%sil, -10(%rbx)
	movb	%sil, -136(%rbp)
	movzbl	-9(%rbx), %esi
	xorb	7(%r12), %sil
	movb	%sil, -9(%rbx)
	xorb	8(%r12), %r15b
	movb	%r15b, -8(%rbx)
	movb	%sil, -137(%rbp)
	movzbl	-7(%rbx), %esi
	xorb	9(%r12), %sil
	movzbl	-5(%rbx), %r10d
	movzbl	-4(%rbx), %r9d
	movzbl	-3(%rbx), %r8d
	movb	%sil, -7(%rbx)
	movb	%sil, -138(%rbp)
	movzbl	-6(%rbx), %esi
	xorb	10(%r12), %sil
	movzbl	-2(%rbx), %edi
	movb	%sil, -6(%rbx)
	xorb	11(%r12), %r10b
	movb	%r10b, -5(%rbx)
	xorb	12(%r12), %r9b
	movb	%r9b, -4(%rbx)
	xorb	13(%r12), %r8b
	movb	%r8b, -3(%rbx)
	xorb	14(%r12), %dil
	movb	%dil, -2(%rbx)
	movb	%sil, -139(%rbp)
	movzbl	-1(%rbx), %esi
	xorb	15(%r12), %sil
	movq	-112(%rbp), %r12
	movq	%r14, -80(%rbp)
	movb	%sil, -1(%rbx)
	movq	%r13, -72(%rbp)
	cmpq	%rbx, -160(%rbp)
	jne	.L51
.L37:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L63
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L62:
	.cfi_restore_state
	leaq	16(%r9), %r13
	cmpq	$15, %rdx
	jbe	.L37
	subq	$16, %rbx
	leaq	-80(%rbp), %rdi
	movq	%r14, -128(%rbp)
	movzbl	(%r9), %esi
	andq	$-16, %rbx
	movq	%rdi, -112(%rbp)
	movq	%r9, %rdx
	leaq	16(%rbx), %rax
	movq	%r14, %rbx
	movq	%r9, -136(%rbp)
	movq	%r13, %r14
	movq	%rax, -104(%rbp)
	addq	%r15, %rax
	movq	%rbx, %r12
	movq	-120(%rbp), %r13
	movq	%rax, %rbx
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L53:
	movq	%rdi, %r12
.L45:
	xorb	(%r15), %sil
	movq	%r12, %rdi
	movb	%sil, (%r12)
	movzbl	1(%r15), %esi
	xorb	1(%rdx), %sil
	movb	%sil, 1(%r12)
	movzbl	2(%r15), %esi
	xorb	2(%rdx), %sil
	movb	%sil, 2(%r12)
	movzbl	3(%r15), %esi
	xorb	3(%rdx), %sil
	movb	%sil, 3(%r12)
	movzbl	4(%r15), %esi
	xorb	4(%rdx), %sil
	movb	%sil, 4(%r12)
	movzbl	5(%r15), %esi
	xorb	5(%rdx), %sil
	movb	%sil, 5(%r12)
	movzbl	6(%r15), %esi
	xorb	6(%rdx), %sil
	movb	%sil, 6(%r12)
	movzbl	7(%r15), %esi
	xorb	7(%rdx), %sil
	movb	%sil, 7(%r12)
	movzbl	8(%r15), %esi
	xorb	8(%rdx), %sil
	movb	%sil, 8(%r12)
	movzbl	9(%r15), %esi
	xorb	9(%rdx), %sil
	movb	%sil, 9(%r12)
	movzbl	10(%r15), %esi
	xorb	10(%rdx), %sil
	movb	%sil, 10(%r12)
	movzbl	11(%r15), %esi
	xorb	11(%rdx), %sil
	movb	%sil, 11(%r12)
	movzbl	12(%r15), %esi
	xorb	12(%rdx), %sil
	movb	%sil, 12(%r12)
	movzbl	13(%r15), %esi
	xorb	13(%rdx), %sil
	movb	%sil, 13(%r12)
	movzbl	14(%r15), %esi
	xorb	14(%rdx), %sil
	movb	%sil, 14(%r12)
	movzbl	15(%r15), %esi
	xorb	15(%rdx), %sil
	movq	%r13, %rdx
	movb	%sil, 15(%r12)
	movq	%r12, %rsi
	call	AES_encrypt@PLT
	movzbl	(%r12), %esi
	xorb	(%r14), %sil
	leaq	16(%r12), %rdi
	movb	%sil, (%r12)
	movzbl	1(%r14), %edx
	xorb	%dl, 1(%r12)
	movzbl	2(%r14), %edx
	xorb	%dl, 2(%r12)
	movzbl	3(%r14), %edx
	xorb	%dl, 3(%r12)
	movzbl	4(%r14), %edx
	xorb	%dl, 4(%r12)
	movzbl	5(%r14), %edx
	xorb	%dl, 5(%r12)
	movzbl	6(%r14), %edx
	xorb	%dl, 6(%r12)
	movzbl	7(%r14), %edx
	xorb	%dl, 7(%r12)
	movzbl	8(%r14), %edx
	xorb	%dl, 8(%r12)
	movzbl	9(%r14), %edx
	xorb	%dl, 9(%r12)
	movzbl	10(%r14), %edx
	xorb	%dl, 10(%r12)
	movzbl	11(%r14), %edx
	xorb	%dl, 11(%r12)
	movzbl	12(%r14), %edx
	xorb	%dl, 12(%r12)
	movzbl	13(%r14), %edx
	xorb	%dl, 13(%r12)
	movzbl	14(%r14), %edx
	xorb	%dl, 14(%r12)
	movzbl	15(%r14), %edx
	xorb	%dl, 15(%r12)
	addq	$16, %r15
	movq	-112(%rbp), %r14
	movq	%r12, %rdx
	movdqu	-16(%r15), %xmm0
	movaps	%xmm0, -80(%rbp)
	cmpq	%rbx, %r15
	jne	.L53
	movq	-136(%rbp), %r12
	movq	-104(%rbp), %rbx
	movq	%r14, %r15
	movq	-128(%rbp), %r14
	movzbl	32(%r12), %edx
	leaq	32(%r12), %rax
	leaq	48(%r12), %r13
	movq	-120(%rbp), %r12
	addq	%r14, %rbx
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L54:
	movq	%rbx, %rax
.L47:
	movdqu	-16(%rbx), %xmm1
	subq	$16, %rbx
	xorb	%dl, (%rbx)
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	movaps	%xmm1, -96(%rbp)
	movzbl	1(%rax), %edx
	xorb	%dl, 1(%rbx)
	movzbl	2(%rax), %edx
	xorb	%dl, 2(%rbx)
	movzbl	3(%rax), %edx
	xorb	%dl, 3(%rbx)
	movzbl	4(%rax), %edx
	xorb	%dl, 4(%rbx)
	movzbl	5(%rax), %edx
	xorb	%dl, 5(%rbx)
	movzbl	6(%rax), %edx
	xorb	%dl, 6(%rbx)
	movzbl	7(%rax), %edx
	xorb	%dl, 7(%rbx)
	movzbl	8(%rax), %edx
	xorb	%dl, 8(%rbx)
	movzbl	9(%rax), %edx
	xorb	%dl, 9(%rbx)
	movzbl	10(%rax), %edx
	xorb	%dl, 10(%rbx)
	movzbl	11(%rax), %edx
	xorb	%dl, 11(%rbx)
	movzbl	12(%rax), %edx
	xorb	%dl, 12(%rbx)
	movzbl	13(%rax), %edx
	xorb	%dl, 13(%rbx)
	movzbl	14(%rax), %edx
	xorb	%dl, 14(%rbx)
	movzbl	15(%rax), %eax
	movq	%r12, %rdx
	xorb	%al, 15(%rbx)
	call	AES_encrypt@PLT
	movzbl	(%rbx), %edx
	xorb	0(%r13), %dl
	movb	%dl, (%rbx)
	movzbl	1(%r13), %eax
	xorb	%al, 1(%rbx)
	movzbl	2(%r13), %eax
	xorb	%al, 2(%rbx)
	movzbl	3(%r13), %eax
	xorb	%al, 3(%rbx)
	movzbl	4(%r13), %eax
	xorb	%al, 4(%rbx)
	movzbl	5(%r13), %eax
	xorb	%al, 5(%rbx)
	movzbl	6(%r13), %eax
	xorb	%al, 6(%rbx)
	movzbl	7(%r13), %eax
	xorb	%al, 7(%rbx)
	movzbl	8(%r13), %eax
	xorb	%al, 8(%rbx)
	movzbl	9(%r13), %eax
	xorb	%al, 9(%rbx)
	movzbl	10(%r13), %eax
	xorb	%al, 10(%rbx)
	movdqa	-96(%rbp), %xmm2
	movzbl	11(%r13), %eax
	xorb	%al, 11(%rbx)
	movzbl	12(%r13), %eax
	xorb	%al, 12(%rbx)
	movzbl	13(%r13), %eax
	xorb	%al, 13(%rbx)
	movzbl	14(%r13), %eax
	xorb	%al, 14(%rbx)
	movzbl	15(%r13), %eax
	movaps	%xmm2, -80(%rbp)
	movq	%r15, %r13
	xorb	%al, 15(%rbx)
	cmpq	%rbx, %r14
	jne	.L54
	jmp	.L37
.L61:
	movl	$185, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	OPENSSL_die@PLT
.L60:
	movl	$184, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	OPENSSL_die@PLT
.L38:
	movl	$183, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	OPENSSL_die@PLT
.L63:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE252:
	.size	AES_bi_ige_encrypt, .-AES_bi_ige_encrypt
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
