	.file	"pmeth_fn.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/evp/pmeth_fn.c"
	.text
	.p2align 4
	.globl	EVP_PKEY_sign_init
	.type	EVP_PKEY_sign_init, @function
EVP_PKEY_sign_init:
.LFB445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	testq	%rdi, %rdi
	je	.L2
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	testq	%rax, %rax
	je	.L2
	cmpq	$0, 72(%rax)
	je	.L2
	movq	64(%rax), %rax
	movl	$8, 32(%rdi)
	testq	%rax, %rax
	je	.L5
	call	*%rax
	testl	%eax, %eax
	jle	.L14
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	movl	$0, 32(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movl	$39, %r8d
	movl	$150, %edx
	movl	$141, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	jmp	.L1
	.cfi_endproc
.LFE445:
	.size	EVP_PKEY_sign_init, .-EVP_PKEY_sign_init
	.p2align 4
	.globl	EVP_PKEY_sign
	.type	EVP_PKEY_sign, @function
EVP_PKEY_sign:
.LFB446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 12, -24
	testq	%rdi, %rdi
	je	.L16
	movq	(%rdi), %rax
	movq	%rdi, %r12
	testq	%rax, %rax
	je	.L16
	movq	72(%rax), %r9
	testq	%r9, %r9
	je	.L16
	cmpl	$8, 32(%rdi)
	jne	.L34
	testb	$2, 4(%rax)
	jne	.L35
	addq	$40, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%r9
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movq	16(%rdi), %rdi
	movq	%r8, -48(%rbp)
	movq	%rcx, -40(%rbp)
	movq	%rdx, -32(%rbp)
	movq	%rsi, -24(%rbp)
	call	EVP_PKEY_size@PLT
	movq	-24(%rbp), %rsi
	movq	-32(%rbp), %rdx
	cltq
	movq	-40(%rbp), %rcx
	movq	-48(%rbp), %r8
	testq	%rax, %rax
	je	.L36
	testq	%rsi, %rsi
	je	.L37
	cmpq	%rax, (%rdx)
	jb	.L23
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	72(%rax), %r9
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%r9
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	movl	$65, %r8d
	movl	$163, %edx
	movl	$140, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L15:
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	movl	$65, %r8d
	movl	$155, %edx
	movl	$140, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L37:
	movq	%rax, (%rdx)
	movl	$1, %eax
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L16:
	movl	$57, %r8d
	movl	$150, %edx
	movl	$140, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	jmp	.L15
.L34:
	movl	$62, %r8d
	movl	$151, %edx
	movl	$140, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L15
	.cfi_endproc
.LFE446:
	.size	EVP_PKEY_sign, .-EVP_PKEY_sign
	.p2align 4
	.globl	EVP_PKEY_verify_init
	.type	EVP_PKEY_verify_init, @function
EVP_PKEY_verify_init:
.LFB447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	testq	%rdi, %rdi
	je	.L39
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	testq	%rax, %rax
	je	.L39
	cmpq	$0, 88(%rax)
	je	.L39
	movq	80(%rax), %rax
	movl	$16, 32(%rdi)
	testq	%rax, %rax
	je	.L42
	call	*%rax
	testl	%eax, %eax
	jle	.L50
.L38:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	movl	$0, 32(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	movl	$73, %r8d
	movl	$150, %edx
	movl	$143, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	jmp	.L38
	.cfi_endproc
.LFE447:
	.size	EVP_PKEY_verify_init, .-EVP_PKEY_verify_init
	.p2align 4
	.globl	EVP_PKEY_verify
	.type	EVP_PKEY_verify, @function
EVP_PKEY_verify:
.LFB448:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rdi, %rdi
	je	.L52
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L52
	movq	88(%rax), %rax
	testq	%rax, %rax
	je	.L52
	cmpl	$16, 32(%rdi)
	jne	.L63
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	movl	$91, %r8d
	movl	$150, %edx
	movl	$142, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L63:
	.cfi_restore_state
	movl	$96, %r8d
	movl	$151, %edx
	movl	$142, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE448:
	.size	EVP_PKEY_verify, .-EVP_PKEY_verify
	.p2align 4
	.globl	EVP_PKEY_verify_recover_init
	.type	EVP_PKEY_verify_recover_init, @function
EVP_PKEY_verify_recover_init:
.LFB449:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	testq	%rdi, %rdi
	je	.L65
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	testq	%rax, %rax
	je	.L65
	cmpq	$0, 104(%rax)
	je	.L65
	movq	96(%rax), %rax
	movl	$32, 32(%rdi)
	testq	%rax, %rax
	je	.L68
	call	*%rax
	testl	%eax, %eax
	jle	.L76
.L64:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	movl	$0, 32(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	movl	$106, %r8d
	movl	$150, %edx
	movl	$145, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	jmp	.L64
	.cfi_endproc
.LFE449:
	.size	EVP_PKEY_verify_recover_init, .-EVP_PKEY_verify_recover_init
	.p2align 4
	.globl	EVP_PKEY_verify_recover
	.type	EVP_PKEY_verify_recover, @function
EVP_PKEY_verify_recover:
.LFB450:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 12, -24
	testq	%rdi, %rdi
	je	.L78
	movq	(%rdi), %rax
	movq	%rdi, %r12
	testq	%rax, %rax
	je	.L78
	movq	104(%rax), %r9
	testq	%r9, %r9
	je	.L78
	cmpl	$32, 32(%rdi)
	jne	.L96
	testb	$2, 4(%rax)
	jne	.L97
	addq	$40, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%r9
	.p2align 4,,10
	.p2align 3
.L97:
	.cfi_restore_state
	movq	16(%rdi), %rdi
	movq	%r8, -48(%rbp)
	movq	%rcx, -40(%rbp)
	movq	%rdx, -32(%rbp)
	movq	%rsi, -24(%rbp)
	call	EVP_PKEY_size@PLT
	movq	-24(%rbp), %rsi
	movq	-32(%rbp), %rdx
	cltq
	movq	-40(%rbp), %rcx
	movq	-48(%rbp), %r8
	testq	%rax, %rax
	je	.L98
	testq	%rsi, %rsi
	je	.L99
	cmpq	%rax, (%rdx)
	jb	.L85
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	104(%rax), %r9
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%r9
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	movl	$132, %r8d
	movl	$163, %edx
	movl	$144, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L77:
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	movl	$132, %r8d
	movl	$155, %edx
	movl	$144, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L99:
	movq	%rax, (%rdx)
	movl	$1, %eax
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L78:
	movl	$124, %r8d
	movl	$150, %edx
	movl	$144, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	jmp	.L77
.L96:
	movl	$129, %r8d
	movl	$151, %edx
	movl	$144, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L77
	.cfi_endproc
.LFE450:
	.size	EVP_PKEY_verify_recover, .-EVP_PKEY_verify_recover
	.p2align 4
	.globl	EVP_PKEY_encrypt_init
	.type	EVP_PKEY_encrypt_init, @function
EVP_PKEY_encrypt_init:
.LFB451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	testq	%rdi, %rdi
	je	.L101
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	testq	%rax, %rax
	je	.L101
	cmpq	$0, 152(%rax)
	je	.L101
	movq	144(%rax), %rax
	movl	$256, 32(%rdi)
	testq	%rax, %rax
	je	.L104
	call	*%rax
	testl	%eax, %eax
	jle	.L112
.L100:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	movl	$0, 32(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	movl	$140, %r8d
	movl	$150, %edx
	movl	$139, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	jmp	.L100
	.cfi_endproc
.LFE451:
	.size	EVP_PKEY_encrypt_init, .-EVP_PKEY_encrypt_init
	.p2align 4
	.globl	EVP_PKEY_encrypt
	.type	EVP_PKEY_encrypt, @function
EVP_PKEY_encrypt:
.LFB452:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 12, -24
	testq	%rdi, %rdi
	je	.L114
	movq	(%rdi), %rax
	movq	%rdi, %r12
	testq	%rax, %rax
	je	.L114
	movq	152(%rax), %r9
	testq	%r9, %r9
	je	.L114
	cmpl	$256, 32(%rdi)
	jne	.L132
	testb	$2, 4(%rax)
	jne	.L133
	addq	$40, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%r9
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore_state
	movq	16(%rdi), %rdi
	movq	%r8, -48(%rbp)
	movq	%rcx, -40(%rbp)
	movq	%rdx, -32(%rbp)
	movq	%rsi, -24(%rbp)
	call	EVP_PKEY_size@PLT
	movq	-24(%rbp), %rsi
	movq	-32(%rbp), %rdx
	cltq
	movq	-40(%rbp), %rcx
	movq	-48(%rbp), %r8
	testq	%rax, %rax
	je	.L134
	testq	%rsi, %rsi
	je	.L135
	cmpq	%rax, (%rdx)
	jb	.L121
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	152(%rax), %r9
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%r9
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	movl	$166, %r8d
	movl	$163, %edx
	movl	$105, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L113:
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	movl	$166, %r8d
	movl	$155, %edx
	movl	$105, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L135:
	movq	%rax, (%rdx)
	movl	$1, %eax
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L114:
	movl	$158, %r8d
	movl	$150, %edx
	movl	$105, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	jmp	.L113
.L132:
	movl	$163, %r8d
	movl	$151, %edx
	movl	$105, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L113
	.cfi_endproc
.LFE452:
	.size	EVP_PKEY_encrypt, .-EVP_PKEY_encrypt
	.p2align 4
	.globl	EVP_PKEY_decrypt_init
	.type	EVP_PKEY_decrypt_init, @function
EVP_PKEY_decrypt_init:
.LFB453:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	testq	%rdi, %rdi
	je	.L137
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	testq	%rax, %rax
	je	.L137
	cmpq	$0, 168(%rax)
	je	.L137
	movq	160(%rax), %rax
	movl	$512, 32(%rdi)
	testq	%rax, %rax
	je	.L140
	call	*%rax
	testl	%eax, %eax
	jle	.L148
.L136:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	.cfi_restore_state
	movl	$0, 32(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
	movl	$174, %r8d
	movl	$150, %edx
	movl	$138, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	jmp	.L136
	.cfi_endproc
.LFE453:
	.size	EVP_PKEY_decrypt_init, .-EVP_PKEY_decrypt_init
	.p2align 4
	.globl	EVP_PKEY_decrypt
	.type	EVP_PKEY_decrypt, @function
EVP_PKEY_decrypt:
.LFB454:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 12, -24
	testq	%rdi, %rdi
	je	.L150
	movq	(%rdi), %rax
	movq	%rdi, %r12
	testq	%rax, %rax
	je	.L150
	movq	168(%rax), %r9
	testq	%r9, %r9
	je	.L150
	cmpl	$512, 32(%rdi)
	jne	.L168
	testb	$2, 4(%rax)
	jne	.L169
	addq	$40, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%r9
	.p2align 4,,10
	.p2align 3
.L169:
	.cfi_restore_state
	movq	16(%rdi), %rdi
	movq	%r8, -48(%rbp)
	movq	%rcx, -40(%rbp)
	movq	%rdx, -32(%rbp)
	movq	%rsi, -24(%rbp)
	call	EVP_PKEY_size@PLT
	movq	-24(%rbp), %rsi
	movq	-32(%rbp), %rdx
	cltq
	movq	-40(%rbp), %rcx
	movq	-48(%rbp), %r8
	testq	%rax, %rax
	je	.L170
	testq	%rsi, %rsi
	je	.L171
	cmpq	%rax, (%rdx)
	jb	.L157
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	168(%rax), %r9
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%r9
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore_state
	movl	$200, %r8d
	movl	$163, %edx
	movl	$104, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L149:
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L157:
	.cfi_restore_state
	movl	$200, %r8d
	movl	$155, %edx
	movl	$104, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L171:
	movq	%rax, (%rdx)
	movl	$1, %eax
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L150:
	movl	$192, %r8d
	movl	$150, %edx
	movl	$104, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	jmp	.L149
.L168:
	movl	$197, %r8d
	movl	$151, %edx
	movl	$104, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L149
	.cfi_endproc
.LFE454:
	.size	EVP_PKEY_decrypt, .-EVP_PKEY_decrypt
	.p2align 4
	.globl	EVP_PKEY_derive_init
	.type	EVP_PKEY_derive_init, @function
EVP_PKEY_derive_init:
.LFB455:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	testq	%rdi, %rdi
	je	.L173
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	testq	%rax, %rax
	je	.L173
	cmpq	$0, 184(%rax)
	je	.L173
	movq	176(%rax), %rax
	movl	$1024, 32(%rdi)
	testq	%rax, %rax
	je	.L176
	call	*%rax
	testl	%eax, %eax
	jle	.L184
.L172:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	movl	$0, 32(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L173:
	.cfi_restore_state
	movl	$208, %r8d
	movl	$150, %edx
	movl	$154, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	jmp	.L172
	.cfi_endproc
.LFE455:
	.size	EVP_PKEY_derive_init, .-EVP_PKEY_derive_init
	.p2align 4
	.globl	EVP_PKEY_derive_set_peer
	.type	EVP_PKEY_derive_set_peer, @function
EVP_PKEY_derive_set_peer:
.LFB456:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	testq	%rdi, %rdi
	je	.L186
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	testq	%rax, %rax
	je	.L186
	cmpq	$0, 184(%rax)
	movq	%rsi, %r12
	je	.L209
.L187:
	movq	192(%rax), %rax
	testq	%rax, %rax
	je	.L186
	movl	32(%rbx), %edx
	leal	-256(%rdx), %ecx
	andl	$-257, %ecx
	je	.L190
	cmpl	$1024, %edx
	jne	.L210
.L190:
	xorl	%edx, %edx
	movq	%r12, %rcx
	movl	$2, %esi
	movq	%rbx, %rdi
	call	*%rax
	testl	%eax, %eax
	jle	.L185
	cmpl	$2, %eax
	je	.L195
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.L211
	movl	(%r12), %ecx
	cmpl	%ecx, (%rax)
	jne	.L212
	movq	%r12, %rdi
	call	EVP_PKEY_missing_parameters@PLT
	testl	%eax, %eax
	jne	.L193
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	EVP_PKEY_cmp_parameters@PLT
	testl	%eax, %eax
	je	.L213
.L193:
	movq	24(%rbx), %rdi
	call	EVP_PKEY_free@PLT
	movq	(%rbx), %rax
	movq	%r12, 24(%rbx)
	movq	%r12, %rcx
	movl	$1, %edx
	movl	$2, %esi
	movq	%rbx, %rdi
	call	*192(%rax)
	testl	%eax, %eax
	jle	.L214
	movq	%r12, %rdi
	call	EVP_PKEY_up_ref@PLT
	movl	$1, %eax
.L185:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L214:
	.cfi_restore_state
	movq	$0, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	.cfi_restore_state
	cmpq	$0, 152(%rax)
	jne	.L187
	cmpq	$0, 168(%rax)
	jne	.L187
	.p2align 4,,10
	.p2align 3
.L186:
	movl	$227, %r8d
	movl	$150, %edx
	movl	$155, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L195:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L210:
	.cfi_restore_state
	movl	$234, %r8d
	movl	$151, %edx
	movl	$155, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L185
.L213:
	movl	$266, %r8d
	movl	$153, %edx
	movl	$155, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L212:
	movl	$253, %r8d
	movl	$101, %edx
	movl	$155, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L185
.L211:
	movl	$248, %r8d
	movl	$154, %edx
	movl	$155, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L185
	.cfi_endproc
.LFE456:
	.size	EVP_PKEY_derive_set_peer, .-EVP_PKEY_derive_set_peer
	.p2align 4
	.globl	EVP_PKEY_derive
	.type	EVP_PKEY_derive, @function
EVP_PKEY_derive:
.LFB457:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	testq	%rdi, %rdi
	je	.L216
	movq	(%rdi), %rax
	movq	%rdi, %r12
	testq	%rax, %rax
	je	.L216
	movq	184(%rax), %rcx
	testq	%rcx, %rcx
	je	.L216
	cmpl	$1024, 32(%rdi)
	jne	.L234
	testb	$2, 4(%rax)
	jne	.L235
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rcx
	.p2align 4,,10
	.p2align 3
.L235:
	.cfi_restore_state
	movq	16(%rdi), %rdi
	movq	%rdx, -32(%rbp)
	movq	%rsi, -24(%rbp)
	call	EVP_PKEY_size@PLT
	movq	-24(%rbp), %rsi
	movq	-32(%rbp), %rdx
	cltq
	testq	%rax, %rax
	je	.L236
	testq	%rsi, %rsi
	je	.L237
	cmpq	%rax, (%rdx)
	jb	.L223
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	184(%rax), %rcx
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rcx
	.p2align 4,,10
	.p2align 3
.L236:
	.cfi_restore_state
	movl	$295, %r8d
	movl	$163, %edx
	movl	$153, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L215:
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	.cfi_restore_state
	movl	$295, %r8d
	movl	$155, %edx
	movl	$153, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L237:
	movq	%rax, (%rdx)
	movl	$1, %eax
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L216:
	movl	$287, %r8d
	movl	$150, %edx
	movl	$153, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	jmp	.L215
.L234:
	movl	$292, %r8d
	movl	$151, %edx
	movl	$153, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L215
	.cfi_endproc
.LFE457:
	.size	EVP_PKEY_derive, .-EVP_PKEY_derive
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
