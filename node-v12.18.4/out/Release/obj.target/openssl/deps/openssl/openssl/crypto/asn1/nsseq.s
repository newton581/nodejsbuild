	.file	"nsseq.c"
	.text
	.p2align 4
	.type	nsseq_cb, @function
nsseq_cb:
.LFB742:
	.cfi_startproc
	endbr64
	cmpl	$1, %edi
	je	.L8
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$79, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rsi), %rbx
	call	OBJ_nid2obj@PLT
	movq	%rax, (%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE742:
	.size	nsseq_cb, .-nsseq_cb
	.p2align 4
	.globl	d2i_NETSCAPE_CERT_SEQUENCE
	.type	d2i_NETSCAPE_CERT_SEQUENCE, @function
d2i_NETSCAPE_CERT_SEQUENCE:
.LFB743:
	.cfi_startproc
	endbr64
	leaq	NETSCAPE_CERT_SEQUENCE_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE743:
	.size	d2i_NETSCAPE_CERT_SEQUENCE, .-d2i_NETSCAPE_CERT_SEQUENCE
	.p2align 4
	.globl	i2d_NETSCAPE_CERT_SEQUENCE
	.type	i2d_NETSCAPE_CERT_SEQUENCE, @function
i2d_NETSCAPE_CERT_SEQUENCE:
.LFB744:
	.cfi_startproc
	endbr64
	leaq	NETSCAPE_CERT_SEQUENCE_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE744:
	.size	i2d_NETSCAPE_CERT_SEQUENCE, .-i2d_NETSCAPE_CERT_SEQUENCE
	.p2align 4
	.globl	NETSCAPE_CERT_SEQUENCE_new
	.type	NETSCAPE_CERT_SEQUENCE_new, @function
NETSCAPE_CERT_SEQUENCE_new:
.LFB745:
	.cfi_startproc
	endbr64
	leaq	NETSCAPE_CERT_SEQUENCE_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE745:
	.size	NETSCAPE_CERT_SEQUENCE_new, .-NETSCAPE_CERT_SEQUENCE_new
	.p2align 4
	.globl	NETSCAPE_CERT_SEQUENCE_free
	.type	NETSCAPE_CERT_SEQUENCE_free, @function
NETSCAPE_CERT_SEQUENCE_free:
.LFB746:
	.cfi_startproc
	endbr64
	leaq	NETSCAPE_CERT_SEQUENCE_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE746:
	.size	NETSCAPE_CERT_SEQUENCE_free, .-NETSCAPE_CERT_SEQUENCE_free
	.globl	NETSCAPE_CERT_SEQUENCE_it
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"NETSCAPE_CERT_SEQUENCE"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	NETSCAPE_CERT_SEQUENCE_it, @object
	.size	NETSCAPE_CERT_SEQUENCE_it, 56
NETSCAPE_CERT_SEQUENCE_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	NETSCAPE_CERT_SEQUENCE_seq_tt
	.quad	2
	.quad	NETSCAPE_CERT_SEQUENCE_aux
	.quad	16
	.quad	.LC0
	.section	.rodata.str1.1
.LC1:
	.string	"type"
.LC2:
	.string	"certs"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	NETSCAPE_CERT_SEQUENCE_seq_tt, @object
	.size	NETSCAPE_CERT_SEQUENCE_seq_tt, 80
NETSCAPE_CERT_SEQUENCE_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC1
	.quad	ASN1_OBJECT_it
	.quad	149
	.quad	0
	.quad	8
	.quad	.LC2
	.quad	X509_it
	.section	.data.rel.ro.local
	.align 32
	.type	NETSCAPE_CERT_SEQUENCE_aux, @object
	.size	NETSCAPE_CERT_SEQUENCE_aux, 40
NETSCAPE_CERT_SEQUENCE_aux:
	.quad	0
	.long	0
	.long	0
	.long	0
	.zero	4
	.quad	nsseq_cb
	.long	0
	.zero	4
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
