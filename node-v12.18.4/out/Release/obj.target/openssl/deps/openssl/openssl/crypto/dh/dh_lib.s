	.file	"dh_lib.c"
	.text
	.p2align 4
	.globl	DH_set_method
	.type	DH_set_method, @function
DH_set_method:
.LFB805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	120(%rdi), %rax
	movq	%rsi, %rbx
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L2
	call	*%rax
.L2:
	movq	128(%r12), %rdi
	call	ENGINE_finish@PLT
	movq	32(%rbx), %rax
	movq	%rbx, 120(%r12)
	movq	$0, 128(%r12)
	testq	%rax, %rax
	je	.L3
	movq	%r12, %rdi
	call	*%rax
.L3:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE805:
	.size	DH_set_method, .-DH_set_method
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/dh/dh_lib.c"
	.text
	.p2align 4
	.globl	DH_free
	.type	DH_free, @function
DH_free:
.LFB808:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L26
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	lock xaddl	%eax, 104(%rdi)
	subl	$1, %eax
	testl	%eax, %eax
	je	.L29
	jle	.L16
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
.L16:
	movq	120(%r12), %rax
	testq	%rax, %rax
	je	.L18
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L18
	movq	%r12, %rdi
	call	*%rax
.L18:
	movq	128(%r12), %rdi
	call	ENGINE_finish@PLT
	leaq	112(%r12), %rdx
	movq	%r12, %rsi
	movl	$6, %edi
	call	CRYPTO_free_ex_data@PLT
	movq	136(%r12), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	movq	8(%r12), %rdi
	call	BN_clear_free@PLT
	movq	16(%r12), %rdi
	call	BN_clear_free@PLT
	movq	64(%r12), %rdi
	call	BN_clear_free@PLT
	movq	72(%r12), %rdi
	call	BN_clear_free@PLT
	movq	80(%r12), %rdi
	movl	$123, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	96(%r12), %rdi
	call	BN_clear_free@PLT
	movq	32(%r12), %rdi
	call	BN_clear_free@PLT
	movq	40(%r12), %rdi
	call	BN_clear_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$127, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L26:
	ret
	.cfi_endproc
.LFE808:
	.size	DH_free, .-DH_free
	.p2align 4
	.globl	DH_new_method
	.type	DH_new_method, @function
DH_new_method:
.LFB807:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$44, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$144, %edi
	pushq	%r12
	.cfi_offset 12, -32
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L48
	movl	$1, 104(%rax)
	mfence
	call	CRYPTO_THREAD_lock_new@PLT
	movq	%rax, 136(%r12)
	testq	%rax, %rax
	je	.L49
	call	DH_get_default_method@PLT
	movq	%rax, 120(%r12)
	movl	48(%rax), %eax
	movl	%eax, 48(%r12)
	testq	%r13, %r13
	je	.L34
	movq	%r13, %rdi
	call	ENGINE_init@PLT
	movl	$64, %r8d
	testl	%eax, %eax
	je	.L47
	movq	%r13, 128(%r12)
.L37:
	movq	%r13, %rdi
	call	ENGINE_get_DH@PLT
	movq	%rax, 120(%r12)
	testq	%rax, %rax
	je	.L50
.L39:
	movl	48(%rax), %eax
	leaq	112(%r12), %rdx
	movq	%r12, %rsi
	movl	$6, %edi
	movl	%eax, 48(%r12)
	call	CRYPTO_new_ex_data@PLT
	testl	%eax, %eax
	je	.L36
	movq	120(%r12), %rax
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.L30
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L51
.L30:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	movl	$73, %r8d
.L47:
	movl	$38, %edx
	movl	$105, %esi
	movl	$5, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L36:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	DH_free
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	call	ENGINE_get_default_DH@PLT
	movq	%rax, 128(%r12)
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L37
	movq	120(%r12), %rax
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L48:
	movl	$47, %r8d
	movl	$65, %edx
	movl	$105, %esi
	movl	$5, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L49:
	movl	$54, %r8d
	movl	$65, %edx
	movl	$105, %esi
	movl	$5, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movl	$55, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L51:
	movl	$85, %r8d
	movl	$70, %edx
	movl	$105, %esi
	movl	$5, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L36
	.cfi_endproc
.LFE807:
	.size	DH_new_method, .-DH_new_method
	.p2align 4
	.globl	DH_new
	.type	DH_new, @function
DH_new:
.LFB806:
	.cfi_startproc
	endbr64
	xorl	%edi, %edi
	jmp	DH_new_method
	.cfi_endproc
.LFE806:
	.size	DH_new, .-DH_new
	.p2align 4
	.globl	DH_up_ref
	.type	DH_up_ref, @function
DH_up_ref:
.LFB809:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	lock xaddl	%eax, 104(%rdi)
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE809:
	.size	DH_up_ref, .-DH_up_ref
	.p2align 4
	.globl	DH_set_ex_data
	.type	DH_set_ex_data, @function
DH_set_ex_data:
.LFB810:
	.cfi_startproc
	endbr64
	addq	$112, %rdi
	jmp	CRYPTO_set_ex_data@PLT
	.cfi_endproc
.LFE810:
	.size	DH_set_ex_data, .-DH_set_ex_data
	.p2align 4
	.globl	DH_get_ex_data
	.type	DH_get_ex_data, @function
DH_get_ex_data:
.LFB811:
	.cfi_startproc
	endbr64
	addq	$112, %rdi
	jmp	CRYPTO_get_ex_data@PLT
	.cfi_endproc
.LFE811:
	.size	DH_get_ex_data, .-DH_get_ex_data
	.p2align 4
	.globl	DH_bits
	.type	DH_bits, @function
DH_bits:
.LFB812:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	jmp	BN_num_bits@PLT
	.cfi_endproc
.LFE812:
	.size	DH_bits, .-DH_bits
	.p2align 4
	.globl	DH_size
	.type	DH_size, @function
DH_size:
.LFB813:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	8(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	BN_num_bits@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	leal	14(%rax), %edx
	addl	$7, %eax
	cmovs	%edx, %eax
	sarl	$3, %eax
	ret
	.cfi_endproc
.LFE813:
	.size	DH_size, .-DH_size
	.p2align 4
	.globl	DH_security_bits
	.type	DH_security_bits, @function
DH_security_bits:
.LFB814:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L60
	call	BN_num_bits@PLT
	movl	%eax, %r12d
.L61:
	movq	8(%rbx), %rdi
	call	BN_num_bits@PLT
	popq	%rbx
	movl	%r12d, %esi
	popq	%r12
	movl	%eax, %edi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BN_security_bits@PLT
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	movl	24(%rbx), %r12d
	movl	$-1, %eax
	testl	%r12d, %r12d
	cmove	%eax, %r12d
	jmp	.L61
	.cfi_endproc
.LFE814:
	.size	DH_security_bits, .-DH_security_bits
	.p2align 4
	.globl	DH_get0_pqg
	.type	DH_get0_pqg, @function
DH_get0_pqg:
.LFB815:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L65
	movq	8(%rdi), %rax
	movq	%rax, (%rsi)
.L65:
	testq	%rdx, %rdx
	je	.L66
	movq	64(%rdi), %rax
	movq	%rax, (%rdx)
.L66:
	testq	%rcx, %rcx
	je	.L64
	movq	16(%rdi), %rax
	movq	%rax, (%rcx)
.L64:
	ret
	.cfi_endproc
.LFE815:
	.size	DH_get0_pqg, .-DH_get0_pqg
	.p2align 4
	.globl	DH_set0_pqg
	.type	DH_set0_pqg, @function
DH_set0_pqg:
.LFB816:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L98
	cmpq	$0, 16(%rbx)
	je	.L99
.L81:
	testq	%r14, %r14
	jne	.L87
	testq	%r12, %r12
	je	.L83
.L100:
	movq	64(%rbx), %rdi
	call	BN_free@PLT
	movq	%r12, 64(%rbx)
	testq	%r13, %r13
	je	.L88
	movq	16(%rbx), %rdi
	call	BN_free@PLT
	movq	%r13, 16(%rbx)
.L88:
	movq	%r12, %rdi
	call	BN_num_bits@PLT
	movl	%eax, 24(%rbx)
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L97
	movq	16(%rbx), %rdi
	call	BN_free@PLT
	movq	%r13, 16(%rbx)
.L97:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L91
	cmpq	$0, 16(%rbx)
	jne	.L87
	testq	%rcx, %rcx
	je	.L91
	.p2align 4,,10
	.p2align 3
.L87:
	call	BN_free@PLT
	movq	%r14, 8(%rbx)
	testq	%r12, %r12
	jne	.L100
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L99:
	testq	%rcx, %rcx
	jne	.L81
.L91:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE816:
	.size	DH_set0_pqg, .-DH_set0_pqg
	.p2align 4
	.globl	DH_get_length
	.type	DH_get_length, @function
DH_get_length:
.LFB817:
	.cfi_startproc
	endbr64
	movslq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE817:
	.size	DH_get_length, .-DH_get_length
	.p2align 4
	.globl	DH_set_length
	.type	DH_set_length, @function
DH_set_length:
.LFB818:
	.cfi_startproc
	endbr64
	movl	%esi, 24(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE818:
	.size	DH_set_length, .-DH_set_length
	.p2align 4
	.globl	DH_get0_key
	.type	DH_get0_key, @function
DH_get0_key:
.LFB819:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L104
	movq	32(%rdi), %rax
	movq	%rax, (%rsi)
.L104:
	testq	%rdx, %rdx
	je	.L103
	movq	40(%rdi), %rax
	movq	%rax, (%rdx)
.L103:
	ret
	.cfi_endproc
.LFE819:
	.size	DH_get0_key, .-DH_get0_key
	.p2align 4
	.globl	DH_set0_key
	.type	DH_set0_key, @function
DH_set0_key:
.LFB820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L113
	movq	32(%rdi), %rdi
	movq	%rsi, %r13
	call	BN_clear_free@PLT
	movq	%r13, 32(%rbx)
.L113:
	testq	%r12, %r12
	je	.L114
	movq	40(%rbx), %rdi
	call	BN_clear_free@PLT
	movq	%r12, 40(%rbx)
.L114:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE820:
	.size	DH_set0_key, .-DH_set0_key
	.p2align 4
	.globl	DH_get0_p
	.type	DH_get0_p, @function
DH_get0_p:
.LFB821:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE821:
	.size	DH_get0_p, .-DH_get0_p
	.p2align 4
	.globl	DH_get0_q
	.type	DH_get0_q, @function
DH_get0_q:
.LFB822:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	ret
	.cfi_endproc
.LFE822:
	.size	DH_get0_q, .-DH_get0_q
	.p2align 4
	.globl	DH_get0_g
	.type	DH_get0_g, @function
DH_get0_g:
.LFB823:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE823:
	.size	DH_get0_g, .-DH_get0_g
	.p2align 4
	.globl	DH_get0_priv_key
	.type	DH_get0_priv_key, @function
DH_get0_priv_key:
.LFB824:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE824:
	.size	DH_get0_priv_key, .-DH_get0_priv_key
	.p2align 4
	.globl	DH_get0_pub_key
	.type	DH_get0_pub_key, @function
DH_get0_pub_key:
.LFB825:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE825:
	.size	DH_get0_pub_key, .-DH_get0_pub_key
	.p2align 4
	.globl	DH_clear_flags
	.type	DH_clear_flags, @function
DH_clear_flags:
.LFB826:
	.cfi_startproc
	endbr64
	notl	%esi
	andl	%esi, 48(%rdi)
	ret
	.cfi_endproc
.LFE826:
	.size	DH_clear_flags, .-DH_clear_flags
	.p2align 4
	.globl	DH_test_flags
	.type	DH_test_flags, @function
DH_test_flags:
.LFB827:
	.cfi_startproc
	endbr64
	movl	48(%rdi), %eax
	andl	%esi, %eax
	ret
	.cfi_endproc
.LFE827:
	.size	DH_test_flags, .-DH_test_flags
	.p2align 4
	.globl	DH_set_flags
	.type	DH_set_flags, @function
DH_set_flags:
.LFB828:
	.cfi_startproc
	endbr64
	orl	%esi, 48(%rdi)
	ret
	.cfi_endproc
.LFE828:
	.size	DH_set_flags, .-DH_set_flags
	.p2align 4
	.globl	DH_get0_engine
	.type	DH_get0_engine, @function
DH_get0_engine:
.LFB829:
	.cfi_startproc
	endbr64
	movq	128(%rdi), %rax
	ret
	.cfi_endproc
.LFE829:
	.size	DH_get0_engine, .-DH_get0_engine
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
