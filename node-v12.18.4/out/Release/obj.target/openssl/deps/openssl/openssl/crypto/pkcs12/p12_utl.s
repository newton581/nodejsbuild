	.file	"p12_utl.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/pkcs12/p12_utl.c"
	.text
	.p2align 4
	.globl	OPENSSL_asc2uni
	.type	OPENSSL_asc2uni, @function
OPENSSL_asc2uni:
.LFB803:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	cmpl	$-1, %esi
	jne	.L2
	call	strlen@PLT
	movl	%eax, %esi
.L2:
	leal	2(%rsi,%rsi), %r14d
	movl	$25, %edx
	leaq	.LC0(%rip), %rsi
	movslq	%r14d, %r15
	movq	%r15, %rdi
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L3
	leal	-2(%r14), %esi
	xorl	%edx, %edx
	testl	%esi, %esi
	jle	.L5
	.p2align 4,,10
	.p2align 3
.L4:
	movl	%edx, %ecx
	movb	$0, (%rax,%rdx)
	sarl	%ecx
	movslq	%ecx, %rcx
	movzbl	0(%r13,%rcx), %ecx
	movb	%cl, 1(%rax,%rdx)
	addq	$2, %rdx
	cmpl	%edx, %esi
	jg	.L4
.L5:
	movb	$0, -2(%rax,%r15)
	movb	$0, -1(%rax,%r15)
	testq	%r12, %r12
	je	.L7
	movl	%r14d, (%r12)
.L7:
	testq	%rbx, %rbx
	je	.L1
	movq	%rax, (%rbx)
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3:
	.cfi_restore_state
	movl	$26, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$121, %esi
	movl	$35, %edi
	movq	%rax, -56(%rbp)
	call	ERR_put_error@PLT
	movq	-56(%rbp), %rax
	jmp	.L1
	.cfi_endproc
.LFE803:
	.size	OPENSSL_asc2uni, .-OPENSSL_asc2uni
	.p2align 4
	.globl	OPENSSL_uni2asc
	.type	OPENSSL_uni2asc, @function
OPENSSL_uni2asc:
.LFB804:
	.cfi_startproc
	endbr64
	testb	$1, %sil
	jne	.L26
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %eax
	shrl	$31, %eax
	addl	%esi, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	sarl	%eax
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%eax, %r13d
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$24, %rsp
	testl	%esi, %esi
	jne	.L37
.L21:
	leal	1(%rax), %r13d
.L22:
	movslq	%r13d, %r13
	movl	$55, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L23
	xorl	%edx, %edx
	testl	%ebx, %ebx
	jle	.L25
	.p2align 4,,10
	.p2align 3
.L24:
	movl	%edx, %ecx
	movzbl	1(%r12,%rdx), %esi
	addq	$2, %rdx
	sarl	%ecx
	movslq	%ecx, %rcx
	movb	%sil, (%rax,%rcx)
	cmpl	%edx, %ebx
	jg	.L24
.L25:
	movb	$0, -1(%rax,%r13)
.L19:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	movslq	%esi, %rdx
	cmpb	$0, -1(%rdi,%rdx)
	je	.L22
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
.L23:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	$56, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$124, %esi
	movl	$35, %edi
	movq	%rax, -40(%rbp)
	call	ERR_put_error@PLT
	movq	-40(%rbp), %rax
	jmp	.L19
	.cfi_endproc
.LFE804:
	.size	OPENSSL_uni2asc, .-OPENSSL_uni2asc
	.p2align 4
	.globl	OPENSSL_utf82uni
	.type	OPENSSL_utf82uni, @function
OPENSSL_utf82uni:
.LFB805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -80(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	cmpl	$-1, %esi
	jne	.L39
	call	strlen@PLT
	movl	%eax, %r14d
.L39:
	testl	%r14d, %r14d
	jle	.L40
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	leaq	-64(%rbp), %rbx
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L41:
	movq	-64(%rbp), %rdx
	cmpq	$1114111, %rdx
	ja	.L54
	leal	4(%r15), %esi
	leal	2(%r15), %ecx
	cmpq	$65536, %rdx
	cmovb	%ecx, %esi
	addl	%eax, %r13d
	movl	%esi, %r15d
	cmpl	%r13d, %r14d
	jle	.L72
.L45:
	movl	%r14d, %esi
	movslq	%r13d, %rdi
	movq	%rbx, %rdx
	subl	%r13d, %esi
	addq	%r12, %rdi
	call	UTF8_getc@PLT
	testl	%eax, %eax
	jns	.L41
	movq	-88(%rbp), %rcx
	movq	-80(%rbp), %rdx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	OPENSSL_asc2uni
	movq	%rax, %r13
.L38:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L73
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	xorl	%r13d, %r13d
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L72:
	leal	2(%rsi), %eax
	movl	$117, %edx
	leaq	.LC0(%rip), %rsi
	movslq	%eax, %rdi
	movl	%eax, -92(%rbp)
	call	CRYPTO_malloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L51
	movq	%rax, %rcx
	xorl	%r15d, %r15d
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L74:
	subq	$65536, %rdx
	addl	%eax, %r15d
	addq	$4, %rcx
	movq	%rdx, %rsi
	movq	%rdx, -64(%rbp)
	andl	$1023, %edx
	shrq	$10, %rsi
	addl	$56320, %edx
	addl	$55296, %esi
	rolw	$8, %dx
	rolw	$8, %si
	movw	%dx, -2(%rcx)
	movw	%si, -4(%rcx)
	cmpl	%r15d, %r14d
	jle	.L52
.L49:
	movl	%r14d, %esi
	movslq	%r15d, %rdi
	movq	%rbx, %rdx
	movq	%rcx, -72(%rbp)
	subl	%r15d, %esi
	addq	%r12, %rdi
	call	UTF8_getc@PLT
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rcx
	cmpq	$65535, %rdx
	ja	.L74
	shrq	$8, %rdx
	addl	%eax, %r15d
	addq	$2, %rcx
	movb	%dl, -2(%rcx)
	movq	-64(%rbp), %rdx
	movb	%dl, -1(%rcx)
	cmpl	%r15d, %r14d
	jg	.L49
.L52:
	xorl	%eax, %eax
	movw	%ax, (%rcx)
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	je	.L50
	movl	-92(%rbp), %ebx
	movl	%ebx, (%rax)
.L50:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L38
	movq	%r13, (%rax)
	jmp	.L38
.L40:
	movl	$117, %edx
	leaq	.LC0(%rip), %rsi
	movl	$2, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L51
	movl	$2, -92(%rbp)
	movq	%rax, %r13
	jmp	.L52
.L51:
	movl	$118, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$129, %esi
	movl	$35, %edi
	call	ERR_put_error@PLT
	jmp	.L38
.L73:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE805:
	.size	OPENSSL_utf82uni, .-OPENSSL_utf82uni
	.p2align 4
	.globl	OPENSSL_uni2utf8
	.type	OPENSSL_uni2utf8, @function
OPENSSL_uni2utf8:
.LFB807:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	andl	$1, %eax
	movl	%eax, -52(%rbp)
	jne	.L98
	movq	%rdi, %r12
	movl	%esi, %r13d
	testl	%esi, %esi
	jle	.L77
	cmpl	$1, %esi
	je	.L79
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	movq	%rdi, %rcx
	movl	$4, %r14d
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L83:
	movl	%r13d, %esi
	movslq	%r15d, %rcx
	subl	%r15d, %esi
	addq	%r12, %rcx
	cmpl	$1, %esi
	je	.L79
.L78:
	movzwl	(%rcx), %edx
	rolw	$8, %dx
	movzwl	%dx, %edx
	leaq	-55296(%rdx), %rax
	cmpq	$2047, %rax
	ja	.L80
	cmpl	$3, %esi
	jle	.L79
	salq	$10, %rax
	movq	%rax, %rdi
	movzwl	2(%rcx), %eax
	rolw	$8, %ax
	movzwl	%ax, %eax
	subl	$56320, %eax
	cmpl	$1023, %eax
	ja	.L79
	movl	%eax, %edx
	movq	%rdx, %rax
	orq	%rdi, %rax
	leaq	65536(%rax), %rdx
.L80:
	cmpl	$4, %esi
	cmovg	%r14d, %esi
	xorl	%edi, %edi
	call	UTF8_putc@PLT
	testl	%eax, %eax
	js	.L79
	leal	4(%r15), %edx
	addl	$2, %r15d
	cmpl	$4, %eax
	cmove	%edx, %r15d
	addl	%eax, %ebx
	cmpl	%r13d, %r15d
	jl	.L83
.L84:
	movslq	%r13d, %rax
	cmpb	$0, -2(%r12,%rax)
	jne	.L87
	cmpb	$0, -1(%r12,%rax)
	jne	.L87
.L113:
	movslq	%ebx, %rdi
.L88:
	movl	$202, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L89
	xorl	%ebx, %ebx
	testl	%r13d, %r13d
	jg	.L90
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L92:
	addl	$2, -52(%rbp)
	addl	%eax, %ebx
	cmpl	%r13d, -52(%rbp)
	jge	.L95
.L90:
	movslq	-52(%rbp), %rcx
	movl	%r13d, %esi
	movslq	%ebx, %rdi
	addq	%r14, %rdi
	subl	%ecx, %esi
	addq	%r12, %rcx
	cmpl	$1, %esi
	je	.L104
	movzwl	(%rcx), %edx
	rolw	$8, %dx
	movzwl	%dx, %edx
	leaq	-55296(%rdx), %rax
	cmpq	$2047, %rax
	ja	.L93
	cmpl	$3, %esi
	jle	.L104
	salq	$10, %rax
	movq	%rax, %r9
	movzwl	2(%rcx), %eax
	rolw	$8, %ax
	movzwl	%ax, %eax
	subl	$56320, %eax
	cmpl	$1023, %eax
	ja	.L104
	movl	%eax, %edx
	movq	%rdx, %rax
	orq	%r9, %rax
	leaq	65536(%rax), %rdx
.L93:
	cmpl	$4, %esi
	movl	$4, %eax
	cmovg	%eax, %esi
	call	UTF8_putc@PLT
	cmpl	$4, %eax
	jne	.L92
	addl	$4, -52(%rbp)
	addl	%eax, %ebx
	cmpl	%r13d, -52(%rbp)
	jl	.L90
.L95:
	movslq	%r13d, %r8
	cmpb	$0, -2(%r12,%r8)
	jne	.L114
	cmpb	$0, -1(%r12,%r8)
	je	.L75
.L114:
	movslq	%ebx, %rax
	addq	%r14, %rax
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L79:
	addq	$24, %rsp
	movl	%r13d, %esi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	OPENSSL_uni2asc
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	addl	$1, %ebx
	jmp	.L113
.L77:
	jne	.L116
	movl	$1, %edi
	jmp	.L88
.L115:
	jne	.L95
.L96:
	movb	$0, (%rax)
.L75:
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	xorl	%r14d, %r14d
	jmp	.L75
.L104:
	movl	$-1, %eax
	jmp	.L92
.L89:
	movl	$203, %r8d
	movl	$65, %edx
	movl	$127, %esi
	movl	$35, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L75
.L116:
	xorl	%ebx, %ebx
	jmp	.L84
	.cfi_endproc
.LFE807:
	.size	OPENSSL_uni2utf8, .-OPENSSL_uni2utf8
	.p2align 4
	.globl	i2d_PKCS12_bio
	.type	i2d_PKCS12_bio, @function
i2d_PKCS12_bio:
.LFB808:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	PKCS12_it(%rip), %rdi
	jmp	ASN1_item_i2d_bio@PLT
	.cfi_endproc
.LFE808:
	.size	i2d_PKCS12_bio, .-i2d_PKCS12_bio
	.p2align 4
	.globl	i2d_PKCS12_fp
	.type	i2d_PKCS12_fp, @function
i2d_PKCS12_fp:
.LFB809:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	PKCS12_it(%rip), %rdi
	jmp	ASN1_item_i2d_fp@PLT
	.cfi_endproc
.LFE809:
	.size	i2d_PKCS12_fp, .-i2d_PKCS12_fp
	.p2align 4
	.globl	d2i_PKCS12_bio
	.type	d2i_PKCS12_bio, @function
d2i_PKCS12_bio:
.LFB810:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	PKCS12_it(%rip), %rdi
	jmp	ASN1_item_d2i_bio@PLT
	.cfi_endproc
.LFE810:
	.size	d2i_PKCS12_bio, .-d2i_PKCS12_bio
	.p2align 4
	.globl	d2i_PKCS12_fp
	.type	d2i_PKCS12_fp, @function
d2i_PKCS12_fp:
.LFB811:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	PKCS12_it(%rip), %rdi
	jmp	ASN1_item_d2i_fp@PLT
	.cfi_endproc
.LFE811:
	.size	d2i_PKCS12_fp, .-d2i_PKCS12_fp
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
