	.file	"aes_wrap.c"
	.text
	.p2align 4
	.globl	AES_wrap_key
	.type	AES_wrap_key, @function
AES_wrap_key:
.LFB251:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	AES_encrypt@GOTPCREL(%rip), %r9
	movl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	CRYPTO_128_wrap@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE251:
	.size	AES_wrap_key, .-AES_wrap_key
	.p2align 4
	.globl	AES_unwrap_key
	.type	AES_unwrap_key, @function
AES_unwrap_key:
.LFB252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	AES_decrypt@GOTPCREL(%rip), %r9
	movl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	CRYPTO_128_unwrap@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE252:
	.size	AES_unwrap_key, .-AES_unwrap_key
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
