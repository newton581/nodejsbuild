	.file	"x_algor.c"
	.text
	.p2align 4
	.globl	d2i_X509_ALGOR
	.type	d2i_X509_ALGOR, @function
d2i_X509_ALGOR:
.LFB768:
	.cfi_startproc
	endbr64
	leaq	X509_ALGOR_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE768:
	.size	d2i_X509_ALGOR, .-d2i_X509_ALGOR
	.p2align 4
	.globl	i2d_X509_ALGOR
	.type	i2d_X509_ALGOR, @function
i2d_X509_ALGOR:
.LFB769:
	.cfi_startproc
	endbr64
	leaq	X509_ALGOR_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE769:
	.size	i2d_X509_ALGOR, .-i2d_X509_ALGOR
	.p2align 4
	.globl	X509_ALGOR_new
	.type	X509_ALGOR_new, @function
X509_ALGOR_new:
.LFB770:
	.cfi_startproc
	endbr64
	leaq	X509_ALGOR_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE770:
	.size	X509_ALGOR_new, .-X509_ALGOR_new
	.p2align 4
	.globl	X509_ALGOR_free
	.type	X509_ALGOR_free, @function
X509_ALGOR_free:
.LFB771:
	.cfi_startproc
	endbr64
	leaq	X509_ALGOR_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE771:
	.size	X509_ALGOR_free, .-X509_ALGOR_free
	.p2align 4
	.globl	d2i_X509_ALGORS
	.type	d2i_X509_ALGORS, @function
d2i_X509_ALGORS:
.LFB772:
	.cfi_startproc
	endbr64
	leaq	X509_ALGORS_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE772:
	.size	d2i_X509_ALGORS, .-d2i_X509_ALGORS
	.p2align 4
	.globl	i2d_X509_ALGORS
	.type	i2d_X509_ALGORS, @function
i2d_X509_ALGORS:
.LFB773:
	.cfi_startproc
	endbr64
	leaq	X509_ALGORS_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE773:
	.size	i2d_X509_ALGORS, .-i2d_X509_ALGORS
	.p2align 4
	.globl	X509_ALGOR_dup
	.type	X509_ALGOR_dup, @function
X509_ALGOR_dup:
.LFB774:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	leaq	X509_ALGOR_it(%rip), %rdi
	jmp	ASN1_item_dup@PLT
	.cfi_endproc
.LFE774:
	.size	X509_ALGOR_dup, .-X509_ALGOR_dup
	.p2align 4
	.globl	X509_ALGOR_set0
	.type	X509_ALGOR_set0, @function
X509_ALGOR_set0:
.LFB775:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testq	%rdi, %rdi
	je	.L14
	movq	%rdi, %rbx
	movq	%rsi, %r13
	movl	%edx, %r12d
	cmpl	$-1, %edx
	je	.L12
	cmpq	$0, 8(%rdi)
	movq	%rcx, %r14
	je	.L23
.L13:
	movq	(%rbx), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	%r13, (%rbx)
	movl	$1, %r13d
	testl	%r12d, %r12d
	je	.L9
	movq	8(%rbx), %rdi
	movq	%r14, %rdx
	movl	%r12d, %esi
	call	ASN1_TYPE_set@PLT
.L9:
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	call	ASN1_TYPE_new@PLT
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	jne	.L13
.L14:
	xorl	%r13d, %r13d
	popq	%rbx
	popq	%r12
	movl	%r13d, %eax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	%r13, (%rbx)
	movq	8(%rbx), %rdi
	movl	$1, %r13d
	call	ASN1_TYPE_free@PLT
	movq	$0, 8(%rbx)
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE775:
	.size	X509_ALGOR_set0, .-X509_ALGOR_set0
	.p2align 4
	.globl	X509_ALGOR_get0
	.type	X509_ALGOR_get0, @function
X509_ALGOR_get0:
.LFB776:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L25
	movq	(%rcx), %rax
	movq	%rax, (%rdi)
.L25:
	testq	%rsi, %rsi
	je	.L24
	movq	8(%rcx), %rax
	testq	%rax, %rax
	je	.L36
	movl	(%rax), %ecx
	movl	%ecx, (%rsi)
	testq	%rdx, %rdx
	je	.L24
	movq	8(%rax), %rax
	movq	%rax, (%rdx)
.L24:
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$-1, (%rsi)
	ret
	.cfi_endproc
.LFE776:
	.size	X509_ALGOR_get0, .-X509_ALGOR_get0
	.p2align 4
	.globl	X509_ALGOR_set_md
	.type	X509_ALGOR_set_md, @function
X509_ALGOR_set_md:
.LFB777:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	testb	$8, 16(%rsi)
	je	.L56
	call	EVP_MD_type@PLT
	movl	%eax, %edi
	call	OBJ_nid2obj@PLT
	movq	%rax, %r12
	testq	%rbx, %rbx
	jne	.L53
.L37:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	call	EVP_MD_type@PLT
	movl	%eax, %edi
	call	OBJ_nid2obj@PLT
	movq	%rax, %r12
	testq	%rbx, %rbx
	je	.L37
	cmpq	$0, 8(%rbx)
	je	.L57
.L40:
	movq	(%rbx), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	%r12, (%rbx)
	movq	8(%rbx), %rdi
	xorl	%edx, %edx
	popq	%rbx
	movl	$5, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ASN1_TYPE_set@PLT
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	%r12, (%rbx)
	movq	8(%rbx), %rdi
	call	ASN1_TYPE_free@PLT
	movq	$0, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	call	ASN1_TYPE_new@PLT
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	jne	.L40
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE777:
	.size	X509_ALGOR_set_md, .-X509_ALGOR_set_md
	.p2align 4
	.globl	X509_ALGOR_cmp
	.type	X509_ALGOR_cmp, @function
X509_ALGOR_cmp:
.LFB778:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movq	(%rdi), %rdi
	movq	(%rsi), %rsi
	call	OBJ_cmp@PLT
	testl	%eax, %eax
	jne	.L58
	movq	8(%r12), %rdi
	movq	8(%rbx), %rsi
	testq	%rdi, %rdi
	je	.L65
.L60:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ASN1_TYPE_cmp@PLT
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	testq	%rsi, %rsi
	jne	.L60
.L58:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE778:
	.size	X509_ALGOR_cmp, .-X509_ALGOR_cmp
	.globl	X509_ALGORS_it
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"X509_ALGORS"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	X509_ALGORS_it, @object
	.size	X509_ALGORS_it, 56
X509_ALGORS_it:
	.byte	0
	.zero	7
	.quad	-1
	.quad	X509_ALGORS_item_tt
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC0
	.section	.rodata.str1.1
.LC1:
	.string	"algorithms"
	.section	.data.rel.ro.local
	.align 32
	.type	X509_ALGORS_item_tt, @object
	.size	X509_ALGORS_item_tt, 40
X509_ALGORS_item_tt:
	.quad	4
	.quad	0
	.quad	0
	.quad	.LC1
	.quad	X509_ALGOR_it
	.globl	X509_ALGOR_it
	.section	.rodata.str1.1
.LC2:
	.string	"X509_ALGOR"
	.section	.data.rel.ro.local
	.align 32
	.type	X509_ALGOR_it, @object
	.size	X509_ALGOR_it, 56
X509_ALGOR_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	X509_ALGOR_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC2
	.section	.rodata.str1.1
.LC3:
	.string	"algorithm"
.LC4:
	.string	"parameter"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	X509_ALGOR_seq_tt, @object
	.size	X509_ALGOR_seq_tt, 80
X509_ALGOR_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC3
	.quad	ASN1_OBJECT_it
	.quad	1
	.quad	0
	.quad	8
	.quad	.LC4
	.quad	ASN1_ANY_it
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
