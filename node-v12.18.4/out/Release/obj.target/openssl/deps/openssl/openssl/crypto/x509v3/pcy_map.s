	.file	"pcy_map.c"
	.text
	.p2align 4
	.globl	policy_cache_set_mapping
	.type	policy_cache_set_mapping, @function
policy_cache_set_mapping:
.LFB1322:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	256(%rdi), %r14
	movq	%rsi, %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jne	.L3
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L4:
	orl	$1, (%rax)
.L8:
	movq	8(%rbx), %rsi
	movq	24(%r8), %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L9
	movq	$0, 8(%rbx)
.L5:
	addl	$1, %r12d
.L3:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L28
	movq	%r13, %rdi
	movl	%r12d, %esi
	call	OPENSSL_sk_value@PLT
	movq	8(%rax), %rdi
	movq	%rax, %rbx
	call	OBJ_obj2nid@PLT
	cmpl	$746, %eax
	je	.L2
	movq	(%rbx), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$746, %eax
	je	.L2
	movq	(%rbx), %rsi
	movq	%r14, %rdi
	call	policy_cache_find_data@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	jne	.L4
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.L5
	movl	(%rax), %edx
	movq	(%rbx), %rsi
	xorl	%edi, %edi
	andl	$16, %edx
	call	policy_data_new@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L9
	movq	(%r14), %rax
	movq	8(%r14), %rdi
	movq	%r8, %rsi
	movq	%r8, -56(%rbp)
	movq	16(%rax), %rax
	orl	$6, (%r8)
	movq	%rax, 16(%r8)
	call	OPENSSL_sk_push@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	jne	.L8
	movq	%r8, %rdi
	movl	%eax, -56(%rbp)
	call	policy_data_free@PLT
	movl	-56(%rbp), %eax
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L2:
	orl	$2048, 224(%r15)
	movl	$-1, %eax
.L7:
	movq	POLICY_MAPPING_free@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	movl	%eax, -56(%rbp)
	call	OPENSSL_sk_pop_free@PLT
	movl	-56(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L28:
	movl	$1, %eax
	jmp	.L7
	.cfi_endproc
.LFE1322:
	.size	policy_cache_set_mapping, .-policy_cache_set_mapping
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
