	.file	"p12_p8e.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/pkcs12/p12_p8e.c"
	.text
	.p2align 4
	.globl	PKCS8_encrypt
	.type	PKCS8_encrypt, @function
PKCS8_encrypt:
.LFB805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r9d, %ebx
	subq	$24, %rsp
	cmpl	$-1, %edi
	je	.L12
	movq	%rsi, -56(%rbp)
	movl	%edi, %r12d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%edi, %esi
	movl	$1, %edi
	call	EVP_PBE_find@PLT
	testl	%eax, %eax
	jne	.L13
	call	ERR_clear_error@PLT
	movl	16(%rbp), %esi
	movl	%r12d, %edi
	movl	%ebx, %ecx
	movq	%r13, %rdx
	call	PKCS5_pbe_set@PLT
	movq	%rax, %r12
.L3:
	testq	%r12, %r12
	je	.L14
	movq	24(%rbp), %r8
	movl	$1, %r9d
	movl	%r15d, %ecx
	movq	%r14, %rdx
	leaq	PKCS8_PRIV_KEY_INFO_it(%rip), %rsi
	movq	%r12, %rdi
	call	PKCS12_item_i2d_encrypt@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L15
	movl	$58, %edx
	leaq	.LC0(%rip), %rsi
	movl	$16, %edi
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L16
	movq	%r12, %xmm0
	movq	%r13, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rax)
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	movq	-56(%rbp), %r11
	movl	16(%rbp), %esi
	movl	%r12d, %r9d
	xorl	%r8d, %r8d
	movl	%ebx, %ecx
	movq	%r13, %rdx
	movq	%r11, %rdi
	call	PKCS5_pbe2_set_iv@PLT
	movq	%rax, %r12
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L12:
	movl	16(%rbp), %esi
	movl	%r9d, %ecx
	movq	%r8, %rdx
	movq	%r11, %rdi
	call	PKCS5_pbe2_set@PLT
	movq	%rax, %r12
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L14:
	movl	$32, %r8d
	movl	$13, %edx
	movl	$125, %esi
	movl	$35, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L15:
	movl	$54, %r8d
	movl	$103, %edx
	movl	$132, %esi
	movl	$35, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L8:
	movq	%r12, %rdi
	call	X509_ALGOR_free@PLT
	xorl	%eax, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L16:
	movl	$61, %r8d
	movl	$65, %edx
	movl	$132, %esi
	movl	$35, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r13, %rdi
	call	ASN1_OCTET_STRING_free@PLT
	jmp	.L8
	.cfi_endproc
.LFE805:
	.size	PKCS8_encrypt, .-PKCS8_encrypt
	.p2align 4
	.globl	PKCS8_set0_pbe
	.type	PKCS8_set0_pbe, @function
PKCS8_set0_pbe:
.LFB806:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r8
	movl	$1, %r9d
	movq	%rdi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rcx, %rbx
	movl	%esi, %ecx
	leaq	PKCS8_PRIV_KEY_INFO_it(%rip), %rsi
	movq	%rbx, %rdi
	subq	$16, %rsp
	call	PKCS12_item_i2d_encrypt@PLT
	testq	%rax, %rax
	je	.L22
	movl	$58, %edx
	leaq	.LC0(%rip), %rsi
	movl	$16, %edi
	movq	%rax, %r12
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L23
	movq	%rbx, %xmm0
	movq	%r12, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rax)
.L17:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	movl	$54, %r8d
	movl	$103, %edx
	movl	$132, %esi
	movl	$35, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L23:
	movl	$61, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$132, %esi
	movl	$35, %edi
	movq	%rax, -24(%rbp)
	call	ERR_put_error@PLT
	movq	%r12, %rdi
	call	ASN1_OCTET_STRING_free@PLT
	movq	-24(%rbp), %rax
	jmp	.L17
	.cfi_endproc
.LFE806:
	.size	PKCS8_set0_pbe, .-PKCS8_set0_pbe
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
