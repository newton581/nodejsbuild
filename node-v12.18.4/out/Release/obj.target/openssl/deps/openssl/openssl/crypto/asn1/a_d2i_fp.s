	.file	"a_d2i_fp.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/asn1/a_d2i_fp.c"
	.text
	.p2align 4
	.globl	asn1_d2i_read_bio
	.type	asn1_d2i_read_bio, @function
asn1_d2i_read_bio:
.LFB447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	BUF_MEM_new@PLT
	testq	%rax, %rax
	jne	.L2
	movl	$110, %r8d
	movl	$65, %edx
	movl	$107, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
.L1:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L55
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movq	%rax, %r13
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	call	ERR_clear_error@PLT
	movl	$0, -84(%rbp)
.L4:
	movq	%rbx, %r8
	subq	%r12, %r8
	cmpq	$8, %r8
	ja	.L5
.L59:
	leaq	8(%r12), %rsi
	movq	%rsi, %r15
	subq	%rbx, %r15
	cmpq	%rsi, %rbx
	ja	.L8
	movq	%r13, %rdi
	movq	%r8, -96(%rbp)
	call	BUF_MEM_grow_clean@PLT
	testq	%rax, %rax
	je	.L8
	movq	8(%r13), %rsi
	movl	%r15d, %edx
	movq	%r14, %rdi
	addq	%rbx, %rsi
	call	BIO_read@PLT
	movq	-96(%rbp), %r8
	testl	%eax, %eax
	jns	.L9
	cmpq	%rbx, %r12
	je	.L56
.L9:
	testl	%eax, %eax
	jle	.L5
	cltq
	addq	%rbx, %rax
	movq	%rax, %rbx
	jc	.L12
	movq	%rax, %r8
	subq	%r12, %r8
.L5:
	movq	8(%r13), %r15
	leaq	-80(%rbp), %rdx
	leaq	-76(%rbp), %rcx
	leaq	-64(%rbp), %rsi
	leaq	-72(%rbp), %rdi
	addq	%r12, %r15
	movq	%r15, -72(%rbp)
	call	ASN1_get_object@PLT
	movl	%eax, %edx
	testb	$-128, %al
	jne	.L57
.L13:
	movq	-72(%rbp), %rax
	subq	%r15, %rax
	cltq
	addq	%rax, %r12
	andl	$1, %edx
	je	.L14
	cmpl	$-1, -84(%rbp)
	je	.L58
	movq	%rbx, %r8
	addl	$1, -84(%rbp)
	subq	%r12, %r8
	cmpq	$8, %r8
	jbe	.L59
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L14:
	movl	-84(%rbp), %ecx
	movq	-64(%rbp), %rax
	testl	%ecx, %ecx
	je	.L17
	testq	%rax, %rax
	jne	.L17
	movl	-80(%rbp), %edx
	testl	%edx, %edx
	jne	.L4
	subl	$1, %ecx
	movl	%ecx, -84(%rbp)
	jne	.L4
.L18:
	cmpq	$2147483647, %r12
	ja	.L60
	movq	-104(%rbp), %rax
	movq	%r13, (%rax)
	movl	%r12d, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L17:
	movq	%rbx, %rdx
	subq	%r12, %rdx
	cmpq	%rax, %rdx
	jb	.L61
.L19:
	addq	%rax, %r12
	jc	.L62
	movl	-84(%rbp), %eax
	testl	%eax, %eax
	jne	.L4
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L57:
	movl	%eax, -96(%rbp)
	call	ERR_peek_error@PLT
	andl	$4095, %eax
	cmpl	$155, %eax
	jne	.L7
	call	ERR_clear_error@PLT
	movl	-96(%rbp), %edx
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L61:
	leaq	(%r12,%rax), %rdx
	movl	$2147483648, %edi
	movq	%rdx, %rcx
	subq	%rbx, %rcx
	movq	%rcx, -120(%rbp)
	cmpq	%rdi, %rcx
	jnb	.L20
	cmpq	%rdx, %rbx
	jbe	.L63
.L20:
	movl	$177, %r8d
.L53:
	movl	$155, %edx
	movl	$107, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L7:
	movq	%r13, %rdi
	call	BUF_MEM_free@PLT
	movl	$-1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L63:
	movq	$16384, -96(%rbp)
	testq	%rcx, %rcx
	je	.L19
.L21:
	movq	-96(%rbp), %rcx
	movq	-120(%rbp), %rax
	movq	%r13, %rdi
	cmpq	%rax, %rcx
	cmovbe	%rcx, %rax
	leaq	(%rax,%rbx), %rsi
	movq	%rax, -112(%rbp)
	call	BUF_MEM_grow_clean@PLT
	testq	%rax, %rax
	je	.L64
	movq	-112(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L22:
	movq	8(%r13), %rsi
	movl	%r15d, %edx
	movq	%r14, %rdi
	addq	%rbx, %rsi
	call	BIO_read@PLT
	testl	%eax, %eax
	jle	.L65
	cltq
	addq	%rax, %rbx
	subq	%rax, %r15
	jne	.L22
	movq	-96(%rbp), %rdi
	movq	-112(%rbp), %rcx
	cmpq	$1073741823, %rdi
	leaq	(%rdi,%rdi), %rax
	cmovnb	%rdi, %rax
	subq	%rcx, -120(%rbp)
	movq	%rax, -96(%rbp)
	jne	.L21
	movq	-64(%rbp), %rax
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L65:
	movl	$197, %r8d
.L52:
	movl	$142, %edx
	movl	$107, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r13, %rdi
	call	BUF_MEM_free@PLT
	movl	$-1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$120, %r8d
.L51:
	movl	$65, %edx
	movl	$107, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L7
.L56:
	movl	$125, %r8d
	jmp	.L52
.L58:
	movl	$156, %r8d
	movl	$123, %edx
	movl	$107, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L7
.L12:
	movl	$130, %r8d
	jmp	.L53
.L64:
	movl	$190, %r8d
	jmp	.L51
.L62:
	movl	$213, %r8d
	jmp	.L53
.L55:
	call	__stack_chk_fail@PLT
.L60:
	movl	$225, %r8d
	jmp	.L53
	.cfi_endproc
.LFE447:
	.size	asn1_d2i_read_bio, .-asn1_d2i_read_bio
	.p2align 4
	.globl	ASN1_d2i_bio
	.type	ASN1_d2i_bio, @function
ASN1_d2i_bio:
.LFB444:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	leaq	-40(%rbp), %rsi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$0, -40(%rbp)
	call	asn1_d2i_read_bio
	testl	%eax, %eax
	js	.L69
	movq	-40(%rbp), %rdx
	movq	%r12, %rdi
	leaq	-32(%rbp), %rsi
	movq	8(%rdx), %rdx
	movq	%rdx, -32(%rbp)
	movslq	%eax, %rdx
	call	*%rbx
	movq	%rax, %r12
.L67:
	movq	-40(%rbp), %rdi
	call	BUF_MEM_free@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L71
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L67
.L71:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE444:
	.size	ASN1_d2i_bio, .-ASN1_d2i_bio
	.p2align 4
	.globl	ASN1_item_d2i_bio
	.type	ASN1_item_d2i_bio, @function
ASN1_item_d2i_bio:
.LFB445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	movq	%rsi, %rdi
	pushq	%r13
	leaq	-56(%rbp), %rsi
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	xorl	%r12d, %r12d
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -56(%rbp)
	call	asn1_d2i_read_bio
	testl	%eax, %eax
	js	.L73
	movq	-56(%rbp), %rdx
	leaq	-48(%rbp), %rsi
	movq	%r14, %rcx
	movq	%r13, %rdi
	movq	8(%rdx), %rdx
	movq	%rdx, -48(%rbp)
	movslq	%eax, %rdx
	call	ASN1_item_d2i@PLT
	movq	%rax, %r12
.L73:
	movq	-56(%rbp), %rdi
	call	BUF_MEM_free@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L78
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L78:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE445:
	.size	ASN1_item_d2i_bio, .-ASN1_item_d2i_bio
	.p2align 4
	.globl	ASN1_d2i_fp
	.type	ASN1_d2i_fp, @function
ASN1_d2i_fp:
.LFB443:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	testq	%rax, %rax
	je	.L86
	movq	%rax, %r12
	xorl	%edx, %edx
	movl	$106, %esi
	movq	%rax, %rdi
	movq	%r14, %rcx
	call	BIO_ctrl@PLT
	leaq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -56(%rbp)
	call	asn1_d2i_read_bio
	testl	%eax, %eax
	js	.L84
	movq	-56(%rbp), %rdx
	movq	%r13, %rdi
	leaq	-48(%rbp), %rsi
	movq	8(%rdx), %rdx
	movq	%rdx, -48(%rbp)
	movslq	%eax, %rdx
	call	*%rbx
	movq	%rax, %r13
.L82:
	movq	-56(%rbp), %rdi
	call	BUF_MEM_free@PLT
	movq	%r12, %rdi
	call	BIO_free@PLT
.L79:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L87
	addq	$32, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	xorl	%r13d, %r13d
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L86:
	movl	$27, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$7, %edx
	xorl	%r13d, %r13d
	movl	$109, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
	jmp	.L79
.L87:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE443:
	.size	ASN1_d2i_fp, .-ASN1_d2i_fp
	.p2align 4
	.globl	ASN1_item_d2i_fp
	.type	ASN1_item_d2i_fp, @function
ASN1_item_d2i_fp:
.LFB446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	subq	$32, %rsp
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	testq	%rax, %rax
	je	.L95
	movq	%r15, %rcx
	movq	%rax, %r12
	xorl	%edx, %edx
	movl	$106, %esi
	movq	%rax, %rdi
	xorl	%r15d, %r15d
	call	BIO_ctrl@PLT
	leaq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -56(%rbp)
	call	asn1_d2i_read_bio
	testl	%eax, %eax
	js	.L91
	movq	-56(%rbp), %rdx
	leaq	-48(%rbp), %rsi
	movq	%r14, %rcx
	movq	%r13, %rdi
	movq	8(%rdx), %rdx
	movq	%rdx, -48(%rbp)
	movslq	%eax, %rdx
	call	ASN1_item_d2i@PLT
	movq	%rax, %r15
.L91:
	movq	-56(%rbp), %rdi
	call	BUF_MEM_free@PLT
	movq	%r12, %rdi
	call	BIO_free@PLT
.L88:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L96
	addq	$32, %rsp
	movq	%r15, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	movl	$82, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$7, %edx
	xorl	%r15d, %r15d
	movl	$206, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
	jmp	.L88
.L96:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE446:
	.size	ASN1_item_d2i_fp, .-ASN1_item_d2i_fp
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
