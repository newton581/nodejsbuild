	.file	"cfb64ede.c"
	.text
	.p2align 4
	.globl	DES_ede3_cfb64_encrypt
	.type	DES_ede3_cfb64_encrypt, @function
DES_ede3_cfb64_encrypt:
.LFB54:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	24(%rbp), %rax
	movl	32(%rbp), %ecx
	movq	16(%rbp), %r15
	movq	%rax, -96(%rbp)
	movl	(%rax), %eax
	movq	%fs:40, %rsi
	movq	%rsi, -56(%rbp)
	xorl	%esi, %esi
	testl	%ecx, %ecx
	jne	.L2
	leaq	(%rdi,%rdx), %r14
	leaq	-64(%rbp), %rdi
	testq	%rdx, %rdx
	je	.L4
	.p2align 4,,10
	.p2align 3
.L10:
	testl	%eax, %eax
	jne	.L8
	movq	(%r15), %rax
	movq	%r8, %rdx
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r9, -88(%rbp)
	addq	$1, %rbx
	addq	$1, %r12
	movq	%rax, -64(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	DES_encrypt3@PLT
	movq	-64(%rbp), %rax
	movq	%rax, (%r15)
	movzbl	-1(%rbx), %eax
	movzbl	(%r15), %edx
	movb	%al, (%r15)
	xorl	%edx, %eax
	movb	%al, -1(%r12)
	movl	$1, %eax
	cmpq	%r14, %rbx
	je	.L4
	movq	-88(%rbp), %r9
	movq	-80(%rbp), %r8
	movq	-72(%rbp), %rdi
.L8:
	movslq	%eax, %rcx
	movzbl	(%rbx), %edx
	addq	$1, %rbx
	addq	$1, %r12
	addq	%r15, %rcx
	addl	$1, %eax
	movzbl	(%rcx), %r10d
	movb	%dl, (%rcx)
	andl	$7, %eax
	xorl	%r10d, %edx
	movb	%dl, -1(%r12)
	cmpq	%r14, %rbx
	jne	.L10
.L4:
	movq	-96(%rbp), %rsi
	movl	%eax, (%rsi)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L22
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	testq	%rdx, %rdx
	je	.L4
	leaq	-64(%rbp), %rsi
	leaq	(%rdi,%rdx), %r14
	movq	%rsi, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L7:
	testl	%eax, %eax
	jne	.L5
	movq	(%r15), %rax
	movq	-88(%rbp), %rdi
	movq	%r9, %rcx
	movq	%r8, %rdx
	movq	%r13, %rsi
	movq	%r9, -80(%rbp)
	addq	$1, %rbx
	addq	$1, %r12
	movq	%rax, -64(%rbp)
	movq	%r8, -72(%rbp)
	call	DES_encrypt3@PLT
	movq	-64(%rbp), %rax
	movq	%rax, (%r15)
	movzbl	-1(%rbx), %eax
	xorb	(%r15), %al
	movb	%al, -1(%r12)
	movb	%al, (%r15)
	movl	$1, %eax
	cmpq	%rbx, %r14
	je	.L4
	movq	-80(%rbp), %r9
	movq	-72(%rbp), %r8
.L5:
	movslq	%eax, %rdx
	movzbl	(%rbx), %ecx
	addq	$1, %rbx
	addq	$1, %r12
	addq	%r15, %rdx
	addl	$1, %eax
	xorb	(%rdx), %cl
	andl	$7, %eax
	movb	%cl, -1(%r12)
	movb	%cl, (%rdx)
	cmpq	%rbx, %r14
	jne	.L7
	jmp	.L4
.L22:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE54:
	.size	DES_ede3_cfb64_encrypt, .-DES_ede3_cfb64_encrypt
	.p2align 4
	.globl	DES_ede3_cfb_encrypt
	.type	DES_ede3_cfb_encrypt, @function
DES_ede3_cfb_encrypt:
.LFB55:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -112(%rbp)
	movq	%r9, -120(%rbp)
	movq	%rax, -128(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	cmpl	$64, %edx
	jg	.L23
	movl	%edx, %r10d
	leal	7(%rdx), %edx
	movq	%rsi, %r12
	movq	%rcx, %r15
	movl	%edx, %esi
	movl	32(%rbp), %ecx
	movl	(%rax), %r9d
	shrl	$3, %esi
	movl	4(%rax), %eax
	movl	%esi, -100(%rbp)
	movl	%esi, %r13d
	testl	%ecx, %ecx
	je	.L112
	cmpq	%r13, %r15
	jb	.L28
	movl	%r10d, %esi
	movq	%rbx, -144(%rbp)
	movq	%r13, %r8
	movl	%r9d, %r13d
	andl	$7, %esi
	testl	%r10d, %r10d
	movl	%r10d, -104(%rbp)
	leaq	.L31(%rip), %r14
	cmovns	%r10d, %edx
	movl	%esi, -176(%rbp)
	leaq	-80(%rbp), %rsi
	movq	%rsi, -184(%rbp)
	sarl	$3, %edx
	movslq	%edx, %rdx
	addq	%rsi, %rdx
	leaq	-88(%rbp), %rsi
	movq	%rdx, -192(%rbp)
	movl	%r10d, %edx
	sarl	$31, %edx
	movq	%rsi, -136(%rbp)
	shrl	$29, %edx
	leal	(%r10,%rdx), %ecx
	andl	$7, %ecx
	movl	%ecx, %edi
	subl	%edx, %edi
	movl	$8, %edx
	subl	%edi, %edx
	movl	%edi, -200(%rbp)
	movl	%edx, -196(%rbp)
	.p2align 4,,10
	.p2align 3
.L52:
	movq	-128(%rbp), %rcx
	movq	-120(%rbp), %rdx
	subq	%r8, %r15
	movl	%eax, -84(%rbp)
	movq	-112(%rbp), %rsi
	movq	-136(%rbp), %rdi
	movl	%eax, -152(%rbp)
	movq	%r8, -160(%rbp)
	movl	%r13d, -88(%rbp)
	call	DES_encrypt3@PLT
	movq	-160(%rbp), %r8
	movq	-144(%rbp), %rbx
	movl	-152(%rbp), %eax
	addq	%r8, %rbx
	cmpl	$8, -100(%rbp)
	ja	.L80
	movslq	(%r14,%r8,4), %rdx
	addq	%r14, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L31:
	.long	.L80-.L31
	.long	.L81-.L31
	.long	.L82-.L31
	.long	.L83-.L31
	.long	.L84-.L31
	.long	.L85-.L31
	.long	.L86-.L31
	.long	.L87-.L31
	.long	.L30-.L31
	.text
	.p2align 4,,10
	.p2align 3
.L40:
	movl	%ecx, %esi
	subq	$1, %r12
	shrl	$24, %esi
	movb	%sil, (%r12)
.L42:
	movl	%ecx, %esi
	subq	$1, %r12
	shrl	$16, %esi
	movb	%sil, (%r12)
.L43:
	movzbl	%ch, %ebx
	leaq	-1(%r12), %rsi
	movb	%bl, -1(%r12)
.L44:
	movb	%cl, -1(%rsi)
	leaq	-1(%rsi), %r12
.L45:
	movl	%edx, %edi
	leaq	-1(%r12), %rsi
	shrl	$24, %edi
	movb	%dil, -1(%r12)
.L46:
	movl	%edx, %edi
	leaq	-1(%rsi), %r12
	shrl	$16, %edi
	movb	%dil, -1(%rsi)
.L47:
	movzbl	%dh, %ebx
	leaq	-1(%r12), %rsi
	movb	%bl, -1(%r12)
.L48:
	movb	%dl, -1(%rsi)
	leaq	-1(%rsi), %r12
.L39:
	movl	-104(%rbp), %ebx
	addq	%r8, %r12
	cmpl	$32, %ebx
	je	.L91
	cmpl	$64, %ebx
	je	.L92
	movl	%edx, -72(%rbp)
	movl	-176(%rbp), %edx
	movl	%r13d, -80(%rbp)
	movl	%eax, -76(%rbp)
	movl	%ecx, -68(%rbp)
	testl	%edx, %edx
	je	.L113
	movq	-192(%rbp), %rsi
	movq	-184(%rbp), %rdi
	movl	$9, %edx
	movq	%r8, -152(%rbp)
	call	memmove@PLT
	movl	-200(%rbp), %esi
	movl	-196(%rbp), %ebx
	movzbl	-79(%rbp), %edx
	movzbl	-80(%rbp), %eax
	movl	%esi, %ecx
	movq	-152(%rbp), %r8
	sall	%cl, %eax
	movl	%edx, %edi
	movl	%ebx, %ecx
	sarl	%cl, %edi
	movl	%esi, %ecx
	orl	%edi, %eax
	sall	%cl, %edx
	movl	%ebx, %ecx
	movb	%al, -80(%rbp)
	movzbl	-78(%rbp), %eax
	movl	%eax, %edi
	sarl	%cl, %edi
	movl	%esi, %ecx
	orl	%edi, %edx
	sall	%cl, %eax
	movl	%ebx, %ecx
	movb	%dl, -79(%rbp)
	movzbl	-77(%rbp), %edx
	movl	%edx, %edi
	sarl	%cl, %edi
	movl	%esi, %ecx
	orl	%edi, %eax
	sall	%cl, %edx
	movl	%ebx, %ecx
	movb	%al, -78(%rbp)
	movzbl	-76(%rbp), %eax
	movl	%eax, %edi
	sarl	%cl, %edi
	movl	%esi, %ecx
	orl	%edi, %edx
	sall	%cl, %eax
	movl	%ebx, %ecx
	movb	%dl, -77(%rbp)
	movzbl	-75(%rbp), %edx
	movl	%edx, %edi
	sarl	%cl, %edi
	movl	%esi, %ecx
	orl	%edi, %eax
	sall	%cl, %edx
	movl	%ebx, %ecx
	movb	%al, -76(%rbp)
	movzbl	-74(%rbp), %eax
	movl	%eax, %edi
	sarl	%cl, %edi
	movl	%esi, %ecx
	orl	%edi, %edx
	sall	%cl, %eax
	movl	%ebx, %ecx
	movb	%dl, -75(%rbp)
	movzbl	-73(%rbp), %edx
	movl	%edx, %edi
	sarl	%cl, %edi
	orl	%edi, %eax
	movb	%al, -74(%rbp)
	movzbl	-72(%rbp), %eax
	sarl	%cl, %eax
	movl	%esi, %ecx
	sall	%cl, %edx
	orl	%edx, %eax
	movb	%al, -73(%rbp)
.L77:
	movl	-80(%rbp), %r13d
	movl	-76(%rbp), %eax
.L49:
	cmpq	%r15, %r8
	jbe	.L52
.L111:
	movl	%r13d, %r9d
.L28:
	movq	-168(%rbp), %rbx
	movl	%r9d, (%rbx)
	movl	%eax, 4(%rbx)
.L23:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L114
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	cmpq	%r13, %r15
	jb	.L28
	movl	%r10d, %ecx
	leaq	-88(%rbp), %rsi
	movl	%r10d, -160(%rbp)
	movq	%r13, %r8
	andl	$7, %ecx
	testl	%r10d, %r10d
	movq	%rsi, -136(%rbp)
	movl	%r9d, %r13d
	cmovns	%r10d, %edx
	movl	%ecx, -104(%rbp)
	leaq	-80(%rbp), %rcx
	movq	%rcx, -184(%rbp)
	sarl	$3, %edx
	movslq	%edx, %rdx
	addq	%rcx, %rdx
	movq	%rdx, -176(%rbp)
	movl	%r10d, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	leal	(%r10,%rdx), %ecx
	andl	$7, %ecx
	movl	%ecx, %edi
	subl	%edx, %edi
	movl	$8, %edx
	subl	%edi, %edx
	movl	%edi, -196(%rbp)
	movl	%edx, -192(%rbp)
	.p2align 4,,10
	.p2align 3
.L76:
	movq	-128(%rbp), %rcx
	movq	-120(%rbp), %rdx
	subq	%r8, %r15
	movl	%eax, -84(%rbp)
	movq	-112(%rbp), %rsi
	movq	-136(%rbp), %rdi
	movl	%eax, -144(%rbp)
	movq	%r8, -152(%rbp)
	movl	%r13d, -88(%rbp)
	call	DES_encrypt3@PLT
	movq	-152(%rbp), %r8
	movl	-144(%rbp), %eax
	addq	%r8, %rbx
	cmpl	$8, -100(%rbp)
	ja	.L93
	leaq	.L55(%rip), %rdi
	movslq	(%rdi,%r8,4), %rdx
	addq	%rdi, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L55:
	.long	.L93-.L55
	.long	.L94-.L55
	.long	.L95-.L55
	.long	.L96-.L55
	.long	.L97-.L55
	.long	.L98-.L55
	.long	.L99-.L55
	.long	.L100-.L55
	.long	.L54-.L55
	.text
	.p2align 4,,10
	.p2align 3
.L100:
	xorl	%ecx, %ecx
.L56:
	movzbl	-1(%rbx), %r10d
	subq	$1, %rbx
	sall	$16, %r10d
	orl	%r10d, %ecx
.L57:
	movzbl	-1(%rbx), %r10d
	leaq	-1(%rbx), %rdx
	sall	$8, %r10d
	orl	%ecx, %r10d
.L58:
	leaq	-1(%rdx), %rbx
	movzbl	-1(%rdx), %edx
	orl	%edx, %r10d
.L59:
	movzbl	-1(%rbx), %ecx
	leaq	-1(%rbx), %rdx
	sall	$24, %ecx
.L60:
	movzbl	-1(%rdx), %r14d
	leaq	-1(%rdx), %rbx
	sall	$16, %r14d
	orl	%r14d, %ecx
.L61:
	movzbl	-1(%rbx), %r14d
	leaq	-1(%rbx), %rdx
	sall	$8, %r14d
	orl	%ecx, %r14d
.L62:
	leaq	-1(%rdx), %rbx
	movzbl	-1(%rdx), %edx
	orl	%edx, %r14d
.L53:
	movl	-160(%rbp), %edi
	addq	%r8, %rbx
	cmpl	$32, %edi
	je	.L101
	cmpl	$64, %edi
	je	.L102
	movl	%eax, -76(%rbp)
	movl	-104(%rbp), %eax
	movl	%r13d, -80(%rbp)
	movl	%r14d, -72(%rbp)
	movl	%r10d, -68(%rbp)
	testl	%eax, %eax
	je	.L115
	movq	-176(%rbp), %rsi
	movq	-184(%rbp), %rdi
	movl	$9, %edx
	movq	%r8, -152(%rbp)
	movl	%r10d, -144(%rbp)
	call	memmove@PLT
	movl	-196(%rbp), %edi
	movl	-192(%rbp), %esi
	movzbl	-79(%rbp), %edx
	movzbl	-80(%rbp), %eax
	movl	%edi, %ecx
	movl	-144(%rbp), %r10d
	movq	-152(%rbp), %r8
	sall	%cl, %eax
	movl	%edx, %r11d
	movl	%esi, %ecx
	sarl	%cl, %r11d
	movl	%edi, %ecx
	orl	%r11d, %eax
	sall	%cl, %edx
	movl	%esi, %ecx
	movb	%al, -80(%rbp)
	movzbl	-78(%rbp), %eax
	movl	%eax, %r11d
	sarl	%cl, %r11d
	movl	%edi, %ecx
	orl	%r11d, %edx
	sall	%cl, %eax
	movl	%esi, %ecx
	movb	%dl, -79(%rbp)
	movzbl	-77(%rbp), %edx
	movl	%edx, %r11d
	sarl	%cl, %r11d
	movl	%edi, %ecx
	orl	%r11d, %eax
	sall	%cl, %edx
	movl	%esi, %ecx
	movb	%al, -78(%rbp)
	movzbl	-76(%rbp), %eax
	movl	%eax, %r11d
	sarl	%cl, %r11d
	movl	%edi, %ecx
	orl	%r11d, %edx
	sall	%cl, %eax
	movl	%esi, %ecx
	movb	%dl, -77(%rbp)
	movzbl	-75(%rbp), %edx
	movl	%edx, %r11d
	sarl	%cl, %r11d
	movl	%edi, %ecx
	orl	%r11d, %eax
	sall	%cl, %edx
	movl	%esi, %ecx
	movb	%al, -76(%rbp)
	movzbl	-74(%rbp), %eax
	movl	%eax, %r11d
	sarl	%cl, %r11d
	movl	%edi, %ecx
	orl	%r11d, %edx
	sall	%cl, %eax
	movl	%esi, %ecx
	movb	%dl, -75(%rbp)
	movzbl	-73(%rbp), %edx
	movl	%edx, %r9d
	sarl	%cl, %r9d
	orl	%r9d, %eax
	movb	%al, -74(%rbp)
	movzbl	-72(%rbp), %eax
	sarl	%cl, %eax
	movl	%edi, %ecx
	sall	%cl, %edx
	orl	%edx, %eax
	movb	%al, -73(%rbp)
.L78:
	movl	-80(%rbp), %r13d
	movl	-76(%rbp), %eax
.L63:
	xorl	-88(%rbp), %r14d
	xorl	-84(%rbp), %r10d
	addq	%r8, %r12
	cmpl	$8, -100(%rbp)
	ja	.L66
	leaq	.L68(%rip), %rsi
	movslq	(%rsi,%r8,4), %rdx
	addq	%rsi, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L68:
	.long	.L66-.L68
	.long	.L103-.L68
	.long	.L74-.L68
	.long	.L104-.L68
	.long	.L72-.L68
	.long	.L105-.L68
	.long	.L70-.L68
	.long	.L69-.L68
	.long	.L67-.L68
	.text
	.p2align 4,,10
	.p2align 3
.L67:
	movl	%r10d, %edx
	subq	$1, %r12
	shrl	$24, %edx
	movb	%dl, (%r12)
.L69:
	movl	%r10d, %edx
	subq	$1, %r12
	shrl	$16, %edx
	movb	%dl, (%r12)
.L70:
	movl	%r10d, %ecx
	leaq	-1(%r12), %rdx
	movzbl	%ch, %ecx
	movb	%cl, -1(%r12)
.L71:
	movb	%r10b, -1(%rdx)
	leaq	-1(%rdx), %r12
.L72:
	movl	%r14d, %ecx
	leaq	-1(%r12), %rdx
	shrl	$24, %ecx
	movb	%cl, -1(%r12)
.L73:
	movl	%r14d, %ecx
	leaq	-1(%rdx), %r12
	shrl	$16, %ecx
	movb	%cl, -1(%rdx)
.L74:
	movl	%r14d, %ecx
	leaq	-1(%r12), %rdx
	movzbl	%ch, %ecx
	movb	%cl, -1(%r12)
.L75:
	movb	%r14b, -1(%rdx)
	leaq	-1(%rdx), %r12
.L66:
	addq	%r8, %r12
	cmpq	%r15, %r8
	jbe	.L76
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L104:
	movq	%r12, %rdx
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L105:
	movq	%r12, %rdx
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L99:
	xorl	%ecx, %ecx
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L98:
	movq	%rbx, %rdx
	xorl	%r10d, %r10d
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L103:
	movq	%r12, %rdx
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L97:
	xorl	%r10d, %r10d
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L96:
	movq	%rbx, %rdx
	xorl	%r10d, %r10d
	xorl	%ecx, %ecx
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L95:
	xorl	%r10d, %r10d
	xorl	%ecx, %ecx
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L94:
	movq	%rbx, %rdx
	xorl	%r10d, %r10d
	xorl	%r14d, %r14d
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L54:
	movzbl	-1(%rbx), %ecx
	subq	$1, %rbx
	sall	$24, %ecx
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L87:
	xorl	%esi, %esi
.L32:
	movzbl	-1(%rbx), %ecx
	subq	$1, %rbx
	sall	$16, %ecx
	orl	%ecx, %esi
.L33:
	movzbl	-1(%rbx), %ecx
	leaq	-1(%rbx), %rdx
	sall	$8, %ecx
	orl	%esi, %ecx
.L34:
	leaq	-1(%rdx), %rbx
	movzbl	-1(%rdx), %edx
	orl	%edx, %ecx
.L35:
	movzbl	-1(%rbx), %edi
	leaq	-1(%rbx), %rsi
	sall	$24, %edi
.L36:
	movzbl	-1(%rsi), %edx
	leaq	-1(%rsi), %rbx
	sall	$16, %edx
	orl	%edx, %edi
.L37:
	movzbl	-1(%rbx), %edx
	leaq	-1(%rbx), %rsi
	sall	$8, %edx
	orl	%edi, %edx
.L38:
	leaq	-1(%rsi), %rbx
	movzbl	-1(%rsi), %esi
	orl	%esi, %edx
.L29:
	addq	%r8, %rbx
	xorl	-88(%rbp), %edx
	xorl	-84(%rbp), %ecx
	addq	%r8, %r12
	cmpl	$8, -100(%rbp)
	movq	%rbx, -144(%rbp)
	ja	.L39
	leaq	.L41(%rip), %rdi
	movslq	(%rdi,%r8,4), %rsi
	addq	%rdi, %rsi
	notrack jmp	*%rsi
	.section	.rodata
	.align 4
	.align 4
.L41:
	.long	.L39-.L41
	.long	.L88-.L41
	.long	.L47-.L41
	.long	.L89-.L41
	.long	.L45-.L41
	.long	.L90-.L41
	.long	.L43-.L41
	.long	.L42-.L41
	.long	.L40-.L41
	.text
	.p2align 4,,10
	.p2align 3
.L88:
	movq	%r12, %rsi
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L90:
	movq	%r12, %rsi
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L86:
	xorl	%esi, %esi
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L81:
	movq	%rbx, %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L85:
	movq	%rbx, %rdx
	xorl	%ecx, %ecx
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L84:
	xorl	%ecx, %ecx
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L83:
	movq	%rbx, %rsi
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L82:
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L89:
	movq	%r12, %rsi
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L30:
	movzbl	-1(%rbx), %esi
	subq	$1, %rbx
	sall	$24, %esi
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L101:
	movl	%eax, %r13d
	movl	%r14d, %eax
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L91:
	movl	%eax, %r13d
	movl	%edx, %eax
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L92:
	movl	%ecx, %eax
	movl	%edx, %r13d
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L102:
	movl	%r10d, %eax
	movl	%r14d, %r13d
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L113:
	movq	-192(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -80(%rbp)
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L115:
	movq	-176(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -80(%rbp)
	jmp	.L78
.L114:
	call	__stack_chk_fail@PLT
.L93:
	xorl	%r10d, %r10d
	xorl	%r14d, %r14d
	jmp	.L53
.L80:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	jmp	.L29
	.cfi_endproc
.LFE55:
	.size	DES_ede3_cfb_encrypt, .-DES_ede3_cfb_encrypt
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
