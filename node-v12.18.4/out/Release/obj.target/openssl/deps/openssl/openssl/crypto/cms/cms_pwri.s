	.file	"cms_pwri.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/cms/cms_pwri.c"
	.text
	.p2align 4
	.globl	CMS_RecipientInfo_set0_password
	.type	CMS_RecipientInfo_set0_password, @function
CMS_RecipientInfo_set0_password:
.LFB1464:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	cmpl	$3, (%rdi)
	jne	.L13
	movq	8(%rdi), %rbx
	movq	%rsi, 32(%rbx)
	testq	%rsi, %rsi
	je	.L6
	testq	%rdx, %rdx
	jns	.L6
	movq	%rsi, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
.L6:
	movq	%rdx, 40(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	movl	$26, %r8d
	movl	$177, %edx
	movl	$168, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1464:
	.size	CMS_RecipientInfo_set0_password, .-CMS_RecipientInfo_set0_password
	.p2align 4
	.globl	CMS_add0_recipient_password
	.type	CMS_add0_recipient_password, @function
CMS_add0_recipient_password:
.LFB1465:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%r8, -88(%rbp)
	movq	16(%rbp), %r15
	movq	%r9, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	cms_get0_enveloped@PLT
	testq	%rax, %rax
	je	.L36
	movq	%rax, %rbx
	testl	%r12d, %r12d
	jle	.L70
	testq	%r15, %r15
	je	.L71
.L19:
	cmpl	$893, %r12d
	je	.L17
	movl	$72, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$179, %edx
	xorl	%r13d, %r13d
	movl	$165, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
.L14:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L72
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	testq	%r15, %r15
	jne	.L17
	movq	24(%rax), %rax
	movq	24(%rax), %r15
	testq	%r15, %r15
	je	.L18
.L17:
	call	X509_ALGOR_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L20
	call	EVP_CIPHER_CTX_new@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	EVP_EncryptInit_ex@PLT
	movl	$85, %r8d
	movl	$6, %edx
	leaq	.LC0(%rip), %rcx
	testl	%eax, %eax
	jle	.L68
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_iv_length@PLT
	movl	%eax, %esi
	testl	%eax, %eax
	jle	.L23
	leaq	-80(%rbp), %r15
	movq	%r15, %rdi
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jle	.L69
	movq	%r15, %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	EVP_EncryptInit_ex@PLT
	movl	$95, %r8d
	movl	$6, %edx
	leaq	.LC0(%rip), %rcx
	testl	%eax, %eax
	jle	.L68
	call	ASN1_TYPE_new@PLT
	movl	$100, %r8d
	movl	$65, %edx
	leaq	.LC0(%rip), %rcx
	movq	%rax, 8(%r12)
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L68
	movq	%r13, %rdi
	call	EVP_CIPHER_param_to_asn1@PLT
	testl	%eax, %eax
	jle	.L73
.L23:
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_type@PLT
	movl	%eax, %edi
	call	OBJ_nid2obj@PLT
	movq	%r13, %rdi
	movq	%rax, (%r12)
	call	EVP_CIPHER_CTX_free@PLT
	leaq	CMS_RecipientInfo_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L20
	leaq	CMS_PasswordRecipientInfo_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, 8(%r13)
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L27
	movl	$3, 0(%r13)
	movq	16(%rax), %rdi
	call	X509_ALGOR_free@PLT
	call	X509_ALGOR_new@PLT
	testq	%rax, %rax
	movq	%rax, 16(%r15)
	movq	%rax, -104(%rbp)
	je	.L27
	movl	$893, %edi
	call	OBJ_nid2obj@PLT
	movq	-104(%rbp), %rdx
	movq	%rax, (%rdx)
	movq	16(%r15), %rdx
	movq	%rdx, -104(%rbp)
	call	ASN1_TYPE_new@PLT
	movq	-104(%rbp), %rdx
	movq	%rax, 8(%rdx)
	movq	16(%r15), %rax
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L27
	addq	$8, %rdx
	leaq	X509_ALGOR_it(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_item_pack@PLT
	testq	%rax, %rax
	je	.L27
	movq	16(%r15), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rax
	movl	$16, (%rax)
	call	X509_ALGOR_free@PLT
	movl	$-1, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$-1, %ecx
	movl	%r14d, %edi
	call	PKCS5_pbkdf2_set@PLT
	movq	%rax, 8(%r15)
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L67
	cmpl	$3, 0(%r13)
	jne	.L74
	movq	8(%r13), %r12
	movq	-88(%rbp), %rax
	movq	%rax, 32(%r12)
	testq	%rax, %rax
	je	.L38
	cmpq	$0, -96(%rbp)
	jns	.L38
	movq	%rax, %rdi
	call	strlen@PLT
.L33:
	movq	%rax, 40(%r12)
.L30:
	movl	$0, (%r15)
	movq	16(%rbx), %rdi
	movq	%r13, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L14
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L27:
	movl	$161, %r8d
	movl	$65, %edx
	movl	$165, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L67:
	xorl	%edi, %edi
	call	EVP_CIPHER_CTX_free@PLT
	leaq	CMS_RecipientInfo_it(%rip), %rsi
	movq	%r13, %rdi
	call	ASN1_item_free@PLT
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L71:
	movq	24(%rax), %rax
	movq	24(%rax), %r15
	testq	%r15, %r15
	jne	.L19
.L18:
	movl	$68, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$126, %edx
	xorl	%r13d, %r13d
	movl	$165, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L73:
	movl	$104, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$102, %edx
	.p2align 4,,10
	.p2align 3
.L68:
	movl	$165, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
.L69:
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_free@PLT
.L22:
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	X509_ALGOR_free@PLT
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L36:
	xorl	%r13d, %r13d
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L38:
	movq	-96(%rbp), %rax
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L74:
	movl	$26, %r8d
	movl	$177, %edx
	movl	$168, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$161, %r8d
	movl	$65, %edx
	movl	$165, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%edi, %edi
	call	EVP_CIPHER_CTX_free@PLT
	jmp	.L22
.L72:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1465:
	.size	CMS_add0_recipient_password, .-CMS_add0_recipient_password
	.p2align 4
	.globl	cms_RecipientInfo_pwri_crypt
	.type	cms_RecipientInfo_pwri_crypt, @function
cms_RecipientInfo_pwri_crypt:
.LFB1468:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rsi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 32(%r15)
	je	.L130
	movq	16(%r15), %r12
	testq	%r12, %r12
	je	.L79
	movq	8(%rdi), %rax
	movq	(%r12), %rdi
	movl	%edx, %r14d
	movq	24(%rax), %rbx
	call	OBJ_obj2nid@PLT
	cmpl	$893, %eax
	je	.L131
.L79:
	movl	$299, %r8d
	movl	$179, %edx
	movl	$167, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L75:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L132
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L131:
	.cfi_restore_state
	movq	8(%r12), %rsi
	leaq	X509_ALGOR_it(%rip), %rdi
	call	ASN1_TYPE_unpack_sequence@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L133
	movq	(%rax), %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_cipherbyname@PLT
	testq	%rax, %rax
	je	.L134
	movq	%rax, -72(%rbp)
	call	EVP_CIPHER_CTX_new@PLT
	movq	-72(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L135
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%r14d, %r9d
	movq	%rax, %rdi
	call	EVP_CipherInit_ex@PLT
	testl	%eax, %eax
	je	.L83
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_set_padding@PLT
	movq	8(%r12), %rsi
	movq	%r13, %rdi
	call	EVP_CIPHER_asn1_to_param@PLT
	movl	$330, %r8d
	movl	$102, %edx
	leaq	.LC0(%rip), %rcx
	testl	%eax, %eax
	jle	.L129
	movq	8(%r15), %rax
	movq	32(%r15), %rsi
	movl	%r14d, %r9d
	movq	%r13, %r8
	movl	40(%r15), %edx
	movq	8(%rax), %rcx
	movq	(%rax), %rdi
	call	EVP_PBE_CipherInit@PLT
	testl	%eax, %eax
	js	.L136
	testl	%r14d, %r14d
	je	.L87
	movq	%r13, %rdi
	movq	40(%rbx), %r14
	call	EVP_CIPHER_CTX_block_size@PLT
	xorl	%edx, %edx
	movslq	%eax, %rcx
	leaq	3(%r14,%rcx), %rax
	divq	%rcx
	imulq	%rcx, %rax
	addq	%rcx, %rcx
	movq	%rax, %rdi
	cmpq	%rcx, %rax
	jb	.L83
	cmpq	$255, %r14
	ja	.L83
	movl	$353, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L128
	movq	40(%rbx), %r8
	movq	32(%rbx), %rsi
	movq	%r13, %rdi
	movq	%r8, -72(%rbp)
	movq	%rsi, -80(%rbp)
	call	EVP_CIPHER_CTX_block_size@PLT
	movq	-72(%rbp), %r8
	xorl	%edx, %edx
	movslq	%eax, %rcx
	leaq	3(%r8,%rcx), %rax
	divq	%rcx
	imulq	%rcx, %rax
	addq	%rcx, %rcx
	movq	%rax, %rbx
	cmpq	%rcx, %rax
	jb	.L128
	cmpq	$255, %r8
	ja	.L128
	movq	-80(%rbp), %rsi
	movb	%r8b, (%r14)
	movq	%r8, %rdx
	leaq	4(%r14), %rdi
	movzbl	(%rsi), %eax
	notl	%eax
	movb	%al, 1(%r14)
	movzbl	1(%rsi), %eax
	notl	%eax
	movb	%al, 2(%r14)
	movzbl	2(%rsi), %eax
	notl	%eax
	movb	%al, 3(%r14)
	call	memcpy@PLT
	movq	-72(%rbp), %r8
	leaq	4(%r8), %rax
	cmpq	%rax, %rbx
	jbe	.L95
	movl	%ebx, %edx
	leaq	(%r14,%rax), %rdi
	subl	%r8d, %edx
	leal	-4(%rdx), %esi
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jle	.L128
.L95:
	leaq	-60(%rbp), %rdx
	movl	%ebx, %r8d
	movq	%r14, %rcx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rdx, -72(%rbp)
	call	EVP_EncryptUpdate@PLT
	testl	%eax, %eax
	je	.L128
	movq	-72(%rbp), %rdx
	movl	%ebx, %r8d
	movq	%r14, %rcx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	EVP_EncryptUpdate@PLT
	testl	%eax, %eax
	je	.L128
	movq	24(%r15), %rax
	movq	%r13, %rdi
	movq	%r14, 8(%rax)
	movl	%ebx, (%rax)
	call	EVP_CIPHER_CTX_free@PLT
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L96:
	movq	%r12, %rdi
	movl	%eax, -72(%rbp)
	call	X509_ALGOR_free@PLT
	movl	-72(%rbp), %eax
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L130:
	movl	$293, %r8d
	movl	$178, %edx
	movl	$167, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L83:
	movq	%r13, %rdi
	xorl	%r14d, %r14d
	call	EVP_CIPHER_CTX_free@PLT
.L85:
	movl	$389, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	xorl	%eax, %eax
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L133:
	movl	$308, %r8d
	movl	$176, %edx
	movl	$167, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L75
.L136:
	movl	$342, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$6, %edx
.L129:
	movl	$167, %esi
	movl	$46, %edi
	xorl	%r14d, %r14d
	call	ERR_put_error@PLT
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_free@PLT
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L134:
	movl	$316, %r8d
	movl	$148, %edx
	movl	$167, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L135:
	movl	$322, %r8d
	movl	$65, %edx
	movl	$167, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L87:
	movq	24(%r15), %rax
	movl	$363, %edx
	leaq	.LC0(%rip), %rsi
	movslq	(%rax), %rdi
	call	CRYPTO_malloc@PLT
	movl	$366, %r8d
	movl	$65, %edx
	leaq	.LC0(%rip), %rcx
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L127
	movq	24(%r15), %rax
	movq	%r13, %rdi
	movslq	(%rax), %r15
	movq	8(%rax), %rax
	movl	%r15d, -92(%rbp)
	movq	%rax, -72(%rbp)
	call	EVP_CIPHER_CTX_block_size@PLT
	movslq	%eax, %r11
	leaq	(%r11,%r11), %rcx
	movl	%r11d, -88(%rbp)
	cmpq	%rcx, %r15
	movq	%rcx, -80(%rbp)
	jb	.L99
	movq	%r15, %rax
	xorl	%edx, %edx
	movq	%r11, -104(%rbp)
	divq	%r11
	testq	%rdx, %rdx
	jne	.L99
	movl	$191, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	call	CRYPTO_malloc@PLT
	movq	-104(%rbp), %r11
	movq	-80(%rbp), %rcx
	testq	%rax, %rax
	movl	-88(%rbp), %r10d
	movq	%rax, %r9
	je	.L137
	movq	%r15, %rax
	leaq	-60(%rbp), %rdx
	leal	(%r10,%r10), %r8d
	movq	%r13, %rdi
	subq	%rcx, %rax
	movq	-72(%rbp), %rcx
	movq	%r11, -112(%rbp)
	leaq	(%r9,%rax), %rsi
	movq	%r9, -104(%rbp)
	addq	%rax, %rcx
	movl	%r10d, -88(%rbp)
	movq	%rax, -120(%rbp)
	movq	%rdx, -80(%rbp)
	call	EVP_DecryptUpdate@PLT
	movl	-88(%rbp), %r10d
	movq	-104(%rbp), %r9
	testl	%eax, %eax
	movq	-112(%rbp), %r11
	je	.L103
	movq	-120(%rbp), %rcx
	movq	-80(%rbp), %rdx
	movq	%r9, %rsi
	movl	%r10d, %r8d
	movq	%r13, %rdi
	movq	%r9, -88(%rbp)
	addq	%r11, %rcx
	movl	%r10d, -104(%rbp)
	addq	%r9, %rcx
	call	EVP_DecryptUpdate@PLT
	movq	-88(%rbp), %r9
	testl	%eax, %eax
	je	.L103
	movl	-92(%rbp), %r8d
	movl	-104(%rbp), %r10d
	movq	%r9, %rsi
	movq	%r13, %rdi
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %rdx
	movq	%r9, -72(%rbp)
	subl	%r10d, %r8d
	call	EVP_DecryptUpdate@PLT
	movq	-72(%rbp), %r9
	testl	%eax, %eax
	je	.L103
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%r9, -72(%rbp)
	call	EVP_DecryptInit_ex@PLT
	movq	-72(%rbp), %r9
	testl	%eax, %eax
	je	.L103
	movl	-92(%rbp), %r8d
	movq	-80(%rbp), %rdx
	movq	%r9, %rcx
	movq	%r9, %rsi
	movq	%r13, %rdi
	movq	%r9, -72(%rbp)
	call	EVP_DecryptUpdate@PLT
	movq	-72(%rbp), %r9
	testl	%eax, %eax
	je	.L103
	movzbl	1(%r9), %eax
	movzbl	2(%r9), %edx
	xorb	4(%r9), %al
	xorb	5(%r9), %dl
	andl	%edx, %eax
	movzbl	3(%r9), %edx
	xorb	6(%r9), %dl
	andl	%edx, %eax
	addb	$1, %al
	jne	.L103
	movzbl	(%r9), %edx
	movq	%rdx, %rax
	subl	$4, %edx
	movslq	%edx, %rdx
	cmpq	%rdx, %r15
	jnb	.L138
.L103:
	movl	$226, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r15, %rsi
	movq	%r9, %rdi
	call	CRYPTO_clear_free@PLT
.L99:
	movl	$372, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$180, %edx
.L127:
	movl	$167, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
.L128:
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_free@PLT
	jmp	.L85
.L138:
	leaq	4(%r9), %rsi
	cmpl	$8, %eax
	jnb	.L109
	testb	$4, %al
	jne	.L139
	testl	%eax, %eax
	je	.L110
	movzbl	4(%r9), %edx
	movb	%dl, (%r14)
	testb	$2, %al
	jne	.L140
.L110:
	movq	%r9, %rdi
	movl	$226, %ecx
	movq	%r15, %rsi
	movq	%rax, -72(%rbp)
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movq	40(%rbx), %rsi
	movq	32(%rbx), %rdi
	movl	$376, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movq	-72(%rbp), %rax
	movq	%r14, 32(%rbx)
	movq	%r13, %rdi
	movq	%rax, 40(%rbx)
	call	EVP_CIPHER_CTX_free@PLT
	movl	$1, %eax
	jmp	.L96
.L109:
	movq	4(%r9), %rdx
	leaq	8(%r14), %rdi
	andq	$-8, %rdi
	movq	%rdx, (%r14)
	movq	-8(%rsi,%rax), %rdx
	movq	%rdx, -8(%r14,%rax)
	movq	%r14, %rdx
	subq	%rdi, %rdx
	subq	%rdx, %rsi
	addl	%eax, %edx
	shrl	$3, %edx
	movl	%edx, %ecx
	rep movsq
	jmp	.L110
.L137:
	movl	$192, %r8d
	movl	$65, %edx
	movl	$180, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L99
.L139:
	movl	4(%r9), %edx
	movl	%edx, (%r14)
	movl	-4(%rsi,%rax), %edx
	movl	%edx, -4(%r14,%rax)
	jmp	.L110
.L132:
	call	__stack_chk_fail@PLT
.L140:
	movzwl	-2(%rsi,%rax), %edx
	movw	%dx, -2(%r14,%rax)
	jmp	.L110
	.cfi_endproc
.LFE1468:
	.size	cms_RecipientInfo_pwri_crypt, .-cms_RecipientInfo_pwri_crypt
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
