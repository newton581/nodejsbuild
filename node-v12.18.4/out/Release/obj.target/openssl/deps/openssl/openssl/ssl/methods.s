	.file	"methods.c"
	.text
	.p2align 4
	.globl	TLS_method
	.type	TLS_method, @function
TLS_method:
.LFB964:
	.cfi_startproc
	endbr64
	leaq	TLS_method_data.23101(%rip), %rax
	ret
	.cfi_endproc
.LFE964:
	.size	TLS_method, .-TLS_method
	.p2align 4
	.globl	tlsv1_3_method
	.type	tlsv1_3_method, @function
tlsv1_3_method:
.LFB965:
	.cfi_startproc
	endbr64
	leaq	tlsv1_3_method_data.23105(%rip), %rax
	ret
	.cfi_endproc
.LFE965:
	.size	tlsv1_3_method, .-tlsv1_3_method
	.p2align 4
	.globl	tlsv1_2_method
	.type	tlsv1_2_method, @function
tlsv1_2_method:
.LFB1017:
	.cfi_startproc
	endbr64
	leaq	tlsv1_2_method_data.23109(%rip), %rax
	ret
	.cfi_endproc
.LFE1017:
	.size	tlsv1_2_method, .-tlsv1_2_method
	.p2align 4
	.globl	tlsv1_1_method
	.type	tlsv1_1_method, @function
tlsv1_1_method:
.LFB1011:
	.cfi_startproc
	endbr64
	leaq	tlsv1_1_method_data.23113(%rip), %rax
	ret
	.cfi_endproc
.LFE1011:
	.size	tlsv1_1_method, .-tlsv1_1_method
	.p2align 4
	.globl	tlsv1_method
	.type	tlsv1_method, @function
tlsv1_method:
.LFB1005:
	.cfi_startproc
	endbr64
	leaq	tlsv1_method_data.23117(%rip), %rax
	ret
	.cfi_endproc
.LFE1005:
	.size	tlsv1_method, .-tlsv1_method
	.p2align 4
	.globl	TLS_server_method
	.type	TLS_server_method, @function
TLS_server_method:
.LFB969:
	.cfi_startproc
	endbr64
	leaq	TLS_server_method_data.23121(%rip), %rax
	ret
	.cfi_endproc
.LFE969:
	.size	TLS_server_method, .-TLS_server_method
	.p2align 4
	.globl	tlsv1_3_server_method
	.type	tlsv1_3_server_method, @function
tlsv1_3_server_method:
.LFB970:
	.cfi_startproc
	endbr64
	leaq	tlsv1_3_server_method_data.23125(%rip), %rax
	ret
	.cfi_endproc
.LFE970:
	.size	tlsv1_3_server_method, .-tlsv1_3_server_method
	.p2align 4
	.globl	tlsv1_2_server_method
	.type	tlsv1_2_server_method, @function
tlsv1_2_server_method:
.LFB1019:
	.cfi_startproc
	endbr64
	leaq	tlsv1_2_server_method_data.23129(%rip), %rax
	ret
	.cfi_endproc
.LFE1019:
	.size	tlsv1_2_server_method, .-tlsv1_2_server_method
	.p2align 4
	.globl	tlsv1_1_server_method
	.type	tlsv1_1_server_method, @function
tlsv1_1_server_method:
.LFB1013:
	.cfi_startproc
	endbr64
	leaq	tlsv1_1_server_method_data.23133(%rip), %rax
	ret
	.cfi_endproc
.LFE1013:
	.size	tlsv1_1_server_method, .-tlsv1_1_server_method
	.p2align 4
	.globl	tlsv1_server_method
	.type	tlsv1_server_method, @function
tlsv1_server_method:
.LFB1007:
	.cfi_startproc
	endbr64
	leaq	tlsv1_server_method_data.23137(%rip), %rax
	ret
	.cfi_endproc
.LFE1007:
	.size	tlsv1_server_method, .-tlsv1_server_method
	.p2align 4
	.globl	TLS_client_method
	.type	TLS_client_method, @function
TLS_client_method:
.LFB974:
	.cfi_startproc
	endbr64
	leaq	TLS_client_method_data.23141(%rip), %rax
	ret
	.cfi_endproc
.LFE974:
	.size	TLS_client_method, .-TLS_client_method
	.p2align 4
	.globl	tlsv1_3_client_method
	.type	tlsv1_3_client_method, @function
tlsv1_3_client_method:
.LFB975:
	.cfi_startproc
	endbr64
	leaq	tlsv1_3_client_method_data.23145(%rip), %rax
	ret
	.cfi_endproc
.LFE975:
	.size	tlsv1_3_client_method, .-tlsv1_3_client_method
	.p2align 4
	.globl	tlsv1_2_client_method
	.type	tlsv1_2_client_method, @function
tlsv1_2_client_method:
.LFB1021:
	.cfi_startproc
	endbr64
	leaq	tlsv1_2_client_method_data.23149(%rip), %rax
	ret
	.cfi_endproc
.LFE1021:
	.size	tlsv1_2_client_method, .-tlsv1_2_client_method
	.p2align 4
	.globl	tlsv1_1_client_method
	.type	tlsv1_1_client_method, @function
tlsv1_1_client_method:
.LFB1015:
	.cfi_startproc
	endbr64
	leaq	tlsv1_1_client_method_data.23153(%rip), %rax
	ret
	.cfi_endproc
.LFE1015:
	.size	tlsv1_1_client_method, .-tlsv1_1_client_method
	.p2align 4
	.globl	tlsv1_client_method
	.type	tlsv1_client_method, @function
tlsv1_client_method:
.LFB1009:
	.cfi_startproc
	endbr64
	leaq	tlsv1_client_method_data.23157(%rip), %rax
	ret
	.cfi_endproc
.LFE1009:
	.size	tlsv1_client_method, .-tlsv1_client_method
	.p2align 4
	.globl	dtlsv1_method
	.type	dtlsv1_method, @function
dtlsv1_method:
.LFB1023:
	.cfi_startproc
	endbr64
	leaq	dtlsv1_method_data.23161(%rip), %rax
	ret
	.cfi_endproc
.LFE1023:
	.size	dtlsv1_method, .-dtlsv1_method
	.p2align 4
	.globl	dtlsv1_2_method
	.type	dtlsv1_2_method, @function
dtlsv1_2_method:
.LFB1029:
	.cfi_startproc
	endbr64
	leaq	dtlsv1_2_method_data.23165(%rip), %rax
	ret
	.cfi_endproc
.LFE1029:
	.size	dtlsv1_2_method, .-dtlsv1_2_method
	.p2align 4
	.globl	DTLS_method
	.type	DTLS_method, @function
DTLS_method:
.LFB981:
	.cfi_startproc
	endbr64
	leaq	DTLS_method_data.23169(%rip), %rax
	ret
	.cfi_endproc
.LFE981:
	.size	DTLS_method, .-DTLS_method
	.p2align 4
	.globl	dtlsv1_server_method
	.type	dtlsv1_server_method, @function
dtlsv1_server_method:
.LFB1025:
	.cfi_startproc
	endbr64
	leaq	dtlsv1_server_method_data.23173(%rip), %rax
	ret
	.cfi_endproc
.LFE1025:
	.size	dtlsv1_server_method, .-dtlsv1_server_method
	.p2align 4
	.globl	dtlsv1_2_server_method
	.type	dtlsv1_2_server_method, @function
dtlsv1_2_server_method:
.LFB1031:
	.cfi_startproc
	endbr64
	leaq	dtlsv1_2_server_method_data.23177(%rip), %rax
	ret
	.cfi_endproc
.LFE1031:
	.size	dtlsv1_2_server_method, .-dtlsv1_2_server_method
	.p2align 4
	.globl	DTLS_server_method
	.type	DTLS_server_method, @function
DTLS_server_method:
.LFB984:
	.cfi_startproc
	endbr64
	leaq	DTLS_server_method_data.23181(%rip), %rax
	ret
	.cfi_endproc
.LFE984:
	.size	DTLS_server_method, .-DTLS_server_method
	.p2align 4
	.globl	dtlsv1_client_method
	.type	dtlsv1_client_method, @function
dtlsv1_client_method:
.LFB1027:
	.cfi_startproc
	endbr64
	leaq	dtlsv1_client_method_data.23185(%rip), %rax
	ret
	.cfi_endproc
.LFE1027:
	.size	dtlsv1_client_method, .-dtlsv1_client_method
	.p2align 4
	.globl	dtls_bad_ver_client_method
	.type	dtls_bad_ver_client_method, @function
dtls_bad_ver_client_method:
.LFB986:
	.cfi_startproc
	endbr64
	leaq	dtls_bad_ver_client_method_data.23189(%rip), %rax
	ret
	.cfi_endproc
.LFE986:
	.size	dtls_bad_ver_client_method, .-dtls_bad_ver_client_method
	.p2align 4
	.globl	dtlsv1_2_client_method
	.type	dtlsv1_2_client_method, @function
dtlsv1_2_client_method:
.LFB1033:
	.cfi_startproc
	endbr64
	leaq	dtlsv1_2_client_method_data.23193(%rip), %rax
	ret
	.cfi_endproc
.LFE1033:
	.size	dtlsv1_2_client_method, .-dtlsv1_2_client_method
	.p2align 4
	.globl	DTLS_client_method
	.type	DTLS_client_method, @function
DTLS_client_method:
.LFB988:
	.cfi_startproc
	endbr64
	leaq	DTLS_client_method_data.23197(%rip), %rax
	ret
	.cfi_endproc
.LFE988:
	.size	DTLS_client_method, .-DTLS_client_method
	.p2align 4
	.globl	TLSv1_2_method
	.type	TLSv1_2_method, @function
TLSv1_2_method:
.LFB989:
	.cfi_startproc
	endbr64
	leaq	tlsv1_2_method_data.23109(%rip), %rax
	ret
	.cfi_endproc
.LFE989:
	.size	TLSv1_2_method, .-TLSv1_2_method
	.p2align 4
	.globl	TLSv1_2_server_method
	.type	TLSv1_2_server_method, @function
TLSv1_2_server_method:
.LFB990:
	.cfi_startproc
	endbr64
	leaq	tlsv1_2_server_method_data.23129(%rip), %rax
	ret
	.cfi_endproc
.LFE990:
	.size	TLSv1_2_server_method, .-TLSv1_2_server_method
	.p2align 4
	.globl	TLSv1_2_client_method
	.type	TLSv1_2_client_method, @function
TLSv1_2_client_method:
.LFB991:
	.cfi_startproc
	endbr64
	leaq	tlsv1_2_client_method_data.23149(%rip), %rax
	ret
	.cfi_endproc
.LFE991:
	.size	TLSv1_2_client_method, .-TLSv1_2_client_method
	.p2align 4
	.globl	TLSv1_1_method
	.type	TLSv1_1_method, @function
TLSv1_1_method:
.LFB992:
	.cfi_startproc
	endbr64
	leaq	tlsv1_1_method_data.23113(%rip), %rax
	ret
	.cfi_endproc
.LFE992:
	.size	TLSv1_1_method, .-TLSv1_1_method
	.p2align 4
	.globl	TLSv1_1_server_method
	.type	TLSv1_1_server_method, @function
TLSv1_1_server_method:
.LFB993:
	.cfi_startproc
	endbr64
	leaq	tlsv1_1_server_method_data.23133(%rip), %rax
	ret
	.cfi_endproc
.LFE993:
	.size	TLSv1_1_server_method, .-TLSv1_1_server_method
	.p2align 4
	.globl	TLSv1_1_client_method
	.type	TLSv1_1_client_method, @function
TLSv1_1_client_method:
.LFB994:
	.cfi_startproc
	endbr64
	leaq	tlsv1_1_client_method_data.23153(%rip), %rax
	ret
	.cfi_endproc
.LFE994:
	.size	TLSv1_1_client_method, .-TLSv1_1_client_method
	.p2align 4
	.globl	TLSv1_method
	.type	TLSv1_method, @function
TLSv1_method:
.LFB995:
	.cfi_startproc
	endbr64
	leaq	tlsv1_method_data.23117(%rip), %rax
	ret
	.cfi_endproc
.LFE995:
	.size	TLSv1_method, .-TLSv1_method
	.p2align 4
	.globl	TLSv1_server_method
	.type	TLSv1_server_method, @function
TLSv1_server_method:
.LFB996:
	.cfi_startproc
	endbr64
	leaq	tlsv1_server_method_data.23137(%rip), %rax
	ret
	.cfi_endproc
.LFE996:
	.size	TLSv1_server_method, .-TLSv1_server_method
	.p2align 4
	.globl	TLSv1_client_method
	.type	TLSv1_client_method, @function
TLSv1_client_method:
.LFB997:
	.cfi_startproc
	endbr64
	leaq	tlsv1_client_method_data.23157(%rip), %rax
	ret
	.cfi_endproc
.LFE997:
	.size	TLSv1_client_method, .-TLSv1_client_method
	.p2align 4
	.globl	DTLSv1_2_method
	.type	DTLSv1_2_method, @function
DTLSv1_2_method:
.LFB998:
	.cfi_startproc
	endbr64
	leaq	dtlsv1_2_method_data.23165(%rip), %rax
	ret
	.cfi_endproc
.LFE998:
	.size	DTLSv1_2_method, .-DTLSv1_2_method
	.p2align 4
	.globl	DTLSv1_2_server_method
	.type	DTLSv1_2_server_method, @function
DTLSv1_2_server_method:
.LFB999:
	.cfi_startproc
	endbr64
	leaq	dtlsv1_2_server_method_data.23177(%rip), %rax
	ret
	.cfi_endproc
.LFE999:
	.size	DTLSv1_2_server_method, .-DTLSv1_2_server_method
	.p2align 4
	.globl	DTLSv1_2_client_method
	.type	DTLSv1_2_client_method, @function
DTLSv1_2_client_method:
.LFB1000:
	.cfi_startproc
	endbr64
	leaq	dtlsv1_2_client_method_data.23193(%rip), %rax
	ret
	.cfi_endproc
.LFE1000:
	.size	DTLSv1_2_client_method, .-DTLSv1_2_client_method
	.p2align 4
	.globl	DTLSv1_method
	.type	DTLSv1_method, @function
DTLSv1_method:
.LFB1001:
	.cfi_startproc
	endbr64
	leaq	dtlsv1_method_data.23161(%rip), %rax
	ret
	.cfi_endproc
.LFE1001:
	.size	DTLSv1_method, .-DTLSv1_method
	.p2align 4
	.globl	DTLSv1_server_method
	.type	DTLSv1_server_method, @function
DTLSv1_server_method:
.LFB1002:
	.cfi_startproc
	endbr64
	leaq	dtlsv1_server_method_data.23173(%rip), %rax
	ret
	.cfi_endproc
.LFE1002:
	.size	DTLSv1_server_method, .-DTLSv1_server_method
	.p2align 4
	.globl	DTLSv1_client_method
	.type	DTLSv1_client_method, @function
DTLSv1_client_method:
.LFB1003:
	.cfi_startproc
	endbr64
	leaq	dtlsv1_client_method_data.23185(%rip), %rax
	ret
	.cfi_endproc
.LFE1003:
	.size	DTLSv1_client_method, .-DTLSv1_client_method
	.section	.data.rel.ro,"aw"
	.align 32
	.type	DTLS_client_method_data.23197, @object
	.size	DTLS_client_method_data.23197, 224
DTLS_client_method_data.23197:
	.long	131071
	.long	0
	.quad	0
	.quad	dtls1_new
	.quad	dtls1_clear
	.quad	dtls1_free
	.quad	ssl_undefined_function
	.quad	ossl_statem_connect
	.quad	ssl3_read
	.quad	ssl3_peek
	.quad	ssl3_write
	.quad	dtls1_shutdown
	.quad	ssl3_renegotiate
	.quad	ssl3_renegotiate_check
	.quad	dtls1_read_bytes
	.quad	dtls1_write_app_data_bytes
	.quad	dtls1_dispatch_alert
	.quad	dtls1_ctrl
	.quad	ssl3_ctx_ctrl
	.quad	ssl3_get_cipher_by_char
	.quad	ssl3_put_cipher_by_char
	.quad	ssl3_pending
	.quad	ssl3_num_ciphers
	.quad	ssl3_get_cipher
	.quad	dtls1_default_timeout
	.quad	DTLSv1_2_enc_data
	.quad	ssl_undefined_void_function
	.quad	ssl3_callback_ctrl
	.quad	ssl3_ctx_callback_ctrl
	.align 32
	.type	dtlsv1_2_client_method_data.23193, @object
	.size	dtlsv1_2_client_method_data.23193, 224
dtlsv1_2_client_method_data.23193:
	.long	65277
	.long	0
	.quad	134217728
	.quad	dtls1_new
	.quad	dtls1_clear
	.quad	dtls1_free
	.quad	ssl_undefined_function
	.quad	ossl_statem_connect
	.quad	ssl3_read
	.quad	ssl3_peek
	.quad	ssl3_write
	.quad	dtls1_shutdown
	.quad	ssl3_renegotiate
	.quad	ssl3_renegotiate_check
	.quad	dtls1_read_bytes
	.quad	dtls1_write_app_data_bytes
	.quad	dtls1_dispatch_alert
	.quad	dtls1_ctrl
	.quad	ssl3_ctx_ctrl
	.quad	ssl3_get_cipher_by_char
	.quad	ssl3_put_cipher_by_char
	.quad	ssl3_pending
	.quad	ssl3_num_ciphers
	.quad	ssl3_get_cipher
	.quad	dtls1_default_timeout
	.quad	DTLSv1_2_enc_data
	.quad	ssl_undefined_void_function
	.quad	ssl3_callback_ctrl
	.quad	ssl3_ctx_callback_ctrl
	.align 32
	.type	dtls_bad_ver_client_method_data.23189, @object
	.size	dtls_bad_ver_client_method_data.23189, 224
dtls_bad_ver_client_method_data.23189:
	.long	256
	.long	2
	.quad	67108864
	.quad	dtls1_new
	.quad	dtls1_clear
	.quad	dtls1_free
	.quad	ssl_undefined_function
	.quad	ossl_statem_connect
	.quad	ssl3_read
	.quad	ssl3_peek
	.quad	ssl3_write
	.quad	dtls1_shutdown
	.quad	ssl3_renegotiate
	.quad	ssl3_renegotiate_check
	.quad	dtls1_read_bytes
	.quad	dtls1_write_app_data_bytes
	.quad	dtls1_dispatch_alert
	.quad	dtls1_ctrl
	.quad	ssl3_ctx_ctrl
	.quad	ssl3_get_cipher_by_char
	.quad	ssl3_put_cipher_by_char
	.quad	ssl3_pending
	.quad	ssl3_num_ciphers
	.quad	ssl3_get_cipher
	.quad	dtls1_default_timeout
	.quad	DTLSv1_enc_data
	.quad	ssl_undefined_void_function
	.quad	ssl3_callback_ctrl
	.quad	ssl3_ctx_callback_ctrl
	.align 32
	.type	dtlsv1_client_method_data.23185, @object
	.size	dtlsv1_client_method_data.23185, 224
dtlsv1_client_method_data.23185:
	.long	65279
	.long	2
	.quad	67108864
	.quad	dtls1_new
	.quad	dtls1_clear
	.quad	dtls1_free
	.quad	ssl_undefined_function
	.quad	ossl_statem_connect
	.quad	ssl3_read
	.quad	ssl3_peek
	.quad	ssl3_write
	.quad	dtls1_shutdown
	.quad	ssl3_renegotiate
	.quad	ssl3_renegotiate_check
	.quad	dtls1_read_bytes
	.quad	dtls1_write_app_data_bytes
	.quad	dtls1_dispatch_alert
	.quad	dtls1_ctrl
	.quad	ssl3_ctx_ctrl
	.quad	ssl3_get_cipher_by_char
	.quad	ssl3_put_cipher_by_char
	.quad	ssl3_pending
	.quad	ssl3_num_ciphers
	.quad	ssl3_get_cipher
	.quad	dtls1_default_timeout
	.quad	DTLSv1_enc_data
	.quad	ssl_undefined_void_function
	.quad	ssl3_callback_ctrl
	.quad	ssl3_ctx_callback_ctrl
	.align 32
	.type	DTLS_server_method_data.23181, @object
	.size	DTLS_server_method_data.23181, 224
DTLS_server_method_data.23181:
	.long	131071
	.long	0
	.quad	0
	.quad	dtls1_new
	.quad	dtls1_clear
	.quad	dtls1_free
	.quad	ossl_statem_accept
	.quad	ssl_undefined_function
	.quad	ssl3_read
	.quad	ssl3_peek
	.quad	ssl3_write
	.quad	dtls1_shutdown
	.quad	ssl3_renegotiate
	.quad	ssl3_renegotiate_check
	.quad	dtls1_read_bytes
	.quad	dtls1_write_app_data_bytes
	.quad	dtls1_dispatch_alert
	.quad	dtls1_ctrl
	.quad	ssl3_ctx_ctrl
	.quad	ssl3_get_cipher_by_char
	.quad	ssl3_put_cipher_by_char
	.quad	ssl3_pending
	.quad	ssl3_num_ciphers
	.quad	ssl3_get_cipher
	.quad	dtls1_default_timeout
	.quad	DTLSv1_2_enc_data
	.quad	ssl_undefined_void_function
	.quad	ssl3_callback_ctrl
	.quad	ssl3_ctx_callback_ctrl
	.align 32
	.type	dtlsv1_2_server_method_data.23177, @object
	.size	dtlsv1_2_server_method_data.23177, 224
dtlsv1_2_server_method_data.23177:
	.long	65277
	.long	0
	.quad	134217728
	.quad	dtls1_new
	.quad	dtls1_clear
	.quad	dtls1_free
	.quad	ossl_statem_accept
	.quad	ssl_undefined_function
	.quad	ssl3_read
	.quad	ssl3_peek
	.quad	ssl3_write
	.quad	dtls1_shutdown
	.quad	ssl3_renegotiate
	.quad	ssl3_renegotiate_check
	.quad	dtls1_read_bytes
	.quad	dtls1_write_app_data_bytes
	.quad	dtls1_dispatch_alert
	.quad	dtls1_ctrl
	.quad	ssl3_ctx_ctrl
	.quad	ssl3_get_cipher_by_char
	.quad	ssl3_put_cipher_by_char
	.quad	ssl3_pending
	.quad	ssl3_num_ciphers
	.quad	ssl3_get_cipher
	.quad	dtls1_default_timeout
	.quad	DTLSv1_2_enc_data
	.quad	ssl_undefined_void_function
	.quad	ssl3_callback_ctrl
	.quad	ssl3_ctx_callback_ctrl
	.align 32
	.type	dtlsv1_server_method_data.23173, @object
	.size	dtlsv1_server_method_data.23173, 224
dtlsv1_server_method_data.23173:
	.long	65279
	.long	2
	.quad	67108864
	.quad	dtls1_new
	.quad	dtls1_clear
	.quad	dtls1_free
	.quad	ossl_statem_accept
	.quad	ssl_undefined_function
	.quad	ssl3_read
	.quad	ssl3_peek
	.quad	ssl3_write
	.quad	dtls1_shutdown
	.quad	ssl3_renegotiate
	.quad	ssl3_renegotiate_check
	.quad	dtls1_read_bytes
	.quad	dtls1_write_app_data_bytes
	.quad	dtls1_dispatch_alert
	.quad	dtls1_ctrl
	.quad	ssl3_ctx_ctrl
	.quad	ssl3_get_cipher_by_char
	.quad	ssl3_put_cipher_by_char
	.quad	ssl3_pending
	.quad	ssl3_num_ciphers
	.quad	ssl3_get_cipher
	.quad	dtls1_default_timeout
	.quad	DTLSv1_enc_data
	.quad	ssl_undefined_void_function
	.quad	ssl3_callback_ctrl
	.quad	ssl3_ctx_callback_ctrl
	.align 32
	.type	DTLS_method_data.23169, @object
	.size	DTLS_method_data.23169, 224
DTLS_method_data.23169:
	.long	131071
	.long	0
	.quad	0
	.quad	dtls1_new
	.quad	dtls1_clear
	.quad	dtls1_free
	.quad	ossl_statem_accept
	.quad	ossl_statem_connect
	.quad	ssl3_read
	.quad	ssl3_peek
	.quad	ssl3_write
	.quad	dtls1_shutdown
	.quad	ssl3_renegotiate
	.quad	ssl3_renegotiate_check
	.quad	dtls1_read_bytes
	.quad	dtls1_write_app_data_bytes
	.quad	dtls1_dispatch_alert
	.quad	dtls1_ctrl
	.quad	ssl3_ctx_ctrl
	.quad	ssl3_get_cipher_by_char
	.quad	ssl3_put_cipher_by_char
	.quad	ssl3_pending
	.quad	ssl3_num_ciphers
	.quad	ssl3_get_cipher
	.quad	dtls1_default_timeout
	.quad	DTLSv1_2_enc_data
	.quad	ssl_undefined_void_function
	.quad	ssl3_callback_ctrl
	.quad	ssl3_ctx_callback_ctrl
	.align 32
	.type	dtlsv1_2_method_data.23165, @object
	.size	dtlsv1_2_method_data.23165, 224
dtlsv1_2_method_data.23165:
	.long	65277
	.long	0
	.quad	134217728
	.quad	dtls1_new
	.quad	dtls1_clear
	.quad	dtls1_free
	.quad	ossl_statem_accept
	.quad	ossl_statem_connect
	.quad	ssl3_read
	.quad	ssl3_peek
	.quad	ssl3_write
	.quad	dtls1_shutdown
	.quad	ssl3_renegotiate
	.quad	ssl3_renegotiate_check
	.quad	dtls1_read_bytes
	.quad	dtls1_write_app_data_bytes
	.quad	dtls1_dispatch_alert
	.quad	dtls1_ctrl
	.quad	ssl3_ctx_ctrl
	.quad	ssl3_get_cipher_by_char
	.quad	ssl3_put_cipher_by_char
	.quad	ssl3_pending
	.quad	ssl3_num_ciphers
	.quad	ssl3_get_cipher
	.quad	dtls1_default_timeout
	.quad	DTLSv1_2_enc_data
	.quad	ssl_undefined_void_function
	.quad	ssl3_callback_ctrl
	.quad	ssl3_ctx_callback_ctrl
	.align 32
	.type	dtlsv1_method_data.23161, @object
	.size	dtlsv1_method_data.23161, 224
dtlsv1_method_data.23161:
	.long	65279
	.long	2
	.quad	67108864
	.quad	dtls1_new
	.quad	dtls1_clear
	.quad	dtls1_free
	.quad	ossl_statem_accept
	.quad	ossl_statem_connect
	.quad	ssl3_read
	.quad	ssl3_peek
	.quad	ssl3_write
	.quad	dtls1_shutdown
	.quad	ssl3_renegotiate
	.quad	ssl3_renegotiate_check
	.quad	dtls1_read_bytes
	.quad	dtls1_write_app_data_bytes
	.quad	dtls1_dispatch_alert
	.quad	dtls1_ctrl
	.quad	ssl3_ctx_ctrl
	.quad	ssl3_get_cipher_by_char
	.quad	ssl3_put_cipher_by_char
	.quad	ssl3_pending
	.quad	ssl3_num_ciphers
	.quad	ssl3_get_cipher
	.quad	dtls1_default_timeout
	.quad	DTLSv1_enc_data
	.quad	ssl_undefined_void_function
	.quad	ssl3_callback_ctrl
	.quad	ssl3_ctx_callback_ctrl
	.align 32
	.type	tlsv1_client_method_data.23157, @object
	.size	tlsv1_client_method_data.23157, 224
tlsv1_client_method_data.23157:
	.long	769
	.long	2
	.quad	67108864
	.quad	tls1_new
	.quad	tls1_clear
	.quad	tls1_free
	.quad	ssl_undefined_function
	.quad	ossl_statem_connect
	.quad	ssl3_read
	.quad	ssl3_peek
	.quad	ssl3_write
	.quad	ssl3_shutdown
	.quad	ssl3_renegotiate
	.quad	ssl3_renegotiate_check
	.quad	ssl3_read_bytes
	.quad	ssl3_write_bytes
	.quad	ssl3_dispatch_alert
	.quad	ssl3_ctrl
	.quad	ssl3_ctx_ctrl
	.quad	ssl3_get_cipher_by_char
	.quad	ssl3_put_cipher_by_char
	.quad	ssl3_pending
	.quad	ssl3_num_ciphers
	.quad	ssl3_get_cipher
	.quad	tls1_default_timeout
	.quad	TLSv1_enc_data
	.quad	ssl_undefined_void_function
	.quad	ssl3_callback_ctrl
	.quad	ssl3_ctx_callback_ctrl
	.align 32
	.type	tlsv1_1_client_method_data.23153, @object
	.size	tlsv1_1_client_method_data.23153, 224
tlsv1_1_client_method_data.23153:
	.long	770
	.long	2
	.quad	268435456
	.quad	tls1_new
	.quad	tls1_clear
	.quad	tls1_free
	.quad	ssl_undefined_function
	.quad	ossl_statem_connect
	.quad	ssl3_read
	.quad	ssl3_peek
	.quad	ssl3_write
	.quad	ssl3_shutdown
	.quad	ssl3_renegotiate
	.quad	ssl3_renegotiate_check
	.quad	ssl3_read_bytes
	.quad	ssl3_write_bytes
	.quad	ssl3_dispatch_alert
	.quad	ssl3_ctrl
	.quad	ssl3_ctx_ctrl
	.quad	ssl3_get_cipher_by_char
	.quad	ssl3_put_cipher_by_char
	.quad	ssl3_pending
	.quad	ssl3_num_ciphers
	.quad	ssl3_get_cipher
	.quad	tls1_default_timeout
	.quad	TLSv1_1_enc_data
	.quad	ssl_undefined_void_function
	.quad	ssl3_callback_ctrl
	.quad	ssl3_ctx_callback_ctrl
	.align 32
	.type	tlsv1_2_client_method_data.23149, @object
	.size	tlsv1_2_client_method_data.23149, 224
tlsv1_2_client_method_data.23149:
	.long	771
	.long	0
	.quad	134217728
	.quad	tls1_new
	.quad	tls1_clear
	.quad	tls1_free
	.quad	ssl_undefined_function
	.quad	ossl_statem_connect
	.quad	ssl3_read
	.quad	ssl3_peek
	.quad	ssl3_write
	.quad	ssl3_shutdown
	.quad	ssl3_renegotiate
	.quad	ssl3_renegotiate_check
	.quad	ssl3_read_bytes
	.quad	ssl3_write_bytes
	.quad	ssl3_dispatch_alert
	.quad	ssl3_ctrl
	.quad	ssl3_ctx_ctrl
	.quad	ssl3_get_cipher_by_char
	.quad	ssl3_put_cipher_by_char
	.quad	ssl3_pending
	.quad	ssl3_num_ciphers
	.quad	ssl3_get_cipher
	.quad	tls1_default_timeout
	.quad	TLSv1_2_enc_data
	.quad	ssl_undefined_void_function
	.quad	ssl3_callback_ctrl
	.quad	ssl3_ctx_callback_ctrl
	.align 32
	.type	tlsv1_3_client_method_data.23145, @object
	.size	tlsv1_3_client_method_data.23145, 224
tlsv1_3_client_method_data.23145:
	.long	772
	.long	0
	.quad	536870912
	.quad	tls1_new
	.quad	tls1_clear
	.quad	tls1_free
	.quad	ssl_undefined_function
	.quad	ossl_statem_connect
	.quad	ssl3_read
	.quad	ssl3_peek
	.quad	ssl3_write
	.quad	ssl3_shutdown
	.quad	ssl3_renegotiate
	.quad	ssl3_renegotiate_check
	.quad	ssl3_read_bytes
	.quad	ssl3_write_bytes
	.quad	ssl3_dispatch_alert
	.quad	ssl3_ctrl
	.quad	ssl3_ctx_ctrl
	.quad	ssl3_get_cipher_by_char
	.quad	ssl3_put_cipher_by_char
	.quad	ssl3_pending
	.quad	ssl3_num_ciphers
	.quad	ssl3_get_cipher
	.quad	tls1_default_timeout
	.quad	TLSv1_3_enc_data
	.quad	ssl_undefined_void_function
	.quad	ssl3_callback_ctrl
	.quad	ssl3_ctx_callback_ctrl
	.align 32
	.type	TLS_client_method_data.23141, @object
	.size	TLS_client_method_data.23141, 224
TLS_client_method_data.23141:
	.long	65536
	.long	0
	.quad	0
	.quad	tls1_new
	.quad	tls1_clear
	.quad	tls1_free
	.quad	ssl_undefined_function
	.quad	ossl_statem_connect
	.quad	ssl3_read
	.quad	ssl3_peek
	.quad	ssl3_write
	.quad	ssl3_shutdown
	.quad	ssl3_renegotiate
	.quad	ssl3_renegotiate_check
	.quad	ssl3_read_bytes
	.quad	ssl3_write_bytes
	.quad	ssl3_dispatch_alert
	.quad	ssl3_ctrl
	.quad	ssl3_ctx_ctrl
	.quad	ssl3_get_cipher_by_char
	.quad	ssl3_put_cipher_by_char
	.quad	ssl3_pending
	.quad	ssl3_num_ciphers
	.quad	ssl3_get_cipher
	.quad	tls1_default_timeout
	.quad	TLSv1_2_enc_data
	.quad	ssl_undefined_void_function
	.quad	ssl3_callback_ctrl
	.quad	ssl3_ctx_callback_ctrl
	.align 32
	.type	tlsv1_server_method_data.23137, @object
	.size	tlsv1_server_method_data.23137, 224
tlsv1_server_method_data.23137:
	.long	769
	.long	2
	.quad	67108864
	.quad	tls1_new
	.quad	tls1_clear
	.quad	tls1_free
	.quad	ossl_statem_accept
	.quad	ssl_undefined_function
	.quad	ssl3_read
	.quad	ssl3_peek
	.quad	ssl3_write
	.quad	ssl3_shutdown
	.quad	ssl3_renegotiate
	.quad	ssl3_renegotiate_check
	.quad	ssl3_read_bytes
	.quad	ssl3_write_bytes
	.quad	ssl3_dispatch_alert
	.quad	ssl3_ctrl
	.quad	ssl3_ctx_ctrl
	.quad	ssl3_get_cipher_by_char
	.quad	ssl3_put_cipher_by_char
	.quad	ssl3_pending
	.quad	ssl3_num_ciphers
	.quad	ssl3_get_cipher
	.quad	tls1_default_timeout
	.quad	TLSv1_enc_data
	.quad	ssl_undefined_void_function
	.quad	ssl3_callback_ctrl
	.quad	ssl3_ctx_callback_ctrl
	.align 32
	.type	tlsv1_1_server_method_data.23133, @object
	.size	tlsv1_1_server_method_data.23133, 224
tlsv1_1_server_method_data.23133:
	.long	770
	.long	2
	.quad	268435456
	.quad	tls1_new
	.quad	tls1_clear
	.quad	tls1_free
	.quad	ossl_statem_accept
	.quad	ssl_undefined_function
	.quad	ssl3_read
	.quad	ssl3_peek
	.quad	ssl3_write
	.quad	ssl3_shutdown
	.quad	ssl3_renegotiate
	.quad	ssl3_renegotiate_check
	.quad	ssl3_read_bytes
	.quad	ssl3_write_bytes
	.quad	ssl3_dispatch_alert
	.quad	ssl3_ctrl
	.quad	ssl3_ctx_ctrl
	.quad	ssl3_get_cipher_by_char
	.quad	ssl3_put_cipher_by_char
	.quad	ssl3_pending
	.quad	ssl3_num_ciphers
	.quad	ssl3_get_cipher
	.quad	tls1_default_timeout
	.quad	TLSv1_1_enc_data
	.quad	ssl_undefined_void_function
	.quad	ssl3_callback_ctrl
	.quad	ssl3_ctx_callback_ctrl
	.align 32
	.type	tlsv1_2_server_method_data.23129, @object
	.size	tlsv1_2_server_method_data.23129, 224
tlsv1_2_server_method_data.23129:
	.long	771
	.long	0
	.quad	134217728
	.quad	tls1_new
	.quad	tls1_clear
	.quad	tls1_free
	.quad	ossl_statem_accept
	.quad	ssl_undefined_function
	.quad	ssl3_read
	.quad	ssl3_peek
	.quad	ssl3_write
	.quad	ssl3_shutdown
	.quad	ssl3_renegotiate
	.quad	ssl3_renegotiate_check
	.quad	ssl3_read_bytes
	.quad	ssl3_write_bytes
	.quad	ssl3_dispatch_alert
	.quad	ssl3_ctrl
	.quad	ssl3_ctx_ctrl
	.quad	ssl3_get_cipher_by_char
	.quad	ssl3_put_cipher_by_char
	.quad	ssl3_pending
	.quad	ssl3_num_ciphers
	.quad	ssl3_get_cipher
	.quad	tls1_default_timeout
	.quad	TLSv1_2_enc_data
	.quad	ssl_undefined_void_function
	.quad	ssl3_callback_ctrl
	.quad	ssl3_ctx_callback_ctrl
	.align 32
	.type	tlsv1_3_server_method_data.23125, @object
	.size	tlsv1_3_server_method_data.23125, 224
tlsv1_3_server_method_data.23125:
	.long	772
	.long	0
	.quad	536870912
	.quad	tls1_new
	.quad	tls1_clear
	.quad	tls1_free
	.quad	ossl_statem_accept
	.quad	ssl_undefined_function
	.quad	ssl3_read
	.quad	ssl3_peek
	.quad	ssl3_write
	.quad	ssl3_shutdown
	.quad	ssl3_renegotiate
	.quad	ssl3_renegotiate_check
	.quad	ssl3_read_bytes
	.quad	ssl3_write_bytes
	.quad	ssl3_dispatch_alert
	.quad	ssl3_ctrl
	.quad	ssl3_ctx_ctrl
	.quad	ssl3_get_cipher_by_char
	.quad	ssl3_put_cipher_by_char
	.quad	ssl3_pending
	.quad	ssl3_num_ciphers
	.quad	ssl3_get_cipher
	.quad	tls1_default_timeout
	.quad	TLSv1_3_enc_data
	.quad	ssl_undefined_void_function
	.quad	ssl3_callback_ctrl
	.quad	ssl3_ctx_callback_ctrl
	.align 32
	.type	TLS_server_method_data.23121, @object
	.size	TLS_server_method_data.23121, 224
TLS_server_method_data.23121:
	.long	65536
	.long	0
	.quad	0
	.quad	tls1_new
	.quad	tls1_clear
	.quad	tls1_free
	.quad	ossl_statem_accept
	.quad	ssl_undefined_function
	.quad	ssl3_read
	.quad	ssl3_peek
	.quad	ssl3_write
	.quad	ssl3_shutdown
	.quad	ssl3_renegotiate
	.quad	ssl3_renegotiate_check
	.quad	ssl3_read_bytes
	.quad	ssl3_write_bytes
	.quad	ssl3_dispatch_alert
	.quad	ssl3_ctrl
	.quad	ssl3_ctx_ctrl
	.quad	ssl3_get_cipher_by_char
	.quad	ssl3_put_cipher_by_char
	.quad	ssl3_pending
	.quad	ssl3_num_ciphers
	.quad	ssl3_get_cipher
	.quad	tls1_default_timeout
	.quad	TLSv1_2_enc_data
	.quad	ssl_undefined_void_function
	.quad	ssl3_callback_ctrl
	.quad	ssl3_ctx_callback_ctrl
	.align 32
	.type	tlsv1_method_data.23117, @object
	.size	tlsv1_method_data.23117, 224
tlsv1_method_data.23117:
	.long	769
	.long	2
	.quad	67108864
	.quad	tls1_new
	.quad	tls1_clear
	.quad	tls1_free
	.quad	ossl_statem_accept
	.quad	ossl_statem_connect
	.quad	ssl3_read
	.quad	ssl3_peek
	.quad	ssl3_write
	.quad	ssl3_shutdown
	.quad	ssl3_renegotiate
	.quad	ssl3_renegotiate_check
	.quad	ssl3_read_bytes
	.quad	ssl3_write_bytes
	.quad	ssl3_dispatch_alert
	.quad	ssl3_ctrl
	.quad	ssl3_ctx_ctrl
	.quad	ssl3_get_cipher_by_char
	.quad	ssl3_put_cipher_by_char
	.quad	ssl3_pending
	.quad	ssl3_num_ciphers
	.quad	ssl3_get_cipher
	.quad	tls1_default_timeout
	.quad	TLSv1_enc_data
	.quad	ssl_undefined_void_function
	.quad	ssl3_callback_ctrl
	.quad	ssl3_ctx_callback_ctrl
	.align 32
	.type	tlsv1_1_method_data.23113, @object
	.size	tlsv1_1_method_data.23113, 224
tlsv1_1_method_data.23113:
	.long	770
	.long	2
	.quad	268435456
	.quad	tls1_new
	.quad	tls1_clear
	.quad	tls1_free
	.quad	ossl_statem_accept
	.quad	ossl_statem_connect
	.quad	ssl3_read
	.quad	ssl3_peek
	.quad	ssl3_write
	.quad	ssl3_shutdown
	.quad	ssl3_renegotiate
	.quad	ssl3_renegotiate_check
	.quad	ssl3_read_bytes
	.quad	ssl3_write_bytes
	.quad	ssl3_dispatch_alert
	.quad	ssl3_ctrl
	.quad	ssl3_ctx_ctrl
	.quad	ssl3_get_cipher_by_char
	.quad	ssl3_put_cipher_by_char
	.quad	ssl3_pending
	.quad	ssl3_num_ciphers
	.quad	ssl3_get_cipher
	.quad	tls1_default_timeout
	.quad	TLSv1_1_enc_data
	.quad	ssl_undefined_void_function
	.quad	ssl3_callback_ctrl
	.quad	ssl3_ctx_callback_ctrl
	.align 32
	.type	tlsv1_2_method_data.23109, @object
	.size	tlsv1_2_method_data.23109, 224
tlsv1_2_method_data.23109:
	.long	771
	.long	0
	.quad	134217728
	.quad	tls1_new
	.quad	tls1_clear
	.quad	tls1_free
	.quad	ossl_statem_accept
	.quad	ossl_statem_connect
	.quad	ssl3_read
	.quad	ssl3_peek
	.quad	ssl3_write
	.quad	ssl3_shutdown
	.quad	ssl3_renegotiate
	.quad	ssl3_renegotiate_check
	.quad	ssl3_read_bytes
	.quad	ssl3_write_bytes
	.quad	ssl3_dispatch_alert
	.quad	ssl3_ctrl
	.quad	ssl3_ctx_ctrl
	.quad	ssl3_get_cipher_by_char
	.quad	ssl3_put_cipher_by_char
	.quad	ssl3_pending
	.quad	ssl3_num_ciphers
	.quad	ssl3_get_cipher
	.quad	tls1_default_timeout
	.quad	TLSv1_2_enc_data
	.quad	ssl_undefined_void_function
	.quad	ssl3_callback_ctrl
	.quad	ssl3_ctx_callback_ctrl
	.align 32
	.type	tlsv1_3_method_data.23105, @object
	.size	tlsv1_3_method_data.23105, 224
tlsv1_3_method_data.23105:
	.long	772
	.long	0
	.quad	536870912
	.quad	tls1_new
	.quad	tls1_clear
	.quad	tls1_free
	.quad	ossl_statem_accept
	.quad	ossl_statem_connect
	.quad	ssl3_read
	.quad	ssl3_peek
	.quad	ssl3_write
	.quad	ssl3_shutdown
	.quad	ssl3_renegotiate
	.quad	ssl3_renegotiate_check
	.quad	ssl3_read_bytes
	.quad	ssl3_write_bytes
	.quad	ssl3_dispatch_alert
	.quad	ssl3_ctrl
	.quad	ssl3_ctx_ctrl
	.quad	ssl3_get_cipher_by_char
	.quad	ssl3_put_cipher_by_char
	.quad	ssl3_pending
	.quad	ssl3_num_ciphers
	.quad	ssl3_get_cipher
	.quad	tls1_default_timeout
	.quad	TLSv1_3_enc_data
	.quad	ssl_undefined_void_function
	.quad	ssl3_callback_ctrl
	.quad	ssl3_ctx_callback_ctrl
	.align 32
	.type	TLS_method_data.23101, @object
	.size	TLS_method_data.23101, 224
TLS_method_data.23101:
	.long	65536
	.long	0
	.quad	0
	.quad	tls1_new
	.quad	tls1_clear
	.quad	tls1_free
	.quad	ossl_statem_accept
	.quad	ossl_statem_connect
	.quad	ssl3_read
	.quad	ssl3_peek
	.quad	ssl3_write
	.quad	ssl3_shutdown
	.quad	ssl3_renegotiate
	.quad	ssl3_renegotiate_check
	.quad	ssl3_read_bytes
	.quad	ssl3_write_bytes
	.quad	ssl3_dispatch_alert
	.quad	ssl3_ctrl
	.quad	ssl3_ctx_ctrl
	.quad	ssl3_get_cipher_by_char
	.quad	ssl3_put_cipher_by_char
	.quad	ssl3_pending
	.quad	ssl3_num_ciphers
	.quad	ssl3_get_cipher
	.quad	tls1_default_timeout
	.quad	TLSv1_2_enc_data
	.quad	ssl_undefined_void_function
	.quad	ssl3_callback_ctrl
	.quad	ssl3_ctx_callback_ctrl
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
