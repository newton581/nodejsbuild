	.file	"dso_lib.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/dso/dso_lib.c"
	.text
	.p2align 4
	.globl	DSO_free
	.type	DSO_free, @function
DSO_free:
.LFB255:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L21
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	lock xaddl	%eax, 16(%rdi)
	subl	$1, %eax
	testl	%eax, %eax
	je	.L22
	jle	.L5
.L19:
	movl	$1, %eax
.L1:
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
.L5:
	movq	(%r12), %rax
	testb	$4, 20(%r12)
	jne	.L7
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L7
	movq	%r12, %rdi
	call	*%rdx
	testl	%eax, %eax
	je	.L8
	movq	(%r12), %rax
.L7:
	movq	64(%rax), %rax
	testq	%rax, %rax
	je	.L9
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L23
.L9:
	movq	8(%r12), %rdi
	call	OPENSSL_sk_free@PLT
	movq	48(%r12), %rdi
	movl	$90, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	56(%r12), %rdi
	movl	$91, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	64(%r12), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	movl	$93, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	$85, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$102, %edx
	movl	%eax, -20(%rbp)
	movl	$111, %esi
	movl	$37, %edi
	call	ERR_put_error@PLT
	movl	-20(%rbp), %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$79, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$107, %edx
	movl	%eax, -20(%rbp)
	movl	$111, %esi
	movl	$37, %edi
	call	ERR_put_error@PLT
	movl	-20(%rbp), %eax
	jmp	.L1
	.cfi_endproc
.LFE255:
	.size	DSO_free, .-DSO_free
	.p2align 4
	.type	DSO_new_method.isra.0, @function
DSO_new_method.isra.0:
.LFB271:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	cmpq	$0, default_DSO_meth(%rip)
	je	.L34
.L25:
	movl	$27, %edx
	leaq	.LC0(%rip), %rsi
	movl	$72, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L35
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L36
	movq	default_DSO_meth(%rip), %rax
	movq	%rax, (%r12)
	movl	$1, 16(%r12)
	mfence
	call	CRYPTO_THREAD_lock_new@PLT
	movq	%rax, 64(%r12)
	testq	%rax, %rax
	je	.L37
	movq	(%r12), %rax
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L24
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L38
.L24:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	DSO_free
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L34:
	call	DSO_METHOD_openssl@PLT
	movq	%rax, default_DSO_meth(%rip)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L35:
	movl	$29, %r8d
	movl	$65, %edx
	movl	$113, %esi
	movl	$37, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$35, %r8d
	movl	$65, %edx
	movl	$113, %esi
	movl	$37, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movl	$36, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L37:
	movl	$43, %r8d
	movl	$65, %edx
	movl	$113, %esi
	movl	$37, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	8(%r12), %rdi
	call	OPENSSL_sk_free@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movl	$45, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L24
	.cfi_endproc
.LFE271:
	.size	DSO_new_method.isra.0, .-DSO_new_method.isra.0
	.p2align 4
	.globl	DSO_new
	.type	DSO_new, @function
DSO_new:
.LFB254:
	.cfi_startproc
	endbr64
	jmp	DSO_new_method.isra.0
	.cfi_endproc
.LFE254:
	.size	DSO_new, .-DSO_new
	.p2align 4
	.globl	DSO_flags
	.type	DSO_flags, @function
DSO_flags:
.LFB256:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L40
	movl	20(%rdi), %eax
.L40:
	ret
	.cfi_endproc
.LFE256:
	.size	DSO_flags, .-DSO_flags
	.p2align 4
	.globl	DSO_up_ref
	.type	DSO_up_ref, @function
DSO_up_ref:
.LFB257:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L51
	movl	$1, %eax
	lock xaddl	%eax, 16(%rdi)
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$67, %edx
	movl	$114, %esi
	movl	$37, %edi
	movl	$107, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE257:
	.size	DSO_up_ref, .-DSO_up_ref
	.p2align 4
	.globl	DSO_bind_func
	.type	DSO_bind_func, @function
DSO_bind_func:
.LFB259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	testq	%rdi, %rdi
	je	.L57
	testq	%rsi, %rsi
	je	.L57
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L59
	call	*%rax
	testq	%rax, %rax
	je	.L60
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	movl	$178, %r8d
	movl	$67, %edx
	movl	$108, %esi
	movl	$37, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	movl	$186, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$106, %edx
	movl	$108, %esi
	movl	$37, %edi
	movq	%rax, -8(%rbp)
	call	ERR_put_error@PLT
	movq	-8(%rbp), %rax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	movl	$182, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$108, %edx
	movl	$108, %esi
	movl	$37, %edi
	movq	%rax, -8(%rbp)
	call	ERR_put_error@PLT
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE259:
	.size	DSO_bind_func, .-DSO_bind_func
	.p2align 4
	.globl	DSO_ctrl
	.type	DSO_ctrl, @function
DSO_ctrl:
.LFB260:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rdi, %rdi
	je	.L73
	cmpl	$2, %esi
	je	.L64
	cmpl	$3, %esi
	je	.L65
	cmpl	$1, %esi
	je	.L74
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L67
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.L67
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	movslq	20(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	orl	%edx, 20(%rdi)
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
	movl	%edx, 20(%rdi)
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L67:
	.cfi_restore_state
	movl	$225, %r8d
	movl	$108, %edx
	movl	$110, %esi
	movl	$37, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	$-1, %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L73:
	.cfi_restore_state
	movl	$205, %r8d
	movl	$67, %edx
	movl	$110, %esi
	movl	$37, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	$-1, %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE260:
	.size	DSO_ctrl, .-DSO_ctrl
	.p2align 4
	.globl	DSO_get_filename
	.type	DSO_get_filename, @function
DSO_get_filename:
.LFB261:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L82
	movq	48(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$67, %edx
	movl	$127, %esi
	movl	$37, %edi
	movl	$234, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE261:
	.size	DSO_get_filename, .-DSO_get_filename
	.p2align 4
	.globl	DSO_set_filename
	.type	DSO_set_filename, @function
DSO_set_filename:
.LFB262:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L89
	movq	%rsi, %rdi
	testq	%rsi, %rsi
	je	.L89
	cmpq	$0, 56(%rbx)
	je	.L87
	movl	$249, %r8d
	movl	$110, %edx
	movl	$129, %esi
	movl	$37, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L83:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	movl	$253, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L91
	movq	48(%rbx), %rdi
	movl	$258, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, 48(%rbx)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	movl	$245, %r8d
	movl	$67, %edx
	movl	$129, %esi
	movl	$37, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	movl	$255, %r8d
	movl	$65, %edx
	movl	$129, %esi
	movl	$37, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L83
	.cfi_endproc
.LFE262:
	.size	DSO_set_filename, .-DSO_set_filename
	.p2align 4
	.globl	DSO_load
	.type	DSO_load, @function
DSO_load:
.LFB258:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	xorl	%ebx, %ebx
	subq	$24, %rsp
	testq	%rdi, %rdi
	je	.L108
.L93:
	cmpq	$0, 48(%r12)
	jne	.L109
	testq	%rsi, %rsi
	je	.L100
	movq	%r12, %rdi
	call	DSO_set_filename
	testl	%eax, %eax
	je	.L110
	cmpq	$0, 48(%r12)
	je	.L100
	movq	(%r12), %rax
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L111
	movq	%r12, %rdi
	movq	%r12, %r13
	call	*%rax
	testl	%eax, %eax
	je	.L112
.L92:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	movl	$154, %r8d
	movl	$111, %edx
	movl	$112, %esi
	movl	$37, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L97:
	xorl	%r13d, %r13d
	testl	%ebx, %ebx
	je	.L92
	movq	%r12, %rdi
	call	DSO_free
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	movl	$140, %r8d
	movl	$110, %edx
	movl	$112, %esi
	movl	$37, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L108:
	movq	%rsi, -40(%rbp)
	movl	%ecx, %r13d
	call	DSO_new_method.isra.0
	movq	-40(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L113
	movl	%r13d, 20(%rax)
	movl	$1, %ebx
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L112:
	movl	$162, %r8d
	movl	$103, %edx
	movl	$112, %esi
	movl	$37, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L110:
	movl	$149, %r8d
	movl	$112, %edx
	movl	$112, %esi
	movl	$37, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L111:
	movl	$158, %r8d
	movl	$108, %edx
	movl	$112, %esi
	movl	$37, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L113:
	movl	$127, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$112, %esi
	movl	$37, %edi
	call	ERR_put_error@PLT
	jmp	.L92
	.cfi_endproc
.LFE258:
	.size	DSO_load, .-DSO_load
	.p2align 4
	.globl	DSO_merge
	.type	DSO_merge, @function
DSO_merge:
.LFB263:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L121
	testq	%rsi, %rsi
	je	.L121
	testb	$1, 20(%rdi)
	jne	.L124
	movq	40(%rdi), %rax
	testq	%rax, %rax
	je	.L118
.L126:
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L118:
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	testq	%rax, %rax
	jne	.L126
.L124:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$67, %edx
	movl	$132, %esi
	movl	$37, %edi
	movl	$268, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE263:
	.size	DSO_merge, .-DSO_merge
	.p2align 4
	.globl	DSO_convert_filename
	.type	DSO_convert_filename, @function
DSO_convert_filename:
.LFB264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	testq	%rdi, %rdi
	je	.L144
	movq	%rsi, %r12
	testq	%rsi, %rsi
	je	.L145
.L130:
	testb	$1, 20(%rdi)
	jne	.L135
	movq	32(%rdi), %rax
	testq	%rax, %rax
	je	.L133
.L143:
	movq	%r12, %rsi
	call	*%rax
	testq	%rax, %rax
	je	.L135
.L127:
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	testq	%rax, %rax
	jne	.L143
	.p2align 4,,10
	.p2align 3
.L135:
	movl	$301, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_strdup@PLT
	testq	%rax, %rax
	jne	.L127
	movl	$303, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$126, %esi
	movl	$37, %edi
	movq	%rax, -24(%rbp)
	call	ERR_put_error@PLT
	movq	-24(%rbp), %rax
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L145:
	movq	48(%rdi), %r12
	testq	%r12, %r12
	jne	.L130
	movl	$291, %r8d
	movl	$111, %edx
	movl	$126, %esi
	movl	$37, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L144:
	movl	$285, %r8d
	movl	$67, %edx
	movl	$126, %esi
	movl	$37, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L127
	.cfi_endproc
.LFE264:
	.size	DSO_convert_filename, .-DSO_convert_filename
	.p2align 4
	.globl	DSO_pathbyaddr
	.type	DSO_pathbyaddr, @function
DSO_pathbyaddr:
.LFB265:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	default_DSO_meth(%rip), %rax
	testq	%rax, %rax
	je	.L153
.L147:
	movq	72(%rax), %rax
	testq	%rax, %rax
	je	.L151
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L153:
	.cfi_restore_state
	movl	%edx, -20(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdi, -8(%rbp)
	call	DSO_METHOD_openssl@PLT
	movl	-20(%rbp), %edx
	movq	-16(%rbp), %rsi
	movq	-8(%rbp), %rdi
	jmp	.L147
.L151:
	movl	$316, %r8d
	movl	$108, %edx
	movl	$105, %esi
	movl	$37, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE265:
	.size	DSO_pathbyaddr, .-DSO_pathbyaddr
	.p2align 4
	.globl	DSO_dsobyaddr
	.type	DSO_dsobyaddr, @function
DSO_dsobyaddr:
.LFB266:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	default_DSO_meth(%rip), %rax
	testq	%rax, %rax
	je	.L166
.L155:
	movq	72(%rax), %rax
	testq	%rax, %rax
	je	.L167
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	call	*%rax
	movl	%eax, %ebx
	testl	%eax, %eax
	js	.L154
	movslq	%eax, %rdi
	movl	$331, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L165
	movq	default_DSO_meth(%rip), %rax
	testq	%rax, %rax
	je	.L168
.L160:
	movq	72(%rax), %rax
	testq	%rax, %rax
	je	.L169
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	cmpl	%eax, %ebx
	je	.L170
.L165:
	xorl	%r15d, %r15d
.L159:
	movl	$336, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
.L154:
	addq	$8, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore_state
	movl	%r14d, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	xorl	%edi, %edi
	call	DSO_load
	movq	%rax, %r15
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L166:
	call	DSO_METHOD_openssl@PLT
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L168:
	call	DSO_METHOD_openssl@PLT
	jmp	.L160
.L167:
	movl	$316, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$108, %edx
	xorl	%r15d, %r15d
	movl	$105, %esi
	movl	$37, %edi
	call	ERR_put_error@PLT
	jmp	.L154
.L169:
	movl	$316, %r8d
	movl	$108, %edx
	movl	$105, %esi
	movl	$37, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L165
	.cfi_endproc
.LFE266:
	.size	DSO_dsobyaddr, .-DSO_dsobyaddr
	.p2align 4
	.globl	DSO_global_lookup
	.type	DSO_global_lookup, @function
DSO_global_lookup:
.LFB267:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	default_DSO_meth(%rip), %rax
	testq	%rax, %rax
	je	.L178
.L172:
	movq	80(%rax), %rax
	testq	%rax, %rax
	je	.L176
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	movq	%rdi, -8(%rbp)
	call	DSO_METHOD_openssl@PLT
	movq	-8(%rbp), %rdi
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L176:
	movl	$346, %r8d
	movl	$108, %edx
	movl	$139, %esi
	movl	$37, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE267:
	.size	DSO_global_lookup, .-DSO_global_lookup
	.local	default_DSO_meth
	.comm	default_DSO_meth,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
