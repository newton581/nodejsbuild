	.file	"bn_lib.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/bn/bn_lib.c"
	.text
	.p2align 4
	.type	bn_expand2.part.0, @function
bn_expand2.part.0:
.LFB347:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	cmpl	$8388607, %esi
	jg	.L14
	movl	20(%rdi), %eax
	movq	%rdi, %rbx
	testb	$2, %al
	jne	.L15
	movslq	%esi, %rdi
	movl	%esi, %r13d
	salq	$3, %rdi
	testb	$8, %al
	jne	.L16
	movl	$268, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
.L6:
	testq	%r12, %r12
	je	.L17
	movslq	8(%rbx), %rdx
	movq	(%rbx), %rdi
	testl	%edx, %edx
	jle	.L8
	movq	%rdi, %rsi
	salq	$3, %rdx
	movq	%r12, %rdi
	call	memcpy@PLT
	movq	(%rbx), %rdi
.L8:
	testq	%rdi, %rdi
	je	.L10
	movslq	12(%rbx), %rsi
	salq	$3, %rsi
	testb	$8, 20(%rbx)
	jne	.L18
	movl	$194, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
.L10:
	movl	%r13d, 12(%rbx)
	movq	%r12, (%rbx)
	movq	%rbx, %r12
.L1:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movl	$266, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_secure_zalloc@PLT
	movq	%rax, %r12
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$192, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_secure_clear_free@PLT
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L14:
	movl	$258, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$114, %edx
	xorl	%r12d, %r12d
	movl	$120, %esi
	movl	$3, %edi
	call	ERR_put_error@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L15:
	movl	$262, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$105, %edx
	xorl	%r12d, %r12d
	movl	$120, %esi
	movl	$3, %edi
	call	ERR_put_error@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L17:
	movl	$270, %r8d
	movl	$65, %edx
	movl	$120, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1
	.cfi_endproc
.LFE347:
	.size	bn_expand2.part.0, .-bn_expand2.part.0
	.p2align 4
	.globl	BN_set_params
	.type	BN_set_params, @function
BN_set_params:
.LFB288:
	.cfi_startproc
	endbr64
	testl	%edi, %edi
	js	.L20
	cmpl	$31, %edi
	movl	$31, %eax
	cmovg	%eax, %edi
	movl	%edi, bn_limit_bits(%rip)
.L20:
	testl	%esi, %esi
	js	.L21
	cmpl	$31, %esi
	movl	$31, %eax
	cmovg	%eax, %esi
	movl	%esi, bn_limit_bits_high(%rip)
.L21:
	testl	%edx, %edx
	js	.L22
	cmpl	$31, %edx
	movl	$31, %eax
	cmovg	%eax, %edx
	movl	%edx, bn_limit_bits_low(%rip)
.L22:
	testl	%ecx, %ecx
	js	.L19
	cmpl	$31, %ecx
	movl	$31, %eax
	cmovg	%eax, %ecx
	movl	%ecx, bn_limit_bits_mont(%rip)
.L19:
	ret
	.cfi_endproc
.LFE288:
	.size	BN_set_params, .-BN_set_params
	.p2align 4
	.globl	BN_get_params
	.type	BN_get_params, @function
BN_get_params:
.LFB289:
	.cfi_startproc
	endbr64
	testl	%edi, %edi
	je	.L30
	cmpl	$1, %edi
	je	.L31
	cmpl	$2, %edi
	je	.L32
	cmpl	$3, %edi
	movl	$0, %eax
	cmove	bn_limit_bits_mont(%rip), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	movl	bn_limit_bits(%rip), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	movl	bn_limit_bits_low(%rip), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	movl	bn_limit_bits_high(%rip), %eax
	ret
	.cfi_endproc
.LFE289:
	.size	BN_get_params, .-BN_get_params
	.p2align 4
	.globl	BN_value_one
	.type	BN_value_one, @function
BN_value_one:
.LFB290:
	.cfi_startproc
	endbr64
	leaq	const_one.7357(%rip), %rax
	ret
	.cfi_endproc
.LFE290:
	.size	BN_value_one, .-BN_value_one
	.p2align 4
	.globl	BN_num_bits_word
	.type	BN_num_bits_word, @function
BN_num_bits_word:
.LFB291:
	.cfi_startproc
	endbr64
	movq	%rdi, %r10
	shrq	$32, %r10
	movq	%r10, %rdx
	xorq	%rdi, %r10
	negq	%rdx
	sarq	$63, %rdx
	andq	%rdx, %r10
	andl	$32, %edx
	xorq	%rdi, %r10
	movq	%r10, %r9
	shrq	$16, %r9
	movq	%r9, %rsi
	xorq	%r10, %r9
	negq	%rsi
	sarq	$63, %rsi
	andq	%rsi, %r9
	xorq	%r9, %r10
	movq	%r10, %r9
	shrq	$8, %r9
	movq	%r9, %rcx
	movq	%r9, %r8
	negq	%rcx
	xorq	%r10, %r8
	sarq	$63, %rcx
	andq	%rcx, %r8
	xorq	%r10, %r8
	movq	%r8, %r9
	shrq	$4, %r9
	movq	%r9, %r10
	xorq	%r8, %r9
	negq	%r10
	sarq	$63, %r10
	andq	%r10, %r9
	xorq	%r8, %r9
	movq	%r9, %r8
	shrq	$2, %r8
	movq	%r8, %rax
	xorq	%r9, %r8
	negq	%rax
	sarq	$63, %rax
	andq	%rax, %r8
	testq	%rdi, %rdi
	setne	%dil
	andl	$16, %esi
	andl	$8, %ecx
	xorq	%r9, %r8
	movzbl	%dil, %edi
	andl	$4, %r10d
	shrq	%r8
	andl	$2, %eax
	addl	%edx, %edi
	negq	%r8
	addl	%edi, %esi
	sarq	$63, %r8
	addl	%esi, %ecx
	leal	(%rcx,%r10), %edx
	addl	%edx, %eax
	subl	%r8d, %eax
	ret
	.cfi_endproc
.LFE291:
	.size	BN_num_bits_word, .-BN_num_bits_word
	.p2align 4
	.globl	BN_num_bits
	.type	BN_num_bits, @function
BN_num_bits:
.LFB293:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	8(%rdi), %eax
	leal	-1(%rax), %r13d
	testb	$4, 20(%rdi)
	jne	.L46
	testl	%eax, %eax
	je	.L35
	movq	(%rdi), %rax
	movslq	%r13d, %rdx
	sall	$6, %r13d
	movq	(%rax,%rdx,8), %rdi
	call	BN_num_bits_word
	addl	%r13d, %eax
.L35:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	notl	%eax
	movl	12(%rdi), %edx
	andl	%r13d, %eax
	sarl	$31, %eax
	notl	%eax
	movl	%eax, -60(%rbp)
	testl	%edx, %edx
	jle	.L40
	leal	-1(%rdx), %eax
	movq	(%rdi), %r15
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L38:
	movl	%r13d, %edx
	movq	(%r15,%rbx,8), %rcx
	xorl	%ebx, %edx
	movl	%edx, %esi
	subl	$1, %edx
	notl	%esi
	andl	%esi, %edx
	movl	%edx, %esi
	movq	%rcx, %rdx
	shrq	$32, %rdx
	sarl	$31, %esi
	movq	%rdx, %r12
	xorq	%rcx, %rdx
	orl	%esi, %r14d
	negq	%r12
	sarq	$63, %r12
	andq	%r12, %rdx
	xorq	%rcx, %rdx
	movq	%rdx, %rdi
	shrq	$16, %rdi
	movq	%rdi, %r11
	xorq	%rdx, %rdi
	negq	%r11
	sarq	$63, %r11
	andq	%r11, %rdi
	xorq	%rdi, %rdx
	movq	%rdx, %rdi
	shrq	$8, %rdi
	movq	%rdi, %r10
	xorq	%rdx, %rdi
	negq	%r10
	sarq	$63, %r10
	andq	%r10, %rdi
	xorq	%rdi, %rdx
	movq	%rdx, %rdi
	shrq	$4, %rdi
	movq	%rdi, %r9
	xorq	%rdx, %rdi
	negq	%r9
	sarq	$63, %r9
	andq	%r9, %rdi
	xorq	%rdi, %rdx
	movq	%rdx, %rdi
	shrq	$2, %rdi
	movq	%rdi, %r8
	negq	%r8
	sarq	$63, %r8
	xorq	%rdx, %rdi
	andl	$32, %r12d
	andq	%r8, %rdi
	testq	%rcx, %rcx
	setne	%cl
	andl	$16, %r11d
	andl	$8, %r10d
	xorq	%rdi, %rdx
	movzbl	%cl, %ecx
	andl	$4, %r9d
	shrq	%rdx
	andl	$2, %r8d
	addl	%r12d, %ecx
	negq	%rdx
	addl	%ecx, %r11d
	sarq	$63, %rdx
	addl	%r11d, %r10d
	addl	%r10d, %r9d
	addl	%r9d, %r8d
	subl	%edx, %r8d
	movl	%r14d, %edx
	notl	%edx
	andl	%r8d, %esi
	andl	$64, %edx
	addl	%edx, %eax
	movq	%rbx, %rdx
	addq	$1, %rbx
	addl	%esi, %eax
	cmpq	%rdx, -56(%rbp)
	jne	.L38
	andl	-60(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L35
	.cfi_endproc
.LFE293:
	.size	BN_num_bits, .-BN_num_bits
	.p2align 4
	.globl	BN_clear_free
	.type	BN_clear_free, @function
BN_clear_free:
.LFB295:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L60
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rdi
	movl	20(%r12), %eax
	testq	%rdi, %rdi
	je	.L50
	testb	$2, %al
	je	.L63
.L50:
	testb	$1, %al
	jne	.L64
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$24, %esi
	call	OPENSSL_cleanse@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$208, %edx
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	movslq	12(%r12), %rsi
	salq	$3, %rsi
	testb	$8, %al
	jne	.L65
	movl	$194, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movl	20(%r12), %eax
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	$192, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_secure_clear_free@PLT
	movl	20(%r12), %eax
	jmp	.L50
	.cfi_endproc
.LFE295:
	.size	BN_clear_free, .-BN_clear_free
	.p2align 4
	.globl	BN_free
	.type	BN_free, @function
BN_free:
.LFB296:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L76
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movl	20(%rdi), %eax
	testb	$2, %al
	je	.L79
.L69:
	testb	$1, %al
	jne	.L80
.L66:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testb	$8, %al
	jne	.L81
	movl	$196, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	20(%r12), %eax
	testb	$1, %al
	je	.L66
.L80:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$219, %edx
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	movslq	12(%r12), %rsi
	movl	$192, %ecx
	leaq	.LC0(%rip), %rdx
	salq	$3, %rsi
	call	CRYPTO_secure_clear_free@PLT
	movl	20(%r12), %eax
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE296:
	.size	BN_free, .-BN_free
	.p2align 4
	.globl	bn_init
	.type	bn_init, @function
bn_init:
.LFB297:
	.cfi_startproc
	endbr64
	movdqa	nilbn.7392(%rip), %xmm0
	movq	16+nilbn.7392(%rip), %rax
	movups	%xmm0, (%rdi)
	movq	%rax, 16(%rdi)
	ret
	.cfi_endproc
.LFE297:
	.size	bn_init, .-bn_init
	.p2align 4
	.globl	BN_new
	.type	BN_new, @function
BN_new:
.LFB298:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$234, %edx
	movl	$24, %edi
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L87
	movl	$1, 20(%rax)
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	movl	$235, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$113, %esi
	movl	$3, %edi
	movq	%rax, -8(%rbp)
	call	ERR_put_error@PLT
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE298:
	.size	BN_new, .-BN_new
	.p2align 4
	.globl	BN_secure_new
	.type	BN_secure_new, @function
BN_secure_new:
.LFB299:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$234, %edx
	movl	$24, %edi
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L92
	movl	$9, 20(%rax)
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	movl	$235, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$113, %esi
	movl	$3, %edi
	movq	%rax, -8(%rbp)
	call	ERR_put_error@PLT
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE299:
	.size	BN_secure_new, .-BN_secure_new
	.p2align 4
	.globl	bn_expand2
	.type	bn_expand2, @function
bn_expand2:
.LFB301:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	cmpl	%esi, 12(%rdi)
	jl	.L106
.L104:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	movl	%esi, %ebx
	cmpl	$8388607, %esi
	jg	.L107
	movl	20(%rdi), %eax
	testb	$2, %al
	jne	.L108
	movslq	%esi, %rdi
	salq	$3, %rdi
	testb	$8, %al
	jne	.L109
	movl	$268, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r13
.L98:
	testq	%r13, %r13
	je	.L110
	movslq	8(%r12), %rdx
	movq	(%r12), %rdi
	testl	%edx, %edx
	jle	.L100
	movq	%rdi, %rsi
	salq	$3, %rdx
	movq	%r13, %rdi
	call	memcpy@PLT
	movq	(%r12), %rdi
.L100:
	testq	%rdi, %rdi
	je	.L102
	movslq	12(%r12), %rsi
	salq	$3, %rsi
	testb	$8, 20(%r12)
	jne	.L111
	movl	$194, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
.L102:
	movq	%r13, (%r12)
	movq	%r12, %rax
	movl	%ebx, 12(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	movl	$266, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_secure_zalloc@PLT
	movq	%rax, %r13
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L111:
	movl	$192, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_secure_clear_free@PLT
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L107:
	movl	$258, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$114, %edx
	xorl	%r12d, %r12d
	movl	$120, %esi
	movl	$3, %edi
	call	ERR_put_error@PLT
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L108:
	movl	$262, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$105, %edx
	xorl	%r12d, %r12d
	movl	$120, %esi
	movl	$3, %edi
	call	ERR_put_error@PLT
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L110:
	movl	$270, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$120, %esi
	movl	$3, %edi
	call	ERR_put_error@PLT
	jmp	.L104
	.cfi_endproc
.LFE301:
	.size	bn_expand2, .-bn_expand2
	.p2align 4
	.globl	BN_dup
	.type	BN_dup, @function
BN_dup:
.LFB302:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	testq	%rdi, %rdi
	je	.L135
	movq	%rdi, %rbx
	movl	$234, %edx
	leaq	.LC0(%rip), %rsi
	movl	$24, %edi
	testb	$8, 20(%rbx)
	jne	.L136
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L118
	movl	$1, 20(%rax)
.L117:
	cmpq	%r12, %rbx
	je	.L112
	movslq	8(%rbx), %rsi
	cmpl	12(%r12), %esi
	jg	.L137
.L119:
	testl	%esi, %esi
	jle	.L123
	leaq	0(,%rsi,8), %rdx
	movq	(%r12), %rdi
	movq	(%rbx), %rsi
	call	memcpy@PLT
	movl	8(%rbx), %esi
.L123:
	movl	16(%rbx), %eax
	movl	%esi, 8(%r12)
	movl	%eax, 16(%r12)
.L112:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	.cfi_restore_state
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L118
	movl	$9, 20(%rax)
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L137:
	movq	%r12, %rdi
	call	bn_expand2.part.0
	testq	%rax, %rax
	je	.L120
	movslq	8(%rbx), %rsi
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L120:
	movl	20(%r12), %eax
	testb	$2, %al
	je	.L138
.L122:
	testb	$1, %al
	je	.L135
	movq	%r12, %rdi
	movl	$219, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%r12d, %r12d
	call	CRYPTO_free@PLT
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L118:
	movl	$235, %r8d
	movl	$65, %edx
	movl	$113, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L135:
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	movq	(%r12), %rdi
	testb	$8, %al
	jne	.L139
	movl	$196, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	20(%r12), %eax
	jmp	.L122
.L139:
	movslq	12(%r12), %rsi
	movl	$192, %ecx
	leaq	.LC0(%rip), %rdx
	salq	$3, %rsi
	call	CRYPTO_secure_clear_free@PLT
	movl	20(%r12), %eax
	jmp	.L122
	.cfi_endproc
.LFE302:
	.size	BN_dup, .-BN_dup
	.p2align 4
	.globl	BN_copy
	.type	BN_copy, @function
BN_copy:
.LFB303:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpq	%rsi, %rdi
	je	.L144
	movq	%rsi, %r12
	movslq	8(%rsi), %rsi
	cmpl	12(%rdi), %esi
	jg	.L149
.L142:
	testl	%esi, %esi
	jg	.L150
.L143:
	movl	16(%r12), %eax
	movl	%esi, 8(%rbx)
	movl	%eax, 16(%rbx)
	movq	%rbx, %rax
.L140:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	.cfi_restore_state
	leaq	0(,%rsi,8), %rdx
	movq	(%rbx), %rdi
	movq	(%r12), %rsi
	call	memcpy@PLT
	movl	8(%r12), %esi
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L149:
	call	bn_expand2.part.0
	testq	%rax, %rax
	je	.L140
	movslq	8(%r12), %rsi
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L144:
	popq	%rbx
	movq	%rdi, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE303:
	.size	BN_copy, .-BN_copy
	.p2align 4
	.globl	BN_swap
	.type	BN_swap, @function
BN_swap:
.LFB304:
	.cfi_startproc
	endbr64
	movq	(%rsi), %r10
	movl	20(%rsi), %eax
	movq	(%rdi), %r9
	movq	8(%rdi), %r8
	movl	20(%rdi), %edx
	movl	16(%rdi), %ecx
	movq	%r10, (%rdi)
	movl	8(%rsi), %r10d
	movl	%r10d, 8(%rdi)
	movl	12(%rsi), %r10d
	movl	%r10d, 12(%rdi)
	movl	16(%rsi), %r10d
	movl	%r10d, 16(%rdi)
	movq	%r8, 8(%rsi)
	movl	%eax, %r8d
	andl	$1, %eax
	movl	%ecx, 16(%rsi)
	movl	%edx, %ecx
	andl	$14, %r8d
	andl	$14, %edx
	andl	$1, %ecx
	orl	%edx, %eax
	movq	%r9, (%rsi)
	orl	%r8d, %ecx
	movl	%ecx, 20(%rdi)
	movl	%eax, 20(%rsi)
	ret
	.cfi_endproc
.LFE304:
	.size	BN_swap, .-BN_swap
	.p2align 4
	.globl	BN_clear
	.type	BN_clear, @function
BN_clear:
.LFB305:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L162
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L154
	movslq	12(%rbx), %rsi
	salq	$3, %rsi
	call	OPENSSL_cleanse@PLT
.L154:
	movl	$0, 16(%rbx)
	movl	$0, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE305:
	.size	BN_clear, .-BN_clear
	.p2align 4
	.globl	BN_get_word
	.type	BN_get_word, @function
BN_get_word:
.LFB306:
	.cfi_startproc
	endbr64
	cmpl	$1, 8(%rdi)
	movq	$-1, %rax
	jg	.L165
	movl	$0, %eax
	je	.L169
.L165:
	ret
	.p2align 4,,10
	.p2align 3
.L169:
	movq	(%rdi), %rax
	movq	(%rax), %rax
	ret
	.cfi_endproc
.LFE306:
	.size	BN_get_word, .-BN_get_word
	.p2align 4
	.globl	BN_set_word
	.type	BN_set_word, @function
BN_set_word:
.LFB307:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	12(%rdi), %eax
	movq	%rdi, %rbx
	testl	%eax, %eax
	jle	.L183
	movq	(%rdi), %r13
.L180:
	xorl	%eax, %eax
	testq	%r12, %r12
	movl	$0, 16(%rbx)
	movl	$1, %r14d
	setne	%al
	movq	%r12, 0(%r13)
	movl	%eax, 8(%rbx)
.L170:
	popq	%rbx
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L183:
	.cfi_restore_state
	movl	20(%rdi), %eax
	movl	%eax, %r14d
	andl	$2, %r14d
	jne	.L184
	testb	$8, %al
	je	.L174
	movl	$266, %edx
	leaq	.LC0(%rip), %rsi
	movl	$8, %edi
	call	CRYPTO_secure_zalloc@PLT
	movq	%rax, %r13
.L175:
	testq	%r13, %r13
	je	.L185
	movslq	8(%rbx), %rdx
	movq	(%rbx), %rdi
	testl	%edx, %edx
	jle	.L177
	movq	%rdi, %rsi
	salq	$3, %rdx
	movq	%r13, %rdi
	call	memcpy@PLT
	movq	(%rbx), %rdi
.L177:
	testq	%rdi, %rdi
	je	.L179
	movslq	12(%rbx), %rsi
	salq	$3, %rsi
	testb	$8, 20(%rbx)
	jne	.L186
	movl	$194, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
.L179:
	movq	%r13, (%rbx)
	movl	$1, 12(%rbx)
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L174:
	movl	$268, %edx
	leaq	.LC0(%rip), %rsi
	movl	$8, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r13
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L186:
	movl	$192, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_secure_clear_free@PLT
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L184:
	movl	$262, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$105, %edx
	xorl	%r14d, %r14d
	movl	$120, %esi
	movl	$3, %edi
	call	ERR_put_error@PLT
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L185:
	movl	$270, %r8d
	movl	$65, %edx
	movl	$120, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L170
	.cfi_endproc
.LFE307:
	.size	BN_set_word, .-BN_set_word
	.p2align 4
	.globl	BN_bin2bn
	.type	BN_bin2bn, @function
BN_bin2bn:
.LFB308:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	testq	%rdx, %rdx
	je	.L221
.L188:
	testl	%r14d, %r14d
	jle	.L205
.L204:
	cmpb	$0, (%rbx)
	je	.L192
.L205:
	testl	%r14d, %r14d
	je	.L191
	subl	$1, %r14d
	movl	%r14d, %esi
	movl	%r14d, %r13d
	shrl	$3, %esi
	andl	$7, %r13d
	addl	$1, %esi
	movl	%esi, %r12d
	cmpl	12(%r15), %esi
	jg	.L222
.L194:
	movl	%esi, 8(%r15)
	movq	(%r15), %r8
	leaq	1(%rbx,%r14), %rdi
	movl	%esi, %ecx
	movl	$0, 16(%r15)
	xorl	%eax, %eax
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L224:
	subl	$1, %r13d
	cmpq	%rdi, %rbx
	je	.L223
.L200:
	movzbl	(%rbx), %edx
	salq	$8, %rax
	addq	$1, %rbx
	orq	%rdx, %rax
	testl	%r13d, %r13d
	jne	.L224
	leal	-1(%rcx), %edx
	movl	$7, %r13d
	movq	%rax, (%r8,%rdx,8)
	movq	%rdx, %rcx
	xorl	%eax, %eax
	cmpq	%rdi, %rbx
	jne	.L200
.L223:
	leaq	(%r8,%rsi,8), %rax
	.p2align 4,,10
	.p2align 3
.L202:
	subq	$8, %rax
	cmpq	$0, (%rax)
	jne	.L201
	subl	$1, %r12d
	jne	.L202
	movl	$0, 8(%r15)
	movq	%r15, %rax
	movl	$0, 16(%r15)
.L187:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L192:
	.cfi_restore_state
	addq	$1, %rbx
	subl	$1, %r14d
	jne	.L204
.L191:
	movl	$0, 8(%r15)
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L201:
	.cfi_restore_state
	movl	%r12d, 8(%r15)
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L222:
	.cfi_restore_state
	movq	%r15, %rdi
	movq	%r8, -64(%rbp)
	movl	%esi, -56(%rbp)
	call	bn_expand2.part.0
	movl	-56(%rbp), %esi
	movq	-64(%rbp), %r8
	testq	%rax, %rax
	jne	.L194
	testq	%r8, %r8
	je	.L187
	movl	20(%r8), %edx
	testb	$2, %dl
	je	.L225
.L196:
	andl	$1, %edx
	je	.L187
	movl	$219, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r8, %rdi
	movq	%rax, -56(%rbp)
	call	CRYPTO_free@PLT
	movq	-56(%rbp), %rax
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L221:
	movl	$234, %edx
	leaq	.LC0(%rip), %rsi
	movl	$24, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L226
	movl	$1, 20(%rax)
	movq	%rax, %r8
	jmp	.L188
.L226:
	movl	$235, %r8d
	movl	$65, %edx
	movl	$113, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L187
.L225:
	andl	$8, %edx
	movq	%rax, -64(%rbp)
	movq	(%r8), %rdi
	jne	.L227
	movl	$196, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r8, -56(%rbp)
	call	CRYPTO_free@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rax
	movl	20(%r8), %edx
	jmp	.L196
.L227:
	movslq	12(%r8), %rsi
	leaq	.LC0(%rip), %rdx
	movl	$192, %ecx
	movq	%r8, -56(%rbp)
	salq	$3, %rsi
	call	CRYPTO_secure_clear_free@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rax
	movl	20(%r8), %edx
	jmp	.L196
	.cfi_endproc
.LFE308:
	.size	BN_bin2bn, .-BN_bin2bn
	.p2align 4
	.globl	BN_bn2binpad
	.type	BN_bn2binpad, @function
BN_bn2binpad:
.LFB310:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	js	.L239
	movl	%edx, %r13d
	movq	%rdi, %rbx
	movq	%rsi, %r12
	call	BN_num_bits
	leal	14(%rax), %edx
	addl	$7, %eax
	cmovs	%edx, %eax
	sarl	$3, %eax
	cmpl	%eax, %r13d
	jl	.L246
.L230:
	movl	12(%rbx), %eax
	movslq	%r13d, %rsi
	sall	$3, %eax
	cltq
	testq	%rax, %rax
	je	.L247
	movl	8(%rbx), %edi
	subq	$1, %rax
	addq	%r12, %rsi
	leal	0(,%rdi,8), %edx
	movslq	%edx, %rdx
	testl	%r13d, %r13d
	je	.L229
	movq	%rsi, %r8
	xorl	%edi, %edi
	subq	%rdx, %r8
	.p2align 4,,10
	.p2align 3
.L237:
	movq	(%rbx), %r9
	movq	%rdi, %rdx
	movl	%edi, %ecx
	andq	$-8, %rdx
	andl	$7, %ecx
	movq	(%r9,%rdx), %rdx
	sall	$3, %ecx
	shrq	%cl, %rdx
	movq	%r8, %rcx
	subq	%rsi, %rcx
	subq	$1, %rsi
	sarq	$63, %rcx
	andl	%ecx, %edx
	movb	%dl, (%rsi)
	movq	%rdi, %rdx
	subq	%rax, %rdx
	shrq	$63, %rdx
	addq	%rdx, %rdi
	cmpq	%rsi, %r12
	jne	.L237
.L229:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L248
	addq	$40, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L246:
	.cfi_restore_state
	movq	16(%rbx), %rax
	movdqu	(%rbx), %xmm0
	movq	%rax, -48(%rbp)
	movaps	%xmm0, -64(%rbp)
	movl	-56(%rbp), %eax
	testl	%eax, %eax
	jle	.L231
	movq	-64(%rbp), %rcx
	movslq	%eax, %rdx
	leaq	(%rcx,%rdx,8), %rdx
	.p2align 4,,10
	.p2align 3
.L233:
	subq	$8, %rdx
	cmpq	$0, (%rdx)
	jne	.L232
	subl	$1, %eax
	jne	.L233
.L232:
	movl	%eax, -56(%rbp)
.L231:
	testl	%eax, %eax
	jne	.L234
	movl	$0, -48(%rbp)
.L234:
	leaq	-64(%rbp), %rdi
	call	BN_num_bits
	leal	14(%rax), %edx
	addl	$7, %eax
	cmovs	%edx, %eax
	sarl	$3, %eax
	cmpl	%eax, %r13d
	jge	.L230
	.p2align 4,,10
	.p2align 3
.L239:
	movl	$-1, %r13d
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L247:
	movq	%r12, %rdi
	call	OPENSSL_cleanse@PLT
	jmp	.L229
.L248:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE310:
	.size	BN_bn2binpad, .-BN_bn2binpad
	.p2align 4
	.globl	BN_bn2bin
	.type	BN_bn2bin, @function
BN_bn2bin:
.LFB311:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	call	BN_num_bits
	leal	14(%rax), %edx
	addl	$7, %eax
	cmovs	%edx, %eax
	movl	12(%rbx), %edx
	sall	$3, %edx
	sarl	$3, %eax
	movslq	%edx, %rdx
	movslq	%eax, %r10
	testq	%rdx, %rdx
	je	.L258
	movl	8(%rbx), %esi
	subq	$1, %rdx
	xorl	%r8d, %r8d
	leal	0(,%rsi,8), %ecx
	leaq	(%r12,%r10), %rsi
	movslq	%ecx, %rcx
	movq	%rsi, %r9
	subq	%rcx, %r9
	testq	%r10, %r10
	je	.L249
	.p2align 4,,10
	.p2align 3
.L252:
	movq	(%rbx), %r10
	movq	%r8, %rdi
	movl	%r8d, %ecx
	andq	$-8, %rdi
	andl	$7, %ecx
	movq	(%r10,%rdi), %rdi
	sall	$3, %ecx
	shrq	%cl, %rdi
	movq	%r9, %rcx
	subq	%rsi, %rcx
	subq	$1, %rsi
	sarq	$63, %rcx
	andl	%ecx, %edi
	movq	%r8, %rcx
	subq	%rdx, %rcx
	movb	%dil, (%rsi)
	shrq	$63, %rcx
	addq	%rcx, %r8
	cmpq	%rsi, %r12
	jne	.L252
.L249:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L258:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%r10, %rsi
	movl	%eax, -20(%rbp)
	call	OPENSSL_cleanse@PLT
	movl	-20(%rbp), %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE311:
	.size	BN_bn2bin, .-BN_bn2bin
	.p2align 4
	.globl	BN_lebin2bn
	.type	BN_lebin2bn, @function
BN_lebin2bn:
.LFB312:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	testq	%rdx, %rdx
	je	.L293
.L260:
	movslq	%r12d, %rdi
	addq	%rdi, %rbx
	testl	%r12d, %r12d
	jle	.L277
.L276:
	cmpb	$0, -1(%rbx)
	je	.L264
.L277:
	testl	%r12d, %r12d
	je	.L263
	subl	$1, %r12d
	movl	%r12d, %esi
	movl	%r12d, %r14d
	shrl	$3, %esi
	andl	$7, %r14d
	addl	$1, %esi
	movl	%esi, %r13d
	cmpl	12(%r15), %esi
	jg	.L294
.L266:
	notq	%r12
	movl	%esi, 8(%r15)
	movq	(%r15), %r8
	movl	%esi, %edi
	movl	$0, 16(%r15)
	leaq	(%rbx,%r12), %rcx
	xorl	%eax, %eax
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L296:
	subl	$1, %r14d
	cmpq	%rcx, %rbx
	je	.L295
.L272:
	movzbl	-1(%rbx), %edx
	salq	$8, %rax
	subq	$1, %rbx
	orq	%rdx, %rax
	testl	%r14d, %r14d
	jne	.L296
	leal	-1(%rdi), %edx
	movl	$7, %r14d
	movq	%rax, (%r8,%rdx,8)
	movq	%rdx, %rdi
	xorl	%eax, %eax
	cmpq	%rcx, %rbx
	jne	.L272
.L295:
	leaq	(%r8,%rsi,8), %rax
	.p2align 4,,10
	.p2align 3
.L274:
	subq	$8, %rax
	cmpq	$0, (%rax)
	jne	.L273
	subl	$1, %r13d
	jne	.L274
	movl	$0, 8(%r15)
	movq	%r15, %rax
	movl	$0, 16(%r15)
.L259:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L264:
	.cfi_restore_state
	subq	$1, %rbx
	subl	$1, %r12d
	jne	.L276
.L263:
	movl	$0, 8(%r15)
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L273:
	.cfi_restore_state
	movl	%r13d, 8(%r15)
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L294:
	.cfi_restore_state
	movq	%r15, %rdi
	movq	%r8, -64(%rbp)
	movl	%esi, -56(%rbp)
	call	bn_expand2.part.0
	movl	-56(%rbp), %esi
	movq	-64(%rbp), %r8
	testq	%rax, %rax
	jne	.L266
	testq	%r8, %r8
	je	.L259
	movl	20(%r8), %edx
	testb	$2, %dl
	je	.L297
.L268:
	andl	$1, %edx
	je	.L259
	movl	$219, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r8, %rdi
	movq	%rax, -56(%rbp)
	call	CRYPTO_free@PLT
	movq	-56(%rbp), %rax
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L293:
	movl	$234, %edx
	leaq	.LC0(%rip), %rsi
	movl	$24, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L298
	movl	$1, 20(%rax)
	movq	%rax, %r8
	jmp	.L260
.L298:
	movl	$235, %r8d
	movl	$65, %edx
	movl	$113, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L259
.L297:
	andl	$8, %edx
	movq	%rax, -64(%rbp)
	movq	(%r8), %rdi
	jne	.L299
	movl	$196, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r8, -56(%rbp)
	call	CRYPTO_free@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rax
	movl	20(%r8), %edx
	jmp	.L268
.L299:
	movslq	12(%r8), %rsi
	leaq	.LC0(%rip), %rdx
	movl	$192, %ecx
	movq	%r8, -56(%rbp)
	salq	$3, %rsi
	call	CRYPTO_secure_clear_free@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rax
	movl	20(%r8), %edx
	jmp	.L268
	.cfi_endproc
.LFE312:
	.size	BN_lebin2bn, .-BN_lebin2bn
	.p2align 4
	.globl	BN_bn2lebinpad
	.type	BN_bn2lebinpad, @function
BN_bn2lebinpad:
.LFB313:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	js	.L311
	movl	%edx, %r13d
	movq	%rdi, %rbx
	movq	%rsi, %r12
	call	BN_num_bits
	leal	14(%rax), %edx
	addl	$7, %eax
	cmovs	%edx, %eax
	sarl	$3, %eax
	cmpl	%eax, %r13d
	jl	.L318
.L302:
	movl	12(%rbx), %eax
	movslq	%r13d, %rsi
	sall	$3, %eax
	cltq
	testq	%rax, %rax
	je	.L319
	movl	8(%rbx), %edi
	subq	$1, %rax
	leal	0(,%rdi,8), %edx
	movslq	%edx, %rdx
	testl	%r13d, %r13d
	je	.L301
	movq	%rdx, %r8
	subq	%rdx, %rsi
	leaq	(%r12,%rdx), %r9
	xorl	%edi, %edi
	negq	%r8
	.p2align 4,,10
	.p2align 3
.L309:
	movq	(%rbx), %r10
	movq	%rdi, %rdx
	movl	%edi, %ecx
	andq	$-8, %rdx
	andl	$7, %ecx
	movq	(%r10,%rdx), %rdx
	sall	$3, %ecx
	shrq	%cl, %rdx
	movq	%r8, %rcx
	sarq	$63, %rcx
	andl	%ecx, %edx
	movb	%dl, (%r9,%r8)
	movq	%rdi, %rdx
	addq	$1, %r8
	subq	%rax, %rdx
	shrq	$63, %rdx
	addq	%rdx, %rdi
	cmpq	%r8, %rsi
	jne	.L309
.L301:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L320
	addq	$40, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L318:
	.cfi_restore_state
	movq	16(%rbx), %rax
	movdqu	(%rbx), %xmm0
	movq	%rax, -48(%rbp)
	movaps	%xmm0, -64(%rbp)
	movl	-56(%rbp), %eax
	testl	%eax, %eax
	jle	.L303
	movq	-64(%rbp), %rcx
	movslq	%eax, %rdx
	leaq	(%rcx,%rdx,8), %rdx
	.p2align 4,,10
	.p2align 3
.L305:
	subq	$8, %rdx
	cmpq	$0, (%rdx)
	jne	.L304
	subl	$1, %eax
	jne	.L305
.L304:
	movl	%eax, -56(%rbp)
.L303:
	testl	%eax, %eax
	jne	.L306
	movl	$0, -48(%rbp)
.L306:
	leaq	-64(%rbp), %rdi
	call	BN_num_bits
	leal	14(%rax), %edx
	addl	$7, %eax
	cmovs	%edx, %eax
	sarl	$3, %eax
	cmpl	%eax, %r13d
	jge	.L302
	.p2align 4,,10
	.p2align 3
.L311:
	movl	$-1, %r13d
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L319:
	movq	%r12, %rdi
	call	OPENSSL_cleanse@PLT
	jmp	.L301
.L320:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE313:
	.size	BN_bn2lebinpad, .-BN_bn2lebinpad
	.p2align 4
	.globl	BN_ucmp
	.type	BN_ucmp, @function
BN_ucmp:
.LFB314:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	movl	%eax, %r8d
	subl	8(%rsi), %r8d
	jne	.L321
	movq	(%rdi), %rdi
	movq	(%rsi), %rsi
	subl	$1, %eax
	js	.L321
	cltq
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L323:
	subq	$1, %rax
	testl	%eax, %eax
	js	.L321
.L324:
	movq	(%rdi,%rax,8), %rdx
	movq	(%rsi,%rax,8), %rcx
	cmpq	%rcx, %rdx
	je	.L323
	cmpq	%rdx, %rcx
	sbbl	%r8d, %r8d
	andl	$2, %r8d
	subl	$1, %r8d
.L321:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE314:
	.size	BN_ucmp, .-BN_ucmp
	.p2align 4
	.globl	BN_cmp
	.type	BN_cmp, @function
BN_cmp:
.LFB315:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L340
	testq	%rsi, %rsi
	je	.L340
	movl	16(%rdi), %eax
	cmpl	16(%rsi), %eax
	je	.L331
	cmpl	$1, %eax
	sbbl	%eax, %eax
	andl	$2, %eax
	subl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L340:
	movl	$-1, %eax
	testq	%rdi, %rdi
	je	.L343
.L327:
	ret
	.p2align 4,,10
	.p2align 3
.L343:
	xorl	%eax, %eax
	testq	%rsi, %rsi
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L331:
	cmpl	$1, %eax
	movl	8(%rdi), %edx
	sbbl	%r8d, %r8d
	orl	$1, %r8d
	cmpl	$1, %eax
	sbbl	%eax, %eax
	andl	$2, %eax
	subl	$1, %eax
	cmpl	8(%rsi), %edx
	jg	.L327
	jl	.L339
	subl	$1, %edx
	js	.L338
	movq	(%rdi), %rdi
	movq	(%rsi), %rcx
	movslq	%edx, %rdx
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L344:
	jb	.L339
	subq	$1, %rdx
	testl	%edx, %edx
	js	.L338
.L333:
	movq	(%rcx,%rdx,8), %rsi
	cmpq	%rsi, (%rdi,%rdx,8)
	jbe	.L344
	ret
	.p2align 4,,10
	.p2align 3
.L339:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L338:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE315:
	.size	BN_cmp, .-BN_cmp
	.p2align 4
	.globl	BN_set_bit
	.type	BN_set_bit, @function
BN_set_bit:
.LFB316:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	js	.L360
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	sarl	$6, %r13d
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	andl	$63, %ebx
	subq	$8, %rsp
	movslq	8(%rdi), %rax
	cmpl	%r13d, %eax
	jg	.L361
	leal	1(%r13), %r15d
	cmpl	12(%rdi), %r15d
	jg	.L362
	movq	(%rdi), %r14
.L353:
	movl	%r13d, %edx
	movl	$8, %ecx
	leaq	(%r14,%rax,8), %rdi
	subl	%eax, %edx
	cmpl	%eax, %r13d
	leaq	8(,%rdx,8), %rdx
	cmovl	%rcx, %rdx
	xorl	%esi, %esi
	call	memset@PLT
.L352:
	movl	%r15d, 8(%r12)
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L361:
	movq	(%rdi), %r14
.L349:
	movslq	%r13d, %r13
	movl	%ebx, %ecx
	movl	$1, %eax
	salq	%cl, %rax
	orq	%rax, (%r14,%r13,8)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L362:
	.cfi_restore_state
	movl	%r15d, %esi
	call	bn_expand2.part.0
	testq	%rax, %rax
	jne	.L363
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L360:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L363:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movslq	8(%r12), %rax
	movq	(%r12), %r14
	cmpl	%eax, %r13d
	jl	.L352
	jmp	.L353
	.cfi_endproc
.LFE316:
	.size	BN_set_bit, .-BN_set_bit
	.p2align 4
	.globl	BN_clear_bit
	.type	BN_clear_bit, @function
BN_clear_bit:
.LFB317:
	.cfi_startproc
	endbr64
	movl	%esi, %ecx
	xorl	%r8d, %r8d
	testl	%esi, %esi
	js	.L364
	movl	%esi, %edx
	movl	8(%rdi), %eax
	sarl	$6, %edx
	cmpl	%edx, %eax
	jle	.L364
	movq	(%rdi), %r8
	movl	$1, %esi
	movslq	%edx, %rdx
	salq	%cl, %rsi
	notq	%rsi
	andq	%rsi, (%r8,%rdx,8)
	movslq	%eax, %rdx
	leaq	(%r8,%rdx,8), %rdx
	.p2align 4,,10
	.p2align 3
.L367:
	subq	$8, %rdx
	cmpq	$0, (%rdx)
	jne	.L366
	subl	$1, %eax
	jne	.L367
	movl	$0, 8(%rdi)
	movl	$1, %r8d
	movl	$0, 16(%rdi)
.L364:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L366:
	movl	$1, %r8d
	movl	%eax, 8(%rdi)
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE317:
	.size	BN_clear_bit, .-BN_clear_bit
	.p2align 4
	.globl	BN_is_bit_set
	.type	BN_is_bit_set, @function
BN_is_bit_set:
.LFB318:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testl	%esi, %esi
	js	.L372
	movl	%esi, %edx
	sarl	$6, %edx
	cmpl	%edx, 8(%rdi)
	jle	.L372
	movq	(%rdi), %rax
	movslq	%edx, %rdx
	movl	%esi, %ecx
	movq	(%rax,%rdx,8), %rax
	shrq	%cl, %rax
	andl	$1, %eax
.L372:
	ret
	.cfi_endproc
.LFE318:
	.size	BN_is_bit_set, .-BN_is_bit_set
	.p2align 4
	.globl	BN_mask_bits
	.type	BN_mask_bits, @function
BN_mask_bits:
.LFB319:
	.cfi_startproc
	endbr64
	xorl	%r8d, %r8d
	testl	%esi, %esi
	js	.L376
	movl	%esi, %edx
	sarl	$6, %edx
	cmpl	%edx, 8(%rdi)
	jle	.L376
	andl	$63, %esi
	jne	.L378
	movl	%edx, 8(%rdi)
	testl	%edx, %edx
	jg	.L388
.L379:
	movl	$0, 16(%rdi)
	movl	$1, %r8d
.L376:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L378:
	movq	(%rdi), %r9
	movq	$-1, %r8
	movl	%esi, %ecx
	leal	1(%rdx), %eax
	salq	%cl, %r8
	movslq	%edx, %rdx
	notq	%r8
	andq	%r8, (%r9,%rdx,8)
.L380:
	movslq	%eax, %rdx
	leaq	(%r9,%rdx,8), %rdx
	.p2align 4,,10
	.p2align 3
.L382:
	subq	$8, %rdx
	cmpq	$0, (%rdx)
	jne	.L381
	subl	$1, %eax
	jne	.L382
.L381:
	movl	%eax, 8(%rdi)
	movl	$1, %r8d
	testl	%eax, %eax
	je	.L379
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L388:
	movq	(%rdi), %r9
	movl	%edx, %eax
	jmp	.L380
	.cfi_endproc
.LFE319:
	.size	BN_mask_bits, .-BN_mask_bits
	.p2align 4
	.globl	BN_set_negative
	.type	BN_set_negative, @function
BN_set_negative:
.LFB320:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	je	.L390
	movl	8(%rdi), %eax
	xorl	%esi, %esi
	testl	%eax, %eax
	setne	%sil
.L390:
	movl	%esi, 16(%rdi)
	ret
	.cfi_endproc
.LFE320:
	.size	BN_set_negative, .-BN_set_negative
	.p2align 4
	.globl	bn_cmp_words
	.type	bn_cmp_words, @function
bn_cmp_words:
.LFB321:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L394
	movslq	%edx, %rcx
	salq	$3, %rcx
	movq	-8(%rdi,%rcx), %r8
	movq	-8(%rsi,%rcx), %rcx
	cmpq	%rcx, %r8
	jne	.L405
	subl	$2, %edx
	js	.L394
	movslq	%edx, %rax
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L397:
	subq	$1, %rax
	testl	%eax, %eax
	js	.L406
.L398:
	movq	(%rdi,%rax,8), %rdx
	movq	(%rsi,%rax,8), %rcx
	cmpq	%rcx, %rdx
	je	.L397
	cmpq	%rdx, %rcx
	sbbl	%eax, %eax
	andl	$2, %eax
	subl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L406:
	xorl	%eax, %eax
.L394:
	ret
	.p2align 4,,10
	.p2align 3
.L405:
	cmpq	%r8, %rcx
	sbbl	%eax, %eax
	andl	$2, %eax
	subl	$1, %eax
	ret
	.cfi_endproc
.LFE321:
	.size	bn_cmp_words, .-bn_cmp_words
	.p2align 4
	.globl	bn_cmp_part_words
	.type	bn_cmp_part_words, @function
bn_cmp_part_words:
.LFB322:
	.cfi_startproc
	endbr64
	leal	-1(%rdx), %eax
	testl	%ecx, %ecx
	js	.L430
	je	.L411
	movslq	%edx, %rax
	movslq	%ecx, %rcx
	leaq	(%rdi,%rax,8), %rax
	.p2align 4,,10
	.p2align 3
.L412:
	cmpq	$0, -8(%rax,%rcx,8)
	jne	.L417
	subq	$1, %rcx
	testl	%ecx, %ecx
	jne	.L412
.L411:
	xorl	%r8d, %r8d
	testl	%edx, %edx
	je	.L407
	movslq	%edx, %rax
	salq	$3, %rax
	movq	-8(%rdi,%rax), %rcx
	movq	-8(%rsi,%rax), %rax
	cmpq	%rax, %rcx
	jne	.L431
	movl	%edx, %eax
	subl	$2, %eax
	js	.L407
	cltq
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L414:
	subq	$1, %rax
	testl	%eax, %eax
	js	.L432
.L415:
	movq	(%rdi,%rax,8), %rdx
	movq	(%rsi,%rax,8), %rcx
	cmpq	%rcx, %rdx
	je	.L414
	cmpq	%rdx, %rcx
	sbbl	%r8d, %r8d
	andl	$2, %r8d
	subl	$1, %r8d
.L407:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L430:
	movslq	%ecx, %r9
	cltq
	movslq	%edx, %r8
	notl	%ecx
	subq	%r9, %rax
	leaq	-16(%rsi,%r8,8), %r8
	salq	$3, %r9
	salq	$3, %rcx
	subq	%r9, %r8
	leaq	(%rsi,%rax,8), %rax
	subq	%rcx, %r8
	cmpq	$0, (%rax)
	movq	%r8, %rcx
	jne	.L416
	.p2align 4,,10
	.p2align 3
.L433:
	subq	$8, %rax
	cmpq	%rax, %rcx
	je	.L411
	cmpq	$0, (%rax)
	je	.L433
.L416:
	movl	$-1, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L417:
	movl	$1, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L431:
	cmpq	%rcx, %rax
	sbbl	%r8d, %r8d
	andl	$2, %r8d
	subl	$1, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L432:
	xorl	%r8d, %r8d
	jmp	.L407
	.cfi_endproc
.LFE322:
	.size	bn_cmp_part_words, .-bn_cmp_part_words
	.p2align 4
	.globl	BN_consttime_swap
	.type	BN_consttime_swap, @function
BN_consttime_swap:
.LFB323:
	.cfi_startproc
	endbr64
	movq	%rdx, %rax
	cmpq	%rdx, %rsi
	je	.L434
	movq	%rdi, %r8
	subq	$1, %rdi
	movl	8(%rdx), %edx
	notq	%r8
	andq	%r8, %rdi
	shrq	$63, %rdi
	leaq	-1(%rdi), %r8
	movl	8(%rsi), %edi
	xorl	%edi, %edx
	movslq	%edx, %rdx
	andq	%r8, %rdx
	xorl	%edx, %edi
	movl	%edi, 8(%rsi)
	movl	16(%rsi), %edi
	xorl	%edx, 8(%rax)
	movl	16(%rax), %edx
	xorl	%edi, %edx
	movslq	%edx, %rdx
	andq	%r8, %rdx
	xorl	%edx, %edi
	movl	%edi, 16(%rsi)
	movl	20(%rsi), %edi
	xorl	%edx, 16(%rax)
	movl	20(%rax), %edx
	xorl	%edi, %edx
	andq	%r8, %rdx
	andl	$4, %edx
	xorl	%edx, %edi
	movl	%edi, 20(%rsi)
	xorl	%edx, 20(%rax)
	testl	%ecx, %ecx
	jle	.L434
	movq	(%rsi), %rdi
	movq	(%rax), %r9
	leal	-1(%rcx), %eax
	leaq	15(%rdi), %rdx
	subq	%r9, %rdx
	cmpq	$30, %rdx
	jbe	.L436
	cmpl	$1, %eax
	jbe	.L436
	movl	%ecx, %eax
	movq	%r8, %xmm2
	movq	%rdi, %rsi
	movq	%r9, %rdx
	shrl	%eax
	punpcklqdq	%xmm2, %xmm2
	salq	$4, %rax
	addq	%rdi, %rax
	.p2align 4,,10
	.p2align 3
.L437:
	movdqu	(%rsi), %xmm3
	movdqu	(%rdx), %xmm0
	addq	$16, %rsi
	addq	$16, %rdx
	pxor	%xmm3, %xmm0
	movdqa	%xmm3, %xmm1
	pand	%xmm2, %xmm0
	pxor	%xmm0, %xmm1
	movups	%xmm1, -16(%rsi)
	movdqu	-16(%rdx), %xmm4
	pxor	%xmm4, %xmm0
	movups	%xmm0, -16(%rdx)
	cmpq	%rax, %rsi
	jne	.L437
	movl	%ecx, %eax
	andl	$-2, %eax
	andl	$1, %ecx
	je	.L434
	salq	$3, %rax
	leaq	(%rdi,%rax), %rsi
	addq	%r9, %rax
	movq	(%rsi), %rcx
	movq	(%rax), %rdx
	xorq	%rcx, %rdx
	andq	%r8, %rdx
	xorq	%rdx, %rcx
	movq	%rcx, (%rsi)
	xorq	%rdx, (%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L436:
	movl	%eax, %esi
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L439:
	movq	(%rdi,%rcx,8), %rdx
	movq	(%r9,%rcx,8), %rax
	xorq	%rdx, %rax
	andq	%r8, %rax
	xorq	%rax, %rdx
	movq	%rdx, (%rdi,%rcx,8)
	xorq	%rax, (%r9,%rcx,8)
	movq	%rcx, %rax
	addq	$1, %rcx
	cmpq	%rsi, %rax
	jne	.L439
.L434:
	ret
	.cfi_endproc
.LFE323:
	.size	BN_consttime_swap, .-BN_consttime_swap
	.p2align 4
	.globl	BN_security_bits
	.type	BN_security_bits, @function
BN_security_bits:
.LFB324:
	.cfi_startproc
	endbr64
	cmpl	$15359, %edi
	jg	.L452
	cmpl	$7679, %edi
	jle	.L461
	movl	$192, %eax
	cmpl	$-1, %esi
	jne	.L462
.L449:
	ret
	.p2align 4,,10
	.p2align 3
.L452:
	movl	$256, %eax
.L450:
	cmpl	$-1, %esi
	je	.L449
.L462:
	cmpl	$159, %esi
	jle	.L457
	sarl	%esi
	cmpl	%esi, %eax
	cmovg	%esi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L461:
	cmpl	$3071, %edi
	jg	.L454
	cmpl	$2047, %edi
	jg	.L455
	xorl	%eax, %eax
	cmpl	$1023, %edi
	jle	.L449
	movl	$80, %eax
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L457:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L454:
	movl	$128, %eax
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L455:
	movl	$112, %eax
	jmp	.L450
	.cfi_endproc
.LFE324:
	.size	BN_security_bits, .-BN_security_bits
	.p2align 4
	.globl	BN_zero_ex
	.type	BN_zero_ex, @function
BN_zero_ex:
.LFB325:
	.cfi_startproc
	endbr64
	movl	$0, 16(%rdi)
	movl	$0, 8(%rdi)
	ret
	.cfi_endproc
.LFE325:
	.size	BN_zero_ex, .-BN_zero_ex
	.p2align 4
	.globl	BN_abs_is_word
	.type	BN_abs_is_word, @function
BN_abs_is_word:
.LFB326:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	cmpl	$1, %eax
	je	.L468
.L465:
	testq	%rsi, %rsi
	sete	%dl
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
	andl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L468:
	movq	(%rdi), %rdx
	cmpq	%rsi, (%rdx)
	jne	.L465
	ret
	.cfi_endproc
.LFE326:
	.size	BN_abs_is_word, .-BN_abs_is_word
	.p2align 4
	.globl	BN_is_zero
	.type	BN_is_zero, @function
BN_is_zero:
.LFB327:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	sete	%al
	ret
	.cfi_endproc
.LFE327:
	.size	BN_is_zero, .-BN_is_zero
	.p2align 4
	.globl	BN_is_one
	.type	BN_is_one, @function
BN_is_one:
.LFB328:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$1, 8(%rdi)
	je	.L474
.L470:
	ret
	.p2align 4,,10
	.p2align 3
.L474:
	movq	(%rdi), %rdx
	cmpq	$1, (%rdx)
	jne	.L470
	movl	16(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	sete	%al
	ret
	.cfi_endproc
.LFE328:
	.size	BN_is_one, .-BN_is_one
	.p2align 4
	.globl	BN_is_word
	.type	BN_is_word, @function
BN_is_word:
.LFB329:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	cmpl	$1, %eax
	je	.L482
	testq	%rsi, %rsi
	sete	%dl
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
	andl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L482:
	movq	(%rdi), %rdx
	cmpq	(%rdx), %rsi
	je	.L477
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L477:
	testq	%rsi, %rsi
	je	.L475
	movl	16(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	sete	%al
.L475:
	ret
	.cfi_endproc
.LFE329:
	.size	BN_is_word, .-BN_is_word
	.p2align 4
	.globl	BN_is_odd
	.type	BN_is_odd, @function
BN_is_odd:
.LFB330:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L483
	movq	(%rdi), %rax
	movq	(%rax), %rax
	andl	$1, %eax
.L483:
	ret
	.cfi_endproc
.LFE330:
	.size	BN_is_odd, .-BN_is_odd
	.p2align 4
	.globl	BN_is_negative
	.type	BN_is_negative, @function
BN_is_negative:
.LFB331:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	setne	%al
	ret
	.cfi_endproc
.LFE331:
	.size	BN_is_negative, .-BN_is_negative
	.p2align 4
	.globl	BN_to_montgomery
	.type	BN_to_montgomery, @function
BN_to_montgomery:
.LFB332:
	.cfi_startproc
	endbr64
	movq	%rdx, %r9
	movq	%rcx, %r8
	leaq	8(%rdx), %rdx
	movq	%r9, %rcx
	jmp	BN_mod_mul_montgomery@PLT
	.cfi_endproc
.LFE332:
	.size	BN_to_montgomery, .-BN_to_montgomery
	.p2align 4
	.globl	BN_with_flags
	.type	BN_with_flags, @function
BN_with_flags:
.LFB333:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movl	%edx, %r8d
	movl	20(%rsi), %edx
	movq	%rax, (%rdi)
	movl	8(%rsi), %eax
	andl	$-2, %edx
	orl	%r8d, %edx
	movl	%eax, 8(%rdi)
	movl	12(%rsi), %eax
	movl	%eax, 12(%rdi)
	movl	16(%rsi), %eax
	movl	%eax, 16(%rdi)
	movl	20(%rdi), %eax
	andl	$1, %eax
	orl	%eax, %edx
	orl	$2, %edx
	movl	%edx, 20(%rdi)
	ret
	.cfi_endproc
.LFE333:
	.size	BN_with_flags, .-BN_with_flags
	.p2align 4
	.globl	BN_GENCB_new
	.type	BN_GENCB_new, @function
BN_GENCB_new:
.LFB334:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$933, %edx
	movl	$24, %edi
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L492
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L492:
	.cfi_restore_state
	movl	$934, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$143, %esi
	movl	$3, %edi
	movq	%rax, -8(%rbp)
	call	ERR_put_error@PLT
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE334:
	.size	BN_GENCB_new, .-BN_GENCB_new
	.p2align 4
	.globl	BN_GENCB_free
	.type	BN_GENCB_free, @function
BN_GENCB_free:
.LFB335:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L493
	movl	$945, %edx
	leaq	.LC0(%rip), %rsi
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L493:
	ret
	.cfi_endproc
.LFE335:
	.size	BN_GENCB_free, .-BN_GENCB_free
	.p2align 4
	.globl	BN_set_flags
	.type	BN_set_flags, @function
BN_set_flags:
.LFB336:
	.cfi_startproc
	endbr64
	orl	%esi, 20(%rdi)
	ret
	.cfi_endproc
.LFE336:
	.size	BN_set_flags, .-BN_set_flags
	.p2align 4
	.globl	BN_get_flags
	.type	BN_get_flags, @function
BN_get_flags:
.LFB337:
	.cfi_startproc
	endbr64
	movl	20(%rdi), %eax
	andl	%esi, %eax
	ret
	.cfi_endproc
.LFE337:
	.size	BN_get_flags, .-BN_get_flags
	.p2align 4
	.globl	BN_GENCB_set_old
	.type	BN_GENCB_set_old, @function
BN_GENCB_set_old:
.LFB338:
	.cfi_startproc
	endbr64
	movl	$1, (%rdi)
	movq	%rdx, 8(%rdi)
	movq	%rsi, 16(%rdi)
	ret
	.cfi_endproc
.LFE338:
	.size	BN_GENCB_set_old, .-BN_GENCB_set_old
	.p2align 4
	.globl	BN_GENCB_set
	.type	BN_GENCB_set, @function
BN_GENCB_set:
.LFB339:
	.cfi_startproc
	endbr64
	movl	$2, (%rdi)
	movq	%rdx, 8(%rdi)
	movq	%rsi, 16(%rdi)
	ret
	.cfi_endproc
.LFE339:
	.size	BN_GENCB_set, .-BN_GENCB_set
	.p2align 4
	.globl	BN_GENCB_get_arg
	.type	BN_GENCB_get_arg, @function
BN_GENCB_get_arg:
.LFB340:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE340:
	.size	BN_GENCB_get_arg, .-BN_GENCB_get_arg
	.p2align 4
	.globl	bn_wexpand
	.type	bn_wexpand, @function
bn_wexpand:
.LFB341:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	cmpl	%esi, 12(%rdi)
	jl	.L513
.L511:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L513:
	.cfi_restore_state
	movl	%esi, %ebx
	cmpl	$8388607, %esi
	jg	.L514
	movl	20(%rdi), %eax
	testb	$2, %al
	jne	.L515
	movslq	%esi, %rdi
	salq	$3, %rdi
	testb	$8, %al
	jne	.L516
	movl	$268, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r13
.L505:
	testq	%r13, %r13
	je	.L517
	movslq	8(%r12), %rdx
	movq	(%r12), %rdi
	testl	%edx, %edx
	jle	.L507
	movq	%rdi, %rsi
	salq	$3, %rdx
	movq	%r13, %rdi
	call	memcpy@PLT
	movq	(%r12), %rdi
.L507:
	testq	%rdi, %rdi
	je	.L509
	movslq	12(%r12), %rsi
	salq	$3, %rsi
	testb	$8, 20(%r12)
	jne	.L518
	movl	$194, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
.L509:
	movq	%r13, (%r12)
	movq	%r12, %rax
	movl	%ebx, 12(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L516:
	.cfi_restore_state
	movl	$266, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_secure_zalloc@PLT
	movq	%rax, %r13
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L518:
	movl	$192, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_secure_clear_free@PLT
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L514:
	movl	$258, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$114, %edx
	xorl	%r12d, %r12d
	movl	$120, %esi
	movl	$3, %edi
	call	ERR_put_error@PLT
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L515:
	movl	$262, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$105, %edx
	xorl	%r12d, %r12d
	movl	$120, %esi
	movl	$3, %edi
	call	ERR_put_error@PLT
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L517:
	movl	$270, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$120, %esi
	movl	$3, %edi
	call	ERR_put_error@PLT
	jmp	.L511
	.cfi_endproc
.LFE341:
	.size	bn_wexpand, .-bn_wexpand
	.p2align 4
	.globl	bn_correct_top
	.type	bn_correct_top, @function
bn_correct_top:
.LFB342:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jle	.L520
	movq	(%rdi), %rcx
	movslq	%eax, %rdx
	leaq	(%rcx,%rdx,8), %rdx
	.p2align 4,,10
	.p2align 3
.L522:
	subq	$8, %rdx
	cmpq	$0, (%rdx)
	jne	.L521
	subl	$1, %eax
	jne	.L522
.L521:
	movl	%eax, 8(%rdi)
.L520:
	testl	%eax, %eax
	jne	.L519
	movl	$0, 16(%rdi)
.L519:
	ret
	.cfi_endproc
.LFE342:
	.size	bn_correct_top, .-bn_correct_top
	.section	.rodata
	.align 16
	.type	nilbn.7392, @object
	.size	nilbn.7392, 24
nilbn.7392:
	.zero	24
	.align 8
	.type	data_one.7356, @object
	.size	data_one.7356, 8
data_one.7356:
	.quad	1
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	const_one.7357, @object
	.size	const_one.7357, 24
const_one.7357:
	.quad	data_one.7356
	.long	1
	.long	1
	.long	0
	.long	2
	.local	bn_limit_bits_mont
	.comm	bn_limit_bits_mont,4,4
	.local	bn_limit_bits_high
	.comm	bn_limit_bits_high,4,4
	.local	bn_limit_bits_low
	.comm	bn_limit_bits_low,4,4
	.local	bn_limit_bits
	.comm	bn_limit_bits,4,4
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
