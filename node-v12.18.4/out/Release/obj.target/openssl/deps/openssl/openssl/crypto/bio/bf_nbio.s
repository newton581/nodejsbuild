	.file	"bf_nbio.c"
	.text
	.p2align 4
	.type	nbiof_callback_ctrl, @function
nbiof_callback_ctrl:
.LFB273:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2
	jmp	BIO_callback_ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L2:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE273:
	.size	nbiof_callback_ctrl, .-nbiof_callback_ctrl
	.p2align 4
	.type	nbiof_gets, @function
nbiof_gets:
.LFB274:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L5
	jmp	BIO_gets@PLT
	.p2align 4,,10
	.p2align 3
.L5:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE274:
	.size	nbiof_gets, .-nbiof_gets
	.p2align 4
	.type	nbiof_puts, @function
nbiof_puts:
.LFB275:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L7
	jmp	BIO_puts@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE275:
	.size	nbiof_puts, .-nbiof_puts
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/bio/bf_nbio.c"
	.text
	.p2align 4
	.type	nbiof_free, @function
nbiof_free:
.LFB269:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L12
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$75, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	56(%rdi), %rdi
	call	CRYPTO_free@PLT
	movq	$0, 56(%rbx)
	movl	$1, %eax
	movl	$0, 32(%rbx)
	movl	$0, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE269:
	.size	nbiof_free, .-nbiof_free
	.p2align 4
	.type	nbiof_new, @function
nbiof_new:
.LFB268:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$60, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$8, %edi
	subq	$8, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L21
	movq	$-1, (%rax)
	movl	$1, 32(%rbx)
	movq	%rax, 56(%rbx)
	movl	$1, %eax
.L17:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	movl	$61, %r8d
	movl	$65, %edx
	movl	$154, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L17
	.cfi_endproc
.LFE268:
	.size	nbiof_new, .-nbiof_new
	.p2align 4
	.type	nbiof_ctrl, @function
nbiof_ctrl:
.LFB272:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L26
	cmpl	$12, %esi
	je	.L26
	cmpl	$101, %esi
	jne	.L24
	movq	%r12, %rdi
	movl	$15, %esi
	movq	%rcx, -32(%rbp)
	movq	%rdx, -24(%rbp)
	call	BIO_clear_flags@PLT
	movq	64(%r12), %rdi
	movq	-32(%rbp), %rcx
	movl	$101, %esi
	movq	-24(%rbp), %rdx
	call	BIO_ctrl@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	BIO_copy_next_retry@PLT
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	addq	$16, %rsp
	xorl	%r13d, %r13d
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE272:
	.size	nbiof_ctrl, .-nbiof_ctrl
	.p2align 4
	.type	nbiof_read, @function
nbiof_read:
.LFB270:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L33
	cmpq	$0, 64(%rdi)
	movq	%rdi, %r12
	je	.L28
	movq	%rsi, %r13
	movl	$15, %esi
	movl	%edx, %ebx
	call	BIO_clear_flags@PLT
	leaq	-41(%rbp), %rdi
	movl	$1, %esi
	call	RAND_priv_bytes@PLT
	testl	%eax, %eax
	jle	.L35
	movzbl	-41(%rbp), %eax
	movl	%eax, %edx
	andl	$7, %edx
	cmpl	%ebx, %edx
	cmovg	%ebx, %edx
	testb	$7, %al
	je	.L37
	movq	64(%r12), %rdi
	movq	%r13, %rsi
	call	BIO_read@PLT
	testl	%eax, %eax
	js	.L38
.L28:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L39
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	%eax, -52(%rbp)
	call	BIO_copy_next_retry@PLT
	movl	-52(%rbp), %eax
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L33:
	xorl	%eax, %eax
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L37:
	movl	$9, %esi
	movq	%r12, %rdi
	call	BIO_set_flags@PLT
	movl	$-1, %eax
	jmp	.L28
.L35:
	movl	$-1, %eax
	jmp	.L28
.L39:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE270:
	.size	nbiof_read, .-nbiof_read
	.p2align 4
	.type	nbiof_write, @function
nbiof_write:
.LFB271:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L46
	movl	%edx, %ebx
	testl	%edx, %edx
	jle	.L46
	cmpq	$0, 64(%rdi)
	movq	%rdi, %r12
	je	.L40
	movq	56(%rdi), %r14
	movq	%rsi, %r13
	movl	$15, %esi
	call	BIO_clear_flags@PLT
	movl	4(%r14), %edx
	testl	%edx, %edx
	jle	.L42
	cmpl	%edx, %ebx
	movl	$0, 4(%r14)
	cmovg	%edx, %ebx
.L43:
	movq	64(%r12), %rdi
	movl	%ebx, %edx
	movq	%r13, %rsi
	call	BIO_write@PLT
	testl	%eax, %eax
	js	.L50
.L40:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L51
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L50:
	movq	%r12, %rdi
	movl	%eax, -52(%rbp)
	call	BIO_copy_next_retry@PLT
	movl	%ebx, 4(%r14)
	movl	-52(%rbp), %eax
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L42:
	leaq	-41(%rbp), %rdi
	movl	$1, %esi
	call	RAND_priv_bytes@PLT
	testl	%eax, %eax
	jle	.L48
	movzbl	-41(%rbp), %eax
	movl	%eax, %edx
	andl	$7, %edx
	cmpl	%ebx, %edx
	cmovle	%edx, %ebx
	testb	$7, %al
	jne	.L43
	movl	$10, %esi
	movq	%r12, %rdi
	call	BIO_set_flags@PLT
	movl	$-1, %eax
	jmp	.L40
.L48:
	orl	$-1, %eax
	jmp	.L40
.L51:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE271:
	.size	nbiof_write, .-nbiof_write
	.p2align 4
	.globl	BIO_f_nbio_test
	.type	BIO_f_nbio_test, @function
BIO_f_nbio_test:
.LFB267:
	.cfi_startproc
	endbr64
	leaq	methods_nbiof(%rip), %rax
	ret
	.cfi_endproc
.LFE267:
	.size	BIO_f_nbio_test, .-BIO_f_nbio_test
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"non-blocking IO test filter"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	methods_nbiof, @object
	.size	methods_nbiof, 96
methods_nbiof:
	.long	528
	.zero	4
	.quad	.LC1
	.quad	bwrite_conv
	.quad	nbiof_write
	.quad	bread_conv
	.quad	nbiof_read
	.quad	nbiof_puts
	.quad	nbiof_gets
	.quad	nbiof_ctrl
	.quad	nbiof_new
	.quad	nbiof_free
	.quad	nbiof_callback_ctrl
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
