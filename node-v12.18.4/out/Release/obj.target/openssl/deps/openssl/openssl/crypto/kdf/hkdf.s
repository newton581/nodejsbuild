	.file	"hkdf.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/kdf/hkdf.c"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB1:
	.text
.LHOTB1:
	.p2align 4
	.type	pkey_hkdf_ctrl, @function
pkey_hkdf_ctrl:
.LFB447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subl	$4099, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	40(%rdi), %r12
	cmpl	$4, %esi
	ja	.L13
	movslq	%edx, %rbx
	leaq	.L4(%rip), %rdx
	movq	%rcx, %r13
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L4:
	.long	.L8-.L4
	.long	.L7-.L4
	.long	.L6-.L4
	.long	.L5-.L4
	.long	.L3-.L4
	.text
	.p2align 4,,10
	.p2align 3
.L5:
	testl	%ebx, %ebx
	je	.L15
	testq	%rcx, %rcx
	je	.L15
	testl	%ebx, %ebx
	js	.L10
	movq	1072(%r12), %rdx
	movl	$1024, %eax
	subl	%edx, %eax
	cmpl	%ebx, %eax
	jge	.L30
.L10:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	movl	%ebx, (%r12)
	movl	$1, %eax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L10
	movq	%rcx, 8(%r12)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	testl	%ebx, %ebx
	je	.L15
	testq	%rcx, %rcx
	je	.L15
	testl	%ebx, %ebx
	js	.L10
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L11
	movq	24(%r12), %rsi
	movl	$94, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
.L11:
	movl	$96, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	CRYPTO_memdup@PLT
	movq	%rax, 16(%r12)
	testq	%rax, %rax
	je	.L10
	movq	%rbx, 24(%r12)
	movl	$1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L6:
	testl	%ebx, %ebx
	js	.L10
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L12
	movq	40(%r12), %rsi
	movl	$108, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
.L12:
	movl	$110, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	CRYPTO_memdup@PLT
	movq	%rax, 32(%r12)
	testq	%rax, %rax
	je	.L10
	movq	%rbx, 40(%r12)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	leaq	48(%r12,%rdx), %rdi
	movq	%rcx, %rsi
	movq	%rbx, %rdx
	call	memcpy@PLT
	addq	%rbx, 1072(%r12)
	movl	$1, %eax
	jmp	.L1
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	pkey_hkdf_ctrl.cold, @function
pkey_hkdf_ctrl.cold:
.LFSB447:
.L13:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	$-2, %eax
	jmp	.L1
	.cfi_endproc
.LFE447:
	.text
	.size	pkey_hkdf_ctrl, .-pkey_hkdf_ctrl
	.section	.text.unlikely
	.size	pkey_hkdf_ctrl.cold, .-pkey_hkdf_ctrl.cold
.LCOLDE1:
	.text
.LHOTE1:
	.p2align 4
	.type	HKDF_Expand, @function
HKDF_Expand:
.LFB453:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$152, %rsp
	movq	%rcx, -168(%rbp)
	movq	%r8, -176(%rbp)
	movq	%r9, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_MD_size@PLT
	xorl	%edx, %edx
	cltq
	movq	%rax, %rcx
	movq	%rax, -152(%rbp)
	movq	16(%rbp), %rax
	divq	%rcx
	cmpq	$1, %rdx
	sbbq	$-1, %rax
	movq	%rax, -160(%rbp)
	cmpq	$255, %rax
	ja	.L36
	testq	%r15, %r15
	je	.L36
	call	HMAC_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L36
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	HMAC_Init_ex@PLT
	testl	%eax, %eax
	je	.L76
	cmpq	$0, -160(%rbp)
	je	.L77
	leaq	-129(%rbp), %rax
	movb	$1, -129(%rbp)
	xorl	%ebx, %ebx
	leaq	-128(%rbp), %r15
	movq	%rax, -192(%rbp)
	movl	$1, %r13d
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L78:
	movq	-192(%rbp), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	HMAC_Update@PLT
	testl	%eax, %eax
	je	.L41
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	HMAC_Final@PLT
	testl	%eax, %eax
	je	.L41
	movq	-152(%rbp), %rdx
	leaq	(%rdx,%rbx), %r14
	cmpq	16(%rbp), %r14
	jbe	.L43
	movq	16(%rbp), %rdx
	movq	16(%rbp), %r14
	subq	%rbx, %rdx
.L43:
	movq	-184(%rbp), %rax
	movq	%r15, %rsi
	leaq	(%rax,%rbx), %rdi
	call	memcpy@PLT
	leal	1(%r13), %eax
	movq	%rax, %r13
	cmpq	-160(%rbp), %rax
	ja	.L46
	movb	%r13b, -129(%rbp)
	cmpl	$1, %r13d
	jbe	.L42
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	HMAC_Init_ex@PLT
	testl	%eax, %eax
	je	.L41
	movq	-152(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	HMAC_Update@PLT
	testl	%eax, %eax
	je	.L41
.L42:
	movq	%r14, %rbx
.L40:
	movq	-176(%rbp), %rdx
	movq	-168(%rbp), %rsi
	movq	%r12, %rdi
	call	HMAC_Update@PLT
	testl	%eax, %eax
	jne	.L78
.L41:
	xorl	%r13d, %r13d
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L76:
	xorl	%r13d, %r13d
	leaq	-128(%rbp), %r15
.L37:
	movq	%r15, %rdi
	movl	$64, %esi
	call	OPENSSL_cleanse@PLT
	movq	%r12, %rdi
	call	HMAC_CTX_free@PLT
.L31:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L79
	addq	$152, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	xorl	%r13d, %r13d
	jmp	.L31
.L77:
	leaq	-128(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L46:
	movq	-184(%rbp), %r13
	jmp	.L37
.L79:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE453:
	.size	HKDF_Expand, .-HKDF_Expand
	.p2align 4
	.type	pkey_hkdf_derive, @function
pkey_hkdf_derive:
.LFB450:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L94
	movq	32(%rax), %r11
	testq	%r11, %r11
	je	.L95
	movl	(%rax), %r12d
	movq	%rsi, %rbx
	movq	%rdx, %r13
	cmpl	$1, %r12d
	je	.L84
	cmpl	$2, %r12d
	je	.L85
	testl	%r12d, %r12d
	je	.L96
.L91:
	xorl	%r12d, %r12d
.L80:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L97
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L98
	movq	16(%rax), %rsi
	subq	$8, %rsp
	movq	40(%rax), %r8
	movq	%rbx, %r9
	movq	24(%rax), %rdx
	leaq	-132(%rbp), %rax
	movq	%r11, %rcx
	pushq	%rax
	call	HMAC@PLT
	popq	%rsi
	popq	%rdi
	testq	%rax, %rax
	je	.L91
	movl	-132(%rbp), %eax
	movq	%rax, 0(%r13)
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L96:
	movq	(%rdx), %rcx
	subq	$8, %rsp
	movq	40(%rax), %r8
	leaq	48(%rax), %r15
	movq	1072(%rax), %r14
	movq	24(%rax), %rdx
	leaq	-128(%rbp), %r13
	movq	%rdi, -160(%rbp)
	movq	16(%rax), %rsi
	leaq	-132(%rbp), %rax
	movq	%rcx, -152(%rbp)
	movq	%r13, %r9
	pushq	%rax
	movq	%r11, %rcx
	call	HMAC@PLT
	popq	%r10
	popq	%r11
	testq	%rax, %rax
	je	.L80
	subq	$8, %rsp
	pushq	-152(%rbp)
	movq	%rbx, %r9
	movq	%r14, %r8
	movl	-132(%rbp), %edx
	movq	%r15, %rcx
	movq	%r13, %rsi
	xorl	%r12d, %r12d
	movq	-160(%rbp), %rdi
	call	HKDF_Expand
	movl	$64, %esi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	OPENSSL_cleanse@PLT
	testq	%rbx, %rbx
	popq	%r8
	popq	%r9
	setne	%r12b
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L85:
	subq	$8, %rsp
	movq	40(%rax), %rdx
	leaq	48(%rax), %rcx
	movq	%rsi, %r9
	movq	1072(%rax), %r8
	pushq	0(%r13)
	movq	%r11, %rsi
	xorl	%r12d, %r12d
	call	HKDF_Expand
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	setne	%r12b
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L94:
	movl	$196, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$105, %edx
	xorl	%r12d, %r12d
	movl	$102, %esi
	movl	$52, %edi
	call	ERR_put_error@PLT
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L95:
	movl	$200, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$104, %edx
	xorl	%r12d, %r12d
	movl	$102, %esi
	movl	$52, %edi
	call	ERR_put_error@PLT
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L98:
	call	EVP_MD_size@PLT
	cltq
	movq	%rax, 0(%r13)
	jmp	.L80
.L97:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE450:
	.size	pkey_hkdf_derive, .-pkey_hkdf_derive
	.p2align 4
	.type	pkey_hkdf_derive_init, @function
pkey_hkdf_derive_init:
.LFB449:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$182, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	40(%rdi), %rbx
	movq	40(%rbx), %rsi
	movq	32(%rbx), %rdi
	call	CRYPTO_clear_free@PLT
	movq	24(%rbx), %rsi
	movq	16(%rbx), %rdi
	movl	$183, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movq	1072(%rbx), %rsi
	leaq	48(%rbx), %rdi
	call	OPENSSL_cleanse@PLT
	leaq	8(%rbx), %rdi
	movq	$0, (%rbx)
	xorl	%eax, %eax
	movq	$0, 1072(%rbx)
	andq	$-8, %rdi
	subq	%rdi, %rbx
	leal	1080(%rbx), %ecx
	shrl	$3, %ecx
	rep stosq
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE449:
	.size	pkey_hkdf_derive_init, .-pkey_hkdf_derive_init
	.p2align 4
	.type	pkey_hkdf_cleanup, @function
pkey_hkdf_cleanup:
.LFB446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$64, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	40(%rdi), %r12
	movq	24(%r12), %rsi
	movq	16(%r12), %rdi
	call	CRYPTO_clear_free@PLT
	movq	40(%r12), %rsi
	movq	32(%r12), %rdi
	leaq	.LC0(%rip), %rdx
	movl	$65, %ecx
	call	CRYPTO_clear_free@PLT
	movq	1072(%r12), %rsi
	leaq	48(%r12), %rdi
	call	OPENSSL_cleanse@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$67, %edx
	popq	%r12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE446:
	.size	pkey_hkdf_cleanup, .-pkey_hkdf_cleanup
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	"mode"
.LC3:
	.string	"EXTRACT_AND_EXPAND"
.LC4:
	.string	"EXTRACT_ONLY"
.LC5:
	.string	"EXPAND_ONLY"
.LC6:
	.string	"salt"
.LC7:
	.string	"hexsalt"
.LC8:
	.string	"key"
.LC9:
	.string	"hexkey"
.LC10:
	.string	"info"
.LC11:
	.string	"hexinfo"
	.text
	.p2align 4
	.type	pkey_hkdf_ctrl_str, @function
pkey_hkdf_ctrl_str:
.LFB448:
	.cfi_startproc
	endbr64
	movq	%rdi, %r10
	movl	$5, %ecx
	leaq	.LC2(%rip), %rdi
	movq	%rsi, %r8
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	movsbl	%al, %eax
	testl	%eax, %eax
	jne	.L104
	movl	$19, %ecx
	leaq	.LC3(%rip), %rdi
	movq	%rdx, %rsi
	repz cmpsb
	seta	%r8b
	sbbb	$0, %r8b
	movsbl	%r8b, %r8d
	testl	%r8d, %r8d
	je	.L105
	movl	$13, %ecx
	leaq	.LC4(%rip), %rdi
	movq	%rdx, %rsi
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L115
	movq	%rdx, %rsi
	movl	$12, %ecx
	leaq	.LC5(%rip), %rdi
	movl	$2, %r8d
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L105
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	cmpb	$109, (%r8)
	je	.L122
.L108:
	movl	$5, %ecx
	leaq	.LC6(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L123
	movl	$8, %ecx
	leaq	.LC7(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L124
	movl	$4, %ecx
	leaq	.LC8(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L125
	movl	$7, %ecx
	leaq	.LC9(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L126
	movl	$5, %ecx
	leaq	.LC10(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L127
	movl	$8, %ecx
	leaq	.LC11(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L114
	movl	$4102, %esi
	movq	%r10, %rdi
	jmp	EVP_PKEY_CTX_hex2ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L122:
	cmpb	$100, 1(%r8)
	jne	.L108
	cmpb	$0, 2(%r8)
	jne	.L108
	movq	%rdx, %rcx
	movl	$1024, %esi
	movl	$4099, %edx
	movq	%r10, %rdi
	jmp	EVP_PKEY_CTX_md@PLT
	.p2align 4,,10
	.p2align 3
.L115:
	movl	$1, %r8d
.L105:
	xorl	%r9d, %r9d
	movl	$4103, %ecx
	movl	$1024, %edx
	movq	%r10, %rdi
	movl	$-1, %esi
	jmp	EVP_PKEY_CTX_ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L123:
	movl	$4100, %esi
	movq	%r10, %rdi
	jmp	EVP_PKEY_CTX_str2ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L124:
	movl	$4100, %esi
	movq	%r10, %rdi
	jmp	EVP_PKEY_CTX_hex2ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L125:
	movl	$4101, %esi
	movq	%r10, %rdi
	jmp	EVP_PKEY_CTX_str2ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L126:
	movl	$4101, %esi
	movq	%r10, %rdi
	jmp	EVP_PKEY_CTX_hex2ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L127:
	movl	$4102, %esi
	movq	%r10, %rdi
	jmp	EVP_PKEY_CTX_str2ctrl@PLT
.L114:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$103, %edx
	movl	$103, %esi
	movl	$52, %edi
	movl	$174, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	movl	$-2, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE448:
	.size	pkey_hkdf_ctrl_str, .-pkey_hkdf_ctrl_str
	.p2align 4
	.type	pkey_hkdf_init, @function
pkey_hkdf_init:
.LFB445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$51, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$1080, %edi
	subq	$8, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L132
	movq	%rax, 40(%rbx)
	movl	$1, %eax
.L128:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	.cfi_restore_state
	movl	$52, %r8d
	movl	$65, %edx
	movl	$108, %esi
	movl	$52, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L128
	.cfi_endproc
.LFE445:
	.size	pkey_hkdf_init, .-pkey_hkdf_init
	.globl	hkdf_pkey_meth
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	hkdf_pkey_meth, @object
	.size	hkdf_pkey_meth, 256
hkdf_pkey_meth:
	.long	1036
	.long	0
	.quad	pkey_hkdf_init
	.quad	0
	.quad	pkey_hkdf_cleanup
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	pkey_hkdf_derive_init
	.quad	pkey_hkdf_derive
	.quad	pkey_hkdf_ctrl
	.quad	pkey_hkdf_ctrl_str
	.zero	48
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
