	.file	"x509_cmp.c"
	.text
	.p2align 4
	.type	check_suite_b, @function
check_suite_b:
.LFB1321:
	.cfi_startproc
	testq	%rdi, %rdi
	je	.L29
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	EVP_PKEY_id@PLT
	cmpl	$408, %eax
	je	.L30
.L4:
	movl	$57, %eax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movq	%r12, %rdi
	call	EVP_PKEY_get0_EC_KEY@PLT
	movq	%rax, %rdi
	call	EC_KEY_get0_group@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L4
	call	EC_GROUP_get_curve_name@PLT
	movl	%eax, %edx
	cmpl	$715, %eax
	je	.L31
	movl	$58, %eax
	cmpl	$415, %edx
	jne	.L1
	cmpl	$-1, %r13d
	je	.L14
	movl	$59, %eax
	cmpl	$794, %r13d
	jne	.L1
.L14:
	movq	(%rbx), %rax
	andl	$65536, %eax
	cmpq	$1, %rax
	sbbl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	andl	$60, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$57, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	cmpl	$-1, %r13d
	je	.L13
	movl	$59, %eax
	cmpl	$795, %r13d
	jne	.L1
.L13:
	movq	(%rbx), %rdx
	movl	$60, %eax
	testl	$131072, %edx
	je	.L1
	andq	$-65537, %rdx
	xorl	%eax, %eax
	movq	%rdx, (%rbx)
	jmp	.L1
	.cfi_endproc
.LFE1321:
	.size	check_suite_b, .-check_suite_b
	.p2align 4
	.globl	X509_issuer_and_serial_cmp
	.type	X509_issuer_and_serial_cmp, @function
X509_issuer_and_serial_cmp:
.LFB1298:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	addq	$8, %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$8, %rdi
	call	ASN1_INTEGER_cmp@PLT
	testl	%eax, %eax
	je	.L46
.L32:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	movq	48(%rbx), %rbx
	movq	48(%r12), %r12
	cmpq	$0, 24(%rbx)
	je	.L34
	movl	8(%rbx), %edx
	testl	%edx, %edx
	je	.L37
.L34:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	i2d_X509_NAME@PLT
	testl	%eax, %eax
	js	.L39
.L37:
	cmpq	$0, 24(%r12)
	je	.L35
	movl	8(%r12), %eax
	testl	%eax, %eax
	je	.L40
.L35:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	i2d_X509_NAME@PLT
	testl	%eax, %eax
	js	.L39
.L40:
	movslq	32(%rbx), %rdx
	movl	%edx, %eax
	subl	32(%r12), %eax
	jne	.L32
	testl	%edx, %edx
	je	.L32
	movq	24(%r12), %rsi
	movq	24(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	memcmp@PLT
.L39:
	.cfi_restore_state
	movl	$-2, %eax
	jmp	.L32
	.cfi_endproc
.LFE1298:
	.size	X509_issuer_and_serial_cmp, .-X509_issuer_and_serial_cmp
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/x509/x509_cmp.c"
	.text
	.p2align 4
	.globl	X509_issuer_and_serial_hash
	.type	X509_issuer_and_serial_hash, @function
X509_issuer_and_serial_hash:
.LFB1299:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L50
	movq	48(%rbx), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	X509_NAME_oneline@PLT
	movq	%rax, %r13
	call	EVP_md5@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	jne	.L65
.L50:
	xorl	%r13d, %r13d
.L49:
	movq	%r12, %rdi
	call	EVP_MD_CTX_free@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L66
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L50
	movl	$46, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	movslq	8(%rbx), %rdx
	movq	16(%rbx), %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L50
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	je	.L50
	movl	-64(%rbp), %r13d
	jmp	.L49
.L66:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1299:
	.size	X509_issuer_and_serial_hash, .-X509_issuer_and_serial_hash
	.p2align 4
	.globl	X509_issuer_name_cmp
	.type	X509_issuer_name_cmp, @function
X509_issuer_name_cmp:
.LFB1300:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	48(%rdi), %r12
	movq	48(%rsi), %rbx
	cmpq	$0, 24(%r12)
	je	.L68
	movl	8(%r12), %edx
	testl	%edx, %edx
	jne	.L68
.L71:
	cmpq	$0, 24(%rbx)
	je	.L69
	movl	8(%rbx), %eax
	testl	%eax, %eax
	jne	.L69
.L75:
	movslq	32(%r12), %rdx
	movl	%edx, %eax
	subl	32(%rbx), %eax
	jne	.L67
	testl	%edx, %edx
	je	.L67
	movq	24(%rbx), %rsi
	movq	24(%r12), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	memcmp@PLT
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	i2d_X509_NAME@PLT
	testl	%eax, %eax
	jns	.L71
.L74:
	movl	$-2, %eax
.L67:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	i2d_X509_NAME@PLT
	testl	%eax, %eax
	jns	.L75
	jmp	.L74
	.cfi_endproc
.LFE1300:
	.size	X509_issuer_name_cmp, .-X509_issuer_name_cmp
	.p2align 4
	.globl	X509_subject_name_cmp
	.type	X509_subject_name_cmp, @function
X509_subject_name_cmp:
.LFB1301:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	72(%rdi), %r12
	movq	72(%rsi), %rbx
	cmpq	$0, 24(%r12)
	je	.L82
	movl	8(%r12), %edx
	testl	%edx, %edx
	jne	.L82
.L85:
	cmpq	$0, 24(%rbx)
	je	.L83
	movl	8(%rbx), %eax
	testl	%eax, %eax
	jne	.L83
.L89:
	movslq	32(%r12), %rdx
	movl	%edx, %eax
	subl	32(%rbx), %eax
	jne	.L81
	testl	%edx, %edx
	je	.L81
	movq	24(%rbx), %rsi
	movq	24(%r12), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	memcmp@PLT
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	i2d_X509_NAME@PLT
	testl	%eax, %eax
	jns	.L85
.L88:
	movl	$-2, %eax
.L81:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	i2d_X509_NAME@PLT
	testl	%eax, %eax
	jns	.L89
	jmp	.L88
	.cfi_endproc
.LFE1301:
	.size	X509_subject_name_cmp, .-X509_subject_name_cmp
	.p2align 4
	.globl	X509_CRL_cmp
	.type	X509_CRL_cmp, @function
X509_CRL_cmp:
.LFB1302:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	24(%rdi), %r12
	movq	24(%rsi), %rbx
	cmpq	$0, 24(%r12)
	je	.L96
	movl	8(%r12), %edx
	testl	%edx, %edx
	jne	.L96
.L99:
	cmpq	$0, 24(%rbx)
	je	.L97
	movl	8(%rbx), %eax
	testl	%eax, %eax
	jne	.L97
.L103:
	movslq	32(%r12), %rdx
	movl	%edx, %eax
	subl	32(%rbx), %eax
	jne	.L95
	testl	%edx, %edx
	je	.L95
	movq	24(%rbx), %rsi
	movq	24(%r12), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	memcmp@PLT
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	i2d_X509_NAME@PLT
	testl	%eax, %eax
	jns	.L99
.L102:
	movl	$-2, %eax
.L95:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	i2d_X509_NAME@PLT
	testl	%eax, %eax
	jns	.L103
	jmp	.L102
	.cfi_endproc
.LFE1302:
	.size	X509_CRL_cmp, .-X509_CRL_cmp
	.p2align 4
	.globl	X509_CRL_match
	.type	X509_CRL_match, @function
X509_CRL_match:
.LFB1303:
	.cfi_startproc
	endbr64
	addq	$184, %rsi
	addq	$184, %rdi
	movl	$20, %edx
	jmp	memcmp@PLT
	.cfi_endproc
.LFE1303:
	.size	X509_CRL_match, .-X509_CRL_match
	.p2align 4
	.globl	X509_get_issuer_name
	.type	X509_get_issuer_name, @function
X509_get_issuer_name:
.LFB1304:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	ret
	.cfi_endproc
.LFE1304:
	.size	X509_get_issuer_name, .-X509_get_issuer_name
	.p2align 4
	.globl	X509_issuer_name_hash
	.type	X509_issuer_name_hash, @function
X509_issuer_name_hash:
.LFB1305:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -24
	movq	48(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rbx, %rdi
	call	i2d_X509_NAME@PLT
	call	EVP_sha1@PLT
	movslq	32(%rbx), %rsi
	movq	24(%rbx), %rdi
	xorl	%r9d, %r9d
	movq	%rax, %r8
	leaq	-48(%rbp), %rdx
	xorl	%ecx, %ecx
	call	EVP_Digest@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	je	.L111
	movl	-48(%rbp), %eax
.L111:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L118
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L118:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1305:
	.size	X509_issuer_name_hash, .-X509_issuer_name_hash
	.p2align 4
	.globl	X509_issuer_name_hash_old
	.type	X509_issuer_name_hash_old, @function
X509_issuer_name_hash_old:
.LFB1306:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	48(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	EVP_MD_CTX_new@PLT
	testq	%rax, %rax
	je	.L119
	movq	%rax, %r12
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	i2d_X509_NAME@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	EVP_MD_CTX_set_flags@PLT
	call	EVP_md5@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	jne	.L121
.L123:
	xorl	%r13d, %r13d
.L122:
	movq	%r12, %rdi
	call	EVP_MD_CTX_free@PLT
.L119:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L135
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	movq	16(%rbx), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rdx
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L123
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	je	.L123
	movl	-64(%rbp), %r13d
	jmp	.L122
.L135:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1306:
	.size	X509_issuer_name_hash_old, .-X509_issuer_name_hash_old
	.p2align 4
	.globl	X509_get_subject_name
	.type	X509_get_subject_name, @function
X509_get_subject_name:
.LFB1307:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rax
	ret
	.cfi_endproc
.LFE1307:
	.size	X509_get_subject_name, .-X509_get_subject_name
	.p2align 4
	.globl	X509_get_serialNumber
	.type	X509_get_serialNumber, @function
X509_get_serialNumber:
.LFB1308:
	.cfi_startproc
	endbr64
	leaq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE1308:
	.size	X509_get_serialNumber, .-X509_get_serialNumber
	.p2align 4
	.globl	X509_get0_serialNumber
	.type	X509_get0_serialNumber, @function
X509_get0_serialNumber:
.LFB1328:
	.cfi_startproc
	endbr64
	leaq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE1328:
	.size	X509_get0_serialNumber, .-X509_get0_serialNumber
	.p2align 4
	.globl	X509_subject_name_hash
	.type	X509_subject_name_hash, @function
X509_subject_name_hash:
.LFB1310:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -24
	movq	72(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rbx, %rdi
	call	i2d_X509_NAME@PLT
	call	EVP_sha1@PLT
	movslq	32(%rbx), %rsi
	movq	24(%rbx), %rdi
	xorl	%r9d, %r9d
	movq	%rax, %r8
	leaq	-48(%rbp), %rdx
	xorl	%ecx, %ecx
	call	EVP_Digest@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	je	.L139
	movl	-48(%rbp), %eax
.L139:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L146
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L146:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1310:
	.size	X509_subject_name_hash, .-X509_subject_name_hash
	.p2align 4
	.globl	X509_subject_name_hash_old
	.type	X509_subject_name_hash_old, @function
X509_subject_name_hash_old:
.LFB1311:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	72(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	EVP_MD_CTX_new@PLT
	testq	%rax, %rax
	je	.L147
	movq	%rax, %r12
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	i2d_X509_NAME@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	EVP_MD_CTX_set_flags@PLT
	call	EVP_md5@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	jne	.L149
.L151:
	xorl	%r13d, %r13d
.L150:
	movq	%r12, %rdi
	call	EVP_MD_CTX_free@PLT
.L147:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L163
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L149:
	.cfi_restore_state
	movq	16(%rbx), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rdx
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L151
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	je	.L151
	movl	-64(%rbp), %r13d
	jmp	.L150
.L163:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1311:
	.size	X509_subject_name_hash_old, .-X509_subject_name_hash_old
	.p2align 4
	.globl	X509_cmp
	.type	X509_cmp, @function
X509_cmp:
.LFB1312:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	movl	$-1, %esi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	X509_check_purpose@PLT
	cmpl	$1, %eax
	jne	.L167
	xorl	%edx, %edx
	movl	$-1, %esi
	movq	%r12, %rdi
	call	X509_check_purpose@PLT
	cmpl	$1, %eax
	jne	.L167
	leaq	304(%r12), %rsi
	leaq	304(%rbx), %rdi
	movl	$20, %edx
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L164
	movl	128(%rbx), %ecx
	testl	%ecx, %ecx
	jne	.L164
	movl	128(%r12), %edx
	testl	%edx, %edx
	jne	.L164
	movq	120(%rbx), %rdx
	cmpq	120(%r12), %rdx
	jl	.L169
	jg	.L170
	movq	112(%r12), %rsi
	movq	112(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	memcmp@PLT
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore_state
	movl	$1, %eax
.L164:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L167:
	.cfi_restore_state
	movl	$-2, %eax
	jmp	.L164
.L169:
	movl	$-1, %eax
	jmp	.L164
	.cfi_endproc
.LFE1312:
	.size	X509_cmp, .-X509_cmp
	.p2align 4
	.globl	X509_NAME_cmp
	.type	X509_NAME_cmp, @function
X509_NAME_cmp:
.LFB1313:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpq	$0, 24(%rdi)
	movq	%rsi, %rbx
	je	.L173
	movl	8(%rdi), %edx
	testl	%edx, %edx
	jne	.L173
.L176:
	cmpq	$0, 24(%rbx)
	je	.L174
	movl	8(%rbx), %eax
	testl	%eax, %eax
	jne	.L174
.L180:
	movslq	32(%r12), %rdx
	movl	%edx, %eax
	subl	32(%rbx), %eax
	jne	.L172
	testl	%edx, %edx
	je	.L172
	movq	24(%rbx), %rsi
	movq	24(%r12), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	memcmp@PLT
	.p2align 4,,10
	.p2align 3
.L173:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	i2d_X509_NAME@PLT
	testl	%eax, %eax
	jns	.L176
.L179:
	movl	$-2, %eax
.L172:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L174:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	i2d_X509_NAME@PLT
	testl	%eax, %eax
	jns	.L180
	jmp	.L179
	.cfi_endproc
.LFE1313:
	.size	X509_NAME_cmp, .-X509_NAME_cmp
	.p2align 4
	.globl	X509_NAME_hash
	.type	X509_NAME_hash, @function
X509_NAME_hash:
.LFB1314:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	i2d_X509_NAME@PLT
	call	EVP_sha1@PLT
	movslq	32(%rbx), %rsi
	movq	24(%rbx), %rdi
	xorl	%r9d, %r9d
	movq	%rax, %r8
	leaq	-48(%rbp), %rdx
	xorl	%ecx, %ecx
	call	EVP_Digest@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	je	.L186
	movl	-48(%rbp), %eax
.L186:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L193
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L193:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1314:
	.size	X509_NAME_hash, .-X509_NAME_hash
	.p2align 4
	.globl	X509_NAME_hash_old
	.type	X509_NAME_hash_old, @function
X509_NAME_hash_old:
.LFB1315:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	EVP_MD_CTX_new@PLT
	testq	%rax, %rax
	je	.L194
	movq	%rax, %r12
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	i2d_X509_NAME@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	EVP_MD_CTX_set_flags@PLT
	call	EVP_md5@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	jne	.L196
.L198:
	xorl	%r13d, %r13d
.L197:
	movq	%r12, %rdi
	call	EVP_MD_CTX_free@PLT
.L194:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L210
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L196:
	.cfi_restore_state
	movq	16(%rbx), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rdx
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L198
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	je	.L198
	movl	-64(%rbp), %r13d
	jmp	.L197
.L210:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1315:
	.size	X509_NAME_hash_old, .-X509_NAME_hash_old
	.p2align 4
	.globl	X509_find_by_issuer_and_serial
	.type	X509_find_by_issuer_and_serial, @function
X509_find_by_issuer_and_serial:
.LFB1316:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$368, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L218
	movdqu	(%rdx), %xmm0
	movq	16(%rdx), %rax
	movq	%rdi, %r13
	xorl	%ebx, %ebx
	movq	%rsi, -352(%rbp)
	leaq	-392(%rbp), %r14
	movq	%rax, -376(%rbp)
	movups	%xmm0, -392(%rbp)
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L217:
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r14, %rsi
	leaq	8(%rax), %rdi
	movq	%rax, %r12
	call	ASN1_INTEGER_cmp@PLT
	testl	%eax, %eax
	jne	.L216
	movq	48(%r12), %rdi
	movq	-352(%rbp), %rsi
	call	X509_NAME_cmp
	testl	%eax, %eax
	je	.L211
.L216:
	addl	$1, %ebx
.L214:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L217
.L218:
	xorl	%r12d, %r12d
.L211:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L224
	addq	$368, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L224:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1316:
	.size	X509_find_by_issuer_and_serial, .-X509_find_by_issuer_and_serial
	.p2align 4
	.globl	X509_find_by_subject
	.type	X509_find_by_subject, @function
X509_find_by_subject:
.LFB1317:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L246:
	movq	24(%r12), %rsi
	movq	24(%rbx), %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	je	.L225
.L231:
	addl	$1, %r14d
.L226:
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r14d
	jge	.L245
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	movq	72(%rax), %rbx
	movq	%rax, %r13
	cmpq	$0, 24(%rbx)
	je	.L227
	movl	8(%rbx), %edx
	testl	%edx, %edx
	je	.L230
.L227:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	i2d_X509_NAME@PLT
	testl	%eax, %eax
	js	.L231
.L230:
	cmpq	$0, 24(%r12)
	je	.L228
	movl	8(%r12), %eax
	testl	%eax, %eax
	je	.L233
.L228:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	i2d_X509_NAME@PLT
	testl	%eax, %eax
	js	.L231
.L233:
	movslq	32(%rbx), %rdx
	cmpl	32(%r12), %edx
	jne	.L231
	testl	%edx, %edx
	jne	.L246
.L225:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L245:
	.cfi_restore_state
	xorl	%r13d, %r13d
	jmp	.L225
	.cfi_endproc
.LFE1317:
	.size	X509_find_by_subject, .-X509_find_by_subject
	.p2align 4
	.globl	X509_get0_pubkey
	.type	X509_get0_pubkey, @function
X509_get0_pubkey:
.LFB1318:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L248
	movq	80(%rdi), %rdi
	jmp	X509_PUBKEY_get0@PLT
	.p2align 4,,10
	.p2align 3
.L248:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1318:
	.size	X509_get0_pubkey, .-X509_get0_pubkey
	.p2align 4
	.globl	X509_get_pubkey
	.type	X509_get_pubkey, @function
X509_get_pubkey:
.LFB1319:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L250
	movq	80(%rdi), %rdi
	jmp	X509_PUBKEY_get@PLT
	.p2align 4,,10
	.p2align 3
.L250:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1319:
	.size	X509_get_pubkey, .-X509_get_pubkey
	.p2align 4
	.globl	X509_check_private_key
	.type	X509_check_private_key, @function
X509_check_private_key:
.LFB1320:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	testq	%rdi, %rdi
	je	.L252
	movq	80(%rdi), %rdi
	movq	%rsi, %r12
	call	X509_PUBKEY_get0@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L252
	movq	%r12, %rsi
	call	EVP_PKEY_cmp@PLT
	cmpl	$-1, %eax
	je	.L254
	testl	%eax, %eax
	je	.L255
	cmpl	$-2, %eax
	je	.L252
	testl	%eax, %eax
	setg	%al
	addq	$24, %rsp
	movzbl	%al, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L252:
	.cfi_restore_state
	movl	$303, %r8d
	movl	$117, %edx
	movl	$128, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L254:
	.cfi_restore_state
	movl	$300, %r8d
	movl	$115, %edx
	movl	$128, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	.cfi_restore_state
	movl	$297, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$116, %edx
	movl	%eax, -20(%rbp)
	movl	$128, %esi
	movl	$11, %edi
	call	ERR_put_error@PLT
	movl	-20(%rbp), %eax
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1320:
	.size	X509_check_private_key, .-X509_check_private_key
	.p2align 4
	.globl	X509_chain_check_suiteb
	.type	X509_chain_check_suiteb, @function
X509_chain_check_suiteb:
.LFB1322:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -80(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rcx, -64(%rbp)
	testl	$196608, %ecx
	je	.L267
	movq	%rsi, %r15
	movq	%rdx, %rbx
	movq	%rcx, %r14
	xorl	%r13d, %r13d
	testq	%rsi, %rsi
	je	.L327
.L268:
	movq	80(%r15), %rdi
	call	X509_PUBKEY_get0@PLT
	movq	%rax, %r12
	testq	%rbx, %rbx
	je	.L328
	movq	%r15, %rdi
	call	X509_get_version@PLT
	cmpq	$2, %rax
	je	.L329
.L274:
	xorl	%r13d, %r13d
	movl	$56, %eax
.L275:
	movq	-80(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L266
	movl	%r13d, (%rcx)
.L266:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L330
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L332:
	.cfi_restore_state
	movq	%r12, %rdi
	call	EVP_PKEY_get0_EC_KEY@PLT
	movq	%rax, %rdi
	call	EC_KEY_get0_group@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L272
	call	EC_GROUP_get_curve_name@PLT
	movl	%eax, %edx
	cmpl	$715, %eax
	je	.L331
	movl	$58, %eax
	cmpl	$415, %edx
	jne	.L266
	movl	$60, %eax
	testb	$1, -62(%rbp)
	je	.L266
	.p2align 4,,10
	.p2align 3
.L267:
	xorl	%eax, %eax
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L328:
	testq	%rax, %rax
	je	.L272
	movq	%rax, %rdi
	call	EVP_PKEY_id@PLT
	cmpl	$408, %eax
	je	.L332
.L272:
	movl	$57, %eax
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L329:
	testq	%r12, %r12
	je	.L276
	movq	%r12, %rdi
	call	EVP_PKEY_id@PLT
	cmpl	$408, %eax
	jne	.L276
	movq	%r12, %rdi
	call	EVP_PKEY_get0_EC_KEY@PLT
	movq	%rax, %rdi
	call	EC_KEY_get0_group@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L276
	call	EC_GROUP_get_curve_name@PLT
	cmpl	$715, %eax
	je	.L333
	cmpl	$415, %eax
	jne	.L294
	movq	-64(%rbp), %rax
	testl	$65536, %eax
	je	.L295
.L280:
	leaq	-64(%rbp), %rax
	movq	%rax, -88(%rbp)
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L335:
	movq	80(%r15), %rdi
	call	X509_PUBKEY_get0@PLT
	movq	-88(%rbp), %rdx
	movl	-68(%rbp), %esi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	check_suite_b
	testl	%eax, %eax
	jne	.L284
.L283:
	addl	$1, %r13d
.L279:
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	movq	%r15, %rdi
	cmpl	%eax, %r13d
	jge	.L334
	call	X509_get_signature_nid@PLT
	movq	%rbx, %rdi
	movl	%r13d, %esi
	movl	%eax, -68(%rbp)
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	movq	%rax, %r15
	call	X509_get_version@PLT
	cmpq	$2, %rax
	jne	.L296
	testq	%r15, %r15
	jne	.L335
	movq	-88(%rbp), %rdx
	movl	-68(%rbp), %esi
	xorl	%edi, %edi
	call	check_suite_b
	testl	%eax, %eax
	jne	.L284
	xorl	%r12d, %r12d
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L327:
	xorl	%esi, %esi
	movq	%rdx, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L289
	testq	%rbx, %rbx
	je	.L272
	xorl	%edi, %edi
	call	X509_get_version@PLT
	cmpq	$2, %rax
	jne	.L274
.L276:
	xorl	%r13d, %r13d
	movl	$57, %eax
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L333:
	movq	-64(%rbp), %rax
	testl	$131072, %eax
	je	.L295
	andq	$-65537, %rax
	movq	%rax, -64(%rbp)
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L334:
	call	X509_get_signature_nid@PLT
	leaq	-64(%rbp), %rdx
	movq	%r12, %rdi
	movl	%eax, %esi
	call	check_suite_b
	testl	%eax, %eax
	je	.L267
	.p2align 4,,10
	.p2align 3
.L284:
	leal	-59(%rax), %edx
	cmpl	$1, %edx
	ja	.L275
	cmpl	$1, %r13d
	adcl	$-1, %r13d
	cmpl	$60, %eax
	jne	.L298
	movq	-64(%rbp), %rax
.L278:
	cmpq	%rax, %r14
	setne	%al
	movzbl	%al, %eax
	addl	$60, %eax
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L296:
	movl	$56, %eax
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L289:
	movl	$1, %r13d
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L294:
	xorl	%r13d, %r13d
	movl	$58, %eax
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L331:
	movl	$60, %eax
	testb	$2, -62(%rbp)
	je	.L266
	xorl	%eax, %eax
	jmp	.L266
.L295:
	xorl	%r13d, %r13d
	jmp	.L278
.L330:
	call	__stack_chk_fail@PLT
.L298:
	movl	$59, %eax
	jmp	.L275
	.cfi_endproc
.LFE1322:
	.size	X509_chain_check_suiteb, .-X509_chain_check_suiteb
	.p2align 4
	.globl	X509_CRL_check_suiteb
	.type	X509_CRL_check_suiteb, @function
X509_CRL_check_suiteb:
.LFB1323:
	.cfi_startproc
	endbr64
	testl	$196608, %edx
	jne	.L367
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L367:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %r13d
	testq	%r12, %r12
	je	.L340
	movq	%r12, %rdi
	call	EVP_PKEY_id@PLT
	cmpl	$408, %eax
	je	.L368
.L340:
	movl	$57, %eax
.L336:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L368:
	.cfi_restore_state
	movq	%r12, %rdi
	call	EVP_PKEY_get0_EC_KEY@PLT
	movq	%rax, %rdi
	call	EC_KEY_get0_group@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L340
	call	EC_GROUP_get_curve_name@PLT
	movl	%eax, %edx
	cmpl	$715, %eax
	je	.L369
	movl	$58, %eax
	cmpl	$415, %edx
	jne	.L336
	cmpl	$-1, %r13d
	je	.L349
	movl	$59, %eax
	cmpl	$794, %r13d
	jne	.L336
.L349:
	testl	$65536, %ebx
	je	.L370
	xorl	%eax, %eax
.L371:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L369:
	.cfi_restore_state
	cmpl	$-1, %r13d
	je	.L348
	movl	$59, %eax
	cmpl	$795, %r13d
	jne	.L336
.L348:
	movl	$60, %eax
	testl	$131072, %ebx
	je	.L336
	xorl	%eax, %eax
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L370:
	movl	$60, %eax
	jmp	.L336
	.cfi_endproc
.LFE1323:
	.size	X509_CRL_check_suiteb, .-X509_CRL_check_suiteb
	.p2align 4
	.globl	X509_chain_up_ref
	.type	X509_chain_up_ref, @function
X509_chain_up_ref:
.LFB1324:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	call	OPENSSL_sk_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L372
	xorl	%ebx, %ebx
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L378:
	movq	%r12, %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	call	X509_up_ref@PLT
	testl	%eax, %eax
	je	.L385
	addl	$1, %ebx
.L374:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L378
.L372:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L385:
	.cfi_restore_state
	leal	-1(%rbx), %r13d
	testl	%ebx, %ebx
	jle	.L377
	.p2align 4,,10
	.p2align 3
.L376:
	movl	%r13d, %esi
	movq	%r12, %rdi
	subl	$1, %r13d
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	call	X509_free@PLT
	cmpl	$-1, %r13d
	jne	.L376
.L377:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	OPENSSL_sk_free@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1324:
	.size	X509_chain_up_ref, .-X509_chain_up_ref
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
