	.file	"eng_lib.c"
	.text
	.p2align 4
	.globl	do_engine_lock_init_ossl_
	.type	do_engine_lock_init_ossl_, @function
do_engine_lock_init_ossl_:
.LFB877:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	OPENSSL_init_crypto@PLT
	testl	%eax, %eax
	je	.L2
	call	CRYPTO_THREAD_lock_new@PLT
	testq	%rax, %rax
	movq	%rax, global_engine_lock(%rip)
	setne	%al
	movzbl	%al, %eax
.L2:
	movl	%eax, do_engine_lock_init_ossl_ret_(%rip)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE877:
	.size	do_engine_lock_init_ossl_, .-do_engine_lock_init_ossl_
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/engine/eng_lib.c"
	.text
	.p2align 4
	.type	engine_cleanup_cb_free, @function
engine_cleanup_cb_free:
.LFB887:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	*(%rdi)
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$163, %edx
	popq	%r12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE887:
	.size	engine_cleanup_cb_free, .-engine_cleanup_cb_free
	.p2align 4
	.globl	ENGINE_new
	.type	ENGINE_new, @function
ENGINE_new:
.LFB879:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	do_engine_lock_init_ossl_(%rip), %rsi
	leaq	engine_lock_init(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L13
	movl	do_engine_lock_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	je	.L13
	movl	$34, %edx
	leaq	.LC0(%rip), %rsi
	movl	$192, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L13
	leaq	168(%rax), %rdx
	movq	%rax, %rsi
	movl	$10, %edi
	movl	$1, 156(%rax)
	mfence
	call	CRYPTO_new_ex_data@PLT
	testl	%eax, %eax
	je	.L18
.L10:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	movl	$35, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$122, %esi
	movl	$38, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$41, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%r12d, %r12d
	call	CRYPTO_free@PLT
	jmp	.L10
	.cfi_endproc
.LFE879:
	.size	ENGINE_new, .-ENGINE_new
	.p2align 4
	.globl	engine_set_all_null
	.type	engine_set_all_null, @function
engine_set_all_null:
.LFB880:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movq	$0, 32(%rdi)
	movq	$0, 48(%rdi)
	movq	$0, 144(%rdi)
	movl	$0, 152(%rdi)
	movups	%xmm0, (%rdi)
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 56(%rdi)
	movups	%xmm0, 88(%rdi)
	movups	%xmm0, 104(%rdi)
	movups	%xmm0, 120(%rdi)
	ret
	.cfi_endproc
.LFE880:
	.size	engine_set_all_null, .-engine_set_all_null
	.p2align 4
	.globl	engine_free_util
	.type	engine_free_util, @function
engine_free_util:
.LFB881:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L32
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	156(%rdi), %rdx
	movl	$-1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	testl	%esi, %esi
	jne	.L35
	lock xaddl	%eax, (%rdx)
	subl	$1, %eax
.L24:
	testl	%eax, %eax
	jle	.L25
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	lock xaddl	%eax, (%rdx)
	subl	$1, %eax
	jne	.L24
.L25:
	movq	%r12, %rdi
	call	engine_pkey_meths_free@PLT
	movq	%r12, %rdi
	call	engine_pkey_asn1_meths_free@PLT
	movq	88(%r12), %rax
	testq	%rax, %rax
	je	.L27
	movq	%r12, %rdi
	call	*%rax
.L27:
	leaq	168(%r12), %rdx
	movq	%r12, %rsi
	movl	$10, %edi
	call	CRYPTO_free_ex_data@PLT
	movq	%r12, %rdi
	movl	$96, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE881:
	.size	engine_free_util, .-engine_free_util
	.p2align 4
	.globl	ENGINE_free
	.type	ENGINE_free, @function
ENGINE_free:
.LFB882:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L47
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	lock xaddl	%eax, 156(%rdi)
	subl	$1, %eax
	testl	%eax, %eax
	je	.L50
	jle	.L40
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
.L40:
	movq	%r12, %rdi
	call	engine_pkey_meths_free@PLT
	movq	%r12, %rdi
	call	engine_pkey_asn1_meths_free@PLT
	movq	88(%r12), %rax
	testq	%rax, %rax
	je	.L42
	movq	%r12, %rdi
	call	*%rax
.L42:
	leaq	168(%r12), %rdx
	movq	%r12, %rsi
	movl	$10, %edi
	call	CRYPTO_free_ex_data@PLT
	movq	%r12, %rdi
	movl	$96, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE882:
	.size	ENGINE_free, .-ENGINE_free
	.p2align 4
	.globl	engine_cleanup_add_first
	.type	engine_cleanup_add_first, @function
engine_cleanup_add_first:
.LFB885:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	$0, cleanup_stack(%rip)
	je	.L58
.L52:
	leaq	.LC0(%rip), %rsi
	movl	$128, %edx
	movl	$8, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L59
	movq	%rbx, (%rsi)
	movq	cleanup_stack(%rip), %rdi
	addq	$8, %rsp
	xorl	%edx, %edx
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	OPENSSL_sk_insert@PLT
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, cleanup_stack(%rip)
	testq	%rax, %rax
	jne	.L52
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$129, %r8d
	movl	$65, %edx
	movl	$199, %esi
	popq	%rbx
	leaq	.LC0(%rip), %rcx
	movl	$38, %edi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	ERR_put_error@PLT
	.cfi_endproc
.LFE885:
	.size	engine_cleanup_add_first, .-engine_cleanup_add_first
	.p2align 4
	.globl	engine_cleanup_add_last
	.type	engine_cleanup_add_last, @function
engine_cleanup_add_last:
.LFB886:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpq	$0, cleanup_stack(%rip)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	je	.L69
.L61:
	movl	$128, %edx
	leaq	.LC0(%rip), %rsi
	movl	$8, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L70
	movq	%rbx, (%r12)
	movq	cleanup_stack(%rip), %rdi
	movq	%r12, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jle	.L71
.L60:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, cleanup_stack(%rip)
	testq	%rax, %rax
	jne	.L61
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L71:
	popq	%rbx
	movq	%r12, %rdi
	movl	$155, %edx
	popq	%r12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	popq	%rbx
	movl	$129, %r8d
	popq	%r12
	movl	$65, %edx
	leaq	.LC0(%rip), %rcx
	movl	$199, %esi
	movl	$38, %edi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	ERR_put_error@PLT
	.cfi_endproc
.LFE886:
	.size	engine_cleanup_add_last, .-engine_cleanup_add_last
	.p2align 4
	.globl	engine_cleanup_int
	.type	engine_cleanup_int, @function
engine_cleanup_int:
.LFB888:
	.cfi_startproc
	endbr64
	movq	cleanup_stack(%rip), %rdi
	testq	%rdi, %rdi
	je	.L78
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	engine_cleanup_cb_free(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	OPENSSL_sk_pop_free@PLT
	movq	global_engine_lock(%rip), %rdi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	movq	$0, cleanup_stack(%rip)
	jmp	CRYPTO_THREAD_lock_free@PLT
	.p2align 4,,10
	.p2align 3
.L78:
	movq	global_engine_lock(%rip), %rdi
	jmp	CRYPTO_THREAD_lock_free@PLT
	.cfi_endproc
.LFE888:
	.size	engine_cleanup_int, .-engine_cleanup_int
	.p2align 4
	.globl	ENGINE_set_ex_data
	.type	ENGINE_set_ex_data, @function
ENGINE_set_ex_data:
.LFB889:
	.cfi_startproc
	endbr64
	addq	$168, %rdi
	jmp	CRYPTO_set_ex_data@PLT
	.cfi_endproc
.LFE889:
	.size	ENGINE_set_ex_data, .-ENGINE_set_ex_data
	.p2align 4
	.globl	ENGINE_get_ex_data
	.type	ENGINE_get_ex_data, @function
ENGINE_get_ex_data:
.LFB890:
	.cfi_startproc
	endbr64
	addq	$168, %rdi
	jmp	CRYPTO_get_ex_data@PLT
	.cfi_endproc
.LFE890:
	.size	ENGINE_get_ex_data, .-ENGINE_get_ex_data
	.p2align 4
	.globl	ENGINE_set_id
	.type	ENGINE_set_id, @function
ENGINE_set_id:
.LFB891:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L90
	movq	%rsi, (%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$67, %edx
	movl	$129, %esi
	movl	$38, %edi
	movl	$196, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE891:
	.size	ENGINE_set_id, .-ENGINE_set_id
	.p2align 4
	.globl	ENGINE_set_name
	.type	ENGINE_set_name, @function
ENGINE_set_name:
.LFB892:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L98
	movq	%rsi, 8(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$67, %edx
	movl	$130, %esi
	movl	$38, %edi
	movl	$206, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE892:
	.size	ENGINE_set_name, .-ENGINE_set_name
	.p2align 4
	.globl	ENGINE_set_destroy_function
	.type	ENGINE_set_destroy_function, @function
ENGINE_set_destroy_function:
.LFB893:
	.cfi_startproc
	endbr64
	movq	%rsi, 88(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE893:
	.size	ENGINE_set_destroy_function, .-ENGINE_set_destroy_function
	.p2align 4
	.globl	ENGINE_set_init_function
	.type	ENGINE_set_init_function, @function
ENGINE_set_init_function:
.LFB894:
	.cfi_startproc
	endbr64
	movq	%rsi, 96(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE894:
	.size	ENGINE_set_init_function, .-ENGINE_set_init_function
	.p2align 4
	.globl	ENGINE_set_finish_function
	.type	ENGINE_set_finish_function, @function
ENGINE_set_finish_function:
.LFB895:
	.cfi_startproc
	endbr64
	movq	%rsi, 104(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE895:
	.size	ENGINE_set_finish_function, .-ENGINE_set_finish_function
	.p2align 4
	.globl	ENGINE_set_ctrl_function
	.type	ENGINE_set_ctrl_function, @function
ENGINE_set_ctrl_function:
.LFB896:
	.cfi_startproc
	endbr64
	movq	%rsi, 112(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE896:
	.size	ENGINE_set_ctrl_function, .-ENGINE_set_ctrl_function
	.p2align 4
	.globl	ENGINE_set_flags
	.type	ENGINE_set_flags, @function
ENGINE_set_flags:
.LFB897:
	.cfi_startproc
	endbr64
	movl	%esi, 152(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE897:
	.size	ENGINE_set_flags, .-ENGINE_set_flags
	.p2align 4
	.globl	ENGINE_set_cmd_defns
	.type	ENGINE_set_cmd_defns, @function
ENGINE_set_cmd_defns:
.LFB898:
	.cfi_startproc
	endbr64
	movq	%rsi, 144(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE898:
	.size	ENGINE_set_cmd_defns, .-ENGINE_set_cmd_defns
	.p2align 4
	.globl	ENGINE_get_id
	.type	ENGINE_get_id, @function
ENGINE_get_id:
.LFB899:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE899:
	.size	ENGINE_get_id, .-ENGINE_get_id
	.p2align 4
	.globl	ENGINE_get_name
	.type	ENGINE_get_name, @function
ENGINE_get_name:
.LFB900:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE900:
	.size	ENGINE_get_name, .-ENGINE_get_name
	.p2align 4
	.globl	ENGINE_get_destroy_function
	.type	ENGINE_get_destroy_function, @function
ENGINE_get_destroy_function:
.LFB901:
	.cfi_startproc
	endbr64
	movq	88(%rdi), %rax
	ret
	.cfi_endproc
.LFE901:
	.size	ENGINE_get_destroy_function, .-ENGINE_get_destroy_function
	.p2align 4
	.globl	ENGINE_get_init_function
	.type	ENGINE_get_init_function, @function
ENGINE_get_init_function:
.LFB902:
	.cfi_startproc
	endbr64
	movq	96(%rdi), %rax
	ret
	.cfi_endproc
.LFE902:
	.size	ENGINE_get_init_function, .-ENGINE_get_init_function
	.p2align 4
	.globl	ENGINE_get_finish_function
	.type	ENGINE_get_finish_function, @function
ENGINE_get_finish_function:
.LFB903:
	.cfi_startproc
	endbr64
	movq	104(%rdi), %rax
	ret
	.cfi_endproc
.LFE903:
	.size	ENGINE_get_finish_function, .-ENGINE_get_finish_function
	.p2align 4
	.globl	ENGINE_get_ctrl_function
	.type	ENGINE_get_ctrl_function, @function
ENGINE_get_ctrl_function:
.LFB904:
	.cfi_startproc
	endbr64
	movq	112(%rdi), %rax
	ret
	.cfi_endproc
.LFE904:
	.size	ENGINE_get_ctrl_function, .-ENGINE_get_ctrl_function
	.p2align 4
	.globl	ENGINE_get_flags
	.type	ENGINE_get_flags, @function
ENGINE_get_flags:
.LFB905:
	.cfi_startproc
	endbr64
	movl	152(%rdi), %eax
	ret
	.cfi_endproc
.LFE905:
	.size	ENGINE_get_flags, .-ENGINE_get_flags
	.p2align 4
	.globl	ENGINE_get_cmd_defns
	.type	ENGINE_get_cmd_defns, @function
ENGINE_get_cmd_defns:
.LFB906:
	.cfi_startproc
	endbr64
	movq	144(%rdi), %rax
	ret
	.cfi_endproc
.LFE906:
	.size	ENGINE_get_cmd_defns, .-ENGINE_get_cmd_defns
	.p2align 4
	.globl	ENGINE_get_static_state
	.type	ENGINE_get_static_state, @function
ENGINE_get_static_state:
.LFB907:
	.cfi_startproc
	endbr64
	leaq	internal_static_hack(%rip), %rax
	ret
	.cfi_endproc
.LFE907:
	.size	ENGINE_get_static_state, .-ENGINE_get_static_state
	.local	internal_static_hack
	.comm	internal_static_hack,4,4
	.local	cleanup_stack
	.comm	cleanup_stack,8,8
	.globl	do_engine_lock_init_ossl_ret_
	.bss
	.align 4
	.type	do_engine_lock_init_ossl_ret_, @object
	.size	do_engine_lock_init_ossl_ret_, 4
do_engine_lock_init_ossl_ret_:
	.zero	4
	.globl	engine_lock_init
	.align 4
	.type	engine_lock_init, @object
	.size	engine_lock_init, 4
engine_lock_init:
	.zero	4
	.comm	global_engine_lock,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
