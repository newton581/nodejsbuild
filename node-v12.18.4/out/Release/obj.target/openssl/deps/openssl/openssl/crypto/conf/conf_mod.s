	.file	"conf_mod.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"openssl_conf"
.LC1:
	.string	"path"
.LC2:
	.string	"OPENSSL_init"
.LC3:
	.string	"OPENSSL_finish"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"../deps/openssl/openssl/crypto/conf/conf_mod.c"
	.section	.rodata.str1.1
.LC5:
	.string	", path="
.LC6:
	.string	"module="
.LC7:
	.string	"%-8d"
.LC8:
	.string	", retcode="
.LC9:
	.string	", value="
	.text
	.p2align 4
	.globl	CONF_modules_load
	.type	CONF_modules_load, @function
CONF_modules_load:
.LFB866:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L71
	movq	%rdx, %rbx
	testq	%rsi, %rsi
	je	.L7
	movq	%rsi, %rdx
	xorl	%esi, %esi
	call	NCONF_get_string@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L73
.L6:
	movq	-104(%rbp), %rdi
	call	NCONF_get_section@PLT
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L42
	movq	%rbx, %rax
	movq	-88(%rbp), %rdi
	andl	$8, %eax
	movq	%rax, -128(%rbp)
	movq	%rbx, %rax
	andl	$1, %ebx
	andl	$4, %eax
	movq	%rbx, -120(%rbp)
	xorl	%ebx, %ebx
	movq	%rax, -112(%rbp)
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L71
	.p2align 4,,10
	.p2align 3
.L32:
	movq	-88(%rbp), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movl	$46, %esi
	movq	8(%rax), %r13
	movq	16(%rax), %rcx
	movq	%r13, %rdi
	movq	%rcx, -96(%rbp)
	call	strrchr@PLT
	testq	%rax, %rax
	je	.L9
	subl	%r13d, %eax
.L10:
	xorl	%r15d, %r15d
	movslq	%eax, %r14
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L13:
	movq	supported_modules(%rip), %rdi
	movl	%r15d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	8(%rax), %rdi
	movq	%rax, %r12
	call	strncmp@PLT
	testl	%eax, %eax
	je	.L12
	addl	$1, %r15d
.L11:
	movq	supported_modules(%rip), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r15d
	jl	.L13
	cmpq	$0, -128(%rbp)
	je	.L74
.L40:
	cmpq	$0, -112(%rbp)
	je	.L75
	.p2align 4,,10
	.p2align 3
.L23:
	cmpq	$0, -120(%rbp)
	je	.L46
.L35:
	movq	-88(%rbp), %rdi
	addl	$1, %ebx
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L32
.L71:
	movl	$1, %eax
.L1:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L76
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L73:
	.cfi_restore_state
	testb	$32, %bl
	je	.L5
.L7:
	movq	-104(%rbp), %rdi
	xorl	%esi, %esi
	leaq	.LC0(%rip), %rdx
	call	NCONF_get_string@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L6
.L5:
	call	ERR_clear_error@PLT
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L74:
	movq	-96(%rbp), %rsi
	movq	-104(%rbp), %rdi
	leaq	.LC1(%rip), %rdx
	call	NCONF_get_string@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L77
.L15:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	xorl	%edi, %edi
	call	DSO_load@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L43
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	call	DSO_bind_func@PLT
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L44
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	call	DSO_bind_func@PLT
	cmpq	$0, supported_modules(%rip)
	movq	%rax, -152(%rbp)
	je	.L17
.L20:
	movl	$238, %edx
	leaq	.LC4(%rip), %rsi
	movl	$48, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L78
	movq	-144(%rbp), %xmm0
	movq	%r14, (%rax)
	movl	$244, %edx
	movq	%r13, %rdi
	leaq	.LC4(%rip), %rsi
	movhps	-152(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	call	CRYPTO_strdup@PLT
	movdqa	-144(%rbp), %xmm0
	movq	%rax, 8(%r12)
	movups	%xmm0, 16(%r12)
	testq	%rax, %rax
	je	.L79
	movq	supported_modules(%rip), %rdi
	movq	%r12, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L80
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$298, %edx
	leaq	.LC4(%rip), %rsi
	movl	$40, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L34
	movq	%r12, (%r15)
	movl	$303, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_strdup@PLT
	movq	-96(%rbp), %rdi
	movl	$304, %edx
	leaq	.LC4(%rip), %rsi
	movq	%rax, 8(%r15)
	call	CRYPTO_strdup@PLT
	movq	8(%r15), %rdi
	movq	$0, 32(%r15)
	movq	%rax, 16(%r15)
	testq	%rdi, %rdi
	sete	%dl
	testq	%rax, %rax
	sete	%r14b
	orb	%dl, %r14b
	jne	.L24
	movq	16(%r12), %rax
	testq	%rax, %rax
	je	.L25
	movq	-104(%rbp), %rsi
	movq	%r15, %rdi
	call	*%rax
	testl	%eax, %eax
	jle	.L26
	movl	$1, %r14d
.L25:
	movq	initialized_modules(%rip), %rdi
	testq	%rdi, %rdi
	je	.L81
.L27:
	movq	%r15, %rsi
	call	OPENSSL_sk_push@PLT
	movl	$328, %r8d
	testl	%eax, %eax
	jne	.L82
.L69:
.L28:
	endbr64
	movl	$65, %edx
	movl	$115, %esi
	movl	$14, %edi
	leaq	.LC4(%rip), %rcx
	call	ERR_put_error@PLT
	movq	24(%r12), %rax
	testq	%rax, %rax
	je	.L70
	testb	%r14b, %r14b
	je	.L70
.L29:
	movq	%r15, %rdi
	call	*%rax
.L70:
	movq	8(%r15), %rdi
.L24:
	movl	$344, %edx
	leaq	.LC4(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	16(%r15), %rdi
	movl	$345, %edx
	leaq	.LC4(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$346, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r15, %rdi
	call	CRYPTO_free@PLT
.L34:
	cmpq	$0, -112(%rbp)
	jne	.L23
	movl	$177, %r8d
	leaq	.LC4(%rip), %rcx
	movl	$109, %edx
	movl	$118, %esi
	movl	$14, %edi
	leaq	-69(%rbp), %r12
	call	ERR_put_error@PLT
	movl	$-1, %ecx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC7(%rip), %rdx
	movl	$13, %esi
	call	BIO_snprintf@PLT
	subq	$8, %rsp
	movq	-96(%rbp), %r8
	xorl	%eax, %eax
	pushq	%r12
	movq	%r13, %rdx
	leaq	.LC8(%rip), %r9
	leaq	.LC9(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	movl	$6, %edi
	call	ERR_add_error_data@PLT
	cmpq	$0, -120(%rbp)
	popq	%rax
	popq	%rdx
	jne	.L35
.L46:
	movl	$-1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%r13, %rdi
	call	strlen@PLT
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L82:
	addl	$1, 32(%r12)
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L81:
	call	OPENSSL_sk_new_null@PLT
	movl	$322, %r8d
	movq	%rax, initialized_modules(%rip)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L27
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L26:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L29
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L42:
	xorl	%eax, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L77:
	call	ERR_clear_error@PLT
	movq	%r13, %r15
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L43:
	movl	$110, %r9d
.L16:
	movq	%r14, %rdi
	movl	%r9d, -96(%rbp)
	call	DSO_free@PLT
	movl	-96(%rbp), %r9d
	movl	$224, %r8d
	leaq	.LC4(%rip), %rcx
	movl	$117, %esi
	movl	$14, %edi
	movl	%r9d, %edx
	call	ERR_put_error@PLT
	movq	%r15, %r8
	movq	%r13, %rdx
	movl	$4, %edi
	leaq	.LC5(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	xorl	%eax, %eax
	call	ERR_add_error_data@PLT
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L44:
	movl	$112, %r9d
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L17:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, supported_modules(%rip)
	testq	%rax, %rax
	jne	.L20
	xorl	%r9d, %r9d
	jmp	.L16
.L80:
	movq	8(%r12), %rdi
	movl	$253, %edx
	leaq	.LC4(%rip), %rsi
	movl	%eax, -96(%rbp)
	call	CRYPTO_free@PLT
	movl	$254, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	movl	-96(%rbp), %r9d
	jmp	.L16
.L75:
	movl	$165, %r8d
	movl	$113, %edx
	movl	$118, %esi
	movl	$14, %edi
	leaq	.LC4(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r13, %rdx
	movl	$2, %edi
	xorl	%eax, %eax
	leaq	.LC6(%rip), %rsi
	call	ERR_add_error_data@PLT
	jmp	.L23
.L78:
	movl	$239, %r8d
	movl	$65, %edx
	movl	$122, %esi
	movl	$14, %edi
	leaq	.LC4(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r9d, %r9d
	jmp	.L16
.L79:
	movl	$248, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	xorl	%r9d, %r9d
	jmp	.L16
.L76:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE866:
	.size	CONF_modules_load, .-CONF_modules_load
	.p2align 4
	.globl	CONF_modules_finish
	.type	CONF_modules_finish, @function
CONF_modules_finish:
.LFB875:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	.LC4(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L85:
	movq	initialized_modules(%rip), %rdi
	call	OPENSSL_sk_num@PLT
	movq	initialized_modules(%rip), %rdi
	testl	%eax, %eax
	jle	.L94
	call	OPENSSL_sk_pop@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L85
	movq	(%rax), %rax
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L86
	movq	%r12, %rdi
	call	*%rdx
	movq	(%r12), %rax
.L86:
	subl	$1, 32(%rax)
	movq	8(%r12), %rdi
	movq	%rbx, %rsi
	movl	$410, %edx
	call	CRYPTO_free@PLT
	movq	16(%r12), %rdi
	movq	%rbx, %rsi
	movl	$411, %edx
	call	CRYPTO_free@PLT
	movl	$412, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L94:
	call	OPENSSL_sk_free@PLT
	popq	%rbx
	popq	%r12
	movq	$0, initialized_modules(%rip)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE875:
	.size	CONF_modules_finish, .-CONF_modules_finish
	.p2align 4
	.globl	CONF_modules_unload
	.type	CONF_modules_unload, @function
CONF_modules_unload:
.LFB873:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%edi, %r12d
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	CONF_modules_finish
	movq	supported_modules(%rip), %rdi
	call	OPENSSL_sk_num@PLT
	subl	$1, %eax
	js	.L96
	movl	%eax, %ebx
	leaq	.LC4(%rip), %r13
	testl	%r12d, %r12d
	je	.L97
	.p2align 4,,10
	.p2align 3
.L98:
	movq	supported_modules(%rip), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	supported_modules(%rip), %rdi
	movl	%ebx, %esi
	subl	$1, %ebx
	movq	%rax, %r12
	call	OPENSSL_sk_delete@PLT
	movq	(%r12), %rdi
	call	DSO_free@PLT
	movq	8(%r12), %rdi
	movl	$384, %edx
	movq	%r13, %rsi
	call	CRYPTO_free@PLT
	movl	$385, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	cmpl	$-1, %ebx
	jne	.L98
.L96:
	movq	supported_modules(%rip), %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	je	.L105
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	cmpq	$0, (%r12)
	je	.L99
	movq	supported_modules(%rip), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_delete@PLT
	movq	(%r12), %rdi
	call	DSO_free@PLT
	movq	8(%r12), %rdi
	movl	$384, %edx
	movq	%r13, %rsi
	call	CRYPTO_free@PLT
	movl	$385, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
.L99:
	subl	$1, %ebx
	cmpl	$-1, %ebx
	je	.L96
.L97:
	movq	supported_modules(%rip), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r12
	movl	32(%rax), %eax
	testl	%eax, %eax
	jg	.L99
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L105:
	movq	supported_modules(%rip), %rdi
	call	OPENSSL_sk_free@PLT
	movq	$0, supported_modules(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE873:
	.size	CONF_modules_unload, .-CONF_modules_unload
	.p2align 4
	.globl	CONF_module_add
	.type	CONF_module_add, @function
CONF_module_add:
.LFB877:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	subq	$32, %rsp
	.cfi_offset 12, -32
	cmpq	$0, supported_modules(%rip)
	movq	%rsi, -32(%rbp)
	movq	%rdx, -40(%rbp)
	je	.L108
.L112:
	movl	$238, %edx
	leaq	.LC4(%rip), %rsi
	movl	$48, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L120
	movq	-32(%rbp), %xmm0
	movl	$244, %edx
	movq	%r13, %rdi
	movq	$0, (%rax)
	leaq	.LC4(%rip), %rsi
	movhps	-40(%rbp), %xmm0
	movaps	%xmm0, -32(%rbp)
	call	CRYPTO_strdup@PLT
	movdqa	-32(%rbp), %xmm0
	movq	%rax, 8(%r12)
	movups	%xmm0, 16(%r12)
	testq	%rax, %rax
	je	.L121
	movq	supported_modules(%rip), %rdi
	movq	%r12, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L122
	movl	$1, %eax
.L107:
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %rdx
	movq	%rax, supported_modules(%rip)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	jne	.L112
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore_state
	movl	$239, %r8d
	movl	$65, %edx
	movl	$122, %esi
	movl	$14, %edi
	leaq	.LC4(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L122:
	movq	8(%r12), %rdi
	movl	$253, %edx
	leaq	.LC4(%rip), %rsi
	movl	%eax, -32(%rbp)
	call	CRYPTO_free@PLT
	movl	$254, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	movl	-32(%rbp), %eax
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L121:
	movl	$248, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	xorl	%eax, %eax
	jmp	.L107
	.cfi_endproc
.LFE877:
	.size	CONF_module_add, .-CONF_module_add
	.p2align 4
	.globl	conf_modules_free_int
	.type	conf_modules_free_int, @function
conf_modules_free_int:
.LFB878:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	CONF_modules_finish
	movl	$1, %edi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	CONF_modules_unload
	.cfi_endproc
.LFE878:
	.size	conf_modules_free_int, .-conf_modules_free_int
	.p2align 4
	.globl	CONF_imodule_get_name
	.type	CONF_imodule_get_name, @function
CONF_imodule_get_name:
.LFB879:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE879:
	.size	CONF_imodule_get_name, .-CONF_imodule_get_name
	.p2align 4
	.globl	CONF_imodule_get_value
	.type	CONF_imodule_get_value, @function
CONF_imodule_get_value:
.LFB880:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE880:
	.size	CONF_imodule_get_value, .-CONF_imodule_get_value
	.p2align 4
	.globl	CONF_imodule_get_usr_data
	.type	CONF_imodule_get_usr_data, @function
CONF_imodule_get_usr_data:
.LFB881:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE881:
	.size	CONF_imodule_get_usr_data, .-CONF_imodule_get_usr_data
	.p2align 4
	.globl	CONF_imodule_set_usr_data
	.type	CONF_imodule_set_usr_data, @function
CONF_imodule_set_usr_data:
.LFB882:
	.cfi_startproc
	endbr64
	movq	%rsi, 32(%rdi)
	ret
	.cfi_endproc
.LFE882:
	.size	CONF_imodule_set_usr_data, .-CONF_imodule_set_usr_data
	.p2align 4
	.globl	CONF_imodule_get_module
	.type	CONF_imodule_get_module, @function
CONF_imodule_get_module:
.LFB883:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE883:
	.size	CONF_imodule_get_module, .-CONF_imodule_get_module
	.p2align 4
	.globl	CONF_imodule_get_flags
	.type	CONF_imodule_get_flags, @function
CONF_imodule_get_flags:
.LFB884:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE884:
	.size	CONF_imodule_get_flags, .-CONF_imodule_get_flags
	.p2align 4
	.globl	CONF_imodule_set_flags
	.type	CONF_imodule_set_flags, @function
CONF_imodule_set_flags:
.LFB885:
	.cfi_startproc
	endbr64
	movq	%rsi, 24(%rdi)
	ret
	.cfi_endproc
.LFE885:
	.size	CONF_imodule_set_flags, .-CONF_imodule_set_flags
	.p2align 4
	.globl	CONF_module_get_usr_data
	.type	CONF_module_get_usr_data, @function
CONF_module_get_usr_data:
.LFB886:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE886:
	.size	CONF_module_get_usr_data, .-CONF_module_get_usr_data
	.p2align 4
	.globl	CONF_module_set_usr_data
	.type	CONF_module_set_usr_data, @function
CONF_module_set_usr_data:
.LFB887:
	.cfi_startproc
	endbr64
	movq	%rsi, 40(%rdi)
	ret
	.cfi_endproc
.LFE887:
	.size	CONF_module_set_usr_data, .-CONF_module_set_usr_data
	.section	.rodata.str1.1
.LC10:
	.string	"OPENSSL_CONF"
.LC11:
	.string	"openssl.cnf"
.LC12:
	.string	"/"
.LC13:
	.string	"%s%s%s"
	.text
	.p2align 4
	.globl	CONF_get1_default_config_file
	.type	CONF_get1_default_config_file, @function
CONF_get1_default_config_file:
.LFB888:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC10(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	call	ossl_safe_getenv@PLT
	testq	%rax, %rax
	je	.L135
	popq	%r12
	movq	%rax, %rdi
	popq	%r13
	movl	$487, %edx
	leaq	.LC4(%rip), %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_strdup@PLT
	.p2align 4,,10
	.p2align 3
.L135:
	.cfi_restore_state
	call	X509_get_default_cert_area@PLT
	movq	%rax, %rdi
	call	strlen@PLT
	movl	$496, %edx
	leaq	.LC4(%rip), %rsi
	leal	13(%rax), %r12d
	movslq	%r12d, %r12
	movq	%r12, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L134
	call	X509_get_default_cert_area@PLT
	leaq	.LC11(%rip), %r9
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rcx
	leaq	.LC12(%rip), %r8
	leaq	.LC13(%rip), %rdx
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
.L134:
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE888:
	.size	CONF_get1_default_config_file, .-CONF_get1_default_config_file
	.p2align 4
	.globl	CONF_modules_load_file
	.type	CONF_modules_load_file, @function
CONF_modules_load_file:
.LFB867:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	xorl	%edi, %edi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	NCONF_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L148
	movq	%r14, %r15
	testq	%r14, %r14
	je	.L157
.L143:
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	NCONF_load@PLT
	testl	%eax, %eax
	jle	.L158
	movq	%r13, %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	CONF_modules_load
	movl	%eax, %r13d
.L142:
	testq	%r14, %r14
	je	.L144
.L146:
	movq	%r12, %rdi
	call	NCONF_free@PLT
	andl	$2, %ebx
	movl	$1, %eax
	cmovne	%eax, %r13d
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	movl	%r13d, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	xorl	%r13d, %r13d
	testb	$16, %bl
	je	.L142
	call	ERR_peek_last_error@PLT
	andl	$4095, %eax
	cmpl	$114, %eax
	jne	.L142
	call	ERR_clear_error@PLT
	movl	$1, %r13d
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L157:
	call	CONF_get1_default_config_file
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L143
	xorl	%r13d, %r13d
.L144:
	movl	$142, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r15, %rdi
	call	CRYPTO_free@PLT
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L148:
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	jmp	.L142
	.cfi_endproc
.LFE867:
	.size	CONF_modules_load_file, .-CONF_modules_load_file
	.p2align 4
	.globl	CONF_parse_list
	.type	CONF_parse_list, @function
CONF_parse_list:
.LFB889:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -56(%rbp)
	testq	%rdi, %rdi
	je	.L194
	movq	%rdi, %r15
	movl	%esi, %r13d
	movl	%edx, %r12d
	movq	%r8, %r14
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L162:
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	strchr@PLT
	movq	%rax, %rbx
	cmpq	%rax, %r15
	je	.L166
	cmpb	$0, (%r15)
	je	.L166
	leaq	-1(%rax), %rsi
	testq	%rax, %rax
	je	.L195
	testl	%r12d, %r12d
	jne	.L196
.L171:
	subq	%r15, %rsi
	movq	-56(%rbp), %rax
	movq	%r14, %rdx
	movq	%r15, %rdi
	addl	$1, %esi
	call	*%rax
	testl	%eax, %eax
	jle	.L159
.L197:
	testq	%rbx, %rbx
	je	.L174
	leaq	1(%rbx), %r15
.L160:
	testl	%r12d, %r12d
	je	.L162
	movzbl	(%r15), %ebx
	testb	%bl, %bl
	je	.L163
	call	__ctype_b_loc@PLT
	movq	(%rax), %rax
	.p2align 4,,10
	.p2align 3
.L164:
	testb	$32, 1(%rax,%rbx,2)
	je	.L162
	movzbl	1(%r15), %ebx
	addq	$1, %r15
	testb	%bl, %bl
	jne	.L164
.L163:
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	strchr@PLT
	movq	%rax, %rbx
.L166:
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	-56(%rbp), %rax
	movq	%r14, %rdx
	call	*%rax
	testl	%eax, %eax
	jg	.L197
.L159:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L195:
	.cfi_restore_state
	movq	%r15, %rdi
	call	strlen@PLT
	leaq	-1(%r15,%rax), %rsi
	testl	%r12d, %r12d
	je	.L171
.L196:
	movq	%rsi, -64(%rbp)
	call	__ctype_b_loc@PLT
	movq	-64(%rbp), %rsi
	movq	(%rax), %rdx
	movzbl	(%rsi), %eax
	testb	$32, 1(%rdx,%rax,2)
	je	.L171
	.p2align 4,,10
	.p2align 3
.L173:
	movzbl	-1(%rsi), %eax
	subq	$1, %rsi
	testb	$32, 1(%rdx,%rax,2)
	jne	.L173
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L174:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L194:
	.cfi_restore_state
	movl	$521, %r8d
	movl	$115, %edx
	movl	$119, %esi
	movl	$14, %edi
	leaq	.LC4(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L159
	.cfi_endproc
.LFE889:
	.size	CONF_parse_list, .-CONF_parse_list
	.local	initialized_modules
	.comm	initialized_modules,8,8
	.local	supported_modules
	.comm	supported_modules,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
