	.file	"async_err.c"
	.text
	.p2align 4
	.globl	ERR_load_ASYNC_strings
	.type	ERR_load_ASYNC_strings, @function
ERR_load_ASYNC_strings:
.LFB203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$856047616, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_func_error_string@PLT
	testq	%rax, %rax
	je	.L5
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	leaq	ASYNC_str_functs(%rip), %rdi
	call	ERR_load_strings_const@PLT
	leaq	ASYNC_str_reasons(%rip), %rdi
	call	ERR_load_strings_const@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE203:
	.size	ERR_load_ASYNC_strings, .-ERR_load_ASYNC_strings
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"failed to set pool"
.LC1:
	.string	"failed to swap context"
.LC2:
	.string	"init failed"
.LC3:
	.string	"invalid pool size"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	ASYNC_str_reasons, @object
	.size	ASYNC_str_reasons, 80
ASYNC_str_reasons:
	.quad	855638117
	.quad	.LC0
	.quad	855638118
	.quad	.LC1
	.quad	855638121
	.quad	.LC2
	.quad	855638119
	.quad	.LC3
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC4:
	.string	"async_ctx_new"
.LC5:
	.string	"ASYNC_init_thread"
.LC6:
	.string	"async_job_new"
.LC7:
	.string	"ASYNC_pause_job"
.LC8:
	.string	"async_start_func"
.LC9:
	.string	"ASYNC_start_job"
.LC10:
	.string	"ASYNC_WAIT_CTX_set_wait_fd"
	.section	.data.rel.ro.local
	.align 32
	.type	ASYNC_str_functs, @object
	.size	ASYNC_str_functs, 128
ASYNC_str_functs:
	.quad	856047616
	.quad	.LC4
	.quad	856051712
	.quad	.LC5
	.quad	856055808
	.quad	.LC6
	.quad	856059904
	.quad	.LC7
	.quad	856064000
	.quad	.LC8
	.quad	856068096
	.quad	.LC9
	.quad	856072192
	.quad	.LC10
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
