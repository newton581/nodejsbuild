	.file	"gcm128.c"
	.text
	.p2align 4
	.globl	CRYPTO_gcm128_init
	.type	CRYPTO_gcm128_init, @function
CRYPTO_gcm128_init:
.LFB152:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rdx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$8, %rdi
	movq	%rbx, %rcx
	leaq	80(%rbx), %r12
	subq	$40, %rsp
	movq	$0, -8(%rdi)
	movq	$0, 424(%rdi)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$440, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	%rdx, 376(%rbx)
	movq	%r12, %rdi
	movq	%rsi, %rdx
	movq	%rsi, 384(%rbx)
	movq	%r12, %rsi
	call	*%r8
	movq	80(%rbx), %rdx
	movq	88(%rbx), %rax
#APP
# 729 "../deps/openssl/openssl/crypto/modes/gcm128.c" 1
	bswapq %rdx
# 0 "" 2
# 730 "../deps/openssl/openssl/crypto/modes/gcm128.c" 1
	bswapq %rax
# 0 "" 2
#NO_APP
	movq	%rdx, 80(%rbx)
	movq	%rax, 88(%rbx)
	movl	4+OPENSSL_ia32cap_P(%rip), %ecx
	testb	$2, %cl
	je	.L2
	shrl	$22, %ecx
	leaq	96(%rbx), %rdi
	andl	$65, %ecx
	cmpl	$65, %ecx
	je	.L7
	movq	gcm_gmult_clmul@GOTPCREL(%rip), %xmm0
	movq	%r12, %rsi
	movhps	gcm_ghash_clmul@GOTPCREL(%rip), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	gcm_init_clmul@PLT
	movdqa	-64(%rbp), %xmm0
	movups	%xmm0, 352(%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movq	%rdx, %r8
	movq	%rax, %rcx
	pxor	%xmm0, %xmm0
	movq	%rdx, %rsi
	shrq	%rcx
	salq	$63, %r8
	movups	%xmm0, 96(%rbx)
	movq	%rax, %xmm1
	orq	%rcx, %r8
	movq	%rax, %rcx
	shrq	%rsi
	movq	%rdx, %xmm0
	andl	$1, %ecx
	movq	%r8, %r14
	punpcklqdq	%xmm1, %xmm0
	movabsq	$-2233785415175766016, %r11
	negq	%rcx
	movq	%r8, 168(%rbx)
	andq	%r11, %rcx
	movups	%xmm0, 224(%rbx)
	xorq	%rsi, %rcx
	movq	%r8, %rsi
	movq	%rcx, %r9
	shrq	%rsi
	movq	%rcx, %rdi
	movq	%rcx, %r15
	salq	$63, %r9
	shrq	%rdi
	movq	%rcx, %r13
	movq	%rcx, 160(%rbx)
	orq	%rsi, %r9
	movq	%r8, %rsi
	andl	$1, %esi
	movq	%r9, 136(%rbx)
	negq	%rsi
	andq	%r11, %rsi
	xorq	%rdi, %rsi
	movq	%r9, %rdi
	movq	%rsi, %r10
	shrq	%rdi
	xorq	%rsi, %r15
	movq	%rsi, %r12
	salq	$63, %r10
	movq	%r15, -72(%rbp)
	orq	%rdi, %r10
	movq	%r9, %rdi
	movq	%r15, 192(%rbx)
	movq	%rcx, %r15
	andl	$1, %edi
	xorq	%r10, %r14
	movq	%rsi, 128(%rbx)
	negq	%rdi
	movq	%r14, -64(%rbp)
	andq	%r11, %rdi
	movq	%rsi, %r11
	movq	%r14, 184(%rbx)
	movq	%r8, %r14
	shrq	%r11
	xorq	%r9, %r14
	movq	%r10, 120(%rbx)
	xorq	%r11, %rdi
	movq	%r9, %r11
	movq	%r14, -80(%rbp)
	xorq	%r10, %r11
	xorq	%rdi, %r12
	movq	%r14, 200(%rbx)
	movq	%r8, %r14
	xorq	%r12, %r15
	xorq	%r11, %r14
	movq	%rdi, 112(%rbx)
	xorq	%rdi, %r13
	xorq	%rdx, %rdi
	xorq	%rdx, %rcx
	xorq	%rax, %r10
	xorq	%rdx, %rsi
	movq	%rcx, 288(%rbx)
	movq	-64(%rbp), %rcx
	xorq	%rax, %r9
	xorq	%rax, %r8
	movq	%r12, 144(%rbx)
	xorq	%rdx, %r12
	xorq	%rax, %rcx
	movq	%r11, 152(%rbx)
	xorq	%rax, %r11
	movq	%rcx, 312(%rbx)
	movq	-72(%rbp), %rcx
	movq	%r13, 176(%rbx)
	xorq	%rdx, %r13
	xorq	%rdx, %rcx
	movq	%r15, 208(%rbx)
	xorq	%r15, %rdx
	movq	%r14, 216(%rbx)
	movq	%r12, 272(%rbx)
	movq	%r13, 304(%rbx)
	movq	%rdi, 240(%rbx)
	movq	%r10, 248(%rbx)
	movq	%rsi, 256(%rbx)
	movq	%r9, 264(%rbx)
	movq	%r11, 280(%rbx)
	movq	%r8, 296(%rbx)
	movq	%rcx, 320(%rbx)
	movq	-80(%rbp), %rcx
	movq	gcm_gmult_4bit@GOTPCREL(%rip), %xmm0
	movq	%rdx, 336(%rbx)
	xorq	%rax, %rcx
	xorq	%r14, %rax
	movhps	gcm_ghash_4bit@GOTPCREL(%rip), %xmm0
	movq	%rcx, 328(%rbx)
	movq	%rax, 344(%rbx)
	movups	%xmm0, 352(%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	movq	gcm_gmult_avx@GOTPCREL(%rip), %xmm0
	movq	%r12, %rsi
	movhps	gcm_ghash_avx@GOTPCREL(%rip), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	gcm_init_avx@PLT
	movdqa	-64(%rbp), %xmm0
	movups	%xmm0, 352(%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE152:
	.size	CRYPTO_gcm128_init, .-CRYPTO_gcm128_init
	.p2align 4
	.globl	CRYPTO_gcm128_setiv
	.type	CRYPTO_gcm128_setiv, @function
CRYPTO_gcm128_setiv:
.LFB153:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	352(%rdi), %rax
	movq	%rdx, -80(%rbp)
	movq	$0, 368(%rdi)
	movq	%rax, -64(%rbp)
	movups	%xmm0, 48(%rdi)
	cmpq	$12, %rdx
	je	.L57
	movq	%rdx, %rax
	movups	%xmm0, 64(%rdi)
	leaq	96(%rdi), %r13
	leaq	64(%rdi), %r14
	cmpq	$15, %rdx
	jbe	.L18
	subq	$16, %rax
	movb	$0, -54(%rbp)
	xorl	%r15d, %r15d
	xorl	%r11d, %r11d
	andq	$-16, %rax
	movb	$0, -53(%rbp)
	xorl	%r10d, %r10d
	xorl	%r9d, %r9d
	leaq	16(%rsi,%rax), %rax
	movb	$0, -52(%rbp)
	xorl	%r8d, %r8d
	xorl	%edi, %edi
	movq	%rax, -72(%rbp)
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movb	$0, -51(%rbp)
	xorl	%eax, %eax
	movb	$0, -50(%rbp)
	movb	$0, -49(%rbp)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L58:
	movzbl	70(%rbx), %r9d
	movzbl	71(%rbx), %r10d
	movzbl	72(%rbx), %r11d
	movzbl	73(%rbx), %r15d
	movzbl	64(%rbx), %eax
	movzbl	65(%rbx), %edx
	movb	%r9b, -49(%rbp)
	movzbl	74(%rbx), %r9d
	movzbl	66(%rbx), %ecx
	movb	%r10b, -50(%rbp)
	movzbl	75(%rbx), %r10d
	movzbl	67(%rbx), %esi
	movb	%r11b, -51(%rbp)
	movzbl	68(%rbx), %edi
	movzbl	69(%rbx), %r8d
	movb	%r15b, -52(%rbp)
	movzbl	78(%rbx), %r11d
	movzbl	79(%rbx), %r15d
	movb	%r9b, -53(%rbp)
	movb	%r10b, -54(%rbp)
	movzbl	76(%rbx), %r9d
	movzbl	77(%rbx), %r10d
.L13:
	xorb	(%r12), %al
	addq	$16, %r12
	movb	%al, 64(%rbx)
	xorb	-15(%r12), %dl
	movb	%dl, 65(%rbx)
	xorb	-14(%r12), %cl
	movb	%cl, 66(%rbx)
	xorb	-13(%r12), %sil
	movb	%sil, 67(%rbx)
	xorb	-12(%r12), %dil
	movq	%r13, %rsi
	movb	%dil, 68(%rbx)
	xorb	-11(%r12), %r8b
	movq	%r14, %rdi
	movb	%r8b, 69(%rbx)
	movzbl	-49(%rbp), %eax
	xorb	-10(%r12), %al
	movb	%al, 70(%rbx)
	movzbl	-50(%rbp), %eax
	xorb	-9(%r12), %al
	movb	%al, 71(%rbx)
	movzbl	-51(%rbp), %eax
	xorb	-8(%r12), %al
	movb	%al, 72(%rbx)
	movzbl	-52(%rbp), %eax
	xorb	-7(%r12), %al
	movb	%al, 73(%rbx)
	movzbl	-53(%rbp), %eax
	xorb	-6(%r12), %al
	movb	%al, 74(%rbx)
	movzbl	-54(%rbp), %eax
	xorb	-5(%r12), %al
	movb	%al, 75(%rbx)
	xorb	-4(%r12), %r9b
	movb	%r9b, 76(%rbx)
	xorb	-3(%r12), %r10b
	movb	%r10b, 77(%rbx)
	xorb	-2(%r12), %r11b
	movb	%r11b, 78(%rbx)
	xorb	-1(%r12), %r15b
	movb	%r15b, 79(%rbx)
	movq	-64(%rbp), %rax
	call	*%rax
	cmpq	%r12, -72(%rbp)
	jne	.L58
	movq	-80(%rbp), %rax
	andl	$15, %eax
.L11:
	testq	%rax, %rax
	jne	.L59
.L17:
	movq	-80(%rbp), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	salq	$3, %rax
#APP
# 874 "../deps/openssl/openssl/crypto/modes/gcm128.c" 1
	bswapq %rax
# 0 "" 2
#NO_APP
	xorq	%rax, 72(%rbx)
	movq	-64(%rbp), %rax
	call	*%rax
	movdqu	64(%rbx), %xmm1
	movl	76(%rbx), %eax
#APP
# 893 "../deps/openssl/openssl/crypto/modes/gcm128.c" 1
	bswapl %eax
# 0 "" 2
#NO_APP
	movups	%xmm1, (%rbx)
	leal	1(%rax), %r12d
.L10:
	pxor	%xmm0, %xmm0
	movq	384(%rbx), %rdx
	leaq	32(%rbx), %rsi
	movq	%rbx, %rdi
	movups	%xmm0, 64(%rbx)
	call	*376(%rbx)
	movl	%r12d, %eax
#APP
# 912 "../deps/openssl/openssl/crypto/modes/gcm128.c" 1
	bswapl %eax
# 0 "" 2
#NO_APP
	movl	%eax, 12(%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L57:
	.cfi_restore_state
	movq	(%rsi), %rax
	movl	$2, %r12d
	movq	%rax, (%rdi)
	movl	8(%rsi), %eax
	movl	$16777216, 12(%rdi)
	movl	%eax, 8(%rdi)
	jmp	.L10
.L18:
	movq	%rsi, -72(%rbp)
	jmp	.L11
.L59:
	movq	-72(%rbp), %rcx
	movzbl	(%rcx), %edx
	xorb	%dl, 64(%rbx)
	cmpq	$1, %rax
	je	.L14
	movzbl	1(%rcx), %edx
	xorb	%dl, 65(%rbx)
	cmpq	$2, %rax
	je	.L14
	movzbl	2(%rcx), %edx
	xorb	%dl, 66(%rbx)
	cmpq	$3, %rax
	je	.L14
	movzbl	3(%rcx), %edx
	xorb	%dl, 67(%rbx)
	cmpq	$4, %rax
	je	.L14
	movzbl	4(%rcx), %edx
	xorb	%dl, 68(%rbx)
	cmpq	$5, %rax
	je	.L14
	movzbl	5(%rcx), %edx
	xorb	%dl, 69(%rbx)
	cmpq	$6, %rax
	je	.L14
	movzbl	6(%rcx), %edx
	xorb	%dl, 70(%rbx)
	cmpq	$7, %rax
	je	.L14
	movzbl	7(%rcx), %edx
	xorb	%dl, 71(%rbx)
	cmpq	$8, %rax
	je	.L14
	movzbl	8(%rcx), %edx
	xorb	%dl, 72(%rbx)
	cmpq	$9, %rax
	je	.L14
	movzbl	9(%rcx), %edx
	xorb	%dl, 73(%rbx)
	cmpq	$10, %rax
	je	.L14
	movzbl	10(%rcx), %edx
	xorb	%dl, 74(%rbx)
	cmpq	$11, %rax
	je	.L14
	movzbl	11(%rcx), %edx
	xorb	%dl, 75(%rbx)
	cmpq	$12, %rax
	je	.L14
	movzbl	12(%rcx), %edx
	xorb	%dl, 76(%rbx)
	cmpq	$13, %rax
	je	.L14
	movzbl	13(%rcx), %edx
	xorb	%dl, 77(%rbx)
	cmpq	$15, %rax
	jne	.L14
	movzbl	14(%rcx), %eax
	xorb	%al, 78(%rbx)
.L14:
	movq	-64(%rbp), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	*%rax
	jmp	.L17
	.cfi_endproc
.LFE153:
	.size	CRYPTO_gcm128_setiv, .-CRYPTO_gcm128_setiv
	.p2align 4
	.globl	CRYPTO_gcm128_aad
	.type	CRYPTO_gcm128_aad, @function
CRYPTO_gcm128_aad:
.LFB154:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	$0, 56(%rdi)
	movq	48(%rdi), %rax
	movq	352(%rdi), %r8
	movq	360(%rdi), %r14
	jne	.L72
	xorl	%ecx, %ecx
	movq	%rdx, %r12
	addq	%rdx, %rax
	movabsq	$2305843009213693952, %rdx
	setc	%cl
	cmpq	%rdx, %rax
	ja	.L73
	testq	%rcx, %rcx
	jne	.L73
	movq	%rax, 48(%rdi)
	movl	372(%rdi), %eax
	movq	%rdi, %rbx
	movq	%rsi, %r13
	testl	%eax, %eax
	je	.L64
	testq	%r12, %r12
	je	.L69
	.p2align 4,,10
	.p2align 3
.L66:
	movl	%eax, %ecx
	addq	$1, %r13
	addl	$1, %eax
	subq	$1, %r12
	movzbl	64(%rbx,%rcx), %edx
	xorb	-1(%r13), %dl
	movb	%dl, 64(%rbx,%rcx)
	andl	$15, %eax
	je	.L141
	testq	%r12, %r12
	jne	.L66
	testl	%eax, %eax
	je	.L141
	.p2align 4,,10
	.p2align 3
.L69:
	movl	%eax, 372(%rbx)
	xorl	%eax, %eax
.L60:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	.cfi_restore_state
	leaq	96(%rbx), %rsi
	leaq	64(%rbx), %rdi
	call	*%r8
.L64:
	movq	%r12, %r15
	andq	$-16, %r15
	je	.L68
	movq	%r13, %rdx
	leaq	96(%rbx), %rsi
	leaq	64(%rbx), %rdi
	movq	%r15, %rcx
	call	*%r14
	addq	%r15, %r13
	subq	%r15, %r12
.L68:
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L69
	leaq	64(%rbx), %rcx
	leaq	16(%r13), %rdx
	movl	%r12d, %eax
	cmpq	%rdx, %rcx
	leaq	80(%rbx), %rdx
	setnb	%cl
	cmpq	%rdx, %r13
	setnb	%dl
	orb	%dl, %cl
	je	.L70
	leaq	-1(%r12), %rdx
	cmpq	$14, %rdx
	jbe	.L70
	movdqu	0(%r13), %xmm0
	movdqu	64(%rbx), %xmm1
	pxor	%xmm1, %xmm0
	movups	%xmm0, 64(%rbx)
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L70:
	movzbl	0(%r13), %edx
	xorb	%dl, 64(%rbx)
	cmpq	$1, %r12
	je	.L69
	movzbl	1(%r13), %edx
	xorb	%dl, 65(%rbx)
	cmpq	$2, %r12
	je	.L69
	movzbl	2(%r13), %edx
	xorb	%dl, 66(%rbx)
	cmpq	$3, %r12
	je	.L69
	movzbl	3(%r13), %edx
	xorb	%dl, 67(%rbx)
	cmpq	$4, %r12
	je	.L69
	movzbl	4(%r13), %edx
	xorb	%dl, 68(%rbx)
	cmpq	$5, %r12
	je	.L69
	movzbl	5(%r13), %edx
	xorb	%dl, 69(%rbx)
	cmpq	$6, %r12
	je	.L69
	movzbl	6(%r13), %edx
	xorb	%dl, 70(%rbx)
	cmpq	$7, %r12
	je	.L69
	movzbl	7(%r13), %edx
	xorb	%dl, 71(%rbx)
	cmpq	$8, %r12
	je	.L69
	movzbl	8(%r13), %edx
	xorb	%dl, 72(%rbx)
	cmpq	$9, %r12
	je	.L69
	movzbl	9(%r13), %edx
	xorb	%dl, 73(%rbx)
	cmpq	$10, %r12
	je	.L69
	movzbl	10(%r13), %edx
	xorb	%dl, 74(%rbx)
	cmpq	$11, %r12
	je	.L69
	movzbl	11(%r13), %edx
	xorb	%dl, 75(%rbx)
	cmpq	$12, %r12
	je	.L69
	movzbl	12(%r13), %edx
	xorb	%dl, 76(%rbx)
	cmpq	$13, %r12
	je	.L69
	movzbl	13(%r13), %edx
	xorb	%dl, 77(%rbx)
	cmpq	$14, %r12
	je	.L69
	movzbl	14(%r13), %edx
	xorb	%dl, 78(%rbx)
	cmpq	$15, %r12
	je	.L69
	movzbl	15(%r13), %edx
	xorb	%dl, 79(%rbx)
	jmp	.L69
.L73:
	movl	$-1, %eax
	jmp	.L60
.L72:
	movl	$-2, %eax
	jmp	.L60
	.cfi_endproc
.LFE154:
	.size	CRYPTO_gcm128_aad, .-CRYPTO_gcm128_aad
	.p2align 4
	.globl	CRYPTO_gcm128_encrypt
	.type	CRYPTO_gcm128_encrypt, @function
CRYPTO_gcm128_encrypt:
.LFB155:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	movq	%rax, %rdx
	subq	$104, %rsp
	movq	%rcx, -80(%rbp)
	addq	56(%rdi), %rdx
	movq	376(%rdi), %rcx
	movq	%rsi, -88(%rbp)
	movq	360(%rdi), %rsi
	movq	384(%rdi), %r13
	movq	%rcx, -64(%rbp)
	movq	352(%rdi), %rcx
	movabsq	$68719476704, %rdi
	movq	%rsi, -136(%rbp)
	setc	%sil
	cmpq	%rdi, %rdx
	ja	.L165
	movzbl	%sil, %esi
	testq	%rsi, %rsi
	jne	.L165
	movq	%rdx, 56(%r15)
	movl	372(%r15), %edx
	testl	%edx, %edx
	jne	.L235
	movl	12(%r15), %eax
	movl	368(%r15), %edx
	movl	%eax, %ecx
	movl	%edx, %eax
#APP
# 1029 "../deps/openssl/openssl/crypto/modes/gcm128.c" 1
	bswapl %ecx
# 0 "" 2
#NO_APP
	movl	%ecx, -96(%rbp)
	andl	$15, %eax
	je	.L150
	movq	-80(%rbp), %r8
	testq	%r8, %r8
	je	.L151
	movq	-88(%rbp), %rdi
	jmp	.L152
.L237:
	testq	%r8, %r8
	je	.L236
.L152:
	movl	%eax, %esi
	movzbl	(%rdi), %ecx
	addq	$1, %rbx
	addl	$1, %eax
	xorb	16(%r15,%rsi), %cl
	movl	%edx, %esi
	addq	$1, %rdi
	addl	$1, %edx
	movb	%cl, -1(%rbx)
	subq	$1, %r8
	movb	%cl, 392(%r15,%rsi)
	andl	$15, %eax
	jne	.L237
	movl	%eax, -100(%rbp)
	movq	%rdi, -88(%rbp)
	movq	%r8, -80(%rbp)
.L231:
	movl	%edx, %ecx
	movq	-136(%rbp), %rax
	leaq	96(%r15), %rsi
	leaq	392(%r15), %rdx
	leaq	64(%r15), %rdi
	call	*%rax
.L154:
	cmpq	$3071, -80(%rbp)
	jbe	.L158
	leaq	96(%r15), %rax
	movq	%rax, -120(%rbp)
	leaq	64(%r15), %rax
	movq	%rax, -128(%rbp)
.L159:
	leaq	16(%r15), %rax
	movq	%rbx, -112(%rbp)
	movq	%rax, -72(%rbp)
	movq	%rbx, %rax
	addq	$3072, %rax
	movq	%rax, -56(%rbp)
.L157:
	movl	-96(%rbp), %ebx
	movq	-88(%rbp), %r14
	movq	%r15, %rax
	movq	-112(%rbp), %r12
	movl	%ebx, %r15d
	movq	%r14, %rbx
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L156:
	addl	$1, %r15d
	movq	%r13, %rdx
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rax
	movq	%r14, %rdi
	addq	$16, %r12
	addq	$16, %rbx
	call	*%rax
	movl	%r15d, %edx
#APP
# 1090 "../deps/openssl/openssl/crypto/modes/gcm128.c" 1
	bswapl %edx
# 0 "" 2
#NO_APP
	movl	%edx, 12(%r14)
	movq	-16(%rbx), %rdx
	xorq	16(%r14), %rdx
	movq	%rdx, -16(%r12)
	movq	-8(%rbx), %rdx
	xorq	24(%r14), %rdx
	movq	%rdx, -8(%r12)
	cmpq	-56(%rbp), %r12
	jne	.L156
	movq	-112(%rbp), %rbx
	movl	$3072, %ecx
	movq	-120(%rbp), %rsi
	movq	%r14, %r15
	movq	-136(%rbp), %rax
	addl	$192, -96(%rbp)
	movq	%rbx, %rdx
	addq	$3072, -88(%rbp)
	movq	-128(%rbp), %rdi
	addq	$3072, %rbx
	call	*%rax
	leaq	3072(%r12), %rcx
	subq	$3072, -80(%rbp)
	movq	-80(%rbp), %rax
	movq	%rbx, -112(%rbp)
	movq	%rcx, -56(%rbp)
	cmpq	$3071, %rax
	ja	.L157
	movq	%r12, %rbx
.L158:
	movq	-80(%rbp), %r9
	andq	$-16, %r9
	jne	.L238
.L160:
	movq	-80(%rbp), %r14
	testq	%r14, %r14
	je	.L162
	leaq	16(%r15), %rsi
	movq	%r13, %rdx
	movq	-64(%rbp), %rax
	movq	%r15, %rdi
	call	*%rax
	movl	-96(%rbp), %edx
	movq	-88(%rbp), %r8
	movl	-100(%rbp), %esi
	addl	$1, %edx
#APP
# 1159 "../deps/openssl/openssl/crypto/modes/gcm128.c" 1
	bswapl %edx
# 0 "" 2
#NO_APP
	movl	%edx, 12(%r15)
	movzbl	16(%r15), %edx
	movq	%rsi, %r11
	leal	1(%rsi), %ecx
	xorb	(%r8), %dl
	movb	%dl, (%rbx)
	movb	%dl, 392(%r15,%rsi)
	movq	%r14, %rsi
	subq	$1, %rsi
	je	.L163
	movzbl	17(%r15), %edx
	movl	%ecx, %r9d
	xorb	1(%r8), %dl
	leal	2(%r11), %edi
	movb	%dl, 1(%rbx)
	movb	%dl, 392(%r15,%r9)
	cmpq	$2, %r14
	je	.L163
	movzbl	2(%r8), %edx
	xorb	18(%r15), %dl
	leal	3(%r11), %r9d
	movb	%dl, 2(%rbx)
	movb	%dl, 392(%r15,%rdi)
	cmpq	$3, %r14
	je	.L163
	movzbl	3(%r8), %edx
	xorb	19(%r15), %dl
	leal	4(%r11), %edi
	movb	%dl, 3(%rbx)
	movb	%dl, 392(%r15,%r9)
	cmpq	$4, %r14
	je	.L163
	movzbl	4(%r8), %edx
	xorb	20(%r15), %dl
	leal	5(%r11), %r9d
	movb	%dl, 4(%rbx)
	movb	%dl, 392(%r15,%rdi)
	cmpq	$5, %r14
	je	.L163
	movzbl	5(%r8), %edx
	xorb	21(%r15), %dl
	leal	6(%r11), %edi
	movb	%dl, 5(%rbx)
	movb	%dl, 392(%r15,%r9)
	cmpq	$6, %r14
	je	.L163
	movzbl	6(%r8), %edx
	xorb	22(%r15), %dl
	leal	7(%r11), %r9d
	movb	%dl, 6(%rbx)
	movb	%dl, 392(%r15,%rdi)
	cmpq	$7, %r14
	je	.L163
	movzbl	7(%r8), %edx
	xorb	23(%r15), %dl
	leal	8(%r11), %edi
	movb	%dl, 7(%rbx)
	movb	%dl, 392(%r15,%r9)
	cmpq	$8, %r14
	je	.L163
	movzbl	8(%r8), %edx
	xorb	24(%r15), %dl
	leal	9(%r11), %r9d
	movb	%dl, 8(%rbx)
	movb	%dl, 392(%r15,%rdi)
	cmpq	$9, %r14
	je	.L163
	movzbl	9(%r8), %edx
	xorb	25(%r15), %dl
	leal	10(%r11), %edi
	movb	%dl, 9(%rbx)
	movb	%dl, 392(%r15,%r9)
	cmpq	$10, %r14
	je	.L163
	movzbl	10(%r8), %edx
	xorb	26(%r15), %dl
	leal	11(%r11), %r9d
	movb	%dl, 10(%rbx)
	movb	%dl, 392(%r15,%rdi)
	cmpq	$11, %r14
	je	.L163
	movzbl	11(%r8), %edx
	xorb	27(%r15), %dl
	leal	12(%r11), %edi
	movb	%dl, 11(%rbx)
	movb	%dl, 392(%r15,%r9)
	cmpq	$12, %r14
	je	.L163
	movzbl	12(%r8), %edx
	xorb	28(%r15), %dl
	leal	13(%r11), %r9d
	movb	%dl, 12(%rbx)
	movb	%dl, 392(%r15,%rdi)
	cmpq	$13, %r14
	je	.L163
	movzbl	13(%r8), %edx
	movl	%r11d, %edi
	xorb	29(%r15), %dl
	movb	%dl, 13(%rbx)
	addl	$14, %edi
	movb	%dl, 392(%r15,%r9)
	cmpq	$14, %r14
	je	.L163
	movzbl	14(%r8), %edx
	movl	%edi, %eax
	xorb	30(%r15), %dl
	movb	%dl, 14(%rbx)
	movb	%dl, 392(%r15,%rax)
.L163:
	leal	(%rcx,%rsi), %eax
	movl	%eax, -100(%rbp)
.L162:
	movl	-100(%rbp), %eax
	movl	%eax, 368(%r15)
	addq	$104, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L235:
	.cfi_restore_state
	testq	%rax, %rax
	jne	.L148
	leaq	96(%r15), %rsi
	leaq	64(%r15), %rdi
	call	*%rcx
	xorl	%eax, %eax
	movl	$0, 372(%r15)
.L143:
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L238:
	.cfi_restore_state
	leaq	96(%r15), %rax
	movq	%rax, -120(%rbp)
	leaq	64(%r15), %rax
	movq	%rax, -128(%rbp)
.L164:
	movq	-80(%rbp), %rax
	movl	-96(%rbp), %ecx
	leaq	16(%r15), %rsi
	movq	%rbx, -72(%rbp)
	movq	%rbx, %r14
	movq	%r13, %rdx
	movq	-88(%rbp), %r12
	movq	%rsi, %r13
	leaq	-16(%rax), %r10
	addl	$1, %ecx
	shrq	$4, %r10
	movl	%ecx, %ebx
	leal	(%rcx,%r10), %eax
	movl	%eax, -56(%rbp)
	jmp	.L161
.L239:
	addl	$1, %ebx
.L161:
	movq	%r10, -112(%rbp)
	movq	-64(%rbp), %rax
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%r9, -144(%rbp)
	addq	$16, %r14
	addq	$16, %r12
	movq	%rdx, -96(%rbp)
	call	*%rax
	movl	%ebx, %eax
	movq	-96(%rbp), %rdx
	movq	-112(%rbp), %r10
#APP
# 1117 "../deps/openssl/openssl/crypto/modes/gcm128.c" 1
	bswapl %eax
# 0 "" 2
#NO_APP
	movl	%eax, 12(%r15)
	movq	-16(%r12), %rax
	xorq	16(%r15), %rax
	movq	-144(%rbp), %r9
	movq	%rax, -16(%r14)
	movq	-8(%r12), %rax
	xorq	24(%r15), %rax
	movq	%rax, -8(%r14)
	cmpl	-56(%rbp), %ebx
	jne	.L239
	movl	%ebx, -96(%rbp)
	movq	%rdx, %r13
	movq	-72(%rbp), %rbx
	leaq	1(%r10), %rdx
	salq	$4, %rdx
	addq	%rdx, -88(%rbp)
	movq	-120(%rbp), %rsi
	movq	%r9, %rcx
	addq	%rdx, %rbx
	andq	$15, -80(%rbp)
	movq	-128(%rbp), %rdi
	movq	%rbx, %rdx
	movq	-136(%rbp), %rax
	subq	%r9, %rdx
	call	*%rax
	jmp	.L160
.L236:
	movl	%eax, -100(%rbp)
	movq	%rdi, -88(%rbp)
	movq	%r8, -80(%rbp)
	testl	%eax, %eax
	je	.L231
.L151:
	movl	%edx, 368(%r15)
	addq	$104, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L148:
	.cfi_restore_state
	movl	12(%r15), %eax
	pxor	%xmm0, %xmm0
	movdqu	64(%r15), %xmm1
	movl	$0, 372(%r15)
	movl	$16, -100(%rbp)
	movl	$1, %ecx
	movl	%eax, %edx
	movups	%xmm1, 392(%r15)
#APP
# 1029 "../deps/openssl/openssl/crypto/modes/gcm128.c" 1
	bswapl %edx
# 0 "" 2
#NO_APP
	movl	%edx, -96(%rbp)
	movups	%xmm0, 64(%r15)
.L149:
	movq	-80(%rbp), %r14
	cmpq	$15, %r14
	jbe	.L154
	testb	%cl, %cl
	je	.L154
	leaq	96(%r15), %rsi
	leaq	64(%r15), %rdi
	movl	-100(%rbp), %ecx
	movq	-136(%rbp), %rax
	movq	%rsi, -120(%rbp)
	leaq	392(%r15), %rdx
	movq	%rdi, -128(%rbp)
	call	*%rax
	cmpq	$3071, %r14
	jbe	.L155
	movl	$0, -100(%rbp)
	jmp	.L159
.L155:
	movq	-80(%rbp), %r9
	movl	$0, -100(%rbp)
	andq	$-16, %r9
	jmp	.L164
.L165:
	movl	$-1, %eax
	jmp	.L143
.L150:
	testl	%edx, %edx
	movl	%edx, -100(%rbp)
	setne	%cl
	jmp	.L149
	.cfi_endproc
.LFE155:
	.size	CRYPTO_gcm128_encrypt, .-CRYPTO_gcm128_encrypt
	.p2align 4
	.globl	CRYPTO_gcm128_decrypt
	.type	CRYPTO_gcm128_decrypt, @function
CRYPTO_gcm128_decrypt:
.LFB156:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	movq	%rax, %rdx
	subq	$88, %rsp
	movq	%rcx, -80(%rbp)
	addq	56(%rdi), %rdx
	movq	376(%rdi), %rcx
	movq	%rsi, -88(%rbp)
	movq	360(%rdi), %rsi
	movq	384(%rdi), %r13
	movq	%rcx, -64(%rbp)
	movq	352(%rdi), %rcx
	movabsq	$68719476704, %rdi
	movq	%rsi, -128(%rbp)
	setc	%sil
	cmpq	%rdi, %rdx
	ja	.L262
	movzbl	%sil, %esi
	testq	%rsi, %rsi
	jne	.L262
	movq	%rdx, 56(%r15)
	movl	372(%r15), %edx
	testl	%edx, %edx
	jne	.L332
	movl	12(%r15), %eax
	movl	368(%r15), %edx
	movl	%eax, %ecx
	movl	%edx, %eax
#APP
# 1264 "../deps/openssl/openssl/crypto/modes/gcm128.c" 1
	bswapl %ecx
# 0 "" 2
#NO_APP
	movl	%ecx, -96(%rbp)
	andl	$15, %eax
	je	.L247
	movq	-80(%rbp), %r8
	testq	%r8, %r8
	je	.L248
	movq	-88(%rbp), %rdi
	jmp	.L249
.L334:
	testq	%r8, %r8
	je	.L333
.L249:
	movzbl	(%rdi), %ecx
	movl	%edx, %esi
	addq	$1, %rbx
	addq	$1, %rdi
	addl	$1, %edx
	subq	$1, %r8
	movb	%cl, 392(%r15,%rsi)
	movl	%eax, %esi
	addl	$1, %eax
	xorb	16(%r15,%rsi), %cl
	movb	%cl, -1(%rbx)
	andl	$15, %eax
	jne	.L334
	movl	%eax, -100(%rbp)
	movq	%rdi, -88(%rbp)
	movq	%r8, -80(%rbp)
.L328:
	movl	%edx, %ecx
	movq	-128(%rbp), %rax
	leaq	96(%r15), %rsi
	leaq	64(%r15), %rdi
	leaq	392(%r15), %rdx
	call	*%rax
.L251:
	cmpq	$3071, -80(%rbp)
	jbe	.L255
	leaq	96(%r15), %rax
	movq	%rax, -112(%rbp)
	leaq	64(%r15), %rax
	movq	%rax, -120(%rbp)
.L256:
	leaq	16(%r15), %rax
	movq	%rax, -72(%rbp)
	leaq	3072(%rbx), %rax
	movq	%rax, -56(%rbp)
.L254:
	movq	-88(%rbp), %rbx
	movq	-56(%rbp), %rax
	movl	$3072, %ecx
	movq	-112(%rbp), %rsi
	movq	-120(%rbp), %rdi
	movq	%rbx, %rdx
	leaq	-3072(%rax), %r12
	movq	-128(%rbp), %rax
	call	*%rax
	movl	-96(%rbp), %r14d
	movq	%r15, %rax
	movl	%r14d, %r15d
	movq	%rbx, %r14
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L253:
	addl	$1, %r15d
	movq	%r13, %rdx
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rax
	movq	%rbx, %rdi
	addq	$16, %r12
	addq	$16, %r14
	call	*%rax
	movl	%r15d, %edx
#APP
# 1328 "../deps/openssl/openssl/crypto/modes/gcm128.c" 1
	bswapl %edx
# 0 "" 2
#NO_APP
	movl	%edx, 12(%rbx)
	movq	-16(%r14), %rdx
	xorq	16(%rbx), %rdx
	movq	%rdx, -16(%r12)
	movq	-8(%r14), %rdx
	xorq	24(%rbx), %rdx
	movq	%rdx, -8(%r12)
	cmpq	-56(%rbp), %r12
	jne	.L253
	subq	$3072, -80(%rbp)
	movq	-80(%rbp), %rax
	movq	%rbx, %r15
	leaq	3072(%r12), %rcx
	addl	$192, -96(%rbp)
	addq	$3072, -88(%rbp)
	movq	%rcx, -56(%rbp)
	cmpq	$3071, %rax
	ja	.L254
	movq	%r12, %rbx
.L255:
	movq	-80(%rbp), %rcx
	andq	$-16, %rcx
	jne	.L335
.L257:
	movq	-80(%rbp), %r14
	testq	%r14, %r14
	je	.L259
	leaq	16(%r15), %rsi
	movq	%r13, %rdx
	movq	-64(%rbp), %rax
	movq	%r15, %rdi
	call	*%rax
	movl	-96(%rbp), %edx
	movq	-88(%rbp), %r10
	movl	-100(%rbp), %esi
	addl	$1, %edx
#APP
# 1397 "../deps/openssl/openssl/crypto/modes/gcm128.c" 1
	bswapl %edx
# 0 "" 2
#NO_APP
	movl	%edx, 12(%r15)
	movzbl	(%r10), %edx
	movq	%rsi, %r11
	leal	1(%rsi), %ecx
	movb	%dl, 392(%r15,%rsi)
	movq	%r14, %rsi
	xorb	16(%r15), %dl
	movb	%dl, (%rbx)
	subq	$1, %rsi
	je	.L260
	movzbl	1(%r10), %edx
	movl	%ecx, %r9d
	leal	2(%r11), %edi
	movb	%dl, 392(%r15,%r9)
	xorb	17(%r15), %dl
	movb	%dl, 1(%rbx)
	cmpq	$2, %r14
	je	.L260
	movzbl	2(%r10), %edx
	leal	3(%r11), %r9d
	movb	%dl, 392(%r15,%rdi)
	xorb	18(%r15), %dl
	movb	%dl, 2(%rbx)
	cmpq	$3, %r14
	je	.L260
	movzbl	3(%r10), %edx
	leal	4(%r11), %edi
	movb	%dl, 392(%r15,%r9)
	xorb	19(%r15), %dl
	movb	%dl, 3(%rbx)
	cmpq	$4, %r14
	je	.L260
	movzbl	4(%r10), %edx
	leal	5(%r11), %r9d
	movb	%dl, 392(%r15,%rdi)
	xorb	20(%r15), %dl
	movb	%dl, 4(%rbx)
	cmpq	$5, %r14
	je	.L260
	movzbl	5(%r10), %edx
	leal	6(%r11), %edi
	movb	%dl, 392(%r15,%r9)
	xorb	21(%r15), %dl
	movb	%dl, 5(%rbx)
	cmpq	$6, %r14
	je	.L260
	movzbl	6(%r10), %edx
	leal	7(%r11), %r9d
	movb	%dl, 392(%r15,%rdi)
	xorb	22(%r15), %dl
	movb	%dl, 6(%rbx)
	cmpq	$7, %r14
	je	.L260
	movzbl	7(%r10), %edx
	leal	8(%r11), %edi
	movb	%dl, 392(%r15,%r9)
	xorb	23(%r15), %dl
	movb	%dl, 7(%rbx)
	cmpq	$8, %r14
	je	.L260
	movzbl	8(%r10), %edx
	leal	9(%r11), %r9d
	movb	%dl, 392(%r15,%rdi)
	xorb	24(%r15), %dl
	movb	%dl, 8(%rbx)
	cmpq	$9, %r14
	je	.L260
	movzbl	9(%r10), %edx
	leal	10(%r11), %edi
	movb	%dl, 392(%r15,%r9)
	xorb	25(%r15), %dl
	movb	%dl, 9(%rbx)
	cmpq	$10, %r14
	je	.L260
	movzbl	10(%r10), %edx
	leal	11(%r11), %r9d
	movb	%dl, 392(%r15,%rdi)
	xorb	26(%r15), %dl
	movb	%dl, 10(%rbx)
	cmpq	$11, %r14
	je	.L260
	movzbl	11(%r10), %edx
	leal	12(%r11), %edi
	movb	%dl, 392(%r15,%r9)
	xorb	27(%r15), %dl
	movb	%dl, 11(%rbx)
	cmpq	$12, %r14
	je	.L260
	movzbl	12(%r10), %edx
	leal	13(%r11), %r9d
	movb	%dl, 392(%r15,%rdi)
	xorb	28(%r15), %dl
	movb	%dl, 12(%rbx)
	cmpq	$13, %r14
	je	.L260
	movzbl	13(%r10), %edx
	movl	%r11d, %edi
	addl	$14, %edi
	movb	%dl, 392(%r15,%r9)
	xorb	29(%r15), %dl
	movb	%dl, 13(%rbx)
	cmpq	$14, %r14
	je	.L260
	movzbl	14(%r10), %edx
	movb	%dl, 392(%r15,%rdi)
	xorb	30(%r15), %dl
	movb	%dl, 14(%rbx)
.L260:
	leal	(%rcx,%rsi), %eax
	movl	%eax, -100(%rbp)
.L259:
	movl	-100(%rbp), %eax
	movl	%eax, 368(%r15)
	addq	$88, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L332:
	.cfi_restore_state
	testq	%rax, %rax
	jne	.L245
	leaq	96(%r15), %rsi
	leaq	64(%r15), %rdi
	call	*%rcx
	xorl	%eax, %eax
	movl	$0, 372(%r15)
.L240:
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L335:
	.cfi_restore_state
	leaq	96(%r15), %rax
	movq	%rax, -112(%rbp)
	leaq	64(%r15), %rax
	movq	%rax, -120(%rbp)
	movq	%rax, %rdi
.L261:
	movq	-88(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	-128(%rbp), %rax
	call	*%rax
	movq	-80(%rbp), %rax
	movl	-96(%rbp), %r10d
	leaq	16(%r15), %rsi
	movq	-88(%rbp), %r9
	movq	%r13, %rdx
	movq	%rbx, -72(%rbp)
	leaq	-16(%rax), %r14
	addl	$1, %r10d
	shrq	$4, %r14
	movl	%r10d, %r13d
	leal	(%r10,%r14), %r12d
	movq	%r14, -56(%rbp)
	movq	%rsi, %r14
	movl	%r12d, %ecx
	movq	%r9, %r12
	jmp	.L258
.L336:
	addl	$1, %r13d
.L258:
	movl	%ecx, -112(%rbp)
	movq	-64(%rbp), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rdx, -96(%rbp)
	addq	$16, %rbx
	addq	$16, %r12
	call	*%rax
	movl	-112(%rbp), %ecx
	movl	%r13d, %eax
	movq	-96(%rbp), %rdx
#APP
# 1353 "../deps/openssl/openssl/crypto/modes/gcm128.c" 1
	bswapl %eax
# 0 "" 2
#NO_APP
	movl	%eax, 12(%r15)
	movq	-16(%r12), %rax
	movl	%r13d, %esi
	xorq	16(%r15), %rax
	movq	%rax, -16(%rbx)
	movq	-8(%r12), %rax
	xorq	24(%r15), %rax
	movq	%rax, -8(%rbx)
	cmpl	%ecx, %r13d
	jne	.L336
	movq	-56(%rbp), %r14
	movq	-72(%rbp), %rbx
	movq	%rdx, %r13
	movl	%esi, -96(%rbp)
	andq	$15, -80(%rbp)
	leaq	1(%r14), %rdx
	salq	$4, %rdx
	addq	%rdx, -88(%rbp)
	addq	%rdx, %rbx
	jmp	.L257
.L333:
	movl	%eax, -100(%rbp)
	movq	%rdi, -88(%rbp)
	movq	%r8, -80(%rbp)
	testl	%eax, %eax
	je	.L328
.L248:
	movl	%edx, 368(%r15)
	addq	$88, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L245:
	.cfi_restore_state
	movl	12(%r15), %eax
	pxor	%xmm0, %xmm0
	movdqu	64(%r15), %xmm1
	movl	$0, 372(%r15)
	movl	$16, -100(%rbp)
	movl	$1, %ecx
	movl	%eax, %edx
	movups	%xmm1, 392(%r15)
#APP
# 1264 "../deps/openssl/openssl/crypto/modes/gcm128.c" 1
	bswapl %edx
# 0 "" 2
#NO_APP
	movl	%edx, -96(%rbp)
	movups	%xmm0, 64(%r15)
.L246:
	movq	-80(%rbp), %r14
	cmpq	$15, %r14
	jbe	.L251
	testb	%cl, %cl
	je	.L251
	leaq	96(%r15), %rsi
	leaq	64(%r15), %rdi
	movl	-100(%rbp), %ecx
	movq	-128(%rbp), %rax
	movq	%rsi, -112(%rbp)
	leaq	392(%r15), %rdx
	movq	%rdi, -120(%rbp)
	call	*%rax
	cmpq	$3071, %r14
	jbe	.L252
	movl	$0, -100(%rbp)
	jmp	.L256
.L252:
	movq	-80(%rbp), %rcx
	movq	-120(%rbp), %rdi
	movl	$0, -100(%rbp)
	andq	$-16, %rcx
	jmp	.L261
.L262:
	movl	$-1, %eax
	jmp	.L240
.L247:
	testl	%edx, %edx
	movl	%edx, -100(%rbp)
	setne	%cl
	jmp	.L246
	.cfi_endproc
.LFE156:
	.size	CRYPTO_gcm128_decrypt, .-CRYPTO_gcm128_decrypt
	.p2align 4
	.globl	CRYPTO_gcm128_encrypt_ctr32
	.type	CRYPTO_gcm128_encrypt_ctr32, @function
CRYPTO_gcm128_encrypt_ctr32:
.LFB157:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movq	%rcx, %rdx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movabsq	$68719476704, %rsi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	360(%rdi), %rax
	addq	56(%rdi), %rdx
	movq	%r8, -64(%rbp)
	movq	384(%rdi), %r9
	movq	352(%rdi), %r8
	setc	%cl
	movq	%rax, -56(%rbp)
	cmpq	%rsi, %rdx
	ja	.L357
	movzbl	%cl, %ecx
	testq	%rcx, %rcx
	jne	.L357
	movl	372(%rdi), %eax
	movq	%rdx, 56(%rdi)
	movq	%rdi, %rbx
	testl	%eax, %eax
	jne	.L430
	movl	368(%rdi), %ecx
	movl	12(%rdi), %r13d
#APP
# 1509 "../deps/openssl/openssl/crypto/modes/gcm128.c" 1
	bswapl %r13d
# 0 "" 2
#NO_APP
	movl	%ecx, %r10d
	andl	$15, %r10d
	je	.L344
	testq	%r12, %r12
	je	.L345
	.p2align 4,,10
	.p2align 3
.L346:
	movl	%r10d, %edx
	movzbl	(%r14), %eax
	addq	$1, %r15
	addl	$1, %r10d
	xorb	16(%rbx,%rdx), %al
	movl	%ecx, %edx
	addq	$1, %r14
	addl	$1, %ecx
	movb	%al, -1(%r15)
	subq	$1, %r12
	movb	%al, 392(%rbx,%rdx)
	andl	$15, %r10d
	je	.L426
	testq	%r12, %r12
	jne	.L346
	testl	%r10d, %r10d
	je	.L426
.L345:
	movl	%ecx, 368(%rbx)
	addq	$72, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L430:
	.cfi_restore_state
	testq	%r12, %r12
	jne	.L342
	leaq	96(%rdi), %rsi
	leaq	64(%rdi), %rdi
	call	*%r8
	xorl	%eax, %eax
	movl	$0, 372(%rbx)
.L337:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L426:
	.cfi_restore_state
	leaq	64(%rbx), %rax
	movl	%r10d, -80(%rbp)
	leaq	96(%rbx), %rsi
	movq	%r9, -72(%rbp)
	movq	%rax, %rdi
	leaq	392(%rbx), %rdx
	movq	-56(%rbp), %rax
	call	*%rax
	movq	-72(%rbp), %r9
	movl	-80(%rbp), %r10d
.L348:
	leaq	96(%rbx), %rax
	movq	%rax, -80(%rbp)
	leaq	64(%rbx), %rax
	movq	%rax, -72(%rbp)
	cmpq	$3071, %r12
	jbe	.L351
	movq	%r9, -88(%rbp)
	movl	%r10d, -96(%rbp)
	.p2align 4,,10
	.p2align 3
.L350:
	addl	$192, %r13d
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rbx, %r8
	subq	$3072, %r12
	movq	-88(%rbp), %rcx
	movq	-64(%rbp), %rax
	movl	$192, %edx
	call	*%rax
	addq	$3072, %r14
	movq	-80(%rbp), %rsi
	movl	%r13d, %edx
	movq	-72(%rbp), %rdi
	movq	-56(%rbp), %rax
	movl	$3072, %ecx
#APP
# 1557 "../deps/openssl/openssl/crypto/modes/gcm128.c" 1
	bswapl %edx
# 0 "" 2
#NO_APP
	movl	%edx, 12(%rbx)
	movq	%r15, %rdx
	addq	$3072, %r15
	call	*%rax
	cmpq	$3071, %r12
	ja	.L350
	movq	-88(%rbp), %r9
	movl	-96(%rbp), %r10d
.L351:
	movq	%r12, %r11
	andq	$-16, %r11
	jne	.L431
.L353:
	testq	%r12, %r12
	je	.L354
	movl	%r10d, -56(%rbp)
	leaq	16(%rbx), %rsi
	movq	%r9, %rdx
	movq	%rbx, %rdi
	call	*376(%rbx)
	movl	-56(%rbp), %ecx
	movzbl	16(%rbx), %edx
	movq	%r12, %rdi
	addl	$1, %r13d
#APP
# 1602 "../deps/openssl/openssl/crypto/modes/gcm128.c" 1
	bswapl %r13d
# 0 "" 2
#NO_APP
	movl	%r13d, 12(%rbx)
	xorb	(%r14), %dl
	movq	%rcx, %r10
	leal	1(%rcx), %esi
	movb	%dl, (%r15)
	movb	%dl, 392(%rbx,%rcx)
	subq	$1, %rdi
	je	.L355
	movzbl	17(%rbx), %edx
	movl	%esi, %r8d
	xorb	1(%r14), %dl
	leal	2(%rcx), %ecx
	movb	%dl, 1(%r15)
	movb	%dl, 392(%rbx,%r8)
	cmpq	$2, %r12
	je	.L355
	movzbl	18(%rbx), %edx
	xorb	2(%r14), %dl
	leal	3(%r10), %r8d
	movb	%dl, 2(%r15)
	movb	%dl, 392(%rbx,%rcx)
	cmpq	$3, %r12
	je	.L355
	movzbl	3(%r14), %edx
	xorb	19(%rbx), %dl
	leal	4(%r10), %ecx
	movb	%dl, 3(%r15)
	movb	%dl, 392(%rbx,%r8)
	cmpq	$4, %r12
	je	.L355
	movzbl	4(%r14), %edx
	xorb	20(%rbx), %dl
	leal	5(%r10), %r8d
	movb	%dl, 4(%r15)
	movb	%dl, 392(%rbx,%rcx)
	cmpq	$5, %r12
	je	.L355
	movzbl	5(%r14), %edx
	xorb	21(%rbx), %dl
	leal	6(%r10), %ecx
	movb	%dl, 5(%r15)
	movb	%dl, 392(%rbx,%r8)
	cmpq	$6, %r12
	je	.L355
	movzbl	6(%r14), %edx
	xorb	22(%rbx), %dl
	leal	7(%r10), %r8d
	movb	%dl, 6(%r15)
	movb	%dl, 392(%rbx,%rcx)
	cmpq	$7, %r12
	je	.L355
	movzbl	7(%r14), %edx
	xorb	23(%rbx), %dl
	leal	8(%r10), %ecx
	movb	%dl, 7(%r15)
	movb	%dl, 392(%rbx,%r8)
	cmpq	$8, %r12
	je	.L355
	movzbl	8(%r14), %edx
	xorb	24(%rbx), %dl
	leal	9(%r10), %r8d
	movb	%dl, 8(%r15)
	movb	%dl, 392(%rbx,%rcx)
	cmpq	$9, %r12
	je	.L355
	movzbl	9(%r14), %edx
	xorb	25(%rbx), %dl
	leal	10(%r10), %ecx
	movb	%dl, 9(%r15)
	movb	%dl, 392(%rbx,%r8)
	cmpq	$10, %r12
	je	.L355
	movzbl	10(%r14), %edx
	xorb	26(%rbx), %dl
	leal	11(%r10), %r8d
	movb	%dl, 10(%r15)
	movb	%dl, 392(%rbx,%rcx)
	cmpq	$11, %r12
	je	.L355
	movzbl	11(%r14), %edx
	xorb	27(%rbx), %dl
	leal	12(%r10), %ecx
	movb	%dl, 11(%r15)
	movb	%dl, 392(%rbx,%r8)
	cmpq	$12, %r12
	je	.L355
	movzbl	12(%r14), %edx
	xorb	28(%rbx), %dl
	leal	13(%r10), %r8d
	movb	%dl, 12(%r15)
	movb	%dl, 392(%rbx,%rcx)
	cmpq	$13, %r12
	je	.L355
	movzbl	13(%r14), %edx
	xorb	29(%rbx), %dl
	leal	14(%r10), %ecx
	movb	%dl, 13(%r15)
	movb	%dl, 392(%rbx,%r8)
	cmpq	$14, %r12
	je	.L355
	movzbl	14(%r14), %edx
	xorb	30(%rbx), %dl
	addl	$15, %r10d
	movb	%dl, 14(%r15)
	movb	%dl, 392(%rbx,%rcx)
	cmpq	$15, %r12
	je	.L355
	movzbl	15(%r14), %edx
	xorb	31(%rbx), %dl
	movb	%dl, 15(%r15)
	movb	%dl, 392(%rbx,%r10)
	.p2align 4,,10
	.p2align 3
.L355:
	leal	(%rsi,%rdi), %r10d
.L354:
	movl	%r10d, 368(%rbx)
	addq	$72, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L431:
	.cfi_restore_state
	leaq	96(%rbx), %rax
	movq	%rax, -80(%rbp)
	leaq	64(%rbx), %rax
	movq	%rax, -72(%rbp)
.L356:
	movq	%r12, %rdx
	movq	%r9, %rcx
	movl	%r10d, -108(%rbp)
	movq	%r15, %rsi
	shrq	$4, %rdx
	movq	%r9, -96(%rbp)
	movq	%r14, %rdi
	movq	-64(%rbp), %rax
	movq	%r11, -104(%rbp)
	movq	%rbx, %r8
	movq	%rdx, -88(%rbp)
	call	*%rax
	movq	-88(%rbp), %rdx
	movq	-104(%rbp), %r11
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdi
	addl	%edx, %r13d
	movq	%r11, -64(%rbp)
	addq	%r11, %r14
	subq	%r11, %r12
	movq	%r11, %rcx
	movq	-56(%rbp), %rax
	movl	%r13d, %edx
#APP
# 1577 "../deps/openssl/openssl/crypto/modes/gcm128.c" 1
	bswapl %edx
# 0 "" 2
#NO_APP
	movl	%edx, 12(%rbx)
	movq	%r15, %rdx
	call	*%rax
	movq	-64(%rbp), %r11
	movl	-108(%rbp), %r10d
	movq	-96(%rbp), %r9
	addq	%r11, %r15
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L342:
	movdqu	64(%rdi), %xmm1
	pxor	%xmm0, %xmm0
	movl	$1, %edx
	movl	$0, 372(%rdi)
	movups	%xmm0, 64(%rdi)
	movl	$16, %ecx
	movl	12(%rdi), %r13d
	movups	%xmm1, 392(%rdi)
#APP
# 1509 "../deps/openssl/openssl/crypto/modes/gcm128.c" 1
	bswapl %r13d
# 0 "" 2
#NO_APP
.L343:
	cmpq	$15, %r12
	jbe	.L358
	testb	%dl, %dl
	je	.L358
	leaq	64(%rbx), %rax
	leaq	96(%rbx), %rsi
	movq	%r9, -88(%rbp)
	movq	%rax, -72(%rbp)
	movq	%rax, %rdi
	leaq	392(%rbx), %rdx
	movq	-56(%rbp), %rax
	movq	%rsi, -80(%rbp)
	call	*%rax
	cmpq	$3071, %r12
	movq	-88(%rbp), %r9
	jbe	.L349
	xorl	%r10d, %r10d
	movl	%r10d, -96(%rbp)
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L344:
	testl	%ecx, %ecx
	setne	%dl
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L358:
	movl	%ecx, %r10d
	jmp	.L348
.L349:
	movq	%r12, %r11
	xorl	%r10d, %r10d
	andq	$-16, %r11
	jmp	.L356
.L357:
	movl	$-1, %eax
	jmp	.L337
	.cfi_endproc
.LFE157:
	.size	CRYPTO_gcm128_encrypt_ctr32, .-CRYPTO_gcm128_encrypt_ctr32
	.p2align 4
	.globl	CRYPTO_gcm128_decrypt_ctr32
	.type	CRYPTO_gcm128_decrypt_ctr32, @function
CRYPTO_gcm128_decrypt_ctr32:
.LFB158:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	360(%rdi), %rax
	movq	%r8, -64(%rbp)
	movq	384(%rdi), %r15
	movq	352(%rdi), %r8
	movq	%rax, -56(%rbp)
	movq	%rcx, %rax
	addq	56(%rdi), %rax
	movabsq	$68719476704, %rcx
	setc	%dl
	cmpq	%rcx, %rax
	ja	.L452
	movzbl	%dl, %edx
	testq	%rdx, %rdx
	jne	.L452
	movq	%rax, 56(%rdi)
	movl	372(%rdi), %eax
	movq	%rdi, %rbx
	movq	%rsi, %r13
	testl	%eax, %eax
	jne	.L525
	movl	368(%rdi), %ecx
	movl	12(%rdi), %eax
#APP
# 1673 "../deps/openssl/openssl/crypto/modes/gcm128.c" 1
	bswapl %eax
# 0 "" 2
#NO_APP
	movl	%ecx, %r9d
	andl	$15, %r9d
	je	.L439
	testq	%r12, %r12
	je	.L440
	.p2align 4,,10
	.p2align 3
.L441:
	movzbl	0(%r13), %edx
	movl	%ecx, %esi
	addq	$1, %r14
	addq	$1, %r13
	addl	$1, %ecx
	subq	$1, %r12
	movb	%dl, 392(%rbx,%rsi)
	movl	%r9d, %esi
	addl	$1, %r9d
	xorb	16(%rbx,%rsi), %dl
	movb	%dl, -1(%r14)
	andl	$15, %r9d
	je	.L521
	testq	%r12, %r12
	jne	.L441
	testl	%r9d, %r9d
	je	.L521
.L440:
	movl	%ecx, 368(%rbx)
	addq	$56, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L525:
	.cfi_restore_state
	testq	%r12, %r12
	jne	.L437
	leaq	96(%rdi), %rsi
	leaq	64(%rdi), %rdi
	call	*%r8
	xorl	%eax, %eax
	movl	$0, 372(%rbx)
.L432:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L521:
	.cfi_restore_state
	leaq	96(%rbx), %r10
	leaq	64(%rbx), %r11
	movl	%r9d, -80(%rbp)
	movl	%eax, -72(%rbp)
	leaq	392(%rbx), %rdx
	movq	%r10, %rsi
	movq	%r11, %rdi
	movq	-56(%rbp), %rax
	call	*%rax
	movl	-72(%rbp), %eax
	movl	-80(%rbp), %r9d
.L443:
	leaq	96(%rbx), %r10
	leaq	64(%rbx), %r11
	cmpq	$3071, %r12
	jbe	.L446
	movl	%r9d, -92(%rbp)
	movq	%r10, -80(%rbp)
	movq	%r11, -88(%rbp)
	movq	%r15, -72(%rbp)
	movq	%r12, %r15
	movl	%eax, %r12d
	.p2align 4,,10
	.p2align 3
.L445:
	movq	%r13, %rdx
	movq	-80(%rbp), %rsi
	movq	-88(%rbp), %rdi
	movl	$3072, %ecx
	addl	$192, %r12d
	subq	$3072, %r15
	movq	-56(%rbp), %rax
	call	*%rax
	movl	$192, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	-72(%rbp), %rcx
	movq	-64(%rbp), %rax
	movq	%rbx, %r8
	addq	$3072, %r14
	call	*%rax
	addq	$3072, %r13
	movl	%r12d, %edx
#APP
# 1724 "../deps/openssl/openssl/crypto/modes/gcm128.c" 1
	bswapl %edx
# 0 "" 2
#NO_APP
	movl	%edx, 12(%rbx)
	cmpq	$3071, %r15
	ja	.L445
	movl	%r12d, %eax
	movl	-92(%rbp), %r9d
	movq	%r15, %r12
	movq	-72(%rbp), %r15
.L446:
	movq	%r12, %rdi
	andq	$-16, %rdi
	movq	%rdi, -72(%rbp)
	jne	.L526
.L448:
	movl	%eax, -56(%rbp)
	testq	%r12, %r12
	je	.L449
	movl	%r9d, -64(%rbp)
	leaq	16(%rbx), %rsi
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	*376(%rbx)
	movl	-56(%rbp), %eax
	movl	-64(%rbp), %ecx
	movq	%r12, %rsi
	addl	$1, %eax
	movq	%rcx, %r9
	leal	1(%rcx), %edx
#APP
# 1771 "../deps/openssl/openssl/crypto/modes/gcm128.c" 1
	bswapl %eax
# 0 "" 2
#NO_APP
	movl	%eax, 12(%rbx)
	movzbl	0(%r13), %eax
	movb	%al, 392(%rbx,%rcx)
	xorb	16(%rbx), %al
	movb	%al, (%r14)
	subq	$1, %rsi
	je	.L450
	movzbl	1(%r13), %eax
	movl	%edx, %edi
	leal	2(%rcx), %ecx
	movb	%al, 392(%rbx,%rdi)
	xorb	17(%rbx), %al
	movb	%al, 1(%r14)
	cmpq	$2, %r12
	je	.L450
	movzbl	2(%r13), %eax
	leal	3(%r9), %edi
	movb	%al, 392(%rbx,%rcx)
	xorb	18(%rbx), %al
	movb	%al, 2(%r14)
	cmpq	$3, %r12
	je	.L450
	movzbl	3(%r13), %eax
	leal	4(%r9), %ecx
	movb	%al, 392(%rbx,%rdi)
	xorb	19(%rbx), %al
	movb	%al, 3(%r14)
	cmpq	$4, %r12
	je	.L450
	movzbl	4(%r13), %eax
	leal	5(%r9), %edi
	movb	%al, 392(%rbx,%rcx)
	xorb	20(%rbx), %al
	movb	%al, 4(%r14)
	cmpq	$5, %r12
	je	.L450
	movzbl	5(%r13), %eax
	leal	6(%r9), %ecx
	movb	%al, 392(%rbx,%rdi)
	xorb	21(%rbx), %al
	movb	%al, 5(%r14)
	cmpq	$6, %r12
	je	.L450
	movzbl	6(%r13), %eax
	leal	7(%r9), %edi
	movb	%al, 392(%rbx,%rcx)
	xorb	22(%rbx), %al
	movb	%al, 6(%r14)
	cmpq	$7, %r12
	je	.L450
	movzbl	7(%r13), %eax
	leal	8(%r9), %ecx
	movb	%al, 392(%rbx,%rdi)
	xorb	23(%rbx), %al
	movb	%al, 7(%r14)
	cmpq	$8, %r12
	je	.L450
	movzbl	8(%r13), %eax
	leal	9(%r9), %edi
	movb	%al, 392(%rbx,%rcx)
	xorb	24(%rbx), %al
	movb	%al, 8(%r14)
	cmpq	$9, %r12
	je	.L450
	movzbl	9(%r13), %eax
	leal	10(%r9), %ecx
	movb	%al, 392(%rbx,%rdi)
	xorb	25(%rbx), %al
	movb	%al, 9(%r14)
	cmpq	$10, %r12
	je	.L450
	movzbl	10(%r13), %eax
	leal	11(%r9), %edi
	movb	%al, 392(%rbx,%rcx)
	xorb	26(%rbx), %al
	movb	%al, 10(%r14)
	cmpq	$11, %r12
	je	.L450
	movzbl	11(%r13), %eax
	leal	12(%r9), %ecx
	movb	%al, 392(%rbx,%rdi)
	xorb	27(%rbx), %al
	movb	%al, 11(%r14)
	cmpq	$12, %r12
	je	.L450
	movzbl	12(%r13), %eax
	leal	13(%r9), %edi
	movb	%al, 392(%rbx,%rcx)
	xorb	28(%rbx), %al
	movb	%al, 12(%r14)
	cmpq	$13, %r12
	je	.L450
	movzbl	13(%r13), %eax
	leal	14(%r9), %ecx
	movb	%al, 392(%rbx,%rdi)
	xorb	29(%rbx), %al
	movb	%al, 13(%r14)
	cmpq	$14, %r12
	je	.L450
	movzbl	14(%r13), %eax
	addl	$15, %r9d
	movb	%al, 392(%rbx,%rcx)
	xorb	30(%rbx), %al
	movb	%al, 14(%r14)
	cmpq	$15, %r12
	je	.L450
	movzbl	15(%r13), %eax
	movb	%al, 392(%rbx,%r9)
	xorb	31(%rbx), %al
	movb	%al, 15(%r14)
	.p2align 4,,10
	.p2align 3
.L450:
	leal	(%rdx,%rsi), %r9d
.L449:
	movl	%r9d, 368(%rbx)
	addq	$56, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L526:
	.cfi_restore_state
	leaq	96(%rbx), %r10
	leaq	64(%rbx), %r11
	movq	%rdi, %rcx
.L451:
	movl	%eax, -88(%rbp)
	movq	%r12, %rax
	movq	%r13, %rdx
	movq	%r10, %rsi
	shrq	$4, %rax
	movl	%r9d, -92(%rbp)
	movq	%r11, %rdi
	movq	-56(%rbp), %r10
	movq	%rax, -80(%rbp)
	call	*%r10
	movq	%r13, %rdi
	movq	-80(%rbp), %rdx
	movq	%r14, %rsi
	movq	-64(%rbp), %r11
	movq	%rbx, %r8
	movq	%r15, %rcx
	call	*%r11
	movq	-72(%rbp), %rdi
	movl	-88(%rbp), %eax
	addl	-80(%rbp), %eax
	movl	-92(%rbp), %r9d
	movl	%eax, %edx
	addq	%rdi, %r14
	addq	%rdi, %r13
	subq	%rdi, %r12
#APP
# 1756 "../deps/openssl/openssl/crypto/modes/gcm128.c" 1
	bswapl %edx
# 0 "" 2
#NO_APP
	movl	%edx, 12(%rbx)
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L437:
	movdqu	64(%rdi), %xmm1
	pxor	%xmm0, %xmm0
	movl	$1, %edx
	movl	12(%rdi), %eax
	movl	$0, 372(%rdi)
	movl	$16, %ecx
	movups	%xmm1, 392(%rdi)
	movups	%xmm0, 64(%rdi)
#APP
# 1673 "../deps/openssl/openssl/crypto/modes/gcm128.c" 1
	bswapl %eax
# 0 "" 2
#NO_APP
.L438:
	cmpq	$15, %r12
	jbe	.L453
	testb	%dl, %dl
	je	.L453
	leaq	96(%rbx), %r10
	leaq	64(%rbx), %r11
	movl	%eax, -88(%rbp)
	movq	-56(%rbp), %rax
	movq	%r10, -80(%rbp)
	movq	%r10, %rsi
	movq	%r11, %rdi
	leaq	392(%rbx), %rdx
	movq	%r11, -72(%rbp)
	call	*%rax
	cmpq	$3071, %r12
	movq	-72(%rbp), %r11
	movl	-88(%rbp), %eax
	movq	-80(%rbp), %r10
	jbe	.L444
	xorl	%r9d, %r9d
	movq	%r15, -72(%rbp)
	movq	%r12, %r15
	movl	%eax, %r12d
	movl	%r9d, -92(%rbp)
	movq	%r11, -88(%rbp)
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L439:
	testl	%ecx, %ecx
	setne	%dl
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L453:
	movl	%ecx, %r9d
	jmp	.L443
.L444:
	movq	%r12, %rdi
	xorl	%r9d, %r9d
	andq	$-16, %rdi
	movq	%rdi, -72(%rbp)
	movq	%rdi, %rcx
	jmp	.L451
.L452:
	movl	$-1, %eax
	jmp	.L432
	.cfi_endproc
.LFE158:
	.size	CRYPTO_gcm128_decrypt_ctr32, .-CRYPTO_gcm128_decrypt_ctr32
	.p2align 4
	.globl	CRYPTO_gcm128_finish
	.type	CRYPTO_gcm128_finish, @function
CRYPTO_gcm128_finish:
.LFB159:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	392(%rbx), %r15
	leaq	96(%rbx), %r9
	leaq	64(%rbx), %r12
	subq	$56, %rsp
	movq	48(%rdi), %rax
	movq	360(%rdi), %r10
	leaq	0(,%rax,8), %r11
	movq	56(%rdi), %rax
	movl	368(%rdi), %edi
	leaq	0(,%rax,8), %r8
	testl	%edi, %edi
	jne	.L540
	movl	372(%rbx), %eax
	testl	%eax, %eax
	jne	.L541
	movq	%r15, %rsi
	movl	$16, %ecx
.L530:
	movq	%r11, %rdx
	movq	%r8, %rax
	movq	%r12, %rdi
#APP
# 1834 "../deps/openssl/openssl/crypto/modes/gcm128.c" 1
	bswapq %rdx
# 0 "" 2
# 1835 "../deps/openssl/openssl/crypto/modes/gcm128.c" 1
	bswapq %rax
# 0 "" 2
#NO_APP
	movq	%rdx, (%rsi)
	movq	%r15, %rdx
	movq	%rax, 8(%rsi)
	movq	%r9, %rsi
	call	*%r10
	movdqu	64(%rbx), %xmm0
	movdqu	32(%rbx), %xmm1
	pxor	%xmm1, %xmm0
	movups	%xmm0, 64(%rbx)
	testq	%r13, %r13
	je	.L531
	cmpq	$16, %r14
	ja	.L531
	addq	$56, %rsp
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_memcmp@PLT
	.p2align 4,,10
	.p2align 3
.L541:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%r10, -80(%rbp)
	movq	%r12, %rdi
	movq	%r8, -72(%rbp)
	movq	%r11, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	*352(%rbx)
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %r11
	movq	%r15, %rsi
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %r10
	movl	$16, %ecx
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L540:
	leal	15(%rdi), %ecx
	xorl	%esi, %esi
	movq	%r9, -88(%rbp)
	andl	$-16, %ecx
	movq	%r10, -80(%rbp)
	movl	%ecx, %edx
	movq	%r8, -72(%rbp)
	subl	%edi, %edx
	addq	%r15, %rdi
	movq	%r11, -64(%rbp)
	movl	%ecx, -56(%rbp)
	call	memset@PLT
	movl	-56(%rbp), %ecx
	movq	-64(%rbp), %r11
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %r10
	cmpl	$48, %ecx
	movq	-88(%rbp), %r9
	je	.L529
	movl	%ecx, %esi
	addl	$16, %ecx
	addq	%r15, %rsi
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L529:
	movq	%r9, %rsi
	movq	%r8, -80(%rbp)
	movl	$48, %ecx
	movq	%r15, %rdx
	movq	%r11, -72(%rbp)
	movq	%r12, %rdi
	movq	%r9, -64(%rbp)
	movq	%r10, -56(%rbp)
	call	*%r10
	movq	-56(%rbp), %r10
	movq	-64(%rbp), %r9
	movq	%r15, %rsi
	movq	-72(%rbp), %r11
	movq	-80(%rbp), %r8
	movl	$16, %ecx
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L531:
	addq	$56, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE159:
	.size	CRYPTO_gcm128_finish, .-CRYPTO_gcm128_finish
	.p2align 4
	.globl	CRYPTO_gcm128_tag
	.type	CRYPTO_gcm128_tag, @function
CRYPTO_gcm128_tag:
.LFB160:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	392(%rbx), %r8
	leaq	96(%rbx), %r9
	leaq	64(%rbx), %r15
	subq	$56, %rsp
	movq	48(%rdi), %rax
	movq	360(%rdi), %r10
	leaq	0(,%rax,8), %r11
	movq	56(%rdi), %rax
	movl	368(%rdi), %edi
	leaq	0(,%rax,8), %r14
	testl	%edi, %edi
	jne	.L558
	movl	372(%rbx), %eax
	testl	%eax, %eax
	jne	.L559
	movq	%r8, %rsi
	movl	$16, %ecx
.L545:
	movq	%r11, %rdx
	movq	%r14, %rax
	movq	%r15, %rdi
#APP
# 1834 "../deps/openssl/openssl/crypto/modes/gcm128.c" 1
	bswapq %rdx
# 0 "" 2
# 1835 "../deps/openssl/openssl/crypto/modes/gcm128.c" 1
	bswapq %rax
# 0 "" 2
#NO_APP
	movq	%rdx, (%rsi)
	movq	%r8, %rdx
	movq	%rax, 8(%rsi)
	movq	%r9, %rsi
	call	*%r10
	movdqu	64(%rbx), %xmm0
	movdqu	32(%rbx), %xmm1
	cmpq	$16, %r12
	movl	$16, %edx
	cmovbe	%r12, %rdx
	pxor	%xmm1, %xmm0
	movups	%xmm0, 64(%rbx)
	cmpl	$8, %edx
	jnb	.L546
	testb	$4, %dl
	jne	.L560
	testl	%edx, %edx
	je	.L542
	movzbl	64(%rbx), %eax
	movb	%al, 0(%r13)
	testb	$2, %dl
	jne	.L561
.L542:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L559:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%r8, -80(%rbp)
	movq	%r15, %rdi
	movq	%r10, -72(%rbp)
	movq	%r11, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	*352(%rbx)
	movq	-80(%rbp), %r8
	movq	-56(%rbp), %r9
	movl	$16, %ecx
	movq	-64(%rbp), %r11
	movq	-72(%rbp), %r10
	movq	%r8, %rsi
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L546:
	movq	64(%rbx), %rax
	movq	%rax, 0(%r13)
	movl	%edx, %eax
	movq	-8(%r15,%rax), %rcx
	movq	%rcx, -8(%r13,%rax)
	leaq	8(%r13), %rcx
	andq	$-8, %rcx
	subq	%rcx, %r13
	addl	%r13d, %edx
	subq	%r13, %r15
	andl	$-8, %edx
	cmpl	$8, %edx
	jb	.L542
	andl	$-8, %edx
	xorl	%eax, %eax
.L550:
	movl	%eax, %esi
	addl	$8, %eax
	movq	(%r15,%rsi), %rdi
	movq	%rdi, (%rcx,%rsi)
	cmpl	%edx, %eax
	jb	.L550
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L558:
	.cfi_restore_state
	leal	15(%rdi), %ecx
	xorl	%esi, %esi
	movq	%r9, -88(%rbp)
	andl	$-16, %ecx
	movq	%r10, -80(%rbp)
	movl	%ecx, %edx
	movq	%r11, -72(%rbp)
	subl	%edi, %edx
	addq	%r8, %rdi
	movl	%ecx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	memset@PLT
	movl	-64(%rbp), %ecx
	movq	-56(%rbp), %r8
	movq	-72(%rbp), %r11
	movq	-80(%rbp), %r10
	cmpl	$48, %ecx
	movq	-88(%rbp), %r9
	je	.L544
	movl	%ecx, %esi
	addl	$16, %ecx
	addq	%r8, %rsi
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L544:
	movq	%r9, %rsi
	movq	%r11, -80(%rbp)
	movl	$48, %ecx
	movq	%r8, %rdx
	movq	%r8, -72(%rbp)
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	movq	%r10, -56(%rbp)
	call	*%r10
	movq	-72(%rbp), %r8
	movq	-56(%rbp), %r10
	movl	$16, %ecx
	movq	-64(%rbp), %r9
	movq	-80(%rbp), %r11
	movq	%r8, %rsi
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L560:
	movl	64(%rbx), %eax
	movl	%eax, 0(%r13)
	movl	%edx, %eax
	movl	-4(%r15,%rax), %edx
	movl	%edx, -4(%r13,%rax)
	jmp	.L542
	.p2align 4,,10
	.p2align 3
.L561:
	movl	%edx, %edx
	movzwl	-2(%r15,%rdx), %eax
	movw	%ax, -2(%r13,%rdx)
	jmp	.L542
	.cfi_endproc
.LFE160:
	.size	CRYPTO_gcm128_tag, .-CRYPTO_gcm128_tag
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/modes/gcm128.c"
	.text
	.p2align 4
	.globl	CRYPTO_gcm128_new
	.type	CRYPTO_gcm128_new, @function
CRYPTO_gcm128_new:
.LFB161:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1879, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	leaq	.LC0(%rip), %rsi
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movl	$440, %edi
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L562
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	CRYPTO_gcm128_init
.L562:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE161:
	.size	CRYPTO_gcm128_new, .-CRYPTO_gcm128_new
	.p2align 4
	.globl	CRYPTO_gcm128_release
	.type	CRYPTO_gcm128_release, @function
CRYPTO_gcm128_release:
.LFB162:
	.cfi_startproc
	endbr64
	movl	$1887, %ecx
	leaq	.LC0(%rip), %rdx
	movl	$440, %esi
	jmp	CRYPTO_clear_free@PLT
	.cfi_endproc
.LFE162:
	.size	CRYPTO_gcm128_release, .-CRYPTO_gcm128_release
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
