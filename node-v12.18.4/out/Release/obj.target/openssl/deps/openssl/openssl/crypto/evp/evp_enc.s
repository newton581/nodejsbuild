	.file	"evp_enc.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/evp/evp_enc.c"
	.align 8
.LC1:
	.string	"assertion failed: bl <= (int)sizeof(ctx->buf)"
	.text
	.p2align 4
	.type	evp_EncryptDecryptUpdate, @function
evp_EncryptDecryptUpdate:
.LFB866:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	$8192, %esi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$40, %rsp
	call	EVP_CIPHER_CTX_test_flags@PLT
	movl	%ebx, %edx
	testl	%eax, %eax
	je	.L2
	movl	%ebx, %eax
	leal	14(%rbx), %edx
	addl	$7, %eax
	cmovns	%eax, %edx
	sarl	$3, %edx
.L2:
	testl	%ebx, %ebx
	js	.L3
	movq	(%r12), %r9
	movl	4(%r9), %r8d
	je	.L51
	testb	$16, 18(%r9)
	je	.L6
.L56:
	cmpl	$1, %r8d
	je	.L52
.L7:
	movslq	%ebx, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*32(%r9)
	testl	%eax, %eax
	js	.L49
	movl	%eax, (%r15)
	addq	$40, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movslq	20(%r12), %rdi
	movslq	%edx, %rsi
	movq	%rsi, %r10
	leaq	(%r14,%rdi), %rax
	negq	%r10
	movq	%rdi, %rcx
	subq	%r13, %rax
	cmpq	%r10, %rax
	seta	%r10b
	cmpq	%rsi, %rax
	setb	%sil
	orb	%sil, %r10b
	je	.L9
	testl	%edx, %edx
	setg	%dl
	testq	%rax, %rax
	setne	%al
	testb	%al, %dl
	je	.L9
	movl	$335, %r8d
.L48:
	movl	$162, %edx
	movl	$219, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L49:
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	movq	%r9, %rdi
	movl	%r8d, -60(%rbp)
	movl	%edx, -56(%rbp)
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpq	$7, %rax
	je	.L53
.L3:
	movl	$0, (%r15)
	xorl	%eax, %eax
	testl	%ebx, %ebx
	sete	%al
.L1:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	testl	%ecx, %ecx
	je	.L54
	cmpl	$32, %r8d
	jg	.L18
	movl	%r8d, %r9d
	leaq	56(%r12,%rdi), %rdi
	subl	%ecx, %r9d
	cmpl	%ebx, %r9d
	jg	.L55
	movslq	%r9d, %r10
	movq	%r13, %rsi
	movl	%r8d, -56(%rbp)
	movq	%r10, %rdx
	movl	%r9d, -64(%rbp)
	movq	%r10, -72(%rbp)
	call	memcpy@PLT
	movslq	-56(%rbp), %rcx
	movq	(%r12), %rax
	movq	%r14, %rsi
	leaq	56(%r12), %rdx
	movq	%r12, %rdi
	movl	%ecx, -60(%rbp)
	movq	%rcx, -56(%rbp)
	call	*32(%rax)
	testl	%eax, %eax
	je	.L49
	movl	-64(%rbp), %r9d
	movq	-72(%rbp), %r10
	movq	-56(%rbp), %rcx
	movl	-60(%rbp), %r8d
	subl	%r9d, %ebx
	addq	%r10, %r13
	movl	%r8d, (%r15)
	addq	%rcx, %r14
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L54:
	testl	%ebx, 132(%r12)
	jne	.L11
	movslq	%ebx, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*32(%r9)
	testl	%eax, %eax
	je	.L12
	movl	%ebx, (%r15)
	movl	$1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L53:
	movq	(%r12), %r9
	movl	-56(%rbp), %edx
	movl	-60(%rbp), %r8d
	testb	$16, 18(%r9)
	je	.L6
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L52:
	movslq	%edx, %rcx
	movq	%r14, %rax
	movq	%rcx, %rsi
	subq	%r13, %rax
	negq	%rsi
	cmpq	%rsi, %rax
	seta	%sil
	cmpq	%rcx, %rax
	setb	%cl
	orb	%cl, %sil
	je	.L7
	testl	%edx, %edx
	movl	$322, %r8d
	setg	%dl
	testq	%rax, %rax
	setne	%al
	testb	%al, %dl
	je	.L7
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L11:
	cmpl	$32, %r8d
	jg	.L18
	movl	$0, (%r15)
.L15:
	subl	$1, %r8d
	andl	%ebx, %r8d
	subl	%r8d, %ebx
	testl	%ebx, %ebx
	jle	.L16
	movq	(%r12), %rax
	movl	%r8d, -56(%rbp)
	movslq	%ebx, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*32(%rax)
	testl	%eax, %eax
	je	.L49
	addl	%ebx, (%r15)
	movl	-56(%rbp), %r8d
.L16:
	testl	%r8d, %r8d
	jne	.L57
.L17:
	movl	%r8d, 20(%r12)
	addq	$40, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	movl	$0, (%r15)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L57:
	movslq	%ebx, %rbx
	movslq	%r8d, %rdx
	leaq	56(%r12), %rdi
	movl	%r8d, -56(%rbp)
	leaq	0(%r13,%rbx), %rsi
	call	memcpy@PLT
	movl	-56(%rbp), %r8d
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L55:
	movslq	%ebx, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	addl	%ebx, 20(%r12)
	movl	$1, %eax
	movl	$0, (%r15)
	jmp	.L1
.L18:
	movl	$349, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE866:
	.size	evp_EncryptDecryptUpdate, .-evp_EncryptDecryptUpdate
	.p2align 4
	.globl	EVP_CIPHER_CTX_reset
	.type	EVP_CIPHER_CTX_reset, @function
EVP_CIPHER_CTX_reset:
.LFB853:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L65
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L76
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L64
	call	*%rax
	testl	%eax, %eax
	je	.L58
.L64:
	movq	120(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L61
	movq	(%rbx), %rax
	movslq	48(%rax), %rsi
	testl	%esi, %esi
	jne	.L77
.L61:
	movl	$32, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	8(%rbx), %rdi
	call	ENGINE_finish@PLT
	leaq	8(%rbx), %rdi
	movq	$0, (%rbx)
	xorl	%eax, %eax
	movq	$0, 160(%rbx)
	andq	$-8, %rdi
	subq	%rdi, %rbx
	leal	168(%rbx), %ecx
	shrl	$3, %ecx
	rep stosq
	movl	$1, %eax
.L58:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	call	OPENSSL_cleanse@PLT
.L76:
	movq	120(%rbx), %rdi
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE853:
	.size	EVP_CIPHER_CTX_reset, .-EVP_CIPHER_CTX_reset
	.p2align 4
	.globl	EVP_CIPHER_CTX_new
	.type	EVP_CIPHER_CTX_new, @function
EVP_CIPHER_CTX_new:
.LFB854:
	.cfi_startproc
	endbr64
	movl	$42, %edx
	leaq	.LC0(%rip), %rsi
	movl	$168, %edi
	jmp	CRYPTO_zalloc@PLT
	.cfi_endproc
.LFE854:
	.size	EVP_CIPHER_CTX_new, .-EVP_CIPHER_CTX_new
	.p2align 4
	.globl	EVP_CIPHER_CTX_free
	.type	EVP_CIPHER_CTX_free, @function
EVP_CIPHER_CTX_free:
.LFB855:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	testq	%rdi, %rdi
	je	.L81
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L95
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L87
	call	*%rax
	testl	%eax, %eax
	je	.L81
.L87:
	movq	120(%r12), %rdi
	testq	%rdi, %rdi
	je	.L83
	movq	(%r12), %rax
	movslq	48(%rax), %rsi
	testl	%esi, %esi
	jne	.L96
.L83:
	movl	$32, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	8(%r12), %rdi
	call	ENGINE_finish@PLT
	leaq	8(%r12), %rdi
	movq	%r12, %rcx
	xorl	%eax, %eax
	andq	$-8, %rdi
	movq	$0, (%r12)
	movq	$0, 160(%r12)
	subq	%rdi, %rcx
	addl	$168, %ecx
	shrl	$3, %ecx
	rep stosq
.L81:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %edx
	popq	%r12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	call	OPENSSL_cleanse@PLT
.L95:
	movq	120(%r12), %rdi
	jmp	.L83
	.cfi_endproc
.LFE855:
	.size	EVP_CIPHER_CTX_free, .-EVP_CIPHER_CTX_free
	.section	.rodata.str1.8
	.align 8
.LC2:
	.string	"assertion failed: ctx->cipher->block_size == 1 || ctx->cipher->block_size == 8 || ctx->cipher->block_size == 16"
	.align 8
.LC3:
	.string	"assertion failed: EVP_CIPHER_CTX_iv_length(ctx) <= (int)sizeof(ctx->iv)"
	.text
	.p2align 4
	.globl	EVP_CipherInit
	.type	EVP_CipherInit, @function
EVP_CipherInit:
.LFB856:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	testq	%rsi, %rsi
	je	.L98
	call	EVP_CIPHER_CTX_reset
.L98:
	cmpl	$-1, %r15d
	je	.L169
	testl	%r15d, %r15d
	movq	(%rbx), %rax
	setne	%r15b
	cmpq	$0, 8(%rbx)
	movzbl	%r15b, %r15d
	movl	%r15d, 16(%rbx)
	je	.L101
.L176:
	testq	%rax, %rax
	je	.L102
	testq	%r12, %r12
	je	.L103
	movl	(%rax), %ecx
	cmpl	%ecx, (%r12)
	je	.L103
.L104:
	movq	112(%rbx), %rdx
	movq	%rbx, %rdi
	movq	%rdx, -56(%rbp)
	call	EVP_CIPHER_CTX_reset
	movq	-56(%rbp), %rdx
	movl	%r15d, 16(%rbx)
	movq	%rdx, 112(%rbx)
.L106:
	movl	(%r12), %edi
	call	ENGINE_get_cipher_engine@PLT
	testq	%rax, %rax
	je	.L107
.L177:
	movl	(%r12), %esi
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	ENGINE_get_cipher@PLT
	movq	-56(%rbp), %rdx
	movl	$112, %r8d
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L166
	movslq	48(%r12), %rdi
	movq	%rdx, 8(%rbx)
	movq	%r12, (%rbx)
	testl	%edi, %edi
	jne	.L170
.L111:
	movq	$0, 120(%rbx)
	movq	%r12, %rax
.L113:
	movl	8(%r12), %edx
	andq	$1, 112(%rbx)
	movl	%edx, 104(%rbx)
	testb	$64, 16(%rax)
	jne	.L171
.L103:
	movl	4(%rax), %eax
	leal	-8(%rax), %edx
	andl	$-9, %edx
	je	.L117
	cmpl	$1, %eax
	jne	.L172
.L117:
	testb	$1, 112(%rbx)
	jne	.L118
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpq	$65538, %rax
	je	.L173
.L118:
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	testb	$16, %al
	je	.L174
.L120:
	movq	(%rbx), %rax
	testq	%r13, %r13
	je	.L175
.L128:
	movl	%r15d, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	*24(%rax)
	testl	%eax, %eax
	je	.L97
	movq	(%rbx), %rax
.L129:
	movl	4(%rax), %eax
	movl	$0, 20(%rbx)
	movl	$0, 128(%rbx)
	subl	$1, %eax
	movl	%eax, 132(%rbx)
	movl	$1, %eax
.L97:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	testq	%r12, %r12
	jne	.L106
.L131:
	movl	$148, %r8d
	movl	$131, %edx
	movl	$123, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L169:
	cmpq	$0, 8(%rbx)
	movl	16(%rbx), %r15d
	movq	(%rbx), %rax
	jne	.L176
.L101:
	testq	%r12, %r12
	je	.L105
	testq	%rax, %rax
	jne	.L104
	movl	(%r12), %edi
	call	ENGINE_get_cipher_engine@PLT
	testq	%rax, %rax
	jne	.L177
.L107:
	movslq	48(%r12), %rdi
	movq	$0, 8(%rbx)
	movq	%r12, (%rbx)
	testl	%edi, %edi
	je	.L111
.L170:
	movl	$128, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_zalloc@PLT
	movq	%rax, 120(%rbx)
	testq	%rax, %rax
	je	.L178
	movq	(%rbx), %rax
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L105:
	testq	%rax, %rax
	jne	.L103
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L174:
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpq	$4, %rax
	ja	.L121
	cmpq	$2, %rax
	ja	.L122
	jne	.L120
.L123:
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_iv_length@PLT
	cmpl	$16, %eax
	jg	.L179
	leaq	24(%rbx), %r12
	testq	%r14, %r14
	je	.L126
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_iv_length@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movslq	%eax, %rdx
	call	memcpy@PLT
.L126:
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_iv_length@PLT
	leaq	40(%rbx), %rdi
	movq	%r12, %rsi
	movslq	%eax, %rdx
	call	memcpy@PLT
	movq	(%rbx), %rax
	testq	%r13, %r13
	jne	.L128
	.p2align 4,,10
	.p2align 3
.L175:
	testb	$32, 16(%rax)
	je	.L129
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L121:
	cmpq	$5, %rax
	jne	.L167
	movl	$0, 88(%rbx)
	testq	%r14, %r14
	je	.L120
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_iv_length@PLT
	leaq	40(%rbx), %rdi
	movq	%r14, %rsi
	movslq	%eax, %rdx
	call	memcpy@PLT
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L180:
	movl	$634, %r8d
	movl	$132, %edx
	movl	$124, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L115:
	movq	$0, (%rbx)
	movl	$143, %r8d
.L166:
	movl	$134, %edx
	movl	$123, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L167:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	movq	72(%rax), %rax
	testq	%rax, %rax
	je	.L180
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	*%rax
	cmpl	$-1, %eax
	je	.L181
	testl	%eax, %eax
	je	.L115
	movq	(%rbx), %rax
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L122:
	movl	$0, 88(%rbx)
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L173:
	movl	$161, %r8d
	movl	$170, %edx
	movl	$123, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L178:
	movl	$131, %r8d
	movl	$65, %edx
	movl	$123, %esi
	movq	$0, (%rbx)
	leaq	.LC0(%rip), %rcx
	movl	$6, %edi
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L181:
	movl	$640, %r8d
	movl	$133, %edx
	movl	$124, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L115
.L172:
	movl	$155, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	OPENSSL_die@PLT
.L179:
	movl	$180, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE856:
	.size	EVP_CipherInit, .-EVP_CipherInit
	.p2align 4
	.globl	EVP_CipherInit_ex
	.type	EVP_CipherInit_ex, @function
EVP_CipherInit_ex:
.LFB857:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpl	$-1, %r9d
	je	.L252
	xorl	%r15d, %r15d
	testl	%r9d, %r9d
	movq	(%rbx), %rax
	setne	%r15b
	cmpq	$0, 8(%rbx)
	movl	%r15d, 16(%rdi)
	je	.L185
.L260:
	testq	%rax, %rax
	je	.L186
	testq	%r12, %r12
	je	.L187
	movl	(%rax), %esi
	cmpl	%esi, (%r12)
	je	.L187
.L188:
	movq	112(%rbx), %rcx
	movq	%rbx, %rdi
	movq	%rdx, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	EVP_CIPHER_CTX_reset
	movq	-56(%rbp), %rcx
	movl	%r15d, 16(%rbx)
	movq	-64(%rbp), %rdx
	movq	%rcx, 112(%rbx)
.L190:
	testq	%rdx, %rdx
	je	.L191
.L261:
	movq	%rdx, %rdi
	movq	%rdx, -56(%rbp)
	call	ENGINE_init@PLT
	movq	-56(%rbp), %rdx
	testl	%eax, %eax
	je	.L253
.L192:
	movl	(%r12), %esi
	movq	%rdx, %rdi
	movq	%rdx, -56(%rbp)
	call	ENGINE_get_cipher@PLT
	movq	-56(%rbp), %rdx
	movl	$112, %r8d
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L249
	movslq	48(%r12), %rdi
	movq	%rdx, 8(%rbx)
	movq	%r12, (%rbx)
	testl	%edi, %edi
	jne	.L254
.L197:
	movq	$0, 120(%rbx)
	movq	%r12, %rax
.L199:
	movl	8(%r12), %edx
	andq	$1, 112(%rbx)
	movl	%edx, 104(%rbx)
	testb	$64, 16(%rax)
	jne	.L255
.L187:
	movl	4(%rax), %eax
	leal	-8(%rax), %edx
	andl	$-9, %edx
	je	.L203
	cmpl	$1, %eax
	jne	.L256
.L203:
	testb	$1, 112(%rbx)
	jne	.L204
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpq	$65538, %rax
	je	.L257
.L204:
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	testb	$16, %al
	je	.L258
.L206:
	movq	(%rbx), %rax
	testq	%r13, %r13
	je	.L259
.L214:
	movl	%r15d, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	*24(%rax)
	testl	%eax, %eax
	je	.L182
	movq	(%rbx), %rax
.L215:
	movl	4(%rax), %eax
	movl	$0, 20(%rbx)
	movl	$0, 128(%rbx)
	subl	$1, %eax
	movl	%eax, 132(%rbx)
	movl	$1, %eax
.L182:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L186:
	.cfi_restore_state
	testq	%r12, %r12
	jne	.L190
.L217:
	movl	$148, %r8d
	movl	$131, %edx
	movl	$123, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L252:
	cmpq	$0, 8(%rbx)
	movl	16(%rdi), %r15d
	movq	(%rbx), %rax
	jne	.L260
.L185:
	testq	%r12, %r12
	je	.L189
	testq	%rax, %rax
	jne	.L188
	testq	%rdx, %rdx
	jne	.L261
.L191:
	movl	(%r12), %edi
	call	ENGINE_get_cipher_engine@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L192
	movslq	48(%r12), %rdi
	movq	$0, 8(%rbx)
	movq	%r12, (%rbx)
	testl	%edi, %edi
	je	.L197
.L254:
	movl	$128, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_zalloc@PLT
	movq	%rax, 120(%rbx)
	testq	%rax, %rax
	je	.L262
	movq	(%rbx), %rax
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L189:
	testq	%rax, %rax
	jne	.L187
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L258:
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpq	$4, %rax
	ja	.L207
	cmpq	$2, %rax
	ja	.L208
	jne	.L206
.L209:
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_iv_length@PLT
	cmpl	$16, %eax
	jg	.L263
	leaq	24(%rbx), %r12
	testq	%r14, %r14
	je	.L212
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_iv_length@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movslq	%eax, %rdx
	call	memcpy@PLT
.L212:
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_iv_length@PLT
	leaq	40(%rbx), %rdi
	movq	%r12, %rsi
	movslq	%eax, %rdx
	call	memcpy@PLT
	movq	(%rbx), %rax
	testq	%r13, %r13
	jne	.L214
	.p2align 4,,10
	.p2align 3
.L259:
	testb	$32, 16(%rax)
	je	.L215
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L207:
	cmpq	$5, %rax
	jne	.L250
	movl	$0, 88(%rbx)
	testq	%r14, %r14
	je	.L206
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_iv_length@PLT
	leaq	40(%rbx), %rdi
	movq	%r14, %rsi
	movslq	%eax, %rdx
	call	memcpy@PLT
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L264:
	movl	$634, %r8d
	movl	$132, %edx
	movl	$124, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L201:
	movq	$0, (%rbx)
	movl	$143, %r8d
.L249:
	movl	$134, %edx
	movl	$123, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L250:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	.cfi_restore_state
	movl	$0, 88(%rbx)
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L255:
	movq	72(%rax), %rax
	testq	%rax, %rax
	je	.L264
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	*%rax
	cmpl	$-1, %eax
	je	.L265
	testl	%eax, %eax
	je	.L201
	movq	(%rbx), %rax
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L253:
	movl	$97, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$134, %edx
	movl	%eax, -56(%rbp)
	movl	$123, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	movl	-56(%rbp), %eax
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L257:
	movl	$161, %r8d
	movl	$170, %edx
	movl	$123, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L262:
	movl	$131, %r8d
	movl	$65, %edx
	movl	$123, %esi
	movq	$0, (%rbx)
	leaq	.LC0(%rip), %rcx
	movl	$6, %edi
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L265:
	movl	$640, %r8d
	movl	$133, %edx
	movl	$124, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L201
.L256:
	movl	$155, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	OPENSSL_die@PLT
.L263:
	movl	$180, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE857:
	.size	EVP_CipherInit_ex, .-EVP_CipherInit_ex
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"assertion failed: b <= sizeof(ctx->final)"
	.text
	.p2align 4
	.globl	EVP_CipherUpdate
	.type	EVP_CipherUpdate, @function
EVP_CipherUpdate:
.LFB858:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$40, %rsp
	movl	16(%rdi), %r15d
	testl	%r15d, %r15d
	jne	.L332
	movq	(%rdi), %rax
	movl	$8192, %esi
	movq	%rcx, -64(%rbp)
	movl	4(%rax), %eax
	movl	%eax, -52(%rbp)
	call	EVP_CIPHER_CTX_test_flags@PLT
	movq	-64(%rbp), %r10
	movl	%ebx, %edx
	testl	%eax, %eax
	jne	.L333
.L268:
	testl	%ebx, %ebx
	js	.L269
	movq	(%r12), %r11
	je	.L334
.L270:
	testb	$16, 18(%r11)
	je	.L272
	cmpl	$1, -52(%rbp)
	je	.L335
.L273:
	movslq	%ebx, %rcx
	movq	%r10, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*32(%r11)
	testl	%eax, %eax
	js	.L336
	movl	%eax, (%r14)
	movl	$1, %r15d
.L266:
	addq	$40, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L332:
	.cfi_restore_state
	addq	$40, %rsp
	movl	%ebx, %r8d
	movq	%r14, %rdx
	movq	%r13, %rsi
	popq	%rbx
	movq	%r12, %rdi
	movq	%r10, %rcx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	evp_EncryptDecryptUpdate
	.p2align 4,,10
	.p2align 3
.L333:
	.cfi_restore_state
	movl	%ebx, %eax
	leal	14(%rbx), %edx
	addl	$7, %eax
	cmovns	%eax, %edx
	sarl	$3, %edx
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L272:
	testb	$1, 113(%r12)
	jne	.L332
	cmpl	$32, -52(%rbp)
	ja	.L337
	movl	128(%r12), %r11d
	testl	%r11d, %r11d
	je	.L277
	cmpq	%r10, %r13
	je	.L278
	movslq	-52(%rbp), %rax
	movq	%r13, %rdx
	subq	%r10, %rdx
	movq	%rax, %rcx
	movq	%rax, %rdi
	negq	%rcx
	cmpq	%rcx, %rdx
	seta	%cl
	cmpq	%rax, %rdx
	setb	%al
	orb	%al, %cl
	je	.L279
	testl	%edi, %edi
	jle	.L279
.L278:
	movl	$502, %r8d
.L331:
	movl	$162, %edx
	movl	$166, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L334:
	movq	%r11, %rdi
	movq	%r10, -72(%rbp)
	movl	%edx, -64(%rbp)
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpq	$7, %rax
	je	.L338
.L269:
	xorl	%r15d, %r15d
	testl	%ebx, %ebx
	movl	$0, (%r14)
	sete	%r15b
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L279:
	movl	-52(%rbp), %r8d
	leaq	136(%r12), %rcx
	movq	%r8, %rax
	cmpl	$8, %r8d
	jnb	.L280
	testb	$4, %al
	jne	.L339
	movl	%r8d, %esi
	testl	%r8d, %r8d
	je	.L281
	movzbl	136(%r12), %eax
	andl	$2, %esi
	movb	%al, 0(%r13)
	jne	.L340
.L281:
	addq	%r8, %r13
	movl	$1, %r11d
.L277:
	movl	%ebx, %r8d
	movq	%r10, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%r11d, -64(%rbp)
	call	evp_EncryptDecryptUpdate
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L266
	cmpl	$1, -52(%rbp)
	movl	-64(%rbp), %r11d
	jbe	.L286
	movl	20(%r12), %eax
	testl	%eax, %eax
	je	.L341
.L286:
	movl	$0, 128(%r12)
.L293:
	movl	$1, %r15d
	testl	%r11d, %r11d
	je	.L266
	movl	-52(%rbp), %eax
	movl	%r11d, %r15d
	addl	%eax, (%r14)
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L338:
	movq	(%r12), %r11
	movl	-64(%rbp), %edx
	movq	-72(%rbp), %r10
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L336:
	movl	$0, (%r14)
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L335:
	movslq	%edx, %rcx
	movq	%r13, %rax
	movq	%rcx, %rsi
	subq	%r10, %rax
	negq	%rsi
	cmpq	%rsi, %rax
	seta	%sil
	cmpq	%rcx, %rax
	setb	%cl
	orb	%cl, %sil
	je	.L273
	testl	%edx, %edx
	movl	$480, %r8d
	setg	%dl
	testq	%rax, %rax
	setne	%al
	testb	%al, %dl
	je	.L273
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L280:
	movq	136(%r12), %rax
	leaq	8(%r13), %rdi
	andq	$-8, %rdi
	movq	%rax, 0(%r13)
	movq	-8(%rcx,%r8), %rax
	movq	%rax, -8(%r13,%r8)
	movq	%r13, %rax
	subq	%rdi, %rax
	subq	%rax, %rcx
	addl	-52(%rbp), %eax
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L281
	andl	$-8, %eax
	xorl	%edx, %edx
.L284:
	movl	%edx, %esi
	addl	$8, %edx
	movq	(%rcx,%rsi), %r9
	movq	%r9, (%rdi,%rsi)
	cmpl	%eax, %edx
	jb	.L284
	jmp	.L281
.L341:
	movl	-52(%rbp), %ebx
	subl	%ebx, (%r14)
	leaq	136(%r12), %rax
	movl	$1, 128(%r12)
	movslq	(%r14), %rdx
	addq	%rdx, %r13
	cmpl	$8, %ebx
	jnb	.L287
	testb	$4, %bl
	jne	.L342
	testl	%ebx, %ebx
	je	.L293
	movzbl	0(%r13), %edx
	andl	$2, %ebx
	movb	%dl, 136(%r12)
	je	.L293
	movl	-52(%rbp), %edx
	movzwl	-2(%r13,%rdx), %ecx
	movw	%cx, -2(%rax,%rdx)
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L287:
	movq	0(%r13), %rdx
	addq	$144, %r12
	movq	%rdx, -8(%r12)
	movl	-52(%rbp), %edx
	andq	$-8, %r12
	movq	-8(%r13,%rdx), %rcx
	movq	%rcx, -8(%rax,%rdx)
	subq	%r12, %rax
	subq	%rax, %r13
	addl	%edx, %eax
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L293
	andl	$-8, %eax
	xorl	%edx, %edx
.L291:
	movl	%edx, %ecx
	addl	$8, %edx
	movq	0(%r13,%rcx), %rsi
	movq	%rsi, (%r12,%rcx)
	cmpl	%eax, %edx
	jb	.L291
	jmp	.L293
.L339:
	movl	136(%r12), %eax
	movl	%eax, 0(%r13)
	movl	-4(%rcx,%r8), %eax
	movl	%eax, -4(%r13,%r8)
	jmp	.L281
.L340:
	movzwl	-2(%rcx,%r8), %eax
	movw	%ax, -2(%r13,%r8)
	jmp	.L281
.L342:
	movl	0(%r13), %edx
	movl	%edx, 136(%r12)
	movl	-52(%rbp), %edx
	movl	-4(%r13,%rdx), %ecx
	movl	%ecx, -4(%rax,%rdx)
	jmp	.L293
.L337:
	movl	$496, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE858:
	.size	EVP_CipherUpdate, .-EVP_CipherUpdate
	.section	.rodata.str1.8
	.align 8
.LC5:
	.string	"assertion failed: b <= sizeof(ctx->buf)"
	.text
	.p2align 4
	.globl	EVP_CipherFinal_ex
	.type	EVP_CipherFinal_ex, @function
EVP_CipherFinal_ex:
.LFB883:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movl	16(%rdi), %eax
	movq	16(%rbx), %rdx
	andl	$1048576, %edx
	testl	%eax, %eax
	je	.L344
	testq	%rdx, %rdx
	jne	.L431
	movl	4(%rbx), %r15d
	cmpl	$32, %r15d
	ja	.L432
	cmpl	$1, %r15d
	je	.L351
	movl	20(%rdi), %eax
	testb	$1, 113(%rdi)
	je	.L350
	testl	%eax, %eax
	jne	.L433
.L351:
	movl	$0, (%r14)
	movl	$1, %eax
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L344:
	movl	$0, (%r14)
	testq	%rdx, %rdx
	je	.L355
.L431:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*32(%rbx)
	testl	%eax, %eax
	js	.L428
	movl	%eax, (%r14)
	movl	$1, %eax
.L343:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L355:
	.cfi_restore_state
	movq	112(%rdi), %rdx
	andl	$256, %edx
	je	.L357
	movl	20(%rdi), %r8d
	testl	%r8d, %r8d
	jne	.L434
.L373:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L433:
	.cfi_restore_state
	movl	$431, %r8d
	movl	$138, %edx
	movl	$127, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L428:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L357:
	.cfi_restore_state
	movl	4(%rbx), %esi
	cmpl	$1, %esi
	jbe	.L373
	movl	20(%rdi), %edi
	testl	%edi, %edi
	jne	.L358
	movl	128(%r12), %ecx
	testl	%ecx, %ecx
	je	.L358
	cmpl	$32, %esi
	ja	.L435
	leal	-1(%rsi), %ecx
	movzbl	136(%r12,%rcx), %r9d
	movq	%rcx, %rax
	movl	%r9d, %edi
	testl	%r9d, %r9d
	je	.L374
	cmpl	%r9d, %esi
	movl	%esi, %r8d
	setl	%cl
	subl	%r9d, %r8d
	testb	%cl, %cl
	jne	.L374
	.p2align 4,,10
	.p2align 3
.L363:
	cmpl	%r8d, %eax
	je	.L436
	leal	-1(%rax), %ecx
	movq	%rcx, %rax
	cmpb	136(%r12,%rcx), %dil
	je	.L363
	movl	$588, %r8d
.L430:
	movl	$100, %edx
	movl	$101, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L350:
	movl	%r15d, %esi
	subl	%eax, %esi
	cmpl	%eax, %r15d
	jbe	.L354
	leal	-1(%rsi), %edx
	leaq	56(%rdi,%rax), %rdi
	movzbl	%sil, %esi
	addq	$1, %rdx
	call	memset@PLT
.L354:
	movl	%r15d, %ecx
	leaq	56(%r12), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*32(%rbx)
	testl	%eax, %eax
	je	.L343
	movl	%r15d, (%r14)
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L358:
	movl	$572, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$109, %edx
	movl	%eax, -52(%rbp)
	movl	$101, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	movl	-52(%rbp), %eax
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L434:
	movl	$563, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$138, %edx
	movl	%eax, -52(%rbp)
	movl	$101, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	movl	-52(%rbp), %eax
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L436:
	subl	%r9d, %esi
	testl	%esi, %esi
	jle	.L368
	leaq	136(%r12), %rdi
	leaq	16(%r13), %rcx
	cmpq	%rcx, %rdi
	leaq	152(%r12), %rcx
	leal	-1(%rsi), %eax
	setnb	%dil
	cmpq	%rcx, %r13
	setnb	%cl
	orb	%cl, %dil
	je	.L370
	cmpl	$14, %eax
	jbe	.L370
	movdqu	136(%r12), %xmm0
	movups	%xmm0, 0(%r13)
	cmpl	$16, %esi
	je	.L368
	movzbl	152(%r12), %eax
	movb	%al, 16(%r13)
	cmpl	$17, %esi
	je	.L368
	movzbl	153(%r12), %eax
	movb	%al, 17(%r13)
	cmpl	$18, %esi
	je	.L368
	movzbl	154(%r12), %eax
	movb	%al, 18(%r13)
	cmpl	$19, %esi
	je	.L368
	movzbl	155(%r12), %eax
	movb	%al, 19(%r13)
	cmpl	$20, %esi
	je	.L368
	movzbl	156(%r12), %eax
	movb	%al, 20(%r13)
	cmpl	$21, %esi
	je	.L368
	movzbl	157(%r12), %eax
	movb	%al, 21(%r13)
	cmpl	$22, %esi
	je	.L368
	movzbl	158(%r12), %eax
	movb	%al, 22(%r13)
	cmpl	$23, %esi
	je	.L368
	movzbl	159(%r12), %eax
	movb	%al, 23(%r13)
	cmpl	$24, %esi
	je	.L368
	movzbl	160(%r12), %eax
	movb	%al, 24(%r13)
	cmpl	$25, %esi
	je	.L368
	movzbl	161(%r12), %eax
	movb	%al, 25(%r13)
	cmpl	$26, %esi
	je	.L368
	movzbl	162(%r12), %eax
	movb	%al, 26(%r13)
	cmpl	$27, %esi
	je	.L368
	movzbl	163(%r12), %eax
	movb	%al, 27(%r13)
	cmpl	$28, %esi
	je	.L368
	movzbl	164(%r12), %eax
	movb	%al, 28(%r13)
	cmpl	$29, %esi
	je	.L368
	movzbl	165(%r12), %eax
	movb	%al, 29(%r13)
	cmpl	$31, %esi
	jne	.L368
	movzbl	166(%r12), %eax
	movb	%al, 30(%r13)
	.p2align 4,,10
	.p2align 3
.L368:
	movl	%esi, (%r14)
	movl	$1, %eax
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L370:
	movzbl	136(%r12,%rdx), %ecx
	movb	%cl, 0(%r13,%rdx)
	movq	%rdx, %rcx
	addq	$1, %rdx
	cmpq	%rax, %rcx
	jne	.L370
	jmp	.L368
.L374:
	movl	$583, %r8d
	jmp	.L430
.L432:
	movl	$423, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	call	OPENSSL_die@PLT
.L435:
	movl	$575, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE883:
	.size	EVP_CipherFinal_ex, .-EVP_CipherFinal_ex
	.p2align 4
	.globl	EVP_CipherFinal
	.type	EVP_CipherFinal, @function
EVP_CipherFinal:
.LFB860:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movl	16(%rdi), %eax
	movq	16(%rbx), %rdx
	andl	$1048576, %edx
	testl	%eax, %eax
	je	.L438
	testq	%rdx, %rdx
	jne	.L525
	movl	4(%rbx), %r15d
	cmpl	$32, %r15d
	ja	.L526
	cmpl	$1, %r15d
	je	.L445
	movl	20(%rdi), %eax
	testb	$1, 113(%rdi)
	je	.L444
	testl	%eax, %eax
	jne	.L527
.L445:
	movl	$0, (%r14)
	movl	$1, %eax
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L438:
	movl	$0, (%r14)
	testq	%rdx, %rdx
	je	.L449
.L525:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*32(%rbx)
	testl	%eax, %eax
	js	.L522
	movl	%eax, (%r14)
	movl	$1, %eax
.L437:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L449:
	.cfi_restore_state
	movq	112(%rdi), %rdx
	andl	$256, %edx
	je	.L451
	movl	20(%rdi), %r8d
	testl	%r8d, %r8d
	jne	.L528
.L467:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L527:
	.cfi_restore_state
	movl	$431, %r8d
	movl	$138, %edx
	movl	$127, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L522:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L451:
	.cfi_restore_state
	movl	4(%rbx), %esi
	cmpl	$1, %esi
	jbe	.L467
	movl	20(%rdi), %edi
	testl	%edi, %edi
	jne	.L452
	movl	128(%r12), %ecx
	testl	%ecx, %ecx
	je	.L452
	cmpl	$32, %esi
	ja	.L529
	leal	-1(%rsi), %ecx
	movzbl	136(%r12,%rcx), %r9d
	movq	%rcx, %rax
	movl	%r9d, %edi
	testl	%r9d, %r9d
	je	.L468
	cmpl	%r9d, %esi
	movl	%esi, %r8d
	setl	%cl
	subl	%r9d, %r8d
	testb	%cl, %cl
	jne	.L468
	.p2align 4,,10
	.p2align 3
.L457:
	cmpl	%r8d, %eax
	je	.L530
	leal	-1(%rax), %ecx
	movq	%rcx, %rax
	cmpb	136(%r12,%rcx), %dil
	je	.L457
	movl	$588, %r8d
.L524:
	movl	$100, %edx
	movl	$101, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L444:
	movl	%r15d, %esi
	subl	%eax, %esi
	cmpl	%eax, %r15d
	jbe	.L448
	leal	-1(%rsi), %edx
	leaq	56(%rdi,%rax), %rdi
	movzbl	%sil, %esi
	addq	$1, %rdx
	call	memset@PLT
.L448:
	movl	%r15d, %ecx
	leaq	56(%r12), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*32(%rbx)
	testl	%eax, %eax
	je	.L437
	movl	%r15d, (%r14)
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L452:
	movl	$572, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$109, %edx
	movl	%eax, -52(%rbp)
	movl	$101, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	movl	-52(%rbp), %eax
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L528:
	movl	$563, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$138, %edx
	movl	%eax, -52(%rbp)
	movl	$101, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	movl	-52(%rbp), %eax
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L530:
	subl	%r9d, %esi
	testl	%esi, %esi
	jle	.L462
	leaq	136(%r12), %rdi
	leaq	16(%r13), %rcx
	cmpq	%rcx, %rdi
	leaq	152(%r12), %rcx
	leal	-1(%rsi), %eax
	setnb	%dil
	cmpq	%rcx, %r13
	setnb	%cl
	orb	%cl, %dil
	je	.L464
	cmpl	$14, %eax
	jbe	.L464
	movdqu	136(%r12), %xmm0
	movups	%xmm0, 0(%r13)
	cmpl	$16, %esi
	je	.L462
	movzbl	152(%r12), %eax
	movb	%al, 16(%r13)
	cmpl	$17, %esi
	je	.L462
	movzbl	153(%r12), %eax
	movb	%al, 17(%r13)
	cmpl	$18, %esi
	je	.L462
	movzbl	154(%r12), %eax
	movb	%al, 18(%r13)
	cmpl	$19, %esi
	je	.L462
	movzbl	155(%r12), %eax
	movb	%al, 19(%r13)
	cmpl	$20, %esi
	je	.L462
	movzbl	156(%r12), %eax
	movb	%al, 20(%r13)
	cmpl	$21, %esi
	je	.L462
	movzbl	157(%r12), %eax
	movb	%al, 21(%r13)
	cmpl	$22, %esi
	je	.L462
	movzbl	158(%r12), %eax
	movb	%al, 22(%r13)
	cmpl	$23, %esi
	je	.L462
	movzbl	159(%r12), %eax
	movb	%al, 23(%r13)
	cmpl	$24, %esi
	je	.L462
	movzbl	160(%r12), %eax
	movb	%al, 24(%r13)
	cmpl	$25, %esi
	je	.L462
	movzbl	161(%r12), %eax
	movb	%al, 25(%r13)
	cmpl	$26, %esi
	je	.L462
	movzbl	162(%r12), %eax
	movb	%al, 26(%r13)
	cmpl	$27, %esi
	je	.L462
	movzbl	163(%r12), %eax
	movb	%al, 27(%r13)
	cmpl	$28, %esi
	je	.L462
	movzbl	164(%r12), %eax
	movb	%al, 28(%r13)
	cmpl	$29, %esi
	je	.L462
	movzbl	165(%r12), %eax
	movb	%al, 29(%r13)
	cmpl	$31, %esi
	jne	.L462
	movzbl	166(%r12), %eax
	movb	%al, 30(%r13)
	.p2align 4,,10
	.p2align 3
.L462:
	movl	%esi, (%r14)
	movl	$1, %eax
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L464:
	movzbl	136(%r12,%rdx), %ecx
	movb	%cl, 0(%r13,%rdx)
	movq	%rdx, %rcx
	addq	$1, %rdx
	cmpq	%rax, %rcx
	jne	.L464
	jmp	.L462
.L468:
	movl	$583, %r8d
	jmp	.L524
.L526:
	movl	$423, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	call	OPENSSL_die@PLT
.L529:
	movl	$575, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE860:
	.size	EVP_CipherFinal, .-EVP_CipherFinal
	.p2align 4
	.globl	EVP_EncryptInit
	.type	EVP_EncryptInit, @function
EVP_EncryptInit:
.LFB861:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L532
	movq	%rsi, %r12
	call	EVP_CIPHER_CTX_reset
	cmpq	$0, 8(%rbx)
	movl	$1, 16(%rbx)
	je	.L596
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L538
	movl	(%rax), %ecx
	cmpl	%ecx, (%r12)
	je	.L535
.L536:
	movq	112(%rbx), %r15
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_reset
	movl	$1, 16(%rbx)
	movq	%r15, 112(%rbx)
.L538:
	movl	(%r12), %edi
	call	ENGINE_get_cipher_engine@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L539
	movl	(%r12), %esi
	movq	%rax, %rdi
	call	ENGINE_get_cipher@PLT
	movl	$112, %r8d
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L594
	movslq	48(%r12), %rdi
	movq	%r15, 8(%rbx)
	movq	%r12, (%rbx)
	testl	%edi, %edi
	jne	.L597
.L542:
	movq	$0, 120(%rbx)
	movq	%r12, %rax
.L544:
	movl	8(%r12), %edx
	andq	$1, 112(%rbx)
	movl	%edx, 104(%rbx)
	testb	$64, 16(%rax)
	jne	.L598
.L535:
	movl	4(%rax), %eax
	leal	-8(%rax), %edx
	andl	$-9, %edx
	je	.L549
	cmpl	$1, %eax
	jne	.L599
.L549:
	testb	$1, 112(%rbx)
	jne	.L550
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpq	$65538, %rax
	je	.L600
.L550:
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	testb	$16, %al
	je	.L601
.L552:
	movq	(%rbx), %rax
	testq	%r13, %r13
	je	.L602
.L560:
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	*24(%rax)
	testl	%eax, %eax
	je	.L531
	movq	(%rbx), %rax
.L561:
	movl	4(%rax), %eax
	movl	$0, 20(%rbx)
	movl	$0, 128(%rbx)
	subl	$1, %eax
	movl	%eax, 132(%rbx)
	movl	$1, %eax
.L531:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L532:
	.cfi_restore_state
	cmpq	$0, 8(%rdi)
	movq	(%rdi), %rax
	movl	$1, 16(%rdi)
	je	.L563
	testq	%rax, %rax
	jne	.L535
.L548:
	movl	$148, %r8d
	movl	$131, %edx
	movl	$123, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L601:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpq	$4, %rax
	ja	.L553
	cmpq	$2, %rax
	ja	.L554
	jne	.L552
.L555:
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_iv_length@PLT
	cmpl	$16, %eax
	jg	.L603
	leaq	24(%rbx), %r12
	testq	%r14, %r14
	je	.L558
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_iv_length@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movslq	%eax, %rdx
	call	memcpy@PLT
.L558:
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_iv_length@PLT
	leaq	40(%rbx), %rdi
	movq	%r12, %rsi
	movslq	%eax, %rdx
	call	memcpy@PLT
	movq	(%rbx), %rax
	testq	%r13, %r13
	jne	.L560
	.p2align 4,,10
	.p2align 3
.L602:
	testb	$32, 16(%rax)
	je	.L561
	jmp	.L560
	.p2align 4,,10
	.p2align 3
.L553:
	cmpq	$5, %rax
	jne	.L595
	movl	$0, 88(%rbx)
	testq	%r14, %r14
	je	.L552
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_iv_length@PLT
	leaq	40(%rbx), %rdi
	movq	%r14, %rsi
	movslq	%eax, %rdx
	call	memcpy@PLT
	jmp	.L552
	.p2align 4,,10
	.p2align 3
.L539:
	movslq	48(%r12), %rdi
	movq	$0, 8(%rbx)
	movq	%r12, (%rbx)
	testl	%edi, %edi
	je	.L542
.L597:
	movl	$128, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_zalloc@PLT
	movq	%rax, 120(%rbx)
	testq	%rax, %rax
	je	.L604
	movq	(%rbx), %rax
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L563:
	testq	%rax, %rax
	jne	.L535
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L596:
	cmpq	$0, (%rbx)
	je	.L538
	jmp	.L536
	.p2align 4,,10
	.p2align 3
.L605:
	movl	$634, %r8d
	movl	$132, %edx
	movl	$124, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L546:
	movq	$0, (%rbx)
	movl	$143, %r8d
.L594:
	movl	$134, %edx
	movl	$123, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L595:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L598:
	.cfi_restore_state
	movq	72(%rax), %rax
	testq	%rax, %rax
	je	.L605
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	*%rax
	cmpl	$-1, %eax
	je	.L606
	testl	%eax, %eax
	je	.L546
	movq	(%rbx), %rax
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L554:
	movl	$0, 88(%rbx)
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L600:
	movl	$161, %r8d
	movl	$170, %edx
	movl	$123, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L604:
	movl	$131, %r8d
	movl	$65, %edx
	movl	$123, %esi
	movq	$0, (%rbx)
	leaq	.LC0(%rip), %rcx
	movl	$6, %edi
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L606:
	movl	$640, %r8d
	movl	$133, %edx
	movl	$124, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L546
.L599:
	movl	$155, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	OPENSSL_die@PLT
.L603:
	movl	$180, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE861:
	.size	EVP_EncryptInit, .-EVP_EncryptInit
	.p2align 4
	.globl	EVP_EncryptInit_ex
	.type	EVP_EncryptInit_ex, @function
EVP_EncryptInit_ex:
.LFB862:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpq	$0, 8(%rdi)
	movl	$1, 16(%rdi)
	movq	(%rdi), %rax
	je	.L608
	testq	%rax, %rax
	je	.L609
	testq	%rsi, %rsi
	je	.L610
	movl	(%rax), %ecx
	cmpl	%ecx, (%rsi)
	je	.L610
.L611:
	movq	112(%rbx), %rdx
	movq	%rbx, %rdi
	movq	%rdx, -56(%rbp)
	call	EVP_CIPHER_CTX_reset
	movq	-56(%rbp), %rdx
	movl	$1, 16(%rbx)
	movq	%rdx, 112(%rbx)
.L613:
	testq	%r15, %r15
	je	.L614
.L682:
	movq	%r15, %rdi
	call	ENGINE_init@PLT
	testl	%eax, %eax
	je	.L675
.L615:
	movl	(%r12), %esi
	movq	%r15, %rdi
	call	ENGINE_get_cipher@PLT
	movl	$112, %r8d
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L672
	movslq	48(%r12), %rdi
	movq	%r15, 8(%rbx)
	movq	%r12, (%rbx)
	testl	%edi, %edi
	jne	.L676
.L620:
	movq	$0, 120(%rbx)
	movq	%r12, %rax
.L622:
	movl	8(%r12), %edx
	andq	$1, 112(%rbx)
	movl	%edx, 104(%rbx)
	testb	$64, 16(%rax)
	jne	.L677
.L610:
	movl	4(%rax), %eax
	leal	-8(%rax), %edx
	andl	$-9, %edx
	je	.L626
	cmpl	$1, %eax
	jne	.L678
.L626:
	testb	$1, 112(%rbx)
	jne	.L627
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpq	$65538, %rax
	je	.L679
.L627:
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	testb	$16, %al
	je	.L680
.L629:
	movq	(%rbx), %rax
	testq	%r13, %r13
	je	.L681
.L637:
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	*24(%rax)
	testl	%eax, %eax
	je	.L607
	movq	(%rbx), %rax
.L638:
	movl	4(%rax), %eax
	movl	$0, 20(%rbx)
	movl	$0, 128(%rbx)
	subl	$1, %eax
	movl	%eax, 132(%rbx)
	movl	$1, %eax
.L607:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L609:
	.cfi_restore_state
	testq	%rsi, %rsi
	jne	.L613
.L640:
	movl	$148, %r8d
	movl	$131, %edx
	movl	$123, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L608:
	testq	%rsi, %rsi
	je	.L612
	testq	%rax, %rax
	jne	.L611
	testq	%r15, %r15
	jne	.L682
.L614:
	movl	(%r12), %edi
	call	ENGINE_get_cipher_engine@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L615
	movslq	48(%r12), %rdi
	movq	$0, 8(%rbx)
	movq	%r12, (%rbx)
	testl	%edi, %edi
	je	.L620
.L676:
	movl	$128, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_zalloc@PLT
	movq	%rax, 120(%rbx)
	testq	%rax, %rax
	je	.L683
	movq	(%rbx), %rax
	jmp	.L622
	.p2align 4,,10
	.p2align 3
.L612:
	testq	%rax, %rax
	jne	.L610
	jmp	.L640
	.p2align 4,,10
	.p2align 3
.L680:
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpq	$4, %rax
	ja	.L630
	cmpq	$2, %rax
	ja	.L631
	jne	.L629
.L632:
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_iv_length@PLT
	cmpl	$16, %eax
	jg	.L684
	leaq	24(%rbx), %r12
	testq	%r14, %r14
	je	.L635
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_iv_length@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movslq	%eax, %rdx
	call	memcpy@PLT
.L635:
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_iv_length@PLT
	leaq	40(%rbx), %rdi
	movq	%r12, %rsi
	movslq	%eax, %rdx
	call	memcpy@PLT
	movq	(%rbx), %rax
	testq	%r13, %r13
	jne	.L637
	.p2align 4,,10
	.p2align 3
.L681:
	testb	$32, 16(%rax)
	je	.L638
	jmp	.L637
	.p2align 4,,10
	.p2align 3
.L630:
	cmpq	$5, %rax
	jne	.L673
	movl	$0, 88(%rbx)
	testq	%r14, %r14
	je	.L629
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_iv_length@PLT
	leaq	40(%rbx), %rdi
	movq	%r14, %rsi
	movslq	%eax, %rdx
	call	memcpy@PLT
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L685:
	movl	$634, %r8d
	movl	$132, %edx
	movl	$124, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L624:
	movq	$0, (%rbx)
	movl	$143, %r8d
.L672:
	movl	$134, %edx
	movl	$123, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L673:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L631:
	.cfi_restore_state
	movl	$0, 88(%rbx)
	jmp	.L632
	.p2align 4,,10
	.p2align 3
.L677:
	movq	72(%rax), %rax
	testq	%rax, %rax
	je	.L685
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	*%rax
	cmpl	$-1, %eax
	je	.L686
	testl	%eax, %eax
	je	.L624
	movq	(%rbx), %rax
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L675:
	movl	$97, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$134, %edx
	movl	%eax, -56(%rbp)
	movl	$123, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	movl	-56(%rbp), %eax
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L679:
	movl	$161, %r8d
	movl	$170, %edx
	movl	$123, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L683:
	movl	$131, %r8d
	movl	$65, %edx
	movl	$123, %esi
	movq	$0, (%rbx)
	leaq	.LC0(%rip), %rcx
	movl	$6, %edi
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L686:
	movl	$640, %r8d
	movl	$133, %edx
	movl	$124, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L624
.L678:
	movl	$155, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	OPENSSL_die@PLT
.L684:
	movl	$180, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE862:
	.size	EVP_EncryptInit_ex, .-EVP_EncryptInit_ex
	.p2align 4
	.globl	EVP_DecryptInit
	.type	EVP_DecryptInit, @function
EVP_DecryptInit:
.LFB863:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L688
	movq	%rsi, %r12
	call	EVP_CIPHER_CTX_reset
	cmpq	$0, 8(%rbx)
	movl	$0, 16(%rbx)
	je	.L752
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L694
	movl	(%rax), %ecx
	cmpl	%ecx, (%r12)
	je	.L691
.L692:
	movq	112(%rbx), %r15
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_reset
	movl	$0, 16(%rbx)
	movq	%r15, 112(%rbx)
.L694:
	movl	(%r12), %edi
	call	ENGINE_get_cipher_engine@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L695
	movl	(%r12), %esi
	movq	%rax, %rdi
	call	ENGINE_get_cipher@PLT
	movl	$112, %r8d
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L750
	movslq	48(%r12), %rdi
	movq	%r15, 8(%rbx)
	movq	%r12, (%rbx)
	testl	%edi, %edi
	jne	.L753
.L698:
	movq	$0, 120(%rbx)
	movq	%r12, %rax
.L700:
	movl	8(%r12), %edx
	andq	$1, 112(%rbx)
	movl	%edx, 104(%rbx)
	testb	$64, 16(%rax)
	jne	.L754
.L691:
	movl	4(%rax), %eax
	leal	-8(%rax), %edx
	andl	$-9, %edx
	je	.L705
	cmpl	$1, %eax
	jne	.L755
.L705:
	testb	$1, 112(%rbx)
	jne	.L706
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpq	$65538, %rax
	je	.L756
.L706:
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	testb	$16, %al
	je	.L757
.L708:
	movq	(%rbx), %rax
	testq	%r13, %r13
	je	.L758
.L716:
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	*24(%rax)
	testl	%eax, %eax
	je	.L687
	movq	(%rbx), %rax
.L717:
	movl	4(%rax), %eax
	movl	$0, 20(%rbx)
	movl	$0, 128(%rbx)
	subl	$1, %eax
	movl	%eax, 132(%rbx)
	movl	$1, %eax
.L687:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L688:
	.cfi_restore_state
	cmpq	$0, 8(%rdi)
	movq	(%rdi), %rax
	movl	$0, 16(%rdi)
	je	.L719
	testq	%rax, %rax
	jne	.L691
.L704:
	movl	$148, %r8d
	movl	$131, %edx
	movl	$123, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L757:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpq	$4, %rax
	ja	.L709
	cmpq	$2, %rax
	ja	.L710
	jne	.L708
.L711:
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_iv_length@PLT
	cmpl	$16, %eax
	jg	.L759
	leaq	24(%rbx), %r12
	testq	%r14, %r14
	je	.L714
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_iv_length@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movslq	%eax, %rdx
	call	memcpy@PLT
.L714:
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_iv_length@PLT
	leaq	40(%rbx), %rdi
	movq	%r12, %rsi
	movslq	%eax, %rdx
	call	memcpy@PLT
	movq	(%rbx), %rax
	testq	%r13, %r13
	jne	.L716
	.p2align 4,,10
	.p2align 3
.L758:
	testb	$32, 16(%rax)
	je	.L717
	jmp	.L716
	.p2align 4,,10
	.p2align 3
.L709:
	cmpq	$5, %rax
	jne	.L751
	movl	$0, 88(%rbx)
	testq	%r14, %r14
	je	.L708
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_iv_length@PLT
	leaq	40(%rbx), %rdi
	movq	%r14, %rsi
	movslq	%eax, %rdx
	call	memcpy@PLT
	jmp	.L708
	.p2align 4,,10
	.p2align 3
.L695:
	movslq	48(%r12), %rdi
	movq	$0, 8(%rbx)
	movq	%r12, (%rbx)
	testl	%edi, %edi
	je	.L698
.L753:
	movl	$128, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_zalloc@PLT
	movq	%rax, 120(%rbx)
	testq	%rax, %rax
	je	.L760
	movq	(%rbx), %rax
	jmp	.L700
	.p2align 4,,10
	.p2align 3
.L719:
	testq	%rax, %rax
	jne	.L691
	jmp	.L704
	.p2align 4,,10
	.p2align 3
.L752:
	cmpq	$0, (%rbx)
	je	.L694
	jmp	.L692
	.p2align 4,,10
	.p2align 3
.L761:
	movl	$634, %r8d
	movl	$132, %edx
	movl	$124, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L702:
	movq	$0, (%rbx)
	movl	$143, %r8d
.L750:
	movl	$134, %edx
	movl	$123, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L751:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L754:
	.cfi_restore_state
	movq	72(%rax), %rax
	testq	%rax, %rax
	je	.L761
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	*%rax
	cmpl	$-1, %eax
	je	.L762
	testl	%eax, %eax
	je	.L702
	movq	(%rbx), %rax
	jmp	.L691
	.p2align 4,,10
	.p2align 3
.L710:
	movl	$0, 88(%rbx)
	jmp	.L711
	.p2align 4,,10
	.p2align 3
.L756:
	movl	$161, %r8d
	movl	$170, %edx
	movl	$123, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L687
	.p2align 4,,10
	.p2align 3
.L760:
	movl	$131, %r8d
	movl	$65, %edx
	movl	$123, %esi
	movq	$0, (%rbx)
	leaq	.LC0(%rip), %rcx
	movl	$6, %edi
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L687
	.p2align 4,,10
	.p2align 3
.L762:
	movl	$640, %r8d
	movl	$133, %edx
	movl	$124, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L702
.L755:
	movl	$155, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	OPENSSL_die@PLT
.L759:
	movl	$180, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE863:
	.size	EVP_DecryptInit, .-EVP_DecryptInit
	.p2align 4
	.globl	EVP_DecryptInit_ex
	.type	EVP_DecryptInit_ex, @function
EVP_DecryptInit_ex:
.LFB864:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpq	$0, 8(%rdi)
	movl	$0, 16(%rdi)
	movq	(%rdi), %rax
	je	.L764
	testq	%rax, %rax
	je	.L765
	testq	%rsi, %rsi
	je	.L766
	movl	(%rax), %ecx
	cmpl	%ecx, (%rsi)
	je	.L766
.L767:
	movq	112(%rbx), %rdx
	movq	%rbx, %rdi
	movq	%rdx, -56(%rbp)
	call	EVP_CIPHER_CTX_reset
	movq	-56(%rbp), %rdx
	movl	$0, 16(%rbx)
	movq	%rdx, 112(%rbx)
.L769:
	testq	%r15, %r15
	je	.L770
.L838:
	movq	%r15, %rdi
	call	ENGINE_init@PLT
	testl	%eax, %eax
	je	.L831
.L771:
	movl	(%r12), %esi
	movq	%r15, %rdi
	call	ENGINE_get_cipher@PLT
	movl	$112, %r8d
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L828
	movslq	48(%r12), %rdi
	movq	%r15, 8(%rbx)
	movq	%r12, (%rbx)
	testl	%edi, %edi
	jne	.L832
.L776:
	movq	$0, 120(%rbx)
	movq	%r12, %rax
.L778:
	movl	8(%r12), %edx
	andq	$1, 112(%rbx)
	movl	%edx, 104(%rbx)
	testb	$64, 16(%rax)
	jne	.L833
.L766:
	movl	4(%rax), %eax
	leal	-8(%rax), %edx
	andl	$-9, %edx
	je	.L782
	cmpl	$1, %eax
	jne	.L834
.L782:
	testb	$1, 112(%rbx)
	jne	.L783
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpq	$65538, %rax
	je	.L835
.L783:
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	testb	$16, %al
	je	.L836
.L785:
	movq	(%rbx), %rax
	testq	%r13, %r13
	je	.L837
.L793:
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	*24(%rax)
	testl	%eax, %eax
	je	.L763
	movq	(%rbx), %rax
.L794:
	movl	4(%rax), %eax
	movl	$0, 20(%rbx)
	movl	$0, 128(%rbx)
	subl	$1, %eax
	movl	%eax, 132(%rbx)
	movl	$1, %eax
.L763:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L765:
	.cfi_restore_state
	testq	%rsi, %rsi
	jne	.L769
.L796:
	movl	$148, %r8d
	movl	$131, %edx
	movl	$123, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L763
	.p2align 4,,10
	.p2align 3
.L764:
	testq	%rsi, %rsi
	je	.L768
	testq	%rax, %rax
	jne	.L767
	testq	%r15, %r15
	jne	.L838
.L770:
	movl	(%r12), %edi
	call	ENGINE_get_cipher_engine@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L771
	movslq	48(%r12), %rdi
	movq	$0, 8(%rbx)
	movq	%r12, (%rbx)
	testl	%edi, %edi
	je	.L776
.L832:
	movl	$128, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_zalloc@PLT
	movq	%rax, 120(%rbx)
	testq	%rax, %rax
	je	.L839
	movq	(%rbx), %rax
	jmp	.L778
	.p2align 4,,10
	.p2align 3
.L768:
	testq	%rax, %rax
	jne	.L766
	jmp	.L796
	.p2align 4,,10
	.p2align 3
.L836:
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpq	$4, %rax
	ja	.L786
	cmpq	$2, %rax
	ja	.L787
	jne	.L785
.L788:
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_iv_length@PLT
	cmpl	$16, %eax
	jg	.L840
	leaq	24(%rbx), %r12
	testq	%r14, %r14
	je	.L791
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_iv_length@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movslq	%eax, %rdx
	call	memcpy@PLT
.L791:
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_iv_length@PLT
	leaq	40(%rbx), %rdi
	movq	%r12, %rsi
	movslq	%eax, %rdx
	call	memcpy@PLT
	movq	(%rbx), %rax
	testq	%r13, %r13
	jne	.L793
	.p2align 4,,10
	.p2align 3
.L837:
	testb	$32, 16(%rax)
	je	.L794
	jmp	.L793
	.p2align 4,,10
	.p2align 3
.L786:
	cmpq	$5, %rax
	jne	.L829
	movl	$0, 88(%rbx)
	testq	%r14, %r14
	je	.L785
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_iv_length@PLT
	leaq	40(%rbx), %rdi
	movq	%r14, %rsi
	movslq	%eax, %rdx
	call	memcpy@PLT
	jmp	.L785
	.p2align 4,,10
	.p2align 3
.L841:
	movl	$634, %r8d
	movl	$132, %edx
	movl	$124, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L780:
	movq	$0, (%rbx)
	movl	$143, %r8d
.L828:
	movl	$134, %edx
	movl	$123, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L829:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L787:
	.cfi_restore_state
	movl	$0, 88(%rbx)
	jmp	.L788
	.p2align 4,,10
	.p2align 3
.L833:
	movq	72(%rax), %rax
	testq	%rax, %rax
	je	.L841
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	*%rax
	cmpl	$-1, %eax
	je	.L842
	testl	%eax, %eax
	je	.L780
	movq	(%rbx), %rax
	jmp	.L766
	.p2align 4,,10
	.p2align 3
.L831:
	movl	$97, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$134, %edx
	movl	%eax, -56(%rbp)
	movl	$123, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	movl	-56(%rbp), %eax
	jmp	.L763
	.p2align 4,,10
	.p2align 3
.L835:
	movl	$161, %r8d
	movl	$170, %edx
	movl	$123, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L763
	.p2align 4,,10
	.p2align 3
.L839:
	movl	$131, %r8d
	movl	$65, %edx
	movl	$123, %esi
	movq	$0, (%rbx)
	leaq	.LC0(%rip), %rcx
	movl	$6, %edi
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L763
	.p2align 4,,10
	.p2align 3
.L842:
	movl	$640, %r8d
	movl	$133, %edx
	movl	$124, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L780
.L834:
	movl	$155, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	OPENSSL_die@PLT
.L840:
	movl	$180, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE864:
	.size	EVP_DecryptInit_ex, .-EVP_DecryptInit_ex
	.p2align 4
	.globl	is_partially_overlapping
	.type	is_partially_overlapping, @function
is_partially_overlapping:
.LFB865:
	.cfi_startproc
	endbr64
	movslq	%edx, %rcx
	subq	%rsi, %rdi
	movq	%rcx, %rax
	negq	%rax
	cmpq	%rdi, %rax
	setb	%al
	cmpq	%rdi, %rcx
	seta	%cl
	orl	%ecx, %eax
	testl	%edx, %edx
	setg	%dl
	testq	%rdi, %rdi
	setne	%cl
	andl	%ecx, %edx
	andl	%edx, %eax
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE865:
	.size	is_partially_overlapping, .-is_partially_overlapping
	.p2align 4
	.globl	EVP_EncryptUpdate
	.type	EVP_EncryptUpdate, @function
EVP_EncryptUpdate:
.LFB867:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	16(%rdi), %r12d
	movq	%rdx, -56(%rbp)
	testl	%r12d, %r12d
	je	.L893
	movq	%rsi, %r15
	movl	$8192, %esi
	movl	%r8d, %ebx
	movq	%rdi, %r14
	movq	%rcx, %r13
	movl	%ebx, %r12d
	call	EVP_CIPHER_CTX_test_flags@PLT
	testl	%eax, %eax
	je	.L847
	movl	%ebx, %eax
	leal	14(%rbx), %r12d
	addl	$7, %eax
	cmovns	%eax, %r12d
	sarl	$3, %r12d
.L847:
	testl	%ebx, %ebx
	js	.L848
	movq	(%r14), %r10
	movl	4(%r10), %r8d
	je	.L894
.L849:
	testb	$16, 18(%r10)
	je	.L850
	cmpl	$1, %r8d
	je	.L895
.L851:
	movslq	%ebx, %rcx
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	*32(%r10)
	testl	%eax, %eax
	js	.L892
	movq	-56(%rbp), %rbx
	movl	$1, %r12d
	movl	%eax, (%rbx)
	jmp	.L844
	.p2align 4,,10
	.p2align 3
.L848:
	movq	-56(%rbp), %rax
	xorl	%r12d, %r12d
	testl	%ebx, %ebx
	sete	%r12b
	movl	$0, (%rax)
.L844:
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L893:
	.cfi_restore_state
	movl	$388, %r8d
	movl	$148, %edx
	movl	$167, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L844
	.p2align 4,,10
	.p2align 3
.L850:
	movslq	20(%r14), %rsi
	movslq	%r12d, %rcx
	movq	%rcx, %rdi
	leaq	(%r15,%rsi), %rax
	negq	%rdi
	movq	%rsi, %rdx
	subq	%r13, %rax
	cmpq	%rdi, %rax
	seta	%dil
	cmpq	%rcx, %rax
	setb	%cl
	orb	%cl, %dil
	je	.L853
	testl	%r12d, %r12d
	setg	%cl
	testq	%rax, %rax
	setne	%al
	testb	%al, %cl
	jne	.L896
.L853:
	testl	%edx, %edx
	jne	.L854
	testl	%ebx, 132(%r14)
	je	.L897
	cmpl	$32, %r8d
	jg	.L862
	movq	-56(%rbp), %rax
	movl	$0, (%rax)
.L859:
	leal	-1(%r8), %r12d
	andl	%ebx, %r12d
	subl	%r12d, %ebx
	testl	%ebx, %ebx
	jle	.L860
	movq	(%r14), %rax
	movslq	%ebx, %rcx
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	*32(%rax)
	testl	%eax, %eax
	je	.L892
	movq	-56(%rbp), %rax
	addl	%ebx, (%rax)
.L860:
	testl	%r12d, %r12d
	jne	.L898
.L861:
	movl	%r12d, 20(%r14)
	movl	$1, %r12d
	jmp	.L844
	.p2align 4,,10
	.p2align 3
.L895:
	movslq	%r12d, %rdx
	movq	%r15, %rax
	movq	%rdx, %rcx
	subq	%r13, %rax
	negq	%rcx
	cmpq	%rcx, %rax
	seta	%cl
	cmpq	%rdx, %rax
	setb	%dl
	orb	%dl, %cl
	je	.L851
	testl	%r12d, %r12d
	movl	$322, %r8d
	setg	%dl
	testq	%rax, %rax
	setne	%al
	testb	%al, %dl
	je	.L851
.L891:
	movl	$162, %edx
	movl	$219, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L892:
	xorl	%r12d, %r12d
	jmp	.L844
	.p2align 4,,10
	.p2align 3
.L894:
	movq	%r10, %rdi
	movl	%r8d, -64(%rbp)
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpq	$7, %rax
	jne	.L848
	movq	(%r14), %r10
	movl	-64(%rbp), %r8d
	jmp	.L849
	.p2align 4,,10
	.p2align 3
.L854:
	cmpl	$32, %r8d
	jg	.L862
	movl	%r8d, %r12d
	leaq	56(%r14,%rsi), %rdi
	subl	%edx, %r12d
	cmpl	%r12d, %ebx
	jl	.L899
	movslq	%r12d, %r10
	movq	%r13, %rsi
	movl	%r8d, -64(%rbp)
	movq	%r10, %rdx
	movq	%r10, -80(%rbp)
	call	memcpy@PLT
	movslq	-64(%rbp), %rcx
	movq	(%r14), %rax
	leaq	56(%r14), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	%ecx, -68(%rbp)
	movq	%rcx, -64(%rbp)
	call	*32(%rax)
	testl	%eax, %eax
	je	.L892
	movq	-80(%rbp), %r10
	movq	-64(%rbp), %rcx
	subl	%r12d, %ebx
	movq	-56(%rbp), %rax
	movl	-68(%rbp), %r8d
	addq	%r10, %r13
	addq	%rcx, %r15
	movl	%r8d, (%rax)
	jmp	.L859
	.p2align 4,,10
	.p2align 3
.L897:
	movslq	%ebx, %rcx
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	*32(%r10)
	movl	%eax, %r12d
	movq	-56(%rbp), %rax
	testl	%r12d, %r12d
	je	.L856
	movl	%ebx, (%rax)
	movl	$1, %r12d
	jmp	.L844
	.p2align 4,,10
	.p2align 3
.L899:
	movslq	%ebx, %rdx
	movq	%r13, %rsi
	movl	$1, %r12d
	call	memcpy@PLT
	movq	-56(%rbp), %rax
	addl	%ebx, 20(%r14)
	movl	$0, (%rax)
	jmp	.L844
	.p2align 4,,10
	.p2align 3
.L896:
	movl	$335, %r8d
	jmp	.L891
	.p2align 4,,10
	.p2align 3
.L898:
	movslq	%ebx, %rbx
	leaq	56(%r14), %rdi
	movslq	%r12d, %rdx
	leaq	0(%r13,%rbx), %rsi
	call	memcpy@PLT
	jmp	.L861
	.p2align 4,,10
	.p2align 3
.L856:
	movl	$0, (%rax)
	jmp	.L844
.L862:
	movl	$349, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE867:
	.size	EVP_EncryptUpdate, .-EVP_EncryptUpdate
	.p2align 4
	.globl	EVP_EncryptFinal
	.type	EVP_EncryptFinal, @function
EVP_EncryptFinal:
.LFB868:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	16(%rdi), %eax
	testl	%eax, %eax
	je	.L916
	movq	(%rdi), %rbx
	movq	%rdi, %r12
	movq	%rsi, %r13
	movq	%rdx, %r14
	testb	$16, 18(%rbx)
	je	.L903
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	call	*32(%rbx)
	movl	%eax, %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	js	.L900
	movl	%edx, (%r14)
	movl	$1, %eax
.L900:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L903:
	.cfi_restore_state
	movl	4(%rbx), %r15d
	cmpl	$32, %r15d
	ja	.L917
	cmpl	$1, %r15d
	je	.L907
	movl	20(%rdi), %eax
	testb	$1, 113(%rdi)
	jne	.L918
	movl	%r15d, %esi
	subl	%eax, %esi
	cmpl	%eax, %r15d
	jbe	.L910
	leal	-1(%rsi), %edx
	leaq	56(%rdi,%rax), %rdi
	movzbl	%sil, %esi
	addq	$1, %rdx
	call	memset@PLT
.L910:
	movl	%r15d, %ecx
	leaq	56(%r12), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*32(%rbx)
	testl	%eax, %eax
	je	.L900
	movl	%r15d, (%r14)
	jmp	.L900
	.p2align 4,,10
	.p2align 3
.L916:
	movl	$409, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$148, %edx
	movl	%eax, -52(%rbp)
	movl	$127, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	movl	-52(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L918:
	.cfi_restore_state
	testl	%eax, %eax
	jne	.L919
.L907:
	movl	$0, (%r14)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L919:
	.cfi_restore_state
	movl	$431, %r8d
	movl	$138, %edx
	movl	$127, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L900
.L917:
	movl	$423, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE868:
	.size	EVP_EncryptFinal, .-EVP_EncryptFinal
	.p2align 4
	.globl	EVP_EncryptFinal_ex
	.type	EVP_EncryptFinal_ex, @function
EVP_EncryptFinal_ex:
.LFB869:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	16(%rdi), %eax
	testl	%eax, %eax
	je	.L936
	movq	(%rdi), %rbx
	movq	%rdi, %r12
	movq	%rsi, %r13
	movq	%rdx, %r14
	testb	$16, 18(%rbx)
	je	.L923
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	call	*32(%rbx)
	movl	%eax, %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	js	.L920
	movl	%edx, (%r14)
	movl	$1, %eax
.L920:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L923:
	.cfi_restore_state
	movl	4(%rbx), %r15d
	cmpl	$32, %r15d
	ja	.L937
	cmpl	$1, %r15d
	je	.L927
	movl	20(%rdi), %eax
	testb	$1, 113(%rdi)
	jne	.L938
	movl	%r15d, %esi
	subl	%eax, %esi
	cmpl	%eax, %r15d
	jbe	.L930
	leal	-1(%rsi), %edx
	leaq	56(%rdi,%rax), %rdi
	movzbl	%sil, %esi
	addq	$1, %rdx
	call	memset@PLT
.L930:
	movl	%r15d, %ecx
	leaq	56(%r12), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*32(%rbx)
	testl	%eax, %eax
	je	.L920
	movl	%r15d, (%r14)
	jmp	.L920
	.p2align 4,,10
	.p2align 3
.L936:
	movl	$409, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$148, %edx
	movl	%eax, -52(%rbp)
	movl	$127, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	movl	-52(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L938:
	.cfi_restore_state
	testl	%eax, %eax
	jne	.L939
.L927:
	movl	$0, (%r14)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L939:
	.cfi_restore_state
	movl	$431, %r8d
	movl	$138, %edx
	movl	$127, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L920
.L937:
	movl	$423, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE869:
	.size	EVP_EncryptFinal_ex, .-EVP_EncryptFinal_ex
	.p2align 4
	.globl	EVP_DecryptUpdate
	.type	EVP_DecryptUpdate, @function
EVP_DecryptUpdate:
.LFB870:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	16(%rdi), %r15d
	movq	%rdx, -56(%rbp)
	testl	%r15d, %r15d
	jne	.L1006
	movq	(%rdi), %rax
	movq	%rsi, %r14
	movl	$8192, %esi
	movl	%r8d, %ebx
	movq	%rcx, -72(%rbp)
	movq	%rdi, %r12
	movl	%ebx, %r13d
	movl	4(%rax), %eax
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_test_flags@PLT
	movq	-72(%rbp), %rdx
	testl	%eax, %eax
	je	.L943
	movl	%ebx, %eax
	leal	14(%rbx), %r13d
	addl	$7, %eax
	cmovns	%eax, %r13d
	sarl	$3, %r13d
.L943:
	testl	%ebx, %ebx
	js	.L944
	movq	(%r12), %r11
	je	.L1007
.L945:
	testb	$16, 18(%r11)
	je	.L946
	cmpl	$1, -60(%rbp)
	je	.L1008
.L947:
	movslq	%ebx, %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*32(%r11)
	testl	%eax, %eax
	js	.L1009
	movq	-56(%rbp), %rbx
	movl	$1, %r15d
	movl	%eax, (%rbx)
	jmp	.L940
	.p2align 4,,10
	.p2align 3
.L944:
	movq	-56(%rbp), %rax
	xorl	%r15d, %r15d
	testl	%ebx, %ebx
	sete	%r15b
	movl	$0, (%rax)
.L940:
	addq	$40, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1006:
	.cfi_restore_state
	movl	$458, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$148, %edx
	xorl	%r15d, %r15d
	movl	$166, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	jmp	.L940
	.p2align 4,,10
	.p2align 3
.L946:
	testb	$1, 113(%r12)
	jne	.L1010
	cmpl	$32, -60(%rbp)
	ja	.L1011
	movl	128(%r12), %r13d
	testl	%r13d, %r13d
	je	.L951
	cmpq	%rdx, %r14
	je	.L952
	movslq	-60(%rbp), %rax
	movq	%r14, %rcx
	subq	%rdx, %rcx
	movq	%rax, %rsi
	movq	%rax, %rdi
	negq	%rsi
	cmpq	%rsi, %rcx
	seta	%sil
	cmpq	%rax, %rcx
	setb	%al
	orb	%al, %sil
	je	.L953
	testl	%edi, %edi
	jle	.L953
.L952:
	movl	$502, %r8d
.L1005:
	movl	$162, %edx
	movl	$166, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L940
	.p2align 4,,10
	.p2align 3
.L1008:
	movslq	%r13d, %rcx
	movq	%r14, %rax
	movq	%rcx, %rsi
	subq	%rdx, %rax
	negq	%rsi
	cmpq	%rsi, %rax
	seta	%sil
	cmpq	%rcx, %rax
	setb	%cl
	orb	%cl, %sil
	je	.L947
	testl	%r13d, %r13d
	movl	$480, %r8d
	setg	%cl
	testq	%rax, %rax
	setne	%al
	testb	%al, %cl
	je	.L947
	jmp	.L1005
	.p2align 4,,10
	.p2align 3
.L1007:
	movq	%r11, %rdi
	movq	%rdx, -72(%rbp)
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpq	$7, %rax
	jne	.L944
	movq	(%r12), %r11
	movq	-72(%rbp), %rdx
	jmp	.L945
	.p2align 4,,10
	.p2align 3
.L953:
	movl	-60(%rbp), %r9d
	leaq	136(%r12), %rsi
	movq	%r9, %rax
	cmpl	$8, %r9d
	jnb	.L954
	testb	$4, %al
	jne	.L1012
	movl	%r9d, %edi
	testl	%r9d, %r9d
	je	.L955
	movzbl	136(%r12), %eax
	andl	$2, %edi
	movb	%al, (%r14)
	jne	.L1013
.L955:
	addq	%r9, %r14
	movl	$1, %r13d
.L951:
	movq	%rdx, %rcx
	movq	-56(%rbp), %rdx
	movl	%ebx, %r8d
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	evp_EncryptDecryptUpdate
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L940
	cmpl	$1, -60(%rbp)
	jbe	.L960
	movl	20(%r12), %eax
	testl	%eax, %eax
	jne	.L960
	movq	-56(%rbp), %rcx
	movl	-60(%rbp), %ebx
	leaq	136(%r12), %rax
	subl	%ebx, (%rcx)
	movl	$1, 128(%r12)
	movslq	(%rcx), %rdx
	leaq	(%r14,%rdx), %r10
	cmpl	$8, %ebx
	jnb	.L961
	testb	$4, %bl
	jne	.L1014
	testl	%ebx, %ebx
	je	.L967
	movzbl	(%r10), %edx
	andl	$2, %ebx
	movb	%dl, 136(%r12)
	je	.L967
	movl	-60(%rbp), %edx
	movzwl	-2(%r10,%rdx), %ecx
	movw	%cx, -2(%rax,%rdx)
	jmp	.L967
	.p2align 4,,10
	.p2align 3
.L960:
	movl	$0, 128(%r12)
.L967:
	movl	$1, %r15d
	testl	%r13d, %r13d
	je	.L940
	movq	-56(%rbp), %rax
	movl	-60(%rbp), %ebx
	movl	%r13d, %r15d
	addl	%ebx, (%rax)
	jmp	.L940
	.p2align 4,,10
	.p2align 3
.L1009:
	movq	-56(%rbp), %rax
	movl	$0, (%rax)
	jmp	.L940
	.p2align 4,,10
	.p2align 3
.L1010:
	movq	%rdx, %rcx
	movq	-56(%rbp), %rdx
	addq	$40, %rsp
	movl	%ebx, %r8d
	movq	%r14, %rsi
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	evp_EncryptDecryptUpdate
	.p2align 4,,10
	.p2align 3
.L954:
	.cfi_restore_state
	movq	136(%r12), %rax
	leaq	8(%r14), %r8
	andq	$-8, %r8
	movq	%rax, (%r14)
	movq	-8(%rsi,%r9), %rax
	movq	%rax, -8(%r14,%r9)
	movq	%r14, %rax
	subq	%r8, %rax
	subq	%rax, %rsi
	addl	-60(%rbp), %eax
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L955
	andl	$-8, %eax
	xorl	%ecx, %ecx
.L958:
	movl	%ecx, %edi
	addl	$8, %ecx
	movq	(%rsi,%rdi), %r11
	movq	%r11, (%r8,%rdi)
	cmpl	%eax, %ecx
	jb	.L958
	jmp	.L955
.L961:
	movq	(%r10), %rdx
	addq	$144, %r12
	movq	%rdx, -8(%r12)
	movl	-60(%rbp), %edx
	andq	$-8, %r12
	movq	-8(%r10,%rdx), %rcx
	movq	%rcx, -8(%rax,%rdx)
	subq	%r12, %rax
	subq	%rax, %r10
	addl	%edx, %eax
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L967
	andl	$-8, %eax
	xorl	%edx, %edx
.L965:
	movl	%edx, %ecx
	addl	$8, %edx
	movq	(%r10,%rcx), %rsi
	movq	%rsi, (%r12,%rcx)
	cmpl	%eax, %edx
	jb	.L965
	jmp	.L967
.L1012:
	movl	136(%r12), %eax
	movl	%eax, (%r14)
	movl	-4(%rsi,%r9), %eax
	movl	%eax, -4(%r14,%r9)
	jmp	.L955
.L1013:
	movzwl	-2(%rsi,%r9), %eax
	movw	%ax, -2(%r14,%r9)
	jmp	.L955
.L1014:
	movl	(%r10), %edx
	movl	%edx, 136(%r12)
	movl	-60(%rbp), %edx
	movl	-4(%r10,%rdx), %ecx
	movl	%ecx, -4(%rax,%rdx)
	jmp	.L967
.L1011:
	movl	$496, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE870:
	.size	EVP_DecryptUpdate, .-EVP_DecryptUpdate
	.p2align 4
	.globl	EVP_DecryptFinal
	.type	EVP_DecryptFinal, @function
EVP_DecryptFinal:
.LFB871:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	16(%rdi), %r12d
	testl	%r12d, %r12d
	jne	.L1088
	movq	(%rdi), %rax
	movl	$0, (%rdx)
	movq	%rdx, %rbx
	testb	$16, 18(%rax)
	je	.L1018
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	*32(%rax)
	testl	%eax, %eax
	js	.L1015
	movl	%eax, (%rbx)
	movl	$1, %r12d
.L1015:
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1018:
	.cfi_restore_state
	movq	112(%rdi), %rdx
	andl	$256, %edx
	jne	.L1089
	movl	4(%rax), %r8d
	cmpl	$1, %r8d
	jbe	.L1035
	movl	20(%rdi), %ecx
	testl	%ecx, %ecx
	jne	.L1020
	movl	128(%rdi), %eax
	testl	%eax, %eax
	je	.L1020
	cmpl	$32, %r8d
	ja	.L1090
	leal	-1(%r8), %ecx
	movzbl	136(%rdi,%rcx), %r11d
	movq	%rcx, %rax
	movl	%r11d, %r9d
	testl	%r11d, %r11d
	je	.L1036
	cmpl	%r11d, %r8d
	movl	%r8d, %r10d
	setl	%cl
	subl	%r11d, %r10d
	testb	%cl, %cl
	jne	.L1036
	.p2align 4,,10
	.p2align 3
.L1025:
	cmpl	%r10d, %eax
	je	.L1091
	leal	-1(%rax), %ecx
	movq	%rcx, %rax
	cmpb	136(%rdi,%rcx), %r9b
	je	.L1025
	movl	$588, %r8d
.L1087:
	leaq	.LC0(%rip), %rcx
	movl	$100, %edx
	movl	$101, %esi
	xorl	%r12d, %r12d
	movl	$6, %edi
	call	ERR_put_error@PLT
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L1088:
	xorl	%r12d, %r12d
	movl	$545, %r8d
	movl	$148, %edx
	movl	$101, %esi
	leaq	.LC0(%rip), %rcx
	movl	$6, %edi
	call	ERR_put_error@PLT
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1089:
	.cfi_restore_state
	movl	20(%rdi), %esi
	testl	%esi, %esi
	jne	.L1092
.L1035:
	movl	$1, %r12d
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1092:
	.cfi_restore_state
	movl	$563, %r8d
	movl	$138, %edx
	movl	$101, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L1020:
	movl	$572, %r8d
	movl	$109, %edx
	movl	$101, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L1036:
	movl	$583, %r8d
	jmp	.L1087
	.p2align 4,,10
	.p2align 3
.L1091:
	subl	%r11d, %r8d
	testl	%r8d, %r8d
	jle	.L1030
	leaq	136(%rdi), %r9
	leaq	16(%rsi), %rcx
	cmpq	%rcx, %r9
	leaq	152(%rdi), %rcx
	leal	-1(%r8), %eax
	setnb	%r9b
	cmpq	%rcx, %rsi
	setnb	%cl
	orb	%cl, %r9b
	je	.L1032
	cmpl	$14, %eax
	jbe	.L1032
	movdqu	136(%rdi), %xmm0
	movups	%xmm0, (%rsi)
	cmpl	$16, %r8d
	je	.L1030
	movzbl	152(%rdi), %eax
	movb	%al, 16(%rsi)
	cmpl	$17, %r8d
	je	.L1030
	movzbl	153(%rdi), %eax
	movb	%al, 17(%rsi)
	cmpl	$18, %r8d
	je	.L1030
	movzbl	154(%rdi), %eax
	movb	%al, 18(%rsi)
	cmpl	$19, %r8d
	je	.L1030
	movzbl	155(%rdi), %eax
	movb	%al, 19(%rsi)
	cmpl	$20, %r8d
	je	.L1030
	movzbl	156(%rdi), %eax
	movb	%al, 20(%rsi)
	cmpl	$21, %r8d
	je	.L1030
	movzbl	157(%rdi), %eax
	movb	%al, 21(%rsi)
	cmpl	$22, %r8d
	je	.L1030
	movzbl	158(%rdi), %eax
	movb	%al, 22(%rsi)
	cmpl	$23, %r8d
	je	.L1030
	movzbl	159(%rdi), %eax
	movb	%al, 23(%rsi)
	cmpl	$24, %r8d
	je	.L1030
	movzbl	160(%rdi), %eax
	movb	%al, 24(%rsi)
	cmpl	$25, %r8d
	je	.L1030
	movzbl	161(%rdi), %eax
	movb	%al, 25(%rsi)
	cmpl	$26, %r8d
	je	.L1030
	movzbl	162(%rdi), %eax
	movb	%al, 26(%rsi)
	cmpl	$27, %r8d
	je	.L1030
	movzbl	163(%rdi), %eax
	movb	%al, 27(%rsi)
	cmpl	$28, %r8d
	je	.L1030
	movzbl	164(%rdi), %eax
	movb	%al, 28(%rsi)
	cmpl	$29, %r8d
	je	.L1030
	movzbl	165(%rdi), %eax
	movb	%al, 29(%rsi)
	cmpl	$31, %r8d
	jne	.L1030
	movzbl	166(%rdi), %eax
	movb	%al, 30(%rsi)
	.p2align 4,,10
	.p2align 3
.L1030:
	movl	%r8d, (%rbx)
	movl	$1, %r12d
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L1032:
	movzbl	136(%rdi,%rdx), %ecx
	movb	%cl, (%rsi,%rdx)
	movq	%rdx, %rcx
	addq	$1, %rdx
	cmpq	%rax, %rcx
	jne	.L1032
	jmp	.L1030
.L1090:
	movl	$575, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE871:
	.size	EVP_DecryptFinal, .-EVP_DecryptFinal
	.p2align 4
	.globl	EVP_DecryptFinal_ex
	.type	EVP_DecryptFinal_ex, @function
EVP_DecryptFinal_ex:
.LFB872:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	16(%rdi), %r12d
	testl	%r12d, %r12d
	jne	.L1166
	movq	(%rdi), %rax
	movl	$0, (%rdx)
	movq	%rdx, %rbx
	testb	$16, 18(%rax)
	je	.L1096
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	*32(%rax)
	testl	%eax, %eax
	js	.L1093
	movl	%eax, (%rbx)
	movl	$1, %r12d
.L1093:
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1096:
	.cfi_restore_state
	movq	112(%rdi), %rdx
	andl	$256, %edx
	jne	.L1167
	movl	4(%rax), %r8d
	cmpl	$1, %r8d
	jbe	.L1113
	movl	20(%rdi), %ecx
	testl	%ecx, %ecx
	jne	.L1098
	movl	128(%rdi), %eax
	testl	%eax, %eax
	je	.L1098
	cmpl	$32, %r8d
	ja	.L1168
	leal	-1(%r8), %ecx
	movzbl	136(%rdi,%rcx), %r11d
	movq	%rcx, %rax
	movl	%r11d, %r9d
	testl	%r11d, %r11d
	je	.L1114
	cmpl	%r11d, %r8d
	movl	%r8d, %r10d
	setl	%cl
	subl	%r11d, %r10d
	testb	%cl, %cl
	jne	.L1114
	.p2align 4,,10
	.p2align 3
.L1103:
	cmpl	%r10d, %eax
	je	.L1169
	leal	-1(%rax), %ecx
	movq	%rcx, %rax
	cmpb	136(%rdi,%rcx), %r9b
	je	.L1103
	movl	$588, %r8d
.L1165:
	leaq	.LC0(%rip), %rcx
	movl	$100, %edx
	movl	$101, %esi
	xorl	%r12d, %r12d
	movl	$6, %edi
	call	ERR_put_error@PLT
	jmp	.L1093
	.p2align 4,,10
	.p2align 3
.L1166:
	xorl	%r12d, %r12d
	movl	$545, %r8d
	movl	$148, %edx
	movl	$101, %esi
	leaq	.LC0(%rip), %rcx
	movl	$6, %edi
	call	ERR_put_error@PLT
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1167:
	.cfi_restore_state
	movl	20(%rdi), %esi
	testl	%esi, %esi
	jne	.L1170
.L1113:
	movl	$1, %r12d
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1170:
	.cfi_restore_state
	movl	$563, %r8d
	movl	$138, %edx
	movl	$101, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1093
	.p2align 4,,10
	.p2align 3
.L1098:
	movl	$572, %r8d
	movl	$109, %edx
	movl	$101, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1093
	.p2align 4,,10
	.p2align 3
.L1114:
	movl	$583, %r8d
	jmp	.L1165
	.p2align 4,,10
	.p2align 3
.L1169:
	subl	%r11d, %r8d
	testl	%r8d, %r8d
	jle	.L1108
	leaq	136(%rdi), %r9
	leaq	16(%rsi), %rcx
	cmpq	%rcx, %r9
	leaq	152(%rdi), %rcx
	leal	-1(%r8), %eax
	setnb	%r9b
	cmpq	%rcx, %rsi
	setnb	%cl
	orb	%cl, %r9b
	je	.L1110
	cmpl	$14, %eax
	jbe	.L1110
	movdqu	136(%rdi), %xmm0
	movups	%xmm0, (%rsi)
	cmpl	$16, %r8d
	je	.L1108
	movzbl	152(%rdi), %eax
	movb	%al, 16(%rsi)
	cmpl	$17, %r8d
	je	.L1108
	movzbl	153(%rdi), %eax
	movb	%al, 17(%rsi)
	cmpl	$18, %r8d
	je	.L1108
	movzbl	154(%rdi), %eax
	movb	%al, 18(%rsi)
	cmpl	$19, %r8d
	je	.L1108
	movzbl	155(%rdi), %eax
	movb	%al, 19(%rsi)
	cmpl	$20, %r8d
	je	.L1108
	movzbl	156(%rdi), %eax
	movb	%al, 20(%rsi)
	cmpl	$21, %r8d
	je	.L1108
	movzbl	157(%rdi), %eax
	movb	%al, 21(%rsi)
	cmpl	$22, %r8d
	je	.L1108
	movzbl	158(%rdi), %eax
	movb	%al, 22(%rsi)
	cmpl	$23, %r8d
	je	.L1108
	movzbl	159(%rdi), %eax
	movb	%al, 23(%rsi)
	cmpl	$24, %r8d
	je	.L1108
	movzbl	160(%rdi), %eax
	movb	%al, 24(%rsi)
	cmpl	$25, %r8d
	je	.L1108
	movzbl	161(%rdi), %eax
	movb	%al, 25(%rsi)
	cmpl	$26, %r8d
	je	.L1108
	movzbl	162(%rdi), %eax
	movb	%al, 26(%rsi)
	cmpl	$27, %r8d
	je	.L1108
	movzbl	163(%rdi), %eax
	movb	%al, 27(%rsi)
	cmpl	$28, %r8d
	je	.L1108
	movzbl	164(%rdi), %eax
	movb	%al, 28(%rsi)
	cmpl	$29, %r8d
	je	.L1108
	movzbl	165(%rdi), %eax
	movb	%al, 29(%rsi)
	cmpl	$31, %r8d
	jne	.L1108
	movzbl	166(%rdi), %eax
	movb	%al, 30(%rsi)
	.p2align 4,,10
	.p2align 3
.L1108:
	movl	%r8d, (%rbx)
	movl	$1, %r12d
	jmp	.L1093
	.p2align 4,,10
	.p2align 3
.L1110:
	movzbl	136(%rdi,%rdx), %ecx
	movb	%cl, (%rsi,%rdx)
	movq	%rdx, %rcx
	addq	$1, %rdx
	cmpq	%rax, %rcx
	jne	.L1110
	jmp	.L1108
.L1168:
	movl	$575, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE872:
	.size	EVP_DecryptFinal_ex, .-EVP_DecryptFinal_ex
	.p2align 4
	.globl	EVP_CIPHER_CTX_set_key_length
	.type	EVP_CIPHER_CTX_set_key_length, @function
EVP_CIPHER_CTX_set_key_length:
.LFB873:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	16(%rax), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testb	$-128, %cl
	jne	.L1181
	movl	$1, %eax
	cmpl	%esi, 104(%rdi)
	je	.L1171
	testl	%esi, %esi
	jle	.L1175
	andl	$8, %ecx
	jne	.L1182
.L1175:
	movl	$611, %r8d
	movl	$130, %edx
	movl	$122, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L1171:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1181:
	.cfi_restore_state
	movq	72(%rax), %rax
	testq	%rax, %rax
	je	.L1183
	xorl	%ecx, %ecx
	movl	%esi, %edx
	movl	$1, %esi
	call	*%rax
	cmpl	$-1, %eax
	jne	.L1171
	movl	$640, %r8d
	movl	$133, %edx
	movl	$124, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1182:
	.cfi_restore_state
	movl	%esi, 104(%rdi)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1183:
	.cfi_restore_state
	movl	$634, %r8d
	movl	$132, %edx
	movl	$124, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE873:
	.size	EVP_CIPHER_CTX_set_key_length, .-EVP_CIPHER_CTX_set_key_length
	.p2align 4
	.globl	EVP_CIPHER_CTX_set_padding
	.type	EVP_CIPHER_CTX_set_padding, @function
EVP_CIPHER_CTX_set_padding:
.LFB874:
	.cfi_startproc
	endbr64
	movq	112(%rdi), %rax
	movq	%rax, %rdx
	orb	$1, %ah
	andb	$-2, %dh
	testl	%esi, %esi
	cmovne	%rdx, %rax
	movq	%rax, 112(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE874:
	.size	EVP_CIPHER_CTX_set_padding, .-EVP_CIPHER_CTX_set_padding
	.p2align 4
	.globl	EVP_CIPHER_CTX_ctrl
	.type	EVP_CIPHER_CTX_ctrl, @function
EVP_CIPHER_CTX_ctrl:
.LFB875:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rax, %rax
	je	.L1192
	movq	72(%rax), %rax
	testq	%rax, %rax
	je	.L1193
	call	*%rax
	cmpl	$-1, %eax
	je	.L1194
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1194:
	.cfi_restore_state
	movl	$640, %r8d
	movl	$133, %edx
	movl	$124, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1192:
	.cfi_restore_state
	movl	$629, %r8d
	movl	$131, %edx
	movl	$124, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1193:
	.cfi_restore_state
	movl	$634, %r8d
	movl	$132, %edx
	movl	$124, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE875:
	.size	EVP_CIPHER_CTX_ctrl, .-EVP_CIPHER_CTX_ctrl
	.p2align 4
	.globl	EVP_CIPHER_CTX_rand_key
	.type	EVP_CIPHER_CTX_rand_key, @function
EVP_CIPHER_CTX_rand_key:
.LFB876:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testb	$2, 17(%rax)
	jne	.L1200
	movl	104(%rdi), %esi
	movq	%r8, %rdi
	call	RAND_priv_bytes@PLT
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
.L1195:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1200:
	.cfi_restore_state
	movq	72(%rax), %rax
	testq	%rax, %rax
	je	.L1201
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movl	$6, %esi
	call	*%rax
	cmpl	$-1, %eax
	jne	.L1195
	movl	$640, %r8d
	movl	$133, %edx
	movl	$124, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1201:
	.cfi_restore_state
	movl	$634, %r8d
	movl	$132, %edx
	movl	$124, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE876:
	.size	EVP_CIPHER_CTX_rand_key, .-EVP_CIPHER_CTX_rand_key
	.p2align 4
	.globl	EVP_CIPHER_CTX_copy
	.type	EVP_CIPHER_CTX_copy, @function
EVP_CIPHER_CTX_copy:
.LFB877:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	testq	%rsi, %rsi
	je	.L1203
	cmpq	$0, (%rsi)
	movq	%rsi, %r12
	je	.L1203
	movq	%rdi, %rbx
	movq	8(%rsi), %rdi
	testq	%rdi, %rdi
	je	.L1206
	call	ENGINE_init@PLT
	testl	%eax, %eax
	je	.L1222
.L1206:
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_reset
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rbx)
	movdqu	16(%r12), %xmm1
	movups	%xmm1, 16(%rbx)
	movdqu	32(%r12), %xmm2
	movups	%xmm2, 32(%rbx)
	movdqu	48(%r12), %xmm3
	movups	%xmm3, 48(%rbx)
	movdqu	64(%r12), %xmm4
	movups	%xmm4, 64(%rbx)
	movdqu	80(%r12), %xmm5
	movups	%xmm5, 80(%rbx)
	movdqu	96(%r12), %xmm6
	movups	%xmm6, 96(%rbx)
	movdqu	112(%r12), %xmm7
	movups	%xmm7, 112(%rbx)
	movdqu	128(%r12), %xmm0
	movups	%xmm0, 128(%rbx)
	movdqu	144(%r12), %xmm1
	movups	%xmm1, 144(%rbx)
	movq	160(%r12), %rax
	movq	%rax, 160(%rbx)
	cmpq	$0, 120(%r12)
	movq	(%r12), %rax
	je	.L1208
	movslq	48(%rax), %rdi
	testl	%edi, %edi
	jne	.L1223
.L1208:
	testb	$4, 17(%rax)
	jne	.L1210
.L1211:
	movl	$1, %eax
.L1202:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1210:
	.cfi_restore_state
	xorl	%edx, %edx
	movq	%rbx, %rcx
	movl	$8, %esi
	movq	%r12, %rdi
	call	*72(%rax)
	testl	%eax, %eax
	jne	.L1211
	movl	$686, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$134, %edx
	movl	%eax, -20(%rbp)
	movq	$0, (%rbx)
	movl	$163, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	movl	-20(%rbp), %eax
	jmp	.L1202
	.p2align 4,,10
	.p2align 3
.L1203:
	movl	$659, %r8d
	movl	$111, %edx
	movl	$163, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1223:
	.cfi_restore_state
	movl	$674, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, 120(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1224
	movq	(%r12), %rax
	movq	120(%r12), %rsi
	movslq	48(%rax), %rdx
	call	memcpy@PLT
	movq	(%r12), %rax
	testb	$4, 17(%rax)
	jne	.L1210
	jmp	.L1211
	.p2align 4,,10
	.p2align 3
.L1222:
	movl	$665, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$38, %edx
	movl	%eax, -20(%rbp)
	movl	$163, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	movl	-20(%rbp), %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1224:
	.cfi_restore_state
	movl	$677, %r8d
	movl	$65, %edx
	movl	$163, %esi
	movq	$0, (%rbx)
	leaq	.LC0(%rip), %rcx
	movl	$6, %edi
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L1202
	.cfi_endproc
.LFE877:
	.size	EVP_CIPHER_CTX_copy, .-EVP_CIPHER_CTX_copy
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
