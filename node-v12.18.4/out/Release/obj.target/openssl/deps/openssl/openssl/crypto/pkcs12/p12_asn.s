	.file	"p12_asn.c"
	.text
	.p2align 4
	.globl	d2i_PKCS12
	.type	d2i_PKCS12, @function
d2i_PKCS12:
.LFB827:
	.cfi_startproc
	endbr64
	leaq	PKCS12_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE827:
	.size	d2i_PKCS12, .-d2i_PKCS12
	.p2align 4
	.globl	i2d_PKCS12
	.type	i2d_PKCS12, @function
i2d_PKCS12:
.LFB828:
	.cfi_startproc
	endbr64
	leaq	PKCS12_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE828:
	.size	i2d_PKCS12, .-i2d_PKCS12
	.p2align 4
	.globl	PKCS12_new
	.type	PKCS12_new, @function
PKCS12_new:
.LFB829:
	.cfi_startproc
	endbr64
	leaq	PKCS12_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE829:
	.size	PKCS12_new, .-PKCS12_new
	.p2align 4
	.globl	PKCS12_free
	.type	PKCS12_free, @function
PKCS12_free:
.LFB830:
	.cfi_startproc
	endbr64
	leaq	PKCS12_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE830:
	.size	PKCS12_free, .-PKCS12_free
	.p2align 4
	.globl	d2i_PKCS12_MAC_DATA
	.type	d2i_PKCS12_MAC_DATA, @function
d2i_PKCS12_MAC_DATA:
.LFB831:
	.cfi_startproc
	endbr64
	leaq	PKCS12_MAC_DATA_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE831:
	.size	d2i_PKCS12_MAC_DATA, .-d2i_PKCS12_MAC_DATA
	.p2align 4
	.globl	i2d_PKCS12_MAC_DATA
	.type	i2d_PKCS12_MAC_DATA, @function
i2d_PKCS12_MAC_DATA:
.LFB832:
	.cfi_startproc
	endbr64
	leaq	PKCS12_MAC_DATA_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE832:
	.size	i2d_PKCS12_MAC_DATA, .-i2d_PKCS12_MAC_DATA
	.p2align 4
	.globl	PKCS12_MAC_DATA_new
	.type	PKCS12_MAC_DATA_new, @function
PKCS12_MAC_DATA_new:
.LFB833:
	.cfi_startproc
	endbr64
	leaq	PKCS12_MAC_DATA_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE833:
	.size	PKCS12_MAC_DATA_new, .-PKCS12_MAC_DATA_new
	.p2align 4
	.globl	PKCS12_MAC_DATA_free
	.type	PKCS12_MAC_DATA_free, @function
PKCS12_MAC_DATA_free:
.LFB834:
	.cfi_startproc
	endbr64
	leaq	PKCS12_MAC_DATA_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE834:
	.size	PKCS12_MAC_DATA_free, .-PKCS12_MAC_DATA_free
	.p2align 4
	.globl	d2i_PKCS12_BAGS
	.type	d2i_PKCS12_BAGS, @function
d2i_PKCS12_BAGS:
.LFB835:
	.cfi_startproc
	endbr64
	leaq	PKCS12_BAGS_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE835:
	.size	d2i_PKCS12_BAGS, .-d2i_PKCS12_BAGS
	.p2align 4
	.globl	i2d_PKCS12_BAGS
	.type	i2d_PKCS12_BAGS, @function
i2d_PKCS12_BAGS:
.LFB836:
	.cfi_startproc
	endbr64
	leaq	PKCS12_BAGS_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE836:
	.size	i2d_PKCS12_BAGS, .-i2d_PKCS12_BAGS
	.p2align 4
	.globl	PKCS12_BAGS_new
	.type	PKCS12_BAGS_new, @function
PKCS12_BAGS_new:
.LFB837:
	.cfi_startproc
	endbr64
	leaq	PKCS12_BAGS_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE837:
	.size	PKCS12_BAGS_new, .-PKCS12_BAGS_new
	.p2align 4
	.globl	PKCS12_BAGS_free
	.type	PKCS12_BAGS_free, @function
PKCS12_BAGS_free:
.LFB838:
	.cfi_startproc
	endbr64
	leaq	PKCS12_BAGS_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE838:
	.size	PKCS12_BAGS_free, .-PKCS12_BAGS_free
	.p2align 4
	.globl	d2i_PKCS12_SAFEBAG
	.type	d2i_PKCS12_SAFEBAG, @function
d2i_PKCS12_SAFEBAG:
.LFB839:
	.cfi_startproc
	endbr64
	leaq	PKCS12_SAFEBAG_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE839:
	.size	d2i_PKCS12_SAFEBAG, .-d2i_PKCS12_SAFEBAG
	.p2align 4
	.globl	i2d_PKCS12_SAFEBAG
	.type	i2d_PKCS12_SAFEBAG, @function
i2d_PKCS12_SAFEBAG:
.LFB840:
	.cfi_startproc
	endbr64
	leaq	PKCS12_SAFEBAG_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE840:
	.size	i2d_PKCS12_SAFEBAG, .-i2d_PKCS12_SAFEBAG
	.p2align 4
	.globl	PKCS12_SAFEBAG_new
	.type	PKCS12_SAFEBAG_new, @function
PKCS12_SAFEBAG_new:
.LFB841:
	.cfi_startproc
	endbr64
	leaq	PKCS12_SAFEBAG_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE841:
	.size	PKCS12_SAFEBAG_new, .-PKCS12_SAFEBAG_new
	.p2align 4
	.globl	PKCS12_SAFEBAG_free
	.type	PKCS12_SAFEBAG_free, @function
PKCS12_SAFEBAG_free:
.LFB842:
	.cfi_startproc
	endbr64
	leaq	PKCS12_SAFEBAG_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE842:
	.size	PKCS12_SAFEBAG_free, .-PKCS12_SAFEBAG_free
	.globl	PKCS12_AUTHSAFES_it
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"PKCS12_AUTHSAFES"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	PKCS12_AUTHSAFES_it, @object
	.size	PKCS12_AUTHSAFES_it, 56
PKCS12_AUTHSAFES_it:
	.byte	0
	.zero	7
	.quad	-1
	.quad	PKCS12_AUTHSAFES_item_tt
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC0
	.section	.data.rel.ro,"aw"
	.align 32
	.type	PKCS12_AUTHSAFES_item_tt, @object
	.size	PKCS12_AUTHSAFES_item_tt, 40
PKCS12_AUTHSAFES_item_tt:
	.quad	4
	.quad	0
	.quad	0
	.quad	.LC0
	.quad	PKCS7_it
	.globl	PKCS12_SAFEBAGS_it
	.section	.rodata.str1.1
.LC1:
	.string	"PKCS12_SAFEBAGS"
	.section	.data.rel.ro.local
	.align 32
	.type	PKCS12_SAFEBAGS_it, @object
	.size	PKCS12_SAFEBAGS_it, 56
PKCS12_SAFEBAGS_it:
	.byte	0
	.zero	7
	.quad	-1
	.quad	PKCS12_SAFEBAGS_item_tt
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC1
	.align 32
	.type	PKCS12_SAFEBAGS_item_tt, @object
	.size	PKCS12_SAFEBAGS_item_tt, 40
PKCS12_SAFEBAGS_item_tt:
	.quad	4
	.quad	0
	.quad	0
	.quad	.LC1
	.quad	PKCS12_SAFEBAG_it
	.globl	PKCS12_SAFEBAG_it
	.section	.rodata.str1.1
.LC2:
	.string	"PKCS12_SAFEBAG"
	.section	.data.rel.ro.local
	.align 32
	.type	PKCS12_SAFEBAG_it, @object
	.size	PKCS12_SAFEBAG_it, 56
PKCS12_SAFEBAG_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	PKCS12_SAFEBAG_seq_tt
	.quad	3
	.quad	0
	.quad	24
	.quad	.LC2
	.section	.rodata.str1.1
.LC3:
	.string	"type"
.LC4:
	.string	"attrib"
	.section	.data.rel.ro
	.align 32
	.type	PKCS12_SAFEBAG_seq_tt, @object
	.size	PKCS12_SAFEBAG_seq_tt, 120
PKCS12_SAFEBAG_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC3
	.quad	ASN1_OBJECT_it
	.quad	256
	.quad	-1
	.quad	0
	.quad	.LC2
	.quad	PKCS12_SAFEBAG_adb
	.quad	3
	.quad	0
	.quad	16
	.quad	.LC4
	.quad	X509_ATTRIBUTE_it
	.section	.data.rel.ro.local
	.align 32
	.type	PKCS12_SAFEBAG_adb, @object
	.size	PKCS12_SAFEBAG_adb, 56
PKCS12_SAFEBAG_adb:
	.quad	0
	.quad	0
	.quad	0
	.quad	PKCS12_SAFEBAG_adbtbl
	.quad	6
	.quad	safebag_default_tt
	.quad	0
	.section	.rodata.str1.1
.LC5:
	.string	"value.keybag"
.LC6:
	.string	"value.shkeybag"
.LC7:
	.string	"value.safes"
.LC8:
	.string	"value.bag"
	.section	.data.rel.ro
	.align 32
	.type	PKCS12_SAFEBAG_adbtbl, @object
	.size	PKCS12_SAFEBAG_adbtbl, 288
PKCS12_SAFEBAG_adbtbl:
	.quad	150
	.quad	144
	.quad	0
	.quad	8
	.quad	.LC5
	.quad	PKCS8_PRIV_KEY_INFO_it
	.quad	151
	.quad	144
	.quad	0
	.quad	8
	.quad	.LC6
	.quad	X509_SIG_it
	.quad	155
	.quad	148
	.quad	0
	.quad	8
	.quad	.LC7
	.quad	PKCS12_SAFEBAG_it
	.quad	152
	.quad	144
	.quad	0
	.quad	8
	.quad	.LC8
	.quad	PKCS12_BAGS_it
	.quad	153
	.quad	144
	.quad	0
	.quad	8
	.quad	.LC8
	.quad	PKCS12_BAGS_it
	.quad	154
	.quad	144
	.quad	0
	.quad	8
	.quad	.LC8
	.quad	PKCS12_BAGS_it
	.section	.rodata.str1.1
.LC9:
	.string	"value.other"
	.section	.data.rel.ro
	.align 32
	.type	safebag_default_tt, @object
	.size	safebag_default_tt, 40
safebag_default_tt:
	.quad	144
	.quad	0
	.quad	8
	.quad	.LC9
	.quad	ASN1_ANY_it
	.globl	PKCS12_BAGS_it
	.section	.rodata.str1.1
.LC10:
	.string	"PKCS12_BAGS"
	.section	.data.rel.ro.local
	.align 32
	.type	PKCS12_BAGS_it, @object
	.size	PKCS12_BAGS_it, 56
PKCS12_BAGS_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	PKCS12_BAGS_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC10
	.section	.data.rel.ro
	.align 32
	.type	PKCS12_BAGS_seq_tt, @object
	.size	PKCS12_BAGS_seq_tt, 80
PKCS12_BAGS_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC3
	.quad	ASN1_OBJECT_it
	.quad	256
	.quad	-1
	.quad	0
	.quad	.LC10
	.quad	PKCS12_BAGS_adb
	.section	.data.rel.ro.local
	.align 32
	.type	PKCS12_BAGS_adb, @object
	.size	PKCS12_BAGS_adb, 56
PKCS12_BAGS_adb:
	.quad	0
	.quad	0
	.quad	0
	.quad	PKCS12_BAGS_adbtbl
	.quad	3
	.quad	bag_default_tt
	.quad	0
	.section	.rodata.str1.1
.LC11:
	.string	"value.x509cert"
.LC12:
	.string	"value.x509crl"
.LC13:
	.string	"value.sdsicert"
	.section	.data.rel.ro
	.align 32
	.type	PKCS12_BAGS_adbtbl, @object
	.size	PKCS12_BAGS_adbtbl, 144
PKCS12_BAGS_adbtbl:
	.quad	158
	.quad	144
	.quad	0
	.quad	8
	.quad	.LC11
	.quad	ASN1_OCTET_STRING_it
	.quad	160
	.quad	144
	.quad	0
	.quad	8
	.quad	.LC12
	.quad	ASN1_OCTET_STRING_it
	.quad	159
	.quad	144
	.quad	0
	.quad	8
	.quad	.LC13
	.quad	ASN1_IA5STRING_it
	.align 32
	.type	bag_default_tt, @object
	.size	bag_default_tt, 40
bag_default_tt:
	.quad	144
	.quad	0
	.quad	8
	.quad	.LC9
	.quad	ASN1_ANY_it
	.globl	PKCS12_MAC_DATA_it
	.section	.rodata.str1.1
.LC14:
	.string	"PKCS12_MAC_DATA"
	.section	.data.rel.ro.local
	.align 32
	.type	PKCS12_MAC_DATA_it, @object
	.size	PKCS12_MAC_DATA_it, 56
PKCS12_MAC_DATA_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	PKCS12_MAC_DATA_seq_tt
	.quad	3
	.quad	0
	.quad	24
	.quad	.LC14
	.section	.rodata.str1.1
.LC15:
	.string	"dinfo"
.LC16:
	.string	"salt"
.LC17:
	.string	"iter"
	.section	.data.rel.ro
	.align 32
	.type	PKCS12_MAC_DATA_seq_tt, @object
	.size	PKCS12_MAC_DATA_seq_tt, 120
PKCS12_MAC_DATA_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC15
	.quad	X509_SIG_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC16
	.quad	ASN1_OCTET_STRING_it
	.quad	1
	.quad	0
	.quad	16
	.quad	.LC17
	.quad	ASN1_INTEGER_it
	.globl	PKCS12_it
	.section	.rodata.str1.1
.LC18:
	.string	"PKCS12"
	.section	.data.rel.ro.local
	.align 32
	.type	PKCS12_it, @object
	.size	PKCS12_it, 56
PKCS12_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	PKCS12_seq_tt
	.quad	3
	.quad	0
	.quad	24
	.quad	.LC18
	.section	.rodata.str1.1
.LC19:
	.string	"version"
.LC20:
	.string	"authsafes"
.LC21:
	.string	"mac"
	.section	.data.rel.ro
	.align 32
	.type	PKCS12_seq_tt, @object
	.size	PKCS12_seq_tt, 120
PKCS12_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC19
	.quad	ASN1_INTEGER_it
	.quad	0
	.quad	0
	.quad	16
	.quad	.LC20
	.quad	PKCS7_it
	.quad	1
	.quad	0
	.quad	8
	.quad	.LC21
	.quad	PKCS12_MAC_DATA_it
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
