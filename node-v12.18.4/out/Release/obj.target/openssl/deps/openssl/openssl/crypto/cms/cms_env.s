	.file	"cms_env.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/cms/cms_env.c"
	.text
	.p2align 4
	.globl	cms_get0_enveloped
	.type	cms_get0_enveloped, @function
cms_get0_enveloped:
.LFB1490:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$23, %eax
	jne	.L6
	movq	8(%rbx), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movl	$26, %r8d
	movl	$107, %edx
	movl	$131, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1490:
	.size	cms_get0_enveloped, .-cms_get0_enveloped
	.p2align 4
	.globl	cms_env_asn1_ctrl
	.type	cms_env_asn1_ctrl, @function
cms_env_asn1_ctrl:
.LFB1492:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	(%rdi), %eax
	movl	%esi, %ebx
	testl	%eax, %eax
	jne	.L8
	movq	8(%rdi), %rax
	movq	40(%rax), %rdi
.L9:
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L14
	movq	176(%rax), %rax
	testq	%rax, %rax
	je	.L14
	movslq	%ebx, %rdx
	movq	%r12, %rcx
	movl	$7, %esi
	call	*%rax
	cmpl	$-2, %eax
	je	.L27
	testl	%eax, %eax
	jle	.L28
.L14:
	movl	$1, %eax
.L7:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	cmpl	$1, %eax
	jne	.L26
	movq	8(%rdi), %rax
	movq	40(%rax), %rdi
	testq	%rdi, %rdi
	je	.L26
	call	EVP_PKEY_CTX_get0_pkey@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L9
	.p2align 4,,10
	.p2align 3
.L26:
	xorl	%eax, %eax
.L29:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movl	$75, %r8d
	movl	$111, %edx
	movl	$171, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L27:
	movl	$70, %r8d
	movl	$125, %edx
	movl	$171, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L7
	.cfi_endproc
.LFE1492:
	.size	cms_env_asn1_ctrl, .-cms_env_asn1_ctrl
	.p2align 4
	.globl	CMS_get0_RecipientInfos
	.type	CMS_get0_RecipientInfos, @function
CMS_get0_RecipientInfos:
.LFB1493:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$23, %eax
	jne	.L37
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L30
	movq	16(%rax), %rax
.L30:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	movl	$26, %r8d
	movl	$107, %edx
	movl	$131, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1493:
	.size	CMS_get0_RecipientInfos, .-CMS_get0_RecipientInfos
	.p2align 4
	.globl	CMS_RecipientInfo_type
	.type	CMS_RecipientInfo_type, @function
CMS_RecipientInfo_type:
.LFB1494:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	ret
	.cfi_endproc
.LFE1494:
	.size	CMS_RecipientInfo_type, .-CMS_RecipientInfo_type
	.p2align 4
	.globl	CMS_RecipientInfo_get0_pkey_ctx
	.type	CMS_RecipientInfo_get0_pkey_ctx, @function
CMS_RecipientInfo_get0_pkey_ctx:
.LFB1495:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	testl	%eax, %eax
	je	.L43
	xorl	%r8d, %r8d
	cmpl	$1, %eax
	je	.L44
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	movq	8(%rdi), %rax
	movq	40(%rax), %r8
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	movq	8(%rdi), %rax
	movq	48(%rax), %r8
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE1495:
	.size	CMS_RecipientInfo_get0_pkey_ctx, .-CMS_RecipientInfo_get0_pkey_ctx
	.p2align 4
	.globl	CMS_EnvelopedData_create
	.type	CMS_EnvelopedData_create, @function
CMS_EnvelopedData_create:
.LFB1496:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	call	CMS_ContentInfo_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L47
	cmpq	$0, 8(%rax)
	je	.L61
	movq	(%rax), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$23, %eax
	jne	.L62
	movq	8(%r12), %rax
.L50:
	testq	%rax, %rax
	je	.L47
	movq	24(%rax), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	cms_EncryptedContent_init@PLT
	testl	%eax, %eax
	je	.L47
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	movl	$38, %r8d
	movl	$65, %edx
	movl	$126, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L47:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	CMS_ContentInfo_free@PLT
	movl	$120, %r8d
	movl	$65, %edx
	leaq	.LC0(%rip), %rcx
	movl	$124, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	leaq	CMS_EnvelopedData_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L63
	movl	$0, (%rax)
	movq	24(%rax), %rbx
	movl	$21, %edi
	call	OBJ_nid2obj@PLT
	movq	(%r12), %rdi
	movq	%rax, (%rbx)
	call	ASN1_OBJECT_free@PLT
	movl	$23, %edi
	call	OBJ_nid2obj@PLT
	movq	%rax, (%r12)
	movq	8(%r12), %rax
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L62:
	movl	$26, %r8d
	movl	$107, %edx
	movl	$131, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L47
	.cfi_endproc
.LFE1496:
	.size	CMS_EnvelopedData_create, .-CMS_EnvelopedData_create
	.p2align 4
	.globl	CMS_add1_recipient_cert
	.type	CMS_add1_recipient_cert, @function
CMS_add1_recipient_cert:
.LFB1498:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	OBJ_obj2nid@PLT
	cmpl	$23, %eax
	jne	.L110
	movq	8(%r12), %rbx
	testq	%rbx, %rbx
	je	.L79
	leaq	CMS_RecipientInfo_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L76
	movq	%r13, %rdi
	call	X509_get0_pubkey@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L111
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L69
	movq	176(%rax), %rax
	testq	%rax, %rax
	je	.L69
	xorl	%edx, %edx
	leaq	-60(%rbp), %rcx
	movl	$8, %esi
	movq	%r14, %rdi
	call	*%rax
	testl	%eax, %eax
	jg	.L112
.L69:
	leaq	CMS_KeyTransRecipientInfo_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, 8(%r12)
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L66
	movl	%r15d, %edx
	movq	8(%rcx), %rdi
	movq	%r13, %rsi
	movq	%rcx, -72(%rbp)
	andl	$65536, %edx
	movl	$0, (%r12)
	cmpl	$1, %edx
	sbbl	%eax, %eax
	notl	%eax
	andl	$2, %eax
	cmpl	$1, %edx
	movl	%eax, (%rcx)
	sbbl	%edx, %edx
	addl	$1, %edx
	call	cms_set1_SignerIdentifier@PLT
	testl	%eax, %eax
	je	.L66
	movq	%r14, %xmm1
	movq	%r13, %xmm0
	movq	%r13, %rdi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -96(%rbp)
	call	X509_up_ref@PLT
	movq	%r14, %rdi
	call	EVP_PKEY_up_ref@PLT
	movdqa	-96(%rbp), %xmm0
	movq	-72(%rbp), %rcx
	andl	$262144, %r15d
	movups	%xmm0, 32(%rcx)
	je	.L74
	movq	%r14, %rdi
	xorl	%esi, %esi
	call	EVP_PKEY_CTX_new@PLT
	movq	-72(%rbp), %rcx
	movq	%rax, %rdi
	movq	%rax, 48(%rcx)
	testq	%rax, %rax
	je	.L66
	call	EVP_PKEY_encrypt_init@PLT
	testl	%eax, %eax
	jg	.L75
	.p2align 4,,10
	.p2align 3
.L66:
	movq	%r12, %rdi
	leaq	CMS_RecipientInfo_it(%rip), %rsi
	xorl	%r12d, %r12d
	call	ASN1_item_free@PLT
.L64:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L113
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	cms_env_asn1_ctrl
	testl	%eax, %eax
	je	.L66
.L75:
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L64
.L76:
	movl	$224, %r8d
	movl	$65, %edx
	movl	$101, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L112:
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	je	.L69
	cmpl	$1, %eax
	je	.L71
	movl	$212, %r8d
	movl	$125, %edx
	movl	$101, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L110:
	movl	$26, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$107, %edx
	xorl	%r12d, %r12d
	movl	$131, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L71:
	movl	%r15d, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	cms_RecipientInfo_kari_init@PLT
	testl	%eax, %eax
	jne	.L75
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L79:
	xorl	%r12d, %r12d
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L111:
	movl	$195, %r8d
	movl	$113, %edx
	movl	$101, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L66
.L113:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1498:
	.size	CMS_add1_recipient_cert, .-CMS_add1_recipient_cert
	.p2align 4
	.globl	CMS_RecipientInfo_ktri_get0_algs
	.type	CMS_RecipientInfo_ktri_get0_algs, @function
CMS_RecipientInfo_ktri_get0_algs:
.LFB1499:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	testl	%eax, %eax
	jne	.L131
	movq	8(%rdi), %rax
	testq	%rsi, %rsi
	je	.L117
	movq	40(%rax), %rdi
	movq	%rdi, (%rsi)
.L117:
	testq	%rdx, %rdx
	je	.L118
	movq	32(%rax), %rsi
	movq	%rsi, (%rdx)
.L118:
	movl	$1, %r8d
	testq	%rcx, %rcx
	je	.L128
	movq	16(%rax), %rax
	movq	%rax, (%rcx)
.L128:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L131:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$124, %edx
	movl	$142, %esi
	movl	$46, %edi
	movl	$237, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%r8d, %r8d
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE1499:
	.size	CMS_RecipientInfo_ktri_get0_algs, .-CMS_RecipientInfo_ktri_get0_algs
	.p2align 4
	.globl	CMS_RecipientInfo_ktri_get0_signer_id
	.type	CMS_RecipientInfo_ktri_get0_signer_id, @function
CMS_RecipientInfo_ktri_get0_signer_id:
.LFB1500:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	testl	%eax, %eax
	jne	.L136
	movq	8(%rdi), %rax
	movq	8(%rax), %rdi
	jmp	cms_SignerIdentifier_get0_signer_id@PLT
	.p2align 4,,10
	.p2align 3
.L136:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$124, %edx
	movl	$143, %esi
	movl	$46, %edi
	movl	$260, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1500:
	.size	CMS_RecipientInfo_ktri_get0_signer_id, .-CMS_RecipientInfo_ktri_get0_signer_id
	.p2align 4
	.globl	CMS_RecipientInfo_ktri_cert_cmp
	.type	CMS_RecipientInfo_ktri_cert_cmp, @function
CMS_RecipientInfo_ktri_cert_cmp:
.LFB1501:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	testl	%eax, %eax
	jne	.L143
	movq	8(%rdi), %rax
	movq	8(%rax), %rdi
	jmp	cms_SignerIdentifier_cert_cmp@PLT
	.p2align 4,,10
	.p2align 3
.L143:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$124, %edx
	movl	$139, %esi
	movl	$46, %edi
	movl	$272, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	movl	$-2, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1501:
	.size	CMS_RecipientInfo_ktri_cert_cmp, .-CMS_RecipientInfo_ktri_cert_cmp
	.p2align 4
	.globl	CMS_RecipientInfo_set0_pkey
	.type	CMS_RecipientInfo_set0_pkey, @function
CMS_RecipientInfo_set0_pkey:
.LFB1502:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	(%rdi), %eax
	testl	%eax, %eax
	jne	.L150
	movq	8(%rdi), %rax
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	40(%rax), %rdi
	call	EVP_PKEY_free@PLT
	movq	8(%rbx), %rax
	movq	%r12, 40(%rax)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	.cfi_restore_state
	movl	$282, %r8d
	movl	$124, %edx
	movl	$145, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1502:
	.size	CMS_RecipientInfo_set0_pkey, .-CMS_RecipientInfo_set0_pkey
	.p2align 4
	.globl	CMS_RecipientInfo_kekri_id_cmp
	.type	CMS_RecipientInfo_kekri_id_cmp, @function
CMS_RecipientInfo_kekri_id_cmp:
.LFB1505:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	cmpl	$2, (%rdi)
	jne	.L156
	movq	8(%rdi), %rax
	movq	%rsi, -24(%rbp)
	leaq	-32(%rbp), %rdi
	movl	$4, -28(%rbp)
	movq	8(%rax), %rax
	movq	$0, -16(%rbp)
	movl	%edx, -32(%rbp)
	movq	(%rax), %rsi
	call	ASN1_OCTET_STRING_cmp@PLT
.L151:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L157
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L156:
	.cfi_restore_state
	movl	$448, %r8d
	movl	$123, %edx
	movl	$138, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	jmp	.L151
.L157:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1505:
	.size	CMS_RecipientInfo_kekri_id_cmp, .-CMS_RecipientInfo_kekri_id_cmp
	.p2align 4
	.globl	CMS_add0_recipient_key
	.type	CMS_add0_recipient_key, @function
CMS_add0_recipient_key:
.LFB1507:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$40, %rsp
	movq	(%rdi), %rdi
	movq	%rdx, -56(%rbp)
	movq	%r8, -64(%rbp)
	movq	%r9, -72(%rbp)
	call	OBJ_obj2nid@PLT
	cmpl	$23, %eax
	jne	.L190
	movq	8(%r15), %r15
	testq	%r15, %r15
	je	.L171
	testl	%r12d, %r12d
	jne	.L161
	cmpq	$24, %rbx
	je	.L172
	cmpq	$32, %rbx
	je	.L173
	movl	$507, %r8d
	cmpq	$16, %rbx
	je	.L191
.L189:
	leaq	.LC0(%rip), %rcx
	movl	$118, %edx
	movl	$100, %esi
	xorl	%r14d, %r14d
	movl	$46, %edi
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L160:
	movq	%r14, %rdi
	leaq	CMS_RecipientInfo_it(%rip), %rsi
	xorl	%r14d, %r14d
	call	ASN1_item_free@PLT
.L158:
	addq	$40, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	leal	-788(%r12), %eax
	cmpl	$2, %eax
	ja	.L163
	movslq	%r12d, %rax
	leaq	-6288(,%rax,8), %rax
	cmpq	%rax, %rbx
	jne	.L192
.L162:
	leaq	CMS_RecipientInfo_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L165
	leaq	CMS_KEKRecipientInfo_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, 8(%r14)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L165
	cmpq	$0, 24(%rbp)
	movl	$2, (%r14)
	je	.L168
	movq	8(%rax), %rdx
	leaq	CMS_OtherKeyAttribute_it(%rip), %rdi
	movq	%rdx, -80(%rbp)
	call	ASN1_item_new@PLT
	movq	-80(%rbp), %rdx
	movq	%rax, 16(%rdx)
	movq	8(%r13), %rax
	cmpq	$0, 16(%rax)
	je	.L165
.L168:
	movq	16(%r15), %rdi
	movq	%r14, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L165
	movq	-56(%rbp), %rax
	movl	-72(%rbp), %edx
	movl	$4, 0(%r13)
	movq	%rbx, 40(%r13)
	movq	-64(%rbp), %rsi
	movq	%rax, 32(%r13)
	movq	8(%r13), %rax
	movq	(%rax), %rdi
	call	ASN1_STRING_set0@PLT
	movq	8(%r13), %rax
	movq	16(%rbp), %rdx
	movq	%rdx, 8(%rax)
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L169
	movq	24(%rbp), %xmm0
	movhps	32(%rbp), %xmm0
	movups	%xmm0, (%rax)
.L169:
	movl	%r12d, %edi
	call	OBJ_nid2obj@PLT
	movq	16(%r13), %rdi
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%rax, %rsi
	call	X509_ALGOR_set0@PLT
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L165:
	movl	$571, %r8d
	movl	$65, %edx
	movl	$100, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L163:
	movl	$516, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$153, %edx
	xorl	%r14d, %r14d
	movl	$100, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L190:
	movl	$26, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$107, %edx
	xorl	%r14d, %r14d
	movl	$131, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L191:
	movl	$788, %r12d
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L173:
	movl	$790, %r12d
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L172:
	movl	$789, %r12d
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L192:
	movl	$522, %r8d
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L171:
	xorl	%r14d, %r14d
	jmp	.L160
	.cfi_endproc
.LFE1507:
	.size	CMS_add0_recipient_key, .-CMS_add0_recipient_key
	.p2align 4
	.globl	CMS_RecipientInfo_kekri_get0_id
	.type	CMS_RecipientInfo_kekri_get0_id, @function
CMS_RecipientInfo_kekri_get0_id:
.LFB1508:
	.cfi_startproc
	endbr64
	cmpl	$2, (%rdi)
	jne	.L224
	movq	8(%rdi), %rdi
	movq	8(%rdi), %rax
	testq	%rsi, %rsi
	je	.L196
	movq	16(%rdi), %rdi
	movq	%rdi, (%rsi)
.L196:
	testq	%rdx, %rdx
	je	.L197
	movq	(%rax), %rsi
	movq	%rsi, (%rdx)
.L197:
	testq	%rcx, %rcx
	je	.L198
	movq	8(%rax), %rdx
	movq	%rdx, (%rcx)
.L198:
	testq	%r8, %r8
	je	.L199
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L200
	movq	(%rax), %rdx
	movq	%rdx, (%r8)
	testq	%r9, %r9
	je	.L223
.L201:
	movq	8(%rax), %rax
	movq	%rax, (%r9)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L199:
	testq	%r9, %r9
	je	.L223
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L201
.L202:
	movq	$0, (%r9)
.L223:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L224:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$123, %edx
	movl	$137, %esi
	movl	$46, %edi
	movl	$587, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L200:
	.cfi_restore 6
	movq	$0, (%r8)
	testq	%r9, %r9
	jne	.L202
	jmp	.L223
	.cfi_endproc
.LFE1508:
	.size	CMS_RecipientInfo_kekri_get0_id, .-CMS_RecipientInfo_kekri_get0_id
	.p2align 4
	.globl	CMS_RecipientInfo_set0_key
	.type	CMS_RecipientInfo_set0_key, @function
CMS_RecipientInfo_set0_key:
.LFB1509:
	.cfi_startproc
	endbr64
	cmpl	$2, (%rdi)
	jne	.L232
	movq	8(%rdi), %rax
	movq	%rsi, 32(%rax)
	movq	%rdx, 40(%rax)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L232:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$123, %edx
	movl	$144, %esi
	movl	$46, %edi
	movl	$617, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1509:
	.size	CMS_RecipientInfo_set0_key, .-CMS_RecipientInfo_set0_key
	.p2align 4
	.globl	CMS_RecipientInfo_decrypt
	.type	CMS_RecipientInfo_decrypt, @function
CMS_RecipientInfo_decrypt:
.LFB1512:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rsi), %r12d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$2, %r12d
	je	.L234
	cmpl	$3, %r12d
	je	.L235
	testl	%r12d, %r12d
	je	.L279
	movl	$768, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$155, %edx
	xorl	%r12d, %r12d
	movl	$134, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
.L233:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L280
	addq	$296, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L234:
	.cfi_restore_state
	movq	8(%rsi), %r12
	cmpq	$0, 32(%r12)
	je	.L281
	movq	8(%rdi), %rax
	movq	24(%rax), %rbx
	movq	16(%r12), %rax
	movq	(%rax), %rdi
	call	OBJ_obj2nid@PLT
	xorl	%esi, %esi
	leal	-788(%rax), %edx
	cmpl	$2, %edx
	jbe	.L282
	cmpq	%rsi, 40(%r12)
	jne	.L283
.L253:
	movq	24(%r12), %rax
	cmpl	$15, (%rax)
	jle	.L284
	leaq	-304(%rbp), %r13
	movq	32(%r12), %rdi
	salq	$3, %rsi
	movq	%r13, %rdx
	call	AES_set_decrypt_key@PLT
	testl	%eax, %eax
	jne	.L285
	movq	24(%r12), %rax
	movl	$724, %edx
	leaq	.LC0(%rip), %rsi
	movl	(%rax), %edi
	subl	$8, %edi
	movslq	%edi, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L286
	movq	24(%r12), %rax
	xorl	%esi, %esi
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	8(%rax), %rcx
	movl	(%rax), %r8d
	call	AES_unwrap_key@PLT
	testl	%eax, %eax
	jle	.L287
	cltq
	movq	%r14, 32(%rbx)
	movl	$1, %r12d
	movq	%rax, 40(%rbx)
.L259:
	movl	$244, %esi
	movq	%r13, %rdi
	call	OPENSSL_cleanse@PLT
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L279:
	movq	8(%rsi), %rbx
	movq	8(%rdi), %rax
	movq	40(%rbx), %r8
	movq	24(%rax), %r14
	testq	%r8, %r8
	je	.L288
	movl	52(%r14), %edx
	xorl	%r15d, %r15d
	testl	%edx, %edx
	je	.L239
	movl	48(%r14), %eax
	testl	%eax, %eax
	je	.L289
.L239:
	movq	%r8, %rdi
	xorl	%esi, %esi
	call	EVP_PKEY_CTX_new@PLT
	movq	%rax, 48(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L233
	call	EVP_PKEY_decrypt_init@PLT
	testl	%eax, %eax
	jle	.L277
	movq	%r13, %rdi
	movl	$1, %esi
	call	cms_env_asn1_ctrl
	movq	48(%rbx), %rdi
	testl	%eax, %eax
	jne	.L290
	call	EVP_PKEY_CTX_free@PLT
	xorl	%r13d, %r13d
	movq	$0, 48(%rbx)
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L235:
	xorl	%edx, %edx
	call	cms_RecipientInfo_pwri_crypt@PLT
	movl	%eax, %r12d
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L282:
	cltq
	leaq	-6288(,%rax,8), %rsi
	cmpq	%rsi, 40(%r12)
	je	.L253
.L283:
	movl	$705, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$118, %edx
	xorl	%r12d, %r12d
	movl	$135, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L292:
	movl	$400, %r8d
	movl	$110, %edx
	movl	$140, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L277:
	movq	48(%rbx), %rdi
	xorl	%r13d, %r13d
	call	EVP_PKEY_CTX_free@PLT
	movq	$0, 48(%rbx)
.L242:
	movl	$435, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L289:
	movq	8(%r14), %rax
	movq	%r8, -328(%rbp)
	movq	(%rax), %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_cipherbyname@PLT
	movq	-328(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L291
	movq	%r8, -328(%rbp)
	call	EVP_CIPHER_key_length@PLT
	movq	-328(%rbp), %r8
	movslq	%eax, %r15
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L290:
	xorl	%r8d, %r8d
	movq	%r13, %r9
	movl	$10, %ecx
	movl	$512, %edx
	movl	$-1, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L292
	movq	24(%rbx), %rax
	leaq	-312(%rbp), %r9
	movq	48(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r9, %rdx
	movq	%r9, -328(%rbp)
	movq	8(%rax), %rcx
	movslq	(%rax), %r8
	call	EVP_PKEY_decrypt@PLT
	testl	%eax, %eax
	jle	.L277
	movq	-312(%rbp), %rdi
	movl	$409, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movl	$412, %r8d
	movl	$65, %edx
	movq	-328(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r13
	leaq	.LC0(%rip), %rcx
	je	.L278
	movq	24(%rbx), %rax
	movq	48(%rbx), %rdi
	movq	%r9, %rdx
	movq	%r13, %rsi
	movq	8(%rax), %rcx
	movslq	(%rax), %r8
	call	EVP_PKEY_decrypt@PLT
	testl	%eax, %eax
	jle	.L247
	movq	-312(%rbp), %rax
	testq	%rax, %rax
	je	.L247
	cmpq	%r15, %rax
	je	.L248
	testq	%r15, %r15
	jne	.L247
.L248:
	movq	40(%r14), %rsi
	movq	32(%r14), %rdi
	movl	$427, %ecx
	leaq	.LC0(%rip), %rdx
	movl	$1, %r12d
	call	CRYPTO_clear_free@PLT
	movq	-312(%rbp), %rax
	movq	%r13, 32(%r14)
	movq	48(%rbx), %rdi
	movq	%rax, 40(%r14)
	call	EVP_PKEY_CTX_free@PLT
	movq	$0, 48(%rbx)
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L286:
	movl	$727, %r8d
	movl	$65, %edx
	movl	$135, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L255:
	movl	$748, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	xorl	%r12d, %r12d
	call	CRYPTO_free@PLT
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L284:
	movl	$713, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$117, %edx
	xorl	%r14d, %r14d
	movl	$135, %esi
	movl	$46, %edi
	leaq	-304(%rbp), %r13
	call	ERR_put_error@PLT
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L281:
	movl	$699, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$130, %edx
	xorl	%r12d, %r12d
	movl	$135, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L285:
	movl	$719, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$115, %edx
	xorl	%r14d, %r14d
	movl	$135, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L288:
	movl	$371, %r8d
	movl	$133, %edx
	movl	$140, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L233
.L247:
	movl	$421, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$104, %edx
.L278:
	movl	$140, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	movq	48(%rbx), %rdi
	call	EVP_PKEY_CTX_free@PLT
	movq	$0, 48(%rbx)
	jmp	.L242
.L291:
	movl	$381, %r8d
	movl	$148, %edx
	movl	$140, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L233
.L287:
	movl	$736, %r8d
	movl	$157, %edx
	movl	$135, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L255
.L280:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1512:
	.size	CMS_RecipientInfo_decrypt, .-CMS_RecipientInfo_decrypt
	.p2align 4
	.globl	CMS_RecipientInfo_encrypt
	.type	CMS_RecipientInfo_encrypt, @function
CMS_RecipientInfo_encrypt:
.LFB1513:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rsi), %r12d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$2, %r12d
	je	.L294
	jg	.L295
	testl	%r12d, %r12d
	je	.L296
	cmpl	$1, %r12d
	jne	.L298
	call	cms_RecipientInfo_kari_encrypt@PLT
	movl	%eax, %r12d
.L293:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L323
	addq	$296, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L295:
	.cfi_restore_state
	cmpl	$3, %r12d
	jne	.L298
	movl	$1, %edx
	call	cms_RecipientInfo_pwri_crypt@PLT
	movl	%eax, %r12d
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L294:
	movq	8(%rsi), %r14
	movq	32(%r14), %r8
	testq	%r8, %r8
	je	.L324
	movq	8(%rdi), %rax
	leaq	-304(%rbp), %r13
	movq	%r8, %rdi
	movq	%r13, %rdx
	movq	24(%rax), %rbx
	movq	40(%r14), %rax
	leaq	0(,%rax,8), %rsi
	call	AES_set_encrypt_key@PLT
	testl	%eax, %eax
	jne	.L325
	movq	40(%rbx), %rax
	movl	$654, %edx
	leaq	.LC0(%rip), %rsi
	leaq	8(%rax), %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L326
	movq	32(%rbx), %rcx
	movl	40(%rbx), %r8d
	movq	%rax, %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	AES_wrap_key@PLT
	movl	%eax, %edx
	testl	%eax, %eax
	jle	.L327
	movq	24(%r14), %rdi
	movq	%r12, %rsi
	movl	$1, %r12d
	call	ASN1_STRING_set0@PLT
.L313:
	movl	$244, %esi
	movq	%r13, %rdi
	call	OPENSSL_cleanse@PLT
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L298:
	movl	$790, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$154, %edx
	xorl	%r12d, %r12d
	movl	$169, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L296:
	movq	8(%rsi), %rbx
	movq	8(%rdi), %rax
	movq	48(%rbx), %r14
	movq	24(%rax), %r15
	testq	%r14, %r14
	je	.L300
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	cms_env_asn1_ctrl
	xorl	%r9d, %r9d
	testl	%eax, %eax
	je	.L301
.L302:
	xorl	%r8d, %r8d
	movq	%r13, %r9
	movl	$9, %ecx
	movl	$256, %edx
	movl	$-1, %esi
	movq	%r14, %rdi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L328
	movq	32(%r15), %rcx
	movq	40(%r15), %r8
	xorl	%esi, %esi
	movq	%r14, %rdi
	leaq	-312(%rbp), %r13
	movq	%r13, %rdx
	call	EVP_PKEY_encrypt@PLT
	xorl	%r9d, %r9d
	testl	%eax, %eax
	jle	.L301
	movq	-312(%rbp), %rdi
	movl	$333, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L329
	movq	32(%r15), %rcx
	movq	40(%r15), %r8
	movq	%r13, %rdx
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	%rax, -328(%rbp)
	call	EVP_PKEY_encrypt@PLT
	movq	-328(%rbp), %r9
	testl	%eax, %eax
	jle	.L301
	movq	24(%rbx), %rdi
	movl	-312(%rbp), %edx
	movq	%r9, %rsi
	movl	$1, %r12d
	call	ASN1_STRING_set0@PLT
	xorl	%r9d, %r9d
	.p2align 4,,10
	.p2align 3
.L301:
	movq	%r14, %rdi
	movq	%r9, -328(%rbp)
	call	EVP_PKEY_CTX_free@PLT
	movq	-328(%rbp), %r9
	movq	$0, 48(%rbx)
	movl	$351, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r9, %rdi
	call	CRYPTO_free@PLT
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L325:
	movl	$649, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$115, %edx
	xorl	%r12d, %r12d
	movl	$136, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
.L310:
	movq	%r12, %rdi
	movl	$675, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%r12d, %r12d
	call	CRYPTO_free@PLT
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L300:
	movq	40(%rbx), %rdi
	xorl	%esi, %esi
	call	EVP_PKEY_CTX_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L293
	movq	%rax, %rdi
	call	EVP_PKEY_encrypt_init@PLT
	xorl	%r9d, %r9d
	testl	%eax, %eax
	jg	.L302
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L324:
	movl	$644, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$130, %edx
	xorl	%r12d, %r12d
	movl	$136, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L328:
	movl	$326, %r8d
	movl	$110, %edx
	movl	$141, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r9d, %r9d
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L327:
	movl	$664, %r8d
	movl	$159, %edx
	movl	$136, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L326:
	movl	$657, %r8d
	movl	$65, %edx
	movl	$136, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L310
.L329:
	movl	$336, %r8d
	movl	$65, %edx
	movl	$141, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	movq	%rax, -328(%rbp)
	call	ERR_put_error@PLT
	movq	-328(%rbp), %r9
	jmp	.L301
.L323:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1513:
	.size	CMS_RecipientInfo_encrypt, .-CMS_RecipientInfo_encrypt
	.p2align 4
	.globl	cms_EnvelopedData_init_bio
	.type	cms_EnvelopedData_init_bio, @function
cms_EnvelopedData_init_bio:
.LFB1516:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	24(%rax), %r14
	movq	%r14, %rdi
	call	cms_EncryptedContent_init_bio@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L330
	cmpq	$0, 24(%r14)
	je	.L330
	movq	8(%r13), %rax
	xorl	%ebx, %ebx
	movq	16(%rax), %r12
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L334:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	CMS_RecipientInfo_encrypt
	testl	%eax, %eax
	jle	.L360
	addl	$1, %ebx
.L332:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L334
	movq	8(%r13), %r12
	movl	(%r12), %eax
	cmpl	$3, %eax
	jle	.L361
.L346:
	movq	40(%r14), %rsi
	movq	32(%r14), %rdi
	movq	$0, 24(%r14)
	movl	$895, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movq	$0, 32(%r14)
	movq	$0, 40(%r14)
.L330:
	addq	$8, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L360:
	.cfi_restore_state
	movl	$884, %r8d
	movl	$116, %edx
	movl	$125, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	40(%r14), %rsi
	movq	32(%r14), %rdi
	movq	$0, 24(%r14)
	movl	$895, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movq	$0, 32(%r14)
	movq	%r15, %rdi
	xorl	%r15d, %r15d
	movq	$0, 40(%r14)
	call	BIO_free@PLT
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L361:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L336
	xorl	%ebx, %ebx
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L339:
	addl	$1, %ebx
.L337:
	movq	0(%r13), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L362
	movq	0(%r13), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movl	(%rax), %eax
	cmpl	$4, %eax
	je	.L359
	cmpl	$3, %eax
	jne	.L339
	cmpl	$2, (%r12)
	jg	.L339
	movl	$3, (%r12)
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L359:
	movl	$4, (%r12)
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L362:
	xorl	%ebx, %ebx
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L343:
	movq	8(%r13), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	cmpl	$1, (%rax)
	je	.L359
	addl	$1, %ebx
.L341:
	movq	8(%r13), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L343
	movl	(%r12), %eax
.L336:
	cmpl	$2, %eax
	jg	.L346
	xorl	%ebx, %ebx
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L349:
	movq	16(%r12), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movl	(%rax), %edx
	leal	-3(%rdx), %ecx
	cmpl	$1, %ecx
	jbe	.L363
	testl	%edx, %edx
	jne	.L347
	movq	8(%rax), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L348
.L347:
	movl	$2, (%r12)
.L348:
	addl	$1, %ebx
.L344:
	movq	16(%r12), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L349
	cmpq	$0, 8(%r12)
	je	.L364
.L350:
	movl	$2, (%r12)
	jmp	.L346
.L363:
	movl	$3, (%r12)
	jmp	.L346
.L364:
	cmpq	$0, 32(%r12)
	jne	.L350
	cmpl	$2, (%r12)
	je	.L346
	movl	$0, (%r12)
	jmp	.L346
	.cfi_endproc
.LFE1516:
	.size	cms_EnvelopedData_init_bio, .-cms_EnvelopedData_init_bio
	.p2align 4
	.globl	cms_pkey_get_ri_type
	.type	cms_pkey_get_ri_type, @function
cms_pkey_get_ri_type:
.LFB1517:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	xorl	%r12d, %r12d
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L365
	movq	176(%rax), %rax
	testq	%rax, %rax
	je	.L365
	xorl	%edx, %edx
	leaq	-28(%rbp), %rcx
	movl	$8, %esi
	call	*%rax
	testl	%eax, %eax
	cmovg	-28(%rbp), %r12d
.L365:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L374
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L374:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1517:
	.size	cms_pkey_get_ri_type, .-cms_pkey_get_ri_type
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
