	.file	"rsa_x931.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/rsa/rsa_x931.c"
	.text
	.p2align 4
	.globl	RSA_padding_add_X931
	.type	RSA_padding_add_X931, @function
RSA_padding_add_X931:
.LFB419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subl	%ecx, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	subl	$2, %r14d
	js	.L12
	movq	%rdi, %r12
	movq	%rdx, %r13
	leaq	1(%rdi), %rdi
	movl	%ecx, %ebx
	testl	%r14d, %r14d
	je	.L13
	movb	$107, (%r12)
	cmpl	$1, %r14d
	je	.L6
	subl	$3, %esi
	movslq	%esi, %rdx
	movl	$187, %esi
	call	memset@PLT
	movslq	%r14d, %rdi
	addq	%r12, %rdi
.L6:
	movb	$-70, (%rdi)
	addq	$1, %rdi
.L5:
	movl	%ebx, %edx
	movq	%r13, %rsi
	movslq	%ebx, %rbx
	call	memcpy@PLT
	movb	$-52, (%rax,%rbx)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	movb	$106, (%r12)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$30, %r8d
	movl	$110, %edx
	movl	$127, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	movl	$-1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE419:
	.size	RSA_padding_add_X931, .-RSA_padding_add_X931
	.p2align 4
	.globl	RSA_padding_check_X931
	.type	RSA_padding_check_X931, @function
RSA_padding_check_X931:
.LFB420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	cmpl	%ecx, %r8d
	jne	.L15
	movzbl	(%rdx), %eax
	leal	-106(%rax), %ecx
	cmpb	$1, %cl
	ja	.L15
	leaq	1(%rdx), %rsi
	leal	-2(%r8), %r12d
	cmpb	$107, %al
	je	.L30
.L24:
	movslq	%r12d, %rax
	cmpb	$-52, (%rsi,%rax)
	jne	.L31
	movl	%r12d, %edx
	call	memcpy@PLT
.L14:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	leal	-3(%r8), %r12d
	testl	%r12d, %r12d
	jle	.L19
	xorl	%eax, %eax
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L34:
	cmpb	$-69, %dl
	jne	.L32
	addl	$1, %eax
	cmpl	%eax, %r12d
	je	.L33
.L22:
	movzbl	(%rsi), %edx
	addq	$1, %rsi
	cmpb	$-70, %dl
	jne	.L34
	subl	%eax, %r12d
	testl	%eax, %eax
	jne	.L24
.L19:
	movl	$80, %r8d
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L32:
	movl	$72, %r8d
.L29:
	movl	$138, %edx
	movl	$128, %esi
	movl	$4, %edi
	movl	$-1, %r12d
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L24
.L15:
	movl	$61, %r8d
	movl	$137, %edx
	movl	$128, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L14
.L31:
	movl	$89, %r8d
	movl	$139, %edx
	movl	$128, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L14
	.cfi_endproc
.LFE420:
	.size	RSA_padding_check_X931, .-RSA_padding_check_X931
	.p2align 4
	.globl	RSA_X931_hash_id
	.type	RSA_X931_hash_id, @function
RSA_X931_hash_id:
.LFB421:
	.cfi_startproc
	endbr64
	cmpl	$673, %edi
	je	.L38
	jg	.L37
	movl	$51, %eax
	cmpl	$64, %edi
	je	.L35
	cmpl	$672, %edi
	movl	$52, %edx
	movl	$-1, %eax
	cmove	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	cmpl	$674, %edi
	movl	$53, %edx
	movl	$-1, %eax
	cmove	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	movl	$54, %eax
.L35:
	ret
	.cfi_endproc
.LFE421:
	.size	RSA_X931_hash_id, .-RSA_X931_hash_id
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
