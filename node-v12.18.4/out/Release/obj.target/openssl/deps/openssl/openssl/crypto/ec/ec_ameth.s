	.file	"ec_ameth.c"
	.text
	.p2align 4
	.type	ec_pkey_public_check, @function
ec_pkey_public_check:
.LFB1494:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	jmp	EC_KEY_check_key@PLT
	.cfi_endproc
.LFE1494:
	.size	ec_pkey_public_check, .-ec_pkey_public_check
	.p2align 4
	.type	old_ec_priv_encode, @function
old_ec_priv_encode:
.LFB1491:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	jmp	i2d_ECPrivateKey@PLT
	.cfi_endproc
.LFE1491:
	.size	old_ec_priv_encode, .-old_ec_priv_encode
	.p2align 4
	.type	ec_missing_parameters, @function
ec_missing_parameters:
.LFB1480:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L8
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	EC_KEY_get0_group@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	sete	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore 6
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1480:
	.size	ec_missing_parameters, .-ec_missing_parameters
	.p2align 4
	.type	int_ec_free, @function
int_ec_free:
.LFB1483:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	jmp	EC_KEY_free@PLT
	.cfi_endproc
.LFE1483:
	.size	int_ec_free, .-int_ec_free
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ec/ec_ameth.c"
	.text
	.p2align 4
	.type	eckey_type2param, @function
eckey_type2param:
.LFB1472:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$16, %edi
	je	.L29
	cmpl	$6, %edi
	jne	.L19
	call	EC_KEY_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L30
	movq	%r13, %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	call	EC_GROUP_new_by_curve_name@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L17
	movl	$1, %esi
	movq	%rax, %rdi
	call	EC_GROUP_set_asn1_flag@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	EC_KEY_set_group@PLT
	testl	%eax, %eax
	je	.L17
	movq	%r13, %rdi
	call	EC_GROUP_free@PLT
.L14:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L31
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movq	8(%rsi), %rax
	movslq	(%rsi), %rdx
	xorl	%edi, %edi
	leaq	-32(%rbp), %rsi
	movq	%rax, -32(%rbp)
	call	d2i_ECParameters@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L14
	movl	$103, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$142, %edx
	xorl	%r13d, %r13d
	movl	$220, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L17:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	EC_KEY_free@PLT
	movq	%r13, %rdi
	call	EC_GROUP_free@PLT
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L19:
	movl	$142, %edx
	movl	$220, %esi
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	movl	$124, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L30:
	movl	$113, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$220, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L17
.L31:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1472:
	.size	eckey_type2param, .-eckey_type2param
	.p2align 4
	.type	ec_pkey_ctrl, @function
ec_pkey_ctrl:
.LFB1492:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$10, %esi
	ja	.L98
	movq	%rdi, %r14
	movq	%rcx, %r13
	leaq	.L35(%rip), %rdi
	movl	%esi, %ecx
	movslq	(%rdi,%rcx,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L35:
	.long	.L98-.L35
	.long	.L41-.L35
	.long	.L98-.L35
	.long	.L40-.L35
	.long	.L98-.L35
	.long	.L39-.L35
	.long	.L98-.L35
	.long	.L38-.L35
	.long	.L37-.L35
	.long	.L36-.L35
	.long	.L34-.L35
	.text
	.p2align 4,,10
	.p2align 3
.L98:
	movl	$-2, %r12d
	.p2align 4,,10
	.p2align 3
.L32:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L183
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movl	%esi, %r12d
	testq	%rdx, %rdx
	jne	.L32
	leaq	-64(%rbp), %rcx
	leaq	-72(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	PKCS7_SIGNER_INFO_get0_algs@PLT
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	jne	.L182
.L43:
	movl	$-1, %r12d
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L40:
	movq	%r14, %rdi
	call	EVP_PKEY_id@PLT
	cmpl	$1172, %eax
	je	.L184
	movl	$672, 0(%r13)
	movl	$1, %r12d
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L39:
	movl	$1, %r12d
	testq	%rdx, %rdx
	jne	.L32
	leaq	-72(%rbp), %rcx
	leaq	-64(%rbp), %r8
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	CMS_SignerInfo_get0_algs@PLT
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L43
.L182:
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L43
	call	OBJ_obj2nid@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L43
	movq	%r14, %rdi
	call	EVP_PKEY_id@PLT
	leaq	-80(%rbp), %rdi
	movl	%r13d, %esi
	movl	%eax, %edx
	call	OBJ_find_sigid_by_algs@PLT
	testl	%eax, %eax
	je	.L43
	movl	-80(%rbp), %edi
	call	OBJ_nid2obj@PLT
	movq	-64(%rbp), %rdi
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%rax, %rsi
	call	X509_ALGOR_set0@PLT
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L38:
	cmpq	$1, %rdx
	je	.L185
	testq	%rdx, %rdx
	jne	.L98
	movq	%r13, %rdi
	movq	$0, -72(%rbp)
	xorl	%r12d, %r12d
	call	CMS_RecipientInfo_get0_pkey_ctx@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L32
	movq	%rax, %rdi
	leaq	-104(%rbp), %r15
	call	EVP_PKEY_CTX_get0_pkey@PLT
	leaq	-88(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	CMS_RecipientInfo_kari_get0_orig_id@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L68
.L177:
	movq	-72(%rbp), %rdi
	xorl	%ebx, %ebx
.L69:
	movl	$942, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%rbx, %rdi
	call	X509_ALGOR_free@PLT
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L37:
	movl	$1, 0(%r13)
	movl	$1, %r12d
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L36:
	movq	%r14, %rdi
	movq	%rdx, -120(%rbp)
	call	EVP_PKEY_get0_EC_KEY@PLT
	movq	-120(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	EC_KEY_oct2key@PLT
	movl	%eax, %r12d
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L34:
	movq	%r14, %rdi
	call	EVP_PKEY_get0_EC_KEY@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$4, %esi
	movq	%rax, %rdi
	call	EC_KEY_key2buf@PLT
	movl	%eax, %r12d
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L68:
	movq	-104(%rbp), %rcx
	leaq	-96(%rbp), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	X509_ALGOR_get0@PLT
	xorl	%edi, %edi
	call	OBJ_nid2obj@PLT
	cmpq	%rax, -96(%rbp)
	je	.L186
.L70:
	xorl	%r9d, %r9d
	movl	$4100, %ecx
	movl	$1024, %edx
	movq	%r14, %rdi
	movl	$-2, %r8d
	movl	$408, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	jle	.L71
	leaq	-64(%rbp), %r9
	xorl	%r8d, %r8d
	movl	$4102, %ecx
	movq	%r14, %rdi
	movl	$1024, %edx
	movl	$408, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L177
	xorl	%r9d, %r9d
	movl	$4099, %ecx
	movl	$1024, %edx
	movq	%r14, %rdi
	movl	$-2, %r8d
	movl	$408, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	movl	%eax, %r10d
	testl	%eax, %eax
	js	.L71
	je	.L100
	cmpl	$1, %eax
	movl	$947, %eax
	cmove	%eax, %r10d
.L77:
	cmpl	$1, %ebx
	je	.L187
	.p2align 4,,10
	.p2align 3
.L71:
	movq	-72(%rbp), %rdi
.L72:
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L184:
	movl	$1143, 0(%r13)
	movl	$1, %r12d
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L185:
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	call	CMS_RecipientInfo_get0_pkey_ctx@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L32
	movq	%rax, %rdi
	leaq	-80(%rbp), %r15
	leaq	-88(%rbp), %r12
	call	EVP_PKEY_CTX_get0_peerkey@PLT
	testq	%rax, %rax
	je	.L188
.L45:
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	$0, -64(%rbp)
	call	CMS_RecipientInfo_kari_get0_alg@PLT
	testl	%eax, %eax
	jne	.L189
.L61:
	movl	$800, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$150, %edx
	xorl	%r12d, %r12d
	movl	$238, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L186:
	movq	40(%rbx), %r12
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	i2o_ECPublicKey@PLT
	testl	%eax, %eax
	jle	.L71
	movslq	%eax, %rdi
	movl	$841, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, -72(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L72
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	i2o_ECPublicKey@PLT
	movl	%eax, %edx
	testl	%eax, %eax
	jle	.L71
	movq	-72(%rbp), %rsi
	movq	-88(%rbp), %rdi
	call	ASN1_STRING_set0@PLT
	movq	-88(%rbp), %rdx
	movl	$408, %edi
	movq	16(%rdx), %rax
	andq	$-16, %rax
	orq	$8, %rax
	movq	%rax, 16(%rdx)
	movq	$0, -72(%rbp)
	call	OBJ_nid2obj@PLT
	movq	-104(%rbp), %rdi
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%rax, %rsi
	call	X509_ALGOR_set0@PLT
	jmp	.L70
.L189:
	movq	-88(%rbp), %rax
	movq	(%rax), %rdi
	call	OBJ_obj2nid@PLT
	testl	%eax, %eax
	je	.L62
	leaq	-72(%rbp), %r12
	leaq	-96(%rbp), %rdx
	movl	%eax, %edi
	movq	%r12, %rsi
	call	OBJ_find_sigid_algs@PLT
	testl	%eax, %eax
	je	.L62
	movl	-96(%rbp), %eax
	cmpl	$946, %eax
	je	.L97
	movl	$1, %r8d
	cmpl	$947, %eax
	jne	.L62
.L63:
	xorl	%r9d, %r9d
	movl	$4099, %ecx
	movl	$1024, %edx
	movq	%r14, %rdi
	movl	$408, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L62
	xorl	%r9d, %r9d
	movl	$4100, %ecx
	movl	$1024, %edx
	movq	%r14, %rdi
	movl	$2, %r8d
	movl	$408, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L62
	movl	-72(%rbp), %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L62
	xorl	%r8d, %r8d
	movl	$4101, %ecx
	movl	$1024, %edx
	movq	%r14, %rdi
	movl	$408, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L62
	movq	-88(%rbp), %rax
	movq	8(%rax), %rax
	cmpl	$16, (%rax)
	jne	.L61
	movq	8(%rax), %rax
	movq	%r12, %rsi
	xorl	%edi, %edi
	movq	8(%rax), %rdx
	movq	%rdx, -72(%rbp)
	movslq	(%rax), %rdx
	call	d2i_X509_ALGOR@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L66
	movq	%r13, %rdi
	call	CMS_RecipientInfo_kari_get0_ctx@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L66
	movq	(%r12), %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_cipherbyname@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L66
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpq	$65538, %rax
	je	.L190
.L66:
	movq	%r12, %rdi
	call	X509_ALGOR_free@PLT
	movq	-64(%rbp), %rdi
	movl	$774, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L61
.L62:
	movl	$735, %r8d
	movl	$148, %edx
	movl	$239, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L61
.L188:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	leaq	-96(%rbp), %rdx
	leaq	-104(%rbp), %rsi
	call	CMS_RecipientInfo_kari_get0_orig_id@PLT
	testl	%eax, %eax
	je	.L46
	movq	-104(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L46
	movq	-96(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L46
	movq	%r12, %rdi
	leaq	-108(%rbp), %rsi
	movq	%r15, %rdx
	movq	$0, -72(%rbp)
	call	X509_ALGOR_get0@PLT
	movq	-88(%rbp), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$408, %eax
	jne	.L51
	movl	-108(%rbp), %edi
	cmpl	$-1, %edi
	je	.L102
	cmpl	$5, %edi
	je	.L102
	movq	-80(%rbp), %rsi
	call	eckey_type2param
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	je	.L52
.L56:
	movq	%rbx, %rdi
	call	ASN1_STRING_length@PLT
	movq	%rbx, %rdi
	movl	%eax, -120(%rbp)
	call	ASN1_STRING_get0_data@PLT
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L51
	movslq	-120(%rbp), %rdx
	testl	%edx, %edx
	je	.L51
	leaq	-64(%rbp), %rsi
	leaq	-72(%rbp), %rdi
	call	o2i_ECPublicKey@PLT
	testq	%rax, %rax
	je	.L51
	call	EVP_PKEY_new@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L51
	movq	-72(%rbp), %rsi
	movq	%rax, %rdi
	call	EVP_PKEY_set1_EC_KEY@PLT
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	EVP_PKEY_derive_set_peer@PLT
	movq	-72(%rbp), %rdi
	testl	%eax, %eax
	jle	.L59
	call	EC_KEY_free@PLT
	movq	%rbx, %rdi
	call	EVP_PKEY_free@PLT
	jmp	.L45
.L100:
	movl	$946, %r10d
	jmp	.L77
.L102:
	movq	%r14, %rdi
	call	EVP_PKEY_CTX_get0_pkey@PLT
	testq	%rax, %rax
	je	.L51
	movq	40(%rax), %rdi
	call	EC_KEY_get0_group@PLT
	movq	%rax, -120(%rbp)
	call	EC_KEY_new@PLT
	movq	%rax, -72(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L52
	movq	-120(%rbp), %rsi
	call	EC_KEY_set_group@PLT
	testl	%eax, %eax
	jne	.L56
.L51:
	movq	-72(%rbp), %rdi
	call	EC_KEY_free@PLT
	xorl	%edi, %edi
	call	EVP_PKEY_free@PLT
.L48:
	movl	$794, %r8d
	movl	$149, %edx
	movl	$238, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L46:
	xorl	%r12d, %r12d
	jmp	.L32
.L187:
	xorl	%r9d, %r9d
	movl	$2, %r8d
	movq	%r14, %rdi
	movl	%r10d, -120(%rbp)
	movl	$4100, %ecx
	movl	$1024, %edx
	movl	$408, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	movl	-120(%rbp), %r10d
	testl	%eax, %eax
	jle	.L71
	movq	-64(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L191
.L80:
	leaq	-80(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	%r10d, -120(%rbp)
	call	CMS_RecipientInfo_kari_get0_alg@PLT
	movl	-120(%rbp), %r10d
	testl	%eax, %eax
	movl	%eax, %r12d
	je	.L177
	movq	-64(%rbp), %rdi
	movl	%r10d, -120(%rbp)
	call	EVP_MD_type@PLT
	movl	-120(%rbp), %r10d
	leaq	-108(%rbp), %rdi
	movl	%eax, %esi
	movl	%r10d, %edx
	call	OBJ_find_sigid_by_algs@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L177
	movq	%r13, %rdi
	call	CMS_RecipientInfo_kari_get0_ctx@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_type@PLT
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	EVP_CIPHER_CTX_key_length@PLT
	movl	%eax, %r13d
	call	X509_ALGOR_new@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L176
	movl	%r15d, %edi
	call	OBJ_nid2obj@PLT
	movq	%rax, (%rbx)
	call	ASN1_TYPE_new@PLT
	movq	%rax, 8(%rbx)
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L176
	movq	%r12, %rdi
	call	EVP_CIPHER_param_to_asn1@PLT
	testl	%eax, %eax
	jle	.L176
	movq	8(%rbx), %rdi
	call	ASN1_TYPE_get@PLT
	testl	%eax, %eax
	je	.L192
.L86:
	xorl	%r9d, %r9d
	movl	%r13d, %r8d
	movl	$4103, %ecx
	movl	$1024, %edx
	movl	$408, %esi
	movq	%r14, %rdi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L176
	movq	-80(%rbp), %rdx
	leaq	-72(%rbp), %r15
	movl	%r13d, %ecx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	CMS_SharedInfo_encode@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L175
	movq	-72(%rbp), %r9
	movl	%eax, %r8d
	movl	$4105, %ecx
	movq	%r14, %rdi
	movl	$1024, %edx
	movl	$408, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L176
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	$0, -72(%rbp)
	call	i2d_X509_ALGOR@PLT
	movq	-72(%rbp), %rdi
	movl	%eax, %r13d
	testq	%rdi, %rdi
	je	.L101
	testl	%eax, %eax
	je	.L101
	call	ASN1_STRING_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L175
	movq	-72(%rbp), %rsi
	movl	%r13d, %edx
	movq	%rax, %rdi
	call	ASN1_STRING_set0@PLT
	movl	-108(%rbp), %edi
	movq	$0, -72(%rbp)
	call	OBJ_nid2obj@PLT
	movq	-104(%rbp), %rdi
	movq	%r12, %rcx
	movl	$16, %edx
	movq	%rax, %rsi
	movl	$1, %r12d
	call	X509_ALGOR_set0@PLT
	movq	-72(%rbp), %rdi
	jmp	.L69
.L183:
	call	__stack_chk_fail@PLT
.L191:
	movl	%r10d, -120(%rbp)
	call	EVP_sha1@PLT
	xorl	%r8d, %r8d
	movl	$4101, %ecx
	movq	%r14, %rdi
	movq	%rax, %r9
	movl	$1024, %edx
	movl	$408, %esi
	movq	%rax, -64(%rbp)
	call	EVP_PKEY_CTX_ctrl@PLT
	movl	-120(%rbp), %r10d
	testl	%eax, %eax
	jg	.L80
.L176:
	movq	-72(%rbp), %rdi
	xorl	%r12d, %r12d
	jmp	.L69
.L97:
	xorl	%r8d, %r8d
	jmp	.L63
.L190:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	EVP_EncryptInit_ex@PLT
	testl	%eax, %eax
	je	.L66
	movq	8(%r12), %rsi
	movq	%r13, %rdi
	call	EVP_CIPHER_asn1_to_param@PLT
	testl	%eax, %eax
	jle	.L66
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_key_length@PLT
	xorl	%r9d, %r9d
	movl	$4103, %ecx
	movq	%r14, %rdi
	movl	%eax, %r8d
	movl	$1024, %edx
	movl	$408, %esi
	movl	%eax, %r13d
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L66
	movq	-80(%rbp), %rdx
	leaq	-64(%rbp), %rdi
	movl	%r13d, %ecx
	movq	%r12, %rsi
	call	CMS_SharedInfo_encode@PLT
	movl	%eax, %r8d
	testl	%eax, %eax
	je	.L66
	movq	-64(%rbp), %r9
	movl	$4105, %ecx
	movl	$1024, %edx
	movq	%r14, %rdi
	movl	$408, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L66
	movq	%r12, %rdi
	movq	$0, -64(%rbp)
	movl	$1, %r12d
	call	X509_ALGOR_free@PLT
	movq	-64(%rbp), %rdi
	movl	$774, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L32
.L52:
	xorl	%edi, %edi
	call	EC_KEY_free@PLT
	xorl	%edi, %edi
	call	EVP_PKEY_free@PLT
	jmp	.L48
.L175:
	movq	-72(%rbp), %rdi
	jmp	.L69
.L192:
	movq	8(%rbx), %rdi
	call	ASN1_TYPE_free@PLT
	movq	$0, 8(%rbx)
	jmp	.L86
.L59:
	call	EC_KEY_free@PLT
	movq	%rbx, %rdi
	call	EVP_PKEY_free@PLT
	jmp	.L48
.L101:
	xorl	%r12d, %r12d
	jmp	.L69
	.cfi_endproc
.LFE1492:
	.size	ec_pkey_ctrl, .-ec_pkey_ctrl
	.p2align 4
	.type	ec_bits, @function
ec_bits:
.LFB1478:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	40(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	EC_KEY_get0_group@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	%rax, %rdi
	jmp	EC_GROUP_order_bits@PLT
	.cfi_endproc
.LFE1478:
	.size	ec_bits, .-ec_bits
	.p2align 4
	.type	ec_security_bits, @function
ec_security_bits:
.LFB1479:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	40(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	EC_KEY_get0_group@PLT
	movq	%rax, %rdi
	call	EC_GROUP_order_bits@PLT
	movl	$256, %r8d
	cmpl	$511, %eax
	jg	.L195
	movl	$192, %r8d
	cmpl	$383, %eax
	jg	.L195
	movl	$128, %r8d
	cmpl	$255, %eax
	jg	.L195
	movl	$112, %r8d
	cmpl	$223, %eax
	jg	.L195
	movl	$80, %r8d
	cmpl	$159, %eax
	jg	.L195
	movl	%eax, %r8d
	shrl	$31, %r8d
	addl	%eax, %r8d
	sarl	%r8d
.L195:
	movl	%r8d, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1479:
	.size	ec_security_bits, .-ec_security_bits
	.p2align 4
	.type	ec_cmp_parameters, @function
ec_cmp_parameters:
.LFB1482:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	40(%rdi), %rdi
	movq	%rsi, %rbx
	call	EC_KEY_get0_group@PLT
	movq	40(%rbx), %rdi
	movq	%rax, %r12
	call	EC_KEY_get0_group@PLT
	testq	%r12, %r12
	je	.L205
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L205
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	EC_GROUP_cmp@PLT
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
.L203:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	.cfi_restore_state
	movl	$-2, %eax
	jmp	.L203
	.cfi_endproc
.LFE1482:
	.size	ec_cmp_parameters, .-ec_cmp_parameters
	.p2align 4
	.type	ec_copy_parameters, @function
ec_copy_parameters:
.LFB1481:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	40(%rsi), %rdi
	call	EC_KEY_get0_group@PLT
	movq	%rax, %rdi
	call	EC_GROUP_dup@PLT
	testq	%rax, %rax
	je	.L211
	movq	40(%rbx), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L219
.L209:
	movq	%r12, %rsi
	call	EC_KEY_set_group@PLT
	testl	%eax, %eax
	jne	.L220
.L210:
	movq	%r12, %rdi
	call	EC_GROUP_free@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L220:
	.cfi_restore_state
	movq	%r12, %rdi
	call	EC_GROUP_free@PLT
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L219:
	.cfi_restore_state
	call	EC_KEY_new@PLT
	movq	%rax, 40(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L209
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L211:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1481:
	.size	ec_copy_parameters, .-ec_copy_parameters
	.p2align 4
	.type	eckey_param_encode, @function
eckey_param_encode:
.LFB1486:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	jmp	i2d_ECParameters@PLT
	.cfi_endproc
.LFE1486:
	.size	eckey_param_encode, .-eckey_param_encode
	.p2align 4
	.type	int_ec_size, @function
int_ec_size:
.LFB1477:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	jmp	ECDSA_size@PLT
	.cfi_endproc
.LFE1477:
	.size	int_ec_size, .-int_ec_size
	.p2align 4
	.type	eckey_priv_decode, @function
eckey_priv_decode:
.LFB1475:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-56(%rbp), %r13
	movq	%rdi, %r12
	leaq	-32(%rbp), %rcx
	xorl	%edi, %edi
	leaq	-60(%rbp), %rdx
	movq	%r13, %rsi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$0, -56(%rbp)
	movq	$0, -40(%rbp)
	call	PKCS8_pkey_get0@PLT
	testl	%eax, %eax
	jne	.L233
.L223:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L234
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L233:
	.cfi_restore_state
	movq	-32(%rbp), %rcx
	leaq	-48(%rbp), %rdx
	leaq	-64(%rbp), %rsi
	xorl	%edi, %edi
	call	X509_ALGOR_get0@PLT
	movq	-48(%rbp), %rsi
	movl	-64(%rbp), %edi
	call	eckey_type2param
	movq	%rax, -40(%rbp)
	testq	%rax, %rax
	je	.L235
	movslq	-60(%rbp), %rdx
	leaq	-40(%rbp), %rdi
	movq	%r13, %rsi
	call	d2i_ECPrivateKey@PLT
	testq	%rax, %rax
	je	.L236
	movq	-40(%rbp), %rdx
	movl	$408, %esi
	movq	%r12, %rdi
	call	EVP_PKEY_assign@PLT
	movl	$1, %eax
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L235:
	movl	$212, %r8d
	movl	$16, %edx
	movl	$213, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L226:
	movq	-40(%rbp), %rdi
	call	EC_KEY_free@PLT
	xorl	%eax, %eax
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L236:
	movl	$204, %r8d
	movl	$142, %edx
	movl	$213, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L226
.L234:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1475:
	.size	eckey_priv_decode, .-eckey_priv_decode
	.p2align 4
	.type	eckey_pub_cmp, @function
eckey_pub_cmp:
.LFB1474:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	40(%rsi), %rdi
	call	EC_KEY_get0_group@PLT
	movq	40(%r13), %rdi
	movq	%rax, %r12
	call	EC_KEY_get0_public_key@PLT
	movq	40(%rbx), %rdi
	movq	%rax, %r13
	call	EC_KEY_get0_public_key@PLT
	testq	%r12, %r12
	sete	%cl
	testq	%r13, %r13
	movq	%rax, %rdx
	sete	%al
	orb	%al, %cl
	jne	.L241
	testq	%rdx, %rdx
	je	.L241
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	EC_POINT_cmp@PLT
	movl	$1, %r8d
	testl	%eax, %eax
	je	.L237
	cmpl	$1, %eax
	jne	.L241
	xorl	%r8d, %r8d
.L237:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L241:
	.cfi_restore_state
	movl	$-2, %r8d
	jmp	.L237
	.cfi_endproc
.LFE1474:
	.size	eckey_pub_cmp, .-eckey_pub_cmp
	.p2align 4
	.type	eckey_pub_decode, @function
eckey_pub_decode:
.LFB1473:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-56(%rbp), %r13
	movq	%rdi, %r12
	leaq	-32(%rbp), %rcx
	xorl	%edi, %edi
	leaq	-60(%rbp), %rdx
	movq	%r13, %rsi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$0, -56(%rbp)
	movq	$0, -40(%rbp)
	call	X509_PUBKEY_get0_param@PLT
	testl	%eax, %eax
	jne	.L254
.L245:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L255
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L254:
	.cfi_restore_state
	movq	-32(%rbp), %rcx
	leaq	-48(%rbp), %rdx
	leaq	-64(%rbp), %rsi
	xorl	%edi, %edi
	call	X509_ALGOR_get0@PLT
	movq	-48(%rbp), %rsi
	movl	-64(%rbp), %edi
	call	eckey_type2param
	movq	%rax, -40(%rbp)
	testq	%rax, %rax
	je	.L256
	movslq	-60(%rbp), %rdx
	leaq	-40(%rbp), %rdi
	movq	%r13, %rsi
	call	o2i_ECPublicKey@PLT
	testq	%rax, %rax
	je	.L257
	movq	-40(%rbp), %rdx
	movl	$408, %esi
	movq	%r12, %rdi
	call	EVP_PKEY_assign@PLT
	movl	$1, %eax
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L256:
	movl	$151, %r8d
	movl	$16, %edx
	movl	$215, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L257:
	movl	$157, %r8d
	movl	$142, %edx
	movl	$215, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-40(%rbp), %rdi
	call	EC_KEY_free@PLT
	xorl	%eax, %eax
	jmp	.L245
.L255:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1473:
	.size	eckey_pub_decode, .-eckey_pub_decode
	.p2align 4
	.type	ec_pkey_param_check, @function
ec_pkey_param_check:
.LFB1495:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movq	24(%rax), %rdi
	testq	%rdi, %rdi
	je	.L262
	xorl	%esi, %esi
	jmp	EC_GROUP_check@PLT
	.p2align 4,,10
	.p2align 3
.L262:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$124, %edx
	movl	$274, %esi
	movl	$16, %edi
	movl	$564, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1495:
	.size	ec_pkey_param_check, .-ec_pkey_param_check
	.p2align 4
	.type	ec_pkey_check, @function
ec_pkey_check:
.LFB1493:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	cmpq	$0, 40(%rdi)
	je	.L269
	jmp	EC_KEY_check_key@PLT
	.p2align 4,,10
	.p2align 3
.L269:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$125, %edx
	movl	$273, %esi
	movl	$16, %edi
	movl	$535, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1493:
	.size	ec_pkey_check, .-ec_pkey_check
	.p2align 4
	.type	old_ec_priv_decode, @function
old_ec_priv_decode:
.LFB1490:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	xorl	%edi, %edi
	subq	$8, %rsp
	call	d2i_ECPrivateKey@PLT
	testq	%rax, %rax
	je	.L276
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	$408, %esi
	call	EVP_PKEY_assign@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L276:
	.cfi_restore_state
	movl	$447, %r8d
	movl	$142, %edx
	movl	$222, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1490:
	.size	old_ec_priv_decode, .-old_ec_priv_decode
	.p2align 4
	.type	eckey_param_decode, @function
eckey_param_decode:
.LFB1485:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	xorl	%edi, %edi
	subq	$8, %rsp
	call	d2i_ECParameters@PLT
	testq	%rax, %rax
	je	.L281
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	$408, %esi
	call	EVP_PKEY_assign@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L281:
	.cfi_restore_state
	movl	$411, %r8d
	movl	$16, %edx
	movl	$212, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1485:
	.size	eckey_param_decode, .-eckey_param_decode
	.p2align 4
	.type	eckey_param2type, @function
eckey_param2type:
.LFB1470:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L285
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	movq	%rsi, %r14
	movq	%rdx, %r12
	call	EC_KEY_get0_group@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L285
	movq	%rax, %rdi
	call	EC_GROUP_get_asn1_flag@PLT
	testl	%eax, %eax
	jne	.L300
.L286:
	call	ASN1_STRING_new@PLT
	xorl	%r15d, %r15d
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L282
	leaq	8(%rax), %rsi
	movq	%r12, %rdi
	call	i2d_ECParameters@PLT
	movl	%eax, 0(%r13)
	testl	%eax, %eax
	jle	.L301
	movq	%r13, (%r14)
	movl	$1, %r15d
	movl	$16, (%rbx)
.L282:
	addq	$8, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L300:
	.cfi_restore_state
	movq	%r13, %rdi
	call	EC_GROUP_get_curve_name@PLT
	testl	%eax, %eax
	je	.L286
	movl	%eax, %edi
	movl	$1, %r15d
	call	OBJ_nid2obj@PLT
	movq	%rax, (%r14)
	movl	$6, (%rbx)
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L285:
	movl	$31, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$124, %edx
	xorl	%r15d, %r15d
	movl	$223, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L301:
	movq	%r13, %rdi
	call	ASN1_STRING_free@PLT
	movl	$49, %r8d
	movl	$16, %edx
	leaq	.LC0(%rip), %rcx
	movl	$223, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L282
	.cfi_endproc
.LFE1470:
	.size	eckey_param2type, .-eckey_param2type
	.p2align 4
	.type	eckey_priv_encode, @function
eckey_priv_encode:
.LFB1476:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-164(%rbp), %rdi
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-144(%rbp), %r13
	pushq	%r12
	movq	%r13, %rdx
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	40(%rsi), %rax
	leaq	-152(%rbp), %rsi
	movdqu	(%rax), %xmm0
	movaps	%xmm0, -144(%rbp)
	movdqu	16(%rax), %xmm1
	movaps	%xmm1, -128(%rbp)
	movdqu	32(%rax), %xmm2
	movaps	%xmm2, -112(%rbp)
	movdqu	48(%rax), %xmm3
	movaps	%xmm3, -96(%rbp)
	movdqu	64(%rax), %xmm4
	movaps	%xmm4, -80(%rbp)
	call	eckey_param2type
	testl	%eax, %eax
	je	.L311
	movq	%r13, %rdi
	call	EC_KEY_get_enc_flags@PLT
	movq	%r13, %rdi
	orl	$1, %eax
	movl	%eax, %esi
	call	EC_KEY_set_enc_flags@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	i2d_ECPrivateKey@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L312
	movslq	%eax, %rdi
	movl	$245, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L313
	leaq	-160(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, -160(%rbp)
	call	i2d_ECPrivateKey@PLT
	testl	%eax, %eax
	je	.L314
	movl	$408, %edi
	movq	-152(%rbp), %rbx
	movl	-164(%rbp), %r13d
	call	OBJ_nid2obj@PLT
	subq	$8, %rsp
	xorl	%edx, %edx
	movq	%r15, %r9
	pushq	%r12
	movq	%rax, %rsi
	movq	%rbx, %r8
	movl	%r13d, %ecx
	movq	%r14, %rdi
	call	PKCS8_pkey_set0@PLT
	movl	%eax, %r12d
	popq	%rax
	popq	%rdx
	testl	%r12d, %r12d
	je	.L315
	movl	$1, %r12d
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L312:
	movl	$242, %r8d
	movl	$16, %edx
	movl	$214, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L302:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L316
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L311:
	.cfi_restore_state
	movl	$227, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$142, %edx
	movl	%eax, %r12d
	movl	$214, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L314:
	movl	$252, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	CRYPTO_free@PLT
	movl	$253, %r8d
	movl	$16, %edx
	leaq	.LC0(%rip), %rcx
	movl	$214, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L313:
	movl	$247, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$214, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L315:
	movl	$259, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	call	CRYPTO_free@PLT
	jmp	.L302
.L316:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1476:
	.size	eckey_priv_encode, .-eckey_priv_encode
	.p2align 4
	.type	eckey_pub_encode, @function
eckey_pub_encode:
.LFB1471:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	leaq	-76(%rbp), %rdi
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	40(%rsi), %r15
	leaq	-72(%rbp), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -72(%rbp)
	movq	%r15, %rdx
	call	eckey_param2type
	testl	%eax, %eax
	je	.L331
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	i2o_ECPublicKey@PLT
	testl	%eax, %eax
	jle	.L322
	movslq	%eax, %rdi
	movl	$73, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L322
	leaq	-64(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	call	i2o_ECPublicKey@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jle	.L321
	movq	-72(%rbp), %rbx
	movl	-76(%rbp), %r15d
	movl	$408, %edi
	call	OBJ_nid2obj@PLT
	movl	%r12d, %r9d
	movq	%r14, %r8
	movq	%r13, %rdi
	movq	%rax, %rsi
	movq	%rbx, %rcx
	movl	%r15d, %edx
	movl	$1, %r12d
	call	X509_PUBKEY_set0_param@PLT
	testl	%eax, %eax
	jne	.L317
	cmpl	$6, -76(%rbp)
	movq	-72(%rbp), %rdi
	jne	.L323
	.p2align 4,,10
	.p2align 3
.L332:
	call	ASN1_OBJECT_free@PLT
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L322:
	xorl	%r14d, %r14d
.L321:
	cmpl	$6, -76(%rbp)
	movq	-72(%rbp), %rdi
	je	.L332
.L323:
	call	ASN1_STRING_free@PLT
.L324:
	movl	$88, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	xorl	%r12d, %r12d
	call	CRYPTO_free@PLT
.L317:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L333
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L331:
	.cfi_restore_state
	movl	$67, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$16, %edx
	movl	%eax, %r12d
	movl	$216, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L317
.L333:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1471:
	.size	eckey_pub_encode, .-eckey_pub_encode
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"ECDSA-Parameters"
.LC2:
	.string	"%s: (%d bit)\n"
	.text
	.p2align 4
	.type	eckey_param_print, @function
eckey_param_print:
.LFB1487:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	40(%rsi), %rdi
	testq	%rdi, %rdi
	je	.L337
	movl	%edx, %r13d
	call	EC_KEY_get0_group@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L337
	movl	$128, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	BIO_indent@PLT
	testl	%eax, %eax
	jne	.L338
.L340:
	movl	$399, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$16, %edx
	xorl	%r12d, %r12d
	movl	$221, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
.L339:
	movl	$400, %ecx
	leaq	.LC0(%rip), %rdx
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	CRYPTO_clear_free@PLT
	movl	$401, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L338:
	.cfi_restore_state
	movq	%r14, %rdi
	call	EC_GROUP_order_bits@PLT
	leaq	.LC1(%rip), %rdx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, %ecx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L340
	movl	%r13d, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	ECPKParameters_print@PLT
	testl	%eax, %eax
	je	.L340
	movl	$1, %r12d
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L337:
	movl	$351, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$67, %edx
	xorl	%r12d, %r12d
	movl	$221, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1487:
	.size	eckey_param_print, .-eckey_param_print
	.section	.rodata.str1.1
.LC3:
	.string	"Public-Key"
.LC4:
	.string	""
.LC5:
	.string	"%*spub:\n"
	.text
	.p2align 4
	.type	eckey_pub_print, @function
eckey_pub_print:
.LFB1488:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	40(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	testq	%r12, %r12
	je	.L351
	movq	%rdi, %r13
	movq	%r12, %rdi
	movl	%edx, %r15d
	call	EC_KEY_get0_group@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L351
	movq	%r12, %rdi
	call	EC_KEY_get0_public_key@PLT
	testq	%rax, %rax
	je	.L358
	movq	%r12, %rdi
	call	EC_KEY_get_conv_form@PLT
	movq	%r12, %rdi
	leaq	-48(%rbp), %rdx
	xorl	%ecx, %ecx
	movl	%eax, %esi
	call	EC_KEY_key2buf@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L354
.L352:
	movl	$128, %edx
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	BIO_indent@PLT
	testl	%eax, %eax
	je	.L354
	movq	%r14, %rdi
	call	EC_GROUP_order_bits@PLT
	leaq	.LC3(%rip), %rdx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	movl	%eax, %ecx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L354
	testq	%r12, %r12
	je	.L356
	xorl	%eax, %eax
	leaq	.LC4(%rip), %rcx
	movl	%r15d, %edx
	movq	%r13, %rdi
	leaq	.LC5(%rip), %rsi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L354
	movq	-48(%rbp), %rsi
	leal	4(%r15), %ecx
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	ASN1_buf_print@PLT
	testl	%eax, %eax
	je	.L354
.L356:
	movl	%r15d, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	$1, %r12d
	call	ECPKParameters_print@PLT
	testl	%eax, %eax
	jne	.L353
	.p2align 4,,10
	.p2align 3
.L354:
	movl	$399, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$16, %edx
	xorl	%r12d, %r12d
	movl	$221, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
.L353:
	leaq	.LC0(%rip), %rdx
	xorl	%esi, %esi
	xorl	%edi, %edi
	movl	$400, %ecx
	call	CRYPTO_clear_free@PLT
	movq	-48(%rbp), %rdi
	movl	$401, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
.L348:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L372
	addq	$16, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L358:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L351:
	movl	$351, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$67, %edx
	xorl	%r12d, %r12d
	movl	$221, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L348
.L372:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1488:
	.size	eckey_pub_print, .-eckey_pub_print
	.section	.rodata.str1.1
.LC6:
	.string	"Private-Key"
.LC7:
	.string	"%*spriv:\n"
	.text
	.p2align 4
	.type	eckey_priv_print, @function
eckey_priv_print:
.LFB1489:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	40(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	testq	%r12, %r12
	je	.L376
	movq	%rdi, %r13
	movq	%r12, %rdi
	movl	%edx, %r14d
	call	EC_KEY_get0_group@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L376
	movq	%r12, %rdi
	call	EC_KEY_get0_public_key@PLT
	testq	%rax, %rax
	je	.L387
	movq	%r12, %rdi
	call	EC_KEY_get_conv_form@PLT
	leaq	-64(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movl	%eax, %esi
	call	EC_KEY_key2buf@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L380
	movq	%r12, %rdi
	call	EC_KEY_get0_private_key@PLT
	testq	%rax, %rax
	je	.L388
.L405:
	movq	%r12, %rdi
	leaq	-72(%rbp), %rsi
	call	EC_KEY_priv2buf@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L380
.L379:
	movl	$128, %edx
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	BIO_indent@PLT
	testl	%eax, %eax
	je	.L378
	movq	%r15, %rdi
	call	EC_GROUP_order_bits@PLT
	leaq	.LC6(%rip), %rdx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	movl	%eax, %ecx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L378
	testq	%r12, %r12
	jne	.L381
.L384:
	testq	%rbx, %rbx
	je	.L383
	xorl	%eax, %eax
	leaq	.LC4(%rip), %rcx
	movl	%r14d, %edx
	movq	%r13, %rdi
	leaq	.LC5(%rip), %rsi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L378
	movq	-64(%rbp), %rsi
	leal	4(%r14), %ecx
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	ASN1_buf_print@PLT
	testl	%eax, %eax
	je	.L378
.L383:
	movq	%r13, %rdi
	movl	%r14d, %edx
	movq	%r15, %rsi
	movl	$1, %r13d
	call	ECPKParameters_print@PLT
	testl	%eax, %eax
	jne	.L385
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L387:
	movq	%r12, %rdi
	xorl	%ebx, %ebx
	call	EC_KEY_get0_private_key@PLT
	testq	%rax, %rax
	jne	.L405
.L388:
	xorl	%r12d, %r12d
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L380:
	xorl	%r12d, %r12d
.L378:
	movl	$399, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$16, %edx
	xorl	%r13d, %r13d
	movl	$221, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
.L385:
	movq	-72(%rbp), %rdi
	leaq	.LC0(%rip), %rdx
	movq	%r12, %rsi
	movl	$400, %ecx
	call	CRYPTO_clear_free@PLT
	movq	-64(%rbp), %rdi
	movl	$401, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
.L373:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L406
	addq	$40, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L376:
	.cfi_restore_state
	movl	$351, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$67, %edx
	xorl	%r13d, %r13d
	movl	$221, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L381:
	xorl	%eax, %eax
	leaq	.LC4(%rip), %rcx
	movl	%r14d, %edx
	movq	%r13, %rdi
	leaq	.LC7(%rip), %rsi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L378
	movq	-72(%rbp), %rsi
	leal	4(%r14), %ecx
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	ASN1_buf_print@PLT
	testl	%eax, %eax
	jne	.L384
	jmp	.L378
.L406:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1489:
	.size	eckey_priv_print, .-eckey_priv_print
	.p2align 4
	.globl	EC_KEY_print
	.type	EC_KEY_print, @function
EC_KEY_print:
.LFB1496:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EC_KEY_get0_private_key@PLT
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	testq	%r14, %r14
	je	.L410
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	EC_KEY_get0_group@PLT
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L410
	movq	%r14, %rdi
	call	EC_KEY_get0_public_key@PLT
	testq	%rax, %rax
	je	.L421
	movq	%r14, %rdi
	call	EC_KEY_get_conv_form@PLT
	leaq	-64(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	movl	%eax, %esi
	call	EC_KEY_key2buf@PLT
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L414
.L411:
	leaq	.LC3(%rip), %rax
	xorl	%r15d, %r15d
	movq	%rax, -96(%rbp)
	testq	%rbx, %rbx
	jne	.L441
.L413:
	movl	$128, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	BIO_indent@PLT
	testl	%eax, %eax
	je	.L412
	movq	-88(%rbp), %rdi
	call	EC_GROUP_order_bits@PLT
	movq	-96(%rbp), %rdx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, %ecx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L412
	testq	%r15, %r15
	jne	.L415
.L418:
	cmpq	$0, -104(%rbp)
	je	.L417
	xorl	%eax, %eax
	leaq	.LC4(%rip), %rcx
	movl	%r13d, %edx
	movq	%r12, %rdi
	leaq	.LC5(%rip), %rsi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L412
	movq	-104(%rbp), %rdx
	movq	-64(%rbp), %rsi
	leal	4(%r13), %ecx
	movq	%r12, %rdi
	call	ASN1_buf_print@PLT
	testl	%eax, %eax
	je	.L412
.L417:
	movq	-88(%rbp), %rsi
	movq	%r12, %rdi
	movl	%r13d, %edx
	movl	$1, %r12d
	call	ECPKParameters_print@PLT
	testl	%eax, %eax
	jne	.L419
	.p2align 4,,10
	.p2align 3
.L412:
	movl	$399, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$16, %edx
	xorl	%r12d, %r12d
	movl	$221, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
.L419:
	movq	-72(%rbp), %rdi
	leaq	.LC0(%rip), %rdx
	movq	%r15, %rsi
	movl	$400, %ecx
	call	CRYPTO_clear_free@PLT
	movq	-64(%rbp), %rdi
	movl	$401, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
.L407:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L442
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L421:
	.cfi_restore_state
	movq	$0, -104(%rbp)
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L410:
	movl	$351, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$67, %edx
	xorl	%r12d, %r12d
	movl	$221, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L441:
	movq	%r14, %rdi
	call	EC_KEY_get0_private_key@PLT
	testq	%rax, %rax
	je	.L423
	leaq	-72(%rbp), %rsi
	movq	%r14, %rdi
	call	EC_KEY_priv2buf@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L414
.L423:
	leaq	.LC6(%rip), %rax
	movq	%rax, -96(%rbp)
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L414:
	xorl	%r15d, %r15d
	jmp	.L412
	.p2align 4,,10
	.p2align 3
.L415:
	xorl	%eax, %eax
	leaq	.LC4(%rip), %rcx
	movl	%r13d, %edx
	movq	%r12, %rdi
	leaq	.LC7(%rip), %rsi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L412
	movq	-72(%rbp), %rsi
	leal	4(%r13), %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	ASN1_buf_print@PLT
	testl	%eax, %eax
	jne	.L418
	jmp	.L412
.L442:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1496:
	.size	EC_KEY_print, .-EC_KEY_print
	.p2align 4
	.globl	ECParameters_print
	.type	ECParameters_print, @function
ECParameters_print:
.LFB1497:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	testq	%rsi, %rsi
	je	.L446
	movq	%rdi, %r12
	movq	%rsi, %rdi
	call	EC_KEY_get0_group@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L446
	movl	$128, %edx
	movl	$4, %esi
	movq	%r12, %rdi
	call	BIO_indent@PLT
	testl	%eax, %eax
	jne	.L447
.L449:
	movl	$399, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$16, %edx
	xorl	%r12d, %r12d
	movl	$221, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
.L448:
	movl	$400, %ecx
	leaq	.LC0(%rip), %rdx
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	CRYPTO_clear_free@PLT
	movl	$401, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	CRYPTO_free@PLT
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L447:
	.cfi_restore_state
	movq	%r13, %rdi
	call	EC_GROUP_order_bits@PLT
	leaq	.LC1(%rip), %rdx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, %ecx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L449
	movl	$4, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	ECPKParameters_print@PLT
	testl	%eax, %eax
	je	.L449
	movl	$1, %r12d
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L446:
	xorl	%r12d, %r12d
	movl	$351, %r8d
	movl	$67, %edx
	movl	$221, %esi
	leaq	.LC0(%rip), %rcx
	movl	$16, %edi
	call	ERR_put_error@PLT
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1497:
	.size	ECParameters_print, .-ECParameters_print
	.globl	sm2_asn1_meth
	.section	.rodata
	.align 32
	.type	sm2_asn1_meth, @object
	.size	sm2_asn1_meth, 280
sm2_asn1_meth:
	.long	1172
	.long	408
	.quad	1
	.zero	264
	.globl	eckey_asn1_meth
	.section	.rodata.str1.1
.LC8:
	.string	"EC"
.LC9:
	.string	"OpenSSL EC algorithm"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	eckey_asn1_meth, @object
	.size	eckey_asn1_meth, 280
eckey_asn1_meth:
	.long	408
	.long	408
	.quad	0
	.quad	.LC8
	.quad	.LC9
	.quad	eckey_pub_decode
	.quad	eckey_pub_encode
	.quad	eckey_pub_cmp
	.quad	eckey_pub_print
	.quad	eckey_priv_decode
	.quad	eckey_priv_encode
	.quad	eckey_priv_print
	.quad	int_ec_size
	.quad	ec_bits
	.quad	ec_security_bits
	.quad	eckey_param_decode
	.quad	eckey_param_encode
	.quad	ec_missing_parameters
	.quad	ec_copy_parameters
	.quad	ec_cmp_parameters
	.quad	eckey_param_print
	.quad	0
	.quad	int_ec_free
	.quad	ec_pkey_ctrl
	.quad	old_ec_priv_decode
	.quad	old_ec_priv_encode
	.quad	0
	.quad	0
	.quad	0
	.quad	ec_pkey_check
	.quad	ec_pkey_public_check
	.quad	ec_pkey_param_check
	.zero	32
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
