	.file	"rsa_pss.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/rsa/rsa_pss.c"
	.text
	.p2align 4
	.globl	RSA_verify_PKCS1_PSS
	.type	RSA_verify_PKCS1_PSS, @function
RSA_verify_PKCS1_PSS:
.LFB445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$136, %rsp
	movq	%rsi, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L55
	movq	%r15, %rdi
	call	EVP_MD_size@PLT
	movl	%eax, %r8d
	testl	%eax, %eax
	js	.L55
	cmpl	$-1, %r13d
	je	.L33
	cmpl	$-3, %r13d
	jl	.L60
.L5:
	movq	24(%r14), %rdi
	movl	%r8d, -144(%rbp)
	call	BN_num_bits@PLT
	movq	%r14, %rdi
	movl	$255, %r14d
	subl	$1, %eax
	andl	$7, %eax
	movl	%eax, -136(%rbp)
	call	RSA_size@PLT
	movzbl	-136(%rbp), %ecx
	movzbl	(%rbx), %edx
	movl	-144(%rbp), %r8d
	sall	%cl, %r14d
	andl	%edx, %r14d
	jne	.L61
	movl	-136(%rbp), %edx
	testl	%edx, %edx
	jne	.L7
	addq	$1, %rbx
	subl	$1, %eax
.L7:
	leal	1(%r8), %edx
	cmpl	%eax, %edx
	jge	.L62
	movl	%eax, %r11d
	subl	%r8d, %r11d
	cmpl	$-3, %r13d
	je	.L63
	leal	-1(%r11), %edx
	cmpl	%edx, %r13d
	jge	.L64
	cltq
	cmpb	$-68, -1(%rbx,%rax)
	jne	.L65
.L11:
	leal	-1(%r11), %r10d
	movl	$93, %edx
	leaq	.LC0(%rip), %rsi
	movl	%r8d, -168(%rbp)
	movslq	%r10d, %rdi
	movl	%r11d, -160(%rbp)
	leaq	(%rbx,%rdi), %rax
	movl	%r10d, -156(%rbp)
	movq	%rdi, -144(%rbp)
	movq	%rax, -176(%rbp)
	call	CRYPTO_malloc@PLT
	movq	-144(%rbp), %rdi
	movl	-156(%rbp), %r10d
	testq	%rax, %rax
	movl	-160(%rbp), %r11d
	movl	-168(%rbp), %r8d
	movq	%rax, %r9
	je	.L66
	movslq	%r8d, %rax
	movq	%rdi, %rsi
	movq	-176(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r15, %r8
	movq	%rax, %rcx
	movq	%r9, -144(%rbp)
	movl	%r11d, -160(%rbp)
	movl	%r10d, -156(%rbp)
	movq	%rax, -168(%rbp)
	call	PKCS1_MGF1@PLT
	movq	-144(%rbp), %r9
	testl	%eax, %eax
	js	.L3
	movl	-156(%rbp), %r10d
	movl	-160(%rbp), %r11d
	testl	%r10d, %r10d
	jle	.L19
	leaq	15(%rbx), %rdx
	leal	-2(%r11), %eax
	subq	%r9, %rdx
	cmpq	$30, %rdx
	jbe	.L16
	cmpl	$14, %eax
	jbe	.L16
	movl	%r10d, %edx
	xorl	%eax, %eax
	shrl	$4, %edx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L17:
	movdqu	(%r9,%rax), %xmm0
	movdqu	(%rbx,%rax), %xmm1
	pxor	%xmm1, %xmm0
	movups	%xmm0, (%r9,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L17
	movl	%r10d, %eax
	andl	$-16, %eax
	testb	$15, %r10b
	je	.L19
	movslq	%eax, %rdx
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r9,%rdx)
	leal	1(%rax), %edx
	cmpl	%edx, %r10d
	jle	.L19
	movslq	%edx, %rdx
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r9,%rdx)
	leal	2(%rax), %edx
	cmpl	%edx, %r10d
	jle	.L19
	movslq	%edx, %rdx
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r9,%rdx)
	leal	3(%rax), %edx
	cmpl	%edx, %r10d
	jle	.L19
	movslq	%edx, %rdx
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r9,%rdx)
	leal	4(%rax), %edx
	cmpl	%edx, %r10d
	jle	.L19
	movslq	%edx, %rdx
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r9,%rdx)
	leal	5(%rax), %edx
	cmpl	%edx, %r10d
	jle	.L19
	movslq	%edx, %rdx
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r9,%rdx)
	leal	6(%rax), %edx
	cmpl	%edx, %r10d
	jle	.L19
	movslq	%edx, %rdx
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r9,%rdx)
	leal	7(%rax), %edx
	cmpl	%edx, %r10d
	jle	.L19
	movslq	%edx, %rdx
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r9,%rdx)
	leal	8(%rax), %edx
	cmpl	%edx, %r10d
	jle	.L19
	movslq	%edx, %rdx
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r9,%rdx)
	leal	9(%rax), %edx
	cmpl	%edx, %r10d
	jle	.L19
	movslq	%edx, %rdx
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r9,%rdx)
	leal	10(%rax), %edx
	cmpl	%edx, %r10d
	jle	.L19
	movslq	%edx, %rdx
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r9,%rdx)
	leal	11(%rax), %edx
	cmpl	%edx, %r10d
	jle	.L19
	movslq	%edx, %rdx
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r9,%rdx)
	leal	12(%rax), %edx
	cmpl	%edx, %r10d
	jle	.L19
	movslq	%edx, %rdx
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r9,%rdx)
	leal	13(%rax), %edx
	cmpl	%edx, %r10d
	jle	.L19
	movslq	%edx, %rdx
	addl	$14, %eax
	movzbl	(%rbx,%rdx), %ecx
	xorb	%cl, (%r9,%rdx)
	cmpl	%eax, %r10d
	jle	.L19
	cltq
	movzbl	(%rbx,%rax), %edx
	xorb	%dl, (%r9,%rax)
	.p2align 4,,10
	.p2align 3
.L19:
	movl	-136(%rbp), %eax
	movzbl	(%r9), %edx
	testl	%eax, %eax
	jne	.L14
.L15:
	testb	%dl, %dl
	jne	.L34
	cmpl	$2, %r11d
	jle	.L24
	leal	-3(%r11), %ecx
	movl	$1, %eax
	addq	$2, %rcx
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L25:
	addq	$1, %rax
	cmpq	%rcx, %rax
	je	.L24
.L26:
	movzbl	(%r9,%rax), %edx
	movl	%eax, %ebx
	testb	%dl, %dl
	je	.L25
	addl	$1, %ebx
.L23:
	cmpb	$1, %dl
	jne	.L24
	movq	%r9, -136(%rbp)
	cmpl	$-2, %r13d
	je	.L28
	movl	%r10d, %eax
	subl	%ebx, %eax
	cmpl	%eax, %r13d
	jne	.L67
.L28:
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%r10d, -144(%rbp)
	call	EVP_DigestInit_ex@PLT
	movq	-136(%rbp), %r9
	testl	%eax, %eax
	je	.L56
	movl	$8, %edx
	leaq	zeroes(%rip), %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	movq	-136(%rbp), %r9
	testl	%eax, %eax
	je	.L56
	movq	-168(%rbp), %rdx
	movq	-152(%rbp), %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	movq	-136(%rbp), %r9
	testl	%eax, %eax
	je	.L56
	movl	-144(%rbp), %r10d
	cmpl	%ebx, %r10d
	je	.L31
	subl	%ebx, %r10d
	movslq	%ebx, %rsi
	movq	%r12, %rdi
	movq	%r9, -136(%rbp)
	addq	%r9, %rsi
	movslq	%r10d, %rdx
	call	EVP_DigestUpdate@PLT
	movq	-136(%rbp), %r9
	testl	%eax, %eax
	je	.L56
.L31:
	leaq	-128(%rbp), %r13
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r9, -136(%rbp)
	movq	%r13, %rsi
	call	EVP_DigestFinal_ex@PLT
	movq	-136(%rbp), %r9
	testl	%eax, %eax
	je	.L56
	movq	-168(%rbp), %rdx
	movq	-176(%rbp), %rsi
	movq	%r13, %rdi
	movq	%r9, -136(%rbp)
	call	memcmp@PLT
	movq	-136(%rbp), %r9
	testl	%eax, %eax
	jne	.L68
	movl	$1, %r14d
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L61:
	movl	$70, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$133, %edx
.L57:
	movl	$126, %esi
	movl	$4, %edi
	call	ERR_put_error@PLT
.L55:
	xorl	%r9d, %r9d
.L56:
	xorl	%r14d, %r14d
.L3:
	movq	%r9, %rdi
	movl	$131, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	call	EVP_MD_CTX_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L69
	addq	$136, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	movl	$88, %r8d
	movl	$134, %edx
	movl	$126, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r9d, %r9d
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L33:
	movl	%eax, %r13d
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L63:
	cltq
	leal	-2(%r11), %r13d
	cmpb	$-68, -1(%rbx,%rax)
	je	.L11
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L60:
	movl	$63, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$136, %edx
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L62:
	movl	$78, %r8d
.L58:
	movl	$109, %edx
	movl	$126, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r9d, %r9d
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L64:
	movl	$84, %r8d
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L14:
	movl	$8, %ecx
	movl	$255, %eax
	subl	-136(%rbp), %ecx
	sarl	%cl, %eax
	andl	%eax, %edx
	movb	%dl, (%r9)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L24:
	movl	$106, %r8d
	movl	$135, %edx
	movl	$126, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	movq	%r9, -136(%rbp)
	call	ERR_put_error@PLT
	movq	-136(%rbp), %r9
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L66:
	movl	$95, %r8d
	movl	$65, %edx
	movl	$126, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	movq	%rax, -136(%rbp)
	call	ERR_put_error@PLT
	movq	-136(%rbp), %r9
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L16:
	movl	%eax, %edx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L21:
	movzbl	(%rbx,%rax), %ecx
	xorb	%cl, (%r9,%rax)
	movq	%rax, %rcx
	addq	$1, %rax
	cmpq	%rdx, %rcx
	jne	.L21
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L67:
	movl	$110, %r8d
	movl	$136, %edx
	movl	$126, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-136(%rbp), %r9
	jmp	.L3
.L34:
	movl	$1, %ebx
	jmp	.L23
.L68:
	movl	$124, %r8d
	movl	$104, %edx
	movl	$126, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-136(%rbp), %r9
	jmp	.L3
.L69:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE445:
	.size	RSA_verify_PKCS1_PSS, .-RSA_verify_PKCS1_PSS
	.p2align 4
	.globl	RSA_verify_PKCS1_PSS_mgf1
	.type	RSA_verify_PKCS1_PSS_mgf1, @function
RSA_verify_PKCS1_PSS_mgf1:
.LFB446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$152, %rsp
	movq	%rsi, -152(%rbp)
	movl	%r9d, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L125
	testq	%rbx, %rbx
	movq	%r14, %rdi
	cmove	%r14, %rbx
	call	EVP_MD_size@PLT
	movl	%eax, %r8d
	testl	%eax, %eax
	js	.L125
	movl	-136(%rbp), %eax
	cmpl	$-1, %eax
	je	.L103
	cmpl	$-3, %eax
	jl	.L129
.L75:
	movq	24(%r15), %rdi
	movl	%r8d, -144(%rbp)
	call	BN_num_bits@PLT
	movq	%r15, %rdi
	movl	$255, %r15d
	subl	$1, %eax
	andl	$7, %eax
	movl	%eax, -140(%rbp)
	call	RSA_size@PLT
	movzbl	-140(%rbp), %ecx
	movzbl	(%r12), %edx
	movl	-144(%rbp), %r8d
	sall	%cl, %r15d
	andl	%edx, %r15d
	jne	.L130
	movl	-140(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L77
	addq	$1, %r12
	subl	$1, %eax
.L77:
	leal	1(%r8), %edx
	cmpl	%eax, %edx
	jge	.L131
	movl	%eax, %r10d
	subl	%r8d, %r10d
	cmpl	$-3, -136(%rbp)
	je	.L132
	leal	-1(%r10), %edx
	cmpl	-136(%rbp), %edx
	jle	.L133
	cltq
	cmpb	$-68, -1(%r12,%rax)
	jne	.L134
.L81:
	leal	-1(%r10), %eax
	movl	$93, %edx
	leaq	.LC0(%rip), %rsi
	movl	%r8d, -176(%rbp)
	movslq	%eax, %r11
	movl	%eax, -144(%rbp)
	movq	%r11, %rdi
	leaq	(%r12,%r11), %rax
	movl	%r10d, -164(%rbp)
	movq	%r11, -160(%rbp)
	movq	%rax, -184(%rbp)
	call	CRYPTO_malloc@PLT
	movq	-160(%rbp), %r11
	movl	-164(%rbp), %r10d
	testq	%rax, %rax
	movl	-176(%rbp), %r8d
	movq	%rax, %r9
	je	.L135
	movslq	%r8d, %rax
	movq	-184(%rbp), %rdx
	movq	%r9, %rdi
	movq	%rbx, %r8
	movq	%rax, %rcx
	movq	%r11, %rsi
	movq	%r9, -160(%rbp)
	movl	%r10d, -164(%rbp)
	movq	%rax, -176(%rbp)
	call	PKCS1_MGF1@PLT
	movq	-160(%rbp), %r9
	testl	%eax, %eax
	js	.L72
	movl	-144(%rbp), %edx
	movl	-164(%rbp), %r10d
	testl	%edx, %edx
	jle	.L89
	leaq	15(%r12), %rdx
	leal	-2(%r10), %eax
	subq	%r9, %rdx
	cmpq	$30, %rdx
	jbe	.L86
	cmpl	$14, %eax
	jbe	.L86
	movl	-144(%rbp), %edx
	xorl	%eax, %eax
	shrl	$4, %edx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L87:
	movdqu	(%r9,%rax), %xmm0
	movdqu	(%r12,%rax), %xmm1
	pxor	%xmm1, %xmm0
	movups	%xmm0, (%r9,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L87
	movl	-144(%rbp), %ebx
	movl	%ebx, %eax
	andl	$-16, %eax
	testb	$15, %bl
	je	.L89
	movslq	%eax, %rdx
	movzbl	(%r12,%rdx), %ecx
	xorb	%cl, (%r9,%rdx)
	leal	1(%rax), %edx
	cmpl	%edx, %ebx
	jle	.L89
	movslq	%edx, %rdx
	movzbl	(%r12,%rdx), %ecx
	xorb	%cl, (%r9,%rdx)
	leal	2(%rax), %edx
	cmpl	%edx, %ebx
	jle	.L89
	movslq	%edx, %rdx
	movzbl	(%r12,%rdx), %ecx
	xorb	%cl, (%r9,%rdx)
	leal	3(%rax), %edx
	cmpl	%edx, %ebx
	jle	.L89
	movslq	%edx, %rdx
	movzbl	(%r12,%rdx), %ecx
	xorb	%cl, (%r9,%rdx)
	leal	4(%rax), %edx
	cmpl	%edx, %ebx
	jle	.L89
	movslq	%edx, %rdx
	movzbl	(%r12,%rdx), %ecx
	xorb	%cl, (%r9,%rdx)
	leal	5(%rax), %edx
	cmpl	%edx, %ebx
	jle	.L89
	movslq	%edx, %rdx
	movzbl	(%r12,%rdx), %ecx
	xorb	%cl, (%r9,%rdx)
	leal	6(%rax), %edx
	cmpl	%edx, %ebx
	jle	.L89
	movslq	%edx, %rdx
	movzbl	(%r12,%rdx), %ecx
	xorb	%cl, (%r9,%rdx)
	leal	7(%rax), %edx
	cmpl	%edx, %ebx
	jle	.L89
	movslq	%edx, %rdx
	movzbl	(%r12,%rdx), %ecx
	xorb	%cl, (%r9,%rdx)
	leal	8(%rax), %edx
	cmpl	%edx, %ebx
	jle	.L89
	movslq	%edx, %rdx
	movzbl	(%r12,%rdx), %ecx
	xorb	%cl, (%r9,%rdx)
	leal	9(%rax), %edx
	cmpl	%edx, %ebx
	jle	.L89
	movslq	%edx, %rdx
	movzbl	(%r12,%rdx), %ecx
	xorb	%cl, (%r9,%rdx)
	leal	10(%rax), %edx
	cmpl	%edx, %ebx
	jle	.L89
	movslq	%edx, %rdx
	movzbl	(%r12,%rdx), %ecx
	xorb	%cl, (%r9,%rdx)
	leal	11(%rax), %edx
	cmpl	%edx, %ebx
	jle	.L89
	movslq	%edx, %rdx
	movzbl	(%r12,%rdx), %ecx
	xorb	%cl, (%r9,%rdx)
	leal	12(%rax), %edx
	cmpl	%edx, %ebx
	jle	.L89
	movslq	%edx, %rdx
	movzbl	(%r12,%rdx), %ecx
	xorb	%cl, (%r9,%rdx)
	leal	13(%rax), %edx
	cmpl	%edx, %ebx
	jle	.L89
	movslq	%edx, %rdx
	addl	$14, %eax
	movzbl	(%r12,%rdx), %ecx
	xorb	%cl, (%r9,%rdx)
	cmpl	%eax, %ebx
	jle	.L89
	cltq
	movzbl	(%r12,%rax), %edx
	xorb	%dl, (%r9,%rax)
	.p2align 4,,10
	.p2align 3
.L89:
	movl	-140(%rbp), %eax
	movzbl	(%r9), %edx
	testl	%eax, %eax
	jne	.L84
.L85:
	testb	%dl, %dl
	jne	.L104
	cmpl	$2, %r10d
	jle	.L94
	leal	-3(%r10), %ecx
	movl	$1, %eax
	addq	$2, %rcx
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L95:
	addq	$1, %rax
	cmpq	%rcx, %rax
	je	.L94
.L96:
	movzbl	(%r9,%rax), %edx
	movl	%eax, %ebx
	testb	%dl, %dl
	je	.L95
	addl	$1, %ebx
.L93:
	cmpb	$1, %dl
	jne	.L94
	movl	-136(%rbp), %esi
	cmpl	$-2, %esi
	je	.L98
	movl	-144(%rbp), %eax
	subl	%ebx, %eax
	cmpl	%esi, %eax
	jne	.L136
.L98:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%r9, -136(%rbp)
	call	EVP_DigestInit_ex@PLT
	movq	-136(%rbp), %r9
	testl	%eax, %eax
	je	.L126
	movl	$8, %edx
	leaq	zeroes(%rip), %rsi
	movq	%r13, %rdi
	call	EVP_DigestUpdate@PLT
	movq	-136(%rbp), %r9
	testl	%eax, %eax
	je	.L126
	movq	-176(%rbp), %rdx
	movq	-152(%rbp), %rsi
	movq	%r13, %rdi
	call	EVP_DigestUpdate@PLT
	movq	-136(%rbp), %r9
	testl	%eax, %eax
	je	.L126
	cmpl	%ebx, -144(%rbp)
	je	.L101
	movl	-144(%rbp), %edx
	movslq	%ebx, %rsi
	movq	%r13, %rdi
	movq	%r9, -136(%rbp)
	addq	%r9, %rsi
	subl	%ebx, %edx
	movslq	%edx, %rdx
	call	EVP_DigestUpdate@PLT
	movq	-136(%rbp), %r9
	testl	%eax, %eax
	je	.L126
.L101:
	leaq	-128(%rbp), %r12
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%r9, -136(%rbp)
	movq	%r12, %rsi
	call	EVP_DigestFinal_ex@PLT
	movq	-136(%rbp), %r9
	testl	%eax, %eax
	je	.L126
	movq	-176(%rbp), %rdx
	movq	-184(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r9, -136(%rbp)
	call	memcmp@PLT
	movq	-136(%rbp), %r9
	testl	%eax, %eax
	jne	.L137
	movl	$1, %r15d
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L130:
	movl	$70, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$133, %edx
.L127:
	movl	$126, %esi
	movl	$4, %edi
	call	ERR_put_error@PLT
.L125:
	xorl	%r9d, %r9d
.L126:
	xorl	%r15d, %r15d
.L72:
	movq	%r9, %rdi
	movl	$131, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r13, %rdi
	call	EVP_MD_CTX_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L138
	addq	$152, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	movl	$88, %r8d
	movl	$134, %edx
	movl	$126, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r9d, %r9d
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L103:
	movl	%r8d, -136(%rbp)
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L132:
	cltq
	leal	-2(%r10), %esi
	cmpb	$-68, -1(%r12,%rax)
	movl	%esi, -136(%rbp)
	je	.L81
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L129:
	movl	$63, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$136, %edx
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L131:
	movl	$78, %r8d
.L128:
	movl	$109, %edx
	movl	$126, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r9d, %r9d
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L133:
	movl	$84, %r8d
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L84:
	movl	$8, %ecx
	movl	$255, %eax
	subl	-140(%rbp), %ecx
	sarl	%cl, %eax
	andl	%eax, %edx
	movb	%dl, (%r9)
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L94:
	movl	$106, %r8d
	movl	$135, %edx
	movl	$126, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	movq	%r9, -136(%rbp)
	call	ERR_put_error@PLT
	movq	-136(%rbp), %r9
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L135:
	movl	$95, %r8d
	movl	$65, %edx
	movl	$126, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	movq	%rax, -136(%rbp)
	call	ERR_put_error@PLT
	movq	-136(%rbp), %r9
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L86:
	movl	%eax, %edx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L91:
	movzbl	(%r12,%rax), %ecx
	xorb	%cl, (%r9,%rax)
	movq	%rax, %rcx
	addq	$1, %rax
	cmpq	%rdx, %rcx
	jne	.L91
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L136:
	movl	$110, %r8d
	movl	$136, %edx
	movl	$126, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	movq	%r9, -136(%rbp)
	call	ERR_put_error@PLT
	movq	-136(%rbp), %r9
	jmp	.L72
.L104:
	movl	$1, %ebx
	jmp	.L93
.L137:
	movl	$124, %r8d
	movl	$104, %edx
	movl	$126, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-136(%rbp), %r9
	jmp	.L72
.L138:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE446:
	.size	RSA_verify_PKCS1_PSS_mgf1, .-RSA_verify_PKCS1_PSS_mgf1
	.p2align 4
	.globl	RSA_padding_add_PKCS1_PSS
	.type	RSA_padding_add_PKCS1_PSS, @function
RSA_padding_add_PKCS1_PSS:
.LFB447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rcx, %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$72, %rsp
	movq	%rsi, -64(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%rcx, -72(%rbp)
	call	EVP_MD_size@PLT
	testl	%eax, %eax
	js	.L195
	movl	%eax, %r12d
	cmpl	$-1, %ebx
	je	.L162
	cmpl	$-2, %ebx
	je	.L163
	cmpl	$-3, %ebx
	jl	.L197
.L142:
	movq	24(%r13), %rdi
	call	BN_num_bits@PLT
	movq	%r13, %rdi
	subl	$1, %eax
	andl	$7, %eax
	movl	%eax, -56(%rbp)
	movl	%eax, %r15d
	call	RSA_size@PLT
	movl	%eax, -52(%rbp)
	testl	%r15d, %r15d
	jne	.L143
	movq	-64(%rbp), %rsi
	subl	$1, %eax
	movl	%eax, -52(%rbp)
	leaq	1(%rsi), %rax
	movb	$0, (%rsi)
	movq	%rax, -64(%rbp)
.L143:
	leal	1(%r12), %eax
	movl	$185, %r8d
	movl	$110, %edx
	leaq	.LC0(%rip), %rcx
	cmpl	-52(%rbp), %eax
	jge	.L196
	movl	-52(%rbp), %eax
	subl	%r12d, %eax
	movl	%eax, -80(%rbp)
	cmpl	$-3, %ebx
	je	.L198
	subl	$1, %eax
	movslq	%ebx, %r15
	cmpl	%eax, %ebx
	jge	.L199
.L146:
	testl	%ebx, %ebx
	jle	.L164
	movl	$197, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L200
	movl	%ebx, %esi
	movq	%rax, %rdi
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jle	.L201
.L147:
	call	EVP_MD_CTX_new@PLT
	xorl	%r14d, %r14d
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L141
	movq	-72(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, %rdi
	movq	%rax, -96(%rbp)
	call	EVP_DigestInit_ex@PLT
	movq	-96(%rbp), %r9
	testl	%eax, %eax
	movl	%eax, %r14d
	jne	.L202
.L141:
	movq	%r9, %rdi
	call	EVP_MD_CTX_free@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	$247, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	addq	$72, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L197:
	.cfi_restore_state
	movl	$174, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$136, %edx
.L196:
	movl	$152, %esi
	movl	$4, %edi
	call	ERR_put_error@PLT
.L195:
	movslq	%ebx, %r15
	xorl	%r9d, %r9d
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L163:
	movl	$-3, %ebx
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L202:
	movq	%r9, %rdi
	movl	$8, %edx
	leaq	zeroes(%rip), %rsi
	call	EVP_DigestUpdate@PLT
	movq	-96(%rbp), %r9
	testl	%eax, %eax
	movl	%eax, %r14d
	je	.L141
	movslq	%r12d, %rax
	movq	-88(%rbp), %rsi
	movq	%r9, %rdi
	movq	%r9, -88(%rbp)
	movq	%rax, %rdx
	movq	%rax, -96(%rbp)
	call	EVP_DigestUpdate@PLT
	movq	-88(%rbp), %r9
	testl	%eax, %eax
	movl	%eax, %r14d
	je	.L141
	testl	%ebx, %ebx
	je	.L151
	movq	%r9, %rdi
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r9, -88(%rbp)
	call	EVP_DigestUpdate@PLT
	movq	-88(%rbp), %r9
	testl	%eax, %eax
	movl	%eax, %r14d
	je	.L141
.L151:
	movl	-80(%rbp), %r14d
	movq	-64(%rbp), %rax
	xorl	%edx, %edx
	movq	%r9, %rdi
	movq	%r9, -80(%rbp)
	subl	$1, %r14d
	movslq	%r14d, %r11
	leaq	(%rax,%r11), %r10
	movq	%r11, -104(%rbp)
	movq	%r10, %rsi
	movq	%r10, -88(%rbp)
	call	EVP_DigestFinal_ex@PLT
	movq	-80(%rbp), %r9
	testl	%eax, %eax
	movl	%eax, %r14d
	je	.L141
	movq	-88(%rbp), %r10
	movq	-104(%rbp), %r11
	movq	%r9, -80(%rbp)
	xorl	%r14d, %r14d
	movq	-72(%rbp), %r8
	movq	-96(%rbp), %rcx
	movq	-64(%rbp), %rdi
	movq	%r10, %rdx
	movq	%r11, %rsi
	call	PKCS1_MGF1@PLT
	movq	-80(%rbp), %r9
	testl	%eax, %eax
	jne	.L141
	movl	-52(%rbp), %eax
	movq	-64(%rbp), %rdx
	subl	%ebx, %eax
	subl	%r12d, %eax
	cltq
	leaq	-2(%rdx,%rax), %rcx
	xorb	$1, (%rcx)
	testl	%ebx, %ebx
	jle	.L158
	movq	%rdx, %rdi
	leaq	-1(%rdx,%rax), %rdx
	leal	-1(%rbx), %esi
	leaq	15(%rdi,%rax), %rax
	cmpq	%rax, %r13
	leaq	16(%r13), %rax
	setnb	%dil
	cmpq	%rax, %rdx
	setnb	%al
	orb	%al, %dil
	je	.L155
	cmpl	$14, %esi
	jbe	.L155
	movl	%ebx, %esi
	xorl	%eax, %eax
	shrl	$4, %esi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L156:
	movdqu	(%rdx,%rax), %xmm0
	movdqu	0(%r13,%rax), %xmm1
	pxor	%xmm1, %xmm0
	movups	%xmm0, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L156
	movl	%ebx, %edx
	andl	$-16, %edx
	movl	%edx, %esi
	leaq	1(%rcx,%rsi), %rax
	cmpl	%edx, %ebx
	je	.L158
	movzbl	0(%r13,%rsi), %ecx
	xorb	%cl, (%rax)
	leal	1(%rdx), %ecx
	cmpl	%ecx, %ebx
	jle	.L158
	movslq	%ecx, %rcx
	movzbl	0(%r13,%rcx), %ecx
	xorb	%cl, 1(%rax)
	leal	2(%rdx), %ecx
	cmpl	%ecx, %ebx
	jle	.L158
	movslq	%ecx, %rcx
	movzbl	0(%r13,%rcx), %ecx
	xorb	%cl, 2(%rax)
	leal	3(%rdx), %ecx
	cmpl	%ecx, %ebx
	jle	.L158
	movslq	%ecx, %rcx
	movzbl	0(%r13,%rcx), %ecx
	xorb	%cl, 3(%rax)
	leal	4(%rdx), %ecx
	cmpl	%ecx, %ebx
	jle	.L158
	movslq	%ecx, %rcx
	movzbl	0(%r13,%rcx), %ecx
	xorb	%cl, 4(%rax)
	leal	5(%rdx), %ecx
	cmpl	%ecx, %ebx
	jle	.L158
	movslq	%ecx, %rcx
	movzbl	0(%r13,%rcx), %ecx
	xorb	%cl, 5(%rax)
	leal	6(%rdx), %ecx
	cmpl	%ecx, %ebx
	jle	.L158
	movslq	%ecx, %rcx
	movzbl	0(%r13,%rcx), %ecx
	xorb	%cl, 6(%rax)
	leal	7(%rdx), %ecx
	cmpl	%ecx, %ebx
	jle	.L158
	movslq	%ecx, %rcx
	movzbl	0(%r13,%rcx), %ecx
	xorb	%cl, 7(%rax)
	leal	8(%rdx), %ecx
	cmpl	%ecx, %ebx
	jle	.L158
	movslq	%ecx, %rcx
	movzbl	0(%r13,%rcx), %ecx
	xorb	%cl, 8(%rax)
	leal	9(%rdx), %ecx
	cmpl	%ecx, %ebx
	jle	.L158
	movslq	%ecx, %rcx
	movzbl	0(%r13,%rcx), %ecx
	xorb	%cl, 9(%rax)
	leal	10(%rdx), %ecx
	cmpl	%ecx, %ebx
	jle	.L158
	movslq	%ecx, %rcx
	movzbl	0(%r13,%rcx), %ecx
	xorb	%cl, 10(%rax)
	leal	11(%rdx), %ecx
	cmpl	%ecx, %ebx
	jle	.L158
	movslq	%ecx, %rcx
	movzbl	0(%r13,%rcx), %ecx
	xorb	%cl, 11(%rax)
	leal	12(%rdx), %ecx
	cmpl	%ecx, %ebx
	jle	.L158
	movslq	%ecx, %rcx
	movzbl	0(%r13,%rcx), %ecx
	xorb	%cl, 12(%rax)
	leal	13(%rdx), %ecx
	cmpl	%ecx, %ebx
	jle	.L158
	movslq	%ecx, %rcx
	addl	$14, %edx
	movzbl	0(%r13,%rcx), %ecx
	xorb	%cl, 13(%rax)
	cmpl	%edx, %ebx
	jle	.L158
	movslq	%edx, %rdx
	movzbl	0(%r13,%rdx), %edx
	xorb	%dl, 14(%rax)
	.p2align 4,,10
	.p2align 3
.L158:
	movl	-56(%rbp), %eax
	testl	%eax, %eax
	je	.L154
	movq	-64(%rbp), %rbx
	movl	$8, %ecx
	movl	$255, %eax
	subl	-56(%rbp), %ecx
	sarl	%cl, %eax
	andb	%al, (%rbx)
.L154:
	movslq	-52(%rbp), %rax
	movq	-64(%rbp), %rbx
	movl	$1, %r14d
	movb	$-68, -1(%rbx,%rax)
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L201:
	xorl	%r9d, %r9d
	xorl	%r14d, %r14d
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L164:
	xorl	%r13d, %r13d
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L198:
	leal	-2(%rax), %ebx
	movslq	%ebx, %r15
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L162:
	movl	%eax, %ebx
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L199:
	movl	$110, %edx
	movl	$152, %esi
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	movl	$192, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$4, %edi
	call	ERR_put_error@PLT
	xorl	%r9d, %r9d
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L200:
	movl	$199, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r14d, %r14d
	movl	$152, %esi
	movl	$4, %edi
	call	ERR_put_error@PLT
	xorl	%r9d, %r9d
	jmp	.L141
.L155:
	movl	%esi, %edx
	movl	$1, %eax
	addq	$2, %rdx
.L160:
	movzbl	-1(%r13,%rax), %esi
	xorb	%sil, (%rcx,%rax)
	addq	$1, %rax
	cmpq	%rdx, %rax
	jne	.L160
	jmp	.L158
	.cfi_endproc
.LFE447:
	.size	RSA_padding_add_PKCS1_PSS, .-RSA_padding_add_PKCS1_PSS
	.p2align 4
	.globl	RSA_padding_add_PKCS1_PSS_mgf1
	.type	RSA_padding_add_PKCS1_PSS_mgf1, @function
RSA_padding_add_PKCS1_PSS_mgf1:
.LFB448:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rcx, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r9d, %ebx
	subq	$72, %rsp
	testq	%r8, %r8
	movq	%rsi, -72(%rbp)
	movq	%rdx, -88(%rbp)
	cmove	%rcx, %r12
	call	EVP_MD_size@PLT
	movl	%eax, -52(%rbp)
	testl	%eax, %eax
	js	.L260
	cmpl	$-1, %ebx
	je	.L227
	cmpl	$-2, %ebx
	je	.L228
	cmpl	$-3, %ebx
	jl	.L262
.L207:
	movq	24(%r14), %rdi
	call	BN_num_bits@PLT
	movq	%r14, %rdi
	subl	$1, %eax
	andl	$7, %eax
	movl	%eax, -92(%rbp)
	movl	%eax, %r15d
	call	RSA_size@PLT
	movl	%eax, -56(%rbp)
	testl	%r15d, %r15d
	jne	.L208
	movq	-72(%rbp), %rsi
	subl	$1, %eax
	movl	%eax, -56(%rbp)
	leaq	1(%rsi), %rax
	movb	$0, (%rsi)
	movq	%rax, -72(%rbp)
.L208:
	movl	-52(%rbp), %eax
	movl	$185, %r8d
	movl	$110, %edx
	leaq	.LC0(%rip), %rcx
	addl	$1, %eax
	cmpl	-56(%rbp), %eax
	jge	.L261
	movl	-56(%rbp), %eax
	subl	-52(%rbp), %eax
	movl	%eax, -80(%rbp)
	cmpl	$-3, %ebx
	je	.L263
	movslq	%ebx, %rax
	movq	%rax, -64(%rbp)
	movl	-80(%rbp), %eax
	subl	$1, %eax
	cmpl	%ebx, %eax
	jle	.L264
.L211:
	testl	%ebx, %ebx
	jle	.L229
	movq	-64(%rbp), %rdi
	movl	$197, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L265
	movl	%ebx, %esi
	movq	%rax, %rdi
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jle	.L266
.L212:
	call	EVP_MD_CTX_new@PLT
	xorl	%r15d, %r15d
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L206
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -104(%rbp)
	call	EVP_DigestInit_ex@PLT
	movq	-104(%rbp), %r9
	testl	%eax, %eax
	movl	%eax, %r15d
	jne	.L267
.L206:
	movq	%r9, %rdi
	call	EVP_MD_CTX_free@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	movl	$247, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	addq	$72, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L262:
	.cfi_restore_state
	movl	$174, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$136, %edx
.L261:
	movl	$152, %esi
	movl	$4, %edi
	call	ERR_put_error@PLT
.L260:
	movslq	%ebx, %rax
	xorl	%r9d, %r9d
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	movq	%rax, -64(%rbp)
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L228:
	movl	$-3, %ebx
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L267:
	movq	%r9, %rdi
	movl	$8, %edx
	leaq	zeroes(%rip), %rsi
	call	EVP_DigestUpdate@PLT
	movq	-104(%rbp), %r9
	testl	%eax, %eax
	movl	%eax, %r15d
	je	.L206
	movslq	-52(%rbp), %r13
	movq	-88(%rbp), %rsi
	movq	%r9, %rdi
	movq	%r9, -88(%rbp)
	movq	%r13, %rdx
	call	EVP_DigestUpdate@PLT
	movq	-88(%rbp), %r9
	testl	%eax, %eax
	movl	%eax, %r15d
	je	.L206
	testl	%ebx, %ebx
	je	.L216
	movq	-64(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r14, %rsi
	movq	%r9, -88(%rbp)
	call	EVP_DigestUpdate@PLT
	movq	-88(%rbp), %r9
	testl	%eax, %eax
	movl	%eax, %r15d
	je	.L206
.L216:
	movl	-80(%rbp), %r15d
	movq	-72(%rbp), %rax
	xorl	%edx, %edx
	movq	%r9, %rdi
	movq	%r9, -80(%rbp)
	subl	$1, %r15d
	movslq	%r15d, %r11
	leaq	(%rax,%r11), %r10
	movq	%r11, -104(%rbp)
	movq	%r10, %rsi
	movq	%r10, -88(%rbp)
	call	EVP_DigestFinal_ex@PLT
	movq	-80(%rbp), %r9
	testl	%eax, %eax
	movl	%eax, %r15d
	je	.L206
	movq	-88(%rbp), %r10
	movq	-104(%rbp), %r11
	movq	%r12, %r8
	movq	%r13, %rcx
	movq	-72(%rbp), %r12
	movq	%r9, -80(%rbp)
	xorl	%r15d, %r15d
	movq	%r10, %rdx
	movq	%r11, %rsi
	movq	%r12, %rdi
	call	PKCS1_MGF1@PLT
	movq	-80(%rbp), %r9
	testl	%eax, %eax
	jne	.L206
	movl	-56(%rbp), %eax
	subl	%ebx, %eax
	subl	-52(%rbp), %eax
	cltq
	leaq	-2(%r12,%rax), %rcx
	xorb	$1, (%rcx)
	testl	%ebx, %ebx
	jle	.L223
	movq	-72(%rbp), %rdi
	leal	-1(%rbx), %esi
	leaq	-1(%rdi,%rax), %rdx
	leaq	15(%rdi,%rax), %rax
	cmpq	%rax, %r14
	leaq	16(%r14), %rax
	setnb	%dil
	cmpq	%rax, %rdx
	setnb	%al
	orb	%al, %dil
	je	.L220
	cmpl	$14, %esi
	jbe	.L220
	movl	%ebx, %esi
	xorl	%eax, %eax
	shrl	$4, %esi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L221:
	movdqu	(%rdx,%rax), %xmm0
	movdqu	(%r14,%rax), %xmm1
	pxor	%xmm1, %xmm0
	movups	%xmm0, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L221
	movl	%ebx, %edx
	andl	$-16, %edx
	movl	%edx, %esi
	leaq	1(%rcx,%rsi), %rax
	cmpl	%edx, %ebx
	je	.L223
	movzbl	(%r14,%rsi), %ecx
	xorb	%cl, (%rax)
	leal	1(%rdx), %ecx
	cmpl	%ecx, %ebx
	jle	.L223
	movslq	%ecx, %rcx
	movzbl	(%r14,%rcx), %ecx
	xorb	%cl, 1(%rax)
	leal	2(%rdx), %ecx
	cmpl	%ecx, %ebx
	jle	.L223
	movslq	%ecx, %rcx
	movzbl	(%r14,%rcx), %ecx
	xorb	%cl, 2(%rax)
	leal	3(%rdx), %ecx
	cmpl	%ecx, %ebx
	jle	.L223
	movslq	%ecx, %rcx
	movzbl	(%r14,%rcx), %ecx
	xorb	%cl, 3(%rax)
	leal	4(%rdx), %ecx
	cmpl	%ecx, %ebx
	jle	.L223
	movslq	%ecx, %rcx
	movzbl	(%r14,%rcx), %ecx
	xorb	%cl, 4(%rax)
	leal	5(%rdx), %ecx
	cmpl	%ecx, %ebx
	jle	.L223
	movslq	%ecx, %rcx
	movzbl	(%r14,%rcx), %ecx
	xorb	%cl, 5(%rax)
	leal	6(%rdx), %ecx
	cmpl	%ecx, %ebx
	jle	.L223
	movslq	%ecx, %rcx
	movzbl	(%r14,%rcx), %ecx
	xorb	%cl, 6(%rax)
	leal	7(%rdx), %ecx
	cmpl	%ecx, %ebx
	jle	.L223
	movslq	%ecx, %rcx
	movzbl	(%r14,%rcx), %ecx
	xorb	%cl, 7(%rax)
	leal	8(%rdx), %ecx
	cmpl	%ecx, %ebx
	jle	.L223
	movslq	%ecx, %rcx
	movzbl	(%r14,%rcx), %ecx
	xorb	%cl, 8(%rax)
	leal	9(%rdx), %ecx
	cmpl	%ecx, %ebx
	jle	.L223
	movslq	%ecx, %rcx
	movzbl	(%r14,%rcx), %ecx
	xorb	%cl, 9(%rax)
	leal	10(%rdx), %ecx
	cmpl	%ecx, %ebx
	jle	.L223
	movslq	%ecx, %rcx
	movzbl	(%r14,%rcx), %ecx
	xorb	%cl, 10(%rax)
	leal	11(%rdx), %ecx
	cmpl	%ecx, %ebx
	jle	.L223
	movslq	%ecx, %rcx
	movzbl	(%r14,%rcx), %ecx
	xorb	%cl, 11(%rax)
	leal	12(%rdx), %ecx
	cmpl	%ecx, %ebx
	jle	.L223
	movslq	%ecx, %rcx
	movzbl	(%r14,%rcx), %ecx
	xorb	%cl, 12(%rax)
	leal	13(%rdx), %ecx
	cmpl	%ecx, %ebx
	jle	.L223
	movslq	%ecx, %rcx
	addl	$14, %edx
	movzbl	(%r14,%rcx), %ecx
	xorb	%cl, 13(%rax)
	cmpl	%edx, %ebx
	jle	.L223
	movslq	%edx, %rdx
	movzbl	(%r14,%rdx), %edx
	xorb	%dl, 14(%rax)
	.p2align 4,,10
	.p2align 3
.L223:
	movl	-92(%rbp), %eax
	testl	%eax, %eax
	je	.L219
	movq	-72(%rbp), %rbx
	movl	$8, %ecx
	movl	$255, %eax
	subl	-92(%rbp), %ecx
	sarl	%cl, %eax
	andb	%al, (%rbx)
.L219:
	movslq	-56(%rbp), %rax
	movq	-72(%rbp), %rbx
	movl	$1, %r15d
	movb	$-68, -1(%rbx,%rax)
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L266:
	xorl	%r9d, %r9d
	xorl	%r15d, %r15d
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L229:
	xorl	%r14d, %r14d
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L263:
	leal	-2(%rax), %ebx
	movslq	%ebx, %rax
	movq	%rax, -64(%rbp)
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L227:
	movl	%eax, %ebx
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L264:
	movl	$110, %edx
	movl	$152, %esi
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	movl	$192, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$4, %edi
	call	ERR_put_error@PLT
	xorl	%r9d, %r9d
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L265:
	movl	$199, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r15d, %r15d
	movl	$152, %esi
	movl	$4, %edi
	call	ERR_put_error@PLT
	xorl	%r9d, %r9d
	jmp	.L206
.L220:
	movl	%esi, %edx
	movl	$1, %eax
	addq	$2, %rdx
.L225:
	movzbl	-1(%r14,%rax), %esi
	xorb	%sil, (%rcx,%rax)
	addq	$1, %rax
	cmpq	%rdx, %rax
	jne	.L225
	jmp	.L223
	.cfi_endproc
.LFE448:
	.size	RSA_padding_add_PKCS1_PSS_mgf1, .-RSA_padding_add_PKCS1_PSS_mgf1
	.section	.rodata
	.align 8
	.type	zeroes, @object
	.size	zeroes, 8
zeroes:
	.zero	8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
