	.file	"bss_dgram.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/bio/bss_dgram.c"
	.text
	.p2align 4
	.type	dgram_new, @function
dgram_new:
.LFB269:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$155, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$168, %edi
	subq	$8, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L3
	movq	%rax, 56(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE269:
	.size	dgram_new, .-dgram_new
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"getsockopt"
	.section	.rodata.str1.8
	.align 8
.LC2:
	.string	"assertion failed: sz.s <= sizeof(data->socket_timeout)"
	.section	.rodata.str1.1
.LC3:
	.string	"setsockopt"
	.text
	.p2align 4
	.type	dgram_read, @function
dgram_read:
.LFB274:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	56(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$112, -220(%rbp)
	testq	%rsi, %rsi
	je	.L32
	movq	%rdi, %rbx
	movq	%rsi, %r14
	leaq	-176(%rbp), %r15
	movl	%edx, %r13d
	call	__errno_location@PLT
	movl	$14, %ecx
	movq	%r15, %rdi
	movq	$0, -216(%rbp)
	movq	%rax, -240(%rbp)
	movl	$0, (%rax)
	xorl	%eax, %eax
	cmpq	$0, 128(%r12)
	rep stosq
	jg	.L8
	cmpq	$0, 136(%r12)
	jle	.L9
.L8:
	movl	48(%rbx), %edi
	movl	$20, %edx
	movl	$1, %esi
	leaq	144(%r12), %rcx
	leaq	-216(%rbp), %r8
	movl	$16, -216(%rbp)
	call	getsockopt@PLT
	testl	%eax, %eax
	js	.L44
	movl	-216(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L45
.L11:
	leaq	-208(%rbp), %rdi
	xorl	%esi, %esi
	call	gettimeofday@PLT
	movq	136(%r12), %rax
	movq	-200(%rbp), %rcx
	movq	128(%r12), %rdx
	cmpq	%rcx, %rax
	jge	.L12
	subq	%rcx, %rax
	subq	$1, %rdx
	addq	$1000000, %rax
.L13:
	movq	-208(%rbp), %rsi
	movq	%rdx, %rcx
	movq	%rax, -184(%rbp)
	subq	%rsi, %rcx
	cmpq	%rdx, %rsi
	jle	.L15
	movq	$1, -184(%rbp)
	movl	$1, %eax
	xorl	%ecx, %ecx
.L15:
	movq	144(%r12), %rdx
	movq	%rcx, -192(%rbp)
	testq	%rdx, %rdx
	jne	.L16
	cmpq	$0, 152(%r12)
	je	.L17
.L18:
	cmpq	%rdx, %rcx
	jne	.L9
	cmpq	%rax, 152(%r12)
	jge	.L17
	.p2align 4,,10
	.p2align 3
.L9:
	movl	160(%r12), %eax
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	testl	%eax, %eax
	setne	%cl
	addl	%ecx, %ecx
	movl	%ecx, -228(%rbp)
	movl	%ecx, -232(%rbp)
	call	BIO_ADDR_sockaddr_noconst@PLT
	movl	48(%rbx), %edi
	movslq	%r13d, %rdx
	movq	%r14, %rsi
	movl	-228(%rbp), %ecx
	movq	%rax, %r8
	leaq	-220(%rbp), %r9
	call	recvfrom@PLT
	movl	112(%r12), %edx
	movq	%rax, %r13
	movl	%eax, %r14d
	testl	%edx, %edx
	jne	.L23
	testl	%eax, %eax
	jns	.L46
	movl	$15, %esi
	movq	%rbx, %rdi
	call	BIO_clear_flags@PLT
.L26:
	addl	$1, %r13d
	cmpl	$1, %r13d
	ja	.L25
	movq	-240(%rbp), %rax
	movl	(%rax), %eax
	cmpl	$71, %eax
	je	.L28
	jg	.L29
	cmpl	$4, %eax
	je	.L28
	cmpl	$11, %eax
	jne	.L25
.L28:
	movl	$9, %esi
	movq	%rbx, %rdi
	call	BIO_set_flags@PLT
	movq	-240(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, 116(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L16:
	cmpq	%rdx, %rcx
	jge	.L18
.L17:
	movl	48(%rbx), %edi
	movl	$16, %r8d
	movl	$20, %edx
	leaq	-192(%rbp), %rcx
	movl	$1, %esi
	call	setsockopt@PLT
	testl	%eax, %eax
	jns	.L9
	leaq	.LC3(%rip), %rdi
	call	perror@PLT
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L32:
	xorl	%r14d, %r14d
.L6:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L47
	addq	$200, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	movl	$15, %esi
	movq	%rbx, %rdi
	call	BIO_clear_flags@PLT
	testl	%r13d, %r13d
	js	.L26
.L25:
	movq	56(%rbx), %rcx
	cmpq	$0, 128(%rcx)
	jg	.L30
	cmpq	$0, 136(%rcx)
	jle	.L6
.L30:
	movl	48(%rbx), %edi
	movl	$16, %r8d
	movl	$20, %edx
	addq	$144, %rcx
	movl	$1, %esi
	call	setsockopt@PLT
	testl	%eax, %eax
	jns	.L6
	leaq	.LC3(%rip), %rdi
	call	perror@PLT
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L12:
	subq	%rcx, %rax
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L45:
	cmpq	$16, -216(%rbp)
	jbe	.L11
	movl	$225, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	OPENSSL_die@PLT
	.p2align 4,,10
	.p2align 3
.L46:
	movq	%rbx, %rdi
	movq	%r15, %rcx
	xorl	%edx, %edx
	movl	$44, %esi
	call	BIO_ctrl@PLT
	movl	$15, %esi
	movq	%rbx, %rdi
	call	BIO_clear_flags@PLT
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L29:
	subl	$114, %eax
	cmpl	$1, %eax
	ja	.L25
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L44:
	leaq	.LC1(%rip), %rdi
	call	perror@PLT
	jmp	.L11
.L47:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE274:
	.size	dgram_read, .-dgram_read
	.p2align 4
	.type	dgram_free, @function
dgram_free:
.LFB270:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L52
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	36(%rdi), %edx
	testl	%edx, %edx
	je	.L50
	movl	32(%rdi), %eax
	testl	%eax, %eax
	jne	.L57
.L51:
	movl	$0, 32(%rbx)
	movl	$0, 40(%rbx)
.L50:
	movq	56(%rbx), %rdi
	movl	$173, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	48(%rdi), %edi
	call	BIO_closesocket@PLT
	jmp	.L51
	.cfi_endproc
.LFE270:
	.size	dgram_free, .-dgram_free
	.p2align 4
	.type	dgram_write, @function
dgram_write:
.LFB275:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movslq	%edx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	56(%rdi), %r15
	movq	%rsi, -56(%rbp)
	call	__errno_location@PLT
	movq	-56(%rbp), %rsi
	movl	$0, (%rax)
	movq	%rax, %r14
	movl	112(%r15), %eax
	testl	%eax, %eax
	je	.L59
	movl	48(%r12), %edi
	movq	%r13, %rdx
	call	write@PLT
	movl	%eax, %r13d
.L60:
	movl	$15, %esi
	movq	%r12, %rdi
	call	BIO_clear_flags@PLT
	testl	%r13d, %r13d
	jle	.L74
.L58:
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	movq	%r15, %rdi
	movq	%rsi, -56(%rbp)
	call	BIO_ADDR_sockaddr_size@PLT
	movq	%r15, %rdi
	movl	%eax, %ebx
	call	BIO_ADDR_sockaddr@PLT
	movl	48(%r12), %edi
	movq	-56(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%rax, %r8
	movl	%ebx, %r9d
	call	sendto@PLT
	movl	%eax, %r13d
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L74:
	leal	1(%r13), %eax
	cmpl	$1, %eax
	ja	.L58
	movl	(%r14), %eax
	cmpl	$71, %eax
	je	.L62
	jg	.L63
	cmpl	$4, %eax
	je	.L62
	cmpl	$11, %eax
	jne	.L58
.L62:
	movl	$10, %esi
	movq	%r12, %rdi
	call	BIO_set_flags@PLT
	movl	(%r14), %eax
	movl	%eax, 116(%r15)
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L63:
	subl	$114, %eax
	cmpl	$1, %eax
	ja	.L58
	jmp	.L62
	.cfi_endproc
.LFE275:
	.size	dgram_write, .-dgram_write
	.p2align 4
	.type	dgram_puts, @function
dgram_puts:
.LFB278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	call	strlen@PLT
	movq	56(%r12), %r15
	movq	%rax, %rbx
	call	__errno_location@PLT
	movslq	%ebx, %rbx
	movl	$0, (%rax)
	movq	%rax, %r14
	movl	112(%r15), %eax
	testl	%eax, %eax
	je	.L76
	movl	48(%r12), %edi
	movq	%r13, %rsi
	movq	%rbx, %rdx
	call	write@PLT
	movl	%eax, %r13d
.L77:
	movl	$15, %esi
	movq	%r12, %rdi
	call	BIO_clear_flags@PLT
	testl	%r13d, %r13d
	jle	.L91
.L75:
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	movq	%r15, %rdi
	call	BIO_ADDR_sockaddr_size@PLT
	movq	%r15, %rdi
	movl	%eax, -52(%rbp)
	call	BIO_ADDR_sockaddr@PLT
	movl	48(%r12), %edi
	movl	-52(%rbp), %r9d
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %r8
	movq	%rbx, %rdx
	call	sendto@PLT
	movl	%eax, %r13d
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L91:
	leal	1(%r13), %eax
	cmpl	$1, %eax
	ja	.L75
	movl	(%r14), %eax
	cmpl	$71, %eax
	je	.L79
	jg	.L80
	cmpl	$4, %eax
	je	.L79
	cmpl	$11, %eax
	jne	.L75
.L79:
	movl	$10, %esi
	movq	%r12, %rdi
	call	BIO_set_flags@PLT
	movl	(%r14), %eax
	movl	%eax, 116(%r15)
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L80:
	subl	$114, %eax
	cmpl	$1, %eax
	ja	.L75
	jmp	.L79
	.cfi_endproc
.LFE278:
	.size	dgram_puts, .-dgram_puts
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"assertion failed: sz.s <= sizeof(struct timeval)"
	.text
	.p2align 4
	.type	dgram_ctrl, @function
dgram_ctrl:
.LFB277:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subl	$8, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$160, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	56(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -176(%rbp)
	cmpl	$97, %esi
	ja	.L124
	movq	%rcx, %r12
	leaq	.L95(%rip), %rcx
	movq	%rdi, %rbx
	movslq	(%rcx,%rsi,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L95:
	.long	.L117-.L95
	.long	.L116-.L95
	.long	.L124-.L95
	.long	.L173-.L95
	.long	.L173-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L103-.L95
	.long	.L114-.L95
	.long	.L113-.L95
	.long	.L112-.L95
	.long	.L111-.L95
	.long	.L110-.L95
	.long	.L109-.L95
	.long	.L109-.L95
	.long	.L108-.L95
	.long	.L107-.L95
	.long	.L106-.L95
	.long	.L105-.L95
	.long	.L104-.L95
	.long	.L103-.L95
	.long	.L102-.L95
	.long	.L101-.L95
	.long	.L100-.L95
	.long	.L99-.L95
	.long	.L98-.L95
	.long	.L97-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L97-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L124-.L95
	.long	.L96-.L95
	.long	.L94-.L95
	.text
	.p2align 4,,10
	.p2align 3
.L104:
	cmpl	$90, 116(%r14)
	je	.L175
	.p2align 4,,10
	.p2align 3
.L124:
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L92:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L176
	addq	$160, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	cmpl	$11, 116(%r14)
	jne	.L124
.L175:
	movl	$0, 116(%r14)
.L173:
	movl	$1, %r13d
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L114:
	testq	%r12, %r12
	je	.L132
	movl	$1, 112(%r14)
.L103:
	movq	%r12, %rdi
	movl	$1, %r13d
	call	BIO_ADDR_sockaddr@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	BIO_ADDR_make@PLT
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L97:
	movl	%edx, 160(%r14)
	movl	$1, %r13d
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L99:
	xorl	%eax, %eax
	testq	%rdx, %rdx
	setne	%al
	movl	%eax, -176(%rbp)
	movzwl	(%r14), %eax
	cmpw	$2, %ax
	je	.L145
	cmpw	$10, %ax
	jne	.L152
	movl	48(%rdi), %edi
	movl	$4, %r8d
	movl	$62, %edx
	leaq	-176(%rbp), %rcx
	movl	$41, %esi
	call	setsockopt@PLT
	movslq	%eax, %r13
	testq	%r13, %r13
	jns	.L92
.L135:
	leaq	.LC3(%rip), %rdi
	orq	$-1, %r13
	call	perror@PLT
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L100:
	movq	%r14, %rdi
	movl	$28, %ebx
	call	BIO_ADDR_family@PLT
	cmpl	$10, %eax
	je	.L177
.L127:
	movq	%r14, %rdi
	movl	$576, %r13d
	call	BIO_ADDR_family@PLT
	subq	%rbx, %r13
	cmpl	$10, %eax
	jne	.L92
	xorl	%edx, %edx
	leaq	-160(%rbp), %rsi
	movq	%r14, %rdi
	call	BIO_ADDR_rawaddress@PLT
	testl	%eax, %eax
	je	.L130
	movl	-160(%rbp), %eax
	orl	-156(%rbp), %eax
	jne	.L130
	cmpl	$-65536, -152(%rbp)
	je	.L92
	.p2align 4,,10
	.p2align 3
.L130:
	movl	$1280, %r13d
	subq	%rbx, %r13
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L101:
	movq	%r14, %rdi
	movq	%rdx, -184(%rbp)
	call	BIO_ADDR_sockaddr_size@PLT
	movq	-184(%rbp), %rdx
	movl	%eax, %r13d
	testq	%rdx, %rdx
	je	.L133
	cmpq	%r13, %rdx
	cmovle	%rdx, %r13
.L133:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	memcpy@PLT
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L102:
	movdqu	(%r12), %xmm0
	movl	$1, %r13d
	movups	%xmm0, 128(%r14)
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L112:
	movl	48(%rdi), %edi
	movq	%r12, %rcx
	movl	$20, %edx
	movl	$1, %esi
	leaq	-168(%rbp), %r8
	movq	$16, -168(%rbp)
	call	getsockopt@PLT
	testl	%eax, %eax
	js	.L174
	movslq	-168(%rbp), %r13
	testl	%r13d, %r13d
	jne	.L92
	movq	-168(%rbp), %r13
	cmpq	$16, %r13
	jbe	.L92
	movl	$635, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	call	OPENSSL_die@PLT
	.p2align 4,,10
	.p2align 3
.L96:
	movl	36(%rdi), %esi
	testl	%esi, %esi
	je	.L118
	movl	32(%rdi), %ecx
	testl	%ecx, %ecx
	jne	.L178
.L119:
	movl	$0, 32(%rbx)
	movl	$0, 40(%rbx)
.L118:
	movl	(%r12), %eax
	movl	$1, %r13d
	movl	%edx, 36(%rbx)
	movl	$1, 32(%rbx)
	movl	%eax, 48(%rbx)
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L116:
	movl	%edx, 36(%rdi)
	movl	$1, %r13d
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L117:
	movslq	36(%rdi), %r13
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L111:
	movl	48(%rdi), %edi
	movq	%r12, %rcx
	movl	$21, %edx
	movl	$1, %esi
	movl	$16, %r8d
	call	setsockopt@PLT
	testl	%eax, %eax
	jns	.L173
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L113:
	movl	48(%rdi), %edi
	movq	%r12, %rcx
	movl	$20, %edx
	movl	$1, %esi
	movl	$16, %r8d
	call	setsockopt@PLT
	testl	%eax, %eax
	jns	.L173
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L110:
	movl	48(%rdi), %edi
	movq	%r12, %rcx
	movl	$21, %edx
	movl	$1, %esi
	leaq	-168(%rbp), %r8
	movq	$16, -168(%rbp)
	call	getsockopt@PLT
	testl	%eax, %eax
	js	.L174
	movslq	-168(%rbp), %r13
	testl	%r13d, %r13d
	jne	.L92
	movq	-168(%rbp), %r13
	cmpq	$16, %r13
	jbe	.L92
	movl	$692, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	call	OPENSSL_die@PLT
	.p2align 4,,10
	.p2align 3
.L107:
	leaq	-160(%rbp), %rsi
	xorl	%eax, %eax
	movl	$14, %ecx
	movl	$112, -168(%rbp)
	movq	%rsi, %rdi
	leaq	-168(%rbp), %rdx
	rep stosq
	movl	48(%rbx), %edi
	call	getsockname@PLT
	testl	%eax, %eax
	js	.L124
	movzwl	-160(%rbp), %eax
	movl	$4, -172(%rbp)
	cmpw	$2, %ax
	je	.L125
	cmpw	$10, %ax
	jne	.L124
	movl	48(%rbx), %edi
	movl	$24, %edx
	movl	$41, %esi
	leaq	-176(%rbp), %rcx
	leaq	-172(%rbp), %r8
	call	getsockopt@PLT
	testl	%eax, %eax
	js	.L124
	movl	-176(%rbp), %eax
	testl	%eax, %eax
	js	.L124
	leal	-48(%rax), %r13d
	movl	%r13d, 120(%r14)
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L108:
	leaq	-160(%rbp), %rsi
	xorl	%eax, %eax
	movl	$14, %ecx
	movl	$112, -168(%rbp)
	movq	%rsi, %rdi
	leaq	-168(%rbp), %rdx
	rep stosq
	movl	48(%rbx), %edi
	call	getsockname@PLT
	testl	%eax, %eax
	js	.L124
	movzwl	-160(%rbp), %eax
	cmpw	$2, %ax
	je	.L122
	cmpw	$10, %ax
	jne	.L152
	movl	48(%rbx), %edi
	movl	$4, %r8d
	movl	$23, %edx
	leaq	-176(%rbp), %rcx
	movl	$2, -176(%rbp)
	movl	$41, %esi
	call	setsockopt@PLT
	movslq	%eax, %r13
	testq	%r13, %r13
	jns	.L92
.L172:
	leaq	.LC3(%rip), %rdi
	call	perror@PLT
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L106:
	movl	120(%r14), %r13d
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L98:
	movq	%r14, %rdi
	movl	$28, %r13d
	call	BIO_ADDR_family@PLT
	cmpl	$10, %eax
	jne	.L92
	leaq	-160(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
	movl	$48, %r13d
	call	BIO_ADDR_rawaddress@PLT
	testl	%eax, %eax
	je	.L92
	movl	-160(%rbp), %eax
	orl	-156(%rbp), %eax
	jne	.L92
	cmpl	$-65536, -152(%rbp)
	movl	$28, %eax
	cmove	%rax, %r13
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L105:
	movl	%edx, 120(%r14)
	movq	%rdx, %r13
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L94:
	movl	32(%rdi), %eax
	testl	%eax, %eax
	je	.L152
	movslq	48(%rdi), %r13
	testq	%r12, %r12
	je	.L92
	movl	%r13d, (%r12)
	movslq	48(%rdi), %r13
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L152:
	movq	$-1, %r13
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L174:
	leaq	.LC1(%rip), %rdi
	movq	$-1, %r13
	call	perror@PLT
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L145:
	cmpq	$1, %rdx
	movl	48(%rdi), %edi
	movl	$10, %edx
	leaq	-176(%rbp), %rcx
	sbbl	%eax, %eax
	movl	$4, %r8d
	xorl	%esi, %esi
	notl	%eax
	andl	$3, %eax
	movl	%eax, -176(%rbp)
	call	setsockopt@PLT
	movslq	%eax, %r13
	testq	%r13, %r13
	jns	.L92
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L177:
	leaq	-160(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
	movl	$48, %ebx
	call	BIO_ADDR_rawaddress@PLT
	testl	%eax, %eax
	je	.L127
	movl	-160(%rbp), %eax
	orl	-156(%rbp), %eax
	jne	.L127
	cmpl	$-65536, -152(%rbp)
	movl	$28, %eax
	cmove	%rax, %rbx
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L132:
	leaq	8(%r14), %rdi
	movl	$0, 112(%r14)
	movq	%r12, %rax
	movl	$1, %r13d
	andq	$-8, %rdi
	movq	$0, (%r14)
	movq	$0, 104(%r14)
	subq	%rdi, %r14
	leal	112(%r14), %ecx
	shrl	$3, %ecx
	rep stosq
	jmp	.L92
.L125:
	movl	48(%rbx), %edi
	xorl	%esi, %esi
	leaq	-176(%rbp), %rcx
	leaq	-172(%rbp), %r8
	movl	$14, %edx
	call	getsockopt@PLT
	testl	%eax, %eax
	js	.L124
	movl	-176(%rbp), %eax
	testl	%eax, %eax
	js	.L124
	leal	-28(%rax), %r13d
	movl	%r13d, 120(%r14)
	jmp	.L92
.L122:
	movl	48(%rbx), %edi
	movl	$4, %r8d
	movl	$10, %edx
	xorl	%esi, %esi
	leaq	-176(%rbp), %rcx
	movl	$2, -176(%rbp)
	call	setsockopt@PLT
	movslq	%eax, %r13
	testq	%r13, %r13
	jns	.L92
	jmp	.L172
.L178:
	movl	48(%rdi), %edi
	movq	%rdx, -184(%rbp)
	call	BIO_closesocket@PLT
	movq	-184(%rbp), %rdx
	jmp	.L119
.L176:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE277:
	.size	dgram_ctrl, .-dgram_ctrl
	.p2align 4
	.globl	BIO_s_datagram
	.type	BIO_s_datagram, @function
BIO_s_datagram:
.LFB267:
	.cfi_startproc
	endbr64
	leaq	methods_dgramp(%rip), %rax
	ret
	.cfi_endproc
.LFE267:
	.size	BIO_s_datagram, .-BIO_s_datagram
	.p2align 4
	.globl	BIO_new_dgram
	.type	BIO_new_dgram, @function
BIO_new_dgram:
.LFB268:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edi, %r13d
	leaq	methods_dgramp(%rip), %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$8, %rsp
	call	BIO_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L180
	movslq	%ebx, %rdx
	movl	%r13d, %ecx
	movl	$104, %esi
	movq	%rax, %rdi
	call	BIO_int_ctrl@PLT
.L180:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE268:
	.size	BIO_new_dgram, .-BIO_new_dgram
	.p2align 4
	.globl	BIO_dgram_non_fatal_error
	.type	BIO_dgram_non_fatal_error, @function
BIO_dgram_non_fatal_error:
.LFB280:
	.cfi_startproc
	endbr64
	cmpl	$71, %edi
	je	.L189
	jg	.L188
	movl	$1, %eax
	cmpl	$4, %edi
	je	.L186
	xorl	%eax, %eax
	cmpl	$11, %edi
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L188:
	subl	$114, %edi
	xorl	%eax, %eax
	cmpl	$1, %edi
	setbe	%al
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	movl	$1, %eax
.L186:
	ret
	.cfi_endproc
.LFE280:
	.size	BIO_dgram_non_fatal_error, .-BIO_dgram_non_fatal_error
	.section	.rodata.str1.1
.LC5:
	.string	"datagram socket"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	methods_dgramp, @object
	.size	methods_dgramp, 96
methods_dgramp:
	.long	1301
	.zero	4
	.quad	.LC5
	.quad	bwrite_conv
	.quad	dgram_write
	.quad	bread_conv
	.quad	dgram_read
	.quad	dgram_puts
	.quad	0
	.quad	dgram_ctrl
	.quad	dgram_new
	.quad	dgram_free
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
