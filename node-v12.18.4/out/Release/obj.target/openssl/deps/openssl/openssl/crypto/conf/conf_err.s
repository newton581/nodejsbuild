	.file	"conf_err.c"
	.text
	.p2align 4
	.globl	ERR_load_CONF_strings
	.type	ERR_load_CONF_strings, @function
ERR_load_CONF_strings:
.LFB203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$235307008, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_func_error_string@PLT
	testq	%rax, %rax
	je	.L5
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	leaq	CONF_str_functs(%rip), %rdi
	call	ERR_load_strings_const@PLT
	leaq	CONF_str_reasons(%rip), %rdi
	call	ERR_load_strings_const@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE203:
	.size	ERR_load_CONF_strings, .-ERR_load_CONF_strings
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"error loading dso"
.LC1:
	.string	"list cannot be null"
.LC2:
	.string	"missing close square bracket"
.LC3:
	.string	"missing equal sign"
.LC4:
	.string	"missing init function"
.LC5:
	.string	"module initialization error"
.LC6:
	.string	"no close brace"
.LC7:
	.string	"no conf"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"no conf or environment variable"
	.section	.rodata.str1.1
.LC9:
	.string	"no section"
.LC10:
	.string	"no such file"
.LC11:
	.string	"no value"
.LC12:
	.string	"number too large"
.LC13:
	.string	"recursive directory include"
.LC14:
	.string	"ssl command section empty"
.LC15:
	.string	"ssl command section not found"
.LC16:
	.string	"ssl section empty"
.LC17:
	.string	"ssl section not found"
.LC18:
	.string	"unable to create new section"
.LC19:
	.string	"unknown module name"
.LC20:
	.string	"variable expansion too long"
.LC21:
	.string	"variable has no value"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	CONF_str_reasons, @object
	.size	CONF_str_reasons, 368
CONF_str_reasons:
	.quad	234881134
	.quad	.LC0
	.quad	234881139
	.quad	.LC1
	.quad	234881124
	.quad	.LC2
	.quad	234881125
	.quad	.LC3
	.quad	234881136
	.quad	.LC4
	.quad	234881133
	.quad	.LC5
	.quad	234881126
	.quad	.LC6
	.quad	234881129
	.quad	.LC7
	.quad	234881130
	.quad	.LC8
	.quad	234881131
	.quad	.LC9
	.quad	234881138
	.quad	.LC10
	.quad	234881132
	.quad	.LC11
	.quad	234881145
	.quad	.LC12
	.quad	234881135
	.quad	.LC13
	.quad	234881141
	.quad	.LC14
	.quad	234881142
	.quad	.LC15
	.quad	234881143
	.quad	.LC16
	.quad	234881144
	.quad	.LC17
	.quad	234881127
	.quad	.LC18
	.quad	234881137
	.quad	.LC19
	.quad	234881140
	.quad	.LC20
	.quad	234881128
	.quad	.LC21
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC22:
	.string	"CONF_dump_fp"
.LC23:
	.string	"CONF_load"
.LC24:
	.string	"CONF_load_fp"
.LC25:
	.string	"CONF_parse_list"
.LC26:
	.string	"def_load"
.LC27:
	.string	"def_load_bio"
.LC28:
	.string	"get_next_file"
.LC29:
	.string	"module_add"
.LC30:
	.string	"module_init"
.LC31:
	.string	"module_load_dso"
.LC32:
	.string	"module_run"
.LC33:
	.string	"NCONF_dump_bio"
.LC34:
	.string	"NCONF_dump_fp"
.LC35:
	.string	"NCONF_get_number_e"
.LC36:
	.string	"NCONF_get_section"
.LC37:
	.string	"NCONF_get_string"
.LC38:
	.string	"NCONF_load"
.LC39:
	.string	"NCONF_load_bio"
.LC40:
	.string	"NCONF_load_fp"
.LC41:
	.string	"NCONF_new"
.LC42:
	.string	"process_include"
.LC43:
	.string	"ssl_module_init"
.LC44:
	.string	"str_copy"
	.section	.data.rel.ro.local
	.align 32
	.type	CONF_str_functs, @object
	.size	CONF_str_functs, 384
CONF_str_functs:
	.quad	235307008
	.quad	.LC22
	.quad	235290624
	.quad	.LC23
	.quad	235302912
	.quad	.LC24
	.quad	235368448
	.quad	.LC25
	.quad	235372544
	.quad	.LC26
	.quad	235376640
	.quad	.LC27
	.quad	235319296
	.quad	.LC28
	.quad	235380736
	.quad	.LC29
	.quad	235352064
	.quad	.LC30
	.quad	235360256
	.quad	.LC31
	.quad	235364352
	.quad	.LC32
	.quad	235311104
	.quad	.LC33
	.quad	235315200
	.quad	.LC34
	.quad	235339776
	.quad	.LC35
	.quad	235323392
	.quad	.LC36
	.quad	235327488
	.quad	.LC37
	.quad	235343872
	.quad	.LC38
	.quad	235331584
	.quad	.LC39
	.quad	235347968
	.quad	.LC40
	.quad	235335680
	.quad	.LC41
	.quad	235356160
	.quad	.LC42
	.quad	235384832
	.quad	.LC43
	.quad	235294720
	.quad	.LC44
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
