	.file	"asn1_item_list.c"
	.text
	.p2align 4
	.globl	ASN1_ITEM_lookup
	.type	ASN1_ITEM_lookup, @function
ASN1_ITEM_lookup:
.LFB1536:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	leaq	asn1_item_list(%rip), %rbx
	leaq	1152(%rbx), %r14
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L11:
	addq	$8, %rbx
	cmpq	%r14, %rbx
	je	.L10
.L3:
	movq	(%rbx), %r12
	movq	%r13, %rsi
	movq	48(%r12), %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L11
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1536:
	.size	ASN1_ITEM_lookup, .-ASN1_ITEM_lookup
	.p2align 4
	.globl	ASN1_ITEM_get
	.type	ASN1_ITEM_get, @function
ASN1_ITEM_get:
.LFB1537:
	.cfi_startproc
	endbr64
	cmpq	$143, %rdi
	ja	.L14
	leaq	asn1_item_list(%rip), %rax
	movq	(%rax,%rdi,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1537:
	.size	ASN1_ITEM_get, .-ASN1_ITEM_get
	.section	.data.rel.ro,"aw"
	.align 32
	.type	asn1_item_list, @object
	.size	asn1_item_list, 1152
asn1_item_list:
	.quad	ACCESS_DESCRIPTION_it
	.quad	ASIdOrRange_it
	.quad	ASIdentifierChoice_it
	.quad	ASIdentifiers_it
	.quad	ASN1_ANY_it
	.quad	ASN1_BIT_STRING_it
	.quad	ASN1_BMPSTRING_it
	.quad	ASN1_BOOLEAN_it
	.quad	ASN1_ENUMERATED_it
	.quad	ASN1_FBOOLEAN_it
	.quad	ASN1_GENERALIZEDTIME_it
	.quad	ASN1_GENERALSTRING_it
	.quad	ASN1_IA5STRING_it
	.quad	ASN1_INTEGER_it
	.quad	ASN1_NULL_it
	.quad	ASN1_OBJECT_it
	.quad	ASN1_OCTET_STRING_NDEF_it
	.quad	ASN1_OCTET_STRING_it
	.quad	ASN1_PRINTABLESTRING_it
	.quad	ASN1_PRINTABLE_it
	.quad	ASN1_SEQUENCE_ANY_it
	.quad	ASN1_SEQUENCE_it
	.quad	ASN1_SET_ANY_it
	.quad	ASN1_T61STRING_it
	.quad	ASN1_TBOOLEAN_it
	.quad	ASN1_TIME_it
	.quad	ASN1_UNIVERSALSTRING_it
	.quad	ASN1_UTCTIME_it
	.quad	ASN1_UTF8STRING_it
	.quad	ASN1_VISIBLESTRING_it
	.quad	ASRange_it
	.quad	AUTHORITY_INFO_ACCESS_it
	.quad	AUTHORITY_KEYID_it
	.quad	BASIC_CONSTRAINTS_it
	.quad	BIGNUM_it
	.quad	CBIGNUM_it
	.quad	CERTIFICATEPOLICIES_it
	.quad	CMS_ContentInfo_it
	.quad	CMS_ReceiptRequest_it
	.quad	CRL_DIST_POINTS_it
	.quad	DHparams_it
	.quad	DIRECTORYSTRING_it
	.quad	DISPLAYTEXT_it
	.quad	DIST_POINT_NAME_it
	.quad	DIST_POINT_it
	.quad	ECPARAMETERS_it
	.quad	ECPKPARAMETERS_it
	.quad	EDIPARTYNAME_it
	.quad	EXTENDED_KEY_USAGE_it
	.quad	GENERAL_NAMES_it
	.quad	GENERAL_NAME_it
	.quad	GENERAL_SUBTREE_it
	.quad	IPAddressChoice_it
	.quad	IPAddressFamily_it
	.quad	IPAddressOrRange_it
	.quad	IPAddressRange_it
	.quad	ISSUING_DIST_POINT_it
	.quad	LONG_it
	.quad	NAME_CONSTRAINTS_it
	.quad	NETSCAPE_CERT_SEQUENCE_it
	.quad	NETSCAPE_SPKAC_it
	.quad	NETSCAPE_SPKI_it
	.quad	NOTICEREF_it
	.quad	OCSP_BASICRESP_it
	.quad	OCSP_CERTID_it
	.quad	OCSP_CERTSTATUS_it
	.quad	OCSP_CRLID_it
	.quad	OCSP_ONEREQ_it
	.quad	OCSP_REQINFO_it
	.quad	OCSP_REQUEST_it
	.quad	OCSP_RESPBYTES_it
	.quad	OCSP_RESPDATA_it
	.quad	OCSP_RESPID_it
	.quad	OCSP_RESPONSE_it
	.quad	OCSP_REVOKEDINFO_it
	.quad	OCSP_SERVICELOC_it
	.quad	OCSP_SIGNATURE_it
	.quad	OCSP_SINGLERESP_it
	.quad	OTHERNAME_it
	.quad	PBE2PARAM_it
	.quad	PBEPARAM_it
	.quad	PBKDF2PARAM_it
	.quad	PKCS12_AUTHSAFES_it
	.quad	PKCS12_BAGS_it
	.quad	PKCS12_MAC_DATA_it
	.quad	PKCS12_SAFEBAGS_it
	.quad	PKCS12_SAFEBAG_it
	.quad	PKCS12_it
	.quad	PKCS7_ATTR_SIGN_it
	.quad	PKCS7_ATTR_VERIFY_it
	.quad	PKCS7_DIGEST_it
	.quad	PKCS7_ENCRYPT_it
	.quad	PKCS7_ENC_CONTENT_it
	.quad	PKCS7_ENVELOPE_it
	.quad	PKCS7_ISSUER_AND_SERIAL_it
	.quad	PKCS7_RECIP_INFO_it
	.quad	PKCS7_SIGNED_it
	.quad	PKCS7_SIGNER_INFO_it
	.quad	PKCS7_SIGN_ENVELOPE_it
	.quad	PKCS7_it
	.quad	PKCS8_PRIV_KEY_INFO_it
	.quad	PKEY_USAGE_PERIOD_it
	.quad	POLICYINFO_it
	.quad	POLICYQUALINFO_it
	.quad	POLICY_CONSTRAINTS_it
	.quad	POLICY_MAPPINGS_it
	.quad	POLICY_MAPPING_it
	.quad	PROXY_CERT_INFO_EXTENSION_it
	.quad	PROXY_POLICY_it
	.quad	RSAPrivateKey_it
	.quad	RSAPublicKey_it
	.quad	RSA_OAEP_PARAMS_it
	.quad	RSA_PSS_PARAMS_it
	.quad	SCRYPT_PARAMS_it
	.quad	SXNETID_it
	.quad	SXNET_it
	.quad	USERNOTICE_it
	.quad	X509_ALGORS_it
	.quad	X509_ALGOR_it
	.quad	X509_ATTRIBUTE_it
	.quad	X509_CERT_AUX_it
	.quad	X509_CINF_it
	.quad	X509_CRL_INFO_it
	.quad	X509_CRL_it
	.quad	X509_EXTENSIONS_it
	.quad	X509_EXTENSION_it
	.quad	X509_NAME_ENTRY_it
	.quad	X509_NAME_it
	.quad	X509_PUBKEY_it
	.quad	X509_REQ_INFO_it
	.quad	X509_REQ_it
	.quad	X509_REVOKED_it
	.quad	X509_SIG_it
	.quad	X509_VAL_it
	.quad	X509_it
	.quad	ZLONG_it
	.quad	INT32_it
	.quad	UINT32_it
	.quad	ZINT32_it
	.quad	ZUINT32_it
	.quad	INT64_it
	.quad	UINT64_it
	.quad	ZINT64_it
	.quad	ZUINT64_it
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
