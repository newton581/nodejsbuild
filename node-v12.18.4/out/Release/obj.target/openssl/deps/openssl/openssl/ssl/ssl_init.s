	.file	"ssl_init.c"
	.text
	.p2align 4
	.type	ossl_init_no_load_ssl_strings_ossl_, @function
ossl_init_no_load_ssl_strings_ossl_:
.LFB968:
	.cfi_startproc
	endbr64
	movl	$1, ossl_init_load_ssl_strings_ossl_ret_(%rip)
	ret
	.cfi_endproc
.LFE968:
	.size	ossl_init_no_load_ssl_strings_ossl_, .-ossl_init_no_load_ssl_strings_ossl_
	.p2align 4
	.type	ossl_init_load_ssl_strings_ossl_, @function
ossl_init_load_ssl_strings_ossl_:
.LFB966:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_load_SSL_strings@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	$1, ssl_strings_inited(%rip)
	movl	$1, ossl_init_load_ssl_strings_ossl_ret_(%rip)
	ret
	.cfi_endproc
.LFE966:
	.size	ossl_init_load_ssl_strings_ossl_, .-ossl_init_load_ssl_strings_ossl_
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"MD5"
.LC1:
	.string	"ssl3-md5"
.LC2:
	.string	"SHA1"
.LC3:
	.string	"ssl3-sha1"
.LC4:
	.string	"RSA-SHA1"
.LC5:
	.string	"RSA-SHA1-2"
	.text
	.p2align 4
	.type	ossl_init_ssl_base_ossl_, @function
ossl_init_ssl_base_ossl_:
.LFB964:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	EVP_des_cbc@PLT
	movq	%rax, %rdi
	call	EVP_add_cipher@PLT
	call	EVP_des_ede3_cbc@PLT
	movq	%rax, %rdi
	call	EVP_add_cipher@PLT
	call	EVP_idea_cbc@PLT
	movq	%rax, %rdi
	call	EVP_add_cipher@PLT
	call	EVP_rc4@PLT
	movq	%rax, %rdi
	call	EVP_add_cipher@PLT
	call	EVP_rc4_hmac_md5@PLT
	movq	%rax, %rdi
	call	EVP_add_cipher@PLT
	call	EVP_rc2_cbc@PLT
	movq	%rax, %rdi
	call	EVP_add_cipher@PLT
	call	EVP_rc2_40_cbc@PLT
	movq	%rax, %rdi
	call	EVP_add_cipher@PLT
	call	EVP_aes_128_cbc@PLT
	movq	%rax, %rdi
	call	EVP_add_cipher@PLT
	call	EVP_aes_192_cbc@PLT
	movq	%rax, %rdi
	call	EVP_add_cipher@PLT
	call	EVP_aes_256_cbc@PLT
	movq	%rax, %rdi
	call	EVP_add_cipher@PLT
	call	EVP_aes_128_gcm@PLT
	movq	%rax, %rdi
	call	EVP_add_cipher@PLT
	call	EVP_aes_256_gcm@PLT
	movq	%rax, %rdi
	call	EVP_add_cipher@PLT
	call	EVP_aes_128_ccm@PLT
	movq	%rax, %rdi
	call	EVP_add_cipher@PLT
	call	EVP_aes_256_ccm@PLT
	movq	%rax, %rdi
	call	EVP_add_cipher@PLT
	call	EVP_aes_128_cbc_hmac_sha1@PLT
	movq	%rax, %rdi
	call	EVP_add_cipher@PLT
	call	EVP_aes_256_cbc_hmac_sha1@PLT
	movq	%rax, %rdi
	call	EVP_add_cipher@PLT
	call	EVP_aes_128_cbc_hmac_sha256@PLT
	movq	%rax, %rdi
	call	EVP_add_cipher@PLT
	call	EVP_aes_256_cbc_hmac_sha256@PLT
	movq	%rax, %rdi
	call	EVP_add_cipher@PLT
	call	EVP_aria_128_gcm@PLT
	movq	%rax, %rdi
	call	EVP_add_cipher@PLT
	call	EVP_aria_256_gcm@PLT
	movq	%rax, %rdi
	call	EVP_add_cipher@PLT
	call	EVP_camellia_128_cbc@PLT
	movq	%rax, %rdi
	call	EVP_add_cipher@PLT
	call	EVP_camellia_256_cbc@PLT
	movq	%rax, %rdi
	call	EVP_add_cipher@PLT
	call	EVP_chacha20_poly1305@PLT
	movq	%rax, %rdi
	call	EVP_add_cipher@PLT
	call	EVP_seed_cbc@PLT
	movq	%rax, %rdi
	call	EVP_add_cipher@PLT
	call	EVP_md5@PLT
	movq	%rax, %rdi
	call	EVP_add_digest@PLT
	leaq	.LC0(%rip), %rdx
	movl	$32769, %esi
	leaq	.LC1(%rip), %rdi
	call	OBJ_NAME_add@PLT
	call	EVP_md5_sha1@PLT
	movq	%rax, %rdi
	call	EVP_add_digest@PLT
	call	EVP_sha1@PLT
	movq	%rax, %rdi
	call	EVP_add_digest@PLT
	leaq	.LC2(%rip), %rdx
	movl	$32769, %esi
	leaq	.LC3(%rip), %rdi
	call	OBJ_NAME_add@PLT
	leaq	.LC4(%rip), %rdx
	movl	$32769, %esi
	leaq	.LC5(%rip), %rdi
	call	OBJ_NAME_add@PLT
	call	EVP_sha224@PLT
	movq	%rax, %rdi
	call	EVP_add_digest@PLT
	call	EVP_sha256@PLT
	movq	%rax, %rdi
	call	EVP_add_digest@PLT
	call	EVP_sha384@PLT
	movq	%rax, %rdi
	call	EVP_add_digest@PLT
	call	EVP_sha512@PLT
	movq	%rax, %rdi
	call	EVP_add_digest@PLT
	call	ssl_load_ciphers@PLT
	testl	%eax, %eax
	je	.L6
	leaq	ssl_library_stop(%rip), %rdi
	call	OPENSSL_atexit@PLT
	movl	$1, %eax
.L6:
	movl	%eax, ossl_init_ssl_base_ossl_ret_(%rip)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE964:
	.size	ossl_init_ssl_base_ossl_, .-ossl_init_ssl_base_ossl_
	.p2align 4
	.type	ssl_library_stop, @function
ssl_library_stop:
.LFB970:
	.cfi_startproc
	endbr64
	movl	stopped(%rip), %edx
	testl	%edx, %edx
	jne	.L11
	movl	$1, stopped(%rip)
	movl	ssl_strings_inited(%rip), %eax
	testl	%eax, %eax
	jne	.L15
.L11:
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	jmp	err_free_strings_int@PLT
	.cfi_endproc
.LFE970:
	.size	ssl_library_stop, .-ssl_library_stop
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"../deps/openssl/openssl/ssl/ssl_init.c"
	.text
	.p2align 4
	.globl	OPENSSL_init_ssl
	.type	OPENSSL_init_ssl, @function
OPENSSL_init_ssl:
.LFB971:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movl	stopped(%rip), %r8d
	testl	%r8d, %r8d
	jne	.L39
	movq	%rdi, %rax
	movq	%rdi, %rbx
	orq	$12, %rax
	orq	$76, %rbx
	andl	$128, %edi
	cmovne	%rax, %rbx
	movq	%rbx, %rdi
	call	OPENSSL_init_crypto@PLT
	testl	%eax, %eax
	jne	.L40
.L18:
	xorl	%eax, %eax
.L16:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	leaq	ossl_init_ssl_base_ossl_(%rip), %rsi
	leaq	ssl_base(%rip), %rdi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L18
	movl	ossl_init_ssl_base_ossl_ret_(%rip), %ecx
	testl	%ecx, %ecx
	je	.L18
	testl	$1048576, %ebx
	jne	.L22
.L25:
	testl	$2097152, %ebx
	jne	.L41
.L24:
	movl	$1, %eax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L39:
	movl	stoperrset.23140(%rip), %eax
	testl	%eax, %eax
	jne	.L18
	movl	$193, %r8d
	leaq	.LC6(%rip), %rcx
	movl	$70, %edx
	movl	%eax, -20(%rbp)
	movl	$342, %esi
	movl	$20, %edi
	movl	$1, stoperrset.23140(%rip)
	call	ERR_put_error@PLT
	movl	-20(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	leaq	ossl_init_no_load_ssl_strings_ossl_(%rip), %rsi
	leaq	ssl_strings(%rip), %rdi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L18
	movl	ossl_init_load_ssl_strings_ossl_ret_(%rip), %edx
	testl	%edx, %edx
	jne	.L25
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L41:
	leaq	ossl_init_load_ssl_strings_ossl_(%rip), %rsi
	leaq	ssl_strings(%rip), %rdi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L18
	movl	ossl_init_load_ssl_strings_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	jne	.L24
	jmp	.L18
	.cfi_endproc
.LFE971:
	.size	OPENSSL_init_ssl, .-OPENSSL_init_ssl
	.local	stoperrset.23140
	.comm	stoperrset.23140,4,4
	.local	ossl_init_load_ssl_strings_ossl_ret_
	.comm	ossl_init_load_ssl_strings_ossl_ret_,4,4
	.local	ssl_strings_inited
	.comm	ssl_strings_inited,4,4
	.local	ssl_strings
	.comm	ssl_strings,4,4
	.local	ossl_init_ssl_base_ossl_ret_
	.comm	ossl_init_ssl_base_ossl_ret_,4,4
	.local	ssl_base
	.comm	ssl_base,4,4
	.local	stopped
	.comm	stopped,4,4
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
