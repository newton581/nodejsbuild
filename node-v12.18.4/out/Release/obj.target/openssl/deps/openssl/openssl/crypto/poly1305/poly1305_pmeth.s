	.file	"poly1305_pmeth.c"
	.text
	.p2align 4
	.type	pkey_poly1305_ctrl, @function
pkey_poly1305_ctrl:
.LFB1329:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_PKEY_CTX_get_data@PLT
	movq	%rax, %r15
	movl	$1, %eax
	cmpl	$1, %ebx
	je	.L1
	leal	-6(%rbx), %edx
	movl	$-2, %eax
	cmpl	$1, %edx
	ja	.L1
	cmpl	$6, %ebx
	jne	.L3
	movq	%r14, -64(%rbp)
.L4:
	testq	%r13, %r13
	je	.L6
	cmpq	$32, -64(%rbp)
	je	.L16
.L6:
	xorl	%eax, %eax
.L1:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L17
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movl	$32, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	ASN1_OCTET_STRING_set@PLT
	testl	%eax, %eax
	je	.L6
	movq	%r15, %rdi
	call	ASN1_STRING_get0_data@PLT
	leaq	24(%r15), %rdi
	movq	%rax, %rsi
	call	Poly1305_Init@PLT
	movl	$1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L3:
	movq	%r12, %rdi
	call	EVP_PKEY_CTX_get0_pkey@PLT
	leaq	-64(%rbp), %rsi
	movq	%rax, %rdi
	call	EVP_PKEY_get0_poly1305@PLT
	movq	%rax, %r13
	jmp	.L4
.L17:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1329:
	.size	pkey_poly1305_ctrl, .-pkey_poly1305_ctrl
	.p2align 4
	.type	poly1305_signctx, @function
poly1305_signctx:
.LFB1328:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	movq	$16, (%rdx)
	testq	%rsi, %rsi
	je	.L24
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$24, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	Poly1305_Final@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore 6
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1328:
	.size	poly1305_signctx, .-poly1305_signctx
	.p2align 4
	.type	poly1305_signctx_init, @function
poly1305_signctx_init:
.LFB1327:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	16(%rdi), %rax
	movq	40(%rax), %rbx
	xorl	%eax, %eax
	cmpl	$32, (%rbx)
	je	.L32
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movq	%rsi, %r12
	movq	40(%rdi), %r13
	movl	$256, %esi
	movq	%r12, %rdi
	call	EVP_MD_CTX_set_flags@PLT
	movq	%r12, %rdi
	leaq	int_update(%rip), %rsi
	call	EVP_MD_CTX_set_update_fn@PLT
	movq	8(%rbx), %rsi
	leaq	24(%r13), %rdi
	call	Poly1305_Init@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1327:
	.size	poly1305_signctx_init, .-poly1305_signctx_init
	.p2align 4
	.type	int_update, @function
int_update:
.LFB1326:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	call	EVP_MD_CTX_pkey_ctx@PLT
	movq	%rax, %rdi
	call	EVP_PKEY_CTX_get_data@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	leaq	24(%rax), %rdi
	call	Poly1305_Update@PLT
	popq	%r12
	movl	$1, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1326:
	.size	int_update, .-int_update
	.p2align 4
	.type	pkey_poly1305_keygen, @function
pkey_poly1305_keygen:
.LFB1325:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	call	EVP_PKEY_CTX_get_data@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	ASN1_STRING_get0_data@PLT
	testq	%rax, %rax
	je	.L37
	movq	%r12, %rdi
	call	ASN1_OCTET_STRING_dup@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L37
	popq	%r12
	movq	%r13, %rdi
	movl	$1061, %esi
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	EVP_PKEY_assign@PLT
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1325:
	.size	pkey_poly1305_keygen, .-pkey_poly1305_keygen
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/poly1305/poly1305_pmeth.c"
	.text
	.p2align 4
	.type	pkey_poly1305_cleanup, @function
pkey_poly1305_cleanup:
.LFB1323:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	call	EVP_PKEY_CTX_get_data@PLT
	testq	%rax, %rax
	je	.L43
	movslq	(%rax), %rsi
	movq	8(%rax), %rdi
	movq	%rax, %r12
	movl	$47, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movq	%r12, %rdi
	movl	$272, %esi
	movl	$48, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	popq	%r12
	movq	%r13, %rdi
	xorl	%esi, %esi
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	EVP_PKEY_CTX_set_data@PLT
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1323:
	.size	pkey_poly1305_cleanup, .-pkey_poly1305_cleanup
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"key"
.LC2:
	.string	"hexkey"
	.text
	.p2align 4
	.type	pkey_poly1305_ctrl_str, @function
pkey_poly1305_ctrl_str:
.LFB1330:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rax
	testq	%rdx, %rdx
	je	.L49
	movl	$4, %ecx
	leaq	.LC1(%rip), %rdi
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L51
	movq	%rax, %rsi
	movl	$7, %ecx
	leaq	.LC2(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L50
	movl	$6, %esi
	movq	%r8, %rdi
	jmp	EVP_PKEY_CTX_hex2ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L49:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	movl	$6, %esi
	movq	%r8, %rdi
	jmp	EVP_PKEY_CTX_str2ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L50:
	movl	$-2, %eax
	ret
	.cfi_endproc
.LFE1330:
	.size	pkey_poly1305_ctrl_str, .-pkey_poly1305_ctrl_str
	.p2align 4
	.type	pkey_poly1305_init, @function
pkey_poly1305_init:
.LFB1322:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$31, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$272, %edi
	subq	$8, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L56
	movl	$4, 4(%rax)
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	EVP_PKEY_CTX_set_data@PLT
	movq	%r12, %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	EVP_PKEY_CTX_set0_keygen_info@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	movl	$32, %r8d
	movl	$65, %edx
	movl	$124, %esi
	movl	$15, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1322:
	.size	pkey_poly1305_init, .-pkey_poly1305_init
	.p2align 4
	.type	pkey_poly1305_copy, @function
pkey_poly1305_copy:
.LFB1324:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$31, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	leaq	.LC0(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movl	$272, %edi
	pushq	%rbx
	.cfi_offset 3, -48
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L68
	movl	$4, 4(%rax)
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	EVP_PKEY_CTX_set_data@PLT
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	EVP_PKEY_CTX_set0_keygen_info@PLT
	movq	%r13, %rdi
	call	EVP_PKEY_CTX_get_data@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	EVP_PKEY_CTX_get_data@PLT
	movq	%rbx, %rdi
	movq	%rax, %r13
	call	ASN1_STRING_get0_data@PLT
	testq	%rax, %rax
	je	.L60
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	ASN1_STRING_copy@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L69
.L60:
	movdqu	24(%rbx), %xmm0
	movl	$1, %r14d
	movups	%xmm0, 24(%r13)
	movdqu	40(%rbx), %xmm1
	movups	%xmm1, 40(%r13)
	movdqu	56(%rbx), %xmm2
	movups	%xmm2, 56(%r13)
	movdqu	72(%rbx), %xmm3
	movups	%xmm3, 72(%r13)
	movdqu	88(%rbx), %xmm4
	movups	%xmm4, 88(%r13)
	movdqu	104(%rbx), %xmm5
	movups	%xmm5, 104(%r13)
	movdqu	120(%rbx), %xmm6
	movups	%xmm6, 120(%r13)
	movdqu	136(%rbx), %xmm7
	movups	%xmm7, 136(%r13)
	movdqu	152(%rbx), %xmm0
	movups	%xmm0, 152(%r13)
	movdqu	168(%rbx), %xmm1
	movups	%xmm1, 168(%r13)
	movdqu	184(%rbx), %xmm2
	movups	%xmm2, 184(%r13)
	movdqu	200(%rbx), %xmm3
	movups	%xmm3, 200(%r13)
	movdqu	216(%rbx), %xmm4
	movups	%xmm4, 216(%r13)
	movdqu	232(%rbx), %xmm5
	movups	%xmm5, 232(%r13)
	movdqu	248(%rbx), %xmm6
	movups	%xmm6, 248(%r13)
	movq	264(%rbx), %rdx
	movq	%rdx, 264(%r13)
.L57:
	popq	%rbx
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	movq	%r12, %rdi
	call	EVP_PKEY_CTX_get_data@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L57
	movslq	(%rax), %rsi
	movq	8(%rax), %rdi
	movl	$47, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movq	%r13, %rdi
	movl	$48, %ecx
	leaq	.LC0(%rip), %rdx
	movl	$272, %esi
	call	CRYPTO_clear_free@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	EVP_PKEY_CTX_set_data@PLT
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L68:
	xorl	%r14d, %r14d
	movl	$32, %r8d
	movl	$65, %edx
	movl	$124, %esi
	leaq	.LC0(%rip), %rcx
	movl	$15, %edi
	call	ERR_put_error@PLT
	popq	%rbx
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1324:
	.size	pkey_poly1305_copy, .-pkey_poly1305_copy
	.globl	poly1305_pkey_meth
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	poly1305_pkey_meth, @object
	.size	poly1305_pkey_meth, 256
poly1305_pkey_meth:
	.long	1061
	.long	4
	.quad	pkey_poly1305_init
	.quad	pkey_poly1305_copy
	.quad	pkey_poly1305_cleanup
	.quad	0
	.quad	0
	.quad	0
	.quad	pkey_poly1305_keygen
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	poly1305_signctx_init
	.quad	poly1305_signctx
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	pkey_poly1305_ctrl
	.quad	pkey_poly1305_ctrl_str
	.zero	48
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
