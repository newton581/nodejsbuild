	.file	"rsa_sign.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/rsa/rsa_sign.c"
	.text
	.p2align 4
	.type	encode_pkcs1, @function
encode_pkcs1:
.LFB805:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movl	%edx, %edi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%r8d, %ebx
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	-96(%rbp), %rax
	movq	$0, -120(%rbp)
	movq	%rax, -112(%rbp)
	call	OBJ_nid2obj@PLT
	movq	%rax, -96(%rbp)
	movq	-112(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L9
	call	OBJ_length@PLT
	testq	%rax, %rax
	je	.L10
	movq	-112(%rbp), %rax
	leaq	-80(%rbp), %rdx
	leaq	-120(%rbp), %rsi
	movl	%ebx, -64(%rbp)
	movq	$0, -72(%rbp)
	leaq	-112(%rbp), %rdi
	movq	%rdx, 8(%rax)
	leaq	-64(%rbp), %rax
	movl	$5, -80(%rbp)
	movq	%rax, -104(%rbp)
	movq	%r14, -56(%rbp)
	call	i2d_X509_SIG@PLT
	xorl	%r8d, %r8d
	testl	%eax, %eax
	js	.L1
	movq	-120(%rbp), %rdx
	movl	$1, %r8d
	movq	%rdx, 0(%r13)
	movl	%eax, (%r12)
.L1:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L11
	addq	$96, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movl	$48, %r8d
	movl	$116, %edx
	movl	$146, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r8d, %r8d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$44, %r8d
	movl	$117, %edx
	movl	$146, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r8d, %r8d
	jmp	.L1
.L11:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE805:
	.size	encode_pkcs1, .-encode_pkcs1
	.p2align 4
	.globl	RSA_sign
	.type	RSA_sign, @function
RSA_sign:
.LFB806:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%r8, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%r9), %rax
	movl	$0, -52(%rbp)
	movq	$0, -48(%rbp)
	movq	88(%rax), %rax
	testq	%rax, %rax
	je	.L13
	call	*%rax
	movl	%eax, %r12d
.L12:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L25
	addq	$32, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	cmpl	$114, %edi
	jne	.L15
	cmpl	$36, %edx
	jne	.L26
	movl	$36, -52(%rbp)
.L17:
	movq	%r12, %rdi
	call	RSA_size@PLT
	movl	-52(%rbp), %edi
	subl	$10, %eax
	cmpl	%edi, %eax
	jle	.L27
	movl	$1, %r8d
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	RSA_private_encrypt@PLT
	testl	%eax, %eax
	jle	.L24
	movl	%eax, (%rbx)
	movl	$1, %r12d
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L15:
	leaq	-48(%rbp), %r9
	movl	%edx, %r8d
	movq	%r14, %rcx
	movl	%edi, %edx
	leaq	-52(%rbp), %rsi
	movq	%r9, %rdi
	call	encode_pkcs1
	movq	-48(%rbp), %r14
	testl	%eax, %eax
	jne	.L17
.L24:
	xorl	%r12d, %r12d
.L19:
	movslq	-52(%rbp), %rsi
	movq	-48(%rbp), %rdi
	movl	$112, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L27:
	movl	$100, %r8d
	movl	$112, %edx
	movl	$117, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L26:
	movl	$88, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$131, %edx
	xorl	%r12d, %r12d
	movl	$117, %esi
	movl	$4, %edi
	call	ERR_put_error@PLT
	jmp	.L12
.L25:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE806:
	.size	RSA_sign, .-RSA_sign
	.p2align 4
	.globl	int_rsa_verify
	.type	int_rsa_verify, @function
int_rsa_verify:
.LFB807:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$72, %rsp
	movq	%rsi, -104(%rbp)
	movq	24(%rbp), %r14
	movq	%rcx, -96(%rbp)
	movq	16(%rbp), %r13
	movq	%r8, -112(%rbp)
	movq	%r14, %rdi
	movq	%r9, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -68(%rbp)
	movq	$0, -64(%rbp)
	call	RSA_size@PLT
	cltq
	cmpq	%r13, %rax
	je	.L29
	movl	$132, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$119, %edx
	xorl	%r12d, %r12d
	movl	$145, %esi
	movl	$4, %edi
	call	ERR_put_error@PLT
.L28:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L71
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movl	$137, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_malloc@PLT
	movl	$139, %r8d
	movl	$65, %edx
	leaq	.LC0(%rip), %rcx
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L67
	movq	-88(%rbp), %rsi
	movq	%r14, %rcx
	movl	$1, %r8d
	movq	%rax, %rdx
	movl	%r13d, %edi
	call	RSA_public_decrypt@PLT
	movslq	%eax, %r14
	testl	%r14d, %r14d
	jle	.L68
	cmpl	$114, %r12d
	je	.L72
	cmpl	$95, %r12d
	jne	.L41
	cmpl	$18, %r14d
	jne	.L41
	cmpb	$4, (%r15)
	je	.L73
.L41:
	cmpq	$0, -96(%rbp)
	je	.L47
	movl	%r12d, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	movl	$202, %r8d
	movl	$117, %edx
	leaq	.LC0(%rip), %rcx
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L67
	call	EVP_MD_size@PLT
	movl	%eax, %edx
	movslq	%r14d, %rax
	movq	%rax, %rsi
	movq	%rdx, %rbx
	subq	%rdx, %rsi
	leaq	(%r15,%rsi), %rcx
	movq	%rcx, -104(%rbp)
	cmpq	%rax, %rdx
	ja	.L74
.L47:
	movq	-104(%rbp), %rcx
	movl	%r12d, %edx
	leaq	-68(%rbp), %rsi
	leaq	-64(%rbp), %rdi
	movl	%ebx, %r8d
	call	encode_pkcs1
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L75
	cmpl	%r14d, -68(%rbp)
	jne	.L51
	movq	-64(%rbp), %r8
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r8, %rdi
	movq	%r8, -88(%rbp)
	call	memcmp@PLT
	movq	-88(%rbp), %r8
	testl	%eax, %eax
	jne	.L51
	movq	-96(%rbp), %rdi
	movl	$1, %r12d
	testq	%rdi, %rdi
	je	.L32
	movq	-104(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r8, -88(%rbp)
	call	memcpy@PLT
	movq	-112(%rbp), %rax
	movq	-88(%rbp), %r8
	movq	%rbx, (%rax)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L51:
	movl	$220, %r8d
.L69:
	leaq	.LC0(%rip), %rcx
	movl	$104, %edx
.L67:
	movl	$145, %esi
	movl	$4, %edi
	call	ERR_put_error@PLT
.L68:
	movq	-64(%rbp), %r8
	movslq	-68(%rbp), %r14
	xorl	%r12d, %r12d
.L32:
	movq	%r14, %rsi
	movq	%r8, %rdi
	movl	$234, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movl	$235, %ecx
	movq	%r13, %rsi
	movq	%r15, %rdi
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L72:
	movl	$155, %r8d
	cmpl	$36, %r14d
	jne	.L69
	movq	-96(%rbp), %rax
	testq	%rax, %rax
	je	.L36
	movdqu	(%r15), %xmm0
	movq	%rax, %rbx
	movq	-64(%rbp), %r8
	movl	$1, %r12d
	movslq	-68(%rbp), %r14
	movups	%xmm0, (%rax)
	movdqu	16(%r15), %xmm1
	movups	%xmm1, 16(%rax)
	movl	32(%r15), %eax
	movl	%eax, 32(%rbx)
	movq	-112(%rbp), %rax
	movq	$36, (%rax)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$164, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$131, %edx
	cmpl	$36, %ebx
	jne	.L67
	movq	-104(%rbp), %rbx
	movq	(%r15), %rax
	movq	8(%r15), %rdx
	xorq	(%rbx), %rax
	xorq	8(%rbx), %rdx
	orq	%rax, %rdx
	jne	.L38
	movq	16(%r15), %rax
	movq	24(%r15), %rdx
	xorq	16(%rbx), %rax
	xorq	24(%rbx), %rdx
	orq	%rax, %rdx
	jne	.L38
	movl	32(%rbx), %eax
	cmpl	%eax, 32(%r15)
	je	.L70
	.p2align 4,,10
	.p2align 3
.L38:
	movl	$169, %r8d
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L75:
	movq	-64(%rbp), %r8
	movslq	-68(%rbp), %r14
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L73:
	cmpb	$16, 1(%r15)
	jne	.L41
	movq	-96(%rbp), %rax
	testq	%rax, %rax
	je	.L42
	movdqu	2(%r15), %xmm2
	movups	%xmm2, (%rax)
	movq	-112(%rbp), %rax
	movq	$16, (%rax)
.L70:
	movq	-64(%rbp), %r8
	movslq	-68(%rbp), %r14
	movl	$1, %r12d
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L74:
	movl	$208, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$143, %edx
	jmp	.L67
.L42:
	movl	$184, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$131, %edx
	cmpl	$16, %ebx
	jne	.L67
	movq	-104(%rbp), %rbx
	movl	$189, %r8d
	movq	(%rbx), %rax
	movq	8(%rbx), %rdx
	xorq	2(%r15), %rax
	xorq	10(%r15), %rdx
	orq	%rax, %rdx
	jne	.L69
	jmp	.L70
.L71:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE807:
	.size	int_rsa_verify, .-int_rsa_verify
	.p2align 4
	.globl	RSA_verify
	.type	RSA_verify, @function
RSA_verify:
.LFB808:
	.cfi_startproc
	endbr64
	movq	8(%r9), %rax
	movq	96(%rax), %rax
	testq	%rax, %rax
	je	.L77
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L77:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r9
	movq	%rcx, %r9
	xorl	%ecx, %ecx
	pushq	%r8
	xorl	%r8d, %r8d
	call	int_rsa_verify
	popq	%rdx
	popq	%rcx
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE808:
	.size	RSA_verify, .-RSA_verify
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
