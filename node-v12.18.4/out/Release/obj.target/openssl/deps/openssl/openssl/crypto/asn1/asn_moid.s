	.file	"asn_moid.c"
	.text
	.p2align 4
	.type	oid_module_finish, @function
oid_module_finish:
.LFB889:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE889:
	.size	oid_module_finish, .-oid_module_finish
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/asn1/asn_moid.c"
	.text
	.p2align 4
	.type	oid_module_init, @function
oid_module_init:
.LFB888:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$24, %rsp
	call	CONF_imodule_get_value@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	NCONF_get_section@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L29
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L30
	.p2align 4,,10
	.p2align 3
.L18:
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	movl	$44, %esi
	movq	16(%rax), %r13
	movq	8(%rax), %rcx
	movq	%r13, %rdi
	movq	%rcx, -56(%rbp)
	call	strrchr@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L20
	cmpb	$0, 1(%rax)
	je	.L8
	leaq	1(%rax), %r14
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L10:
	addq	$1, %r14
.L9:
	movsbl	(%r14), %edi
	movl	$8, %esi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	jne	.L10
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L12:
	addq	$1, %r13
.L11:
	movsbl	0(%r13), %edi
	movl	$8, %esi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	jne	.L12
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L15:
	cmpq	%r12, %r13
	je	.L8
.L28:
	movsbl	-1(%r12), %edi
	movl	$8, %esi
	subq	$1, %r12
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	jne	.L15
	addq	$1, %r12
	movl	$86, %edx
	leaq	.LC0(%rip), %rsi
	subq	%r13, %r12
	leaq	1(%r12), %rdi
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L31
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%r14, %r13
	call	memcpy@PLT
	movb	$0, (%rax,%r12)
	movq	%rax, %r8
	movq	%rax, %r12
.L6:
	movq	-56(%rbp), %rsi
	movq	%r13, %rdi
	movq	%r8, %rdx
	call	OBJ_create@PLT
	movl	$97, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	CRYPTO_free@PLT
	testl	%r13d, %r13d
	je	.L8
	movq	%r15, %rdi
	addl	$1, %ebx
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L18
.L30:
	movl	$1, %eax
.L3:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movq	-56(%rbp), %r8
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L31:
	movl	$87, %r8d
	movl	$65, %edx
	movl	$124, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L8:
	movl	$38, %r8d
	movl	$171, %edx
	movl	$174, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movl	$32, %r8d
	movl	$172, %edx
	movl	$174, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L3
	.cfi_endproc
.LFE888:
	.size	oid_module_init, .-oid_module_init
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"oid_section"
	.text
	.p2align 4
	.globl	ASN1_add_oid_module
	.type	ASN1_add_oid_module, @function
ASN1_add_oid_module:
.LFB890:
	.cfi_startproc
	endbr64
	leaq	oid_module_finish(%rip), %rdx
	leaq	oid_module_init(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	jmp	CONF_module_add@PLT
	.cfi_endproc
.LFE890:
	.size	ASN1_add_oid_module, .-ASN1_add_oid_module
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
