	.file	"ui_util.c"
	.text
	.p2align 4
	.type	ui_new_method_data, @function
ui_new_method_data:
.LFB755:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE755:
	.size	ui_new_method_data, .-ui_new_method_data
	.p2align 4
	.type	ui_open, @function
ui_open:
.LFB760:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE760:
	.size	ui_open, .-ui_open
	.p2align 4
	.type	ui_write, @function
ui_write:
.LFB762:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE762:
	.size	ui_write, .-ui_write
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ui/ui_util.c"
	.text
	.p2align 4
	.type	ui_free_method_data, @function
ui_free_method_data:
.LFB757:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdi
	movl	$85, %edx
	leaq	.LC0(%rip), %rsi
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE757:
	.size	ui_free_method_data, .-ui_free_method_data
	.p2align 4
	.type	ui_method_data_index_init_ossl_, @function
ui_method_data_index_init_ossl_:
.LFB758:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	ui_free_method_data(%rip), %r9
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	ui_dup_method_data(%rip), %r8
	leaq	ui_new_method_data(%rip), %rcx
	movl	$14, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	CRYPTO_get_ex_new_index@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	$1, ui_method_data_index_init_ossl_ret_(%rip)
	movl	%eax, ui_method_data_index(%rip)
	ret
	.cfi_endproc
.LFE758:
	.size	ui_method_data_index_init_ossl_, .-ui_method_data_index_init_ossl_
	.p2align 4
	.type	ui_dup_method_data, @function
ui_dup_method_data:
.LFB756:
	.cfi_startproc
	endbr64
	movq	(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L14
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$78, %ecx
	movl	$16, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdx, %rbx
	leaq	.LC0(%rip), %rdx
	subq	$8, %rsp
	call	CRYPTO_memdup@PLT
	movq	%rax, (%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore 3
	.cfi_restore 6
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE756:
	.size	ui_dup_method_data, .-ui_dup_method_data
	.p2align 4
	.type	ui_read.part.0, @function
ui_read.part.0:
.LFB765:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$1048, %rsp
	.cfi_offset 3, -56
	movl	ui_method_data_index(%rip), %r14d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	UI_get_method@PLT
	movl	%r14d, %esi
	movq	%rax, %rdi
	call	UI_method_get_ex_data@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	UI_get_result_maxsize@PLT
	movq	%r13, %rdi
	movq	(%r14), %r15
	movl	%eax, %ebx
	call	UI_get0_user_data@PLT
	cmpl	$1024, %ebx
	movl	8(%r14), %edx
	movl	$1024, %esi
	leaq	-1088(%rbp), %r14
	cmovle	%ebx, %esi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	*%r15
	testl	%eax, %eax
	js	.L17
	movslq	%eax, %rdx
	movb	$0, -1088(%rbp,%rdx)
	je	.L17
	movl	%eax, %ecx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	UI_set_result_ex@PLT
	notl	%eax
	shrl	$31, %eax
.L17:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L24
	addq	$1048, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L24:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE765:
	.size	ui_read.part.0, .-ui_read.part.0
	.p2align 4
	.type	ui_read, @function
ui_read:
.LFB761:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	call	UI_get_string_type@PLT
	cmpl	$1, %eax
	je	.L28
	popq	%r12
	movl	$1, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	ui_read.part.0
	.cfi_endproc
.LFE761:
	.size	ui_read, .-ui_read
	.p2align 4
	.type	ui_close, @function
ui_close:
.LFB767:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE767:
	.size	ui_close, .-ui_close
	.p2align 4
	.globl	UI_UTIL_read_pw_string
	.type	UI_UTIL_read_pw_string, @function
UI_UTIL_read_pw_string:
.LFB753:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%ecx, -8264(%rbp)
	movq	%rdx, %r12
	movq	%fs:40, %rdx
	movq	%rdx, -56(%rbp)
	xorl	%edx, %edx
	testl	%esi, %esi
	jle	.L35
	movq	%rdi, %r13
	movl	%esi, %ebx
	call	UI_new@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L36
	cmpl	$8192, %ebx
	movl	$8192, %r9d
	movq	%r13, %rcx
	movq	%r12, %rsi
	cmovg	%r9d, %ebx
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%rax, -8272(%rbp)
	leaq	-8256(%rbp), %r14
	subl	$1, %ebx
	movl	%ebx, %r9d
	call	UI_add_input_string@PLT
	movq	-8272(%rbp), %rdi
	testl	%eax, %eax
	movl	%eax, %r15d
	js	.L33
	movl	-8264(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L42
.L32:
	testl	%r15d, %r15d
	js	.L33
	movq	%rdi, -8264(%rbp)
	call	UI_process@PLT
	movq	-8264(%rbp), %rdi
	movl	%eax, %r15d
.L33:
	call	UI_free@PLT
	testl	%r15d, %r15d
	movl	$0, %eax
	cmovg	%eax, %r15d
.L31:
	movl	$8192, %esi
	movq	%r14, %rdi
	call	OPENSSL_cleanse@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L43
	leaq	-40(%rbp), %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	xorl	%r15d, %r15d
	leaq	-8256(%rbp), %r14
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L42:
	subq	$8, %rsp
	xorl	%edx, %edx
	movl	%ebx, %r9d
	xorl	%r8d, %r8d
	pushq	%r13
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	%rdi, -8264(%rbp)
	call	UI_add_verify_string@PLT
	movq	-8264(%rbp), %rdi
	movl	%eax, %r15d
	popq	%rax
	popq	%rdx
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L35:
	movl	$-1, %r15d
	leaq	-8256(%rbp), %r14
	jmp	.L31
.L43:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE753:
	.size	UI_UTIL_read_pw_string, .-UI_UTIL_read_pw_string
	.p2align 4
	.globl	UI_UTIL_read_pw
	.type	UI_UTIL_read_pw, @function
UI_UTIL_read_pw:
.LFB754:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -56(%rbp)
	testl	%edx, %edx
	jle	.L48
	movq	%rdi, %r13
	movl	%edx, %ebx
	movq	%rcx, %r12
	movl	%r8d, %r14d
	call	UI_new@PLT
	movq	%rax, %r15
	xorl	%eax, %eax
	testq	%r15, %r15
	je	.L44
	leal	-1(%rbx), %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movl	%r9d, -60(%rbp)
	call	UI_add_input_string@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	js	.L47
	testl	%r14d, %r14d
	jne	.L55
.L46:
	testl	%ebx, %ebx
	js	.L47
	movq	%r15, %rdi
	call	UI_process@PLT
	movl	%eax, %ebx
.L47:
	movq	%r15, %rdi
	call	UI_free@PLT
	testl	%ebx, %ebx
	movl	$0, %eax
	cmovle	%ebx, %eax
.L44:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	subq	$8, %rsp
	movl	-60(%rbp), %r9d
	movq	-56(%rbp), %rcx
	xorl	%edx, %edx
	pushq	%r13
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	UI_add_verify_string@PLT
	movl	%eax, %ebx
	popq	%rax
	popq	%rdx
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L48:
	movl	$-1, %eax
	jmp	.L44
	.cfi_endproc
.LFE754:
	.size	UI_UTIL_read_pw, .-UI_UTIL_read_pw
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"PEM password callback wrapper"
	.text
	.p2align 4
	.globl	UI_UTIL_wrap_read_pem_callback
	.type	UI_UTIL_wrap_read_pem_callback, @function
UI_UTIL_wrap_read_pem_callback:
.LFB764:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$147, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	leaq	.LC0(%rip), %rsi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movl	$16, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L59
	leaq	.LC1(%rip), %rdi
	call	UI_create_method@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L59
	leaq	ui_open(%rip), %rsi
	movq	%rax, %rdi
	call	UI_method_set_opener@PLT
	testl	%eax, %eax
	js	.L58
	leaq	ui_read(%rip), %rsi
	movq	%r12, %rdi
	call	UI_method_set_reader@PLT
	testl	%eax, %eax
	js	.L58
	leaq	ui_write(%rip), %rsi
	movq	%r12, %rdi
	call	UI_method_set_writer@PLT
	testl	%eax, %eax
	js	.L58
	leaq	ui_close(%rip), %rsi
	movq	%r12, %rdi
	call	UI_method_set_closer@PLT
	testl	%eax, %eax
	js	.L58
	leaq	ui_method_data_index_init_ossl_(%rip), %rsi
	leaq	get_index_once(%rip), %rdi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L58
	movl	ui_method_data_index_init_ossl_ret_(%rip), %eax
	testl	%eax, %eax
	je	.L58
	movl	ui_method_data_index(%rip), %esi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	UI_method_set_ex_data@PLT
	testl	%eax, %eax
	js	.L58
	movl	%r14d, 8(%r13)
	testq	%rbx, %rbx
	je	.L70
.L62:
	movq	%rbx, 0(%r13)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	xorl	%r12d, %r12d
.L58:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	UI_destroy_method@PLT
	movq	%r13, %rdi
	movl	$156, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	movq	PEM_def_callback@GOTPCREL(%rip), %rbx
	jmp	.L62
	.cfi_endproc
.LFE764:
	.size	UI_UTIL_wrap_read_pem_callback, .-UI_UTIL_wrap_read_pem_callback
	.local	ui_method_data_index_init_ossl_ret_
	.comm	ui_method_data_index_init_ossl_ret_,4,4
	.data
	.align 4
	.type	ui_method_data_index, @object
	.size	ui_method_data_index, 4
ui_method_data_index:
	.long	-1
	.local	get_index_once
	.comm	get_index_once,4,4
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
