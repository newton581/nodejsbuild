	.file	"v3_pcia.c"
	.text
	.p2align 4
	.globl	d2i_PROXY_POLICY
	.type	d2i_PROXY_POLICY, @function
d2i_PROXY_POLICY:
.LFB1259:
	.cfi_startproc
	endbr64
	leaq	PROXY_POLICY_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1259:
	.size	d2i_PROXY_POLICY, .-d2i_PROXY_POLICY
	.p2align 4
	.globl	i2d_PROXY_POLICY
	.type	i2d_PROXY_POLICY, @function
i2d_PROXY_POLICY:
.LFB1260:
	.cfi_startproc
	endbr64
	leaq	PROXY_POLICY_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1260:
	.size	i2d_PROXY_POLICY, .-i2d_PROXY_POLICY
	.p2align 4
	.globl	PROXY_POLICY_new
	.type	PROXY_POLICY_new, @function
PROXY_POLICY_new:
.LFB1261:
	.cfi_startproc
	endbr64
	leaq	PROXY_POLICY_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1261:
	.size	PROXY_POLICY_new, .-PROXY_POLICY_new
	.p2align 4
	.globl	PROXY_POLICY_free
	.type	PROXY_POLICY_free, @function
PROXY_POLICY_free:
.LFB1262:
	.cfi_startproc
	endbr64
	leaq	PROXY_POLICY_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1262:
	.size	PROXY_POLICY_free, .-PROXY_POLICY_free
	.p2align 4
	.globl	d2i_PROXY_CERT_INFO_EXTENSION
	.type	d2i_PROXY_CERT_INFO_EXTENSION, @function
d2i_PROXY_CERT_INFO_EXTENSION:
.LFB1263:
	.cfi_startproc
	endbr64
	leaq	PROXY_CERT_INFO_EXTENSION_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1263:
	.size	d2i_PROXY_CERT_INFO_EXTENSION, .-d2i_PROXY_CERT_INFO_EXTENSION
	.p2align 4
	.globl	i2d_PROXY_CERT_INFO_EXTENSION
	.type	i2d_PROXY_CERT_INFO_EXTENSION, @function
i2d_PROXY_CERT_INFO_EXTENSION:
.LFB1264:
	.cfi_startproc
	endbr64
	leaq	PROXY_CERT_INFO_EXTENSION_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1264:
	.size	i2d_PROXY_CERT_INFO_EXTENSION, .-i2d_PROXY_CERT_INFO_EXTENSION
	.p2align 4
	.globl	PROXY_CERT_INFO_EXTENSION_new
	.type	PROXY_CERT_INFO_EXTENSION_new, @function
PROXY_CERT_INFO_EXTENSION_new:
.LFB1265:
	.cfi_startproc
	endbr64
	leaq	PROXY_CERT_INFO_EXTENSION_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1265:
	.size	PROXY_CERT_INFO_EXTENSION_new, .-PROXY_CERT_INFO_EXTENSION_new
	.p2align 4
	.globl	PROXY_CERT_INFO_EXTENSION_free
	.type	PROXY_CERT_INFO_EXTENSION_free, @function
PROXY_CERT_INFO_EXTENSION_free:
.LFB1266:
	.cfi_startproc
	endbr64
	leaq	PROXY_CERT_INFO_EXTENSION_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1266:
	.size	PROXY_CERT_INFO_EXTENSION_free, .-PROXY_CERT_INFO_EXTENSION_free
	.globl	PROXY_CERT_INFO_EXTENSION_it
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"PROXY_CERT_INFO_EXTENSION"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	PROXY_CERT_INFO_EXTENSION_it, @object
	.size	PROXY_CERT_INFO_EXTENSION_it, 56
PROXY_CERT_INFO_EXTENSION_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	PROXY_CERT_INFO_EXTENSION_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC0
	.section	.rodata.str1.1
.LC1:
	.string	"pcPathLengthConstraint"
.LC2:
	.string	"proxyPolicy"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	PROXY_CERT_INFO_EXTENSION_seq_tt, @object
	.size	PROXY_CERT_INFO_EXTENSION_seq_tt, 80
PROXY_CERT_INFO_EXTENSION_seq_tt:
	.quad	1
	.quad	0
	.quad	0
	.quad	.LC1
	.quad	ASN1_INTEGER_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC2
	.quad	PROXY_POLICY_it
	.globl	PROXY_POLICY_it
	.section	.rodata.str1.1
.LC3:
	.string	"PROXY_POLICY"
	.section	.data.rel.ro.local
	.align 32
	.type	PROXY_POLICY_it, @object
	.size	PROXY_POLICY_it, 56
PROXY_POLICY_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	PROXY_POLICY_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC3
	.section	.rodata.str1.1
.LC4:
	.string	"policyLanguage"
.LC5:
	.string	"policy"
	.section	.data.rel.ro
	.align 32
	.type	PROXY_POLICY_seq_tt, @object
	.size	PROXY_POLICY_seq_tt, 80
PROXY_POLICY_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC4
	.quad	ASN1_OBJECT_it
	.quad	1
	.quad	0
	.quad	8
	.quad	.LC5
	.quad	ASN1_OCTET_STRING_it
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
