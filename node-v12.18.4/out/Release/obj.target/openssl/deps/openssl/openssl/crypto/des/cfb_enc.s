	.file	"cfb_enc.c"
	.text
	.p2align 4
	.globl	DES_cfb_encrypt
	.type	DES_cfb_encrypt, @function
DES_cfb_encrypt:
.LFB162:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, -112(%rbp)
	movq	%r9, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leal	-1(%rdx), %eax
	cmpl	$63, %eax
	ja	.L1
	movq	%rsi, %r12
	movq	%r9, %rsi
	leal	7(%rdx), %eax
	movq	%rcx, %r9
	movl	(%rsi), %r13d
	movl	%edx, %ecx
	movl	4(%rsi), %r15d
	movl	%edx, %r10d
	movl	16(%rbp), %esi
	sarl	$3, %ecx
	sarl	$3, %eax
	andl	$7, %edx
	movl	%ecx, -156(%rbp)
	movq	%rdi, %rbx
	movslq	%eax, %r14
	movl	%edx, -140(%rbp)
	testl	%esi, %esi
	je	.L83
	cmpq	%r14, %r9
	jb	.L6
	leaq	-88(%rbp), %rcx
	movl	%r10d, -144(%rbp)
	movq	%r14, %r8
	movq	%rcx, -120(%rbp)
	leaq	-1(%r14), %rcx
	movl	%r15d, %r14d
	movl	%eax, %r15d
	movq	%rcx, -104(%rbp)
	movslq	-156(%rbp), %rcx
	movq	%rcx, -176(%rbp)
	leaq	-80(%rbp,%rcx), %rcx
	movq	%rcx, -168(%rbp)
	.p2align 4,,10
	.p2align 3
.L28:
	movq	-112(%rbp), %rsi
	movq	-120(%rbp), %rdi
	subq	%r8, %r9
	movl	$1, %edx
	movq	%r8, -136(%rbp)
	movq	%r9, -128(%rbp)
	movl	%r13d, -88(%rbp)
	movl	%r14d, -84(%rbp)
	call	DES_encrypt1@PLT
	movq	-136(%rbp), %r8
	movq	-128(%rbp), %r9
	addq	%r8, %rbx
	cmpl	$8, %r15d
	ja	.L52
	leaq	.L9(%rip), %rax
	movl	%r15d, %edx
	movslq	(%rax,%rdx,4), %rdx
	addq	%rax, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L9:
	.long	.L52-.L9
	.long	.L52-.L9
	.long	.L53-.L9
	.long	.L54-.L9
	.long	.L55-.L9
	.long	.L56-.L9
	.long	.L57-.L9
	.long	.L58-.L9
	.long	.L8-.L9
	.text
	.p2align 4,,10
	.p2align 3
.L17:
	movl	%esi, %edi
	subq	$1, %r12
	shrl	$24, %edi
	movb	%dil, (%r12)
.L19:
	movl	%esi, %edi
	subq	$1, %r12
	shrl	$16, %edi
	movb	%dil, (%r12)
.L20:
	movl	%esi, %eax
	leaq	-1(%r12), %rdi
	movzbl	%ah, %eax
	movb	%al, -1(%r12)
.L21:
	movb	%sil, -1(%rdi)
	leaq	-1(%rdi), %r12
.L22:
	movl	%edx, %edi
	leaq	-1(%r12), %r11
	shrl	$24, %edi
	movb	%dil, -1(%r12)
.L23:
	movl	%edx, %r12d
	leaq	-1(%r11), %rdi
	shrl	$16, %r12d
	movb	%r12b, -1(%r11)
.L24:
	movb	%dh, -1(%rdi)
	leaq	-1(%rdi), %r12
.L16:
	movl	-144(%rbp), %eax
	movb	%dl, -1(%r12)
	addq	-104(%rbp), %r12
	cmpl	$32, %eax
	je	.L62
	cmpl	$64, %eax
	je	.L63
	movd	%edx, %xmm1
	movd	%esi, %xmm2
	movd	%r13d, %xmm0
	movl	-140(%rbp), %edx
	movd	%r14d, %xmm3
	punpckldq	%xmm2, %xmm1
	punpckldq	%xmm3, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	testl	%edx, %edx
	jne	.L84
	movq	-168(%rbp), %rax
	movq	(%rax), %rdx
	movq	%rdx, -80(%rbp)
.L27:
	movl	-80(%rbp), %r13d
	movl	-76(%rbp), %r14d
.L25:
	cmpq	%r8, %r9
	jnb	.L28
.L81:
	movl	%r14d, %r15d
.L6:
	movq	-152(%rbp), %rax
	movl	%r13d, (%rax)
	movl	%r15d, 4(%rax)
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L85
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	leaq	-88(%rbp), %rsi
	movslq	%ecx, %rcx
	movq	%rsi, -120(%rbp)
	leaq	-1(%r14), %rsi
	movq	%rcx, -176(%rbp)
	leaq	-80(%rbp,%rcx), %rcx
	movq	%rsi, -104(%rbp)
	movq	%rcx, -168(%rbp)
	cmpq	%r14, %r9
	jb	.L6
	movl	%r10d, -144(%rbp)
	movq	%r14, %r8
	movl	%r15d, %r14d
	movl	%eax, %r15d
	.p2align 4,,10
	.p2align 3
.L50:
	movq	-112(%rbp), %rsi
	movq	-120(%rbp), %rdi
	subq	%r8, %r9
	movl	$1, %edx
	movq	%r8, -136(%rbp)
	movq	%r9, -128(%rbp)
	movl	%r13d, -88(%rbp)
	movl	%r14d, -84(%rbp)
	call	DES_encrypt1@PLT
	movq	-136(%rbp), %r8
	movq	-128(%rbp), %r9
	addq	%r8, %rbx
	cmpl	$8, %r15d
	ja	.L64
	leaq	.L31(%rip), %rax
	movl	%r15d, %edx
	movslq	(%rax,%rdx,4), %rdx
	addq	%rax, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L31:
	.long	.L64-.L31
	.long	.L64-.L31
	.long	.L65-.L31
	.long	.L66-.L31
	.long	.L67-.L31
	.long	.L68-.L31
	.long	.L69-.L31
	.long	.L70-.L31
	.long	.L30-.L31
	.text
	.p2align 4,,10
	.p2align 3
.L70:
	xorl	%edi, %edi
.L32:
	movzbl	-1(%rbx), %esi
	subq	$1, %rbx
	sall	$16, %esi
	orl	%esi, %edi
.L33:
	movzbl	-1(%rbx), %esi
	leaq	-1(%rbx), %rdx
	sall	$8, %esi
	orl	%edi, %esi
.L34:
	leaq	-1(%rdx), %rbx
	movzbl	-1(%rdx), %edx
	orl	%edx, %esi
.L35:
	leaq	-1(%rbx), %rdi
	movzbl	-1(%rbx), %ebx
	sall	$24, %ebx
.L36:
	movzbl	-1(%rdi), %edx
	leaq	-1(%rdi), %r11
	sall	$16, %edx
	movl	%edx, %edi
	orl	%ebx, %edi
.L37:
	movzbl	-1(%r11), %edx
	leaq	-1(%r11), %rbx
	sall	$8, %edx
	orl	%edi, %edx
.L29:
	movzbl	-1(%rbx), %edi
	movl	-144(%rbp), %eax
	addq	-104(%rbp), %rbx
	orl	%edi, %edx
	cmpl	$32, %eax
	je	.L71
	cmpl	$64, %eax
	je	.L72
	movd	%r13d, %xmm0
	movd	%r14d, %xmm4
	movd	%edx, %xmm1
	movl	-140(%rbp), %eax
	movd	%esi, %xmm5
	punpckldq	%xmm4, %xmm0
	punpckldq	%xmm5, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	testl	%eax, %eax
	jne	.L86
	movq	-168(%rbp), %rax
	movq	(%rax), %rcx
	movq	%rcx, -80(%rbp)
.L40:
	movl	-80(%rbp), %r13d
	movl	-76(%rbp), %r14d
.L38:
	xorl	-88(%rbp), %edx
	xorl	-84(%rbp), %esi
	addq	%r8, %r12
	cmpl	$8, %r15d
	ja	.L41
	leaq	.L43(%rip), %r11
	movl	%r15d, %edi
	movslq	(%r11,%rdi,4), %rdi
	addq	%r11, %rdi
	notrack jmp	*%rdi
	.section	.rodata
	.align 4
	.align 4
.L43:
	.long	.L41-.L43
	.long	.L41-.L43
	.long	.L73-.L43
	.long	.L74-.L43
	.long	.L47-.L43
	.long	.L75-.L43
	.long	.L45-.L43
	.long	.L44-.L43
	.long	.L42-.L43
	.text
	.p2align 4,,10
	.p2align 3
.L42:
	movl	%esi, %edi
	subq	$1, %r12
	shrl	$24, %edi
	movb	%dil, (%r12)
.L44:
	movl	%esi, %edi
	subq	$1, %r12
	shrl	$16, %edi
	movb	%dil, (%r12)
.L45:
	movl	%esi, %eax
	leaq	-1(%r12), %rdi
	movzbl	%ah, %eax
	movb	%al, -1(%r12)
.L46:
	movb	%sil, -1(%rdi)
	leaq	-1(%rdi), %r12
.L47:
	movl	%edx, %esi
	leaq	-1(%r12), %rdi
	shrl	$24, %esi
	movb	%sil, -1(%r12)
.L48:
	movl	%edx, %r11d
	leaq	-1(%rdi), %rsi
	shrl	$16, %r11d
	movb	%r11b, -1(%rdi)
.L49:
	movb	%dh, -1(%rsi)
	leaq	-1(%rsi), %r12
.L41:
	movb	%dl, -1(%r12)
	addq	-104(%rbp), %r12
	cmpq	%r8, %r9
	jnb	.L50
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L69:
	xorl	%edi, %edi
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L73:
	movq	%r12, %rsi
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L75:
	movq	%r12, %rdi
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L74:
	movq	%r12, %rdi
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L68:
	movq	%rbx, %rdx
	xorl	%esi, %esi
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L67:
	xorl	%esi, %esi
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L66:
	movq	%rbx, %rdi
	xorl	%esi, %esi
	xorl	%ebx, %ebx
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L65:
	movq	%rbx, %r11
	xorl	%esi, %esi
	xorl	%edi, %edi
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L30:
	movzbl	-1(%rbx), %edi
	subq	$1, %rbx
	sall	$24, %edi
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L58:
	xorl	%edi, %edi
.L10:
	movzbl	-1(%rbx), %esi
	subq	$1, %rbx
	sall	$16, %esi
	orl	%esi, %edi
.L11:
	movzbl	-1(%rbx), %esi
	leaq	-1(%rbx), %rdx
	sall	$8, %esi
	orl	%edi, %esi
.L12:
	leaq	-1(%rdx), %rbx
	movzbl	-1(%rdx), %edx
	orl	%edx, %esi
.L13:
	leaq	-1(%rbx), %rdi
	movzbl	-1(%rbx), %ebx
	sall	$24, %ebx
.L14:
	movzbl	-1(%rdi), %edx
	leaq	-1(%rdi), %r11
	sall	$16, %edx
	orl	%ebx, %edx
	movl	%edx, %edi
.L15:
	movzbl	-1(%r11), %edx
	leaq	-1(%r11), %rbx
	sall	$8, %edx
	orl	%edi, %edx
.L7:
	movzbl	-1(%rbx), %edi
	xorl	-84(%rbp), %esi
	addq	%r8, %r12
	addq	-104(%rbp), %rbx
	orl	%edi, %edx
	xorl	-88(%rbp), %edx
	cmpl	$8, %r15d
	ja	.L16
	leaq	.L18(%rip), %r11
	movl	%r15d, %edi
	movslq	(%r11,%rdi,4), %rdi
	addq	%r11, %rdi
	notrack jmp	*%rdi
	.section	.rodata
	.align 4
	.align 4
.L18:
	.long	.L16-.L18
	.long	.L16-.L18
	.long	.L59-.L18
	.long	.L60-.L18
	.long	.L22-.L18
	.long	.L61-.L18
	.long	.L20-.L18
	.long	.L19-.L18
	.long	.L17-.L18
	.text
	.p2align 4,,10
	.p2align 3
.L60:
	movq	%r12, %r11
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L59:
	movq	%r12, %rdi
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L53:
	movq	%rbx, %r11
	xorl	%esi, %esi
	xorl	%edi, %edi
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L57:
	xorl	%edi, %edi
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L56:
	movq	%rbx, %rdx
	xorl	%esi, %esi
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L55:
	xorl	%esi, %esi
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L54:
	movq	%rbx, %rdi
	xorl	%esi, %esi
	xorl	%ebx, %ebx
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L61:
	movq	%r12, %rdi
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L8:
	movzbl	-1(%rbx), %edi
	subq	$1, %rbx
	sall	$24, %edi
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L71:
	movl	%r14d, %r13d
	movl	%edx, %r14d
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L62:
	movl	%r14d, %r13d
	movl	%edx, %r14d
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L63:
	movl	%esi, %r14d
	movl	%edx, %r13d
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L72:
	movl	%esi, %r14d
	movl	%edx, %r13d
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L84:
	movl	-140(%rbp), %eax
	movq	-176(%rbp), %rcx
	movl	$8, %edx
	movl	-156(%rbp), %r14d
	movzbl	-79(%rbp,%rcx), %esi
	subl	%eax, %edx
	movl	%edx, %ecx
	sarl	%cl, %esi
	movq	-168(%rbp), %rcx
	movzbl	(%rcx), %edi
	movl	%eax, %ecx
	sall	%cl, %edi
	movl	%edx, %ecx
	orl	%edi, %esi
	leal	1(%r14), %edi
	movslq	%edi, %rdi
	movb	%sil, -80(%rbp)
	movzbl	-79(%rbp,%rdi), %esi
	movzbl	-80(%rbp,%rdi), %edi
	sarl	%cl, %esi
	movl	%eax, %ecx
	sall	%cl, %edi
	movl	%edx, %ecx
	orl	%edi, %esi
	leal	2(%r14), %edi
	movslq	%edi, %rdi
	movb	%sil, -79(%rbp)
	movzbl	-79(%rbp,%rdi), %esi
	movzbl	-80(%rbp,%rdi), %edi
	sarl	%cl, %esi
	movl	%eax, %ecx
	sall	%cl, %edi
	movl	%edx, %ecx
	orl	%edi, %esi
	leal	3(%r14), %edi
	movslq	%edi, %rdi
	movb	%sil, -78(%rbp)
	movzbl	-79(%rbp,%rdi), %esi
	movzbl	-80(%rbp,%rdi), %edi
	sarl	%cl, %esi
	movl	%eax, %ecx
	sall	%cl, %edi
	movl	%edx, %ecx
	orl	%edi, %esi
	leal	4(%r14), %edi
	movslq	%edi, %rdi
	movb	%sil, -77(%rbp)
	movzbl	-79(%rbp,%rdi), %esi
	movzbl	-80(%rbp,%rdi), %edi
	sarl	%cl, %esi
	movl	%eax, %ecx
	sall	%cl, %edi
	movl	%edx, %ecx
	orl	%edi, %esi
	leal	5(%r14), %edi
	movslq	%edi, %rdi
	movb	%sil, -76(%rbp)
	movzbl	-79(%rbp,%rdi), %esi
	movzbl	-80(%rbp,%rdi), %edi
	sarl	%cl, %esi
	movl	%eax, %ecx
	sall	%cl, %edi
	movl	%edx, %ecx
	orl	%edi, %esi
	leal	6(%r14), %edi
	movslq	%edi, %rdi
	movb	%sil, -75(%rbp)
	movzbl	-79(%rbp,%rdi), %esi
	movzbl	-80(%rbp,%rdi), %edi
	sarl	%cl, %esi
	movl	%eax, %ecx
	sall	%cl, %edi
	movl	%edx, %ecx
	orl	%edi, %esi
	leal	7(%r14), %edi
	movslq	%edi, %rdi
	movb	%sil, -74(%rbp)
	movzbl	-79(%rbp,%rdi), %esi
	sarl	%cl, %esi
	movl	%eax, %ecx
	movl	%esi, %edx
	movzbl	-80(%rbp,%rdi), %esi
	sall	%cl, %esi
	orl	%esi, %edx
	movb	%dl, -73(%rbp)
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L86:
	movl	-140(%rbp), %eax
	movq	-176(%rbp), %rcx
	movl	$8, %edi
	movl	-156(%rbp), %r14d
	movzbl	-79(%rbp,%rcx), %r10d
	subl	%eax, %edi
	movl	%edi, %ecx
	sarl	%cl, %r10d
	movq	-168(%rbp), %rcx
	movzbl	(%rcx), %r11d
	movl	%eax, %ecx
	sall	%cl, %r11d
	movl	%edi, %ecx
	orl	%r11d, %r10d
	leal	1(%r14), %r11d
	movslq	%r11d, %r11
	movb	%r10b, -80(%rbp)
	movzbl	-79(%rbp,%r11), %r10d
	movzbl	-80(%rbp,%r11), %r11d
	sarl	%cl, %r10d
	movl	%eax, %ecx
	sall	%cl, %r11d
	movl	%edi, %ecx
	orl	%r11d, %r10d
	leal	2(%r14), %r11d
	movslq	%r11d, %r11
	movb	%r10b, -79(%rbp)
	movzbl	-79(%rbp,%r11), %r10d
	movzbl	-80(%rbp,%r11), %r11d
	sarl	%cl, %r10d
	movl	%eax, %ecx
	sall	%cl, %r11d
	movl	%edi, %ecx
	orl	%r11d, %r10d
	leal	3(%r14), %r11d
	movslq	%r11d, %r11
	movb	%r10b, -78(%rbp)
	movzbl	-79(%rbp,%r11), %r10d
	movzbl	-80(%rbp,%r11), %r11d
	sarl	%cl, %r10d
	movl	%eax, %ecx
	sall	%cl, %r11d
	movl	%edi, %ecx
	orl	%r11d, %r10d
	leal	4(%r14), %r11d
	movslq	%r11d, %r11
	movb	%r10b, -77(%rbp)
	movzbl	-79(%rbp,%r11), %r10d
	movzbl	-80(%rbp,%r11), %r11d
	sarl	%cl, %r10d
	movl	%eax, %ecx
	sall	%cl, %r11d
	movl	%edi, %ecx
	orl	%r11d, %r10d
	leal	5(%r14), %r11d
	movslq	%r11d, %r11
	movb	%r10b, -76(%rbp)
	movzbl	-79(%rbp,%r11), %r10d
	movzbl	-80(%rbp,%r11), %r11d
	sarl	%cl, %r10d
	movl	%eax, %ecx
	sall	%cl, %r11d
	movl	%edi, %ecx
	orl	%r11d, %r10d
	leal	6(%r14), %r11d
	movslq	%r11d, %r11
	movb	%r10b, -75(%rbp)
	movzbl	-79(%rbp,%r11), %r10d
	movzbl	-80(%rbp,%r11), %r11d
	sarl	%cl, %r10d
	movl	%eax, %ecx
	sall	%cl, %r11d
	movl	%edi, %ecx
	orl	%r11d, %r10d
	movb	%r10b, -74(%rbp)
	leal	7(%r14), %r10d
	movslq	%r10d, %r10
	movzbl	-79(%rbp,%r10), %r11d
	movzbl	-80(%rbp,%r10), %r10d
	sarl	%cl, %r11d
	movl	%eax, %ecx
	movl	%r11d, %edi
	sall	%cl, %r10d
	orl	%r10d, %edi
	movb	%dil, -73(%rbp)
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L52:
	xorl	%esi, %esi
	xorl	%edx, %edx
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L64:
	xorl	%esi, %esi
	xorl	%edx, %edx
	jmp	.L29
.L85:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE162:
	.size	DES_cfb_encrypt, .-DES_cfb_encrypt
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
