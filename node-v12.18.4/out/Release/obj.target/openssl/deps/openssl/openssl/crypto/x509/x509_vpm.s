	.file	"x509_vpm.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/x509/x509_vpm.c"
	.text
	.p2align 4
	.type	str_free, @function
str_free:
.LFB1371:
	.cfi_startproc
	endbr64
	movl	$33, %edx
	leaq	.LC0(%rip), %rsi
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE1371:
	.size	str_free, .-str_free
	.p2align 4
	.type	str_copy, @function
str_copy:
.LFB1370:
	.cfi_startproc
	endbr64
	movl	$28, %edx
	leaq	.LC0(%rip), %rsi
	jmp	CRYPTO_strdup@PLT
	.cfi_endproc
.LFE1370:
	.size	str_copy, .-str_copy
	.p2align 4
	.type	param_cmp, @function
param_cmp:
.LFB1407:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rdx
	movq	(%rdi), %rax
	movq	(%rdx), %rsi
	movq	(%rax), %rdi
	jmp	strcmp@PLT
	.cfi_endproc
.LFE1407:
	.size	param_cmp, .-param_cmp
	.p2align 4
	.type	table_cmp_BSEARCH_CMP_FN, @function
table_cmp_BSEARCH_CMP_FN:
.LFB1405:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rsi
	movq	(%rdi), %rdi
	jmp	strcmp@PLT
	.cfi_endproc
.LFE1405:
	.size	table_cmp_BSEARCH_CMP_FN, .-table_cmp_BSEARCH_CMP_FN
	.p2align 4
	.globl	X509_VERIFY_PARAM_free
	.type	X509_VERIFY_PARAM_free, @function
X509_VERIFY_PARAM_free:
.LFB1374:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L6
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	48(%rdi), %rdi
	movq	ASN1_OBJECT_free@GOTPCREL(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	56(%r12), %rdi
	leaq	str_free(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	72(%r12), %rdi
	movl	$104, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	80(%r12), %rdi
	movl	$105, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	96(%r12), %rdi
	movl	$106, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$107, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L6:
	ret
	.cfi_endproc
.LFE1374:
	.size	X509_VERIFY_PARAM_free, .-X509_VERIFY_PARAM_free
	.p2align 4
	.globl	X509_VERIFY_PARAM_new
	.type	X509_VERIFY_PARAM_new, @function
X509_VERIFY_PARAM_new:
.LFB1373:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$86, %edx
	movl	$112, %edi
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L15
	movl	$0, 36(%rax)
	movq	$-1, 40(%rax)
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	movl	$88, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$159, %esi
	movl	$11, %edi
	movq	%rax, -8(%rbp)
	call	ERR_put_error@PLT
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1373:
	.size	X509_VERIFY_PARAM_new, .-X509_VERIFY_PARAM_new
	.p2align 4
	.globl	X509_VERIFY_PARAM_set1_name
	.type	X509_VERIFY_PARAM_set1_name, @function
X509_VERIFY_PARAM_set1_name:
.LFB1378:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$263, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	leaq	.LC0(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	movl	$264, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	testq	%rax, %rax
	movq	%rax, (%rbx)
	popq	%rbx
	setne	%al
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1378:
	.size	X509_VERIFY_PARAM_set1_name, .-X509_VERIFY_PARAM_set1_name
	.p2align 4
	.globl	X509_VERIFY_PARAM_set_flags
	.type	X509_VERIFY_PARAM_set_flags, @function
X509_VERIFY_PARAM_set_flags:
.LFB1379:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	orq	%rsi, %rax
	movq	%rax, %rdx
	orb	$-128, %dl
	testl	$1920, %esi
	cmovne	%rdx, %rax
	movq	%rax, 24(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1379:
	.size	X509_VERIFY_PARAM_set_flags, .-X509_VERIFY_PARAM_set_flags
	.p2align 4
	.globl	X509_VERIFY_PARAM_clear_flags
	.type	X509_VERIFY_PARAM_clear_flags, @function
X509_VERIFY_PARAM_clear_flags:
.LFB1380:
	.cfi_startproc
	endbr64
	notq	%rsi
	movl	$1, %eax
	andq	%rsi, 24(%rdi)
	ret
	.cfi_endproc
.LFE1380:
	.size	X509_VERIFY_PARAM_clear_flags, .-X509_VERIFY_PARAM_clear_flags
	.p2align 4
	.globl	X509_VERIFY_PARAM_get_flags
	.type	X509_VERIFY_PARAM_get_flags, @function
X509_VERIFY_PARAM_get_flags:
.LFB1381:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE1381:
	.size	X509_VERIFY_PARAM_get_flags, .-X509_VERIFY_PARAM_get_flags
	.p2align 4
	.globl	X509_VERIFY_PARAM_get_inh_flags
	.type	X509_VERIFY_PARAM_get_inh_flags, @function
X509_VERIFY_PARAM_get_inh_flags:
.LFB1382:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %eax
	ret
	.cfi_endproc
.LFE1382:
	.size	X509_VERIFY_PARAM_get_inh_flags, .-X509_VERIFY_PARAM_get_inh_flags
	.p2align 4
	.globl	X509_VERIFY_PARAM_set_inh_flags
	.type	X509_VERIFY_PARAM_set_inh_flags, @function
X509_VERIFY_PARAM_set_inh_flags:
.LFB1383:
	.cfi_startproc
	endbr64
	movl	%esi, 16(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1383:
	.size	X509_VERIFY_PARAM_set_inh_flags, .-X509_VERIFY_PARAM_set_inh_flags
	.p2align 4
	.globl	X509_VERIFY_PARAM_set_purpose
	.type	X509_VERIFY_PARAM_set_purpose, @function
X509_VERIFY_PARAM_set_purpose:
.LFB1384:
	.cfi_startproc
	endbr64
	addq	$32, %rdi
	jmp	X509_PURPOSE_set@PLT
	.cfi_endproc
.LFE1384:
	.size	X509_VERIFY_PARAM_set_purpose, .-X509_VERIFY_PARAM_set_purpose
	.p2align 4
	.globl	X509_VERIFY_PARAM_set_trust
	.type	X509_VERIFY_PARAM_set_trust, @function
X509_VERIFY_PARAM_set_trust:
.LFB1385:
	.cfi_startproc
	endbr64
	addq	$36, %rdi
	jmp	X509_TRUST_set@PLT
	.cfi_endproc
.LFE1385:
	.size	X509_VERIFY_PARAM_set_trust, .-X509_VERIFY_PARAM_set_trust
	.p2align 4
	.globl	X509_VERIFY_PARAM_set_depth
	.type	X509_VERIFY_PARAM_set_depth, @function
X509_VERIFY_PARAM_set_depth:
.LFB1386:
	.cfi_startproc
	endbr64
	movl	%esi, 40(%rdi)
	ret
	.cfi_endproc
.LFE1386:
	.size	X509_VERIFY_PARAM_set_depth, .-X509_VERIFY_PARAM_set_depth
	.p2align 4
	.globl	X509_VERIFY_PARAM_set_auth_level
	.type	X509_VERIFY_PARAM_set_auth_level, @function
X509_VERIFY_PARAM_set_auth_level:
.LFB1387:
	.cfi_startproc
	endbr64
	movl	%esi, 44(%rdi)
	ret
	.cfi_endproc
.LFE1387:
	.size	X509_VERIFY_PARAM_set_auth_level, .-X509_VERIFY_PARAM_set_auth_level
	.p2align 4
	.globl	X509_VERIFY_PARAM_get_time
	.type	X509_VERIFY_PARAM_get_time, @function
X509_VERIFY_PARAM_get_time:
.LFB1388:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE1388:
	.size	X509_VERIFY_PARAM_get_time, .-X509_VERIFY_PARAM_get_time
	.p2align 4
	.globl	X509_VERIFY_PARAM_set_time
	.type	X509_VERIFY_PARAM_set_time, @function
X509_VERIFY_PARAM_set_time:
.LFB1389:
	.cfi_startproc
	endbr64
	orq	$2, 24(%rdi)
	movq	%rsi, 8(%rdi)
	ret
	.cfi_endproc
.LFE1389:
	.size	X509_VERIFY_PARAM_set_time, .-X509_VERIFY_PARAM_set_time
	.p2align 4
	.globl	X509_VERIFY_PARAM_add0_policy
	.type	X509_VERIFY_PARAM_add0_policy, @function
X509_VERIFY_PARAM_add0_policy:
.LFB1390:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	48(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L36
.L32:
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
.L31:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	movq	%rsi, -24(%rbp)
	call	OPENSSL_sk_new_null@PLT
	movq	-24(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, 48(%rbx)
	movq	%rax, %rdi
	jne	.L32
	xorl	%eax, %eax
	jmp	.L31
	.cfi_endproc
.LFE1390:
	.size	X509_VERIFY_PARAM_add0_policy, .-X509_VERIFY_PARAM_add0_policy
	.p2align 4
	.globl	X509_VERIFY_PARAM_set1_policies
	.type	X509_VERIFY_PARAM_set1_policies, @function
X509_VERIFY_PARAM_set1_policies:
.LFB1391:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L54
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	48(%rdi), %rdi
	movq	ASN1_OBJECT_free@GOTPCREL(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	testq	%r13, %r13
	je	.L55
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 48(%r14)
	testq	%rax, %rax
	je	.L41
	xorl	%ebx, %ebx
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L44:
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	call	OBJ_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L41
	movq	48(%r14), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L56
	addl	$1, %ebx
.L42:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L44
	orq	$128, 24(%r14)
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	movq	$0, 48(%r14)
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	%r12, %rdi
	movl	%eax, -36(%rbp)
	call	ASN1_OBJECT_free@PLT
	movl	-36(%rbp), %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1391:
	.size	X509_VERIFY_PARAM_set1_policies, .-X509_VERIFY_PARAM_set1_policies
	.p2align 4
	.globl	X509_VERIFY_PARAM_inherit
	.type	X509_VERIFY_PARAM_inherit, @function
X509_VERIFY_PARAM_inherit:
.LFB1375:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L188
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movl	16(%rdi), %eax
	orl	16(%rsi), %eax
	testb	$16, %al
	jne	.L189
.L60:
	testb	$8, %al
	jne	.L187
	movl	%eax, %r13d
	movl	%eax, %r9d
	movl	32(%rbx), %ecx
	movl	36(%rbx), %r8d
	movl	44(%rbx), %edi
	movq	24(%r12), %rdx
	andl	$1, %r13d
	andl	$4, %r9d
	movl	40(%rbx), %esi
	testb	$2, %al
	je	.L190
	movd	%esi, %xmm1
	movd	%edi, %xmm2
	movd	%ecx, %xmm0
	movq	8(%rbx), %rax
	movd	%r8d, %xmm3
	punpckldq	%xmm2, %xmm1
	andq	$-3, %rdx
	punpckldq	%xmm3, %xmm0
	movq	%rax, 8(%r12)
	punpcklqdq	%xmm1, %xmm0
	movq	%rdx, 24(%r12)
	movups	%xmm0, 32(%r12)
	testl	%r9d, %r9d
	jne	.L70
	orq	24(%rbx), %rdx
	movq	%rdx, 24(%r12)
.L80:
	movq	48(%rbx), %rsi
	movq	%r12, %rdi
	call	X509_VERIFY_PARAM_set1_policies
	testl	%eax, %eax
	je	.L93
	movq	56(%r12), %rdi
	leaq	str_free(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	$0, 56(%r12)
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L88
	leaq	str_free(%rip), %rdx
	leaq	str_copy(%rip), %rsi
	call	OPENSSL_sk_deep_copy@PLT
	movq	%rax, 56(%r12)
	testq	%rax, %rax
	je	.L93
	movl	64(%rbx), %eax
	movl	%eax, 64(%r12)
.L88:
	movq	80(%rbx), %r14
	testq	%r14, %r14
	je	.L191
	movl	$1, -52(%rbp)
	movq	88(%rbx), %r15
.L110:
	testq	%r15, %r15
	jne	.L92
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rax, %r15
.L92:
	movq	%r14, %rdi
	movl	$247, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r15, %rsi
	call	CRYPTO_memdup@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L93
	movq	80(%r12), %rdi
	movl	$254, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	-52(%rbp), %eax
	movq	%r14, 80(%r12)
	movq	%r15, 88(%r12)
	testl	%eax, %eax
	je	.L89
.L100:
	movq	104(%rbx), %rax
	movq	96(%rbx), %r14
	movq	%rax, %rbx
	andq	$-5, %rbx
	je	.L96
	cmpq	$16, %rax
	jne	.L93
	testq	%r14, %r14
	je	.L123
.L121:
	movl	$16, %ebx
.L99:
	movq	%r14, %rdi
	movl	$247, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%rbx, %rsi
	call	CRYPTO_memdup@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L93
.L98:
	movq	96(%r12), %rdi
	movl	$254, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r14, 96(%r12)
	movq	%rbx, 104(%r12)
.L187:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore_state
	movl	$0, 16(%rdi)
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L190:
	testl	%ecx, %ecx
	je	.L192
	testl	%r13d, %r13d
	jne	.L66
	movl	32(%r12), %r10d
	testl	%r10d, %r10d
	jne	.L67
	movl	%ecx, 32(%r12)
.L67:
	testl	%r8d, %r8d
	je	.L193
.L72:
	movl	36(%r12), %ecx
	testl	%ecx, %ecx
	je	.L194
.L65:
	cmpl	$-1, %esi
	je	.L73
	testl	%r13d, %r13d
	jne	.L74
.L120:
	cmpl	$-1, 40(%r12)
	je	.L74
.L185:
	cmpl	$-1, %edi
	je	.L76
.L119:
	cmpl	$-1, 44(%r12)
	je	.L77
.L76:
	testb	$2, %dl
	jne	.L195
.L78:
	movq	8(%rbx), %rax
	andq	$-3, %rdx
	movq	%rdx, 24(%r12)
	movq	%rax, 8(%r12)
	testl	%r9d, %r9d
	je	.L181
.L117:
	movq	$0, 24(%r12)
	movq	48(%rbx), %rsi
	movq	24(%rbx), %rax
	movq	%rax, 24(%r12)
	testq	%rsi, %rsi
	je	.L81
.L196:
	testl	%r13d, %r13d
	jne	.L82
	cmpq	$0, 48(%r12)
	je	.L82
	cmpq	$0, 56(%rbx)
	je	.L114
	movq	56(%r12), %rdi
.L116:
	testq	%rdi, %rdi
	je	.L85
	movq	80(%rbx), %r14
	testq	%r14, %r14
	je	.L89
.L113:
	cmpq	$0, 80(%r12)
	je	.L90
	movq	96(%rbx), %r14
	testq	%r14, %r14
	je	.L187
.L109:
	cmpq	$0, 96(%r12)
	jne	.L187
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L70:
	movq	$0, 24(%r12)
	movq	24(%rbx), %rax
	movq	%rax, 24(%r12)
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L192:
	testl	%r8d, %r8d
	je	.L65
	testl	%r13d, %r13d
	je	.L72
.L186:
	movl	%r8d, 36(%r12)
	cmpl	$-1, %esi
	je	.L73
.L74:
	movl	%esi, 40(%r12)
.L73:
	cmpl	$-1, %edi
	je	.L76
	testl	%r13d, %r13d
	je	.L119
.L77:
	movl	%edi, 44(%r12)
	testb	$2, %dl
	je	.L78
.L195:
	testl	%r9d, %r9d
	jne	.L117
.L181:
	movq	48(%rbx), %rsi
	orq	24(%rbx), %rdx
	movq	%rdx, 24(%r12)
	testq	%rsi, %rsi
	jne	.L196
.L81:
	cmpq	$0, 56(%rbx)
	je	.L114
	movq	56(%r12), %rdi
	testl	%r13d, %r13d
	je	.L116
.L85:
	leaq	str_free(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	$0, 56(%r12)
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L114
	leaq	str_free(%rip), %rdx
	leaq	str_copy(%rip), %rsi
	call	OPENSSL_sk_deep_copy@PLT
	movq	%rax, 56(%r12)
	testq	%rax, %rax
	je	.L93
	movl	64(%rbx), %eax
	movl	%eax, 64(%r12)
.L114:
	movq	80(%rbx), %r14
	testq	%r14, %r14
	je	.L89
	testl	%r13d, %r13d
	je	.L113
.L90:
	movl	$0, -52(%rbp)
	movq	88(%rbx), %r15
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L89:
	movq	96(%rbx), %r14
	testq	%r14, %r14
	je	.L187
	testl	%r13d, %r13d
	je	.L109
.L95:
	movq	104(%rbx), %rax
	testq	$-5, %rax
	je	.L108
	cmpq	$16, %rax
	je	.L121
	.p2align 4,,10
	.p2align 3
.L93:
	xorl	%eax, %eax
.L197:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	testq	%r14, %r14
	je	.L98
.L108:
	testq	%rax, %rax
	jne	.L122
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rax, %rbx
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L82:
	movq	%r12, %rdi
	call	X509_VERIFY_PARAM_set1_policies
	testl	%eax, %eax
	jne	.L81
	xorl	%eax, %eax
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L188:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	%ecx, 32(%r12)
	testl	%r8d, %r8d
	jne	.L186
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L193:
	cmpl	$-1, %esi
	jne	.L120
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L194:
	movl	%r8d, 36(%r12)
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L123:
	xorl	%ebx, %ebx
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L191:
	movq	80(%r12), %rdi
	movl	$254, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 80(%r12)
	movq	$0, 88(%r12)
	jmp	.L100
.L122:
	movq	%rax, %rbx
	jmp	.L99
	.cfi_endproc
.LFE1375:
	.size	X509_VERIFY_PARAM_inherit, .-X509_VERIFY_PARAM_inherit
	.p2align 4
	.globl	X509_VERIFY_PARAM_set1
	.type	X509_VERIFY_PARAM_set1, @function
X509_VERIFY_PARAM_set1:
.LFB1376:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	16(%rdi), %r13d
	movl	%r13d, %eax
	orl	$1, %eax
	movl	%eax, 16(%rdi)
	testq	%rsi, %rsi
	je	.L287
	orl	16(%rsi), %eax
	movq	%rsi, %r12
	testb	$16, %al
	jne	.L288
.L201:
	testb	$8, %al
	jne	.L287
	movl	%eax, %r8d
	movl	32(%r12), %ecx
	movl	36(%r12), %r9d
	movl	40(%r12), %esi
	movl	44(%r12), %edi
	andl	$4, %r8d
	movq	24(%rbx), %rdx
	testb	$2, %al
	je	.L289
	movd	%esi, %xmm1
	movd	%edi, %xmm2
	movd	%ecx, %xmm0
	andq	$-3, %rdx
	movd	%r9d, %xmm3
	movq	8(%r12), %rax
	punpckldq	%xmm2, %xmm1
	movq	%rdx, 24(%rbx)
	punpckldq	%xmm3, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, 8(%rbx)
	movups	%xmm0, 32(%rbx)
	testl	%r8d, %r8d
	jne	.L208
	orq	24(%r12), %rdx
	movq	%rdx, 24(%rbx)
.L214:
	movq	48(%r12), %rsi
	movq	%rbx, %rdi
	call	X509_VERIFY_PARAM_set1_policies
	testl	%eax, %eax
	je	.L223
	movq	56(%rbx), %rdi
	leaq	str_free(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	$0, 56(%rbx)
	movq	56(%r12), %rdi
	testq	%rdi, %rdi
	je	.L219
	leaq	str_free(%rip), %rdx
	leaq	str_copy(%rip), %rsi
	call	OPENSSL_sk_deep_copy@PLT
	movq	%rax, 56(%rbx)
	testq	%rax, %rax
	je	.L223
	movl	64(%r12), %eax
	movl	%eax, 64(%rbx)
.L219:
	movq	80(%r12), %rdi
	testq	%rdi, %rdi
	je	.L290
	movq	88(%r12), %r14
	movl	$1, %r15d
.L238:
	testq	%r14, %r14
	jne	.L222
	movq	%rdi, -56(%rbp)
	call	strlen@PLT
	movq	-56(%rbp), %rdi
	movq	%rax, %r14
.L222:
	movl	$247, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r14, %rsi
	call	CRYPTO_memdup@PLT
	testq	%rax, %rax
	je	.L223
	movq	80(%rbx), %rdi
	movl	$254, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	CRYPTO_free@PLT
	movq	-56(%rbp), %rax
	movq	%r14, 88(%rbx)
	movq	%rax, 80(%rbx)
	testl	%r15d, %r15d
	je	.L221
.L229:
	movq	104(%r12), %r15
	testq	$-5, %r15
	je	.L248
	cmpq	$16, %r15
	jne	.L223
.L248:
	movq	96(%r12), %r14
	testq	%r14, %r14
	je	.L247
.L237:
	testq	%r15, %r15
	jne	.L228
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rax, %r15
.L228:
	movq	%r14, %rdi
	movl	$247, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r15, %rsi
	call	CRYPTO_memdup@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L223
.L227:
	movq	96(%rbx), %rdi
	movl	$254, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r14, 96(%rbx)
	movq	%r15, 104(%rbx)
	.p2align 4,,10
	.p2align 3
.L287:
	movl	$1, %eax
.L200:
	movl	%r13d, 16(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L288:
	.cfi_restore_state
	movl	$0, 16(%rdi)
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L289:
	testl	%ecx, %ecx
	je	.L286
	movl	%ecx, 32(%rbx)
.L286:
	testl	%r9d, %r9d
	je	.L206
	movl	%r9d, 36(%rbx)
.L206:
	cmpl	$-1, %esi
	je	.L246
	movl	%esi, 40(%rbx)
.L246:
	cmpl	$-1, %edi
	je	.L245
	movl	%edi, 44(%rbx)
.L245:
	testb	$2, %dl
	jne	.L291
	movq	8(%r12), %rax
	andq	$-3, %rdx
	movq	%rdx, 24(%rbx)
	movq	%rax, 8(%rbx)
	testl	%r8d, %r8d
	je	.L283
.L243:
	movq	$0, 24(%rbx)
	movq	24(%r12), %rax
	movq	%rax, 24(%rbx)
.L234:
	movq	48(%r12), %rsi
	testq	%rsi, %rsi
	je	.L233
	movq	%rbx, %rdi
	call	X509_VERIFY_PARAM_set1_policies
	testl	%eax, %eax
	je	.L223
.L233:
	cmpq	$0, 56(%r12)
	je	.L241
	movq	56(%rbx), %rdi
	leaq	str_free(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	$0, 56(%rbx)
	movq	56(%r12), %rdi
	testq	%rdi, %rdi
	je	.L241
	leaq	str_free(%rip), %rdx
	leaq	str_copy(%rip), %rsi
	call	OPENSSL_sk_deep_copy@PLT
	movq	%rax, 56(%rbx)
	testq	%rax, %rax
	je	.L223
	movl	64(%r12), %eax
	movl	%eax, 64(%rbx)
.L241:
	movq	80(%r12), %rdi
	testq	%rdi, %rdi
	je	.L221
	movq	88(%r12), %r14
	xorl	%r15d, %r15d
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L208:
	movq	$0, 24(%rbx)
	movq	24(%r12), %rax
	movq	%rax, 24(%rbx)
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L291:
	testl	%r8d, %r8d
	jne	.L243
.L283:
	orq	24(%r12), %rdx
	movq	%rdx, 24(%rbx)
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L221:
	movq	96(%r12), %r14
	testq	%r14, %r14
	je	.L287
	movq	104(%r12), %r15
	testq	$-5, %r15
	je	.L237
	cmpq	$16, %r15
	je	.L237
	.p2align 4,,10
	.p2align 3
.L223:
	xorl	%eax, %eax
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L290:
	movq	80(%rbx), %rdi
	movl	$254, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 80(%rbx)
	movq	$0, 88(%rbx)
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L247:
	xorl	%r15d, %r15d
	jmp	.L227
	.cfi_endproc
.LFE1376:
	.size	X509_VERIFY_PARAM_set1, .-X509_VERIFY_PARAM_set1
	.p2align 4
	.globl	X509_VERIFY_PARAM_set1_host
	.type	X509_VERIFY_PARAM_set1_host, @function
X509_VERIFY_PARAM_set1_host:
.LFB1392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	testq	%rsi, %rsi
	sete	%r15b
	testq	%rdx, %rdx
	je	.L310
	testb	%r15b, %r15b
	jne	.L310
	cmpq	$1, %rdx
	leaq	-1(%rdx), %r14
	movq	%rdx, %rbx
	movl	$1, %edx
	cmova	%r14, %rdx
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	memchr@PLT
	testq	%rax, %rax
	je	.L298
.L322:
	xorl	%eax, %eax
.L292:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L310:
	.cfi_restore_state
	movq	56(%r13), %r8
	testq	%r12, %r12
	je	.L295
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	strlen@PLT
	movq	-56(%rbp), %r8
	testq	%rax, %rax
	jne	.L324
.L295:
	leaq	str_free(%rip), %rsi
	movq	%r8, %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	$0, 56(%r13)
.L323:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L298:
	.cfi_restore_state
	cmpb	$0, -1(%r12,%rbx)
	movq	56(%r13), %r8
	jne	.L325
.L307:
	leaq	str_free(%rip), %rsi
	movq	%r8, %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	$0, 56(%r13)
	testq	%r14, %r14
	je	.L323
	testb	%r15b, %r15b
	jne	.L323
.L306:
	movq	%r12, %rdi
	movl	$59, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r14, %rsi
	call	CRYPTO_strndup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L322
	movq	56(%r13), %rdi
	testq	%rdi, %rdi
	je	.L326
.L304:
	movq	%r12, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L323
	movq	%r12, %rdi
	movl	$70, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	56(%r13), %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jne	.L322
	movq	56(%r13), %rdi
	movl	%eax, -56(%rbp)
	call	OPENSSL_sk_free@PLT
	movq	$0, 56(%r13)
	movl	-56(%rbp), %eax
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L325:
	leaq	str_free(%rip), %rsi
	movq	%r8, %rdi
	movq	%rbx, %r14
	call	OPENSSL_sk_pop_free@PLT
	movq	$0, 56(%r13)
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L326:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 56(%r13)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L304
	movl	$65, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L324:
	cmpb	$0, -1(%r12,%rax)
	leaq	-1(%rax), %r14
	je	.L307
	leaq	str_free(%rip), %rsi
	movq	%r8, %rdi
	movq	%rax, -56(%rbp)
	call	OPENSSL_sk_pop_free@PLT
	movq	-56(%rbp), %rax
	movq	$0, 56(%r13)
	movq	%rax, %r14
	jmp	.L306
	.cfi_endproc
.LFE1392:
	.size	X509_VERIFY_PARAM_set1_host, .-X509_VERIFY_PARAM_set1_host
	.p2align 4
	.globl	X509_VERIFY_PARAM_add1_host
	.type	X509_VERIFY_PARAM_add1_host, @function
X509_VERIFY_PARAM_add1_host:
.LFB1393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	testq	%rsi, %rsi
	sete	%r15b
	testq	%rdx, %rdx
	je	.L343
	testb	%r15b, %r15b
	jne	.L343
	cmpq	$1, %rdx
	leaq	-1(%rdx), %r14
	movq	%rdx, %rbx
	movl	$1, %edx
	cmova	%r14, %rdx
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	memchr@PLT
	testq	%rax, %rax
	je	.L335
.L348:
	xorl	%eax, %eax
.L327:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L343:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L333
	movq	%r12, %rdi
	call	strlen@PLT
	testq	%rax, %rax
	jne	.L349
.L333:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L335:
	.cfi_restore_state
	cmpb	$0, -1(%r12,%rbx)
	movq	%r14, %rsi
	jne	.L350
.L339:
	testq	%rsi, %rsi
	je	.L333
	testb	%r15b, %r15b
	jne	.L333
.L340:
	movq	%r12, %rdi
	movl	$59, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_strndup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L348
	movq	56(%r13), %rdi
	testq	%rdi, %rdi
	je	.L351
.L338:
	movq	%r12, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L333
	movq	%r12, %rdi
	movl	$70, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	56(%r13), %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jne	.L348
	movq	56(%r13), %rdi
	movl	%eax, -52(%rbp)
	call	OPENSSL_sk_free@PLT
	movq	$0, 56(%r13)
	movl	-52(%rbp), %eax
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L350:
	movq	%rbx, %rsi
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L351:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 56(%r13)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L338
	movl	$65, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L349:
	cmpb	$0, -1(%r12,%rax)
	leaq	-1(%rax), %rsi
	je	.L339
	movq	%rax, %rsi
	jmp	.L340
	.cfi_endproc
.LFE1393:
	.size	X509_VERIFY_PARAM_add1_host, .-X509_VERIFY_PARAM_add1_host
	.p2align 4
	.globl	X509_VERIFY_PARAM_set_hostflags
	.type	X509_VERIFY_PARAM_set_hostflags, @function
X509_VERIFY_PARAM_set_hostflags:
.LFB1394:
	.cfi_startproc
	endbr64
	movl	%esi, 64(%rdi)
	ret
	.cfi_endproc
.LFE1394:
	.size	X509_VERIFY_PARAM_set_hostflags, .-X509_VERIFY_PARAM_set_hostflags
	.p2align 4
	.globl	X509_VERIFY_PARAM_get_hostflags
	.type	X509_VERIFY_PARAM_get_hostflags, @function
X509_VERIFY_PARAM_get_hostflags:
.LFB1395:
	.cfi_startproc
	endbr64
	movl	64(%rdi), %eax
	ret
	.cfi_endproc
.LFE1395:
	.size	X509_VERIFY_PARAM_get_hostflags, .-X509_VERIFY_PARAM_get_hostflags
	.p2align 4
	.globl	X509_VERIFY_PARAM_get0_peername
	.type	X509_VERIFY_PARAM_get0_peername, @function
X509_VERIFY_PARAM_get0_peername:
.LFB1396:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rax
	ret
	.cfi_endproc
.LFE1396:
	.size	X509_VERIFY_PARAM_get0_peername, .-X509_VERIFY_PARAM_get0_peername
	.p2align 4
	.globl	X509_VERIFY_PARAM_move_peername
	.type	X509_VERIFY_PARAM_move_peername, @function
X509_VERIFY_PARAM_move_peername:
.LFB1397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	72(%rdi), %rdi
	testq	%rsi, %rsi
	je	.L356
	movq	72(%rsi), %r13
	movq	%rsi, %rbx
	cmpq	%rdi, %r13
	je	.L360
	movl	$417, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r13, 72(%r12)
.L360:
	movq	$0, 72(%rbx)
.L355:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L356:
	.cfi_restore_state
	testq	%rdi, %rdi
	je	.L355
	movl	$417, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 72(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1397:
	.size	X509_VERIFY_PARAM_move_peername, .-X509_VERIFY_PARAM_move_peername
	.p2align 4
	.globl	X509_VERIFY_PARAM_set1_email
	.type	X509_VERIFY_PARAM_set1_email, @function
X509_VERIFY_PARAM_set1_email:
.LFB1398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	testq	%rsi, %rsi
	je	.L366
	movq	%rdx, %rbx
	testq	%rdx, %rdx
	je	.L368
.L364:
	movq	%r12, %rdi
	movl	$247, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%rbx, %rsi
	call	CRYPTO_memdup@PLT
	movq	%rax, %r12
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L362
.L363:
	movq	80(%r13), %rdi
	movl	$254, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, 80(%r13)
	movl	$1, %eax
	movq	%rbx, 88(%r13)
.L362:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L368:
	.cfi_restore_state
	movq	%rsi, %rdi
	call	strlen@PLT
	movq	%rax, %rbx
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L366:
	xorl	%ebx, %ebx
	jmp	.L363
	.cfi_endproc
.LFE1398:
	.size	X509_VERIFY_PARAM_set1_email, .-X509_VERIFY_PARAM_set1_email
	.p2align 4
	.globl	X509_VERIFY_PARAM_set1_ip
	.type	X509_VERIFY_PARAM_set1_ip, @function
X509_VERIFY_PARAM_set1_ip:
.LFB1399:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	testq	$-5, %rdx
	je	.L370
	cmpq	$16, %rdx
	jne	.L375
	testq	%rsi, %rsi
	je	.L376
.L374:
	movq	%r12, %rdi
	movl	$247, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%rbx, %rsi
	call	CRYPTO_memdup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L375
.L373:
	movq	96(%r13), %rdi
	movl	$254, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, 96(%r13)
	movl	$1, %eax
	movq	%rbx, 104(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L375:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L370:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L376
	testq	%rdx, %rdx
	jne	.L374
	movq	%rsi, %rdi
	call	strlen@PLT
	movq	%rax, %rbx
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L376:
	xorl	%ebx, %ebx
	jmp	.L373
	.cfi_endproc
.LFE1399:
	.size	X509_VERIFY_PARAM_set1_ip, .-X509_VERIFY_PARAM_set1_ip
	.p2align 4
	.globl	X509_VERIFY_PARAM_set1_ip_asc
	.type	X509_VERIFY_PARAM_set1_ip_asc, @function
X509_VERIFY_PARAM_set1_ip_asc:
.LFB1400:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	pushq	%rbx
	movq	%r13, %rdi
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	a2i_ipadd@PLT
	movslq	%eax, %rbx
	testq	%rbx, %rbx
	je	.L384
	cmpq	$4, %rbx
	je	.L386
	cmpq	$16, %rbx
	jne	.L384
.L386:
	movq	%r13, %rdi
	movl	$247, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%rbx, %rsi
	call	CRYPTO_memdup@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L384
	movq	96(%r12), %rdi
	movl	$254, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r13, 96(%r12)
	movl	$1, %eax
	movq	%rbx, 104(%r12)
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L384:
	xorl	%eax, %eax
.L381:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L397
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L397:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1400:
	.size	X509_VERIFY_PARAM_set1_ip_asc, .-X509_VERIFY_PARAM_set1_ip_asc
	.p2align 4
	.globl	X509_VERIFY_PARAM_get_depth
	.type	X509_VERIFY_PARAM_get_depth, @function
X509_VERIFY_PARAM_get_depth:
.LFB1401:
	.cfi_startproc
	endbr64
	movl	40(%rdi), %eax
	ret
	.cfi_endproc
.LFE1401:
	.size	X509_VERIFY_PARAM_get_depth, .-X509_VERIFY_PARAM_get_depth
	.p2align 4
	.globl	X509_VERIFY_PARAM_get_auth_level
	.type	X509_VERIFY_PARAM_get_auth_level, @function
X509_VERIFY_PARAM_get_auth_level:
.LFB1402:
	.cfi_startproc
	endbr64
	movl	44(%rdi), %eax
	ret
	.cfi_endproc
.LFE1402:
	.size	X509_VERIFY_PARAM_get_auth_level, .-X509_VERIFY_PARAM_get_auth_level
	.p2align 4
	.globl	X509_VERIFY_PARAM_get0_name
	.type	X509_VERIFY_PARAM_get0_name, @function
X509_VERIFY_PARAM_get0_name:
.LFB1403:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE1403:
	.size	X509_VERIFY_PARAM_get0_name, .-X509_VERIFY_PARAM_get0_name
	.p2align 4
	.globl	X509_VERIFY_PARAM_add0_table
	.type	X509_VERIFY_PARAM_add0_table, @function
X509_VERIFY_PARAM_add0_table:
.LFB1408:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	param_table(%rip), %rdi
	testq	%rdi, %rdi
	je	.L410
	movq	%r12, %rsi
	call	OPENSSL_sk_find@PLT
	movq	param_table(%rip), %rdi
	movl	%eax, %esi
	testl	%eax, %eax
	jns	.L405
.L404:
	movq	%r12, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
.L401:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L405:
	.cfi_restore_state
	call	OPENSSL_sk_delete@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L409
	movq	48(%rax), %rdi
	movq	ASN1_OBJECT_free@GOTPCREL(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	56(%r13), %rdi
	leaq	str_free(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	72(%r13), %rdi
	movl	$104, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	80(%r13), %rdi
	movl	$105, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	96(%r13), %rdi
	movl	$106, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$107, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
.L409:
	movq	param_table(%rip), %rdi
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L410:
	leaq	param_cmp(%rip), %rdi
	call	OPENSSL_sk_new@PLT
	movq	%rax, param_table(%rip)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L404
	xorl	%eax, %eax
	jmp	.L401
	.cfi_endproc
.LFE1408:
	.size	X509_VERIFY_PARAM_add0_table, .-X509_VERIFY_PARAM_add0_table
	.p2align 4
	.globl	X509_VERIFY_PARAM_get_count
	.type	X509_VERIFY_PARAM_get_count, @function
X509_VERIFY_PARAM_get_count:
.LFB1409:
	.cfi_startproc
	endbr64
	movq	param_table(%rip), %rdi
	testq	%rdi, %rdi
	je	.L415
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	OPENSSL_sk_num@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	addl	$5, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L415:
	.cfi_restore 6
	movl	$5, %eax
	ret
	.cfi_endproc
.LFE1409:
	.size	X509_VERIFY_PARAM_get_count, .-X509_VERIFY_PARAM_get_count
	.p2align 4
	.globl	X509_VERIFY_PARAM_get0
	.type	X509_VERIFY_PARAM_get0, @function
X509_VERIFY_PARAM_get0:
.LFB1410:
	.cfi_startproc
	endbr64
	cmpl	$4, %edi
	jg	.L421
	movslq	%edi, %rdi
	leaq	default_table(%rip), %rdx
	leaq	0(,%rdi,8), %rax
	subq	%rdi, %rax
	salq	$4, %rax
	addq	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L421:
	leal	-5(%rdi), %esi
	movq	param_table(%rip), %rdi
	jmp	OPENSSL_sk_value@PLT
	.cfi_endproc
.LFE1410:
	.size	X509_VERIFY_PARAM_get0, .-X509_VERIFY_PARAM_get0
	.p2align 4
	.globl	X509_VERIFY_PARAM_lookup
	.type	X509_VERIFY_PARAM_lookup, @function
X509_VERIFY_PARAM_lookup:
.LFB1411:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-144(%rbp), %r12
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -144(%rbp)
	movq	param_table(%rip), %rdi
	testq	%rdi, %rdi
	je	.L425
	movq	%r12, %rsi
	call	OPENSSL_sk_find@PLT
	testl	%eax, %eax
	jns	.L432
.L425:
	leaq	table_cmp_BSEARCH_CMP_FN(%rip), %r8
	movl	$112, %ecx
	movl	$5, %edx
	movq	%r12, %rdi
	leaq	default_table(%rip), %rsi
	call	OBJ_bsearch_@PLT
.L424:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L433
	addq	$136, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L432:
	.cfi_restore_state
	movq	param_table(%rip), %rdi
	movl	%eax, %esi
	call	OPENSSL_sk_value@PLT
	jmp	.L424
.L433:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1411:
	.size	X509_VERIFY_PARAM_lookup, .-X509_VERIFY_PARAM_lookup
	.p2align 4
	.globl	X509_VERIFY_PARAM_table_cleanup
	.type	X509_VERIFY_PARAM_table_cleanup, @function
X509_VERIFY_PARAM_table_cleanup:
.LFB1412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	param_table(%rip), %rdi
	leaq	X509_VERIFY_PARAM_free(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	OPENSSL_sk_pop_free@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	$0, param_table(%rip)
	ret
	.cfi_endproc
.LFE1412:
	.size	X509_VERIFY_PARAM_table_cleanup, .-X509_VERIFY_PARAM_table_cleanup
	.local	param_table
	.comm	param_table,8,8
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"default"
.LC2:
	.string	"pkcs7"
.LC3:
	.string	"smime_sign"
.LC4:
	.string	"ssl_client"
.LC5:
	.string	"ssl_server"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	default_table, @object
	.size	default_table, 560
default_table:
	.quad	.LC1
	.quad	0
	.long	0
	.zero	4
	.quad	32768
	.long	0
	.long	0
	.long	100
	.long	-1
	.quad	0
	.quad	0
	.long	0
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC2
	.quad	0
	.long	0
	.zero	4
	.quad	0
	.long	4
	.long	4
	.long	-1
	.long	-1
	.quad	0
	.quad	0
	.long	0
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC3
	.quad	0
	.long	0
	.zero	4
	.quad	0
	.long	4
	.long	4
	.long	-1
	.long	-1
	.quad	0
	.quad	0
	.long	0
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC4
	.quad	0
	.long	0
	.zero	4
	.quad	0
	.long	1
	.long	2
	.long	-1
	.long	-1
	.quad	0
	.quad	0
	.long	0
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC5
	.quad	0
	.long	0
	.zero	4
	.quad	0
	.long	2
	.long	3
	.long	-1
	.long	-1
	.quad	0
	.quad	0
	.long	0
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
