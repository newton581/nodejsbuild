	.file	"v3_cpols.c"
	.text
	.p2align 4
	.globl	POLICYINFO_free
	.type	POLICYINFO_free, @function
POLICYINFO_free:
.LFB1351:
	.cfi_startproc
	endbr64
	leaq	POLICYINFO_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1351:
	.size	POLICYINFO_free, .-POLICYINFO_free
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"s"
.LC1:
	.string	""
.LC2:
	.string	"%*sCPS: %s\n"
.LC3:
	.string	"%*sUser Notice:\n"
.LC4:
	.string	"%*sOrganization: %s\n"
.LC5:
	.string	"%*sNumber%s: "
.LC6:
	.string	", "
.LC7:
	.string	"(null)"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"../deps/openssl/openssl/crypto/x509v3/v3_cpols.c"
	.section	.rodata.str1.1
.LC9:
	.string	"\n"
.LC10:
	.string	"%*sExplicit Text: %s\n"
.LC11:
	.string	"%*sUnknown Qualifier: "
	.text
	.p2align 4
	.type	print_qualifiers, @function
print_qualifiers:
.LFB1371:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %eax
	addl	$2, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movl	%edx, -52(%rbp)
	movl	%eax, -56(%rbp)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L33:
	movl	-56(%rbp), %edx
	leaq	.LC1(%rip), %rcx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC11(%rip), %rsi
	call	BIO_printf@PLT
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	i2a_ASN1_OBJECT@PLT
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
.L8:
	addl	$1, %r13d
.L4:
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jge	.L32
	movq	%r15, %rdi
	movl	%r13d, %esi
	call	OPENSSL_sk_value@PLT
	movq	(%rax), %rdi
	movq	%rax, %rbx
	call	OBJ_obj2nid@PLT
	cmpl	$164, %eax
	je	.L5
	cmpl	$165, %eax
	jne	.L33
	movl	-52(%rbp), %edx
	leaq	.LC1(%rip), %rcx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC3(%rip), %rsi
	call	BIO_printf@PLT
	movq	8(%rbx), %rax
	movq	(%rax), %r14
	movq	%rax, -64(%rbp)
	testq	%r14, %r14
	je	.L9
	movq	(%r14), %rax
	movl	-56(%rbp), %ebx
	leaq	.LC1(%rip), %rcx
	movq	%r12, %rdi
	leaq	.LC4(%rip), %rsi
	movq	8(%rax), %r8
	movl	%ebx, %edx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	8(%r14), %rdi
	call	OPENSSL_sk_num@PLT
	leaq	.LC1(%rip), %rcx
	movl	%ebx, %edx
	leaq	.LC0(%rip), %r8
	cmpl	$1, %eax
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	cmovle	%rcx, %r8
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	call	BIO_printf@PLT
	movl	%r13d, -68(%rbp)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L12:
	testq	%r13, %r13
	je	.L34
.L13:
	movq	%r13, %rsi
	xorl	%edi, %edi
	call	i2s_ASN1_INTEGER@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L30
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	movl	$467, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
.L14:
	addl	$1, %ebx
.L11:
	movq	8(%r14), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L35
	movq	8(%r14), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r13
	testl	%ebx, %ebx
	je	.L12
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testq	%r13, %r13
	jne	.L13
.L34:
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L5:
	movq	8(%rbx), %rax
	movl	-52(%rbp), %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rcx
	leaq	.LC2(%rip), %rsi
	movq	8(%rax), %r8
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L35:
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	movl	-68(%rbp), %r13d
	call	BIO_puts@PLT
.L9:
	movq	-64(%rbp), %rax
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L8
	movq	8(%rax), %r8
	movl	-56(%rbp), %edx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rcx
	leaq	.LC10(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L32:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movl	-68(%rbp), %r13d
	jmp	.L8
	.cfi_endproc
.LFE1371:
	.size	print_qualifiers, .-print_qualifiers
	.section	.rodata.str1.1
.LC12:
	.string	"%*sPolicy: "
	.text
	.p2align 4
	.type	i2r_certpol, @function
i2r_certpol:
.LFB1370:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	2(%rcx), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	%eax, -52(%rbp)
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L39:
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	leaq	.LC1(%rip), %rcx
	movl	%r15d, %edx
	movq	%r13, %rdi
	movq	%rax, %rbx
	leaq	.LC12(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	(%rbx), %rsi
	movq	%r13, %rdi
	call	i2a_ASN1_OBJECT@PLT
	leaq	.LC9(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_puts@PLT
	movq	8(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L38
	movl	-52(%rbp), %edx
	movq	%r13, %rdi
	call	print_qualifiers
.L38:
	addl	$1, %r12d
.L37:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jl	.L39
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1370:
	.size	i2r_certpol, .-i2r_certpol
	.section	.rodata.str1.1
.LC13:
	.string	",value:"
.LC14:
	.string	",name:"
.LC15:
	.string	"section:"
.LC16:
	.string	"ia5org"
.LC17:
	.string	"policyIdentifier"
.LC18:
	.string	"CPS"
.LC19:
	.string	"userNotice"
.LC20:
	.string	"explicitText"
.LC21:
	.string	"UTF8"
.LC22:
	.string	"UTF8String"
.LC23:
	.string	"BMPSTRING"
.LC24:
	.string	"VISIBLE"
.LC25:
	.string	"VISIBLESTRING"
.LC26:
	.string	"organization"
.LC27:
	.string	"noticeNumbers"
	.text
	.p2align 4
	.type	r2i_certpol, @function
r2i_certpol:
.LFB1364:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -88(%rbp)
	call	X509V3_parse_list@PLT
	movq	%rax, %rdi
	movq	%rax, -128(%rbp)
	movq	%rax, %rbx
	call	OPENSSL_sk_num@PLT
	movl	%eax, -104(%rbp)
	testq	%rbx, %rbx
	je	.L196
	movl	%eax, %esi
	xorl	%edi, %edi
	movl	%eax, %ebx
	call	OPENSSL_sk_new_reserve@PLT
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L47
	testl	%ebx, %ebx
	jle	.L197
	movq	X509V3_conf_free@GOTPCREL(%rip), %rax
	movl	$0, -100(%rbp)
	movl	$0, -60(%rbp)
	movq	%rax, -96(%rbp)
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L202:
	cmpb	$64, (%r8)
	je	.L198
	xorl	%esi, %esi
	movq	%r8, %rdi
	call	OBJ_txt2obj@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L199
	leaq	POLICYINFO_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L200
	movq	%r12, (%rax)
.L103:
	movq	-136(%rbp), %rdi
	movq	%r14, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L201
	addl	$1, -60(%rbp)
	movl	-60(%rbp), %eax
	cmpl	-104(%rbp), %eax
	je	.L106
.L48:
	movl	-60(%rbp), %esi
	movq	-128(%rbp), %rdi
	call	OPENSSL_sk_value@PLT
	cmpq	$0, 16(%rax)
	movq	%rax, %rbx
	jne	.L50
	movq	8(%rax), %r8
	testq	%r8, %r8
	je	.L50
	movl	$7, %ecx
	movq	%r8, %rsi
	leaq	.LC16(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L202
	addl	$1, -60(%rbp)
	movl	-60(%rbp), %eax
	movl	$1, -100(%rbp)
	cmpl	-104(%rbp), %eax
	jne	.L48
.L106:
	movq	-96(%rbp), %rsi
	movq	-128(%rbp), %rdi
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L198:
	movq	-88(%rbp), %rdi
	leaq	1(%r8), %rsi
	call	X509V3_get_section@PLT
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	je	.L203
	leaq	POLICYINFO_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L55
	movl	$0, -64(%rbp)
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L206:
	movq	16(%rbx), %rdi
	xorl	%esi, %esi
	call	OBJ_txt2obj@PLT
	testq	%rax, %rax
	je	.L204
	movq	%rax, (%r14)
.L60:
	addl	$1, -64(%rbp)
.L56:
	movq	-72(%rbp), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, -64(%rbp)
	jge	.L205
	movl	-64(%rbp), %esi
	movq	-72(%rbp), %rdi
	call	OPENSSL_sk_value@PLT
	movl	$17, %ecx
	leaq	.LC17(%rip), %rdi
	movq	8(%rax), %r8
	movq	%rax, %rbx
	movq	%r8, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L206
	leaq	.LC18(%rip), %rsi
	movq	%r8, %rdi
	call	name_cmp@PLT
	testl	%eax, %eax
	jne	.L61
	cmpq	$0, 8(%r14)
	je	.L207
.L62:
	leaq	POLICYQUALINFO_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L55
	movq	8(%r14), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L55
	movl	$164, %edi
	call	OBJ_nid2obj@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L208
	call	ASN1_IA5STRING_new@PLT
	movq	%rax, 8(%r12)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L55
	movq	16(%rbx), %r12
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	ASN1_STRING_set@PLT
	testl	%eax, %eax
	jne	.L60
	.p2align 4,,10
	.p2align 3
.L55:
	movl	$243, %r8d
	movl	$65, %edx
	movl	$131, %esi
	movl	$34, %edi
	leaq	.LC8(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L59:
	leaq	POLICYINFO_it(%rip), %rsi
	movq	%r14, %rdi
	call	ASN1_item_free@PLT
	movq	-72(%rbp), %rsi
	movq	-88(%rbp), %rdi
	call	X509V3_section_free@PLT
.L49:
	movq	-96(%rbp), %rsi
	movq	-128(%rbp), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	-136(%rbp), %rdi
	leaq	POLICYINFO_free(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	$0, -136(%rbp)
.L44:
	movq	-136(%rbp), %rax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	leaq	.LC19(%rip), %rsi
	call	name_cmp@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	jne	.L65
	movq	16(%rbx), %rsi
	cmpb	$64, (%rsi)
	jne	.L209
	movq	-88(%rbp), %rdi
	addq	$1, %rsi
	call	X509V3_get_section@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L210
	leaq	POLICYQUALINFO_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, -112(%rbp)
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L183
	movl	$165, %edi
	call	OBJ_nid2obj@PLT
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L211
	leaq	USERNOTICE_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L183
	movq	-112(%rbp), %rcx
	movq	%r14, -120(%rbp)
	movq	%rax, 8(%rcx)
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L215:
	movq	-56(%rbp), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L212
.L86:
	cmpl	$1, -100(%rbp)
	movq	(%rax), %r14
	sbbl	%eax, %eax
	andl	$4, %eax
	addl	$22, %eax
	movl	%eax, 4(%r14)
.L193:
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	ASN1_STRING_set@PLT
	testl	%eax, %eax
	je	.L181
.L97:
	addl	$1, %r15d
.L71:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r15d
	jge	.L213
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	movl	$13, %ecx
	leaq	.LC20(%rip), %rdi
	movq	%rax, -80(%rbp)
	movq	16(%rax), %r13
	movq	8(%rax), %rax
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L214
	movl	$13, %ecx
	movq	%rax, %rsi
	leaq	.LC26(%rip), %rdi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L215
	movl	$14, %ecx
	movq	%rax, %rsi
	leaq	.LC27(%rip), %rdi
	repz cmpsb
	seta	%bl
	sbbb	$0, %bl
	movsbl	%bl, %ebx
	testl	%ebx, %ebx
	jne	.L89
	movq	-56(%rbp), %rax
	movq	(%rax), %r14
	testq	%r14, %r14
	je	.L216
	movq	%r13, %rdi
	call	X509V3_parse_list@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L91
.L222:
	movq	%rax, %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	je	.L91
	movl	%r15d, -80(%rbp)
	movq	8(%r14), %r14
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L96:
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	xorl	%edi, %edi
	movq	8(%rax), %rsi
	call	s2i_ASN1_INTEGER@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L217
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L218
	addl	$1, %ebx
.L93:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L96
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	movl	-80(%rbp), %r15d
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L214:
	movl	$58, %esi
	movq	%r13, %rdi
	call	strchr@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L75
	subq	%r13, %rbx
	cmpl	$-1, %ebx
	je	.L75
	movl	$4, %ecx
	leaq	.LC21(%rip), %rdi
	cmpl	$4, %ebx
	je	.L189
	cmpl	$10, %ebx
	je	.L219
	cmpl	$3, %ebx
	je	.L220
	cmpl	$9, %ebx
	je	.L221
	cmpl	$7, %ebx
	jne	.L83
	movl	$7, %ecx
	leaq	.LC24(%rip), %rdi
	movq	%r13, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L116
	.p2align 4,,10
	.p2align 3
.L75:
	movl	$26, %edi
	call	ASN1_STRING_type_new@PLT
	movq	%rax, %r14
	movq	-56(%rbp), %rax
	movq	%r14, 8(%rax)
	testq	%r14, %r14
	jne	.L193
	.p2align 4,,10
	.p2align 3
.L181:
	movq	-120(%rbp), %r14
.L183:
.L68:
	endbr64
	movl	$366, %r8d
	leaq	.LC8(%rip), %rcx
	movl	$65, %edx
	movq	%r12, %r13
	movl	$132, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L70:
	movq	-112(%rbp), %rdi
	leaq	POLICYQUALINFO_it(%rip), %rsi
	call	ASN1_item_free@PLT
	movq	-88(%rbp), %rdi
	movq	%r13, %rsi
	call	X509V3_section_free@PLT
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L219:
	movl	$10, %ecx
	leaq	.LC22(%rip), %rdi
.L189:
	movq	%r13, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L75
	movl	$12, %edi
.L77:
	call	ASN1_STRING_type_new@PLT
	movq	%rax, %r14
	movq	-56(%rbp), %rax
	movq	%r14, 8(%rax)
	testq	%r14, %r14
	je	.L181
	testl	%ebx, %ebx
	je	.L193
	leal	1(%rbx), %eax
	addq	%rax, %r13
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L217:
	movq	%r13, %rax
	movq	-120(%rbp), %r14
	movq	%r12, %r13
	movl	$383, %r8d
	movq	%rax, %r12
	leaq	.LC8(%rip), %rcx
	movl	$140, %edx
.L184:
	movl	$133, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	movq	-96(%rbp), %rsi
	movq	%r12, %rdi
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L218:
	movq	%r13, %rax
	movq	%r15, %rdi
	movq	-120(%rbp), %r14
	movq	%r12, %r13
	movq	%rax, %r12
	call	ASN1_INTEGER_free@PLT
	movl	$393, %r8d
	movl	$65, %edx
	leaq	.LC8(%rip), %rcx
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L216:
	leaq	NOTICEREF_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L181
	movq	-56(%rbp), %rax
	movq	%r14, (%rax)
	movq	-80(%rbp), %rax
	movq	16(%rax), %r13
	movq	%r13, %rdi
	call	X509V3_parse_list@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L222
	.p2align 4,,10
	.p2align 3
.L91:
	movq	%r13, %rax
	movl	$340, %r8d
	movl	$141, %edx
	movl	$132, %esi
	leaq	.LC8(%rip), %rcx
	movl	$34, %edi
	movq	-120(%rbp), %r14
	movq	%r12, %r13
	movq	%rax, %r12
	call	ERR_put_error@PLT
	movq	-80(%rbp), %rax
	subq	$8, %rsp
	leaq	.LC14(%rip), %rcx
	leaq	.LC13(%rip), %r9
	leaq	.LC15(%rip), %rsi
	movl	$6, %edi
	movq	8(%rax), %r8
	pushq	16(%rax)
	movq	(%rax), %rdx
	xorl	%eax, %eax
	call	ERR_add_error_data@PLT
	movq	-96(%rbp), %rsi
	movq	%r12, %rdi
	call	OPENSSL_sk_pop_free@PLT
	popq	%rcx
	popq	%rsi
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L220:
	cmpb	$66, 0(%r13)
	jne	.L75
	cmpb	$77, 1(%r13)
	jne	.L75
	cmpb	$80, 2(%r13)
	movl	$30, %edi
	je	.L77
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L221:
	movl	$9, %ecx
	leaq	.LC23(%rip), %rdi
	movq	%r13, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L75
	movl	$30, %edi
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L212:
	leaq	NOTICEREF_it(%rip), %rdi
	call	ASN1_item_new@PLT
	testq	%rax, %rax
	je	.L181
	movq	-56(%rbp), %rdx
	movq	-80(%rbp), %rcx
	movq	%rax, (%rdx)
	movq	16(%rcx), %r13
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L83:
	cmpl	$13, %ebx
	jne	.L75
	movl	$13, %ecx
	movq	%r13, %rsi
	leaq	.LC25(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L75
.L116:
	movl	$26, %edi
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L207:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 8(%r14)
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L213:
	movq	-56(%rbp), %rax
	movq	-120(%rbp), %r14
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L99
	cmpq	$0, 8(%rax)
	je	.L100
	cmpq	$0, (%rax)
	je	.L100
.L99:
	movq	-88(%rbp), %rdi
	movq	%r12, %rsi
	call	X509V3_section_free@PLT
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.L223
.L108:
	movq	-112(%rbp), %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L60
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L223:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 8(%r14)
	movq	%rax, %rdi
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L205:
	cmpq	$0, (%r14)
	je	.L224
	movq	-72(%rbp), %rsi
	movq	-88(%rbp), %rdi
	call	X509V3_section_free@PLT
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L50:
	movl	$116, %r8d
	leaq	.LC8(%rip), %rcx
	movl	$134, %edx
.L187:
	movl	$130, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	subq	$8, %rsp
	movq	8(%rbx), %r8
	pushq	16(%rbx)
	movq	(%rbx), %rdx
	leaq	.LC13(%rip), %r9
	xorl	%eax, %eax
	leaq	.LC14(%rip), %rcx
	leaq	.LC15(%rip), %rsi
	movl	$6, %edi
	call	ERR_add_error_data@PLT
	popq	%r9
	popq	%r10
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L211:
	movl	$296, %r8d
	leaq	.LC8(%rip), %rcx
	movl	$68, %edx
	movq	%r12, %r13
	movl	$132, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L89:
	movl	$350, %r8d
	leaq	.LC8(%rip), %rcx
	movl	$138, %edx
	movl	$132, %esi
	movl	$34, %edi
	movq	-120(%rbp), %r14
	movq	%r12, %r13
	call	ERR_put_error@PLT
	movq	-80(%rbp), %rax
	subq	$8, %rsp
	leaq	.LC13(%rip), %r9
	leaq	.LC14(%rip), %rcx
	leaq	.LC15(%rip), %rsi
	movl	$6, %edi
	movq	8(%rax), %r8
	pushq	16(%rax)
	movq	(%rax), %rdx
	xorl	%eax, %eax
	call	ERR_add_error_data@PLT
	popq	%rax
	popq	%rdx
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L204:
	movl	$182, %r8d
	leaq	.LC8(%rip), %rcx
	movl	$110, %edx
.L185:
	movl	$131, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	subq	$8, %rsp
	movq	8(%rbx), %r8
	pushq	16(%rbx)
	movq	(%rbx), %rdx
	movl	$6, %edi
	xorl	%eax, %eax
	leaq	.LC13(%rip), %r9
	leaq	.LC14(%rip), %rcx
	leaq	.LC15(%rip), %rsi
	call	ERR_add_error_data@PLT
	popq	%rdi
	popq	%r8
	jmp	.L59
.L100:
	movl	$358, %r8d
	leaq	.LC8(%rip), %rcx
	movl	$142, %edx
	movq	%r12, %r13
	movl	$132, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L208:
	movl	$197, %r8d
	movl	$68, %edx
	movl	$131, %esi
	movl	$34, %edi
	leaq	.LC8(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L201:
	leaq	POLICYINFO_it(%rip), %rsi
	movq	%r14, %rdi
	call	ASN1_item_free@PLT
	movl	$155, %r8d
.L186:
	movl	$65, %edx
	movl	$130, %esi
	movl	$34, %edi
	leaq	.LC8(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L49
.L196:
	movl	$101, %r8d
	movl	$34, %edx
	movl	$130, %esi
	movl	$34, %edi
	leaq	.LC8(%rip), %rcx
	call	ERR_put_error@PLT
	movq	$0, -136(%rbp)
	jmp	.L44
.L199:
	movl	$140, %r8d
	leaq	.LC8(%rip), %rcx
	movl	$110, %edx
	jmp	.L187
.L200:
	movq	%r12, %rdi
	call	ASN1_OBJECT_free@PLT
	movl	$148, %r8d
	jmp	.L186
.L65:
	movl	$229, %r8d
	leaq	.LC8(%rip), %rcx
	movl	$138, %edx
	jmp	.L185
.L209:
	movl	$208, %r8d
	leaq	.LC8(%rip), %rcx
	movl	$137, %edx
	jmp	.L185
.L210:
	movl	$215, %r8d
	leaq	.LC8(%rip), %rcx
	movl	$135, %edx
	jmp	.L185
.L47:
	movl	$107, %r8d
	movl	$65, %edx
	movl	$130, %esi
	movl	$34, %edi
	leaq	.LC8(%rip), %rcx
	call	ERR_put_error@PLT
	movq	X509V3_conf_free@GOTPCREL(%rip), %rax
	movq	%rax, -96(%rbp)
	jmp	.L49
.L203:
	movl	$129, %r8d
	leaq	.LC8(%rip), %rcx
	movl	$135, %edx
	jmp	.L187
.L224:
	movl	$236, %r8d
	movl	$139, %edx
	movl	$131, %esi
	movl	$34, %edi
	leaq	.LC8(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L59
.L197:
	movq	X509V3_conf_free@GOTPCREL(%rip), %rax
	movq	%rax, -96(%rbp)
	jmp	.L106
	.cfi_endproc
.LFE1364:
	.size	r2i_certpol, .-r2i_certpol
	.p2align 4
	.globl	d2i_CERTIFICATEPOLICIES
	.type	d2i_CERTIFICATEPOLICIES, @function
d2i_CERTIFICATEPOLICIES:
.LFB1344:
	.cfi_startproc
	endbr64
	leaq	CERTIFICATEPOLICIES_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1344:
	.size	d2i_CERTIFICATEPOLICIES, .-d2i_CERTIFICATEPOLICIES
	.p2align 4
	.globl	i2d_CERTIFICATEPOLICIES
	.type	i2d_CERTIFICATEPOLICIES, @function
i2d_CERTIFICATEPOLICIES:
.LFB1345:
	.cfi_startproc
	endbr64
	leaq	CERTIFICATEPOLICIES_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1345:
	.size	i2d_CERTIFICATEPOLICIES, .-i2d_CERTIFICATEPOLICIES
	.p2align 4
	.globl	CERTIFICATEPOLICIES_new
	.type	CERTIFICATEPOLICIES_new, @function
CERTIFICATEPOLICIES_new:
.LFB1346:
	.cfi_startproc
	endbr64
	leaq	CERTIFICATEPOLICIES_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1346:
	.size	CERTIFICATEPOLICIES_new, .-CERTIFICATEPOLICIES_new
	.p2align 4
	.globl	CERTIFICATEPOLICIES_free
	.type	CERTIFICATEPOLICIES_free, @function
CERTIFICATEPOLICIES_free:
.LFB1347:
	.cfi_startproc
	endbr64
	leaq	CERTIFICATEPOLICIES_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1347:
	.size	CERTIFICATEPOLICIES_free, .-CERTIFICATEPOLICIES_free
	.p2align 4
	.globl	d2i_POLICYINFO
	.type	d2i_POLICYINFO, @function
d2i_POLICYINFO:
.LFB1348:
	.cfi_startproc
	endbr64
	leaq	POLICYINFO_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1348:
	.size	d2i_POLICYINFO, .-d2i_POLICYINFO
	.p2align 4
	.globl	i2d_POLICYINFO
	.type	i2d_POLICYINFO, @function
i2d_POLICYINFO:
.LFB1349:
	.cfi_startproc
	endbr64
	leaq	POLICYINFO_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1349:
	.size	i2d_POLICYINFO, .-i2d_POLICYINFO
	.p2align 4
	.globl	POLICYINFO_new
	.type	POLICYINFO_new, @function
POLICYINFO_new:
.LFB1350:
	.cfi_startproc
	endbr64
	leaq	POLICYINFO_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1350:
	.size	POLICYINFO_new, .-POLICYINFO_new
	.p2align 4
	.globl	d2i_POLICYQUALINFO
	.type	d2i_POLICYQUALINFO, @function
d2i_POLICYQUALINFO:
.LFB1352:
	.cfi_startproc
	endbr64
	leaq	POLICYQUALINFO_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1352:
	.size	d2i_POLICYQUALINFO, .-d2i_POLICYQUALINFO
	.p2align 4
	.globl	i2d_POLICYQUALINFO
	.type	i2d_POLICYQUALINFO, @function
i2d_POLICYQUALINFO:
.LFB1353:
	.cfi_startproc
	endbr64
	leaq	POLICYQUALINFO_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1353:
	.size	i2d_POLICYQUALINFO, .-i2d_POLICYQUALINFO
	.p2align 4
	.globl	POLICYQUALINFO_new
	.type	POLICYQUALINFO_new, @function
POLICYQUALINFO_new:
.LFB1354:
	.cfi_startproc
	endbr64
	leaq	POLICYQUALINFO_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1354:
	.size	POLICYQUALINFO_new, .-POLICYQUALINFO_new
	.p2align 4
	.globl	POLICYQUALINFO_free
	.type	POLICYQUALINFO_free, @function
POLICYQUALINFO_free:
.LFB1355:
	.cfi_startproc
	endbr64
	leaq	POLICYQUALINFO_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1355:
	.size	POLICYQUALINFO_free, .-POLICYQUALINFO_free
	.p2align 4
	.globl	d2i_USERNOTICE
	.type	d2i_USERNOTICE, @function
d2i_USERNOTICE:
.LFB1356:
	.cfi_startproc
	endbr64
	leaq	USERNOTICE_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1356:
	.size	d2i_USERNOTICE, .-d2i_USERNOTICE
	.p2align 4
	.globl	i2d_USERNOTICE
	.type	i2d_USERNOTICE, @function
i2d_USERNOTICE:
.LFB1357:
	.cfi_startproc
	endbr64
	leaq	USERNOTICE_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1357:
	.size	i2d_USERNOTICE, .-i2d_USERNOTICE
	.p2align 4
	.globl	USERNOTICE_new
	.type	USERNOTICE_new, @function
USERNOTICE_new:
.LFB1358:
	.cfi_startproc
	endbr64
	leaq	USERNOTICE_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1358:
	.size	USERNOTICE_new, .-USERNOTICE_new
	.p2align 4
	.globl	USERNOTICE_free
	.type	USERNOTICE_free, @function
USERNOTICE_free:
.LFB1359:
	.cfi_startproc
	endbr64
	leaq	USERNOTICE_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1359:
	.size	USERNOTICE_free, .-USERNOTICE_free
	.p2align 4
	.globl	d2i_NOTICEREF
	.type	d2i_NOTICEREF, @function
d2i_NOTICEREF:
.LFB1360:
	.cfi_startproc
	endbr64
	leaq	NOTICEREF_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1360:
	.size	d2i_NOTICEREF, .-d2i_NOTICEREF
	.p2align 4
	.globl	i2d_NOTICEREF
	.type	i2d_NOTICEREF, @function
i2d_NOTICEREF:
.LFB1361:
	.cfi_startproc
	endbr64
	leaq	NOTICEREF_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1361:
	.size	i2d_NOTICEREF, .-i2d_NOTICEREF
	.p2align 4
	.globl	NOTICEREF_new
	.type	NOTICEREF_new, @function
NOTICEREF_new:
.LFB1362:
	.cfi_startproc
	endbr64
	leaq	NOTICEREF_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1362:
	.size	NOTICEREF_new, .-NOTICEREF_new
	.p2align 4
	.globl	NOTICEREF_free
	.type	NOTICEREF_free, @function
NOTICEREF_free:
.LFB1363:
	.cfi_startproc
	endbr64
	leaq	NOTICEREF_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1363:
	.size	NOTICEREF_free, .-NOTICEREF_free
	.section	.rodata.str1.1
.LC28:
	.string	"Critical"
.LC29:
	.string	"Non Critical"
.LC30:
	.string	"%*s%s\n"
.LC31:
	.string	"%*sNo Qualifiers\n"
	.text
	.p2align 4
	.globl	X509_POLICY_NODE_print
	.type	X509_POLICY_NODE_print, @function
X509_POLICY_NODE_print:
.LFB1373:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rcx
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	(%rsi), %rbx
	leaq	.LC12(%rip), %rsi
	call	BIO_printf@PLT
	movq	%r12, %rdi
	movq	8(%rbx), %rsi
	call	i2a_ASN1_OBJECT@PLT
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testb	$16, (%rbx)
	movq	%r12, %rdi
	leaq	.LC29(%rip), %rax
	leaq	.LC28(%rip), %r8
	leaq	.LC30(%rip), %rsi
	cmove	%rax, %r8
	addl	$2, %r13d
	leaq	.LC1(%rip), %rcx
	xorl	%eax, %eax
	movl	%r13d, %edx
	call	BIO_printf@PLT
	movq	16(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L246
	addq	$8, %rsp
	movl	%r13d, %edx
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	print_qualifiers
	.p2align 4,,10
	.p2align 3
.L246:
	.cfi_restore_state
	addq	$8, %rsp
	movl	%r13d, %edx
	movq	%r12, %rdi
	xorl	%eax, %eax
	popq	%rbx
	leaq	.LC1(%rip), %rcx
	leaq	.LC31(%rip), %rsi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	BIO_printf@PLT
	.cfi_endproc
.LFE1373:
	.size	X509_POLICY_NODE_print, .-X509_POLICY_NODE_print
	.globl	NOTICEREF_it
	.section	.rodata.str1.1
.LC32:
	.string	"NOTICEREF"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	NOTICEREF_it, @object
	.size	NOTICEREF_it, 56
NOTICEREF_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	NOTICEREF_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC32
	.section	.rodata.str1.1
.LC33:
	.string	"noticenos"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	NOTICEREF_seq_tt, @object
	.size	NOTICEREF_seq_tt, 80
NOTICEREF_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC26
	.quad	DISPLAYTEXT_it
	.quad	4
	.quad	0
	.quad	8
	.quad	.LC33
	.quad	ASN1_INTEGER_it
	.globl	USERNOTICE_it
	.section	.rodata.str1.1
.LC34:
	.string	"USERNOTICE"
	.section	.data.rel.ro.local
	.align 32
	.type	USERNOTICE_it, @object
	.size	USERNOTICE_it, 56
USERNOTICE_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	USERNOTICE_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC34
	.section	.rodata.str1.1
.LC35:
	.string	"noticeref"
.LC36:
	.string	"exptext"
	.section	.data.rel.ro
	.align 32
	.type	USERNOTICE_seq_tt, @object
	.size	USERNOTICE_seq_tt, 80
USERNOTICE_seq_tt:
	.quad	1
	.quad	0
	.quad	0
	.quad	.LC35
	.quad	NOTICEREF_it
	.quad	1
	.quad	0
	.quad	8
	.quad	.LC36
	.quad	DISPLAYTEXT_it
	.globl	POLICYQUALINFO_it
	.section	.rodata.str1.1
.LC37:
	.string	"POLICYQUALINFO"
	.section	.data.rel.ro.local
	.align 32
	.type	POLICYQUALINFO_it, @object
	.size	POLICYQUALINFO_it, 56
POLICYQUALINFO_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	POLICYQUALINFO_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC37
	.section	.rodata.str1.1
.LC38:
	.string	"pqualid"
	.section	.data.rel.ro
	.align 32
	.type	POLICYQUALINFO_seq_tt, @object
	.size	POLICYQUALINFO_seq_tt, 80
POLICYQUALINFO_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC38
	.quad	ASN1_OBJECT_it
	.quad	256
	.quad	-1
	.quad	0
	.quad	.LC37
	.quad	POLICYQUALINFO_adb
	.section	.data.rel.ro.local
	.align 32
	.type	POLICYQUALINFO_adb, @object
	.size	POLICYQUALINFO_adb, 56
POLICYQUALINFO_adb:
	.quad	0
	.quad	0
	.quad	0
	.quad	POLICYQUALINFO_adbtbl
	.quad	2
	.quad	policydefault_tt
	.quad	0
	.section	.rodata.str1.1
.LC39:
	.string	"d.cpsuri"
.LC40:
	.string	"d.usernotice"
	.section	.data.rel.ro
	.align 32
	.type	POLICYQUALINFO_adbtbl, @object
	.size	POLICYQUALINFO_adbtbl, 96
POLICYQUALINFO_adbtbl:
	.quad	164
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC39
	.quad	ASN1_IA5STRING_it
	.quad	165
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC40
	.quad	USERNOTICE_it
	.section	.rodata.str1.1
.LC41:
	.string	"d.other"
	.section	.data.rel.ro
	.align 32
	.type	policydefault_tt, @object
	.size	policydefault_tt, 40
policydefault_tt:
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC41
	.quad	ASN1_ANY_it
	.globl	POLICYINFO_it
	.section	.rodata.str1.1
.LC42:
	.string	"POLICYINFO"
	.section	.data.rel.ro.local
	.align 32
	.type	POLICYINFO_it, @object
	.size	POLICYINFO_it, 56
POLICYINFO_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	POLICYINFO_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC42
	.section	.rodata.str1.1
.LC43:
	.string	"policyid"
.LC44:
	.string	"qualifiers"
	.section	.data.rel.ro
	.align 32
	.type	POLICYINFO_seq_tt, @object
	.size	POLICYINFO_seq_tt, 80
POLICYINFO_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC43
	.quad	ASN1_OBJECT_it
	.quad	5
	.quad	0
	.quad	8
	.quad	.LC44
	.quad	POLICYQUALINFO_it
	.globl	CERTIFICATEPOLICIES_it
	.section	.rodata.str1.1
.LC45:
	.string	"CERTIFICATEPOLICIES"
	.section	.data.rel.ro.local
	.align 32
	.type	CERTIFICATEPOLICIES_it, @object
	.size	CERTIFICATEPOLICIES_it, 56
CERTIFICATEPOLICIES_it:
	.byte	0
	.zero	7
	.quad	-1
	.quad	CERTIFICATEPOLICIES_item_tt
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC45
	.align 32
	.type	CERTIFICATEPOLICIES_item_tt, @object
	.size	CERTIFICATEPOLICIES_item_tt, 40
CERTIFICATEPOLICIES_item_tt:
	.quad	4
	.quad	0
	.quad	0
	.quad	.LC45
	.quad	POLICYINFO_it
	.globl	v3_cpols
	.align 32
	.type	v3_cpols, @object
	.size	v3_cpols, 104
v3_cpols:
	.long	89
	.long	0
	.quad	CERTIFICATEPOLICIES_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	i2r_certpol
	.quad	r2i_certpol
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
