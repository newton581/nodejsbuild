	.file	"srp_lib.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/srp/srp_lib.c"
	.text
	.p2align 4
	.type	srp_Calc_xy, @function
srp_Calc_xy:
.LFB491:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rdx, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	BN_num_bits@PLT
	movl	%eax, %ebx
	cmpq	%r13, %r12
	je	.L5
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	jns	.L9
.L5:
	cmpq	%r14, %r12
	je	.L4
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	jns	.L9
.L4:
	leal	14(%rbx), %r12d
	addl	$7, %ebx
	movl	$34, %edx
	cmovns	%ebx, %r12d
	leaq	.LC0(%rip), %rsi
	sarl	$3, %r12d
	leal	(%r12,%r12), %ebx
	movslq	%ebx, %rbx
	movq	%rbx, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L7
	movl	%r12d, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	BN_bn2binpad@PLT
	testl	%eax, %eax
	js	.L7
	movslq	%r12d, %rsi
	movl	%r12d, %edx
	movq	%r14, %rdi
	addq	%r15, %rsi
	call	BN_bn2binpad@PLT
	testl	%eax, %eax
	js	.L7
	call	EVP_sha1@PLT
	leaq	-80(%rbp), %r12
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	%rax, %r8
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	EVP_Digest@PLT
	testl	%eax, %eax
	je	.L7
	movq	%r12, %rdi
	xorl	%edx, %edx
	movl	$20, %esi
	call	BN_bin2bn@PLT
	movq	%rax, %r12
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L7:
	xorl	%r12d, %r12d
.L10:
	movl	$42, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	call	CRYPTO_free@PLT
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L21
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L1
.L21:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE491:
	.size	srp_Calc_xy, .-srp_Calc_xy
	.p2align 4
	.globl	SRP_Calc_u
	.type	SRP_Calc_u, @function
SRP_Calc_u:
.LFB493:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rdx, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	BN_num_bits@PLT
	movl	%eax, %ebx
	cmpq	%r12, %r13
	je	.L26
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	jns	.L30
.L26:
	cmpq	%r12, %r14
	je	.L25
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	jns	.L30
.L25:
	leal	14(%rbx), %r12d
	addl	$7, %ebx
	movl	$34, %edx
	cmovns	%ebx, %r12d
	leaq	.LC0(%rip), %rsi
	sarl	$3, %r12d
	leal	(%r12,%r12), %ebx
	movslq	%ebx, %rbx
	movq	%rbx, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L28
	movl	%r12d, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	BN_bn2binpad@PLT
	testl	%eax, %eax
	js	.L28
	movslq	%r12d, %rsi
	movl	%r12d, %edx
	movq	%r14, %rdi
	addq	%r15, %rsi
	call	BN_bn2binpad@PLT
	testl	%eax, %eax
	js	.L28
	call	EVP_sha1@PLT
	leaq	-80(%rbp), %r12
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	%rax, %r8
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	EVP_Digest@PLT
	testl	%eax, %eax
	je	.L28
	movq	%r12, %rdi
	xorl	%edx, %edx
	movl	$20, %esi
	call	BN_bin2bn@PLT
	movq	%rax, %r12
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L28:
	xorl	%r12d, %r12d
.L31:
	movl	$42, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	call	CRYPTO_free@PLT
.L22:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L41
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L22
.L41:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE493:
	.size	SRP_Calc_u, .-SRP_Calc_u
	.p2align 4
	.globl	SRP_Calc_server_key
	.type	SRP_Calc_server_key, @function
SRP_Calc_server_key:
.LFB494:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -64(%rbp)
	testq	%rdx, %rdx
	je	.L48
	movq	%rdi, %r14
	testq	%rdi, %rdi
	je	.L48
	testq	%rsi, %rsi
	movq	%rdx, %r12
	movq	%rsi, -56(%rbp)
	sete	%dl
	testq	%rcx, %rcx
	sete	%al
	orb	%al, %dl
	jne	.L48
	movq	%r8, %r13
	testq	%r8, %r8
	je	.L48
	call	BN_CTX_new@PLT
	movq	-56(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L46
	movq	%rsi, -56(%rbp)
	call	BN_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L46
	movq	-56(%rbp), %rsi
	movq	%r12, %rdx
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	%rax, %rdi
	xorl	%r12d, %r12d
	call	BN_mod_exp@PLT
	testl	%eax, %eax
	je	.L45
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	BN_mod_mul@PLT
	testl	%eax, %eax
	je	.L45
	call	BN_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L45
	movq	-64(%rbp), %rdx
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	BN_mod_exp@PLT
	testl	%eax, %eax
	jne	.L45
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	BN_free@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L46:
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
.L45:
	movq	%rbx, %rdi
	call	BN_CTX_free@PLT
	movq	%r15, %rdi
	call	BN_clear_free@PLT
.L42:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L42
	.cfi_endproc
.LFE494:
	.size	SRP_Calc_server_key, .-SRP_Calc_server_key
	.p2align 4
	.globl	SRP_Calc_B
	.type	SRP_Calc_B, @function
SRP_Calc_B:
.LFB495:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -56(%rbp)
	testq	%rdi, %rdi
	je	.L64
	movq	%rsi, %r12
	testq	%rsi, %rsi
	je	.L64
	movq	%rdx, %r14
	testq	%rdx, %rdx
	je	.L64
	cmpq	$0, -56(%rbp)
	je	.L64
	movq	%rdi, %r13
	call	BN_CTX_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L64
	call	BN_new@PLT
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L67
	call	BN_new@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L67
	call	BN_new@PLT
	xorl	%r10d, %r10d
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L66
	movq	%r15, %r8
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%rax, -72(%rbp)
	call	BN_mod_exp@PLT
	movq	-72(%rbp), %r9
	testl	%eax, %eax
	je	.L70
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r9, -72(%rbp)
	call	srp_Calc_xy
	movq	-72(%rbp), %r9
	testq	%rax, %rax
	je	.L70
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movq	%r15, %r8
	movq	%r12, %rcx
	movq	%rax, %rdx
	movq	%r9, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	BN_mod_mul@PLT
	movq	-72(%rbp), %r10
	movq	-80(%rbp), %r9
	testl	%eax, %eax
	je	.L69
	movq	-64(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r15, %r8
	movq	%r12, %rcx
	movq	%rbx, %rsi
	movq	%r9, -56(%rbp)
	call	BN_mod_add@PLT
	movq	-56(%rbp), %r9
	movq	-72(%rbp), %r10
	testl	%eax, %eax
	je	.L69
	.p2align 4,,10
	.p2align 3
.L66:
	movq	%r15, %rdi
	movq	%r9, -72(%rbp)
	movq	%r10, -56(%rbp)
	call	BN_CTX_free@PLT
	movq	-64(%rbp), %rdi
	call	BN_clear_free@PLT
	movq	%rbx, %rdi
	call	BN_clear_free@PLT
	movq	-56(%rbp), %r10
	movq	%r10, %rdi
	call	BN_free@PLT
	movq	-72(%rbp), %r9
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L64:
	xorl	%r9d, %r9d
.L60:
	addq	$40, %rsp
	movq	%r9, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	xorl	%r10d, %r10d
	xorl	%r9d, %r9d
	xorl	%ebx, %ebx
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L70:
	xorl	%r10d, %r10d
.L69:
	movq	%r9, %rdi
	movq	%r10, -56(%rbp)
	call	BN_free@PLT
	movq	-56(%rbp), %r10
	xorl	%r9d, %r9d
	jmp	.L66
	.cfi_endproc
.LFE495:
	.size	SRP_Calc_B, .-SRP_Calc_B
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	":"
	.text
	.p2align 4
	.globl	SRP_Calc_x
	.type	SRP_Calc_x, @function
SRP_Calc_x:
.LFB496:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	sete	%dl
	testq	%r15, %r15
	sete	%al
	orb	%al, %dl
	jne	.L90
	movq	%rdi, %r12
	testq	%rdi, %rdi
	je	.L90
	movq	%rsi, %r13
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L90
	movq	%r12, %rdi
	call	BN_num_bits@PLT
	movl	$133, %edx
	leaq	.LC0(%rip), %rsi
	leal	14(%rax), %edi
	addl	$7, %eax
	cmovns	%eax, %edi
	sarl	$3, %edi
	movslq	%edi, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L93
	call	EVP_sha1@PLT
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	jne	.L126
.L93:
	xorl	%r12d, %r12d
.L92:
	movq	%rbx, %rdi
	movl	$155, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r14, %rdi
	call	EVP_MD_CTX_free@PLT
.L86:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L127
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L126:
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L93
	movl	$1, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L93
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L93
	leaq	-80(%rbp), %r13
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	je	.L93
	call	EVP_sha1@PLT
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	je	.L93
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	BN_bn2bin@PLT
	testl	%eax, %eax
	js	.L93
	movq	%r12, %rdi
	call	BN_num_bits@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	leal	14(%rax), %edx
	addl	$7, %eax
	cmovns	%eax, %edx
	sarl	$3, %edx
	movslq	%edx, %rdx
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L93
	movl	$20, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L93
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	je	.L93
	xorl	%edx, %edx
	movl	$20, %esi
	movq	%r13, %rdi
	call	BN_bin2bn@PLT
	movq	%rax, %r12
	jmp	.L92
.L127:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE496:
	.size	SRP_Calc_x, .-SRP_Calc_x
	.p2align 4
	.globl	SRP_Calc_A
	.type	SRP_Calc_A, @function
SRP_Calc_A:
.LFB497:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	testq	%rsi, %rsi
	sete	%dl
	testq	%r12, %r12
	sete	%al
	orb	%al, %dl
	jne	.L132
	movq	%rdi, %r15
	testq	%rdi, %rdi
	je	.L132
	movq	%rsi, %r13
	call	BN_CTX_new@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L132
	call	BN_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L133
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	BN_mod_exp@PLT
	testl	%eax, %eax
	je	.L141
.L133:
	movq	%rbx, %rdi
	call	BN_CTX_free@PLT
.L128:
	addq	$8, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	.cfi_restore_state
	xorl	%r14d, %r14d
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L141:
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	BN_free@PLT
	jmp	.L133
	.cfi_endproc
.LFE497:
	.size	SRP_Calc_A, .-SRP_Calc_A
	.p2align 4
	.globl	SRP_Calc_client_key
	.type	SRP_Calc_client_key, @function
SRP_Calc_client_key:
.LFB498:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, -56(%rbp)
	testq	%r9, %r9
	je	.L146
	movq	%rsi, %r15
	testq	%rsi, %rsi
	je	.L146
	movq	%rdi, %r12
	testq	%rdi, %rdi
	je	.L146
	movq	%rdx, %r14
	testq	%rdx, %rdx
	je	.L146
	movq	%rcx, %rbx
	testq	%rcx, %rcx
	je	.L146
	cmpq	$0, -56(%rbp)
	je	.L146
	movq	%r9, %r13
	call	BN_CTX_new@PLT
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L146
	call	BN_new@PLT
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	je	.L149
	call	BN_new@PLT
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L149
	call	BN_new@PLT
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L151
	movq	-64(%rbp), %r8
	movq	-72(%rbp), %rdi
	movq	%r12, %rcx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	BN_mod_exp@PLT
	testl	%eax, %eax
	je	.L151
	movq	%r14, %rsi
	movq	%r12, %rdx
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	srp_Calc_xy
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L148
	movq	-64(%rbp), %r8
	movq	-72(%rbp), %rsi
	movq	%r12, %rcx
	movq	%rax, %rdx
	movq	-80(%rbp), %rdi
	movq	%rax, -96(%rbp)
	call	BN_mod_mul@PLT
	movq	-96(%rbp), %r9
	testl	%eax, %eax
	je	.L148
	movq	-64(%rbp), %r8
	movq	-80(%rbp), %rdx
	movq	%r12, %rcx
	movq	%r15, %rsi
	movq	-72(%rbp), %rdi
	call	BN_mod_sub@PLT
	movq	-96(%rbp), %r9
	testl	%eax, %eax
	je	.L148
	movq	-88(%rbp), %r15
	movq	-64(%rbp), %rcx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	BN_mul@PLT
	movq	-96(%rbp), %r9
	testl	%eax, %eax
	je	.L148
	movq	-80(%rbp), %rbx
	movq	-56(%rbp), %rsi
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	BN_add@PLT
	movq	-96(%rbp), %r9
	testl	%eax, %eax
	je	.L148
	movq	%r9, -56(%rbp)
	call	BN_new@PLT
	movq	-56(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L148
	movq	-64(%rbp), %r8
	movq	-72(%rbp), %rsi
	movq	%r12, %rcx
	movq	%rbx, %rdx
	movq	%rax, %rdi
	call	BN_mod_exp@PLT
	movq	-56(%rbp), %r9
	testl	%eax, %eax
	jne	.L148
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	BN_free@PLT
	movq	-56(%rbp), %r9
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L146:
	xorl	%r14d, %r14d
.L142:
	addq	$56, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L149:
	.cfi_restore_state
	movq	$0, -88(%rbp)
	xorl	%r14d, %r14d
	xorl	%r9d, %r9d
	movq	$0, -80(%rbp)
.L148:
	movq	-64(%rbp), %rdi
	movq	%r9, -56(%rbp)
	call	BN_CTX_free@PLT
	movq	-72(%rbp), %rdi
	call	BN_clear_free@PLT
	movq	-80(%rbp), %rdi
	call	BN_clear_free@PLT
	movq	-88(%rbp), %rdi
	call	BN_clear_free@PLT
	movq	-56(%rbp), %r9
	movq	%r9, %rdi
	call	BN_free@PLT
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L151:
	xorl	%r14d, %r14d
	xorl	%r9d, %r9d
	jmp	.L148
	.cfi_endproc
.LFE498:
	.size	SRP_Calc_client_key, .-SRP_Calc_client_key
	.p2align 4
	.globl	SRP_Verify_B_mod_N
	.type	SRP_Verify_B_mod_N, @function
SRP_Verify_B_mod_N:
.LFB499:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	testq	%rdi, %rdi
	je	.L176
	movq	%rsi, %r13
	testq	%rsi, %rsi
	je	.L176
	movq	%rdi, %r12
	call	BN_CTX_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L176
	call	BN_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L179
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	BN_nnmod@PLT
	testl	%eax, %eax
	jne	.L187
.L179:
	xorl	%r12d, %r12d
.L178:
	movq	%r14, %rdi
	call	BN_CTX_free@PLT
	movq	%r15, %rdi
	call	BN_free@PLT
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_restore_state
	xorl	%r12d, %r12d
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L187:
	.cfi_restore_state
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	BN_is_zero@PLT
	testl	%eax, %eax
	sete	%r12b
	jmp	.L178
	.cfi_endproc
.LFE499:
	.size	SRP_Verify_B_mod_N, .-SRP_Verify_B_mod_N
	.p2align 4
	.globl	SRP_Verify_A_mod_N
	.type	SRP_Verify_A_mod_N, @function
SRP_Verify_A_mod_N:
.LFB500:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	testq	%rdi, %rdi
	je	.L192
	movq	%rsi, %r13
	testq	%rsi, %rsi
	je	.L192
	movq	%rdi, %r12
	call	BN_CTX_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L192
	call	BN_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L195
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	BN_nnmod@PLT
	testl	%eax, %eax
	jne	.L203
.L195:
	xorl	%r12d, %r12d
.L194:
	movq	%r14, %rdi
	call	BN_CTX_free@PLT
	movq	%r15, %rdi
	call	BN_free@PLT
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L192:
	.cfi_restore_state
	xorl	%r12d, %r12d
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L203:
	.cfi_restore_state
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	BN_is_zero@PLT
	testl	%eax, %eax
	sete	%r12b
	jmp	.L194
	.cfi_endproc
.LFE500:
	.size	SRP_Verify_A_mod_N, .-SRP_Verify_A_mod_N
	.p2align 4
	.globl	SRP_check_known_gN_param
	.type	SRP_check_known_gN_param, @function
SRP_check_known_gN_param:
.LFB501:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L211
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testq	%rsi, %rsi
	je	.L205
	movq	%rdi, %r13
	leaq	8+knowngN(%rip), %rbx
	xorl	%r12d, %r12d
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L206:
	addq	$1, %r12
	addq	$24, %rbx
	cmpq	$7, %r12
	je	.L205
.L208:
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	call	BN_cmp@PLT
	testl	%eax, %eax
	jne	.L206
	movq	8(%rbx), %rdi
	movq	%r14, %rsi
	call	BN_cmp@PLT
	testl	%eax, %eax
	jne	.L206
	leaq	(%r12,%r12,2), %rdx
	leaq	knowngN(%rip), %rax
	movq	(%rax,%rdx,8), %rax
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L205:
	xorl	%eax, %eax
.L204:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L211:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE501:
	.size	SRP_check_known_gN_param, .-SRP_check_known_gN_param
	.p2align 4
	.globl	SRP_get_default_gN
	.type	SRP_get_default_gN, @function
SRP_get_default_gN:
.LFB502:
	.cfi_startproc
	endbr64
	leaq	knowngN(%rip), %rax
	testq	%rdi, %rdi
	je	.L228
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	knowngN(%rip), %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L219
	movq	24+knowngN(%rip), %rdi
	movq	%r12, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L220
	movq	48+knowngN(%rip), %rdi
	movq	%r12, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L221
	movq	72+knowngN(%rip), %rdi
	movq	%r12, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L222
	movq	96+knowngN(%rip), %rdi
	movq	%r12, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L223
	movq	120+knowngN(%rip), %rdi
	movq	%r12, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L224
	movq	144+knowngN(%rip), %rdi
	movq	%r12, %rsi
	call	strcmp@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	je	.L231
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L228:
	.cfi_restore 6
	.cfi_restore 12
	ret
.L219:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	xorl	%eax, %eax
.L217:
	leaq	(%rax,%rax,2), %rdx
	addq	$8, %rsp
	leaq	knowngN(%rip), %rax
	leaq	(%rax,%rdx,8), %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L220:
	.cfi_restore_state
	movl	$1, %eax
	jmp	.L217
.L221:
	movl	$2, %eax
	jmp	.L217
.L222:
	movl	$3, %eax
	jmp	.L217
.L223:
	movl	$4, %eax
	jmp	.L217
.L224:
	movl	$5, %eax
	jmp	.L217
.L231:
	movl	$6, %eax
	jmp	.L217
	.cfi_endproc
.LFE502:
	.size	SRP_get_default_gN, .-SRP_get_default_gN
	.section	.rodata.str1.1
.LC2:
	.string	"8192"
.LC3:
	.string	"6144"
.LC4:
	.string	"4096"
.LC5:
	.string	"3072"
.LC6:
	.string	"2048"
.LC7:
	.string	"1536"
.LC8:
	.string	"1024"
	.section	.data.rel,"aw"
	.align 32
	.type	knowngN, @object
	.size	knowngN, 168
knowngN:
	.quad	.LC2
	.quad	bn_generator_19
	.quad	bn_group_8192
	.quad	.LC3
	.quad	bn_generator_5
	.quad	bn_group_6144
	.quad	.LC4
	.quad	bn_generator_5
	.quad	bn_group_4096
	.quad	.LC5
	.quad	bn_generator_5
	.quad	bn_group_3072
	.quad	.LC6
	.quad	bn_generator_2
	.quad	bn_group_2048
	.quad	.LC7
	.quad	bn_generator_2
	.quad	bn_group_1536
	.quad	.LC8
	.quad	bn_generator_2
	.quad	bn_group_1024
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
