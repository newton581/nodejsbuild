	.file	"c_enc.c"
	.text
	.p2align 4
	.globl	CAST_encrypt
	.type	CAST_encrypt, @function
CAST_encrypt:
.LFB0:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	CAST_S_table0(%rip), %r10
	leaq	CAST_S_table1(%rip), %r9
	leaq	CAST_S_table3(%rip), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	4(%rdi), %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rsi), %eax
	movl	4(%rsi), %ecx
	addl	%r15d, %eax
	roll	%cl, %eax
	movzbl	%ah, %ecx
	movzbl	%al, %edx
	movl	(%r10,%rcx,4), %ebx
	movl	%eax, %ecx
	xorl	(%r9,%rdx,4), %ebx
	shrl	$24, %eax
	shrl	$16, %ecx
	leaq	CAST_S_table2(%rip), %rdx
	movzbl	%cl, %ecx
	addl	(%r8,%rcx,4), %ebx
	subl	(%rdx,%rax,4), %ebx
	movl	8(%rsi), %eax
	xorl	(%rdi), %ebx
	movl	12(%rsi), %ecx
	xorl	%ebx, %eax
	roll	%cl, %eax
	movl	%eax, %ecx
	shrl	$16, %ecx
	movzbl	%cl, %ecx
	xorl	(%r8,%rcx,4), %r15d
	movzbl	%ah, %ecx
	movq	%rcx, %r11
	movzbl	%al, %ecx
	shrl	$24, %eax
	movl	(%r10,%r11,4), %r12d
	subl	(%r9,%rcx,4), %r12d
	movl	%r12d, %ecx
	addl	(%rdx,%rax,4), %ecx
	movl	16(%rsi), %eax
	movl	%ecx, %r12d
	movl	20(%rsi), %ecx
	xorl	%r15d, %r12d
	subl	%r12d, %eax
	roll	%cl, %eax
	movzbl	%ah, %ecx
	movzbl	%al, %r11d
	movl	(%r9,%r11,4), %r14d
	addl	(%r10,%rcx,4), %r14d
	movl	%eax, %ecx
	shrl	$16, %eax
	shrl	$24, %ecx
	movzbl	%al, %eax
	xorl	(%rdx,%rcx,4), %r14d
	subl	(%r8,%rax,4), %r14d
	movl	24(%rsi), %eax
	xorl	%r14d, %ebx
	movl	28(%rsi), %ecx
	addl	%ebx, %eax
	roll	%cl, %eax
	movzbl	%ah, %ecx
	movq	%rcx, %r11
	movzbl	%al, %ecx
	movl	(%r10,%r11,4), %r11d
	xorl	(%r9,%rcx,4), %r11d
	movl	%eax, %ecx
	shrl	$24, %eax
	shrl	$16, %ecx
	movzbl	%cl, %ecx
	addl	(%r8,%rcx,4), %r11d
	subl	(%rdx,%rax,4), %r11d
	movl	%r11d, %r15d
	movl	32(%rsi), %eax
	movl	36(%rsi), %ecx
	xorl	%r12d, %r15d
	xorl	%r15d, %eax
	roll	%cl, %eax
	movl	%eax, %ecx
	shrl	$16, %eax
	movzbl	%al, %eax
	xorl	(%r8,%rax,4), %ebx
	movzbl	%ch, %eax
	movq	%rax, %r11
	movzbl	%cl, %eax
	shrl	$24, %ecx
	movl	(%r10,%r11,4), %r13d
	subl	(%r9,%rax,4), %r13d
	movl	%r13d, %eax
	addl	(%rdx,%rcx,4), %eax
	movl	44(%rsi), %ecx
	movl	%eax, %r14d
	movl	40(%rsi), %eax
	xorl	%ebx, %r14d
	subl	%r14d, %eax
	roll	%cl, %eax
	movl	%eax, %ecx
	movzbl	%ah, %eax
	movzbl	%cl, %r11d
	movl	(%r9,%r11,4), %r11d
	addl	(%r10,%rax,4), %r11d
	movl	%ecx, %eax
	shrl	$16, %ecx
	shrl	$24, %eax
	movzbl	%cl, %ecx
	xorl	(%rdx,%rax,4), %r11d
	movl	48(%rsi), %eax
	subl	(%r8,%rcx,4), %r11d
	movl	52(%rsi), %ecx
	xorl	%r15d, %r11d
	addl	%r11d, %eax
	roll	%cl, %eax
	movl	%eax, %ecx
	movzbl	%ah, %ebx
	movzbl	%al, %eax
	movl	(%r10,%rbx,4), %ebx
	xorl	(%r9,%rax,4), %ebx
	movl	%ecx, %eax
	shrl	$24, %ecx
	shrl	$16, %eax
	movzbl	%al, %eax
	addl	(%r8,%rax,4), %ebx
	movl	56(%rsi), %eax
	subl	(%rdx,%rcx,4), %ebx
	movl	60(%rsi), %ecx
	xorl	%r14d, %ebx
	xorl	%ebx, %eax
	movl	%ebx, %r13d
	roll	%cl, %eax
	movl	%eax, %ecx
	shrl	$16, %eax
	movzbl	%ch, %ebx
	movzbl	%al, %eax
	movl	(%r10,%rbx,4), %r12d
	xorl	(%r8,%rax,4), %r11d
	movzbl	%cl, %eax
	shrl	$24, %ecx
	movl	%r12d, %ebx
	subl	(%r9,%rax,4), %ebx
	addl	(%rdx,%rcx,4), %ebx
	movl	64(%rsi), %eax
	movl	%ebx, %r12d
	movl	68(%rsi), %ecx
	xorl	%r11d, %r12d
	subl	%r12d, %eax
	roll	%cl, %eax
	movl	%eax, %ecx
	movzbl	%ah, %eax
	movzbl	%cl, %r11d
	movl	(%r9,%r11,4), %ebx
	addl	(%r10,%rax,4), %ebx
	movl	%ecx, %eax
	shrl	$16, %ecx
	shrl	$24, %eax
	movzbl	%cl, %ecx
	xorl	(%rdx,%rax,4), %ebx
	movl	72(%rsi), %eax
	subl	(%r8,%rcx,4), %ebx
	movl	76(%rsi), %ecx
	xorl	%r13d, %ebx
	addl	%ebx, %eax
	roll	%cl, %eax
	movl	%eax, %ecx
	movzbl	%ah, %eax
	movzbl	%cl, %r11d
	movl	(%r10,%rax,4), %eax
	xorl	(%r9,%r11,4), %eax
	movl	%ecx, %r11d
	shrl	$24, %ecx
	shrl	$16, %r11d
	movzbl	%r11b, %r11d
	addl	(%r8,%r11,4), %eax
	subl	(%rdx,%rcx,4), %eax
	xorl	%r12d, %eax
	movl	84(%rsi), %ecx
	movl	%eax, %r11d
	xorl	80(%rsi), %eax
	roll	%cl, %eax
	movl	%eax, %ecx
	shrl	$16, %eax
	movzbl	%al, %eax
	movzbl	%cl, %r12d
	xorl	(%r8,%rax,4), %ebx
	movzbl	%ch, %eax
	shrl	$24, %ecx
	movl	(%r10,%rax,4), %eax
	subl	(%r9,%r12,4), %eax
	addl	(%rdx,%rcx,4), %eax
	movl	92(%rsi), %ecx
	xorl	%eax, %ebx
	movl	88(%rsi), %eax
	subl	%ebx, %eax
	roll	%cl, %eax
	movl	%eax, %ecx
	movzbl	%ah, %eax
	movq	%rax, %r12
	movzbl	%cl, %eax
	movl	(%r9,%rax,4), %eax
	addl	(%r10,%r12,4), %eax
	movl	%ecx, %r12d
	shrl	$16, %ecx
	movzbl	%cl, %ecx
	shrl	$24, %r12d
	xorl	(%rdx,%r12,4), %eax
	subl	(%r8,%rcx,4), %eax
	movl	128(%rsi), %ecx
	xorl	%r11d, %eax
	movl	%eax, %r15d
	testl	%ecx, %ecx
	jne	.L2
	movl	100(%rsi), %ecx
	addl	96(%rsi), %eax
	movl	%eax, %r11d
	roll	%cl, %r11d
	movl	%r11d, %ecx
	movzbl	%r11b, %r12d
	movzbl	%ch, %eax
	movl	(%r10,%rax,4), %r11d
	xorl	(%r9,%r12,4), %r11d
	movl	%ecx, %r12d
	shrl	$24, %ecx
	shrl	$16, %r12d
	movzbl	%r12b, %r12d
	addl	(%r8,%r12,4), %r11d
	subl	(%rdx,%rcx,4), %r11d
	xorl	%r11d, %ebx
	movl	104(%rsi), %r11d
	movl	108(%rsi), %ecx
	xorl	%ebx, %r11d
	roll	%cl, %r11d
	movl	%r11d, %ecx
	shrl	$16, %r11d
	movzbl	%r11b, %r11d
	movzbl	%ch, %eax
	xorl	(%r8,%r11,4), %r15d
	movzbl	%cl, %r11d
	shrl	$24, %ecx
	movl	(%r10,%rax,4), %r12d
	movl	112(%rsi), %eax
	subl	(%r9,%r11,4), %r12d
	addl	(%rdx,%rcx,4), %r12d
	movl	116(%rsi), %ecx
	xorl	%r15d, %r12d
	subl	%r12d, %eax
	roll	%cl, %eax
	movl	%eax, %ecx
	movzbl	%ah, %eax
	movq	%rax, %r11
	movzbl	%cl, %eax
	movl	(%r9,%rax,4), %eax
	addl	(%r10,%r11,4), %eax
	movl	%ecx, %r11d
	shrl	$16, %ecx
	shrl	$24, %r11d
	movzbl	%cl, %ecx
	xorl	(%rdx,%r11,4), %eax
	movl	120(%rsi), %r11d
	subl	(%r8,%rcx,4), %eax
	movl	124(%rsi), %ecx
	xorl	%eax, %ebx
	addl	%ebx, %r11d
	roll	%cl, %r11d
	movl	%r11d, %ecx
	movzbl	%r11b, %esi
	movzbl	%ch, %eax
	shrl	$24, %ecx
	movl	(%r10,%rax,4), %eax
	xorl	(%r9,%rsi,4), %eax
	movl	%r11d, %esi
	shrl	$16, %esi
	movzbl	%sil, %esi
	addl	(%r8,%rsi,4), %eax
	subl	(%rdx,%rcx,4), %eax
	xorl	%r12d, %eax
	movl	%eax, %r15d
.L2:
	movl	%ebx, 4(%rdi)
	popq	%rbx
	movl	%r15d, (%rdi)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	CAST_encrypt, .-CAST_encrypt
	.p2align 4
	.globl	CAST_decrypt
	.type	CAST_decrypt, @function
CAST_decrypt:
.LFB1:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	CAST_S_table0(%rip), %r9
	leaq	CAST_S_table1(%rip), %r8
	leaq	CAST_S_table3(%rip), %rdx
	leaq	CAST_S_table2(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	128(%rsi), %ecx
	movl	(%rdi), %r13d
	movl	4(%rdi), %ebx
	testl	%ecx, %ecx
	jne	.L6
	movl	120(%rsi), %r10d
	movl	124(%rsi), %ecx
	addl	%ebx, %r10d
	roll	%cl, %r10d
	movl	%r10d, %ecx
	movzbl	%r10b, %r11d
	movzbl	%ch, %edi
	movl	(%r9,%rdi,4), %r10d
	xorl	(%r8,%r11,4), %r10d
	movl	%ecx, %r11d
	shrl	$24, %ecx
	shrl	$16, %r11d
	movzbl	%r11b, %r11d
	addl	(%rdx,%r11,4), %r10d
	subl	(%rax,%rcx,4), %r10d
	xorl	%r10d, %r13d
	movl	112(%rsi), %r10d
	movl	116(%rsi), %ecx
	subl	%r13d, %r10d
	roll	%cl, %r10d
	movl	%r10d, %ecx
	movzbl	%r10b, %r11d
	shrl	$24, %r10d
	movzbl	%ch, %edi
	shrl	$16, %ecx
	movl	(%r8,%r11,4), %r12d
	movzbl	%cl, %ecx
	addl	(%r9,%rdi,4), %r12d
	xorl	(%rax,%r10,4), %r12d
	movl	104(%rsi), %r10d
	subl	(%rdx,%rcx,4), %r12d
	xorl	%r12d, %ebx
	movl	108(%rsi), %ecx
	xorl	%ebx, %r10d
	roll	%cl, %r10d
	movl	%r10d, %ecx
	shrl	$16, %r10d
	movzbl	%r10b, %r10d
	movzbl	%ch, %edi
	xorl	(%rdx,%r10,4), %r13d
	movzbl	%cl, %r10d
	shrl	$24, %ecx
	movl	(%r9,%rdi,4), %r11d
	subl	(%r8,%r10,4), %r11d
	movl	96(%rsi), %r10d
	addl	(%rax,%rcx,4), %r11d
	movl	100(%rsi), %ecx
	xorl	%r11d, %r13d
	addl	%r13d, %r10d
	roll	%cl, %r10d
	movl	%r10d, %ecx
	movzbl	%r10b, %r10d
	movzbl	%ch, %edi
	movl	(%r9,%rdi,4), %r12d
	xorl	(%r8,%r10,4), %r12d
	movl	%ecx, %r10d
	shrl	$24, %ecx
	shrl	$16, %r10d
	movzbl	%r10b, %r10d
	addl	(%rdx,%r10,4), %r12d
	subl	(%rax,%rcx,4), %r12d
	xorl	%r12d, %ebx
.L6:
	movl	88(%rsi), %r10d
	movl	92(%rsi), %ecx
	subl	%ebx, %r10d
	roll	%cl, %r10d
	movl	%r10d, %ecx
	movzbl	%r10b, %r10d
	movzbl	%ch, %edi
	movl	%ecx, %r11d
	shrl	$16, %ecx
	movl	(%r8,%r10,4), %r10d
	shrl	$24, %r11d
	movzbl	%cl, %ecx
	addl	(%r9,%rdi,4), %r10d
	xorl	(%rax,%r11,4), %r10d
	subl	(%rdx,%rcx,4), %r10d
	xorl	%r10d, %r13d
	movl	80(%rsi), %r10d
	movl	84(%rsi), %ecx
	xorl	%r13d, %r10d
	roll	%cl, %r10d
	movl	%r10d, %ecx
	shrl	$16, %r10d
	movzbl	%r10b, %r10d
	movzbl	%ch, %edi
	xorl	(%rdx,%r10,4), %ebx
	movzbl	%cl, %r10d
	movl	(%r9,%rdi,4), %r12d
	shrl	$24, %ecx
	subl	(%r8,%r10,4), %r12d
	movl	%r12d, %r10d
	addl	(%rax,%rcx,4), %r10d
	movl	76(%rsi), %ecx
	movl	%r10d, %r12d
	movl	72(%rsi), %r10d
	xorl	%ebx, %r12d
	addl	%r12d, %r10d
	roll	%cl, %r10d
	movl	%r10d, %ecx
	movzbl	%r10b, %r11d
	movzbl	%ch, %ebx
	movl	(%r9,%rbx,4), %r10d
	xorl	(%r8,%r11,4), %r10d
	movl	%ecx, %r11d
	shrl	$24, %ecx
	shrl	$16, %r11d
	movzbl	%r11b, %r11d
	addl	(%rdx,%r11,4), %r10d
	movl	64(%rsi), %r11d
	subl	(%rax,%rcx,4), %r10d
	movl	68(%rsi), %ecx
	xorl	%r13d, %r10d
	subl	%r10d, %r11d
	roll	%cl, %r11d
	movl	%r11d, %ecx
	movzbl	%r11b, %r11d
	movzbl	%ch, %ebx
	movl	(%r8,%r11,4), %r11d
	addl	(%r9,%rbx,4), %r11d
	movl	%ecx, %ebx
	shrl	$16, %ecx
	shrl	$24, %ebx
	movzbl	%cl, %ecx
	xorl	(%rax,%rbx,4), %r11d
	subl	(%rdx,%rcx,4), %r11d
	movl	%r11d, %ebx
	movl	56(%rsi), %r11d
	movl	60(%rsi), %ecx
	xorl	%r12d, %ebx
	xorl	%ebx, %r11d
	roll	%cl, %r11d
	movl	%r11d, %ecx
	shrl	$16, %r11d
	movzbl	%r11b, %r11d
	movzbl	%ch, %edi
	xorl	(%rdx,%r11,4), %r10d
	movzbl	%cl, %r11d
	movl	(%r9,%rdi,4), %r12d
	shrl	$24, %ecx
	subl	(%r8,%r11,4), %r12d
	movl	%r12d, %r11d
	addl	(%rax,%rcx,4), %r11d
	movl	52(%rsi), %ecx
	xorl	%r11d, %r10d
	movl	48(%rsi), %r11d
	addl	%r10d, %r11d
	roll	%cl, %r11d
	movl	%r11d, %ecx
	movzbl	%r11b, %r12d
	movzbl	%ch, %edi
	movl	(%r9,%rdi,4), %r11d
	xorl	(%r8,%r12,4), %r11d
	movl	%ecx, %r12d
	shrl	$24, %ecx
	shrl	$16, %r12d
	movzbl	%r12b, %r12d
	addl	(%rdx,%r12,4), %r11d
	subl	(%rax,%rcx,4), %r11d
	xorl	%ebx, %r11d
	movl	40(%rsi), %ebx
	movl	44(%rsi), %ecx
	subl	%r11d, %ebx
	roll	%cl, %ebx
	movl	%ebx, %ecx
	movzbl	%bh, %ebx
	movzbl	%cl, %r12d
	movl	(%r8,%r12,4), %r12d
	addl	(%r9,%rbx,4), %r12d
	movl	%ecx, %ebx
	shrl	$16, %ecx
	shrl	$24, %ebx
	movzbl	%cl, %ecx
	xorl	(%rax,%rbx,4), %r12d
	subl	(%rdx,%rcx,4), %r12d
	xorl	%r10d, %r12d
	movl	32(%rsi), %r10d
	movl	36(%rsi), %ecx
	xorl	%r12d, %r10d
	roll	%cl, %r10d
	movl	%r10d, %ecx
	shrl	$16, %r10d
	movzbl	%r10b, %r10d
	movzbl	%ch, %ebx
	xorl	(%rdx,%r10,4), %r11d
	movzbl	%cl, %r10d
	shrl	$24, %ecx
	movl	(%r9,%rbx,4), %ebx
	subl	(%r8,%r10,4), %ebx
	movl	24(%rsi), %r10d
	addl	(%rax,%rcx,4), %ebx
	movl	28(%rsi), %ecx
	xorl	%r11d, %ebx
	addl	%ebx, %r10d
	roll	%cl, %r10d
	movl	%r10d, %ecx
	movzbl	%r10b, %r11d
	movzbl	%ch, %edi
	movl	(%r9,%rdi,4), %r10d
	xorl	(%r8,%r11,4), %r10d
	movl	%ecx, %r11d
	shrl	$24, %ecx
	shrl	$16, %r11d
	movzbl	%r11b, %r11d
	addl	(%rdx,%r11,4), %r10d
	movl	16(%rsi), %r11d
	subl	(%rax,%rcx,4), %r10d
	movl	20(%rsi), %ecx
	xorl	%r12d, %r10d
	subl	%r10d, %r11d
	roll	%cl, %r11d
	movl	%r11d, %ecx
	movzbl	%r11b, %r11d
	movzbl	%ch, %edi
	movl	%ecx, %r12d
	shrl	$16, %ecx
	movl	(%r8,%r11,4), %r11d
	shrl	$24, %r12d
	movzbl	%cl, %ecx
	addl	(%r9,%rdi,4), %r11d
	xorl	(%rax,%r12,4), %r11d
	subl	(%rdx,%rcx,4), %r11d
	xorl	%ebx, %r11d
	movl	8(%rsi), %ebx
	movl	12(%rsi), %ecx
	xorl	%r11d, %ebx
	roll	%cl, %ebx
	movl	%ebx, %ecx
	shrl	$16, %ebx
	movzbl	%bl, %ebx
	xorl	(%rdx,%rbx,4), %r10d
	movzbl	%ch, %ebx
	movl	%r10d, %r12d
	movzbl	%cl, %r10d
	shrl	$24, %ecx
	movl	(%r9,%rbx,4), %ebx
	subl	(%r8,%r10,4), %ebx
	addl	(%rax,%rcx,4), %ebx
	movl	%ebx, %r10d
	movl	(%rsi), %ebx
	movl	4(%rsi), %ecx
	xorl	%r12d, %r10d
	addl	%r10d, %ebx
	movl	%r10d, 4(%r14)
	roll	%cl, %ebx
	movl	%ebx, %ecx
	movzbl	%bh, %esi
	movzbl	%bl, %ebx
	movl	(%r9,%rsi,4), %esi
	xorl	(%r8,%rbx,4), %esi
	movl	%ecx, %r8d
	shrl	$24, %ecx
	shrl	$16, %r8d
	popq	%rbx
	popq	%r12
	movzbl	%r8b, %r8d
	popq	%r13
	addl	(%rdx,%r8,4), %esi
	movl	%esi, %edx
	subl	(%rax,%rcx,4), %edx
	xorl	%edx, %r11d
	movl	%r11d, (%r14)
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1:
	.size	CAST_decrypt, .-CAST_decrypt
	.p2align 4
	.globl	CAST_cbc_encrypt
	.type	CAST_cbc_encrypt, @function
CAST_cbc_encrypt:
.LFB2:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$104, %rsp
	movq	%rdx, -104(%rbp)
	movl	4(%r8), %ecx
	movq	%rdi, -112(%rbp)
	bswap	%ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-8(%rdx), %rax
	movl	(%r8), %edx
	movq	%rax, -120(%rbp)
	bswap	%edx
	testl	%r9d, %r9d
	je	.L10
	testq	%rax, %rax
	js	.L37
	shrq	$3, %rax
	leaq	-64(%rbp), %r12
	movq	%rdi, %r13
	movq	%rax, -72(%rbp)
	leaq	8(,%rax,8), %rax
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	addq	%rsi, %rax
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L12:
	movl	0(%r13), %esi
	movl	4(%r13), %eax
	addq	$8, %r15
	addq	$8, %r13
	bswap	%eax
	bswap	%esi
	xorl	%eax, %ecx
	xorl	%esi, %edx
	movq	%r14, %rsi
	movl	%edx, -64(%rbp)
	movl	%ecx, -60(%rbp)
	call	CAST_encrypt
	movl	-64(%rbp), %edx
	movl	-60(%rbp), %ecx
	movl	%edx, %eax
	movb	%dl, -5(%r15)
	shrl	$24, %eax
	movb	%al, -8(%r15)
	movl	%edx, %eax
	shrl	$16, %eax
	movb	%al, -7(%r15)
	movzbl	%dh, %eax
	movb	%al, -6(%r15)
	movl	%ecx, %eax
	shrl	$24, %eax
	movb	%al, -4(%r15)
	movl	%ecx, %eax
	shrl	$16, %eax
	movb	%al, -3(%r15)
	movzbl	%ch, %eax
	movb	%al, -2(%r15)
	movb	%cl, -1(%r15)
	cmpq	%r15, %r12
	jne	.L12
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %rdi
	addq	%rdi, -112(%rbp)
	movq	-104(%rbp), %rdi
	negq	%rax
	salq	$3, %rax
	leaq	-16(%rdi,%rax), %rdi
	addq	-120(%rbp), %rax
	movq	%rax, -104(%rbp)
.L11:
	cmpq	$-8, %rdi
	je	.L54
	movq	-112(%rbp), %rax
	leaq	8(%rax,%rdi), %r8
	movq	-104(%rbp), %rax
	cmpq	$7, %rax
	ja	.L15
	leaq	.L17(%rip), %r9
	movslq	(%r9,%rax,4), %rdi
	addq	%r9, %rdi
	notrack jmp	*%rdi
	.section	.rodata
	.align 4
	.align 4
.L17:
	.long	.L15-.L17
	.long	.L38-.L17
	.long	.L39-.L17
	.long	.L40-.L17
	.long	.L41-.L17
	.long	.L42-.L17
	.long	.L43-.L17
	.long	.L16-.L17
	.text
	.p2align 4,,10
	.p2align 3
.L10:
	testq	%rax, %rax
	js	.L44
	shrq	$3, %rax
	movq	%r8, -144(%rbp)
	leaq	-64(%rbp), %r12
	movq	%rdi, %rbx
	movq	%rax, -136(%rbp)
	leaq	8(,%rax,8), %rax
	movq	%r12, %r13
	movl	%edx, %r12d
	movq	%rax, -128(%rbp)
	addq	%rsi, %rax
	movq	%rax, -96(%rbp)
	.p2align 4,,10
	.p2align 3
.L26:
	movl	(%rbx), %r8d
	movl	%ecx, -80(%rbp)
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	4(%rbx), %ecx
	movl	%r12d, -84(%rbp)
	addq	$8, %r15
	addq	$8, %rbx
	bswap	%r8d
	movl	%r8d, -64(%rbp)
	movl	%r8d, %r12d
	bswap	%ecx
	movl	%ecx, -60(%rbp)
	movl	%ecx, -72(%rbp)
	call	CAST_decrypt
	movl	-84(%rbp), %edx
	xorl	-64(%rbp), %edx
	movl	%edx, %esi
	movl	-80(%rbp), %eax
	xorl	-60(%rbp), %eax
	movb	%dl, -5(%r15)
	shrl	$24, %esi
	movzbl	%dh, %ecx
	movb	%sil, -8(%r15)
	movl	%edx, %esi
	movl	%eax, %edx
	shrl	$24, %edx
	movb	%cl, -6(%r15)
	shrl	$16, %esi
	movzbl	%ah, %ecx
	movb	%dl, -4(%r15)
	movl	%eax, %edx
	movb	%cl, -2(%r15)
	shrl	$16, %edx
	movl	-72(%rbp), %ecx
	movb	%sil, -7(%r15)
	movb	%dl, -3(%r15)
	cmpq	-96(%rbp), %r15
	movb	%al, -1(%r15)
	jne	.L26
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rdi
	movl	%r12d, %edx
	addq	%rdi, -112(%rbp)
	movq	-104(%rbp), %rdi
	negq	%rax
	movq	-144(%rbp), %rbx
	salq	$3, %rax
	leaq	-16(%rdi,%rax), %r12
	addq	-120(%rbp), %rax
	movq	%rax, -104(%rbp)
.L25:
	cmpq	$-8, %r12
	je	.L27
	movq	-112(%rbp), %rax
	movq	%r14, %rsi
	leaq	-64(%rbp), %rdi
	movl	%ecx, -84(%rbp)
	movl	%edx, -80(%rbp)
	movl	(%rax), %r13d
	movl	4(%rax), %eax
	bswap	%r13d
	bswap	%eax
	movl	%r13d, -64(%rbp)
	movl	%eax, -60(%rbp)
	movl	%eax, -72(%rbp)
	call	CAST_decrypt
	movl	-80(%rbp), %edx
	movl	-84(%rbp), %ecx
	leaq	8(%r15,%r12), %rax
	movq	-104(%rbp), %rsi
	xorl	-64(%rbp), %edx
	xorl	-60(%rbp), %ecx
	movl	-72(%rbp), %r8d
	movl	%edx, %r10d
	cmpq	$7, %rsi
	ja	.L45
	leaq	.L29(%rip), %rdi
	movslq	(%rdi,%rsi,4), %rsi
	addq	%rdi, %rsi
	notrack jmp	*%rsi
	.section	.rodata
	.align 4
	.align 4
.L29:
	.long	.L45-.L29
	.long	.L35-.L29
	.long	.L46-.L29
	.long	.L33-.L29
	.long	.L47-.L29
	.long	.L31-.L29
	.long	.L30-.L29
	.long	.L28-.L29
	.text
.L28:
	movb	%ch, -1(%rax)
	subq	$1, %rax
.L30:
	movl	%ecx, %esi
	subq	$1, %rax
	shrl	$16, %esi
	movb	%sil, (%rax)
.L31:
	shrl	$24, %ecx
	leaq	-1(%rax), %rsi
	movb	%cl, -1(%rax)
.L32:
	movb	%r10b, -1(%rsi)
	leaq	-1(%rsi), %rax
.L33:
	movl	%r10d, %ecx
	leaq	-1(%rax), %rdi
	movb	%ch, -1(%rax)
.L34:
	movl	%r10d, %esi
	leaq	-1(%rdi), %rax
	shrl	$16, %esi
	movb	%sil, -1(%rdi)
.L35:
	movl	%r10d, %edx
	movl	%r8d, %ecx
	shrl	$24, %edx
	movb	%dl, -1(%rax)
	movl	%r13d, %edx
.L27:
	bswap	%edx
	bswap	%ecx
	movl	%edx, (%rbx)
	movl	%ecx, 4(%rbx)
.L9:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L55
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	xorl	%edi, %edi
.L18:
	movzbl	-1(%r8), %r9d
	subq	$1, %r8
	salq	$16, %r9
	orl	%r9d, %edi
.L19:
	leaq	-1(%r8), %r9
	movzbl	-1(%r8), %r8d
	salq	$24, %r8
	orl	%r8d, %edi
	xorl	%edi, %ecx
.L20:
	movzbl	-1(%r9), %edi
	leaq	-1(%r9), %r8
.L21:
	leaq	-1(%r8), %r9
	movzbl	-1(%r8), %r8d
	salq	$8, %r8
	orl	%r8d, %edi
.L22:
	leaq	-1(%r9), %r8
	movzbl	-1(%r9), %r9d
	salq	$16, %r9
	orl	%r9d, %edi
.L23:
	movzbl	-1(%r8), %r8d
	salq	$24, %r8
	orl	%r8d, %edi
	xorl	%edi, %edx
.L15:
	leaq	-64(%rbp), %rdi
	movq	%r14, %rsi
	movl	%edx, -64(%rbp)
	movl	%ecx, -60(%rbp)
	call	CAST_encrypt
	movl	-64(%rbp), %edx
	movl	-60(%rbp), %ecx
	movl	%edx, %eax
	movl	%ecx, %r12d
	movl	%edx, %esi
	movl	%edx, %edi
	shrl	$24, %eax
	bswap	%r12d
	movl	%r12d, 4(%r15)
	bswap	%esi
	movl	%eax, %r9d
	movzbl	%dh, %eax
	movl	%esi, (%r15)
	movl	%ecx, %esi
	shrl	$16, %edi
	movl	%eax, %r8d
	movl	%ecx, %eax
	shrl	$16, %esi
	shrl	$24, %eax
	movl	%eax, %r11d
	movzbl	%ch, %eax
.L14:
	movb	%r9b, (%rbx)
	movb	%dil, 1(%rbx)
	movb	%r8b, 2(%rbx)
	movb	%dl, 3(%rbx)
	movb	%r11b, 4(%rbx)
	movb	%sil, 5(%rbx)
	movb	%al, 6(%rbx)
	movb	%cl, 7(%rbx)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L37:
	movq	%rax, %rdi
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L44:
	movq	%rax, %r12
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L54:
	movl	%edx, %eax
	movl	%edx, %edi
	movl	%ecx, %esi
	shrl	$24, %eax
	shrl	$16, %edi
	movl	%eax, %r9d
	movzbl	%dh, %eax
	shrl	$16, %esi
	movl	%eax, %r8d
	movl	%ecx, %eax
	shrl	$24, %eax
	movl	%eax, %r11d
	movzbl	%ch, %eax
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L38:
	xorl	%edi, %edi
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L39:
	movq	%r8, %r9
	xorl	%edi, %edi
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L40:
	xorl	%edi, %edi
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L41:
	movq	%r8, %r9
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L42:
	xorl	%edi, %edi
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L16:
	movzbl	-1(%r8), %edi
	subq	$1, %r8
	sall	$8, %edi
	jmp	.L18
.L46:
	movq	%rax, %rdi
	jmp	.L34
.L47:
	movq	%rax, %rsi
	jmp	.L32
.L55:
	call	__stack_chk_fail@PLT
.L45:
	movl	%r8d, %ecx
	movl	%r13d, %edx
	jmp	.L27
	.cfi_endproc
.LFE2:
	.size	CAST_cbc_encrypt, .-CAST_cbc_encrypt
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
