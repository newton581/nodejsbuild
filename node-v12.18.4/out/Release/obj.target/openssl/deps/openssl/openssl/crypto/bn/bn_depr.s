	.file	"bn_depr.c"
	.text
	.p2align 4
	.globl	BN_generate_prime
	.type	BN_generate_prime, @function
BN_generate_prime:
.LFB252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	movq	%r9, %rsi
	leaq	-80(%rbp), %r9
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r9, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$56, %rsp
	movq	16(%rbp), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r9, -88(%rbp)
	call	BN_GENCB_set_old@PLT
	testq	%r12, %r12
	movq	-88(%rbp), %r9
	je	.L11
.L2:
	movq	%rbx, %r8
	movq	%r15, %rcx
	movl	%r14d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	BN_generate_prime_ex@PLT
	testl	%eax, %eax
	je	.L3
.L4:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L12
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	call	BN_new@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.L2
	.p2align 4,,10
	.p2align 3
.L3:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	BN_free@PLT
	jmp	.L4
.L12:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE252:
	.size	BN_generate_prime, .-BN_generate_prime
	.p2align 4
	.globl	BN_is_prime
	.type	BN_is_prime, @function
BN_is_prime:
.LFB253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r15
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	movq	%rdx, %rsi
	movq	%r8, %rdx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	BN_GENCB_set_old@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	BN_is_prime_ex@PLT
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L16
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L16:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE253:
	.size	BN_is_prime, .-BN_is_prime
	.p2align 4
	.globl	BN_is_prime_fasttest
	.type	BN_is_prime_fasttest, @function
BN_is_prime_fasttest:
.LFB254:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r9d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	movq	%rdx, %rsi
	movq	%r8, %rdx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-80(%rbp), %rbx
	movq	%rbx, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	BN_GENCB_set_old@PLT
	movq	%rbx, %r8
	movl	%r15d, %ecx
	movq	%r14, %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	BN_is_prime_fasttest_ex@PLT
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L20
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L20:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE254:
	.size	BN_is_prime_fasttest, .-BN_is_prime_fasttest
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
