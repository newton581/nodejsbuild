	.file	"e_rc4.c"
	.text
	.p2align 4
	.type	rc4_cipher, @function
rc4_cipher:
.LFB448:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	subq	$8, %rsp
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	RC4@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE448:
	.size	rc4_cipher, .-rc4_cipher
	.p2align 4
	.type	rc4_init_key, @function
rc4_init_key:
.LFB447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r14, %rdx
	movl	%r13d, %esi
	movq	%rax, %rdi
	call	RC4_set_key@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE447:
	.size	rc4_init_key, .-rc4_init_key
	.p2align 4
	.globl	EVP_rc4
	.type	EVP_rc4, @function
EVP_rc4:
.LFB445:
	.cfi_startproc
	endbr64
	leaq	r4_cipher(%rip), %rax
	ret
	.cfi_endproc
.LFE445:
	.size	EVP_rc4, .-EVP_rc4
	.p2align 4
	.globl	EVP_rc4_40
	.type	EVP_rc4_40, @function
EVP_rc4_40:
.LFB446:
	.cfi_startproc
	endbr64
	leaq	r4_40_cipher(%rip), %rax
	ret
	.cfi_endproc
.LFE446:
	.size	EVP_rc4_40, .-EVP_rc4_40
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	r4_40_cipher, @object
	.size	r4_40_cipher, 88
r4_40_cipher:
	.long	97
	.long	1
	.long	5
	.long	0
	.quad	8
	.quad	rc4_init_key
	.quad	rc4_cipher
	.quad	0
	.long	1032
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	r4_cipher, @object
	.size	r4_cipher, 88
r4_cipher:
	.long	5
	.long	1
	.long	16
	.long	0
	.quad	8
	.quad	rc4_init_key
	.quad	rc4_cipher
	.quad	0
	.long	1032
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
