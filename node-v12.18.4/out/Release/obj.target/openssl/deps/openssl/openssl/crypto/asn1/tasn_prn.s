	.file	"tasn_prn.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	" (%s)"
.LC1:
	.string	": "
	.text
	.p2align 4
	.type	asn1_print_fsname.isra.0, @function
asn1_print_fsname.isra.0:
.LFB1413:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$24, %rsp
	movq	%r8, -56(%rbp)
	cmpl	$20, %esi
	jle	.L2
	leaq	spaces.21071(%rip), %r12
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L3:
	subl	$20, %ebx
	cmpl	$20, %ebx
	jle	.L2
.L5:
	movl	$20, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	BIO_write@PLT
	cmpl	$20, %eax
	je	.L3
.L6:
	xorl	%eax, %eax
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movl	%ebx, %edx
	leaq	spaces.21071(%rip), %rsi
	movq	%r14, %rdi
	call	BIO_write@PLT
	cmpl	%ebx, %eax
	jne	.L6
	movq	-56(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$64, %edx
	testb	$1, %ah
	je	.L26
	movl	$1, %eax
	testq	%rdx, %rdx
	jne	.L1
	testq	%r15, %r15
	je	.L1
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L6
.L10:
	movq	%r14, %rdi
	movl	$2, %edx
	leaq	.LC1(%rip), %rsi
	call	BIO_write@PLT
	cmpl	$2, %eax
	sete	%al
	addq	$24, %rsp
	popq	%rbx
	movzbl	%al, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	testq	%rdx, %rdx
	je	.L8
	testq	%r13, %r13
	je	.L27
.L9:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jg	.L10
	jmp	.L6
.L8:
	movq	%r13, %rcx
	movl	$1, %eax
	orq	%r15, %rcx
	je	.L1
	testq	%r15, %r15
	je	.L11
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L6
	testq	%r13, %r13
	je	.L10
	xorl	%eax, %eax
	movq	%r13, %rdx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jg	.L10
	jmp	.L6
.L11:
	testq	%r13, %r13
	je	.L10
	jmp	.L9
.L27:
	movl	$1, %eax
	jmp	.L1
	.cfi_endproc
.LFE1413:
	.size	asn1_print_fsname.isra.0, .-asn1_print_fsname.isra.0
	.section	.rodata.str1.1
.LC2:
	.string	""
.LC3:
	.string	"TRUE"
.LC4:
	.string	"FALSE"
.LC5:
	.string	"NULL\n"
.LC6:
	.string	":"
.LC7:
	.string	"BOOL ABSENT"
.LC8:
	.string	"%s (%s)"
.LC9:
	.string	" (%ld unused bits)\n"
.LC10:
	.string	"\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"../deps/openssl/openssl/crypto/asn1/tasn_prn.c"
	.text
	.p2align 4
	.type	asn1_primitive_print, @function
asn1_primitive_print:
.LFB1412:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	movq	%r9, %rcx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movq	%r8, %rdx
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %rbx
	movq	%rsi, -152(%rbp)
	movl	%r14d, %esi
	movq	32(%r12), %r15
	movq	%rbx, %r8
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	asn1_print_fsname.isra.0
	testl	%eax, %eax
	je	.L101
	testq	%r15, %r15
	je	.L31
	movq	56(%r15), %rax
	testq	%rax, %rax
	je	.L31
	movq	-152(%rbp), %rsi
	movq	%rbx, %r8
	movl	%r14d, %ecx
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	*%rax
	jmp	.L28
.L108:
	movl	$402, %edx
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L101:
	xorl	%eax, %eax
.L28:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L102
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	cmpb	$5, (%r12)
	movq	(%rbx), %rax
	je	.L103
	movq	8(%r12), %rdx
	cmpq	$1, %rdx
	je	.L34
	movq	-152(%rbp), %rdi
	movq	(%rdi), %r8
	cmpq	$-4, %rdx
	jne	.L33
	movslq	(%r8), %rdx
	leaq	8(%r8), %rsi
	movq	8(%r8), %r8
	movq	%rsi, -152(%rbp)
	movq	%rdx, %rdi
	testb	$16, %al
	je	.L35
.L98:
	cmpq	$5, %rdx
	je	.L41
.L36:
	addq	$3, %rdx
	cmpq	$27, %rdx
	ja	.L44
	leaq	.L46(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L46:
	.long	.L48-.L46
	.long	.L44-.L46
	.long	.L44-.L46
	.long	.L44-.L46
	.long	.L39-.L46
	.long	.L49-.L46
	.long	.L51-.L46
	.long	.L51-.L46
	.long	.L44-.L46
	.long	.L50-.L46
	.long	.L44-.L46
	.long	.L44-.L46
	.long	.L44-.L46
	.long	.L49-.L46
	.long	.L44-.L46
	.long	.L44-.L46
	.long	.L44-.L46
	.long	.L44-.L46
	.long	.L44-.L46
	.long	.L48-.L46
	.long	.L48-.L46
	.long	.L44-.L46
	.long	.L44-.L46
	.long	.L44-.L46
	.long	.L44-.L46
	.long	.L44-.L46
	.long	.L47-.L46
	.long	.L45-.L46
	.text
	.p2align 4,,10
	.p2align 3
.L34:
	testb	$8, %al
	jne	.L104
.L39:
	movq	-152(%rbp), %rax
	movl	(%rax), %eax
	cmpl	$-1, %eax
	je	.L105
.L52:
	testl	%eax, %eax
	leaq	.LC4(%rip), %rsi
	leaq	.LC3(%rip), %rax
	movq	%r13, %rdi
	cmovne	%rax, %rsi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jg	.L56
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L103:
	movq	-152(%rbp), %rsi
	movq	(%rsi), %r8
	movl	4(%r8), %edx
	andb	$-2, %dh
	movslq	%edx, %rdx
.L33:
	testb	$8, %al
	je	.L98
	movq	%r8, -168(%rbp)
	movl	%edx, %edi
.L99:
	movq	%rdx, -160(%rbp)
	call	ASN1_tag2str@PLT
	movq	-160(%rbp), %rdx
	movq	-168(%rbp), %r8
	movq	%rax, %rsi
	cmpq	$5, %rdx
	je	.L41
	testq	%rax, %rax
	je	.L36
.L42:
	movq	%r13, %rdi
	movq	%r8, -168(%rbp)
	movq	%rdx, -160(%rbp)
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L101
	leaq	.LC6(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_puts@PLT
	movq	-160(%rbp), %rdx
	movq	-168(%rbp), %r8
	testl	%eax, %eax
	jg	.L36
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L41:
	leaq	.LC5(%rip), %rsi
.L100:
	movq	%r13, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L44:
	movq	32(%rbx), %rdx
	movq	%r8, %rsi
	movq	%r13, %rdi
	call	ASN1_STRING_print_ex@PLT
	testl	%eax, %eax
	je	.L101
	.p2align 4,,10
	.p2align 3
.L56:
	leaq	.LC10(%rip), %rsi
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L48:
	leaq	.LC10(%rip), %rsi
	movq	%r13, %rdi
	movq	%r8, -152(%rbp)
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L101
	movq	-152(%rbp), %r8
	movl	%r14d, %ecx
	movq	%r13, %rdi
	movslq	(%r8), %rdx
	movq	8(%r8), %rsi
	xorl	%r8d, %r8d
	call	ASN1_parse_dump@PLT
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L51:
	cmpl	$3, 4(%r8)
	je	.L106
	leaq	.LC10(%rip), %rsi
	movq	%r13, %rdi
	movq	%r8, -152(%rbp)
	call	BIO_puts@PLT
	movq	-152(%rbp), %r8
	testl	%eax, %eax
	jle	.L101
.L64:
	movl	(%r8), %edx
	testl	%edx, %edx
	jle	.L107
	movq	8(%r8), %rsi
	leal	2(%r14), %ecx
	movq	%r13, %rdi
	call	BIO_dump_indent@PLT
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L49:
	movq	%r8, %rsi
	xorl	%edi, %edi
	call	i2s_ASN1_INTEGER@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L101
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L108
	movl	$402, %edx
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	jmp	.L56
.L105:
	movq	40(%r12), %rdx
	movl	%edx, %eax
	cmpl	$-1, %edx
	jne	.L52
	leaq	.LC7(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jg	.L56
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L47:
	movq	%r8, %rsi
	movq	%r13, %rdi
	call	ASN1_UTCTIME_print@PLT
	testl	%eax, %eax
	jne	.L56
	jmp	.L101
.L50:
	movq	-152(%rbp), %rax
	leaq	-144(%rbp), %r14
	movq	(%rax), %r15
	movq	%r15, %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2ln@PLT
	movl	$1, %ecx
	movq	%r15, %rdx
	movq	%r14, %rdi
	testq	%rax, %rax
	movq	%rax, %r12
	leaq	.LC2(%rip), %rax
	movl	$80, %esi
	cmove	%rax, %r12
	call	OBJ_obj2txt@PLT
	movq	%r14, %rcx
	movq	%r13, %rdi
	xorl	%eax, %eax
	movq	%r12, %rdx
	leaq	.LC8(%rip), %rsi
	call	BIO_printf@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L56
	jmp	.L28
.L45:
	movq	%r8, %rsi
	movq	%r13, %rdi
	call	ASN1_GENERALIZEDTIME_print@PLT
	testl	%eax, %eax
	jne	.L56
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L35:
	movq	%r8, -168(%rbp)
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L104:
	movl	$1, %edi
	movq	%rdx, -160(%rbp)
	call	ASN1_tag2str@PLT
	xorl	%r8d, %r8d
	movq	-160(%rbp), %rdx
	testq	%rax, %rax
	movq	%rax, %rsi
	jne	.L42
	jmp	.L39
.L106:
	movq	16(%r8), %rdx
	xorl	%eax, %eax
	leaq	.LC9(%rip), %rsi
	movq	%r13, %rdi
	movq	%r8, -152(%rbp)
	andl	$7, %edx
	call	BIO_printf@PLT
	movq	-152(%rbp), %r8
	testl	%eax, %eax
	jg	.L64
	jmp	.L101
.L107:
	movl	$1, %eax
	jmp	.L28
.L102:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1412:
	.size	asn1_primitive_print, .-asn1_primitive_print
	.section	.rodata.str1.1
.LC12:
	.string	"SET"
.LC13:
	.string	"SEQUENCE"
.LC14:
	.string	"ABSENT"
.LC15:
	.string	"EMPTY"
.LC16:
	.string	"%*s%s OF %s {\n"
.LC17:
	.string	"%*s%s:\n"
.LC18:
	.string	"<ABSENT>\n"
.LC19:
	.string	"ERROR: selector [%d] invalid\n"
.LC20:
	.string	"%*s}\n"
.LC21:
	.string	"Unprocessed type %d\n"
.LC22:
	.string	"%*s<%s>\n"
.LC23:
	.string	":EXTERNAL TYPE %s\n"
.LC24:
	.string	" {\n"
	.text
	.p2align 4
	.type	asn1_template_print_ctx, @function
asn1_template_print_ctx:
.LFB1406:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$136, %rsp
	movl	%edx, -148(%rbp)
	movq	(%r8), %rdx
	movq	%rcx, -112(%rbp)
	movq	%r8, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rcx), %rax
	testb	$-128, %dl
	je	.L110
	movq	32(%rcx), %rcx
	movq	48(%rcx), %rbx
.L110:
	xorl	%r14d, %r14d
	testb	$64, %dl
	jne	.L111
	movq	-112(%rbp), %rdi
	movq	24(%rdi), %r14
.L111:
	testb	$16, %ah
	je	.L112
	movq	%r13, -96(%rbp)
	leaq	-96(%rbp), %r13
.L112:
	movl	%eax, %r15d
	andl	$6, %r15d
	je	.L113
	testq	%r14, %r14
	je	.L118
	andl	$4, %edx
	je	.L116
	testb	$2, %al
	movq	-112(%rbp), %rax
	leaq	.LC12(%rip), %r8
	movl	-148(%rbp), %edx
	leaq	.LC2(%rip), %rcx
	leaq	.LC16(%rip), %rsi
	movq	%r12, %rdi
	movq	24(%rax), %r9
	leaq	.LC13(%rip), %rax
	cmove	%rax, %r8
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L289
.L118:
	movl	-148(%rbp), %eax
	movq	0(%r13), %r15
	xorl	%ebx, %ebx
	leal	2(%rax), %edx
	addl	$4, %eax
	movl	%edx, -136(%rbp)
	movl	%eax, -152(%rbp)
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L296:
	cmpq	$1, 8(%r14)
	je	.L127
	testq	%rax, %rax
	jne	.L127
.L180:
	movq	-104(%rbp), %rax
	testb	$1, (%rax)
	je	.L131
	leaq	.LC18(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L289
	.p2align 4,,10
	.p2align 3
.L131:
	addl	$1, %ebx
.L115:
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L294
	testl	%ebx, %ebx
	je	.L124
	leaq	.LC10(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L289
.L124:
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	movq	-112(%rbp), %rcx
	movq	%rax, -88(%rbp)
	movq	32(%rcx), %r14
	movq	32(%r14), %rcx
	testq	%rcx, %rcx
	je	.L295
	movq	24(%rcx), %rsi
	movq	%rsi, -128(%rbp)
	testq	%rsi, %rsi
	je	.L123
	movl	-136(%rbp), %edx
	movq	-104(%rbp), %rdi
	movq	%r12, -80(%rbp)
	movl	%edx, -72(%rbp)
	movq	%rdi, -64(%rbp)
.L123:
	movzbl	(%r14), %edx
	testb	%dl, %dl
	je	.L296
	testq	%rax, %rax
	je	.L180
	cmpb	$6, %dl
	ja	.L132
	leaq	.L134(%rip), %rdi
	movzbl	%dl, %eax
	movslq	(%rdi,%rax,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L134:
	.long	.L132-.L134
	.long	.L133-.L134
	.long	.L137-.L134
	.long	.L132-.L134
	.long	.L136-.L134
	.long	.L135-.L134
	.long	.L133-.L134
	.text
	.p2align 4,,10
	.p2align 3
.L127:
	movq	16(%r14), %rcx
	testq	%rcx, %rcx
	jne	.L297
.L135:
	subq	$8, %rsp
	pushq	-104(%rbp)
	movl	-136(%rbp), %ecx
	movq	%r12, %rdi
	leaq	-88(%rbp), %r13
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	asn1_primitive_print
	popq	%rsi
	popq	%rdi
	testl	%eax, %eax
	jne	.L131
	jmp	.L289
.L132:
	movsbl	%dl, %edx
	leaq	.LC21(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L289:
	xorl	%r15d, %r15d
.L109:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L298
	leaq	-40(%rbp), %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L136:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L131
	movq	48(%rcx), %rax
	testq	%rax, %rax
	je	.L131
	leaq	-88(%rbp), %r13
	movq	-104(%rbp), %r8
	movl	-136(%rbp), %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rcx
	movq	%r13, %rsi
	call	*%rax
	testl	%eax, %eax
	je	.L289
	cmpl	$2, %eax
	jne	.L131
	leaq	.LC10(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jg	.L131
	jmp	.L289
.L137:
	leaq	-88(%rbp), %r13
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	asn1_get_choice_selector@PLT
	movl	%eax, %edx
	testl	%eax, %eax
	js	.L138
	cltq
	cmpq	24(%r14), %rax
	jl	.L139
.L138:
	xorl	%eax, %eax
	leaq	.LC19(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jg	.L131
	jmp	.L289
.L133:
	movq	-128(%rbp), %rax
	testq	%rax, %rax
	je	.L140
	leaq	-88(%rbp), %r13
	leaq	-80(%rbp), %rcx
	movq	%r14, %rdx
	movl	$8, %edi
	movq	%r13, %rsi
	call	*%rax
	testl	%eax, %eax
	je	.L289
	cmpl	$2, %eax
	je	.L131
.L140:
	cmpq	$0, 24(%r14)
	movq	16(%r14), %rax
	jle	.L141
	movq	%r15, -168(%rbp)
	xorl	%edx, %edx
	leaq	-88(%rbp), %r13
	movl	-152(%rbp), %r15d
	movq	%r14, -144(%rbp)
	movq	%rdx, %r14
	movl	%ebx, -156(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L143:
	movl	$1, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	asn1_do_adb@PLT
	testq	%rax, %rax
	je	.L289
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	%rax, -120(%rbp)
	call	asn1_get_field_ptr@PLT
	movq	-104(%rbp), %r8
	movq	-120(%rbp), %rcx
	movl	%r15d, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	asn1_template_print_ctx
	testl	%eax, %eax
	je	.L289
	movq	-144(%rbp), %rax
	addq	$40, %rbx
	addq	$1, %r14
	cmpq	%r14, 24(%rax)
	jg	.L143
	movl	-156(%rbp), %ebx
	movq	-168(%rbp), %r15
	movq	%rax, %r14
.L141:
	movq	-104(%rbp), %rax
	testb	$2, (%rax)
	jne	.L299
.L144:
	movq	-128(%rbp), %rax
	testq	%rax, %rax
	je	.L131
	leaq	-88(%rbp), %r13
	leaq	-80(%rbp), %rcx
	movq	%r14, %rdx
	movl	$9, %edi
	movq	%r13, %rsi
	call	*%rax
	testl	%eax, %eax
	jne	.L131
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L295:
	movq	$0, -128(%rbp)
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L297:
	leaq	-88(%rbp), %r13
	movq	-104(%rbp), %r8
	movl	-136(%rbp), %edx
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	asn1_template_print_ctx
	testl	%eax, %eax
	jne	.L131
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L113:
	movq	-112(%rbp), %rax
	xorl	%r11d, %r11d
	movq	32(%rax), %r10
	movq	32(%r10), %rax
	testq	%rax, %rax
	je	.L151
	movq	24(%rax), %r11
	testq	%r11, %r11
	je	.L151
	movl	-148(%rbp), %eax
	movq	%r12, -80(%rbp)
	movl	%eax, -72(%rbp)
	movq	-104(%rbp), %rax
	movq	%rax, -64(%rbp)
.L151:
	movzbl	(%r10), %ecx
	testb	%cl, %cl
	jne	.L152
	cmpq	$1, 8(%r10)
	je	.L154
	cmpq	$0, 0(%r13)
	jne	.L154
.L181:
	andl	$1, %edx
	je	.L167
	movl	-148(%rbp), %esi
	movq	-104(%rbp), %r8
	movq	%rbx, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	asn1_print_fsname.isra.0
	leaq	.LC18(%rip), %rsi
	testl	%eax, %eax
	jne	.L290
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L294:
	testl	%ebx, %ebx
	je	.L146
.L149:
	movq	-104(%rbp), %rax
	testb	$2, (%rax)
	je	.L167
	movl	-148(%rbp), %edx
	xorl	%eax, %eax
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	leaq	.LC2(%rip), %rcx
	leaq	.LC20(%rip), %rsi
	call	BIO_printf@PLT
	testl	%eax, %eax
	setg	%r15b
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L139:
	leaq	(%rax,%rax,4), %rdx
	movq	16(%r14), %rax
	movq	%r13, %rdi
	leaq	(%rax,%rdx,8), %r14
	movq	%r14, %rsi
	call	asn1_get_field_ptr@PLT
	movq	-104(%rbp), %r8
	movq	%r14, %rcx
	movq	%r12, %rdi
	movl	-136(%rbp), %edx
	movq	%rax, %rsi
	call	asn1_template_print_ctx
	testl	%eax, %eax
	jne	.L131
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L152:
	cmpq	$0, 0(%r13)
	je	.L181
	cmpb	$6, %cl
	ja	.L159
	leaq	.L161(%rip), %rsi
	movzbl	%cl, %edx
	movslq	(%rsi,%rdx,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L161:
	.long	.L159-.L161
	.long	.L160-.L161
	.long	.L164-.L161
	.long	.L159-.L161
	.long	.L163-.L161
	.long	.L162-.L161
	.long	.L160-.L161
	.text
.L162:
	subq	$8, %rsp
	pushq	-104(%rbp)
	movl	-148(%rbp), %ecx
	movq	%r10, %rdx
	movq	%rbx, %r9
	movq	%r14, %r8
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	call	asn1_primitive_print
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	setne	%r15b
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L146:
	testq	%r15, %r15
	leaq	.LC15(%rip), %rax
	leaq	.LC14(%rip), %r8
	movq	%r12, %rdi
	cmovne	%rax, %r8
	movl	-148(%rbp), %eax
	leaq	.LC2(%rip), %rcx
	leaq	.LC22(%rip), %rsi
	leal	2(%rax), %edx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jg	.L149
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L116:
	movl	-148(%rbp), %edx
	xorl	%eax, %eax
	movq	%r14, %r8
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rcx
	leaq	.LC17(%rip), %rsi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jg	.L118
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L167:
	movl	$1, %r15d
	jmp	.L109
.L163:
	movq	%r14, %rdx
	movl	-148(%rbp), %r14d
	movq	%rbx, %rcx
	movq	%r12, %rdi
	movq	-104(%rbp), %r8
	movq	%r10, -112(%rbp)
	movl	%r14d, %esi
	call	asn1_print_fsname.isra.0
	testl	%eax, %eax
	je	.L109
	movq	-112(%rbp), %r10
	movq	32(%r10), %rax
	testq	%rax, %rax
	je	.L166
	movq	48(%rax), %rax
	testq	%rax, %rax
	je	.L166
	movq	-104(%rbp), %r8
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rcx
	call	*%rax
	testl	%eax, %eax
	je	.L109
	cmpl	$2, %eax
	jne	.L167
	leaq	.LC10(%rip), %rsi
.L290:
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	call	BIO_puts@PLT
	testl	%eax, %eax
	setg	%r15b
	jmp	.L109
.L164:
	movq	%r10, %rsi
	movq	%r13, %rdi
	movq	%r10, -112(%rbp)
	call	asn1_get_choice_selector@PLT
	movl	%eax, %edx
	testl	%eax, %eax
	js	.L168
	movq	-112(%rbp), %r10
	cltq
	cmpq	24(%r10), %rax
	jl	.L169
.L168:
	xorl	%eax, %eax
	leaq	.LC19(%rip), %rsi
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	call	BIO_printf@PLT
	testl	%eax, %eax
	setg	%r15b
	jmp	.L109
.L160:
	movq	-104(%rbp), %r8
	movl	-148(%rbp), %esi
	movq	%rbx, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%r11, -120(%rbp)
	movq	%r10, -112(%rbp)
	call	asn1_print_fsname.isra.0
	testl	%eax, %eax
	je	.L109
	orq	%r14, %rbx
	movq	-112(%rbp), %r10
	movq	-120(%rbp), %r11
	je	.L174
	movq	-104(%rbp), %rax
	movq	%r11, -120(%rbp)
	leaq	.LC24(%rip), %rsi
	movq	%r10, -112(%rbp)
	testb	$2, (%rax)
	jne	.L293
	leaq	.LC10(%rip), %rsi
.L293:
	movq	%r12, %rdi
	call	BIO_puts@PLT
	movq	-112(%rbp), %r10
	movq	-120(%rbp), %r11
	testl	%eax, %eax
	jle	.L109
.L174:
	testq	%r11, %r11
	je	.L172
	movq	%r10, -120(%rbp)
	leaq	-80(%rbp), %rcx
	movq	%r10, %rdx
	movq	%r13, %rsi
	movq	%r11, -112(%rbp)
	movl	$8, %edi
	call	*%r11
	testl	%eax, %eax
	je	.L109
	cmpl	$2, %eax
	movq	-112(%rbp), %r11
	movq	-120(%rbp), %r10
	je	.L167
.L172:
	movl	-148(%rbp), %eax
	xorl	%r14d, %r14d
	movq	16(%r10), %rbx
	addl	$2, %eax
	cmpq	$0, 24(%r10)
	jle	.L176
	movl	%r15d, -128(%rbp)
	movl	%eax, %r15d
	movq	%r10, -120(%rbp)
	movq	%r11, -136(%rbp)
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L301:
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	%rax, -112(%rbp)
	call	asn1_get_field_ptr@PLT
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %rcx
	movl	%r15d, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	asn1_template_print_ctx
	testl	%eax, %eax
	je	.L286
	movq	-120(%rbp), %rax
	addq	$40, %rbx
	addq	$1, %r14
	cmpq	%r14, 24(%rax)
	jle	.L300
.L175:
	movl	$1, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	asn1_do_adb@PLT
	testq	%rax, %rax
	jne	.L301
.L286:
	movl	-128(%rbp), %r15d
	jmp	.L109
.L159:
	movsbl	%cl, %edx
	leaq	.LC21(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L109
.L299:
	movl	-136(%rbp), %edx
	xorl	%eax, %eax
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rcx
	leaq	.LC20(%rip), %rsi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jns	.L144
	jmp	.L289
.L154:
	movq	16(%r10), %rcx
	testq	%rcx, %rcx
	je	.L162
	movq	-104(%rbp), %r8
	movl	-148(%rbp), %edx
	movq	%r13, %rsi
.L291:
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	call	asn1_template_print_ctx
	testl	%eax, %eax
	setne	%r15b
	jmp	.L109
.L166:
	testq	%rbx, %rbx
	je	.L167
	xorl	%eax, %eax
	movq	%rbx, %rdx
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	xorl	%r15d, %r15d
	testl	%eax, %eax
	setg	%r15b
	jmp	.L109
.L169:
	leaq	(%rax,%rax,4), %rdx
	movq	16(%r10), %rax
	movq	%r13, %rdi
	leaq	(%rax,%rdx,8), %r14
	movq	%r14, %rsi
	call	asn1_get_field_ptr@PLT
	movq	-104(%rbp), %r8
	movl	-148(%rbp), %edx
	movq	%r14, %rcx
	movq	%rax, %rsi
	jmp	.L291
.L300:
	movq	-136(%rbp), %r11
	movl	-128(%rbp), %r15d
	movq	%rax, %r10
.L176:
	movq	-104(%rbp), %rax
	testb	$2, (%rax)
	jne	.L177
.L179:
	testq	%r11, %r11
	je	.L167
	leaq	-80(%rbp), %rcx
	movq	%r10, %rdx
	movq	%r13, %rsi
	movl	$9, %edi
	call	*%r11
	xorl	%r15d, %r15d
	testl	%eax, %eax
	setne	%r15b
	jmp	.L109
.L177:
	movl	-148(%rbp), %edx
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%r11, -112(%rbp)
	leaq	.LC2(%rip), %rcx
	leaq	.LC20(%rip), %rsi
	movq	%r10, -104(%rbp)
	call	BIO_printf@PLT
	movq	-104(%rbp), %r10
	movq	-112(%rbp), %r11
	testl	%eax, %eax
	jns	.L179
	jmp	.L109
.L298:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1406:
	.size	asn1_template_print_ctx, .-asn1_template_print_ctx
	.p2align 4
	.globl	ASN1_PCTX_new
	.type	ASN1_PCTX_new, @function
ASN1_PCTX_new:
.LFB1392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$39, %edx
	movl	$40, %edi
	leaq	.LC11(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L305
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L305:
	.cfi_restore_state
	movl	$41, %r8d
	leaq	.LC11(%rip), %rcx
	movl	$65, %edx
	movl	$205, %esi
	movl	$13, %edi
	movq	%rax, -8(%rbp)
	call	ERR_put_error@PLT
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1392:
	.size	ASN1_PCTX_new, .-ASN1_PCTX_new
	.p2align 4
	.globl	ASN1_PCTX_free
	.type	ASN1_PCTX_free, @function
ASN1_PCTX_free:
.LFB1393:
	.cfi_startproc
	endbr64
	movl	$49, %edx
	leaq	.LC11(%rip), %rsi
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE1393:
	.size	ASN1_PCTX_free, .-ASN1_PCTX_free
	.p2align 4
	.globl	ASN1_PCTX_get_flags
	.type	ASN1_PCTX_get_flags, @function
ASN1_PCTX_get_flags:
.LFB1394:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE1394:
	.size	ASN1_PCTX_get_flags, .-ASN1_PCTX_get_flags
	.p2align 4
	.globl	ASN1_PCTX_set_flags
	.type	ASN1_PCTX_set_flags, @function
ASN1_PCTX_set_flags:
.LFB1395:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE1395:
	.size	ASN1_PCTX_set_flags, .-ASN1_PCTX_set_flags
	.p2align 4
	.globl	ASN1_PCTX_get_nm_flags
	.type	ASN1_PCTX_get_nm_flags, @function
ASN1_PCTX_get_nm_flags:
.LFB1396:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE1396:
	.size	ASN1_PCTX_get_nm_flags, .-ASN1_PCTX_get_nm_flags
	.p2align 4
	.globl	ASN1_PCTX_set_nm_flags
	.type	ASN1_PCTX_set_nm_flags, @function
ASN1_PCTX_set_nm_flags:
.LFB1397:
	.cfi_startproc
	endbr64
	movq	%rsi, 8(%rdi)
	ret
	.cfi_endproc
.LFE1397:
	.size	ASN1_PCTX_set_nm_flags, .-ASN1_PCTX_set_nm_flags
	.p2align 4
	.globl	ASN1_PCTX_get_cert_flags
	.type	ASN1_PCTX_get_cert_flags, @function
ASN1_PCTX_get_cert_flags:
.LFB1398:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE1398:
	.size	ASN1_PCTX_get_cert_flags, .-ASN1_PCTX_get_cert_flags
	.p2align 4
	.globl	ASN1_PCTX_set_cert_flags
	.type	ASN1_PCTX_set_cert_flags, @function
ASN1_PCTX_set_cert_flags:
.LFB1399:
	.cfi_startproc
	endbr64
	movq	%rsi, 16(%rdi)
	ret
	.cfi_endproc
.LFE1399:
	.size	ASN1_PCTX_set_cert_flags, .-ASN1_PCTX_set_cert_flags
	.p2align 4
	.globl	ASN1_PCTX_get_oid_flags
	.type	ASN1_PCTX_get_oid_flags, @function
ASN1_PCTX_get_oid_flags:
.LFB1400:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE1400:
	.size	ASN1_PCTX_get_oid_flags, .-ASN1_PCTX_get_oid_flags
	.p2align 4
	.globl	ASN1_PCTX_set_oid_flags
	.type	ASN1_PCTX_set_oid_flags, @function
ASN1_PCTX_set_oid_flags:
.LFB1401:
	.cfi_startproc
	endbr64
	movq	%rsi, 24(%rdi)
	ret
	.cfi_endproc
.LFE1401:
	.size	ASN1_PCTX_set_oid_flags, .-ASN1_PCTX_set_oid_flags
	.p2align 4
	.globl	ASN1_PCTX_get_str_flags
	.type	ASN1_PCTX_get_str_flags, @function
ASN1_PCTX_get_str_flags:
.LFB1402:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE1402:
	.size	ASN1_PCTX_get_str_flags, .-ASN1_PCTX_get_str_flags
	.p2align 4
	.globl	ASN1_PCTX_set_str_flags
	.type	ASN1_PCTX_set_str_flags, @function
ASN1_PCTX_set_str_flags:
.LFB1403:
	.cfi_startproc
	endbr64
	movq	%rsi, 32(%rdi)
	ret
	.cfi_endproc
.LFE1403:
	.size	ASN1_PCTX_set_str_flags, .-ASN1_PCTX_set_str_flags
	.p2align 4
	.globl	ASN1_item_print
	.type	ASN1_item_print, @function
ASN1_item_print:
.LFB1404:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$88, %rsp
	movq	%rsi, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r8, %r8
	leaq	default_pctx(%rip), %rax
	cmove	%rax, %rbx
	xorl	%r14d, %r14d
	movq	(%rbx), %rcx
	testb	$1, %ch
	jne	.L319
	movq	48(%r12), %r14
.L319:
	movq	32(%r12), %rax
	xorl	%r11d, %r11d
	testq	%rax, %rax
	je	.L320
	movq	24(%rax), %r11
	testq	%r11, %r11
	je	.L320
	movq	%r13, -80(%rbp)
	movl	%r15d, -72(%rbp)
	movq	%rbx, -64(%rbp)
.L320:
	movzbl	(%r12), %edx
	testb	%dl, %dl
	jne	.L321
	cmpq	$1, 8(%r12)
	je	.L323
	cmpq	$0, -88(%rbp)
	jne	.L323
.L349:
	andl	$1, %ecx
	je	.L336
	movl	%r15d, %esi
	movq	%rbx, %r8
	movq	%r14, %rcx
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	asn1_print_fsname.isra.0
	leaq	.LC18(%rip), %rsi
	testl	%eax, %eax
	je	.L397
.L398:
	movq	%r13, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L321:
	cmpq	$0, -88(%rbp)
	je	.L349
	cmpb	$6, %dl
	ja	.L328
	leaq	.L330(%rip), %rsi
	movzbl	%dl, %ecx
	movslq	(%rsi,%rcx,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L330:
	.long	.L328-.L330
	.long	.L329-.L330
	.long	.L333-.L330
	.long	.L328-.L330
	.long	.L332-.L330
	.long	.L331-.L330
	.long	.L329-.L330
	.text
.L331:
	subq	$8, %rsp
	xorl	%r8d, %r8d
	movl	%r15d, %ecx
	movq	%r12, %rdx
	pushq	%rbx
	leaq	-88(%rbp), %rsi
	movq	%r14, %r9
	movq	%r13, %rdi
	call	asn1_primitive_print
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	.p2align 4,,10
	.p2align 3
.L317:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L400
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L329:
	.cfi_restore_state
	xorl	%edx, %edx
	movq	%rbx, %r8
	movq	%r14, %rcx
	movl	%r15d, %esi
	movq	%r13, %rdi
	movq	%r11, -96(%rbp)
	call	asn1_print_fsname.isra.0
	testl	%eax, %eax
	je	.L397
	testq	%r14, %r14
	movq	-96(%rbp), %r11
	je	.L343
	testb	$2, (%rbx)
	movq	%r11, -96(%rbp)
	leaq	.LC24(%rip), %rsi
	jne	.L396
	leaq	.LC10(%rip), %rsi
.L396:
	movq	%r13, %rdi
	call	BIO_puts@PLT
	movq	-96(%rbp), %r11
	testl	%eax, %eax
	jle	.L397
.L343:
	testq	%r11, %r11
	je	.L341
	leaq	-88(%rbp), %r14
	movq	%r11, -96(%rbp)
	leaq	-80(%rbp), %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movl	$8, %edi
	call	*%r11
	testl	%eax, %eax
	je	.L397
	cmpl	$2, %eax
	movq	-96(%rbp), %r11
	je	.L336
.L341:
	movq	16(%r12), %rax
	cmpq	$0, 24(%r12)
	movq	$0, -96(%rbp)
	leaq	-88(%rbp), %r14
	movq	%rax, -104(%rbp)
	leal	2(%r15), %eax
	movl	%eax, -124(%rbp)
	jg	.L344
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L401:
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	%rax, -120(%rbp)
	call	asn1_get_field_ptr@PLT
	movq	-120(%rbp), %rcx
	movl	-124(%rbp), %edx
	movq	%rbx, %r8
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	asn1_template_print_ctx
	testl	%eax, %eax
	je	.L397
	addq	$1, -96(%rbp)
	movq	-96(%rbp), %rax
	addq	$40, -104(%rbp)
	cmpq	24(%r12), %rax
	movq	-112(%rbp), %r11
	jge	.L345
.L344:
	movq	-104(%rbp), %rsi
	movl	$1, %edx
	movq	%r14, %rdi
	movq	%r11, -112(%rbp)
	call	asn1_do_adb@PLT
	testq	%rax, %rax
	jne	.L401
	.p2align 4,,10
	.p2align 3
.L397:
	xorl	%eax, %eax
	jmp	.L317
.L333:
	leaq	-88(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	asn1_get_choice_selector@PLT
	movl	%eax, %edx
	testl	%eax, %eax
	js	.L337
	cltq
	cmpq	24(%r12), %rax
	jl	.L338
.L337:
	xorl	%eax, %eax
	leaq	.LC19(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
	jmp	.L317
.L332:
	xorl	%edx, %edx
	movq	%rbx, %r8
	movq	%r14, %rcx
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	asn1_print_fsname.isra.0
	testl	%eax, %eax
	je	.L397
	movq	32(%r12), %rax
	testq	%rax, %rax
	je	.L335
	movq	48(%rax), %rax
	testq	%rax, %rax
	je	.L335
	leaq	-88(%rbp), %rsi
	movq	%rbx, %r8
	movl	%r15d, %edx
	movq	%r13, %rdi
	leaq	.LC2(%rip), %rcx
	call	*%rax
	testl	%eax, %eax
	je	.L397
	cmpl	$2, %eax
	jne	.L336
	leaq	.LC10(%rip), %rsi
	jmp	.L398
.L328:
	movsbl	%dl, %edx
	leaq	.LC21(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	xorl	%eax, %eax
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L323:
	movq	16(%r12), %rcx
	testq	%rcx, %rcx
	je	.L331
	leaq	-88(%rbp), %rsi
	movq	%rbx, %r8
.L399:
	movl	%r15d, %edx
	movq	%r13, %rdi
	call	asn1_template_print_ctx
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L336:
	movl	$1, %eax
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L335:
	testq	%r14, %r14
	je	.L336
	xorl	%eax, %eax
	movq	%r14, %rdx
	leaq	.LC23(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L338:
	leaq	(%rax,%rax,4), %rdx
	movq	16(%r12), %rax
	movq	%r14, %rdi
	leaq	(%rax,%rdx,8), %r12
	movq	%r12, %rsi
	call	asn1_get_field_ptr@PLT
	movq	%rbx, %r8
	movq	%r12, %rcx
	movq	%rax, %rsi
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L345:
	testb	$2, (%rbx)
	jne	.L346
.L348:
	testq	%r11, %r11
	je	.L336
	leaq	-80(%rbp), %rcx
	leaq	-88(%rbp), %rsi
	movq	%r12, %rdx
	movl	$9, %edi
	call	*%r11
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L317
.L346:
	xorl	%eax, %eax
	leaq	.LC2(%rip), %rcx
	movl	%r15d, %edx
	movq	%r13, %rdi
	leaq	.LC20(%rip), %rsi
	movq	%r11, -96(%rbp)
	call	BIO_printf@PLT
	movq	-96(%rbp), %r11
	testl	%eax, %eax
	jns	.L348
	xorl	%eax, %eax
	jmp	.L317
.L400:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1404:
	.size	ASN1_item_print, .-ASN1_item_print
	.section	.rodata
	.align 16
	.type	spaces.21071, @object
	.size	spaces.21071, 21
spaces.21071:
	.string	"                    "
	.data
	.align 32
	.type	default_pctx, @object
	.size	default_pctx, 40
default_pctx:
	.quad	1
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
