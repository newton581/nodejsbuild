	.file	"a_time.c"
	.text
	.p2align 4
	.globl	d2i_ASN1_TIME
	.type	d2i_ASN1_TIME, @function
d2i_ASN1_TIME:
.LFB491:
	.cfi_startproc
	endbr64
	leaq	ASN1_TIME_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE491:
	.size	d2i_ASN1_TIME, .-d2i_ASN1_TIME
	.p2align 4
	.globl	i2d_ASN1_TIME
	.type	i2d_ASN1_TIME, @function
i2d_ASN1_TIME:
.LFB492:
	.cfi_startproc
	endbr64
	leaq	ASN1_TIME_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE492:
	.size	i2d_ASN1_TIME, .-i2d_ASN1_TIME
	.p2align 4
	.globl	ASN1_TIME_new
	.type	ASN1_TIME_new, @function
ASN1_TIME_new:
.LFB493:
	.cfi_startproc
	endbr64
	leaq	ASN1_TIME_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE493:
	.size	ASN1_TIME_new, .-ASN1_TIME_new
	.p2align 4
	.globl	ASN1_TIME_free
	.type	ASN1_TIME_free, @function
ASN1_TIME_free:
.LFB494:
	.cfi_startproc
	endbr64
	leaq	ASN1_TIME_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE494:
	.size	ASN1_TIME_free, .-ASN1_TIME_free
	.p2align 4
	.globl	asn1_time_to_tm
	.type	asn1_time_to_tm, @function
asn1_time_to_tm:
.LFB498:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	4(%rsi), %eax
	cmpl	$23, %eax
	je	.L96
	cmpl	$24, %eax
	jne	.L11
	movq	16(%rsi), %rax
	movl	$7, -116(%rbp)
	movl	$6, -132(%rbp)
	andl	$256, %eax
	cmpq	$1, %rax
	sbbl	%edx, %edx
	addl	$1, %edx
	cmpq	$1, %rax
	sbbl	%eax, %eax
	movl	%edx, -136(%rbp)
	andl	$-2, %eax
	addl	$15, %eax
.L8:
	movq	8(%r13), %rbx
	movl	0(%r13), %r15d
	pxor	%xmm0, %xmm0
	movq	$0, -64(%rbp)
	movaps	%xmm0, -112(%rbp)
	movq	%rbx, -152(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	cmpl	%r15d, %eax
	jg	.L11
	movl	-136(%rbp), %eax
	movq	%r13, -128(%rbp)
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	xorl	$1, %eax
	andl	$1, %eax
	movb	%al, -120(%rbp)
	.p2align 4,,10
	.p2align 3
.L12:
	movsbl	(%rbx), %edi
	call	ascii_isdigit@PLT
	testl	%eax, %eax
	je	.L11
	movsbl	(%rbx), %edx
	leal	1(%r14), %eax
	leal	-48(%rdx), %r13d
	cmpl	%eax, %r15d
	je	.L11
	movsbl	1(%rbx), %edi
	call	ascii_isdigit@PLT
	testl	%eax, %eax
	je	.L11
	movsbl	1(%rbx), %eax
	leal	0(%r13,%r13,4), %edx
	leal	(%rax,%rdx,2), %ecx
	leal	2(%r14), %edx
	leal	-48(%rcx), %eax
	cmpl	%edx, %r15d
	je	.L11
	movq	-128(%rbp), %rdi
	xorl	%esi, %esi
	leaq	min.9051(%rip), %r10
	movl	4(%rdi), %r11d
	cmpl	$23, %r11d
	sete	%sil
	addl	%r12d, %esi
	movslq	%esi, %rdi
	cmpl	%eax, (%r10,%rdi,4)
	jg	.L11
	leaq	max.9052(%rip), %r9
	cmpl	%eax, (%r9,%rdi,4)
	jl	.L11
	cmpl	$6, %esi
	ja	.L16
	leaq	.L18(%rip), %rdi
	movl	%esi, %esi
	movslq	(%rdi,%rsi,4), %rsi
	addq	%rdi, %rsi
	notrack jmp	*%rsi
	.section	.rodata
	.align 4
	.align 4
.L18:
	.long	.L24-.L18
	.long	.L23-.L18
	.long	.L22-.L18
	.long	.L21-.L18
	.long	.L20-.L18
	.long	.L19-.L18
	.long	.L17-.L18
	.text
	.p2align 4,,10
	.p2align 3
.L14:
	movq	-128(%rbp), %r13
	cmpl	$24, %r11d
	je	.L97
.L34:
	cmpb	$90, %al
	je	.L46
	movl	-136(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L47
	.p2align 4,,10
	.p2align 3
.L11:
	xorl	%eax, %eax
.L6:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L98
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L19:
	.cfi_restore_state
	movl	%eax, -108(%rbp)
.L16:
	movzbl	2(%rbx), %eax
	addl	$1, %r12d
	cmpl	%r12d, -116(%rbp)
	je	.L14
	cmpb	$0, -120(%rbp)
	je	.L13
	cmpl	%r12d, -132(%rbp)
	jne	.L13
	leal	-43(%rax), %ecx
	andl	$253, %ecx
	je	.L14
	cmpb	$90, %al
	je	.L14
.L13:
	addq	$2, %rbx
	movl	%edx, %r14d
	jmp	.L12
.L20:
	movl	%eax, -104(%rbp)
	jmp	.L16
.L21:
	movl	-96(%rbp), %r8d
	cmpl	$1, %r8d
	je	.L99
	movslq	%r8d, %rcx
	leaq	mdays.9053(%rip), %rsi
	cmpl	(%rsi,%rcx,4), %eax
	jg	.L11
	leaq	ydays.9042(%rip), %r9
	movl	-92(%rbp), %esi
	movl	%eax, -100(%rbp)
	movl	(%r9,%rcx,4), %r10d
	leal	1900(%rsi), %edi
	addl	%eax, %r10d
	leal	-1(%r10), %r13d
	cmpl	$1, %r8d
	jle	.L30
	movslq	%edi, %rsi
	movl	%edi, %r9d
	imulq	$1374389535, %rsi, %rcx
	sarl	$31, %r9d
	sarq	$37, %rcx
	subl	%r9d, %ecx
	movl	%edi, %r9d
	imull	$100, %ecx, %ecx
	subl	%ecx, %r9d
	movl	%r9d, %ecx
	imull	$-1030792151, %edi, %r9d
	addl	$85899344, %r9d
	rorl	$4, %r9d
	cmpl	$10737418, %r9d
	jbe	.L31
	movl	%r13d, %r10d
	testl	%ecx, %ecx
	je	.L31
	xorl	%r9d, %r9d
	testb	$3, %dil
	sete	%r9b
	addl	%r9d, %r10d
.L31:
	movl	%r10d, -84(%rbp)
	addl	$2, %r8d
.L32:
	leal	(%r8,%r8,2), %r9d
	leal	(%r8,%r9,4), %r9d
	movslq	%r9d, %r8
	sarl	$31, %r9d
	imulq	$1717986919, %r8, %r8
	sarq	$33, %r8
	subl	%r9d, %r8d
	addl	%r8d, %eax
	leal	3(%rcx), %r8d
	addl	%ecx, %eax
	testl	%ecx, %ecx
	cmovs	%r8d, %ecx
	imulq	$1374389535, %rsi, %rsi
	sarl	$31, %edi
	sarl	$2, %ecx
	addl	%eax, %ecx
	movq	%rsi, %rax
	sarq	$37, %rsi
	sarq	$39, %rax
	subl	%edi, %esi
	subl	%edi, %eax
	addl	%ecx, %eax
	leal	(%rsi,%rsi,4), %ecx
	leal	6(%rax,%rcx), %ecx
	movslq	%ecx, %rax
	movl	%ecx, %esi
	imulq	$-1840700269, %rax, %rax
	sarl	$31, %esi
	shrq	$32, %rax
	addl	%ecx, %eax
	sarl	$2, %eax
	subl	%esi, %eax
	leal	0(,%rax,8), %esi
	subl	%eax, %esi
	subl	%esi, %ecx
	movl	%ecx, -88(%rbp)
	jmp	.L16
.L22:
	subl	$49, %ecx
	movl	%ecx, -96(%rbp)
	jmp	.L16
.L17:
	movl	%eax, -112(%rbp)
	jmp	.L16
.L23:
	cmpl	$23, %r11d
	je	.L100
	addl	%eax, -92(%rbp)
	jmp	.L16
.L24:
	imull	$100, %eax, %eax
	subl	$1900, %eax
	movl	%eax, -92(%rbp)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L96:
	movq	16(%rsi), %rax
	movl	$6, -116(%rbp)
	movl	$5, -132(%rbp)
	andl	$256, %eax
	cmpq	$1, %rax
	sbbl	%edx, %edx
	addl	$1, %edx
	cmpq	$1, %rax
	sbbl	%eax, %eax
	movl	%edx, -136(%rbp)
	andl	$-2, %eax
	addl	$13, %eax
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L100:
	addl	$52, %ecx
	cmpl	$50, %eax
	cmovl	%ecx, %eax
	movl	%eax, -92(%rbp)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L99:
	movl	-92(%rbp), %esi
	movl	$29, %r9d
	leal	1900(%rsi), %r10d
	imull	$-1030792151, %r10d, %edi
	addl	$85899344, %edi
	movl	%edi, %r13d
	rorl	$4, %r13d
	cmpl	$10737418, %r13d
	jbe	.L28
	rorl	$2, %edi
	movl	$28, %r9d
	cmpl	$42949672, %edi
	ja	.L101
.L28:
	cmpl	%r9d, %eax
	jg	.L11
	subl	$18, %ecx
	movl	%eax, -100(%rbp)
	movl	%ecx, -84(%rbp)
.L48:
	leal	1899(%rsi), %edi
	addl	$14, %r8d
	movslq	%edi, %rsi
	movl	%edi, %r9d
	movl	%edi, %r10d
	imulq	$1374389535, %rsi, %rcx
	sarl	$31, %r9d
	sarq	$37, %rcx
	subl	%r9d, %ecx
	imull	$100, %ecx, %ecx
	subl	%ecx, %r10d
	movl	%r10d, %ecx
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L47:
	leal	-43(%rax), %ecx
	andl	$253, %ecx
	jne	.L11
	xorl	%ebx, %ebx
	cmpb	$45, %al
	leal	5(%rdx), %eax
	sete	%bl
	leal	-1(%rbx,%rbx), %ebx
	cmpl	%r15d, %eax
	jne	.L11
	movl	-116(%rbp), %eax
	addl	$3, %edx
	movl	$0, -132(%rbp)
	movslq	%edx, %r12
	movl	%r15d, -156(%rbp)
	leal	1(%rax), %ecx
	leal	2(%rax), %edx
	movq	%r12, %r14
	movq	-152(%rbp), %r12
	movl	%ecx, -120(%rbp)
	movl	%edx, -128(%rbp)
	movl	%ebx, -136(%rbp)
	movl	%eax, %ebx
.L45:
	movsbl	-2(%r12,%r14), %edi
	call	ascii_isdigit@PLT
	testl	%eax, %eax
	je	.L11
	movsbl	-2(%r12,%r14), %edx
	movsbl	-1(%r12,%r14), %edi
	leal	-48(%rdx), %r15d
	call	ascii_isdigit@PLT
	testl	%eax, %eax
	je	.L11
	movsbl	-1(%r12,%r14), %eax
	leal	(%r15,%r15,4), %edx
	leaq	min.9051(%rip), %rcx
	leal	-48(%rax,%rdx,2), %edx
	xorl	%eax, %eax
	cmpl	$23, 4(%r13)
	sete	%al
	addl	%ebx, %eax
	cltq
	cmpl	%edx, (%rcx,%rax,4)
	jg	.L11
	leaq	max.9052(%rip), %rcx
	cmpl	%edx, (%rcx,%rax,4)
	jl	.L11
	cmpq	$0, -144(%rbp)
	je	.L43
	cmpl	%ebx, -116(%rbp)
	je	.L102
	cmpl	%ebx, -120(%rbp)
	je	.L103
.L43:
	movl	%r14d, %r8d
	addl	$1, %ebx
	addq	$2, %r14
	cmpl	-128(%rbp), %ebx
	jne	.L45
	movl	-132(%rbp), %eax
	movl	-136(%rbp), %ebx
	movl	-156(%rbp), %r15d
	testl	%eax, %eax
	je	.L40
	imull	%ebx, %eax
	xorl	%esi, %esi
	leaq	-112(%rbp), %rdi
	movl	%r8d, -116(%rbp)
	movslq	%eax, %rdx
	call	OPENSSL_gmtime_adj@PLT
	movl	-116(%rbp), %r8d
	testl	%eax, %eax
	je	.L11
	.p2align 4,,10
	.p2align 3
.L40:
	cmpl	%r15d, %r8d
	jne	.L11
	movq	-144(%rbp), %rbx
	movl	$1, %eax
	testq	%rbx, %rbx
	je	.L6
	movdqa	-112(%rbp), %xmm1
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	movq	-64(%rbp), %rdx
	movups	%xmm1, (%rbx)
	movq	%rdx, 48(%rbx)
	movups	%xmm2, 16(%rbx)
	movups	%xmm3, 32(%rbx)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L101:
	xorl	%r9d, %r9d
	andl	$3, %r10d
	sete	%r9b
	addl	$28, %r9d
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L97:
	cmpb	$46, %al
	jne	.L34
	movl	-136(%rbp), %esi
	testl	%esi, %esi
	jne	.L11
	addl	$3, %r14d
	cmpl	%r14d, %r15d
	je	.L11
	jle	.L11
	movq	-152(%rbp), %r12
	movslq	%r14d, %rbx
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L37:
	leal	1(%rbx), %r13d
	addq	$1, %rbx
	cmpl	%ebx, %r15d
	jle	.L36
.L35:
	movsbl	(%r12,%rbx), %edi
	movl	%ebx, %r13d
	call	ascii_isdigit@PLT
	testl	%eax, %eax
	jne	.L37
.L36:
	movl	%r13d, %edx
	movq	-128(%rbp), %r13
	cmpl	%edx, %r15d
	je	.L11
	cmpl	%edx, %r14d
	je	.L11
	movq	-152(%rbp), %rbx
	movslq	%edx, %rax
	movzbl	(%rbx,%rax), %eax
	cmpb	$90, %al
	jne	.L47
	.p2align 4,,10
	.p2align 3
.L46:
	leal	1(%rdx), %r8d
	jmp	.L40
.L30:
	movl	%r13d, -84(%rbp)
	jmp	.L48
.L102:
	imull	$3600, %edx, %eax
	movl	%eax, -132(%rbp)
	jmp	.L43
.L103:
	imull	$60, %edx, %edx
	addl	%edx, -132(%rbp)
	jmp	.L43
.L98:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE498:
	.size	asn1_time_to_tm, .-asn1_time_to_tm
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%04d%02d%02d%02d%02d%02dZ"
.LC1:
	.string	"%02d%02d%02d%02d%02d%02dZ"
	.text
	.p2align 4
	.globl	asn1_time_from_tm
	.type	asn1_time_from_tm, @function
asn1_time_from_tm:
.LFB499:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	cmpl	$-1, %edx
	je	.L129
	movl	%edx, %ebx
	cmpl	$23, %edx
	je	.L130
	cmpl	$24, %edx
	je	.L106
.L108:
	testq	%r12, %r12
	je	.L128
	xorl	%r12d, %r12d
.L114:
	movq	%r12, %rdi
	call	ASN1_STRING_free@PLT
.L128:
	leaq	-24(%rbp), %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	movl	20(%rsi), %eax
	subl	$50, %eax
	cmpl	$99, %eax
	ja	.L108
.L106:
	testq	%r12, %r12
	je	.L131
.L109:
	xorl	%esi, %esi
	movl	$20, %edx
	movq	%r12, %rdi
	call	ASN1_STRING_set@PLT
	testl	%eax, %eax
	je	.L128
.L115:
	movl	16(%r13), %eax
	movq	8(%r12), %rdi
	movl	%ebx, 4(%r12)
	movl	0(%r13), %r10d
	movl	4(%r13), %esi
	movl	8(%r13), %edx
	movl	12(%r13), %r9d
	leal	1(%rax), %r8d
	movl	20(%r13), %ecx
	cmpl	$24, %ebx
	je	.L132
	movslq	%ecx, %rax
	movl	%ecx, %r11d
	subq	$8, %rsp
	imulq	$1374389535, %rax, %rax
	sarl	$31, %r11d
	pushq	%r10
	pushq	%rsi
	pushq	%rdx
	leaq	.LC1(%rip), %rdx
	sarq	$37, %rax
	subl	%r11d, %eax
	imull	$100, %eax, %eax
	subl	%eax, %ecx
.L127:
	movl	$20, %esi
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	addq	$32, %rsp
	movl	%eax, (%r12)
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	movl	20(%rsi), %eax
	xorl	%ebx, %ebx
	subl	$50, %eax
	cmpl	$99, %eax
	seta	%bl
	addl	$23, %ebx
	testq	%r12, %r12
	jne	.L109
.L131:
	call	ASN1_STRING_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L128
	xorl	%esi, %esi
	movl	$20, %edx
	movq	%rax, %rdi
	call	ASN1_STRING_set@PLT
	testl	%eax, %eax
	jne	.L115
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L132:
	subq	$8, %rsp
	addl	$1900, %ecx
	pushq	%r10
	pushq	%rsi
	pushq	%rdx
	leaq	.LC0(%rip), %rdx
	jmp	.L127
	.cfi_endproc
.LFE499:
	.size	asn1_time_from_tm, .-asn1_time_from_tm
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"../deps/openssl/openssl/crypto/asn1/a_time.c"
	.text
	.p2align 4
	.globl	ASN1_TIME_set
	.type	ASN1_TIME_set, @function
ASN1_TIME_set:
.LFB500:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	-104(%rbp), %rdi
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -104(%rbp)
	leaq	-96(%rbp), %rsi
	call	OPENSSL_gmtime@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L151
	movl	20(%rax), %eax
	xorl	%r13d, %r13d
	subl	$50, %eax
	cmpl	$99, %eax
	seta	%r13b
	addl	$23, %r13d
	testq	%rbx, %rbx
	je	.L152
	xorl	%esi, %esi
	movl	$20, %edx
	movq	%rbx, %rdi
	call	ASN1_STRING_set@PLT
	testl	%eax, %eax
	je	.L150
.L141:
	movl	16(%r12), %eax
	movq	8(%rbx), %rdi
	movl	%r13d, 4(%rbx)
	movl	(%r12), %r10d
	movl	4(%r12), %esi
	movl	8(%r12), %edx
	movl	12(%r12), %r9d
	leal	1(%rax), %r8d
	movl	20(%r12), %ecx
	cmpl	$24, %r13d
	je	.L153
	movslq	%ecx, %rax
	movl	%ecx, %r11d
	subq	$8, %rsp
	imulq	$1374389535, %rax, %rax
	sarl	$31, %r11d
	pushq	%r10
	pushq	%rsi
	pushq	%rdx
	leaq	.LC1(%rip), %rdx
	sarq	$37, %rax
	subl	%r11d, %eax
	imull	$100, %eax, %eax
	subl	%eax, %ecx
.L149:
	movl	$20, %esi
	xorl	%eax, %eax
	movq	%rbx, %r12
	call	BIO_snprintf@PLT
	addq	$32, %rsp
	movl	%eax, (%rbx)
.L133:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L154
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore_state
	call	ASN1_STRING_new@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L150
	xorl	%esi, %esi
	movl	$20, %edx
	movq	%rax, -120(%rbp)
	call	ASN1_STRING_set@PLT
	movq	-120(%rbp), %rdi
	testl	%eax, %eax
	jne	.L155
	call	ASN1_STRING_free@PLT
.L150:
	xorl	%r12d, %r12d
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L153:
	subq	$8, %rsp
	addl	$1900, %ecx
	pushq	%r10
	pushq	%rsi
	pushq	%rdx
	leaq	.LC0(%rip), %rdx
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L151:
	movl	$330, %r8d
	movl	$173, %edx
	movl	$217, %esi
	movl	$13, %edi
	leaq	.LC2(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L133
.L154:
	call	__stack_chk_fail@PLT
.L155:
	movq	%rdi, %rbx
	jmp	.L141
	.cfi_endproc
.LFE500:
	.size	ASN1_TIME_set, .-ASN1_TIME_set
	.p2align 4
	.globl	ASN1_TIME_adj
	.type	ASN1_TIME_adj, @function
ASN1_TIME_adj:
.LFB501:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	leaq	-104(%rbp), %rdi
	subq	$80, %rsp
	movq	%rsi, -104(%rbp)
	leaq	-96(%rbp), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	OPENSSL_gmtime@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L181
	testl	%r13d, %r13d
	jne	.L159
	testq	%r14, %r14
	jne	.L159
.L162:
	movl	20(%r12), %eax
	xorl	%r13d, %r13d
	subl	$50, %eax
	cmpl	$99, %eax
	seta	%r13b
	addl	$23, %r13d
	testq	%rbx, %rbx
	je	.L182
	xorl	%esi, %esi
	movl	$20, %edx
	movq	%rbx, %rdi
	call	ASN1_STRING_set@PLT
	testl	%eax, %eax
	je	.L180
.L168:
	movl	16(%r12), %eax
	movq	8(%rbx), %rdi
	movl	%r13d, 4(%rbx)
	movl	(%r12), %r10d
	movl	4(%r12), %esi
	movl	8(%r12), %edx
	movl	12(%r12), %r9d
	leal	1(%rax), %r8d
	movl	20(%r12), %ecx
	cmpl	$24, %r13d
	je	.L183
	movslq	%ecx, %rax
	movl	%ecx, %r11d
	subq	$8, %rsp
	imulq	$1374389535, %rax, %rax
	sarl	$31, %r11d
	pushq	%r10
	pushq	%rsi
	pushq	%rdx
	leaq	.LC1(%rip), %rdx
	sarq	$37, %rax
	subl	%r11d, %eax
	imull	$100, %eax, %eax
	subl	%eax, %ecx
.L179:
	movl	$20, %esi
	xorl	%eax, %eax
	movq	%rbx, %r12
	call	BIO_snprintf@PLT
	addq	$32, %rsp
	movl	%eax, (%rbx)
.L156:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L184
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore_state
	movq	%r14, %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	OPENSSL_gmtime_adj@PLT
	testl	%eax, %eax
	jne	.L162
.L180:
	xorl	%r12d, %r12d
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L183:
	subq	$8, %rsp
	addl	$1900, %ecx
	pushq	%r10
	pushq	%rsi
	pushq	%rdx
	leaq	.LC0(%rip), %rdx
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L182:
	call	ASN1_STRING_new@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L180
	xorl	%esi, %esi
	movl	$20, %edx
	movq	%rax, -112(%rbp)
	call	ASN1_STRING_set@PLT
	movq	-112(%rbp), %rdi
	testl	%eax, %eax
	jne	.L185
	call	ASN1_STRING_free@PLT
	xorl	%r12d, %r12d
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L181:
	movl	$330, %r8d
	movl	$173, %edx
	movl	$217, %esi
	movl	$13, %edi
	leaq	.LC2(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L156
.L184:
	call	__stack_chk_fail@PLT
.L185:
	movq	%rdi, %rbx
	jmp	.L168
	.cfi_endproc
.LFE501:
	.size	ASN1_TIME_adj, .-ASN1_TIME_adj
	.p2align 4
	.globl	ASN1_TIME_check
	.type	ASN1_TIME_check, @function
ASN1_TIME_check:
.LFB502:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	cmpl	$24, %eax
	je	.L189
	cmpl	$23, %eax
	je	.L190
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	jmp	ASN1_GENERALIZEDTIME_check@PLT
	.p2align 4,,10
	.p2align 3
.L190:
	jmp	ASN1_UTCTIME_check@PLT
	.cfi_endproc
.LFE502:
	.size	ASN1_TIME_check, .-ASN1_TIME_check
	.p2align 4
	.globl	ASN1_TIME_to_generalizedtime
	.type	ASN1_TIME_to_generalizedtime, @function
ASN1_TIME_to_generalizedtime:
.LFB503:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L222
	movq	%rdi, %r8
	leaq	-80(%rbp), %rdi
	movq	%r8, %rsi
	call	asn1_time_to_tm
	testl	%eax, %eax
	je	.L221
.L194:
	testq	%rbx, %rbx
	je	.L196
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L196
	xorl	%esi, %esi
	movl	$20, %edx
	movq	%r12, %rdi
	call	ASN1_STRING_set@PLT
	testl	%eax, %eax
	je	.L221
	movl	-60(%rbp), %eax
	subq	$8, %rsp
	movl	-68(%rbp), %r9d
	movl	$20, %esi
	movl	$24, 4(%r12)
	movq	8(%r12), %rdi
	leaq	.LC0(%rip), %rdx
	leal	1900(%rax), %ecx
	movl	-80(%rbp), %eax
	pushq	%rax
	movl	-76(%rbp), %eax
	pushq	%rax
	movl	-72(%rbp), %eax
	pushq	%rax
	movl	-64(%rbp), %eax
	leal	1(%rax), %r8d
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	addq	$32, %rsp
	movl	%eax, (%r12)
.L200:
	movq	%r12, (%rbx)
.L191:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L223
	leaq	-16(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L196:
	.cfi_restore_state
	call	ASN1_STRING_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L221
	xorl	%esi, %esi
	movl	$20, %edx
	movq	%rax, %rdi
	call	ASN1_STRING_set@PLT
	testl	%eax, %eax
	jne	.L224
	movq	%r12, %rdi
	call	ASN1_STRING_free@PLT
.L221:
	xorl	%r12d, %r12d
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L222:
	leaq	-88(%rbp), %r12
	movq	%r12, %rdi
	call	time@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movq	$0, -32(%rbp)
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm0, -64(%rbp)
	movaps	%xmm0, -48(%rbp)
	call	OPENSSL_gmtime@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L194
	jmp	.L191
.L224:
	movl	-60(%rbp), %eax
	subq	$8, %rsp
	movl	-68(%rbp), %r9d
	movl	$20, %esi
	movl	$24, 4(%r12)
	movq	8(%r12), %rdi
	leaq	.LC0(%rip), %rdx
	leal	1900(%rax), %ecx
	movl	-80(%rbp), %eax
	pushq	%rax
	movl	-76(%rbp), %eax
	pushq	%rax
	movl	-72(%rbp), %eax
	pushq	%rax
	movl	-64(%rbp), %eax
	leal	1(%rax), %r8d
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	addq	$32, %rsp
	movl	%eax, (%r12)
	testq	%rbx, %rbx
	jne	.L200
	jmp	.L191
.L223:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE503:
	.size	ASN1_TIME_to_generalizedtime, .-ASN1_TIME_to_generalizedtime
	.p2align 4
	.globl	ASN1_TIME_set_string
	.type	ASN1_TIME_set_string, @function
ASN1_TIME_set_string:
.LFB504:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	ASN1_UTCTIME_set_string@PLT
	testl	%eax, %eax
	je	.L228
	popq	%r12
	movl	$1, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L228:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	ASN1_GENERALIZEDTIME_set_string@PLT
	.cfi_endproc
.LFE504:
	.size	ASN1_TIME_set_string, .-ASN1_TIME_set_string
	.p2align 4
	.globl	ASN1_TIME_set_string_X509
	.type	ASN1_TIME_set_string_X509, @function
ASN1_TIME_set_string_X509:
.LFB505:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-128(%rbp), %r13
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	strlen@PLT
	movq	%r13, %rdi
	movq	%rbx, -120(%rbp)
	movl	%eax, -128(%rbp)
	movq	$256, -112(%rbp)
	movl	$23, -124(%rbp)
	call	ASN1_UTCTIME_check@PLT
	testl	%eax, %eax
	je	.L230
.L233:
	testq	%r12, %r12
	je	.L246
	cmpl	$24, -124(%rbp)
	je	.L247
.L237:
	movq	%r12, %rdi
	movq	%r13, %rsi
	xorl	%r12d, %r12d
	call	ASN1_STRING_copy@PLT
	testl	%eax, %eax
	setne	%r12b
.L232:
	movq	-120(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L229
	movl	$432, %edx
	leaq	.LC2(%rip), %rsi
	call	CRYPTO_free@PLT
.L234:
	endbr64
.L229:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L248
	addq	$104, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L230:
	.cfi_restore_state
	movq	%r13, %rdi
	movl	$24, -124(%rbp)
	call	ASN1_GENERALIZEDTIME_check@PLT
	testl	%eax, %eax
	jne	.L233
.L236:
	xorl	%r12d, %r12d
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L246:
	movl	$1, %r12d
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L247:
	leaq	-96(%rbp), %rdi
	movq	%r13, %rsi
	call	asn1_time_to_tm
	testl	%eax, %eax
	je	.L236
	movl	-76(%rbp), %eax
	subl	$50, %eax
	cmpl	$99, %eax
	ja	.L237
	movl	-128(%rbp), %eax
	leaq	.LC2(%rip), %rsi
	leal	-2(%rax), %edx
	subl	$1, %eax
	movl	%edx, -128(%rbp)
	movslq	%eax, %rdi
	movl	$420, %edx
	call	CRYPTO_zalloc@PLT
	movq	%rax, -120(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L236
	movslq	-128(%rbp), %rdx
	leaq	2(%rbx), %rsi
	call	memcpy@PLT
	movl	$23, -124(%rbp)
	jmp	.L237
.L248:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE505:
	.size	ASN1_TIME_set_string_X509, .-ASN1_TIME_set_string_X509
	.p2align 4
	.globl	ASN1_TIME_to_tm
	.type	ASN1_TIME_to_tm, @function
ASN1_TIME_to_tm:
.LFB506:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L254
	movq	%rdi, %rsi
	movq	%r12, %rdi
	call	asn1_time_to_tm
.L249:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L255
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L254:
	.cfi_restore_state
	leaq	-32(%rbp), %r13
	movq	%r13, %rdi
	call	time@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	$0, 48(%r12)
	movups	%xmm0, (%r12)
	movups	%xmm0, 16(%r12)
	movups	%xmm0, 32(%r12)
	call	OPENSSL_gmtime@PLT
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	jmp	.L249
.L255:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE506:
	.size	ASN1_TIME_to_tm, .-ASN1_TIME_to_tm
	.p2align 4
	.globl	ASN1_TIME_diff
	.type	ASN1_TIME_diff, @function
ASN1_TIME_diff:
.LFB507:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L272
	leaq	-176(%rbp), %r15
	movq	%rdx, %rsi
	leaq	-112(%rbp), %rbx
	movq	%r15, %rdi
	call	asn1_time_to_tm
	testl	%eax, %eax
	jne	.L259
.L264:
	xorl	%eax, %eax
.L256:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L273
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L272:
	.cfi_restore_state
	leaq	-112(%rbp), %rbx
	leaq	-176(%rbp), %r15
	movq	%rbx, %rdi
	call	time@PLT
	pxor	%xmm0, %xmm0
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	$0, -128(%rbp)
	movaps	%xmm0, -176(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	call	OPENSSL_gmtime@PLT
	movq	%rax, %r8
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L256
.L259:
	testq	%r14, %r14
	je	.L274
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	asn1_time_to_tm
	testl	%eax, %eax
	je	.L264
.L263:
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	OPENSSL_gmtime_diff@PLT
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L274:
	leaq	-184(%rbp), %r14
	movq	%r14, %rdi
	call	time@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	$0, -64(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	OPENSSL_gmtime@PLT
	testq	%rax, %rax
	jne	.L263
	jmp	.L256
.L273:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE507:
	.size	ASN1_TIME_diff, .-ASN1_TIME_diff
	.section	.rodata.str1.1
.LC3:
	.string	" GMT"
.LC4:
	.string	""
.LC5:
	.string	"Bad time value"
	.section	.rodata.str1.8
	.align 8
.LC6:
	.string	"%s %2d %02d:%02d:%02d%.*s %d%s"
	.section	.rodata.str1.1
.LC7:
	.string	"%s %2d %02d:%02d:%02d %d%s"
	.text
	.p2align 4
	.globl	ASN1_TIME_print
	.type	ASN1_TIME_print, @function
ASN1_TIME_print:
.LFB508:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	leaq	-112(%rbp), %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	asn1_time_to_tm
	testl	%eax, %eax
	je	.L297
	movslq	(%rbx), %rcx
	movq	8(%rbx), %rax
	movl	4(%rbx), %edx
	cmpb	$90, -1(%rax,%rcx)
	movq	%rcx, %r14
	je	.L278
	cmpl	$24, %edx
	je	.L279
	leaq	.LC4(%rip), %rax
.L280:
	subq	$8, %rsp
	movslq	-96(%rbp), %rcx
	movl	-108(%rbp), %r9d
	movq	%r13, %rdi
	pushq	%rax
	movl	-92(%rbp), %eax
	leaq	_asn1_mon(%rip), %rdx
	xorl	%r12d, %r12d
	leaq	(%rdx,%rcx,4), %rdx
	movl	-104(%rbp), %r8d
	movl	-100(%rbp), %ecx
	leaq	.LC7(%rip), %rsi
	addl	$1900, %eax
	pushq	%rax
	movl	-112(%rbp), %eax
	pushq	%rax
	xorl	%eax, %eax
	call	BIO_printf@PLT
	addq	$32, %rsp
	testl	%eax, %eax
	setg	%r12b
.L275:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L298
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L278:
	.cfi_restore_state
	cmpl	$24, %edx
	je	.L281
	leaq	.LC3(%rip), %rax
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L297:
	movl	$14, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r13, %rdi
	movl	%eax, %r12d
	call	BIO_write@PLT
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L279:
	cmpl	$15, %ecx
	jle	.L295
	cmpb	$46, 14(%rax)
	je	.L289
.L295:
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	leaq	.LC4(%rip), %rax
.L286:
	subq	$8, %rsp
	movslq	-96(%rbp), %rsi
	movl	-108(%rbp), %r9d
	movq	%r13, %rdi
	pushq	%rax
	movl	-92(%rbp), %eax
	leaq	_asn1_mon(%rip), %rcx
	leaq	(%rcx,%rsi,4), %rdx
	movl	-104(%rbp), %r8d
	movl	-100(%rbp), %ecx
	leaq	.LC6(%rip), %rsi
	addl	$1900, %eax
	pushq	%rax
	movl	-112(%rbp), %eax
	pushq	%r12
	xorl	%r12d, %r12d
	pushq	%r15
	pushq	%rax
	xorl	%eax, %eax
	call	BIO_printf@PLT
	addq	$48, %rsp
	testl	%eax, %eax
	setg	%r12b
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L281:
	cmpl	$15, %ecx
	jle	.L296
	cmpb	$46, 14(%rax)
	jne	.L296
	movl	$1, -116(%rbp)
	leaq	14(%rax), %r12
	.p2align 4,,10
	.p2align 3
.L282:
	movl	$1, %ebx
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L285:
	leal	1(%rbx), %r15d
	addq	$1, %rbx
	leal	14(%rbx), %eax
	cmpl	%eax, %r14d
	jle	.L284
.L283:
	movsbl	(%r12,%rbx), %edi
	movl	%ebx, %r15d
	call	ascii_isdigit@PLT
	testl	%eax, %eax
	jne	.L285
.L284:
	movl	-116(%rbp), %eax
	leaq	.LC4(%rip), %rcx
	testl	%eax, %eax
	leaq	.LC3(%rip), %rax
	cmove	%rcx, %rax
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L296:
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	leaq	.LC3(%rip), %rax
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L289:
	movl	$0, -116(%rbp)
	leaq	14(%rax), %r12
	jmp	.L282
.L298:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE508:
	.size	ASN1_TIME_print, .-ASN1_TIME_print
	.p2align 4
	.globl	ASN1_TIME_cmp_time_t
	.type	ASN1_TIME_cmp_time_t, @function
ASN1_TIME_cmp_time_t:
.LFB509:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$160, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L317
	leaq	-144(%rbp), %r13
	movq	%rdi, %rsi
	movq	%r13, %rdi
	call	asn1_time_to_tm
	testl	%eax, %eax
	je	.L304
	leaq	-80(%rbp), %r12
.L302:
	leaq	-168(%rbp), %rdi
	movq	%r12, %rsi
	call	OPENSSL_gmtime@PLT
	testq	%rax, %rax
	je	.L304
	leaq	-148(%rbp), %rsi
	leaq	-152(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r12, %rdx
	call	OPENSSL_gmtime_diff@PLT
	testl	%eax, %eax
	je	.L304
	movl	-152(%rbp), %edx
	movl	$1, %eax
	testl	%edx, %edx
	jg	.L299
	movl	-148(%rbp), %ecx
	testl	%ecx, %ecx
	jg	.L299
	xorl	%eax, %eax
	orl	%ecx, %edx
	setne	%al
	negl	%eax
.L299:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L318
	addq	$160, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L317:
	.cfi_restore_state
	leaq	-80(%rbp), %r12
	leaq	-144(%rbp), %r13
	movq	%r12, %rdi
	call	time@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	$0, -96(%rbp)
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	OPENSSL_gmtime@PLT
	movq	%rax, %r8
	movl	$-2, %eax
	testq	%r8, %r8
	jne	.L302
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L304:
	movl	$-2, %eax
	jmp	.L299
.L318:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE509:
	.size	ASN1_TIME_cmp_time_t, .-ASN1_TIME_cmp_time_t
	.p2align 4
	.globl	ASN1_TIME_normalize
	.type	ASN1_TIME_normalize, @function
ASN1_TIME_normalize:
.LFB510:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L351
	movq	%rdi, %r12
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	asn1_time_to_tm
	testl	%eax, %eax
	jne	.L352
.L319:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L353
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L352:
	.cfi_restore_state
	movl	-92(%rbp), %eax
	movl	$20, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	subl	$50, %eax
	cmpl	$99, %eax
	jbe	.L354
	call	ASN1_STRING_set@PLT
	testl	%eax, %eax
	jne	.L355
.L330:
	xorl	%eax, %eax
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L351:
	leaq	-120(%rbp), %r12
	movq	%r12, %rdi
	call	time@PLT
	pxor	%xmm0, %xmm0
	leaq	-112(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -64(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	OPENSSL_gmtime@PLT
	movq	%rax, %r8
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L319
	movl	-92(%rbp), %eax
	xorl	%ebx, %ebx
	subl	$50, %eax
	cmpl	$99, %eax
	seta	%bl
	call	ASN1_STRING_new@PLT
	addl	$23, %ebx
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L330
	xorl	%esi, %esi
	movl	$20, %edx
	movq	%rax, %rdi
	call	ASN1_STRING_set@PLT
	testl	%eax, %eax
	je	.L327
	movl	-112(%rbp), %r11d
	movl	-108(%rbp), %r10d
	movl	%ebx, 4(%r12)
	movl	-104(%rbp), %esi
	movl	-100(%rbp), %edx
	movl	-96(%rbp), %eax
	movl	-92(%rbp), %ecx
	movl	%r11d, %r15d
	movl	%r10d, %r14d
	movq	8(%r12), %rdi
	movl	%esi, %r13d
	movl	%edx, %r9d
	movl	%ecx, -132(%rbp)
	leal	1(%rax), %r8d
	cmpl	$24, %ebx
	jne	.L328
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L355:
	movl	-92(%rbp), %eax
	movq	8(%r12), %rdi
	movl	$24, 4(%r12)
	movl	-112(%rbp), %r11d
	movl	-108(%rbp), %r10d
	movl	-104(%rbp), %esi
	movl	-100(%rbp), %edx
	movl	%eax, -132(%rbp)
	movl	-96(%rbp), %eax
.L332:
	subq	$8, %rsp
	movl	-132(%rbp), %ecx
	movl	%edx, %r9d
	leal	1(%rax), %r8d
	pushq	%r11
	leaq	.LC0(%rip), %rdx
	pushq	%r10
	addl	$1900, %ecx
	pushq	%rsi
.L350:
	movl	$20, %esi
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	addq	$32, %rsp
	movl	%eax, (%r12)
	movl	$1, %eax
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L354:
	call	ASN1_STRING_set@PLT
	testl	%eax, %eax
	je	.L330
	movl	-96(%rbp), %eax
	movq	8(%r12), %rdi
	movl	$23, 4(%r12)
	movl	-112(%rbp), %r15d
	movl	-108(%rbp), %r14d
	movl	-104(%rbp), %r13d
	movl	-100(%rbp), %r9d
	leal	1(%rax), %r8d
	movl	-92(%rbp), %ecx
.L328:
	movslq	%ecx, %rax
	movl	%ecx, %edx
	subq	$8, %rsp
	imulq	$1374389535, %rax, %rax
	sarl	$31, %edx
	pushq	%r15
	pushq	%r14
	pushq	%r13
	sarq	$37, %rax
	subl	%edx, %eax
	leaq	.LC1(%rip), %rdx
	imull	$100, %eax, %eax
	subl	%eax, %ecx
	jmp	.L350
.L327:
	movq	%r12, %rdi
	movl	%eax, -132(%rbp)
	call	ASN1_STRING_free@PLT
	movl	-132(%rbp), %eax
	jmp	.L319
.L353:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE510:
	.size	ASN1_TIME_normalize, .-ASN1_TIME_normalize
	.p2align 4
	.globl	ASN1_TIME_compare
	.type	ASN1_TIME_compare, @function
ASN1_TIME_compare:
.LFB511:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L379
	leaq	-160(%rbp), %r13
	movq	%r13, %rdi
	call	asn1_time_to_tm
	testl	%eax, %eax
	jne	.L380
.L367:
	movl	$-2, %eax
.L356:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L381
	addq	$152, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L380:
	.cfi_restore_state
	leaq	-96(%rbp), %r14
	testq	%r12, %r12
	je	.L382
.L362:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	asn1_time_to_tm
	testl	%eax, %eax
	je	.L367
	leaq	-168(%rbp), %r12
.L364:
	leaq	-172(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	OPENSSL_gmtime_diff@PLT
	testl	%eax, %eax
	je	.L367
	movl	-172(%rbp), %edx
	movl	$1, %eax
	testl	%edx, %edx
	jg	.L356
	movl	-168(%rbp), %ecx
	testl	%ecx, %ecx
	jg	.L356
	xorl	%eax, %eax
	orl	%ecx, %edx
	setne	%al
	negl	%eax
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L379:
	leaq	-96(%rbp), %r14
	leaq	-160(%rbp), %r13
	movq	%r14, %rdi
	call	time@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	$0, -112(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm0, -128(%rbp)
	call	OPENSSL_gmtime@PLT
	testq	%rax, %rax
	je	.L367
	testq	%r12, %r12
	jne	.L362
.L382:
	leaq	-168(%rbp), %r12
	movq	%r12, %rdi
	call	time@PLT
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	$0, -48(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	OPENSSL_gmtime@PLT
	testq	%rax, %rax
	jne	.L364
	jmp	.L367
.L381:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE511:
	.size	ASN1_TIME_compare, .-ASN1_TIME_compare
	.section	.rodata
	.align 32
	.type	ydays.9042, @object
	.size	ydays.9042, 48
ydays.9042:
	.long	0
	.long	31
	.long	59
	.long	90
	.long	120
	.long	151
	.long	181
	.long	212
	.long	243
	.long	273
	.long	304
	.long	334
	.align 32
	.type	mdays.9053, @object
	.size	mdays.9053, 48
mdays.9053:
	.long	31
	.long	28
	.long	31
	.long	30
	.long	31
	.long	30
	.long	31
	.long	31
	.long	30
	.long	31
	.long	30
	.long	31
	.align 32
	.type	max.9052, @object
	.size	max.9052, 36
max.9052:
	.long	99
	.long	99
	.long	12
	.long	31
	.long	23
	.long	59
	.long	59
	.long	12
	.long	59
	.align 32
	.type	min.9051, @object
	.size	min.9051, 36
min.9051:
	.long	0
	.long	0
	.long	1
	.long	1
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.align 32
	.type	_asn1_mon, @object
	.size	_asn1_mon, 48
_asn1_mon:
	.string	"Jan"
	.string	"Feb"
	.string	"Mar"
	.string	"Apr"
	.string	"May"
	.string	"Jun"
	.string	"Jul"
	.string	"Aug"
	.string	"Sep"
	.string	"Oct"
	.string	"Nov"
	.string	"Dec"
	.globl	ASN1_TIME_it
	.section	.rodata.str1.1
.LC8:
	.string	"ASN1_TIME"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	ASN1_TIME_it, @object
	.size	ASN1_TIME_it, 56
ASN1_TIME_it:
	.byte	5
	.zero	7
	.quad	49152
	.quad	0
	.quad	0
	.quad	0
	.quad	24
	.quad	.LC8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
