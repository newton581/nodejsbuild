	.file	"v3_genn.c"
	.text
	.p2align 4
	.globl	d2i_GENERAL_NAME
	.type	d2i_GENERAL_NAME, @function
d2i_GENERAL_NAME:
.LFB1328:
	.cfi_startproc
	endbr64
	leaq	GENERAL_NAME_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1328:
	.size	d2i_GENERAL_NAME, .-d2i_GENERAL_NAME
	.p2align 4
	.globl	i2d_GENERAL_NAME
	.type	i2d_GENERAL_NAME, @function
i2d_GENERAL_NAME:
.LFB1329:
	.cfi_startproc
	endbr64
	leaq	GENERAL_NAME_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1329:
	.size	i2d_GENERAL_NAME, .-i2d_GENERAL_NAME
	.p2align 4
	.globl	d2i_OTHERNAME
	.type	d2i_OTHERNAME, @function
d2i_OTHERNAME:
.LFB1320:
	.cfi_startproc
	endbr64
	leaq	OTHERNAME_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1320:
	.size	d2i_OTHERNAME, .-d2i_OTHERNAME
	.p2align 4
	.globl	i2d_OTHERNAME
	.type	i2d_OTHERNAME, @function
i2d_OTHERNAME:
.LFB1321:
	.cfi_startproc
	endbr64
	leaq	OTHERNAME_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1321:
	.size	i2d_OTHERNAME, .-i2d_OTHERNAME
	.p2align 4
	.globl	OTHERNAME_new
	.type	OTHERNAME_new, @function
OTHERNAME_new:
.LFB1322:
	.cfi_startproc
	endbr64
	leaq	OTHERNAME_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1322:
	.size	OTHERNAME_new, .-OTHERNAME_new
	.p2align 4
	.globl	OTHERNAME_free
	.type	OTHERNAME_free, @function
OTHERNAME_free:
.LFB1323:
	.cfi_startproc
	endbr64
	leaq	OTHERNAME_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1323:
	.size	OTHERNAME_free, .-OTHERNAME_free
	.p2align 4
	.globl	d2i_EDIPARTYNAME
	.type	d2i_EDIPARTYNAME, @function
d2i_EDIPARTYNAME:
.LFB1324:
	.cfi_startproc
	endbr64
	leaq	EDIPARTYNAME_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1324:
	.size	d2i_EDIPARTYNAME, .-d2i_EDIPARTYNAME
	.p2align 4
	.globl	i2d_EDIPARTYNAME
	.type	i2d_EDIPARTYNAME, @function
i2d_EDIPARTYNAME:
.LFB1325:
	.cfi_startproc
	endbr64
	leaq	EDIPARTYNAME_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1325:
	.size	i2d_EDIPARTYNAME, .-i2d_EDIPARTYNAME
	.p2align 4
	.globl	EDIPARTYNAME_new
	.type	EDIPARTYNAME_new, @function
EDIPARTYNAME_new:
.LFB1326:
	.cfi_startproc
	endbr64
	leaq	EDIPARTYNAME_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1326:
	.size	EDIPARTYNAME_new, .-EDIPARTYNAME_new
	.p2align 4
	.globl	EDIPARTYNAME_free
	.type	EDIPARTYNAME_free, @function
EDIPARTYNAME_free:
.LFB1327:
	.cfi_startproc
	endbr64
	leaq	EDIPARTYNAME_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1327:
	.size	EDIPARTYNAME_free, .-EDIPARTYNAME_free
	.p2align 4
	.globl	GENERAL_NAME_new
	.type	GENERAL_NAME_new, @function
GENERAL_NAME_new:
.LFB1330:
	.cfi_startproc
	endbr64
	leaq	GENERAL_NAME_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1330:
	.size	GENERAL_NAME_new, .-GENERAL_NAME_new
	.p2align 4
	.globl	GENERAL_NAME_free
	.type	GENERAL_NAME_free, @function
GENERAL_NAME_free:
.LFB1331:
	.cfi_startproc
	endbr64
	leaq	GENERAL_NAME_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1331:
	.size	GENERAL_NAME_free, .-GENERAL_NAME_free
	.p2align 4
	.globl	d2i_GENERAL_NAMES
	.type	d2i_GENERAL_NAMES, @function
d2i_GENERAL_NAMES:
.LFB1332:
	.cfi_startproc
	endbr64
	leaq	GENERAL_NAMES_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1332:
	.size	d2i_GENERAL_NAMES, .-d2i_GENERAL_NAMES
	.p2align 4
	.globl	i2d_GENERAL_NAMES
	.type	i2d_GENERAL_NAMES, @function
i2d_GENERAL_NAMES:
.LFB1333:
	.cfi_startproc
	endbr64
	leaq	GENERAL_NAMES_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1333:
	.size	i2d_GENERAL_NAMES, .-i2d_GENERAL_NAMES
	.p2align 4
	.globl	GENERAL_NAMES_new
	.type	GENERAL_NAMES_new, @function
GENERAL_NAMES_new:
.LFB1334:
	.cfi_startproc
	endbr64
	leaq	GENERAL_NAMES_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1334:
	.size	GENERAL_NAMES_new, .-GENERAL_NAMES_new
	.p2align 4
	.globl	GENERAL_NAMES_free
	.type	GENERAL_NAMES_free, @function
GENERAL_NAMES_free:
.LFB1335:
	.cfi_startproc
	endbr64
	leaq	GENERAL_NAMES_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1335:
	.size	GENERAL_NAMES_free, .-GENERAL_NAMES_free
	.p2align 4
	.globl	GENERAL_NAME_dup
	.type	GENERAL_NAME_dup, @function
GENERAL_NAME_dup:
.LFB1336:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	leaq	d2i_GENERAL_NAME(%rip), %rsi
	leaq	i2d_GENERAL_NAME(%rip), %rdi
	jmp	ASN1_dup@PLT
	.cfi_endproc
.LFE1336:
	.size	GENERAL_NAME_dup, .-GENERAL_NAME_dup
	.section	.text.unlikely,"ax",@progbits
.LCOLDB0:
	.text
.LHOTB0:
	.p2align 4
	.globl	GENERAL_NAME_cmp
	.type	GENERAL_NAME_cmp, @function
GENERAL_NAME_cmp:
.LFB1337:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	testq	%rdi, %rdi
	je	.L31
	testq	%rsi, %rsi
	je	.L31
	movl	(%rdi), %eax
	cmpl	(%rsi), %eax
	jne	.L31
	cmpl	$8, %eax
	ja	.L30
	leaq	.L22(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L22:
	.long	.L27-.L22
	.long	.L24-.L22
	.long	.L24-.L22
	.long	.L25-.L22
	.long	.L26-.L22
	.long	.L25-.L22
	.long	.L24-.L22
	.long	.L23-.L22
	.long	.L21-.L22
	.text
.L23:
	movq	8(%rsi), %rsi
	movq	8(%rdi), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ASN1_OCTET_STRING_cmp@PLT
.L21:
	.cfi_restore_state
	movq	8(%rsi), %rsi
	movq	8(%rdi), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	OBJ_cmp@PLT
.L27:
	.cfi_restore_state
	movq	8(%rdi), %rbx
	movq	8(%rsi), %r12
	testq	%rbx, %rbx
	je	.L31
	testq	%r12, %r12
	je	.L31
	movq	(%r12), %rsi
	movq	(%rbx), %rdi
	call	OBJ_cmp@PLT
	testl	%eax, %eax
	jne	.L19
	movq	8(%r12), %rsi
	movq	8(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ASN1_TYPE_cmp@PLT
.L24:
	.cfi_restore_state
	movq	8(%rsi), %rsi
	movq	8(%rdi), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ASN1_STRING_cmp@PLT
.L25:
	.cfi_restore_state
	movq	8(%rsi), %rsi
	movq	8(%rdi), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ASN1_TYPE_cmp@PLT
.L26:
	.cfi_restore_state
	movq	8(%rsi), %rsi
	movq	8(%rdi), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	X509_NAME_cmp@PLT
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	movl	$-1, %eax
.L19:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	GENERAL_NAME_cmp.cold, @function
GENERAL_NAME_cmp.cold:
.LFSB1337:
.L30:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	orl	$-1, %eax
	jmp	.L19
	.cfi_endproc
.LFE1337:
	.text
	.size	GENERAL_NAME_cmp, .-GENERAL_NAME_cmp
	.section	.text.unlikely
	.size	GENERAL_NAME_cmp.cold, .-GENERAL_NAME_cmp.cold
.LCOLDE0:
	.text
.LHOTE0:
	.p2align 4
	.globl	OTHERNAME_cmp
	.type	OTHERNAME_cmp, @function
OTHERNAME_cmp:
.LFB1338:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L37
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testq	%rsi, %rsi
	je	.L35
	movq	%rdi, %rbx
	movq	(%rsi), %rsi
	movq	(%rdi), %rdi
	call	OBJ_cmp@PLT
	testl	%eax, %eax
	je	.L41
.L33:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movq	8(%r12), %rsi
	movq	8(%rbx), %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	ASN1_TYPE_cmp@PLT
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movl	$-1, %eax
	jmp	.L33
.L37:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	orl	$-1, %eax
	ret
	.cfi_endproc
.LFE1338:
	.size	OTHERNAME_cmp, .-OTHERNAME_cmp
	.p2align 4
	.globl	GENERAL_NAME_set0_value
	.type	GENERAL_NAME_set0_value, @function
GENERAL_NAME_set0_value:
.LFB1339:
	.cfi_startproc
	endbr64
	cmpl	$8, %esi
	ja	.L43
	leaq	.L45(%rip), %r8
	movl	%esi, %ecx
	movq	%rdx, 8(%rdi)
	movslq	(%r8,%rcx,4), %rax
	addq	%r8, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L45:
	.long	.L43-.L45
	.long	.L43-.L45
	.long	.L43-.L45
	.long	.L43-.L45
	.long	.L43-.L45
	.long	.L43-.L45
	.long	.L43-.L45
	.long	.L43-.L45
	.long	.L43-.L45
	.text
	.p2align 4,,10
	.p2align 3
.L43:
	movl	%esi, (%rdi)
	ret
	.cfi_endproc
.LFE1339:
	.size	GENERAL_NAME_set0_value, .-GENERAL_NAME_set0_value
	.section	.text.unlikely
.LCOLDB1:
	.text
.LHOTB1:
	.p2align 4
	.globl	GENERAL_NAME_get0_value
	.type	GENERAL_NAME_get0_value, @function
GENERAL_NAME_get0_value:
.LFB1340:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	testq	%rsi, %rsi
	je	.L52
	movl	%eax, (%rsi)
	movl	(%rdi), %eax
.L52:
	cmpl	$8, %eax
	ja	.L61
	leaq	.L55(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L55:
	.long	.L60-.L55
	.long	.L57-.L55
	.long	.L57-.L55
	.long	.L58-.L55
	.long	.L59-.L55
	.long	.L58-.L55
	.long	.L57-.L55
	.long	.L56-.L55
	.long	.L54-.L55
	.text
	.p2align 4,,10
	.p2align 3
.L57:
	movq	8(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	movq	8(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	movq	8(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	movq	8(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	movq	8(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	GENERAL_NAME_get0_value.cold, @function
GENERAL_NAME_get0_value.cold:
.LFSB1340:
.L61:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1340:
	.text
	.size	GENERAL_NAME_get0_value, .-GENERAL_NAME_get0_value
	.section	.text.unlikely
	.size	GENERAL_NAME_get0_value.cold, .-GENERAL_NAME_get0_value.cold
.LCOLDE1:
	.text
.LHOTE1:
	.p2align 4
	.globl	GENERAL_NAME_set0_othername
	.type	GENERAL_NAME_set0_othername, @function
GENERAL_NAME_set0_othername:
.LFB1341:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	OTHERNAME_it(%rip), %rdi
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -32
	movq	%rsi, -32(%rbp)
	movq	%rdx, -40(%rbp)
	call	ASN1_item_new@PLT
	movq	%rax, %rbx
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L65
	movq	-32(%rbp), %xmm0
	movq	8(%rbx), %rdi
	movhps	-40(%rbp), %xmm0
	movaps	%xmm0, -32(%rbp)
	call	ASN1_TYPE_free@PLT
	movdqa	-32(%rbp), %xmm0
	movl	$1, %eax
	movups	%xmm0, (%rbx)
	movq	%rbx, 8(%r12)
	movl	$0, (%r12)
.L65:
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1341:
	.size	GENERAL_NAME_set0_othername, .-GENERAL_NAME_set0_othername
	.p2align 4
	.globl	GENERAL_NAME_get0_otherName
	.type	GENERAL_NAME_get0_otherName, @function
GENERAL_NAME_get0_otherName:
.LFB1342:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jne	.L71
	testq	%rsi, %rsi
	je	.L73
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	movq	%rax, (%rsi)
.L73:
	movl	$1, %eax
	testq	%rdx, %rdx
	je	.L71
	movq	8(%rdi), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, (%rdx)
.L71:
	ret
	.cfi_endproc
.LFE1342:
	.size	GENERAL_NAME_get0_otherName, .-GENERAL_NAME_get0_otherName
	.globl	GENERAL_NAMES_it
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	"GENERAL_NAMES"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	GENERAL_NAMES_it, @object
	.size	GENERAL_NAMES_it, 56
GENERAL_NAMES_it:
	.byte	0
	.zero	7
	.quad	-1
	.quad	GENERAL_NAMES_item_tt
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC2
	.section	.rodata.str1.1
.LC3:
	.string	"GeneralNames"
	.section	.data.rel.ro.local
	.align 32
	.type	GENERAL_NAMES_item_tt, @object
	.size	GENERAL_NAMES_item_tt, 40
GENERAL_NAMES_item_tt:
	.quad	4
	.quad	0
	.quad	0
	.quad	.LC3
	.quad	GENERAL_NAME_it
	.globl	GENERAL_NAME_it
	.section	.rodata.str1.1
.LC4:
	.string	"GENERAL_NAME"
	.section	.data.rel.ro.local
	.align 32
	.type	GENERAL_NAME_it, @object
	.size	GENERAL_NAME_it, 56
GENERAL_NAME_it:
	.byte	2
	.zero	7
	.quad	0
	.quad	GENERAL_NAME_ch_tt
	.quad	9
	.quad	0
	.quad	16
	.quad	.LC4
	.section	.rodata.str1.1
.LC5:
	.string	"d.otherName"
.LC6:
	.string	"d.rfc822Name"
.LC7:
	.string	"d.dNSName"
.LC8:
	.string	"d.x400Address"
.LC9:
	.string	"d.directoryName"
.LC10:
	.string	"d.ediPartyName"
.LC11:
	.string	"d.uniformResourceIdentifier"
.LC12:
	.string	"d.iPAddress"
.LC13:
	.string	"d.registeredID"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	GENERAL_NAME_ch_tt, @object
	.size	GENERAL_NAME_ch_tt, 360
GENERAL_NAME_ch_tt:
	.quad	136
	.quad	0
	.quad	8
	.quad	.LC5
	.quad	OTHERNAME_it
	.quad	136
	.quad	1
	.quad	8
	.quad	.LC6
	.quad	ASN1_IA5STRING_it
	.quad	136
	.quad	2
	.quad	8
	.quad	.LC7
	.quad	ASN1_IA5STRING_it
	.quad	136
	.quad	3
	.quad	8
	.quad	.LC8
	.quad	ASN1_SEQUENCE_it
	.quad	144
	.quad	4
	.quad	8
	.quad	.LC9
	.quad	X509_NAME_it
	.quad	136
	.quad	5
	.quad	8
	.quad	.LC10
	.quad	EDIPARTYNAME_it
	.quad	136
	.quad	6
	.quad	8
	.quad	.LC11
	.quad	ASN1_IA5STRING_it
	.quad	136
	.quad	7
	.quad	8
	.quad	.LC12
	.quad	ASN1_OCTET_STRING_it
	.quad	136
	.quad	8
	.quad	8
	.quad	.LC13
	.quad	ASN1_OBJECT_it
	.globl	EDIPARTYNAME_it
	.section	.rodata.str1.1
.LC14:
	.string	"EDIPARTYNAME"
	.section	.data.rel.ro.local
	.align 32
	.type	EDIPARTYNAME_it, @object
	.size	EDIPARTYNAME_it, 56
EDIPARTYNAME_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	EDIPARTYNAME_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC14
	.section	.rodata.str1.1
.LC15:
	.string	"nameAssigner"
.LC16:
	.string	"partyName"
	.section	.data.rel.ro
	.align 32
	.type	EDIPARTYNAME_seq_tt, @object
	.size	EDIPARTYNAME_seq_tt, 80
EDIPARTYNAME_seq_tt:
	.quad	137
	.quad	0
	.quad	0
	.quad	.LC15
	.quad	DIRECTORYSTRING_it
	.quad	137
	.quad	1
	.quad	8
	.quad	.LC16
	.quad	DIRECTORYSTRING_it
	.globl	OTHERNAME_it
	.section	.rodata.str1.1
.LC17:
	.string	"OTHERNAME"
	.section	.data.rel.ro.local
	.align 32
	.type	OTHERNAME_it, @object
	.size	OTHERNAME_it, 56
OTHERNAME_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	OTHERNAME_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC17
	.section	.rodata.str1.1
.LC18:
	.string	"type_id"
.LC19:
	.string	"value"
	.section	.data.rel.ro
	.align 32
	.type	OTHERNAME_seq_tt, @object
	.size	OTHERNAME_seq_tt, 80
OTHERNAME_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC18
	.quad	ASN1_OBJECT_it
	.quad	144
	.quad	0
	.quad	8
	.quad	.LC19
	.quad	ASN1_ANY_it
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
