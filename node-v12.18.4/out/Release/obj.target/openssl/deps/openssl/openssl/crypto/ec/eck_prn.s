	.file	"eck_prn.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ec/eck_prn.c"
	.text
	.p2align 4
	.globl	EC_KEY_print_fp
	.type	EC_KEY_print_fp, @function
EC_KEY_print_fp:
.LFB420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	testq	%rax, %rax
	je	.L6
	movq	%rax, %r12
	movq	%r15, %rcx
	xorl	%edx, %edx
	movl	$106, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	EC_KEY_print@PLT
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	BIO_free@PLT
	popq	%r12
	movl	%r13d, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	xorl	%r13d, %r13d
	movl	$39, %r8d
	movl	$32, %edx
	movl	$181, %esi
	leaq	.LC0(%rip), %rcx
	movl	$16, %edi
	call	ERR_put_error@PLT
	popq	%r12
	movl	%r13d, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE420:
	.size	EC_KEY_print_fp, .-EC_KEY_print_fp
	.p2align 4
	.globl	ECParameters_print_fp
	.type	ECParameters_print_fp, @function
ECParameters_print_fp:
.LFB421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	testq	%rax, %rax
	je	.L11
	movq	%rax, %r12
	movq	%r14, %rcx
	xorl	%edx, %edx
	movl	$106, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	ECParameters_print@PLT
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	BIO_free@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movl	$54, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$32, %edx
	xorl	%r13d, %r13d
	movl	$148, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE421:
	.size	ECParameters_print_fp, .-ECParameters_print_fp
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	""
.LC2:
	.string	":"
.LC3:
	.string	"ASN1 OID: %s"
.LC4:
	.string	"\n"
.LC5:
	.string	"NIST CURVE: %s\n"
.LC6:
	.string	"Field Type: %s\n"
.LC7:
	.string	"Basis Type: %s\n"
.LC8:
	.string	"Polynomial:"
.LC9:
	.string	"A:   "
.LC10:
	.string	"Prime:"
.LC11:
	.string	"B:   "
.LC12:
	.string	"Generator (compressed):"
.LC13:
	.string	"Order: "
.LC14:
	.string	"Generator (uncompressed):"
.LC15:
	.string	"Generator (hybrid):"
.LC16:
	.string	"Cofactor: "
.LC17:
	.string	"Seed:"
.LC18:
	.string	"%s"
.LC19:
	.string	"%02x%s"
	.text
	.p2align 4
	.globl	ECPKParameters_print
	.type	ECPKParameters_print, @function
ECPKParameters_print:
.LFB422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -208(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L39
	movq	%rdi, %r13
	movl	%edx, %ebx
	call	BN_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L40
	movq	%r15, %rdi
	call	EC_GROUP_get_asn1_flag@PLT
	movl	%eax, -212(%rbp)
	testl	%eax, %eax
	je	.L14
	movl	$128, %edx
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	BIO_indent@PLT
	testl	%eax, %eax
	jne	.L79
.L46:
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	movl	$32, %edx
	movq	$0, -200(%rbp)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L40:
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	movl	$65, %edx
	movq	$0, -200(%rbp)
.L13:
	movl	$214, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$149, %esi
	xorl	%r13d, %r13d
	movl	$16, %edi
	call	ERR_put_error@PLT
.L15:
	movq	%r15, %rdi
	call	BN_free@PLT
	movq	%rbx, %rdi
	call	BN_free@PLT
	movq	-200(%rbp), %rdi
	call	BN_free@PLT
	movq	%r14, %rdi
	call	BN_free@PLT
	movq	%r12, %rdi
	call	BN_CTX_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L80
	addq	$232, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	movl	$67, %edx
	movq	$0, -200(%rbp)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L14:
	movq	%r15, %rdi
	call	EC_GROUP_method_of@PLT
	movq	%rax, %rdi
	call	EC_METHOD_get_field_type@PLT
	movl	%eax, -224(%rbp)
	call	BN_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L48
	call	BN_new@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L49
	call	BN_new@PLT
	movq	%rax, -200(%rbp)
	testq	%rax, %rax
	je	.L50
	movq	%r12, %r8
	movq	%rax, %rcx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	EC_GROUP_get_curve@PLT
	testl	%eax, %eax
	jne	.L81
.L54:
	movq	%r14, %r15
	movl	$16, %edx
	xorl	%r14d, %r14d
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L79:
	movq	%r15, %rdi
	call	EC_GROUP_get_curve_name@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L46
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	leaq	.LC3(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L46
	xorl	%eax, %eax
	leaq	.LC4(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L46
	movl	%r14d, %edi
	call	EC_curve_nid2nist@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L45
	movl	$128, %edx
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	BIO_indent@PLT
	testl	%eax, %eax
	je	.L46
	movq	%r15, %rdx
	leaq	.LC5(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	movq	$0, -200(%rbp)
	testl	%eax, %eax
	jle	.L76
.L77:
	movl	$1, %r13d
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L48:
	movq	$0, -200(%rbp)
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	movl	$65, %edx
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L49:
	movq	%r14, %r15
	movl	$65, %edx
	xorl	%r14d, %r14d
	movq	$0, -200(%rbp)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L81:
	movq	%r15, %rdi
	call	EC_GROUP_get0_generator@PLT
	testq	%rax, %rax
	movq	%rax, -232(%rbp)
	je	.L54
	movq	%r15, %rdi
	call	EC_GROUP_get0_order@PLT
	movq	%r15, %rdi
	movq	%rax, -240(%rbp)
	call	EC_GROUP_get0_cofactor@PLT
	cmpq	$0, -240(%rbp)
	movq	%rax, -248(%rbp)
	je	.L54
	movq	%r15, %rdi
	call	EC_GROUP_get_point_conversion_form@PLT
	movq	%r12, %r8
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movq	-232(%rbp), %rsi
	movl	%eax, %edx
	movl	%eax, -216(%rbp)
	call	EC_POINT_point2bn@PLT
	movq	%rax, -232(%rbp)
	testq	%rax, %rax
	je	.L54
	movq	%r15, %rdi
	call	EC_GROUP_get0_seed@PLT
	movq	$0, -256(%rbp)
	movq	%rax, -264(%rbp)
	testq	%rax, %rax
	je	.L16
	movq	%r15, %rdi
	call	EC_GROUP_get_seed_len@PLT
	movq	%rax, -256(%rbp)
.L16:
	movl	-208(%rbp), %esi
	movl	$128, %edx
	movq	%r13, %rdi
	call	BIO_indent@PLT
	testl	%eax, %eax
	je	.L69
	movl	-224(%rbp), %edi
	call	OBJ_nid2sn@PLT
	leaq	.LC6(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L69
	cmpl	$407, -224(%rbp)
	jne	.L17
	movq	%r15, %rdi
	call	EC_GROUP_get_basis_type@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L69
	movl	-208(%rbp), %esi
	movl	$128, %edx
	movq	%r13, %rdi
	call	BIO_indent@PLT
	testl	%eax, %eax
	je	.L69
	movl	%r15d, %edi
	call	OBJ_nid2sn@PLT
	leaq	.LC7(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L69
	movl	-208(%rbp), %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r13, %rdi
	leaq	.LC8(%rip), %rsi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L69
.L19:
	movl	-208(%rbp), %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%r13, %rdi
	leaq	.LC9(%rip), %rsi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L69
	movl	-208(%rbp), %r8d
	movq	-200(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	leaq	.LC11(%rip), %rsi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L69
	xorl	%ecx, %ecx
	cmpl	$2, -216(%rbp)
	movl	-208(%rbp), %r8d
	movq	-232(%rbp), %rdx
	je	.L82
	cmpl	$4, -216(%rbp)
	je	.L83
	leaq	.LC15(%rip), %rsi
	movq	%r13, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L69
.L23:
	movl	-208(%rbp), %r8d
	movq	-240(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	leaq	.LC13(%rip), %rsi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L69
	cmpq	$0, -248(%rbp)
	je	.L26
	movl	-208(%rbp), %r8d
	movq	-248(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	leaq	.LC16(%rip), %rsi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	je	.L69
.L26:
	cmpq	$0, -264(%rbp)
	je	.L84
	movl	-208(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.L27
	cmpl	$128, %ecx
	movl	$128, %eax
	movl	$32, %esi
	cmovle	%ecx, %eax
	leaq	-192(%rbp), %r8
	movl	$133, %ecx
	movq	%r8, %rdi
	movslq	%eax, %rdx
	movl	%eax, %r15d
	movl	%eax, -212(%rbp)
	call	__memset_chk@PLT
	movl	%r15d, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	BIO_write@PLT
	testl	%eax, %eax
	jle	.L69
.L27:
	xorl	%eax, %eax
	leaq	.LC17(%rip), %rdx
	leaq	.LC18(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L69
	movl	-212(%rbp), %eax
	xorl	%r8d, %r8d
	movq	%rbx, -208(%rbp)
	movq	%r8, %rbx
	addl	$4, %eax
	movslq	%eax, %r15
	jmp	.L30
.L35:
	addq	$1, %rbx
	leaq	.LC1(%rip), %rax
	cmpq	%rbx, -256(%rbp)
	movq	%r13, %rdi
	leaq	.LC2(%rip), %rcx
	leaq	.LC19(%rip), %rsi
	cmove	%rax, %rcx
	movq	-264(%rbp), %rax
	movzbl	-1(%rax,%rbx), %edx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L78
.L30:
	cmpq	%rbx, -256(%rbp)
	je	.L85
	movl	$15, %ecx
	movq	%rbx, %rax
	xorl	%edx, %edx
	divq	%rcx
	testq	%rdx, %rdx
	jne	.L35
	leaq	-192(%rbp), %r9
	leaq	-191(%rbp), %rdi
	movq	%r15, %rdx
	movl	$32, %esi
	movl	$132, %ecx
	movq	%r9, -224(%rbp)
	movb	$10, -192(%rbp)
	call	__memset_chk@PLT
	movl	-212(%rbp), %eax
	movq	-224(%rbp), %r9
	movq	%r13, %rdi
	leal	5(%rax), %edx
	movq	%r9, %rsi
	call	BIO_write@PLT
	testl	%eax, %eax
	jg	.L35
.L78:
	movq	-208(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L69:
	movq	%r14, %r15
	movq	-232(%rbp), %r14
.L76:
	movl	$32, %edx
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L50:
	movq	%r14, %r15
	movl	$65, %edx
	xorl	%r14d, %r14d
	jmp	.L13
.L45:
	movq	$0, -200(%rbp)
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L77
.L17:
	movl	-208(%rbp), %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r13, %rdi
	leaq	.LC10(%rip), %rsi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	jne	.L19
	jmp	.L69
.L80:
	call	__stack_chk_fail@PLT
.L82:
	leaq	.LC12(%rip), %rsi
	movq	%r13, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	jne	.L23
	jmp	.L69
.L84:
	movq	%r14, %r15
	movl	$1, %r13d
	movq	-232(%rbp), %r14
	jmp	.L15
.L83:
	leaq	.LC14(%rip), %rsi
	movq	%r13, %rdi
	call	ASN1_bn_print@PLT
	testl	%eax, %eax
	jne	.L23
	jmp	.L69
.L85:
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%r14, %r15
	movq	-208(%rbp), %rbx
	leaq	.LC4(%rip), %rsi
	call	BIO_write@PLT
	movq	-232(%rbp), %r14
	testl	%eax, %eax
	jg	.L77
	jmp	.L76
	.cfi_endproc
.LFE422:
	.size	ECPKParameters_print, .-ECPKParameters_print
	.p2align 4
	.globl	ECPKParameters_print_fp
	.type	ECPKParameters_print_fp, @function
ECPKParameters_print_fp:
.LFB419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	testq	%rax, %rax
	je	.L90
	movq	%rax, %r12
	movq	%r15, %rcx
	xorl	%edx, %edx
	movl	$106, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	ECPKParameters_print
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	BIO_free@PLT
	popq	%r12
	movl	%r13d, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	xorl	%r13d, %r13d
	movl	$24, %r8d
	movl	$7, %edx
	movl	$150, %esi
	leaq	.LC0(%rip), %rcx
	movl	$16, %edi
	call	ERR_put_error@PLT
	popq	%r12
	movl	%r13d, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE419:
	.size	ECPKParameters_print_fp, .-ECPKParameters_print_fp
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
