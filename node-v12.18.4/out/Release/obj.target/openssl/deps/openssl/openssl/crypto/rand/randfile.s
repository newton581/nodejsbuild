	.file	"randfile.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"rb"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../deps/openssl/openssl/crypto/rand/randfile.c"
	.section	.rodata.str1.1
.LC2:
	.string	"Filename="
	.text
	.p2align 4
	.globl	RAND_load_file
	.type	RAND_load_file, @function
RAND_load_file:
.LFB259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1464, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -1496(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	jne	.L25
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L26
	addq	$1464, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	movq	%rsi, %r12
	leaq	.LC0(%rip), %rsi
	call	openssl_fopen@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L27
	movq	%rax, %rdi
	call	fileno@PLT
	leaq	-1488(%rbp), %rdx
	movl	$1, %edi
	movl	%eax, %esi
	call	__fxstat@PLT
	testl	%eax, %eax
	js	.L28
	testq	%r12, %r12
	jns	.L5
	movl	-1464(%rbp), %eax
	movl	$256, %r12d
	andl	$61440, %eax
	cmpl	$32768, %eax
	cmove	-1440(%rbp), %r12
.L5:
	xorl	%esi, %esi
	movq	%r13, %rdi
	xorl	%r14d, %r14d
	call	setbuf@PLT
	leaq	-1344(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$1280, %ecx
	testq	%r12, %r12
	jle	.L7
	cmpq	$1280, %r12
	movl	$1024, %ecx
	cmovle	%r12, %rcx
.L7:
	movq	%r15, %rdi
	movq	%r13, %r8
	movl	$1, %edx
	movl	$1280, %esi
	call	__fread_chk@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	ferror@PLT
	testl	%eax, %eax
	je	.L8
	call	__errno_location@PLT
	cmpl	$4, (%rax)
	je	.L29
.L8:
	testl	%ebx, %ebx
	je	.L11
.L12:
	pxor	%xmm0, %xmm0
	movl	%ebx, %esi
	movq	%r15, %rdi
	addl	%ebx, %r14d
	cvtsi2sdl	%ebx, %xmm0
	call	RAND_add@PLT
	testq	%r12, %r12
	jle	.L6
	movslq	%ebx, %rbx
	subq	%rbx, %r12
	testq	%r12, %r12
	jg	.L6
.L11:
	movl	$1280, %esi
	movq	%r15, %rdi
	call	OPENSSL_cleanse@PLT
	movq	%r13, %rdi
	call	fclose@PLT
	call	RAND_status@PLT
	testl	%eax, %eax
	jne	.L1
	movl	$166, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$118, %edx
.L23:
	movl	$111, %esi
	movl	$36, %edi
	movl	$-1, %r14d
	call	ERR_put_error@PLT
	movq	-1496(%rbp), %rdx
	leaq	.LC2(%rip), %rsi
	xorl	%eax, %eax
	movl	$2, %edi
	call	ERR_add_error_data@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L29:
	movq	%r13, %rdi
	call	clearerr@PLT
	testl	%ebx, %ebx
	jne	.L12
	jmp	.L6
.L27:
	movl	$98, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$121, %edx
	jmp	.L23
.L28:
	movl	$105, %r8d
	movl	$113, %edx
	movl	$111, %esi
	movl	$36, %edi
	leaq	.LC1(%rip), %rcx
	movl	$-1, %r14d
	call	ERR_put_error@PLT
	movq	-1496(%rbp), %rdx
	leaq	.LC2(%rip), %rsi
	xorl	%eax, %eax
	movl	$2, %edi
	call	ERR_add_error_data@PLT
	movq	%r13, %rdi
	call	fclose@PLT
	jmp	.L1
.L26:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE259:
	.size	RAND_load_file, .-RAND_load_file
	.section	.rodata.str1.1
.LC3:
	.string	"wb"
	.text
	.p2align 4
	.globl	RAND_write_file
	.type	RAND_write_file, @function
RAND_write_file:
.LFB260:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	leaq	-1216(%rbp), %rdx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movl	$1, %edi
	pushq	%r12
	subq	$1192, %rsp
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	__xstat@PLT
	testl	%eax, %eax
	js	.L31
	movl	-1192(%rbp), %eax
	andl	$61440, %eax
	cmpl	$32768, %eax
	jne	.L47
.L31:
	leaq	-1072(%rbp), %r14
	movl	$1024, %esi
	movq	%r14, %rdi
	call	RAND_priv_bytes@PLT
	cmpl	$1, %eax
	jne	.L38
	movq	%r13, %rdi
	movl	$384, %edx
	movl	$65, %esi
	xorl	%eax, %eax
	call	open@PLT
	movl	%eax, %edi
	cmpl	$-1, %eax
	je	.L36
	leaq	.LC3(%rip), %rsi
	call	fdopen@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L36
.L35:
	movq	%r13, %rdi
	movl	$384, %esi
	call	chmod@PLT
	movl	$1, %esi
	movq	%r12, %rcx
	movq	%r14, %rdi
	movl	$1024, %edx
	call	fwrite@PLT
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	fclose@PLT
	movl	$1024, %esi
	movq	%r14, %rdi
	call	OPENSSL_cleanse@PLT
.L30:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L48
	addq	$1192, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	leaq	.LC3(%rip), %rsi
	movq	%r13, %rdi
	call	openssl_fopen@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L35
	movl	$233, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$121, %edx
.L46:
	movl	$112, %esi
	movl	$36, %edi
	call	ERR_put_error@PLT
	movq	%r13, %rdx
	movl	$2, %edi
	xorl	%eax, %eax
	leaq	.LC2(%rip), %rsi
	movl	$-1, %r13d
	call	ERR_add_error_data@PLT
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L38:
	movl	$-1, %r13d
	jmp	.L30
.L47:
	movl	$183, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$122, %edx
	jmp	.L46
.L48:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE260:
	.size	RAND_write_file, .-RAND_write_file
	.section	.rodata.str1.1
.LC4:
	.string	"RANDFILE"
.LC5:
	.string	"HOME"
	.text
	.p2align 4
	.globl	RAND_file_name
	.type	RAND_file_name, @function
RAND_file_name:
.LFB261:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	leaq	.LC4(%rip), %rdi
	call	ossl_safe_getenv@PLT
	testq	%rax, %rax
	je	.L50
	cmpb	$0, (%rax)
	movq	%rax, %r12
	jne	.L51
.L50:
	leaq	.LC5(%rip), %rdi
	call	ossl_safe_getenv@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L54
	cmpb	$0, (%rax)
	je	.L54
	movq	%rax, %rdi
	call	strlen@PLT
	movq	%rax, %r14
	leaq	6(%rax), %rax
	cmpq	%r13, %rax
	jnb	.L54
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	memcpy@PLT
	leaq	1(%rbx,%r14), %rax
	movb	$47, (%rbx,%r14)
	movl	$1684959790, (%rax)
	movb	$0, 4(%rax)
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	movq	%rax, %rdi
	call	strlen@PLT
	leaq	1(%rax), %rdx
	cmpq	%r13, %rdx
	jnb	.L54
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	memcpy@PLT
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE261:
	.size	RAND_file_name, .-RAND_file_name
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
