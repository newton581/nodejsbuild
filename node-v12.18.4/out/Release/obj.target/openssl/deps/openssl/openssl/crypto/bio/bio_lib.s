	.file	"bio_lib.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/bio/bio_lib.c"
	.text
	.p2align 4
	.globl	BIO_new
	.type	BIO_new, @function
BIO_new:
.LFB268:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$73, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$120, %edi
	subq	$8, %rsp
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L14
	movq	%rbx, (%rax)
	leaq	104(%rax), %r13
	movq	%rax, %rsi
	movl	$12, %edi
	movl	$1, 36(%rax)
	movq	%r13, %rdx
	movl	$1, 80(%rax)
	mfence
	call	CRYPTO_new_ex_data@PLT
	testl	%eax, %eax
	je	.L4
	call	CRYPTO_THREAD_lock_new@PLT
	movq	%rax, 112(%r12)
	testq	%rax, %rax
	je	.L15
	movq	72(%rbx), %rax
	testq	%rax, %rax
	je	.L8
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L16
	cmpq	$0, 72(%rbx)
	je	.L8
.L1:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	movl	$65, %edx
	movl	$108, %esi
	movl	$32, %edi
	movl	$89, %r8d
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	movl	$12, %edi
	call	CRYPTO_free_ex_data@PLT
.L4:
	movq	%r12, %rdi
	movl	$106, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%r12d, %r12d
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movl	$1, 32(%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	movl	$76, %r8d
	movl	$65, %edx
	movl	$108, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L16:
	movl	$95, %r8d
	movl	$70, %edx
	movl	$108, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	movl	$12, %edi
	call	CRYPTO_free_ex_data@PLT
	movq	112(%r12), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	jmp	.L4
	.cfi_endproc
.LFE268:
	.size	BIO_new, .-BIO_new
	.p2align 4
	.globl	BIO_free
	.type	BIO_free, @function
BIO_free:
.LFB269:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L35
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	lock xaddl	%eax, 80(%rdi)
	subl	$1, %eax
	testl	%eax, %eax
	je	.L38
	movl	$1, %eax
	jle	.L20
.L17:
	movq	-8(%rbp), %r12
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
.L20:
	movq	8(%r12), %r10
	movq	16(%r12), %rax
	testq	%r10, %r10
	je	.L39
	testq	%rax, %rax
	jne	.L22
	xorl	%edx, %edx
	movl	$1, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %esi
	movq	%r12, %rdi
	call	*%r10
	movq	%rax, %rdx
.L26:
	movl	%edx, %eax
	testl	%edx, %edx
	jle	.L17
.L27:
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L24
	movq	80(%rax), %rax
	testq	%rax, %rax
	je	.L24
	movq	%r12, %rdi
	call	*%rax
.L24:
	leaq	104(%r12), %rdx
	movq	%r12, %rsi
	movl	$12, %edi
	call	CRYPTO_free_ex_data@PLT
	movq	112(%r12), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	movq	%r12, %rdi
	movl	$138, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-8(%rbp), %r12
	movl	$1, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore 6
	.cfi_restore 12
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	testq	%rax, %rax
	je	.L27
.L22:
	pushq	$0
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r9d, %r9d
	pushq	$1
	xorl	%r8d, %r8d
	movl	$1, %esi
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %rdx
	popq	%rax
	popq	%rcx
	jmp	.L26
	.cfi_endproc
.LFE269:
	.size	BIO_free, .-BIO_free
	.p2align 4
	.globl	BIO_set_data
	.type	BIO_set_data, @function
BIO_set_data:
.LFB270:
	.cfi_startproc
	endbr64
	movq	%rsi, 56(%rdi)
	ret
	.cfi_endproc
.LFE270:
	.size	BIO_set_data, .-BIO_set_data
	.p2align 4
	.globl	BIO_get_data
	.type	BIO_get_data, @function
BIO_get_data:
.LFB271:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rax
	ret
	.cfi_endproc
.LFE271:
	.size	BIO_get_data, .-BIO_get_data
	.p2align 4
	.globl	BIO_set_init
	.type	BIO_set_init, @function
BIO_set_init:
.LFB272:
	.cfi_startproc
	endbr64
	movl	%esi, 32(%rdi)
	ret
	.cfi_endproc
.LFE272:
	.size	BIO_set_init, .-BIO_set_init
	.p2align 4
	.globl	BIO_get_init
	.type	BIO_get_init, @function
BIO_get_init:
.LFB273:
	.cfi_startproc
	endbr64
	movl	32(%rdi), %eax
	ret
	.cfi_endproc
.LFE273:
	.size	BIO_get_init, .-BIO_get_init
	.p2align 4
	.globl	BIO_set_shutdown
	.type	BIO_set_shutdown, @function
BIO_set_shutdown:
.LFB274:
	.cfi_startproc
	endbr64
	movl	%esi, 36(%rdi)
	ret
	.cfi_endproc
.LFE274:
	.size	BIO_set_shutdown, .-BIO_set_shutdown
	.p2align 4
	.globl	BIO_get_shutdown
	.type	BIO_get_shutdown, @function
BIO_get_shutdown:
.LFB275:
	.cfi_startproc
	endbr64
	movl	36(%rdi), %eax
	ret
	.cfi_endproc
.LFE275:
	.size	BIO_get_shutdown, .-BIO_get_shutdown
	.p2align 4
	.globl	BIO_vfree
	.type	BIO_vfree, @function
BIO_vfree:
.LFB276:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L63
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	lock xaddl	%eax, 80(%rdi)
	subl	$1, %eax
	testl	%eax, %eax
	je	.L66
	jle	.L50
.L46:
	movq	-8(%rbp), %r12
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
.L50:
	movq	8(%r12), %r10
	movq	16(%r12), %rax
	testq	%r10, %r10
	je	.L67
	testq	%rax, %rax
	jne	.L53
	movl	$1, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%r12, %rdi
	call	*%r10
.L57:
	testl	%eax, %eax
	jle	.L46
.L58:
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L55
	movq	80(%rax), %rax
	testq	%rax, %rax
	je	.L55
	movq	%r12, %rdi
	call	*%rax
.L55:
	leaq	104(%r12), %rdx
	movq	%r12, %rsi
	movl	$12, %edi
	call	CRYPTO_free_ex_data@PLT
	movq	112(%r12), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	movq	%r12, %rdi
	movq	-8(%rbp), %r12
	movl	$138, %edx
	leave
	.cfi_restore 6
	.cfi_restore 12
	.cfi_def_cfa 7, 8
	leaq	.LC0(%rip), %rsi
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L63:
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	testq	%rax, %rax
	je	.L58
.L53:
	pushq	$0
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r9d, %r9d
	pushq	$1
	xorl	%r8d, %r8d
	movl	$1, %esi
	movq	%r12, %rdi
	call	*%rax
	popq	%rdx
	popq	%rcx
	jmp	.L57
	.cfi_endproc
.LFE276:
	.size	BIO_vfree, .-BIO_vfree
	.p2align 4
	.globl	BIO_up_ref
	.type	BIO_up_ref, @function
BIO_up_ref:
.LFB277:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	lock xaddl	%eax, 80(%rdi)
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE277:
	.size	BIO_up_ref, .-BIO_up_ref
	.p2align 4
	.globl	BIO_clear_flags
	.type	BIO_clear_flags, @function
BIO_clear_flags:
.LFB278:
	.cfi_startproc
	endbr64
	notl	%esi
	andl	%esi, 40(%rdi)
	ret
	.cfi_endproc
.LFE278:
	.size	BIO_clear_flags, .-BIO_clear_flags
	.p2align 4
	.globl	BIO_test_flags
	.type	BIO_test_flags, @function
BIO_test_flags:
.LFB279:
	.cfi_startproc
	endbr64
	movl	40(%rdi), %eax
	andl	%esi, %eax
	ret
	.cfi_endproc
.LFE279:
	.size	BIO_test_flags, .-BIO_test_flags
	.p2align 4
	.globl	BIO_set_flags
	.type	BIO_set_flags, @function
BIO_set_flags:
.LFB280:
	.cfi_startproc
	endbr64
	orl	%esi, 40(%rdi)
	ret
	.cfi_endproc
.LFE280:
	.size	BIO_set_flags, .-BIO_set_flags
	.p2align 4
	.globl	BIO_get_callback
	.type	BIO_get_callback, @function
BIO_get_callback:
.LFB281:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE281:
	.size	BIO_get_callback, .-BIO_get_callback
	.p2align 4
	.globl	BIO_set_callback
	.type	BIO_set_callback, @function
BIO_set_callback:
.LFB282:
	.cfi_startproc
	endbr64
	movq	%rsi, 8(%rdi)
	ret
	.cfi_endproc
.LFE282:
	.size	BIO_set_callback, .-BIO_set_callback
	.p2align 4
	.globl	BIO_get_callback_ex
	.type	BIO_get_callback_ex, @function
BIO_get_callback_ex:
.LFB283:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE283:
	.size	BIO_get_callback_ex, .-BIO_get_callback_ex
	.p2align 4
	.globl	BIO_set_callback_ex
	.type	BIO_set_callback_ex, @function
BIO_set_callback_ex:
.LFB284:
	.cfi_startproc
	endbr64
	movq	%rsi, 16(%rdi)
	ret
	.cfi_endproc
.LFE284:
	.size	BIO_set_callback_ex, .-BIO_set_callback_ex
	.p2align 4
	.globl	BIO_set_callback_arg
	.type	BIO_set_callback_arg, @function
BIO_set_callback_arg:
.LFB285:
	.cfi_startproc
	endbr64
	movq	%rsi, 24(%rdi)
	ret
	.cfi_endproc
.LFE285:
	.size	BIO_set_callback_arg, .-BIO_set_callback_arg
	.p2align 4
	.globl	BIO_get_callback_arg
	.type	BIO_get_callback_arg, @function
BIO_get_callback_arg:
.LFB286:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE286:
	.size	BIO_get_callback_arg, .-BIO_get_callback_arg
	.p2align 4
	.globl	BIO_method_name
	.type	BIO_method_name, @function
BIO_method_name:
.LFB287:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	ret
	.cfi_endproc
.LFE287:
	.size	BIO_method_name, .-BIO_method_name
	.p2align 4
	.globl	BIO_method_type
	.type	BIO_method_type, @function
BIO_method_type:
.LFB288:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	(%rax), %eax
	ret
	.cfi_endproc
.LFE288:
	.size	BIO_method_type, .-BIO_method_type
	.p2align 4
	.globl	BIO_read
	.type	BIO_read, @function
BIO_read:
.LFB290:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	js	.L80
	movq	%rdi, %r12
	movl	%edx, %r13d
	movslq	%edx, %rbx
	testq	%rdi, %rdi
	je	.L82
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L82
	cmpq	$0, 32(%rax)
	je	.L82
	movq	8(%rdi), %r10
	movq	16(%rdi), %rax
	movq	%rsi, %r14
	testq	%r10, %r10
	je	.L117
	testq	%rax, %rax
	je	.L118
.L85:
	pushq	$0
	movq	%r14, %rdx
	movl	$2, %esi
	movq	%r12, %rdi
	pushq	$1
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	call	*%rax
	popq	%rsi
	popq	%rdi
	movq	%rax, %rdx
.L88:
	movl	%edx, %eax
	testl	%edx, %edx
	jle	.L80
.L89:
	movl	32(%r12), %r8d
	testl	%r8d, %r8d
	je	.L119
	movq	(%r12), %rax
	leaq	-64(%rbp), %r15
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r15, %rcx
	movq	%r12, %rdi
	call	*32(%rax)
	testl	%eax, %eax
	jle	.L90
	movq	-64(%rbp), %rdx
	addq	%rdx, 88(%r12)
.L90:
	movq	8(%r12), %r10
	movq	16(%r12), %r11
	testq	%r10, %r10
	je	.L120
	testq	%r11, %r11
	je	.L121
.L98:
	pushq	%r15
	movq	%rbx, %rcx
	movq	%r14, %rdx
	xorl	%r9d, %r9d
	pushq	%rax
	xorl	%r8d, %r8d
	movl	$130, %esi
	movq	%r12, %rdi
	call	*%r11
	popq	%rdx
	popq	%rcx
.L92:
	testl	%eax, %eax
	jle	.L80
.L96:
	movq	-64(%rbp), %rax
	cmpq	%rax, %rbx
	jb	.L122
.L80:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L123
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	.cfi_restore_state
	testq	%rax, %rax
	jne	.L85
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L118:
	movq	%r14, %rdx
	movl	$1, %r9d
	xorl	%r8d, %r8d
	movl	%r13d, %ecx
	movl	$2, %esi
	movq	%r12, %rdi
	call	*%r10
	movq	%rax, %rdx
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L120:
	testq	%r11, %r11
	jne	.L98
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L121:
	movslq	%eax, %r9
	testq	%r9, %r9
	jle	.L94
	movq	-64(%rbp), %rax
	cmpq	$2147483647, %rax
	ja	.L103
	movq	%rax, %r9
.L94:
	xorl	%r8d, %r8d
	movl	%r13d, %ecx
	movq	%r14, %rdx
	movl	$130, %esi
	movq	%r12, %rdi
	call	*%r10
	testq	%rax, %rax
	jle	.L92
	movq	%rax, -64(%rbp)
	jmp	.L96
.L122:
	movl	$281, %r8d
	movl	$68, %edx
	movl	$120, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L80
.L103:
	orl	$-1, %eax
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L82:
	movl	$256, %r8d
	movl	$121, %edx
	movl	$120, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	jmp	.L80
.L119:
	movl	$266, %r8d
	movl	$120, %edx
	movl	$120, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	jmp	.L80
.L123:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE290:
	.size	BIO_read, .-BIO_read
	.p2align 4
	.globl	BIO_read_ex
	.type	BIO_read_ex, @function
BIO_read_ex:
.LFB291:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testq	%rdi, %rdi
	je	.L125
	movq	(%rdi), %rax
	movq	%rdi, %r12
	testq	%rax, %rax
	je	.L125
	cmpq	$0, 32(%rax)
	je	.L125
	movq	8(%rdi), %r10
	movq	16(%rdi), %rax
	movq	%rsi, %r14
	movq	%rdx, %r13
	movq	%rcx, %rbx
	testq	%r10, %r10
	je	.L156
	testq	%rax, %rax
	jne	.L129
	cmpq	$2147483647, %rdx
	ja	.L155
	xorl	%r8d, %r8d
	movl	%edx, %ecx
	movl	$1, %r9d
	movq	%rsi, %rdx
	movl	$2, %esi
	call	*%r10
	testl	%eax, %eax
	jg	.L136
	.p2align 4,,10
	.p2align 3
.L155:
	xorl	%eax, %eax
.L160:
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L156:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L136
.L129:
	pushq	$0
	movl	$2, %esi
	movq	%r12, %rdi
	xorl	%r9d, %r9d
	pushq	$1
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r14, %rdx
	call	*%rax
	popq	%rsi
	popq	%rdi
	testl	%eax, %eax
	jle	.L155
.L136:
	movl	32(%r12), %eax
	testl	%eax, %eax
	je	.L157
	movq	(%r12), %rax
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*32(%rax)
	testl	%eax, %eax
	jle	.L137
	movq	(%rbx), %rdx
	addq	%rdx, 88(%r12)
.L137:
	movq	8(%r12), %r10
	movq	16(%r12), %r11
	testq	%r10, %r10
	je	.L158
	testq	%r11, %r11
	jne	.L139
	movl	$2147483648, %edx
	cmpq	%rdx, %r13
	jnb	.L155
	movslq	%eax, %r9
	testq	%r9, %r9
	jle	.L143
	movq	(%rbx), %rax
	cmpq	%rdx, %rax
	jnb	.L155
	movq	%rax, %r9
.L143:
	xorl	%r8d, %r8d
	movl	%r13d, %ecx
	movq	%r14, %rdx
	movl	$130, %esi
	movq	%r12, %rdi
	call	*%r10
	testq	%rax, %rax
	jle	.L140
	movq	%rax, (%rbx)
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L158:
	testq	%r11, %r11
	je	.L140
.L139:
	pushq	%rbx
	movq	%r13, %rcx
	movq	%r14, %rdx
	xorl	%r9d, %r9d
	pushq	%rax
	xorl	%r8d, %r8d
	movl	$130, %esi
	movq	%r12, %rdi
	call	*%r11
	popq	%rdx
	popq	%rcx
.L140:
	testl	%eax, %eax
	jle	.L155
.L145:
	cmpq	(%rbx), %r13
	jb	.L159
	movl	$1, %eax
.L124:
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_restore_state
	movl	$256, %r8d
	movl	$121, %edx
	movl	$120, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L124
.L157:
	movl	$266, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$120, %edx
	movl	%eax, -36(%rbp)
	movl	$120, %esi
	movl	$32, %edi
	call	ERR_put_error@PLT
	movl	-36(%rbp), %eax
	jmp	.L124
.L159:
	movl	$281, %r8d
	movl	$68, %edx
	movl	$120, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L160
	.cfi_endproc
.LFE291:
	.size	BIO_read_ex, .-BIO_read_ex
	.p2align 4
	.globl	BIO_write
	.type	BIO_write, @function
BIO_write:
.LFB293:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	js	.L161
	movq	%rdi, %r12
	movl	%edx, %r13d
	movslq	%edx, %r15
	testq	%rdi, %rdi
	je	.L161
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L163
	cmpq	$0, 16(%rax)
	je	.L163
	movq	8(%rdi), %r10
	movq	16(%rdi), %rax
	movq	%rsi, %r14
	testq	%r10, %r10
	je	.L195
	testq	%rax, %rax
	je	.L196
.L166:
	pushq	$0
	movq	%r14, %rdx
	movl	$3, %esi
	movq	%r12, %rdi
	pushq	$1
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	call	*%rax
	popq	%rsi
	popq	%rdi
	movq	%rax, %rdx
.L169:
	movl	%edx, %eax
	testl	%edx, %edx
	jle	.L161
.L170:
	movl	32(%r12), %r8d
	testl	%r8d, %r8d
	je	.L197
	movq	(%r12), %rax
	leaq	-64(%rbp), %rbx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rcx
	movq	%r12, %rdi
	call	*16(%rax)
	testl	%eax, %eax
	jle	.L171
	movq	-64(%rbp), %rdx
	addq	%rdx, 96(%r12)
.L171:
	movq	8(%r12), %r10
	movq	16(%r12), %r11
	testq	%r10, %r10
	je	.L198
	testq	%r11, %r11
	je	.L199
.L178:
	pushq	%rbx
	movq	%r15, %rcx
	movq	%r14, %rdx
	xorl	%r9d, %r9d
	pushq	%rax
	xorl	%r8d, %r8d
	movl	$131, %esi
	movq	%r12, %rdi
	call	*%r11
	popq	%rdx
	popq	%rcx
.L173:
	testl	%eax, %eax
	jle	.L161
.L177:
	movl	-64(%rbp), %eax
.L161:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L200
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L195:
	.cfi_restore_state
	testq	%rax, %rax
	jne	.L166
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L198:
	testq	%r11, %r11
	jne	.L178
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L196:
	movq	%r14, %rdx
	movl	$1, %r9d
	xorl	%r8d, %r8d
	movl	%r13d, %ecx
	movl	$3, %esi
	movq	%r12, %rdi
	call	*%r10
	movq	%rax, %rdx
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L199:
	movslq	%eax, %r9
	testq	%r9, %r9
	jle	.L175
	movq	-64(%rbp), %rax
	cmpq	$2147483647, %rax
	ja	.L184
	movq	%rax, %r9
.L175:
	xorl	%r8d, %r8d
	movl	%r13d, %ecx
	movq	%r14, %rdx
	movl	$131, %esi
	movq	%r12, %rdi
	call	*%r10
	testq	%rax, %rax
	jle	.L173
	movq	%rax, -64(%rbp)
	jmp	.L177
.L184:
	orl	$-1, %eax
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L163:
	movl	$329, %r8d
	movl	$121, %edx
	movl	$128, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	jmp	.L161
.L197:
	movl	$339, %r8d
	movl	$120, %edx
	movl	$128, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	jmp	.L161
.L200:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE293:
	.size	BIO_write, .-BIO_write
	.p2align 4
	.globl	BIO_write_ex
	.type	BIO_write_ex, @function
BIO_write_ex:
.LFB294:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L232
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L204
	cmpq	$0, 16(%rax)
	je	.L204
	movq	8(%rdi), %r10
	movq	16(%rdi), %rax
	movq	%rsi, %r14
	movq	%rdx, %r13
	movq	%rcx, %rbx
	testq	%r10, %r10
	je	.L233
	testq	%rax, %rax
	jne	.L207
	cmpq	$2147483647, %rdx
	ja	.L223
	xorl	%r8d, %r8d
	movl	%edx, %ecx
	movl	$1, %r9d
	movq	%rsi, %rdx
	movl	$3, %esi
	call	*%r10
	testl	%eax, %eax
	jg	.L213
.L223:
	xorl	%eax, %eax
.L201:
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L233:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L213
.L207:
	pushq	$0
	movl	$3, %esi
	movq	%r12, %rdi
	xorl	%r9d, %r9d
	pushq	$1
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r14, %rdx
	call	*%rax
	popq	%rsi
	popq	%rdi
	testl	%eax, %eax
	jle	.L223
.L213:
	movl	32(%r12), %eax
	testl	%eax, %eax
	je	.L234
	movq	(%r12), %rax
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*16(%rax)
	testl	%eax, %eax
	jle	.L214
	movq	(%rbx), %rdx
	addq	%rdx, 96(%r12)
.L214:
	movq	8(%r12), %r10
	movq	16(%r12), %r11
	testq	%r10, %r10
	je	.L235
	testq	%r11, %r11
	jne	.L216
	movl	$2147483648, %edx
	cmpq	%rdx, %r13
	jnb	.L223
	movslq	%eax, %r9
	testq	%r9, %r9
	jle	.L220
	movq	(%rbx), %rax
	cmpq	%rdx, %rax
	jnb	.L223
	movq	%rax, %r9
.L220:
	xorl	%r8d, %r8d
	movl	%r13d, %ecx
	movq	%r14, %rdx
	movl	$131, %esi
	movq	%r12, %rdi
	call	*%r10
	testq	%rax, %rax
	jle	.L231
	movq	%rax, (%rbx)
	movl	$1, %eax
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L235:
	testq	%r11, %r11
	je	.L231
.L216:
	pushq	%rbx
	movq	%r13, %rcx
	movq	%r14, %rdx
	xorl	%r9d, %r9d
	pushq	%rax
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movl	$131, %esi
	call	*%r11
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	setg	%al
	leaq	-32(%rbp), %rsp
	popq	%rbx
	movzbl	%al, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L231:
	.cfi_restore_state
	testl	%eax, %eax
	setg	%al
	leaq	-32(%rbp), %rsp
	popq	%rbx
	movzbl	%al, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L232:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%eax, %eax
	ret
.L204:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movl	$329, %r8d
	movl	$121, %edx
	movl	$128, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L201
.L234:
	movl	$339, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$120, %edx
	movl	%eax, -36(%rbp)
	movl	$128, %esi
	movl	$32, %edi
	call	ERR_put_error@PLT
	movl	-36(%rbp), %eax
	jmp	.L201
	.cfi_endproc
.LFE294:
	.size	BIO_write_ex, .-BIO_write_ex
	.p2align 4
	.globl	BIO_puts
	.type	BIO_puts, @function
BIO_puts:
.LFB295:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$0, -32(%rbp)
	testq	%rdi, %rdi
	je	.L237
	movq	(%rdi), %rax
	movq	%rdi, %r12
	testq	%rax, %rax
	je	.L237
	cmpq	$0, 48(%rax)
	je	.L237
	movq	8(%rdi), %r10
	movq	16(%rdi), %rax
	movq	%rsi, %r13
	testq	%r10, %r10
	je	.L268
	testq	%rax, %rax
	jne	.L241
	movq	%rsi, %rdx
	movl	$1, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$4, %esi
	call	*%r10
	movq	%rax, %rdx
.L245:
	movl	%edx, %eax
	testl	%edx, %edx
	jle	.L236
.L246:
	movl	32(%r12), %edi
	testl	%edi, %edi
	je	.L269
	movq	(%r12), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*48(%rax)
	testl	%eax, %eax
	jle	.L247
	movq	8(%r12), %r11
	movslq	%eax, %r9
	movq	16(%r12), %r10
	addq	%r9, 96(%r12)
	movq	%r9, -32(%rbp)
	testq	%r11, %r11
	je	.L270
	testq	%r10, %r10
	jne	.L258
.L255:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$132, %esi
	movq	%r12, %rdi
	call	*%r11
	movq	%rax, %r9
	testq	%rax, %rax
	jle	.L253
	movq	%rax, -32(%rbp)
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L268:
	testq	%rax, %rax
	je	.L246
.L241:
	pushq	$0
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$4, %esi
	pushq	$1
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	call	*%rax
	popq	%rcx
	popq	%rsi
	movq	%rax, %rdx
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L258:
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L249:
	leaq	-32(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	pushq	%rdx
	movl	$132, %esi
	movq	%r13, %rdx
	movq	%r12, %rdi
	pushq	%rax
	call	*%r10
	movq	%rax, %r9
	popq	%rax
	popq	%rdx
.L253:
	movl	%r9d, %eax
	testl	%r9d, %r9d
	jle	.L236
	movq	-32(%rbp), %r9
.L254:
	cmpq	$2147483647, %r9
	ja	.L271
.L250:
	movl	%r9d, %eax
.L236:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L272
	leaq	-16(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L247:
	.cfi_restore_state
	movq	8(%r12), %r11
	movq	16(%r12), %r10
	testq	%r11, %r11
	je	.L273
	movslq	%eax, %r9
	testq	%r10, %r10
	je	.L255
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L270:
	movl	$1, %eax
	testq	%r10, %r10
	jne	.L249
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L273:
	testq	%r10, %r10
	jne	.L249
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L237:
	movl	$393, %r8d
	movl	$121, %edx
	movl	$110, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	jmp	.L236
.L271:
	movl	$422, %r8d
	movl	$102, %edx
	movl	$110, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L236
.L269:
	movl	$404, %r8d
	movl	$120, %edx
	movl	$110, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	jmp	.L236
.L272:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE295:
	.size	BIO_puts, .-BIO_puts
	.p2align 4
	.globl	BIO_gets
	.type	BIO_gets, @function
BIO_gets:
.LFB296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	testq	%rdi, %rdi
	je	.L275
	movq	(%rdi), %rax
	movq	%rdi, %r12
	testq	%rax, %rax
	je	.L275
	cmpq	$0, 56(%rax)
	je	.L275
	movl	%edx, %r13d
	testl	%edx, %edx
	js	.L313
	movq	8(%rdi), %r10
	movq	16(%rdi), %rax
	movq	%rsi, %r14
	movslq	%edx, %rcx
	testq	%r10, %r10
	je	.L314
	testq	%rax, %rax
	je	.L315
.L297:
	pushq	$0
	movq	%r14, %rdx
	movl	$5, %esi
	xorl	%r9d, %r9d
	pushq	$1
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	call	*%rax
	popq	%rcx
	popq	%rsi
	movq	%rax, %rdx
.L283:
	movl	%edx, %eax
	testl	%edx, %edx
	jle	.L274
.L284:
	movl	32(%r12), %edi
	testl	%edi, %edi
	je	.L316
	movq	(%r12), %rax
	movl	%r13d, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*56(%rax)
	testl	%eax, %eax
	jle	.L285
	movq	8(%r12), %r10
	movslq	%eax, %r9
	movq	16(%r12), %r11
	movslq	%r13d, %rbx
	movq	%r9, -48(%rbp)
	movl	$1, %eax
	testq	%r10, %r10
	je	.L317
	testq	%r11, %r11
	je	.L296
.L287:
	leaq	-48(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	pushq	%rdx
	movl	$133, %esi
	movq	%r14, %rdx
	movq	%r12, %rdi
	pushq	%rax
	call	*%r11
	movq	%rax, %r9
	popq	%rax
	popq	%rdx
.L291:
	movl	%r9d, %eax
	testl	%r9d, %r9d
	jle	.L274
	movq	-48(%rbp), %r9
.L288:
	cmpq	%r9, %rbx
	movl	$-1, %eax
	cmovnb	%r9d, %eax
.L274:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L318
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L314:
	.cfi_restore_state
	testq	%rax, %rax
	jne	.L297
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L285:
	movq	8(%r12), %r10
	movq	16(%r12), %r11
	movslq	%r13d, %rbx
	testq	%r10, %r10
	je	.L319
	testq	%r11, %r11
	jne	.L287
	movslq	%eax, %r9
.L296:
	xorl	%r8d, %r8d
	movl	%r13d, %ecx
	movq	%r14, %rdx
	movl	$133, %esi
	movq	%r12, %rdi
	call	*%r10
	movq	%rax, %r9
	testq	%rax, %rax
	jg	.L288
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L315:
	movq	%r14, %rdx
	movl	$1, %r9d
	xorl	%r8d, %r8d
	movl	%r13d, %ecx
	movl	$5, %esi
	movq	%r12, %rdi
	call	*%r10
	movq	%rax, %rdx
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L319:
	testq	%r11, %r11
	jne	.L287
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L317:
	testq	%r11, %r11
	jne	.L287
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L313:
	movl	$443, %r8d
	movl	$125, %edx
	movl	$104, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L275:
	movl	$438, %r8d
	movl	$121, %edx
	movl	$104, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	jmp	.L274
.L316:
	movl	$454, %r8d
	movl	$120, %edx
	movl	$104, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	jmp	.L274
.L318:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE296:
	.size	BIO_gets, .-BIO_gets
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	" "
	.text
	.p2align 4
	.globl	BIO_indent
	.type	BIO_indent, @function
BIO_indent:
.LFB297:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	$2147483648, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movl	$0, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	cmovs	%r12d, %esi
	cmpl	%edx, %esi
	cmovle	%esi, %edx
	movl	%edx, %r12d
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L325:
	testq	%rax, %rax
	jne	.L326
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %r9d
	movl	$4, %esi
	leaq	.LC1(%rip), %rdx
	movq	%rbx, %rdi
	call	*%r10
	testl	%eax, %eax
	jle	.L331
.L332:
	movl	32(%rbx), %eax
	testl	%eax, %eax
	je	.L356
	movq	(%rbx), %rax
	leaq	.LC1(%rip), %rsi
	movq	%rbx, %rdi
	call	*48(%rax)
	testl	%eax, %eax
	jle	.L333
	movq	8(%rbx), %r11
	movslq	%eax, %r9
	movq	16(%rbx), %r10
	addq	%r9, 96(%rbx)
	movq	%r9, -48(%rbp)
	testq	%r11, %r11
	je	.L357
	testq	%r10, %r10
	jne	.L346
.L342:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%rbx, %rdi
	movl	$132, %esi
	call	*%r11
	testq	%rax, %rax
	jle	.L339
	movq	%rax, -48(%rbp)
	movq	%rax, %r9
	.p2align 4,,10
	.p2align 3
.L340:
	cmpq	%r13, %r9
	jnb	.L358
.L336:
	subl	$1, %r12d
	cmpq	$1, %r9
	jne	.L331
.L321:
	testl	%r12d, %r12d
	je	.L359
	movq	$0, -48(%rbp)
	testq	%rbx, %rbx
	je	.L322
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L322
	cmpq	$0, 48(%rax)
	je	.L322
	movq	8(%rbx), %r10
	movq	16(%rbx), %rax
	testq	%r10, %r10
	jne	.L325
	testq	%rax, %rax
	je	.L332
.L326:
	pushq	$0
	movl	$4, %esi
	movq	%rbx, %rdi
	xorl	%r9d, %r9d
	pushq	$1
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	.LC1(%rip), %rdx
	call	*%rax
	popq	%rsi
	popq	%rdi
	testl	%eax, %eax
	jg	.L332
.L331:
	xorl	%eax, %eax
.L320:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L360
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L346:
	.cfi_restore_state
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L335:
	leaq	-48(%rbp), %rdx
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	%rdx
	movl	$132, %esi
	leaq	.LC1(%rip), %rdx
	movq	%rbx, %rdi
	pushq	%rax
	call	*%r10
	popq	%rdx
	popq	%rcx
.L339:
	testl	%eax, %eax
	jle	.L331
	movq	-48(%rbp), %r9
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L333:
	movq	8(%rbx), %r11
	movq	16(%rbx), %r10
	testq	%r11, %r11
	je	.L361
	movslq	%eax, %r9
	testq	%r10, %r10
	je	.L342
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L357:
	movl	$1, %eax
	testq	%r10, %r10
	jne	.L335
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L361:
	testq	%r10, %r10
	jne	.L335
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L359:
	movl	$1, %eax
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L322:
	movl	$393, %r8d
	movl	$121, %edx
	movl	$110, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L320
.L358:
	movl	$422, %r8d
	movl	$102, %edx
	movl	$110, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L320
.L356:
	movl	$404, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$120, %edx
	movl	%eax, -52(%rbp)
	movl	$110, %esi
	movl	$32, %edi
	call	ERR_put_error@PLT
	movl	-52(%rbp), %eax
	jmp	.L320
.L360:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE297:
	.size	BIO_indent, .-BIO_indent
	.p2align 4
	.globl	BIO_int_ctrl
	.type	BIO_int_ctrl, @function
BIO_int_ctrl:
.LFB298:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	%ecx, -44(%rbp)
	testq	%rdi, %rdi
	je	.L375
	movq	(%rdi), %rax
	movq	%rdi, %r12
	testq	%rax, %rax
	je	.L364
	movq	64(%rax), %rax
	testq	%rax, %rax
	je	.L364
	movq	8(%rdi), %r11
	movq	16(%rdi), %r10
	movl	%esi, %r13d
	movq	%rdx, %rbx
	testq	%r11, %r11
	je	.L380
	testq	%r10, %r10
	jne	.L367
	leaq	-44(%rbp), %r14
	movq	%rdx, %r8
	movl	%esi, %ecx
	movl	$1, %r9d
	movq	%r14, %rdx
	movl	$6, %esi
	call	*%r11
.L370:
	testq	%rax, %rax
	jle	.L362
	movq	(%r12), %rax
	movq	64(%rax), %rax
.L368:
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	*%rax
	movq	8(%r12), %r11
	movq	16(%r12), %r10
	testq	%r11, %r11
	je	.L381
	testq	%r10, %r10
	jne	.L372
	movq	%rax, %r9
	movq	%rbx, %r8
	movl	%r13d, %ecx
	movq	%r14, %rdx
	movl	$134, %esi
	movq	%r12, %rdi
	call	*%r11
.L362:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L382
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L380:
	.cfi_restore_state
	leaq	-44(%rbp), %r14
	testq	%r10, %r10
	je	.L368
.L367:
	leaq	-44(%rbp), %r14
	pushq	$0
	movl	$6, %esi
	movq	%r12, %rdi
	pushq	$1
	movq	%rbx, %r9
	movl	%r13d, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	call	*%r10
	popq	%rsi
	popq	%rdi
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L381:
	testq	%r10, %r10
	je	.L362
.L372:
	pushq	$0
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%rbx, %r9
	pushq	%rax
	movl	%r13d, %r8d
	movl	$134, %esi
	movq	%r12, %rdi
	call	*%r10
	popq	%rdx
	popq	%rcx
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L375:
	xorl	%eax, %eax
	jmp	.L362
.L364:
	movl	$518, %r8d
	movl	$121, %edx
	movl	$103, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	$-2, %rax
	jmp	.L362
.L382:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE298:
	.size	BIO_int_ctrl, .-BIO_int_ctrl
	.p2align 4
	.globl	BIO_ptr_ctrl
	.type	BIO_ptr_ctrl, @function
BIO_ptr_ctrl:
.LFB299:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	testq	%rdi, %rdi
	je	.L403
	movq	(%rdi), %rax
	movq	%rdi, %r12
	testq	%rax, %rax
	je	.L386
	movq	64(%rax), %rax
	testq	%rax, %rax
	je	.L386
	movq	8(%rdi), %r11
	movq	16(%rdi), %r10
	movl	%esi, %r13d
	movq	%rdx, %rbx
	testq	%r11, %r11
	je	.L404
	testq	%r10, %r10
	jne	.L389
	leaq	-48(%rbp), %r14
	movq	%rdx, %r8
	movl	%esi, %ecx
	movl	$1, %r9d
	movq	%r14, %rdx
	movl	$6, %esi
	call	*%r11
	testq	%rax, %rax
	jle	.L403
.L407:
	movq	(%r12), %rax
	movq	64(%rax), %rax
.L390:
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	*%rax
	movq	8(%r12), %r10
	movq	%rax, %r9
	movq	16(%r12), %rax
	testq	%r10, %r10
	je	.L405
	testq	%rax, %rax
	jne	.L395
	movq	%rbx, %r8
	movl	%r13d, %ecx
	movq	%r14, %rdx
	movl	$134, %esi
	movq	%r12, %rdi
	call	*%r10
	movq	%rax, %r9
.L396:
	testq	%r9, %r9
	jle	.L403
	movq	-48(%rbp), %rax
.L383:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L406
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L404:
	.cfi_restore_state
	leaq	-48(%rbp), %r14
	testq	%r10, %r10
	je	.L390
.L389:
	leaq	-48(%rbp), %r14
	pushq	$0
	xorl	%ecx, %ecx
	movl	$6, %esi
	pushq	$1
	movq	%rbx, %r9
	movl	%r13d, %r8d
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	*%r10
	popq	%rcx
	popq	%rsi
	testq	%rax, %rax
	jg	.L407
.L403:
	xorl	%eax, %eax
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L405:
	testq	%rax, %rax
	je	.L396
.L395:
	pushq	$0
	movq	%r14, %rdx
	movl	%r13d, %r8d
	xorl	%ecx, %ecx
	pushq	%r9
	movl	$134, %esi
	movq	%rbx, %r9
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %r9
	popq	%rax
	popq	%rdx
	jmp	.L396
.L386:
	movl	$518, %r8d
	movl	$121, %edx
	movl	$103, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L383
.L406:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE299:
	.size	BIO_ptr_ctrl, .-BIO_ptr_ctrl
	.p2align 4
	.globl	BIO_ctrl
	.type	BIO_ctrl, @function
BIO_ctrl:
.LFB300:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L420
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L410
	movq	64(%rax), %rax
	testq	%rax, %rax
	je	.L410
	movq	8(%rdi), %r11
	movq	16(%rdi), %r10
	movl	%esi, %r14d
	movq	%rdx, %rbx
	movq	%rcx, %r13
	testq	%r11, %r11
	je	.L428
	testq	%r10, %r10
	jne	.L413
	movq	%rdx, %r8
	movl	%esi, %ecx
	movl	$1, %r9d
	movq	%r13, %rdx
	movl	$6, %esi
	call	*%r11
	testq	%rax, %rax
	jle	.L408
.L430:
	movq	(%r12), %rax
	movq	64(%rax), %rax
.L414:
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	*%rax
	movq	8(%r12), %r11
	movq	16(%r12), %r10
	testq	%r11, %r11
	je	.L429
	testq	%r10, %r10
	jne	.L418
	leaq	-32(%rbp), %rsp
	movq	%rbx, %r8
	movl	%r14d, %ecx
	movq	%r13, %rdx
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	movq	%r12, %rdi
	movq	%rax, %r9
	popq	%r12
	.cfi_restore 12
	movl	$134, %esi
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%r11
	.p2align 4,,10
	.p2align 3
.L428:
	.cfi_restore_state
	testq	%r10, %r10
	je	.L414
.L413:
	pushq	$0
	movl	$6, %esi
	movq	%r12, %rdi
	movq	%rbx, %r9
	pushq	$1
	movl	%r14d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	call	*%r10
	popq	%rsi
	popq	%rdi
	testq	%rax, %rax
	jg	.L430
.L408:
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L429:
	.cfi_restore_state
	testq	%r10, %r10
	je	.L408
.L418:
	pushq	$0
	movq	%r13, %rdx
	movq	%rbx, %r9
	movl	%r14d, %r8d
	pushq	%rax
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movl	$134, %esi
	call	*%r10
	popq	%rdx
	popq	%rcx
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L420:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%eax, %eax
	ret
.L410:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movl	$518, %r8d
	movl	$121, %edx
	movl	$103, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	$-2, %rax
	jmp	.L408
	.cfi_endproc
.LFE300:
	.size	BIO_ctrl, .-BIO_ctrl
	.p2align 4
	.globl	BIO_callback_ctrl
	.type	BIO_callback_ctrl, @function
BIO_callback_ctrl:
.LFB301:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movq	%rdx, -24(%rbp)
	testq	%rdi, %rdi
	je	.L444
	movq	(%rdi), %rax
	movq	%rdi, %r12
	testq	%rax, %rax
	je	.L433
	movq	88(%rax), %rax
	cmpl	$14, %esi
	jne	.L433
	testq	%rax, %rax
	je	.L433
	movq	8(%rdi), %r11
	movq	16(%rdi), %r10
	testq	%r11, %r11
	je	.L449
	testq	%r10, %r10
	jne	.L437
	leaq	-24(%rbp), %rdx
	movl	$1, %r9d
	xorl	%r8d, %r8d
	movl	$14, %ecx
	movl	$6, %esi
	call	*%r11
.L440:
	testq	%rax, %rax
	jle	.L431
	movq	(%r12), %rax
	movq	88(%rax), %rax
.L438:
	movq	-24(%rbp), %rdx
	movl	$14, %esi
	movq	%r12, %rdi
	call	*%rax
	movq	8(%r12), %r11
	movq	16(%r12), %r10
	testq	%r11, %r11
	je	.L450
	testq	%r10, %r10
	jne	.L442
	leaq	-24(%rbp), %rdx
	movq	%rax, %r9
	xorl	%r8d, %r8d
	movl	$14, %ecx
	movl	$134, %esi
	movq	%r12, %rdi
	call	*%r11
.L431:
	movq	-8(%rbp), %r12
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L449:
	.cfi_restore_state
	testq	%r10, %r10
	je	.L438
.L437:
	pushq	$0
	movl	$6, %esi
	movq	%r12, %rdi
	leaq	-24(%rbp), %rdx
	pushq	$1
	xorl	%r9d, %r9d
	movl	$14, %r8d
	xorl	%ecx, %ecx
	call	*%r10
	popq	%rsi
	popq	%rdi
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L450:
	testq	%r10, %r10
	je	.L431
.L442:
	pushq	$0
	leaq	-24(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	pushq	%rax
	xorl	%r9d, %r9d
	movl	$14, %r8d
	movl	$134, %esi
	call	*%r10
	popq	%rdx
	movq	-8(%rbp), %r12
	popq	%rcx
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L444:
	.cfi_restore_state
	movq	-8(%rbp), %r12
	xorl	%eax, %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L433:
	.cfi_restore_state
	movl	$546, %r8d
	movl	$121, %edx
	movl	$131, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	$-2, %rax
	jmp	.L431
	.cfi_endproc
.LFE301:
	.size	BIO_callback_ctrl, .-BIO_callback_ctrl
	.p2align 4
	.globl	BIO_ctrl_pending
	.type	BIO_ctrl_pending, @function
BIO_ctrl_pending:
.LFB302:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L464
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L453
	movq	64(%rax), %rax
	testq	%rax, %rax
	je	.L453
	movq	8(%rdi), %r11
	movq	16(%rdi), %r10
	testq	%r11, %r11
	je	.L472
	testq	%r10, %r10
	jne	.L456
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movl	$1, %r9d
	movl	$10, %ecx
	movl	$6, %esi
	call	*%r11
	testq	%rax, %rax
	jle	.L451
.L474:
	movq	(%r12), %rax
	movq	64(%rax), %rax
.L457:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$10, %esi
	movq	%r12, %rdi
	call	*%rax
	movq	8(%r12), %r11
	movq	16(%r12), %r10
	testq	%r11, %r11
	je	.L473
	testq	%r10, %r10
	jne	.L462
	movq	%r12, %rdi
	movq	%rax, %r9
	movq	-8(%rbp), %r12
	xorl	%r8d, %r8d
	leave
	.cfi_remember_state
	.cfi_restore 6
	.cfi_restore 12
	.cfi_def_cfa 7, 8
	movl	$10, %ecx
	xorl	%edx, %edx
	movl	$134, %esi
	jmp	*%r11
	.p2align 4,,10
	.p2align 3
.L472:
	.cfi_restore_state
	testq	%r10, %r10
	je	.L457
.L456:
	pushq	$0
	movl	$6, %esi
	movq	%r12, %rdi
	xorl	%r9d, %r9d
	pushq	$1
	movl	$10, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	*%r10
	popq	%rsi
	popq	%rdi
	testq	%rax, %rax
	jg	.L474
.L451:
	movq	-8(%rbp), %r12
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L473:
	.cfi_restore_state
	testq	%r10, %r10
	je	.L451
.L462:
	pushq	$0
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	pushq	%rax
	xorl	%r9d, %r9d
	movl	$10, %r8d
	movl	$134, %esi
	call	*%r10
	popq	%rdx
	movq	-8(%rbp), %r12
	popq	%rcx
	leave
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L464:
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
.L453:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	$518, %r8d
	movl	$121, %edx
	movl	$103, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	$-2, %rax
	jmp	.L451
	.cfi_endproc
.LFE302:
	.size	BIO_ctrl_pending, .-BIO_ctrl_pending
	.p2align 4
	.globl	BIO_ctrl_wpending
	.type	BIO_ctrl_wpending, @function
BIO_ctrl_wpending:
.LFB303:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L488
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L477
	movq	64(%rax), %rax
	testq	%rax, %rax
	je	.L477
	movq	8(%rdi), %r11
	movq	16(%rdi), %r10
	testq	%r11, %r11
	je	.L496
	testq	%r10, %r10
	jne	.L480
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movl	$1, %r9d
	movl	$13, %ecx
	movl	$6, %esi
	call	*%r11
	testq	%rax, %rax
	jle	.L475
.L498:
	movq	(%r12), %rax
	movq	64(%rax), %rax
.L481:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$13, %esi
	movq	%r12, %rdi
	call	*%rax
	movq	8(%r12), %r11
	movq	16(%r12), %r10
	testq	%r11, %r11
	je	.L497
	testq	%r10, %r10
	jne	.L486
	movq	%r12, %rdi
	movq	%rax, %r9
	movq	-8(%rbp), %r12
	xorl	%r8d, %r8d
	leave
	.cfi_remember_state
	.cfi_restore 6
	.cfi_restore 12
	.cfi_def_cfa 7, 8
	movl	$13, %ecx
	xorl	%edx, %edx
	movl	$134, %esi
	jmp	*%r11
	.p2align 4,,10
	.p2align 3
.L496:
	.cfi_restore_state
	testq	%r10, %r10
	je	.L481
.L480:
	pushq	$0
	movl	$6, %esi
	movq	%r12, %rdi
	xorl	%r9d, %r9d
	pushq	$1
	movl	$13, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	*%r10
	popq	%rsi
	popq	%rdi
	testq	%rax, %rax
	jg	.L498
.L475:
	movq	-8(%rbp), %r12
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L497:
	.cfi_restore_state
	testq	%r10, %r10
	je	.L475
.L486:
	pushq	$0
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	pushq	%rax
	xorl	%r9d, %r9d
	movl	$13, %r8d
	movl	$134, %esi
	call	*%r10
	popq	%rdx
	movq	-8(%rbp), %r12
	popq	%rcx
	leave
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L488:
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
.L477:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	$518, %r8d
	movl	$121, %edx
	movl	$103, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	$-2, %rax
	jmp	.L475
	.cfi_endproc
.LFE303:
	.size	BIO_ctrl_wpending, .-BIO_ctrl_wpending
	.p2align 4
	.globl	BIO_push
	.type	BIO_push, @function
BIO_push:
.LFB304:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L515
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	.p2align 4,,10
	.p2align 3
.L501:
	movq	%rdx, %r12
	movq	64(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L501
	movq	%rsi, 64(%r12)
	testq	%rsi, %rsi
	je	.L502
	movq	%r12, 72(%rsi)
.L502:
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L503
	movq	64(%rax), %rax
	testq	%rax, %rax
	je	.L503
	movq	8(%rbx), %r11
	movq	16(%rbx), %r10
	testq	%r11, %r11
	je	.L528
	testq	%r10, %r10
	jne	.L506
	movl	$1, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movl	$6, %ecx
	movl	$6, %esi
	call	*%r11
.L509:
	testq	%rax, %rax
	jle	.L527
	movq	(%rbx), %rax
	movq	64(%rax), %rax
.L507:
	movq	%r12, %rcx
	xorl	%edx, %edx
	movl	$6, %esi
	movq	%rbx, %rdi
	call	*%rax
	movq	8(%rbx), %r11
	movq	16(%rbx), %r10
	testq	%r11, %r11
	je	.L529
	testq	%r10, %r10
	jne	.L512
	movq	%rax, %r9
	xorl	%r8d, %r8d
	movl	$6, %ecx
	movq	%r12, %rdx
	movl	$134, %esi
	movq	%rbx, %rdi
	call	*%r11
.L527:
	movq	%rbx, %rax
.L499:
	leaq	-16(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L528:
	.cfi_restore_state
	testq	%r10, %r10
	je	.L507
.L506:
	pushq	$0
	xorl	%ecx, %ecx
	movl	$6, %esi
	xorl	%r9d, %r9d
	pushq	$1
	movl	$6, %r8d
	movq	%r12, %rdx
	movq	%rbx, %rdi
	call	*%r10
	popq	%rcx
	popq	%rsi
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L529:
	testq	%r10, %r10
	je	.L527
.L512:
	pushq	$0
	movq	%r12, %rdx
	movq	%rbx, %rdi
	xorl	%r9d, %r9d
	pushq	%rax
	movl	$6, %r8d
	xorl	%ecx, %ecx
	movl	$134, %esi
	call	*%r10
	popq	%rax
	popq	%rdx
	leaq	-16(%rbp), %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L515:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movq	%rsi, %rax
	ret
.L503:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	$518, %r8d
	movl	$121, %edx
	movl	$103, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%rbx, %rax
	jmp	.L499
	.cfi_endproc
.LFE304:
	.size	BIO_push, .-BIO_push
	.p2align 4
	.globl	BIO_pop
	.type	BIO_pop, @function
BIO_pop:
.LFB305:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	testq	%rdi, %rdi
	je	.L547
	movq	(%rdi), %rax
	movq	64(%rdi), %r12
	movq	%rdi, %rbx
	testq	%rax, %rax
	je	.L532
	movq	64(%rax), %rax
	testq	%rax, %rax
	je	.L532
	movq	8(%rdi), %r11
	movq	16(%rdi), %r10
	testq	%r11, %r11
	je	.L558
	testq	%r10, %r10
	jne	.L536
	xorl	%r8d, %r8d
	movl	$7, %ecx
	movq	%rdi, %rdx
	movl	$6, %esi
	movl	$1, %r9d
	call	*%r11
.L539:
	testq	%rax, %rax
	jle	.L534
	movq	(%rbx), %rax
	movq	64(%rax), %rax
.L537:
	movq	%rbx, %rcx
	xorl	%edx, %edx
	movl	$7, %esi
	movq	%rbx, %rdi
	call	*%rax
	movq	8(%rbx), %r11
	movq	16(%rbx), %r10
	testq	%r11, %r11
	je	.L559
	testq	%r10, %r10
	jne	.L542
	movq	%rax, %r9
	xorl	%r8d, %r8d
	movl	$7, %ecx
	movq	%rbx, %rdx
	movl	$134, %esi
	movq	%rbx, %rdi
	call	*%r11
.L534:
	movq	72(%rbx), %rdx
	movq	64(%rbx), %rax
	testq	%rdx, %rdx
	je	.L545
	movq	%rax, 64(%rdx)
	movq	64(%rbx), %rax
.L545:
	testq	%rax, %rax
	je	.L546
	movq	%rdx, 72(%rax)
.L546:
	pxor	%xmm0, %xmm0
	movq	%r12, %rax
	movups	%xmm0, 64(%rbx)
	leaq	-16(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L558:
	.cfi_restore_state
	testq	%r10, %r10
	je	.L537
.L536:
	pushq	$0
	xorl	%ecx, %ecx
	movl	$6, %esi
	xorl	%r9d, %r9d
	pushq	$1
	movl	$7, %r8d
	movq	%rbx, %rdx
	movq	%rbx, %rdi
	call	*%r10
	popq	%rcx
	popq	%rsi
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L559:
	testq	%r10, %r10
	je	.L534
.L542:
	pushq	$0
	movq	%rbx, %rdx
	xorl	%r9d, %r9d
	movl	$7, %r8d
	pushq	%rax
	xorl	%ecx, %ecx
	movl	$134, %esi
	movq	%rbx, %rdi
	call	*%r10
	popq	%rax
	popq	%rdx
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L547:
	leaq	-16(%rbp), %rsp
	xorl	%r12d, %r12d
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L532:
	.cfi_restore_state
	movl	$518, %r8d
	movl	$121, %edx
	movl	$103, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L534
	.cfi_endproc
.LFE305:
	.size	BIO_pop, .-BIO_pop
	.p2align 4
	.globl	BIO_get_retry_BIO
	.type	BIO_get_retry_BIO, @function
BIO_get_retry_BIO:
.LFB306:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L571:
	movq	64(%rdi), %rax
	movq	%rdi, %r8
	testq	%rax, %rax
	je	.L561
	movq	%rax, %rdi
.L562:
	testb	$8, 40(%rdi)
	jne	.L571
.L561:
	testq	%rsi, %rsi
	je	.L560
	movl	44(%r8), %eax
	movl	%eax, (%rsi)
.L560:
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE306:
	.size	BIO_get_retry_BIO, .-BIO_get_retry_BIO
	.p2align 4
	.globl	BIO_get_retry_reason
	.type	BIO_get_retry_reason, @function
BIO_get_retry_reason:
.LFB307:
	.cfi_startproc
	endbr64
	movl	44(%rdi), %eax
	ret
	.cfi_endproc
.LFE307:
	.size	BIO_get_retry_reason, .-BIO_get_retry_reason
	.p2align 4
	.globl	BIO_set_retry_reason
	.type	BIO_set_retry_reason, @function
BIO_set_retry_reason:
.LFB308:
	.cfi_startproc
	endbr64
	movl	%esi, 44(%rdi)
	ret
	.cfi_endproc
.LFE308:
	.size	BIO_set_retry_reason, .-BIO_set_retry_reason
	.p2align 4
	.globl	BIO_find_type
	.type	BIO_find_type, @function
BIO_find_type:
.LFB309:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L580
	testb	%sil, %sil
	je	.L576
	.p2align 4,,10
	.p2align 3
.L578:
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L577
	cmpl	(%rax), %esi
	je	.L582
.L577:
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.L578
.L580:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L576:
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L579
	testl	%esi, (%rax)
	jne	.L582
.L579:
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.L576
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L582:
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE309:
	.size	BIO_find_type, .-BIO_find_type
	.p2align 4
	.globl	BIO_next
	.type	BIO_next, @function
BIO_next:
.LFB310:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L596
	movq	64(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L596:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE310:
	.size	BIO_next, .-BIO_next
	.p2align 4
	.globl	BIO_set_next
	.type	BIO_set_next, @function
BIO_set_next:
.LFB311:
	.cfi_startproc
	endbr64
	movq	%rsi, 64(%rdi)
	ret
	.cfi_endproc
.LFE311:
	.size	BIO_set_next, .-BIO_set_next
	.p2align 4
	.globl	BIO_free_all
	.type	BIO_free_all, @function
BIO_free_all:
.LFB312:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	.LC0(%rip), %rbx
	jmp	.L599
	.p2align 4,,10
	.p2align 3
.L600:
	jle	.L601
.L602:
	cmpl	$1, %r13d
	jg	.L598
.L619:
	movq	%r14, %r12
.L599:
	testq	%r12, %r12
	je	.L598
	leaq	80(%r12), %rdx
	movl	$-1, %eax
	movl	80(%r12), %r13d
	movq	64(%r12), %r14
	lock xaddl	%eax, (%rdx)
	subl	$1, %eax
	testl	%eax, %eax
	jne	.L600
.L601:
	movq	8(%r12), %r10
	movq	16(%r12), %rax
	testq	%r10, %r10
	je	.L618
	testq	%rax, %rax
	jne	.L604
	movl	$1, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%r12, %rdi
	call	*%r10
.L608:
	testl	%eax, %eax
	jle	.L602
.L610:
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L606
	movq	80(%rax), %rax
	testq	%rax, %rax
	je	.L606
	movq	%r12, %rdi
	call	*%rax
.L606:
	leaq	104(%r12), %rdx
	movq	%r12, %rsi
	movl	$12, %edi
	call	CRYPTO_free_ex_data@PLT
	movq	112(%r12), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	movl	$138, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	cmpl	$1, %r13d
	jle	.L619
.L598:
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L618:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L610
.L604:
	pushq	$0
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r9d, %r9d
	pushq	$1
	xorl	%r8d, %r8d
	movl	$1, %esi
	movq	%r12, %rdi
	call	*%rax
	popq	%rdx
	popq	%rcx
	jmp	.L608
	.cfi_endproc
.LFE312:
	.size	BIO_free_all, .-BIO_free_all
	.p2align 4
	.globl	BIO_dup_chain
	.type	BIO_dup_chain, @function
BIO_dup_chain:
.LFB313:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L628
	.p2align 4,,10
	.p2align 3
.L621:
	movq	(%rbx), %rdi
	movq	%r12, %r14
	call	BIO_new
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L629
	movdqu	8(%rbx), %xmm0
	xorl	%edx, %edx
	movq	%r12, %rcx
	movl	$12, %esi
	movq	%rbx, %rdi
	movups	%xmm0, 8(%rax)
	movq	24(%rbx), %rax
	movq	%rax, 24(%r12)
	movl	32(%rbx), %eax
	movl	%eax, 32(%r12)
	movl	36(%rbx), %eax
	movl	%eax, 36(%r12)
	movl	40(%rbx), %eax
	movl	%eax, 40(%r12)
	movl	48(%rbx), %eax
	movl	%eax, 48(%r12)
	call	BIO_ctrl
	testq	%rax, %rax
	je	.L644
	leaq	104(%rbx), %rdx
	leaq	104(%r12), %rsi
	movl	$12, %edi
	call	CRYPTO_dup_ex_data@PLT
	testl	%eax, %eax
	je	.L644
	testq	%r13, %r13
	je	.L632
	testq	%r14, %r14
	je	.L626
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L627:
	movq	%rax, %rcx
	movq	64(%rax), %rax
	testq	%rax, %rax
	jne	.L627
	movq	%r12, 64(%rcx)
	xorl	%edx, %edx
	movl	$6, %esi
	movq	%r14, %rdi
	movq	%rcx, 72(%r12)
	call	BIO_ctrl
.L626:
	movq	64(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L621
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L630:
	.cfi_restore_state
	movq	%r13, %rdi
	movl	80(%r13), %ebx
	movq	64(%r13), %r12
	call	BIO_free
	cmpl	$1, %ebx
	jg	.L628
	movq	%r12, %r13
.L629:
	testq	%r13, %r13
	jne	.L630
.L628:
	xorl	%r13d, %r13d
	popq	%rbx
	popq	%r12
	movq	%r13, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L632:
	.cfi_restore_state
	movq	%r12, %r13
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L644:
	movq	%r12, %rdi
	call	BIO_free
	jmp	.L629
	.cfi_endproc
.LFE313:
	.size	BIO_dup_chain, .-BIO_dup_chain
	.p2align 4
	.globl	BIO_copy_next_retry
	.type	BIO_copy_next_retry, @function
BIO_copy_next_retry:
.LFB314:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rdx
	movl	40(%rdx), %eax
	andl	$15, %eax
	orl	%eax, 40(%rdi)
	movl	44(%rdx), %eax
	movl	%eax, 44(%rdi)
	ret
	.cfi_endproc
.LFE314:
	.size	BIO_copy_next_retry, .-BIO_copy_next_retry
	.p2align 4
	.globl	BIO_set_ex_data
	.type	BIO_set_ex_data, @function
BIO_set_ex_data:
.LFB315:
	.cfi_startproc
	endbr64
	addq	$104, %rdi
	jmp	CRYPTO_set_ex_data@PLT
	.cfi_endproc
.LFE315:
	.size	BIO_set_ex_data, .-BIO_set_ex_data
	.p2align 4
	.globl	BIO_get_ex_data
	.type	BIO_get_ex_data, @function
BIO_get_ex_data:
.LFB316:
	.cfi_startproc
	endbr64
	addq	$104, %rdi
	jmp	CRYPTO_get_ex_data@PLT
	.cfi_endproc
.LFE316:
	.size	BIO_get_ex_data, .-BIO_get_ex_data
	.p2align 4
	.globl	BIO_number_read
	.type	BIO_number_read, @function
BIO_number_read:
.LFB317:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L648
	movq	88(%rdi), %rax
.L648:
	ret
	.cfi_endproc
.LFE317:
	.size	BIO_number_read, .-BIO_number_read
	.p2align 4
	.globl	BIO_number_written
	.type	BIO_number_written, @function
BIO_number_written:
.LFB318:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L652
	movq	96(%rdi), %rax
.L652:
	ret
	.cfi_endproc
.LFE318:
	.size	BIO_number_written, .-BIO_number_written
	.p2align 4
	.globl	bio_free_ex_data
	.type	bio_free_ex_data, @function
bio_free_ex_data:
.LFB319:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	leaq	104(%rdi), %rdx
	movl	$12, %edi
	jmp	CRYPTO_free_ex_data@PLT
	.cfi_endproc
.LFE319:
	.size	bio_free_ex_data, .-bio_free_ex_data
	.p2align 4
	.globl	bio_cleanup
	.type	bio_cleanup, @function
bio_cleanup:
.LFB320:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	bio_sock_cleanup_int@PLT
	movq	bio_lookup_lock(%rip), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	movq	bio_type_lock(%rip), %rdi
	movq	$0, bio_lookup_lock(%rip)
	call	CRYPTO_THREAD_lock_free@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	$0, bio_type_lock(%rip)
	ret
	.cfi_endproc
.LFE320:
	.size	bio_cleanup, .-bio_cleanup
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
