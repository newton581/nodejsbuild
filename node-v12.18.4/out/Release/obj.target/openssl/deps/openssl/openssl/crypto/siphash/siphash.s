	.file	"siphash.c"
	.text
	.p2align 4
	.globl	SipHash_ctx_size
	.type	SipHash_ctx_size, @function
SipHash_ctx_size:
.LFB151:
	.cfi_startproc
	endbr64
	movl	$64, %eax
	ret
	.cfi_endproc
.LFE151:
	.size	SipHash_ctx_size, .-SipHash_ctx_size
	.p2align 4
	.globl	SipHash_hash_size
	.type	SipHash_hash_size, @function
SipHash_hash_size:
.LFB152:
	.cfi_startproc
	endbr64
	movslq	44(%rdi), %rax
	ret
	.cfi_endproc
.LFE152:
	.size	SipHash_hash_size, .-SipHash_hash_size
	.p2align 4
	.globl	SipHash_set_hash_size
	.type	SipHash_set_hash_size, @function
SipHash_set_hash_size:
.LFB154:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L5
	leaq	-8(%rsi), %rdx
	xorl	%eax, %eax
	testq	$-9, %rdx
	je	.L14
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movslq	44(%rdi), %rcx
	movl	$16, %edx
	testq	%rcx, %rcx
	movq	%rcx, %rax
	cmovne	%rcx, %rdx
	movl	$16, %ecx
	cmove	%ecx, %eax
.L7:
	movl	%eax, 44(%rdi)
	cmpq	%rdx, %rsi
	je	.L13
	xorq	$238, 16(%rdi)
	movl	$1, %eax
	movl	%esi, 44(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movslq	44(%rdi), %rdx
	movl	$16, %esi
	movq	%rdx, %rax
	testq	%rdx, %rdx
	jne	.L7
	movl	$16, 44(%rdi)
.L13:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE154:
	.size	SipHash_set_hash_size, .-SipHash_set_hash_size
	.p2align 4
	.globl	SipHash_Init
	.type	SipHash_Init, @function
SipHash_Init:
.LFB155:
	.cfi_startproc
	endbr64
	movl	44(%rdi), %eax
	movl	$16, %r9d
	movq	(%rsi), %r8
	movq	8(%rsi), %rsi
	movl	$0, 40(%rdi)
	testl	%eax, %eax
	movq	$0, (%rdi)
	cmove	%r9d, %eax
	testl	%ecx, %ecx
	movl	$4, %r9d
	cmove	%r9d, %ecx
	testl	%edx, %edx
	movl	$2, %r9d
	cmove	%r9d, %edx
	movl	%eax, 44(%rdi)
	movl	%ecx, 52(%rdi)
	movl	%edx, 48(%rdi)
	movabsq	$8317987319222330741, %rdx
	xorq	%r8, %rdx
	movq	%rdx, 8(%rdi)
	movabsq	$7237128888997146477, %rdx
	xorq	%rsi, %rdx
	movq	%rdx, 16(%rdi)
	movabsq	$7816392313619706465, %rdx
	xorq	%rdx, %r8
	movabsq	$8387220255154660723, %rdx
	xorq	%rsi, %rdx
	movq	%r8, 24(%rdi)
	movq	%rdx, 32(%rdi)
	cmpl	$16, %eax
	jne	.L19
	movabsq	$7237128888997146499, %rax
	xorq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L19:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE155:
	.size	SipHash_Init, .-SipHash_Init
	.p2align 4
	.globl	SipHash_Update
	.type	SipHash_Update, @function
SipHash_Update:
.LFB156:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	40(%rdi), %eax
	addq	%rdx, (%rdi)
	movq	8(%rdi), %r13
	movq	16(%rdi), %r12
	movq	24(%rdi), %r14
	movq	32(%rdi), %rbx
	testl	%eax, %eax
	je	.L21
	movl	$8, %edx
	leaq	56(%rdi,%rax), %rdi
	subl	%eax, %edx
	cmpq	%rdx, %rcx
	jb	.L47
	movq	%rcx, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rcx
	movq	-56(%rbp), %rsi
	movq	56(%r15), %rdi
	subq	%rdx, %rcx
	addq	%rdx, %rsi
	movl	48(%r15), %edx
	xorq	%rdi, %rbx
	testl	%edx, %edx
	jle	.L24
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L25:
	addq	%r12, %r13
	addq	%rbx, %r14
	rolq	$13, %r12
	addl	$1, %eax
	rolq	$16, %rbx
	xorq	%r13, %r12
	rolq	$32, %r13
	xorq	%r14, %rbx
	addq	%r12, %r14
	rolq	$17, %r12
	addq	%rbx, %r13
	rolq	$21, %rbx
	xorq	%r14, %r12
	xorq	%r13, %rbx
	rolq	$32, %r14
	cmpl	%edx, %eax
	jne	.L25
.L24:
	xorq	%rdi, %r13
.L21:
	movl	%ecx, %r8d
	andq	$-8, %rcx
	leaq	(%rsi,%rcx), %rdi
	andl	$7, %r8d
	cmpq	%rdi, %rsi
	je	.L26
	movl	48(%r15), %edx
	.p2align 4,,10
	.p2align 3
.L29:
	movq	(%rsi), %rcx
	xorq	%rcx, %rbx
	testl	%edx, %edx
	jle	.L27
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L28:
	addq	%r12, %r13
	addq	%rbx, %r14
	rolq	$13, %r12
	addl	$1, %eax
	rolq	$16, %rbx
	xorq	%r13, %r12
	rolq	$32, %r13
	xorq	%r14, %rbx
	addq	%r12, %r14
	rolq	$17, %r12
	addq	%rbx, %r13
	rolq	$21, %rbx
	xorq	%r14, %r12
	xorq	%r13, %rbx
	rolq	$32, %r14
	cmpl	%edx, %eax
	jne	.L28
.L27:
	addq	$8, %rsi
	xorq	%rcx, %r13
	cmpq	%rsi, %rdi
	jne	.L29
.L26:
	testl	%r8d, %r8d
	jne	.L48
.L30:
	movq	%r13, %xmm0
	movq	%r12, %xmm1
	movq	%rbx, %xmm2
	movl	%r8d, 40(%r15)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r15)
	movq	%r14, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 24(%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L48:
	.cfi_restore_state
	leaq	56(%r15), %rsi
	xorl	%eax, %eax
.L31:
	movl	%eax, %edx
	addl	$1, %eax
	movzbl	(%rdi,%rdx), %ecx
	movb	%cl, (%rsi,%rdx)
	cmpl	%r8d, %eax
	jb	.L31
	jmp	.L30
.L47:
	movq	%rcx, %rdx
	movq	%rcx, -56(%rbp)
	call	memcpy@PLT
	movq	-56(%rbp), %rcx
	addl	%ecx, 40(%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE156:
	.size	SipHash_Update, .-SipHash_Update
	.p2align 4
	.globl	SipHash_Final
	.type	SipHash_Final, @function
SipHash_Final:
.LFB157:
	.cfi_startproc
	endbr64
	movslq	44(%rdi), %rax
	xorl	%r9d, %r9d
	cmpq	%rdx, %rax
	jne	.L76
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rax, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	(%rdi), %r10
	salq	$56, %r10
	cmpl	$7, 40(%rdi)
	ja	.L51
	movl	40(%rdi), %eax
	leaq	.L53(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L53:
	.long	.L51-.L53
	.long	.L59-.L53
	.long	.L58-.L53
	.long	.L57-.L53
	.long	.L56-.L53
	.long	.L55-.L53
	.long	.L54-.L53
	.long	.L52-.L53
	.text
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movzbl	62(%rdi), %eax
	salq	$48, %rax
	orq	%rax, %r10
.L54:
	movzbl	61(%rdi), %eax
	salq	$40, %rax
	orq	%rax, %r10
.L55:
	movzbl	60(%rdi), %eax
	salq	$32, %rax
	orq	%rax, %r10
.L56:
	movzbl	59(%rdi), %eax
	salq	$24, %rax
	orq	%rax, %r10
.L57:
	movzbl	58(%rdi), %eax
	salq	$16, %rax
	orq	%rax, %r10
.L58:
	movzbl	57(%rdi), %eax
	salq	$8, %rax
	orq	%rax, %r10
.L59:
	movzbl	56(%rdi), %eax
	orq	%rax, %r10
.L51:
	movq	32(%rdi), %rcx
	movl	48(%rdi), %ebx
	movq	8(%rdi), %r8
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	xorq	%r10, %rcx
	testl	%ebx, %ebx
	jle	.L60
	xorl	%r9d, %r9d
	.p2align 4,,10
	.p2align 3
.L61:
	addq	%rax, %r8
	addq	%rcx, %rdx
	rolq	$13, %rax
	addl	$1, %r9d
	rolq	$16, %rcx
	xorq	%r8, %rax
	rolq	$32, %r8
	xorq	%rdx, %rcx
	addq	%rax, %rdx
	rolq	$17, %rax
	addq	%rcx, %r8
	rolq	$21, %rcx
	xorq	%rdx, %rax
	xorq	%r8, %rcx
	rolq	$32, %rdx
	cmpl	%ebx, %r9d
	jne	.L61
.L60:
	movq	%rdx, %r9
	xorq	%r10, %r8
	xorb	$-1, %dl
	xorb	$-18, %r9b
	cmpl	$16, %r11d
	movl	52(%rdi), %r11d
	cmove	%r9, %rdx
	xorl	%r10d, %r10d
	testl	%r11d, %r11d
	jle	.L64
	.p2align 4,,10
	.p2align 3
.L63:
	addq	%rax, %r8
	addq	%rcx, %rdx
	rolq	$13, %rax
	addl	$1, %r10d
	rolq	$16, %rcx
	xorq	%r8, %rax
	rolq	$32, %r8
	xorq	%rdx, %rcx
	addq	%rax, %rdx
	rolq	$17, %rax
	addq	%rcx, %r8
	rolq	$21, %rcx
	xorq	%rdx, %rax
	xorq	%r8, %rcx
	rolq	$32, %rdx
	cmpl	%r11d, %r10d
	jne	.L63
.L64:
	movq	%r8, %r9
	xorq	%rax, %r9
	xorq	%rdx, %r9
	xorq	%rcx, %r9
	movq	%r9, (%rsi)
	cmpl	$8, 44(%rdi)
	movl	$1, %r9d
	je	.L49
	movl	52(%rdi), %r9d
	xorb	$-35, %al
	testl	%r9d, %r9d
	jle	.L66
	xorl	%edi, %edi
	.p2align 4,,10
	.p2align 3
.L67:
	addq	%rax, %r8
	addq	%rcx, %rdx
	rolq	$13, %rax
	addl	$1, %edi
	rolq	$16, %rcx
	xorq	%r8, %rax
	rolq	$32, %r8
	xorq	%rdx, %rcx
	addq	%rax, %rdx
	rolq	$17, %rax
	addq	%rcx, %r8
	rolq	$21, %rcx
	xorq	%rdx, %rax
	xorq	%r8, %rcx
	rolq	$32, %rdx
	cmpl	%edi, %r9d
	jne	.L67
.L66:
	xorq	%r8, %rax
	movl	$1, %r9d
	xorq	%rax, %rdx
	xorq	%rdx, %rcx
	movq	%rcx, 8(%rsi)
.L49:
	movl	%r9d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE157:
	.size	SipHash_Final, .-SipHash_Final
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
