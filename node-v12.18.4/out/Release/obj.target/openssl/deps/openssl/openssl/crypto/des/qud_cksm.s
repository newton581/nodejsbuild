	.file	"qud_cksm.c"
	.text
	.p2align 4
	.globl	DES_quad_cksum
	.type	DES_quad_cksum, @function
DES_quad_cksum:
.LFB54:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	testl	%ecx, %ecx
	leaq	-1(%rdx), %rax
	movabsq	$-9223372032559808509, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	$1, %r14d
	pushq	%r13
	cmovg	%ecx, %r14d
	shrq	%rax
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	(%r8), %r9d
	movq	%rsi, %rdi
	movl	4(%r8), %ecx
	leaq	2(%rbx,%rax,2), %rsi
	leaq	(%rbx,%rdx), %r11
.L2:
	movq	%rbx, %r8
	testq	%r12, %r12
	jg	.L4
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L22:
	movzwl	(%r8), %ecx
	addq	$2, %r8
	addl	%r9d, %ecx
	movl	%ecx, %edx
	imull	%ecx, %edx
	imull	%r15d, %ecx
	leal	(%rdx,%rax), %r9d
	movq	%r9, %rax
	imulq	%r10
	addq	%r9, %rdx
	sarq	$30, %rdx
	movq	%rdx, %rax
	salq	$31, %rax
	subq	%rdx, %rax
	subl	%eax, %r9d
	movq	%rcx, %rax
	imulq	%r10
	addq	%rcx, %rdx
	sarq	$30, %rdx
	movq	%rdx, %rax
	salq	$31, %rax
	subq	%rdx, %rax
	subl	%eax, %ecx
	cmpq	%r8, %rsi
	je	.L5
.L4:
	movl	%ecx, %eax
	leal	83653421(%rcx), %r15d
	movzbl	(%r8), %edx
	imull	%ecx, %eax
	movq	%r11, %rcx
	subq	%r8, %rcx
	cmpq	$1, %rcx
	jne	.L22
	movzbl	%dl, %ecx
	addl	%r9d, %ecx
	movl	%ecx, %edx
	imull	%ecx, %edx
	imull	%r15d, %ecx
	leal	(%rdx,%rax), %r9d
	movq	%r9, %rax
	imulq	%r10
	addq	%r9, %rdx
	sarq	$30, %rdx
	movq	%rdx, %rax
	salq	$31, %rax
	subq	%rdx, %rax
	subl	%eax, %r9d
	movq	%rcx, %rax
	imulq	%r10
	addq	%rcx, %rdx
	sarq	$30, %rdx
	movq	%rdx, %rax
	salq	$31, %rax
	subq	%rdx, %rax
	subl	%eax, %ecx
.L5:
	testq	%rdi, %rdi
	je	.L6
	movl	%r9d, (%rdi)
	addq	$8, %rdi
	movl	%ecx, -4(%rdi)
.L6:
	addl	$1, %r13d
	cmpl	$3, %r13d
	jg	.L1
	cmpl	%r13d, %r14d
	jg	.L2
.L1:
	popq	%rbx
	movl	%r9d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE54:
	.size	DES_quad_cksum, .-DES_quad_cksum
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
