	.file	"bio_cb.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"BIO[%p]: "
.LC1:
	.string	"Free - %s\n"
.LC2:
	.string	"read(%d,%lu) - %s fd=%d\n"
.LC3:
	.string	"read(%d,%lu) - %s\n"
.LC4:
	.string	"write(%d,%lu) - %s fd=%d\n"
.LC5:
	.string	"write(%d,%lu) - %s\n"
.LC6:
	.string	"puts() - %s\n"
.LC7:
	.string	"gets(%lu) - %s\n"
.LC8:
	.string	"ctrl(%lu) - %s\n"
.LC9:
	.string	"read return %ld\n"
.LC10:
	.string	"write return %ld\n"
.LC11:
	.string	"gets return %ld\n"
.LC12:
	.string	"puts return %ld\n"
.LC13:
	.string	"ctrl return %ld\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC14:
	.string	"bio callback - unknown type (%d)\n"
	.text
	.p2align 4
	.globl	BIO_debug_callback
	.type	BIO_debug_callback, @function
BIO_debug_callback:
.LFB267:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-320(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	$1, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	movl	$256, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$296, %rsp
	movl	%ecx, -324(%rbp)
	movq	%rdi, %rcx
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$-128, %r12b
	cmovne	%r9, %r13
	call	BIO_snprintf@PLT
	movl	$0, %edx
	movl	$256, %esi
	testl	%eax, %eax
	cmovs	%edx, %eax
	movslq	%eax, %rdi
	subl	%eax, %esi
	addq	%r14, %rdi
	movslq	%esi, %rsi
	cmpl	$6, %r12d
	jg	.L3
	testl	%r12d, %r12d
	jle	.L5
	cmpl	$6, %r12d
	ja	.L12
	leaq	.L14(%rip), %rdx
	movslq	(%rdx,%r12,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L14:
	.long	.L12-.L14
	.long	.L12-.L14
	.long	.L18-.L14
	.long	.L17-.L14
	.long	.L16-.L14
	.long	.L15-.L14
	.long	.L13-.L14
	.text
	.p2align 4,,10
	.p2align 3
.L3:
	leal	-130(%r12), %eax
	cmpl	$4, %eax
	ja	.L5
	leaq	.L7(%rip), %rdx
	movq	%rbx, %rcx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L7:
	.long	.L11-.L7
	.long	.L10-.L7
	.long	.L9-.L7
	.long	.L8-.L7
	.long	.L6-.L7
	.text
.L8:
	leaq	.LC11(%rip), %rdx
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	.p2align 4,,10
	.p2align 3
.L20:
	movq	24(%r15), %rdi
	testq	%rdi, %rdi
	je	.L23
	movq	%r14, %rdx
.L24:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L24
	movl	%eax, %ecx
	movq	%r14, %rsi
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	cmove	%rcx, %rdx
	movl	%eax, %ebx
	addb	%al, %bl
	sbbq	$3, %rdx
	subq	%r14, %rdx
	call	BIO_write@PLT
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L34
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L12:
	.cfi_restore_state
	movq	(%r15), %rax
	leaq	.LC1(%rip), %rdx
	movq	8(%rax), %rcx
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	jmp	.L20
.L13:
	movq	(%r15), %rax
	movslq	-324(%rbp), %rcx
	leaq	.LC8(%rip), %rdx
	movq	8(%rax), %r8
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	jmp	.L20
.L18:
	movq	(%r15), %rax
	movl	48(%r15), %ecx
	movslq	-324(%rbp), %r8
	movq	8(%rax), %r9
	movl	(%rax), %eax
	testb	$1, %ah
	je	.L21
	subq	$8, %rsp
	leaq	.LC2(%rip), %rdx
	xorl	%eax, %eax
	pushq	%rcx
	call	BIO_snprintf@PLT
	popq	%rcx
	popq	%rsi
	jmp	.L20
.L17:
	movq	(%r15), %rax
	movl	48(%r15), %ecx
	movslq	-324(%rbp), %r8
	movq	8(%rax), %r9
	movl	(%rax), %eax
	testb	$1, %ah
	je	.L22
	subq	$8, %rsp
	leaq	.LC4(%rip), %rdx
	xorl	%eax, %eax
	pushq	%rcx
	call	BIO_snprintf@PLT
	popq	%rax
	popq	%rdx
	jmp	.L20
.L16:
	movq	(%r15), %rax
	leaq	.LC6(%rip), %rdx
	movq	8(%rax), %rcx
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	jmp	.L20
.L15:
	movq	(%r15), %rax
	movslq	-324(%rbp), %rcx
	leaq	.LC7(%rip), %rdx
	movq	8(%rax), %r8
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	jmp	.L20
.L6:
	leaq	.LC13(%rip), %rdx
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	jmp	.L20
.L11:
	leaq	.LC9(%rip), %rdx
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	jmp	.L20
.L10:
	leaq	.LC10(%rip), %rdx
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	jmp	.L20
.L9:
	leaq	.LC12(%rip), %rdx
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L23:
	movq	stderr(%rip), %rsi
	movq	%r14, %rdi
	call	fputs@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	movl	%r12d, %ecx
	leaq	.LC14(%rip), %rdx
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L22:
	leaq	.LC5(%rip), %rdx
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L21:
	leaq	.LC3(%rip), %rdx
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	jmp	.L20
.L34:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE267:
	.size	BIO_debug_callback, .-BIO_debug_callback
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
