	.file	"cmll_misc.c"
	.text
	.p2align 4
	.globl	Camellia_set_key
	.type	Camellia_set_key, @function
Camellia_set_key:
.LFB0:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	testq	%rdi, %rdi
	je	.L11
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdx, %rbx
	subq	$8, %rsp
	testq	%rdx, %rdx
	je	.L4
	movl	%esi, %eax
	movl	%esi, %edi
	andl	$-65, %eax
	cmpl	$128, %eax
	je	.L6
	cmpl	$256, %esi
	jne	.L5
.L6:
	movq	%rbx, %rdx
	movq	%r8, %rsi
	call	Camellia_Ekeygen@PLT
	movl	%eax, 272(%rbx)
	xorl	%eax, %eax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	movl	$-2, %eax
	jmp	.L1
.L4:
	movl	$-1, %eax
	jmp	.L1
.L11:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	orl	$-1, %eax
	ret
	.cfi_endproc
.LFE0:
	.size	Camellia_set_key, .-Camellia_set_key
	.p2align 4
	.globl	Camellia_encrypt
	.type	Camellia_encrypt, @function
Camellia_encrypt:
.LFB1:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movl	272(%rdx), %edi
	movq	%rsi, %rcx
	movq	%r8, %rsi
	jmp	Camellia_EncryptBlock_Rounds@PLT
	.cfi_endproc
.LFE1:
	.size	Camellia_encrypt, .-Camellia_encrypt
	.p2align 4
	.globl	Camellia_decrypt
	.type	Camellia_decrypt, @function
Camellia_decrypt:
.LFB2:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movl	272(%rdx), %edi
	movq	%rsi, %rcx
	movq	%r8, %rsi
	jmp	Camellia_DecryptBlock_Rounds@PLT
	.cfi_endproc
.LFE2:
	.size	Camellia_decrypt, .-Camellia_decrypt
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
