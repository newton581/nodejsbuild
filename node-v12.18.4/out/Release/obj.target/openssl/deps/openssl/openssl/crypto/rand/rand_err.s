	.file	"rand_err.c"
	.text
	.p2align 4
	.globl	ERR_load_RAND_strings
	.type	ERR_load_RAND_strings, @function
ERR_load_RAND_strings:
.LFB203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$604499968, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_func_error_string@PLT
	testq	%rax, %rax
	je	.L5
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	leaq	RAND_str_functs(%rip), %rdi
	call	ERR_load_strings_const@PLT
	leaq	RAND_str_reasons(%rip), %rdi
	call	ERR_load_strings_const@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE203:
	.size	ERR_load_RAND_strings, .-ERR_load_RAND_strings
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"additional input too long"
.LC1:
	.string	"already instantiated"
.LC2:
	.string	"argument out of range"
.LC3:
	.string	"Cannot open file"
.LC4:
	.string	"drbg already initialized"
.LC5:
	.string	"drbg not initialised"
.LC6:
	.string	"entropy input too long"
.LC7:
	.string	"entropy out of range"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"error entropy pool was ignored"
	.section	.rodata.str1.1
.LC9:
	.string	"error initialising drbg"
.LC10:
	.string	"error instantiating drbg"
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"error retrieving additional input"
	.section	.rodata.str1.1
.LC12:
	.string	"error retrieving entropy"
.LC13:
	.string	"error retrieving nonce"
.LC14:
	.string	"failed to create lock"
.LC15:
	.string	"Function not implemented"
.LC16:
	.string	"Error writing file"
.LC17:
	.string	"generate error"
.LC18:
	.string	"internal error"
.LC19:
	.string	"in error state"
.LC20:
	.string	"Not a regular file"
.LC21:
	.string	"not instantiated"
	.section	.rodata.str1.8
	.align 8
.LC22:
	.string	"no drbg implementation selected"
	.section	.rodata.str1.1
.LC23:
	.string	"parent locking not enabled"
.LC24:
	.string	"parent strength too weak"
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"personalisation string too long"
	.align 8
.LC26:
	.string	"prediction resistance not supported"
	.section	.rodata.str1.1
.LC27:
	.string	"PRNG not seeded"
.LC28:
	.string	"random pool overflow"
.LC29:
	.string	"random pool underflow"
.LC30:
	.string	"request too large for drbg"
.LC31:
	.string	"reseed error"
.LC32:
	.string	"selftest failure"
.LC33:
	.string	"too little nonce requested"
.LC34:
	.string	"too much nonce requested"
.LC35:
	.string	"unsupported drbg flags"
.LC36:
	.string	"unsupported drbg type"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	RAND_str_reasons, @object
	.size	RAND_str_reasons, 608
RAND_str_reasons:
	.quad	603979878
	.quad	.LC0
	.quad	603979879
	.quad	.LC1
	.quad	603979881
	.quad	.LC2
	.quad	603979897
	.quad	.LC3
	.quad	603979905
	.quad	.LC4
	.quad	603979880
	.quad	.LC5
	.quad	603979882
	.quad	.LC6
	.quad	603979900
	.quad	.LC7
	.quad	603979903
	.quad	.LC8
	.quad	603979883
	.quad	.LC9
	.quad	603979884
	.quad	.LC10
	.quad	603979885
	.quad	.LC11
	.quad	603979886
	.quad	.LC12
	.quad	603979887
	.quad	.LC13
	.quad	603979902
	.quad	.LC14
	.quad	603979877
	.quad	.LC15
	.quad	603979899
	.quad	.LC16
	.quad	603979888
	.quad	.LC17
	.quad	603979889
	.quad	.LC18
	.quad	603979890
	.quad	.LC19
	.quad	603979898
	.quad	.LC20
	.quad	603979891
	.quad	.LC21
	.quad	603979904
	.quad	.LC22
	.quad	603979906
	.quad	.LC23
	.quad	603979907
	.quad	.LC24
	.quad	603979892
	.quad	.LC25
	.quad	603979909
	.quad	.LC26
	.quad	603979876
	.quad	.LC27
	.quad	603979901
	.quad	.LC28
	.quad	603979910
	.quad	.LC29
	.quad	603979893
	.quad	.LC30
	.quad	603979894
	.quad	.LC31
	.quad	603979895
	.quad	.LC32
	.quad	603979911
	.quad	.LC33
	.quad	603979912
	.quad	.LC34
	.quad	603979908
	.quad	.LC35
	.quad	603979896
	.quad	.LC36
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC37:
	.string	"data_collect_method"
.LC38:
	.string	"drbg_bytes"
.LC39:
	.string	"drbg_get_entropy"
.LC40:
	.string	"drbg_setup"
.LC41:
	.string	"get_entropy"
.LC42:
	.string	"RAND_bytes"
.LC43:
	.string	"rand_drbg_enable_locking"
.LC44:
	.string	"RAND_DRBG_generate"
.LC45:
	.string	"rand_drbg_get_entropy"
.LC46:
	.string	"rand_drbg_get_nonce"
.LC47:
	.string	"RAND_DRBG_instantiate"
.LC48:
	.string	"RAND_DRBG_new"
.LC49:
	.string	"RAND_DRBG_reseed"
.LC50:
	.string	"rand_drbg_restart"
.LC51:
	.string	"RAND_DRBG_set"
.LC52:
	.string	"RAND_DRBG_set_defaults"
.LC53:
	.string	"RAND_DRBG_uninstantiate"
.LC54:
	.string	"RAND_load_file"
.LC55:
	.string	"rand_pool_acquire_entropy"
.LC56:
	.string	"rand_pool_add"
.LC57:
	.string	"rand_pool_add_begin"
.LC58:
	.string	"rand_pool_add_end"
.LC59:
	.string	"rand_pool_attach"
.LC60:
	.string	"rand_pool_bytes_needed"
.LC61:
	.string	"rand_pool_grow"
.LC62:
	.string	"rand_pool_new"
.LC63:
	.string	"RAND_pseudo_bytes"
.LC64:
	.string	"RAND_write_file"
	.section	.data.rel.ro.local
	.align 32
	.type	RAND_str_functs, @object
	.size	RAND_str_functs, 464
RAND_str_functs:
	.quad	604499968
	.quad	.LC37
	.quad	604393472
	.quad	.LC38
	.quad	604409856
	.quad	.LC39
	.quad	604459008
	.quad	.LC40
	.quad	604413952
	.quad	.LC41
	.quad	604389376
	.quad	.LC42
	.quad	604467200
	.quad	.LC43
	.quad	604418048
	.quad	.LC44
	.quad	604471296
	.quad	.LC45
	.quad	604483584
	.quad	.LC46
	.quad	604422144
	.quad	.LC47
	.quad	604426240
	.quad	.LC48
	.quad	604430336
	.quad	.LC49
	.quad	604397568
	.quad	.LC50
	.quad	604405760
	.quad	.LC51
	.quad	604475392
	.quad	.LC52
	.quad	604463104
	.quad	.LC53
	.quad	604434432
	.quad	.LC54
	.quad	604479488
	.quad	.LC55
	.quad	604401664
	.quad	.LC56
	.quad	604442624
	.quad	.LC57
	.quad	604446720
	.quad	.LC58
	.quad	604487680
	.quad	.LC59
	.quad	604450816
	.quad	.LC60
	.quad	604491776
	.quad	.LC61
	.quad	604454912
	.quad	.LC62
	.quad	604495872
	.quad	.LC63
	.quad	604438528
	.quad	.LC64
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
