	.file	"ecdsa_sign.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ec/ecdsa_sign.c"
	.text
	.p2align 4
	.globl	ECDSA_do_sign
	.type	ECDSA_do_sign, @function
ECDSA_do_sign:
.LFB377:
	.cfi_startproc
	endbr64
	movq	(%rdx), %rax
	movq	96(%rax), %rax
	testq	%rax, %rax
	je	.L2
	movq	%rdx, %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L2:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$152, %edx
	movl	$251, %esi
	movl	$16, %edi
	movl	$25, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE377:
	.size	ECDSA_do_sign, .-ECDSA_do_sign
	.p2align 4
	.globl	ECDSA_do_sign_ex
	.type	ECDSA_do_sign_ex, @function
ECDSA_do_sign_ex:
.LFB378:
	.cfi_startproc
	endbr64
	movq	(%r8), %rax
	movq	96(%rax), %rax
	testq	%rax, %rax
	je	.L7
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L7:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$152, %edx
	movl	$251, %esi
	movl	$16, %edi
	movl	$25, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE378:
	.size	ECDSA_do_sign_ex, .-ECDSA_do_sign_ex
	.p2align 4
	.globl	ECDSA_sign
	.type	ECDSA_sign, @function
ECDSA_sign:
.LFB379:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%r9), %rax
	movq	80(%rax), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rax, %rax
	je	.L11
	pushq	%r9
	xorl	%r9d, %r9d
	pushq	$0
	call	*%rax
	popq	%rdx
	popq	%rcx
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movl	$41, %r8d
	movl	$152, %edx
	movl	$254, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE379:
	.size	ECDSA_sign, .-ECDSA_sign
	.p2align 4
	.globl	ECDSA_sign_ex
	.type	ECDSA_sign_ex, @function
ECDSA_sign_ex:
.LFB380:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	24(%rbp), %rax
	movq	(%rax), %rax
	movq	80(%rax), %rax
	testq	%rax, %rax
	je	.L15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	movl	$41, %r8d
	movl	$152, %edx
	movl	$254, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE380:
	.size	ECDSA_sign_ex, .-ECDSA_sign_ex
	.p2align 4
	.globl	ECDSA_sign_setup
	.type	ECDSA_sign_setup, @function
ECDSA_sign_setup:
.LFB381:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	88(%rax), %rax
	testq	%rax, %rax
	je	.L18
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L18:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$152, %edx
	movl	$248, %esi
	movl	$16, %edi
	movl	$50, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE381:
	.size	ECDSA_sign_setup, .-ECDSA_sign_setup
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
