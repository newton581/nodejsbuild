	.file	"bf_enc.c"
	.text
	.p2align 4
	.globl	BF_encrypt
	.type	BF_encrypt, @function
BF_encrypt:
.LFB4:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	72(%rsi), %rax
	movq	%rdi, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	(%rdi), %edx
	xorl	(%rsi), %edx
	movl	4(%rdi), %ebx
	movl	%edx, %r9d
	movl	%edx, %ecx
	xorl	4(%rsi), %ebx
	shrl	$16, %r9d
	shrl	$24, %ecx
	movl	%ebx, %r8d
	movzbl	%r9b, %r9d
	movl	(%rax,%rcx,4), %ecx
	movl	1024(%rax,%r9,4), %ebx
	movzbl	%dl, %r9d
	addl	%ecx, %ebx
	movzbl	%dh, %ecx
	xorl	8(%rsi), %edx
	xorl	2048(%rax,%rcx,4), %ebx
	addl	3072(%rax,%r9,4), %ebx
	movl	%edx, %ecx
	xorl	%r8d, %ebx
	movl	%ebx, %r8d
	movl	%ebx, %edx
	movzbl	%bh, %edi
	shrl	$16, %r8d
	shrl	$24, %edx
	movzbl	%r8b, %r8d
	movl	(%rax,%rdx,4), %edx
	addl	1024(%rax,%r8,4), %edx
	movzbl	%bl, %r8d
	xorl	2048(%rax,%rdi,4), %edx
	addl	3072(%rax,%r8,4), %edx
	xorl	12(%rsi), %ebx
	xorl	%ecx, %edx
	movl	%edx, %ecx
	movl	%edx, %r9d
	shrl	$16, %ecx
	shrl	$24, %r9d
	movzbl	%cl, %ecx
	movl	(%rax,%r9,4), %r8d
	addl	1024(%rax,%rcx,4), %r8d
	movzbl	%dh, %ecx
	xorl	2048(%rax,%rcx,4), %r8d
	movzbl	%dl, %ecx
	xorl	16(%rsi), %edx
	addl	3072(%rax,%rcx,4), %r8d
	xorl	%r8d, %ebx
	movl	%ebx, %r8d
	movl	%ebx, %ecx
	movzbl	%bh, %edi
	shrl	$16, %r8d
	shrl	$24, %ecx
	movzbl	%r8b, %r8d
	movl	(%rax,%rcx,4), %ecx
	addl	1024(%rax,%r8,4), %ecx
	movzbl	%bl, %r8d
	xorl	2048(%rax,%rdi,4), %ecx
	addl	3072(%rax,%r8,4), %ecx
	xorl	20(%rsi), %ebx
	xorl	%ecx, %edx
	movl	%edx, %ecx
	movl	%edx, %r9d
	shrl	$16, %ecx
	shrl	$24, %r9d
	movzbl	%cl, %ecx
	movl	(%rax,%r9,4), %r8d
	addl	1024(%rax,%rcx,4), %r8d
	movzbl	%dh, %ecx
	xorl	2048(%rax,%rcx,4), %r8d
	movzbl	%dl, %ecx
	xorl	24(%rsi), %edx
	addl	3072(%rax,%rcx,4), %r8d
	xorl	%r8d, %ebx
	movl	%ebx, %r8d
	movl	%ebx, %ecx
	movzbl	%bh, %edi
	shrl	$16, %r8d
	shrl	$24, %ecx
	movzbl	%r8b, %r8d
	movl	(%rax,%rcx,4), %ecx
	addl	1024(%rax,%r8,4), %ecx
	movzbl	%bl, %r8d
	xorl	2048(%rax,%rdi,4), %ecx
	addl	3072(%rax,%r8,4), %ecx
	xorl	28(%rsi), %ebx
	xorl	%ecx, %edx
	movl	%edx, %ecx
	movl	%edx, %r9d
	shrl	$16, %ecx
	shrl	$24, %r9d
	movzbl	%cl, %ecx
	movl	(%rax,%r9,4), %r8d
	addl	1024(%rax,%rcx,4), %r8d
	movzbl	%dh, %ecx
	xorl	2048(%rax,%rcx,4), %r8d
	movzbl	%dl, %ecx
	xorl	32(%rsi), %edx
	addl	3072(%rax,%rcx,4), %r8d
	xorl	%r8d, %ebx
	movl	%ebx, %r8d
	movl	%ebx, %ecx
	movzbl	%bh, %edi
	shrl	$16, %r8d
	shrl	$24, %ecx
	movzbl	%r8b, %r8d
	movl	(%rax,%rcx,4), %ecx
	addl	1024(%rax,%r8,4), %ecx
	movzbl	%bl, %r8d
	xorl	2048(%rax,%rdi,4), %ecx
	addl	3072(%rax,%r8,4), %ecx
	xorl	36(%rsi), %ebx
	xorl	%edx, %ecx
	movl	%ecx, %r9d
	movl	%ecx, %r10d
	movzbl	%ch, %edi
	movzbl	%cl, %r8d
	shrl	$16, %r9d
	shrl	$24, %r10d
	movzbl	%r9b, %r9d
	movl	(%rax,%r10,4), %edx
	addl	1024(%rax,%r9,4), %edx
	xorl	2048(%rax,%rdi,4), %edx
	addl	3072(%rax,%r8,4), %edx
	xorl	%edx, %ebx
	movl	40(%rsi), %edx
	movl	%ebx, %r8d
	movzbl	%bh, %edi
	xorl	%ecx, %edx
	shrl	$16, %r8d
	movl	%ebx, %ecx
	movzbl	%r8b, %r8d
	shrl	$24, %ecx
	movl	(%rax,%rcx,4), %ecx
	addl	1024(%rax,%r8,4), %ecx
	movzbl	%bl, %r8d
	xorl	2048(%rax,%rdi,4), %ecx
	addl	3072(%rax,%r8,4), %ecx
	xorl	%edx, %ecx
	xorl	44(%rsi), %ebx
	movl	%ecx, %r9d
	movl	%ecx, %r10d
	movzbl	%cl, %r8d
	movzbl	%ch, %edi
	shrl	$16, %r9d
	shrl	$24, %r10d
	xorl	48(%rsi), %ecx
	movzbl	%r9b, %r9d
	movl	(%rax,%r10,4), %edx
	addl	1024(%rax,%r9,4), %edx
	xorl	2048(%rax,%rdi,4), %edx
	movl	%ecx, %r9d
	addl	3072(%rax,%r8,4), %edx
	xorl	%ebx, %edx
	movl	%edx, %r8d
	movl	%edx, %ecx
	movzbl	%dh, %ebx
	shrl	$16, %r8d
	shrl	$24, %ecx
	movzbl	%r8b, %r8d
	movl	(%rax,%rcx,4), %ecx
	addl	1024(%rax,%r8,4), %ecx
	movzbl	%dl, %r8d
	xorl	2048(%rax,%rbx,4), %ecx
	addl	3072(%rax,%r8,4), %ecx
	xorl	52(%rsi), %edx
	xorl	%r9d, %ecx
	movl	%edx, %r8d
	movl	%ecx, %r9d
	movl	%ecx, %r10d
	movzbl	%ch, %ebx
	shrl	$16, %r9d
	shrl	$24, %r10d
	movzbl	%r9b, %r9d
	movl	(%rax,%r10,4), %edx
	addl	1024(%rax,%r9,4), %edx
	movzbl	%cl, %r9d
	xorl	2048(%rax,%rbx,4), %edx
	addl	3072(%rax,%r9,4), %edx
	xorl	56(%rsi), %ecx
	xorl	%r8d, %edx
	movl	%ecx, %r9d
	movl	%edx, %r8d
	movl	%edx, %ecx
	movzbl	%dh, %ebx
	shrl	$16, %r8d
	shrl	$24, %ecx
	movzbl	%r8b, %r8d
	movl	(%rax,%rcx,4), %ecx
	addl	1024(%rax,%r8,4), %ecx
	movzbl	%dl, %r8d
	xorl	2048(%rax,%rbx,4), %ecx
	addl	3072(%rax,%r8,4), %ecx
	xorl	60(%rsi), %edx
	xorl	%r9d, %ecx
	movl	%edx, %r10d
	movl	%ecx, %r8d
	movl	%ecx, %r9d
	movzbl	%ch, %ebx
	shrl	$16, %r8d
	shrl	$24, %r9d
	movzbl	%r8b, %r8d
	movl	(%rax,%r9,4), %edx
	addl	1024(%rax,%r8,4), %edx
	movzbl	%cl, %r8d
	xorl	2048(%rax,%rbx,4), %edx
	addl	3072(%rax,%r8,4), %edx
	xorl	64(%rsi), %ecx
	xorl	%r10d, %edx
	movl	68(%rsi), %r8d
	movl	%ecx, %r9d
	popq	%rbx
	movl	%edx, %esi
	movl	%edx, %ecx
	popq	%rbp
	.cfi_def_cfa 7, 8
	shrl	$16, %esi
	shrl	$24, %ecx
	xorl	%edx, %r8d
	movzbl	%sil, %esi
	movl	(%rax,%rcx,4), %ecx
	addl	1024(%rax,%rsi,4), %ecx
	movzbl	%dh, %esi
	movzbl	%dl, %edx
	xorl	2048(%rax,%rsi,4), %ecx
	addl	3072(%rax,%rdx,4), %ecx
	movl	%r8d, (%r11)
	movl	%ecx, %eax
	xorl	%r9d, %eax
	movl	%eax, 4(%r11)
	ret
	.cfi_endproc
.LFE4:
	.size	BF_encrypt, .-BF_encrypt
	.p2align 4
	.globl	BF_decrypt
	.type	BF_decrypt, @function
BF_decrypt:
.LFB5:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	72(%rsi), %rax
	movq	%rdi, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	(%rdi), %edx
	xorl	68(%rsi), %edx
	movl	4(%rdi), %ebx
	movl	%edx, %r9d
	movl	%edx, %ecx
	xorl	64(%rsi), %ebx
	shrl	$16, %r9d
	shrl	$24, %ecx
	movl	%ebx, %r8d
	movzbl	%r9b, %r9d
	movl	(%rax,%rcx,4), %ecx
	movl	1024(%rax,%r9,4), %ebx
	movzbl	%dl, %r9d
	addl	%ecx, %ebx
	movzbl	%dh, %ecx
	xorl	60(%rsi), %edx
	xorl	2048(%rax,%rcx,4), %ebx
	addl	3072(%rax,%r9,4), %ebx
	movl	%edx, %ecx
	xorl	%r8d, %ebx
	movl	%ebx, %r8d
	movl	%ebx, %edx
	movzbl	%bh, %edi
	shrl	$16, %r8d
	shrl	$24, %edx
	movzbl	%r8b, %r8d
	movl	(%rax,%rdx,4), %edx
	addl	1024(%rax,%r8,4), %edx
	movzbl	%bl, %r8d
	xorl	2048(%rax,%rdi,4), %edx
	addl	3072(%rax,%r8,4), %edx
	xorl	56(%rsi), %ebx
	xorl	%ecx, %edx
	movl	%edx, %ecx
	movl	%edx, %r9d
	shrl	$16, %ecx
	shrl	$24, %r9d
	movzbl	%cl, %ecx
	movl	(%rax,%r9,4), %r8d
	addl	1024(%rax,%rcx,4), %r8d
	movzbl	%dh, %ecx
	xorl	2048(%rax,%rcx,4), %r8d
	movzbl	%dl, %ecx
	xorl	52(%rsi), %edx
	addl	3072(%rax,%rcx,4), %r8d
	xorl	%r8d, %ebx
	movl	%ebx, %r8d
	movl	%ebx, %ecx
	movzbl	%bh, %edi
	shrl	$16, %r8d
	shrl	$24, %ecx
	movzbl	%r8b, %r8d
	movl	(%rax,%rcx,4), %ecx
	addl	1024(%rax,%r8,4), %ecx
	movzbl	%bl, %r8d
	xorl	2048(%rax,%rdi,4), %ecx
	addl	3072(%rax,%r8,4), %ecx
	xorl	48(%rsi), %ebx
	xorl	%ecx, %edx
	movl	%edx, %ecx
	movl	%edx, %r9d
	shrl	$16, %ecx
	shrl	$24, %r9d
	movzbl	%cl, %ecx
	movl	(%rax,%r9,4), %r8d
	addl	1024(%rax,%rcx,4), %r8d
	movzbl	%dh, %ecx
	xorl	2048(%rax,%rcx,4), %r8d
	movzbl	%dl, %ecx
	xorl	44(%rsi), %edx
	addl	3072(%rax,%rcx,4), %r8d
	xorl	%r8d, %ebx
	movl	%ebx, %r8d
	movl	%ebx, %ecx
	movzbl	%bh, %edi
	shrl	$16, %r8d
	shrl	$24, %ecx
	movzbl	%r8b, %r8d
	movl	(%rax,%rcx,4), %ecx
	addl	1024(%rax,%r8,4), %ecx
	movzbl	%bl, %r8d
	xorl	2048(%rax,%rdi,4), %ecx
	addl	3072(%rax,%r8,4), %ecx
	xorl	40(%rsi), %ebx
	xorl	%ecx, %edx
	movl	%edx, %ecx
	movl	%edx, %r9d
	shrl	$16, %ecx
	shrl	$24, %r9d
	movzbl	%cl, %ecx
	movl	(%rax,%r9,4), %r8d
	addl	1024(%rax,%rcx,4), %r8d
	movzbl	%dh, %ecx
	xorl	2048(%rax,%rcx,4), %r8d
	movzbl	%dl, %ecx
	xorl	36(%rsi), %edx
	addl	3072(%rax,%rcx,4), %r8d
	xorl	%r8d, %ebx
	movl	%ebx, %r8d
	movl	%ebx, %ecx
	movzbl	%bh, %edi
	shrl	$16, %r8d
	shrl	$24, %ecx
	movzbl	%r8b, %r8d
	movl	(%rax,%rcx,4), %ecx
	addl	1024(%rax,%r8,4), %ecx
	movzbl	%bl, %r8d
	xorl	2048(%rax,%rdi,4), %ecx
	addl	3072(%rax,%r8,4), %ecx
	xorl	32(%rsi), %ebx
	xorl	%edx, %ecx
	movl	%ecx, %r9d
	movl	%ecx, %r10d
	movzbl	%ch, %edi
	movzbl	%cl, %r8d
	shrl	$16, %r9d
	shrl	$24, %r10d
	movzbl	%r9b, %r9d
	movl	(%rax,%r10,4), %edx
	addl	1024(%rax,%r9,4), %edx
	xorl	2048(%rax,%rdi,4), %edx
	addl	3072(%rax,%r8,4), %edx
	xorl	%edx, %ebx
	movl	28(%rsi), %edx
	movl	%ebx, %r8d
	movzbl	%bh, %edi
	xorl	%ecx, %edx
	shrl	$16, %r8d
	movl	%ebx, %ecx
	movzbl	%r8b, %r8d
	shrl	$24, %ecx
	movl	(%rax,%rcx,4), %ecx
	addl	1024(%rax,%r8,4), %ecx
	movzbl	%bl, %r8d
	xorl	2048(%rax,%rdi,4), %ecx
	addl	3072(%rax,%r8,4), %ecx
	xorl	%edx, %ecx
	xorl	24(%rsi), %ebx
	movl	%ecx, %r9d
	movl	%ecx, %r10d
	movzbl	%cl, %r8d
	movzbl	%ch, %edi
	shrl	$16, %r9d
	shrl	$24, %r10d
	xorl	20(%rsi), %ecx
	movzbl	%r9b, %r9d
	movl	(%rax,%r10,4), %edx
	addl	1024(%rax,%r9,4), %edx
	xorl	2048(%rax,%rdi,4), %edx
	movl	%ecx, %r9d
	addl	3072(%rax,%r8,4), %edx
	xorl	%ebx, %edx
	movl	%edx, %r8d
	movl	%edx, %ecx
	movzbl	%dh, %ebx
	shrl	$16, %r8d
	shrl	$24, %ecx
	movzbl	%r8b, %r8d
	movl	(%rax,%rcx,4), %ecx
	addl	1024(%rax,%r8,4), %ecx
	movzbl	%dl, %r8d
	xorl	2048(%rax,%rbx,4), %ecx
	addl	3072(%rax,%r8,4), %ecx
	xorl	16(%rsi), %edx
	xorl	%r9d, %ecx
	movl	%edx, %r8d
	movl	%ecx, %r9d
	movl	%ecx, %r10d
	movzbl	%ch, %ebx
	shrl	$16, %r9d
	shrl	$24, %r10d
	movzbl	%r9b, %r9d
	movl	(%rax,%r10,4), %edx
	addl	1024(%rax,%r9,4), %edx
	movzbl	%cl, %r9d
	xorl	2048(%rax,%rbx,4), %edx
	addl	3072(%rax,%r9,4), %edx
	xorl	12(%rsi), %ecx
	xorl	%r8d, %edx
	movl	%ecx, %r9d
	movl	%edx, %r8d
	movl	%edx, %ecx
	movzbl	%dh, %ebx
	shrl	$16, %r8d
	shrl	$24, %ecx
	movzbl	%r8b, %r8d
	movl	(%rax,%rcx,4), %ecx
	addl	1024(%rax,%r8,4), %ecx
	movzbl	%dl, %r8d
	xorl	2048(%rax,%rbx,4), %ecx
	addl	3072(%rax,%r8,4), %ecx
	xorl	8(%rsi), %edx
	xorl	%r9d, %ecx
	movl	%edx, %r10d
	movl	%ecx, %r8d
	movl	%ecx, %r9d
	movzbl	%ch, %ebx
	shrl	$16, %r8d
	shrl	$24, %r9d
	movzbl	%r8b, %r8d
	movl	(%rax,%r9,4), %edx
	addl	1024(%rax,%r8,4), %edx
	movzbl	%cl, %r8d
	xorl	2048(%rax,%rbx,4), %edx
	addl	3072(%rax,%r8,4), %edx
	xorl	4(%rsi), %ecx
	xorl	%r10d, %edx
	movl	(%rsi), %r8d
	movl	%ecx, %r9d
	popq	%rbx
	movl	%edx, %esi
	movl	%edx, %ecx
	popq	%rbp
	.cfi_def_cfa 7, 8
	shrl	$16, %esi
	shrl	$24, %ecx
	xorl	%edx, %r8d
	movzbl	%sil, %esi
	movl	(%rax,%rcx,4), %ecx
	addl	1024(%rax,%rsi,4), %ecx
	movzbl	%dh, %esi
	movzbl	%dl, %edx
	xorl	2048(%rax,%rsi,4), %ecx
	addl	3072(%rax,%rdx,4), %ecx
	movl	%r8d, (%r11)
	movl	%ecx, %eax
	xorl	%r9d, %eax
	movl	%eax, 4(%r11)
	ret
	.cfi_endproc
.LFE5:
	.size	BF_decrypt, .-BF_decrypt
	.p2align 4
	.globl	BF_cbc_encrypt
	.type	BF_cbc_encrypt, @function
BF_cbc_encrypt:
.LFB6:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$104, %rsp
	movq	%rdx, -104(%rbp)
	movl	4(%r8), %ecx
	movq	%rdi, -112(%rbp)
	bswap	%ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-8(%rdx), %rax
	movl	(%r8), %edx
	movq	%rax, -120(%rbp)
	bswap	%edx
	testl	%r9d, %r9d
	je	.L7
	testq	%rax, %rax
	js	.L34
	shrq	$3, %rax
	movq	%r8, -88(%rbp)
	leaq	-64(%rbp), %r12
	movq	%rdi, %rbx
	movq	%rax, -72(%rbp)
	leaq	8(,%rax,8), %rax
	movq	%r12, %r13
	movq	%rax, -80(%rbp)
	addq	%rsi, %rax
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L9:
	movl	(%rbx), %esi
	movl	4(%rbx), %eax
	movq	%r13, %rdi
	addq	$8, %r15
	addq	$8, %rbx
	bswap	%eax
	bswap	%esi
	xorl	%eax, %ecx
	xorl	%esi, %edx
	movq	%r14, %rsi
	movl	%edx, -64(%rbp)
	movl	%ecx, -60(%rbp)
	call	BF_encrypt
	movl	-64(%rbp), %edx
	movl	-60(%rbp), %ecx
	movl	%edx, %eax
	movb	%dl, -5(%r15)
	shrl	$24, %eax
	movb	%al, -8(%r15)
	movl	%edx, %eax
	shrl	$16, %eax
	movb	%al, -7(%r15)
	movzbl	%dh, %eax
	movb	%al, -6(%r15)
	movl	%ecx, %eax
	shrl	$24, %eax
	movb	%al, -4(%r15)
	movl	%ecx, %eax
	shrl	$16, %eax
	movb	%al, -3(%r15)
	movzbl	%ch, %eax
	movb	%al, -2(%r15)
	movb	%cl, -1(%r15)
	cmpq	%r15, %r12
	jne	.L9
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %rdi
	addq	%rdi, -112(%rbp)
	movq	-104(%rbp), %rdi
	negq	%rax
	movq	-88(%rbp), %rbx
	salq	$3, %rax
	leaq	-16(%rdi,%rax), %rdi
	addq	-120(%rbp), %rax
	movq	%rax, -104(%rbp)
.L8:
	cmpq	$-8, %rdi
	je	.L51
	movq	-112(%rbp), %rax
	leaq	8(%rax,%rdi), %r8
	movq	-104(%rbp), %rax
	cmpq	$7, %rax
	ja	.L12
	leaq	.L14(%rip), %r9
	movslq	(%r9,%rax,4), %rdi
	addq	%r9, %rdi
	notrack jmp	*%rdi
	.section	.rodata
	.align 4
	.align 4
.L14:
	.long	.L12-.L14
	.long	.L35-.L14
	.long	.L36-.L14
	.long	.L37-.L14
	.long	.L38-.L14
	.long	.L39-.L14
	.long	.L40-.L14
	.long	.L13-.L14
	.text
	.p2align 4,,10
	.p2align 3
.L7:
	testq	%rax, %rax
	js	.L41
	shrq	$3, %rax
	movq	%r8, -144(%rbp)
	leaq	-64(%rbp), %r12
	movq	%rdi, %rbx
	movq	%rax, -136(%rbp)
	leaq	8(,%rax,8), %rax
	movq	%r12, %r13
	movl	%edx, %r12d
	movq	%rax, -128(%rbp)
	addq	%rsi, %rax
	movq	%rax, -96(%rbp)
	.p2align 4,,10
	.p2align 3
.L23:
	movl	(%rbx), %r8d
	movl	%ecx, -80(%rbp)
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	4(%rbx), %ecx
	movl	%r12d, -88(%rbp)
	addq	$8, %r15
	addq	$8, %rbx
	bswap	%r8d
	movl	%r8d, -64(%rbp)
	movl	%r8d, %r12d
	bswap	%ecx
	movl	%ecx, -60(%rbp)
	movl	%ecx, -72(%rbp)
	call	BF_decrypt
	movl	-88(%rbp), %edx
	xorl	-64(%rbp), %edx
	movl	%edx, %esi
	movl	-80(%rbp), %eax
	xorl	-60(%rbp), %eax
	movb	%dl, -5(%r15)
	shrl	$24, %esi
	movzbl	%dh, %ecx
	movb	%sil, -8(%r15)
	movl	%edx, %esi
	movl	%eax, %edx
	shrl	$24, %edx
	movb	%cl, -6(%r15)
	shrl	$16, %esi
	movzbl	%ah, %ecx
	movb	%dl, -4(%r15)
	movl	%eax, %edx
	movb	%cl, -2(%r15)
	shrl	$16, %edx
	movl	-72(%rbp), %ecx
	movb	%sil, -7(%r15)
	movb	%dl, -3(%r15)
	cmpq	-96(%rbp), %r15
	movb	%al, -1(%r15)
	jne	.L23
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rdi
	movl	%r12d, %edx
	addq	%rdi, -112(%rbp)
	movq	-104(%rbp), %rdi
	negq	%rax
	movq	-144(%rbp), %rbx
	salq	$3, %rax
	leaq	-16(%rdi,%rax), %r12
	addq	-120(%rbp), %rax
	movq	%rax, -104(%rbp)
.L22:
	cmpq	$-8, %r12
	je	.L24
	movq	-112(%rbp), %rax
	movq	%r14, %rsi
	leaq	-64(%rbp), %rdi
	movl	%ecx, -88(%rbp)
	movl	%edx, -80(%rbp)
	movl	(%rax), %r13d
	movl	4(%rax), %eax
	bswap	%r13d
	bswap	%eax
	movl	%r13d, -64(%rbp)
	movl	%eax, -60(%rbp)
	movl	%eax, -72(%rbp)
	call	BF_decrypt
	movl	-80(%rbp), %edx
	movl	-88(%rbp), %ecx
	leaq	8(%r15,%r12), %rax
	movq	-104(%rbp), %rsi
	xorl	-64(%rbp), %edx
	xorl	-60(%rbp), %ecx
	movl	-72(%rbp), %r8d
	movl	%edx, %r10d
	cmpq	$7, %rsi
	ja	.L42
	leaq	.L26(%rip), %rdi
	movslq	(%rdi,%rsi,4), %rsi
	addq	%rdi, %rsi
	notrack jmp	*%rsi
	.section	.rodata
	.align 4
	.align 4
.L26:
	.long	.L42-.L26
	.long	.L32-.L26
	.long	.L43-.L26
	.long	.L30-.L26
	.long	.L44-.L26
	.long	.L28-.L26
	.long	.L27-.L26
	.long	.L25-.L26
	.text
.L25:
	movb	%ch, -1(%rax)
	subq	$1, %rax
.L27:
	movl	%ecx, %esi
	subq	$1, %rax
	shrl	$16, %esi
	movb	%sil, (%rax)
.L28:
	shrl	$24, %ecx
	leaq	-1(%rax), %rsi
	movb	%cl, -1(%rax)
.L29:
	movb	%r10b, -1(%rsi)
	leaq	-1(%rsi), %rax
.L30:
	movl	%r10d, %ecx
	leaq	-1(%rax), %rdi
	movb	%ch, -1(%rax)
.L31:
	movl	%r10d, %esi
	leaq	-1(%rdi), %rax
	shrl	$16, %esi
	movb	%sil, -1(%rdi)
.L32:
	movl	%r10d, %edx
	movl	%r8d, %ecx
	shrl	$24, %edx
	movb	%dl, -1(%rax)
	movl	%r13d, %edx
.L24:
	bswap	%edx
	bswap	%ecx
	movl	%edx, (%rbx)
	movl	%ecx, 4(%rbx)
.L6:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L52
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	xorl	%edi, %edi
.L15:
	movzbl	-1(%r8), %r9d
	subq	$1, %r8
	salq	$16, %r9
	orl	%r9d, %edi
.L16:
	leaq	-1(%r8), %r9
	movzbl	-1(%r8), %r8d
	salq	$24, %r8
	orl	%r8d, %edi
	xorl	%edi, %ecx
.L17:
	movzbl	-1(%r9), %edi
	leaq	-1(%r9), %r8
.L18:
	leaq	-1(%r8), %r9
	movzbl	-1(%r8), %r8d
	salq	$8, %r8
	orl	%r8d, %edi
.L19:
	leaq	-1(%r9), %r8
	movzbl	-1(%r9), %r9d
	salq	$16, %r9
	orl	%r9d, %edi
.L20:
	movzbl	-1(%r8), %r8d
	salq	$24, %r8
	orl	%r8d, %edi
	xorl	%edi, %edx
.L12:
	leaq	-64(%rbp), %rdi
	movq	%r14, %rsi
	movl	%edx, -64(%rbp)
	movl	%ecx, -60(%rbp)
	call	BF_encrypt
	movl	-64(%rbp), %edx
	movl	-60(%rbp), %ecx
	movl	%edx, %eax
	movl	%ecx, %r12d
	movl	%edx, %esi
	movl	%edx, %edi
	shrl	$24, %eax
	bswap	%r12d
	movl	%r12d, 4(%r15)
	bswap	%esi
	movl	%eax, %r9d
	movzbl	%dh, %eax
	movl	%esi, (%r15)
	movl	%ecx, %esi
	shrl	$16, %edi
	movl	%eax, %r8d
	movl	%ecx, %eax
	shrl	$16, %esi
	shrl	$24, %eax
	movl	%eax, %r11d
	movzbl	%ch, %eax
.L11:
	movb	%r9b, (%rbx)
	movb	%dil, 1(%rbx)
	movb	%r8b, 2(%rbx)
	movb	%dl, 3(%rbx)
	movb	%r11b, 4(%rbx)
	movb	%sil, 5(%rbx)
	movb	%al, 6(%rbx)
	movb	%cl, 7(%rbx)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L34:
	movq	%rax, %rdi
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L41:
	movq	%rax, %r12
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L51:
	movl	%edx, %eax
	movl	%edx, %edi
	movl	%ecx, %esi
	shrl	$24, %eax
	shrl	$16, %edi
	movl	%eax, %r9d
	movzbl	%dh, %eax
	shrl	$16, %esi
	movl	%eax, %r8d
	movl	%ecx, %eax
	shrl	$24, %eax
	movl	%eax, %r11d
	movzbl	%ch, %eax
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L35:
	xorl	%edi, %edi
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L36:
	movq	%r8, %r9
	xorl	%edi, %edi
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L37:
	xorl	%edi, %edi
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L38:
	movq	%r8, %r9
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L39:
	xorl	%edi, %edi
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L13:
	movzbl	-1(%r8), %edi
	subq	$1, %r8
	sall	$8, %edi
	jmp	.L15
.L43:
	movq	%rax, %rdi
	jmp	.L31
.L44:
	movq	%rax, %rsi
	jmp	.L29
.L52:
	call	__stack_chk_fail@PLT
.L42:
	movl	%r8d, %ecx
	movl	%r13d, %edx
	jmp	.L24
	.cfi_endproc
.LFE6:
	.size	BF_cbc_encrypt, .-BF_cbc_encrypt
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
