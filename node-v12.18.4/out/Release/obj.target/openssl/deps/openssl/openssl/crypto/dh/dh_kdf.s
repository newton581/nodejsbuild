	.file	"dh_kdf.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/dh/dh_kdf.c"
	.text
	.p2align 4
	.globl	DH_KDF_X9_42
	.type	DH_KDF_X9_42, @function
DH_KDF_X9_42:
.LFB1355:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rbp), %rax
	movq	%rdx, -272(%rbp)
	movq	%rcx, -280(%rbp)
	movq	%rax, -264(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -248(%rbp)
	cmpq	$1073741824, %rcx
	jbe	.L2
.L4:
	movl	$0, -288(%rbp)
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L57
	movl	-288(%rbp), %eax
	addq	$296, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movq	%rdi, %r15
	movq	%rsi, %r13
	movq	%r8, %rbx
	movq	%r9, %r14
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L4
	movq	-264(%rbp), %rdi
	call	EVP_MD_size@PLT
	cmpq	$1073741824, 16(%rbp)
	movl	%eax, -304(%rbp)
	ja	.L55
	cmpq	$1073741824, %r13
	ja	.L55
	leaq	ctr.21504(%rip), %rax
	movq	%rbx, -224(%rbp)
	movq	%rax, -184(%rbp)
	movabsq	$17179869188, %rax
	movq	%rax, -192(%rbp)
	leaq	-192(%rbp), %rax
	movq	%rax, -200(%rbp)
	leaq	-208(%rbp), %rax
	movq	$0, -176(%rbp)
	movl	$4, -208(%rbp)
	movq	%rax, -216(%rbp)
	testq	%r14, %r14
	je	.L8
	movl	16(%rbp), %eax
	movq	%r14, -152(%rbp)
	leaq	-160(%rbp), %r14
	movl	$4, -156(%rbp)
	movq	$0, -144(%rbp)
	movl	%eax, -160(%rbp)
.L8:
	leaq	-224(%rbp), %rsi
	leaq	-248(%rbp), %rdi
	movl	%r13d, %ecx
	movq	%r14, %rdx
	call	CMS_SharedInfo_encode@PLT
	testl	%eax, %eax
	jle	.L55
	movq	-248(%rbp), %rbx
	movslq	%eax, %r14
	leaq	-252(%rbp), %rcx
	leaq	-256(%rbp), %rdx
	leaq	-232(%rbp), %rsi
	leaq	-240(%rbp), %rdi
	movq	%r14, %r8
	movq	%rcx, -320(%rbp)
	movq	%rdx, -312(%rbp)
	movq	%rsi, -296(%rbp)
	movq	%rdi, -288(%rbp)
	movq	%rbx, -240(%rbp)
	call	ASN1_get_object@PLT
	movq	-288(%rbp), %rdi
	movq	-296(%rbp), %rsi
	testb	$-128, %al
	movq	-312(%rbp), %rdx
	movq	-320(%rbp), %rcx
	jne	.L55
	cmpl	$16, -256(%rbp)
	jne	.L55
	movl	-252(%rbp), %eax
	testl	%eax, %eax
	jne	.L55
	movq	-240(%rbp), %r9
	movq	%rcx, -320(%rbp)
	movq	%rdx, -312(%rbp)
	movq	%r9, %rax
	movq	%r9, -328(%rbp)
	subq	%rbx, %rax
	movq	%r14, %rbx
	movq	%rsi, -296(%rbp)
	subq	%rax, %rbx
	movq	%rdi, -288(%rbp)
	movq	%rbx, %r8
	call	ASN1_get_object@PLT
	andl	$128, %eax
	cmpl	$16, -256(%rbp)
	jne	.L55
	orl	-252(%rbp), %eax
	jne	.L55
	movq	-240(%rbp), %r10
	movq	-328(%rbp), %r9
	movq	-320(%rbp), %rcx
	movq	-312(%rbp), %rdx
	movq	%r10, %rax
	movq	-296(%rbp), %rsi
	movq	-288(%rbp), %rdi
	movq	%r10, -336(%rbp)
	subq	%r9, %rax
	subq	%rax, %rbx
	movq	%rbx, %r8
	call	ASN1_get_object@PLT
	andl	$128, %eax
	cmpl	$6, -256(%rbp)
	jne	.L55
	orl	-252(%rbp), %eax
	jne	.L55
	movq	-336(%rbp), %r10
	movq	-232(%rbp), %rax
	addq	-240(%rbp), %rax
	movq	-320(%rbp), %rcx
	movq	%rax, -240(%rbp)
	subq	%r10, %rax
	movq	-312(%rbp), %rdx
	subq	%rax, %rbx
	movq	-296(%rbp), %rsi
	movq	-288(%rbp), %rdi
	movq	%rbx, %r8
	call	ASN1_get_object@PLT
	andl	$128, %eax
	cmpl	$4, -256(%rbp)
	jne	.L55
	orl	-252(%rbp), %eax
	jne	.L55
	movq	-240(%rbp), %rax
	movl	$4, %edx
	leaq	ctr.21504(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -296(%rbp)
	call	CRYPTO_memcmp@PLT
	movl	%eax, -288(%rbp)
	testl	%eax, %eax
	jne	.L55
	movslq	-304(%rbp), %rax
	movl	$1, %ebx
	movq	%r14, -304(%rbp)
	movq	%rax, -312(%rbp)
	movq	%rax, %r14
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L58:
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	EVP_DigestFinal@PLT
	testl	%eax, %eax
	je	.L7
	subq	%r14, %r13
	je	.L21
	addq	%r14, %r15
	addl	$1, %ebx
.L22:
	movq	-264(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	je	.L7
	movq	-280(%rbp), %rdx
	movq	-272(%rbp), %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L7
	movq	-296(%rbp), %rcx
	movl	%ebx, %eax
	movq	-304(%rbp), %rdx
	movq	%r12, %rdi
	bswap	%eax
	movl	%eax, (%rcx)
	movq	-248(%rbp), %rsi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L7
	cmpq	%r14, %r13
	jnb	.L58
	leaq	-128(%rbp), %r14
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	EVP_DigestFinal@PLT
	testl	%eax, %eax
	je	.L7
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%r13, %rdx
	call	memcpy@PLT
	movq	-312(%rbp), %rsi
	movq	%r14, %rdi
	call	OPENSSL_cleanse@PLT
.L21:
	movl	$1, -288(%rbp)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L55:
	movl	$0, -288(%rbp)
.L7:
	movq	-248(%rbp), %rdi
	movl	$146, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	call	EVP_MD_CTX_free@PLT
	jmp	.L1
.L57:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1355:
	.size	DH_KDF_X9_42, .-DH_KDF_X9_42
	.data
	.type	ctr.21504, @object
	.size	ctr.21504, 4
ctr.21504:
	.ascii	"\363\027\"S"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
