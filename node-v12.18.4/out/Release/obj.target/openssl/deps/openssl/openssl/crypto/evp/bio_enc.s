	.file	"bio_enc.c"
	.text
	.p2align 4
	.type	enc_callback_ctrl, @function
enc_callback_ctrl:
.LFB425:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	call	BIO_next@PLT
	testq	%rax, %rax
	je	.L2
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	%rax, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_callback_ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE425:
	.size	enc_callback_ctrl, .-enc_callback_ctrl
	.p2align 4
	.type	enc_write, @function
enc_write:
.LFB423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	%edx, -52(%rbp)
	call	BIO_get_data@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	BIO_next@PLT
	testq	%r14, %r14
	je	.L10
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L10
	movl	$15, %esi
	movq	%r12, %rdi
	call	BIO_clear_flags@PLT
	movl	4(%r14), %edx
	movl	(%r14), %r15d
	subl	%edx, %r15d
	testl	%r15d, %r15d
	jg	.L9
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L12:
	movl	4(%r14), %edx
	subl	%eax, %r15d
	addl	%eax, %edx
	movl	%edx, 4(%r14)
	testl	%r15d, %r15d
	jle	.L13
.L9:
	movslq	%edx, %rdx
	movq	%rbx, %rdi
	leaq	48(%r14,%rdx), %rsi
	movl	%r15d, %edx
	call	BIO_write@PLT
	testl	%eax, %eax
	jg	.L12
	movq	%r12, %rdi
	movl	%eax, -52(%rbp)
	call	BIO_copy_next_retry@PLT
	movl	-52(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L10
	movl	-52(%rbp), %edx
	testl	%edx, %edx
	jg	.L26
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%eax, %eax
.L5:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	leaq	48(%r14), %rax
	movl	$0, 4(%r14)
	movq	%rax, -64(%rbp)
	movl	-52(%rbp), %eax
	movl	%eax, -56(%rbp)
.L16:
	movl	-56(%rbp), %eax
	movl	$4096, %r15d
	movq	24(%r14), %rdi
	movq	%r13, %rcx
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	cmpl	$4096, %eax
	cmovle	%eax, %r15d
	movl	%r15d, %r8d
	call	EVP_CipherUpdate@PLT
	testl	%eax, %eax
	je	.L29
	movslq	%r15d, %r8
	subl	%r15d, -56(%rbp)
	movl	(%r14), %r15d
	xorl	%eax, %eax
	movl	$0, 4(%r14)
	addq	%r8, %r13
	testl	%r15d, %r15d
	jg	.L15
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L18:
	subl	%eax, %r15d
	addl	4(%r14), %eax
	movl	%eax, 4(%r14)
	testl	%r15d, %r15d
	jle	.L19
.L15:
	cltq
	movl	%r15d, %edx
	movq	%rbx, %rdi
	leaq	48(%r14,%rax), %rsi
	call	BIO_write@PLT
	testl	%eax, %eax
	jg	.L18
	movq	%r12, %rdi
	movl	%eax, -64(%rbp)
	call	BIO_copy_next_retry@PLT
	movl	-52(%rbp), %eax
	movl	-56(%rbp), %ecx
	movl	%eax, %edx
	subl	%ecx, %edx
	cmpl	%ecx, %eax
	movl	-64(%rbp), %eax
	cmovne	%edx, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movl	-56(%rbp), %eax
	movq	$0, (%r14)
	testl	%eax, %eax
	jg	.L16
	movq	%r12, %rdi
	call	BIO_copy_next_retry@PLT
	movl	-52(%rbp), %eax
	jmp	.L5
.L29:
	movl	$15, %esi
	movq	%r12, %rdi
	movl	%eax, -52(%rbp)
	call	BIO_clear_flags@PLT
	movl	$0, 16(%r14)
	movl	-52(%rbp), %eax
	jmp	.L5
	.cfi_endproc
.LFE423:
	.size	enc_write, .-enc_write
	.p2align 4
	.type	enc_ctrl, @function
enc_ctrl:
.LFB424:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$24, %rsp
	call	BIO_get_data@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	BIO_next@PLT
	movq	%rax, -56(%rbp)
	testq	%r14, %r14
	je	.L45
	cmpl	$13, %ebx
	jg	.L33
	testl	%ebx, %ebx
	jle	.L34
	cmpl	$13, %ebx
	ja	.L34
	leaq	.L36(%rip), %rsi
	movl	%ebx, %edx
	movslq	(%rsi,%rdx,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L36:
	.long	.L34-.L36
	.long	.L41-.L36
	.long	.L40-.L36
	.long	.L34-.L36
	.long	.L34-.L36
	.long	.L34-.L36
	.long	.L34-.L36
	.long	.L34-.L36
	.long	.L34-.L36
	.long	.L34-.L36
	.long	.L39-.L36
	.long	.L56-.L36
	.long	.L37-.L36
	.long	.L35-.L36
	.text
.L56:
	leaq	48(%r14), %rbx
.L38:
	movl	4(%r14), %eax
	cmpl	%eax, (%r14)
	jne	.L47
	movl	12(%r14), %eax
	testl	%eax, %eax
	jne	.L48
	movl	$1, 12(%r14)
	movq	24(%r14), %rdi
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movl	$0, 4(%r14)
	call	EVP_CipherFinal_ex@PLT
	movl	%eax, %edx
	cltq
	movl	%edx, 16(%r14)
	testq	%rax, %rax
	jg	.L38
.L30:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	cmpl	$113, %ebx
	je	.L42
	cmpl	$129, %ebx
	jne	.L62
	movq	24(%r14), %rax
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, 0(%r13)
	call	BIO_set_init@PLT
	movl	$1, %eax
	jmp	.L30
.L62:
	cmpl	$101, %ebx
	jne	.L34
	movl	$15, %esi
	movq	%r12, %rdi
	call	BIO_clear_flags@PLT
	movq	-56(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r15, %rdx
	movl	$101, %esi
	call	BIO_ctrl@PLT
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	BIO_copy_next_retry@PLT
	movq	-56(%rbp), %rax
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L47:
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	enc_write
	testl	%eax, %eax
	jns	.L38
	cltq
	jmp	.L30
.L41:
	movabsq	$4294967296, %rax
	movq	24(%r14), %rdi
	movq	%rax, 12(%r14)
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	24(%r14), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	%eax, %r9d
	call	EVP_CipherInit_ex@PLT
	testl	%eax, %eax
	jne	.L63
.L45:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L35:
	.cfi_restore_state
	movl	(%r14), %eax
	subl	4(%r14), %eax
	cltq
	testq	%rax, %rax
	jg	.L30
	movq	%r13, %rcx
	movq	%r15, %rdx
	movl	$13, %esi
	jmp	.L61
.L34:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movl	%ebx, %esi
.L61:
	movq	-56(%rbp), %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_ctrl@PLT
.L40:
	.cfi_restore_state
	movl	8(%r14), %edx
	movl	$1, %eax
	testl	%edx, %edx
	jle	.L30
	movq	%r13, %rcx
	movq	%r15, %rdx
	movl	$2, %esi
	jmp	.L61
.L39:
	movl	(%r14), %eax
	subl	4(%r14), %eax
	cltq
	testq	%rax, %rax
	jg	.L30
	movq	%r13, %rcx
	movq	%r15, %rdx
	movl	$10, %esi
	jmp	.L61
.L37:
	movq	%r13, %rdi
	call	BIO_get_data@PLT
	movq	%rax, %rbx
	call	EVP_CIPHER_CTX_new@PLT
	movq	%rax, 24(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L45
	movq	24(%r14), %rsi
	call	EVP_CIPHER_CTX_copy@PLT
	cltq
	testq	%rax, %rax
	je	.L30
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%rax, -56(%rbp)
	call	BIO_set_init@PLT
	movq	-56(%rbp), %rax
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L42:
	movslq	16(%r14), %rax
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L63:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movl	$1, %esi
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L48:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movl	$11, %esi
	jmp	.L61
	.cfi_endproc
.LFE424:
	.size	enc_ctrl, .-enc_ctrl
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/evp/bio_enc.c"
	.text
	.p2align 4
	.type	enc_free, @function
enc_free:
.LFB421:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L74
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	BIO_get_data@PLT
	movq	%rax, %r13
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L64
	movq	24(%r13), %rdi
	call	EVP_CIPHER_CTX_free@PLT
	movl	$99, %ecx
	movl	$4432, %esi
	movq	%r13, %rdi
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	BIO_set_data@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	BIO_set_init@PLT
	movl	$1, %eax
.L64:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE421:
	.size	enc_free, .-enc_free
	.p2align 4
	.type	enc_new, @function
enc_new:
.LFB420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$68, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$4432, %edi
	pushq	%r12
	.cfi_offset 12, -32
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L80
	movq	%rax, %r12
	call	EVP_CIPHER_CTX_new@PLT
	movq	%rax, 24(%r12)
	testq	%rax, %rax
	je	.L81
	leaq	336(%r12), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$1, 8(%r12)
	movl	$1, 16(%r12)
	movq	%rax, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 32(%r12)
	call	BIO_set_data@PLT
	movq	%r13, %rdi
	movl	$1, %esi
	call	BIO_set_init@PLT
	popq	%r12
	movl	$1, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	movl	$69, %r8d
	movl	$65, %edx
	movl	$199, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$75, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE420:
	.size	enc_new, .-enc_new
	.p2align 4
	.type	enc_read, @function
enc_read:
.LFB422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L113
	movq	%rdi, %r14
	movq	%rsi, %r12
	movl	%edx, %ebx
	call	BIO_get_data@PLT
	movq	%r14, %rdi
	movq	%rax, %r15
	call	BIO_next@PLT
	movq	%rax, -88(%rbp)
	testq	%r15, %r15
	je	.L113
	testq	%rax, %rax
	je	.L113
	movl	(%r15), %eax
	xorl	%r13d, %r13d
	testl	%eax, %eax
	jg	.L114
.L86:
	movq	24(%r15), %rdi
	call	EVP_CIPHER_CTX_block_size@PLT
	movl	%eax, %esi
	cmpl	$1, %eax
	movl	$0, %eax
	cmovne	%esi, %eax
	movl	%eax, -80(%rbp)
	testl	%ebx, %ebx
	jle	.L88
	leaq	336(%r15), %rax
	leaq	48(%r15), %r14
	movq	%rax, %xmm0
	movq	%rax, -104(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -128(%rbp)
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L91:
	movq	24(%r15), %rdi
	cmpl	$256, %ebx
	jle	.L95
	movl	%ebx, %r11d
	subl	-80(%rbp), %r11d
	movl	%r10d, %r8d
	movq	%r12, %rsi
	cmpl	%r10d, %r11d
	leaq	-60(%rbp), %rdx
	movl	%r10d, -76(%rbp)
	cmovle	%r11d, %r8d
	movl	%r11d, -72(%rbp)
	call	EVP_CipherUpdate@PLT
	movl	-72(%rbp), %r11d
	movl	-76(%rbp), %r10d
	testl	%eax, %eax
	je	.L115
	movslq	-60(%rbp), %rdx
	subl	%r11d, %r10d
	addl	%edx, %r13d
	addq	%rdx, %r12
	subl	%edx, %ebx
	testl	%r10d, %r10d
	jle	.L116
	movslq	%r11d, %rcx
	addq	32(%r15), %rcx
	movq	24(%r15), %rdi
	movq	%rcx, 32(%r15)
.L95:
	cmpl	$256, %r10d
	movl	$256, %r8d
	movq	%r15, %rdx
	movq	%r14, %rsi
	cmovle	%r10d, %r8d
	movl	%r8d, -72(%rbp)
	call	EVP_CipherUpdate@PLT
	movslq	-72(%rbp), %r8
	testl	%eax, %eax
	je	.L117
	movl	(%r15), %ecx
	addq	%r8, 32(%r15)
	movl	$1, 8(%r15)
	testl	%ecx, %ecx
	je	.L98
.L93:
	cmpl	%ecx, %ebx
	cmovle	%ebx, %ecx
	testl	%ecx, %ecx
	jle	.L88
	movslq	%ecx, %rdx
	movq	%r12, %rdi
	movq	%r14, %rsi
	movl	%ecx, -76(%rbp)
	movq	%rdx, -72(%rbp)
	call	memcpy@PLT
	movl	-76(%rbp), %ecx
	movq	-72(%rbp), %rdx
	movl	%ecx, 4(%r15)
	addl	%ecx, %r13d
	subl	%ecx, %ebx
	addq	%rdx, %r12
.L98:
	testl	%ebx, %ebx
	jle	.L88
.L100:
	movl	8(%r15), %eax
	testl	%eax, %eax
	jle	.L88
	movq	32(%r15), %rcx
	movq	40(%r15), %rax
	cmpq	%rax, %rcx
	je	.L118
	subq	%rcx, %rax
	movl	%eax, %r10d
	testl	%eax, %eax
	jg	.L91
.L90:
	movq	-88(%rbp), %rdi
	movl	$8, %esi
	movl	%r10d, -72(%rbp)
	call	BIO_test_flags@PLT
	movl	-72(%rbp), %r10d
	testl	%eax, %eax
	jne	.L92
	movl	%r10d, 8(%r15)
	movq	24(%r15), %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	EVP_CipherFinal_ex@PLT
	movl	$0, 4(%r15)
	movl	(%r15), %ecx
	movl	%eax, 16(%r15)
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L117:
	movq	-96(%rbp), %rdi
	movl	$15, %esi
	call	BIO_clear_flags@PLT
	movl	$0, 16(%r15)
	.p2align 4,,10
	.p2align 3
.L113:
	xorl	%r13d, %r13d
.L82:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L119
	addq	$88, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	movdqa	-128(%rbp), %xmm1
	movq	-104(%rbp), %rsi
	movl	$4096, %edx
	movq	-88(%rbp), %rdi
	movups	%xmm1, 32(%r15)
	call	BIO_read@PLT
	movl	%eax, %r10d
	testl	%eax, %eax
	jle	.L90
	cltq
	movq	32(%r15), %rcx
	addq	%rax, 40(%r15)
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L116:
	movq	40(%r15), %rax
	movq	%rax, 32(%r15)
	testl	%ebx, %ebx
	jg	.L100
	.p2align 4,,10
	.p2align 3
.L88:
	movq	-96(%rbp), %rbx
	movl	$15, %esi
	movq	%rbx, %rdi
	call	BIO_clear_flags@PLT
	movq	%rbx, %rdi
	call	BIO_copy_next_retry@PLT
	testl	%r13d, %r13d
	jne	.L82
	movl	8(%r15), %r13d
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L114:
	movslq	4(%r15), %rdx
	movq	%r12, %rdi
	subl	%edx, %eax
	leaq	48(%r15,%rdx), %rsi
	cmpl	%eax, %ebx
	cmovle	%ebx, %eax
	movslq	%eax, %r14
	movq	%r14, %rdx
	movq	%r14, %r13
	addq	%r14, %r12
	subl	%r14d, %ebx
	call	memcpy@PLT
	movl	4(%r15), %eax
	addl	%r14d, %eax
	movl	%eax, 4(%r15)
	cmpl	(%r15), %eax
	jne	.L86
	movq	$0, (%r15)
	jmp	.L86
.L115:
	movq	-96(%rbp), %rdi
	movl	$15, %esi
	xorl	%r13d, %r13d
	call	BIO_clear_flags@PLT
	jmp	.L82
.L92:
	testl	%r13d, %r13d
	jne	.L120
	movl	%r10d, %r13d
	jmp	.L88
.L120:
	movq	-96(%rbp), %rbx
	movl	$15, %esi
	movq	%rbx, %rdi
	call	BIO_clear_flags@PLT
	movq	%rbx, %rdi
	call	BIO_copy_next_retry@PLT
	jmp	.L82
.L119:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE422:
	.size	enc_read, .-enc_read
	.p2align 4
	.globl	BIO_f_cipher
	.type	BIO_f_cipher, @function
BIO_f_cipher:
.LFB419:
	.cfi_startproc
	endbr64
	leaq	methods_enc(%rip), %rax
	ret
	.cfi_endproc
.LFE419:
	.size	BIO_f_cipher, .-BIO_f_cipher
	.p2align 4
	.globl	BIO_set_cipher
	.type	BIO_set_cipher, @function
BIO_set_cipher:
.LFB426:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -56(%rbp)
	movq	%rcx, -64(%rbp)
	call	BIO_get_data@PLT
	testq	%rax, %rax
	je	.L126
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	BIO_get_callback@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L125
	movslq	%r13d, %rax
	xorl	%r9d, %r9d
	movl	$4, %ecx
	movq	%r12, %rdx
	movq	%rax, -72(%rbp)
	movq	%rax, %r8
	movl	$6, %esi
	movq	%r14, %rdi
	call	*%r15
	testq	%rax, %rax
	jle	.L126
	movl	$1, %esi
	movq	%r14, %rdi
	call	BIO_set_init@PLT
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %r8
	xorl	%edx, %edx
	movq	-56(%rbp), %rcx
	movl	%r13d, %r9d
	movq	%r12, %rsi
	call	EVP_CipherInit_ex@PLT
	testl	%eax, %eax
	jne	.L135
.L126:
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	.cfi_restore_state
	movq	-72(%rbp), %r8
	movq	%r12, %rdx
	movq	%r14, %rdi
	movl	$1, %r9d
	movl	$4, %ecx
	movl	$6, %esi
	call	*%r15
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_restore_state
	movq	%r14, %rdi
	movl	$1, %esi
	call	BIO_set_init@PLT
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %r8
	movl	%r13d, %r9d
	movq	-56(%rbp), %rcx
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	EVP_CipherInit_ex@PLT
	testl	%eax, %eax
	setne	%al
	addq	$40, %rsp
	popq	%rbx
	movzbl	%al, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE426:
	.size	BIO_set_cipher, .-BIO_set_cipher
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"cipher"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	methods_enc, @object
	.size	methods_enc, 96
methods_enc:
	.long	522
	.zero	4
	.quad	.LC1
	.quad	bwrite_conv
	.quad	enc_write
	.quad	bread_conv
	.quad	enc_read
	.quad	0
	.quad	0
	.quad	enc_ctrl
	.quad	enc_new
	.quad	enc_free
	.quad	enc_callback_ctrl
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
