	.file	"cfb128.c"
	.text
	.p2align 4
	.globl	CRYPTO_cfb128_encrypt
	.type	CRYPTO_cfb128_encrypt, @function
CRYPTO_cfb128_encrypt:
.LFB151:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movq	%rcx, %rdx
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movl	(%r9), %r14d
	movl	16(%rbp), %eax
	movq	%r9, -64(%rbp)
	testl	%r14d, %r14d
	setne	%cl
	testq	%r15, %r15
	setne	%sil
	andl	%esi, %ecx
	testl	%eax, %eax
	jne	.L2
	testb	%cl, %cl
	jne	.L3
	.p2align 4,,10
	.p2align 3
.L4:
	cmpq	$15, %r15
	jbe	.L23
	leaq	-16(%r15), %rcx
	movq	%r12, -80(%rbp)
	andq	$-16, %rcx
	movq	%r15, -88(%rbp)
	movl	%r14d, %r15d
	movq	%r12, %r14
	leaq	16(%rcx), %rax
	movq	%rdx, %r12
	movq	%rax, -72(%rbp)
	addq	%rbx, %rax
	movq	%rax, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L18:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*24(%rbp)
	cmpl	$15, %r15d
	ja	.L19
	movl	%r15d, %edx
	leal	8(%r15), %ecx
	leaq	0(%r13,%rdx), %rdi
	movq	(%rbx,%rdx), %r9
	movq	(%rdi), %r10
	xorq	%r9, %r10
	movq	%r10, (%r14,%rdx)
	movq	%r9, (%rdi)
	cmpl	$15, %ecx
	ja	.L19
	leaq	0(%r13,%rcx), %rdx
	movq	(%rbx,%rcx), %rdi
	movq	(%rdx), %r9
	xorq	%rdi, %r9
	movq	%r9, (%r14,%rcx)
	movq	%rdi, (%rdx)
.L19:
	addq	$16, %r14
	addq	$16, %rbx
	xorl	%r15d, %r15d
	cmpq	-56(%rbp), %rbx
	jne	.L18
	movl	%r15d, %r14d
	movq	-88(%rbp), %r15
	movq	%r12, %rdx
	movq	-80(%rbp), %r12
	addq	-72(%rbp), %r12
	andl	$15, %r15d
.L16:
	testq	%r15, %r15
	je	.L20
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*24(%rbp)
	movl	%r14d, %edx
	movq	-56(%rbp), %r10
	leaq	0(%r13,%rdx), %rcx
	movzbl	(%r10,%rdx), %esi
	movzbl	(%rcx), %edi
	xorl	%esi, %edi
	movb	%dil, (%r12,%rdx)
	movb	%sil, (%rcx)
	movq	%r15, %rsi
	leal	1(%r14), %ecx
	subq	$1, %rsi
	je	.L21
	movl	%ecx, %edx
	leaq	0(%r13,%rdx), %rdi
	movzbl	(%r10,%rdx), %r11d
	movzbl	(%rdi), %ebx
	xorl	%r11d, %ebx
	movb	%bl, (%r12,%rdx)
	leal	2(%r14), %edx
	movb	%r11b, (%rdi)
	cmpq	$2, %r15
	je	.L21
	leaq	0(%r13,%rdx), %rdi
	movzbl	(%r10,%rdx), %r11d
	movzbl	(%rdi), %ebx
	xorl	%r11d, %ebx
	movb	%bl, (%r12,%rdx)
	leal	3(%r14), %edx
	movb	%r11b, (%rdi)
	cmpq	$3, %r15
	je	.L21
	leaq	0(%r13,%rdx), %rdi
	movzbl	(%r10,%rdx), %r11d
	movzbl	(%rdi), %ebx
	xorl	%r11d, %ebx
	movb	%bl, (%r12,%rdx)
	leal	4(%r14), %edx
	movb	%r11b, (%rdi)
	cmpq	$4, %r15
	je	.L21
	leaq	0(%r13,%rdx), %rdi
	movzbl	(%r10,%rdx), %r11d
	movzbl	(%rdi), %ebx
	xorl	%r11d, %ebx
	movb	%bl, (%r12,%rdx)
	leal	5(%r14), %edx
	movb	%r11b, (%rdi)
	cmpq	$5, %r15
	je	.L21
	leaq	0(%r13,%rdx), %rdi
	movzbl	(%r10,%rdx), %r11d
	movzbl	(%rdi), %ebx
	xorl	%r11d, %ebx
	movb	%bl, (%r12,%rdx)
	leal	6(%r14), %edx
	movb	%r11b, (%rdi)
	cmpq	$6, %r15
	je	.L21
	leaq	0(%r13,%rdx), %rdi
	movzbl	(%r10,%rdx), %r11d
	movzbl	(%rdi), %ebx
	xorl	%r11d, %ebx
	movb	%bl, (%r12,%rdx)
	leal	7(%r14), %edx
	movb	%r11b, (%rdi)
	cmpq	$7, %r15
	je	.L21
	leaq	0(%r13,%rdx), %rdi
	movzbl	(%r10,%rdx), %r11d
	movzbl	(%rdi), %ebx
	xorl	%r11d, %ebx
	movb	%bl, (%r12,%rdx)
	leal	8(%r14), %edx
	movb	%r11b, (%rdi)
	cmpq	$8, %r15
	je	.L21
	leaq	0(%r13,%rdx), %rdi
	movzbl	(%r10,%rdx), %r11d
	movzbl	(%rdi), %ebx
	xorl	%r11d, %ebx
	movb	%bl, (%r12,%rdx)
	leal	9(%r14), %edx
	movb	%r11b, (%rdi)
	cmpq	$9, %r15
	je	.L21
	leaq	0(%r13,%rdx), %rdi
	movzbl	(%r10,%rdx), %r11d
	movzbl	(%rdi), %ebx
	xorl	%r11d, %ebx
	movb	%bl, (%r12,%rdx)
	leal	10(%r14), %edx
	movb	%r11b, (%rdi)
	cmpq	$10, %r15
	je	.L21
	leaq	0(%r13,%rdx), %rdi
	movzbl	(%r10,%rdx), %r11d
	movzbl	(%rdi), %ebx
	xorl	%r11d, %ebx
	movb	%bl, (%r12,%rdx)
	leal	11(%r14), %edx
	movb	%r11b, (%rdi)
	cmpq	$11, %r15
	je	.L21
	leaq	0(%r13,%rdx), %rdi
	movzbl	(%r10,%rdx), %r11d
	movzbl	(%rdi), %ebx
	xorl	%r11d, %ebx
	movb	%bl, (%r12,%rdx)
	leal	12(%r14), %edx
	movb	%r11b, (%rdi)
	cmpq	$12, %r15
	je	.L21
	leaq	0(%r13,%rdx), %rdi
	movzbl	(%r10,%rdx), %r11d
	movzbl	(%rdi), %ebx
	xorl	%r11d, %ebx
	movb	%bl, (%r12,%rdx)
	leal	13(%r14), %edx
	movb	%r11b, (%rdi)
	cmpq	$13, %r15
	je	.L21
	leaq	0(%r13,%rdx), %rdi
	movzbl	(%r10,%rdx), %r11d
	leal	14(%r14), %r9d
	movzbl	(%rdi), %ebx
	xorl	%r11d, %ebx
	movb	%bl, (%r12,%rdx)
	movb	%r11b, (%rdi)
	cmpq	$14, %r15
	je	.L21
	movl	%r9d, %eax
	leaq	0(%r13,%rax), %r8
	movzbl	(%r10,%rax), %edx
	movzbl	(%r8), %edi
	xorl	%edx, %edi
	movb	%dil, (%r12,%rax)
	movb	%dl, (%r8)
	.p2align 4,,10
	.p2align 3
.L21:
	leal	(%rcx,%rsi), %r14d
.L20:
	movq	-64(%rbp), %rax
	movl	%r14d, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore_state
	testq	%r15, %r15
	je	.L4
.L3:
	movl	%r14d, %eax
	movzbl	(%rbx), %ecx
	addq	$1, %r12
	addl	$1, %r14d
	addq	%r13, %rax
	addq	$1, %rbx
	subq	$1, %r15
	movzbl	(%rax), %esi
	xorl	%ecx, %esi
	movb	%sil, -1(%r12)
	movb	%cl, (%rax)
	andl	$15, %r14d
	jne	.L133
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L2:
	testb	%cl, %cl
	jne	.L6
	.p2align 4,,10
	.p2align 3
.L5:
	cmpq	$15, %r15
	jbe	.L22
	leaq	-16(%r15), %rcx
	movq	%r12, -80(%rbp)
	andq	$-16, %rcx
	movq	%r15, -88(%rbp)
	movl	%r14d, %r15d
	movq	%r12, %r14
	leaq	16(%rcx), %rax
	movq	%rdx, %r12
	movq	%rax, -72(%rbp)
	addq	%rbx, %rax
	movq	%rax, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L10:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*24(%rbp)
	cmpl	$15, %r15d
	ja	.L11
	movl	%r15d, %edi
	leal	8(%r15), %eax
	leaq	0(%r13,%rdi), %r9
	movq	(%rbx,%rdi), %rdx
	xorq	(%r9), %rdx
	movq	%rdx, (%r9)
	movq	%rdx, (%r14,%rdi)
	cmpl	$15, %eax
	ja	.L11
	leaq	0(%r13,%rax), %rdi
	movq	(%rbx,%rax), %rdx
	xorq	(%rdi), %rdx
	movq	%rdx, (%rdi)
	movq	%rdx, (%r14,%rax)
.L11:
	addq	$16, %r14
	addq	$16, %rbx
	xorl	%r15d, %r15d
	cmpq	-56(%rbp), %rbx
	jne	.L10
	movl	%r15d, %r14d
	movq	-88(%rbp), %r15
	movq	%r12, %rdx
	movq	-80(%rbp), %r12
	addq	-72(%rbp), %r12
	andl	$15, %r15d
.L8:
	testq	%r15, %r15
	je	.L20
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*24(%rbp)
	movl	%r14d, %ecx
	movq	-56(%rbp), %rbx
	leaq	0(%r13,%rcx), %rsi
	movzbl	(%rbx,%rcx), %edx
	xorb	(%rsi), %dl
	movb	%dl, (%rsi)
	movq	%r15, %rsi
	movb	%dl, (%r12,%rcx)
	leal	1(%r14), %ecx
	subq	$1, %rsi
	je	.L21
	movl	%ecx, %edi
	leaq	0(%r13,%rdi), %r11
	movzbl	(%rbx,%rdi), %edx
	xorb	(%r11), %dl
	movb	%dl, (%r11)
	movb	%dl, (%r12,%rdi)
	leal	2(%r14), %edx
	cmpq	$2, %r15
	je	.L21
	leaq	0(%r13,%rdx), %r11
	movzbl	(%rbx,%rdx), %edi
	xorb	(%r11), %dil
	movb	%dil, (%r11)
	movb	%dil, (%r12,%rdx)
	leal	3(%r14), %edx
	cmpq	$3, %r15
	je	.L21
	leaq	0(%r13,%rdx), %r11
	movzbl	(%rbx,%rdx), %edi
	xorb	(%r11), %dil
	movb	%dil, (%r11)
	movb	%dil, (%r12,%rdx)
	leal	4(%r14), %edx
	cmpq	$4, %r15
	je	.L21
	leaq	0(%r13,%rdx), %r11
	movzbl	(%rbx,%rdx), %edi
	xorb	(%r11), %dil
	movb	%dil, (%r11)
	movb	%dil, (%r12,%rdx)
	leal	5(%r14), %edx
	cmpq	$5, %r15
	je	.L21
	leaq	0(%r13,%rdx), %r11
	movzbl	(%rbx,%rdx), %edi
	xorb	(%r11), %dil
	movb	%dil, (%r11)
	movb	%dil, (%r12,%rdx)
	leal	6(%r14), %edx
	cmpq	$6, %r15
	je	.L21
	leaq	0(%r13,%rdx), %r11
	movzbl	(%rbx,%rdx), %edi
	xorb	(%r11), %dil
	movb	%dil, (%r11)
	movb	%dil, (%r12,%rdx)
	leal	7(%r14), %edx
	cmpq	$7, %r15
	je	.L21
	leaq	0(%r13,%rdx), %r11
	movzbl	(%rbx,%rdx), %edi
	xorb	(%r11), %dil
	movb	%dil, (%r11)
	movb	%dil, (%r12,%rdx)
	leal	8(%r14), %edx
	cmpq	$8, %r15
	je	.L21
	leaq	0(%r13,%rdx), %r11
	movzbl	(%rbx,%rdx), %edi
	xorb	(%r11), %dil
	movb	%dil, (%r11)
	movb	%dil, (%r12,%rdx)
	leal	9(%r14), %edx
	cmpq	$9, %r15
	je	.L21
	leaq	0(%r13,%rdx), %r11
	movzbl	(%rbx,%rdx), %edi
	xorb	(%r11), %dil
	movb	%dil, (%r11)
	movb	%dil, (%r12,%rdx)
	leal	10(%r14), %edx
	cmpq	$10, %r15
	je	.L21
	leaq	0(%r13,%rdx), %r11
	movzbl	(%rbx,%rdx), %edi
	xorb	(%r11), %dil
	movb	%dil, (%r11)
	movb	%dil, (%r12,%rdx)
	leal	11(%r14), %edx
	cmpq	$11, %r15
	je	.L21
	leaq	0(%r13,%rdx), %r11
	movzbl	(%rbx,%rdx), %edi
	xorb	(%r11), %dil
	movb	%dil, (%r11)
	movb	%dil, (%r12,%rdx)
	leal	12(%r14), %edx
	cmpq	$12, %r15
	je	.L21
	leaq	0(%r13,%rdx), %r11
	movzbl	(%rbx,%rdx), %edi
	xorb	(%r11), %dil
	movb	%dil, (%r11)
	movb	%dil, (%r12,%rdx)
	leal	13(%r14), %edx
	cmpq	$13, %r15
	je	.L21
	leaq	0(%r13,%rdx), %r11
	movzbl	(%rbx,%rdx), %edi
	leal	14(%r14), %r9d
	xorb	(%r11), %dil
	movb	%dil, (%r11)
	movb	%dil, (%r12,%rdx)
	cmpq	$14, %r15
	je	.L21
	movl	%r9d, %eax
	leaq	0(%r13,%rax), %r8
	movzbl	(%rbx,%rax), %edx
	xorb	(%r8), %dl
	movb	%dl, (%r8)
	movb	%dl, (%r12,%rax)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L134:
	testq	%r15, %r15
	je	.L5
.L6:
	movl	%r14d, %ecx
	addq	$1, %rbx
	addq	$1, %r12
	addl	$1, %r14d
	addq	%r13, %rcx
	subq	$1, %r15
	movzbl	(%rcx), %eax
	xorb	-1(%rbx), %al
	movb	%al, (%rcx)
	movb	%al, -1(%r12)
	andl	$15, %r14d
	jne	.L134
	jmp	.L5
.L23:
	movq	%rbx, -56(%rbp)
	jmp	.L16
.L22:
	movq	%rbx, -56(%rbp)
	jmp	.L8
	.cfi_endproc
.LFE151:
	.size	CRYPTO_cfb128_encrypt, .-CRYPTO_cfb128_encrypt
	.p2align 4
	.globl	CRYPTO_cfb128_1_encrypt
	.type	CRYPTO_cfb128_1_encrypt, @function
CRYPTO_cfb128_1_encrypt:
.LFB153:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -80(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%rcx, -104(%rbp)
	testq	%rdx, %rdx
	je	.L135
	movq	%r8, %rbx
	xorl	%r14d, %r14d
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L145:
	movzbl	(%rbx), %r10d
	movzbl	-65(%rbp), %r9d
	xorl	%eax, %r10d
.L138:
	movl	%r12d, %edx
	movl	%r8d, %eax
	movzbl	%dh, %esi
	leal	(%r12,%r12), %edx
	movl	%esi, %edi
	addl	%esi, %esi
	sarl	$7, %edi
	orl	%edi, %edx
	movb	%dl, (%rbx)
	movq	%r12, %rdx
	shrq	$16, %rdx
	movzbl	%dl, %edx
	movl	%edx, %edi
	addl	%edx, %edx
	sarl	$7, %edi
	orl	%edi, %esi
	movl	%r12d, %edi
	shrl	$31, %edi
	movb	%sil, 1(%rbx)
	movl	%r12d, %esi
	orl	%edi, %edx
	shrl	$24, %esi
	movb	%dl, 2(%rbx)
	movq	%r12, %rdx
	addl	%esi, %esi
	shrq	$32, %rdx
	movzbl	%dl, %edx
	movl	%edx, %edi
	addl	%edx, %edx
	sarl	$7, %edi
	orl	%edi, %esi
	movb	%sil, 3(%rbx)
	movq	%r12, %rsi
	shrq	$40, %rsi
	movzbl	%sil, %esi
	movl	%esi, %edi
	addl	%esi, %esi
	sarl	$7, %edi
	orl	%edi, %edx
	movb	%dl, 4(%rbx)
	movq	%r12, %rdx
	shrq	$56, %r12
	shrq	$48, %rdx
	movzbl	%dl, %edx
	movl	%edx, %edi
	addl	%edx, %edx
	sarl	$7, %edi
	orl	%edi, %esi
	movb	%sil, 5(%rbx)
	movl	%r12d, %esi
	addl	%r12d, %r12d
	sarl	$7, %esi
	orl	%esi, %edx
	movb	%dl, 6(%rbx)
	movzbl	%r8b, %edx
	movl	%edx, %esi
	addl	%edx, %edx
	sarl	$7, %esi
	orl	%esi, %r12d
	movzbl	%ah, %esi
	movq	-88(%rbp), %rax
	movl	%esi, %edi
	movb	%r12b, 7(%rbx)
	sarl	$7, %edi
	leaq	(%rax,%r15), %r11
	movl	%r10d, %eax
	orl	%edi, %edx
	movb	%dl, 8(%rbx)
	movq	%r8, %rdx
	shrq	$16, %rdx
	addl	%esi, %esi
	shrb	$7, %r9b
	andl	$-128, %eax
	movzbl	%dl, %edx
	movzbl	%al, %eax
	movl	%edx, %edi
	addl	%edx, %edx
	sarl	$7, %edi
	orl	%edi, %esi
	movl	%r8d, %edi
	shrl	$31, %edi
	movb	%sil, 9(%rbx)
	movl	%r8d, %esi
	orl	%edi, %edx
	shrl	$24, %esi
	movb	%dl, 10(%rbx)
	movq	%r8, %rdx
	addl	%esi, %esi
	shrq	$32, %rdx
	movzbl	%dl, %edx
	movl	%edx, %edi
	addl	%edx, %edx
	sarl	$7, %edi
	orl	%edi, %esi
	movb	%sil, 11(%rbx)
	movq	%r8, %rsi
	shrq	$40, %rsi
	movzbl	%sil, %esi
	movl	%esi, %edi
	addl	%esi, %esi
	sarl	$7, %edi
	orl	%edi, %edx
	movb	%dl, 12(%rbx)
	movq	%r8, %rdx
	shrq	$56, %r8
	shrq	$48, %rdx
	movzbl	%dl, %edx
	movl	%edx, %edi
	addl	%edx, %edx
	sarl	$7, %edi
	orl	%edi, %esi
	movb	%sil, 13(%rbx)
	movl	%r8d, %esi
	addl	%r8d, %r8d
	sarl	$7, %esi
	orl	%r8d, %r9d
	orl	%esi, %edx
	movb	%r9b, 15(%rbx)
	movb	%dl, 14(%rbx)
	movl	$1, %edx
	sall	%cl, %edx
	movl	%r13d, %ecx
	andl	$7, %ecx
	notl	%edx
	andb	(%r11), %dl
	sarl	%cl, %eax
	orl	%edx, %eax
	addq	$1, %r14
	movb	%al, (%r11)
	cmpq	%r14, -96(%rbp)
	je	.L135
.L139:
	movq	-80(%rbp), %rax
	movq	%r14, %r15
	movl	%r14d, %ecx
	movq	8(%rbx), %r8
	shrq	$3, %r15
	notl	%ecx
	movq	(%rbx), %r12
	movq	-104(%rbp), %rdx
	movzbl	(%rax,%r15), %eax
	andl	$7, %ecx
	movq	%r8, -56(%rbp)
	movq	%rbx, %rsi
	movl	%ecx, -64(%rbp)
	movq	%rbx, %rdi
	movl	%r14d, %r13d
	sarl	%cl, %eax
	sall	$7, %eax
	movl	%eax, -60(%rbp)
	movb	%al, -65(%rbp)
	call	*24(%rbp)
	movl	16(%rbp), %eax
	movq	-56(%rbp), %r8
	movl	-64(%rbp), %ecx
	testl	%eax, %eax
	movl	-60(%rbp), %eax
	je	.L145
	movzbl	(%rbx), %r9d
	xorl	%eax, %r9d
	movl	%r9d, %r10d
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L135:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE153:
	.size	CRYPTO_cfb128_1_encrypt, .-CRYPTO_cfb128_1_encrypt
	.p2align 4
	.globl	CRYPTO_cfb128_8_encrypt
	.type	CRYPTO_cfb128_8_encrypt, @function
CRYPTO_cfb128_8_encrypt:
.LFB154:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rbp), %r13
	movq	%fs:40, %rsi
	movq	%rsi, -56(%rbp)
	xorl	%esi, %esi
	testq	%rdx, %rdx
	je	.L146
	movq	%rax, %r15
	leaq	(%rdi,%rdx), %rax
	movl	16(%rbp), %edx
	movdqu	(%r8), %xmm0
	movq	%rax, -104(%rbp)
	movq	%rdi, %rbx
	movq	%rcx, %r12
	movq	%r8, %r14
	testl	%edx, %edx
	jne	.L151
	.p2align 4,,10
	.p2align 3
.L149:
	movaps	%xmm0, -96(%rbp)
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	*%r13
	movzbl	(%rbx), %edx
	movzbl	(%r14), %ecx
	addq	$1, %rbx
	addq	$1, %r15
	movb	%dl, -80(%rbp)
	movdqu	-95(%rbp), %xmm0
	xorl	%edx, %ecx
	movb	%cl, -1(%r15)
	movups	%xmm0, (%r14)
	cmpq	%rbx, -104(%rbp)
	jne	.L149
.L146:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L157
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_restore_state
	movaps	%xmm0, -96(%rbp)
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	*%r13
	movzbl	(%rbx), %edx
	xorb	(%r14), %dl
	addq	$1, %rbx
	movb	%dl, -80(%rbp)
	movdqu	-95(%rbp), %xmm0
	addq	$1, %r15
	movb	%dl, -1(%r15)
	movups	%xmm0, (%r14)
	cmpq	%rbx, -104(%rbp)
	jne	.L151
	jmp	.L146
.L157:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE154:
	.size	CRYPTO_cfb128_8_encrypt, .-CRYPTO_cfb128_8_encrypt
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
