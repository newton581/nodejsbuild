	.file	"cmeth_lib.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/evp/cmeth_lib.c"
	.text
	.p2align 4
	.globl	EVP_CIPHER_meth_new
	.type	EVP_CIPHER_meth_new, @function
EVP_CIPHER_meth_new:
.LFB393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	movl	$18, %edx
	pushq	%r12
	.cfi_offset 12, -32
	movl	%edi, %r12d
	movl	$88, %edi
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	leaq	.LC0(%rip), %rsi
	subq	$8, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L1
	movl	%r12d, (%rax)
	movl	%ebx, 4(%rax)
	movl	%r13d, 8(%rax)
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE393:
	.size	EVP_CIPHER_meth_new, .-EVP_CIPHER_meth_new
	.p2align 4
	.globl	EVP_CIPHER_meth_dup
	.type	EVP_CIPHER_meth_dup, @function
EVP_CIPHER_meth_dup:
.LFB394:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$18, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	8(%rdi), %r12d
	movq	(%rdi), %r13
	movl	$88, %edi
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L8
	movq	%r13, (%rax)
	movl	%r12d, 8(%rax)
	movdqu	(%rbx), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%rbx), %xmm1
	movups	%xmm1, 16(%rax)
	movdqu	32(%rbx), %xmm2
	movups	%xmm2, 32(%rax)
	movdqu	48(%rbx), %xmm3
	movups	%xmm3, 48(%rax)
	movdqu	64(%rbx), %xmm4
	movups	%xmm4, 64(%rax)
	movq	80(%rbx), %rdx
	movq	%rdx, 80(%rax)
.L8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE394:
	.size	EVP_CIPHER_meth_dup, .-EVP_CIPHER_meth_dup
	.p2align 4
	.globl	EVP_CIPHER_meth_free
	.type	EVP_CIPHER_meth_free, @function
EVP_CIPHER_meth_free:
.LFB395:
	.cfi_startproc
	endbr64
	movl	$40, %edx
	leaq	.LC0(%rip), %rsi
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE395:
	.size	EVP_CIPHER_meth_free, .-EVP_CIPHER_meth_free
	.p2align 4
	.globl	EVP_CIPHER_meth_set_iv_length
	.type	EVP_CIPHER_meth_set_iv_length, @function
EVP_CIPHER_meth_set_iv_length:
.LFB396:
	.cfi_startproc
	endbr64
	movl	%esi, 12(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE396:
	.size	EVP_CIPHER_meth_set_iv_length, .-EVP_CIPHER_meth_set_iv_length
	.p2align 4
	.globl	EVP_CIPHER_meth_set_flags
	.type	EVP_CIPHER_meth_set_flags, @function
EVP_CIPHER_meth_set_flags:
.LFB397:
	.cfi_startproc
	endbr64
	movq	%rsi, 16(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE397:
	.size	EVP_CIPHER_meth_set_flags, .-EVP_CIPHER_meth_set_flags
	.p2align 4
	.globl	EVP_CIPHER_meth_set_impl_ctx_size
	.type	EVP_CIPHER_meth_set_impl_ctx_size, @function
EVP_CIPHER_meth_set_impl_ctx_size:
.LFB398:
	.cfi_startproc
	endbr64
	movl	%esi, 48(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE398:
	.size	EVP_CIPHER_meth_set_impl_ctx_size, .-EVP_CIPHER_meth_set_impl_ctx_size
	.p2align 4
	.globl	EVP_CIPHER_meth_set_init
	.type	EVP_CIPHER_meth_set_init, @function
EVP_CIPHER_meth_set_init:
.LFB399:
	.cfi_startproc
	endbr64
	movq	%rsi, 24(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE399:
	.size	EVP_CIPHER_meth_set_init, .-EVP_CIPHER_meth_set_init
	.p2align 4
	.globl	EVP_CIPHER_meth_set_do_cipher
	.type	EVP_CIPHER_meth_set_do_cipher, @function
EVP_CIPHER_meth_set_do_cipher:
.LFB400:
	.cfi_startproc
	endbr64
	movq	%rsi, 32(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE400:
	.size	EVP_CIPHER_meth_set_do_cipher, .-EVP_CIPHER_meth_set_do_cipher
	.p2align 4
	.globl	EVP_CIPHER_meth_set_cleanup
	.type	EVP_CIPHER_meth_set_cleanup, @function
EVP_CIPHER_meth_set_cleanup:
.LFB401:
	.cfi_startproc
	endbr64
	movq	%rsi, 40(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE401:
	.size	EVP_CIPHER_meth_set_cleanup, .-EVP_CIPHER_meth_set_cleanup
	.p2align 4
	.globl	EVP_CIPHER_meth_set_set_asn1_params
	.type	EVP_CIPHER_meth_set_set_asn1_params, @function
EVP_CIPHER_meth_set_set_asn1_params:
.LFB402:
	.cfi_startproc
	endbr64
	movq	%rsi, 56(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE402:
	.size	EVP_CIPHER_meth_set_set_asn1_params, .-EVP_CIPHER_meth_set_set_asn1_params
	.p2align 4
	.globl	EVP_CIPHER_meth_set_get_asn1_params
	.type	EVP_CIPHER_meth_set_get_asn1_params, @function
EVP_CIPHER_meth_set_get_asn1_params:
.LFB403:
	.cfi_startproc
	endbr64
	movq	%rsi, 64(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE403:
	.size	EVP_CIPHER_meth_set_get_asn1_params, .-EVP_CIPHER_meth_set_get_asn1_params
	.p2align 4
	.globl	EVP_CIPHER_meth_set_ctrl
	.type	EVP_CIPHER_meth_set_ctrl, @function
EVP_CIPHER_meth_set_ctrl:
.LFB404:
	.cfi_startproc
	endbr64
	movq	%rsi, 72(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE404:
	.size	EVP_CIPHER_meth_set_ctrl, .-EVP_CIPHER_meth_set_ctrl
	.p2align 4
	.globl	EVP_CIPHER_meth_get_init
	.type	EVP_CIPHER_meth_get_init, @function
EVP_CIPHER_meth_get_init:
.LFB405:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE405:
	.size	EVP_CIPHER_meth_get_init, .-EVP_CIPHER_meth_get_init
	.p2align 4
	.globl	EVP_CIPHER_meth_get_do_cipher
	.type	EVP_CIPHER_meth_get_do_cipher, @function
EVP_CIPHER_meth_get_do_cipher:
.LFB406:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE406:
	.size	EVP_CIPHER_meth_get_do_cipher, .-EVP_CIPHER_meth_get_do_cipher
	.p2align 4
	.globl	EVP_CIPHER_meth_get_cleanup
	.type	EVP_CIPHER_meth_get_cleanup, @function
EVP_CIPHER_meth_get_cleanup:
.LFB407:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE407:
	.size	EVP_CIPHER_meth_get_cleanup, .-EVP_CIPHER_meth_get_cleanup
	.p2align 4
	.globl	EVP_CIPHER_meth_get_set_asn1_params
	.type	EVP_CIPHER_meth_get_set_asn1_params, @function
EVP_CIPHER_meth_get_set_asn1_params:
.LFB408:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rax
	ret
	.cfi_endproc
.LFE408:
	.size	EVP_CIPHER_meth_get_set_asn1_params, .-EVP_CIPHER_meth_get_set_asn1_params
	.p2align 4
	.globl	EVP_CIPHER_meth_get_get_asn1_params
	.type	EVP_CIPHER_meth_get_get_asn1_params, @function
EVP_CIPHER_meth_get_get_asn1_params:
.LFB409:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	ret
	.cfi_endproc
.LFE409:
	.size	EVP_CIPHER_meth_get_get_asn1_params, .-EVP_CIPHER_meth_get_get_asn1_params
	.p2align 4
	.globl	EVP_CIPHER_meth_get_ctrl
	.type	EVP_CIPHER_meth_get_ctrl, @function
EVP_CIPHER_meth_get_ctrl:
.LFB410:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rax
	ret
	.cfi_endproc
.LFE410:
	.size	EVP_CIPHER_meth_get_ctrl, .-EVP_CIPHER_meth_get_ctrl
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
