	.file	"x_long.c"
	.text
	.p2align 4
	.type	long_i2c, @function
long_i2c:
.LFB446:
	.cfi_startproc
	endbr64
	movq	(%rdi), %r8
	cmpq	%r8, 40(%rcx)
	je	.L10
	xorl	%r9d, %r9d
	testq	%r8, %r8
	jns	.L3
	notq	%r8
	movl	$-1, %r9d
.L3:
	movq	%r8, %rdx
	movl	$64, %eax
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L4:
	xorl	%edi, %edi
	testq	%rdx, %rdx
	setne	%dil
	shrq	%rdx
	addq	%rdi, %rcx
	subq	$1, %rax
	jne	.L4
	leal	7(%rcx), %edx
	sarl	$3, %edx
	andl	$7, %ecx
	je	.L16
	movl	%edx, %r10d
	testq	%rsi, %rsi
	je	.L1
.L8:
	movl	%edx, %edi
	subl	$1, %edi
	js	.L1
	movslq	%edx, %rcx
	movslq	%edi, %rax
	movl	%edi, %edi
	subq	$2, %rcx
	addq	%rsi, %rax
	subq	%rdi, %rcx
	addq	%rsi, %rcx
	.p2align 4,,10
	.p2align 3
.L9:
	movl	%r9d, %edx
	subq	$1, %rax
	xorl	%r8d, %edx
	shrq	$8, %r8
	movb	%dl, 1(%rax)
	cmpq	%rcx, %rax
	jne	.L9
.L1:
	movl	%r10d, %eax
	ret
.L16:
	leal	1(%rdx), %r10d
	testq	%rsi, %rsi
	je	.L1
	movb	%r9b, (%rsi)
	addq	$1, %rsi
	jmp	.L8
.L10:
	movl	$-1, %r10d
	jmp	.L1
	.cfi_endproc
.LFE446:
	.size	long_i2c, .-long_i2c
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%ld\n"
	.text
	.p2align 4
	.type	long_print, @function
long_print:
.LFB448:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rdx
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rsi
	jmp	BIO_printf@PLT
	.cfi_endproc
.LFE448:
	.size	long_print, .-long_print
	.p2align 4
	.type	long_new, @function
long_new:
.LFB443:
	.cfi_startproc
	endbr64
	movq	40(%rsi), %rax
	movq	%rax, (%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE443:
	.size	long_new, .-long_new
	.p2align 4
	.type	long_free, @function
long_free:
.LFB444:
	.cfi_startproc
	endbr64
	movq	40(%rsi), %rax
	movq	%rax, (%rdi)
	ret
	.cfi_endproc
.LFE444:
	.size	long_free, .-long_free
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../deps/openssl/openssl/crypto/asn1/x_long.c"
	.text
	.p2align 4
	.type	long_c2i, @function
long_c2i:
.LFB447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$1, %edx
	jle	.L71
	movzbl	(%rsi), %eax
	testb	%al, %al
	je	.L24
	cmpb	$-1, %al
	jne	.L25
	subl	$1, %edx
	addq	$1, %rsi
	cmpl	$8, %edx
	jg	.L26
	movzbl	(%rsi), %eax
	movl	$255, %ecx
	xorq	%rcx, %rax
	testb	$-128, %al
	je	.L72
.L32:
	movzbl	(%rsi), %eax
	xorq	%rcx, %rax
	cmpl	$1, %edx
	je	.L36
.L40:
	movzbl	1(%rsi), %r8d
	salq	$8, %rax
	xorq	%rcx, %r8
	orq	%r8, %rax
	cmpl	$2, %edx
	je	.L36
	movzbl	2(%rsi), %r8d
	salq	$8, %rax
	xorq	%rcx, %r8
	orq	%r8, %rax
	cmpl	$3, %edx
	je	.L36
	movzbl	3(%rsi), %r8d
	salq	$8, %rax
	xorq	%rcx, %r8
	orq	%r8, %rax
	cmpl	$4, %edx
	je	.L36
	movzbl	4(%rsi), %r8d
	salq	$8, %rax
	xorq	%rcx, %r8
	orq	%r8, %rax
	cmpl	$5, %edx
	je	.L36
	movzbl	5(%rsi), %r8d
	salq	$8, %rax
	xorq	%rcx, %r8
	orq	%r8, %rax
	cmpl	$6, %edx
	je	.L36
	movzbl	6(%rsi), %r8d
	salq	$8, %rax
	xorq	%rcx, %r8
	orq	%r8, %rax
	cmpl	$7, %edx
	je	.L36
	movzbl	7(%rsi), %edx
	salq	$8, %rax
	movl	$180, %r8d
	xorq	%rcx, %rdx
	orq	%rdx, %rax
	js	.L70
.L36:
	movq	%rax, %rdx
	testq	%rcx, %rcx
	notq	%rdx
	cmovne	%rdx, %rax
	cmpq	%rax, 40(%r9)
	jne	.L37
.L73:
	movl	$186, %r8d
.L70:
	movl	$128, %edx
	movl	$166, %esi
	movl	$13, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	testl	%edx, %edx
	jne	.L22
.L69:
	xorl	%eax, %eax
.L23:
	cmpq	%rax, 40(%r9)
	je	.L73
.L37:
	movq	%rax, (%rdi)
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	cmpl	$8, %edx
	jg	.L26
	testb	%al, %al
	js	.L38
	movzbl	(%rsi), %eax
	xorl	%ecx, %ecx
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L24:
	subl	$1, %edx
	addq	$1, %rsi
	cmpl	$8, %edx
	jg	.L26
	movzbl	(%rsi), %eax
	xorl	%ecx, %ecx
	xorq	%rcx, %rax
	testb	$-128, %al
	jne	.L32
.L72:
	movl	$170, %r8d
	movl	$221, %edx
	movl	$166, %esi
	movl	$13, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movl	$159, %r8d
	jmp	.L70
.L38:
	notl	%eax
	movl	$255, %ecx
	movzbl	%al, %eax
	jmp	.L40
.L22:
	cmpb	$0, (%rsi)
	js	.L74
	cmpl	$1, %edx
	jne	.L69
	movzbl	(%rsi), %eax
	jmp	.L23
.L74:
	testl	%edx, %edx
	jg	.L75
	movq	$-1, %rax
	jmp	.L23
.L75:
	movl	$255, %ecx
	jmp	.L32
	.cfi_endproc
.LFE447:
	.size	long_c2i, .-long_c2i
	.globl	ZLONG_it
	.section	.rodata.str1.1
.LC2:
	.string	"ZLONG"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	ZLONG_it, @object
	.size	ZLONG_it, 56
ZLONG_it:
	.byte	0
	.zero	7
	.quad	2
	.quad	0
	.quad	0
	.quad	long_pf
	.quad	0
	.quad	.LC2
	.globl	LONG_it
	.section	.rodata.str1.1
.LC3:
	.string	"LONG"
	.section	.data.rel.ro.local
	.align 32
	.type	LONG_it, @object
	.size	LONG_it, 56
LONG_it:
	.byte	0
	.zero	7
	.quad	2
	.quad	0
	.quad	0
	.quad	long_pf
	.quad	2147483647
	.quad	.LC3
	.section	.data.rel.local,"aw"
	.align 32
	.type	long_pf, @object
	.size	long_pf, 64
long_pf:
	.quad	0
	.quad	0
	.quad	long_new
	.quad	long_free
	.quad	long_free
	.quad	long_c2i
	.quad	long_i2c
	.quad	long_print
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
